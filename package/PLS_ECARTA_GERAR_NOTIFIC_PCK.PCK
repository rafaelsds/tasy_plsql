create or replace package pls_ecarta_gerar_notific_pck is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Enviar Notifica��es de Atraso via integra��o e-Carta dos Correios
----------------------------------------------------------------------------------------------------------------------------------
Objeto criado por Bruno Maximo AMCOM
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

procedure atualizar_notificacoes(	cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		in usuario.nm_usuario%type);

procedure atualizar_notificacoes(	nm_usuario_p		in usuario.nm_usuario%type);

procedure gerar_lote(	nr_seq_lote_p		in pls_notificacao_lote.nr_sequencia%type,
			nr_seq_pagador_p	in pls_notificacao_pagador.nr_sequencia%type,
			cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
			nm_usuario_p		in usuario.nm_usuario%type);

end pls_ecarta_gerar_notific_pck;
/

create or replace package body pls_ecarta_gerar_notific_pck is

procedure atualizar_notificacoes(	cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		in usuario.nm_usuario%type) is
	-- tipos
	type pls_solicitacao_r is record (
		nr_seq_notific_pagador	pls_notificacao_pagador.nr_sequencia%type,
		dt_baixa		pls_ecarta_solicitacao.dt_baixa%type,
		ie_tipo_baixa		pls_ecarta_solicitacao.ie_tipo_baixa%type,
		nr_aviso_recebimento	pls_ecarta_solicitacao.nr_aviso_recebimento%type,
		nm_entregador		pls_notificacao_devolucao.nm_entregador%type,
		nm_recebedor		pls_notificacao_pagador.nm_pessoa_notific%type,
		nr_seq_tipo_devolucao	pls_notif_tipo_devolucao.nr_sequencia%type,
		ie_status		pls_notificacao_pagador.ie_status%type
	);
	type pls_solicitacao_row is table of pls_solicitacao_r index by pls_integer;
	
	-- vari�veis
	pls_solicitacao_t	pls_solicitacao_row;
	ds_erro_w		varchar2(255);
	
	-- Solicita��es e-Carta baixadas de notifica��es de atraso do estabelecimento
	cursor	c01_w is
		select	d.nr_seq_notific_pagador,
			d.dt_baixa,
			d.ie_tipo_baixa,
			d.nr_aviso_recebimento,
			null nm_entregador,	--//TODO: Verificar com Correios se existe esta informa��o
			null nm_recebedor,	--//TODO: Verificar com Correios se existe esta informa��o
			(	select	max(z.nr_sequencia) nr_seq_tipo_devolucao
				from	pls_notif_tipo_devolucao z
				where	z.ie_situacao		= 'A' 
				and	z.cd_estabelecimento	= a.cd_estabelecimento
				and	z.ie_tipo_baixa_ecarta	= d.ie_tipo_baixa
			) nr_seq_tipo_devolucao,
			e.ie_status
		from	pls_notificacao_pagador	e,
			pls_ecarta_solicitacao	d,
			pls_ecarta_lote		c,
			pls_ecarta_param_matriz	b,
			pls_ecarta_parametro	a
		where	d.dt_baixa			is not null	-- e-Carta - Baixado
		and	nvl(c.ie_cancelado, 'N')	= 'N'		-- e-Carta - N�o cancelado
		and	d.ie_resposta_devolucao		is null		-- e-Carta - Sem ocorr�ncias
		and	e.ie_status			in ('E', 'G')	-- Notifica��o - Enviada e Gerada --//TODO: Verificar esta regra
		and	e.nr_sequencia			= d.nr_seq_notific_pagador
		and	d.nr_seq_ecarta_lote		= c.nr_sequencia
		and	c.nr_seq_ecarta_parametro	= b.nr_seq_ecarta_parametro
		and	c.nr_seq_ecarta_matriz		= b.nr_seq_ecarta_matriz
		and	b.nr_seq_ecarta_parametro	= a.nr_sequencia
		and	a.ie_origem			= 1 -- OPS - Controle de Notifica��es de Atraso
		and	a.cd_estabelecimento		= cd_estabelecimento_p;
		
begin
	-- Carrega solicita��es
	open c01_w;
	loop fetch c01_w bulk collect into pls_solicitacao_t limit pls_util_pck.qt_registro_transacao_w;
		exit when pls_solicitacao_t.count = 0;
		begin
			-- Processa as solicita��es carregadas
			for i in pls_solicitacao_t.first..pls_solicitacao_t.last loop
				-- Reinicia vari�veis
				ds_erro_w := null;
				
				-- Verifica o tipo de baixa
				if	(pls_solicitacao_t(i).ie_tipo_baixa = '1') then
					-- Notifica��o recebida
					pls_receber_notificacao(	pls_solicitacao_t(i).nr_seq_notific_pagador,
									pls_solicitacao_t(i).dt_baixa,
									pls_solicitacao_t(i).nm_recebedor,
									null,	--Hoje os correios n�o enviam esta informa��o
									nm_usuario_p,
									'C',
									ds_erro_w);
				else
					-- Notifica��o devolvida
					pls_devolver_notificacao(	pls_solicitacao_t(i).nr_seq_notific_pagador,
									nvl(pls_solicitacao_t(i).nr_seq_tipo_devolucao, 2),
									pls_solicitacao_t(i).dt_baixa,
									nvl(pls_solicitacao_t(i).nm_entregador, 'E-Carta'),
									'C',
									'S',
									nm_usuario_p);
				end if;
				
				-- Verifica se ocorreu erro
				if	(ds_erro_w is not null) then
					wheb_mensagem_pck.exibir_mensagem_abort(460257, ds_erro_w);
				end if;
			end loop;
			
			-- Confirma altera��es da transa��o
			commit;
		exception
			when others then
				-- Defaz altera��es
				rollback;
				-- Levanta a excess�o ocorrida
				wheb_mensagem_pck.exibir_mensagem_abort(sqlerrm(sqlcode));
		end;
	end loop;
	close c01_w;
end atualizar_notificacoes;

procedure atualizar_notificacoes(	nm_usuario_p	in usuario.nm_usuario%type) is
	-- Estabelecimentos
	cursor	c01_w is
	select	cd_estabelecimento 
	from	estabelecimento a
	where	a.ie_situacao = 'A'; -- Ativo
begin
	-- Processa os estabelecimentos
	for r01_w in c01_w loop
		atualizar_notificacoes(r01_w.cd_estabelecimento, nm_usuario_p);
	end loop;
	
	-- Confirma altera��es da transa��o
	commit;
	
end atualizar_notificacoes;

procedure gerar_lote(	nr_seq_lote_p		in pls_notificacao_lote.nr_sequencia%type,
			nr_seq_pagador_p	in pls_notificacao_pagador.nr_sequencia%type,
			cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
			nm_usuario_p		in usuario.nm_usuario%type) is
	-- tipos
	type pls_pagador_r is record (
		nr_sequencia	pls_notificacao_pagador.nr_sequencia%type,
		nm_destinatario	pessoa_juridica.ds_razao_social%type,
		ds_endereco	compl_pessoa_fisica.ds_endereco%type,
		nr_endereco	pessoa_juridica.nr_endereco%type,
		ds_complemento	pessoa_juridica.ds_complemento%type,
		ds_bairro	compl_pessoa_fisica.ds_bairro%type,
		ds_municipio	compl_pessoa_fisica.ds_municipio%type,
		sg_estado	compl_pessoa_fisica.sg_estado%type,
		cd_cep		compl_pessoa_fisica.cd_cep%type,
		nr_seq_pagador	pls_contrato_pagador.nr_sequencia%type
	);
	type pls_pagador_row	is table of pls_pagador_r index by pls_integer;
	
	-- vari�veis
	pls_pagador_t			pls_pagador_row;
	nr_seq_regra_w			pls_notificacao_lote.nr_seq_regra%type;
	nr_seq_matriz_w			pls_ecarta_matriz.nr_sequencia%type;
	nr_seq_ecarta_parametro_w	pls_ecarta_parametro.nr_sequencia%type;
	ds_diretorio_relatorio_w	pls_ecarta_parametro.ds_diretorio_relatorio%type;
	nr_seq_ecarta_lote_w		pls_ecarta_lote.nr_sequencia%type;
	nr_seq_ecarta_solicitacao_w	pls_ecarta_solicitacao.nr_sequencia%type;
	nr_seq_ecarta_arq_solic_w	pls_ecarta_arquivo_solic.nr_sequencia%type;
	nr_seq_relatorio_w		pls_notificacao_regra.nr_seq_relatorio%type;
	nr_cep_w			pls_ecarta_endereco_solic.cd_cep%type;
	qt_notificacao_recebida_w	pls_integer;
	
	nr_seq_pls_relatorio_w		pls_relatorio.nr_sequencia%type;
	cd_classif_relat_w		relatorio.cd_classif_relat%type;
	cd_relatorio_w			relatorio.cd_relatorio%type;
	ds_local_rede_w			evento_tasy_utl_file.ds_local_rede%type;
	nm_arquivo_w			varchar2(255);
	
	-- Par�metro e matriz da regra do lote
	cursor	c01_w is
		select	b.nr_seq_regra, 
			a.nr_seq_ecarta_matriz, 
			c.nr_seq_ecarta_parametro,
			a.nr_seq_relatorio,
			d.ds_diretorio_relatorio
		from	pls_ecarta_parametro	d,
			pls_ecarta_param_matriz	c,
			pls_notificacao_lote	b,
			pls_notificacao_regra	a
		where	d.nr_sequencia		= c.nr_seq_ecarta_parametro
		and	c.nr_seq_ecarta_matriz	= a.nr_seq_ecarta_matriz
		and	b.nr_seq_regra		= a.nr_sequencia
		and	b.nr_sequencia		= nr_seq_lote_p;
	
	-- pagadores
	cursor c02_w is
		select	a.nr_sequencia,
			obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc)						nm_destinatario,
			pls_obter_end_pagador(a.nr_seq_pagador, 'E')						ds_endereco,
			pls_obter_end_pagador(a.nr_seq_pagador, 'NR')						nr_endereco,
			substr(trim(pls_obter_end_pagador(a.nr_seq_pagador, 'CO')), 1, 36)			ds_complemento,
			substr(trim(pls_obter_end_pagador(a.nr_seq_pagador, 'B')), 1, 72)			ds_bairro,
			pls_obter_end_pagador(a.nr_seq_pagador, 'CI')						ds_municipio,
			substr(trim(pls_obter_end_pagador(a.nr_seq_pagador, 'UF')), 1, 2)			sg_estado,
			substr(somente_numero(pls_obter_end_pagador(a.nr_seq_pagador, 'CEP')), 1, 8)		cd_cep,
			b.nr_sequencia										nr_seq_pagador
		from	pls_contrato_pagador	b,
			pls_notificacao_pagador	a
		where	b.nr_sequencia		= a.nr_seq_pagador
		and	a.nr_seq_lote		= nr_seq_lote_p
		and	nr_seq_pagador_p 	is null
		union
		select	a.nr_sequencia,
			obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc)						nm_destinatario,
			pls_obter_end_pagador(a.nr_seq_pagador, 'E')						ds_endereco,
			pls_obter_end_pagador(a.nr_seq_pagador, 'NR')						nr_endereco,
			substr(trim(pls_obter_end_pagador(a.nr_seq_pagador, 'CO')), 1, 36)			ds_complemento,
			substr(trim(pls_obter_end_pagador(a.nr_seq_pagador, 'B')), 1, 72)			ds_bairro,
			pls_obter_end_pagador(a.nr_seq_pagador, 'CI')						ds_municipio,
			substr(trim(pls_obter_end_pagador(a.nr_seq_pagador, 'UF')), 1, 2)			sg_estado,
			substr(somente_numero(pls_obter_end_pagador(a.nr_seq_pagador, 'CEP')), 1, 8)		cd_cep,
			b.nr_sequencia										nr_seq_pagador
		from	pls_contrato_pagador	b,
			pls_notificacao_pagador	a
		where	b.nr_sequencia		= a.nr_seq_pagador
		and	a.nr_seq_lote		= nr_seq_lote_p
		and	a.nr_sequencia	 	= nr_seq_pagador_p;
	
begin
	-- Valida n�mero do lote
	if	(nr_seq_lote_p is null) then
		wheb_mensagem_pck.exibir_mensagem_abort('O N�mero do Lote de Notifica��o � obrigat�rio!');
	end if;
	
	-- Seleciona a matriz do e-Carta a partir da regra do lote de notifica��o
	open c01_w;
	fetch c01_w into 
		nr_seq_regra_w, 
		nr_seq_matriz_w, 
		nr_seq_ecarta_parametro_w,
		nr_seq_relatorio_w,
		ds_diretorio_relatorio_w;
	close c01_w;
	
	-- Verifica se o lote de notifica��o possui regra, e se a regra possui matriz do e-Carta
	if	(nr_seq_regra_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort('O Lote de Notifica��o (' || nr_seq_lote_p || ') n�o possui Regra para gera��o definida!');
	elsif	(nr_seq_matriz_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort('A Regra para gera��o (' || nr_seq_regra_w || ') do Lote de Notifica��o (' || nr_seq_lote_p || ') n�o possui Matriz e-Carta definida!');
	elsif	(nr_seq_ecarta_parametro_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort('A Matriz da Regra para gera��o (' || nr_seq_matriz_w || ') do Lote de Notifica��o (' || nr_seq_lote_p || ') n�o possui Par�metro e-Carta associado!');
	end if;

	-- Carrega pagadores
	open c02_w;
	loop fetch c02_w bulk collect into pls_pagador_t limit pls_util_pck.qt_registro_transacao_w;
		exit when pls_pagador_t.count = 0;
		begin
			-- Cria o lote de solicita��es
			select pls_ecarta_lote_seq.nextval into nr_seq_ecarta_lote_w from dual;
			insert into pls_ecarta_lote (
				nr_sequencia,
				nr_seq_ecarta_parametro,
				nr_seq_ecarta_matriz,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				cd_estabelecimento
			) values (
				nr_seq_ecarta_lote_w,		-- nr_sequencia
				nr_seq_ecarta_parametro_w,	-- nr_seq_ecarta_parametro
				nr_seq_matriz_w,		-- nr_seq_ecarta_matriz
				sysdate,			-- dt_atualizacao_nrec
				nm_usuario_p,			-- nm_usuario_nrec
				sysdate,			-- dt_atualizacao
				nm_usuario_p,			-- nm_usuario
				cd_estabelecimento_p
			);
			
			-- Processa pagadores
			for i in pls_pagador_t.first..pls_pagador_t.last loop
				
				--Verificar se o pagador j� possui e-Carta recebido
				select	count(1)
				into	qt_notificacao_recebida_w
				from	pls_ecarta_solicitacao a,
					pls_ecarta_lote b
				where	b.nr_sequencia	= a.nr_seq_ecarta_lote
				and	a.nr_seq_notific_pagador	= pls_pagador_t(i).nr_sequencia
				and	nvl(b.ie_cancelado,'N')	= 'N'
				and	(a.dt_recebimento_notificacao is not null or b.dt_envio is null);
				
				if	(qt_notificacao_recebida_w = 0) then
					-- Insere a solicita��o
					insert into pls_ecarta_solicitacao (
						nr_sequencia,
						nr_seq_ecarta_lote,
						nr_seq_notific_pagador,
						nm_destinatario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_atualizacao,
						nm_usuario
					) values (
						pls_ecarta_solicitacao_seq.nextval,	-- nr_sequencia
						nr_seq_ecarta_lote_w,			-- nr_seq_ecarta_lote
						pls_pagador_t(i).nr_sequencia,		-- nr_seq_notific_pagador
						pls_pagador_t(i).nm_destinatario,	-- nm_destinatario
						sysdate,				-- dt_atualizacao_nrec
						nm_usuario_p,				-- nm_usuario_nrec
						sysdate,				-- dt_atualizacao
						nm_usuario_p				-- nm_usuario
					) returning nr_sequencia into nr_seq_ecarta_solicitacao_w;
					
					-- Converte CEP em n�meros
					nr_cep_w := null;
					begin
						if	(pls_pagador_t(i).cd_cep is not null) then
							nr_cep_w := to_number(pls_pagador_t(i).cd_cep);
						end if;
					exception
						when others then
							nr_cep_w := null;
					end;
					
					if	(pls_pagador_t(i).ds_endereco is null) then
						wheb_mensagem_pck.exibir_mensagem_abort(1076496, 'DS_PAGADOR='|| pls_pagador_t(i).nr_seq_pagador || ' - ' || pls_pagador_t(i).nm_destinatario);
					elsif	(pls_pagador_t(i).nr_endereco is null) then
						wheb_mensagem_pck.exibir_mensagem_abort(1076497, 'DS_PAGADOR='|| pls_pagador_t(i).nr_seq_pagador || ' - ' || pls_pagador_t(i).nm_destinatario);
					elsif	(pls_pagador_t(i).ds_municipio is null) then
						wheb_mensagem_pck.exibir_mensagem_abort(1076498, 'DS_PAGADOR='|| pls_pagador_t(i).nr_seq_pagador || ' - ' || pls_pagador_t(i).nm_destinatario);
					elsif	(pls_pagador_t(i).sg_estado is null) then
						wheb_mensagem_pck.exibir_mensagem_abort(1076499, 'DS_PAGADOR='|| pls_pagador_t(i).nr_seq_pagador || ' - ' || pls_pagador_t(i).nm_destinatario);
					end if;
					
					-- Insere o endere�o (1 por solicita��o)
					insert into pls_ecarta_endereco_solic (
						nr_sequencia,
						nr_seq_ecarta_solicitacao,
						ds_endereco,
						nr_endereco,
						ds_complemento,
						ds_bairro,
						ds_municipio,
						sg_estado,
						cd_cep,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_atualizacao,
						nm_usuario
					) values (
						pls_ecarta_endereco_solic_seq.nextval,	-- nr_sequencia
						nr_seq_ecarta_solicitacao_w,		-- nr_seq_ecarta_solicitacao
						pls_pagador_t(i).ds_endereco,		-- ds_endereco
						pls_pagador_t(i).nr_endereco,		-- nr_endereco
						pls_pagador_t(i).ds_complemento,	-- ds_complemento
						pls_pagador_t(i).ds_bairro,		-- ds_bairro
						pls_pagador_t(i).ds_municipio,		-- ds_municipio
						pls_pagador_t(i).sg_estado,		-- sg_estado
						nr_cep_w,				-- cd_cep
						sysdate, 				-- dt_atualizacao_nrec
						nm_usuario_p,				-- nm_usuario_nrec
						sysdate,				-- dt_atualizacao
						nm_usuario_p				-- nm_usuario
					);
					
					
					--//TODO: Verificar como ser� o comportamento do IE_GERACAO_COMPLETA
					null;
					
					-- Insere o Arquivo da Solicita��o
					if	(nr_seq_relatorio_w is not null) then
						nm_arquivo_w	:= 'notificacao-' || pls_pagador_t(i).nr_sequencia;
						
						insert into pls_ecarta_arquivo_solic (
							nr_sequencia,
							nr_seq_ecarta_solicitacao,
							nm_arquivo,
							ds_caminho,
							qt_tamanho,
							qt_paginas,
							ie_aviso_recebimento,
							ie_geracao_completa,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							dt_atualizacao,
							nm_usuario
						) values (
							pls_ecarta_arquivo_solic_seq.nextval,			-- nr_sequencia
							nr_seq_ecarta_solicitacao_w,				-- nr_seq_ecarta_solicitacao
							nm_arquivo_w,						-- nm_arquivo
							ds_diretorio_relatorio_w,				-- ds_caminho -- Tem que ser salvo a partir do contexto do diret�rio da fun��o do e-Carta
							73768,							-- qt_tamanho
							4,							-- qt_paginas
							'N', 							-- ie_aviso_recebimento
							'S',							-- ie_geracao_completa
							sysdate, 						-- dt_atualizacao_nrec
							nm_usuario_p,						-- nm_usuario_nrec
							sysdate,						-- dt_atualizacao
							nm_usuario_p						-- nm_usuario
						) returning nr_sequencia into nr_seq_ecarta_arq_solic_w;
						
						select	cd_classif_relat,
							cd_relatorio
						into	cd_classif_relat_w,
							cd_relatorio_w
						from	relatorio
						where	nr_sequencia	= nr_seq_relatorio_w;
						
						select	max(ds_local_rede)
						into	ds_local_rede_w
						from	evento_tasy_utl_file
						where	cd_evento = 30;
						
						insert	into	pls_relatorio
							(	nr_sequencia,
								nr_seq_ecarta_arq_solic,
								cd_estabelecimento,
								cd_classif_relat,
								cd_relatorio,
								dt_atualizacao,
								dt_atualizacao_nrec,
								nm_usuario,
								nm_usuario_nrec,
								ie_origem,
								ie_status,
								ds_arquivo,
								ds_caminho
							)
							values (pls_relatorio_seq.nextval,
								nr_seq_ecarta_arq_solic_w,
								cd_estabelecimento_p,
								cd_classif_relat_w,
								cd_relatorio_w,
								sysdate,
								sysdate,
								nm_usuario_p,
								nm_usuario_p,
								'3',
								'1',
								nm_arquivo_w,
								ds_local_rede_w||ds_diretorio_relatorio_w
								) returning nr_sequencia into nr_seq_pls_relatorio_w;
								
						insert	into	pls_relat_param
							(	nr_sequencia,
								nr_seq_relat,
								ie_tipo_parametro,
								nm_parametro,
								ds_valor_parametro,
								dt_atualizacao,
								dt_atualizacao_nrec,
								nm_usuario,
								nm_usuario_nrec
								)
							values(	pls_relat_param_seq.nextval,
								nr_seq_pls_relatorio_w,
								'C',
								'NR_SEQ_PAGADOR',
								pls_pagador_t(i).nr_sequencia,
								sysdate,
								sysdate,
								nm_usuario_p,
								nm_usuario_p);
					end if;
				end if;
			end loop;
			
			-- Confirma altera��es da transa��o
			commit;
		exception
			when others then
				-- Defaz altera��es
				rollback;
				-- Levanta a excess�o ocorrida
				wheb_mensagem_pck.exibir_mensagem_abort(sqlerrm(sqlcode));
		end;
	end loop;
	close c02_w;
end gerar_lote;

end pls_ecarta_gerar_notific_pck;
/
