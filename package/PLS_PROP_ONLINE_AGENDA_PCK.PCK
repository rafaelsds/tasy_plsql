create or replace package pls_prop_online_agenda_pck as

type t_item_row is record
	(dt_agenda	date,
	ds_periodo	pls_solic_prop_on_ag_dia.ds_periodo%type);
type t_item is table of t_item_row;

function obter_se_feriado(	dt_referencia_p	date,
				cd_estabelecimento_p	pls_solic_prop_on_agenda.cd_estabelecimento%type)
				return varchar2;

function obter_horarios(	dt_entrada_p	date,
				cd_estabelecimento_p	pls_solic_prop_on_agenda.cd_estabelecimento%type)
				return t_item pipelined;
				

end pls_prop_online_agenda_pck;
/

create or replace package body pls_prop_online_agenda_pck as

	function obter_se_feriado(	dt_referencia_p	date,
					cd_estabelecimento_p	pls_solic_prop_on_agenda.cd_estabelecimento%type)
	 		    	return varchar2 is
	
	ie_feriado_w		varchar2(1);
	
	begin
	
	select	nvl(max('S'), 'N')
	into	ie_feriado_w
	from 	feriado
	where 	dt_feriado		= dt_referencia_p
	and	cd_estabelecimento	= cd_estabelecimento_p;
	
	return	ie_feriado_w;
	
	end obter_se_feriado;
	
	function obter_horarios(	dt_entrada_p	date,
					cd_estabelecimento_p	pls_solic_prop_on_agenda.cd_estabelecimento%type)
					return t_item pipelined is
					
	line_w				t_item_row;
	qt_agendas_w			pls_solic_prop_on_agenda.qt_agendas%type;
	qt_dias_intervalo_w		pls_solic_prop_on_agenda.qt_dias_intervalo%type;
	nr_dia_semana_w			pls_solic_prop_on_ag_dia.nr_dia_semana%type;
	ds_periodo_w			pls_solic_prop_on_ag_dia.ds_periodo%type;
	nr_seq_solic_prop_on_ag_w	pls_solic_prop_on_ag_dia.nr_seq_solic_prop_on_ag%type;
	qt_retorno_w			number;
	dt_agenda_w			date;
	
	begin
	qt_retorno_w := 0;
	
	select	nr_sequencia,
		qt_agendas,
		nvl(qt_dias_intervalo,0)
	into	nr_seq_solic_prop_on_ag_w,
		qt_agendas_w,
		qt_dias_intervalo_w
	from	pls_solic_prop_on_agenda
	where	cd_estabelecimento = cd_estabelecimento_p
	and	dt_entrada_p between dt_inicio_vigencia and nvl(dt_fim_vigencia, dt_entrada_p);
	
	dt_agenda_w	:= trunc(dt_entrada_p + qt_dias_intervalo_w,'dd');
	
	while (qt_retorno_w < qt_agendas_w) loop 
		begin
		if	(obter_se_feriado(dt_agenda_w, cd_estabelecimento_p) = 'N') then
			nr_dia_semana_w	:= to_char(dt_agenda_w,'D');
		end if;
		
		select	max(ds_periodo)
		into	ds_periodo_w
		from	pls_solic_prop_on_ag_dia
		where	nr_seq_solic_prop_on_ag = nr_seq_solic_prop_on_ag_w
		and	nr_dia_semana 		= nr_dia_semana_w;
		
		if	(ds_periodo_w is not null) then
			line_w.dt_agenda	:= dt_agenda_w;
			line_w.ds_periodo	:= ds_periodo_w;
			pipe row(line_w);
			qt_retorno_w := qt_retorno_w + 1;
		end if;
		
		dt_agenda_w	:= dt_agenda_w + 1;
		end;
	end loop;
	
	return;
	
	end obter_horarios;

end pls_prop_online_agenda_pck;
/