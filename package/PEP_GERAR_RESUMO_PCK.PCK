create or replace package PEP_Gerar_Resumo_PCK as
	
	procedure Gerar_Resumo(	cd_protocolo_p		number,
				nr_seq_medicacao_p	number,
				ie_rtf_html_p		varchar2,
				nm_usuario_p		Varchar2);
						
	procedure Gerar_PEP_Resumo_Medic;

	procedure Gerar_PEP_Resumo_Solucao;
	
	procedure Gerar_PEP_Resumo_Proc;
	
	procedure Gerar_PEP_Resumo_Recomend;
	
	procedure Gerar_PEP_Desc_protocolo(ie_rtf_html_p varchar2 default '');
	
	procedure Gerar_PEP_Desc_Subprotocolo(ie_rtf_html_p varchar2 default '');	

	function Get_TR_Html(	col1_p varchar2,
				col2_p varchar2,
				col3_p varchar2,
				col4_p varchar2,
				col5_p varchar2,
				col6_p varchar2,
				qt_col_p number) return varchar2;
	
	function Get_TR_Html_Justif( ds_justificativa_p varchar2) return varchar2;
	
	function Get_TR_Html_obs( ds_observacao_p varchar2) return varchar2;	
	
	Function Get_TR_Html_Desc_Protocolo( ds_desc_protocolo_p long) return long;
	
	function Get_TR_Html_Desc_Sub_Protocolo( ds_desc_sub_protocolo_p long) return long;
		
	function Get_TR_Rtf(	col1_p varchar2,
				col2_p varchar2,
				col3_p varchar2,
				col4_p varchar2,
				col5_p varchar2,
				col6_p varchar2,
				qt_col_p number) return varchar2;
	
end PEP_Gerar_Resumo_PCK;
/

create or replace package body PEP_Gerar_Resumo_PCK as
	cd_protocolo_w		Number(10);
	nr_seq_medicacao_w	Number(6);
	ds_resumo_w		long;
	ds_resumo_ww		long;
	ie_rtf_w		varchar2(1);
	nm_protocolo_w		varchar2(255);
	nm_medicacao_w		varchar2(255);
	nm_usuario_w		varchar2(255);
	
	nl_w			Varchar2(255);

	procedure Gerar_Resumo(
				cd_protocolo_p		number,
				nr_seq_medicacao_p	number,
				ie_rtf_html_p		varchar2,
				nm_usuario_p		Varchar2) is

	nr_sequencia_w			Number(10,0);
	ds_protocolo_w			Varchar2(255);
    ds_tipo_protocolo_w		Varchar2(255);
    ds_medicamento_w		Varchar2(255);
    ds_solucao_desc_w		Varchar2(255);
    ds_descricao_w			Varchar2(255);
    ds_dose_w				Varchar2(255);
    ds_unidade_w			Varchar2(255);
    ds_intervalo_desc_w		Varchar2(255);
    ds_procedimento_desc_w	Varchar2(255);
    ds_recomendacao_desc_w	Varchar2(255);
	ds_resumo_desc_w		Varchar2(255);	
	ds_via_adm_w			Varchar2(255);
			
	begin

	cd_protocolo_w		:= cd_protocolo_p;
	nr_seq_medicacao_w	:= nr_seq_medicacao_p;
	nm_usuario_w := nm_usuario_p;

	select	a.nm_protocolo,
		b.nm_medicacao
	into	nm_protocolo_w,
		nm_medicacao_w
	from	protocolo a,
		protocolo_medicacao b
	where	a.cd_protocolo = cd_protocolo_w
	and	b.cd_protocolo = a.cd_protocolo
	and	b.nr_sequencia = nr_seq_medicacao_w;


	Select substr(obter_desc_expressao(344742,'Descrição do protocolo'),1,255),
		   substr(obter_desc_expressao(695961,'Descrição do subtipo protocolo'),1,255),
		   substr(obter_desc_expressao(710353,'Medicamentos'),1,255),
		   substr(obter_desc_expressao(298694,'Soluções'),1,255),
		   substr(obter_desc_expressao(291705,'Descrição'),1,255),
		   substr(obter_desc_expressao(288220,'Dose'),1,255),
		   substr(obter_desc_expressao(300775,'Unidade de medida'),1,255),
		   substr(obter_desc_expressao(292190,'Intervalo'),1,255),
		   substr(obter_desc_expressao(305831,'Procedimentos / Exames'),1,255),
		   substr(obter_desc_expressao(297348,'Recomendações'),1,255),
		   substr(obter_desc_expressao(328907,'Resumo protocolo'),1,255),
		   substr(obter_desc_expressao(301661,'Via'),1,255)
	into   ds_protocolo_w,
		   ds_tipo_protocolo_w,
		   ds_medicamento_w,
		   ds_solucao_desc_w,
		   ds_descricao_w,
		   ds_dose_w,
		   ds_unidade_w,
		   ds_intervalo_desc_w,
		   ds_procedimento_desc_w,
		   ds_recomendacao_desc_w,
		   ds_resumo_desc_w,
		   ds_via_adm_w
	from dual;
	


	if	(ie_rtf_html_p = 'RTF') or (ie_rtf_html_p = 'HTML5') then
		begin
		ie_rtf_w := 'N'; 
		nl_w := chr(13) || chr(10);
		end;
	else
		begin
		ie_rtf_w := 'S';
		nl_w := '<br>';
		end;
	end if;
	ds_resumo_ww	:= '<html>
		
				<head>
					<title>'||nm_protocolo_w||' / '|| nm_medicacao_w||'</title>
					<style>
					body {
						font-family: tahoma, arial, sans-serif;
						margin: 0;
						padding: 28px;
					}
					td {
						font-size: 1.3rem;
						padding: 8px 16px;
						width: auto;
					}
					h1 {
						border-bottom: 1px solid;
						padding-bottom: 16px;
					}
					html {
						font-size: 11px;
					}
					</style>
				</head>
				<body data-gr-c-s-loaded="true">
					<h1> '||ds_resumo_desc_w||' ('||nm_protocolo_w||' / '|| nm_medicacao_w||') </h1> <br>					
					
					<table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" >';

	delete	protocolo_medic_resumo
	where	cd_protocolo = cd_protocolo_w
	and	nr_seq_medicacao = nr_seq_medicacao_w
	and	nm_usuario	= nm_usuario_p;
	
	Gerar_PEP_Desc_protocolo(ie_rtf_html_p);	
	
	
	if	(ds_resumo_w is not null) then
		ds_resumo_ww	:= substr(ds_resumo_ww || 
			'	<tr>
					<td colspan="6">
						<b>'||ds_protocolo_w||'</b>
					</td>
				</tr>' || nl_w || substr(ds_resumo_w,1,30000)  || nl_w,1,30000);
	end if;	
	
	Gerar_PEP_Desc_Subprotocolo(ie_rtf_html_p);
	
	if	(ds_resumo_w is not null) then
		ds_resumo_ww	:= substr(ds_resumo_ww || 
			'	<tr>
					<td colspan="6">
						<b>'||ds_tipo_protocolo_w||'</b>
					</td>
				</tr>' || nl_w || substr(ds_resumo_w,1,30000)  || nl_w,1,30000);
	end if;
	
	
	Gerar_PEP_Resumo_Medic;

	if	(ds_resumo_w is not null) then
		ds_resumo_ww	:= substr(ds_resumo_ww || 
			'	<tr>
					<td colspan="6">
						<b>'||ds_medicamento_w||'</b>
					</td>
				</tr>
				<tr bgcolor="#E6E6E6">
					<td width="35%">
						'||ds_descricao_w||'
					</td>
					<td width="5%">
						'||ds_dose_w||'
					</td>
					<td width="5%">
						'||ds_unidade_w||'
					</td width="5%">
					<td width="5%">
						'||ds_via_adm_w||'
					</td>
					<td>
						'||ds_intervalo_desc_w||'
					</td width="5%">
					<td>					
					</td width="5%">	
				</tr> ' || nl_w || ds_resumo_w  || nl_w,1,30000);
	end if;

	Gerar_PEP_Resumo_Solucao;

	if	(ds_resumo_w is not null) then
		ds_resumo_ww	:= substr(ds_resumo_ww ||
			'	<tr>
					<td colspan="6">
						<b>'||ds_solucao_desc_w||'</b>
					</td>
				</tr> 
				<tr bgcolor="#E6E6E6">
					<td width="70%" colspan="5">
						'||ds_descricao_w||'
					</td>				
				</tr> ' || nl_w || ds_resumo_w  || nl_w,1,30000);
	end if;
	
	Gerar_PEP_Resumo_Proc;
	
	if	(ds_resumo_w is not null) then
		ds_resumo_ww	:= substr(ds_resumo_ww ||
			'	<tr>
					<td colspan="6">
						<b>'||ds_procedimento_desc_w||'</b>
					</td>
				</tr>
				<tr bgcolor="#E6E6E6">
					<td width="35%">
						'||ds_descricao_w||'
					</td>
					<td width="5%">
						'|| ds_intervalo_desc_w ||'
					</td>
					<td width="5%">
						'|| '' ||'
					</td width="5%">
					<td width="5%">
						'|| '' ||'
					</td>
					<td>
						'|| '' ||'
					</td width="5%">
					<td>					
					</td width="5%">	
				</tr> ' || nl_w || ds_resumo_w  || nl_w,1,30000);
	end if;
	
	Gerar_PEP_Resumo_Recomend;
	
	if	(ds_resumo_w is not null) then
		ds_resumo_ww	:= substr(ds_resumo_ww ||
			'	<tr>
					<td colspan="6">
						<b>'||ds_recomendacao_desc_w||'</b>
					</td>
				</tr> 
				<tr bgcolor="#E6E6E6">
					<td width="70%" colspan="5">
						'||ds_descricao_w||'
					</td>					
				</tr> ' || nl_w || ds_resumo_w  || nl_w,1,30000);
	end if;	
	
	ds_resumo_ww	:= substr(ds_resumo_ww ||
		'				</table> '||
		'		</body>'||
		'</html>',1,30000);

	if	(ds_resumo_ww is not null) then
		select	protocolo_medic_resumo_seq.nextval
		into	nr_sequencia_w
		
		from	dual;
		insert into protocolo_medic_resumo(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_protocolo,
			nr_seq_medicacao,
			ds_resumo)
		values	(nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_protocolo_w,
			nr_seq_medicacao_w,
			ds_resumo_ww);
	end if;
	
	
	end Gerar_Resumo;
	
	procedure Gerar_PEP_Resumo_Medic is

	ds_material_w			varchar2(400);
	qt_dose_w			number(15,4);
	ds_intervalo_w			Varchar2(80);
	ds_unidade_medida_w		Varchar2(80);
	ds_unid_med_w			Varchar2(50);
	ds_diluicao_w			Varchar2(255);
	ie_suspenso_w			varchar2(1);
	ds_horarios_w			varchar2(2000);
	ds_observacao_w			varchar2(255);
	ds_justificativa_w		varchar2(255);
	ds_via_adm_w			varchar2(255);

	cursor C01 is
	select	substr(nvl(c.ds_reduzida, c.ds_material),1,255),
		a.qt_dose,
		substr(obter_desc_unid_med(a.cd_unidade_medida),1,60),
		b.ds_intervalo || decode(a.ie_se_necessario,'S',' - SN', decode(a.ie_acm,'S',' - ACM')),
		substr(a.ds_recomendacao,1,255),
		substr(a.ds_justificativa,1,255),
		substr(obter_via_aplicacao(a.ie_via_aplicacao,'D'),1,255)
	from	intervalo_prescricao b,
		material c,
		protocolo_medic_material a
	where	a.cd_intervalo	= b.cd_intervalo (+)
	and	a.cd_material	= c.cd_material
	and	a.cd_protocolo	= cd_protocolo_w
	and	a.nr_sequencia 	= nr_seq_medicacao_w
	and	a.ie_agrupador	= 1;

	begin
	ds_resumo_w		:= '';
	
	open C01;
	loop
	fetch C01 into	
		ds_material_w,
		qt_dose_w,
		ds_unidade_medida_w,
		ds_intervalo_w,
		ds_observacao_w,
		ds_justificativa_w,
		ds_via_adm_w;
	exit when C01%notfound;
		begin

		if	(ie_rtf_w = 'N') then
			ds_resumo_w	:= substr(ds_resumo_w || 
					Get_TR_Html(ds_material_w, to_char(qt_dose_w) , ds_unidade_medida_w, ds_via_adm_w, ds_intervalo_w,null,6),1,30000);
							
			if	(ds_observacao_w is not null) then
				ds_resumo_w := substr(ds_resumo_w || get_tr_html_obs(ds_observacao_w),1,30000);
			end if;
			
			if	(ds_justificativa_w is not null) then
				ds_resumo_w := substr(ds_resumo_w || get_tr_html_justif(ds_justificativa_w),1,30000);
			end if;				
							
		else
			ds_resumo_w	:= substr(ds_resumo_w || 
					Get_TR_Rtf(ds_material_w, to_char(qt_dose_w) , ds_unidade_medida_w, ds_intervalo_w,null,null,6),1,30000);
		end if;
				
		end;
	end loop;
	close C01;
	
	end Gerar_PEP_Resumo_Medic;
	
	procedure Gerar_PEP_Resumo_Solucao is
	
	ds_solucao_w		varchar2(255);
	nr_seq_solucao_w	number(6);
	ds_material_w		varchar2(400);
	ds_unidade_medida_w	varchar2(30);
	qt_dose_w		number(15,4);
	ds_componentes_w	varchar2(2000);
	ie_susp_sol_w		varchar2(1);
	ie_susp_comp_w		varchar2(1);
	ds_horarios_w		varchar2(2000);
	ds_justificativa_w	varchar2(255);
	ds_observacao_w		varchar2(255);
	
	cursor C01 is
	select	Obter_Desc_Vol_Sol_Protocolo(cd_protocolo, nr_sequencia, nr_seq_solucao),
		nr_seq_solucao
	from	protocolo_medic_solucao
	where	cd_protocolo	= cd_protocolo_w
	and	nr_sequencia 	= nr_seq_medicacao_w
	and	nvl(ie_hemodialise,'N') = 'N';
	
	cursor C02 is
	select	b.ds_material,
		a.cd_unidade_medida,
		a.qt_dose,
		substr(a.ds_recomendacao,1,255),
		substr(a.ds_justificativa,1,255)
	from	material b,
		PROTOCOLO_MEDIC_MATERIAL a
	where	a.cd_material		= b.cd_material
	and	a.cd_protocolo		= cd_protocolo_w
	and	a.nr_sequencia 		= nr_seq_medicacao_w
	and	a.nr_seq_solucao	= nr_seq_solucao_w;
	
	begin
	ds_resumo_w := '';
	
	open C01;
	loop
	fetch C01 into	
		ds_solucao_w,
		nr_seq_solucao_w;
	exit when C01%notfound;
		begin
		
		if	(ie_rtf_w = 'N') then
			ds_componentes_w := '<table align="center" border="0" cellpadding="5" cellspacing="0" width="100%" >';
		end if;
		
		open C02;
		loop
		fetch C02 into	
			ds_material_w,
			ds_unidade_medida_w,
			qt_dose_w,
			ds_observacao_w,
			ds_justificativa_w;
		exit when C02%notfound;
			begin
			if	(ie_rtf_w = 'N') then
				ds_componentes_w	:= substr(ds_componentes_w || 
							Get_TR_Html(qt_dose_w||' '|| ds_unidade_medida_W || ' - ' ||ds_material_w,
								null,null,null,null,null,3),1,30000);	
								
				if	(ds_observacao_w is not null) then
					ds_componentes_w := substr(ds_componentes_w || get_tr_html_obs(ds_observacao_w),1,30000);
				end if;
			
				if	(ds_justificativa_w is not null) then
					ds_componentes_w := substr(ds_componentes_w || get_tr_html_justif(ds_justificativa_w),1,30000);
				end if;
				
			else
				ds_componentes_w	:= substr(ds_componentes_w || 
							Get_TR_Rtf(qt_dose_w||' '|| ds_unidade_medida_W || ' - ' ||ds_material_w,
								    null,null,null,null,null,3),1,30000);	
			end if;

			end;
		end loop;
		close C02;
		
		if	(ie_rtf_w = 'N') then		
			ds_componentes_w := ds_componentes_w||'</table>';
		

			ds_resumo_w	:= substr(ds_resumo_w || 
					Get_TR_Html(ds_solucao_w,ds_horarios_w,null,null,null,null, 2)||
					Get_TR_Html(ds_componentes_w,null,null,null,null,null,1),1,30000);
		else
			ds_resumo_w	:= substr(ds_resumo_w || 
					Get_TR_Rtf(ds_solucao_w,ds_horarios_w,null,null,null,null, 2)||
					Get_TR_Rtf(ds_componentes_w,null,null,null,null,null,1),1,30000);
		end if;
		end;
	end loop;
	close C01;
	
	end Gerar_PEP_Resumo_Solucao;
	
	
	procedure Gerar_PEP_Resumo_Proc is

	ds_procedimento_w		varchar2(200);
	ds_observacao_w			varchar2(255);
	ds_justificativa_w		varchar2(255);
	ds_procedimento_ww		varchar2(100);
	nr_seq_exame_w			number(10);
	ds_exame_w	 			varchar2(100);
	cd_intervalo_exame_w	varchar2(10);
	ds_intervalo_exame_w	varchar2(100);

	cursor C01 is
	select	substr(Obter_Desc_Prescr_Proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,80),
		substr(a.ds_observacao,1,255),
		substr(a.ds_justificativa,1,255),
		a.nr_seq_exame,
		a.cd_intervalo
	from	protocolo_medic_proc a
	where	a.cd_protocolo		= cd_protocolo_w
	and	a.nr_sequencia 		= nr_seq_medicacao_w
	and 	nvl(ie_anatomia,'N') = 'N'
	and 	((nr_seq_proc_interno is null) or (Obter_tipo_proc_interno(nr_seq_proc_interno) = 'O'));
	
	begin
	ds_resumo_w		:= '';
	open C01;
	loop
	fetch C01 into	
		ds_procedimento_ww,
		ds_observacao_w,
		ds_justificativa_w,
		nr_seq_exame_w,
		cd_intervalo_exame_w;
	exit when C01%notfound;
		begin
		
		if (cd_intervalo_exame_w is not null) then
			select substr (max(ds_intervalo),1,80) 
			into ds_intervalo_exame_w
			from intervalo_prescricao 
			where cd_intervalo = cd_intervalo_exame_w;
		end if;
		
		
		if (nr_seq_exame_w > 0) then		
			select substr (max(nm_exame),1,80) 
			into ds_exame_w
			from exame_laboratorio 
			where nr_seq_exame = nr_seq_exame_w;
		
			if (ds_procedimento_ww = ' ') then
				ds_procedimento_w := ds_exame_w;
			else
				ds_procedimento_w := ds_procedimento_ww || ' / ' || ds_exame_w;
			end if;
		else
			ds_procedimento_w := ds_procedimento_ww;
		end if;
		
		if	(ie_rtf_w = 'N') then
			ds_resumo_w	:= substr(ds_resumo_w || 
					Get_TR_Html(ds_procedimento_w, ds_intervalo_exame_w, null, 
							null, null, null,6),1,30000);
							
			if	(ds_observacao_w is not null) then
				ds_resumo_w := substr(ds_resumo_w || get_tr_html_obs(ds_observacao_w),1,30000);
			end if;
			
			if	(ds_justificativa_w is not null) then
				ds_resumo_w := substr(ds_resumo_w || get_tr_html_justif(ds_justificativa_w),1,30000);
			end if;				
							
		else
			ds_resumo_w	:= substr(ds_resumo_w || 
					Get_TR_Rtf(ds_procedimento_w, ds_intervalo_exame_w, null, 
							null, null, null,6),1,30000);

		end if;
		
		end;
	end loop;
	close C01;
	end Gerar_PEP_Resumo_Proc;
	
	procedure Gerar_PEP_Resumo_Recomend is
	
	ds_tipo_recomendacao_w		varchar2(255);
	ds_observacao_w			varchar2(255);
	
	Cursor C01 is
	select	substr(nvl(b.ds_tipo_recomendacao,ds_recomendacao),1,255),
		substr(a.ds_recomendacao,1,255)
	from	tipo_recomendacao b,
		protocolo_medic_rec a
	where	a.cd_recomendacao	= b.cd_tipo_recomendacao(+)
	and	a.cd_protocolo		= cd_protocolo_w
	and	a.nr_sequencia 		= nr_seq_medicacao_w;
	
	begin
	
	ds_resumo_w	:= '';
	open C01;
	loop
	fetch C01 into	
		ds_tipo_recomendacao_w,
		ds_observacao_w;
	exit when C01%notfound;
		begin
		if	(ie_rtf_w = 'N') then
		
			ds_resumo_w	:= substr(ds_resumo_w || 
					Get_TR_Html(ds_tipo_recomendacao_w, null, null, 
							null, null, null,2),1,30000);
							
			if	(ds_observacao_w is not null) then
				ds_resumo_w := substr(ds_resumo_w || get_tr_html_obs(ds_observacao_w),1,30000);
			end if;
							
		else
			ds_resumo_w	:= substr(ds_resumo_w || 
					Get_TR_Rtf(ds_tipo_recomendacao_w, null, null, 
							null, null, null,2),1,30000);

		end if;
		
		end;
	end loop;
	close C01;
	
	end Gerar_PEP_Resumo_Recomend;
	
	procedure Gerar_PEP_Desc_protocolo(ie_rtf_html_p varchar2 default '') is

	ds_desc_protocolo_w		long;
	nr_seq_conversao_w		NUmber;
	qt_reg_w				NUmber;

	begin
	
	ds_resumo_w		:= '';

	Select count(*)
	into   qt_reg_w
	from   protocolo 
	where  cd_protocolo	= cd_protocolo_w
	and	   ds_protocolo is not null;
	
	if	(qt_reg_w > 0) then
	






		qt_reg_w := 0;
	
		if (ie_rtf_html_p = 'HTML5') then
			Select ds_protocolo 
			into   ds_desc_protocolo_w 
			from   protocolo 
			where  cd_protocolo = cd_protocolo_w;
			
			qt_reg_w := DBMS_LOB.INSTR(ds_desc_protocolo_w,'<html');
		end if;
		
		if (qt_reg_w <= 0) then
			CONVERTE_RTF_HTML('Select ds_protocolo from protocolo where cd_protocolo = :nr', cd_protocolo_w, nm_usuario_w, nr_seq_conversao_w);	
		
			select  ds_texto
			into	ds_desc_protocolo_w
			from 	TASY_CONVERSAO_RTF
			where	nr_sequencia = nr_seq_conversao_w;
		end if;
		
		if	( ds_desc_protocolo_w is not null ) then
		
			if	(ie_rtf_w = 'N') then		
				ds_resumo_w := substr(Get_TR_Html_Desc_Protocolo(ds_desc_protocolo_w),1,30000);									
			else
				ds_resumo_w	:= substr(Get_TR_Rtf(substr(ds_desc_protocolo_w,1,30000), null, null, null, null, null,1),1,30000);							
			end if;
					
		end if;
		
	end if;
	
	end Gerar_PEP_Desc_protocolo;

	procedure Gerar_PEP_Desc_Subprotocolo(ie_rtf_html_p varchar2 default '') is

	ds_desc_sub_protocolo_w			long;
	nr_seq_desc_sub_tipo_w		number;
	nr_seq_conversao_w		NUmber;	
	qt_reg_w				NUmber;

	begin
	
	ds_resumo_w		:= '';	
	
	Select  nvl(max(b.nr_sequencia),0)
	into	nr_seq_desc_sub_tipo_w
	from	protocolo a,			
			desc_subtipo_protocolo b,
			protocolo_medicacao c
	where	a.cd_protocolo = cd_protocolo_w
	and		c.cd_protocolo = a.cd_protocolo
	and		c.nr_seq_interna = b.nr_seq_protocolo
	and		c.nr_sequencia = nr_seq_medicacao_w;
	
	if	(nr_seq_desc_sub_tipo_w > 0) then
	
		qt_reg_w := 0;
	
		if (ie_rtf_html_p = 'HTML5') then
			Select  ds_subtipo_prot 
			into    ds_desc_sub_protocolo_w 
			from 	desc_subtipo_protocolo 
			where 	nr_sequencia = nr_seq_desc_sub_tipo_w;
			
			qt_reg_w := DBMS_LOB.INSTR(ds_desc_sub_protocolo_w,'<html');
		end if;
		
		if (qt_reg_w <= 0) then	
			CONVERTE_RTF_HTML('Select DS_SUBTIPO_PROT from desc_subtipo_protocolo where nr_sequencia = :nr', nr_seq_desc_sub_tipo_w, nm_usuario_w, nr_seq_conversao_w);
			
			select  ds_texto
			into	ds_desc_sub_protocolo_w
			from 	TASY_CONVERSAO_RTF
			where	nr_sequencia = nr_seq_conversao_w;






		end if;
				
		if	( ds_desc_sub_protocolo_w is not null ) then
		
			if	(ie_rtf_w = 'N') then		
				ds_resumo_w := substr(Get_TR_Html_Desc_Sub_Protocolo(ds_desc_sub_protocolo_w),1,30000);									
			else
				ds_resumo_w	:= substr(Get_TR_Rtf(substr(ds_desc_sub_protocolo_w,1,30000), null, null, null, null, null,1),1,30000);							
			end if;
					
		end if;
	
	end if;
	
	end Gerar_PEP_Desc_Subprotocolo;	
	
	function Get_TR_Html_Justif( ds_justificativa_p varchar2) 
				return varchar2 as
	
	ds_retorno_w		varchar2(2000);
	ds_justificava_w	varchar2(255);
					
	begin
	
	Select 	substr(obter_desc_expressao(292337,'Justificativa'),1,255)
	into    ds_justificava_w    
	from 	dual;
	
	
		ds_retorno_w :=	'<tr valign="topmargin">' || chr(13) ||
				'<td colspan="6" style="font-size:13px">' || '   '||ds_justificava_w||': ' || ds_justificativa_p || '</td>' || chr(13) ||
				'</tr>';
			
	return ds_retorno_w;
	end;
	
	function Get_TR_Html_obs( ds_observacao_p varchar2) 
				return varchar2 as
	
	ds_retorno_w		varchar2(2000);
	ds_observacao_w		varchar2(255);
					
	begin
	
	Select 	substr(obter_desc_expressao(753406,'Observação'),1,255)
	into    ds_observacao_w    
	from 	dual;
	
		ds_retorno_w :=	'<tr valign="topmargin">' || chr(13) ||
				'<td colspan="6" style="font-size:13px">' || '   '||ds_observacao_w ||': ' || ds_observacao_p || '</td>' || chr(13) ||
				'</tr>';
			
	return ds_retorno_w;
	end;
	
	function Get_TR_Html_Desc_Protocolo( ds_desc_protocolo_p long) 
				return long as
	
	ds_retorno_w	long;
					
	begin
	
		ds_retorno_w :=	'<tr valign="topmargin">' || chr(13) ||
				'<td colspan="6" style="font-size:13px">' || ds_desc_protocolo_p || '</td>' || chr(13) ||
				'</tr>';
			
	return ds_retorno_w;
	end;
	
	function Get_TR_Html_Desc_Sub_Protocolo( ds_desc_sub_protocolo_p long) 
				return long as
	
	ds_retorno_w	long;
					
	begin
	
		ds_retorno_w :=	'<tr valign="topmargin">' || chr(13) ||
				'<td colspan="6" style="font-size:13px">' || ds_desc_sub_protocolo_p || '</td>' || chr(13) ||
				'</tr>';
			
	return ds_retorno_w;
	end;
	
	function Get_TR_Html(	col1_p varchar2,
				col2_p varchar2,
				col3_p varchar2,
				col4_p varchar2,
				col5_p varchar2,
				col6_p varchar2,
				qt_col_p number) return varchar2 as
	ds_retorno_w 	varchar2(20000);
	begin
	
	
	ds_retorno_w := ' <tr valign="topmargin"> ' || chr(13) ||
		'	<td colspan="'|| (7 - qt_col_p) ||'">' || chr(13) || 
						nvl(col1_p,'&' || 'nbsp;') || chr(13) ||
				'	</td>'|| chr(13) ;
		if (qt_col_p >= 2) then
			ds_retorno_w  := ds_retorno_w ||
				'	<td>' || chr(13) ||
						 nvl(col2_p,'&' || 'nbsp;') || chr(13) ||
				'	</td>'|| chr(13);
		end if;
		if (qt_col_p >= 3) then
			ds_retorno_w  := ds_retorno_w ||
			'	<td>' || chr(13) ||
					 nvl(col3_p,'&' || 'nbsp;') || chr(13) ||
			'	</td>'|| chr(13) ;
		end if;
		if (qt_col_p >= 4) then
			ds_retorno_w  := ds_retorno_w ||
			'	<td>' || chr(13) ||
					 nvl(col4_p,'&' || 'nbsp;') || chr(13) ||
			'	</td>'|| chr(13) ;
		end if;
		if (qt_col_p >= 5) then
			ds_retorno_w  := ds_retorno_w ||
			'	<td>' || chr(13) ||
					 nvl(col5_p,'&' || 'nbsp;') || chr(13) ||
			'	</td>'|| chr(13) ;
		end if;
		if (qt_col_p >= 6) then
			ds_retorno_w  := ds_retorno_w ||
			'	<td>' || chr(13) ||
					 nvl(replace(col6_P, chr(13), '<br>' || chr(13)),'&' || 'nbsp;') ||
			'	</td>'|| chr(13);
		end if;
		ds_retorno_w  := ds_retorno_w || '</tr>' || chr(13);
	return ds_retorno_w;
	end;
	
	function Get_TR_Rtf(	col1_p varchar2,
				col2_p varchar2,
				col3_p varchar2,
				col4_p varchar2,
				col5_p varchar2,
				col6_p varchar2,
				qt_col_p number) return varchar2 as
	ds_retorno_w 	varchar2(30000);
	ds_cor_susp_w	varchar2(255);
	begin
		
	ds_retorno_w := '   ';
	
	if (qt_col_p >= 2) then
		ds_retorno_w  := ds_retorno_w || nvl(col2_p,'  ') || '  ';
	end if;
	if (qt_col_p >= 3) then
		ds_retorno_w  := ds_retorno_w || ' ' || nvl(col3_p,'    ') || '    ';
	end if;
	ds_retorno_w  := ds_retorno_w || nvl(col1_p,'    ') || chr(13);
	if (qt_col_p >= 4) then
		ds_retorno_w  := ds_retorno_w || '     ' || nvl(col4_p,'    ') || '    ';
	end if;
	if (qt_col_p >= 5) then
		ds_retorno_w  := ds_retorno_w || '	' || nvl(col5_p,'    ') || '    ';
	end if;
	if (qt_col_p >= 6) then
		ds_retorno_w  := ds_retorno_w || '	' || nvl(col6_p,'    ') || '    ' || chr(13);
	end if;
	
	ds_retorno_w  := ds_retorno_w;
	
	return	ds_retorno_w;
	
	end;
	
end PEP_Gerar_Resumo_PCK;
/
