create or replace
package pls_segurado_preco_pck as

type t_preco_beneficiario_row is record (
	nr_sequencia			number,
	nr_seq_segurado_valor		number,
	dt_preco			date,
	dt_alteracao			date,
	dt_inativacao			date,
	qt_idade			number,
	vl_preco			number,
	vl_sem_desconto			number,
	vl_preco_nao_subsidiado		number,
	vl_adaptacao			number,
	vl_reajuste_indice		number,
	vl_reajuste_fx_etaria		number,
	vl_minimo			number,
	ie_motivo_alteracao		varchar2(10),
	nr_seq_reajuste_preco		number,
	nr_seq_plano			number,
	nr_seq_tabela			number,
	nr_seq_plano_preco		number,
	ie_tipo_titularidade		varchar2(3),
	ie_tabela_qt_vidas		varchar2(1),
	qt_vidas			number,
	nr_seq_processo_judicial	number,
	ie_permite_reaj_fx_etaria	varchar2(1),
	ie_permite_reaj_indice		varchar2(1),
	nr_seq_regra_desconto		number,
	tx_desconto			number,
	vl_desconto			number,
	nr_seq_regra_apropriacao	number,
	ie_situacao			varchar2(1));

type t_preco_beneficiario is table of t_preco_beneficiario_row;

qt_registro_transacao_w		pls_integer := 2000;

function obter_preco_segurado(	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
				dt_referencia_p		date,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type)
				return t_preco_beneficiario pipelined;

end pls_segurado_preco_pck;
/

create or replace
package body pls_segurado_preco_pck as

pls_parametros_w		pls_parametros%rowtype;

procedure carregar_parametros(	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
begin

select	*
into	pls_parametros_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

end carregar_parametros;

function obter_se_permite_reaj(	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
				dt_preco_p		date,
				qt_idade_limite_reaj_p	pls_segurado.qt_idade_limite_reaj%type,
				qt_anos_limite_reaj_p	pls_segurado.qt_anos_limite_reaj%type,
				qt_idade_p		pls_integer,
				ie_regulamentacao_p	pls_plano.ie_regulamentacao%type,
				dt_contratacao_p	pls_segurado.dt_contratacao%type,
				dt_inclusao_operadora_p	pls_segurado.dt_inclusao_operadora%type)
				return varchar2 is
qt_anos_operadora_w	pls_integer;
dt_base_inclusao_w	date;
nr_seq_mtvo_alteracao_w	pls_motivo_alteracao_plano.nr_sequencia%type;
dt_adaptacao_w		date;
begin
if	(qt_idade_limite_reaj_p is not null) or (qt_anos_limite_reaj_p is not null) then
	if	(qt_idade_p >= nvl(qt_idade_limite_reaj_p,qt_idade_p)) then
		if	(ie_regulamentacao_p = 'A') then
			if	(pls_parametros_w.ie_data_ref_reaj_adaptado = 'A') then
				dt_base_inclusao_w	:= dt_contratacao_p;
			elsif	(pls_parametros_w.ie_data_ref_reaj_adaptado = 'D') then
				select	max(nr_sequencia)
				into	nr_seq_mtvo_alteracao_w
				from	pls_motivo_alteracao_plano
				where	cd_ans	= '12';
				
				select	max(dt_alteracao)
				into	dt_adaptacao_w
				from	pls_segurado_alt_plano
				where	nr_seq_segurado		= nr_seq_segurado_p
				and	ie_situacao 		= 'A'
				and	nr_seq_motivo_alt	= nr_seq_mtvo_alteracao_w;
				
				dt_base_inclusao_w	:= nvl(dt_adaptacao_w,dt_inclusao_operadora_p);
			else
				dt_base_inclusao_w	:= dt_inclusao_operadora_p;
			end if;
		else
			dt_base_inclusao_w	:= dt_inclusao_operadora_p;
		end if;
		
		qt_anos_operadora_w	:= trunc(months_between(dt_preco_p, dt_base_inclusao_w) / 12);
		
		if	(qt_anos_operadora_w >= nvl(qt_anos_limite_reaj_p,qt_anos_operadora_w)) then
			return 'N';
		else
			return 'S';
		end if;
	else
		return 'S';
	end if;
else
	return 'S';
end if;

end obter_se_permite_reaj;

function obter_qt_meses_niver(	ie_mes_cobranca_reaj_p	pls_contrato.ie_mes_cobranca_reaj%type,
				ie_regulamentacao_p	pls_plano.ie_regulamentacao%type)
				return number is
ie_mes_cobranca_reaj_w		pls_contrato.ie_mes_cobranca_reaj%type;
begin
ie_mes_cobranca_reaj_w	:= nvl(ie_mes_cobranca_reaj_p,'R');

if	(ie_mes_cobranca_reaj_w = 'R') then
	if	(ie_regulamentacao_p = 'R') then
		ie_mes_cobranca_reaj_w	:= nvl(pls_parametros_w.ie_mes_cobranca_reaj,'M');
	else
		ie_mes_cobranca_reaj_w	:= nvl(pls_parametros_w.ie_mes_cobranca_reaj_reg,'M');
	end if;
end if;

if	(ie_mes_cobranca_reaj_w = 'P') then
	return 1;
else
	return 0;
end if;

end obter_qt_meses_niver;

procedure obter_valor(	nr_seq_tabela_p		in	pls_segurado_valor.nr_seq_tabela%type,
			dt_preco_p		in	pls_segurado_valor.dt_preco%type,
			qt_idade_p		in	number,
			ie_tipo_parentesco_p	in	pls_segurado_valor.ie_tipo_parentesco%type,
			ie_tabela_qt_vidas_p	in	pls_segurado_valor.ie_tabela_qt_vidas%type,
			qt_vidas_p		in	pls_segurado_valor.qt_vidas%type,
			nr_seq_plano_preco_p	out	pls_plano_preco.nr_sequencia%type,
			vl_preco_atual_p	out	pls_plano_preco.vl_preco_atual%type,
			vl_preco_nao_subsid_p	out	pls_plano_preco.vl_preco_nao_subsidiado%type,
			vl_adaptacao_p		out	pls_plano_preco.vl_adaptacao%type,
			vl_minimo_p		out	pls_plano_preco.vl_minimo%type,
			tx_acrescimo_p		out	pls_plano_preco.tx_acrescimo%type) is
			

nr_seq_plano_preco_w		pls_plano_preco.nr_sequencia%type;
tx_acrescimo_w			pls_plano_preco.tx_acrescimo%type;
vl_preco_atual_w		pls_plano_preco.vl_preco_atual%type;
vl_preco_nao_subsidiado_w	pls_plano_preco.vl_preco_nao_subsidiado%type;
vl_adaptacao_w			pls_plano_preco.vl_adaptacao%type;
vl_minimo_w			pls_plano_preco.vl_minimo%type;

Cursor C01 (	nr_seq_tabela_pc	pls_segurado_valor.nr_seq_tabela%type,
		qt_idade_pc		number,
		ie_tipo_parentesco_pc	grau_parentesco.ie_tipo_parentesco%type) is
	select	a.nr_sequencia nr_seq_plano_preco,
		nvl(a.vl_preco_atual,0) vl_preco_atual,
		nvl(a.vl_preco_nao_subsid_atual,0) vl_preco_nao_subsidiado,
		nvl(a.vl_minimo,0) vl_minimo,
		nvl(a.vl_adaptacao,0) vl_adaptacao,
		nvl(a.tx_acrescimo,0) tx_acrescimo,
		(	select	min(x.nr_sequencia)
			from	pls_reajuste_preco x
			where	x.nr_seq_preco = a.nr_sequencia
			and	x.dt_reajuste > dt_preco_p) nr_seq_reajuste_preco
	from	pls_plano_preco a
	where	a.nr_seq_tabela	= nr_seq_tabela_pc
	and	qt_idade_pc between a.qt_idade_inicial and a.qt_idade_final
	and	(a.ie_grau_titularidade = ie_tipo_parentesco_pc or a.ie_grau_titularidade is null)
	and	(((ie_tabela_qt_vidas_p = 'S') and qt_vidas_p between nvl(a.qt_vidas_inicial,qt_vidas_p) and nvl(a.qt_vidas_final,qt_vidas_p))
	or	 (ie_tabela_qt_vidas_p = 'N'))
	order	by	nvl(a.ie_grau_titularidade,' ');

begin

for r_c01_w in C01(nr_seq_tabela_p,qt_idade_p,ie_tipo_parentesco_p) loop
	begin
	if	(r_c01_w.nr_seq_reajuste_preco is not null) then
		select	vl_base,
			vl_preco_nao_subsid_base,
			vl_adaptacao_base,
			vl_minimo
		into	vl_preco_atual_w,
			vl_preco_nao_subsidiado_w,
			vl_adaptacao_w,
			vl_minimo_w
		from	pls_reajuste_preco
		where	nr_sequencia	= r_c01_w.nr_seq_reajuste_preco;
	else
		vl_preco_atual_w		:= r_c01_w.vl_preco_atual;
		vl_preco_nao_subsidiado_w	:= r_c01_w.vl_preco_nao_subsidiado;
		vl_adaptacao_w			:= r_c01_w.vl_adaptacao;
		vl_minimo_w			:= r_c01_w.vl_minimo;
	end if;
	
	nr_seq_plano_preco_w	:= r_c01_w.nr_seq_plano_preco;
	tx_acrescimo_w		:= r_c01_w.tx_acrescimo;
	end;
end loop; --C01

vl_preco_atual_p	:= nvl(vl_preco_atual_w,0);
vl_preco_nao_subsid_p	:= nvl(vl_preco_nao_subsidiado_w,0);
vl_adaptacao_p		:= nvl(vl_adaptacao_w,0);
vl_minimo_p		:= nvl(vl_minimo_w,0);
tx_acrescimo_p		:= nvl(tx_acrescimo_w,0);
nr_seq_plano_preco_p	:= nvl(nr_seq_plano_preco_w,0);

end obter_valor;

function obter_valor_com_desconto(	vl_preco_p	number,
					tx_desconto_p	number,
					vl_desconto_p	number)
				return number is
vl_retorno_w	number(15,2);
begin
vl_retorno_w	:= vl_preco_p;
if	(tx_desconto_p > 0) then
	vl_retorno_w	:= vl_retorno_w - (vl_retorno_w * tx_desconto_p / 100);
end if;

if	(vl_retorno_w = 0) then
	return 0;
else
	return vl_retorno_w - nvl(vl_desconto_p,0);
end if;

end obter_valor_com_desconto;

function obter_preco_segurado(	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
				dt_referencia_p		date,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type)
				return t_preco_beneficiario pipelined is

t_preco_beneficiario_row_w	t_preco_beneficiario_row;
nr_sequencia_w			number(10);
dt_nascimento_w			pessoa_fisica.dt_nascimento%type;
qt_idade_w			number(10);
qt_idade_fx_etaria_w		number(10);
qt_idade_reaj_w			number(10);
nr_seq_contrato_w		pls_contrato.nr_sequencia%type;
nr_seq_plano_preco_w		pls_plano_preco.nr_sequencia%type;
vl_preco_atual_w		pls_plano_preco.vl_preco_atual%type;
vl_preco_nao_subsidiado_w	pls_plano_preco.vl_preco_nao_subsidiado%type;
vl_adaptacao_w			pls_plano_preco.vl_adaptacao%type;
vl_minimo_w			pls_plano_preco.vl_minimo%type;
vl_reajuste_indice_w		number(15,2);
vl_reajuste_fx_etaria_w		number(15,2);
ie_regulamentacao_w		pls_plano.ie_regulamentacao%type;
qt_meses_niver_w		number(1);
tx_acrescimo_w			pls_plano_preco.tx_acrescimo%type;
dt_prox_valor_w			pls_segurado_valor.dt_preco%type;
qt_idade_limite_reaj_w		pls_segurado.qt_idade_limite_reaj%type;
qt_anos_limite_reaj_w		pls_segurado.qt_anos_limite_reaj%type;
ie_mes_cobranca_reaj_w		pls_contrato.ie_mes_cobranca_reaj%type;
dt_contratacao_w		pls_segurado.dt_contratacao%type;
dt_inclusao_operadora_w		pls_segurado.dt_inclusao_operadora%type;
nr_seq_segurado_valor_w		pls_segurado_valor.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,
		dt_preco,
		dt_alteracao,
		dt_inativacao,
		ie_motivo_alteracao,
		nr_seq_plano,
		nr_seq_tabela,
		ie_tipo_parentesco,
		ie_tabela_qt_vidas,
		qt_vidas,
		nr_seq_processo_judicial,
		ie_permite_reaj_fx_etaria,
		ie_permite_reaj_indice,
		nr_seq_regra_desconto,
		tx_desconto,
		vl_desconto,
		nr_seq_regra_apropriacao,
		ie_situacao
	from	(select	*
		from	pls_segurado_valor
		where	nr_seq_segurado	= nr_seq_segurado_p
		and	nr_seq_segurado_valor_w is null
		union all
		select	*
		from	pls_segurado_valor
		where	nr_sequencia	= nr_seq_segurado_valor_w)
	order by nr_sequencia;

Cursor C02 (	nr_seq_tabela_pc	pls_segurado_valor.nr_seq_tabela%type,
		qt_idade_pc		number,
		ie_tipo_parentesco_pc	pls_segurado_valor.ie_tipo_parentesco%type,
		ie_tabela_qt_vidas_pc	pls_segurado_valor.ie_tabela_qt_vidas%type,
		qt_vidas_pc		pls_segurado_valor.qt_vidas%type) is
	select	nr_sequencia nr_seq_plano_preco,
		trunc(add_months(dt_nascimento_w,(qt_idade_inicial*12)),'month') dt_preco
	from	pls_plano_preco
	where	nr_seq_tabela = nr_seq_tabela_pc
	and	qt_idade_inicial > qt_idade_w
	and	(ie_grau_titularidade = ie_tipo_parentesco_pc or ie_grau_titularidade is null)
	and	(((ie_tabela_qt_vidas_pc = 'S') and qt_vidas_pc between nvl(qt_vidas_inicial,qt_vidas_pc) and nvl(qt_vidas_final,qt_vidas_pc))
	or	 (ie_tabela_qt_vidas_pc = 'N'));

Cursor C03 (	nr_seq_tabela_pc	pls_segurado_valor.nr_seq_tabela%type,
		qt_idade_pc		number,
		ie_tipo_parentesco_pc	pls_segurado_valor.ie_tipo_parentesco%type,
		ie_tabela_qt_vidas_pc	pls_segurado_valor.ie_tabela_qt_vidas%type,
		qt_vidas_pc		pls_segurado_valor.qt_vidas%type,
		dt_prox_valor_pc	date) is
	select	a.nr_sequencia nr_seq_plano_preco,
		b.dt_reajuste dt_preco,
		b.dt_liberacao,
		a.qt_idade_inicial,
		a.qt_idade_final,
		b.vl_reajustado vl_preco,
		b.vl_reajustado - b.vl_base vl_reajuste_indice,
		b.vl_preco_nao_subsidiado,
		b.vl_adaptacao,
		b.vl_minimo,
		b.nr_sequencia nr_seq_reajuste_preco
	from	pls_plano_preco a,
		pls_reajuste_preco b
	where	a.nr_sequencia	= b.nr_seq_preco
	and	a.nr_seq_tabela = nr_seq_tabela_pc
	and	b.dt_liberacao is not null
	and	(b.dt_reajuste < dt_prox_valor_pc or dt_prox_valor_pc is null)
	and	(a.ie_grau_titularidade = ie_tipo_parentesco_pc or a.ie_grau_titularidade is null)
	and	(((ie_tabela_qt_vidas_pc = 'S') and qt_vidas_pc between nvl(a.qt_vidas_inicial,qt_vidas_pc) and nvl(a.qt_vidas_final,qt_vidas_pc))
	or	 (ie_tabela_qt_vidas_pc = 'N'));

begin
nr_sequencia_w	:= 0;
carregar_parametros(cd_estabelecimento_p);

if	(dt_referencia_p is not null) then
	select	max(nr_sequencia)
	into	nr_seq_segurado_valor_w
	from	pls_segurado_valor
	where	nr_seq_segurado	= nr_seq_segurado_p
	and	ie_situacao = 'A'
	and	dt_preco <= dt_referencia_p;
else
	nr_seq_segurado_valor_w	:= null;
end if;


select	trunc(b.dt_nascimento,'month'),
	a.nr_seq_contrato,
	c.ie_regulamentacao,
	nvl(a.qt_idade_limite_reaj,d.qt_idade_limite_reaj) qt_idade_limite_reaj,
	nvl(a.qt_anos_limite_reaj,d.qt_anos_limite_reaj) qt_anos_limite_reaj,
	d.ie_mes_cobranca_reaj,
	a.dt_contratacao,
	a.dt_inclusao_operadora
into	dt_nascimento_w,
	nr_seq_contrato_w,
	ie_regulamentacao_w,
	qt_idade_limite_reaj_w,
	qt_anos_limite_reaj_w,
	ie_mes_cobranca_reaj_w,
	dt_contratacao_w,
	dt_inclusao_operadora_w
from	pls_segurado	a,
	pessoa_fisica	b,
	pls_plano	c,
	pls_contrato	d
where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
and	c.nr_sequencia		= a.nr_seq_plano
and	d.nr_sequencia(+)	= a.nr_seq_contrato
and	a.nr_sequencia		= nr_seq_segurado_p;

qt_meses_niver_w	:= obter_qt_meses_niver(ie_mes_cobranca_reaj_w,ie_regulamentacao_w);
dt_nascimento_w		:= add_months(dt_nascimento_w,qt_meses_niver_w);

if	(qt_idade_limite_reaj_w is null) and
	(qt_anos_limite_reaj_w is null) then
	qt_idade_limite_reaj_w	:= pls_parametros_w.qt_idade_limite;
	qt_anos_limite_reaj_w	:= pls_parametros_w.qt_tempo_limite;
end if;

for r_c01_w in C01 loop
	begin
	if	(r_c01_w.ie_permite_reaj_fx_etaria = 'S') or (r_c01_w.ie_motivo_alteracao = '1') then
		qt_idade_w	:= trunc(months_between(r_c01_w.dt_preco, dt_nascimento_w) / 12);
	end if;
	
	obter_valor(r_c01_w.nr_seq_tabela, r_c01_w.dt_preco, qt_idade_w, r_c01_w.ie_tipo_parentesco, r_c01_w.ie_tabela_qt_vidas, r_c01_w.qt_vidas,
			nr_seq_plano_preco_w, vl_preco_atual_w, vl_preco_nao_subsidiado_w, vl_adaptacao_w, vl_minimo_w, tx_acrescimo_w);
	
	nr_sequencia_w	:= nr_sequencia_w+1;
	t_preco_beneficiario_row_w.nr_sequencia			:= nr_sequencia_w;
	t_preco_beneficiario_row_w.nr_seq_segurado_valor	:= r_c01_w.nr_sequencia;
	t_preco_beneficiario_row_w.dt_preco			:= r_c01_w.dt_preco;
	t_preco_beneficiario_row_w.dt_alteracao			:= r_c01_w.dt_alteracao;
	t_preco_beneficiario_row_w.dt_inativacao		:= r_c01_w.dt_inativacao;
	t_preco_beneficiario_row_w.qt_idade			:= qt_idade_w;
	t_preco_beneficiario_row_w.vl_preco			:= obter_valor_com_desconto(vl_preco_atual_w,r_c01_w.tx_desconto,r_c01_w.vl_desconto);
	t_preco_beneficiario_row_w.vl_sem_desconto		:= vl_preco_atual_w;
	t_preco_beneficiario_row_w.vl_preco_nao_subsidiado	:= obter_valor_com_desconto(vl_preco_nao_subsidiado_w,r_c01_w.tx_desconto,r_c01_w.vl_desconto);
	t_preco_beneficiario_row_w.vl_adaptacao			:= vl_adaptacao_w;
	t_preco_beneficiario_row_w.vl_reajuste_indice		:= 0;
	t_preco_beneficiario_row_w.vl_reajuste_fx_etaria	:= 0;
	t_preco_beneficiario_row_w.vl_minimo			:= vl_minimo_w;
	t_preco_beneficiario_row_w.ie_motivo_alteracao		:= r_c01_w.ie_motivo_alteracao;
	t_preco_beneficiario_row_w.nr_seq_reajuste_preco	:= null;
	t_preco_beneficiario_row_w.nr_seq_plano			:= r_c01_w.nr_seq_plano;
	t_preco_beneficiario_row_w.nr_seq_tabela		:= r_c01_w.nr_seq_tabela;
	t_preco_beneficiario_row_w.nr_seq_plano_preco		:= nr_seq_plano_preco_w;
	t_preco_beneficiario_row_w.ie_tipo_titularidade		:= r_c01_w.ie_tipo_parentesco;
	t_preco_beneficiario_row_w.ie_tabela_qt_vidas		:= r_c01_w.ie_tabela_qt_vidas;
	t_preco_beneficiario_row_w.qt_vidas			:= r_c01_w.qt_vidas;
	t_preco_beneficiario_row_w.nr_seq_processo_judicial	:= r_c01_w.nr_seq_processo_judicial;
	t_preco_beneficiario_row_w.ie_permite_reaj_fx_etaria	:= r_c01_w.ie_permite_reaj_fx_etaria;
	t_preco_beneficiario_row_w.ie_permite_reaj_indice	:= r_c01_w.ie_permite_reaj_indice;
	t_preco_beneficiario_row_w.nr_seq_regra_desconto	:= r_c01_w.nr_seq_regra_desconto;
	t_preco_beneficiario_row_w.tx_desconto			:= r_c01_w.tx_desconto;
	t_preco_beneficiario_row_w.vl_desconto			:= r_c01_w.vl_desconto;
	t_preco_beneficiario_row_w.nr_seq_regra_apropriacao	:= r_c01_w.nr_seq_regra_apropriacao;
	t_preco_beneficiario_row_w.ie_situacao			:= r_c01_w.ie_situacao;
	
	pipe row(t_preco_beneficiario_row_w);
	
	if	(r_c01_w.ie_situacao = 'A') then
		select	min(dt_preco)
		into	dt_prox_valor_w
		from	pls_segurado_valor
		where	nr_seq_segurado	= nr_seq_segurado_p
		and	dt_preco > r_c01_w.dt_preco
		and	ie_situacao	= 'A';
		
		--Adicionar reajustes por faixa etaria
		if	(r_c01_w.ie_permite_reaj_fx_etaria = 'S') then
			for r_c02_w in C02(r_c01_w.nr_seq_tabela, qt_idade_w, r_c01_w.ie_tipo_parentesco, r_c01_w.ie_tabela_qt_vidas, r_c01_w.qt_vidas) loop
				begin
				if	(dt_prox_valor_w is null or r_c02_w.dt_preco < dt_prox_valor_w) and
					(dt_referencia_p is null or r_c02_w.dt_preco <= trunc(dt_referencia_p,'month')) then
					qt_idade_fx_etaria_w	:= trunc(months_between(r_c02_w.dt_preco, dt_nascimento_w) / 12);
					
					if	(obter_se_permite_reaj(nr_seq_segurado_p,r_c02_w.dt_preco,qt_idade_limite_reaj_w,qt_anos_limite_reaj_w,qt_idade_fx_etaria_w,ie_regulamentacao_w,dt_contratacao_w,dt_inclusao_operadora_w) = 'S') then
						obter_valor(r_c01_w.nr_seq_tabela, r_c02_w.dt_preco, qt_idade_fx_etaria_w, r_c01_w.ie_tipo_parentesco, r_c01_w.ie_tabela_qt_vidas, r_c01_w.qt_vidas,
								nr_seq_plano_preco_w, vl_preco_atual_w, vl_preco_nao_subsidiado_w, vl_adaptacao_w, vl_minimo_w, tx_acrescimo_w);
						
						if	(tx_acrescimo_w > 0) then
							vl_reajuste_fx_etaria_w	:= vl_preco_atual_w - ((vl_preco_atual_w * 100) / (100 + tx_acrescimo_w));
						else
							vl_reajuste_fx_etaria_w	:= 0;
						end if;
						
						nr_sequencia_w	:= nr_sequencia_w+1;
						t_preco_beneficiario_row_w.nr_sequencia			:= nr_sequencia_w;
						t_preco_beneficiario_row_w.nr_seq_segurado_valor	:= r_c01_w.nr_sequencia;
						t_preco_beneficiario_row_w.dt_preco			:= r_c02_w.dt_preco;
						t_preco_beneficiario_row_w.dt_alteracao			:= r_c02_w.dt_preco;
						t_preco_beneficiario_row_w.dt_inativacao		:= r_c01_w.dt_inativacao;
						t_preco_beneficiario_row_w.qt_idade			:= qt_idade_fx_etaria_w;
						t_preco_beneficiario_row_w.vl_preco			:= obter_valor_com_desconto(vl_preco_atual_w,r_c01_w.tx_desconto,r_c01_w.vl_desconto);
						t_preco_beneficiario_row_w.vl_sem_desconto		:= vl_preco_atual_w;
						t_preco_beneficiario_row_w.vl_preco_nao_subsidiado	:= obter_valor_com_desconto(vl_preco_nao_subsidiado_w,r_c01_w.tx_desconto,r_c01_w.vl_desconto);
						t_preco_beneficiario_row_w.vl_adaptacao			:= vl_adaptacao_w;
						t_preco_beneficiario_row_w.vl_minimo			:= vl_minimo_w;
						t_preco_beneficiario_row_w.vl_reajuste_indice		:= 0;
						t_preco_beneficiario_row_w.vl_reajuste_fx_etaria	:= vl_reajuste_fx_etaria_w;
						t_preco_beneficiario_row_w.ie_motivo_alteracao		:= '2';
						t_preco_beneficiario_row_w.nr_seq_reajuste_preco	:= null;
						t_preco_beneficiario_row_w.nr_seq_plano			:= r_c01_w.nr_seq_plano;
						t_preco_beneficiario_row_w.nr_seq_tabela		:= r_c01_w.nr_seq_tabela;
						t_preco_beneficiario_row_w.nr_seq_plano_preco		:= r_c02_w.nr_seq_plano_preco;
						t_preco_beneficiario_row_w.ie_tipo_titularidade		:= r_c01_w.ie_tipo_parentesco;
						t_preco_beneficiario_row_w.ie_tabela_qt_vidas		:= r_c01_w.ie_tabela_qt_vidas;
						t_preco_beneficiario_row_w.qt_vidas			:= r_c01_w.qt_vidas;
						t_preco_beneficiario_row_w.nr_seq_processo_judicial	:= r_c01_w.nr_seq_processo_judicial;
						t_preco_beneficiario_row_w.ie_permite_reaj_fx_etaria	:= r_c01_w.ie_permite_reaj_fx_etaria;
						t_preco_beneficiario_row_w.ie_permite_reaj_indice	:= r_c01_w.ie_permite_reaj_indice;
						t_preco_beneficiario_row_w.nr_seq_regra_desconto	:= r_c01_w.nr_seq_regra_desconto;
						t_preco_beneficiario_row_w.tx_desconto			:= r_c01_w.tx_desconto;
						t_preco_beneficiario_row_w.vl_desconto			:= r_c01_w.vl_desconto;
						t_preco_beneficiario_row_w.nr_seq_regra_apropriacao	:= r_c01_w.nr_seq_regra_apropriacao;
						t_preco_beneficiario_row_w.ie_situacao			:= r_c01_w.ie_situacao;
						
						pipe row(t_preco_beneficiario_row_w);
					end if;
				end if;
				end;
			end loop; --C02
		end if;
		
		--Adicionar reajustes por indice de correcao
		if	(r_c01_w.ie_permite_reaj_indice = 'S') then
			for r_c03_w in C03(r_c01_w.nr_seq_tabela, qt_idade_w, r_c01_w.ie_tipo_parentesco, r_c01_w.ie_tabela_qt_vidas, r_c01_w.qt_vidas, dt_prox_valor_w) loop
				begin
				if	(r_c01_w.ie_permite_reaj_fx_etaria = 'S') then
					qt_idade_reaj_w	:= trunc(months_between(r_c03_w.dt_preco, dt_nascimento_w) / 12);
				else
					qt_idade_reaj_w	:= qt_idade_w;
				end if;
				
				if	(qt_idade_reaj_w between r_c03_w.qt_idade_inicial and r_c03_w.qt_idade_final) and
					(r_c03_w.dt_preco > r_c01_w.dt_preco) and
					(dt_referencia_p is null or r_c03_w.dt_preco <= trunc(dt_referencia_p,'month')) then
					nr_sequencia_w	:= nr_sequencia_w+1;
					t_preco_beneficiario_row_w.nr_sequencia			:= nr_sequencia_w;
					t_preco_beneficiario_row_w.nr_seq_segurado_valor	:= r_c01_w.nr_sequencia;
					t_preco_beneficiario_row_w.dt_preco			:= r_c03_w.dt_preco;
					t_preco_beneficiario_row_w.dt_alteracao			:= r_c03_w.dt_liberacao;
					t_preco_beneficiario_row_w.dt_inativacao		:= r_c01_w.dt_inativacao;
					t_preco_beneficiario_row_w.qt_idade			:= qt_idade_reaj_w;
					t_preco_beneficiario_row_w.vl_preco			:= obter_valor_com_desconto(r_c03_w.vl_preco,r_c01_w.tx_desconto,r_c01_w.vl_desconto);
					t_preco_beneficiario_row_w.vl_sem_desconto		:= r_c03_w.vl_preco;
					t_preco_beneficiario_row_w.vl_preco_nao_subsidiado	:= obter_valor_com_desconto(r_c03_w.vl_preco_nao_subsidiado,r_c01_w.tx_desconto,r_c01_w.vl_desconto);
					t_preco_beneficiario_row_w.vl_adaptacao			:= r_c03_w.vl_adaptacao;
					t_preco_beneficiario_row_w.vl_minimo			:= r_c03_w.vl_minimo;
					t_preco_beneficiario_row_w.vl_reajuste_indice		:= r_c03_w.vl_reajuste_indice;
					t_preco_beneficiario_row_w.vl_reajuste_fx_etaria	:= 0;
					t_preco_beneficiario_row_w.ie_motivo_alteracao		:= '3';
					t_preco_beneficiario_row_w.nr_seq_reajuste_preco	:= r_c03_w.nr_seq_reajuste_preco;
					t_preco_beneficiario_row_w.nr_seq_plano			:= r_c01_w.nr_seq_plano;
					t_preco_beneficiario_row_w.nr_seq_tabela		:= r_c01_w.nr_seq_tabela;
					t_preco_beneficiario_row_w.nr_seq_plano_preco		:= r_c03_w.nr_seq_plano_preco;
					t_preco_beneficiario_row_w.ie_tipo_titularidade		:= r_c01_w.ie_tipo_parentesco;
					t_preco_beneficiario_row_w.ie_tabela_qt_vidas		:= r_c01_w.ie_tabela_qt_vidas;
					t_preco_beneficiario_row_w.qt_vidas			:= r_c01_w.qt_vidas;
					t_preco_beneficiario_row_w.nr_seq_processo_judicial	:= r_c01_w.nr_seq_processo_judicial;
					t_preco_beneficiario_row_w.ie_permite_reaj_fx_etaria	:= r_c01_w.ie_permite_reaj_fx_etaria;
					t_preco_beneficiario_row_w.ie_permite_reaj_indice	:= r_c01_w.ie_permite_reaj_indice;
					t_preco_beneficiario_row_w.nr_seq_regra_desconto	:= r_c01_w.nr_seq_regra_desconto;
					t_preco_beneficiario_row_w.tx_desconto			:= r_c01_w.tx_desconto;
					t_preco_beneficiario_row_w.vl_desconto			:= r_c01_w.vl_desconto;
					t_preco_beneficiario_row_w.nr_seq_regra_apropriacao	:= r_c01_w.nr_seq_regra_apropriacao;
					t_preco_beneficiario_row_w.ie_situacao			:= r_c01_w.ie_situacao;
					
					pipe row(t_preco_beneficiario_row_w);
				end if;
				end;
			end loop; --C03
		end if;
	end if;
	
	end;
end loop; --C01

end obter_preco_segurado;

end pls_segurado_preco_pck;
/
