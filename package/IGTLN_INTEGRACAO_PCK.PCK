create	or replace
package igtln_integracao_pck as

-- Atualiza data e hora da integra��o
procedure apae_ws_atualiza_lote(nr_lote_p	number);
-- Atualiza dados endere�o
procedure apae_ws_atualiza_endereco(	cd_pessoa_fisica_p 		varchar2,
										nm_usuario_p 			varchar2, 
										ds_endereco_p 			varchar2, 
										nr_endereco_p 			varchar2, 
										ds_bairro_p 			varchar2, 
										ds_municipio_p 			varchar2, 
										sg_estado_p 			varchar2, 
										cd_cep_p 				varchar2);
-- Atualiza dados telefone
procedure apae_ws_atualiza_fone(cd_pessoa_fisica_p 			varchar2,
										nm_usuario_p 		varchar2, 
										ie_tipo_p			varchar2,
										nr_ddd_telefone_p	varchar2,
										nr_telefone_p		varchar2);
-- Grava dados exame
procedure apae_ws_grava_exame(	nr_seq_ficha_p  			number,
								cd_exame_equip_p			varchar2, 
								nm_usuario_p 				varchar2);

-- Grava ficha paciente 
procedure apae_ws_grava_ficha(	nr_sequencia_sec_p  		number,
								nm_usuario_p 				varchar2, 
								cd_pessoa_fisica_p      	varchar2, 				
								cd_prontuario_f_p			varchar2, 
								cd_barras_p  				varchar2, 
								cd_dnv_f_p  				varchar2,
								nr_cartao_nac_sus_p  		varchar2, 
								dt_nascimento_f_p   		varchar2, 
								hr_nascimento_f_p    		varchar2, 
								ie_periodo_nasc_f_p   		varchar2,  
								qt_peso_f_p   				number, 
								ie_sexo_f_p   				varchar2, 
								ie_cor_pf_f_p  				varchar2, 
								nr_idade_gest_f_p  			number, 
								dt_coleta_ficha_f_p     	varchar2,
								hr_coleta_f_p  				varchar2, 	
								ie_periodo_col_f_p      	varchar2,
								ie_npp_f_p  				varchar2, 
								ie_amamentado_f_p  			varchar2, 
								ie_alim_leite_f_p  			varchar2, 
								ie_premat_s_f_p 			varchar2, 
								ie_transfusao_f_p  			varchar2, 
								dt_transf_f_p   			varchar2, 
								ie_ictericia_f_p  			varchar2, 
								ie_susp_am_inad_p  			varchar2, 
								ie_amostra_p  				varchar2, 
								nr_seq_grau_parentesco_p	number, 
								nm_mae_f_p					varchar2,
								ie_gemelar_f_p				varchar2,
								ie_corticoide_f_p			varchar2,
								ie_mae_veg_f_p				varchar2,
								dt_ult_menst_p				varchar2,
								qt_gest_f_p					varchar2,
								ie_tipo_parto_p				varchar2,
								ie_aborto_p					varchar2,
								qt_aborto_p					varchar2,
								nr_sisprenatal_p			varchar2,								
								nr_prontuario_ex_p			varchar2, 
								nr_ficha_ext_p				varchar2, 
								cd_usuario_conv_ext_p		varchar2, 
								cd_lote_f_p					varchar2, 
								cd_pessoa_externa_p			varchar2,
								ds_observacao_p				varchar2,  
								ie_ficha_urg_p				varchar2,
								nr_seq_tipo_mat_lote_p		varchar2,
								dt_fabricacao_p				varchar2,
								dt_validade_p				varchar2,
								nr_sequencia_out_p 			out number);

-- Grava lote 
procedure apae_ws_grava_lote(	nr_lote_p 				number,
								nr_seq_instituicao_p 	number,
								nr_seq_posto_p 			number,
								ie_tipo_ficha_p 		varchar2,
								nr_seq_pacote_p 		number,		
								nm_usuario_p			varchar2, 
								nr_sequencia_out_p 		out number);
	
-- Obtem c�digo da pessoa fisica
procedure apae_ws_obter_cd_pessoa(	cd_prontuario_p 		varchar2,
									nm_paciente_p 			varchar2, 
									nm_usuario_p       		varchar2,
									cd_pessoa_fisica_out_p 	out varchar2);
	
-- Retorna verdadeiro se todos os exames de um lote est�o com status >= 35
function apae_ws_obter_result_exames(nr_lote_p	number) return varchar2;	
							
-- Realiza exclus�o de um determinado lote
procedure apae_ws_excluir_lote(nr_lote_p 	number);
							
end igtln_integracao_pck;
/

create or replace
package body igtln_integracao_pck as

	-- Atualiza data e hora da integra��o		
	procedure apae_ws_atualiza_lote(nr_lote_p	number) is
	begin
		if	(nr_lote_p > 0) then
			update	lote_ent_secretaria
			set		dt_integracao = sysdate, 
					ie_integracao = 'S', 
					qt_fichas_lote = (select count(1) from lote_ent_sec_ficha where nr_seq_lote_sec = nr_lote_p)
			where	nr_sequencia = nr_lote_p;
		end if;	
	end apae_ws_atualiza_lote;

	-- Atualiza dados endere�o	
	procedure apae_ws_atualiza_endereco(	cd_pessoa_fisica_p 	varchar2,
											nm_usuario_p 		varchar2, 
											ds_endereco_p 		varchar2, 
											nr_endereco_p 		varchar2, 
											ds_bairro_p 		varchar2, 
											ds_municipio_p 		varchar2, 
											sg_estado_p 		varchar2, 
											cd_cep_p 			varchar2) is
	nr_sequencia_w	number;
	tag_pais_w	varchar2(15);	
	begin
		select	max(ds_locale) 
		into	tag_pais_w
		from	user_locale 
		where	nm_user = nm_usuario_p;

		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from	compl_pessoa_fisica 
		where	ie_tipo_complemento = 1
		and 	cd_pessoa_fisica = cd_pessoa_fisica_p;
		
		if	(nr_sequencia_w is null) then 		
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica = cd_pessoa_fisica_p;		   
			
			if(tag_pais_w = 'de_DE')then
				insert into compl_pessoa_fisica (
					nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento, 
					dt_atualizacao, 
					nm_usuario,
					ds_endereco,
					ds_compl_end,
					ds_bairro,
					ds_municipio,
					sg_estado,
					cd_cep
				) values (
					nr_sequencia_w, 
					cd_pessoa_fisica_p, 
					1, 
					SYSDATE, 
					nm_usuario_p,
					ds_endereco_p,
					nr_endereco_p,
					ds_bairro_p,
					ds_municipio_p,
					sg_estado_p,
					cd_cep_p
				);
			else
				insert into compl_pessoa_fisica (
					nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento, 
					dt_atualizacao, 
					nm_usuario,
					ds_endereco,
					nr_endereco,
					ds_bairro,
					ds_municipio,
					sg_estado,
					cd_cep
				) values (
					nr_sequencia_w, 
					cd_pessoa_fisica_p, 
					1, 
					SYSDATE, 
					nm_usuario_p,
					ds_endereco_p,
					nr_endereco_p,
					ds_bairro_p,
					ds_municipio_p,
					sg_estado_p,
					cd_cep_p
				);
			end if;
		else
			if(tag_pais_w = 'de_DE')then
				update	compl_pessoa_fisica 
				set	dt_atualizacao = SYSDATE,
					nm_usuario = nm_usuario_p,
					ds_endereco = ds_endereco_p,
					ds_compl_end = nr_endereco_p,
					ds_bairro = ds_bairro_p,
					ds_municipio = ds_municipio_p,
					sg_estado = sg_estado_p,
					cd_cep = cd_cep_p
				where	nr_sequencia = nr_sequencia_w
				and		ie_tipo_complemento = 1
				and		cd_pessoa_fisica = cd_pessoa_fisica_p;
			else
				update	compl_pessoa_fisica 
				set	dt_atualizacao = SYSDATE,
					nm_usuario = nm_usuario_p,
					ds_endereco = ds_endereco_p,
					nr_endereco = nr_endereco_p,
					ds_bairro = ds_bairro_p,
					ds_municipio = ds_municipio_p,
					sg_estado = sg_estado_p,
					cd_cep = cd_cep_p
				where	nr_sequencia = nr_sequencia_w
				and		ie_tipo_complemento = 1
				and		cd_pessoa_fisica = cd_pessoa_fisica_p;
			end if;
		end if;						

	end apae_ws_atualiza_endereco;
    
	-- Atualiza dados telefone
	procedure apae_ws_atualiza_fone(cd_pessoa_fisica_p 	varchar2,
									nm_usuario_p 		varchar2, 
									ie_tipo_p			varchar2,
									nr_ddd_telefone_p	varchar2,
									nr_telefone_p		varchar2) is
	nr_sequencia_w	number;			
	begin
		if 	(ie_tipo_p = 'F') then
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	compl_pessoa_fisica 
			where	ie_tipo_complemento = 1
			and		cd_pessoa_fisica = cd_pessoa_fisica_p;
			
			if	(nr_sequencia_w is null) then 		
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica = cd_pessoa_fisica_p;		   
			
				insert into compl_pessoa_fisica (
					nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento, 
					dt_atualizacao, 
					nm_usuario, 
					nr_ddd_telefone,
					nr_telefone
				) values (
					nr_sequencia_w, 
					cd_pessoa_fisica_p, 
					1, 
					SYSDATE, 
					nm_usuario_p, 
					nr_ddd_telefone_p,
					nr_telefone_p				
				);			
			else
				update	compl_pessoa_fisica 
				set		dt_atualizacao = SYSDATE,
						nm_usuario = nm_usuario_p, 
						nr_ddd_telefone = nr_ddd_telefone_p,
						nr_telefone = nr_telefone_p
				where	nr_sequencia = nr_sequencia_w
				and		ie_tipo_complemento = 1
				and		cd_pessoa_fisica = cd_pessoa_fisica_p;
			end if;						
		end if;

		if 	(ie_tipo_p = 'M') then
			update	pessoa_fisica
			set		nr_ddd_celular = nr_ddd_telefone_p,
					nr_telefone_celular = nr_telefone_p
			where	cd_pessoa_fisica = cd_pessoa_fisica_p;		 
		end if;

	end apae_ws_atualiza_fone;

	-- Grava dados exame
	procedure apae_ws_grava_exame(	nr_seq_ficha_p  	number,
									cd_exame_equip_p	varchar2, 
									nm_usuario_p 		varchar2) is
	nr_sequencia_w		number;			
	nr_seq_exame_w		number;			
	cd_material_w		varchar(20);
	begin

		if  ((nr_seq_ficha_p is not null) and (cd_exame_equip_p is not null)) then 
			select	a.nr_seq_exame, c.cd_material_exame
			into	nr_seq_exame_w, cd_material_w   
			from	lab_exame_equip a, 
					equipamento_lab b,
					material_exame_lab c
			where	a.cd_exame_equip = cd_exame_equip_p
			and		a.cd_equipamento = b.cd_equipamento
			and		a.nr_seq_material = c.nr_sequencia 
			and		upper(b.ds_sigla) = 'APAE';			 			 
			 
			if (nr_seq_exame_w is not null) then 			
				select	lote_ent_sec_ficha_exam_seq.nextval
				into	nr_sequencia_w
				from	dual;
			
				insert into lote_ent_sec_ficha_exam ( 
					nr_sequencia, 
					dt_atualizacao, 
					nm_usuario, 
					nr_seq_exame, 
					cd_material_exame, 
					nr_seq_ficha
				) values ( 
					nr_sequencia_w, 
					SYSDATE,
					nm_usuario_p, 
					nr_seq_exame_w, 
					cd_material_w, 
					nr_seq_ficha_p
				); 					
			
			end if;
		end if;

	end apae_ws_grava_exame;		

	-- Grava ficha paciente 
	procedure apae_ws_grava_ficha(	nr_sequencia_sec_p  		number,
									nm_usuario_p 				varchar2, 
									cd_pessoa_fisica_p     		varchar2, 				
									cd_prontuario_f_p			varchar2, 
									cd_barras_p  				varchar2, 
									cd_dnv_f_p  				varchar2,
									nr_cartao_nac_sus_p  		varchar2, 
									dt_nascimento_f_p   		varchar2, 
									hr_nascimento_f_p    		varchar2, 
									ie_periodo_nasc_f_p   		varchar2,  
									qt_peso_f_p   				number, 
									ie_sexo_f_p   				varchar2, 
									ie_cor_pf_f_p  				varchar2, 
									nr_idade_gest_f_p  			number, 
									dt_coleta_ficha_f_p     	varchar2,
									hr_coleta_f_p  				varchar2, 	
									ie_periodo_col_f_p      	varchar2,
									ie_npp_f_p  				varchar2, 
									ie_amamentado_f_p  			varchar2, 
									ie_alim_leite_f_p  			varchar2, 
									ie_premat_s_f_p 			varchar2, 
									ie_transfusao_f_p  			varchar2, 
									dt_transf_f_p   			varchar2, 
									ie_ictericia_f_p  			varchar2, 
									ie_susp_am_inad_p  			varchar2, 
									ie_amostra_p  				varchar2, 
									nr_seq_grau_parentesco_p	number, 
									nm_mae_f_p					varchar2,
									ie_gemelar_f_p				varchar2,
									ie_corticoide_f_p			varchar2,
									ie_mae_veg_f_p				varchar2,
									dt_ult_menst_p				varchar2,
									qt_gest_f_p					varchar2,
									ie_tipo_parto_p				varchar2,
									ie_aborto_p					varchar2,
									qt_aborto_p					varchar2,
									nr_sisprenatal_p			varchar2,								
									nr_prontuario_ex_p			varchar2, 
									nr_ficha_ext_p				varchar2, 
									cd_usuario_conv_ext_p		varchar2, 
									cd_lote_f_p					varchar2, 
									cd_pessoa_externa_p			varchar2,
									ds_observacao_p				varchar2,  
									ie_ficha_urg_p				varchar2,
									nr_seq_tipo_mat_lote_p		varchar2,
									dt_fabricacao_p				varchar2,
									dt_validade_p				varchar2,
									nr_sequencia_out_p 			out number) is
	nr_sequencia_w	number;			
	begin
		if 	(nr_sequencia_sec_p is not null) then		
			select	lote_ent_sec_ficha_seq.nextval
			into	nr_sequencia_w
			from	dual;
		
			insert into lote_ent_sec_ficha ( 
				nr_sequencia, 
				nr_seq_lote_sec, 
				dt_atualizacao, 
				nm_usuario,	
				cd_pessoa_fisica, 			
				cd_prontuario_f,
				cd_barras,
				cd_dnv_f,
				nr_cartao_nac_sus,
				dt_nascimento_f,
				hr_nascimento_f,
				ie_periodo_nasc_f,
				qt_peso_f,
				ie_sexo_f,
				ie_cor_pf_f,
				nr_idade_gest_f,
				dt_coleta_ficha_f, 
				hr_coleta_f,
				ie_periodo_col_f, 
				ie_npp_f,
				ie_amamentado_f,
				ie_alim_leite_f,
				ie_premat_s_f,
				ie_transfusao_f,
				dt_transf_f,
				ie_ictericia_f,
				ie_susp_am_inad,
				ie_amostra,			
				nr_seq_grau_parentesco,
				nm_mae_f,
				ie_gemelar_f,
				ie_corticoide_f,
				ie_mae_veg_f,
				dt_ult_menst,
				qt_gest_f,
				ie_tipo_parto,
				ie_aborto,
				qt_aborto,
				nr_sisprenatal,
				nr_prontuario_ex, 
				nr_ficha_ext,
				cd_usuario_conv_ext,
				cd_lote_f,
				cd_pessoa_externa,
				ds_observacao,
				ie_ficha_urg,
				ie_ficha_adicional,
				nr_seq_tipo_mat_lote,
				dt_fabricacao,
				dt_validade
			) values ( 
				nr_sequencia_w,
				nr_sequencia_sec_p,
				sysdate,
				nm_usuario_p, 
				cd_pessoa_fisica_p,
				cd_prontuario_f_p, 
				cd_barras_p, 
				cd_dnv_f_p,
				nr_cartao_nac_sus_p, 
				TO_DATE(dt_nascimento_f_p,'dd-mm-yyyy'),
				TO_DATE(hr_nascimento_f_p, 'hh24:mi'), 
				ie_periodo_nasc_f_p, 
				qt_peso_f_p, 
				ie_sexo_f_p, 
				ie_cor_pf_f_p, 
				nr_idade_gest_f_p,
				TO_DATE(dt_coleta_ficha_f_p,'dd-mm-yyyy'),
				TO_DATE(hr_coleta_f_p, 'hh24:mi'), 			
				ie_periodo_col_f_p,
				ie_npp_f_p, 
				ie_amamentado_f_p, 
				ie_alim_leite_f_p, 
				ie_premat_s_f_p, 
				ie_transfusao_f_p, 			
				TO_DATE(dt_transf_f_p,'dd-mm-yyyy'),			
				ie_ictericia_f_p, 
				NVL(ie_susp_am_inad_p, 'I'), 
				ie_amostra_p, 
				nr_seq_grau_parentesco_p, 			
				nm_mae_f_p,	
				ie_gemelar_f_p,
				ie_corticoide_f_p,
				ie_mae_veg_f_p,
				TO_DATE(dt_ult_menst_p,'dd-mm-yyyy'),
				qt_gest_f_p,
				ie_tipo_parto_p,
				ie_aborto_p,
				qt_aborto_p,
				nr_sisprenatal_p,
				nr_prontuario_ex_p, 
				nr_ficha_ext_p,
				cd_usuario_conv_ext_p,
				cd_lote_f_p,
				cd_pessoa_externa_p,			
				ds_observacao_p,
				ie_ficha_urg_p,
				'N',
				nr_seq_tipo_mat_lote_p,
				TO_DATE(dt_fabricacao_p,'dd-mm-yyyy'),
				TO_DATE(dt_validade_p,'dd-mm-yyyy')
				); 							
		end if;	

	nr_sequencia_out_p := nr_sequencia_w;

	end apae_ws_grava_ficha;
	
	-- Grava lote 
	procedure apae_ws_grava_lote(	nr_lote_p 				number,
									nr_seq_instituicao_p 	number,
									nr_seq_posto_p 			number,
									ie_tipo_ficha_p 		varchar2,
									nr_seq_pacote_p 		number,		
									nm_usuario_p			varchar2, 
									nr_sequencia_out_p 		out number) is
	nr_sequencia_w	number;			
	nr_seq_pacote_w number;	
	begin
		-- Se tipo de ficha for igual a N, cadastros de Pacotes � Obrigat�rio
		if 	(ie_tipo_ficha_p = 'N') then 
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	lote_ent_secretaria
			where	nr_lote = nr_lote_p 
			and		nr_seq_instituicao = nr_seq_instituicao_p
			and		nr_seq_posto = nr_seq_posto_p 
			and		ie_tipo_ficha = ie_tipo_ficha_p
			and		nr_seq_pacote = nr_seq_pacote_p;
			   
			nr_seq_pacote_w := nr_seq_pacote_p;   
		else 
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	lote_ent_secretaria
			where	nr_lote = nr_lote_p 
			and		nr_seq_instituicao = nr_seq_instituicao_p
			and		nr_seq_posto = nr_seq_posto_p 
			and		ie_tipo_ficha = ie_tipo_ficha_p;		   
			   
			nr_seq_pacote_w := null;   
		end if;

		-- se n�o achou, insere registro
		if	(nr_sequencia_w is null) then
			select	lote_ent_secretaria_seq.nextval
			into	nr_sequencia_w
			from 	dual;
		
			insert into lote_ent_secretaria ( 
				nr_sequencia, 
				nr_seq_instituicao, 
				dt_atualizacao, 
				nm_usuario, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				ie_tipo_ficha, 			
				dt_recebimento, 			
				nr_seq_pacote, 
				nr_lote, 			
				nr_seq_posto
			) values ( 
				nr_sequencia_w, 
				nr_seq_instituicao_p, 
				sysdate, 
				nm_usuario_p, 
				sysdate, 
				nm_usuario_p, 
				ie_tipo_ficha_p, 			
				sysdate, 
				nr_seq_pacote_w, 
				nr_lote_p, 
				nr_seq_posto_p
				); 		
		end if;
		
	nr_sequencia_out_p := nr_sequencia_w;

	end apae_ws_grava_lote;
	
	-- Obtem c�digo da pessoa fisica
	procedure apae_ws_obter_cd_pessoa(	cd_prontuario_p 		varchar2,
										nm_paciente_p 			varchar2, 
										nm_usuario_p            varchar2,
										cd_pessoa_fisica_out_p 	out varchar2) is
	cd_pessoa_fisica_w	varchar2(10);			
	begin
		-- se encontrar pelo c�digo do prontuario, retorna o cd_pessoa_fisica
		-- sen�o, insere na tabela pessoas_fisica where nome_p e retorna o novo cd_pessoa_fisica
		if 	(cd_prontuario_p  is not null) then
			select	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w 	
			from	pessoa_fisica
			where	cd_pessoa_fisica = cd_prontuario_p;	
		end if;
		
		if 	(cd_pessoa_fisica_w is null) then		
			select	pessoa_fisica_seq.nextval
			into	cd_pessoa_fisica_w
			from	dual;
		
			insert into pessoa_fisica ( 
				cd_pessoa_fisica, 
				ie_tipo_pessoa, 
				nm_pessoa_fisica, 
				dt_atualizacao, 
				nm_usuario 
			) values ( 
				cd_pessoa_fisica_w,
				2, 
				nm_paciente_p, 
				SYSDATE,
				nm_usuario_p
				); 							
		end if;

	cd_pessoa_fisica_out_p := cd_pessoa_fisica_w;

	end apae_ws_obter_cd_pessoa;

	-- Retorna verdadeiro se todos os exames de um lote est�o com status >= 35
	function apae_ws_obter_result_exames(nr_lote_p	number) return varchar2 is 
	nr_aprovados_w	number;
	nr_total_w		number;
	begin	   
		select	count(*) aprovados 
		into	nr_aprovados_w
		from	prescr_procedimento a, 
				lote_ent_sec_ficha_exam b, 
				lote_ent_sec_ficha c
		where	a.ie_status_atend >= 35
		and		a.nr_seq_exame = b.nr_seq_exame
		and		c.nr_sequencia = b.nr_seq_ficha
		and		c.nr_prescricao = a.nr_prescricao
		and		c.nr_seq_lote_sec = nr_lote_p;
		   
		select	count(*) total 
		into	nr_total_w
		from	prescr_procedimento a, 
				lote_ent_sec_ficha_exam b, 
				lote_ent_sec_ficha c
		where	a.nr_seq_exame = b.nr_seq_exame
		and		c.nr_sequencia = b.nr_seq_ficha
		and		c.nr_prescricao = a.nr_prescricao
		and		c.nr_seq_lote_sec = nr_lote_p;          
		 
    if 	((nr_total_w > 0) and (nr_aprovados_w = nr_total_w)) then 
		return 'S';	
	else 
		return 'N';	
	end if;
	
	end apae_ws_obter_result_exames;	

	-- Realiza exclus�o de um determinado lote	          
	procedure apae_ws_excluir_lote(nr_lote_p 	number) is
	begin
			delete from lote_ent_sec_ficha_exam where nr_seq_ficha in (select nr_sequencia from lote_ent_sec_ficha where nr_seq_lote_sec = nr_lote_p);
			delete from lote_ent_sec_ficha where nr_seq_lote_sec = nr_lote_p;
			delete from lote_ent_secretaria where nr_sequencia = nr_lote_p;
			commit;
	end apae_ws_excluir_lote;
	
end igtln_integracao_pck;
/
