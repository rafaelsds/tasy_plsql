create or replace package pls_gerar_alerta_web_pck as

procedure pls_gerar_alerta_evento_audc(	ie_tipo_evento_p		pls_regra_geracao_evento.ie_evento_disparo%type,
					ie_tipo_processo_audc_p		pls_regra_geracao_evento.ie_tipo_processo_audc%type,
					nr_seq_audc_atend_p		audc_atendimento.nr_sequencia%type,
					ie_executa_commit_p		Varchar2,
					nm_usuario_p			usuario.nm_usuario%type);
					
					
end pls_gerar_alerta_web_pck; 
/

create or replace package body      pls_gerar_alerta_web_pck as 


procedure pls_gerar_notificacao_web(	ds_titulo_p		pls_comunic_externa_web.ds_titulo%type,
					ds_mensagem_p 		pls_comunic_externa_web.ds_texto%type,
					nm_usuario_prestador_p	pls_comunic_externa_web.nm_usuario_prestador%type,
					nm_usuario_tasy_p	pls_comunic_externa_web.nm_usuario_tasy%type,
					ie_tipo_login_p		pls_comunic_externa_web.ie_tipo_login%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

begin
	insert into pls_comunic_externa_web(
	 	nr_sequencia, ie_situacao, dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		ds_titulo, ds_texto, dt_criacao, 
		dt_liberacao, dt_fim_liberacao, nm_usuario_prestador,
		nm_usuario_tasy, ie_tipo_login, ie_tipo_comunicado_web)
	values(	pls_comunic_externa_web_seq.nextval, 'A', sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		ds_titulo_p, ds_mensagem_p, sysdate,
		sysdate, (sysdate+60), nm_usuario_prestador_p,
		nm_usuario_tasy_p, ie_tipo_login_p,'N');

end pls_gerar_notificacao_web;



procedure pls_gerar_evento_controle( 	ie_tipo_evento_p		pls_regra_geracao_evento.ie_evento_disparo%type,
					nr_seq_tipo_evento_p		pls_tipo_alerta_evento.nr_sequencia%type,
					ds_titulo_p			pls_comunic_externa_web.ds_titulo%type,
					ds_mensagem_p 			pls_comunic_externa_web.ds_texto%type,
					nm_usuario_p			usuario.nm_usuario%type,
					nr_seq_evento_controle_p	out nocopy pls_alerta_evento_controle.nr_sequencia%type) is

nr_seq_evento_controle_w 	pls_alerta_evento_controle.nr_sequencia%type;

begin
	insert into pls_alerta_evento_controle(
		nr_sequencia, dt_geracao_evento, ie_evento_disparo,
		nr_seq_tipo_evento, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, ds_mensagem,
		ds_titulo, ie_tipo_envio)
	values(	pls_alerta_evento_controle_seq.nextval, sysdate, ie_tipo_evento_p,
		nr_seq_tipo_evento_p, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, ds_mensagem_p,
		ds_titulo_p,'AE') returning nr_sequencia into nr_seq_evento_controle_w;
		
	nr_seq_evento_controle_p := nr_seq_evento_controle_w;

end pls_gerar_evento_controle;


procedure pls_gerar_evento_controle_dest( 	nr_seq_evento_controle_p	pls_alerta_evento_controle.nr_sequencia%type,
						ie_forma_envio_p		pls_alerta_evento_cont_des.ie_forma_envio%type,
						ie_tipo_pessoa_dest_p		pls_alerta_evento_cont_des.ie_tipo_pessoa_dest%type,
						ds_mensagem_p 			pls_comunic_externa_web.ds_texto%type,						
						nm_usuario_p			usuario.nm_usuario%type) is	
begin

	if 	( nr_seq_evento_controle_p  is not null ) then
	
		insert into pls_alerta_evento_cont_des( 
			nr_sequencia, nr_seq_evento_controle, ie_status_envio,
			ie_forma_envio, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, ie_tipo_pessoa_dest,
			dt_envio_mensagem, ds_mensagem)
		values(	pls_alerta_evento_cont_des_seq.nextval, nr_seq_evento_controle_p, 'E',
			ie_forma_envio_p, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, ie_tipo_pessoa_dest_p,
			sysdate, ds_mensagem_p);	
	end if;

end pls_gerar_evento_controle_dest;


procedure pls_gerar_alerta_evento_audc
			(	ie_tipo_evento_p		pls_regra_geracao_evento.ie_evento_disparo%type,
				ie_tipo_processo_audc_p		pls_regra_geracao_evento.ie_tipo_processo_audc%type,
				nr_seq_audc_atend_p		audc_atendimento.nr_sequencia%type,
				ie_executa_commit_p		Varchar2,
				nm_usuario_p			usuario.nm_usuario%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar alerta de evento.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
Eventos ie_evento_disparo 14 - Auditoria Concorrente

IE_TIPO_PROCESSO_AUDC
1 Gerar atendimento para auditoria concorrente
2 Encerrar auditoria concorrente
3 Solicitar justificativa auditoria concorrente
4 Responder justificativa auditoria concorrente
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


cursor C01 (	ie_tipo_evento_pc		pls_regra_geracao_evento.ie_evento_disparo%type,
		ie_tipo_processo_audc_pc	pls_regra_geracao_evento.ie_tipo_processo_audc%type) is
		
	select	nr_sequencia,
		nr_seq_evento_mens
	from	pls_regra_geracao_evento
	where	ie_situacao			= 'A'
	and	ie_evento_disparo		= ie_tipo_evento_pc
	and	ie_tipo_processo_audc		= ie_tipo_processo_audc_pc;
	

cursor C02 (	nr_seq_evento_mens_pc		pls_alerta_evento_mensagem.nr_sequencia%type ) is
	select	b.ds_mensagem,
		b.ie_tipo_envio,
		b.ds_titulo,
		b.nr_seq_tipo_evento,
		b.ds_remetente_sms,
		b.ds_remetente_email		
	from	pls_tipo_alerta_evento a,
		pls_alerta_evento_mensagem b
	where 	b.nr_seq_tipo_evento = a.nr_sequencia
	and	b.nr_sequencia	= nr_seq_evento_mens_pc
	and	b.ie_situacao	= 'A'
	and	a.ie_situacao   = 'A';
	

cursor C03 (	nr_seq_evento_mens_pc		pls_alerta_evento_mensagem.nr_sequencia%type ) is
	select	nr_sequencia,
		ie_forma_envio,
		ie_tipo_pessoa_dest
	from	pls_alerta_evento_destino
	where	nr_seq_evento_mens	= nr_seq_evento_mens_pc
	and	ie_situacao 		= 'A';

	
cursor C04 ( nr_seq_prestador_pc	pls_prestador.nr_sequencia%type) is 
	select	nm_usuario_web,
		pls_obter_param_web(1246, 68, cd_estabelecimento, nr_sequencia, nr_seq_perfil_web, null, null,'P', null,null) vl_parametro
	from	pls_usuario_web
	where	nr_seq_prestador = nr_seq_prestador_pc
	and     ie_situacao = 'A'
	union	
	select  nm_usuario_web,
		pls_obter_param_web(1246, 68, a.cd_estabelecimento, a.nr_sequencia, a.nr_seq_perfil_web, null, null,'P', null,null) vl_parametro
	from    pls_usuario_web a,
		pls_prestador_usuario_web b
	where   b.nr_seq_usuario = a.nr_sequencia
	and     b.nr_seq_prestador = nr_seq_prestador_pc
	and     b.ie_situacao = 'A';	
	
cursor C05 ( nr_seq_audc_atend_pc audc_atendimento.nr_sequencia%type ) is
	select  distinct c.nm_usuario_exec,
		(select u.ds_email from usuario u where u.nm_usuario = c.nm_usuario_exec) ds_email
	from    audc_atend_grupo_aud a,
		pls_grupo_auditor b,
		pls_membro_grupo_aud c
	where   a.nr_seq_grupo = b.nr_sequencia
	and     c.nr_Seq_grupo = b.nr_sequencia
	and	c.ie_situacao = 'A'
	and     a.nr_seq_audc_atend = nr_seq_audc_atend_pc;
	

nr_seq_prestador_w 		pls_prestador.nr_sequencia%type;
nr_seq_evento_controle_w	pls_alerta_evento_controle.nr_sequencia%type;
ds_email_dest_w			Varchar2(255);
ds_usuarios_w			Varchar2(4000);


begin

if	( ie_tipo_evento_p = 14 and nr_seq_audc_atend_p is not null ) then
	
	select 	max(nr_seq_prestador_solic)
	into	nr_seq_prestador_w
	from 	audc_atendimento
	where	nr_sequencia = nr_seq_audc_atend_p;
	
end if;	

for c01_w in C01 ( ie_tipo_evento_p, ie_tipo_processo_audc_p) loop 
	
	nr_seq_evento_controle_w := null;
	
	for c02_w  in C02 ( c01_w.nr_seq_evento_mens ) loop 
		
		if	( nr_seq_evento_controle_w is null ) then
			pls_gerar_evento_controle( ie_tipo_evento_p, c02_w.nr_seq_tipo_evento, c02_w.ds_titulo, c02_w.ds_mensagem, nm_usuario_p, nr_seq_evento_controle_w );
		end if;	
		
		for c03_w in C03 ( c01_w.nr_seq_evento_mens ) loop 
			
			if	( c03_w.ie_forma_envio = 'EM') then
				
				begin
					if	( c03_w.ie_tipo_pessoa_dest = 'PR') then
	
						ds_email_dest_w := substr(pls_obter_dados_prestador( nr_seq_prestador_w,'M'), 0, 255);	
					
						if	( ds_email_dest_w is not null and c02_w.ds_remetente_email is not null ) then
							enviar_email( c02_w.ds_titulo, c02_w.ds_mensagem, c02_w.ds_remetente_email, ds_email_dest_w, nm_usuario_p, 'M');
							pls_gerar_evento_controle_dest( nr_seq_evento_controle_w, c03_w.ie_forma_envio,c03_w.ie_tipo_pessoa_dest, c02_w.ds_mensagem, nm_usuario_p);
						end if;
					
					elsif	( c03_w.ie_tipo_pessoa_dest = 'AU' ) then
					
						for c05_w in C05 ( nr_seq_audc_atend_p ) loop						
							if	( c05_w.ds_email is not null and c02_w.ds_remetente_email is not null ) then
								enviar_email( c02_w.ds_titulo, c02_w.ds_mensagem, c02_w.ds_remetente_email, c05_w.ds_email, nm_usuario_p, 'M');
								pls_gerar_evento_controle_dest( nr_seq_evento_controle_w, c03_w.ie_forma_envio,c03_w.ie_tipo_pessoa_dest, c02_w.ds_mensagem, nm_usuario_p);
							end if;
						end loop;
					end if;
					
				exception when others then
					ds_email_dest_w := null; 
				end;	
				
			elsif	( c03_w.ie_forma_envio = 'NTF') then
				if	( c03_w.ie_tipo_pessoa_dest = 'PR') then	
						
						/*Bloco utilizado para obter os logins do prestador que possuem acesso a Auditoria Concorrente*/
						ds_usuarios_w := null;
						for c04_w in C04 ( nr_seq_prestador_w ) loop								
							
							if	( c04_w.vl_parametro = 'S') then
								if	( ds_usuarios_w is null ) then							
									ds_usuarios_w := c04_w.nm_usuario_web;
								else
									ds_usuarios_w := ds_usuarios_w || ','||c04_w.nm_usuario_web;
								end if;
							end if;
						end loop;
						
						if	( ds_usuarios_w is not null ) then	
							pls_gerar_notificacao_web( c02_w.ds_titulo, c02_w.ds_mensagem, ds_usuarios_w, null, 'P', nm_usuario_p);	
							pls_gerar_evento_controle_dest( nr_seq_evento_controle_w, c03_w.ie_forma_envio,c03_w.ie_tipo_pessoa_dest, c02_w.ds_mensagem, nm_usuario_p);
						end if;
						
				elsif	( c03_w.ie_tipo_pessoa_dest = 'AU' ) then
						/* Bloco utilizado para obter os logins auditor concorrente que possuem acesso a Auditoria Concorrente */
						ds_usuarios_w := null;
						for c05_w in C05 ( nr_seq_audc_atend_p ) loop	
						
							if	( ds_usuarios_w is null ) then							
								ds_usuarios_w := c05_w.nm_usuario_exec;
							else
								ds_usuarios_w := ds_usuarios_w || ','||c05_w.nm_usuario_exec;
							end if;
						end loop;
												
						if	( ds_usuarios_w is not null ) then
							pls_gerar_notificacao_web( c02_w.ds_titulo, c02_w.ds_mensagem, null, ds_usuarios_w, 'AC', nm_usuario_p);
							pls_gerar_evento_controle_dest( nr_seq_evento_controle_w, c03_w.ie_forma_envio,c03_w.ie_tipo_pessoa_dest, c02_w.ds_mensagem, nm_usuario_p);	
						end if;	
				end if;
			end if;
			
			if	( ie_executa_commit_p = 'S' ) then
				commit;
			end if;			
		end loop;
	end loop;
end loop;

end pls_gerar_alerta_evento_audc;

end pls_gerar_alerta_web_pck;
/