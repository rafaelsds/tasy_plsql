CREATE OR REPLACE PACKAGE PKG_BSI_REPORT AS
  --
  TYPE T_REC_ATEND_UNIDADE IS RECORD (CD_ESTABELECIMENTO   SETOR_ATENDIMENTO.CD_ESTABELECIMENTO%TYPE,
                                      CD_SETOR_ATENDIMENTO ATEND_PACIENTE_UNIDADE.CD_SETOR_ATENDIMENTO%TYPE,
                                      NR_ATENDIMENTO       ATEND_PACIENTE_UNIDADE.NR_ATENDIMENTO%TYPE,
                                      DT_ENTRADA_UNIDADE   DATE,
                                      DT_SAIDA             DATE);
  --
  TYPE T_TAB_ATEND_UNIDADE IS TABLE OF T_REC_ATEND_UNIDADE;
  --
  FUNCTION GET_TAB_ATEND_UNIDADE(P_START_DATE    DATE,
                                 P_END_DATE      DATE,
                                 P_AGE           NUMBER,
                                 P_CLINICAL_UNIT VARCHAR2)
    RETURN T_TAB_ATEND_UNIDADE
    PIPELINED;
  --
  FUNCTION GET_GOAL_COMPLIANCE(P_NR_ATEND          NUMBER,
                               P_NR_SEQ_ATEND_DISP NUMBER,
                               P_ESTAB             NUMBER,
                               P_DT_ATEND          DATE,
                               P_GOAL              NUMBER) RETURN NUMBER;
  --
  PROCEDURE RUN(
    P_START_DATE IN DATE DEFAULT SYSDATE, 
    P_END_DATE IN DATE DEFAULT SYSDATE,
    P_AGE IN NUMBER DEFAULT 0,
    P_CLINICAL_UNIT IN VARCHAR2,
    P_ELEMENTS IN VARCHAR2 DEFAULT 'N');   
  --
END PKG_BSI_REPORT;
/
CREATE OR REPLACE PACKAGE BODY PKG_BSI_REPORT AS
  --
  PROCEDURE TABLE_CLEANING IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE BSI_REPORT_RESUME';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE BSI_REPORT';
  END TABLE_CLEANING;
  --
  FUNCTION GET_TAB_ATEND_UNIDADE(P_START_DATE    DATE,
                                 P_END_DATE      DATE,
                                 P_AGE           NUMBER,
                                 P_CLINICAL_UNIT VARCHAR2)
    RETURN T_TAB_ATEND_UNIDADE PIPELINED IS

    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE; 
    
    CURSOR C_UNIDADE_ATEND_BSI IS
      SELECT S.CD_ESTABELECIMENTO,
             U.CD_SETOR_ATENDIMENTO,
             U.NR_ATENDIMENTO,
             MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
             PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                  U.DT_ENTRADA_UNIDADE,
                                                  U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR                                              
        FROM (SELECT DISTINCT P.NR_ATENDIMENTO
                FROM ATEND_PAC_DISPOSITIVO A,
                     ATENDIMENTO_PACIENTE P 
               WHERE A.NR_ATENDIMENTO = P.NR_ATENDIMENTO   
                 AND EXISTS (SELECT 1 FROM DISPOSITIVO D WHERE D.NR_SEQUENCIA = A.NR_SEQ_DISPOSITIVO AND D.IE_CLASSIF_DISP_NISS = 'CCIP')
                 AND OBTER_IDADE_PF(P.CD_PESSOA_FISICA, A.DT_INSTALACAO, 'A') >= P_AGE
                 AND A.DT_INSTALACAO <= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(P_END_DATE), SYSDATE))
                 AND (A.DT_RETIRADA IS NULL OR
                      A.DT_RETIRADA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, P_START_DATE))
                 AND P.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(P_END_DATE), SYSDATE))
                 AND (P.DT_ALTA IS NULL OR
                      P.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, P_START_DATE))
                 AND EXISTS 
                     (SELECT 1
                        FROM GQA_PROTOCOLO_PAC G
                       WHERE G.NR_ATENDIMENTO = A.NR_ATENDIMENTO
                         AND G.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(6)
                         AND G.IE_SITUACAO = 'A'
                         AND G.DT_INCLUSAO <= NVL(A.DT_RETIRADA, G.DT_INCLUSAO)
                         AND A.DT_INSTALACAO <= NVL(G.DT_TERMINO, A.DT_INSTALACAO))) AP,
             ATEND_PACIENTE_UNIDADE U,
             SETOR_ATENDIMENTO S
       WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
         AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
         AND (P_CLINICAL_UNIT = '0' OR OBTER_SE_CONTIDO(S.CD_SETOR_ATENDIMENTO, P_CLINICAL_UNIT) = 'S')
         AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND S.CD_SETOR_ATENDIMENTO = US.CD_SETOR_ATENDIMENTO)  
         AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
         AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
         AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(P_END_DATE), SYSDATE))         
         AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                      U.DT_ENTRADA_UNIDADE,
                                                      U.CD_SETOR_ATENDIMENTO),
                 PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)) >=
             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
       GROUP BY S.CD_ESTABELECIMENTO,
                U.CD_SETOR_ATENDIMENTO,
                U.NR_ATENDIMENTO,
                PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                     U.DT_ENTRADA_UNIDADE,
                                                     U.CD_SETOR_ATENDIMENTO);

  TYPE T_UNIDADE_ATEND_BSI IS TABLE OF C_UNIDADE_ATEND_BSI%ROWTYPE;
  R_UNIDADE_ATEND_BSI T_UNIDADE_ATEND_BSI;

  BEGIN 
  
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    
    OPEN C_UNIDADE_ATEND_BSI;
    LOOP
      FETCH C_UNIDADE_ATEND_BSI BULK COLLECT INTO R_UNIDADE_ATEND_BSI LIMIT 1000;
      
        BEGIN
          FOR i IN R_UNIDADE_ATEND_BSI.FIRST..R_UNIDADE_ATEND_BSI.LAST LOOP
            PIPE ROW(R_UNIDADE_ATEND_BSI(i));  
          END LOOP;
        EXCEPTION WHEN OTHERS THEN
          NULL;
        END; 

      EXIT WHEN C_UNIDADE_ATEND_BSI%NOTFOUND; 
 
    END LOOP;
    CLOSE C_UNIDADE_ATEND_BSI;                                  
  
    RETURN;
  END GET_TAB_ATEND_UNIDADE;
  --
  FUNCTION GET_GOAL_COMPLIANCE(P_NR_ATEND          NUMBER,
                               P_NR_SEQ_ATEND_DISP NUMBER,
                               P_ESTAB             NUMBER,
                               P_DT_ATEND          DATE,
                               P_GOAL              NUMBER) RETURN NUMBER IS
  
    NR NUMBER;
  
  BEGIN

    SELECT COUNT(1)
      INTO NR
      FROM DISPOSITIVO_INF_ADIC D
     WHERE D.NR_SEQ_ATEND_DISP = P_NR_SEQ_ATEND_DISP
       AND D.NR_SEQ_INF_ADIC = PKG_REPORT_DATA.GET_DEVICE_INFO(P_GOAL);

    IF NR <> 0 THEN
      
      SELECT DECODE(COUNT(1), 0, 0, 1)
        INTO NR
        FROM TOF_META_ATEND M
       WHERE M.NR_ATENDIMENTO = P_NR_ATEND
         AND M.NR_SEQ_META = PKG_REPORT_DATA.GET_CARE_GOAL(P_GOAL)
         AND IE_STATUS = 'A'
         AND TRUNC(PKG_ATEND_PAC_UNID.GET_ESTAB_DT(P_ESTAB, M.DT_INICIO), 'DD') = P_DT_ATEND;
    
    END IF; 
    
    RETURN NR;
    
  END GET_GOAL_COMPLIANCE;  
  --
  PROCEDURE PRE_PROCESS(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_AGE IN NUMBER,
    P_CLINICAL_UNIT IN VARCHAR2,
    P_ELEMENTS IN VARCHAR2) IS
    --
    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
    --
    CURSOR C_BSI_RESUME IS
      SELECT M.NR_ATENDIMENTO,
             M.NR_SEQUENCIA NR_SEQ_ATEND_DISP,
             M.CD_ESTABELECIMENTO,
             M.CD_SETOR_ATENDIMENTO,             
             M.DT_INSTALACAO,
             M.DT_RETIRADA,
             M.DT_INSTALACAO_ESTAB,
             M.DT_RETIRADA_ESTAB,
             M.DT_ENTRADA_SETOR,
             M.DT_SAIDA_SETOR,
             GREATEST(M.DT_INSTALACAO_ESTAB, M.DT_ENTRADA_SETOR) DT_INSTALACAO_SETOR,
             LEAST(NVL(M.DT_RETIRADA_ESTAB, M.DT_SAIDA_SETOR),
                   NVL(M.DT_SAIDA_SETOR, DT_RETIRADA_ESTAB)) DT_RETIRADA_SETOR
        FROM (SELECT DISTINCT UN.CD_ESTABELECIMENTO,
                              UN.CD_SETOR_ATENDIMENTO,
                              UN.NR_ATENDIMENTO,
                              APD.NR_SEQUENCIA,
                              APD.DT_INSTALACAO,
                              APD.DT_RETIRADA,
                              PKG_ATEND_PAC_UNID.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_INSTALACAO) DT_INSTALACAO_ESTAB,
                              PKG_ATEND_PAC_UNID.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_RETIRADA) DT_RETIRADA_ESTAB,
                              PKG_ATEND_PAC_UNID.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, UN.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                              PKG_ATEND_PAC_UNID.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, UN.DT_SAIDA) DT_SAIDA_SETOR                               
                FROM TABLE(GET_TAB_ATEND_UNIDADE(P_START_DATE, P_END_DATE, P_AGE, P_CLINICAL_UNIT)) UN,
                     ATEND_PAC_DISPOSITIVO  APD                                       
               WHERE UN.NR_ATENDIMENTO = APD.NR_ATENDIMENTO
                 AND EXISTS (SELECT 1 FROM DISPOSITIVO D WHERE D.NR_SEQUENCIA = APD.NR_SEQ_DISPOSITIVO AND D.IE_CLASSIF_DISP_NISS = 'CCIP')
                 AND OBTER_IDADE_PF(OBTER_CD_PES_FIS_ATEND(APD.NR_ATENDIMENTO), APD.DT_INSTALACAO, 'A') >= P_AGE
                 AND TRUNC(PKG_ATEND_PAC_UNID.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_INSTALACAO), 'DD') <= P_END_DATE
                 AND NVL(TRUNC(PKG_ATEND_PAC_UNID.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_RETIRADA), 'DD'), P_END_DATE) >= P_START_DATE
                 AND APD.DT_INSTALACAO <= NVL(UN.DT_SAIDA, APD.DT_INSTALACAO)                                                                    
                 AND UN.DT_ENTRADA_UNIDADE <= NVL(APD.DT_RETIRADA, UN.DT_ENTRADA_UNIDADE)
                 AND EXISTS(SELECT 1
                              FROM GQA_PROTOCOLO_PAC P
                             WHERE P.NR_ATENDIMENTO = APD.NR_ATENDIMENTO
                               AND P.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(6)
                               AND P.IE_SITUACAO = 'A'
                               AND P.DT_INCLUSAO <= NVL(APD.DT_RETIRADA, P.DT_INCLUSAO)
                               AND APD.DT_INSTALACAO <= NVL(P.DT_TERMINO, APD.DT_INSTALACAO))) M;
    --
    TYPE T_BSI_RESUME IS TABLE OF C_BSI_RESUME%ROWTYPE;
    R_BSI_RESUME T_BSI_RESUME;
    --
    CURSOR C_PRE_PROCESS IS
      SELECT X.DT_ATEND,
             X.NR_ATENDIMENTO,
             X.CD_ESTABELECIMENTO,
             X.CD_SETOR_ATENDIMENTO,
             X.CENTRAL_LINE_INSERTIONS,
             1 CENTRAL_LINE_DAYS,
             X.CENTRAL_LINE_COMPLIANT_DAYS,
             DECODE(X.INFECTIONS, 0, 0, 1) INFECTION,
             X.OCS_COUNT,
             X.OCS_COMPLIANCE,
             X.PATIENT_BARRIER_COUNT,
             X.PATIENT_BARRIER_COMPLIANCE,
             X.PROVIDER_BARRIER_COUNT,
             X.PROVIDER_BARRIER_COMPLIANCE,
             X.ANTISEPTIC_SKIN_COUNT,
             X.ANTISEPTIC_SKIN_COMPLIANCE,
             X.HAND_HYGIENE_COUNT,
             X.HAND_HYGIENE_COMPLIANCE         
        FROM (SELECT A.DT_ATEND,
                     A.NR_ATENDIMENTO,
                     A.CD_ESTABELECIMENTO,
                     A.CD_SETOR_ATENDIMENTO,
                     SUM(A.CENTRAL_LINE_INSERTIONS) CENTRAL_LINE_INSERTIONS,
                     DECODE(SUM(A.CENTRAL_LINE_INSERTIONS), 0, 0, ROUND(SUM(A.CENTRAL_LINE_COMPLIANT_DAYS) / SUM(A.CENTRAL_LINE_INSERTIONS), 2)) CENTRAL_LINE_COMPLIANT_DAYS,
                     SUM(A.INFECTIONS) INFECTIONS,
                     SUM(A.OCS_COUNT) OCS_COUNT,
                     SUM(A.OCS_COMPLIANCE) OCS_COMPLIANCE,
                     SUM(A.PATIENT_BARRIER_COUNT) PATIENT_BARRIER_COUNT,
                     SUM(A.PATIENT_BARRIER_COMPLIANCE) PATIENT_BARRIER_COMPLIANCE,
                     SUM(A.PROVIDER_BARRIER_COUNT) PROVIDER_BARRIER_COUNT,
                     SUM(A.PROVIDER_BARRIER_COMPLIANCE) PROVIDER_BARRIER_COMPLIANCE,
                     SUM(A.ANTISEPTIC_SKIN_COUNT) ANTISEPTIC_SKIN_COUNT,
                     SUM(A.ANTISEPTIC_SKIN_COMPLIANCE) ANTISEPTIC_SKIN_COMPLIANCE,
                     SUM(A.HAND_HYGIENE_COUNT) HAND_HYGIENE_COUNT,
                     SUM(A.HAND_HYGIENE_COMPLIANCE) HAND_HYGIENE_COMPLIANCE
                FROM (SELECT C.DT_ATEND,
                             C.NR_ATENDIMENTO,
                             C.NR_SEQ_ATEND_DISP,
                             C.CD_ESTABELECIMENTO,
                             C.CD_SETOR_ATENDIMENTO,
                             C.DT_INSTALACAO,
                             C.DT_RETIRADA,
                             C.DT_INSTALACAO_ESTAB,
                             C.DT_RETIRADA_ESTAB,
                             C.DT_ENTRADA_SETOR,
                             C.DT_SAIDA_SETOR,
                             C.DT_ENTRADA_ATEND,
                             C.DT_SAIDA_ATEND,
                             C.CENTRAL_LINE_INSERTIONS,
                             DECODE(C.OCS_COMPLIANCE + 
                                    C.PATIENT_BARRIER_COMPLIANCE + 
                                    C.PROVIDER_BARRIER_COMPLIANCE + 
                                    C.ANTISEPTIC_SKIN_COMPLIANCE + 
                                    C.HAND_HYGIENE_COMPLIANCE,
                                    5,
                                    1,
                                    0) CENTRAL_LINE_COMPLIANT_DAYS,
                             C.OCS_COUNT,                             
                             C.OCS_COMPLIANCE,
                             C.PATIENT_BARRIER_COUNT,
                             C.PATIENT_BARRIER_COMPLIANCE,
                             C.PROVIDER_BARRIER_COUNT,
                             C.PROVIDER_BARRIER_COMPLIANCE,
                             C.ANTISEPTIC_SKIN_COUNT,
                             C.ANTISEPTIC_SKIN_COMPLIANCE,
                             C.HAND_HYGIENE_COUNT,
                             C.HAND_HYGIENE_COMPLIANCE,
                             C.INFECTIONS
                        FROM (SELECT T.DT_ATEND,
                                     T.NR_ATENDIMENTO,
                                     T.NR_SEQ_ATEND_DISP,
                                     T.CD_ESTABELECIMENTO,
                                     T.CD_SETOR_ATENDIMENTO,
                                     T.DT_INSTALACAO,
                                     T.DT_RETIRADA,
                                     T.DT_INSTALACAO_ESTAB,
                                     T.DT_RETIRADA_ESTAB,
                                     T.DT_ENTRADA_SETOR,
                                     T.DT_SAIDA_SETOR,
                                     T.DT_ENTRADA_ATEND,
                                     T.DT_SAIDA_ATEND,
                                     DECODE(TRUNC(T.DT_INSTALACAO_SETOR, 'DD'), T.DT_ATEND, 1, 0) CENTRAL_LINE_INSERTIONS,
                                     DECODE(P_ELEMENTS, 
                                            'N', 
                                            0,
                                            1) OCS_COUNT,                            
                                     DECODE(P_ELEMENTS, 
                                            'N', 
                                            0,
                                            -- OPTIMAL_CATHETER_COMPLIANCE 
                                            PKG_BSI_REPORT.GET_GOAL_COMPLIANCE(T.NR_ATENDIMENTO, T.NR_SEQ_ATEND_DISP, T.CD_ESTABELECIMENTO, T.DT_ATEND, 1)) OCS_COMPLIANCE,
                                     DECODE(P_ELEMENTS, 
                                            'N', 
                                            0,
                                            1) PATIENT_BARRIER_COUNT,
                                     DECODE(P_ELEMENTS, 
                                            'N', 
                                            0,
                                            -- PATIENT_BARRIER_COMPLIANCE
                                            PKG_BSI_REPORT.GET_GOAL_COMPLIANCE(T.NR_ATENDIMENTO, T.NR_SEQ_ATEND_DISP, T.CD_ESTABELECIMENTO, T.DT_ATEND, 2)) PATIENT_BARRIER_COMPLIANCE,
                                     DECODE(P_ELEMENTS,
                                            'N',
                                            0,
                                            1) PROVIDER_BARRIER_COUNT,
                                     DECODE(P_ELEMENTS,
                                            'N',
                                            0,
                                            -- PROVIDER_BARRIER_COMPLIANCE
                                            PKG_BSI_REPORT.GET_GOAL_COMPLIANCE(T.NR_ATENDIMENTO, T.NR_SEQ_ATEND_DISP, T.CD_ESTABELECIMENTO, T.DT_ATEND, 4)) PROVIDER_BARRIER_COMPLIANCE,
                                     DECODE(P_ELEMENTS,
                                            'N',
                                            0,
                                            1) ANTISEPTIC_SKIN_COUNT,
                                     DECODE(P_ELEMENTS,
                                            'N',
                                            0,
                                            -- ANTISEPTIC_SKIN_COMPLIANCE
                                            PKG_BSI_REPORT.GET_GOAL_COMPLIANCE(T.NR_ATENDIMENTO, T.NR_SEQ_ATEND_DISP, T.CD_ESTABELECIMENTO, T.DT_ATEND, 3)) ANTISEPTIC_SKIN_COMPLIANCE,
                                     DECODE(P_ELEMENTS,
                                            'N',
                                            '0',
                                            1) HAND_HYGIENE_COUNT,
                                     DECODE(P_ELEMENTS,
                                            'N',
                                            0,
                                            -- HAND_HYGIENE_COMPLIANCE
                                            PKG_BSI_REPORT.GET_GOAL_COMPLIANCE(T.NR_ATENDIMENTO, T.NR_SEQ_ATEND_DISP, T.CD_ESTABELECIMENTO, T.DT_ATEND, 5)) HAND_HYGIENE_COMPLIANCE,
                                     (SELECT COUNT(1)
                                        FROM CIH_FICHA_OCORRENCIA O,
                                             CIH_LOCAL_INFECCAO I
                                       WHERE O.NR_ATENDIMENTO = T.NR_ATENDIMENTO
                                         AND O.NR_FICHA_OCORRENCIA = I.NR_FICHA_OCORRENCIA
                                         AND I.IE_INFECCAO_SECUNDARIA = 'S'
                                         AND PKG_ATEND_PAC_UNID.GET_ESTAB_DT(T.CD_ESTABELECIMENTO, 
                                                                             I.DT_INFECCAO) 
                                             BETWEEN T.DT_ENTRADA_ATEND AND T.DT_SAIDA_ATEND + 2) INFECTIONS
                                FROM (WITH DT_ATEND AS (SELECT level, P_END_DATE - level + 1 dt_atend
                                                          FROM DUAL
                                                       CONNECT BY LEVEL <= P_END_DATE - P_START_DATE + 1)                              
                                      SELECT DT.DT_ATEND,
                                             N.NR_ATENDIMENTO,
                                             N.NR_SEQ_ATEND_DISP,
                                             N.CD_ESTABELECIMENTO,
                                             N.CD_SETOR_ATENDIMENTO,
                                             N.DT_INSTALACAO,
                                             N.DT_RETIRADA,
                                             N.DT_INSTALACAO_ESTAB,
                                             N.DT_RETIRADA_ESTAB,
                                             N.DT_ENTRADA_SETOR,
                                             N.DT_SAIDA_SETOR,
                                             N.DT_INSTALACAO_SETOR,
                                             N.DT_RETIRADA_SETOR,
                                             GREATEST(N.DT_INSTALACAO_SETOR, DT.DT_ATEND) DT_ENTRADA_ATEND,
                                             LEAST(NVL(N.DT_RETIRADA_SETOR, 
                                                       LEAST(DT.DT_ATEND + 1, 
                                                             PKG_ATEND_PAC_UNID.GET_ESTAB_DT(N.CD_ESTABELECIMENTO, SYSDATE))), 
                                                   LEAST(DT.DT_ATEND + 1, 
                                                         PKG_ATEND_PAC_UNID.GET_ESTAB_DT(N.CD_ESTABELECIMENTO, SYSDATE))) DT_SAIDA_ATEND
                                        FROM BSI_REPORT_RESUME N,
                                             DT_ATEND DT  
                                       WHERE DT.DT_ATEND BETWEEN TRUNC(N.DT_INSTALACAO_SETOR, 'DD') 
                                                             AND NVL(TRUNC(N.DT_RETIRADA_SETOR, 'DD'), P_END_DATE)) T) C) A
               GROUP BY A.DT_ATEND,
                        A.NR_ATENDIMENTO,
                        A.CD_ESTABELECIMENTO,
                        A.CD_SETOR_ATENDIMENTO) X;
    --
    TYPE T_PRE_PROCESS IS TABLE OF C_PRE_PROCESS%ROWTYPE;
    R_PRE_PROCESS T_PRE_PROCESS;                      
    --     
  BEGIN
    --
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    --
    OPEN C_BSI_RESUME;
    LOOP
      FETCH C_BSI_RESUME BULK COLLECT INTO R_BSI_RESUME LIMIT 1000;
        BEGIN
          FOR i IN R_BSI_RESUME.FIRST..R_BSI_RESUME.LAST LOOP
            INSERT INTO BSI_REPORT_RESUME (
              nr_atendimento, 
              nr_seq_atend_disp,
              cd_estabelecimento, 
              cd_setor_atendimento,           
              dt_instalacao, 
              dt_retirada, 
              dt_instalacao_estab, 
              dt_retirada_estab, 
              dt_entrada_setor, 
              dt_saida_setor, 
              dt_instalacao_setor, 
              dt_retirada_setor,
              nm_usuario)
            VALUES (
              R_BSI_RESUME(i).NR_ATENDIMENTO, --nr_atendimento, 
              R_BSI_RESUME(i).NR_SEQ_ATEND_DISP, --nr_seq_atend_disp, 
              R_BSI_RESUME(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
              R_BSI_RESUME(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
              R_BSI_RESUME(i).DT_INSTALACAO, --dt_instalacao, 
              R_BSI_RESUME(i).DT_RETIRADA, --dt_retirada, 
              R_BSI_RESUME(i).DT_INSTALACAO_ESTAB, --dt_instalacao_estab, 
              R_BSI_RESUME(i).DT_RETIRADA_ESTAB, --dt_retirada_estab, 
              R_BSI_RESUME(i).DT_ENTRADA_SETOR, --dt_entrada_setor, 
              R_BSI_RESUME(i).DT_SAIDA_SETOR, --dt_saida_setor, 
              R_BSI_RESUME(i).DT_INSTALACAO_SETOR, --dt_instalacao_setor, 
              R_BSI_RESUME(i).DT_RETIRADA_SETOR, --dt_retirada_setor,
              NM_USUARIO_W);  --nm_usuario
          END LOOP;
          COMMIT;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
      EXIT WHEN C_BSI_RESUME%NOTFOUND; 
    END LOOP;
    CLOSE C_BSI_RESUME; 
    COMMIT;
    
    OPEN C_PRE_PROCESS;
    LOOP
      FETCH C_PRE_PROCESS BULK COLLECT INTO R_PRE_PROCESS LIMIT 1000;
        BEGIN
          FOR i IN R_PRE_PROCESS.FIRST..R_PRE_PROCESS.LAST LOOP
            INSERT INTO BSI_REPORT (
              dt_atendimento, 
              month, 
              month_name, 
              year, 
              cd_estabelecimento, 
              cd_setor_atend, 
              nr_atendimento,
              central_line_insertions,
              central_line_days,
              central_line_compliant_days,
              infection,
              ocs_count, 
              ocs_compliance, 
              patient_barrier_count, 
              patient_barrier_compliance, 
              provider_barrier_count, 
              provider_barrier_compliance, 
              antiseptic_skin_count, 
              antiseptic_skin_compliance, 
              hand_hygiene_count, 
              hand_hygiene_compliance,
              nm_usuario) 
            VALUES (
              R_PRE_PROCESS(i).DT_ATEND, --dt_atendimento, 
              TO_CHAR(R_PRE_PROCESS(i).DT_ATEND, 'MM'), --month,
              TRIM(TO_CHAR(R_PRE_PROCESS(i).DT_ATEND, 'Month', 'NLS_DATE_LANGUAGE = American')), --month_name,           
              TO_CHAR(R_PRE_PROCESS(i).DT_ATEND, 'YYYY'), --year, 
              R_PRE_PROCESS(i).CD_ESTABELECIMENTO, --cd_estabelecimento,
              R_PRE_PROCESS(i).CD_SETOR_ATENDIMENTO, --cd_setor_atend,
              R_PRE_PROCESS(i).NR_ATENDIMENTO, --nr_atendimento,
              R_PRE_PROCESS(i).CENTRAL_LINE_INSERTIONS, --central_line_insertions,  
              R_PRE_PROCESS(i).CENTRAL_LINE_DAYS, --central_line_days,
              R_PRE_PROCESS(i).CENTRAL_LINE_COMPLIANT_DAYS, --central_line_compliant_days,
              R_PRE_PROCESS(i).INFECTION, --infection,
              R_PRE_PROCESS(i).OCS_COUNT, --ocs_count, 
              R_PRE_PROCESS(i).OCS_COMPLIANCE, --ocs_compliance, 
              R_PRE_PROCESS(i).PATIENT_BARRIER_COUNT, --patient_barrier_count, 
              R_PRE_PROCESS(i).PATIENT_BARRIER_COMPLIANCE, --patient_barrier_compliance, 
              R_PRE_PROCESS(i).PROVIDER_BARRIER_COUNT, --provider_barrier_count, 
              R_PRE_PROCESS(i).PROVIDER_BARRIER_COMPLIANCE, --provider_barrier_compliance, 
              R_PRE_PROCESS(i).ANTISEPTIC_SKIN_COUNT, --antiseptic_skin_count, 
              R_PRE_PROCESS(i).ANTISEPTIC_SKIN_COMPLIANCE, --antiseptic_skin_compliance, 
              R_PRE_PROCESS(i).HAND_HYGIENE_COUNT, --hand_hygiene_count, 
              R_PRE_PROCESS(i).HAND_HYGIENE_COMPLIANCE, --hand_hygiene_compliance,
              NM_USUARIO_W); --nm_usuario
          END LOOP;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
      EXIT WHEN C_PRE_PROCESS%NOTFOUND; 
    END LOOP;
    CLOSE C_PRE_PROCESS;        
    COMMIT;

  END PRE_PROCESS;
  -- 
  PROCEDURE RUN(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_AGE IN NUMBER,
    P_CLINICAL_UNIT IN VARCHAR2,
    P_ELEMENTS IN VARCHAR2) IS
  BEGIN  
    TABLE_CLEANING;
    PRE_PROCESS(P_START_DATE, LEAST(P_END_DATE, TRUNC(SYSDATE, 'DD')), P_AGE, P_CLINICAL_UNIT, P_ELEMENTS);
  END RUN;
  --
END PKG_BSI_REPORT;
/
