create or replace
package pfcs_file_pck is

	procedure pfcs_generate;

	procedure pfcs_generate_panel;

	procedure pfcs_generate_panel_detail;

	procedure pfcs_generate_detail_patient;

	procedure pfcs_generate_detail_bed;

	procedure pfcs_generate_detail_employee;

	procedure pfcs_generate_detail_exam_lab;

	procedure pfcs_generate_lote;

end pfcs_file_pck;
/
create or replace package body pfcs_file_pck is

	ds_time_w		varchar2(255);
	cd_enterprise_w		pfcs_enterprise.cd_enterprise%type;

	procedure pfcs_generate is

	begin

	ds_time_w	:= to_char(sysdate,'ddmmyyyyhh24miss');

	select	max(a.cd_enterprise)
	into	cd_enterprise_w
	from	pfcs_enterprise a;

	pfcs_generate_panel;
	pfcs_generate_panel_detail;
	pfcs_generate_detail_patient;
	pfcs_generate_detail_bed;
	pfcs_generate_detail_employee;
	pfcs_generate_detail_exam_lab;
	pfcs_generate_lote;

	end pfcs_generate;

	procedure pfcs_generate_panel is

	ds_err_w				varchar2(255);
	ds_directory_w				varchar2(255);
	ds_file_w				utl_file.file_type;
	nm_file_w				varchar2(255);
	ds_columns_w 				varchar2(4000) :=  	'NR_SEQUENCIA;DT_ATUALIZACAO;NM_USUARIO;DT_ATUALIZACAO_NREC;NM_USUARIO_NREC;CD_EXP_INDICATOR;IE_SITUATION;VL_INDICATOR;DS_REFERENCE_VALUE;' ||
									'VL_INDICATOR_AUX;VL_INDICATOR_HELP;VL_INDICATOR_ASSIST;VL_INDICATOR_COLLAB;CD_REFERENCE_VALUE;CD_REFERENCE_AUX;DS_REFERENCE_AUX;' ||
									'NR_SEQ_INDICATOR;NR_SEQ_OPERATIONAL_LEVEL';

	cursor c_row is
		select	chr(39) || a.nr_sequencia		|| chr(39) || ';' ||
			chr(39) || to_char(a.dt_atualizacao,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || a.nm_usuario			|| chr(39) || ';' ||
			chr(39) || to_char(a.dt_atualizacao_nrec,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || a.nm_usuario_nrec		|| chr(39) || ';' ||
			chr(39) || a.cd_exp_indicator		|| chr(39) || ';' ||
			chr(39) || a.ie_situation		|| chr(39) || ';' ||
			chr(39) || a.vl_indicator		|| chr(39) || ';' ||
			chr(39) || a.ds_reference_value		|| chr(39) || ';' ||
			chr(39) || a.vl_indicator_aux		|| chr(39) || ';' ||
			chr(39) || a.vl_indicator_help		|| chr(39) || ';' ||
			chr(39) || a.vl_indicator_assist	|| chr(39) || ';' ||
			chr(39) || a.vl_indicator_collab	|| chr(39) || ';' ||
			chr(39) || a.cd_reference_value 	|| chr(39) || ';' ||
			chr(39) || a.cd_reference_aux	 	|| chr(39) || ';' ||
			chr(39) || a.ds_reference_aux 		|| chr(39) || ';' ||
			chr(39) || a.nr_seq_indicator		|| chr(39) || ';' ||
			chr(39) || b.cd_cgc			|| chr(39) ds_row
		from	pfcs_panel		a,
			estabelecimento		b
		where	b.cd_estabelecimento	= a.nr_seq_operational_level
		and	a.ie_situation		= 'A'
		and	a.nr_seq_indicator in (77,78,79,80,83,84,85,86,87,88);

	begin

	nm_file_w	:= 'PFCS_PANEL_' || ds_time_w || '.csv';

	obter_evento_utl_file(34, null, ds_directory_w, ds_err_w);

	ds_file_w := utl_file.fopen(ds_directory_w, nm_file_w, 'W');

	utl_file.put_line(ds_file_w, ds_columns_w);
	utl_file.fflush(ds_file_w);
	for c_row_w in c_row loop
		begin
		utl_file.put_line(ds_file_w,c_row_w.ds_row);
		utl_file.fflush(ds_file_w);
		end;
	end loop;
    utl_file.fclose(ds_file_w);
    EXCEPTION
        WHEN OTHERS
        THEN
          IF UTL_FILE.is_open (ds_file_w)
          THEN
             UTL_FILE.fclose (ds_file_w);
          END IF;
	end pfcs_generate_panel;

	procedure pfcs_generate_panel_detail is

	ds_err_w				varchar2(255);
	ds_directory_w				varchar2(255);
	ds_file_w				utl_file.file_type;
	nm_file_w				varchar2(255);
	ds_columns_w 				varchar2(4000) :=  	'NR_SEQUENCIA;DT_ATUALIZACAO;NM_USUARIO;DT_ATUALIZACAO_NREC;NM_USUARIO_NREC;NR_SEQ_INDICATOR;' ||
									'VL_INDICATOR;IE_SITUATION;NR_SEQ_PANEL;NR_SEQ_OPERATIONAL_LEVEL;NR_SEQ_TACTICAL_LEVEL';

	cursor c_row is
		select	chr(39) || a.nr_sequencia		|| chr(39) || ';' ||
			chr(39) || to_char(a.dt_atualizacao,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || a.nm_usuario			|| chr(39) || ';' ||
			chr(39) || to_char(a.dt_atualizacao_nrec,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || a.nm_usuario_nrec		|| chr(39) || ';' ||
			chr(39) || a.nr_seq_indicator		|| chr(39) || ';' ||
			chr(39) || a.vl_indicator		|| chr(39) || ';' ||
			chr(39) || a.ie_situation		|| chr(39) || ';' ||
			chr(39) || a.nr_seq_panel		|| chr(39) || ';' ||
			chr(39) || b.cd_cgc			|| chr(39) || ';' ||
			chr(39) || cd_enterprise_w 		|| chr(39) ds_row
		from	pfcs_panel_detail	a,
			estabelecimento		b
		where	b.cd_estabelecimento	= a.nr_seq_operational_level
		and	a.ie_situation		= 'A'
		and	a.nr_seq_indicator in (77,78,79,80,83,84,85,86,87,88);
	begin

	nm_file_w	:= 'PFCS_PANEL_DETAIL_' || ds_time_w || '.csv';

	obter_evento_utl_file(34, null, ds_directory_w, ds_err_w);

	ds_file_w := utl_file.fopen(ds_directory_w, nm_file_w, 'W');

	utl_file.put_line(ds_file_w, ds_columns_w);
	utl_file.fflush(ds_file_w);
	for c_row_w in c_row loop
		begin
		utl_file.put_line(ds_file_w,c_row_w.ds_row);
		utl_file.fflush(ds_file_w);
		end;
	end loop;

    utl_file.fclose(ds_file_w);
    EXCEPTION
        WHEN OTHERS
        THEN
          IF UTL_FILE.is_open (ds_file_w)
          THEN
             UTL_FILE.fclose (ds_file_w);
          END IF;
	end pfcs_generate_panel_detail;

	procedure pfcs_generate_detail_patient is

	ds_err_w				varchar2(255);
	ds_directory_w				varchar2(255);
	ds_file_w				utl_file.file_type;
	nm_file_w				varchar2(255);
	ds_columns_w 				varchar2(4000) :=  	'NR_SEQUENCIA;DT_ATUALIZACAO;NM_USUARIO;DT_ATUALIZACAO_NREC;NM_USUARIO_NREC;ID_PATIENT;NM_PATIENT;' ||
									'NR_SEQ_DETAIL;QT_TIME_AWAITING_BED;DS_UNIT_PA;DS_CLASSIFICATION;QT_TIME_BOX_PA;DT_ENTRANCE;NR_ENCOUNTER;' ||
									'QT_TIME_QUEUE;QT_TIME_RECEPTION;QT_TIME_TRIAGE;QT_TIME_CALL;QT_TIME_EXAM;QT_TIME_MEDIC;QT_TIME_OUTCOME;' ||
									'QT_TIME_TOTAL_PA;QT_TIME_TOTAL_HOSPITALIZATION;QT_TIME_AWAIT_AFTER_DISCHARGE;NR_LAST_ENCOUNTER;DT_LAST_ENTRANCE;' ||
									'DS_GENDER;DT_BIRTHDATE;DS_PRIMARY_DIAGNOSIS;DS_DNR_STATUS;QT_TIME_TELEMETRY;DS_REQUESTED_UNIT;DS_BED_ASSIGNED;' ||
									'QT_RED_ALARMS;QT_YELLOW_ALARMS;DS_SERVICE_LINE;QT_TIME_STAGE_PA;IE_STAGE_PA;IE_OVER_THRESHOLD;CD_GROUP_PA;' ||
									'DS_GROUP_PA;IE_CLINIC;DS_CLINIC;DT_EXPECTED_DISCHARGE;DT_LAST_DISCHARGE;DS_PHYSICIAN;DS_REASON_DISCHARGE;CD_REASON_DISCHARGE;' ||
									'QT_TIME_SCHEDULE;CD_CLASSIFICATION;CD_STAGE_PA;DS_STAGE_PA;DS_CURRENT_LOCATION;IE_SYMPTOMS;DS_SYMPTOMS;DS_AGE_RANGE;' ||
									'NM_PHYSICIAN;IE_CONFIRMED;NR_ENCOUNTER_VARCHAR';

	cursor c_row is
		select	chr(39) || nr_sequencia				|| chr(39) || ';' ||
			chr(39) || to_char(dt_atualizacao,'dd/mm/yy hh24:mi:ss')	|| chr(39) || ';' ||
			chr(39) || nm_usuario				|| chr(39) || ';' ||
			chr(39) || to_char(dt_atualizacao_nrec,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || nm_usuario_nrec			|| chr(39) || ';' ||
			chr(39) || id_patient				|| chr(39) || ';' ||
			chr(39) /*|| nm_patient */			|| chr(39) || ';' ||
			chr(39) || nr_seq_detail			|| chr(39) || ';' ||
			chr(39) || qt_time_awaiting_bed			|| chr(39) || ';' ||
			chr(39) || ds_unit_pa				|| chr(39) || ';' ||
			chr(39) || ds_classification			|| chr(39) || ';' ||
			chr(39) || qt_time_box_pa			|| chr(39) || ';' ||
			chr(39) || to_char(dt_entrance,'dd/mm/yy hh24:mi:ss')	|| chr(39) || ';' ||
			chr(39) || nr_encounter				|| chr(39) || ';' ||
			chr(39) || qt_time_queue			|| chr(39) || ';' ||
			chr(39) || qt_time_reception			|| chr(39) || ';' ||
			chr(39) || qt_time_triage			|| chr(39) || ';' ||
			chr(39) || qt_time_call				|| chr(39) || ';' ||
			chr(39) || qt_time_exam				|| chr(39) || ';' ||
			chr(39) || qt_time_medic			|| chr(39) || ';' ||
			chr(39) || qt_time_outcome			|| chr(39) || ';' ||
			chr(39) || qt_time_total_pa			|| chr(39) || ';' ||
			chr(39) || qt_time_total_hospitalization	|| chr(39) || ';' ||
			chr(39) || qt_time_await_after_discharge	|| chr(39) || ';' ||
			chr(39) || nr_last_encounter			|| chr(39) || ';' ||
			chr(39) || to_char(dt_last_entrance,'dd/mm/yy hh24:mi:ss')	|| chr(39) || ';' ||
			chr(39) || ds_gender				|| chr(39) || ';' ||
			chr(39) /*|| to_char(dt_birthdate,'dd/mm/yy hh24:mi:ss')*/	|| chr(39) || ';' ||
			chr(39) || ds_primary_diagnosis			|| chr(39) || ';' ||
			chr(39) || ds_dnr_status			|| chr(39) || ';' ||
			chr(39) || qt_time_telemetry			|| chr(39) || ';' ||
			chr(39) /*|| ds_requested_unit */		|| chr(39) || ';' ||
			chr(39) || ds_bed_assigned			|| chr(39) || ';' ||
			chr(39) || qt_red_alarms			|| chr(39) || ';' ||
			chr(39) || qt_yellow_alarms			|| chr(39) || ';' ||
			chr(39) || ds_service_line			|| chr(39) || ';' ||
			chr(39) || qt_time_stage_pa			|| chr(39) || ';' ||
			chr(39) || ie_stage_pa				|| chr(39) || ';' ||
			chr(39) || ie_over_threshold			|| chr(39) || ';' ||
			chr(39) || cd_group_pa				|| chr(39) || ';' ||
			chr(39) || ds_group_pa				|| chr(39) || ';' ||
			chr(39) || ie_clinic				|| chr(39) || ';' ||
			chr(39) || ds_clinic				|| chr(39) || ';' ||
			chr(39) || to_char(dt_expected_discharge,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || to_char(dt_last_discharge,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) /*|| ds_physician		*/		|| chr(39) || ';' ||
			chr(39) || ds_reason_discharge			|| chr(39) || ';' ||
			chr(39) || cd_reason_discharge			|| chr(39) || ';' ||
			chr(39) || qt_time_schedule			|| chr(39) || ';' ||
			chr(39) || cd_classification			|| chr(39) || ';' ||
			chr(39) || cd_stage_pa				|| chr(39) || ';' ||
			chr(39) || ds_stage_pa				|| chr(39) || ';' ||
			chr(39) || ds_current_location 			|| chr(39) || ';' ||
			chr(39) || ie_symptoms 				|| chr(39) || ';' ||
			chr(39) || ds_symptoms 				|| chr(39) || ';' ||
			chr(39) || ds_age_range 			|| chr(39) || ';' ||
			chr(39) /*|| nm_physician */			|| chr(39) || ';' ||
			chr(39) || ie_confirmed 			|| chr(39) || ';' ||
			chr(39) || nr_encounter_varchar			|| chr(39) ds_row
		from	pfcs_detail_patient a
		where	exists (select	1
				from	pfcs_panel_detail x
				where	x.nr_sequencia	= a.nr_seq_detail
				and	x.ie_situation	= 'A'
				and	x.nr_seq_indicator in (77,78,79,80,83,84,85,86,87,88));

	begin

	nm_file_w	:= 'PFCS_DETAIL_PATIENT_' || ds_time_w || '.csv';

	obter_evento_utl_file(34, null, ds_directory_w, ds_err_w);

	ds_file_w := utl_file.fopen(ds_directory_w, nm_file_w, 'W');

	utl_file.put_line(ds_file_w, ds_columns_w);
	utl_file.fflush(ds_file_w);
	for c_row_w in c_row loop
		begin
		utl_file.put_line(ds_file_w,c_row_w.ds_row);
		utl_file.fflush(ds_file_w);
		end;
	end loop;

    utl_file.fclose(ds_file_w);
    EXCEPTION
        WHEN OTHERS
        THEN
          IF UTL_FILE.is_open (ds_file_w)
          THEN
             UTL_FILE.fclose (ds_file_w);
          END IF;

	end pfcs_generate_detail_patient;

	procedure pfcs_generate_detail_bed is


	ds_err_w				varchar2(255);
	ds_directory_w				varchar2(255);
	ds_file_w				utl_file.file_type;
	nm_file_w				varchar2(255);
	ds_columns_w 				varchar2(4000) :=  	'CD_BED;CD_DEPARTMENT;CD_STATUS;CD_TYPE;DS_CLASSIFICATION;DS_DEPARTMENT;DS_LOCATION;DS_REASON_BLOCKED;DS_SANITIZATION_SERVICE;' ||
									'DS_STATUS;DS_TYPE;DT_ATUALIZACAO;DT_ATUALIZACAO_NREC;DT_ENTRY_UNIT;IE_STATUS;IE_TEMPORARY;IE_TYPE;NM_USUARIO;NM_USUARIO_NREC;' ||
									'NR_SEQ_CLASSIF;NR_SEQ_DETAIL;NR_SEQUENCIA;QT_TIME_AVERAGE_AVAILABLE;QT_TIME_BLOCKED;QT_TIME_OCUP_AFTER_DISCHARGE;QT_TIME_SANITIZATION;' ||
									'IE_RESPIRATOR;IE_CLASSIFICATION';

	cursor c_row is
		select	chr(39) || cd_bed			|| chr(39) || ';' ||
			chr(39) || cd_department		|| chr(39) || ';' ||
			chr(39) || cd_status			|| chr(39) || ';' ||
			chr(39) || cd_type			|| chr(39) || ';' ||
			chr(39) || ds_classification		|| chr(39) || ';' ||
			chr(39) || ds_department		|| chr(39) || ';' ||
			chr(39) || ds_location			|| chr(39) || ';' ||
			chr(39) || ds_reason_blocked		|| chr(39) || ';' ||
			chr(39) || ds_sanitization_service	|| chr(39) || ';' ||
			chr(39) || ds_status			|| chr(39) || ';' ||
			chr(39) || ds_type			|| chr(39) || ';' ||
			chr(39) || to_char(dt_atualizacao,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || to_char(dt_atualizacao_nrec,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || to_char(dt_entry_unit,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || ie_status			|| chr(39) || ';' ||
			chr(39) || ie_temporary			|| chr(39) || ';' ||
			chr(39) || ie_type			|| chr(39) || ';' ||
			chr(39) || nm_usuario			|| chr(39) || ';' ||
			chr(39) || nm_usuario_nrec		|| chr(39) || ';' ||
			chr(39) || nr_seq_classif		|| chr(39) || ';' ||
			chr(39) || nr_seq_detail		|| chr(39) || ';' ||
			chr(39) || nr_sequencia			|| chr(39) || ';' ||
			chr(39) || qt_time_average_available	|| chr(39) || ';' ||
			chr(39) || qt_time_blocked		|| chr(39) || ';' ||
			chr(39) || qt_time_ocup_after_discharge	|| chr(39) || ';' ||
			chr(39) || qt_time_sanitization 	|| chr(39) || ';' ||
			chr(39) || ie_respirator 		|| chr(39) || ';' ||
			chr(39) || ie_classification 		|| chr(39) ds_row
		from	pfcs_detail_bed a
		where	exists (select	1
				from	pfcs_panel_detail x
				where	x.nr_sequencia	= a.nr_seq_detail
				and	x.ie_situation	= 'A'
				and	x.nr_seq_indicator in (77,78,79,80,83,84,85,86,87,88));

	begin

	nm_file_w	:= 'PFCS_DETAIL_BED_' || ds_time_w || '.csv';

	obter_evento_utl_file(34, null, ds_directory_w, ds_err_w);

	ds_file_w := utl_file.fopen(ds_directory_w, nm_file_w, 'W');

	utl_file.put_line(ds_file_w, ds_columns_w);
	utl_file.fflush(ds_file_w);
	for c_row_w in c_row loop
		begin
		utl_file.put_line(ds_file_w,c_row_w.ds_row);
		utl_file.fflush(ds_file_w);
		end;
	end loop;
    utl_file.fclose(ds_file_w);
    EXCEPTION
        WHEN OTHERS
        THEN
          IF UTL_FILE.is_open (ds_file_w)
          THEN
             UTL_FILE.fclose (ds_file_w);
          END IF;
	end pfcs_generate_detail_bed;

	procedure pfcs_generate_detail_employee is

	ds_err_w				varchar2(255);
	ds_directory_w				varchar2(255);
	ds_file_w				utl_file.file_type;
	nm_file_w				varchar2(255);
	ds_columns_w 				varchar2(4000) :=  	'CD_PERSON;DS_CLINICAL_NOTE;DS_CONSULTATION_DEPT;DS_PERSON;DT_ATUALIZACAO;DT_ATUALIZACAO_NREC;NM_USUARIO;NM_USUARIO_NREC;NR_SEQ_DETAIL;NR_SEQUENCIA;' ||
									'CD_PERSON_COVID;DT_POSS_CONTACT';

	cursor c_row is
		select	chr(39) || cd_person			|| chr(39) || ';' ||
			chr(39) || ds_clinical_note		|| chr(39) || ';' ||
			chr(39) || ds_consultation_dept		|| chr(39) || ';' ||
			chr(39) /*|| ds_person	*/		|| chr(39) || ';' ||
			chr(39) || to_char(dt_atualizacao,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || to_char(dt_atualizacao_nrec,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || nm_usuario			|| chr(39) || ';' ||
			chr(39) || nm_usuario_nrec		|| chr(39) || ';' ||
			chr(39) || nr_seq_detail		|| chr(39) || ';' ||
			chr(39) || nr_sequencia 		|| chr(39) || ';' ||
			chr(39) || cd_person_covid 		|| chr(39) || ';' ||
			chr(39) || to_char(dt_poss_contact,'dd/mm/yy hh24:mi:ss') || chr(39) ds_row
		from	pfcs_detail_employee a
		where	exists (select	1
				from	pfcs_panel_detail x
				where	x.nr_sequencia	= a.nr_seq_detail
				and	x.ie_situation	= 'A'
				and	x.nr_seq_indicator in (77,78,79,80,83,84,85,86,87,88));

	begin

	nm_file_w	:= 'PFCS_DETAIL_EMPLOYEE_' || ds_time_w || '.csv';

	obter_evento_utl_file(34, null, ds_directory_w, ds_err_w);

	ds_file_w := utl_file.fopen(ds_directory_w, nm_file_w, 'W');

	utl_file.put_line(ds_file_w, ds_columns_w);
	utl_file.fflush(ds_file_w);
	for c_row_w in c_row loop
		begin
		utl_file.put_line(ds_file_w,c_row_w.ds_row);
		utl_file.fflush(ds_file_w);
		end;
	end loop;

    utl_file.fclose(ds_file_w);
    EXCEPTION
        WHEN OTHERS
        THEN
          IF UTL_FILE.is_open (ds_file_w)
          THEN
             UTL_FILE.fclose (ds_file_w);
          END IF;

	end pfcs_generate_detail_employee;

	procedure pfcs_generate_detail_exam_lab is


	ds_err_w				varchar2(255);
	ds_directory_w				varchar2(255);
	ds_file_w				utl_file.file_type;
	nm_file_w				varchar2(255);
	ds_columns_w 				varchar2(4000) :=  	'DS_REQUEST_PHYSICIAN;DS_RESULT;DT_APPROVAL;DT_ATUALIZACAO;DT_ATUALIZACAO_NREC;DT_RELEASE_REQUEST;DT_REQUEST;IE_FAST_RESULT_CHECK;' ||
									'IE_STATUS_ATEND;NM_EXAM;NM_USER_APPROVAL;NM_USUARIO;NM_USUARIO_NREC;NR_REQUEST;NR_SEQ_DETAIL;NR_SEQ_EXAME;NR_SEQUENCIA';

	cursor c_row is
		select	chr(39) /*|| ds_request_physician	*/	|| chr(39) || ';' ||
			chr(39) || ds_result			|| chr(39) || ';' ||
			chr(39) || to_char(dt_approval,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || to_char(dt_atualizacao,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || to_char(dt_atualizacao_nrec,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || to_char(dt_release_request,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || to_char(dt_request,'dd/mm/yy hh24:mi:ss') || chr(39) || ';' ||
			chr(39) || ie_fast_result_check		|| chr(39) || ';' ||
			chr(39) || ie_status_atend		|| chr(39) || ';' ||
			chr(39) || nm_exam			|| chr(39) || ';' ||
			chr(39) || nm_user_approval		|| chr(39) || ';' ||
			chr(39) || nm_usuario			|| chr(39) || ';' ||
			chr(39) || nm_usuario_nrec		|| chr(39) || ';' ||
			chr(39) || nr_request			|| chr(39) || ';' ||
			chr(39) || nr_seq_detail		|| chr(39) || ';' ||
			chr(39) || nr_seq_exame			|| chr(39) || ';' ||
			chr(39) || nr_sequencia 		|| chr(39) ds_row
		from	pfcs_detail_exam_lab a
		where	exists (select	1
				from	pfcs_panel_detail x
				where	x.nr_sequencia	= a.nr_seq_detail
				and	x.ie_situation	= 'A'
				and	x.nr_seq_indicator in (77,78,79,80,83,84,85,86,87,88));

	begin

	nm_file_w	:= 'PFCS_DETAIL_EXAM_LAB_' || ds_time_w || '.csv';

	obter_evento_utl_file(34, null, ds_directory_w, ds_err_w);

	ds_file_w := utl_file.fopen(ds_directory_w, nm_file_w, 'W');

	utl_file.put_line(ds_file_w, ds_columns_w);
	utl_file.fflush(ds_file_w);
	for c_row_w in c_row loop
		begin
		utl_file.put_line(ds_file_w,c_row_w.ds_row);
		utl_file.fflush(ds_file_w);
		end;
	end loop;
    utl_file.fclose(ds_file_w);
    EXCEPTION
        WHEN OTHERS
        THEN
          IF UTL_FILE.is_open (ds_file_w)
          THEN
             UTL_FILE.fclose (ds_file_w);
          END IF;
	end pfcs_generate_detail_exam_lab;

	procedure pfcs_generate_lote is

	ds_err_w				varchar2(255);
	ds_directory_w				varchar2(255);
	ds_file_w				utl_file.file_type;
	nm_file_w				varchar2(255);

	begin

	nm_file_w	:= 'PFCS_LOTE_' || ds_time_w || '.lote';

	obter_evento_utl_file(34, null, ds_directory_w, ds_err_w);

	ds_file_w := utl_file.fopen(ds_directory_w, nm_file_w, 'W');

	utl_file.put_line(ds_file_w, 'PFCS_PANEL_' || ds_time_w || '.csv');
	utl_file.put_line(ds_file_w, 'PFCS_PANEL_DETAIL_' || ds_time_w || '.csv');
	utl_file.put_line(ds_file_w, 'PFCS_DETAIL_PATIENT_' || ds_time_w || '.csv');
	utl_file.put_line(ds_file_w, 'PFCS_DETAIL_BED_' || ds_time_w || '.csv');
	utl_file.put_line(ds_file_w, 'PFCS_DETAIL_EMPLOYEE_' || ds_time_w || '.csv');
	utl_file.put_line(ds_file_w, 'PFCS_DETAIL_EXAM_LAB_' || ds_time_w || '.csv');
	utl_file.fflush(ds_file_w);
    utl_file.fclose(ds_file_w);
     EXCEPTION
         WHEN OTHERS
         THEN
           IF UTL_FILE.is_open (ds_file_w)
           THEN
              UTL_FILE.fclose (ds_file_w);
           END IF;
	end pfcs_generate_lote;

end pfcs_file_pck;
/