create or replace package tws_usuario_hpms_pck as

procedure 	gerar_usuario_beneficiario(	nr_seq_segurado_web_p 	pls_segurado_web.nr_sequencia%type);	

procedure 	gerar_usuario_audc(	nm_usuario_tasy_p		usuario.nm_usuario%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type);
					
procedure 	gerar_usuario_estipulante(	nm_usuario_web_p		usuario.nm_usuario%type,
						ds_email_p			wsuite_usuario.ds_email%type,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
						cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type,
						nr_seq_grupo_contrato_p		pls_grupo_contrato.nr_sequencia%type,
						nr_seq_contrato_p		pls_contrato.nr_sequencia%type,
						nr_seq_intercambio_p		pls_intercambio.nr_sequencia%type,
						nr_seq_subestipulante_p		pls_sub_estipulante.nr_sequencia%type,
						nr_seq_perfil_p			wsuite_perfil.nr_sequencia%type,
						ds_seqs_cont_grupo_p		varchar2,
						nm_usuario_p			usuario.nm_usuario%type,
						nr_seq_usuario_p	out	wsuite_usuario.nr_sequencia%type);

procedure  	gerar_usuario_prestador(	nm_usuario_web_p		usuario.nm_usuario%type,
						ds_email_p			wsuite_usuario.ds_email%type,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
						cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type,
						nr_seq_perfil_p			wsuite_perfil.nr_sequencia%type,
						nm_usuario_p			usuario.nm_usuario%type,
						nr_seq_usuario_p	out	wsuite_usuario.nr_sequencia%type);
						
end tws_usuario_hpms_pck;
/

create or replace package body tws_usuario_hpms_pck as

procedure 	gerar_usuario_beneficiario(	nr_seq_segurado_web_p 	pls_segurado_web.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Criar os registros do usuario beneficiario gerados no security para as tabelas pls.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ x] Outros: TWS
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

id_hpms_application_w		application.id%type;
id_hpms_datasource_w		datasource.id%type;
id_beneficiary_client_w		client.id%type;
id_beneficiary_role_w		role.id%type;

Cursor C01 is
	
	select	a.nr_seq_segurado,
		a.dt_atualizacao,
		a.ds_senha,
		a.ds_tec,
		a.ie_situacao,
		a.cd_estabelecimento,
		b.cd_pessoa_fisica,
		b.nm_pessoa_fisica,
		d.cd_usuario_plano cd_carteira_atual,
		(select  ca.CD_USUARIO_ANT
        	from     pls_segurado_cart_ant ca
       	 	where    ca.NR_SEQ_SEGURADO = a.nr_seq_segurado
       		and      ca.nr_sequencia = (select max(m.nr_sequencia) from pls_segurado_cart_ant m where m.nr_seq_segurado = a.nr_seq_segurado)) cd_carteira_anterior
	from	pls_segurado_web a,
		pessoa_fisica b,
		pls_segurado c,
		pls_segurado_carteira d
	where	a.nr_seq_segurado 	= c.nr_sequencia
	and	b.cd_pessoa_fisica 	= c.cd_pessoa_fisica
	and	d.nr_seq_segurado 	= c.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_web_p; 
	
	
hpms_pls_segurado_web_w		C01%rowtype;

begin

open C01; 
loop
fetch C01 into	
	hpms_pls_segurado_web_w;
exit when C01%notfound;
	begin
	import_users_to_psa_tws_pack.load_beneficiary_parameters(	id_hpms_application_w, id_hpms_datasource_w, id_beneficiary_client_w,
									id_beneficiary_role_w);

	import_users_to_psa_tws_pack.insert_beneficiary(hpms_pls_segurado_web_w, id_hpms_application_w, id_hpms_datasource_w, 
							id_beneficiary_client_w, id_beneficiary_role_w);
	end;
end loop;
close C01;

end gerar_usuario_beneficiario;



procedure gerar_usuario_audc(	nm_usuario_tasy_p		usuario.nm_usuario%type,
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
				cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Procedure utilizada para geracao do usuario para Auditoria Concorrente
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


Cursor C01 is
 	select	a.id id_application,
		(select d.id from datasource d where  d.nm_datasource = 'WTASY' and d.id_application = a.id) id_data_source,
		(select c.id from client c where  c.nm_client = 'concurrentaudit' and c.id_application = a.id) id_client,
		(select r.id from role r where  r.nm_role = 'concurrentaudit' and r.id_application = a.id) id_role				
	from	application a
	where   nm_application = 'hpms';
	

Cursor C02 ( 	id_subject_c		subject.id%type,
		id_data_source_c	datasource.id%type,
		id_client_c		client.id%type,
		id_role_c		role.id%type) is
 	select	(select count(1) from	application_subject a where a.id_subject = id_subject_c and a.id_application = a.id ) app_subject,
		(select count(1) from	subject_datasource s where s.id_subject = id_subject_c and s.id_datasource = id_data_source_c ) sub_datasource,		
		(select count(1) from	subject_client c where c.id_subject = id_subject_c and c.id_client = id_client_c ) sub_client,
		(select count(1) from	subject_role r where r.id_subject = id_subject_c and r.id_role =  id_role_c ) sub_role,
		(select count(1) from	wsuite_usuario w where	w.id_subject = id_subject_c) user_wsuite
	from	application a
	where   nm_application = 'hpms';
	
	
	
id_subject_w	subject.id%type;
application_w	application.id%type;

begin


select	count(1)
into	application_w			
from	application a
where   nm_application = 'hpms';

select 	max(s.id )
into	id_subject_w
from   	subject s 
where  	s.ds_login = nm_usuario_tasy_p;



if	(application_w = 0 ) then
	/* Aplicacao HPMS nao encontrada */
	wheb_mensagem_pck.exibir_mensagem_abort(1090118);
end if;


if	( id_subject_w is not null ) then 

	for c01_w in C01 loop
	
		
		if ( c01_w.id_application is null or  c01_w.id_data_source is null or c01_w.id_client is null or  c01_w.id_role is null ) then			
			/* Configuracao HPMS nao encontrada */
			wheb_mensagem_pck.exibir_mensagem_abort(1090119);
		end if;
		
		
		for c02_w in C02 ( id_subject_w, c01_w.id_data_source, c01_w.id_client, c01_w.id_role )  loop		
			
			if	( c02_w.app_subject = 0 ) then
				/* Links the subject to the application*/
				insert into application_subject (
					id_application, id_subject
				) values (
					c01_w.id_application, id_subject_w);
			
			end if;
			
			
			if	( c02_w.sub_datasource = 0 ) then
				/* Links the subject to the datasource */
				insert into subject_datasource (
				    id, dt_creation, dt_modification, id_subject, 
				    id_datasource, vl_activation_status, vl_attempts_so_far
				) values (
				    psa_uuid_generator, sysdate, sysdate, id_subject_w, 
				    c01_w.id_data_source, 0, 0);
			end if;
			
			
			if	( c02_w.sub_client = 0 ) then
				/* Links the subject to the client */
				insert into subject_client (
				    id, id_subject,  id_client,
				    dt_creation, dt_modification, vl_activation_status, vl_attempts_so_far
				) values (
				    psa_uuid_generator, id_subject_w, c01_w.id_client,
				    sysdate, sysdate, 0, 0);
			end if;

			
			if	( c02_w.sub_role = 0 ) then			
				insert into subject_role (
				    id_subject, id_role, dt_creation,
				    dt_modification
				) values (
				    id_subject_w, c01_w.id_role, sysdate,
				    sysdate);	
			end if;    
			    
			   
			if ( c02_w.user_wsuite = 0 ) then
				insert	into wsuite_usuario (
					nr_sequencia, ie_situacao, cd_estabelecimento,
					nm_usuario_web, cd_pessoa_fisica, dt_atualizacao,
					nm_usuario, dt_atualizacao_nrec,
					id_subject, ds_login, ds_alternative_login,
					dt_ativacao, dt_criacao
				) values (
					wsuite_usuario_seq.nextval,'A', cd_estabelecimento_p,
					nm_usuario_tasy_p, cd_pessoa_fisica_p, sysdate,
					nm_usuario_tasy_p,  sysdate, 
					id_subject_w, nm_usuario_tasy_p, null,
					sysdate, sysdate);	
			end if;
			
			commit; 
		end loop;
	end loop;
end if;

end gerar_usuario_audc;

procedure inserir_contrato_web(	nr_seq_grupo_contrato_web_p	pls_grupo_contrato_web.nr_sequencia%type,
				nr_seq_contrato_p		pls_contrato.nr_sequencia%type, 
				nr_seq_intercambio_p		pls_intercambio.nr_sequencia%type,
				nr_seq_subestipulante_p		pls_sub_estipulante.nr_sequencia%type,
				nm_usuario_p			usuario.nm_usuario%type, 
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type) is

begin

if	((nr_seq_contrato_p is not null) or (nr_seq_intercambio_p is not null)) then
	insert into pls_contrato_web	(	nr_sequencia, ie_situacao, nr_seq_usuario_grupo,   
						dt_atualizacao, nm_usuario, dt_atualizacao_nrec,    
						nm_usuario_nrec, nr_seq_contrato, nr_seq_intercambio,     
						nr_seq_subestipulante)
				values	(	pls_contrato_web_seq.nextval, 'A', nr_seq_grupo_contrato_web_p,	
						sysdate, nm_usuario_p, sysdate,
						nm_usuario_p, nr_seq_contrato_p, nr_seq_intercambio_p,
						nr_seq_subestipulante_p);				
						
end if;

end inserir_contrato_web;

procedure 	gerar_grupo_contrato_web(	nr_seq_grupo_contrato_p			pls_grupo_contrato.nr_sequencia%type, 
						cd_pessoa_fisica_p			pessoa_fisica.cd_pessoa_fisica%type,
						nm_usuario_web_p			pls_grupo_contrato_web.nm_usuario_web%type, 
						nr_seq_contrato_p			pls_contrato.nr_sequencia%type, 
						nr_seq_intercambio_p			pls_intercambio.nr_sequencia%type, 
						ds_seqs_cont_grupo_p			varchar2,	
						nr_seq_subestipulante_p			pls_sub_estipulante.nr_sequencia%type,
						nm_usuario_p				usuario.nm_usuario%type, 
						cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
						nr_seq_grupo_contrato_web_p	out 	pls_grupo_contrato_web.nr_sequencia%type) is
			

ie_origem_grupo_web_w		pls_grupo_contrato_web.ie_origem_grupo_web%type;
nr_seq_grupo_contrato_web_w	pls_grupo_contrato_web.nr_sequencia%type;
ds_seqs_cont_grupo_w		varchar2(4000);
nr_seq_contrato_w		pls_contrato.nr_sequencia%type;
nr_seq_intercambio_w		pls_intercambio.nr_sequencia%type;
nr_seq_contrato_grupo_w		pls_contrato_grupo.nr_sequencia%type;

begin
	if	(nr_seq_grupo_contrato_p is not null) then
		select	decode(ie_tipo_grupo, 'C',3,4)
		into	ie_origem_grupo_web_w
		from	pls_grupo_contrato
		where	nr_sequencia	= nr_seq_grupo_contrato_p;
		
	elsif 	(nr_seq_contrato_p is not null) then
		ie_origem_grupo_web_w	:= 1;
		
	elsif 	(nr_seq_intercambio_p is not null) then
		ie_origem_grupo_web_w	:= 2;
	end if;
		
	insert into pls_grupo_contrato_web (	nr_sequencia, nm_usuario_web, ds_senha,                      
						ie_situacao, dt_atualizacao, nm_usuario,                     
						dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_grupo_contrato,          
						cd_estabelecimento, cd_pessoa_fisica, ie_origem_grupo_web,              
						ds_email, ds_tec)  

				values	(	pls_grupo_contrato_web_seq.nextval, nm_usuario_web_p, null,
						'A', sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, nr_seq_grupo_contrato_p,
						cd_estabelecimento_p, cd_pessoa_fisica_p, ie_origem_grupo_web_w,																
						null, null) returning nr_sequencia INTO nr_seq_grupo_contrato_web_w;
	 
	if	(ds_seqs_cont_grupo_p is not null) then
		ds_seqs_cont_grupo_w	:= ds_seqs_cont_grupo_p;
		
		while	(instr(ds_seqs_cont_grupo_w,',') <> 0) loop 
			begin
				nr_seq_contrato_grupo_w		:= substr(ds_seqs_cont_grupo_w,1,instr(ds_seqs_cont_grupo_w,',') - 1);
				ds_seqs_cont_grupo_w		:= substr(ds_seqs_cont_grupo_w,instr(ds_seqs_cont_grupo_w,',') + 1,255);
	
				begin
					select	nr_seq_contrato,
						nr_seq_intercambio
					into	nr_seq_contrato_w,
						nr_seq_intercambio_w
					from	pls_contrato_grupo
					where	nr_sequencia	= nr_seq_contrato_grupo_w;
				exception
				when others then
					nr_seq_contrato_w	:= null;
					nr_seq_intercambio_w	:= null;
				end;
				
				inserir_contrato_web(	nr_seq_grupo_contrato_web_w, nr_seq_contrato_w, nr_seq_intercambio_w,
							nr_seq_subestipulante_p, nm_usuario_p, cd_estabelecimento_p);
			end;
		end loop;
	else
		inserir_contrato_web(	nr_seq_grupo_contrato_web_w, nr_seq_contrato_p, nr_seq_intercambio_p,
					nr_seq_subestipulante_p, nm_usuario_p, cd_estabelecimento_p);
	end if;

	nr_seq_grupo_contrato_web_p	:= nr_seq_grupo_contrato_web_w;
	
end gerar_grupo_contrato_web;

								
procedure  	gerar_usuario_estipulante(	nm_usuario_web_p		usuario.nm_usuario%type,
						ds_email_p			wsuite_usuario.ds_email%type,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
						cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type,
						nr_seq_grupo_contrato_p		pls_grupo_contrato.nr_sequencia%type,
						nr_seq_contrato_p		pls_contrato.nr_sequencia%type,
						nr_seq_intercambio_p		pls_intercambio.nr_sequencia%type,
						nr_seq_subestipulante_p		pls_sub_estipulante.nr_sequencia%type,
						nr_seq_perfil_p			wsuite_perfil.nr_sequencia%type,
						ds_seqs_cont_grupo_p		varchar2,
						nm_usuario_p			usuario.nm_usuario%type,
						nr_seq_usuario_p	out	wsuite_usuario.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Criar usuario estipulante nas tabelas do security e TWS.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ x ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros: TWS
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

Cursor C01 is
 	select	a.id id_application,
		(select d.id from datasource d where  d.nm_datasource = 'WTASY' and d.id_application = a.id) id_data_source,
		(select c.id from client c where  c.nm_client = 'stipulator' and c.id_application = a.id) id_client,
		(select r.id from role r where  r.nm_role = 'stipulator' and r.id_application = a.id) id_role				
	from	application a
	where   nm_application = 'hpms';
	
id_subject_w			subject.id%type;
application_w			application.id%type;
ds_login_w			subject.ds_login%type;	
ds_senha_w			subject.ds_password%type;
ds_salt_w			subject.ds_salt%type;
nr_seq_grupo_contrato_web_w	pls_grupo_contrato_web.nr_sequencia%type;
ds_hash_ativacao_w		wsuite_usuario.ds_hash_ativacao%type;

begin

select	count(1)
into	application_w			
from	application a
where   nm_application = 'hpms';

select	max(a.id)
into	id_subject_w
from    subject a,
	subject_client b,
	client c
where   b.id_subject = a.id
and     b.id_client = c.id
and     a.ds_replacement_login = nm_usuario_web_p
and     c.nm_client = 'stipulator';

if	(application_w = 0 ) then
	/* Aplicacao HPMS nao encontrada */
	wheb_mensagem_pck.exibir_mensagem_abort(1090118);
end if;


if	( id_subject_w is null) and	
	((nr_seq_contrato_p is not null) or
	(nr_seq_intercambio_p is not null) or 
	(nr_seq_grupo_contrato_p is not null)) then 
	
	gerar_grupo_contrato_web(	nr_seq_grupo_contrato_p, cd_pessoa_fisica_p, nm_usuario_web_p, 
					nr_seq_contrato_p, nr_seq_intercambio_p, ds_seqs_cont_grupo_p, 
					nr_seq_subestipulante_p, nm_usuario_p, cd_estabelecimento_p, 
					nr_seq_grupo_contrato_web_w);		
		   
	if 	(nr_seq_grupo_contrato_web_w is not null) then
		for c01_w in C01 loop	
		
			if ( c01_w.id_application is null or  c01_w.id_data_source is null or c01_w.id_client is null or  c01_w.id_role is null ) then			
				/* Configuracao HPMS nao encontrada */
				wheb_mensagem_pck.exibir_mensagem_abort(1090119);
			end if;
			
			select 	replace(replace(dbms_random.string('P', 15), chr(39), ''), ';', '') 
			into	ds_salt_w
			from 	dual;

			ds_senha_w	:= obter_sha2(ds_salt_w, 256);
			
			select	'TWS-' || substr('00000000000' || TO_CHAR(tws_dslogin_sequence.NEXTVAL),-11,11)
			into 	ds_login_w
			from 	dual;

			ds_hash_ativacao_w	:= wsuite_login_pck.wsuite_generate_hash;
			
			INSERT INTO subject (	id,
						dt_creation,
						dt_modification,
						ds_login,
						nm_subject,
						ds_password,
						ds_salt,
						dt_password_modification,
						vl_auth_type,
						ds_replacement_login,
						ds_alternative_login,
						vl_password_style
				) VALUES (	psa_uuid_generator,
						sysdate,
						sysdate,
						ds_login_w,
						( select pf.nm_pessoa_fisica from pessoa_fisica pf where pf.cd_pessoa_fisica = cd_pessoa_fisica_p),
						ds_senha_w,
						ds_salt_w,
						sysdate,
						0,
						nm_usuario_web_p,
						null,
						1) RETURNING id INTO id_subject_w;
			
			/* Links the subject to the application*/
			insert into application_subject (id_application, id_subject
			) values (			c01_w.id_application, id_subject_w);			
		
			/* Links the subject to the datasource */
			insert into subject_datasource (	id, dt_creation, dt_modification, id_subject, 
								id_datasource, vl_activation_status, vl_attempts_so_far
			) values (				psa_uuid_generator, sysdate, sysdate, id_subject_w, 
								c01_w.id_data_source, 0, 0);
		
			/* Links the subject to the client */
			insert into subject_client ( 	id, id_subject,  id_client,
							dt_creation, dt_modification, vl_activation_status, vl_attempts_so_far
			) values ( 			psa_uuid_generator, id_subject_w, c01_w.id_client,
							sysdate, sysdate, 0, 0);
				
			insert into subject_role ( 	id_subject, id_role, dt_creation,
							dt_modification
			) values ( 			id_subject_w, c01_w.id_role, sysdate,
							sysdate);	
					
			insert	into wsuite_usuario (	nr_sequencia, ie_situacao, cd_estabelecimento,
							nm_usuario_web, cd_pessoa_fisica, dt_atualizacao,
							nm_usuario, dt_atualizacao_nrec,
							id_subject, ds_login, ds_alternative_login,
							dt_ativacao, dt_criacao, ds_email,
							nr_seq_grupo_contrato_web, nr_seq_perfil_wsuite, ds_hash_ativacao
					) values (	wsuite_usuario_seq.nextval,'I', cd_estabelecimento_p,
							nm_usuario_web_p, cd_pessoa_fisica_p, sysdate,
							nm_usuario_p,  sysdate, 
							id_subject_w, ds_login_w, null,
							null, sysdate, ds_email_p,
							nr_seq_grupo_contrato_web_w, nr_seq_perfil_p, ds_hash_ativacao_w) returning nr_sequencia INTO nr_seq_usuario_p;	
							
			commit;		
		end loop;
	end if;
else	
	/* Aplicacao HPMS nao encontrada */
	wheb_mensagem_pck.exibir_mensagem_abort(1090118);
end if;

end gerar_usuario_estipulante;


procedure  	gerar_usuario_prestador(	nm_usuario_web_p		usuario.nm_usuario%type,
						ds_email_p			wsuite_usuario.ds_email%type,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
						cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type,
						nr_seq_perfil_p			wsuite_perfil.nr_sequencia%type,
						nm_usuario_p			usuario.nm_usuario%type,
						nr_seq_usuario_p	out	wsuite_usuario.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Criar usuario prestador nas tabelas do security e TWS.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ x ] Tasy (HTML5) [  ] Portal [  ]  Relatorios [ ] Outros: TWS
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

Cursor C01 is
	select	a.id id_application,
		(select d.id from datasource d where  d.nm_datasource = 'WTASY' and d.id_application = a.id) id_data_source,
		(select c.id from client c where  c.nm_client = 'provider_hpms' and c.id_application = a.id) id_client,
		(select r.id from role r where  r.nm_role = 'providerHpms' and r.id_application = a.id) id_role				
	from	application a
	where   nm_application = 'hpms';


	id_subject_w		subject.id%type;
	application_w		application.id%type;
	ds_login_w			subject.ds_login%type;	
	ds_senha_w			subject.ds_password%type;
	ds_salt_w			subject.ds_salt%type;
	ds_hash_ativacao_w	wsuite_usuario.ds_hash_ativacao%type;
	ie_selecionados 	number(8);
	nr_seq_usuario_w 	wsuite_usuario.nr_sequencia%type;

begin

	select	count(1)
	into	application_w			
	from	application a
	where   nm_application = 'hpms';

	select	max(a.id)
	into	id_subject_w
	from    subject a,
		subject_client b,
		client c
	where   b.id_subject = a.id
	and     b.id_client = c.id
	and     a.ds_replacement_login = nm_usuario_web_p
	and     c.nm_client = 'provider_hpms';

	if	(application_w = 0 ) then
		/* Aplicacao HPMS nao encontrada */
		wheb_mensagem_pck.exibir_mensagem_abort(1090118);
	end if;


	if	( id_subject_w is null) then 

		for c01_w in C01 loop

			if ( c01_w.id_application is null or  c01_w.id_data_source is null or c01_w.id_client is null or  c01_w.id_role is null ) then			
				/* Configuracao HPMS nao encontrada */
				wheb_mensagem_pck.exibir_mensagem_abort(1090119);
			end if;

			select 	replace(replace(dbms_random.string('P', 15), chr(39), ''), ';', '') 
			into	ds_salt_w
			from 	dual;

			ds_senha_w	:= obter_sha2(ds_salt_w, 256);

			select	'TWS-' || substr('00000000000' || TO_CHAR(tws_dslogin_sequence.NEXTVAL),-11,11)
			into 	ds_login_w
			from 	dual;

			ds_hash_ativacao_w	:= wsuite_login_pck.wsuite_generate_hash;

			INSERT INTO subject (	id,
						dt_creation,
						dt_modification,
						ds_login,
						nm_subject,
						ds_password,
						ds_salt,
						dt_password_modification,
						vl_auth_type,
						ds_replacement_login,
						ds_alternative_login,
						vl_password_style
				) VALUES (	psa_uuid_generator,
						sysdate,
						sysdate,
						ds_login_w,
						( select pf.nm_pessoa_fisica from pessoa_fisica pf where pf.cd_pessoa_fisica = cd_pessoa_fisica_p),
						ds_senha_w,
						ds_salt_w,
						sysdate,
						0,
						nm_usuario_web_p,
						null,
						1) RETURNING id INTO id_subject_w;

			/* Links the subject to the application*/
			insert into application_subject (id_application, id_subject
			) values (			c01_w.id_application, id_subject_w);			

			/* Links the subject to the datasource */
			insert into subject_datasource (	id, dt_creation, dt_modification, id_subject, 
								id_datasource, vl_activation_status, vl_attempts_so_far
			) values (				psa_uuid_generator, sysdate, sysdate, id_subject_w, 
								c01_w.id_data_source, 0, 0);

			/* Links the subject to the client */
			insert into subject_client ( 	id, id_subject,  id_client,
							dt_creation, dt_modification, vl_activation_status, vl_attempts_so_far
			) values ( 			psa_uuid_generator, id_subject_w, c01_w.id_client,
							sysdate, sysdate, 0, 0);

			insert into subject_role ( 	id_subject, id_role, dt_creation,
							dt_modification
			) values ( 			id_subject_w, c01_w.id_role, sysdate,
							sysdate);	

			select count(*) into ie_selecionados 
			from pls_cad_prestador_auxiliar 
			where nm_usuario = wheb_usuario_pck.get_nm_usuario;

			if ie_selecionados >  0 then

				select pls_usuario_web_seq.nextval into nr_seq_usuario_w from dual;

				INSERT INTO pls_usuario_web 
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nm_usuario_web,
					cd_pessoa_fisica_resp,
					ds_senha,
					ds_email,
					nr_seq_prestador,
					ie_contas_medicas_on_line,
					ie_upload_contas_medicas,
					ie_solicitacao_autorizacao,
					ds_observacao,
					ie_situacao,
					cd_estabelecimento,
					ie_tipo_complemento,
					ie_exige_biometria,
					nr_seq_perfil_web,
					dt_alteracao_senha,
					nr_seq_local_atend,
					qt_tentativa_acesso,
					ds_tec,
					ie_biometria_tasy_agent) 
				VALUES 
					(nr_seq_usuario_w,
					SYSDATE,
					nm_usuario_P, 
					SYSDATE,
					nm_usuario_P, 
					nm_usuario_web_p,
					cd_pessoa_fisica_P, 
					ds_senha_w, 
					ds_email_P, 
					NULL, 
					'N',
					'S',
					'S',
					NULL,
					'I',
					cd_estabelecimento_P,
					NULL,
					'N',
					NULL,
					NULL,
					NULL,
					NULL,
					ds_salt_w,
					NULL);

				insert	into wsuite_usuario (	nr_sequencia, ie_situacao, cd_estabelecimento,
								nm_usuario_web, cd_pessoa_fisica, ie_tipo_login, dt_atualizacao,
								nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
								id_subject, ds_login, ds_alternative_login,
								dt_ativacao, dt_criacao, ds_email,
								nr_seq_perfil_wsuite, ds_hash_ativacao, nr_seq_prest_usuario_web
				) values (	wsuite_usuario_seq.nextval,'I', cd_estabelecimento_p,
								nm_usuario_web_p, cd_pessoa_fisica_p, 'PR', sysdate,
								nm_usuario_p, sysdate, nm_usuario_p, 
								id_subject_w, ds_login_w, null,
								null, sysdate, ds_email_p,
								nr_seq_perfil_p, ds_hash_ativacao_w, nr_seq_usuario_w) 
								returning nr_sequencia INTO nr_seq_usuario_p;	

				pls_salvar_usu_wsuite_prest(nr_seq_usuario_w);

			end if;
			commit;		
		end loop;
	else
	/* Aplicacao HPMS nao encontrada */
	wheb_mensagem_pck.exibir_mensagem_abort(1065325);
	end if;

end gerar_usuario_prestador;

end tws_usuario_hpms_pck;
/
