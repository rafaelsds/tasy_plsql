create or replace
package projeto_xml_pck as

	type t_projeto_xml_row is record (
		nm_atributo_xml		varchar2(255),
		nivel			number(5),
		ie_tipo			varchar2(15),
		ie_tipo_atributo		xml_atributo.ie_tipo_atributo%type,
		ds_mascara		xml_atributo.ds_mascara%type,
		ie_obrigatorio		xml_atributo.ie_obrigatorio%type,
		ds_observacao		varchar2(255),
		id			number(5),
		nr_seq_apresentacao	number(10));

	type t_projeto_xml is table of t_projeto_xml_row;


	function obter_estrutura_xml(nr_seq_projeto_p	number)
			return t_projeto_xml pipelined;

end projeto_xml_pck;
/

create or replace
package body projeto_xml_pck as

function obter_estrutura_xml(nr_seq_projeto_p	number)
		return t_projeto_xml pipelined is

t_projeto_xml_row_w	t_projeto_xml_row;

cursor c01 is
select	substr(lpad(' ',(level - 1) * 2,' ') || nm_atributo_xml,1,255) nm_atributo_xml, 
	level nivel,
	ie_tipo,
	ie_tipo_atributo,
	ds_mascara,
	ie_obrigatorio,
	ds_observacao,
	rownum id,
	nr_seq_apresentacao
from (	select	b.nm_atributo_xml,
		b.nr_sequencia,
		b.nr_seq_elemento nr_seq_superior,
		'A' ie_tipo,
		b.ie_tipo_atributo,
		b.ds_mascara,
		b.ie_obrigatorio,
		substr(obter_desc_expressao(cd_exp_observacao),1,255) ds_observacao,
		b.nr_seq_apresentacao
	from	xml_elemento a,
		xml_atributo b
	where	a.nr_sequencia = b.nr_seq_elemento
	and	a.nr_seq_projeto = nr_seq_projeto_p
	and	b.nr_seq_atrib_elem is null
	union all
	select	nvl(a.ds_grupo, b.nm_atributo_xml) nm_atributo_xml,
		b.nr_sequencia,
		b.nr_seq_elemento nr_seq_superior,
		decode(nvl(a.ds_grupo,'NULL'), 'NULL', 'AE', 'E') ie_tipo,
		null ie_tipo_atributo,
		null ds_mascara,
		null ie_obrigatorio,
		null ds_observacao,
		b.nr_seq_apresentacao*1000
	from	xml_elemento a,
		xml_atributo b
	where	b.nr_seq_atrib_elem = a.nr_sequencia
	and	a.nr_seq_projeto = nr_seq_projeto_p
	union all
	select	a.nm_elemento nm_atributo_xml,
		a.nr_sequencia,
		(select	max(y.nr_sequencia)
		from	xml_elemento x,
			xml_atributo y
		where	x.nr_sequencia = y.nr_seq_elemento
		and	x.nr_seq_projeto = a.nr_seq_projeto
		and	y.nr_seq_atrib_elem = a.nr_sequencia) nr_seq_superior,
		'E' ie_tipo,
		null ie_tipo_atributo,
		null ds_mascara,
		null ie_obrigatorio,
		null ds_observacao,
		a.nr_seq_apresentacao*1000
	from	xml_elemento a
	where	a.nr_seq_projeto = nr_seq_projeto_p) z
start with nr_seq_superior is null
connect by prior nr_sequencia = nr_seq_superior
order by	nivel, z.nr_seq_apresentacao, z.nm_atributo_xml;

c01_w	c01%rowtype;

begin

if	(nvl(nr_seq_projeto_p,0) > 0) then

	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;
		begin
		if	(c01_w.ie_tipo <> 'AE') then		
			begin
			t_projeto_xml_row_w.nm_atributo_xml	:=	c01_w.nm_atributo_xml;
			t_projeto_xml_row_w.nivel		:=	c01_w.nivel;
			t_projeto_xml_row_w.ie_tipo		:=	c01_w.ie_tipo;
			t_projeto_xml_row_w.ie_tipo_atributo	:=	c01_w.ie_tipo_atributo;
			t_projeto_xml_row_w.ds_mascara		:=	c01_w.ds_mascara;
			t_projeto_xml_row_w.ie_obrigatorio	:=	c01_w.ie_obrigatorio;
			t_projeto_xml_row_w.ds_observacao	:=	c01_w.ds_observacao;
			t_projeto_xml_row_w.id			:=	c01_w.id;
			t_projeto_xml_row_w.nr_seq_apresentacao	:=	c01_w.nr_seq_apresentacao;			

			pipe row(t_projeto_xml_row_w);
			end;
		end if;
		end;
	end loop;
	close C01;

end if;

end obter_estrutura_xml;

end projeto_xml_pck;
/