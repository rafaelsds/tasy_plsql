create or replace 
package meta_cuidado_pck as

type t_objeto_row is record (
	ds_meta		varchar2(255),
	ie_meta		varchar2(1),
	ie_valor	varchar2(1),
	dt_inicio		date,
	nr_sequencia	number,
	ie_status		varchar2(5),
	nr_seq_meta		number,
	ie_status_geral	varchar2(5),
	ie_status_hoje	varchar2(5),
	ie_status_1	varchar2(5),
	ie_status_2	varchar2(5),
	ie_status_3	varchar2(5),
	ie_status_4	varchar2(5),
	ie_status_5	varchar2(5),
	ie_status_6	varchar2(5),
	ie_status_7	varchar2(5),
	nm_meta varchar2(255),
	dt_final_prevista date
     );	
     
type t_objeto_row_data is table of t_objeto_row;
     
function obter_dados (nr_atendimento_p	number, ie_html_p	varchar2 default 'S', ie_tipo_meta varchar2 default 'A') return t_objeto_row_data pipelined;

function getStatusDia(nr_atendimento_p number,nr_seq_meta_p number, nr_dia_p number) return varchar2;

function getStatusSingle(nr_seq_meta_atend_p number) return varchar2;
					
end meta_cuidado_pck;
/



create or replace package body meta_cuidado_pck as



function getStatusDia( nr_atendimento_p number,nr_seq_meta_p number,	nr_dia_p	number) 
					return varchar2 is
					
ie_status_w	varchar2(10);

Cursor C001 is
	select	nvl(b.ie_status,'I') ie_status
	from	tof_meta_atend a, 
		tof_meta_atend_hor b, 
		tof_meta c 
	where	a.nr_sequencia = b.nr_seq_meta_atend 
	and  	a.nr_seq_meta = c.nr_sequencia
	and	c.nr_sequencia	= nr_seq_meta_p
	and	trunc(b.dt_geracao) = trunc(sysdate - nr_dia_p)
	and	c.ie_tipo_meta = 'D'
	and	Obter_se_meta_liberada(a.nr_atendimento,a.nr_seq_meta) = 'S'
	and	a.nr_atendimento = nr_atendimento_p
	and a.dt_liberacao is not null
	and a.dt_inativacao is null
	and b.dt_liberacao is not null
	and b.dt_inativacao is null
	order by dt_geracao;

begin

open C001;
loop
fetch C001 into	
	ie_status_w;
exit when C001%notfound;
end loop;
close C001;

return	nvl(ie_status_w,'I');

end getStatusDia;

function getStatusSingle( nr_seq_meta_atend_p number) 
					return varchar2 is
					
ie_status_w	varchar2(10);

Cursor C001 is
	select	nvl(a.ie_status,'I') ie_status
	from	tof_meta_atend a, 
			tof_meta c 
	where	a.nr_seq_meta = c.nr_sequencia
	and		a.nr_sequencia = nr_seq_meta_atend_p
	and		c.ie_tipo_meta = 'U'
	and		Obter_se_meta_liberada(a.nr_atendimento,a.nr_seq_meta) = 'S'
	and     a.dt_liberacao is not null
	and     a.dt_inativacao is null;

begin
open C001;
loop
fetch C001 into	
	ie_status_w;
exit when C001%notfound;
end loop;
close C001;


return	nvl(ie_status_w,'I');

end getStatusSingle;

function obter_dados(	nr_atendimento_p	number,
						ie_html_p	varchar2 default 'S',
						ie_tipo_meta varchar2 default 'A')
 		    	return t_objeto_row_data pipelined is

t_objeto_row_w			t_objeto_row;
ds_inconsistencia_w		varchar2(4000);
lista_w					dbms_sql.varchar2_table;
ds_linha_w				varchar2(255);
ds_valores_w			varchar2(255);
qt_reg_w				number(10) := 0;
ds_espaco_w				varchar2(255) := chr(38)||'nbsp;'||chr(38)||'nbsp;'||chr(38)||'nbsp;';

Cursor C01 is
	 select  a.nr_seq_meta nr_seq_meta,  
			 max(nvl(a.nm_meta,c.ds_meta)) ds_meta,  
			 max(a.nr_sequencia) nr_sequencia,  
			 max(a.ie_status) ie_status,  
			 max(a.dt_inicio) dt_inicio,
			 max(c.nr_sequencia) nr_seq_meta_tof,
			 max(a.nm_meta) nm_meta,
			 max(a.dt_final_prevista) dt_final_prevista
	from  	 tof_meta_atend a,  
			 tof_meta_atend_hor b,  
			 tof_meta c  
	where    a.nr_sequencia = b.nr_seq_meta_atend  
	and      Obter_se_meta_liberada(a.nr_atendimento,a.nr_seq_meta) =  'S' 
	and      a.nr_seq_meta = c.nr_sequencia  
	and      c.ie_tipo_meta = 'D'  
	and	     a.nr_atendimento = nr_atendimento_p
	and      a.dt_liberacao is not null
	and      a.dt_inativacao is null
	and      b.dt_liberacao is not null
	and      b.dt_inativacao is null
	and      a.dt_finalizacao is null
	group by a.nr_seq_meta 
	order by ds_meta;
		
c01_w c01%rowtype;


Cursor C02 is
	 select   a.nr_seq_meta nr_seq_meta,  
	       max(nvl(a.nm_meta,c.ds_meta)) ds_meta,  
	       max(a.nr_sequencia) nr_sequencia,
           max(a.nm_meta) nm_meta,
           max(a.dt_final_prevista) dt_final_prevista  ,
		   max(a.dt_inicio) dt_inicio
	from  	tof_meta_atend a,  
	       tof_meta_atend_hor b,  
	       tof_meta c  
	where    a.nr_sequencia = b.nr_seq_meta_atend  
	and      Obter_se_meta_liberada(a.nr_atendimento,a.nr_seq_meta) =  'S' 
	and      a.nr_seq_meta = c.nr_sequencia  
	and      c.ie_tipo_meta = 'U'  
	and	     a.nr_atendimento = nr_atendimento_p
	and      a.dt_liberacao is not null
	and      a.dt_inativacao is null
	and      b.dt_liberacao is not null
	and      b.dt_inativacao is null
	and      a.dt_finalizacao is null
	group by a.nr_seq_meta 
	order by ds_meta;

c02_w c02%rowtype;

begin

if (ie_html_p = 'N') then
	ds_espaco_w := chr(32)||chr(32)||chr(32);
end if;

if (nvl(ie_tipo_meta,'A') = 'A' or nvl(ie_tipo_meta,'A') = 'D') then
	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		begin	
		if	(qt_reg_w	= 0) then
				if (ie_html_p = 'N') then
					t_objeto_row_w.ds_meta	:= obter_desc_expressao(297359);
				else
					t_objeto_row_w.ds_meta	:= '<b>'||obter_desc_expressao(297359)||'</b>';
				end if;
				t_objeto_row_w.ie_meta	:= 'D';
				t_objeto_row_w.ie_valor	:= 'T';
			
			pipe row (t_objeto_row_w);
		end if;
		
		t_objeto_row_w.ds_meta	:= ds_espaco_w|| c01_w.ds_meta;
		t_objeto_row_w.ie_meta	:= 'D';
		t_objeto_row_w.ie_valor	:= 'D';
		t_objeto_row_w.dt_inicio	:= c01_w.dt_inicio;
		t_objeto_row_w.nr_seq_meta	:= c01_w.nr_seq_meta;
		t_objeto_row_w.nr_sequencia	:= c01_w.nr_sequencia;
		t_objeto_row_w.ie_status_geral := c01_w.ie_status;
		t_objeto_row_w.ie_status_hoje	:= getStatusDia(nr_atendimento_p, c01_w.nr_seq_meta_tof,0);
		t_objeto_row_w.ie_status_1	:= getStatusDia(nr_atendimento_p, c01_w.nr_seq_meta_tof,1);
		t_objeto_row_w.ie_status_2	:= getStatusDia(nr_atendimento_p, c01_w.nr_seq_meta_tof,2);
		t_objeto_row_w.ie_status_3	:= getStatusDia(nr_atendimento_p, c01_w.nr_seq_meta_tof,3);
		t_objeto_row_w.ie_status_4	:= getStatusDia(nr_atendimento_p, c01_w.nr_seq_meta_tof,4);
		t_objeto_row_w.ie_status_5	:= getStatusDia(nr_atendimento_p, c01_w.nr_seq_meta_tof,5);
		t_objeto_row_w.ie_status_6	:= getStatusDia(nr_atendimento_p, c01_w.nr_seq_meta_tof,6);
		t_objeto_row_w.ie_status_7	:= getStatusDia(nr_atendimento_p, c01_w.nr_seq_meta_tof,7);
		t_objeto_row_w.nm_meta := c01_w.nm_meta;
		t_objeto_row_w.dt_final_prevista := c01_w.dt_final_prevista;
		
		pipe row (t_objeto_row_w);
		qt_reg_w	:= qt_reg_w+1;
		end;
	end loop;
	close C01;
end if;

qt_reg_w	:= 0;

if (nvl(ie_tipo_meta,'A') = 'A' or nvl(ie_tipo_meta,'A') = 'U') then
open C02;
loop
fetch C02 into	
	c02_w;
exit when C02%notfound;
	begin
	if	(qt_reg_w	= 0) then
		if (ie_html_p = 'N') then
			t_objeto_row_w.ds_meta	:= obter_desc_expressao(303885);
		else
			t_objeto_row_w.ds_meta	:= '<b>'||obter_desc_expressao(303885)||'</b>';
		end if;
		t_objeto_row_w.ie_meta	:= 'U';
		t_objeto_row_w.ie_valor	:= 'T';
		t_objeto_row_w.dt_inicio := null;
		t_objeto_row_w.ie_status_geral	:= null;
		t_objeto_row_w.ie_status_hoje	:= null;
		t_objeto_row_w.ie_status_1	:= null;
		t_objeto_row_w.ie_status_2	:= null;
		t_objeto_row_w.ie_status_3	:= null;
		t_objeto_row_w.ie_status_4	:= null;
		t_objeto_row_w.ie_status_5	:= null;
		t_objeto_row_w.ie_status_6	:= null;
		t_objeto_row_w.ie_status_7	:= null;
		t_objeto_row_w.nm_meta := null;
		pipe row (t_objeto_row_w);
	end if;
	
	t_objeto_row_w.ds_meta	:= ds_espaco_w|| c02_w.ds_meta;
	t_objeto_row_w.ie_meta	:= 'U';
	t_objeto_row_w.ie_valor	:= 'D';
	t_objeto_row_w.ie_status_geral	:= getStatusSingle(c02_w.nr_sequencia);
	t_objeto_row_w.dt_inicio	:= c02_w.dt_inicio;
	t_objeto_row_w.nr_seq_meta	:= c02_w.nr_seq_meta;
	t_objeto_row_w.nr_sequencia	:= c02_w.nr_sequencia;
	t_objeto_row_w.ie_status_hoje	:= null;
	t_objeto_row_w.ie_status_1	:= null;
	t_objeto_row_w.ie_status_2	:= null;
	t_objeto_row_w.ie_status_3	:= null;
	t_objeto_row_w.ie_status_4	:= null;
	t_objeto_row_w.ie_status_5	:= null;
	t_objeto_row_w.ie_status_6	:= null;
	t_objeto_row_w.ie_status_7	:= null;
	t_objeto_row_w.nm_meta :=  null;
	
	pipe row (t_objeto_row_w);
	qt_reg_w	:= qt_reg_w+1;
	end;
end loop;
close C02;
end if;

return;

end obter_dados;

end meta_cuidado_pck;
/
