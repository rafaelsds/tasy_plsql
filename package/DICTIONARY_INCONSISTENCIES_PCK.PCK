create or replace package dictionary_inconsistencies_pck is
	procedure validate_inconsistencies_table(nm_table varchar2);
	procedure inconsistencies_by_dev_group(nr_group number, dt_initial date, dt_final date);
	procedure inconsistencies_by_serv_order(nr_service_order_p number, dt_initial date, dt_final date);
	
	SYSTEM_TABLE_NAME CONSTANT varchar2(30) := 'TABELA_SISTEMA';
	ATTRIBUTE_TABLE_NAME CONSTANT varchar2(30) := 'TABELA_ATRIBUTO';
	INDEX_NAME CONSTANT varchar2(30) := 'INDICE';
	ATTRIBUTE_INDEX_NAME CONSTANT varchar2(30) := 'INDICE_ATRIBUTO';
	REFERENTIAL_INTEGRITY_NAME CONSTANT varchar2(30) := 'INTEGRIDADE_REFERENCIAL';
	ATTRIBUTE_INTEGRITY_NAME CONSTANT varchar2(30) := 'INTEGRIDADE_ATRIBUTO';
	
end dictionary_inconsistencies_pck;
/
create or replace package body dictionary_inconsistencies_pck is

	procedure validate_inconsistencies_table(nm_table varchar2) is
	begin
		tasy_consistir_base_tabela(nm_table);
	end validate_inconsistencies_table;
	
	procedure inconsistencies_by_dev_group(nr_group number, dt_initial date, dt_final date) is
	begin
		FOR w_changes IN (
		select  distinct
				case 
					when instr(nr_seq_documento, ';') = 0 then nr_seq_documento
					when instr(nr_seq_documento, ';') > 0 then substr(nr_seq_documento, 1, instr(nr_seq_documento, ';')-1) 
				end table_name
		from    reg_object_log_v
		where   nm_tabela in (SYSTEM_TABLE_NAME, ATTRIBUTE_TABLE_NAME, INDEX_NAME, ATTRIBUTE_INDEX_NAME, REFERENTIAL_INTEGRITY_NAME, ATTRIBUTE_INTEGRITY_NAME)
		and		nm_usuario in (select nm_usuario_grupo from corp.usuario_grupo_des@whebl01_dbcorp where nr_seq_grupo = nr_group)
		and		dt_atualizacao between dt_initial and dt_final)
		LOOP
			validate_inconsistencies_table(w_changes.table_name);
		END LOOP w_changes;			
	end inconsistencies_by_dev_group;
	
	procedure inconsistencies_by_serv_order(nr_service_order_p number, dt_initial date, dt_final date) is
	begin
		FOR w_changes IN (
		select  distinct
				case 
					when instr(nr_seq_documento, ';') = 0 then nr_seq_documento
					when instr(nr_seq_documento, ';') > 0 then substr(nr_seq_documento, 1, instr(nr_seq_documento, ';')-1) 
				end table_name
		from    reg_object_log_v
		where   nm_tabela in (SYSTEM_TABLE_NAME, ATTRIBUTE_TABLE_NAME, INDEX_NAME, ATTRIBUTE_INDEX_NAME, REFERENTIAL_INTEGRITY_NAME, ATTRIBUTE_INTEGRITY_NAME)
		and		nr_service_order = nr_service_order_p
		and		dt_atualizacao between dt_initial and dt_final)
		LOOP
			validate_inconsistencies_table(w_changes.table_name);
		END LOOP w_changes;
	end inconsistencies_by_serv_order;

end dictionary_inconsistencies_pck;
/