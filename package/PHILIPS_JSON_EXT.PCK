create or replace package philips_json_ext as
  
  
  function parsePath(json_path varchar2, base number default 1) return philips_json_list;
  
  
  function get_json_value(obj philips_json, v_path varchar2, base number default 1) return philips_json_value;
  function get_string(obj philips_json, path varchar2,       base number default 1) return varchar2;
  function get_number(obj philips_json, path varchar2,       base number default 1) return number;
  function get_json(obj philips_json, path varchar2,         base number default 1) return philips_json;
  function get_json_list(obj philips_json, path varchar2,    base number default 1) return philips_json_list;
  function get_bool(obj philips_json, path varchar2,         base number default 1) return boolean;

  
  procedure put(obj in out nocopy philips_json, path varchar2, elem varchar2,   base number default 1);
  procedure put(obj in out nocopy philips_json, path varchar2, elem number,     base number default 1);
  procedure put(obj in out nocopy philips_json, path varchar2, elem philips_json,       base number default 1);
  procedure put(obj in out nocopy philips_json, path varchar2, elem philips_json_list,  base number default 1);
  procedure put(obj in out nocopy philips_json, path varchar2, elem boolean,    base number default 1);
  procedure put(obj in out nocopy philips_json, path varchar2, elem philips_json_value, base number default 1);

  procedure remove(obj in out nocopy philips_json, path varchar2, base number default 1);
  
  
  function pp(obj philips_json, v_path varchar2) return varchar2; 
  procedure pp(obj philips_json, v_path varchar2); 
  procedure pp_htp(obj philips_json, v_path varchar2); 

  
  function is_integer(v philips_json_value) return boolean;
  
  format_string varchar2(30 char) := 'yyyy-mm-dd hh24:mi:ss';
  
  function to_json_value(d date) return philips_json_value;
  
  function is_date(v philips_json_value) return boolean;
  
  function to_date(v philips_json_value) return date;
  
  function to_date2(v philips_json_value) return date;
  
  function get_date(obj philips_json, path varchar2, base number default 1) return date;
  procedure put(obj in out nocopy philips_json, path varchar2, elem date, base number default 1);
  
  
  function base64(binarydata blob) return philips_json_list;
  function base64(l philips_json_list) return blob;

  function encode(binarydata blob) return philips_json_value;
  function decode(v philips_json_value) return blob;
  
end philips_json_ext;
/
create or replace package body philips_json_ext as
  scanner_exception exception;
  pragma exception_init(scanner_exception, -20100);
  parser_exception exception;
  pragma exception_init(parser_exception, -20101);
  jext_exception exception;
  pragma exception_init(jext_exception, -20110);
  
  
  function is_integer(v philips_json_value) return boolean as
    myint number(38); 
  begin
    if(v.is_number) then
      myint := v.get_number;
      return (myint = v.get_number); 
    else
      return false;
    end if;
  end;
  
  
  function to_json_value(d date) return philips_json_value as
  begin
    return philips_json_value(to_char(d, format_string));
  end;
  
  
  function is_date(v philips_json_value) return boolean as
    temp date;
  begin
    temp := philips_json_ext.to_date(v);
    return true;
  exception
    when others then 
      return false;
  end;
  
  
  function to_date(v philips_json_value) return date as
  begin
    if(v.is_string) then
      return standard.to_date(v.get_string, format_string);
    else
      --rase_application_error(-20110, 'Anydata did not contain a date-value');
	  wheb_mensagem_pck.exibir_mensagem_abort('Anydata did not contain a date-value');
    end if;
  exception
    when others then
      --rase_application_error(-20110, 'Anydata did not contain a date on the format: '||format_string);
	  wheb_mensagem_pck.exibir_mensagem_abort('Anydata did not contain a date on the format: '||format_string);
  end;
  
 
  function to_date2(v philips_json_value) return date as
  begin
    return to_date(v);
  end;
  
 
  function parsePath(json_path varchar2, base number default 1) return philips_json_list as
    build_path varchar2(32767) := '[';
    buf varchar2(4);
    endstring varchar2(1);
    indx number := 1;
    ret philips_json_list;
    
    procedure next_char as
    begin
      if(indx <= length(json_path)) then
        buf := substr(json_path, indx, 1);
        indx := indx + 1;
      else 
        buf := null;
      end if;
    end;  
 
    procedure skipws as begin while(buf in (chr(9),chr(10),chr(13),' ')) loop next_char; end loop; end;
  
  begin
    next_char();
    while(buf is not null) loop
      if(buf = '.') then
        next_char();
        if(buf is null) then wheb_mensagem_pck.exibir_mensagem_abort('philips_json Path parse error: . is not a valid json_path end'); end if;		
        if(not regexp_like(buf, '^[[:alnum:]\_ ]+', 'c') ) then
          wheb_mensagem_pck.exibir_mensagem_abort('philips_json Path parse error: alpha-numeric character or space expected at position '||indx);
        end if;
        
        if(build_path != '[') then build_path := build_path || ','; end if;
        build_path := build_path || '"';
        while(regexp_like(buf, '^[[:alnum:]\_ ]+', 'c') ) loop
          build_path := build_path || buf;
          next_char();
        end loop;
        build_path := build_path || '"';
      elsif(buf = '[') then
        next_char();
        skipws();
        if(buf is null) then wheb_mensagem_pck.exibir_mensagem_abort('philips_json Path parse error: [ is not a valid json_path end'); end if;
        if(buf in ('1','2','3','4','5','6','7','8','9') or (buf = '0' and base = 0)) then
          if(build_path != '[') then build_path := build_path || ','; end if;
          while(buf in ('0','1','2','3','4','5','6','7','8','9')) loop
            build_path := build_path || buf;
            next_char();
          end loop;      
        elsif (regexp_like(buf, '^(\"|\'')', 'c')) then
          endstring := buf;
          if(build_path != '[') then build_path := build_path || ','; end if;
          build_path := build_path || '"';
          next_char();
          if(buf is null) then wheb_mensagem_pck.exibir_mensagem_abort('philips_json Path parse error: premature json_path end'); end if;
          while(buf != endstring) loop
            build_path := build_path || buf;
            next_char();
            if(buf is null) then wheb_mensagem_pck.exibir_mensagem_abort('philips_json Path parse error: premature json_path end'); end if;
            if(buf = '\') then 
              next_char(); 
              build_path := build_path || '\' || buf; 
              next_char(); 
            end if;
          end loop;
          build_path := build_path || '"';
          next_char(); 
        else
          wheb_mensagem_pck.exibir_mensagem_abort('philips_json Path parse error: expected a string or an positive integer at '||indx);
        end if;
        skipws();
        if(buf is null) then wheb_mensagem_pck.exibir_mensagem_abort( 'philips_json Path parse error: premature json_path end'); end if;
        if(buf != ']') then wheb_mensagem_pck.exibir_mensagem_abort( 'philips_json Path parse error: no array ending found. found: '|| buf); end if;
        next_char();
        skipws();
      elsif(build_path = '[') then
        if(not regexp_like(buf, '^[[:alnum:]\_ ]+', 'c') ) then
          wheb_mensagem_pck.exibir_mensagem_abort('philips_json Path parse error: alpha-numeric character or space expected at position '||indx);
        end if;
        build_path := build_path || '"';
        while(regexp_like(buf, '^[[:alnum:]\_ ]+', 'c') ) loop
          build_path := build_path || buf;
          next_char();
        end loop;
        build_path := build_path || '"';
      else 
        wheb_mensagem_pck.exibir_mensagem_abort( 'philips_json Path parse error: expected . or [ found '|| buf || ' at position '|| indx);
      end if;
        
    end loop;
  
    build_path := build_path || ']';
    build_path := replace(replace(replace(replace(replace(build_path, chr(9), '\t'), chr(10), '\n'), chr(13), '\f'), chr(8), '\b'), chr(14), '\r');
    
    ret := philips_json_list(build_path);
    if(base != 1) then
 
      declare
        elem philips_json_value;
      begin
        for i in 1 .. ret.count loop
          elem := ret.get(i);
          if(elem.is_number) then
            ret.replace(i,elem.get_number()+1);
          end if;
        end loop;
      end;
    end if;
    
    return ret;
  end parsePath;
    
 
  function get_json_value(obj philips_json, v_path varchar2, base number default 1) return philips_json_value as 
    path philips_json_list;
    ret philips_json_value; 
    o philips_json; l philips_json_list;
  begin
    path := parsePath(v_path, base);
    ret := obj.to_json_value;
    if(path.count = 0) then return ret; end if;
    
    for i in 1 .. path.count loop
      if(path.get(i).is_string()) then
 
        o := philips_json(ret);
        ret := o.get(path.get(i).get_string());
      else
 
        if(ret.is_array()) then
          l := philips_json_list(ret);
          ret := l.get(path.get(i).get_number());
        else 
          o := philips_json(ret);
          l := o.get_values();
          ret := l.get(path.get(i).get_number());
        end if;
      end if;
    end loop;
    
    return ret;
  exception
    when scanner_exception then wheb_mensagem_pck.exibir_mensagem_abort('Error');
    when parser_exception then wheb_mensagem_pck.exibir_mensagem_abort('Error');
    when jext_exception then wheb_mensagem_pck.exibir_mensagem_abort('Error');
    when others then return null;
  end get_json_value;
  
 
  function get_string(obj philips_json, path varchar2, base number default 1) return varchar2 as 
    temp philips_json_value;
  begin 
    temp := get_json_value(obj, path, base);
    if(temp is null or not temp.is_string) then 
      return null; 
    else 
      return temp.get_string;
    end if;
  end;
  
  function get_number(obj philips_json, path varchar2, base number default 1) return number as 
    temp philips_json_value;
  begin 
    temp := get_json_value(obj, path, base);
    if(temp is null or not temp.is_number) then 
      return null; 
    else 
      return temp.get_number;
    end if;
  end;
  
  function get_json(obj philips_json, path varchar2, base number default 1) return philips_json as 
    temp philips_json_value;
  begin 
    temp := get_json_value(obj, path, base);
    if(temp is null or not temp.is_object) then 
      return null; 
    else 
      return philips_json(temp);
    end if;
  end;
  
  function get_json_list(obj philips_json, path varchar2, base number default 1) return philips_json_list as 
    temp philips_json_value;
  begin 
    temp := get_json_value(obj, path, base);
    if(temp is null or not temp.is_array) then 
      return null; 
    else 
      return philips_json_list(temp);
    end if;
  end;
  
  function get_bool(obj philips_json, path varchar2, base number default 1) return boolean as 
    temp philips_json_value;
  begin 
    temp := get_json_value(obj, path, base);
    if(temp is null or not temp.is_bool) then 
      return null; 
    else 
      return temp.get_bool;
    end if;
  end;
  
  function get_date(obj philips_json, path varchar2, base number default 1) return date as 
    temp philips_json_value;
  begin 
    temp := get_json_value(obj, path, base);
    if(temp is null or not is_date(temp)) then 
      return null; 
    else 
      return philips_json_ext.to_date(temp);
    end if;
  end;
  
 
  procedure put_internal(obj in out nocopy philips_json, v_path varchar2, elem philips_json_value, base number) as
    val philips_json_value := elem;
    path philips_json_list;
    backreference philips_json_list := philips_json_list();
    
    keyval philips_json_value; keynum number; keystring varchar2(4000);
    temp philips_json_value := obj.to_json_value;
    obj_temp  philips_json;
    list_temp philips_json_list;
    inserter philips_json_value;
  begin
    path := philips_json_ext.parsePath(v_path, base);
    if(path.count = 0) then wheb_mensagem_pck.exibir_mensagem_abort( 'philips_json_ext put error: cannot put with empty string.'); end if;  
 
    for i in 1 .. path.count loop
 
      keyval := path.get(i);
      if (keyval.is_number()) then
        --nummer index
        keynum := keyval.get_number();
        if((not temp.is_object()) and (not temp.is_array())) then
          if(val is null) then return; end if;
          backreference.remove_last;
          temp := philips_json_list().to_json_value();
          backreference.append(temp);
        end if;
  
        if(temp.is_object()) then 
          obj_temp := philips_json(temp);
          if(obj_temp.count < keynum) then 
            if(val is null) then return; end if;
            wheb_mensagem_pck.exibir_mensagem_abort( 'philips_json_ext put error: access object with to few members.'); 
          end if;
          temp := obj_temp.get(keynum);
        else 
          list_temp := philips_json_list(temp);
          if(list_temp.count < keynum) then 
            if(val is null) then return; end if;
            
            for i in list_temp.count+1 .. keynum loop
              list_temp.append(philips_json_value.makenull);
            end loop;
            backreference.remove_last;
            backreference.append(list_temp);
          end if;
  
          temp := list_temp.get(keynum);
        end if;
      else 
 
        keystring := keyval.get_string();
        if(not temp.is_object()) then 
 
          if(val is null) then return; end if;
          backreference.remove_last;
          temp := philips_json().to_json_value();
          backreference.append(temp);
 
        end if;
        obj_temp := philips_json(temp);
        temp := obj_temp.get(keystring);
      end if;
  
      if(temp is null) then 
        if(val is null) then return; end if;
 
        keyval := path.get(i+1);
        if(keyval is not null and keyval.is_number()) then
          temp := philips_json_list().to_json_value; 
        else 
          temp := philips_json().to_json_value; 
        end if;
      end if;
      backreference.append(temp);
    end loop;
      
    inserter := val;  
    for i in reverse 1 .. backreference.count loop
 
      if( i = 1 ) then
        keyval := path.get(1);
        if(keyval.is_string()) then
          keystring := keyval.get_string();
        else 
          keynum := keyval.get_number();
          declare
            t1 philips_json_value := obj.get(keynum);
          begin
            keystring := t1.mapname;
          end;
        end if;
        if(inserter is null) then obj.remove(keystring); else obj.put(keystring, inserter); end if;
      else
        temp := backreference.get(i-1);
        if(temp.is_object()) then
          keyval := path.get(i);
          obj_temp := philips_json(temp);
          if(keyval.is_string()) then
            keystring := keyval.get_string();
          else 
            keynum := keyval.get_number();
            declare
              t1 philips_json_value := obj_temp.get(keynum);
            begin
              keystring := t1.mapname;
            end;
          end if;
          if(inserter is null) then 
            obj_temp.remove(keystring); 
            if(obj_temp.count > 0) then inserter := obj_temp.to_json_value; end if;
          else 
            obj_temp.put(keystring, inserter);
            inserter := obj_temp.to_json_value; 
          end if;
        else 
 
          keynum := path.get(i).get_number();
          list_temp := philips_json_list(temp);
          list_temp.remove(keynum);
          if(not inserter is null) then 
            list_temp.append(inserter, keynum);
            inserter := list_temp.to_json_value; 
          else 
            if(list_temp.count > 0) then inserter := list_temp.to_json_value; end if;
          end if; 
        end if;    
      end if;
      
    end loop;

  end put_internal;

 
  procedure put(obj in out nocopy philips_json, path varchar2, elem varchar2, base number default 1) as
  begin
    if elem is null then
        put_internal(obj, path, philips_json_value(), base);
    else
        put_internal(obj, path, philips_json_value(elem), base);
    end if;
  end;

  procedure put(obj in out nocopy philips_json, path varchar2, elem number, base number default 1) as
  begin
    if elem is null then
        put_internal(obj, path, philips_json_value(), base);
    else
        put_internal(obj, path, philips_json_value(elem), base);
    end if;
  end;

  procedure put(obj in out nocopy philips_json, path varchar2, elem philips_json, base number default 1) as
  begin
    if elem is null then
        put_internal(obj, path, philips_json_value(), base);
    else
        put_internal(obj, path, elem.to_json_value, base);
    end if;
  end;

  procedure put(obj in out nocopy philips_json, path varchar2, elem philips_json_list, base number default 1) as
  begin
    if elem is null then
        put_internal(obj, path, philips_json_value(), base);
    else
        put_internal(obj, path, elem.to_json_value, base);
    end if;
  end;

  procedure put(obj in out nocopy philips_json, path varchar2, elem boolean, base number default 1) as
  begin
    if elem is null then
        put_internal(obj, path, philips_json_value(), base);
    else
        put_internal(obj, path, philips_json_value(elem), base);
    end if;
  end;

  procedure put(obj in out nocopy philips_json, path varchar2, elem philips_json_value, base number default 1) as
  begin
    if elem is null then
        put_internal(obj, path, philips_json_value(), base);
    else
        put_internal(obj, path, elem, base);
    end if;
  end;

  procedure put(obj in out nocopy philips_json, path varchar2, elem date, base number default 1) as
  begin
    if elem is null then
        put_internal(obj, path, philips_json_value(), base);
    else
        put_internal(obj, path, philips_json_ext.to_json_value(elem), base);
    end if;
  end;

  procedure remove(obj in out nocopy philips_json, path varchar2, base number default 1) as
  begin
    philips_json_ext.put_internal(obj,path,null,base);
  end remove;

  function pp(obj philips_json, v_path varchar2) return varchar2 as
    json_part philips_json_value;
  begin
    json_part := philips_json_ext.get_json_value(obj, v_path);
    if(json_part is null) then 
      return ''; 
    else 
      return philips_json_printer.pretty_print_any(json_part); 
    end if;
  end pp;
  
  procedure pp(obj philips_json, v_path varchar2) as 
  begin
    dbms_output.put_line(pp(obj, v_path));
  end pp;
  
  
  procedure pp_htp(obj philips_json, v_path varchar2) as 
    json_part philips_json_value;
  begin
    json_part := philips_json_ext.get_json_value(obj, v_path);
    if(json_part is null) then htp.print; else 
      htp.print(philips_json_printer.pretty_print_any(json_part, false)); 
    end if;
  end pp_htp;
  
  function base64(binarydata blob) return philips_json_list as
    obj philips_json_list := philips_json_list();
    c clob := empty_clob();
    benc blob;    
  
    v_blob_offset NUMBER := 1;
    v_clob_offset NUMBER := 1;
    v_lang_context NUMBER := DBMS_LOB.DEFAULT_LANG_CTX;
    v_warning NUMBER;
    v_amount PLS_INTEGER;


    FUNCTION encodeBlob2Base64(pBlobIn IN BLOB) RETURN BLOB IS
      vAmount NUMBER := 45;
      vBlobEnc BLOB := empty_blob();
      vBlobEncLen NUMBER := 0;
      vBlobInLen NUMBER := 0;
      vBuffer RAW(45);
      vOffset NUMBER := 1;
    BEGIN

      vBlobInLen := dbms_lob.getlength(pBlobIn);

      dbms_lob.createtemporary(vBlobEnc, TRUE);
      LOOP
        IF vOffset >= vBlobInLen THEN
          EXIT;
        END IF;
        dbms_lob.read(pBlobIn, vAmount, vOffset, vBuffer);
        BEGIN
          dbms_lob.append(vBlobEnc, utl_encode.base64_encode(vBuffer));
        EXCEPTION
          WHEN OTHERS THEN
          dbms_output.put_line('<vAmount>' || vAmount || '<vOffset>' || vOffset || '<vBuffer>' || vBuffer);
          dbms_output.put_line('ERROR IN append: ' || SQLERRM);
          wheb_mensagem_pck.exibir_mensagem_abort('Error');
        END;
        vOffset := vOffset + vAmount;
      END LOOP;
      vBlobEncLen := dbms_lob.getlength(vBlobEnc);
      RETURN vBlobEnc;
    END encodeBlob2Base64;
  begin
    benc := encodeBlob2Base64(binarydata);
    dbms_lob.createtemporary(c, TRUE);
    v_amount := DBMS_LOB.GETLENGTH(benc);
    DBMS_LOB.CONVERTTOCLOB(c, benc, v_amount, v_clob_offset, v_blob_offset, 1, v_lang_context, v_warning);
  
    v_amount := DBMS_LOB.GETLENGTH(c);
    v_clob_offset := 1;
    while(v_clob_offset < v_amount) loop
      obj.append(dbms_lob.SUBSTR(c, 4000,v_clob_offset));
      v_clob_offset := v_clob_offset + 4000;
    end loop;
    dbms_lob.freetemporary(benc);
    dbms_lob.freetemporary(c);
    return obj;
  
  end base64;


  function base64(l philips_json_list) return blob as
    c clob := empty_clob();
    b blob := empty_blob();
    bret blob;
  
    v_blob_offset NUMBER := 1;
    v_clob_offset NUMBER := 1;
    v_lang_context NUMBER := 0; 
    v_warning NUMBER;
    v_amount PLS_INTEGER;

    FUNCTION decodeBase642Blob(pBlobIn IN BLOB) RETURN BLOB IS
      vAmount NUMBER := 256;
      vBlobDec BLOB := empty_blob();
      vBlobDecLen NUMBER := 0;
      vBlobInLen NUMBER := 0;
      vBuffer RAW(256);
      vOffset NUMBER := 1;
    BEGIN

      vBlobInLen := dbms_lob.getlength(pBlobIn);

      dbms_lob.createtemporary(vBlobDec, TRUE);
      LOOP
        IF vOffset >= vBlobInLen THEN
          EXIT;
        END IF;
        dbms_lob.read(pBlobIn, vAmount, vOffset, vBuffer);
        BEGIN
          dbms_lob.append(vBlobDec, utl_encode.base64_decode(vBuffer));
        EXCEPTION
          WHEN OTHERS THEN
          dbms_output.put_line('<vAmount>' || vAmount || '<vOffset>' || vOffset || '<vBuffer>' || vBuffer);
          dbms_output.put_line('ERROR IN append: ' || SQLERRM);
          wheb_mensagem_pck.exibir_mensagem_abort('Error');
        END;
        vOffset := vOffset + vAmount;
      END LOOP;
      vBlobDecLen := dbms_lob.getlength(vBlobDec);
      RETURN vBlobDec;
    END decodeBase642Blob;
  begin
    dbms_lob.createtemporary(c, TRUE);
    for i in 1 .. l.count loop
      dbms_lob.append(c, l.get(i).get_string());
    end loop;
    v_amount := DBMS_LOB.GETLENGTH(c);
    
    dbms_lob.createtemporary(b, TRUE);
    DBMS_LOB.CONVERTTOBLOB(b, c, dbms_lob.lobmaxsize, v_clob_offset, v_blob_offset, 1, v_lang_context, v_warning);
    dbms_lob.freetemporary(c);
    v_amount := DBMS_LOB.GETLENGTH(b);
    
    bret := decodeBase642Blob(b); 
    dbms_lob.freetemporary(b);
    return bret;
  
  end base64;

  function encode(binarydata blob) return philips_json_value as
    obj philips_json_value;
    c clob := empty_clob();
    benc blob;    
  
    v_blob_offset NUMBER := 1;
    v_clob_offset NUMBER := 1;
    v_lang_context NUMBER := DBMS_LOB.DEFAULT_LANG_CTX;
    v_warning NUMBER;
    v_amount PLS_INTEGER;

    FUNCTION encodeBlob2Base64(pBlobIn IN BLOB) RETURN BLOB IS
      vAmount NUMBER := 45;
      vBlobEnc BLOB := empty_blob();
      vBlobEncLen NUMBER := 0;
      vBlobInLen NUMBER := 0;
      vBuffer RAW(45);
      vOffset NUMBER := 1;
    BEGIN
      vBlobInLen := dbms_lob.getlength(pBlobIn);
      dbms_lob.createtemporary(vBlobEnc, TRUE);
      LOOP
        IF vOffset >= vBlobInLen THEN
          EXIT;
        END IF;
        dbms_lob.read(pBlobIn, vAmount, vOffset, vBuffer);
        BEGIN
          dbms_lob.append(vBlobEnc, utl_encode.base64_encode(vBuffer));
        EXCEPTION
          WHEN OTHERS THEN
          dbms_output.put_line('<vAmount>' || vAmount || '<vOffset>' || vOffset || '<vBuffer>' || vBuffer);
          dbms_output.put_line('ERROR IN append: ' || SQLERRM);
          wheb_mensagem_pck.exibir_mensagem_abort('Error');
        END;
        vOffset := vOffset + vAmount;
      END LOOP;
      vBlobEncLen := dbms_lob.getlength(vBlobEnc);
      RETURN vBlobEnc;
    END encodeBlob2Base64;
  begin
    benc := encodeBlob2Base64(binarydata);
    dbms_lob.createtemporary(c, TRUE);
    v_amount := DBMS_LOB.GETLENGTH(benc);
    DBMS_LOB.CONVERTTOCLOB(c, benc, v_amount, v_clob_offset, v_blob_offset, 1, v_lang_context, v_warning);
    
    obj := philips_json_value(c);  

    dbms_lob.freetemporary(benc);
    dbms_lob.freetemporary(c);
    return obj;
  
  end encode;
  
  function decode(v philips_json_value) return blob as
    c clob := empty_clob();
    b blob := empty_blob();
    bret blob;
  
    v_blob_offset NUMBER := 1;
    v_clob_offset NUMBER := 1;
    v_lang_context NUMBER := 0; 
    v_warning NUMBER;
    v_amount PLS_INTEGER;

    FUNCTION decodeBase642Blob(pBlobIn IN BLOB) RETURN BLOB IS
      vAmount NUMBER := 256;
      vBlobDec BLOB := empty_blob();
      vBlobDecLen NUMBER := 0;
      vBlobInLen NUMBER := 0;
      vBuffer RAW(256);
      vOffset NUMBER := 1;
    BEGIN

      vBlobInLen := dbms_lob.getlength(pBlobIn);

      dbms_lob.createtemporary(vBlobDec, TRUE);
      LOOP
        IF vOffset >= vBlobInLen THEN
          EXIT;
        END IF;
        dbms_lob.read(pBlobIn, vAmount, vOffset, vBuffer);
        BEGIN
          dbms_lob.append(vBlobDec, utl_encode.base64_decode(vBuffer));
        EXCEPTION
          WHEN OTHERS THEN
          dbms_output.put_line('<vAmount>' || vAmount || '<vOffset>' || vOffset || '<vBuffer>' || vBuffer);
          dbms_output.put_line('ERROR IN append: ' || SQLERRM);
          wheb_mensagem_pck.exibir_mensagem_abort('error');
        END;
        vOffset := vOffset + vAmount;
      END LOOP;
      vBlobDecLen := dbms_lob.getlength(vBlobDec);
      RETURN vBlobDec;
    END decodeBase642Blob;
  begin
    dbms_lob.createtemporary(c, TRUE);
    v.get_string(c);
    v_amount := DBMS_LOB.GETLENGTH(c);
    
    dbms_lob.createtemporary(b, TRUE);
    DBMS_LOB.CONVERTTOBLOB(b, c, dbms_lob.lobmaxsize, v_clob_offset, v_blob_offset, 1, v_lang_context, v_warning);
    dbms_lob.freetemporary(c);
    v_amount := DBMS_LOB.GETLENGTH(b);
    
    bret := decodeBase642Blob(b); 
    dbms_lob.freetemporary(b);
    return bret;
  
  end decode;


end philips_json_ext;
/