create or replace package product_mgmt_pck is

  procedure add_product(p_product_name in varchar2,
                        p_nm_usuario   in varchar2 default 'Tasy');

  procedure add_version(p_product_name in varchar2,
                        p_version      in varchar2,
                        p_build_date   in date,
                        p_nm_usuario   in varchar2 default 'Tasy');

  procedure add_compatibility(p_product_name           in varchar2,
                              p_product_version        in varchar2,
                              p_service_pack           in varchar2,
                              p_product_name_compat    in varchar2,
                              p_product_version_compat in varchar2,
                              p_service_pack_compat    in varchar2,
                              p_nm_usuario             in varchar2 default 'Tasy');

  procedure install_product(p_product_name in varchar2);

  procedure activate_version(p_product_name    in varchar2,
                             p_product_version in varchar2);

  function is_product_installed(p_product_name in varchar2) return varchar2;

  function current_version(p_product_name in varchar2) return varchar2;

  function is_version_active(p_product_name    in varchar2,
                             p_product_version in varchar2) return varchar2;

  function get_version_list(p_product_name in varchar2) return varchar2;
end;
/
create or replace package body product_mgmt_pck is

  ---------------------------------------------------------------
  --
  -- Load product tables from legacy Tasy tables.
  --
  ---------------------------------------------------------------
  procedure load_product_tables is
  begin
    insert into product
      (product_name,
       is_product_installed,
       last_service_pack_applied,
       nm_usuario,
       dt_atualizacao)
      select t.cd_aplicacao_tasy, 'N', 0, t.nm_usuario, t.dt_atualizacao
        from aplicacao_tasy t;
  
    insert into product_version
      (product_name,
       product_version,
       is_version_active,
       version_build_date,
       nm_usuario,
       dt_atualizacao)
      select t.cd_aplicacao_tasy,
             t.cd_versao,
             'N',
             t.dt_versao,
             t.nm_usuario,
             t.dt_atualizacao
        from aplicacao_tasy_versao t;
  
    commit;
  end;

  ---------------------------------------------------------------
  --
  -- Add a new product.
  --
  ---------------------------------------------------------------
  procedure add_product(p_product_name in varchar2,
                        p_nm_usuario   in varchar2 default 'Tasy') is
  begin
    insert into product
      (product_name,
       is_product_installed,
       last_service_pack_applied,
       nm_usuario,
       dt_atualizacao)
    values
      (p_product_name, 'N', 0, p_nm_usuario, sysdate);
  end;

  ---------------------------------------------------------------
  --
  -- Add a new version for a specified product.
  --
  ---------------------------------------------------------------
  procedure add_version(p_product_name in varchar2,
                        p_version      in varchar2,
                        p_build_date   in date,
                        p_nm_usuario   in varchar2 default 'Tasy') is
  begin
    insert into product_version
      (product_name,
       product_version,
       version_build_date,
       nm_usuario,
       dt_atualizacao)
    values
      (p_product_name, p_version, p_build_date, p_nm_usuario, sysdate);
  end;

  ---------------------------------------------------------------
  --
  -- Add a record in the compatibility matrix.
  --
  ---------------------------------------------------------------
  procedure add_compatibility(p_product_name           in varchar2,
                              p_product_version        in varchar2,
                              p_service_pack           in varchar2,
                              p_product_name_compat    in varchar2,
                              p_product_version_compat in varchar2,
                              p_service_pack_compat    in varchar2,
                              p_nm_usuario             in varchar2 default 'Tasy') is
  begin
    insert into product_compatibility
      (product_name,
       product_version,
       service_pack,
       product_name_compat,
       product_version_compat,
       service_pack_compat,
       nm_usuario,
       dt_atualizacao)
    values
      (p_product_name,
       p_product_version,
       p_service_pack,
       p_product_name_compat,
       p_product_version_compat,
       p_service_pack_compat,
       p_nm_usuario,
       sysdate);
  end;

  ---------------------------------------------------------------
  --
  -- Mark a product as installed. To be installed, the product
  -- must have a record in the product table.
  --
  ---------------------------------------------------------------
  procedure install_product(p_product_name in varchar2) is
  begin
    update product
       set is_product_installed = 'Y'
     where product_name = p_product_name;
  end;

  ---------------------------------------------------------------
  --
  -- Active a version of a product.
  --
  ---------------------------------------------------------------
  procedure activate_version(p_product_name    in varchar2,
                             p_product_version in varchar2) is
  begin
    update product_version
       set is_version_active = 'N'
     where product_name = p_product_name
       and is_version_active = 'Y';
  
    update product_version
       set is_version_active = 'Y'
     where product_name = p_product_name
       and product_version = p_product_version;
  end;

  ---------------------------------------------------------------
  --
  -- Check if the informed product is marked as installed.
  --
  ---------------------------------------------------------------
  function is_product_installed(p_product_name in varchar2) return varchar2 is
    v_ret varchar2(1);
  begin
    begin
      select is_product_installed
        into v_ret
        from product
       where product_name = p_product_name;
    exception
      when no_data_found then
        v_ret := 'N';
    end;
    return v_ret;
  end;

  ---------------------------------------------------------------
  --
  -- Get the current version of a specified product.
  --
  ---------------------------------------------------------------
  function current_version(p_product_name in varchar2) return varchar2 is
    v_ret          varchar2(20);
    v_product_name varchar2(100);
  begin
    v_product_name := p_product_name;
  
    -- Condicao para permitir a coexistencia dos produtos Tasy e HIS
    if v_product_name = 'HIS' then
      if is_product_installed('HIS') = 'N' and
         is_product_installed('Tasy') = 'Y' then
        v_product_name := 'Tasy';
      end if;
    end if;
  
    if is_product_installed(v_product_name) = 'Y' then
      begin
        select product_version
          into v_ret
          from product_version
         where product_name = v_product_name
           and is_version_active = 'Y';
      exception
        when no_data_found then
          v_ret := null;
      end;
    else
      v_ret := null;
    end if;
    return v_ret;
  end;

  ---------------------------------------------------------------
  --
  -- Check if the specified version of a product is marked
  -- as active.
  --
  ---------------------------------------------------------------
  function is_version_active(p_product_name    in varchar2,
                             p_product_version in varchar2) return varchar2 is
    v_ret varchar2(1);
  begin
    begin
      select is_version_active
        into v_ret
        from product_version
       where product_name = p_product_name
         and product_version = p_product_version;
    exception
      when no_data_found then
        v_ret := 'N';
    end;
    return v_ret;
  end;

  ---------------------------------------------------------------
  --
  -- Return a list of versions for a specified product.
  -- The values returned are separated by commas.
  --
  ---------------------------------------------------------------
  function get_version_list(p_product_name in varchar2) return varchar2 is
    v_ret varchar2(4000);
  begin
    for r in (select product_version
                from product_version
               where product_name = p_product_name
               order by product_version) loop
      v_ret := v_ret || ',' || r.product_version;
    end loop;
    v_ret := trim(',' from v_ret);
    return v_ret;
  end;

end;
/
