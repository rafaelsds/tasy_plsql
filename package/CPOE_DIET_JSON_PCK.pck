create or replace 
package cpoe_diet_json_pck  as
   	
	function get_diet_message(nr_cpoe_diet_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json;
	
	function get_message_clob(nr_cpoe_diet_p		number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob;
	
	procedure getCpoeIntegracaoDiet(nr_seq_diet_cpoe_p number, nr_entity_identifier_p out number);

end cpoe_diet_json_pck;
/

create or replace 
package body cpoe_diet_json_pck as
		
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			varchar2) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;
	
	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			number) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;

	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			date) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,pkg_date_utils.get_isoformat(value_p));
	end if;	
	end add_json_value;
	
	function get_default_message(	nr_cpoe_diet_p	number ) return philips_json is
	
	json_encounter_w		philips_json;
	json_patient_w			philips_json;
	cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
	json_return_w			philips_json;
	json_allergy_w   		philips_json_list;
	
	nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
	
	begin
	
	json_return_w	:= philips_json();
	
	select max(nr_atendimento)
	into nr_atendimento_w
	from cpoe_dieta
	where nr_sequencia = nr_cpoe_diet_p;
	
	SELECT MAX(CD_PESSOA_FISICA)
	INTO CD_PESSOA_FISICA_W
	FROM ATENDIMENTO_PACIENTE
	WHERE NR_ATENDIMENTO = nr_atendimento_w;
	
	json_encounter_w		:= encounter_json_pck.get_encounter(nr_atendimento_w);
	json_patient_w			:= person_json_pck.get_person(cd_pessoa_fisica_w);
	json_allergy_w			:= person_json_pck.get_allergy(cd_pessoa_fisica_w);
	
	json_return_w.put('patientIdentification',json_patient_w.to_json_value);
	json_return_w.put('patientVisit',json_encounter_w.to_json_value);
	json_return_w.put('patientAllergy',json_allergy_w.to_json_value);
	
	return json_return_w;
	
	end get_default_message;	
	
	function get_fasting_times(nr_cpoe_diet_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	Cursor C01 is
		select 	a.NR_SEQUENCIA NR_SEQUENCIA,
				a.IE_TIPO_DIETA IE_TIPO_DIETA,
				a.DT_LIBERACAO DT_LIBERACAO,
				a.DT_LIB_SUSPENSAO DT_LIB_SUSPENSAO,
				a.DT_INICIO DT_INICIO,
				a.DT_FIM DT_FIM,
				a.IE_DURACAO IE_DURACAO,
				a.IE_URGENCIA IE_URGENCIA,
				a.NR_SEQ_OBJETIVO NR_SEQ_OBJETIVO,
				a.DT_ATUALIZACAO DT_ATUALIZACAO,
				cpoe_obter_dt_suspensao(nr_sequencia,'N') dt_suspensao,
				substr(obter_desc_tipo_jejum(nr_seq_tipo),1,80) desc_tipo_jejum, 
				a.cd_medico ordering_provider_id_number,
				obter_conv_envio('INTERVALO_PRESCRICAO', 'CD_INTERVALO', a.CD_INTERVALO, 'E')  interval_code_value,
				obter_conv_envio('CPOE_DIETA', 'IE_TIPO_DIETA', a.IE_TIPO_DIETA, 'E') diet_type_value,
				decode(a.DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(a.DS_OBSERVACAO)) DS_OBSERVACAO, 
				decode(a.DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(a.DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA, 
				obter_dados_pf(a.cd_medico,'PNG') ordering_provider_given_name,
				obter_dados_pf(a.cd_medico,'PNL') ordering_provider_last_name,
				obter_dados_pf(a.cd_medico,'PNM') ordering_provider_middle_name, 
				Obter_Pessoa_Fisica_Usuario(a.nm_usuario_susp,'C') order_prov_susp_id_number,
				substr(obter_nome_pf(a.cd_medico),1,80) nm_medico_solicitante,
				Obter_Pessoa_Fisica_Usuario(a.nm_usuario_nrec,'C') ordering_user_id_number,
				obter_conv_envio('REP_TIPO_JEJUM', 'NR_SEQUENCIA', a.NR_SEQ_TIPO, 'E')  code_value,
				obter_conv_envio('REP_TIPO_JEJUM', 'NR_SEQUENCIA', a.NR_SEQ_TIPO, 'S')  code_system
		from CPOE_DIETA a
		where a.NR_SEQUENCIA = nr_cpoe_diet_p 
		and a.IE_TIPO_DIETA = 'J';
		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();

		add_json_value(json_item_w, 'nrSequencia', r_c01.nr_sequencia);
		add_json_value(json_item_w, 'ieTipoDieta', r_c01.ie_tipo_dieta);
		add_json_value(json_item_w, 'dtLiberacao', r_c01.dt_liberacao);
		add_json_value(json_item_w, 'dtLibSuspensao', r_c01.dt_lib_suspensao);
		add_json_value(json_item_w, 'dtInicio', r_c01.dt_inicio);
		add_json_value(json_item_w, 'dtFim', r_c01.dt_fim);
		add_json_value(json_item_w, 'ieDuracao', r_c01.ie_duracao);
		add_json_value(json_item_w, 'ieUrgencia', r_c01.ie_urgencia);
		add_json_value(json_item_w, 'nrSeqObjetivo', r_c01.nr_seq_objetivo);
		add_json_value(json_item_w, 'dtAtualizacao', r_c01.dt_atualizacao);	
		add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);		
		add_json_value(json_item_w, 'descDieta', r_c01.desc_tipo_jejum);
		add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);
		add_json_value(json_item_w, 'intervalCodeValue', r_c01.interval_code_value);
		add_json_value(json_item_w, 'dietTypeValue', r_c01.diet_type_value);
		add_json_value(json_item_w, 'dsObservacao', r_c01.ds_observacao);
		add_json_value(json_item_w, 'dsJustificativa', r_c01.ds_justificativa);
		add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ordering_provider_id_number);
		add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
		add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
		add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
		add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
		add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
		add_json_value(json_item_w, 'codeValue', r_c01.code_value);
		add_json_value(json_item_w, 'codeSystem', r_c01.code_system);

		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.dt_fim));
		end if;
		
		add_json_value(json_item_w, 'priority', 'R');
		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.dt_lib_suspensao, r_c01.dt_liberacao));
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		add_json_value(json_item_w, 'testInstruction', r_c01.ds_justificativa || '~' || r_c01.ds_observacao);
		
		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_fasting_times;
	
	function get_oral_diet_times(nr_cpoe_diet_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	Cursor C01 is
		select a.NR_SEQUENCIA,
			a.IE_TIPO_DIETA,  
			a.DT_LIBERACAO, 
			a.DT_LIB_SUSPENSAO, 
			a.DT_INICIO, 
			a.DT_FIM, 
			a.IE_DURACAO, 
			a.IE_URGENCIA, 
			a.NR_SEQ_OBJETIVO,
			a.CD_DIETA,
			a.DT_ATUALIZACAO,
			cpoe_obter_dt_suspensao(a.nr_sequencia,'N') dt_suspensao,
			obter_se_intervalo_sn(a.cd_intervalo)   ie_sn,
			obter_se_intervalo_acm(a.cd_intervalo)  ie_acm,
			obter_se_intervalo_agora(a.cd_intervalo) ie_agora,
			a.CD_INTERVALO,
			a.NR_SEQ_PERFIL_CAL, 
			substr(OBTER_DESC_DIETA(a.CD_DIETA),1,200) desc_tipo_dieta, 
			decode(a.DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(a.DS_OBSERVACAO)) DS_OBSERVACAO, 
			decode(a.DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(a.DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA, 
			obter_conv_envio('CPOE_DIETA', 'IE_TIPO_DIETA', a.IE_TIPO_DIETA, 'E') diet_type_value,
			a.cd_medico ordering_provider_id_number,
			obter_dados_pf(a.cd_medico,'PNG') ordering_provider_given_name,
			obter_dados_pf(a.cd_medico,'PNL') ordering_provider_last_name,
			obter_dados_pf(a.cd_medico,'PNM') ordering_provider_middle_name, 
			substr(obter_nome_pf(a.cd_medico),1,80) nm_medico_solicitante,
			Obter_Pessoa_Fisica_Usuario(a.nm_usuario_nrec,'C') ordering_user_id_number,
			Obter_Pessoa_Fisica_Usuario(a.nm_usuario_susp,'C') order_prov_susp_id_number,		
			obter_conv_envio('DIETA', 'CD_DIETA', a.cd_dieta, 'E')  code_value,
			obter_conv_envio('DIETA', 'CD_DIETA', a.cd_dieta, 'S')  code_system
		from CPOE_DIETA a
		where a.NR_SEQUENCIA = nr_cpoe_diet_p 
		AND a.IE_TIPO_DIETA = 'O';

		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();
		
		add_json_value(json_item_w, 'nrSequencia', r_c01.nr_sequencia);
		add_json_value(json_item_w, 'ieTipoDieta', r_c01.ie_tipo_dieta);
		add_json_value(json_item_w, 'dtLiberacao', r_c01.dt_liberacao);
		add_json_value(json_item_w, 'dtLibSuspensao', r_c01.dt_lib_suspensao);
		add_json_value(json_item_w, 'dtInicio', r_c01.dt_inicio);
		add_json_value(json_item_w, 'dtFim', r_c01.dt_fim);
		add_json_value(json_item_w, 'ieDuracao', r_c01.ie_duracao);
		add_json_value(json_item_w, 'ieUrgencia', r_c01.ie_urgencia);
		add_json_value(json_item_w, 'nrSeqObjetivo', r_c01.nr_seq_objetivo);
		add_json_value(json_item_w, 'cdDieta', r_c01.cd_dieta);
		add_json_value(json_item_w, 'dtAtualizacao', r_c01.dt_atualizacao);	
		add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);	
		add_json_value(json_item_w, 'ieSn', r_c01.ie_sn);
		add_json_value(json_item_w, 'ieAcm', r_c01.ie_acm);
		add_json_value(json_item_w, 'ieAgora', r_c01.ie_agora);
		add_json_value(json_item_w, 'cdIntervalo', r_c01.cd_intervalo);
		add_json_value(json_item_w, 'nrSeqPerfilCal', r_c01.nr_seq_perfil_cal);
		add_json_value(json_item_w, 'descTipoDieta', r_c01.desc_tipo_dieta);
		add_json_value(json_item_w, 'dsObservacao', r_c01.ds_observacao);
		add_json_value(json_item_w, 'dsJustificativa', r_c01.ds_justificativa);
		add_json_value(json_item_w, 'dietTypeValue', r_c01.diet_type_value);
		add_json_value(json_item_w, 'orderingProviderIdNumber', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_provider_id_number));
		add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
		add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
		add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
		add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
		add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);

		add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
		
		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.dt_fim));
		end if;
		
		if (r_c01.ie_sn = 'S' or r_c01.ie_acm = 'S') then
			add_json_value(json_item_w, 'priority', 'PRN');
		elsif (r_c01.ie_agora = 'S' and r_c01.ie_urgencia = '0') then
			add_json_value(json_item_w, 'priority', 'S');
		elsif (r_c01.ie_agora = 'S' and r_c01.ie_urgencia = '5') then
			add_json_value(json_item_w, 'priority', 'TM5');
		elsif (r_c01.ie_agora = 'S' and r_c01.ie_urgencia = '10') then
			add_json_value(json_item_w, 'priority', 'TM10');
		elsif (r_c01.ie_agora = 'S' and r_c01.ie_urgencia = '15') then
			add_json_value(json_item_w, 'priority', 'TM15');
		else
			add_json_value(json_item_w, 'priority', 'R');
		end if;

		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.dt_lib_suspensao, r_c01.dt_liberacao));
		
		if (r_c01.desc_tipo_dieta is null)
			or (r_c01.code_system is null) 
			or (r_c01.code_value is null) then
			begin
				select b.DS_COMPOSICAO desc_tipo_dieta,
					   obter_conv_envio('COMPOSICAO_DIETA', 'NR_SEQUENCIA', b.NR_SEQUENCIA, 'S')  code_system,
					   obter_conv_envio('COMPOSICAO_DIETA', 'NR_SEQUENCIA', b.NR_SEQUENCIA, 'E') code_value
				into r_c01.desc_tipo_dieta,
					r_c01.code_system,
					r_c01.code_value
				from DIETA_COMPOSICAO  a,
					COMPOSICAO_DIETA b 
				where cd_dieta = r_c01.cd_dieta
				and  b.nr_sequencia = a.nr_seq_composicao;
			exception
				when NO_DATA_FOUND then
					r_c01.desc_tipo_dieta := null;
					r_c01.code_value := null;
					r_c01.code_system := null;
			end;
		end if;
		
		add_json_value(json_item_w, 'descDieta', r_c01.desc_tipo_dieta);
		add_json_value(json_item_w, 'codeValue', r_c01.code_value);
		add_json_value(json_item_w, 'codeSystem', r_c01.code_system);
		
		add_json_value(json_item_w, 'testInstruction', r_c01.ds_justificativa || '~' || r_c01.ds_observacao);

		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_oral_diet_times;
	
	
	function get_enteral_diet_times(nr_cpoe_diet_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	Cursor C01 is
		select a.NR_SEQUENCIA,
				a.IE_TIPO_DIETA,  
				a.DT_LIBERACAO requested_date_time, 
				a.DT_LIB_SUSPENSAO suspended_time, 
				a.DT_INICIO, 
				a.DT_FIM, 
				a.DT_ATUALIZACAO,
				a.IE_DURACAO, 
				a.IE_URGENCIA,
				a.IE_ADMINISTRACAO,
				a.NR_SEQ_OBJETIVO,
				a.CD_MATERIAL ,
				a.IE_VIA_APLICACAO,
				a.QT_DOSE,
				a.CD_UNIDADE_MEDIDA_DOSE, 
				a.QT_VOLUME_TOTAL,
				a.QT_TEMPO_APLIC,
				a.QT_VEL_INFUSAO,
				a.IE_TIPO_DOSAGEM ,
				a.IE_BOMBA_INFUSAO, 
				cpoe_obter_dt_suspensao(nr_sequencia,'N') dt_suspensao,
				obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', a.IE_TIPO_DOSAGEM, 'S')  rate_unit_code_system,
				obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', a.IE_TIPO_DOSAGEM, 'E') rate_unit_code_value,
				Obter_Descricao_Dominio(93,a.IE_TIPO_DOSAGEM) rate_unit_text,
				obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', a.IE_BOMBA_INFUSAO, 'S')  infusion_pump_code_system,
				obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', a.IE_BOMBA_INFUSAO, 'E')  infusion_pump_code_value,
				Obter_Descricao_Dominio(1537,a.IE_BOMBA_INFUSAO) infusion_pump_text,
				obter_minutos_hora(a.QT_TEMPO_APLIC)  QT_MINUTE_TEMPO_APLIC,
				obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'S')  minute_code_system,
				obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'E')  minute_code_value,
				obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNIDADE_MEDIDA_DOSE, 'S')  unidade_medida_code_system,
				obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNIDADE_MEDIDA_DOSE, 'E')  unidade_medida_code_value, 
				obter_desc_unidade_medida(a.CD_UNIDADE_MEDIDA_DOSE)  unidade_medida_text,
				obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', a.IE_VIA_APLICACAO, 'S')  via_code_system,
				obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', a.IE_VIA_APLICACAO, 'E')  via_code_value, 
				Obter_Desc_via(a.IE_VIA_APLICACAO)   desc_via,
				obter_conv_envio('INTERVALO_PRESCRICAO', 'CD_INTERVALO', a.CD_INTERVALO, 'E')  interval_code_value,
				decode(a.DS_ORIENTACAO,null,'','. ' ||elimina_acentuacao(a.DS_ORIENTACAO)) DS_ORIENTACAO, 
				a.CD_INTERVALO,
				substr(OBTER_DESC_material(a.CD_MATERIAL),1,200) desc_material, 
				a.cd_medico ordering_provider_id_number,
				decode(a.DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(a.DS_OBSERVACAO)) DS_OBSERVACAO, 
				decode(a.DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(a.DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA, 
				obter_dados_pf(a.cd_medico,'PNG') ordering_provider_given_name,
				obter_dados_pf(a.cd_medico,'PNL') ordering_provider_last_name,
				obter_dados_pf(a.cd_medico,'PNM') ordering_provider_middle_name, 
				substr(obter_nome_pf(a.cd_medico),1,80) nm_medico_solicitante,
				Obter_Pessoa_Fisica_Usuario(a.nm_usuario_nrec,'C') ordering_user_id_number,
				Obter_Pessoa_Fisica_Usuario(a.nm_usuario_susp,'C') order_prov_susp_id_number,
				obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MATERIAL, 'E')  code_value,
				obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MATERIAL, 'S')  code_system
		from CPOE_DIETA a
		where a.NR_SEQUENCIA = nr_cpoe_diet_p 
		AND a.IE_TIPO_DIETA = 'E';


		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();
		
		add_json_value(json_item_w, 'nrSequencia', r_c01.nr_sequencia);
		add_json_value(json_item_w, 'ieTipoDieta', r_c01.ie_tipo_dieta);
		add_json_value(json_item_w, 'requestedDateTime', r_c01.requested_date_time);
		add_json_value(json_item_w, 'suspendedTime', r_c01.suspended_time);
		add_json_value(json_item_w, 'dtInicio', r_c01.dt_inicio);
		add_json_value(json_item_w, 'dtFim', r_c01.dt_fim);
		add_json_value(json_item_w, 'ieDuracao', r_c01.ie_duracao);
		add_json_value(json_item_w, 'ieUrgencia', r_c01.ie_urgencia);
		add_json_value(json_item_w, 'ieAdministracao', r_c01.ie_administracao);
		add_json_value(json_item_w, 'nrSeqObjetivo', r_c01.nr_seq_objetivo);
		add_json_value(json_item_w, 'dtAtualizacao', r_c01.dt_atualizacao);
		add_json_value(json_item_w, 'cdMaterial', r_c01.cd_material);
		add_json_value(json_item_w, 'ieViaAplicacao', r_c01.ie_via_aplicacao);
		add_json_value(json_item_w, 'qtDose', r_c01.qt_dose);
		add_json_value(json_item_w, 'cdUnidadeMedidaDose', r_c01.cd_unidade_medida_dose);
		add_json_value(json_item_w, 'qtVolumeTotal', r_c01.qt_volume_total);
		add_json_value(json_item_w, 'qtTempoAplic', r_c01.qt_tempo_aplic);
		add_json_value(json_item_w, 'qtVelInfusao', r_c01.qt_vel_infusao);
		add_json_value(json_item_w, 'ieTipoDosagem', r_c01.ie_tipo_dosagem);
		add_json_value(json_item_w, 'ieBombaInfusao', r_c01.ie_bomba_infusao);
		add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);
		add_json_value(json_item_w, 'rateUnitCodeSystem', r_c01.rate_unit_code_system);
		add_json_value(json_item_w, 'rateUnitCodeValue', r_c01.rate_unit_code_value);
		add_json_value(json_item_w, 'rateUnitText', r_c01.rate_unit_text);
		add_json_value(json_item_w, 'infusionPumpCodeSystem', r_c01.infusion_pump_code_system);
		add_json_value(json_item_w, 'infusionPumpCodeValue', r_c01.infusion_pump_code_value);
		add_json_value(json_item_w, 'infusionPumpText', r_c01.infusion_pump_text);
		add_json_value(json_item_w, 'qtMinuteTempoAplic', r_c01.qt_minute_tempo_aplic);
		add_json_value(json_item_w, 'minuteCodeSystem', r_c01.minute_code_system);
		add_json_value(json_item_w, 'minuteCodeValue', r_c01.minute_code_value);
		add_json_value(json_item_w, 'unidadeMedidaCodeSystem', r_c01.unidade_medida_code_system);
		add_json_value(json_item_w, 'unidadeMedidaCodeValue', r_c01.unidade_medida_code_value);
		add_json_value(json_item_w, 'unidadeMedidaText', r_c01.unidade_medida_text);
		add_json_value(json_item_w, 'viaCodeSystem', r_c01.via_code_system);
		add_json_value(json_item_w, 'viaCodeValue', r_c01.via_code_value);
		add_json_value(json_item_w, 'descVia', r_c01.desc_via);
		add_json_value(json_item_w, 'intervalCodeValue', r_c01.interval_code_value);
		add_json_value(json_item_w, 'dsOrientacao', r_c01.ds_orientacao);
		add_json_value(json_item_w, 'cdIntervalo', r_c01.cd_intervalo);
		add_json_value(json_item_w, 'descMaterial', r_c01.desc_material);
		add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ordering_provider_id_number);
		add_json_value(json_item_w, 'dsObservacao', r_c01.ds_observacao);
		add_json_value(json_item_w, 'dsJustificativa', r_c01.ds_justificativa);
		add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
		add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
		add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
		add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
		add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);
		add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
		add_json_value(json_item_w, 'codeValue', r_c01.code_value);
		add_json_value(json_item_w, 'codeSystem', r_c01.code_system);
				
		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.dt_fim));
		end if;
		
		if (r_c01.ie_administracao in ('N', 'C')) then
			add_json_value(json_item_w, 'priority', 'PRN');
		elsif (r_c01.ie_urgencia = '0') then
			add_json_value(json_item_w, 'priority', 'S');
		elsif (r_c01.ie_urgencia = '5') then
			add_json_value(json_item_w, 'priority', 'TM5');
		elsif (r_c01.ie_urgencia = '10') then
			add_json_value(json_item_w, 'priority', 'TM10');
		elsif (r_c01.ie_urgencia = '15') then
			add_json_value(json_item_w, 'priority', 'TM15');
		else
			add_json_value(json_item_w, 'priority', 'R');
		end if;

		add_json_value(json_item_w, 'occurrenceDurationId', (r_c01.minute_code_value || r_c01.qt_minute_tempo_aplic));
		add_json_value(json_item_w, 'occurrenceDurationCodeSystem', r_c01.minute_code_system);
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.suspended_time, r_c01.requested_date_time));

		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_enteral_diet_times;
	
	
	function get_supplement_diet_times(nr_cpoe_diet_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	json_component_item_list_w	philips_json_list;	
	json_component_item_w		philips_json;
	
	Cursor C01 is
		select a.NR_SEQUENCIA,
			a.IE_TIPO_DIETA,  
			a.DT_INICIO, 
			a.DT_FIM, 
			a.DT_ATUALIZACAO,
			a.IE_DURACAO, 
			a.IE_URGENCIA,
			a.IE_ADMINISTRACAO, 
			a.NR_SEQ_OBJETIVO,
			a.CD_MATERIAL ,
			a.IE_VIA_APLICACAO,
			a.QT_DOSE,
			a.CD_UNIDADE_MEDIDA_DOSE, 
			a.QT_VOLUME_TOTAL,
			a.QT_TEMPO_APLIC,
			a.QT_VEL_INFUSAO,
			a.IE_TIPO_DOSAGEM ,
			a.IE_BOMBA_INFUSAO, 
			a.CD_INTERVALO,
			a.DT_LIBERACAO       requested_date_time, 
			a.DT_LIB_SUSPENSAO   suspended_time, 
			cpoe_obter_dt_suspensao(a.nr_sequencia,'N') dt_suspensao,
			substr(OBTER_DESC_material(a.CD_MATERIAL),1,200) desc_material, 
			obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', a.IE_TIPO_DOSAGEM, 'S')  rate_unit_code_system,
			obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', a.IE_TIPO_DOSAGEM, 'E')  rate_unit_code_value,
			Obter_Descricao_Dominio(93, a.IE_TIPO_DOSAGEM) rate_unit_text,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNIDADE_MEDIDA_DOSE, 'S')  unidade_medida_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNIDADE_MEDIDA_DOSE, 'E')  unidade_medida_code_value, 
			obter_desc_unidade_medida(a.CD_UNIDADE_MEDIDA_DOSE)  unidade_medida_text,
			obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', a.IE_VIA_APLICACAO, 'S')  via_code_system,
			obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', a.IE_VIA_APLICACAO, 'E')  via_code_value, 
			Obter_Desc_via(a.IE_VIA_APLICACAO)   desc_via,
			obter_minutos_hora(a.QT_TEMPO_APLIC)  QT_MINUTE_TEMPO_APLIC,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'S')  minute_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'E')  minute_code_value,
			obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', a.IE_BOMBA_INFUSAO, 'S')  infusion_pump_code_system,
			obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', a.IE_BOMBA_INFUSAO, 'E')  infusion_pump_code_value,
			Obter_Descricao_Dominio(1537, a.IE_BOMBA_INFUSAO) infusion_pump_text,
			-- Diluente
			substr(OBTER_DESC_material(a.CD_MAT_PROD1),1,200) desc_diluente,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD1, 'E')  dil_code_value,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD1, 'S')  dil_code_system,
			a.QT_DOSE_PROD1 QT_DOSE_DIL,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_DOSE_DIL, 'S')  unid_dil_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_DOSE_DIL, 'E')  unid_dil_code_value, 
			obter_desc_unidade_medida(a.CD_UNID_MED_DOSE_DIL)  unid_dil_text,
			-- Diluente
			obter_conv_envio('INTERVALO_PRESCRICAO', 'CD_INTERVALO', a.CD_INTERVALO, 'E')  interval_code_value,     
			decode(a.DS_ORIENTACAO,null,'','. ' ||elimina_acentuacao(a.DS_ORIENTACAO)) DS_ORIENTACAO,  
			decode(a.DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(a.DS_OBSERVACAO)) DS_OBSERVACAO, 
			decode(a.DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(a.DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA, 
			a.cd_medico ordering_provider_id_number,
			obter_dados_pf(a.cd_medico,'PNG') ordering_provider_given_name,
			obter_dados_pf(a.cd_medico,'PNL') ordering_provider_last_name,
			obter_dados_pf(a.cd_medico,'PNM') ordering_provider_middle_name, 
			substr(obter_nome_pf(a.cd_medico),1,80) nm_medico_solicitante,
			Obter_Pessoa_Fisica_Usuario(a.nm_usuario_nrec,'C') ordering_user_id_number,
			Obter_Pessoa_Fisica_Usuario(a.nm_usuario_susp,'C') order_prov_susp_id_number,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MATERIAL, 'E')  code_value,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MATERIAL, 'S')  code_system
		from CPOE_DIETA a
		where NR_SEQUENCIA = nr_cpoe_diet_p 
		AND IE_TIPO_DIETA = 'S';
		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();
		
		add_json_value(json_item_w, 'nrSequencia', r_c01.nr_sequencia);
		add_json_value(json_item_w, 'ieTipoDieta', r_c01.ie_tipo_dieta);
		add_json_value(json_item_w, 'dtInicio', r_c01.dt_inicio);
		add_json_value(json_item_w, 'dtFim', r_c01.dt_fim);
		add_json_value(json_item_w, 'dtAtualizacao', r_c01.dt_atualizacao);
		add_json_value(json_item_w, 'ieDuracao', r_c01.ie_duracao);
		add_json_value(json_item_w, 'ieUrgencia', r_c01.ie_urgencia);
		add_json_value(json_item_w, 'ieAdministracao', r_c01.ie_administracao);
		add_json_value(json_item_w, 'nrSeqObjetivo', r_c01.nr_seq_objetivo);
		add_json_value(json_item_w, 'cdMaterial', r_c01.cd_material);
		add_json_value(json_item_w, 'ieViaAplicacao', r_c01.ie_via_aplicacao);
		add_json_value(json_item_w, 'qtDose', r_c01.qt_dose);
		add_json_value(json_item_w, 'cdUnidadeMedidaDose', r_c01.cd_unidade_medida_dose);
		add_json_value(json_item_w, 'qtVolumeTotal', r_c01.qt_volume_total);
		add_json_value(json_item_w, 'qtTempoAplic', r_c01.qt_tempo_aplic);
		add_json_value(json_item_w, 'qtVelInfusao', r_c01.qt_vel_infusao);
		add_json_value(json_item_w, 'ieTipoDosagem', r_c01.ie_tipo_dosagem);
		add_json_value(json_item_w, 'ieBombaInfusao', r_c01.ie_bomba_infusao);
		add_json_value(json_item_w, 'cdIntervalo', r_c01.cd_intervalo);
		add_json_value(json_item_w, 'requestedDateTime', r_c01.requested_date_time);
		add_json_value(json_item_w, 'suspendedTime', r_c01.suspended_time);
		add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);
		add_json_value(json_item_w, 'descMaterial', r_c01.desc_material);
		add_json_value(json_item_w, 'rateUnitCodeSystem', r_c01.rate_unit_code_system);
		add_json_value(json_item_w, 'rateUnitCodeValue', r_c01.rate_unit_code_value);
		add_json_value(json_item_w, 'rateUnitText', r_c01.rate_unit_text);
		add_json_value(json_item_w, 'unidadeMedidaCodeSystem', r_c01.unidade_medida_code_system);
		add_json_value(json_item_w, 'unidadeMedidaCodeValue', r_c01.unidade_medida_code_value);
		add_json_value(json_item_w, 'unidadeMedidaText', r_c01.unidade_medida_text);
		add_json_value(json_item_w, 'viaCodeSystem', r_c01.via_code_system);
		add_json_value(json_item_w, 'viaCodeValue', r_c01.via_code_value);
		add_json_value(json_item_w, 'descVia', r_c01.desc_via);
		add_json_value(json_item_w, 'qtMinuteTempoAplic', r_c01.qt_minute_tempo_aplic);
		add_json_value(json_item_w, 'minuteCodeSystem', r_c01.minute_code_system);
		add_json_value(json_item_w, 'minuteCodeValue', r_c01.minute_code_value);
		add_json_value(json_item_w, 'infusionPumpCodeSystem', r_c01.infusion_pump_code_system);
		add_json_value(json_item_w, 'infusionPumpCodeValue', r_c01.infusion_pump_code_value);
		add_json_value(json_item_w, 'infusionPumpText', r_c01.infusion_pump_text);
		
		add_json_value(json_item_w, 'intervalCodeValue', r_c01.interval_code_value);
		add_json_value(json_item_w, 'dsOrientacao', r_c01.ds_orientacao);
		add_json_value(json_item_w, 'dsObservacao', r_c01.ds_observacao);
		add_json_value(json_item_w, 'dsJustificativa', r_c01.ds_justificativa);
		add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ordering_provider_id_number);
		add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
		add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
		add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
		add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
		add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);
		add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
		add_json_value(json_item_w, 'codeValue', r_c01.code_value);
		add_json_value(json_item_w, 'codeSystem', r_c01.code_system);
		
		
		json_component_item_list_w	:= philips_json_list();	
		
		-- Componente principal
		json_component_item_w		:= philips_json();
		add_json_value(json_component_item_w, 'componentType', 'A');
		add_json_value(json_component_item_w, 'codeValue', r_c01.code_value);
		add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material);
		add_json_value(json_component_item_w, 'codeSystem', r_c01.code_system);
		add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose);
		add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unidade_medida_code_value);
		add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_text);
		add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unidade_medida_code_system);
		json_component_item_list_w.append(json_component_item_w.to_json_value());
		
		-- Diluente
		if (r_c01.qt_dose_dil is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'B');
			add_json_value(json_component_item_w, 'codeValue', r_c01.dil_code_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_diluente);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.dil_code_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_dil);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unid_dil_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unid_dil_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unid_dil_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;
		
		json_item_w.put('componentList', json_component_item_list_w.to_json_value());
		
		
		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.dt_fim));
		end if;
		
		if (r_c01.ie_administracao in ('N', 'C')) then
			add_json_value(json_item_w, 'priority', 'PRN');
		elsif (r_c01.ie_urgencia = '0') then
			add_json_value(json_item_w, 'priority', 'S');
		elsif (r_c01.ie_urgencia = '5') then
			add_json_value(json_item_w, 'priority', 'TM5');
		elsif (r_c01.ie_urgencia = '10') then
			add_json_value(json_item_w, 'priority', 'TM10');
		elsif (r_c01.ie_urgencia = '15') then
			add_json_value(json_item_w, 'priority', 'TM15');
		else
			add_json_value(json_item_w, 'priority', 'R');
		end if;

		add_json_value(json_item_w, 'occurrenceDurationId', (r_c01.minute_code_value || r_c01.qt_minute_tempo_aplic));
		add_json_value(json_item_w, 'occurrenceDurationCodeSystem', r_c01.minute_code_system);
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.suspended_time, r_c01.requested_date_time));

		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_supplement_diet_times;
	
	
	function get_milk_diet_times(nr_cpoe_diet_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	json_component_item_list_w	philips_json_list;	
	json_component_item_w		philips_json;
	
	Cursor C01 is
		select a.NR_SEQUENCIA,
			a.IE_TIPO_DIETA,  
			a.DT_INICIO, 
			a.DT_FIM, 
			a.DT_ATUALIZACAO,
			a.DT_LIBERACAO        requested_date_time, 
			a.DT_LIB_SUSPENSAO      suspended_time, 
			a.IE_DURACAO, 
			a.IE_URGENCIA, 
			a.Ie_administracao,
			a.NR_SEQ_OBJETIVO,
			a.CD_MATERIAL ,
			a.IE_VIA_APLICACAO,
			a.CD_UNID_MED_PROD1, 
			a.QT_VOLUME_TOTAL,
			a.QT_TEMPO_APLIC,
			a.QT_VEL_INFUSAO,
			a.IE_TIPO_DOSAGEM ,
			a.IE_BOMBA_INFUSAO, 
			a.CD_INTERVALO,
			a.QT_DOSE_PROD1 QT_DOSE,
			cpoe_obter_dt_suspensao(a.nr_sequencia,'N') dt_suspensao,
			substr(OBTER_DESC_material(a.CD_MAT_PROD1),1,200) desc_material, 
			obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', a.IE_TIPO_DOSAGEM, 'S')  rate_unit_code_system,
			obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', a.IE_TIPO_DOSAGEM, 'E')  rate_unit_code_value,
			Obter_Descricao_Dominio(93,a.IE_TIPO_DOSAGEM) rate_unit_text,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD1, 'S')  unidade_medida_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD1, 'E')  unidade_medida_code_value, 
			obter_desc_unidade_medida(a.CD_UNID_MED_PROD1)  unidade_medida_text,
			obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', a.IE_VIA_LEITE1 , 'S')  via_code_system,
			obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', a.IE_VIA_LEITE1 , 'E')  via_code_value, 
			Obter_Desc_via(a.IE_VIA_LEITE1)   desc_via,
			obter_minutos_hora(a.QT_TEMPO_APLIC)  QT_MINUTE_TEMPO_APLIC,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'S')  minute_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'E')  minute_code_value,
			-- Item adicional 1
			substr(OBTER_DESC_material(a.CD_MAT_PROD_ADIC1  ),1,200) desc_item_adic1,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD_ADIC1, 'E')  item_adic1_code_value,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD_ADIC1, 'S')  item_adic1_code_system,
			a.QT_DOSE_PROD_ADIC1,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD_ADIC1, 'S')  item_adic1_unid_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD_ADIC1, 'E')  item_adic1_unid_code_value, 
			obter_desc_unidade_medida(a.CD_UNID_MED_PROD_ADIC1)  item_adic1_unid_text,
			-- Item adicional 2
			substr(OBTER_DESC_material(a.CD_MAT_PROD_ADIC2),1,200) desc_item_adic2,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD_ADIC2, 'E')  item_adic2_code_value,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD_ADIC2, 'S')  item_adic2_code_system,
			a.QT_DOSE_PROD_ADIC2,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD_ADIC2, 'S')  item_adic2_unid_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD_ADIC2, 'E')  item_adic2_unid__code_value, 
			obter_desc_unidade_medida(a.CD_UNID_MED_PROD_ADIC2)  item_adic2_unid_text,
			-- Item adicional 3
			substr(OBTER_DESC_material(a.CD_MAT_PROD_ADIC3),1,200) desc_item_adic3,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD_ADIC2, 'E')  item_adic3_code_value,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD_ADIC2, 'S')  item_adic3_code_system,
			a.QT_DOSE_PROD_ADIC3,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD_ADIC3, 'S')  item_adic3_unid_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD_ADIC3, 'E')  item_adic3_unid_code_value, 
			obter_desc_unidade_medida(CD_UNID_MED_PROD_ADIC3)  item_adic3_unid_text,
			-- Item adicional 4
			substr(OBTER_DESC_material(a.CD_MAT_PROD_ADIC4),1,200) desc_item_adic4,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD_ADIC4, 'E')  item_adic4_code_value,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD_ADIC4, 'S')  item_adic4_code_system,
			a.QT_DOSE_PROD_ADIC4,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD_ADIC4, 'S')  item_adic4_unid_code_system,
			obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', a.CD_UNID_MED_PROD_ADIC4, 'E')  item_adic4_unid_code_value, 
			obter_desc_unidade_medida(a.CD_UNID_MED_PROD_ADIC4)  item_adic4_unid_text,
      --
			obter_conv_envio('INTERVALO_PRESCRICAO', 'CD_INTERVALO', a.CD_INTERVALO, 'E')  interval_code_value,     
			decode(a.DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(a.DS_OBSERVACAO)) DS_OBSERVACAO, 
			decode(a.DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(a.DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA, 
			a.cd_medico ordering_provider_id_number,
			obter_dados_pf(a.cd_medico,'PNG') ordering_provider_given_name,
			obter_dados_pf(a.cd_medico,'PNL') ordering_provider_last_name,
			obter_dados_pf(a.cd_medico,'PNM') ordering_provider_middle_name, 
			substr(obter_nome_pf(a.cd_medico),1,80) nm_medico_solicitante,
			Obter_Pessoa_Fisica_Usuario(a.nm_usuario_nrec,'C') ordering_user_id_number,
			Obter_Pessoa_Fisica_Usuario(a.nm_usuario_susp,'C') order_prov_susp_id_number,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD1, 'E')  code_value,
			obter_conv_envio('MATERIAL', 'CD_MATERIAL', a.CD_MAT_PROD1, 'S')  code_system
		from CPOE_DIETA a
		where a.NR_SEQUENCIA = nr_cpoe_diet_p 
		AND a.IE_TIPO_DIETA = 'L'
		;
		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();
		
		add_json_value(json_item_w, 'nrSequencia', r_c01.nr_sequencia);		
		add_json_value(json_item_w, 'ieTipoDieta', r_c01.ie_tipo_dieta);
		add_json_value(json_item_w, 'dtInicio', r_c01.dt_inicio);
		add_json_value(json_item_w, 'dtIim', r_c01.dt_fim);
		add_json_value(json_item_w, 'dtAtualizacao', r_c01.dt_atualizacao);
		add_json_value(json_item_w, 'requestedDateTime', r_c01.requested_date_time);
		add_json_value(json_item_w, 'suspendedTime', r_c01.suspended_time);
		add_json_value(json_item_w, 'ieDuracao', r_c01.ie_duracao);
		add_json_value(json_item_w, 'ieUrgencia', r_c01.ie_urgencia);
		add_json_value(json_item_w, 'ieAdministracao', r_c01.ie_administracao);
		add_json_value(json_item_w, 'nrSeqObjetivo', r_c01.nr_seq_objetivo);
		add_json_value(json_item_w, 'cdMaterial', r_c01.cd_material);
		add_json_value(json_item_w, 'ieViaAplicacao', r_c01.ie_via_aplicacao);
		add_json_value(json_item_w, 'cdUnidMedProd1', r_c01.cd_unid_med_prod1);
		add_json_value(json_item_w, 'qtVolumeTotal', r_c01.qt_volume_total);
		add_json_value(json_item_w, 'qtTempoAplic', r_c01.qt_tempo_aplic);
		add_json_value(json_item_w, 'qtVelInfusao', r_c01.qt_vel_infusao);
		add_json_value(json_item_w, 'ieTipoDosagem', r_c01.ie_tipo_dosagem);
		add_json_value(json_item_w, 'ieBombaInfusao', r_c01.ie_bomba_infusao);
		add_json_value(json_item_w, 'cdIntervalo', r_c01.cd_intervalo);
		add_json_value(json_item_w, 'qtDose', r_c01.qt_dose);
		add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);
		add_json_value(json_item_w, 'descMaterial', r_c01.desc_material);
		add_json_value(json_item_w, 'rateUnitCodeSystem', r_c01.rate_unit_code_system);
		add_json_value(json_item_w, 'rateUnitCodeValue', r_c01.rate_unit_code_value);
		add_json_value(json_item_w, 'rateUnitText', r_c01.rate_unit_text);
		add_json_value(json_item_w, 'unidadeMedidaCodeSystem', r_c01.unidade_medida_code_system);
		add_json_value(json_item_w, 'unidadeMedidaCodeValue', r_c01.unidade_medida_code_value);
		add_json_value(json_item_w, 'unidadeMedidaText', r_c01.unidade_medida_text);
		add_json_value(json_item_w, 'viaCodeSystem', r_c01.via_code_system);
		add_json_value(json_item_w, 'viaCodeValue', r_c01.via_code_value);
		add_json_value(json_item_w, 'descVia', r_c01.desc_via);
		add_json_value(json_item_w, 'qtMinuteTempoAplic', r_c01.qt_minute_tempo_aplic);
		add_json_value(json_item_w, 'minuteCodeSystem', r_c01.minute_code_system);
		add_json_value(json_item_w, 'minuteCodeValue', r_c01.minute_code_value);
		
		add_json_value(json_item_w, 'intervalCodeValue', r_c01.interval_code_value);
		add_json_value(json_item_w, 'dsObservacao', r_c01.ds_observacao);
		add_json_value(json_item_w, 'dsJustificativa', r_c01.ds_justificativa);
		add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ordering_provider_id_number);
		add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
		add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
		add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
		add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
		add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);
		add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
		add_json_value(json_item_w, 'codeValue', r_c01.code_value);
		add_json_value(json_item_w, 'codeSystem', r_c01.code_system);
		
		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.dt_fim));
		end if;
		
		if (r_c01.ie_administracao in ('N')) then
			add_json_value(json_item_w, 'priority', 'PRN');
		elsif (r_c01.ie_urgencia = '0') then
			add_json_value(json_item_w, 'priority', 'S');
		elsif (r_c01.ie_urgencia = '5') then
			add_json_value(json_item_w, 'priority', 'TM5');
		elsif (r_c01.ie_urgencia = '10') then
			add_json_value(json_item_w, 'priority', 'TM10');
		elsif (r_c01.ie_urgencia = '15') then
			add_json_value(json_item_w, 'priority', 'TM15');
		else
			add_json_value(json_item_w, 'priority', 'R');
		end if;


		json_component_item_list_w	:= philips_json_list();	
		
		-- Componente principal
		json_component_item_w		:= philips_json();
		add_json_value(json_component_item_w, 'componentType', 'A');
		add_json_value(json_component_item_w, 'codeValue', r_c01.code_value);
		add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_material);
		add_json_value(json_component_item_w, 'codeSystem', r_c01.code_system);
		add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose);
		add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.unidade_medida_code_value);
		add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.unidade_medida_text);
		add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.unidade_medida_code_system);
		json_component_item_list_w.append(json_component_item_w.to_json_value());
		
		-- Componente adicional 1
		if (r_c01.qt_dose_prod_adic1 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.item_adic1_code_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_item_adic1);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.item_adic1_code_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_prod_adic1);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.item_adic1_unid_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.item_adic1_unid_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.item_adic1_unid_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;
		
		-- adicional 2
		if (r_c01.qt_dose_prod_adic2 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.item_adic2_code_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_item_adic2);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.item_adic2_code_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_prod_adic2);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.item_adic2_unid__code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.item_adic2_unid_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.item_adic2_unid_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;
		
		-- adicional 3
		if (r_c01.qt_dose_prod_adic3 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.item_adic3_code_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_item_adic3);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.item_adic3_code_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_prod_adic3);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.item_adic3_unid_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.item_adic3_unid_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.item_adic3_unid_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;
		
		-- adicional 4
		if (r_c01.qt_dose_prod_adic4 is not null) then
			json_component_item_w		:= philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c01.item_adic4_code_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c01.desc_item_adic4);
			add_json_value(json_component_item_w, 'codeSystem', r_c01.item_adic4_code_system);
			add_json_value(json_component_item_w, 'qtDose', r_c01.qt_dose_prod_adic4);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c01.item_adic4_unid_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c01.item_adic4_unid_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c01.item_adic4_unid_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
		end if;
		
		json_item_w.put('componentList', json_component_item_list_w.to_json_value());

		
		add_json_value(json_item_w, 'occurrenceDurationId', (r_c01.minute_code_value || r_c01.qt_minute_tempo_aplic));
		add_json_value(json_item_w, 'occurrenceDurationCodeSystem', r_c01.minute_code_system);
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.suspended_time, r_c01.requested_date_time));

		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_milk_diet_times;
	
	
	function get_npt_adult_diet_times(nr_cpoe_diet_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	json_component_item_list_w	philips_json_list;	
	json_component_item_w		philips_json;
	
	Cursor C01 is
		select NR_SEQUENCIA,
			   IE_TIPO_DIETA,  
			   DT_INICIO, 
			   DT_FIM, 
			   DT_ATUALIZACAO,
			   IE_DURACAO, 
			   IE_URGENCIA,
			   IE_ADMINISTRACAO, 
			   NR_SEQ_OBJETIVO,
			   CD_MATERIAL,
			   IE_VIA_APLICACAO,
			   QT_DOSE,
			   CD_UNIDADE_MEDIDA_DOSE, 
			   QT_VOLUME_TOTAL,
			   QT_TEMPO_APLIC,
			   QT_GOTEJO_NPT,
			   IE_TIPO_DOSAGEM ,
			   IE_BOMBA_INFUSAO, 
			   DT_LIBERACAO          requested_date_time, 
			   DT_LIB_SUSPENSAO      suspended_time, 
			   cpoe_obter_dt_suspensao(nr_sequencia,'N') dt_suspensao,
			   obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', IE_TIPO_DOSAGEM, 'S')  rate_unit_code_system,
			   obter_conv_envio('CPOE_MATERIAL', 'IE_TIPO_DOSAGEM', IE_TIPO_DOSAGEM, 'E')  rate_unit_code_value,
			   Obter_Descricao_Dominio(93,IE_TIPO_DOSAGEM) rate_unit_text,
			   obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'S')  minute_code_system,
			   obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'E')  minute_code_value,
			   obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO, 'S')  infusion_pump_code_system,
			   obter_conv_envio('CPOE_MATERIAL', 'IE_BOMBA_INFUSAO', IE_BOMBA_INFUSAO, 'E')  infusion_pump_code_value,
			   Obter_Descricao_Dominio(1537,IE_BOMBA_INFUSAO) infusion_pump_text,
			   obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', IE_VIA_APLICACAO, 'S')  via_code_system,
			   obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', IE_VIA_APLICACAO, 'E')  via_code_value, 
			   Obter_Desc_via(IE_VIA_APLICACAO)   desc_via, 
			   decode(a.DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(DS_OBSERVACAO)) DS_OBSERVACAO, 
			   decode(a.DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA, 
			   a.cd_medico ordering_provider_id_number,
			   obter_dados_pf(cd_medico,'PNG') ordering_provider_given_name,
			   obter_dados_pf(cd_medico,'PNL') ordering_provider_last_name,
			   obter_dados_pf(cd_medico,'PNM') ordering_provider_middle_name, 
			   substr(obter_nome_pf(a.cd_medico),1,80) nm_medico_solicitante,
			   Obter_Pessoa_Fisica_Usuario(nm_usuario_nrec,'C') ordering_user_id_number,
			   Obter_Pessoa_Fisica_Usuario(nm_usuario_susp,'C') order_prov_susp_id_number,
			   obter_minutos_hora(a.QT_TEMPO_APLIC)  QT_MINUTE_TEMPO_APLIC
		from CPOE_DIETA a
		where NR_SEQUENCIA = nr_cpoe_diet_p 
		AND IE_TIPO_DIETA = 'P';
		
	Cursor c02 is
		select 	obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MATERIAL, 'E')  elemento_code_value,
				obter_conv_envio('MATERIAL', 'CD_MATERIAL', CD_MATERIAL, 'S')  elemento_code_system,
				substr(OBTER_DESC_material(CD_MATERIAL),1,200) desc_material, 
				obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNIDADE_MEDIDA, 'S')  unidade_medida_code_system,
				obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', CD_UNIDADE_MEDIDA, 'E')  unidade_medida_code_value, 
				obter_desc_unidade_medida(CD_UNIDADE_MEDIDA)  unidade_medida_text, 
				QT_DOSE
		from  CPOE_NPT_PRODUTO
		where NR_SEQ_NPT_CPOE = nr_cpoe_diet_p;
		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();
		
		add_json_value(json_item_w, 'nrSequencia', r_c01.nr_sequencia);		
		add_json_value(json_item_w, 'ieTipoDieta', r_c01.ie_tipo_dieta);
		add_json_value(json_item_w, 'dtInicio', r_c01.dt_inicio);
		add_json_value(json_item_w, 'dtIim', r_c01.dt_fim);
		add_json_value(json_item_w, 'dtAtualizacao', r_c01.dt_atualizacao);
		add_json_value(json_item_w, 'ieDuracao', r_c01.ie_duracao);
		add_json_value(json_item_w, 'ieUrgencia', r_c01.ie_urgencia);
		add_json_value(json_item_w, 'ieAdministracao', r_c01.ie_administracao);
		add_json_value(json_item_w, 'nrSeqObjetivo', r_c01.nr_seq_objetivo);
		add_json_value(json_item_w, 'cdMaterial', r_c01.cd_material);
		add_json_value(json_item_w, 'ieViaAplicacao', r_c01.ie_via_aplicacao);
		add_json_value(json_item_w, 'qtDose', r_c01.qt_dose);
		add_json_value(json_item_w, 'cdUnidadeMedidaDose', r_c01.cd_unidade_medida_dose);
		add_json_value(json_item_w, 'qtVolumeTotal', r_c01.qt_volume_total);
		add_json_value(json_item_w, 'qtTempoAplic', r_c01.qt_tempo_aplic);
		add_json_value(json_item_w, 'qtVelInfusao', r_c01.qt_gotejo_npt);
		add_json_value(json_item_w, 'ieTipoDosagem', r_c01.ie_tipo_dosagem);
		add_json_value(json_item_w, 'ieBombaInfusao', r_c01.ie_bomba_infusao);
		add_json_value(json_item_w, 'requestedDateTime', r_c01.requested_date_time);
		add_json_value(json_item_w, 'suspendedTime', r_c01.suspended_time);
		add_json_value(json_item_w, 'dtSuspensao', r_c01.dt_suspensao);
		add_json_value(json_item_w, 'rateUnitCodeSystem', r_c01.rate_unit_code_system);
		add_json_value(json_item_w, 'rateUnitCodeValue', r_c01.rate_unit_code_value);
		add_json_value(json_item_w, 'rateUnitText', r_c01.rate_unit_text);
		add_json_value(json_item_w, 'minuteCodeSystem', r_c01.minute_code_system);
		add_json_value(json_item_w, 'minuteCodeValue', r_c01.minute_code_value);
		
		add_json_value(json_item_w, 'infusionPumpCodeSystem', r_c01.infusion_pump_code_system);
		add_json_value(json_item_w, 'infusionPumpCodeValue', r_c01.infusion_pump_code_value);
		add_json_value(json_item_w, 'infusionPumpText', r_c01.infusion_pump_text);
		
		add_json_value(json_item_w, 'viaCodeSystem', r_c01.via_code_system);
		add_json_value(json_item_w, 'viaCodeValue', r_c01.via_code_value);
		add_json_value(json_item_w, 'descVia', r_c01.desc_via);
		add_json_value(json_item_w, 'dsObservacao', r_c01.ds_observacao);
		add_json_value(json_item_w, 'dsJustificativa', r_c01.ds_justificativa);
		add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ordering_provider_id_number);
		add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ordering_provider_given_name);
		add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ordering_provider_last_name);
		add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ordering_provider_middle_name);
		add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.nm_medico_solicitante);
		add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ordering_user_id_number);
		add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.order_prov_susp_id_number);
				
		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.dt_inicio, r_c01.dt_fim));
		end if;
		
		if (r_c01.ie_administracao in ('C')) then
			add_json_value(json_item_w, 'priority', 'PRN');
		else
			add_json_value(json_item_w, 'priority', 'R');
		end if;


		json_component_item_list_w	:= philips_json_list();	
		
		-- Produtos-Elementos NPT
		
		for r_c02 in c02 loop
			begin
			
			json_component_item_w := philips_json();
			add_json_value(json_component_item_w, 'componentType', 'A');
			add_json_value(json_component_item_w, 'codeValue', r_c02.elemento_code_value);
			add_json_value(json_component_item_w, 'descMaterial', r_c02.desc_material);
			add_json_value(json_component_item_w, 'codeSystem', r_c02.elemento_code_system);
			add_json_value(json_component_item_w, 'qtDose', r_c02.qt_dose);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeValue', r_c02.unidade_medida_code_value);
			add_json_value(json_component_item_w, 'unidadeMedidaText', r_c02.unidade_medida_text);
			add_json_value(json_component_item_w, 'unidadeMedidaCodeSystem', r_c02.unidade_medida_code_system);
			json_component_item_list_w.append(json_component_item_w.to_json_value());
			
			end;
		end loop;
		
		json_item_w.put('componentList', json_component_item_list_w.to_json_value());

		
		add_json_value(json_item_w, 'occurrenceDurationId', (r_c01.minute_code_value || r_c01.qt_minute_tempo_aplic));
		add_json_value(json_item_w, 'occurrenceDurationCodeSystem', r_c01.minute_code_system);
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.suspended_time, r_c01.requested_date_time));

		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_npt_adult_diet_times;
	
	
	function get_diet_message(nr_cpoe_diet_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json is
	json_return_w		philips_json;
	json_item_list_w   	philips_json_list;
	ie_tipo_dieta_w	cpoe_dieta.ie_tipo_dieta%type;
	ie_hl7_msg_type_w	varchar(3);
	begin
	
	select ie_tipo_dieta
	into ie_tipo_dieta_w
	from cpoe_dieta
	where nr_sequencia = nr_cpoe_diet_p;
	
	if (ie_tipo_dieta_w = 'J') then
		json_item_list_w	:= get_fasting_times(nr_cpoe_diet_p);
		ie_hl7_msg_type_w := 'ORM';
	elsif (ie_tipo_dieta_w = 'O') then
		json_item_list_w	:= get_oral_diet_times(nr_cpoe_diet_p);
		ie_hl7_msg_type_w := 'ORM';
	elsif (ie_tipo_dieta_w = 'E') then
		json_item_list_w	:= get_enteral_diet_times(nr_cpoe_diet_p);
		ie_hl7_msg_type_w := 'OMP';
	elsif (ie_tipo_dieta_w = 'S') then
		json_item_list_w	:= get_supplement_diet_times(nr_cpoe_diet_p);
		ie_hl7_msg_type_w := 'OMP';
	elsif (ie_tipo_dieta_w = 'L') then
		json_item_list_w	:= get_milk_diet_times(nr_cpoe_diet_p);
		ie_hl7_msg_type_w := 'OMP';
	elsif (ie_tipo_dieta_w = 'P') then
		json_item_list_w	:= get_npt_adult_diet_times(nr_cpoe_diet_p);
		ie_hl7_msg_type_w := 'OMP';
	end if;
	
	if	(json_item_list_w.count > 0) then
		json_return_w	:= philips_json();
		json_return_w		:= get_default_message(nr_cpoe_diet_p);
		add_json_value(json_return_w, 'orderControl', ie_order_control_p);
		add_json_value(json_return_w, 'entityidentifier', nr_entity_identifier_p);
		add_json_value(json_return_w, 'hl7MsgType', ie_hl7_msg_type_w);
		
		json_return_w.put('dietList', json_item_list_w.to_json_value());
	end if;
	
	return json_return_w;
	
	end get_diet_message;
					

	function get_message_clob(nr_cpoe_diet_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob is
	ds_json_out_w		clob;
	json_diet_w	philips_json;
	
	begin
		
	json_diet_w		:= get_diet_message(nr_cpoe_diet_p, ie_order_control_p, nr_entity_identifier_p);
	
	if	(json_diet_w is null) then
		return null;
	end if;
	
	dbms_lob.createtemporary( ds_json_out_w, true);
	json_diet_w.to_clob(ds_json_out_w);
	
	return ds_json_out_w;
	end get_message_clob;


	procedure getCpoeIntegracaoDiet(nr_seq_diet_cpoe_p number, nr_entity_identifier_p out number) is
	begin

	select max(nr_sequencia)
	into nr_entity_identifier_p
	from cpoe_integracao
	where nr_seq_diet_cpoe = nr_seq_diet_cpoe_p;
	
	if (nr_entity_identifier_p is null) then
		select CPOE_INTEGRACAO_SEQ.nextval
		into nr_entity_identifier_p
		from dual;

		insert into cpoe_integracao (
			nr_sequencia,
			nr_seq_diet_cpoe
			)
		values (
			nr_entity_identifier_p,
			nr_seq_diet_cpoe_p
		);
		/* No commit so it can be used inside trigger */
	end if;	

	end getCpoeIntegracaoDiet;	

end cpoe_diet_json_pck;
/
