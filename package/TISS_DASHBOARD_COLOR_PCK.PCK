create or replace 
package tiss_dashboard_color_pck as

/*
	Date/Timestamp:	July 09th, 2020
	Object: 	TISS_DASHBOARD_COLOR_PCK
	Type:		Package
	Job avaiable:	No.
	Author(s):	Cunha, Nicolas Filipe; Alves, Ronaldo Rodrigues
	Usage:		Returns color values for each chart in the Dashboard tab.
			The dashboard tab and it's charts are populated in the package TISS_DASHBOARD_DATA_PCK.			
	Project:	12069
	Note:		Further and specific documentation is avaiable below in portuguese/english, but in case of any doubts, feel free to contact the author(s) through Microsoft Lync (Skype for Business) or Microsoft Teams.
			If we're not avaiable, contact the RnD Billing and Pharmacy management, specifically the TISS team.
*/

/*
	Color and cell constants used through out the package.
*/

FIRST_COLUMN	varchar2(20) := 'firstCell:';
SECOND_COLUMN	varchar2(20) := 'secondCell:';
THIRD_COLUMN	varchar2(20) := 'thirdCell:';
FOURTH_COLUMN	varchar2(20) := 'fourthCell:';

COLOR_GREEN 	varchar2(20) := '#00FF00';
COLOR_YELLOW 	varchar2(20) := '#FFFF00';
COLOR_RED 	varchar2(20) := '#FF0000';

COLOR_SEPARATOR varchar2(1) := ';';

/*
	Generic method to create subtitle colors for the second and fourth column cells.
	
	Every method that invokes this one passes three parameters:
	vl_item_p => Value to be compared to the target values;
	vl_tgt_med_p => First target value;
	vl_tgt_max_p => Second target value.
	
	If the value assigned to vl_item_p is greater than vl_tgt_max_p, returns the RED color.
	If the value assigned is in between vl_tgt_med_P and vl_tgt_max_p, returns the YELLOW color.
	Otherwise, returns the GREEN color.
	
	As such, this method can only be used in the cases where the lowest value is the best possible outcome.
	
*/
function get_color_min(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

/*
	Generic method to create subtitle colors for the second and fourth column cells.
	
	Every method that invokes this one passes three parameters:
	vl_item_p => Value to be compared to the target values;
	vl_tgt_med_p => First target value;
	vl_tgt_max_p => Second target value.
	
	If the value assigned to vl_item_p is greater than vl_tgt_max_p, returns the GREEN color.
	If the value assigned is in between vl_tgt_med_P and vl_tgt_max_p, returns the YELLOW color.
	Otherwise, returns the RED color.
	
	As such, this method can only be used in the cases where the lowest value is the worst possible outcome.
	
*/
function get_color_max(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

/*
	Generic method to create subtitle colors for the specific column cell.
	
	Every method that invokes this one passes three parameters:
	vl_item_p => Value to be compared to the target values;
	vl_tgt_med_p => First target value;
	vl_tgt_max_p => Second target value;
	ds_cell_p => Column cell.
	
	If the value assigned to vl_item_p is greater than vl_tgt_max_p, returns the RED color.
	If the value assigned is in between vl_tgt_med_P and vl_tgt_max_p, returns the YELLOW color.
	Otherwise, returns the GREEN color.
	
	As such, this method can only be used in the cases where the lowest value is the best possible outcome.
	
*/
function get_color_cell_min(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number, ds_cell_p in varchar2) 	return varchar2;

/*
	Generic method to create subtitle colors for the specific column cell.
	
	Every method that invokes this one passes three parameters:
	vl_item_p => Value to be compared to the target values;
	vl_tgt_med_p => First target value;
	vl_tgt_max_p => Second target value;
	ds_cell_p => Column cell.
	
	If the value assigned to vl_item_p is greater than vl_tgt_max_p, returns the GREEN color.
	If the value assigned is in between vl_tgt_med_P and vl_tgt_max_p, returns the YELLOW color.
	Otherwise, returns the RED color.
	
	As such, this method can only be used in the cases where the lowest value is the worst possible outcome.
	
*/
function get_color_cell_max(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number, ds_cell_p in varchar2) 	return varchar2;


/*
	Create subtitle colors for the Most Denied Item WChart.
	Schematics Tree Code/Sequence: 1150051
	Internal Code/Sequence: 1119556
	Tab: WChartSeries
*/
function get_color_denied_item(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

/*
	Create subtitle colors for the Overdue Items WChart.
	Schematics Tree Code/Sequence: 1148474
	Internal Code/Sequence: 1117731
	Tab: WChartSeries
*/
function get_color_item_overdue(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

/*
	Create subtitle colors for the Items Near Due Date WChart.
	Schematics Tree Code/Sequence: 1150434
	Internal Code/Sequence: 1118113
	Tab: WChartSeries
*/
function get_color_item_near_date(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

/*
	Create subtitle colors for the Items Outside the Expected Appeal Period WChart.
	Schematics Tree Code/Sequence: 1152042
	Internal Code/Sequence: 1119999
	Tab: WChartSeries
*/
function get_color_item_resub_date(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2;

/*
	Create subtitle colors for the Accepted Denial Value WChart.
	Schematics Tree Code/Sequence: 1152279
	Internal Code/Sequence: 1120363
	Tab: WChartSeries
*/
function get_color_acpt_denial(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

/*
	Create subtitle colors for the Recovered Value WChart.
	Schematics Tree Code/Sequence: 1153750
	Internal Code/Sequence: 1122180
	Tab: WChartSeries
*/
function get_color_recov_value(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

/*
	Create subtitle colors for the Productivity WChart.
	Schematics Tree Code/Sequence: 1150889
	Internal Code/Sequence: 1118978
	Tab: WChartSeries
*/
function get_color_productivity(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

/*
	Create subtitle colors for the Internal Reason Code WChart.
	Schematics Tree Code/Sequence: 1148529
	Internal Code/Sequence: 1115589
	Tab: WChartSeries
*/
function get_color_internal_reason(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2;

/*
	Create subtitle colors for the Tiss Reason Code WChart.
	Schematics Tree Code/Sequence: 1149793
	Internal Code/Sequence: 1117283
	Tab: WChartSeries
*/
function get_color_tiss_reason(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) 	return varchar2;

end tiss_dashboard_color_pck;
/

create or replace package body tiss_dashboard_color_pck as

function get_color_min(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

ds_color_w varchar2(200);

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 5);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 10);

begin
	
	if 	vl_item_p > vl_tgt_max_w then
		ds_color_w := SECOND_COLUMN || COLOR_RED || COLOR_SEPARATOR || FOURTH_COLUMN || COLOR_RED;
	elsif	vl_item_p > vl_tgt_med_w then
		ds_color_w := SECOND_COLUMN || COLOR_YELLOW || COLOR_SEPARATOR || FOURTH_COLUMN || COLOR_YELLOW;
	else
		ds_color_w := SECOND_COLUMN || COLOR_GREEN || COLOR_SEPARATOR || FOURTH_COLUMN || COLOR_GREEN;
	end if;

	return ds_color_w;

end;

function get_color_max(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

ds_color_w varchar2(200);

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 5);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 10);

begin
	
	if 	vl_item_p > vl_tgt_max_w then
		ds_color_w := SECOND_COLUMN || COLOR_GREEN || COLOR_SEPARATOR || FOURTH_COLUMN || COLOR_GREEN;
	elsif	vl_item_p > vl_tgt_med_w then
		ds_color_w := SECOND_COLUMN || COLOR_YELLOW || COLOR_SEPARATOR || FOURTH_COLUMN || COLOR_YELLOW;
	else
		ds_color_w := SECOND_COLUMN || COLOR_RED || COLOR_SEPARATOR || FOURTH_COLUMN || COLOR_RED;
	end if;

	return ds_color_w;

end;

function get_color_cell_min(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number, ds_cell_p in varchar2) 	return varchar2 as

ds_color_w varchar2(200);

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 5);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 10);

begin
	
	if 	vl_item_p > vl_tgt_max_w then
		ds_color_w := ds_cell_p || COLOR_RED;
	elsif	vl_item_p > vl_tgt_med_w then
		ds_color_w := ds_cell_p || COLOR_YELLOW;
	else
		ds_color_w := ds_cell_p || COLOR_GREEN;
	end if;

	return ds_color_w;

end;

function get_color_cell_max(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number, ds_cell_p in varchar2) 	return varchar2 as

ds_color_w varchar2(200);

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 5);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 10);

begin
	
	if 	vl_item_p > vl_tgt_max_w then
		ds_color_w := ds_cell_p || COLOR_GREEN;
	elsif	vl_item_p > vl_tgt_med_w then
		ds_color_w := ds_cell_p || COLOR_YELLOW;
	else
		ds_color_w := ds_cell_p || COLOR_RED;
	end if;

	return ds_color_w;

end;

function get_color_denied_item(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 900);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 1000);

begin
	
	return get_color_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w);

end;

function get_color_item_overdue(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 900);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 1000);

begin
	
	return get_color_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w);

end;

function get_color_item_near_date(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 500);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 1000);

begin
	
	return get_color_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w);

end;

function get_color_item_resub_date(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 500);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 1000);

begin
	
	return get_color_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w);

end;

function get_color_acpt_denial(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 500);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 1000);

begin
	
	return get_color_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w);

end;

function get_color_recov_value(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 500);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 1000);

begin
	
	return get_color_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w);

end;

function get_color_productivity(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 400);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 500);

begin
	return get_color_cell_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w, SECOND_COLUMN);

end;

function get_color_internal_reason(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 25);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 30);

begin
	
	return get_color_cell_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w, FOURTH_COLUMN);

end;

function get_color_tiss_reason(vl_item_p in number, vl_tgt_med_p in number, vl_tgt_max_p in number) return varchar2 as

vl_tgt_med_w	number := nvl(vl_tgt_med_p, 25);
vl_tgt_max_w	number := nvl(vl_tgt_max_p, 30);

begin
	return get_color_cell_min(vl_item_p, vl_tgt_med_w, vl_tgt_max_w, FOURTH_COLUMN);
end;

end tiss_dashboard_color_pck;

/
