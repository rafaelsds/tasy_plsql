create or replace 
package tiss_dashboard_data_pck as

/*
	Date/Timestamp:	June 22th, 2020
	Object: 	TISS_DASHBOARD_DATA_PCK
	Type:		Package
	Job avaiable:	Yes - TISS_DASHBOARD_JOB
	Author(s):	Cunha, Nicolas Filipe; Alves, Ronaldo Rodrigues
	Usage:		Generate and maintain data on the database tables that are used to generate charts and business intelligence cards.
			These cards/charts are avaiable at the Denial Appeal Management feature, first tab, Dashboard.
			The charts colors are generated on the package TISS_DASHBOARD_COLOR_PCK.
	Project:	12069
	Note:		Further and specific documentation is avaiable below in portuguese/english, but in case of any doubts, feel free to contact the author(s) through Microsoft Lync (Skype for Business) or Microsoft Teams.
			If we're not avaiable, contact the RnD Billing and Pharmacy management, specifically the TISS team.
*/



/* 
	Le registros da tabela PROTOCOLO_CONVENIO, gerando suas contas, guias e items. 
	Estes dados sao utilizadsos para geracao dos graficos na funcao Gestao de Recurso de Glosas, aba Dashboard.
	O metodo POPULAR_PROTOCOLO e o primeiro da package a ser utilizado para geracao de todas as tabelas.
	Exemplo de uso:
	
	declare
	begin
		tiss_dashboard_data_pck.popular_protocolo(to_date('01/01/2010'), 1, null, 'JOB');
	end;
	
	Nesse caso, os protocolos de janeiro de 2010 para frente serao lidos e tratados, junto com suas contas, guias e itens.
	
*/
procedure popular_protocolo(dt_base_p in date, cd_estab_base_p in estabelecimento.cd_estabelecimento%type, nr_seq_protocolo_p number default null, nm_usuario_p in usuario.nm_usuario%type default 'JOB');
procedure popular_conta(nr_seq_prot_fatur_p in protocolo_faturado.nr_sequencia%type, nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type, dt_primeiro_retorno_p in protocolo_faturado.dt_primeiro_retorno%type);
procedure popular_guia(nr_seq_conc_pac_p in conciliacao_conta_paciente.nr_sequencia%type, nr_interno_conta_p in conta_paciente.nr_interno_conta%type);
procedure popular_glosa(nr_seq_conc_guia_p in conciliacao_conta_pac_guia.nr_sequencia%type, nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type);

/*
	Le os usuarios previstos e popula a tabela utilizada para gerar o grafico de produtividade, gerando os registros de quantos itens cada usuario que estava previsto realmente executou (analisou, glosou, etc) cada item das guias.
	
	nr_interno_conta_p	: Numero da conta da guia sendo iterada;
	cd_autorizacao_p	: Guia sendo iterada;
	nr_seq_conc_guia_p	: Numero de seq. do registros na CONCILIACAO_CONTA_PAC_GUIA a ser vinculado.
*/
procedure popular_usuario_previsto(nr_seq_conc_guia_p in conciliacao_conta_pac_guia.nr_sequencia%type, nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type);

/*
	Gera registros na tabela CONCILIACAO_ITEM_RET_REC referente aos itens (tanto procedimento quanto material) mais glosados pelas operadoras, sua quantidade e valor.
	
	nr_interno_conta_p	: Numero da conta da guia sendo iterada;
	cd_autorizacao_p	: Guia sendo iterada;
	nr_seq_conc_guia_p	: Numero de seq. do registros na CONCILIACAO_CONTA_PAC_GUIA a ser vinculado.
*/
procedure popular_item_conciliado(nr_seq_conc_guia_p in conciliacao_conta_pac_guia.nr_sequencia%type, nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type);

/*
	Os metodos calculam a quantidade de itens nas seguintes situacoes - respectivamente:
		-	Proximos ao vencimento (data limite de reapresentacao);
		-	Itens que ja passaram da data de vencimento;
		-	Itens que foram enviados na data de reapresentacao, mas nao possuiram retorno pela operadora.
		
	As datas sao consideradas em relacao aos cadastros das operadoras na funcao Cadastro de Convenios.
	Estes metodos sao invocados na POPULAR_GUIA.
	
	nr_interno_conta_p	: Numero da conta da guia sendo iterada;
	cd_autorizacao_p	: Guia sendo iterada;
	vl_item_p		: Valor total dos itens proximos ao prazo;
	qt_item_p		: Quantidade total dos itens proximos ao prazo.
	
	Os tres metodos foram mesclados dentro da CALCULA_ITEM_DATAS.
	Deixaremos os fontes com eles ainda para referencia, mas estao depreciados.
	Futuramente serao removidos.
	
*/
procedure calcula_item_prox_venc(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, vl_item_p out number, qt_item_p out number);
procedure calcula_item_vencido(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, vl_item_p out number, qt_item_p out number);
procedure calcula_item_prazo_resp_venc(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, vl_item_p out number, qt_item_p out number);
procedure calcula_item_datas(nr_interno_conta_p in conta_paciente.nr_interno_conta%type,
	cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type,
	vl_item_prox_venc_p out number,
	qt_item_prox_venc_p out number,
	vl_item_vencido_p out number,
	qt_item_vencido_p out number,
	vl_item_sem_resp_p out number,
	qt_item_sem_resp_p out number);

/*
	Calcula a quantidade de itens da guia em questao que nao foram analisados ainda.
	Atualmente consideramos um item nao analisado aquele que nao possui sua acao de glosa definida ainda.
	
	nr_interno_conta_p	: Numero da conta da guia sendo iterada;
	cd_autorizacao_p	: Guia sendo iterada;
	qt_item_p		: Quantidade de item pendente.
*/
procedure calcula_item_pendente(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, qt_item_p out number);

/*
	Metodo para logging. Utiliza a package DBMS_OUTPUT.
	Requer serveroutput on.
*/
procedure printlog(ds_log_p in varchar2);

function obter_qt_item_conv(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return number;

function obter_desc_item_maior_glosa(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return varchar2;
function obter_maior_item_glosa(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return procedimento_paciente.cd_procedimento%type;
function obter_maior_glosa_mat_proc(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return varchar2;

function obter_max_cd_glosa_conv(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return motivo_glosa.cd_motivo_glosa%type;
function obter_max_cd_tiss_conv(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return tiss_motivo_glosa.cd_motivo_tiss%type;

function obter_valor_recebido_guia(nr_interno_conta_p in convenio_retorno_item.nr_interno_conta%type, cd_autorizacao_p in convenio_retorno_item.cd_autorizacao%type) return number;
function obter_pagamento_previsto(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) return date;
function obter_estabelecimento_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return estabelecimento.cd_estabelecimento%type;
function obter_dias_reap_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return convenio_estabelecimento.qt_dias_reapre%type;
function obter_dias_rec_glosa_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return convenio.qt_dias_regurso_glosa%type;
function obter_motivo_ans(cd_motivo_glosa_p in motivo_glosa.cd_motivo_glosa%type) return tiss_motivo_glosa.cd_motivo_tiss%type;	
function obter_protocolo_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return number;
function obter_vencimento_titulo(nr_titulo_p titulo_receber.nr_titulo%type) return date;
function obter_se_guia_paga_pos_data(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, dt_base_p in date) return varchar2;
function obter_saldo_titulo_prot(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return number;
function obter_saldo_titulo_conta(nr_interno_conta_p in conta_paciente_guia.nr_interno_conta%type) return number;
function obter_saldo_titulo_guia(nr_interno_conta_p in conta_paciente_guia.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) return number;
function obter_valor_recuperado_guia(nr_interno_conta_p in convenio_retorno_item.nr_interno_conta%type, cd_autorizacao_p in convenio_retorno_item.cd_autorizacao%type) return number;
function obter_se_saldo_difere_prot(nr_seq_protocolo_p in protocolo_faturado.nr_seq_protocolo%type) return varchar2;
function obter_se_saldo_difere_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return varchar2;
function obter_se_saldo_difere_guia(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p conta_paciente_guia.cd_autorizacao%type) return varchar2;
function obter_nota_fiscal(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return varchar2;
function obter_seq_nota_fiscal_prot(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return number;
function obter_dt_primeiro_retorno(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return date;
function obter_dt_ultimo_retorno(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) return date;
function obter_dt_envio_recurso(nr_interno_conta_p in lote_audit_hist_guia.nr_interno_conta%type, cd_autorizacao_p in lote_audit_hist_guia.cd_autorizacao%type) return date;
function obter_dt_previsao_pag_recurso(nr_interno_conta_p in lote_audit_hist_guia.nr_interno_conta%type, cd_autorizacao_p in lote_audit_hist_guia.cd_autorizacao%type) return date;
function obter_vencimento_protocolo(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return date;
function obter_vencimento_guia(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) return date;
function obter_se_prot_possui_guia_inad(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return varchar2;
function obter_se_guia_inadimplente(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in autorizacao_convenio.cd_autorizacao%type, dt_referencia_p in date) return varchar2;
function obter_valores_guia(nr_interno_conta_p in conta_paciente_guia.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, ie_tipo_p in varchar2) return number;
function obter_valor_inadimplencia(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return number;
function obter_valor_inad_prot(nr_seq_prot_fatur_p in protocolo_faturado.nr_sequencia%type) return number;
function obter_valor_inad_conta(nr_seq_conta_pac_conc_p in conciliacao_conta_paciente.nr_sequencia%type) return number;
function obter_valor_inad_guia(nr_seq_conta_pac_guia_p in conciliacao_conta_pac_guia.nr_sequencia%type) return number;
function obter_possui_guia_grg_retorno(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type, 
	cd_convenio_p in convenio.cd_convenio%Type, 
	cd_estabelecimento_p in estabelecimento.cd_estabelecimento%type) return varchar2;

end tiss_dashboard_data_pck;
/
create or replace package body tiss_dashboard_data_pck as

tmp_ini_pkg_w timestamp;
tmp_fim_pkg_w timestamp;
qt_prot_w number;
qt_conta_w number;
qt_guia_w number;
qt_glosa_w number;
nm_usuario_job_w usuario.nm_usuario%type;

procedure printlog(ds_log_p in varchar2) as

begin
	dbms_output.put_line(ds_log_p);
end;

function obter_nota_fiscal(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return varchar2 as

ds_nota_fiscal_w 	varchar2(255);

cursor nota_fiscal is
select	nr_nota_fiscal
from	nota_fiscal
where	nr_seq_protocolo = nr_seq_protocolo_p;

begin
	ds_nota_fiscal_w := null;
	begin
		for	nota_r in nota_fiscal loop
			if ds_nota_fiscal_w is null then
				ds_nota_fiscal_w := nota_r.nr_nota_fiscal;
			else
				ds_nota_fiscal_w := ds_nota_fiscal_w || ', ' || nota_r.nr_nota_fiscal;
			end if;
		end loop;
	exception
	when others then
		ds_nota_fiscal_w := null;
	end;

	return substr(ds_nota_fiscal_w, 1, 255);

end;


function obter_seq_nota_fiscal_prot(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return number as

nr_nota_fiscal_w nota_fiscal.nr_sequencia%type;

begin

	begin
		select 	nr_sequencia
		into 	nr_nota_fiscal_w
		from 	nota_fiscal
		where 	nr_seq_protocolo = nr_seq_protocolo_p;
	exception
	when others then
		nr_nota_fiscal_w := null;
	end;

	return nr_nota_fiscal_w;

end;

function obter_dt_primeiro_retorno(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return date as

dt_primeiro_retorno_w	date;

begin

	begin
		select	max(a.dt_emissao)
		into	dt_primeiro_retorno_w
		from	tiss_demonstrativo a,
			tiss_cabecalho b
		where	a.nr_seq_cabecalho = b.nr_sequencia
		and	b.nr_seq_retorno in (
					select 	x.nr_sequencia
					from	convenio_retorno x,
						convenio_retorno_item y
					where	y.nr_seq_retorno = x.nr_sequencia
					and	x.ie_status_retorno = 'F'
					and	y.nr_interno_conta in 	(
									select	z.nr_interno_conta
									from	conta_paciente z
									where	z.nr_seq_protocolo = nr_seq_protocolo_p
									)
					and	nvl(y.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) in	(
									select  nvl(z.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738))
									from	conta_paciente_guia z
									where	z.nr_interno_conta in (
													select 	c.nr_interno_conta
													from	conta_paciente c
													where 	c.nr_seq_protocolo = nr_seq_protocolo_p
													)
									)
					);
	exception
	when others then
		dt_primeiro_retorno_w := null;
	end;

	if dt_primeiro_retorno_w is null then
		begin
			select	min(a.dt_retorno)
			into	dt_primeiro_retorno_w
			from	convenio_retorno a
			where	a.nr_sequencia in (
					select 	x.nr_sequencia
					from	convenio_retorno x,
						convenio_retorno_item y
					where	y.nr_seq_retorno = x.nr_sequencia
					and	a.ie_status_retorno = 'F'
					and	y.nr_interno_conta in 	(
									select	z.nr_interno_conta
									from	conta_paciente_guia z
									where	z.nr_interno_conta in (
													select 	c.nr_interno_conta
													from	conta_paciente c
													where 	c.nr_seq_protocolo = nr_seq_protocolo_p
													)
									)
					and	nvl(y.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) in	(
									select  nvl(z.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 
									from	conta_paciente_guia z
									where	z.nr_interno_conta in (
													select 	c.nr_interno_conta
													from	conta_paciente c
													where 	c.nr_seq_protocolo = nr_seq_protocolo_p
													)
									)
					);			
		exception
		when others then
			dt_primeiro_retorno_w := null;
		end;
	end if;

	return dt_primeiro_retorno_w;

end;

function obter_dt_ultimo_retorno(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) return date as

dt_retorno_w date;

dt_ret_conv_w date;
dt_lote_audit_w date;

begin

	begin
		select	coalesce(a.dt_emissao_demonstrativo, a.dt_pagamento)
		into	dt_retorno_w
		from	imp_dem_retorno_prot a,
			convenio_retorno b,
			convenio_retorno_item c
		where	a.nr_seq_protocolo 	= (select max(x.nr_seq_protocolo) from conta_paciente x where x.nr_interno_conta = nr_interno_conta_p)
		and	b.nr_sequencia 		= a.nr_seq_retorno_conv
		and	c.nr_seq_retorno 	= b.nr_sequencia
		and	c.nr_interno_conta 	= nr_interno_conta_p
		and	c.cd_autorizacao 	= cd_autorizacao_p
		and	b.ie_status_retorno 	= 'F';
	exception
	when others then
		dt_ret_conv_w := null;
	end;
	
	if dt_ret_conv_w is null then
	
		begin
			select 	max(c.dt_emissao)
			into	dt_ret_conv_w
			from	convenio_retorno a,
				tiss_cabecalho b,
				tiss_demonstrativo c,
				tiss_dem_fatura d,
				tiss_dem_lote e,
				tiss_dem_conta f
			where	f.nr_seq_lote 		= e.nr_sequencia
			and	e.nr_seq_fatura 	= d.nr_sequencia
			and	d.nr_seq_demonstrativo 	= c.nr_sequencia
			and	c.nr_seq_cabecalho 	= b.nr_sequencia
			and	b.nr_seq_retorno 	= a.nr_sequencia
			and	a.ie_status_retorno 	= 'F'
			and	f.nr_interno_conta 	= nr_interno_conta_p
			and	f.nr_guia_operadora 	= cd_autorizacao_p;
		exception
		when others then
			dt_ret_conv_w := null;
		end;

		if dt_ret_conv_w is null then
			begin
				select	max(a.dt_retorno)
				into	dt_ret_conv_w
				from	convenio_retorno a,
					convenio_retorno_item b
				where	b.nr_seq_retorno 	= a.nr_sequencia
				and 	a.ie_status_retorno 	= 'F'
				and	b.nr_interno_conta 	= nr_interno_conta_p
				and	b.cd_autorizacao 	= cd_autorizacao_p;
			exception
			when others then
				dt_ret_conv_w := null;
			end;			
		end if;		

		begin
			select	max(b.dt_fechamento)
			into	dt_lote_audit_w
			from	lote_auditoria a,
				lote_audit_hist b,
				lote_audit_hist_guia c
			where	c.nr_seq_lote_hist 		= b.nr_sequencia
			and	b.nr_seq_lote_audit 		= a.nr_sequencia
			and	b.nr_analise 			= (select max(x.nr_analise) from lote_audit_hist x where x.nr_seq_lote_audit = a.nr_sequencia)
			and	b.dt_envio 			is not null
			and	b.dt_fechamento 		is not null		
			and	c.nr_interno_conta 		= nr_interno_conta_p
			and	c.cd_autorizacao 		= cd_autorizacao_p;
		exception
		when others then
			dt_lote_audit_w := null;
		end;
		
		if 	dt_ret_conv_w is not null 
			and dt_ret_conv_w > nvl(dt_lote_audit_w, dt_ret_conv_w - 1) then
			dt_retorno_w := dt_ret_conv_w;
		elsif 	dt_lote_audit_w is not null then
			dt_retorno_w := dt_lote_audit_w;
		end if;
	
	end if;

	return dt_retorno_w;

end;

function obter_saldo_titulo_prot(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return number as

vl_saldo_w	number(15,2) := 0;	

cursor 	titulo is
select	vl_saldo_titulo
from	titulo_receber
where	nr_seq_protocolo = nr_seq_protocolo_p;

begin

	begin
		for tit_r in titulo loop
			vl_saldo_w := vl_saldo_w + tit_r.vl_saldo_titulo;
		end loop;
	exception
	when others then
		vl_saldo_w := 0;
	end;

	return nvl(vl_saldo_w, 0);

end;

function obter_saldo_titulo_conta(nr_interno_conta_p in conta_paciente_guia.nr_interno_conta%type) return number as

vl_saldo_w	number(15,2) := 0;	

cursor 	titulo is
select	vl_saldo_titulo
from	titulo_receber
where	nr_interno_conta = nr_interno_conta_p;

begin

	begin
		for tit_r in titulo loop
			vl_saldo_w := vl_saldo_w + tit_r.vl_saldo_titulo;
		end loop;
	exception
	when others then
		vl_saldo_w := 0;
	end;

	return nvl(vl_saldo_w, 0);

end;

function obter_possui_guia_grg_retorno(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type, 
	cd_convenio_p in convenio.cd_convenio%Type, 
	cd_estabelecimento_p in estabelecimento.cd_estabelecimento%type) return varchar2 as

ie_possui_w varchar2(1);

qt_reg_w number;

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type := cd_estabelecimento_p;
cd_convenio_w		convenio.cd_convenio%type := cd_convenio_p;
dt_envio_recurso_w  date;
dt_envio_grg_w  date;

begin

	ie_possui_w := 'N';
	
	select 	nvl(count(a.nr_sequencia), 0)
	into	qt_reg_w
	from	convenio_retorno a,
		convenio_retorno_item b
	where	b.nr_seq_retorno = a.nr_sequencia
	and	a.ie_status_retorno <> 'F'
	and	a.cd_estabelecimento = cd_estabelecimento_w
	and	a.cd_convenio = cd_convenio_w
	and	b.nr_interno_conta in (select x.nr_interno_conta from conta_paciente x where x.nr_seq_protocolo = nr_seq_protocolo_p);
	
	if qt_reg_w > 0 then
		ie_possui_w := 'S';
	else
		select 	nvl(count(a.nr_sequencia), 0)
		into	qt_reg_w
		from	lote_auditoria a,
			lote_audit_hist b,
			lote_audit_hist_guia c
		where	c.nr_seq_lote_hist = b.nr_sequencia
		and	b.nr_seq_lote_audit = a.nr_sequencia
		and	a.cd_estabelecimento = cd_estabelecimento_w
		and	a.cd_convenio = cd_convenio_w
		and	b.dt_envio is null
		and	b.dt_fechamento is null
		and	c.nr_interno_conta in (select x.nr_interno_conta from conta_paciente x where nr_seq_protocolo = nr_seq_protocolo_p);
		
		if qt_reg_w > 0 then
			ie_possui_w := 'S';
		else			
			
			select 	dt_envio_recurso
			into	dt_envio_recurso_w				
			from	protocolo_faturado
			where	nr_seq_protocolo = nr_seq_protocolo_p;

			select  nvl(max(a.dt_envio), max(l.dt_lote))
			into    dt_envio_grg_w
			from    lote_audit_hist a,
				lote_audit_hist_guia b,
				lote_auditoria l,
				conta_paciente cp
			where   a.nr_seq_lote_audit 	= l.nr_sequencia
			and     a.nr_sequencia 		= b.nr_seq_lote_hist
			and     l.cd_convenio 		= cd_convenio_w
			and     l.cd_estabelecimento 	= cd_estabelecimento_w
			and     cp.nr_interno_conta 	= b.nr_interno_conta
			and     cp.nr_seq_protocolo 	= nr_seq_protocolo_p;

			if  (dt_envio_grg_w is not null) and (Nvl(dt_envio_recurso_w,sysdate + 1) <> dt_envio_grg_w) then
				ie_possui_w := 'S';
			end if;
			
		end if;
		
	end if;
	
	return ie_possui_w;

end;

function obter_se_saldo_difere_prot(nr_seq_protocolo_p in protocolo_faturado.nr_seq_protocolo%type) return varchar2 as

ie_difere_w varchar2(1);

vl_saldo_w number(15,2);

qt_w number;
dt_envio_recurso_w  date;
dt_envio_grg_w  date;
cd_convenio_w   convenio.cd_convenio%type;
cd_estabelecimento_w estabelecimento.cd_estabelecimento%type;

begin
	ie_difere_w := 'N';
	begin
		select 	nvl(count(*), 0) qt
		into	qt_w
		from	protocolo_faturado
		where	nr_seq_protocolo = nr_seq_protocolo_p;
	exception
	when others then
		qt_w := 0;
	end;

	if qt_w = 0 then
		ie_difere_w := 'S';
	else 
		select 	vl_saldo_titulo, dt_envio_recurso, cd_convenio, cd_estabelecimento
		into	vl_saldo_w,
                dt_envio_recurso_w,
                cd_convenio_w,
                cd_estabelecimento_w
		from	protocolo_faturado
		where	nr_seq_protocolo = nr_seq_protocolo_p;
        
		if vl_saldo_w <> obter_saldo_titulo_prot(nr_seq_protocolo_p) then
			ie_difere_w := 'S';
		end if;
	end if;

	return nvl(ie_difere_w, 'N');
end;



function obter_se_saldo_difere_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return varchar2 as

ie_difere_w varchar2(1);

vl_saldo_w number(15,2);

qt_w number;

begin

	begin
		select 	nvl(count(*), 0) qt
		into	qt_w
		from	conciliacao_conta_paciente
		where	nr_interno_conta = nr_interno_conta_p;
	exception
	when others then
		qt_w := 0;
	end;

	if qt_w = 0 then
		ie_difere_w := 'S';	
	else 
		select 	vl_saldo_titulo
		into	vl_saldo_w
		from	conciliacao_conta_paciente
		where	nr_interno_conta = nr_interno_conta_p;

		if vl_saldo_w <> obter_saldo_titulo_conta(nr_interno_conta_p) then
			ie_difere_w := 'S';
		else
			ie_difere_w := 'N';
		end if;

	end if;

	return nvl(ie_difere_w, 'S');
end;

function obter_se_saldo_difere_guia(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p conta_paciente_guia.cd_autorizacao%type) return varchar2 as

ie_difere_w varchar2(1);

vl_saldo_w number(15,2);

qt_w number;

begin

	begin
		select 	nvl(count(*), 0) qt
		into	qt_w
		from	conciliacao_conta_pac_guia
		where	nr_interno_conta = nr_interno_conta_p
		and	cd_autorizacao = cd_autorizacao_p;
	exception
	when others then
		qt_w := 0;
	end;

	if qt_w = 0 then
		ie_difere_w := 'S';	
	else 
		select 	vl_saldo_titulo
		into	vl_saldo_w
		from	conciliacao_conta_pac_guia
		where	nr_interno_conta = nr_interno_conta_p
		and	cd_autorizacao = cd_autorizacao_p;

		if vl_saldo_w <> obter_saldo_titulo_guia(nr_interno_conta_p, cd_autorizacao_p) then
			ie_difere_w := 'S';
		else
			ie_difere_w := 'N';
		end if;

	end if;

	return nvl(ie_difere_w, 'S');
end;

procedure popular_protocolo(dt_base_p in date, cd_estab_base_p in estabelecimento.cd_estabelecimento%type, nr_seq_protocolo_p number default null, nm_usuario_p in usuario.nm_usuario%type default 'JOB') as

cursor c_protocolo is
select  a.cd_convenio 										cd_convenio,
	a.cd_estabelecimento 									cd_estabelecimento,
	a.dt_mesano_referencia 									dt_mesano_referencia,
	a.nr_seq_protocolo 									nr_seq_protocolo,
	obter_total_protocolo(a.nr_seq_protocolo)						vl_protocolo,
	a.nm_usuario										nm_usuario,
	obter_vencimento_protocolo(a.nr_seq_protocolo)						dt_vencimento,	
	obter_nota_fiscal(a.nr_seq_protocolo)							nota_fiscal,
	obter_seq_nota_fiscal_prot(a.nr_seq_protocolo)						seq_nota_fiscal,
	obter_dt_primeiro_retorno(a.nr_seq_protocolo)   					dt_primeiro_retorno,
	obter_saldo_titulo_prot(a.nr_seq_protocolo)						saldo_titulo,
	substr(obter_titulo_conta_protocolo(a.nr_seq_protocolo, 0),1, 255) 			titulo_protocolo,
	obter_se_saldo_difere_prot(a.nr_seq_protocolo)						saldo_difere,
	obter_possui_guia_grg_retorno(a.nr_seq_protocolo, a.cd_convenio, a.cd_estabelecimento) 	possui_guia
from 	protocolo_convenio a
where	a.dt_mesano_referencia >= dt_base_p
and     (a.cd_estabelecimento = cd_estab_base_p or cd_estab_base_p is null)
and     (nr_seq_protocolo_p is null or nr_seq_protocolo_p = a.nr_seq_protocolo);

vl_adic_w		protocolo_faturado.vl_adicional%type;
vl_desc_w		protocolo_faturado.vl_desconto_retorno%type;
vl_glosa_aceita_w	protocolo_faturado.vl_glosa_aceita%type;
vl_recebido_w		protocolo_faturado.vl_recebido%type;
vl_recursado_w		protocolo_faturado.vl_recursado%type;
vl_recurso_recuperado_w protocolo_faturado.vl_recurso_recuperado%type;
vl_pend_reapresentacao_w protocolo_faturado.vl_pend_reapresentacao%type;

dt_envio_recurso_w 		date;
dt_previsao_pag_recurso_w 	date;

nr_seq_prot_fatur_w	protocolo_faturado.nr_sequencia%type;

vl_soma_saldo_tit_w	number(15,2);

dt_primeiro_retorno_w	protocolo_faturado.dt_primeiro_retorno%type;

qt_item_prox_venc_w 	number(15);
vl_item_prox_venc_w 	number(15, 2);

begin	
	nm_usuario_job_w := nm_usuario_p;
	tmp_ini_pkg_w := systimestamp;	
	qt_prot_w := 0;
	qt_conta_w := 0;
	qt_guia_w := 0;
	qt_glosa_w := 0;

	for protocolo in c_protocolo loop

		if protocolo.saldo_difere = 'S' or protocolo.possui_guia = 'S' or obter_se_prot_possui_guia_inad(protocolo.nr_seq_protocolo) = 'S' then
			qt_prot_w := qt_prot_w + 1;
			printlog('#@ INICIO LOG PROTOCOLO @#');
			printlog('protocolo = ' ||  protocolo.nr_seq_protocolo);
			printlog('protocolo.titulo_protocolo = ' || protocolo.titulo_protocolo);
			printlog('protocolo saldo difere = ' || protocolo.saldo_difere);
			printlog('cd_empresa = ' || obter_empresa_estab(protocolo.cd_estabelecimento));
			printlog('cd_estabelecimento = ' || protocolo.cd_estabelecimento);

			delete from protocolo_faturado
			where 	nr_seq_protocolo = protocolo.nr_seq_protocolo;
			
			if protocolo.titulo_protocolo is not null then

				select	protocolo_faturado_seq.nextval
				into	nr_seq_prot_fatur_w
				from	dual;

				insert into protocolo_faturado
				(
					cd_empresa,
					cd_estabelecimento,
					dt_referencia,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					nr_sequencia
				) 
				values 
				( 
					obter_empresa_estab(protocolo.cd_estabelecimento),
					protocolo.cd_estabelecimento,
					protocolo.dt_mesano_referencia,
					sysdate,
					sysdate,
					nm_usuario_job_w,
					nm_usuario_job_w,
					nr_seq_prot_fatur_w
				);

				popular_conta(nr_seq_prot_fatur_w, protocolo.nr_seq_protocolo, protocolo.dt_primeiro_retorno);

				select	sum(nvl(a.vl_adicional, 0)),
					sum(nvl(a.vl_desconto_retorno, 0)),
					sum(nvl(a.vl_glosa_aceita, 0)),
					sum(nvl(a.vl_recebido, 0)),
					sum(nvl(a.vl_recursado, 0)),
					sum(nvl(a.vl_recurso_recuperado, 0)),
					sum(nvl(a.vl_pend_reapresentacao,0))
				into	vl_adic_w,
					vl_desc_w,
					vl_glosa_aceita_w,
					vl_recebido_w,
					vl_recursado_w,
					vl_recurso_recuperado_w,
					vl_pend_reapresentacao_w
				from	conciliacao_conta_paciente a
				where	a.nr_seq_protoc_faturado = nr_seq_prot_fatur_w;

				select	min(a.dt_envio_recurso),
					min(a.dt_previsao_pag_recurso)
				into	dt_envio_recurso_w,
					dt_previsao_pag_recurso_w
				from	conciliacao_conta_paciente a
				where	a.nr_seq_protoc_faturado = nr_seq_prot_fatur_w;

				select 	sum(a.vl_item_rec_prox_venc),
					sum(a.qt_item_rec_prox_venc)
				into	vl_item_prox_venc_w,
					qt_item_prox_venc_w
				from	conciliacao_conta_paciente a
				where	a.nr_seq_protoc_faturado = nr_seq_prot_fatur_w;

				printlog('vl_adic_w = ' || vl_adic_w);
				printlog('vl_desc_w = ' || vl_desc_w);
				printlog('vl_glosa_aceita_w = ' || vl_glosa_aceita_w);
				printlog('vl_recebido_w = ' || vl_recebido_w);
				printlog('vl_recursado_w = ' || vl_recursado_w);
				printlog('vl_recurso_recuperado_w = ' || vl_recurso_recuperado_w);
				printlog('cd_convenio = ' || protocolo.cd_convenio);
				printlog('dt_mesano_referencia = ' || protocolo.dt_mesano_referencia);
				printlog('vl_protocolo = ' || protocolo.vl_protocolo);
				printlog('dt_vencimento = ' || protocolo.dt_vencimento);
				printlog('dt_primeiro_retorno_w = ' || protocolo.dt_primeiro_retorno);			
				printlog('titulo_protocolo = ' || protocolo.titulo_protocolo);
				printlog('nr_nota_fiscal = ' || protocolo.nota_fiscal);
				printlog('seq_nota_fiscal = ' || protocolo.seq_nota_fiscal);			
				printlog('saldo_titulo = ' || protocolo.saldo_titulo);
				printlog('dt_envio_recurso = ' || dt_envio_recurso_w);
				printlog('dt_previsao_pag_recurso = ' || dt_previsao_pag_recurso_w);

				printlog('#@ FIM LOG PROTOCOLO @#');

				update 	protocolo_faturado
				set	vl_adicional 			= vl_adic_w,
					vl_desconto_retorno 		= vl_desc_w,
					vl_glosa_aceita 		= vl_glosa_aceita_w,
					vl_recebido 			= vl_recebido_w,
					vl_recursado 			= vl_recursado_w,
					vl_recurso_recuperado 		= vl_recurso_recuperado_w,
					cd_convenio 			= protocolo.cd_convenio,
					dt_referencia 			= protocolo.dt_mesano_referencia,
					vl_protocolo 			= protocolo.vl_protocolo,
					nr_seq_protocolo 		= protocolo.nr_seq_protocolo,
					dt_previsao_primeiro_pag 	= protocolo.dt_vencimento,
					dt_primeiro_retorno 		= dt_primeiro_retorno_w,
					nr_titulo_receber 		= protocolo.titulo_protocolo,
					nr_nota_fiscal 			= protocolo.nota_fiscal,
					nr_seq_nota_fiscal 		= protocolo.seq_nota_fiscal,
					dt_envio_recurso 		= dt_envio_recurso_w,
					dt_previsao_pag_recurso 	= dt_previsao_pag_recurso_w,
					dt_atualizacao_nrec 		= sysdate,
					nm_usuario_nrec 		= protocolo.nm_usuario,
					vl_saldo_titulo			= protocolo.saldo_titulo,
					vl_item_rec_prox_venc		= nvl(vl_item_prox_venc_w, 0),
					qt_item_rec_prox_venc		= nvl(qt_item_prox_venc_w, 0),
					vl_pend_reapresentacao		= nvl(vl_pend_reapresentacao_w, 0)				
				where	nr_sequencia 			= nr_seq_prot_fatur_w;
				
			end if;

		end if;

	end loop;
	printlog('Foram processados ' || qt_prot_w || ' protocolos.');
	printlog('Foram processadas ' || qt_conta_w || ' contas.');
	printlog('Foram processadas ' || qt_guia_w || ' guias.');
	printlog('Foram processadas ' || qt_glosa_w || ' glosas.');
	tmp_fim_pkg_w := systimestamp;
	printlog('O processo levou ' || extract(second from tmp_fim_pkg_w - tmp_ini_pkg_w) || ' segundos. ');	
	-- rollback;
	commit;
end popular_protocolo;

procedure popular_conta(nr_seq_prot_fatur_p in protocolo_faturado.nr_sequencia%type, nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type, dt_primeiro_retorno_p in protocolo_faturado.dt_primeiro_retorno%type) as

cursor c_conta is
select 	a.nr_interno_conta				nr_interno_conta,
	a.nr_seq_protocolo				nr_seq_protocolo,
	obter_nf_conta(a.nr_interno_conta, 1)		seq_nota_fiscal,
	obter_nf_conta(a.nr_interno_conta, 2)		nota_fiscal,
	substr(obter_titulo_conta(a.nr_interno_conta),1,255)		titulo,
	a.vl_conta					vl_conta,
	a.dt_mesano_referencia				dt_referencia,
	a.nm_usuario					nm_usuario,
	(select	max(x.dt_pagamento_previsto) from	titulo_receber x where	x.nr_interno_conta	= a.nr_interno_conta)	dt_vencimento_prot,
	obter_saldo_titulo_conta(a.nr_interno_conta) 	saldo_titulo	
from 	conta_paciente a	
where 	a.nr_seq_protocolo = nr_seq_protocolo_p;

vl_adic_w		conciliacao_conta_paciente.vl_adicional%type;
vl_desc_w		conciliacao_conta_paciente.vl_desconto_retorno%type;
vl_glosa_aceita_w	conciliacao_conta_paciente.vl_glosa_aceita%type;
vl_recebido_w		conciliacao_conta_paciente.vl_recebido%type;
vl_recursado_w		conciliacao_conta_paciente.vl_recursado%type;
vl_recurso_recuperado_w conciliacao_conta_paciente.vl_recurso_recuperado%type;
vl_pend_reapresentacao_w conciliacao_conta_paciente.vl_pend_reapresentacao%type;

dt_envio_recurso_w 		date;
dt_previsao_pag_recurso_w 	date;

nr_seq_conta_w	conciliacao_conta_paciente.nr_sequencia%type;

qt_item_prox_venc_w number(15);
vl_item_prox_venc_w number(15, 2);

begin

	for conta in c_conta loop
		qt_conta_w := qt_conta_w + 1;
		printlog('#@ INICIO LOG CONTA @#');
		printlog('conta = ' || conta.nr_interno_conta);

		select	conciliacao_conta_paciente_seq.nextval
		into	nr_seq_conta_w
		from	dual;

		insert into conciliacao_conta_paciente
		(
			dt_atualizacao,
			dt_atualizacao_nrec,
			dt_referencia,
			nm_usuario,
			nm_usuario_nrec,
			nr_sequencia,
			nr_seq_protoc_faturado
		) 
		values 
		(
			sysdate,
			sysdate,
			conta.dt_referencia,
			nm_usuario_job_w,
			nm_usuario_job_w,
			nr_seq_conta_w,
			nr_seq_prot_fatur_p
		);

		popular_guia(nr_seq_conta_w, conta.nr_interno_conta);

		select	sum(nvl(a.vl_adicional, 0)),
			sum(nvl(a.vl_desconto_retorno, 0)),
			sum(nvl(a.vl_glosa_aceita, 0)),
			sum(nvl(a.vl_recebido, 0)),
			sum(nvl(a.vl_recursado, 0)),
			sum(nvl(a.vl_recurso_recuperado, 0)),
			sum(nvl(a.vl_pend_reapresentacao,0))
		into	vl_adic_w,
			vl_desc_w,
			vl_glosa_aceita_w,
			vl_recebido_w,
			vl_recursado_w,
			vl_recurso_recuperado_w,
			vl_pend_reapresentacao_w
		from	conciliacao_conta_pac_guia a
		where	a.nr_seq_conta_pac_conc 	= nr_seq_conta_w;

		select	min(a.dt_envio_recurso),
			min(a.dt_previsao_pag_recurso)
		into	dt_envio_recurso_w,
			dt_previsao_pag_recurso_w
		from	conciliacao_conta_pac_guia a
		where	a.nr_seq_conta_pac_conc 	= nr_seq_conta_w;

		select 	sum(a.vl_item_rec_prox_venc),
			sum(a.qt_item_rec_prox_venc)
		into	vl_item_prox_venc_w,
			qt_item_prox_venc_w
		from	conciliacao_conta_pac_guia a
		where	a.nr_seq_conta_pac_conc 	= nr_seq_conta_w;

		printlog('conta.nm_usuario = ' || conta.nm_usuario);
		printlog('conta.nr_interno_conta = ' || conta.nr_interno_conta);
		printlog('conta.nr_seq_protocolo = ' || conta.nr_seq_protocolo);
		printlog('conta.nota_fiscal = ' || conta.nota_fiscal);
		printlog('conta.seq_nota_fiscal = ' || conta.seq_nota_fiscal);
		printlog('conta.titulo = ' || conta.titulo);
		printlog('conta.vl_conta = ' || conta.vl_conta);
		printlog('conta.dt_referencia = ' || conta.dt_referencia);
		printlog('conta.dt_vencimento_prot = ' || conta.dt_vencimento_prot);
		printlog('dt_primeiro_retorno_p = ' || dt_primeiro_retorno_p);
		printlog('dt_envio_recurso_w = ' || dt_envio_recurso_w );
		printlog('dt_previsao_pag_recurso_w = ' || dt_previsao_pag_recurso_w);
		printlog('vl_adic_w = ' || vl_adic_w);
		printlog('vl_desc_w = ' || vl_desc_w);
		printlog('vl_glosa_aceita_w = ' || vl_glosa_aceita_w);
		printlog('vl_recebido_w = ' || vl_recebido_w);
		printlog('vl_recursado_w = ' || vl_recursado_w);
		printlog('vl_recurso_recuperado_w = ' || vl_recurso_recuperado_w);
		printlog('saldo_titulo = ' || conta.saldo_titulo);
		printlog('#@ FIM LOG CONTA @#');

		update	conciliacao_conta_paciente
		set	dt_atualizacao_nrec 		= sysdate,
			nm_usuario_nrec 		= conta.nm_usuario,
			nr_interno_conta 		= conta.nr_interno_conta,
			nr_seq_protocolo 		= conta.nr_seq_protocolo,
			nr_nota_fiscal 			= conta.nota_fiscal,
			nr_seq_nota_fiscal 		= conta.seq_nota_fiscal,
			nr_titulo_receber 		= conta.titulo,
			vl_saldo_titulo			= conta.saldo_titulo,
			vl_conta 			= conta.vl_conta,
			dt_referencia 			= conta.dt_referencia,			
			dt_previsao_primeiro_pag 	= conta.dt_vencimento_prot,
			dt_primeiro_retorno 		= dt_primeiro_retorno_p,
			dt_envio_recurso 		= dt_envio_recurso_w,
			dt_previsao_pag_recurso 	= dt_previsao_pag_recurso_w,
			vl_adicional 			= vl_adic_w,
			vl_desconto_retorno 		= vl_desc_w,
			vl_glosa_aceita 		= vl_glosa_aceita_w,
			vl_recebido 			= vl_recebido_w,
			vl_recursado 			= vl_recursado_w,
			vl_recurso_recuperado 		= vl_recurso_recuperado_w,
			vl_item_rec_prox_venc		= nvl(vl_item_prox_venc_w, 0),
			qt_item_rec_prox_venc		= nvl(qt_item_prox_venc_w, 0),
			vl_pend_reapresentacao		= nvl(vl_pend_reapresentacao_w, 0)			
		where	nr_sequencia 			= nr_seq_conta_w;

	end loop;

end popular_conta;

function obter_se_prot_possui_guia_inad(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return varchar2 as

ie_possui_w varchar2(10) := 'N';

cursor guias is
select	a.cd_autorizacao	cd_autorizacao,
	a.nr_interno_conta	nr_interno_conta
from	conta_paciente_guia a,
	conta_paciente b
where	a.nr_interno_conta = b.nr_interno_conta
and	b.nr_seq_protocolo = nr_seq_protocolo_p;

begin

	dbms_output.put_line('nr_seq_protocolo_p = ' || nr_seq_protocolo_p);
	
	for guia in guias loop
		dbms_output.put_line('guia.cd_autorizacao = ' || guia.cd_autorizacao);
		dbms_output.put_line('guia.nr_interno_conta = ' || guia.nr_interno_conta);

		ie_possui_w := obter_se_guia_inadimplente(guia.nr_interno_conta, guia.cd_autorizacao, obter_pagamento_previsto(guia.nr_interno_conta, nvl(guia.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738))));
		
		if ie_possui_w = 'S' then
			exit;
		end if;
	
	end loop;
	
	return ie_possui_w;

end obter_se_prot_possui_guia_inad;

function obter_se_guia_inadimplente(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in autorizacao_convenio.cd_autorizacao%type, dt_referencia_p in date) return varchar2 as

ie_inadimplente_w varchar2(1) := 'N';
qt_w number;


begin
	if  (sysdate > Nvl(dt_referencia_p,sysdate)) then

		select 	count(a.nr_sequencia) qt
		into	qt_w
		from	convenio_retorno_item a,
			convenio_retorno b
		where	a.nr_seq_retorno = b.nr_sequencia
		and	a.nr_interno_conta 	= nr_interno_conta_p
		and	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
		and	b.dt_retorno 		> dt_referencia_p;

		if  qt_w = 0 then
			select  count(a.nr_sequencia) qt
			into	qt_w
			from	lote_audit_hist_guia a,
			    lote_audit_hist b,
			    lote_auditoria c
			where	a.nr_seq_lote_hist 	= b.nr_sequencia
			and	    b.nr_seq_lote_audit 	= c.nr_sequencia
			and	    c.dt_lote		> dt_referencia_p
			and	    a.nr_interno_conta 	= nr_interno_conta_p
			and	    nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p;
		    
		end if;

		if qt_w = 0 then 

			select 	count(nr_sequencia) qt
			into	qt_w
			from	tiss_dem_conta
			where	nr_interno_conta 	= nr_interno_conta_p
			and	nr_guia_operadora 	= cd_autorizacao_p
			and	dt_atualizacao_nrec 	< dt_referencia_p;

		end if;
			

		if  qt_w = 0 then
		    
			ie_inadimplente_w := 'S';
		    
		end if;

	end if;

	return nvl(ie_inadimplente_w, 'N');

end;

function obter_dt_envio_recurso(nr_interno_conta_p in lote_audit_hist_guia.nr_interno_conta%type, cd_autorizacao_p in lote_audit_hist_guia.cd_autorizacao%type) return date as

dt_recurso_w	lote_audit_hist.dt_envio%type;

begin

	begin
		select	min(b.dt_envio)
		into	dt_recurso_w
		from	lote_audit_hist_guia a,
			lote_audit_hist b
		where 	a.nr_seq_lote_hist 	= b.nr_sequencia
		and	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
		and	a.nr_interno_conta 	= nr_interno_conta_p;
	exception
	when others then
		dt_recurso_w := null;
	end;

	return dt_recurso_w;

end;

function obter_dt_previsao_pag_recurso(nr_interno_conta_p in lote_audit_hist_guia.nr_interno_conta%type, cd_autorizacao_p in lote_audit_hist_guia.cd_autorizacao%type) return date as

dt_ret_previsto_w	lote_audit_hist.dt_ret_previsto%type;

begin

	begin
		select	min(b.dt_ret_previsto)
		into	dt_ret_previsto_w
		from	lote_audit_hist_guia a,
			lote_audit_hist b
		where 	a.nr_seq_lote_hist 	= b.nr_sequencia
		and	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
		and	a.nr_interno_conta 	= nr_interno_conta_p;
	exception
	when others then
		dt_ret_previsto_w := null;
	end;

	return dt_ret_previsto_w;

end;

function obter_valor_recebido_guia(nr_interno_conta_p in convenio_retorno_item.nr_interno_conta%type, cd_autorizacao_p in convenio_retorno_item.cd_autorizacao%type) return number as

vl_recebido_w number(15,2);

begin

	begin
		select	sum(nvl(a.vl_pago, 0))
		into	vl_recebido_w
		from	convenio_retorno_item a
		where	a.nr_interno_conta 	= nr_interno_conta_p
		and	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
		and	a.nr_seq_retorno 	in (	
							select 	b.nr_sequencia
							from	convenio_retorno b,
								convenio_retorno_item c
							where	c.nr_seq_retorno 	= b.nr_sequencia
							and	c.nr_interno_conta 	= nr_interno_conta_p
							and	b.ie_status_retorno = 'F'
							and	nvl(c.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
						);
	exception
	when others then
		vl_recebido_w := 0;
	end;

	return nvl(vl_recebido_w, 0);

end;

function obter_valor_recuperado_guia(nr_interno_conta_p in convenio_retorno_item.nr_interno_conta%type, cd_autorizacao_p in convenio_retorno_item.cd_autorizacao%type) return number as

vl_recuperado_w number(15,2);

qt_retorno_w number;
qt_grg_w number;

vl_pago_primeiro_retorno_w number(15,2);
vl_pago_retorno_total_w number(15,2);
vl_pago_grg_total_w number(15,2);

begin

	begin
		select 	count(a.nr_sequencia) qt		
		into	qt_retorno_w
		from	convenio_retorno a,
			convenio_retorno_item b
		where	b.nr_seq_retorno 	= a.nr_sequencia
		and	b.nr_interno_conta 	= nr_interno_conta_p
		and	nvl(b.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p;

		select 	count(a.nr_sequencia) qt
		into	qt_grg_w
		from	lote_auditoria a,
			lote_audit_hist b,
			lote_audit_hist_guia c
		where	c.nr_seq_lote_hist 	= b.nr_sequencia
		and	b.nr_seq_lote_audit 	= a.nr_sequencia
		and	c.nr_interno_conta 	= nr_interno_conta_p
		and	nvl(c.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p;

		if qt_retorno_w <= 1 and qt_grg_w = 0 then
			vl_recuperado_w := 0;
		else		

			select	a.vl_pago
			into	vl_pago_primeiro_retorno_w
			from	convenio_retorno_item a
			where	a.nr_interno_conta 	= nr_interno_conta_p
			and	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
			and	a.nr_seq_retorno 	= (	
								select 	min(b.nr_sequencia)
								from	convenio_retorno b,
									convenio_retorno_item c
								where	c.nr_seq_retorno 	= b.nr_sequencia
								and	c.nr_interno_conta 	= nr_interno_conta_p
								and	nvl(c.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
							);

			select	a.vl_pago
			into	vl_pago_retorno_total_w
			from	convenio_retorno_item a
			where	a.nr_interno_conta 	= nr_interno_conta_p
			and	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p;

			select	obter_valores_guia_grc(a.nr_seq_lote_hist, a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)), 'VP')
			into	vl_pago_grg_total_w
			from	lote_audit_hist_guia a
			where	a.nr_interno_conta 	= nr_interno_conta_p
			and	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p;

			vl_recuperado_w := vl_pago_retorno_total_w + vl_pago_grg_total_w - vl_pago_primeiro_retorno_w;

		end if;

	exception
	when others then
		vl_recuperado_w := 0;
	end;

	return nvl(vl_recuperado_w, 0);

end;

function obter_valores_guia(nr_interno_conta_p in conta_paciente_guia.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, ie_tipo_p in varchar2) return number as

vl_retorno_func_w number(15,2);

vl_adic_w 	number(15,2);
vl_desc_w 	number(15,2);
vl_glosa_w 	number(15,2);
vl_recursado_w	number(15,2);

begin
	select	sum(nvl(a.vl_adicional,0)),
		sum(nvl(a.vl_desconto,0)),
		sum(nvl(a.vl_glosado,0)),
		sum(nvl(a.vl_amenor,0))
	into	vl_adic_w,
		vl_desc_w,
		vl_glosa_w,
		vl_recursado_w
	from	convenio_retorno_item a,
		convenio_retorno b
	where	a.nr_seq_retorno = b.nr_sequencia
	and	b.ie_status_retorno = 'F'
	and	a.nr_interno_conta = nr_interno_conta_p
	and	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) = cd_autorizacao_p;


	select	vl_glosa_w + nvl(sum(a.vl_glosa),0) vl_glosa
	into	vl_glosa_w
	from	lote_audit_hist c,
		lote_audit_hist_item a,
		lote_audit_hist_guia b
	where	b.nr_sequencia			= a.nr_seq_guia
	and	nvl(b.cd_autorizacao,'0')	= nvl(nvl(cd_autorizacao_p,b.cd_autorizacao),'0')
	and	b.nr_interno_conta		= nr_interno_conta_p
	and	b.nr_seq_lote_hist		= c.nr_sequencia
	and	(b.dt_baixa_glosa is not null or c.nr_sequencia <> to_number(obter_ultima_analise(c.nr_seq_lote_audit)));	

	if ie_tipo_p = 'ADIC' then
		vl_retorno_func_w := vl_adic_w;
	elsif ie_tipo_p = 'DESC' then
		vl_retorno_func_w := vl_desc_w;
	elsif ie_tipo_p = 'GLOSA' then
		vl_retorno_func_w := vl_glosa_w;
	elsif ie_tipo_p = 'RECEB' then
		vl_retorno_func_w := obter_valor_recebido_guia(nr_interno_conta_p, cd_autorizacao_p);
	elsif ie_tipo_p = 'REC' then -- recursado
		vl_retorno_func_w := vl_recursado_w;
	else
		vl_retorno_func_w := obter_valor_recuperado_guia(nr_interno_conta_p, cd_autorizacao_p);
	end if;

	return vl_retorno_func_w;

end;

function obter_saldo_titulo_guia(nr_interno_conta_p in conta_paciente_guia.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) return number as

vl_saldo_titulo_w	titulo_receber.vl_saldo_titulo%type;

begin

	begin
		select	vl_saldo_titulo
		into	vl_saldo_titulo_w
		from	titulo_receber
		where	nr_titulo = OBTER_TITULO_CONTA_GUIA(nr_interno_conta_p, cd_autorizacao_p, null, null);
	exception
	when others then
		vl_saldo_titulo_w := 0;
	end;


	return nvl(vl_saldo_titulo_w, 0);

end;

function obter_vencimento_protocolo(nr_seq_protocolo_p in protocolo_convenio.nr_seq_protocolo%type) return date as

dt_vencimento_w		date;

begin

	begin
		select	a.dt_vencimento
		into	dt_vencimento_w
		from	protocolo_convenio a
		where	a.nr_seq_protocolo = nr_seq_protocolo_p;
	exception
	when others then
		dt_vencimento_w := null;
	end;

	if dt_vencimento_w is null then

		begin
			select	a.dt_vencimento
			into	dt_vencimento_w
			from	titulo_receber a
			where	a.nr_titulo = (
					select max(z.nr_titulo)
					from titulo_receber z
					where z.nr_seq_protocolo = nr_seq_protocolo_p
			);
		exception
		when others then
			dt_vencimento_w	:= null;
		end;

		if dt_vencimento_w is null then
			begin
				select	a.dt_vencimento
				into	dt_vencimento_w
				from	titulo_receber a
				where	a.nr_titulo = (								
					select 	max(z.nr_titulo)
					from	titulo_receber z
					where	z.nr_interno_conta in (
						select y.nr_interno_conta
						from conta_paciente y
						where y.nr_seq_protocolo = nr_seq_protocolo_p
					)
				);
			exception
			when others then
				dt_vencimento_w := null;
			end;

			if dt_vencimento_w is null then
				begin
					select	a.dt_vencimento
					into	dt_vencimento_w
					from	titulo_receber a
					where	a.nr_titulo in (
						select	OBTER_TITULO_CONTA_GUIA(z.nr_interno_conta, z.cd_autorizacao, null, null)
						from	conta_paciente_guia z
						where	z.nr_interno_conta in (
							select y.nr_interno_conta
							from	conta_paciente y
							where	y.nr_seq_protocolo = nr_seq_protocolo_p
						)					
					);
				exception
				when others then
					dt_vencimento_w := null;
				end;
			end if;

		end if;

	end if;

	return dt_vencimento_w;

end;

function obter_protocolo_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return number as

nr_seq_protocolo_w	protocolo_convenio.nr_seq_protocolo%type;

begin

	begin
		select	a.nr_seq_protocolo
		into	nr_seq_protocolo_w
		from	conta_paciente a
		where	a.nr_interno_conta = nr_interno_conta_p;		
	exception
	when others then
		nr_seq_protocolo_w := null;
	end;

	return nr_seq_protocolo_w;

end;

function obter_vencimento_guia(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) return date as

dt_vencimento_w date;
nr_seq_protocolo_w protocolo_convenio.nr_seq_protocolo%type;

begin

	begin
		select	a.dt_vencimento
		into	dt_vencimento_w
		from	titulo_receber a
		where	a.nr_titulo = OBTER_TITULO_CONTA_GUIA(nr_interno_conta_p, cd_autorizacao_p, null, null);
	exception
	when others then
		dt_vencimento_w := null;
	end;

	if dt_vencimento_w is null then

		begin
			select	a.dt_vencimento
			into	dt_vencimento_w
			from	titulo_receber a
			where	a.nr_titulo = (								
				select 	max(z.nr_titulo)
				from	titulo_receber z
				where	z.nr_interno_conta = nr_interno_conta_p
			);
		exception
		when others then
			dt_vencimento_w := null;
		end;

		if dt_vencimento_w is null then

			nr_seq_protocolo_w := obter_protocolo_conta(nr_interno_conta_p);

			begin
				select	a.dt_vencimento
				into	dt_vencimento_w
				from	titulo_receber a
				where	a.nr_titulo = (
							select max(z.nr_titulo)
							from titulo_receber z
							where z.nr_seq_protocolo = nr_seq_protocolo_w
				);
			exception
			when others then
				dt_vencimento_w	:= null;
			end;

		end if;

	end if;

	return dt_vencimento_w;

end;

function obter_se_guia_paga_pos_data(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, dt_base_p in date) return varchar2 as

ie_paga_w	varchar2(1);

begin
	begin
		select	nvl(max('S'), 'N')
		into	ie_paga_w
		from	convenio_retorno a,
			convenio_retorno_item b
		where	b.nr_seq_retorno = a.nr_sequencia
		and	b.nr_interno_conta = nr_interno_conta_p
		and	nvl(b.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) = cd_autorizacao_p
		and	a.dt_retorno > dt_base_p;
	exception
	when others then
		ie_paga_w := 'N';
	end;

	return nvl(ie_paga_w, 'N');
end;

procedure calcula_item_vencido(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, vl_item_p out number, qt_item_p out number) as

qt_item_vencido_w number(15);
vl_item_vencido_w number(15,2);

begin
	select	count(a.nr_sequencia) qt,
		nvl(sum(a.vl_amenor),0) vl_glosa
	into	qt_item_vencido_w,
		vl_item_vencido_w
	from	lote_audit_hist_item a,
		lote_audit_hist_guia b,
		lote_audit_hist c,
		lote_auditoria d
	where	a.nr_seq_guia 		= b.nr_sequencia
	and	b.nr_seq_lote_hist 	= c.nr_sequencia
	and	c.nr_seq_lote_audit 	= d.nr_sequencia	
	and	d.dt_fechamento is null
	and	c.dt_fechamento is null
	and	c.dt_envio 	is null
	and	c.nr_analise = (select max(x.nr_analise) from lote_audit_hist x where x.nr_seq_lote_audit = d.nr_sequencia)
	and	b.nr_interno_conta 	= nr_interno_conta_p
	and	nvl(b.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738))	= cd_autorizacao_p
	and	a.ie_acao_glosa 	= 'R'	
	and	trunc(sysdate, 'dd')	> (obter_dt_ultimo_retorno(b.nr_interno_conta, b.cd_autorizacao) + obter_dias_reap_conta(b.nr_interno_conta));


	vl_item_p := nvl(vl_item_vencido_w, 0);
	qt_item_p := nvl(qt_item_vencido_w, 0);

end;

procedure calcula_item_prazo_resp_venc(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, vl_item_p out number, qt_item_p out number) as

qt_item_w number(15);
vl_item_w number(15,2);

begin
	select	count(a.nr_sequencia) qt,
		nvl(sum(a.vl_amenor),0) vl_glosa
	into	qt_item_w,
		vl_item_w
	from	lote_audit_hist_item a,
		lote_audit_hist_guia b,
		lote_audit_hist c,
		lote_auditoria d
	where	a.nr_seq_guia 		= b.nr_sequencia
	and	b.nr_seq_lote_hist 	= c.nr_sequencia
	and	c.nr_seq_lote_audit 	= d.nr_sequencia	
	and	d.dt_fechamento is not null
	and	c.dt_fechamento is not null
	and	c.dt_envio 	is not null
	and	c.nr_analise = (select max(x.nr_analise) from lote_audit_hist x where x.nr_seq_lote_audit = d.nr_sequencia)
	and	b.nr_interno_conta 	= nr_interno_conta_p
	and	nvl(b.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738))	= cd_autorizacao_p
	and	a.ie_acao_glosa 	= 'R'	
	and	trunc(sysdate, 'dd')	> (c.dt_envio + obter_dias_rec_glosa_conta(nr_interno_conta_p))
	and	obter_se_guia_paga_pos_data(b.nr_interno_conta, b.cd_autorizacao, c.dt_envio) = 'N';


	vl_item_p := nvl(vl_item_w, 0);
	qt_item_p := nvl(qt_item_w, 0);

end;

procedure calcula_item_datas(nr_interno_conta_p in conta_paciente.nr_interno_conta%type,
	cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type,
	vl_item_prox_venc_p out number,
	qt_item_prox_venc_p out number,
	vl_item_vencido_p out number,
	qt_item_vencido_p out number,
	vl_item_sem_resp_p out number,
	qt_item_sem_resp_p out number) as
	
qt_item_w	number(15);
vl_item_w	number(15);

dt_atual_trc_w		date;
dt_ultimo_ret_guia_w	date;
qt_dias_reap_conta_w	number;
qt_dias_glosa_conta_w	number;

dt_fechamento_lote_w date;
dt_fechamento_analise_w date;
dt_envio_analise_w date;
dt_baixa_glosa_w date;
	
begin

	vl_item_prox_venc_p := 0;
	qt_item_prox_venc_p := 0;
	vl_item_vencido_p := 0;
	qt_item_vencido_p := 0;
	vl_item_sem_resp_p := 0;
	qt_item_sem_resp_p := 0;

	select 	trunc(sysdate, 'dd'),
		obter_dt_ultimo_retorno(nr_interno_conta_p, cd_autorizacao_p),
		obter_dias_reap_conta(nr_interno_conta_p),
		obter_dias_rec_glosa_conta(nr_interno_conta_p)
	into	dt_atual_trc_w,
		dt_ultimo_ret_guia_w,
		qt_dias_reap_conta_w,
		qt_dias_glosa_conta_w
	from	dual;
	
	select	count(a.nr_sequencia) qt,
		nvl(sum(a.vl_amenor),0) vl_glosa,
		max(d.dt_fechamento) dt_fech_lote,
		max(c.dt_fechamento) dt_fech_analise,
		max(c.dt_envio) dt_envio,
		max(c.dt_baixa_glosa) dt_baixa
	into	qt_item_w,
		vl_item_w,
		dt_fechamento_lote_w,
		dt_fechamento_analise_w,
		dt_envio_analise_w,
		dt_baixa_glosa_w
	from	lote_audit_hist_item a,
		lote_audit_hist_guia b,
		lote_audit_hist c,
		lote_auditoria d
	where	a.nr_seq_guia 		= b.nr_sequencia
	and	b.nr_seq_lote_hist 	= c.nr_sequencia
	and	c.nr_seq_lote_audit 	= d.nr_sequencia
	and	b.nr_interno_conta 	= nr_interno_conta_p	
	and	c.nr_analise = (select max(x.nr_analise) from lote_audit_hist x where x.nr_seq_lote_audit = d.nr_sequencia)
	and	nvl(b.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
	and	a.ie_acao_glosa 	= 'R';
	
	if 	dt_fechamento_lote_w 		is null
		and	dt_fechamento_analise_w is null
		and	dt_envio_analise_w 	is null
		and 	((dt_ultimo_ret_guia_w + qt_dias_reap_conta_w) - dt_atual_trc_w)  between 0 and 15 then
		
		vl_item_prox_venc_p := vl_item_w;
		qt_item_prox_venc_p := qt_item_w;
		
	elsif 	dt_fechamento_lote_w 		is null
		and	dt_fechamento_analise_w is null
		and	dt_envio_analise_w 	is null
		and 	dt_atual_trc_w > (dt_ultimo_ret_guia_w + qt_dias_reap_conta_w) then
		
		vl_item_vencido_p := vl_item_w;
		qt_item_vencido_p := qt_item_w;	
		
	elsif 	dt_fechamento_analise_w is not null
		and	dt_envio_analise_w 	is not null
		and 	dt_atual_trc_w > (dt_envio_analise_w + qt_dias_glosa_conta_w)
		and 	obter_se_guia_paga_pos_data(nr_interno_conta_p, cd_autorizacao_p, dt_envio_analise_w) = 'N' then
		
		vl_item_sem_resp_p := vl_item_w;
		qt_item_sem_resp_p := qt_item_w;
		
	end if;	

end;

procedure calcula_item_pendente(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, qt_item_p out number) as

qt_item_pend_w number(10);

begin

	select	count(a.nr_sequencia)
	into	qt_item_pend_w
	from	lote_audit_hist_item a,
		lote_audit_hist_guia b
	where	a.nr_seq_guia = b.nr_sequencia
	and	b.nr_interno_conta = nr_interno_conta_p
	and	nvl(b.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) = cd_autorizacao_p
	and	a.ie_acao_glosa is null;

	qt_item_p := nvl(qt_item_pend_w, 0);

end;

function obter_estabelecimento_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return estabelecimento.cd_estabelecimento%type as

cd_estab_w	estabelecimento.cd_estabelecimento%type;

begin

	select 	cd_estabelecimento
	into	cd_estab_w
	from	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;

	return cd_estab_w;

end;

function obter_dias_reap_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return convenio_estabelecimento.qt_dias_reapre%type as

qt_dias_w number;

begin
	begin
		select 	x.qt_dias_reapre
		into	qt_dias_w
		from	convenio_estabelecimento x
		where 	x.cd_convenio = to_number(Obter_cd_ds_convenio_conta(nr_interno_conta_p, 'CD'))
		and 	x.cd_estabelecimento = obter_estabelecimento_conta(nr_interno_conta_p);
	exception
	when others then
		qt_dias_w := null;
	end;

	if qt_dias_w is null then
		select	nvl(x.qt_dias_reapresentacao, 30)
		into	qt_dias_w
		from	convenio x
		where	x.cd_convenio = to_number(Obter_cd_ds_convenio_conta(nr_interno_conta_p, 'CD'));
	end if;

	return qt_dias_w;

end;

function obter_dias_rec_glosa_conta(nr_interno_conta_p in conta_paciente.nr_interno_conta%type) return convenio.qt_dias_regurso_glosa%type as

qt_dias_w convenio.qt_dias_regurso_glosa%type;

begin

	select 	nvl(x.qt_dias_regurso_glosa, 30)
	into 	qt_dias_w
	from	convenio x
	where 	x.cd_convenio = to_number(Obter_cd_ds_convenio_conta(nr_interno_conta_p, 'CD'));

	return qt_dias_w;

end;

procedure calcula_item_prox_venc(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type, vl_item_p out number, qt_item_p out number) as

qt_item_vencido_w number(15);
vl_item_vencido_w number(15,2);

dt_atual_trc_w		date;
dt_ultimo_ret_guia_w	date;
qt_dias_glosa_conta_w	number;

begin

	begin
		select 	trunc(sysdate, 'dd'),
			obter_dt_ultimo_retorno(nr_interno_conta_p, cd_autorizacao_p),
			obter_dias_reap_conta(nr_interno_conta_p)
		into	dt_atual_trc_w,
			dt_ultimo_ret_guia_w,
			qt_dias_glosa_conta_w
		from	dual;
	exception
	when 	others then
		dt_atual_trc_w := null;
		dt_ultimo_ret_guia_w := null;
		qt_dias_glosa_conta_w :=  null;
	end;
	
	if 	(dt_atual_trc_w is not null and dt_ultimo_ret_guia_w is not null and qt_dias_glosa_conta_w is not null) then
		select	count(a.nr_sequencia) qt,
			nvl(sum(a.vl_amenor),0) vl_glosa
		into	qt_item_vencido_w,
			vl_item_vencido_w
		from	lote_audit_hist_item a,
			lote_audit_hist_guia b,
			lote_audit_hist c,
			lote_auditoria d
		where	a.nr_seq_guia 		= b.nr_sequencia
		and	b.nr_seq_lote_hist 	= c.nr_sequencia
		and	c.nr_seq_lote_audit 	= d.nr_sequencia
		and	b.nr_interno_conta 	= nr_interno_conta_p
		and	d.dt_fechamento is null
		and	c.dt_fechamento is null
		and	c.dt_envio 	is null
		and	nvl(b.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 	= cd_autorizacao_p
		and	a.ie_acao_glosa 	= 'R'
		and	(dt_atual_trc_w - (dt_ultimo_ret_guia_w + qt_dias_glosa_conta_w) <= 15
			and
			(dt_ultimo_ret_guia_w + qt_dias_glosa_conta_w) <= dt_atual_trc_w)
			;
	end if;
	vl_item_p := nvl(vl_item_vencido_w, 0);
	qt_item_p := nvl(qt_item_vencido_w, 0);

end;

function obter_vencimento_titulo(nr_titulo_p titulo_receber.nr_titulo%type) return date as

dt_vencimento_w	titulo_receber.dt_vencimento%type;

begin
	begin
		select	a.dt_vencimento
		into	dt_vencimento_w
		from	titulo_receber a
		where	a.nr_titulo = nr_titulo_p;
	exception
	when others then
		dt_vencimento_w := null;
	end;

	return dt_vencimento_w;

end;

function obter_pagamento_previsto(nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) return date as

dt_previsto_w date;

begin

	select	max(a.dt_pagamento_previsto)
	into	dt_previsto_w
	from	titulo_receber a
	where	a.nr_titulo = obter_titulo_conta_guia(nr_interno_conta_p, cd_autorizacao_p, null, null);
	
	return dt_previsto_w;

end;

procedure popular_guia(nr_seq_conc_pac_p in conciliacao_conta_paciente.nr_sequencia%type, nr_interno_conta_p in conta_paciente.nr_interno_conta%type) as

cursor 	c_guia is
select	nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) 										cd_autorizacao,
	a.nr_interno_conta 															nr_interno_conta,
	b.dt_mesano_referencia															dt_referencia,
	obter_se_guia_inadimplente(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)),  obter_pagamento_previsto(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738))))		ie_inadimplente,
	obter_nf_conta(a.nr_interno_conta, 1)													seq_nota_fiscal,
	obter_nf_conta(a.nr_interno_conta, 2)													nota_fiscal,	
	a.vl_guia																vl_guia,
	b.nm_usuario																usuario,
	obter_dt_primeiro_retorno(b.nr_seq_protocolo) 												dt_primeiro_retorno,
	(select	max(x.dt_pagamento_previsto) from	titulo_receber x where	x.nr_titulo	= to_number(obter_titulo_conta_guia(a.nr_interno_conta,a.cd_autorizacao,null,null))) dt_vencimento,
	obter_dt_envio_recurso(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)))					dt_envio_recurso,
	obter_dt_previsao_pag_recurso(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)))				dt_previsao_pag_recurso,
	nvl(obter_valores_guia(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)), 'ADIC'), 0)			vl_adicional,
	nvl(obter_valores_guia(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)), 'DESC'), 0)			vl_desconto,
	nvl(obter_valores_guia(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)), 'GLOSA'), 0)			vl_glosado,
	nvl(obter_valores_guia(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)), 'RECEB'), 0)			vl_recebido,
	nvl(obter_valores_guia(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)), 'REC'), 0)			vl_recursado,
	nvl(obter_valores_guia(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)), null), 0)			vl_rec_recuperado,
	OBTER_TITULO_CONTA_GUIA(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)), null, null)			titulo,
	obter_saldo_titulo_guia(a.nr_interno_conta, nvl(a.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)))				saldo_titulo
from	conta_paciente_guia a,
	conta_paciente b,
	protocolo_convenio c
where	a.nr_interno_conta = b.nr_interno_conta
and	b.nr_interno_conta = nr_interno_conta_p
and	b.nr_seq_protocolo = c.nr_seq_protocolo;

nr_seq_con_conta_pac_w	conciliacao_conta_pac_guia.nr_sequencia%type;

qt_item_vencido_w number(15);
vl_item_vencido_w number(15,2);

qt_item_prox_venc_w number(15);
vl_item_prox_venc_w number(15,2);

qt_item_fora_prazo_w number(15);
vl_item_fora_prazo_w number(15, 2);

qt_item_pend_analise_w number(10);

begin	
	for guia in c_guia loop
		qt_guia_w := qt_guia_w + 1;
		printlog('#@ INICIO LOG GUIA @#');
		printlog('guia = ' || guia.cd_autorizacao);
		printlog('guia.dt_primeiro_retorno = ' || guia.dt_primeiro_retorno);
		printlog('guia.ie_inadimplente = ' || guia.ie_inadimplente);
		printlog('guia.dt_vencimento = ' || guia.dt_vencimento);
		printlog('guia.dt_envio_recurso = ' || guia.dt_envio_recurso);
		printlog('guia.dt_previsao_pag_recurso = ' || guia.dt_previsao_pag_recurso);
		printlog('guia.vl_adicional = ' || guia.vl_adicional);
		printlog('guia.vl_desconto = ' || guia.vl_desconto);
		printlog('guia.vl_glosado = ' || guia.vl_glosado);
		printlog('guia.vl_guia = ' || guia.vl_guia);
		printlog('guia.vl_recebido = ' || guia.vl_recebido);
		printlog('guia.vl_recursado = ' || guia.vl_recursado);
		printlog('guia.vl_rec_recuperado = ' || guia.vl_rec_recuperado);
		printlog('guia.titulo = ' || guia.titulo);
		printlog('guia.saldo_titulo = ' || guia.saldo_titulo);
		printlog('guia.cd_autorizacao = ' || guia.cd_autorizacao);
		printlog('guia.nr_interno_conta = ' || guia.nr_interno_conta);


		select 	conciliacao_conta_pac_guia_seq.nextval
		into	nr_seq_con_conta_pac_w
		from	dual;

		calcula_item_datas(guia.nr_interno_conta,
			guia.cd_autorizacao,
			vl_item_prox_venc_w,
			qt_item_prox_venc_w,
			vl_item_vencido_w,
			qt_item_vencido_w,
			vl_item_fora_prazo_w,
			qt_item_fora_prazo_w);

		calcula_item_pendente(guia.nr_interno_conta, guia.cd_autorizacao, qt_item_pend_analise_w);
		
		printlog('vl_item_vencido_w = ' || vl_item_vencido_w);
		printlog('qt_item_vencido_w = ' || qt_item_vencido_w);
		printlog('vl_item_fora_prazo_w = ' || vl_item_fora_prazo_w);
		printlog('qt_item_fora_prazo_w = ' || qt_item_fora_prazo_w);
		printlog('vl_item_prox_venc_w = ' || vl_item_prox_venc_w);
		printlog('qt_item_prox_venc_w = ' || qt_item_prox_venc_w);
		printlog('qt_item_pend_analise_w = ' || qt_item_pend_analise_w);
		printlog('#@ FIM LOG GUIA @#');

		insert into conciliacao_conta_pac_guia
		(
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			nr_sequencia,
			dt_primeiro_retorno,
			dt_previsao_primeiro_pag,
			dt_envio_recurso,
			dt_previsao_pag_recurso,
			vl_adicional,
			vl_desconto_retorno,
			vl_glosa_aceita,
			vl_guia,
			vl_recebido,
			vl_recursado,
			vl_recurso_recuperado,
			nr_titulo_receber,
			vl_saldo_titulo,
			cd_autorizacao,
			nr_interno_conta,
			ie_inadimplencia,
			nr_seq_conta_pac_conc,
			qt_item_recurso_venc, -- Vencido
			vl_item_recurso_venc,
			qt_item_rec_prox_venc, -- Proximo
			vl_item_rec_prox_venc,
			qt_item_fora_prazo_reap, -- Fora prazo reap
			vl_item_fora_prazo_reap,
			qt_item_pend_analise,
			vl_pend_reapresentacao,
			dt_referencia
		)
		values
		(
			sysdate,
			sysdate,
			nm_usuario_job_w,
			nm_usuario_job_w,
			nr_seq_con_conta_pac_w,
			guia.dt_primeiro_retorno,
			guia.dt_vencimento,
			guia.dt_envio_recurso,
			guia.dt_previsao_pag_recurso,
			guia.vl_adicional,
			guia.vl_desconto,
			guia.vl_glosado,
			guia.vl_guia,
			guia.vl_recebido,
			guia.vl_recursado,
			guia.vl_rec_recuperado,
			guia.titulo,
			guia.saldo_titulo,
			guia.cd_autorizacao,
			guia.nr_interno_conta,
			guia.ie_inadimplente,
			nr_seq_conc_pac_p,
			nvl(qt_item_vencido_w, 0),
			nvl(vl_item_vencido_w, 0),
			nvl(qt_item_prox_venc_w, 0),
			nvl(vl_item_prox_venc_w, 0),
			nvl(qt_item_fora_prazo_w, 0),
			nvl(vl_item_fora_prazo_w, 0),
			nvl(qt_item_pend_analise_w, 0),
			(guia.vl_guia - (nvl(guia.vl_recebido,0) + nvl(guia.vl_rec_recuperado,0) + nvl(guia.vl_glosado,0) + nvl(guia.vl_recursado,0))),
			guia.dt_referencia
		);

		popular_usuario_previsto(nr_seq_con_conta_pac_w, guia.nr_interno_conta, guia.cd_autorizacao);		
		popular_glosa(nr_seq_con_conta_pac_w, guia.nr_interno_conta, guia.cd_autorizacao);
		popular_item_conciliado(nr_seq_con_conta_pac_w, guia.nr_interno_conta, guia.cd_autorizacao);

	end loop;
end popular_guia;

procedure popular_usuario_previsto(nr_seq_conc_guia_p in conciliacao_conta_pac_guia.nr_sequencia%type, nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) as

cursor usuarios is
select 	distinct a.nm_usuario_previsto nome
from 	lote_audit_hist_item a,
	lote_audit_hist_guia b
where 	a.nr_seq_guia = b.nr_sequencia
and	a.nm_usuario_previsto is not null
and	b.nr_interno_conta = nr_interno_conta_p
and	b.cd_autorizacao = cd_autorizacao_p;

qt_analisado_w number;
qt_pendente_w number;

nr_seq_usu_resp_w	usuario_resp_item_rec.nr_sequencia%type;

begin
	
	for usuario in usuarios loop
		
		select 	count(a.nr_sequencia)
		into	qt_analisado_w
		from	lote_audit_hist_item a,
			lote_audit_hist_guia b
		where	a.nr_seq_guia 		= b.nr_sequencia
		and	b.nr_interno_conta 	= nr_interno_conta_p
		and	b.cd_autorizacao 	= cd_autorizacao_p
		and	a.ie_acao_glosa 	is not null
		and	a.nm_usuario_resp 	= usuario.nome;
		
		select 	count(a.nr_sequencia)
		into	qt_pendente_w
		from	lote_audit_hist_item a,
			lote_audit_hist_guia b
		where	a.nr_seq_guia 		= b.nr_sequencia
		and	b.nr_interno_conta 	= nr_interno_conta_p
		and	b.cd_autorizacao 	= cd_autorizacao_p
		and	a.ie_acao_glosa 	is null
		and	a.nm_usuario_previsto 	= usuario.nome;
		
		select	usuario_resp_item_rec_seq.nextval
		into	nr_seq_usu_resp_w
		from	dual;
		
		insert into usuario_resp_item_rec
		(
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario_previsto,
			qt_itens_analisados,
			qt_itens_pendentes,
			nr_sequencia,
			nr_seq_conc_guia
		)
		values
		(
			nm_usuario_job_w,
			nm_usuario_job_w,
			sysdate,
			sysdate,
			usuario.nome,
			nvl(qt_analisado_w, 0),
			nvl(qt_pendente_w, 0),			
			nr_seq_usu_resp_w,
			nr_seq_conc_guia_p
		);
		
	end loop;
	
end;

procedure popular_item_conciliado(nr_seq_conc_guia_p in conciliacao_conta_pac_guia.nr_sequencia%type, nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) as

cursor 	itens	is
select	(select x.cd_material 
	from 	material_atend_paciente x
	where 	x.nr_sequencia = a.nr_seq_matpaci) 	cd_material,
	(select x.cd_procedimento 
	from 	procedimento_paciente x
	where 	x.nr_sequencia = a.nr_seq_propaci) 	cd_procedimento,
	(select x.ie_origem_proced 
	from 	procedimento_paciente x
	where 	x.nr_sequencia = a.nr_seq_propaci) 	ie_origem_proced,
	a.qt_item					qt_item,
	a.vl_glosa					vl_glosa
from	lote_audit_hist_item a,
	lote_audit_hist_guia b
where	a.nr_seq_guia 		= b.nr_sequencia
and	a.ie_acao_glosa		is not null
and	a.ie_acao_glosa		in ('A', 'P')
and	b.nr_interno_conta 	= nr_interno_conta_p
and	b.cd_autorizacao 	= cd_autorizacao_p;

type	rec is record (
	procedimento		conciliacao_item_ret_rec.cd_procedimento%type,
	origem			conciliacao_item_ret_rec.ie_origem_proced%type,
	material		conciliacao_item_ret_rec.cd_material%type,
	qt			conciliacao_item_ret_rec.qt_glosada%type,
	vl			conciliacao_item_ret_rec.vl_glosa_aceita%type
);

type	rec_tbl is table of rec index by binary_integer;

rec_v 	rec_tbl;

i_exist binary_integer;

nr_seq_conc_ret_w	conciliacao_item_ret_rec.nr_sequencia%type;

begin
	rec_v.delete;	
	for item in itens loop
		if rec_v.count > 0 then
			i_exist := -1;
			for i in rec_v.first .. rec_v.last loop
				if 	(item.cd_procedimento = rec_v(i).procedimento and item.ie_origem_proced = rec_v(i).origem)
					or 
					(item.cd_material = rec_v(i).material) then
					i_exist := i;
					exit;
				end if;
			end loop;
			if i_exist <> -1 then				
				rec_v(i_exist).qt := rec_v(i_exist).qt + item.qt_item;
				rec_v(i_exist).vl := rec_v(i_exist).vl + item.vl_glosa;
			else
				i_exist := rec_v.last+1;
				rec_v(i_exist).procedimento 	:= item.cd_procedimento;
				rec_v(i_exist).origem 		:= item.ie_origem_proced;
				rec_v(i_exist).material 	:= item.cd_material;
				rec_v(i_exist).qt 		:= item.qt_item;
				rec_v(i_exist).vl 		:= item.vl_glosa;
			end if;
		else
			i_exist := nvl(rec_v.first, 0);
			rec_v(i_exist).procedimento 		:= item.cd_procedimento;
			rec_v(i_exist).origem 			:= item.ie_origem_proced;
			rec_v(i_exist).material 		:= item.cd_material;
			rec_v(i_exist).qt 			:= item.qt_item;
			rec_v(i_exist).vl 			:= item.vl_glosa;
		end if;
	end loop;
	
	if rec_v.count > 0 then
		for i in rec_v.first .. rec_v.last loop

			select	conciliacao_item_ret_rec_seq.nextval
			into	nr_seq_conc_ret_w
			from	dual;
			printlog('#@ INICIO LOG TBL ITEM #@');
			printlog('i = ' || i);
			printlog('material = ' || rec_v(i).material);
			printlog('procedimento = ' || rec_v(i).procedimento);
			printlog('origem = ' || rec_v(i).origem);
			printlog('qt = ' || rec_v(i).qt);
			printlog('vl = ' || rec_v(i).vl);
			printlog('#@ FIM LOG TBL ITEM #@');
			insert into conciliacao_item_ret_rec
			(
				cd_material,
				cd_procedimento,
				ie_origem_proced,
				qt_glosada,
				vl_glosa_aceita,
				nr_sequencia,
				nr_seq_conc_guia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec
			)
			values
			(
				rec_v(i).material,
				rec_v(i).procedimento,
				rec_v(i).origem,
				rec_v(i).qt,
				rec_v(i).vl,
				nr_seq_conc_ret_w,
				nr_seq_conc_guia_p,
				sysdate,
				sysdate,
				nm_usuario_job_w,
				nm_usuario_job_w
			);
		end loop;
	end if;
end;

function obter_motivo_ans(cd_motivo_glosa_p in motivo_glosa.cd_motivo_glosa%type) return tiss_motivo_glosa.cd_motivo_tiss%type as

cd_motivo_ans_w	tiss_motivo_glosa.cd_motivo_tiss%type;

begin
	begin
	select	max(a.cd_motivo_tiss)
	into	cd_motivo_ans_w
	from	tiss_motivo_glosa a
	where	a.cd_motivo_glosa = cd_motivo_glosa_p;
	exception
	when others then	
		cd_motivo_ans_w := null;
	end;

	return cd_motivo_ans_w;

end;

procedure popular_glosa(nr_seq_conc_guia_p in conciliacao_conta_pac_guia.nr_sequencia%type, nr_interno_conta_p in conta_paciente.nr_interno_conta%type, cd_autorizacao_p in conta_paciente_guia.cd_autorizacao%type) as

cursor 	c_glosa is
select	a.cd_motivo_glosa							cd_motivo_glosa,
	a.cd_motivo_glosa_tiss							cd_motivo_ans,
	a.nm_usuario								nm_usuario
from	lote_audit_hist_item a,
	lote_audit_hist_guia b
where	a.nr_seq_guia 		= b.nr_sequencia
and	b.nr_interno_conta 	= nr_interno_conta_p
and	nvl(b.cd_autorizacao, wheb_mensagem_pck.get_texto(1097738)) = cd_autorizacao_p
and	(a.cd_motivo_glosa is not null or a.cd_motivo_glosa_tiss is not null);

type glosa is record (
	cd_motivo	motivo_glosa.cd_motivo_glosa%type,
	cd_motivo_ans	tiss_motivo_glosa.cd_motivo_tiss%type,
	qt_motivo	number	
);

type glosa_v is table of glosa index by pls_integer;

glosas	glosa_v;

i pls_integer;
i_exist pls_integer;
k pls_integer;

nr_seq_mot_glosa_w	motivo_glosa_itens.nr_sequencia%type;

nm_usuario_w	usuario.nm_usuario%type;

begin
	glosas.delete;
	i := 0;	
	for glosa in c_glosa loop
		nm_usuario_w := glosa.nm_usuario;
		qt_glosa_w := qt_glosa_w + 1;
		i_exist := -1;
		if glosas.count > 0 then
			for j in glosas.first .. glosas.last loop
				if i_exist = -1 and glosas(j).cd_motivo = glosa.cd_motivo_glosa 
					and glosas(j).cd_motivo_ans = glosa.cd_motivo_ans then
					i_exist := j;
				end if;
			end loop;

			if i_exist <> -1 then
				glosas(i_exist).qt_motivo := glosas(i_exist).qt_motivo + 1;
			else
				k := glosas.last + 1;
				glosas(k).cd_motivo := glosa.cd_motivo_glosa;				
				glosas(k).cd_motivo_ans := glosa.cd_motivo_ans;
				glosas(k).qt_motivo := 1;
			end if;
		else
			glosas(0).cd_motivo := glosa.cd_motivo_glosa;
			glosas(0).cd_motivo_ans := glosa.cd_motivo_ans;
			glosas(0).qt_motivo := 1;			
		end if;		
	end loop;
	printlog('#@ INICIO LOG GLOSA ITENS @#');
	if (glosas.count > 0) then
		for j in glosas.first .. glosas.last loop
			select	motivo_glosa_itens_seq.nextval
			into	nr_seq_mot_glosa_w
			from	dual;

			printlog('j = ' || j);
			printlog('cd = ' || glosas(j).cd_motivo);
			printlog('ans = ' || glosas(j).cd_motivo_ans);
			printlog('qt = ' || glosas(j).qt_motivo);
			insert into motivo_glosa_itens
			(				
				cd_motivo_glosa,
				cd_motivo_ans,
				qt_incidencia,
				nr_seq_conciliacao_guia,
				nr_sequencia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec
			)
			values
			(
				glosas(j).cd_motivo,
				glosas(j).cd_motivo_ans,
				glosas(j).qt_motivo,
				nr_seq_conc_guia_p,
				nr_seq_mot_glosa_w,
				sysdate,
				sysdate,
				nvl(wheb_usuario_pck.get_nm_usuario, nm_usuario_w),
				nvl(wheb_usuario_pck.get_nm_usuario, nm_usuario_w)
			);

		end loop;
	end if;
	printlog('#@ FIM LOG GLOSA ITENS @#');
end;

function obter_max_cd_glosa_conv(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return motivo_glosa.cd_motivo_glosa%type as

cd_mot_w motivo_glosa.cd_motivo_glosa%type;

begin

	select 	cd
	into	cd_mot_w
	from 	(
		select  a.cd_motivo_glosa cd	
		from 	motivo_glosa_itens a,
			conciliacao_conta_pac_guia b,
			conciliacao_conta_paciente c,
			protocolo_faturado d
		where 	a.nr_seq_conciliacao_guia = b.nr_sequencia
		and 	b.nr_seq_conta_pac_conc = c.nr_sequencia
		and 	c.nr_seq_protoc_faturado = d.nr_sequencia
		and	(dt_ref_p is null or trunc(d.dt_referencia, 'mm') = trunc(dt_ref_p, 'mm'))
		and 	d.cd_convenio = cd_convenio_p
		and 	a.cd_motivo_glosa is not null	
		group by a.cd_motivo_glosa
		order by sum(a.qt_incidencia) desc
	)
	where rownum = 1;

	return cd_mot_w;

end;

function obter_max_cd_tiss_conv(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return tiss_motivo_glosa.cd_motivo_tiss%type as

cd_mot_w tiss_motivo_glosa.cd_motivo_tiss%type;

begin

	select	cd
	into	cd_mot_w
	from	(
		select  a.cd_motivo_ans cd	
		from 	motivo_glosa_itens a,
			conciliacao_conta_pac_guia b,
			conciliacao_conta_paciente c,
			protocolo_faturado d
		where 	a.nr_seq_conciliacao_guia = b.nr_sequencia
		and 	b.nr_seq_conta_pac_conc = c.nr_sequencia
		and 	c.nr_seq_protoc_faturado = d.nr_sequencia
		and	(dt_ref_p is null or trunc(d.dt_referencia, 'mm') = trunc(dt_ref_p, 'mm'))
		and 	d.cd_convenio = cd_convenio_p
		and 	a.cd_motivo_ans is not null
		group by a.cd_motivo_ans
		order by sum(a.qt_incidencia) desc
	)
	where rownum = 1;

	return cd_mot_w;

end;

function obter_maior_glosa_mat_proc(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return varchar2 as

ie_ret_w varchar2(1);

vl_proc_w number(15, 2);
vl_mat_w number(15, 2);

begin
	begin
		select	sum(nvl(a.vl_glosa_aceita,0))
		into	vl_mat_w
		from    conciliacao_item_ret_rec a,
			conciliacao_conta_pac_guia b,
			conciliacao_conta_paciente c,
			protocolo_faturado d
		where   a.nr_seq_conc_guia          	= b.nr_sequencia
		and     b.nr_seq_conta_pac_conc     	= c.nr_sequencia
		and     c.nr_seq_protoc_faturado    	= d.nr_sequencia
		and	(dt_ref_p is null or trunc(d.dt_referencia, 'mm') 	= trunc(dt_ref_p, 'mm'))
		and	a.cd_procedimento 	    	is null
		and	a.ie_origem_proced		is null
		and	a.cd_material			is not null
		and	d.cd_convenio = cd_convenio_p;
	exception
	when others then
		vl_mat_w := 0;
	end;
	
	begin
		select	sum(nvl(a.vl_glosa_aceita,0))
		into	vl_proc_w
		from    conciliacao_item_ret_rec a,
			conciliacao_conta_pac_guia b,
			conciliacao_conta_paciente c,
			protocolo_faturado d
		where   a.nr_seq_conc_guia          	= b.nr_sequencia
		and     b.nr_seq_conta_pac_conc     	= c.nr_sequencia
		and     c.nr_seq_protoc_faturado    	= d.nr_sequencia
		and	(dt_ref_p is null or trunc(d.dt_referencia, 'mm') 	= trunc(dt_ref_p, 'mm'))
		and	a.cd_procedimento 	    	is not null
		and	a.ie_origem_proced		is not null
		and	a.cd_material			is null
		and	d.cd_convenio 		    	= cd_convenio_p;
	exception
	when others then
		vl_proc_w := 0;
	end;
	
	if nvl(vl_proc_w,0) > nvl(vl_mat_w,0) then
		ie_ret_w := 'P';
	else
		ie_ret_w := 'M';
	end if;

	return ie_ret_w;

end;

function obter_maior_item_glosa(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return procedimento_paciente.cd_procedimento%type as

cd_item_w	procedimento_paciente.cd_procedimento%type;

begin

	if obter_maior_glosa_mat_proc(cd_convenio_p, dt_ref_p) = 'P' then
		select 	cd
		into	cd_item_w
		from	(
			select	a.cd_procedimento cd
			from    conciliacao_item_ret_rec a,
				conciliacao_conta_pac_guia b,
				conciliacao_conta_paciente c,
				protocolo_faturado d
			where   a.nr_seq_conc_guia          	= b.nr_sequencia
			and     b.nr_seq_conta_pac_conc     	= c.nr_sequencia
			and     c.nr_seq_protoc_faturado    	= d.nr_sequencia
			and	a.cd_procedimento 		is not null
			and	a.ie_origem_proced 		is not null
			and	a.cd_material 			is null
			and	d.cd_convenio 			= cd_convenio_p
			and	(dt_ref_p is null or trunc(d.dt_referencia, 'mm')	= trunc(dt_ref_p, 'mm'))
			group	by				a.cd_procedimento
			order	by				sum(a.vl_glosa_aceita) 	desc,
								sum(a.qt_glosada) 	desc
		)
		where rownum = 1;
	else
		select	cd
		into	cd_item_w
		from	(
			select	a.cd_material cd
			from    conciliacao_item_ret_rec a,
				conciliacao_conta_pac_guia b,
				conciliacao_conta_paciente c,
				protocolo_faturado d
			where   a.nr_seq_conc_guia          	= b.nr_sequencia
			and     b.nr_seq_conta_pac_conc     	= c.nr_sequencia
			and     c.nr_seq_protoc_faturado    	= d.nr_sequencia
			and	a.cd_procedimento 		is null
			and	a.ie_origem_proced 		is null
			and	a.cd_material 			is not null
			and	d.cd_convenio 			= cd_convenio_p		
			and	(dt_ref_p is null or trunc(d.dt_referencia, 'mm')	= trunc(dt_ref_p, 'mm'))
			group	by				a.cd_material
			order	by				sum(a.vl_glosa_aceita) 	desc,
								sum(a.qt_glosada) 	desc
		)
		where rownum = 1;
	end if;

	return cd_item_w;

end;

function obter_desc_item_maior_glosa(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return varchar2 as

ds_item_w	varchar2(2000);

cd_proc_w	procedimento_paciente.cd_procedimento%type;
ie_proc_w	procedimento_paciente.ie_origem_proced%type;

begin

	if obter_maior_glosa_mat_proc(cd_convenio_p, dt_ref_p) = 'P' then
		select	cd, ie
		into	cd_proc_w,
			ie_proc_w
		from	(
			select	a.cd_procedimento cd,
				a.ie_origem_proced ie		
			from    conciliacao_item_ret_rec a,
				conciliacao_conta_pac_guia b,
				conciliacao_conta_paciente c,
				protocolo_faturado d
			where   a.nr_seq_conc_guia          	= b.nr_sequencia
			and     b.nr_seq_conta_pac_conc     	= c.nr_sequencia
			and     c.nr_seq_protoc_faturado    	= d.nr_sequencia
			and	a.cd_procedimento 		is not null
			and	a.ie_origem_proced 		is not null
			and	a.cd_material 			is null
			and	d.cd_convenio 			= cd_convenio_p
			and	(dt_ref_p is null or trunc(d.dt_referencia, 'mm') = trunc(dt_ref_p, 'mm'))

			group	by				a.cd_procedimento, 
								a.ie_origem_proced
			order	by				sum(a.vl_glosa_aceita) 	desc,
								sum(a.qt_glosada) 	desc
		)
		where	rownum				= 1;

		select	obter_desc_procedimento(cd_proc_w, ie_proc_w)
		into	ds_item_w
		from	dual;
	else
		select 	obter_desc_material(obter_maior_item_glosa(cd_convenio_p, dt_ref_p))
		into	ds_item_w
		from 	dual;
	end if;

	return ds_item_w;

end;

function obter_qt_item_conv(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return number as

qt_item_w number(15);

begin

	begin
		select	count(a.nr_sequencia)
		into	qt_item_w
		from	motivo_glosa_itens a,
			conciliacao_conta_pac_guia b,
			conciliacao_conta_paciente c,
			protocolo_faturado d
		where	a.nr_seq_conciliacao_guia       = b.nr_sequencia
		and	b.nr_seq_conta_pac_conc		= c.nr_sequencia
		and	c.nr_seq_protoc_faturado	= d.nr_sequencia
		and	d.cd_convenio			= cd_convenio_p
		and	(dt_ref_p is null or trunc(d.dt_referencia, 'mm') = trunc(dt_ref_p, 'mm'));
	exception
	when others then
		qt_item_w := 0;
	end;

	return qt_item_w;

end;

function obter_valor_inadimplencia(cd_convenio_p in convenio.cd_convenio%type, dt_ref_p in date) return number as

vl_inadimplencia_w number(15,2) := 0;

begin

	begin
		select	nvl(sum(nvl(a.vl_guia,0)), 0)
		into	vl_inadimplencia_w
		from	conciliacao_conta_pac_guia a,
			conciliacao_conta_paciente b,
			protocolo_faturado c
		where	nvl(a.ie_inadimplencia, 'N') = 'S'
		and	a.nr_seq_conta_pac_conc = b.nr_sequencia
		and	b.nr_seq_protoc_faturado = c.nr_sequencia
		and	c.cd_convenio	= cd_convenio_p
		and	trunc(c.dt_referencia, 'mm') = trunc(nvl(dt_ref_p, add_months(sysdate, -2)), 'mm');
	exception
	when others then
		vl_inadimplencia_w := 0;
	end;
	
	return vl_inadimplencia_w;

end;

function obter_valor_inad_prot(nr_seq_prot_fatur_p in protocolo_faturado.nr_sequencia%type) return number as

vl_inadimplencia_w number(15,2) := 0;

begin

	begin
		select	nvl(sum(nvl(a.vl_guia,0)), 0)
		into	vl_inadimplencia_w
		from	conciliacao_conta_pac_guia a,
			conciliacao_conta_paciente b,
			protocolo_faturado c
		where	nvl(a.ie_inadimplencia, 'N') = 'S'
		and	a.nr_seq_conta_pac_conc = b.nr_sequencia
		and	b.nr_seq_protoc_faturado = c.nr_sequencia
		and	c.nr_sequencia	= nr_seq_prot_fatur_p;
	exception
	when others then
		vl_inadimplencia_w := 0;
	end;
	
	return vl_inadimplencia_w;

end;

function obter_valor_inad_conta(nr_seq_conta_pac_conc_p in conciliacao_conta_paciente.nr_sequencia%type) return number as

vl_inadimplencia_w number(15,2) := 0;

begin

	begin
		select	nvl(sum(nvl(a.vl_guia,0)), 0)
		into	vl_inadimplencia_w
		from	conciliacao_conta_pac_guia a,
			conciliacao_conta_paciente b
		where	nvl(a.ie_inadimplencia, 'N') = 'S'
		and	a.nr_seq_conta_pac_conc = b.nr_sequencia
		and	b.nr_sequencia = nr_seq_conta_pac_conc_p;
	exception
	when others then
		vl_inadimplencia_w := 0;
	end;
	
	return vl_inadimplencia_w;

end;


function obter_valor_inad_guia(nr_seq_conta_pac_guia_p in conciliacao_conta_pac_guia.nr_sequencia%type) return number as

vl_inadimplencia_w number(15,2) := 0;

begin

	begin
		select	nvl(sum(nvl(a.vl_guia,0)), 0)
		into	vl_inadimplencia_w
		from	conciliacao_conta_pac_guia a
		where	nvl(a.ie_inadimplencia, 'N') = 'S'
		and	a.nr_sequencia = nr_seq_conta_pac_guia_p;
	exception
	when others then
		vl_inadimplencia_w := 0;
	end;
	
	return vl_inadimplencia_w;

end;

end tiss_dashboard_data_pck;
/
