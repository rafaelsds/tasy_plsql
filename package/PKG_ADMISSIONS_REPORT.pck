CREATE OR REPLACE PACKAGE PKG_ADMISSIONS_REPORT AS
  --
  FUNCTION GET_ELECTIVE_ADMISSION(NR_ATENDIMENTO_P ATENDIMENTO_PACIENTE.NR_ATENDIMENTO%TYPE) RETURN NUMBER;
  --
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0);
  --
END PKG_ADMISSIONS_REPORT;
/
CREATE OR REPLACE PACKAGE BODY PKG_ADMISSIONS_REPORT AS

  PROCEDURE TABLE_CLEANING IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_ADMISSIONS_REPORT';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_ADMISSIONS_REPORT_BY_DAY';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_ADMISSIONS_REP_BY_RANGE';
  END TABLE_CLEANING;
  --
  FUNCTION GET_ELECTIVE_ADMISSION(NR_ATENDIMENTO_P ATENDIMENTO_PACIENTE.NR_ATENDIMENTO%TYPE) RETURN NUMBER IS
  
  NR_W NUMBER;
  
  BEGIN
  
    NR_W := 0;
    
    BEGIN
      
      SELECT 1
        INTO NR_W
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM AGENDA_QUIMIO Q
                      WHERE Q.NR_ATENDIMENTO IS NOT NULL
                        AND Q.NR_ATENDIMENTO = NR_ATENDIMENTO_P
                        AND Q.IE_STATUS_AGENDA <> 'C');

    EXCEPTION WHEN NO_DATA_FOUND THEN
      
      BEGIN
        
        SELECT 1
          INTO NR_W
          FROM DUAL
         WHERE EXISTS (SELECT 1
                         FROM AGENDA_CONSULTA C
                        WHERE C.NR_ATENDIMENTO IS NOT NULL
                          AND C.NR_ATENDIMENTO = NR_ATENDIMENTO_P
                          AND C.IE_STATUS_AGENDA <> 'C');

      EXCEPTION WHEN NO_DATA_FOUND THEN
        
        BEGIN
        
          SELECT 1
            INTO NR_W
            FROM DUAL
           WHERE EXISTS (SELECT 1
                           FROM AGENDA_PACIENTE AP
                          WHERE AP.NR_ATENDIMENTO IS NOT NULL
                            AND AP.NR_ATENDIMENTO = NR_ATENDIMENTO_P
                            AND AP.IE_STATUS_AGENDA <> 'C');

        EXCEPTION WHEN NO_DATA_FOUND THEN
          
          NR_W := 0;
        
        END;
      
      END;
    
    END;  
    
    RETURN NR_W;

  END;
  --
  PROCEDURE PRE_PROCESS(
    P_START_DATE IN DATE, 
    P_END_DATE   IN DATE,
    P_CD_ESTAB   IN NUMBER,
    P_CD_SETOR   IN NUMBER) IS
    --
    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
    --
    CURSOR C_ADMISSIONS IS 
      SELECT D.*,
             DECODE(D.IS_ELECTIVE, 0, 1, 0) IS_EMERGENT
        FROM (SELECT C.*,
                     PKG_ADMISSIONS_REPORT.GET_ELECTIVE_ADMISSION(C.NR_ATENDIMENTO) IS_ELECTIVE            
                FROM (SELECT B.*,
                             TRIM(TO_CHAR(B.DT_ENTRY, 'Day', 'NLS_DATE_LANGUAGE = American')) DAY_OF_WEEK,
                             TRUNC(TO_CHAR(B.DT_ENTRY, 'HH24') / 4) + 1 RANGE_OF_DAY
                        FROM (SELECT A.*,
                                     PKG_ATEND_PAC_UNID.GET_ESTAB_DT(A.CD_ESTABELECIMENTO, A.DT_ENTRADA_SETOR) DT_ENTRY
                                FROM (SELECT S.CD_ESTABELECIMENTO,
                                             U.CD_SETOR_ATENDIMENTO,
                                             U.NR_ATENDIMENTO,
                                             MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR
                                        FROM ATEND_PACIENTE_UNIDADE U,
                                             SETOR_ATENDIMENTO S
                                       WHERE S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                                         AND DECODE(P_CD_ESTAB, 0, 0, S.CD_ESTABELECIMENTO) = P_CD_ESTAB
                                         AND DECODE(P_CD_SETOR, 0, 0, U.CD_SETOR_ATENDIMENTO) = P_CD_SETOR
                                         AND EXISTS(SELECT 1
                                                      FROM USUARIO_SETOR US
                                                     WHERE US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO
                                                       AND US.NM_USUARIO_PARAM = NM_USUARIO_W)
                                         AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                                         AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                                         AND U.DT_ENTRADA_UNIDADE BETWEEN PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, P_START_DATE)
                                                                      AND PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(P_END_DATE, SYSDATE))
                                       GROUP BY U.NR_ATENDIMENTO,
                                                S.CD_ESTABELECIMENTO,
                                                U.CD_SETOR_ATENDIMENTO,
                                                PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO, U.DT_ENTRADA_UNIDADE, U.CD_SETOR_ATENDIMENTO))
                                     A)
                             B)
                     C)
             D;
    --
    TYPE T_ADMISSIONS IS TABLE OF C_ADMISSIONS%ROWTYPE;
    R_ADMISSIONS T_ADMISSIONS;
    --
    CURSOR C_ADMISSIONS_BY_DAY IS
      SELECT A.CD_SETOR_ATENDIMENTO,
             SUM(A.IE_ELETIVA) ADM_ELECT,
             SUM(A.IE_EMERGENCIAL) ADM_EMERG,
             SUM(1) ADM_TOTAL,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Monday', IE_ELETIVA, 0)) MON_ELECT,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Monday', IE_EMERGENCIAL, 0)) MON_EMERG,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Monday', 1, 0)) MON_TOTAL,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Tuesday', IE_ELETIVA, 0)) TUE_ELECT,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Tuesday', IE_EMERGENCIAL, 0)) TUE_EMERG,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Tuesday', 1, 0)) TUE_TOTAL,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Wednesday', IE_ELETIVA, 0)) WED_ELECT,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Wednesday', IE_EMERGENCIAL, 0)) WED_EMERG,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Wednesday', 1, 0)) WED_TOTAL,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Thursday', IE_ELETIVA, 0)) THU_ELECT,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Thursday', IE_EMERGENCIAL, 0)) THU_EMERG,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Thursday', 1, 0)) THU_TOTAL,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Friday', IE_ELETIVA, 0)) FRI_ELECT,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Friday', IE_EMERGENCIAL, 0)) FRI_EMERG,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Friday', 1, 0)) FRI_TOTAL,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Saturday', IE_ELETIVA, 0)) SAT_ELECT,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Saturday', IE_EMERGENCIAL, 0)) SAT_EMERG,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Saturday', 1, 0)) SAT_TOTAL,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Sunday', IE_ELETIVA, 0)) SUN_ELECT,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Sunday', IE_EMERGENCIAL, 0)) SUN_EMERG,
             SUM(DECODE(A.DS_DIA_SEMANA, 'Sunday', 1, 0)) SUN_TOTAL
        FROM W_ADMISSIONS_REPORT A
       GROUP BY A.CD_SETOR_ATENDIMENTO;
    --
    TYPE T_ADMISSIONS_BY_DAY IS TABLE OF C_ADMISSIONS_BY_DAY%ROWTYPE;
    R_ADMISSIONS_BY_DAY T_ADMISSIONS_BY_DAY;
    --
    CURSOR C_ADMISSIONS_BY_RANGE IS
      SELECT B.CD_SETOR_ATENDIMENTO,
             B.NR INDEX_OF_DAY,
             B.DAY DAY_OF_WEEK,
             B.TOTAL_ELECT,
             B.TOTAL_EMERG,
             B.TOTAL_ADMIS,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 1, R.IE_ELETIVA, 0), 0)) RANGE_1_ELECT,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 1, R.IE_EMERGENCIAL, 0), 0)) RANGE_1_EMERG,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 1, 1, 0), 0)) RANGE_1_TOTAL,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 2, R.IE_ELETIVA, 0), 0)) RANGE_2_ELECT,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 2, R.IE_EMERGENCIAL, 0), 0)) RANGE_2_EMERG,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 2, 1, 0), 0)) RANGE_2_TOTAL,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 3, R.IE_ELETIVA, 0), 0)) RANGE_3_ELECT,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 3, R.IE_EMERGENCIAL, 0), 0)) RANGE_3_EMERG,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 3, 1, 0), 0)) RANGE_3_TOTAL,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 4, R.IE_ELETIVA, 0), 0)) RANGE_4_ELECT,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 4, R.IE_EMERGENCIAL, 0), 0)) RANGE_4_EMERG,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 4, 1, 0), 0)) RANGE_4_TOTAL,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 5, R.IE_ELETIVA, 0), 0)) RANGE_5_ELECT,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 5, R.IE_EMERGENCIAL, 0), 0)) RANGE_5_EMERG,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 5, 1, 0), 0)) RANGE_5_TOTAL,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 6, R.IE_ELETIVA, 0), 0)) RANGE_6_ELECT,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 6, R.IE_EMERGENCIAL, 0), 0)) RANGE_6_EMERG,
             SUM(DECODE(B.DAY, R.DS_DIA_SEMANA, DECODE(R.NR_FAIXA_DIA, 6, 1, 0), 0)) RANGE_6_TOTAL
        FROM (WITH DAYS AS (SELECT LEVEL NR,
                                   DECODE(LEVEL, 1, 'Monday', 2, 'Tuesday', 3, 'Wednesday', 4, 'Thursday', 5, 'Friday', 6, 'Saturday', 'Sunday') DAY
                              FROM DUAL
                            CONNECT BY LEVEL <= 7) 
              SELECT A.CD_SETOR_ATENDIMENTO,
                     D.*,
                     DECODE(D.NR, 1, A.NR_SEG_ELET, 2, A.NR_TER_ELET, 3, A.NR_QUA_ELET, 4, A.NR_QUI_ELET, 5, A.NR_SEX_ELET, 6, A.NR_SAB_ELET, A.NR_DOM_ELET) TOTAL_ELECT,
                     DECODE(D.NR, 1, A.NR_SEG_EMERG, 2, A.NR_TER_EMERG, 3, A.NR_QUA_EMERG, 4, A.NR_QUI_EMERG, 5, A.NR_SEX_EMERG, 6, A.NR_SAB_EMERG, A.NR_DOM_EMERG) TOTAL_EMERG,
                     DECODE(D.NR, 1, A.NR_SEG_TOTAL, 2, A.NR_TER_TOTAL, 3, A.NR_QUA_TOTAL, 4, A.NR_QUI_TOTAL, 5, A.NR_SEX_TOTAL, 6, A.NR_SAB_TOTAL, A.NR_DOM_TOTAL) TOTAL_ADMIS    
                FROM W_ADMISSIONS_REPORT_BY_DAY A,
                     DAYS D) B,
             W_ADMISSIONS_REPORT R
       WHERE B.CD_SETOR_ATENDIMENTO = R.CD_SETOR_ATENDIMENTO
       GROUP BY B.CD_SETOR_ATENDIMENTO, B.NR, B.DAY, B.TOTAL_ELECT, B.TOTAL_EMERG, B.TOTAL_ADMIS;
    --
    TYPE T_ADMISSIONS_BY_RANGE IS TABLE OF C_ADMISSIONS_BY_RANGE%ROWTYPE;
    R_ADMISSIONS_BY_RANGE T_ADMISSIONS_BY_RANGE;
    --  
  BEGIN
    --
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    
    OPEN C_ADMISSIONS;
    LOOP
      FETCH C_ADMISSIONS BULK COLLECT INTO R_ADMISSIONS LIMIT 1000;
        
        BEGIN
          FOR i IN R_ADMISSIONS.FIRST..R_ADMISSIONS.LAST LOOP
            INSERT INTO W_ADMISSIONS_REPORT (
              cd_estabelecimento, 
              cd_setor_atendimento, 
              nr_atendimento, 
              dt_entrada, 
              ds_dia_semana, 
              nr_faixa_dia, 
              ie_eletiva, 
              ie_emergencial)
            VALUES (
              R_ADMISSIONS(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
              R_ADMISSIONS(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
              R_ADMISSIONS(i).NR_ATENDIMENTO, --nr_atendimento, 
              R_ADMISSIONS(i).DT_ENTRY, --dt_entrada, 
              R_ADMISSIONS(i).DAY_OF_WEEK, --ds_dia_semana,
              R_ADMISSIONS(i).RANGE_OF_DAY, --nr_faixa_dia, 
              R_ADMISSIONS(i).IS_ELECTIVE, --ie_eletiva, 
              R_ADMISSIONS(i).IS_EMERGENT); --is_emergencial)
          END LOOP;         
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
        COMMIT;
            
      EXIT WHEN C_ADMISSIONS%NOTFOUND; 
    END LOOP;
    CLOSE C_ADMISSIONS;

    OPEN C_ADMISSIONS_BY_DAY;
    LOOP
      FETCH C_ADMISSIONS_BY_DAY BULK COLLECT INTO R_ADMISSIONS_BY_DAY LIMIT 1000;
        
        BEGIN
          FOR i IN R_ADMISSIONS_BY_DAY.FIRST..R_ADMISSIONS_BY_DAY.LAST LOOP
            INSERT INTO W_ADMISSIONS_REPORT_BY_DAY (
              cd_setor_atendimento, 
              nr_adm_elet, 
              nr_adm_emerg, 
              nr_adm_total, 
              nr_seg_elet, 
              nr_seg_emerg, 
              nr_seg_total, 
              nr_ter_elet, 
              nr_ter_emerg, 
              nr_ter_total, 
              nr_qua_elet, 
              nr_qua_emerg, 
              nr_qua_total, 
              nr_qui_elet, 
              nr_qui_emerg, 
              nr_qui_total, 
              nr_sex_elet, 
              nr_sex_emerg, 
              nr_sex_total, 
              nr_sab_elet, 
              nr_sab_emerg, 
              nr_sab_total, 
              nr_dom_elet, 
              nr_dom_emerg, 
              nr_dom_total)
            VALUES (
              R_ADMISSIONS_BY_DAY(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
              R_ADMISSIONS_BY_DAY(i).ADM_ELECT, --nr_adm_elet, 
              R_ADMISSIONS_BY_DAY(i).ADM_EMERG, --nr_adm_emerg, 
              R_ADMISSIONS_BY_DAY(i).ADM_TOTAL, --nr_adm_total, 
              R_ADMISSIONS_BY_DAY(i).MON_ELECT, --nr_seg_elet, 
              R_ADMISSIONS_BY_DAY(i).MON_EMERG, --nr_seg_emerg, 
              R_ADMISSIONS_BY_DAY(i).MON_TOTAL, --nr_seg_total, 
              R_ADMISSIONS_BY_DAY(i).TUE_ELECT, --nr_ter_elet, 
              R_ADMISSIONS_BY_DAY(i).TUE_EMERG, --nr_ter_emerg, 
              R_ADMISSIONS_BY_DAY(i).TUE_TOTAL, --nr_ter_total, 
              R_ADMISSIONS_BY_DAY(i).WED_ELECT, --nr_qua_elet, 
              R_ADMISSIONS_BY_DAY(i).WED_EMERG, --nr_qua_emerg, 
              R_ADMISSIONS_BY_DAY(i).WED_TOTAL, --nr_qua_total, 
              R_ADMISSIONS_BY_DAY(i).THU_ELECT, --nr_qui_elet, 
              R_ADMISSIONS_BY_DAY(i).THU_EMERG, --nr_qui_emerg, 
              R_ADMISSIONS_BY_DAY(i).THU_TOTAL, --nr_qui_total, 
              R_ADMISSIONS_BY_DAY(i).FRI_ELECT, --nr_sex_elet, 
              R_ADMISSIONS_BY_DAY(i).FRI_EMERG, --nr_sex_emerg, 
              R_ADMISSIONS_BY_DAY(i).FRI_TOTAL, --nr_sex_total, 
              R_ADMISSIONS_BY_DAY(i).SAT_ELECT, --nr_sab_elet, 
              R_ADMISSIONS_BY_DAY(i).SAT_EMERG, --nr_sab_emerg, 
              R_ADMISSIONS_BY_DAY(i).SAT_TOTAL, --nr_sab_total, 
              R_ADMISSIONS_BY_DAY(i).SUN_ELECT, --nr_dom_elet, 
              R_ADMISSIONS_BY_DAY(i).SUN_EMERG, --nr_dom_emerg, 
              R_ADMISSIONS_BY_DAY(i).SUN_TOTAL); --nr_dom_total)
          END LOOP;         
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
        COMMIT;
            
      EXIT WHEN C_ADMISSIONS_BY_DAY%NOTFOUND; 
    END LOOP;
    CLOSE C_ADMISSIONS_BY_DAY;
    
    OPEN C_ADMISSIONS_BY_RANGE;
    LOOP
      FETCH C_ADMISSIONS_BY_RANGE BULK COLLECT INTO R_ADMISSIONS_BY_RANGE LIMIT 1000;
        
        BEGIN
          FOR i IN R_ADMISSIONS_BY_RANGE.FIRST..R_ADMISSIONS_BY_RANGE.LAST LOOP
            INSERT INTO W_ADMISSIONS_REP_BY_RANGE (
              cd_setor_atendimento, 
              nr_dia, 
              ds_dia, 
              nr_total_elet, 
              nr_total_emerg, 
              nr_total_admis, 
              nr_range_1_elet, 
              nr_range_1_emerg, 
              nr_range_1_total, 
              nr_range_2_elet, 
              nr_range_2_emerg, 
              nr_range_2_total, 
              nr_range_3_elet, 
              nr_range_3_emerg, 
              nr_range_3_total, 
              nr_range_4_elet, 
              nr_range_4_emerg, 
              nr_range_4_total, 
              nr_range_5_elet, 
              nr_range_5_emerg, 
              nr_range_5_total, 
              nr_range_6_elet, 
              nr_range_6_emerg, 
              nr_range_6_total)
            VALUES (
              R_ADMISSIONS_BY_RANGE(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento,
              R_ADMISSIONS_BY_RANGE(i).INDEX_OF_DAY, --nr_dia,
              R_ADMISSIONS_BY_RANGE(i).DAY_OF_WEEK, --ds_dia, 
              R_ADMISSIONS_BY_RANGE(i).TOTAL_ELECT, --nr_total_elet,
              R_ADMISSIONS_BY_RANGE(i).TOTAL_EMERG, --nr_total_emerg,
              R_ADMISSIONS_BY_RANGE(i).TOTAL_ADMIS, --nr_total_admis,
              R_ADMISSIONS_BY_RANGE(i).RANGE_1_ELECT, --nr_range_1_elet, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_1_EMERG, --nr_range_1_emerg, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_1_TOTAL, --nr_range_1_total, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_2_ELECT, --nr_range_2_elet, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_2_EMERG, --nr_range_2_emerg, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_2_TOTAL, --nr_range_2_total, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_3_ELECT, --nr_range_3_elet, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_3_EMERG, --nr_range_3_emerg, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_3_TOTAL, --nr_range_3_total, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_4_ELECT, --nr_range_4_elet, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_4_EMERG, --nr_range_4_emerg, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_4_TOTAL, --nr_range_4_total, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_5_ELECT, --nr_range_5_elet, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_5_EMERG, --nr_range_5_emerg, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_5_TOTAL, --nr_range_5_total, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_6_ELECT, --nr_range_6_elet, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_6_EMERG, --nr_range_6_emerg, 
              R_ADMISSIONS_BY_RANGE(i).RANGE_6_TOTAL); --nr_range_6_total)
          END LOOP;         
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
        COMMIT;
            
      EXIT WHEN C_ADMISSIONS_BY_RANGE%NOTFOUND; 
    END LOOP;
    CLOSE C_ADMISSIONS_BY_RANGE;
        
  END PRE_PROCESS;
  -- 
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0) IS
  BEGIN
    TABLE_CLEANING;
    PRE_PROCESS(P_START_DATE, LEAST(P_END_DATE, TRUNC(SYSDATE, 'DD')), P_CD_ESTAB, P_CD_SETOR);
  END RUN;
END PKG_ADMISSIONS_REPORT;
/
