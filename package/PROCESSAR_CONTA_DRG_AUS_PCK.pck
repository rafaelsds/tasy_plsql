create or replace package processar_conta_drg_aus_pck as
	
	procedure 	set_qt_procedimento(qt_procedimento_p number);
	function 	get_qt_procedimento return number;
  
	procedure 	set_vl_procedimento(vl_procedimento_p number);
	function 	get_vl_procedimento return number;
	
	procedure 	set_vl_custo_operacional(vl_custo_operacional_p number);
	function 	get_vl_custo_operacional return number;
	
	procedure 	set_vl_trnfr_dscnt(vl_trnfr_dscnt_p number);
	function 	get_vl_trnfr_dscnt return number;

	procedure 	set_vl_sso_benefit(vl_sso_benefit_p number);
	function 	get_vl_sso_benefit return number;

	procedure 	set_vl_short_diem_weight(vl_short_diem_weight_p number);
	function 	get_vl_short_diem_weight return number;

	procedure 	set_vl_same_day_weight(vl_same_day_weight_p number);
	function 	get_vl_same_day_weight return number;

	procedure 	set_vl_same_day_rate(vl_same_day_rate_p number);
	function 	get_vl_same_day_rate return number;

	procedure 	set_vl_rate_lt_6_hrs(vl_rate_lt_6_hrs_p number);
	function 	get_vl_rate_lt_6_hrs return number;

	procedure 	set_vl_rate_gt_6_hrs(vl_rate_gt_6_hrs_p number);
	function 	get_vl_rate_gt_6_hrs return number;

	procedure 	set_vl_proc_only_fee(vl_proc_only_fee_p number);
	function 	get_vl_proc_only_fee return number;

	procedure 	set_vl_overnight_flagfall_mv(vl_overnight_flagfall_mv_p number);
	function 	get_vl_overnight_flagfall_mv return number;
	
	procedure 	set_vl_overnight_flagfall(vl_overnight_flagfall_p number);
	function 	get_vl_overnight_flagfall return number;

	procedure 	set_vl_lso_benefit(vl_lso_benefit_p number);
	function 	get_vl_lso_benefit return number;

	procedure 	set_vl_long_diem_weight(vl_long_diem_weight_p number);
	function 	get_vl_long_diem_weight return number;

	procedure 	set_vl_inlier_weight(vl_inlier_weight_p number);
	function 	get_vl_inlier_weight return number;

	procedure 	set_vl_inlier_rate_tier3(vl_inlier_rate_tier3_p number);
	function 	get_vl_inlier_rate_tier3 return number;

	procedure 	set_vl_inlier_rate_tier2(vl_inlier_rate_tier2_p number);
	function 	get_vl_inlier_rate_tier2 return number;

	procedure 	set_vl_inlier_rate(vl_inlier_rate_p number);
	function 	get_vl_inlier_rate return number;

	procedure 	set_vl_icu_sso(vl_icu_sso_p number);
	function 	get_vl_icu_sso return number;
	
	procedure 	set_qt_dias_icu_a(qt_dias_icu_a_p number);
	function 	get_qt_dias_icu_a return number;

	procedure 	set_qt_dias_icu_b(qt_dias_icu_b_p number);
	function 	get_qt_dias_icu_b return number;

	procedure 	set_qt_dias_icu_c(qt_dias_icu_c_p number);
	function 	get_qt_dias_icu_c return number;

	procedure 	set_qt_dias_not_icu(qt_dias_not_icu_p number);
	function 	get_qt_dias_not_icu return number;

	procedure 	set_qt_horas_int(qt_horas_int_p number);
	function 	get_qt_horas_int return number;

	procedure 	set_qt_total_days(qt_total_days_p number);
	function 	get_qt_total_days return number;

	procedure 	set_qt_days_in_icu(qt_days_in_icu_p number);
	function 	get_qt_days_in_icu return number;

	procedure 	set_qt_days_not_in_icu_cat_a(qt_days_not_in_icu_cat_a_p number);
	function 	get_qt_days_not_in_icu_cat_a return number;

	procedure 	set_is_tier3_range_applied(is_tier3_range_applied_p varchar2);
	function 	get_is_tier3_range_applied return varchar2;

	procedure 	set_qt_days_exceeds_tier3(qt_days_exceeds_tier3_p number);
	function 	get_qt_days_exceeds_tier3 return number;

	procedure 	set_is_tier2_range_applied(is_tier2_range_applied_p varchar2);
	function 	get_is_tier2_range_applied return varchar2;

	procedure 	set_qt_days_exceeds_tier2(qt_days_exceeds_tier2_p number);
	function 	get_qt_days_exceeds_tier2 return number;

	procedure 	set_is_tier1_range_applied(is_tier1_range_applied_p varchar2);
	function 	get_is_tier1_range_applied return varchar2;

	procedure 	set_qt_days_exceeds_tier1(qt_days_exceeds_tier1_p number);
	function 	get_qt_days_exceeds_tier1 return number;

	procedure 	set_is_only_flagfall_sso_app(is_only_flagfall_sso_app_p varchar2);
	function 	get_is_only_flagfall_sso_app return varchar2;

	procedure 	set_is_all_days_in_icu(is_all_days_in_icu_p varchar2);
	function 	get_is_all_days_in_icu return varchar2;

	procedure 	set_is_none_days_in_icu(is_none_days_in_icu_p varchar2);
	function 	get_is_none_days_in_icu return varchar2;

	procedure 	set_is_some_days_in_icu(is_some_days_in_icu_p varchar2);
	function 	get_is_some_days_in_icu return varchar2;

	procedure 	set_is_transfer_cutoff_reached(is_transfer_cutoff_reached_p varchar2);
	function 	get_is_transfer_cutoff_reached return varchar2;

	procedure 	set_qt_days_in_tier1(qt_days_in_tier1_p number);
	function 	get_qt_days_in_tier1 return number;

	procedure 	set_qt_days_in_tier2(qt_days_in_tier2_p number);
	function 	get_qt_days_in_tier2 return number;
	
	procedure 	set_qt_days_for_tran_disc(qt_days_for_tran_disc_p number);
	function 	get_qt_days_for_tran_disc return number;

	procedure 	set_qt_days_for_priv_share(qt_days_for_priv_share_p number);
	function 	get_qt_days_for_priv_share return number;																	
end processar_conta_drg_aus_pck;
/

create or replace package body processar_conta_drg_aus_pck as
	
 qt_procedimento_w			 	procedimento_paciente.qt_procedimento%type;
 vl_procedimento_w			 	procedimento_paciente.vl_procedimento%type;
 vl_custo_operacional_w		 	procedimento_paciente.vl_custo_operacional%type;
 vl_trnfr_dscnt_w			 	drg_procedimento.vl_trnfr_dscnt%type;
 vl_sso_benefit_w			 	drg_procedimento.vl_sso_benefit%type;
 vl_short_diem_weight_w		 	drg_procedimento.vl_short_diem_weight%type;
 vl_same_day_weight_w		 	drg_procedimento.vl_same_day_weight%type;
 vl_same_day_rate_w			 	drg_procedimento.vl_same_day_rate%type;
 vl_rate_lt_6_hrs_w			 	drg_procedimento.vl_rate_lt_6_hrs%type;
 vl_rate_gt_6_hrs_w			 	drg_procedimento.vl_rate_gt_6_hrs%type;
 vl_proc_only_fee_w			 	drg_procedimento.vl_proc_only_fee%type;
 vl_overnight_flagfall_mv_w	 	drg_procedimento.vl_overnight_flagfall_mv%type;
 vl_overnight_flagfall_w	 	drg_procedimento.vl_overnight_flagfall%type;
 vl_lso_benefit_w			 	drg_procedimento.vl_lso_benefit%type;
 vl_long_diem_weight_w		 	drg_procedimento.vl_long_diem_weight%type;
 vl_inlier_weight_w			 	drg_procedimento.vl_inlier_weight%type;
 vl_inlier_rate_tier3_w		 	drg_procedimento.vl_inlier_rate_tier3%type;
 vl_inlier_rate_tier2_w		 	drg_procedimento.vl_inlier_rate_tier2%type;
 vl_inlier_rate_w			 	drg_procedimento.vl_inlier_rate%type;
 vl_icu_sso_w				 	drg_procedimento.vl_icu_sso%type;
 qt_dias_icu_a_w			 	number(10,0);
 qt_dias_icu_b_w			 	number(10,0);
 qt_dias_icu_c_w			 	number(10,0);
 qt_dias_not_icu_w			 	number(10,0);
 qt_estedia_min_tier2_w			drg_procedimento.qt_estedia_min_tier2%type;
 qt_estedia_max_tier2_w 		drg_procedimento.qt_estedia_max_tier2%type;
 qt_estedia_min_tier3_w  		drg_procedimento.qt_estedia_min_tier3%type;
 qt_estedia_max_tier3_w  		drg_procedimento.qt_estedia_max_tier3%type;
 qt_horas_int_w 				number(10,2);
 qt_total_days_w 				number(10,0);
 qt_days_in_icu_w 				number(10,0);
 qt_days_in_icu_cat_a_w 		number(10,0);
 qt_days_in_icu_cat_b_w 		number(10,0);
 qt_days_in_icu_cat_c_w 		number(10,0);
 qt_days_not_in_icu_cat_a_w 	number(10,0);
 qt_days_not_in_icu_w 			number(10,0);
 qt_days_for_tran_disc_w 		number(10,0);
 qt_days_for_priv_share_w		number(10,0);									
 is_tier3_range_applied_w 		varchar2(1):='N';
 qt_days_exceeds_tier3_w 		number(10,0);
 is_tier2_range_applied_w 		varchar2(1):='N';
 qt_days_exceeds_tier2_w 		number(10,0);
 is_tier1_range_applied_w 		varchar2(1):='N';
 qt_days_exceeds_tier1_w 		number(10,0);
 qt_days_in_tier1_w 			number(10,0);
 qt_days_in_tier2_w 			number(10,0);
 is_only_flagfall_sso_app_w 	varchar2(1):='N';
 is_all_days_in_icu_w 			varchar2(1):='N';
 is_none_days_in_icu_w 			varchar2(1):='N';
 is_some_days_in_icu_w 			varchar2(1):='N';
 is_transfer_cutoff_reached_w 	varchar2(1):='N';
	
  	procedure 	set_qt_procedimento(qt_procedimento_p number) as
	begin
		qt_procedimento_w := qt_procedimento_p;
	end;

	function 	get_qt_procedimento return number as
	begin
		return qt_procedimento_w;
	end;
  
	procedure 	set_vl_procedimento(vl_procedimento_p number) as
	begin
		vl_procedimento_w := vl_procedimento_p;
	end;

	function 	get_vl_procedimento return number as
	begin
		return vl_procedimento_w;
	end;
	
	procedure 	set_vl_custo_operacional(vl_custo_operacional_p number) as
	begin
		vl_custo_operacional_w := vl_custo_operacional_p;
	end;

	function 	get_vl_custo_operacional return number as
	begin
		return vl_custo_operacional_w;
	end;

	procedure 	set_vl_trnfr_dscnt(vl_trnfr_dscnt_p number) as
	begin
		vl_trnfr_dscnt_w := vl_trnfr_dscnt_p;
	end;

	function 	get_vl_trnfr_dscnt return number as
	begin
		return vl_trnfr_dscnt_w;
	end;

	procedure 	set_vl_sso_benefit(vl_sso_benefit_p number) as
	begin
		vl_sso_benefit_w := vl_sso_benefit_p;
	end;

	function 	get_vl_sso_benefit return number as
	begin
		return vl_sso_benefit_w;
	end;

	procedure 	set_vl_short_diem_weight(vl_short_diem_weight_p number) as
	begin
		vl_short_diem_weight_w := vl_short_diem_weight_p;
	end;

	function 	get_vl_short_diem_weight return number as
	begin
		return vl_short_diem_weight_w;
	end;
	
	procedure 	set_vl_same_day_weight(vl_same_day_weight_p number) as
	begin
		vl_same_day_weight_w := vl_same_day_weight_p;
	end;

	function 	get_vl_same_day_weight return number as
	begin
		return vl_same_day_weight_w;
	end;
	
	procedure 	set_vl_same_day_rate(vl_same_day_rate_p number) as
	begin
		vl_same_day_rate_w := vl_same_day_rate_p;
	end;

	function 	get_vl_same_day_rate return number as
	begin
		return vl_same_day_rate_w;
	end;
	
	procedure 	set_vl_rate_lt_6_hrs(vl_rate_lt_6_hrs_p number) as
	begin
		vl_rate_lt_6_hrs_w := vl_rate_lt_6_hrs_p;
	end;

	function 	get_vl_rate_lt_6_hrs return number as
	begin
		return vl_rate_lt_6_hrs_w;
	end;
	
	procedure 	set_vl_rate_gt_6_hrs(vl_rate_gt_6_hrs_p number) as
	begin
		vl_rate_gt_6_hrs_w := vl_rate_gt_6_hrs_p;
	end;

	function 	get_vl_rate_gt_6_hrs return number as
	begin
		return vl_rate_gt_6_hrs_w;
	end;
	
	procedure set_vl_proc_only_fee(vl_proc_only_fee_p number) as 
	begin 
		vl_proc_only_fee_w := vl_proc_only_fee_p; 
	end;
	
	function get_vl_proc_only_fee return number as 
	begin  
		return vl_proc_only_fee_w; 
	end;
	
	procedure set_vl_overnight_flagfall_mv(vl_overnight_flagfall_mv_p number) as 
	begin 
		vl_overnight_flagfall_mv_w := vl_overnight_flagfall_mv_p; 
	end;
	
	function get_vl_overnight_flagfall_mv return number as 
	begin  
		return vl_overnight_flagfall_mv_w; 
	end;
	
	procedure set_vl_overnight_flagfall(vl_overnight_flagfall_p number) as 
	begin 
		vl_overnight_flagfall_w := vl_overnight_flagfall_p; 
	end;
	
	function get_vl_overnight_flagfall return number as 
	begin  
		return vl_overnight_flagfall_w; 
	end;
	
	procedure set_vl_lso_benefit(vl_lso_benefit_p number) as 
	begin 
		vl_lso_benefit_w := vl_lso_benefit_p; 
	end;
	
	function get_vl_lso_benefit return number as 
	begin  
		return vl_lso_benefit_w; 
	end;

	procedure set_vl_long_diem_weight(vl_long_diem_weight_p number) as 
	begin 
		vl_long_diem_weight_w := vl_long_diem_weight_p; 
	end;

	function get_vl_long_diem_weight return number as 
	begin  
		return vl_long_diem_weight_w; 
	end;
	
	procedure set_vl_inlier_weight(vl_inlier_weight_p number) as 
	begin 
		vl_inlier_weight_w := vl_inlier_weight_p; 
	end;

	function get_vl_inlier_weight return number as 
	begin  
		return vl_inlier_weight_w; 
	end;

	procedure set_vl_inlier_rate_tier3(vl_inlier_rate_tier3_p number) as 
	begin 
		vl_inlier_rate_tier3_w := vl_inlier_rate_tier3_p; 
	end;

	function get_vl_inlier_rate_tier3 return number as 
	begin  
		return vl_inlier_rate_tier3_w; 
	end;

	procedure set_vl_inlier_rate_tier2(vl_inlier_rate_tier2_p number) as 
	begin 
		vl_inlier_rate_tier2_w := vl_inlier_rate_tier2_p; 
	end;

	function get_vl_inlier_rate_tier2 return number as 
	begin  
		return vl_inlier_rate_tier2_w; 
	end;

	procedure set_vl_inlier_rate(vl_inlier_rate_p number) as 
	begin 
		vl_inlier_rate_w := vl_inlier_rate_p; 
	end;
	
	function get_vl_inlier_rate return number as 
	begin  
		return vl_inlier_rate_w; 
	end;
	
	procedure set_vl_icu_sso(vl_icu_sso_p number) as 
	begin 
		vl_icu_sso_w := vl_icu_sso_p; 
	end;
	
	function get_vl_icu_sso return number as 
	begin  
		return vl_icu_sso_w; 
	end;
	
	procedure set_qt_dias_icu_a(qt_dias_icu_a_p number) as 
	begin 
		qt_dias_icu_a_w := qt_dias_icu_a_p; 
	end;
	
	function get_qt_dias_icu_a return number as 
	begin  
		return qt_dias_icu_a_w; 
	end;
	
	procedure set_qt_dias_icu_b(qt_dias_icu_b_p number) as 
	begin 
		qt_dias_icu_b_w := qt_dias_icu_b_p; 
	end;
	
	function get_qt_dias_icu_b return number as 
	begin  
		return qt_dias_icu_b_w; 
	end;
	
	procedure set_qt_dias_icu_c(qt_dias_icu_c_p number) as 
	begin 
		qt_dias_icu_c_w := qt_dias_icu_c_p; 
	end;
	
	function get_qt_dias_icu_c return number as 
	begin  
		return qt_dias_icu_c_w; 
	end;
	
	procedure set_qt_dias_not_icu(qt_dias_not_icu_p number) as 
	begin 
		qt_dias_not_icu_w := qt_dias_not_icu_p; 
	end;
	
	function get_qt_dias_not_icu return number as 
	begin  
		return qt_dias_not_icu_w; 
	end;


	procedure set_qt_horas_int(qt_horas_int_p number) as 
	begin 
		qt_horas_int_w := qt_horas_int_p; 
	end;
	
	function get_qt_horas_int return number as 
	begin  
		return qt_horas_int_w; 
	end;
  
  	procedure set_qt_total_days(qt_total_days_p number) as 
	begin 
		qt_total_days_w := qt_total_days_p; 
	end;
	
	function get_qt_total_days return number as 
	begin  
		return qt_total_days_w; 
	end;
  
  	procedure set_qt_days_in_icu(qt_days_in_icu_p number) as 
	begin 
		qt_days_in_icu_w := qt_days_in_icu_p; 
	end;
	
	function get_qt_days_in_icu return number as 
	begin  
		return qt_days_in_icu_w; 
	end;
  
  	procedure set_qt_days_not_in_icu_cat_a(qt_days_not_in_icu_cat_a_p number) as 
	begin 
		qt_days_not_in_icu_cat_a_w := qt_days_not_in_icu_cat_a_p; 
	end;
	
	function get_qt_days_not_in_icu_cat_a return number as 
	begin  
		return qt_days_not_in_icu_cat_a_w; 
	end;

	procedure set_is_tier3_range_applied(is_tier3_range_applied_p varchar2) as 
	begin 
		is_tier3_range_applied_w := is_tier3_range_applied_p; 
	end;
	
	function get_is_tier3_range_applied return varchar2 as 
	begin  
		return is_tier3_range_applied_w; 
	end;

 	procedure set_qt_days_exceeds_tier3(qt_days_exceeds_tier3_p number) as 
	begin 
		qt_days_exceeds_tier3_w := qt_days_exceeds_tier3_p; 
	end;
	
	function get_qt_days_exceeds_tier3 return number as 
	begin  
		return qt_days_exceeds_tier3_w; 
	end;

	procedure set_is_tier2_range_applied(is_tier2_range_applied_p varchar2) as 
	begin 
		is_tier2_range_applied_w := is_tier2_range_applied_p; 
	end;
	
	function get_is_tier2_range_applied return varchar2 as 
	begin  
		return is_tier2_range_applied_w; 
	end;

 	procedure set_qt_days_exceeds_tier2(qt_days_exceeds_tier2_p number) as 
	begin 
		qt_days_exceeds_tier2_w := qt_days_exceeds_tier2_p; 
	end;
	
	function get_qt_days_exceeds_tier2 return number as 
	begin  
		return qt_days_exceeds_tier2_w; 
	end;

	procedure set_is_tier1_range_applied(is_tier1_range_applied_p varchar2) as 
	begin 
		is_tier1_range_applied_w := is_tier1_range_applied_p; 
	end;
	
	function get_is_tier1_range_applied return varchar2 as 
	begin  
		return is_tier1_range_applied_w; 
	end;

 	procedure set_qt_days_exceeds_tier1(qt_days_exceeds_tier1_p number) as 
	begin 
		qt_days_exceeds_tier1_w := qt_days_exceeds_tier1_p; 
	end;
	
	function get_qt_days_exceeds_tier1 return number as 
	begin  
		return qt_days_exceeds_tier1_w; 
	end;

	procedure set_is_only_flagfall_sso_app(is_only_flagfall_sso_app_p varchar2) as 
	begin 
		is_only_flagfall_sso_app_w := is_only_flagfall_sso_app_p; 
	end;
	
	function get_is_only_flagfall_sso_app return varchar2 as 
	begin  
		return is_only_flagfall_sso_app_w; 
	end;

	procedure set_is_all_days_in_icu(is_all_days_in_icu_p varchar2) as 
	begin 
		is_all_days_in_icu_w := is_all_days_in_icu_p; 
	end;
	
	function get_is_all_days_in_icu return varchar2 as 
	begin  
		return is_all_days_in_icu_w; 
	end;

	procedure set_is_none_days_in_icu(is_none_days_in_icu_p varchar2) as 
	begin 
		is_none_days_in_icu_w := is_none_days_in_icu_p; 
	end;
	
	function get_is_none_days_in_icu return varchar2 as 
	begin  
		return is_none_days_in_icu_w; 
	end;

	procedure set_is_some_days_in_icu(is_some_days_in_icu_p varchar2) as 
	begin 
		is_some_days_in_icu_w := is_some_days_in_icu_p; 
	end;
	
	function get_is_some_days_in_icu return varchar2 as 
	begin  
		return is_some_days_in_icu_w; 
	end;

	procedure set_is_transfer_cutoff_reached(is_transfer_cutoff_reached_p varchar2) as 
	begin 
		is_transfer_cutoff_reached_w := is_transfer_cutoff_reached_p; 
	end;
	
	function get_is_transfer_cutoff_reached return varchar2 as 
	begin  
		return is_transfer_cutoff_reached_w; 
	end;

	procedure set_qt_days_in_tier1(qt_days_in_tier1_p number) as 
	begin 
		qt_days_in_tier1_w := qt_days_in_tier1_p; 
	end;

	function get_qt_days_in_tier1 return number as 
	begin  
		return qt_days_in_tier1_w; 
	end;

	procedure set_qt_days_in_tier2(qt_days_in_tier2_p number) as 
	begin 
		qt_days_in_tier2_w := qt_days_in_tier2_p; 
	end;

	function get_qt_days_in_tier2 return number as 
	begin  
		return qt_days_in_tier2_w; 
	end;
	
	procedure set_qt_days_for_tran_disc(qt_days_for_tran_disc_p number) as 
	begin 
		qt_days_for_tran_disc_w := qt_days_for_tran_disc_p; 
	end;

	function get_qt_days_for_tran_disc return number as 
	begin  
		return qt_days_for_tran_disc_w; 
	end;

	procedure set_qt_days_for_priv_share(qt_days_for_priv_share_p number) as 
	begin 
		qt_days_for_priv_share_w := qt_days_for_priv_share_p; 
	end;

	function get_qt_days_for_priv_share return number as 
	begin  
		return qt_days_for_priv_share_w; 
	end;
	
end processar_conta_drg_aus_pck;
/