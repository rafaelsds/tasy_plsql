create or replace 
package cpoe_hemo_order_json_pck  as
   	
	function get_hemo_message(nr_cpoe_hemo_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json;
	
	function get_message_clob(nr_cpoe_hemo_p		number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob;
	
	procedure getCpoeIntegracaoHemo(nr_seq_hemo_cpoe_p number, nr_entity_identifier_p out number);

end cpoe_hemo_order_json_pck;
/

create or replace 
package body cpoe_hemo_order_json_pck as
		
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			varchar2) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;
	
	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			number) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,value_p);
	end if;
	end add_json_value;

	
	procedure	add_json_value(	json_p	in out nocopy philips_json,
					name_p			varchar2,
					value_p			date) is
	begin
	if	(value_p is not null) then
		json_p.put(name_p,pkg_date_utils.get_isoformat(value_p));
	end if;	
	end add_json_value;
	
	function get_default_message(	nr_cpoe_hemo_p	number ) return philips_json is
	
	json_encounter_w		philips_json;
	json_patient_w			philips_json;
	cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
	json_return_w			philips_json;
	json_allergy_w   		philips_json_list;
	
	nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
	
	begin
	
	json_return_w	:= philips_json();
	
	select max(nr_atendimento)
	into nr_atendimento_w
	from cpoe_hemoterapia
	where nr_sequencia = nr_cpoe_hemo_p;
	
	SELECT MAX(CD_PESSOA_FISICA)
	INTO CD_PESSOA_FISICA_W
	FROM ATENDIMENTO_PACIENTE
	WHERE NR_ATENDIMENTO = nr_atendimento_w;
	
	json_encounter_w		:= encounter_json_pck.get_encounter(nr_atendimento_w);
	json_patient_w			:= person_json_pck.get_person(cd_pessoa_fisica_w);
	json_allergy_w			:= person_json_pck.get_allergy(cd_pessoa_fisica_w);
	
	json_return_w.put('patientIdentification',json_patient_w.to_json_value);
	json_return_w.put('patientVisit',json_encounter_w.to_json_value);
	json_return_w.put('patientAllergy',json_allergy_w.to_json_value);
	
	return json_return_w;
	
	end get_default_message;	
	
  function escapeHL7(hl7_message_p varchar2) return varchar2 is

  escape_char_w                 varchar2(1) := '\';
  field_separator_char_w        varchar2(1) := '|';
  repetition_separator_char_w   varchar2(1) := '~';
  component_separator_char_w    varchar2(1) := '^';
  line_feed_w                   varchar2(1) := chr(10);
  carriage_return_w             varchar2(1) := chr(13);

  ds_escaped_msg_w  varchar2(4000);

  begin

  ds_escaped_msg_w := replace(hl7_message_p, escape_char_w, '\E\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, field_separator_char_w, '\F\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, repetition_separator_char_w, '\R\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, component_separator_char_w, '\S\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, line_feed_w, '\.br\');
  ds_escaped_msg_w := replace(ds_escaped_msg_w, carriage_return_w, '\.br\');

  return ds_escaped_msg_w;

  end escapeHL7;

	function get_hemo_data(nr_cpoe_hemo_p		number) return philips_json_list is
	json_item_w		philips_json;
	json_item_list_w	philips_json_list;
	
	Cursor C01 is
		select 
       NR_SEQUENCIA,
       IE_TIPO_HEMOTERAP, 
       CD_INTERVALO,
       IE_TIPO,
       adep_obter_desc_info_material(nr_sequencia, 'HM' )  desc_info_item,
       cpoe_obter_dt_suspensao(nr_sequencia,'N') dt_suspensao,
       obter_conv_envio('CID_DOENCA', 'CD_DOENCA_CID', CD_DOENCA_CID, 'E') code_red_value,
       obter_conv_envio('CID_DOENCA', 'CD_DOENCA_CID', CD_DOENCA_CID, 'S') code_red_system, 
       OBTER_DESC_CID(CD_DOENCA_CID) DESC_CID,
       Obter_Desc_Hemocomponente(NR_SEQ_DERIVADO) desc_Hemocomponente, 
       obter_conv_envio('SAN_DERIVADO', 'NR_SEQUENCIA', NR_SEQ_DERIVADO, 'E')    hem_code_value,
       obter_conv_envio('SAN_DERIVADO', 'NR_SEQUENCIA', NR_SEQ_DERIVADO, 'S')    hem_code_system, 
       obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', IE_VIA_APLICACAO, 'S')  via_code_system,
       obter_conv_envio('VIA_APLICACAO', 'IE_VIA_APLICACAO', IE_VIA_APLICACAO, 'E')  via_code_vale, 
       Obter_Desc_via(IE_VIA_APLICACAO)   desc_via,
       obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'ml', 'S')  unidade_medida_code_system,
       obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'ml', 'E')  unidade_medida_code_value, 
       obter_desc_unidade_medida('ml')  unidade_medida_text,
       obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'S')  minute_code_system,
       obter_conv_envio('UNIDADE_MEDIDA', 'CD_UNIDADE_MEDIDA', 'Min', 'E')  minute_code_value,
       obter_minutos_hora(QT_HORA_MIN_INFUSAO)  MIN_QT_HORA_MIN_INFUSAO,
       obter_minutos_hora(QT_TEMPO_INF_ETAPA)   MIN_QT_TEMPO_INF_ETAPA,
       Obter_Desc_Proc_Interno(NR_SEQ_PROC_INTERNO) desc_procedimento, 
       obter_conv_envio('PROC_INTERNO', 'NR_SEQUENCIA', NR_SEQ_PROC_INTERNO, 'E')    proc_code_value,
       obter_conv_envio('PROC_INTERNO', 'NR_SEQUENCIA', NR_SEQ_PROC_INTERNO, 'S')    proc_code_system, 
       obter_conv_envio('INTERVALO_PRESCRICAO', 'CD_INTERVALO', CD_INTERVALO, 'E')  interval_code_vale, 
       QT_PROCEDIMENTO, 
       QT_VOL_HEMOCOMP, 
       QT_VOLUME_RETIRAR,
       QT_TEMPO_INF_ETAPA, 
       QT_HORA_MIN_INFUSAO, 
       /* list */
       IE_ALIQUOTADO, 
       IE_FILTRADO, 
       IE_IRRADIADO, 
       IE_LAVADO, 
       NR_SEQ_INDICACAO1, 
       QT_HEMOGLOBINA, 
       QT_HEMATOCRITO, 
       QT_PLAQUETA, 
       QT_TAP, 
       QT_TAP_INR,
       QT_TTPA, 
       QT_TTPA_REL, 
       QT_FIBRINOGENIO, 
       QT_ALBUMINA, 
       QT_CALCIO, 
       QT_MAGNESIO, 
       QT_BILIRRUBINA_DIR, 
       QT_BILIRRUBINA_IND,
       QT_HEMOGLOBINA_S, 
       QT_LEUCOCITOS, 
       Obter_Descricao_Dominio(4506,IE_COOMBS_DIRETO) DS_IE_COOMBS_DIRETO,
       DS_COAGULOPATIA, 
       decode(DS_OBSERVACAO,null,'','. ' ||elimina_acentuacao(DS_OBSERVACAO)) DS_OBSERVACAO,
       decode(DS_JUSTIFICATIVA,null,'','. ' ||elimina_acentuacao(DS_JUSTIFICATIVA)) DS_JUSTIFICATIVA, 
       /* end list */
       DT_PROGRAMADA, 
       DT_FIM, 
       DT_LIBERACAO           requested_date_time, 
       DT_LIB_SUSPENSAO  suspended_time, 
       cd_medico ordering_provider_id_number,
       obter_dados_pf(cd_medico,'PNG') ordering_provider_given_name,
       obter_dados_pf(cd_medico,'PNL') ordering_provider_last_name,
       obter_dados_pf(cd_medico,'PNM') ordering_provider_middle_name, 
       Obter_Pessoa_Fisica_Usuario(nm_usuario_susp,'C') ordering_prov_susp_id_number,
       substr(obter_nome_pf(cd_medico),1,80) nm_medico_solicitante,
       Obter_Pessoa_Fisica_Usuario(nm_usuario_nrec,'C') ordering_user_id_number,
       Obter_Pessoa_Fisica_Usuario(nm_usuario_susp,'C') order_prov_susp_id_number
from CPOE_HEMOTERAPIA
where NR_SEQUENCIA = nr_cpoe_hemo_p;

		
	begin
	json_item_list_w	:= philips_json_list();
	
	for r_c01 in c01 loop
		begin
		json_item_w		:= philips_json();

    add_json_value(json_item_w, 'nrSequencia', r_c01.NR_SEQUENCIA);
    add_json_value(json_item_w, 'ieTipoHemoterap', r_c01.IE_TIPO_HEMOTERAP);
    add_json_value(json_item_w, 'cdIntervalo', r_c01.CD_INTERVALO);
    add_json_value(json_item_w, 'nrSequencia', r_c01.NR_SEQUENCIA);
    add_json_value(json_item_w, 'dtSuspensao', r_c01.DT_SUSPENSAO);
    add_json_value(json_item_w, 'codeRedValue', r_c01.CODE_RED_VALUE);
    add_json_value(json_item_w, 'codeRedSystem', r_c01.CODE_RED_SYSTEM);
    add_json_value(json_item_w, 'descCid', r_c01.DESC_CID);
    add_json_value(json_item_w, 'descHemocomponente', r_c01.DESC_HEMOCOMPONENTE);
    add_json_value(json_item_w, 'hemCodeValue', r_c01.HEM_CODE_VALUE);
    add_json_value(json_item_w, 'hemCodeSystem', r_c01.HEM_CODE_SYSTEM);
    add_json_value(json_item_w, 'viaCodeSystem', r_c01.VIA_CODE_SYSTEM);
    add_json_value(json_item_w, 'viaCodeVale', r_c01.VIA_CODE_VALE);
    add_json_value(json_item_w, 'descVia', r_c01.DESC_VIA);
    add_json_value(json_item_w, 'unidadeMedidaCodeSystem', r_c01.UNIDADE_MEDIDA_CODE_SYSTEM);
    add_json_value(json_item_w, 'unidadeMedidaCodeValue', r_c01.UNIDADE_MEDIDA_CODE_VALUE);
    add_json_value(json_item_w, 'unidadeMedidaText', r_c01.UNIDADE_MEDIDA_TEXT);
    add_json_value(json_item_w, 'minuteCodeSystem', r_c01.MINUTE_CODE_SYSTEM);
    add_json_value(json_item_w, 'minuteCodeValue', r_c01.MINUTE_CODE_VALUE);
    add_json_value(json_item_w, 'minQtHoraMinInfusao', r_c01.MIN_QT_HORA_MIN_INFUSAO);
    add_json_value(json_item_w, 'minQtTempoInfEtapa', r_c01.MIN_QT_TEMPO_INF_ETAPA);
    add_json_value(json_item_w, 'descProcedimento', r_c01.DESC_PROCEDIMENTO);
    add_json_value(json_item_w, 'procCodeValue', r_c01.PROC_CODE_VALUE);
    add_json_value(json_item_w, 'procCodeSystem', r_c01.PROC_CODE_SYSTEM);
    add_json_value(json_item_w, 'intervalCodeVale', r_c01.INTERVAL_CODE_VALE);
    add_json_value(json_item_w, 'qtProcedimento', r_c01.QT_PROCEDIMENTO);
    add_json_value(json_item_w, 'qtVolHemocomp', r_c01.QT_VOL_HEMOCOMP);
    add_json_value(json_item_w, 'qtVolumeRetirar', r_c01.QT_VOLUME_RETIRAR);
    add_json_value(json_item_w, 'qtTempoInfEtapa', r_c01.QT_TEMPO_INF_ETAPA);
    add_json_value(json_item_w, 'qtHoraMinInfusao', r_c01.QT_HORA_MIN_INFUSAO);
    add_json_value(json_item_w, 'ieAliquotado', r_c01.IE_ALIQUOTADO);
    add_json_value(json_item_w, 'ieFiltrado', r_c01.IE_FILTRADO);
    add_json_value(json_item_w, 'ieIrradiado', r_c01.IE_IRRADIADO);
    add_json_value(json_item_w, 'ieLavado', r_c01.IE_LAVADO);
    add_json_value(json_item_w, 'nrSeqIndicacao1', r_c01.NR_SEQ_INDICACAO1);
    add_json_value(json_item_w, 'qtHemoglobina', r_c01.QT_HEMOGLOBINA);
    add_json_value(json_item_w, 'qtHematocrito', r_c01.QT_HEMATOCRITO);
    add_json_value(json_item_w, 'qtPlaqueta', r_c01.QT_PLAQUETA);
    add_json_value(json_item_w, 'qtTap', r_c01.QT_TAP);
    add_json_value(json_item_w, 'qtTapInr', r_c01.QT_TAP_INR);
    add_json_value(json_item_w, 'qtTtpa', r_c01.QT_TTPA);
    add_json_value(json_item_w, 'qtTtpaRel', r_c01.QT_TTPA_REL);
    add_json_value(json_item_w, 'qtFibrinogenio', r_c01.QT_FIBRINOGENIO);
    add_json_value(json_item_w, 'qtAlbumina', r_c01.QT_ALBUMINA);
    add_json_value(json_item_w, 'qtCalcio', r_c01.QT_CALCIO);
    add_json_value(json_item_w, 'qtMagnesio', r_c01.QT_MAGNESIO);
    add_json_value(json_item_w, 'qtBilirrubinaDir', r_c01.QT_BILIRRUBINA_DIR);
    add_json_value(json_item_w, 'qtBilirrubinaInd', r_c01.QT_BILIRRUBINA_IND);
    add_json_value(json_item_w, 'qtHemoglobinaS', r_c01.QT_HEMOGLOBINA_S);
    add_json_value(json_item_w, 'qtLeucocitos', r_c01.QT_LEUCOCITOS);
    add_json_value(json_item_w, 'dsIeCoombsDireto', r_c01.DS_IE_COOMBS_DIRETO);
    add_json_value(json_item_w, 'dsCoagulopatia', r_c01.DS_COAGULOPATIA);
    add_json_value(json_item_w, 'dsObservacao', r_c01.DS_OBSERVACAO);
    add_json_value(json_item_w, 'dsJustificativa', r_c01.DS_JUSTIFICATIVA);
    add_json_value(json_item_w, 'dtProgramada', r_c01.DT_PROGRAMADA);
    add_json_value(json_item_w, 'dtFim', r_c01.DT_FIM);
    add_json_value(json_item_w, 'requestedDateTime', r_c01.REQUESTED_DATE_TIME);
    add_json_value(json_item_w, 'suspendedTime', r_c01.SUSPENDED_TIME);
    add_json_value(json_item_w, 'orderingProviderIdNumber', r_c01.ORDERING_PROVIDER_ID_NUMBER);
    add_json_value(json_item_w, 'orderingProviderGivenName', r_c01.ORDERING_PROVIDER_GIVEN_NAME);
    add_json_value(json_item_w, 'orderingProviderLastName', r_c01.ORDERING_PROVIDER_LAST_NAME);
    add_json_value(json_item_w, 'orderingProviderMiddleName', r_c01.ORDERING_PROVIDER_MIDDLE_NAME);
    add_json_value(json_item_w, 'orderingProvSuspIdNumber', r_c01.ORDERING_PROV_SUSP_ID_NUMBER);
    add_json_value(json_item_w, 'nmMedicoSolicitante', r_c01.NM_MEDICO_SOLICITANTE);
    add_json_value(json_item_w, 'orderingUserIdNumber', r_c01.ORDERING_USER_ID_NUMBER);
    add_json_value(json_item_w, 'orderProvSuspIdNumber', r_c01.ORDER_PROV_SUSP_ID_NUMBER);
    add_json_value(json_item_w, 'descInfoItem', escapeHL7(r_c01.desc_info_item));

		if (r_c01.dt_fim is not null) then
				add_json_value(json_item_w, 'numberOfDays', OBTER_DIAS_ENTRE_DATAS(r_c01.DT_PROGRAMADA, r_c01.dt_fim));
		end if;
		
		if (r_c01.ie_tipo in ('4', '3')) then
      add_json_value(json_item_w, 'priority', 'S');
    else
      add_json_value(json_item_w, 'priority', 'R');
    end if;

    if (r_c01.MIN_QT_TEMPO_INF_ETAPA is not null) then
      add_json_value(json_item_w, 'occurrenceDurationId', r_c01.minute_code_value || r_c01.min_qt_tempo_inf_etapa);
      add_json_value(json_item_w, 'occurrenceDurationCodeSystem', r_c01.minute_code_value);
    end if;

		add_json_value(json_item_w, 'effectiveDate', nvl(r_c01.suspended_time, r_c01.requested_date_time));
		add_json_value(json_item_w, 'enteredBy', nvl(r_c01.order_prov_susp_id_number, r_c01.ordering_user_id_number));
		
		
		json_item_list_w.append(json_item_w.to_json_value());
		end;
	end loop;
	
	return json_item_list_w;
	
	end get_hemo_data;

	
	function get_hemo_message(nr_cpoe_hemo_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return philips_json is
	json_return_w		philips_json;
	json_item_list_w   	philips_json_list;
	ie_hl7_msg_type_w	varchar(3);
  ie_tipo_hemoterap_w   cpoe_hemoterapia.ie_tipo_hemoterap%type;
	begin
	
  select ie_tipo_hemoterap
  into ie_tipo_hemoterap_w 
  from cpoe_hemoterapia
  where nr_sequencia = nr_cpoe_hemo_p;

  if (ie_tipo_hemoterap_w = 0) then
    ie_hl7_msg_type_w := 'OMP';
  elsif (ie_tipo_hemoterap_w = 1) then
    ie_hl7_msg_type_w := 'ORM';
  end if;

  json_item_list_w	:= get_hemo_data(nr_cpoe_hemo_p);
	
	if	(json_item_list_w.count > 0) then
		json_return_w	:= philips_json();
		json_return_w		:= get_default_message(nr_cpoe_hemo_p);
		add_json_value(json_return_w, 'orderControl', ie_order_control_p);
		add_json_value(json_return_w, 'entityidentifier', nr_entity_identifier_p);
		add_json_value(json_return_w, 'hl7MsgType', ie_hl7_msg_type_w);
		
		json_return_w.put('hemoList', json_item_list_w.to_json_value());
	end if;
	
	return json_return_w;
	
	end get_hemo_message;
					

	function get_message_clob(nr_cpoe_hemo_p	number, ie_order_control_p varchar2, nr_entity_identifier_p number) return clob is
	ds_json_out_w		clob;
	json_hemo_w	philips_json;
	
	begin
		
	json_hemo_w		:= get_hemo_message(nr_cpoe_hemo_p, ie_order_control_p, nr_entity_identifier_p);
	
	if	(json_hemo_w is null) then
		return null;
	end if;
	
	dbms_lob.createtemporary( ds_json_out_w, true);
	json_hemo_w.to_clob(ds_json_out_w);
	
	return ds_json_out_w;
	end get_message_clob;


	procedure getCpoeIntegracaoHemo(nr_seq_hemo_cpoe_p number, nr_entity_identifier_p out number) is
	begin

	select max(nr_sequencia)
	into nr_entity_identifier_p
	from cpoe_integracao
	where NR_SEQ_HEMO_CPOE = nr_seq_hemo_cpoe_p;
	
	if (nr_entity_identifier_p is null) then
		select CPOE_INTEGRACAO_SEQ.nextval
		into nr_entity_identifier_p
		from dual;

		insert into cpoe_integracao (
			nr_sequencia,
			NR_SEQ_HEMO_CPOE
			)
		values (
			nr_entity_identifier_p,
			nr_seq_hemo_cpoe_p
		);
		/* No commit so it can be used inside trigger */
	end if;	

	end getCpoeIntegracaoHemo;	

end cpoe_hemo_order_json_pck;
/
