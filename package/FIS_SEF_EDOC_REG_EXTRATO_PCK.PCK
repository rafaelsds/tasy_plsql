create or replace 
package fis_sef_edoc_reg_extrato_pck as
/*Cabe�alho*/

/*VARIAVEIS GERAIS DA PCK*/

/*Obte��o do usu�rio ativo no tasy*/
nm_usuario_w			usuario.nm_usuario%type:= Obter_Usuario_Ativo;

/*Essas variaveis s�o alimentadas pela procedure fis_gerar_dados_controle_edoc */
cd_estabelecimento_w		fis_sef_edoc_controle.cd_estabelecimento%type;
dt_inicio_apuracao_w		fis_sef_edoc_controle.dt_inicio_apuracao%type;
dt_fim_apuracao_w		fis_sef_edoc_controle.dt_fim_apuracao%type;
cd_ver_w			fis_sef_edoc_controle.cd_ver%type;
ie_retificadora_w		fis_sef_edoc_controle.ie_retificadora%type;
cd_ctd_w			fis_sef_edoc_controle.cd_ctd%type;
ie_tipo_arquivo_w		fis_sef_edoc_controle.ie_tipo_arquivo%type;
nr_seq_controle_w		fis_sef_edoc_controle.nr_sequencia%type;
nr_seq_modelo_nf_w		fis_sef_edoc_controle.nr_seq_modelo_nf%type;
ie_tipo_data_w			fis_sef_edoc_controle.ie_tipo_data%type;
nr_cod_0450_w			fis_sef_edoc_controle.nr_cod_0450%type;

/*Variavel de controle para o c020 e c300, isso deve ficar assim devido a grava��o foda da rotina*/
qt_cursor_c020_w		number(10) := 0;
qt_cursor_c300_w		number(10) := 0;

/*Cria��o de array para consegui salvar com nr_seq_superior*/
/*Cria��o do array com o tipo sendo da tabela especificada - fis_sef_edoc_c020*/
type registro_c020 is table of fis_sef_edoc_c020%rowtype index by pls_integer;
fis_registros_c020_w registro_c020;

/*Cria��o do array com o tipo sendo da tabela especificada - fis_sef_edoc_c300*/
type registro_c300 is table of fis_sef_edoc_c300%rowtype index by pls_integer;
fis_registros_c300_w registro_c300;

/*Array de totalizadores para o c020*/
type totalizador_c300 is varray(7) of number(15,2);
fis_totalizador_c300_w totalizador_c300 := totalizador_c300(0,0,0,0,0,0,0);

/*Procedure que chama as rotinas para regra��o dos dados*/
procedure fis_gerar_dados_edoc_extrato(nr_seq_controle_p number);

/*Procedure que limpa os registros*/
procedure fis_limpa_dados_edoc_extrato(nr_seq_controle_p number);

/*Procedure que carrega os dados da tabela fis_sef_edoc_controle nas variaveis globais da pck*/
procedure fis_gerar_dados_controle_edoc(nr_seq_controle_p	number,
					nr_seq_regra_p		number	);

/*Procedure para gravar os registro c020 e c300, devido ao nr_seq_superior*/
procedure fis_gravar_reg_c020_c300_edoc;

/*Procedure que gera os dados dos registros c020 dos arquivos*/
procedure fis_gerar_reg_c020_edoc;

/*Procedure que gera os dados dos registros c300 dos arquivos*/
procedure fis_gerar_reg_c300_edoc(	nr_seq_superior_p	number,
					nr_seq_nota_p		number);

end fis_sef_edoc_reg_extrato_pck;
/

create or replace 
package body fis_sef_edoc_reg_extrato_pck as

/*Procedure de gra��o de dados dos registros*/
procedure fis_gerar_dados_edoc_extrato(	nr_seq_controle_p	number) is

nr_seq_regra_w	fis_sef_edoc_regra.nr_sequencia%type;

Cursor C01 is
	select	b.nr_sequencia
	from	fis_sef_edoc_controle a,
		fis_sef_edoc_regra b
	where	a.nr_sequencia		= b.nr_seq_controle
	and	a.nr_sequencia		= nr_seq_controle_p; 

begin

/*Limpa os dados da tabela*/
fis_limpa_dados_edoc_extrato(nr_seq_controle_p);

open C01;
loop
fetch C01 into	
	nr_seq_regra_w;
exit when C01%notfound;
	begin
	
	update	fis_sef_edoc_controle
	set	nr_cod_0450 = nr_cod_0450_w
	where	nr_sequencia = nr_seq_controle_p;
	
	/*Chamada da procedure que carrega os dados da tabela fis_sef_edoc_controle nas variaveis globais da pck*/
	fis_gerar_dados_controle_edoc(	nr_seq_controle_p,
					nr_seq_regra_w);

	/*ATEN��O - O C300 � CHAMANDO DENTRO DO C020
	Chamada da procedure que gera os dados dos registros c020 dos arquivos*/
	fis_gerar_reg_c020_edoc;

	end;
end loop;
close C01;


end fis_gerar_dados_edoc_extrato;

/*Procedure que carrega os dados da tabela fis_sef_edoc_controle nas variaveis globais da pck*/
procedure fis_gerar_dados_controle_edoc(	nr_seq_controle_p	number,
						nr_seq_regra_p		number) is 
begin

begin

select	a.cd_estabelecimento,
	a.dt_inicio_apuracao,
	a.dt_fim_apuracao,
	a.cd_ver,
	a.ie_retificadora,
	a.cd_ctd,
	a.nr_sequencia,
	a.ie_tipo_arquivo,
	b.nr_seq_modelo_nf,
	b.ie_tipo_data,
	nvl(a.nr_cod_0450,0)
into	cd_estabelecimento_w,
	dt_inicio_apuracao_w,
	dt_fim_apuracao_w,
	cd_ver_w,
	ie_retificadora_w,
	cd_ctd_w,
	nr_seq_controle_w,
	ie_tipo_arquivo_w,
	nr_seq_modelo_nf_w,
	ie_tipo_data_w,
	nr_cod_0450_w
from	fis_sef_edoc_controle a,
	fis_sef_edoc_regra b
where	a.nr_sequencia		= b.nr_seq_controle
and	a.nr_sequencia		= nr_seq_controle_p
and 	b.nr_sequencia		= nr_seq_regra_p;

exception
when others then
	cd_estabelecimento_w	:=	null;
	dt_inicio_apuracao_w	:=	null;
	dt_fim_apuracao_w	:=	null;
	cd_ver_w		:=	null;
	ie_retificadora_w	:=	null;
	cd_ctd_w		:=	null;
	nr_seq_controle_w	:=	null;
	ie_tipo_arquivo_w	:=	null;
	nr_seq_modelo_nf_w	:=	null;
	ie_tipo_data_w		:=	null;
	nr_cod_0450_w		:=	null;
end;

commit;

end fis_gerar_dados_controle_edoc;

/*Procedure que limpa os registros*/
procedure fis_limpa_dados_edoc_extrato(	nr_seq_controle_p	number) is 
begin

delete	from	fis_sef_edoc_c300
where	nr_seq_controle	= nr_seq_controle_p;

delete	from	fis_sef_edoc_c020
where	nr_seq_controle	= nr_seq_controle_p;

update	fis_sef_edoc_controle
set	nr_cod_0450 = 0
where	nr_sequencia = nr_seq_controle_p;

commit;

end fis_limpa_dados_edoc_extrato;

/*Grava��o dos registro c020 e c300*/
procedure fis_gravar_reg_c020_c300_edoc is 

begin
/*Grava o registro c020 para depois o c300, pois precisa do nr_sequencia gravado para poder inserir o nr_Seq_superior do c300*/
forall i in fis_registros_c020_w.first .. fis_registros_c020_w.last
	insert into fis_sef_edoc_c020 values fis_registros_c020_w(i);

/*Limpa o array*/
fis_registros_c020_w.delete;

/*Grava o c300*/
forall i in fis_registros_c300_w.first .. fis_registros_c300_w.last
	insert into fis_sef_edoc_c300 values fis_registros_c300_w(i);

/*Limpa o array*/
fis_registros_c300_w.delete;

commit;

qt_cursor_c020_w := 0; 
qt_cursor_c300_w := 0;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

end fis_gravar_reg_c020_c300_edoc;

/*Registro c020: documento - nota fiscal (c�digo 01), nota fiscal de produtor (c�digo 04) e nota fiscal eletr�nica (c�digo 55)*/
procedure fis_gerar_reg_c020_edoc is 

nr_seq_fis_sef_edoc_c020_w	fis_sef_edoc_c020.nr_sequencia%type;
cd_sit_w			fis_efd_icmsipi_C100.cd_sit%type	:= null;

cursor c_reg_c020 is
	select	decode(e.ie_operacao_fiscal, 'S', 1, 0) 											cd_ind_oper,
		decode(e.ie_operacao_fiscal, 'S', 0, 'E', decode(ie_tipo_nota, 'EP', 0, 1)) 							cd_ind_emit,
		decode(e.ie_operacao_fiscal, 'E', nvl(a.cd_cgc_emitente, a.cd_pessoa_fisica), nvl(cd_cgc,a.cd_pessoa_fisica)) 			cd_part,
		lpad(b.cd_modelo_nf, 2, 0) 													cd_mod, 
		a.cd_serie_nf 															cd_ser,
		a.nr_nota_fiscal 														nr_doc,
		a.nr_danfe 															ds_chv_nfe,
		decode(e.ie_operacao_fiscal, 'E', a.dt_emissao, null) 										dt_emis,
		decode(e.ie_operacao_fiscal, 'E', a.dt_entrada_saida, a.dt_emissao) 								dt_doc,
		a.cd_natureza_operacao														cd_nat,
		substr(decode(nvl(a.cd_condicao_pagamento,0), 0, 2, decode(obter_dados_nota_fiscal(a.nr_sequencia, '28'), 1, 0, 1)), 1, 1) 	cd_ind_pgto,
		a.vl_total_nota															vl_doc,
		null																vl_acmo,
		a.vl_frete															vl_frt,
		a.vl_seguro															vl_seg,
		a.vl_despesa_acessoria														vl_out_da,
		null																vl_at,
		null																vl_op_iss,
		null																cd_inf_obs,
		a.nr_sequencia 															nr_seq_nota,
		a.ie_status_envio														ie_status_envio,
		a.ds_observacao															ds_observacao
	from	nota_fiscal 			a,
		operacao_nota_modelo		d,
		modelo_nota_fiscal		b,		
		nota_fiscal_transportadora	c,
		operacao_nota			e
	where	a.cd_operacao_nf	= d.cd_operacao_nf
	and 	a.cd_operacao_nf	= e.cd_operacao_nf
	and 	d.cd_operacao_nf	= e.cd_operacao_nf
	and	d.nr_seq_modelo		= b.nr_sequencia(+)
	and 	a.nr_sequencia		= c.nr_seq_nota(+)
	and 	(((ie_tipo_data_w = 1) and (a.dt_emissao between dt_inicio_apuracao_w and dt_fim_apuracao_w)) 
		or ((ie_tipo_data_w = 2) and (a.dt_entrada_saida between dt_inicio_apuracao_w and dt_fim_apuracao_w)))
	and (((obter_dados_operacao_nota(a.cd_operacao_nf, '6') = 'E') and		-- Notas de Entrada
			(a.ie_status_envio is not null) and 				-- Sem envio ao fisco
			(a.ie_situacao in (2,3,9)))                                   	-- Sitaua��o 2- Estornada    3- Estorno - 9-Cancelada
		or ((obter_dados_operacao_nota(a.cd_operacao_nf, '6') = 'E') and  	-- Notas de Entrada
			(a.ie_situacao not in (2,3,9)))                               	-- Sitaua��o 2- Estornada    3- Estorno - 9-Cancelada
		or (obter_dados_operacao_nota(a.cd_operacao_nf, '6') = 'S')) 		-- Nostas de saida		
	and 	b.nr_sequencia		= nr_seq_modelo_nf_w
	and	a.cd_estabelecimento	= cd_estabelecimento_w;	
		
/*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_0150*/
type reg_c_reg_c020 is table of c_reg_c020%RowType;
vetregc020 reg_c_reg_c020;

begin

open c_reg_c020;
loop
fetch c_reg_c020 bulk collect into vetregc020 limit 1000;
	for i in 1 .. vetregc020.Count loop
		begin

		/*Incrementa a variavel para o array*/
		qt_cursor_c020_w:=	qt_cursor_c020_w + 1;
		
		/*Incrementar variavel de controle do codigo da observa��o se tiver observa��o na nota*/
		if	(vetregc020(i).ds_observacao is not null) then
			nr_cod_0450_w	:=	nr_cod_0450_w + 1;
		end if;
		
		/*Verifica��o para encontrar  a situa��o da nota*/
		if	(nvl(vetregc020(i).ie_status_envio, 'XX') <> 'XX') then
			case vetregc020(i).ie_status_envio
				when 'E' then
					cd_sit_w := '00';
				when 'C' then
					cd_sit_w := '90';
				when 'X' then
					cd_sit_w := '81';
				when 'D' then
					cd_sit_w := '80';
				else
					cd_sit_w := null;
			end case;
		else
			if	(vetregc020(i).cd_ind_oper = 0) then
				cd_sit_w :=	'00'; 
			end if;
		end if;

		/*Pega a sequencia da taleba fis_sef_edoc_ para o insert*/
		select	fis_sef_edoc_c020_seq.nextval
		into	nr_seq_fis_sef_edoc_c020_w
		from	dual;
				
		fis_gerar_reg_c300_edoc(nr_seq_fis_sef_edoc_c020_w,
					vetregc020(i).nr_seq_nota);
		

		/*Inserindo valores no array para realiza��o do forall posteriormente*/      
		fis_registros_c020_w(qt_cursor_c020_w).nr_sequencia 		:= nr_seq_fis_sef_edoc_c020_w;
		fis_registros_c020_w(qt_cursor_c020_w).dt_atualizacao 		:= sysdate;
		fis_registros_c020_w(qt_cursor_c020_w).nm_usuario 		:= nm_usuario_w;
		fis_registros_c020_w(qt_cursor_c020_w).dt_atualizacao_nrec	:= sysdate;
		fis_registros_c020_w(qt_cursor_c020_w).nm_usuario_nrec 		:= nm_usuario_w;
		fis_registros_c020_w(qt_cursor_c020_w).cd_reg 			:= 'C020';
		fis_registros_c020_w(qt_cursor_c020_w).cd_ind_oper		:= vetregc020(i).cd_ind_oper;		
		fis_registros_c020_w(qt_cursor_c020_w).cd_ind_emit		:= vetregc020(i).cd_ind_emit;
		fis_registros_c020_w(qt_cursor_c020_w).cd_part			:= vetregc020(i).cd_part;
		fis_registros_c020_w(qt_cursor_c020_w).cd_mod			:= vetregc020(i).cd_mod;
		fis_registros_c020_w(qt_cursor_c020_w).cd_sit			:= cd_sit_w;
		fis_registros_c020_w(qt_cursor_c020_w).cd_ser			:= substr(vetregc020(i).cd_ser,1,3);
		fis_registros_c020_w(qt_cursor_c020_w).nr_doc			:= substr(vetregc020(i).nr_doc,1,9);
		fis_registros_c020_w(qt_cursor_c020_w).ds_chv_nfe		:= vetregc020(i).ds_chv_nfe;
		fis_registros_c020_w(qt_cursor_c020_w).dt_doc			:= vetregc020(i).dt_doc;
		fis_registros_c020_w(qt_cursor_c020_w).dt_emis			:= vetregc020(i).dt_emis;
		fis_registros_c020_w(qt_cursor_c020_w).vl_doc			:= vetregc020(i).vl_doc;
		fis_registros_c020_w(qt_cursor_c020_w).cd_ind_pgto		:= vetregc020(i).cd_ind_pgto;
		fis_registros_c020_w(qt_cursor_c020_w).vl_desc			:= fis_totalizador_c300_w(1);
		fis_registros_c020_w(qt_cursor_c020_w).vl_merc			:= fis_totalizador_c300_w(2);
		fis_registros_c020_w(qt_cursor_c020_w).vl_frt			:= vetregc020(i).vl_frt;
		fis_registros_c020_w(qt_cursor_c020_w).vl_seg			:= vetregc020(i).vl_seg;
		fis_registros_c020_w(qt_cursor_c020_w).vl_out_da		:= vetregc020(i).vl_out_da;
		fis_registros_c020_w(qt_cursor_c020_w).vl_bc_icms		:= fis_totalizador_c300_w(3);
		fis_registros_c020_w(qt_cursor_c020_w).vl_icms			:= fis_totalizador_c300_w(4);
		fis_registros_c020_w(qt_cursor_c020_w).vl_bc_icms_st		:= fis_totalizador_c300_w(5);
		fis_registros_c020_w(qt_cursor_c020_w).vl_icms_st		:= fis_totalizador_c300_w(6);
		fis_registros_c020_w(qt_cursor_c020_w).vl_ipi			:= fis_totalizador_c300_w(7);
		fis_registros_c020_w(qt_cursor_c020_w).cd_nat			:= vetregc020(i).cd_nat;
		fis_registros_c020_w(qt_cursor_c020_w).vl_acmo			:= vetregc020(i).vl_acmo;
		fis_registros_c020_w(qt_cursor_c020_w).vl_op_iss		:= vetregc020(i).vl_op_iss;
		if	(vetregc020(i).ds_observacao is not null) then
			fis_registros_c020_w(qt_cursor_c020_w).cd_inf_obs		:= nr_cod_0450_w;
		end if;
		fis_registros_c020_w(qt_cursor_c020_w).nr_seq_nota		:= vetregc020(i).nr_seq_nota;
		fis_registros_c020_w(qt_cursor_c020_w).nr_seq_controle		:= nr_seq_controle_w;	
		
		fis_totalizador_c300_w(1) :=	0;
		fis_totalizador_c300_w(2) :=	0;
		fis_totalizador_c300_w(3) :=	0;
		fis_totalizador_c300_w(4) :=	0;
		fis_totalizador_c300_w(5) :=	0;
		fis_totalizador_c300_w(6) :=	0;
		fis_totalizador_c300_w(7) :=	0;		
		
		/*Quando a quantidade de registros no array do c300 for superior ou igual a 1000 deve-se gravar primeiro o
		registro c020 depois o c300*/
		if (fis_registros_c300_w.count >= 1000) then
			begin
			
			fis_gravar_reg_c020_c300_edoc;
			
			end;
		end if;

		end;
	end loop;
exit when c_reg_C020%notfound;
end loop;
close c_reg_C020;

if (fis_registros_c300_w.count > 0) or (fis_registros_c020_w.count > 0) then
	begin

	fis_gravar_reg_c020_c300_edoc;

	end;
end if;

end fis_gerar_reg_c020_edoc;

/*registro c300: itens do documento*/
procedure fis_gerar_reg_c300_edoc(	nr_seq_superior_p	number,
					nr_seq_nota_p		number) is 

nr_seq_fis_sef_edoc_c300_w	fis_sef_edoc_c300.nr_sequencia%type;

vl_bc_icms_w			fis_sef_edoc_c300.vl_bc_icms%type;
tx_aliq_icms_w			fis_sef_edoc_c300.tx_aliq_icms%type;
vl_icms_w			fis_sef_edoc_c300.vl_icms%type;
vl_bc_icms_st_w			fis_sef_edoc_c300.vl_bc_icms_st%type;
tx_aliq_st_w			fis_sef_edoc_c300.tx_aliq_st%type;
vl_icms_st_w			fis_sef_edoc_c300.vl_icms_st%type;
vl_bc_ipi_w			fis_sef_edoc_c300.vl_bc_ipi%type;
tx_aliq_ipi_w			fis_sef_edoc_c300.tx_aliq_ipi%type;
vl_ipi_w			fis_sef_edoc_c300.vl_ipi%type;

/**/
cursor c_reg_c300 is
	select	b.nr_item_nf nr_item,
		b.cd_material cd_item,
		substr(b.cd_unidade_medida_compra, 1,6) cd_unid,
		b.vl_unitario_item_nf vl_unid,
		b.qt_item_nf qt_item,
		b.vl_desconto vl_desc,
		null vl_acmo,
		b.vl_liquido vl_item,
		null cd_ncm,
		substr(c.ie_origem_mercadoria, 1, 1) || substr(c.ie_tributacao_icms, 1, 2) cd_cst,
		substr(Elimina_Caracter(obter_dados_natureza_operacao(a.cd_natureza_operacao, 'CF'), '.'), 1, 4) cd_cfop,
		c.ie_tributacao_icms,		-- Verifica��o para icms
		c.ie_tributacao_ipi,		-- Verifica��o para ipi
		b.nr_sequencia nr_seq_item_nf 	-- Informa��o para obter os impostos			
	from	nota_fiscal a,
		nota_fiscal_item b,
		material_fiscal c
	where	a.nr_sequencia = b.nr_Sequencia
	and 	b.cd_material	= c.cd_material(+)
	and 	a.nr_sequencia = nr_seq_nota_p;	
		
/*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_0175*/
type reg_c_reg_c300 is table of c_reg_c300%RowType;
vetregc300 reg_c_reg_c300;

begin

open c_reg_c300;
loop
fetch c_reg_c300 bulk collect into vetregc300 limit 1000;
	for i in 1 .. vetregc300.Count loop
		begin
		
		/*Limpeza de variaveis*/
		vl_icms_w	:= 0;
		vl_bc_icms_w	:= 0;
		tx_aliq_icms_w	:= 0;
		vl_icms_st_w	:= 0;
		vl_bc_icms_st_w	:= 0;
		tx_aliq_st_w	:= 0;
		vl_ipi_w	:= 0;
		vl_bc_ipi_w	:= 0;
		tx_aliq_ipi_w	:= 0;

		/*Incrementa a variavel para o array*/
		qt_cursor_c300_w:=	qt_cursor_c300_w + 1;
		
		/*Bloco para obter os valores de impostos. ICMS,*/
		if (vetregc300(i).ie_tributacao_icms in ('00', '10', '20', '70')) then
			fis_obter_valor_trib_item(	vetregc300(i).nr_seq_item_nf,
							vetregc300(i).nr_item,
							'ICMS',
							vl_icms_w,
							vl_bc_icms_w,
							tx_aliq_icms_w); 
		end if;
		
		/*Bloco para obter os valores de impostos. ICMSST*/
		if (vetregc300(i).ie_tributacao_icms in ('10', '30', '70')) then
			fis_obter_valor_trib_item(	vetregc300(i).nr_seq_item_nf,
							vetregc300(i).nr_item,
							'ICMSST',
							vl_icms_st_w,
							vl_bc_icms_st_w,
							tx_aliq_st_w);
		end if;
		
		/*Bloco para obter os valores de impostos. IPI*/
		if (vetregc300(i).ie_tributacao_ipi in ('00', '50')) then
			fis_obter_valor_trib_item(	vetregc300(i).nr_seq_item_nf,
							vetregc300(i).nr_item,
							'IPI',
							vl_ipi_w,
							vl_bc_ipi_w,
							tx_aliq_ipi_w);
		end if;

		/*Pega a sequencia da taleba fis_sef_edoc_c300 para o insert*/
		select	fis_sef_edoc_c300_seq.nextval
		into	nr_seq_fis_sef_edoc_c300_w
		from	dual;

		/*Inserindo valores no array para realiza��o do forall posteriormente*/      
		fis_registros_c300_w(qt_cursor_c300_w).nr_sequencia 		:= nr_seq_fis_sef_edoc_c300_w;
		fis_registros_c300_w(qt_cursor_c300_w).dt_atualizacao 		:= sysdate;
		fis_registros_c300_w(qt_cursor_c300_w).nm_usuario 		:= nm_usuario_w;
		fis_registros_c300_w(qt_cursor_c300_w).dt_atualizacao_nrec	:= sysdate;
		fis_registros_c300_w(qt_cursor_c300_w).nm_usuario_nrec 		:= nm_usuario_w;
		fis_registros_c300_w(qt_cursor_c300_w).cd_reg 			:= 'C300';
		fis_registros_c300_w(qt_cursor_c300_w).nr_item			:= vetregc300(i).nr_item; 
		fis_registros_c300_w(qt_cursor_c300_w).cd_item			:= vetregc300(i).cd_item;
		fis_registros_c300_w(qt_cursor_c300_w).cd_unid			:= vetregc300(i).cd_unid;
		fis_registros_c300_w(qt_cursor_c300_w).vl_unit			:= vetregc300(i).vl_unid;
		fis_registros_c300_w(qt_cursor_c300_w).qt_item			:= vetregc300(i).qt_item;
		fis_registros_c300_w(qt_cursor_c300_w).vl_desc			:= vetregc300(i).vl_desc;
		fis_registros_c300_w(qt_cursor_c300_w).vl_acmo			:= vetregc300(i).vl_acmo;
		fis_registros_c300_w(qt_cursor_c300_w).vl_item			:= vetregc300(i).vl_item;
		fis_registros_c300_w(qt_cursor_c300_w).cd_ncm			:= vetregc300(i).cd_ncm;
		fis_registros_c300_w(qt_cursor_c300_w).cd_cst			:= vetregc300(i).cd_cst;
		fis_registros_c300_w(qt_cursor_c300_w).cd_cfop			:= vetregc300(i).cd_cfop;
		fis_registros_c300_w(qt_cursor_c300_w).vl_bc_icms		:= vl_bc_icms_w;
		fis_registros_c300_w(qt_cursor_c300_w).tx_aliq_icms		:= tx_aliq_icms_w;
		fis_registros_c300_w(qt_cursor_c300_w).vl_icms			:= vl_icms_w;
		fis_registros_c300_w(qt_cursor_c300_w).vl_bc_icms_st		:= vl_bc_icms_st_w;
		fis_registros_c300_w(qt_cursor_c300_w).tx_aliq_st		:= tx_aliq_st_w;
		fis_registros_c300_w(qt_cursor_c300_w).vl_icms_st		:= vl_icms_st_w;
		fis_registros_c300_w(qt_cursor_c300_w).vl_bc_ipi		:= vl_bc_ipi_w;
		fis_registros_c300_w(qt_cursor_c300_w).tx_aliq_ipi		:= tx_aliq_ipi_w;
		fis_registros_c300_w(qt_cursor_c300_w).vl_ipi			:= vl_ipi_w;
		fis_registros_c300_w(qt_cursor_c300_w).nr_seq_superior		:= nr_seq_superior_p;
		fis_registros_c300_w(qt_cursor_c300_w).nr_seq_controle 		:= nr_seq_controle_w;
		
		/*Totalizadores para o c020*/
		/*Totalizador do valore de desconto*/
		fis_totalizador_c300_w(1) :=	fis_totalizador_c300_w(1) + vetregc300(i).vl_desc;
		/*Totalizador do valor do item/ valor da mercadoria*/
		fis_totalizador_c300_w(2) :=	fis_totalizador_c300_w(2) + vetregc300(i).vl_item;
		/*Totalizador do valor base de ICMS*/
		fis_totalizador_c300_w(3) :=	fis_totalizador_c300_w(3) + vl_bc_icms_w;
		/*Totalizador do valor de ICMS*/
		fis_totalizador_c300_w(4) :=	fis_totalizador_c300_w(4) + vl_icms_w;
		/*Totalizador do valor base de ICMSST*/
		fis_totalizador_c300_w(5) :=	fis_totalizador_c300_w(5) + vl_bc_icms_st_w;
		/*Totalizador do valor de ICMSST*/
		fis_totalizador_c300_w(6) :=	fis_totalizador_c300_w(6) + vl_icms_st_w;
		/*Totalizador dO valor de IPI*/
		fis_totalizador_c300_w(7) :=	fis_totalizador_c300_w(7) + vl_ipi_w;
		
		end;
	end loop;
exit when c_reg_c300%notfound;
end loop;
close c_reg_c300;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

end fis_gerar_reg_c300_edoc;

end fis_sef_edoc_reg_extrato_pck;
/
