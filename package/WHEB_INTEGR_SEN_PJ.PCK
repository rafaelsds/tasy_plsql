create or replace
package WHEB_INTEGR_SEN_PJ is

procedure inserir_pessoa_juridica(	cd_estabelecimento_p	number,
				cd_tipo_pessoa_p		number,
				cd_cgc_p		varchar2,
				ds_razao_social_p		varchar2,
				nm_fantasia_p		varchar2,
				cd_cep_p		varchar2,
				nm_pessoa_contato_p	varchar2,
				nr_ddd_telefone_p		varchar2,
				nr_telefone_p		varchar2,
				ds_email_p		varchar2,
				ds_site_internet_p		varchar2,
				ds_endereco_p		varchar2,
				nr_endereco_p		varchar2,
				ds_complemento_p		varchar2,
				ds_bairro_p		varchar2,
				ds_municipio_p		varchar2,
				sg_estado_p		varchar2,
				nr_inscricao_estadual_p	varchar2,
				nr_inscricao_municipal_p	varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p		out	varchar2);

end WHEB_INTEGR_SEN_PJ;
/
create or replace
package body WHEB_INTEGR_SEN_PJ is

	procedure inserir_pessoa_juridica(	cd_estabelecimento_p	number,
					cd_tipo_pessoa_p		number,
					cd_cgc_p		varchar2,
					ds_razao_social_p		varchar2,
					nm_fantasia_p		varchar2,
					cd_cep_p		varchar2,
					nm_pessoa_contato_p	varchar2,
					nr_ddd_telefone_p		varchar2,
					nr_telefone_p		varchar2,
					ds_email_p		varchar2,
					ds_site_internet_p		varchar2,
					ds_endereco_p		varchar2,
					nr_endereco_p		varchar2,
					ds_complemento_p		varchar2,
					ds_bairro_p		varchar2,
					ds_municipio_p		varchar2,
					sg_estado_p		varchar2,
					nr_inscricao_estadual_p	varchar2,
					nr_inscricao_municipal_p	varchar2,
					nm_usuario_p		varchar2,
					ds_erro_p		out	varchar2) is

	nr_sequencia_w			number(10);
	cd_cgc_w			varchar2(14);
	ds_erro_w			varchar2(255);
	cd_tipo_pessoa_w			number(3);
		
	begin
	
	cd_tipo_pessoa_w	:= nvl(cd_tipo_pessoa_p,0);
	
	if	(cd_tipo_pessoa_w = 0) then
		
		select	nvl(min(cd_tipo_pessoa),0)
		into	cd_tipo_pessoa_w
		from	tipo_pessoa_juridica;
	end if;
	
	if	(cd_tipo_pessoa_w = 0) then
		ds_erro_w	:= 'Falta cadastrar o tipo de pessoa juridica no Tasy.';
	end if;
	
	cd_cgc_w	:= replace(cd_cgc_p,'.','');
	
	if	(length(cd_cgc_w) <> 14) then
		ds_erro_w	:= 'CNPJ Invalido: ' || cd_cgc_p;
	end if;

	if	(nvl(ds_erro_w,'X') = 'X') then
			
		begin
		insert into pessoa_juridica(
			cd_cgc,
			ds_razao_social,
			nm_fantasia,
			cd_cep,
			ds_endereco,
			ds_bairro,
			ds_municipio,
			sg_estado,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_complemento,
			nr_endereco,
			nr_ddd_telefone,
			nr_telefone,
			nr_inscricao_estadual,
			nr_inscricao_municipal,
			cd_tipo_pessoa,
			ie_prod_fabric,
			ie_situacao,
			ds_site_internet)
		values(	cd_cgc_w,
			substr(ds_razao_social_p,1,255),
			substr(nm_fantasia_p,1,80),
			cd_cep_p,
			ds_endereco_p,
			ds_bairro_p,
			ds_municipio_p,
			sg_estado_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			'IntegracaoSen',
			ds_complemento_p,
			nr_endereco_p,
			nr_ddd_telefone_p,
			nr_telefone_p,
			nr_inscricao_estadual_p,
			nr_inscricao_municipal_p,
			cd_tipo_pessoa_w,
			'N',
			'A',
			ds_site_internet_p);
		exception when others then
			ds_erro_w	:= substr(sqlerrm(sqlcode),1,255);
			ds_erro_p	:= substr('Erro ao cadastrar a PJ: ' || cd_cgc_w || ' - ' || ds_erro_w,1,255);
		end;
		if	(nvl(ds_erro_w,'X') = 'X') and
			(nvl(cd_estabelecimento_p,0) <> 0) then
			
			begin
			insert into pessoa_juridica_estab ( 
				nr_sequencia,
				cd_cgc, 
				cd_estabelecimento, 
				dt_atualizacao, 
				nm_usuario, 
				nm_pessoa_contato, 
				nr_ramal_contato,
				ds_email, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec,
				ie_conta_dif_nf) 
			values( pessoa_juridica_estab_seq.nextval,
				lpad(cd_cgc_w,14,0),
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				nm_pessoa_contato_p,
				null,
				ds_email_p,
				sysdate,
				'IntegracaoSen',
				'N');
			exception when others then
				ds_erro_w	:= substr(sqlerrm(sqlcode),1,255);
				ds_erro_p	:= substr('Erro ao cadastrar a PJ (2): ' || cd_cgc_w || ' - ' || ds_erro_w,1,255);
			end;
		end if;
			
	end if;
	commit;
	end;
	
end WHEB_INTEGR_SEN_PJ;
/
