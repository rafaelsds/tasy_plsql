create or replace package philips_business_log_pck as

procedure write_log(cd_log_p varchar2, ds_log_p varchar2);

end philips_business_log_pck;

/

create or replace package body philips_business_log_pck as

ie_grava_log_w	varchar2(1);

procedure check_if_should_log as

begin

	dbms_output.put_line('Ie_grava_log_w ' || ie_grava_log_w);
	if ie_grava_log_w is null then
		select 	nvl(max(ie_gravar_log), 'N')
		into	ie_grava_log_w
		from	parametro_faturamento
		where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
	end if;	

end check_if_should_log;

procedure write_log(cd_log_p in varchar2, ds_log_p in varchar2) as

nr_seq_proc_log_w	number;

begin

	check_if_should_log;
	if ie_grava_log_w = 'S' then
		select 	log_proc_faturamento_seq.nextval
		into	nr_seq_proc_log_w
		from	dual;
		
		insert into log_proc_faturamento
		(nr_sequencia, cd_funcao, cd_estabelecimento, dt_log, nm_usuario, ds_log, cd_tipo_evento)
		values
		(nr_seq_proc_log_w, obter_funcao_ativa, wheb_usuario_pck.get_cd_estabelecimento, 
		sysdate, wheb_usuario_pck.get_nm_usuario, ds_log_p, cd_log_p);
		commit;
	end if;

end write_log;

end philips_business_log_pck;
/

