create	or replace
package cronograma_padrao_npi as

-- dados do projeto em tratamento, quando houver necessidade de mais campos relativos ao projeto, adicion�-los aqui.
type	dados_projeto	is record	(
		nr_seq_projeto		proj_projeto.nr_sequencia%type,
		dt_inicio_prev		proj_projeto.dt_inicio_prev%type,
		cd_estabelecimento	proj_projeto.cd_estabelecimento%type
	);

--procedure que cria os cronogramas e suas etapas	
procedure criar_cronograma_npi(	nr_seq_projeto_p	number,
								nm_usuario_p	varchar2); 	
	
-- chamada sempre antes de qualquer procedure afim de atualizar a vari�vel global com os dados necess�rios.
procedure seta_dados_projeto (nr_seq_projeto_p	number);

-- ap�s criado o projeto, 	esta procedure ajusta as datas com as depend�ncias cadastradas conforme a regra
procedure gera_datas_cronograma_npi(	nr_seq_projeto_p	number,
										nm_usuario_p		varchar2);

-- procedure chamada na trigger afterpost 
procedure seta_dt_inicio_fim(	nr_seq_cron_p	number,
								nm_usuario_p	varchar2);
				
-- procedure que verifica e seta as datas das etapas dependentes
procedure gerar_datas_etapas_dep(	nr_seq_etapa_cron_p	number,
									nm_usuario_p	varchar2);				

procedure alterar_datas_cronograma_npi(	nr_seq_cronograma_p	number,
										dt_inicio_p			date,
										ds_justificativa_p	varchar2,
										nm_usuario_p		varchar2);

procedure recalcula_cronograma_npi(	nr_seq_cronograma_p	number,
										nm_usuario_p		varchar2);
				
end cronograma_padrao_npi;
/


create or replace
package body 	cronograma_padrao_npi as

/*---------------------------------------------------------------------------------------------------------------------------------------------
|			VARIAVEIS_GLOBAIS					|
-----------------------------------------------------------------------------------------------------------------------------------------------*/
projeto		dados_projeto;

/*---------------------------------------------------------------------------------------------------------------------------------------------
|			SETA_DADOS_PROJETO					|
-----------------------------------------------------------------------------------------------------------------------------------------------*/
	procedure seta_dados_projeto (nr_seq_projeto_p	number) is
	begin
		projeto.nr_seq_projeto := nr_seq_projeto_p;

		select	nvl(min(a.dt_inicio_prev),sysdate),
			max(cd_estabelecimento)
		into	projeto.dt_inicio_prev,
			projeto.cd_estabelecimento
		from	proj_projeto a
		where	a.nr_sequencia = projeto.nr_seq_projeto;

	end seta_dados_projeto;
	
/*---------------------------------------------------------------------------------------------------------------------------------------------
|			SETA_DT_INICIO_FIM					|
-----------------------------------------------------------------------------------------------------------------------------------------------*/			
	procedure seta_dt_inicio_fim	(	nr_seq_cron_p	number,
					nm_usuario_p	varchar2) is

	min_dt_inicio_w		date;
	max_dt_fim_w		date;
	nr_seq_proj_w		number(10);
	qt_hora_prev_w		proj_cron_etapa.qt_hora_prev%type;

	begin
	
	select	max(nr_seq_proj)
	into	nr_seq_proj_w
	from	proj_cronograma
	where	nr_sequencia = nr_seq_cron_p;
	
	-- menor data de inicio e maior data fim
	select	min(dt_inicio_prev),
			max(dt_fim_prev),
			sum(qt_hora_prev)
	into	min_dt_inicio_w,
			max_dt_fim_w,
			qt_hora_prev_w
	from	proj_cron_etapa
	where	nr_seq_cronograma = nr_seq_cron_p
	and		dt_inicio_prev is not null
	and		dt_fim_prev is not null;
	
	--seta no cronograma atual a menor data de inicio das etapas e a maior data de fim
	update	proj_cronograma
	set		dt_inicio = nvl(min_dt_inicio_w,sysdate),
			dt_fim = nvl(max_dt_fim_w,sysdate),
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			qt_total_horas = qt_hora_prev_w
	where	nr_sequencia = nr_seq_cron_p;
	
	--busca a menor data de inicio dos cronogramas do projeto e a maior data de fim
	select	min(dt_inicio),
			max(dt_fim)
	into	min_dt_inicio_w,
			max_dt_fim_w	
	from	proj_cronograma
	where	nr_seq_proj = nr_seq_proj_w;

	--atualiza o projeto com as datas conforme as datas dos cronogramas
	update	proj_projeto
	set		dt_inicio_prev = min_dt_inicio_w,
			dt_fim_prev = max_dt_fim_w,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
	where	nr_sequencia = nr_seq_proj_w;

	commit;
		
	end 	seta_dt_inicio_fim;

/*---------------------------------------------------------------------------------------------------------------------------------------------
|			GERA_DATAS_CRONOGRAMA_NPI					|
-----------------------------------------------------------------------------------------------------------------------------------------------*/
	procedure gera_datas_cronograma_npi(	nr_seq_projeto_p	number,
					nm_usuario_p		varchar2) is

	nr_seq_etapa_w			proj_cron_etapa.nr_sequencia%type;
	nr_seq_etapa_processo_w	prp_etapa_processo.nr_sequencia%type;
	nr_seq_cronograma_w 	proj_cronograma.nr_sequencia%type;
	nr_seq_cron_etapa_w 	proj_cron_etapa.nr_sequencia%type;
	--qt_hora_prev_w			proj_cron_etapa.qt_hora_prev%type;
	qt_dias_w			proj_cron_etapa.qt_dias%type;
	dt_inicio_w				date;
	dt_fim_prev_w			date;
	nr_seq_tipo_projeto_w	proj_projeto.nr_seq_tipo_projeto%type;
	
	--Cronogramas do projeto ordenados conforme o mapa do projeto
	Cursor C01 is
		select	nr_sequencia
		from	proj_cronograma
		where	nr_seq_proj = nr_seq_projeto_p
		order by nr_seq_processo_fase;
		
	--etapas do cronograma que n�o dependem de nenhuma outra etapa
	Cursor C02 is
		select	a.nr_sequencia
		from	proj_cron_etapa a,
				prp_etapa_processo b
		where	a.nr_seq_etapa_npi = b.nr_sequencia
		and		a.nr_seq_cronograma = nr_seq_cronograma_w
		and	not exists (select	1 
						from	prp_etapa_dependencia x,
								proj_cron_etapa y,
								prp_etapa_processo z
						where	x.nr_seq_etapa_principal = b.nr_sequencia
						AND   y.nr_seq_etapa_npi = z.nr_sequencia
						AND x.nr_Seq_etapa_processo = z.nr_sequencia
						and		x.nr_seq_tipo_projeto = nr_seq_tipo_projeto_w
						AND a.nr_seq_cronograma = y.nr_Seq_cronograma);
	
	begin
	
	-- Pega a data inicial prevista do projeto, para o primeiro cronograma/atividades
	select	max(dt_inicio_prev),
			max(nr_seq_tipo_projeto)
	into	dt_inicio_w,
			nr_seq_tipo_projeto_w
	from	proj_projeto
	where	nr_sequencia = nr_seq_projeto_p;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_cronograma_w;
	exit when C01%notfound;
		begin
		
		--Chama as atividades do cronograma
		open C02;
		loop
		fetch C02 into	
			nr_seq_cron_etapa_w;
		exit when C02%notfound;
			begin
			
			select	max(qt_dias)
			into	qt_dias_w
			from	proj_cron_etapa
			where	nr_sequencia = nr_seq_cron_etapa_w;
			
			dt_fim_prev_w := dt_inicio_w + (qt_dias_w) -1;
			
			if (pkg_date_utils.get_WeekDay(dt_fim_prev_w) = 7) then
				dt_fim_prev_w := dt_fim_prev_w+2;
			elsif (pkg_date_utils.get_WeekDay(dt_fim_prev_w) = 1) then
				dt_fim_prev_w := dt_fim_prev_w+1;
			end if;

			update	proj_cron_etapa
			set		dt_inicio_prev = dt_inicio_w,
					dt_fim_prev = OBTER_DATA_DIAS_UTEIS(1,dt_inicio_w,(qt_dias_w)-1)
			where	nr_sequencia = nr_seq_cron_etapa_w;
			
			commit;
					
			--verifica etapas dependentes
			gerar_datas_etapas_dep(nr_seq_cron_etapa_w,nm_usuario_p);
			
			end;
		end loop;
		close C02;
		
		select	max(dt_fim_prev)+1
		into	dt_inicio_w
		from	proj_cron_etapa
		where	nr_seq_cronograma = nr_seq_cronograma_w;
		
		seta_dt_inicio_fim(nr_seq_cronograma_w,nm_usuario_p);
		
		end;
	end loop;
	close C01;
	
	end gera_datas_cronograma_npi;

/*---------------------------------------------------------------------------------------------------------------------------------------------
|			GERAR_DATAS_ETAPAS_DEP										|
-----------------------------------------------------------------------------------------------------------------------------------------------*/
	procedure gerar_datas_etapas_dep(	nr_seq_etapa_cron_p	number,
										nm_usuario_p	varchar2) is
	
	nr_seq_etapa_npi_w	proj_cron_etapa.nr_seq_etapa_npi%type;
	nr_seq_etapa_cron_w	proj_cron_etapa.nr_sequencia%type;
	nr_seq_cronograma_w	proj_cron_etapa.nr_seq_cronograma%type;
	dt_fim_prev_w		date;
	dt_inicio_prev_w	date;
	dt_fim_w			date;
	qt_hora_prev_w		proj_cron_etapa.qt_hora_prev%type;
	qt_dias_w			proj_cron_etapa.qt_dias%type;
	nr_seq_proj_w		proj_projeto.nr_sequencia%type;
	nr_seq_tipo_projeto_w	proj_projeto.nr_seq_tipo_projeto%type;
	
	Cursor C01 is
		select	a.nr_sequencia
		from	proj_cron_etapa a,
				prp_etapa_processo b
		where	a.nr_seq_etapa_npi = b.nr_sequencia
		and		a.nr_seq_cronograma = nr_seq_cronograma_w
		and	exists (select	1 
						from	prp_etapa_dependencia x,
								proj_cron_etapa y,
								prp_etapa_processo z
						where	x.nr_seq_etapa_principal = b.nr_sequencia
						AND		x.nr_Seq_etapa_processo = z.nr_sequencia
						AND		y.nr_seq_etapa_npi = z.nr_sequencia
						and		y.nr_seq_etapa_npi = nr_seq_etapa_npi_w
						and		x.nr_seq_tipo_projeto = nr_seq_tipo_projeto_w
						AND		a.nr_seq_cronograma = y.nr_Seq_cronograma);
	
	begin
		
		select	max(nr_seq_etapa_npi),
				max(dt_fim_prev),
				max(nr_seq_cronograma)
		into	nr_seq_etapa_npi_w,
				dt_fim_prev_w,
				nr_seq_cronograma_w
		from	proj_cron_etapa
		where	nr_sequencia = nr_seq_etapa_cron_p;
		
		select	max(a.nr_sequencia),
				max(a.nr_seq_tipo_projeto)
		into	nr_seq_proj_w,
				nr_seq_tipo_projeto_w
		from	proj_projeto a,
				proj_cronograma b
		where	a.nr_sequencia = b.nr_seq_proj
		and		b.nr_sequencia = nr_seq_cronograma_w;
		
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_etapa_cron_w;
		exit when C01%notfound;
			begin
			
			select	max(qt_dias)
			into	qt_dias_w
			from	proj_cron_etapa
			where	nr_sequencia = nr_seq_etapa_cron_w;
			
			dt_inicio_prev_w := dt_fim_prev_w+1;
			
			if (pkg_date_utils.get_WeekDay(dt_inicio_prev_w) = 7) then
				dt_inicio_prev_w := dt_inicio_prev_w+2;
			elsif (pkg_date_utils.get_WeekDay(dt_inicio_prev_w) = 1) then
				dt_inicio_prev_w := dt_inicio_prev_w+1;
			end if;
			
			dt_fim_w := trunc(dt_fim_prev_w + round(qt_dias_w,0));
			
			if (pkg_date_utils.get_WeekDay(dt_fim_w) = 7) then
				dt_fim_w := dt_fim_w+2;
			elsif (pkg_date_utils.get_WeekDay(dt_fim_w) = 1) then
				dt_fim_w := dt_fim_w+1;
			end if;
			
			update	proj_cron_etapa
			set		dt_inicio_prev = dt_inicio_prev_w,
					dt_fim_prev = OBTER_DATA_DIAS_UTEIS(1,dt_inicio_prev_w,round(qt_dias_w,0)-1)
			where	nr_sequencia = nr_seq_etapa_cron_w;
			
			commit;
			
			gerar_datas_etapas_dep(nr_seq_etapa_cron_w,nm_usuario_p);
			
			end;
		end loop;
		close C01;

	end gerar_datas_etapas_dep;	

/*---------------------------------------------------------------------------------------------------------------------------------------------
|			CRIAR_CRONOGRAMA_NPI						|
-----------------------------------------------------------------------------------------------------------------------------------------------*/
	procedure criar_cronograma_npi(	nr_seq_projeto_p	number,
					nm_usuario_p	varchar2) is

	dt_inicio_prev_w		date;
	dt_fim_prev_w		date;
	qt_etapas_w		number(5);
	qt_regra_tempo_w		number(5);
	qt_horas_w				number(15,2);
	nr_seq_processo_fase_w	number(10);
	nr_seq_processo_etapa_w	number(10);
	nr_seq_etapa_processo_w	number(10);
	nr_seq_etapa_w		number(10);
	nr_seq_cronograma_w	number(10);
	nr_seq_cliente_w		number(10);
	nr_seq_fase_processo_w	number(10);
	cd_gerente_cliente_w	varchar2(10);
	cd_pessoa_fisica_w	varchar2(10);
	nm_etapa_w		varchar2(255);
	ds_objetivo_w		varchar2(4000);
	nr_seq_apresentacao_w	number(15);
	ds_fase_w		varchar2(4000);
	qt_hora_prev_w		number(15);
	cd_estabelecimento_w	number(4);
	ie_data_nula_w		boolean:= true;
	ie_gerou_low_w		boolean:= false;
	ie_status_etapa_w		varchar2(1);
	nr_seq_area_processo_w	number(10);
	nr_seq_tipo_projeto_w	number(10);
	nr_seq_grupo_w			prp_area_processo_partic.nr_seq_grupo%type;

	Cursor C01 is
		select	distinct b.nr_sequencia,
			c.ds_fase,
			c.nr_sequencia nr_seq_fase_processo
		from	prp_processo a,
			prp_processo_fase b,
			prp_fase_processo c
		where	a.nr_sequencia = b.nr_seq_processo
		and	c.nr_sequencia = b.nr_seq_fase_processo
		and	a.nr_seq_projeto = nr_seq_projeto_p
		and	exists (select	1
				from	prp_processo_etapa e
				where	b.nr_sequencia = e.nr_seq_processo_fase)
		order by b.nr_sequencia;

	Cursor C02 is
		select	a.nr_sequencia,
			a.nm_etapa,
			a.ds_objetivo,
			c.nr_seq_apresentacao,
			c.nr_sequencia nr_seq_processo_etapa
		from	prp_processo_etapa c,
			prp_etapa_processo a
		where	c.nr_seq_processo_fase = nr_seq_processo_fase_w
		and	a.nr_sequencia = c.nr_seq_etapa_processo
		order 	by c.nr_seq_apresentacao;

	Cursor C03 is
		select	x.nr_seq_grupo,
				d.cd_pessoa_fisica
		from	prp_fase_processo a,
				prp_fase_processo_etapa b,
				prp_regra_equipe_padrao c,
				prp_regra_equipe_partic d,
				prp_area_processo_partic x
		where	a.nr_sequencia = b.nr_seq_fase_processo
		and		b.nr_seq_area_processo = c.nr_seq_area_processo(+)
		and		b.nr_seq_area_proc_partic = c.nr_seq_area_proc_partic(+)
		and		c.nr_sequencia = d.nr_seq_equipe_padrao(+)
		and		x.nr_sequencia = b.nr_seq_area_proc_partic
		and		b.nr_seq_etapa_processo = nr_seq_etapa_processo_w;

	begin

	select	dt_inicio_prev,
		cd_estabelecimento
	into	dt_inicio_prev_w,
		cd_estabelecimento_w
	from	proj_projeto
	where	nr_sequencia = nr_seq_projeto_p;

	if (dt_inicio_prev_w is not null) then

		select	count(*)
		into	qt_etapas_w
		from	prp_processo a,
			prp_processo_fase b,
			prp_processo_etapa c
		where	a.nr_sequencia = b.nr_seq_processo
		and	b.nr_sequencia = c.nr_seq_processo_fase
		and	a.nr_seq_projeto = nr_seq_projeto_p;

		if (qt_etapas_w > 0) then

		open C01;
		loop
		fetch C01 into
			nr_seq_processo_fase_w,
			ds_fase_w,
			nr_seq_fase_processo_w;
		exit when C01%notfound;
			begin

			select	proj_cronograma_seq.nextval
			into	nr_seq_cronograma_w
			from	dual;

			select	max(nr_seq_cliente),
				max(cd_gerente_cliente)
			into	nr_seq_cliente_w,
				cd_gerente_cliente_w
			from	proj_projeto
			where	nr_sequencia = nr_seq_projeto_p;
			insert into proj_cronograma (	nr_sequencia,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_atualizacao,
						nm_usuario,
						nr_seq_cliente,
						cd_empresa,
						cd_pessoa_cliente,
						ie_estrutura,
						nr_seq_proj,
						ie_tipo_cronograma,
						dt_inicio,
						ds_objetivo,
						ie_situacao,
						nr_seq_processo_fase,
						ie_classificacao)
					values (	nr_seq_cronograma_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_cliente_w,
						1,
						cd_gerente_cliente_w,
						'EL',
						nr_seq_projeto_p,
						'E',
						nvl(dt_inicio_prev_w,sysdate),
						ds_fase_w,
						'A',
						nr_seq_processo_fase_w,
						'N');

			if (ie_gerou_low_w <> true) then

				insert into prp_log_processo_fase (		nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_projeto,
									nr_seq_fase_processo)
								values	(prp_log_processo_fase_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_projeto_p,
									nr_seq_fase_processo_w);
				ie_gerou_low_w:= true;
			end if;

			open C02;
			loop
			fetch C02 into
				nr_seq_etapa_processo_w,
				nm_etapa_w,
				ds_objetivo_w,
				nr_seq_apresentacao_w,
				nr_Seq_processo_etapa_w;
			exit when C02%notfound;
				begin

				select	max(nr_seq_tipo_projeto)
				into	nr_seq_tipo_projeto_w
				from	proj_projeto
				where	nr_sequencia = nr_seq_projeto_p;
				
				select	nvl(max(qt_dias),1),
						nvl(max(qt_horas),1)
				into	qt_regra_tempo_w,
						qt_horas_w
				from	prp_regra_tempo_fase a,
					prp_regra_tempo_etapa b
				where	a.nr_sequencia = b.nr_seq_regra_fase
				and	b.nr_seq_etapa_processo = nr_seq_etapa_processo_w;
				--and	a.nr_seq_tipo_projeto = nvl(nr_seq_tipo_projeto_w,a.nr_seq_tipo_projeto);

				select	proj_cron_etapa_seq.nextval
				into	nr_seq_etapa_w
				from	dual;

				if ((qt_regra_tempo_w = 0) or (ie_data_nula_w = false)) then
				   ie_data_nula_w:= false;
				   dt_inicio_prev_w:= null;
				   dt_fim_prev_w:= null;
				end if;

				if (ie_data_nula_w) then

					dt_fim_prev_w:= (dt_inicio_prev_w+qt_regra_tempo_w);
					while (Obter_Se_Dia_Util(dt_fim_prev_w,cd_estabelecimento_w) = 'N') loop
						begin
						dt_fim_prev_w:=dt_fim_prev_w+1;
						end;
					end loop;

				end if;

				insert into proj_cron_etapa (		nr_sequencia,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								dt_atualizacao,
								nm_usuario,
								qt_hora_prev,
								pr_etapa,
								ie_modulo,
								ie_fase,
								nr_seq_apres,
								nr_seq_cronograma,
								ds_atividade,
								ie_tipo_ativ_met,
								nr_seq_superior,
								ds_etapa,
								dt_inicio_prev,
								dt_fim_prev,
								ie_obrigatorio,
								ie_gera_os,
								nr_seq_processo_etapa,
								ie_status_etapa,
								nr_seq_etapa_npi,
								qt_dias)
							values (	nr_seq_etapa_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								(qt_horas_w),
								0,
								'N',
								'S',
								nr_seq_apresentacao_w,
								nr_seq_cronograma_w,
								nm_etapa_w,
								null,
								null,
								ds_objetivo_w,
								null,
								null,
								'S',
								'S',
								nr_Seq_processo_etapa_w,
								'2',
								nr_seq_etapa_processo_w,
								qt_regra_tempo_w);

				open C03;
				loop
				fetch C03 into
					nr_seq_grupo_w,
					cd_pessoa_fisica_w;
				exit when C03%notfound;
					begin
					
					if (nr_seq_grupo_w in(4,77)) then /*Analista de Sistemas*/
						select	max(cd_coordenador)
						into  	cd_pessoa_fisica_w
						from	proj_projeto
						where	nr_sequencia = nr_seq_projeto_p;
					elsif (nr_seq_grupo_w = 5) then /*Analista de Sistemas*/
						select	max(a.cd_responsavel)
						into  	cd_pessoa_fisica_w
						from	gerencia_wheb a,
								proj_projeto b
						where	b.nr_seq_gerencia = a.nr_sequencia
						and		b.nr_sequencia = nr_seq_projeto_p;
					end if;
					
					insert into proj_cron_etapa_equipe (	nr_sequencia,
									nr_seq_etapa_cron,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									ie_recurso_externo,
									cd_programador)
							values	(	proj_cron_etapa_equipe_seq.nextval,
									nr_seq_etapa_w,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									'N',
									cd_pessoa_fisica_w);

					end;	
				end loop;
				close C03;

				end;
			end loop;
			close C02;

			end;
		end loop;
		close C01;

		end if;

	end if;

	commit;
	gera_datas_cronograma_npi(nr_seq_projeto_p, nm_usuario_p);
	
	end criar_cronograma_npi;

/*---------------------------------------------------------------------------------------------------------------------------------------------
|			ALTERA DATA DE INICIO DO CRONOGRAMA NPI						|
-----------------------------------------------------------------------------------------------------------------------------------------------*/

procedure alterar_datas_cronograma_npi(	nr_seq_cronograma_p	number,
										dt_inicio_p			date,
										ds_justificativa_p	varchar2,
										nm_usuario_p		varchar2) is

nr_seq_etapa_w			proj_cron_etapa.nr_sequencia%type;
nr_seq_etapa_processo_w	prp_etapa_processo.nr_sequencia%type;
nr_seq_cronograma_w 	proj_cronograma.nr_sequencia%type;
nr_seq_cron_etapa_w 	proj_cron_etapa.nr_sequencia%type;
--qt_hora_prev_w			proj_cron_etapa.qt_hora_prev%type;
qt_dias_w			proj_cron_etapa.qt_dias%type;
dt_inicio_w				date;
dt_fim_prev_w			date;
nr_seq_tipo_projeto_w	proj_projeto.nr_seq_tipo_projeto%type;
nr_seq_cliente_w		proj_projeto.nr_seq_cliente%type;
nr_seq_projeto_w		proj_projeto.nr_sequencia%type;

--Cronogramas do projeto ordenados conforme o mapa do projeto
Cursor C01 is
	select	nr_sequencia
	from	proj_cronograma
	where	nr_seq_proj = nr_seq_projeto_w
	and		nr_sequencia >= nr_seq_cronograma_p
	and		nr_seq_processo_fase is not null
	order by nr_seq_processo_fase;

--etapas do cronograma que n�o dependem de nenhuma outra etapa
Cursor C02 is
	select	a.nr_sequencia
	from	proj_cron_etapa a,
			prp_etapa_processo b
	where	a.nr_seq_etapa_npi = b.nr_sequencia
	and		a.nr_seq_cronograma = nr_seq_cronograma_w
	and	not exists (select	1 
					from	prp_etapa_dependencia x,
							proj_cron_etapa y,
							prp_etapa_processo z
					where	x.nr_seq_etapa_principal = b.nr_sequencia
					AND   y.nr_seq_etapa_npi = z.nr_sequencia
					AND x.nr_Seq_etapa_processo = z.nr_sequencia
					and		x.nr_seq_tipo_projeto = nr_seq_tipo_projeto_w
					AND a.nr_seq_cronograma = y.nr_Seq_cronograma);

begin

if (ds_justificativa_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(310927);
end if;

-- Pega a data inicial prevista do projeto, para o primeiro cronograma/atividades
select	max(a.nr_seq_tipo_projeto),
		max(a.nr_seq_cliente),
		max(a.nr_sequencia)
into	nr_seq_tipo_projeto_w,
		nr_seq_cliente_w,
		nr_seq_projeto_w
from	proj_projeto a,
		proj_cronograma b
where	a.nr_sequencia = b.nr_seq_proj
and		b.nr_sequencia = nr_seq_cronograma_p;


insert into com_cliente_hist(
		nr_sequencia,
		nr_seq_cliente,
		dt_atualizacao,
		nm_usuario,
		nr_seq_tipo,
		dt_historico,
		ds_historico,	
		nr_seq_projeto,
		ds_titulo,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_liberacao)
values	(com_cliente_hist_seq.nextval,
		nr_seq_cliente_w,
		sysdate,
		nm_usuario_p,
		38,
		sysdate,
		substr('Realizada a altera��o da data de in�cio do cronograma '|| nr_seq_cronograma_p ||' para '||to_char(dt_inicio_p,'dd/mm/yyyy')||chr(13)||chr(10)||
		'Justificativa: '||ds_justificativa_p,1,2000),
		nr_seq_projeto_w,
		'Altera��o de data do cronograma',
		sysdate,
		nm_usuario_p,
		sysdate);
commit;

dt_inicio_w := dt_inicio_p;

open C01;
loop
fetch C01 into	
	nr_seq_cronograma_w;
exit when C01%notfound;
	begin
	
	--Chama as atividades do cronograma
	open C02;
		loop
		fetch C02 into	
			nr_seq_cron_etapa_w;
		exit when C02%notfound;
			begin
			
			select	max(qt_dias)
			into	qt_dias_w
			from	proj_cron_etapa
			where	nr_sequencia = nr_seq_cron_etapa_w;
			
			dt_fim_prev_w := dt_inicio_w + (qt_dias_w) -1;
			
			if (pkg_date_utils.get_WeekDay(dt_fim_prev_w) = 7) then
				dt_fim_prev_w := dt_fim_prev_w+2;
			elsif (pkg_date_utils.get_WeekDay(dt_fim_prev_w) = 1) then
				dt_fim_prev_w := dt_fim_prev_w+1;
			end if;

			update	proj_cron_etapa
			set		dt_inicio_prev = dt_inicio_w,
					dt_fim_prev = OBTER_DATA_DIAS_UTEIS(1,dt_inicio_w,(qt_dias_w)-1)
			where	nr_sequencia = nr_seq_cron_etapa_w;

			commit;

			--verifica etapas dependentes
			gerar_datas_etapas_dep(nr_seq_cron_etapa_w,nm_usuario_p);
			
			end;
		end loop;
	close C02;

	select	max(dt_fim_prev)+1
	into	dt_inicio_w
	from	proj_cron_etapa
	where	nr_seq_cronograma = nr_seq_cronograma_w;
	
	seta_dt_inicio_fim(nr_seq_cronograma_w,nm_usuario_p);

	end;
end loop;
close C01;

end alterar_datas_cronograma_npi;

/*---------------------------------------------------------------------------------------------------------------------------------------------
|			RECALCULA DATAS DAS ETAPAS DO CRONOGRAMA NPI						|
-----------------------------------------------------------------------------------------------------------------------------------------------*/

procedure recalcula_cronograma_npi(	nr_seq_cronograma_p	number,
										nm_usuario_p		varchar2) is

nr_seq_etapa_w			proj_cron_etapa.nr_sequencia%type;
nr_seq_etapa_processo_w	prp_etapa_processo.nr_sequencia%type;
nr_seq_cronograma_w 	proj_cronograma.nr_sequencia%type;
nr_seq_cron_etapa_w 	proj_cron_etapa.nr_sequencia%type;
--qt_hora_prev_w			proj_cron_etapa.qt_hora_prev%type;
qt_dias_w				proj_cron_etapa.qt_dias%type;
dt_inicio_w				date;
dt_fim_prev_w			date;
nr_seq_tipo_projeto_w	proj_projeto.nr_seq_tipo_projeto%type;
nr_seq_cliente_w		proj_projeto.nr_seq_cliente%type;
nr_seq_projeto_w		proj_projeto.nr_sequencia%type;

--Cronogramas do projeto ordenados conforme o mapa do projeto
Cursor C01 is
	select	nr_sequencia
	from	proj_cronograma
	where	nr_seq_proj = nr_seq_projeto_w
	and		nr_sequencia >= nr_seq_cronograma_p
	and		nr_seq_processo_fase is not null
	order by nr_seq_processo_fase;

--etapas do cronograma que n�o dependem de nenhuma outra etapa
Cursor C02 is
	select	a.nr_sequencia
	from	proj_cron_etapa a,
			prp_etapa_processo b
	where	a.nr_seq_etapa_npi = b.nr_sequencia
	and		a.nr_seq_cronograma = nr_seq_cronograma_w
	and	not exists (select	1 
					from	prp_etapa_dependencia x,
							proj_cron_etapa y,
							prp_etapa_processo z
					where	x.nr_seq_etapa_principal = b.nr_sequencia
					AND   y.nr_seq_etapa_npi = z.nr_sequencia
					AND x.nr_Seq_etapa_processo = z.nr_sequencia
					and		x.nr_seq_tipo_projeto = nr_seq_tipo_projeto_w
					AND a.nr_seq_cronograma = y.nr_Seq_cronograma);

begin

-- Pega a data inicial prevista do projeto, para o primeiro cronograma/atividades
select	max(a.nr_seq_tipo_projeto),
		max(a.nr_seq_cliente),
		max(a.nr_sequencia),
		max(b.dt_inicio)
into	nr_seq_tipo_projeto_w,
		nr_seq_cliente_w,
		nr_seq_projeto_w,
		dt_inicio_w
from	proj_projeto a,
		proj_cronograma b
where	a.nr_sequencia = b.nr_seq_proj
and		b.nr_sequencia = nr_seq_cronograma_p;


open C01;
loop
fetch C01 into	
	nr_seq_cronograma_w;
exit when C01%notfound;
	begin
	
	--Chama as atividades do cronograma
	open C02;
		loop
		fetch C02 into	
			nr_seq_cron_etapa_w;
		exit when C02%notfound;
			begin
			
			select	max(qt_dias)
			into	qt_dias_w
			from	proj_cron_etapa
			where	nr_sequencia = nr_seq_cron_etapa_w;
			
			dt_fim_prev_w := dt_inicio_w + (qt_dias_w) -1;
			
			if (pkg_date_utils.get_WeekDay(dt_fim_prev_w) = 7) then
				dt_fim_prev_w := dt_fim_prev_w+2;
			elsif (pkg_date_utils.get_WeekDay(dt_fim_prev_w) = 1) then
				dt_fim_prev_w := dt_fim_prev_w+1;
			end if;

			update	proj_cron_etapa
			set		dt_inicio_prev = dt_inicio_w,
					dt_fim_prev = OBTER_DATA_DIAS_UTEIS(1,dt_inicio_w,(qt_dias_w)-1)
			where	nr_sequencia = nr_seq_cron_etapa_w;

			commit;

			--verifica etapas dependentes
			gerar_datas_etapas_dep(nr_seq_cron_etapa_w,nm_usuario_p);
			
			end;
		end loop;
	close C02;

	select	max(dt_fim_prev)+1
	into	dt_inicio_w
	from	proj_cron_etapa
	where	nr_seq_cronograma = nr_seq_cronograma_w;

	seta_dt_inicio_fim(nr_seq_cronograma_w,nm_usuario_p);

	end;
end loop;
close C01;

end recalcula_cronograma_npi;

end cronograma_padrao_npi;
/
