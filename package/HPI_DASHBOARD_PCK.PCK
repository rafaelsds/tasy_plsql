create or replace package hpi_dashboard_pck as


type hpi_row is record (
	ie_informacao			varchar2(30),
	vl_resultado			number,
	ds_cor				varchar2(15)
);

type hpi_row_data is table of hpi_row;

function obter_folha_dashboard_hpi(nm_usuario_grupo_p varchar2, nr_seq_gerencia_p number, nr_seq_grupo_p number) return hpi_row_data pipelined;

end hpi_dashboard_pck;
/

create or replace package body hpi_dashboard_pck as

function obter_folha_dashboard_hpi(nm_usuario_grupo_p varchar2, nr_seq_gerencia_p number, nr_seq_grupo_p number) return hpi_row_data pipelined is
hpi_row_w		hpi_row;
nr_ordem_serv_w		number(10);
qt_min_prev_w		number(10);
qt_min_exec_w		number(10);
pr_consumido_w		number(10);
ie_status_w		varchar2(10);

begin



SELECT  max(nr_sequencia),
        max(qt_min_prev),
        max(qt_min_exec),
        max(pr_consumido),
        CASE
		WHEN (max(pr_consumido)) <= 50 THEN 'GREEN'
		WHEN (max(pr_consumido)) BETWEEN 50 AND 70 THEN 'YELLOW'
		WHEN (max(pr_consumido)) BETWEEN 70 AND 100 THEN 'ORNGE'
		WHEN (max(pr_consumido)) > 100 THEN 'RED'
        END ie_status
into	nr_ordem_serv_w,
	qt_min_prev_w,
	qt_min_exec_w,
	pr_consumido_w,
	ie_status_w
FROM    (
		SELECT 	a.nr_sequencia,
			man_obter_min_prev_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE) qt_min_prev,
			man_obter_min_real_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE) qt_min_exec,
			((man_obter_min_real_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE) * 100 ) / DECODE(man_obter_min_prev_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE),0,1,man_obter_min_prev_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE))) pr_consumido
		FROM    grupo_desenvolvimento b,
			man_ordem_servico_v a
		WHERE   a.nr_seq_grupo_des     = b.nr_sequencia
		AND     (b.nr_sequencia      = nr_seq_grupo_p OR NVL(nr_seq_grupo_p, 0) = 0)
		AND     b.nr_seq_gerencia     = nr_seq_gerencia_p
		AND     SUBSTR(obter_se_os_desenv(a.nr_sequencia),1,1) = 'S'
		AND     obter_nome_usuario_exec(a.nr_sequencia) = nm_usuario_grupo_p
		AND EXISTS (SELECT 1 FROM    man_ordem_servico_exec y WHERE y.nr_seq_ordem = a.nr_sequencia AND    y.dt_fim_execucao IS NULL )
		AND EXISTS (SELECT 1 FROM    man_ordem_serv_ativ y, man_ordem_ativ_prev x WHERE x.nr_seq_ordem_serv = a.nr_sequencia
							AND    x.nr_sequencia  = y.nr_seq_ativ_prev    AND    TRUNC(x.dt_prevista) = TRUNC(SYSDATE)    AND    TRUNC(y.dt_atividade) = TRUNC(SYSDATE)
							AND    y.dt_fim_atividade IS NULL )
		union
		SELECT 	a.nr_sequencia,
			man_obter_min_prev_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE) qt_min_prev,
			man_obter_min_real_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE) qt_min_exec,
			((man_obter_min_real_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE) * 100 ) / DECODE(man_obter_min_prev_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE),0,1,man_obter_min_prev_usuario(a.nr_sequencia,nm_usuario_grupo_p, SYSDATE))) pr_consumido		
		FROM	grupo_suporte b,
			man_ordem_servico_v a
		WHERE 	a.nr_seq_grupo_des     = b.nr_sequencia
		AND 	(b.nr_sequencia      = nr_seq_grupo_p	OR NVL(nr_seq_grupo_p, 0)      = 0)
		AND 	b.nr_seq_gerencia_sup     = nr_seq_gerencia_p
		AND 	SUBSTR(obter_se_os_desenv(a.nr_sequencia),1,1) = 'S'
		AND EXISTS	(SELECT 1 FROM	man_ordem_servico_exec y WHERE y.nr_seq_ordem = a.nr_sequencia AND	y.dt_fim_execucao IS NULL)
		AND EXISTS 	(SELECT 1 FROM	man_ordem_serv_ativ y,man_ordem_ativ_prev x WHERE x.nr_seq_ordem_serv = a.nr_sequencia
					AND	x.nr_sequencia  = y.nr_seq_ativ_prev AND	TRUNC(x.dt_prevista) = TRUNC(SYSDATE) AND	TRUNC(y.dt_atividade) = TRUNC(SYSDATE)
					AND	y.dt_fim_atividade IS NULL)
		);		

	


	
if	(nr_ordem_serv_w is null) then

	for req in ( 
			select 'NROS' ie_informacao,
				0 vl_resultado,  
				'GREEN' ds_cor
			from	dual
			union
			select 'QT_PREV' ie_informacao,
				0 vl_resultado,
				'GREEN' ds_cor
			from	dual
			union
			select 'QT_EXEC' ie_informacao,
				0 vl_resultado,
				'GREEN' ds_cor
			from	dual
			union 
			select 'PR_EXEC' ie_informacao,
				0 vl_resultado,
				'GREEN' ds_cor
			from	dual


	) loop
			begin
				hpi_row_w.ie_informacao := req.ie_informacao;
				hpi_row_w.vl_resultado	:= req.vl_resultado;
				hpi_row_w.ds_cor	:= req.ds_cor;
				pipe row (hpi_row_w);
			end;
		end loop;
else		
		
		
	for req in ( 
			select 'NROS' ie_informacao,
				nr_ordem_serv_w vl_resultado,  
				ie_status_w ds_cor
			from	dual
			union
			select 'QT_PREV' ie_informacao,
				qt_min_prev_w vl_resultado,
				ie_status_w ds_cor
			from	dual
			union
			select 'QT_EXEC' ie_informacao,
				qt_min_exec_w vl_resultado,
				ie_status_w ds_cor
			from	dual
			union 
			select 'PR_EXEC' ie_informacao,
				pr_consumido_w vl_resultado,
				ie_status_w ds_cor
			from	dual


	) loop
			begin
				hpi_row_w.ie_informacao := req.ie_informacao;
				hpi_row_w.vl_resultado	:= req.vl_resultado;
				hpi_row_w.ds_cor	:= req.ds_cor;
				pipe row (hpi_row_w);
			end;
		end loop;


end if;	

return;

end obter_folha_dashboard_hpi;	

end hpi_dashboard_pck;
/