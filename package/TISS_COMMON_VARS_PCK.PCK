create or replace package tiss_common_vars_pck as

type tiss_common_vars is record (
    item_sequence number
);

procedure reset_item_sequence;
procedure set_item_sequence(item_sequence_p number);
function get_item_sequence return number;
function get_item_sequence_default return number;
function get_all_vars return tiss_common_vars;

end tiss_common_vars_pck;
/

create or replace package body tiss_common_vars_pck as

tiss_var tiss_common_vars;

procedure reset_item_sequence is
begin
    set_item_sequence(null);
end;

procedure set_item_sequence(item_sequence_p number) is

begin
    tiss_var.item_sequence := item_sequence_p;
end;

function get_item_sequence return number is
begin
    return tiss_var.item_sequence;
end;

function get_item_sequence_default return number is
begin
    return nvl(tiss_var.item_sequence, 0);
end;

function get_all_vars return tiss_common_vars is
begin
    return tiss_var;
end;

end tiss_common_vars_pck;
/
