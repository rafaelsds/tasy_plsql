CREATE OR REPLACE PACKAGE PKG_VAP_REPORT AS
  --
  TYPE T_REC_ATEND_UNIDADE IS RECORD (CD_ESTABELECIMENTO   SETOR_ATENDIMENTO.CD_ESTABELECIMENTO%TYPE,
                                      CD_SETOR_ATENDIMENTO ATEND_PACIENTE_UNIDADE.CD_SETOR_ATENDIMENTO%TYPE,
                                      NR_ATENDIMENTO       ATEND_PACIENTE_UNIDADE.NR_ATENDIMENTO%TYPE,
                                      DT_ENTRADA_UNIDADE   DATE,
                                      DT_SAIDA             DATE);
  --
  TYPE T_TAB_ATEND_UNIDADE IS TABLE OF T_REC_ATEND_UNIDADE;
  --
  FUNCTION GET_TAB_ATEND_UNIDADE(P_START_DATE DATE,
                                 P_END_DATE   DATE,
                                 P_AGE        NUMBER,
                                 P_PROTOCOL   NUMBER DEFAULT 0)
    RETURN T_TAB_ATEND_UNIDADE
    PIPELINED;
  --
  FUNCTION GET_ESTAB_DT(P_ESTAB NUMBER, P_DT DATE) RETURN DATE;
  --
  FUNCTION GET_STAGE_COUNT(P_NR_ATEND NUMBER,
                           P_ESTAB    NUMBER,
                           P_DT_START DATE,
                           P_DT_END   DATE,
                           P_STAGE    NUMBER) RETURN NUMBER;
  --
  FUNCTION GET_STAGE_COMPLIANCE(P_NR_ATEND NUMBER,
                                P_ESTAB    NUMBER,
                                P_DT_START DATE,
                                P_DT_END   DATE,
                                P_STAGE    NUMBER) RETURN NUMBER;
  --
  FUNCTION GET_STAGE_NOT_INDIC(P_NR_ATEND NUMBER,
                               P_ESTAB    NUMBER,
                               P_DT_START DATE,
                               P_DT_END   DATE,
                               P_STAGE    NUMBER) RETURN NUMBER;
  --
  FUNCTION GET_PATIENT_NAME(P_NR_ATEND NUMBER) RETURN VARCHAR2;
  --
  FUNCTION GET_REFACTORY_PATIENT_NAME(NM_PATIENTE_P VARCHAR2)
    RETURN VARCHAR2;
  --
  PROCEDURE RUN(P_START_DATE    IN DATE DEFAULT SYSDATE,
                P_END_DATE      IN DATE DEFAULT SYSDATE,
                P_AGE           IN NUMBER DEFAULT 0,
                P_CLINICAL_UNIT IN VARCHAR2 DEFAULT '0',
                P_PROTOCOL      IN VARCHAR2 DEFAULT 'N',
                P_INCIDENCE     IN VARCHAR2 DEFAULT 'N');
  --
END PKG_VAP_REPORT;
/
CREATE OR REPLACE PACKAGE BODY PKG_VAP_REPORT AS

  PROCEDURE TABLE_CLEANING IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VAP_REPORT_INCIDENCE';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VAP_REPORT_RESUME';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VAP_REPORT';
  END TABLE_CLEANING;
  --
  FUNCTION GET_TAB_ATEND_UNIDADE(P_START_DATE DATE,
                                 P_END_DATE   DATE,
                                 P_AGE        NUMBER,
                                 P_PROTOCOL   NUMBER DEFAULT 0)
    RETURN T_TAB_ATEND_UNIDADE PIPELINED IS
    
    CURSOR C_UNIDADE_ATEND_VMI IS
      SELECT T.CD_ESTABELECIMENTO,
             T.CD_SETOR_ATENDIMENTO,
             T.NR_ATENDIMENTO,
             T.DT_ENTRADA_UNIDADE,
             LEAST(NVL(T.DT_SAIDA_UNIDADE, DT_SAIDA_CALC),
                   NVL(DT_SAIDA_CALC, T.DT_SAIDA_UNIDADE)) DT_SAIDA
        FROM (SELECT S.CD_ESTABELECIMENTO,
                     S.CD_SETOR_ATENDIMENTO,
                     U.NR_ATENDIMENTO,
                     U.DT_ENTRADA_UNIDADE,
                     U.DT_SAIDA_UNIDADE,
                     (SELECT MIN(B.DT_ENTRADA_UNIDADE)
                        FROM ATEND_PACIENTE_UNIDADE B
                       WHERE B.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                         AND B.DT_ENTRADA_UNIDADE > U.DT_ENTRADA_UNIDADE) DT_SAIDA_CALC
                FROM ATEND_PACIENTE_UNIDADE U, SETOR_ATENDIMENTO S
               WHERE U.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO
                 AND TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(S.CD_ESTABELECIMENTO,
                                                       U.DT_ENTRADA_UNIDADE),
                           'DD') <= P_END_DATE
                 AND EXISTS
                     (SELECT 1
                        FROM ATEND_PAC_DISPOSITIVO AD,
                             ATENDIMENTO_PACIENTE  PC
                       WHERE AD.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                         AND AD.NR_ATENDIMENTO = PC.NR_ATENDIMENTO
                         AND OBTER_IDADE_PF(PC.CD_PESSOA_FISICA,
                                            AD.DT_INSTALACAO,
                                            'A') >= P_AGE
                         AND TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(PC.CD_ESTABELECIMENTO,
                                                               AD.DT_INSTALACAO),
                                   'DD') <= P_END_DATE
                         AND NVL(TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(PC.CD_ESTABELECIMENTO,
                                                                   AD.DT_RETIRADA),
                                       'DD'),
                                   P_START_DATE) >= P_START_DATE
                         AND EXISTS
                             (SELECT 1
                                FROM DISPOSITIVO DP
                               WHERE AD.NR_SEQ_DISPOSITIVO = DP.NR_SEQUENCIA
                                 AND DP.IE_CLASSIF_DISP_NISS = 'VMI')
                         AND (P_PROTOCOL = 0 OR
                             (P_PROTOCOL <> 0 AND EXISTS
                              (SELECT 1
                                  FROM GQA_PROTOCOLO_PAC G
                                 WHERE G.NR_ATENDIMENTO = AD.NR_ATENDIMENTO
                                   AND G.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(1)
                                   AND G.IE_SITUACAO = 'A'
                                   AND G.DT_INCLUSAO <= NVL(AD.DT_RETIRADA, G.DT_INCLUSAO)
                                   AND AD.DT_INSTALACAO <= NVL(G.DT_TERMINO, AD.DT_INSTALACAO)))))) T
       WHERE NVL(TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(T.CD_ESTABELECIMENTO,
                                                   LEAST(NVL(T.DT_SAIDA_UNIDADE,
                                                             DT_SAIDA_CALC),
                                                         NVL(DT_SAIDA_CALC,
                                                             T.DT_SAIDA_UNIDADE))),
                       'DD'),
                 P_START_DATE) >= P_START_DATE;
  
  TYPE T_UNIDADE_ATEND_VMI IS TABLE OF C_UNIDADE_ATEND_VMI%ROWTYPE;
  R_UNIDADE_ATEND_VMI T_UNIDADE_ATEND_VMI;
  
  BEGIN 
    OPEN C_UNIDADE_ATEND_VMI;
    LOOP
      FETCH C_UNIDADE_ATEND_VMI BULK COLLECT INTO R_UNIDADE_ATEND_VMI LIMIT 1000;
      
        BEGIN
          FOR i IN R_UNIDADE_ATEND_VMI.FIRST..R_UNIDADE_ATEND_VMI.LAST LOOP
            PIPE ROW(R_UNIDADE_ATEND_VMI(i));  
          END LOOP;
        EXCEPTION WHEN OTHERS THEN
          NULL;
        END;  

      EXIT WHEN C_UNIDADE_ATEND_VMI%NOTFOUND; 
 
    END LOOP;
    CLOSE C_UNIDADE_ATEND_VMI;                                  
  
    RETURN;
  END GET_TAB_ATEND_UNIDADE;
  --
  FUNCTION GET_ESTAB_TZ(P_ESTAB NUMBER, P_DT DATE DEFAULT SYSDATE)
    RETURN VARCHAR2 IS
  
    IE_UTC_V VARCHAR(15);
  
  BEGIN
  
    BEGIN
      SELECT IE_UTC
        INTO IE_UTC_V
        FROM ESTAB_HORARIO_VERAO
       WHERE CD_ESTABELECIMENTO = P_ESTAB
         AND P_DT BETWEEN DT_INICIAL AND DT_FINAL;
    EXCEPTION
      WHEN OTHERS THEN
        IE_UTC_V := NULL;
    END;
      
    IF IE_UTC_V IS NULL THEN
    --
      BEGIN
        SELECT NVL(IE_UTC, TO_CHAR(SYSTIMESTAMP, 'TZR'))
          INTO IE_UTC_V
          FROM ESTABELECIMENTO
         WHERE CD_ESTABELECIMENTO = P_ESTAB;
      EXCEPTION
        WHEN OTHERS THEN
          IE_UTC_V := TO_CHAR(SYSTIMESTAMP, 'TZR');
      END;
    --
    END IF;
  
    RETURN IE_UTC_V;
  
  END GET_ESTAB_TZ;
  --
  FUNCTION GET_ESTAB_DT(P_ESTAB NUMBER, P_DT DATE) RETURN DATE IS
  
    ESTAB_TZ_V VARCHAR2(15);
    SYS_TZ_V   VARCHAR2(15);
    DT_V       DATE;
    TZ_V       TIMESTAMP WITH TIME ZONE;
    CMD        VARCHAR2(255);
  
  BEGIN
  
    IF P_DT IS NULL THEN 
    --
      DT_V := NULL;
    --
    ELSIF P_ESTAB IS NULL THEN
      DT_V := P_DT;
    --
    ELSE
    --  
      ESTAB_TZ_V := GET_ESTAB_TZ(P_ESTAB, P_DT);
      SYS_TZ_V   := TO_CHAR(SYSTIMESTAMP, 'TZR');
    
      IF ESTAB_TZ_V = SYS_TZ_V THEN
      --
        DT_V := P_DT;
      --
      ELSE
      --
        BEGIN
          
          CMD := 'SELECT TO_TIMESTAMP_TZ(TO_CHAR(:1,''DD/MM/YYYY HH24:MI:SS'')||'''||SYS_TZ_V||''',''DD/MM/YYYY HH24:MI:SSTZH:TZM'') AT TIME ZONE '''|| ESTAB_TZ_V ||''' FROM DUAL'; 
          
          EXECUTE IMMEDIATE CMD INTO TZ_V USING P_DT;
          
          DT_V := TO_DATE(TO_CHAR(TZ_V, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS');
          
        EXCEPTION WHEN OTHERS THEN
          
          DT_V := P_DT;
          
        END;
      --
      END IF;
    --
    END IF;
      
    RETURN DT_V;
  
  END GET_ESTAB_DT;
  --
  FUNCTION GET_STAGE_COUNT(P_NR_ATEND NUMBER,
                           P_ESTAB    NUMBER,
                           P_DT_START DATE,
                           P_DT_END   DATE,
                           P_STAGE    NUMBER) RETURN NUMBER IS
  
    NR NUMBER;
  
  BEGIN
  
    SELECT COUNT(DISTINCT DT_INICIO)
      INTO NR
      FROM GQA_PROTOCOLO_PAC P, GQA_PROTOCOLO_ETAPA_PAC E
     WHERE P.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(1)
       AND P.IE_SITUACAO = 'A'
       AND P.NR_ATENDIMENTO = P_NR_ATEND
       AND E.NR_SEQ_PROT_PAC = P.NR_SEQUENCIA
       AND E.NR_SEQ_ETAPA = PKG_REPORT_DATA.GET_STAGE(P_STAGE)
       AND GET_ESTAB_DT(P_ESTAB, E.DT_INICIO) BETWEEN P_DT_START AND P_DT_END;
        
    RETURN NR;
  
  END GET_STAGE_COUNT;
  --
  FUNCTION GET_STAGE_COMPLIANCE(P_NR_ATEND NUMBER,
                                P_ESTAB    NUMBER,
                                P_DT_START DATE,
                                P_DT_END   DATE,
                                P_STAGE    NUMBER) RETURN NUMBER IS
  
    NR NUMBER;
  
  BEGIN
  
    SELECT COUNT(DISTINCT DT_FIM)
      INTO NR
      FROM GQA_PROTOCOLO_PAC P, GQA_PROTOCOLO_ETAPA_PAC E
     WHERE P.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(1)
       AND P.IE_SITUACAO = 'A'
       AND P.NR_ATENDIMENTO = P_NR_ATEND
       AND E.NR_SEQ_PROT_PAC = P.NR_SEQUENCIA
       AND E.NR_SEQ_ETAPA = PKG_REPORT_DATA.GET_STAGE(P_STAGE)
       AND GET_ESTAB_DT(P_ESTAB, E.DT_INICIO) BETWEEN P_DT_START AND P_DT_END
       AND E.DT_FIM IS NOT NULL;
      
    RETURN NR;
  
  END GET_STAGE_COMPLIANCE;
  --
  FUNCTION GET_STAGE_NOT_INDIC(P_NR_ATEND NUMBER, 
                               P_ESTAB    NUMBER,
                               P_DT_START DATE,
                               P_DT_END   DATE,
                               P_STAGE NUMBER)
    RETURN NUMBER IS
  
    NR NUMBER;
  
  BEGIN
  
    SELECT COUNT(DISTINCT DT_INICIO)
      INTO NR
      FROM GQA_PROTOCOLO_PAC P, GQA_PROTOCOLO_ETAPA_PAC E
     WHERE P.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(1)
       AND P.IE_SITUACAO = 'A'
       AND P.NR_ATENDIMENTO = P_NR_ATEND
       AND E.NR_SEQ_PROT_PAC = P.NR_SEQUENCIA
       AND E.NR_SEQ_ETAPA = PKG_REPORT_DATA.GET_STAGE(P_STAGE)
       AND GET_ESTAB_DT(P_ESTAB, E.DT_INICIO) BETWEEN P_DT_START AND P_DT_END;
         
    IF NR = 0 THEN
      --
      NR := 1;
      --
    ELSE
      -- 
      NR := 0;
      --
    END IF;
    --
  
    RETURN NR;
  
  END GET_STAGE_NOT_INDIC;
  --
  FUNCTION GET_PATIENT_NAME(P_NR_ATEND NUMBER) RETURN VARCHAR2 IS
  
    RET VARCHAR2(255) := NULL;
  
  BEGIN
  
    IF P_NR_ATEND IS NOT NULL THEN
      BEGIN
        SELECT GET_REFACTORY_PATIENT_NAME(OBTER_NOME_PACIENTE(P_NR_ATEND))
          INTO RET
          FROM DUAL;
      EXCEPTION WHEN OTHERS THEN
        RET := NULL;
      END;
    END IF;
  
    RETURN RET;
  
  END GET_PATIENT_NAME;
  --
  FUNCTION GET_REFACTORY_PATIENT_NAME(NM_PATIENTE_P VARCHAR2) RETURN VARCHAR2 IS

    RET VARCHAR2(255) := NULL;

  BEGIN

    IF NM_PATIENTE_P IS NOT NULL THEN
      SELECT DECODE(INSTR(T.NM, ',', 1),
                    0,
                    DECODE(INSTR(T.NM, ' ', -1),
                           0,
                           T.NM,
                           SUBSTR(T.NM, INSTR(T.NM, ' ', -1) + 1) || ', ' ||
                           SUBSTR(T.NM, 1, INSTR(T.NM, ' ', -1) - 1)),
                    T.NM)
        INTO RET
        FROM (SELECT TRIM(NM_PATIENTE_P) NM FROM DUAL) T;
    END IF;

    RETURN RET;

  END GET_REFACTORY_PATIENT_NAME;
  --
  PROCEDURE PRE_PROCESS(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_AGE IN NUMBER,
    P_CLINICAL_UNIT IN VARCHAR2,
    P_PROTOCOL IN VARCHAR2,
    P_INCIDENCE IN VARCHAR2) IS
    --
    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
    --
    CURSOR C_VAP_RESUME IS
      SELECT M.NR_ATENDIMENTO,
             M.CD_ESTABELECIMENTO,
             M.CD_SETOR_ATENDIMENTO,
             M.DT_INSTALACAO,
             M.DT_RETIRADA,
             M.DT_INSTALACAO_ESTAB,
             M.DT_RETIRADA_ESTAB,
             M.DT_ENTRADA_SETOR,
             M.DT_SAIDA_SETOR,
             GREATEST(M.DT_INSTALACAO_ESTAB, M.DT_ENTRADA_SETOR) DT_INSTALACAO_SETOR,
             LEAST(NVL(M.DT_RETIRADA_ESTAB, M.DT_SAIDA_SETOR),
                   NVL(M.DT_SAIDA_SETOR, DT_RETIRADA_ESTAB)) DT_RETIRADA_SETOR
        FROM (SELECT DISTINCT UN.CD_ESTABELECIMENTO,
                              UN.CD_SETOR_ATENDIMENTO,
                              UN.NR_ATENDIMENTO,
                              APD.DT_INSTALACAO,
                              APD.DT_RETIRADA,
                              PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_INSTALACAO) DT_INSTALACAO_ESTAB,
                              PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_RETIRADA) DT_RETIRADA_ESTAB,
                              PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, UN.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                              PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, UN.DT_SAIDA) DT_SAIDA_SETOR                             
                FROM TABLE(GET_TAB_ATEND_UNIDADE(P_START_DATE, P_END_DATE, P_AGE, 1)) UN, 
                     ATEND_PAC_DISPOSITIVO  APD                                       
               WHERE UN.NR_ATENDIMENTO = APD.NR_ATENDIMENTO
                 AND EXISTS (SELECT 1 FROM DISPOSITIVO D WHERE D.NR_SEQUENCIA = APD.NR_SEQ_DISPOSITIVO AND D.IE_CLASSIF_DISP_NISS = 'VMI')
                 AND OBTER_IDADE_PF(OBTER_CD_PES_FIS_ATEND(APD.NR_ATENDIMENTO), APD.DT_INSTALACAO, 'A') >= P_AGE
                 AND TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_INSTALACAO), 'DD') <= P_END_DATE
                 AND NVL(TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_RETIRADA), 'DD'), P_START_DATE) >= P_START_DATE
                 AND (P_CLINICAL_UNIT = '0' OR OBTER_SE_CONTIDO(UN.CD_SETOR_ATENDIMENTO, P_CLINICAL_UNIT) = 'S')
                 AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND UN.CD_SETOR_ATENDIMENTO = US.CD_SETOR_ATENDIMENTO)
                 AND APD.DT_INSTALACAO <= NVL(UN.DT_SAIDA, APD.DT_INSTALACAO)                                                                    
                 AND UN.DT_ENTRADA_UNIDADE <= NVL(APD.DT_RETIRADA, UN.DT_ENTRADA_UNIDADE)
                 AND EXISTS(SELECT 1
                              FROM GQA_PROTOCOLO_PAC P
                             WHERE P.NR_ATENDIMENTO = APD.NR_ATENDIMENTO
                               AND P.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(1)
                               AND P.IE_SITUACAO = 'A'
                               AND P.DT_INCLUSAO <= NVL(APD.DT_RETIRADA, P.DT_INCLUSAO)
                               AND APD.DT_INSTALACAO <= NVL(P.DT_TERMINO, APD.DT_INSTALACAO))) M;
    --
    TYPE T_VAP_RESUME IS TABLE OF C_VAP_RESUME%ROWTYPE;
    R_VAP_RESUME T_VAP_RESUME;
    --
    CURSOR C_VAP_REPORT IS
      SELECT X.DT_ATEND,
             X.NR_ATENDIMENTO,
             X.CD_ESTABELECIMENTO,
             X.CD_SETOR_ATENDIMENTO,
             TRIM(SUBSTR(OBTER_NOME_SETOR(X.CD_SETOR_ATENDIMENTO), 1, 25)) CLINICAL_UNIT,
             LEAST(X.QT_ATENDIMENTO, 1440) QT_ATENDIMENTO,
             1 VENT_DAYS,
             X.DVT_COUNT,
             X.DVT_COMPLIANCE,
             X.SUD_COUNT,
             X.SUD_COMPLIANCE,
             X.HOB_COUNT,
             X.HOB_COMPLIANCE,
             X.HOLD_SED_COUNT,
             X.HOLD_SED_COMPLIANCE,
             X.WEANING_COUNT,
             X.WEANING_COMPLIANCE,
             X.ORALCARE_COUNT,
             X.ORALCARE_CMPL
        FROM (SELECT A.DT_ATEND,
                     A.NR_ATENDIMENTO,
                     A.CD_ESTABELECIMENTO,
                     A.CD_SETOR_ATENDIMENTO,
                     SUM(A.QT_ATENDIMENTO) QT_ATENDIMENTO,
                     SUM(A.DVT_COUNT) DVT_COUNT,
                     SUM(A.DVT_COMPLIANCE) DVT_COMPLIANCE,
                     SUM(A.SUD_COUNT) SUD_COUNT,
                     SUM(A.SUD_COMPLIANCE) SUD_COMPLIANCE,
                     SUM(A.HOB_COUNT) HOB_COUNT,
                     SUM(A.HOB_COMPLIANCE + A.HOB_JCAHO) HOB_COMPLIANCE,
                     SUM(A.HOLD_SED_COUNT) HOLD_SED_COUNT,
                     SUM(A.HOLD_SED_COMPLIANCE) HOLD_SED_COMPLIANCE,
                     SUM(A.WEANING_COUNT) WEANING_COUNT,
                     SUM(A.WEANING_COMPLIANCE) WEANING_COMPLIANCE,
                     SUM(A.ORALCARE_COUNT) ORALCARE_COUNT,
                     SUM(A.ORALCARE_CMPL) ORALCARE_CMPL
                FROM (SELECT T.DT_ATEND,
                             T.NR_ATENDIMENTO,
                             T.CD_ESTABELECIMENTO,
                             T.CD_SETOR_ATENDIMENTO,
                             T.DT_INSTALACAO,
                             T.DT_RETIRADA,
                             T.DT_INSTALACAO_ESTAB,
                             T.DT_RETIRADA_ESTAB,
                             T.DT_ENTRADA_SETOR,
                             T.DT_SAIDA_SETOR,
                             T.DT_ENTRADA_ATEND,
                             T.DT_SAIDA_ATEND,
                             T.QT_ATENDIMENTO,
                             DECODE(P_PROTOCOL, 
                                    'N', 
                                    0,
                                    'S',
                                    0,
                                    'H',
                                    0, 
                                    -- DVT_COUNT      
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 7), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COUNT(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 3),
                                           1)) DVT_COUNT,                             
                             DECODE(P_PROTOCOL, 
                                    'N', 
                                    0,
                                    'S',
                                    0,
                                    'H',
                                    0, 
                                    -- DVT_COMPLIANCE 
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 7), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COMPLIANCE(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 3),
                                           1)) DVT_COMPLIANCE,
                             DECODE(P_PROTOCOL, 
                                    'N', 
                                    0,
                                    'D',
                                    0,
                                    'H',
                                    0,
                                    -- SUD_COUNT
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 8), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COUNT(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 4),
                                           1)) SUD_COUNT,
                             DECODE(P_PROTOCOL, 
                                    'N', 
                                    0,
                                    'D',
                                    0,
                                    'H',
                                    0,
                                    -- SUD_COMPLIANCE
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 8), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COMPLIANCE(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 4),
                                           1)) SUD_COMPLIANCE,
                             DECODE(P_PROTOCOL,
                                    'N',
                                    0,
                                    'S',
                                    0,
                                    'D',
                                    0, 
                                    -- HOB_COUNT
                                    'J', --JCAHO
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 9), 
                                           0,
                                           DECODE(PKG_VAP_REPORT.GET_STAGE_COUNT(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 2), 0, 0, 1),
                                           1),
                                    --IHI / HOB
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 9), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COUNT(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 2),
                                           1)) HOB_COUNT,
                             DECODE(P_PROTOCOL,
                                    'N',
                                    0,
                                    'S',
                                    0,
                                    'D',
                                    0, 
                                    'J',
                                    0,
                                    -- HOB_COMPLIANCE
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 9), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COMPLIANCE(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 2),
                                           1)) HOB_COMPLIANCE,
                             DECODE(P_PROTOCOL,
                                    'I',
                                    -- HOLD_SEDATION_COUNT
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 10), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COUNT(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 6),
                                           1),
                                    0) HOLD_SED_COUNT,
                             DECODE(P_PROTOCOL,
                                    'I',
                                    -- HOLD_SEDATION_COMPLIANCE
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 10), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COMPLIANCE(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 6),
                                           1),
                                    0) HOLD_SED_COMPLIANCE,
                             DECODE(P_PROTOCOL,
                                    'I',
                                    -- WEANING_COUNT
                                    PKG_VAP_REPORT.GET_STAGE_COUNT(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 5),
                                    0) WEANING_COUNT,
                             DECODE(P_PROTOCOL,
                                    'I',
                                    -- WEANING_COMPLIANCE
                                    PKG_VAP_REPORT.GET_STAGE_COMPLIANCE(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 5),
                                    0) WEANING_COMPLIANCE,
                             DECODE(P_PROTOCOL,
                                    'I',
                                    -- ORALCARE_COUNT
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 12), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COUNT(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 13),
                                           1),
                                    0) ORALCARE_COUNT,
                             DECODE(P_PROTOCOL,
                                    'I',
                                    -- ORALCARE_COMPLIANCE
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 12), 
                                           0,
                                           PKG_VAP_REPORT.GET_STAGE_COMPLIANCE(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 13),
                                           1),
                                    0) ORALCARE_CMPL,
                             DECODE(P_PROTOCOL,
                                    'J',
                                    DECODE(PKG_VAP_REPORT.GET_STAGE_NOT_INDIC(T.NR_ATENDIMENTO, T.CD_ESTABELECIMENTO, T.DT_ENTRADA_ATEND, T.DT_SAIDA_ATEND, 9), 
                                           0,
                                           -- HOB_COMPLIANCE_JCAHO
                                           DECODE((SELECT COUNT(DISTINCT DT_FIM)
                                                     FROM GQA_PROTOCOLO_PAC P, GQA_PROTOCOLO_ETAPA_PAC E
                                                    WHERE P.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(1)
                                                      AND P.IE_SITUACAO = 'A'
                                                      AND P.NR_ATENDIMENTO = T.NR_ATENDIMENTO
                                                      AND E.NR_SEQ_PROT_PAC = P.NR_SEQUENCIA
                                                      AND E.NR_SEQ_ETAPA = PKG_REPORT_DATA.GET_STAGE(2)
                                                      AND PKG_VAP_REPORT.GET_ESTAB_DT(T.CD_ESTABELECIMENTO, E.DT_INICIO) BETWEEN T.DT_ENTRADA_ATEND AND T.DT_SAIDA_ATEND
                                                      AND E.DT_FIM IS NOT NULL
                                                      AND E.DT_FIM - 1/3 >= (SELECT MAX(EJ.DT_FIM)
                                                                               FROM GQA_PROTOCOLO_PAC PJ, GQA_PROTOCOLO_ETAPA_PAC EJ
                                                                              WHERE PJ.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(1)
                                                                                AND PJ.IE_SITUACAO = 'A'
                                                                                AND PJ.NR_ATENDIMENTO = T.NR_ATENDIMENTO
                                                                                AND EJ.NR_SEQ_PROT_PAC = PJ.NR_SEQUENCIA
                                                                                AND EJ.NR_SEQ_ETAPA = PKG_REPORT_DATA.GET_STAGE(2)
                                                                                AND TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(T.CD_ESTABELECIMENTO, EJ.DT_INICIO), 'DD') = T.DT_ATEND
                                                                                AND EJ.DT_FIM IS NOT NULL
                                                                                AND EJ.DT_FIM < E.DT_FIM)),
                                                  0,
                                                  0,
                                                  1),
                                           1),
                                    0) HOB_JCAHO                    
                        FROM (WITH DT_ATEND AS (SELECT level, P_END_DATE - level + 1 dt_atend
                                                  FROM DUAL
                                               CONNECT BY LEVEL <= P_END_DATE - P_START_DATE + 1)
                              SELECT DT.DT_ATEND,
                                     N.NR_ATENDIMENTO,
                                     N.CD_ESTABELECIMENTO,
                                     N.CD_SETOR_ATENDIMENTO,
                                     N.DT_INSTALACAO,
                                     N.DT_RETIRADA,
                                     N.DT_INSTALACAO_ESTAB,
                                     N.DT_RETIRADA_ESTAB,
                                     N.DT_ENTRADA_SETOR,
                                     N.DT_SAIDA_SETOR,
                                     N.DT_INSTALACAO_SETOR,
                                     N.DT_RETIRADA_SETOR,
                                     GREATEST(N.DT_INSTALACAO_SETOR, DT.DT_ATEND) DT_ENTRADA_ATEND,
                                     LEAST(NVL(N.DT_RETIRADA_SETOR, LEAST(DT.DT_ATEND + 1, PKG_VAP_REPORT.GET_ESTAB_DT(N.CD_ESTABELECIMENTO, SYSDATE))), LEAST(DT.DT_ATEND + 1, PKG_VAP_REPORT.GET_ESTAB_DT(N.CD_ESTABELECIMENTO, SYSDATE))) DT_SAIDA_ATEND,
                                     CASE 
                                       WHEN TRUNC(N.DT_INSTALACAO_SETOR, 'DD') = DT.DT_ATEND AND 
                                            NVL(TRUNC(N.DT_RETIRADA_SETOR, 'DD'), P_END_DATE) = DT.DT_ATEND THEN
                                         NVL(N.DT_RETIRADA_SETOR, LEAST(P_END_DATE + 1, PKG_VAP_REPORT.GET_ESTAB_DT(N.CD_ESTABELECIMENTO, SYSDATE))) - N.DT_INSTALACAO_SETOR
                                       WHEN TRUNC(N.DT_INSTALACAO_SETOR, 'DD') = DT.DT_ATEND THEN
                                         DT.DT_ATEND + 1 - N.DT_INSTALACAO_SETOR
                                       WHEN NVL(TRUNC(N.DT_RETIRADA_SETOR, 'DD'), P_END_DATE) = DT.DT_ATEND THEN
                                         NVL(N.DT_RETIRADA_SETOR, LEAST(P_END_DATE + 1, PKG_VAP_REPORT.GET_ESTAB_DT(N.CD_ESTABELECIMENTO, SYSDATE))) - DT_ATEND
                                       ELSE 1
                                     END * 24 * 60 QT_ATENDIMENTO
                                FROM W_VAP_REPORT_RESUME N,
                                     DT_ATEND DT  
                               WHERE DT.DT_ATEND BETWEEN TRUNC(N.DT_INSTALACAO_SETOR, 'DD') 
                                                     AND NVL(TRUNC(N.DT_RETIRADA_SETOR, 'DD'), P_END_DATE)) T) A
               GROUP BY A.DT_ATEND,
                        A.NR_ATENDIMENTO,
                        A.CD_ESTABELECIMENTO,
                        A.CD_SETOR_ATENDIMENTO) X;
    --
    TYPE T_VAP_REPORT IS TABLE OF C_VAP_REPORT%ROWTYPE;
    R_VAP_REPORT T_VAP_REPORT;                      
    --     
    CURSOR C_VAP_INCIDENCE IS
      SELECT X.MONTH,
             X.MONTH_NAME,
             X.YEAR,
             X.CD_ESTABELECIMENTO,
             X.CD_SETOR_ATENDIMENTO,
             X.CLINICAL_UNIT,
             X.NR_ATEND_COUNT,
             SUM(X.VENT_DAYS) VENT_DAYS,
             X.PROTOCOLS_COUNT,
             X.DIAGNOSIS_COUNT
        FROM (SELECT T.DT_ATEND,
                     T.MONTH,
                     T.MONTH_NAME,
                     T.YEAR,
                     T.CD_ESTABELECIMENTO,
                     T.CD_SETOR_ATENDIMENTO,
                     T.CLINICAL_UNIT,
                     T.NR_ATEND_COUNT,
                     COUNT(DISTINCT T.NR_ATENDIMENTO) VENT_DAYS,
                     T.PROTOCOLS_COUNT,
                     T.DIAGNOSIS_COUNT
                FROM (WITH DT_ATEND AS (SELECT level, P_END_DATE - level + 1 dt_atend
                                          FROM DUAL
                                       CONNECT BY LEVEL <= P_END_DATE - P_START_DATE + 1)
                      SELECT D.DT_ATEND,
                             N.* 
                        FROM (WITH VAP AS (SELECT T.MONTH,
                                                  T.MONTH_NAME,
                                                  T.YEAR,
                                                  GREATEST(TO_DATE(T.YEAR || T.MONTH, 'YYYYMM'), P_START_DATE) START_DATE,
                                                  LEAST(LAST_DAY(TO_DATE(T.YEAR || T.MONTH, 'YYYYMM')), P_END_DATE) END_DATE,
                                                  T.CD_SETOR_ATENDIMENTO,
                                                  T.CLINICAL_UNIT,
                                                  COUNT(DISTINCT T.NR_ATENDIMENTO) NR_ATEND_COUNT,
                                                  SUM(T.PROTOCOLS_COUNT) PROTOCOLS_COUNT,
                                                  SUM(T.DIAGNOSIS_COUNT) DIAGNOSIS_COUNT
                                             FROM (SELECT DISTINCT V.NR_MONTH MONTH,
                                                                   V.DS_MONTH MONTH_NAME,
                                                                   V.DS_YEAR YEAR,
                                                                   V.CD_SETOR_ATENDIMENTO,
                                                                   V.DS_SETOR_ATENDIMENTO CLINICAL_UNIT,
                                                                   R.NR_ATENDIMENTO,
                                                                   (SELECT COUNT(1)
                                                                      FROM GQA_PROTOCOLO_PAC GPP
                                                                     WHERE GPP.NR_ATENDIMENTO = R.NR_ATENDIMENTO
                                                                       AND GPP.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(1)
                                                                       AND GPP.IE_SITUACAO = 'A'
                                                                       AND GPP.DT_INCLUSAO <=
                                                                           NVL(R.DT_RETIRADA, GPP.DT_INCLUSAO)
                                                                       AND R.DT_INSTALACAO <=
                                                                           NVL(GPP.DT_TERMINO, R.DT_INSTALACAO)) PROTOCOLS_COUNT,
                                                                   (SELECT COUNT(1)
                                                                      FROM DIAGNOSTICO_DOENCA DD, CID_DOENCA CD
                                                                     WHERE DD.NR_ATENDIMENTO = R.NR_ATENDIMENTO
                                                                       AND DD.IE_TIPO_DIAGNOSTICO = 2
                                                                       AND DD.CD_DOENCA = CD.CD_DOENCA_CID
                                                                       AND CD.IE_DOENCA_REL = 12
                                                                       AND DD.DT_DIAGNOSTICO BETWEEN R.DT_INSTALACAO - 2 AND
                                                                           NVL(R.DT_RETIRADA, P_END_DATE)) DIAGNOSIS_COUNT
                                                    FROM W_VAP_REPORT_RESUME R, W_VAP_REPORT V
                                                   WHERE V.NR_ATENDIMENTO = R.NR_ATENDIMENTO
                                                     AND V.CD_SETOR_ATENDIMENTO = R.CD_SETOR_ATENDIMENTO
                                                     AND TO_DATE(V.DS_YEAR || V.NR_MONTH, 'YYYYMM') BETWEEN
                                                         TRUNC(R.DT_INSTALACAO_SETOR, 'MM') AND
                                                         NVL(TRUNC(R.DT_RETIRADA_SETOR, 'MM'),
                                                             TO_DATE(V.DS_YEAR || V.NR_MONTH, 'YYYYMM'))) T
                                           GROUP BY T.MONTH,
                                                    T.MONTH_NAME,
                                                    T.YEAR,
                                                    T.CD_SETOR_ATENDIMENTO,
                                                    T.CLINICAL_UNIT)
                              SELECT V.MONTH,
                                     V.MONTH_NAME,
                                     V.YEAR,
                                     V.START_DATE,
                                     V.END_DATE,
                                     V.CLINICAL_UNIT,
                                     V.NR_ATEND_COUNT,
                                     V.PROTOCOLS_COUNT,
                                     V.DIAGNOSIS_COUNT,
                                     U.*
                                FROM (SELECT M.NR_ATENDIMENTO,
                                             M.CD_ESTABELECIMENTO,
                                             M.CD_SETOR_ATENDIMENTO,
                                             GREATEST(M.DT_INSTALACAO_ESTAB, M.DT_ENTRADA_SETOR) DT_INSTALACAO_SETOR,
                                             LEAST(NVL(M.DT_RETIRADA_ESTAB, M.DT_SAIDA_SETOR),
                                                   NVL(M.DT_SAIDA_SETOR, DT_RETIRADA_ESTAB)) DT_RETIRADA_SETOR      
                                        FROM (SELECT DISTINCT UN.CD_ESTABELECIMENTO,
                                                              UN.CD_SETOR_ATENDIMENTO,
                                                              UN.NR_ATENDIMENTO,
                                                              PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_INSTALACAO) DT_INSTALACAO_ESTAB,
                                                              PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_RETIRADA) DT_RETIRADA_ESTAB,
                                                              PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, UN.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                                                              PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, UN.DT_SAIDA) DT_SAIDA_SETOR                              
                                                FROM TABLE(GET_TAB_ATEND_UNIDADE(P_START_DATE, P_END_DATE, P_AGE, 0)) UN, 
                                                     ATEND_PAC_DISPOSITIVO  APD                                       
                                               WHERE UN.NR_ATENDIMENTO = APD.NR_ATENDIMENTO
                                                 AND EXISTS (SELECT 1 FROM DISPOSITIVO D WHERE D.NR_SEQUENCIA = APD.NR_SEQ_DISPOSITIVO AND D.IE_CLASSIF_DISP_NISS = 'VMI')
                                                 AND EXISTS (SELECT 1 FROM W_VAP_REPORT_RESUME R WHERE R.CD_SETOR_ATENDIMENTO = UN.CD_SETOR_ATENDIMENTO)
                                                 AND OBTER_IDADE_PF(OBTER_CD_PES_FIS_ATEND(APD.NR_ATENDIMENTO), APD.DT_INSTALACAO, 'A') >= P_AGE
                                                 AND TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_INSTALACAO), 'DD') <= P_END_DATE
                                                 AND NVL(TRUNC(PKG_VAP_REPORT.GET_ESTAB_DT(UN.CD_ESTABELECIMENTO, APD.DT_RETIRADA), 'DD'), P_START_DATE) >= P_START_DATE
                                                 AND (P_CLINICAL_UNIT = '0' OR OBTER_SE_CONTIDO(UN.CD_SETOR_ATENDIMENTO, P_CLINICAL_UNIT) = 'S')
                                                 AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND UN.CD_SETOR_ATENDIMENTO = US.CD_SETOR_ATENDIMENTO)
                                                 AND APD.DT_INSTALACAO <= NVL(UN.DT_SAIDA, APD.DT_INSTALACAO)                                                                    
                                                 AND UN.DT_ENTRADA_UNIDADE <= NVL(APD.DT_RETIRADA, UN.DT_ENTRADA_UNIDADE)) M) U, 
                                     VAP V
                               WHERE U.CD_SETOR_ATENDIMENTO = V.CD_SETOR_ATENDIMENTO
                                 AND TRUNC(U.DT_INSTALACAO_SETOR, 'DD') <= V.END_DATE
                                 AND NVL(U.DT_RETIRADA_SETOR, V.START_DATE) >= V.START_DATE) N,
                             DT_ATEND D  
                       WHERE D.DT_ATEND BETWEEN N.START_DATE AND N.END_DATE
                         AND D.DT_ATEND BETWEEN TRUNC(N.DT_INSTALACAO_SETOR, 'DD') 
                                            AND NVL(TRUNC(N.DT_RETIRADA_SETOR, 'DD'), P_END_DATE)) T
               GROUP BY T.MONTH,
                        T.DT_ATEND,
                        T.MONTH_NAME,
                        T.YEAR,
                        T.CD_ESTABELECIMENTO,
                        T.CD_SETOR_ATENDIMENTO,
                        T.CLINICAL_UNIT,
                        T.NR_ATEND_COUNT,
                        T.PROTOCOLS_COUNT,
                        T.DIAGNOSIS_COUNT) X
       GROUP BY X.MONTH,
                X.MONTH_NAME,
                X.YEAR,
                X.CD_ESTABELECIMENTO,
                X.CD_SETOR_ATENDIMENTO,
                X.CLINICAL_UNIT,
                X.NR_ATEND_COUNT,
                X.PROTOCOLS_COUNT,
                X.DIAGNOSIS_COUNT;
    --
    TYPE T_VAP_INCIDENCE IS TABLE OF C_VAP_INCIDENCE%ROWTYPE;
    R_VAP_INCIDENCE T_VAP_INCIDENCE;                      
    --  
  BEGIN
    --
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    
    OPEN C_VAP_RESUME;
    LOOP
      FETCH C_VAP_RESUME BULK COLLECT INTO R_VAP_RESUME LIMIT 1000;
      
        BEGIN 
          FOR i IN R_VAP_RESUME.FIRST..R_VAP_RESUME.LAST LOOP
            INSERT INTO W_VAP_REPORT_RESUME (
              nr_atendimento, 
              cd_estabelecimento, 
              cd_setor_atendimento, 
              dt_instalacao, 
              dt_retirada, 
              dt_instalacao_estab, 
              dt_retirada_estab, 
              dt_entrada_setor,
              dt_saida_setor, 
              dt_instalacao_setor, 
              dt_retirada_setor,
              nm_usuario)
            VALUES (
              R_VAP_RESUME(i).NR_ATENDIMENTO, --nr_atendimento, 
              R_VAP_RESUME(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
              R_VAP_RESUME(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
              R_VAP_RESUME(i).DT_INSTALACAO, --dt_instalacao, 
              R_VAP_RESUME(i).DT_RETIRADA, --dt_retirada, 
              R_VAP_RESUME(i).DT_INSTALACAO_ESTAB, --dt_instalacao_estab, 
              R_VAP_RESUME(i).DT_RETIRADA_ESTAB, --dt_retirada_estab, 
              R_VAP_RESUME(i).DT_ENTRADA_SETOR, --dt_entrada_setor, 
              R_VAP_RESUME(i).DT_SAIDA_SETOR, --dt_saida_setor, 
              R_VAP_RESUME(i).DT_INSTALACAO_SETOR, --dt_instalacao_setor, 
              R_VAP_RESUME(i).DT_RETIRADA_SETOR, --dt_retirada_setor,
              NM_USUARIO_W); --nm_usuario
            END LOOP;          
          COMMIT;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
      EXIT WHEN C_VAP_RESUME%NOTFOUND; 
    END LOOP;
    CLOSE C_VAP_RESUME; 
    COMMIT;
    
    OPEN C_VAP_REPORT;
    LOOP
      FETCH C_VAP_REPORT BULK COLLECT INTO R_VAP_REPORT LIMIT 1000;
      
        BEGIN
          FOR i IN R_VAP_REPORT.FIRST..R_VAP_REPORT.LAST LOOP
            INSERT INTO W_VAP_REPORT (
              dt_atendimento, 
              nr_month, 
              ds_month,
              ds_year, 
              cd_estabelecimento,
              cd_setor_atendimento,
              nr_atendimento, 
              ds_setor_atendimento, 
              qt_atendimento, 
              nr_vent_days, 
              nr_dvt_count, 
              nr_dvt_compliance, 
              nr_sud_count, 
              nr_sud_compliance, 
              nr_hob_count, 
              nr_hob_compliance, 
              nr_hold_sed_count, 
              nr_hold_sed_cmpl, 
              nr_weaning_count, 
              nr_weaning_cmpl,
              nr_oralcare_count,
              nr_oralcare_cmpl,
              nm_usuario) 
            VALUES (
              R_VAP_REPORT(i).DT_ATEND, --dt_atendimento, 
              TO_CHAR(R_VAP_REPORT(i).DT_ATEND, 'MM'), --nr_month,
              TRIM(TO_CHAR(R_VAP_REPORT(i).DT_ATEND, 'Month', 'NLS_DATE_LANGUAGE = American')), --ds_month,           
              TO_CHAR(R_VAP_REPORT(i).DT_ATEND, 'YYYY'), --ds_year, 
              R_VAP_REPORT(i).CD_ESTABELECIMENTO, --cd_estabelecimento,
              R_VAP_REPORT(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento,
              R_VAP_REPORT(i).NR_ATENDIMENTO, --nr_atendimento,   
              R_VAP_REPORT(i).CLINICAL_UNIT, --ds_setor_atendimento,  
              R_VAP_REPORT(i).QT_ATENDIMENTO, --qt_atendimento, 
              R_VAP_REPORT(i).VENT_DAYS, --nr_vent_days, 
              R_VAP_REPORT(i).DVT_COUNT, --nr_dvt_count, 
              R_VAP_REPORT(i).DVT_COMPLIANCE, --nr_dvt_compliance, 
              R_VAP_REPORT(i).SUD_COUNT, --nr_sud_count, 
              R_VAP_REPORT(i).SUD_COMPLIANCE, --nr_sud_compliance, 
              R_VAP_REPORT(i).HOB_COUNT, --nr_hob_count, 
              R_VAP_REPORT(i).HOB_COMPLIANCE, --nr_hob_compliance, 
              R_VAP_REPORT(i).HOLD_SED_COUNT, --nr_hold_sed_count, 
              R_VAP_REPORT(i).HOLD_SED_COMPLIANCE, --nr_hold_sed_cmpl, 
              R_VAP_REPORT(i).WEANING_COUNT, --nr_weaning_count, 
              R_VAP_REPORT(i).WEANING_COMPLIANCE, --nr_weaning_cmpl,
              R_VAP_REPORT(i).ORALCARE_COUNT, --nr_oralcare_count,
              R_VAP_REPORT(i).ORALCARE_CMPL, --nr_oralcare_cmpl,
              NM_USUARIO_W); --nm_usuario 
            END LOOP;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
      EXIT WHEN C_VAP_REPORT%NOTFOUND; 
    END LOOP;
    CLOSE C_VAP_REPORT;        
    COMMIT;

    IF P_INCIDENCE = 'S' THEN
      --
      OPEN C_VAP_INCIDENCE;
      LOOP
        FETCH C_VAP_INCIDENCE BULK COLLECT INTO R_VAP_INCIDENCE LIMIT 1000;
          
          BEGIN
            FOR i IN R_VAP_INCIDENCE.FIRST..R_VAP_INCIDENCE.LAST LOOP
              INSERT INTO W_VAP_REPORT_INCIDENCE
                (ds_month, 
                 nm_month, 
                 ds_year, 
                 cd_estabelecimento, 
                 cd_setor_atendimento, 
                 ds_setor_atendimento, 
                 qt_atendimento, 
                 nr_vent_days,  
                 nr_diagnosis, 
                 nr_protocols,
                 nm_usuario)
              VALUES (
                 R_VAP_INCIDENCE(i).MONTH, --ds_month, 
                 R_VAP_INCIDENCE(i).MONTH_NAME, --nm_month, 
                 R_VAP_INCIDENCE(i).YEAR, --ds_year, 
                 R_VAP_INCIDENCE(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                 R_VAP_INCIDENCE(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
                 R_VAP_INCIDENCE(i).CLINICAL_UNIT, --ds_setor_atendimento, 
                 R_VAP_INCIDENCE(i).NR_ATEND_COUNT, --qt_atendimento, 
                 R_VAP_INCIDENCE(i).VENT_DAYS, --nr_vent_days,  
                 R_VAP_INCIDENCE(i).DIAGNOSIS_COUNT, --nr_diagnosis, 
                 R_VAP_INCIDENCE(i).PROTOCOLS_COUNT, --nr_protocols,
                 NM_USUARIO_W); --nm_usuario 
            END LOOP;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        EXIT WHEN C_VAP_INCIDENCE%NOTFOUND; 
      END LOOP;               
      CLOSE C_VAP_INCIDENCE;
      COMMIT;
      --
    END IF;
  END PRE_PROCESS;
  -- 
  PROCEDURE RUN(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_AGE IN NUMBER,
    P_CLINICAL_UNIT IN VARCHAR2,
    P_PROTOCOL IN VARCHAR2,
    P_INCIDENCE IN VARCHAR2) IS
  BEGIN  
    TABLE_CLEANING;
    PRE_PROCESS(P_START_DATE, LEAST(P_END_DATE, TRUNC(SYSDATE, 'DD')), P_AGE, P_CLINICAL_UNIT, P_PROTOCOL, P_INCIDENCE);
  END RUN;
END PKG_VAP_REPORT;
/
