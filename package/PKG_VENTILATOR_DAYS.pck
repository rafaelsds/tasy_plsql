CREATE OR REPLACE PACKAGE PKG_VENTILATOR_DAYS AS
  --
  TYPE T_TAB_CD_PES_FIS IS TABLE OF PESSOA_FISICA.CD_PESSOA_FISICA%TYPE;
  --
  FUNCTION GET_TAB_CD_PES_FIS(P_START_DATE DATE,
                              P_END_DATE   DATE,
                              P_YEAR       NUMBER,
                              P_QUARTER    NUMBER,
                              P_ESTAB      NUMBER,
                              P_SETOR      NUMBER)
    RETURN T_TAB_CD_PES_FIS
    PIPELINED;  
  --
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0,
                P_YEAR       IN NUMBER,
                P_QUARTER    IN NUMBER DEFAULT 1,
                P_TYPE       IN NUMBER DEFAULT 1,
                P_PATIENT    IN VARCHAR2);
  --
END PKG_VENTILATOR_DAYS;
/
CREATE OR REPLACE PACKAGE BODY PKG_VENTILATOR_DAYS AS

  PROCEDURE TABLE_CLEANING IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VENTILATOR_DAYS';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VENTILATOR_DAYS_ESTAB';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VENTILATOR_DAYS_UNIT';
  END TABLE_CLEANING;
  --
  FUNCTION GET_TAB_CD_PES_FIS(P_START_DATE DATE,
                              P_END_DATE   DATE,
                              P_YEAR       NUMBER,
                              P_QUARTER    NUMBER,
                              P_ESTAB      NUMBER,
                              P_SETOR      NUMBER)
                              
    RETURN T_TAB_CD_PES_FIS PIPELINED IS
    
  V_START_DATE DATE;
  V_END_DATE   DATE;
  NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
  
  CURSOR C_CD_PES_FIS IS
    SELECT DISTINCT UN.CD_PESSOA_FISICA
      FROM (SELECT NR_ATENDIMENTO,
                   CD_PESSOA_FISICA,
                   CD_ESTABELECIMENTO,
                   CD_SETOR_ATENDIMENTO,
                   DT_INI,
                   DT_FIM,
                   DT_ENTRADA,
                   DT_ALTA,
                   DT_ENTRADA_SETOR,
                   DT_SAIDA_SETOR
              FROM (SELECT U.NR_ATENDIMENTO,
                           AP.CD_PESSOA_FISICA,
                           S.CD_ESTABELECIMENTO,
                           U.CD_SETOR_ATENDIMENTO,
                           PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE) DT_INI,
                           PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(V_END_DATE + 1, SYSDATE)) DT_FIM,
                           AP.DT_ENTRADA,
                           AP.DT_ALTA,
                           MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                           PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                U.DT_ENTRADA_UNIDADE,
                                                                U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR                                              
                      FROM (SELECT DISTINCT P.NR_ATENDIMENTO, 
                                            P.CD_PESSOA_FISICA,
                                            P.DT_ENTRADA,
                                            P.DT_ALTA 
                              FROM ATEND_PAC_DISPOSITIVO A,
                                   ATENDIMENTO_PACIENTE P 
                             WHERE A.NR_ATENDIMENTO = P.NR_ATENDIMENTO   
                               AND A.DT_INSTALACAO <= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                               AND (A.DT_RETIRADA IS NULL OR
                                    A.DT_RETIRADA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, V_START_DATE))
                               AND P.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                               AND (P.DT_ALTA IS NULL OR
                                    P.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, V_START_DATE))
                               AND EXISTS 
                                   (SELECT 1
                                      FROM DISPOSITIVO D
                                     WHERE D.NR_SEQUENCIA = A.NR_SEQ_DISPOSITIVO
                                       AND D.IE_CLASSIF_DISP_NISS = 'VMI')) AP,
                           ATEND_PACIENTE_UNIDADE U,
                           SETOR_ATENDIMENTO S
                     WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                       AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                       AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO)   
                       AND DECODE(NVL(P_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_ESTAB, 0)
                       AND DECODE(NVL(P_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_SETOR, 0)
                       AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                       AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                       AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                       AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                    U.DT_ENTRADA_UNIDADE,
                                                                    U.CD_SETOR_ATENDIMENTO),
                               PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)) >=
                           PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)
                     GROUP BY U.NR_ATENDIMENTO,
                              AP.CD_PESSOA_FISICA,
                              S.CD_ESTABELECIMENTO,
                              U.CD_SETOR_ATENDIMENTO,
                              AP.DT_ENTRADA,
                              AP.DT_ALTA,
                              PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                   U.DT_ENTRADA_UNIDADE,
                                                                   U.CD_SETOR_ATENDIMENTO)) UN
             WHERE (DT_ALTA IS NULL OR DT_ENTRADA_SETOR <= DT_ALTA)
               AND (DT_SAIDA_SETOR IS NULL OR DT_SAIDA_SETOR >= DT_ENTRADA)) UN,
           ATEND_PAC_DISPOSITIVO AD
     WHERE AD.NR_ATENDIMENTO = UN.NR_ATENDIMENTO
       AND EXISTS 
           (SELECT 1
              FROM DISPOSITIVO D
             WHERE D.NR_SEQUENCIA = AD.NR_SEQ_DISPOSITIVO
               AND D.IE_CLASSIF_DISP_NISS = 'VMI')
       AND AD.DT_INSTALACAO <= LEAST(UN.DT_FIM, NVL(UN.DT_SAIDA_SETOR, UN.DT_FIM))
       AND NVL(AD.DT_RETIRADA, GREATEST(UN.DT_ENTRADA_SETOR, UN.DT_INI)) >= GREATEST(UN.DT_ENTRADA_SETOR, UN.DT_INI);
  
  TYPE T_CD_PES_FIS IS TABLE OF C_CD_PES_FIS%ROWTYPE;
  R_CD_PES_FIS T_CD_PES_FIS;
  
  BEGIN
  
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
  
    IF P_YEAR IS NOT NULL AND P_QUARTER IS NOT NULL THEN
    
      SELECT GREATEST(INI, NVL(P_START_DATE, INI)) INI,
             LEAST(FIM, NVL(P_END_DATE, FIM)) FIM
        INTO V_START_DATE,
             V_END_DATE
        FROM (SELECT TO_DATE(LPAD((P_QUARTER * 3) - 2, 2, '0') || P_YEAR, 'MMYYYY') INI,
                     LAST_DAY(TO_DATE(LPAD((P_QUARTER * 3), 2, '0') || P_YEAR, 'MMYYYY')) FIM
                FROM DUAL)
       WHERE INI <= NVL(P_END_DATE, INI)
         AND FIM >= NVL(P_START_DATE, FIM);
  
      OPEN C_CD_PES_FIS;
      LOOP
        FETCH C_CD_PES_FIS BULK COLLECT INTO R_CD_PES_FIS LIMIT 1000;
          
          BEGIN
            FOR i IN R_CD_PES_FIS.FIRST..R_CD_PES_FIS.LAST LOOP
              PIPE ROW(R_CD_PES_FIS(i).CD_PESSOA_FISICA);  
            END LOOP;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;  

        EXIT WHEN C_CD_PES_FIS%NOTFOUND; 
     
      END LOOP;
      CLOSE C_CD_PES_FIS;                                  
  
    END IF;
    
    RETURN;
  END GET_TAB_CD_PES_FIS;
  --
  PROCEDURE PRE_PROCESS(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_CD_ESTAB IN NUMBER,
    P_CD_SETOR IN NUMBER,
    P_YEAR IN NUMBER,
    P_QUARTER IN NUMBER,
    P_TYPE IN NUMBER,
    P_PATIENT IN VARCHAR2) IS
    --
    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
    --
    CURSOR C_VENTILATOR_DAYS (V_START_DATE DATE,
                              V_END_DATE   DATE,
                              V_SETOR      NUMBER,
                              V_PATIENT    VARCHAR2) IS 
      SELECT UN.NR_ATENDIMENTO,
             UN.CD_ESTABELECIMENTO,
             UN.CD_SETOR_ATENDIMENTO,
             AD.NR_SEQUENCIA NR_SEQ_ATEND_DISP,
             UN.DT_ENTRADA_SETOR,
             UN.DT_SAIDA_SETOR,
             GREATEST(AD.DT_INSTALACAO, GREATEST(UN.DT_ENTRADA_SETOR, UN.DT_INI)) DT_ENTRADA_DISP,
             LEAST(LEAST(UN.DT_FIM, NVL(UN.DT_SAIDA_SETOR, UN.DT_FIM)),
                   NVL(AD.DT_RETIRADA, LEAST(UN.DT_FIM, NVL(UN.DT_SAIDA_SETOR, UN.DT_FIM)))) DT_SAIDA_DISP
        FROM (SELECT NR_ATENDIMENTO,
                     CD_ESTABELECIMENTO,
                     CD_SETOR_ATENDIMENTO,
                     DT_INI,
                     DT_FIM,
                     GREATEST(DT_ENTRADA_SETOR, DT_ENTRADA - .25) DT_ENTRADA_SETOR,
                     LEAST(NVL(DT_SAIDA_SETOR, NVL2(DT_ALTA, DT_ALTA + .25, NULL)),
                           NVL2(DT_ALTA, DT_ALTA + .25, DT_SAIDA_SETOR)) DT_SAIDA_SETOR
                FROM (SELECT U.NR_ATENDIMENTO,
                             S.CD_ESTABELECIMENTO,
                             U.CD_SETOR_ATENDIMENTO,
                             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE) DT_INI,
                             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(V_END_DATE + 1, SYSDATE)) DT_FIM,
                             AP.DT_ENTRADA,
                             AP.DT_ALTA,
                             MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                             PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                  U.DT_ENTRADA_UNIDADE,
                                                                  U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR                                              
                        FROM (SELECT DISTINCT P.NR_ATENDIMENTO,
                                              P.DT_ENTRADA,
                                              P.DT_ALTA
                                FROM ATEND_PAC_DISPOSITIVO A,
                                     ATENDIMENTO_PACIENTE P 
                               WHERE A.NR_ATENDIMENTO = P.NR_ATENDIMENTO   
                                 AND (V_PATIENT IS NULL OR V_PATIENT = P.CD_PESSOA_FISICA)
                                 AND A.DT_INSTALACAO <= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                 AND (A.DT_RETIRADA IS NULL OR
                                      A.DT_RETIRADA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, V_START_DATE))
                                 AND P.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                 AND (P.DT_ALTA IS NULL OR
                                      P.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(P.CD_ESTABELECIMENTO, V_START_DATE))
                                 AND EXISTS 
                                     (SELECT 1
                                        FROM DISPOSITIVO D
                                       WHERE D.NR_SEQUENCIA = A.NR_SEQ_DISPOSITIVO
                                         AND D.IE_CLASSIF_DISP_NISS = 'VMI')) AP,
                             ATEND_PACIENTE_UNIDADE U,
                             SETOR_ATENDIMENTO S
                       WHERE AP.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                         AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                         AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO)
                         AND DECODE(P_CD_ESTAB, 0, 0, S.CD_ESTABELECIMENTO) = P_CD_ESTAB
                         AND DECODE(V_SETOR, 0, 0, U.CD_SETOR_ATENDIMENTO) = V_SETOR
                         AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                         AND S.CD_CLASSIF_SETOR NOT IN ('6', '7', '10')
                         AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))         
                         AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                      U.DT_ENTRADA_UNIDADE,
                                                                      U.CD_SETOR_ATENDIMENTO),
                                 PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)) >=
                             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)
                       GROUP BY U.NR_ATENDIMENTO,
                                S.CD_ESTABELECIMENTO,
                                U.CD_SETOR_ATENDIMENTO,
                                AP.DT_ENTRADA,
                                AP.DT_ALTA,
                                PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                     U.DT_ENTRADA_UNIDADE,
                                                                     U.CD_SETOR_ATENDIMENTO)) UN
               WHERE (DT_ALTA IS NULL OR DT_ENTRADA_SETOR <= DT_ALTA)
                 AND (DT_SAIDA_SETOR IS NULL OR DT_SAIDA_SETOR >= DT_ENTRADA)) UN,
             ATEND_PAC_DISPOSITIVO AD
       WHERE AD.NR_ATENDIMENTO = UN.NR_ATENDIMENTO
         AND EXISTS 
             (SELECT 1
                FROM DISPOSITIVO D
               WHERE D.NR_SEQUENCIA = AD.NR_SEQ_DISPOSITIVO
                 AND D.IE_CLASSIF_DISP_NISS = 'VMI')
         AND AD.DT_INSTALACAO <= LEAST(UN.DT_FIM, NVL(UN.DT_SAIDA_SETOR, UN.DT_FIM))
         AND NVL(AD.DT_RETIRADA, GREATEST(UN.DT_ENTRADA_SETOR, UN.DT_INI)) >= GREATEST(UN.DT_ENTRADA_SETOR, UN.DT_INI);
    --
    TYPE T_VENTILATOR_DAYS IS TABLE OF C_VENTILATOR_DAYS%ROWTYPE;
    R_VENTILATOR_DAYS T_VENTILATOR_DAYS;
    --
    CURSOR C_VENTILATOR_DAYS_ESTAB IS
      SELECT C.CD_ESTABELECIMENTO,
             '' ESTAB_VALUES,
             0 TOTAL_STAYS,
             0 TOTAL_DAYS,
             C.VENT_STAYS,
             C.VENT_DAYS,
             ROUND(C.VENT_DAYS / C.VENT_STAYS, 1) VENT_AVG,
             C.VENT_MEDIAN,
             0 VENT_RATE
        FROM (SELECT B.CD_ESTABELECIMENTO,
                     SUM(B.VENT_STAYS) VENT_STAYS,
                     SUM(B.VENT_DAYS) VENT_DAYS,
                     ROUND(MEDIAN(B.VENT_DAYS), 1) VENT_MEDIAN
                FROM (SELECT A.CD_ESTABELECIMENTO,
                             A.CD_SETOR_ATENDIMENTO,
                             A.NR_ATENDIMENTO,
                             A.DT_ENTRADA_SETOR,
                             A.DT_SAIDA_SETOR,
                             1 VENT_STAYS,
                             SUM(A.DT_SAIDA_DISP - A.DT_ENTRADA_DISP) VENT_DAYS
                        FROM W_VENTILATOR_DAYS A
                       WHERE A.NR_QUARTER = 4
                       GROUP BY A.CD_ESTABELECIMENTO,
                                A.CD_SETOR_ATENDIMENTO,
                                A.NR_ATENDIMENTO,
                                A.DT_ENTRADA_SETOR,
                                A.DT_SAIDA_SETOR) B
               GROUP BY B.CD_ESTABELECIMENTO) C;
    --
    TYPE T_VENTILATOR_DAYS_ESTAB IS TABLE OF C_VENTILATOR_DAYS_ESTAB%ROWTYPE;
    R_VENTILATOR_DAYS_ESTAB T_VENTILATOR_DAYS_ESTAB;
    --
    CURSOR C_VENTILATOR_DAYS_UNIT(V_QUARTER NUMBER) IS 
      SELECT E.CD_ESTABELECIMENTO,
             E.CD_SETOR_ATENDIMENTO,
             E.QUARTER,
             '' UNIT_VALUES,
             0 TOTAL_STAYS,
             0 TOTAL_DAYS,
             E.VENT_STAYS,
             E.VENT_DAYS,
             ROUND(DECODE(E.VENT_STAYS, 0, 0, E.VENT_DAYS / E.VENT_STAYS), 1) VENT_AVG,
             E.VENT_MEDIAN,
             0 VENT_RATE
        FROM (SELECT D.CD_ESTABELECIMENTO,
                     D.CD_SETOR_ATENDIMENTO,
                     D.QUARTER,
                     SUM(D.VENT_STAYS) VENT_STAYS,
                     SUM(D.VENT_DAYS) VENT_DAYS,
                     ROUND(MEDIAN(D.VENT_DAYS), 1) VENT_MEDIAN
                FROM (SELECT B.CD_ESTABELECIMENTO,
                             B.CD_SETOR_ATENDIMENTO,
                             B.NR_QUARTER QUARTER,
                             C.NR_ATENDIMENTO,
                             C.DT_ENTRADA_SETOR,
                             C.DT_SAIDA_SETOR,
                             NVL2(C.NR_ATENDIMENTO, 1, 0) VENT_STAYS,
                             NVL(SUM(C.DT_SAIDA_DISP - C.DT_ENTRADA_DISP), 0) VENT_DAYS
                        FROM (SELECT DISTINCT A.CD_ESTABELECIMENTO,
                                              A.CD_SETOR_ATENDIMENTO,
                                              V_QUARTER NR_QUARTER
                                FROM W_VENTILATOR_DAYS A
                               WHERE DECODE(P_CD_SETOR, 0, 0, A.CD_SETOR_ATENDIMENTO) = P_CD_SETOR) B
                        LEFT JOIN W_VENTILATOR_DAYS C 
                               ON B.NR_QUARTER = C.NR_QUARTER
                              AND B.CD_SETOR_ATENDIMENTO = C.CD_SETOR_ATENDIMENTO
                       GROUP BY B.CD_ESTABELECIMENTO,
                                B.CD_SETOR_ATENDIMENTO,
                                B.NR_QUARTER,
                                C.NR_ATENDIMENTO,
                                C.DT_ENTRADA_SETOR,
                                C.DT_SAIDA_SETOR) D
               GROUP BY D.CD_ESTABELECIMENTO,
                        D.CD_SETOR_ATENDIMENTO,
                        D.QUARTER) E;
    --
    TYPE T_VENTILATOR_DAYS_UNIT IS TABLE OF C_VENTILATOR_DAYS_UNIT%ROWTYPE;
    R_VENTILATOR_DAYS_UNIT T_VENTILATOR_DAYS_UNIT;
    --  
  BEGIN
    --
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    --
    IF P_TYPE = 1 THEN
      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT 1 QUARTER,
                               TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3) -2, 2, '0') || SUBSTR(PR, -4), 'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3), 2, '0') || SUBSTR(PR, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 2 QUARTER,
                               TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3) -2, 2, '0') || SUBSTR(SEG, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3), 2, '0') || SUBSTR(SEG, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 3 QUARTER,
                               TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3) -2, 2, '0') || SUBSTR(TER, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3), 2, '0') || SUBSTR(TER, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 4 QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE)
      LOOP

        OPEN C_VENTILATOR_DAYS(N.INI, N.FIM, CASE WHEN N.QUARTER = 4 THEN 0 ELSE P_CD_SETOR END, NULL);
        LOOP
          FETCH C_VENTILATOR_DAYS BULK COLLECT INTO R_VENTILATOR_DAYS LIMIT 1000;
        
            BEGIN
              FOR i IN R_VENTILATOR_DAYS.FIRST..R_VENTILATOR_DAYS.LAST LOOP
                INSERT INTO W_VENTILATOR_DAYS (
                  nr_quarter,
                  dt_ini_quarter,
                  dt_fim_quarter, 
                  nr_atendimento, 
                  cd_estabelecimento, 
                  cd_setor_atendimento, 
                  nr_seq_atend_disp, 
                  dt_entrada_setor, 
                  dt_saida_setor, 
                  dt_entrada_disp, 
                  dt_saida_disp,
                  nm_usuario)
                VALUES (
                  N.QUARTER, -- nr_quarter, 
                  N.INI, -- dt_ini_quarter, 
                  N.FIM, -- dt_fim_quarter, 
                  R_VENTILATOR_DAYS(i).NR_ATENDIMENTO, -- nr_atendimento, 
                  R_VENTILATOR_DAYS(i).CD_ESTABELECIMENTO, -- cd_estabelecimento, 
                  R_VENTILATOR_DAYS(i).CD_SETOR_ATENDIMENTO, -- cd_setor_atendimento, 
                  R_VENTILATOR_DAYS(i).NR_SEQ_ATEND_DISP, -- nr_seq_atend_disp, 
                  R_VENTILATOR_DAYS(i).DT_ENTRADA_SETOR, -- dt_entrada_setor, 
                  R_VENTILATOR_DAYS(i).DT_SAIDA_SETOR, -- dt_saida_setor, 
                  R_VENTILATOR_DAYS(i).DT_ENTRADA_DISP, -- dt_entrada_disp, 
                  R_VENTILATOR_DAYS(i).DT_SAIDA_DISP, -- dt_saida_disp,
                  NM_USUARIO_W); --nm_usuario_w
              END LOOP;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              NULL;
            END;
            COMMIT;
            
          EXIT WHEN C_VENTILATOR_DAYS%NOTFOUND; 
        END LOOP;
        CLOSE C_VENTILATOR_DAYS;

      END LOOP;
      
      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT 1 QUARTER,
                               TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3) -2, 2, '0') || SUBSTR(PR, -4), 'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3), 2, '0') || SUBSTR(PR, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 2 QUARTER,
                               TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3) -2, 2, '0') || SUBSTR(SEG, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3), 2, '0') || SUBSTR(SEG, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 3 QUARTER,
                               TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3) -2, 2, '0') || SUBSTR(TER, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3), 2, '0') || SUBSTR(TER, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 4 QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE) 
      LOOP
      
        OPEN C_VENTILATOR_DAYS_UNIT(N.QUARTER);
        LOOP
          FETCH C_VENTILATOR_DAYS_UNIT BULK COLLECT INTO R_VENTILATOR_DAYS_UNIT LIMIT 1000;            

            BEGIN
              FOR i IN R_VENTILATOR_DAYS_UNIT.FIRST..R_VENTILATOR_DAYS_UNIT.LAST LOOP
                    
                R_VENTILATOR_DAYS_UNIT(i).UNIT_VALUES := PKG_ATEND_PAC_UNID.GET_STAY_COUNT_AND_AMOUNT(N.INI, N.FIM, R_VENTILATOR_DAYS_UNIT(i).CD_ESTABELECIMENTO, R_VENTILATOR_DAYS_UNIT(i).CD_SETOR_ATENDIMENTO); 
                R_VENTILATOR_DAYS_UNIT(i).TOTAL_STAYS := TO_NUMBER(REGEXP_SUBSTR(R_VENTILATOR_DAYS_UNIT(i).UNIT_VALUES, '[^;]+', 1, 1));
                R_VENTILATOR_DAYS_UNIT(i).TOTAL_DAYS := TO_NUMBER(REGEXP_SUBSTR(R_VENTILATOR_DAYS_UNIT(i).UNIT_VALUES, '[^;]+', 1, 2));
                    
                IF R_VENTILATOR_DAYS_UNIT(i).VENT_DAYS <> 0 AND R_VENTILATOR_DAYS_UNIT(i).TOTAL_DAYS <> 0 THEN
                  R_VENTILATOR_DAYS_UNIT(i).VENT_RATE := ROUND(100 * R_VENTILATOR_DAYS_UNIT(i).VENT_DAYS / R_VENTILATOR_DAYS_UNIT(i).TOTAL_DAYS, 1);
                END IF;  
                    
                INSERT INTO W_VENTILATOR_DAYS_UNIT (
                  cd_estabelecimento,
                  cd_setor_atendimento,
                  nr_quarter, 
                  nr_total_stays, 
                  nr_total_days, 
                  nr_vent_stays, 
                  nr_vent_days, 
                  nr_vent_avg, 
                  nr_vent_median, 
                  nr_vent_rate,
                  nm_usuario)
                VALUES (
                  R_VENTILATOR_DAYS_UNIT(i).CD_ESTABELECIMENTO, -- cd_estabelecimento, 
                  R_VENTILATOR_DAYS_UNIT(i).CD_SETOR_ATENDIMENTO, -- cd_setor_atendimento,
                  N.QUARTER, -- nr_quarter,
                  R_VENTILATOR_DAYS_UNIT(i).TOTAL_STAYS, -- nr_total_stays, 
                  ROUND(R_VENTILATOR_DAYS_UNIT(i).TOTAL_DAYS, 1), -- nr_total_days, 
                  R_VENTILATOR_DAYS_UNIT(i).VENT_STAYS, -- nr_vent_stays, 
                  ROUND(R_VENTILATOR_DAYS_UNIT(i).VENT_DAYS, 1), -- nr_vent_days, 
                  R_VENTILATOR_DAYS_UNIT(i).VENT_AVG, -- nr_vent_avg, 
                  R_VENTILATOR_DAYS_UNIT(i).VENT_MEDIAN, -- nr_vent_median, 
                  R_VENTILATOR_DAYS_UNIT(i).VENT_RATE, -- nr_vent_rate,
                  NM_USUARIO_W); -- nm_usuario

              END LOOP;
                  
            EXCEPTION WHEN NO_DATA_FOUND THEN 
              NULL;
            END;
            COMMIT;
          EXIT WHEN C_VENTILATOR_DAYS_UNIT%NOTFOUND; 
        END LOOP;
        CLOSE C_VENTILATOR_DAYS_UNIT;
     
        IF N.QUARTER = 4 THEN
          OPEN C_VENTILATOR_DAYS_ESTAB;
          LOOP
            FETCH C_VENTILATOR_DAYS_ESTAB BULK COLLECT INTO R_VENTILATOR_DAYS_ESTAB LIMIT 1000;            
              BEGIN
                FOR i IN R_VENTILATOR_DAYS_ESTAB.FIRST..R_VENTILATOR_DAYS_ESTAB.LAST LOOP
                  
                  R_VENTILATOR_DAYS_ESTAB(i).ESTAB_VALUES := PKG_ATEND_PAC_UNID.GET_STAY_COUNT_AND_AMOUNT(N.INI, N.FIM, R_VENTILATOR_DAYS_ESTAB(i).CD_ESTABELECIMENTO, 0); 
                  R_VENTILATOR_DAYS_ESTAB(i).TOTAL_STAYS := TO_NUMBER(REGEXP_SUBSTR(R_VENTILATOR_DAYS_ESTAB(i).ESTAB_VALUES, '[^;]+', 1, 1));
                  R_VENTILATOR_DAYS_ESTAB(i).TOTAL_DAYS := TO_NUMBER(REGEXP_SUBSTR(R_VENTILATOR_DAYS_ESTAB(i).ESTAB_VALUES, '[^;]+', 1, 2));
                  
                  IF R_VENTILATOR_DAYS_ESTAB(i).VENT_DAYS <> 0 OR R_VENTILATOR_DAYS_ESTAB(i).TOTAL_DAYS <> 0 THEN
                    R_VENTILATOR_DAYS_ESTAB(i).VENT_RATE := ROUND(100 * R_VENTILATOR_DAYS_ESTAB(i).VENT_DAYS / R_VENTILATOR_DAYS_ESTAB(i).TOTAL_DAYS, 1);
                  END IF;  
                  
                  INSERT INTO W_VENTILATOR_DAYS_ESTAB (
                    cd_estabelecimento, 
                    nr_total_stays, 
                    nr_total_days, 
                    nr_vent_stays, 
                    nr_vent_days, 
                    nr_vent_avg, 
                    nr_vent_median, 
                    nr_vent_rate,
                    nm_usuario)
                  VALUES (
                    R_VENTILATOR_DAYS_ESTAB(i).CD_ESTABELECIMENTO, -- cd_estabelecimento, 
                    R_VENTILATOR_DAYS_ESTAB(i).TOTAL_STAYS, -- nr_total_stays, 
                    ROUND(R_VENTILATOR_DAYS_ESTAB(i).TOTAL_DAYS, 1), -- nr_total_days, 
                    R_VENTILATOR_DAYS_ESTAB(i).VENT_STAYS, -- nr_vent_stays, 
                    ROUND(R_VENTILATOR_DAYS_ESTAB(i).VENT_DAYS, 1), -- nr_vent_days, 
                    R_VENTILATOR_DAYS_ESTAB(i).VENT_AVG, -- nr_vent_avg, 
                    R_VENTILATOR_DAYS_ESTAB(i).VENT_MEDIAN, -- nr_vent_median, 
                    R_VENTILATOR_DAYS_ESTAB(i).VENT_RATE, -- nr_vent_rate,
                    NM_USUARIO_W); -- nm_usuario

                END LOOP;
                
              EXCEPTION WHEN NO_DATA_FOUND THEN 
                NULL;
              END;
              COMMIT;
            EXIT WHEN C_VENTILATOR_DAYS_ESTAB%NOTFOUND; 
          END LOOP;
          CLOSE C_VENTILATOR_DAYS_ESTAB;
                    
        END IF;
        
      END LOOP;

    ELSE

      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT P_QUARTER QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE)
      LOOP

        OPEN C_VENTILATOR_DAYS(N.INI, N.FIM, P_CD_SETOR, CASE WHEN P_TYPE = 2 THEN NULL ELSE P_PATIENT END);
        LOOP
          FETCH C_VENTILATOR_DAYS BULK COLLECT INTO R_VENTILATOR_DAYS LIMIT 1000;
          
            BEGIN
              FOR i IN R_VENTILATOR_DAYS.FIRST..R_VENTILATOR_DAYS.LAST LOOP
                INSERT INTO W_VENTILATOR_DAYS (
                  nr_quarter, 
                  dt_ini_quarter, 
                  dt_fim_quarter, 
                  nr_atendimento, 
                  cd_estabelecimento, 
                  cd_setor_atendimento, 
                  nr_seq_atend_disp, 
                  dt_entrada_setor, 
                  dt_saida_setor, 
                  dt_entrada_disp, 
                  dt_saida_disp, 
                  nm_usuario)
                VALUES (
                  P_QUARTER, -- nr_quarter, 
                  N.INI, -- dt_ini_quarter, 
                  N.FIM, -- dt_fim_quarter, 
                  R_VENTILATOR_DAYS(i).NR_ATENDIMENTO, -- nr_atendimento, 
                  R_VENTILATOR_DAYS(i).CD_ESTABELECIMENTO, -- cd_estabelecimento, 
                  R_VENTILATOR_DAYS(i).CD_SETOR_ATENDIMENTO, -- cd_setor_atendimento, 
                  R_VENTILATOR_DAYS(i).NR_SEQ_ATEND_DISP, -- nr_seq_atend_disp, 
                  R_VENTILATOR_DAYS(i).DT_ENTRADA_SETOR, -- dt_entrada_setor, 
                  R_VENTILATOR_DAYS(i).DT_SAIDA_SETOR, -- dt_saida_setor, 
                  R_VENTILATOR_DAYS(i).DT_ENTRADA_DISP, -- dt_entrada_disp, 
                  R_VENTILATOR_DAYS(i).DT_SAIDA_DISP, -- dt_saida_disp,
                  NM_USUARIO_W); -- nm_usuario)
              END LOOP;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              NULL;
            END;
            COMMIT;
            
          EXIT WHEN C_VENTILATOR_DAYS%NOTFOUND; 
        END LOOP;
        CLOSE C_VENTILATOR_DAYS;      

        IF P_TYPE = 2 THEN
          OPEN C_VENTILATOR_DAYS_UNIT(N.QUARTER);
          LOOP
            FETCH C_VENTILATOR_DAYS_UNIT BULK COLLECT INTO R_VENTILATOR_DAYS_UNIT LIMIT 1000;            

              BEGIN
                FOR i IN R_VENTILATOR_DAYS_UNIT.FIRST..R_VENTILATOR_DAYS_UNIT.LAST LOOP
                        
                  R_VENTILATOR_DAYS_UNIT(i).UNIT_VALUES := PKG_ATEND_PAC_UNID.GET_STAY_COUNT_AND_AMOUNT(P_START_DATE, P_END_DATE, R_VENTILATOR_DAYS_UNIT(i).CD_ESTABELECIMENTO, R_VENTILATOR_DAYS_UNIT(i).CD_SETOR_ATENDIMENTO); 
                  R_VENTILATOR_DAYS_UNIT(i).TOTAL_STAYS := TO_NUMBER(REGEXP_SUBSTR(R_VENTILATOR_DAYS_UNIT(i).UNIT_VALUES, '[^;]+', 1, 1));
                  R_VENTILATOR_DAYS_UNIT(i).TOTAL_DAYS := TO_NUMBER(REGEXP_SUBSTR(R_VENTILATOR_DAYS_UNIT(i).UNIT_VALUES, '[^;]+', 1, 2));                
                  
                  IF R_VENTILATOR_DAYS_UNIT(i).VENT_DAYS <> 0 AND R_VENTILATOR_DAYS_UNIT(i).TOTAL_DAYS <> 0 THEN
                    R_VENTILATOR_DAYS_UNIT(i).VENT_RATE := ROUND(100 * R_VENTILATOR_DAYS_UNIT(i).VENT_DAYS / R_VENTILATOR_DAYS_UNIT(i).TOTAL_DAYS, 1);
                  END IF;
                        
                  INSERT INTO W_VENTILATOR_DAYS_UNIT (
                    cd_estabelecimento,
                    cd_setor_atendimento,
                    nr_quarter, 
                    nr_total_stays, 
                    nr_total_days, 
                    nr_vent_stays, 
                    nr_vent_days, 
                    nr_vent_avg, 
                    nr_vent_median, 
                    nr_vent_rate,
                    nm_usuario)
                  VALUES (
                    R_VENTILATOR_DAYS_UNIT(i).CD_ESTABELECIMENTO, -- cd_estabelecimento, 
                    R_VENTILATOR_DAYS_UNIT(i).CD_SETOR_ATENDIMENTO, -- cd_setor_atendimento,
                    R_VENTILATOR_DAYS_UNIT(i).QUARTER, -- nr_quarter,
                    R_VENTILATOR_DAYS_UNIT(i).TOTAL_STAYS, -- nr_total_stays, 
                    ROUND(R_VENTILATOR_DAYS_UNIT(i).TOTAL_DAYS, 1), -- nr_total_days, 
                    R_VENTILATOR_DAYS_UNIT(i).VENT_STAYS, -- nr_vent_stays, 
                    ROUND(R_VENTILATOR_DAYS_UNIT(i).VENT_DAYS, 1), -- nr_vent_days, 
                    R_VENTILATOR_DAYS_UNIT(i).VENT_AVG, -- nr_vent_avg, 
                    R_VENTILATOR_DAYS_UNIT(i).VENT_MEDIAN, -- nr_vent_median, 
                    R_VENTILATOR_DAYS_UNIT(i).VENT_RATE, -- nr_vent_rate,
                    NM_USUARIO_W); -- nm_usuario

                END LOOP;
                    
              EXCEPTION WHEN NO_DATA_FOUND THEN 
                NULL;
              END;
              COMMIT;
            EXIT WHEN C_VENTILATOR_DAYS_UNIT%NOTFOUND; 
          END LOOP;
          CLOSE C_VENTILATOR_DAYS_UNIT;

        END IF;
      
      END LOOP;
      
    END IF;

  END PRE_PROCESS;
  -- 
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0,
                P_YEAR       IN NUMBER,
                P_QUARTER    IN NUMBER DEFAULT 1,
                P_TYPE       IN NUMBER DEFAULT 1,
                P_PATIENT    IN VARCHAR2) IS
  BEGIN  
    TABLE_CLEANING;
    PRE_PROCESS(P_START_DATE, LEAST(P_END_DATE, TRUNC(SYSDATE, 'DD')), P_CD_ESTAB, P_CD_SETOR, P_YEAR, P_QUARTER, P_TYPE, P_PATIENT);
  END RUN;
END PKG_VENTILATOR_DAYS;
/
