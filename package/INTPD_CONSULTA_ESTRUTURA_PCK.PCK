CREATE OR REPLACE package intpd_consulta_estrutura_pck as

	/*Atributos do elemento do XML*/
	type r_classe_row is record(CD_CLASSE_MATERIAL   NUMBER(5),
				DS_CLASSE_MATERIAL   VARCHAR2(255),
				CD_SUBGRUPO_MATERIAL NUMBER(3),
				IE_SITUACAO          VARCHAR2(1),
				DT_ATUALIZACAO       DATE,
				NM_USUARIO           VARCHAR2(15),
				CD_CONTA_CONTABIL    VARCHAR2(20),
				IE_TIPO_CLASSE       VARCHAR2(2),
				NR_SEQ_APRES         NUMBER(3),
				CD_SISTEMA_ANT       VARCHAR2(80));
				
	type r_subgrupo_row is record(CD_SUBGRUPO_MATERIAL       NUMBER(3),
					DS_SUBGRUPO_MATERIAL     VARCHAR2(255),
					CD_GRUPO_MATERIAL        NUMBER(3),
					IE_SITUACAO              VARCHAR2(1),
					DT_ATUALIZACAO           DATE,
					NM_USUARIO               VARCHAR2(15),
					CD_CONTA_CONTABIL        VARCHAR2(20),
					CD_SISTEMA_ANT           VARCHAR2(80));			
				
	type r_grupo_row is record(CD_GRUPO_MATERIAL       NUMBER(3),
					DS_GRUPO_MATERIAL       VARCHAR2(255),
					IE_SITUACAO             VARCHAR2(1),
					DT_ATUALIZACAO          DATE,
					NM_USUARIO              VARCHAR2(15),
					CD_CONTA_CONTABIL       VARCHAR2(20),
					IE_HIST_SAUDE           VARCHAR2(1),
					CD_SISTEMA_ANT          VARCHAR2(80));	
					
	type t_classe is table of r_classe_row;
	type t_subgrupo is table of r_subgrupo_row;
	type t_grupo is table of r_grupo_row;

	function intpd_consulta_classe(nr_seq_fila_p number,ie_opcao_p varchar2) return t_classe pipelined;
	function intpd_consulta_subgrupo(nr_seq_fila_p number,ie_opcao_p varchar2) return t_subgrupo pipelined;
	function intpd_consulta_grupo(nr_seq_fila_p number,ie_opcao_p varchar2) return t_grupo pipelined;

end intpd_consulta_estrutura_pck;
/

create or replace package body intpd_consulta_estrutura_pck as

function intpd_consulta_classe(nr_seq_fila_p number,ie_opcao_p varchar2)
return t_classe pipelined is

r_classe_w 		r_classe_row;
xml_w			xmltype;
query_w			long;
cd_classe_material_w	Number(5);	
cd_subgrupo_material_w	Number(3);
ds_classe_material_w    Varchar2(255);
ie_situacao_w		varchar2(1);

--Classe material
cursor c01 is
select CD_CLASSE_MATERIAL, CD_SUBGRUPO_MATERIAL, DS_CLASSE_MATERIAL, IE_SITUACAO,NM_USUARIO
from   xmltable('/STRUCTURE/FILTERS' passing (select xmltype.createxml(ds_xml) from   intpd_fila_transmissao where  nr_sequencia = nr_seq_fila_p) COLUMNS
		CD_CLASSE_MATERIAL			Number(5) 	path 'CD_MATERIAL_CLASS',
		CD_SUBGRUPO_MATERIAL			Number(3) 	path 'CD_SUBGROUP_MATERIAL',
		DS_CLASSE_MATERIAL 			Varchar2(255) 	path 'DS_MATERIAL_CLASS',
		IE_SITUACAO				varchar2(1) 	path 'IE_STATUS',
		NM_USUARIO				Varchar2(15) 	path 'NM_USER');
c01_w	c01%rowtype;

--Classe material
cursor c02 is
select 	 CD_CLASSE_MATERIAL  ,                     
	 DS_CLASSE_MATERIAL  ,                     
	 CD_SUBGRUPO_MATERIAL,                      
	 IE_SITUACAO         ,                     
	 DT_ATUALIZACAO      ,                      
	 NM_USUARIO          ,                     
	 CD_CONTA_CONTABIL   ,                           
	 IE_TIPO_CLASSE      ,             
	 NR_SEQ_APRES        ,          
	 CD_SISTEMA_ANT
from   CLASSE_MATERIAL
where  IE_SITUACAO = nvl(ie_situacao_w,'A')
and    CD_CLASSE_MATERIAL = nvl(cd_classe_material_w,CD_CLASSE_MATERIAL)
and   ((ds_classe_material_w is not null and upper(DS_CLASSE_MATERIAL) like upper('%'|| ds_classe_material_w ||'%')) or (nvl(ds_classe_material_w,'0') = '0'))
and    CD_SUBGRUPO_MATERIAL =  nvl(cd_subgrupo_material_w,CD_SUBGRUPO_MATERIAL);
c02_w	c02%rowtype;

begin

if	(nvl(nr_seq_fila_p,0) > 0) then
	intpd_inicializacao(nr_seq_fila_p);

	if (nvl(ie_opcao_p,'X') = 'C')	then
		open c01;
		loop
		fetch c01 into
			c01_w;
		exit when c01%notfound;
			begin
			
			cd_classe_material_w	:= c01_w.CD_CLASSE_MATERIAL;
			cd_subgrupo_material_w	:= c01_w.CD_SUBGRUPO_MATERIAL;
			ds_classe_material_w    := c01_w.DS_CLASSE_MATERIAL;
			ie_situacao_w		:= c01_w.IE_SITUACAO;


			open c02;
			loop
			fetch c02 into
				c02_w;
			exit when c02%notfound;
				begin

				r_classe_w.CD_CLASSE_MATERIAL		:= c02_w.CD_CLASSE_MATERIAL;
				r_classe_w.DS_CLASSE_MATERIAL		:= c02_w.DS_CLASSE_MATERIAL;
				r_classe_w.CD_SUBGRUPO_MATERIAL		:= c02_w.CD_SUBGRUPO_MATERIAL;
				r_classe_w.IE_SITUACAO			:= c02_w.IE_SITUACAO;
				r_classe_w.DT_ATUALIZACAO               := c02_w.DT_ATUALIZACAO;
				r_classe_w.NM_USUARIO                   := c02_w.NM_USUARIO;
				r_classe_w.CD_CONTA_CONTABIL            := c02_w.CD_CONTA_CONTABIL;
				r_classe_w.IE_TIPO_CLASSE               := c02_w.IE_TIPO_CLASSE;
				r_classe_w.NR_SEQ_APRES                 := c02_w.NR_SEQ_APRES;
				r_classe_w.CD_SISTEMA_ANT               := c02_w.CD_SISTEMA_ANT;
							
				pipe row(r_classe_w);
								
				end;
			end loop;
			close c02;
			end;
		end loop;
		close c01;
	
	end if;
	
end if;

end intpd_consulta_classe;

function intpd_consulta_subgrupo(nr_seq_fila_p number,ie_opcao_p varchar2)
return t_subgrupo pipelined is

r_subgrupo_w 		r_subgrupo_row;
xml_w			xmltype;
query_w			long;
cd_subgrupo_material_w	number(3);	
cd_grupo_material_w	number(3);
ds_subgrupo_material_w    varchar2(255);
ie_situacao_w		varchar2(1);

--subgrupomaterial
cursor c03 is
select *
from   xmltable('/STRUCTURE/FILTERS' passing (select xmltype.createxml(ds_xml) from   intpd_fila_transmissao where  nr_sequencia = nr_seq_fila_p) COLUMNS
		CD_SUBGRUPO_MATERIAL			Number(3) 	path 'CD_MATERIAL_SUBGROUP',
		DS_SUBGRUPO_MATERIAL			varchar2(255) 	path 'DS_MATERIAL_SUBGROUP',
		CD_GRUPO_MATERIAL 			number(3) 	path 'CD_GROUP_MATERIAL',
		IE_SITUACAO				varchar2(1) 	path 'IE_STATUS',
		NM_USUARIO				Varchar2(15) 	path 'NM_USER');
c03_w	c03%rowtype;

--subgrupo material
cursor c04 is
select 	 cd_grupo_material,
	 ds_subgrupo_material,                     
	 cd_subgrupo_material,                      
	 ie_situacao ,                     
	 dt_atualizacao,                      
	 nm_usuario,                     
	 cd_conta_contabil,                                    
	 cd_sistema_ant
from   SUBGRUPO_MATERIAL
where  IE_SITUACAO = nvl(ie_situacao_w,'A')
and    cd_grupo_material = nvl(cd_grupo_material_w,cd_grupo_material)
and   ((ds_subgrupo_material_w is not null and upper(ds_subgrupo_material) like upper('%'|| ds_subgrupo_material_w ||'%')) OR (nvl(ds_subgrupo_material_w,'0') = '0'))
and    CD_SUBGRUPO_MATERIAL =  nvl(cd_subgrupo_material_w,CD_SUBGRUPO_MATERIAL);
c04_w	c04%rowtype;

begin

if	(nvl(nr_seq_fila_p,0) > 0) then

	intpd_inicializacao(nr_seq_fila_p);

	if (nvl(ie_opcao_p,'X') = 'S')	then
		open c03;
		loop
		fetch c03 into
			c03_w;
		exit when c03%notfound;
			begin
	
		cd_subgrupo_material_w	:= c03_w.cd_subgrupo_material;
		cd_grupo_material_w	:= c03_w.cd_grupo_material;
		ds_subgrupo_material_w  := c03_w.ds_subgrupo_material;
		ie_situacao_w		:= c03_w.ie_situacao;
		

			open c04;
			loop
			fetch c04 into
				c04_w;
			exit when c04%notfound;
				begin

				r_subgrupo_w.CD_SUBGRUPO_MATERIAL		:= c04_w.CD_SUBGRUPO_MATERIAL;
				r_subgrupo_w.DS_SUBGRUPO_MATERIAL		:= c04_w.DS_SUBGRUPO_MATERIAL;
				r_subgrupo_w.CD_GRUPO_MATERIAL			:= c04_w.CD_GRUPO_MATERIAL;
				r_subgrupo_w.IE_SITUACAO			:= c04_w.IE_SITUACAO;
				r_subgrupo_w.DT_ATUALIZACAO               	:= c04_w.DT_ATUALIZACAO;
				r_subgrupo_w.NM_USUARIO                   	:= c04_w.NM_USUARIO;
				r_subgrupo_w.CD_CONTA_CONTABIL            	:= c04_w.CD_CONTA_CONTABIL;				
				r_subgrupo_w.CD_SISTEMA_ANT               	:= c04_w.CD_SISTEMA_ANT;							
				
				pipe row(r_subgrupo_w);
								
				end;
			end loop;
			close c04;
			end;
		end loop;
		close c03;
	
	end if;
	
end if;

end intpd_consulta_subgrupo;

function intpd_consulta_grupo(nr_seq_fila_p number,ie_opcao_p varchar2)
return t_grupo pipelined is

r_grupo_w 		r_grupo_row;
xml_w			xmltype;
query_w			long;	
cd_grupo_material_w	number(3);
ds_grupo_material_w    varchar2(255);
ie_situacao_w		varchar2(1);

--srupo material
cursor c05 is
select *
from   xmltable('/STRUCTURE/FILTERS' passing (select xmltype.createxml(ds_xml) from   intpd_fila_transmissao where  nr_sequencia = nr_seq_fila_p) COLUMNS
		CD_GRUPO_MATERIAL			Number(3) 	path 'CD_MATERIAL_GROUP',
		DS_GRUPO_MATERIAL			varchar2(255) 	path 'DS_MATERIAL_GROUP',
		IE_SITUACAO				varchar2(1) 	path 'IE_STATUS',
		NM_USUARIO				Varchar2(15) 	path 'NM_USER');
c05_w	c05%rowtype;

--grupo material
cursor c06 is
select 	cd_grupo_material,     
	ds_grupo_material,     
	ie_situacao,           
	dt_atualizacao,       
	nm_usuario,            
	cd_conta_contabil,     
	ie_hist_saude,         
	cd_sistema_ant   
from    grupo_material
where  ie_situacao = nvl(ie_situacao_w,'A')
and    cd_grupo_material = nvl(cd_grupo_material_w,cd_grupo_material)
and   ((ds_grupo_material_w is not null and upper(DS_GRUPO_MATERIAL) like upper('%'|| ds_grupo_material_w ||'%')) OR (nvl(ds_grupo_material_w,'0') = '0'));
c06_w	c06%rowtype;

begin

if	(nvl(nr_seq_fila_p,0) > 0) then

	intpd_inicializacao(nr_seq_fila_p);

	if (nvl(ie_opcao_p,'X') = 'G')	then
		open c05;
		loop
		fetch c05 into
			c05_w;
		exit when c05%notfound;
			begin
	
		cd_grupo_material_w	:= c05_w.cd_grupo_material;
		ds_grupo_material_w  := c05_w.ds_grupo_material;
		ie_situacao_w		:= c05_w.ie_situacao;
		

			open c06;
			loop
			fetch c06 into
				c06_w;
			exit when c06%notfound;
				begin

				
				r_grupo_w.DS_GRUPO_MATERIAL		:= c06_w.DS_GRUPO_MATERIAL;
				r_grupo_w.CD_GRUPO_MATERIAL		:= c06_w.CD_GRUPO_MATERIAL;
				r_grupo_w.IE_SITUACAO			:= c06_w.IE_SITUACAO;
				r_grupo_w.DT_ATUALIZACAO               	:= c06_w.DT_ATUALIZACAO;
				r_grupo_w.NM_USUARIO                   	:= c06_w.NM_USUARIO;
				r_grupo_w.CD_CONTA_CONTABIL            	:= c06_w.CD_CONTA_CONTABIL;				
				r_grupo_w.CD_SISTEMA_ANT               	:= c06_w.CD_SISTEMA_ANT;
				r_grupo_w.IE_HIST_SAUDE			:= c06_w.IE_HIST_SAUDE;
				
				pipe row(r_grupo_w);
								
				end;
			end loop;
			close c06;
			end;
		end loop;
		close c05;
	
	end if;
	
end if;

end intpd_consulta_grupo;


end intpd_consulta_estrutura_pck;
/
