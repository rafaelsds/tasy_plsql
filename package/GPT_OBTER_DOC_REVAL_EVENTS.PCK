CREATE OR REPLACE package gpt_obter_doc_reval_events as
	type t_revalidation_events is record (
		nr_sequencia			cpoe_revalidation_events.nr_sequencia%type,
		nr_atendimento			cpoe_revalidation_events.nr_atendimento%type,
		dt_validacao			cpoe_revalidation_events.dt_validacao%type,
		dt_atualizacao_nrec		cpoe_revalidation_events.dt_atualizacao_nrec%type
	);
	type t_objeto_revalidation_events is
		table of t_revalidation_events;

	function get_sequencias(ds_itens_p	in varchar2 default null) return t_objeto_revalidation_events
		pipelined;

	function get_status_intervencao(nr_atendimento_p	in cpoe_revalidation_events.nr_atendimento%type) return varchar2;

end gpt_obter_doc_reval_events;
/


CREATE OR REPLACE package body gpt_obter_doc_reval_events as

	function get_sequencias(ds_itens_p	in varchar2 default null) return t_objeto_revalidation_events
		pipelined
	is

		t_objeto_row_w		t_revalidation_events;
		ie_existe_w			varchar2(1 char);

		cursor c01 is
		select		x.nr_atendimento,
					x.nr_sequencia,
					x.dt_validacao,
					x.dt_atualizacao_nrec,
					x.nr_seq_cpoe,
					x.nm_tabela,
					gpt_se_item_doc_reval(x.nm_tabela, x.nr_seq_cpoe, null, 1) ie_tipo_item
		from		gpt_doc_reval_events_v x
		where		x.ie_pendente_reval = 'S'
		and			gpt_se_item_doc_reval(x.nm_tabela, x.nr_seq_cpoe, null, 1) <> 'XPTO';

	begin


		for c01_w in c01 loop
		
			if (ds_itens_p is not null) then
			
				select	max('S')
				into	ie_existe_w
				from	table (lista_pck.obter_lista_char(ds_itens_p))
				where	c01_w.ie_tipo_item = cd_registro;
			
			end if;

			if	(ds_itens_p is null) or (ie_existe_w = 'S') then

				t_objeto_row_w.nr_sequencia := c01_w.nr_sequencia;
				t_objeto_row_w.nr_atendimento := c01_w.nr_atendimento;
				t_objeto_row_w.dt_validacao := c01_w.dt_validacao;
				t_objeto_row_w.dt_atualizacao_nrec := c01_w.dt_atualizacao_nrec;

			pipe row (t_objeto_row_w);
			
			end if;

		end loop;

		return;

	end get_sequencias;

	function get_status_intervencao(nr_atendimento_p	in cpoe_revalidation_events.nr_atendimento%type) 
	return varchar2 is

	qt_itens_pend_w				pls_integer;
	qt_itens_reval_w			pls_integer;
	ie_status_intervencao_w		varchar2(2 char);
	dt_inicio_w					cpoe_revalidation_events.dt_validacao%type;

	begin

		if	(nr_atendimento_p is not null) then

			dt_inicio_w	:=	gpt_get_doc_reval_period();

			select		count(1)
			into		qt_itens_pend_w
			from		gpt_doc_reval_events_v a
			where		a.ie_pendente_reval = 'S'
			and			gpt_se_item_doc_reval(a.nm_tabela, a.nr_seq_cpoe, null, 2) = 'S'
			and			a.nr_atendimento = nr_atendimento_p;
			
			select	count(1)
			into	qt_itens_reval_w
			from	gpt_doc_reval_events_v a
			where	a.ie_pendente_reval = 'S'
			and		gpt_se_item_doc_reval(a.nm_tabela, a.nr_seq_cpoe, null, 2) = 'S'
			and		a.nr_atendimento = nr_atendimento_p
			and		a.nr_sequencia = (select max(x.nr_seq_reval_events) from gpt_hist_reval_events_doc x where a.nr_seq_cpoe = x.nr_seq_cpoe);

			if ((qt_itens_pend_w = qt_itens_reval_w) and (qt_itens_pend_w <> 0)) then
				ie_status_intervencao_w := 'CO'; -- Completa

			elsif ((qt_itens_pend_w > qt_itens_reval_w) and (qt_itens_reval_w <> 0)) then
				ie_status_intervencao_w := 'PA'; -- Parcial

			elsif ((qt_itens_pend_w > 0) and (qt_itens_reval_w = 0)) then
				ie_status_intervencao_w := 'PE'; -- Pendente

			end if;

		end if;

		return ie_status_intervencao_w;

	end get_status_intervencao;

end gpt_obter_doc_reval_events;
/

