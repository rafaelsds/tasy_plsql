create or replace
package pls_obj_dados_rescisao_pck as

type t_dados_rescisao_row is record (	nr_sequencia			number,
					ie_tipo_item			number(10),
					ie_tipo_valor			pls_solic_resc_fin_item.ie_tipo_valor%type,
					nr_seq_segurado			pls_segurado.nr_sequencia%type,
					vl_item				number(15,2),
					vl_devolucao_mens		number(15,2),
					dt_referencia			date,
					qt_dias_cobertura		number(10),
					qt_dias_devolucao		number(10),
					nr_seq_mensalidade		pls_mensalidade.nr_sequencia%type,
					nr_seq_item_mensalidade		pls_mensalidade_seg_item.nr_sequencia%type,
					nr_seq_lanc_manual		pls_lancamento_mensalidade.nr_sequencia%type,
					nr_seq_lanc_auto		pls_segurado_mensalidade.nr_sequencia%type,
					nr_seq_regra_multa		pls_regra_multa_contratual.nr_sequencia%type,
					nr_seq_conta			pls_conta.nr_sequencia%type,
					nr_seq_copartic			pls_conta_coparticipacao.nr_sequencia%type,
					nr_seq_pos_estab		pls_conta_pos_estabelecido.nr_sequencia%type,
					nr_seq_pos_proc			pls_conta_pos_proc.nr_sequencia%type,
					nr_seq_pos_mat			pls_conta_pos_mat.nr_sequencia%type,
					nr_seq_co			pls_conta_co.nr_sequencia%type,
					nr_seq_val_atend		pls_conta_val_atend.nr_sequencia%type,
					ie_acao				pls_solic_resc_fin_item.ie_acao%type,
					dt_contabil			pls_solic_resc_fin_item.dt_contabil%type,
					vl_devido			number(15,2),
					vl_devolver			number(15,2),
					vl_cancelar			number(15,2),
					nr_titulo			titulo_receber.nr_titulo%type,
					ie_exibir			varchar2(1),
					vl_pro_rata_dia			number(15,2),
					vl_antecipacao			number(15,2),
					vl_ato_cooperado		number(15,2),
					vl_ato_auxiliar			number(15,2),
					vl_ato_nao_cooperado		number(15,2),
					vl_ato_cooperado_pro_rata	number(15,2),
					vl_ato_cooperado_antec		number(15,2),
					vl_ato_auxiliar_pro_rata	number(15,2),
					vl_ato_auxiliar_antec		number(15,2),
					vl_ato_nao_coop_pro_rata	number(15,2),
					vl_ato_nao_coop_antec		number(15,2));

type t_dados_rescisao is table of t_dados_rescisao_row;

function obter_dados_rescisao(	nr_seq_rescisao_fin_p		pls_solic_rescisao_fin.nr_sequencia%type,
				cd_estabelecimento_p		number)
				return t_dados_rescisao pipelined;

end pls_obj_dados_rescisao_pck;
/

create or replace package body pls_obj_dados_rescisao_pck as

function obter_dados_rescisao(	nr_seq_rescisao_fin_p		pls_solic_rescisao_fin.nr_sequencia%type,
				cd_estabelecimento_p		number)
				return t_dados_rescisao pipelined is

linha_w				t_dados_rescisao_row;

vl_mensalidade_w		pls_segurado_preco.vl_preco_atual%type;
dt_origem_w			pls_solicitacao_rescisao.dt_solicitacao%type;
dt_origem_ww			pls_solicitacao_rescisao.dt_solicitacao%type;
dt_fim_multa_contr_w		pls_solicitacao_rescisao.dt_solicitacao%type;
vl_mensalidades_nao_geradas_w	pls_segurado_preco.vl_preco_atual%type;
qt_meses_nao_gerados_w		number(10);
qt_registros_w			number(10);
qt_dias_cobertura_w		number(10);
qt_dias_cobertura_pro_w		number(10);
qt_dias_w			number(10,4);
qt_dias_mes_w			number(10);
qt_passegens_loop_w		number(10);
dt_mensalidade_w		date;
ie_devolver_dia_rescisao_w	pls_resc_fin_mens.ie_devolver_dia_rescisao%type;
ie_contas_medicas_pendentes_w	pls_resc_fin_mens.ie_contas_medicas_pendentes%type;
ie_lanc_prog_pendentes_w	pls_resc_fin_mens.ie_lanc_prog_pendentes%type;
ie_baixa_dev_titulo_w		pls_resc_fin_mens.ie_baixa_dev_titulo%type;
ie_desc_liq_rescisao_w		pls_resc_fin_mens.ie_desconsiderar_liq_rescisao%type;
ie_devolucao_item_w		varchar2(1);
nr_titulo_w			titulo_receber.nr_titulo%type;
dt_solicitacao_w		pls_solicitacao_rescisao.dt_solicitacao%type;
nr_hora_minuto_w		number(15,4);
ie_status_w			pls_solic_rescisao_fin.nr_sequencia%type;
ie_titulo_liq_mens_w		pls_resc_fin_mens.ie_titulo_liq_mens%type;
ie_situacao_tit_w		titulo_receber.ie_situacao%type;
ie_exibir_w			varchar2(1);
ie_isentar_multa_w		pls_motivo_cancelamento.ie_isentar_multa%type;
nr_seq_regra_w			pls_resc_fin_mens.nr_sequencia%type;
tx_valor_dev_w			number(7,4);
vl_devolucao_mens_w		number(15,2);
vl_pro_rata_w			number(15,2);
vl_antecipacao_w		number(15,2);
vl_ato_cooperado_w		number(15,2);
vl_ato_nao_cooperado_w		number(15,2);
vl_ato_auxiliar_w		number(15,2);
vl_ato_auxiliar_antec_w		number(15,2);
vl_ato_nao_coop_antec_w		number(15,2);
vl_ato_cooperado_antec_w	number(15,2);
vl_ato_auxiliar_pro_rata_w	number(15,2);
vl_ato_nao_coop_pro_rata_w	number(15,2);
vl_ato_cooperado_pro_rata_w	number(15,2);
vl_total_ato_w			number(15,2);
ie_baixa_rescisao_contrato_w	titulo_receber_liq.ie_baixa_rescisao_contrato%type;
ie_mes_rescisao_w		varchar2(1);
ie_mes_posterior_rescisao_w	varchar2(1);

Cursor C01 is
	select	 b.nr_seq_segurado,
		 a.nr_seq_pagador
	from	 pls_solic_rescisao_fin a,
		 pls_solic_rescisao_benef b,
		 pls_solicitacao_rescisao c,
		 pls_segurado_pagador d
	where	 c.nr_sequencia	= b.nr_seq_solicitacao	
	and	 c.nr_sequencia	= a.nr_seq_solicitacao
	and	 d.nr_seq_segurado = b.nr_seq_segurado
	and	 d.nr_seq_pagador = a.nr_seq_pagador
	and	 a.nr_sequencia	= nr_seq_rescisao_fin_p
	group by b.nr_seq_segurado,
		 a.nr_seq_pagador;

--1 - Mensalidade
Cursor C02(	nr_seq_segurado_pc	number,
		nr_seq_pagador_pc	number,
		dt_solicitacao_pc	date) is
	select	a.vl_item,
		b.dt_mesano_referencia,
		nvl(a.dt_inicio_cobertura, b.dt_inicio_cobertura) dt_inicio_cobertura,
		nvl(a.dt_fim_cobertura, b.dt_fim_cobertura) dt_fim_cobertura,
		a.nr_sequencia nr_seq_item_mensalidade,
		c.nr_sequencia nr_seq_mensalidade,
		a.ie_tipo_item ie_tipo_item_mens,
		a.nr_seq_tipo_lanc,
		null nr_titulo,
		(select	max(ie_situacao)
		from	titulo_receber x
		where	c.nr_sequencia = x.nr_seq_mensalidade) ie_situacao_tit,
		nvl(a.vl_ato_auxiliar, 0) vl_ato_auxiliar,
		nvl(a.vl_ato_auxiliar_antec, 0) vl_ato_auxiliar_antec,
		nvl(a.vl_ato_auxiliar_pro_rata, 0) vl_ato_auxiliar_pro_rata,
		nvl(a.vl_ato_cooperado, 0) vl_ato_cooperado,
		nvl(a.vl_ato_cooperado_antec, 0) vl_ato_cooperado_antec,
		nvl(a.vl_ato_cooperado_pro_rata, 0) vl_ato_cooperado_pro_rata,
		nvl(a.vl_ato_nao_coop_antec, 0) vl_ato_nao_coop_antec,
		nvl(a.vl_ato_nao_cooperado, 0) vl_ato_nao_cooperado,
		nvl(a.vl_ato_nao_coop_pro_rata, 0) vl_ato_nao_coop_pro_rata,
		nvl(a.vl_pro_rata_dia, 0) vl_pro_rata_dia,
		nvl(a.vl_antecipacao, 0) vl_antecipacao
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_mensalidade c,
		pls_lote_mensalidade d
	where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
	and	c.nr_sequencia	= b.nr_seq_mensalidade
	and	d.nr_sequencia	= c.nr_seq_lote
	and	b.nr_seq_segurado = nr_seq_segurado_pc
	and	c.nr_seq_pagador = nr_seq_pagador_pc
	and	d.ie_status = 2
	and	c.ie_cancelamento is null
	and	((dt_solicitacao_pc between nvl(a.dt_inicio_cobertura,b.dt_inicio_cobertura) and nvl(a.dt_fim_cobertura,b.dt_fim_cobertura))
	or	 (nvl(a.dt_inicio_cobertura,b.dt_inicio_cobertura) > dt_solicitacao_pc))
	union all --Titulos atrasados / Com cobertura anterior a rescisao do beneficiario
	select	a.vl_item,
		b.dt_mesano_referencia,
		nvl(a.dt_inicio_cobertura, b.dt_inicio_cobertura) dt_inicio_cobertura,
		nvl(a.dt_fim_cobertura, b.dt_fim_cobertura) dt_fim_cobertura,
		a.nr_sequencia nr_seq_item_mensalidade,
		c.nr_sequencia nr_seq_mensalidade,
		a.ie_tipo_item ie_tipo_item_mens,
		a.nr_seq_tipo_lanc,
		d.nr_titulo nr_titulo,
		d.ie_situacao ie_situacao_tit,
		nvl(a.vl_ato_auxiliar, 0) vl_ato_auxiliar,
		nvl(a.vl_ato_auxiliar_antec, 0) vl_ato_auxiliar_antec,
		nvl(a.vl_ato_auxiliar_pro_rata, 0) vl_ato_auxiliar_pro_rata,
		nvl(a.vl_ato_cooperado, 0) vl_ato_cooperado,
		nvl(a.vl_ato_cooperado_antec, 0) vl_ato_cooperado_antec,
		nvl(a.vl_ato_cooperado_pro_rata, 0) vl_ato_cooperado_pro_rata,
		nvl(a.vl_ato_nao_coop_antec, 0) vl_ato_nao_coop_antec,
		nvl(a.vl_ato_nao_cooperado, 0) vl_ato_nao_cooperado,
		nvl(a.vl_ato_nao_coop_pro_rata, 0) vl_ato_nao_coop_pro_rata,
		nvl(a.vl_pro_rata_dia, 0) vl_pro_rata_dia,
		nvl(a.vl_antecipacao, 0) vl_antecipacao
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_mensalidade c,
		titulo_receber d
	where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
	and	c.nr_sequencia	= b.nr_seq_mensalidade
	and	c.nr_sequencia	= d.nr_seq_mensalidade
	and	d.ie_situacao	= '1'
	and	b.nr_seq_segurado = nr_seq_segurado_pc
	and	c.nr_seq_pagador = nr_seq_pagador_pc
	and	c.ie_cancelamento is null
	and	nvl(a.dt_fim_cobertura,b.dt_fim_cobertura) < dt_solicitacao_pc;

Cursor C10 (	nr_seq_rescisao_fin_pc number) is
	select	nr_sequencia,
		ie_tipo_item,
		ie_tipo_valor,
		nr_seq_segurado,
		vl_item,
		vl_devolucao_mens,
		dt_referencia,
		qt_dias_cobertura,
		qt_dias_devolucao,
		nr_seq_mensalidade,
		nr_seq_item_mensalidade,
		ie_acao,
		nr_titulo,
		vl_pro_rata_dia,
		vl_antecipacao,
		vl_ato_auxiliar,
		vl_ato_auxiliar_antec,
		vl_ato_auxiliar_pro_rata,	
		vl_ato_cooperado,		
		vl_ato_cooperado_antec,		
		vl_ato_cooperado_pro_rata,	
		vl_ato_nao_cooperado,		
		vl_ato_nao_coop_antec,		
		vl_ato_nao_coop_pro_rata,
		vl_devolver,
		vl_cancelar,
		dt_contabil
	from	pls_solic_resc_fin_item
	where	nr_seq_solic_resc_fin	= nr_seq_rescisao_fin_pc;

begin

if	(nr_seq_rescisao_fin_p is not null) then
	select	ie_status
	into	ie_status_w
	from	pls_solic_rescisao_fin
	where	nr_sequencia	= nr_seq_rescisao_fin_p;
	
	if	(ie_status_w <> 1) then --Se o status nao for Pendente
		for r_c10_w in C10(nr_seq_rescisao_fin_p) loop
			begin
			linha_w	:= null;
			linha_w.nr_sequencia			:= r_c10_w.nr_sequencia;
			linha_w.ie_tipo_item			:= r_c10_w.ie_tipo_item;
			linha_w.ie_tipo_valor			:= r_c10_w.ie_tipo_valor;
			linha_w.nr_seq_segurado			:= r_c10_w.nr_seq_segurado;
			linha_w.vl_item				:= r_c10_w.vl_item;
			linha_w.vl_devolucao_mens		:= r_c10_w.vl_devolucao_mens;
			linha_w.dt_referencia			:= r_c10_w.dt_referencia;
			linha_w.qt_dias_cobertura		:= r_c10_w.qt_dias_cobertura;
			linha_w.qt_dias_devolucao		:= r_c10_w.qt_dias_devolucao;
			linha_w.nr_seq_mensalidade		:= r_c10_w.nr_seq_mensalidade;
			linha_w.nr_seq_item_mensalidade		:= r_c10_w.nr_seq_item_mensalidade;
			linha_w.ie_acao				:= r_c10_w.ie_acao;
			linha_w.nr_titulo			:= r_c10_w.nr_titulo;
			linha_w.vl_pro_rata_dia			:= r_c10_w.vl_pro_rata_dia;
			linha_w.vl_antecipacao			:= r_c10_w.vl_antecipacao;
			linha_w.vl_ato_auxiliar			:= r_c10_w.vl_ato_auxiliar;
			linha_w.vl_ato_auxiliar_antec		:= r_c10_w.vl_ato_auxiliar_antec;
			linha_w.vl_ato_auxiliar_pro_rata	:= r_c10_w.vl_ato_auxiliar_pro_rata;
			linha_w.vl_ato_cooperado		:= r_c10_w.vl_ato_cooperado;
			linha_w.vl_ato_cooperado_antec		:= r_c10_w.vl_ato_cooperado_antec;
			linha_w.vl_ato_cooperado_pro_rata	:= r_c10_w.vl_ato_cooperado_pro_rata;
			linha_w.vl_ato_nao_cooperado		:= r_c10_w.vl_ato_nao_cooperado;
			linha_w.vl_ato_nao_coop_antec		:= r_c10_w.vl_ato_nao_coop_antec;
			linha_w.vl_ato_nao_coop_pro_rata	:= r_c10_w.vl_ato_nao_coop_pro_rata;
			linha_w.vl_devolver			:= r_c10_w.vl_devolver;
			linha_w.vl_cancelar			:= r_c10_w.vl_cancelar;
			linha_w.dt_contabil			:= r_c10_w.dt_contabil;
			pipe row(linha_w);
			end;
		end loop;
	else
		select	nvl(b.dt_rescisao,b.dt_solicitacao) --A data de rescisao pode ser diferente e precisa ser considerada nos calculos
		into	dt_solicitacao_w
		from	pls_solic_rescisao_fin a,
			pls_solicitacao_rescisao b
		where	b.nr_sequencia	= a.nr_seq_solicitacao
		and	a.nr_sequencia	= nr_seq_rescisao_fin_p;
		
		nr_seq_regra_w	:= pls_obter_regra_resc_fin(nr_seq_rescisao_fin_p, cd_estabelecimento_p);
		
		select	max(ie_devolver_dia_rescisao),
			nvl(max(ie_contas_medicas_pendentes),'N'),
			nvl(max(ie_lanc_prog_pendentes),'N'),
			nvl(max(ie_titulo_liq_mens), 'N'),
			nvl(max(ie_baixa_dev_titulo),'N'),
			nvl(max(ie_desconsiderar_liq_rescisao),'N')
		into	ie_devolver_dia_rescisao_w,
			ie_contas_medicas_pendentes_w,
			ie_lanc_prog_pendentes_w,
			ie_titulo_liq_mens_w,
			ie_baixa_dev_titulo_w,
			ie_desc_liq_rescisao_w
		from	pls_resc_fin_mens
		where	nr_sequencia	= nr_seq_regra_w;
		
		if	(nr_seq_regra_w is not null) then
			for r_C01_w in C01 loop
				--1 - Mensalidade
				for r_C02_w in C02(	r_C01_w.nr_seq_segurado,
							r_C01_w.nr_seq_pagador,
							trunc(dt_solicitacao_w,'dd')) loop
					ie_exibir_w	:= 'S';

					if	(r_c02_w.nr_titulo is null) then
						select	max(nr_titulo)
						into	nr_titulo_w
						from	titulo_receber
						where	nr_seq_mensalidade = r_c02_w.nr_seq_mensalidade;
						
						if	(ie_titulo_liq_mens_w = 'S') then
							if	(nr_titulo_w is not null) then
								select	max(ie_situacao)
								into	ie_situacao_tit_w
								from	titulo_receber
								where	nr_titulo = nr_titulo_w;
								
								if	(ie_situacao_tit_w = '2') then
									ie_exibir_w	:= 'S';
								else
									ie_exibir_w	:= 'N';
								end if;
							else
								ie_exibir_w	:= 'N';
							end if;
						end if;
					elsif	(ie_titulo_liq_mens_w = 'S') then
						ie_exibir_w	:= 'N';
					else
						nr_titulo_w	:= r_c02_w.nr_titulo;
					end if;
					
					if	(ie_desc_liq_rescisao_w = 'S') and
						(ie_exibir_w = 'S') and
						(nr_titulo_w is not null) then
						select	count(1)
						into	qt_registros_w
						from	titulo_receber_liq
						where	nr_titulo	= nr_titulo_w
						and	nvl(ie_baixa_rescisao_contrato,'N') = 'S';
						
						if	(qt_registros_w > 0) then
							ie_exibir_w	:= 'N';
						end if;
					end if;
					
					if	(ie_exibir_w = 'S') then
						linha_w := null;
						linha_w.ie_tipo_item	:= 1;
						linha_w.ie_tipo_valor	:= 'P'; --Pagar - Devolucacao
						linha_w.nr_seq_segurado	:= r_C01_w.nr_seq_segurado;
						
						select	decode(count(1),0,'N','S')
						into	ie_devolucao_item_w
						from	pls_resc_fin_mens_item x,
							pls_resc_fin_mens y
						where	y.nr_sequencia		= x.nr_seq_regra
						and	y.nr_sequencia		= nr_seq_regra_w
						and	x.ie_tipo_item		= r_c02_w.ie_tipo_item_mens
						and	(x.nr_seq_tipo_lanc = r_c02_w.nr_seq_tipo_lanc or x.nr_seq_tipo_lanc is null);

						if	(trunc(r_c02_w.dt_inicio_cobertura,'mm') = trunc(dt_solicitacao_w,'mm')) then
							ie_mes_rescisao_w := 'S';
							ie_mes_posterior_rescisao_w := 'N';
						else
							ie_mes_rescisao_w := 'N';
							
							if	(trunc(r_c02_w.dt_fim_cobertura,'mm') > trunc(r_c02_w.dt_inicio_cobertura,'mm')) then
								if	(trunc(r_c02_w.dt_fim_cobertura,'mm') = trunc(dt_solicitacao_w,'mm')) then
									ie_mes_posterior_rescisao_w := 'S';
								else
									ie_mes_posterior_rescisao_w := 'N';
								end if;
							else
								ie_mes_posterior_rescisao_w := 'N';
							end if;
						end if;
						
						if	(r_c02_w.dt_inicio_cobertura > dt_solicitacao_w) then
							linha_w.vl_item			:= r_C02_w.vl_item;
							linha_w.qt_dias_cobertura	:= round(trunc(r_C02_w.dt_fim_cobertura,'dd') - r_C02_w.dt_inicio_cobertura)+1;
							
							if	(ie_devolucao_item_w = 'S') then
								vl_devolucao_mens_w			:= r_c02_w.vl_item;
								linha_w.qt_dias_devolucao		:= round(trunc(r_C02_w.dt_fim_cobertura,'dd') - r_C02_w.dt_inicio_cobertura)+1;
								linha_w.vl_devolucao_mens		:= vl_devolucao_mens_w;
								linha_w.vl_ato_cooperado		:= r_c02_w.vl_ato_cooperado;
								linha_w.vl_ato_auxiliar			:= r_c02_w.vl_ato_auxiliar;
								linha_w.vl_ato_nao_cooperado		:= r_c02_w.vl_ato_nao_cooperado;
								
								if	(ie_mes_posterior_rescisao_w = 'S') then
									linha_w.vl_ato_auxiliar_antec		:= r_c02_w.vl_ato_auxiliar_antec + r_c02_w.vl_ato_auxiliar_pro_rata;
									linha_w.vl_ato_cooperado_antec		:= r_c02_w.vl_ato_cooperado_antec + r_c02_w.vl_ato_cooperado_pro_rata;
									linha_w.vl_ato_nao_coop_antec		:= r_c02_w.vl_ato_nao_coop_antec + r_c02_w.vl_ato_nao_coop_pro_rata;
									linha_w.vl_ato_auxiliar_pro_rata	:= 0;
									linha_w.vl_ato_cooperado_pro_rata	:= 0;
									linha_w.vl_ato_nao_coop_pro_rata	:= 0;
									linha_w.vl_pro_rata_dia			:= 0;
									linha_w.vl_antecipacao			:= r_c02_w.vl_antecipacao + r_c02_w.vl_pro_rata_dia;
								else
									linha_w.vl_ato_auxiliar_antec		:= r_c02_w.vl_ato_auxiliar_antec;
									linha_w.vl_ato_cooperado_antec		:= r_c02_w.vl_ato_cooperado_antec;
									linha_w.vl_ato_nao_coop_antec		:= r_c02_w.vl_ato_nao_coop_antec;
									linha_w.vl_ato_auxiliar_pro_rata	:= r_c02_w.vl_ato_auxiliar_pro_rata;
									linha_w.vl_ato_cooperado_pro_rata	:= r_c02_w.vl_ato_cooperado_pro_rata;
									linha_w.vl_ato_nao_coop_pro_rata	:= r_c02_w.vl_ato_nao_coop_pro_rata;
									linha_w.vl_pro_rata_dia			:= r_c02_w.vl_pro_rata_dia;
									linha_w.vl_antecipacao			:= r_c02_w.vl_antecipacao;
								end if;
							else
								vl_devolucao_mens_w			:= 0;
								linha_w.qt_dias_devolucao		:= 0;
								linha_w.vl_devolucao_mens		:= vl_devolucao_mens_w;
								linha_w.vl_ato_auxiliar			:= 0;
								linha_w.vl_ato_auxiliar_antec		:= 0;
								linha_w.vl_ato_auxiliar_pro_rata	:= 0;
								linha_w.vl_ato_cooperado		:= 0;
								linha_w.vl_ato_cooperado_antec		:= 0;
								linha_w.vl_ato_cooperado_pro_rata	:= 0;
								linha_w.vl_ato_nao_coop_antec		:= 0;
								linha_w.vl_ato_nao_cooperado		:= 0;
								linha_w.vl_ato_nao_coop_pro_rata	:= 0;
								linha_w.vl_pro_rata_dia			:= 0;
								linha_w.vl_antecipacao			:= 0;
							end if;
						elsif	(trunc(dt_solicitacao_w,'dd') between r_C02_w.dt_inicio_cobertura and r_c02_w.dt_fim_cobertura) then
							qt_dias_cobertura_w	:= round(trunc(r_C02_w.dt_fim_cobertura,'dd') - r_C02_w.dt_inicio_cobertura)+1;
							qt_dias_w		:= round(trunc(r_C02_w.dt_fim_cobertura,'dd') - trunc(dt_solicitacao_w,'dd'));
							
							if	(ie_devolver_dia_rescisao_w = 'S') then
								qt_dias_w := qt_dias_w+1;
							elsif	(ie_devolver_dia_rescisao_w = 'P') then
								if	(fim_dia(r_c02_w.dt_fim_cobertura) = dt_solicitacao_w) then
									ie_devolucao_item_w := 'N';
								else
									nr_hora_minuto_w := ((to_number((to_number(to_char(dt_solicitacao_w,'hh24'))*60) + to_number(to_char(dt_solicitacao_w,'mi'))) / 1440) - 1) *-1;
									qt_dias_w := qt_dias_w+nr_hora_minuto_w;
								end if;
							end if;
							
							linha_w.vl_item			:= r_C02_w.vl_item;
							linha_w.qt_dias_cobertura	:= qt_dias_cobertura_w;
							
							if	(ie_devolucao_item_w = 'S') then
								vl_devolucao_mens_w			:= (r_C02_w.vl_item/qt_dias_cobertura_w) * qt_dias_w;
								tx_valor_dev_w				:= dividir_sem_round(vl_devolucao_mens_w, r_c02_w.vl_item);
								
								vl_ato_cooperado_w			:= r_c02_w.vl_ato_cooperado * tx_valor_dev_w;
								vl_ato_nao_cooperado_w			:= r_c02_w.vl_ato_nao_cooperado * tx_valor_dev_w;
								vl_ato_auxiliar_w			:= r_c02_w.vl_ato_auxiliar * tx_valor_dev_w;
								
								vl_total_ato_w				:= vl_ato_cooperado_w + vl_ato_nao_cooperado_w + vl_ato_auxiliar_w;

								if	(vl_devolucao_mens_w <> vl_total_ato_w) then
									if	(vl_ato_cooperado_w	<> 0) then
										vl_ato_cooperado_w	:= vl_ato_cooperado_w + (vl_devolucao_mens_w - vl_total_ato_w);
									elsif	(vl_ato_auxiliar_w	<> 0) then
										vl_ato_auxiliar_w	:= vl_ato_auxiliar_w + (vl_devolucao_mens_w - vl_total_ato_w);
									elsif	(vl_ato_nao_cooperado_w <> 0) then
										vl_ato_nao_cooperado_w	:= vl_ato_nao_cooperado_w + (vl_devolucao_mens_w - vl_total_ato_w);
									end if;
								end if;
								
								if	(ie_mes_rescisao_w = 'S') then
									if	((r_c02_w.vl_pro_rata_dia <> 0) and (r_c02_w.vl_antecipacao <> 0)) then -- Se tiver valor de pro rata e antecipacao faz o rateio
										if	(dt_solicitacao_w <= fim_mes(r_C02_w.dt_inicio_cobertura)) then
											vl_antecipacao_w	:= r_c02_w.vl_antecipacao;
											vl_pro_rata_w		:= vl_devolucao_mens_w - vl_antecipacao_w;
										else
											vl_pro_rata_w		:= 0;
											vl_antecipacao_w	:= vl_devolucao_mens_w;
										end if;
									else	-- Se tiver apenas o valor de pro rata, o sistema usa o valor da devolucao para o valor de pro rata e o valor de antecipacao fica 0
										vl_pro_rata_w			:= vl_devolucao_mens_w;
										vl_antecipacao_w		:= 0;
									end if;

									/*
									Devolucao, Exemplo 30$
										Valor dev	Pro-rata	Antecipacao
									Item	R$ 30,00	R$ 21,00	 R$ 9,00
									Ato coop	R$ 10,50	R$ 3,00	 R$ 7,50
									Ato aux	R$ 6,00	R$ 6,00	 R$ 0,00
									Ato nao	R$ 13,50	R$ 12,00	 R$ 1,50 
									*/
									
									vl_ato_auxiliar_antec_w			:= r_c02_w.vl_ato_auxiliar_antec * tx_valor_dev_w;
									vl_ato_nao_coop_antec_w			:= r_c02_w.vl_ato_nao_coop_antec * tx_valor_dev_w;
									vl_ato_cooperado_antec_w		:= r_c02_w.vl_ato_cooperado_antec * tx_valor_dev_w;
									
									if	(vl_antecipacao_w <> (vl_ato_nao_coop_antec_w + vl_ato_auxiliar_antec_w + vl_ato_cooperado_antec_w)) then
										if	(vl_ato_nao_coop_antec_w <> 0) then
											vl_ato_nao_coop_antec_w		:= vl_ato_nao_coop_antec_w + (vl_antecipacao_w - (vl_ato_nao_coop_antec_w + vl_ato_auxiliar_antec_w + vl_ato_cooperado_antec_w));
										elsif	(vl_ato_auxiliar_antec_w <> 0) then
											vl_ato_auxiliar_antec_w		:= vl_ato_auxiliar_antec_w + (vl_antecipacao_w - (vl_ato_nao_coop_antec_w + vl_ato_auxiliar_antec_w + vl_ato_cooperado_antec_w));
										elsif	(vl_ato_cooperado_antec_w <> 0) then
											vl_ato_cooperado_antec_w	:= vl_ato_cooperado_antec_w + (vl_antecipacao_w - (vl_ato_nao_coop_antec_w + vl_ato_auxiliar_antec_w + vl_ato_cooperado_antec_w));
										end if;
									end if;
									
									vl_ato_auxiliar_pro_rata_w	:= vl_ato_auxiliar_w - vl_ato_auxiliar_antec_w;
									vl_ato_nao_coop_pro_rata_w	:= vl_ato_nao_cooperado_w - vl_ato_nao_coop_antec_w;
									vl_ato_cooperado_pro_rata_w	:= vl_ato_cooperado_w - vl_ato_cooperado_antec_w;
								else
									/*
									Devolucao, Exemplo 30$
										Valor dev	Pro-rata	Antecipacao
									Item	R$ 30,00	R$ 0	 R$ 30,00
									Ato coop	R$ 10,50	R$ 0	 R$ 10,50
									Ato aux	R$ 6,00	R$ 0	 R$ 6,00
									Ato nao	R$ 13,50	R$ 0	 R$ 13,50 
									*/
									
									vl_antecipacao_w		:= vl_devolucao_mens_w;
									vl_pro_rata_w			:= 0;
									vl_ato_auxiliar_pro_rata_w	:= 0;
									vl_ato_cooperado_pro_rata_w	:= 0;
									vl_ato_nao_coop_pro_rata_w	:= 0;
									vl_ato_auxiliar_antec_w		:= vl_ato_auxiliar_w;
									vl_ato_cooperado_antec_w	:= vl_ato_cooperado_w;
									vl_ato_nao_coop_antec_w		:= vl_ato_nao_cooperado_w;
								end if;
									
								linha_w.qt_dias_devolucao		:= qt_dias_w;
								linha_w.vl_devolucao_mens		:= vl_devolucao_mens_w;
								linha_w.vl_pro_rata_dia			:= vl_pro_rata_w;
								linha_w.vl_antecipacao			:= vl_antecipacao_w;
								linha_w.vl_ato_auxiliar			:= vl_ato_auxiliar_w;
								linha_w.vl_ato_auxiliar_antec		:= vl_ato_auxiliar_antec_w;
								linha_w.vl_ato_auxiliar_pro_rata	:= vl_ato_auxiliar_pro_rata_w;
								linha_w.vl_ato_cooperado		:= vl_ato_cooperado_w;
								linha_w.vl_ato_cooperado_antec		:= vl_ato_cooperado_antec_w;
								linha_w.vl_ato_cooperado_pro_rata	:= vl_ato_cooperado_pro_rata_w;
								linha_w.vl_ato_nao_cooperado		:= vl_ato_nao_cooperado_w;
								linha_w.vl_ato_nao_coop_antec		:= vl_ato_nao_coop_antec_w;
								linha_w.vl_ato_nao_coop_pro_rata	:= vl_ato_nao_coop_pro_rata_w;
							else
								vl_devolucao_mens_w			:= 0;
								linha_w.qt_dias_devolucao		:= 0;
								linha_w.vl_devolucao_mens		:= vl_devolucao_mens_w;
								linha_w.vl_ato_auxiliar			:= 0;
								linha_w.vl_ato_auxiliar_antec		:= 0;
								linha_w.vl_ato_auxiliar_pro_rata	:= 0;
								linha_w.vl_ato_cooperado		:= 0;
								linha_w.vl_ato_cooperado_antec		:= 0;
								linha_w.vl_ato_cooperado_pro_rata	:= 0;
								linha_w.vl_ato_nao_coop_antec		:= 0;
								linha_w.vl_ato_nao_cooperado		:= 0;
								linha_w.vl_ato_nao_coop_pro_rata	:= 0;
								linha_w.vl_pro_rata_dia			:= 0;
								linha_w.vl_antecipacao			:= 0;
							end if;
						else
							vl_devolucao_mens_w			:= 0;
							linha_w.vl_item				:= r_C02_w.vl_item;
							linha_w.qt_dias_cobertura		:= round(trunc(r_C02_w.dt_fim_cobertura,'dd') - r_C02_w.dt_inicio_cobertura)+1;
							linha_w.qt_dias_devolucao		:= 0;
							linha_w.vl_devolucao_mens		:= vl_devolucao_mens_w;
							linha_w.vl_ato_auxiliar			:= 0;
							linha_w.vl_ato_auxiliar_antec		:= 0;
							linha_w.vl_ato_auxiliar_pro_rata	:= 0;
							linha_w.vl_ato_cooperado		:= 0;
							linha_w.vl_ato_cooperado_antec		:= 0;
							linha_w.vl_ato_cooperado_pro_rata	:= 0;
							linha_w.vl_ato_nao_coop_antec		:= 0;
							linha_w.vl_ato_nao_cooperado		:= 0;
							linha_w.vl_ato_nao_coop_pro_rata	:= 0;
							linha_w.vl_pro_rata_dia			:= 0;
							linha_w.vl_antecipacao			:= 0;
						end if;
						

						if	(nvl(r_c02_w.ie_situacao_tit, '1') = '2') or
							(ie_baixa_dev_titulo_w = 'N') then
							linha_w.vl_devolver := vl_devolucao_mens_w;
							linha_w.vl_cancelar := 0;
						else
							linha_w.vl_cancelar := vl_devolucao_mens_w;
							linha_w.vl_devolver := 0;
						end if;

						if	(r_C02_w.dt_mesano_referencia > dt_solicitacao_w) then
							linha_w.dt_contabil	:= r_C02_w.dt_mesano_referencia;
						else
							linha_w.dt_contabil	:= dt_solicitacao_w;
						end if;
						
						linha_w.nr_seq_mensalidade	:= r_c02_w.nr_seq_mensalidade;
						linha_w.nr_seq_item_mensalidade	:= r_C02_w.nr_seq_item_mensalidade;
						linha_w.dt_referencia		:= r_C02_w.dt_mesano_referencia;
						linha_w.nr_titulo		:= nr_titulo_w;
						linha_w.ie_acao			:= 2; --Devolver

						if	(nr_titulo_w is null) then
							linha_w.ie_exibir := 'N';
						end if;
						
						pipe row(linha_w);
					end if;
				end loop; --C02
			end loop; --C01
		end if;
	end if;
end if;

--Nao retorna nada
return;

end obter_dados_rescisao;

end pls_obj_dados_rescisao_pck;
/