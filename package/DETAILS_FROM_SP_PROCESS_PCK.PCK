create or replace PACKAGE details_from_sp_process_pck AS
 
  FUNCTION get_nr_etapa_atual(cd_versao_p varchar2) RETURN NUMBER;
  
  FUNCTION get_dt_etapa_atual(nr_etapa_p number, cd_versao_p varchar2) RETURN DATE;
                                      
  FUNCTION get_date_build(ds_version_p  varchar2, 
                          cd_build_p varchar2,
                          ie_periodo_p  Varchar2) RETURN DATE;
  FUNCTION get_nm_etapa_atual(ie_estagio_exec_p varchar2) return varchar2;      
  
  FUNCTION get_os_from_disapproved_build(nr_sequencia_p number, nr_sp_p number default null) return varchar2;
  
  FUNCTION verify_if_exists_db_test_vv(cd_versao_p varchar2, ds_hotfix_p varchar2 default null) return varchar2; 
  
  FUNCTION get_cd_build(nr_seq_calendario_p number, dt_geracao_p date default null) return varchar2;
  
  FUNCTION get_status_tasy_q_build(ie_status_build_p varchar2, cd_build_p varchar2 default null) return varchar2;

END details_from_sp_process_pck;
/

create or replace PACKAGE body details_from_sp_process_pck as

  FUNCTION get_dt_etapa_atual(
    nr_etapa_p  NUMBER,
    cd_versao_p VARCHAR2)
  RETURN DATE
AS
  dt_etapa_w       DATE;
  dt_schedule_w    VARCHAR2(30);
  dt_execution_w   VARCHAR2(30);
  dt_ini_analise_w VARCHAR2(30);
  dt_fim_analise_w VARCHAR2(30);
  dt_doc_inicio_w  VARCHAR2(30);
  dt_doc_fim_w     VARCHAR2(30);
BEGIN
  SELECT DISTINCT NVL(TO_CHAR(a.dt_schedule, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_execution, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_ini_analise, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_fim_analise, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_doc_inicio, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_doc_fim, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00')
  INTO dt_schedule_w,
    dt_execution_w,
    dt_ini_analise_w,
    dt_fim_analise_w,
    dt_doc_inicio_w,
    dt_doc_fim_w
  FROM schem_test_package_sched@schematic4test a,
    schem_test_schedule@schematic4test b
  WHERE a.nr_sequencia = b.nr_seq_package
  AND a.ie_estag_exec  = 3
  AND cd_versao_p      = SUBSTR(b.ds_version,1,9);
  IF (nr_etapa_p       = 1) THEN
    dt_etapa_w        := get_date_build(cd_versao_p, '-1', 'I');
  ELSIF (nr_etapa_p    = 2) THEN
    dt_etapa_w        := to_date(dt_schedule_w);--'Automated Testing';
  ELSIF (nr_etapa_p    = 3) THEN
    dt_etapa_w        := to_date(dt_ini_analise_w);--'Analysis';
  ELSIF (nr_etapa_p    = 4) THEN
    dt_etapa_w        := to_date(dt_doc_inicio_w);--'Documentation';
  ELSIF (nr_etapa_p    = 5) THEN
    dt_etapa_w        := to_date(dt_doc_fim_w);--'Finished';
  END IF;
  RETURN dt_etapa_w;
END;
FUNCTION get_nr_etapa_atual(
    cd_versao_p VARCHAR2)
  RETURN NUMBER
AS
  dt_schedule_w    VARCHAR2(30);
  dt_execution_w   VARCHAR2(30);
  dt_ini_analise_w VARCHAR2(30);
  dt_fim_analise_w VARCHAR2(30);
  dt_doc_inicio_w  VARCHAR2(30);
  dt_doc_fim_w     VARCHAR2(30);
  dt_build_ini_w   DATE;
  dt_build_fim_w   DATE;
  nr_etapa_atual_w NUMBER;
BEGIN
  SELECT DISTINCT NVL(TO_CHAR(a.dt_schedule, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_execution, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_ini_analise, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_fim_analise, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_doc_inicio, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00'),
    NVL(TO_CHAR(a.dt_doc_fim, 'DD/MM/YYYY HH:mm:ss'), '00/00/0000 00:00:00')
  INTO dt_schedule_w,
    dt_execution_w,
    dt_ini_analise_w,
    dt_fim_analise_w,
    dt_doc_inicio_w,
    dt_doc_fim_w
  FROM schem_test_package_sched@schematic4test a,
    schem_test_schedule@schematic4test b
  WHERE a.nr_sequencia     = b.nr_seq_package
  AND a.ie_estag_exec      = 3
  AND cd_versao_p          = SUBSTR(b.ds_version,1,9);
  dt_build_ini_w          := get_date_build(cd_versao_p, '-1', 'I');
  dt_build_fim_w          := get_date_build(cd_versao_p, '-1', 'F');
  IF (dt_build_ini_w      IS NOT NULL AND dt_build_fim_w IS NULL) THEN
    nr_etapa_atual_w      := 1;
  ELSIF (dt_schedule_w    <> '00/00/0000 00:00:00' AND dt_execution_w = '00/00/0000 00:00:00') THEN
    nr_etapa_atual_w      := 2;--'Automated Testing';
  ELSIF (dt_ini_analise_w <> '00/00/0000 00:00:00' AND dt_fim_analise_w = '00/00/0000 00:00:00') THEN
    nr_etapa_atual_w      := 3;--'Analysis';
  ELSIF (dt_doc_inicio_w  <> '00/00/0000 00:00:00' AND dt_doc_fim_w = '00/00/0000 00:00:00') THEN
    nr_etapa_atual_w      := 4;--'Documentation';
  ELSIF (dt_doc_inicio_w  <> '00/00/0000 00:00:00' AND dt_doc_fim_w <> '00/00/0000 00:00:00') THEN
    nr_etapa_atual_w      := 5;--'Finished';
  END IF;
  RETURN nr_etapa_atual_w;
END;
FUNCTION get_date_build(
    ds_version_p VARCHAR2,
    cd_build_p   VARCHAR2,
    ie_periodo_p VARCHAR2)
  RETURN DATE
AS
  dt_build_w DATE;
BEGIN
  IF (ie_periodo_p = 'I') THEN
    SELECT MIN(dt_inicio_geracao)
    INTO dt_build_w
    FROM corp.man_os_ctrl_build@whebl01_dbcorp mocb,
      corp.man_os_ctrl_desc@whebl01_dbcorp mocd
    WHERE mocd.nr_sequencia = mocb.nr_seq_man_os_ctrl_desc
    AND mocd.dt_inativacao IS NULL
    AND mocd.dt_liberacao  IS NOT NULL
    AND mocd.ie_aplicacao   = 'H'
    AND mocb.cd_versao      = ds_version_p
    AND mocb.cd_build       = to_number(cd_build_p);
  ELSE
    SELECT MIN(dt_fim_geracao)
    INTO dt_build_w
    FROM corp.man_os_ctrl_build@whebl01_dbcorp mocb,
      corp.man_os_ctrl_desc@whebl01_dbcorp mocd
    WHERE mocd.nr_sequencia = mocb.nr_seq_man_os_ctrl_desc
    AND mocd.dt_inativacao IS NULL
    AND mocd.dt_liberacao  IS NOT NULL
    AND mocd.ie_aplicacao   = 'H'
    AND mocb.cd_versao      = ds_version_p
    AND mocb.cd_build       = to_number(cd_build_p);
  END IF;
  RETURN dt_build_w;
END;
FUNCTION get_nm_etapa_atual(
    ie_estagio_exec_p VARCHAR2)
  RETURN VARCHAR2
AS
  nm_status_w VARCHAR2(30);
BEGIN
  nm_status_w := obter_valor_dominio(9583, ie_estagio_exec_p);
  RETURN nm_status_w;
END;
FUNCTION get_os_from_disapproved_build(
    nr_sequencia_p NUMBER,
    nr_sp_p        NUMBER DEFAULT NULL)
  RETURN VARCHAR2
AS
  os_list_w    VARCHAR2(500);
  gerencia_w   VARCHAR2(50);
  ds_estagio_w VARCHAR2(50);
BEGIN
  os_list_w   := '';
  IF (nr_sp_p IS NOT NULL)THEN
    RETURN '--';
  ELSE
    FOR os IN
    ( SELECT DISTINCT TO_CHAR(sc.nr_os) nr_os
    FROM schem_test_os@schematic4test sc,
      schem_test_package_sched@schematic4test a
    WHERE Sc.Nr_Seq_Pack_Sched = a.nr_sequencia
    AND sc.ds_severidade      IN ('S1', 'S2')
    AND a.nr_sequencia         = nr_sequencia_p
    )
    LOOP
      SELECT ds_estagio
      INTO ds_estagio_w
      FROM corp.man_ordem_servico@whebl01_dbcorp mos,
        corp.man_estagio_processo@whebl01_dbcorp mep
      WHERE mos.nr_seq_estagio = mep.nr_sequencia
      AND mos.nr_Sequencia     = os.nr_os;
      SELECT OBTER_DESC_GRUPO_DESEN(nr_seq_grupo_des)
      INTO gerencia_w
      FROM corp.man_ordem_servico@whebl01_dbcorp
      WHERE nr_Sequencia = os.nr_os;
      os_list_w         := concat(os_list_w, ' SO-'||os.nr_os||' - '||gerencia_w);
    END LOOP;
    RETURN os_list_w;
  END IF;
END;
FUNCTION verify_if_exists_db_test_vv(
    cd_versao_p VARCHAR2,
    ds_hotfix_p VARCHAR2 DEFAULT NULL)
  RETURN VARCHAR2
AS
  exists_w NUMBER;
BEGIN
  SELECT COUNT(*) countx
  INTO exists_w
  FROM SCHEM_TEST_SNAPSHOT@schematic4test s
  WHERE regexp_substr(s.ds_versao, '[0-9](\.)[0-9]{2}(\.)[0-9]{4}', 1)     = cd_versao_p
  AND NVL(regexp_substr(s.ds_versao, '(\-)[0-9]{2}(\-)[0-9]{7}', 11), '0') = NVL(ds_hotfix_p, '0');
  IF (exists_w                                                             = 1) THEN
    RETURN 'Y';
  ELSE
    RETURN 'N';
  END IF;
END;
FUNCTION get_cd_build(
    nr_seq_calendario_p NUMBER,
    dt_geracao_p        DATE DEFAULT NULL)
  RETURN VARCHAR2
AS
  cd_build_w NUMBER;
BEGIN
  SELECT MAX(NVL(cd_build, -1))
  INTO cd_build_w
  FROM corp.man_Versao_calendario@whebl01_dbcorp mv
  WHERE mv.nr_Sequencia = nr_seq_calendario_p
  OR mv.dt_geracao BETWEEN dt_geracao_p - 1 AND dt_geracao_p + 1 ;
  IF (cd_build_w =                      -1) THEN
    RETURN '--';
  ELSE
    RETURN TO_CHAR(cd_build_w);
  END IF;
END;
FUNCTION get_status_tasy_q_build(
    ie_status_build_p VARCHAR2,
    cd_build_p        VARCHAR2 DEFAULT NULL)
  RETURN VARCHAR2
AS
  status_build_w VARCHAR2(50);
  cd_build_W     NUMBER;
BEGIN
  SELECT DECODE(ie_status_build_p, 'F', 'Build Finished', 'B', 'Failure', 'A', 'Scheduled', 'C', 'Canceled', 'W', '', 'G', 'Building') ie_status_build
  INTO status_build_w
  FROM dual;
  IF (cd_build_p <> '--' AND ie_status_build_p = 'B') THEN
    RETURN 'Rebuilt Finished';
  ELSE
    RETURN status_build_w;
  END IF;
END;
  
  
END details_from_sp_process_pck;
/
