create or replace package get_val_from_field_pck is

  -- Author  : TOLIVEIRA
  -- Created : 28/07/2020 09:16:05
  -- Purpose : Get all values by executing Valores field

  type r_result_row is record(
    DS_EXP varchar2(500),
    CD varchar2(500),
    DS varchar2(500),
    col3 varchar2(500),
    col4 varchar2(500));

  type t_result is table of r_result_row;

  function get_valores(query_text_p varchar2) return t_result pipelined;

end get_val_from_field_pck;
/
create or replace package body get_val_from_field_pck is
  function get_valores(query_text_p varchar2) return t_result pipelined is
  
    vr_cursor     VARCHAR2(2000) := query_text_p;
    vr_nrcursor   NUMBER;
    vr_nrretorn   NUMBER;
    vr_qtdretor   NUMBER;
    vr_dscodigo   VARCHAR2(4000);
    vr_descricao  VARCHAR2(4000);
	vr_descricao1 VARCHAR2(4000);
    vr_descricao2 VARCHAR2(4000);
    vr_descricao3 VARCHAR2(4000);
	vr_descricao4 VARCHAR2(4000);
	vr_descricao5 VARCHAR2(4000);
	v_campo	      varchar2(255);
	
	v_col_cnt integer;
    v_columns dbms_sql.desc_tab;
	v_count_columns integer;
	
	r_result_w r_result_row;
	v_composto_w CHAR(1);
    --cursores
	cursor c01 is
	select query_text_p DS_EXP, 
			regexp_substr(replace(SUBSTR(replace(query_text_p,' ',''),
				INSTR(replace(query_text_p,' ',''),
				  ')(') + 2), ')', ''), '[^,]+', 1, level) DS, 
			regexp_substr(replace(SUBSTR(replace(query_text_p,' ',''),
				INSTR(replace(query_text_p,' ',''), '(') + 1, INSTR(replace(query_text_p,' ',''),
					')(')), ')(', ''), '[^,]+', 1, level) CD
	from dual
	connect by regexp_substr(replace(SUBSTR(replace(query_text_p,' ',''),
					INSTR(replace(query_text_p,' ',''),
						')(') + 2), ')', ''), '[^,]+', 1, level) is not null;
	c01_w	c01%rowtype;

  begin
  
	open c01;
		loop
			fetch c01 into c01_w;
			exit when c01%notfound;
				begin
					r_result_w.DS_EXP := c01_w.DS_EXP;
					r_result_w.CD := c01_w.CD;
					r_result_w.DS := c01_w.DS;
					pipe row(r_result_w);
				end;
		end loop;
	close c01;		
  
  end get_valores;
  
end get_val_from_field_pck;
/
