create or replace
package pls_termo_beneficiario_pck as

type t_termos_row is record (	nr_seq_segurado			pls_segurado.nr_sequencia%type,
				nr_seq_termo			pls_termo_beneficiario.nr_sequencia%type,
				ds_tipo_termo			pls_termo_beneficiario.ds_tipo_termo%type,
				ds_termo			pls_termo_beneficiario.ds_termo%type,
				ie_obrigatorio			pls_termo_beneficiario.ie_obrigatorio%type);

type t_termos is table of t_termos_row;

function obter_termos(		nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type)
				return t_termos pipelined;

function obter_se_benef_possui_termo(	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
					nr_seq_termo_p		pls_termo_beneficiario.nr_sequencia%type)
					return char;

end pls_termo_beneficiario_pck;
/

create or replace package body pls_termo_beneficiario_pck as

linha_w				t_termos_row;

function obter_termos(		nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type)
				return t_termos pipelined is

nr_seq_plano_w			pls_segurado.nr_seq_plano%type;
dt_contratacao_w		pls_segurado.dt_contratacao%type;
qt_idade_w			number(3);
ie_titularidade_w		varchar2(1);

nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type;
ds_nacionalidade_w		nacionalidade.ds_nacionalidade%type;
ds_estado_civil_w		varchar2(255);
nr_cpf_w			pessoa_fisica.nr_cpf%type;
nr_cpf_pagador_w		pessoa_fisica.nr_cpf%type;
nm_pagador_w			varchar2(255);
ds_endereco_w			compl_pessoa_fisica.ds_endereco%type;
nr_endereco_w			compl_pessoa_fisica.nr_endereco%type;
ds_bairro_w			compl_pessoa_fisica.ds_bairro%type;
ds_municipio_w			compl_pessoa_fisica.ds_municipio%type;
sg_estado_w			compl_pessoa_fisica.sg_estado%type;
cd_cep_w			compl_pessoa_fisica.cd_cep%type;
ds_email_w			compl_pessoa_fisica.ds_email%type;
nr_telefone_w			compl_pessoa_fisica.nr_telefone%type;
ds_profissao_w			profissao.ds_profissao%type;
dt_adesao_w			pls_segurado.dt_contratacao%type;
ds_tipo_beneficiario_w		varchar2(255);

Cursor C01 is
	select	a.ie_acao,
		a.nr_seq_termo,
		b.ie_obrigatorio,
		b.ds_tipo_termo,
		b.ds_termo
	from	pls_regra_termo_benef_web a,
		pls_termo_beneficiario b
	where	b.nr_sequencia		= a.nr_seq_termo
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	(a.nr_seq_plano = nr_seq_plano_w or a.nr_seq_plano is null)
	and	qt_idade_w between nvl(a.qt_idade_inicial,qt_idade_w) and nvl(a.qt_idade_final,qt_idade_w)
	and	dt_contratacao_w between nvl(a.dt_contratacao_inicial,dt_contratacao_w) and nvl(a.dt_contratacao_final,dt_contratacao_w)
	and	(a.ie_titularidade = ie_titularidade_w or nvl(ie_titularidade,'A') = 'A');

Cursor C02 is
	select	a.nr_sequencia,
		substr(obter_idade_pf(a.cd_pessoa_fisica,sysdate,'A'),1,3) qt_idade
	from	pls_segurado a,
		pessoa_fisica b,
		compl_pessoa_fisica c,
		pls_contrato_pagador d
	where	c.cd_pessoa_fisica = a.cd_pessoa_fisica
	and	b.cd_pessoa_fisica = c.cd_pessoa_fisica(+)
	and	a.nr_seq_pagador = d.nr_sequencia(+)
	and	c.ie_tipo_complemento = 1
	and	a.nr_seq_titular = nr_seq_segurado_p;

begin
select	nr_seq_plano,
	dt_contratacao,
	substr(obter_idade_pf(cd_pessoa_fisica,sysdate,'A'),1,3) qt_idade,
	decode(nr_seq_titular,null,'T','D') ie_titularidade
into	nr_seq_plano_w,
	dt_contratacao_w,
	qt_idade_w,
	ie_titularidade_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

for c01_w in C01 loop
	begin
	if	(c01_w.ie_acao in ('B','BD')) then
		if	(obter_se_benef_possui_termo(nr_seq_segurado_p, c01_w.nr_seq_termo) = 'N') then
			
			select	b.nm_pessoa_fisica,
				(	select	max(x.ds_nacionalidade)
					from	nacionalidade x
					where	x.cd_nacionalidade = b.cd_nacionalidade) ds_nacionalidade,
				obter_valor_dominio(5,b.ie_estado_civil) ds_estado_civil,
				b.nr_cpf,
				obter_dados_pf_pj(d.cd_pessoa_fisica,d.cd_cgc,'N') nm_pagador,
				obter_dados_pf(d.cd_pessoa_fisica,'CPF') nr_cpf_pagador,
				c.ds_endereco,
				c.nr_endereco,
				c.ds_bairro,
				c.ds_municipio,
				c.sg_estado,
				c.cd_cep,
				c.ds_email,
				c.nr_telefone,
				(	select	max(x.ds_profissao)
					from	profissao x
					where	x.cd_profissao = c.cd_profissao) ds_profissao,
				a.dt_contratacao,
				obter_valor_dominio(2406,a.ie_tipo_segurado) ds_tipo_beneficiario
			into	nm_pessoa_fisica_w,
				ds_nacionalidade_w,
				ds_estado_civil_w,
				nr_cpf_w,
				nm_pagador_w,
				nr_cpf_pagador_w,
				ds_endereco_w,
				nr_endereco_w,
				ds_bairro_w,
				ds_municipio_w,
				sg_estado_w,
				cd_cep_w,
				ds_email_w,
				nr_telefone_w,
				ds_profissao_w,
				dt_adesao_w,
				ds_tipo_beneficiario_w
			from	pls_segurado	a,
				pessoa_fisica	b,
				compl_pessoa_fisica c,
				pls_contrato_pagador d
			where	b.cd_pessoa_fisica = a.cd_pessoa_fisica
			and	b.cd_pessoa_fisica = c.cd_pessoa_fisica(+)
			and	a.nr_seq_pagador = d.nr_sequencia(+)
			and	c.ie_tipo_complemento = 1
			and	a.nr_sequencia	= nr_seq_segurado_p;
			
			linha_w.nr_seq_segurado	:= nr_seq_segurado_p;
			linha_w.nr_seq_termo	:= c01_w.nr_seq_termo;
			linha_w.ds_tipo_termo	:= c01_w.ds_tipo_termo;
			linha_w.ds_termo	:= c01_w.ds_termo;
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@NM_BENEFICIARIO',nm_pessoa_fisica_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@NACIONALIDADE',ds_nacionalidade_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@ESTADO_CIVIL',ds_estado_civil_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@PROFISSAO',ds_profissao_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@CPF',nr_cpf_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@LOGRADOURO',ds_endereco_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@NR_LOGRADOURO',to_char(nr_endereco_w));
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@BAIRRO',ds_bairro_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@CIDADE',ds_municipio_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@UF',sg_estado_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@CEP',cd_cep_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@EMAIL',ds_email_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@TELEFONE',nr_telefone_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@DT_ADESAO',to_char(dt_adesao_w,'DD/MM/YYYY'));
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@TIPO_BENEFICIARIO',ds_tipo_beneficiario_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@PAGADOR_NOME',nm_pagador_w);
			linha_w.ds_termo	:= replace(linha_w.ds_termo,'@PAGADOR_CPF',nr_cpf_pagador_w);
			
			linha_w.ie_obrigatorio		:= c01_w.ie_obrigatorio;
			pipe row(linha_w);
		end if;
	end if;
	
	if	(c01_w.ie_acao in ('BD','D')) then
		for c02_w in c02 loop
			begin
			if	(obter_se_benef_possui_termo(c02_w.nr_sequencia, c01_w.nr_seq_termo) = 'N') then
				if	(c02_w.qt_idade < 18) then
					select	b.nm_pessoa_fisica,
						(	select	max(x.ds_nacionalidade)
							from	nacionalidade x
							where	x.cd_nacionalidade = b.cd_nacionalidade) ds_nacionalidade,
						obter_valor_dominio(5,b.ie_estado_civil) ds_estado_civil,
						b.nr_cpf,
						obter_dados_pf_pj(d.cd_pessoa_fisica,d.cd_cgc,'N') nm_pagador,
						obter_dados_pf(d.cd_pessoa_fisica,'CPF') nr_cpf_pagador,
						c.ds_endereco,
						c.nr_endereco,
						c.ds_bairro,
						c.ds_municipio,
						c.sg_estado,
						c.cd_cep,
						c.ds_email,
						c.nr_telefone,
						(	select	max(x.ds_profissao)
							from	profissao x
							where	x.cd_profissao = c.cd_profissao) ds_profissao,
						a.dt_contratacao,
						obter_valor_dominio(2406,a.ie_tipo_segurado) ds_tipo_beneficiario
					into	nm_pessoa_fisica_w,
						ds_nacionalidade_w,
						ds_estado_civil_w,
						nr_cpf_w,
						nm_pagador_w,
						nr_cpf_pagador_w,
						ds_endereco_w,
						nr_endereco_w,
						ds_bairro_w,
						ds_municipio_w,
						sg_estado_w,
						cd_cep_w,
						ds_email_w,
						nr_telefone_w,
						ds_profissao_w,
						dt_adesao_w,
						ds_tipo_beneficiario_w
					from	pls_segurado	a,
						pessoa_fisica	b,
						compl_pessoa_fisica c,
						pls_contrato_pagador d
					where	b.cd_pessoa_fisica = a.cd_pessoa_fisica
					and	b.cd_pessoa_fisica = c.cd_pessoa_fisica(+)
					and	a.nr_seq_pagador = d.nr_sequencia(+)
					and	c.ie_tipo_complemento = 1
					and	a.nr_sequencia	= c02_w.nr_sequencia;
					
					linha_w.nr_seq_segurado	:= c02_w.nr_sequencia;
					linha_w.nr_seq_termo	:= c01_w.nr_seq_termo;
					linha_w.ds_tipo_termo	:= c01_w.ds_tipo_termo;
					linha_w.ds_termo	:= c01_w.ds_termo;
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@NM_BENEFICIARIO',nm_pessoa_fisica_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@NACIONALIDADE',ds_nacionalidade_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@ESTADO_CIVIL',ds_estado_civil_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@PROFISSAO',ds_profissao_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@CPF',nr_cpf_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@LOGRADOURO',ds_endereco_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@NR_LOGRADOURO',to_char(nr_endereco_w));
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@BAIRRO',ds_bairro_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@CIDADE',ds_municipio_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@UF',sg_estado_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@CEP',cd_cep_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@EMAIL',ds_email_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@TELEFONE',nr_telefone_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@DT_ADESAO',to_char(dt_adesao_w,'DD/MM/YYYY'));
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@TIPO_BENEFICIARIO',ds_tipo_beneficiario_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@PAGADOR_NOME',nm_pagador_w);
					linha_w.ds_termo	:= replace(linha_w.ds_termo,'@PAGADOR_CPF',nr_cpf_pagador_w);
					
					linha_w.ie_obrigatorio	:= c01_w.ie_obrigatorio;
					pipe row(linha_w);
				end if;
			end if;
			
			end;
		end loop;
	end if;
	end;
end loop;

return;

end obter_termos;

function obter_se_benef_possui_termo(	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
					nr_seq_termo_p		pls_termo_beneficiario.nr_sequencia%type)
					return char is

qt_contador_w	number(4);
ds_retorno_w	char(1)	:= 'N';

begin

select	count(*)
into	qt_contador_w
from	pls_segurado_termo
where	nr_seq_segurado	= nr_seq_segurado_p
and	nr_seq_termo	= nr_seq_termo_p
and	dt_confirmacao is not null;

if	(qt_contador_w > 0) then
	ds_retorno_w	:= 'S';
end if;

return ds_retorno_w;

end obter_se_benef_possui_termo;

end pls_termo_beneficiario_pck;
/