create or replace package patient_billing_tree_pck as

	type r_tree_item is record(
		ie_item			varchar2(50),
		ds_item			varchar2(255),
		tree_node		number(10),
		ie_ordem		number(3),
		nr_seq_episodio		episodio_paciente.nr_sequencia%type,
		nr_atendimento		atendimento_paciente.nr_atendimento%type,
		cd_pessoa_fisica	pessoa_fisica.cd_pessoa_fisica%type,
		nr_interno_conta	conta_paciente.nr_interno_conta%type);

	type t_tree_item is table of r_tree_item;

	function get_tree_itens
			(cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type,
			nr_seq_episodio_p	episodio_paciente.nr_sequencia%type,
			nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
			cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
			cd_perfil_p		perfil.cd_perfil%type,
			nm_usuario_p		usuario.nm_usuario%type)
			return t_tree_item pipelined;

	function OBTER_SE_ITENS_EXCLUIDOS
			(cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type,
			nr_seq_episodio_p	episodio_paciente.nr_sequencia%type,
			nr_atendimento_p	atendimento_paciente.nr_atendimento%type)
			return boolean;

	function OBTER_SE_MOSTRA_ITEM
			(ie_item_p		varchar2,
			nr_seq_episodio_p	episodio_paciente.nr_sequencia%type,
			nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
			cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
			cd_perfil_p		perfil.cd_perfil%type,
			nm_usuario_p		usuario.nm_usuario%type)
			return varchar2;


end patient_billing_tree_pck;
/
create or replace package body PATIENT_BILLING_TREE_PCK as

	function get_tree_itens(cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type,
				nr_seq_episodio_p	episodio_paciente.nr_sequencia%type,
				nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				cd_perfil_p		perfil.cd_perfil%type,
				nm_usuario_p		usuario.nm_usuario%type)
				return t_tree_item pipelined is

	r_tree_item_w		r_tree_item;
	qt_registro_w		number(10);

	cursor c01 is
	select	vl_dominio cd,
		ds_expressao ds,
		1094964 tree_node,
		5 ie_ordem
	from	valor_dominio_v
	where	cd_dominio		= 8978
	and	vl_dominio	= 'AT'
	union
	select	vl_dominio cd,
		ds_expressao ds,
		1094745 tree_node,
		10 ie_ordem
	from	valor_dominio_v
	where	cd_dominio		= 8978
	and	vl_dominio	= 'CT'
	union
	select	vl_dominio cd,
		ds_expressao ds,
		1094749 tree_node,
		15 ie_ordem
	from	valor_dominio_v
	where	cd_dominio		= 8978
	and	vl_dominio	= 'DIAG'
	/*union
	select	vl_dominio cd,
		ds_expressao ds,
		1094745 tree_node,
		20 ie_ordem
	from	valor_dominio_v
	where	cd_dominio		= 8978
	and	vl_dominio	= 'PROC'*/
	union
	select	vl_dominio cd,
		ds_expressao ds,
		1094748 tree_node,
		30 ie_ordem
	from	valor_dominio_v
	where	cd_dominio		= 8978
	and	ds_valor_dominio	= 'DRG'
	union
	select	vl_dominio cd,
		ds_expressao ds,
		1094750 tree_node,
		40 ie_ordem
	from	valor_dominio_v
	where	cd_dominio		= 8978
	and	vl_dominio	= 'AAT'
	order by ie_ordem;

	c01_w	c01%rowtype;

	begin

		if	(cd_pessoa_fisica_p is not null) or
			(nr_seq_episodio_p is not null) or
			(nr_atendimento_p is not null) then

			r_tree_item_w.nr_seq_episodio	:= nr_seq_episodio_p;
			r_tree_item_w.nr_atendimento	:= nr_atendimento_p;
			r_tree_item_w.cd_pessoa_fisica	:= cd_pessoa_fisica_p;
			r_tree_item_w.nr_interno_conta	:= 0;

			open C01;
			loop
			fetch C01 into
				c01_w;
			exit when C01%notfound;

				if	(OBTER_SE_MOSTRA_ITEM(c01_w.cd, nr_seq_episodio_p, nr_atendimento_p, cd_estabelecimento_p, cd_perfil_p, nm_usuario_p) = 'S') then

					r_tree_item_w.ie_item		:= c01_w.cd;
					r_tree_item_w.ds_item		:= c01_w.ds;
					r_tree_item_w.tree_node		:= c01_w.tree_node;
					r_tree_item_w.ie_ordem		:= c01_w.ie_ordem;

					pipe row(r_tree_item_w);
				end if;
			end loop;
			close C01;

			if	(OBTER_SE_ITENS_EXCLUIDOS(cd_pessoa_fisica_p, nr_seq_episodio_p, nr_atendimento_p) and
				(nvl(nr_seq_episodio_p,0) > 0 and nvl(nr_atendimento_p,0) > 0)) then

				r_tree_item_w.ie_item		:= 'EXC';
				r_tree_item_w.ds_item		:= obter_desc_expressao(308459);
				r_tree_item_w.tree_node		:= 1096205;
				r_tree_item_w.ie_ordem		:= 13;
				pipe row(r_tree_item_w);
			end if;
		end if;

	end get_tree_itens;

	function OBTER_SE_ITENS_EXCLUIDOS(cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type,
			nr_seq_episodio_p	episodio_paciente.nr_sequencia%type,
			nr_atendimento_p	atendimento_paciente.nr_atendimento%type)
			return boolean is

	ie_retorno_w	boolean := false;
	qt_registro_w	number(10);

	begin

	select	1
	into	qt_registro_w
	from	dual
	where	exists
		(select	1
		from	procedimento_paciente x
		where	x.nr_atendimento	= nr_atendimento_p
		and	nvl(nr_seq_episodio_p,0) = 0
		and	x.cd_motivo_exc_conta	is not null
		and	rownum = 1
		union all
		select	1
		from	material_atend_paciente x
		where	x.nr_atendimento	= nr_atendimento_p
		and	nvl(nr_seq_episodio_p,0) = 0
		and	x.cd_motivo_exc_conta	is not null
		and	rownum = 1
		union all
		select	1
		from	procedimento_paciente x,
			atendimento_paciente y
		where	x.nr_atendimento	= y.nr_atendimento
		and	y.nr_seq_episodio	= nr_seq_episodio_p
		and	x.cd_motivo_exc_conta	is not null
		and	rownum = 1
		union all
		select	1
		from	material_atend_paciente x,
			atendimento_paciente y
		where	x.nr_atendimento	= y.nr_atendimento
		and	y.nr_seq_episodio	= nr_seq_episodio_p
		and	x.cd_motivo_exc_conta	is not null
		and	rownum = 1);

	if	(qt_registro_w > 0) then
		return true;
	else
		return false;
	end if;

	end OBTER_SE_ITENS_EXCLUIDOS;

	function OBTER_SE_MOSTRA_ITEM
			(ie_item_p		varchar2,
			nr_seq_episodio_p	episodio_paciente.nr_sequencia%type,
			nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
			cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
			cd_perfil_p		perfil.cd_perfil%type,
			nm_usuario_p		usuario.nm_usuario%type)
			return varchar2 is

	ie_visualiza_w	varchar2(1);
    ie_situacao_w   varchar2(1);
	nr_seq_regra_w	number(10);
	ie_tipo_case_w	atendimento_paciente.ie_tipo_atendimento%type;

	cursor c01 is
	select	ie_visualiza
	from	conta_regra_visual
	where	nr_seq_regra					= nr_seq_regra_w
	and	ie_situacao					= 'A'
	and	nvl(ie_tipo_case,nvl(ie_tipo_case_w,0))		= nvl(ie_tipo_case_w,0)
	and	nvl(cd_perfil,cd_perfil_p)			= cd_perfil_p
	and	nvl(nm_usuario_regra,nm_usuario_p)		= nm_usuario_p
	order by	ie_tipo_case nulls first,
		cd_perfil nulls first;

	begin

	begin
	select	ie_situacao, nr_sequencia
	into	ie_situacao_w, nr_seq_regra_w
	from	conta_paciente_regra a
	where	a.ie_item_conta		= ie_item_p
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	rownum			= 1;
	exception
	when others then
		nr_seq_regra_w 	:= null;
        ie_situacao_w := 'A';
	end;

	if	(nr_seq_regra_w is null and ie_situacao_w = 'A') then
		ie_visualiza_w := 'S';
	elsif (ie_situacao_w = 'A') then
		if	(nr_seq_episodio_p is not null) then
			select	max(b.ie_tipo)
			into	ie_tipo_case_w
			from	tipo_episodio b,
				episodio_paciente a
			where	a.nr_seq_tipo_episodio	= b.nr_sequencia
			and	a.nr_sequencia		= nr_seq_episodio_p;
		elsif	(nr_atendimento_p is not null) then
			select	max(ie_tipo_atendimento)
			into	ie_tipo_case_w
			from	atendimento_paciente
			where	nr_atendimento	= nr_atendimento_p;
		end if;

		open C01;
		loop
		fetch C01 into
			ie_visualiza_w;
		exit when C01%notfound;
		end loop;
		close C01;
    else ie_visualiza_w := 'N';
	end if;

	return	ie_visualiza_w;

	end OBTER_SE_MOSTRA_ITEM;

end patient_billing_tree_pck;
/