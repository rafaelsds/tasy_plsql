create or replace package tiss_convenio_pck as

procedure set_dt_referencia_tiss(dt_referencia_p date);
procedure set_gravar_log_inconsist(ie_gravar_log_inconsist_p varchar2);
procedure gravar_log_inconsist(	nm_usuario_p		varchar2,
				nr_seq_inconsistencia_p	number,
				nr_sequencia_autor_p	number,
				nr_interno_conta_p	number,
				nr_seq_protocolo_p	number);

function get_dt_referencia_tiss return date;
function get_gravar_log_inconsist return varchar2;

function tiss_obter_versao(	cd_convenio_p		number,
				 cd_estabelecimento_p	number,
				 dt_referencia_p	date default null,
			  	 ie_tipo_tiss_p		varchar2 default null) return varchar2;
			
			
function get_tiss_param_convenio	(	cd_convenio_p	number,
						cd_estabelecimento_p	number,
						ie_opcao_p	varchar2) return varchar2;
						
function TISS_OBTER_XML_PROJETO
		(cd_convenio_p		in	number,
		cd_estabelecimento_p	in	number,
		ie_tipo_tiss_p		in	varchar2,
		dt_referencia_p 	in	date default null) return number;


end tiss_convenio_pck;
/

create or replace
package body tiss_convenio_pck as

dt_referencia_w			date;
ie_gravar_log_inconsist_w	varchar2(1);
dt_referencia_local_w		date	:= sysdate;



type campos is record 	(	cd_convenio		number(10),
				cd_estabelecimento	number(10),
				dt_referencia		date,
				ds_versao		varchar2(50),
				ie_tipo_tiss		varchar(20));
				
type Vetor  is table of campos index 	by binary_integer;
versoes_w		Vetor;


type tiss_param_conv is record 	(	cd_convenio		number(10),
					cd_estabelecimento	number(10),
					IE_DATA_DESPESA		varchar2(40));
				
type tiss_param_conv_vet  is table of tiss_param_conv index 	by binary_integer;
tiss_param_conv_vet_w		tiss_param_conv_vet;


type tiss_proj_xml is record 	(	cd_convenio		number(10),
					cd_estabelecimento	number(10),
					ie_tipo_tiss		varchar2(40),
					dt_referencia		date,
					nr_seq_projeto		number(10));
				
type tiss_proj_xml_vet  is table of tiss_proj_xml index 	by binary_integer;
tiss_proj_xml_vet_w		tiss_proj_xml_vet;



procedure set_dt_referencia_tiss(dt_referencia_p date) as
begin
dt_referencia_w	:= dt_referencia_p;
end;

function get_dt_referencia_tiss return date as
begin
return dt_referencia_w;
end;

procedure set_gravar_log_inconsist(ie_gravar_log_inconsist_p varchar2) as
begin
ie_gravar_log_inconsist_w	:= ie_gravar_log_inconsist_p;

end;

function get_gravar_log_inconsist return varchar2 as
begin
return ie_gravar_log_inconsist_w;
end;


procedure gravar_log_inconsist(	nm_usuario_p		varchar2,
				nr_seq_inconsistencia_p	number,
				nr_sequencia_autor_p	number,
				nr_interno_conta_p	number,
				nr_seq_protocolo_p	number) as
begin

insert into tiss_log_inconsistencia
	(cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	nr_seq_inconsistencia,
	nr_sequencia_autor,
	nr_interno_conta,
	nr_seq_protocolo)
values	(wheb_usuario_pck.get_cd_estabelecimento,
	sysdate,
	nm_usuario_p,
	nr_seq_inconsistencia_p,
	nr_sequencia_autor_p,
	nr_interno_conta_p,
	nr_seq_protocolo_p);

end;

function tiss_obter_versao(	cd_convenio_p		number,
				cd_estabelecimento_p	number,
				dt_referencia_p	date default null,
			  	ie_tipo_tiss_p varchar2 default null) return varchar2 is
				 
nr_seq_xml_projeto_w	number(15,0);
ds_versao_w		varchar2(50);
dt_referencia_w		date;
indice_w		number(10);

nr_seq_tiss_conv_w	tiss_convenio.nr_sequencia%type;
ie_versao_tiss_w	tiss_convenio.ie_versao_tiss%type;
ie_tipo_tiss_w		tiss_convenio.ie_tipo_tiss%type;


begin
nr_seq_xml_projeto_w := 0;
dt_referencia_w	:= nvl(nvl(tiss_convenio_pck.get_dt_referencia_tiss,dt_referencia_p),dt_referencia_local_w);
ie_tipo_tiss_w := nvl(ie_tipo_tiss_p, '7');

for i in 1..versoes_w.count loop
	begin
	if 	(versoes_w(i).cd_convenio	= cd_convenio_p) and
		(versoes_w(i).cd_estabelecimento = cd_estabelecimento_p) and
		(versoes_w(i).dt_referencia	= dt_referencia_w) and
		(versoes_w(i).ie_tipo_tiss	= ie_tipo_tiss_w) then
		return versoes_w(i).ds_versao;
	end if;
	end;
end loop;

TISS_OBTER_VALORES(cd_convenio_p, 
	cd_estabelecimento_p, 
	ie_tipo_tiss_w,
	dt_referencia_w, 
	nr_seq_xml_projeto_w, 
	ie_versao_tiss_w, 
	nr_seq_tiss_conv_w);

if	(nr_seq_xml_projeto_w > 0
	and ie_versao_tiss_w is not null) then
	
	ds_versao_w := ie_versao_tiss_w;
	
else

	nr_seq_xml_projeto_w	:= tiss_obter_xml_projeto(cd_convenio_p, cd_estabelecimento_p, '7',dt_referencia_w);

	select	max(ds_versao)
	into	ds_versao_w
	from	xml_projeto
	where	nr_sequencia	= nr_seq_xml_projeto_w;

end if;

indice_w	:= versoes_w.count+1;

versoes_w(indice_w).cd_convenio		:= cd_convenio_p;
versoes_w(indice_w).cd_estabelecimento	:= cd_estabelecimento_p;
versoes_w(indice_w).dt_referencia	:= dt_referencia_w;
versoes_w(indice_w).ds_versao		:= ds_versao_w;
versoes_w(indice_w).ie_tipo_tiss	:= ie_tipo_tiss_w;

return ds_versao_w;

end tiss_obter_versao;

function get_tiss_param_convenio	(	cd_convenio_p	number,
						cd_estabelecimento_p	number,
						ie_opcao_p	varchar2) return varchar2 is
indice_w		number(10);	
ie_data_despesa_w	varchar2(40);
begin


for i in 1..tiss_param_conv_vet_w.count loop
	begin
	
	if	(tiss_param_conv_vet_w(i).cd_convenio		= cd_convenio_p) and	
		(tiss_param_conv_vet_w(i).cd_estabelecimento	= cd_estabelecimento_p) then
		
		if	(ie_opcao_p	=	'DD') then
			return tiss_param_conv_vet_w(i).IE_DATA_DESPESA;
		end if;
		
	end if;
	
	end;
end loop;




select	nvl(max(IE_DATA_DESPESA), 'C') 
into	ie_data_despesa_w
from	tiss_parametros_convenio
where	cd_estabelecimento	= cd_estabelecimento_p
and	cd_convenio		= cd_convenio_p;


indice_w	:= tiss_param_conv_vet_w.count+1;

tiss_param_conv_vet_w(indice_w).cd_convenio		:= cd_convenio_p;
tiss_param_conv_vet_w(indice_w).cd_estabelecimento	:= cd_estabelecimento_p;
tiss_param_conv_vet_w(indice_w).IE_DATA_DESPESA		:= ie_data_despesa_w;


if	(ie_opcao_p	=	'DD') then
	return ie_data_despesa_w;
end if;


return null;

end get_tiss_param_convenio;


function TISS_OBTER_XML_PROJETO
		(cd_convenio_p		in	number,
		cd_estabelecimento_p	in	number,
		ie_tipo_tiss_p		in	varchar2,
		dt_referencia_p 	in	date default null) return number is
		
		
dt_referencia_w		date;
indice_w		number(10);	
nr_seq_projeto_w	number(10);	
i 			integer;
begin

dt_referencia_w	:= nvl(dt_referencia_p,dt_referencia_local_w);

for i in 1..tiss_proj_xml_vet_w.count loop
begin
	if	(tiss_proj_xml_vet_w(i).cd_convenio		= cd_convenio_p) and
		(tiss_proj_xml_vet_w(i).cd_estabelecimento	= cd_estabelecimento_p) and
		(tiss_proj_xml_vet_w(i).dt_referencia		= dt_referencia_w) and 
		(tiss_proj_xml_vet_w(i).ie_tipo_tiss		= ie_tipo_tiss_p) then
		return  tiss_proj_xml_vet_w(i).nr_seq_projeto;
	end if;
end;
end loop;

nr_seq_projeto_w	:= tiss_obter_xml_projeto2(	cd_convenio_p,
							cd_estabelecimento_p,
							ie_tipo_tiss_p,
							dt_referencia_p);

indice_w						:= tiss_proj_xml_vet_w.count+1;
tiss_proj_xml_vet_w(indice_w).cd_convenio		:= cd_convenio_p;
tiss_proj_xml_vet_w(indice_w).cd_estabelecimento	:= cd_estabelecimento_p;
tiss_proj_xml_vet_w(indice_w).ie_tipo_tiss		:= ie_tipo_tiss_p;
tiss_proj_xml_vet_w(indice_w).nr_seq_projeto		:= nr_seq_projeto_w;
tiss_proj_xml_vet_w(indice_w).dt_referencia		:= dt_referencia_w;

return nr_seq_projeto_w;

end TISS_OBTER_XML_PROJETO;

end tiss_convenio_pck;
/
