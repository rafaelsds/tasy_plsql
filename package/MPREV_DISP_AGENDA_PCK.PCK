create or replace 
package mprev_disp_agenda_pck as

--Finalidade:  Retornar para a funcao da agenda os horarios nao disponiveis:
-- sem turno
-- intervalo
-- bloqueado

--Locais de chamada direta: 
--[X] Objetos do dicionario [ X] Tasy (Delphi/Java) [X] Portal [X]  Relatorios [X] Outros:

-- Pontos de atencao: 
-- Deve se ter atencao ao tipo de retorno das functions, algumas sao pipelineds e devem ser convertidas para tabelas usando table(nm_funciton(ds_parametro,ds_parametro2));

-- Tipo de Dados 
type t_horario_indisp_row is record 
	(ie_tipo		varchar2(4),
	ds_tipo			varchar2(30),	
	ie_status		varchar2(2),
	ds_motivo_bloqueio	agenda_motivo.ds_motivo%type,
	dt_inicio_turno		date,
	dt_fim_turno		date,
	dt_horario_inicio	date,
	dt_horario_fim		date,
	nr_minutos_duracao	number(10)
	);
type t_horario_indisp_data is table of t_horario_indisp_row;

-- FUNCTIONS
function obter_horarios_indisp(	nr_seq_equipe_p		number,
				ie_loc_interno_p	varchar2,
				ie_loc_tipo_atend_p	varchar2,
				cd_agenda_p		number,
				cd_estabelecimento_p	number,
				dt_dia_agenda_p		date,
				ie_tipo_p		varchar2)
				return t_horario_indisp_data pipelined;
end mprev_disp_agenda_pck;
/

-- Corpo da PCK
create or replace package body mprev_disp_agenda_pck as

	function obter_horarios_indisp(	nr_seq_equipe_p		number,
					ie_loc_interno_p	varchar2,
					ie_loc_tipo_atend_p	varchar2,
					cd_agenda_p		number,
					cd_estabelecimento_p	number,
					dt_dia_agenda_p		date,
					ie_tipo_p		varchar2)
				return t_horario_indisp_data pipelined is

	linha_w			t_horario_indisp_row;
	i			number(10);
	j			number(10);
	tipo_w			varchar2(30);
	dt_dia_agenda_w		date;
	dt_comeco_turno_w	date;
	hr_final_anterior_w	date;
	hr_final_turno_anterior_w	date;
	cd_agenda_w		number(10);
	dt_horario_inicio_w	date;
	dt_horario_fim_w	date;
	ie_dia_semana_w		agenda_turno.ie_dia_semana%type;
	gerou_linha_w	boolean;
	
	ds_tipo_w		varchar2(30);
	ds_tipo_sem_horario_w	varchar2(30);
	ds_tipo_intervalo_w	varchar2(30);
	ds_tipo_bloqueio_w	varchar2(30);
	-- retorno 
	qt_intervalo_turno_w	number(10);
	hr_inicial_turno_w	date;
	hr_final_turno_w	date;
	ie_bloqueio_feriado_w	varchar2(1);
	nr_minutos_duracao_w	number(10);
	dt_fim_bloqueio_ant_w	date	:= null;

	cursor C02(cd_agenda_p		number, dt_dia_agenda_p date) is
		select	ie_ordem, -- Criado para o sem intervalo (SHD2), pois deve ser o ultimo 
			ie_tipo,
			ie_dia_semana,
			dt_inicio_turno,
			dt_fim_turno,
			ds_motivo_bloqueio
		from	(-- Inicio dia 
			select	1 ie_ordem,
				'SHD1' ie_tipo,
				a.ie_dia_semana,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_inicial, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_inicial, 0), 0),
				0,
				0) dt_inicio_turno,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_final, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_final, 0), 0),
				0,
				0) dt_fim_turno,
				null ds_motivo_bloqueio
			from	agenda_turno a
			where	a.cd_agenda 	= cd_agenda_p
			and	(a.ie_dia_semana = ie_dia_semana_w or (a.ie_dia_semana = 9 and ie_dia_semana_w not in (1,7)))
			and	dt_dia_agenda_w between a.dt_inicio_vigencia and nvl(a.dt_final_vigencia, dt_dia_agenda_w)
			and	(obter_se_gerar_turno_agecons(a.dt_inicio_vigencia,a.ie_frequencia,dt_dia_agenda_w) = 'S')
			and	((Obter_Semana_Dia_Agecons(dt_dia_agenda_w,a.ie_dia_semana) = nvl(a.ie_semana,0)) or (nvl(a.ie_semana,0) = 0))
			union all
			-- Intervalo 
			select	1 ie_ordem,
				'INT' ie_tipo,
				a.ie_dia_semana,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_inicial_intervalo, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_inicial_intervalo, 0), 0),
				0,
				0) dt_inicio_turno,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_final_intervalo, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_final_intervalo, 0), 0),
				0,
				0) dt_fim_turno,				
				null ds_motivo_bloqueio
			from	agenda_turno a
			where	a.cd_agenda 	= cd_agenda_p
			and	a.hr_inicial_intervalo is not null
			and	a.hr_final_intervalo is not null
			and	(a.ie_dia_semana = ie_dia_semana_w or (a.ie_dia_semana = 9 and ie_dia_semana_w not in (1,7)))
			and	dt_dia_agenda_w between a.dt_inicio_vigencia and nvl(a.dt_final_vigencia, dt_dia_agenda_w)
			union all
			-- Bloqueio - Dia inicio 
			select	1 ie_ordem,
				'BLO' ie_tipo,
				a.ie_dia_semana,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_inicio_bloqueio, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_inicio_bloqueio, 0), 0),
				0,
				0) dt_inicio_turno,				
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', hr_final_turno_w, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', hr_final_turno_w, 0), 0),
				0,
				0) dt_fim_turno,				
				obter_valor_dominio(1007, a.ie_motivo_bloqueio) ds_motivo_bloqueio
			from	agenda_bloqueio a
			where	a.cd_agenda 	= cd_agenda_p
			and	a.hr_inicio_bloqueio is not null
			and	a.hr_final_bloqueio is not null
			and	dt_dia_agenda_w = pkg_date_utils.start_of(a.dt_inicial,'DD',0)
			and	dt_dia_agenda_w <> pkg_date_utils.start_of(a.dt_final,'DD',0)
			and	a.ie_dia_semana is null
			union all
			-- Bloqueio - Dias intermediarios 
			select	1 ie_ordem,
				'BLO' ie_tipo,
				a.ie_dia_semana,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', hr_inicial_turno_w, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', hr_inicial_turno_w, 0), 0),
				0,
				0)  dt_inicio_turno,				
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', hr_final_turno_w, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', hr_final_turno_w, 0), 0),
				0,
				0) dt_fim_turno,
				obter_valor_dominio(1007, a.ie_motivo_bloqueio) ds_motivo_bloqueio
			from	agenda_bloqueio a
			where	a.cd_agenda 	= cd_agenda_p
			and	a.hr_inicio_bloqueio is not null
			and	a.hr_final_bloqueio is not null
			and	dt_dia_agenda_w > pkg_date_utils.start_of(a.dt_inicial,'DD',0)
			and	dt_dia_agenda_w < pkg_date_utils.start_of(a.dt_final,'DD',0)
			and	a.ie_dia_semana is null
			union all
			-- Bloqueio - Dia final 
			select	1 ie_ordem,
				'BLO' ie_tipo,
				a.ie_dia_semana,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', hr_inicial_turno_w, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', hr_inicial_turno_w, 0), 0),
				0,
				0) dt_inicio_turno,				
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_final_bloqueio, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_final_bloqueio, 0), 0),
				0,
				0) dt_fim_turno,				
				obter_valor_dominio(1007, a.ie_motivo_bloqueio) ds_motivo_bloqueio
			from	agenda_bloqueio a
			where	a.cd_agenda 	= cd_agenda_p
			and	a.hr_inicio_bloqueio is not null
			and	a.hr_final_bloqueio is not null
			and	dt_dia_agenda_w = pkg_date_utils.start_of(a.dt_final,'DD',0)
			and	dt_dia_agenda_w <> pkg_date_utils.start_of(a.dt_inicial,'DD',0)
			and	a.ie_dia_semana is null
			union all
			-- Bloqueio - Comeca e termina no mesmo dia 
			select	1 ie_ordem,
				'BLO' ie_tipo,
				a.ie_dia_semana,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_inicio_bloqueio, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_inicio_bloqueio, 0), 0),
				0,
				0) dt_inicio_turno,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_final_bloqueio, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_final_bloqueio, 0), 0),
				0,
				0) dt_fim_turno,				
				obter_valor_dominio(1007, a.ie_motivo_bloqueio) ds_motivo_bloqueio
			from	agenda_bloqueio a
			where	a.cd_agenda 	= cd_agenda_p
			and	a.hr_inicio_bloqueio is not null
			and	a.hr_final_bloqueio is not null
			and	dt_dia_agenda_w = pkg_date_utils.start_of(a.dt_final,'DD',0)
			and	dt_dia_agenda_w = pkg_date_utils.start_of(a.dt_inicial,'DD',0)
			and	a.ie_dia_semana is null
			union all
			-- Bloqueio - Para dia da semana
			select	1 ie_ordem,
				'BLO' ie_tipo,
				a.ie_dia_semana,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_inicio_bloqueio, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_inicio_bloqueio, 0), 0),
				0,
				0) dt_inicio_turno,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_final_bloqueio, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_final_bloqueio, 0), 0),
				0,
				0) dt_fim_turno,
				obter_valor_dominio(1007, a.ie_motivo_bloqueio) ds_motivo_bloqueio
			from	agenda_bloqueio a
			where	a.cd_agenda 	= cd_agenda_p
			and	a.hr_inicio_bloqueio is not null
			and	a.hr_final_bloqueio is not null
			and	dt_dia_agenda_w >= pkg_date_utils.start_of(a.dt_inicial,'DD',0)
			and	dt_dia_agenda_w <= pkg_date_utils.start_of(a.dt_final,'DD',0)
			and	(a.ie_dia_semana  = ie_dia_semana_w or a.ie_dia_semana = 9)
			union all
			-- Fim dia 
			select	2 ie_ordem,
				'SHD2' ie_tipo,
				a.ie_dia_semana,
				pkg_date_utils.get_time(
				dt_dia_agenda_w,
				nvl(pkg_date_utils.extract_field('HOUR', a.hr_final, 0), 0),
				nvl(pkg_date_utils.extract_field('MINUTE', a.hr_final, 0), 0),
				0,
				0) dt_inicio_turno,
				hr_final_turno_w dt_fim_turno,
				null ds_motivo_bloqueio
			from	agenda_turno a
			where	a.cd_agenda 	= cd_agenda_p
			and	(a.ie_dia_semana = ie_dia_semana_w or (a.ie_dia_semana = 9 and ie_dia_semana_w not in (1,7)))
			and	dt_dia_agenda_w between a.dt_inicio_vigencia and nvl(a.dt_final_vigencia, dt_dia_agenda_w)
			and	(obter_se_gerar_turno_agecons(a.dt_inicio_vigencia,a.ie_frequencia,dt_dia_agenda_w) = 'S')
			and	((Obter_Semana_Dia_Agecons(dt_dia_agenda_w,a.ie_dia_semana) = nvl(a.ie_semana,0)) or (nvl(a.ie_semana,0) = 0))
			) a
		group by
			ie_ordem,
			ie_tipo,
			ie_dia_semana,
			dt_inicio_turno,
			dt_fim_turno,
			ds_motivo_bloqueio
		order by
			ie_ordem,
			dt_inicio_turno;


	begin
	dt_dia_agenda_w	:= pkg_date_utils.start_of(dt_dia_agenda_p,'DD',0);
	ie_dia_semana_w	:= pkg_date_utils.get_weekday(dt_dia_agenda_w);
	cd_agenda_w	:= cd_agenda_p;

	ds_tipo_sem_horario_w	:= substr(obter_texto_tasy(317449,null),1,30);
	ds_tipo_intervalo_w	:= substr(obter_texto_tasy(317450,null),1,30);
	ds_tipo_bloqueio_w	:= substr(obter_texto_tasy(317476,null),1,30);

	if	(nr_seq_equipe_p is not null) or
		(ie_loc_interno_p is not null) or
		(ie_loc_tipo_atend_p is not null) then
		cd_agenda_w	:= null;
	end if;

	-- Se nao achou, pegar primeiro horario do dia 
	mprev_obter_horarios_agenda(	dt_dia_agenda_w,
					nr_seq_equipe_p,
					ie_loc_interno_p,
					ie_loc_tipo_atend_p,
					cd_agenda_w,
					cd_estabelecimento_p,
					qt_intervalo_turno_w,
					hr_inicial_turno_w,
					hr_final_turno_w,
					ie_bloqueio_feriado_w);

	hr_final_anterior_w		:= null;
	hr_final_turno_anterior_w	:= null;
	gerou_linha_w	:= false;
	for r_C02 in C02(cd_agenda_p, dt_dia_agenda_w) loop
		dt_horario_inicio_w	:= null;
		dt_horario_fim_w	:= null;
		nr_minutos_duracao_w	:= 0;
		if	(r_C02.ie_tipo = 'SHD1') then
			ds_tipo_w	:= ds_tipo_sem_horario_w;

			nr_minutos_duracao_w	:= round((r_c02.dt_inicio_turno - nvl(hr_final_turno_anterior_w,hr_inicial_turno_w)) * 1440);

			dt_horario_inicio_w	:= nvl(hr_final_turno_anterior_w,hr_inicial_turno_w);
			dt_horario_fim_w	:= dt_horario_inicio_w + (nr_minutos_duracao_w/1440);

		elsif	(r_c02.ie_tipo = 'INT') then
			ds_tipo_w	:= ds_tipo_intervalo_w;

			dt_horario_inicio_w	:= r_c02.dt_inicio_turno;
			dt_horario_fim_w	:= r_c02.dt_fim_turno;

			nr_minutos_duracao_w	:= round((dt_horario_fim_w - dt_horario_inicio_w) * 1440);
		elsif	(r_c02.ie_tipo = 'BLO') then
			ds_tipo_w	:= ds_tipo_bloqueio_w;

			if 	(to_date(r_c02.dt_inicio_turno, 'dd/mm/yyyy') = to_date(dt_dia_agenda_w, 'dd/mm/yyyy')) then
				dt_horario_inicio_w	:= r_c02.dt_inicio_turno;
			else 
				dt_horario_inicio_w	:= hr_inicial_turno_w;
			end if;
			
			--Verificar se esta sobrescrevendo outro bloqueio 
			if	(dt_horario_inicio_w <= dt_fim_bloqueio_ant_w) then
				dt_horario_inicio_w	:= dt_fim_bloqueio_ant_w;
			end if;

			if 	(to_date(r_c02.dt_fim_turno, 'dd/mm/yyyy') = to_date(dt_dia_agenda_w, 'dd/mm/yyyy')) then
				dt_horario_fim_w	:= r_c02.dt_fim_turno;
			else 
				dt_horario_fim_w	:= hr_final_turno_w;
			end if;

			nr_minutos_duracao_w	:= round((dt_horario_fim_w - dt_horario_inicio_w) * 1440);
			dt_fim_bloqueio_ant_w	:= dt_horario_fim_w;
		elsif	(r_C02.ie_tipo = 'SHD2') then
			ds_tipo_w	:= ds_tipo_sem_horario_w;

			dt_horario_inicio_w	:= r_c02.dt_inicio_turno;
			dt_horario_fim_w	:= r_c02.dt_fim_turno;

			nr_minutos_duracao_w	:= round((dt_horario_fim_w - dt_horario_inicio_w) * 1440);
		end if;

		-- So criar linha caso o horario ja nao tenha sido colocado como indisponivel 
		if	(((r_C02.ie_tipo = 'SHD2') and (dt_horario_inicio_w > hr_final_anterior_w) or (hr_final_anterior_w is null))
            or ((r_C02.ie_tipo != 'SHD2') and (dt_horario_inicio_w >= hr_final_anterior_w) or (hr_final_anterior_w is null))) then
			
			gerou_linha_w	:= true;
			
			linha_w.ie_tipo			:= r_C02.ie_tipo;
			linha_w.ds_tipo			:= ds_tipo_w;
			linha_w.ie_status		:= 'SH';
			linha_w.ds_motivo_bloqueio	:= r_C02.ds_motivo_bloqueio;
			linha_w.dt_inicio_turno		:= r_c02.dt_inicio_turno;
			linha_w.dt_fim_turno		:= r_C02.dt_fim_turno;
			linha_w.dt_horario_inicio	:= dt_horario_inicio_w;
			linha_w.dt_horario_fim		:= dt_horario_fim_w;
			linha_w.nr_minutos_duracao	:= nr_minutos_duracao_w;

			pipe row(linha_w);

			hr_final_anterior_w	:= dt_horario_fim_w;
			-- Caso seja SHD, deve alimentar tambem qual era o turno anterior 
			if	(r_C02.ie_tipo = 'SHD1') then
				hr_final_turno_anterior_w	:= r_C02.dt_fim_turno;
			elsif	(r_C02.ie_tipo = 'SHD2') then
				hr_final_turno_anterior_w	:= dt_horario_fim_w;
			end if;
		end if;
	end loop;
	
	-- Se nao gerou nenhum horario e porque nao tem turno, deve entao voltar indisponivel do inicio ao fim do turno da equipe 
	if	(nr_seq_equipe_p is not null) and
		(not gerou_linha_w) then
		nr_minutos_duracao_w	:= round((hr_final_turno_w - hr_inicial_turno_w) * 1440);
	
		linha_w.ie_tipo			:= 'SHD3';
		linha_w.ds_tipo			:= ds_tipo_sem_horario_w;
		linha_w.ie_status		:= 'SH';
		linha_w.ds_motivo_bloqueio	:= null;
		linha_w.dt_inicio_turno		:= hr_inicial_turno_w;
		linha_w.dt_fim_turno		:= hr_final_turno_w;
		linha_w.dt_horario_inicio	:= hr_inicial_turno_w;
		linha_w.dt_horario_fim		:= hr_final_turno_w;
		linha_w.nr_minutos_duracao	:= nr_minutos_duracao_w;

		pipe row(linha_w);
	end if;

	return;

	end obter_horarios_indisp;

end mprev_disp_agenda_pck;
/