CREATE OR REPLACE PACKAGE PKG_VTE_PROPHYLAXIS AS
  --
  FUNCTION GET_DT_FIRST_DEV(NR_ATEND number, 
                            DT_ENTRADA_SETOR DATE, 
                            DT_SAIDA_SETOR DATE)
           RETURN DATE;
  --
  FUNCTION GET_CD_FIRST_MED(NR_ATEND number, 
                            DT_ENTRADA_SETOR DATE, 
                            DT_SAIDA_SETOR DATE)
           RETURN CLASSE_MATERIAL.IE_CLASSE_MATERIAL_REL%TYPE;
  --
  FUNCTION GET_DT_FIRST_MED(NR_ATEND number, 
                            DT_ENTRADA_SETOR DATE, 
                            DT_SAIDA_SETOR DATE)
           RETURN DATE;
  --
  TYPE T_TAB_CD_PES_FIS IS TABLE OF PESSOA_FISICA.CD_PESSOA_FISICA%TYPE;
  --
  FUNCTION GET_TAB_CD_PES_FIS(P_START_DATE DATE,
                              P_END_DATE   DATE,
                              P_YEAR       NUMBER,
                              P_QUARTER    NUMBER,
                              P_ESTAB      NUMBER,
                              P_SETOR      NUMBER)
    RETURN T_TAB_CD_PES_FIS
    PIPELINED;  
  --
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0,
                P_YEAR       IN NUMBER,
                P_QUARTER    IN NUMBER DEFAULT 1,
                P_TYPE       IN NUMBER DEFAULT 1,
                P_PATIENT    IN VARCHAR2);
  --
END PKG_VTE_PROPHYLAXIS;
/
CREATE OR REPLACE PACKAGE BODY PKG_VTE_PROPHYLAXIS AS

  PROCEDURE TABLE_CLEANING IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VTE_PROPHYLAXIS';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VTE_PROPHYLAXIS_ESTAB';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_VTE_PROPHYLAXIS_UNIT';
  END TABLE_CLEANING;
  --
  FUNCTION GET_TAB_CD_PES_FIS(P_START_DATE DATE,
                              P_END_DATE   DATE,
                              P_YEAR       NUMBER,
                              P_QUARTER    NUMBER,
                              P_ESTAB      NUMBER,
                              P_SETOR      NUMBER)
                              
    RETURN T_TAB_CD_PES_FIS PIPELINED IS
    
  V_START_DATE DATE;
  V_END_DATE   DATE;
  NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
  
  CURSOR C_CD_PES_FIS IS
    SELECT DISTINCT CD_PESSOA_FISICA
      FROM (SELECT S.CD_ESTABELECIMENTO,
                   U.CD_SETOR_ATENDIMENTO,
                   A.CD_PESSOA_FISICA,
                   U.NR_ATENDIMENTO,
                   LEAST(V_END_DATE + 1, SYSDATE) DT_FIM,
                   MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                   PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                        U.DT_ENTRADA_UNIDADE,
                                                        U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR
              FROM ATENDIMENTO_PACIENTE A,
                   ATEND_PACIENTE_UNIDADE U,
                   SETOR_ATENDIMENTO S
             WHERE A.NR_ATENDIMENTO = U.NR_ATENDIMENTO
               AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
               AND S.CD_CLASSIF_SETOR = '4'
               AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
               AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO)
               AND DECODE(NVL(P_ESTAB, 0), 0, 0, S.CD_ESTABELECIMENTO) = NVL(P_ESTAB, 0)
               AND DECODE(NVL(P_SETOR, 0), 0, 0, U.CD_SETOR_ATENDIMENTO) = NVL(P_SETOR, 0)
               AND A.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(A.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
               AND (A.DT_ALTA IS NULL OR
                    A.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(A.CD_ESTABELECIMENTO, V_START_DATE))
               AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
               AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                            U.DT_ENTRADA_UNIDADE,
                                                            U.CD_SETOR_ATENDIMENTO),
                       PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)) >=
                   PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)
               GROUP BY U.NR_ATENDIMENTO,
                        A.CD_PESSOA_FISICA,
                        S.CD_ESTABELECIMENTO,
                        U.CD_SETOR_ATENDIMENTO,
                        PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                             U.DT_ENTRADA_UNIDADE,
                                                             U.CD_SETOR_ATENDIMENTO))
     WHERE NVL(DT_SAIDA_SETOR, DT_FIM) - DT_ENTRADA_SETOR >= 1; 
  TYPE T_CD_PES_FIS IS TABLE OF C_CD_PES_FIS%ROWTYPE;
  R_CD_PES_FIS T_CD_PES_FIS;
  
  BEGIN
  
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
  
    IF P_YEAR IS NOT NULL AND P_QUARTER IS NOT NULL THEN
    
      SELECT GREATEST(INI, NVL(P_START_DATE, INI)) INI,
             LEAST(FIM, NVL(P_END_DATE, FIM)) FIM
        INTO V_START_DATE,
             V_END_DATE
        FROM (SELECT TO_DATE(LPAD((P_QUARTER * 3) - 2, 2, '0') || P_YEAR, 'MMYYYY') INI,
                     LAST_DAY(TO_DATE(LPAD((P_QUARTER * 3), 2, '0') || P_YEAR, 'MMYYYY')) FIM
                FROM DUAL)
       WHERE INI <= NVL(P_END_DATE, INI)
         AND FIM >= NVL(P_START_DATE, FIM);
  
      OPEN C_CD_PES_FIS;
      LOOP
        FETCH C_CD_PES_FIS BULK COLLECT INTO R_CD_PES_FIS LIMIT 1000;
          
          BEGIN
            FOR i IN R_CD_PES_FIS.FIRST..R_CD_PES_FIS.LAST LOOP
              PIPE ROW(R_CD_PES_FIS(i).CD_PESSOA_FISICA);  
            END LOOP;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;  

        EXIT WHEN C_CD_PES_FIS%NOTFOUND; 
     
      END LOOP;
      CLOSE C_CD_PES_FIS;                                  
  
    END IF;
    
    RETURN;
  END GET_TAB_CD_PES_FIS;
  --
  FUNCTION GET_DT_FIRST_MED(NR_ATEND number, 
                            DT_ENTRADA_SETOR DATE, 
                            DT_SAIDA_SETOR DATE)
           RETURN DATE IS
           
    dt_treat_w  DATE;

  begin

    SELECT MIN(pme.dt_prescricao)
      INTO dt_treat_w
      FROM prescr_medica pme
     INNER JOIN prescr_material pma
        ON pma.nr_prescricao = pme.nr_prescricao
     INNER JOIN material m
        ON m.cd_material = pma.cd_material
       AND m.ie_situacao = 'A'
     INNER JOIN classe_material cm
        ON cm.cd_classe_material = m.cd_classe_material
       AND cm.ie_situacao = 'A'
       AND cm.ie_classe_material_rel IN (27, 28, 29, 30, 31, 32)
       AND (cm.ie_classe_material_rel <> 27 OR
            (cm.ie_classe_material_rel = 27 AND 
              (pma.qt_dose >= 500 or
               exists (select 1 
                         from via_aplicacao v 
                        where pma.ie_via_aplicacao = v.ie_via_aplicacao 
                          and v.ie_situacao = 'A' 
                          and v.ie_tipo_rel in (1, 2, 3, 4)))))
     WHERE pme.nr_atendimento = NR_ATEND
       AND pme.dt_prescricao BETWEEN DT_ENTRADA_SETOR AND DT_SAIDA_SETOR;

    RETURN dt_treat_w;

  END get_dt_first_med;
  --
  FUNCTION GET_CD_FIRST_MED(NR_ATEND number, 
                            DT_ENTRADA_SETOR DATE, 
                            DT_SAIDA_SETOR DATE)
           RETURN CLASSE_MATERIAL.IE_CLASSE_MATERIAL_REL%TYPE IS
           
    dt_treat_w  DATE;
    nr_seq_w    PRESCR_MATERIAL.NR_SEQ_INTERNO%TYPE; 
    cd_treat_w  CLASSE_MATERIAL.IE_CLASSE_MATERIAL_REL%TYPE;

  begin

    dt_treat_w := GET_DT_FIRST_MED(NR_ATEND, DT_ENTRADA_SETOR, DT_SAIDA_SETOR);

    if dt_treat_w is not null then
     
      SELECT MIN(pma.nr_seq_interno)
        INTO nr_seq_w
        FROM prescr_medica pme
       INNER JOIN prescr_material pma
          ON pma.nr_prescricao = pme.nr_prescricao
       INNER JOIN material m
          ON m.cd_material = pma.cd_material
         AND m.ie_situacao = 'A'
       INNER JOIN classe_material cm
          ON cm.cd_classe_material = m.cd_classe_material
         AND cm.ie_situacao = 'A'
         AND cm.ie_classe_material_rel IN (27, 28, 29, 30, 31, 32)
         AND (cm.ie_classe_material_rel <> 27 OR
              (cm.ie_classe_material_rel = 27 AND 
               (pma.qt_dose >= 500 or
                exists (select 1 
                          from via_aplicacao v 
                         where pma.ie_via_aplicacao = v.ie_via_aplicacao 
                           and v.ie_situacao = 'A' 
                           and v.ie_tipo_rel in (1, 2, 3, 4)))))
       WHERE pme.nr_atendimento = NR_ATEND
         AND pme.dt_prescricao = dt_treat_w;
     
      IF nr_seq_w is not null THEN
        SELECT MIN(cm.ie_classe_material_rel)
          INTO cd_treat_w
          FROM prescr_material pma 
         INNER JOIN material m
            ON m.cd_material = pma.cd_material
         INNER JOIN classe_material cm
            ON cm.cd_classe_material = m.cd_classe_material
         WHERE pma.nr_seq_interno = nr_seq_w;  
      END IF;
     
    end if;
    
    RETURN cd_treat_w;

  END get_cd_first_med;
  --
  FUNCTION GET_DT_FIRST_DEV(NR_ATEND number, 
                            DT_ENTRADA_SETOR DATE, 
                            DT_SAIDA_SETOR DATE)
           RETURN DATE IS
           
    dt_treat_w  DATE;

  begin

    SELECT MIN(apd.dt_instalacao)
      INTO dt_treat_w
      FROM atend_pac_dispositivo apd
     INNER JOIN dispositivo d
        ON d.nr_sequencia = apd.nr_seq_dispositivo
       AND d.ie_situacao = 'A'
       AND d.ie_tipo_rel in (1, 2)
     WHERE apd.nr_atendimento = NR_ATEND
       AND apd.dt_instalacao BETWEEN DT_ENTRADA_SETOR AND DT_SAIDA_SETOR;

    RETURN dt_treat_w;

  END get_dt_first_dev;
  --
  PROCEDURE PRE_PROCESS(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_CD_ESTAB IN NUMBER,
    P_CD_SETOR IN NUMBER,
    P_YEAR IN NUMBER,
    P_QUARTER IN NUMBER,
    P_TYPE IN NUMBER,
    P_PATIENT IN VARCHAR2) IS
    --
    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
    --
    CURSOR C_ATEND (V_START_DATE DATE,
                    V_END_DATE   DATE,
                    V_SETOR      NUMBER,
                    V_PATIENT    VARCHAR2) IS 
      SELECT CD_ESTABELECIMENTO,
             CD_SETOR_ATENDIMENTO,
             NR_ATENDIMENTO,
             DT_ENTRADA_SETOR,
             DT_SAIDA_SETOR,
             IE_AT_RISK,
             DT_FIRST_MED,
             IE_FIRST_MED,
             DT_FIRST_DEV,
             DT_FIRST_TREAT,
             IE_FIRST_TREAT,
             CASE WHEN IE_AT_RISK = 0 THEN 0
                  WHEN DT_FIRST_TREAT IS NULL THEN 4
                  WHEN TO_DATE(DT_FIRST_TREAT) - DT_ENTRADA_SETOR < 1 THEN 1
                  WHEN TO_DATE(DT_FIRST_TREAT) - DT_ENTRADA_SETOR <= 2 THEN 2
                  ELSE 3
             END IE_TIME_TREAT,
             IE_TYPE_TREAT
        FROM (SELECT CD_ESTABELECIMENTO,
                     CD_SETOR_ATENDIMENTO,
                     NR_ATENDIMENTO,
                     DT_ENTRADA_SETOR,
                     DT_SAIDA_SETOR,
                     IE_AT_RISK,
                     DT_FIRST_MED,
                     DECODE(CD_FIRST_MED, NULL, 0, 27, 1, 28, 2, 29, 3, 30, 4, 31, 5, 32, 6, NULL) IE_FIRST_MED,
                     DT_FIRST_DEV,
                     DECODE(IE_TYPE_TREAT, 0, NULL, 1, DT_FIRST_DEV, 2, DT_FIRST_MED, LEAST(DT_FIRST_DEV, DT_FIRST_MED)) DT_FIRST_TREAT,
                     DECODE(IE_TYPE_TREAT, 3, DECODE(LEAST(DT_FIRST_DEV, DT_FIRST_MED), DT_FIRST_MED, 2, 1), IE_TYPE_TREAT) IE_FIRST_TREAT,
                     IE_TYPE_TREAT
                FROM (SELECT CD_ESTABELECIMENTO,
                             CD_SETOR_ATENDIMENTO,
                             NR_ATENDIMENTO,
                             DT_ENTRADA_SETOR,
                             DT_SAIDA_SETOR,
                             IE_AT_RISK,
                             DT_FIRST_MED,
                             DECODE(P_TYPE, 3, NULL, DECODE(DT_FIRST_MED, NULL, NULL, GET_CD_FIRST_MED(NR_ATENDIMENTO, DT_ENTRADA_SETOR, NVL(DT_SAIDA_SETOR, DT_FIM)))) CD_FIRST_MED,
                             DT_FIRST_DEV,
                             CASE WHEN DT_FIRST_MED IS NULL AND DT_FIRST_DEV IS NULL THEN 0
                                  WHEN DT_FIRST_MED IS NULL AND DT_FIRST_DEV IS NOT NULL THEN 1
                                  WHEN DT_FIRST_MED IS NOT NULL AND DT_FIRST_DEV IS NULL THEN 2
                                  ELSE 3
                             END IE_TYPE_TREAT
                        FROM (SELECT CD_ESTABELECIMENTO,
                                     CD_SETOR_ATENDIMENTO,
                                     NR_ATENDIMENTO,
                                     DT_FIM,
                                     DT_ENTRADA_SETOR,
                                     DT_SAIDA_SETOR,
                                     IE_AT_RISK,
                                     DECODE(IE_AT_RISK, 0, NULL, GET_DT_FIRST_MED(NR_ATENDIMENTO, DT_ENTRADA_SETOR, NVL(DT_SAIDA_SETOR, DT_FIM))) DT_FIRST_MED,
                                     DECODE(IE_AT_RISK, 0, NULL, GET_DT_FIRST_DEV(NR_ATENDIMENTO, DT_ENTRADA_SETOR, NVL(DT_SAIDA_SETOR, DT_FIM))) DT_FIRST_DEV
                                FROM (SELECT CD_ESTABELECIMENTO,
                                             CD_SETOR_ATENDIMENTO,
                                             NR_ATENDIMENTO,
                                             DT_FIM,
                                             DT_ENTRADA_SETOR,
                                             DT_SAIDA_SETOR,
                                             CASE WHEN QT_DAYS_UNIT < 1 THEN 0 ELSE 1 END IE_AT_RISK
                                        FROM (SELECT CD_ESTABELECIMENTO,
                                                     CD_SETOR_ATENDIMENTO,
                                                     NR_ATENDIMENTO,
                                                     DT_FIM,
                                                     DT_ENTRADA_SETOR,
                                                     DT_SAIDA_SETOR,
                                                     NVL(DT_SAIDA_SETOR, DT_FIM) - DT_ENTRADA_SETOR QT_DAYS_UNIT
                                                FROM (SELECT S.CD_ESTABELECIMENTO,
                                                             U.CD_SETOR_ATENDIMENTO,
                                                             U.NR_ATENDIMENTO,
                                                             LEAST(V_END_DATE + 1, SYSDATE) DT_FIM,
                                                             MIN(U.DT_ENTRADA_UNIDADE) DT_ENTRADA_SETOR,
                                                             PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                                  U.DT_ENTRADA_UNIDADE,
                                                                                                  U.CD_SETOR_ATENDIMENTO) DT_SAIDA_SETOR                                              
                                                        FROM ATENDIMENTO_PACIENTE A,
                                                             ATEND_PACIENTE_UNIDADE U,
                                                             SETOR_ATENDIMENTO S
                                                       WHERE A.NR_ATENDIMENTO = U.NR_ATENDIMENTO
                                                         AND S.CD_SETOR_ATENDIMENTO = U.CD_SETOR_ATENDIMENTO
                                                         AND S.CD_CLASSIF_SETOR = '4'
                                                         AND U.IE_PASSAGEM_SETOR NOT IN ('S', 'L')
                                                         AND EXISTS (SELECT 1 FROM USUARIO_SETOR US WHERE US.NM_USUARIO_PARAM = NM_USUARIO_W AND US.CD_SETOR_ATENDIMENTO = S.CD_SETOR_ATENDIMENTO)
                                                         AND DECODE(P_CD_ESTAB, 0, 0, S.CD_ESTABELECIMENTO) = P_CD_ESTAB
                                                         AND DECODE(V_SETOR, 0, 0, S.CD_SETOR_ATENDIMENTO) = V_SETOR
                                                         AND (V_PATIENT IS NULL OR A.CD_PESSOA_FISICA = V_PATIENT)
                                                         AND A.DT_ENTRADA <= PKG_ATEND_PAC_UNID.GET_SYS_DT(A.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))
                                                         AND (A.DT_ALTA IS NULL OR
                                                              A.DT_ALTA >= PKG_ATEND_PAC_UNID.GET_SYS_DT(A.CD_ESTABELECIMENTO, V_START_DATE))
                                                         AND U.DT_ENTRADA_UNIDADE <= PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, LEAST(FIM_DIA(V_END_DATE), SYSDATE))         
                                                         AND NVL(PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                                      U.DT_ENTRADA_UNIDADE,
                                                                                                      U.CD_SETOR_ATENDIMENTO),
                                                                 PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)) >=
                                                             PKG_ATEND_PAC_UNID.GET_SYS_DT(S.CD_ESTABELECIMENTO, V_START_DATE)
                                                       GROUP BY U.NR_ATENDIMENTO,
                                                                S.CD_ESTABELECIMENTO,
                                                                U.CD_SETOR_ATENDIMENTO,
                                                                PKG_ATEND_PAC_UNID.GET_MAX_EXIT_UNIT(U.NR_ATENDIMENTO,
                                                                                                     U.DT_ENTRADA_UNIDADE,
                                                                                                     U.CD_SETOR_ATENDIMENTO))
                                                WHERE P_TYPE <> 3 
                                                   OR NVL(DT_SAIDA_SETOR, DT_FIM) - DT_ENTRADA_SETOR >= 1)))));
    --
    TYPE T_ATEND IS TABLE OF C_ATEND%ROWTYPE;
    R_ATEND T_ATEND;
    --
    CURSOR C_ESTAB IS
      SELECT D.CD_ESTABELECIMENTO,
             D.QUARTER,
             D.QT_STAYS,
             D.QT_DAYS,
             D.QT_AT_RISK,
             D.QT_TIME_1,
             D.QT_TIME_2,
             D.QT_TIME_3,
             D.QT_TIME_4,
             DECODE(P_TYPE, 1, DECODE(D.QT_AT_RISK, 0, 0, ROUND(100* D.QT_TIME_1 / D.QT_AT_RISK, 1)), 0) AVG_TIME_1,
             DECODE(P_TYPE, 1, DECODE(D.QT_AT_RISK, 0, 0, ROUND(100* D.QT_TIME_2 / D.QT_AT_RISK, 1)), 0) AVG_TIME_2,
             DECODE(P_TYPE, 1, DECODE(D.QT_AT_RISK, 0, 0, ROUND(100* D.QT_TIME_3 / D.QT_AT_RISK, 1)), 0) AVG_TIME_3,
             DECODE(P_TYPE, 1, DECODE(D.QT_AT_RISK, 0, 0, ROUND(100* D.QT_TIME_4 / D.QT_AT_RISK, 1)), 0) AVG_TIME_4,
             D.TOTAL_TREAT,
             D.QT_TREAT_1,
             D.QT_TREAT_2,
             D.QT_TREAT_3,
             DECODE(P_TYPE, 1, DECODE(D.TOTAL_TREAT, 0, 0, ROUND(100 * D.QT_TREAT_1 / D.TOTAL_TREAT, 1)), 0) AVG_TREAT_1,
             DECODE(P_TYPE, 1, DECODE(D.TOTAL_TREAT, 0, 0, ROUND(100 * D.QT_TREAT_2 / D.TOTAL_TREAT, 1)), 0) AVG_TREAT_2,
             DECODE(P_TYPE, 1, DECODE(D.TOTAL_TREAT, 0, 0, ROUND(100 * D.QT_TREAT_3 / D.TOTAL_TREAT, 1)), 0) AVG_TREAT_3,
             D.QT_MED_1,
             D.QT_MED_2,
             D.QT_MED_3,
             D.QT_MED_4,
             D.QT_MED_5,
             D.QT_MED_6
        FROM (SELECT B.CD_ESTABELECIMENTO,
                     B.QUARTER,
                     SUM(NVL2(C.CD_ESTABELECIMENTO, 1, 0)) QT_STAYS,
                     ROUND(SUM(DECODE(C.CD_ESTABELECIMENTO, 
                                      NULL,
                                      0, 
                                      LEAST(NVL(C.DT_SAIDA_SETOR, B.DT_FIM), B.DT_FIM) - GREATEST(C.DT_ENTRADA_SETOR, B.DT_INI))), 1) QT_DAYS,
                     SUM(NVL(C.IE_AT_RISK, 0)) QT_AT_RISK,
                     SUM(DECODE(NVL(C.IE_TIME_TREAT, 0), 1, 1, 0)) QT_TIME_1,
                     SUM(DECODE(NVL(C.IE_TIME_TREAT, 0), 2, 1, 0)) QT_TIME_2,             
                     SUM(DECODE(NVL(C.IE_TIME_TREAT, 0), 3, 1, 0)) QT_TIME_3,
                     SUM(DECODE(NVL(C.IE_TIME_TREAT, 0), 4, 1, 0)) QT_TIME_4,
                     SUM(DECODE(NVL(C.IE_TYPE_TREAT, 0), 0, 0, 1)) TOTAL_TREAT,
                     SUM(DECODE(NVL(C.IE_TYPE_TREAT, 0), 1, 1, 0)) QT_TREAT_1,
                     SUM(DECODE(NVL(C.IE_TYPE_TREAT, 0), 2, 1, 0)) QT_TREAT_2,
                     SUM(DECODE(NVL(C.IE_TYPE_TREAT, 0), 3, 1, 0)) QT_TREAT_3,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 1, 1, 0)) QT_MED_1,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 2, 1, 0)) QT_MED_2,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 3, 1, 0)) QT_MED_3,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 4, 1, 0)) QT_MED_4,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 5, 1, 0)) QT_MED_5,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 6, 1, 0)) QT_MED_6
                FROM (SELECT DISTINCT A.CD_ESTABELECIMENTO,
                                      A.DT_INI_QUARTER DT_INI,
                                      LEAST(A.DT_END_QUARTER + 1, SYSDATE) DT_FIM,
                                      4 QUARTER
                        FROM W_VTE_PROPHYLAXIS A
                       WHERE DECODE(P_CD_SETOR, 0, 0, A.CD_SETOR_ATENDIMENTO) = P_CD_SETOR) B
                LEFT JOIN W_VTE_PROPHYLAXIS C 
                       ON B.QUARTER = C.NR_QUARTER
                      AND B.CD_ESTABELECIMENTO = C.CD_ESTABELECIMENTO
               GROUP BY B.CD_ESTABELECIMENTO,
                        B.QUARTER) D;
    --
    TYPE T_ESTAB IS TABLE OF C_ESTAB%ROWTYPE;
    R_ESTAB T_ESTAB; 
    --
    CURSOR C_UNIT(V_QUARTER NUMBER) IS 
      SELECT D.CD_ESTABELECIMENTO,
             D.CD_SETOR_ATENDIMENTO,
             D.QUARTER,
             D.QT_STAYS,
             D.QT_DAYS,
             D.QT_AT_RISK,
             D.QT_TIME_1,
             D.QT_TIME_2,
             D.QT_TIME_3,
             D.QT_TIME_4,
             DECODE(P_TYPE, 1, DECODE(D.QT_AT_RISK, 0, 0, ROUND(100* D.QT_TIME_1 / D.QT_AT_RISK, 1)), 0) AVG_TIME_1,
             DECODE(P_TYPE, 1, DECODE(D.QT_AT_RISK, 0, 0, ROUND(100* D.QT_TIME_2 / D.QT_AT_RISK, 1)), 0) AVG_TIME_2,
             DECODE(P_TYPE, 1, DECODE(D.QT_AT_RISK, 0, 0, ROUND(100* D.QT_TIME_3 / D.QT_AT_RISK, 1)), 0) AVG_TIME_3,
             DECODE(P_TYPE, 1, DECODE(D.QT_AT_RISK, 0, 0, ROUND(100* D.QT_TIME_4 / D.QT_AT_RISK, 1)), 0) AVG_TIME_4,
             D.TOTAL_TREAT,
             D.QT_TREAT_1,
             D.QT_TREAT_2,
             D.QT_TREAT_3,
             DECODE(P_TYPE, 1, DECODE(D.TOTAL_TREAT, 0, 0, ROUND(100 * D.QT_TREAT_1 / D.TOTAL_TREAT, 1)), 0) AVG_TREAT_1,
             DECODE(P_TYPE, 1, DECODE(D.TOTAL_TREAT, 0, 0, ROUND(100 * D.QT_TREAT_2 / D.TOTAL_TREAT, 1)), 0) AVG_TREAT_2,
             DECODE(P_TYPE, 1, DECODE(D.TOTAL_TREAT, 0, 0, ROUND(100 * D.QT_TREAT_3 / D.TOTAL_TREAT, 1)), 0) AVG_TREAT_3,
             D.QT_MED_1,
             D.QT_MED_2,
             D.QT_MED_3,
             D.QT_MED_4,
             D.QT_MED_5,
             D.QT_MED_6
        FROM (SELECT B.CD_ESTABELECIMENTO,
                     B.CD_SETOR_ATENDIMENTO,
                     B.QUARTER,
                     SUM(NVL2(C.CD_ESTABELECIMENTO, 1, 0)) QT_STAYS,
                     ROUND(SUM(DECODE(C.CD_ESTABELECIMENTO, 
                                      NULL,
                                      0, 
                                      LEAST(NVL(C.DT_SAIDA_SETOR, B.DT_FIM), B.DT_FIM) - GREATEST(C.DT_ENTRADA_SETOR, B.DT_INI))), 1) QT_DAYS,
                     SUM(NVL(C.IE_AT_RISK, 0)) QT_AT_RISK,
                     SUM(DECODE(NVL(C.IE_TIME_TREAT, 0), 1, 1, 0)) QT_TIME_1,
                     SUM(DECODE(NVL(C.IE_TIME_TREAT, 0), 2, 1, 0)) QT_TIME_2,             
                     SUM(DECODE(NVL(C.IE_TIME_TREAT, 0), 3, 1, 0)) QT_TIME_3,
                     SUM(DECODE(NVL(C.IE_TIME_TREAT, 0), 4, 1, 0)) QT_TIME_4,
                     SUM(DECODE(NVL(C.IE_TYPE_TREAT, 0), 0, 0, 1)) TOTAL_TREAT,
                     SUM(DECODE(NVL(C.IE_TYPE_TREAT, 0), 1, 1, 0)) QT_TREAT_1,
                     SUM(DECODE(NVL(C.IE_TYPE_TREAT, 0), 2, 1, 0)) QT_TREAT_2,
                     SUM(DECODE(NVL(C.IE_TYPE_TREAT, 0), 3, 1, 0)) QT_TREAT_3,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 1, 1, 0)) QT_MED_1,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 2, 1, 0)) QT_MED_2,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 3, 1, 0)) QT_MED_3,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 4, 1, 0)) QT_MED_4,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 5, 1, 0)) QT_MED_5,
                     SUM(DECODE(NVL(C.IE_FIRST_MED, 0), 6, 1, 0)) QT_MED_6
                FROM (SELECT DISTINCT A.CD_ESTABELECIMENTO,
                                      A.CD_SETOR_ATENDIMENTO,
                                      A.DT_INI_QUARTER DT_INI,
                                      LEAST(A.DT_END_QUARTER + 1, SYSDATE) DT_FIM,
                                      V_QUARTER QUARTER
                        FROM W_VTE_PROPHYLAXIS A
                       WHERE DECODE(P_CD_SETOR, 0, 0, A.CD_SETOR_ATENDIMENTO) = P_CD_SETOR) B
                LEFT JOIN W_VTE_PROPHYLAXIS C 
                       ON B.QUARTER = C.NR_QUARTER
                      AND B.CD_SETOR_ATENDIMENTO = C.CD_SETOR_ATENDIMENTO
               GROUP BY B.CD_ESTABELECIMENTO,
                        B.CD_SETOR_ATENDIMENTO,
                        B.QUARTER) D;
    --
    TYPE T_UNIT IS TABLE OF C_UNIT%ROWTYPE;
    R_UNIT T_UNIT;
    --  
  BEGIN
    --
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    --
    IF P_TYPE = 1 THEN
      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT 1 QUARTER,
                               TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3) -2, 2, '0') || SUBSTR(PR, -4), 'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3), 2, '0') || SUBSTR(PR, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 2 QUARTER,
                               TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3) -2, 2, '0') || SUBSTR(SEG, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3), 2, '0') || SUBSTR(SEG, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 3 QUARTER,
                               TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3) -2, 2, '0') || SUBSTR(TER, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3), 2, '0') || SUBSTR(TER, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 4 QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE)
      LOOP

        OPEN C_ATEND(N.INI, N.FIM, CASE WHEN N.QUARTER = 4 THEN 0 ELSE P_CD_SETOR END, NULL);
        LOOP
          FETCH C_ATEND BULK COLLECT INTO R_ATEND LIMIT 1000;
        
            BEGIN
              FOR i IN R_ATEND.FIRST..R_ATEND.LAST LOOP
                INSERT INTO W_VTE_PROPHYLAXIS (
                  cd_estabelecimento, 
                  cd_setor_atendimento, 
                  nr_quarter, 
                  dt_ini_quarter,
                  dt_end_quarter,
                  nr_atendimento, 
                  dt_entrada_setor, 
                  dt_saida_setor, 
                  ie_at_risk, 
                  dt_first_med, 
                  ie_first_med, 
                  dt_first_dev, 
                  dt_first_treat, 
                  ie_first_treat, 
                  ie_time_treat, 
                  ie_type_treat)
                VALUES (
                  R_ATEND(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                  R_ATEND(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
                  N.QUARTER, --nr_quarter, 
                  N.INI, --dt_ini_quarter,
                  N.FIM, --dt_end_quarter,
                  R_ATEND(i).NR_ATENDIMENTO, --nr_atendimento, 
                  R_ATEND(i).DT_ENTRADA_SETOR, --dt_entrada_setor, 
                  R_ATEND(i).DT_SAIDA_SETOR, --dt_saida_setor, 
                  R_ATEND(i).IE_AT_RISK, --ie_at_risk, 
                  R_ATEND(i).DT_FIRST_MED, --dt_first_med, 
                  R_ATEND(i).IE_FIRST_MED, --ie_first_med, 
                  R_ATEND(i).DT_FIRST_DEV, --dt_first_dev, 
                  R_ATEND(i).DT_FIRST_TREAT, --dt_first_treat, 
                  R_ATEND(i).IE_FIRST_TREAT, --ie_first_treat, 
                  R_ATEND(i).IE_TIME_TREAT, --ie_time_treat, 
                  R_ATEND(i).IE_TYPE_TREAT); --ie_type_treat)
              END LOOP;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              NULL;
            END;
            COMMIT;
            
          EXIT WHEN C_ATEND%NOTFOUND; 
        END LOOP;
        CLOSE C_ATEND;

      END LOOP;
      
      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT 1 QUARTER,
                               TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3) -2, 2, '0') || SUBSTR(PR, -4), 'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(PR, 2, 1) * 3), 2, '0') || SUBSTR(PR, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 2 QUARTER,
                               TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3) -2, 2, '0') || SUBSTR(SEG, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(SEG, 2, 1) * 3), 2, '0') || SUBSTR(SEG, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 3 QUARTER,
                               TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3) -2, 2, '0') || SUBSTR(TER, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(TER, 2, 1) * 3), 2, '0') || SUBSTR(TER, -4), 'MMYYYY')) FIM
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR))
                        UNION
                        SELECT 4 QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE) 
      LOOP
      
        OPEN C_UNIT(N.QUARTER);
        LOOP
          FETCH C_UNIT BULK COLLECT INTO R_UNIT LIMIT 1000;            

            BEGIN
              FOR i IN R_UNIT.FIRST..R_UNIT.LAST LOOP
                INSERT INTO W_VTE_PROPHYLAXIS_UNIT (
                  cd_estabelecimento, 
                  cd_setor_atendimento, 
                  nr_quarter, 
                  nr_stays, 
                  nr_days, 
                  nr_at_risk, 
                  nr_time_1, 
                  nr_time_2, 
                  nr_time_3, 
                  nr_time_4, 
                  nr_avg_time_1, 
                  nr_avg_time_2, 
                  nr_avg_time_3, 
                  nr_avg_time_4, 
                  nr_total_treat, 
                  nr_treat_1, 
                  nr_treat_2, 
                  nr_treat_3, 
                  nr_avg_treat_1, 
                  nr_avg_treat_2, 
                  nr_avg_treat_3, 
                  nr_med_1, 
                  nr_med_2, 
                  nr_med_3, 
                  nr_med_4, 
                  nr_med_5, 
                  nr_med_6)
                VALUES (
                  R_UNIT(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                  R_UNIT(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
                  R_UNIT(i).QUARTER, --nr_quarter, 
                  R_UNIT(i).QT_STAYS, --nr_stays, 
                  R_UNIT(i).QT_DAYS, --nr_days, 
                  R_UNIT(i).QT_AT_RISK, --nr_at_risk, 
                  R_UNIT(i).QT_TIME_1, --nr_time_1, 
                  R_UNIT(i).QT_TIME_2, --nr_time_2, 
                  R_UNIT(i).QT_TIME_3, --nr_time_3, 
                  R_UNIT(i).QT_TIME_4, --nr_time_4, 
                  R_UNIT(i).AVG_TIME_1, --nr_avg_time_1, 
                  R_UNIT(i).AVG_TIME_2, --nr_avg_time_2, 
                  R_UNIT(i).AVG_TIME_3, --nr_avg_time_3, 
                  R_UNIT(i).AVG_TIME_4, --nr_avg_time_4, 
                  R_UNIT(i).TOTAL_TREAT, --nr_total_treat, 
                  R_UNIT(i).QT_TREAT_1, --nr_treat_1, 
                  R_UNIT(i).QT_TREAT_2, --nr_treat_2, 
                  R_UNIT(i).QT_TREAT_3, --nr_treat_3, 
                  R_UNIT(i).AVG_TREAT_1, --nr_avg_treat_1, 
                  R_UNIT(i).AVG_TREAT_2, --nr_avg_treat_2, 
                  R_UNIT(i).AVG_TREAT_3, --nr_avg_treat_3, 
                  R_UNIT(i).QT_MED_1, --nr_med_1, 
                  R_UNIT(i).QT_MED_2, --nr_med_2, 
                  R_UNIT(i).QT_MED_3, --nr_med_3, 
                  R_UNIT(i).QT_MED_4, --nr_med_4, 
                  R_UNIT(i).QT_MED_5, --nr_med_5, 
                  R_UNIT(i).QT_MED_6); --nr_med_6)  
              END LOOP;
            EXCEPTION WHEN NO_DATA_FOUND THEN 
              NULL;
            END;
            COMMIT;
          EXIT WHEN C_UNIT%NOTFOUND; 
        END LOOP;
        CLOSE C_UNIT;
     
        IF N.QUARTER = 4 THEN
          OPEN C_ESTAB;
          LOOP
            FETCH C_ESTAB BULK COLLECT INTO R_ESTAB LIMIT 1000;
              BEGIN
                FOR i IN R_ESTAB.FIRST..R_ESTAB.LAST LOOP
                  INSERT INTO W_VTE_PROPHYLAXIS_ESTAB (
                    cd_estabelecimento, 
                    nr_stays, 
                    nr_days, 
                    nr_at_risk, 
                    nr_time_1, 
                    nr_time_2, 
                    nr_time_3, 
                    nr_time_4, 
                    nr_avg_time_1, 
                    nr_avg_time_2, 
                    nr_avg_time_3, 
                    nr_avg_time_4, 
                    nr_total_treat, 
                    nr_treat_1, 
                    nr_treat_2, 
                    nr_treat_3, 
                    nr_avg_treat_1, 
                    nr_avg_treat_2, 
                    nr_avg_treat_3, 
                    nr_med_1, 
                    nr_med_2, 
                    nr_med_3, 
                    nr_med_4, 
                    nr_med_5, 
                    nr_med_6)
                  VALUES (
                    R_ESTAB(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                    R_ESTAB(i).QT_STAYS, --nr_stays, 
                    R_ESTAB(i).QT_DAYS, --nr_days, 
                    R_ESTAB(i).QT_AT_RISK, --nr_at_risk, 
                    R_ESTAB(i).QT_TIME_1, --nr_time_1, 
                    R_ESTAB(i).QT_TIME_2, --nr_time_2, 
                    R_ESTAB(i).QT_TIME_3, --nr_time_3, 
                    R_ESTAB(i).QT_TIME_4, --nr_time_4, 
                    R_ESTAB(i).AVG_TIME_1, --nr_avg_time_1, 
                    R_ESTAB(i).AVG_TIME_2, --nr_avg_time_2, 
                    R_ESTAB(i).AVG_TIME_3, --nr_avg_time_3, 
                    R_ESTAB(i).AVG_TIME_4, --nr_avg_time_4, 
                    R_ESTAB(i).TOTAL_TREAT, --nr_total_treat, 
                    R_ESTAB(i).QT_TREAT_1, --nr_treat_1, 
                    R_ESTAB(i).QT_TREAT_2, --nr_treat_2, 
                    R_ESTAB(i).QT_TREAT_3, --nr_treat_3, 
                    R_ESTAB(i).AVG_TREAT_1, --nr_avg_treat_1, 
                    R_ESTAB(i).AVG_TREAT_2, --nr_avg_treat_2, 
                    R_ESTAB(i).AVG_TREAT_3, --nr_avg_treat_3, 
                    R_ESTAB(i).QT_MED_1, --nr_med_1, 
                    R_ESTAB(i).QT_MED_2, --nr_med_2, 
                    R_ESTAB(i).QT_MED_3, --nr_med_3, 
                    R_ESTAB(i).QT_MED_4, --nr_med_4, 
                    R_ESTAB(i).QT_MED_5, --nr_med_5, 
                    R_ESTAB(i).QT_MED_6); --nr_med_6)  
                END LOOP;
              EXCEPTION WHEN NO_DATA_FOUND THEN 
                NULL;
              END;
              COMMIT;
            EXIT WHEN C_ESTAB%NOTFOUND; 
          END LOOP;
          CLOSE C_ESTAB;
                    
        END IF;
        
      END LOOP;

    ELSE

      FOR N IN (SELECT QUARTER,
                       GREATEST(INI, P_START_DATE) INI,
                       LEAST(FIM, P_END_DATE) FIM
                  FROM (SELECT P_QUARTER QUARTER,
                               TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3) -2, 2, '0') || SUBSTR(QUA, -4),'MMYYYY') INI,
                               LAST_DAY(TO_DATE(LPAD((SUBSTR(QUA, 2, 1) * 3), 2, '0') || SUBSTR(QUA, -4), 'MMYYYY')) FIM 
                          FROM TABLE(PKG_TRIMESTRE.BUSCAR_TRIMESTRES_PIPELINED(P_QUARTER, P_YEAR)))
                 WHERE INI <= P_END_DATE
                   AND FIM >= P_START_DATE)
      LOOP

        OPEN C_ATEND(N.INI, N.FIM, P_CD_SETOR, CASE WHEN P_TYPE = 2 THEN NULL ELSE P_PATIENT END);
        LOOP
          FETCH C_ATEND BULK COLLECT INTO R_ATEND LIMIT 1000;

          BEGIN
              FOR i IN R_ATEND.FIRST..R_ATEND.LAST LOOP
                INSERT INTO W_VTE_PROPHYLAXIS (
                  cd_estabelecimento, 
                  cd_setor_atendimento, 
                  nr_quarter, 
                  dt_ini_quarter,
                  dt_end_quarter,
                  nr_atendimento, 
                  dt_entrada_setor, 
                  dt_saida_setor, 
                  ie_at_risk, 
                  dt_first_med, 
                  ie_first_med, 
                  dt_first_dev, 
                  dt_first_treat, 
                  ie_first_treat, 
                  ie_time_treat, 
                  ie_type_treat)
                VALUES (
                  R_ATEND(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                  R_ATEND(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
                  N.QUARTER, --nr_quarter, 
                  N.INI, --dt_ini_quarter,
                  N.FIM, --dt_end_quarter,
                  R_ATEND(i).NR_ATENDIMENTO, --nr_atendimento, 
                  R_ATEND(i).DT_ENTRADA_SETOR, --dt_entrada_setor, 
                  R_ATEND(i).DT_SAIDA_SETOR, --dt_saida_setor, 
                  R_ATEND(i).IE_AT_RISK, --ie_at_risk, 
                  R_ATEND(i).DT_FIRST_MED, --dt_first_med, 
                  R_ATEND(i).IE_FIRST_MED, --ie_first_med, 
                  R_ATEND(i).DT_FIRST_DEV, --dt_first_dev, 
                  R_ATEND(i).DT_FIRST_TREAT, --dt_first_treat, 
                  R_ATEND(i).IE_FIRST_TREAT, --ie_first_treat, 
                  R_ATEND(i).IE_TIME_TREAT, --ie_time_treat, 
                  R_ATEND(i).IE_TYPE_TREAT); --ie_type_treat)
              END LOOP;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              NULL;
            END;
            COMMIT;
            
          EXIT WHEN C_ATEND%NOTFOUND; 
        END LOOP;
        CLOSE C_ATEND;      

        IF P_TYPE = 2 THEN
          OPEN C_UNIT(N.QUARTER);
          LOOP
            FETCH C_UNIT BULK COLLECT INTO R_UNIT LIMIT 1000;            

              BEGIN
                FOR i IN R_UNIT.FIRST..R_UNIT.LAST LOOP
                  INSERT INTO W_VTE_PROPHYLAXIS_UNIT (
                    cd_estabelecimento, 
                    cd_setor_atendimento, 
                    nr_quarter, 
                    nr_stays, 
                    nr_days, 
                    nr_at_risk, 
                    nr_time_1, 
                    nr_time_2, 
                    nr_time_3, 
                    nr_time_4, 
                    nr_avg_time_1, 
                    nr_avg_time_2, 
                    nr_avg_time_3, 
                    nr_avg_time_4, 
                    nr_total_treat, 
                    nr_treat_1, 
                    nr_treat_2, 
                    nr_treat_3, 
                    nr_avg_treat_1, 
                    nr_avg_treat_2, 
                    nr_avg_treat_3, 
                    nr_med_1, 
                    nr_med_2, 
                    nr_med_3, 
                    nr_med_4, 
                    nr_med_5, 
                    nr_med_6)
                  VALUES (
                    R_UNIT(i).CD_ESTABELECIMENTO, --cd_estabelecimento, 
                    R_UNIT(i).CD_SETOR_ATENDIMENTO, --cd_setor_atendimento, 
                    R_UNIT(i).QUARTER, --nr_quarter, 
                    R_UNIT(i).QT_STAYS, --nr_stays, 
                    R_UNIT(i).QT_DAYS, --nr_days, 
                    R_UNIT(i).QT_AT_RISK, --nr_at_risk, 
                    R_UNIT(i).QT_TIME_1, --nr_time_1, 
                    R_UNIT(i).QT_TIME_2, --nr_time_2, 
                    R_UNIT(i).QT_TIME_3, --nr_time_3, 
                    R_UNIT(i).QT_TIME_4, --nr_time_4, 
                    R_UNIT(i).AVG_TIME_1, --nr_avg_time_1, 
                    R_UNIT(i).AVG_TIME_2, --nr_avg_time_2, 
                    R_UNIT(i).AVG_TIME_3, --nr_avg_time_3, 
                    R_UNIT(i).AVG_TIME_4, --nr_avg_time_4, 
                    R_UNIT(i).TOTAL_TREAT, --nr_total_treat, 
                    R_UNIT(i).QT_TREAT_1, --nr_treat_1, 
                    R_UNIT(i).QT_TREAT_2, --nr_treat_2, 
                    R_UNIT(i).QT_TREAT_3, --nr_treat_3, 
                    R_UNIT(i).AVG_TREAT_1, --nr_avg_treat_1, 
                    R_UNIT(i).AVG_TREAT_2, --nr_avg_treat_2, 
                    R_UNIT(i).AVG_TREAT_3, --nr_avg_treat_3, 
                    R_UNIT(i).QT_MED_1, --nr_med_1, 
                    R_UNIT(i).QT_MED_2, --nr_med_2, 
                    R_UNIT(i).QT_MED_3, --nr_med_3, 
                    R_UNIT(i).QT_MED_4, --nr_med_4, 
                    R_UNIT(i).QT_MED_5, --nr_med_5, 
                    R_UNIT(i).QT_MED_6); --nr_med_6)  
                END LOOP;
              EXCEPTION WHEN NO_DATA_FOUND THEN 
                NULL;
              END;
              COMMIT;
            EXIT WHEN C_UNIT%NOTFOUND; 
          END LOOP;
          CLOSE C_UNIT;

        END IF;
      
      END LOOP;
      
    END IF;

  END PRE_PROCESS;
  --
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_ESTAB   IN NUMBER DEFAULT 0,
                P_CD_SETOR   IN NUMBER DEFAULT 0,
                P_YEAR       IN NUMBER,
                P_QUARTER    IN NUMBER DEFAULT 1,
                P_TYPE       IN NUMBER DEFAULT 1,
                P_PATIENT    IN VARCHAR2) IS
  BEGIN  
    TABLE_CLEANING;
    PRE_PROCESS(P_START_DATE, LEAST(P_END_DATE, TRUNC(SYSDATE, 'DD')), P_CD_ESTAB, P_CD_SETOR, P_YEAR, P_QUARTER, P_TYPE, P_PATIENT);
  END RUN;
END PKG_VTE_PROPHYLAXIS;
/
