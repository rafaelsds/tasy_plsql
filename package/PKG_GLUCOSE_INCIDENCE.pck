CREATE OR REPLACE PACKAGE PKG_GLUCOSE_INCIDENCE AS
  --
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_SETOR   IN VARCHAR2 DEFAULT '0',
                P_TYPE       IN NUMBER DEFAULT 1);
  --
END PKG_GLUCOSE_INCIDENCE;
/
CREATE OR REPLACE PACKAGE BODY PKG_GLUCOSE_INCIDENCE AS

  PROCEDURE TABLE_CLEANING IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_GLUCOSE_INCIDENCE';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_GLUCOSE_INCIDENCE_BY_DAY';
  END TABLE_CLEANING;
  --
  PROCEDURE PRE_PROCESS(
    P_START_DATE IN DATE, 
    P_END_DATE IN DATE,
    P_CD_SETOR IN VARCHAR2,
    P_TYPE     IN NUMBER) IS
    --
    NM_USUARIO_W USUARIO.NM_USUARIO%TYPE;
    --
    CURSOR C_REC IS 
      SELECT DS_MONTH,
             DS_YEAR,
             SUM(QT_TESTS) QT_TESTS,
             SUM(QT_HIGH_GLUCOSE) QT_HIGH_GLUCOSE,
             SUM(QT_LOW_GLUCOSE) QT_LOW_GLUCOSE,
             DT_CONTROLE
        FROM (SELECT DISTINCT TO_CHAR(RG.DT_CONTROLE, 'Month', 'nls_date_language=english') DS_MONTH,
                              TO_CHAR(RG.DT_CONTROLE, 'yyyy', 'nls_date_language=english') DS_YEAR,
                              (SELECT COUNT(R.QTD)
                                 FROM REL_RESULTADO_GLICEMIA_V R
                                WHERE R.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4)
                                  AND TO_CHAR(R.DT_CONTROLE, 'YYYY-MM') = TO_CHAR(RG.DT_CONTROLE, 'YYYY-MM')) QT_TESTS,
                              (SELECT NVL(SUM(R.QTD), 0)
                                 FROM REL_RESULTADO_GLICEMIA_V R
                                WHERE R.DS_RESULTADO = 'Low Glucose'
                                  AND R.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4)
                                  AND TO_CHAR(R.DT_CONTROLE, 'YYYY-MM') = TO_CHAR(RG.DT_CONTROLE, 'YYYY-MM')) QT_LOW_GLUCOSE,
                              (SELECT NVL(SUM(R.QTD), 0)
                                 FROM REL_RESULTADO_GLICEMIA_V R
                                WHERE R.DS_RESULTADO = 'High Glucose'
                                  AND R.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4)
                                  AND TO_CHAR(R.DT_CONTROLE, 'YYYY-MM') = TO_CHAR(RG.DT_CONTROLE, 'YYYY-MM')) QT_HIGH_GLUCOSE,
                              TO_CHAR(RG.DT_CONTROLE, 'YYYY-MM') DT_CONTROLE
                FROM REL_RESULTADO_GLICEMIA_V RG
               WHERE RG.DT_CONTROLE BETWEEN P_START_DATE AND P_END_DATE
                 AND RG.DT_CONTROLE >= RG.DT_INCLUSAO
                 AND EXISTS (SELECT 1 FROM USUARIO_SETOR U WHERE U.NM_USUARIO_PARAM = NM_USUARIO_W AND U.CD_SETOR_ATENDIMENTO = OBTER_SETOR_ATENDIMENTO(RG.NR_ATENDIMENTO))
                 AND (P_CD_SETOR = '0' OR OBTER_SE_CONTIDO(OBTER_SETOR_ATENDIMENTO(RG.NR_ATENDIMENTO), P_CD_SETOR) = 'S')
                 AND RG.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4))
       GROUP BY DS_MONTH, DS_YEAR, DT_CONTROLE;
    --
    TYPE T_REC IS TABLE OF C_REC%ROWTYPE;
    R_REC T_REC;
    --
    CURSOR C_DAY IS 
      SELECT DISTINCT TRUNC(RG.DT_CONTROLE, 'DD') DT_CONTROLE,
                      PKG_ATEND_PAC_UNID.GET_PATIENT_NAME(RG.NR_ATENDIMENTO) NM_PATIENT,
                      OBTER_DATA_NASCTO_PF(RG.CD_PESSOA_FISICA) DT_NASCTO,
                      RG.NR_ATENDIMENTO,
                      RG.CD_PESSOA_FISICA,
                      OBTER_NOME_SETOR(OBTER_SETOR_ATENDIMENTO(RG.NR_ATENDIMENTO)) DS_SETOR,
                      (SELECT COUNT(R.QTD)
                         FROM REL_RESULTADO_GLICEMIA_V R
                        WHERE R.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4)
                          AND R.NR_ATENDIMENTO = RG.NR_ATENDIMENTO
                          AND TRUNC(R.DT_CONTROLE, 'DD') = TRUNC(RG.DT_CONTROLE, 'DD')) QT_TESTS,
                      (SELECT NVL(SUM(R.QTD), 0)
                         FROM REL_RESULTADO_GLICEMIA_V R
                        WHERE R.DS_RESULTADO = 'Glucose Compliance'
                          AND R.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4)
                          AND R.NR_ATENDIMENTO = RG.NR_ATENDIMENTO
                          AND TRUNC(R.DT_CONTROLE, 'DD') = TRUNC(RG.DT_CONTROLE, 'DD')) QT_COMPLIANCE,
                      (SELECT NVL(SUM(R.QTD), 0)
                         FROM REL_RESULTADO_GLICEMIA_V R
                        WHERE R.DS_RESULTADO = 'Low Glucose'
                          AND R.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4)
                          AND R.NR_ATENDIMENTO = RG.NR_ATENDIMENTO
                          AND TRUNC(R.DT_CONTROLE, 'DD') = TRUNC(RG.DT_CONTROLE, 'DD')) QT_LOW,
                      (SELECT NVL(SUM(R.QTD), 0)
                         FROM REL_RESULTADO_GLICEMIA_V R
                        WHERE R.DS_RESULTADO = 'High Glucose'
                          AND R.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4)
                          AND R.NR_ATENDIMENTO = RG.NR_ATENDIMENTO
                          AND TRUNC(R.DT_CONTROLE, 'DD') = TRUNC(RG.DT_CONTROLE, 'DD')) QT_HIGH
        FROM REL_RESULTADO_GLICEMIA_V RG
       WHERE RG.DT_CONTROLE BETWEEN P_START_DATE AND P_END_DATE
         AND EXISTS (SELECT 1 FROM W_GLUCOSE_INCIDENCE G WHERE G.DT_CONTROLE = TO_CHAR(RG.DT_CONTROLE, 'YYYY-MM'))
         AND EXISTS (SELECT 1 FROM USUARIO_SETOR U WHERE U.NM_USUARIO_PARAM = NM_USUARIO_W AND U.CD_SETOR_ATENDIMENTO = OBTER_SETOR_ATENDIMENTO(RG.NR_ATENDIMENTO))
         AND (P_CD_SETOR = '0' OR OBTER_SE_CONTIDO(OBTER_SETOR_ATENDIMENTO(RG.NR_ATENDIMENTO), P_CD_SETOR) = 'S')
         AND RG.NR_SEQ_PROTOCOLO = PKG_REPORT_DATA.GET_PROTOCOL(4);
    --
    TYPE T_DAY IS TABLE OF C_DAY%ROWTYPE;
    R_DAY T_DAY;
    --
  BEGIN
    --
    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    --
    OPEN C_REC;
    LOOP
      FETCH C_REC BULK COLLECT INTO R_REC LIMIT 1000;
        
        BEGIN
          FOR i IN R_REC.FIRST..R_REC.LAST LOOP
            INSERT INTO W_GLUCOSE_INCIDENCE (
              ds_month, 
              ds_year, 
              qt_tests, 
              qt_high_glucose, 
              qt_low_glucose, 
              dt_controle)
            VALUES (
              R_REC(i).DS_MONTH, --ds_month, 
              R_REC(i).DS_YEAR, --ds_year, 
              R_REC(i).QT_TESTS, --qt_tests, 
              R_REC(i).QT_HIGH_GLUCOSE, --qt_high_glucose, 
              R_REC(i).QT_LOW_GLUCOSE, --qt_low_glucose, 
              R_REC(i).DT_CONTROLE); --dt_controle)
          END LOOP;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
        COMMIT;
            
      EXIT WHEN C_REC%NOTFOUND; 
    END LOOP;
    CLOSE C_REC;

    IF P_TYPE = 2 THEN
      OPEN C_DAY;
      LOOP
        FETCH C_DAY BULK COLLECT INTO R_DAY LIMIT 1000;
          
          BEGIN
            FOR i IN R_DAY.FIRST..R_DAY.LAST LOOP
              INSERT INTO W_GLUCOSE_INCIDENCE_BY_DAY (
                dt_controle, 
                nm_patient, 
                dt_nascto, 
                cd_pessoa_fisica, 
                nr_atendimento, 
                ds_setor, 
                qt_tests, 
                qt_compliance, 
                qt_high, 
                qt_low)
              VALUES (
                R_DAY(i).DT_CONTROLE, --dt_controle, 
                R_DAY(i).NM_PATIENT, --nm_patient, 
                R_DAY(i).DT_NASCTO, --dt_nascto, 
                R_DAY(i).CD_PESSOA_FISICA, --cd_pessoa_fisica, 
                R_DAY(i).NR_ATENDIMENTO, --nr_atendimento, 
                R_DAY(i).DS_SETOR, --ds_setor, 
                R_DAY(i).QT_TESTS, --qt_tests, 
                R_DAY(i).QT_COMPLIANCE, --qt_compliance, 
                R_DAY(i).QT_HIGH, --qt_high, 
                R_DAY(i).QT_LOW); --qt_low); 
            END LOOP;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
          COMMIT;
              
        EXIT WHEN C_DAY%NOTFOUND; 
      END LOOP;
      CLOSE C_DAY;    
    END IF;

  END PRE_PROCESS;
  -- 
  PROCEDURE RUN(P_START_DATE IN DATE DEFAULT SYSDATE,
                P_END_DATE   IN DATE DEFAULT SYSDATE,
                P_CD_SETOR   IN VARCHAR2 DEFAULT '0',
                P_TYPE       IN NUMBER DEFAULT 1) IS
  BEGIN  
    TABLE_CLEANING;
    PRE_PROCESS(P_START_DATE, LEAST(FIM_DIA(P_END_DATE), SYSDATE), P_CD_SETOR, P_TYPE);
  END RUN;
END PKG_GLUCOSE_INCIDENCE;
/
