create or replace
procedure informa_declaracao_imp(
			nr_declaracao_imp_p	varchar2,
			nr_ordem_compra_p		number) is 

begin

update	ordem_compra 
set 	nr_declaracao_imp		= nr_declaracao_imp_p 
where 	nr_ordem_compra		= nr_ordem_compra_p;

commit;

end informa_declaracao_imp;
/
