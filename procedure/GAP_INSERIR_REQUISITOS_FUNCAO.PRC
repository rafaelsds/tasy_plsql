create or replace
procedure GAP_INSERIR_REQUISITOS_FUNCAO(nr_seq_gap_p number,
					nm_usuario_p varchar2) is


/* --------------- Estrutura da tabela GAP_REQUISITOS_MODULO --------------- */
/* CREATE TABLE GAP_REQUISITOS_MODULO (NR_SEQ_MODULO		NUMBER(10),
					NM_MODULO		VARCHAR2(2000),
					CD_REQUISITO		VARCHAR2(255),
					DS_REQUISITO		VARCHAR2(4000),
					DS_PROCESSO		VARCHAR2(255),
					IE_TIPO_REQUISITO	VARCHAR2(1),
					IE_ADERENCIA		VARCHAR2(1),
					IE_PRIORIDADE_CLI	VARCHAR2(10),
					IE_PRIORIDADE_PAIS	VARCHAR2(10),
					IE_COMPLEXIDADE		VARCHAR2(2),
					IE_ESFORCO		VARCHAR2(2),
					QT_HORAS		NUMBER(10),
					DS_OBSERVACAO		VARCHAR2(4000), 
					DS_CONFIANTE		VARCHAR2(255),
					IE_TECNOLOGIA		VARCHAR2(1),
					IE_DESIGN		VARCHAR2(1))
/* ------------------------------------------------------------------------- */

nr_seq_cad_modulo_w		latam_cadastro_modulo.nr_sequencia%type;
nr_seq_modulo_w			latam_modulo.nr_sequencia%type;
nr_seq_pais_w			latam_modulo.nr_seq_pais%type;
nr_seq_hospital_w		latam_modulo.nr_seq_hospital%type;
nr_seq_gerencia_w		latam_modulo.nr_seq_gerencia%type;
qt_requisito_w			number(10);

cursor c01 is
	select	distinct nr_seq_modulo,
		nm_modulo
	from	gap_requisitos_modulo;
	
vet01	c01%rowtype;

cursor c02 is
	select	nr_seq_modulo,
		nm_modulo,
		cd_requisito,
		ds_requisito,
		ds_processo,
		ie_tipo_requisito,
		ie_aderencia,
		ie_prioridade_cli,
		ie_prioridade_pais,
		ie_complexidade,
		ie_esforco,
		qt_horas,
		ds_observacao,
		somente_numero(ds_confiante) pr_confianca,
		ie_tecnologia,
		ie_design
	from   	gap_requisitos_modulo
	where	nm_modulo = vet01.nm_modulo;

vet02	c02%rowtype;

begin

select	max(nr_seq_pais),
	max(nr_seq_hospital)
into	nr_seq_pais_w,
	nr_seq_hospital_w
from	latam_gap
where	nr_sequencia = nr_seq_gap_p;

/* Busca a gerencia do usu�rio que est� fazendo a carga*/
select	max(b.nr_seq_gerencia)
into	nr_seq_gerencia_w
from	usuario_grupo_des a,
	grupo_desenvolvimento b
where	a.nr_seq_grupo = b.nr_sequencia
and	a.nm_usuario_grupo = nm_usuario_p;

open c01;
loop
fetch c01 into
	vet01;
exit when c01%notfound;
	begin
	
	select	max(nr_sequencia)
	into	nr_seq_cad_modulo_w
	from	latam_cadastro_modulo
	where	upper(ds_modulo) = upper(vet01.nm_modulo);
	
	if (nr_seq_modulo_w is null) then
		select	latam_cadastro_modulo_seq.nextval
		into	nr_seq_cad_modulo_w
		from	dual;
		
		insert into latam_cadastro_modulo
			(nr_sequencia,
			ds_modulo,
			dt_atualizacao,
			nm_usuario)
		values (nr_seq_cad_modulo_w,
			vet01.nm_modulo,
			sysdate,
			nm_usuario_p);
	end if;
	
	select	max(nr_sequencia)
	into	nr_seq_modulo_w
	from	latam_modulo
	where	nr_seq_modulo = nr_seq_cad_modulo_w
	and	nr_seq_pais = nr_seq_pais_w
	and	nr_seq_hospital = nr_seq_hospital_w;
	
	if (nr_seq_modulo_w is null) then
		select	latam_modulo_seq.nextval
		into	nr_seq_modulo_w
		from	dual;
		
		insert into latam_modulo
			(nr_sequencia,
			nr_seq_modulo,
			nr_seq_pais,
			nr_seq_hospital,
			nr_seq_gerencia,
			ie_situacao,
			dt_atualizacao,
			nm_usuario)
		values (nr_seq_modulo_w,
			nr_seq_cad_modulo_w,
			nr_seq_pais_w,
			nr_seq_hospital_w,
			nvl(nr_seq_gerencia_w,1),
			'A',
			sysdate,
			nm_usuario_p);
	end if;
	
	open c02;
	loop
	fetch c02 into
		vet02;
	exit when c02%notfound;
		begin

		select	count(*)
		into	qt_requisito_w
		from	latam_requisito
		where	cd_requisito = substr(vet02.cd_requisito,0,15)
		and	nr_seq_modulo = nr_seq_modulo_w
		and	nr_seq_gap = nr_seq_gap_p;
		
		if (qt_requisito_w > 0) then
			update	latam_requisito
			set	ds_requisito_breve = substr(vet02.ds_requisito,0,80),
				ds_requisito = substr(vet02.ds_requisito,0,4000),
				ds_observacao = substr(vet02.ds_observacao,0,2000),
				ie_situacao = decode(vet02.ie_aderencia,'D','D','P','P','M','M',null),
				ie_tipo_requisito = decode(vet02.ie_tipo_requisito,'F','F','N','N',null),
				ie_esforco_desenv = decode(vet02.ie_esforco,'PP',1,'P',2,'M',3,'G',4,'GG',5,null),
				ie_complexidade = decode(vet02.ie_complexidade,'PP',1,'P',2,'M',3,'G',4,'GG',5,null),
				qt_tempo_horas = nvl(vet02.qt_horas,0),
				ie_sob_medida = decode(nvl(vet02.qt_horas,0),0,'N','S'),
				pr_confianca = decode(nvl(vet02.pr_confianca,0),0,null,vet02.pr_confianca),
				ie_importancia_cliente = decode(vet02.ie_prioridade_cli,'MUST','C','SHOULD','I','COULD','D','WONT','T',null),
				ie_importancia_pais = decode(vet02.ie_prioridade_pais,'MUST','C','SHOULD','I','COULD','D','WONT','T',null),
				ds_processo = substr(vet02.ds_processo,0,255),
				ie_necessita_tecnologia = decode(vet02.ie_tecnologia,'Y','S','N'),
				ie_necessita_design = decode(vet02.ie_design,'Y','S','N'),
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where	cd_requisito = substr(vet02.cd_requisito,0,15)
			and	nr_seq_modulo = nr_seq_modulo_w
			and	nr_seq_gap = nr_seq_gap_p;
			
		else
			insert into latam_requisito
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_modulo,
				nr_seq_gap,
				cd_requisito,
				ds_requisito_breve,
				ds_requisito,
				ds_observacao,
				ds_criterio,
				ie_situacao,
				ie_tipo_requisito,
				ie_esforco_desenv,
				ie_complexidade,
				qt_tempo_horas,
				ie_sob_medida,
				pr_confianca,
				qt_programador,
				ie_importancia_cliente,
				ie_importancia_pais,
				ie_situacao_req,
				ie_regulatorio,
				ds_processo,
				ie_necessita_tecnologia,
				ie_necessita_design)
			values (latam_requisito_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_modulo_w,
				nr_seq_gap_p,
				substr(vet02.cd_requisito,0,15),
				substr(vet02.ds_requisito,0,80),
				substr(vet02.ds_requisito,0,4000),
				substr(vet02.ds_observacao,0,2000),
				null,
				decode(vet02.ie_aderencia,'D','D','P','P','M','M',null),
				decode(vet02.ie_tipo_requisito,'F','F','N','N',null),
				decode(vet02.ie_esforco,'PP',1,'P',2,'M',3,'G',4,'GG',5,null),
				decode(vet02.ie_complexidade,'PP',1,'P',2,'M',3,'G',4,'GG',5,null),
				nvl(vet02.qt_horas,0),
				decode(nvl(vet02.qt_horas,0),0,'N','S'),
				decode(nvl(vet02.pr_confianca,0),0,null,vet02.pr_confianca),
				1,
				decode(vet02.ie_prioridade_cli,'MUST','C','SHOULD','I','COULD','D','WONT','T',null),
				decode(vet02.ie_prioridade_pais,'MUST','C','SHOULD','I','COULD','D','WONT','T',null),
				'A',
				'N',
				substr(vet02.ds_processo,0,255),
				decode(vet02.ie_tecnologia,'Y','S','N'),
				decode(vet02.ie_design,'Y','S','N'));
		end if;
		
		end;
	end loop;
	close c02;
	
	nr_seq_cad_modulo_w := null;
	nr_seq_modulo_w := null;
	end;
end loop;
close c01;

delete from gap_requisitos_modulo;

commit;

end GAP_INSERIR_REQUISITOS_FUNCAO;
/
