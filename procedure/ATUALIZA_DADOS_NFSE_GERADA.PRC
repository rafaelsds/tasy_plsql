create or replace
procedure atualiza_dados_nfse_gerada(
			nm_usuario_p		varchar2,
			cd_verificacao_nfse_p	varchar2,
			nr_nota_fiscal_p	varchar2,
			nr_nfse_p		varchar2,
			cd_estabelecimento_p	number,
			cd_serie_nf_p		varchar2,
			cd_cpf_cnpj_prest_p	varchar2,
			dt_emissao_nfe_p	varchar2
			) is 


nr_sequencia_w		number(10);
dt_emissao_nfe_aux_w	varchar2(50);
dt_emissao_nfe_w 	date;

begin 

if (dt_emissao_nfe_p is not null) then
	begin
	dt_emissao_nfe_aux_w := dt_emissao_nfe_p;
	dt_emissao_nfe_w     := replace(dt_emissao_nfe_aux_w, 'T', ' ');

	exception when others then
		begin
		
		if	(length(trim(dt_emissao_nfe_aux_w)) > 19) then
			begin
			dt_emissao_nfe_aux_w := substr(dt_emissao_nfe_aux_w,1,19);
			end;
		end if;	
		
		dt_emissao_nfe_w		:= to_date(replace(dt_emissao_nfe_aux_w, 'T', ' '), 'yyyy-mm-dd hh24:mi:ss');
		exception when others then
			begin
			dt_emissao_nfe_w	:= to_date(replace(dt_emissao_nfe_aux_w, 'T', ' '), 'dd-mm-yyyy hh24:mi:ss');
			exception when others then
			--'N�o foi poss�vel converter a data de emiss�o enviada no arquivo: ' + dt_emissao_nfe_p;
				wheb_mensagem_pck.exibir_mensagem_abort(265333,'DT_EMISSAO=' || dt_emissao_nfe_p);
			end;
		end;


	end;
end if;

/* OS 365130
if (dt_emissao_nfe_p is not null) then

dt_emissao_nfe_w := TO_DATE(SUBSTR(dt_emissao_nfe_p,9,2) || '/' || SUBSTR(dt_emissao_nfe_p,6,2) || '/' || SUBSTR(dt_emissao_nfe_p,1,4) || ' ' || SUBSTR(dt_emissao_nfe_p,12,8), 'dd/mm/yyyy hh:mi:ss');

end if;
*/

select	nvl(max(nr_sequencia), 0)
into	nr_sequencia_w
from	nota_fiscal
where	cd_estabelecimento	= cd_estabelecimento_p
and	nr_nota_fiscal		= nr_nota_fiscal_p
and	cd_serie_nf		= cd_serie_nf_p
and	cd_cgc_emitente		= cd_cpf_cnpj_prest_p
and	nvl(nr_nfe_imp, 0) <> nr_nfse_p;

if	(nr_sequencia_w > 0) then

	update	nota_fiscal
	set	nr_nfe_imp 		= nr_nfse_p,
		cd_verificacao_nfse	= cd_verificacao_nfse_p,
		dt_emissao_nfe		= dt_emissao_nfe_w
	where	nr_sequencia 		= nr_sequencia_w;

	update	nota_fiscal_item
	set	nr_nfe_imp 	= nr_nfse_p
	where	nr_sequencia 	= nr_sequencia_w;

	insert into nota_fiscal_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_nota,
		cd_evento,
		ds_historico)
	values(	nota_fiscal_hist_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		'27',
		'NFS-e: ' || nr_nfse_p);
		
end if;

commit;

end atualiza_dados_nfse_gerada;
/
