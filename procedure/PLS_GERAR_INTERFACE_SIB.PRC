create or replace
procedure pls_gerar_interface_sib
			(	nr_seq_lote_sib_p		number,
				ie_gerar_todos_p		varchar2,
				ie_opcao_p			varchar2,
				nm_usuario_p			varchar2,
				qt_inconsistencias_p	out	number,
				cd_estabelecimento_p 		number) is
/* ie_gerar_todos_p
	E - Exclus�o de todos
	S - Inclus�o de todos
	N - Inclus�o/Altera��o/Exclus�o do m�s de refer�ncia 
	A - Altera��o dos cart�es de identifica��o do sistema anterior*/
	
	
/* ie_opcao_p
	C - Gerar lote SIB
	D - Desfazer lote SIB 
	R - Gerar lote re-envio do SIB 
	A - Altera��o dos cart�es de identifica��o do sistema anterior*/

cd_cgc_w			varchar2(14);
cd_ans_w			varchar2(20);
dt_geracao_w			date;
dt_referencia_w			date;
nr_seq_arquivo_w		number(10);
nr_seq_reg_arquivo_w		number(10);
ie_ajuste_conferencia_w		varchar2(10);

nr_seq_segurado_w		number(10);
nr_cco_w			number(10);
cd_usuario_plano_w		varchar2(30);
ie_tipo_reenvio_w		number(2);
qt_header_w			number(10);
qt_trailer_w			number(10);
qt_tipo1_w			number(10);
qt_tipo2_w			number(10);
qt_tipo5_w			number(10);
qt_tipo6_w			number(10);
qt_tipo7_w			number(10);
qt_tipo8_w			number(10);

qt_registros_incosistentes_w	number(10);
qt_registros_w			number(10);
ie_flexibilizado_w		varchar2(1);
ie_entrou_flex_w		varchar2(1):='N';

begin

if	(ie_opcao_p	= 'D') then
	update	pls_retorno_sib
	set	nr_seq_lote_envio	= null
	where	nr_seq_lote_envio	= nr_seq_lote_sib_p;
	
	update	pls_lote_sib
	set	dt_geracao		= null,
		dt_geracao_arquivo	= null
	where	nr_sequencia		= nr_seq_lote_sib_p;

	update	sib_devolucao_erro
	set	nr_seq_lote_envio	= null,
		ie_reenvio		= 'S'
	where	nr_seq_lote_envio	= nr_seq_lote_sib_p;
	
	update	sib_devolucao_erro
	set	nr_seq_interf_sib	= null
	where	nr_seq_lote		= nr_seq_lote_sib_p;
	
	delete	PLS_INCONSIST_CONF_SIB
	where	nr_seq_lote_sib	= nr_seq_lote_sib_p;
	
	update	PLS_SEGURADO_CART_ANT
	set	NR_SEQ_LOTE_SIB	= null
	where	NR_SEQ_LOTE_SIB		= nr_seq_lote_sib_p;
	
	update	sib_log_exclusao
	set	ie_enviar_sib		= 'S',
		nr_seq_lote_envio	= null,
		nr_seq_interf_sib	= null
	where	nr_seq_lote_envio	= nr_seq_lote_sib_p;
	
	pls_excluir_log_excluidos_sib(nr_seq_lote_sib_p, nm_usuario_p);
		
	delete pls_interf_sib
	where  nr_seq_lote_sib = nr_seq_lote_sib_p;
elsif	(nvl(ie_opcao_p,'C')	in ('C','R','A')) then
	update	pls_lote_sib
	set	dt_geracao	= sysdate
	where	nr_sequencia	= nr_seq_lote_sib_p;
	
	--aaschlote 24/08/2011 - Apenas gerar os dados da compara��o da altera��o quando for gerar o lote
	if	(ie_opcao_p = 'C') then
		pls_gerar_segurado_sib(nr_seq_lote_sib_p, nm_usuario_p);
	end if;	
	
	begin
	select  a.cd_cgc_outorgante,
		substr(to_char(somente_numero(a.cd_ans)),1,6)
	into 	cd_cgc_w,
		cd_ans_w
	from	pls_outorgante a
	where 	a.cd_estabelecimento	= cd_estabelecimento_p;
	exception
	when others then
		cd_cgc_w	:= null;
		cd_ans_w	:= null;
	end;
	
	select 	nvl(max(nr_seq_arquivo),0) + 1
	into	nr_seq_arquivo_w
	from	pls_interf_sib;
	
	select	dt_geracao,
		dt_mesano_referencia,
		nvl(ie_ajuste_conferencia,'N'),
		nvl(ie_flexibilizado,'N')
	into	dt_geracao_w,
		dt_referencia_w,
		ie_ajuste_conferencia_w,
		ie_flexibilizado_w
	from	pls_lote_sib
	where	nr_sequencia = nr_seq_lote_sib_p;
	
	select	count(*)
	into	qt_header_w
	from	pls_interf_sib
	where	nr_seq_lote_sib	= nr_seq_lote_sib_p
	and	ie_tipo_reg	= 0;
	
	if	(qt_header_w	= 0) then
		insert into pls_interf_sib
			(nr_sequencia,
			ie_tipo_reg,
			dt_geracao_arquivo,
			cd_ans,
			cd_cgc,
			nr_seq_arquivo,
			dt_mesano_referencia,
			benef,
			ds_constante,
			ds_modalidade,
			ds_branco,
			zero,
			nr_seq_lote_sib)
		values 	(1,
			0,
			dt_geracao_w,
			cd_ans_w,
			cd_cgc_w,
			nr_seq_arquivo_w,
			dt_referencia_w,
			'BENEF',
			'V01',
			'O',
			' ',
			0,
			nr_seq_lote_sib_p);
	end if;
	
	if	(nvl(ie_opcao_p,'C')	= 'C') then
		if	(ie_gerar_todos_p	= 'N') then
			
			/* Inclus�es no m�s de refer�ncia do lote */
			if (ie_flexibilizado_w = 'S') then
				pls_gerar_inclusao_sib_flex(nr_seq_lote_sib_p, ie_gerar_todos_p, nm_usuario_p, cd_estabelecimento_p);
				ie_entrou_flex_w := 'S';
			else
				pls_gerar_inclusao_sib(nr_seq_lote_sib_p, ie_gerar_todos_p, nm_usuario_p, cd_estabelecimento_p);
			end if;	

			/*aaschlote 13/04/2011 OS - 306712 Reinclus�o no m�s de refer�ncia do lote*/
			pls_gerar_reinclusao_sib(nr_seq_lote_sib_p,nm_usuario_p, cd_estabelecimento_p);
			
			/* Altera��es no m�s de refer�ncia do lote */
			if (ie_flexibilizado_w = 'S') then
				pls_gerar_alteracao_sib_flex(nr_seq_lote_sib_p, ie_gerar_todos_p, nm_usuario_p, cd_estabelecimento_p);
				ie_entrou_flex_w := 'S';
			else			
				pls_gerar_alteracao_sib(nr_seq_lote_sib_p, ie_gerar_todos_p, nm_usuario_p, cd_estabelecimento_p);
			end if;
			
			/*aaschlote 08/10/2012 OS - 490891 - Migra��es contratuis no m�s de refer�ncia do lote*/
			if (ie_flexibilizado_w = 'S') then
				pls_gerar_mig_cont_sib_flex(nr_seq_lote_sib_p,cd_estabelecimento_p,nm_usuario_p);
				ie_entrou_flex_w := 'S';
			else			
				pls_gerar_mig_contratual_sib(nr_seq_lote_sib_p,cd_estabelecimento_p,nm_usuario_p);
			end if;
			
			/* Exclus�es no m�s de refer�ncia do lote */
			pls_gerar_exclusao_sib(nr_seq_lote_sib_p, ie_gerar_todos_p, nm_usuario_p, cd_estabelecimento_p);
			
			/*aaschlote 27/04/2011 -OS 313957 - Mandar a Confer�ncia SIB caso esteja marcado no lote o ajuste confer�ncia*/
			if	(ie_ajuste_conferencia_w	= 'S') then
				pls_inserir_conferencia_sib(nr_seq_lote_sib_p, cd_estabelecimento_p,nm_usuario_p);
			end if;	

			select	count(*)
			into	qt_registros_w
			from	pls_interf_sib
			where	nr_seq_lote_sib	= nr_seq_lote_sib_p
			and	ie_tipo_reg	<> 0;
			
			if	(qt_registros_w	= 0) then
				nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
				insert into pls_interf_sib
					(nr_sequencia,
					ie_tipo_reg,
					ds_observacao,
					nr_seq_lote_sib)
				values 	(nr_seq_reg_arquivo_w,
					6,
					'N�o houve atualiza�ao de dados benefici�rios',
					nr_seq_lote_sib_p);
			end if;
			
		elsif	(ie_gerar_todos_p	= 'S') then
			pls_gerar_inclusao_sib(nr_seq_lote_sib_p, ie_gerar_todos_p, nm_usuario_p, cd_estabelecimento_p);
		elsif	(ie_gerar_todos_p	= 'E') then
			pls_gerar_exclusao_sib(nr_seq_lote_sib_p, ie_gerar_todos_p, nm_usuario_p, cd_estabelecimento_p);
		end if;
	elsif	(ie_opcao_p	= 'R') then
		pls_gerar_reenvio_sib(nr_seq_lote_sib_p, nm_usuario_p, cd_estabelecimento_p);
	elsif	(ie_opcao_p	= 'A') then
		pls_gerar_alt_cart_benef_sib(nr_seq_lote_sib_p, nm_usuario_p, cd_estabelecimento_p);
	end if;
	
	select	sum(qt_tipo_1), /* Inclus�o */
		sum(qt_tipo_2), /* Altera��o */
		sum(qt_tipo_5), /*Migra��o contratual*/
		sum(qt_tipo_6), /* Indica��o de inexist�ncia de benefici�rio ou de altera��o cadastral */
		sum(qt_tipo_7), /* Exclus�o */
		sum(qt_tipo_8)  /* Reinclus�o */
	into	qt_tipo1_w,
		qt_tipo2_w,
		qt_tipo5_w,
		qt_tipo6_w,
		qt_tipo7_w,
		qt_tipo8_w
	from	(select	decode(ie_tipo_reg,1,1,0) qt_tipo_1,
			decode(ie_tipo_reg,2,1,0) qt_tipo_2,
			decode(ie_tipo_reg,5,1,0) qt_tipo_5,
			decode(ie_tipo_reg,6,1,0) qt_tipo_6,
			decode(ie_tipo_reg,7,1,0) qt_tipo_7,
			decode(ie_tipo_reg,8,1,0) qt_tipo_8
		from	pls_interf_sib
		where	nr_seq_lote_sib = nr_seq_lote_sib_p);
	
	select	max(nr_sequencia)
	into	nr_seq_reg_arquivo_w
	from	pls_interf_sib
	where	nr_seq_lote_sib	= nr_seq_lote_sib_p;
	
	if	(nvl(nr_seq_reg_arquivo_w,0) = 0) then
		nr_seq_reg_arquivo_w	:= 1;
	end if;

	nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
	
	if (ie_entrou_flex_w = 'N') then
		pls_consistir_lote_sib(nr_seq_lote_sib_p, null, nm_usuario_p, cd_estabelecimento_p);
	else
		pls_consistir_lote_sib_flex(nr_seq_lote_sib_p, null, nm_usuario_p, cd_estabelecimento_p);
	end if;	
	
	select	count(*)
	into	qt_trailer_w
	from	pls_interf_sib
	where	nr_seq_lote_sib	= nr_seq_lote_sib_p
	and	ie_tipo_reg	= 9;
	
	if	(qt_trailer_w	= 0) then
		select 	count(*)
		into	qt_registros_incosistentes_w
		from 	pls_interf_sib a 
		where 	nr_seq_lote_sib = nr_seq_lote_sib_p
		and	exists (	select	1
					from	pls_lote_consistencia_sib b
					where	b.nr_seq_lote_sib	= a.nr_seq_lote_sib
					and	b.nr_seq_segurado = a.nr_seq_segurado);
		
		insert into pls_interf_sib
			(nr_sequencia,
			ie_tipo_reg,
			qt_tipo1,
			qt_tipo2,
			qt_tipo5,
			qt_tipo6,
			qt_tipo7,
			qt_tipo8,
			dt_mesano_referencia,
			ds_branco,
			nr_seq_lote_sib,
			qt_registros_incosistentes)
		values 	(nr_seq_reg_arquivo_w,
			9,
			qt_tipo1_w,
			qt_tipo2_w,
			qt_tipo5_w,
			qt_tipo6_w,
			qt_tipo7_w,
			qt_tipo8_w,
			dt_referencia_w,
			'',
			nr_seq_lote_sib_p,
			qt_registros_incosistentes_w);
	end if;
end if;

--aaschlote 27/04/2011 - Ajustar a seq. ordem para envio do SIB
if	(ie_ajuste_conferencia_w	= 'S') then
	sib_ordenar_segurados_envio(nr_seq_lote_sib_p,nm_usuario_p);
end if;	

--aaschlote 13/07/2011 - Ajustar o nome dos benefici�rios e das m�es dos registros inclus�o e altera��o
pls_ajustar_nomes_sib(nr_seq_lote_sib_p,cd_estabelecimento_p,nm_usuario_p);

/*aaschlote 25/07/2013 - Retorna a quantidade de registros inconsistentes*/
if	(ie_opcao_p <> 'D') then
	qt_inconsistencias_p := qt_registros_incosistentes_w;
end if;

commit;

end pls_gerar_interface_sib;
/