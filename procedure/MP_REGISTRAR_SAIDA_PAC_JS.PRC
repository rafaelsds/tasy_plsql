create or replace
procedure mp_registrar_saida_pac_js(
		nr_atendimento_p		number,
		nr_sequencia_p		number,
		nm_usuario_p		varchar2) is
		
begin
if	(nr_atendimento_p is not null) then
begin
	update	atendimento_paciente 
	set	nr_seq_local_pa = null,
		nm_usuario	= nm_usuario_p
	where	nr_atendimento 	= nr_atendimento_p;
	end;
	commit;
end if;

if	(nr_sequencia_p is not null) then
begin
	update	historico_localizacao_pa 
	set	dt_saida_local 	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia 	= nr_sequencia_p;
	end;
	commit;
end if;

end mp_registrar_saida_pac_js;
/
