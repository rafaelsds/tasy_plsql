create or replace
procedure gc_cirurgia_beforedelete_js(
				nr_prescricao_p			number,				
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				ds_pergunta_p		out	varchar2
				)is

excl_cirur_prescr_mat_med_w	varchar2(1);
ds_pergunta_w			varchar2(255):= '';
qt_prescr_material_w          	number(10):= 0;
qt_prescr_procedimento_w	number(10):= 0;
				
begin

obter_param_usuario(900, 143, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, excl_cirur_prescr_mat_med_w);

select	count(*) 
into	qt_prescr_material_w
from	prescr_material
where	nr_prescricao = nr_prescricao_p;

select	count(*)
into	qt_prescr_procedimento_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p;


if	((excl_cirur_prescr_mat_med_w = 'N') or 
	(excl_cirur_prescr_mat_med_w = 'Q')) and
	(qt_prescr_material_w <> 0) or
	(qt_prescr_procedimento_w <> 0) then
	begin
	
	if	(excl_cirur_prescr_mat_med_w = 'N') then
		begin
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(49906);
		
		end;
	else
		begin
		ds_pergunta_w := substr(obter_texto_tasy (49907, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		end;
	end if;
		
	end;
end if;

commit;

ds_pergunta_p := ds_pergunta_w;

end gc_cirurgia_beforeDelete_js;
/
