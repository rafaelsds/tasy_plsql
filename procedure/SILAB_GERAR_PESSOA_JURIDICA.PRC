create or replace procedure silab_gerar_pessoa_juridica
			(	cd_sistema_ant_p	varchar2,
				ds_razao_social_p	varchar2,
				nm_fantasia_p		varchar2,
				ds_endereco_p		varchar2,
				ds_municipio_p		varchar2,
				cd_cep_p		varchar2,
				sg_estado_p		varchar2,
				ie_prod_fabric_p	varchar2,
				ie_situacao_p		varchar2,
				nr_seq_principal_p	number,
				ie_inclusao_p		varchar2,
				ie_status_integracao_p	varchar2,
				ie_direcao_p		varchar2,
				cd_cgc_p		varchar2
				
			) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [  X ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin
insert into silab_pessoa_juridica
	(	nr_sequencia,--
		dt_atualizacao,--
		nm_usuario,--
		dt_atualizacao_nrec,--
		nm_usuario_nrec,--
		cd_tipo_pessoa,--
		cd_sistema_ant,--
		ds_razao_social,--
		nm_fantasia,--
		ds_endereco,--
		ds_municipio,--
		cd_cep,--
		sg_estado,--
		ie_prod_fabric,--
		ie_situacao,--
		nr_seq_principal,--
		ie_inclusao,--
		ie_status_integracao,--
		dt_gravacao,
		dt_integracao,
		ie_direcao,
		cd_cgc
		)
 values	(	silab_pessoa_juridica_seq.nextval, 
		sysdate,
		'SILAB',
		sysdate,
		'SILAB',
		'14',
		cd_sistema_ant_p,
		ds_razao_social_p,
		nm_fantasia_p,
		ds_endereco_p,
		ds_municipio_p,
		cd_cep_p,
		sg_estado_p,
		ie_prod_fabric_p,
		ie_situacao_p,
		nr_seq_principal_p,
		ie_inclusao_p,
		ie_status_integracao_p,
		sysdate,
		sysdate,
		ie_direcao_p,
		cd_cgc_p); 


commit;

end silab_gerar_pessoa_juridica;
/
