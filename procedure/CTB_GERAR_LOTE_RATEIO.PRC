create or replace
procedure ctb_gerar_lote_rateio(	nr_seq_mes_ref_p	number,
									cd_estabelecimento_p	number,
									nm_usuario_p		varchar2,
									dt_referencia_p		date) is 

cd_conta_origem_w		varchar2(20);
cd_centro_origem_w		number(10);
cd_conta_contabil_dest_w		varchar2(20);
cd_centro_custo_ww		number(10);
cd_conta_contabil_ww		varchar2(20);

cd_centro_custo_dest_w		number(10);
cd_classif_credito_w		varchar2(40);
cd_classif_debito_w		varchar2(40);
cd_conta_contabil_w		varchar2(20);
cd_conta_debito_w			varchar2(40);
cd_conta_credito_w		varchar2(40);
ie_debito_credito_w			varchar2(01);
cd_historico_w			number(10);
cd_tipo_lote_contabil_w		number(10);
ds_observacao_w			varchar2(2000);
dt_atualizacao_saldo_w		date;
dt_movimento_w			date;
dt_referencia_w			date;
ie_lote_gerado_w			varchar2(1)	:= 'N';
nr_lote_contabil_w			number(10);
nr_seq_contrapartida_w		number(10);
nr_sequencia_w			number(10);
nr_seq_regra_rateio_w		number(10);
pr_rateio_w			number(15,2);
pr_total_rateio_w			number(15,2);
vl_base_w			number(15,2);
vl_movimento_w			number(15,2);
vl_total_rateio_w			number(15,2);

cursor c01 is
select	a.nr_sequencia,
		a.cd_conta_origem,
		a.cd_centro_origem,
		a.cd_historico,
		--a.dt_movimento,
		a.pr_total_rateio
from	ctb_regra_rat_contabil a
where	a.cd_estabelecimento = cd_estabelecimento_p
and		dt_referencia_p between dt_inicio_vigencia and nvl(dt_final_vigencia,dt_referencia_p)
and	nvl(ie_regra_rat_saldo,'N') = 'N'
order by a.nr_seq_calculo, a.nr_sequencia;

cursor c02 is
select	a.cd_centro_custo,
		a.cd_conta_contabil,
		a.cd_classificacao,
		a.vl_movimento
from	ctb_saldo a
where	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
and		a.cd_estabelecimento	= cd_estabelecimento_p
and		a.cd_conta_contabil	= nvl(cd_conta_origem_w, a.cd_conta_contabil)
/*and	nvl(a.cd_centro_custo,0) = nvl(cd_centro_origem_w,0)*/
/*and	nvl(a.cd_centro_custo, nvl(cd_centro_origem_w,0)) = nvl(cd_centro_origem_w,0)*/	-- M�rcio  OS: 486261
and		(((a.cd_centro_custo = cd_centro_origem_w)
and		(nvl(cd_centro_origem_w,0) > 0))
or		(nvl(cd_centro_origem_w,0) = 0))
and		a.vl_movimento > 0
order by a.cd_conta_contabil, a.cd_centro_custo;

vet02	C02%RowType;

cursor c03 is
select	nvl(a.cd_conta_contabil_dest, cd_conta_contabil_ww),
		nvl(a.cd_centro_custo_dest, cd_centro_custo_ww),
		a.pr_rateio
from	ctb_regra_rat_dest a
where	a.nr_seq_regra_rateio	= nr_seq_regra_rateio_w
order by 1,2;

begin

dt_movimento_w := dt_referencia_p;

select	nvl(max(campo_numerico(nvl(vl_parametro, vl_parametro_padrao))),0)
into	cd_tipo_lote_contabil_w
from	funcao_parametro
where	cd_funcao	= 923
and		nr_sequencia	= 3;

open C01;
loop
fetch C01 into	
	nr_seq_regra_rateio_w,
	cd_conta_origem_w,
	cd_centro_origem_w,
	cd_historico_w,
	--dt_movimento_w,
	pr_total_rateio_w;
exit when C01%notfound;
	begin
	
	
	if	(nr_lote_contabil_w is null) and
		(cd_tipo_lote_contabil_w <> 0) then
		begin
		
		select	max(dt_referencia)
		into	dt_referencia_w
		from	ctb_mes_ref
		where	nr_sequencia	= nr_seq_mes_ref_p;

		if	(trunc(dt_referencia_w,'mm') <>
			 trunc(dt_movimento_w,'mm')) then
			wheb_mensagem_pck.exibir_mensagem_abort(260279); 
		end if;
		
		select	nvl(max(nr_lote_contabil),0) + 1
		into	nr_lote_contabil_w
		from	lote_contabil;
		
		insert into lote_contabil (
			nr_lote_contabil,
			dt_referencia,
			cd_tipo_lote_contabil,
			dt_atualizacao,
			nm_usuario,
			cd_estabelecimento,
			ie_situacao,
			vl_debito,
			vl_credito,
			dt_integracao,
			dt_atualizacao_saldo,
			dt_consistencia,
			nm_usuario_original,
			nr_seq_mes_ref,
			ie_encerramento,
			ds_observacao)
		values(	nr_lote_contabil_w,
			trunc(dt_movimento_w),
			cd_tipo_lote_contabil_w,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			'A', 0, 0, null, null, null, 
			nm_usuario_p,
			nr_seq_mes_ref_p,
			'N',
			obter_desc_expressao(779283));
			
		
		ie_lote_gerado_w	:= 'S';
		end;
	end if;
	cd_conta_contabil_w		:= '0';
	open C02;
	loop
	fetch C02 into	
		vet02;
	exit when C02%notfound;
		begin
	
		vl_base_w		:= vet02.vl_movimento * dividir(pr_total_rateio_w,100);
		vl_total_rateio_w	:= 0;
		cd_conta_debito_w	:= '';
		cd_conta_credito_w	:= '';
		cd_classif_debito_w	:= '';
		cd_classif_credito_w	:= '';
		
		select	nvl(max(ie_debito_credito),'D')
		into	ie_debito_credito_w
		from	ctb_grupo_conta b,
			conta_contabil a
		where	a.cd_grupo		= b.cd_grupo
		and	a.cd_conta_contabil	= vet02.cd_conta_contabil;
		
		
		if	(ie_debito_credito_w = 'D') then
			cd_conta_credito_w	:= vet02.cd_conta_contabil;
			cd_classif_credito_w	:= substr(ctb_obter_classif_conta(cd_conta_credito_w,null,dt_movimento_w),1,40);
		else
			cd_conta_debito_w	:= vet02.cd_conta_contabil;
			cd_classif_debito_w	:= substr(ctb_obter_classif_conta(cd_conta_debito_w,null,dt_movimento_w),1,40);
		end if;
		
		/*Movimento de Contra-partida*/
		if	(vet02.cd_conta_contabil <> cd_conta_contabil_w) then
			begin
			
			select	ctb_movimento_seq.nextval
			into	nr_seq_contrapartida_w
			from	dual;
			
			insert into ctb_movimento(
				nr_sequencia,
				nr_lote_contabil,
				nr_seq_mes_ref,
				dt_movimento,
				vl_movimento,
				dt_atualizacao,
				nm_usuario,
				cd_historico,
				cd_conta_debito,
				cd_conta_credito,
				ds_compl_historico,
				nr_seq_agrupamento,
				ie_revisado,
				cd_estabelecimento,
				cd_classif_debito,
				cd_classif_credito)
			values(	nr_seq_contrapartida_w,
				nr_lote_contabil_w,
				nr_seq_mes_ref_p,
				dt_movimento_w,
				vl_base_w, 
				sysdate,
				nm_usuario_p,
				cd_historico_w, 
				cd_conta_debito_w,
				cd_conta_credito_w, 
				null, 
				null,
				'N',
				cd_estabelecimento_p,
				cd_classif_debito_w,
				cd_classif_credito_w);
			end;
		else
			update	ctb_movimento
			set	vl_movimento	= vl_movimento + vl_base_w
			where	nr_Sequencia	= nr_seq_contrapartida_w;
			
		end if;
		
		if	(vet02.cd_centro_custo is not null) and
			(nr_seq_contrapartida_w is not null) then
			begin
			insert into ctb_movto_centro_custo(
				nr_sequencia,
				nr_seq_movimento,
				cd_centro_custo,
				dt_atualizacao,
				nm_usuario,
				vl_movimento,
				pr_rateio)
			values(	ctb_movto_centro_custo_seq.nextval,
				nr_seq_contrapartida_w,
				vet02.cd_centro_custo,
				sysdate,
				nm_usuario_p,
				vl_base_w,
				100);
			end;
		end if;
		cd_conta_contabil_w	:= vet02.cd_conta_contabil;
		cd_conta_contabil_ww	:= vet02.cd_conta_contabil;
		cd_centro_custo_ww	:= vet02.cd_centro_custo;
		nr_sequencia_w		:= null;
		open C03;
		loop
		fetch C03 into	
			cd_conta_contabil_dest_w,
			cd_centro_custo_dest_w,
			pr_rateio_w;
		exit when C03%notfound;
			begin
			if	(nr_sequencia_w is null) then
				cd_conta_contabil_ww	:= '0';
			end if;
			
			cd_conta_debito_w		:= '';
			cd_conta_credito_w		:= '';
			cd_classif_credito_w		:= '';
			cd_classif_debito_w		:= '';
			ie_debito_credito_w		:= '';
			vl_movimento_w			:= vl_base_w * dividir(pr_rateio_w,100);
			vl_total_rateio_w		:= vl_total_rateio_w + vl_movimento_w;
			
			select	nvl(max(ie_debito_credito),'D')
			into	ie_debito_credito_w
			from	ctb_grupo_conta b,
				conta_contabil a
			where	a.cd_grupo	= b.cd_grupo
			and	a.cd_conta_contabil	= cd_conta_contabil_dest_w;
			
			if	(ie_debito_credito_w = 'D') then
				cd_conta_debito_w	:= cd_conta_contabil_dest_w;
				cd_classif_debito_w	:= substr(ctb_obter_classif_conta(cd_conta_debito_w,null,dt_movimento_w),1,40);
			else
				cd_conta_credito_w	:= cd_conta_contabil_dest_w;
				cd_classif_credito_w	:= substr(ctb_obter_classif_conta(cd_conta_credito_w,null,dt_movimento_w),1,40);
			end if;

			if	(cd_conta_contabil_dest_w <> cd_conta_contabil_ww) then
				
				select	ctb_movimento_seq.nextval
				into	nr_sequencia_w
				from	dual;
			
				insert into ctb_movimento(
					nr_sequencia,
					nr_lote_contabil,
					nr_seq_mes_ref,
					dt_movimento,
					vl_movimento,
					dt_atualizacao,
					nm_usuario,
					cd_historico,
					cd_conta_debito,
					cd_conta_credito,
					ds_compl_historico,
					nr_seq_agrupamento,
					ie_revisado,
					cd_estabelecimento,
					cd_classif_debito,
					cd_classif_credito)
				values(	nr_sequencia_w,
					nr_lote_contabil_w,
					nr_seq_mes_ref_p,
					dt_movimento_w,
					vl_movimento_w, 
					sysdate,
					nm_usuario_p,
					cd_historico_w, 
					cd_conta_debito_w,
					cd_conta_credito_w, 
					null, 
					null,
					'N',
					cd_estabelecimento_p,
					cd_classif_debito_w,
					cd_classif_credito_w);
			else
				update	ctb_movimento
				set	vl_movimento	= vl_movimento + vl_movimento_w
				where	nr_sequencia	= nr_sequencia_w;
			end if;
			
			if	(nr_sequencia_w is not null) and
				(cd_centro_custo_dest_w is not null) then
			
				insert into ctb_movto_centro_custo(
					nr_sequencia,
					nr_seq_movimento,
					cd_centro_custo,
					dt_atualizacao,
					nm_usuario,
					vl_movimento,
					pr_rateio)
				values(	ctb_movto_centro_custo_seq.nextval,
					nr_sequencia_w,
					cd_centro_custo_dest_w,
					sysdate,
					nm_usuario_p,
					vl_movimento_w,
					100);
			end if;
			
			cd_conta_contabil_ww	:= cd_conta_contabil_dest_w;
			
			end;
		end loop;
		close C03;

		cd_conta_contabil_w	:= vet02.cd_conta_contabil;
		end;
	end loop;
	close C02;
	
	/*if	(nr_sequencia_w is not null) and
		(nr_lote_contabil_w is not null) then
		
		update	ctb_regra_rateio
		set	nr_lote_contabil	= nr_lote_contabil_w
		where	nr_sequencia		= nr_seq_regra_rateio_w;
	end if;*/
	
	end;
end loop;
close C01;

commit;

end ctb_gerar_lote_rateio;
/