create or replace
procedure san_inutiliza_sangria(	nr_seq_sangria_p		number,
				nr_seq_motivo_p		number,
				ds_observacao_p		varchar2,
				cd_estabelecimento_p 	number,
				nm_usuario_p		Varchar2) is 
cd_pessoa_fisica_w	varchar2(10);
nr_seq_inutil_w		number(10);
begin
select 	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	usuario
where	nm_usuario = nm_usuario_p;

select	san_inutilizacao_seq.nextval
into	nr_seq_inutil_w
from	dual;

if	(nr_seq_sangria_p is not null) then

	insert into san_inutilizacao(
		nr_sequencia,
		dt_inutilizacao,
		nr_seq_motivo_inutil,
		dt_atualizacao,
		nm_usuario,
		cd_pf_realizou,
		ds_observacao,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_fechamento,	
		cd_estabelecimento,
		nm_usuario_fechamento)
	values( nr_seq_inutil_w,
		sysdate,
		nr_seq_motivo_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_w,
		ds_observacao_p,
		sysdate,
		nm_usuario_p,
		sysdate,	
		cd_estabelecimento_p,
		nm_usuario_p);

	update	san_sangria_terapeutica
	set	nr_seq_inutil 	= nr_seq_inutil_w,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		dt_inutilizacao = sysdate, 
		nm_usuario_inut = nm_usuario_p
	where	nr_sequencia	= nr_seq_sangria_p;
end if;

commit;

end san_inutiliza_sangria;
/
