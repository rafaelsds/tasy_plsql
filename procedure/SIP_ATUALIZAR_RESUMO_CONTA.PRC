create or replace
procedure sip_atualizar_resumo_conta
			(	nr_seq_lote_sip_p	Number,
				nm_usuario_p		Varchar2) is 

dt_periodo_inicial_w		Date;
dt_periodo_final_w		Date;
ie_tipo_conta_w			Varchar2(1);
nr_seq_conta_w			Number(10);
nr_seq_item_w			Number(10);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
ie_tipo_guia_w			Varchar2(2);
nr_seq_tipo_atendimento_w	Number(10);
cd_medico_executor_w		Varchar2(10);
ie_regime_internacao_w		Varchar2(1);
nr_seq_conselho_w		Number(10);
nr_seq_grupo_ans_w		Number(10);
ie_tipo_desp_mat_w		Varchar2(2);
cd_estabelecimento_w		number(10);

Cursor C01 is
	select	1,
		b.nr_sequencia,
		c.nr_sequencia,
		c.cd_procedimento,
		c.ie_origem_proced,
		b.cd_estabelecimento
	from	pls_conta_proc		c,
		pls_conta		b,
		pls_protocolo_conta	a
	where	a.nr_sequencia	= b.nr_seq_protocolo
	and	b.nr_sequencia	= c.nr_seq_conta
	and	a.ie_status	= 3
	and	a.dt_mes_competencia between dt_periodo_inicial_w and dt_periodo_final_w
	and	nvl(b.ie_tipo_segurado,'B') in ('B','A')
	union all
	select	2,
		b.nr_sequencia,
		c.nr_sequencia,
		null,
		null,
		b.cd_estabelecimento
	from	pls_conta_mat		c,
		pls_conta		b,
		pls_protocolo_conta	a
	where	a.nr_sequencia	= b.nr_seq_protocolo
	and	b.nr_sequencia	= c.nr_seq_conta
	and	a.ie_status	= 3
	and	a.dt_mes_competencia between dt_periodo_inicial_w and dt_periodo_final_w
	and	nvl(b.ie_tipo_segurado,'B') in ('B','A');

begin

select	dt_periodo_inicial,
	nvl(dt_periodo_final, sysdate)
into	dt_periodo_inicial_w,
	dt_periodo_final_w
from	pls_lote_sip
where	nr_sequencia	= nr_seq_lote_sip_p;

open C01;
loop
fetch C01 into	
	ie_tipo_conta_w,
	nr_seq_conta_w,
	nr_seq_item_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_estabelecimento_w;
exit when C01%notfound;
	begin
	select	ie_tipo_guia,
		nr_seq_tipo_atendimento,
		cd_medico_executor,
		ie_regime_internacao
	into	ie_tipo_guia_w,
		nr_seq_tipo_atendimento_w,
		cd_medico_executor_w,
		ie_regime_internacao_w
	from	pls_conta a
	where	nr_sequencia	= nr_seq_conta_w;
	
	select	max(nr_seq_conselho)
	into	nr_seq_conselho_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_medico_executor_w;
	
	if	(ie_tipo_conta_w = '2') then
		select	nvl(max(ie_tipo_despesa),'')
		into	ie_tipo_desp_mat_w
		from	pls_conta_mat
		where	nr_sequencia	= nr_seq_item_w;
	end if;
	
	select	pls_obter_grupo_ans(cd_procedimento_w, ie_origem_proced_w, nr_seq_conselho_w, 
				nr_seq_tipo_atendimento_w, ie_tipo_guia_w, ie_regime_internacao_w, 
				ie_tipo_desp_mat_w, 'G', cd_estabelecimento_w,
				nr_seq_conta_w)
	into	nr_seq_grupo_ans_w
	from	dual;
	
	delete	from sip_resumo_conta
	where	nr_seq_conta	= nr_seq_conta_w
	and	ie_tipo_conta	= ie_tipo_conta_w
	and	nr_seq_item	= nr_seq_item_w;
	
	sip_gerar_resumo_conta(nr_seq_item_w, ie_tipo_conta_w, nm_usuario_p);
	end;
end loop;
close C01;

commit;

end sip_atualizar_resumo_conta;
/
