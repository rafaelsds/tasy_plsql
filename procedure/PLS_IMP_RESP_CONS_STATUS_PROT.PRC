create or replace
procedure pls_imp_resp_cons_status_prot(cd_operadora_p				ptu_resp_cons_status_pa.cd_operadora%type,
					cd_operadora_destino_p          	ptu_resp_cons_status_pa.cd_operadora_destino%type,
					cd_operadora_origem_p           	ptu_resp_cons_status_pa.cd_operadora_origem%type,
					cd_transacao_p                  	ptu_resp_cons_status_pa.cd_transacao%type,
					ie_tipo_cliente_p	              	ptu_resp_cons_status_pa.ie_tipo_cliente%type,
					nr_registro_ans_p               	ptu_resp_cons_status_pa.nr_registro_ans%type,
					cd_usuario_plano_p	             	ptu_resp_cons_status_pa.cd_usuario_plano%type,
					nr_protocolo_p	                 	ptu_resp_cons_status_pa.nr_protocolo%type,
					nr_seq_execucao_p               	ptu_resp_cons_status_pa.nr_seq_execucao%type,
					nr_trans_intercambio_p          	ptu_solicitacao_pa.nr_trans_intercambio%type,
					ie_origem_resposta_p            	ptu_resp_cons_status_pa.ie_origem_resposta%type,
					nr_versao_p                     	ptu_resp_cons_status_pa.nr_versao%type,
					nm_usuario_p				usuario.nm_usuario%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nr_seq_resp_consulta_status_p	out	ptu_resp_cons_status_pa.nr_sequencia%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a importa��o dos dados da transa��o de Resposta da Consulta de Status de Protocolo recebidas via WebService
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [ x ] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin


pls_imp_xml_integracao_ws_pck.pls_imp_resp_cons_status_prot(	cd_operadora_p, cd_operadora_destino_p, cd_operadora_origem_p,
								cd_transacao_p, ie_tipo_cliente_p, nr_registro_ans_p,
								cd_usuario_plano_p, nr_protocolo_p, nr_seq_execucao_p,
								nr_trans_intercambio_p, ie_origem_resposta_p, nr_versao_p,
								nm_usuario_p, cd_estabelecimento_p, nr_seq_resp_consulta_status_p);

end pls_imp_resp_cons_status_prot;
/