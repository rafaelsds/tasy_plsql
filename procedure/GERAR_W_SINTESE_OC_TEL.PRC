create or replace
procedure gerar_w_sintese_oc_tel(
			nr_seq_ordem_p		number,
			tp_telefone_p		varchar2,
			nr_telefone_p		varchar2,
			nr_ramal_p		varchar2,
			nm_usuario_p		varchar2) is 

nr_seq_oc_tel_w	number(10);
			
begin

select	w_sintese_oc_tel_seq.nextval
into	nr_seq_oc_tel_w
from	dual;

insert into w_sintese_oc_tel(	
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_ordem,
	tp_telefone,
	nr_telefone,
	nr_ramal)
values(	nr_seq_oc_tel_w,
	sysdate,
	nm_usuario_p,
	sysdate,    
	nm_usuario_p,
	nr_seq_ordem_p,
	tp_telefone_p,
	nr_telefone_p,
	nr_ramal_p);

commit;

end gerar_w_sintese_oc_tel;
/