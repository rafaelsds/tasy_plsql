create or replace
procedure atualizar_transmitir_nfe(
			nr_seq_transmissao_p  number,
			nm_usuario_p	      Varchar2) is 

begin

if (nr_seq_transmissao_p is not null) then
	begin
		update	nfe_transmissao 
		set	ie_status_transmissao = 'EC' 
		where	nr_sequencia = nr_seq_transmissao_p;
		commit;
	end;
end if;

end atualizar_transmitir_nfe;
/