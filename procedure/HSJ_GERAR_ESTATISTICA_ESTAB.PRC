create or replace
procedure hsj_gerar_estatistica_estab is 

cd_estabelecimento_w	number(10);
ds_comando_w		varchar2(255);
qt_estab_1_w		number(10);
qt_estab_2_w		number(10);
qt_total_reg_w		number(10);

cursor C01 is
select	a.nm_tabela,
	substr(ds_tabela,1,100) ds_tabela,
        b.ds_aplicacao,
	c.nm_atributo
from	integridade_atributo c,
	integridade_referencial a,
	tabela_sistema b
where	a.nm_tabela			= b.nm_tabela
and	a.nm_integridade_referencial	= c.nm_integridade_referencial
and	a.nm_tabela_referencia		= 'ESTABELECIMENTO'
order by a.nm_tabela;

vet01	C01%RowType;

begin

delete hsj_tabelas_estab;

commit;

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin

	qt_estab_1_w		:= 0;
	qt_estab_2_w		:= 0;
	
	ds_comando_w		:= 'select count(*) from ' || vet01.nm_tabela;
	obter_valor_dinamico(ds_comando_w, qt_total_reg_w);
	
	if	(qt_total_reg_w > 0) then
		begin
		ds_comando_w		:= 'select count(*) from ' || vet01.nm_tabela || ' where ' || vet01.nm_atributo || ' = :cd_estabelecimento';
		
		cd_estabelecimento_w	:= 1;
		
		obter_valor_dinamico_bv(ds_comando_w,'cd_estabelecimento=' || cd_estabelecimento_w || ';',qt_estab_1_w);
		
		cd_estabelecimento_w	:= 2;
		
		obter_valor_dinamico_bv(ds_comando_w,'cd_estabelecimento=' || cd_estabelecimento_w || ';',qt_estab_2_w);
		
		insert into hsj_tabelas_estab(
			nm_tabela,
			qt_total_registro,
			qt_estab_1,
			qt_estab_2)
		values(	vet01.nm_tabela,
			qt_total_reg_w,
			qt_estab_1_w,
			qt_estab_2_w);
		end;
	end if;
	end;
end loop;
close C01;

commit;

end hsj_gerar_estatistica_estab;
/