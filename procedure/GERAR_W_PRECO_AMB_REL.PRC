create or replace procedure gerar_w_preco_amb_rel(	cd_edicao_amb_p		number,
						cd_convenio_p		number,
						cd_categoria_p		varchar2,
						cd_especialidade_p		number,
						dt_inicio_vigencia_p	date) is

dt_atualizacao_w			date		:= sysdate;
qt_pontos_w			preco_amb.qt_pontuacao%type;
cd_procedimento_w		number(15)	:= 0;
vl_procedimento_w			number(15,2) 	:= 0;
vl_custo_operacional_w		number(15,2) 	:= 0;
vl_anestesista_w			number(15,2) 	:= 0;
vl_medico_w			number(15,2) 	:= 0;
vl_auxiliares_w			number(15,2) 	:= 0;
vl_filme_w			number(15,4) 	:= 0;
qt_filme_w			number(15,4) 	:= 0;
nr_auxiliares_w			number(2)    	:= 0;
nr_incidencia_w			number(2)    	:= 0;
qt_porte_anestesico_w		number(2)    	:= 0;
cd_estabelecimento_w		number(4)    	:= 1;
vl_pto_custo_operac_w		number(15,2) 	:= 0;
vl_pto_procedimento_w		number(15,2) 	:= 0;
vl_pto_anestesista_w		number(15,2) 	:= 0;
vl_pto_medico_w			number(15,2) 	:= 0;
vl_pto_auxiliares_w			number(15,4) 	:= 0;
vl_pto_materiais_w			number(15,4)	:= 0;
cd_edicao_amb_w			number(6)	:= 0;
nr_porte_anestesico_w		number(2)	:= 0;
ie_origem_proced_w		number(10);
cd_usuario_convenio_w		varchar2(40);
cd_plano_w			varchar2(20);
ie_clinica_w			number(15,0);
cd_empresa_ref_w			number(15,0);
ie_preco_informado_w		varchar2(01);
nr_seq_ajuste_proc_w		number(10,0);

cursor c01 is
select	a.cd_procedimento,
	a.qt_filme,
	a.nr_auxiliares,
	a.nr_incidencia,
	a.qt_porte_anestesico,
	a.ie_origem_proced
from	grupo_proc b,
	procedimento c,
	preco_amb a
where	a.cd_edicao_amb    	= cd_edicao_amb_p
and	a.cd_procedimento  	= c.cd_procedimento
and	a.ie_origem_proced	= c.ie_origem_proced
and	b.cd_grupo_proc    	= c.cd_grupo_proc
and	b.cd_especialidade 	= nvl(cd_especialidade_p,-1)
and	nvl(a.dt_inicio_vigencia, sysdate)	= (
			select 	max( nvl( x.dt_inicio_vigencia, sysdate) )
			from	preco_amb x
			where	x.cd_procedimento 	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced
			and	x.cd_edicao_amb    	= cd_edicao_amb_p
			and	nvl( x.dt_inicio_vigencia, sysdate)	<= nvl(dt_inicio_vigencia_p, nvl( x.dt_inicio_vigencia, sysdate) )
			)
union
select	a.cd_procedimento,
	a.qt_filme,
	a.nr_auxiliares,
	a.nr_incidencia,
	a.qt_porte_anestesico,
	a.ie_origem_proced
from	preco_amb a
where	a.cd_edicao_amb    	= cd_edicao_amb_p
and	nvl(cd_especialidade_p,-1) 	= -1
and	nvl( a.dt_inicio_vigencia, sysdate)	= (
			select 	max( nvl( x.dt_inicio_vigencia, sysdate) )
			from	preco_amb x
			where	x.cd_procedimento 	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced
			and	x.cd_edicao_amb    	= cd_edicao_amb_p
			and	nvl( x.dt_inicio_vigencia, sysdate)	<= nvl( dt_inicio_vigencia_p, nvl( x.dt_inicio_vigencia, sysdate) )
			);


begin

/* limpar tabela w_preco_amb */
begin
delete from w_preco_amb;
commit;
end;

if	nvl(cd_convenio_p,0) <> 0 and
	nvl(cd_categoria_p,'0') <> '0' then

	/* gerar tabela w_preco_amb a partir da preco_amb */

	open C01;
	loop
	fetch C01 into	
		cd_procedimento_w,
		qt_filme_w,
		nr_auxiliares_w,
		nr_incidencia_w,
		qt_porte_anestesico_w,
		ie_origem_proced_w;
	exit when C01%notfound;
		begin
		
		define_preco_procedimento
				(cd_estabelecimento_w,
				cd_convenio_p,
				cd_categoria_p,
				dt_atualizacao_w,
				cd_procedimento_w,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				cd_usuario_convenio_w,
				cd_plano_w,
				ie_clinica_w,
				cd_empresa_ref_w,null,
				vl_procedimento_w,
				vl_custo_operacional_w,
				vl_anestesista_w,
				vl_medico_w,
				vl_auxiliares_w,
				vl_filme_w,
				vl_pto_procedimento_w,
				vl_pto_custo_operac_w,
				vl_pto_anestesista_w,
				vl_pto_medico_w,
				vl_pto_auxiliares_w,
				vl_pto_materiais_w,
				nr_porte_anestesico_w,
				qt_pontos_w,
				cd_edicao_amb_w, ie_preco_informado_w, nr_seq_ajuste_proc_w,
				0, null, 0, null, null,
				null, null, null, null,
				null, null, null, null,
				null, null, null,null,null, null);

		insert into w_preco_amb	(
					cd_edicao_amb,
					cd_procedimento,
					vl_procedimento,
					vl_custo_operacional,
					vl_anestesista,
					vl_medico,
					vl_filme,
					qt_filme,
					nr_auxiliares,
					nr_incidencia,
					qt_porte_anestesico,
					ie_origem_proced,
					vl_auxiliares)
				values	(
					cd_edicao_amb_p,
					cd_procedimento_w,
					nvl(vl_procedimento_w,0),
					vl_custo_operacional_w,
					vl_anestesista_w,
					vl_medico_w,
					vl_filme_w,
					qt_filme_w,
					nr_auxiliares_w,
					nr_incidencia_w,
					qt_porte_anestesico_w,
					ie_origem_proced_w,
					vl_auxiliares_w);
		end;
	end loop;
	close C01;
	
	commit;
end if;

end gerar_w_preco_amb_rel;
/
