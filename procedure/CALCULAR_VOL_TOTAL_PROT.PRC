create or replace
procedure calcular_vol_total_prot(
			cd_protocolo_p		Number,
			nr_seq_solucao_p	Number,
			qt_volume_p			Number,
			nr_sequencia_p		Number) IS



Gotas_W					number(10,5);
Tempo_W 				number(10,5);
Resultado_W				number(10,1);
nr_etapas_ww 			number(10,0);
qt_hora_fase_w			number(15,4);
ie_calc_aut_w			varchar2(1) := 'N';
ie_esquema_Alternado_w  varchar2(1) := 'N';
ie_utiliza_disp_inf_w	varchar2(1);
ie_tipo_dosagem_w	    varchar2(10);
qt_tempo_aplicacao_w   	number(10,0);
qt_solucao_total_w	 	number(10,0);
qt_solucao_total_2w  	protocolo_medic_solucao.qt_solucao_total%type;

begin

select	nvl(max(ie_bomba_infusao),'N'),
		nvl(max(qt_hora_fase),0),
		nvl(max(nr_etapas),0),
		nvl(max(ie_calc_aut),'N'),
		nvl(max(ie_esquema_Alternado),'N'),
		nvl(max(ie_tipo_dosagem),'gtm'),
		nvl(max(qt_tempo_aplicacao),0),
		nvl(max(qt_solucao_total),0)
into	ie_utiliza_disp_inf_w,
		qt_hora_fase_w,
		nr_etapas_ww,
		ie_calc_aut_w,
		ie_esquema_Alternado_w,
		ie_tipo_dosagem_w,
		qt_tempo_aplicacao_w,
		qt_solucao_total_2w
from   	protocolo_medic_solucao
where  	cd_protocolo   = cd_protocolo_p
and		nr_seq_solucao = nr_seq_solucao_p
and		nr_sequencia   = nr_sequencia_p;

if     (upper(ie_tipo_dosagem_w) = 'GTM') then
    begin
    Gotas_w	:= 20;
    Tempo_w	:= 60;
    end;

elsif  (upper(ie_tipo_dosagem_w) = 'MLH') then
    begin
    Gotas_w	:= 1;
    Tempo_w	:= 1;
    end;

else
    begin
    Gotas_w	:= 60;
    Tempo_w	:= 60;
    end;
end if;

Resultado_w	:= 0;

if	(ie_calc_aut_w = 'S') then
	if	(ie_esquema_Alternado_w <> 'S') then
		qt_solucao_total_w	:= qt_volume_p * nr_etapas_ww;
	else
		qt_solucao_total_w	:= qt_volume_p;
	end if;
	
	Resultado_w  := Dividir(Dividir((Round(qt_solucao_total_w) * Gotas_w) ,
							qt_tempo_aplicacao_w) , Tempo_w);

	if	(resultado_w = 0) then
		resultado_w := null;
	end if;
	
	update	protocolo_medic_solucao
	set		qt_solucao_total	= qt_solucao_total_w,
			qt_volume		= qt_volume_p,
			qt_dosagem		= resultado_w
	where  	cd_protocolo = cd_protocolo_p
	and		nr_seq_solucao = nr_seq_solucao_p
	and		nr_sequencia = nr_sequencia_p;

else
	update	protocolo_medic_solucao
	set		qt_volume		= qt_volume_p
	where  	cd_protocolo = cd_protocolo_p
	and		nr_seq_solucao = nr_seq_solucao_p
	and		nr_sequencia = nr_sequencia_p;
end if;

end Calcular_Vol_Total_Prot;
/