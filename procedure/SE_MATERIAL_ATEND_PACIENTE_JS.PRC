create or replace 
procedure se_material_atend_paciente_js(
		cd_material_p			number,
		cd_local_estoque_p		number,
		cd_estabelecimento_p		number,
		qt_baixa_estoque_p		number,
		qt_baixa_consumo_p		number,
		cd_cgc_fornecedor_p		varchar2,
		nm_usuario_p			varchar2,
		cd_unidade_baixa_p		varchar2) is
		
permite_estoque_negativo_w	varchar2(1);
disp_estoque_w			varchar2(1);
ds_local_w			varchar2(255);
qt_material_w			number(13,4);

begin
select	max(EME_OBTER_DOSE_CONV(cd_unidade_baixa_p,cd_material_p,qt_baixa_consumo_p,cd_estabelecimento_p))
into	qt_material_w
from	dual;

obter_param_usuario(929, 21, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, permite_estoque_negativo_w);
obter_disp_estoque(cd_material_p,cd_local_estoque_p,cd_estabelecimento_p,qt_baixa_estoque_p,qt_material_w,cd_cgc_fornecedor_p,disp_estoque_w);

if	(permite_estoque_negativo_w = 'N') and
	(disp_estoque_w = 'N') then
	begin
		select	ds_local_estoque
		into 	ds_local_w
		from	local_estoque
		where	cd_local_estoque = cd_local_estoque_p;
	
		
	Wheb_mensagem_pck.exibir_mensagem_abort(67850,'CD_MATERIAL='|| cd_material_p || ';CD_LOCAL_ESTOQUE='|| cd_local_estoque_p || ';DS_LOCAL='|| ds_local_w);	
	end;
end if;
	
end se_material_atend_paciente_js;
/
