create or replace
procedure CIH_Copia_Def_Infecto(nr_prescricao_p		number,
				nr_seq_prescr_p		number,				
				nr_prescricao_novo_p	number,
				nr_seq_prescr_novo_p	number,
				nm_usuario_p		Varchar2) is 

			
nr_seq_def_infect_w		number(10);			
ie_copiar_def_infecto_w		varchar2(1);
			
Cursor C01 is
	select	nr_sequencia
	from	cih_def_infect
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_prescr = nr_seq_prescr_p;			
begin
begin

open C01;
loop
fetch C01 into	
	nr_seq_def_infect_w;
exit when C01%notfound;
	begin
	insert into cih_def_infect (	
		cd_material,
		cd_unidade_medida,
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_atendimento,
		cd_medicamento,							
		qt_dia_lib,
		qt_dose,
		cd_intervalo,							
		ie_via_aplicacao,
		ds_horarios,
		nr_seq_prescr,
		ds_observacao,
		ie_definicao,
		nr_prescricao,
		ie_gera_alerta,
		cd_topografia_cih,
		ie_origem_infeccao)
	select	cd_material,
		cd_unidade_medida,
		cih_def_infect_seq.NextVal,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_atendimento,
		cd_medicamento,
		qt_dia_lib,
		qt_dose,
		cd_intervalo,
		ie_via_aplicacao,
		ds_horarios,
		nr_seq_prescr_novo_p,
		ds_observacao,
		ie_definicao,
		nr_prescricao_novo_p,
		ie_gera_alerta,
		cd_topografia_cih,
		ie_origem_infeccao
	from	cih_def_infect
	where	nr_sequencia = nr_seq_def_infect_w;
	end;
end loop;
close C01;
exception
	when others then
	nr_seq_def_infect_w := 0;
end;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end CIH_Copia_Def_Infecto;
/
