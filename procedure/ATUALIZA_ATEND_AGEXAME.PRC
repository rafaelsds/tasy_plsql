CREATE OR REPLACE
PROCEDURE atualiza_atend_agexame (nr_atendimento_p		NUMBER,
				  ds_lista_agenda_cons_p	VARCHAR2,
				  nm_usuario_p			varchar2) IS

nr_sequencia_w			NUMBER(10,0);
nr_atendimento_w		NUMBER(10,0);
ds_lista_w				VARCHAR2(1000);
tam_lista_w				NUMBER(10,0);
ie_pos_virgula_w		NUMBER(3,0);
ie_gerar_oft_ag_ex_w	varchar2(15);
ie_gerar_oft_eup_w	varchar2(15);
nr_seq_oftalmo_w		agenda_paciente.nr_seq_oftalmo%type;
cd_medico_exec_w		agenda_paciente.cd_medico_exec%type;	

		

begin

obter_param_usuario(820, 369, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gerar_oft_ag_ex_w);
obter_param_usuario(916, 1091, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gerar_oft_eup_w);


ds_lista_w := ds_lista_agenda_cons_p;	

if	(substr(ds_lista_w,length(ds_lista_w) - 1, length(ds_lista_w))	<> ',') then
	ds_lista_w	:= ds_lista_w ||',';
end if;

while (ds_lista_w is not null) loop 
	begin
	
	tam_lista_w		:= length(ds_lista_w);
	ie_pos_virgula_w	:= instr(ds_lista_w,',');
	
	if	(ie_pos_virgula_w <> 0) then
		nr_sequencia_w		:= to_number(substr(ds_lista_w,1,(ie_pos_virgula_w - 1)));
		ds_lista_w		:= substr(ds_lista_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;
	
	update	agenda_paciente
	set		nr_atendimento 	= nr_atendimento_p,
				dt_atualizacao		= sysdate,
				nm_usuario			= nm_usuario_p
	where		nr_sequencia		= nr_sequencia_w;
	
	select	max(nr_seq_oftalmo),
				max(cd_medico_exec)
	into		nr_seq_oftalmo_w,
				cd_medico_exec_w
	from		agenda_paciente
	where		nr_sequencia = nr_sequencia_w;
	
	if	(ie_gerar_oft_eup_w = 'N') and
		(ie_gerar_oft_ag_ex_w = 'A') and
		(nvl(nr_sequencia_w,0) > 0) and
		(nvl(nr_atendimento_p,0) > 0) and
		(nr_seq_oftalmo_w is null) and
		(cd_medico_exec_w is not null) then 
		gerar_consulta_oft_agenda(null,nr_sequencia_w,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento);
	end if;	
		
	end;
end loop;

commit;

end atualiza_atend_agexame;
/