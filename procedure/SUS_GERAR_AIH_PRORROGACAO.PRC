create or replace
procedure SUS_Gerar_AIH_Prorrogacao(	nr_aih_p	number,
					nr_sequencia_p	number,
					nm_usuario_p	varchar2) is

nr_seq_aih_w	number(10);

begin

select	nvl(max(nr_sequencia),0) + 1
into	nr_seq_aih_w
from	sus_aih
where	nr_aih = nr_aih_p
  and	nr_sequencia = nr_sequencia_p;

insert into sus_aih (	NR_AIH,	NR_ATENDIMENTO,	DT_EMISSAO,
			CD_ORGAO_EMISSOR, IE_IDENTIFICACAO, CD_MEDICO_RESPONSAVEL,
			CD_PROCEDIMENTO_SOLIC, IE_ORIGEM_PROCED, DT_ATUALIZACAO,
			NM_USUARIO, NR_PROTOCOLO, VL_TOTAL_AIH,
			CD_MOTIVO_COBRANCA, IE_ESPECIALIDADE_AIH, QT_DIA_UTI_MES_INI,
			QT_DIA_UTI_MES_ANT, QT_DIA_UTI_MES_ALTA, QT_NASCIDOS_VIVOS,
			QT_NASCIDOS_MORTOS, NR_ANTERIOR_AIH, CD_CID_NOTIFICACAO,
			IE_GESTACAO_RISCO, IE_INFECCAO_HOSPITALAR, QT_DIA_ACOMPANHANTE,
			DT_AUTORIZACAO_ACOMP, QT_NASC_SAI_ALTA, QT_NASC_SAI_TRANS,
			QT_NASC_SAI_OBITO, VL_PONTO_SP, CD_MEDICO_GESTOR,
			DT_AUTORIZACAO_GESTOR, IE_AUTORIZACAO_GESTOR, QT_PERMANENCIA_SUS,
			QT_PERMANENCIA_REAL, QT_LONGA_PERMANENCIA, NR_GESTANTE_PRE_NATAL,
			NR_PROXIMA_AIH, QT_FILHO, IE_GRAU_INSTRUCAO,
			IE_CONTRACEPTIVO_UM, IE_CONTRACEPTIVO_DOIS, IE_MOTIVO_ALTA_UTI_NEO,
			QT_PESO_GRAMA, QT_MES_GESTACAO, NR_INTERNO_CONTA,
			CD_MOTIVO_ALTA, NR_CAT, DT_INICIAL,
			DT_FINAL, IE_CLINICA, IE_CARATER_INTER_SUS,
			CD_CID_PRINCIPAL, CD_CID_SECUNDARIO, NR_SEQUENCIA, NR_SEQ_FAEC,
			QT_PONTO_SP, VL_TOT_SP)
select 			NR_AIH,	A.NR_ATENDIMENTO,	DT_EMISSAO,
			CD_ORGAO_EMISSOR, 5, CD_MEDICO_RESPONSAVEL,
			CD_PROCEDIMENTO_SOLIC, IE_ORIGEM_PROCED, sysdate,
			nm_usuario_p, NR_PROTOCOLO, VL_TOTAL_AIH,
			CD_MOTIVO_COBRANCA, IE_ESPECIALIDADE_AIH, QT_DIA_UTI_MES_INI,
			QT_DIA_UTI_MES_ANT, QT_DIA_UTI_MES_ALTA, QT_NASCIDOS_VIVOS,
			QT_NASCIDOS_MORTOS, NR_AIH, CD_CID_NOTIFICACAO,
			IE_GESTACAO_RISCO, IE_INFECCAO_HOSPITALAR, QT_DIA_ACOMPANHANTE,
			DT_AUTORIZACAO_ACOMP, QT_NASC_SAI_ALTA, QT_NASC_SAI_TRANS,
			QT_NASC_SAI_OBITO, VL_PONTO_SP, CD_MEDICO_GESTOR,
			DT_AUTORIZACAO_GESTOR, IE_AUTORIZACAO_GESTOR, QT_PERMANENCIA_SUS,
			QT_PERMANENCIA_REAL, QT_LONGA_PERMANENCIA, A.NR_GESTANTE_PRE_NATAL,
			NR_PROXIMA_AIH, QT_FILHO, IE_GRAU_INSTRUCAO,
			IE_CONTRACEPTIVO_UM, IE_CONTRACEPTIVO_DOIS, IE_MOTIVO_ALTA_UTI_NEO,
			QT_PESO_GRAMA, QT_MES_GESTACAO, null,
			A.CD_MOTIVO_ALTA, A.NR_CAT, add_months(trunc(dt_inicial,'month'),1),
			nvl(trunc(b.dt_alta),last_day(add_months(dt_final,1))), A.IE_CLINICA, A.IE_CARATER_INTER_SUS,
			CD_CID_PRINCIPAL, CD_CID_SECUNDARIO, nr_seq_aih_w, NR_SEQ_FAEC, QT_PONTO_SP, VL_TOT_SP      
from	atendimento_paciente	b,
	sus_aih			a
where	a.nr_atendimento	= b.nr_atendimento
and	nr_aih		= nr_aih_p
and 	nr_sequencia	= nr_sequencia_p;

commit;

end;
/
