create or replace PROCEDURE obter_info_aval_ant(
  cd_perfil_p           NUMBER,
  nr_atendimento_p      NUMBER,
  cd_pessoa_fisica_p    usuario.cd_pessoa_fisica%type,
  nm_usuario_p          VARCHAR2,
  nr_seq_prescr_atual_p NUMBER,
  nr_seq_prescr_p   OUT NUMBER,
  nr_seq_modelo_p   OUT NUMBER)
IS

  type_of_care_user_w    VARCHAR2(1);
  type_evol_prescricao_w VARCHAR2(1);
  type_evol_user_w       usuario.ie_tipo_evolucao%type;
  qt_itens_prescr_atual_w NUMBER;

  CURSOR C01 IS
    SELECT b.nr_sequencia, c.ie_tipo_evolucao, b.nr_seq_modelo
    FROM pe_prescricao b, usuario c
    WHERE c.nm_usuario = b.nm_usuario
    AND b.dt_liberacao IS NOT NULL
    AND b.dt_suspensao IS NULL
    AND (
          (type_of_care_user_w = 'T' AND (b.cd_pessoa_fisica = cd_pessoa_fisica_p AND b.ie_nivel_atencao = 'T'))
          OR
          (type_of_care_user_w = 'S' AND (b.cd_pessoa_fisica = cd_pessoa_fisica_p AND b.ie_nivel_atencao = 'S')) 
        );

BEGIN
  nr_seq_prescr_p := 0; 
  nr_seq_modelo_p := 0;

  SELECT MAX(ie_tipo_evolucao)
  INTO type_evol_user_w
  FROM usuario
  WHERE nm_usuario = nm_usuario_p;

  SELECT NVL(MAX(a.ie_nivel_atencao),'T')
  INTO type_of_care_user_w
  FROM perfil a
  WHERE a.cd_perfil = cd_perfil_p;

  OPEN C01;
  FETCH C01
  INTO nr_seq_prescr_p,
    type_evol_prescricao_w,
    nr_seq_modelo_p;
  CLOSE C01;

  IF (type_evol_prescricao_w <> type_evol_user_w) THEN
    nr_seq_prescr_p := 0;
  END IF;

END obter_info_aval_ant;
/ 