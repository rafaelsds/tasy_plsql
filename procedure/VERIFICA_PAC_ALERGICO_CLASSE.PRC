create or replace
procedure Verifica_Pac_Alergico_classe (	cd_pessoa_fisica_p		varchar2,
										cd_material_p			Number,
										ds_item_p			out	varchar2,
										ie_retorno_p		out	varchar2 ) is

ie_pac_alergia_w		Number(10) := 0;
cd_classe_mat_w			Number(10);
qt_alergia_w			Number(10);
cd_classe_material_w		Number(5);
cont_w				number(10);

Cursor C01 is
	select	obter_dados_material(CD_MATERIAL,'CCLA')
	from	paciente_alergia
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	dt_fim		is null
	and	CD_MATERIAL	is not null
	and	dt_inativacao	is null
	union all
	select 	obter_dados_material(m.cd_material,'CCLA')
	from	paciente_alergia a,
		medic_ficha_tecnica f,
		material m
	where	a.nr_seq_ficha_tecnica = f.nr_sequencia
	and	f.nr_sequencia = m.nr_seq_ficha_tecnica
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	a.nr_seq_ficha_tecnica	is not null
	and	f.nr_seq_superior 	is null
	and	a.dt_fim	is null	
	and	a.cd_material	is null
	and	a.dt_inativacao	is null
	and 	pkg_i18n.get_user_locale() = 'en_AU'
	group by obter_dados_material(m.cd_material,'CCLA')
	union all
	select 	obter_dados_material(m.cd_material,'CCLA')
	from	paciente_alergia a,
		medic_ficha_tecnica f,
		material m
	where	a.nr_seq_ficha_tecnica = f.nr_seq_superior
	and   	f.nr_sequencia = m.nr_seq_ficha_tecnica
	and   	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	a.nr_seq_ficha_tecnica	is not null
	and	a.dt_fim	is null	
	and	a.cd_material	is null
	and	a.dt_inativacao	is null
	and	pkg_i18n.get_user_locale() = 'en_AU'
	group by obter_dados_material(m.cd_material,'CCLA');

Cursor C02 is
	select	CD_CLASSE_MATERIAL
	from	material
	where	cd_material in 	(select	cd_material_p
				from	dual
				union
				select	b.cd_material_generico
				from	material b
				where	b.cd_material = cd_material_p
				union
				select	b.cd_material
				from	material b
				where	b.cd_material_generico = cd_material_p);

begin

ie_retorno_p	:= 'N';
ds_item_p	:= '';

select	count(*)
into	qt_alergia_w
from	paciente_alergia
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	(CD_MATERIAL	is not null or (pkg_i18n.get_user_locale() = 'en_AU' and nr_seq_ficha_tecnica is not null))
and	dt_fim		is null;

if	(qt_alergia_w > 0) then
	
	open c01;
	loop
	fetch c01 into	
		cd_classe_mat_w;
	exit when c01%notfound;
	
		if	(cd_classe_mat_w > 0) then
			select	cd_classe_material
			into	cd_classe_material_w
			from	estrutura_material_v
			where	cd_material	= cd_material_p;

			if	(cd_classe_material_w = cd_classe_mat_w) then
				ds_item_p	:= substr(Obter_desc_classe_mat(cd_classe_material_w),1,35);
				ie_retorno_p	:= 'S';
				exit;
			end if;
		end if;
		
		open c02;
		loop
		fetch c02 into	
			cd_classe_material_w;
		exit when c02%notfound;
			if	(cd_classe_material_w = cd_classe_mat_w) then
				ds_item_p	:= substr(Obter_desc_classe_mat(cd_classe_material_w),1,35);
				ie_retorno_p	:= 'S';
				exit;
			end if;		
		end loop;
		close c02;

		if	(ie_retorno_p = 'S') then
			exit;
		end if;

	end loop;
	close c01;
	
end if;

end Verifica_Pac_Alergico_classe;
/