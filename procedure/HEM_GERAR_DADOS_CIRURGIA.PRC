create or replace
procedure hem_gerar_dados_cirurgia(	nr_cirurgia_p	Number,	
					nm_usuario_p		Varchar2) is 

Cursor C01 is
	select	b.nr_seq_proc_interno -- PROCEDIMENTOS DA CIRURGIA QUE EST�O CADASTRADOS NO DIS
	from	cirurgia a,
		hem_proc_interno b,
		hem_procedimento c
	where	a.nr_cirurgia		= nr_cirurgia_p
	and b.nr_seq_proc = c.nr_sequencia
	and	a.nr_seq_proc_interno	= b.nr_seq_proc_interno
	and	a.dt_fim_cirurgia is null
	and	nvl(c.ie_situacao,'A') = 'A'
	union
	select	b.nr_seq_proc_interno -- PROCEDIMENTOS ADICIONAIS DA CIRURGIA QUE EST�O CADASTRADOS NO DIS
	from	cirurgia a,
		hem_proc_interno b,
		hem_procedimento c,
		prescr_procedimento d
	where	a.nr_cirurgia		= nr_cirurgia_p
	and b.nr_seq_proc = c.nr_sequencia
	and	a.nr_prescricao		= d.nr_prescricao
	and	d.nr_seq_proc_interno	= b.nr_seq_proc_interno
	and	nvl(c.ie_situacao,'A') = 'A'
	and	a.dt_fim_cirurgia is null;
					
nr_seq_proc_w		Number(10);
nr_seq_proc_interno_w	Number(10);

begin
if	(nvl(nr_cirurgia_p,0) > 0) then
	begin
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_proc_interno_w;
	exit when C01%notfound;
		begin
			
		hem_inserir_procedimento_pepo(nr_cirurgia_p, nr_seq_proc_interno_w, nm_usuario_p, nr_seq_proc_w);
		
		if (nr_seq_proc_w is not null) then
		
			insert into hem_proc_partic (
				nr_sequencia, 
				nr_seq_proc, 
				dt_atualizacao, 
				nm_usuario, 
				cd_pessoa_fisica, 
				cd_funcao, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec
			)(	select	hem_proc_partic_seq.nextval,
					nr_seq_proc,
					sysdate,
					nm_usuario_p,
					cd_pessoa_fisica,
					ie_funcao,
					sysdate,
					nm_usuario_p
				from	(
					select	a.nr_sequencia nr_seq_proc,
						b.cd_pessoa_fisica,
						b.ie_funcao
					from	hem_proc a,
						cirurgia_participante b
					where	a.nr_seq_cirurgia	= b.nr_cirurgia
					and	a.nr_seq_cirurgia	= nr_cirurgia_p
					and	a.nr_sequencia		= nr_seq_proc_w
					union
					select	a.nr_sequencia nr_seq_proc,
						b.cd_medico_cirurgiao,
						'' --ie_funcao
					from	hem_proc a,
						cirurgia b
					where	a.nr_seq_cirurgia	= b.nr_cirurgia
					and	a.nr_seq_cirurgia	= nr_cirurgia_p
					and	a.nr_sequencia		= nr_seq_proc_w
					and not exists (select 1
								   from cirurgia_participante c
								   where a.nr_seq_cirurgia = c.nr_cirurgia
								   and	 b.cd_medico_cirurgiao = c.cd_pessoa_fisica))
			);
		end if;
		end;
	end loop;
	close C01;
	end;
end if;
commit;

end hem_gerar_dados_cirurgia;
/
