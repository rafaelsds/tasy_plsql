create or replace
procedure bsc_calcular_indicador(	nr_seq_indicador_p	number,
					nr_seq_ind_inf_p	number,
					qt_real_p	out	number,
					nm_usuario_p		Varchar2) is 

nr_seq_regra_w			number(10);
nm_objeto_w			varchar2(30);


ie_origem_real_w			varchar2(15);
qt_real_w				number(15,2);		

/* Regras de calculo do indicador */
cursor c01 is
select	b.nr_sequencia,
	b.ie_origem_real 
from	bsc_regra_calc a,
	bsc_regra_calc_ind b
where	a.nr_sequencia		= b.nr_seq_regra
and	sysdate between a.dt_inicio_validade and nvl(a.dt_validade,sysdate)
and	b.nr_seq_indicador	= nr_seq_indicador_p
order by b.nr_seq_calc;

BEGIN

open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	ie_origem_real_w;
exit when C01%notfound;
	begin
	if	(ie_origem_real_w = 'F') then
		bsc_calcular_formula_ind(nr_seq_indicador_p, nr_seq_ind_inf_p,nr_seq_regra_w, qt_real_w);
	elsif	(ie_origem_real_w in ('S','P')) then
		bsc_calcular_sql(nr_seq_indicador_p, nr_seq_ind_inf_p, nr_seq_regra_w, qt_real_w);
	elsif	(ie_origem_real_w = 'CTB') then
		bsc_calcular_ctb(nr_seq_indicador_p, nr_seq_ind_inf_p, nr_seq_regra_w, qt_real_w);
	elsif	(ie_origem_real_w = 'CTBE') then
		bsc_calcular_ctb_especial(nr_seq_indicador_p, nr_seq_ind_inf_p, nr_seq_regra_w, qt_real_w);
	end if;
	
	end;
end loop;
close C01;

qt_real_p	:= nvl(qt_real_w,0);

end bsc_calcular_indicador;
/