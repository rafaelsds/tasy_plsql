create or replace
procedure Gerar_EIS_Reinternacao_UTI_ANS(	
	dt_parametro_p		date,
	nm_usuario_p		varchar2) IS 
	
nr_sequencia_w			number(10,0);
dt_parametro_w			date;
dt_parametro_fim_w		date;
nr_atendimento_w		number(10);
dt_saida_old_w			date;
dt_entrada_unidade_w	date;
dt_saida_unidade_w		date;
qt_reg_w			 	number(10);

Cursor c01 is	
	select  distinct z.nr_atendimento
	from	pessoa_fisica p,			
			atendimento_paciente t,
			atend_paciente_unidade z,
			setor_atendimento y
	where	t.cd_pessoa_fisica		= p.cd_pessoa_fisica	
	and		t.nr_atendimento		= z.nr_atendimento
	and     z.cd_setor_atendimento  = y.cd_setor_atendimento
	and     z.ie_passagem_setor 	= 'N'
	and     y.cd_classif_setor 		= 4
	and     z.dt_entrada_unidade between dt_parametro_w and dt_parametro_fim_w
	and	exists
		(select 1
		 from    setor_atendimento b,
				 atend_paciente_unidade a
		 where   a.nr_atendimento        	= t.nr_atendimento
		 and  	 a.cd_setor_atendimento  	= b.cd_setor_atendimento
		 and	 a.ie_passagem_setor 		= 'N'
		 and   	 b.cd_classif_setor 		= 4
		 and   	 a.dt_entrada_unidade <> z.dt_entrada_unidade
		 and   	 a.dt_saida_unidade between z.dt_entrada_unidade - 1 and z.dt_entrada_unidade);		
	
Cursor c02 is
	select	dt_entrada_unidade,
			dt_saida_unidade
    from	setor_atendimento b,
			atend_paciente_unidade a
    where	a.nr_atendimento = nr_atendimento_w
	and     (dt_saida_unidade between dt_parametro_w - 1 and dt_parametro_fim_w
	or       dt_entrada_unidade between dt_parametro_w - 1 and dt_parametro_fim_w)
	and		a.ie_passagem_setor = 'N'
    and		a.cd_setor_atendimento  = b.cd_setor_atendimento
	and     b.cd_classif_setor = 4
	order by dt_entrada_unidade asc;

begin

Gravar_Log_Indicador(806, 'Reinternação UTI ANS', sysdate, trunc(dt_parametro_p), nm_usuario_p, nr_sequencia_w);

dt_parametro_w		:=  trunc(dt_parametro_p,'month');
dt_parametro_fim_w	:= last_day(Trunc(dt_parametro_p,'month')) + 86399/86400;

delete from eis_reinternacao_uti_ans
where dt_referencia	between dt_parametro_w and dt_parametro_fim_w;
commit;

open c01;
loop
fetch c01 into 
	nr_atendimento_w;
	exit when c01%NOTFOUND;
	begin
		dt_saida_old_w := null;
		open c02;
		loop
		fetch c02 into
			dt_entrada_unidade_w,
			dt_saida_unidade_w;
			exit when c02%NOTFOUND;
			begin
				if	(dt_saida_old_w	is not null) and
					(dt_saida_old_w > (dt_entrada_unidade_w - 1) ) then
					
					
					select	count(*)
					into	qt_reg_w
					from	atend_paciente_unidade a,
							setor_atendimento b
					where	a.nr_atendimento	= nr_atendimento_w
					and		a.cd_setor_atendimento	= b.cd_setor_atendimento
					and		a.ie_passagem_setor = 'N'
					and		b.cd_classif_setor	= '3'
					and		a.DT_ENTRADA_UNIDADE between dt_saida_old_w and dt_entrada_unidade_w;
					
					if	(qt_reg_w	> 0) then

						insert into EIS_REINTERNACAO_UTI_ANS(	nr_sequencia,
											dt_referencia,
											nm_usuario,
											dt_atualizacao,
											nm_usuario_nrec,
											dt_atualizacao_nrec,
											nr_atendimento)
								values		(	EIS_REINTERNACAO_UTI_ANS_seq.nextval,
											dt_entrada_unidade_w,
											nm_usuario_p,
											sysdate,
											nm_usuario_p,
											sysdate,
											nr_atendimento_w);
					end if;
					
				end if;
				dt_saida_old_w	:= dt_saida_unidade_w;
			end;
		end loop;
		close c02;
	end;
end loop;
close c01;

Atualizar_Log_Indicador(sysdate, nr_sequencia_w);

end Gerar_EIS_Reinternacao_UTI_ANS;
/
