create or replace
procedure descartar_diagnostico_pend(	nr_sequencia_p		varchar2,
										nm_usuario_p		varchar2,
										ds_justificativa_p	varchar2) is

nr_sequencia_w number;

cursor c01 is
	select regexp_substr(nr_sequencia_p,'[^,]+', 1, level) 
	from dual 
	connect BY regexp_substr(nr_sequencia_p, '[^,]+', 1, level) is not null;
	
begin


open c01;
loop
fetch c01 into 
	nr_sequencia_w;
exit when c01%notfound;
begin
	update 	ATEND_DIAGNOSTICO_PENDENTE
	set		ie_situacao = 'I',
			dt_inativacao = sysdate,
			nm_usuario_inativacao = nm_usuario_p,
			ds_justificativa = ds_justificativa_p
	where 	nr_sequencia = nr_sequencia_w;
end;
end loop;
close c01;

commit;

end descartar_diagnostico_pend;
/
