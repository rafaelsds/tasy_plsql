create or replace PROCEDURE INSERT_CODIGO_CONTROL_HIST(
    nm_usuario_p        varchar2,
    nr_autorizacao_p    number,
    nr_factura_p        number,
    dt_emision_p        date,
    nr_nit_p            number default 0,
    nr_monto_p          number,
    cd_llave_p          varchar2,
    cd_codigo_control_p varchar2,
    nr_item_p           number default null,
    cd_medico_p         varchar2 default null
) AS 

dt_sysdate_s    date := sysdate;

BEGIN
    if(nr_autorizacao_p is not null and nr_factura_p is not null and dt_emision_p is not null
        and nr_monto_p is not null and cd_llave_p is not null and cd_codigo_control_p is not null) then

        insert into GERAR_CODIGO_CONTROL_HIST (
            nr_sequencia,
            dt_atualizacao,
            dt_atualizacao_nrec,
            nm_usuario,
            nm_usuario_nrec,
            nr_autorizacao,
            nr_factura,
            dt_emision,
            nr_nit,
            nr_monto,
            cd_llave,
            cd_codigo_control,
            cd_medico
        ) values (
            GERAR_CODIGO_CONTROL_HIST_SEQ.nextval,
            dt_sysdate_s,
            dt_sysdate_s,
            nvl(nm_usuario_p, wheb_usuario_pck.get_nm_usuario),
            nvl(nm_usuario_p, wheb_usuario_pck.get_nm_usuario),
            nr_autorizacao_p,
            nr_factura_p,
            dt_emision_p,
            nvl(nr_nit_p, 0),
            nr_monto_p,
            cd_llave_p,
            cd_codigo_control_p,
            cd_medico_p
        );

        if(nr_item_p is not null) then
            update  nota_fiscal
            set     cd_control_code = cd_codigo_control_p,
                    nr_autorizacao = (
                        select  max(nr_autorizacao) 
                        from    autorizacao_numero_hist 
                        where   ie_situacao = 'A' 
                        and     dt_sysdate_s between dt_inicial and dt_fim)
            where   nr_sequencia = nr_item_p;
        end if;

        if(cd_medico_p is not null) then
            update  third_party_invoice
            set     cd_control_code = cd_codigo_control_p,
                    dt_emissao  = dt_emision_p
            where   nr_fatura   = nr_factura_p; 
        end if;

        commit;
    end if;
END INSERT_CODIGO_CONTROL_HIST;
/
