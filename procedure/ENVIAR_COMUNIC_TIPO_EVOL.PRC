create or replace
procedure enviar_comunic_tipo_evol( 	cd_evolucao_p		number,
					nm_usuario_p		Varchar2) is 

ds_evolucao_w 			varchar2(32000) := '';
ds_evolucao_ww			varchar2(32000) := '';
ds_pos_inicio_rtf_w		number(10,0);
nm_usuario_w			varchar2(15);
nm_destino_w			varchar2(4000) := '';
nm_usuario_cad_w		varchar2(15) := ''; 
nr_sequencia_w			number(10,0);
nr_atendimento_w		number(10,0);
nm_paciente_w			varchar2(300);
nm_medico_w			varchar2(100) := 0;
cd_pessoa_fisica_w		varchar2(10);
nr_seq_equipe_w			number(10);
ds_seq_equipe_w			varchar2(255);
ds_leito_w			varchar2(255);
ds_setor_w			varchar2(255);
ie_equipe_w			varchar2(10);
cd_setor_w			number(10);
cd_setor_destino_w		varchar2(4000);
cd_perfil_w			number(10);
cd_perfil_destino_w		varchar2(4000);
nm_usuario_comunic_w		varchar2(15);
nm_usuario_destino_w		varchar2(4000);
nr_seq_grupo_w			number(10);
nr_seq_grupo_dest_w		varchar2(4000);
cd_tipo_evolucao_w		varchar2(3);
qt_reg_w			number(10);	
ds_tipo_evolucao_w	varchar2(255);
nr_seq_classif_w	number(10);
visualiza_desc_evol_w		char(1);	
nr_prontuario_w			number(10);
estabelecimento_w		number(4);
estabelecimento_dest_w		number(4);
	
Cursor C01 is
	select	cd_setor_atendimento,
		cd_perfil,
		nm_usuario_comunic,
		nr_seq_grupo,
		cd_estabelecimento
	from	tipo_evolucao_comunic
	where	cd_tipo_evolucao = cd_tipo_evolucao_w
	and	nvl(cd_estabelecimento,obter_estabelecimento_ativo) = obter_estabelecimento_ativo;
	
begin

select	nr_atendimento,
	obter_nome_pf(a.cd_pessoa_fisica),
	a.cd_pessoa_fisica,
	SUBSTR(Obter_Unidade_Atendimento(nr_atendimento,'A','S'),1,120),
	substr(Obter_Unidade_Atendimento(nr_atendimento,'A','UB')||Obter_Unidade_Atendimento(nr_atendimento,'A','UC'),1,80),
	ie_evolucao_clinica,
	nr_prontuario
into	nr_atendimento_w,
	nm_paciente_w,
	cd_pessoa_fisica_w,
	ds_setor_w,
	ds_leito_w,
	cd_tipo_evolucao_w,
	nr_prontuario_w
from    evolucao_paciente a,
        pessoa_fisica b
where   cd_evolucao = cd_evolucao_p
and   	a.cd_pessoa_fisica = b.cd_pessoa_fisica;

select	count(*)
into	qt_reg_w
from	tipo_evolucao_comunic
where	cd_tipo_evolucao = cd_tipo_evolucao_w
and		nvl(cd_estabelecimento,obter_estabelecimento_ativo) = obter_estabelecimento_ativo;



if (cd_evolucao_p is not null) and
	(qt_reg_w	> 0)then

	select	comunic_interna_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	select	obter_nome_medico(cd_medico,'N'),
		ds_evolucao,
		substr(obter_usuario_pf(cd_medico),1,100)
	into	nm_medico_w,
		ds_evolucao_w,
		nm_usuario_cad_w
	from	evolucao_paciente
	where	cd_evolucao = cd_evolucao_p;	
	
	select	max(ds_tipo_evolucao),
		max(NR_SEQ_CLASSIF),
		nvl(max(IE_TEXTO_COMUNIC),'S')
	into	ds_tipo_evolucao_w,
		nr_seq_classif_w,
		visualiza_desc_evol_w
	from	tipo_evolucao
	where	cd_tipo_evolucao = cd_tipo_evolucao_w;
	
	
	if	(ds_evolucao_w is not null) then
			/*Pega o cabecalho do RTF*/
			ds_pos_inicio_rtf_w := instr(ds_evolucao_w,'\lang')+9;
			ds_evolucao_ww 	:= substr(ds_evolucao_w,1,ds_pos_inicio_rtf_w) || 'fs20 ';
			
			
			if	(nr_atendimento_w	is not null) then
				ds_evolucao_ww := ds_evolucao_ww ||
				WHEB_MENSAGEM_PCK.get_texto(457601,
				'nr_atendimento_w=' || nr_atendimento_w || ' \par ' ||
				';nr_prontuario_w=' || nr_prontuario_w || ' \par ' ||
				';nm_paciente_w=' || nm_paciente_w || ' \par ');
				/*'Evolu��o liberada para o atendimento: ' || nr_atendimento_w || ' \par ' ||
				'Prontu�rio: '||nr_prontuario_w||' \par ' ||
				'Paciente: ' || nm_paciente_w || ' \par ';*/
			else
				ds_evolucao_ww := ds_evolucao_ww ||
				OBTER_DESC_EXPRESSAO(728912) ||nm_paciente_w|| ' \par '; 			-- 728912: 'Evolu��o liberada para o paciente: '
			end if;
			
			ds_evolucao_ww	:= ds_evolucao_ww ||OBTER_DESC_EXPRESSAO(329821) || ' ' ||ds_setor_w || ' \par ';															-- 329821: 'Setor:'
			ds_evolucao_ww	:= ds_evolucao_ww ||OBTER_DESC_EXPRESSAO(728916) || ' ' ||ds_leito_w || ' \par ';															-- 728916: 'Leito\Compl:'
			
			ds_evolucao_ww	:= ds_evolucao_ww ||OBTER_DESC_EXPRESSAO(728918) || ' ' || nm_medico_w || ' \par ';															-- 728918: 'Profissional:'
			ds_evolucao_ww	:= ds_evolucao_ww ||OBTER_DESC_EXPRESSAO(728932) || ' ' || ds_tipo_evolucao_w || ' \par ';													-- 728932: 'Tipo evolu��o:'
	
			if (visualiza_desc_evol_w = 'S') then
			ds_evolucao_ww := ds_evolucao_ww || OBTER_DESC_EXPRESSAO(728940) || ' \par \par '|| substr(ds_evolucao_w,ds_pos_inicio_rtf_w,length(ds_evolucao_w));		-- 728940: 'Evolu��o:'
			else
			ds_evolucao_ww := ds_evolucao_ww || ' \par \par '|| wheb_rtf_pck.GET_RODAPE;
			end if;
	end if;
	
	if (nm_usuario_cad_w is not null) then
		nm_destino_w := nm_usuario_cad_w || ',';
	end if;
		
	/*obter_param_usuario(281,733,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_equipe_w);*/
	open C01;
	loop
	fetch C01 into
		cd_setor_w,
		cd_perfil_w,
		nm_usuario_comunic_w,
		nr_seq_grupo_w,
		estabelecimento_w;
	exit when C01%notfound;
		begin
			if	(cd_setor_w is not null) then
				cd_setor_destino_w 	:= cd_setor_w||','||cd_setor_destino_w;
			end if;
			if	(cd_perfil_w is not null) then
				cd_perfil_destino_w 	:= cd_perfil_w||','||cd_perfil_destino_w ;						
			end if;
			if	(nm_usuario_comunic_w is not null) then
				nm_usuario_destino_w 	:= nm_usuario_comunic_w|| ',' || nm_usuario_destino_w ;						
			end if;
			if	(nr_seq_grupo_w is not null) then
				nr_seq_grupo_dest_w 	:= nr_seq_grupo_w|| ','||nr_seq_grupo_dest_w ;
			end if;
			if	(estabelecimento_w is not null) then
				estabelecimento_dest_w 	:= estabelecimento_w;
			end if;
			
			
		end;
	end loop;
	close C01;
	

	insert	into comunic_interna
			(cd_estab_destino,
			nr_sequencia,
			ds_titulo,
			dt_comunicado,
			ds_comunicado,
			nm_usuario,
			nm_usuario_destino,
			dt_atualizacao,
			ie_geral,
			ie_gerencial,
			ds_perfil_adicional,
			dt_liberacao,
			ds_setor_adicional,
			ds_grupo,
			nr_seq_classif)
	values	(
			nvl(estabelecimento_dest_w,wheb_usuario_pck.get_cd_estabelecimento),
			nr_sequencia_w,
			OBTER_DESC_EXPRESSAO(728946) || ' ' ||nm_paciente_w,												-- 728946: 'Evolu��o do paciente:
			sysdate,
			ds_evolucao_ww,
			nm_usuario_p,
			nm_usuario_destino_w,
			sysdate,
			'N',
			'N',
			cd_perfil_destino_w,
			sysdate,
			cd_setor_destino_w,
			nr_seq_grupo_dest_w,
			nr_seq_classif_w);


end if;	

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end enviar_comunic_tipo_evol;
/