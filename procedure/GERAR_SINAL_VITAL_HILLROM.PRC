create or replace
procedure gerar_sinal_vital_HillRom(	cd_pessoa_fisica_p	varchar2,
					dt_sinal_vital_p	varchar2,
					ds_lista_parametros_p	varchar2,
					ds_lista_parametros2_p	varchar2,
					nm_usuario_p		Varchar2) is



type campos is record (		nm_sinal_vital	varchar2(100),
				vl_sinal_vital	varchar2(100),
				ds_unidade_medida	varchar2(100),
				ds_lista		varchar2(255));
type Vetor is table of campos index 	by binary_integer;
Vetor_w			Vetor;



nr_atendimento_w	number(10);
dt_sinal_vital_w	date;
nr_seq_sinal_vital_w	number(10)	:= 0;
nr_seq_atend_info_leito_w number(10) := 0;
ds_sep_w		varchar2(100)	:= ';';
nr_pos_separador_w	NUMBER(15);
qt_parametros_w		NUMBER(15);
qt_contador_w		number(15);
ds_parametros_w		varchar2(32000);
i			Integer;
ds_lista_aux_w		varchar2(255);
ds_sep_bv_w		varchar2(30)	:= obter_separador_bv;
nr_seq_monit_resp_w	number(10)	:= 0;
nr_seq_monit_hemo_w	number(10)	:= 0;
nr_seq_inf_leito_w number(10) := 0;
cd_modalidade_w		varchar2(15);
ie_respiracao_w		varchar2(7);
ds_erro_w		varchar2(4000);
nr_cirurgia_w		number(15);
nr_seq_pepo_w		number(10);
nr_cirurgia_ww		number(15);
nr_seq_pepo_ww		number(10);
nr_seq_age_anest_ocor_w	number(10);
cd_estabelecimento_w	number(10);


	procedure inserir_sv is
	begin
	if	(nr_seq_sinal_vital_w	= 0) then

		select	atendimento_sinal_vital_seq.nextval
		into	nr_seq_sinal_vital_w
		from	dual;

		insert into atendimento_sinal_vital(	nr_sequencia,
							nr_atendimento,
							dt_sinal_vital,
							dt_atualizacao,
							nm_usuario,
							CD_PESSOA_FISICA,
							dt_liberacao,
							ie_importado,
							nr_cirurgia,
							nr_seq_pepo,
							ie_situacao,
							ie_integracao)
				values		  (	nr_seq_sinal_vital_w,
							nr_atendimento_w,
							dt_sinal_vital_w,
							sysdate,
							nm_usuario_p,
							null,
							sysdate,
							'S',
							nr_cirurgia_w,
							nr_seq_pepo_w,
							'A',
							'S');
	end if;
	end;
	
	procedure inserir_info_leito is
	begin
	if (nr_seq_atend_info_leito_w = 0) then
		
		select ATEND_INFORMACAO_LEITO_seq.nextval
		into nr_seq_atend_info_leito_w
		from dual;
		
		insert into ATEND_INFORMACAO_LEITO( nr_sequencia,
							nr_atendimento,
							dt_atualizacao,
							dt_liberacao,
							nm_usuario,
							cd_paciente,
							ie_situacao)
				values			(nr_seq_atend_info_leito_w,
							nr_atendimento_w,
							sysdate,
							sysdate,
							nm_usuario_p,
							cd_pessoa_fisica_p,
							'A');
							
	end if;
	end;



	procedure atualizar_valor_sv(	nm_tabela_p	varchar2,
					nm_atributo_p	varchar2,
					vl_parametro_p	varchar2) is
	ds_comando_w	varchar2(2000);
	ds_parametros_w	varchar2(2000);
	vl_parametro_w	varchar2(2000);
	begin
	ds_comando_w	:= 	'	update	'||nm_tabela_p	||
				'	set	'||nm_atributo_p||' = :vl_parametro'||
				'	where	nr_sequencia	= :nr_sequencia ';
	begin
	
	vl_parametro_w	:= vl_parametro_p;
	if	((substr(nm_atributo_p,1,2)	= 'QT') or
		(substr(nm_atributo_p,1,2)	= 'PR') or
		(substr(nm_atributo_p,1,2)	= 'VL') or
		(substr(nm_atributo_p,1,2)	= 'TX')) then
		vl_parametro_w	:= replace(vl_parametro_w,'.',',');
	end if;

	if	(nm_tabela_p	= 'ATENDIMENTO_SINAL_VITAL') and
		(vl_parametro_p	is not null) and
		(lower(vl_parametro_p)	<> 'null')then
		inserir_sv;
		ds_parametros_w:=	'vl_parametro='||vl_parametro_w||ds_sep_bv_w||
					'nr_sequencia='||nr_seq_sinal_vital_w||ds_sep_bv_w;

		Exec_sql_Dinamico_bv(nm_usuario_p,ds_comando_w,ds_parametros_w);
	elsif	(nm_tabela_p	= 'ATENDIMENTO_MONIT_RESP') and
		(vl_parametro_p	is not null) and
		(lower(vl_parametro_p)	<> 'null')then
		--inserir_resp;
		ds_parametros_w:=	'vl_parametro='||vl_parametro_w||ds_sep_bv_w||
					'nr_sequencia='||nr_seq_monit_resp_w||ds_sep_bv_w;

		Exec_sql_Dinamico_bv(nm_usuario_p,ds_comando_w,ds_parametros_w);
	elsif	(nm_tabela_p	= 'ATEND_MONIT_HEMOD') and
		(vl_parametro_p	is not null) and
		(lower(vl_parametro_p)	<> 'null')then
		--inserir_hemo;
		ds_parametros_w:=	'vl_parametro='||vl_parametro_w||ds_sep_bv_w||
					'nr_sequencia='||nr_seq_monit_hemo_w||ds_sep_bv_w;

		Exec_sql_Dinamico_bv(nm_usuario_p,ds_comando_w,ds_parametros_w);
	elsif (nm_tabela_p = 'ATEND_INFORMACAO_LEITO') and
		(vl_parametro_p is not null) and
		(lower(vl_parametro_p) <> 'null')then
		inserir_info_leito;
		ds_parametros_w:= 'vl_parametro='||vl_parametro_w||ds_sep_bv_w||
					'nr_sequencia='||nr_seq_atend_info_leito_w||ds_sep_bv_w;
					
		Exec_sql_Dinamico_bv(nm_usuario_p,ds_comando_w,ds_parametros_w);
	end if;

	exception
		when others then
		ds_erro_w		:= sqlerrm(sqlcode);
		
	end;

	end;
	

	
begin

begin
dt_sinal_vital_w := to_date(dt_sinal_vital_p, 'yyyymmddhh24miss');
exception
	when others then
	dt_sinal_vital_w	:= sysdate;
end;
select 	max(nr_atendimento)
into 	nr_atendimento_w
from 	atendimento_paciente
where 	cd_pessoa_fisica  = cd_pessoa_fisica_p;

select max(cd_estabelecimento)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;

select	max(nr_cirurgia),
		max(nr_seq_pepo)
into	nr_cirurgia_w,
		nr_seq_pepo_w
from	cirurgia
where	nr_atendimento = nr_atendimento_w
and		dt_sinal_vital_w between dt_inicio_real and nvl(dt_termino, dt_sinal_vital_w);

select	max(nr_cirurgia),
		max(nr_seq_pepo)
into	nr_cirurgia_ww,
		nr_seq_pepo_ww
from	cirurgia
where	nr_atendimento = nr_atendimento_w;

ds_parametros_w    := ds_lista_parametros_p||ds_lista_parametros2_p;
ds_parametros_w	   := replace(ds_parametros_w,'null','');
i	:= 0;

while (length(ds_parametros_w) > 0) loop
	begin
	i	:= i+1;
	if	(instr(ds_parametros_w,';')	>0)  then
		Vetor_w(i).ds_lista	:= substr(ds_parametros_w,1,instr(ds_parametros_w,';')-1 );
		ds_parametros_w	:= substr(ds_parametros_w,instr(ds_parametros_w,';')+1,40000);

	else
		Vetor_w(i).ds_lista	:=substr(ds_parametros_w,1,length(ds_parametros_w) - 1);
		ds_parametros_w	:= null;
	end if;

	end;
end loop;

--dbms_output.put_line(Vetor_w.count);
for j in 1..Vetor_w.count loop
	begin

	ds_lista_aux_w	:= Vetor_w(j).ds_lista;

	Vetor_w(j).nm_sinal_vital	:= substr(ds_lista_aux_w,1,instr(ds_lista_aux_w,'#@#@')-1 );
	ds_lista_aux_w	:= substr(ds_lista_aux_w,instr(ds_lista_aux_w,'#@#@')+4,40000);
	Vetor_w(j).ds_unidade_medida	:= substr(ds_lista_aux_w,1,instr(ds_lista_aux_w,'#@#@')- 1 );
	ds_lista_aux_w	:= substr(ds_lista_aux_w,instr(ds_lista_aux_w,'#@#@')+4,40000);

	Vetor_w(j).vl_sinal_vital	:= substr(ds_lista_aux_w,1,4000 );

	end;
end loop;

if	(nr_atendimento_w	is not null) then

	for i in 1..Vetor_w.count loop
		begin

		if	(Vetor_w(i).nm_sinal_vital	= 'ScaleInfo.PatientWeightInKg') then
			atualizar_valor_sv('ATENDIMENTO_SINAL_VITAL','QT_PESO',Vetor_w(i).vl_sinal_vital);
    elsif	(Vetor_w(i).nm_sinal_vital	= 'ScaleInfo.CapturedWeightInKg') then
      atualizar_valor_sv('ATENDIMENTO_SINAL_VITAL','QT_PESO',Vetor_w(i).vl_sinal_vital);	
		elsif	(Vetor_w(i).nm_sinal_vital	= 'FrameArticulationInfo.HobAngleInDegrees') then
			atualizar_valor_sv('ATENDIMENTO_SINAL_VITAL','QT_ANGULO_CABECEIRA',Vetor_w(i).vl_sinal_vital);
      atualizar_valor_sv('ATEND_INFORMACAO_LEITO','NR_HOBANGLEINDEGREE', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'ConnectivityInfo.ConnectionState') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_CONNECTIONSTATE', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'FrameArticulationInfo.BedPosition') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_BEDPOSITION', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'FrameArticulationInfo.HeadRailsPosition') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_HEADRAILSPOSITION', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'FrameArticulationInfo.FootRailsPosition') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_FOOTRAILSPOSITION', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'FrameArticulationInfo.HobAngleInDegree') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','NR_HOBANGLEINDEGREE', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'SurfaceInfo.SurfaceMode') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_SURFACEMODE', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'SwitchesInfo.CprMode') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_CPRMODE', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'TransportInfo.BrakePosition') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_BRAKEPOSITION', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'PpmInfo.AlarmMode') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_ALARMMODE', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'PpmInfo.AlarmStatus') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_ALARMSTATUS', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'TherapyInfo.RotationTherapy') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_ROTATIONTHERAPY', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'TherapyInfo.PercussionTherapy') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_PERCUSSIONTHERAPY', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'TherapyInfo.VibrationTherapy') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_VIBRATIONTHERAPY', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'HobAlarmInfo.Mode') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_HOBALARMINFO', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'LockoutsInfo.HobAngleMotors') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_HOBANGLEMOTORS', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'LockoutsInfo.KneeAngleMotors') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_KNEEANGLEMOTORS', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'LockoutsInfo.BedHeightMotors') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_BEDHEIGHTMOTORS', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'LockoutsInfo.TiltAngleMotors') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_TILTANGLEMOTORS', Vetor_w(i).vl_sinal_vital);
		elsif (Vetor_w(i).nm_sinal_vital = 'LockoutsInfo.AllMotors') then
			atualizar_valor_sv('ATEND_INFORMACAO_LEITO','IE_ALLMOTORS', Vetor_w(i).vl_sinal_vital);
		end if;
		end;
	end loop;

end if;

commit;

end gerar_sinal_vital_HillRom;
/
