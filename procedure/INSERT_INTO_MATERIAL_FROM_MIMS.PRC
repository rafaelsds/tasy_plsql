create or replace 
procedure	insert_into_material_from_mims( cd_imp_material_p in imp_material.cd_material%type, 

						nm_usuario_p	varchar2,

						cd_class_material_p  material.cd_classe_material%type,

						nr_mims_version_p number

						) 

as 

nr_seq_material_w material.cd_material%type; 
nr_seq_out_w	number := 0;
prodcode_p 	number :=0;
nr_seq_ficha_tecnica_w 	number := 0;
prodcode_w mims_material_tasy.prodcode%type;
cd_mat_gen_tasy_w	number(15) := null;
formcode_w mims_material_tasy.formcode%type;
packcode_w mims_material_tasy.packcode%type;

begin 

select	material_seq.nextval
into	nr_seq_material_w 
from	dual ;

select	max(prodcode) 
into	prodcode_w
from 	mims_material_tasy
where 	cd_imp_material = cd_imp_material_p;

material_ficha_tecnica_prod(	prodcode_w,

				nm_usuario_p,

				nr_seq_ficha_tecnica_w,

				nr_mims_version_p

				);          
commit;
	
insert into material 

        (cd_material, 

        ie_tipo_material,  

        ds_material, 

        ds_reduzida, 

        cd_classe_material, 

        cd_unidade_medida_compra, 

        cd_unidade_medida_estoque, 

        cd_unidade_medida_consumo, 

        cd_unidade_medida_solic, 

        ds_orientacao_usuario, 

        ie_via_aplicacao, 

        qt_conversao_mg, 

        cd_unid_med_concetracao, 

        ie_padronizado, 

        cd_material_estoque, 

        ie_material_estoque, 

        ie_receita, 

        ie_cobra_paciente, 

        ie_baixa_inteira, 

        ie_situacao, 

        dt_cadastramento, 

        dt_atualizacao, 

        nm_usuario, 

        nr_minimo_cotacao, 

        ie_disponivel_mercado, 

        qt_minimo_multiplo_solic, 

        qt_conv_compra_estoque, 

        ie_prescricao, 

        qt_conv_estoque_consumo, 

        ie_preco_compra, 

        ie_material_direto, 

        ie_consignado, 

        ie_dias_util_medic, 

        ie_controle_medico, 

        ie_baixa_estoq_pac, 

        ie_curva_abc, 

        ie_bomba_infusao, 

        ie_diluicao, 

        ie_solucao, 

        ie_mistura, 

        ie_abrigo_luz, 

        ie_umidade_controlada, 

        ie_gravar_obs_prescr, 

        nm_usuario_nrec, 

        ie_inf_ultima_compra, 

        ie_gerar_lote, 

        ie_alto_risco, 

        cd_sistema_ant,
		QT_ORIG_MED_TOTAL_CONC,
		QT_BASE_MED_TOTAL_CONC,
		CD_ORIG_MED_CONC_UNIT,
		CD_BASE_MED_CONC_UNIT,
		CD_UNID_MED_BASE_CONC,
        NR_SEQ_FICHA_TECNICA) 

SELECT 	nr_seq_material_w, 

	ie_tipo_material, 

	ds_material, 

	ds_reduzida, 

	cd_class_material_p, 

	cd_unidade_medida_compra, 

	cd_unidade_medida_estoque, 

	cd_unidade_medida_consumo, 

	cd_unidade_medida_solic, 

	ds_orientacao_usuario, 

	ie_via_aplicacao, 

	qt_conversao_mg, 

	cd_unid_med_concetracao,  

	ie_padronizado, 

	nr_seq_material_w, 

	ie_material_estoque, 

	ie_receita, 

	ie_cobra_paciente, 

	ie_baixa_inteira, 

	'I', 

	dt_cadastramento, 

	SYSDATE, 

	nm_usuario_p, 

	nr_minimo_cotacao, 

	ie_disponivel_mercado, 

	qt_minimo_multiplo_solic, 

	qt_conv_compra_estoque, 

	ie_prescricao, 

	qt_conv_estoque_consumo, 

	ie_preco_compra, 

	ie_material_direto, 

	ie_consignado, 

	ie_dias_util_medic, 

	ie_controle_medico, 

	ie_baixa_estoq_pac, 

	ie_curva_abc, 

	ie_bomba_infusao, 

	ie_diluicao, 

	ie_solucao, 

	ie_mistura, 

	ie_abrigo_luz, 

	ie_umidade_controlada, 

	ie_gravar_obs_prescr, 

	nm_usuario_nrec, 

	ie_inf_ultima_compra, 

	ie_gerar_lote, 

	ie_alto_risco, 

	cd_sistema_ant,
	QT_ORIG_MED_TOTAL_CONC,
	QT_BASE_MED_TOTAL_CONC,
	CD_ORIG_MED_CONC_UNIT,
	CD_BASE_MED_CONC_UNIT,
	CD_UNID_MED_BASE_CONC,
	nr_seq_ficha_tecnica_w

from   	imp_material a 

where  	a.cd_material = cd_imp_material_p; 

--update	imp_material a

--set	a.ie_dirty_check = null

--where  	a.cd_material = cd_imp_material_p;



update 	mims_material_tasy a 

set    	a.CD_MATERIAL_TASY = nr_seq_material_w 

where  	a.CD_IMP_MATERIAL = cd_imp_material_p; 

begin
	INSERT_MATERIAL_MIMS_MAPPING(cd_imp_material_p, nr_seq_material_w, nm_usuario_p );
exception when others then
  null;
end;

begin
  select formcode, packcode
  into formcode_w, packcode_w
   from 	mims_material_tasy
  where 	cd_imp_material = cd_imp_material_p and prodcode = prodcode_w and rownum < 2;

  if(prodcode_w is not null and formcode_w is not null and packcode_w is not null) then
    insert_material_gen_mims(prodcode_w, formcode_w, packcode_w, nvl(WHEB_USUARIO_PCK.get_cd_estabelecimento(), 1), cd_mat_gen_tasy_w);
  end if;

  update	material
			set	cd_material_generico = cd_mat_gen_tasy_w
			where	cd_material = nr_seq_material_w;

exception when others then
  null;
end;

-- setting this value to avoid execution of trigger  
wheb_usuario_pck.set_ie_executar_trigger('N');
Insert_mat_estab_frm_mims(cd_imp_material_p, nm_usuario_p); 
Inst_mat_sis_ext_frm_mims(cd_imp_material_p, nm_usuario_p); 
wheb_usuario_pck.set_ie_executar_trigger('S');
    

--end if; 

END;
/
