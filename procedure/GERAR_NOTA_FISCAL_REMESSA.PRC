create or replace
procedure gerar_nota_fiscal_remessa(
					nm_usuario_p			varchar2,
					nr_nota_fiscal_p		number,
					cd_serie_p			varchar2,
					dt_emissao_p			date,
					dt_ent_saida_p			date,
					cd_moeda_p			varchar2,
					vl_cotacao_p			number,
					cd_operacao_p			number,
					cd_natureza_p			number,
					nr_danfe_p			number,
					cd_pessoa_fisica_p		varchar2,
					cd_cgc_p			varchar2,
					cd_fornec_nota_ordem_p		varchar2,
					cd_condicao_pagto_p		number,
					cd_estabelecimento_p		number,
					ds_notas_itens_p	 	varchar2,
					nr_sequencia_p			out number) is

ds_lista_notas_w		varchar2(200);
nr_sequencia_w			number(10,0);
nr_pos_virgula_w		number(10,0);
nr_sequencia_nf_w		nota_fiscal.nr_sequencia_nf%type;
nr_item_nf_w			nota_fiscal_item.nr_item_nf%type := 0;
ie_tipo_nota_w                  nota_fiscal.ie_tipo_nota%type;


Cursor C01 is
select	a.cd_cgc_emitente,
		a.cd_serie_nf,
		a.nr_nota_fiscal,
		a.cd_natureza_operacao,
		a.qt_item_nf,
		a.vl_unitario_item_nf,
		a.vl_total_item_nf,
		a.vl_frete,
		a.vl_desconto,
		a.vl_despesa_acessoria,
		a.cd_material,
		a.cd_procedimento,
		a.cd_setor_atendimento,
		a.cd_conta,
		a.cd_local_estoque,
		a.ds_observacao,
		a.qt_peso_bruto,
		a.qt_peso_liquido,
		a.cd_unidade_medida_compra,
		a.qt_item_estoque,
		a.cd_unidade_medida_estoque,
		a.cd_lote_fabricacao,
		a.dt_validade,
		a.cd_conta_contabil,
		a.vl_desconto_rateio,
		a.vl_seguro,
		a.cd_centro_custo,
		a.cd_material_estoque,
		a.ie_origem_proced,
		a.nr_ordem_compra,
		(a.vl_total_item_nf - a.vl_desconto) vl_liquido,
		a.pr_desconto,
		a.nr_item_oci,
		a.dt_entrega_ordem,
		a.nr_seq_conta_financ,
		a.nr_contrato,
		a.ds_complemento,
		a.nr_seq_proj_rec,
		a.vl_projeto,
		a.nr_atendimento,
		a.pr_desc_financ,
		a.vl_desc_financ,
		a.nr_seq_lic_item,
		a.nr_seq_ordem_serv,
		a.ds_inconsistencia,
		a.ds_justificativa,
		a.vl_liq_moeda_ref,
		a.nr_seq_unidade_adic,
		a.nr_emprestimo,
		a.nr_seq_item_emprestimo,
		a.ie_atualizar_barras,
		a.cd_barra_material,
		a.nr_sequencia_vinc_consig,
		a.nr_seq_proj_gpi,
		a.nr_seq_etapa_gpi,
		a.nr_seq_conta_gpi,
		a.dt_inicio_garantia,
		a.dt_fim_garantia,
		a.nr_seq_marca,
		a.ie_indeterminado,
		a.vl_item_preco_pub
from	nota_fiscal_item a
where	a.nr_sequencia = nr_sequencia_w;

Vet01 	C01%RowType;

begin

if (nr_nota_fiscal_p is not null) and 
   (ds_notas_itens_p is not null) then
								
	select	nvl(max(nr_sequencia_nf),0) + 1
	into	nr_sequencia_nf_w
	from	nota_fiscal
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_cgc_emitente 	is null
	and	cd_serie_nf 		= cd_serie_p
	and	nr_nota_fiscal		= nr_nota_fiscal_p;

	select 	nota_fiscal_seq.nextval
	into	nr_sequencia_p
	from 	dual;
	
	if (nvl(cd_pessoa_fisica_p,0) > 0) then
	  ie_tipo_nota_w := 'EF';
	else
	  ie_tipo_nota_w := 'EN';
	end if;
	
	insert into nota_fiscal (
			nr_sequencia,
			nr_nota_fiscal,
			nr_sequencia_nf,
			cd_serie_nf,
			cd_estabelecimento,
			dt_emissao,
			dt_entrada_saida,
			cd_condicao_pagamento,
			cd_moeda,
			vl_conv_moeda,
			vl_frete,
			vl_mercadoria,
			vl_total_nota,
			qt_peso_bruto,
			qt_peso_liquido,
			cd_operacao_nf,
			cd_natureza_operacao,
			nr_danfe,
			cd_cgc_emitente, 
			ie_acao_nf,
			ie_emissao_nf,
			ie_tipo_frete,
			ie_tipo_nota,
			ie_situacao,
			dt_atualizacao,
			nm_usuario,
			cd_pessoa_fisica,
			cd_cgc)
	values	(
			nr_sequencia_p,
			nr_nota_fiscal_p,
			nr_sequencia_nf_w,
			cd_serie_p,
			cd_estabelecimento_p,
			dt_emissao_p,
			dt_ent_saida_p,
			cd_condicao_pagto_p,
			cd_moeda_p,
			vl_cotacao_p,
			0,
			0,
			0,
			0,
			0,
			cd_operacao_p,
			cd_natureza_p,
			nr_danfe_p,
			cd_cgc_p,
			'1',
			'0',
			0,
			ie_tipo_nota_w,
			'1',
			sysdate,
			nm_usuario_p,
			cd_pessoa_fisica_p,
			cd_cgc_p);		
						
	if	(ds_notas_itens_p is not null) and 
		(nr_sequencia_p is not null) then
		begin
			ds_lista_notas_w	:= ds_notas_itens_p;
			while	(ds_lista_notas_w is not null) loop
				begin
				nr_pos_virgula_w := instr(ds_lista_notas_w,',');
				
					if	(nr_pos_virgula_w > 0) then
						begin
						nr_sequencia_w			:= substr(ds_lista_notas_w,0,nr_pos_virgula_w-1);
						ds_lista_notas_w		:= substr(ds_lista_notas_w,nr_pos_virgula_w+1,length(ds_lista_notas_w));			
						end;
					else
						begin
						nr_sequencia_w			:= to_number(ds_lista_notas_w);
						ds_lista_notas_w		:= null;
						end;
					end if;	
					
					
					if	(nvl(nr_sequencia_w,0) > 0) then
						open C01;
						loop
						fetch C01 into	
							Vet01;
						exit when C01%notfound;
							begin
							nr_item_nf_w := (nr_item_nf_w + 1);	
							insert into nota_fiscal_item(
									nr_sequencia,
									cd_estabelecimento,
									cd_cgc_emitente,
									cd_serie_nf,
									nr_nota_fiscal,
									nr_sequencia_nf,
									nr_item_nf,
									cd_natureza_operacao,
									qt_item_nf,
									vl_unitario_item_nf,
									vl_total_item_nf,
									dt_atualizacao,
									nm_usuario,
									vl_frete,
									vl_desconto,
									vl_despesa_acessoria,
									cd_material,
									cd_procedimento,
									cd_setor_atendimento,
									cd_conta,
									cd_local_estoque,
									ds_observacao,
									qt_peso_bruto,
									qt_peso_liquido,
									cd_unidade_medida_compra,
									qt_item_estoque,
									cd_unidade_medida_estoque,
									cd_lote_fabricacao,
									dt_validade,
									dt_atualizacao_estoque,
									cd_conta_contabil,
									vl_desconto_rateio,
									vl_seguro,
									cd_centro_custo,
									cd_material_estoque,
									ie_origem_proced,
									nr_ordem_compra,
									vl_liquido,
									pr_desconto,
									nr_item_oci,
									dt_entrega_ordem,
									nr_seq_conta_financ,
									nr_contrato,
									ds_complemento,
									nr_seq_proj_rec,
									vl_projeto,
									nr_atendimento,
									pr_desc_financ,
									vl_desc_financ,
									nr_seq_lic_item,
									nr_seq_ordem_serv,
									ds_inconsistencia,
									ds_justificativa,
									vl_liq_moeda_ref,
									nr_seq_unidade_adic,
									nr_emprestimo,
									nr_seq_item_emprestimo,
									ie_atualizar_barras,
									cd_barra_material,
									nr_sequencia_vinc_consig,
									nr_seq_proj_gpi,
									nr_seq_etapa_gpi,
									nr_seq_conta_gpi,
									dt_inicio_garantia,
									dt_fim_garantia,
									nr_seq_marca,
									ie_indeterminado,
									vl_item_preco_pub)
							values (
									nr_sequencia_p,
									cd_estabelecimento_p,
									Vet01.cd_cgc_emitente,
									Vet01.cd_serie_nf,
									Vet01.nr_nota_fiscal,
									nr_sequencia_nf_w,
									nr_item_nf_w,
									Vet01.cd_natureza_operacao,
									Vet01.qt_item_nf,
									Vet01.vl_unitario_item_nf,
									Vet01.vl_total_item_nf,
									sysdate,
									nm_usuario_p,
									Vet01.vl_frete,
									Vet01.vl_desconto,
									Vet01.vl_despesa_acessoria,
									Vet01.cd_material,
									Vet01.cd_procedimento,
									Vet01.cd_setor_atendimento,
									Vet01.cd_conta,
									Vet01.cd_local_estoque,
									Vet01.ds_observacao,
									Vet01.qt_peso_bruto,
									Vet01.qt_peso_liquido,
									Vet01.cd_unidade_medida_compra,
									Vet01.qt_item_estoque,
									Vet01.cd_unidade_medida_estoque,
									Vet01.cd_lote_fabricacao,
									Vet01.dt_validade,
									'',
									Vet01.cd_conta_contabil,
									Vet01.vl_desconto_rateio,
									Vet01.vl_seguro,
									Vet01.cd_centro_custo,
									Vet01.cd_material_estoque,
									Vet01.ie_origem_proced,
									Vet01.nr_ordem_compra,
									Vet01.vl_liquido,
									Vet01.pr_desconto,
									Vet01.nr_item_oci,
									Vet01.dt_entrega_ordem,
									Vet01.nr_seq_conta_financ,
									Vet01.nr_contrato,
									Vet01.ds_complemento,
									Vet01.nr_seq_proj_rec,
									Vet01.vl_projeto,
									Vet01.nr_atendimento,
									Vet01.pr_desc_financ,
									Vet01.vl_desc_financ,
									Vet01.nr_seq_lic_item,
									Vet01.nr_seq_ordem_serv,
									Vet01.ds_inconsistencia,
									Vet01.ds_justificativa,
									Vet01.vl_liq_moeda_ref,
									Vet01.nr_seq_unidade_adic,
									Vet01.nr_emprestimo,
									Vet01.nr_seq_item_emprestimo,
									Vet01.ie_atualizar_barras,
									Vet01.cd_barra_material,
									Vet01.nr_sequencia_vinc_consig,
									Vet01.nr_seq_proj_gpi,
									Vet01.nr_seq_etapa_gpi,
									Vet01.nr_seq_conta_gpi,
									Vet01.dt_inicio_garantia,
									Vet01.dt_fim_garantia,
									Vet01.nr_seq_marca,
									Vet01.ie_indeterminado,
									Vet01.vl_item_preco_pub);				
								
							update	nota_fiscal
							set	nr_seq_nota_reemissao 	= nr_sequencia_p --nota nova criada
							where 	nr_sequencia 		= nr_sequencia_w; --nota da lista
							
							end;
						end loop;
						close C01;
					end if;
				end;
			end loop;
		end;
	end if;
	atualiza_total_nota_fiscal(nr_sequencia_p, nm_usuario_p);
end if;

commit;

end gerar_nota_fiscal_remessa;
/