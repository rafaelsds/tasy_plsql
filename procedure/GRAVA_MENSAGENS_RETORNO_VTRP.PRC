create or replace
procedure grava_mensagens_retorno_vtrp(nr_sequencia_pdf_p number, 
                                       cd_mensagem_ext_p varchar2,
                                       nm_usuario_p varchar2,
                                       cd_procedimento_p number,
                                       nr_seq_lote_externo_p number,
                                       novo_nr_lote_ext_p	out number) is 

nr_sequencia_w             number(10);
nr_prescricao_w            number(10);
nr_seq_prescr_w            number(10);
cd_mensagem_ext_w          varchar2(20);
nr_seq_exame_w             number;
cd_material_exame_w        varchar2(20);
cd_setor_coleta_w          number;
nr_seq_lote_externo_w      number;
cd_cgc_externo_w	         varchar2(14);
nr_atendimento_w	         atendimento_paciente.nr_atendimento%type;
cd_convenio_w		           convenio.cd_convenio%type;
cd_cgc_ext_cad_w	         exame_laboratorio_externo.cd_cgc_externo%type;
nr_seq_material_w	         exame_laboratorio_externo.nr_seq_material%type;
Ie_atual_desc_lote_w	     varchar2(1);
ie_utilizar_setor_coleta_w varchar2(1);
cd_estabelecimento_w       number;

Cursor C01 is                   
  select rl.nr_prescricao, rl.nr_seq_prescricao
    from result_laboratorio rl
   inner join result_laboratorio_pdf rlp on rlp.nr_seq_resultado = rl.nr_sequencia and rlp.nr_prescricao = rl.nr_prescricao and rlp.nr_seq_prescricao = rl.nr_seq_prescricao  
   where rl.nr_sequencia = nr_sequencia_pdf_p 
     and ((rl.cd_procedimento = cd_procedimento_p) or 
          (cd_procedimento_p is null))
  union
  select pp.nr_prescricao, pp.nr_sequencia nr_seq_prescricao
    from prescr_procedimento pp
   inner join result_laboratorio rl on rl.nr_prescricao = pp.nr_prescricao and rl.nr_seq_prescricao = pp.nr_sequencia
   inner join result_laboratorio_pdf rlp on rlp.nr_seq_resultado = rl.nr_sequencia and rlp.nr_prescricao = rl.nr_prescricao and rlp.nr_seq_prescricao = rl.nr_seq_prescricao
   where pp.nr_seq_lote_externo = nr_seq_lote_externo_p
     and ((pp.cd_procedimento = cd_procedimento_p) or 
          (cd_procedimento_p is null));
begin
  nr_seq_lote_externo_w := null;      
  
  select max(cd_estabelecimento)
    into cd_estabelecimento_w
    from lab_lote_externo
   where nr_sequencia = nr_seq_lote_externo_p;

	open C01;
	loop
	fetch C01 into nr_prescricao_w, nr_seq_prescr_w;
	exit when C01%notfound;    
    
    if (cd_mensagem_ext_p <> '1') then
      update prescr_procedimento set nr_seq_lote_externo = null
       where nr_prescricao = nr_prescricao_w
         and nr_sequencia = nr_seq_prescr_w;       

      select pp.nr_seq_exame, pp.cd_material_exame, pp.cd_setor_coleta
        into nr_seq_exame_w, cd_material_exame_w, cd_setor_coleta_w
        from prescr_procedimento pp
  inner join lab_exame_equip lee on lee.nr_seq_exame = pp.nr_seq_exame
  inner join equipamento_lab el on el.cd_equipamento = lee.cd_equipamento and el.ds_sigla = 'RESPRESTUN'    
       where pp.nr_prescricao = nr_prescricao_w
         and pp.nr_sequencia = nr_seq_prescr_w;                 

      select nvl(max(cd_cgc_externo),null)
        into cd_cgc_externo_w
        from exame_laboratorio
       where nr_seq_exame = nr_seq_exame_w;
           
      if (cd_cgc_externo_w is null)  then
        select Max(nr_atendimento)
          into nr_atendimento_w
          from prescr_medica
         where nr_prescricao = nr_prescricao_w;

        select nvl(max(c.cd_convenio),0)
          into cd_convenio_w
          from atend_categoria_convenio c
         where c.nr_atendimento = nr_atendimento_w
	         and c.nr_seq_interno = ( SELECT NVL(MAX(nr_seq_interno),0)
		                                  FROM atend_categoria_convenio z
		                                 WHERE z.nr_atendimento = c.nr_atendimento
		                                   AND z.dt_inicio_vigencia	=	(SELECT	MAX(x.dt_inicio_vigencia)
			                                                               FROM atend_categoria_convenio x
			                                                              WHERE x.nr_atendimento = c.nr_atendimento));

        select nvl(max(nr_sequencia),0)
          into nr_seq_material_w
          from material_exame_lab
         where cd_material_exame = cd_material_exame_w;

        select MAX(cd_cgc_externo)
       	  into cd_cgc_ext_cad_w
          from (select cd_cgc_externo
                  from exame_laboratorio_externo
                 where nr_seq_exame = nr_seq_exame_w
                   and nvl(cd_convenio, cd_convenio_w) = cd_convenio_w
		               and nvl(nr_seq_material, nr_seq_material_w) = nr_seq_material_w
		            order by nvl(cd_convenio,9999999999), nvl(ie_prioridade,1)) 
         where rownum = 1;

        cd_cgc_externo_w  :=  cd_cgc_ext_cad_w;
      end if;                         

      Obter_Param_Usuario(718,6,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,Ie_atual_desc_lote_w);
      Obter_Param_Usuario(718,22,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_utilizar_setor_coleta_w);          

      if ((ie_utilizar_setor_coleta_w = 'S') and (cd_setor_coleta_w is not null)) then
     		select	nvl(max(nr_sequencia),0)
      		into	nr_seq_lote_externo_w
      		from	lab_lote_externo
      	 where	cd_cgc = cd_cgc_externo_w
       		 and		lab_obter_regra_estab_ext(cd_estabelecimento,cd_estabelecimento_w,0) = 'S'
       		 and		nvl(cd_setor_atendimento,cd_setor_coleta_w) = cd_setor_coleta_w
           and		dt_envio is null;
      else
    		select	nvl(max(nr_sequencia),0)
      		into	nr_seq_lote_externo_w
      		from	lab_lote_externo
      	 where	cd_cgc = cd_cgc_externo_w
       		 and		lab_obter_regra_estab_ext(cd_estabelecimento,cd_estabelecimento_w,0) = 'S'
        	 and		dt_envio is null;
      end if;

      if (nr_seq_lote_externo_w = 0) then          
         if (cd_cgc_externo_w is not null) then
           select lab_lote_externo_seq.NextVal
             into nr_seq_lote_externo_w
             from dual;

           if (ie_utilizar_setor_coleta_w <> 'S') then
             cd_setor_coleta_w := null;
           end if;

           insert into lab_lote_externo ( nr_sequencia, cd_cgc, dt_lote, dt_retorno_prev, dt_atualizacao, nm_usuario,
                                          dt_envio, dt_retorno, cd_estabelecimento,ds_arquivo,cd_setor_atendimento)
                              	 values ( nr_seq_lote_externo_w, cd_cgc_externo_w, sysdate, sysdate, sysdate, nm_usuario_p,
                                     			null,null, cd_estabelecimento_w,decode(Ie_atual_desc_lote_w,'S',to_char(sysdate,'ddmmyyyy'),null),cd_setor_coleta_w);
         end if;          
       end if;  

      if (nr_seq_lote_externo_w <> 0) then
         update prescr_procedimento set NR_SEQ_LOTE_EXTERNO = nr_seq_lote_externo_w,
                                        nm_usuario = nm_usuario_p,
                                        dt_atualizacao = sysdate
          where nr_prescricao = nr_prescricao_w
            and nr_sequencia  = nr_seq_prescr_w;
      end if;    
    end if;         
	end loop;
	close c01;    
  
  novo_nr_lote_ext_p := nr_seq_lote_externo_w;

  commit;
end grava_mensagens_retorno_vtrp;
/