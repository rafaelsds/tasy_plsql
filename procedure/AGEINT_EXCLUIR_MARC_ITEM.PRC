create or replace
procedure ageint_excluir_marc_item	(	nr_seq_item_p		Number,
						nm_usuario_p		Varchar2) is 

begin
if (nvl(nr_seq_item_p,0) > 0) then

	delete
	from	ageint_marcacao_usuario 
	where 	nr_seq_ageint_item	= nr_seq_item_p 
	and 	nm_usuario 		= nm_usuario_p;
	
end if;

commit;

end ageint_excluir_marc_item;
/