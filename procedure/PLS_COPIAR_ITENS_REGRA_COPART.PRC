create or replace
procedure pls_copiar_itens_regra_copart
			(	nr_seq_regra_ant_p	number,
				nr_seq_regra_nova_p	number,
				nm_usuario_p		varchar2,
				ie_commit_p		varchar2) is

nr_seq_regra_aprop_w	pls_regra_copartic_aprop.nr_sequencia%type;
nr_seq_regra_benef_w	pls_regra_copartic_benef.nr_sequencia%type;
nr_seq_regra_conta_w	pls_regra_copartic_conta.nr_sequencia%type;
nr_seq_regra_util_w	pls_regra_copartic_util.nr_sequencia%type;
nr_seq_regra_prest_w	pls_regra_copartic_prest.nr_sequencia%type;
nr_seq_regra_guia_w	pls_regra_copartic_guia.nr_sequencia%type;
nr_seq_regra_inter_w	pls_regra_copartic_interna.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_regra_copartic_aprop
	where	nr_seq_regra	= nr_seq_regra_ant_p;

Cursor C02 is
	select	nr_sequencia
	from	pls_regra_copartic_benef
	where	nr_seq_regra	= nr_seq_regra_ant_p;

Cursor C03 is
	select	nr_sequencia
	from	pls_regra_copartic_conta
	where	nr_seq_regra	= nr_seq_regra_ant_p;

Cursor C04 is
	select	nr_sequencia
	from	pls_regra_copartic_util
	where	nr_seq_regra	= nr_seq_regra_ant_p;

Cursor C05 is
	select	nr_sequencia
	from	pls_regra_copartic_prest
	where	nr_seq_regra	= nr_seq_regra_ant_p;

Cursor C06 is
	select	nr_sequencia
	from	pls_regra_copartic_guia
	where	nr_seq_regra	= nr_seq_regra_ant_p;
	
Cursor C07 is
	select	nr_sequencia
	from	pls_regra_copartic_interna
	where	nr_seq_regra	= nr_seq_regra_ant_p;

begin

open C01;
loop
fetch C01 into	
	nr_seq_regra_aprop_w;
exit when C01%notfound;
	begin
	pls_duplicar_registro('pls_regra_copartic_aprop',nm_usuario_p,ie_commit_p,nr_seq_regra_aprop_w);
	update	pls_regra_copartic_aprop
	set	nr_seq_regra = nr_seq_regra_nova_p
	where	nr_sequencia = nr_seq_regra_aprop_w;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_seq_regra_benef_w;
exit when C02%notfound;
	begin
	pls_duplicar_registro('pls_regra_copartic_benef',nm_usuario_p,ie_commit_p,nr_seq_regra_benef_w);
	update	pls_regra_copartic_benef
	set	nr_seq_regra = nr_seq_regra_nova_p
	where	nr_sequencia = nr_seq_regra_benef_w;	
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_regra_conta_w;
exit when C03%notfound;
	begin
	pls_duplicar_registro('pls_regra_copartic_conta',nm_usuario_p,ie_commit_p,nr_seq_regra_conta_w);
	update	pls_regra_copartic_conta
	set	nr_seq_regra = nr_seq_regra_nova_p
	where	nr_sequencia = nr_seq_regra_conta_w;
	end;
end loop;
close C03;
		
open C04;
loop
fetch C04 into	
	nr_seq_regra_util_w;
exit when C04%notfound;
	begin
	pls_duplicar_registro('pls_regra_copartic_util',nm_usuario_p,ie_commit_p,nr_seq_regra_util_w);
	update	pls_regra_copartic_util
	set	nr_seq_regra = nr_seq_regra_nova_p
	where	nr_sequencia = nr_seq_regra_util_w;
	end;
end loop;
close C04;

open C05;
loop
fetch C05 into	
	nr_seq_regra_prest_w;
exit when C05%notfound;
	begin
	pls_duplicar_registro('pls_regra_copartic_prest',nm_usuario_p,ie_commit_p,nr_seq_regra_prest_w);
	update	pls_regra_copartic_prest
	set	nr_seq_regra = nr_seq_regra_nova_p
	where	nr_sequencia = nr_seq_regra_prest_w;
	end;
end loop;
close C05;

open C06;
loop
fetch C06 into	
	nr_seq_regra_guia_w;
exit when C06%notfound;
	begin
	pls_duplicar_registro('pls_regra_copartic_guia',nm_usuario_p,ie_commit_p,nr_seq_regra_guia_w);
	update	pls_regra_copartic_guia
	set	nr_seq_regra = nr_seq_regra_nova_p
	where	nr_sequencia = nr_seq_regra_guia_w;	
	end;
end loop;
close C06;

open C07;
loop
fetch C07 into	
	nr_seq_regra_inter_w;
exit when C07%notfound;
	begin
	pls_duplicar_registro('pls_regra_copartic_interna',nm_usuario_p,ie_commit_p,nr_seq_regra_inter_w);
	update	pls_regra_copartic_interna
	set	nr_seq_regra = nr_seq_regra_nova_p
	where	nr_sequencia = nr_seq_regra_inter_w;
	
	end;
end loop;
close C07;

if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_copiar_itens_regra_copart;
/