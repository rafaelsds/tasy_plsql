create or replace
procedure pls_oc_cta_aplic_val_exig_proc(
			dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de exig�ncia de procedimento, conforme estrutura
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Nesta valida��o espec�ficamente n�o � atualizado o ie_valido_temp para N pois a logica da regra
vai em contra-partida das demais.

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjung OS 601993 - 11/06/2013 - Cria��o da rotina.
------------------------------------------------------------------------------------------------------------------
jjung OS 602057 - 18/06/2013
 
 Altera��o:	Retirado o campo dados_filtro_w.ieececao e substituido pelo ie_gera_ocorrencia.
 
 Motivo:	Foi identificado que a l�gica do campo ie_excecao era muito confusa e poderia
	trazer problemas.
 ------------------------------------------------------------------------------------------------------------------
 jjung 29/06/2013 

Altera��o:	Adicionado parametro nos m�todos de atualiza��o dos campos IE_VALIDO e IE_VALIDO_TEMP 
	da PLS_TIPOS_OCOR_PCK
	
Motivo:	Se tornou necess�rio diferenciar os filtros das valida��es na hora de realizar esta opera��o
	para que os filtros de exce��o funcionem corretamente.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
null;
end pls_oc_cta_aplic_val_exig_proc;
/