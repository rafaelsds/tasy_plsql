create or replace
procedure estornar_lib_cot_compra_forn(
				nr_sequencia_p		number,
				nm_usuario_p		varchar2,
				ds_retorno_p	out	varchar2) as


ie_status_w			varchar2(5);
ds_erro_w			varchar2(255);
qt_existe_ordem_w		number(5);
nr_cot_compra_w			number(10);
dt_retorno_prev_w		date;
dt_geracao_ordem_compra_w	date;


/*Esta procedure deve ser utilizada somente pelo Portal, ou seja
Pode ser utilizada somente pelo proprio fornecedor*/

begin

select	nvl(max(ie_status), 'X')
into	ie_status_w
from	cot_compra_forn
where	nr_sequencia = nr_sequencia_p;
if	(ie_status_w in ('FF', 'CF')) then
	begin
	/*Verifica se este fornecedor tem algum item que gerou Ordem de compras*/
	select	count(*)
	into	qt_existe_ordem_w
	from	cot_compra_forn_item
	where	nr_seq_cot_forn = nr_sequencia_p
	and	nr_ordem_compra is not null;
	if	(qt_existe_ordem_w > 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(300339);
	end if;

	/*Verifica o prazo de validade da cota��o*/
	select	nr_cot_compra
	into	nr_cot_compra_w
	from	cot_compra_forn
	where	nr_sequencia = nr_sequencia_p;
	select	max(dt_retorno_prev)
	into	dt_retorno_prev_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_w;
	if	(dt_retorno_prev_w is not null) and
		(dt_retorno_prev_w < sysdate) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(300340);
	end if;

	/*Verifica se a cota��o tem data de gera��o*/
	select	max(dt_geracao_ordem_compra)
	into	dt_geracao_ordem_compra_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_w;
	if	(dt_geracao_ordem_compra_w is not null) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(300339);
	end if;
	end;
else
	ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(300341);
end if;	


if	(ds_erro_w is null) then
	/*Passa a cota��o para (Andamento com o fornecedor)*/
	update	cot_compra_forn
	set	ie_status	= 'AF',
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
	commit;
else
	ds_retorno_p	:= substr(ds_erro_w,1,255);
end if;

end estornar_lib_cot_compra_forn;
/
