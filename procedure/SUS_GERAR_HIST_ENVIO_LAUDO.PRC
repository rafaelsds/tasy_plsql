create or replace
procedure sus_gerar_hist_envio_laudo(	nr_seq_lote_p		number,
				nm_usuario_p		Varchar2) is 

nr_sequencia_w		number(10);
tp_registro_w		number(10);
cd_estabelecimento_w	number(4);
nr_lote_w			number(10);
nr_seq_laudo_w          	number(10);
ie_tipo_laudo_w          	varchar2(1);
nr_laudo_w               	varchar2(13);
cd_procedimento_princ_w	number(15);
nr_aih_w                		number(13);
nr_seq_aih_w            	number(10);
cd_procedimento_supl_w  	number(15);
cd_cid_principal_w       	varchar2(4);
qt_procedimento_w		number(10);
dt_competencia_w         	varchar2(6);
nr_prontuario_w         	varchar2(20);
nr_cartao_sus_w         	varchar2(20);
nr_atendimento_w		number(10);
nr_interno_conta_w		number(10);
qt_solic_w			number(5);

Cursor C01 is
	select	tp_registro,
		nr_lote,
		nr_seq_laudo,
		ie_tipo_laudo,
		nr_laudo,
		cd_procedimento_solic,
		nr_aih,
		nr_sequencia,
		cd_proced_solic,
		cd_cid_principal,
		qt_procedimento,
		dt_competencia,
		nr_prontuario,
		nr_cartao_sus,
		nr_atendimento,
		nr_interno_conta,
		qt_solic
	from	sus_interf_aih_iscmpoa_v
	where	nr_lote = nr_seq_lote_p
	and	tp_registro <> 1
	order by	tp_registro, 
		nr_seq_laudo, 
		ie_tipo_laudo, 
		cd_proced_solic;			
			
begin

cd_estabelecimento_w := nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

delete from sus_hist_envio_laudo_pac
where	nr_lote = nr_seq_lote_p;

open C01;
loop
fetch C01 into	
	tp_registro_w,
	nr_lote_w,
	nr_seq_laudo_w,
	ie_tipo_laudo_w,
	nr_laudo_w,
	cd_procedimento_princ_w,
	nr_aih_w,
	nr_seq_aih_w,
	cd_procedimento_supl_w,
	cd_cid_principal_w,
	qt_procedimento_w,
	dt_competencia_w,
	nr_prontuario_w,
	nr_cartao_sus_w,
	nr_atendimento_w,
	nr_interno_conta_w,
	qt_solic_w;
exit when C01%notfound;
	begin
	
	select	sus_hist_envio_laudo_pac_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into sus_hist_envio_laudo_pac (	
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_lote,
		nr_seq_laudo,
		ie_tipo_laudo,
		nr_laudo,
		cd_procedimento_princ,
		nr_aih,
		nr_seq_aih,
		cd_procedimento_supl,
		cd_cid_principal,
		qt_procedimento,
		dt_competencia,
		nr_prontuario,
		nr_cartao_sus,
		nr_atendimento,
		nr_interno_conta,
		qt_solic)
	values(	nr_sequencia_w,
		cd_estabelecimento_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_lote_w,
		nr_seq_laudo_w,
		ie_tipo_laudo_w,
		nr_laudo_w,
		cd_procedimento_princ_w,
		nr_aih_w,
		nr_seq_aih_w,
		cd_procedimento_supl_w,
		cd_cid_principal_w,
		qt_procedimento_w,
		dt_competencia_w,
		nr_prontuario_w,
		nr_cartao_sus_w,
		nr_atendimento_w,
		nr_interno_conta_w,
		qt_solic_w);
		
	end;
end loop;
close C01;

commit;

end sus_gerar_hist_envio_laudo;
/
