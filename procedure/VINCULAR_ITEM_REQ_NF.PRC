create or replace
procedure vincular_item_req_nf(	nr_requisicao_p		number,
				nr_sequencia_p		number,
				nr_seq_nf_p		number,
				nm_usuario_p		Varchar2) is 

begin

insert into item_requisicao_mat_nf(
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_requisicao,
	nr_sequencia,
	nr_seq_nf)
values(	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_requisicao_p,
	nr_sequencia_p,
	nr_seq_nf_p);

commit;

end vincular_item_req_nf;
/