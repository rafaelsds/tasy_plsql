create or replace
Procedure Inserir_Hdm_Resumo_Mensal(Seq_Hdm_Benef_Cubo_p number,
				    Dt_Mesano_Referencia_P Date Default Null,
				    Nm_Usuario_P Varchar2 Default Null,
				    Vl_Resultado_P number Default Null,
				    Pr_Sinistralidade_P number Default Null,
				    Vl_Custo_P number Default Null,
				    Vl_Receita_P number Default Null,
				    Ie_Origem_P Varchar2 Default Null)Is

Pessoa_Fisica_W Number(10,0);
Valida_Insercao_W Number(10,0);
Sql_Errm_W varchar2(255);

Begin

Select coalesce(max(nr_sequencia),0) Into Valida_Insercao_W From Hdm_Benef_Custo_Cubo Where  Nr_Seq_Benef_Cubo = Seq_Hdm_Benef_Cubo_P And Dt_Mesano_Referencia = Dt_Mesano_Referencia_P;

If(Valida_Insercao_W = 0) Then

	Select Max(Cd_Pessoa_Fisica) Into Pessoa_Fisica_W From Hdm_Benef_Cubo Where Nr_Sequencia = Seq_Hdm_Benef_Cubo_P;
	
	begin
	Insert Into Hdm_Benef_Custo_Cubo(Nr_Sequencia,
		Dt_Mesano_Referencia,
		Nm_Usuario,
		nm_usuario_nrec,
		Vl_Resultado,
		Pr_Sinistralidade,
		Vl_Custo,
		Vl_Receita,
		Dt_Atualizacao,
		dt_atualizacao_nrec,
		Nr_Seq_Benef_Cubo,
		Ie_Origem)
	Values(Hdm_Benef_Custo_Cubo_Seq.Nextval,
		Dt_Mesano_Referencia_P,
		Nm_Usuario_P,
		Nm_Usuario_P,
		Vl_Resultado_P,
		Pr_Sinistralidade_P,
		Vl_Custo_P,
		Vl_Receita_P,
		Sysdate,
		sysdate,
		Seq_Hdm_Benef_Cubo_P,
		Ie_Origem_P);
	
	Exception
	When Others Then 
	Sql_Errm_W	:= Sqlerrm;
	Insert Into Hdm_Log_Integracao (Nr_Sequencia,
		Dt_Atualizacao,
		Nm_Usuario,
		Dt_Atualizacao_Nrec,
		Nm_Usuario_Nrec,
		Ie_Tipo_Log,
		Ds_Titulo,
		Ds_Log_Hdm)
	Values (Hdm_Log_Integracao_Seq.Nextval,
		Sysdate,
		Nm_Usuario_P,
		Sysdate,
		Nm_Usuario_P,
		'I',
		'Inserir HDM_BENEF_CUSTO_CUBO (Resumo_Mensal)',
		Sql_Errm_W);
	end;
end if;


End Inserir_Hdm_Resumo_Mensal;
/