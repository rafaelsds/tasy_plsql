create or replace
procedure TF_Processar_Ligacao(		cd_estabelecimento_p	number,
					ie_entrada_saida_p	varchar2,
					nr_seq_log_lig_p	number,
					nr_atendimento_p	number,
					nm_usuario_p		varchar2) is
nr_seq_log_w		number(10);
nr_seq_tipo_lig_w	number(10);
nr_seq_ramal_w		number(10);
nr_fone_discado_w	varchar2(30);
vl_ligacao_w		number(15,2);
dt_fim_w		date;
cd_setor_atend_w	number(5);
ie_tipo_ramal_w		number(2);
cd_ramal_w		varchar2(20);
ie_cobranca_w		number(2);
nr_seq_proc_interno_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
nr_atendimento_w	number(10);
cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);
nr_seq_interno_w	number(10);
dt_entrada_unidade_w	date;
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
cd_cnpj_w		varchar2(14);
nr_sequencia_w		number(10);
ie_valor_informado_w	varchar2(1);
ds_erro_w		varchar2(255);
ds_obs_w		varchar2(255);

cd_unidade_basica_w	varchar2(10);
cd_unidade_compl_w	varchar2(10);

ie_entrada_saida_w	varchar2(1);
dt_fim_conta_w		date:= null;
nr_seq_item_w		number(10,0);
qt_item_w		number(10,0);

ie_acao_excesso_w		varchar2(10);
qt_excedida_w			number(15,3)	:= 0;
cd_convenio_glosa_w		number(5);
cd_categoria_glosa_w		varchar2(10);
nr_interno_conta_w		number(10,0);
dt_ligacao_integracao_w		date;
cd_convenio_excesso_w		number(5,0);
cd_categoria_excesso_w		varchar2(10);


TYPE cc01 IS REF CURSOR;	
c01 cc01;

/*cursor c01 is
	select	nr_sequencia,
		nr_seq_tipo_lig,
		nr_seq_ramal,
		nr_fone_discado,
		nvl(vl_ligacao,0),
		nvl(nvl(dt_fim,dt_inicio),sysdate),
		nr_atendimento,
		cd_setor_atendimento
	from	tf_log_ligacao
	where	ie_processado		= 0
	  and	cd_estabelecimento	= cd_estabelecimento_p
	  and	nr_sequencia		= nvl(nr_seq_log_lig_p,nr_sequencia)
	  and	nvl(ie_entrada_saida,ie_entrada_saida_w) = ie_entrada_saida_w
	  and	nr_atendimento		= nr_atendimento_p 
	  and 	nr_atendimento_p is not null
	union all
	select	nr_sequencia,
		nr_seq_tipo_lig,
		nr_seq_ramal,
		nr_fone_discado,
		nvl(vl_ligacao,0),
		nvl(nvl(dt_fim,dt_inicio),sysdate),
		nr_atendimento,
		cd_setor_atendimento
	from	tf_log_ligacao
	where	ie_processado		= 0
	  and	cd_estabelecimento	= cd_estabelecimento_p
	  and	nr_sequencia		= nvl(nr_seq_log_lig_p,nr_sequencia)
	  and	nvl(ie_entrada_saida,ie_entrada_saida_w) = ie_entrada_saida_w
	  and 	nr_atendimento_p is null;
*/

cursor c02 is
	select	ie_cobranca,
		nr_seq_proc_interno
	from	tf_regra_cobranca
	where	cd_estabelecimento = cd_estabelecimento_p
	  and	nvl(cd_setor_atendimento, cd_setor_atend_w) = cd_setor_atend_w
	  and	nvl(nr_seq_tipo_lig, nr_seq_tipo_lig_w) = nr_seq_tipo_lig_w
	  and	nvl(ie_tipo_ramal, ie_tipo_ramal_w) = ie_tipo_ramal_w
	  and	nvl(nr_seq_ramal, nr_seq_ramal_w) = nr_seq_ramal_w
	order by nvl(cd_Setor_atendimento, 0), nvl(ie_tipo_ramal, 0), nvl(nr_seq_ramal, 0);
	
cursor c03 is
	select 	nr_sequencia
	from 	tf_log_ligacao
	where	ie_processado = 1
	and 	dt_atualizacao < sysdate - 180
	order by nr_sequencia;
	
begin

ie_entrada_saida_w := nvl(ie_entrada_saida_p,'X');

if	(nr_atendimento_p is not null) then
	open c01 for
	select	nr_sequencia,
		nr_seq_tipo_lig,
		nr_seq_ramal,
		nr_fone_discado,
		nvl(vl_ligacao,0),
		nvl(nvl(dt_fim,dt_inicio),sysdate),
		nr_atendimento,
		cd_setor_atendimento,
		dt_inicio
	from	tf_log_ligacao
	where	ie_processado		= 0
	and	nr_sequencia		= nvl(nr_seq_log_lig_p,nr_sequencia)
	and	nvl(ie_entrada_saida,ie_entrada_saida_w) = ie_entrada_saida_w
	and	nr_atendimento		= nr_atendimento_p;
else
	open c01 for
	select	nr_sequencia,
		nr_seq_tipo_lig,
		nr_seq_ramal,
		nr_fone_discado,
		nvl(vl_ligacao,0),
		nvl(nvl(dt_fim,dt_inicio),sysdate),
		nr_atendimento,
		cd_setor_atendimento,
		dt_inicio
	from	tf_log_ligacao
	where	ie_processado		= 0
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	nr_sequencia		= nvl(nr_seq_log_lig_p,nr_sequencia)
	and	nvl(ie_entrada_saida,ie_entrada_saida_w) = ie_entrada_saida_w;
end if;

select	nvl(max(nr_sequencia),null)
into	nr_seq_log_w
from	tf_log_ligacao;

if	(nr_seq_log_w is not null) then
	select	cd_cgc
	into	cd_cnpj_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_p;
	--open c01;
	loop
	fetch c01 into	nr_seq_log_w,
			nr_seq_tipo_lig_w,
			nr_seq_ramal_w,
			nr_fone_discado_w,
			vl_ligacao_w,
			dt_fim_w,
			nr_atendimento_w,
			cd_setor_atend_w,
			dt_ligacao_integracao_w;
	exit when c01%notfound;
		ds_erro_w	:= null;
		ie_valor_informado_w := 'N';
		if	(vl_ligacao_w <> 0) then
			ie_valor_informado_w := 'S';
		end if;
		begin
		select	nvl(ie_tipo_ramal,0),
			nvl(cd_setor_atend_w, nvl(cd_setor_atendimento,0)),
			nvl(cd_ramal,'')
		into	ie_tipo_ramal_w,
			cd_setor_atend_w,
			cd_ramal_w
		from	ramal_interno
		where	nr_sequencia = nr_seq_ramal_w;
		exception
			when others then
				--ds_erro_w := 'Ramal n�o encontrado!';
				ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(297110);
				
		end;
		if	(ds_erro_w is null) then
			open c02;
			loop
			fetch c02 into	ie_cobranca_w,
					nr_seq_proc_interno_w;
			exit when c02%notfound;
			end loop;
			close c02;
			if	(ie_cobranca_w	= 1) and
				(nr_seq_proc_interno_w is not null) and
				(ie_tipo_ramal_w in (2,9)) then
				begin
				if	(nr_atendimento_w is null) then
					begin
					select	cd_setor_atendimento,
						cd_unidade_basica,
						cd_unidade_compl
					into	cd_setor_atend_w,
						cd_unidade_basica_w,
						cd_unidade_compl_w
					from	unidade_atendimento
					where	nr_ramal = cd_ramal_w;
					exception
						when others then
							--ds_erro_w := 'N�o foi encontrado leito para o ramal';
							ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(297111);
					end;

					if	(cd_setor_atend_w is not null) then
						begin
						select	a.nr_atendimento
						into	nr_atendimento_w
						from	atend_paciente_unidade a,
							atendimento_paciente b
						where	a.nr_atendimento	= b.nr_atendimento
						  and   b.dt_alta is null	-- OS 127384   17/02/2009  Fabr�cio
						  and 	a.cd_setor_atendimento	= cd_setor_atend_w
						  and	a.cd_unidade_basica	= cd_unidade_basica_w
						  and	a.cd_unidade_compl	= cd_unidade_compl_w
						  and	dt_fim_w	between a.dt_entrada_unidade and nvl(a.dt_saida_unidade,sysdate);
						exception
							when others then
								--ds_erro_w := 'N�o foi encontrado atendimento para o ramal';
								ds_erro_w:= WHEB_MENSAGEM_PCK.get_texto(297112);
						end;
					end if;
				end if;
				
				dt_fim_conta_w:= null;
				
				if	(nr_atendimento_w is not null) then
					select 	max(dt_fim_conta)
					into	dt_fim_conta_w
					from 	atendimento_paciente
					where 	nr_atendimento = nr_atendimento_w;
				end if;
				
				if	(ds_erro_w is null) and (dt_fim_conta_w is null) and
					(nr_atendimento_w is not null) then
					dbms_output.put_line('Atendimento'|| nr_atendimento_w);
					select	cd_pessoa_fisica
					into	cd_pessoa_fisica_w
					from	atendimento_paciente
					where	nr_atendimento = nr_atendimento_w;
					dbms_output.put_line('Unidade');
					select	max(nr_seq_interno)
					into	nr_seq_interno_w
					from	atend_paciente_unidade
					where	cd_setor_atendimento	= cd_setor_atend_w
					and	nr_atendimento		= nr_atendimento_w;
					dbms_output.put_line('Conv�nio');
					select	obter_convenio_atendimento(nr_atendimento_w),
						obter_categoria_atendimento(nr_atendimento_w)
					into	cd_convenio_w,
						cd_categoria_w
					from	dual;
					dbms_output.put_line('Proc Interno'||nr_seq_proc_interno_w || 'Conv�nio'||cd_convenio_w);
					cd_procedimento_w := Obter_Cod_Proc_Interno_conv(nr_seq_proc_interno_w, cd_convenio_w, cd_categoria_w, dt_fim_w, cd_estabelecimento_p);
					select	nvl(max(ie_origem_proced),null)
					into	ie_origem_proced_w
					from	proc_interno_conv
					where	nr_seq_proc_interno	= nr_seq_proc_interno_w
					  and	cd_procedimento		= cd_procedimento_w;
					if	(ie_origem_proced_w is null) then
						select	nvl(max(ie_origem_proced),1)
						into	ie_origem_proced_w
						from	proc_interno
						where	nr_sequencia	= nr_seq_proc_interno_w;
					end if;
					select	dt_entrada_unidade
					into	dt_entrada_unidade_w
					from	atend_paciente_unidade
					where	nr_seq_interno = nr_seq_interno_w;
					select	procedimento_paciente_seq.nextval
					into	nr_sequencia_w
					from	dual;
					
					/*ds_obs_w	:= 'Seq.:' || nr_seq_log_w || ' Tipo:' || nr_seq_tipo_lig_w ||
							   ' Ramal:' || cd_ramal_w || ' Fone:' || nr_fone_discado_w ||
							   ' Valor:' || substr(campo_mascara(vl_ligacao_w,2),1,15) || ' Data:' || dt_fim_w;*/
										

										
					ds_obs_w	:= WHEB_MENSAGEM_PCK.get_texto(297224, 'NR_SEQ_LOG='|| nr_seq_log_w || ';' ||
											       'NR_SEQ_TIPO_LIG=' || nr_seq_tipo_lig_w || ';' ||
											       'CD_RAMAL=' || cd_ramal_w || ';' ||
											       'NR_FONE_DISCADO=' || nr_fone_discado_w || ';' ||
											       'VL_LIGACAO=' || substr(campo_mascara(vl_ligacao_w,2),1,15) || ';' ||
											       'DT_FIM=' || dt_fim_w);
					
					dbms_output.put_line('Insert');   
					insert into procedimento_paciente
						(nr_sequencia,
						nr_atendimento,
						dt_entrada_unidade,
						cd_procedimento,
						dt_procedimento,
						qt_procedimento,
						dt_atualizacao,
						nm_usuario,
						cd_convenio,
						cd_categoria,
						cd_acao,
						cd_setor_atendimento,
						ie_origem_proced,
						tx_procedimento,
						cd_cgc_prestador,
						nm_usuario_original,
						nr_seq_atepacu,
						ie_auditoria,
						nr_seq_proc_interno,
						cd_pessoa_fisica,
						vl_procedimento,
						ie_valor_informado,
						ds_observacao,
						nr_fone_integracao,
						dt_ligacao_integracao)
					values (nr_sequencia_w,
						nr_atendimento_w,
						dt_entrada_unidade_w,
						cd_procedimento_w,
						dt_fim_w,
						1,
						dt_fim_w,
						nvl(nm_usuario_p,'Tarifador'),
						cd_convenio_w,
						cd_categoria_w,
						1,
						cd_setor_atend_w,
						ie_origem_proced_w,
						100,
						cd_cnpj_w,
						nvl(nm_usuario_p,'Tarifador'),
						nr_seq_interno_w,
						'N',
						nr_seq_proc_interno_w,
						cd_pessoa_fisica_w,
						vl_ligacao_w,
						ie_valor_informado_w,
						ds_obs_w,
						nr_fone_discado_w,
						dt_ligacao_integracao_w);
					dbms_output.put_line('Pre�o');
					atualiza_preco_procedimento(nr_sequencia_w, cd_convenio_w, 'Tarifador');
					
					--Inclus�o do tratamento da Regra de USO
					obter_regra_qtde_proc_exec(nr_atendimento_w, cd_procedimento_w, ie_origem_proced_w, 0, dt_fim_w, null,
							ie_acao_excesso_w, qt_excedida_w, ds_erro_w, cd_convenio_excesso_w, cd_categoria_excesso_w, nr_seq_proc_interno_w, cd_categoria_w, NULL,
							0, 0, 0, cd_setor_atend_w,null);
							
					if	(ie_acao_excesso_w = 'P') and (qt_excedida_w   > 0) then
					
						obter_convenio_particular_pf(cd_estabelecimento_p, cd_convenio_w, cd_pessoa_fisica_w, dt_fim_w, cd_convenio_glosa_w, cd_categoria_glosa_w);
						
						update	procedimento_paciente
						set	nr_interno_conta	= null,
							cd_convenio		= cd_convenio_glosa_w,
							cd_categoria		= cd_categoria_glosa_w
						where	nr_sequencia 		= nr_sequencia_w;
					
						if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
					
						atualiza_preco_procedimento(nr_sequencia_w, cd_convenio_glosa_w, nm_usuario_p);					
						Ajustar_Conta_Vazia(nr_atendimento_w, nm_usuario_p);						
					
					end if;
					
					if 	(ie_acao_excesso_w = 'C') and
						(qt_excedida_w > 0) and
						(cd_convenio_excesso_w is not null) and
						(cd_categoria_excesso_w is not null) then
						
						update	procedimento_paciente
						set	nr_interno_conta	= null,
							cd_convenio		= cd_convenio_excesso_w,
							cd_categoria		= cd_categoria_excesso_w
						where	nr_sequencia 		= nr_sequencia_w;
						
						atualiza_preco_procedimento(nr_sequencia_w, cd_convenio_excesso_w, nm_usuario_p);
						Ajustar_Conta_Vazia(nr_atendimento_w, nm_usuario_p);
						
					end if;
					
					select 	max(nr_interno_conta)
					into	nr_interno_conta_w
					from 	procedimento_paciente
					where 	nr_sequencia = nr_sequencia_w;
					
					update	tf_log_ligacao
					set	nr_atendimento	= nr_atendimento_w,
						nr_interno_conta = nr_interno_conta_w,
						ie_processado	= 1,
						--ds_erro		= 'Gerado para conta. Sequencia:' || to_char(nr_sequencia_w),
						ds_erro		= WHEB_MENSAGEM_PCK.get_texto(297123) || to_char(nr_sequencia_w),
						dt_atualizacao	= sysdate
					where	nr_sequencia	= nr_seq_log_w;
					if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;			
				else
					update	tf_log_ligacao
					set	ds_erro		= ds_erro_w,
						dt_atualizacao	= sysdate
					where	nr_sequencia	= nr_seq_log_w;
				end if;
				exception
					when others then   
						--ds_erro_w := substr('Erro ao lan�ar cobran�a.' || SQLErrM(SQLCode),1,255);
						ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(297124) || SQLErrM(SQLCode),1,255);						
						update	tf_log_ligacao
						set	ds_erro		= ds_erro_w,
							dt_atualizacao	= sysdate
						where	nr_sequencia	= nr_seq_log_w;
				end;
			else
				update	tf_log_ligacao
				set	--ds_erro		= 'Regra de cobran�a n�o encontrada',   
					ds_erro		= WHEB_MENSAGEM_PCK.get_texto(297125),
					dt_atualizacao	= sysdate,
					ie_processado		= 2
				where	nr_sequencia	= nr_seq_log_w;
			end if;
		else
			update	tf_log_ligacao
			set	ds_erro		= ds_erro_w,
				dt_atualizacao	= sysdate,
				ie_processado		= 2
			where	nr_sequencia	= nr_seq_log_w;
		end if;
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	end loop;
	close c01;
end if;

/* Rotina para lipar a tabela TF_LOG_LIGACAO de registros antigos j� gerados para a conta*/
if	(nr_atendimento_p is null) then
	qt_item_w:= 0;
	open C03;
	loop
	fetch C03 into	
		nr_seq_item_w;
	exit when C03%notfound;
		begin
		
		qt_item_w:= qt_item_w + 1;
	
		begin
		delete from tf_log_ligacao
		where nr_sequencia = nr_seq_item_w;
		exception
			when others then
				update	tf_log_ligacao
				set	ie_processado = 2
				where	nr_sequencia = nr_seq_item_w;

				if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;		
		end;
	
		if	(qt_item_w > 5000) then
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
			qt_item_w	:= 0;
		end if;
	
		end;
	end loop;
	close C03;
end if;
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end TF_Processar_Ligacao;
/
