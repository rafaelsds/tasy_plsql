create or replace
procedure	integra_unid_medida_protheus(
			nr_sequencia_p		number,
			nm_usuario_p		varchar2) is 

cd_cadastro_w		varchar2(20);
ds_cadastro_w		varchar2(80);
ie_operacao_w			varchar2(15);
qt_registros_w			number(10);
ie_erro_w			varchar2(1) := 'N';

begin

select	cd_cadastro,
	ds_cadastro,
	ie_operacao
into	cd_cadastro_w,
	ds_cadastro_w,
	ie_operacao_w
from	w_protheus_cadastro
where	nr_sequencia = nr_sequencia_p
and	tp_cadastro = 'UM';

if	(cd_cadastro_w is null) then
	gravar_log_protheus('ERRO', 'UM', WHEB_MENSAGEM_PCK.get_texto(310127) , nm_usuario_p); --O c�digo da unidade de medida recebida do Protheus est� vazia.
	ie_erro_w := 'S';
end if;

if	(ds_cadastro_w is null) then
	gravar_log_protheus('ERRO', 'UM', WHEB_MENSAGEM_PCK.get_texto(310128) , nm_usuario_p); --A descri��o da unidade de medida recebida do Protheus est� vazia.
	ie_erro_w := 'S';
end if;

if	(ie_erro_w = 'N') and
	(cd_cadastro_w is not null) and
	(ds_cadastro_w is not null) then
	begin
	
	if	(ie_operacao_w in ('I','A')) then
	
		select	count(*)
		into	qt_registros_w
		from	unidade_medida
		where	cd_sistema_ant = cd_cadastro_w;
	
		if	(qt_registros_w = 0) and
			(ie_operacao_w = 'I') then

			insert into unidade_medida(
				cd_unidade_medida,
				ds_unidade_medida,
				ie_situacao,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_fracao_dose,
				ie_permite_fracionar,
				ie_adm_diluicao,
				ie_mult_h_aplic,
				cd_sistema_ant)
			values (
				cd_cadastro_w,
				substr(cd_cadastro_w || ' - ' || ds_cadastro_w,1,40),
				'A',
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				'N',
				'S',
				'N',
				'N',
				cd_cadastro_w);
		end if;
		
		if	(qt_registros_w > 0) and
			(ie_operacao_w = 'A') then
			
			update	unidade_medida
			set	ds_unidade_medida		= substr(cd_cadastro_w || ' - ' || ds_cadastro_w,1,40),
				ie_situacao		= 'A',
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p 
			where	cd_sistema_ant = cd_cadastro_w;
		end if;
		
	elsif	(ie_operacao_w = 'E') then
	
		update	unidade_medida
		set	ie_situacao = 'I',
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p
		where	cd_sistema_ant = cd_cadastro_w;
	end if;

	end;
end if;
commit;
end integra_unid_medida_protheus;
/
