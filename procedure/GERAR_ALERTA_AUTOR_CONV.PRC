create or replace
procedure gerar_alerta_autor_conv(nm_usuario_p		Varchar2) is 

cd_pessoa_fisica_w		varchar2(10);
cd_pessoa_fisica_ww		varchar2(10);
nm_pessoa_w			varchar2(60);
nr_sequencia_autor_w		number(10,0);
ds_alerta_w			varchar2(2000);
dt_fim_vigencia_w		date;
nm_usuario_w			varchar2(15);

Cursor C01 is
	select	a.nr_sequencia,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,60),
		nvl(c.cd_medico_resp,a.cd_medico_solicitante),
		c.cd_pessoa_fisica,
		a.dt_fim_vigencia,
		a.nm_usuario
	from	autorizacao_convenio a,
		estagio_autorizacao b,
		atendimento_paciente c
	where	a.nr_atendimento = c.nr_atendimento
	and	a.nr_seq_estagio = b.nr_sequencia
	and	a.ie_tipo_autorizacao = '2'
	and	a.dt_fim_vigencia is null
	and	b.ie_interno not in ('70','90','10')
	and	a.dt_autorizacao >= (sysdate - 30)
	--and	 a.dt_fim_vigencia between (sysdate - (24 * 1/24)) and (sysdate + (24 * 1/24))
	--and	a.dt_fim_vigencia > sysdate
	and	not exists(	select	1
				from	autor_convenio_alerta x
				where	x.nr_sequencia_autor = a.nr_sequencia
				and	sysdate between x.dt_alerta and x.dt_fim_alerta);

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_autor_w,
	nm_pessoa_w,
	cd_pessoa_fisica_ww,
	cd_pessoa_fisica_w,
	dt_fim_vigencia_w,
	nm_usuario_w;
exit when C01%notfound;
	begin
	
	ds_alerta_w := wheb_mensagem_pck.get_texto(307598, 'DT_FIM_VIGENCIA_W=' || to_char(dt_fim_vigencia_w,'dd/mm/yyyy hh24:mi:ss'));
				/*
					Existe necessidade de prorrogação pendente de autorização
					Vigência final: #@DT_FIM_VIGENCIA_W#@
				*/
				
	insert	into	autor_convenio_alerta (
			cd_pessoa_alerta,         
			ds_alerta,                
			dt_alerta,                
			dt_atualizacao,           
			dt_atualizacao_nrec,      
			dt_fim_alerta,            
			dt_liberacao,             
			ie_situacao,              
			nm_usuario,               
			nm_usuario_nrec,           
			nr_sequencia,             
			nr_sequencia_autor)
	values	(	cd_pessoa_fisica_w,         
			ds_alerta_w,                
			sysdate,                
			sysdate,           
			sysdate,      
			trunc(sysdate) + 86399/86400,
			null,             
			'A',              
			nm_usuario_w,
			nm_usuario_p,
			autor_convenio_alerta_seq.nextval,             
			nr_sequencia_autor_w);
	end;
end loop;
close C01;	
	
	
commit;

end gerar_alerta_autor_conv;
/