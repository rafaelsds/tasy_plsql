create or replace
procedure patient_query_add (	nr_sequencia_p	number,
								ie_opcao_p		varchar2,
								nm_usuario_p	Varchar2) is 


DS_PATIENT_PROVIDER_w 			varchar2(255);
DS_PATIENT_NAME_GIVEN_w			varchar2(255); 
DS_PATIENT_NAME_FAMILY_w		varchar2(255);
DS_PATIENT_STATUS_w				varchar2(255);
DS_PATIENT_IDS_ISSUERNAME_w		varchar2(255); 
DS_PATIENT_IDS_ISSUERCODE_w		varchar2(255); 
DS_PATIENT_IDS_ID_w				varchar2(255); 
DS_GENDER_w						varchar2(255);
DS_GENDERTEXT_w					varchar2(255);
DS_BIRTHDATE_w					varchar2(255);
DS_ADDRESS_COUNTRY_w			varchar2(255);
DS_ADDRESS_CITY_w				varchar2(255);
DS_ADDRESS_STREET_w				varchar2(255);
DS_ADDRESS_POSTALCODE_w			varchar2(255);
DS_IDENTIFIERS_ISSUERID_w		varchar2(255);
DS_IDENTIFIERS_ID_w				varchar2(255);
nm_pessoa_fisica_w				varchar2(60);


nr_seq_issuer_w					number(10);
cd_pessoa_fisica_w				varchar2(10);
nr_seq_query_pf_w				number(10);
nr_seq_complemento_w			number(10);
nr_seq_pais_w					number(10);

cd_pessoa_complemento_w			number(10);

ds_patient_cpf_w				patient_query.ds_patient_cpf%type;
ds_patient_cns_w				patient_query.ds_patient_cns%type;
nr_seq_query_issuer_w			patient_query_issuer.nr_sequencia%type;
ds_user_id_w					patient_query.ds_user_id%type;
ds_issuer_w						patient_query.ds_issuer%type;
ds_patient_oid_name_w			patient_query_pf.ds_patient_oid_name%type;
ds_patient_oid_code_w			patient_query_pf.ds_patient_oid_code%type;


begin

if ( nr_sequencia_p is not null ) then


	select  max(DS_PATIENT_PROVIDER), 
			max(DS_PATIENT_NAME_GIVEN), 
			max(DS_PATIENT_NAME_FAMILY),
			max(DS_PATIENT_STATUS),
			max(DS_PATIENT_IDS_ISSUERNAME), 
			max(DS_PATIENT_IDS_ISSUERCODE), 
			max(DS_PATIENT_IDS_ID), 
			max(DS_GENDER),
			max(DS_GENDERTEXT),
			max(DS_BIRTHDATE),
			max(DS_ADDRESS_COUNTRY),
			max(DS_ADDRESS_CITY),
			max(DS_ADDRESS_STREET),
			max(DS_ADDRESS_POSTALCODE),
			max(DS_IDENTIFIERS_ISSUERID),
			max(DS_IDENTIFIERS_ID),
			max(ds_patient_cpf),
			max(ds_patient_cns),
			max(ds_user_id),
			max(ds_issuer)
	into	DS_PATIENT_PROVIDER_w, 		
			DS_PATIENT_NAME_GIVEN_w,		 
			DS_PATIENT_NAME_FAMILY_w,	
			DS_PATIENT_STATUS_w,			
			DS_PATIENT_IDS_ISSUERNAME_w,	 
			DS_PATIENT_IDS_ISSUERCODE_w,	 
			DS_PATIENT_IDS_ID_w,			 
			DS_GENDER_w,					
			DS_GENDERTEXT_w,				
			DS_BIRTHDATE_w,				
			DS_ADDRESS_COUNTRY_w,		
			DS_ADDRESS_CITY_w,			
			DS_ADDRESS_STREET_w,			
			DS_ADDRESS_POSTALCODE_w,		
			DS_IDENTIFIERS_ISSUERID_w,	
			DS_IDENTIFIERS_ID_w,
			ds_patient_cpf_w,
			ds_patient_cns_w,
			ds_user_id_w,
			ds_issuer_w
	from   	patient_query
	where   nr_sequencia = nr_sequencia_p;
	
	If ( DS_PATIENT_IDS_ISSUERCODE_w is not null) then	
	
		Select  max(nr_sequencia)
		into	nr_seq_issuer_w
		from	patient_query_issuer
		where	upper(DS_PATIENT_IDS_ISSUERCODE) = upper(DS_PATIENT_IDS_ISSUERCODE_w);
		
	
		if (nvl(nr_seq_issuer_w,0) = 0 ) then
		
			
			Select  patient_query_issuer_seq.nextval
			into	nr_seq_issuer_w
			from 	dual;	

			insert into patient_query_issuer ( nr_sequencia,
											   dt_atualizacao,
											   dt_Atualizacao_nrec,
											   nm_usuario,
											   nm_usuario_nrec,
											   ds_patient_ids_issuername,
											   ds_patient_ids_issuercode)
										values ( nr_seq_issuer_w,
										         sysdate,
												 sysdate,
												 nm_usuario_p,
												 nm_usuario_p,
												 DS_PATIENT_IDS_ISSUERNAME_w,
												 DS_PATIENT_IDS_ISSUERCODE_w);
												 
           Commit;
		
		end if;
		
		
		if ( ds_patient_cpf_w is not null ) then
		
			ds_patient_oid_name_w := '2.16.840.1.113883.13.237';
			ds_patient_oid_code_w := ds_patient_cpf_w;
		
			Select  max(cd_pessoa_fisica),
					max(nr_sequencia)				
			into	cd_pessoa_fisica_w,
					nr_Seq_query_pf_w
			from	patient_query_pf
			where	ds_patient_oid_name = ds_patient_oid_name_w
			and		upper(DS_PATIENT_OID_CODE) = upper(ds_patient_cpf_w);			
		
		end if;
		
		if ( cd_pessoa_fisica_w is null and ds_patient_cns_w is not null) then
		
			ds_patient_oid_name_w := '2.16.840.1.113883.13.236';
			ds_patient_oid_code_w := ds_patient_cns_w;
	
			Select  max(cd_pessoa_fisica),
					max(nr_sequencia)					
			into	cd_pessoa_fisica_w,
					nr_Seq_query_pf_w
			from	patient_query_pf
			where	ds_patient_oid_name = ds_patient_oid_name_w
			and		upper(DS_PATIENT_OID_CODE) = upper(ds_patient_cns_w);	
	
	
		end if;
		
		if ( cd_pessoa_fisica_w is null and ds_user_id_w is not null) then
		
			ds_patient_oid_name_w := ds_issuer_w;
			ds_patient_oid_code_w := ds_user_id_w;
	
			Select  max(cd_pessoa_fisica),
					max(nr_sequencia)					
			into	cd_pessoa_fisica_w,
					nr_Seq_query_pf_w
			from	patient_query_pf
			where	ds_patient_oid_name = ds_issuer_w
			and		upper(DS_PATIENT_OID_CODE) = upper(ds_user_id_w);
		
		end if;
		
		
		if  (nvl(cd_pessoa_fisica_w,0)  = 0 ) then
		
			if ( ds_patient_cpf_w is not null ) then
		
				Select  max(cd_pessoa_fisica)
				into	cd_pessoa_fisica_w
				from	pessoa_fisica
				where 	upper(nr_cpf) = upper(ds_patient_cpf_w);
				
			end if;
			
			
			if ( cd_pessoa_fisica_w is null and ds_patient_cns_w is not null ) then
		
				Select  max(cd_pessoa_fisica)
				into	cd_pessoa_fisica_w
				from	pessoa_fisica
				where 	upper(nr_cartao_nac_sus) = upper(ds_patient_cns_w);
				
			end if;
		
		
			if  (cd_pessoa_fisica_w is null) then
		
				Select   substr(ds_patient_name_given_w||'  '||ds_patient_name_family_w,1,60)
				into	 nm_pessoa_fisica_w
				from	 dual;
			
					
				Select  pessoa_fisica_seq.nextval
				into	cd_pessoa_fisica_w
				from	dual;
				
				insert into pessoa_fisica (cd_pessoa_fisica,
										   dt_atualizacao,
										   dt_Atualizacao_nrec,
										   nm_usuario,
										   nm_usuario_nrec,
										   nm_pessoa_fisica,
										   dt_nascimento,
										   ie_sexo,
										   cd_sistema_ant,
										   ie_tipo_pessoa,
										   nr_cpf,
										   nr_cartao_nac_sus)
									values  ( cd_pessoa_fisica_w, 
											  sysdate,
											  sysdate,
											  nm_usuario_p,
											  nm_usuario_p,
											  nm_pessoa_fisica_w,
											  to_date(ds_birthdate_w,'dd/mm/yyyy'),
											  ds_gender_w,
											  substr(ds_patient_provider_w,1,20),
											  1,
											  ds_patient_cpf_w,
											  ds_patient_cns_w);
											  
				commit;
				
			end if;
			
			insert into patient_query_pf 	(  nr_sequencia,
											   dt_atualizacao,
											   dt_Atualizacao_nrec,
											   nm_usuario,
											   nm_usuario_nrec,
											   ds_patient_ids_id,
											   cd_pessoa_fisica,
											   nr_seq_issuer,
											   ds_patient_oid_name,
											   ds_patient_oid_code) 
									values	(  patient_query_pf_seq.nextval,
											   sysdate,
											   sysdate,
											   nm_usuario_p,
										       nm_usuario_p,
											   ds_identifiers_id_w,
											   cd_pessoa_fisica_w,
											   nr_seq_issuer_w,
											   ds_patient_oid_name_w,
											   ds_patient_oid_code_w);
											   
           commit;
			
			
		end if;
		
		if (ie_opcao_p = '2') then
		
			update 	pessoa_fisica
			set		dt_atualizacao  =  sysdate,
					nm_usuario 		=  nm_usuario_p,
					dt_nascimento 	=  nvl(to_date(ds_birthdate_w,'dd/mm/yyyy'),dt_nascimento),
					ie_sexo 		=  nvl(ds_gender_w,ie_sexo),
					cd_sistema_ant  =  nvl(substr(ds_patient_provider_w,1,20),cd_sistema_ant),
					nr_cpf  		=  nvl(substr(ds_patient_cpf_w,1,20),nr_cpf),
					nr_cartao_nac_sus =  nvl(substr(ds_patient_cns_w,1,20),nr_cartao_nac_sus)
			where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		
			commit;
		
			Select  max(nr_sequencia)
			into	nr_seq_complemento_w
			from    compl_pessoa_fisica
			where   cd_pessoa_fisica = cd_pessoa_fisica_w
			and		ie_tipo_complemento = 1;
			
			if	(nvl(nr_seq_complemento_w,0) = 0 ) then
				
				select  max(nr_sequencia)
				into	nr_seq_pais_w
				from	pais
				where	upper(SG_PAIS)  like upper((ds_address_country_w||'%'));
				
				
				Select  nvl(max(nr_sequencia),0) + 1
				into	cd_pessoa_complemento_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica = cd_pessoa_fisica_w;
			
				Insert into   compl_pessoa_fisica (	 nr_sequencia,
													 dt_atualizacao,
												     dt_Atualizacao_nrec,
												     nm_usuario,
												     nm_usuario_nrec,
													 cd_pessoa_fisica,
													 nm_contato,
													 nr_seq_pais,
													 ds_municipio,
													 ds_endereco,
													 cd_cep,
													 ie_tipo_complemento) 
											values ( cd_pessoa_complemento_w,
													 sysdate,
													 sysdate,
													 nm_usuario_p,
													 nm_usuario_p,
													 cd_pessoa_fisica_w,
													 nm_pessoa_fisica_w,
													 nr_seq_pais_w,
													 substr(ds_address_city_w,1,40),
													 substr(ds_address_street_w,1,100),
													 substr(ds_address_postalcode_w,1,15),
													 1);
													 
				commit;
			
			
			else			
				
				update  	compl_pessoa_fisica
				set		 	dt_atualizacao = sysdate,
							nm_usuario = nm_usuario_p,							
						    nr_seq_pais	= nvl(nr_seq_pais_w,nr_seq_pais),
						    ds_municipio = nvl(substr(ds_address_city_w,1,40),ds_municipio),
						    ds_endereco =  nvl(substr(ds_address_street_w,1,100),ds_endereco),
						    cd_cep		=  nvl(substr(ds_address_postalcode_w,1,15),cd_cep)
				where		nr_sequencia = nr_seq_complemento_w
				and		    cd_pessoa_fisica = cd_pessoa_fisica_w ;
			
				commit;
			
			
			end if;
		
		
		elsif (ie_opcao_p = '3') then
		
				-- Necess�rio verificar como o arquivo ser� gerado
			
				insert into GED_ATENDIMENTO( nr_sequencia,
											 nm_usuario,
											 dt_atualizacao,
											 DT_REGISTRO,
											 ds_arquivo,
											 dt_liberacao)
									values ( GED_ATENDIMENTO_seq.nextval,
											 nm_usuario_p,
											 sysdate,
											 sysdate,
											 '',
											 sysdate);
											 
				commit;
			
		
		
		end if;
		
	
	end if;
	

end if;

end patient_query_add;
/