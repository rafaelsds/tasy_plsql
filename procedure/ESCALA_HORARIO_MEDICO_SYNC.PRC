create or replace trigger ESCALA_HORARIO_MEDICO_SYNC
before insert or update on ESCALA_HORARIO_MEDICO
for each row

declare

begin

begin
if (nvl(:old.HR_INICIAL,'X') <> nvl(:new.HR_INICIAL,'X')) then
 if (:new.HR_INICIAL is not null) then
  :new.HR_INICIAL_DATE := pkg_date_utils.get_Time(sysdate,:new.HR_INICIAL);
 else
  :new.HR_INICIAL_DATE := null;
 end if;
elsif (nvl(:old.HR_INICIAL_DATE,pkg_date_utils.get_Time('')) <> nvl(:new.HR_INICIAL_DATE,pkg_date_utils.get_Time(''))) then
 :new.HR_INICIAL := lpad(pkg_date_utils.extract_field('HOUR',:new.HR_INICIAL_DATE),2,0) || ':' || lpad(pkg_date_utils.extract_field('MINUTE',:new.HR_INICIAL_DATE),2,0);
end if;

if (nvl(:old.HR_FINAL,'X') <> nvl(:new.HR_FINAL,'X')) then
 if (:new.HR_FINAL is not null) then
  :new.HR_FINAL_DATE := pkg_date_utils.get_Time(sysdate,:new.HR_FINAL);
 else
  :new.HR_FINAL_DATE := null;
 end if;
elsif (nvl(:old.HR_FINAL_DATE,pkg_date_utils.get_Time('')) <> nvl(:new.HR_FINAL_DATE,pkg_date_utils.get_Time(''))) then
 :new.HR_FINAL := lpad(pkg_date_utils.extract_field('HOUR',:new.HR_FINAL_DATE),2,0) || ':' || lpad(pkg_date_utils.extract_field('MINUTE',:new.HR_FINAL_DATE),2,0);
end if;
exception
when others then
 null;
end;

end;
/