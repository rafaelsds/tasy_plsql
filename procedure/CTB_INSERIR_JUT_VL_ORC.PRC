create or replace
procedure ctb_inserir_jut_vl_orc(
			ds_justificativa_p		Varchar2,
			nm_usuario_p			varchar2,
			nr_seq_orcamento_p		number) is 

begin


update	ctb_orcamento
set		ds_justificativa = ds_justificativa_p,
		nm_usuario 		 = nm_usuario_p 
where	nr_sequencia 	 = nr_seq_orcamento_p;


commit;

end ctb_inserir_jut_vl_orc;
/