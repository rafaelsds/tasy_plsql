create or replace
procedure Gravar_Mat_Atend_Pac_Ajuste(
                        nr_atendimento_p                number,
                        nr_interno_conta_p              number,
                        cd_material_p                   number,
                        dt_atendimento_p                date,
                        cd_convenio_p                   number,
                        cd_categoria_p                  varchar2,
                        nr_seq_atepacu_p                number,
                        nm_usuario_p                    varchar2,
                        qt_material_p                   number,
                        cd_local_estoque_p              number,
                        cd_acao_p                       varchar2,
                        nr_doc_convenio_p               varchar2,
                        ie_valor_informado_p            varchar2,
                        nr_seq_motivo_p                 number,
			nr_seq_auditoria_p		number default null,
			ie_agrupado_p			varchar2,
			cd_unidade_medida_p		varchar2,
                        nr_sequencia_p                  out number,
			cd_material_tiss_p		varchar2 default null) is

nr_sequencia_w			number(15);
cd_setor_atendimento_w		number(5);
dt_entrada_unidade_w		date;
cd_unidade_medida_w		varchar2(30);
cd_acao_w			varchar2(1):= '1';
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_estab_logado_w		estabelecimento.cd_estabelecimento%type;
ie_estabelecimento_conta_w	parametro_faturamento.ie_estabelecimento_conta%type;
ie_calcular_mat_auditado_w	parametro_faturamento.ie_calcular_mat_auditado%type;
ie_auditoria_w			material_atend_paciente.ie_auditoria%type;

begin

cd_acao_w:= nvl(cd_acao_p,'1');
if      (qt_material_p < 0) then
        cd_acao_w:= '2';
end if;

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select  cd_setor_Atendimento,
        dt_entrada_unidade
into    cd_setor_atendimento_w,
        dt_entrada_unidade_w
from    atend_paciente_unidade
where   nr_seq_interno  = nr_seq_atepacu_p;

select  material_atend_paciente_seq.nextval
into    nr_sequencia_w
from    dual;

cd_unidade_medida_w	:= cd_unidade_medida_p;

if	(cd_unidade_medida_w is null) then
	select  cd_unidade_medida_consumo
	into    cd_unidade_medida_w
	from    material
	where   cd_material     = cd_material_p;
end if;

select	max(wheb_usuario_pck.get_cd_estabelecimento)
into	cd_estab_logado_w
from	dual;

select	nvl(max(ie_estabelecimento_conta), 'A')
into	ie_estabelecimento_conta_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estab_logado_w;

if	(ie_estabelecimento_conta_w = 'L') then
	cd_estabelecimento_w := cd_estab_logado_w;
end if;

select	nvl(max(ie_calcular_mat_auditado),'S')
into	ie_calcular_mat_auditado_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_w;

ie_auditoria_w	:= 'S';

if	(nvl(ie_calcular_mat_auditado_w,'S') = 'N') then
	ie_auditoria_w	:= 'N';
end if;

insert into material_atend_paciente(
                        nr_sequencia,
                        cd_material,
                        dt_atendimento,
                        cd_convenio,
                        cd_categoria,
                        nr_seq_atepacu,
                        cd_setor_atendimento,
                        dt_entrada_unidade,
                        qt_material,
                        cd_local_estoque,
                        dt_Atualizacao,
                        nm_usuario,
                        nr_atendimento,
                        cd_unidade_medida,
                        cd_acao,
                        ie_valor_informado,
                        nr_interno_conta,
                        ie_auditoria,
                        nr_doc_convenio,
			cd_material_tiss,
			nr_seq_cor_exec)
        values  (       nr_sequencia_w,
                        cd_material_p,
                        nvl(dt_atendimento_p,sysdate),
                        cd_convenio_p,
                        cd_categoria_p,
                        nr_seq_atepacu_p,
                        cd_setor_atendimento_w,
                        dt_entrada_unidade_w,
                        qt_material_p,
                        cd_local_estoque_p,
                        sysdate,
                        nm_usuario_p,
                        nr_atendimento_p,
                        cd_unidade_medida_w,
                        nvl(cd_acao_w,'1'),
                        nvl(ie_valor_informado_p,'N'),
                        nr_interno_conta_p,
                        ie_auditoria_w, -- Se o par�metro ie_calcular_mat_auditado_w estiver para 'N', insere primeiramente como N�o auditado, para atualizar os valores.
                        nr_doc_convenio_p,
			cd_material_tiss_p,
			6697);

nr_sequencia_p          := nr_sequencia_w;

atualiza_preco_material(nr_sequencia_w, nm_usuario_p);

if	(ie_auditoria_w	= 'N') then
	update	material_atend_paciente
	set	ie_auditoria = 'S'
	where	nr_sequencia = nr_sequencia_w;
end if;

Atualizar_Lista_Itens_Audit(nr_interno_conta_p, nr_sequencia_w, 1, qt_material_p, nm_usuario_p,nr_seq_motivo_p, nr_seq_auditoria_p, ie_agrupado_p);

commit;

end Gravar_Mat_Atend_Pac_Ajuste;
/