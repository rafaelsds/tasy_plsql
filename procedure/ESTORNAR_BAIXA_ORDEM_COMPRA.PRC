create or replace
procedure estornar_baixa_ordem_compra(
				nr_ordem_compra_p	number,
				nr_item_oci_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

				
nr_nota_fiscal_w				nota_fiscal.nr_nota_fiscal%type;
ie_permite_estornar_com_nf_w			funcao_param_usuario.vl_parametro%type;
ds_lista_nota_w					varchar2(4000) := '';
ds_mensagem_w					varchar2(4000) := '';
				
cursor c01 is
select	distinct
	nr_nota_fiscal
from	ordem_compra_nota_fiscal
where	nr_ordem_compra	= nr_ordem_compra_p
and	nr_item_oci	= nr_item_oci_p;
				
begin

select	max(obter_valor_param_usuario(917, 78, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p))
into	ie_permite_estornar_com_nf_w
from	dual;

consiste_estornar_baixa_oc(nr_ordem_compra_p, nr_item_oci_p, cd_estabelecimento_p, nm_usuario_p);

open C01;
loop
fetch C01 into	
	nr_nota_fiscal_w;
exit when C01%notfound;
	begin
	ds_lista_nota_w := substr(ds_lista_nota_w || nr_nota_fiscal_w || ', ',1,4000);
	end;
end loop;
close C01;

update	ordem_compra_item
set	qt_material_entregue = 0,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_ordem_compra	= nr_ordem_compra_p
and	nr_item_oci	= nr_item_oci_p;

update	ordem_compra_item_entrega
set	dt_real_entrega	= null,
	qt_real_entrega	= 0,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_ordem_compra	= nr_ordem_compra_p
and	nr_item_oci	= nr_item_oci_p;

if	(ie_permite_estornar_com_nf_w <> 'V') then
	delete	from ordem_compra_nota_fiscal
	where	nr_ordem_compra	= nr_ordem_compra_p
	and	nr_item_oci	= nr_item_oci_p;
end if;	

update	ordem_compra
set	dt_baixa		= null,
	nr_seq_motivo_cancel	= null,
	nr_seq_motivo_baixa = null
where	nr_ordem_compra	= nr_ordem_compra_p;

ds_mensagem_w := WHEB_MENSAGEM_PCK.get_texto(300311,'NR_ITEM_OCI_P=' || nr_item_oci_p);

if	(ds_lista_nota_w is not null) then
	ds_mensagem_w := ds_mensagem_w || chr(13) || chr(10) || WHEB_MENSAGEM_PCK.get_texto(300313,'DS_LISTA_NOTA_W=' || substr(ds_lista_nota_w,1,length(ds_lista_nota_w) -2));
end if;

inserir_historico_ordem_compra(
	nr_ordem_compra_p,
	'S',
	WHEB_MENSAGEM_PCK.get_texto(300307),      	
	ds_mensagem_w,
	nm_usuario_p);

commit;

end estornar_baixa_ordem_compra;
/
