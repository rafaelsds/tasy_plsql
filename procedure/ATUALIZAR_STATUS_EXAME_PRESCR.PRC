create or replace
procedure atualizar_status_exame_prescr	(nr_prescricao_p	number,
					nr_seq_prescr_p		number,
					ie_opcao_p		varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is
					
nr_seq_agenda_w			number(10);
ie_atualiza_status_agenda_w	varchar2(1);
nr_atendimento_w		number(10);
dt_atendido_w			date;
dt_atendimento_w		date;
ie_status_w			varchar2(15);
ie_utiliza_prepado_w		varchar2(1);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_proc_interno_W		number(10);
ie_gerar_passagem_setor_w	varchar2(10);
cd_setor_atendimento_w		number(5);
ie_status_atualizar_w		varchar2(10);

begin

obter_param_usuario(942, 115, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_utiliza_prepado_w);		

if	(nvl(nr_prescricao_p,0) > 0) then

	if	(ie_opcao_p	= 'I') then
	
		select 	max(cd_procedimento),
			max(ie_origem_proced),
			max(nr_seq_proc_interno),
			max(cd_setor_atendimento)
		into	cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_W,
			cd_setor_atendimento_w
		from	prescr_procedimento
		where	 nr_prescricao        	=  nr_prescricao_p
		and      nr_sequencia         	=  nr_seq_prescr_p;
	
		update   prescr_procedimento     
		set      ie_status_execucao   	=  15,
			 dt_inicio_exame      	=  sysdate,
			 nm_usuario		=  nm_usuario_p
		where    nr_prescricao        	=  nr_prescricao_p
		and      nr_sequencia         	=  nr_seq_prescr_p
		and      ie_status_execucao	in (10,11,13,14,17,18);
		
		obter_param_usuario(942, 90, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_status_agenda_w);
		
		
		if	(ie_atualiza_status_agenda_w = 'S') then
		
			select	nvl(max(nr_seq_agenda),0)
			into	nr_seq_agenda_w
			from	prescr_medica
			where	nr_prescricao        	=  nr_prescricao_p;


			if	(nr_seq_agenda_w = 0) then

				select	max(nr_atendimento)
				into	nr_atendimento_w
				from	prescr_medica
				where	nr_prescricao = nr_prescricao_p;

				select	nvl(max(nr_sequencia),0)
				into	nr_seq_agenda_w
				from	agenda_paciente
				where	nr_atendimento        	=  nr_atendimento_w
				and	ie_status_agenda  not in ('B','C','E')
				and	((cd_procedimento = cd_procedimento_w and	ie_origem_proced = ie_origem_proced_w) or (nr_seq_proc_interno = nr_seq_proc_interno_w));

			end if;
			
			if	(nr_seq_agenda_w > 0) then
			
				update 	agenda_paciente
				set    	ie_status_agenda = 'EE',
					dt_em_exame	 = sysdate
				where	nr_sequencia	= nr_seq_agenda_w
				and	ie_status_agenda  not in ('B','C','E');
			
			end if;
		end if;
		
		obter_param_usuario(942, 331, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_passagem_setor_w);
		
		if 	(ie_gerar_passagem_setor_w = 'S') then
		
				select	max(nr_atendimento)
				into	nr_atendimento_w
				from	prescr_medica
				where	nr_prescricao = nr_prescricao_p;
				
				
				if (nr_atendimento_w is not null) then				
								
					gerar_passagem_setor_atend(nr_atendimento_w, cd_setor_atendimento_w, sysdate, 'S', nm_usuario_p);				
				
				end if;
		
		end if;
		
	end if;


	if	(ie_opcao_p	= 'D') then
	
		ie_status_atualizar_w := obter_status_anterior_presc(nr_prescricao_p, nr_seq_prescr_p, 15);
	
		update   prescr_procedimento     
		set      ie_status_execucao   	=  nvl(ie_status_atualizar_w, 10),
			 dt_inicio_exame      	=  null,
			 dt_fim_exame		=  null,
			 nm_usuario		=  nm_usuario_p
		where    nr_prescricao        	=  nr_prescricao_p
		and      nr_sequencia         	=  nr_seq_prescr_p
		and      ie_status_execucao	= 15;
		
		obter_param_usuario(942, 90, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_status_agenda_w);
		
		if	(ie_atualiza_status_agenda_w = 'S') then
		
			select	nvl(max(nr_seq_agenda),0)
			into	nr_seq_agenda_w
			from	prescr_medica
			where	nr_prescricao        	=  nr_prescricao_p;


			if	(nr_seq_agenda_w = 0) then

				select	max(nr_atendimento)
				into	nr_atendimento_w
				from	prescr_medica
				where	nr_prescricao = nr_prescricao_p;

				select	nvl(max(nr_sequencia),0)
				into	nr_seq_agenda_w
				from	agenda_paciente
				where	nr_atendimento        	=  nr_atendimento_w
				and	ie_status_agenda  = 'EE'
				and	((cd_procedimento = cd_procedimento_w and	ie_origem_proced = ie_origem_proced_w) or (nr_seq_proc_interno = nr_seq_proc_interno_w));

			end if;
			
			if	(nr_seq_agenda_w > 0) then

				select	dt_atendido,
					dt_atendimento
				into	dt_atendido_w,
					dt_atendimento_w
				from	agenda_paciente
				where	nr_sequencia	= nr_seq_agenda_w;
		
				if	(dt_atendido_w is not null) then
					ie_status_w	:= 'AD';
				else	
					ie_status_w	:= 'O';
				end if;

				update 	agenda_paciente
				set    	ie_status_agenda = ie_status_w,
					dt_em_exame	 = null
				where	nr_sequencia	= nr_seq_agenda_w
				and	ie_status_agenda  = 'EE';
			
			end if;
		end if;		
	end if;
end if;

commit;

end atualizar_status_exame_prescr;
/
