create or replace 
procedure tax_reinf_delphi_bifrost(event_p              varchar2,
                                   event_type_p         varchar2,
                                   batch_code_p         varchar2,
                                   log_code_p           varchar2,
                                   establishment_code_p varchar2,
                                   username_p           varchar2,
                                   control_code_p       number, 
                                   ie_status_p          out number) is
  arguments_w   philips_json;
  ie_status_w   number(10);
begin
  arguments_w := philips_json();
  arguments_w.put('event', event_p);
  arguments_w.put('eventType', event_type_p);
  arguments_w.put('batchCode', batch_code_p);
  arguments_w.put('logCode', log_code_p);
  arguments_w.put('establishmentCode', establishment_code_p);
  arguments_w.put('username', username_p);
  arguments_w.put('controlCode', control_code_p);
  tax_reinf_bifrost_integration(arguments_w.to_char(), username_p, control_code_p, ie_status_w);
  ie_status_p := ie_status_w;
end tax_reinf_delphi_bifrost;
/
