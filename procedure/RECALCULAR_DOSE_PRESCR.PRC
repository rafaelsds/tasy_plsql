create or replace
procedure recalcular_dose_prescr(
		ie_tipo_perc_p		number,
		qt_percentual_p		number,
		nr_seq_atendimento_p	number,
		nr_seq_material_p		number) is 

/*	ie_tipo_perc_p
	1 - Aumento
	2 - Redu��o	*/

begin

if	(qt_percentual_p is not null) then
	begin
	if	(ie_tipo_perc_p = 1) then
		begin
		update	paciente_atend_medic
		set	qt_dose_prescricao	= (qt_dose_prescricao + ((qt_percentual_p * nvl(qt_dose_prescricao,0)) / 100))
		where	nr_seq_atendimento	= nr_seq_atendimento_p
		and	nr_seq_material	= nr_seq_material_p;
		end;

	elsif	(ie_tipo_perc_p = 2) then
		begin
		update	paciente_atend_medic
		set	qt_dose_prescricao	= (qt_dose_prescricao - ((qt_percentual_p * nvl(qt_dose_prescricao,0)) / 100))
		where	nr_seq_atendimento	= nr_seq_atendimento_p
		and	nr_seq_material	= nr_seq_material_p;
		end;
	end if;
	end;
end if;

commit;

end recalcular_dose_prescr;
/