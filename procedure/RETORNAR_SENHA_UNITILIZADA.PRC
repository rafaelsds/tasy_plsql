create or replace
procedure retornar_senha_unitilizada(nr_seq_senha_p		number) is 

begin

if	(nvl(nr_seq_senha_p,0) > 0) then

	delete  atendimentos_senha
	where   nr_seq_pac_senha_fila = nr_seq_senha_p
	and 	dt_fim_atendimento is null;

	update	paciente_senha_fila
	set		dt_inutilizacao				= null,
			nr_seq_motivo_inutilizacao	= null,
			dt_retorno_senha			= sysdate,
			qt_chamadas					= 0,
			DT_FIM_ATENDIMENTO			= null,
			DT_INICIO_ATENDIMENTO		= null,
			nm_usuario_inutilizacao		= null,
			ds_maquina_inutilizacao		= null,
			DT_PRIMEIRA_CHAMADA			= null,
			DT_CHAMADA					= null,
			NM_USUARIO_CHAMADA			= null,
			DS_MAQUINA_CHAMADA			= null,
			nr_seq_local_senha			= null
	where	nr_sequencia = nr_seq_senha_p;

end if;

commit;

end retornar_senha_unitilizada;
/
