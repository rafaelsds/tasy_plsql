create or replace
procedure atualiza_deposito_identificado(nr_sequencia_p number, nm_usuario_p  varchar2) is

begin

UPDATE	deposito_identificado
SET	ie_valor_deposito = NULL,
	dt_deposito	= null,
	ie_status	= 'AD',
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

commit;

end atualiza_deposito_identificado;
/
