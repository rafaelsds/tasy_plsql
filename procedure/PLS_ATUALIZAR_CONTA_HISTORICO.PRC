create or replace
procedure pls_atualizar_conta_historico(
		nr_seq_segurado_p	number,
		nr_sequencia_p		number,
		nm_usuario_p		Varchar2) is 

begin
if	(nr_seq_segurado_p is not null) and
	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	update	pls_conta
        set     nr_seq_segurado	= nr_seq_segurado_p,
		nm_usuario	= nm_usuario_p
	where   nr_sequencia  	= nr_sequencia_p;
	
	insert into
	pls_conta_historico (
                nr_sequencia,   
                dt_atualizacao, 
                nm_usuario,     
                nr_seq_conta,   
                dt_historico,   
                ds_titulo)      
	values ( 
		pls_conta_historico_seq.nextval,
                sysdate,
                nm_usuario_p,
                nr_sequencia_p,
                sysdate, 
                'Alterado o beneficiário da conta.'); 
	end;
end if;
commit;
end pls_atualizar_conta_historico;
/