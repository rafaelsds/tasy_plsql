create or replace
procedure consistir_qtd_conv_regra_cons (
									nr_seq_agenda_p		number,
									cd_convenio_p		number,
									dt_agenda_p		date,
									cd_agenda_p		number,
									cd_pessoa_fisica_p	varchar2,
									cd_categoria_p		varchar2,
									cd_plano_p		varchar2,
									cd_estabelecimento_p	number,
									nm_usuario_p		varchar2,
									cd_medico_p		varchar2,
									nr_seq_proc_interno_p	number,
									ie_tipo_atendimento_p	number,
									qt_agendamento_p 	out number,
									qt_perm_regra_p		out number,
									ds_mensagem_p		out varchar2) is 

nr_seq_regra_w			number(10,0);
qt_regra_w			number(10,0)	:= 0;
qt_agendamento_w		number(10,0)	:= 0;
qt_agendamento_out_w		number(10,0) 	:= 0;
cd_funcao_ativa_w		number(10);
cd_convenio_w			number(10);
cd_tipo_agenda_w		number(10,0);
ds_mensagem_customizada_w	varchar2(255)	:= '';
dt_inicio_regra_w		date;
dt_final_regra_w		date;
nr_seq_proc_interno_w		number(10,0);
ie_tipo_regra_w			varchar2(1);
ie_mesmo_pac_w			varchar2(1);
cd_especialidade_w				agenda.cd_especialidade%type;				
cd_agenda_w				agenda_regra_conv.cd_agenda%type;

Cursor C01 is
	select	nr_sequencia,
			cd_convenio,
			cd_agenda
	from	agenda_regra_conv
	where	cd_convenio 		= cd_convenio_p
	and	((cd_agenda 		= cd_agenda_p) 		or (cd_agenda is null))
	and	((ie_tipo_atendimento 	= ie_tipo_atendimento_p) or (ie_tipo_atendimento is null))
	and	((cd_medico		= cd_medico_p) 		or (cd_medico is null))
	and	((cd_categoria		= cd_categoria_p) 	or (cd_categoria is null))
	and	((cd_plano		= cd_plano_p) 		or (cd_plano is null))
	and	((nr_seq_proc_interno 	= nr_seq_proc_interno_p) or (nr_seq_proc_interno is null))
	and	((cd_tipo_agenda	= cd_tipo_agenda_w) 	or (nvl(cd_tipo_agenda, 0) = 0))
	and	((cd_especialidade	= cd_especialidade_w) 	or (nvl(cd_especialidade, 0) = 0))	
	and	((obter_cod_dia_semana(dt_agenda_p)	= ie_dia_semana) or (ie_dia_semana = 9) or (ie_dia_semana is null))	
	and	dt_agenda_p	between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_inicial,'hh24:mi:ss'), '00:00:00'),'dd/mm/yyyy hh24:mi:ss') 	
	and	to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_final,'hh24:mi:ss'), '23:59:59'),'dd/mm/yyyy hh24:mi:ss')	
	order by 	nvl(nr_seq_proc_interno,0),
			nvl(qt_regra, 0),
			nvl(cd_especialidade, 0),
			nvl(cd_agenda, 0),
			nvl(cd_convenio, 0),
			nvl(cd_categoria, 0),
			nvl(cd_plano, 0),
			nvl(ie_tipo_atendimento,0);

begin
cd_funcao_ativa_w	:= Obter_Funcao_Ativa;

select	nvl(max(cd_tipo_agenda), 0),
		nvl(max(cd_especialidade), 0)
into	cd_tipo_agenda_w,
		cd_especialidade_w
from	agenda
where	cd_agenda	= cd_agenda_p;

if	(cd_tipo_agenda_w = 0) then
	select	nvl(max(cd_tipo_agenda), 0)
	into	cd_tipo_agenda_w
	from	agenda a,
			agenda_consulta b
	where	a.cd_agenda = b.cd_agenda
	and		b.nr_sequencia = nr_seq_agenda_p;
end if;

open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	cd_convenio_w,
	cd_agenda_w;
exit when C01%notfound;
	begin
	nr_seq_regra_w 		:= nr_seq_regra_w; 	
	
	select	nvl(ds_mensagem,''),
		to_date(to_char(dt_agenda_p, 'dd/mm/yyyy')||' '||to_char(nvl(hr_inicial, to_date('30/12/1899 00:00:01', 'dd/mm/yyyy hh24:mi:ss')), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
		to_date(to_char(dt_agenda_p, 'dd/mm/yyyy')||' '||to_char(nvl(hr_final, to_date('30/12/1899 23:59:59', 'dd/mm/yyyy hh24:mi:ss')), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'),
		nvl(ie_mesmo_pac,'N')
	into	ds_mensagem_customizada_w,
		dt_inicio_regra_w,
		dt_final_regra_w,
		ie_mesmo_pac_w
	from	agenda_regra_conv
	where	nr_sequencia	= nr_seq_regra_w;

	end;
end loop;
close C01;

if	(nr_seq_regra_w is not null) then
	select	max(qt_regra),
		max(nr_seq_proc_interno),
		nvl(max(ie_tipo_regra),'D')
	into	qt_regra_w,
		nr_seq_proc_interno_w,
		ie_tipo_regra_w
	from	agenda_regra_conv
	where	nr_sequencia = nr_seq_regra_w;
	
	if	(cd_tipo_agenda_w = 2)then
	
		select	count(*)
		into	qt_agendamento_w
		from	agenda_paciente
		where	ie_status_agenda not in ('F','I','C','L','B','R')
		and	cd_convenio 	= cd_convenio_p
		and	(((ie_tipo_regra_w = 'D') and (hr_inicio between dt_inicio_regra_w and dt_final_regra_w)) or 
			((ie_tipo_regra_w = 'M')  and (trunc(hr_inicio,'month') = trunc(dt_agenda_p, 'month'))) or
			((ie_tipo_regra_w = 'S')  and (trunc(hr_inicio) between obter_inicio_fim_semana(dt_agenda_p,'I') and obter_inicio_fim_semana(dt_agenda_p,'F'))))
		and	cd_agenda 	= cd_agenda_p
		and	nr_sequencia 	<> nr_seq_agenda_p
		and	(((cd_pessoa_fisica = cd_pessoa_fisica_p) and (ie_mesmo_pac_w = 'S')) or (ie_mesmo_pac_w = 'N'))
		and	((nr_seq_proc_interno = nr_seq_proc_interno_w) or (nr_seq_proc_interno_w is null));
		
	elsif	(cd_tipo_agenda_w in (3,5)) then
	
		select	count(*)
		into	qt_agendamento_w
		from	agenda_consulta
		where	ie_status_agenda not in ('F','I','C','L','B','R')
		and	cd_convenio 	= cd_convenio_p
		and	(((ie_tipo_regra_w = 'D') and (dt_agenda between dt_inicio_regra_w and dt_final_regra_w)) or 
			((ie_tipo_regra_w = 'M')  and (trunc(dt_agenda,'month') = trunc(dt_agenda_p, 'month'))) or
			((ie_tipo_regra_w = 'S')  and (trunc(dt_agenda) between obter_inicio_fim_semana(dt_agenda_p,'I') and obter_inicio_fim_semana(dt_agenda_p,'F'))))		
		and	((cd_agenda = cd_agenda_w) or (cd_agenda_w is null))
		and	nr_sequencia 	<> nr_seq_agenda_p
		and	(((cd_pessoa_fisica = cd_pessoa_fisica_p) and (ie_mesmo_pac_w = 'S')) or (ie_mesmo_pac_w = 'N'));
		
	end if;
	
	if	(cd_convenio_w = cd_convenio_p)then
		qt_agendamento_w	:= qt_agendamento_w + 1;
	end if;	
	
	if	(qt_agendamento_w > qt_regra_w) and
		(cd_funcao_ativa_w <> 869) then
		Wheb_mensagem_pck.exibir_mensagem_abort(192298);
	end if;
end if;

qt_agendamento_out_w	:= 	qt_agendamento_w;
qt_agendamento_p	:= 	qt_agendamento_out_w;
qt_perm_regra_p		:=	qt_regra_w;
ds_mensagem_p		:=  ds_mensagem_customizada_w;

end consistir_qtd_conv_regra_cons;
/
