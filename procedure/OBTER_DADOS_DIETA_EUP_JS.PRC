create or replace
procedure obter_dados_dieta_eup_js(
					ie_exibe_dieta_categ_conv_p	varchar2,
					nr_seq_regra_acomp_p		number,
					ie_lib_dieta_p			varchar2,
					qt_dieta_acomp_p		number,
					nr_acompanhante_p		number,
					ie_atualiza_dieta_nenhuma_p	varchar2,
					ie_gera_qt_acomp_p		varchar2,
					ie_atualiza_dieta_p		varchar2,
					nr_atendimento_p		number,
					cd_pessoa_fisica_p		varchar2,
					cd_plano_convenio_p		varchar2,
					cd_categoria_p			varchar2,
					cd_convenio_p			number,
					nm_usuario_p			Varchar2,
					nr_acompanhante_ret_p       out number,
					qt_dieta_acomp_ret_p	    out number,
					ie_lib_dieta_ret_p	   out varchar2,
					nr_seq_regra_acomp_ret_p   out number,
					ds_msg_dieta_p		   out varchar2) is 

nr_acompanhante_w		number(3);
qt_dieta_acomp_w		number(3);
ie_lib_dieta_w			varchar2(15);
nr_seq_regra_acomp_w		number(10);
ds_dieta_w			varchar2(255);

				
					
begin

Obter_Dados_Dieta_Categ_Conv(cd_convenio_p, cd_categoria_p, cd_plano_convenio_p, cd_pessoa_fisica_p, nr_acompanhante_w, qt_dieta_acomp_w, ie_lib_dieta_w, nr_seq_regra_acomp_w, nr_atendimento_p);

if	(nvl(nr_acompanhante_p,0) = 0) or
	(ie_atualiza_dieta_p = 'S') then
	nr_acompanhante_ret_p := nvl(nr_acompanhante_w,0);
end if;
	
if	(ie_gera_qt_acomp_p = 'S') then
	if	(nvl(qt_dieta_acomp_p,0) = 0) or
		(ie_atualiza_dieta_p = 'S') then
		qt_dieta_acomp_ret_p := nvl(qt_dieta_acomp_w,0);
	end if;
elsif	(ie_gera_qt_acomp_p = 'R') and
	((nvl(qt_dieta_acomp_w,0) > 0) or
	 (ie_atualiza_dieta_p = 'S')) then
	if	((nvl(qt_dieta_acomp_p,0) = 0) or 
	         (ie_atualiza_dieta_p = 'S')) then
		qt_dieta_acomp_ret_p := nvl(qt_dieta_acomp_w,0);
	end if;
end if;
if	((ie_lib_dieta_p is null) or
	((ie_atualiza_dieta_p = 'S') and
	 (ie_atualiza_dieta_nenhuma_p = 'N')) or
	((ie_atualiza_dieta_nenhuma_p = 'S') and
	 (ie_lib_dieta_p = 'N'))) then
	ie_lib_dieta_ret_p := ie_lib_dieta_w;
end if;

if	((nvl(nr_seq_regra_acomp_p,0) = 0) or
	(ie_atualiza_dieta_p = 'S')) and
	(nvl(nr_seq_regra_acomp_w,0) > 0) then
	nr_seq_regra_acomp_ret_p := nvl(nr_seq_regra_acomp_w,0);	
end if;

if	(ie_exibe_dieta_categ_conv_p = 'S') then

	ds_dieta_w := Obter_Valor_Dominio(1396, nvl(ie_lib_dieta_ret_p,nvl(ie_lib_dieta_p,'N')));
	ds_msg_dieta_p := substr(obter_texto_dic_objeto(297302, wheb_usuario_pck.get_nr_seq_idioma, 'DIETA='||ds_dieta_w),1,255);
end if;

commit;

end obter_dados_dieta_eup_js;
/