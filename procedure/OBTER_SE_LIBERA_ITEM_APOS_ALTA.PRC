create or replace
procedure Obter_Se_Libera_Item_Apos_Alta(nr_prescricao_p		number,
					 nm_usuario_p			Varchar2,
					 ds_mensagem_p			out varchar2,
					 ie_libera_p			out varchar) is 

ds_mensagem_w	varchar2(255)	:= '';	
ie_permite_w	varchar2(2) 	:= 'S';	
possui_reg_w	number(10)	:= 0;	
qt_coletas_w	number(10)	:= 0;	
qt_glicemia_w	number(10)	:= 0;	
qt_dieta_w	number(10)	:= 0;	
qt_gas_w	number(10)	:= 0;	
qt_hemoterapia_w number(10)	:= 0;	
qt_ivc_w	 number(10)	:= 0;	
qt_jejum_w	 number(10)	:= 0;	
qt_leite_w	number(10)	:= 0;
qt_material_w	number(10)	:= 0;
qt_medicamento_w number(10)	:= 0;	
qt_npt_adulta_w	 number(10)	:= 0;	
qt_npt_neo_w     number(10)	:= 0;	
qt_proced_w	 number(10)	:= 0;	
qt_recomendacao_w number(10)	:= 0;
qt_solucao_w	  number(10)	:= 0;
qt_suplemento_w	  number(10)	:= 0;
qt_suporte_w	  number(10)	:= 0;
qt_dialise_w	  number(10)	:= 0;
quebra_w	varchar2(10)	:= chr(13)||chr(10);					 
					 
begin


select	count(ie_permite)
into	possui_reg_w
from	plt_regra_lib_apos_alta
where	ie_permite = 'N';

if	(possui_reg_w > 0)	then

	--  In�cio Coletas
	
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 20;
		
	if	(ie_permite_w = 'N') then
		select	count(nr_seq_exame)
		into	qt_coletas_w 
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_exame is not null
		and	nr_seq_solic_sangue is null
		and	nr_seq_derivado is null
		and	nr_seq_exame_sangue is null;

		if	(qt_coletas_w > 0) then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,20),1,50) ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,20),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim coletas

	--  In�cio Controle de glicemia

	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 18;
		
	if	(ie_permite_w = 'N') then
		select	count(nr_seq_prot_glic)
		into	qt_glicemia_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_prot_glic is not null;

		if	(qt_glicemia_w > 0)  then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,18),1,50) ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,18),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;
		
	-- Fim Controle de Glicemia

	-- In�cio Dieta

	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 2;
		
	if	(ie_permite_w = 'N') then
		select	count(cd_dieta)
		into	qt_dieta_w
		from	prescr_Dieta
		where	nr_prescricao = nr_prescricao_p;

		if	(qt_dieta_w > 0)	then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,2),1,50) ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,2),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
		
	end if;

	-- Fim Dieta

	--In�cio Gasoterapia
	
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 3;
		
	if	(ie_permite_w = 'N') then
		select	count(nr_seq_gas)
		into	qt_gas_w
		from	prescr_gasoterapia
		where	nr_prescricao	= nr_prescricao_p;

		if	(qt_gas_w > 0)	then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,3),1,50) ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,3),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim Gasoterapia

	-- In�cio Hemoterapia
	
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 4;
		
	if	(ie_permite_w = 'N') then
		select	count(nr_seq_derivado)
		into	qt_hemoterapia_w 
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_exame is null
		and	nr_seq_solic_sangue is not null
		and	((nr_seq_derivado is not null) or 
			(Obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'AT') = 'S') or 
			(Obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'BSST') = 'S') or
			(Obter_se_exibe_proced(nr_prescricao,nr_sequencia,ie_tipo_proced,'BS') = 'S'))
		and	nr_seq_exame_sangue is null;	

		if	(qt_hemoterapia_w > 0)	then
		
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,4),1,50) ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,4),1,50)||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
		
	end if;

	-- Fim Hemoterapia


	-- In�cio IVC
	
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 19;
		
	if	(ie_permite_w = 'N') then
		select	count(w.ie_ivc)
		into	qt_ivc_w
		from	proc_interno w,
			prescr_procedimento x
		where	x.nr_prescricao = nr_prescricao_p
		and	w.nr_sequencia = x.nr_seq_proc_interno
		and	w.ie_tipo <> 'G'
		and	w.ie_tipo <> 'BS'
		and	w.ie_ivc = 'S'
		and	x.nr_seq_proc_interno is not null
		and	x.nr_seq_prot_glic is null
		and	x.nr_seq_exame is null
		and	x.nr_seq_solic_sangue is null
		and	x.nr_seq_derivado is null;

		if	(qt_ivc_w > 0)	then
			
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,19),1,50) ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,19),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim IVC

	-- In�cio Jejum
	
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 5;
		
	if	(ie_permite_w = 'N') then
		select	count(nr_seq_tipo)
		into	qt_jejum_w
		from	rep_jejum
		where	nr_prescricao = nr_prescricao_p;

		if	(qt_jejum_w > 0) 	then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,5),1,50) ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,5),1,50) ||quebra_w;
			end if;
		ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim Jejum

	-- In�cio Leite e Derivados
	
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 21;
		
	if	(ie_permite_w = 'N') then
		select	count(nr_sequencia)
		into	qt_leite_w
		from	prescr_leite_deriv
		where	nr_prescricao 	= nr_prescricao_p;

		if	(qt_leite_w > 0)	then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,21),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w || ''||substr(obter_valor_dominio(3270,21),1,50) ||quebra_w;
			end if;
		ie_libera_p  := 'N'; 
		end if;
		
	end if;

	-- Fim Leite e Derivados

	-- In�cio Materiais

	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 6;

	if	(ie_permite_w = 'N') then
		select	count(nr_sequencia)
		into	qt_material_w
		from	prescr_material
		where	nr_prescricao 	= nr_prescricao_p
		and	ie_agrupador = 2;
		
		if	(qt_material_w > 0) then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,6),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w || ''||substr(obter_valor_dominio(3270,6),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim materiis

	-- In�cio medicamentos
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  =	7;

	if	(ie_permite_w = 'N') then
		select	count(nr_sequencia)
		into	qt_medicamento_w
		from	prescr_material
		where	nr_prescricao 	= nr_prescricao_p
		and	ie_agrupador = 1;
		
		if	(qt_medicamento_w > 0) then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,7),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w || ''||substr(obter_valor_dominio(3270,7),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim medicamentos

	-- In�cio npt Adulta
		
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  =	8;
		
	if	(ie_permite_w = 'N') then
		select	count(nr_sequencia)
		into	qt_npt_adulta_w
		from	nut_pac
		where	nr_prescricao	= nr_prescricao_p
		and	nvl(ie_npt_adulta,'N') = 'S';
		
		if	(qt_npt_adulta_w > 0)	then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,8),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w || ''||substr(obter_valor_dominio(3270,8),1,50)||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim npt Adulta

	-- In�cio npt neo
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  =	9;
		
	if	(ie_permite_w = 'N') then
		select	count(nr_sequencia)
		into	qt_npt_neo_w
		from	nut_pac
		where	nr_prescricao	= nr_prescricao_p
		and	nvl(ie_npt_adulta,'N') = 'N';
		
		if	(qt_npt_neo_w > 0)	then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,9),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w || ''||substr(obter_valor_dominio(3270,9),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim npt neo

	-- In�cio procedimento
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  =	12;

	if	(ie_permite_w = 'N')	then
		select	count(nr_sequencia)
		into	qt_proced_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_exame is null
		and	nr_seq_solic_sangue is null
		and	nr_seq_derivado is null
		and	nr_seq_exame_sangue is null
		and	nr_seq_origem is null
		and	nr_seq_prot_glic is null;
		
		if	(qt_proced_w > 0)	then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,12),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w || ''||substr(obter_valor_dominio(3270,12),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim procedimento

	-- In�cio Recomenda��o
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 13;

	if	(ie_permite_w = 'N')	then
		select	count(nr_sequencia)
		into	qt_recomendacao_w
		from	prescr_recomendacao
		where	nr_prescricao = nr_prescricao_p;
		
		if	(qt_recomendacao_w > 0) then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,13),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w || ''||substr(obter_valor_dominio(3270,13),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- Fim Recomenda��o

	-- In�cio Solu��o
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 16;

	if	(ie_permite_w = 'N') 	then
		select	count(nr_seq_solucao)
		into	qt_solucao_w
		from	prescr_solucao
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_dialise is null;
		
		if	(qt_solucao_w > 0) then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,16),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w || ''||substr(obter_valor_dominio(3270,16),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N';
		end if;

	end if;

	-- FIm Solu��o

	-- In�cio Suplemento
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 15;

	if	(ie_permite_w	= 'N')	then
		select	count(nr_sequencia)
		into	qt_suplemento_w
		from	prescr_material
		where	nr_prescricao 	= nr_prescricao_p
		and	ie_agrupador = 12;
		
		if	(qt_suplemento_w > 0) then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,15),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,15),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;

	-- FIm Suplemento

	-- In�cio Suporte
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 14;

	if	(ie_permite_w	= 'N')	then
		select	count(nr_sequencia)
		into	qt_suporte_w
		from	prescr_material
		where	nr_prescricao 	= nr_prescricao_p
		and	ie_agrupador = 8;
		
		if	(qt_suporte_w > 0) then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,14),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''||substr(obter_valor_dominio(3270,14),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;
	end if;
		
	-- Fim Suporte

	-- In�cio Di�lise
	select	nvl(max(ie_permite),'S')
	into	ie_permite_w
	from	plt_regra_lib_apos_alta
	where	ie_tipo_item  = 1;

	if	(ie_permite_w = 'N') then
		select	count(ie_tipo_dialise)
		into	qt_dialise_w
		from	hd_prescricao
		where	nr_prescricao = nr_prescricao_p
		and	ie_tipo_dialise in ('H','P');
		
		if	(qt_dialise_w > 0) then
			if	(ds_mensagem_w is null) then
				ds_mensagem_w := substr(obter_valor_dominio(3270,1),1,50)  ||quebra_w;
			else
				ds_mensagem_w := ds_mensagem_w ||''|| substr(obter_valor_dominio(3270,1),1,50) ||quebra_w;
			end if;
			ie_libera_p  := 'N'; 
		end if;

	end if;

	-- FIm Di�lise
end if;
if	(ds_mensagem_w is not null) and
	(ie_libera_p = 'N') then
	ds_mensagem_p :=  substr(/*'Conforme regra n�o � permitido liberar os seguintes itens ap�s alta m�dica : '*/ obter_desc_expressao(782116)||' '||quebra_w|| ds_mensagem_w,1,255);
end if;

end Obter_Se_Libera_Item_Apos_Alta;
/