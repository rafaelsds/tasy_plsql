create or replace
procedure registrar_log_aplicacao_swing(
			nm_projeto_p		varchar2,
			dt_ult_commit_p		date,
			nm_usuario_commit_p	varchar2,
			ds_revisao_p		varchar2,
			ds_versao_java_p	varchar2,
			dt_versao_java_p	date,
			nm_usuario_p		varchar2) is 

qt_registros_w	number(10);

begin

select	count(*)
into	qt_registros_w
from	aplicacao_swing_log
where	nm_projeto = nm_projeto_p;

if	( qt_registros_w = 0) then

	insert into aplicacao_swing_log (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nm_projeto,
		dt_ult_commit,
		nm_usuario_commit,
		ds_revisao,
		ds_versao_java,
		dt_versao_java
	) values (
		aplicacao_swing_log_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nm_projeto_p,
		dt_ult_commit_p,
		nm_usuario_commit_p,
		ds_revisao_p,
		ds_versao_java_p,
		dt_versao_java_p
	);
	
	commit;
	
else

	update 	aplicacao_swing_log
	set	nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		dt_ult_commit = dt_ult_commit_p,
		nm_usuario_commit = nm_usuario_commit_p,
		ds_revisao = ds_revisao_p,
		ds_versao_java = ds_versao_java_p,
		dt_versao_java = dt_versao_java_p
	where	nm_projeto = nm_projeto_p;
	
	commit;

end if;

end registrar_log_aplicacao_swing;
/