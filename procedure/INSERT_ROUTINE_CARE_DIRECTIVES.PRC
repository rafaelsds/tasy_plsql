create or replace 
procedure insert_routine_care_directives(
		nr_atendimento_p number,
		cd_pessoa_fisica_p varchar2,
		ds_lista_p varchar2,
		nm_usuario_p varchar2,
		cd_professional_p number		
)
	is
	
ds_lista_w					varchar2(2000);			
ds_lista_aux_w				varchar2(2000);
nr_seq_aux_w				number(10);
k							integer;
hasQtDescription			pls_integer;
ie_guideline_w 				routine_care_config.ie_guideline%type;
ie_response_w				routine_care_config.ie_response%type;
ds_guideline_description_w	routine_care_config.ds_guideline_description%type;
ie_display_patientbar_w 	routine_care_config.ie_display_patientbar%type;
nr_seq_presentation_w 		routine_care_config.nr_seq_presentation%type;

begin
	ds_lista_w := ds_lista_p;

	while ds_lista_w is not null loop 
		begin
		
			select	instr(ds_lista_w, ',')
			into	k
			from	dual;
			
			if	(k > 1) and 
				(substr(ds_lista_w, 1, k -1) is not null) then
				ds_lista_aux_w			:= substr(ds_lista_w, 1, k-1);
				nr_seq_aux_w			:= replace(ds_lista_aux_w, ',','');
				ds_lista_w			:= substr(ds_lista_w, k + 1, 2000);
			elsif	(ds_lista_w is not null) then
				nr_seq_aux_w			:= replace(ds_lista_w,',','');
				ds_lista_w			:= '';
			end if;
			
			if(nr_seq_aux_w > 0) then
				select
					ie_guideline,
					ie_response,
					ds_guideline_description,
					ie_display_patientbar,
					nr_seq_presentation
				into	ie_guideline_w,
					ie_response_w,
					ds_guideline_description_w,
					ie_display_patientbar_w,
					nr_seq_presentation_w
				from
					ROUTINE_CARE_CONFIG
				where nr_sequencia = nr_seq_aux_w;
				
				select count(nr_sequencia)
				into	hasQtDescription
				from 
					diretriz_atendimento
				where ie_diretriz 				= ie_guideline_w
				and cd_pessoa_fisica 			= cd_pessoa_fisica_p
				and	ie_status 					= 'C'
				and	dt_liberacao 				is not null
				and	dt_inativacao 				is null;
				
				commit;
				if(hasQtDescription = 0) then
					insert into 
					diretriz_atendimento (
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						ie_situacao,
						cd_pessoa_fisica,
						nr_atendimento,
						ie_diretriz,
						ie_resposta,
						ds_guideline_description,
						ie_display_patientbar,
						nr_seq_presentation,
						ie_status,
						nm_usuario_nrec, 
						dt_atualizacao_nrec,
						dt_registration,
						cd_professional,
						ie_decision_maker,
						cd_clinical_decison_maker
					)
					values(
						diretriz_atendimento_seq.nextval,
						sysdate,
						nm_usuario_p,
						'A',
						cd_pessoa_fisica_p,
						nr_atendimento_p,
						ie_guideline_w,
						ie_response_w,
						ds_guideline_description_w,
						ie_display_patientbar_w,
						nr_seq_presentation_w,
						'C',
						nm_usuario_p,
						sysdate,
						sysdate,
						cd_professional_p,
						1,
						cd_professional_p
					);
				end if;
			end if;
		end;
	end loop;
	
	commit;
end insert_routine_care_directives;
/