create or replace
procedure consistir_solucao_adep	(ie_tipo_solucao_p		number,
					nr_prescricao_p		number,
					nr_seq_solucao_p		number,
					nm_usuario_p		varchar2) is

nr_etapas_sol_w		number(3,0);
nr_etapas_atual_w		number(3,0);
ie_acm_w		varchar2(1);
ie_sn_w			prescr_material.ie_se_necessario%type;
qt_pool_w		number(3,0);
cd_estabelecimento_w	number(4,0);
ie_consiste_sne_w		varchar2(1);
qt_cont_w		number(3,0);
ie_estender_etapa_w	varchar2(1);
cd_intervalo_w		varchar2(7);
qt_etapas_reg_w		Number(5);
cd_funcao_origem_w	prescr_medica.cd_funcao_origem%type;
ie_continua_instavel_w	prescr_solucao.cd_intervalo%type;
ie_solucoes_separadas_w	varchar2(1 char);

begin
if	(ie_tipo_solucao_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) then
	if	(ie_tipo_solucao_p = 1) then -- solucoes
		select	decode(nvl(a.IE_ETAPA_ESPECIAL,'N'),'N', a.nr_etapas, a.nr_etapas+1),
			obter_etapas_adep_sol(ie_tipo_solucao_p,nr_prescricao_p,nr_seq_solucao_p),
			nvl(a.ie_acm,'N'),
			nvl(a.ie_estender_etapa,'N'),
			a.cd_intervalo,
			b.cd_funcao_origem
		into	nr_etapas_sol_w,
			nr_etapas_atual_w,
			ie_acm_w,
			ie_estender_etapa_w,
			cd_intervalo_w,
			cd_funcao_origem_w
		from	prescr_solucao a,
				prescr_medica b
		where	a.nr_prescricao = b.nr_prescricao
		and		a.nr_prescricao = nr_prescricao_p
		and		a.nr_seq_solucao = nr_seq_solucao_p;

		obter_param_usuario(1113, 675, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_continua_instavel_w);
		obter_param_usuario(1113, 718, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_solucoes_separadas_w);
		
		if (cd_funcao_origem_w = 2314) and
			((nr_etapas_sol_w = 1 and
				ie_acm_w = 'S' and
				upper(ie_continua_instavel_w) = upper(cd_intervalo_w)) or
			(ie_acm_w = 'N' and
				nvl(ie_solucoes_separadas_w, 'S') = 'N')) then
				nr_etapas_sol_w := null;
		end if;
		
		if	((ie_acm_w = 'N') or
			 ((ie_acm_w = 'S') and (nr_etapas_sol_w is not null))) and /* Cfe Marcus em 20/6/8 */
			(nr_etapas_sol_w is not null) and
			(nr_etapas_atual_w is not null) and
			(nr_etapas_atual_w + 1 > nr_etapas_sol_w) and
			((ie_estender_etapa_w = 'N') or
			 ((ie_estender_etapa_w = 'S') and
			  (nr_etapas_atual_w >= obter_qt_etapas_validade(nr_prescricao_p, nr_seq_solucao_p)))) then			
			/*ATENcaO: Esta solucao eh composta por ' || to_char(nr_etapas_sol_w) || ' etapas.' || chr(10) ||
			'Ja foram realizadas todas as trocas de frascos (' || to_char(nr_etapas_sol_w - 1) || ') previstas para a '|| chr(10) ||'  mesma, favor verificar!*/
			Wheb_mensagem_pck.exibir_mensagem_abort(261383,'ETAPAS='||to_char(nr_etapas_sol_w) || ';' || 'ETAPAS2=' || to_char(nr_etapas_sol_w - 1));
		end if;
	elsif	(ie_tipo_solucao_p = 2) then --Suporte nutricional

		select	obter_etapas_adep_sol(ie_tipo_solucao_p,nr_prescricao_p,nr_seq_solucao_p),
			b.cd_estabelecimento,
			nvl(a.ie_acm,'N'),
			nvl(a.ie_se_necessario, 'N'),
			a.cd_intervalo
		into	nr_etapas_atual_w,
			cd_estabelecimento_w,
			ie_acm_w,
			ie_sn_w,
			cd_intervalo_w
		from	prescr_material a,
			prescr_medica b
		where	a.nr_prescricao = b.nr_prescricao
		and	a.nr_prescricao = nr_prescricao_p
		and	a.nr_sequencia = nr_seq_solucao_p;
		
		select	count(*)
		into	nr_etapas_sol_w
		from	prescr_mat_hor	a
		where 	a.nr_prescricao = nr_prescricao_p
		and		a.nr_seq_material = nr_seq_solucao_p
		and		nvl(a.ie_horario_especial,'N') <> 'S'
		and		a.ie_agrupador = 8;

		obter_param_usuario(924, 401, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_consiste_sne_w);

		if	(nvl(ie_consiste_sne_w,'N') = 'S') then
			begin
			select	count(*)
			into	qt_cont_w
			from	prescr_medica
			where	nr_prescricao = nr_prescricao_p
			and	dt_validade_prescr > sysdate;

			if	(qt_cont_w = 0) then
				--Nao eh possivel realizar a troca de frascos. A validade da prescricao esta vencida!
				Wheb_mensagem_pck.exibir_mensagem_abort(261390);
			end if;

			end;
		else
			begin
			if 	((ie_acm_w = 'N') or
				((ie_acm_w = 'S') and (nr_etapas_sol_w is not null))) and
				(nvl(nr_etapas_sol_w,0) > 0) and
				(nvl(nr_etapas_atual_w,0) > 0) and
				(nr_etapas_atual_w + 1 > nr_etapas_sol_w) then
				/*ATENcaO: Esta solucao eh composta por ' || to_char(nr_etapas_sol_w) || ' etapas.' || chr(10) ||
									'Ja foram realizadas todas as trocas de frascos (' || to_char(nr_etapas_sol_w - 1) || ') previstas para a   mesma, favor verificar!#@#@');*/
				Wheb_mensagem_pck.exibir_mensagem_abort(261383,'ETAPAS='||to_char(nr_etapas_sol_w) || ';' || 'ETAPAS2=' || to_char(nr_etapas_sol_w - 1));
			end if;
			end;
		end if;

	elsif	(ie_tipo_solucao_p = 3) then -- hemocomponentes
		select	nvl(decode(obter_ocorrencias_horarios_rep(ds_horarios),0,1),obter_ocorrencias_horarios_rep(ds_horarios)) * nvl(qt_procedimento,1),
				OBTER_ETAPA_ATUAL_PROC(nr_prescricao_p,nr_seq_solucao_p) + nvl(nr_etapas_suspensa,0),			
				qt_etapa
		into	nr_etapas_sol_w,
				nr_etapas_atual_w,
				qt_pool_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		and		nr_sequencia = nr_seq_solucao_p
		and		nr_seq_solic_sangue is not null;
		
		select 	count(1)
		into	qt_etapas_reg_w
		from	prescr_proc_bolsa a
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_procedimento = nr_seq_solucao_p
		and		dt_cancelamento is null
		and		not exists	(	select	1
						from	prescr_proc_hor b
						where	b.nr_prescricao = a.nr_prescricao
						and		b.nr_seq_procedimento = a.nr_seq_procedimento
						and		b.nr_sequencia = a.nr_seq_horario
						and		dt_suspensao is not null);
		
		if	(qt_etapas_reg_w > nr_etapas_sol_w) then
			nr_etapas_sol_w	:= qt_etapas_reg_w;
		end if;
		
		

		if	(nr_etapas_sol_w is not null) and
			(nr_etapas_atual_w is not null) and
			(qt_pool_w is null) and
			(nr_etapas_atual_w + 1 > nr_etapas_sol_w) then
			/*(ATENcaO: Esta solucao eh composta por ' || to_char(nr_etapas_sol_w) || ' etapas.' || chr(10) ||
							'Ja foram realizadas todas as trocas de frascos (' || to_char(nr_etapas_sol_w - 1) || ') previstas para a  mesma, favor verificar!*/
			Wheb_mensagem_pck.exibir_mensagem_abort(261383,'ETAPAS='||to_char(nr_etapas_sol_w) || ';' || 'ETAPAS2=' || to_char(nr_etapas_sol_w - 1));
		elsif	(nr_etapas_sol_w is not null) and
			(nr_etapas_atual_w is not null) and
			(qt_pool_w is not null) and
			(nr_etapas_atual_w + 1 > qt_pool_w) then
			/*ATENcaO: Esta solucao eh composta por ' || to_char(qt_pool_w) || ' pool.' || chr(10) ||
							'Ja foram realizadas todas as trocas de frascos (' || to_char(qt_pool_w - 1) || ') previstas para o mesmo, favor verificar!*/
			Wheb_mensagem_pck.exibir_mensagem_abort(261383,'ETAPAS='||to_char(qt_pool_w) || ';' || 'ETAPAS2=' || to_char(qt_pool_w - 1));
		end if;
	elsif	(ie_tipo_solucao_p = 4) then -- npt adulta
		select	qt_fase_npt,
			obter_etapas_adep_sol(ie_tipo_solucao_p,nr_prescricao_p,nr_seq_solucao_p)
		into	nr_etapas_sol_w,
			nr_etapas_atual_w
		from	nut_paciente
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia = nr_seq_solucao_p;

		if	(nr_etapas_sol_w is not null) and
			(nr_etapas_atual_w is not null) and
			(nr_etapas_atual_w + 1 > nr_etapas_sol_w) then
			/*ATENcaO: Esta solucao eh composta por ' || to_char(nr_etapas_sol_w) || ' etapas.' || chr(10) ||
								'Ja foram realizadas todas as trocas de frascos (' || to_char(nr_etapas_sol_w - 1) || ') previstas para a   mesma, favor verificar!*/
			Wheb_mensagem_pck.exibir_mensagem_abort(261383,'ETAPAS='||to_char(nr_etapas_sol_w) || ';' || 'ETAPAS2=' || to_char(nr_etapas_sol_w - 1));
		end if;


	elsif	(ie_tipo_solucao_p = 5) then -- npt neonatal
		select	qt_fase_npt,
			obter_etapas_adep_sol(ie_tipo_solucao_p,nr_prescricao_p,nr_seq_solucao_p)
		into	nr_etapas_sol_w,
			nr_etapas_atual_w
		from	nut_pac
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia = nr_seq_solucao_p;

		if	(nr_etapas_sol_w is not null) and
			(nr_etapas_atual_w is not null) and
			(nr_etapas_atual_w + 1 > nr_etapas_sol_w) then
			/*ATENcaO: Esta solucao eh composta por ' || to_char(nr_etapas_sol_w) || ' etapas.' || chr(10) ||
							'Ja foram realizadas todas as trocas de frascos (' || to_char(nr_etapas_sol_w - 1) || ') previstas para a   mesma, favor verificar!*/
			Wheb_mensagem_pck.exibir_mensagem_abort(261383,'ETAPAS='||to_char(nr_etapas_sol_w) || ';' || 'ETAPAS2=' || to_char(nr_etapas_sol_w - 1));
		end if;
	end if;
end if;

end consistir_solucao_adep;
/
