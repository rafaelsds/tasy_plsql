create or replace
procedure man_qua_atualizar_trein(
			nm_usuario_p		Varchar2,
			nr_seq_ordem_p		number) is 
			
nr_seq_doc_w	  number(10);

begin
-- Verifica se a OS est� v�nculada a uma revis�o do documento
select	max(nr_seq_doc)
into	nr_seq_doc_w
from	qua_doc_revisao
where	nr_seq_ordem_trein = nr_seq_ordem_p;
			
-- Caso seja seja um documento novo, n�o vai possuir revis�o, e a OS vai estar v�nculada ao documento
if (nr_seq_doc_w is null) then
	select	max(nr_sequencia)
	into	nr_seq_doc_w
	from	qua_documento
	where	nr_seq_ordem_trein = nr_seq_ordem_p;			
end if;

--Agora, caso a Os esteja v�nculada ao documento ou a uma revis�o dele, atualiza o status
if(nr_seq_doc_w is not null) then
	begin
		qua_atualizar_status_doc(nr_seq_doc_w, qua_doc_obter_novo_status(nr_seq_doc_w), wheb_usuario_pck.get_nm_usuario);
	end;
end if;

commit;

end man_qua_atualizar_trein;
/