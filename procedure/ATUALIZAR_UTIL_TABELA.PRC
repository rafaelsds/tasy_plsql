create or replace
procedure atualizar_util_tabela(	cd_estabelecimento_p		number,
					cd_tabela_custo_p		number,
					cd_tabela_utilizada_p	number,
					nm_usuario_p			varchar2) is

dt_atualizacao_w			date	:= sysdate;

begin

insert into tabela_utilizada(
	cd_estabelecimento, 
	cd_tabela_custo, 
	cd_tabela_utilizada, 
	nm_usuario, 
	dt_atualizacao)
values(cd_estabelecimento_p,
	cd_tabela_custo_p, 
	cd_tabela_utilizada_p,
	nm_usuario_p,
	dt_atualizacao_w);
exception when others then
	dt_atualizacao_w := sysdate;

end atualizar_util_tabela;
/