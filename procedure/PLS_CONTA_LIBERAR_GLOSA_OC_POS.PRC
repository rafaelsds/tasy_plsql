create or replace
procedure pls_conta_liberar_glosa_oc_pos
			(	nr_seq_analise_conta_item_p	Number,
				nr_seq_proc_p			Number,
				nr_seq_mat_p			Number,
				nr_seq_conta_p			Number,
				nr_seq_analise_p		Number,
				nr_seq_glosa_oc_p		Number,
				ie_tipo_glosa_oc_p		Varchar2,
				nr_seq_mot_liberacao_p		Number,
				ds_observacao_p			Varchar2,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number,				
				nr_seq_grupo_atual_p		Number,
				ie_consistir_grupo_p		Varchar2, 
				ie_reconsistencia_p		Varchar2	) is

/*	ie_tipo_glosa_oc_p
	G - Glosa
	O - Ocorr�ncia
*/

qt_registros_w			Number(10)	:= 0;
ie_tipo_motivo_w		Varchar2(3);
ie_tipo_historico_w		Number(2);
nr_seq_item_w			Number(10);
ie_tipo_item_w			Varchar2(1);
nr_seq_ocorrencia_w		Number(10);
nr_seq_glosa_w			Number(10);
nr_nivel_liberacao_w		Number(10);
nr_nivel_liberacao_auditor_w	Number(10);
nr_seq_oc_benef_w		Number(10);
nr_seq_analise_conta_glosa_w	Number(10);
nr_seq_conta_proc_w		Number(10);
nr_seq_conta_mat_w		Number(10);
nr_seq_conta_w			Number(10);
nr_seq_glosa_oc_w		Number(10);
ie_grupo_liberado_w		Varchar2(1);
ie_se_grupo_auditor_w		Varchar2(1);
cd_codigo_glosa_oco_w		Varchar2(20);
ie_status_w			Varchar2(1);
nr_seq_motivo_glosa_w		Number(10);
nr_seq_ocor_w			Number(10);
ie_existe_grupo_glosa_w		Varchar2(1);
nr_seq_nivel_lib_w		Number(10);
qt_glosa_w			Number(10,2);
vl_glosa_w			Number(15,2);
qt_apresentado_w		Number(10,2);
vl_total_apres_w		Number(15,2);
ds_observacao_w			Varchar2(4000);
ds_tipo_motivo_w		Varchar2(255);
ds_motivo_liberacao_w		Varchar2(255);
ie_origem_analise_w		Number(10);
var_tipo_item_w			Varchar2(2);
nr_seq_proc_partic_w		Number(10);


/*Obter as glosas que foram geradas pela ocorrencia*/
Cursor C01 is
	select	b.nr_sequencia,
		b.nr_seq_conta_proc,
		b.nr_seq_conta_mat,
		b.nr_seq_conta,
		b.nr_seq_glosa_oc
	from	pls_conta_glosa a,
		pls_analise_conta_item b
	where	a.nr_sequencia = b.nr_seq_glosa_oc
	and	a.nr_seq_ocorrencia = nr_seq_oc_benef_w
	and	b.ie_status = 'P'
	and	b.nr_seq_analise = nr_seq_analise_p
	order by 1;

begin

/*Obter dados da glosa/ocorr�ncia*/
select	ie_status,
	cd_codigo,
	nr_seq_conta_proc,
	nr_seq_conta_mat,
	nr_seq_proc_partic,
	nvl(nr_seq_conta,0),
	decode(ie_tipo, 'O', nr_seq_glosa_oc, null),
	decode(ie_tipo, 'G', nr_seq_glosa_oc, null)
into	ie_status_w,
	cd_codigo_glosa_oco_w,
	nr_seq_conta_proc_w,
	nr_seq_conta_mat_w,
	nr_seq_proc_partic_w,
	nr_seq_conta_w,	
	nr_seq_ocorrencia_w,
	nr_seq_glosa_w
from	pls_analise_conta_item
where 	nr_sequencia = nr_seq_analise_conta_item_p;

/*Verificar se a ocorr�ncia j� foi liberado*/
if	(ie_status_w = 'C') then
	/*'Esta ocorr�ncia � uma ocorr�ncia corrigida. Este tipo de ocorr�ncia n�o pode ser liberada.'*/
	wheb_mensagem_pck.exibir_mensagem_abort(173775);
elsif	(ie_status_w = 'E') then
	/*'Esta ocorr�ncia � uma ocorr�ncia que necessita de corre��o. Este tipo de ocorr�ncia n�o pode ser liberada.'*/
	wheb_mensagem_pck.exibir_mensagem_abort(173776);	
elsif	(ie_status_w <> 'P') then
	/*'Esta glosa/ocorr�ncia j� foi liberada.'*/
	wheb_mensagem_pck.exibir_mensagem_abort(173777);	
end if;

/*Verificar se o grupo j� foi liberado*/
ie_grupo_liberado_w := pls_obter_se_grupo_liberado(nm_usuario_p, nr_seq_analise_p, nr_seq_grupo_atual_p);
if	(ie_grupo_liberado_w = 'S') and
	(nvl(ie_consistir_grupo_p,'S') = 'S') then	
	/*'A a��o deste grupo de auditores j� foi encerrada.'*/
	wheb_mensagem_pck.exibir_mensagem_abort(173778);		
end if;

if	(ie_tipo_glosa_oc_p = 'G') then
	nr_seq_motivo_glosa_w := pls_obter_seq_motivo_glosa(cd_codigo_glosa_oco_w);
else
	nr_seq_ocor_w := pls_obter_seq_ocorrencia(cd_codigo_glosa_oco_w);
end if;

/*Obter se � um dos auditores do grupo no caso da ocorrencia.
    Ou se for glosa, verifica se existe grupo na glosa e se este faz parte do grupo*/
ie_se_grupo_auditor_w := pls_obter_dados_auditor(cd_codigo_glosa_oco_w, null, nm_usuario_p, 'C', nr_seq_grupo_atual_p);
ie_existe_grupo_glosa_w := pls_obter_se_grupo_glosa(nr_seq_motivo_glosa_w);

if	(((ie_se_grupo_auditor_w = 'N') and (ie_tipo_glosa_oc_p = 'O')) or
	((ie_existe_grupo_glosa_w = 'S') and (ie_tipo_glosa_oc_p = 'G') and (ie_se_grupo_auditor_w = 'N'))) and
	(nvl(ie_consistir_grupo_p,'S') = 'S') then	
	/*'Voc� n�o faz parte do grupo de an�lise desta glosa/ocorr�ncia. Opera��o abortada.'*/
	wheb_mensagem_pck.exibir_mensagem_abort(173779);
end if;

/*Consist�ncia dos niveis de libera��o do auditor e da ocorr�ncia*/
if	(ie_tipo_glosa_oc_p = 'O') and
	(nvl(ie_consistir_grupo_p,'S') = 'S') then

	/*Nivel de libera��o da ocorrencia*/
	select	a.nr_seq_nivel_lib,
		a.nr_sequencia
	into	nr_seq_nivel_lib_w,
		nr_seq_oc_benef_w
	from	pls_ocorrencia a,
		pls_ocorrencia_benef b
	where	b.nr_seq_ocorrencia = a.nr_sequencia
	and	b.nr_sequencia 	= nr_seq_glosa_oc_p;

	select	max(nr_nivel_liberacao)
	into	nr_nivel_liberacao_w
	from	pls_nivel_liberacao
	where	nr_sequencia = nr_seq_nivel_lib_w;

	nr_nivel_liberacao_auditor_w := to_number(pls_obter_dados_auditor(cd_codigo_glosa_oco_w, null, nm_usuario_p, 'L', nr_seq_grupo_atual_p));
	
	if	(nvl(nr_nivel_liberacao_auditor_w,0) < nvl(nr_nivel_liberacao_w,0)) then	
		/*'Voc� n�o possui o n�vel de libera��o necess�rio para esta ocorr�ncia. Verifique.'*/
		wheb_mensagem_pck.exibir_mensagem_abort(173780);		
	end if;
end if;

/*Verificar se foi uma libera��o favor�vel ou desfavor�vel*/
select	decode(ie_tipo_motivo,'S','A','N','N'),	
	ds_motivo_liberacao
into	ie_tipo_motivo_w,	
	ds_motivo_liberacao_w
from	pls_mot_lib_analise_conta
where	nr_sequencia = nr_seq_mot_liberacao_p;

/*Obter quantidade e valor apresentado*/
begin
select	qt_apresentado,	
	vl_total_apres	
into	qt_apresentado_w,	
	vl_total_apres_w	
from	w_pls_resumo_conta	
where	(((nr_seq_item = nr_seq_conta_proc_w) 		and (ie_tipo_item = 'P'))
or 	 ((nr_seq_item = nr_seq_conta_mat_w)  		and (ie_tipo_item = 'M'))
or	 ((nr_seq_partic_proc = nr_seq_proc_partic_w)	and (ie_tipo_item = 'R')))
and	nr_seq_analise = nr_seq_analise_p;
exception
when others then	
	qt_apresentado_w := null;	
	vl_total_apres_w := null;	
end;

/*update	pls_analise_conta_item
set	qt_glosa	= decode(nvl(nr_seq_conta_proc,nvl(nr_seq_conta_mat,nvl(nr_seq_proc_partic,0))), 0, 0, decode(ie_tipo_motivo_w, 'A', 0, 'N', qt_glosa)),
	vl_glosa	= decode(nvl(nr_seq_conta_proc,nvl(nr_seq_conta_mat,nvl(nr_seq_proc_partic,0))), 0, 0, decode(ie_tipo_motivo_w, 'A', 0, 'N', vl_glosa))
where	nr_sequencia	= nr_seq_analise_conta_item_p;*/
	
if	(nvl(nr_seq_proc_p,0) > 0) then
	nr_seq_item_w	:= nr_seq_proc_p;
	ie_tipo_item_w	:= 'P';
elsif	(nvl(nr_seq_mat_p,0) > 0) then
	nr_seq_item_w	:= nr_seq_mat_p;
	ie_tipo_item_w	:= 'M';
elsif	(nvl(nr_seq_proc_partic_w,0) > 0) then
	nr_seq_item_w	:= nr_seq_proc_partic_w;
	ie_tipo_item_w	:= 'R';
end if;

if	(nvl(nr_seq_glosa_oc_p,0) > 0) then
	if	(ie_tipo_glosa_oc_p = 'G') then
		nr_seq_glosa_w		:= nr_seq_glosa_oc_p;
	else
		nr_seq_ocorrencia_w	:= nr_seq_glosa_oc_p;
	end if;
end if;

/*Obtem o tipo de hist�rico se � uma libera��o de glosa ou de ocorrencia*/
select	decode(ie_tipo_glosa_oc_p, 'G', 5, 'O', 6)
into	ie_tipo_historico_w
from	dual;

/*Atualizado a glosa / ocorrencia*/
update	pls_analise_conta_item
set	ie_status 	= ie_tipo_motivo_w,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao 	= sysdate,
	ie_situacao	= decode(ie_tipo_motivo_w, 'N', 'A', 'L', 'I', 'A', 'I')
where	nr_sequencia	= nr_seq_analise_conta_item_p;

/*Criado o parecer*/
insert into pls_analise_parecer_item
	(nr_sequencia, nr_seq_item, nr_seq_motivo,
	 dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
	 nm_usuario_nrec, ds_parecer, ie_tipo_motivo)
values	(pls_analise_parecer_item_seq.nextval, nr_seq_analise_conta_item_p, nr_seq_mot_liberacao_p,
	 sysdate, nm_usuario_p, sysdate,
	 nm_usuario_p, ds_observacao_p, ie_tipo_motivo_w);
	
select	decode(ie_tipo_motivo_w,'A','Libera��o favor�vel','N','Libera��o desfavor�vel','L','Libera��o parcial')
into	ds_tipo_motivo_w
from	dual;

ds_observacao_w := 	'Tipo de libera��o:  '||chr(13)||chr(10)||
			chr(9)||ds_tipo_motivo_w||chr(13)||chr(10)||chr(13)||chr(10)||
			'Motivo de libera��o:'||chr(13)||chr(10)||
			chr(9)||ds_motivo_liberacao_w||chr(13)||chr(10)||chr(13)||chr(10)||
			'Observa��o/Parecer: '||chr(13)||chr(10)||
			chr(9)||ds_observacao_p;
			
/*Adiciona o hist�rico da a��o*/
pls_inserir_hist_analise(nr_seq_conta_p, nr_seq_analise_p, ie_tipo_historico_w,
			 nr_seq_item_w, ie_tipo_item_w, nr_seq_ocorrencia_w,
			 nr_seq_glosa_w, ds_observacao_w, nr_seq_grupo_atual_p,
			 nm_usuario_p, cd_estabelecimento_p);
			 		
pls_analise_status_item(nr_seq_conta_p, nr_seq_mat_p, nr_seq_proc_p,
			nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p, 
			nr_seq_proc_partic_w);

pls_analise_status_pgto_pos(	nr_seq_conta_p, nr_seq_mat_p, nr_seq_proc_p,				
				nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);	
			
pls_analise_status_fat(		nr_seq_conta_p, nr_seq_mat_p, nr_seq_proc_p,				
				nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);

pls_atualizar_status_fat_pos(	nr_seq_conta_p, nr_seq_mat_p, nr_seq_proc_p,				
				nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p	);
commit;

end pls_conta_liberar_glosa_oc_pos;
/