create or replace
procedure intpd_recebe_agrupamento_lote(
			nr_sequencia_p	in	number,
			xml_p		in	xmltype) is 

ap_lote_agrup_w		ap_lote_agrup%rowtype;
ap_lote_w		ap_lote%rowtype;
reg_integracao_w		gerar_int_padrao.reg_integracao_conv;
ie_conversao_w		intpd_eventos_sistema.ie_conversao%type;
nr_seq_projeto_xml_w	intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_sistema_w		intpd_eventos_sistema.nr_seq_sistema%type;
nr_seq_regra_w		conversao_meio_externo.nr_seq_regra%type;
ie_sistema_externo_w	varchar2(15);
nr_seq_lote_agrup_w	number(10);
ds_erro_w		varchar2(4000);
i			integer;

/*'Efetua a consulta transformando o elemento XML num tipo de tabela'*/
cursor c01 is
select	*
from	xmltable('/STRUCTURE/BATCH_GROUPING' passing xml_p columns
	CD_CODIGO_EXTERNO		number(10)	path	'CD_EXTERNAL_CODE',
	CD_SETOR_ATENDIMENTO		number(10)	path	'CD_CARE_DEPARTMENT',
	NR_SEQ_CLASSIF			number(10)	path	'NR_SEQ_CLASSIFICATION',
	NR_SEQ_TURNO			number(10)	path	'NR_SEQ_SHIFT',
	xml_lotes				xmltype		path	'BATCHES');

c01_w	c01%rowtype;

/*'Efetua a consulta transformando o elemento XML num tipo de tabela'*/
cursor c02 is
select	*
from	xmltable('/BATCHES/BATCH' passing c01_w.xml_lotes columns
	NR_SEQUENCIA		number(10)	path	'NR_SEQ_BATCH');
	
c02_w	c02%rowtype;

begin
/*'Atualiza o status da fila para Em processamento'*/
update	intpd_fila_transmissao
set	ie_status = 'R'
where	nr_sequencia = nr_sequencia_p;

/*'Realiza o commit para n�o alterar o status de processamento em caso de rollback por existir consist�ncia. Existe tratamento de exce��o abaixo para colocar o status de erro em caso de falha'*/
commit;

/*'In�cio de controle de falha'*/
begin

/*'Busca os dados da regra do registro da fila que est� em processamento'*/
select	nvl(b.ie_conversao,'I'),
	nr_seq_sistema,
	nr_seq_projeto_xml,
	nr_seq_regra_conv
into	ie_conversao_w,
	nr_seq_sistema_w,
	nr_seq_projeto_xml_w,
	nr_seq_regra_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;

ie_sistema_externo_w	:=	nr_seq_sistema_w;

/*'Alimenta as informa��es iniciais de controle e consist�ncia de cada atributo do XML'*/
reg_integracao_w.nr_seq_fila_transmissao	:= nr_sequencia_p;
reg_integracao_w.ie_envio_recebe		:= 'R';
reg_integracao_w.ie_sistema_externo		:= ie_sistema_externo_w;
reg_integracao_w.ie_conversao		:= ie_conversao_w;
reg_integracao_w.nr_seq_projeto_xml		:= nr_seq_projeto_xml_w;
reg_integracao_w.nr_seq_regra_conversao	:= nr_seq_regra_w;
reg_integracao_w.qt_reg_log			:= 0;
reg_integracao_w.intpd_log_receb.delete;

open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin
	/*'Alimenta as informa��es de controle e consist�ncia referente ao Elemento a ser processado no momento. � importante manter dentro do cursor e n�o fora.'*/
	reg_integracao_w.nm_tabela		:=	'AP_LOTE_AGRUP';
	reg_integracao_w.nm_elemento	:=	'BATCH_GROUPING';
	reg_integracao_w.nr_seq_visao	:=	null;	
	
	/*'Consiste cada atributo do XML'*/
	intpd_processar_atributo(reg_integracao_w, 'CD_CODIGO_EXTERNO', c01_w.cd_codigo_externo, 'N', ap_lote_agrup_w.cd_codigo_externo); -- N�mero enviado pelo sistema WMS que realiza a integra��o.
	intpd_processar_atributo(reg_integracao_w, 'CD_SETOR_ATENDIMENTO', c01_w.cd_setor_atendimento, 'N', ap_lote_agrup_w.cd_setor_atendimento);
	intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CLASSIF', c01_w.nr_seq_classif, 'N', ap_lote_agrup_w.nr_seq_classif);
	intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_TURNO', c01_w.nr_seq_turno, 'N', ap_lote_agrup_w.nr_seq_turno);
	
	nr_seq_lote_agrup_w := 0;
		
	open c02;
	loop
	fetch c02 into	
		c02_w;
	exit when c02%notfound;
		begin
		/*'Alimenta as informa��es de controle e consist�ncia referente ao Elemento a ser processado no momento. � importante manter dentro do cursor e n�o fora.'*/
		reg_integracao_w.nm_tabela		:=	'AP_LOTE';
		reg_integracao_w.nm_elemento	:=	'BATCH';
		reg_integracao_w.nr_seq_visao	:=	null;		
		
		/*'Consiste cada atributo do XML'*/
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQUENCIA', c02_w.nr_sequencia, 'N', ap_lote_w.nr_sequencia);
		
		if	(reg_integracao_w.qt_reg_log = 0) then
			begin
			
			recebe_agrup_lote_integracao(
				ap_lote_w.nr_sequencia,
				ap_lote_agrup_w.cd_setor_atendimento,
				ap_lote_agrup_w.nr_seq_turno,
				ap_lote_agrup_w.nr_seq_classif,
				ap_lote_agrup_w.cd_codigo_externo,
				nr_seq_lote_agrup_w,
				ds_erro_w);
				
			if	(ds_erro_w is null) then
				update	intpd_fila_transmissao
				set	ie_status = 'S',
					nr_seq_documento = ap_lote_w.nr_sequencia
				where	nr_sequencia = nr_sequencia_p;
			else
				reg_integracao_w.intpd_log_receb(reg_integracao_w.qt_reg_log).ds_log	:=	substr(ds_erro_w,1,4000);
				reg_integracao_w.qt_reg_log						:=	reg_integracao_w.qt_reg_log + 1;
			end if;
			
			end;
		end if;
		
		end;
	end loop;
	close c02;
	end;
end loop;
close c01;
exception
when others then
	begin
	/*'Em caso de qualquer falha o sistema captura a mensagem de erro, efetua o rollback, atualiza o status para Erro e registra a falha ocorrida'*/
	ds_erro_w	:=	substr(sqlerrm,1,4000);
	rollback;
	update	intpd_fila_transmissao
	set	ie_status = 'E',
		ds_log = ds_erro_w
	where	nr_sequencia = nr_sequencia_p;
	end;
end;

if	(reg_integracao_w.qt_reg_log > 0) then
	begin
	/*'Em caso de qualquer consist�ncia o sistema efetua rollback, atualiza o status para Erro e registra todos os logs de consist�ncia'*/
	rollback;
	
	update	intpd_fila_transmissao
	set	ie_status = 'E',
		ds_log = ds_erro_w
	where	nr_sequencia = nr_sequencia_p;
	
	for i in 0..reg_integracao_w.qt_reg_log-1 loop
		intpd_gravar_log_recebimento(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');
	end loop;
	end;
end if;
	
commit;
end intpd_recebe_agrupamento_lote;
/