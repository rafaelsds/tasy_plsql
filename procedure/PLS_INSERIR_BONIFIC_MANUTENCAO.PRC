create or replace
procedure pls_inserir_bonific_manutencao
		(	nr_seq_vinculo_p	number,
			ie_opcao_p		varchar2,
			nm_usuario_p		Varchar2) is 

/*
ie_opcao_p
PA - Prospota de Ades�o
BP - Benefici�rio de plano de sa�de
BE - Benefici�rios de plano de sa�de(contatos)
CO- Contratos de plano de sa�de
PC - Pagador de plano de sa�de(contatos)
SC - SCA
*/
	
nr_seq_bonficacao_w	number(10);
tx_bonificacao_w	number(7,4);
vl_bonificacao_w	number(15,2);
qt_registros_w		number(10);
qt_alteracao_bonific_w	number(10);
dt_inicio_vigencia_w	date;
dt_fim_vigencia_w	date;
	
Cursor C01 is
	select	nr_seq_bonificacao,
		tx_bonificacao,
		vl_bonificacao,
		dt_inicio_vigencia,
		dt_fim_vigencia
	from	w_pls_bonificacao_vinculo
	where	nr_seq_proposta	= nr_seq_vinculo_p
	and	ie_opcao_p		= 'PA'
	union
	select	nr_seq_bonificacao,
		tx_bonificacao,
		vl_bonificacao,
		dt_inicio_vigencia,
		dt_fim_vigencia
	from	w_pls_bonificacao_vinculo
	where	nr_seq_benef_proposta	= nr_seq_vinculo_p
	and	ie_opcao_p		= 'BP';
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_bonficacao_w,
	tx_bonificacao_w,
	vl_bonificacao_w,
	dt_inicio_vigencia_w,
	dt_fim_vigencia_w;
exit when C01%notfound;
	begin
	if	(ie_opcao_p = 'PA') then
		select	count(*)
		into	qt_registros_w
		from	pls_bonificacao_vinculo
		where	nr_seq_proposta	= nr_seq_vinculo_p
		and	nr_seq_bonificacao	= nr_seq_bonficacao_w;
		
		if	(qt_registros_w	= 0) then
			insert into pls_bonificacao_vinculo
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					nr_seq_proposta,nr_seq_bonificacao,tx_bonificacao,vl_bonificacao,dt_inicio_vigencia,
					dt_fim_vigencia)
			values	(	pls_bonificacao_vinculo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					nr_seq_vinculo_p,nr_seq_bonficacao_w,tx_bonificacao_w,vl_bonificacao_w,dt_inicio_vigencia_w,
					dt_fim_vigencia_w);
		elsif	(qt_registros_w	> 0) then
			select	count(*)
			into	qt_alteracao_bonific_w
			from	pls_bonificacao_vinculo
			where	nr_seq_proposta				= nr_seq_vinculo_p
			and	nr_seq_bonificacao			= nr_seq_bonficacao_w
			and	nvl(tx_bonificacao,0)			= nvl(tx_bonificacao_w,0)
			and	nvl(vl_bonificacao,0)			= nvl(vl_bonificacao_w,0)
			and	nvl(dt_inicio_vigencia,'01/01/1900')	= nvl(dt_inicio_vigencia_w,'01/01/1900')
			and	nvl(dt_fim_vigencia,'01/01/1900') 	= nvl(dt_fim_vigencia_w,'01/01/1900');
			
			if	(qt_alteracao_bonific_w	= 0) then
				update	pls_bonificacao_vinculo
				set	tx_bonificacao		= tx_bonificacao_w,
					vl_bonificacao		= vl_bonificacao_w,
					dt_inicio_vigencia	= dt_inicio_vigencia_w,
					dt_fim_vigencia		= dt_fim_vigencia_w,
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p
				where	nr_seq_proposta	= nr_seq_vinculo_p
				and	nr_seq_bonificacao	= nr_seq_bonficacao_w;
			end if;	
		end if;
	elsif	(ie_opcao_p = 'BP') then
		select	count(*)
		into	qt_registros_w
		from	pls_bonificacao_vinculo
		where	nr_seq_segurado_prop	= nr_seq_vinculo_p
		and	nr_seq_bonificacao	= nr_seq_bonficacao_w;
		
		if	(qt_registros_w	= 0) then
			insert into pls_bonificacao_vinculo
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					nr_seq_segurado_prop,nr_seq_bonificacao,tx_bonificacao,vl_bonificacao,dt_inicio_vigencia,
					dt_fim_vigencia)
			values	(	pls_bonificacao_vinculo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					nr_seq_vinculo_p,nr_seq_bonficacao_w,tx_bonificacao_w,vl_bonificacao_w,dt_inicio_vigencia_w,
					dt_fim_vigencia_w);
		elsif	(qt_registros_w	> 0) then
			select	count(*)
			into	qt_alteracao_bonific_w
			from	pls_bonificacao_vinculo
			where	nr_seq_segurado_prop			= nr_seq_vinculo_p
			and	nr_seq_bonificacao			= nr_seq_bonficacao_w
			and	nvl(tx_bonificacao,0)			= nvl(tx_bonificacao_w,0)
			and	nvl(vl_bonificacao,0)			= nvl(vl_bonificacao_w,0)
			and	nvl(dt_inicio_vigencia,'01/01/1900')	= nvl(dt_inicio_vigencia_w,'01/01/1900')
			and	nvl(dt_fim_vigencia,'01/01/1900') 	= nvl(dt_fim_vigencia_w,'01/01/1900');
			
			if	(qt_alteracao_bonific_w	= 0) then
				update	pls_bonificacao_vinculo
				set	tx_bonificacao		= tx_bonificacao_w,
					vl_bonificacao		= vl_bonificacao_w,
					dt_inicio_vigencia	= dt_inicio_vigencia_w,
					dt_fim_vigencia		= dt_fim_vigencia_w,
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p
				where	nr_seq_segurado_prop	= nr_seq_vinculo_p
				and	nr_seq_bonificacao	= nr_seq_bonficacao_w;
			end if;	
		end if;
	end if;	
	end;
end loop;
close C01;

commit;

end pls_inserir_bonific_manutencao;
/
