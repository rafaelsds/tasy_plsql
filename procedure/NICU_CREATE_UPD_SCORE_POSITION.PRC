create or replace procedure nicu_create_upd_score_position (nr_sequencia_p        in nicu_positioning_score.nr_sequencia%type,
                                                            nr_seq_positioning_p  in nicu_positioning_score.nr_seq_positioning%type,
                                                            nr_seq_atendimento_p  in nicu_positioning.nr_seq_encounter%type,
                                                            ds_body_part_p        in nicu_positioning_score.ds_body_part%type,
                                                            nr_row_p              in nicu_positioning_score.nr_row%type,
                                                            vl_score_p            in nicu_positioning_score.vl_score%type,
                                                            mensagem_erro_p       out varchar2,
                                                            dt_positioning_p      in date default sysdate) is
  
  nr_seq_positioning_score_w  nicu_positioning_score.nr_sequencia%type;
  exeption_w exception;
begin
  if (nr_sequencia_p is null) then
     select nicu_positioning_score_seq.nextval
       into nr_seq_positioning_score_w
       from dual;
  else
    nr_seq_positioning_score_w := nr_sequencia_p;
  end if;

  begin
    insert into nicu_positioning_score
       (nr_sequencia,
        nr_seq_positioning,
        nr_row,
        ds_body_part,
        vl_score) 
    values
       (nr_seq_positioning_score_w,
        nr_seq_positioning_p,
        nr_row_p,
        ds_body_part_p,
        vl_score_p);
  exception
    when dup_val_on_index then
      begin
        update nicu_positioning_score
           set nr_seq_positioning = nr_seq_positioning_p,
               nr_row = nr_row_p,
               ds_body_part = ds_body_part_p,
               vl_score = vl_score_p
         where nr_sequencia = nr_seq_positioning_score_w;
      exception
        when others then
          mensagem_erro_p := expressao_pck.obter_desc_expressao(776218)||' Error: '|| sqlerrm;
          raise exeption_w;
      end;
  when others then
     mensagem_erro_p := expressao_pck.obter_desc_expressao(776218)||' Error: '|| sqlerrm;
     raise exeption_w;
  end;

  commit;
exception
  when exeption_w then
     rollback;
  when others then
    mensagem_erro_p := expressao_pck.obter_desc_expressao(776218)||' Error: '|| sqlerrm;
end nicu_create_upd_score_position;
/
