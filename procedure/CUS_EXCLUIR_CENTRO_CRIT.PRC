create or replace
procedure cus_excluir_centro_crit(	cd_sequencia_criterio_p		number,
				cd_estabelecimento_p		number,
				cd_centro_controle_dest_p		number) is

begin

delete 	
from 	criterio_distr_orc_dest
where	cd_sequencia_criterio	= cd_sequencia_criterio_p
and	cd_estabelecimento		= cd_estabelecimento_p
and	cd_centro_controle_dest	= cd_centro_controle_dest_p;

commit;

end cus_excluir_centro_crit;
/