create or replace 
procedure ml_atualizar_integracao_carta(
                            nr_sequencia_p carta_medica.nr_sequencia%type, 
                            nm_usuario_p carta_medica.nm_usuario%type) is

begin

	update	carta_medica
	   set	dt_integracao= sysdate,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where nr_sequencia = nr_sequencia_p
          and dt_integracao is null
          and ie_preliminar = 'N';

commit;

end ml_atualizar_integracao_carta;
/

