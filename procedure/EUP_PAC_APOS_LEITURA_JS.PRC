create or replace
procedure eup_pac_apos_leitura_js(
					nr_atend_p		number,
					nr_atendimento_p	number,
					cd_pessoa_fisica_p	varchar2,
					cd_estab_p		number,
					nm_usuario_p		Varchar2,
					ie_cancelado_p	    out	varchar2,
					ds_msg_atend_canc_p out varchar2,
					qt_avisos_p	    out number,
					ie_mostrar_debito_p out varchar2,
					qt_dispensa_p	    out number,
					ds_msg_dispensa_p   out varchar2,
					ds_msg_origem_pront_p out varchar2,
					ie_laudo_same_p     out varchar2,
					ds_msg_atend_sem_alta_p out varchar2,
					ds_msg_atend_cancelado_p out varchar2) is 

begin

if	(nvl(nr_atend_p,0) > 0) then
	ds_msg_atend_cancelado_p := eup_obter_nm_usuario_cancel(nr_atend_p);
elsif	(nvl(nr_atendimento_p,0) > 0) then
	ds_msg_atend_cancelado_p := eup_obter_nm_usuario_cancel(nr_atendimento_p);
end if;

if	(nvl(nr_atendimento_p,0) > 0) then

	select	decode(count(*),0,'N','S') 
	into	ie_cancelado_p
	from	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_p
	and   	dt_cancelamento is not null;

	ds_msg_atend_canc_p := substr(obter_texto_tasy(244742,philips_param_pck.get_nr_seq_idioma),1,255);
	
	select 	count(*) 
	into	qt_dispensa_p
	from 	dispensa_cob_diaria 
	where 	nr_atendimento = nr_atendimento_p
	and 	dt_liberacao is not null;

	ds_msg_dispensa_p := substr(obter_texto_tasy(120858,philips_param_pck.get_nr_seq_idioma),1,255);
end if;

if	(cd_pessoa_fisica_p is not null) then

	select 	count(*)
	into	qt_avisos_p
	from    cartao_fidelidade a, 
		pf_cartao_fidelidade b
	where   a.nr_sequencia = b.nr_seq_cartao
	and     a.ie_situacao  = 'A'
	and     cd_pessoa_fisica  = cd_pessoa_fisica_p
	and 	(( a.ds_cartao is not null) or 
	         (a.ds_observacao is not null) or 
		 (b.ds_observacao is not null));
		 
	ie_mostrar_debito_p := obter_se_mostra_debito_estab(cd_pessoa_fisica_p, cd_estab_p);
	
	Verifica_origem_prontuario(cd_pessoa_fisica_p, ds_msg_origem_pront_p);
	
	ie_laudo_same_p := obter_se_pac_laudo_same(cd_pessoa_fisica_p);   

end if;

consistir_atendimento_sem_alta(cd_pessoa_fisica_p, nr_atendimento_p, obter_perfil_ativo, wheb_usuario_pck.get_cd_estabelecimento, nm_usuario_p, ds_msg_atend_sem_alta_p);

commit;

end eup_pac_apos_leitura_js;
/
