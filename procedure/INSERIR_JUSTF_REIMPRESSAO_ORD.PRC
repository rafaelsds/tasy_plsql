create or replace 
procedure inserir_justf_reimpressao_ord(
		ds_justif_reimpressao_p		varchar2,
		nr_sequencia_p			number,
		nm_usuario_p			varchar2) is
		
begin

if 	(nr_sequencia_p is not null) and
	(ds_justif_reimpressao_p is not null) then
		
	update	can_ordem_prod
	set	ds_justif_reimpressao	= ds_justif_reimpressao_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencia_p;
	
	commit;
	
end if;

end inserir_justf_reimpressao_ord;
/