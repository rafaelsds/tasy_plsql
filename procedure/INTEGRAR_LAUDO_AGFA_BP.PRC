CREATE OR REPLACE
PROCEDURE INTEGRAR_LAUDO_AGFA is

oru_01_his_date_w       date;
oru_02_status_his_w     varchar2(1);
obr_03_acc_number_w     varchar2(50);
msh_07_date_w           date;
pid_02_id_w             varchar2(48);
obr_32_author_id_w      varchar2(50);
obr_32_author_name_w    varchar2(50);
obr_27_validate_date_w  date;
obr_03_batch_id_w       varchar(22);
obr_25_status_rep_w     Number(10);
obx_05_observation_w    clob;
oru_03_id_report_w      varchar2(10);
obr_02_qp_exam_id_w     varchar2(30);

ie_status_laudo_w       varchar2(15);
qt_minuto_integra_w     number(10);
nm_usuario_w            varchar2(15);
nr_prescricao_w         number(14);
nr_seq_prescr_w         number(6);
nr_atendimento_w        number(10);
cd_setor_atendimento_w  number(5);
dt_entrada_unidade_w    date;
nr_seq_propaci_w        number(10);
nr_laudo_w              number(10);
ds_titulo_laudo_w       varchar2(255)    := 'Integração TasyXAGFA';
cd_laudo_padrao_w       number(5)    := null;
ds_laudo_w              long;
cd_protocolo_w          number(10)    := null;
cd_projeto_w            number(6)    := null;
dt_prev_entrega_w       date        := null;
dt_entrega_real_w       date        := null;
nr_controle_w           number(15)    := null;
nr_seq_motivo_parada_w  number(10)    := null;
ds_erro_w               varchar2(2000);
nr_seq_laudo_w          number(10)      := 0;
nr_telefone_w           varchar2(20);
id_sms_w                number(10);
nr_seq_laudo_ww         number(10)      := 0;
ie_status_laudo_sms_w   varchar2(15);
ds_mensagem_sms_w       varchar2(255);
ie_medico_w             varchar2(1);
ds_remetente_w          varchar2(20);
dt_liberacao_w          date;
dt_aprovacao_w          date;
dt_seg_aprovacao_w      date;
nr_seq_superior_w	number(10);
ie_erro_laudo_w		varchar2(1) := 'N';
nr_seq_laudo_atual_w	number(10);

cd_medico_w				varchar(10);

c001			INTEGER;
ds_conteudo_1_w		varchar2(32764);
ds_conteudo_2_w		varchar2(32764);
retorno_w Number(5);

Cursor C01 is
        select  oru_01_his_date,
                oru_02_status_his,
                obr_03_acc_number,
                msh_07_date,
                pid_02_id,
                obr_32_author_id,
                obr_32_author_name,
                obr_27_validate_date,
                obr_03_batch_id,
                somente_numero(obr_25_status_rep),
                obx_05_observation,
                oru_03_id_report,
                obr_02_qp_exam_id
        from    agfa_oru_out
        where   oru_02_status_his = 'N'
        order by oru_01_his_date;

Cursor C02 is
        select  nvl(qt_minuto_integra,0)
        from    regra_integracao_laudo
        where   nvl(cd_setor_atendimento, cd_setor_atendimento_w) = cd_setor_atendimento_w
        and     nvl(ie_status_laudo, obr_25_status_rep_w) = obr_25_status_rep_w
        order by nvl(ie_status_laudo, 'AAAAAA'), nvl(cd_setor_atendimento,0);

Cursor C03 is --Laudo Cancelado
        select  distinct substr(a.nr_telefone_celular,1,20)
        from    pessoa_fisica a,
                log_acesso_laudo b
        where   b.cd_pessoa_fisica = a.cd_pessoa_fisica
        and     b.nr_seq_laudo = nr_seq_laudo_ww
        and     a.nr_telefone_celular is not null
        and     exists (select  1
                        from    dual
                        where   ie_medico_w = 'N'
                        union all
                        select  1
                        from    medico c
                        where   c.cd_pessoa_fisica = b.cd_pessoa_fisica
                        and     ie_medico_w = 'S');


Cursor C04 is --Laudo  liberado
        select  distinct substr(a.nr_telefone_celular,1,20)
        from    pessoa_fisica a,
                log_acesso_laudo c,
                laudo_paciente_copia b
        where   c.cd_pessoa_fisica = a.cd_pessoa_fisica
        and     b.nr_seq_laudo = c.nr_seq_laudo
        and     a.nr_telefone_celular is not null
        and     b.nr_atendimento = nr_atendimento_w
        and     b.nr_seq_proc   = nr_seq_propaci_w
        and     nvl(b.ie_status_laudo,'A') = 'LC'
        and     exists (select  1
                        from    dual
                        where   ie_medico_w = 'N'
                        union all
                        select  1
                        from    medico d
                        where   d.cd_pessoa_fisica = c.cd_pessoa_fisica
                        and     ie_medico_w = 'S');


Cursor C06 is --Laudo modificado
        select  distinct substr(a.nr_telefone_celular,1,20)
        from    pessoa_fisica a,
                log_acesso_laudo c,
                laudo_paciente_copia b
        where   c.cd_pessoa_fisica = a.cd_pessoa_fisica
        and     b.nr_seq_laudo = c.nr_seq_laudo
        and     a.nr_telefone_celular is not null
        and     b.nr_atendimento = nr_atendimento_w
        and     b.nr_seq_proc   = nr_seq_propaci_w
        and     nvl(b.ie_status_laudo,'A') = 'LD'
        and     exists (select  1
                        from    dual
                        where   ie_medico_w = 'N'
                        union all
                        select  1
                        from    medico d
                        where   d.cd_pessoa_fisica = c.cd_pessoa_fisica
                        and     ie_medico_w = 'S');

Cursor C07 is --Laudo criado
        select  distinct substr(a.nr_telefone_celular,1,20)
        from    pessoa_fisica a,
                log_acesso_laudo c,
                laudo_paciente_copia b
        where   c.cd_pessoa_fisica = a.cd_pessoa_fisica
        and     b.nr_seq_laudo = c.nr_seq_laudo
        and     a.nr_telefone_celular is not null
        and     b.nr_atendimento = nr_atendimento_w
        and     b.nr_seq_proc   = nr_seq_propaci_w
        and     nvl(b.ie_status_laudo,'A') = 'LC'
        and     exists (select  1
                        from    dual
                        where   ie_medico_w = 'N'
                        union all
                        select  1
                        from    medico d
                        where   d.cd_pessoa_fisica = c.cd_pessoa_fisica
                        and     ie_medico_w = 'S');

Cursor C08 is --Laudo parcialmente validado
        select  distinct substr(a.nr_telefone_celular,1,20)
        from    pessoa_fisica a,
                log_acesso_laudo c,
                laudo_paciente_copia b
        where   c.cd_pessoa_fisica = a.cd_pessoa_fisica
        and     b.nr_seq_laudo = c.nr_seq_laudo
        and     a.nr_telefone_celular is not null
        and     b.nr_atendimento = nr_atendimento_w
        and     b.nr_seq_proc   = nr_seq_propaci_w
        and     nvl(b.ie_status_laudo,'A') = 'LAP'
        and     exists (select  1
                        from    dual
                        where   ie_medico_w = 'N'
                        union all
                        select  1
                        from    medico d
                        where   d.cd_pessoa_fisica = c.cd_pessoa_fisica
                        and     ie_medico_w = 'S');

Cursor C09 is --Adendo criado
        select  distinct substr(a.nr_telefone_celular,1,20)
        from    pessoa_fisica a,
                log_acesso_laudo c,
                laudo_paciente_copia b
        where   c.cd_pessoa_fisica = a.cd_pessoa_fisica
        and     b.nr_seq_laudo = c.nr_seq_laudo
        and     a.nr_telefone_celular is not null
        and     b.nr_atendimento = nr_atendimento_w
        and     b.nr_seq_proc   = nr_seq_propaci_w
        and     nvl(b.ie_status_laudo,'A') = 'AD'
        and     exists (select  1
                        from    dual
                        where   ie_medico_w = 'N'
                        union all
                        select  1
                        from    medico d
                        where   d.cd_pessoa_fisica = c.cd_pessoa_fisica
                        and     ie_medico_w = 'S');

Cursor C10 is --Adendo validado
        select  distinct substr(a.nr_telefone_celular,1,20)
        from    pessoa_fisica a,
                log_acesso_laudo c,
                laudo_paciente_copia b
        where   c.cd_pessoa_fisica = a.cd_pessoa_fisica
        and     b.nr_seq_laudo = c.nr_seq_laudo
        and     a.nr_telefone_celular is not null
        and     b.nr_atendimento = nr_atendimento_w
        and     b.nr_seq_proc   = nr_seq_propaci_w
        and     nvl(b.ie_status_laudo,'A') = 'AL'
        and     exists (select  1
                        from    dual
                        where   ie_medico_w = 'N'
                        union all
                        select  1
                        from    medico d
                        where   d.cd_pessoa_fisica = c.cd_pessoa_fisica
                        and     ie_medico_w = 'S');


Cursor C05 is
        select  ie_status_laudo,
                ds_mensagem,
                ie_medico
        from    regra_integra_laudo_sms;

begin

open C01;
loop
fetch C01 into
        oru_01_his_date_w,
        oru_02_status_his_w,
        obr_03_acc_number_w,
        msh_07_date_w,
        pid_02_id_w,
        obr_32_author_id_w,
        obr_32_author_name_w,
        obr_27_validate_date_w,
        obr_03_batch_id_w,
        obr_25_status_rep_w,
        obx_05_observation_w,
        oru_03_id_report_w,
        obr_02_qp_exam_id_w;
exit when C01%notfound;
        begin
        begin
        nr_atendimento_w        := null;
        nr_prescricao_w         := null;
        nr_seq_prescr_w         := null;
        ds_titulo_laudo_w       := '';
        nm_usuario_w            := '';
        nr_seq_laudo_w          := 0;
        nr_seq_laudo_ww         := 0;

				insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, 'accession:'||obr_03_acc_number_w || ' ponto 1');

        select  nvl(max(nr_prescricao),null),
                nvl(max(nr_sequencia),null),
                nvl(max(substr(obter_desc_prescr_proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,255)),ds_titulo_laudo_w)
        into    nr_prescricao_w,
                nr_seq_prescr_w,
                ds_titulo_laudo_w
        from    prescr_procedimento
        where    nr_acesso_dicom = obr_03_acc_number_w;

				insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, 'accession:'||obr_03_acc_number_w || ' prescr:'||nr_prescricao_w||' exam:'||obr_02_qp_exam_id_w);

        if      (nr_prescricao_w is null) and
                (obr_02_qp_exam_id_w is not null) and
                (Instr(obr_02_qp_exam_id_w,'-') > 0) Then
                begin
                nr_prescricao_w := substr(obr_02_qp_exam_id_w,1,instr(obr_02_qp_exam_id_w,'-') - 1);
                nr_seq_prescr_w := substr(obr_02_qp_exam_id_w,instr(obr_02_qp_exam_id_w,'-') + 1,length(obr_02_qp_exam_id_w));

				insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, 'accession:'||obr_03_acc_number_w || ' prescr:'||nr_prescricao_w||' seq:' ||nr_seq_prescr_w);

                Agfa_Prescr_Proc_Acc_Number(    nr_prescricao_w,
                                                nr_seq_prescr_w,
                                                obr_03_acc_number_w,
                                                ds_erro_w);

				insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, 'accession:'||obr_03_acc_number_w || ' erro:' || ds_erro_w);

                select  nvl(max(substr(obter_desc_prescr_proc(cd_procedimento, ie_origem_proced, nr_seq_proc_interno),1,255)),ds_titulo_laudo_w)
                into    ds_titulo_laudo_w
                from    prescr_procedimento
                where   nr_acesso_dicom = obr_03_acc_number_w;

                /*if    (ds_erro_w Is Not Null) Then
                        :New.ORU_02_STATUS_HIS := 'E';
                end If; * fazer update*/
                /* Registro processado como Erro */
                if      (ds_erro_w is not null) then
                        begin
                        update  agfa_oru_out
                        set     oru_02_status_his = 'E'
                        where   obr_03_acc_number = obr_03_acc_number_w
                        and     msh_07_date = msh_07_date_w;--Verificar chave
                        end;
                end if;
                end;
        end if;


        if      (nr_prescricao_w is not null) and
                (oru_02_status_his_w = 'N') then
                begin
                select  nvl(max(nr_atendimento), null),
                        nvl(max(cd_setor_atendimento), null),
                        nvl(max(dt_entrada_unidade), null),
                        nvl(max(nr_sequencia), null)
                into    nr_atendimento_w,
                        cd_setor_atendimento_w,
                        dt_entrada_unidade_w,
                        nr_seq_propaci_w
                from    procedimento_paciente
                where   nr_prescricao = nr_prescricao_w
                and     nr_sequencia_prescricao = nr_seq_prescr_w;
                end;
        end if;

        qt_minuto_integra_w     := -1;

        open C02;
        loop
        fetch C02 into
                qt_minuto_integra_w;
        exit when C02%notfound;
                begin
                qt_minuto_integra_w := qt_minuto_integra_w;
                end;
        end loop;
        close C02;

insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, 'accession:'||obr_03_acc_number_w || ' prescr:'||nr_prescricao_w||' qtmin:'||qt_minuto_integra_w);

        if      (nr_atendimento_w is not null) and
                ((qt_minuto_integra_w > -1) and
                ((msh_07_date_w + (Dividir(qt_minuto_integra_w, 1440))) <= sysdate)) then
                begin

insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, 'accession:'||obr_03_acc_number_w || ' prescr:'||nr_prescricao_w||' entrou');

				select 	nvl(max(cd_pessoa_fisica),2991910)
				into	cd_medico_w
				from 	medico		
				where	cd_pessoa_fisica = obr_32_author_id_w;

				obr_32_author_id_w := cd_medico_w;
				
										
                select   nvl(max(nm_usuario),'AGFA_ORU_OUT')
                into     nm_usuario_w
                from     usuario
                where    cd_pessoa_fisica = obr_32_author_id_w;

                select  nvl(max(nr_sequencia),0)
                into    nr_seq_laudo_w
                from    laudo_paciente
                where   nr_atendimento  = nr_atendimento_w
                and     nr_seq_proc     = nr_seq_propaci_w;

                dt_seg_aprovacao_w      := obr_27_validate_date_w;
                dt_aprovacao_w          := obr_27_validate_date_w;
                dt_liberacao_w          := obr_27_validate_date_w;

                if      (obr_25_status_rep_w in (1,2,5)) then
                        begin
                        dt_seg_aprovacao_w      := null;
                        dt_aprovacao_w          := null;
                        dt_liberacao_w          := null;
                        end;
                elsif   (obr_25_status_rep_w = 3) then
                        begin
                        dt_liberacao_w          := null;
                        end;
                end if;

                ie_status_laudo_w := 'LC';
                if      --(nr_seq_laudo_w > 0) and
                        (obr_25_status_rep_w <> 5) then
                        begin
                        if      (obr_25_status_rep_w = 6) then
                                begin
                                select  nvl(nvl(max(nr_sequencia),nr_seq_laudo_w),0)
                                into    nr_seq_laudo_w
                                from    laudo_paciente
                                where   nr_atendimento  = nr_atendimento_w
                                and     nr_seq_proc     = nr_seq_propaci_w
                                and     ie_status_laudo = 'AD';

				nr_seq_superior_w := nr_seq_laudo_w;

                                ie_status_laudo_w := 'AL';
                                end;
                        end if;

                        if      (nr_seq_laudo_w > 0) then
                                gerar_copia_laudo(nr_seq_laudo_w, nm_usuario_w);
                        end if;

			/*delete  laudo_paciente
			where   nr_sequencia = nr_seq_laudo_w;*/
			cancelar_laudo_paciente(nr_seq_laudo_w, 'C', nm_usuario_w, null);

                        if      (obr_25_status_rep_w = 0) and
                                (nr_seq_laudo_w > 0) then
                                begin
                                update  laudo_paciente_copia
                                set     ds_titulo_laudo = substr(ds_titulo_laudo || ' - Cancelado',1,255),
                                        ie_status_laudo = 'LC'
                                where   nr_seq_laudo = nr_seq_laudo_w;

                                nr_seq_laudo_ww := nr_seq_laudo_w;
                                nr_seq_laudo_w := -1;
                                end;
                        elsif   (obr_25_status_rep_w = 2) then
                                begin
                                update  laudo_paciente_copia
                                set     ds_titulo_laudo = substr(ds_titulo_laudo || ' - Modificado',1,255),
                                        ie_status_laudo = 'LD'
                                where   nr_seq_laudo = nr_seq_laudo_w;

                                ie_status_laudo_w := 'LD';
                                end;
                        elsif   (obr_25_status_rep_w = 3) then
                                begin
                                update  laudo_paciente_copia
                                set     ds_titulo_laudo = substr(ds_titulo_laudo || ' - Parcialmente validado',1,255),
                                        ie_status_laudo = 'LAP'
                                where   nr_seq_laudo = nr_seq_laudo_w;

                                ie_status_laudo_w := 'LAP';
                                end;
                        elsif   (obr_25_status_rep_w = 4) then
                                begin
                                update  laudo_paciente_copia
                                set     ds_titulo_laudo = substr(ds_titulo_laudo || ' - Validado',1,255),
                                        ie_status_laudo = 'LL'
                                where   nr_seq_laudo = nr_seq_laudo_w;

                                ie_status_laudo_w := 'LL';
                                end;
                        end if;
                        end;
                end if;

				insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, 'accession:'||obr_03_acc_number_w || ' prescr:'||nr_prescricao_w||' status_rep: '||obr_25_status_rep_w);

                if      (obr_25_status_rep_w = 0) and
                        (nr_seq_laudo_w = 0)then
                        begin
                        nr_seq_laudo_w := -2;
                        end;
                end if;



				insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, 'accession:'||obr_03_acc_number_w || ' prescr:'||nr_prescricao_w||' nr_seq_laudo: '||nr_seq_laudo_w);

                if      (nr_seq_laudo_w > -1) then
                    begin
                        select  nvl(max(nr_laudo),0) + 1
                        into    nr_laudo_w
                        from    laudo_paciente
                        where   nr_atendimento = nr_atendimento_w;

						begin

									/*select	replace(replace(replace(dbms_lob.substr(obx_05_observation_w, dbms_lob.GetLength(obx_05_observation_w), 1), CHR(10), CHR(13)), '[CR-LF]', CHR(13)), CHR(47) || 'n', CHR(13))
									into    ds_laudo_w
									from    dual;*/

                  select  replace(dbms_lob.substr(obx_05_observation_w, dbms_lob.GetLength(obx_05_observation_w), 1),chr(13),chr(13)||chr(10))
                  into    ds_laudo_w
									from    dual;

						exception
							when others then
							ie_erro_laudo_w := 'S';
							ds_laudo_w := null;
							ds_conteudo_1_w := dbms_lob.SUBSTR(obx_05_observation_w,32764,1);
							ds_conteudo_2_w := dbms_lob.SUBSTR(obx_05_observation_w,32764,32765);
						end;

						insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, substr('accession:'||obr_03_acc_number_w || ' prescr:'||nr_prescricao_w||' TXT:'||ds_laudo_w,1,1999));

						select  laudo_paciente_seq.NextVal
						into	nr_seq_laudo_atual_w
						from	dual;

                        insert into laudo_paciente (    NR_SEQUENCIA,        NR_ATENDIMENTO,
                                                        DT_ENTRADA_UNIDADE,    NR_LAUDO,
                                                        NM_USUARIO,        DT_ATUALIZACAO,
                                                        CD_MEDICO_RESP,        DS_TITULO_LAUDO,
                                                        DT_LAUDO,        CD_LAUDO_PADRAO,
                                                        IE_NORMAL,        DT_EXAME,
                                                        NR_PRESCRICAO,        DS_LAUDO,
                                                        DT_APROVACAO,        NM_USUARIO_APROVACAO,
                                                        CD_PROTOCOLO,        CD_PROJETO,
                                                        NR_SEQ_PROC,        NR_SEQ_PRESCRICAO,
                                                        DT_LIBERACAO,        DT_PREV_ENTREGA,
                                                        DT_REAL_ENTREGA,    QT_IMAGEM,
                                                        NR_CONTROLE,        DT_SEG_APROVACAO,
                                                        NM_USUARIO_SEG_APROV,    NR_SEQ_MOTIVO_PARADA,
                                                        CD_SETOR_ATENDIMENTO,    NR_EXAME,
                                                        CD_MEDICO_AUX,        DT_ENVELOPADO,
                                                        DT_IMPRESSAO,        DT_FIM_DIGITACAO,
                                                        NM_USUARIO_DIGITACAO,    DT_INICIO_DIGITACAO,
                                                        DT_INTEGRACAO,        NM_MEDICO_SOLICITANTE,
                                                        IE_MIDIA_ENTREGUE,    CD_TECNICO_RESP,
                                                        CD_SETOR_USUARIO,    NM_USUARIO_CANCEL,
                                                        DT_CANCELAMENTO,    NM_USUARIO_LIBERACAO,
                                                        IE_STATUS_LAUDO,	nr_seq_superior)
                        values (                        nr_seq_laudo_atual_w,    nr_atendimento_w,
                                                        dt_entrada_unidade_w,        nr_laudo_w,
                                                        nm_usuario_w,            msh_07_date_w,
                                                        obr_32_author_id_w,        ds_titulo_laudo_w,
                                                        msh_07_date_w,        cd_laudo_padrao_w,
                                                        'S',                msh_07_date_w,
                                                        nr_prescricao_w,        ds_laudo_w,
                                                        dt_aprovacao_w,    nm_usuario_w,
                                                        cd_protocolo_w,            cd_projeto_w,
                                                        nr_seq_propaci_w,        nr_seq_prescr_w,
                                                        dt_liberacao_w,    dt_prev_entrega_w,
                                                        dt_entrega_real_w,        0,
                                                        nr_controle_w,            dt_seg_aprovacao_w,
                                                        nm_usuario_w,            nr_seq_motivo_parada_w,
                                                        cd_setor_atendimento_w,  oru_03_id_report_w,
                                                        null,                null,
                                                        null,                null,
                                                        null,                null,
                                                        sysdate,            null,
                                                        null,                null,
                                                        null,                null,
                                                        null,                null,
                                                        ie_status_laudo_w,	nr_seq_superior_w);



						--Vincular Laudo e Conta
						vincular_proced_laudo(nr_seq_laudo_atual_w, nr_seq_propaci_w, nm_usuario_w);

						if (ie_erro_laudo_w = 'S') then

							insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
										values  (sysdate, 'integra_laudo', -44441, substr('accession:'||obr_03_acc_number_w || ' prescr:'||nr_prescricao_w||' Update Texto:'||ds_conteudo_1_w || ds_conteudo_2_w,1,1900));

							ie_erro_laudo_w := 'N';

							C001 := DBMS_SQL.OPEN_CURSOR;
							DBMS_SQL.PARSE(C001, ' update  laudo_paciente set ds_laudo = :ds_texto where nr_sequencia = ' || nr_seq_laudo_atual_w,
							dbms_sql.Native);
							DBMS_SQL.BIND_VARIABLE(C001, 'DS_TEXTO', ds_conteudo_1_w || ds_conteudo_2_w);
							retorno_w := DBMS_SQL.EXECUTE(c001);
							DBMS_SQL.CLOSE_CURSOR(C001);

						end if;
                    end;
                end if;

                open C05;
                loop
                fetch C05 into
                        ie_status_laudo_sms_w,
                        ds_mensagem_sms_w,
                        ie_medico_w;
                exit when C05%notfound;
                        begin

                        if      (ie_status_laudo_sms_w = 'LC') and
                                (obr_25_status_rep_w = 0) then --Laudo ivalidado
                                begin
                                open C03;
                                loop
                                fetch C03 into
                                        nr_telefone_w;
                                exit when C03%notfound;
                                        begin
                                        wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_w, ds_mensagem_sms_w, nm_usuario_w,id_sms_w);
                                        end;
                                end loop;
                                close C03;
                                end;
                        elsif   (ie_status_laudo_sms_w = 'LL') and
                                (obr_25_status_rep_w = 4) then --Laudo liberado
                                begin
                                open C04;
                                loop
                                fetch C04 into
                                        nr_telefone_w;
                                exit when C04%notfound;
                                        begin
                                        wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_w, ds_mensagem_sms_w, nm_usuario_w,id_sms_w);
                                        end;
                                end loop;
                                close C04;
                                end;
                        elsif   (ie_status_laudo_sms_w = 'LD') and
                                (obr_25_status_rep_w = 2) then --Laudo modificado
                                begin
                                open C06;
                                loop
                                fetch C06 into
                                        nr_telefone_w;
                                exit when C06%notfound;
                                        begin
                                        wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_w, ds_mensagem_sms_w, nm_usuario_w,id_sms_w);
                                        end;
                                end loop;
                                close C06;

                        /*elsif (ie_status_laudo_sms_w = 'LC') and
                                (obr_25_status_rep_w = 1) then --Laudo criado

                                open C07;
                                loop
                                fetch C07 into
                                        nr_telefone_w;
                                exit when C07%notfound;
                                        begin
                                        --wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_w, ds_mensagem_sms_
                                        insert into caldas values (ds_mensagem_sms_w||nr_telefone_w||'2');
                                        end;
                                end loop;
                                close C07;
                        */
                                end;
                        elsif   (ie_status_laudo_sms_w = 'LAP') and
                                (obr_25_status_rep_w = 3) then --Laudo parcialmente validado
                                begin
                                open C08;
                                loop
                                fetch C08 into
                                        nr_telefone_w;
                                exit when C08%notfound;
                                        begin
                                        wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_w, ds_mensagem_sms_w, nm_usuario_w,id_sms_w);
                                        end;
                                end loop;
                                close C08;

                        /*elsif (ie_status_laudo_sms_w = 'AD') and
                                (obr_25_status_rep_w = 5) then --Adendo criado

                                open C09;
                                loop
                                fetch C09 into
                                        nr_telefone_w;
                                exit when C09%notfound;
                                        begin
                                        --wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_w, ds_mensagem_sms_
                                        insert into caldas values (ds_mensagem_sms_w||nr_telefone_w||'2');
                                        end;
                                end loop;
                                close C09;
                        */
                                end;
                        elsif   (ie_status_laudo_sms_w = 'AL') and
                                (obr_25_status_rep_w = 6) then --Adendo validado
                                begin
                                open C10;
                                loop
                                fetch C10 into
                                        nr_telefone_w;
                                exit when C10%notfound;
                                        begin
                                        wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_w, ds_mensagem_sms_w, nm_usuario_w,id_sms_w);
                                        end;
                                end loop;
                                close C10;
                                end;
                        end if;
                        end;
                end loop;
                close C05;

                --SMS
                /*if    (obr_25_status_rep_w = 4) then --Laudo validado
                        open C04;
                        loop
                        fetch C04 into
                                nr_telefone_w;
                        exit when C04%notfound;
                                begin
                                --wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_w, ds_mensagem_p, nm_usuari
                                end;
                        end loop;
                        close C01;

                elsif   (obr_25_status_rep_w = 0) then --Laudo ivalidado
                        open C03;
                        loop
                        fetch C03 into
                                nr_telefone_ww;
                        exit when C03%notfound;
                                begin
                                --wheb_sms.enviar_sms(ds_remetente_w, nr_telefone_ww, ds_mensagem_p, nm_usuar
                                end;
                        end loop;
                        close C03;
                end if;*/

                /*:new.ORU_02_STATUS_HIS    := 'T';
                if      (nr_seq_laudo_w = -2) then
                        :new.ORU_02_STATUS_HIS    := 'I';
                end if;  */
                if      (nr_seq_laudo_w = -2) then
                        begin
                        update  agfa_oru_out
                        set     oru_02_status_his = 'I'
                        where   obr_03_acc_number = obr_03_acc_number_w
                        and     msh_07_date = msh_07_date_w;--Verificar chave
                        end;
                else
                        begin
                        update  agfa_oru_out
                        set     oru_02_status_his = 'T'
                        where   obr_03_acc_number = obr_03_acc_number_w
                        and     msh_07_date = msh_07_date_w;--Verificar chave
                        end;
                end if;

                end;
        end if;
                exception
                when others then

                update  agfa_oru_out
                set     oru_02_status_his = 'E'
                where   obr_03_acc_number = obr_03_acc_number_w
                and     msh_07_date = msh_07_date_w;--Verificar chave

                ds_erro_w        := substr(sqlerrm,1,1900);
                insert into log_tasy    (dt_atualizacao, nm_usuario, cd_log, ds_log)
                        values    (sysdate, nm_usuario_w, -33333, ds_erro_w);
                end;
        end;
end loop;
close C01;

COMMIT;

end INTEGRAR_LAUDO_AGFA;

/
