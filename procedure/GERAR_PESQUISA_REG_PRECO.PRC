create or replace
procedure gerar_pesquisa_reg_preco(	nr_seq_reg_compra_p		number,
					nm_usuario_p			Varchar2) is 

qt_itens_w			number(5);
nr_cot_compra_w			number(10);
cd_estabelecimento_w		number(4);
nr_seq_licitacao_w			number(10);
nr_seq_reg_compra_item_w		number(10);
cd_comprador_w			varchar2(10);
qt_dias_retorno_w			number(5);
dt_retorno_prev_w			date;
nr_seq_lic_item_w			number(5);
cd_material_w			number(6);
cd_unid_medida_w			varchar2(30);
qt_item_w			number(15,4);
ds_historico_w			varchar2(4000);
qt_dias_prazo_entrega_w  reg_licitacao.qt_dias_prazo_entrega%TYPE;
dt_entrega_com_prazo_entrega_w  cot_compra.dt_entrega%TYPE;

cursor c01 is
select	a.nr_sequencia,
	a.nr_seq_lic_item,
	a.cd_material,
	b.cd_unid_medida,
	a.qt_item
from	reg_compra_item a,
	reg_lic_item b
where	a.nr_seq_licitacao	= b.nr_seq_licitacao
and	a.nr_seq_lic_item	= b.nr_seq_lic_item
and	a.nr_seq_reg_compra	= nr_seq_reg_compra_p
and a.dt_cancelamento is null;

begin

select	count(*)
into	qt_itens_w
from	reg_compra_item a,
	reg_lic_item b
where	a.nr_seq_licitacao		= b.nr_seq_licitacao
and	a.nr_seq_lic_item		= b.nr_seq_lic_item
and	a.nr_seq_reg_compra	= nr_seq_reg_compra_p;
if	(qt_itens_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(266027);
	--'E necessario existir itens no registro de preco para gerar uma pesquisa de preco.'
end if;

select	nr_seq_licitacao
into	nr_seq_licitacao_w
from	reg_compra
where	nr_sequencia	= nr_seq_reg_compra_p;

select	cd_estabelecimento,
		qt_dias_prazo_entrega
into	cd_estabelecimento_w,
		qt_dias_prazo_entrega_w
from	reg_licitacao
where	nr_sequencia	= nr_seq_licitacao_w;

if (qt_dias_prazo_entrega_w is not null) then
	dt_entrega_com_prazo_entrega_w := sysdate + qt_dias_prazo_entrega_w;
end if;

begin
select	cd_comprador_padrao
into	cd_comprador_w
from	parametro_compras
where	cd_estabelecimento	= cd_estabelecimento_w;
exception when others then
	wheb_mensagem_pck.exibir_mensagem_abort(266028);
	--'Nao encontrado o comprador padrao nos parametros de compras.'
end;

select	obter_valor_param_usuario(915, 2, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)
into	qt_dias_retorno_w
from	dual;
dt_retorno_prev_w	:= sysdate + qt_dias_retorno_w;
dt_retorno_prev_w	:= trunc(dt_retorno_prev_w, 'dd');

select	cot_compra_seq.nextval
into	nr_cot_compra_w
from	dual;

insert into cot_compra(
	nr_cot_compra,
	dt_cot_compra,
	dt_atualizacao,
	cd_comprador,
	nm_usuario,
	ds_observacao,
	cd_pessoa_solicitante,
	cd_estabelecimento,
	dt_retorno_prev,
	nr_seq_reg_compra,
	ie_finalidade_cotacao,
	dt_entrega)
values(	nr_cot_compra_w,
	sysdate,
	sysdate,
	cd_comprador_w,
	nm_usuario_p,
	null,
	cd_comprador_w,
	cd_estabelecimento_w,
	dt_retorno_prev_w,
	nr_seq_reg_compra_p,
	'P',
	dt_entrega_com_prazo_entrega_w);


open c01;
loop
fetch c01 into
	nr_seq_reg_compra_item_w,
	nr_seq_lic_item_w,
	cd_material_w,
	cd_unid_medida_w,
	qt_item_w;
exit when c01%notfound;
	begin

	insert into cot_compra_item(
		nr_cot_compra,
		nr_item_cot_compra,
		cd_material,
		qt_material,
		cd_unidade_medida_compra,
		dt_atualizacao,
		dt_limite_entrega,
		nm_usuario,
		ie_situacao,
		cd_estab_item,
		nr_seq_reg_compra,
		nr_seq_reg_compra_item)
	values(	nr_cot_compra_w,
		nr_seq_lic_item_w,
		cd_material_w,
		qt_item_w,
		cd_unid_medida_w,
		sysdate,
		dt_retorno_prev_w,
		nm_usuario_p,
		'A',
		cd_estabelecimento_w,
		nr_seq_reg_compra_p,
		nr_seq_reg_compra_item_w);

	gerar_cot_compra_item_entrega(
			nr_cot_compra_w,
			nr_seq_lic_item_w,
			nm_usuario_p);


	end;
end loop;
close c01;


ds_historico_w := 	substr(WHEB_MENSAGEM_PCK.get_texto(301900,'NR_COT_COMPRA_W='||nr_cot_compra_w),1,4000);
			
lic_gerar_historico_reg_preco(nr_seq_reg_compra_p, ds_historico_w, 'S', nm_usuario_p);


commit;

end gerar_pesquisa_reg_preco;
/