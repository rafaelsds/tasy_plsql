CREATE OR REPLACE PROCEDURE insert_imagem_captura (
            nm_usuario_p                VARCHAR2,
            nr_prescricao_p             NUMBER,
            nr_sequencia_prescricao_p   VARCHAR2,
            ds_caminho_p                VARCHAR2,
            nr_sequencia_p              NUMBER,
            nr_sequencia_out            OUT NUMBER) IS
    

ds_lista_w                VARCHAR2(2000);    
qt_existe_virgula_w       NUMBER(10);
tam_lista_w               NUMBER(10,0);
ie_pos_virgula_w		  NUMBER(3,0);
nr_sequencia_prescricao_w NUMBER(10);
nr_next_sequencia         NUMBER(10);

nr_seq_log_atual_w NUMBER(10);
ie_log_w           VARCHAR2(10);
    
BEGIN
  IF (nr_sequencia_p IS NULL) THEN  
    SELECT captura_imagem_seq.NEXTVAL
    INTO   nr_sequencia_out 
    FROM   dual;
  ELSE 
    nr_sequencia_out := nr_sequencia_p;
  END IF;
  
  gravar_log_alteracao(NULL, nr_sequencia_out, nm_usuario_p, nr_seq_log_atual_w, 'NR_SEQUENCIA', ie_log_w, NULL, 'CAPTURA_IMAGEM', nr_sequencia_out, NULL);
  gravar_log_alteracao(NULL, nm_usuario_p, nm_usuario_p, nr_seq_log_atual_w, 'NM_USUARIO', ie_log_w, NULL, 'CAPTURA_IMAGEM', nr_sequencia_out, NULL);
  gravar_log_alteracao(NULL, ds_caminho_p, nm_usuario_p, nr_seq_log_atual_w, 'DS_CAMINHO', ie_log_w, NULL, 'CAPTURA_IMAGEM', nr_sequencia_out, NULL);
  ie_log_w := NULL;   
 
  INSERT INTO captura_imagem(nr_sequencia, dt_atualizacao, nm_usuario, ds_caminho)
  VALUES (nr_sequencia_out, SYSDATE, nm_usuario_p, ds_caminho_p);
      
  ds_lista_w := nr_sequencia_prescricao_p;    
   
  SELECT INSTR(ds_lista_w, ',')
  INTO   qt_existe_virgula_w
  FROM   dual;  
    
  IF (qt_existe_virgula_w = 0) THEN
    ds_lista_w := ds_lista_w || ',';
  END IF;  

  IF (ds_lista_w IS NOT NULL) THEN
  BEGIN
    WHILE (ds_lista_w IS NOT NULL) LOOP
    BEGIN
      tam_lista_w := LENGTH(ds_lista_w);
      ie_pos_virgula_w := INSTR(ds_lista_w,',');
                  
      IF (ie_pos_virgula_w <> 0) THEN
        nr_sequencia_prescricao_w := to_NUMBER(SUBSTR(ds_lista_w, 1, (ie_pos_virgula_w - 1)));
        ds_lista_w := SUBSTR(ds_lista_w, (ie_pos_virgula_w + 1), tam_lista_w);
      END IF;
      
      select prescr_proc_captura_img_seq.NEXTVAL into nr_next_sequencia from dual;
      
      gravar_log_alteracao(NULL, nr_next_sequencia, nm_usuario_p, nr_seq_log_atual_w, 'NR_SEQUENCIA', ie_log_w, NULL, 'PRESCR_PROC_CAPTURA_IMG', nr_next_sequencia, NULL);
      gravar_log_alteracao(NULL, nr_prescricao_p, nm_usuario_p, nr_seq_log_atual_w, 'NR_PRESCRICAO', ie_log_w, NULL, 'PRESCR_PROC_CAPTURA_IMG', nr_next_sequencia, NULL);
      gravar_log_alteracao(NULL, nr_sequencia_prescricao_w, nm_usuario_p, nr_seq_log_atual_w, 'NR_SEQUENCIA_PRESCRICAO', ie_log_w, NULL, 'PRESCR_PROC_CAPTURA_IMG', nr_next_sequencia, null);
      gravar_log_alteracao(NULL, nr_sequencia_out, nm_usuario_p, nr_seq_log_atual_w, 'NR_SEQ_CAPTURA', ie_log_w, NULL, 'PRESCR_PROC_CAPTURA_IMG', nr_next_sequencia, NULL);
        
      INSERT INTO prescr_proc_captura_img (nr_sequencia, dt_atualizacao, nm_usuario, nr_prescricao, nr_sequencia_prescricao, nr_seq_captura)
      VALUES   (nr_next_sequencia, SYSDATE, nm_usuario_p, nr_prescricao_p, nr_sequencia_prescricao_w, nr_sequencia_out);
                    
    END;  
    END LOOP;        
  END; 
  END IF;

  COMMIT;
END insert_imagem_captura;
/
