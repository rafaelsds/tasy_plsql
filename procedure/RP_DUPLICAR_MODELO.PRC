create or replace
procedure rp_duplicar_modelo(
			ds_modelo_p	varchar2,
			nr_seq_modelo_p	number,	
			nm_usuario_p	Varchar2) is 
			
qt_vagas_w			rp_modelo_agendamento.qt_vagas%type;
ie_situacao_w			rp_modelo_agendamento.ie_situacao%type;
cd_estabelecimento_w		rp_modelo_agendamento.cd_estabelecimento%type;
ie_verifica_mod_individual_w	rp_modelo_agendamento.ie_verifica_mod_individual%type;
nr_seq_tipo_tratamento_w	rp_modelo_agendamento.nr_seq_tipo_tratamento%type;
nr_seq_equipe_w			rp_modelo_agendamento.nr_seq_equipe%type;
nr_seq_tipo_deficiencia_w	rp_modelo_agendamento.nr_seq_tipo_deficiencia%type;
nr_sequencia_modelo_w		rp_modelo_agendamento.nr_sequencia%type;
cd_setor_atendimento_w		rp_item_modelo_agenda.cd_setor_atendimento%type;
cd_agenda_w			rp_item_modelo_agenda.cd_agenda%type;
hr_horario_w			rp_item_modelo_agenda.hr_horario%type;
ie_dia_semana_w			rp_item_modelo_agenda.ie_dia_semana%type;
cd_profissional_padrao_w	rp_item_modelo_agenda.cd_profissional_padrao%type;
ie_classif_agenda_w		rp_item_modelo_agenda.ie_classif_agenda%type;
ie_situacao_item_w		rp_item_modelo_agenda.ie_situacao%type;

cursor C01 is
	select	cd_agenda,
		hr_horario,
		ie_dia_semana,
		cd_profissional_padrao,
		ie_classif_agenda,
		ie_situacao,
		cd_setor_atendimento
	from	rp_item_modelo_agenda
	where	nr_seq_modelo = nr_seq_modelo_p;

begin

select	qt_vagas,
	ie_situacao,
	cd_estabelecimento,
	ie_verifica_mod_individual,
	nr_seq_tipo_tratamento,
	nr_seq_equipe,
	nr_seq_tipo_deficiencia
into	qt_vagas_w,
	ie_situacao_w,
	cd_estabelecimento_w,
	ie_verifica_mod_individual_w,
	nr_seq_tipo_tratamento_w,
	nr_seq_equipe_w,
	nr_seq_tipo_deficiencia_w
from	rp_modelo_agendamento
where	nr_sequencia = nr_seq_modelo_p;

select	rp_modelo_agendamento_seq.nextval
into	nr_sequencia_modelo_w
from	dual;	

insert into	rp_modelo_agendamento(	nr_sequencia,
				        dt_atualizacao,
				        nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					qt_vagas,
					ds_modelo,
					ie_situacao,
					cd_estabelecimento,
					ie_verifica_mod_individual,
					nr_seq_tipo_tratamento,
					nr_seq_equipe,
					nr_seq_tipo_deficiencia
					)
				values(	nr_sequencia_modelo_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					qt_vagas_w,
					ds_modelo_p,
					ie_situacao_w,
					cd_estabelecimento_w,
					ie_verifica_mod_individual_w,
					nr_seq_tipo_tratamento_w,
					nr_seq_equipe_w,
					nr_seq_tipo_deficiencia_w);

open C01;
loop
fetch C01 into	
	cd_agenda_w,
	hr_horario_w,
	ie_dia_semana_w,
	cd_profissional_padrao_w,
	ie_classif_agenda_w,
	ie_situacao_item_w,
	cd_setor_atendimento_w;
exit when C01%notfound;
	begin
	insert into rp_item_modelo_agenda(    nr_sequencia,
					      dt_atualizacao,
					      nm_usuario,
					      dt_atualizacao_nrec,
					      nm_usuario_nrec,
					      cd_agenda,
					      hr_horario,
					      ie_dia_semana,
					      nr_seq_modelo,
					      cd_profissional_padrao,
					      ie_classif_agenda,
					      ie_situacao,
						  cd_setor_atendimento)
				values(       rp_item_modelo_agenda_seq.nextval,
					      sysdate,
					      nm_usuario_p,
					      sysdate,
					      nm_usuario_p,
					      cd_agenda_w,
					      hr_horario_w,
					      ie_dia_semana_w,
					      nr_sequencia_modelo_w,
					      cd_profissional_padrao_w,
					      ie_classif_agenda_w,
					      ie_situacao_item_w,
						  cd_setor_atendimento_w);	
	
	end;
end loop;
close C01;

commit;

end rp_duplicar_modelo;
/
