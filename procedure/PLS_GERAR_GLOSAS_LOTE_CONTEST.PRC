create or replace
procedure pls_gerar_glosas_lote_contest
			(	nr_seq_lote_contest_p		pls_lote_contestacao.nr_sequencia%type,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2,
				ie_commit_p			varchar2) is 

ds_observacao_w			pls_contest_item_glosa.ds_observacao%type;
ds_parecer_w			pls_contest_item_glosa.ds_parecer_glosa%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;
nr_seq_conta_proc_w		pls_conta_proc.nr_sequencia%type;
nr_seq_conta_mat_w		pls_conta_mat.nr_sequencia%type;
nr_seq_ocorrencia_w		pls_ocorrencia.nr_sequencia%type;
nr_seq_motivo_glosa_w		pls_acao_glosa_tiss.nr_seq_motivo_glosa%type;
nr_seq_mot_quest_w		ptu_motivo_questionamento.nr_sequencia%type;
nr_seq_contestacao_w		pls_contestacao.nr_sequencia%type;
nr_seq_contest_item_w		pls_contest_item_glosa.nr_seq_contest_proc%type;
cd_motivo_w			ptu_motivo_questionamento.cd_motivo%type;
ie_tipo_w			varchar2(1);
ie_parametro_31_w		varchar2(1);
nr_seq_item_w			number(10);
qt_glosa_w			pls_integer;
count_w				pls_integer;
	
Cursor C01 is
	select	nr_sequencia
	from	pls_contestacao
	where	nr_seq_lote	= nr_seq_lote_contest_p;

Cursor C02 is
	select	nr_seq_conta_proc,
		nr_sequencia,
		'P' ie_tipo
	from	pls_contestacao_proc
	where	nr_seq_contestacao	= nr_seq_contestacao_w
	union all
	select	nr_seq_conta_mat,
		nr_sequencia,
		'M' ie_tipo
	from	pls_contestacao_mat
	where	nr_seq_contestacao	= nr_seq_contestacao_w;
	
Cursor C03 is
	select	a.nr_seq_motivo_glosa,
		a.nr_ocorrencia,
		a.ds_observacao
	from	pls_conta_glosa_ocorrencia_v a
	where	a.nr_seq_proc	= nr_seq_item_w
	and	ie_situacao <> 'I'
	union all
	select	a.nr_seq_motivo_glosa,
		a.nr_ocorrencia,
		a.ds_observacao
	from	pls_conta_glosa_ocorrencia_v a
	where	a.nr_seq_conta	= nr_seq_conta_w
	and	ie_situacao <> 'I'
	and	nr_seq_proc is null
	and	nr_seq_mat is null;

Cursor C04 is
	select	a.nr_seq_motivo_glosa,
		a.nr_ocorrencia,
		a.ds_observacao
	from	pls_conta_glosa_ocorrencia_v a
	where	a.nr_seq_mat	= nr_seq_item_w
	and	ie_situacao <> 'I'
	union all
	select	a.nr_seq_motivo_glosa,
		a.nr_ocorrencia,
		a.ds_observacao
	from	pls_conta_glosa_ocorrencia_v a
	where	a.nr_seq_conta	= nr_seq_conta_w
	and	ie_situacao <> 'I'
	and	nr_seq_proc is null
	and	nr_seq_mat is null;

begin
-- par�metro [31] - Gerar apenas a glosa principal na contesta��o 
obter_param_usuario(1293,31, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_parametro_31_w);

open C01;
loop
fetch C01 into	
	nr_seq_contestacao_w;
exit when C01%notfound;
	begin
	select	max(a.nr_seq_conta)
	into	nr_seq_conta_w
	from	pls_contestacao	a
	where	nr_sequencia	= nr_seq_contestacao_w;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_item_w,
		nr_seq_contest_item_w,
		ie_tipo_w;
	exit when C02%notfound;
		begin		
		if	(ie_tipo_w = 'P') then
			-- Gerar glosas
			open C03;
			loop
			fetch C03 into	
				nr_seq_motivo_glosa_w,
				nr_seq_ocorrencia_w,
				ds_observacao_w;
			exit when C03%notfound;
				begin
				ds_parecer_w	:= null;
				
				select	max(a.nr_seq_motivo_ptu)
				into	nr_seq_mot_quest_w
				from	pls_acao_glosa_tiss	a
				where	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w
				and	a.ie_situacao		= 'A';
				
				if	(nr_seq_mot_quest_w is null) then
					select	max(a.nr_seq_motivo_ptu)
					into	nr_seq_mot_quest_w
					from	pls_ocorrencia	a
					where	a.nr_sequencia	= nr_seq_ocorrencia_w
					and	a.ie_situacao	= 'A';
				end if;
				
				-- Pegar o parecer da glosa principal
				if	(nr_seq_mot_quest_w is not null) then
					select	max(a.ds_parecer)
					into	ds_parecer_w
					from	pls_analise_fluxo_item	a
					where	a.nr_seq_conta_proc	= nr_seq_item_w
					and	a.ie_pagamento		= 'G';
					
					if	(ds_parecer_w is null) then
						select	count(1)
						into	count_w
						from	pls_analise_fluxo_item	a
						where	a.nr_seq_conta_proc	= nr_seq_item_w
						and	rownum <= 1;
					
						if	(count_w = 0) then
							select	max(a.ds_parecer)
							into	ds_parecer_w
							from	pls_analise_fluxo_item	a
							where	a.nr_seq_conta	= nr_seq_conta_w
							and	a.ie_pagamento	= 'G';
						end if;
					end if;
				end if;
				
				-- Pegar o parecer da glosa parcial caso n�o tenha parecer anterior
				if	(ds_parecer_w is null) and
					(ie_parametro_31_w = 'N') and
					(nr_seq_mot_quest_w is not null) then
					select	max(cd_motivo)
					into	cd_motivo_w
					from	ptu_motivo_questionamento
					where	nr_sequencia = nr_seq_mot_quest_w;
					
					if	(cd_motivo_w = '98') then
						select	max(a.ds_parecer)
						into	ds_parecer_w
						from	pls_analise_fluxo_item	a
						where	a.nr_seq_conta_proc	= nr_seq_item_w
						and	a.ie_pagamento		= 'P';
						
						if	(ds_parecer_w is null) then
							select	count(1)
							into	count_w
							from	pls_analise_fluxo_item	a
							where	a.nr_seq_conta_proc	= nr_seq_item_w
							and	rownum <= 1;
						
							if	(count_w = 0) then
								select	max(a.ds_parecer)
								into	ds_parecer_w
								from	pls_analise_fluxo_item	a
								where	a.nr_seq_conta	= nr_seq_conta_w
								and	a.ie_pagamento	= 'P';
							end if;
						end if;
					end if;
				end if;
				
				if	((ie_parametro_31_w = 'S') and
					(ds_parecer_w is not null)) or
					(ie_parametro_31_w = 'N') then
					select	count(1)
					into	qt_glosa_w
					from	pls_contest_item_glosa
					where	nr_seq_contest_proc	= nr_seq_contest_item_w
					and	nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w;
				
					if	(qt_glosa_w = 0) then
						insert into pls_contest_item_glosa
							(nr_sequencia,
							nm_usuario,
							dt_atualizacao,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							nr_seq_contest_proc,
							nr_seq_motivo_glosa,
							nr_seq_ocorrencia,
							nr_seq_mot_quest,
							ds_observacao,
							ds_parecer_glosa)
						values	(pls_contest_item_glosa_seq.nextval,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							nr_seq_contest_item_w,
							nr_seq_motivo_glosa_w,
							nr_seq_ocorrencia_w,
							nr_seq_mot_quest_w,
							ds_observacao_w,
							ds_parecer_w);
					end if;
				end if;
				end;
			end loop;
			close C03;
		else	
			-- Gerar glosas
			open C04;
			loop
			fetch C04 into	
				nr_seq_motivo_glosa_w,
				nr_seq_ocorrencia_w,
				ds_observacao_w;
			exit when C04%notfound;
				begin
				ds_parecer_w	:= null;
				
				select	max(a.nr_seq_motivo_ptu)
				into	nr_seq_mot_quest_w
				from	pls_acao_glosa_tiss	a
				where	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w
				and	a.ie_situacao		= 'A';
			
				if	(nr_seq_mot_quest_w is null) then
					select	max(a.nr_seq_motivo_ptu)
					into	nr_seq_mot_quest_w
					from	pls_ocorrencia	a
					where	a.nr_sequencia	= nr_seq_ocorrencia_w
					and	a.ie_situacao	= 'A';
				end if;
				
				-- Pegar o parecer da glosa principal 
				if	(nr_seq_mot_quest_w is not null) then
					select	max(a.ds_parecer)
					into	ds_parecer_w
					from	pls_analise_fluxo_item	a
					where	a.nr_seq_conta_mat	= nr_seq_item_w
					and	a.ie_pagamento		= 'G';
				end if;
				
				-- Pegar o parecer da glosa parcial caso n�o tenha parecer anterior
				if	(ds_parecer_w is null) and
					(ie_parametro_31_w = 'N') and
					(nr_seq_mot_quest_w is not null) then
					select	max(cd_motivo)
					into	cd_motivo_w
					from	ptu_motivo_questionamento
					where	nr_sequencia = nr_seq_mot_quest_w;
					
					if	(cd_motivo_w = '98') then
						select	max(a.ds_parecer)
						into	ds_parecer_w
						from	pls_analise_fluxo_item	a
						where	a.nr_seq_conta_mat	= nr_seq_item_w
						and	a.ie_pagamento		= 'P';
					end if;
				end if;
				
				if	((ie_parametro_31_w = 'S') and
					(ds_parecer_w is not null)) or
					(ie_parametro_31_w = 'N') then
					select	count(1)
					into	qt_glosa_w
					from	pls_contest_item_glosa	a
					where	a.nr_seq_contest_mat	= nr_seq_contest_item_w
					and	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w;
					
					if	(qt_glosa_w = 0) then
						insert into pls_contest_item_glosa
							(nr_sequencia,
							nm_usuario,
							dt_atualizacao,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							nr_seq_contest_mat,
							nr_seq_motivo_glosa,
							nr_seq_ocorrencia,
							nr_seq_mot_quest,
							ds_observacao,
							ds_parecer_glosa)
						values	(pls_contest_item_glosa_seq.nextval,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							nr_seq_contest_item_w,
							nr_seq_motivo_glosa_w,
							nr_seq_ocorrencia_w,
							nr_seq_mot_quest_w,
							ds_observacao_w,
							ds_parecer_w);
					end if;
				end if;
				end;
			end loop;
			close C04;
		end if;
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;
	
pls_atualizar_valores_contest(	nr_seq_lote_contest_p, 'N');

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end pls_gerar_glosas_lote_contest;
/