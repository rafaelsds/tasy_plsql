create or replace
procedure ADICIONAR_REGISTRO_TEMPLATE (
				nr_seq_registro_p		number,
				ds_lista_templates_p	varchar2,
				nm_usuario_p		varchar2) is
			
ds_lista_templates_w	varchar2(2000);
ds_lista_prontuario_w	varchar2(2000);
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);
nr_seq_template_w	number(10,0);
nr_seq_item_pep_w	number(10,0);
nr_seq_reg_template_w	number(10,0);
nr_seq_reg_item_w	number(10,0);
nr_seq_apres_w		number(5);
			
begin
if	(nr_seq_registro_p is not null) and
	(ds_lista_templates_p is not null) and
	(nm_usuario_p is not null) then
	
	ds_lista_templates_w	:= ds_lista_templates_p;
		
	while	ds_lista_templates_w is not null loop
		begin
		tam_lista_w	:= length(ds_lista_templates_w);
		ie_pos_virgula_w	:= instr(ds_lista_templates_w, ',');

		if	(ie_pos_virgula_w <> 0) then
			nr_seq_template_w	:= to_number(substr(ds_lista_templates_w, 1, (ie_pos_virgula_w - 1)));
			ds_lista_templates_w	:= substr(ds_lista_templates_w, (ie_pos_virgula_w + 1), tam_lista_w);
		else	
			nr_seq_template_w	:= ds_lista_templates_w;
			ds_lista_templates_w	:= null;
		end if;
		
		if	(nr_seq_template_w > 0) then
			
		
			select	ehr_reg_template_seq.nextval
			into	nr_seq_reg_template_w
			from	dual;

			insert into ehr_reg_template (
				nr_sequencia,
				nr_seq_reg,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_registro,
				nr_seq_template,
				nr_seq_item_pep,
				dt_liberacao,
				nr_seq_apres)
			values (
				nr_seq_reg_template_w,
				nr_seq_registro_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_template_w,
				null,
				null,
				nr_seq_apres_w);
				
		end if;
		end;
	end loop;		
	
end if;

commit;

end adicionar_registro_template;
/
