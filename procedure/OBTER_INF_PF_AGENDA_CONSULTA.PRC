create or replace
procedure obter_inf_pf_agenda_consulta(
		cd_agenda_p			number,
		dt_referencia_p			date,
		cd_pessoa_fisica_p		varchar2,
		qt_idade_p		out	number,
		dt_nascimento_p		out	date,
		ds_msg_p		out	varchar2,
		cd_medico_solic_p	out	varchar2) is 

ds_perm_pf_classif_p	varchar2(80);
cd_medico_solic_w	varchar2(255);
begin

select	max(substr(obter_se_perm_pf_classif(866, cd_agenda_p, cd_pessoa_fisica_p, dt_referencia_p, 'DS'), 1,80))
into	ds_perm_pf_classif_p
from	dual;

if	(ds_perm_pf_classif_p <> '') then
	begin
	ds_msg_p	:= substr(obter_texto_tasy(47964, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	ds_msg_p	:= replace(ds_msg_p, '#@DS_PERM_AGENDAR_CLASSIF#@', ds_perm_pf_classif_p);
	end;
else
	begin
	select	max(obter_idade(dt_nascimento, sysdate, 'A')),
		max(dt_nascimento)
	into	qt_idade_p,
		dt_nascimento_p
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	end;
end if;

if	(cd_pessoa_fisica_p is not null) then
	begin
	
	cd_medico_solic_w	:= substr(agserv_obter_med_solic(cd_pessoa_fisica_p,dt_referencia_p),1,255);
	
	end;
end if;

cd_medico_solic_p	:= cd_medico_solic_w;

end obter_inf_pf_agenda_consulta;
/
