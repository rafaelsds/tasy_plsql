create or replace
procedure ATUALIZAR_COBRANCA_ETAPA
	(nr_seq_cobranca_p	in	number,
	 nm_usuario_p		in	varchar2) is

nr_seq_etapa_w	number(10,0);

begin

begin
select	nr_seq_etapa
into	nr_seq_etapa_w
from	(select	a.nr_seq_etapa
	from	cobranca_etapa a
	where	a.nr_seq_cobranca	= nr_seq_cobranca_p
	order	by a.dt_etapa desc)
where	rownum	< 2;
exception
when others then
	nr_seq_etapa_w	:= null;
end;

update	cobranca
set	nr_seq_etapa	= nr_seq_etapa_w,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_cobranca_p;

commit;

end ATUALIZAR_COBRANCA_ETAPA;
/
