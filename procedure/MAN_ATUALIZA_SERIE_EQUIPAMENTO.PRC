create or replace
procedure man_atualiza_serie_equipamento(
				nr_seq_equipamento_p	number,
				nr_serie_p		varchar2,
				nm_usuario_p		Varchar2) is 


nr_seq_nf_integ_w		number(10,0);
nr_seq_item_nf_integ_w	number(10,0);
ds_historico_w		varchar2(4000);
nr_seq_tipo_w		number(10,0);
nr_serie_ant_w		varchar2(30);

begin

if	(nr_seq_equipamento_p > 0) then
	begin
	select	nr_serie
	into	nr_serie_ant_w
	from	man_equipamento
	where	nr_sequencia = nr_seq_equipamento_p;
	
	update	man_equipamento
	set	nr_serie	 	= nr_serie_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia 	= nr_seq_equipamento_p;

	select	nr_seq_nf_integ,
		nr_seq_item_nf_integ
	into	nr_seq_nf_integ_w,
		nr_seq_item_nf_integ_w
	from	man_equipamento
	where	nr_sequencia = nr_seq_equipamento_p;
	
	select	man_obter_tipo_hist_equip_evt('S')
	into	nr_seq_tipo_w
	from	dual;
	/*'Altera��o da s�rie do equipamento de: ' || nr_serie_ant_w || ' para: ' || nr_serie_p || chr(13) || chr(10) ||
					'Data: ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')*/
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(305941,'NR_SERIE_ANT_W='||nr_serie_ant_w||';NR_SERIE_P='||nr_serie_p||';DT_DATA_W='||PKG_DATE_FORMATERS.to_varchar(sysdate, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p)),1,4000);
	
	insert into man_equipamento_hist (
			nr_sequencia,
			nr_seq_equipamento,
			dt_historico,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_historico,
			ie_origem,
			nr_seq_tipo)
		values(	man_equipamento_hist_seq.nextval,
			nr_seq_equipamento_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ds_historico_w,
			'S',
			nr_seq_tipo_w);
	
	if	(nr_seq_nf_integ_w > 0) and
		(nr_seq_item_nf_integ_w > 0) then
		update	nota_fiscal_item
		set	ds_observacao 	= substr(ds_observacao || wheb_mensagem_pck.get_texto(305947,'NR_SERIE_P='||nr_serie_p),1,255)   --' S�rie alterada para: ' || nr_serie_p
		where	nr_sequencia 	= nr_seq_nf_integ_w
		and	nr_item_nf 	= nr_seq_item_nf_integ_w;
	end if;	

	commit;
	end;
end if;

end man_atualiza_serie_equipamento;
/