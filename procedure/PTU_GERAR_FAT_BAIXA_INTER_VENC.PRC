create or replace
procedure ptu_gerar_fat_baixa_inter_venc(nm_usuario_p		varchar2) is

dt_atual_w			date;
dt_referencia_w			date := to_date('31/01/2017');
qt_dias_venc_a510_w		pls_parametros.qt_dias_venc_a510%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type := pls_obter_cd_estab_ops;

cursor c01 is
	select	t.nr_titulo,
		p.nr_sequencia nr_seq_ptu_fatura,
		t.cd_estabelecimento,
		f.nm_usuario,
		t.dt_pagamento_previsto
	from	titulo_receber		t,
		pls_fatura		f,
		ptu_fatura		p
	where	t.nr_titulo		= f.nr_titulo
	and	f.nr_sequencia		= p.nr_seq_pls_fatura
	and	t.ie_origem_titulo	= '13' -- OPS - Faturamento
	and	t.ie_situacao		= '1' -- Aberto
	and	t.dt_pagamento_previsto < dt_atual_w
	and	t.dt_pagamento_previsto	> dt_referencia_w
	and	t.dt_liquidacao is null
	and	not exists	(select	1
				from	ptu_fat_baixa_interc i
				where	i.nr_seq_ptu_fatura = p.nr_sequencia)
	and	not exists	(select	1
				from	titulo_receber_liq q
				where	q.nr_titulo	= t.nr_titulo
				and	q.vl_recebido	> 0)
	union
	select	t.nr_titulo,
		p.nr_sequencia nr_seq_ptu_fatura,
		t.cd_estabelecimento,
		f.nm_usuario,
		t.dt_pagamento_previsto
	from	titulo_receber		t,
		pls_fatura		f,
		ptu_fatura		p
	where	t.nr_titulo		= f.nr_titulo_ndc
	and	f.nr_sequencia		= p.nr_seq_pls_fatura
	and	t.ie_origem_titulo	= '13' -- OPS - Faturamento
	and	t.ie_situacao		= '1' -- Aberto
	and	t.dt_pagamento_previsto < dt_atual_w
	and	t.dt_pagamento_previsto	> dt_referencia_w
	and	t.dt_liquidacao is null
	and	not exists	(select	1
				from	ptu_fat_baixa_interc i
				where	i.nr_seq_ptu_fatura = p.nr_sequencia)
	and	not exists	(select	1
				from	titulo_receber_liq q
				where	q.nr_titulo	= t.nr_titulo
				and	q.vl_recebido	> 0);

begin
select	max(qt_dias_venc_a510)
into	qt_dias_venc_a510_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_w;

-- Caso seja menor que zero ou nulo, o par�metro ser� zero
if	(nvl(qt_dias_venc_a510_w,0) <= 0) then
	qt_dias_venc_a510_w := 0;
end if;

-- Montar a data conforme par�metro
dt_atual_w := trunc(sysdate) - qt_dias_venc_a510_w;

for r_c01_w in c01 loop	
	if	(r_c01_w.nr_seq_ptu_fatura is not null) then
		-- Gerar A510
		ptu_gerar_fat_baixa_interc( r_c01_w.nr_seq_ptu_fatura, null, r_c01_w.cd_estabelecimento, nm_usuario_p, 'N', null, null, 0, r_c01_w.dt_pagamento_previsto);
	end if;
end loop;

commit;
end ptu_gerar_fat_baixa_inter_venc;
/
