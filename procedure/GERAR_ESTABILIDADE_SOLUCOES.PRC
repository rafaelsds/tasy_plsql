create or replace
procedure gerar_estabilidade_solucoes	(nr_prescricao_p	number) is

nr_prescr_w		number(14,0);
nr_seq_sol_w		number(6,0);
qt_hor_estab_w	number(15,3);

cursor c01 is
select	b.nr_prescricao,
	b.nr_seq_solucao
from	prescr_solucao b,
	prescr_medica a
where	a.nr_prescricao = nr_prescricao_p
and	a.nr_prescricao	= b.nr_prescricao
order by
	nr_seq_solucao;

begin
if	(nr_prescricao_p is not null) then

	open c01;
	loop
	fetch c01 into	nr_prescr_w,
			nr_seq_sol_w;
	exit when c01%notfound;
		begin
		
		select	min(qt_horas_estabilidade)
		into	qt_hor_estab_w
		from	prescr_material
		where	ie_agrupador		= 4
		and	nr_prescricao 	= nr_prescr_w
		and	nr_sequencia_solucao	= nr_seq_sol_w;
		
		update	prescr_solucao
		set	qt_horas_estabilidade 	= qt_hor_estab_w
		where	nr_prescricao			= nr_prescr_w
		and	nr_seq_solucao		= nr_seq_sol_w;
		
		end;
	end loop;
	close c01;

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_estabilidade_solucoes;
/
