create or replace procedure liberar_pepo_html (	nr_cirurgia_p   		in 	number,
					nr_seq_pepo_p   		in 	number,
					cd_local_estoque_p  	in 	number,
					ie_opcao_p   			varchar2) is

ie_parametro_158_w  	varchar2(1);
ie_gerar_lanc_autom_w 	varchar2(1);
cd_perfil_w    		number(5);
nm_usuario_w   		varchar2(15);
cd_estabelecimento_w 	number(5);
nr_cirurgia_w   		cirurgia.NR_CIRURGIA%TYPE;
nr_prescricao_w   		cirurgia.NR_PRESCRICAO%TYPE;

cursor c01 is
	select   	nr_cirurgia,
		nr_prescricao
	from     	cirurgia
	where    	nr_seq_pepo = nr_seq_pepo_p;

begin

cd_perfil_w    		:= wheb_usuario_pck.get_cd_perfil;
nm_usuario_w   		:= wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w 	:= wheb_usuario_pck.get_cd_estabelecimento;

obter_param_usuario(872, 323, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_gerar_lanc_autom_w);
obter_param_usuario(872, 158, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, ie_parametro_158_w);

if (nvl(nr_cirurgia_p,0) = 0) and (nvl(nr_seq_pepo_p,0) > 0) then
	open C01;
	loop
	fetch C01 into
		nr_cirurgia_w,
		nr_prescricao_w;
	exit when C01%notfound;
	begin
		liberar_pepo(nr_cirurgia_w, ie_opcao_p, nm_usuario_w, nr_seq_pepo_p);
		if (ie_gerar_lanc_autom_w = 'S') and (ie_opcao_p = 'L') then
			liberar_pepob_js(nr_cirurgia_w, nr_prescricao_w, cd_local_estoque_p, nm_usuario_w);
		end if;
	end;
	end loop;
	close c01;
elsif (nvl(nr_cirurgia_p,0) > 0) then
	select 	max(nr_prescricao)
	into 	nr_prescricao_w
	from 	cirurgia
	where 	nr_cirurgia = nr_cirurgia_p;

	liberar_pepo(nr_cirurgia_p, ie_opcao_p, nm_usuario_w);
	if (ie_gerar_lanc_autom_w = 'S') and (ie_opcao_p = 'L') then
		liberar_pepob_js(nr_cirurgia_p, nr_prescricao_w, cd_local_estoque_p, nm_usuario_w);
	end if;
end if;

end liberar_pepo_html;
/