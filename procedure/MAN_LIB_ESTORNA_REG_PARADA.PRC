create or replace
procedure	man_lib_estorna_reg_parada(
			nr_sequencia_p		number,
			ie_opcao_p		varchar2,
			nm_usuario_p		varchar2) is

begin

if	(ie_opcao_p = 'L') then
	begin

	update	man_ordem_servico_parada
	set	dt_liberacao = sysdate,
		nm_usuario_lib = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

	end;
elsif	(ie_opcao_p = 'E') then
	begin

	update	man_ordem_servico_parada
	set	dt_liberacao = null,
		nm_usuario_lib = null
	where	nr_sequencia = nr_sequencia_p;

	end;
end if;

commit;

end man_lib_estorna_reg_parada;
/