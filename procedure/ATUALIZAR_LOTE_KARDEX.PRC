create or replace
procedure atualizar_lote_kardex(	nr_sequencia_p	number) is 

begin

if	(nr_sequencia_p is not null)then
	begin
	
	update 	ap_lote 
	set 	dt_envio_integracao = sysdate 
	where 	nr_sequencia = nr_sequencia_p;
	
	end;
end if;

commit;

end atualizar_lote_kardex;
/