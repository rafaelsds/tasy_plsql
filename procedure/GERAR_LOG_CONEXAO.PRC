CREATE OR REPLACE 
PROCEDURE Gerar_Log_Conexao  IS

ds_hash_w			Varchar2(32);
qt_total_w			Number(15,0);
qt_tasy_w			Number(15,0);
qt_tasy_ops_w			Number(15,0);
qt_prestador_w			Number(15,0);
qt_tasymed_w		Number(15,0);
qt_tasyOut_w		Number(15,0);
qt_estacao_w		Number(15,0);
qt_ios_w		Number(15,0);
dt_log_w			date;
nr_sequencia_w		number(10);
ds_modulo_logoff_w	varchar2(255)  := 'CORSIS_F0';

BEGIN

dt_log_w	:= sysdate;

/* Usu�rio conectados no TASY*/
SELECT	COUNT(*)
INTO	qt_tasy_w
FROM	gv$session
WHERE	UPPER(program) LIKE 'TASY%'
AND 	UPPER(program) NOT LIKE '%TASYMON%'
AND 	UPPER(program) NOT LIKE 'TASY JAVA'
AND 	UPPER(program) NOT LIKE 'TASY SERVER'
AND 	UPPER(program) NOT LIKE '%TASYMED%'
AND 	UPPER(program) NOT LIKE '%TASYREL%'
AND 	UPPER(program) NOT LIKE '%TASYGER%'
AND 	UPPER(program) NOT LIKE '%TASYSCS%'
AND	upper(substr(module,7,9)) <> ds_modulo_logoff_w;

-- Usu�rio conectados no TASY
SELECT	COUNT(*)
INTO	qt_tasy_ops_w
FROM	funcao b, gv$session a
WHERE	UPPER(a.program) LIKE '%TASY.EXE%'
and	upper(substr(a.module,7,9)) = upper(b.ds_form)
and	b.ds_aplicacao = 'TasyPLS'
and	upper(substr(a.module,7,9)) <> ds_modulo_logoff_w
and	b.cd_funcao <> 1203;

SELECT	COUNT(*)
INTO	qt_prestador_w
FROM	funcao b, gv$session a
WHERE	UPPER(a.program) LIKE '%TASY.EXE%'
and	upper(substr(a.module,7,9)) = upper(b.ds_form)
and	b.ds_aplicacao = 'TasyPLS'
and	upper(substr(a.module,7,9)) <> ds_modulo_logoff_w
and	b.cd_funcao = 1203;

-- Usu�rio conectados no TASYMED
SELECT	COUNT(*)
INTO	qt_tasymed_w
FROM	gv$session
WHERE	UPPER(program) LIKE '%TASYMED%'
and	upper(substr(module,7,9)) <> ds_modulo_logoff_w;

-- Usu�rio conectados em outros
SELECT	COUNT(*)
INTO	qt_tasyOut_w
FROM	gv$session
WHERE	UPPER(program) LIKE '%TASYMON%'
OR 	UPPER(program) LIKE '%TASYREL%'
OR	UPPER(program) LIKE '%TASYCIH%'
OR 	UPPER(program) LIKE '%TASYGER%'
and	upper(substr(module,7,9)) <> ds_modulo_logoff_w;

-- N�mero de esta��es conectadas
SELECT	COUNT( DISTINCT machine || osuser)
into	qt_estacao_w
FROM	gv$session
WHERE	UPPER(program) LIKE '%TASY%'
AND 	UPPER(program) NOT LIKE 'TASY JAVA'
AND 	UPPER(program) NOT LIKE 'TASY SERVER'
AND	UPPER(program) NOT LIKE '%TASYGER%'
AND	UPPER(program) NOT LIKE '%TASYREL%'
AND	UPPER(program) NOT LIKE '%TASYMON%'
AND	UPPER(program) NOT LIKE '%TASYCIH%'
AND	upper(substr(module,7,9)) <> ds_modulo_logoff_w;	

-- N�mero de dispositivos iOS conectados
SELECT	COUNT(*)
into	qt_ios_w
FROM	gv$session
WHERE	upper(program) = 'TASY SERVER'
AND		substr(client_info,1,8) = 'Tipo=iOS'; 

qt_total_w 	:= qt_tasy_w + qt_tasymed_w + qt_tasyOut_w + qt_tasy_ops_w + qt_prestador_w;
ds_hash_w	:= RAWTOHEX(dbms_obfuscation_toolkit.md5(input => utl_raw.cast_to_raw(qt_tasy_w || qt_tasymed_w || 
qt_tasyOut_w || qt_tasy_ops_w || qt_prestador_w || qt_estacao_w || to_char(dt_log_w,'dd/mm/yyy hh24:mi:ss'))));

select	log_usuario_conectado_seq.nextval
into		nr_sequencia_w
from		dual;

insert into log_usuario_conectado(
	NR_SEQUENCIA,
	DT_LOG,
	QT_TOTAL,
	QT_TASY,
	QT_TASYMED,
	QT_OUTRO,
	QT_ESTACAO,
	DS_HASH,
	QT_OPS,
	QT_PRESTADOR,
	QT_IOS)
values (
	nr_sequencia_w,
	dt_log_w,
	qt_total_w,
	qt_tasy_w,
	qt_tasymed_w,
	qt_tasyout_w,
	qt_estacao_w,
	ds_hash_w,
	qt_tasy_ops_w,
	qt_prestador_w,
	qt_ios_w);

commit;

END Gerar_Log_Conexao;
/