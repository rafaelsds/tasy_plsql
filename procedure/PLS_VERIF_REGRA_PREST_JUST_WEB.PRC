create or replace
procedure pls_verif_regra_prest_just_web( 	nr_seq_guia_p		number,
						nr_seq_requisicao_p	Number,
						nm_usuario_p		Varchar2) is 
					  
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Permitir algum usu�rio web que n�o seja o solicitante da requisi��o justificar a requisi��o, desde que o mesmo possua o prestador solicitante em seu login.
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Deve ser chamada ap�s a requisi��o ter sido consistida.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
					  
nr_seq_prestador_web_w 		Number(10);
nr_seq_auditoria_w		Number(10);
qt_registros_w			Number(10);

begin

if (nr_seq_guia_p is not null) then
	begin
		select	nr_sequencia
		into	nr_seq_auditoria_w
		from	pls_auditoria
		where	nr_seq_guia	= nr_seq_guia_p;
	exception
	when others then
		nr_seq_auditoria_w := null;
	end;	
	if (nr_seq_auditoria_w is not null) then
		begin
			select	nr_seq_prestador_web
			into	nr_seq_prestador_web_w
			from	pls_guia_plano
			where	nr_sequencia = nr_seq_guia_p
			and	ie_estagio = '1';
		exception
		when others then
			nr_seq_prestador_web_w := null;
		end;
		if	(nr_seq_prestador_web_w is not null) then
			if	(pls_obter_se_controle_estab('RE') = 'S') then
				select	count(1)
				into	qt_registros_w
				from	pls_regra_prest_justif
				where	nr_seq_prestador_web = nr_seq_prestador_web_w
				and    (trunc(sysdate)  between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, sysdate)))
				and	ie_situacao = 'A'
				and 	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;
			else
				select	count(1)
				into	qt_registros_w
				from	pls_regra_prest_justif
				where	nr_seq_prestador_web = nr_seq_prestador_web_w
				and    (trunc(sysdate)  between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, sysdate)))
				and	ie_situacao = 'A';
			end if;
		
			if	(qt_registros_w > 0) then		
				update	pls_auditoria
				set	ie_permite_just_prestadores = 'S'
				where	nr_sequencia 	= nr_seq_auditoria_w;
				commit;
			end if;		
		end if;
	end if;		
elsif (nr_seq_requisicao_p is not null) then
	begin
		select	nr_sequencia
		into	nr_seq_auditoria_w
		from	pls_auditoria
		where	nr_seq_requisicao	= nr_seq_requisicao_p;
	exception
	when others then
		nr_seq_auditoria_w := null;
	end;
	if	(nr_seq_auditoria_w is not null) then
		begin
			select	nr_seq_prestador_web
			into	nr_seq_prestador_web_w
			from	pls_requisicao
			where	nr_sequencia = nr_seq_requisicao_p
			and	ie_estagio = '4';
		exception
		when others then
			nr_seq_prestador_web_w := null;
		end;
		if	(nr_seq_prestador_web_w is not null) then
			if	(pls_obter_se_controle_estab('RE') = 'S') then
				select	count(1)
				into	qt_registros_w
				from	pls_regra_prest_justif
				where	nr_seq_prestador_web = nr_seq_prestador_web_w
				and    (trunc(sysdate)  between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, sysdate)))
				and	ie_situacao = 'A'
				and 	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;
			else
				select	count(1)
				into	qt_registros_w
				from	pls_regra_prest_justif
				where	nr_seq_prestador_web = nr_seq_prestador_web_w
				and    (trunc(sysdate)  between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia, sysdate)))
				and	ie_situacao = 'A';
			end if;
			if	(qt_registros_w > 0) then			
				update	pls_auditoria
				set	ie_permite_just_prestadores = 'S'
				where	nr_sequencia = nr_seq_auditoria_w;
				commit;	
			end if;			
		end if;
	end if;
end if;

end pls_verif_regra_prest_just_web;
/
