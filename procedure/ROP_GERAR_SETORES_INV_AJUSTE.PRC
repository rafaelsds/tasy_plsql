create or replace
procedure rop_gerar_setores_inv_ajuste(	nr_sequencia_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

cd_setor_atendimento_w	number(5);
cd_pessoa_resp_w		varchar2(10);

cursor c01 is
select distinct cd_setor_atendimento
from (
	select	a.cd_setor_atendimento
	from	rop_regra_roupa_setor a
	where	a.cd_estabelecimento = cd_estabelecimento_p
	and not exists(
		select	1
		from	rop_inv_ajuste_setor x
		where	x.nr_seq_inventario = nr_sequencia_p
		and	a.cd_setor_atendimento = x.cd_setor_atendimento)
	union
	select	b.cd_setor_atendimento
	from	rop_regra_roupa_setor a,
		rop_regra_roupa_setores b
	where	a.nr_sequencia = b.nr_seq_regra
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and not exists(
		select	1
		from	rop_inv_ajuste_setor x
		where	x.nr_seq_inventario = nr_sequencia_p
		and	a.cd_setor_atendimento = x.cd_setor_atendimento));

begin

open C01;
loop
fetch C01 into	
	cd_setor_atendimento_w;
exit when C01%notfound;
	begin
	select	cd_pessoa_resp
	into	cd_pessoa_resp_w
	from	setor_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_w;
	
	insert into rop_inv_ajuste_setor(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_inventario,
		cd_setor_atendimento,
		cd_resp_contagem)
	values(	rop_inv_ajuste_setor_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_p,
		cd_setor_atendimento_w,
		cd_pessoa_resp_w);	
	end;
end loop;
close C01;

commit;

end rop_gerar_setores_inv_ajuste;
/
