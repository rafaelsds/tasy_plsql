create or replace
procedure pls_alterar_tipo_cobranca
		(nr_seq_fatura_p	ptu_fatura_geral.nr_sequencia%type,
		 id_cobranca_p		ptu_fatura_geral.id_cobranca%type,
		 nm_usuario_p		usuario.nm_usuario%type) is

begin

update	ptu_fatura_geral
set	id_cobranca		= id_cobranca_p,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_fatura_p;

commit;

end pls_alterar_tipo_cobranca;
/
