create or replace
procedure liberar_avaliacao_prescr_js(	nr_sequencia_p		number,
					nr_seq_tipo_avaliacao_p	number,
					nm_usuario_p		varchar2,
					ds_mensagem_p		out varchar2) is 

ds_mensagem_w	varchar2(255);
				
begin

if	(nvl(nr_sequencia_p,0) > 0)then
	begin
	
	atualizar_dt_lib_med_aval(nr_sequencia_p, nm_usuario_p);
	
	gerar_mensagem_avaliacao(nr_sequencia_p, nr_seq_tipo_avaliacao_p, ds_mensagem_w);
	
	ds_mensagem_p	:= ds_mensagem_w;
	
	end;
end if;

commit;

end liberar_avaliacao_prescr_js;
/