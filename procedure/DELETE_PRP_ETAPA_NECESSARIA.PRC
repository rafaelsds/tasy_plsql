CREATE OR REPLACE procedure delete_prp_etapa_necessaria(nr_seq_projeto_p Number) is

begin
	if	(nr_seq_projeto_p is not null) then
		delete from prp_etapa_necessaria
		where nr_seq_projeto = nr_seq_projeto_p;
	end if;
commit;
end delete_prp_etapa_necessaria;
/