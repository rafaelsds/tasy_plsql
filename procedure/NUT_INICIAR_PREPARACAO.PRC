create or replace
procedure nut_iniciar_preparacao(nr_sequencia_p	number,
				nm_usuario_p	Varchar2) is 

begin

	if (nr_sequencia_p <> 0 and nm_usuario_p is not null) then
		begin
			update 	NUT_ITEM_EXTRA
			set	dt_preparacao = sysdate,
				nm_usuario_preparacao = nm_usuario_p
			where	nr_sequencia = nr_sequencia_p;

			commit;
		end;
	end if;

end nut_iniciar_preparacao;
/
