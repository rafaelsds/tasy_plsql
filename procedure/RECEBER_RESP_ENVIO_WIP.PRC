create or replace
procedure receber_resp_envio_wip(	cd_material_p		number,
				ie_status_retorno_p		varchar2,
				ds_retorno_p		varchar2,
				nm_usuario_p		Varchar2) is 

begin

if	(ie_status_retorno_p = '1') then

	update	material
	set	ie_status_envio	= 'O'
	where	cd_material	= cd_material_p;

	insert into cot_compra_log_integracao(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_material,
		ie_status_envio,
		ie_tipo_integracao,
		ds_log)
	values(	cot_compra_log_integracao_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_material_p,
		'OM',
		'M',
		Wheb_mensagem_pck.get_Texto(306732)); /*'O material foi integrado corretamente.'*/
			
	update	material_fila_transm
	set	ie_evento = 'R'
	where	cd_material = cd_material_p
	and	ie_evento = 'EN';
	
else

	update	material
	set	ie_status_envio	= 'E'
	where	cd_material	= cd_material_p;

	insert into cot_compra_log_integracao(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_material,
		ie_status_envio,
		ie_tipo_integracao,
		ds_log)
	values(	cot_compra_log_integracao_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_material_p,
		'EM',
		'M',
		Wheb_mensagem_pck.get_Texto(306733, 'DS_RETORNO_P='|| DS_RETORNO_P));
		/*O material foi enviado, por�m n�o foi integrado porque retornou o seguinte erro: #@DS_RETORNO_P#@*/

	
	update	material_fila_transm
	set	ie_evento = 'R'
	where	cd_material = cd_material_p
	and	ie_evento = 'EN';

end if;	
		
commit;

end receber_resp_envio_wip;
/
