create or replace
procedure pls_rescindir_sub_estipulante
			(	nr_sequencia_p		Number,
				nm_usuario_p		Varchar2) is 

begin

update	pls_sub_estipulante
set	dt_rescisao	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

end pls_rescindir_sub_estipulante;
/