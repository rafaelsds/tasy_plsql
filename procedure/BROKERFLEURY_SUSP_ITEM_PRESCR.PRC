create or replace
procedure  brokerfleury_susp_item_prescr(	nr_ficha_p	varchar2,
					cd_unidade_p	varchar2,
					nr_seq_prescr_p number) is 

cd_estabelecimento_w	number(10);
ie_agrupa_w				varchar2(1);
nr_prescricao_w			number(14) := null;
nr_seq_prescr_w			number(10) := null;
--nr_seq_prescr_w			varchar2(255);
ie_parametro_91_w		varchar2(1);
ds_retorno_w			varchar2(4000);
					
begin

select	fleury_obter_dados_unidade(cd_unidade_p, 'E'),
		fleury_obter_dados_unidade(cd_unidade_p, 'AF')
into	cd_estabelecimento_w,
		ie_agrupa_w
from	dual;

if (ie_agrupa_w = 'S') then
	select	max(a.nr_prescricao)
	into	nr_prescricao_w
	from	prescr_procedimento a,
			prescr_medica b
	where	a.nr_prescricao = b.nr_prescricao
	and		a.nr_controle_ext = nr_ficha_p
	and		b.cd_estabelecimento = cd_estabelecimento_w;
else
	select	max(a.nr_prescricao)
	into	nr_prescricao_w
	from	prescr_medica a
	where	a.nr_controle = nr_ficha_p
	and		a.cd_estabelecimento = cd_estabelecimento_w;
	
	/*if (nr_prescricao_w is null) then
		select	max(b.nr_prescricao)
		into	nr_prescricao_w
		from	prescr_procedimento a, prescr_medica b
		where	a.nr_prescricao = b.nr_prescricao
		and	a.nr_controle_ext = nr_ficha_p
		and	((b.cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento_w = 0));
	end if;*/
	
end if;

--nr_seq_prescr_w := obter_seq_prescr_prescricao(nr_prescricao_w, 'FLEURYWS', cd_exame_p);
nr_seq_prescr_w	:= nr_seq_prescr_p;
	
if	(nr_seq_prescr_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(194701, 'CD_EXAME='||nvl(nr_seq_prescr_w,'Valor n�o encontrado'));		
end if;

ie_parametro_91_w	:= nvl(obter_valor_param_usuario(924, 91, null, 'FLEURYWS', cd_estabelecimento_w),'N');

Consiste_supensao_prescr(nr_prescricao_w, nr_seq_prescr_w, 'PRESCR_PROCEDIMENTO', ie_parametro_91_w, cd_estabelecimento_w, ds_retorno_w, 'FLEURYWS', 924);
Suspender_item_Prescricao(nr_prescricao_w, nr_seq_prescr_w, 0, null, 'PRESCR_PROCEDIMENTO', 'FLEURYWS');

end brokerfleury_susp_item_prescr;
/