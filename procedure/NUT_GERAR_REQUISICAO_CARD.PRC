create or replace
procedure Nut_gerar_requisicao_card(	nr_seq_ordem_prod_p		Number,
					cd_estabelecimento_p		Number,
					cd_setor_atendimento_p		Number,
					cd_pessoa_requisitante_p	Varchar2,
					nm_usuario_p			Varchar2) is 

ie_insere_obs_ordem_w		Varchar2(1);
cd_centro_custo_w			Number(8);
cd_centro_w			Number(8);
cd_local_estoque_w		Number(4);
cd_operacao_estoque_w		Number(3);
cd_operacao_requisicao_w		Number(3);
ds_ordem_w			Varchar2(80);
qt_componente_w			Number(15,4);
cd_material_w			Number(6);
nr_seq_item_w			Number(10);
nr_sequencia_w			Number(5) := 0;
nr_requisicao_w			Number(10);
nr_seq_ordem_prod_card_w		Number(10);
qt_refeicao_w			Number(10);
qt_perda_w			Number(15);
ie_material_estoque_w		Varchar2(1);
cd_oper_perda_cons_w		Number(3);
nr_movimento_estoque_w		Number(10);
cd_unidade_medida_consumo_w	Varchar2(30);
cd_unidade_medida_estoque_w	Varchar2(30);
qt_conv_estoque_consumo_w	Number(13,4);
dt_mes_referencia_w		Date;
ie_origem_documento_w		Varchar2(3) := '20';
cd_conta_contabil_w		Varchar2(20)	:= null;
					
Cursor C01 is
	select	distinct
		a.cd_centro_custo
	from	nut_ordem_prod_centro a,
		nut_ordem_prod_card b
	where	a.nr_seq_prod_card = b.nr_sequencia
	and	b.nr_seq_ordem = nr_seq_ordem_prod_p
	order by 1;
	
Cursor C02 is
	select	b.cd_material,
		substr(obter_dados_material_estab(b.cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo,
		sum(a.qt_refeicao)
	from	nut_ordem_prod_centro a,
		nut_ordem_prod_card b		
	where	a.nr_seq_prod_card = b.nr_sequencia
	and	b.nr_seq_ordem = nr_seq_ordem_prod_p
	and	a.cd_centro_custo = cd_centro_custo_w
	and	b.cd_material is not null
	group by b.cd_material,
		substr(obter_dados_material_estab(b.cd_material,cd_estabelecimento_p,'UMS'),1,30)
	order by 1;
	
Cursor C03 is
	select	b.nr_sequencia,
		b.cd_material,
		b.qt_refeicao
	from	nut_ordem_prod_card b
	where	b.nr_seq_ordem = nr_seq_ordem_prod_p
	order by 1;
					
begin

obter_param_usuario(912, 19, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_insere_obs_ordem_w);

select	cd_local_estoque,
	cd_operacao_estoque,
	decode(ie_insere_obs_ordem_w, 'S', ds_ordem,''),
	trunc(dt_ordem,'mm')
into	cd_local_estoque_w,
	cd_operacao_estoque_w,
	ds_ordem_w,
	dt_mes_referencia_w
from	nut_ordem_prod
where	nr_sequencia	= nr_seq_ordem_prod_p;	

select	nvl(max(cd_operacao_requisicao),cd_operacao_estoque_w)
into	cd_operacao_requisicao_w
from	parametros_nutricao;

open C01;
loop
fetch C01 into	
	cd_centro_custo_w;
exit when C01%notfound;
	begin
	
	select	requisicao_seq.nextval
	into	nr_requisicao_w
	from	dual;

	begin
	insert into requisicao_material(
		nr_requisicao,
		cd_estabelecimento,
		cd_setor_atendimento,
		cd_local_estoque,
		dt_solicitacao_requisicao,
		dt_atualizacao,
		nm_usuario,
		cd_operacao_estoque,
		cd_pessoa_requisitante,
		cd_estabelecimento_destino,
		cd_local_estoque_destino,
		cd_centro_custo,
		ie_urgente,
		ds_observacao)
	values(	nr_requisicao_w,
		cd_estabelecimento_p,
		cd_setor_atendimento_p,
		cd_local_estoque_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		cd_operacao_requisicao_w,
		cd_pessoa_requisitante_p,
		cd_estabelecimento_p,
		null,
		cd_centro_custo_w,
		'N',
		ds_ordem_w);
	exception
		when others then			
			--Erro ao gravar requisicao!
			Wheb_mensagem_pck.exibir_mensagem_abort(204865);
	end;

	insert into nut_ordem_prod_req(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_ordem,
		nr_requisicao)
	values(	nut_ordem_prod_req_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_ordem_prod_p,
		nr_requisicao_w);

	commit;
	
	open C02;
	loop
	fetch C02 into	
		cd_material_w,
		cd_unidade_medida_consumo_w,
		qt_componente_w;
	exit when C02%notfound;
		begin
		
		select nvl(max(nr_sequencia),0)
		into	nr_seq_item_w
		from	item_requisicao_material
		where 	nr_requisicao 		= nr_requisicao_w
		and	cd_unidade_medida 	= cd_unidade_medida_consumo_w
		and	cd_material		= cd_material_w;
		
		if	(nr_seq_item_w > 0) then
			update	item_requisicao_material
			set	qt_material_requisitada = qt_material_requisitada + qt_componente_w,
				qt_estoque 		= qt_estoque + qt_componente_w
			where	nr_sequencia 		= nr_seq_item_w
			and	nr_requisicao 		= nr_requisicao_w;
		else
			begin
			nr_sequencia_w   	:= (nr_sequencia_w + 1);

			insert into item_requisicao_material(
				nr_requisicao,
				nr_sequencia,
				cd_estabelecimento,
				cd_material,
				qt_material_requisitada,
				qt_material_atendida,
				vl_material,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida,
				qt_estoque,
				cd_unidade_medida_estoque,
				ie_acao,
				cd_motivo_baixa)
			values(	nr_requisicao_w,
				nr_sequencia_w,
				cd_estabelecimento_p,
				cd_material_w,
				qt_componente_w,
				0,
				0,
				sysdate,
				nm_usuario_p,
				cd_unidade_medida_consumo_w,
				qt_componente_w,
				cd_unidade_medida_consumo_w,
				'1',
				0);
			end;
		end if;
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

open C03;
loop
fetch C03 into	
	nr_seq_ordem_prod_card_w,
	cd_material_w,
	qt_refeicao_w;
exit when C03%notfound;
	begin
	
	select	qt_refeicao_w - sum(qt_refeicao)
	into	qt_perda_w
	from	nut_ordem_prod_centro
	where	nr_seq_prod_card = nr_seq_ordem_prod_card_w;

	if (qt_perda_w > 0) then
	
	ie_material_estoque_w := substr(obter_se_material_estoque(cd_estabelecimento_p, 0, cd_material_w),1,1);

	if	(ie_material_estoque_w = 'S') then
		begin

		select	nvl(max(cd_oper_perda_cons),cd_operacao_estoque_w)
		into	cd_oper_perda_cons_w
		from	parametro_estoque
		where	cd_estabelecimento	= cd_estabelecimento_p;

		select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo,
			substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_consumo,
			qt_conv_estoque_consumo
		into	cd_unidade_medida_consumo_w,
			cd_unidade_medida_estoque_w,
			qt_conv_estoque_consumo_w
		from	material
		where	cd_material = cd_material_w;

		Define_Conta_Material(	cd_estabelecimento_p,
					cd_material_w,
					2,
					null,
					cd_setor_atendimento_p,
					null,
					null,
					null,
					null,
					null,
					cd_local_estoque_w,
					cd_oper_perda_cons_w,
					sysdate,
					cd_conta_contabil_w,
					cd_centro_w,
					null,
					null);
		
		select	movimento_estoque_seq.nextval
		into	nr_movimento_estoque_w
		from	dual;

		insert into movimento_estoque(
			nr_movimento_estoque,
			cd_estabelecimento,
			cd_local_estoque,
			dt_movimento_estoque,
			cd_operacao_estoque,
			cd_acao,
			cd_material,
			dt_mesano_referencia,
			qt_movimento,
			dt_atualizacao,
			nm_usuario,
			ie_origem_documento,
			nr_documento,
			nr_sequencia_item_docto,
			cd_cgc_emitente,
			cd_serie_nf,
			nr_sequencia_documento,
			vl_movimento,
			cd_unidade_medida_estoque,
			cd_procedimento,
			cd_setor_atendimento,
			cd_conta,
			dt_contabil,
			cd_lote_fabricacao,
			dt_validade,
			qt_estoque,
			dt_processo,
			cd_local_estoque_destino,
			cd_centro_custo,
			cd_unidade_med_mov,
			nr_movimento_estoque_corresp,
			cd_conta_contabil,
			cd_material_estoque,
			ie_origem_proced,
			cd_fornecedor,
			nr_lote_contabil,
			ds_observacao,
			nr_seq_lote_fornec)
		values(	nr_movimento_estoque_w,
			cd_estabelecimento_p,
			cd_local_estoque_w,
			sysdate,
			cd_oper_perda_cons_w,
			'1',
			cd_material_w,
			dt_mes_referencia_w,
			(qt_perda_w * qt_conv_estoque_consumo_w),
			sysdate,
			nm_usuario_p,
			ie_origem_documento_w,
			nr_seq_ordem_prod_p,
			null,
			null,
			null,
			null,
			null,
			cd_unidade_medida_estoque_w,
			null,
			null,
			null,
			null,
			nr_seq_ordem_prod_p,
			null,
			qt_perda_w,
			null,
			null,
			null,
			cd_unidade_medida_consumo_w,
			null,
			cd_conta_contabil_w,
			null,
			null,
			null,
			null,
			WHEB_MENSAGEM_PCK.get_texto(303621,'NR_SEQ_ORDEM_PROD_P='|| NR_SEQ_ORDEM_PROD_P ||';NR_SEQ_ORDEM_PROD_CARD_W='|| NR_SEQ_ORDEM_PROD_CARD_W), /*'Nut_gerar_requisicao_card - Produ��o: '||nr_seq_ordem_prod_p||', Card�pio: '||nr_seq_ordem_prod_card_w,*/
			null);
		end;
	end if;
	
	end if;
	end;
end loop;
close C03;

commit;

end Nut_gerar_requisicao_card;
/
