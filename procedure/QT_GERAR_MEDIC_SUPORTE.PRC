create or replace procedure QT_GERAR_MEDIC_SUPORTE(	nr_seq_pac_p    number ) is

nr_seq_material_w           paciente_protocolo_medic.nr_seq_material%type;
nr_agrupamento_w            paciente_protocolo_medic.nr_agrupamento%type;
nm_usuario_w	            paciente_protocolo_medic.nm_usuario%type;
nr_seq_grupo_suporte_w      protocolo_medicacao.nr_seq_grupo_suporte%type;
cd_material_w               grupo_medic_suporte_item.cd_material%type;
qt_dose_w                   grupo_medic_suporte_item.qt_dose%type;
cd_unidade_medida_w         grupo_medic_suporte_item.cd_unidade_medida%type;
ds_dias_aplicacao_w         grupo_medic_suporte_item.ds_dias_aplicacao%type;
ds_ciclos_aplicacao_w       grupo_medic_suporte_item.ds_ciclos_aplicacao%type;
ie_via_aplicacao_w          grupo_medic_suporte_item.ie_via_aplicacao%type;
qt_medic_paciente_w         number(10);
nr_agrup_medic_sup_w        number(10) := 0;

--medicamentos do protocolo sem os diluentes			
cursor c01 is
	select	nr_seq_material,
            nr_agrupamento
	from	paciente_protocolo_medic
	where	nr_seq_paciente = nr_seq_pac_p
    and     nr_seq_diluicao is null
	order by 1;
    
cursor c02 is
	select  cd_material,
            qt_dose,
            cd_unidade_medida,
            ds_dias_aplicacao,
            ds_ciclos_aplicacao,
            ie_via_aplicacao
    from    grupo_medic_suporte_item
    where   nr_sequencia = nr_seq_grupo_suporte_w;

begin
if (nr_seq_pac_p is not null) then
    select  nvl(b.nr_seq_grupo_suporte,0)
    into    nr_seq_grupo_suporte_w
    from    paciente_setor a,
            protocolo_medicacao b
    where   a.nr_seq_paciente = nr_seq_pac_p
    and     a.cd_protocolo = b.cd_protocolo
    and a.nr_seq_medicacao = b.nr_sequencia;
	
	nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
	
	if (nm_usuario_w is null) then
		select nm_usuario
		into nm_usuario_w
		from paciente_setor
		where nr_seq_paciente = nr_seq_pac_p;
	end if;
    
    if (nr_seq_grupo_suporte_w > 0) then
        select  count(*)
        into    qt_medic_paciente_w
        from    grupo_medic_suporte_item
        where   nr_sequencia = nr_seq_grupo_suporte_w;
        
        open c01;
        loop
        fetch c01 into	
            nr_seq_material_w,
            nr_agrupamento_w;
        exit when c01%notfound;
            begin
                update paciente_protocolo_medic
                set     nr_agrupamento = nr_agrupamento_w + qt_medic_paciente_w
                where   nr_seq_paciente = nr_seq_pac_p
                and     nr_seq_material = nr_seq_material_w;
            end;
        end loop;
        close c01;
        
        open c02;
        loop
        fetch c02 into	
            cd_material_w,
            qt_dose_w,
            cd_unidade_medida_w,
            ds_dias_aplicacao_w,
            ds_ciclos_aplicacao_w,
            ie_via_aplicacao_w;
        exit when c02%notfound;
            begin
                nr_agrup_medic_sup_w := nr_agrup_medic_sup_w + 1;
                
                select	nvl(max(nr_seq_material),0) +1
                into	nr_seq_material_w
                from	paciente_protocolo_medic
                where	nr_seq_paciente	= nr_seq_pac_p;
				
                insert into paciente_protocolo_medic
                (
                    nr_seq_paciente,
                    nr_seq_material,
                    nr_agrupamento,
                    cd_material,
                    qt_dose,
                    cd_unidade_medida,
                    ds_dias_aplicacao,
                    ds_ciclos_aplicacao,
                    ie_via_aplicacao,
                    dt_atualizacao,
                    nm_usuario,
                    ie_bomba_infusao,
                    nr_seq_interno
                    
                ) values
                (
                    nr_seq_pac_p,
                    nr_seq_material_w,
                    nr_agrup_medic_sup_w,
                    cd_material_w,
                    qt_dose_w,
                    cd_unidade_medida_w,
                    ds_dias_aplicacao_w,
                    ds_ciclos_aplicacao_w,
                    ie_via_aplicacao_w,
                    sysdate,
                    nm_usuario_w,
                    'n',
                    paciente_protocolo_medic_seq.nextval
                );
            end;
        end loop;
        close c02;
    end if;
end if;

commit;

end QT_GERAR_MEDIC_SUPORTE;
/
