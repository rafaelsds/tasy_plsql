create or replace
procedure libera_analise_conta(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 
begin
update	conta_paciente_retorno	
set	dt_analise = sysdate 
where	nr_sequencia = nr_sequencia_p;
commit;
end libera_analise_conta;
/