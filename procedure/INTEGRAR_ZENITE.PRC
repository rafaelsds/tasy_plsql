create or replace
procedure integrar_zenite (	nr_prescricao_p			number,
							nr_seq_prescricao_p		number,
							nr_seq_evento_p			number) is 

ds_parametros_w		varchar2(255);							
							
begin

if	(nr_seq_evento_p = 380) then
	ds_parametros_w	:= 	'NR_PRESCRICAO='||to_char(nr_prescricao_p)||';'||
						'NR_SEQ_PRESCRICAO='||to_char(nr_seq_prescricao_p)||';';
end if;

if	(ds_parametros_w is not null) then
	gravar_log_lab(225, ds_parametros_w, 'Zenite', null, 'Zenite');
	gravar_agend_integracao(380, ds_parametros_w);
end if;

end integrar_zenite;
/