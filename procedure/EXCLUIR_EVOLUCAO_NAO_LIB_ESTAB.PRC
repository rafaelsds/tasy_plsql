CREATE OR REPLACE
PROCEDURE Excluir_evolucao_Nao_Lib_Estab(
				cd_estabelecimento_p		number,
				qt_hora_adicional_p		number,
				dt_parametro_p			date)	   IS

cd_evolucao_w		Number(10);

Cursor c010 is
	select	 /*+INDEX (a EVOPACI_I5) */
		distinct a.cd_evolucao
	from	atendimento_paciente b,
		evolucao_paciente a
	where	a.dt_evolucao < (dt_parametro_p - qt_hora_adicional_p / 24)
	and	a.nr_atendimento	= b.nr_atendimento
	and	to_char(b.cd_estabelecimento)	= to_char(cd_estabelecimento_p)
	and	a.dt_liberacao is null
	and	a.ie_evolucao_clinica <> 'AE';

BEGIN

OPEN C010;
LOOP
FETCH C010 into	
	cd_evolucao_w;
EXIT WHEN C010%NOTFOUND;
	BEGIN
	
	begin
	delete from evolucao_paciente 
	where	cd_evolucao = cd_evolucao_w;
	exception
		when others then
			cd_evolucao_w := cd_evolucao_w;			
	end;
	gravar_log_exclusao('EVOLUCAO_PACIENTE','JOB_ESTAB',
			'CD_EVOLUCAO : ' || cd_evolucao_w ||obter_desc_expressao(330299)/*' Estabelecimento: '*/||to_char(cd_estabelecimento_p),'N');
	END;
END LOOP;
close c010;

commit;
END Excluir_evolucao_Nao_Lib_Estab; 
/
