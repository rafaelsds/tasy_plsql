create or replace
procedure pls_resposta_solicitacao_aud
			(	nr_seq_auditoria_p	Number,				
				ds_texto_solicitacao_p	Varchar2,
				nm_usuario_p		Varchar2) is 

nr_seq_grupo_atual_w		number(10);
nr_seq_grupo_solic_w		number(10);
nr_seq_guia_w			number(10);
nr_seq_requisicao_w		number(10);
nr_seq_grupo_solicitado_w	number(10);
nr_seq_grupo_w			number(10);
				
begin

select	pls_obter_grupo_analise_atual(nr_seq_auditoria_p)
into	nr_seq_grupo_atual_w
from	dual;

select	nr_seq_grupo_solic,
	nr_seq_grupo
into	nr_seq_grupo_solic_w,
	nr_seq_grupo_w
from	pls_auditoria_grupo
where	nr_sequencia = nr_seq_grupo_atual_w;

pls_inserir_grupo_auditor(nr_seq_grupo_solic_w,nr_seq_auditoria_p,null,nm_usuario_p);

select	nr_seq_guia,
	nr_seq_requisicao
into	nr_seq_guia_w,
	nr_seq_requisicao_w
from	pls_auditoria
where	nr_sequencia = nr_seq_auditoria_p;

if	(nr_seq_guia_w is not null) then
	insert into pls_guia_plano_historico 
		(nr_sequencia, nr_seq_guia, ie_tipo_log,
		dt_historico, dt_atualizacao, nm_usuario,
		ds_observacao, ie_origem_historico, ie_tipo_historico,
		nr_seq_grupo)
	values(	pls_guia_plano_historico_seq.nextval, nr_seq_guia_w, null,
		sysdate, sysdate, nm_usuario_p,
		ds_texto_solicitacao_p,'M','RGA',
		nr_seq_grupo_w);
elsif	(nr_seq_requisicao_w is not null) then
	insert into pls_requisicao_historico 
		(nr_sequencia, nr_seq_requisicao,
		dt_historico, dt_atualizacao, nm_usuario,
		ds_historico, ie_origem_historico, ie_tipo_historico,
		nr_seq_grupo)
	values(	pls_requisicao_historico_seq.nextval, nr_seq_requisicao_w,
		sysdate, sysdate, nm_usuario_p,
		ds_texto_solicitacao_p,'M','RGA',
		nr_seq_grupo_w);
end if;

update	pls_auditoria_grupo
set	dt_liberacao	= sysdate,
	ie_status	= 'P',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_grupo_atual_w;

select	pls_obter_grupo_analise_atual(nr_seq_auditoria_p)
into	nr_seq_grupo_solicitado_w
from	dual;

update	pls_auditoria_grupo
set	ie_manual	= 'N'
where	nr_sequencia	= nr_seq_grupo_solicitado_w;

commit;

end pls_resposta_solicitacao_aud;
/
