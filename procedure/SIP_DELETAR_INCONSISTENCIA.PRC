create or replace
procedure 	sip_deletar_inconsistencia
			(	nr_seq_lote_p		Number,
				nm_usuario_p		Varchar2 ) is

begin

if (nr_seq_lote_p <> 0 ) then
	delete
	from	sip_lote_inconsistencia
	where nr_seq_lote = nr_seq_lote_p;
end if;

commit;
end sip_deletar_inconsistencia;
/
