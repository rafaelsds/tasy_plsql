create or replace 
procedure alterar_juros_multa(
		nr_titulo_p		number,
		tx_juros_p		number,
		tx_multa_p		number,
		cd_tipo_taxa_juro_p	number,
		cd_tipo_taxa_multa_p	number,
		nm_usuario_p		varchar2) is

begin

	if 	(nr_titulo_p is not null) then
	
		update	titulo_receber 
		set	tx_juros = tx_juros_p,
			tx_multa = tx_multa_p,
			cd_tipo_taxa_juro = cd_tipo_taxa_juro_p, 
			cd_tipo_taxa_multa = cd_tipo_taxa_multa_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_titulo = nr_titulo_p;
		
		commit;

	end if;

end alterar_juros_multa;
/