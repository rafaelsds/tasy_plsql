create or replace 
procedure Atualizar_Guia_Retorno(	nr_seq_retorno_p	number,
				    	nm_usuario_p		varchar2) is

nr_interno_conta_w	number(10);
cd_autorizacao_w	varchar2(20);
vl_pago_w		number(15,2);

cursor C01 is
	select	nr_interno_conta,
		cd_autorizacao,
		vl_pago
	from convenio_retorno_item
	where nr_seq_retorno = nr_seq_retorno_p;

begin

open c01;
loop
	fetch c01 into	nr_interno_conta_w,
			cd_autorizacao_w,
			vl_pago_w;
	exit when C01%notfound;

	update conta_paciente_guia
	set vl_convenio = nvl(vl_convenio,0) + vl_pago_w
	where nr_interno_conta	= nr_interno_conta_w
	  and cd_autorizacao	= cd_autorizacao_w
	  and dt_convenio	is null;
end loop;
close c01;
/* commit; Bruna, 0603/2007 - Comentei pois estava dando problema no retorno	*/ 
END Atualizar_Guia_Retorno;
/