create or replace
procedure GRAVA_USUARIO_CONECTADO	(ds_endereco_ip_p		in	varchar2,
					cd_funcao_p			in	varchar2,
					nm_usuario_p			in	varchar2,
					nm_usuario_con_p		in	varchar2,
					ds_maquina_p			in	varchar2,
					dt_fim_conexao_p		in	date,
					nr_porta_p			out 	varchar2) is 

begin

update	usuario_conectado
set	dt_fim_conexao	= sysdate
where 	dt_inicio_conexao 	< (sysdate - 1)
and	dt_fim_conexao is null
and	nm_usuario_con	= nm_usuario_con_p;

update	usuario_conectado
set	dt_fim_conexao	= sysdate
where	dt_fim_conexao	is null
and	ds_endereco_ip	= ds_endereco_ip_p
and	nm_usuario_con	= nm_usuario_con_p;

if	(dt_fim_conexao_p is null) then
	select	to_char(nvl(max(to_number(nr_porta)),'0'))
	into	nr_porta_p
	from	usuario_conectado
	where	dt_fim_conexao	is null
	  and	ds_endereco_ip	= ds_endereco_ip_p
	  and	nm_usuario_con	= nm_usuario_con_p;

	if	(nr_porta_p = '0') then
		select	to_char(decode(nvl(max(to_number(nr_porta)),0),0,100,max(to_number(nr_porta))) + 2) 
		into	nr_porta_p
		from	usuario_conectado
		where	dt_fim_conexao	is null
		and	ds_endereco_ip	= ds_endereco_ip_p;
	end if;

	insert into 
	usuario_conectado	(nr_sequencia,
				nm_usuario_con,
				cd_funcao,
				dt_inicio_conexao,
				dt_atualizacao,
				nm_usuario,
				ds_endereco_ip,
				ds_maquina,
				nr_porta)
	values			(usuario_conectado_seq.nextval,
				nm_usuario_con_p,
				cd_funcao_p,
				sysdate,
				sysdate,
				nm_usuario_p,
				ds_endereco_ip_p,
				ds_maquina_p,
				nr_porta_p);
else
	update usuario_conectado
	set	dt_fim_conexao = dt_fim_conexao_p
	where	nm_usuario_con = nm_usuario_con_p
	and	ds_endereco_ip = ds_endereco_ip_p
	and	dt_fim_conexao is null;
end if;
commit;
end GRAVA_USUARIO_CONECTADO;
/