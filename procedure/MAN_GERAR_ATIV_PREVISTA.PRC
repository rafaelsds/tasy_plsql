create or replace
procedure man_gerar_ativ_prevista(	nr_seq_ordem_p		number,
				nr_seq_funcao_p		number,
				qt_min_prev_p		number,
				nm_usuario_exec_p		varchar2,
				ie_commit_p		varchar2,
				nm_usuario_p		Varchar2,
				nr_seq_ativ_exec_p	number,
				ie_classif_os_p		varchar2 default null,
				nr_seq_estagio_p	number default null,
				nr_seq_origem_p		number default null,
				nr_seq_localizacao_p	number default null,
				nr_seq_gerencia_p		number default null) is 

qt_registro_w			number(10);
qt_prioridade_os_w			number(10) := 10;
ie_desenv_w				varchar2(10);
dt_prevista_w			date;

begin

if	(ie_classif_os_p = 'E') then
	qt_prioridade_os_w := 10;
end if;

select	count(*)
into	qt_registro_w
from	man_ordem_ativ_prev
where	nr_seq_ordem_serv	= nr_seq_ordem_p
and	nr_seq_funcao	= nr_seq_funcao_p
and	nm_usuario_prev	= nm_usuario_exec_p;

if	(qt_registro_w = 0) then

	select  max(ie_desenv)
	into    ie_desenv_w
	from    man_estagio_processo
	where   nr_sequencia    = nr_seq_estagio_p;

	dt_prevista_w	:= null;
	if	(ie_desenv_w = 'S') and
		(nr_seq_origem_p > 0) and
		(nr_seq_localizacao_p	= 1272) and
		(nr_seq_gerencia_p in(3,11)) then
		dt_prevista_w	:= sysdate;
	end if;
		
	insert into man_ordem_ativ_prev(
		nr_sequencia,
		nr_seq_ordem_serv,
		dt_atualizacao,
		nm_usuario,
		ds_atividade,
		dt_prevista,
		qt_min_prev,
		nm_usuario_prev,
		nr_seq_funcao,
		nr_seq_ativ_exec,
		ie_prioridade_desen,
		nm_usuario_nrec,
		dt_atualizacao_nrec)
	values(	man_ordem_ativ_prev_seq.nextval,
		nr_seq_ordem_p,
		sysdate,
		nm_usuario_p,
		'',
		dt_prevista_w, /*Solicitação do Edilson*/
		least(qt_min_prev_p,420),
		nm_usuario_exec_p,
		nr_seq_funcao_p,
		nr_seq_ativ_exec_p,
		qt_prioridade_os_w,
		nm_usuario_p,
		sysdate);
elsif	(qt_registro_w > 0) then
	update	man_ordem_ativ_prev
	set	nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		qt_min_prev	= least(qt_min_prev_p,420)
	where	nr_seq_ordem_serv	= nr_seq_ordem_p
	and	nr_seq_funcao	= nr_seq_funcao_p
	and	nm_usuario_prev	= nm_usuario_exec_p;
end if;

if	(ie_commit_p = 'S') then
	commit;
end if;

end man_gerar_ativ_prevista;
/