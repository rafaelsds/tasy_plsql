create or replace
function Retorna_Data_Inicio_Fim_Prog(nr_seq_prog_p		number,
										ie_tipo_p			varchar2)
 		    	return date is

ie_retorno_date  date;
ie_inicio_prev_w  date;
ie_inicio_real_w  date;

/*
	Esta function tem como objetivo retornar a data prevista de in�cio OU fim de um programa.
	
	ie_tipo_p = 'I'  ir� retornar a data de Inicio do programa
	ie_tipo_p = 'F'  ir� retornar a data de Fim do programa
	
*/
				
begin

if(ie_tipo_p = 'I')then
	select   trunc(min(e.dt_inicio_prev)),
			 trunc(min(e.dt_inicio_real))		
	into  	 ie_inicio_prev_w,
			 ie_inicio_real_w
	from     proj_programa pr,
			 proj_projeto p,
			 proj_cronograma c, 
			 proj_cron_etapa e
	where    pr.nr_sequencia = p.nr_seq_programa
	and		 p.nr_sequencia = c.nr_seq_proj
	and      c.nr_sequencia = e.nr_seq_cronograma
	and      c.ie_situacao  = 'A'
	and		 pr.nr_sequencia = nr_seq_prog_p
	and	e.ie_fase = 'N'
	and	c.dt_aprovacao is not null
	and  	 not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  e.nr_sequencia);

	--Se o programa come�ar antes da data prevista, ent�o deve ser retornado o valor de inicio realizado do programa	
	if(ie_inicio_real_w < ie_inicio_prev_w)then
		ie_retorno_date := ie_inicio_real_w;
	else 
		ie_retorno_date := ie_inicio_prev_w;
	end if;	
	
end if;


if(ie_tipo_p = 'F') then
	select   trunc(max(e.dt_fim_prev))	
	into  	ie_retorno_date
	from     proj_programa pr,
			 proj_projeto p,
			 proj_cronograma c, 
			 proj_cron_etapa e
	where    pr.nr_sequencia = p.nr_seq_programa
	and		 p.nr_sequencia = c.nr_seq_proj
	and      c.nr_sequencia = e.nr_seq_cronograma
	and      c.ie_situacao  = 'A'
	and	e.ie_fase = 'N'
	and	c.dt_aprovacao is not null
	and		 pr.nr_sequencia = nr_seq_prog_p
	and  	 not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  e.nr_sequencia);
end if;

return	ie_retorno_date;

end Retorna_Data_Inicio_Fim_Prog;
/