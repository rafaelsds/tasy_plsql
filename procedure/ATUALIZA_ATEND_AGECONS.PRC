CREATE OR REPLACE
PROCEDURE atualiza_atend_agecons (nr_atendimento_p		NUMBER,
				  ds_lista_agenda_cons_p	VARCHAR2,
				  nm_usuario_p			varchar2) IS

nr_sequencia_w		NUMBER(10,0);
nr_atendimento_w	NUMBER(10,0);
ds_lista_w		VARCHAR2(1000);
tam_lista_w		NUMBER(10,0);
ie_pos_virgula_w	NUMBER(3,0);
				

begin

ds_lista_w := ds_lista_agenda_cons_p;	

if	(substr(ds_lista_w,length(ds_lista_w) - 1, length(ds_lista_w))	<> ',') then
	ds_lista_w	:= ds_lista_w ||',';
end if;

while (ds_lista_w is not null) loop 
	begin
	
	tam_lista_w		:= length(ds_lista_w);
	ie_pos_virgula_w	:= instr(ds_lista_w,',');
	
	if	(ie_pos_virgula_w <> 0) then
		nr_sequencia_w		:= to_number(substr(ds_lista_w,1,(ie_pos_virgula_w - 1)));
		ds_lista_w		:= substr(ds_lista_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;
	
	update	agenda_consulta
	set	nr_atendimento 	= nr_atendimento_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_w;
		
	end;
end loop;

commit;

end atualiza_atend_agecons;
/