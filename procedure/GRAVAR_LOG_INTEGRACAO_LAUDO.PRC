create or replace
procedure gravar_log_integracao_laudo (	nr_acesso_dicom_p	varchar2,
					ds_log_p		varchar2,
					nm_usuario_p		Varchar2 ) is 
					
ie_gravar_log_w		varchar2(1);

begin

select	decode(count(*),0,'N','S')
into	ie_gravar_log_w
from	parametro_integracao_pacs
where	ie_gravar_log_integracao = 'S';


if (ie_gravar_log_w = 'S') then

	insert into log_integracao_laudo (	
		nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		nr_acesso_dicom, 
		ds_log)
	values(	log_integracao_laudo_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_acesso_dicom_p,
		ds_log_p);
	
end if;
	

end gravar_log_integracao_laudo;
/