create or replace
procedure CONSISTE_ENCAIXE_TURNO_AGECONS(
		dt_agenda_p date,
		dt_encaixe_p date,
		cd_agenda_p number
		) is
				
hr_inicial_turno_w			agenda_turno.hr_inicial%type;
hr_final_turno_w			agenda_turno.hr_final%type;
qt_perm_enc_turno_w			agenda_turno.qt_encaixe%type;
nr_seq_turno_val_marc_w		agenda_turno.nr_sequencia%type;
qt_encaixe_turno_w			number(10);
ds_erro_w					varchar2(255);
begin

select	max(nr_sequencia)
into	nr_seq_turno_val_marc_w
from	agenda_turno
where	to_char(dt_encaixe_p, 'hh24:mi:ss') between to_char(hr_inicial,'hh24:mi:ss') and to_char(hr_final,'hh24:mi:ss')
and		nvl(dt_inicio_vigencia, to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) <=
		to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
and		nvl(dt_final_vigencia, to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')) >=
		to_date(to_char(dt_agenda_p, 'dd/mm/yyyy') || ' ' || to_char(dt_encaixe_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
and		nr_minuto_intervalo > 0
and		((ie_dia_semana = obter_cod_dia_semana(dt_agenda_p)) or ((ie_dia_semana = 9) and (obter_cod_dia_semana(dt_agenda_p) not in (7,1))))
and		cd_agenda 		= cd_agenda_p
and		qt_encaixe		is not null;									

if	(nr_seq_turno_val_marc_w is not null) then
	begin
	
	select	max(hr_inicial),
			max(hr_final),
			max(qt_encaixe)
	into	hr_inicial_turno_w,
			hr_final_turno_w,
			qt_perm_enc_turno_w
	from	agenda_turno
	where	nr_sequencia = nr_seq_turno_val_marc_w;
	
	if	(hr_inicial_turno_w is not null) and
		(hr_final_turno_w is not null) and
		(qt_perm_enc_turno_w is not null) then
		
		select	count(*) + 1
		into	qt_encaixe_turno_w
		from	agenda_consulta
		where	nvl(ie_encaixe,'N')	= 'S'		
		and		dt_Agenda 		between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_inicial_turno_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') and
				to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_final_turno_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
		and		ie_status_agenda	not in ('C', 'F', 'I', 'II', 'B', 'R')
		and		cd_agenda			= cd_agenda_p;
	
	end if;
	
	exception
	when others then
		ds_erro_w := substr(sqlerrm,1,255);
	end;
end if;

if	(qt_perm_enc_turno_w is not null) and
	(nvl(qt_encaixe_turno_w,0) > nvl(qt_perm_enc_turno_w,0))then
	/*A quantidade limite de encaixes para este turno foi atingida! Verifique a qtd. permitida no cadastro dos hor�rios da agenda.*/
	wheb_mensagem_pck.exibir_mensagem_abort(260557);
end if;

end CONSISTE_ENCAIXE_TURNO_AGECONS;
/