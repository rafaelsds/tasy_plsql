create or replace
procedure cih_importar_cultura	(nr_ficha_ocorrencia_p	number,
				nm_usuario_p		varchar2,
				dt_cultura_p		date	default null,
				nr_atendimento_p	number	default null) is

nr_atendimento_w	number(10);
cd_amostra_cultura_w	number(10);
cd_topografia_w		number(10);

nr_seq_resultado_w	number(10);
nr_seq_resultado_ant_w	number(10);
dt_coleta_w		date;
cd_microorganismo_w	number(10);
cd_microorganismo_ant_w	number(10);
cd_medicamento_w	number(10);
ie_resultado_w		varchar2(1);
nr_seq_cultura_w	number(10);

nr_seq_material_w	number(10);
nr_seq_prescr_w		number(6);
nr_seq_prescr_ant_w	number(6);
qt_itens_w		number(10);
nr_prescricao_w		number(14);
nr_seq_grupo_microorg_w	number(10);

ds_resultado_antib_w	varchar2(255);
qt_mic_w		number(11);
cd_setor_atendimento_w	number(5);
ie_nao_insere_analito_w	varchar2(1);
		
cursor c01 is
	select	distinct
		d.nr_seq_resultado,
	        nvl(c.dt_coleta,sysdate),
		d.cd_microorganismo,
		d.cd_medicamento,
		d.ie_resultado,
		c.nr_seq_prescr,
		b.nr_prescricao,
		d.ds_resultado_antib,
		d.qt_mic,
		a.cd_setor_atendimento
	from	exame_lab_result_antib d,
		exame_lab_result_item c,
		exame_lab_resultado b,
		prescr_medica a,
		exame_laboratorio f,
		prescr_procedimento e
	where	a.nr_prescricao		= b.nr_prescricao
	and	a.nr_prescricao		= e.nr_prescricao
	and	c.nr_seq_prescr		= e.nr_sequencia
	and	f.nr_seq_exame		= e.nr_seq_exame
	and	b.nr_seq_resultado	= c.nr_seq_resultado
	and	c.nr_seq_resultado	= d.nr_seq_resultado
	and	c.nr_sequencia		= d.nr_seq_result_item
	and	((c.nr_seq_material is not null) or (ie_nao_insere_analito_w = 'N'))
	and	a.nr_atendimento	= nvl(nr_atendimento_p, nr_atendimento_w)
	and	(d.ie_resultado		in ('R','S','I') or upper(c.ds_resultado) like '%POSITIV%')
	and	(	(dt_cultura_p is null) or 
			(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_prescricao) > ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_cultura_p)))
	and	((obter_data_aprov_lab(b.nr_prescricao,c.nr_seq_prescr) is not null) 
	or      (exists(select 1 from result_laboratorio where nr_prescricao = a.nr_prescricao and nr_seq_prescricao = e.nr_sequencia)))
	and	not exists	(select	1
				from	cih_cultura e
				where	e.nr_prescricao = b.nr_prescricao
				and	e.nr_seq_prescr = c.nr_seq_prescr)
	union all
	select	distinct
		d.nr_seq_resultado,
	        nvl(c.dt_coleta,sysdate),
		d.cd_microorganismo,
		null,
		d.ie_resultado,
		c.nr_seq_prescr,
		b.nr_prescricao,
		d.ds_resultado_antib,
		d.qt_mic,
		a.cd_setor_atendimento
	from	exame_lab_result_antib d,
		exame_lab_result_item c,
		exame_lab_resultado b,
		prescr_medica a,
		exame_laboratorio f,
		prescr_procedimento e
	where	a.nr_prescricao		= b.nr_prescricao
	and	a.nr_prescricao		= e.nr_prescricao
	and	c.nr_seq_prescr		= e.nr_sequencia
	and	f.nr_seq_exame		= e.nr_seq_exame
	and	b.nr_seq_resultado	= c.nr_seq_resultado
	and	c.nr_seq_resultado	= d.nr_seq_resultado
	and	c.nr_sequencia		= d.nr_seq_result_item
	and	((c.nr_seq_material is not null) or (ie_nao_insere_analito_w = 'N'))
	and	a.nr_atendimento	= nvl(nr_atendimento_p, nr_atendimento_w)
	and	(	(dt_cultura_p is null) or 
			(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_prescricao) > ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_cultura_p)))
	and	f.ie_formato_resultado	= 'SDM'
	and	not exists	(select	1
				from	exame_lab_result_antib x
				where	x.nr_seq_resultado	= d.nr_seq_resultado
				and	x.nr_seq_result_item	= d.nr_seq_result_item
				and	d.ie_resultado		in ('R','S','I'))
	and	((obter_data_aprov_lab(b.nr_prescricao,c.nr_seq_prescr) is not null) 
	or      (exists(select 1 from result_laboratorio where nr_prescricao = a.nr_prescricao and nr_seq_prescricao = e.nr_sequencia)))
	and	not exists	(select	1
				from	cih_cultura e
				where	e.nr_prescricao = b.nr_prescricao
				and	e.nr_seq_prescr = c.nr_seq_prescr)
	and	not exists (	select	w.nr_seq_exame
				from	exame_lab_dependente y,
					prescr_procedimento w,
					exame_laboratorio q,
					exame_lab_result_antib x,
					exame_lab_result_item k,
					exame_lab_resultado p
				where	y.ie_geracao = 3
				and	y.nr_seq_exame = e.nr_seq_exame
				and	w.nr_prescricao = a.nr_prescricao
				and	w.nr_seq_exame = y.nr_seq_exame_dep
				and	w.nr_seq_superior = e.nr_sequencia
				and	q.nr_seq_exame = y.nr_seq_exame_dep
				and	k.nr_seq_prescr = w.nr_sequencia
				and	w.nr_prescricao = p.nr_prescricao
				and	p.nr_seq_resultado = k.nr_seq_resultado
				and	k.nr_seq_resultado = x.nr_seq_resultado
				and	k.nr_sequencia = x.nr_seq_result_item
				and	x.ie_resultado in ('R','S','I')
				and	q.ie_formato_resultado = 'SM'
				and	nvl(y.ie_gera_antibiograma,'N') = 'S'
				and	y.nr_seq_grupo_micro = cih_obter_grupo_mic(d.cd_microorganismo))
	order by	1,
			2,
			6,
			3,
			4;

/* microbiologia negativa */
cursor c02 is
	select	c.nr_seq_resultado,
		nvl(c.dt_coleta,sysdate),
		c.nr_seq_prescr,
		b.nr_prescricao,
		a.cd_setor_atendimento
	from	exame_lab_result_item c,
		exame_lab_resultado b,
		exame_laboratorio d,
		prescr_medica a
	where	a.nr_prescricao		= b.nr_prescricao
	and	b.nr_seq_resultado	= c.nr_seq_resultado
	and	d.nr_seq_exame		= c.nr_seq_exame
	and	((c.nr_seq_material is not null) or (ie_nao_insere_analito_w = 'N'))
	and	(upper(nvl(c.ds_resultado,' ')) not like '%INCONCLUS%')
	and	a.nr_atendimento	= nvl(nr_atendimento_p, nr_atendimento_w)
	and	((obter_data_aprov_lab(b.nr_prescricao,c.nr_seq_prescr) is not null)
	or	(exists(select 1 from result_laboratorio where nr_prescricao = a.nr_prescricao and nr_seq_prescricao = c.nr_seq_prescr)))
	and	((dt_cultura_p is null) or (ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_prescricao) > ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_cultura_p)))	
	and	d.ie_formato_resultado in ('SM','SDM')
	and	(	(upper(c.ds_resultado) not like '%POSITIV%') or
			(	(c.ds_resultado is null) and 	
				(not exists	(select	1
						from	exame_lab_result_antib x
						where	x.nr_seq_resultado = b.nr_seq_resultado
						and	x.nr_seq_result_item = c.nr_sequencia
						and	(d.ie_formato_resultado = 'SM' and x.ie_resultado in ('R','S','I'))
						or	(d.ie_formato_resultado = 'SDM')))
			)
		)
	and	not exists	(select	1
				from	cih_cultura e
				where	e.nr_prescricao = b.nr_prescricao
				and	e.nr_seq_prescr = c.nr_seq_prescr);

begin
obter_param_usuario(936,92,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_nao_insere_analito_w);

select	nvl(max(nr_atendimento),0)
into	nr_atendimento_w
from	cih_ficha_ocorrencia
where	nr_ficha_ocorrencia = nr_ficha_ocorrencia_p;

select	min(cd_amostra_cultura)
into	cd_amostra_cultura_w
from	cih_amostra_cultura
where	ie_situacao = 'A';

select	min(cd_topografia)
into	cd_topografia_w
from	cih_topografia
where	ie_situacao = 'A';

select 	nvl(max(cd_topografia),cd_topografia_w)
into 	cd_topografia_w
from 	cih_local_infeccao
where 	nr_ficha_ocorrencia = nr_ficha_ocorrencia_p;

cd_microorganismo_ant_w := 0;
nr_seq_resultado_ant_w := 0;
nr_seq_prescr_ant_w 	:= 0;

open c01;
loop
fetch c01 into	nr_seq_resultado_w,
		dt_coleta_w,
		cd_microorganismo_w,
		cd_medicamento_w,
		ie_resultado_w,
		nr_seq_prescr_w,
		nr_prescricao_w,
		ds_resultado_antib_w,
		qt_mic_w,
		cd_setor_atendimento_w;
exit when c01%notfound;


	if	(nr_seq_resultado_w <> nr_seq_resultado_ant_w) or
		(cd_microorganismo_w <> cd_microorganismo_ant_w) or
		(nr_seq_prescr_w <> nr_seq_prescr_ant_w) then

		select	max(nr_seq_grupo)
		into	nr_seq_grupo_microorg_w
		from	cih_microorganismo
		where	cd_microorganismo = cd_microorganismo_w;
		
		select	max(nr_seq_material)
		into	nr_seq_material_w
		from	exame_lab_result_item
		where 	nr_seq_resultado = nr_seq_resultado_w
		and	nr_seq_prescr	 = nr_seq_prescr_w;

		select 	nvl(max(cd_amostra_cultura),cd_amostra_cultura_w),
			nvl(max(cd_topografia),cd_topografia_w)
		into	cd_amostra_cultura_w,
			cd_topografia_w
		from 	material_exame_lab
		where 	nr_sequencia = nr_seq_material_w;

		select	nvl(max(nr_seq_cultura),0) + 1
		into	nr_seq_cultura_w
		from	cih_cultura
		where	nr_ficha_ocorrencia = nr_ficha_ocorrencia_p;

		insert into cih_cultura	(
			nr_ficha_ocorrencia,
			nr_seq_cultura,
			cd_amostra_cultura,
			dt_coleta,
			cd_microorganismo,
			cd_topografia,
			dt_atualizacao,
			nm_usuario,
			cd_laboratorio,
			ds_codigo_laboratorio,
			ie_resultado,
			cd_caso_infeccao,
			cd_setor_atendimento,
			cd_procedimento,
			ie_multi_resistente,
			nr_prescricao,
			nr_seq_prescr,
			nr_seq_grupo_microorg
		) values (
			nr_ficha_ocorrencia_p,
			nr_seq_cultura_w,
			cd_amostra_cultura_w,
			dt_coleta_w,
			cd_microorganismo_w,
			cd_topografia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			'P',
			null,
			cd_setor_atendimento_w,
			null,
			'N',
			nr_prescricao_w,
			nr_seq_prescr_w,
			nr_seq_grupo_microorg_w);
	end if;
	
	if	(cd_medicamento_w is not null) then
		cd_microorganismo_ant_w := cd_microorganismo_w;
		nr_seq_resultado_ant_w	:= nr_seq_resultado_w;
		nr_seq_prescr_ant_w	:= nr_seq_prescr_w;
		
		select 	count(*)
		into	qt_itens_w
		from	cih_cultura_medic
		where	nr_ficha_ocorrencia	= nr_ficha_ocorrencia_p
		and	nr_seq_cultura		= nr_seq_cultura_w
		and	cd_medicamento		= cd_medicamento_w;

		if	(qt_itens_w = 0) and
			(ie_resultado_w <> 'N') then
		
			insert into cih_cultura_medic (
				nr_ficha_ocorrencia,
				nr_seq_cultura,
				cd_medicamento,
				ie_resultado_cultura,
				dt_atualizacao,
				nm_usuario,
				ds_concentracao
			) values (
				nr_ficha_ocorrencia_p,
				nr_seq_cultura_w,
				cd_medicamento_w,
				decode(ie_resultado_w,'R',2,decode(ie_resultado_w,'S',3,4)),
				sysdate,
				nm_usuario_p,
				nvl(ds_resultado_antib_w,qt_mic_w));
		end if;
	end if;
end loop;
close c01;

nr_seq_prescr_ant_w    := 0;
nr_seq_resultado_ant_w := 0;

open c02;
loop
fetch c02 into	nr_seq_resultado_w,
		dt_coleta_w,
		nr_seq_prescr_w,
		nr_prescricao_w,
		cd_setor_atendimento_w;
exit when c02%notfound;

	select	nvl(max(nr_seq_cultura),0) + 1
	into	nr_seq_cultura_w
	from	cih_cultura
	where	nr_ficha_ocorrencia = nr_ficha_ocorrencia_p;

	select	max(nr_seq_material)
	into	nr_seq_material_w
	from	exame_lab_result_item
	where 	nr_seq_resultado = nr_seq_resultado_w
	and	nr_seq_prescr	 = nr_seq_prescr_w;

	select 	nvl(cd_amostra_cultura,cd_amostra_cultura_w),
		nvl(cd_topografia,cd_topografia_w)
	into	cd_amostra_cultura_w,
		cd_topografia_w
	from 	material_exame_lab
	where 	nr_sequencia = nr_seq_material_w;
	
	
	if	(ie_nao_insere_analito_w = 'N') or
		((ie_nao_insere_analito_w = 'S') and 
		(nr_seq_resultado_w <> nr_seq_resultado_ant_w or nr_seq_prescr_w <> nr_seq_prescr_ant_w))  then
		
		nr_seq_prescr_ant_w    := nr_seq_prescr_w;
		nr_seq_resultado_ant_w := nr_seq_resultado_w;

		insert into cih_cultura	(
			nr_ficha_ocorrencia,
			nr_seq_cultura,
			cd_amostra_cultura,
			dt_coleta,
			cd_microorganismo,
			cd_topografia,
			dt_atualizacao,
			nm_usuario,
			cd_laboratorio,
			ds_codigo_laboratorio,
			ie_resultado,
			cd_caso_infeccao,
			cd_setor_atendimento,
			cd_procedimento,
			ie_multi_resistente,
			nr_prescricao,
			nr_seq_prescr
		) values (
			nr_ficha_ocorrencia_p,
			nr_seq_cultura_w,
			cd_amostra_cultura_w,
			dt_coleta_w,
			null,
			cd_topografia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			'N',
			null,
			cd_setor_atendimento_w,
			null,
			'N',
			nr_prescricao_w,
			nr_seq_prescr_w);
	end if;

end loop;
close c02;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end cih_importar_cultura;
/