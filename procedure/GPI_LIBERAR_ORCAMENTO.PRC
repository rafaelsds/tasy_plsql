create or replace
procedure gpi_liberar_orcamento(	nr_seq_orcamento_p	number,
				ie_operacao_p		varchar2,
				nm_usuario_p		Varchar2) is 

qt_registro_w			number(10);
nr_seq_cronograma_w		number(10);

BEGIN

select	max(nr_seq_cronograma)
into	nr_seq_cronograma_w
from	gpi_orcamento
where	nr_sequencia = nr_seq_orcamento_p;

if	(nr_seq_cronograma_w is not null) then

	select	count(*)
	into	qt_registro_w
	from	gpi_cron_etapa b,
		gpi_orc_item a
	where	a.nr_seq_etapa = b.nr_sequencia
	and	b.nr_seq_cronograma <> nr_seq_cronograma_w
	and	a.nr_seq_orcamento   = nr_seq_orcamento_p;
	
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(279671);
	end if;
end if;
	
if	(ie_operacao_p = 'L') then
	
	update	gpi_orcamento
	set	dt_liberacao 	= sysdate,
		nm_usuario_lib	= nm_usuario_p
	where	nr_sequencia	= nr_seq_orcamento_p;

else
	update	gpi_orcamento
	set	dt_liberacao 	= null,
		nm_usuario_lib	= null
	where	nr_sequencia	= nr_seq_orcamento_p;
end if;
commit;

END gpi_liberar_orcamento;
/