create or replace
procedure integrar_dasa_ws(
					nr_seq_evento_p 		IN NUMBER,
          ds_parametros_p 		in varchar2
                    ) is

 vCountSel number;
begin

  if nr_seq_evento_p = 639  then
    if (wheb_usuario_pck.is_evento_ativo(nr_seq_evento_p)) = 'S' then
        select distinct
         count(1) into vCountSel
        from atendimento_paciente h	,
          prescr_medica m,
          prescr_procedimento a,
          lab_exame_equip e,
          equipamento_lab f
        where h.nr_atendimento = m.nr_atendimento
        and a.nr_prescricao = m.nr_prescricao
        and	a.nr_seq_exame = e.nr_seq_exame
        and	e.cd_equipamento = f.cd_equipamento
        and	f.ds_sigla = 'DASA'
        and	m.nr_prescricao = ds_parametros_p
        order	by a.nr_sequencia;

        if vcountsel > 0 then
          gravar_agend_integracao(nr_seq_evento_p, 'nr_prescricao_p=' || ds_parametros_p || ';');
        end if;

    end if;
  end if;

commit;

end integrar_dasa_ws;
/