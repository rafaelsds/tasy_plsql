create or replace
procedure oft_copiar_formulario (	nr_seq_regra_origem_p		number,
												nr_seq_regra_destino_p		number,
												nm_usuario_p					varchar2) is
												
begin

insert	into oft_formulario_item	(	nr_sequencia, 
													dt_atualizacao, 
													nm_usuario, 
													dt_atualizacao_nrec, 
													nm_usuario_nrec, 
													nr_seq_regra_form, 
													nr_linha, 
													nr_seq_grupo, 
													nr_seq_apresent, 
													qt_tamanho, 
													nm_atributo, 
													nr_seq_visao, 
													ie_obrigatorio, 
													qt_altura)
													select	oft_formulario_item_seq.nextval,
																sysdate, 
																nm_usuario_p, 
																sysdate, 
																nm_usuario_p, 
																nr_seq_regra_destino_p, 
																nr_linha, 
																nr_seq_grupo, 
																nr_seq_apresent, 
																qt_tamanho, 
																nm_atributo, 
																nr_seq_visao, 
																ie_obrigatorio, 
																qt_altura
													from		oft_formulario_item
													where		nr_seq_regra_form	= nr_seq_regra_origem_p;
													
commit;													
													
end oft_copiar_formulario;
/