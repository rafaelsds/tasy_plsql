create or replace
procedure apagar_cot_compra_forn_item(nr_seq_cot_forn_p varchar2) is

begin

delete	from cot_compra_forn_item 
where	nr_seq_cot_forn = nr_seq_cot_forn_p;

commit;

end apagar_cot_compra_forn_item;
/