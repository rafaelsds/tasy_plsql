create or replace
procedure pls_atualizar_valor_digitacao
			(	ie_tipo_despesa_p	Varchar2,
				nr_seq_conta_proc_p	Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

cd_procedimento_w	Number(10);
ie_origem_proced_w	Number(5);
ie_tipo_despesa_w	Varchar2(3);
nr_seq_pacote_w		Number(10);
ie_calculo_pacote_w	Varchar2(2):= 'P';


begin

select	cd_procedimento,
		ie_origem_proced
into	cd_procedimento_w,
		ie_origem_proced_w
from	pls_conta_proc
where	nr_sequencia = nr_seq_conta_proc_p;

select	max(nr_sequencia)
into	nr_seq_pacote_w
from	pls_pacote
where	cd_procedimento	= cd_procedimento_w
and	ie_origem_proced	= ie_origem_proced_w
and	ie_situacao			= 'A';

if	(nvl(nr_seq_pacote_w,0) > 0 ) then
	ie_tipo_despesa_w := 4;
	select	nvl(max(ie_calculo_pacote),'P')
	into	ie_calculo_pacote_w
	from	pls_parametros
	where	cd_estabelecimento	= cd_estabelecimento_p;
else
	select 	substr(nvl(max(ie_classificacao),'1'),1,1)
	into	ie_tipo_despesa_w
	from	procedimento
	where	cd_procedimento	= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w;
	
	ie_tipo_despesa_w	:= substr(ie_tipo_despesa_w,1,1);
end if;
if	(ie_tipo_despesa_w = '1') then /* Atualizar o valor do procedimento */ 
	pls_atualiza_valor_proc(nr_seq_conta_proc_p, 'N', nm_usuario_p,'S',null,null);
elsif	(ie_tipo_despesa_w in ('2','3')) then /* Atualizar os valores das taxas e di�rias */
	pls_atualiza_valor_servico(nr_seq_conta_proc_p, 'N', nm_usuario_p,'S'); 
elsif	(ie_tipo_despesa_w = '4') then /* Atualizar os valores dos pacotes */
	if	(ie_calculo_pacote_w	= 'R') then
		pls_atualiza_valor_proc(nr_seq_conta_proc_p, 'N', nm_usuario_p,'S',null,null);
	else
		pls_atualiza_valor_pacote(nr_seq_conta_proc_p, 'C', nm_usuario_p, 'S', 'N');
	end if;
else
	wheb_mensagem_pck.exibir_mensagem_abort(191853);
	--(-20011,'Problemas com a gera��o de valores. Tipo de despesa do procedimento inv�lido!#@#@');
end if;
commit;

end pls_atualizar_valor_digitacao;
/