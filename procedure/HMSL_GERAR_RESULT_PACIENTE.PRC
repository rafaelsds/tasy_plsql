create or replace
procedure hmsl_gerar_result_paciente(	nr_atendimento_p	number,
				dt_inicial_p		date,
				dt_final_p			date,
				cd_empresa_p		number,
				cd_estabelecimento_p	number,
				ie_status_conta_p	number,
				nm_usuario_p		varchar2) is 


					
cd_medico_w			varchar2(10);
cd_convenio_w			number(10);
cd_classif_setor_w			number(10);
cd_procedimento_amb_w		number(20);
cd_procedimento_tuss_w		number(20);
cd_especialidade_w		number(15);
cd_tipo_procedimento_w		number(10);
nr_cirurgia_w			number(10);
ds_tipo_cirurgia_w			varchar2(255)	:= '';
ie_clinica_w			number(5);
ie_porte_procedimento_w		varchar2(1)	:= '';
ds_porte_procedimento_w		varchar2(255)	:= '';
vl_custo_w			number(15,2);
vl_receita_bruta_w			number(15,2)	:= 0;
vl_diaria_w			number(15,2)	:= 0;
qt_diaria_w			number(10)	:= 0;
qt_cirurgia_w			number(10)	:= 0;
qt_parto_w			number(10)	:= 0;
qt_procedimento_ps_w		number(10)	:= 0;
qt_commit_w			number(10)	:= 0;
ie_tipo_atendimento_w		number(10);

/**Cursor dos pacientes*/
cursor	c00 is					
select	distinct
	a.nr_atendimento
from	conta_paciente_resumo b,
	conta_paciente a
where	a.nr_interno_conta	= b.nr_interno_conta
and	a.nr_atendimento	= nvl(nr_atendimento_p, a.nr_atendimento)
and	b.dt_referencia	between dt_inicial_p and dt_final_p
and	(a.ie_status_acerto = ie_status_conta_p or ie_status_conta_p = 0);

vet00	c00%rowtype;

cursor	c01 is
select	'M' ie_mat_proc,
	a.nr_interno_conta,
	a.dt_mesano_referencia,
	b.cd_material,
	null cd_procedimento,
	null ie_origem_proced,
	null nr_cirurgia,
	b.cd_setor_atendimento,
	b.vl_imposto,
	b.vl_glosa,
	b.vl_custo_sadt,
	b.vl_custo_operacional,
	b.vl_custo,
	b.vl_medico,
	a.cd_convenio_parametro,
	'N' ie_diaria,
	0 qt_resumo,
	b.vl_material,
	0 vl_procedimento,
	b.dt_referencia,
	null nr_seq_proc_interno,
	b.nr_seq_grupo_rec
from	conta_paciente_resumo b,
	conta_paciente a
where	a.nr_interno_conta	= b.nr_interno_conta
and	a.nr_atendimento	= vet00.nr_atendimento
and	b.dt_referencia	between dt_inicial_p and dt_final_p
and	b.cd_material is not null
and	(a.ie_status_acerto = ie_status_conta_p or ie_status_conta_p = 0)
and	nvl(b.qt_exclusao_conta,0) = 0
union
select	'P' ie_mat_proc,
	a.nr_interno_conta,
	a.dt_mesano_referencia,
	null cd_material,
	b.cd_procedimento,
	b.ie_origem_proced,
	b.nr_cirugia nr_cirurgia,
	b.cd_setor_atendimento,
	b.vl_imposto,
	b.vl_glosa,
	b.vl_custo_sadt,
	b.vl_custo_operacional,
	b.vl_custo,
	b.vl_medico,
	a.cd_convenio_parametro,
	b.ie_diaria,
	b.qt_resumo,
	0 vl_material,
	b.vl_procedimento,
	b.dt_referencia,
	b.nr_seq_proc_interno,
	b.nr_seq_grupo_rec
from	conta_paciente_resumo b,
	conta_paciente a
where	a.nr_interno_conta	= b.nr_interno_conta
and	a.nr_atendimento	= vet00.nr_atendimento
and	b.dt_referencia	between dt_inicial_p and dt_final_p
and	b.cd_procedimento is not null
and	(a.ie_status_acerto = ie_status_conta_p or ie_status_conta_p = 0)
and	nvl(b.qt_exclusao_conta,0) = 0;

vet01	c01%rowtype;

begin

delete	from w_hmsl_result_paciente
where	nm_usuario	= nm_usuario_p;

commit;

open c00;
loop
fetch c00 into	
	vet00;
exit when c00%notfound; /**Pacientes**/
	begin
	
	qt_commit_w	:= nvl(qt_commit_w, 0) + 1;
	
	select	nvl(max(ie_clinica),0),
		max(cd_medico_resp),
		max(ie_tipo_atendimento)
	into	ie_clinica_w,
		cd_medico_w,
		ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento	= vet00.nr_atendimento;
	
	open C01;
	loop
	fetch C01 into	
		vet01;
	exit when C01%notfound; 
		begin
		vl_custo_w			:= nvl(vet01.vl_custo,0);
		cd_procedimento_amb_w		:= null;
		cd_procedimento_tuss_w		:= null;
		ds_tipo_cirurgia_w			:= null;
		qt_diaria_w			:= 0;
		vl_diaria_w			:= 0;
		qt_cirurgia_w			:= 0;
		qt_parto_w			:= 0;
		qt_procedimento_ps_w		:= 0;
		
		cd_classif_setor_w			:= obter_classif_setor(vet01.cd_setor_atendimento); 
		
		if	(vet01.ie_mat_proc = 'P') then
		
			
			/*select	count(*)
			into	qt_cirurgia_w
			from	procedimento_paciente b,
				conta_paciente c
			where	c.nr_interno_conta	= b.nr_interno_conta
			and	(c.ie_status_acerto = ie_status_conta_p or ie_status_conta_p = 0)
			and	c.nr_atendimento		= vet00.nr_atendimento
			and	nr_cirurgia is not null;*/
			
			/*Cirurgias*/
			select	count(*)
			into	qt_cirurgia_w
			from	cirurgia
			where	nr_atendimento	= vet00.nr_atendimento;
			
			/*Partos*/
			select	count(*)
			into	qt_parto_w
			from	procedimento b,
				conta_paciente_resumo a,
				conta_paciente c
			where	a.cd_procedimento	= b.cd_procedimento
			and	a.ie_origem_proced	= b.ie_origem_proced
			and	c.nr_interno_conta	= a.nr_interno_conta
			and	c.nr_atendimento		= vet00.nr_atendimento
			and	(c.ie_status_acerto = ie_status_conta_p or ie_status_conta_p = 0)
			and	b.cd_tipo_procedimento	= 95;
			
			/*Procedimentos Pronto Socorro*/
			select	count(*)
			into	qt_procedimento_ps_w
			from	setor_atendimento b,
				conta_paciente_resumo a,
				conta_paciente c
			where	a.cd_setor_atendimento	= b.cd_setor_atendimento
			and	c.nr_interno_conta	= a.nr_interno_conta
			and	c.nr_atendimento		= vet00.nr_atendimento
			and	(c.ie_status_acerto = ie_status_conta_p or ie_status_conta_p = 0)
			and	b.cd_classif_setor	= '1';
						
			select	count(*)
			into	qt_diaria_w
			from	conta_paciente_resumo a,
				conta_paciente c
			where	c.nr_interno_conta	= a.nr_interno_conta
			and	c.nr_atendimento	= vet00.nr_atendimento
			and	a.ie_diaria		= 'S'
			and	(c.ie_status_acerto = ie_status_conta_p or ie_status_conta_p = 0);
			if	(vet01.nr_cirurgia is null) then
				
				ds_tipo_cirurgia_w	:= '�nica';
			else
				ds_tipo_cirurgia_w	:= 'Associada';
			end if;
			
			cd_especialidade_w		:= obter_especialidade_proced(vet01.cd_procedimento, vet01.ie_origem_proced);
			cd_tipo_procedimento_w		:= obter_tipo_procedimento(vet01.cd_procedimento, vet01.ie_origem_proced, 'C');
			ie_porte_procedimento_w		:= obter_porte_procedimento(vet01.cd_procedimento, vet01.ie_origem_proced, null); --Verificar se trazer proc interno
			ds_porte_procedimento_w		:= obter_desc_porte_cirurgico(ie_porte_procedimento_w);
			cd_procedimento_amb_w		:= vet01.cd_procedimento;
			
			if	(vet01.nr_seq_proc_interno is not null) then
				begin
				cd_procedimento_tuss_w	:= define_procedimento_tuss(	cd_estabelecimento_p, 		vet01.nr_seq_proc_interno,
											vet01.cd_convenio_parametro, 	null,
											ie_tipo_atendimento_w,		vet01.dt_referencia,
											vet01.cd_procedimento,		vet01.ie_origem_proced, null, null, null);
				exception when others then
					cd_procedimento_tuss_w	:= null;
				end;
			end if;
		end if;
		
			
		if	(vet01.cd_material is not null) then
			vl_custo_w	:= obter_valor_ultima_compra(cd_estabelecimento_p, 90, vet01.cd_material, null,'N');
		end if;
		
		insert into w_hmsl_result_paciente(
			nr_sequencia,
			dt_mesano_referencia,
			nr_atendimento,
			cd_classif_setor,
			cd_setor_atendimento,
			cd_tipo_procedimento,
			ds_tipo_conta,
			ds_tipo_cirurgia,
			ds_porte_procedimento,
			cd_convenio,
			cd_medico,
			cd_material,
			ie_origem_proced,
			cd_procedimento_amb,
			cd_procedimento_tuss,
			cd_especialidade_proc,
			vl_imposto,
			vl_glosa,
			vl_custo,
			vl_honorario_medico,
			vl_custo_sadt,
			vl_diaria,
			vl_procedimento,
			vl_material,
			qt_cirurgia,
			qt_parto,
			qt_procedimento_ps,
			cd_empresa,
			cd_estabelecimento,
			nm_usuario,
			dt_atualizacao,
			nr_seq_grupo_rec)
		values(	W_HMSL_RESULT_PACIENTE_SEQ.nextval,
			vet01.dt_referencia,
			vet00.nr_atendimento,
			cd_classif_setor_w,
			vet01.cd_setor_atendimento,
			ie_clinica_w,
			'Aberta',
			ds_tipo_cirurgia_w,
			ds_porte_procedimento_w,
			vet01.cd_convenio_parametro,
			cd_medico_w,
			vet01.cd_material,
			vet01.ie_origem_proced,
			cd_procedimento_amb_w,
			cd_procedimento_tuss_w,
			cd_especialidade_w,
			vet01.vl_imposto,
			vet01.vl_glosa,
			vet01.vl_custo,
			vet01.vl_medico,
			vet01.vl_custo_sadt,
			qt_diaria_w,
			vet01.vl_procedimento,
			vet01.vl_material,
			qt_cirurgia_w,
			qt_parto_w,
			qt_procedimento_ps_w,
			cd_empresa_p,
			cd_estabelecimento_p,
			nm_usuario_p,
			sysdate,
			vet01.nr_seq_grupo_rec);
			
			if	(qt_commit_w >= 500) then
				
				commit;
				qt_commit_w	:= 0;
			end if;
		end;
	end loop;
	close C01;
	
	end;
end loop;
close c00;


commit;

end hmsl_gerar_result_paciente;
/