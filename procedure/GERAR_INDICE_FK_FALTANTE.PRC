CREATE OR REPLACE
PROCEDURE Gerar_Indice_FK_Faltante(
			nm_usuario_p		VARCHAR2) IS

nm_tabela_w			VARCHAR2(50);
nm_integr_ref_w		VARCHAR2(50);
ds_erro_w			Varchar2(2000);

CURSOR C01 is
	Select 
		nm_tabela,
		nm_integridade_referencial
	from	integridade_referencial
	WHERE obter_indice_constraint(null, nm_tabela, nm_integridade_referencial, 'D') IS NULL
	order by 1,2;


BEGIN


OPEN C01;
LOOP
FETCH C01 into
	nm_tabela_w,
	nm_integr_ref_w;
Exit when C01%notfound;
	begin
	Gerar_Indice_FK(nm_tabela_w, nm_integr_ref_w, 'Tasy');
	exception
		when others then
			ds_erro_w	:= SQLERRM(sqlcode);
			--insert into log_xxxtasy values(sysdate, nm_usuario_p, 778, 'Tabela: ' || nm_tabela_w || ' IF: ' || 
	---			nm_integr_ref_w || ' Erro: ' || ds_erro_w);
	end;
END LOOP;
Close C01;
	
commit;

END Gerar_Indice_FK_Faltante;
/