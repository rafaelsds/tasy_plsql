CREATE OR REPLACE PROCEDURE LIMPAR_FILA_VISU_SENHA(dias INTEGER) IS
BEGIN

    IF dias > 0 THEN
        DELETE FROM fila_visualizacao_senha
         WHERE dt_visualizacao_senha IS NULL
           AND dt_atualizacao < (SYSDATE - dias);
        COMMIT;
    END IF;

END LIMPAR_FILA_VISU_SENHA;
/