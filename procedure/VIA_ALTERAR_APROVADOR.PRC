create or replace procedure via_alterar_aprovador(
		nr_seq_viagem_p 		number,
		ie_tipo_troca_p 		varchar2,
		cd_pessoa_fisica_new_p 	varchar2,
		ds_justificativa_p 		varchar2,
		nm_usuario_p 		varchar2 ) is
	
	cd_pessoa_fisica_old_w 		pessoa_fisica.cd_pessoa_fisica%type;

begin

	SELECT 	CASE ie_tipo_troca_p
			WHEN 'APV' THEN MAX(cd_pessoa_aprov)
			WHEN 'ASV' THEN MAX(cd_aprov_sub)
			WHEN 'APR' THEN MAX(cd_pessoa_aprov_rel_desp)
			WHEN 'ASR' THEN MAX(cd_pessoa_aprov_rel_desp_sub)
		END cd_pessoa_fisica_old
	INTO 	cd_pessoa_fisica_old_w
	FROM 	fin_gv_pend_aprov
	WHERE 	nr_seq_viagem = nr_seq_viagem_p;
	
	INSERT INTO VIA_VIAGEM_LOG_APROV (
		NR_SEQUENCIA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DS_JUSTIFICATIVA,
		CD_PESSOA_FISICA_OLD,
		IE_TIPO_TROCA,
		NR_SEQ_VIAGEM
	) VALUES (
		VIA_VIAGEM_LOG_APROV_SEQ.NEXTVAL,
		SYSDATE,
		nm_usuario_p,
		SYSDATE,
		nm_usuario_p,
		ds_justificativa_p,
		cd_pessoa_fisica_old_w,
		ie_tipo_troca_p,
		nr_seq_viagem_p
	);
	
	CASE ie_tipo_troca_p
		WHEN 'APV' THEN
			UPDATE	fin_gv_pend_aprov
			SET	cd_pessoa_aprov 			= cd_pessoa_fisica_new_p,
				nm_usuario 			= nm_usuario_p,
				dt_atualizacao 			= sysdate
			WHERE	nr_seq_viagem = nr_seq_viagem_p;
		WHEN 'ASV' THEN
			UPDATE	fin_gv_pend_aprov
			SET	cd_aprov_sub 			= cd_pessoa_fisica_new_p,
				nm_usuario 			= nm_usuario_p,
				dt_atualizacao 			= sysdate
			WHERE	nr_seq_viagem = nr_seq_viagem_p;
		WHEN 'APR' THEN
			UPDATE	fin_gv_pend_aprov
			SET	cd_pessoa_aprov_rel_desp 		= cd_pessoa_fisica_new_p,
				nm_usuario 			= nm_usuario_p,
				dt_atualizacao 			= sysdate
			WHERE	nr_seq_viagem = nr_seq_viagem_p;
		WHEN 'ASR' THEN
			UPDATE	fin_gv_pend_aprov
			SET 	cd_pessoa_aprov_rel_desp_sub 	= cd_pessoa_fisica_new_p,
				nm_usuario 			= nm_usuario_p,
				dt_atualizacao 			= sysdate
			WHERE 	nr_seq_viagem = nr_seq_viagem_p;
	END CASE;
	
end via_alterar_aprovador;
/