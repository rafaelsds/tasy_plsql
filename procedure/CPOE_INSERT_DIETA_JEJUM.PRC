create or replace
procedure cpoe_insert_dieta_jejum(
								nr_atendimento_p		number,
								nr_prescricao_p			number,
								nr_seq_dieta_p			number,
								nr_seq_tipo_P			number,
								hr_prim_horario_p		varchar2,
								ie_inicio_p				varchar2,
								dt_inicio_p				date,
								qt_min_ant_p			number,
								qt_min_depois_p			number,
								qt_hora_ant_p			number,
								qt_hora_depois_p		number,
								nr_seq_objetivo_p		number,
								dt_evento_p				date,
								dt_fim_p				date,
								cd_perfil_p				number,
								nm_usuario_p			varchar2,
								dt_atualizacao_p		date,
								ds_evento_p				varchar2,
								ds_observacao_p			varchar2) is

ie_inicio_w					rep_jejum.ie_inicio%type;
ds_erro_w					varchar2(2000);

begin

ie_inicio_w := 'N';
if (ie_inicio_p in ('C','P')) then
	ie_inicio_w := 'F';
elsif (ie_inicio_p = 'R') then
	ie_inicio_w := 'C';
end if;

insert into rep_jejum
			(nr_sequencia,
			nr_prescricao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_tipo,
			nr_seq_objetivo,
			ie_inicio,
			dt_inicio,
			qt_min_ant,
			qt_min_depois,
			dt_evento,
			ds_evento,
			ds_observacao,
			dt_fim,
			qt_hora_ant,
			qt_hora_depois,
			ie_suspenso,
			ie_repete_copia,
			nr_seq_anterior,
			nr_prescricao_original,
			nr_seq_dieta_cpoe)
values  (
			rep_jejum_seq.nextval,
			nr_prescricao_p,
			dt_atualizacao_p,
			nm_usuario_p,
			dt_atualizacao_p,
			nm_usuario_p,
			nr_seq_tipo_p,
			nr_seq_objetivo_p,
			ie_inicio_w,
			dt_inicio_p,
			qt_min_ant_p,
			qt_min_depois_p,
			dt_evento_p,
			ds_evento_p,
			substr(ds_observacao_p,1,255),
			dt_fim_p,
			qt_hora_ant_p,
			qt_hora_depois_p,
			'N',
			'N',
			null,
			null,
			nr_seq_dieta_p);

commit;

exception when others then
	gravar_log_cpoe(substr('CPOE_INSERT_DIETA_JEJUM EXCEPTION:'|| substr(to_char(sqlerrm),1,2000)
		||'nr_seq_dieta_p: '||nr_seq_dieta_p||' nr_prescricao_p :'||nr_prescricao_p||'nr_atendimento_p: '||nr_atendimento_p||' nr_seq_tipo_P : '|| nr_seq_tipo_P
		||'hr_prim_horario_p:'|| hr_prim_horario_p||' ie_inicio_p : '|| ie_inicio_p||'dt_inicio_p:'|| to_char(dt_inicio_p,'dd/mm/yyyy hh24:mi:ss')
		||'qt_min_ant_p:'|| qt_min_ant_p||'qt_min_depois_p:'||qt_min_depois_p||'qt_hora_ant_p:'||qt_hora_ant_p
		||'qt_hora_depois_p:'||qt_hora_depois_p||'nr_seq_objetivo_p:'|| nr_seq_objetivo_p||'dt_evento_p:'|| dt_evento_p	
		||'dt_fim_p:'|| dt_fim_p||'cd_perfil_p :'|| cd_perfil_p||'nm_usuario_p:'|| nm_usuario_p
		||'dt_atualizacao_p:'|| dt_atualizacao_p||'ds_evento_p:'||ds_evento_p||'ds_observacao_p:'||ds_observacao_p,1,2000), 
		nr_atendimento_p, 'N', nr_seq_dieta_p);
end cpoe_insert_dieta_jejum;
/
