CREATE OR REPLACE
PROCEDURE Duplicar_Preco_ProcAIH
		(	dt_comp_origem_p	date,
			ie_versao_origem_p	varchar2,
			dt_comp_destino_p	date,
			ie_versao_destino_p	varchar2,
			nm_usuario_p		varchar2) is

dt_competencia_w		date;
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
dt_competencia_inicial_w	date;
dt_competencia_final_w		date;
qt_idade_minima_w		number(3);
qt_idade_maxima_w		number(3);
ie_sexo_sus_w			varchar2(1);
ie_inreal_w			varchar2(1);
ie_inatom_w			varchar2(1);
qt_permanencia_w		number(3);
qt_ato_medico_w			number(4);
qt_ato_anestesista_w		number(4);
vl_matmed_w			number(15,2);
vl_diaria_w			number(15,2);
vl_taxas_w			number(15,2);
vl_medico_w			number(15,2);
vl_sadt_w			number(15,2);
ie_versao_w			varchar2(20);

qt_registros_w			number(5)	:= 0;		

Cursor C01 is
	select	dt_comp_destino_p,
		cd_procedimento,
		ie_origem_proced,
		dt_competencia_inicial,
		dt_competencia_final,
		qt_idade_minima,
		qt_idade_maxima,
		ie_sexo_sus,
		ie_inreal,
		ie_inatom,
		qt_permanencia,
		qt_ato_medico,
		qt_ato_anestesista,
		vl_matmed,
		vl_diaria,
		vl_taxas,
		vl_medico,
		vl_sadt,
		ie_versao_destino_p
	from	sus_preco_procAIh
	where	nvl(dt_comp_origem_p, dt_competencia)	= dt_competencia
	and	nvl(ie_versao_origem_p, ie_versao)	= ie_versao;

BEGIN

select	count(*)
into	qt_registros_w
from	sus_preco_procaih
where	dt_competencia	= dt_comp_destino_p;
if	(qt_registros_w	> 0) then
	--r.aise_application_error(-20011,'J� existe uma tabela com a data de competencia: '|| DT_COMP_DESTINO_P);
	wheb_mensagem_pck.exibir_mensagem_abort(263303,'DT_COMP_DESTINO_P='||DT_COMP_DESTINO_P);
end if;

select	count(*)
into	qt_registros_w
from	sus_preco_procaih
where	ie_versao	= ie_versao_destino_p;
if	(qt_registros_w	> 0) then
	--r.aise_application_error(-20011,'J� existe uma tabela com a vers�o: '|| IE_VERSAO_DESTINO_P);
	wheb_mensagem_pck.exibir_mensagem_abort(263304,'IE_VERSAO_DESTINO_P='||IE_VERSAO_DESTINO_P);
end if;

OPEN C01;
LOOP
FETCH C01 into
	dt_competencia_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	dt_competencia_inicial_w,
	dt_competencia_final_w,
	qt_idade_minima_w,
	qt_idade_maxima_w,
	ie_sexo_sus_w,
	ie_inreal_w,
	ie_inatom_w,
	qt_permanencia_w,
	qt_ato_medico_w,
	qt_ato_anestesista_w,
	vl_matmed_w,
	vl_diaria_w,
	vl_taxas_w,
	vl_medico_w,
	vl_sadt_w,
	ie_versao_w;
exit when c01%notfound;
	begin
	insert into sus_preco_procAIH
		(DT_COMPETENCIA,
		CD_PROCEDIMENTO,
		IE_ORIGEM_PROCED,
		DT_COMPETENCIA_INICIAL,
		DT_COMPETENCIA_FINAL,
		QT_IDADE_MINIMA,
		QT_IDADE_MAXIMA,
		IE_SEXO_SUS,
		IE_INREAL,
		IE_INATOM,
		QT_PERMANENCIA,
		QT_ATO_MEDICO,
		QT_ATO_ANESTESISTA,
		VL_MATMED,
		VL_DIARIA,
		VL_TAXAS,
		VL_MEDICO,
		VL_SADT,
		DT_ATUALIZACAO,
		NM_USUARIO,
		IE_VERSAO)
	values	(dt_competencia_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		dt_competencia_inicial_w,
		dt_competencia_final_w,
		qt_idade_minima_w,
		qt_idade_maxima_w,
		ie_sexo_sus_w,
		ie_inreal_w,
		ie_inatom_w,
		qt_permanencia_w,
		qt_ato_medico_w,
		qt_ato_anestesista_w,
		vl_matmed_w,
		vl_diaria_w,
		vl_taxas_w,
		vl_medico_w,
		vl_sadt_w,
		sysdate,
		nm_usuario_p,
		ie_versao_w);
	end;
END LOOP;
CLOSE C01;

commit;

END Duplicar_Preco_ProcAIH;
/