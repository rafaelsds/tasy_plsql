CREATE OR REPLACE PROCEDURE iniciar_finalizar_retriagem_pa(nr_seq_triagem_p NUMBER
                                                          ,nm_usuario_p     VARCHAR2
                                                          ,ie_opcao_p       VARCHAR2) IS
														  
ds_erro_w		varchar2(255);														  
														  
BEGIN
  IF nr_seq_triagem_p IS NOT NULL THEN
    IF ie_opcao_p = 'I' THEN
          
      UPDATE triagem_pronto_atend
         SET dt_inicio_retriagem = SYSDATE
            ,dt_fim_retriagem	=	null		
       WHERE nr_sequencia = nr_seq_triagem_p;
	   
		begin		
			gerar_log_triagem_pa( null,'I',nm_usuario_p, NULL, 'S', NULL, nr_seq_triagem_p);		
		exception
		when others then
			ds_erro_w	:= substr(sqlerrm,1,255);
		end;   
	   
       
    ELSIF ie_opcao_p = 'F' THEN
    
      UPDATE triagem_pronto_atend
         SET dt_fim_retriagem	=	SYSDATE		
       WHERE nr_sequencia = nr_seq_triagem_p;
	   
	   
		begin		
			gerar_log_triagem_pa( null,'F',nm_usuario_p, NULL, 'S', NULL, nr_seq_triagem_p);		
		exception
		when others then
			ds_erro_w	:= substr(sqlerrm,1,255);
		end;
	   
       
    END IF;
  END IF;

  COMMIT;

END iniciar_finalizar_retriagem_pa;
/
