create or replace
procedure replicar_convenios_atendimento(   nr_seq_episodio_p  in episodio_paciente.nr_sequencia%type,
                                            nr_atend_origem_p  in atendimento_paciente.nr_atendimento%type,
                                            nr_atend_destino_p in atendimento_paciente.nr_atendimento%type) is
cursor  cur_convenios is
select  atcapa.cd_convenio cd_convenio,
        atcapa.cd_categoria cd_categoria,
        atcapa.dt_inicio_vigencia dt_inicio_vigencia,
        pefita.nr_sequencia nr_sequencia,
        max(atcapa.cd_tipo_acomodacao) cd_tipo_acomodacao,
        max(atcapa.cd_complemento) cd_complemento,
        max(atcapa.dt_validade_carteira) dt_validade_carteira,
        max(atcapa.dt_ultimo_pagto) dt_ultimo_pagto,
        max(atcapa.cd_dependente) cd_dependente,
        max(atcapa.nr_seq_origem) nr_seq_origem,
        max(atcapa.cd_municipio_convenio) cd_municipio_convenio,
        max(atcapa.ie_regime_internacao) ie_regime_internacao,
        max(atcapa.nm_usuario_original) nm_usuario_original,
        max(atcapa.nr_seq_abrangencia) nr_seq_abrangencia,
        max(atcapa.cd_empresa) cd_empresa,
        max(atcapa.qt_dia_internacao) qt_dia_internacao,
        max(atcapa.ie_tipo_guia) ie_tipo_guia,
        max(atcapa.nr_doc_convenio) nr_doc_convenio,
        max(atcapa.cd_senha) cd_senha,
        max(atcapa.nr_seq_tipo_lib_guia) nr_seq_tipo_lib_guia,
        max(atcapa.nr_doc_conv_principal) nr_doc_conv_principal,
        max(atcapa.cd_senha_provisoria) cd_senha_provisoria,
        max(atcapa.ie_cod_usuario_mae_resp) ie_cod_usuario_mae_resp,
        max(atcapa.ie_autoriza_envio_convenio) ie_autoriza_envio_convenio,
        max(atcapa.ds_observacao) ds_observacao,
        max(atcapa.cd_convenio_glosa) cd_convenio_glosa,
        max(atcapa.cd_categoria_glosa) cd_categoria_glosa,
        max(atcapa.cd_plano_glosa) cd_plano_glosa,
        max(atcapa.cd_usuario_conv_glosa) cd_usuario_conv_glosa,
        max(atcapa.cd_complemento_glosa) cd_complemento_glosa,
        max(atcapa.dt_validade_cart_glosa) dt_validade_cart_glosa,
        max(atcapa.nr_acompanhante) nr_acompanhante,
        max(atcapa.qt_dieta_acomp) qt_dieta_acomp,
        max(atcapa.nr_seq_lib_dieta_conv) nr_seq_lib_dieta_conv,
        max(atcapa.nr_seq_regra_acomp) nr_seq_regra_acomp,
        max(atcapa.ie_lib_dieta) ie_lib_dieta,
        max(atcapa.cd_plano_convenio) cd_plano_convenio,
        max(atcapa.ie_tipo_conveniado) ie_tipo_conveniado,
        max(atcapa.dt_final_vigencia) dt_final_vigencia,
        max(atcapa.cd_usuario_convenio) cd_usuario_convenio,
        max(atcapa.nr_prioridade) nr_prioridade,
        max(atcapa.cd_pessoa_titular) cd_pessoa_titular
from	atendimento_paciente atepac,
        atend_categoria_convenio atcapa,
        pessoa_fisica_taxa pefita
where	atepac.nr_atendimento = atcapa.nr_atendimento
and     atcapa.nr_seq_interno = pefita.nr_seq_atecaco(+)
and	    atepac.nr_seq_episodio = nr_seq_episodio_p
and	    atepac.nr_atendimento = nr_atend_origem_p
and	    nr_atend_origem_p is not null
group by atcapa.cd_convenio,
         atcapa.cd_categoria,
         atcapa.dt_inicio_vigencia,
         pefita.nr_sequencia
union all
select	atcapa.cd_convenio cd_convenio,
        atcapa.cd_categoria cd_categoria,
        atcapa.dt_inicio_vigencia dt_inicio_vigencia,
        pefita.nr_sequencia nr_sequencia,
        max(atcapa.cd_tipo_acomodacao) cd_tipo_acomodacao,
        max(atcapa.cd_complemento) cd_complemento,
        max(atcapa.dt_validade_carteira) dt_validade_carteira,
        max(atcapa.dt_ultimo_pagto) dt_ultimo_pagto,
        max(atcapa.cd_dependente) cd_dependente,
        max(atcapa.nr_seq_origem) nr_seq_origem,
        max(atcapa.cd_municipio_convenio) cd_municipio_convenio,
        max(atcapa.ie_regime_internacao) ie_regime_internacao,
        max(atcapa.nm_usuario_original) nm_usuario_original,
        max(atcapa.nr_seq_abrangencia) nr_seq_abrangencia,
        max(atcapa.cd_empresa) cd_empresa,
        max(atcapa.qt_dia_internacao) qt_dia_internacao,
        max(atcapa.ie_tipo_guia) ie_tipo_guia,
        max(atcapa.nr_doc_convenio) nr_doc_convenio,
        max(atcapa.cd_senha) cd_senha,
        max(atcapa.nr_seq_tipo_lib_guia) nr_seq_tipo_lib_guia,
        max(atcapa.nr_doc_conv_principal) nr_doc_conv_principal,
        max(atcapa.cd_senha_provisoria) cd_senha_provisoria,
        max(atcapa.ie_cod_usuario_mae_resp) ie_cod_usuario_mae_resp,
        max(atcapa.ie_autoriza_envio_convenio) ie_autoriza_envio_convenio,
        max(atcapa.ds_observacao) ds_observacao,
        max(atcapa.cd_convenio_glosa) cd_convenio_glosa,
        max(atcapa.cd_categoria_glosa) cd_categoria_glosa,
        max(atcapa.cd_plano_glosa) cd_plano_glosa,
        max(atcapa.cd_usuario_conv_glosa) cd_usuario_conv_glosa,
        max(atcapa.cd_complemento_glosa) cd_complemento_glosa,
        max(atcapa.dt_validade_cart_glosa) dt_validade_cart_glosa,
        max(atcapa.nr_acompanhante) nr_acompanhante,
        max(atcapa.qt_dieta_acomp) qt_dieta_acomp,
        max(atcapa.nr_seq_lib_dieta_conv) nr_seq_lib_dieta_conv,
        max(atcapa.nr_seq_regra_acomp) nr_seq_regra_acomp,
        max(atcapa.ie_lib_dieta) ie_lib_dieta,
        max(atcapa.cd_plano_convenio) cd_plano_convenio,
        max(atcapa.ie_tipo_conveniado) ie_tipo_conveniado,
        max(atcapa.dt_final_vigencia) dt_final_vigencia,
        max(atcapa.cd_usuario_convenio) cd_usuario_convenio,
        max(atcapa.nr_prioridade) nr_prioridade,
        max(atcapa.cd_pessoa_titular) cd_pessoa_titular
from	atendimento_paciente atepac,
        atend_categoria_convenio atcapa,
        pessoa_fisica_taxa pefita
where	atepac.nr_atendimento = atcapa.nr_atendimento
and     atcapa.nr_seq_interno = pefita.nr_seq_atecaco(+)
and	    atepac.nr_seq_episodio = nr_seq_episodio_p
and	    nr_atend_origem_p is null
group by atcapa.cd_convenio,
         atcapa.cd_categoria,
         atcapa.dt_inicio_vigencia,
         pefita.nr_sequencia;
rec_convenios	cur_convenios%rowtype;

cursor cur_atendimentos is
select  atepac.nr_atendimento,
        atepac.nr_seq_tipo_admissao_fat
from    atendimento_paciente atepac
where   atepac.nr_seq_episodio = nr_seq_episodio_p
and     atepac.nr_atendimento = nr_atend_destino_p
union all
select  atepac.nr_atendimento,
        atepac.nr_seq_tipo_admissao_fat
from    atendimento_paciente atepac
where   atepac.nr_seq_episodio = nr_seq_episodio_p
and     nr_atend_destino_p is null
and     atepac.nr_atendimento <> nvl(nr_atend_origem_p, 0);
rec_atendimentos	cur_atendimentos%rowtype;

cursor cur_atend_destino(nr_atend_dest_p number) is
select  atcapa.cd_convenio,
        atcapa.cd_categoria,
        atcapa.dt_inicio_vigencia
from    atendimento_paciente atepac,
        atend_categoria_convenio atcapa
where   atepac.nr_atendimento = atcapa.nr_atendimento
and     atepac.nr_seq_episodio = nr_seq_episodio_p
and     atepac.nr_atendimento = nr_atend_dest_p;
rec_atend_destino	cur_atend_destino%rowtype;

cursor cur_pessoa_taxa(nr_sequencia_p number) is
select  pefita.qt_dias_pagamento,
        pefita.nr_seq_justificativa,
        pefita.nr_seq_atecaco,
        pefita.nr_atendimento,
        pefita.ie_obriga_pag_adicional,
        pefita.dt_pagamento,
        pefita.cd_pessoa_fisica
from    pessoa_fisica_taxa pefita
where   pefita.nr_sequencia= nr_sequencia_p;
rec_pessoa_taxa	cur_pessoa_taxa%rowtype;

cursor cur_pess_taxa_atend(nr_atend_dest_p number) is
select	atcapa.cd_convenio,
        atcapa.cd_categoria,
        pefita.nr_sequencia
from	atendimento_paciente atepac,
        atend_categoria_convenio atcapa,
        pessoa_fisica_taxa pefita
where   atepac.nr_atendimento = atcapa.nr_atendimento
and     atcapa.nr_seq_interno = pefita.nr_seq_atecaco(+)
and     atepac.nr_atendimento = nr_atend_dest_p;
rec_pess_taxa_atend	cur_pess_taxa_atend%rowtype;

atend_categoria_convenio_w	atend_categoria_convenio%rowtype;
pessoa_fisica_taxa_w        pessoa_fisica_taxa%rowtype;
nr_seq_interno_w		    atend_categoria_convenio.nr_seq_interno%type;
cd_categoria_w		atend_categoria_convenio.cd_categoria%type;
    
begin

open cur_convenios;
loop
fetch cur_convenios into
        rec_convenios;
exit when cur_convenios%notfound;
begin
    open cur_atendimentos;
    loop
    fetch cur_atendimentos into
            rec_atendimentos;
    exit when cur_atendimentos%notfound;
    begin
    atend_categoria_convenio_w := null;

    begin
    select  cd_categoria
    into	cd_categoria_w
    from	categoria_convenio
    where	cd_convenio	    = rec_convenios.cd_convenio
    and     cd_categoria    = rec_convenios.cd_categoria
    and     ie_situacao	    = 'A'
    and     rownum		    = 1;
    exception
    when others then
        select  max(cd_categoria)
        into	cd_categoria_w
        from 	categoria_convenio
        where	cd_convenio 	= rec_convenios.cd_convenio
        and 	ie_situacao 	= 'A'
        and     obter_se_cat_lib_tipo_adm(rec_atendimentos.nr_seq_tipo_admissao_fat, cd_convenio, cd_categoria) = 'S';
    
        -- Categoria e obritatorio, por isso se nao achar, busca a primeira
        if (atend_categoria_convenio_w.cd_categoria is null) then
            select	min(cd_categoria)
            into	cd_categoria_w
            from	categoria_convenio
            where	cd_convenio 	= rec_convenios.cd_convenio
            and     ie_situacao 	= 'A';
        end if;
    end;

    begin 
    select 	*
    into    atend_categoria_convenio_w
    from    atend_categoria_convenio
    where   nr_atendimento      = rec_atendimentos.nr_atendimento
    and     cd_convenio         = rec_convenios.cd_convenio
    and     cd_categoria        = cd_categoria_w
    and     trunc(dt_inicio_vigencia,'mi')  = trunc(rec_convenios.dt_inicio_vigencia,'mi')
    and     rownum              = 1;
    exception
    when others then
        atend_categoria_convenio_w.nr_seq_interno   := null;
        atend_categoria_convenio_w.nr_atendimento   := rec_atendimentos.nr_atendimento;
        atend_categoria_convenio_w.cd_convenio      := rec_convenios.cd_convenio;
        atend_categoria_convenio_w.cd_categoria     := cd_categoria_w;
    end;

    atend_categoria_convenio_w.dt_inicio_vigencia	        :=	rec_convenios.dt_inicio_vigencia;
    atend_categoria_convenio_w.dt_final_vigencia	        :=	rec_convenios.dt_final_vigencia;
    atend_categoria_convenio_w.cd_plano_convenio	        :=	rec_convenios.cd_plano_convenio;
    atend_categoria_convenio_w.ie_tipo_conveniado	        :=	rec_convenios.ie_tipo_conveniado;
    atend_categoria_convenio_w.cd_usuario_convenio	        :=	rec_convenios.cd_usuario_convenio;
    atend_categoria_convenio_w.nr_prioridade	            := 	rec_convenios.nr_prioridade;
    atend_categoria_convenio_w.dt_atualizacao	            :=	sysdate;
    atend_categoria_convenio_w.nm_usuario		            :=	wheb_usuario_pck.get_nm_usuario();
    atend_categoria_convenio_w.cd_tipo_acomodacao           :=	rec_convenios.cd_tipo_acomodacao;
    atend_categoria_convenio_w.cd_complemento               :=	rec_convenios.cd_complemento;
    atend_categoria_convenio_w.dt_validade_carteira         :=	rec_convenios.dt_validade_carteira;
    atend_categoria_convenio_w.dt_ultimo_pagto              :=	rec_convenios.dt_ultimo_pagto;
    atend_categoria_convenio_w.cd_dependente                :=	rec_convenios.cd_dependente;
    atend_categoria_convenio_w.nr_seq_origem                :=	rec_convenios.nr_seq_origem;
    atend_categoria_convenio_w.cd_municipio_convenio        :=	rec_convenios.cd_municipio_convenio;
    atend_categoria_convenio_w.ie_regime_internacao         :=	rec_convenios.ie_regime_internacao;
    atend_categoria_convenio_w.nm_usuario_original          :=	rec_convenios.nm_usuario_original;
    atend_categoria_convenio_w.nr_seq_abrangencia           :=	rec_convenios.nr_seq_abrangencia;
    atend_categoria_convenio_w.cd_empresa                   :=	rec_convenios.cd_empresa;
    atend_categoria_convenio_w.qt_dia_internacao            :=	rec_convenios.qt_dia_internacao;
    atend_categoria_convenio_w.ie_tipo_guia                 :=	rec_convenios.ie_tipo_guia;
    atend_categoria_convenio_w.nr_doc_convenio              :=	rec_convenios.nr_doc_convenio;
    atend_categoria_convenio_w.cd_senha                     :=	rec_convenios.cd_senha;
    atend_categoria_convenio_w.nr_seq_tipo_lib_guia         :=	rec_convenios.nr_seq_tipo_lib_guia;
    atend_categoria_convenio_w.nr_doc_conv_principal        :=	rec_convenios.nr_doc_conv_principal;
    atend_categoria_convenio_w.cd_senha_provisoria          :=	rec_convenios.cd_senha_provisoria;
    atend_categoria_convenio_w.ie_cod_usuario_mae_resp      :=	rec_convenios.ie_cod_usuario_mae_resp;
    atend_categoria_convenio_w.ds_observacao                :=	rec_convenios.ds_observacao;
    atend_categoria_convenio_w.ie_autoriza_envio_convenio   :=	rec_convenios.ie_autoriza_envio_convenio;
    atend_categoria_convenio_w.cd_convenio_glosa            :=	rec_convenios.cd_convenio_glosa;
    atend_categoria_convenio_w.cd_categoria_glosa           :=	rec_convenios.cd_categoria_glosa;
    atend_categoria_convenio_w.cd_plano_glosa               :=	rec_convenios.cd_plano_glosa;
    atend_categoria_convenio_w.cd_usuario_conv_glosa        :=	rec_convenios.cd_usuario_conv_glosa;
    atend_categoria_convenio_w.cd_complemento_glosa         :=	rec_convenios.cd_complemento_glosa;
    atend_categoria_convenio_w.dt_validade_cart_glosa       :=	rec_convenios.dt_validade_cart_glosa;
    atend_categoria_convenio_w.nr_acompanhante              :=	rec_convenios.nr_acompanhante;
    atend_categoria_convenio_w.qt_dieta_acomp               :=	rec_convenios.qt_dieta_acomp;
    atend_categoria_convenio_w.nr_seq_lib_dieta_conv        :=	rec_convenios.nr_seq_lib_dieta_conv;
    atend_categoria_convenio_w.nr_seq_regra_acomp           :=	rec_convenios.nr_seq_regra_acomp;
    atend_categoria_convenio_w.ie_lib_dieta                 :=	rec_convenios.ie_lib_dieta;
    atend_categoria_convenio_w.cd_pessoa_titular            :=  rec_convenios.cd_pessoa_titular;

    if (atend_categoria_convenio_w.nr_seq_interno is null) then
        begin
        select  atend_categoria_convenio_seq.nextval
        into	atend_categoria_convenio_w.nr_seq_interno
        from	dual;
        insert into atend_categoria_convenio values atend_categoria_convenio_w;
        end;
    else
        update	atend_categoria_convenio
        set row = atend_categoria_convenio_w
        where	nr_seq_interno = atend_categoria_convenio_w.nr_seq_interno;
    end if;

    if (rec_convenios.nr_sequencia is not null)  then 
        open cur_pessoa_taxa(rec_convenios.nr_sequencia);
        loop
        fetch cur_pessoa_taxa into
                rec_pessoa_taxa;
        exit when cur_pessoa_taxa%notfound;
        begin 
            
        select 	nr_seq_interno
        into	nr_seq_interno_w
        from	atend_categoria_convenio
        where	nr_atendimento	= rec_atendimentos.nr_atendimento
        and     cd_convenio		= rec_convenios.cd_convenio
        and     cd_categoria	= rec_convenios.cd_categoria
        and     rownum			= 1;                
        
        exception
        when others then
                nr_seq_interno_w := null;
        end;
        --
        begin
        pessoa_fisica_taxa_w := null;
        
        begin 
        select 	*
        into	pessoa_fisica_taxa_w
        from	pessoa_fisica_taxa
        where	nr_seq_atecaco = nr_seq_interno_w;
        exception
        when others then
                pessoa_fisica_taxa_w.nr_sequencia := null;
        end;

        pessoa_fisica_taxa_w.qt_dias_pagamento          := rec_pessoa_taxa.qt_dias_pagamento;
        pessoa_fisica_taxa_w.nr_seq_justificativa       := rec_pessoa_taxa.nr_seq_justificativa;
        pessoa_fisica_taxa_w.nr_seq_atecaco             := nr_seq_interno_w;
        pessoa_fisica_taxa_w.nr_atendimento             := rec_atendimentos.nr_atendimento;
        pessoa_fisica_taxa_w.nm_usuario_nrec            := wheb_usuario_pck.get_nm_usuario();
        pessoa_fisica_taxa_w.nm_usuario                 := wheb_usuario_pck.get_nm_usuario();
        pessoa_fisica_taxa_w.ie_obriga_pag_adicional    := rec_pessoa_taxa.ie_obriga_pag_adicional;
        pessoa_fisica_taxa_w.dt_pagamento               := rec_pessoa_taxa.dt_pagamento;
        pessoa_fisica_taxa_w.dt_atualizacao_nrec        := sysdate;
        pessoa_fisica_taxa_w.dt_atualizacao             := sysdate;
        pessoa_fisica_taxa_w.cd_pessoa_fisica           := rec_pessoa_taxa.cd_pessoa_fisica;

        if (pessoa_fisica_taxa_w.nr_sequencia is null)  then 
            begin
            select  pessoa_fisica_taxa_seq.nextval
            into	pessoa_fisica_taxa_w.nr_sequencia
            from	dual;
            insert into pessoa_fisica_taxa values pessoa_fisica_taxa_w;
            end;
        else 
            update	pessoa_fisica_taxa
            set row = pessoa_fisica_taxa_w
            where	nr_sequencia = pessoa_fisica_taxa_w.nr_sequencia;
        end if;

        end;
        end loop;
        close cur_pessoa_taxa;

    end if;
    
    end;
    end loop;
    close cur_atendimentos;
end;
end loop;
close cur_convenios;

if  (nr_atend_origem_p is not null) then
    open cur_atendimentos;
    loop
    fetch cur_atendimentos into
            rec_atendimentos;
    exit when cur_atendimentos%notfound;
    
        open cur_atend_destino(rec_atendimentos.nr_atendimento);
        loop
        fetch cur_atend_destino into
                rec_atend_destino;
        exit when cur_atend_destino%notfound;
        begin 
        select 	*
        into	atend_categoria_convenio_w
        from	atend_categoria_convenio
        where	nr_atendimento	= nr_atend_origem_p
        and     cd_convenio		= rec_atend_destino.cd_convenio
        and     cd_categoria	= rec_atend_destino.cd_categoria
        and     rownum			= 1;
        exception
        when no_data_found then
            begin
            delete from atend_categoria_convenio
            where   nr_atendimento      =   rec_atendimentos.nr_atendimento
            and     cd_convenio         =   rec_atend_destino.cd_convenio
            and     cd_categoria        =   rec_atend_destino.cd_categoria
            and     trunc(dt_inicio_vigencia,'mi')  =   trunc(rec_atend_destino.dt_inicio_vigencia,'mi');
            end;
        end;
        end loop;
        close cur_atend_destino;
    
        open cur_pess_taxa_atend(rec_atendimentos.nr_atendimento);
        loop
        fetch cur_pess_taxa_atend into
                rec_pess_taxa_atend;
        exit when cur_pess_taxa_atend%notfound;
        begin
        select  pefita.*
        into	pessoa_fisica_taxa_w
        from    atendimento_paciente atepac,
                atend_categoria_convenio atcapa,
                pessoa_fisica_taxa pefita
        where	atepac.nr_atendimento   = atcapa.nr_atendimento
        and     atcapa.nr_seq_interno   = pefita.nr_seq_atecaco
        and	    atcapa.cd_convenio      = rec_pess_taxa_atend.cd_convenio
        and	    atcapa.cd_categoria     = rec_pess_taxa_atend.cd_categoria
        and	    pefita.nr_atendimento   = nr_atend_origem_p
		and     rownum			= 1;
        exception
        when no_data_found then
            begin
            delete from pessoa_fisica_taxa
            where nr_sequencia = rec_pess_taxa_atend.nr_sequencia;
            end;
        end;
        end loop;
        close cur_pess_taxa_atend;

    end loop;
    close cur_atendimentos;

end if;

end replicar_convenios_atendimento;
/