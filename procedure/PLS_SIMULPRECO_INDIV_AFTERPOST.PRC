create or replace
procedure pls_simulpreco_indiv_afterpost(
				nr_sequencia_p		number,
				nr_seq_preco_p		number,
				ie_acao_executada_p	number,
				ie_tipo_benef_p		varchar2,
				nr_seq_produto_p	number,
				ie_alt_prod_dep_p	varchar2,
				qt_sca_bonif_tit_p	out varchar2,
				qt_sca_bonif_dep_p	out varchar2,
				nr_seq_titular_p	out number,
				nm_usuario_p		varchar2) is


nr_sequencia_w		number(10);
qt_scas_bonificacao_w	number(10);
qt_sca_bonif_depen_w	number(10);

begin

if	(ie_acao_executada_p in (1,2)) then
	--Gerar o lançamento automático da simulação
	pls_lancamento_sca_simulacao(nr_sequencia_p, 1, nm_usuario_p);
end if;

select	max(nr_sequencia)
into	nr_sequencia_w
from	pls_simulpreco_individual
where	nr_seq_simulacao = nr_seq_preco_p
and	ie_tipo_benef = 'T';

if	(nvl(nr_sequencia_w,0) > 0) and
	(ie_acao_executada_p in (1,2)) and
	(nvl(nr_seq_produto_p,0)  > 0) then
	
	if	(ie_tipo_benef_p <> 'T') and
		(nr_sequencia_p <> nr_sequencia_w) then
	
		select	sum(qt_scas_bonificacao)
		into	qt_sca_bonif_tit_p
		from	(	select	count(*) qt_scas_bonificacao
				from	pls_bonificacao_vinculo
				where	nr_seq_segurado_simul = nr_sequencia_w
				union all
				select	count(*) qt_scas_bonificacao
				from	pls_sca_vinculo
				where	nr_seq_segurado_simul = nr_sequencia_w);
		
		select	sum(qt_scas_bonificacao)
		into	qt_sca_bonif_dep_p
		from	(	select	count(*) qt_scas_bonificacao
				from	pls_bonificacao_vinculo
				where	nr_seq_segurado_simul = nr_sequencia_p
				union all
				select	count(*) qt_scas_bonificacao
				from	pls_sca_vinculo
				where	nr_seq_segurado_simul = nr_sequencia_p);
		nr_seq_titular_p	:= nr_sequencia_w;
	end if;
	
	if	(ie_alt_prod_dep_p = 'S') then
		pls_alt_produto_dep_sim_preco(nr_sequencia_w,nm_usuario_p);
	end if;	
end if;

commit;

end pls_simulpreco_indiv_afterpost;
/
