create or replace
procedure GERAR_PAGTO_SANTANDER_CNAB_240
		(	nr_seq_envio_p		number,
			nm_usuario_p		varchar2) is

/* PADRAO CNAB 240 VERSAO 060 DE 08/2009 */

ds_conteudo_w		varchar2(240);
cd_estabelecimento_w	number(4);
nr_seq_apres_w		number(10)	:= 0;
qt_titulo_w		number(10);
qt_folha_pagto_w	number(10);
ie_folha_pagto_w	varchar2(1);
qt_cara_w		number(10);
ie_atualizar_bloq_w varchar2(1);

/* header de arquivo */
cd_agencia_estab_w	varchar2(5);
cd_cgc_estab_w		varchar2(14);
cd_convenio_banco_w	varchar2(20);
dt_geracao_w		varchar2(14);
nm_empresa_w		varchar2(30);
nr_conta_estab_w	varchar2(13);
nr_remessa_w		number(10);
ds_endereco_estab_w	varchar2(30);
nr_endereco_estab_w	varchar2(5);
ds_complemento_estab_w	varchar2(15);
ds_municipio_estab_w	varchar2(20);
cd_cep_estab_w		varchar2(8);
sg_estado_estab_w	varchar2(15);
nr_tot_bloqueto_w	varchar2(50);


/* header de lote - DOC */
nr_lote_servico_w	number(10);
ie_forma_lanc_w		varchar2(2);
ie_tipo_pagamento_w	varchar2(3);
ie_finalidade_w		varchar2(2);
ds_endereco_w		varchar2(30);
nr_endereco_w		varchar2(5);
ds_complemento_w	varchar2(15);
ds_municipio_w		varchar2(20);
cd_cep_w		varchar2(8);
sg_estado_w		varchar2(15);
ds_bairro_w		varchar2(15);

/* segmento N - GPS */
cd_contribuinte_gps_w		titulo_pagar.cd_cgc%type;
nm_contribuinte_gps_w		pessoa_juridica.ds_razao_social%type;

/*
G013. Finalidade DOC, TED CIP e TED STR
01 = Credito em Conta Corrente
02 = Pagamento de Aluguel / Condominio
03 = Pagamento de Duplicatas e Titulos
04 = Pagamento de Dividendos
05 = Pagamento de Mensalidades Escolares
07 = Pagamento a Fornecedor / Honorarios
08 = Pagamento de Cambio / Fundos / Bolsas
09 = Repasse de Arrecadacao / Pagamento de Tributos
11 = DOC / TED para Poupanca
12 = DOC / TED para Deposito Judicial
13 = Pensao Alimenticia
14 = Restituicao de Imposto de Renda
99 = Outros 
*/
cursor	c01 is
select	distinct
	decode(b.ie_tipo_pagamento,'CC','01','DOC','03','TED','03','CCP','05','OP','10'),
	decode(nvl(ie_folha_pagto_w,'N'),'S','07',decode(b.ie_tipo_pagamento,'CC','01','99'))
from	titulo_pagar_escrit b,
	banco_escritural a
where	not exists
	(select	1
	from	darf_titulo_pagar x
	where	x.nr_titulo	= b.nr_titulo)
and	not exists
	(select	1
	from	gps_titulo x
	where	x.nr_titulo	= b.nr_titulo)
and	b.ie_tipo_pagamento	in ('DOC','CC','CCP','OP','TED')
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* detalhe - DOC */
nr_sequencia_w		number(10);
cd_camara_compensacao_w	varchar2(3);
cd_banco_w		number(3);
cd_agencia_bancaria_w	varchar2(5);
nr_conta_w		varchar2(13);
nm_pessoa_w		varchar2(30);
nr_titulo_w		number(10);
dt_remessa_retorno_w	date;
vl_escritural_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_despesa_w		number(15,2);
ie_tipo_inscricao_w	varchar2(1);
nr_inscricao_w		varchar2(14);
dt_vencimento_w		date;
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
cd_banco_externo_w	varchar2(3);
qt_reg_w number(10);
ie_digito_conta_w	varchar2(1);
ie_digito_agencia_w	varchar2(1);
cd_historico_w		varchar2(4);

cursor	c02 is
select	lpad(nvl(b.cd_camara_compensacao,decode(b.ie_tipo_pagamento,'TED','018','DOC','700','000')),3,'0'),
	b.cd_banco,
	lpad(substr(b.cd_agencia_bancaria,1,5),5,'0'),
	lpad(substr(b.nr_conta,1,12) || substr(b.ie_digito_conta,1,1),13,'0'),
	rpad(elimina_caractere_especial(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30)),30,' '),
	c.nr_titulo,
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	decode(c.cd_pessoa_fisica,null,'2','1'),
	lpad(nvl(d.nr_cpf,c.cd_cgc),14,'0'),
	c.dt_vencimento_atual,
	b.vl_juros,
	b.vl_multa,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'SNR'),1,255)),' '),30,' ') ds_endereco,
	lpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'NR'),1,255)),'0'),5,'0') nr_endereco,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CO'),1,255)),' '),15,' ') ds_complemento,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'B'),1,255)),' '),15,' ') ds_bairro,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CI'),1,255)),' '),20,' ') ds_municipio,
	lpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CEP'),1,255)),'0'),8,'0') cd_cep,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'UF'),1,255)),' '),2,' ') sg_estado,
	substr(e.cd_banco_externo,1,3),
	nvl(substr(b.ie_digito_agencia,1,1),'0') ie_digito_agencia
from	banco e,
	pessoa_fisica d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	b.cd_banco		= e.cd_banco(+)
and	not exists
	(select	1
	from	darf_titulo_pagar x
	where	x.nr_titulo	= c.nr_titulo)
and	not exists
	(select	1
	from	gps_titulo x
	where	x.nr_titulo	= c.nr_titulo)
and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	b.nr_titulo		= c.nr_titulo
and	b.ie_tipo_pagamento	in ('DOC','CC','CCP','OP','TED')
and	decode(b.ie_tipo_pagamento,'CC','01','DOC','03','TED','03','CCP','05','OP','10')	= ie_forma_lanc_w
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer lote - DOC */
vl_total_w		number(15,2);

/* header de lote - BLQ */
nr_versao_w		varchar2(3);
ie_tipo_servico_w	varchar2(2);

cursor	c03 is
select	distinct
	decode(	(select	count(*)
		from	gps_titulo x
		where	x.nr_titulo	= b.nr_titulo),0,
		decode(	(select	count(*)
			from	darf_titulo_pagar x
			where	x.nr_titulo	= b.nr_titulo),0,
			decode(	b.ie_tipo_pagamento,'PC','11',
				decode(	decode(d.cd_banco_externo,null,b.cd_banco,somente_numero(d.cd_banco_externo)),33,'30',
					decode(	(select	count(*)
						from	banco x
						where	decode(x.cd_banco_externo,null,x.cd_banco,somente_numero(x.cd_banco_externo))	= somente_numero(substr(c.nr_bloqueto,1,3))),0,'11','31'))),
		decode(	c.nr_bloqueto,null,'16','11')),
	decode(c.nr_bloqueto,null,'17','11')) ie_forma_lanc
from	banco d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	b.cd_banco		= d.cd_banco(+)
and	b.nr_titulo		= c.nr_titulo
and	(b.ie_tipo_pagamento in ('BLQ','PC','DDA') or
	exists
	(select	1
	from	darf_titulo_pagar x
	where	x.nr_titulo	= b.nr_titulo) or
	exists
	(select	1
	from	gps_titulo x
	where	x.nr_titulo	= b.nr_titulo))
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p
order by	1;

/* detalhe - BLQ */
nr_bloqueto_cov_w	varchar2(44);
nr_bloqueto_w		varchar2(44);
ie_tipo_identificacao_w	varchar2(2);
dt_apuracao_w		date;
cd_darf_w		varchar2(6);
nr_referencia_w		varchar2(17);
cd_pagamento_w		varchar2(6);
dt_referencia_w		varchar2(6);
vl_inss_w		number(15,2);
vl_outras_entidades_w	number(15,2);
cd_receita_w		varchar2(6);
qt_banco_w		number(10);

cursor	c04 is
select	rpad(elimina_caractere_especial(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30)),30,' '),
	c.nr_titulo,
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	c.dt_vencimento_atual,
	b.vl_juros,
	b.vl_multa,
	rpad(substr(c.nr_bloqueto,1,44),44,' '),
	decode(c.cd_cgc,null,'01','02') ie_tipo_identificacao,
	lpad(nvl(c.cd_cgc,d.nr_cpf),14,'0') nr_inscricao,
	c.nr_bloqueto
from	banco e,
	pessoa_fisica d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	b.nr_titulo		= c.nr_titulo
and	decode(	(select	count(*)
		from	gps_titulo x
		where	x.nr_titulo	= b.nr_titulo),0,
		decode(	(select	count(*)
			from	darf_titulo_pagar x
			where	x.nr_titulo	= b.nr_titulo),0,
			decode(	b.ie_tipo_pagamento,'PC','11',
				decode(	decode(e.cd_banco_externo,null,b.cd_banco,somente_numero(e.cd_banco_externo)),33,'30',
					decode(	(select	count(*)
						from	banco x
						where	decode(x.cd_banco_externo,null,x.cd_banco,somente_numero(x.cd_banco_externo))	= somente_numero(substr(c.nr_bloqueto,1,3))),0,'11','31'))),
		decode(	c.nr_bloqueto,null,'16','11')),
	decode(c.nr_bloqueto,null,'17','11'))	= ie_forma_lanc_w
and	b.cd_banco		= e.cd_banco(+)
and	(b.ie_tipo_pagamento in ('BLQ','PC','DDA') or
	exists
	(select	1
	from	darf_titulo_pagar x
	where	x.nr_titulo	= b.nr_titulo) or
	exists
	(select	1
	from	gps_titulo x
	where	x.nr_titulo	= b.nr_titulo))
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer do arquivo */
qt_registro_w		number(10);

begin

obter_param_usuario(857,64,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_atualizar_bloq_w);


delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

select	count(*)
into	qt_titulo_w
from	titulo_pagar_escrit a
where	a.nr_seq_escrit	= nr_seq_envio_p;

select	count(*)
into	qt_folha_pagto_w
from	titulo_pagar b,
	titulo_pagar_escrit a
where	b.ie_tipo_titulo	= '6'
and	a.nr_titulo		= b.nr_titulo
and	a.nr_seq_escrit		= nr_seq_envio_p;

/* Se todos os titulos forem de folha de pagamento, entao sera gerado o arquivo para folha de pagamento */
if	(nvl(qt_titulo_w,0) = nvl(qt_folha_pagto_w,0)) then

	ie_folha_pagto_w	:= 'S';

else

	ie_folha_pagto_w	:= 'N';

end if;

/* header de arquivo */
nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;

select	lpad(b.cd_cgc,14,'0'),
	rpad(nvl(substr(c.cd_convenio_banco,1,20),' '),20,' '),
	lpad(nvl(substr(c.cd_agencia_bancaria,1,5),'0'),5,'0'),
	lpad(substr(c.cd_conta,1,12) || nvl(c.ie_digito_conta,' '),13,'0'),
	rpad(substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,30),30,' '),
	to_char(sysdate,'ddmmyyyyhh24miss'),
	nvl(a.nr_remessa,a.nr_sequencia),
	b.cd_estabelecimento,
	a.dt_remessa_retorno,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(null,b.cd_cgc,'SNR'),1,255)),' '),30,' ') ds_endereco,
	lpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(null,b.cd_cgc,'NR'),1,255)),'0'),5,'0') nr_endereco,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(null,b.cd_cgc,'CO'),1,255)),' '),15,' ') ds_complemento,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(null,b.cd_cgc,'CI'),1,255)),' '),20,' ') ds_municipio,
	lpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(null,b.cd_cgc,'CEP'),1,255)),'0'),8,'0') cd_cep,
	rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(null,b.cd_cgc,'UF'),1,255)),' '),2,' ') sg_estado
into	cd_cgc_estab_w,
	cd_convenio_banco_w,
	cd_agencia_estab_w,
	nr_conta_estab_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_remessa_w,
	cd_estabelecimento_w,
	dt_remessa_retorno_w,
	ds_endereco_estab_w,
	nr_endereco_estab_w,
	ds_complemento_estab_w,
	ds_municipio_estab_w,
	cd_cep_estab_w,
	sg_estado_estab_w
from	banco_estabelecimento c,
	estabelecimento b,
	banco_escritural a
where	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_sequencia		= nr_seq_envio_p;

ds_conteudo_w	:=	'033' ||
			'0000' ||
			'0' ||
			rpad(' ',9,' ') ||
			'2' ||
			cd_cgc_estab_w ||
			cd_convenio_banco_w ||
			cd_agencia_estab_w ||
			' ' ||
			nr_conta_estab_w ||
			' ' ||
			nm_empresa_w ||
			rpad('Banco Santander',30,' ') ||
			rpad(' ',10,' ') ||
			'1' ||
			dt_geracao_w ||
			lpad(nr_remessa_w,6,'0') ||
			'060' ||
			'00000' ||
			rpad(' ',69,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

if	(nvl(ie_folha_pagto_w,'N') = 'S') then

	ie_tipo_servico_w	:= '30';
	cd_historico_w		:= '2007';

else

	ie_tipo_servico_w	:= '20';
	cd_historico_w		:= '0000';

end if;

open	c01;
loop
fetch	c01 into
	ie_forma_lanc_w,
	ie_finalidade_w;
exit	when c01%notfound;

	/* header de lote */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w		:= 0;

	ds_conteudo_w	:=	'033' ||
				lpad(nr_lote_servico_w,4,'0') ||
				'1' ||
				'C' ||
				ie_tipo_servico_w ||
				ie_forma_lanc_w ||
				'031' ||
				' ' ||
				'2' ||
				cd_cgc_estab_w ||
				cd_convenio_banco_w ||
				cd_agencia_estab_w ||
				' ' ||
				nr_conta_estab_w ||
				' ' ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_estab_w ||
				nr_endereco_estab_w ||
				ds_complemento_estab_w ||
				ds_municipio_estab_w ||
				cd_cep_estab_w ||
				substr(sg_estado_estab_w,1,2) ||
				rpad(' ',18,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c02;
	loop
	fetch	c02 into
		cd_camara_compensacao_w,
		cd_banco_w,
		cd_agencia_bancaria_w,
		nr_conta_w,
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		ie_tipo_inscricao_w,
		nr_inscricao_w,
		dt_vencimento_w,
		vl_juros_w,
		vl_multa_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_complemento_w,
		ds_bairro_w,
		ds_municipio_w,
		cd_cep_w,
		sg_estado_w,
		cd_banco_externo_w,
		ie_digito_agencia_w;
	exit	when c02%notfound;

		if	(cd_banco_externo_w	is not null) then

			cd_banco_w	:= somente_numero(cd_banco_externo_w);

		end if;
		
		select	count(*)
		into	qt_reg_w
		from	titulo_pagar_favorecido
		where	nr_titulo = nr_titulo_w;
		
		if (qt_reg_w > 0) then
		
			select	somente_numero(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'B'),1,3)) cd_banco,
					lpad(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'A'),1,5),5,'0') cd_agencia,
					substr(nvl(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DA'),'0'),1,1) ie_digito,
					lpad(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'C'),1,255),12,'0') nr_conta,
					nvl(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DC'),1,1),'0') ie_digito_conta,
					rpad(elimina_caractere_especial(substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,30)),30,' '),
					decode(a.cd_pessoa_fisica,null,'2','1'),
					lpad(nvl(Obter_cpf_pessoa_fisica(a.cd_pessoa_fisica),a.cd_cgc),14,'0'),
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'SNR'),1,255)),' '),30,' ') ds_endereco,
					lpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'NR'),1,255)),'0'),5,'0') nr_endereco,
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CO'),1,255)),' '),15,' ') ds_complemento,
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'B'),1,255)),' '),15,' ') ds_bairro,
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,255)),' '),20,' ') ds_municipio,
					lpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CEP'),1,255)),'0'),8,'0') cd_cep,
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'UF'),1,255)),' '),2,' ') sg_estado
			into	cd_banco_w,
					cd_agencia_bancaria_w,
					ie_digito_agencia_w,
					nr_conta_w,
					ie_digito_conta_w,
					nm_pessoa_w,
					ie_tipo_inscricao_w,
					nr_inscricao_w,
					ds_endereco_w,
					nr_endereco_w,
					ds_complemento_w,
					ds_bairro_w,
					ds_municipio_w,
					cd_cep_w,
					sg_estado_w
			from	titulo_pagar_favorecido a
			where	a.nr_titulo	= nr_titulo_w;
			
			nr_conta_w := nr_conta_w || ie_digito_conta_w;

		end if;

		/* segmento A */
		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;
		vl_total_w	:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0));

		ds_conteudo_w	:=	'033' || 					-- 001/003
					lpad(nr_lote_servico_w,4,'0') ||		-- 004/007
					'3' ||						-- 008/008
					lpad(nr_sequencia_w,5,'0') ||			-- 009/013
					'A' ||						-- 014/014
					'0' ||						-- 015/015
					'00' ||						-- 016/017







					cd_camara_compensacao_w ||			-- 018/020
					lpad(cd_banco_w,3,'0') ||			-- 021/023
					cd_agencia_bancaria_w ||			-- 024/028
					ie_digito_agencia_w ||				-- 029/029
					nr_conta_w ||					-- 030/041 -- 042/042
					' ' ||						-- 043/043


					nm_pessoa_w ||					-- 044/073
					rpad(nr_titulo_w,20,' ') ||			-- 074/093
					to_char(dt_remessa_retorno_w,'ddmmyyyy') ||	-- 094/101
					'BRL' ||					-- 102/104

					'000000000000000' ||				-- 105/119
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') || --120/134
					rpad(' ',20,' ') ||				-- 135/154
					'00000000' ||					-- 155/162
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') || --163/177
					--'000000000000000' ||				-- 163/177 -- OS 1890444


					rpad(' ',40,' ') ||				-- 178/217
					ie_finalidade_w ||				-- 218/219
					rpad(' ',10,' ') ||				-- 220/229
					'0' ||						-- 230/230

					rpad(' ',10,' ');				-- 231/240

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

		/* segmento B */
		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;

		ds_conteudo_w	:=	'033' ||
					lpad(nr_lote_servico_w,4,'0') ||
					'3' ||
					lpad(nr_sequencia_w,5,'0') ||




					'B' ||
					rpad(' ',3,' ') ||
					ie_tipo_inscricao_w ||
					nr_inscricao_w ||
					ds_endereco_w ||
					nr_endereco_w ||
					ds_complemento_w ||
					ds_bairro_w ||
					ds_municipio_w ||
					cd_cep_w ||
					substr(sg_estado_w,1,2) ||
					to_char(dt_vencimento_w,'ddmmyyyy') ||
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') ||
					'000000000000000' ||
					lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'9999999999990.00')),15,'0') ||
					lpad(somente_numero(to_char(nvl(vl_juros_w,0),'9999999999990.00')),15,'0') ||
					lpad(somente_numero(to_char(nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
					'0000' ||
					rpad(' ',11,' ') ||
					cd_historico_w ||

					rpad(' ',11,' ');

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

	end	loop;
	close	c02;

	/* trailer de lote */
	nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	'033' ||
				lpad(nr_lote_servico_w,4,'0') ||
				'5' ||
				rpad(' ',9,' ') ||
				lpad(nr_sequencia_w + 2,6,'0') ||
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') ||
				'000000000000000000' ||
				'000000' ||
				rpad(' ',175,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c01;

open	c03;
loop
fetch	c03 into
	ie_forma_lanc_w;
exit	when c03%notfound;

	/* header de lote - BLQ */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w		:= 0;

	if	(ie_forma_lanc_w	= '16') or
		(ie_forma_lanc_w	= '17') then

		nr_versao_w		:= '010';
		ie_tipo_servico_w	:= '22';

	elsif	(ie_forma_lanc_w	= '11') then

		nr_versao_w		:= '010';
		ie_tipo_servico_w	:= '20';

	else

		nr_versao_w		:= '030';
		ie_tipo_servico_w	:= '20';

	end if;

	ds_conteudo_w	:=	'033' ||
				lpad(nr_lote_servico_w,4,'0') ||
				'1' ||
				'C' ||
				ie_tipo_servico_w ||
				ie_forma_lanc_w ||
				nr_versao_w ||
				' ' ||
				'2' ||
				cd_cgc_estab_w ||
				cd_convenio_banco_w ||
				cd_agencia_estab_w ||
				' ' ||
				nr_conta_estab_w ||
				' ' ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_estab_w ||
				nr_endereco_estab_w ||
				ds_complemento_estab_w ||
				ds_municipio_estab_w ||
				cd_cep_estab_w ||
				substr(sg_estado_estab_w,1,2) ||
				rpad(' ',18,' ');

	
	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c04;
	loop
	fetch	c04 into
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		dt_vencimento_w,
		vl_juros_w,
		vl_multa_w,
		nr_bloqueto_w,
		ie_tipo_identificacao_w,
		nr_inscricao_w,
		nr_tot_bloqueto_w;
	exit	when c04%notfound;

		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;
		vl_total_w	:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0));

		select	count(*)
		into	qt_banco_w
		from	banco a
		where	decode(a.cd_banco_externo,null,a.cd_banco,somente_numero(a.cd_banco_externo))	= somente_numero(substr(nr_bloqueto_w,1,3));

		if	(nvl(nr_bloqueto_w,'X') <> 'X')  then
		
			if (ie_forma_lanc_w in ('11', '16', '17')) then
				nr_bloqueto_cov_w := Substr(Substr(trim(nr_tot_bloqueto_w),0,12),0,11) || 
					Substr(Substr(trim(nr_tot_bloqueto_w),13,12),0,11) ||
					Substr(Substr(trim(nr_tot_bloqueto_w),25,12),0,11) ||
					Substr(Substr(trim(nr_tot_bloqueto_w),37,12),0,11);	
			else    		  
				select	Converte_Codigo_Bloqueto('Lido_Barra',nr_tot_bloqueto_w)
				into	nr_bloqueto_cov_w
				from	dual;
			end if;			 
		
			if	(length(trim(nr_tot_bloqueto_w)) > 44) then
				
					nr_bloqueto_w := nr_bloqueto_cov_w;
			end if;
			
			if	(nvl(nr_tot_bloqueto_w,'X') <> 'X') and
				(length(trim(nr_tot_bloqueto_w)) > 44) then
				 
				
			    if (nvl(ie_atualizar_bloq_w,'N') = 'S') then	
			 
				   update	titulo_pagar a
				   set	    a.nr_bloqueto = nr_bloqueto_cov_w				
                   where    nr_titulo	= nr_titulo_w;
				
				end if;
				
			end if;
		
		end if;
		
		/* segmento N - DARF normal */
		if	(ie_forma_lanc_w	= '16') then

			select	max(b.dt_apuracao) dt_apuracao,
				lpad(substr(nvl(max(b.cd_darf),'0'),1,6),6,'0') cd_darf,
				lpad(substr(nvl(max(b.nr_referencia),'0'),1,17),17,'0') nr_referencia
			into	dt_apuracao_w,
				cd_darf_w,
				nr_referencia_w
			from	darf b,
				darf_titulo_pagar a
			where	a.nr_seq_darf	= b.nr_sequencia
			and	a.nr_titulo	= nr_titulo_w;

			ds_conteudo_w	:=	'033' ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'N' ||
						'0' ||
						'00' ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(' ',20,' ') ||
						nm_empresa_w ||
						to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						cd_darf_w ||
						ie_tipo_identificacao_w ||
						cd_cgc_estab_w ||
						'16' ||
						to_char(dt_apuracao_w,'ddmmyyyy') ||
						nr_referencia_w ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') ||
						lpad(somente_numero(to_char(nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						lpad(somente_numero(to_char(nvl(vl_juros_w,0),'9999999999990.00')),15,'0') ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						rpad(' ',18,' ') ||
						rpad(' ',10,' ');

		/* segmento N - GPS */
		elsif	(ie_forma_lanc_w	= '17') then

			select	to_char(max(b.dt_referencia),'mmyyyy') dt_referencia,
				lpad(substr(nvl(max(b.cd_pagamento),'0'),1,6),6,'0') cd_pagamento,
				max(c.vl_saldo_titulo) vl_inss,
				max(b.vl_outras_entidades) vl_outras_entidades,
				lpad(nvl(max(obter_dados_tit_pagar(obter_dados_tit_pagar(c.nr_titulo,'TO'),'CNPJ')), 
					max(obter_dados_pf(obter_dados_tit_pagar(c.nr_titulo,'PTO'),'CPF'))),14, '0') cd_contribuinte_gps,
				rpad(max(elimina_caractere_especial(substr(obter_dados_tit_pagar(obter_dados_tit_pagar(c.nr_titulo,'TO'),'PS'),1,200))),30, ' ') nm_contribuinte_gps
			into	dt_referencia_w,
				cd_pagamento_w,
				vl_inss_w,
				vl_outras_entidades_w,
				cd_contribuinte_gps_w,
				nm_contribuinte_gps_w
			from	gps b,
				gps_titulo a,
				titulo_pagar c
			where	a.nr_seq_gps	= b.nr_sequencia
			and	a.nr_titulo = c.nr_titulo
			and	a.nr_titulo	= nr_titulo_w;

			ds_conteudo_w	:=	'033' ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'N' ||
						'0' ||
						'00' ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(' ',20,' ') ||
						nvl(nm_contribuinte_gps_w,nm_empresa_w) ||
						to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						cd_pagamento_w ||
						ie_tipo_identificacao_w ||
						nvl(cd_contribuinte_gps_w, cd_cgc_estab_w) ||
						'17' ||
						dt_referencia_w ||
						lpad(somente_numero(to_char(nvl(vl_inss_w,0),'9999999999990.00')),15,'0') ||
						lpad(somente_numero(to_char(nvl(vl_outras_entidades_w,0),'9999999999990.00')),15,'0') ||
						lpad('0',15,'0') ||
						rpad(' ',45,' ') ||
						rpad(' ',10,' ');

		/* segmento O */
		elsif	(ie_forma_lanc_w	= '11') or
			(qt_banco_w		= 0) then

			ds_conteudo_w	:=	'033' ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'O' ||
						'0' ||
						'00' ||
						rpad(substr(nvl(nr_bloqueto_w,' '),1,44),44,' ') ||
						nm_pessoa_w ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(' ',20,' ') ||
						rpad(' ',78,' ');

		/* segmento J */
		else
		
		select	count(*)
		into	qt_reg_w
		from	titulo_pagar_favorecido
		where	nr_titulo = nr_titulo_w;
		
		if (qt_reg_w > 0) then
		
			select	somente_numero(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'B'),1,3)) cd_banco,
					lpad(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'A'),1,5),5,'0') cd_agencia,
					substr(nvl(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DA'),'0'),1,1) ie_digito,
					lpad(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'C'),1,255),12,'0') nr_conta,
					nvl(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DC'),1,1),'0') ie_digito_conta,
					rpad(elimina_caractere_especial(substr(obter_nome_pf_pj(a.cd_pessoa_fisica,a.cd_cgc),1,30)),30,' '),
					decode(a.cd_pessoa_fisica,null,'2','1'),
					lpad(nvl(Obter_cpf_pessoa_fisica(a.cd_pessoa_fisica),a.cd_cgc),14,'0'),
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'SNR'),1,255)),' '),30,' ') ds_endereco,
					lpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'NR'),1,255)),'0'),5,'0') nr_endereco,
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CO'),1,255)),' '),15,' ') ds_complemento,
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'B'),1,255)),' '),15,' ') ds_bairro,
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,255)),' '),20,' ') ds_municipio,
					lpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CEP'),1,255)),'0'),8,'0') cd_cep,
					rpad(nvl(elimina_caractere_especial(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'UF'),1,255)),' '),2,' ') sg_estado
			into	cd_banco_w,
					cd_agencia_bancaria_w,
					ie_digito_agencia_w,
					nr_conta_w,
					ie_digito_conta_w,
					nm_pessoa_w,
					ie_tipo_inscricao_w,
					nr_inscricao_w,
					ds_endereco_w,
					nr_endereco_w,
					ds_complemento_w,
					ds_bairro_w,
					ds_municipio_w,
					cd_cep_w,
					sg_estado_w
			from	titulo_pagar_favorecido a
			where	a.nr_titulo	= nr_titulo_w;
			
			nr_conta_w := nr_conta_w || ie_digito_conta_w;

		end if;

			ds_conteudo_w	:=	'033' ||															--  1 3
						lpad(nr_lote_servico_w,4,'0') ||												--  4 7
						'3' ||																--  8 8

						lpad(nr_sequencia_w,5,'0') ||													--  9 13
						'J' ||																-- 14 14
						'0' ||																-- 15 15
						'00' ||																-- 16 17



						lpad(substr(nvl(nr_bloqueto_w,' '),1,44),44,' ') ||										-- 18 61
						lpad(substr(nvl(nm_pessoa_w,' '),1,30),30,' ') ||										-- 62 91
						to_char(dt_vencimento_w,'ddmmyyyy') ||												-- 92 99
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') ||						--100 114
						lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'9999999999990.00')),15,'0') ||						--115 129
						lpad(somente_numero(to_char(nvl(vl_multa_w,0) + nvl(vl_juros_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') ||				--130 144
						to_char(dt_remessa_retorno_w,'ddmmyyyy') ||											--145 152
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') || --153 167
						'000000000000000' ||														--168 182
						rpad(nr_titulo_w,20,' ') ||													--183 202
						rpad(' ',20,' ') ||														--203 222
						'09' ||																--223 224
						rpad(' ',16,' ');														--225 240

		end if;

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

		/* segmento J-52 */
		if	(ie_forma_lanc_w	<> '16') and
			(ie_forma_lanc_w	<> '17') and
			(ie_forma_lanc_w	<> '11') and
			(qt_banco_w		<> 0) then

			nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
			nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
			qt_registro_w	:= nvl(qt_registro_w,0) + 1;

			ds_conteudo_w	:=	'033' ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'J' ||
						' ' || 
						'00' ||
						'52' ||
						'2' ||
						lpad(cd_cgc_estab_w,15,'0') ||
						rpad(nm_empresa_w,40,' ') ||
						substr(ie_tipo_identificacao_w,2,1) ||
						lpad(nr_inscricao_w,15,'0') ||
						rpad(nm_pessoa_w,40,' ') ||
						'2' ||
						lpad(cd_cgc_estab_w,15,'0') ||
						rpad(nm_empresa_w,40,' ') ||
						rpad(' ',52,' ');

			insert	into w_envio_banco
				(cd_estabelecimento,
				ds_conteudo,
				dt_atualizacao,
				nm_usuario,
				nr_seq_apres,
				nr_seq_apres_2,
				nr_sequencia)
			values	(cd_estabelecimento_w,
				ds_conteudo_w,
				sysdate,
				nm_usuario_p,
				nr_seq_apres_w,
				nr_seq_apres_w,
				w_envio_banco_seq.nextval);

		end if;

	end	loop;
	close	c04;

	/* trailer de lote */
	nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	'033' || /*Pos 01 a 03*/
				lpad(nr_lote_servico_w,4,'0') || /*Pos 04 a 07*/
				'5' || /*Pos 08*/
				rpad(' ',9,' ') || /*Pos 09 a 17*/
				lpad(nr_sequencia_w + 2,6,'0') || --18 a 23
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') || /*Pos 24 a 41*/
				'000000000000000000' || /*Pos 42 a 59*/
				'000000' || /*Pos 60 a 65*/
				rpad(' ',175,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c03;

/* trailer de arquivo */
nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

ds_conteudo_w	:=	'033' ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			lpad(nr_lote_servico_w,6,'0') ||
			lpad(nvl(qt_registro_w,0) + (nvl(nr_lote_servico_w,0) * 2) + 2,6,'0') ||	/* registros detalhe + header e trailer dos lotes + header e trailer do arquivo */
			rpad(' ',211,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

commit;

end GERAR_PAGTO_SANTANDER_CNAB_240;
/
