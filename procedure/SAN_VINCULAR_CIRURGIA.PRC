create or replace
procedure san_vincular_cirurgia(nr_atendimento_p 		NUMBER,
				nr_sequencia_p 			NUMBER,
				nr_cirurgia_p 			NUMBER,
				nr_atendimento_cirurgia_p 	NUMBER,
				ie_pasta_p			VARCHAR2
			) is 

nr_seq_reserva_W	number(10);
			
begin

if	(nr_sequencia_p is not null) then
	if	(ie_pasta_p ='R') then		
		
		update	san_reserva
		set	nr_atendimento	= nvl(nr_atendimento, nr_atendimento_cirurgia_p),
			nr_cirurgia	= nr_cirurgia_p
		where 	nr_sequencia	= nr_sequencia_p;	
		
		update	san_transfusao
		set 	nr_cirurgia 	= nr_cirurgia_p
		where	nr_seq_reserva 	= nr_sequencia_p
		and	(nr_cirurgia 	<> nr_cirurgia_p
		or	nr_cirurgia is null);
		
	end if;

	if	(ie_pasta_p ='T') then

		update	san_transfusao
		set	nr_atendimento 	= nvl(nr_atendimento, nr_atendimento_cirurgia_p),
			nr_cirurgia	= nr_cirurgia_p
		where 	nr_sequencia	= nr_sequencia_p;
			
		select	nr_seq_reserva
		into	nr_seq_reserva_W
		from	san_transfusao
		where	nr_sequencia =	nr_sequencia_p;

		if	(nr_seq_reserva_w is not null) then
			update	san_reserva
			set	nr_cirurgia 	= nr_cirurgia_p
			where	nr_sequencia 	= nr_seq_reserva_w
			and	(nr_cirurgia	<> nr_cirurgia_p
			or	nr_cirurgia is null);
		end if ;
	end if;
end if;
commit;

end san_vincular_cirurgia;
/