create or replace
procedure Gerar_Vime_Adep (
			nr_atendimento_p	number,			
			cd_material_p		number,
			ie_tipo_item_p		varchar2,
			ie_atend_pac_p		varchar2,
			nr_prescricao_p		number,
			ie_dias_medicamento_p	varchar2,			
			nm_usuario_p		varchar2
			) is	
			
-- ATEND - Todas do atendimento
-- PAC - Todos do paciente

qt_dias_w		number(10);
i			number(10);
nr_sequencia_w		number(10);
nr_sequencia_ww		number(10);
dt_medicacao_w		date;
dt_medicacao_ww		date;
dt_entrada_w		date;
dt_alta_w		date;
nr_atendimento_max_w	number(10);
nr_atendimento_mim_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ie_suspenso_w		varchar2(1);
ie_adm_w		varchar2(1);
ie_status_w		varchar2(1);
ds_comando_sql_w	varchar2(500);
ds_sep_bv_w		varchar2(50);
cd_material_generico_w	number(6);
ie_tem_medicamento_w	varchar2(1);


Cursor C01 is
select	dt_medicacao,
	nr_sequencia
from	Vime_Adep
where	nm_usuario	= nm_usuario_p;

Cursor C02 is
select	a.dt_horario,
	decode(a.dt_suspensao, null, null, 'S'),
	decode(a.dt_fim_horario, null, null, 'A'),
	a.nr_sequencia
from	prescr_medica b,
	prescr_material c,
	prescr_mat_hor a
where	a.nr_prescricao = b.nr_prescricao
and	a.nr_prescricao = c.nr_prescricao
and	a.nr_seq_material = c.nr_sequencia
and	(((nr_atendimento_p is not null) and
	(ie_atend_pac_p = 'ATEND') and
	(b.nr_atendimento = nr_atendimento_p)) or
	((cd_pessoa_fisica_w is not null) and
	(ie_atend_pac_p <> 'ATEND') and	
	(b.cd_pessoa_fisica = cd_pessoa_fisica_w)))	
and	(
	((ie_tipo_item_p = 'PA') and
	(a.cd_material in (	select 	nvl(d.cd_material,0)
				from	material d
				where	d.nr_seq_ficha_tecnica = cd_material_p))) or
	((ie_tipo_item_p <> 'PA') and
	(a.cd_material = cd_material_p))
	)
and	b.dt_liberacao is not null
and	a.dt_horario between dt_entrada_w and dt_alta_w
and	(((nvl(c.ie_acm, 'N') = 'S') and (a.ie_aprazado = 'S')) or
	(nvl(c.ie_acm, 'N') = 'N'))
and	(((nvl(c.ie_se_necessario, 'N') = 'S') and (a.ie_aprazado = 'S')) or
	(nvl(c.ie_se_necessario, 'N') = 'N'))
and	Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
and	nvl(a.ie_horario_especial,'N') = 'N'
order by a.dt_suspensao, a.dt_fim_horario;

begin

delete	Vime_Adep
where	nm_usuario	= nm_usuario_p;

commit;

select	nvl(max(cd_pessoa_fisica),'0')
into	cd_pessoa_fisica_w
from	prescr_medica
where	(((nr_prescricao_p is not null) and
	(nr_prescricao = nr_prescricao_p)) or
	((nr_atendimento_p is not null) and
	(nr_atendimento = nr_atendimento_p)));

if	(ie_atend_pac_p = 'ATEND') then

	select	nvl(min(dt_entrada), sysdate) - 1,
		nvl(max(dt_alta),sysdate) + 1
	into	dt_entrada_w,
		dt_alta_w
	from 	atendimento_paciente	
	where	(((nr_atendimento_p is not null) and
		(nr_atendimento = nr_atendimento_p)) or
		((nr_atendimento_p is null) and
		(cd_pessoa_fisica = cd_pessoa_fisica_w)));		
else
	select	min(nr_atendimento),
		max(nr_atendimento)
	into	nr_atendimento_mim_w,
		nr_atendimento_max_w
	from	atendimento_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	select	nvl(max(dt_entrada),sysdate - 1)
	into	dt_entrada_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_mim_w;
	
	select	nvl(max(dt_alta),sysdate + 1)
	into	dt_alta_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_max_w;	
end if;

qt_dias_w		:= obter_dias_entre_datas(dt_entrada_w,dt_alta_w);
dt_alta_w 		:= fim_dia(dt_alta_w);
dt_medicacao_w		:= trunc(dt_entrada_w);
ie_tem_medicamento_w	:= 'S';

for i in 1..qt_dias_w loop
	begin
	
	if	(ie_dias_medicamento_p = 'S') then
		select	nvl(max('S'),'N')
		into	ie_tem_medicamento_w
		from	prescr_medica b,
			prescr_mat_hor a
		where	a.nr_prescricao = b.nr_prescricao
		and	(
			((ie_tipo_item_p = 'PA') and
			(a.cd_material in (	select 	nvl(d.cd_material,0)
						from	material d
						where	d.nr_seq_ficha_tecnica = cd_material_p))) or
			((ie_tipo_item_p <> 'PA') and
			(a.cd_material = cd_material_p))
			)	
		and	trunc(a.dt_horario) = trunc(dt_medicacao_w)
		and	b.cd_pessoa_fisica = cd_pessoa_fisica_w
		and	(((nr_atendimento_p is not null) and
			(ie_atend_pac_p = 'ATEND') and
			(b.nr_atendimento = nr_atendimento_p)) or
			((cd_pessoa_fisica_w is not null) and
			(ie_atend_pac_p <> 'ATEND') and	
			(b.cd_pessoa_fisica = cd_pessoa_fisica_w)))
		and	b.dt_liberacao is not null
		and	nvl(a.ie_horario_especial,'N') = 'N'
		and	Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S';	
	end if;
	
	if	(ie_tem_medicamento_w = 'S') then
	
		select	Vime_Adep_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		insert into Vime_Adep(
			nr_sequencia,
			nm_usuario,
			dt_medicacao)
		values(	nr_sequencia_w,
			nm_usuario_p,
			dt_medicacao_w);
	end if;
	
	dt_medicacao_w	:= dt_medicacao_w + 1;
	end;
end loop;
commit;

ds_sep_bv_w	:= obter_separador_bv;

open C02;
loop
fetch C02 into	
	dt_medicacao_ww,
	ie_suspenso_w,
	ie_adm_w,
	nr_sequencia_ww;
exit when C02%notfound;
	begin
	
	ds_comando_sql_w := 	' update vime_Adep'||
				' set ds_hora_'||to_char(dt_medicacao_ww,'hh24')||' = :ie'||
				' where dt_medicacao = :dt_medicacao'||
				' and nm_usuario = :nm_usuario';				
				
	if	(ie_suspenso_w is not null) then
		ie_status_w := 'S';
	elsif	(ie_adm_w is not null) then
		ie_status_w := 'A';
	else
		ie_status_w := 'P';
	end if;
	begin
	
	exec_sql_dinamico_bv('VIME', ds_comando_sql_w,		'ie='||nr_sequencia_ww||';'||ie_status_w||ds_sep_bv_w||
								'dt_medicacao='||to_char(dt_medicacao_ww, 'dd/mm/yyyy')||ds_sep_bv_w||
								'nm_usuario='||nm_usuario_p||ds_sep_bv_w);								
											
	exception
	when others then
	--Problemas na gera��o!'||'#@#@');
		null;
	end;
				
				
	end;
end loop;
close C02; 	

commit;

end Gerar_Vime_Adep;
/
