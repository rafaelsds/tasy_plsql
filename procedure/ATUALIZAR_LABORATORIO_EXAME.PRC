create or replace
procedure atualizar_laboratorio_exame (
		nr_seq_grupo_p		number,
		cd_cgc_externo_novo_p	varchar2,
		cd_cgc_externo_ant_p	varchar2) is

begin
if	(nr_seq_grupo_p is not null) and
	(cd_cgc_externo_novo_p is not null) and
	(cd_cgc_externo_ant_p is not null) then
	begin
	update	exame_laboratorio
	set	cd_cgc_externo	= cd_cgc_externo_novo_p
	where	nr_seq_grupo	= nr_seq_grupo_p
	and	cd_cgc_externo	= cd_cgc_externo_ant_p;
	end;
end if;	

commit;

end atualizar_laboratorio_exame;
/