CREATE OR REPLACE
PROCEDURE Tasy_Excluir_Base_Sincro IS 

nm_tabela_w            		varchar2(50)      := null;
ds_comando_w			Varchar2(2000);
vl_retorno_w			varchar2(255);

CURSOR C010 IS
SELECT tname
from tab
where substr(tname,1,2) = 'S_';

BEGIN

OPEN C010;
LOOP
	FETCH C010 INTO 	nm_tabela_w;
	EXIT WHEN C010%NOTFOUND;
		begin
		ds_comando_w	:= 'drop table ' || nm_tabela_w;
		Obter_Valor_Dinamico(ds_comando_w, vl_retorno_w);
		end;
END LOOP; 
CLOSE C010;

END Tasy_Excluir_Base_Sincro;
/	
