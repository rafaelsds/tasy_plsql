create or replace
procedure qua_vincular_acao_inef_causa(
			nr_seq_causa_p		number,
			nr_seq_acao_p		number,
			ie_opcao_p		varchar2,
			nm_usuario_p		varchar2) is 

qt_acao_causa_w		number(10);

begin

if	(nr_seq_causa_p > 0) and
	(nr_seq_acao_p > 0) then 
	
	select	count(*)
	into	qt_acao_causa_w
	from	qua_nc_causa_ac_inef
	where	nr_seq_causa	= nr_seq_causa_p
	and	nr_seq_acao	= nr_seq_acao_p;
	
	if	(qt_acao_causa_w = 0) then
		insert into qua_nc_causa_ac_inef (
				nr_sequencia,
				nr_seq_causa,
				nr_seq_acao,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(
				qua_nc_causa_ac_inef_seq.nextval,
				nr_seq_causa_p,
				nr_seq_acao_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p);
				
		commit;
	end if;
end if;

end qua_vincular_acao_inef_causa;
/