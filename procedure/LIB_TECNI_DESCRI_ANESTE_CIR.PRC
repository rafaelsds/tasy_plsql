create or replace
procedure lib_tecni_descri_aneste_cir (	nr_sequencia_p		number,
					nm_tabela_p		varchar,
					nm_usuario_p		varchar) 
				is
				
nr_cirurgia_w	number(10);
nr_seq_pepo_w	number(10);

begin

if	(nvl(nr_sequencia_p,0) > 0) then
	select	nr_cirurgia,
		nr_seq_pepo
	into	nr_cirurgia_w,
		nr_seq_pepo_w
	from	cirurgia_tec_anestesica
	where	nr_sequencia = nr_sequencia_p;
end if;


if	(upper(nm_tabela_p) = 'CIRURGIA_TEC_ANESTESICA') then
	
		update 	cirurgia_tec_anestesica
		set	dt_liberacao	=	sysdate
		where	nr_sequencia	=	nr_sequencia_p	
		and	nm_usuario	=	nm_usuario_p
		and	dt_liberacao 		is null
		and	(((nr_cirurgia	= nr_cirurgia_w) and (nr_cirurgia_w is not null)) or ((nr_seq_pepo = nr_seq_pepo_w) and (nr_seq_pepo_w is not null)));
	
else
		update 	ANESTESIA_DESCRICAO
		set	dt_liberacao	=	sysdate
		where	nm_usuario	=	nm_usuario_p
		and	dt_liberacao 		is null
		and	(((nr_cirurgia	= nr_cirurgia_w) and (nr_cirurgia_w is not null)) or ((nr_seq_pepo = nr_seq_pepo_w) and (nr_seq_pepo_w is not null)));
	
end if;

commit;

end LIB_TECNI_DESCRI_ANESTE_CIR;
/