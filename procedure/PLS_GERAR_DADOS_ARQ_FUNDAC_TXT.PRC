create or replace
procedure pls_gerar_dados_arq_fundac_txt(	nr_seq_fatura_p		pls_fatura.nr_sequencia%type,
						nr_seq_lote_fat_p	pls_lote_faturamento.nr_sequencia%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
						nm_usuario_p		usuario.nm_usuario%type) is 
						
vl_tributo_w		pls_fatura_trib.vl_tributo%type;
nr_seq_lote_w		pls_lote_arq_fundacao_txt.nr_sequencia%type;
ie_fatura_taxa_w		pls_fatura.ie_fatura_taxa%type;

cursor c01	(nr_seq_fatura_pc		pls_fatura.nr_sequencia%type,
		nr_seq_lote_fat_pc		pls_lote_faturamento.nr_sequencia%type) is
	select	a.nr_sequencia,
		a.nr_seq_lote,
		nvl(nvl(a.nr_titulo,a.nr_titulo_ndc),a.nr_sequencia) nr_fatura,
		to_char(b.dt_mesano_referencia,'mm') dt_mes,
		to_char(b.dt_mesano_referencia,'yyyy') dt_ano,
		nvl(vl_total_ndc,0) + nvl(vl_fatura,0) vl_total,
		to_char(nvl(a.dt_emissao,b.dt_emissao),'ddmmyyyy') dt_emissao,
		nvl(a.ie_fatura_taxa,'N') ie_fatura_taxa
	from	pls_lote_faturamento	b,
		pls_fatura		a
	where	b.nr_sequencia	= a.nr_seq_lote
	and	a.nr_seq_lote	= nr_seq_lote_fat_pc
	and	a.ie_cancelamento is null
	and	nr_seq_lote_fat_pc is not null
	and	nvl(a.ie_impedimento_cobranca, 'F') <> 'NF'
	and	nvl(a.ie_fatura_taxa, 'N')	= 'N'
	union
	select	a.nr_sequencia,
		a.nr_seq_lote,
		nvl(nvl(a.nr_titulo,a.nr_titulo_ndc),a.nr_sequencia) nr_fatura,
		to_char(b.dt_mesano_referencia,'mm') dt_mes,
		to_char(b.dt_mesano_referencia,'yyyy') dt_ano,
		nvl(vl_total_ndc,0) + nvl(vl_fatura,0) vl_total,
		to_char(nvl(a.dt_emissao,b.dt_emissao),'ddmmyyyy') dt_emissao,
		nvl(a.ie_fatura_taxa,'N') ie_fatura_taxa
	from	pls_lote_faturamento	b,
		pls_fatura		a
	where	b.nr_sequencia	= a.nr_seq_lote
	and	a.nr_sequencia	= nr_seq_fatura_pc
	and	a.ie_cancelamento is null
	and	nr_seq_fatura_pc is not null
	and	nvl(a.ie_impedimento_cobranca, 'F') <> 'NF';

begin
pls_desfazer_dados_fundac_txt( nr_seq_fatura_p, nr_seq_lote_fat_p, cd_estabelecimento_p, nm_usuario_p);

for r_C01_w in C01( nr_seq_fatura_p , nr_seq_lote_fat_p ) loop

	ie_fatura_taxa_w	:= r_c01_w.ie_fatura_taxa;
		
	select	nvl(sum(nvl(vl_tributo,0)),0)
	into	vl_tributo_w
	from	pls_fatura_trib
	where	nr_seq_fatura	= r_c01_w.nr_sequencia;

	insert into pls_lote_arq_fundacao_txt
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_pls_fatura,
		cd_unimed,
		nr_contrato,
		cd_usuario,
		cd_dependente,
		nr_fatura,
		dt_mes_fatura,
		dt_ano_fatura,
		vl_total_servico,
		vl_retencao_irrf,
		nr_seq_lote_fat,
		dt_emissao_fatura)
	values	(pls_lote_arq_fundacao_txt_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		r_C01_w.nr_sequencia,
		'999',
		'9999',
		'999999',
		'99',
		r_C01_w.nr_fatura,
		r_C01_w.dt_mes,
		r_C01_w.dt_ano,
		elimina_caractere_especial(r_C01_w.vl_total),
		elimina_caractere_especial(vl_tributo_w),
		r_C01_w.nr_seq_lote,
		r_C01_w.dt_emissao) returning nr_sequencia into nr_seq_lote_w;
		
	pls_gerar_dados_arq_fund_item( nr_seq_lote_w, r_C01_w.nr_sequencia, cd_estabelecimento_p, nm_usuario_p);
end loop;

commit;

end pls_gerar_dados_arq_fundac_txt;
/