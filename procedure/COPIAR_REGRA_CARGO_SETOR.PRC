create or replace
procedure COPIAR_REGRA_CARGO_SETOR
			(cd_cargo_origem_p	in	number,
			cd_cargo_destino_p	in	number,
	 		nm_usuario_p		in	varchar2) is

cd_setor_atendimento_w	number(5);
nr_seq_regra_w		number(10);
nr_seq_regra_gerada_w	number(10);
cd_perfil_w		number(5);
cd_setor_atendimento_lib_w	number(5);

cursor c01 is
select	a.cd_setor_atendimento,
	a.nr_sequencia
from	regra_cargo_setor a
where	a.cd_cargo	= cd_cargo_origem_p;

cursor c02 is
select	a.cd_perfil
from	regra_cargo_perfil_lib a
where	a.nr_seq_regra_cargo	= nr_seq_regra_w;

cursor c03 is
select	a.cd_setor_atendimento
from	regra_cargo_setor_lib a
where	a.nr_seq_regra_cargo	= nr_seq_regra_w;

begin

open c01;
loop
fetch c01 into
	cd_setor_atendimento_w,
	nr_seq_regra_w;
exit when c01%notfound;

	select	regra_cargo_setor_seq.nextval
	into	nr_seq_regra_gerada_w
	from	dual;

	insert	into regra_cargo_setor
		(cd_cargo,
		cd_setor_atendimento,
		dt_atualizacao,
		nm_usuario,
		nr_sequencia)
	values	(cd_cargo_destino_p,
		cd_setor_atendimento_w,
		sysdate,
		nm_usuario_p,
		nr_seq_regra_gerada_w);

	open c02;
	loop
	fetch c02 into
		cd_perfil_w;
	exit when c02%notfound;

		insert	into regra_cargo_perfil_lib
			(cd_perfil,
			dt_atualizacao,
			nm_usuario,
			nr_seq_regra_cargo,
			nr_sequencia)
		values	(cd_perfil_w,
			sysdate,
			nm_usuario_p,
			nr_seq_regra_gerada_w,
			regra_cargo_perfil_lib_seq.nextval);

	end loop;
	close c02;

	open c03;
	loop
	fetch c03 into
		cd_setor_atendimento_lib_w;
	exit when c03%notfound;

		insert	into regra_cargo_setor_lib
			(cd_setor_atendimento,
			dt_atualizacao,
			nm_usuario,
			nr_seq_regra_cargo,
			nr_sequencia)
		values	(cd_setor_atendimento_lib_w,
			sysdate,
			nm_usuario_p,
			nr_seq_regra_gerada_w,
			regra_cargo_setor_lib_seq.nextval);

	end loop;
	close c03;

end loop;
close c01;

commit;

end COPIAR_REGRA_CARGO_SETOR;
/