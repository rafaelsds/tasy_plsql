create or replace
procedure Med_Marcar_Agenda_Tasy
		(nr_seq_agenda_cons_p		number,
		cd_agenda_p			number,
		cd_procedimento_p		number,
		ie_origem_proced_p		number,
		nm_usuario_p			varchar2) is

cd_pessoa_fisica_w		varchar2(10);
dt_agenda_w			date;
nr_minuto_duracao_w		number(10,0);
cd_medico_w			varchar2(10);
cd_convenio_w			number(05,0);
nr_sequencia_w			number(10,0);

begin

select	a.cd_pessoa_fisica,
	a.dt_agenda,
	a.nr_minuto_duracao,
	a.cd_convenio,
	b.cd_pessoa_fisica
into	cd_pessoa_fisica_w,
	dt_agenda_w,
	nr_minuto_duracao_w,
	cd_convenio_w,
	cd_medico_w
from	agenda b, 
	agenda_consulta a
where	a.nr_sequencia	= nr_seq_agenda_cons_p
and	a.cd_agenda	= b.cd_agenda;


select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	agenda_paciente
where	cd_pessoa_fisica	is not null
and	dt_agenda		= trunc(dt_agenda_w)
and	hr_inicio		= dt_agenda_w
and	cd_agenda		= cd_agenda_p;

if	(nr_sequencia_w > 0) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262569);
end if;

select	agenda_paciente_seq.nextval
into	nr_sequencia_w
from	dual;


insert into agenda_paciente
	(CD_AGENDA             ,
	CD_PESSOA_FISICA       ,
	DT_AGENDA              ,
	HR_INICIO              ,
	NR_MINUTO_DURACAO      ,
	NM_USUARIO             ,
	DT_ATUALIZACAO         ,
	CD_MEDICO              ,
	NM_PESSOA_CONTATO      ,
	CD_PROCEDIMENTO        ,
	DS_OBSERVACAO          ,
	CD_CONVENIO            ,
	NR_CIRURGIA            ,
	DS_CIRURGIA            ,
	QT_IDADE_PACIENTE      ,
	CD_TIPO_ANESTESIA      ,
	IE_ORIGEM_PROCED       ,
	IE_STATUS_AGENDA       ,
	NM_INSTRUMENTADOR      ,
	NM_CIRCULANTE          ,
	IE_ORTESE_PROTESE      ,
	IE_CDI                 ,
	IE_UTI                 ,
	IE_BANCO_SANGUE        ,
	IE_SERV_ESPECIAL       ,
	CD_MOTIVO_CANCELAMENTO,
	NR_SEQUENCIA           ,
	DS_SENHA               ,
	CD_TURNO               ,
	CD_ANESTESISTA         ,
	CD_PEDIATRA            ,
	NM_PACIENTE            ,
	IE_ANESTESIA           ,
	NR_ATENDIMENTO         ,
	IE_CARATER_CIRURGIA    ,
	CD_USUARIO_CONVENIO    ,
	NM_USUARIO_ORIG        ,
	QT_IDADE_MES           ,
	CD_PLANO               ,
	IE_LEITO               ,
	NR_TELEFONE            ,
	DT_AGENDAMENTO         ,
	IE_EQUIPAMENTO         ,
	IE_AUTORIZACAO         ,
	VL_PREVISTO            ,
	NR_SEQ_AGE_CONS        )
values	(cd_agenda_p,
	cd_pessoa_fisica_w,
	trunc(dt_agenda_w),
	dt_agenda_w,	
	nr_minuto_duracao_w,
	nm_usuario_p,
	sysdate,
	cd_medico_w,
	null,
	cd_procedimento_p,
	null,
	cd_convenio_w,
	null, null,
	null, null,
	ie_origem_proced_p,
	'N', null, null,
	'N', 'N', 'N', 'N', 'N',
	null, nr_sequencia_w,
	null, null, null, null,
	substr(obter_nome_pf(cd_pessoa_fisica_w),1,60), --Ivan em 10/03/2008 OS85558 
	null, null, null,
	null, null, null, null,
	'N', null, sysdate, 'N',
	'N', null, nr_seq_agenda_cons_p);

commit;

end Med_Marcar_Agenda_Tasy;
/
