create or replace 
procedure duv_gerar_segmento_ufb(nr_seq_mensagem_p in duv_mensagem.nr_sequencia%type,
				nm_usuario_p      in usuario.nm_usuario%type,
				nr_seq_episodio_p in episodio_paciente.nr_sequencia%type)is
                                                   
  
cursor 	c01 is
select	a.nr_seq_episodio,
	null ie_pode_trabalhar, 
	null dt_inicio_afastamento, 
	null dt_prev_retorno, 
	null ie_mais_3_meses, 
	substr(c.nm_empresa,1,200) ds_empresa, 
	null cd_pais_acidente, 
	null cd_cep_acidente, 
	substr(obter_desc_municipio_ibge(a.cd_municipio_ocorrencia),1,30) ds_cidade_acidente, 
	null ds_endereco_acidente, 
	substr(obter_desc_profissao(cd_profissao),1,30) ds_ocupacao, 
	null dt_admissao
from	atendimento_paciente a,
	compl_pessoa_fisica b,
	empresa_referencia c
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.ie_tipo_complemento	= '2'
and	b.cd_empresa_refer	= c.cd_empresa(+)
and	a.nr_seq_episodio	= nr_seq_episodio_p;

c01_w c01%rowtype;                                                   

begin 
c01_w := null;
open c01;
fetch c01 into c01_w;
close c01;
  
insert into duv_ufb 
	(nr_sequencia, 
	dt_atualizacao, 
	nm_usuario, 
	dt_atualizacao_nrec, 
	nm_usuario_nrec, 
	nr_seq_mensagem, 
	ie_pode_trabalhar, 
	dt_inicio_afastamento, 
	dt_prev_retorno, 
	ie_mais_3_meses, 
	ds_empresa, 
	cd_pais_acidente, 
	cd_cep_acidente, 
	ds_cidade_acidente, 
	ds_endereco_acidente, 
	ds_ocupacao, 
	dt_admissao) 
values 	(duv_ufb_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_mensagem_p,
	c01_w.ie_pode_trabalhar,
	c01_w.dt_inicio_afastamento,
	c01_w.dt_prev_retorno,
	c01_w.ie_mais_3_meses,
	c01_w.ds_empresa,
	c01_w.cd_pais_acidente,
	c01_w.cd_cep_acidente,
	c01_w.ds_cidade_acidente,
	c01_w.ds_endereco_acidente,
	c01_w.ds_ocupacao,
	c01_w.dt_admissao);
	
end duv_gerar_segmento_ufb;
/