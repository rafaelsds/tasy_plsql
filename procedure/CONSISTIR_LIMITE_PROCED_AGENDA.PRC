create or replace
procedure consistir_limite_proced_agenda (	cd_procedimento_p	number,
						cd_perfil_p		number,
						ie_origem_proced_p	number,
						nr_seq_proc_interno_p	number,
						cd_tipo_agenda_p	number,
						dt_agenda_p		date,
						cd_convenio_p		number default null,
						cd_medico_p		varchar default null,
						ie_acao_regra_p		out varchar2) 
						is

qt_regra_w		number(10,0);
cd_area_procedimento_w	number(15,0);
cd_especialidade_w	number(15,0);
cd_grupo_proc_w		number(15,0);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
cd_perfil_w		number(10,0);
dt_agenda_w		date;
ie_acao_regra_w		varchar2(1);
ie_periodo_w		varchar2(1);
qt_proced_w		number(10,0);
qt_limite_w		number(10,0);
dt_agenda_fim_w 	date;
cd_convenio_w		number(5);
cd_medico_w		varchar2(10);
ie_dia_semana_w		varchar2(2);


begin
dt_agenda_w 	:= dt_agenda_p;
ie_dia_semana_w	:= pkg_date_utils.get_WeekDay(dt_agenda_p);

if	(cd_procedimento_p is not null) and
	(ie_origem_proced_p is not null) then

	begin
	select	nvl(cd_area_procedimento,0),
		nvl(cd_especialidade,0),
		nvl(cd_grupo_proc,0)
	into	cd_area_procedimento_W,
		cd_especialidade_w,
		cd_grupo_proc_w
	from	estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;
	exception
		when others then
			cd_area_procedimento_w	:= 0;
			cd_especialidade_w	:= 0;
			cd_grupo_proc_w		:= 0;
	end;
		
	select	max(ie_periodo),
		max(qt_limite),      
		max(ie_acao_regra),
		max(nvl(cd_area_procedimento,0)),
		max(nvl(cd_especialidade,0)),
		max(nvl(cd_grupo_proc,0)),	
		max(nvl(cd_procedimento,0)),
		max(nvl(ie_origem_proced,0)),
		max(nvl(nr_seq_proc_interno,0)),
		max(nvl(cd_convenio,0)),
		max(nvl(cd_medico,'X'))
	into	ie_periodo_w,
		qt_limite_w,
		ie_acao_regra_w,
		cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		cd_convenio_w,
		cd_medico_w
	from	regra_limite_proced_agenda
	where	nvl(cd_area_procedimento, cd_area_procedimento_w)	= cd_area_procedimento_w
	and	nvl(cd_especialidade, cd_especialidade_w)		= cd_especialidade_w
	and	nvl(cd_grupo_proc, cd_grupo_proc_w)			= cd_grupo_proc_w
	and	nvl(cd_procedimento, cd_procedimento_p)			= cd_procedimento_p
	and	nvl(ie_origem_proced, ie_origem_proced_p)		= ie_origem_proced_p
	and	nvl(nr_seq_proc_interno, nvl(nr_seq_proc_interno_p,0))	= nvl(nr_seq_proc_interno_p,0)
	and	nvl(cd_convenio, nvl(cd_convenio_p,0))			= nvl(cd_convenio_p,0)
	and	nvl(cd_medico, nvl(cd_medico_p,'X'))			= nvl(cd_medico_p,'X')
	and	nvl(cd_perfil, cd_perfil_p)				= cd_perfil_p
	and	((nvl(ie_dia_semana,ie_dia_semana_w) = ie_dia_semana_w) or ((nvl(ie_dia_semana,ie_dia_semana_w) = 9) and (ie_dia_semana_w not in (7,1))));

	if	(cd_tipo_agenda_p in (1,2)) then
		if	(cd_area_procedimento_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_paciente a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	obter_tipo_agenda(a.cd_agenda) 				= cd_tipo_agenda_p
			and	nvl(b.cd_area_procedimento, cd_area_procedimento_w)	= cd_area_procedimento_w
			and	((nvl(a.cd_medico,cd_medico_w)				= cd_medico_w) or (cd_medico_w = 'X'))
			and	((nvl(a.cd_convenio,cd_convenio_w)			= cd_convenio_w) or (cd_convenio_w = 0));
		elsif	(cd_especialidade_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_paciente a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	obter_tipo_agenda(a.cd_agenda) 				= cd_tipo_agenda_p
			and	nvl(b.cd_especialidade, cd_especialidade_w)		= cd_especialidade_w
			and	((nvl(a.cd_medico,cd_medico_w)				= cd_medico_w) or (cd_medico_w = 'X'))
			and	((nvl(a.cd_convenio,cd_convenio_w)			= cd_convenio_w) or (cd_convenio_w = 0));
		elsif	(cd_grupo_proc_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_paciente a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	obter_tipo_agenda(a.cd_agenda) 				= cd_tipo_agenda_p
			and	nvl(b.cd_grupo_proc, cd_grupo_proc_w)			= cd_grupo_proc_w
			and	((nvl(a.cd_medico,cd_medico_w)				= cd_medico_w) or (cd_medico_w = 'X'))
			and	((nvl(a.cd_convenio,cd_convenio_w)			= cd_convenio_w) or (cd_convenio_w = 0));
		elsif	(cd_procedimento_w > 0) and
			(ie_origem_proced_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_paciente a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	nvl(a.cd_procedimento, cd_procedimento_w)		= cd_procedimento_w
			and	obter_tipo_agenda(a.cd_agenda) 				= cd_tipo_agenda_p
			and	nvl(a.ie_origem_proced, ie_origem_proced_w)		= ie_origem_proced_w
			and	((nvl(a.cd_medico,cd_medico_w)				= cd_medico_w) or (cd_medico_w = 'X'))
			and	((nvl(a.cd_convenio,cd_convenio_w)			= cd_convenio_w) or (cd_convenio_w = 0));
		elsif	(nr_seq_proc_interno_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_paciente a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	obter_tipo_agenda(a.cd_agenda) 				= cd_tipo_agenda_p
			and	nvl(a.nr_seq_proc_interno, nr_seq_proc_interno_w)	= nr_seq_proc_interno_w
			and	((nvl(a.cd_medico,cd_medico_w)				= cd_medico_w) or (cd_medico_w = 'X'))
			and	((nvl(a.cd_convenio,cd_convenio_w)			= cd_convenio_w) or (cd_convenio_w = 0));
		end if;		

	elsif	(cd_tipo_agenda_p in (3,4,5)) then
		if	(cd_area_procedimento_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_consulta a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	nvl(b.cd_area_procedimento, cd_area_procedimento_w)	= cd_area_procedimento_w;
		
		elsif	(cd_especialidade_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_consulta a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	nvl(b.cd_especialidade, cd_especialidade_w)		= cd_especialidade_w;

		elsif	(cd_grupo_proc_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_consulta a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	nvl(b.cd_grupo_proc, cd_grupo_proc_w)			= cd_grupo_proc_w;
				
		elsif	(cd_procedimento_w > 0) and
			(ie_origem_proced_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_consulta a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	nvl(a.cd_procedimento, cd_procedimento_w)		= cd_procedimento_w
			and	nvl(a.ie_origem_proced, ie_origem_proced_w)		= ie_origem_proced_w;

		elsif	(nr_seq_proc_interno_w > 0) then
			select	count(*)
			into	qt_proced_w
			from	agenda_consulta a,
				estrutura_procedimento_v b
			where	trunc(a.dt_agenda,'dd') between obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'I') and obter_limite_regra_proc_agenda(dt_agenda_w, ie_periodo_w, 'F')
			and	a.cd_procedimento					= b.cd_procedimento
			and	a.ie_origem_proced					= b.ie_origem_proced
			and	nvl(a.nr_seq_proc_interno, nr_seq_proc_interno_w)	= nr_seq_proc_interno_w;
			
		end if;		
	end if;
	if	(qt_proced_w >= qt_limite_w) then
		ie_acao_regra_p := ie_acao_regra_w;
	end if;

end if;

end consistir_limite_proced_agenda;
/
