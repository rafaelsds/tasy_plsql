create or replace
procedure sus_gerar_conta_unica(	nr_atendimento_ant_p	number,
				nr_interno_conta_p		number,
				nm_usuario_p		varchar2) as

nr_aih_w				number(13);
nr_interno_conta_ant_w		number(10);
nr_atendimento_w			number(10);
dt_periodo_final_w			date;
dt_periodo_inicial_w		date;
ds_erro_w			varchar2(255);
nr_sequencia_w			number(10);
nr_seq_protocolo_w		number(10);
qt_protocolo_def_w			number(10) := 0;
ie_status_acerto_w			number(10);
ie_tipo_atendimento_w		number(3);
				
begin

begin
select	max(a.nr_aih),
	max(a.nr_sequencia),
	max(nvl(c.dt_alta,b.dt_periodo_final)),
	max(b.nr_atendimento),
	max(ie_tipo_atendimento)
into	nr_aih_w,
	nr_sequencia_w,
	dt_periodo_final_w,
	nr_atendimento_w,
	ie_tipo_atendimento_w
from	atendimento_paciente c,
	sus_aih_unif a,
	conta_paciente b
where	b.nr_interno_conta = nr_interno_conta_p
and	b.nr_interno_conta = a.nr_interno_conta(+)
and	b.nr_atendimento = c.nr_atendimento;
exception
	when others then
	nr_aih_w	:= 0;
	nr_sequencia_w	:= 0;
	
	select	max(nvl(c.dt_alta,b.dt_periodo_final)),
		max(b.nr_atendimento),
		max(ie_tipo_atendimento)
	into	dt_periodo_final_w,
		nr_atendimento_w,
		ie_tipo_atendimento_w
	from	conta_paciente b,
		atendimento_paciente c
	where	b.nr_interno_conta = nr_interno_conta_p
	and	b.nr_atendimento = c.nr_atendimento;
end;

select	max(dt_entrada)
into	dt_periodo_inicial_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_ant_p;

if	(nvl(nr_aih_w,0) <> 0) then
	begin
	
	select	nvl(max(nr_interno_conta),0)
	into	nr_interno_conta_ant_w
	from	sus_aih_unif
	where	nr_aih		= nr_aih_w
	and	nr_atendimento 	= nr_atendimento_ant_p;
	
	end;
else
	begin
	
	select	nvl(max(nr_interno_conta),0)
	into	nr_interno_conta_ant_w
	from	conta_paciente 
	where	nr_atendimento 	= nr_atendimento_ant_p
	and	ie_cancelamento is null;

	end;
end if;

if	(nvl(nr_interno_conta_ant_w,0) > 0) then
	begin
	
	select	nvl(max(nr_seq_protocolo),0),
		nvl(max(ie_status_acerto),1)	
	into	nr_seq_protocolo_w,
		ie_status_acerto_w
	from	conta_paciente 
	where	nr_interno_conta = nr_interno_conta_ant_w;
	
	if	(nr_seq_protocolo_w <> 0) then
		begin
	
		select	count(*)
		into	qt_protocolo_def_w
		from	protocolo_convenio
		where	nr_seq_protocolo = nr_seq_protocolo_w
		and	ie_status_protocolo <> 1;
		
		if	(qt_protocolo_def_w = 0) then
			begin
			
			begin
			update	conta_paciente
			set	nr_seq_protocolo 	= null,
				nr_protocolo		= '0',
				ie_status_acerto	= 1,
				dt_periodo_final	= dt_periodo_final_w,
				ds_observacao 		= substr(ds_observacao|| WHEB_MENSAGEM_PCK.get_texto(299784) ,1,255),
				nm_usuario		= nm_usuario_p,
				dt_atualizacao 		= sysdate
			where	nr_interno_conta = nr_interno_conta_ant_w;
			exception
			when others then
				wheb_mensagem_pck.exibir_mensagem_abort(186158,'NR_INTERNO_CONTA='||nr_interno_conta_ant_w);
				/* Ocorreram problemas ao tentar retirar a conta #@NR_INTERNO_CONTA_P#@ do protocolo. */	
			end;
			
			end;
		else
			wheb_mensagem_pck.exibir_mensagem_abort(186161,'NR_CONTA_ORG='||nr_interno_conta_p||
									';NR_CONTA_DES='||nr_interno_conta_ant_w);
			/* N�o ser� poss�vel transferir os gastos da conta #@NR_CONTA_ORG#@ para a conta #@NR_CONTA_DES#@, 
			pois esta conta est� inserida em um protocolo com status diferente de provis�rio. */	
		end if;
		
		end;
	elsif	(ie_status_acerto_w = 2) then
		begin
		
		update	conta_paciente
		set	ie_status_acerto	= 1,
			dt_periodo_final 	= dt_periodo_final_w,
			ds_observacao 		= substr(ds_observacao|| WHEB_MENSAGEM_PCK.get_texto(299784),1,255),
			nm_usuario		= nm_usuario_p,
			dt_atualizacao 		= sysdate
		where	nr_interno_conta = nr_interno_conta_ant_w;
		end;
	else
		begin
		
		begin
		update	conta_paciente
		set	dt_periodo_final 	= dt_periodo_final_w,
			ds_observacao 	= substr(ds_observacao|| WHEB_MENSAGEM_PCK.get_texto(299784),1,255),
			nm_usuario	= nm_usuario_p,
			dt_atualizacao 	= sysdate
		where	nr_interno_conta	= nr_interno_conta_ant_w;
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(186113);
			/* Ocorreram problemas ao alterar a data da conta destino. */	
		end;
		
		end;
	end if;	

	sus_transferir_conta_pac_aih(nr_interno_conta_p,nr_interno_conta_ant_w,nr_atendimento_ant_p,nm_usuario_p);

	if	(ie_tipo_atendimento_w = 1) then
		begin
		
		begin
		update	sus_aih_unif
		set	dt_final		= dt_periodo_final_w,
			dt_inicial		= dt_periodo_inicial_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao 		= sysdate
		where	nr_interno_conta 	= nr_interno_conta_ant_w;
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(186118);
			/* Ocorrerem problemas ao alterar a data final da AIH do primeiro atendimento. */	
		end;
	
		begin
		delete	from sus_aih_unif
		where	nr_interno_conta 	= nr_interno_conta_p
		and	nr_atendimento		= nr_atendimento_w
		and	nr_aih			= nr_aih_w
		and	nr_sequencia		= nr_sequencia_w;
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(186120);
			/* Ocorreram problemas ao deletar a AIH do segundo atendimento. */	
		end;
		
		update	conta_paciente
		set 	ds_observacao = substr(ds_observacao|| WHEB_MENSAGEM_PCK.get_texto(299784),1,255)
		where 	nr_interno_conta = nr_interno_conta_p;

		sus_gerar_longa_permanencia(nr_atendimento_ant_p,nr_interno_conta_ant_w,nm_usuario_p);

		end;
	end if;

	fechar_conta_paciente(nr_interno_conta_p,nr_atendimento_w,2,nm_usuario_p,ds_erro_w);

	commit;
	
	end;
else
	commit;
	wheb_mensagem_pck.exibir_mensagem_abort(186170,'NR_ATENDIMENTO='||nr_atendimento_ant_p);
	/* N�o foi encontrada uma conta dispon�vel no atendimento #@NR_ATENDIMENTO_P#@, 
	por isso a transfer�ncia n�o ser� efetuada. */
end if;

end sus_gerar_conta_unica;
/
