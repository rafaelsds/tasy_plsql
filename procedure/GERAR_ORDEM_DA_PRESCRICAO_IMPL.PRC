create or replace
PROCEDURE GERAR_ORDEM_DA_PRESCRICAO_IMPL(
					dt_referencia_p			Date,
					cd_estabelecimento_p		Number,
					ie_gerar_ordem_pac_chegada_p	varchar2,
					nm_usuario_p			Varchar2,
					nr_prescricao_p			varchar2,
                    nr_sequencia_log_p      number,
					ds_erro_p	out varchar2) IS

nr_seq_atendimento_w		Number(10);
nr_seq_paciente_w		Number(10);
cd_medico_resp_w		Varchar2(10);
cd_pessoa_fisica_w		Varchar2(10);
nr_prescricao_w			Number(14);
qt_superf_corporal_w		Number(15,4);
dt_real_w			Date;
dt_prevista_w			Date;
nr_sequencia_w			Number(10);
nr_ordem_prod_w 		Number(10);
cd_estabelecimento_ww		Number(4);
cd_material_w			Number(6);
nr_seq_prescr_w			Number(10);
ie_via_aplicacao_w		Varchar2(5);
cd_farmaceutico_w		Varchar2(10);
ds_precaucao_ordem_w		Varchar2(255);
ds_recomendacao_w		Varchar2(2000); --Ivan em 17/07/2007 OS61569
cd_setor_atendimento_w		Number(5);
qt_min_aplicacao_w		Number(4,0);
qt_hora_aplicacao_w		Number(3,0);
ie_tipo_dosagem_w		Varchar2(3);
qt_dose_diluente_w		Number(15,3);
vl_gotejo_w			Number(15,3);
nr_agrupamento_w		Number(07,1);
nr_agrup_ant_w			Number(07,1) := 0;
cd_material_medic_w		Number(6);
nr_sequencia_medic_w		Number(6);
qt_dose_medic_w			Number(15,4);
cd_unidade_medida_dose_w	Varchar2(30);
nr_seq_item_prescr_w		Number(10);
nr_ocorrencia_w			Number(15,4);
cd_forma_armaz_w		Varchar2(255);
cd_estagio_armaz_w		Varchar2(255);
qt_estabilidade_w		Number(14,2);
ie_tempo_estab_w		Varchar2(3);
qt_hora_validade_w		Number(5,0);
qt_min_validade_w		Number(5);
qt_dias_util_w			Number(3);
ie_exige_liberacao_w		Varchar2(1);
ie_generico_w			varchar2(1);
ie_gerar_reconstituinte_w	varchar2(1);
qt_peso_w			number(6,3);
qt_superf_corporea_w		number(7,3);
ie_libera_enfer_w		varchar2(1);
nr_seq_ordem_prod_w		number(10);
ie_usuario_manipulador_w	varchar2(1);
ie_restringir_ordem_w		varchar2(1);
ie_pre_medicacao_w		varchar2(1);
ie_agrupador_w			number(4);
ie_agrup_ant_w			number(4);
ie_grava_aspirado_w		varchar2(1);
ie_libera_medic_w		varchar2(1);
qt_volume_aspirado_w		number(15,2);
ie_exibe_apto_w			varchar2(1);
ie_estabe_tratamento_w		varchar2(1);
ie_bomba_infusao_w			varchar2(1);
nr_sequencia_solucao_w		number(10);
cd_perfil_w                 number(5);
ie_consiste_atend_w         varchar2(1);
nr_atendimento_w            number(10);
nr_prescricao_ww            number(10);
dt_chagada_w				date;
ie_exige_lib_farma_w		varchar2(1);
qt_mat_regra_w				number(10);
ds_entrada_w		varchar2(4000);
ds_log_w		varchar2(4000);
ds_erro_log_w		varchar2(4000);
ds_erro_w			varchar2(2000);
ie_tipo_atendimento_w	number(3);
nr_atendimento_ww		number(10);
ie_via_aplicacao_got_w	varchar2(5);
ie_aplica_gotejamento_w	varchar2(1);
nr_seq_mat_hor_w        number(10);

Cursor C01 is
	select	nr_seq_atendimento,
		nr_seq_paciente,
		nr_prescricao,
		qt_superf_corporal,
		nvl(dt_real,dt_prevista),
		ie_exige_liberacao,
		cd_estabelecimento,
		dt_chegada,
		nr_atendimento
	from	paciente_atendimento
	where	trunc(nvl(dt_real,dt_prevista)) = trunc(dt_referencia_p)
	AND 	((cd_estabelecimento = cd_estabelecimento_p) OR (ie_restringir_ordem_w = 'N'))
	and	((dt_chegada is not null) or (ie_gerar_ordem_pac_chegada_p = 'N'))
	and 	((dt_apto is not null) or (nvl(ie_exibe_apto_w,'N') = 'N'))
	and	dt_cancelamento is null
	and	nr_prescricao = nr_prescricao_p;

Cursor C02 is
	select	decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
		a.nr_sequencia,
		a.ie_via_aplicacao,
		substr(b.ds_precaucao_ordem,1,255),
		substr(a.ds_observacao,1,255), --Ivan em 17/07/2007 OS61569
		nvl(a.qt_min_aplicacao,0),
		nvl(a.qt_hora_aplicacao,0),
		decode(obter_um_rep_dispositivo(a.ie_bomba_infusao,a.ie_agrupador),null,decode(a.ie_bomba_infusao,'S','mlh','gtm'),obter_um_rep_dispositivo(a.ie_bomba_infusao,a.ie_agrupador)),
		a.nr_agrupamento,
		nvl(a.nr_ocorrencia,1),
		a.qt_dias_util,
		a.ie_pre_medicacao,
		a.ie_agrupador,
		a.ie_bomba_infusao,
		a.nr_sequencia_solucao,
        a.nr_prescricao
	from	material b,
		prescr_material a,
		prescr_medica c
	where	a.nr_prescricao = nr_prescricao_w
	and	a.nr_prescricao = c.nr_prescricao
	and	a.cd_material	= b.cd_material
	and	a.nr_seq_ordem_prod is null
	and	a.ie_via_aplicacao is not null

	and	nvl(a.ie_suspenso,'N')	= 'N'
	and 	a.dt_suspensao is null
	and	(((b.ie_mistura	= 'S') and (nvl(a.ie_medicacao_paciente, 'N') <> 'S')) or


		 ((b.ie_mistura	= 'R') and (obter_se_gera_ordem(a.ie_via_aplicacao,a.cd_intervalo, a.qt_dose, a.cd_material, a.cd_unidade_medida,'P', nvl(a.ie_medicacao_paciente,'N'),nr_atendimento_ww) = 'S')) or
		 ((b.ie_mistura	= 'I') and (ie_tipo_atendimento_w = 1) and (nvl(a.ie_medicacao_paciente, 'N') <> 'S')))
	and 	a.ie_agrupador in (1,2,4)
		and 	(((qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'S') and (dt_chagada_w is not null))
			or (qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'N'))
	and	  ((c.dt_liberacao_farmacia is not null) or (nvl(ie_exige_lib_farma_w,'N') = 	'N'))
	order by a.nr_agrupamento, a.nr_sequencia;

Cursor C03 is
	select	decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),decode(ie_generico_w,'C',obter_medic_regra_onc(a.cd_material,b.cd_material_generico,nr_atendimento_w,ie_tipo_material,b.nr_seq_ficha_tecnica),a.cd_material)),
		a.nr_sequencia,
		a.qt_dose,
		a.cd_unidade_medida_dose
	from	material b,
		prescr_material a
	where	a.nr_prescricao		= nr_prescricao_w
	and	a.nr_agrupamento	= nr_agrupamento_w
	and 	a.ie_agrupador 		= ie_agrupador_w
	and	a.cd_material		= b.cd_material
	and 	a.dt_suspensao is null
	and	a.ie_agrupador		in (1,2,4)
	and	a.cd_unidade_medida_dose is not null;
BEGIN

ds_erro_p := '';
ds_entrada_w := '1';
cd_perfil_w :=  obter_perfil_ativo;

Obter_Param_Usuario(3130,2,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,cd_farmaceutico_w);
Obter_Param_Usuario(3130,22,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_generico_w);
Obter_Param_Usuario(3130,40,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_gerar_reconstituinte_w);
Obter_Param_Usuario(3130,50,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_libera_enfer_w);
Obter_Param_Usuario(3130,142,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_usuario_manipulador_w);
Obter_Param_Usuario(3130,165,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_restringir_ordem_w);
Obter_Param_Usuario(3130,176,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_grava_aspirado_w);
Obter_Param_Usuario(3130,203,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_libera_medic_w);
Obter_Param_Usuario(3130,204,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_exibe_apto_w);
Obter_Param_Usuario(3130,331,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_estabe_tratamento_w);
Obter_Param_Usuario(3130,421,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_consiste_atend_w);
Obter_Param_Usuario(3130,429,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_exige_lib_farma_w);

begin

	/*select	nvl(vl_parametro, vl_parametro_padrao)
	into	cd_farmaceutico_w
	from	funcao_parametro
	where	cd_funcao	= 3130
	and	nr_sequencia	= 2;*/

	select	nvl(vl_parametro, vl_parametro_padrao)
	into	cd_estagio_armaz_w
	from	funcao_parametro
	where	cd_funcao	= 3130
	and	nr_sequencia	= 10;

	select	nvl(vl_parametro, vl_parametro_padrao)
	into	cd_forma_armaz_w
	from	funcao_parametro
	where	cd_funcao	= 3130
	and	nr_sequencia	= 11;
	ds_entrada_w := substr(ds_entrada_w || ',2',1,4000);

	OPEN C01;
	LOOP
	FETCH C01 into
		nr_seq_atendimento_w,
		nr_seq_paciente_w,
		nr_prescricao_w,
		qt_superf_corporal_w,
		dt_prevista_w,
		ie_exige_liberacao_w,
		cd_estabelecimento_ww,
		dt_chagada_w,
		nr_atendimento_ww;
	exit when C01%notfound;
		begin
		ds_entrada_w := substr(ds_entrada_w || ',3',1,4000);
		select	max(cd_pessoa_fisica),
			max(cd_medico_resp),
			max(cd_setor_atendimento),
			max(qt_peso),
			max(round(obter_superficie_corp_red_ped(qt_peso, qt_altura, qt_redutor_sc, cd_pessoa_fisica, nm_usuario),2))
		into	cd_pessoa_fisica_w,
			cd_medico_resp_w,
			cd_setor_atendimento_w,
			qt_peso_w,
			qt_superf_corporea_w
		from	paciente_setor
		where	nr_seq_paciente	= nr_seq_paciente_w;

		update	prescr_medica
		set	dt_liberacao		= decode(ie_libera_enfer_w,'S',sysdate,null),
			dt_liberacao_medico	= decode(ie_libera_medic_w,'S',sysdate),
			cd_setor_atendimento	= cd_setor_atendimento_w
		where	nr_prescricao		= nr_prescricao_w
		and	dt_liberacao is null
		and	dt_liberacao_medico is null;

		nr_agrup_ant_w	:= 0;
		ie_agrup_ant_w	:= 0;
		ds_entrada_w := substr(ds_entrada_w || ',4',1,4000);

		select	max(ie_tipo_atendimento)
		into	ie_tipo_atendimento_w
		from	atendimento_paciente
		where	nr_atendimento	=	nr_atendimento_ww;

		OPEN C02;
		LOOP
		FETCH C02 into
			cd_material_w,
			nr_seq_prescr_w,
			ie_via_aplicacao_w,
			ds_precaucao_ordem_w,
			ds_recomendacao_w,
			qt_min_aplicacao_w,
			qt_hora_aplicacao_w,
			ie_tipo_dosagem_w,
			nr_agrupamento_w,
			nr_ocorrencia_w,
			qt_dias_util_w,
			ie_pre_medicacao_w,
			ie_agrupador_w,
			ie_bomba_infusao_w,
			nr_sequencia_solucao_w,
            nr_prescricao_ww;
		exit when C02%notfound;
			begin
			ds_entrada_w := substr(ds_entrada_w || ',5',1,4000);
			commit;
            if (ie_consiste_atend_w = 'S') then
                select  max(nr_atendimento)
                into    nr_atendimento_w
                from    prescr_medica
                where   nr_prescricao   =   nr_prescricao_ww;

                if (nvl(nr_atendimento_w,0) = 0) then
                ds_erro_p := wheb_mensagem_pck.get_texto(237879);
                    	wheb_mensagem_pck.Exibir_Mensagem_Abort(237879);
                end if;
            end if;
			if	(nr_agrup_ant_w <> nr_agrupamento_w) or
				((nr_agrup_ant_w = nr_agrupamento_w) and
				 (ie_agrup_ant_w <> ie_agrupador_w)) then
				begin
				ds_entrada_w := substr(ds_entrada_w || ',6',1,4000);
				select	nvl(min(qt_estabilidade),0),
					nvl(min(ie_tempo_estab),'I')
				into	qt_estabilidade_w,
					ie_tempo_estab_w
				from	material_armazenamento
				where	cd_material	= cd_material_w
				and	nr_seq_estagio	= cd_estagio_armaz_w
				and	nr_seq_forma	= cd_forma_armaz_w
				and	nvl(cd_estabelecimento,nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0);



				qt_hora_validade_w	:= 0;
				qt_min_validade_w	:= 0;
				if	(ie_tempo_estab_w = 'H') then
					qt_hora_validade_w	:= qt_estabilidade_w;
				elsif	(ie_tempo_estab_w = 'D') then
					qt_hora_validade_w	:= qt_estabilidade_w * 24;
				elsif	(ie_tempo_estab_w = 'M') then
					qt_hora_validade_w	:= qt_estabilidade_w * 30 * 24;
				elsif	(ie_tempo_estab_w = 'A') then
					qt_hora_validade_w	:= qt_estabilidade_w * 365 * 24;
				end if;

				nr_agrup_ant_w	:= nr_agrupamento_w;
				ie_agrup_ant_w  := ie_agrupador_w;

				select	nvl(max(nr_seq_ordem_prod),0)
				into	nr_seq_ordem_prod_w
				from	prescr_material
				where	nr_prescricao	= nr_prescricao_w
				and	nr_sequencia	= nr_seq_prescr_w;

				/*
				insert into logx_tasy(dt_atualizacao, nm_usuario, cd_log, ds_log)
				values (sysdate, nm_usuario_p, 54784, nr_ocorrencia_w || chr(13) || nr_prescricao_w || ' - ' || nr_seq_prescr_w||' - '||'Ordem Ambulatorial');
				*/
				ds_entrada_w := substr(ds_entrada_w || ',7',1,4000);
				while	(nr_ocorrencia_w > 0) and
					(nr_seq_ordem_prod_w = 0) LOOP
					ds_entrada_w := substr(ds_entrada_w || ',8',1,4000);
					begin

					nr_ocorrencia_w	:= nr_ocorrencia_w - 1;

					select	can_ordem_prod_seq.nextval
					into	nr_sequencia_w
					from	dual;

					nr_ordem_prod_w := nr_sequencia_w;

					if (ie_agrupador_w = 4) then
						ds_entrada_w := substr(ds_entrada_w || ',9',1,4000);
						select 	nvl(max(ie_via_aplicacao),ie_via_aplicacao_w)
						into	ie_via_aplicacao_w
						from	prescr_solucao
						where	nr_prescricao = nr_prescricao_w
						and		nr_seq_solucao = nr_sequencia_solucao_w;
					end if;
					ds_entrada_w := substr(ds_entrada_w || ',10',1,4000);
					insert into can_ordem_prod(
						nr_sequencia,
						cd_estabelecimento,
						dt_atualizacao,
						nm_usuario,
						cd_pessoa_fisica,
						dt_prevista,
						ie_via_aplicacao,
						qt_hora_validade,
						qt_min_validade,
						qt_hora_aplic,
						qt_min_aplic,
						cd_medico_resp,
						cd_farmaceutico,
						cd_manipulador,
						cd_auxiliar,
						dt_inicio_preparo,
						dt_entrega_setor,
						dt_administracao,
						nr_seq_atendimento,
						nr_prescricao,
						dt_fim_preparo,
						ie_tipo_dosagem,
						ds_precaucao,
						ie_cancelada,
						ds_orientacao_adm,
						cd_setor_paciente,
						ie_conferido,
						qt_dias_util,
						ie_suspensa,
						qt_peso,
						qt_superf_corporea,
						dt_geracao_ordem,
						ie_pre_medicacao,
						ie_bomba_infusao)
					Values(
						nr_sequencia_w,
						decode(nvl(ie_estabe_tratamento_w,'N'),'N',cd_estabelecimento_p,cd_estabelecimento_ww),
						sysdate,
						nm_usuario_p,
						cd_pessoa_fisica_w,
						dt_prevista_w,
						ie_via_aplicacao_w,
						nvl(qt_hora_validade_w,0),
						nvl(qt_min_validade_w,0),
						nvl(qt_hora_aplicacao_w,0),
						nvl(qt_min_aplicacao_w,0),
						cd_medico_resp_w,
						cd_farmaceutico_w,
						decode(ie_usuario_manipulador_w,'S',Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C'),null),
						null,
						null,
						null,
						null,
						nr_seq_atendimento_w,
						nr_prescricao_w,
						null,
						ie_tipo_dosagem_w,
						ds_precaucao_ordem_w,
						'N',
						ds_recomendacao_w,
						cd_setor_atendimento_w,
						'N',
						qt_dias_util_w,
						ie_exige_liberacao_w,
						qt_peso_w,
						qt_superf_corporea_w,
						sysdate,
						ie_pre_medicacao_w,
						ie_bomba_infusao_w);

					commit;

					OPEN C03;
					LOOP
					FETCH C03 into
						cd_material_medic_w,
						nr_sequencia_medic_w,
						qt_dose_medic_w,
						cd_unidade_medida_dose_w;
					exit when C03%notfound;
						begin
						ds_entrada_w := substr(ds_entrada_w || ',11',1,4000);
						select	can_ordem_item_prescr_seq.nextval
						into	nr_seq_item_prescr_w
						from	dual;

						select	count(*)
						into	qt_mat_regra_w
						from	regra_geracao_ordem_tot
						where	cd_material = cd_material_medic_w;

						if	(qt_mat_regra_w > 0) then
							ds_entrada_w := substr(ds_entrada_w || ',12',1,4000);
							select	qt_material
							into	qt_dose_medic_w
							from	prescr_material
							where	nr_sequencia	= nr_sequencia_medic_w
							and		nr_prescricao	= nr_prescricao_w;
						end if;
						ds_entrada_w := substr(ds_entrada_w || ',13',1,4000);

                        select min  (a.nr_sequencia)
                        into    nr_seq_mat_hor_w
                        from    prescr_mat_hor a
                        where   a.nr_seq_material = nr_sequencia_medic_w
                        and     a.nr_prescricao = nr_prescricao_w
                        and     a.cd_material = cd_material_medic_w
                        and     not exists (select 1 from can_ordem_item_prescr b
                        where   b.nr_seq_mat_hor = a.nr_sequencia);

                        insert into can_ordem_item_prescr(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							nr_seq_ordem,
							cd_material,
							qt_dose,
							cd_unidade_medida,
							nr_prescricao,
							nr_seq_prescricao,
							nr_sequencia_diluente,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
                            						nr_seq_mat_hor)
						values(	nr_seq_item_prescr_w,
							sysdate,
							nm_usuario_p,
							nr_sequencia_w,
							cd_material_medic_w,
							nvl(qt_dose_medic_w,0),
							cd_unidade_medida_dose_w,
							nr_prescricao_w,
							nr_sequencia_medic_w,
							null,
							nm_usuario_p,
							sysdate,
                            						nr_seq_mat_hor_w);

                        ds_entrada_w := substr(ds_entrada_w || ',14',1,4000);


                        insert into can_ordem_item_prescr(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							nr_seq_ordem,
							cd_material,
							qt_dose,
							cd_unidade_medida,
							nr_prescricao,
							nr_seq_prescricao,
							nr_sequencia_diluente,
							ie_agrupador,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
                            						nr_seq_mat_hor)
						select	can_ordem_item_prescr_seq.nextval,
							sysdate,
							nm_usuario_p,
							nr_sequencia_w,
							decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
							nvl(a.qt_dose,0),
							a.cd_unidade_medida_dose,
							nr_prescricao_w,
							a.nr_sequencia,
							nr_seq_item_prescr_w,
							nvl(ie_agrupador_quimio,ie_agrupador),
							nm_usuario_p,
							sysdate,
                            						nr_seq_mat_hor_w
						from	material b,
							prescr_material a
						where	a.nr_prescricao		= nr_prescricao_w
						and	a.nr_sequencia_diluicao	= nr_sequencia_medic_w
						and	a.cd_material		= b.cd_material
						and	a.ie_agrupador		in (3)
						and 	a.dt_suspensao 		is null
						and	a.cd_unidade_medida_dose is not null;
						ds_entrada_w := substr(ds_entrada_w || ',15',1,4000);
						if 	(ie_gerar_reconstituinte_w = 'S') then
							ds_entrada_w := substr(ds_entrada_w || ',16',1,4000);

                            select min  (a.nr_sequencia)
                            into nr_seq_mat_hor_w
                            from    prescr_mat_hor a
                            where   a.nr_seq_material = nr_sequencia_medic_w
                            and     a.nr_prescricao = nr_prescricao_w
                            and   a.cd_material = cd_material_medic_w
                            and     not exists (select 1 from can_ordem_item_prescr b
                            where   b.nr_seq_mat_hor = a.nr_sequencia);

                            insert into can_ordem_item_prescr(
								nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_ordem,
								cd_material,
								qt_dose,
								cd_unidade_medida,
								nr_prescricao,
								nr_seq_prescricao,
								nr_sequencia_diluente,
								ie_agrupador,
								nm_usuario_nrec,
								dt_atualizacao_nrec,
                                						nr_seq_mat_hor)
								select	can_ordem_item_prescr_seq.nextval,
								sysdate,
								nm_usuario_p,
								nr_sequencia_w,
								decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
								nvl(a.qt_dose,0),
								a.cd_unidade_medida_dose,
								nr_prescricao_w,
								a.nr_sequencia,
								nr_seq_item_prescr_w,
								ie_agrupador,
								nm_usuario_p,
								sysdate,
                                						nr_seq_mat_hor_w
							from	material b,
								prescr_material a
							where	a.nr_prescricao		= nr_prescricao_w
							and	a.nr_sequencia_diluicao	= nr_sequencia_medic_w
							and	a.cd_material		= b.cd_material
							and	a.ie_agrupador		in (9)
							and	a.cd_unidade_medida_dose is not null;

						end if;

						begin
						ds_entrada_w := substr(ds_entrada_w || ',17',1,4000);
						select	decode(upper(cd_unidade_medida_dose),upper(obter_unid_med_usua('ml')),qt_dose, qt_dose * obter_conversao_unid_med(cd_material,upper(obter_unid_med_usua('ml'))))
						into	qt_dose_diluente_w
						from	prescr_material
						where	nr_sequencia_diluicao	= nr_seq_prescr_w
						and	nr_prescricao		= nr_prescricao_w
						and	ie_agrupador		= 3;
						exception
						when others then
							qt_dose_diluente_w	:= 0;
						end;

						ie_aplica_gotejamento_w := 'S';

						if (ie_via_aplicacao_w is not null) then
							select nvl(ie_gotejamento,'S')
							into   ie_aplica_gotejamento_w
							from   via_aplicacao
							where  ie_via_aplicacao = ie_via_aplicacao_w;
						end if;

						if (ie_aplica_gotejamento_w = 'S') then
							Calcular_gotejamento_ordem(ie_tipo_dosagem_w,dividir(qt_min_aplicacao_w,60) + qt_hora_aplicacao_w,qt_dose_diluente_w,vl_gotejo_w);
						else
							vl_gotejo_w := null;
							ie_tipo_dosagem_w := null;
						end if;

						qt_volume_aspirado_w := null;

						if 	(ie_grava_aspirado_w = 'S') then
							qt_volume_aspirado_w := obter_volume_aspirado(nr_sequencia_w);
						end if;

						update	can_ordem_prod
						set	qt_dosagem	        = vl_gotejo_w,
							qt_volume_aspirado	= qt_volume_aspirado_w,
							ie_tipo_dosagem		= ie_tipo_dosagem_w
						where	nr_sequencia		= nr_sequencia_w;

						ds_entrada_w := substr(ds_entrada_w || ',18',1,4000);
						end;
					END LOOP;
					Close C03;
					end;
				END LOOP;

				commit;

				end;
			end if;
			end;
		ds_entrada_w := substr(ds_entrada_w || ',19',1,4000);
		update	prescr_material   /* ALterado cursor pois quando gerava duas ordens ao mesmo tempo estava duplicando   OS240707*/
		set	nr_seq_ordem_prod	= nr_ordem_prod_w
		where	nr_prescricao		= nr_prescricao_w
		and	nr_agrupamento		= nr_agrupamento_w
		and 	ie_agrupador 		= ie_agrupador_w;

		commit;

		END LOOP;
		Close C02;
		end;

	END LOOP;
	Close C01;

	ds_log_w := substr('1 : GERAR_ORDEM_DA_PRESCRICAO_IMPL ds_entrada = ' || ds_entrada_w 	     || CHR(10) || CHR(13) ||
				'nr_seq_atendimento_w'              || nr_seq_atendimento_w          || CHR(10) || CHR(13) ||
				'nr_seq_paciente_w'                 || nr_seq_paciente_w             || CHR(10) || CHR(13) ||
				'cd_medico_resp_w'                  || cd_medico_resp_w              || CHR(10) || CHR(13) ||
				'cd_pessoa_fisica_w'                || cd_pessoa_fisica_w            || CHR(10) || CHR(13) ||
				'nr_prescricao_w'                   || nr_prescricao_w               || CHR(10) || CHR(13) ||
				'qt_superf_corporal_w'              || qt_superf_corporal_w          || CHR(10) || CHR(13) ||
				'dt_real_w'                         || dt_real_w                     || CHR(10) || CHR(13) ||
				'dt_prevista_w'                     || dt_prevista_w                 || CHR(10) || CHR(13) ||
				'nr_sequencia_w'                    || nr_sequencia_w                || CHR(10) || CHR(13) ||
				'nr_ordem_prod_w'                   || nr_ordem_prod_w               || CHR(10) || CHR(13) ||
				'cd_estabelecimento_ww'             || cd_estabelecimento_ww         || CHR(10) || CHR(13) ||
				'cd_material_w'                     || cd_material_w                 || CHR(10) || CHR(13) ||
				'nr_seq_prescr_w'                   || nr_seq_prescr_w               || CHR(10) || CHR(13) ||
				'ie_via_aplicacao_w'                || ie_via_aplicacao_w            || CHR(10) || CHR(13) ||
				'cd_farmaceutico_w'                 || cd_farmaceutico_w             || CHR(10) || CHR(13) ||
				'cd_setor_atendimento_w'            || cd_setor_atendimento_w        || CHR(10) || CHR(13) ||
				'ie_tipo_dosagem_w'                 || ie_tipo_dosagem_w             || CHR(10) || CHR(13) ||
				'qt_dose_diluente_w'                || qt_dose_diluente_w            || CHR(10) || CHR(13) ||
				'nr_agrupamento_w'                  || nr_agrupamento_w              || CHR(10) || CHR(13) ||
				'nr_agrup_ant_w'                    || nr_agrup_ant_w                || CHR(10) || CHR(13) ||
				'cd_material_medic_w'               || cd_material_medic_w           || CHR(10) || CHR(13) ||
				'nr_sequencia_medic_w'              || nr_sequencia_medic_w          || CHR(10) || CHR(13) ||
				'qt_dose_medic_w'                   || qt_dose_medic_w               || CHR(10) || CHR(13) ||
				'cd_unidade_medida_dose_w'          || cd_unidade_medida_dose_w      || CHR(10) || CHR(13) ||
				'nr_seq_item_prescr_w'              || nr_seq_item_prescr_w          || CHR(10) || CHR(13) ||
				'nr_ocorrencia_w'                   || nr_ocorrencia_w               || CHR(10) || CHR(13) ||
				'cd_forma_armaz_w'                  || cd_forma_armaz_w              || CHR(10) || CHR(13) ||
				'cd_estagio_armaz_w'                || cd_estagio_armaz_w            || CHR(10) || CHR(13) ||
				'qt_estabilidade_w'                 || qt_estabilidade_w             || CHR(10) || CHR(13) ||
				'ie_tempo_estab_w'                  || ie_tempo_estab_w              || CHR(10) || CHR(13) ||
				'qt_hora_validade_w'                || qt_hora_validade_w            || CHR(10) || CHR(13) ||
				'qt_min_validade_w'                 || qt_min_validade_w             || CHR(10) || CHR(13) ||
				'qt_dias_util_w'                    || qt_dias_util_w                || CHR(10) || CHR(13) ||
				'ie_exige_liberacao_w'              || ie_exige_liberacao_w          || CHR(10) || CHR(13) ||
				'ie_generico_w'                     || ie_generico_w                 || CHR(10) || CHR(13) ||
				'ie_gerar_reconstituinte_w'         || ie_gerar_reconstituinte_w     || CHR(10) || CHR(13) ||
				'qt_peso_w'                         || qt_peso_w                     || CHR(10) || CHR(13) ||
				'qt_superf_corporea_w'              || qt_superf_corporea_w          || CHR(10) || CHR(13) ||
				'ie_libera_enfer_w'                 || ie_libera_enfer_w             || CHR(10) || CHR(13) ||
				'nr_seq_ordem_prod_w'               || nr_seq_ordem_prod_w           || CHR(10) || CHR(13) ||
				'ie_usuario_manipulador_w'          || ie_usuario_manipulador_w      || CHR(10) || CHR(13) ||
				'ie_restringir_ordem_w'             || ie_restringir_ordem_w         || CHR(10) || CHR(13) ||
				'ie_pre_medicacao_w'                || ie_pre_medicacao_w            || CHR(10) || CHR(13) ||
				'nr_sequencia_log_p'                || nr_sequencia_log_p            || CHR(10) || CHR(13) ||
				'ie_agrupador_w'                    || ie_agrupador_w                || CHR(10) || CHR(13) ||
				'ie_agrup_ant_w'                    || ie_agrup_ant_w                || CHR(10) || CHR(13) ||
				'ie_grava_aspirado_w'               || ie_grava_aspirado_w           || CHR(10) || CHR(13) ||
				'ie_libera_medic_w'                 || ie_libera_medic_w             || CHR(10) || CHR(13) ||
				'qt_volume_aspirado_w'              || qt_volume_aspirado_w          || CHR(10) || CHR(13) ||
				'ie_exibe_apto_w'                   || ie_exibe_apto_w               || CHR(10) || CHR(13) ||
				'ie_estabe_tratamento_w'            || ie_estabe_tratamento_w        || CHR(10) || CHR(13) ||
				'nr_sequencia_solucao_w'            || nr_sequencia_solucao_w        || CHR(10) || CHR(13) ||
				'cd_perfil_w'                       || cd_perfil_w                   || CHR(10) || CHR(13) ||
				'ie_consiste_atend_w'               || ie_consiste_atend_w           || CHR(10) || CHR(13) ||
				'nr_atendimento_w'                  || nr_atendimento_w              || CHR(10) || CHR(13) ||
				'nr_prescricao_ww'                  || nr_prescricao_ww              || CHR(10) || CHR(13) ||
				'ie_exige_lib_farma_w'              || ie_exige_lib_farma_w          || CHR(10) || CHR(13) ||
				'qt_mat_regra_w'                    || qt_mat_regra_w                || CHR(10) || CHR(13) ,1,4000);

	gerar_log_quimio(NULL,
			nr_prescricao_w,
			nr_seq_item_prescr_w,
			ds_log_w,
			'G',
			nm_usuario_p);

exception
when others then
	ds_erro_w := substr(SQLERRM(sqlcode),1,255);
	ds_erro_log_w := ds_erro_log_w || ' - ' || ds_erro_w;
	ds_log_w := substr(Wheb_mensagem_pck.get_texto(455586) || ' : GERAR_ORDEM_DA_PRESCRICAO_IMPL ds_entrada = '  || ds_entrada_w 	     || CHR(10) || CHR(13) || /* ERRO!! */
				'nr_seq_atendimento_w'              || nr_seq_atendimento_w          || CHR(10) || CHR(13) ||
				'nr_seq_paciente_w'                 || nr_seq_paciente_w             || CHR(10) || CHR(13) ||
				'cd_medico_resp_w'                  || cd_medico_resp_w              || CHR(10) || CHR(13) ||
				'cd_pessoa_fisica_w'                || cd_pessoa_fisica_w            || CHR(10) || CHR(13) ||
				'nr_prescricao_w'                   || nr_prescricao_w               || CHR(10) || CHR(13) ||
				'qt_superf_corporal_w'              || qt_superf_corporal_w          || CHR(10) || CHR(13) ||
				'dt_real_w'                         || dt_real_w                     || CHR(10) || CHR(13) ||
				'dt_prevista_w'                     || dt_prevista_w                 || CHR(10) || CHR(13) ||
				'nr_sequencia_w'                    || nr_sequencia_w                || CHR(10) || CHR(13) ||
				'nr_ordem_prod_w'                   || nr_ordem_prod_w               || CHR(10) || CHR(13) ||
				'cd_estabelecimento_ww'             || cd_estabelecimento_ww         || CHR(10) || CHR(13) ||
				'cd_material_w'                     || cd_material_w                 || CHR(10) || CHR(13) ||
				'nr_seq_prescr_w'                   || nr_seq_prescr_w               || CHR(10) || CHR(13) ||
				'ie_via_aplicacao_w'                || ie_via_aplicacao_w            || CHR(10) || CHR(13) ||
				'cd_farmaceutico_w'                 || cd_farmaceutico_w             || CHR(10) || CHR(13) ||
				'cd_setor_atendimento_w'            || cd_setor_atendimento_w        || CHR(10) || CHR(13) ||
				'ie_tipo_dosagem_w'                 || ie_tipo_dosagem_w             || CHR(10) || CHR(13) ||
				'qt_dose_diluente_w'                || qt_dose_diluente_w            || CHR(10) || CHR(13) ||
				'nr_agrupamento_w'                  || nr_agrupamento_w              || CHR(10) || CHR(13) ||
				'nr_agrup_ant_w'                    || nr_agrup_ant_w                || CHR(10) || CHR(13) ||
				'cd_material_medic_w'               || cd_material_medic_w           || CHR(10) || CHR(13) ||
				'nr_sequencia_medic_w'              || nr_sequencia_medic_w          || CHR(10) || CHR(13) ||
				'qt_dose_medic_w'                   || qt_dose_medic_w               || CHR(10) || CHR(13) ||
				'cd_unidade_medida_dose_w'          || cd_unidade_medida_dose_w      || CHR(10) || CHR(13) ||
				'nr_seq_item_prescr_w'              || nr_seq_item_prescr_w          || CHR(10) || CHR(13) ||
				'nr_ocorrencia_w'                   || nr_ocorrencia_w               || CHR(10) || CHR(13) ||
				'cd_forma_armaz_w'                  || cd_forma_armaz_w              || CHR(10) || CHR(13) ||
				'cd_estagio_armaz_w'                || cd_estagio_armaz_w            || CHR(10) || CHR(13) ||
				'qt_estabilidade_w'                 || qt_estabilidade_w             || CHR(10) || CHR(13) ||
				'ie_tempo_estab_w'                  || ie_tempo_estab_w              || CHR(10) || CHR(13) ||
				'qt_hora_validade_w'                || qt_hora_validade_w            || CHR(10) || CHR(13) ||
				'qt_min_validade_w'                 || qt_min_validade_w             || CHR(10) || CHR(13) ||
				'qt_dias_util_w'                    || qt_dias_util_w                || CHR(10) || CHR(13) ||
				'ie_exige_liberacao_w'              || ie_exige_liberacao_w          || CHR(10) || CHR(13) ||
				'ie_generico_w'                     || ie_generico_w                 || CHR(10) || CHR(13) ||
				'ie_gerar_reconstituinte_w'         || ie_gerar_reconstituinte_w     || CHR(10) || CHR(13) ||
				'qt_peso_w'                         || qt_peso_w                     || CHR(10) || CHR(13) ||
				'qt_superf_corporea_w'              || qt_superf_corporea_w          || CHR(10) || CHR(13) ||
				'ie_libera_enfer_w'                 || ie_libera_enfer_w             || CHR(10) || CHR(13) ||
				'nr_seq_ordem_prod_w'               || nr_seq_ordem_prod_w           || CHR(10) || CHR(13) ||
				'ie_usuario_manipulador_w'          || ie_usuario_manipulador_w      || CHR(10) || CHR(13) ||
				'ie_restringir_ordem_w'             || ie_restringir_ordem_w         || CHR(10) || CHR(13) ||
				'ie_pre_medicacao_w'                || ie_pre_medicacao_w            || CHR(10) || CHR(13) ||
				'nr_sequencia_log_p'                || nr_sequencia_log_p            || CHR(10) || CHR(13) ||
				'ie_agrupador_w'                    || ie_agrupador_w                || CHR(10) || CHR(13) ||
				'ie_agrup_ant_w'                    || ie_agrup_ant_w                || CHR(10) || CHR(13) ||
				'ie_grava_aspirado_w'               || ie_grava_aspirado_w           || CHR(10) || CHR(13) ||
				'ie_libera_medic_w'                 || ie_libera_medic_w             || CHR(10) || CHR(13) ||
				'qt_volume_aspirado_w'              || qt_volume_aspirado_w          || CHR(10) || CHR(13) ||
				'ie_exibe_apto_w'                   || ie_exibe_apto_w               || CHR(10) || CHR(13) ||
				'ie_estabe_tratamento_w'            || ie_estabe_tratamento_w        || CHR(10) || CHR(13) ||
				'nr_sequencia_solucao_w'            || nr_sequencia_solucao_w        || CHR(10) || CHR(13) ||
				'cd_perfil_w'                       || cd_perfil_w                   || CHR(10) || CHR(13) ||
				'ie_consiste_atend_w'               || ie_consiste_atend_w           || CHR(10) || CHR(13) ||
				'nr_atendimento_w'                  || nr_atendimento_w              || CHR(10) || CHR(13) ||
				'nr_prescricao_ww'                  || nr_prescricao_ww              || CHR(10) || CHR(13) ||
				'ie_exige_lib_farma_w'              || ie_exige_lib_farma_w          || CHR(10) || CHR(13) ||
				'qt_mat_regra_w'                    || qt_mat_regra_w                || CHR(10) || CHR(13) ||
				Wheb_mensagem_pck.get_texto(455587) || '=' ||ds_erro_log_w ,1,4000); /* ERRO */

	gerar_log_quimio(NULL,
			nr_prescricao_w,
			nr_seq_item_prescr_w,
			ds_log_w,
			'G',
			nm_usuario_p);

    raise;

end;

end GERAR_ORDEM_DA_PRESCRICAO_IMPL;
/