create or replace
procedure pfcs_patients_by_classif( 	nr_seq_indicator_p	number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p	varchar2) is

cursor c01 is
select	a.nr_sequencia nr_seq_classif,
	a.ds_classificacao ds_classification
from	classif_pessoa a
where	nvl(a.ie_exibe_pfcs, 'N') = 'S'
and	a.ie_situacao = 'A';

cursor c02(nr_seq_classif_p number) is
select	obter_nome_pf(a.cd_pessoa_fisica) nm_patient,
	a.cd_pessoa_fisica id_patient,
	a.dt_solicitacao dt_time_request,
	substr(obter_desc_status_gv(a.ie_status,'D'),1,100) ds_status,
	obter_valor_dominio(1410, a.ie_tipo_vaga) ds_type,
	a.nr_atendimento nr_encounter
from	gestao_vaga	a,
	pessoa_classif	b
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	sysdate between b.dt_inicio_vigencia and nvl(b.dt_final_vigencia, sysdate)
and	a.nr_atendimento is not null
and	a.ie_status in ('A','H','I','L')
and	b.nr_seq_classif	= nr_seq_classif_p
and	a.cd_estabelecimento	= to_number(cd_estabelecimento_p);

qt_total_w				number(15) := 0;
pfcs_panel_detail_seq_w			pfcs_panel_detail.nr_sequencia%type;
pfcs_panel_seq_w			pfcs_panel.nr_sequencia%type;
nr_seq_operational_level_w		pfcs_operational_level.nr_sequencia%type;

begin

nr_seq_operational_level_w := pfcs_get_structure_level(
		cd_establishment_p => cd_estabelecimento_p,
		ie_level_p => 'O',
		ie_info_p => 'C');

for c01_w in c01 loop
	begin

	for c02_w in c02(c01_w.nr_seq_classif) loop
		begin

		qt_total_w := qt_total_w + 1;

		select	pfcs_panel_detail_seq.nextval
		into	pfcs_panel_detail_seq_w
		from	dual;

		insert into pfcs_panel_detail(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ie_situation,
			nr_seq_indicator,
			nr_seq_operational_level)
		values (
			pfcs_panel_detail_seq_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			'T',
			nr_seq_indicator_p,
			nr_seq_operational_level_w);

		insert into pfcs_detail_patient(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_detail,
			id_patient,
			nm_patient,
			ds_classification,
			nr_encounter)
		values (
			pfcs_detail_patient_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			pfcs_panel_detail_seq_w,
			c02_w.id_patient,
			c02_w.nm_patient,
			c01_w.ds_classification,
			c02_w.nr_encounter);

		insert into pfcs_detail_bed_request(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_detail,
			dt_time_request,
			ds_status,
			ds_type)
		values (
			pfcs_detail_bed_request_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			pfcs_panel_detail_seq_w,
			c02_w.dt_time_request,
			c02_w.ds_status,
			c02_w.ds_type);

		end;
	end loop;

	commit;

	pfcs_pck.pfcs_generate_results(
			vl_indicator_p => qt_total_w,
			ds_reference_value_p => c01_w.ds_classification,
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => nr_seq_operational_level_w,
			nm_usuario_p => nm_usuario_p,
			nr_seq_panel_p => pfcs_panel_seq_w);

	pfcs_pck.pfcs_update_detail(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_panel_p => pfcs_panel_seq_w,
			nr_seq_operational_level_p => nr_seq_operational_level_w,
			nm_usuario_p => nm_usuario_p);

	qt_total_w := 0;

	end;
end loop;

pfcs_pck.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

end pfcs_patients_by_classif;
/