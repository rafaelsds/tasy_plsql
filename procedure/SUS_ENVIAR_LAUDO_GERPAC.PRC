create or replace
procedure sus_enviar_laudo_gerpac(	nr_seq_laudo_p  in      sus_laudo_paciente.nr_laudo_sus%type,
                                        nm_usuario_p	in      sus_laudo_paciente.nm_usuario%type) is

NR_INTEGRATION_CODE     constant number(3) := 997;
nr_seq_laudo_json_w     varchar2(255);

begin

nr_seq_laudo_json_w := '{"nrSeqInterno" : ' || nr_seq_laudo_p || '}';
execute_bifrost_integration(NR_INTEGRATION_CODE,nr_seq_laudo_json_w);

update	sus_laudo_paciente
set	nm_usuario_gerpac       = nm_usuario_p,
        dt_envio_gerpac	        = sysdate
where	nr_seq_interno	        = nr_seq_laudo_p;

commit;

end sus_enviar_laudo_gerpac;
/
