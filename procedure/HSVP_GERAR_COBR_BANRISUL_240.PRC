create or replace procedure HSVP_GERAR_COBR_BANRISUL_240(	nr_seq_cobr_escrit_p		number,
						cd_estabelecimento_p		number,
						nm_usuario_p			varchar2)  is

ds_conteudo_w			varchar2(240);

/* Header do arquivo */
nm_empresa_w			varchar2(30);
nm_banco_w			varchar2(30);
cd_cgc_w			varchar2(14);
cd_agencia_bancaria_w		varchar2(5);
dt_remessa_retorno_w		date;

/* Detalhe */
nm_pessoa_w			varchar2(40);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(15);
ds_cidade_w			varchar2(15);
nr_inscricao_w			varchar2(15);
ds_nosso_numero_w		varchar2(20);
vl_titulo_w			varchar2(15);
vl_desconto_w			varchar2(15);
vl_juros_mora_w			varchar2(15);
nr_titulo_w			varchar2(15);
cd_cep_w			varchar2(8);
dt_emissao_w			varchar2(8);
dt_vencimento_w			varchar2(8);
ds_uf_w				varchar2(2);
ie_tipo_inscricao_w			varchar2(1);
qt_linhas_w			number(5);
ds_mensagem_w1			varchar2(66);
ds_mensagem_w2			varchar2(66);
ds_mensagem_w3			varchar2(66);
ds_mensagem_ant_w		varchar2(140);
cd_conta_w			varchar2(20);
cd_juros_mora_w			varchar2(1);
ie_emissao_bloqueto_w 	cobranca_escritural.ie_emissao_bloqueto%type;
qt_mesagens_w			number(5);
ds_conteudo_aux_w		varchar2(200);
ie_impressao_w            	varchar2(1);
nr_linha_w			banco_instrucao_boleto.nr_linha%type;

/* Trailer de Lote */
vl_titulos_cobr_w			varchar2(15);
qt_titulos_cobr_w			number(5);
qt_registro_lote_w			number(4)	:= 0;

/* Trailer do Arquivo */
qt_registro_w			number(5)	:= 0;
nr_seq_reg_lote_w			number(6)	:= 0;
cd_cedente_w			varchar2(20)	:= 0;
ie_emite_bloq_w			varchar2(1);

cd_tipo_taxa_multa_w	titulo_receber.cd_tipo_taxa_multa%type;
tx_multa_w				titulo_receber.tx_multa%type;
tx_multa_aplic_w		varchar2(15);
ie_tipo_taxa_w			tipo_taxa.ie_tipo_taxa%type;
ie_tipo_multa_w			varchar2(1);
nr_seq_classe_w			titulo_receber.nr_seq_classe%type;
cd_protesto_w			varchar2(1);
nr_dias_protesto_w		varchar2(2);
nr_seq_grupo_inst_w		number(10);
cd_banco_w				cobranca_escritural.cd_banco%type;
dt_limite_desconto_w	titulo_receber.dt_limite_desconto%type;
dt_limite_desconto1_w   varchar2(8);
cd_desconto_w		varchar2(2);



Cursor	C01 is
select	lpad(nvl(c.cd_agencia_bancaria,'0'),5,'0'),
	--lpad(82||somente_numero(lpad(b.nr_titulo,8,'0')||'-'|| calcula_digito('Modulo11',(82||lpad(b.nr_titulo,8,'0')))),15,'0') ds_nosso_numero,
	RPAD(NVL(b.nr_nosso_numero,0),20,0) ds_nosso_numero,
	lpad(b.nr_titulo,15,0) nr_titulo,
	to_char(nvl(b.dt_pagamento_previsto,sysdate),'DDMMYYYY') dt_vencimento,
	lpad(replace(to_char(b.vl_saldo_titulo, 'fm0000000000000.00'),'.',''),15,'0') vl_titulo,
	to_char(nvl(b.dt_emissao,sysdate),'DDMMYYYY') dt_emissao,
	lpad(replace(to_char(b.vl_saldo_titulo * b.tx_juros / 100 / 30, 'fm0000000000000.00'),'.',''),15,'0') vl_juros_mora,
	lpad(replace(to_char(b.TX_DESC_ANTECIPACAO, 'fm0000000000000.00'),'.',''),15,'0') vl_desconto,
	nvl(decode(b.cd_pessoa_fisica, null, '2', '1'),'0') ie_tipo_inscricao,
	lpad(decode(decode(b.cd_pessoa_fisica, null, 2, 1),2,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,(substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,9) || substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),10,2)),'000000000000000'),15,'0') nr_inscricao,
	rpad(substr(nvl(elimina_caractere_especial(obter_nome_pf_pj(b.cd_pessoa_fisica, b.cd_cgc)),' '),1,40),40,' ') nm_pessoa,
	rpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'R')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,1,'EDN')),' ')),1,40),40,' ') ds_endereco,
	rpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'B')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,1,'B')),' ')),1,15),15,' ') ds_bairro,
	lpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'CEP')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,1,'CEP')),' ')),1,8),8,'0') cd_cep,
	rpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'CI')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,1,'CI')),' ')),1,15),15,' ') ds_cidade,
	rpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'UF')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,1,'UF')),' ')),1,2),2,' ') ds_uf,
	b.cd_tipo_taxa_multa,
	b.tx_multa,
	b.nr_seq_classe,
	a.ie_emissao_bloqueto,
	a.cd_banco,
	b.dt_limite_desconto
from	pls_mensalidade d,
	titulo_receber b,
	titulo_receber_cobr c,
	cobranca_escritural a
where	a.nr_sequencia		= c.nr_seq_cobranca
and	c.nr_titulo			= b.nr_titulo
and	d.nr_sequencia(+)		= b.nr_seq_mensalidade
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

Cursor C02 is
	select	ds_mensagem,
		ie_impressao,
		somente_numero(substr(nr_linha,1,2)) nr_linha
	from	(select	rpad(substr(obter_instrucao_boleto_estab(nr_titulo_w,cd_banco_w,a.nr_linha,cd_estabelecimento_p),1,100),100,' ') ds_mensagem,
			'1' ie_impressao,
			nvl(a.nr_linha,0)  nr_linha
		from	banco_instrucao_boleto a
		where	a.cd_banco	= cd_banco_w
		and	a.cd_estabelecimento = cd_estabelecimento_p
		order by	a.nr_linha)
	where	trim(ds_mensagem)	is not null
	order by	nr_linha;
	
begin

delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;



/* Header do Arquivo */

select	lpad(b.cd_cgc,14,'0'),
	lpad(nvl(c.cd_agencia_bancaria,'0'),5,'0') cd_agencia_bancaria,
	rpad(substr(nvl(elimina_caractere_especial(obter_razao_social(b.cd_cgc)),' '),1,30),30,' ') nm_empresa,
	rpad('BANRISUL', 30, ' ') nm_banco,
	nvl(a.dt_remessa_retorno,sysdate),
	rpad(substr(nvl((
	select	max(x.cd_convenio_banco)
	from	banco_estab_interf x,
		banco_estabelecimento y
	where	a.nr_seq_conta_banco = y.nr_sequencia
	and	y.nr_sequencia = x.nr_seq_conta_banco
	and	upper(x.ds_objeto)	= 'HSVP_GERAR_COBR_BANRISUL_240'
	and	x.ie_tipo_interf_fin	= 'CE'
	),c.cd_convenio_banco),1,20),20,'0') cd_cedente,
	lpad(elimina_caractere_especial(nvl(c.cd_conta,'0')),12,'0') || lpad(elimina_caractere_especial(nvl(c.ie_digito_conta,'0')),1,'0') cd_conta,
	a.ie_emissao_bloqueto
into	cd_cgc_w,
	cd_agencia_bancaria_w,
	nm_empresa_w,
	nm_banco_w,
	dt_remessa_retorno_w,
	cd_cedente_w,
	cd_conta_w,
	ie_emite_bloq_w
from	banco_estabelecimento c,
	estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;



qt_registro_w	:= qt_registro_w + 1;

ds_conteudo_w	:= 	'041' ||
			'0000' || 
			'0' || 
			rpad(' ',9,' ') ||
			'2' ||
			cd_cgc_w ||
			rpad(cd_cedente_w,20,'0') ||
			cd_agencia_bancaria_w ||
			' ' ||
			lpad(cd_conta_w,13,'0') ||


			' ' ||
			nm_empresa_w ||
			nm_banco_w ||
			rpad(' ',10,' ') ||
			'1' ||
			lpad(to_char(dt_remessa_retorno_w,'DDMMYYYY'),8,'0') ||
			lpad(to_char(dt_remessa_retorno_w,'hh24miss'),6,'0') ||
			'000001' ||
			'040' ||
			'00000' ||
			rpad(' ',20,' ') ||
			rpad(' ',20,' ') ||
			rpad(' ',29,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia,
	nr_seq_apres,
	nr_seq_apres_2)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval,
	1,
	1);

/*	Header do Lote	*/

qt_registro_lote_w	:= qt_registro_lote_w + 1;
qt_registro_w	:= qt_registro_w + 1;

ds_conteudo_w		:= 	'041' ||
				lpad(qt_registro_lote_w,4,'0') ||
				'1' ||
				'R' ||
				'01' ||
				'00' ||
				'020' || 
				' ' ||
				'2' ||
				lpad(cd_cgc_w,15,'0') ||
				rpad(cd_cedente_w,20,'0') ||
				cd_agencia_bancaria_w ||
				' ' ||
				lpad(cd_conta_w,13,'0') ||


				' ' ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				rpad(' ',40,' ') ||
				'00000001' ||
				to_char(dt_remessa_retorno_w,'DDMMYYYY') ||
				'00000000' ||
				rpad(' ',33,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia,
	nr_seq_apres,
	nr_seq_apres_2)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval,
	2,
	2);
				
/* Segmento P, Segmento Q*/

open	C01;
loop
fetch	C01 into
	cd_agencia_bancaria_w,
	ds_nosso_numero_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_mora_w,
	vl_desconto_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	ds_uf_w,
	cd_tipo_taxa_multa_w,
	tx_multa_w,
	nr_seq_classe_w,
	ie_emissao_bloqueto_w,
	cd_banco_w,
	dt_limite_desconto_w;
exit	when C01%notfound;
	begin
	/* Segmento P */
	qt_registro_w		:= qt_registro_w + 1;
	nr_seq_reg_lote_w	:= nr_seq_reg_lote_w + 1;
	
	if	(nvl(vl_juros_mora_w,0) <> 0) then
	
		cd_juros_mora_w	:= '1';
	else
		cd_juros_mora_w	:= '0';
		
	end if;
	
	if (nr_seq_classe_w = 24) and (ie_emissao_bloqueto_w = 2)then
	cd_protesto_w		:= '3';
	nr_dias_protesto_w	:= '00';	
	
	else
	cd_protesto_w		:= '1';
	nr_dias_protesto_w	:= '30';
		
	end if;

	ds_conteudo_w		:=	'041' || /*pos 1 a3*/
							lpad(qt_registro_lote_w,4,'0') || /*pos 4 a 7*/
					'3' || /*pos 8*/
					lpad(nr_seq_reg_lote_w,5,'0') || /*pos 9 a 13*/
					'P' || /*pos 14*/
					' ' || /*pos 15*/
					'01' ||	/*pos 16 a 17*/
					cd_agencia_bancaria_w || /*pos 18 a22*/
					' ' || /*pos  23*/
					lpad(cd_conta_w,13,'0') || /*pos 24 a 36*/
					' ' ||					/*pos 37*/
					ds_nosso_numero_w ||  /* pos 38  a  57 nosso numero*/
					'1' || 
					'2' || -- Forma de cad. do t�tulo no banco
					'2' || -- Tipo de documento
					ie_emite_bloq_w || -- Ident. emiss�o do bloqueto
					'1' || -- Ident. da distribui��o					
					nr_titulo_w ||
					dt_vencimento_w ||
					vl_titulo_w ||
					'00000' ||
					' ' ||
					'02' || -- Esp�cie do t�tulo	pos 107 a 108				
					'A' || /*pos 109*/
					dt_emissao_w || /*pos 110 a 117*/
					cd_juros_mora_w || -- C�digo juros mora pos 118 
					'00000000' || /*pos 119 a 126*/
					vl_juros_mora_w || /*pos 127 a 141*/
					'0' || -- C�digo desconto
					'00000000' ||
					vl_desconto_w ||
					'000000000000000' ||
					'000000000000000' ||
					lpad(nr_titulo_w, 25, ' ') ||
					cd_protesto_w /*'3'*/ || -- C�digo para protesto
					nr_dias_protesto_w /*'00'*/ ||
					'1' ||
					'060' ||
					'09' ||
					'0000000000' ||
					' ';

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval,
		3,
		nr_seq_reg_lote_w);

	/* Segmento Q */

	qt_registro_w	:= qt_registro_w + 1;
	nr_seq_reg_lote_w	:= nr_seq_reg_lote_w + 1;
	
	ds_conteudo_w		:=	'041' ||
					lpad(qt_registro_lote_w, 4, '0') ||
					'3' ||
					lpad(nr_seq_reg_lote_w, 5, '0') ||
					'Q' ||
					' ' ||
					'01' ||
					ie_tipo_inscricao_w ||
					nr_inscricao_w ||
					nm_pessoa_w ||
					ds_endereco_w ||
					ds_bairro_w ||
					cd_cep_w ||
					ds_cidade_w ||
					ds_uf_w ||
					'0' ||
					'000000000000000' ||
					rpad(' ',40,' ') ||
					'000' ||
					rpad(' ',20,' ') ||
					rpad(' ',8,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval,
		3,
		nr_seq_reg_lote_w);
		
	/*Segmento R*/
if(tx_multa_w is not null) and (tx_multa_w > 0) then
	qt_registro_w	:= qt_registro_w + 1;
	nr_seq_reg_lote_w	:= nr_seq_reg_lote_w + 1;
	
	--Conforme OS 1438708, a taxa de multa deve sempre ser enviada com uma casa decimal, de acordo com o cliente o tipo de taxa da manuten��o de t�tulos a receber n�o deve ser considerado.
	/*if (cd_tipo_taxa_multa_w is not null) then

		select	max(a.ie_tipo_taxa)
		into	ie_tipo_taxa_w
		from	tipo_taxa a
		where	a.cd_tipo_taxa = cd_tipo_taxa_multa_w;*/
	

	
		/*if (ie_tipo_taxa_w = 'F') then
			tx_multa_aplic_w := lpad(replace(to_char(tx_multa_w, 'fm0000000000000.00'),'.',''),15,'0');
			ie_tipo_multa_w := '1';
		else */
			/*Se for taxa, 1 casa decimal*/
	tx_multa_aplic_w := lpad(replace(to_char(tx_multa_w, 'fm00000000000000.0'),'.',''),15,'0');
	ie_tipo_multa_w := '3';
		/*end if;*/
		
	/*end if;*/
	
	/*if (tx_multa_w is null) then
		tx_multa_aplic_w := lpad(replace(to_char(tx_multa_w, 'fm0000000000000.00'),'.',''),15,'0');
	end if;*/

	if(vl_desconto_w <= 0) then
	vl_desconto_w := rpad(' ',15,' ');
	cd_desconto_w := ' ';
	end if;

	if(lpad(nvl(to_char(dt_limite_desconto_w,'DDMMYYYY'),'0'),8,'0') <= 0)then

	dt_limite_desconto1_w := rpad(' ',8,' ');
	cd_desconto_w := ' ';
	else
	dt_limite_desconto1_w := lpad(nvl(to_char(dt_limite_desconto_w,'DDMMYYYY'),'0'),8,'0');
	cd_desconto_w := '1';
	end if;
	
	ds_conteudo_w		:=	'041' ||
					lpad(qt_registro_lote_w, 4, '0') ||
					'3' ||
					lpad(nr_seq_reg_lote_w, 5, '0') ||
					'R' ||
					' ' ||
					'01' || /*pos 16 a 17*/
					'0'  || /* pos 18 codigo desconto*/
					'00000000' || /*pos 19 a 26*/
					vl_desconto_w || /*pos 27 a 41*/
					nvl(cd_desconto_w, ' ') ||/* pos 42*/
					dt_limite_desconto1_w|| /*pos 43 a 50*/
					vl_desconto_w || /*pos 51 a 65*/
					nvl(ie_tipo_multa_w, '2') || /*pos 66 tipo valor multa*/
					dt_vencimento_w || /*pos 67 a 74*/
					tx_multa_aplic_w ||/*POs 75 a 89*/
					'          ' || /*Pos 90 a 99*/
					rpad(' ',40,' ') || /*pos 100 a 139*/
					rpad(' ',40,' ') || /*pos 140 a 179*/
					'000' || /*pos  180 a 182*/
					'0000' || /*pos 183 a 186*/
					'0000000000000' || /*pos 187a 199*/ 
					'00000000' || /*pos 200 a 207*/
					rpad(' ',33,' '); /*pos 208 a 240*/
		
	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval,
		3,
		nr_seq_reg_lote_w);
	
    /*Fim segmento R*/	
end if;	
end;
end	loop;
close	C01;		

/*	Trailer de Lote	*/

select	lpad(count(1),6,'0'),
	replace(to_char(sum(c.vl_saldo_titulo), 'fm0000000000000.00'),'.','')
into	qt_titulos_cobr_w,
	vl_titulos_cobr_w
from	titulo_receber c,
	titulo_receber_cobr b,
	cobranca_escritural a
where	b.nr_titulo	= c.nr_titulo
and	a.nr_sequencia	= b.nr_seq_cobranca
and	a.nr_sequencia	= nr_seq_cobr_escrit_p;

ds_conteudo_w		:=	'041' ||
				'0001' || 
				'5' ||
				rpad(' ',9,' ') ||
				lpad(qt_registro_w,6,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				rpad(' ',8,' ') ||
				rpad(' ',117,' ');
qt_registro_w			:= qt_registro_w + 1;

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia,
	nr_seq_apres,
	nr_seq_apres_2)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval,
	5,
	5);

/* Trailer do Arquivo */

ds_conteudo_w	:=	'041' ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			'000001' ||
			lpad(qt_registro_w + 1,6,'0') ||
			'000000' ||
			rpad(' ',205,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia,
	nr_seq_apres,
	nr_seq_apres_2)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval,
	6,
	6);

end HSVP_GERAR_COBR_BANRISUL_240;
/