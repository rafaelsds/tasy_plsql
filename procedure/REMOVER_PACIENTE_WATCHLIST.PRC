create or replace procedure REMOVER_PACIENTE_WATCHLIST(		
                                            nm_usuario_p	    	   pessoa_fisica.nm_usuario%TYPE,
                                            cd_pessoa_fisica_p		 panorama_acomp_prof.cd_pessoa_fisica%Type) is

BEGIN
IF (nm_usuario_p IS NOT NULL AND cd_pessoa_fisica_p IS NOT NULL) THEN
    BEGIN
        DELETE panorama_acomp_watchlist
        WHERE nm_usuario = nm_usuario_p
        AND cd_pessoa_fisica = cd_pessoa_fisica_p;
		
	DELETE panorama_watchlist
        WHERE nm_usuario = nm_usuario_p
        AND cd_profissional = cd_pessoa_fisica_p;
    END;
END IF;
COMMIT;

END REMOVER_PACIENTE_WATCHLIST;

/
