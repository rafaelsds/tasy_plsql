create or replace
procedure vincular_desvincular_apae(	nr_seq_apae_p	number,
					nr_cirurgia_p	number,
					ie_opcao_p	varchar2)
					is
begin

if	(ie_opcao_p = 'V') then
	
	update	aval_pre_anestesica
	set	nr_cirurgia	=	nr_cirurgia_p
	where	nr_sequencia	=	nr_seq_apae_p;	

elsif	(ie_opcao_p = 'D') then

	update	aval_pre_anestesica
	set	nr_cirurgia	=	null,
		nr_seq_pepo     =       null
	where	nr_sequencia	=	nr_seq_apae_p;

end if;

commit;

end vincular_desvincular_apae;
/
