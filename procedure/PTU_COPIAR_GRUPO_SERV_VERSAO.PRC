create or replace
procedure ptu_copiar_grupo_serv_versao(	nr_seq_versao_p			Number,
					nr_seq_versao_copia_p		Number,
					cd_estabelecimento_p		Number,
					nm_usuario_p			Varchar2,
					ds_retorno_p		out	Varchar2) is 
qt_regra_versao_p	Number(10);
begin

select	count(1)
into	qt_regra_versao_p
from	ptu_versao_tabela
where	nr_seq_versao = nr_seq_versao_copia_p
and	rownum = 1;

if	(qt_regra_versao_p > 0) then
	insert into	ptu_versao_tabela ( 	dt_atualizacao, dt_atualizacao_nrec, ie_situacao,
						nm_usuario, nm_usuario_nrec, nr_seq_grupo_servico,
						nr_sequencia, nr_seq_versao)
					select	sysdate, sysdate, a.ie_situacao,
						nm_usuario_p, nm_usuario_p, a.nr_seq_grupo_servico,
						ptu_versao_tabela_seq.nextval, nr_seq_versao_p
					from	ptu_versao_tabela	a
					where	nr_seq_versao = nr_seq_versao_copia_p
					and		not exists	(	select 1
										from	ptu_versao_tabela x
										where	x.nr_seq_grupo_servico = a.nr_seq_grupo_servico
										and	x.nr_seq_versao = nr_seq_versao_p);
	ds_retorno_p := '';
else
	ds_retorno_p := 'N�o existem grupos de servi�o cadastrados para a vers�o informada! ';
end if;



commit;

end ptu_copiar_grupo_serv_versao;
/