create or replace
procedure excluir_historico_pendencia(	nr_sequencia_p	Number,
				nm_usuario_p	Varchar2) is 

begin

if	(nvl(nr_sequencia_p,0) > 0) then

	delete	from cta_pendencia_hist
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end excluir_historico_pendencia;
/