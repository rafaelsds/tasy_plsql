create or replace
procedure BL_vincular_nascimento(nr_seq_doacao_p	Number,
				nr_atendimento_p	Number,
				nr_seq_nascimento_p	Number,
				nm_usuario_p		Varchar2) is 

begin
if	(nr_seq_doacao_p is not null) and (nr_atendimento_p is not null) and (nr_seq_nascimento_p is not null) then

	insert into bl_nascimento (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_sexo,
		qt_peso,
		qt_altura,
		dt_nascimento,
		ie_destino,
		ds_observacao,
		nr_seq_doacao)
	select	bl_nascimento_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_sexo,
		qt_peso,
		qt_altura,
		dt_nascimento,
		ie_destino,
		ds_observacao,
		nr_seq_doacao_p
	from	nascimento
	where	nr_atendimento 	= nr_atendimento_p
	and	nr_sequencia	= nr_seq_nascimento_p;

end if;

commit;

end BL_vincular_nascimento;
/