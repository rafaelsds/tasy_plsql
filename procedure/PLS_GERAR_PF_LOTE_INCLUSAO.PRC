create or replace
procedure pls_gerar_pf_lote_inclusao
			(	nr_seq_lote_inclusao_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

nr_seq_beneficiario_w		number(15);
qt_pessoa_fisica_cpf_w		number(15)	:= 0;
nr_cpf_w			varchar2(255);
cd_pessoa_fisica_w		varchar2(255);
ie_permite_cpf_duplicado_w	varchar2(1);
ie_cpf_duplicado_w 		varchar2(1);

Cursor C01 is
	select	nr_sequencia,
		nr_cpf,
		cd_pessoa_fisica
	from	pls_inclusao_beneficiario
	where	nr_seq_lote_inclusao = nr_seq_lote_inclusao_p
	minus
	select	distinct(a.nr_sequencia),
		nr_cpf,
		a.cd_pessoa_fisica
	from	pls_inclusao_beneficiario	a,
		pls_homonimo_pessoa_fisica	b
	where	b.nr_seq_inclusao_benef	= a.nr_sequencia
	and	a.nr_seq_lote_inclusao	= nr_seq_lote_inclusao_p;

begin

ie_permite_cpf_duplicado_w := obter_valor_param_usuario(1232,90,Obter_Perfil_Ativo,nm_usuario_p,cd_estabelecimento_p);

open C01;
loop
fetch C01 into
	nr_seq_beneficiario_w,
	nr_cpf_w,
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	ie_cpf_duplicado_w	:= 'N';
	if	(cd_pessoa_fisica_w is null) then
		if	((ie_permite_cpf_duplicado_w = 'N') and (nr_cpf_w <> '')) then
			ie_cpf_duplicado_w := obter_duplic_CPF('a', nr_cpf_w);
		end if;
		if	(ie_cpf_duplicado_w  = 'N') then
			pls_inclusao_benef_gerar_pf(nr_seq_beneficiario_w,nm_usuario_p);
		end if;
	end if;
	end;
end loop;
close C01;

end pls_gerar_pf_lote_inclusao;
/