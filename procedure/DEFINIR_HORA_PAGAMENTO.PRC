create or replace
procedure definir_hora_pagamento(
			nr_seq_escrit_p		varchar2,
			nr_titulo_p 		varchar2,
			dt_hora_p			varchar2) is 

begin

if (dt_hora_p is not null) and (( length(dt_hora_p) < 4) or ( substr(dt_hora_p,1,2) >= 24 ) or ( substr(dt_hora_p,3,4) >= 60 )) then
    wheb_mensagem_pck.exibir_mensagem_abort(449232);
end if;


update titulo_pagar_escrit 
set hr_pagamento = dt_hora_p 
where nr_seq_escrit = nr_seq_escrit_p 
and nr_titulo = nr_titulo_p;

commit;

end definir_hora_pagamento;
/



