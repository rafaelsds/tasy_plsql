create or replace
procedure desvincular_terceiro(	cd_pessoa_fisica_p	varchar2,
				nr_seq_terceiro_p	number) is 

begin

if	(cd_pessoa_fisica_p	is not null) and
	(nr_seq_terceiro_p	is not null) then
	begin
	
	delete	from	terceiro_pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	nr_seq_terceiro = nr_seq_terceiro_p;
	commit;
	
	end;
end if;

end desvincular_terceiro;
/