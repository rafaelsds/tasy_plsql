create or replace
procedure fis_obter_dados_manif(nr_seq_nota_p	varchar2,
			     nr_danfe_p	out varchar2,
			    cd_cgc_p	out varchar2) is 

nr_danfe_w	nota_fiscal.nr_danfe%type;
cd_cgc_w	nota_fiscal.cd_cgc%type;

begin

select 	max(nr_danfe), max(cd_cgc)
into	nr_danfe_w, cd_cgc_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nota_p;

nr_danfe_p 	:= nr_danfe_w;
cd_cgc_p 	:= cd_cgc_w;

end fis_obter_dados_manif;
/
