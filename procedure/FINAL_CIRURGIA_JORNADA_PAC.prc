CREATE OR REPLACE PROCEDURE final_cirurgia_jornada_pac(nr_cirurgia_p           NUMBER,
                                                       ie_origem_finalizacao_p VARCHAR,
                                                       data_termino_p          DATE) IS

    cd_procedimento_w  procedimento.cd_procedimento%TYPE;
    ie_origem_proced_w procedimento.ie_origem_proced%TYPE;
    cd_pessoa_fisica_w pessoa_fisica.cd_pessoa_fisica %TYPE;

BEGIN

    IF (ie_origem_finalizacao_p = 'GESTAO_CIRURGIA') THEN
        SELECT cd_procedimento_princ,
               ie_origem_proced,
               cd_pessoa_fisica
          INTO cd_procedimento_w,
               ie_origem_proced_w,
               cd_pessoa_fisica_w
          FROM cirurgia
         WHERE nr_cirurgia = nr_cirurgia_p;
    
    ELSIF (ie_origem_finalizacao_p = 'HIST_SAUDE_CIRURGIA') THEN
        SELECT cd_procedimento,
               ie_origem_proced,
               cd_pessoa_fisica
          INTO cd_procedimento_w,
               ie_origem_proced_w,
               cd_pessoa_fisica_w
          FROM historico_saude_cirurgia
         WHERE nr_sequencia = nr_cirurgia_p;
    END IF;

    UPDATE protocolo_int_pac_evento ev
       SET ev.dt_real = data_termino_p
     WHERE EXISTS (SELECT 1
              FROM protocolo_int_pac_etapa et,
                   protocolo_int_paciente  pt
             WHERE ev.nr_seq_prt_int_pac_etapa = et.nr_sequencia
               AND et.nr_seq_protocolo_int_pac = pt.nr_sequencia
               AND pt.cd_pessoa_fisica = cd_pessoa_fisica_w
               AND data_termino_p BETWEEN pt.dt_inicial_previsto AND pt.dt_final_previsto)
       AND ev.dt_real IS NULL
       AND ev.cd_procedimento = cd_procedimento_w
       AND ev.ie_origem_proced = nvl(ie_origem_proced_w, ev.ie_origem_proced);
    COMMIT;

END final_cirurgia_jornada_pac;
/
