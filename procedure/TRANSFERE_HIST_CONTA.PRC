create or replace
procedure transfere_hist_conta(
			nr_seq_conpaci_ret_hist_p	number,
			nr_sequencia_p			number,
			nm_usuario_p			Varchar2) is 

vl_historico_w			number(15,2);
nr_seq_conpaci_ret_hist_ant_w	number(10);
			
begin

select	max(nr_seq_conpaci_ret_hist)
into	nr_seq_conpaci_ret_hist_ant_w
from	convenio_retorno_glosa
where	nr_sequencia		= nr_sequencia_p;

update	convenio_retorno_glosa 
set	nr_seq_conpaci_ret_hist	= nr_seq_conpaci_ret_hist_p,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao 		= sysdate
where	nr_sequencia 		= nr_sequencia_p;

select 	nvl(sum(vl_glosa),0)
into 	vl_historico_w
from 	convenio_retorno_glosa 
where   nr_seq_conpaci_ret_hist = nr_seq_conpaci_ret_hist_ant_w;

update	conta_paciente_ret_hist 
set	vl_historico 	= vl_historico_w
where	nr_sequencia 	= nr_seq_conpaci_ret_hist_ant_w;

select 	nvl(sum(vl_glosa),0) 
into 	vl_historico_w
from 	convenio_retorno_glosa 
where   nr_seq_conpaci_ret_hist = nr_seq_conpaci_ret_hist_p;

update	conta_paciente_ret_hist 
set	vl_historico 	= vl_historico_w
where	nr_sequencia 	= nr_seq_conpaci_ret_hist_p;

commit;
end transfere_hist_conta; 
/
