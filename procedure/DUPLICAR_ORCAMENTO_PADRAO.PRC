create or replace
procedure Duplicar_Orcamento_Padrao(
			cd_estabelecimento_p	number,
			nr_sequencia_p		number,
			ie_documentacao_p   	varchar2,
			ie_materiais_p   	varchar2,
			ie_procedimento_p   	varchar2,
			ds_orcamento_padrao_p  	varchar2,
			nm_usuario_p		varchar2) is 
			
nr_seq_orcamento_w	number(10);
			
Cursor C01 is
	select	*
	from	orcamento_padrao
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	nr_sequencia		= nr_sequencia_p
	order by nr_sequencia;
c01_w	c01%rowtype;
	
Cursor C02 is
	select	*
	from	orcamento_padrao_doc
	where	nr_seq_orcamento_padrao = c01_w.nr_sequencia
	order by nr_sequencia;
c02_w	c02%rowtype;

Cursor C03 is
	select	*
	from	orcamento_padrao_material
	where	nr_seq_orcamento_padrao = c01_w.nr_sequencia
	order by nr_sequencia;
c03_w	c03%rowtype;

Cursor C04 is
	select	*
	from	orcamento_padrao_proc
	where	nr_seq_orcamento_padrao = c01_w.nr_sequencia
	order by nr_sequencia;
c04_w	c04%rowtype;
			
begin

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	
	select	orcamento_padrao_seq.nextval
	into	nr_seq_orcamento_w
	from 	dual;
	
	insert into orcamento_padrao ( 
		cd_estabelecimento,
		ds_orcamento_padrao,
		dt_atualizacao,
		dt_atualizacao_nrec,
		ie_situacao,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia)
	values (
		cd_estabelecimento_p,
		ds_orcamento_padrao_p,
		sysdate,
		sysdate,
		c01_w.ie_situacao,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_orcamento_w);
	
	/*  insere na tabela orcamento_padrao_doc   */
	if	(ie_documentacao_p = 'S') then
		open C02;
		loop
		fetch C02 into	
			c02_w;
		exit when C02%notfound;
			begin
			
			insert into orcamento_padrao_doc (
				ds_arquivo,
				ds_documento,
				dt_atualizacao,
				dt_atualizacao_nrec,
				ie_imprime_doc,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_orcamento_padrao,
				nr_sequencia)
			values (
				c02_w.ds_arquivo,
				c02_w.ds_documento,
				sysdate,
				sysdate,
				c02_w.ie_imprime_doc,
				nm_usuario_p,
				nm_usuario_p,
				nr_seq_orcamento_w,
				orcamento_padrao_doc_seq.nextval);
			
			end;
		end loop;
		close C02;
	end if;
	
	/*  insere na tabela orcamento_padrao_material   */
	if	(ie_materiais_p = 'S') then
		open C03;
		loop
		fetch C03 into	
			c03_w;
		exit when C03%notfound;
			begin
			
			insert into orcamento_padrao_material (
				cd_material,
				dt_atualizacao,
				dt_atualizacao_nrec,
				ie_situacao,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_orcamento_padrao,
				nr_sequencia,
				qt_material)
			values (
				c03_w.cd_material,
				sysdate,
				sysdate,
				c03_w.ie_situacao,
				nm_usuario_p,
				nm_usuario_p,
				nr_seq_orcamento_w,
				orcamento_padrao_material_seq.nextval,
				c03_w.qt_material);
			
			end;
		end loop;
		close C03;
	end if;
	
	/*  insere na tabela orcamento_padrao_proc   */
	if	(ie_procedimento_p = 'S') then
		open C04;
		loop
		fetch C04 into	
			c04_w;
		exit when C04%notfound;
			begin
			
			insert into orcamento_padrao_proc (
				cd_procedimento,
				dt_atualizacao,
				dt_atualizacao_nrec,
				ie_origem_proced,
				ie_situacao,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_orcamento_padrao,
				nr_sequencia,
				qt_procedimento,
				nr_seq_proc_interno,
				nr_seq_exame)
			values (
				c04_w.cd_procedimento,
				sysdate,
				sysdate,
				c04_w.ie_origem_proced,
				c04_w.ie_situacao,
				nm_usuario_p,
				nm_usuario_p,
				nr_seq_orcamento_w,
				orcamento_padrao_proc_seq.nextval,
				c04_w.qt_procedimento,
				c04_w.nr_seq_proc_interno,
				c04_w.nr_seq_exame);
			
			end;
		end loop;
		close C04;
	end if;
	
	end;
end loop;
close C01;

commit;

end Duplicar_Orcamento_Padrao;
/
