create or replace
procedure tx_avisar_prioridade_exame(nm_usuario_p	varchar2) is

cd_pessoa_fisica_w		varchar2(10);
nm_pessoa_fisica_w		varchar2(255);
cd_perfil_w			number(5);
nr_seq_prior_w			number(10);

cursor c01 is
	select	cd_pessoa_fisica,
		substr(obter_nome_pf(cd_pessoa_fisica),1,80),
		a.nr_sequencia		
	from	tx_receptor_prioridade	a,
		tx_receptor b
	where	a.nr_seq_receptor = b.nr_sequencia
	and	(dt_ultimo_aviso is null or
		(dt_ultimo_aviso + 72/24) < sysdate)
	and	tx_obter_se_paciente_ativo(b.nr_sequencia) = 'S'
	and	cd_perfil_w > 0;

	
begin

begin
select	cd_perfil_prior
into	cd_perfil_w
from	tx_parametros;
exception
	when others then
	cd_perfil_w	:= 0;
end;

open c01;
	loop
	fetch c01 into
		cd_pessoa_fisica_w,
		nm_pessoa_fisica_w,
		nr_seq_prior_w;
	exit when c01%notfound;
	
	insert into comunic_interna (
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nm_usuario_destino,
		ds_perfil_adicional,
		nr_sequencia,
		ie_gerencial,
		dt_liberacao,
		cd_estab_destino
	) values (
		sysdate,
		wheb_mensagem_pck.get_texto(798733) || ' ',
		wheb_mensagem_pck.get_texto(798762,
					'CD_PESSOA_FISICA='||cd_pessoa_fisica_w||
					';NM_PESSOA_FISICA='||nm_pessoa_fisica_w),
		nm_usuario_p,
		sysdate,
		'N',
		'',
		cd_perfil_w||', ',
		comunic_interna_seq.nextval,
		'N',
		sysdate,
		null
	);

	update 	tx_receptor_prioridade
	set 	dt_ultimo_aviso = sysdate
	where 	nr_sequencia = nr_seq_prior_w;

	end loop;
close c01;

commit;

end tx_avisar_prioridade_exame;
/	
