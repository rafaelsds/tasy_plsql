create or replace
procedure pls_gerar_faixa_etaria_simul
			(	nr_seq_simulacao_p	Number,
				nm_usuario_p		Varchar2) is 

qt_idade_inicial_w		Number(5);
qt_idade_final_w		Number(5);
qt_registros_w			number(10);
nr_seq_simul_perfil_w		pls_simulacao_perfil.nr_sequencia%type;
nr_seq_parametro_com_w		pls_simulacao_preco.nr_seq_parametro_com%type;
nr_seq_faixa_etaria_w		pls_faixa_etaria.nr_sequencia%type;

Cursor C01 is
	select	qt_idade_inicial,
		qt_idade_final
	from	pls_faixa_etaria_item
	where	nr_seq_faixa_etaria = nr_seq_faixa_etaria_w;

Cursor C02 is
	select	nr_sequencia
	from	pls_simulacao_perfil
	where	nr_seq_simulacao = nr_seq_simulacao_p;
	
begin

begin
select	nr_seq_parametro_com  
into	nr_seq_parametro_com_w
from	pls_simulacao_preco
where	nr_sequencia	= nr_seq_simulacao_p;	
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort(262236, 'NR_SEQ_SIMULACAO=' || nr_seq_simulacao_p);
	/* Mensagem: N�o foi poss�vel gerar a tabela de faixa et�ria da simula��o de pre�o! (Simula��o NR_SEQ_SIMULACAO) */
end;

if	(nr_seq_parametro_com_w is not null) then
	select	max(nr_seq_faixa_etaria)
	into	nr_seq_faixa_etaria_w
	from	pls_simul_regra_param
	where	nr_sequencia = nr_seq_parametro_com_w;
end if;

select	count(*)
into	qt_registros_w
from	pls_simulpreco_coletivo
where	nr_seq_simulacao	= nr_seq_simulacao_p;

if	(qt_registros_w > 0) then
	delete	pls_simulpreco_coletivo
	where	nr_seq_simulacao	= nr_seq_simulacao_p;
end if;

open C02;
loop
fetch C02 into	
	nr_seq_simul_perfil_w;
exit when C02%notfound;
	begin
	
	open C01;
	loop
	fetch C01 into	
		qt_idade_inicial_w,
		qt_idade_final_w;
	exit when C01%notfound;
		begin
		insert into pls_simulpreco_coletivo
			(nr_sequencia,
			nr_seq_simulacao,
			qt_idade_inicial,
			qt_idade_final,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			qt_beneficiario,
			nr_seq_simul_perfil)
		values(	pls_simulpreco_coletivo_seq.nextval,
			nr_seq_simulacao_p,
			qt_idade_inicial_w,
			qt_idade_final_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			0,
			nr_seq_simul_perfil_w);
		end;
	end loop;
	close C01;
	
	end;
end loop;
close C02;

commit;

end pls_gerar_faixa_etaria_simul;
/
