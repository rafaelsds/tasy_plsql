create or replace
procedure pls_div_proc_rel(
			NR_SEQ_ANALISE_p	W_PLS_ANALISE_REL_AUD_PROC.NR_SEQ_ANALISE%TYPE,
			nm_usuario_p		Varchar2) is

count_w		number(1) := 0;
count_6_w	number(10) := 0;

cursor C01 ( NR_SEQ_ANALISE_cp	W_PLS_ANALISE_REL_AUD_PROC.NR_SEQ_ANALISE%TYPE ) is
	select	nr_sequencia
	from	W_PLS_ANALISE_REL_AUD_PROC
	where	nr_seq_analise = NR_SEQ_ANALISE_cp;

begin
if	( NR_SEQ_ANALISE_p is not null ) then
	for r_c01_w in C01( NR_SEQ_ANALISE_p ) loop
		update	W_PLS_ANALISE_REL_AUD_PROC
		set	NR_SEQ_MAXIMO = count_6_w
		where	nr_sequencia = r_c01_w.nr_sequencia;
		
		count_w := count_w + 1;
		
		if	(count_w = 6) then
			count_w := 0;
			count_6_w := count_6_w + 1;
		end if;
	end loop;
end if;

COMMIT;

end pls_div_proc_rel;
/