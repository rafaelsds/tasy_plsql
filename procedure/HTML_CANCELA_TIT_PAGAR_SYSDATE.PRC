create or replace
procedure HTML_CANCELA_TIT_PAGAR_SYSDATE
			(nr_titulo_p		number,
			nm_usuario_p		varchar2) is

/*Procedure criada para o HTML, quando � preciso passar evitar programa��o, passando sysdate como data para par�metro*/
begin

CANCELAR_TITULO_PAGAR(nr_titulo_p, nm_usuario_p, sysdate);

end HTML_CANCELA_TIT_PAGAR_SYSDATE;
/