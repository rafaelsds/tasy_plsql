create or replace
procedure GERAR_TIT_REC_TERC_CONTA_MES
	(dt_mes_ref_p		in	date,
	 cd_estabelecimento_p	in	number,
	 nm_usuario_p		in	varchar2,
	 nr_seq_conta_p		in	number,
	 ds_erro_p		out 	varchar2) is

nr_sequencia_w			number(10,0);
ds_terceiros_liberados_w		varchar2(4000) 	:= null;
ds_lista_terceiros_liberados_w		varchar2(4000)	:= null;
ie_somente_terc_lib_w		varchar2(255);
cd_condicao_pagamento_w		number(10,0);
dt_vencimento_w			date;
nr_seq_terceiro_w		number(10,0);

cursor c01 is
select	a.nr_sequencia,
	a.cd_condicao_pagamento,
	a.dt_vencimento,
	a.nr_seq_terceiro
from	terceiro_conta a
where	trunc(dt_mesano_referencia, 'month')	= trunc(dt_mes_ref_p, 'month')
and	a.cd_estabelecimento		= cd_estabelecimento_p
and	a.ie_status_conta		= 'D'
and	a.nr_sequencia			= nvl(nr_seq_conta_p, a.nr_sequencia)
and 	((ds_lista_terceiros_liberados_w like '% ' || a.nr_seq_terceiro || ' %') or ds_terceiros_liberados_w is null)
and	not exists (select	x.nr_seq_terc_conta
		from	titulo_receber x
	     	where	x.nr_seq_terc_conta = a.nr_sequencia
		and	x.ie_situacao	<> '3');

begin

obter_param_usuario (907,53, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_somente_terc_lib_w);
if	(nvl(ie_somente_terc_lib_w,'N') = 'S') then
	obter_param_usuario (907,43, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ds_terceiros_liberados_w);
	ds_lista_terceiros_liberados_w	:= ' ' || replace(ds_terceiros_liberados_w,',',' ') || ' ';
end if;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_condicao_pagamento_w,
	dt_vencimento_w,
	nr_seq_terceiro_w;
exit when c01%notfound;

	if	(nvl(cd_condicao_pagamento_w,0) > 0) or
		(trunc(dt_vencimento_w, 'dd') >= trunc(sysdate, 'dd')) then
	
		GERAR_TIT_REC_TERC_CONTA(nr_sequencia_w, nm_usuario_p);
		
	elsif	(nvl(length(ds_erro_p) + (length(substr(obter_nome_terceiro(nr_seq_terceiro_w),1,60) || '/' ||  nr_sequencia_w)),0) <= 255) then
			
		ds_erro_p := substr(ds_erro_p || substr(obter_nome_terceiro(nr_seq_terceiro_w),1,60) || '/' ||  nr_sequencia_w || chr(13),1,255);
		
	end if;
end loop;
close c01;
ds_erro_p := substr(ds_erro_p,1,255);

commit;

end GERAR_TIT_REC_TERC_CONTA_MES;
/