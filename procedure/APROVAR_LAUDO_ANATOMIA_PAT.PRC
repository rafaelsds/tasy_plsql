create or replace
procedure aprovar_laudo_anatomia_pat(
		nr_sequencia_p		number,
		ie_tipo_aprovacao_p 	number,
		ie_forma_aprovacao_p 	varchar2,
		qt_caracteres_p		number,
		nm_usuario_p 		varchar2) is

ie_liberado_w	varchar2(1);

begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	select	decode(dt_liberacao,null,'N','S')
	into	ie_liberado_w
	from	laudo_paciente
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_tipo_aprovacao_p = 0) then
		begin
		if	(ie_liberado_w = 'N') and
			(ie_forma_aprovacao_p = 'A') then
			begin
			update	laudo_paciente
			set	dt_aprovacao 		= sysdate,
				nm_usuario_aprovacao 	= nm_usuario_p,
				dt_liberacao 		= sysdate,
				qt_caracteres		= qt_caracteres_p,
				nm_usuario		= nm_usuario_p
			where 	nr_sequencia 		= nr_sequencia_p;
			end;
		else
			begin
			update	laudo_paciente
			set	dt_aprovacao 		= sysdate,
				nm_usuario_aprovacao 	= nm_usuario_p,
				dt_liberacao 		= null,
				qt_caracteres		= qt_caracteres_p,
				nm_usuario		= nm_usuario_p
			where 	nr_sequencia 		= nr_sequencia_p;
			end;
		end if;
		end;
	
	elsif	(ie_tipo_aprovacao_p = 1) then
		begin
		if	(ie_liberado_w = 'N') and
			(ie_forma_aprovacao_p = 'S') then
			begin
			update	laudo_paciente
			set	dt_seg_aprovacao	= sysdate,
				nm_usuario_seg_aprov	= nm_usuario_p,
				dt_liberacao		= sysdate,
				qt_caracteres		= qt_caracteres_p,
				nm_usuario		= nm_usuario_p
			where 	nr_sequencia		= nr_sequencia_p;
			end;
		else
			begin
			update	laudo_paciente
			set	dt_seg_aprovacao	= sysdate,
				nm_usuario_seg_aprov	= nm_usuario_p,
				dt_liberacao		= null,
				qt_caracteres		= qt_caracteres_p,
				nm_usuario		= nm_usuario_p
			where 	nr_sequencia		= nr_sequencia_p;
			end;
		end if;
		end;
	end if;
	end;
end if;
end aprovar_laudo_anatomia_pat;
/