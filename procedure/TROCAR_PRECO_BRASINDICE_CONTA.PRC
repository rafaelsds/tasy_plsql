create or replace procedure Trocar_Preco_Brasindice_Conta(
			cd_material_p			number,
			cd_apresentacao_p		varchar2,
			cd_laboratorio_p		varchar2,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number,
			cd_convenio_p			number,
			cd_categoria_p         		varchar2,
			nr_sequencia_p			number,
			dt_inicio_vigencia_p		date) is 

tx_brasindice_pfb_w		CONVENIO_BRASINDICE.TX_PRECO_FABRICA%type := 0;--number(15,4)	:= 0;
tx_brasindice_pmc_w		CONVENIO_BRASINDICE.TX_BRASINDICE_PMC%type := 0;--number(15,4)	:= 0;
tx_pmc_neg_w			CONVENIO_BRASINDICE.TX_PMC_NEG%type := 0;--number(15,4)	:= 0;
tx_pmc_pos_w			CONVENIO_BRASINDICE.TX_PMC_POS%type := 0;--number(15,4)	:= 0;
tx_simpro_pfb_w			number(15,4);
tx_simpro_pmc_w			number(15,4);
tx_pfb_neg_w			CONVENIO_BRASINDICE.TX_PFB_NEG%type;--number(15,4);
tx_pfb_pos_w			CONVENIO_BRASINDICE.TX_PFB_POS%type;--number(15,4);	
cd_grupo_material_w		number(3,0);
vl_preco_brasindice_w		number(15,4)	:= 0;
vl_unitario_w			number(15,4)	:= 0;
qt_material_w			number(9,4)	:= 0;
ie_tipo_convenio_w		number(2);

nr_atendimento_w		number(10);
ie_tipo_atendimento_w     	number(3)    	:= 0;
cd_setor_atendimento_w   	number(5,0)  	:= 0;
qt_idade_w			number(03,0);
nr_seq_proc_princ_w		number(10,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
dt_entrada_w			date;
cd_tipo_acomodacao_w      	number(4);	
cd_plano_w			varchar2(10);	
cd_medicamento_w		varchar2(6);	

cd_aux_w			number(15)	:= 0;
ds_aux_w			varchar2(2000)	:= '';			
ds_erro_w			varchar2(2000);

tx_brasindice_pfb_neg_w		REGRA_AJUSTE_MATERIAL.tx_pfb_neg%type := 0;--number(15,4)	:= 0;
tx_brasindice_pfb_pos_w		REGRA_AJUSTE_MATERIAL.tx_pfb_pos%type := 0;--number(15,4)	:= 0;

ie_versao_atual_w		number(10,0);
cd_classe_material_w		number(5);
cd_subgrupo_material_w		number(3);
ie_tipo_material_w		varchar2(3);
nr_seq_origem_w			number(10,0);
nr_seq_cobertura_w		number(10,0);
tx_simpro_pos_pfb_w		number(15,4);
tx_simpro_neg_pfb_w		number(15,4);
tx_simpro_pos_pmc_w		number(15,4);
tx_simpro_neg_pmc_w		number(15,4);
qt_dias_internacao_w		number(10,0);
nr_seq_regra_lanc_w		number(10,0);
cd_usuario_convenio_w		varchar2(30);
nr_seq_classif_atend_w		atendimento_paciente.nr_seq_classificacao%type;
vl_minimo_brasind_w			CONVENIO_BRASINDICE.VL_MINIMO%type;--number(15,2);
vl_maximo_brasind_w			CONVENIO_BRASINDICE.VL_MAXIMO%type;--number(15,2);
nr_Seq_w number(10,0);

nr_seq_bras_preco_w		brasindice_preco.nr_sequencia%type;

ie_origem_preco_w		material_atend_paciente.ie_origem_preco%type;
cd_cgc_fornecedor_w		material_atend_paciente.cd_cgc_fornecedor%type;
nr_seq_lote_fornec_w		material_atend_paciente.nr_seq_lote_fornec%type;
cd_material_tuss_w		material_atend_paciente.cd_material_tuss%type;
nr_seq_tuss_mat_item_w		material_atend_paciente.nr_seq_tuss_mat_item%type;
ds_material_tuss_w		material_atend_paciente.ds_material_tuss%type;

Cursor C01 is
	select	nvl(nvl(tx_brasindice_pfb_w,tx_preco_fabrica),1),
		nvl(nvl(tx_brasindice_pmc_w,tx_brasindice_pmc),1),
		nvl(tx_pmc_neg_w,tx_pmc_neg),
		nvl(tx_pmc_pos_w,tx_pmc_pos),
		nvl(tx_brasindice_pfb_neg_w, tx_pfb_neg),
		nvl(tx_brasindice_pfb_pos_w, tx_pfb_pos),
		nvl(vl_minimo_brasind_w, vl_minimo), 
		nvl(vl_maximo_brasind_w, vl_maximo), 
		nvl(nr_Seq_w, nr_sequencia )
	from	convenio_brasindice
	where	cd_convenio		= cd_convenio_p
	and	((cd_categoria is null)	or (cd_categoria = cd_categoria_p))
	and 	((cd_grupo_material is null) or (cd_grupo_material = cd_grupo_material_w))
	and 	((cd_subgrupo_material is null) or (cd_subgrupo_material = cd_subgrupo_material_w))
	and 	((cd_classe_material is null) or (cd_classe_material = cd_classe_material_w))
	and	((nr_seq_estrutura is null) or (consistir_se_mat_estrutura(nr_seq_estrutura,cd_material_p) = 'S'))
	and 	((ie_tipo_material is null) or (ie_tipo_material = ie_tipo_material_w))
	and	((ie_tipo_atendimento is null) or (ie_tipo_atendimento = nvl(ie_tipo_atendimento_w,0)))
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	nvl(ie_situacao,'A')	= 'A'
	and	dt_inicio_vigencia	= (
		select max(a.dt_inicio_vigencia)
		from	convenio_brasindice a
		where	a.cd_convenio		= cd_convenio_p
		and	((cd_categoria is null)	or (cd_categoria = cd_categoria_p))
		and 	((cd_grupo_material is null) or (cd_grupo_material = cd_grupo_material_w))
		and 	((cd_subgrupo_material is null) or (cd_subgrupo_material = cd_subgrupo_material_w))
		and 	((cd_classe_material is null) or (cd_classe_material = cd_classe_material_w))
		and	((nr_seq_estrutura is null) or (consistir_se_mat_estrutura(nr_seq_estrutura,cd_material_p) = 'S'))
		and 	((ie_tipo_material is null) or (ie_tipo_material = ie_tipo_material_w))
		and	((ie_tipo_atendimento is null) or (ie_tipo_atendimento = nvl(ie_tipo_atendimento_w,0)))
		and	cd_estabelecimento	= cd_estabelecimento_p
		and	nvl(a.ie_situacao,'A')= 'A'
		and	a.dt_inicio_vigencia <= dt_inicio_vigencia_p)
	order by nvl(cd_categoria,'0'),
		nvl(nr_seq_estrutura,0),
		nvl(cd_grupo_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_classe_material,0),
		nvl(ie_tipo_material,'0'),
		nvl(ie_tipo_atendimento,0);			

begin

select	nr_atendimento,
	qt_material,
	nr_seq_regra_lanc,
	ie_origem_preco,
	cd_cgc_fornecedor,
	nr_seq_lote_fornec
into	nr_atendimento_w,
	qt_material_w,
	nr_seq_regra_lanc_w,
	ie_origem_preco_w,	
	cd_cgc_fornecedor_w,
	nr_seq_lote_fornec_w	
from	material_atend_paciente
where 	nr_sequencia = nr_sequencia_p;

select 	max(cd_grupo_material),
	max(cd_subgrupo_material),
	max(cd_classe_material),
	max(ie_tipo_material)
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	ie_tipo_material_w
from 	estrutura_material_v
where 	cd_material = cd_material_p;

ie_tipo_convenio_w	:= nvl(obter_tipo_convenio(cd_convenio_p),0);

begin
select	nvl(cd_tipo_acomodacao,0),
	cd_plano_convenio
into	cd_tipo_acomodacao_w,
	cd_plano_w
from	atend_categoria_convenio_v
where	nr_atendimento		= nr_atendimento_w
and	cd_convenio		= cd_convenio_p
and	cd_categoria		= cd_categoria_p;
exception
     	when others then
        cd_tipo_acomodacao_w	:= 0;
	cd_plano_w		:= null;
end;

select 	a.ie_tipo_atendimento,
	nvl(trunc((dt_entrada - nvl(dt_nascimento, a.dt_entrada)) / 365),0),
	a.dt_entrada,
	nvl(to_number(obter_dados_categ_conv(a.nr_atendimento, 'OC')),0),
	nvl(to_number(obter_dados_categ_conv(a.nr_atendimento, 'COB')),0),
	nvl(trunc(nvl(a.dt_alta, sysdate) - a.dt_entrada),0),
	obter_dados_categ_conv(a.nr_atendimento, 'U'),
	a.nr_seq_classificacao
into  	ie_tipo_atendimento_w,
	qt_idade_w,
	dt_entrada_w,
	nr_seq_origem_w,
	nr_seq_cobertura_w,
	qt_dias_internacao_w,
	cd_usuario_convenio_w,
	nr_seq_classif_atend_w
from 	pessoa_fisica b,
	atendimento_paciente a
where	a.nr_atendimento	= nr_atendimento_w
and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

begin
select	cd_setor_atendimento,
	nr_seq_proc_princ
into	cd_setor_atendimento_w,
	nr_seq_proc_princ_w
from	material_atend_paciente
where 	nr_sequencia     = nr_sequencia_p;
exception when others then
	ds_erro_w	:= sqlerrm(sqlcode);
	-- PreMat01: Material_Atend_Paciente nao localizado #@DS_ERRO#@
	Wheb_mensagem_pck.exibir_mensagem_abort(189232,'DS_ERRO=' || ds_erro_w);
end;

select	max(cd_procedimento),
	max(ie_origem_proced)
into	cd_procedimento_w,
	ie_origem_proced_w
from	procedimento_paciente
where	nr_sequencia	= nr_seq_proc_princ_w;

obter_regra_ajuste_mat(
	cd_estabelecimento_p,	--cd_estabelecimento_p	
	cd_convenio_p,		--cd_convenio_p
	cd_categoria_p,         --cd_categoria_p
	cd_material_p,          --cd_material_p
	dt_inicio_vigencia_p,   --dt_vigencia_p
	cd_tipo_acomodacao_w,	--cd_tipo_acomodacao_p
	ie_tipo_atendimento_w,  --ie_tipo_atendimento_p
	cd_setor_atendimento_w, --cd_setor_atendimento_p
	qt_idade_w,	        --qt_idade_p
	nr_sequencia_p,         --nr_sequencia_p
	cd_plano_w,	        --cd_plano_p
	cd_procedimento_w,      --cd_proc_referencia_p
	ie_origem_proced_w,     --ie_origem_proced_p
	null,                   --nr_seq_proc_interno_p
	dt_entrada_w,	        --dt_entrada_p
	cd_aux_w,               --OUT tx_ajuste_p
	cd_aux_w,               --OUT vl_negociado_p
	ds_aux_w,               --OUT ie_preco_informado_p
	ds_aux_w,               --OUT ie_glosa_p
	tx_brasindice_pfb_w,    --OUT tx_brasindice_pfb_p
	tx_brasindice_pmc_w,    --OUT tx_brasindice_pmc_p
	tx_pmc_neg_w,           --OUT tx_pmc_neg_p
	tx_pmc_pos_w,           --OUT tx_pmc_pos_p
	cd_aux_w,               --OUT tx_afaturar_p
	cd_aux_w,               --OUT tx_simpro_pfb_p
	cd_aux_w,               --OUT tx_simpro_pmc_p
	ds_aux_w, 		     --OUT ie_origem_preco_p
	ds_aux_w,               --OUT ie_precedencia_p
	cd_aux_w,               --OUT pr_glosa_p
	cd_aux_w,		--OUT vl_glosa_p
	cd_aux_w,		--OUT cd_tabela_preco_p
	cd_aux_w,               --OUT cd_motivo_exc_conta_p
	cd_aux_w,               --OUT nr_seq_regra_p
	ds_aux_w,               --OUT ie_autor_particular_p
	cd_aux_w,               --OUT cd_convenio_glosa_p
	ds_aux_w,               --OUT cd_categoria_glosa_p
	ds_aux_w,               --ie_atend_retorno_p
	tx_brasindice_pfb_neg_w,--OUT tx_pfb_neg_p 
	tx_brasindice_pfb_pos_w,--OUT tx_pfb_pos_p
	ds_aux_w,               --OUT ie_ignora_preco_venda_p
	tx_simpro_pos_pfb_w,    --OUT tx_simpro_pos_pfb_p
	tx_simpro_neg_pfb_w,    --OUT tx_simpro_neg_pfb_p
	tx_simpro_pos_pmc_w,    --OUT tx_simpro_pos_pmc_p
	tx_simpro_neg_pmc_w,    --OUT tx_simpro_neg_pmc_p
	nr_seq_origem_w,        --nr_seq_origem_p
	nr_seq_cobertura_w,     --nr_seq_cobertura_p
	qt_dias_internacao_w,   --qt_dias_internacao_p
	nr_seq_regra_lanc_w,    --nr_seq_regra_lanc_p
	null,                   --nr_seq_lib_dieta_conv_p
	null,                   --ie_clinica_p
	cd_usuario_convenio_w,  --cd_usuario_convenio_p
	nr_seq_classif_atend_w);--nr_seq_classif_atend_p

open C01;
loop
fetch C01 into	
	tx_brasindice_pfb_w,
	tx_brasindice_pmc_w,
	tx_pmc_neg_w,
	tx_pmc_pos_w,
	tx_pfb_neg_w,
	tx_pfb_pos_w, 
	vl_minimo_brasind_w, 
	vl_maximo_brasind_w, 
	nr_Seq_w;
exit when C01%notfound;
	begin
	tx_brasindice_pfb_w	:=	tx_brasindice_pfb_w;
	tx_brasindice_pmc_w	:=	tx_brasindice_pmc_w;
	tx_pmc_neg_w		:=	tx_pmc_neg_w;
	tx_pmc_pos_w		:=	tx_pmc_pos_w;
	tx_pfb_neg_w		:=	tx_pfb_neg_w;
	tx_pfb_pos_w		:=	tx_pfb_pos_w;
	vl_minimo_brasind_w :=	vl_minimo_brasind_w; 
	vl_maximo_brasind_w :=	vl_maximo_brasind_w;
	nr_Seq_w            := nr_Seq_w;
	end;
end loop;
close C01;

begin

select	nvl(obter_valor_medic_brasindice(	cd_estabelecimento_p,
						cd_medicamento,
						cd_apresentacao,
						cd_laboratorio,
						qt_conversao,
						nvl(dt_inicio_vigencia_p, dt_vigencia),
						tx_brasindice_pfb_w,
						tx_brasindice_pmc_w,
						tx_pmc_neg_w,
						tx_pmc_pos_w,
						tx_pfb_pos_w,
						tx_pfb_neg_w,
						cd_convenio_p,
						cd_categoria_p,
						cd_material_p,
						ie_tipo_atendimento_w,
						null),0),
						cd_medicamento
into	vl_preco_brasindice_w,
		cd_medicamento_w
from	material_brasindice
where	cd_material				= cd_material_p
and	cd_laboratorio				= cd_laboratorio_p
and	cd_apresentacao				= cd_apresentacao_p
and	nvl(ie_tipo_convenio,ie_tipo_convenio_w) = ie_tipo_convenio_w
and	nvl(dt_vigencia,dt_inicio_vigencia_p)	<= dt_inicio_vigencia_p
and	nvl(ie_situacao, 'A')			= 'A'
and	nvl(cd_convenio,cd_convenio_p)		= cd_convenio_p
and	nvl(cd_estabelecimento, nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0)
order by nvl(dt_vigencia,dt_inicio_vigencia_p - 10000),
	nvl(cd_convenio,0),
	nvl(ie_tipo_convenio,0);	
exception	
	when no_data_found then
	wheb_mensagem_pck.exibir_mensagem_abort(354994); 			
end;

if ((nvl(vl_minimo_brasind_w, 0) > 0) or (nvl(vl_maximo_brasind_w, 0) > 0)) then 
	if (((vl_preco_brasindice_w < nvl(vl_minimo_brasind_w, vl_preco_brasindice_w))) or ((vl_preco_brasindice_w > nvl(vl_maximo_brasind_w, vl_preco_brasindice_w)))) then 
		vl_preco_brasindice_w := 0;
	end if;
end if;

vl_unitario_w	:= vl_preco_brasindice_w;
vl_preco_brasindice_w := vl_preco_brasindice_w * qt_material_w;

select	max(ie_versao_atual)
into	ie_versao_atual_w
from	material_brasindice a,
	brasindice_preco b
where	a.cd_material					= cd_material_p
and	a.cd_laboratorio				= cd_laboratorio_p
and	a.cd_apresentacao				= cd_apresentacao_p
and	nvl(a.ie_tipo_convenio,ie_tipo_convenio_w) 	= ie_tipo_convenio_w
and	nvl(a.dt_vigencia,dt_inicio_vigencia_p)		<= dt_inicio_vigencia_p
and	nvl(a.ie_situacao, 'A')				= 'A'
and	nvl(a.cd_convenio,cd_convenio_p)		= cd_convenio_p
and	nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0)
and	a.cd_medicamento				= b.cd_medicamento
and	a.cd_laboratorio				= b.cd_laboratorio
and	a.cd_apresentacao				= b.cd_apresentacao;

if (ie_versao_atual_w is not null) then
	select	max(b.nr_sequencia)
	into	nr_seq_bras_preco_w
	from	material_brasindice a,
		brasindice_preco b
	where	a.cd_material					= cd_material_p
	and	a.cd_laboratorio				= cd_laboratorio_p
	and	a.cd_apresentacao				= cd_apresentacao_p
	and	nvl(a.ie_tipo_convenio,ie_tipo_convenio_w) 	= ie_tipo_convenio_w
	and	nvl(a.dt_vigencia,dt_inicio_vigencia_p)		<= dt_inicio_vigencia_p
	and	nvl(a.ie_situacao, 'A')				= 'A'
	and	nvl(a.cd_convenio,cd_convenio_p)		= cd_convenio_p
	and	nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0)
	and	a.cd_medicamento				= b.cd_medicamento
	and	a.cd_laboratorio				= b.cd_laboratorio
	and	a.cd_apresentacao				= b.cd_apresentacao
	and	b.ie_versao_atual				= ie_versao_atual_w;
end if;

define_material_tuss( cd_estabelecimento_p,
		      cd_material_p,
		      cd_convenio_p,
		      cd_categoria_p,
		      ie_tipo_atendimento_w,
		      ie_origem_preco_w,
		      dt_inicio_vigencia_p,
		      null,
		      nr_seq_bras_preco_w,
		      nr_seq_lote_fornec_w,
		      nm_usuario_p,
		      cd_material_tuss_w,
		      nr_seq_tuss_mat_item_w,
		      ds_material_tuss_w,
		      cd_cgc_fornecedor_w);

update	material_atend_paciente
set	vl_material = vl_preco_brasindice_w,
	ie_valor_informado = 'S',
	vl_unitario = vl_unitario_w,
	cd_medicamento = cd_medicamento_w,
	cd_apresentacao = cd_apresentacao_p,
	cd_laboratorio = cd_laboratorio_p,
	ie_versao_atual = ie_versao_atual_w,
	cd_material_tuss = decode(cd_material_tuss_w, 0,null, cd_material_tuss_w),
	nr_seq_tuss_mat_item = decode(nr_seq_tuss_mat_item_w,0,null, nr_seq_tuss_mat_item_w),
	ds_material_tuss = decode(ds_material_tuss_w, null, null, ds_material_tuss_w),
	nr_seq_bras_preco = nr_seq_bras_preco_w
where	nr_sequencia = nr_sequencia_p;

commit;

end Trocar_Preco_Brasindice_Conta;
/
