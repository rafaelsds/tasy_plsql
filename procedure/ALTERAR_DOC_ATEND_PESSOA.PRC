create or replace
procedure alterar_doc_atend_pessoa (nr_seq_documento_old_p number, 
                                    nr_seq_documento_p     number, 
                                    nr_sequencia_p		   number,
                                    dt_validade_p          date) is 

ds_arquivo_w		      atendimento_paciente_anexo.ds_arquivo%type;			
cd_pessoa_fisica_w	      pessoa_fisica.cd_pessoa_fisica%type;
nm_usuario_w		      usuario.nm_usuario%type;
nr_sequencia_pessoa_doc_w pessoa_documentacao.nr_sequencia%type;

begin

if (nr_sequencia_p is not null) then
	select	a.ds_arquivo,
            b.cd_pessoa_fisica
	into	ds_arquivo_w,
            cd_pessoa_fisica_w
	from	atendimento_paciente_anexo a,
            atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	    a.nr_sequencia   = nr_sequencia_p;

    select	max(nr_sequencia)
    into    nr_sequencia_pessoa_doc_w
    from	pessoa_documentacao
    where	cd_pessoa_fisica = cd_pessoa_fisica_w
    and     ds_arquivo       = ds_arquivo_w
    and     nr_seq_documento = NR_SEQ_DOCUMENTO_OLD_P;

	select	obter_usuario_ativo
	into	nm_usuario_w
	from	dual;
		
	update pessoa_documentacao 
    set    dt_validade 	    = dt_validade_p
          ,nr_seq_documento = NR_SEQ_DOCUMENTO_P 
          ,dt_atualizacao 	= sysdate
          ,nm_usuario 		= nm_usuario_w
	where  nr_sequencia     = nr_sequencia_pessoa_doc_w;
			
	commit;

end if;

end alterar_doc_atend_pessoa;
/
