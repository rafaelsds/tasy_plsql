CREATE OR REPLACE
PROCEDURE med_Gerar_Interf_Conta_Total
			(nr_seq_protocolo_p     	number,
			nr_seq_envio_convenio_p		number,
			cd_convenio_p			number,
			nr_atendimento_p		number,
			nr_interno_conta_p		number,
			cd_cgc_hospital_p		varchar2,
			cd_cgc_convenio_p		varchar2,
			cd_interno_p			varchar2,
			ie_excluir_hon_med_p		varchar2,
			ie_excluir_hon_terc_p		varchar2,
			nm_usuario_p			varchar2) IS

nr_sequencia_w				number(10)	:= 0;
VL_MATERIAIS_W				NUMBER(15,2)	:= 0;
VL_MATMED_W				NUMBER(15,2)	:= 0;
VL_MEDICAMENTOS_W			NUMBER(15,2)	:= 0;
vl_proc_w				number(15,2)	:= 0;
vl_medico_w				number(15,2)	:= 0;
VL_PROCEDIMENTO_W			NUMBER(15,2)	:= 0;
VL_DIARIAS_W				NUMBER(15,2)	:= 0;
VL_SERVICO_W				NUMBER(15,2)	:= 0;
VL_TAXAS_W				NUMBER(15,2)	:= 0;
VL_SADT_W				NUMBER(15,2)	:= 0;
VL_ORTESE_PROTESE_W			NUMBER(15,2)	:= 0;
VL_ORTESE_W				NUMBER(15,2)	:= 0;
VL_PROTESE_W				NUMBER(15,2)	:= 0;
VL_HONORARIOS_W				NUMBER(15,2)	:= 0;
VL_HONOR_CONV_W				NUMBER(15,2)	:= 0;
VL_HONOR_NCONV_W			NUMBER(15,2)	:= 0;
VL_EXTRA_W				NUMBER(15,2)	:= 0;
VL_TOTAL_CONTA_W			NUMBER(15,2)	:= 0;
VL_TOTAL_PACOTE_W			NUMBER(15,2)	:= 0;
VL_TOTAL_FORA_PACOTE_W			NUMBER(15,2)	:= 0;
QT_DIARIAS_W				NUMBER(4)	:= 0;
QT_PARTICIPANTES_W			NUMBER(4)	:= 0;
QT_PROCEDIMENTO_W			NUMBER(9,3)	:= 0;
QT_ITEM_CONTA_W				NUMBER(5)	:= 0;


CURSOR C02 IS
select 	nvl(sum(a.vl_item),0),
	0,
	nvl(sum(a.qt_item),0)
from	med_faturamento a	 
where	a.nr_seq_protocolo	= nr_seq_protocolo_p;

BEGIN

/* Obter sequencia do registro total da conta */ 
select 	w_interf_conta_total_seq.nextval
into	nr_sequencia_w
from 	dual;


/* Calcular valores totais dos procedimentos */ 
OPEN C02;
LOOP
	FETCH C02 	into
			vl_proc_w,
			vl_medico_w,
			qt_procedimento_w;
	EXIT WHEN 	C02%NOTFOUND;
     			BEGIN

			vl_procedimento_w	:= vl_proc_w;

			END;
END LOOP;
close C02;

qt_participantes_w 	:= 0;
vl_total_pacote_w	:= 0;
vl_total_fora_pacote_w	:= 0;

/* Calcular totais conta */
begin
select 	nvl(sum(vl_item),0)
into	vl_total_conta_w
from	med_faturamento
where	nr_seq_protocolo		= nr_seq_protocolo_p;
exception
	when others then
		vl_total_conta_w	:= 0;
end;


/* Calcular QTD total de itens da conta */
begin
select 	count(*)
into	qt_item_conta_w
from	w_interf_conta_item
where	nr_interno_conta = nr_interno_conta_p;
exception
	when others then
		qt_item_conta_w := 0;
end;



/* Gravar totais da conta */ 
insert into w_interf_conta_total(
			nr_sequencia,
			cd_tipo_registro,
 			nr_seq_registro,
 			nr_seq_interface,
 			nr_remessa,
			nr_atendimento,
			nr_interno_conta,
 			vl_total_conta,
 			nr_seq_protocolo,
			cd_convenio,
			cd_cgc_hospital,
			cd_cgc_convenio,
			cd_interno,
			vl_honorarios,
			vl_diarias,
			vl_taxas,
			vl_medicamentos,
			vl_materiais,
			vl_sadt,
			vl_pacote,
			vl_ortese,
			vl_protese,
			qt_diarias,
			qt_participantes,
			vl_matmed,
			vl_procedimento,
			vl_servico,
			vl_ortese_protese,
			vl_honor_conv,
			vl_honor_nconv,
			vl_extra,
			vl_fora_pacote,
			qt_item_conta,
			nm_usuario)
     	values
			(nr_sequencia_w,
 			5,
 			1,
 			1,
 			nr_seq_envio_convenio_p,
			nr_atendimento_p,
			nr_interno_conta_p,
 			vl_total_conta_w,
 			nr_seq_protocolo_p,
			cd_convenio_p,
			cd_cgc_hospital_p,
			cd_cgc_convenio_p,
			cd_interno_p,
			vl_honorarios_w,
			vl_diarias_w,
			vl_taxas_w,
			vl_medicamentos_w,
			vl_materiais_w,
			vl_sadt_w,
			vl_total_pacote_w,
			vl_ortese_w,
			vl_protese_w,
			qt_diarias_w,
			qt_participantes_w,
			vl_matmed_w,
			vl_procedimento_w,
			vl_servico_w,
			vl_ortese_protese_w,
			vl_honor_conv_w,
			vl_honor_nconv_w,
			vl_extra_w,
			vl_total_fora_pacote_w,
			qt_item_conta_w,
			nm_usuario_p);

commit;

END med_Gerar_Interf_Conta_Total;
/