create or replace
procedure duplica_tabela_servico(
				cd_estabelecimento_p	number,
				cd_tabela_origem_p	number,
				cd_tabela_destino_p	number,
				cd_moeda_destino_p	number,
				cd_area_restrita_p	number,
				cd_espec_restrita_p	number,
				cd_grupo_restrito_p	number,
				ie_indice_p		number,
				nm_usuario_p		varchar2,
				ie_substituir_p		varchar2,
				dt_vigencia_origem_p	date,
				dt_vigencia_p		date,
				ie_arredondamento_p	varchar2,
				qt_casas_decimais_p	number,
				ie_ultima_vigencia_p	varchar2,
				ie_sem_fim_vigencia_p	varchar2) is
				
cd_procedimento_w			number(15);
vl_servico_w				number(15,5);
cd_moeda_w				number(3);
ie_origem_proced_w			number(10);
ds_erro_w				varchar2(255);
dt_inicio_vigencia_w			date;
nr_sequencia_w				number(10);
cd_estabelecimento_w			number(10,0);
cd_unidade_medida_w			varchar2(30);
ds_tab_serv_orig_w			varchar2(100);
ds_tab_serv_dest_w			varchar2(100);
cd_procedimento_loc_w preco_servico.cd_procedimento_loc%type;

cursor c01 is
	select	a.cd_estabelecimento,
		a.cd_procedimento,
		a.dt_inicio_vigencia,
		a.vl_servico,
		a.cd_moeda,
		a.ie_origem_proced,
		a.cd_unidade_medida,
		a.cd_procedimento_loc					
	from	estrutura_procedimento_v b,
		preco_servico a
	where	a.cd_tabela_servico = cd_tabela_origem_p
	and	a.cd_procedimento = b.cd_procedimento
	and	a.ie_origem_proced = b.ie_origem_proced
	and	b.cd_area_procedimento = nvl(cd_area_restrita_p,b.cd_area_procedimento)
	and	b.cd_especialidade = nvl(cd_espec_restrita_p,b.cd_especialidade)
	and	b.cd_grupo_proc	= nvl(cd_grupo_restrito_p,b.cd_grupo_proc)
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.dt_inicio_vigencia = nvl(dt_vigencia_origem_p, a.dt_inicio_vigencia)
	and	not exists(	select	1
				from	preco_servico x
				where	x.cd_tabela_servico = cd_tabela_destino_p
				and	x.cd_procedimento = a.cd_procedimento
				and	x.dt_inicio_vigencia = a.dt_inicio_vigencia
				and	x.dt_inicio_vigencia = dt_vigencia_p
				and	x.cd_estabelecimento = cd_estabelecimento_p
				and	nvl(ie_substituir_p,'N') = 'N')
	and	nvl(ie_ultima_vigencia_p,'N') = 'N'
	and	((nvl(ie_sem_fim_vigencia_p,'N') = 'N') or (a.dt_vigencia_final is null))
	union
	select	a.cd_estabelecimento,
		a.cd_procedimento,
		a.dt_inicio_vigencia,
		a.vl_servico,
		a.cd_moeda,
		a.ie_origem_proced,
		a.cd_unidade_medida,
		a.cd_procedimento_loc					
	from	estrutura_procedimento_v b,
		preco_servico a
	where	a.cd_tabela_servico = cd_tabela_origem_p
	and	a.cd_procedimento = b.cd_procedimento
	and	a.ie_origem_proced = b.ie_origem_proced
	and	b.cd_area_procedimento = nvl(cd_area_restrita_p,b.cd_area_procedimento)
	and	b.cd_especialidade = nvl(cd_espec_restrita_p,b.cd_especialidade)
	and	b.cd_grupo_proc	= nvl(cd_grupo_restrito_p,b.cd_grupo_proc)
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.dt_inicio_vigencia = nvl(dt_vigencia_origem_p, a.dt_inicio_vigencia)
	and	not exists
		(select 1 from preco_servico x
		where	x.cd_tabela_servico = cd_tabela_destino_p
		and	x.cd_procedimento = a.cd_procedimento
		and	x.dt_inicio_vigencia = a.dt_inicio_vigencia
		and	x.dt_inicio_vigencia = dt_vigencia_p
		and	x.cd_estabelecimento = cd_estabelecimento_p
		and	nvl(ie_substituir_p,'N') = 'N')
	and 	a.dt_inicio_vigencia = (select max(x.dt_inicio_vigencia)
					from 	preco_servico x
					where 	x.cd_tabela_servico = a.cd_tabela_servico
					and   	x.cd_procedimento = a.cd_procedimento
					and   	x.ie_origem_proced = a.ie_origem_proced
					and	x.cd_estabelecimento = cd_estabelecimento_p)
	and	nvl(ie_ultima_vigencia_p,'N')		= 'S'
	and	((nvl(ie_sem_fim_vigencia_p,'N') = 'N') or (a.dt_vigencia_final is null));

begin

ds_erro_w	:= '';

select	max(ds_tabela_servico)
into	ds_tab_serv_orig_w
from	tabela_servico
where 	cd_tabela_servico	= cd_tabela_origem_p;

select	max(ds_tabela_servico)
into	ds_tab_serv_dest_w
from	tabela_servico
where 	cd_tabela_servico	= cd_tabela_destino_p;

open c01;
loop
fetch c01 into
	cd_estabelecimento_w,
	cd_procedimento_w,
	dt_inicio_vigencia_w,
	vl_servico_w,
	cd_moeda_w,
	ie_origem_proced_w,
	cd_unidade_medida_w,
	cd_procedimento_loc_w;						 
exit when c01%notfound;
	begin

	if	(nvl(ie_substituir_p,'N')	= 'S') then
		if	(dt_vigencia_p is not null) then
			delete	from preco_servico
			where	cd_tabela_servico	= cd_tabela_destino_p
			and	cd_procedimento		= cd_procedimento_w
			and	cd_estabelecimento	= cd_estabelecimento_p;
		else
			delete	from preco_servico
			where	cd_tabela_servico	= cd_tabela_destino_p
			and	cd_procedimento		= cd_procedimento_w
			and	dt_inicio_vigencia	= dt_inicio_vigencia_w
			and	cd_estabelecimento	= cd_estabelecimento_p;
		end if;
	end if;
	
	if (ie_arredondamento_p = 'P') then
		vl_servico_w := round(vl_servico_w * ie_indice_p,qt_casas_decimais_p);
	elsif (ie_arredondamento_p = 'D') then
		vl_servico_w := trunc(vl_servico_w * ie_indice_p,qt_casas_decimais_p);
	elsif (ie_arredondamento_p = 'N') or (qt_casas_decimais_p = 0) then
		vl_servico_w := vl_servico_w * ie_indice_p;
	end if;
	
	
	
	insert into preco_servico(
			cd_estabelecimento,
			cd_tabela_servico,
			cd_procedimento,
			dt_inicio_vigencia,
			vl_servico,
			cd_moeda,
			dt_atualizacao,
			nm_usuario,
			ie_origem_proced,
			cd_unidade_medida,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_inativacao,
			cd_procedimento_loc)				   
	values	(	cd_estabelecimento_w,
			cd_tabela_destino_p,
			cd_procedimento_w,
			nvl(dt_vigencia_p, dt_inicio_vigencia_w),
			vl_servico_w,
			nvl(cd_moeda_destino_p,cd_moeda_w),
			sysdate,
			nvl(nm_usuario_p,'Tasy'),
			ie_origem_proced_w,
			cd_unidade_medida_w,
			sysdate,
			nvl(nm_usuario_p,'Tasy'),
			null,
			cd_procedimento_loc_w);


	exception
    		when others then
			ds_erro_w	:= sqlerrm(sqlcode);
			wheb_mensagem_pck.exibir_mensagem_abort(177821,'CD_PROCEDIMENTO_W='||cd_procedimento_w||';'||'DS_ERRO_W='||ds_erro_w);
	end;
end loop;
close c01;

commit;

end duplica_tabela_servico;
/