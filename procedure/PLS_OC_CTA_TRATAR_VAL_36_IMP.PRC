create or replace
procedure pls_oc_cta_tratar_val_36_imp(	nr_seq_combinada_p		in pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p		in pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p		in pls_oc_cta_selecao_imp.nr_id_transacao%type) is 

nr_index_sel_w				pls_integer;
tb_seq_selecao_w			pls_util_cta_pck.t_number_table;
tb_valido_w				pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w				pls_util_cta_pck.t_varchar2_table_4000;

-- Informa��es da valida��o de n�o-utiliza��o de item autorizado
cursor C01 (	nr_seq_oc_cta_comb_pc	pls_oc_cta_combinada.nr_sequencia%type) is
	select	ie_exige_medico
	from	pls_oc_cta_val_exig_med
	where	nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_pc;
	
--Procedimentos selecionados que ser�o gerados as ocorr�ncias
cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type) is
	select	sel.nr_sequencia, 	
		conta.cd_profissional_exec_conv
	from	pls_oc_cta_selecao_imp	sel,
		pls_conta_imp		conta
	where	sel.nr_id_transacao 	= nr_id_transacao_pc 
	and	sel.ie_valido 		= 'S'
	and	sel.ie_tipo_registro	= 'P'
	and	conta.nr_sequencia	= sel.nr_seq_conta
	and	not exists(	select	1
				from	pls_conta_item_equipe_imp	part
				where	part.nr_seq_conta_proc		= sel.nr_seq_conta_proc
				and	part.cd_profissional_conv is not null);
				
begin
-- Deve exisitr informa��o da regra para executar a valida��o
if	(nr_seq_combinada_p is not null) and
	(nr_id_transacao_p is not null)  then

	for	r_C01_w in C01(nr_seq_combinada_p) loop
		--Verifica se na valida��o exige m�dico
		if	(r_C01_w.ie_exige_medico = 'S') then		
			-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
			pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', 'N', nr_id_transacao_p, null);
			--limpa as vari�veis
			pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
								tb_valido_w,
								tb_observacao_w);
			nr_index_sel_w := 0;
			for	r_C02_w in C02(nr_id_transacao_p) loop
				if	(r_C02_w.cd_profissional_exec_conv is null) then
					
					tb_seq_selecao_w(nr_index_sel_w) := r_C02_w.nr_sequencia;
					tb_observacao_w(nr_index_sel_w)  := null;
					tb_valido_w(nr_index_sel_w) 	 := 'S';	
				
					if	(nr_index_sel_w >= pls_cta_consistir_pck.qt_registro_transacao_w ) then
						
						--Grava o que restar nas vari�veis na tabela
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
												tb_observacao_w, nr_id_transacao_p, 
												'SEQ');
						--limpa as vari�veis	
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
											tb_valido_w,
											tb_observacao_w);
											
						nr_index_sel_w		:= 0;
					else
						nr_index_sel_w := nr_index_sel_w + 1;	
					end if;	
				end if;
			end loop;
		end if;
	end loop; -- C01
	--Grava o que restar nas vari�veis na tabela
	pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
							tb_observacao_w, nr_id_transacao_p, 
							'SEQ');
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', 
						ie_regra_excecao_p, null,
						nr_id_transacao_p, null);
end if;
end pls_oc_cta_tratar_val_36_imp;
/
