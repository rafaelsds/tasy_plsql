create or replace
procedure Same_Consistir_Solic_Interna(	nr_atendimento_p	Number,
					cd_pessoa_fisica_p	Varchar2,
					cd_estabelecimento_p	Number,
					nm_usuario_p		Varchar2,
					ie_pasta_p		Varchar2) is 

ie_verifica_se_setor_same_w	Varchar2(1);
ie_fora_do_same_w		Varchar2(1);
ie_permite_mais_de_uma_solic_w	Varchar2(1);
ie_existe_solic_aberta_w	Varchar2(1);
ds_setor_atendimento_w		Varchar2(255);
ie_fora_de_local_permitido_w	Varchar2(1);
	
/*
ie_pasta_p
S - Solicita��o interna de prontu�rio
L - Solicita��o interna em lote
*/
	
begin

ie_verifica_se_setor_same_w	:= obter_valor_param_usuario(941,159,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
ie_permite_mais_de_uma_solic_w	:= obter_valor_param_usuario(941,160,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);

if (ie_verifica_se_setor_same_w = 'S' and ie_pasta_p = 'S') or (ie_verifica_se_setor_same_w = 'L') then
	begin
	select	'S'
	into	ie_fora_do_same_w
	from	same_prontuario
	where	((nr_atendimento = nr_atendimento_p) or ((nvl(nr_atendimento_p,0) = 0) and (cd_pessoa_fisica = cd_pessoa_fisica_p)))
	and	ie_status not in (2,5,6,10)
	and	obter_classif_setor(cd_setor_atendimento) <> 6
	and	rownum = 1;
	exception
	when others then
		ie_fora_do_same_w := 'N';
	end;

	if (ie_fora_do_same_w = 'S') then
		select	max(obter_dados_setor(cd_setor_atendimento,'DS'))
		into	ds_setor_atendimento_w
		from	same_prontuario
		where	((nr_atendimento = nr_atendimento_p) or ((nvl(nr_atendimento_p,0) = 0) and (cd_pessoa_fisica = cd_pessoa_fisica_p)))
		and	ie_status not in (2,5,6,10);
	
		--N�o � poss�vel realizar a solicita��o, pois o prontu�rio solicitado n�o se encontra no setor do SAME!
		wheb_mensagem_pck.exibir_mensagem_abort(114227,'DS_SETOR=' || ds_setor_atendimento_w);
	end if;	
end if;

if (ie_permite_mais_de_uma_solic_w = 'N' and ie_pasta_p = 'S') or (ie_permite_mais_de_uma_solic_w = 'L') then
	begin
	select	'S'
	into	ie_existe_solic_aberta_w
	from	same_solic_pront
	where	((nr_atendimento is null and cd_pessoa_fisica = cd_pessoa_fisica_p)
	or	(nr_atendimento = nr_atendimento_p or (nvl(nr_atendimento_p,0) = 0 and cd_pessoa_fisica = cd_pessoa_fisica_p)))
	and	(nr_seq_lote is null or ie_permite_mais_de_uma_solic_w = 'L')
	and	ie_status = 'P'
	and	rownum = 1;
	exception
	when others then
		ie_existe_solic_aberta_w := 'N';
	end;

	if (ie_existe_solic_aberta_w = 'S') then
		--J� existe uma solicita��o em aberto para este prontu�rio! Par�metro 160.
		wheb_mensagem_pck.exibir_mensagem_abort(114254);
	end if;
end if;

begin
select	'S'
into	ie_fora_de_local_permitido_w
from	same_prontuario
where	((nr_atendimento = nr_atendimento_p) or ((nvl(nr_atendimento_p,0) = 0) and (cd_pessoa_fisica = cd_pessoa_fisica_p)))
and    	Obter_se_local_permite_solic(nr_seq_local) = 'N'
and	rownum = 1;
exception
when others then
	ie_fora_de_local_permitido_w := 'N';
end;

if (ie_fora_de_local_permitido_w = 'S') then
	--N�o � poss�vel realizar a solicita��o, pois o prontu�rio solicitado se encontra em um local que n�o permite retirada/solicita��o!
	wheb_mensagem_pck.exibir_mensagem_abort(199073);
end if;

end Same_Consistir_Solic_Interna;
/