create or replace
procedure obter_parametro_fatur_mat(	cd_convenio_p		number,
				cd_material_p		number,
				nr_interno_conta_p		number,
				ie_acao_p		varchar2,
				cd_estab_p		number,
				nm_usuario_p		varchar2,
				ds_erro_p		out	varchar2,
				vl_material_p		number,
				vl_unitario_p		number,
				cd_setor_atendimento_p	number) is


/*
'1' - Incluir
'2' - Alterar
'3' - Excluir
'4' - Utilizar valor informado
*/

ie_incluir_conta_w	varchar2(1):= 'S';
ie_alterar_conta_w	varchar2(1):= 'S';
ie_excluir_conta_w	varchar2(1):= 'S';
ie_utilizar_conta_w	varchar2(1):= 'S';
ds_erro_w		varchar2(255) := '';
qt_existe_regra_w	varchar2(1);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
ie_tipo_atendimento_w	number(3);
nr_seq_etapa_w		number(10);

cursor c01 is
	select	nvl(ie_incluir_conta,'N'),
		nvl(ie_alterar_conta,'N'),
		nvl(ie_excluir_conta,'N'),
		nvl(ie_utiliza_valor_inf,'N')
	from	conv_regra_fatur_mat
	where	ie_situacao = 'A'
	and	nvl(cd_convenio, nvl(cd_convenio_p,0)) = nvl(cd_convenio_p,0)
	and	nvl(cd_perfil, nvl(obter_perfil_ativo,0)) = nvl(obter_perfil_ativo,0)
	and	nvl(cd_estabelecimento, nvl(cd_estab_p,0)) = nvl(cd_estab_p,0)
	and	((cd_grupo_material is null) or (nvl(cd_grupo_material,0) = nvl(cd_grupo_material_w,0)))
	and	((ie_tipo_atendimento is null) or (nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = nvl(ie_tipo_atendimento_w,0)))
	and	((cd_subgrupo_material is null) or (nvl(cd_subgrupo_material,0) = nvl(cd_subgrupo_material_w,0)))
	and	((cd_classe_material is null) or (nvl(cd_classe_material,0) = nvl(cd_classe_material_w,0)))
	and	((cd_material is null) or (nvl(cd_material,0) = nvl(cd_material_p,0)))
	and	(((ie_tipo_valor = 'U') and ((vl_unitario_p = - 1) or (nvl(vl_unitario_p,0) between nvl(vl_minimo,nvl(vl_unitario_p,0)) and nvl(vl_maximo,nvl(vl_unitario_p,0)))))
	or	(((ie_tipo_valor = 'T') and ((vl_material_p = - 1) or (nvl(vl_material_p,0) between nvl(vl_minimo,nvl(vl_material_p,0)) and nvl(vl_maximo,nvl(vl_material_p,0)))))))
	and	nvl(nm_usuario_filtro, nvl(nm_usuario_p,'0')) = nvl(nm_usuario_p,'0')
	and	nvl(nr_seq_etapa_filtro, nvl(nr_seq_etapa_w,0)) = nvl(nr_seq_etapa_w,0)
	and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_p,0)) = nvl(cd_setor_atendimento_p,0)
	order by nvl(cd_convenio,0),
		nvl(cd_estabelecimento, 1),
		nvl(nm_usuario_filtro,' '),
		nvl(nr_seq_etapa_filtro,0),
		nvl(cd_perfil,0),
		nvl(ie_tipo_atendimento,0),
		nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_setor_atendimento,0);
		
		
		

begin

select	nvl(max('S'),'N')
into	qt_existe_regra_w
from	conv_regra_fatur_mat
where	cd_convenio = cd_convenio_p
and	cd_estabelecimento = cd_estab_p;

if	(qt_existe_regra_w = 'S') then

	select	nvl(cd_grupo_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_classe_material,0)
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w
	from	estrutura_material_v
	where	cd_material = cd_material_p;

	select	nvl(max(substr(obter_tipo_atendimento(nr_atendimento),1,5)),0)
	into	ie_tipo_atendimento_w
	from	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;

	select	max(obter_conta_paciente_etapa(nr_interno_conta_p,'C'))
	into	nr_seq_etapa_w
	from	dual;

	open c01;
	loop
		fetch c01 into
			ie_incluir_conta_w,
			ie_alterar_conta_w,
			ie_excluir_conta_w,
			ie_utilizar_conta_w;
		exit when c01%notfound;
			begin

			ie_incluir_conta_w	:= ie_incluir_conta_w;
			ie_alterar_conta_w	:= ie_alterar_conta_w;
			ie_excluir_conta_w	:= ie_excluir_conta_w;
			ie_utilizar_conta_w	:= ie_utilizar_conta_w;

			end;
	end loop;
	close c01;

	if	(ie_acao_p = '1') and (ie_incluir_conta_w = 'N') then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280159);
		--goto final;
	elsif	(ie_acao_p = '2') and (ie_alterar_conta_w = 'N') then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280160);
		--goto final;
	elsif	(ie_acao_p = '3') and (ie_excluir_conta_w = 'N') then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280161);
		--goto final;
	elsif	(ie_acao_p = '4') and (ie_utilizar_conta_w = 'N') then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280162);
		--goto final;
	end if;

end if;

<<final>>

ds_erro_p := ds_erro_w;

end obter_parametro_fatur_mat;
/
