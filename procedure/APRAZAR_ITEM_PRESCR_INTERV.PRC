create or replace
procedure aprazar_item_prescr_interv	(ie_tipo_item_p		varchar2,
				nr_prescricao_p		number,
				nr_seq_item_p		number,
				dt_primeiro_horario_p	date,
				nr_seq_motivo_p		number,
				ds_justificativa_p		varchar2,
				nm_usuario_p		varchar2,
				nr_seq_assinatura_p	number default null) is
					
ie_inconsistencia_w		varchar2(1);
ds_inconsistentes_w	varchar2(255);					
					
dt_prim_horario_w		date;
nr_horas_validade_w	number(5,0);
cd_material_w		number(6,0);
hr_prim_horario_w		date;
cd_intervalo_w		varchar2(7);
qt_dose_w		number(18,6);
qt_hora_intervalo_w	number(2,0);
qt_min_intervalo_w		number(5,0);

nr_intervalo_w		number(15,4);
ds_horarios_w		varchar2(255);
ds_horarios_ww		varchar2(255);
ds_horarios_www		varchar2(2000);
nr_seq_mat_cpoe_w	prescr_material.nr_seq_mat_cpoe%type;
ds_horarios_cpoe_w	cpoe_material.ds_horarios%type;

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_item_p is not null) and
	(dt_primeiro_horario_p is not null) then
	
	ie_inconsistencia_w		:= null;
	ds_inconsistentes_w	:= null;
	
	if	(ie_tipo_item_p in ('S','M','IA', 'IAH')) then
	
		select	a.dt_primeiro_horario,
			a.nr_horas_validade,
			b.cd_material,
			dt_primeiro_horario_p, --nvl(b.hr_prim_horario,a.dt_primeiro_horario),
			b.cd_intervalo,		
			b.qt_dose,
			b.qt_hora_intervalo,
			b.qt_min_intervalo
		into	dt_prim_horario_w,
			nr_horas_validade_w,
			cd_material_w,
			hr_prim_horario_w,
			cd_intervalo_w,
			qt_dose_w,
			qt_hora_intervalo_w,
			qt_min_intervalo_w
		from	prescr_medica a,
			prescr_material b
		where	a.nr_prescricao	= b.nr_prescricao
		and	a.nr_prescricao	= nr_prescricao_p
		and	b.nr_sequencia	= nr_seq_item_p;
		
		calcular_horario_prescricao(nr_prescricao_p, cd_intervalo_w, dt_prim_horario_w, hr_prim_horario_w, nr_horas_validade_w, cd_material_w, qt_hora_intervalo_w, qt_min_intervalo_w, nr_intervalo_w, ds_horarios_w, ds_horarios_ww, 'N', null);
		
		ds_horarios_www	:= ds_horarios_w || ds_horarios_ww;
		
		if(ie_tipo_item_p = 'M') then
			select	b.nr_seq_mat_cpoe,
					a.ds_horarios
				into 	nr_seq_mat_cpoe_w,
						ds_horarios_cpoe_w
				from cpoe_material a,
				prescr_material b
			where b.nr_seq_mat_cpoe	= a.nr_sequencia
			and b.nr_prescricao		= nr_prescricao_p
			and b.nr_sequencia		= nr_seq_item_p;
			
			if(nr_seq_mat_cpoe_w is not null and ds_horarios_cpoe_w is null) then
				update cpoe_material
				set ds_horarios = ds_horarios_www
				where nr_sequencia = nr_seq_mat_cpoe_w;
			end if;
		end if;
		
		gerar_aprazamento_interv(ie_tipo_item_p, nr_prescricao_p, nr_seq_item_p, ds_horarios_www, nr_seq_motivo_p, ds_justificativa_p, nm_usuario_p, nr_seq_assinatura_p);
	
	end if;
	
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end aprazar_item_prescr_interv;
/
