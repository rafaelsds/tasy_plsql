create or replace
procedure excluir_modelo_os(
		nr_seq_modelo_p		number,
		nm_usuario_p		Varchar2) is

begin

delete
from	modelo_desenho_os
where	nr_seq_modelo = nr_seq_modelo_p;

delete
from	modelo_conteudo_os
where	nr_seq_modelo = nr_seq_modelo_p;

delete
from	modelo_os
where	nr_sequencia = nr_seq_modelo_p;

commit;

end excluir_modelo_os;
/ 
