create or replace
procedure SAN_Atualiza_Impressao_Corresp (	nr_seq_envio_p		Number,
						nm_usuario_p		Varchar2) is 

begin

if (nr_seq_envio_p is not null) then

	update	san_envio_correspondencia
	set	dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p,
		dt_impressao = sysdate,          
		nm_usuario_impressao = nm_usuario_p
	where	nr_sequencia = nr_seq_envio_p;

end if;

commit;

end SAN_Atualiza_Impressao_Corresp;
/