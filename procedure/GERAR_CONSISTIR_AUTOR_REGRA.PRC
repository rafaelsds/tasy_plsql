create or replace
procedure gerar_consistir_autor_regra(
		nr_seq_agenda_proc_adic_p	number,
		nr_seq_agenda_cons_p		number,
		ds_retorno_p	out			varchar2,
		nm_usuario_p				varchar2) is

ds_retorno_w	varchar2(4000);
begin
if (nm_usuario_p is not null) then
	begin
	gerar_autor_regra(	null, null, null, null, null, null, 'AC',
						nm_usuario_p, null, null, null, nr_seq_agenda_cons_p,
						null, nr_seq_agenda_proc_adic_p, null, null, null);

	consistir_gerar_autor_agrup(nr_seq_agenda_proc_adic_p, 'AP', nm_usuario_p, ds_retorno_w);
	end;
end if;
ds_retorno_p	:= ds_retorno_w;
commit;
end gerar_consistir_autor_regra;
/