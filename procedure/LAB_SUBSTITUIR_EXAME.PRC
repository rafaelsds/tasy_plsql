CREATE OR REPLACE
PROCEDURE lab_substituir_exame	(	nr_prescricao_p		Number,
					nr_seq_prescr_p		Number,
					cd_exame_p		varchar2,
					nr_seq_material_p	number,
					nm_usuario_p		Varchar2,
					cd_perfil_p		number,
					ds_retorno_p		out varchar2) IS 

nr_seq_exame_w			number(10);
nr_seq_grupo_imp_w		number(10);
nr_seq_lab_w			varchar2(20);
cd_material_exame_w		varchar2(20);
cd_estab_w			number(5);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
ie_tipo_atendimento_w		number(3);
ie_tipo_convenio_w		number(2);
cd_setor_atend_w		number(5);
nr_atendimento_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
dt_prescricao_w			date;
ds_erro_w			varchar2(2000);
nr_seq_exame_ant_w		number(10);
nr_Seq_proc_interno_w		number(10);
qt_procedimento_w		number(8,3);
ie_erro_autorizacao_w		varchar2(255);
ie_regra_plano_w		number(2,0);
nr_seq_regra_plano_w		number(10,0);
ie_nova_autorizacao_w		varchar2(1);
ie_consiste_autorizacao_w	varchar2(1)	:= 'N';
ie_gerou_autorizacao_w		varchar2(1);
cd_plano_convenio_w		varchar2(10);
nr_seq_proc_interno_aux_w	number(10);
ie_gerar_sequencia_w		varchar2(1);
ie_gera_amostra_coleta_w 	varchar2(1);
ie_status_atend_w		number(2);
qt_regra_conv_w			number(10);
cd_pessoa_fisica_w		varchar2(10);
ie_glosa_plano_w		regra_ajuste_proc.ie_glosa%type;
nr_seq_regra_preco_w		regra_ajuste_proc.nr_sequencia%type;
nr_seq_ficha_w			lote_ent_sec_ficha.nr_sequencia%type;
BEGIN

select	nvl(max(nr_seq_exame),null)
into	nr_seq_exame_w
from	exame_laboratorio
where	upper(cd_exame)	= upper(cd_exame_p);

if	(nr_seq_exame_w > 0) then
	
	select  max(cd_estabelecimento),
			nvl(max(nr_atendimento), 0),
			nvl(max(dt_prescricao), sysdate),
			max(cd_pessoa_fisica)
	into	cd_estab_w,
			nr_atendimento_w,
			dt_prescricao_w,
			cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;

	select  max(cd_material_exame),
			max(nr_seq_exame),
			max(nr_seq_proc_interno),
			max(ie_status_atend),
			max(qt_procedimento)
	into	cd_material_exame_w,
			nr_seq_exame_ant_w,
			nr_seq_proc_interno_w,
			ie_status_atend_w,
			qt_procedimento_w
	from	prescr_procedimento
	where   nr_prescricao =  nr_prescricao_p
	and     nr_sequencia = 	nr_seq_prescr_p;
	
	if 	(nr_seq_material_p is not null) then	

		select 	max(cd_material_exame)
		into	cd_material_exame_w
		from	material_exame_lab
		where	nr_sequencia = nr_seq_material_p;
	end if;

	select  nvl(max(ie_gerar_sequencia),'P'),
		nvl(MAX(ie_gera_amostra_coleta),'N')
	into 	ie_gerar_sequencia_w,
		ie_gera_amostra_coleta_w	
	from 	lab_parametro 
	where 	cd_estabelecimento = cd_estab_w;
	
	if	(nvl(nr_atendimento_w, 0) > 0) then
		begin
		select	a.ie_tipo_convenio,
			a.ie_tipo_atendimento,
			b.cd_convenio,
			b.cd_categoria,
			a.cd_estabelecimento,
			b.cd_plano_convenio
		into	ie_tipo_convenio_w,
			ie_tipo_atendimento_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_estab_w,
			cd_plano_convenio_w
		from	atend_categoria_convenio b,
			atendimento_paciente a
		where	a.nr_atendimento	= nr_atendimento_w
		and	b.nr_atendimento	= a.nr_atendimento
		and	b.nr_seq_interno	= (select	nvl(max(t.nr_seq_interno),0)
								   from 	atend_categoria_convenio t
									where	t.nr_atendimento   = a.nr_atendimento
									 and 	t.dt_inicio_vigencia = (select max(u.dt_inicio_vigencia)
																	from atend_categoria_convenio u
																	 where u.nr_atendimento = a.nr_atendimento));
		
		exception	
			when no_data_found then
			--rai_application_error(-20011,'Faltam informa��es do conv�nio na Entrada �nica.');
			wheb_mensagem_pck.exibir_mensagem_abort(192526);
		end;
		Obter_Param_Usuario(722,164,cd_perfil_p,nm_usuario_p,cd_estab_w,ie_consiste_autorizacao_w);
		
		select	count(*)
		into	qt_regra_conv_w
		from	exame_lab_convenio
		where 	nr_seq_exame = nr_seq_exame_w;
		
		if (qt_regra_conv_w > 0) then
			obter_exame_lab_convenio(nr_seq_exame_w, cd_convenio_w, cd_categoria_w, ie_tipo_atendimento_w,
					 cd_estab_w, ie_tipo_convenio_w, nr_Seq_proc_interno_w, cd_material_exame_w, cd_plano_convenio_w,
					 cd_setor_atend_w, cd_procedimento_w, ie_origem_proced_w, ds_erro_w, nr_seq_proc_interno_aux_w);
		end if;
		
	end if;
	
	select 	max(nvl((select max(nr_seq_grupo_imp)
					from 	exame_lab_grupo_imp
					where	nr_seq_exame = a.nr_seq_exame
					and	nvl(cd_estabelecimento, nvl(cd_estab_w, 0 )) = nvl(cd_estab_w,0)
					and	nvl(cd_convenio, nvl(cd_convenio_w, 0)) =  nvl(cd_convenio_w, 0)),a.nr_seq_grupo_imp))
	into	nr_seq_grupo_imp_w
	from	exame_laboratorio a
	where	a.nr_seq_exame =  nr_seq_exame_w;		

	/*select 	min(nr_seq_lab)
	into	nr_seq_lab_w
	from	exame_laboratorio a,
			prescr_procedimento b
	where	a.nr_seq_exame = b.nr_seq_exame
	and		cd_material_exame = cd_material_exame_w
	and		nvl(lab_obter_grupo_imp_estab(cd_estab_w,a.nr_seq_exame),a.nr_seq_grupo_imp) = nr_seq_grupo_imp_w;*/
	
	/*OS603904 - Incluida restri��o de pessoa para indexa��o, conforme padr�o de gera��o de seq lab na Gerar_Prescr_Proc_Seq_Lab*/
	select 	nvl(min(b.nr_seq_lab),'0')
	into	nr_seq_lab_w
	from	exame_laboratorio a,
			prescr_procedimento b,
			prescr_medica d
	where	d.nr_prescricao = b.nr_prescricao
	and		a.nr_seq_exame = b.nr_seq_exame
	and		b.cd_material_exame = cd_material_exame_w
	and		d.cd_pessoa_fisica = cd_pessoa_fisica_w
	and		nvl((select max(c.nr_seq_grupo_imp)
				from 	exame_lab_grupo_imp c
				where	c.nr_seq_exame = a.nr_seq_exame
				and	nvl(c.cd_estabelecimento, nvl(cd_estab_w, 0 )) = nvl(cd_estab_w,0)
				and	nvl(c.cd_convenio, nvl(cd_convenio_w, 0)) =  nvl(cd_convenio_w, 0)),a.nr_seq_grupo_imp) = nr_seq_grupo_imp_w
	and 	b.nr_seq_lab is not null
	and 	d.nr_prescricao <= nr_prescricao_p;

	--Rai_application_error(-20011,cd_material_exame_w || '#@#@');
	
	update	prescr_procedimento
	set	nm_usuario 	= nm_usuario_p,
	       	cd_procedimento	= cd_procedimento_w,
	       	ie_origem_proced	= ie_origem_proced_w,
	       	nr_seq_exame 	= nr_seq_exame_w,
	       	nr_seq_lab 	= nr_seq_lab_w,
	       	cd_material_exame = cd_material_exame_w,
	       	ds_observacao 	= ds_observacao || ' ' || wheb_mensagem_pck.get_texto(307896,	'NM_USUARIO_P=' || nm_usuario_p || ';' ||
																							'DT_ATUAL='|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')),
							-- Procedimento adicional inclu�do por #@NM_USUARIO_P#@ em #@DT_ATUAL#@ via Exames Pendentes
	       	nr_seq_exame_anterior = nr_seq_exame_ant_w,
	       	nm_usuario_substituicao = nm_usuario_p,
	       	dt_substituicao = sysdate	
	where 	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia 	= nr_seq_prescr_p;
	
	select 	MAX(nr_sequencia)
	into	nr_seq_ficha_w
	from	lote_ent_sec_ficha
	where	nr_prescricao = nr_prescricao_p;
	
	if (nr_seq_ficha_w is not null) then
		
		update  lote_ent_sec_ficha_exam
		set		nm_usuario 		  = nm_usuario_p,
				dt_atualizacao 	  = sysdate,
				nr_seq_exame 	  = nr_seq_exame_w,
				cd_material_exame = cd_material_exame_w
		where   nr_seq_ficha 	  = nr_seq_ficha_w
		and		nr_seq_exame	  = nr_seq_exame_ant_w;		
	
	end if;
	
	
	delete	prescr_proc_mat_item	
	where 	nr_prescricao 	= nr_prescricao_p
	and	nr_seq_prescr	= nr_seq_prescr_p;

	if 	((ie_gerar_sequencia_w <> 'C') or
		(ie_gera_amostra_coleta_w <> 'S')) or
		((ie_gerar_sequencia_w = 'C') and
		(ie_gera_amostra_coleta_w = 'S') and
		(ie_status_atend_w >= 20))  then
		gerar_prescr_proc_mat_item (nr_prescricao_p, nm_usuario_p, cd_estab_w);
	end if;
	
	if	(ie_consiste_autorizacao_w	= 'S') then

		consiste_plano_convenio(	nr_atendimento_w, 
						cd_convenio_w, 
						cd_procedimento_w, 
						ie_origem_proced_w, 
						dt_prescricao_w, 
						qt_procedimento_w, 
						ie_tipo_atendimento_w, 
						cd_plano_convenio_w, 
						null, 
						ie_erro_autorizacao_w,
						cd_setor_atend_w, 
						nr_seq_exame_w, 
						ie_regra_plano_w, 
						null, 
						nr_seq_regra_plano_w, 
						null,
						cd_categoria_w,
						cd_estab_w,
						null,
						null,
						'',
						ie_glosa_plano_w,
						nr_seq_regra_preco_w);

		update	prescr_procedimento
		set	nr_seq_regra_plano	= nr_seq_regra_plano_w
		where	nr_prescricao		= nr_prescricao_p
		and	nr_sequencia		= nr_seq_prescr_p;

		if	(ie_regra_plano_w in (3,6,7)) then
				
			select	max(ie_nova_autorizacao)
			into	ie_nova_autorizacao_w
			from	regra_convenio_plano
			where	nr_sequencia	= nr_seq_regra_plano_w;

			if	(ie_nova_autorizacao_w	= 'S') then
				gerar_autor_regra(nr_atendimento_w,  null, null, nr_prescricao_p, null, nr_seq_prescr_p, 'LP', nm_usuario_p, null, null, null, null, null, null,'','','');
				Atualizar_autoriz_proced(nr_prescricao_p,nr_atendimento_w);
				select	decode(count(nr_sequencia),0,'N','S')
				into	ie_gerou_autorizacao_w
				from	procedimento_autorizado
				where	nr_prescricao		= nr_prescricao_p
				and	nr_seq_prescricao	= nr_seq_prescr_p;
				
				if	(ie_gerou_autorizacao_w	= 'S') then
					ds_retorno_p	:= wheb_mensagem_pck.get_texto(307894); -- Exame necessita de autoriza��o, a mesma foi gerada!
				else
					ds_retorno_p	:= wheb_mensagem_pck.get_texto(307892); -- Exame necessita de autoriza��o!
				end if;	
			else
				ds_retorno_p	:= wheb_mensagem_pck.get_texto(307892); -- Exame necessita de autoriza��o!
			end if;
			
		end if;		
		
	end if;	

end if;

commit;

END lab_substituir_exame;
/
