create or replace procedure Delete_immuni_dose_patient(nr_sequencia_p	number ,
                                                       nr_sequencia_vacina_p	number,
                                                       nm_usuario_p varchar2) is 

cd_material_w		number(10);
qt_material_w		number(20,5);
cd_unidade_medida_w	varchar2(35);
ie_retornar_estoque_w	varchar2(1);

nr_seq_vacina_w number;

  
  Cursor C01 is
	select 	a.cd_material,
		a.qt_material,
		a.cd_unidade_medida
	from   	vacina_paciente_material a,
		paciente_vacina b
	where  	b.nr_sequencia = nr_sequencia_vacina_p
	and	a.nr_seq_vacina = b.nr_sequencia;

begin
ie_retornar_estoque_w := nvl(obter_valor_param_usuario(903,25,Obter_Perfil_ativo,nm_usuario_p,obter_estabelecimento_ativo),'N');

if (nr_sequencia_p is not null) then

  select nr_seq_vacina into nr_seq_vacina_w from paciente_vacina where nr_sequencia = nr_sequencia_p;
  
  if ('S' = ie_retornar_estoque_w) then
		open C01;
		loop
		fetch C01 into
			cd_material_w,
			qt_material_w,
			cd_unidade_medida_w;
		exit when C01%notfound;
			begin
			Inserir_Mat_Med_Vacina(nr_seq_vacina_w, cd_material_w, qt_material_w, cd_unidade_medida_w, obter_estabelecimento_ativo, nm_usuario_p, 'D',null);
			end;
		end loop;
		close C01;
	end if;
  
  Delete 	paciente_vacina
  where 	nr_sequencia = nr_sequencia_p;

	end if;

commit;

end Delete_immuni_dose_patient;
/