create or replace
procedure pls_atualizar_regra_preco_proc
			(	nr_seq_regra_p		number,
				ie_opcao_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

/* ie_opcao_p
	'A' - Atualizar
	'D' - Deletar
*/

nr_seq_regra_w			number(10)	:= null;
nr_seq_regra_proc_ref_w		number(10);
ds_instrucao_w			varchar2(32767);
ret_w					number;
nm_atributo_w			Tabela_Atributo.nm_atributo%type;


cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_regra_proc_ref
	from	pls_regra_preco_proc	a
	start with a.nr_sequencia in	(select	x.nr_sequencia
					from	pls_regra_preco_proc x
					where	x.nr_sequencia	= nr_seq_regra_p)
	connect by prior a.nr_sequencia = a.nr_seq_regra_proc_ref;
	
Cursor C02 is
	select	a.nr_sequencia
	from	pls_regra_preco_proc	a
	start with a.nr_sequencia in	(select	x.nr_sequencia
					from	pls_regra_preco_proc x
					where	x.nr_seq_regra_proc_ref	= nr_seq_regra_p)
	connect by prior a.nr_sequencia = a.nr_seq_regra_proc_ref
	order by level desc;
	
Cursor C03 is
	select	a.nm_atributo
	From	Tabela_Atributo	a
	where	upper(a.nm_tabela) = 'PLS_REGRA_PRECO_PROC'
	and		a.NR_SEQ_APRESENT is not null
	and		upper(ie_tipo_atributo) not in ('FUNCTION', 'VISUAL')
	and		upper(a.nm_atributo) not in
	('NR_SEQUENCIA','CD_OPERADORA_EMPRESA','NR_SEQ_REGRA_PROC_REF','DS_REGRA','NR_SEQ_RP_COMBINADA','IE_TIPO_TABELA')
	order by 1;

begin
if	(ie_opcao_p = 'A') then
	ds_instrucao_w := 'update pls_regra_preco_proc set' || chr(13);
	
	open C03;
	loop
	fetch C03 into	
		nm_atributo_w;
	exit when C03%notfound;
		ds_instrucao_w := ds_instrucao_w || nm_atributo_w || ' = (select ' || nm_atributo_w || ' from pls_regra_preco_proc where nr_sequencia = #seq# ), ' || chr(13);
	end loop;
	close C03;
	
	ds_instrucao_w := substr(ds_instrucao_w,1,length(ds_instrucao_w)-3) || chr(13);
	ds_instrucao_w := ds_instrucao_w || 'where nr_sequencia	= #seqregra# ';
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_regra_w,
		nr_seq_regra_proc_ref_w;
	exit when C01%notfound;
	begin
		if	(nr_seq_regra_proc_ref_w is not null) then
			executar_sql_dinamico(replace(replace(ds_instrucao_w, '#seqregra#', nr_seq_regra_w), '#seq#', nr_seq_regra_proc_ref_w),ret_w);
		end if;
	end;
	end loop;
	close C01;

elsif	(ie_opcao_p = 'D') then
	open C02;
	loop
	fetch C02 into	
		nr_seq_regra_w;
	exit when C02%notfound;
		begin
			delete	pls_regra_preco_proc
			where	nr_sequencia	= nr_seq_regra_w;
		end;
	end loop;
	close C02;
end if;

commit;

end pls_atualizar_regra_preco_proc;
/