create or replace procedure gerar_transacao_res	(   nr_atendimento_p		number,
													cd_transacao_p			varchar2,													
													nm_usuario_p			varchar2,
													cd_convenio_p			number default null,		
													nr_seq_prontuario_p		varchar2 default null,
													ie_tipo_inativacao_p	varchar2 default null,
													nr_sequencia_anexo_p	varchar2 default null) is

ie_resUnimed_w			varchar2(1);
ie_resPhilips_w			varchar2(1);

ds_param_integ_hl7_w 	varchar2(4000);
cd_convenio_w			number(5);
ie_atualiza_w			varchar2(1);
cd_pessoa_fisica_w		varchar2(10	);
ie_privacidade_w		varchar2(1);
cd_pf_usuario_w			varchar2(10);
nr_seq_idioma_w			number(10);
cd_estabelecimento_w	varchar2(50) := obter_estabelecimento_ativo;
cd_interno_w			conversao_meio_externo.cd_interno%type;
qt_dias_retroativo_w	conversao_meio_externo.cd_interno%type;
ds_patient_id_w			varchar2(255);
ds_creationTimeFrom_w	varchar2(255);
ds_creationTimeTo_w		varchar2(255);
ds_retorno_integracao_w varchar2(4000);


begin

If ( nvl(nr_atendimento_p,0) > 0 ) then

	begin

	nr_seq_idioma_w := wheb_usuario_pck.get_nr_seq_idioma;

	cd_pf_usuario_w := Obter_Pf_Usuario(nm_usuario_p, 'C');

	cd_convenio_w := nvl(cd_convenio_p,0);

	Select 	obter_pessoa_atendimento(nr_atendimento_p,'C')
	into	cd_pessoa_fisica_w
	from 	dual;

	if ( cd_convenio_w = 0) then

		Select  Obter_Convenio_Atendimento(nr_atendimento_p)
		into	cd_convenio_w
		from	dual;		

	end if;	

	if (cd_convenio_w > 0) then	

		if( cd_transacao_p = '01000' ) then

			Select  nvl(max('S'),'N')
			into	ie_resPhilips_w
			from    conversao_meio_externo
			where   upper(cd_externo) = 'RESPHILIPS'
			and		upper(nm_tabela) = 'ATEND_CATEGORIA_CONVENIO'
			and     cd_interno = to_char(cd_convenio_w);



			if (ie_resPhilips_w = 'N') then

				Select  nvl(max('S'),'N')
				into	ie_resPhilips_w
				from    res_envio a,
						res_envio_protocolo e
				where   e.nr_seq_envio = a.nr_sequencia
				and     nvl(a.cd_convenio,cd_convenio_w) = cd_convenio_w
				and     nvl(a.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w;

			end if;


			if ( ie_resPhilips_w = 'S') then

					Select  nvl(max('S'),'N')
					into	ie_privacidade_w
					from	pep_pac_ci a,
							pep_pac_ci_anexo b
					where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
					and		b.nr_seq_pac_ci = a.nr_sequencia
					and		a.ie_tipo_consentimento = 'L'
					and		a.dt_liberacao is not null
					and		a.dt_inativacao is null
					and		b.ds_arquivo is not null;
					--and		obter_se_prontuario_envio(a.cd_pessoa_fisica) = 'S';

					if ( ie_privacidade_w = 'S' ) then

						ie_privacidade_w := 'S';

					end if;	

			end if;	


		elsif ( cd_transacao_p = '01100' ) then				

				Select  nvl(max('S'),'N')
				into	ie_resPhilips_w
				from    conversao_meio_externo
				where   upper(cd_externo) = 'RESPHILIPS'
				and		upper(nm_tabela) = 'ATEND_CATEGORIA_CONVENIO'
				and     cd_interno = to_char(cd_convenio_w);


				if ( ie_resPhilips_w = 'S') then

					Select  max(cd_interno)
					into	cd_interno_w
					from    conversao_meio_externo
					where   upper(cd_externo) = 'OID'
					and		upper(nm_tabela) = 'RES_ENVIO';

					if ( cd_interno_w is not null) then

						if (cd_interno_w = '2.16.840.1.113883.13.237') then -- CPF

							Select  max(nr_cpf)
							into	ds_patient_id_w
							from 	pessoa_fisica
							where	cd_pessoa_fisica = cd_pessoa_fisica_w;

						elsif (cd_interno_w = '2.16.840.1.113883.13.236') then -- CNS

							Select  max(nr_cartao_nac_sus)
							into	ds_patient_id_w
							from 	pessoa_fisica
							where	cd_pessoa_fisica = cd_pessoa_fisica_w;	


						end if;

						if ( ds_patient_id_w is not null ) then

							Select  nvl(max(cd_interno),'30')
							into	qt_dias_retroativo_w
							from    conversao_meio_externo
							where   upper(cd_externo) = 'QT_DIAS_RETROATIVOS'
							and		upper(nm_tabela) = 'RES_ENVIO';

							Select 	to_char(sysdate - (somente_numero(qt_dias_retroativo_w)),'yyyymmdd'),
									to_char(sysdate,'yyyymmdd')
							into	ds_creationTimeFrom_w,
									ds_creationTimeTo_w
							from	dual;


							SELECT BIFROST.SEND_INTEGRATION(
							'patientDocument',
							'com.philips.tasy.integration.atepac.documents.Document',
							'{"patientID" : '||'"'||ds_patient_id_w ||'"'||','||
							'"patientsOID" : '||'"'|| cd_interno_w ||'"'||','||
							'"creationTimeFrom" : '||'"'|| ds_creationTimeFrom_w || '"'||','||
							'"creationTimeTo" : '||'"'|| ds_creationTimeTo_w ||'"'|| '} ',
							nm_usuario_p)
							INTO ds_retorno_integracao_w
							FROM dual;

						end if;

					end if;

				end if;

		else

			Select  nvl(max('S'),'N')
			into	ie_resUnimed_w
			from    conversao_meio_externo
			where   upper(cd_externo) = 'RESUNIMED'
			and		upper(nm_tabela) = 'ATEND_CATEGORIA_CONVENIO'
			and     cd_interno = to_char(cd_convenio_w);

			if ( ie_resUnimed_w = 'S') then

				if(cd_transacao_p = '00710') then
					ie_privacidade_w := 'S';
				elsif(cd_transacao_p = '00790') then
					Select  nvl(max('S'),'N')
					into	ie_privacidade_w
					from	pep_pac_ci_anexo
					where	nr_seq_pac_ci = nr_sequencia_anexo_p;
				else
				  Select  nvl(max('S'),'N')
					into	ie_privacidade_w
				  from	pep_pac_ci_anexo
							where	nr_seq_pac_ci = (
					select MAX(nr_sequencia)
					from PEP_PAC_CI pac
					where pac.cd_pessoa_fisica = cd_pessoa_fisica_w
						AND pac.ie_situacao = 'A');
				end if;

				if (ie_privacidade_w = 'S') then

					ds_param_integ_hl7_w := 'nr_seq_prontuario_emissao=' || nr_seq_prontuario_p  || ';' ||
											'nr_atendimento='             || nr_atendimento_p            || ';' ||
											'cd_transacao='               || cd_transacao_p       || ';' ||
											'nm_usuario_solic='               || nm_usuario_p      || ';' ||
											'cd_pf_usuario='               || cd_pf_usuario_w      || ';' ||
											'nr_seq_idioma='               || nr_seq_idioma_w      || ';' ||
											'cd_estabelecimento='               || cd_estabelecimento_w      || ';' ||
											'ie_tipo_inativacao='         || ie_tipo_inativacao_p || ';' ;


						gravar_agend_integracao(682, ds_param_integ_hl7_w);			
				end if;			
		end if;		
		end if;		
	end if;

	exception
	when others then
      	ie_resUnimed_w := 'N';
	end;

end if;


end gerar_transacao_res;
/
