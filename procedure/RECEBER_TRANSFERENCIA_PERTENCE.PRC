create or replace
procedure receber_transferencia_pertence(
			nr_sequencia_p		number,
			qt_item_recebimento_p	number,
			nr_seq_registro_p	number,
			nm_usuario_p		varchar2,
			cd_setor_atendimento_p	number) is 

			
			
ds_observacao_w		   varchar2(2000);		
ds_especificacao_w	   varchar2(2000);		
nr_seq_pertence_w	   number(10);
dt_exame_w			   date;
nr_sequencia_w		   number(10);
cd_setor_atendimento_w number(10);
nr_seq_armario_w	   number(10);
begin

select	nr_seq_pertence,
	ds_especificacao,
	ds_observacao,
	dt_exame
into	nr_seq_pertence_w,
	ds_especificacao_w,
	ds_observacao_w,
	dt_exame_w
from	pertence_paciente_item
where	nr_sequencia = nr_sequencia_p;


if	(qt_item_recebimento_p > 0) then
	begin
	
	select  pertence_paciente_item_seq.nextval
	into	nr_sequencia_w
	from 	dual;
	
	insert into	pertence_paciente_item(
				nr_sequencia,
				dt_Atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_liberacao,
				nr_seq_pertence,
				ds_especificacao,
				ds_observacao,
				dt_exame,
				qt_item,
				nr_seq_pertence_paciente)
			values(
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_pertence_w,
				ds_especificacao_w,
				ds_observacao_w,
				dt_exame_w,
				qt_item_recebimento_p,
				nr_seq_registro_p);
	
	insert into	pertence_pac_item_evento(
				nr_sequencia,
				nr_seq_pert_pac,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_tipo_evento,
				qt_transacao,
				cd_setor_atendimento)
			values(
				pertence_pac_item_evento_seq.nextval,
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				'R',
				qt_item_recebimento_p,
				cd_setor_atendimento_p);
	end;
	
	update 	pertence_pac_item_evento
	set	   	nm_usuario_receb = nm_usuario_p
	where  	nr_seq_pert_pac = nr_sequencia_p
	and	   	nm_usuario_receb is null
	and	  	cd_tipo_evento = 'T';
	
	select max(cd_setor_atendimento), max(nr_seq_armario)
	into cd_setor_atendimento_w, nr_seq_armario_w
	from pertence_pac_item_evento
	where  	nr_seq_pert_pac = nr_sequencia_p
	and	   	nm_usuario_receb is not null
	and	  	cd_tipo_evento = 'T';
	
	if((cd_setor_atendimento_w is not null) or (nr_seq_armario_w is not null)) then
		update pertence_paciente
		set nr_seq_armario = nr_seq_armario_w,
			cd_setor_atendimento = cd_setor_atendimento_w
		where nr_sequencia = nr_seq_registro_p
		and (cd_setor_atendimento is null or nr_seq_armario is null) ;
	end if;
	
end if;
commit;

end receber_transferencia_pertence;
/
