create or replace
procedure rxt_cancelar_tratamento(nr_sequencia_p		number,
								  nr_Seq_motivo_canc_p number,
								  ds_justificativa_p varchar2,
								  nm_usuario_p varchar2) is 

nr_seq_agenda_w		Number(10);
ie_cancelar_agenda_w	Varchar2(1);
								  
Cursor C01 is
	select	nr_sequencia
	from	rxt_agenda
	where	nr_seq_tratamento = nr_sequencia_p
	and	ie_status_agenda in ('M','V')
	order by 1;
								  
begin

ie_cancelar_agenda_w := obter_valor_param_usuario(3030, 33, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);

update 	RXT_TRATAMENTO 
set 	dt_cancelamento 	= sysdate,
	nm_usuario_cancelamento = nm_usuario_p,
	nr_seq_motivo_canc	= nr_seq_motivo_canc_p,
	ds_just_cancelamento	= ds_justificativa_p
where	nr_sequencia		= nr_sequencia_p;

if (ie_cancelar_agenda_w = 'S') then
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_agenda_w;
	exit when C01%notfound;
		begin
		
		rxt_alterar_status_agenda(nr_seq_agenda_w,'C',nm_usuario_p);
		
		end;
	end loop;
	close C01;
	
end if;

commit;

end rxt_cancelar_tratamento;
/