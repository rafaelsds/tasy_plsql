create or replace
procedure verifica_contr_acompanhante(	nr_controle_p		varchar2,
					ds_mensagem_p	out	varchar2,
					ds_mensagem1_p	out	varchar2) is 

nr_atendimento_w        number(10);
nr_acompanhante_w       number(3);
nm_acompanhante_w	varchar2(40);
ds_msg_w		varchar2(255);
qt_acomp_controle_w	number(10);
ds_msg1_w               varchar2(255);
					
begin
ds_msg_w := '';
ds_msg1_w := wheb_mensagem_pck.get_texto(795933);


if	(nr_controle_p is not null) then

	begin
	
	select	count(*)
	into	qt_acomp_controle_w
	from	atendimento_acompanhante
	where	dt_acompanhante = ( 	select max(dt_acompanhante)
					from   atendimento_acompanhante
					where  nr_controle = nr_controle_p 
					and    dt_saida is null) 
	and   nr_controle = nr_controle_p
	and    dt_saida is null;	
	
	exception
	when others then
	
	qt_acomp_controle_w := 0;
	
	end;

	
	if	(nvl(qt_acomp_controle_w,0) > 0) then
		
		begin
		
		select  nr_acompanhante,
			nvl(nr_atendimento,0), 
			substr(nvl(obter_nome_pf(cd_pessoa_fisica),nm_acompanhante),1,40)
		into	nr_acompanhante_w,
			nr_atendimento_w,
			nm_acompanhante_w
		from    atendimento_acompanhante
		where   dt_acompanhante = ( 	select max(dt_acompanhante)
						from   atendimento_acompanhante
						where  nr_controle = nr_controle_p 
						and    dt_saida is null) 
		and   nr_controle = nr_controle_p
		and    dt_saida is null;

		if	(nvl(nr_atendimento_w,0) > 0) then
			
			if	(nm_acompanhante_w is not null) then
				ds_msg_w := wheb_mensagem_pck.get_texto(795934,
								'NM_ACOMPANHANTE='||nm_acompanhante_w||
								';NR_ATENDIMENTO='||nr_atendimento_w);				             					  					    
			else
				ds_msg_w := wheb_mensagem_pck.get_texto(795935,
								'NR_ATENDIMENTO='||nr_atendimento_w);					     
			end if;
			
		end if;		
		exception
		when others then
		
		ds_msg_w := '';
		
		end;		
	end if;

end if;

ds_mensagem_p := substr(ds_msg_w,1,255);
ds_mensagem1_p := substr(ds_msg1_w,1,255);

commit;

end verifica_contr_acompanhante;
/
