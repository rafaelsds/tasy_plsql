CREATE OR REPLACE
PROCEDURE apagar_indicacao_clinica(
			nr_seq_solic_p		Number,
			nr_seq_indicacao_p	Number,
			nm_usuario_p		Varchar2) IS 

ie_tipo_hemoc_w		varchar2(3);

BEGIN

select	max(ie_tipo_hemoc)
into		ie_tipo_hemoc_w
from		san_indicacao
where	nr_sequencia	= nr_seq_indicacao_p;


delete	from	prescr_sol_bs_indicacao
where	nr_seq_solic_bs = nr_seq_solic_p
and		nr_seq_indicacao = nr_seq_indicacao_p
and		ie_grupo_hemocom = ie_tipo_hemoc_w;

commit;

END apagar_indicacao_clinica;
/