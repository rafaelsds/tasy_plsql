create or replace PROCEDURE Generate_qhapdc_adm_content(

    nr_seq_dataset_p             NUMBER,

    nm_usuario_p                 VARCHAR2,

    returned_value_p OUT nocopy  NUMBER,

    other_exception_p OUT nocopy VARCHAR2)

IS

  ds_segment_w VARCHAR2(32767);

  ds_segment_clob_w CLOB              := empty_clob;

  is_clob_insertion_successful NUMBER := 0;

  excp_caught_dataset_clob_ins VARCHAR2( 500 );

BEGIN

  BEGIN

    SELECT 'N' --Record Identifier

      || Lpad(nr_atendimento, 12, '0') 

      || Lpad(Obter_pessoa_atendimento(nr_atendimento, 'C'), 8, '0')

      || Lpad(nr_atendimento, 12, '0')

      || DECODE(DT_INCIDENT, NULL, '        ' ,  TO_CHAR(dt_entry, 'yyyymmdd'))

      || DECODE(DT_INCIDENT, NULL, '    ' , TO_CHAR(dt_entry_time, 'hhmm'))

      || Lpad(' ',12, ' ')

      || DECODE(ie_chargeable_statue, NULL, '1' ,Lpad(trim(ie_chargeable_statue),1,'1'))

      || DECODE(nr_seq_care_type, NULL, '  ' , Lpad(nr_seq_care_type, 2, '0'))

      || DECODE(CD_BAND, NULL, '8' , Lpad(trim(ie_compensable_status),1,'8'))

      || DECODE(CD_BAND, NULL, '  ' , Lpad(trim(CD_BAND), 2, '0'))

      || DECODE(cd_admission_detail, NULL, '  ' , Lpad(cd_admission_detail, 2, '0'))

	    || DECODE(CD_FACILITY_FROM, NULL, '     ' , Lpad(trim(CD_FACILITY_FROM), 5, '0'))

      || DECODE(nr_hosp_insurance, NULL, '8' , Lpad(trim(nr_hosp_insurance),1,'8'))

      || DECODE(DT_INCIDENT, NULL, '        ' ,  TO_CHAR(dt_discharge, 'yyyymmdd'))

      || DECODE(DT_INCIDENT, NULL, '    ' ,TO_CHAR(dt_discharge_time, 'hhmm'))

      || DECODE(cd_sepration_mode, NULL, '01' ,Lpad(cd_sepration_mode, 2, '0'))

      || DECODE(CD_FACILITY_TO, NULL, '     ' , Lpad(trim(CD_FACILITY_TO), 5, '0'))

      || Lpad(' ', 8, ' ') 

      || DECODE(NR_BABY_ADMIN_WEIG, NULL, '    ' , Lpad(trim(NR_BABY_ADMIN_WEIG), 4, '0')) 

      || DECODE(CD_ADMITING_WARD, NULL, '      ' , Lpad(CD_ADMITING_WARD, 6, ' '))

      || DECODE(CD_ADMITTING_UNIT, NULL, '    ' , Lpad(trim(CD_ADMITTING_UNIT), 4, ' '))

      || DECODE(CD_STNDRD_UNIT_CODE, NULL, '    ' , Lpad(CD_STNDRD_UNIT_CODE, 4, ' '))

      || DECODE(CD_PHYSICIAN_RESP, NULL, '      ' , Lpad(trim(CD_PHYSICIAN_RESP), 6, ' '))

      || DECODE(IE_SAME_DAY, NULL, 'N' , Lpad(trim(IE_SAME_DAY),1,'N'))

      || DECODE(IE_ENCOUNTER_NATURE, NULL, '3' , Lpad(trim(IE_ENCOUNTER_NATURE),1,'3'))

      || DECODE(CD_QUALIFICATION_STATUS, NULL, ' ' , Lpad(trim(CD_QUALIFICATION_STATUS), 1, ' '))

      || DECODE(CD_STNRD_WARD_CODE, NULL, '    ' , Lpad(trim(CD_STNRD_WARD_CODE), 4, ' ')) -- CD_STNRD_WARD_CODE

    --|| lpad(' ',1,' ')

      || DECODE(ie_contract_role,NULL,' ',lpad(TRIM(ie_contract_role),1,' ') )       --IE_CONTRACT_ROLE

      || DECODE(CD_CONTRACT_TYPE, NULL, ' ' , Lpad(trim(CD_CONTRACT_TYPE), 1, ' '))       --CD_CONTRACT_TYPE

      || DECODE(CD_FUNDING_SOURCE, NULL, '  ' , Lpad(trim(CD_FUNDING_SOURCE), 2, '0'))  --CD_FUNDING_SOURCE

      || DECODE(DT_INCIDENT, NULL, '        ' ,  TO_CHAR(DT_INCIDENT, 'yyyymmdd'))  --DT_INCIDENT

      || DECODE(CD_INCIDENT_FLAG, NULL, ' ' , Lpad(trim(CD_INCIDENT_FLAG), 1, ' '))  --CD_INCIDENT_FLAG  

      || 'U'                                                                         --CD_WORKCONER_QUEENSLAND

      || DECODE(CD_MOTOR_ACC_INC, NULL, 'U' , Lpad(trim(CD_MOTOR_ACC_INC), 1, ' '))  --CD_MOTOR_ACC_INC    

      || DECODE(CD_DVA, NULL, 'U' , Lpad(trim(CD_DVA), 1, ' '))  --CD_DVA                        DEpartement of veterans affairs  

      || DECODE(CD_DDC, NULL, 'U' , Lpad(trim(CD_DDC), 1, ' ')) --  CD_DDC,

      || DECODE(NR_LANGUAGE, NULL, '    ' , Lpad(trim(NR_LANGUAGE), 4, ' ')) -- NR_LANGUAGE,

      || DECODE(IE_INTERPRETER_REQ, NULL, ' ' , Lpad(trim(IE_INTERPRETER_REQ), 1, ' ')) -- IE_INTERPRETER_REQ,

      || Lpad(' ',4, ' ') 

      || DECODE(NR_QAS_NUMBER, NULL, '            ' , Lpad(trim(NR_QAS_NUMBER), 12, ' ')) -- NR_QAS_NUMBER,

      || DECODE(NR_PROVIDER_IDENTIFIER, NULL, '     ' , Lpad(trim(NR_PROVIDER_IDENTIFIER), 5, '0')) --  NR_PROVIDER_IDENTIFIER,  

      || Lpad(' ',6, ' ') -- Filler                    

      || DECODE(QT_INTENSE_CARE, NULL, '       ' , Lpad(trim(QT_INTENSE_CARE), 7, '0')) -- QT_INTENSE_CARE,

      || DECODE(QT_VENTILATORY_SUPPORT, NULL, '       ' , Lpad(trim(QT_VENTILATORY_SUPPORT), 7, '0')) -- QT_VENTILATORY_SUPPORT,
      
      || chr(10)

    INTO ds_segment_w

    FROM qhapdc_segment_adm

    WHERE nr_dataset = nr_seq_dataset_p;

  EXCEPTION

  WHEN OTHERS THEN

    ds_segment_w := NULL;

  END;

  SELECT Concat(ds_segment_clob_w, ds_segment_w)

  INTO ds_segment_clob_w

  FROM dual;

  Insert_dataset_content(nm_usuario_p, nr_seq_dataset_p, ds_segment_clob_w, is_clob_insertion_successful, excp_caught_dataset_clob_ins);

  IF( is_clob_insertion_successful = 1 ) THEN

    returned_value_p              :=1;

    other_exception_p             :=NULL;

  ELSE

    IF( excp_caught_dataset_clob_ins IS NOT NULL ) THEN

      returned_value_p               :=0;

      other_exception_p              := excp_caught_dataset_clob_ins;

    END IF;

  END IF;

END generate_qhapdc_adm_content;
/