create or replace
procedure san_reutilizar_hemocomponente(	nr_seq_doacao_p		number,
						nr_seq_producao_p	number,
						qt_volume_bolsa_p	varchar2,
						nm_usuario_p		Varchar2) is	 

qt_volume_atual_w	number(4);
qt_volume_w		number(4);
				
begin

if (nr_seq_doacao_p is not null) and (nr_seq_producao_p is not null) then

	select  to_number(a.qt_volume_atual + qt_volume_bolsa_p)
	into	qt_volume_atual_w
	from 	san_doacao a
	where 	a.nr_sequencia = nr_seq_doacao_p;

	select  to_number(b.qt_volume - qt_volume_bolsa_p)
	into	qt_volume_w
	from 	san_producao b
	where 	b.nr_sequencia = nr_seq_producao_p;


	update 		san_doacao
	set 		qt_volume_atual = qt_volume_atual_w,
			nm_usuario 	= nm_usuario_p
	where 		nr_sequencia 	= nr_seq_doacao_p;

	update 		san_producao
	set 		qt_volume 	= qt_volume_w,
			nm_usuario 	= nm_usuario_p
	where 		nr_sequencia 	= nr_seq_producao_p;
end if;

commit;

end san_reutilizar_hemocomponente;
/