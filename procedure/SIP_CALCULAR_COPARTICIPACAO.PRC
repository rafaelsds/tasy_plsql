create or replace
procedure sip_calcular_coparticipacao
			(	nr_seq_lote_sip_p	Number,
				nm_usuario_p		Varchar2) is 
				
dt_periodo_inicial_w		Date;
dt_periodo_final_w		Date;
ie_ind_familiar_w		Varchar2(1);
ie_coletivo_sem_patro_w		Varchar2(1);
ie_coletivo_com_patro_w		Varchar2(1);
ie_tipo_despesa_w		Varchar2(3);
cd_estrutura_sip_w		Varchar2(40);
ie_tipo_beneficiario_w		Varchar2(3);
ie_tipo_plano_w			Varchar2(3);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
vl_coparticipacao_w		Number(15,2);
ds_estrutura_sip_w		Varchar2(80);
ie_expostos_w			Varchar2(1);
ie_eventos_w			Varchar2(1);
ie_total_despesa_w		Varchar2(1);
ie_coparticipacao_w		Varchar2(1);
ie_seguros_w			Varchar2(1);
ie_tipo_w			Varchar2(1);
nr_seq_classif_sup_w		Number(10);

Cursor C01 is
	/* Ambulatorial */
	select	'A', 
		'X' cd_estrutura_sip,
		ie_tipo_beneficiario,
		ie_tipo_plano,
		cd_procedimento,
		ie_origem_proced,
		nvl(sum(d.vl_coparticipacao),0)
	from 	pls_lote_mensalidade		g,
		pls_mensalidade			f,
		pls_mensalidade_segurado 	e,
		pls_conta_coparticipacao 	d,
		pls_conta_proc   		c,
		pls_conta			b,
		pls_protocolo_conta		a
	where 	a.nr_sequencia			= b.nr_seq_protocolo
	and	b.nr_sequencia  		= c.nr_seq_conta
	and 	c.nr_sequencia  		= d.nr_seq_conta_proc
	and 	d.nr_seq_mensalidade_seg	= e.nr_sequencia
	and	e.nr_seq_mensalidade		= f.nr_sequencia
	and	f.nr_seq_lote			= g.nr_sequencia
	and	a.ie_status			= 3
	and	a.ie_tipo_protocolo		= 'C'
	and	pls_obter_se_internado(b.nr_sequencia, 'C')	= 'N'
	and 	g.dt_mesano_referencia between dt_periodo_inicial_w and dt_periodo_final_w
	and	nvl(b.ie_tipo_segurado,'B')	= 'B'
	group by
		ie_tipo_beneficiario,
		ie_tipo_plano,
		cd_procedimento,
		ie_origem_proced
	union
	/* Procedimentos para interna��es menores de 24 horas*/
	select	'C1',
		'1.04.1' cd_estrutura_sip,
		ie_tipo_beneficiario,
		ie_tipo_plano,
		0 cd_procedimento,
		0 ie_origem_proced,
		nvl(sum(d.vl_coparticipacao),0)
	from 	pls_lote_mensalidade		g,
		pls_mensalidade			f,
		pls_mensalidade_segurado 	e,
		pls_conta_coparticipacao 	d,
		pls_conta			b,
		pls_protocolo_conta		a
	where 	a.nr_sequencia			= b.nr_seq_protocolo
	and	b.nr_sequencia			= d.nr_seq_conta
	and 	d.nr_seq_mensalidade_seg	= e.nr_sequencia
	and	e.nr_seq_mensalidade		= f.nr_sequencia
	and	f.nr_seq_lote			= g.nr_sequencia
	and	a.ie_status			= 3
	and	a.ie_tipo_protocolo		= 'C'
	and	nvl(b.qt_diarias_sip,2)		= 1
	and	pls_obter_se_internado(b.nr_sequencia, 'C')	= 'S'
	and 	g.dt_mesano_referencia between dt_periodo_inicial_w and dt_periodo_final_w
	and	nvl(b.ie_tipo_segurado,'B')	= 'B'
	group by
		ie_tipo_beneficiario,
		ie_tipo_plano
	union
	/* Procedimentos para interna��es maiores do que 24 horas*/
	select	'C2',
		'1.04.2' cd_estrutura_sip,
		ie_tipo_beneficiario,
		ie_tipo_plano,
		0 cd_procedimento,
		0 ie_origem_proced,
		nvl(sum(d.vl_coparticipacao),0)
	from 	pls_lote_mensalidade		g,
		pls_mensalidade			f,
		pls_mensalidade_segurado 	e,
		pls_conta_coparticipacao 	d,
		pls_conta			b,
		pls_protocolo_conta		a
	where 	a.nr_sequencia			= b.nr_seq_protocolo
	and	b.nr_sequencia			= d.nr_seq_conta
	and 	d.nr_seq_mensalidade_seg	= e.nr_sequencia
	and	e.nr_seq_mensalidade		= f.nr_sequencia
	and	f.nr_seq_lote			= g.nr_sequencia
	and	a.ie_status			= 3
	and	a.ie_tipo_protocolo		= 'C'
	and	nvl(b.qt_diarias_sip,2)		<> 1
	and	pls_obter_se_internado(b.nr_sequencia, 'C')	= 'S'
	and 	g.dt_mesano_referencia between dt_periodo_inicial_w and dt_periodo_final_w
	and	nvl(b.ie_tipo_segurado,'B')	= 'B'
	group by
		ie_tipo_beneficiario,
		ie_tipo_plano
	union
	/* Reembolso*/
	select	'RA',
		'X' cd_estrutura_sip,
		ie_tipo_beneficiario,
		ie_tipo_plano,
		cd_procedimento,
		ie_origem_proced,
		nvl(sum(d.vl_coparticipacao),0)
	from	pls_conta_coparticipacao	d,
		pls_conta_proc   		c,
		pls_conta			b,
		pls_protocolo_conta		a
	where	a.nr_sequencia   	= b.nr_seq_protocolo
	and	b.nr_sequencia  	= c.nr_seq_conta
	and 	c.nr_sequencia  	= d.nr_seq_conta_proc
	and	a.ie_status   		= 3
	and	a.ie_tipo_protocolo 	= 'R'
	and	a.dt_mes_competencia between dt_periodo_inicial_w and dt_periodo_final_w
	and	nvl(b.ie_tipo_segurado,'B') = 'B'
	group by ie_tipo_beneficiario,
		ie_tipo_plano,
		cd_procedimento,
		ie_origem_proced;
		
begin

begin
select	dt_periodo_inicial,
	nvl(dt_periodo_final, sysdate),
	ie_ind_familiar,
	ie_coletivo_sem_patroci,
	ie_coletivo_com_patroci
into	dt_periodo_inicial_w,
	dt_periodo_final_w,
	ie_ind_familiar_w,
	ie_coletivo_sem_patro_w,
	ie_coletivo_com_patro_w
from	pls_lote_sip
where	nr_sequencia	= nr_seq_lote_sip_p;	
exception
	when others then
	ie_ind_familiar_w	:= 'N';
	ie_coletivo_sem_patro_w	:= 'N';
	ie_coletivo_com_patro_w	:= 'N';
	wheb_mensagem_pck.exibir_mensagem_abort('Problema na leitura dos dados do lote SIP (' || nr_seq_lote_sip_p || ')');
end;

/*delete from logxxxxx_tasy where cd_log = 20081;*/
open C01;
loop
fetch C01 into	
	ie_tipo_despesa_w,
	cd_estrutura_sip_w,
	ie_tipo_beneficiario_w,
	ie_tipo_plano_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	vl_coparticipacao_w;
exit when C01%notfound;
	begin
	/* Para os ambulatoriais deve ser identificado qual a estrutura do SIP que ir� se encaixar os valores */
	if	(ie_tipo_despesa_w in ('A','RA')) then
		/* Obter a estrutura SIP cadastrada para o procedimento, caso n�o encontre ent�o busca na procedure SIP_OBTER_ESTRUTURA_PROCED */
		select	nvl(max(cd_estrutura_sip),'X')
		into	cd_estrutura_sip_w
		from	sip_anexo_ii_procedimento
		where	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w
		and	nr_seq_lote_sip		= nr_seq_lote_sip_p;
		if	(cd_estrutura_sip_w	= 'X') then
			cd_estrutura_sip_w	:= sip_obter_estrutura_proced(cd_procedimento_w, ie_origem_proced_w, 'C');
		end if;
	end if;
	
	/* Obter dados da estrutura SIP do procedimento */
	begin
	select	nvl(ds_estrutura,''),
		nvl(ie_expostos,'S'),
		nvl(ie_eventos,'S'),
		nvl(ie_total_despesa,'S'),
		nvl(ie_coparticipacao,'S'),
		nvl(ie_seguros,'S'),
		nvl(ie_tipo,'P'),
		nr_seq_classif_sup
	into	ds_estrutura_sip_w,
		ie_expostos_w,
		ie_eventos_w,
		ie_total_despesa_w,
		ie_coparticipacao_w,
		ie_seguros_w,
		ie_tipo_w,
		nr_seq_classif_sup_w
	from	sip_estrutura_proc
	where	ie_situacao	= 'A'
	and	cd_estrutura	= cd_estrutura_sip_w;
	exception
		when others then
		ie_tipo_w	:= 'X';
	end;
	
	/* Gravar os dados lidos */
	sip_gravar_item_despesa(null, null, null,
			cd_estrutura_sip_w, 0, 0,
			0, vl_coparticipacao_w, 0,
			nr_seq_lote_sip_p, ie_tipo_plano_w, ds_estrutura_sip_w,
			ie_tipo_beneficiario_w, ie_expostos_w, ie_eventos_w,
			ie_total_despesa_w, ie_coparticipacao_w, ie_seguros_w,
			0, ie_tipo_despesa_w, nm_usuario_p, 
			0, 0, 0);
	/* Log de progresso da gera��o dos dados do SIP*/
	/*insert into logxxxxx_tasy 
		(dt_atualizacao, nm_usuario, cd_log,
		ds_log)       
	values (sysdate, nm_usuario_p, 20081,
		ie_tipo_plano_w || ' - ' || ie_tipo_beneficiario_w);*/
	commit;
	end;
end loop;
close C01;

end sip_calcular_coparticipacao;
/