create or replace procedure carrega_nf_integ_pat_equip(	nr_seq_nota_p		number,
				dt_inicio_p		date,
				dt_final_p			date,
				nm_usuario_p		Varchar2,
				ie_replica_qt_item_p	varchar2) is

qt_existe_w		number(10);
ie_desdobrar_w		varchar2(1);

cursor c01 is
select	a.nr_sequencia,
	b.nr_item_nf,
	substr(obter_regra_nf_pat_equip(b.cd_material,nvl(b.cd_local_estoque,0),nvl(b.cd_conta_contabil,'0'),a.cd_estabelecimento,'E', 'E', obter_valor_item_regra(b.nr_sequencia, b.nr_item_nf), a.dt_entrada_saida),1,1) ie_equipamento,
	substr(obter_regra_nf_pat_equip(b.cd_material,nvl(b.cd_local_estoque,0),nvl(b.cd_conta_contabil,'0'),a.cd_estabelecimento,'P', 'E', obter_valor_item_regra(b.nr_sequencia, b.nr_item_nf), a.dt_entrada_saida),1,1) ie_patrimonio,
	substr(obter_regra_nf_pat_equip(b.cd_material,nvl(b.cd_local_estoque,0),nvl(b.cd_conta_contabil,'0'),a.cd_estabelecimento,'P','D', obter_valor_item_regra(b.nr_sequencia, b.nr_item_nf), a.dt_entrada_saida),1,1) ie_desdobrar_itens,
	b.qt_item_nf
from	nota_fiscal_item b,
	nota_fiscal a
where	a.nr_sequencia = b.nr_sequencia
and	a.ie_situacao = 1
and	obter_entrada_saida_nf(a.nr_sequencia) = 'E'
and	a.dt_atualizacao_estoque is not null
and	(((nr_seq_nota_p is not null) and (a.nr_sequencia = nr_seq_nota_p)) or
	(a.dt_entrada_saida between dt_inicio_p and fim_dia(dt_final_p)))
and	((obter_regra_nf_pat_equip(b.cd_material,nvl(b.cd_local_estoque,0),nvl(b.cd_conta_contabil,'0'),a.cd_estabelecimento,'E', 'E', obter_valor_item_regra(b.nr_sequencia, b.nr_item_nf), a.dt_entrada_saida) = 'S') or
	(obter_regra_nf_pat_equip(b.cd_material,nvl(b.cd_local_estoque,0),nvl(b.cd_conta_contabil,'0'),a.cd_estabelecimento,'P', 'E', obter_valor_item_regra(b.nr_sequencia, b.nr_item_nf), a.dt_entrada_saida) = 'S'))
and	not exists (
		select	1
		from	nf_integracao_pat_equip x
		where	x.nr_seq_nota = a.nr_sequencia
		and	x.nr_seq_item = b.nr_item_nf
		and	x.ie_equipamento= substr(obter_regra_nf_pat_equip(b.cd_material,nvl(b.cd_local_estoque,0),nvl(b.cd_conta_contabil,'0'),a.cd_estabelecimento,'E', 'E', obter_valor_item_regra(b.nr_sequencia, b.nr_item_nf), a.dt_entrada_saida),1,1)
		and	x.ie_patrimonio	= substr(obter_regra_nf_pat_equip(b.cd_material,nvl(b.cd_local_estoque,0),nvl(b.cd_conta_contabil,'0'),a.cd_estabelecimento,'P', 'E', obter_valor_item_regra(b.nr_sequencia, b.nr_item_nf), a.dt_entrada_saida),1,1))
order by	a.nr_sequencia,
	b.nr_item_nf;

c01_w		c01%rowtype;

begin

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;
	begin

	if	(ie_replica_qt_item_p = 'N') then
		ie_desdobrar_w := 'N';
	elsif	(ie_replica_qt_item_p = 'D') then
		ie_desdobrar_w := c01_w.ie_desdobrar_itens;
	elsif	(ie_replica_qt_item_p = 'S') then
		ie_desdobrar_w := 'S';
	end if;

	if	(nvl(ie_desdobrar_w,'N') = 'N') then
		begin
		select	count(*)
		into	qt_existe_w
		from	nf_integracao_pat_equip
		where	nr_seq_nota	= c01_w.nr_sequencia
		and	nr_seq_item	= c01_w.nr_item_nf;

		if	(qt_existe_w = 0) then
			insert into nf_integracao_pat_equip(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_nota,
				nr_seq_item,
				ie_equipamento,
				ie_patrimonio,
				ie_desdobrar_itens)
			values(	nf_integracao_pat_equip_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				c01_w.nr_sequencia,
				c01_w.nr_item_nf,
				c01_w.ie_equipamento,
				c01_w.ie_patrimonio,
				ie_desdobrar_w);
		else
			update	nf_integracao_pat_equip
			set	ie_equipamento	= c01_w.ie_equipamento,
				ie_patrimonio	= c01_w.ie_patrimonio
			where	nr_seq_nota	= c01_w.nr_sequencia
			and	nr_seq_item	= c01_w.nr_item_nf;
		end if;
		end;
	elsif	(nvl(ie_desdobrar_w,'N') in ('S','U')) then
		begin

		if	(nvl(ie_desdobrar_w,'N') = 'U') then
			begin
			c01_w.qt_item_nf := 1;
			end;
		end if;

		for i in 1..c01_w.qt_item_nf loop
			begin
			insert into nf_integracao_pat_equip(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_nota,
					nr_seq_item,
					ie_equipamento,
					ie_patrimonio,
					ie_desdobrar_itens)
				values(	nf_integracao_pat_equip_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					c01_w.nr_sequencia,
					c01_w.nr_item_nf,
					c01_w.ie_equipamento,
					c01_w.ie_patrimonio,
					ie_desdobrar_w);
			end;
		end loop;
		end;
	end if;
	end;
end loop;
close C01;

commit;

end carrega_nf_integ_pat_equip;
/
