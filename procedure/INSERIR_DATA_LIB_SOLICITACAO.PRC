create or replace
procedure inserir_data_lib_solicitacao(nr_seq_solic_p	number) is    

    qt_existe_transfusao_w	number(10);
    q_existe_integracao_w	number(10);    
begin     

    UPDATE  san_reserva  
    SET     dt_liberacao_solicitacao = sysdate,
            ie_status = 'L'
    where	  nr_sequencia = nr_seq_solic_p;



    if 	(nvl(nr_seq_solic_p,0) > 0) then 

        select  count(*)    
        into    qt_existe_transfusao_w 
        from    san_transfusao   
	      where   nr_seq_reserva = nr_seq_solic_p; 

	      if	(nvl(qt_existe_transfusao_w,0) > 0) then     
		        
            update  san_reserva   
		        set     ie_status = 'T'   
		        where   nr_sequencia = nr_seq_solic_p; 

	      end if;

        --in�cio amcom - verifica se existe integra��o e chama a nova procedure   RF 02 11/06/2015 
        select  count(1)
	      into    q_existe_integracao_w
	      from    san_reserva_integracao
	      where   nr_seq_reserva = nr_seq_solic_p;

	      if	(nvl(q_existe_integracao_w,0) > 0) then

		        san_gerar_itens_integracao(nr_seq_solic_p,
				                               wheb_usuario_pck.get_nm_usuario);

	      end if;
	      --fim amcom

    end if;  

    commit;  

end inserir_data_lib_solicitacao;
/
