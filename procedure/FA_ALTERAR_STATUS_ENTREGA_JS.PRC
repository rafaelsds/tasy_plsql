create or replace 
procedure fa_alterar_status_entrega_js(
		nr_sequencia_p		number,
		nr_seq_entrega_p	number,
		nr_seq_entrega_item_p	number,
		ie_status_medicacao_p	varchar2,
		ie_separacao_medic_p out varchar2) is
		
begin

	fa_alterar_status_entrega(nr_seq_entrega_p, nr_seq_entrega_item_p, ie_status_medicacao_p);
	
	ie_separacao_medic_p := obter_se_separacao_medic(nr_sequencia_p);

end fa_alterar_status_entrega_js;
/