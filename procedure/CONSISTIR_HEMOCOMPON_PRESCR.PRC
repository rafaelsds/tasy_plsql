create or replace
procedure consistir_hemocompon_prescr(
			nr_prescricao_p		number,
			cd_perfil_p		number,
			nm_usuario_p		Varchar2) is 
nr_sequencia_w	number(10);
ds_erro_w	varchar2(2000);

Cursor C01 is
select	nr_sequencia
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and 	nr_seq_solic_sangue is not null;
begin
open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	consistir_prescr_procedimento(nr_prescricao_p, nr_sequencia_w, nm_usuario_p, cd_perfil_p, ds_erro_w);
end loop;
close C01;

commit;

end consistir_hemocompon_prescr;
/