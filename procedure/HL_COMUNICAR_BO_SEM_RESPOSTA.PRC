create or replace
procedure HL_comunicar_bo_sem_resposta(nm_usuario_p		Varchar2) is

nr_seq_bo_w			number(10);
qt_dia_w				number(15,2);
nm_usuario_destino_w		varchar2(2000);
ds_email_destino_w			varchar2(2000);
ds_responsaveis_w			varchar2(2000);
nr_seq_resp_w			number(10,0);
cd_setor_resp_w			number(5);
nm_usuario_setor_w		varchar2(15);
ds_comunicado_w			varchar2(4000);
ds_responsavel_w			varchar2(40);
nr_seq_comunicacao_w		number(10,0);
nr_seq_classif_w			number(10,0);
ds_setor_atendimento_w		varchar2(100);
nm_usuario_pessoa_w		varchar2(15);
nr_seq_estrut_sup_w		number(10) := 0;
nr_seq_estrut_sup2_w		number(10) := 0;
nr_seq_estrut_sup3_w		number(10) := 0;
nm_usuario_estrut2_w		varchar2(15);
nm_usuario_estrut3_w		varchar2(15);
nm_usuario_resp_w 		varchar2(15);
nm_usuario_estrutura_w		varchar(2000);
ie_gerar_w			varchar2(1) := 'N';
ie_nivel_w			number(5);
ie_nivel2_w			number(5);
qt_dias_classif_w			number(5);
--qt_dias_parametro_w		varchar2(25);
cd_estabelecimento_w		number(4);
nr_seq_bol_envio_w		number(10);
nr_seq_estrut_org_usu_w		number(10);

Cursor C01 is
	select	a.nr_sequencia,
		b.nr_sequencia,
		obter_qt_dia_util_periodo(b.dt_resposta, sysdate, a.cd_estabelecimento) qt_dia,
		a.cd_estabelecimento
	from	sac_boletim_ocorrencia a,
		sac_resp_bol_ocor b
	where	a.nr_sequencia = b.nr_seq_bo
	and	b.nr_sequencia = (	select	max(x.nr_sequencia)
				from	sac_resp_bol_ocor x
				where	x.ie_status = 'N'
				and	x.nr_Seq_bo = a.nr_sequencia)
	order by 1;

Cursor c02 is -- Usu�rios respons�veis
	select	distinct a.nm_usuario_resp,
		b.nr_seq_classif,
		substr(obter_descricao_padrao('SAC_RESPONSAVEL', 'DS_RESPONSAVEL', b.nr_seq_responsavel),1,100),
		a.nr_seq_estrut_org_usu
	from	sac_boletim_ocorrencia c,
		sac_resp_bol_ocor b,
		sac_resp_usuario a
	where	a.nr_seq_resp	= b.nr_seq_responsavel
	and	c.nr_sequencia	= b.nr_seq_bo
	and	b.nr_sequencia	= nr_seq_resp_w
	and	exists	(select	1
			from	usuario_estabelecimento x
			where	x.nm_usuario_param	= a.nm_usuario_resp
			and	x.cd_estabelecimento	= c.cd_estabelecimento
			union
			select	1
			from	usuario y
			where	y.nm_usuario		= a.nm_usuario_resp
			and	y.cd_estabelecimento	= c.cd_estabelecimento);

begin
--obter_param_usuario(87, 68, obter_perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, qt_dias_parametro_w);

open C01;
loop
fetch C01 into
		nr_seq_bo_w,
		nr_seq_resp_w,
		qt_dia_w,
		cd_estabelecimento_w;
exit when C01%notfound;
	begin
	nm_usuario_destino_w	:= '';
	ds_email_destino_w	:= '';

	open C02;
	loop
	fetch C02 into
		nm_usuario_resp_w,
		nr_seq_classif_w,
		ds_responsavel_w,
		nr_seq_estrut_org_usu_w;
	exit when C02%notfound;
		begin
		select	qt_dia_prev_solucao
		into	qt_dias_classif_w
		from	sac_classif_ocorrencia
		where	nr_sequencia = nr_seq_classif_w;

		if	(nvl(nm_usuario_resp_w,'X') <> 'X') then
			begin
			if ((nvl(qt_dias_classif_w,0) <> 0) and
				(nvl(qt_dia_w,0) >=  nvl(qt_dias_classif_w,0) + 1)) then
				begin
				if	(nm_usuario_resp_w <> nm_usuario_p) and
					(nvl(instr(nm_usuario_destino_w,nm_usuario_resp_w),0) = 0) then
					nm_usuario_destino_w := nm_usuario_destino_w || nm_usuario_resp_w || ',';
					ie_gerar_w := 'S';
				end if;
				end;
			end if;
			if	((nvl(qt_dias_classif_w,0)<> 0) and
				nvl(qt_dia_w,0) >= (nvl(qt_dias_classif_w,0) + 4)) then
				begin
				select	max(nr_seq_superior)
				into	nr_seq_estrut_sup_w
				from	sac_estrut_org_usuario
				where	nm_usuario_estrut 	= nm_usuario_resp_w
				and	nr_sequencia	= nvl(nr_seq_estrut_org_usu_w,nr_sequencia);

				if (nvl(nr_seq_estrut_sup_w,0) <> 0) then
					begin
					select	max(1) ie_nivel,
						max(nm_usuario_estrut),
						max(nr_seq_superior)
					into	ie_nivel_w,
						nm_usuario_estrut2_w,
						nr_seq_estrut_Sup2_w
					from	sac_estrut_org_usuario
					where	nr_sequencia = nr_seq_estrut_sup_w;
					end;
				end if;

				if	((nvl(ie_nivel_w,0) <> 0) and
					(nvl(nm_usuario_estrut2_w,'X') <> 'X' )and
					(nvl(qt_dia_w,0) >= (nvl(qt_dias_classif_w,0) + 5))) then
					if	(nm_usuario_estrut2_w <> nm_usuario_p) and
						(nvl(instr(nm_usuario_destino_w,nm_usuario_estrut2_w),0) = 0) then
							nm_usuario_destino_w := nm_usuario_destino_w || nm_usuario_estrut2_w ||',';
					end if;
				end if;

				if	(nvl(nr_seq_estrut_Sup2_w,0) <> 0) then
					begin
					select	max(2) ie_nivel_2,
						max(nm_usuario_estrut),
						max(nr_seq_superior)
					into	ie_nivel2_w,
						nm_usuario_estrut3_w,
						nr_seq_estrut_Sup3_w
					from	sac_estrut_org_usuario
					where	nr_sequencia = nr_seq_estrut_Sup2_w;
					end;
				end if;

				if	((nvl(ie_nivel_w,0) <> 0) and(nvl(nm_usuario_estrut3_w,'X') <> 'X' )) then
					if	(nm_usuario_estrut3_w <> nm_usuario_p) and
						(nvl(instr(nm_usuario_destino_w,nm_usuario_estrut3_w),0) = 0) then
						nm_usuario_destino_w := nm_usuario_destino_w || nm_usuario_estrut3_w ||',';
					end if;
				end if;
				ie_gerar_w := 'S';
				end;
			end if;

			end;

		end if;
		end;
	end loop;
	close C02;

	select	comunic_interna_seq.nextval
	into	nr_seq_comunicacao_w
	from	dual;

	select  sac_bol_ocor_envio_seq.nextval
	into	nr_seq_bol_envio_w
	from	dual;

	if   (nvl(nm_usuario_destino_w,'X') <> 'X')then
	Insert into SAC_BOL_OCOR_ENVIO (
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		dt_envio,
		nr_seq_bo,
		ds_destino,
		ds_observacao) values (
		nr_seq_bol_envio_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		sysdate,
		nr_seq_bo_w,
		substr(nm_usuario_destino_w,1,255),
		'Comunicado de BO sem resposta.');
	end if;


	ds_comunicado_w := 'O BO ' || ' ' || nr_seq_bo_w || chr(13) || chr(10) ||
							'N�o foi respondido dentro do prazo.' || chr(13)||chr(10) ||
							'Respons�vel do Bo :' || ' ' || ds_responsavel_w || chr(13) || chr(10) ||
							'Tempo sem resposta em dias : ' || ' ' || qt_dia_w ;

	if (ie_gerar_w = 'S')and (nvl(nm_usuario_destino_w,'X') <> 'X') then
		begin
		insert into comunic_interna(	dt_comunicado,
					ds_titulo,
					ds_comunicado,
					nm_usuario,
					dt_atualizacao,
					ie_geral,
					nm_usuario_destino,
					nr_sequencia,
					ie_gerencial,
					dt_liberacao,
					cd_estab_destino)
				values(	sysdate,
					'Comunicado de BO sem resposta.',
					ds_comunicado_w,
					nm_usuario_p,
					sysdate,
					'N',
					nm_usuario_destino_w,
					nr_seq_comunicacao_w,
					'N',
					sysdate,
					cd_estabelecimento_w);
		end;
	end if;

	end;
end loop;
close C01;

commit;

end HL_comunicar_bo_sem_resposta;
/
