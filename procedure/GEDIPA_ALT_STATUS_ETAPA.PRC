create or replace
procedure gedipa_alt_status_etapa(	nr_seq_etapa_hor_p	number,
					status_p 		varchar2,
					cd_estabelecimento_p 	number,
					nm_usuario_p 		varchar2) is
					
nr_seq_etapa_gedipa_w number(10) := 0;
ie_status_anterior_w varchar(1);
nr_seq_estagio_w number(10);
nr_horas_estab_w number(14,5);
dt_estabilidade_w date;
cd_material_w number(10);

begin

select 	ie_status_preparo,
	cd_material,
	dt_estabilidade
into 	ie_status_anterior_w,
	cd_material_w,
	dt_estabilidade_w
from 	gedipa_etapa_hor
where 	nr_sequencia = nr_seq_etapa_hor_p;

if (status_p = 'R') and (ie_status_anterior_w <> status_p) then -- caso tenha passado para preparado, seta dt_estabilidade
	begin
	Obter_Param_Usuario(3112,68,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,nr_seq_estagio_w); 
	
	if	(nr_seq_estagio_w is not null) and
		(nr_seq_estagio_w > 0) then
		begin
		
		select	max(converte_estab_hor(ie_tempo_estab, qt_estabilidade))
		into	nr_horas_estab_w
		from	material_armazenamento
		where	nr_seq_estagio = nr_seq_estagio_w
		and	cd_material	= cd_material_w;
		
		dt_estabilidade_w := sysdate + (nr_horas_estab_w/24);
		
		end;	
	end if;
	end;
end if;

gravar_log_tasy(212,
substr('{'||chr(10)||
'"nr_seq_etapa_hor_p" : "'||nr_seq_etapa_hor_p||'",'||chr(10)||
'"status_p" : "'||status_p||'"}'||chr(10)||
'ds_stack : '||substr(dbms_utility.format_call_stack,1,1500),1,2000), nm_usuario_p);

update 	gedipa_etapa_hor
set 	ie_status_preparo = status_p,
		dt_estabilidade = dt_estabilidade_w,
		nm_usuario_preparo = nm_usuario_p
where 	nr_sequencia = nr_seq_etapa_hor_p;	

select 	max(nr_seq_etapa_gedipa)
into 	nr_seq_etapa_gedipa_w
from 	gedipa_etapa_hor
where 	nr_sequencia = nr_seq_etapa_hor_p;

gedipa_atualizar_status_etapa(nr_seq_etapa_gedipa_w, cd_estabelecimento_p, nm_usuario_p);

commit;

end gedipa_alt_status_etapa;
/
