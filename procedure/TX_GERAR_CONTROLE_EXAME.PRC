Create or replace
procedure tx_gerar_controle_exame	(cd_pessoa_fisica_p	varchar2,
					nr_seq_protocolo_p	number,
					dt_referencia_p		date,
					ds_itens_p		varchar2,
					ie_atualizar_todos_p	varchar2,	
					nm_usuario_p		varchar2) is


/*Ordem: 
1 -> Titulo
5 -> Data
*/ 
					
/* vetor */
type colunas is record (nm_coluna_w varchar2(100));
type vetor is table of colunas index by binary_integer;


dt_referencia_w		date;				
/* globais */
vetor_w			vetor;
vetor1_w		vetor;
ds_item_w		varchar2(2000);
ds_item_ant_w		varchar2(2000);
ivet			integer;
ind			integer;
qt_itens_w		number(5);
nr_seq_nova_w		number(10);
dt_referencia_ant_w	date;
ds_formula_w		varchar2(2000);
ds_cores_w		varchar2(2000);
ds_cor_item_w		varchar2(20);
ds_cor_w		varchar2(20);
nr_seq_exame_w		number(10);
nr_seq_apresent_w	number(10);
nr_sequencia_w		number(10);
qt_casas_decimais_w	number(10);
		
cursor c01 is
	select	ds_item
	from	tx_item_controle_exame
	where	rownum < 50
	and	((nr_seq_protocolo_p is null) or (Tx_obter_se_item_protocolo(nr_seq_protocolo_p,nr_sequencia) = 'S'))
	and	((ds_itens_p is null) or (OBTER_SE_CONTIDO(nr_sequencia,ds_itens_p) = 'S'))
	order by nr_seq_apresent, nr_sequencia;


/* atualiza os antigos */
cursor	c03 is
	select	distinct
		dt_referencia
	from	tx_EXAME_GRID_CONTROLE
	where	nm_usuario = nm_usuario_p
	and	ie_ordem = 5;
	
cursor 	c02 is
	select	ds_formula,
		nr_seq_exame,
		nr_seq_apresent,
		ds_cor,
		nvl(qt_casas_decimais,2)		
	from	tx_item_controle_exame
	where	rownum < 50
	and	((nr_seq_protocolo_p is null) or (Tx_obter_se_item_protocolo(nr_seq_protocolo_p,nr_sequencia) = 'S'))
	and	((ds_itens_p is null) or (OBTER_SE_CONTIDO(nr_sequencia,ds_itens_p) = 'S'))	
	order by nr_seq_apresent, nr_sequencia;
	

begin


dt_referencia_w	:= trunc(dt_referencia_p,'month');

delete 	TX_EXAME_GRID_CONTROLE
where	ie_ordem = 1
and	nm_usuario = nm_usuario_p;

ivet	:= 0;
begin
open	c01;
loop
fetch c01 into
	ds_item_w;
exit	when c01%notfound;
	begin
	ivet := ivet + 1;
	vetor_w(ivet).nm_coluna_w 	  := ds_item_w;
	end;
end loop;
close c01;
end;

ind := ivet;
while	(ind < 50) loop
	begin
	ind := ind + 1;
	vetor_w(ind).nm_coluna_w := null;
	end;
end loop;

select	TX_EXAME_GRID_CONTROLE_seq.nextval
into	nr_sequencia_w
from	dual;

insert into TX_EXAME_GRID_CONTROLE(
	nr_sequencia,           
	nm_usuario,
	ie_ordem,
	dt_referencia,
	cd_pessoa_fisica,
	ds_resultado1,
	ds_resultado2,
	ds_resultado3,
	ds_resultado4,
	ds_resultado5,
	ds_resultado6,
	ds_resultado7,
	ds_resultado8,
	ds_resultado9,
	ds_resultado10,
	ds_resultado11,
	ds_resultado12,
	ds_resultado13,
	ds_resultado14,
	ds_resultado15,
	ds_resultado16,
	ds_resultado17,
	ds_resultado18,
	ds_resultado19,
	ds_resultado20,
	ds_resultado21,
	ds_resultado22,
	ds_resultado23,
	ds_resultado24,
	ds_resultado25,
	ds_resultado26,
	ds_resultado27,
	ds_resultado28,
	ds_resultado29,
	ds_resultado30,
	ds_resultado31,
	ds_resultado32,
	ds_resultado33,
	ds_resultado34,
	ds_resultado35,
	ds_resultado36,
	ds_resultado37,
	ds_resultado38,
	ds_resultado39,
	ds_resultado40,
	ds_resultado41,
	ds_resultado42,
	ds_resultado43,
	ds_resultado44,
	ds_resultado45,
	ds_resultado46,
	ds_resultado47,
	ds_resultado48,
	ds_resultado49,
	ds_resultado50,
	dt_atualizacao)
Values( nr_sequencia_w,
	nm_usuario_p,
	1,
	dt_referencia_w,
	cd_pessoa_fisica_p,
	vetor_w(1).nm_coluna_w,
	vetor_w(2).nm_coluna_w,
	vetor_w(3).nm_coluna_w,
	vetor_w(4).nm_coluna_w,
	vetor_w(5).nm_coluna_w,
	vetor_w(6).nm_coluna_w,
	vetor_w(7).nm_coluna_w,
	vetor_w(8).nm_coluna_w,
	vetor_w(9).nm_coluna_w,
	vetor_w(10).nm_coluna_w,
	vetor_w(11).nm_coluna_w,
	vetor_w(12).nm_coluna_w,
	vetor_w(13).nm_coluna_w,
	vetor_w(14).nm_coluna_w,
	vetor_w(15).nm_coluna_w,
	vetor_w(16).nm_coluna_w,
	vetor_w(17).nm_coluna_w,
	vetor_w(18).nm_coluna_w,
	vetor_w(19).nm_coluna_w,
	vetor_w(20).nm_coluna_w,
	vetor_w(21).nm_coluna_w,
	vetor_w(22).nm_coluna_w,
	vetor_w(23).nm_coluna_w,
	vetor_w(24).nm_coluna_w,
	vetor_w(25).nm_coluna_w,
	vetor_w(26).nm_coluna_w,
	vetor_w(27).nm_coluna_w,
	vetor_w(28).nm_coluna_w,
	vetor_w(29).nm_coluna_w,
	vetor_w(30).nm_coluna_w,
	vetor_w(31).nm_coluna_w,
	vetor_w(32).nm_coluna_w,
	vetor_w(33).nm_coluna_w,
	vetor_w(34).nm_coluna_w,
	vetor_w(35).nm_coluna_w,
	vetor_w(36).nm_coluna_w,
	vetor_w(37).nm_coluna_w,
	vetor_w(38).nm_coluna_w,
	vetor_w(39).nm_coluna_w,
	vetor_w(40).nm_coluna_w,
	vetor_w(41).nm_coluna_w,
	vetor_w(42).nm_coluna_w,
	vetor_w(43).nm_coluna_w,
	vetor_w(44).nm_coluna_w,
	vetor_w(45).nm_coluna_w,
	vetor_w(46).nm_coluna_w,
	vetor_w(47).nm_coluna_w,
	vetor_w(48).nm_coluna_w,
	vetor_w(49).nm_coluna_w,
	vetor_w(50).nm_coluna_w,
	sysdate);

	
open	c03;
loop
fetch c03 into
	dt_referencia_ant_w;
exit	when c03%notfound;
	begin
	
	select 	TX_EXAME_GRID_CONTROLE_seq.nextval
	into	nr_seq_nova_w
	from 	dual;
	
	insert into TX_EXAME_GRID_CONTROLE(
		nr_sequencia,           
		nm_usuario,
		ie_ordem,
		dt_referencia,
		cd_pessoa_fisica,
		dt_atualizacao)
	Values( nr_seq_nova_w,
		nm_usuario_p,
		5,
		dt_referencia_ant_w,
		cd_pessoa_fisica_p,
		sysdate);
	
	ind 		:= 0;
	ds_cores_w	:= null;
	open	c02;
	loop
	fetch c02 into
		ds_formula_w,
		nr_seq_exame_w,
		nr_seq_apresent_w,
		ds_cor_item_w,
		qt_casas_decimais_w;
	exit	when c02%notfound;
		begin
		
		ds_item_w	:= null;
		
		if	(nr_seq_exame_w is not null) then
			
			ds_item_w	:= tx_obter_ult_result_data_lab(cd_pessoa_fisica_p, nr_seq_nova_w, nr_seq_exame_w); --andrey
			ds_item_ant_w	:= ds_item_w;

			begin
			
			select	campo_mascara_virgula_casas(to_number(ds_item_w), qt_casas_decimais_w)
			into	ds_item_w
			from	dual;	
			
			exception
				when others then
				ds_item_w := ds_item_ant_w;
			end;
			
			--ds_item_w := ds_item_w||'+'||vetor1_w(ind).qt_casas_decimais_w;

		
		elsif	(ds_formula_w is not null) then
		
			ds_item_w	:= 	tx_obter_formula(cd_pessoa_fisica_p, nr_seq_nova_w, ds_formula_w,qt_casas_decimais_w); --andrey
			
			--ds_item_w := ds_item_w||'*';
		
		end if;
		
		ind := ind + 1;
		vetor1_w(ind).nm_coluna_w := ds_item_w;
		
		begin
		select	decode(length(to_char(ind)),1,'0'||to_char(ind),to_char(ind))
		into	ds_cor_w
		from	dual;
			
		ds_cores_w	:= ds_cores_w || ds_cor_w || ds_cor_item_w ||',';
		
		end;
		
		end;
	end loop;
	close c02;
	
	ind := ivet;
	while	(ind < 50) loop
		begin
		ind := ind + 1;
		vetor1_w(ind).nm_coluna_w := null;
		end;
	end loop;
	
	UPDATE TX_EXAME_GRID_CONTROLE
	SET	ds_cor		= ds_cores_w,
		ds_resultado1	= vetor1_w(1).nm_coluna_w,
		ds_resultado2	= vetor1_w(2).nm_coluna_w,
		ds_resultado3	= vetor1_w(3).nm_coluna_w,
		ds_resultado4	= vetor1_w(4).nm_coluna_w,
		ds_resultado5	= vetor1_w(5).nm_coluna_w,
		ds_resultado6	= vetor1_w(6).nm_coluna_w,
		ds_resultado7	= vetor1_w(7).nm_coluna_w,
		ds_resultado8	= vetor1_w(8).nm_coluna_w,
		ds_resultado9	= vetor1_w(9).nm_coluna_w,
		ds_resultado10	= vetor1_w(10).nm_coluna_w,
		ds_resultado11	= vetor1_w(11).nm_coluna_w,
		ds_resultado12	= vetor1_w(12).nm_coluna_w,
		ds_resultado13	= vetor1_w(13).nm_coluna_w,
		ds_resultado14	= vetor1_w(14).nm_coluna_w,
		ds_resultado15	= vetor1_w(15).nm_coluna_w,
		ds_resultado16	= vetor1_w(16).nm_coluna_w,
		ds_resultado17	= vetor1_w(17).nm_coluna_w,
		ds_resultado18	= vetor1_w(18).nm_coluna_w,
		ds_resultado19	= vetor1_w(19).nm_coluna_w,
		ds_resultado20	= vetor1_w(20).nm_coluna_w,
		ds_resultado21	= vetor1_w(21).nm_coluna_w,
		ds_resultado22	= vetor1_w(22).nm_coluna_w,
		ds_resultado23	= vetor1_w(23).nm_coluna_w,
		ds_resultado24	= vetor1_w(24).nm_coluna_w,
		ds_resultado25	= vetor1_w(25).nm_coluna_w,
		ds_resultado26	= vetor1_w(26).nm_coluna_w,
		ds_resultado27	= vetor1_w(27).nm_coluna_w,
		ds_resultado28	= vetor1_w(28).nm_coluna_w,
		ds_resultado29	= vetor1_w(29).nm_coluna_w,
		ds_resultado30	= vetor1_w(30).nm_coluna_w,
		ds_resultado31	= vetor1_w(31).nm_coluna_w,
		ds_resultado32	= vetor1_w(32).nm_coluna_w,
		ds_resultado33	= vetor1_w(33).nm_coluna_w,
		ds_resultado34	= vetor1_w(34).nm_coluna_w,
		ds_resultado35	= vetor1_w(35).nm_coluna_w,
		ds_resultado36	= vetor1_w(36).nm_coluna_w,
		ds_resultado37	= vetor1_w(37).nm_coluna_w,
		ds_resultado38	= vetor1_w(38).nm_coluna_w,
		ds_resultado39	= vetor1_w(39).nm_coluna_w,
		ds_resultado40	= vetor1_w(40).nm_coluna_w,
		ds_resultado41	= vetor1_w(41).nm_coluna_w,
		ds_resultado42	= vetor1_w(42).nm_coluna_w,
		ds_resultado43	= vetor1_w(43).nm_coluna_w,
		ds_resultado44	= vetor1_w(44).nm_coluna_w,
		ds_resultado45	= vetor1_w(45).nm_coluna_w,
		ds_resultado46	= vetor1_w(46).nm_coluna_w,
		ds_resultado47	= vetor1_w(47).nm_coluna_w,
		ds_resultado48	= vetor1_w(48).nm_coluna_w,
		ds_resultado49	= vetor1_w(49).nm_coluna_w,
		ds_resultado50	= vetor1_w(50).nm_coluna_w
	WHERE	NR_SEQUENCIA = nr_seq_nova_w;
			
	delete	TX_EXAME_GRID_CONTROLE
	where	dt_referencia = dt_referencia_ant_w
	and	nm_usuario = nm_usuario_p
	and	ie_ordem = 5
	and	NR_SEQUENCIA <> nr_seq_nova_w
	and	ie_atualizar_todos_p = 'S';
	
	delete	TX_EXAME_GRID_CONTROLE
	where	dt_referencia = dt_referencia_ant_w
	and	nm_usuario = nm_usuario_p
	and	ie_ordem = 5
	and	NR_SEQUENCIA <> nr_seq_nova_w
	and	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	ie_atualizar_todos_p = 'N';
		
	end;
end loop;
close c03;

select 	nvl(count(*),0)
into	qt_itens_w
from	TX_EXAME_GRID_CONTROLE
where	trunc(dt_referencia) = trunc(dt_referencia_w)
and	cd_pessoa_fisica = cd_pessoa_fisica_p
and	nm_usuario = nm_usuario_p
and	ie_ordem = 5;

if	(qt_itens_w = 0) then

	select 	TX_EXAME_GRID_CONTROLE_seq.nextval
	into	nr_seq_nova_w
	from 	dual;
	
	insert into TX_EXAME_GRID_CONTROLE(
		nr_sequencia,           
		nm_usuario,
		ie_ordem,
		dt_referencia,
		cd_pessoa_fisica,
		dt_atualizacao)
	Values( nr_seq_nova_w,
		nm_usuario_p,
		5,
		dt_referencia_w,
		cd_pessoa_fisica_p,
		sysdate);
	
	ind := 0;	
	ds_cores_w	:= null;
	
	open	c02;
	loop
	fetch c02 into
		ds_formula_w,
		nr_seq_exame_w,
		nr_seq_apresent_w,
		ds_cor_item_w,
		qt_casas_decimais_w;
	exit	when c02%notfound;
		begin

		ds_item_w	:= 0;
		
		if	(nr_seq_exame_w is not null) then
		
			ds_item_w	:= 	tx_obter_ult_result_data_lab(cd_pessoa_fisica_p, nr_seq_nova_w, nr_seq_exame_w); --andrey
			ds_item_ant_w	:= 	ds_item_w;

			
		
		elsif	(ds_formula_w is not null) then
		
			ds_item_w	:= 	tx_obter_formula(cd_pessoa_fisica_p, nr_seq_nova_w, ds_formula_w,qt_casas_decimais_w); --andrey
		
		end if;
		
		
		
		begin
		
		--select 	campo_mascara_virgula(to_number(ds_item_w))
		select	campo_mascara_virgula_casas(to_number(ds_item_w), qt_casas_decimais_w)
		into	ds_item_w
		from	dual;

		exception
			when others then
			ds_item_w := ds_item_ant_w;
		end;
		
		
		ind := ind + 1;
		vetor1_w(ind).nm_coluna_w := ds_item_w;
		
		begin
		select	decode(length(to_char(ind)),1,'0'||to_char(ind),to_char(ind))
		into	ds_cor_w
		from	dual;
			
		ds_cores_w	:= ds_cores_w || ds_cor_w || ds_cor_item_w ||',';
		end;

		end;
	end loop;
	close c02;
	
	ind := ind;
	while	(ind < 50) loop
		begin
		ind := ind + 1;
		vetor1_w(ind).nm_coluna_w := null;
		end;
	end loop;
	
	UPDATE TX_EXAME_GRID_CONTROLE
	SET	ds_cor		= ds_cores_w,
		ds_resultado1	= vetor1_w(1).nm_coluna_w,
		ds_resultado2	= vetor1_w(2).nm_coluna_w,
		ds_resultado3	= vetor1_w(3).nm_coluna_w,
		ds_resultado4	= vetor1_w(4).nm_coluna_w,
		ds_resultado5	= vetor1_w(5).nm_coluna_w,
		ds_resultado6	= vetor1_w(6).nm_coluna_w,
		ds_resultado7	= vetor1_w(7).nm_coluna_w,
		ds_resultado8	= vetor1_w(8).nm_coluna_w,
		ds_resultado9	= vetor1_w(9).nm_coluna_w,
		ds_resultado10	= vetor1_w(10).nm_coluna_w,
		ds_resultado11	= vetor1_w(11).nm_coluna_w,
		ds_resultado12	= vetor1_w(12).nm_coluna_w,
		ds_resultado13	= vetor1_w(13).nm_coluna_w,
		ds_resultado14	= vetor1_w(14).nm_coluna_w,
		ds_resultado15	= vetor1_w(15).nm_coluna_w,
		ds_resultado16	= vetor1_w(16).nm_coluna_w,
		ds_resultado17	= vetor1_w(17).nm_coluna_w,
		ds_resultado18	= vetor1_w(18).nm_coluna_w,
		ds_resultado19	= vetor1_w(19).nm_coluna_w,
		ds_resultado20	= vetor1_w(20).nm_coluna_w,
		ds_resultado21	= vetor1_w(21).nm_coluna_w,
		ds_resultado22	= vetor1_w(22).nm_coluna_w,
		ds_resultado23	= vetor1_w(23).nm_coluna_w,
		ds_resultado24	= vetor1_w(24).nm_coluna_w,
		ds_resultado25	= vetor1_w(25).nm_coluna_w,
		ds_resultado26	= vetor1_w(26).nm_coluna_w,
		ds_resultado27	= vetor1_w(27).nm_coluna_w,
		ds_resultado28	= vetor1_w(28).nm_coluna_w,
		ds_resultado29	= vetor1_w(29).nm_coluna_w,
		ds_resultado30	= vetor1_w(30).nm_coluna_w,
		ds_resultado31	= vetor1_w(31).nm_coluna_w,
		ds_resultado32	= vetor1_w(32).nm_coluna_w,
		ds_resultado33	= vetor1_w(33).nm_coluna_w,
		ds_resultado34	= vetor1_w(34).nm_coluna_w,
		ds_resultado35	= vetor1_w(35).nm_coluna_w,
		ds_resultado36	= vetor1_w(36).nm_coluna_w,
		ds_resultado37	= vetor1_w(37).nm_coluna_w,
		ds_resultado38	= vetor1_w(38).nm_coluna_w,
		ds_resultado39	= vetor1_w(39).nm_coluna_w,
		ds_resultado40	= vetor1_w(40).nm_coluna_w,
		ds_resultado41	= vetor1_w(41).nm_coluna_w,
		ds_resultado42	= vetor1_w(42).nm_coluna_w,
		ds_resultado43	= vetor1_w(43).nm_coluna_w,
		ds_resultado44	= vetor1_w(44).nm_coluna_w,
		ds_resultado45	= vetor1_w(45).nm_coluna_w,
		ds_resultado46	= vetor1_w(46).nm_coluna_w,
		ds_resultado47	= vetor1_w(47).nm_coluna_w,
		ds_resultado48	= vetor1_w(48).nm_coluna_w,
		ds_resultado49	= vetor1_w(49).nm_coluna_w,
		ds_resultado50	= vetor1_w(50).nm_coluna_w
	WHERE	NR_SEQUENCIA = nr_seq_nova_w;
end if;

commit; 
end;
/