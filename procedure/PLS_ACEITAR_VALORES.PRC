create or replace
procedure pls_aceitar_valores(
			ds_lista_contas_p	varchar2,
			ie_tipo_valor_p		varchar2,
			ie_servico_material	varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is 

ds_lista_contas_w	varchar2(2000);
ie_status_w		varchar2(1);
nr_pos_virgula_w		number(10,0);
nr_sequencia_w		number(10,0);
nr_seq_conta_proc_w	number(10,0);		
nr_seq_conta_mat_w	number(10,0);
		
/* 
IE_SERVICO_MATERIAL
S = Servi�os
M = Materiais
*/		
begin
if	(ds_lista_contas_p is not null) and
	(ie_tipo_valor_p is not null) and
	(ie_servico_material is not null) and
	(cd_estabelecimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	ds_lista_contas_w := ds_lista_contas_p;
	
	while (ds_lista_contas_w is not null) loop
		begin
		
		nr_pos_virgula_w	:= instr(ds_lista_contas_w,',');
		
		if	(nr_pos_virgula_w > 0) then
			begin			
			nr_sequencia_w		:= to_number(substr(ds_lista_contas_w,0,nr_pos_virgula_w-1));
			ds_lista_contas_w	:= substr(ds_lista_contas_w,nr_pos_virgula_w+1,length(ds_lista_contas_w));			
			end;
		else
			begin
			nr_sequencia_w		:= to_number(ds_lista_contas_w);
			ds_lista_contas_w	:= null;
			end;
		end if;
			
		if	(nr_sequencia_w > 0) then
			begin			
			select	max(ie_status)
			into	ie_status_w
			from	pls_conta 
			where	nr_sequencia = nr_sequencia_w;

			if 	(ie_status_w = 'P') then
				begin			
				if	(ie_servico_material = 'S') then
					begin				
					nr_seq_conta_proc_w := nr_sequencia_w;
					nr_seq_conta_mat_w  := 0;						
					end;
				elsif	(ie_servico_material = 'M') then
					begin					
					nr_seq_conta_proc_w := 0;
					nr_seq_conta_mat_w  := nr_sequencia_w;					
					end;
				end if;			
				
				pls_conta_aceitar_valor(
					0,
					nr_seq_conta_proc_w,
					nr_seq_conta_mat_w,
					'N',
					ie_tipo_valor_p,
					cd_estabelecimento_p,
					'C',
					nm_usuario_p,
					null,
					null);						
				end;
			end if;	
			end;
		end if;		
		end;
	end loop;
	end;
end if;
commit;
end pls_aceitar_valores;
/