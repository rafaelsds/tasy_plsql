create or replace
procedure limpar_tabelas is 
ds_erro_w varchar2(512);
begin

begin
	gravar_processo_longo('LIMPAR_TABELAS','LIMPAR_TABELAS_REGRA_NEGOCIO',1);
	limpar_tabelas_regra_negocio;
exception
when others then
	ds_erro_w := substr(SQLERRM(sqlcode),1,512);
	tratar_erro_banco(ds_erro_w);
	GRAVAR_LOG_TASY(88936,'LIMPAR_TABELAS_REGRA_NEGOCIO','TASY');
end;

begin
	gravar_processo_longo('LIMPAR_TABELAS','LIMPAR_TABELAS_TEMPORARIAS',-1);
	limpar_tabelas_temporarias;
exception
when others then
	ds_erro_w := substr(SQLERRM(sqlcode),1,512);
	tratar_erro_banco(ds_erro_w);
	GRAVAR_LOG_TASY(88936,'LIMPAR_TABELAS_TEMPORARIAS','TASY');
end;
begin
	gravar_processo_longo('LIMPAR_TABELAS','LIMPAR_TABELA_LOG_TASY',-1);
	limpar_tabela_log_tasy;
exception
when others then
	ds_erro_w := substr(SQLERRM(sqlcode),1,512);
	tratar_erro_banco(ds_erro_w);
	GRAVAR_LOG_TASY(88936,'LIMPAR_TABELA_LOG_TASY','TASY');
end;

begin
	gravar_processo_longo('LIMPAR_TABELAS','LIMPAR_TABELA_LOG_TASY_TIPO',-1);
	limpar_tabela_log_tasy_tipo;
exception
when others then
	ds_erro_w := substr(SQLERRM(sqlcode),1,512);
	tratar_erro_banco(ds_erro_w);
	GRAVAR_LOG_TASY(88936,'LIMPAR_TABELA_LOG_TASY_TIPO','TASY');
end;

begin
	gravar_processo_longo('LIMPAR_TABELAS','LIMPAR_TABELA_LOG_LAB',-1);
	limpar_tabela_log_lab;
exception
when others then
	ds_erro_w := substr(SQLERRM(sqlcode),1,512);
	tratar_erro_banco(ds_erro_w);
	GRAVAR_LOG_TASY(88936,'LIMPAR_TABELA_LOG_LAB','TASY');
end;

begin
	gravar_processo_longo('LIMPAR_TABELAS','LIMPAR_TABELA_LAB_TIPO_LOG',-1);
	limpar_tabela_lab_tipo_log;
exception
when others then
	ds_erro_w := substr(SQLERRM(sqlcode),1,512);
	tratar_erro_banco(ds_erro_w);
	GRAVAR_LOG_TASY(88936,'LIMPAR_TABELA_LAB_TIPO_LOG','TASY');
end;

end limpar_tabelas;
/