create or replace
procedure pls_gerar_parcelas_agravo
			(	nr_seq_segurado_agravo_p	number,
				ie_opcao_p			varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

/*
ie_opcao_p
	G - Gerar
	D - Desfazer
*/

nr_seq_agravo_w			number(10);
qt_parcelas_w			number(10);
vl_agravo_total_w		number(15,2);
vl_agravo_w			number(15,2);
nr_parcela_w			number(10)	:= 1;
nr_seq_segurado_w		number(10);
dt_referencia_w			date;
qt_parcelas_existentes_w	number(10);
vl_diferenca_w			number(15,2);
qt_mens_geradas_w		number(10);
tx_agravo_w			number(7,4);

begin

if	(ie_opcao_p = 'G') then
	select	nr_seq_agravo,
		nr_seq_segurado,
		qt_parcelas,
		vl_agravo,
		tx_agravo
	into	nr_seq_agravo_w,
		nr_seq_segurado_w,
		qt_parcelas_w,
		vl_agravo_total_w,
		tx_agravo_w
	from	pls_segurado_agravo
	where	nr_sequencia	= nr_seq_segurado_agravo_p;
	
	if	(nvl(qt_parcelas_w,0) = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(293555);
	end if;
	
	select	add_months(max(c.dt_referencia),1)
	into	dt_referencia_w
	from	pls_segurado		a,
		pls_contrato_pagador	b,
		pls_mensalidade		c
	where	a.nr_seq_pagador	= b.nr_sequencia
	and	c.nr_seq_pagador	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_w;
	
	if	(dt_referencia_w is null) then
		select	nvl(max(dt_contratacao),sysdate)
		into	dt_referencia_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
	end if;
	
	vl_agravo_w	:= vl_agravo_total_w / qt_parcelas_w;
	
	vl_diferenca_w	:= vl_agravo_total_w - (qt_parcelas_w * vl_agravo_w);
	
	select	count(1)
	into	qt_parcelas_existentes_w
	from	pls_segurado_agravo_parc
	where	nr_seq_segurado_agravo	= nr_seq_segurado_agravo_p;
	
	if	(qt_parcelas_existentes_w = 0) then
		while (nr_parcela_w <= qt_parcelas_w) loop
			begin
			
			if	((nr_parcela_w = qt_parcelas_w) and (vl_diferenca_w <> 0)) then
				vl_agravo_w	:= vl_agravo_w + vl_diferenca_w;
			end if;
			
			insert	into	pls_segurado_agravo_parc
				(	nr_sequencia, nr_seq_segurado_agravo, cd_estabelecimento,
					dt_mes_competencia, vl_agravo, nr_parcela,
					dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec,tx_agravo)
				values
				(	pls_segurado_agravo_parc_seq.NextVal, nr_seq_segurado_agravo_p, cd_estabelecimento_p,
					dt_referencia_w, vl_agravo_w, nr_parcela_w,
					sysdate, nm_usuario_p, sysdate,
					nm_usuario_p,tx_agravo_w);
			
			nr_parcela_w := nr_parcela_w + 1;
			dt_referencia_w	:= add_months(dt_referencia_w,1);
			end;
		end loop;
	end if;
elsif	(ie_opcao_p = 'D') then
	select	count(1)
	into	qt_mens_geradas_w
	from	pls_segurado_agravo_parc
	where	nr_seq_segurado_agravo	= nr_seq_segurado_agravo_p
	and	nr_seq_mensalidade_item is not null;
	
	if	(nvl(qt_mens_geradas_w,0) <> 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(262242);
		/* Mensagem: N�o � poss�vel desfazer as parcelas! J� foi gerada uma mensalidade para o agravo, favor verificar! */
	else
		delete	from	pls_segurado_agravo_parc
		where	nr_seq_segurado_agravo	= nr_seq_segurado_agravo_p;
	end if;
end if;

commit;

end pls_gerar_parcelas_agravo;
/