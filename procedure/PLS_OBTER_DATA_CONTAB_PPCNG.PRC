create or replace
function pls_obter_data_contab_ppcng(	ie_cancelamento_p	varchar2,
					dt_cancelamento_p	date,
					dt_inicio_cobertura_p	date,
					dt_mesano_referencia_p	date,
					nr_seq_item_cancel_p	number)
			return date is

dt_contabil_w		date;
			
begin

if	(ie_cancelamento_p = 'E') then
	if	(dt_cancelamento_p >= dt_mesano_referencia_p) then
		dt_contabil_w	:= dt_cancelamento_p;
	else
		dt_contabil_w	:= dt_inicio_cobertura_p;
	end if;
	
else
	if	(dt_inicio_cobertura_p is not null) then
		dt_contabil_w	:= dt_inicio_cobertura_p;
	else
		select 	max(y.dt_inicio_cobertura)
		into	dt_contabil_w
		from	pls_mensalidade_segurado y,
			pls_mensalidade_seg_item x
		where	y.nr_sequencia	= x.nr_seq_mensalidade_seg
		and	x.nr_sequencia	= nr_seq_item_cancel_p;
	end if;

end if;

return	dt_contabil_w;

end pls_obter_data_contab_ppcng;
/