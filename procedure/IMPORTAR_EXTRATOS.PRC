create or replace
procedure importar_extratos(
		nm_usuario_p	varchar2,
		nr_seq_extrato_p	number,
		ds_conteudo_p	varchar2	) is 

begin
	insert  into w_extrato_cartao_cr (  	nr_sequencia,
					dt_atualizacao,      
					nm_usuario,           
					dt_atualizacao_nrec,  
					nm_usuario_nrec,     
					nr_seq_extrato,       
					ds_conteudo)
				values (w_extrato_cartao_cr_seq.nextval, 
					sysdate,     
					nm_usuario_p,  
					sysdate,  
					nm_usuario_p,
					nr_seq_extrato_p,
					ds_conteudo_p );

end importar_extratos;
/