create or replace procedure gerar_requisicao_reg_atend (	ie_tipo_integracao_p		varchar2,
										nr_seq_segurado_p			number,
										nr_seq_regulacao_p			number,
										cd_cpf_medico_p				Varchar2,
										cd_cpf_benef_p				varchar2,
										nm_beneficiario_p			varchar2,
										nm_mae_benef_p				varchar2,
										dt_nasc_benef_p				date,
										ie_sexo_p					varchar2,
										cd_nascionalidade_p			varchar2,
										cd_prestador_p				varchar2,
										cd_cgc_p					varchar2,
										ds_observacao_p				varchar2,
										ds_indicacao_clinica_p		varchar2,
										nm_usuario_p				Varchar2,
										nr_seq_requisicao_p			out	number,
										ie_tipo_p					pls_requisicao.ie_tipo%type default null,
										cd_procedimento_p			number,
										ie_origem_proced_p			varchar2,
										qt_solicitado_p				number,
										nr_seq_prioridade_p			varchar2,
                    cd_material_p           number,
                    qt_material_p           number) is

nr_seq_plano_w			number(10);
nr_seq_requisicao_w		number(10);
nr_seq_uni_exec_w		number(10);
nr_seq_segurado_w		number(10);
ie_lanc_auto_w			varchar2(1);
ie_sexo_benef_w			varchar2(1);
ie_tipo_segurado_w		varchar2(255);
ie_tipo_processo_w		varchar2(255)	:= 'M';
ie_tipo_intercambio_w	varchar2(255);
nm_beneficiario_w		varchar2(255);
nm_mae_benef_w			varchar2(255);
nr_seq_cbo_w			tiss_cbo_saude.nr_seq_cbo_saude%type;
cd_cooperativa_w		varchar2(10);
cd_medico_w				varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
cd_nacionalidade_w		varchar2(10);
dt_nasc_benef_w			date;
nr_seq_prestador_w		number(10);
nr_parecer_w			number(10);

begin

if (ie_tipo_integracao_p = 'N') then

	nr_seq_segurado_w := nr_seq_segurado_p;

	if	(nvl(nr_seq_segurado_w,0) = 0) then

		if	(trim(cd_cpf_benef_p) is not null) then

			select	max(cd_pessoa_fisica),
					max(dt_nascimento),
					max(cd_nacionalidade),
					max(nm_pessoa_fisica),
					max(ie_sexo),
					max(substr(obter_nome_mae_pf(cd_pessoa_fisica),1,255))
			into	cd_pessoa_fisica_w,
					dt_nasc_benef_w,
					cd_nacionalidade_w,
					nm_beneficiario_w,
					ie_sexo_benef_w,
					nm_mae_benef_w
			from	pessoa_fisica
			where	NR_CPF = cd_cpf_medico_p;

			if	((Trim(dt_nasc_benef_p) 	is not null) and (dt_nasc_benef_w 		= dt_nasc_benef_p)) 	and
				((Trim(cd_nascionalidade_p) is not null) and (cd_nacionalidade_w 	= cd_nascionalidade_p)) and
				((Trim(nm_beneficiario_p) 	is not null) and (nm_beneficiario_w 	= nm_beneficiario_p)) 	and
				((Trim(ie_sexo_p) 			is not null) and (ie_sexo_benef_w 		= ie_sexo_p)) 			and
				((Trim(nm_mae_benef_p) 		is not null) and (nm_mae_benef_w 		= nm_mae_benef_p)) then

				select	max(a.nr_sequencia)
				into	nr_seq_segurado_w
				from	pls_segurado a
				where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;

			end if;

		end if;

	end if;

	if	(nvl(nr_seq_segurado_w,0) > 0) then

		select	pls_requisicao_seq.nextval
		into	nr_seq_requisicao_w
		from	dual;

		select	max(nr_seq_plano)
		into	nr_seq_plano_w
		from	pls_segurado
		where	nr_sequencia = nr_seq_segurado_w;

		ie_tipo_segurado_w	:= pls_obter_dados_segurado(nr_seq_segurado_w, 'TP');

		if	(nvl(ie_tipo_segurado_w,'X')	= 'I') then
			ie_tipo_processo_w	:= 'I';
			ie_tipo_intercambio_w	:= 'I';
		end if;

		/* Caso tenha sido informado o m�dico, � obtido o CBO deste m�dico para gravar na requisi��o */
		if	(cd_cpf_medico_p is not null) then
			begin
				select	max(cd_pessoa_fisica)
				into	cd_medico_w
				from	pessoa_fisica
				where	NR_CPF = cd_cpf_medico_p;

				select	max(substr(PLS_OBTER_CBO_MEDICO(cd_medico_w),1,255))
				into	nr_seq_cbo_w
				from	dual;
			exception
			when others then
				nr_seq_cbo_w	:= null;
			end;
		end if;

		if	(nvl(cd_prestador_p,0) > 0) then

			select	max(nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador
			where	cd_prestador = cd_prestador_p;

		else

			select	max(nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador
			where	cd_pessoa_fisica = cd_pessoa_fisica_w;

		end if;
    
		insert into pls_requisicao(	nr_sequencia,
									dt_requisicao,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_segurado,
									cd_medico_solicitante,
									nr_seq_plano,
									cd_estabelecimento,
									ie_tipo_guia,
									ie_estagio,
									ie_tipo_requisicao,
									nm_usuario_solic,
									ie_origem_solic,
									nr_seq_prestador,
									ie_tipo_processo,
									ie_tipo_intercambio,
									ie_tipo_gat,
									nr_seq_cbo_saude,
									nr_seq_regulacao,
									ie_integracao,
									ie_estagio_regulacao,
									nr_seq_prioridade,
									ds_observacao,
									ds_indicacao_clinica,
									ie_tipo)
						values	(	nr_seq_requisicao_w,
									sysdate,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_segurado_w,
									cd_medico_w,
									nr_seq_plano_w,
									obter_estabelecimento_ativo,
									null,
									1,
									'M', -- ie_tipo_requisicao
									nm_usuario_p,
									'R',  -- ie_origem_solic
									nr_seq_prestador_w,
									'E', --ie_tipo_processo
									ie_tipo_intercambio_w,
									'N',
									nr_seq_cbo_w,
									nr_seq_regulacao_p,
									ie_tipo_integracao_p,
									'AG',
									nvl(nr_seq_prioridade_p,1),
									ds_observacao_p,
									ds_indicacao_clinica_p,
									ie_tipo_p);
		commit;
		

		if(nr_seq_requisicao_w is not null and cd_procedimento_p is not null and ie_origem_proced_p is not null)then
			gerar_requisicao_int_proc(nr_seq_requisicao_w, cd_procedimento_p, ie_origem_proced_p, qt_solicitado_p, nm_usuario_p ,nr_seq_prioridade_p);
		elsif (nr_seq_requisicao_w is not null and cd_material_p is not null and qt_material_p is not null) then 
			gerar_mat_req_reg(cd_material_p, qt_material_p, nr_seq_requisicao_w, nm_usuario_p );
		end if;

		pls_requisicao_gravar_hist(	nr_seq_requisicao_w,
									'L',
									'Requisi��o n� '||nr_seq_requisicao_w||' criada pelo usu�rio '||nm_usuario_p||' em '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),
									null,
									nm_usuario_p);

		pls_consistir_requisicao(nr_seq_requisicao_w, obter_estabelecimento_ativo, nm_usuario_p);

		nr_seq_requisicao_p := nr_seq_requisicao_w;

	end if;

end if;

commit;

end gerar_requisicao_reg_atend;
/