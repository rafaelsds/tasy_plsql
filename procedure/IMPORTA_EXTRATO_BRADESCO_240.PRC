CREATE OR REPLACE PROCEDURE
IMPORTA_EXTRATO_BRADESCO_240(	nr_seq_extrato_p	number) is

/* Layout CNAB Vers�o 3.0 de 16/09/2011 */

nr_seq_conta_W		number(10);
vl_saldo_inicial_w	number(15,2);
vl_saldo_final_w	number(15,2);
dt_saldo_inicial_w	date;
dt_saldo_final_w	date;
nr_seq_extrato_w	number(10);
nr_seq_conta_nova_w	number(10);
nr_conta_ant_w		varchar2(12)	:= '-1';
ie_digito_ant_w		varchar2(1);
ie_deb_cred_saldo_ini_w	varchar2(1);
ie_deb_cred_saldo_fin_w	varchar2(1);
cd_banco_w		number(3);
ie_atualizar_extrato_lanc_w varchar2(1)	 := 'N';

/* Registro detalhe */
nr_documento_w		varchar2(20);
dt_lancamento_w		varchar2(8);
vl_lancamento_w		varchar2(18);
ie_deb_cred_w		varchar2(1);
nr_conta_w		varchar2(12);
digito_conta_w		varchar2(1);
ds_historico_w		varchar2(25);
qt_conta_w		number(10);

cursor c01 is
Select	substr(ds_conteudo,202,20) nr_documento,
	substr(ds_conteudo,143,8) dt_lancto,
	substr(ds_conteudo,151,18) vl_lancto,
	substr(ds_conteudo,169,1) ie_cred_deb,
	substr(ds_conteudo,59,12) nr_conta,
	substr(ds_conteudo,71,1) digito_conta,
	substr(ds_conteudo,177,25) ds_historico
from	W_interf_concil
where	substr(ds_conteudo,8,1)	= '3'
and	nr_seq_conta		= nr_seq_conta_W;

begin

ie_atualizar_extrato_lanc_w := Obter_Valor_Param_Usuario(814, 47, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, 0);

Select	max(b.nr_seq_conta),
	max(a.cd_banco)
into	nr_seq_conta_w,
	cd_banco_w
from	banco_estabelecimento a,
	banco_extrato b
where	b.nr_seq_conta	= a.nr_sequencia
and	b.nr_sequencia	= nr_seq_extrato_p;

open c01;
loop
fetch c01 into
	nr_documento_w,
	dt_lancamento_w,
	vl_lancamento_w,
	ie_deb_cred_w,
	nr_conta_w,
	digito_conta_w,
	ds_historico_w;
exit when c01%notfound;

	if	(nr_conta_ant_w	= '-1') or
		(nr_conta_ant_w <> nr_conta_w) or
		(ie_digito_ant_w <> digito_conta_w) then

		nr_conta_ant_w	:= nr_conta_w;
		ie_digito_ant_w := digito_conta_w;

		select	to_date(substr(a.ds_conteudo,143,8),'dd/mm/yyyy') dt_lancto,
			somente_numero(substr(a.ds_conteudo,151,18)) / 100 vl_lancto,
			substr(a.ds_conteudo,169,1) ie_cred_deb
		into	dt_saldo_inicial_w,
			vl_saldo_inicial_w,
			ie_deb_cred_saldo_ini_w
		from	w_interf_concil a
		where	substr(ds_conteudo,71,1)	= digito_conta_w
		and	substr(ds_conteudo,59,12)	= nr_conta_w
		and	substr(ds_conteudo,8,1)		= '1'
		and	a.nr_seq_conta			= nr_seq_conta_w;

		select	to_date(substr(a.ds_conteudo,143,8),'dd/mm/yyyy') dt_lancto,
			somente_numero(substr(a.ds_conteudo,151,18)) / 100 vl_lancto,
			substr(a.ds_conteudo,169,1) ie_cred_deb
		into	dt_saldo_final_w,
			vl_saldo_final_w,
			ie_deb_cred_saldo_fin_w
		from	w_interf_concil a
		where	substr(ds_conteudo,71,1)	= digito_conta_w
		and	substr(ds_conteudo,59,12)	= nr_conta_w
		and	substr(ds_conteudo,8,1)		= '5'
		and	a.nr_seq_conta			= nr_seq_conta_w;

		select	max(a.nr_sequencia),
			count(*)
		into	nr_seq_conta_nova_w,
			qt_conta_w
		from	banco_estabelecimento a
		where	nvl(a.ie_situacao,'A')			= 'A'
		and	somente_numero(a.ie_digito_conta)	= somente_numero(digito_conta_w)
		and	somente_numero(a.cd_conta)		= somente_numero(nr_conta_w)
		and	cd_banco				= cd_banco_w;

		if	(qt_conta_w	> 1) then

			select	max(a.nr_sequencia)
			into	nr_seq_conta_nova_w
			from	conta_banco_tipo b,
				banco_estabelecimento a
			where	nvl(a.ie_situacao,'A')			= 'A'
			and	b.ie_classif_conta			= 'CC'
			and	a.nr_seq_tipo_conta			= b.nr_sequencia
			and	somente_numero(a.ie_digito_conta)	= somente_numero(digito_conta_w)
			and	somente_numero(a.cd_conta)		= somente_numero(nr_conta_w)
			and	cd_banco				= cd_banco_w;

		end if;

		if	(nr_seq_conta_nova_w	is null) then

			nr_seq_conta_nova_w	:= nr_seq_conta_w;

		end if;

		if	(ie_deb_cred_saldo_ini_w	= 'D') then
			vl_saldo_inicial_w	:= nvl(vl_saldo_inicial_w,0) * -1;
		end if;

		if	(ie_deb_cred_saldo_fin_w	= 'D') then
			vl_saldo_final_w	:= nvl(vl_saldo_final_w,0) * -1;
		end if;

		if	(nr_seq_conta_w	= nr_seq_conta_nova_w) then

			nr_seq_extrato_w	:= nr_seq_extrato_p;
			nr_seq_conta_nova_w	:= nr_seq_conta_w;

			if (nvl(ie_atualizar_extrato_lanc_w, 'N') = 'S') then
				update	banco_extrato
				set	vl_saldo_inicial	= vl_saldo_inicial_w,
					vl_saldo_final		= vl_saldo_final_w,
					dt_inicio		= dt_saldo_inicial_w,
					dt_final		= dt_saldo_final_w,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= nr_seq_extrato_w;
			end if;	

		else

			if	(nr_seq_conta_nova_w	is not null) then

				select	banco_extrato_seq.nextval
				into	nr_seq_extrato_w
				from	dual;

				insert	into banco_extrato
					(dt_atualizacao,
					dt_atualizacao_nrec,
					dt_final,
					dt_importacao,
					dt_inicio,
					nm_usuario,
					nm_usuario_nrec,
					nr_seq_conta,
					nr_sequencia,
					vl_saldo_final,
					vl_saldo_inicial)
				values	(sysdate,
					sysdate,
					dt_saldo_final_w,
					sysdate,
					dt_saldo_inicial_w,
					'Tasy',
					'Tasy',
					nr_seq_conta_nova_w,
					nr_seq_extrato_w,
					vl_saldo_final_w,
					vl_saldo_inicial_w);

			end if;

		end if;

	end if;

	if	(nr_seq_conta_nova_w	is not null) then

		insert	into banco_extrato_lanc
			(nr_documento,
			dt_movimento,
			vl_lancamento,
			ie_deb_cred,
			nr_seq_extrato,
			nr_sequencia,
			nm_usuario,
			ie_conciliacao,
			dt_atualizacao,
			ds_historico)
		values	(nr_documento_w,
			to_date(dt_lancamento_w,'dd/mm/yyyy'),
			somente_numero(vl_lancamento_w) / 100,
			ie_deb_cred_w,
			nr_seq_extrato_w,
			banco_extrato_lanc_seq.nextVal,
			'Tasy',
			'N',
			sysdate,
			ds_historico_w);

	end if;

end loop;
close c01;

delete 	from w_interf_concil
where	nr_seq_conta	= nr_seq_conta_W;

commit;

end IMPORTA_EXTRATO_BRADESCO_240;
/
