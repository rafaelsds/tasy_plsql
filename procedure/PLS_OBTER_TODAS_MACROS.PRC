create or replace
procedure pls_obter_todas_macros(
		ds_separador_p		varchar2,
		cd_pessoa_fisica_p		varchar2,
		nr_sequencia_p		number,
		ie_opcao_p		varchar2,
		ds_macros_p	out	varchar2) is

ds_separador_w	varchar2(1);
ds_macro_w	varchar2(20);
nm_atributo_w	varchar2(50);
ds_resultado_w	varchar2(10);
ds_substituir_w	varchar2(255);

Cursor C01 is
	select	upper(ds_macro),
		upper(nm_atributo)
	from	pls_macro_plano
	order by	ds_macro;

begin
ds_macros_p	:= '';
ds_separador_w	:= ds_separador_p;
if	(ds_separador_w is null) then
	ds_separador_w	:= '#';
end if;

open C01;
loop
fetch C01 into	
	ds_macro_w,
	nm_atributo_w;
exit when C01%notfound;
	begin
	if	(nm_atributo_w = 'CD_PESSOA_FISICA') then
		ds_resultado_w	:= cd_pessoa_fisica_p;
	elsif	(nm_atributo_w = 'NR_SEQ_CONTRATO') then
		ds_resultado_w	:= nr_sequencia_p;
	end if;

	select	substr(pls_substituir_macro(ds_macro_w, nm_atributo_w, ds_resultado_w, ie_opcao_p), 1,255)
	into	ds_substituir_w
	from	dual;

	ds_macros_p	:= ds_macros_p || ds_separador_p || ds_macro_w || ds_separador_p || nvl(ds_substituir_w, ' ');
	end;
end loop;
close C01;

ds_macros_p	:= substr(ds_macros_p, 2, length(ds_macros_p));

end pls_obter_todas_macros;
/