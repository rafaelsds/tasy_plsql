create or replace
procedure grava_log_cancel_solic(
				nr_solic_compra_p	Number,
				nm_usuario_p		varchar2) as

begin

update	solic_compra
set	ie_forma_exportar	= '4'
where	ie_forma_exportar	= '3'
and	nr_solic_compra		= nr_solic_compra_p;

update	solic_compra_item
set	ie_forma_exportar	= '4'
where	ie_forma_exportar	= '3'
and	nr_solic_compra		= nr_solic_compra_p;

commit;
end grava_log_cancel_solic;
/