create or replace
procedure Gerar_w_resultado_atend_hmj(	dt_inicial_p		date,
				dt_final_p			date,
				cd_convenio_p		number,
				nr_seq_protocolo_p		varchar2,
				nr_atendimento_p		number,
				nr_interno_conta_p		number,
				cd_medico_p		number,
				ie_quebra_p		number,
				nm_usuario_p		varchar2,
				ie_tipo_atendimento_p	number) is 
					
nr_atendimento_w			number(10);
nr_atendimento_ww			number(10);
nr_interno_conta_w			number(10);
ds_convenio_w			varchar2(255);
cd_convenio_w			number(5);
nm_medico_w			varchar2(100);
cd_medico_w			varchar2(10);
nr_seq_protocolo_w		number(10);
nr_protocolo_w			varchar2(40);
nm_paciente_w			varchar2(60);
cd_setor_atendimento_w		number(5);
ds_setor_atendimento_w		varchar2(100);
cd_estrutura_w			number(5);
ds_estrutura_w			varchar2(80);
dt_referencia_w			date;
cd_item_w			number(15);
ds_item_w			varchar2(255);
qt_resumo_w			number(15,2);
vl_imposto_w			number(15,2);
vl_unitario_w			number(15,2);
vl_receita_w			number(15,2);
vl_custo_unitario_w			number(15,2);
vl_custo_total_w			number(15,2);
vl_resultado_w			number(15,2);
vl_item_w				number(15,2);
ds_quebra_w			varchar2(200);
cd_quebra_w			number(15);
nr_tabela_w			number := 0;
nr_interno_conta_ww		number(10);
nr_seq_protocolo_ww		number(10);
cd_convenio_ww			number(10);
cd_medico_ww			varchar2(10);
ie_considera_data_w		varchar2(1)	:= 'N';
ie_tipo_atendimento_ww		number(10);
ie_tipo_atendimento_w		number(10);
ie_status_protocolo_w		number(1);

Cursor C01 is
select	nr_atendimento,
	nr_interno_conta,
	ds_convenio,
	cd_convenio,
	nm_medico,
	cd_medico,
	nr_seq_protocolo,
	nr_protocolo,
	nm_paciente,
	cd_setor_atendimento,
	ds_setor_atendimento,		
	cd_estrutura,
	ds_estrutura,
	dt_referencia,
	cd_item,
	ds_item,
	sum(nvl(qt_resumo,0)),
	sum(nvl(vl_imposto,0)),
	dividir(sum(nvl(vl_receita,0)),sum(nvl(qt_resumo,0))) vl_unitario,
	sum(nvl(vl_receita,0)),
	0 vl_custo_unitario,
	sum(nvl(vl_custo,0)) vl_custo_total,
	((sum(nvl(vl_receita,0)) - sum(nvl(vl_custo,0))) - sum(nvl(vl_imposto,0))) vl_resultado,
	sum(nvl(vl_item,0)),
	ie_tipo_atendimento,
	ie_status_protocolo
from	resumo_atendimentos_v
where	1=1
and	((ie_considera_data_w	= 'N') or (dt_referencia between dt_inicial_p and dt_final_p))
and	((obter_se_contido(nr_seq_protocolo, nvl(nr_seq_protocolo_ww, nr_seq_protocolo)) = 'S') or (nr_seq_protocolo_ww = '0'))
and	nr_atendimento	= nvl(nr_atendimento_ww, nr_atendimento)
and	nr_interno_conta 	= nvl(nr_interno_conta_ww, nr_interno_conta)
and	cd_convenio 	= nvl(cd_convenio_ww, cd_convenio)
and	cd_medico 	= nvl(cd_medico_ww, cd_medico)
and	ie_tipo_atendimento = nvl(ie_tipo_atendimento_ww, ie_tipo_atendimento)
group by 	ds_convenio,
		nr_seq_protocolo,
		nr_protocolo,
		nm_medico,
		nm_paciente,
		cd_setor_atendimento,
		ds_setor_atendimento,		
		cd_estrutura,
		ds_estrutura,
		nr_atendimento,
		nr_interno_conta,
		dt_referencia,
		cd_convenio,
		cd_medico,
		cd_item,
		ds_item,
		ie_tipo_atendimento,
		ie_status_protocolo;					

begin
select	decode(nr_atendimento_p,0,null,nr_atendimento_p) nr_atendimento,
	decode(nr_interno_conta_p,0,null,nr_interno_conta_p) nr_interno_conta,
	decode(nr_seq_protocolo_p,'','0',nr_seq_protocolo_p) nr_seq_protocolo,
	decode(cd_convenio_p,0,null,cd_convenio_p) cd_convenio,
	decode(cd_medico_p,0,null,cd_medico_p) cd_medico,
	decode(ie_tipo_atendimento_p,0,null,ie_tipo_atendimento_p) ie_tipo_atendimento
into	nr_atendimento_ww,
	nr_interno_conta_ww,
	nr_seq_protocolo_ww,
	cd_convenio_ww,
	cd_medico_ww,
	ie_tipo_atendimento_ww
from	dual;
	
if	(nr_atendimento_w = 0) then
	nr_atendimento_ww	:= null;
end if;

if	(dt_inicial_p	is not null) and
	(dt_final_p	is not null) then
	ie_considera_data_w	:= 'S';
end if;	



delete from w_resultado_atendimento
where	nm_usuario = nm_usuario_p;
commit;								
open C01;
loop
fetch C01 into	
	nr_atendimento_w,
	nr_interno_conta_w,
	ds_convenio_w,
	cd_convenio_w,
	nm_medico_w,
	cd_medico_w,
	nr_seq_protocolo_w,
	nr_protocolo_w,
	nm_paciente_w,
	cd_setor_atendimento_w,
	ds_setor_atendimento_w,
	cd_estrutura_w,
	ds_estrutura_w,
	dt_referencia_w,
	cd_item_w,
	ds_item_w,
	qt_resumo_w,
	vl_imposto_w,
	vl_unitario_w,
	vl_receita_w,
	vl_custo_unitario_w,
	vl_custo_total_w,
	vl_resultado_w,
	vl_item_w,
	ie_tipo_atendimento_w,
	ie_status_protocolo_w;
exit when C01%notfound;
	begin
	if (ie_quebra_p = 1) then
		ds_quebra_w := substr(wheb_mensagem_pck.get_texto(311874,'NR_ATENDIMENTO=' || nr_atendimento_w ||
				';NM_PACIENTE=' || nm_paciente_w ||
				';DS_CONVENIO=' || ds_convenio_w),1,255);
		cd_quebra_w := nr_atendimento_w;
	elsif (ie_quebra_p = 2) then
		ds_quebra_w := nr_interno_conta_w;
		cd_quebra_w := nr_interno_conta_w;
	elsif (ie_quebra_p = 3) then
		ds_quebra_w := ds_convenio_w;
		cd_quebra_w := cd_convenio_w;
	elsif (ie_quebra_p = 4) then
		ds_quebra_w := nm_medico_w;
		cd_quebra_w := cd_medico_w;
	elsif (ie_quebra_p = 5) then
		ds_quebra_w := nr_protocolo_w;
		cd_quebra_w := nr_seq_protocolo_w;
	end if;
	
	insert into w_resultado_atendimento(
		ds_quebra,
		cd_quebra,
		nm_paciente,
		cd_setor_atendimento,
		ds_setor_atendimento,
		cd_estrutura,
		ds_estrutura,
		dt_referencia,
		cd_item,
		ds_item,
		qt_resumo,
		vl_imposto,
		vl_unitario,
		vl_receita,
		vl_custo,
		vl_custo_total,
		vl_resultado,
		pr_resultado,
		nm_usuario,
		vl_item,
		ie_tipo_atendimento,
		ie_status_protocolo)
	values(	ds_quebra_w,
		cd_quebra_w,
		nm_paciente_w,
		cd_setor_atendimento_w,
		ds_setor_atendimento_w,
		cd_estrutura_w,
		ds_estrutura_w,
		dt_referencia_w,
		cd_item_w,
		ds_item_w,
		qt_resumo_w,
		vl_imposto_w,
		vl_unitario_w,
		vl_receita_w,
		vl_custo_unitario_w,
		vl_custo_total_w,
		vl_resultado_w,
		null,
		nm_usuario_p,
		vl_item_w,
		ie_tipo_atendimento_w,
		ie_status_protocolo_w);
	end;
end loop;
close C01;

commit;
end Gerar_w_resultado_atend_hmj;
/
