create or replace
procedure obter_se_equip_disp_cir(
					nr_seq_agenda_p			in number,
					cd_equipamento_p		in number,
					nm_usuario_p			in varchar2,
					cd_estabelecimento_p	in number,
					nr_seq_agenda_ant_p		in number,
					ie_novo_registro_p		in varchar2)
					is

qt_equip_man_w		        number(10,0);
qt_equipamento_w	        number(10,0);
qt_equip_agenda_w	        number(10,0);
nr_seq_agenda_w		        number(10,0);
qt_novo_registro_w			number(5);
cd_classif_equip_w			number(10,0);
qt_tempo_esterilizacao_w	number(10,0);
qt_equip_agenda_exec_w		number(10,0);
nr_minuto_duracao_w			number(10,0);
ie_contador_w				number(10,0)	:= 0;
tam_lista_w					number(10,0);
ie_pos_virgula_w			number(3,0);
nr_sequencia_w				number(10);
qt_equip_reserv_med_w		number(10);
nr_seq_equipe_w				number(10);
qt_equip_reservado_w		number(10);
qt_horas_w					number(15,4);
cd_agenda_w					number(10);
ds_equipamento_w	        varchar2(80);
ds_agenda_w			        varchar2(50);
nm_paciente_w		        varchar2(60);
nm_medico_w			        varchar2(50);
ie_consiste_w				varchar2(15);
ds_erro_w					varchar2(255)	:= null;
hr_duracao_w				varchar2(255);
ie_primeiro_registro_w		varchar2(1);
ds_lista_agenda_w			varchar2(255)	:= null;
cd_pessoa_fisica_w			varchar2(10);
cd_medico_w					varchar2(10);
ie_dia_da_semana_w			varchar2(100);
dt_inicial_w				date;
dt_final_w			        date;
dt_agenda_w			        date;
hr_inicio_atual_w			date;
hr_fim_atual_w				date;
hr_inicio_anterior_w 		date;
hr_fim_anterior_w			date;

cursor	C02 is
	select	/*+ INDEX(A AGEPACI_UK) */
		b.hr_inicio,
		b.hr_inicio + (qt_tempo_esterilizacao_w / 1440) - (1/86400),
		b.nr_sequencia
	from	agenda c,
		agenda_pac_equip a,
		agenda_paciente b
	where	b.nr_sequencia	= a.nr_seq_agenda
	and	((hr_inicio between dt_inicial_w and dt_final_w) or
		(hr_inicio + (qt_tempo_esterilizacao_w / 1440) - (1/86400) between dt_inicial_w and dt_final_w) or
		((hr_inicio < dt_inicial_w) and (hr_inicio + (qt_tempo_esterilizacao_w / 1440) - (1/86400) > dt_final_w)))
	and	a.cd_equipamento = cd_equipamento_p
	--and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(hr_inicio)))
	--and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(hr_inicio)))
	and 	b.cd_agenda = c.cd_agenda 
	and 	c.cd_tipo_agenda = 1
	and	ie_status_agenda = 'E'
	and	ie_origem_inf = 'I'
	and	b.nr_sequencia <> nr_seq_agenda_p;

cursor	C03 is
	select	/*+ INDEX(A AGEPACI_UK) */
		b.hr_inicio,
		b.hr_inicio + ((nr_minuto_duracao + nvl(qt_tempo_esterilizacao_w,0)) / 1440) - (1/86400),
		b.nr_sequencia
	from	agenda c,
		agenda_pac_equip a,
		agenda_paciente b
	where	b.nr_sequencia	= a.nr_seq_agenda
	and	((hr_inicio between dt_inicial_w and dt_final_w) or
		(hr_inicio + ((nr_minuto_duracao + nvl(qt_tempo_esterilizacao_w,0)) / 1440) - (1/86400) between dt_inicial_w and dt_final_w) or
		((hr_inicio < dt_inicial_w) and (hr_inicio + ((nr_minuto_duracao + nvl(qt_tempo_esterilizacao_w,0)) / 1440) - (1/86400) > dt_final_w)))
	and	a.cd_equipamento = cd_equipamento_p
	and 	b.cd_agenda = c.cd_agenda 
	and 	c.cd_tipo_agenda = 1
	and	ie_status_agenda not in ('C','E')
	and	ie_origem_inf = 'I'
	and	b.nr_sequencia <> nr_seq_agenda_p
	order by 1,2;

begin

if	(nvl(cd_equipamento_p,0) > 0) then
	Obter_Param_Usuario(871, 81, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_w);
	qt_novo_registro_w	:= 0;
	
	if	(ie_novo_registro_p = 'S') then
		qt_novo_registro_w := 1;
	end if;

	select	max(hr_inicio),
		max(cd_medico),
		max(cd_agenda)
	into	dt_inicial_w,
		cd_medico_w,
		cd_agenda_w
	from	agenda_paciente
	where	nr_sequencia	= nr_seq_agenda_p;

	qt_equip_man_w	:= obter_qt_equip_manutencao(null, cd_equipamento_p,dt_inicial_w);

	select	nvl(sum(nvl(qt_compartilhamento,qt_equipamento)) - qt_equip_man_w, 1),
		nvl(max(cd_classificacao),0)
	into	qt_equipamento_w,
		cd_classif_equip_w
	from	equipamento
	where	cd_equipamento	= cd_equipamento_p
	and	ie_situacao	= 'A';

	qt_tempo_esterilizacao_w	:= 0;

	/* Obter o tempo de esteriliza��o da classifica��o do equipamento */
	if	(cd_classif_equip_w <> 0) then
		select	nvl(max(qt_tempo_esterelizacao),0)
		into	qt_tempo_esterilizacao_w
		from	classif_equipamento
		where	nr_sequencia = cd_classif_equip_w;
	end if;

	select	hr_inicio,
		decode(ie_status_agenda, 'E', (hr_inicio + (qt_tempo_esterilizacao_w / 1440) - (1/86400)), (hr_inicio + ((nr_minuto_duracao + nvl(qt_tempo_esterilizacao_w,0)) / 1440) - (1/86400)))
	into	dt_inicial_w,
		dt_final_w
	from	agenda_paciente
	where	nr_sequencia	= nr_seq_agenda_p;

	/* Obter qtde de agendamentos que foram executadas no periodo - neste s� � contado o tempo de esteriliza��o e n�o considera mais o tempo de dura��o pois o mesmo j� foi realizado */
	ie_primeiro_registro_w 	:= 'S';

	open C02;
	loop
	fetch C02 into
		hr_inicio_atual_w,
		hr_fim_atual_w,
		nr_sequencia_w;
	exit when C02%notfound;
		if	(ie_primeiro_registro_w = 'S') then
			hr_inicio_anterior_w 	:= hr_inicio_atual_w;
			hr_fim_anterior_w		:= hr_fim_atual_w;
			qt_equip_agenda_exec_w	:= 1;
			ie_primeiro_registro_w 	:= 'N';
			ds_lista_agenda_w		:= ds_lista_agenda_w || nr_sequencia_w;
		else
			if	(hr_inicio_atual_w < hr_fim_anterior_w) then
				qt_equip_agenda_exec_w 	:= qt_equip_agenda_exec_w + 1;
				ds_lista_agenda_w		:= ds_lista_agenda_w || ',' || nr_sequencia_w;
			end if;
		end if;
	end loop;
	close C02;

	ie_primeiro_registro_w 	:= 'S';

	open C03;
	loop
	fetch C03 into
		hr_inicio_atual_w,
		hr_fim_atual_w,
		nr_sequencia_w;
	exit when C03%notfound;
		if	(ie_primeiro_registro_w = 'S') then
			hr_inicio_anterior_w 	:= hr_inicio_atual_w;
			hr_fim_anterior_w	:= hr_fim_atual_w;
			qt_equip_agenda_w	:= 1;
			ie_primeiro_registro_w 	:= 'N';
			if	(ds_lista_agenda_w is null) then
				ds_lista_agenda_w	:= ds_lista_agenda_w || nr_sequencia_w;
			else
				ds_lista_agenda_w	:= ds_lista_agenda_w || ',' ||nr_sequencia_w;
			end if;
		else
			if	(hr_inicio_atual_w < hr_fim_anterior_w) then
				qt_equip_agenda_w := qt_equip_agenda_w + 1;
				ds_lista_agenda_w := ds_lista_agenda_w || ',' ||nr_sequencia_w;
			end if;
		end if;
	end loop;
	close C03;

	qt_equip_agenda_w	:= nvl(qt_equip_agenda_exec_w,0) + nvl(qt_equip_agenda_w,0) + qt_novo_registro_w;

	if	(ds_lista_agenda_w is not null) then
		ds_lista_agenda_w := ds_lista_agenda_w || ',';
	end if;

	if	(qt_equipamento_w < qt_equip_agenda_w) then
		if	(ds_lista_agenda_w is not null) then
			while	ds_lista_agenda_w is not null or
				ie_contador_w > 200 loop
				begin

				tam_lista_w		:= length(ds_lista_agenda_w);
				ie_pos_virgula_w	:= instr(ds_lista_agenda_w,',');

				if	(ie_pos_virgula_w <> 0) then
					begin
					nr_seq_agenda_w	:= substr(ds_lista_agenda_w,1,(ie_pos_virgula_w - 1));
					select	substr(Obter_Desc_Equipamento(cd_equipamento_p),1,80),
						hr_inicio,
						substr(obter_nome_agenda(cd_agenda),1,50),
						nm_paciente,
						substr(obter_nome_pf(cd_medico),1,50),
						nr_minuto_duracao,
						obter_horario_formatado(nr_minuto_duracao/60)
					into	ds_equipamento_w,
						dt_agenda_w,
						ds_agenda_w,
						nm_paciente_w,
						nm_medico_w,
						nr_minuto_duracao_w,
						hr_duracao_w
					from	agenda_paciente
					where	nr_sequencia	= nr_seq_agenda_w;

					if	(qt_equip_man_w > 0) then
						ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(300669, 'DS_EQUIPAMENTO_P=' || obter_desc_equipamento(cd_equipamento_p)), 1, 255);
						
						insert into	consistencia_agenda_cir(
												nr_sequencia,
												dt_atualizacao,
												nm_usuario,
												nr_seq_agenda,
												ie_tipo_consistencia,
												ds_consistencia,
												cd_equipamento,
												nm_paciente)
										values(	consistencia_agenda_cir_seq.nextval,
												sysdate,
												nm_usuario_p,
												nr_seq_agenda_p,
												'E',
												ds_erro_w,
												cd_equipamento_p,
												nm_paciente_w);
						commit;
					else
						ds_erro_w := wheb_mensagem_pck.get_texto(300672, 'DT_AGENDA_W=' || to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ' ' || ds_agenda_w ||
										';NM_MEDICO_W=' || nm_medico_w || ';NR_MIN_W=' || to_char(nr_minuto_duracao_w) || ';HR_DURACAO_W=' || hr_duracao_w);

						insert into	consistencia_agenda_cir(	nr_sequencia,
												dt_atualizacao,
												nm_usuario,
												nr_seq_agenda,
												ie_tipo_consistencia,
												ds_consistencia,
												cd_equipamento,
												nm_paciente)
										values(		consistencia_agenda_cir_seq.nextval,
												sysdate,
												nm_usuario_p,
												nr_seq_agenda_p,
												'E',
												ds_erro_w,
												cd_equipamento_p,
												nm_paciente_w);
						commit;
					end if;

					if	(ie_consiste_w <> 'N') then
						insert into agenda_pac_hist(
							nr_sequencia,
							nr_seq_agenda,
							ds_historico,
							dt_atualizacao,
							nm_usuario,
							dt_historico,
							cd_pessoa_fisica,
							dt_atualizacao_nrec,
							nm_usuario_nrec)
						values(
							agenda_pac_hist_seq.nextval,
							nr_seq_agenda_p,
							wheb_mensagem_pck.get_texto(300681, 'CD_EQUIPAMENTO_P=' || substr(Obter_Desc_Equipamento(cd_equipamento_p), 1, 80)) || ds_erro_w,
							sysdate,
							'Tasy',
							sysdate,
							Obter_Dados_Usuario_Opcao(nm_usuario_p,'C'),
							sysdate,
							'Tasy');
						commit;
					end if;
					ds_lista_agenda_w	:= substr(ds_lista_agenda_w,(ie_pos_virgula_w + 1),tam_lista_w);
					end;
				end if;
				ie_contador_w	:= ie_contador_w + 1;
				end;
			end loop;
		elsif	(qt_equip_man_w > 0) and
			(ie_novo_registro_p = 'S') then

			ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(300669, 'DS_EQUIPAMENTO_P=' || obter_desc_equipamento(cd_equipamento_p)), 1, 255);

			insert into	consistencia_agenda_cir(
									nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									nr_seq_agenda,
									ie_tipo_consistencia,
									ds_consistencia,
									cd_equipamento)
							values(	consistencia_agenda_cir_seq.nextval,
									sysdate,
									nm_usuario_p,
									nr_seq_agenda_p,
									'E',
									ds_erro_w,
									cd_equipamento_p);
			commit;

			if	(ie_consiste_w <> 'N') then
				insert into agenda_pac_hist(
					nr_sequencia,
					nr_seq_agenda,
					ds_historico,
					dt_atualizacao,
					nm_usuario,
					dt_historico,
					cd_pessoa_fisica,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values(
					agenda_pac_hist_seq.nextval,
					nr_seq_agenda_p,
					wheb_mensagem_pck.get_texto(300681, 'CD_EQUIPAMENTO_P=' || substr(Obter_Desc_Equipamento(cd_equipamento_p), 1, 80)) || ds_erro_w,
					sysdate,
					'Tasy',
					sysdate,
					Obter_Dados_Usuario_Opcao(nm_usuario_p,'C'),
					sysdate,
					'Tasy');
				commit;
			end if;
		end if;
	end if;

	if	(ds_erro_w is null) then
		ie_dia_da_semana_w := Obter_Cod_Dia_Semana(dt_inicial_w);
		qt_horas_w := (dt_inicial_w - sysdate) * 24;
		-- Busca a quantidade de reservas do equipamento naquele hor�rio
		if	(cd_medico_w is not null) then
			select 	count(*)
			into	qt_equip_reservado_w
			from   	regra_reserva_equipamento
			where  	cd_equipamento = cd_equipamento_p
			and		nvl(cd_Agenda, cd_agenda_w)	= cd_agenda_w
			and 	nvl(ie_dia_semana,nvl(ie_dia_da_semana_w,'X')) = nvl(ie_dia_da_semana_w,'X')
			and	(dt_inicial_w between 	to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_inicio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') and
							to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_fim,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'))
			and	((nvl(qt_horas_ant_agenda,0) = 0) or (qt_horas_w > nvl(qt_horas_ant_agenda,0)))
			and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_inicial_w)))
			and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_inicial_w)));
			-- Se possui equipamento reservado naquele hor�rio, consiste disponibilidade do mesmo
			if	(qt_equip_reservado_w > 0) then
				-- Verfica se o equipamento est� reservado para o m�dico
				select 	count(*)
				into	qt_equip_reserv_med_w
				from   	regra_reserva_equipamento
				where  	cd_equipamento = cd_equipamento_p
				and		nvl(cd_Agenda, cd_agenda_w)	= cd_agenda_w
				and 	nvl(ie_dia_semana,nvl(ie_dia_da_semana_w,'X')) = nvl(ie_dia_da_semana_w,'X')
				and	(dt_inicial_w between 	to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_inicio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') and
								to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_fim,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'))
				and	((cd_pessoa_fisica = cd_medico_w) or
					 (obter_se_medico_equipe(nr_seq_equipe,cd_medico_w) = 'S'))
				and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_inicial_w)))
				and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_inicial_w)));

				-- Se n�o est� reservado, consiste disponibilidade de quantidade de equipamento dispon�vel
				if	(qt_equip_reserv_med_w = 0) then
					qt_equip_agenda_w	:= qt_equip_agenda_w - qt_equip_reservado_w;

					if	(qt_equip_agenda_w = 0) and
						(ie_consiste_w <> 'N') then
						select 	max(cd_pessoa_fisica),
							max(nr_seq_equipe)
						into	cd_pessoa_fisica_w,
							nr_seq_equipe_w
						from   	regra_reserva_equipamento
						where	nr_sequencia = (select 	max(nr_sequencia) 
									from	regra_reserva_equipamento
									where  	cd_equipamento = cd_equipamento_p
									and		nvl(cd_Agenda, cd_agenda_w)	= cd_agenda_w
									and 	nvl(ie_dia_semana,nvl(ie_dia_da_semana_w,'X')) = nvl(ie_dia_da_semana_w,'X')
									and	(dt_inicial_w between 	to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_inicio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') and
													to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_fim,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'))
									and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_inicial_w)))
									and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_inicial_w))));
						if	(cd_pessoa_fisica_w is not null) then
							ds_erro_w	:= wheb_mensagem_pck.get_texto(300682, 'DS_EQUIPAMENTO_P=' || substr(Obter_Desc_Equipamento(cd_equipamento_p), 1, 80) ||
												';CD_PESSOA_FISICA_P=' || substr(obter_nome_pf(cd_pessoa_fisica_w), 1, 50));
						else
							ds_erro_w	:= wheb_mensagem_pck.get_texto(300684, 'DS_EQUIPAMENTO_P=' || substr(Obter_Desc_Equipamento(cd_equipamento_p), 1, 80) ||
												';NR_SEQ_EQUIPE_P=' || substr(Obter_Desc_PF_Equipe(nr_seq_equipe_w), 1, 50));
						end if;

						insert into consistencia_agenda_cir(
								nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_agenda,
								ie_tipo_consistencia,
								ds_consistencia,
								cd_equipamento)
						values(	consistencia_agenda_cir_seq.nextval,
								sysdate,
								nm_usuario_p,
								nr_seq_agenda_p,
								'E',
								ds_erro_w,
								cd_equipamento_p);
						commit;	
						
						insert into agenda_pac_hist(
							nr_sequencia,           
							nr_seq_agenda,          
							ds_historico,           
							dt_atualizacao,         
							nm_usuario,             
							dt_historico,           
							cd_pessoa_fisica,       
							dt_atualizacao_nrec,    
							nm_usuario_nrec)
						values(
							agenda_pac_hist_seq.nextval,
							nr_seq_agenda_p,
							wheb_mensagem_pck.get_texto(300681, 'CD_EQUIPAMENTO_P=' || substr(Obter_Desc_Equipamento(cd_equipamento_p), 1, 80)) || ds_erro_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							Obter_Dados_Usuario_Opcao(nm_usuario_p,'C'),
							sysdate,
							nm_usuario_p);
							commit;
					end if;	
				end if;	
			end if;	
		end if;
	end if;
end if;	

end obter_se_equip_disp_cir;
/