create or replace
procedure	cm_atualiza_ciclo_conj(
			nr_seq_conj_p		number,
			nr_seq_ciclo_p		number,
			nm_usuario_p		varchar2,
			nm_usuario_orig_p  varchar2 default null,
			nm_usuario_prepa_p varchar2 default null) is
			
nr_seq_embalagem_w	cm_classif_embalagem.nr_sequencia%type;
qt_dia_validade_w	cm_classif_embalagem.qt_dia_validade%type;
qt_dia_validade_ww	cm_classif_embalagem.qt_dia_validade%type;

begin

update	cm_conjunto_cont
set	nr_seq_ciclo = nr_seq_ciclo_p,
	nm_usuario = nm_usuario_p,
    nm_usuario_origem = nvl(nm_usuario_orig_p,nm_usuario_origem)
where	nr_sequencia = nr_seq_conj_p;


if (nm_usuario_prepa_p is not null) then
	
	update cm_ciclo
	set nm_usuario_preparo = nm_usuario_prepa_p
	where nr_sequencia = nr_seq_ciclo_p;
	
end if;


select	max(nr_seq_embalagem)
into	nr_seq_embalagem_w
from	cm_conjunto_cont
where	nr_sequencia = nr_seq_conj_p;

select	nvl(max(qt_dia_validade), 0)
into	qt_dia_validade_w
from	cm_classif_embalagem
where	nr_sequencia = nr_seq_embalagem_w;

if	(qt_dia_validade_w > 0) then

	select	nvl(max(qt_dia_validade), 0)
	into	qt_dia_validade_ww
	from	cm_classif_embalagem
	where	nr_sequencia = nr_seq_embalagem_w
	and	nvl(ie_indeterminado, 'N') = 'S';
	
	if	(qt_dia_validade_ww = 0) then
		update	cm_conjunto_cont
		set	dt_validade = (sysdate + qt_dia_validade_w)
		where	nr_sequencia = nr_seq_conj_p;
	else
		update	cm_conjunto_cont
		set	dt_validade = null,
			ie_indeterminado = 'S'
		where	nr_sequencia = nr_seq_conj_p;
	end if;
end if;

commit;

end cm_atualiza_ciclo_conj;
/
