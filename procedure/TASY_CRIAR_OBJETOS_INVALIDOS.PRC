CREATE OR REPLACE
PROCEDURE tasy_criar_objetos_invalidos(ie_tipo_objeto_p in Varchar2,nm_owner_origem_p in Varchar2) IS

/*
Parametro IE_TIPO_OBJETO_P identifica qual tipo de objeto dever� ser criado
caso for passado em branco ir� criar todos.

V - View
T - Trigger
F - Function
P - Procedure
*/
nm_objeto_w	 	varchar2(40);
nr_ordem_w	 	number(10);
ds_tipo_objeto_w 	varchar2(15);
nm_owner_origem_w	varchar2(15);
qt_processada_w	number(10);
nm_owner_atual_w	varchar2(30);
qt_job_process		number(10);

/*CRIA AS VIEW ONDE OS CAMPOS FICAM COMO UNDEFINED*/
Cursor C01 is
	select 	distinct table_name 
	from 	USER_TAB_COLUMNS
	where 	data_type = 'UNDEFINED';

/*CRIA OS OBJETOS QUE EST�O DOCUMENTADOS E N�O EST�O NA BASE*/
/*Variaveis para cursor dinamico*/
c02		 Integer;
retorno_w	 Number(5);
ds_comando_w	 varchar2(512);

/*CRIA A DEPEND�NCIA QUE EST� INVALIDANDO UM OBJETO*/
Cursor c03 is
	SELECT 	c.referenced_name,
		1
	FROM   	user_errors a,
		objetos_invalidos_v b,
		USER_DEPENDENCIES C,
		objeto_sistema d
	WHERE   SUBSTR(a.text,9,9) = 'ORA-00904'
	AND	c.name = a.name
	AND	b.nm_objeto = d.nm_objeto
	and	gerar_Objeto_Aplicacao(d.ds_aplicacao) = 'S'
	AND	b.nm_objeto = a.name
	and	d.ie_tipo_objeto <> 'Script'
	AND	c.referenced_owner = nm_owner_atual_w
	AND	c.referenced_name LIKE '%_V'
	UNION
	SELECT 	DISTINCT OBTER_NOME_OBJETO_ERRO(text),
		2
	FROM   	user_errors a,
		objetos_invalidos_v b,
		objeto_sistema d
	WHERE   SUBSTR(a.text,1,9) = 'PLS-00306'
	AND	b.nm_objeto = a.name
	AND	b.nm_objeto = d.nm_objeto
	and	d.ie_tipo_objeto <> 'Script'
	and	gerar_Objeto_Aplicacao(d.ds_aplicacao) = 'S'
	order  	by 2;

/*CRIA OS OBJETOS INVALIDOS*/
Cursor C04 is
	select	a.nm_objeto
	FROM 	objetos_invalidos_v a,
		objeto_sistema b
	WHERE 	a.nm_objeto = b.nm_objeto
	and	(ds_tipo_objeto_w = 'TODOS' OR ds_tipo_objeto_w = a.ds_tipo)
	and	gerar_Objeto_Aplicacao(b.ds_aplicacao) = 'S'
	and	b.ie_tipo_objeto <> 'Script'
	order by a.nr_ordem;


BEGIN
qt_processada_w := 0;
GRAVAR_PROCESSO_LONGO('','TASY_CRIAR_OBJETOS_INVALIDOS',qt_processada_w);

select	user
into	nm_owner_atual_w
from	dual;

nm_owner_origem_w := nm_owner_origem_p;

if	( nm_owner_origem_p is null ) or
	( nm_owner_origem_p = '')  or
	( nm_owner_atual_w =  nm_owner_origem_w ) then
	nm_owner_origem_w := 'TASY_VERSAO';
end if;

ds_comando_w	:= ' SELECT	a.nm_objeto,' ||
		   '		obter_ordem_Objeto(a.ie_tipo_objeto) nr_ordem ' ||
		   ' FROM	'||nm_owner_origem_w||'.objeto_sistema a ' ||
		   ' WHERE	not exists ( 	select 	1 ' ||
		   '				from	user_objects b ' ||
		   '				where	a.nm_objeto = b.object_name) ' ||
		   ' AND 	upper(a.ie_tipo_objeto) IN (:IE_1,:IE_2,:IE_3,:IE_4,:IE_5,:IE_6,:IE_7)  ' ||
		   ' AND	Gerar_Objeto_Aplicacao(a.ds_aplicacao) = :DS_APLICACAO ' ||
		   ' AND	a.ie_tipo_objeto <> ' || chr(39) || 'Script' || chr(39) || 
		   ' ORDER BY nr_ordem ';

if	( ie_tipo_objeto_p = 'V' ) then
	ds_tipo_objeto_w := 'VIEW';
elsif	( ie_tipo_objeto_p = 'T' ) then
	ds_tipo_objeto_w := 'TRIGGER';
elsif	( ie_tipo_objeto_p = 'F' ) then
	ds_tipo_objeto_w := 'FUNCTION';
elsif	( ie_tipo_objeto_p = 'P' ) then
	ds_tipo_objeto_w := 'PROCEDURE';
else
	ds_tipo_objeto_w := 'TODOS';
end if;

GRAVAR_PROCESSO_LONGO('VIEW UNDEFINED','TASY_CRIAR_OBJETOS_INVALIDOS',qt_processada_w);

OPEN C01;
LOOP
FETCH C01 into
	nm_objeto_w;
exit when c01%notfound;
	begin
		tasy_criar_objeto_sistema(nm_objeto_w,nm_owner_origem_w);
		qt_processada_w := qt_processada_w + 1;
		GRAVAR_PROCESSO_LONGO(obter_desc_expressao(786485) || ' ' || nm_objeto_w,'TASY_CRIAR_OBJETOS_INVALIDOS',qt_processada_w);
	exception
	when others then
		begin
			nm_objeto_w := nm_objeto_w;
		end;
end;
END LOOP;
CLOSE C01;

GRAVAR_PROCESSO_LONGO('OBJETO FALTANDO NA BASE','TASY_CRIAR_OBJETOS_INVALIDOS',qt_processada_w);


C02 := DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(C02, ds_comando_w, dbms_sql.Native);
DBMS_SQL.DEFINE_COLUMN(C02,1,nm_objeto_w,50);	
DBMS_SQL.DEFINE_COLUMN(C02,2,nr_ordem_w,10);	

DBMS_SQL.BIND_VARIABLE(C02,'IE_1', 'PROCEDURE',50);
DBMS_SQL.BIND_VARIABLE(C02,'IE_2', 'VIEW',50);
DBMS_SQL.BIND_VARIABLE(C02,'IE_3', 'TRIGGER',50);
DBMS_SQL.BIND_VARIABLE(C02,'IE_4', 'FUNCTION',50);
DBMS_SQL.BIND_VARIABLE(C02,'IE_5', 'PACKAGE',50);
DBMS_SQL.BIND_VARIABLE(C02,'IE_6', 'BASE',50);
DBMS_SQL.BIND_VARIABLE(C02,'IE_7', 'TYPE',50);
DBMS_SQL.BIND_VARIABLE(C02,'DS_APLICACAO', 'S',50);

retorno_w := DBMS_SQL.execute(C02);

while	( DBMS_SQL.FETCH_ROWS(C02) > 0 ) loop
	DBMS_SQL.COLUMN_VALUE(C02, 1, nm_objeto_w);
	begin
		tasy_criar_objeto_sistema(nm_objeto_w,nm_owner_origem_w);
		qt_processada_w := qt_processada_w + 1;
		GRAVAR_PROCESSO_LONGO(obter_desc_expressao(786485) || ' ' || nm_objeto_w,'TASY_CRIAR_OBJETOS_INVALIDOS',qt_processada_w);
	exception
		when others then
		begin
			nm_objeto_w := nm_objeto_w;
		end;
	end;
END LOOP; 
DBMS_SQL.CLOSE_CURSOR(C02);

Compila_Objetos_Invalidos;

OPEN C03;
LOOP
FETCH C03 into
	nm_objeto_w,
	nr_ordem_w;
exit when C03%notfound;
	begin
		tasy_criar_objeto_sistema(nm_objeto_w,nm_owner_origem_w);
		qt_processada_w := qt_processada_w + 1;
		GRAVAR_PROCESSO_LONGO(obter_desc_expressao(786485) || ' ' || nm_objeto_w,'TASY_CRIAR_OBJETOS_INVALIDOS',qt_processada_w);
	exception
	when others then
		begin
			nm_objeto_w := nm_objeto_w;
		end;
end;
END LOOP;
CLOSE C03;

Compila_Objetos_Invalidos;

OPEN C04;
LOOP
FETCH C04 into
	nm_objeto_w;
exit when C04%notfound;
	begin
		tasy_criar_objeto_sistema(nm_objeto_w,nm_owner_origem_w);
		qt_processada_w := qt_processada_w + 1;
		GRAVAR_PROCESSO_LONGO(obter_desc_expressao(786485) || ' ' || nm_objeto_w,'TASY_CRIAR_OBJETOS_INVALIDOS',qt_processada_w);
	exception
	when others then
		begin
			nm_objeto_w := nm_objeto_w;
		end;
end;
END LOOP;
CLOSE C04;

if	( nm_owner_origem_p = '' ) or
	( nm_owner_origem_p is null) then
	
	qt_job_process := 0;
	begin
		ds_comando_w :=	' select	nvl(value,0) ' ||
				' from		v$parameter ' ||
				' where 	name =' || chr(39) || 'job_queue_processes' || chr(39);
		execute immediate ds_comando_w into qt_job_process;
	exception
		when others then
			qt_job_process := 0;
	end;
	
	begin	
		if	qt_job_process = 0 then
			qt_job_process := 10;
		end	if;
		ds_comando_w := 'begin SYS.UTL_RECOMP.recomp_parallel(' || qt_job_process || ','|| chr(39) || USER || chr(39) || '); end;';
		execute immediate ds_comando_w;
	exception
		when others then
			valida_objetos_sistema;			
	end;
end if;
GRAVAR_PROCESSO_LONGO('','',0);

END tasy_criar_objetos_invalidos;
/
