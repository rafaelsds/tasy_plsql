CREATE OR REPLACE
PROCEDURE Copiar_Ultima_Avaliacao(	nr_sequencia_p	number,
					nm_usuario_p	varchar2) IS 

nr_seq_tipo_avaliacao_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);

begin

select	max(nr_seq_tipo_avaliacao),
	max(cd_pessoa_fisica)
into	nr_seq_tipo_avaliacao_w,
	cd_pessoa_fisica_w
from	med_avaliacao_paciente
where	nr_sequencia = nr_sequencia_p;

insert into	Med_Avaliacao_Result(
	nr_seq_avaliacao,
	nr_seq_item,
	dt_atualizacao,
	nm_usuario,
	qt_resultado,
	ds_resultado)
select	nr_sequencia_p,
	nr_seq_item,
	sysdate,
	nm_usuario_p,
	qt_resultado,
	ds_resultado
from	Med_Avaliacao_Result a
where	nr_seq_avaliacao =	(	select	max(nr_sequencia)
					from	med_avaliacao_paciente
					where	nr_seq_tipo_avaliacao	= nr_seq_tipo_avaliacao_w
					and	cd_pessoa_fisica	= cd_pessoa_fisica_w
					and	nr_sequencia		<> nr_sequencia_p)
and	not exists	(	select	1
					from	Med_Avaliacao_Result x
					where	x.nr_seq_avaliacao = nr_sequencia_p
					and		x.nr_seq_item		= a.nr_seq_item);	
commit;

END Copiar_Ultima_Avaliacao;
/