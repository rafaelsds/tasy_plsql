create or replace
procedure gerar_audit_ajuste_dia(	nr_seq_auditoria_p	number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is 					
				
dt_conta_w			date;
cd_item_w			number(10);
vl_unitario			number(9,3);
nr_interno_conta_w		number(10);
qt_item_w			number(15,4);
qt_real_w			number(15,4);
nr_atendimento_w		number(10);
cd_setor_atendimento_w		number(10);
ie_origem_proced_w		number(10);
ds_item_w			varchar2(255);
dt_conta_item_w			date;
nr_doc_convenio_w		varchar2(20);
nr_seq_auditoria_w		number(10);
cd_convenio_w			number(5);	
cd_categoria_w			varchar2(10);
qt_original_w			number(9,3);
nr_sequencia_w			number(10);
cd_unidade_medida_w		varchar2(30);
ie_tipo_material_w	        varchar2(3);
vl_unitario_w			number(10,4);
nr_seq_ordem_w			number(10);

	
cursor c01 is
select		trunc(a.dt_conta) dt_conta,
		a.cd_procedimento cd_item, 
		dividir(sum(a.vl_procedimento),sum(a.qt_procedimento)) vl_unitario, 
		a.nr_interno_conta, 
		sum(a.qt_procedimento) qt_item, 
		sum(a.qt_procedimento) qt_real, 
		a.nr_atendimento,
		a.cd_setor_atendimento,	
		a.ie_origem_proced,
		c.ds_procedimento ds_item,
		min(dt_conta) dt_conta_item,
		a.nr_doc_convenio,
		d.nr_seq_auditoria,
		a.cd_convenio,
		a.cd_categoria,
		sum(decode(d.ie_tipo_auditoria,'E',0,d.qt_original))  qt_original
from  		auditoria_propaci d,
		procedimento_paciente a,
		procedimento c
where 		a.cd_motivo_exc_conta is null
and 		a.nr_sequencia = d.nr_seq_propaci
and		a.cd_procedimento = c.cd_procedimento
and		a.ie_origem_proced = c.ie_origem_proced
and		nr_seq_auditoria = nr_seq_auditoria_p
group by 	trunc(a.dt_conta),
		a.cd_procedimento, 
		a.ie_origem_proced,
		a.nr_interno_conta,
		a.nr_atendimento,
		a.cd_setor_atendimento,	
		c.ds_procedimento,
		a.nr_doc_convenio,
		nr_seq_auditoria,
		a.cd_convenio,
		a.cd_categoria;
cursor c02 is
select		trunc(a.dt_conta) dt_conta,
		a.cd_material cd_item, 
		a.cd_unidade_medida, 
		a.vl_unitario, 
		a.nr_interno_conta, 
		sum(a.qt_material) qt_item, 
		sum(a.qt_material) qt_real, 
		a.nr_atendimento,
		a.cd_setor_atendimento,	
		c.ds_material ds_item,
		min(dt_conta) dt_conta_item,
		a.nr_doc_convenio,
		c.ie_tipo_material,
		d.nr_seq_auditoria,
		a.cd_convenio,
		a.cd_categoria,
		sum(decode(d.ie_tipo_auditoria,'E',0,d.qt_original))  qt_original
from  		conta_paciente b, 
		auditoria_matpaci d,
		material_atend_paciente a,
		material c
where 		a.cd_motivo_exc_conta is null
and 		a.nr_interno_conta = b.nr_interno_conta
and 		a.nr_sequencia = d.nr_seq_matpaci
and 		b.ie_status_acerto = 1
and		a.cd_material = c.cd_material
group by 	trunc(a.dt_conta),
		a.cd_material, 
		a.cd_unidade_medida, 
		a.vl_unitario, 
		a.nr_interno_conta,
		a.nr_atendimento,
		a.cd_setor_atendimento,	
		a.cd_material,
		c.ds_material,
		a.nr_doc_convenio,
		c.ie_tipo_material,
		nr_seq_auditoria,
		a.cd_convenio,
		a.cd_categoria;
					
begin


delete	from w_audit_ajuste_dia
where 	nm_usuario = nm_usuario_p
and	nr_seq_auditoria = nr_seq_auditoria_p;


nr_seq_ordem_w := 0;

open C01;
loop
fetch C01 into	
	dt_conta_w,
	cd_item_w,
	vl_unitario_w,
	nr_interno_conta_w,
	qt_item_w,
	qt_real_w,
	nr_atendimento_w,
	cd_setor_atendimento_w,
	ie_origem_proced_w,
	ds_item_w,
	dt_conta_item_w,
	nr_doc_convenio_w,
	nr_seq_auditoria_w,
	cd_convenio_w,
	cd_categoria_w,
	qt_original_w;
exit when C01%notfound;
	begin
	
	select	w_audit_ajuste_dia_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	nr_seq_ordem_w := nr_seq_ordem_w + 1;
	
	insert into w_audit_ajuste_dia
		(	nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			dt_conta,
			cd_item, 
			vl_unitario, 
			nr_interno_conta, 
			qt_item, 
			qt_real, 
			nr_atendimento,
			cd_setor_atendimento,	
			ie_origem_proced,
			ds_item,
			dt_conta_item,
			nr_doc_convenio,
			nr_seq_auditoria,
			cd_convenio,
			cd_categoria,
			qt_original,
			nr_seq_ordem)
	values(	nr_sequencia_w,
		nm_usuario_p,
		sysdate,		
		dt_conta_w,
		cd_item_w, 
		vl_unitario_w, 
		nr_interno_conta_w, 
		qt_item_w, 
		qt_real_w, 
		nr_atendimento_w,
		cd_setor_atendimento_w,	
		ie_origem_proced_w,
		ds_item_w,
		dt_conta_item_w,
		nr_doc_convenio_w,
		nr_seq_auditoria_w,
		cd_convenio_w,
		cd_categoria_w,
		qt_original_w,
		nr_seq_ordem_w);
		
	end;
end loop;
close C01;


delete	from w_audit_ajuste_dia_mat
where 	nm_usuario = nm_usuario_p
and	nr_seq_auditoria = nr_seq_auditoria_p;
nr_seq_ordem_w := 0;

open C02;
loop
fetch C02 into	
	dt_conta_w,
	cd_item_w, 
	cd_unidade_medida_w, 
	vl_unitario_w, 
	nr_interno_conta_w, 
	qt_item_w, 
	qt_real_w, 
	nr_atendimento_w,
	cd_setor_atendimento_w,	
	ds_item_w,
	dt_conta_item_w,
	nr_doc_convenio_w,
	ie_tipo_material_w,
	nr_seq_auditoria_w,
	cd_convenio_w,
	cd_categoria_w,
	qt_original_w;
exit when C02%notfound;
	begin
	
	select	w_audit_ajuste_dia_mat_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	nr_seq_ordem_w := nr_seq_ordem_w + 1;
	
	insert into w_audit_ajuste_dia_mat
			(   nr_sequencia,
			    nm_usuario,
			    dt_atualizacao,
			    dt_conta,
			    cd_material, 
			    cd_unidade_medida, 
			    vl_unitario, 
			    nr_interno_conta, 
			    qt_item, 
			    qt_real, 
			    nr_atendimento,
			    cd_setor_atendimento,
			    dt_conta_item,
			    nr_doc_convenio,
			    ie_tipo_material,
			    nr_seq_auditoria,
			    cd_convenio,
			    cd_categoria,
			    qt_original,
			    nr_seq_ordem)
	values(	nr_sequencia_w,
		nm_usuario_p,
		sysdate,
		dt_conta_w,
		cd_item_w, 
		cd_unidade_medida_w, 
		vl_unitario_w, 
		nr_interno_conta_w, 
		qt_item_w, 
		qt_real_w, 
		nr_atendimento_w,
		cd_setor_atendimento_w,
		dt_conta_item_w,
		nr_doc_convenio_w,
		ie_tipo_material_w,
		nr_seq_auditoria_w,
		cd_convenio_w,
		cd_categoria_w,
		qt_original_w,
		nr_seq_ordem_w);			
	
	end;
end loop;
close C02;


commit;

end gerar_audit_ajuste_dia;
/