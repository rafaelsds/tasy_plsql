create or replace
procedure fleury_atualizar_status_laudo (	nr_sequencia_p	number,
				ds_tipo_resultado varchar2 default 'F') is 


nr_prescricao_w		number(14);
nr_seq_prescricao_w	number(10);
ie_status_execucao_w	varchar2(3);
						
begin

select	max(a.nr_prescricao),
	max(a.nr_seq_prescricao)
into	nr_prescricao_w,
	nr_seq_prescricao_w
from	laudo_paciente a
where	a.nr_sequencia = nr_sequencia_p;

if	(nr_prescricao_w is not null) and
	(nr_seq_prescricao_w is not null) then

	if (ds_tipo_resultado = 'F') then
		update	laudo_paciente
		set	dt_liberacao = sysdate,
			nm_usuario_liberacao = 'TasyFleuryWS',
			dt_aprovacao = sysdate,
			nm_usuario_aprovacao = 'TasyFleuryWS',
			nm_usuario_seg_aprov = 'TasyFleuryWS',
			dt_seg_aprovacao = sysdate,
			nm_usuario = 'TasyFleuryWS'
		where	nr_sequencia = nr_sequencia_p;
		
		/* *****  Atualiza status execu��o na prescri��o ***** */
		update	prescr_procedimento a
		set	a.ie_status_execucao 	= '40', --Liberado
			a.nm_usuario 	= 'TasyFleuryWS'
		where	a.nr_prescricao = nr_prescricao_w
		and	a.nr_sequencia  in (	select	b.nr_sequencia_prescricao
						from	procedimento_paciente b
						where	b.nr_prescricao 	= a.nr_prescricao
						and	b.nr_sequencia_prescricao = a.nr_sequencia
						and	b.nr_laudo 		= nr_seq_prescricao_w);

		select 	max(ie_status_execucao)
		into	ie_status_execucao_w
		from	prescr_procedimento a
		where	a.nr_prescricao = nr_prescricao_w
		and	a.nr_sequencia  = nr_seq_prescricao_w;

		if	(ie_status_execucao_w <> '40') then

			update	prescr_procedimento a
			set	a.ie_status_execucao = '40',
				a.nm_usuario 	= 'TasyFleuryWS'
			where	a.nr_prescricao = nr_prescricao_w
			and	a.nr_sequencia  in (	select	b.nr_sequencia_prescricao
							from	procedimento_paciente b
							where	b.nr_prescricao = a.nr_prescricao
							and 	b.nr_prescricao = nr_prescricao_w
							and	b.nr_sequencia_prescricao = a.nr_sequencia
							and	b.nr_sequencia_prescricao = nr_seq_prescricao_w);
		end if;

	elsif (ds_tipo_resultado = 'P') then
	
		update	laudo_paciente
		set	dt_aprovacao = sysdate,
			nm_usuario_aprovacao = 'TasyFleuryWS',
			dt_liberacao = null
		where	nr_sequencia = nr_sequencia_p;
		
		/* *****  Atualiza status execu��o na prescri��o ***** */
		update	prescr_procedimento a
		set	a.ie_status_execucao 	= '35', --Aguardando segunda aprova��o
			a.nm_usuario 	= 'TasyFleuryWS'
		where	a.nr_prescricao = nr_prescricao_w
		and	a.nr_sequencia  in (	select	b.nr_sequencia_prescricao
						from	procedimento_paciente b
						where	b.nr_prescricao 	= a.nr_prescricao
						and	b.nr_sequencia_prescricao = a.nr_sequencia
						and	b.nr_laudo 		= nr_seq_prescricao_w);						
						
		select 	max(ie_status_execucao)
		into	ie_status_execucao_w
		from	prescr_procedimento a
		where	a.nr_prescricao = nr_prescricao_w
		and	a.nr_sequencia  = nr_seq_prescricao_w;

		if	(ie_status_execucao_w <> '35') then
		
			update	prescr_procedimento a
			set	a.ie_status_execucao = '35', --Aguardando segunda aprova��o
				a.nm_usuario 	= 'TasyFleuryWS'
			where	a.nr_prescricao = nr_prescricao_w
			and	a.nr_sequencia  in (	select	b.nr_sequencia_prescricao
							from	procedimento_paciente b
							where	b.nr_prescricao = a.nr_prescricao
							and 	b.nr_prescricao = nr_prescricao_w
							and	b.nr_sequencia_prescricao = a.nr_sequencia
							and	b.nr_sequencia_prescricao = nr_seq_prescricao_w);
		end if;
	end if;
	
	commit;
	
end if;

end fleury_atualizar_status_laudo;
/
