create or replace
procedure pls_desfazer_susp_segurado
			(	nr_seq_segurado_p	number,
				nm_usuario_p		varchar2,
				nr_titulo_p		number) is

nr_seq_pagador_w	number(10);
ds_motivo_fim_susp_w	varchar2(2000)	:= '';
dt_rescisao_w		date	:= null;
dt_suspensao_pagador_w	date;

begin

if	(nr_seq_segurado_p is not null) then
	select	nr_seq_pagador,
		dt_rescisao
	into	nr_seq_pagador_w,
		dt_rescisao_w
	from	pls_segurado a
	where	a.nr_sequencia	= nr_seq_segurado_p;
	
	if	(nr_titulo_p is not null) then
		ds_motivo_fim_susp_w	:= ds_motivo_fim_susp_w || ' Reativa��o do atendimento de forma autom�tica, ao baixar o t�tulo ' ||
							nr_titulo_p || '.';
	end if;
	
	update	pls_segurado_suspensao
	set	dt_fim_suspensao	= sysdate,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		ds_motivo_fim_susp	= ds_motivo_fim_susp_w
	where	nr_seq_segurado		= nr_seq_segurado_p
	and	dt_fim_suspensao is null
	and	dt_inicio_suspensao is not null;
	
	if	(dt_rescisao_w is null or dt_rescisao_w > sysdate) then
		update	pls_segurado
		set	ie_situacao_atend	= 'A',
			nr_seq_motivo_susp	= null,
			nr_seq_notific_susp	= null
		where	nr_sequencia	= nr_seq_segurado_p;
	else /* Lepinski - OS 419439 - Caso o benefici�rio esteja rescindido, deve desfazer a suspen��o do mesmo, por�m n�o pode alterar a situa��o de atendimento para APTO */
		update	pls_segurado
		set	nr_seq_motivo_susp	= null,
			nr_seq_notific_susp	= null
		where	nr_sequencia	= nr_seq_segurado_p;
	end if;
	
	select	max(dt_suspensao)
	into	dt_suspensao_pagador_w
	from	pls_contrato_pagador
	where	nr_sequencia	= nr_seq_pagador_w;
	
	if	(dt_suspensao_pagador_w is not null) then
		update	pls_contrato_pagador
		set	dt_suspensao	= null
		where	nr_sequencia	= nr_seq_pagador_w;
		
		insert	into	pls_pagador_historico
			(	nr_sequencia, nr_seq_pagador, dt_atualizacao, nm_usuario, cd_estabelecimento,
				dt_atualizacao_nrec, nm_usuario_nrec, ds_historico,
				dt_historico, ds_titulo, nm_usuario_historico,
				ie_tipo_historico, ie_origem)
			values(	pls_pagador_historico_seq.nextval, nr_seq_pagador_w, sysdate, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento,
				sysdate, nm_usuario_p, 'Desfeita a suspens�o do pagador ap�s o pagamento do t�tulo '||nr_titulo_p,
				sysdate, 'Altera��o no cadastro do pagador', nm_usuario_p,
				'S', 'GC');
	end if;
end if;

/* Nao pode dar commit nesta procedure */

end pls_desfazer_susp_segurado;
/