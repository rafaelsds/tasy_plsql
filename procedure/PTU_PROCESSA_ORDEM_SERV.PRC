create or replace
procedure ptu_processa_ordem_serv(	nr_seq_ordem_serv_p  			ptu_requisicao_ordem_serv.nr_sequencia%type,
					ie_possui_pedido_p			varchar2,
					nm_usuario_p				usuario.nm_usuario%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nr_seq_ret_ordem_serv_p		out	ptu_resposta_req_ord_serv.nr_sequencia%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Rotina utilizada para valida��o da transa��o 00806 - Ordem de servi�o do PTU
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin

cd_estabelecimento_w := cd_estabelecimento_p;

--Quando as transa��es s�o geradas pelo WebService, n�o existe estabelecimento definido, ent�o � verificado o estabeleicmento do par�metro
if	( cd_estabelecimento_w is null	) then
	cd_estabelecimento_w := ptu_obter_estab_padrao;
end if;

if	(ie_possui_pedido_p = 'N') then

	ptu_gerar_ord_serv(nr_seq_ordem_serv_p, nm_usuario_p, cd_estabelecimento_w, nr_seq_ret_ordem_serv_p);

	ptu_gerar_itens_ord_serv(nr_seq_ordem_serv_p, nr_seq_ret_ordem_serv_p, nm_usuario_p, cd_estabelecimento_w);

end if;

--Retorna a sequencia da resposta do 00806 - Ordem de servi�o do PTU, esta sequ�ncia � utilizada para gerar o XML de resposta pelo Webservice
select(	select	max(nr_sequencia)
	from	ptu_resposta_req_ord_serv c
	where	c.nr_seq_execucao	= a.nr_sequencia
	and	c.cd_unimed_executora	= a.cd_unimed_executora) nr_seq_ret_ordem_serv
into	nr_seq_ret_ordem_serv_p
from	ptu_requisicao_ordem_serv a
where	a.nr_sequencia = nr_seq_ordem_serv_p;

end ptu_processa_ordem_serv;
/
