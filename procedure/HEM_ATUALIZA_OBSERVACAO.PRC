create or replace 
procedure hem_atualiza_observacao(	nr_sequencia_p 		Number,	
			DS_OBSERVACAO_LAUDO_p	varchar2) is				
				
begin

if	(nvl(nr_sequencia_p,0) > 0 and DS_OBSERVACAO_LAUDO_p is not null) then

	update	HEM_OBSERVACAO_LAUDO
	set	DS_OBSERVACAO_LAUDO = DS_OBSERVACAO_LAUDO_p
	where	nr_sequencia = nr_sequencia_p;
	
	commit;

end if;
end hem_atualiza_observacao;
/
