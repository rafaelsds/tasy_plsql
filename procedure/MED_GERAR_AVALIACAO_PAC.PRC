create or replace
procedure Med_Gerar_Avaliacao_Pac
			(nr_seq_aval_compl_p	in	number,
			nr_seq_tipo_aval_p	in	number,
			cd_medico_p		in 	varchar2,
			nm_usuario_p		in	varchar2,
			nr_seq_avaliacao_p	out	number) is

nr_seq_avaliacao_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);
dt_avaliacao_w		date;

begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_avaliacao_w
from	med_avaliacao_paciente
where 	nr_seq_aval_compl	= nr_seq_aval_compl_p
and	nr_seq_tipo_avaliacao	= nr_seq_tipo_aval_p;


if 	(nr_seq_avaliacao_w = 0) then
	begin

	select	med_avaliacao_paciente_seq.nextval
	into	nr_seq_avaliacao_w
	from	dual;

	insert	into med_avaliacao_paciente
		(nr_sequencia,
		cd_pessoa_fisica,
		cd_medico,
		dt_avaliacao,
		dt_atualizacao,
		nm_usuario,
		ds_observacao,
		nr_seq_tipo_avaliacao,
		ie_avaliacao_parcial,	
		nr_atendimento,
		nr_prescricao,
		nr_seq_aval_compl)
	(select	nr_seq_avaliacao_w,
		b.cd_pessoa_fisica,
		cd_medico_p,
		a.dt_avaliacao,
		sysdate,
		nm_usuario_p,
		null,
		nr_seq_tipo_aval_p, 'N',
		null, null, a.nr_sequencia
	from	med_cliente b,
		med_aval_completa_pac a
	where	a.nr_seq_cliente	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_aval_compl_p);

	end;
end if;

commit;

nr_seq_avaliacao_p	:= nr_seq_avaliacao_w;

end Med_Gerar_Avaliacao_Pac;
/ 