create or replace
procedure pls_liberar_proposta
			(	nr_seq_proposta_p	number,
				ie_acao_p		number,
				ie_commit_p		varchar2,
				nm_usuario_p		varchar2,
				ds_erro_p	out	varchar2) is

/* IE_ACAO_P
	1 - Liberar
	2 - Desfazer liberacao
*/

ie_status_w			Varchar2(1);
ie_aprovar_proposta_w		Varchar2(1)	:= 'N';
ie_liberacao_especial_w		Varchar2(1);
qt_inconsistencia_proposta_w	number(10)	:= 0;
qt_inconsistencia_doc_w		number(10)	:= 0;
qt_inconsistencia_inadimp_w	number(10)	:= 0;
ie_libera_proposta_w		varchar2(1) 	:= 'S';
nr_seq_beneficiario_w		number(10);
ie_exige_historico_w		Varchar2(1)	:= 'N';
qt_historicos_w			number(10);
ds_erro_w			varchar2(4000) 	:= null;
cd_estabelecimento_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_proposta_beneficiario
	where	nr_seq_proposta	= nr_seq_proposta_p;

begin

pls_verificar_parecer_benef(nr_seq_proposta_p, nm_usuario_p);

cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

--Leitura do parametro 23 da OPS - Proposta de adesao eletronica
ie_aprovar_proposta_w	:= nvl(obter_valor_param_usuario(1232, 23, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w), 'N');

--aaschlote 01/11/2010 - OS 251067 - [45] - Permite liberar inconsistencia de inadimplencia
ie_liberacao_especial_w	:= nvl(obter_valor_param_usuario(1232, 45, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w), 'N');

--sideker 05/09/2011 - OS 347732 - [63] - Exigir historico para liberar a proposta de adesao
ie_exige_historico_w	:= nvl(obter_valor_param_usuario(1232, 63, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w), 'N');

if	(ie_acao_p	= 1) then
	
	select	nvl(max(ie_status),'C')
	into	ie_status_w
	from	pls_proposta_adesao
	where	nr_sequencia	= nr_seq_proposta_p;
	
	if	(ie_status_w <> 'C') then
		ds_erro_w := wheb_mensagem_pck.get_texto(206706);
		/* Mensagem: Proposta de adesao nao esta consistida, utilize a opcao "Consistir proposta"! */
	end if;	
	
	if	(ds_erro_w is null) then 	
		select	count(1)
		into	qt_inconsistencia_proposta_w
		from	pls_proposta_validacao
		where	nr_seq_proposta	= nr_seq_proposta_p
		and 	dt_liberacao is null;
		
		select	count(1)
		into	qt_inconsistencia_doc_w
		from	pls_proposta_inconsist_doc
		where	nr_seq_proposta = nr_seq_proposta_p
		and	dt_liberacao is null;
	
		if	(qt_inconsistencia_proposta_w > 0) or
			(qt_inconsistencia_doc_w > 0) then
			ds_erro_w := wheb_mensagem_pck.get_texto(325972);		
			/* Mensagem: Nao sera possivel liberar esta proposta sem a liberacao de todas as inconsistencias. */
		end if;
	end if;	
	
	if	(ds_erro_w is null) then
		select	count(1)
		into	qt_inconsistencia_inadimp_w
		from	pls_proposta_validacao	b,
			pls_proposta_inconsist	a
		where	b.nr_seq_inconsistencia	= a.nr_sequencia
		and	b.nr_seq_proposta	= nr_seq_proposta_p
		and	a.ie_inadimplente	= 'S';
	
		select	count(1)
		into	qt_historicos_w
		from	pls_proposta_historico
		where	nr_seq_proposta = nr_seq_proposta_p;
		
		if	(qt_inconsistencia_inadimp_w = 0) and
			(ie_exige_historico_w = 'N') then
			ie_libera_proposta_w	:= 'S';
		elsif	(qt_inconsistencia_inadimp_w = 0) and
			(ie_exige_historico_w = 'S') and
			(qt_historicos_w > 0) then
			ie_libera_proposta_w	:= 'S';
		elsif	(qt_inconsistencia_inadimp_w > 0) and
			(ie_liberacao_especial_w = 'S') and
			(ie_exige_historico_w = 'N') then
			ie_libera_proposta_w	:= 'S';
		elsif	(qt_inconsistencia_inadimp_w > 0) and
			(ie_liberacao_especial_w = 'S') and
			(ie_exige_historico_w = 'S') and
			(qt_historicos_w > 0) then
			ie_libera_proposta_w	:= 'S';	
		elsif	(qt_inconsistencia_inadimp_w > 0) and
			(ie_liberacao_especial_w = 'N') then
			ie_libera_proposta_w	:= 'N';
		else	
			ie_libera_proposta_w	:= 'N';
		end if;
		
		if	(ie_libera_proposta_w = 'S') then
			update	pls_proposta_adesao
			set	dt_fim_proposta	= sysdate,
				ie_status	= 'E',
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia	= nr_seq_proposta_p;
			
			/*aaschlote 02/05/2011 OS - 314422 - Gerar historico para o beneficiario*/
			open C01;
			loop
			fetch C01 into	
				nr_seq_beneficiario_w;
			exit when C01%notfound;
				begin
				pls_gravar_histor_prop_benef(nr_seq_beneficiario_w,sysdate,'3', wheb_mensagem_pck.get_texto(1168529),nm_usuario_p);
				/*aaschlote 02/11/2011 - OS 378886*/
				pls_vincular_tipo_benef_comer(nr_seq_beneficiario_w,nm_usuario_p);
				end;
			end loop;
			close C01;
			
			if	(ie_aprovar_proposta_w = 'S') then
				pls_aprovar_proposta(nr_seq_proposta_p,'N', nm_usuario_p);
			end if;
		elsif	(ie_libera_proposta_w	= 'N') then
			if	(ie_exige_historico_w = 'S') and
				(qt_historicos_w = 0) then
				ds_erro_w := wheb_mensagem_pck.get_texto(206707);
				/* Mensagem: Nao sera possivel liberar esta proposta sem o registro de pelo menos um historico. Parametro [63] */
			else
				ds_erro_w := wheb_mensagem_pck.get_texto(206711);
				/* Mensagem: Proposta de adesao possui inconsistencia por inadimplencia. e nao pode ser liberada.
					   Verifique o parametro [45] - Permite liberar inconsistencia de inadimplencia */
			end if;
		end if;
	end if;
elsif	(ie_acao_p	= 2) then
	update	pls_proposta_adesao
	set	dt_fim_proposta	= null,
		ie_status	= 'U',
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_proposta_p;
	
	/*aaschlote 02/05/2011 OS - 314422 - Gerar historico para o beneficiario*/
	open C01;
	loop
	fetch C01 into
		nr_seq_beneficiario_w;
	exit when C01%notfound;
		begin
		pls_gravar_histor_prop_benef(nr_seq_beneficiario_w,sysdate,'4', wheb_mensagem_pck.get_texto(1168530),nm_usuario_p);
		end;
	end loop;
	close C01;
	
	select	count(1)
	into	qt_inconsistencia_proposta_w
	from	pls_proposta_validacao
	where	nr_seq_proposta	= nr_seq_proposta_p;
	
	--aaschlote 06/04/2011 OS - 303632
	if	(qt_inconsistencia_proposta_w > 0) then
		delete	pls_proposta_check_list
		where	nr_seq_validacao	in (	select	nr_sequencia
							from	pls_proposta_validacao
							where	nr_seq_proposta	= nr_seq_proposta_p);
		
		delete	pls_proposta_validacao
		where	nr_seq_proposta	= nr_seq_proposta_p;
	end if;
end if;

ds_erro_p := substr(ds_erro_w,1,255);

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end pls_liberar_proposta;
/
