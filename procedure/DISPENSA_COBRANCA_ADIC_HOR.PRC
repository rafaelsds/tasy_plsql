create or replace
procedure Dispensa_cobranca_adic_hor(	nr_sequencia_p		number,
					nr_seq_proc_crit_hor_p  number,
					ds_justificativa_p 	varchar2,
					ie_dispensa_p		varchar2,
					nm_usuario_p		Varchar2) is

begin

insert	into proc_pac_adic_hor(
		NR_SEQUENCIA,
		NR_SEQ_PROPACI,
		NR_SEQ_PROC_CRIT_HOR,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DS_JUSTIFICATIVA,
		IE_ADIC_HORARIO)
	values (proc_pac_adic_hor_seq.NextVal,
		nr_sequencia_p,
		nr_seq_proc_crit_hor_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_justificativa_p,
		decode(ie_dispensa_p, 'S', 'C', 'N', 'D'));
		
commit;

end Dispensa_cobranca_adic_hor;
/