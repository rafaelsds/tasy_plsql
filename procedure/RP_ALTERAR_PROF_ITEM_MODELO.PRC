create or replace
procedure rp_alterar_prof_item_modelo(nr_seq_item_modelo_p	varchar2,
				      cd_profissional_p		varchar2,
				      nm_usuario_p		varchar2) is 

begin

update	rp_pac_modelo_agend_item
set	cd_medico_exec 	= cd_profissional_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia 	= nr_seq_item_modelo_p;

commit;

end rp_alterar_prof_item_modelo;
/