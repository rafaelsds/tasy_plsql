create or replace
PROCEDURE Gerar_Proc_Agenda_Cirurgia(	nr_seq_agenda_p		Number,
					nr_prescricao_p		number,
					nm_usuario_p		Varchar2) IS


cd_procedimento_w		number(15) := 0;
ie_origem_proced_w		number(10,0) := 0;
nr_sequencia_w			number(06,0) := 0;
dt_prescricao_w			date;
cd_convenio_w			number(05,0);
nr_seq_agenda_w			number(10,0);
cd_categoria_w			varchar2(10);
nr_seq_proc_interno_w		number(10,0);
ie_lado_w			varchar(1);
cd_medico_w			varchar2(10);
ie_grava_medico_w		Varchar2(1);
ie_proced_liberado_w		varchar2(1);
qt_procedimento_w		number(8,3);
cd_tipo_cirurgia_w		number(10,0);
ie_porte_w				varchar2(1);
cd_setor_atendimento_w  number(5,0);
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type;
ds_observacao_w agenda_paciente_proc.ds_observacao%type;

cursor	c01 is
	select 	a.cd_procedimento,
		a.ie_origem_proced,
		cd_convenio,
		cd_categoria,
		a.nr_seq_proc_interno,
		a.ie_lado,
		a.cd_medico,
		nvl(a.qt_procedimento,1),
		decode(campo_numerico(obter_dados_proc_interno(a.nr_seq_proc_interno,'TC')),0, null, campo_numerico(obter_dados_proc_interno(a.nr_seq_proc_interno,'TC'))),
		nr_seq_agenda,
		a.ds_observacao
	from 	agenda_paciente_proc a
	where 	nr_sequencia		= nr_seq_agenda_p
	and	nr_seq_agenda_p		<> 0
	and 	ie_proced_liberado_w 	= 'N'
	and	not exists
		(select	1
		from	prescr_procedimento x
		where	x.cd_procedimento	= a.cd_procedimento
		and	x.ie_origem_proced	= a.ie_origem_proced
		and	x.nr_prescricao		= nr_prescricao_p)
	union
	select 	b.cd_procedimento,
		b.ie_origem_proced,
		null,
		null,
		b.nr_seq_proc_interno,
		c.ie_lado,
		c.cd_medico,
		nvl(b.qt_autorizada,1),
		decode(campo_numerico(obter_dados_proc_interno(b.nr_seq_proc_interno,'TC')),0, null, campo_numerico(obter_dados_proc_interno(b.nr_seq_proc_interno,'TC'))),
		0,
		null
	from 	agenda_paciente c,
		procedimento_autorizado b,
		autorizacao_convenio a
	where	a.nr_seq_agenda		= c.nr_sequencia
	and	b.nr_sequencia_autor	= a.nr_sequencia
	and	a.nr_seq_agenda		= nr_seq_agenda_p
	and	nr_seq_agenda_p		<> 0
	and	qt_autorizada		> 0
	and 	ie_proced_liberado_w 	= 'S'
	and	not exists
		(select	1
		from	prescr_procedimento x
		where	x.cd_procedimento	= b.cd_procedimento
		and	x.ie_origem_proced	= b.ie_origem_proced
		and	x.nr_prescricao		= nr_prescricao_p);

begin


select 	dt_prescricao,
		cd_setor_atendimento
into 	dt_prescricao_w,
		cd_setor_atendimento_w
from 	prescr_medica
where 	nr_prescricao = nr_prescricao_p;


obter_param_usuario(871,443,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_grava_medico_w);
obter_param_usuario(871,448,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_proced_liberado_w);


select 	nvl(max (nr_atendimento),0)
into 	nr_atendimento_w
from 	prescr_medica
where 	nr_prescricao = nr_prescricao_p;

if	(nr_atendimento_w > 0) then
	select	max(ie_tipo_atendimento)
	into	ie_tipo_atendimento_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_w;
end if;

open	c01;
loop
fetch	c01 into
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_convenio_w,
	cd_categoria_w,
	nr_seq_proc_interno_w,
	ie_lado_w,
	cd_medico_w,
	qt_procedimento_w,
	cd_tipo_cirurgia_w,
	nr_seq_agenda_w,
	ds_observacao_w;
exit	when c01%notfound;
	begin

	select	nvl(max(nr_sequencia), 0) + 1
	into	nr_seq_agenda_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p;

	select 	substr(max(obter_dados_proc_interno(nr_seq_proc_interno_w,'PO')),1,255)
	into	ie_porte_w
	from 	dual;

	insert into prescr_procedimento(
		nr_prescricao,
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_procedimento,
		ie_urgencia,
		ie_suspenso,
		dt_prev_execucao,
		ie_status_atend,
		dt_atualizacao,
		nm_usuario,
		ie_origem_inf,
		nr_seq_interno,
		ie_avisar_result,
		cd_convenio,
		cd_categoria,
		cd_motivo_baixa,
		nr_seq_proc_interno,
		ie_lado,
		CD_MEDICO_exec,
		ie_porte,
		cd_setor_atendimento,
		cd_tipo_cirurgia,
		ds_observacao) /* Acrescentei o cd_motivo_baixa em 02/10/2007 OS 70363 - Dalcastagne */
	values	(nr_prescricao_p,
		nr_seq_agenda_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_procedimento_w,
		'N', 'N',dt_prescricao_w,
		5, sysdate, nm_usuario_p,
		'1', prescr_procedimento_seq.NextVal,
		'N',
		decode(cd_categoria_w,null,null,cd_convenio_w),
		cd_categoria_w,
		0,
		nr_seq_proc_interno_w,
		ie_lado_w,
		decode(ie_grava_medico_w,'S',cd_medico_w),
		nvl(ie_porte_w,null),
		Obter_Setor_exec_proc_interno(nr_seq_proc_interno_w, nvl(cd_setor_atendimento_w,null), ie_tipo_atendimento_w, cd_convenio_w, cd_categoria_w),
		cd_tipo_cirurgia_w,
		ds_observacao_w);
	end;
end loop;
close c01;

commit;

end gerar_proc_agenda_cirurgia;
/