create or replace
procedure atualizar_equip_paciente_alta(nm_usuario_p	varchar2) is

nr_equipamento_w	number(10);
nr_atendimento_w	number(10);
nr_prescricao_w	        number(10);
cd_pessoa_fisica_w      number(10);
cd_pessoa_atend_w       number(10);
cd_estabelecimento_w	number(4);

Cursor C01 is
	select	a.nr_atendimento,
			nvl(a.cd_estabelecimento,1)
	from	atendimento_paciente a
	where	trunc(a.dt_alta) >= trunc(sysdate) - 7
	and	(exists 
			(select 1 
			from 	rp_paciente_reabilitacao b
			where	b.cd_pessoa_fisica = a.cd_pessoa_fisica) or (obter_se_possui_atend_opme(a.nr_atendimento) = 'S'))
	order by 1;
	
Cursor C02 is
	select	a.nr_sequencia
	from	man_equipamento a,
		material_atend_paciente b
	where	b.nr_atendimento 	= nr_atendimento_w
	and	b.cd_material		= a.cd_material	     
	and	((b.nr_seq_lote_fornec = a.nr_seq_lote_fornec) or 
	         ((b.nr_seq_lote_fornec is null) and (b.nr_serie_material = a.nr_serie)))
	and not exists(	select	1
			from	rp_equipamento c
			where	c.nr_seq_equipamento = a.nr_sequencia) 
	union
	select	a.nr_sequencia
	from	man_equipamento a
	WHERE	obter_se_possui_atend_opme(nr_atendimento_w) = 'S'
	and		exists( select	1
					from	atendimento_paciente x
					where	x.nr_atendimento = nr_atendimento_w
					and		x.cd_pessoa_fisica = a.cd_pessoa_terceiro)	
	and not exists(	select	1
			from	rp_equipamento c
			where	c.nr_seq_equipamento = a.nr_sequencia) 
	order by 1;

begin

open C01;
loop
fetch C01 into	
	nr_atendimento_w,
	cd_estabelecimento_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into
		nr_equipamento_w;
	exit when C02%notfound;
		begin							 
		 select obter_pessoa_atendimento(nr_atendimento_w,'C') 
		 into cd_pessoa_atend_w 
		 from dual; 		 		 								
		 
		 select nvl(max(d.cd_pessoa_fisica),0)
		 into cd_pessoa_fisica_w
		 from rp_paciente_reabilitacao d
		 where d.cd_pessoa_fisica = cd_pessoa_atend_w;
		
		  if ((obter_se_possui_atend_opme(nr_atendimento_w) = 'S') and (cd_pessoa_fisica_w = 0)) then
		   begin
	            insert into rp_paciente_reabilitacao(
		           nr_sequencia,
		  	   dt_atualizacao,
	  	           nm_usuario,
		           cd_pessoa_fisica,
				   cd_estabelecimento)
		    values (rp_paciente_reabilitacao_seq.nextval,
		            sysdate,
		            nm_usuario_p,
		            cd_pessoa_atend_w,
					cd_estabelecimento_w);	
		   end;		   
		  end if;
		   man_atualiza_equip_alta(nr_atendimento_w,
		   			 nr_equipamento_w,
					 nm_usuario_p);
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

commit;

end atualizar_equip_paciente_alta;
/
