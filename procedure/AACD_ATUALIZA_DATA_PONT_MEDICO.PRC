create or replace
procedure aacd_atualiza_data_pont_medico(
			nm_usuario_p		Varchar2) is 

Cursor C01 is
	select	b.nr_sequencia
	from	medico a,
		pontuacao_medico b
	where	b.cd_medico = a.cd_pessoa_fisica
	and	a.dt_admissao is not null
	and	a.ie_situacao = 'A'
	and	b.dt_expirada is null
	and	sysdate > add_months(b.dt_registro,12);

qt_atualizado_w		number(10,0);
c01_w			c01%rowtype;

begin
qt_atualizado_w	:= 0;

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	
	update	pontuacao_medico
	set		dt_expirada = sysdate,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			qt_pontos = 0
	where	nr_sequencia = c01_w.nr_sequencia;

	qt_atualizado_w := qt_atualizado_w + 1;
			
	if	(qt_atualizado_w	= 100) then
		commit;
		qt_atualizado_w	:= 0;
	end if;	
	
	end;
end loop;
close C01;
commit;

end aacd_atualiza_data_pont_medico;
/
