create or replace
procedure fa_cancelar_receita(	nr_sequencia_p		Number,
				nm_usuario_p		Varchar2,
				nr_seq_motivo_canc_p	Number default null) is 

qtd_entrega_w			Number(10);
VarIeConsisteCancel_w	Varchar2(1) := 'S';
nr_seq_regulacao_w		regulacao_atend.nr_sequencia%Type;
ie_integracao_w 		varchar2(2);

begin

Obter_Param_Usuario(10015,52,obter_perfil_ativo,nm_usuario_p,Wheb_Usuario_pck.Get_cd_estabelecimento,VarIeConsisteCancel_w);

if 	(nr_sequencia_p is not null) then

	if (VarIeConsisteCancel_w = 'S') then
		select	count(*)
		into	qtd_entrega_w
		from	fa_entrega_medicacao
		where	nr_seq_receita_amb = nr_sequencia_p
		and	dt_cancelamento is null;
		
		if (qtd_entrega_w > 0) then
			-- Para cancelar a receita � necess�rio cancelar as entregas. Favor verificar
			wheb_mensagem_pck.exibir_mensagem_abort(228528);
		end if;
		
	else
		-- Verificar se tem entrega vinculada a receita
		select	count(*)
		into	qtd_entrega_w
		from	fa_entrega_medicacao
		where	nr_seq_receita_amb = nr_sequencia_p;

		if	(qtd_entrega_w > 0) then
			-- N�o � poss�vel cancelar uma receita que j� teve entrega. Favor verificar
			wheb_mensagem_pck.exibir_mensagem_abort(228529);
		end if;
	end if;
	
	update	fa_receita_farmacia
	set	DT_CANCELAMENTO = sysdate,
		dt_atualizacao  = sysdate,
		nm_usuario	= nm_usuario_p,
		nm_usuario_canc	= nm_usuario_p,
		nr_seq_motivo_canc = nr_seq_motivo_canc_p
	where 	nr_sequencia 	= nr_sequencia_p;
	
	
	Select  max(nr_sequencia)
	into	nr_seq_regulacao_w
	from	regulacao_atend
	where 	nr_Seq_receita_amb = nr_sequencia_p;
	
	if ( nvl(nr_seq_regulacao_w,0) > 0) then
	
		  select	nvl(max('S'),'N')
			into	ie_integracao_w
			from	fa_receita_farmacia r
			where	r.nr_sequencia = nr_seq_regulacao_w
			and		r.nr_seq_origem is not null; 
	
			alterar_status_regulacao(nr_seq_regulacao_w,'CA','',null,'S',null, null, ie_integracao_w);
			
	end if;
	
	
end if;

commit;

end fa_cancelar_receita;
/