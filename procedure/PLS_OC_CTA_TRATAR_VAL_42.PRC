create or replace
procedure pls_oc_cta_tratar_val_42(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Aplicar a validacao da regra de atributos para a estrutura de ocorrencias combinadas.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
------------------------------------------------------------------------------------------------------------------
Pontos de atencao:

Alteracoes:

------------------------------------------------------------------------------------------------------------------
jjung OS 601992 - 13/09/2013- Criacao da rotina
------------------------------------------------------------------------------------------------------------------
dlehmkuhl OS 688483 - 14/04/2014 -

Alteracao:	Modificada a forma de trabalho em relacao a atualizacao dos campos de controle
	que basicamente decidem se a ocorrencia sera ou nao gerada. Foi feita tambem a 
	substituicao da rotina obterX_seX_geraX.

Motivo:	Necessario realizar essas alteracoes para corrigir bugs principalmente no que se
	refere a questao de aplicacao de filtros (passo anterior ao da validacao). Tambem
	tivemos um foco especial em performance, visto que a mesma precisou ser melhorada
	para nao inviabilizar a nova solicitacao que diz que a excecao deve verificar todo
	o atendimento.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_sql_w		varchar2(4000);
dados_select_w		pls_tipos_ocor_pck.dados_select;

var_cur_w		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;

dados_val_regra_atrib_w	pls_tipos_ocor_pck.dados_val_regra_atrib;
nr_seq_selecao_w		pls_conta_ocor_v.nr_sequencia%type;
ds_valor_atrib_w	varchar2(4000);
qt_caracter_atrib_w	pls_integer;
ie_gera_ocorrencia_w	pls_oc_cta_selecao_ocor_v.ie_valido%type;

tb_seq_selecao_w	dbms_sql.number_table;
tb_observacao_w		dbms_sql.varchar2_table;
tb_valido_w		dbms_sql.varchar2_table;
qt_iterac_w		pls_integer;
v_cur			pls_util_pck.t_cursor;
ie_valido_w		varchar2(1);

cursor c_regra_val	(	nr_seq_oc_cta_comb_pc	dados_regra_p.nr_sequencia%type) is
	select	regra_ocor.nr_sequencia nr_seq_validacao,
		regra_ocor.nr_seq_regra_atrib,
		regra_atrib.ie_atributo,
		regra_atrib.qt_minimo_caracter,
		regra_atrib.ie_obrigatorio,
		regra_atrib.ie_qtd_parto,
		regra_atrib.dt_inicio_vigencia_ref,
		regra_atrib.dt_fim_vigencia_ref
	from	pls_oc_cta_val_regra_atrib regra_ocor,
		pls_ocorrencia_conta_atrib regra_atrib
	where	regra_ocor.nr_seq_oc_cta_comb 	= nr_seq_oc_cta_comb_pc
	and	regra_atrib.nr_sequencia	= regra_ocor.nr_seq_regra_atrib;

begin

-- So deve ser executada a validacao caso a regra passada por parametro for valida, do contrario nem aplica a validacao.
if	(dados_regra_p.nr_sequencia is not null) then
	
	qt_iterac_w := 0;
	
	-- Varrer as regras de validacao dos atributos cadastradas para verificar cada uma delas.
	for	r_c_regra_val_w in c_regra_val( dados_regra_p.nr_sequencia ) loop
		
		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
		
		dados_val_regra_atrib_w.nr_sequencia		:= r_c_regra_val_w.nr_seq_regra_atrib;	
		dados_val_regra_atrib_w.ie_atributo		:= r_c_regra_val_w.ie_atributo;
		dados_val_regra_atrib_w.ie_obrigatorio		:= r_c_regra_val_w.ie_obrigatorio;
		dados_val_regra_atrib_w.ie_qtd_parto		:= r_c_regra_val_w.ie_qtd_parto;
		dados_val_regra_atrib_w.qt_minimo_caracter	:= r_c_regra_val_w.qt_minimo_caracter;

		-- Quando for para verificar a regra para quantidade de parto > 0 so deve ser verificado se na conta nao existe
		-- quantidade de parto informada. E ignorado os campos de atributo e todos os outros, portanto e executado
		-- este select que traz as contas que estao na tabela de selecao e nao tem parto informado, para que seja
		-- gerada ocorrencia para as mesmas.
		if	(dados_val_regra_atrib_w.ie_qtd_parto = 'S') then
			
			-- Atualizar o campo ie_valido_temp para N  na tabela PLS_SELECAO_OCOR_CTA  para todos os registros.
			pls_tipos_ocor_pck.atualiza_sel_ie_valido_temp (nr_id_transacao_p, null, 'V', dados_regra_p);
			
			-- Montar o select das contas sem quantidade de parto informada
			ds_sql_w :=	'select	distinct ' || pls_tipos_ocor_pck.enter_w ||
					'	sel.nr_sequencia nr_seq_selecao, ' || pls_tipos_ocor_pck.enter_w ||
					' 	''S'' ie_registro_valido, ' ||pls_tipos_ocor_pck.enter_w||
					' 	null ds_obervacao ' ||pls_tipos_ocor_pck.enter_w||
					'from	pls_oc_cta_selecao_ocor_v	sel, '  || pls_tipos_ocor_pck.enter_w ||
					'	pls_conta_ocor_v		conta '  || pls_tipos_ocor_pck.enter_w ||
					'where	sel.nr_id_transacao	= :nr_id_transacao '  || pls_tipos_ocor_pck.enter_w ||
					'and	sel.ie_valido		= ''S'' ' || pls_tipos_ocor_pck.enter_w ||
					'and	conta.nr_sequencia	= sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
					'and	conta.dt_atendimento_conta between :dt_inicio_vigencia and :dt_fim_vigencia ';
			
			-- Verificar o evento de geracao da regra para definir se busca a informacao dos campos IMP ou dos campos quentes.
			-- Importacao XML
			if	(dados_regra_p.ie_evento = 'IMP') then
				
				-- So traz as contas que nao tem registro de nascimento informado.
				ds_sql_w :=	ds_sql_w || pls_tipos_ocor_pck.enter_w ||
						'and	conta.qt_nasc_total_imp = 0 ';
			-- Qualquer outro valor diferente busca dos campos quentes.
			else
				-- So traz as contas que nao tem registro de nascimento informado.
				ds_sql_w :=	ds_sql_w || pls_tipos_ocor_pck.enter_w ||
						'and	conta.qt_nasc_total = 0 ';
			end if;
			
			-- Comeco do tratamento de execcao dos sqls dinamicos da ocorrencia combinada.
			begin		
			open v_cur for ds_sql_w using 	nr_id_transacao_p, r_c_regra_val_w.dt_inicio_vigencia_ref, r_c_regra_val_w.dt_fim_vigencia_ref;		
				loop
				tb_seq_selecao_w.delete;
				tb_valido_w.delete;	
				tb_observacao_w.delete;
				
				fetch v_cur bulk collect
				into  tb_seq_selecao_w, tb_valido_w, tb_observacao_w
				limit pls_util_cta_pck.qt_registro_transacao_w;
					exit when tb_seq_selecao_w.count = 0;	
					
					pls_tipos_ocor_pck.gerencia_selecao_validacao( tb_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w, 'SEQ', tb_observacao_w, 
										tb_valido_w, nm_usuario_p); 						
				end loop;
				close v_cur;
			exception			
			when others then
				--Fecha cursor
				close v_cur;
				-- Insere o log na tabela e aborta a operacao
				pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p,ds_sql_w,nr_id_transacao_p,nm_usuario_p);
			end;
			
		-- Quando for regra para verificacao de atributos entao deve ter um atributo valido informado.
		-- caso nao tenha nem executa a validacao.
		elsif	(dados_val_regra_atrib_w.ie_atributo is not null) then
			
			-- Obter os dados para montagem do select a ser executado.
			dados_select_w := pls_oc_cta_obter_sel_val_42 (dados_regra_p, dados_val_regra_atrib_w);
		
			-- Montar o select com o retorno da function para validacao do atributo selecionado para a regra.
			ds_sql_w :=	'select	distinct ' || pls_tipos_ocor_pck.enter_w ||
					'	sel.nr_sequencia nr_seq_selecao ' ||
					dados_select_w.ds_campos || pls_tipos_ocor_pck.enter_w ||
					'from	pls_oc_cta_selecao_ocor_v sel, ' || pls_tipos_ocor_pck.enter_w ||
					'	pls_conta_ocor_v conta ' ||
					dados_select_w.ds_tabelas ||
					'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_tipos_ocor_pck.enter_w ||
					'and	sel.ie_valido = ''S'' ' || pls_tipos_ocor_pck.enter_w ||
					'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
					'and	conta.dt_atendimento_conta between :dt_inicio_vigencia and :dt_fim_vigencia ' ||
					dados_select_w.ds_restricao || pls_tipos_ocor_pck.enter_w ||
					'order by atrib';
					
			-- Comeco do tratamento de execcao dos sqls dinamicos da ocorrencia combinada.
			begin
					
				open v_cur for ds_sql_w using 	nr_id_transacao_p, r_c_regra_val_w.dt_inicio_vigencia_ref, r_c_regra_val_w.dt_fim_vigencia_ref;		
					loop
							
					fetch v_cur 
					into  nr_seq_selecao_w, ds_valor_atrib_w;
					exit when v_cur%notfound;
				
					-- Por default deve ser gerada a ocorrencia, pois se a conta se encaixou nos filtros e a regra for em branco, deve ser gerada a ocorrencia para ela.
					ie_gera_ocorrencia_w := 'N';
					ie_valido_w := 'S'; -- Variavel utilizada para consistir padrao TISS
					
					-- Zerar o contador de caracteres.
					qt_caracter_atrib_w := null;
					
					-- Obter o valor valido, sem espacos em branco no comeco ou no fim
					ds_valor_atrib_w := trim(ds_valor_atrib_w);
					
					-- Agora exitem 3 opcoes para a obrigatoriedade
					-- S = Preenchido - Quando a regra definir que o campo deve ser preenchido
					-- V = Nao preenchido - Quando a regra definir que o campo deve ser vazio
					-- N = Nao se aplica -  o preenchimento do campo nao influencia na regra
					
					-- S = Quando a regra definir que o campo e obrigatorio, se ele nao for informado entao gera a ocorrencia
					if	(dados_val_regra_atrib_w.ie_obrigatorio = 'S') then
						
						-- Caso o campo tenha sido informado entao nao deve gerar a ocorrencia.
						if	(ds_valor_atrib_w is null or 
							ds_valor_atrib_w = '') then
							
							ie_gera_ocorrencia_w := 'S';
						end if;
					end if;
					
					-- V = Quando a regra definir que o campo nao deve ser informado, se ele for informado entao gera a ocorrencia
					if	(dados_val_regra_atrib_w.ie_obrigatorio = 'V') then
						
						-- Caso o campo tenha sido informado entao nao deve gerar a ocorrencia.
						if	(ds_valor_atrib_w is not null or 
							ds_valor_atrib_w <> '') then
							
							ie_gera_ocorrencia_w := 'S';
						end if;
					end if;
					

					-- Quando a regra definir uma quantidade minima de caracteres, so deve gerar a ocorrencia caso a quantidade de caracteres seja
					-- inferior a quantidade informada na regra, caso contrario nao gera a ocorrencia
					if	(dados_val_regra_atrib_w.qt_minimo_caracter is not null) then
					
						-- Para evitar o desperdicio de processamento, foi colocado para contar os caracteres apenas quando 
						-- a regra for de quantidade minima.
						qt_caracter_atrib_w := length(ds_valor_atrib_w);
						
						-- Se a quantidade de caracteres informada no campo for maior ou igual a quantidade definida na regra, nao sera gerada a ocorrencia
						if	(qt_caracter_atrib_w is null or
							qt_caracter_atrib_w < dados_val_regra_atrib_w.qt_minimo_caracter) then
							
							ie_gera_ocorrencia_w := 'S';
						end if;
					end if;
					
					-- Somente verifica o padrao TISS se as demais consistencias nao foram validas
					if	(ie_gera_ocorrencia_w = 'N') then
						ie_valido_w := pls_valida_atrib_padrao_tiss (dados_val_regra_atrib_w.ie_atributo, ds_valor_atrib_w);
						
						-- Se estiver fora do padrao TISS, gera a ocorrencia
						if	(ie_valido_w = 'N') then
							ie_gera_ocorrencia_w := 'S';
						end if;
					end if;
					
					if	(ie_gera_ocorrencia_w = 'S') then
						qt_iterac_w	:= qt_iterac_w + 1;
						tb_seq_selecao_w(qt_iterac_w)	:= nr_seq_selecao_w ;
						tb_valido_w(qt_iterac_w)	:= 'S';
						tb_observacao_w(qt_iterac_w)	:= null;	
					end if;
					
					if	(qt_iterac_w = pls_util_cta_pck.qt_registro_transacao_w) then
						
						qt_iterac_w := 0;
						pls_tipos_ocor_pck.gerencia_selecao_validacao( tb_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w, 'SEQ', tb_observacao_w, 
								tb_valido_w, nm_usuario_p); 
					end if;
					
					
				end loop;
				if	(qt_iterac_w > 0 ) then
					qt_iterac_w := 0;
					pls_tipos_ocor_pck.gerencia_selecao_validacao( tb_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w, 'SEQ', tb_observacao_w, 
							tb_valido_w, nm_usuario_p); 
				end if;
				close v_cur;
				
			exception
			when others then
			
				-- Insere o log na tabela e aborta a operacao
				pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p,ds_sql_w,nr_id_transacao_p,nm_usuario_p);
			end;
		end if;
		
	end loop; -- c_regra_val
	
	-- seta os registros que serao validos ou invalidos apos o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;

end pls_oc_cta_tratar_val_42;
/