create or replace
procedure gerar_sigiss_tipo3(	dt_inicio_p		date,
				dt_fim_p		date,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

cd_registro_w		varchar2(1);
nr_linhas_detalhe_w	varchar2(10);
vl_total_nota_w		varchar2(15);
vl_bc_iss_w		varchar2(15);
cd_estabelecimento_w	number(4);
ds_arquivo_ww		varchar2(2000);
				
begin

select 	lpad(to_char(count(*)),'10','0')
into	nr_linhas_detalhe_w
from    nota_fiscal n,
	nota_fiscal_item i,
	operacao_nota o
where   i.nr_sequencia = n.nr_sequencia
and	o.cd_operacao_nf = n.cd_operacao_nf
and	n.cd_estabelecimento = cd_estabelecimento_p
and	n.dt_emissao between dt_inicio_p and fim_dia(dt_fim_p);

select	'9'											cd_registro,
	lpad(nvl(elimina_caractere_especial(campo_mascara_virgula(sum(n.vl_total_nota))),' '),'15','0') 	vl_total_nota,
       	lpad(nvl(elimina_caractere_especial(campo_mascara_virgula(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')))),' '),'15','0') vl_bc_iss,
	n.cd_estabelecimento									cd_estabelecimento
into	cd_registro_w,
	vl_total_nota_w,
	vl_bc_iss_w,
	cd_estabelecimento_w
from    nota_fiscal n,
	nota_fiscal_item i,
	operacao_nota o
where   i.nr_sequencia = n.nr_sequencia
and	o.cd_operacao_nf = n.cd_operacao_nf
and	n.cd_estabelecimento = cd_estabelecimento_p
and	n.dt_emissao between dt_inicio_p and fim_dia(dt_fim_p)
group by n.cd_estabelecimento;


ds_arquivo_ww := cd_registro_w		||
		nr_linhas_detalhe_w	||
		vl_total_nota_w		||
		vl_bc_iss_w;

insert into w_inss_direp_arquivo(nr_sequencia,
				 cd_estabelecimento,
				 dt_atualizacao,
				 nm_usuario,
				 dt_atualizacao_nrec,
				 nm_usuario_nrec,
				 ds_arquivo_w)
			values	(w_inss_direp_arquivo_seq.nextval,
				 cd_estabelecimento_w,
				 sysdate,
				 nm_usuario_p,
				 sysdate,
				 nm_usuario_p,
				 ds_arquivo_ww ); 
commit;

end gerar_sigiss_tipo3;
/