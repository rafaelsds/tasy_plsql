create or replace
procedure PPM_METRICA_OUT_PROJECT(
			nr_seq_metrica_p		number,
			nr_seq_objetivo_metrica_p	number,
			cd_pessoa_fisica_p		varchar2,
			nm_usuario_p			varchar2,
			dt_referencia_p			date) is
			
dt_referencia_w 	date;
resultado_w			number;
qt_hora_prev_w		number;
qt_hora_real_w		number;

begin

PPM_GRAVAR_RESULTADO(nr_seq_objetivo_metrica_p,dt_referencia_p, 0, null, null, nm_usuario_p);

end PPM_METRICA_OUT_PROJECT;
/
