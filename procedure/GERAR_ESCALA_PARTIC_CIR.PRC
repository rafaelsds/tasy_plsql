create or replace
procedure gerar_escala_partic_cir	(	nr_seq_agenda_p		number,
						nm_usuario_p		varchar,
						cd_estabelecimento_p	number) 
					as
					
qt_registros_w		number(10);	
dia_da_semana_w		varchar(100);
hr_inicio_w		date;
hr_fim_w		date;
cd_agenda_w		number(10);
nr_seq_equipe_w		number(10);
cd_funcao_w		number(3);
cd_pessoa_fisica_w	varchar2(10);
					
cursor	c01 is
	select 	a.cd_pessoa_fisica,
		a.nr_seq_equipe,
		a.cd_funcao
	from   	regra_escala_partic_cir a
	where	nvl(a.ie_situacao,'A') = 'A'
	and	nvl(a.cd_agenda,cd_agenda_w) = cd_agenda_w
	and	((nvl(a.ie_dia_semana,dia_da_semana_w) = dia_da_semana_w) or ((nvl(a.ie_dia_semana,dia_da_semana_w) = 9) and (dia_da_semana_w not in (7,1))))
	and	hr_inicio_w	between nvl(a.dt_inicio_agenda,to_date('01/01/1900 00:00:01','dd/mm/yyyy hh24:mi:ss')) and nvl(a.dt_fim_vigencia,to_date('01/01/2099 23:59:59','dd/mm/yyyy hh24:mi:ss'))
	and	((hr_inicio_w	between	to_date(to_char(hr_inicio_w,'dd/mm/yyyy') || ' ' || nvl(to_char(a.hr_inicio,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
					to_date(to_char(hr_inicio_w,'dd/mm/yyyy') || ' ' || nvl(to_char(a.hr_fim,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss')) or
		 (hr_fim_w  		between to_date(to_char(hr_inicio_w,'dd/mm/yyyy') || ' ' || nvl(to_char(a.hr_inicio,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
					to_date(to_char(hr_inicio_w,'dd/mm/yyyy') || ' ' || nvl(to_char(a.hr_fim,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss')) or
		 ((hr_inicio_w 	< to_date(to_char(hr_inicio_w,'dd/mm/yyyy') || ' ' || nvl(to_char(a.hr_inicio,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss')) and
		  (hr_fim_w > to_date(to_char(hr_inicio_w,'dd/mm/yyyy') || ' ' || nvl(to_char(a.hr_fim,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss'))))
	and	not 	exists (select	1
				from	profissional_agenda x
				where	((x.cd_profissional 	= a.cd_pessoa_fisica) or (x.nr_seq_equipe	= a.nr_seq_equipe))
				and	x.nr_seq_agenda		= nr_seq_agenda_p);
					
begin

if	(nvl(nr_seq_agenda_p,0) > 0) then
	select 	count(*)
	into	qt_registros_w
	from   	regra_escala_partic_cir
	where	nvl(ie_situacao,'A') = 'A';
	
	if	(qt_registros_w > 0) then
		SELECT 	max(hr_inicio),
			max(hr_inicio) + (max(nr_minuto_duracao) /1440) - (1/86400),
			max(cd_agenda)
		into	hr_inicio_w,
			hr_fim_w,
			cd_agenda_w
		FROM   	agenda_paciente
		WHERE  	nr_sequencia = nr_seq_agenda_p
		and	ie_status_agenda not in ('C','L','B');
		
		if	(hr_inicio_w is not null) then
			dia_da_Semana_w := Obter_Cod_Dia_Semana(hr_inicio_w);
			open C01;
			loop
			fetch C01 into	
				cd_pessoa_fisica_w,
				nr_seq_equipe_w,
				cd_funcao_w;
			exit when C01%notfound;
				begin
				insert	into	profissional_agenda(
							nr_sequencia, 
							cd_profissional, 
							cd_funcao, 
							nr_seq_agenda, 
							dt_atualizacao, 
							nm_usuario, 
							dt_atualizacao_nrec, 
							nm_usuario_nrec, 
							ie_indicacao, 
							nr_seq_equipe, 
							cd_especialidade)
				values			(profissional_agenda_seq.nextval, 
							cd_pessoa_fisica_w, 
							cd_funcao_w, 
							nr_seq_agenda_p, 
							sysdate, 
							nm_usuario_p, 
							sysdate, 
							nm_usuario_p, 
							'N', 
							nr_seq_equipe_w, 
							null);
				commit;			
				end;
			end loop;
			close C01;
		end if;
	end if;	
end if;	
		
end gerar_escala_partic_cir;
/	