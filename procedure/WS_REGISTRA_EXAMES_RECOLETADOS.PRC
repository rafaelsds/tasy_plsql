create or replace
procedure ws_registra_exames_recoletados (	nr_prescricao_p		number,
						nr_seq_prescricao_p	number,
						nm_usuario_p	varchar2) is 

cd_estabelecimento_w		number(4);
						
begin

if	(nr_prescricao_p is not null) and
	(nr_seq_prescricao_p is not null) then
	
	update	prescr_procedimento
	set	dt_integracao_recoleta = sysdate,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_seq_prescricao_p;	
	
end if;


commit;

end ws_registra_exames_recoletados;
/
