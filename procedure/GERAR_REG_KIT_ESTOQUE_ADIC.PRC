create or replace
procedure gerar_reg_kit_estoque_adic(	cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				nr_seq_proc_p		number,
				nr_sequencia_p		number) is 

cd_kit_material_w		number(5);
nr_sequencia_w		number(10);
nr_seq_kit_estoque_w	number(10);	
nr_seq_pac_pedido_w	number(10);	
nr_seq_reg_kit_w		number(10);
cd_local_estoque_w	number(10);
ie_commit_w		varchar2(1) := 'N';
				
Cursor C01 is
	
select	c.cd_kit_material
from  	proc_interno b,
	proc_interno_kit c
where	b.nr_sequencia = nr_seq_proc_p
and     c.nr_seq_proc_interno = b.nr_sequencia;

begin

select 	max(nr_sequencia)
into	nr_seq_pac_pedido_w
from 	agenda_pac_pedido
where 	nr_seq_agenda = nr_sequencia_p;

if	(nr_seq_pac_pedido_w is not null) then
	open C01;
	loop
	fetch C01 into	
		cd_kit_material_w;
	exit when C01%notfound;
		begin
		ie_commit_w := 'S';
			
		select 	max(nr_sequencia)
		into	nr_seq_reg_kit_w
		from 	kit_estoque_reg
		where 	nr_seq_pedido_agenda = nr_seq_pac_pedido_w;
		
		select 	nvl(max(cd_local_estoque),0)
		into	cd_local_estoque_w
		from	kit_estoque
		where	cd_kit_material = cd_kit_material_w
		and	nr_seq_reg_kit = nr_seq_reg_kit_w;
		
		select	kit_estoque_seq.nextval
		into	nr_seq_kit_estoque_w
		from	dual;
		
		if	(nr_seq_reg_kit_w is not null) then
			insert 	into kit_estoque(
					nr_sequencia, 
					cd_kit_material,
					dt_atualizacao,
					nm_usuario,
					dt_montagem,
					nm_usuario_montagem,
					dt_utilizacao,           
					nm_usuario_util,
					nr_cirurgia,
					cd_estabelecimento,
					cd_medico,
					nr_prescricao,
					nr_atendimento,
					cd_local_estoque,
					nr_seq_reg_kit,
					ie_status)
			values	(	nr_seq_kit_estoque_w,
					cd_kit_material_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					null,
					null,
					null,
					cd_estabelecimento_p,
					null,
					null,
					null,
					decode(cd_local_estoque_w,0,'',cd_local_estoque_w),
					nr_seq_reg_kit_w,
					null);
		end if;			
		end;
	end loop;
	close C01;

	if	(ie_commit_w = 'S') then
		commit;
	end if;
end if;	

end gerar_reg_kit_estoque_adic;
/