create or replace
procedure ctb_inicializar_rateio(	nr_seq_criterio_p	number,
					nm_usuario_p		Varchar2) is 


begin

update	ctb_criterio_rateio_item
set	pr_rateio	= 0,
	qt_rateio	= 0,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_seq_criterio	= nr_seq_criterio_p;

commit;

end ctb_inicializar_rateio;
/