create or replace
procedure replica_roteiros_modulos( nr_seq_mod_destino_p	number,
				    nm_usuario_p		varchar2,
				    nr_seq_roteiro_p        	number) is 
				   
nr_sequencia_w		number;
nr_sequencia_ww 	number;
nr_sequencia_apres_w 	number;
ds_item_w	    	varchar2(255);
pr_grau_importancia_ww 	varchar2(10);
ie_situacao_w		varchar2(1);
nr_seq_apres_w 		number;
ie_nao_se_aplica_w 	varchar2(1);
ds_roteiro_w   		varchar2(255);
pr_grau_importancia_w 	number;

cursor c01 is
	select  a.nr_seq_apres,
		a.ds_item,
		a.ie_nao_se_aplica,
		a.pr_grau_importancia,
		a.ie_situacao
	from	proj_tp_rot_item a
	where	nr_seq_roteiro = nr_seq_roteiro_p;

begin
	select  max(a.ds_roteiro),
		max(a.pr_grau_importancia)
	into	ds_roteiro_w,
		pr_grau_importancia_w
	from    proj_tp_roteiro a,
		modulo_implantacao b
	where	a.nr_seq_modulo =  b.nr_sequencia
	and     a.nr_sequencia = nr_seq_roteiro_p;

	select	proj_tp_roteiro_seq.nextval
	into	nr_sequencia_w
	from	dual;


	insert into proj_tp_roteiro( 
					 nr_sequencia,           
					 dt_atualizacao,        
					 nm_usuario,             
					 dt_atualizacao_nrec,
					 nm_usuario_nrec,        
					 ds_roteiro,             
					 ie_situacao,            
					 nr_seq_modulo,          
					 pr_grau_importancia
					 )    
					values
					(
					nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					ds_roteiro_w,
					'A',
					nr_seq_mod_destino_p,
					pr_grau_importancia_w					
					);
					
open c01;
loop
fetch c01 into	
		nr_seq_apres_w,	
		ds_item_w,
		ie_nao_se_aplica_w,
		pr_grau_importancia_ww,
		ie_situacao_w;
	exit when c01%notfound;
	
	begin
		select	proj_tp_rot_item_seq.nextval
		into	nr_sequencia_ww
		from	dual;
	
		insert into proj_tp_rot_item(			
					 nr_sequencia,         
					 nr_seq_roteiro,         
					 dt_atualizacao,        
					 nm_usuario,             
					 dt_atualizacao_nrec,
					 nm_usuario_nrec,        
					 ds_item,                
					 ie_nao_se_aplica,       
					 nr_seq_apres,           
					 pr_grau_importancia,    
					 ie_situacao   
					)
					values
					(
					nr_sequencia_ww,
					nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					ds_item_w,
					ie_nao_se_aplica_w,
					nr_seq_apres_w,
					pr_grau_importancia_ww,
					ie_situacao_w
					);
	end;
end loop;
close c01;

commit;

end replica_roteiros_modulos;
/