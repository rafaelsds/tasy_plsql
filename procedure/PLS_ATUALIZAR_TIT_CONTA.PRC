create or replace
procedure pls_atualizar_tit_conta
			(	dt_referencia_p		Date,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

nr_titulo_w			number(10);
nr_seq_protocolo_w		number(10);
nr_seq_nota_fiscal_w		number(10);
ie_tipo_despesa_w		varchar2(1);
vl_liberado_w			number(15,2);
ie_tipo_item_w			varchar2(1);
cd_conta_contabil_w		varchar2(20);
cd_conta_cred_w			varchar2(20);
nr_item_nf_w			number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_material_w			number(6);
nr_seq_conta_financ_w		number(10);
cd_operacao_nf_w		number(4);
cd_conta_financ_w		number(10);
nr_seq_classif_w		number(10);
vl_titulo_w			number(15,2);
vl_total_item_nf_w		number(15,2);
nr_contrato_w			number(10);
cd_centro_custo_w		number(8);
nr_seq_lote_pag_w		number(10);
vl_imposto_w			number(15,2);
vl_imposto_rateio_w		number(15,2);
vl_classif_w			number(15,2);
vl_saldo_titulo_w		number(15,2);

Cursor C01 is
	select	b.nr_titulo,
		0,
		0,
		b.vl_titulo,
		a.nr_sequencia
	from	titulo_pagar         b,
		pls_lote_protocolo   a
	where	a.nr_sequencia	= b.nr_seq_lote_res_pls
	and	b.ie_tipo_titulo	<> '4'
	and	a.dt_mes_competencia	= dt_referencia_p
	and	b.ie_situacao	<> 'C'
	UNION
	select	c.nr_titulo,
		b.nr_sequencia,
		b.cd_operacao_nf,
		c.vl_titulo,
		a.nr_sequencia
	from	pls_lote_protocolo a,
		nota_fiscal b,
		titulo_pagar c
	where	a.nr_sequencia    	= b.nr_seq_lote_res_pls
	and	b.nr_sequencia    	= c.nr_seq_nota_fiscal
	and	a.dt_mes_competencia	= dt_referencia_p
	and	c.ie_tipo_titulo  	<> '4'
	and	b.ie_situacao 	= '1'
	order by 	nr_titulo;

Cursor C02 is
	select	b.ie_tipo_despesa,
		sum(b.vl_liberado),
		'P',
		b.cd_conta_cred
	from	pls_conta a,
		pls_conta_proc b,
		pls_protocolo_conta c,
		pls_prot_conta_titulo d,
		pls_lote_protocolo e
	where	a.nr_sequencia		= b.nr_seq_conta
	and	c.nr_sequencia		= a.nr_seq_protocolo
	and	c.nr_sequencia		= d.nr_seq_protocolo
	and	e.nr_sequencia		= d.nr_seq_lote
	and	e.nr_sequencia		= nr_seq_lote_pag_w
	and	b.cd_conta_cred	is not null
	group by	b.ie_tipo_despesa,
			b.cd_conta_cred
	union all
	select	b.ie_tipo_despesa,
		sum(b.vl_liberado),
		'M',
		b.cd_conta_cred
	from	pls_conta a,
		pls_conta_mat b,
		pls_protocolo_conta c,
		pls_prot_conta_titulo d,
		pls_lote_protocolo e
	where	a.nr_sequencia		= b.nr_seq_conta
	and	c.nr_sequencia		= a.nr_seq_protocolo
	and	c.nr_sequencia		= d.nr_seq_protocolo
	and	e.nr_sequencia		= d.nr_seq_lote
	and	e.nr_sequencia		= nr_seq_lote_pag_w
	and	b.cd_conta_cred	is not null
	group by	b.ie_tipo_despesa,
			b.cd_conta_cred;


begin

select	cd_conta_financ_conta
into	cd_conta_financ_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

open C01;
loop
fetch C01 into	
	nr_titulo_w,
	nr_seq_nota_fiscal_w,
	cd_operacao_nf_w,
	vl_titulo_w,
	nr_seq_lote_pag_w;
exit when C01%notfound;
	begin
	nr_seq_classif_w	:= 0;
	
	delete	from titulo_pagar_classif
	where	nr_titulo	= nr_titulo_w;
	
	if	(nvl(nr_seq_nota_fiscal_w,0) <> 0) then
		open C02;
		loop
		fetch C02 into	
			ie_tipo_despesa_w,
			vl_liberado_w,
			ie_tipo_item_w,
			cd_conta_cred_w;
		exit when C02%notfound;
			begin
			if	(ie_tipo_item_w	= 'P') then
				select	nvl(max(cd_procedimento),0),
					nvl(max(ie_origem_proced),0)
				into	cd_procedimento_w,
					ie_origem_proced_w
				from	pls_tipo_desp_proc
				where	ie_tipo_despesa	= ie_tipo_despesa_w
				and	cd_estabelecimento	= cd_estabelecimento_p;
				if	(cd_procedimento_w = 0) then
					wheb_mensagem_pck.exibir_mensagem_abort(189002,'IE_TIPO_DESPESA='||ie_tipo_despesa_w);
				end if;
				if	(nvl(cd_procedimento_w,0) <> 0) then
					select	max(nr_item_nf)
					into	nr_item_nf_w
					from	nota_fiscal_item
					where	nr_sequencia	= nr_seq_nota_fiscal_w
					and	cd_procedimento	= cd_procedimento_w;
					
					if	(nvl(nr_item_nf_w,0) <> 0) then
						select	vl_total_item_nf,
							nr_contrato,
							cd_centro_custo
						into	vl_total_item_nf_w,
							nr_contrato_w,
							cd_centro_custo_w
						from	nota_fiscal_item
						where	nr_sequencia	= nr_seq_nota_fiscal_w
						and	nr_item_nf	= nr_item_nf_w;
					end if;
					
				end if;
			elsif	(ie_tipo_item_w	= 'M') then
				select	max(b.cd_material)
				into	cd_material_w
				from	pls_tipo_desp_mat a,
					pls_material b
				where	a.nr_seq_material	= b.nr_sequencia
				and	a.ie_tipo_despesa	= ie_tipo_despesa_w
				and	a.cd_estabelecimento	= cd_estabelecimento_p;
				
				if	(nvl(cd_material_w,0) <> 0) then
					select	nr_item_nf,
						vl_total_item_nf,
						nr_contrato,
						cd_centro_custo
					into	nr_item_nf_w,
						vl_total_item_nf_w,
						nr_contrato_w,
						cd_centro_custo_w
					from	nota_fiscal_item
					where	nr_sequencia	= nr_seq_nota_fiscal_w
					and	cd_material	= cd_material_w;
				end if;
			end if;
			
			if	(nvl(nr_item_nf_w,0) > 0) then
				update	nota_fiscal_item
				set	cd_conta_contabil	= cd_conta_cred_w,
					nr_seq_conta_financ	= cd_conta_financ_w
				where	nr_sequencia		= nr_seq_nota_fiscal_w
				and	nr_item_nf		= nr_item_nf_w;
			end if;
			end;
		end loop;
		close C02;
	end if;
	
	select	sum(a.vl_imposto)
	into	vl_imposto_w
	from	titulo_pagar_imposto a
	where	a.nr_titulo	= nr_titulo_w;
		
	open C02;
	loop
	fetch C02 into	
		ie_tipo_despesa_w,
		vl_liberado_w,
		ie_tipo_item_w,
		cd_conta_cred_w;
	exit when C02%notfound;
		begin
		if	(nvl(vl_imposto_w,0) <> 0) then
			vl_imposto_rateio_w	:= round((vl_liberado_w / vl_titulo_w) * vl_imposto_w,2);
			vl_classif_w	:= vl_liberado_w - vl_imposto_rateio_w;
		else
			vl_classif_w	:= vl_liberado_w;
		end if;
		
		nr_seq_classif_w	:= nr_seq_classif_w + 1;
		
		insert into Titulo_Pagar_Classif(
			nr_titulo, nr_sequencia, vl_titulo,
			dt_atualizacao, nm_usuario, 
			cd_conta_contabil, cd_centro_custo,
			nr_seq_conta_financ)
		values(	nr_titulo_w, nr_seq_classif_w, vl_classif_w,
			sysdate, nm_usuario_p, 
			cd_conta_cred_w, cd_centro_custo_w,
			cd_conta_financ_w);
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

commit;

end pls_atualizar_tit_conta;
/