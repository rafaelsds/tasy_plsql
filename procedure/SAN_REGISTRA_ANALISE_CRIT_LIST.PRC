create or replace
procedure san_registra_analise_crit_list(
		nr_seq_producao_p		number,
		nr_seq_doacao_p		number,
		nr_seq_analise_lista_p	varchar2,
		nm_usuario_p		varchar2) is 

nr_seq_analise_lista_w	varchar2(2000);
nr_seq_analise_w		number(10);

begin
nr_seq_analise_lista_w	:= nr_seq_analise_lista_p;

while nr_seq_analise_lista_w is not null loop 
	begin
	nr_seq_analise_w		:= substr(nr_seq_analise_lista_w, 1, instr(nr_seq_analise_lista_w, ',') - 1);
	nr_seq_analise_lista_w	:= substr(nr_seq_analise_lista_w, instr(nr_seq_analise_lista_w, ',') + 1, length(nr_seq_analise_lista_w));

	san_registrar_analise_critica(
		nr_seq_producao_p,
		nr_seq_doacao_p,
		nr_seq_analise_w,
		nm_usuario_p);
	end;
end loop;

end san_registra_analise_crit_list;
/