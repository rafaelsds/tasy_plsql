create or replace
procedure pls_gerar_valor_cart_benef
			(	nr_seq_segurado_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_carteira_w		number(10);
dt_inicio_vigencia_w		date;
nr_via_solicitacao_w		number(10);
nr_seq_contrato_w		number(10);
nr_seq_regra_w			number(10);
nr_seq_segurado_preco_w		number(10);
vl_via_adicional_w		number(15,2);
vl_preco_pre_w			number(15,2);
pr_via_adicional_w		number(7,4);
nr_seq_plano_w			number(10);
nr_seq_intercambio_w		number(10);
dt_contratacao_w		pls_segurado.dt_contratacao%type;

begin

select	max(nr_sequencia)
into	nr_seq_carteira_w
from	pls_segurado_carteira
where	nr_seq_segurado	= nr_seq_segurado_p;

select	nr_seq_contrato,
	nr_seq_intercambio,
	nr_seq_plano,
	dt_contratacao
into	nr_seq_contrato_w,
	nr_seq_intercambio_w,
	nr_seq_plano_w,
	dt_contratacao_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

if	(nr_seq_carteira_w is not null) then
	select	dt_inicio_vigencia,
		nr_via_solicitacao
	into	dt_inicio_vigencia_w,
		nr_via_solicitacao_w
	from	pls_segurado_carteira
	where	nr_sequencia	= nr_seq_carteira_w;
	
	pls_obter_regra_via_adic(nr_seq_contrato_w, nr_seq_intercambio_w, nr_seq_plano_w, nr_via_solicitacao_w, 'N', nm_usuario_p, cd_estabelecimento_p, dt_contratacao_w, nr_seq_regra_w, vl_via_adicional_w, pr_via_adicional_w);
	
	if	(nr_seq_regra_w is not null) then
		select	vl_via_adicional,
			tx_via_adicional
		into	vl_via_adicional_w,
			pr_via_adicional_w
		from	pls_regra_segurado_cart
		where	nr_sequencia	= nr_seq_regra_w;
		
		if	(nvl(pr_via_adicional_w,0) <> 0) then
			select	max(a.nr_sequencia)
			into	nr_seq_segurado_preco_w
			from	pls_segurado_preco	a,
				pls_segurado		b
			where	a.nr_seq_segurado	= b.nr_sequencia
			and	b.nr_sequencia		= nr_seq_segurado_p
			and	a.dt_liberacao	is not null;
			
			if	(nr_seq_segurado_preco_w is not null) then
				select	vl_preco_atual
				into	vl_preco_pre_w
				from	pls_segurado_preco
				where	nr_sequencia	= nr_seq_segurado_preco_w;
			else
				vl_preco_pre_w	:= 0;
			end if;
			
			vl_via_adicional_w	:= (pr_via_adicional_w * vl_preco_pre_w) / 100;
		end if;
		
		update	pls_segurado_carteira
		set	nr_seq_regra_via	= nr_seq_regra_w,
			vl_via_adicional	= vl_via_adicional_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_seq_carteira_w;
	end if;
end if;

end pls_gerar_valor_cart_benef;
/