create or replace
procedure importa_preco_pj(	CD_CGC_P			Varchar2,
			CD_ESTABELECIMENTO_P		NUMBER,
			NM_USUARIO_P			Varchar2,
			DT_VIGENCIA_P			DATE,
			CD_ITEM_P			Varchar2,
			DS_ITEM_P			Varchar2,
			CD_BARRA_P			Varchar2,
			VL_ITEM_P			NUMBER,
			DT_VIGENCIA_FIM_P		varchar2 default null,
			CD_PRODUTO_P			varchar2 default null) is

nr_sequencia_w		Number(10);
cd_material_w		Number(6);
dt_vigencia_w		date;
qt_registros_w		number(10);
ie_envia_sem_barras_w	varchar2(1);

BEGIN

if (dt_vigencia_fim_p = '0') then
	dt_vigencia_w := null;
else	
	dt_vigencia_w := to_date(dt_vigencia_fim_p,'dd/mm/yyyy');
end if;

select	preco_pj_seq.nextval
into	nr_sequencia_w
from	dual;

select	Obter_cod_material_barra(cd_barra_p, cd_cgc_p)
into	cd_material_w
from	dual;

obter_param_usuario(6,72,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_envia_sem_barras_w);

if (ie_envia_sem_barras_w = 'N') then

	select	nvl(count(*),0)
	into	qt_registros_w
	from	material
	where	cd_material = cd_produto_p;
	
	if (qt_registros_w > 0) then
		cd_material_w := cd_produto_p;
	else
		cd_material_w := '';
	end if;
end if;

insert into preco_pj(
		nr_sequencia,
		cd_estabelecimento,
		cd_cgc,
		dt_atualizacao,
		nm_usuario,
		dt_vigencia,
		dt_vigencia_fim,
		cd_item,
		ds_item,
		cd_barra,
		vl_item,
		cd_material)
	values(
		nr_sequencia_w,
		cd_estabelecimento_p,
		cd_cgc_p,
		sysdate,
		nm_usuario_p,
		dt_vigencia_p,
		dt_vigencia_w,
		cd_item_p,
		ds_item_p,
		cd_barra_p,
		vl_item_p,
		cd_material_w);

commit;

end importa_preco_pj;
/