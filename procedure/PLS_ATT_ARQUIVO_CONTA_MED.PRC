create or replace
procedure pls_att_arquivo_conta_med	(	nr_sequencia_p	number,
						ds_arquivo_p 	varchar2,
						nm_usuario_p	Varchar2) is 

begin
	update 	pls_conta_medica_ted 
        set     ds_arquivo 		= ds_arquivo_p,
		dt_ultima_geracao 	= sysdate,
		nm_usuario_geracao 	= nm_usuario_p,
		dt_atualizacao 		= sysdate,
		nm_usuario 		= nm_usuario_p
        where  	nr_sequencia 		= nr_sequencia_p;
commit;

end pls_att_arquivo_conta_med;
/