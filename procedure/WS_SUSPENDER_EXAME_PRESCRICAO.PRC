create or replace
procedure ws_suspender_exame_prescricao (	nr_prescricao_p	number,
					nr_seq_prescr_p		number,
					ds_motivo_p		varchar2,
					nm_usuario_p		Varchar2,
					cd_erro_p		out varchar2,
					ds_erro_p		out varchar2) is 

ie_erro_w		varchar2(1);
											
/* C�digos de erro
0 - SUCESSO
1 - Dados n�o informados
	1.1 - Prescri��o n�o informada
	1.2 - Sequ�ncia da prescri��o n�o informada
2 - Dados n�o encontrados
*/
											
begin

cd_erro_p	:= 0;

if	(nr_prescricao_p is null) then
	cd_erro_p	:= 1.1;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277599,null);
elsif (nr_seq_prescr_p is null) then
	cd_erro_p	:= 1.2;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277600,null);
end if;

if	(cd_erro_p = 0) then
	
	select	decode(count(*),0,'N','S')
	into	ie_erro_w
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_p
	and	a.nr_sequencia = nr_seq_prescr_p;
	
	if	(ie_erro_w = 'N') then
		cd_erro_p	:= 2.1;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278764,null);
	else
		begin
		Suspender_item_Prescricao(nr_prescricao_p,
					nr_seq_prescr_p,
					null,
					ds_motivo_p,
					'PRESCR_PROCEDIMENTO',
					nm_usuario_p);
		exception
		when others then
			cd_erro_p	:= 3;
			ds_erro_p	:= substr(WHEB_MENSAGEM_PCK.get_texto(278088,'DS_ERRO='||sqlerrm),1,2000);
		end;
	end if;
	
end if;

end ws_suspender_exame_prescricao;
/