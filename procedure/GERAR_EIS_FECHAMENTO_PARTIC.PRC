create or replace
procedure gerar_eis_fechamento_partic(	dt_inicial_p			date,
				dt_final_p				date,
				ie_data_p				varchar2,
				cd_setor_atendimento_p		varchar2,
				ie_status_atendimento_p		varchar2,
				cd_estagio_conta_p		varchar2,
				cd_medico_executor_p		varchar2,
				cd_convenio_p			varchar2,
				cd_categoria_convenio_p		varchar2,
				ie_tipo_atendimento_p		varchar2,
				ie_pac_leito_internacao_p		varchar2,
				nm_usuario_p			Varchar2) is 

nr_atendimento_w			number(10);
nr_interno_conta_w			number(10);
dt_alta_w				date;
dt_previsto_alta_w			date;
cd_convenio_w			number(05,0);
ds_convenio_w			varchar2(255);
cd_categoria_convenio_w		varchar2(10);
ie_tipo_atendimento_w		number(03,0);
ds_motivo_alta_w			varchar2(255);
nm_pessoa_fisica_w		varchar2(255);
nm_medico_w			varchar2(255);
cd_medico_resp_w			varchar2(10);
ds_setor_atual_w			varchar2(255);
ds_estagio_conta_w		varchar2(255);
nr_seq_estagio_conta_w		number(10,0);
vl_conta_w			number(15,2);
ds_unidades_w			varchar2(255);
ds_categoria_w			varchar2(255);
ie_insere_w			varchar2(01) := 'S';
nr_seq_ult_audit_conta_w		number(10,0);
dt_inicio_auditoria_w		date;
dt_fim_auditoria_w			date;
dt_liberacao_w			date;
dt_alta_medico_w			date;
ie_probabilidade_alta_w		varchar2(03);
ie_status_atend_w			varchar2(03);
ds_status_atend_w			varchar2(255);
dt_status_atend_w			date;
cd_setor_atual_w			number(05,0);
ds_tipo_atendimento_w		varchar2(255);
cd_estabelecimento_w		number(5);

cursor c01 is
select	b.nr_atendimento,
	a.nr_interno_conta,
	b.dt_alta,
	b.dt_previsto_alta,
	c.cd_convenio,
	c.ds_convenio,
	m.ds_motivo_alta,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_pessoa_fisica,
	b.cd_medico_resp,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_setor_atual,
	a.nr_seq_estagio_conta,
	a.vl_conta,
	substr(Obter_Unidade_Atendimento(a.nr_atendimento, 'A', 'U'),1,255) ds_unidades,
	substr(obter_dados_conta_paciente(a.nr_interno_conta, 'DC'),1,255) ds_categoria,
	b.ie_probabilidade_alta,
	b.dt_alta_medico,
	a.cd_categoria_parametro,
	b.ie_tipo_atendimento
from	motivo_alta m,
	conta_paciente a,
	atendimento_paciente b,
	convenio c
where	a.nr_atendimento 		= b.nr_atendimento
and	a.cd_convenio_parametro	= c.cd_convenio
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	c.ie_tipo_convenio		= 1
and	b.cd_motivo_alta 		= m.cd_motivo_alta(+)
and	a.ie_status_acerto		= 1
and	(b.dt_alta >= dt_inicial_p and b.dt_alta <= dt_final_p) 
and 	ie_data_p = 'A'
order by 1;


cursor c02 is
select	b.nr_atendimento,
	a.nr_interno_conta,
	b.dt_alta,
	b.dt_previsto_alta,
	c.cd_convenio,
	c.ds_convenio,
	m.ds_motivo_alta,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_pessoa_fisica,
	b.cd_medico_resp,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_setor_atual,
	a.nr_seq_estagio_conta,
	a.vl_conta,
	substr(Obter_Unidade_Atendimento(a.nr_atendimento, 'A', 'U'),1,255) ds_unidades,
	substr(obter_dados_conta_paciente(a.nr_interno_conta, 'DC'),1,255) ds_categoria,
	b.ie_probabilidade_alta,
	b.dt_alta_medico,
	a.cd_categoria_parametro,
	b.ie_tipo_atendimento
from	motivo_alta m,
	conta_paciente a,
	atendimento_paciente b,
	convenio c
where	a.nr_atendimento 		= b.nr_atendimento
and	a.cd_convenio_parametro	= c.cd_convenio
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	c.ie_tipo_convenio		= 1
and	a.ie_status_acerto		= 1
and 	ie_data_p			= 'S'
and	b.cd_motivo_alta 		= m.cd_motivo_alta(+)
and	b.dt_alta is not null 
and 	b.dt_alta >= dt_inicial_p 
and	b.dt_alta <= dt_final_p
union
select	b.nr_atendimento,
	a.nr_interno_conta,
	b.dt_alta,
	b.dt_previsto_alta,
	c.cd_convenio,
	c.ds_convenio,
	m.ds_motivo_alta,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_pessoa_fisica,
	b.cd_medico_resp,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_setor_atual,
	a.nr_seq_estagio_conta,
	a.vl_conta,
	substr(Obter_Unidade_Atendimento(a.nr_atendimento, 'A', 'U'),1,255) ds_unidades,
	substr(obter_dados_conta_paciente(a.nr_interno_conta, 'DC'),1,255) ds_categoria,
	b.ie_probabilidade_alta,
	b.dt_alta_medico,
	a.cd_categoria_parametro,
	b.ie_tipo_atendimento
from	motivo_alta m,
	conta_paciente a,
	atendimento_paciente b,
	convenio c
where	a.nr_atendimento 		= b.nr_atendimento
and	a.cd_convenio_parametro	= c.cd_convenio
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	c.ie_tipo_convenio		= 1
and	a.ie_status_acerto		= 1
and 	ie_data_p 		= 'S'
and	b.cd_motivo_alta 		= m.cd_motivo_alta(+)
and	b.dt_alta is null 
and 	b.dt_alta_medico >= dt_inicial_p 
and	b.dt_alta_medico <= dt_final_p
union
select	b.nr_atendimento,
	a.nr_interno_conta,
	b.dt_alta,
	b.dt_previsto_alta,
	c.cd_convenio,
	c.ds_convenio,
	m.ds_motivo_alta,
	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255) nm_pessoa_fisica,
	b.cd_medico_resp,
	Obter_Setor_Atendimento(b.nr_atendimento) cd_setor_atual,
	a.nr_seq_estagio_conta,
	a.vl_conta,
	substr(Obter_Unidade_Atendimento(a.nr_atendimento, 'A', 'U'),1,255) ds_unidades,
	substr(obter_dados_conta_paciente(a.nr_interno_conta, 'DC'),1,255) ds_categoria,
	b.ie_probabilidade_alta,
	b.dt_alta_medico,
	a.cd_categoria_parametro,
	b.ie_tipo_atendimento
from	motivo_alta m,
	conta_paciente a,
	atendimento_paciente b,
	convenio c
where	a.nr_atendimento 		= b.nr_atendimento
and	a.cd_convenio_parametro	= c.cd_convenio
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	c.ie_tipo_convenio	= 1
and	a.ie_status_acerto	= 1
and 	ie_data_p = 'S'
and	b.cd_motivo_alta 	= m.cd_motivo_alta(+)
and	b.dt_alta is null 
and 	b.dt_previsto_alta >= dt_inicial_p 
and	b.dt_previsto_alta <= dt_final_p
order by 1;


TYPE 		fetch_array IS TABLE OF c01%ROWTYPE;
s_array 		fetch_array;
i		Integer := 1;
type Vetor is table of fetch_array index by binary_integer;
Vetor_c01_w	Vetor;

begin
exec_sql_dinamico('Anderson','truncate table W_EIS_FECHAMENTO_PARTIC');

cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_data_p = 'A') then
	begin
	open c01;
	loop
	FETCH C01 BULK COLLECT INTO s_array LIMIT 1000;
		Vetor_c01_w(i) := s_array;
		i := i + 1;
	EXIT WHEN C01%NOTFOUND;
	END LOOP;
	CLOSE C01;
	end;
elsif	(ie_data_p = 'S') then
	begin
	open c02;
	loop
	FETCH C02 BULK COLLECT INTO s_array LIMIT 1000;
		Vetor_c01_w(i) := s_array;
		i := i + 1;
	EXIT WHEN C02%NOTFOUND;
	END LOOP;
	CLOSE C02;
	end;
end if;

for i in 1..Vetor_c01_w.COUNT loop
	s_array := Vetor_c01_w(i);
	for z in 1..s_array.COUNT loop
	begin
	
	nr_atendimento_w			:= s_array(z).nr_atendimento;
	nr_interno_conta_w			:= s_array(z).nr_interno_conta;
	dt_alta_w				:= s_array(z).dt_alta;
	dt_previsto_alta_w			:= s_array(z).dt_previsto_alta;
	cd_convenio_w			:= s_array(z).cd_convenio;
	ds_convenio_w			:= s_array(z).ds_convenio;
	ds_motivo_alta_w			:= s_array(z).ds_motivo_alta;
	nm_pessoa_fisica_w		:= s_array(z).nm_pessoa_fisica;
	cd_medico_resp_w			:= s_array(z).cd_medico_resp;
	cd_setor_atual_w			:= s_array(z).cd_setor_atual;
	nr_seq_estagio_conta_w		:= s_array(z).nr_seq_estagio_conta;
	vl_conta_w			:= s_array(z).vl_conta;
	ds_unidades_w			:= s_array(z).ds_unidades;
	ds_categoria_w			:= s_array(z).ds_categoria;
	cd_categoria_convenio_w		:= s_array(z).cd_categoria_parametro;
	ie_tipo_atendimento_w		:= s_array(z).ie_tipo_atendimento;
	ie_probabilidade_alta_w		:= s_array(z).ie_probabilidade_alta;
	ie_insere_w			:= 'S';

	if	(nvl(nr_seq_estagio_conta_w,0) > 0) then
		select  ds_estagio
		into    ds_estagio_conta_w
		from    estagio_conta
		where   nr_sequencia = nr_seq_estagio_conta_w;
	end if;

	if	(nvl(cd_medico_resp_w,'X') <> 'X') then
		select	nm_pessoa_fisica
		into	nm_medico_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_medico_resp_w;
	end if;

	
	if	(nvl(cd_setor_atual_w,0) > 0) then
		select	ds_setor_atendimento
		into	ds_setor_atual_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atual_w;
	end if;
	
	if	(nvl(ie_tipo_atendimento_w,0) > 0) then
		select	substr(obter_valor_dominio(12,to_char(ie_tipo_atendimento_w)),1,255)
		into	ds_tipo_atendimento_w
		from	dual;
	end if;

	
	if	(cd_setor_atendimento_p is not null) and
		(obter_se_contido(cd_setor_atual_w, substr(cd_setor_atendimento_p,2,length(cd_setor_atendimento_p))) <> substr(cd_setor_atendimento_p,1,1)) then
		ie_insere_w	:= 'N';
		goto final;
		
	end if;
	
	
	if	(cd_estagio_conta_p is not null) and
		(obter_se_contido(nr_seq_estagio_conta_w, substr(cd_estagio_conta_p,2,length(cd_estagio_conta_p))) <> substr(cd_estagio_conta_p,1,1)) then
		ie_insere_w	:= 'N';
		goto final;
	end if;
	
	
	if	(cd_medico_executor_p is not null) and
		(obter_se_contido(cd_medico_resp_w, substr(cd_medico_executor_p,2,length(cd_medico_executor_p))) <> substr(cd_medico_executor_p,1,1)) then
		ie_insere_w	:= 'N';
		goto final;
	end if;	
	
	if	(cd_convenio_p is not null) and
		(obter_se_contido(cd_convenio_w, substr(cd_convenio_p,2,length(cd_convenio_p))) <> substr(cd_convenio_p,1,1)) then
		ie_insere_w	:= 'N';
		goto final;
	end if;		

	if	(cd_categoria_convenio_p is not null) and
		(obter_se_contido_char(replace(cd_convenio_w||cd_categoria_convenio_w,' ',''),replace(substr(cd_categoria_convenio_p,2,length(cd_categoria_convenio_p)),' ','')) <> substr(cd_categoria_convenio_p,1,1)) then
		ie_insere_w	:= 'N';
		goto final;
	end if;
	
	if	(ie_tipo_atendimento_p is not null) and
		(obter_se_contido(ie_tipo_atendimento_w, substr(ie_tipo_atendimento_p,2,length(ie_tipo_atendimento_p))) <> substr(ie_tipo_atendimento_p,1,1)) then
		ie_insere_w	:= 'N';
		goto final;
	end if;
	
	ie_status_atend_w	:= ie_probabilidade_alta_w;
	ds_status_atend_w	:= substr(obter_valor_dominio(1267, ie_probabilidade_alta_w),1,255);
	
	if	(dt_alta_w is not null) then
		ie_status_atend_w	:= 'X';
		--ds_status_atend_w	:= 'Alta hospitalar';
		ds_status_atend_w := wheb_mensagem_pck.get_texto(304107);
	end if;
		
	if	(dt_alta_w is not null) then
		dt_status_atend_w	:= dt_alta_w;
	elsif	(dt_alta_medico_w is not null) then
		dt_status_atend_w	:= dt_alta_medico_w;
	elsif	(dt_previsto_alta_w is not null) then
		dt_status_atend_w	:= dt_previsto_alta_w;
	else
		dt_status_atend_w	:= '';
	end if;
	
	if	(ie_status_atendimento_p is not null) and
		(obter_se_contido_char(replace(ie_status_atend_w,' ',''),replace(substr(ie_status_atendimento_p,2,length(ie_status_atendimento_p)),' ','')) <> substr(ie_status_atendimento_p,1,1)) then
		ie_insere_w	:= 'N';
		goto final;
	end if;
	
	select  max(nr_sequencia)
	into    nr_seq_ult_audit_conta_w
	from    auditoria_conta_paciente
	where   nr_interno_conta = nr_interno_conta_w;

	if	(nvl(nr_seq_ult_audit_conta_w,0) > 0) then
		select	min(x.dt_periodo_inicial),
			max(x.dt_periodo_final),
			max(x.dt_liberacao)
		into	dt_inicio_auditoria_w,
			dt_fim_auditoria_w,
			dt_liberacao_w
		from	auditoria_conta_paciente x 
		where	nr_sequencia = nr_seq_ult_audit_conta_w;
	end if;

	if	(nvl(ie_pac_leito_internacao_p,'N') = 'S') then
		begin
		select	'S'
		into	ie_insere_w
		from	setor_atendimento a,
			unidade_atendimento b
		where	a.cd_setor_atendimento 	= b.cd_setor_atendimento
		and 	a.ie_ocup_hospitalar 	= 'S'
		and	a.cd_setor_atendimento 	= cd_setor_atual_w
		and	rownum < 2;
		exception
		when others then
			ie_insere_w := 'N';
		end;
		
		if	(ie_insere_w	= 'N') then
			goto final;
		end if;
	end if;

	<<final>>
	if	(ie_insere_w = 'S') and
		(nvl(nr_interno_conta_w,0) > 0) then
		begin
		insert into w_eis_fechamento_partic(
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nr_atendimento,
					nr_interno_conta,
					dt_alta,
					dt_previsto_alta,
					ds_convenio,
					ds_motivo_alta,
					nm_pessoa_fisica,
					nm_medico,
					nm_setor_atual,
					ds_status_atend,
					ds_estagio_conta,
					vl_conta,
					dt_inicio_auditoria,
					dt_fim_auditoria,
					dt_liberacao,
					dt_status_atend,
					ie_status_atend,
					ds_unidades,
					ds_categoria,
					ie_tipo_atendimento,
					ds_tipo_atendimento)
				values(	nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nr_atendimento_w,
					nr_interno_conta_w,
					dt_alta_w,
					dt_previsto_alta_w,
					ds_convenio_w,
					ds_motivo_alta_w,
					nm_pessoa_fisica_w,
					nm_medico_w,
					ds_setor_atual_w,
					ds_status_atend_w,
					ds_estagio_conta_w,
					vl_conta_w,
					dt_inicio_auditoria_w,
					dt_fim_auditoria_w,
					dt_liberacao_w,
					dt_status_atend_w,
					ie_status_atend_w,
					ds_unidades_w,
					ds_categoria_w,
					ie_tipo_atendimento_w,
					ds_tipo_atendimento_w);
		end;
	end if;
	end;
	end loop;
	commit;
end loop;

commit;

end gerar_eis_fechamento_partic;
/