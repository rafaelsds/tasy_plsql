create or replace
procedure aviso_atraso_gerar_oc_cotacao(	cd_estabelecimento_p	number,
					qt_dias_p			number,
					nm_usuario_p		Varchar2) is 
						
cd_comprador_w			varchar2(10);
nm_comprador_w			varchar2(100);
nr_cot_compra_w			number(10);
ds_lista_cotacao_w			varchar2(4000) := '';
nr_seq_regra_w			number(10);
qt_regra_usuario_w			number(10);
nm_usuario_destino_w		varchar2(255) := '';
nr_sequencia_w			number(10);
nr_seq_classif_w			number(10);
ds_titulo_w			varchar2(80);
ds_comunicado_w			varchar2(4000);
cd_perfil_w			varchar2(10);
ie_ci_lida_w			varchar2(1);
/* Se tiver setor na regra, envia CI para os setores */
ds_setor_adicional_w                   	 varchar2(2000) := '';
/* Campos da regra Usu�rio da Regra */
cd_setor_regra_usuario_w		number(5);
cd_perfil_ativo_w			number(5);

cursor c01 is
select	distinct
	cd_comprador,
	substr(obter_nome_pf_pj(cd_comprador, null),1,100) nm_comprador
from	cot_compra
where	cd_estabelecimento = cd_estabelecimento_p
and	nr_seq_motivo_cancel is null
and	dt_geracao_ordem_compra is null
and	trunc(dt_cot_compra,'dd')  <= trunc(sysdate,'dd') - nvl(qt_dias_p,7)
order by cd_comprador;

cursor c02 is
select	distinct
	nr_cot_compra	
from	cot_compra
where	cd_estabelecimento = cd_estabelecimento_p
and	nr_seq_motivo_cancel is null
and	dt_geracao_ordem_compra is null
and	trunc(dt_cot_compra,'dd')  <= trunc(sysdate,'dd') - nvl(qt_dias_p,7)
and	cd_comprador = cd_comprador_w
order by nr_cot_compra;

Cursor c05 is
select	nvl(a.cd_setor_atendimento,0) cd_setor_atendimento
from	regra_envio_comunic_usu a
where	a.nr_seq_evento = nr_seq_regra_w;

begin

select	obter_classif_comunic('F')
into	nr_seq_classif_w
from	dual;

begin
select	obter_perfil_ativo
into	cd_perfil_ativo_w
from	dual;
exception when others then
	cd_perfil_ativo_w := 0;
end;

select	nvl(max(b.nr_sequencia),0),
	max(cd_perfil)
into	nr_seq_regra_w,
	cd_perfil_w
from	regra_envio_comunic_compra a,
	regra_envio_comunic_evento b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_funcao = 915
and	b.cd_evento = 17
and	b.ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p
and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,0,'CC',cd_perfil_ativo_w,nm_usuario_p,null),1,1) = 'S';

if	(nr_seq_regra_w > 0) then

	open C05;
	loop
	fetch C05 into	
		cd_setor_regra_usuario_w;
	exit when C05%notfound;
		begin
		if	(cd_setor_regra_usuario_w <> 0) and
			(obter_se_contido_char(cd_setor_regra_usuario_w, ds_setor_adicional_w) = 'N') then
			ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_regra_usuario_w || ',',1,2000);
		end if;
		end;
	end loop;
	close C05;

	open C01;
	loop
	fetch C01 into	
		cd_comprador_w,
		nm_comprador_w;
	exit when C01%notfound;
		begin
		ds_lista_cotacao_w := '';
		open C02;
		loop
		fetch C02 into	
			nr_cot_compra_w;
		exit when C02%notfound;
			begin
			ds_lista_cotacao_w := substr(ds_lista_cotacao_w || nr_cot_compra_w || ', ',1,4000);
			end;
		end loop;
		close C02;
		
		select	substr(ds_titulo,1,80),
			ds_comunicacao
		into	ds_titulo_w,
			ds_comunicado_w
		from	regra_envio_comunic_evento
		where	nr_sequencia = nr_seq_regra_w;

		ds_comunicado_w := substr(replace_macro(ds_comunicado_w,'@comprador', nm_comprador_w),1,4000);
		ds_comunicado_w := substr(replace_macro(ds_comunicado_w,'@lista_cotacao', substr(ds_lista_cotacao_w,1,length(ds_lista_cotacao_w)-2)),1,4000);

		select	count(*)
		into	qt_regra_usuario_w
		from	regra_envio_comunic_compra a,
			regra_envio_comunic_evento b,
			regra_envio_comunic_usu c
		where	a.nr_sequencia = b.nr_seq_regra
		and	b.nr_sequencia = c.nr_seq_evento
		and	b.nr_sequencia = nr_seq_regra_w;

		select	nvl(ie_ci_lida,'N')
		into	ie_ci_lida_w
		from 	regra_envio_comunic_evento
		where 	nr_sequencia = nr_seq_regra_w;

		if	(qt_regra_usuario_w > 0) then
			nm_usuario_destino_w := obter_usuarios_comunic_compras(nr_cot_compra_w,null,17,nr_seq_regra_w,'');
		end if;
		
		if	(nm_usuario_destino_w is not null) then
				
			select	comunic_interna_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			if	(cd_perfil_w is not null) then
				cd_perfil_w := cd_perfil_w ||',';
			end if;

			insert into comunic_interna(
				dt_comunicado,			ds_titulo,				ds_comunicado,
				nm_usuario,			dt_atualizacao,			ie_geral,
				nm_usuario_destino,		nr_sequencia,			ie_gerencial,
				nr_seq_classif,			dt_liberacao,			ds_perfil_adicional,
				ds_setor_adicional)
			values(	sysdate,				ds_titulo_w,			ds_comunicado_w,
				nm_usuario_p,			sysdate,				'N',
				nm_usuario_destino_w,		nr_sequencia_w,			'N',
				nr_seq_classif_w,			sysdate,		cd_perfil_w,
				ds_setor_adicional_w);

			/*Para que a comunica��o seja gerada como lida ao pr�prio usu�rio */
			if	(ie_ci_lida_w = 'S') then
				insert into comunic_interna_lida(nr_sequencia,nm_usuario,dt_atualizacao)values(nr_sequencia_w,nm_usuario_p,sysdate);
			end if;

		end if;
		end;		
	end loop;
	close C01;
end if;

commit;

end aviso_atraso_gerar_oc_cotacao;
/