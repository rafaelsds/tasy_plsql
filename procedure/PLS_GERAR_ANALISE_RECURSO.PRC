create or replace
procedure pls_gerar_analise_recurso
			(	nr_seq_recurso_conta_p		pls_rec_glosa_conta.nr_sequencia%type,
				nm_usuario_p			usuario.nm_usuario%type,
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type) is 
				
cd_guia_referencia_w		pls_conta.cd_guia_referencia%type;
cd_guia_w			pls_conta.cd_guia%type;
nr_seq_segurado_w		pls_conta.nr_seq_segurado%type;
nr_seq_lote_conta_w		pls_rec_glosa_protocolo.nr_seq_lote_conta%type;
nr_seq_conta_w			pls_conta.nr_sequencia%type;
nm_segurado_w			varchar2(255);
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type;
nr_seq_prestador_w		pls_rec_glosa_protocolo.nr_seq_prestador%type;
ie_origem_conta_w		pls_conta.ie_origem_conta%type;
nm_prestador_w			varchar2(255);
ie_preco_w			pls_plano.ie_preco%type;
ie_existe_analise_w		pls_integer;
nr_seq_analise_w		pls_analise_conta.nr_sequencia%type;
nr_seq_analise_ref_w		pls_conta.nr_seq_analise%type;
qt_regra_w			pls_integer;

begin
select	c.cd_guia_referencia,
	c.cd_guia,
	c.nr_seq_segurado,
	rp.nr_seq_lote_conta,
	c.nr_sequencia,
	substr(pls_obter_dados_segurado(c.nr_seq_segurado,'N'),1,255) nm_segurado,
	c.ie_tipo_guia,
	rp.nr_seq_prestador,
	c.ie_origem_conta,
	substr(pls_obter_dados_prestador(rp.nr_seq_prestador,'N'),1,255) nm_prestador
into	cd_guia_referencia_w,
	cd_guia_w,
	nr_seq_segurado_w,
	nr_seq_lote_conta_w,
	nr_seq_conta_w,
	nm_segurado_w,
	ie_tipo_guia_w,
	nr_seq_prestador_w,
	ie_origem_conta_w,
	nm_prestador_w
from	pls_rec_glosa_conta	rc,
	pls_rec_glosa_protocolo	rp,
	pls_conta		c
where	rp.nr_sequencia	= rc.nr_seq_protocolo
and	c.nr_sequencia	= rc.nr_seq_conta
and	rc.nr_sequencia	= nr_seq_recurso_conta_p;

if	(cd_guia_w is null) or
	(somente_numero(cd_guia_referencia_w) > 0) then
	cd_guia_w	:= cd_guia_referencia_w;
end if;

if	(nr_seq_segurado_w is not null) then
	select	max(pl.ie_preco)
	into	ie_preco_w
	from	pls_segurado	sg,
		pls_plano	pl
	where	pl.nr_sequencia	= sg.nr_seq_plano
	and	sg.nr_sequencia	= nr_seq_segurado_w;
end if;

if	(nr_seq_lote_conta_w is not null) then
	--Verifica se existe analise para esta discussao 
	select	count(1)
	into	ie_existe_analise_w
	from	pls_analise_conta 	a
	where	a.cd_guia		= cd_guia_w
	and	a.nr_seq_lote_protocolo	= nr_seq_lote_conta_w
	and	a.nr_seq_segurado	= nr_seq_segurado_w
	and	(((nvl(ie_auditoria,'N') = 'S') and (ie_status = 'G'))	--Gerada na mesma an�lise se a analise estiver em aguardando auditoria ou seja sem o processo ter sido iniciado
	or	(nvl(ie_auditoria,'N') = 'N'));				--Ou se a conta n�o for de auditoria, dai pode-se iniciar em todas as etapas	
		
	if	(ie_existe_analise_w > 0) then		
		select	max(nr_sequencia)
		into	nr_seq_analise_w
		from	pls_analise_conta	a
		where	a.cd_guia		= cd_guia_w
		and	a.nr_seq_segurado	= nr_seq_segurado_w
		and	a.nr_seq_lote_protocolo	= nr_seq_lote_conta_w
		and	(((nvl(a.ie_auditoria,'N') = 'S') and (a.ie_status = 'G'))
		or	(nvl(a.ie_auditoria,'N') = 'N'));
		
		update	pls_analise_conta
		set	ie_status	= 'G'
		where	nr_sequencia	= nr_seq_analise_w;
	else
		if	(nr_seq_prestador_w is not null) then
			-- Levanta-se a analise principal, com base na conta principal, para se fazer o vinculo.
			begin
			select	a.nr_seq_analise
			into	nr_seq_analise_ref_w
			from	pls_conta	a,
				pls_conta	b
			where	a.nr_sequencia	= b.nr_seq_conta_princ
			and	b.nr_sequencia	= nr_seq_conta_w;
			exception
			when no_data_found then
				nr_seq_analise_ref_w := null;
			end;

			--Se n�o existe cria-se um novo registro de analise
			insert into pls_analise_conta
				(nr_sequencia, nr_seq_lote_protocolo, nr_seq_prestador,
				 nr_seq_segurado, nm_usuario,
				 nm_usuario_nrec, dt_atualizacao, dt_atualizacao_nrec,
				 cd_guia, ie_status, dt_analise, 
				 dt_inicio_analise, ie_auditoria, nr_seq_congenere,
				 ie_pre_analise, ie_origem_analise, ie_origem_conta,
				 ie_preco, cd_estabelecimento, nm_prestador,
				 nm_cooperativa, cd_cooperativa, nm_segurado,
				 ie_tipo_guia, nr_seq_analise_ref)
			values	(pls_analise_conta_seq.nextval, nr_seq_lote_conta_w, nr_seq_prestador_w,
				 nr_seq_segurado_w, nm_usuario_p,
				 nm_usuario_p, sysdate, sysdate,
				 cd_guia_w, 'G', sysdate, 
				 sysdate, 'S', null,
				 'N', 4, ie_origem_conta_w,
				 ie_preco_w, cd_estabelecimento_p, nm_prestador_w,
				 null, null, nm_segurado_w,
				 ie_tipo_guia_w, nr_seq_analise_ref_w) returning nr_sequencia into nr_seq_analise_w;
		end if;
	end if;
		
	if	(nr_seq_analise_w is not null) then		
		update	pls_rec_glosa_conta
		set	nr_seq_analise	= nr_seq_analise_w
		where	nr_sequencia	= nr_seq_recurso_conta_p;
	
		--Copiar as glosas e ocorrencias para a tabela ser usado na an�lise
		pls_gerar_analise_rec_item( nr_seq_recurso_conta_p, nr_seq_analise_w, cd_estabelecimento_p, nm_usuario_p);
	
		-- Gerar grupo da analise
		pls_gerar_grupo_aud_recurso( nr_seq_recurso_conta_p, nr_seq_analise_w, cd_estabelecimento_p, nm_usuario_p);
	end if;
		
	pls_gerar_lib_analise( nr_seq_analise_w, cd_estabelecimento_p, nm_usuario_p);
end if;

select	count(1)
into	qt_regra_w
from	pls_rec_regra_valor
where	cd_estabelecimento = cd_estabelecimento_p;

if	(qt_regra_w > 0) then
	pls_regra_valor_rec_glosa(nr_seq_analise_w, nm_usuario_p, cd_estabelecimento_p);
end if;

end pls_gerar_analise_recurso;
/
