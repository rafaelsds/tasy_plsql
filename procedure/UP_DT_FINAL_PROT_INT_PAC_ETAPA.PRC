create or replace procedure up_dt_final_prot_int_pac_etapa (nr_seq_protocolo_p number) is 

dt_inicial_real_w date;
dt_final_real_w date;
dt_real_w date;
qt_etapas_incompletas_W number;

begin
    select max(DT_REAL)
    into dt_inicial_real_w 
    from PROTOCOLO_INT_PAC_EVENTO 
    where nr_seq_prt_int_pac_etapa = nr_seq_protocolo_p;
 
 select min(dt_final_real)
    into dt_final_real_w 
    from PROTOCOLO_INT_PAC_ETAPA 
    where nr_seq_protocolo_int_pac = nr_seq_protocolo_p;
    
       if (dt_real_w is null) then
        update PROTOCOLO_INT_PAC_ETAPA set dt_final_real = dt_real_w where nr_sequencia = nr_seq_protocolo_p;
        commit;
    end if;
    
end up_dt_final_prot_int_pac_etapa;
/