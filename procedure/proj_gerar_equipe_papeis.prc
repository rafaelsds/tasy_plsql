create or replace procedure proj_gerar_equipe_papeis(
                                NR_PROJETO_P	        NUMBER,
                                NR_PROJETO_SUPERIOR_P 	NUMBER,
                                NM_USUARIO_P	        varchar2) is

nr_seq_equipe_w			proj_equipe.nr_sequencia%type;
nr_seq_proj_equipe_w      	proj_equipe.nr_sequencia%type;
dt_inicio_w               	proj_equipe.dt_inicio%type;
ds_objetivo_w             	proj_equipe.ds_objetivo%type;
nr_seq_equipe_funcao_w    	proj_equipe.nr_seq_equipe_funcao%type;
ie_interna_externa_w      	proj_equipe.ie_interna_externa%type;
ie_virada_w               	proj_equipe.ie_virada%type;
nr_seq_apres_equipe_w		proj_equipe.nr_seq_apres%type;
dt_liberacao_w            	proj_equipe.dt_liberacao%type;
nr_seq_virada_w           	proj_equipe.nr_seq_virada%type;
nr_seq_cliente_w          	proj_equipe.nr_seq_cliente%type;
nr_seq_proj_equipe_papel_w	proj_equipe_papel.nr_sequencia%type;
nr_seq_apres_papel_w           	proj_equipe_papel.nr_seq_apres%type;
nr_seq_funcao_w           	proj_equipe_papel.nr_seq_funcao%type;
nm_pessoa_w               	proj_equipe_papel.nm_pessoa%type;
cd_pessoa_fisica_w        	proj_equipe_papel.cd_pessoa_fisica%type;
ds_setor_w                	proj_equipe_papel.ds_setor%type;
ds_fone_w                 	proj_equipe_papel.ds_fone%type;
dt_fim_w                  	proj_equipe_papel.dt_fim%type;
nr_seq_cadastro_w         	proj_equipe_papel.nr_seq_cadastro%type;
ds_atuacao_w              	proj_equipe_papel.ds_atuacao%type;
ds_cargo_w                	proj_equipe_papel.ds_cargo%type;
nm_guerra_w               	proj_equipe_papel.nm_guerra%type;
ds_email_w                	proj_equipe_papel.ds_email%type;
ie_alerta_w               	proj_equipe_papel.ie_alerta%type;
ie_situacao_w             	proj_equipe_papel.ie_situacao%type;
ie_funcao_rec_migr_w      	proj_equipe_papel.ie_funcao_rec_migr%type;
qt_horas_rec_proj_w       	proj_equipe_papel.qt_horas_rec_proj%type;
dt_inicio_resp_w          	proj_equipe_papel.dt_inicio_resp%type;
dt_fim_resp_w             	proj_equipe_papel.dt_fim_resp%type;
ie_alocado_original_w     	proj_equipe_papel.ie_alocado_original%type;
pr_alocacao_w             	proj_equipe_papel.pr_alocacao%type;

cursor C01 is
select	nr_sequencia,
	dt_inicio,
        ds_objetivo,
        nr_seq_equipe_funcao,
        ie_interna_externa,
        ie_virada,
        nr_seq_apres,
        dt_liberacao,
        nr_seq_virada,
        nr_seq_cliente
from	proj_equipe
where	nr_seq_proj = nr_projeto_superior_p
order by nr_sequencia;

cursor C02 is
select	nr_seq_apres,
        nr_seq_funcao,
        nm_pessoa,
        cd_pessoa_fisica,
        ds_setor,
        ds_fone,
        dt_fim,
        nr_seq_cadastro,
        ds_atuacao,
        ds_cargo,
        nm_guerra,
        ds_email,
        ie_alerta,
        ie_situacao,
        ie_funcao_rec_migr,
        qt_horas_rec_proj,
        dt_inicio_resp,
        dt_fim_resp,
        ie_alocado_original,
        pr_alocacao
from	proj_equipe_papel
where	nr_seq_equipe = nr_seq_equipe_w
order by nr_sequencia;

begin

if (nr_projeto_superior_p > 0 and nr_projeto_p > 0 and nm_usuario_p is not null) then

open C01;
loop
fetch C01 into
	nr_seq_equipe_w,
	dt_inicio_w,
        ds_objetivo_w,
        nr_seq_equipe_funcao_w,
        ie_interna_externa_w,
        ie_virada_w,
        nr_seq_apres_equipe_w,
        dt_liberacao_w,
        nr_seq_virada_w,
        nr_seq_cliente_w;
exit when C01%notfound;

	select 	proj_equipe_seq.nextval
	into	nr_seq_proj_equipe_w
	from	dual;

	insert into proj_equipe ( 	nr_sequencia,
                                        dt_atualizacao,
                                        nm_usuario,
                                        dt_atualizacao_nrec,
                                        nm_usuario_nrec,
                                        dt_inicio,
                                        ds_objetivo,
                                        nr_seq_equipe_funcao,
                                        ie_interna_externa,
                                        ie_virada,
                                        nr_seq_apres,
                                        nr_seq_proj,
                                        dt_liberacao,
                                        nr_seq_virada,
                                        nr_seq_cliente )
        values (	                nr_seq_proj_equipe_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
                                        dt_inicio_w,
                                        ds_objetivo_w,
                                        nr_seq_equipe_funcao_w,
                                        ie_interna_externa_w,
                                        ie_virada_w,
                                        nr_seq_apres_equipe_w,
                                        nr_projeto_p,
                                        dt_liberacao_w,
                                        nr_seq_virada_w,
                                        nr_seq_cliente_w );

	open C02;
	loop
	fetch C02 into
		nr_seq_apres_papel_w,
                nr_seq_funcao_w,
                nm_pessoa_w,
                cd_pessoa_fisica_w,
                ds_setor_w,
                ds_fone_w,
                dt_fim_w,
                nr_seq_cadastro_w,
                ds_atuacao_w,
                ds_cargo_w,
                nm_guerra_w,
                ds_email_w,
                ie_alerta_w,
                ie_situacao_w,
                ie_funcao_rec_migr_w,
                qt_horas_rec_proj_w,
                dt_inicio_resp_w,
                dt_fim_resp_w,
                ie_alocado_original_w,
                pr_alocacao_w;
	exit when C02%notfound;

	select 	proj_equipe_papel_seq.nextval
	into	nr_seq_proj_equipe_papel_w
	from	dual;

	insert into proj_equipe_papel (	nr_sequencia,
                                        nr_seq_equipe,
                                        dt_atualizacao,
                                        nm_usuario,
                                        dt_atualizacao_nrec,
                                        nm_usuario_nrec,
                                        nr_seq_apres,
                                        nr_seq_funcao,
                                        nm_pessoa,
                                        cd_pessoa_fisica,
                                        ds_setor,
                                        ds_fone,
                                        dt_fim,
                                        nr_seq_cadastro,
                                        ds_atuacao,
                                        ds_cargo,
                                        nm_guerra,
                                        ds_email,
                                        ie_alerta,
                                        ie_situacao,
                                        ie_funcao_rec_migr,
                                        qt_horas_rec_proj,
                                        dt_inicio_resp,
                                        dt_fim_resp,
                                        ie_alocado_original,
                                        pr_alocacao )
	values 			    (	nr_seq_proj_equipe_papel_w,
                                        nr_seq_proj_equipe_w,
                                        sysdate,
                                        nm_usuario_p,
                                        sysdate,
                                        nm_usuario_p,
                                        nr_seq_apres_papel_w,
                                        nr_seq_funcao_w,
                                        nm_pessoa_w,
                                        cd_pessoa_fisica_w,
                                        ds_setor_w,
                                        ds_fone_w,
                                        dt_fim_w,
                                        nr_seq_cadastro_w,
                                        ds_atuacao_w,
                                        ds_cargo_w,
                                        nm_guerra_w,
                                        ds_email_w,
                                        ie_alerta_w,
                                        ie_situacao_w,
                                        ie_funcao_rec_migr_w,
                                        qt_horas_rec_proj_w,
                                        dt_inicio_resp_w,
                                        dt_fim_resp_w,
                                        ie_alocado_original_w,
                                        pr_alocacao_w );

	end loop;
	close C02;
end loop;
close C01;
end if;

commit;

end proj_gerar_equipe_papeis;
/
