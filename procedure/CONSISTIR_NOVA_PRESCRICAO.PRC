create or replace
procedure consistir_nova_prescricao(
		nr_atendimento_p			number,
		nr_horas_sem_diag_p			number,
		nr_horas_laudo_p			number,
		ie_perm_prim_prescr_p		varchar2,
		ie_consiste_sofa_p			varchar2,
		ie_consiste_apache_p		varchar2,
		ie_data_alta_prescr_p		varchar2,
		ie_prescr_nao_lib_p			varchar2,
		ie_quest_prescr_p 		out	varchar2,
		ie_laudo_sus_p 			out	varchar2,
		dt_prescricao_p 		out	Date, 	
		nm_usuario_p				varchar2,
		ie_questiona_assumir_pac_p	varchar2,
		cd_medico_resp_p		out varchar2,
		nm_pessoa_fisica_p		out varchar2) is

ie_existe_prescr_w			varchar2(1) := 'N';
ie_consiste_laudo_w			varchar2(1);
ie_apres_msg_laudo_sus_w	varchar2(1)	:= 'N';
ie_questiona_prescr_w		varchar2(1)	:= 'N';
dt_entrada_w				Date;
dt_alta_w					Date;
dt_prescricao_w				Date;
cd_medico_resp_w			varchar2(60)  := '0';
nm_pessoa_fisica_w  		varchar2(200) := '';
begin
if	(nr_atendimento_p is not null) and
	(nr_atendimento_p > 0) and
	(nm_usuario_p is not null) then
	begin
	if	(ie_perm_prim_prescr_p = 'S') then
		begin
		select	decode(count(*),0,'N','S')
		into	ie_existe_prescr_w
		from	prescr_medica
		where	nr_atendimento = nr_atendimento_p
		and	obter_classif_setor(cd_setor_atendimento) <> 1 
		and 	dt_liberacao is not null
		and	obter_tipo_atendimento(nr_atendimento) = 1;
		
		if	(ie_existe_prescr_w = 'S') then
			begin
			-- O paciente j� possui uma prescri��o. Par�metro [563]
			Wheb_mensagem_pck.exibir_mensagem_abort(56382);
			end;
		end if;
		end;
	end if;
	
	if	(nr_horas_sem_diag_p > 0) and
		(nr_atendimento_p > 0) then
		begin
		select	max(dt_entrada)
		into	dt_entrada_w
		from	atendimento_paciente
		where	nr_atendimento 	= nr_atendimento_p;
		
		if	((sysdate - nr_horas_sem_diag_p/24) > dt_entrada_w) then
			begin
			ie_consiste_laudo_w	:= obter_se_consiste_laudo_aih(nr_atendimento_p, nr_horas_sem_diag_p);
			if	(ie_consiste_laudo_w = 'S') then
				begin
				-- N�o � permitido fazer nova prescri��o para paciente sem laudo AIH. Par�metro [732].
				Wheb_mensagem_pck.exibir_mensagem_abort(56383);
				end;
			end if;
			end;
		end if;
		end;
	end if;
	
	if	(ie_consiste_sofa_p <> 'S') then
		begin
		select	decode(count(*),0,'N','S')
		into	ie_existe_prescr_w
		from	escala_sofa
		where	nr_atendimento = nr_atendimento_p
		and	trunc(dt_avaliacao,'dd') = trunc(sysdate,'dd');
		
		if	(ie_existe_prescr_w = 'N') then
			begin
			-- N�o foi cadastrada a escala SOFA para este atendimento no dia, favor informar! Par�metro [792].
			Wheb_mensagem_pck.exibir_mensagem_abort( 56385 );
			end;			
		end if;
		end;
	end if;
	
	if	(ie_consiste_apache_p <> 'S') then
		begin
		select	decode(count(*),0,'N','S')
		into	ie_existe_prescr_w
		from	apache
		where	nr_atendimento = nr_atendimento_p;
		
		if	(ie_existe_prescr_w = 'N') then
			begin
			-- N�o foi cadastrada a escala Apache II para este atendimento, favor informar! Par�metro [793].
			Wheb_mensagem_pck.exibir_mensagem_abort( 56386 );
			end;
		end if;
		end;
	end if;
	
	if	(ie_questiona_assumir_pac_p = 'S') then
		begin
		select	a.cd_medico_resp, 
				substr(obter_nome_pf(b.cd_pessoa_fisica),1,200)
		into	cd_medico_resp_w,
				nm_pessoa_fisica_w
		from	pessoa_fisica b, 
				atendimento_paciente a
		where	a.nr_atendimento = nr_atendimento_p
		and		a.cd_medico_resp = b.cd_pessoa_fisica;
		end;
	end if;
	
	if	(nr_horas_laudo_p > 0) then
		begin
		ie_consiste_laudo_w	:= obter_se_consiste_laudo_aih(nr_atendimento_p, nr_horas_laudo_p);
		if	(ie_consiste_laudo_w = 'S') then
			begin
			ie_apres_msg_laudo_sus_w := 'S';
			end;
		end if;
		end;
	end if;
	
	if	(ie_data_alta_prescr_p in ('S','U','5')) then
		begin
		if	(nr_atendimento_p > 0) then			
			select	dt_alta
			into	dt_alta_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
		end if;	
		
		if	(dt_alta_w is not null) then
			begin
			if	(ie_data_alta_prescr_p = 'S') then
				begin
				dt_prescricao_w	:= dt_alta_w;
				end;
			elsif	(ie_data_alta_prescr_p = 'U') then
				begin
				dt_prescricao_w	:= dt_alta_w - 1/24;
				end;
			else
				begin
				dt_prescricao_w	:= dt_alta_w - 5/1440;
				end;
			end if;
			end;
		end if;
		end;
	end if;
	
	if	(ie_prescr_nao_lib_p = 'S') then
		begin
		select	decode(count(*),0,'N','S')
		into	ie_existe_prescr_w
		from	prescr_medica
		where	dt_liberacao is null
		and	nr_atendimento 	= nr_atendimento_p
		and	nm_usuario	= nm_usuario_p
		and	trunc(dt_prescricao) = trunc(sysdate);
		
		ie_questiona_prescr_w := ie_existe_prescr_w;
		end;
	end if;
	end;
end if;
commit;
ie_laudo_sus_p		:= ie_apres_msg_laudo_sus_w;
dt_prescricao_p		:= dt_prescricao_w;
ie_quest_prescr_p	:= ie_questiona_prescr_w;
cd_medico_resp_p	:= cd_medico_resp_w;
nm_pessoa_fisica_p	:= nm_pessoa_fisica_w;
end consistir_nova_prescricao;
/