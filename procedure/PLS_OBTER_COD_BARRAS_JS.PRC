/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter dados do c�digo de barras e retornar para o Java Swing
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_obter_cod_barras_js
			(	cd_usuario_plano_p	in 	varchar2,
				nm_beneficiario_p	out	Varchar2,
				nr_via_p		out	Varchar2,
				nr_seq_segurado_p	out	varchar2,
				dt_validade_p		out	varchar2,
				nr_digito_p		out	varchar2,
				cd_pessoa_fisica_p	out 	varchar2,
				dt_contratacao_p	out	varchar2,
				ds_motivo_cancelam_p	out	varchar2,
				dt_rescisao_p		out	varchar2,
				ds_abrengencia_p	out	varchar2,
				DS_COOPERATIVA_P	out	varchar2,
				cd_estabelecimento_p	in	number,
				nm_usuario_p		in	varchar2) is
			
dt_contratacao_w		date;
ds_motivo_cancelamento_w	varchar2(255);
dt_rescisao_w			date;

begin

pls_obter_dados_codigo_barras(	cd_usuario_plano_p,nm_beneficiario_p,nr_via_p,nr_seq_segurado_p,
				dt_validade_p,nr_digito_p,cd_pessoa_fisica_p,cd_estabelecimento_p,nm_usuario_p);
				
if	(nr_seq_segurado_p is not null) then
	select	dt_contratacao,
		DT_RESCISAO,
		substr(pls_obter_desc_motivo_cancel(nr_seq_motivo_cancelamento),1,255)
	into	dt_contratacao_w,
		dt_rescisao_w,
		ds_motivo_cancelamento_w
	from	pls_segurado
	where	nr_sequencia	= to_number(nr_seq_segurado_p);
	
	ds_abrengencia_p	:= substr(pls_obter_abrangencia_resumida(to_number(nr_seq_segurado_p)),1,255);
	PLS_CONSISTIR_BENEF_REPASSE(to_number(nr_seq_segurado_p),DS_COOPERATIVA_P);
end if;

dt_contratacao_p	:= to_char(dt_contratacao_w,'dd/mm/yyyy');
ds_motivo_cancelam_p	:= ds_motivo_cancelamento_w;
dt_rescisao_p		:= to_char(dt_rescisao_w,'dd/mm/yyyy');

end pls_obter_cod_barras_js;
/