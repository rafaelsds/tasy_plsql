create or replace
procedure inserir_traducao_ajuste_versao(
			cd_funcao_p			ajuste_versao.cd_funcao%type,
			cd_pessoa_fisica_p	ajuste_versao.cd_pessoa_fisica%type,			
			ds_motivo_p			ajuste_versao.ds_motivo%type,			
			nm_usuario_p		ajuste_versao.nm_usuario%type,
			cd_versao_p			ajuste_versao.cd_versao%type,
			cd_expressao_p		dic_expressao.cd_expressao%type,
			nr_seq_traducao_p	dic_expressao_idioma.nr_sequencia%type,
			cd_cliente_p		ajuste_versao_cnpj.cd_cliente%type,
			ds_retorno_p		out varchar2) is
			
	nr_seq_ajuste_versao_w	ajuste_versao.nr_sequencia%type;
	nr_pacote_w				ajuste_versao.nr_pacote%type;
	ds_idioma_w				varchar2(50);
	nr_ordem_serv_w			man_ordem_servico.nr_sequencia%type;
	
	procedure inserir_ajuste_expressao(
				ie_acao_p ajuste_versao_registro.ie_acao%type) is
		nr_seq_registro_w	ajuste_versao_registro.nr_sequencia%type;
	begin
		select	ajuste_versao_registro_seq.nextval
		into	nr_seq_registro_w
		from	dual;
	
		insert into	ajuste_versao_registro(
			nr_sequencia,
			nr_seq_ajuste_versao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_tabela,
			ie_acao,
			ds_valor,
			ie_usuario)
		values(
			nr_seq_registro_w,
			nr_seq_ajuste_versao_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'DIC_EXPRESSAO',
			ie_acao_p,
			' and CD_EXPRESSAO = '||chr(39)||cd_expressao_p||chr(39),
			'T');
	end;
	
	procedure inserir_ajuste_idioma(
				ie_acao_p ajuste_versao_registro.ie_acao%type) is
		nr_seq_registro_w	ajuste_versao_registro.nr_sequencia%type;
	begin		
		select	ajuste_versao_registro_seq.nextval
		into	nr_seq_registro_w
		from	dual;
	
		insert into	ajuste_versao_registro(
			nr_sequencia,
			nr_seq_ajuste_versao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_tabela,
			ie_acao,
			ds_valor,
			ie_usuario)
		values(
			nr_seq_registro_w,
			nr_seq_ajuste_versao_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'DIC_EXPRESSAO_IDIOMA',
			ie_acao_p,
			' and NR_SEQUENCIA = '||chr(39)||nr_seq_traducao_p||chr(39),
			'T');
	end;
	
	procedure inserir_ajuste_cnpj is
		nr_seq_ajuste_versao_cnpj_w	ajuste_versao_cnpj.nr_sequencia%type;
	begin
		select	ajuste_versao_cnpj_seq.nextval
		into	nr_seq_ajuste_versao_cnpj_w
		from	dual;
		
		insert into ajuste_versao_cnpj(
			nr_sequencia,
			cd_cliente,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_ajuste_versao
		)
		values(
			nr_seq_ajuste_versao_cnpj_w,
			cd_cliente_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			nr_seq_ajuste_versao_w
		);
		
		commit;
	end;
	
	procedure inserir_hist_ordem_serv is
		ds_text_w	varchar2(500);
	begin
	
		select	wheb_mensagem_pck.get_texto(1060409, 
					'CD_EXPRESSAO=' || cd_expressao_p || 
					';NR_PACOTE=' || nr_pacote_w || 
					';NM_USUARIO=' || nm_usuario_p || 
					';CD_VERSAO=' || cd_versao_p || 
					';DS_IDIOMA=' || ds_idioma_w)
		into	ds_text_w
		from	dual;
	
		insert into corp.MAN_ORDEM_SERV_TECNICO@whebl01_dbcorp(
			nr_sequencia,
            nr_seq_ordem_serv,
            dt_atualizacao,
            nm_usuario,
            dt_historico,
            dt_liberacao,
            nr_seq_tipo,
            ie_origem,
            ds_relat_tecnico)
		values(
			corp.MAN_ORDEM_SERV_TECNICO_SEQ.nextval@whebl01_dbcorp,
			nr_ordem_serv_w,
			sysdate, 
			NM_USUARIO_P, 
			sysdate,
			sysdate,
			7,--'Dialogo interno Philips'
			'I',
			ds_text_w);
			
		commit;
	
	end;
	
begin
	
	nr_ordem_serv_w := 1858448;
	
	select	ajuste_versao_seq.nextval
	into	nr_seq_ajuste_versao_w
	from	dual;

	select	substr(obter_valor_dominio(8923,ds_idioma),1,255)
	into	ds_idioma_w
	from	dic_expressao_idioma
	where	nr_sequencia = nr_seq_traducao_p;
	
	insert into ajuste_versao(
		nr_sequencia,
		cd_funcao,
		cd_pessoa_fisica,
		cd_versao,
		ds_motivo,
		ie_situacao,
		nr_seq_ordem_serv,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(
		nr_seq_ajuste_versao_w,
		cd_funcao_p,
		cd_pessoa_fisica_p,
		cd_versao_p,
		ds_motivo_p,
		'A',
		nr_ordem_serv_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p
	);
		
	inserir_ajuste_expressao('I');
	inserir_ajuste_expressao('A');
	
	inserir_ajuste_idioma('I');
	inserir_ajuste_idioma('A');
	
	commit;
	
	inserir_ajuste_cnpj();
	
	liberar_ajuste_versao(nm_usuario_p, nr_seq_ajuste_versao_w, ds_retorno_p);
	
	select	nr_pacote
	into	nr_pacote_w
	from	ajuste_versao
	where	nr_sequencia = nr_seq_ajuste_versao_w;
	
	inserir_hist_ordem_serv();
	
	if (ds_retorno_p is null) then
		select	wheb_mensagem_pck.get_texto(1060406, 'NR_PACOTE=' || nr_pacote_w)
		into	ds_retorno_p
		from	dual;		
	end if;
	
end inserir_traducao_ajuste_versao;
/