create or replace
procedure consis_alter_status_emprestimo(
		nr_sequencia_p		number,
		nr_seq_status_p		number,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2,
		ie_tipo_reserva_p	varchar2,
		ds_erro_p		out varchar2) is
ds_erro_w varchar2(2000);
begin
if	(nr_sequencia_p is not null) and
	(nr_seq_status_p is not null) and
	(cd_estabelecimento_p is not null) and
	(nm_usuario_p is not null) and
	(ie_tipo_reserva_p is not null) then
	begin
	obter_se_recurso_disp_aprov(
			nr_sequencia_p,
			nr_seq_status_p,
			ie_tipo_reserva_p,
			'N',
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_erro_w);
	if	(ds_erro_w is not null) then
		begin
		ds_erro_p := ds_erro_w;
		--raise_application_error(-20011, ds_erro_w||'#@#@');
		end;
	else
		begin
		altera_status_emprestimo(
				nr_sequencia_p,
				nr_seq_status_p,
				nm_usuario_p,
				cd_estabelecimento_p);
		end;
	end if;
	end;
end if;
end consis_alter_status_emprestimo;
/