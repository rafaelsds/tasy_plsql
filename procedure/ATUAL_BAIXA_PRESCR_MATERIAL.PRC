create or replace
procedure atual_baixa_prescr_material(
			nr_sequencia_p		number,
			nr_prescricao_p		number,
			cd_motivo_baixa_p		number,
			cd_local_estoque_p	number,
			cd_material_baixa_p	number,
			nm_usuario_p		varchar2) is 

begin

if	(nr_sequencia_p is not null) and
	(nr_prescricao_p is not null) and 
	(cd_motivo_baixa_p is not null) and
	(cd_local_estoque_p is not null) and 
	(cd_material_baixa_p is not null) then
	begin
	update 	prescr_material
	set     cd_motivo_baixa	= cd_motivo_baixa_p,
                cd_local_estoque		= cd_local_estoque_p,
                cd_material_baixa		= cd_material_baixa_p,
	        dt_atualizacao		= sysdate,
		dt_baixa		= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p
	and 	nr_prescricao	= nr_prescricao_p;
		
	end;            
end if;                 
                        
                        	
commit;                 

end atual_baixa_prescr_material;
/