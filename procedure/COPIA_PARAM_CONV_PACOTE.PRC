create or replace 
Procedure Copia_Param_Conv_Pacote( 		cd_convenio_orig_p	number,
						cd_convenio_dest_p	number,
						nm_tabela_p		varchar2,
						nm_usuario_p		varchar2,
						ie_excluir_p		Varchar2,
						cd_estabelecimento_p	Number) is

nr_seq_w			number(10);
nr_sequencia_w			number(10);
nr_seq_acomod_w			number(10);
nr_seq_tipo_acomodacao_w	number(10);
ie_tipo_acomod_w		VARCHAR2(2);
IE_REGRA_HORA_INICIO_w		VARCHAR2(5);
IE_REGRA_HORA_FIM_w		VARCHAR2(5);
qt_dias_pacote_w		NUMBER(5);
qt_dias_hospital_w		NUMBER(5);
qt_dias_uti_w			NUMBER(5);
IE_EXCEDENTE_w			NUMBER(2);
CD_PROCEDIMENTO_w		NUMBER(15);
IE_ORIGEM_PROCED_w		NUMBER(10);
VL_PACOTE_w			NUMBER(15,2);
VL_HONORARIO_w			NUMBER(15,2);
QT_PONTO_PACOTE_w		NUMBER(15,4);
QT_PONTO_HONORARIO_w		NUMBER(15,4);
CD_ESTRUTURA_CONTA_w		NUMBER(5);
IE_CLASSIFICACAO_w		VARCHAR2(1);
cd_categoria_w			Varchar2(10);
dt_vigencia_w			Date;
ie_exige_gabarito_w		Varchar2(01);
ie_ratear_repasse_w		varchar2(2);
DS_ARQUIVO_W			varchar2(255);
ds_documento_w			long;
ie_tipo_doc_w			varchar2(1);
dt_vigencia_final_w		date;
ie_tipo_atendimento_w		number(10,0);
ie_tipo_atend_conta_w		number(3,0);
ie_gerar_proced_negativo_w	varchar2(1);
vL_ANESTESISTA_w		number(15,4);
IE_ATEND_RETORNO_w		varchar2(1);
ie_atend_acomp_w		varchar(1);
nr_seq_proc_interno_w		number(10);
cd_setor_atendimento_w		number(5,0);
cd_medico_executor_w		varchar2(10);
ie_clinica_w			number(5,0);
vl_materiais_w			number(15,2);
ie_consiste_cirurgia_w		varchar2(1);
ie_exige_item_conta_w		varchar2(1);
vl_auxiliares_w			number(15,2);
ds_observacao_w			varchar2(255);
ie_sexo_w			varchar2(1);
pr_acrescimo_rn_w		number(15,2);
ie_lado_w			varchar2(1);
ie_setor_lanc_exclusivo_w	varchar2(1);
qt_procedimento_w		number(9,3);
cd_centro_custo_w		number(8);
ie_credenciado_w		varchar2(1);
qt_hora_w			pacote_tipo_acomodacao.qt_hora%type;
cd_doenca_w           	        VARCHAR2(10);
nr_seq_pacote_w			NUMBER(10);       
ie_situacao_w			VARCHAR2(1);
qt_idade_min_w			pacote_tipo_acomodacao.qt_idade_min%type;  
qt_idade_max_w			pacote_tipo_acomodacao.qt_idade_max%type;           
qt_dias_inter_inicio_w		pacote_tipo_acomodacao.qt_dias_inter_inicio%type; 
qt_dias_inter_final_w		pacote_tipo_acomodacao.qt_dias_inter_final%type;    
ie_tipo_anestesia_w		pacote_tipo_acomodacao.ie_tipo_anestesia%type;  
hr_final_pacote_w		pacote_tipo_acomodacao.hr_final_pacote%type;       
pr_faturar_pacote_w		pacote_tipo_acomodacao.pr_faturar_pacote%type;  
ie_atualiza_medico_w		pacote_tipo_acomodacao.ie_atualiza_medico%type;  
ie_consiste_dias_inter_w	pacote_tipo_acomodacao.ie_consiste_dias_inter%type; 
ie_data_consiste_idade_w	pacote_tipo_acomodacao.ie_data_consiste_idade%type;
ie_pacote_mat_ipasgo_w		pacote_tipo_acomodacao.ie_pacote_mat_ipasgo%type;
ie_carater_inter_sus_w          pacote_tipo_acomodacao.ie_carater_inter_sus%type;

cursor c001 is
	select nr_seq_pacote
	from pacote
	where cd_convenio = cd_convenio_orig_p
	and	cd_estabelecimento = cd_estabelecimento_p;

cursor c002 is
	select
			ie_tipo_acomod,
			qt_dias_pacote, 
			qt_dias_hospital,
			qt_dias_uti, 
			ie_excedente,
			cd_procedimento, 
			ie_origem_proced,
			vl_pacote, 
			vl_honorario, 
			qt_ponto_pacote, 
			qt_ponto_honorario,
			cd_estrutura_conta,
			ie_classificacao,
			cd_categoria,
			dt_vigencia,
			dt_vigencia_final,
			ie_exige_gabarito,
			ie_regra_hora_inicio,
			ie_regra_hora_fim,
			ie_ratear_repasse,
			nr_sequencia,
			ie_tipo_atendimento,
			ie_gerar_proced_negativo,
			vl_anestesista,
			ie_atend_retorno,
			ie_atend_acomp,
			nr_seq_proc_interno,
			cd_setor_atendimento,
			cd_medico_executor,
			ie_clinica,
			vl_materiais,
			ie_consiste_cirurgia,
			ie_exige_item_conta,
			vl_auxiliares,
			ds_observacao,
			ie_sexo,
			pr_acrescimo_rn,
			ie_lado,
			ie_setor_lanc_exclusivo,
			qt_procedimento,
			ie_tipo_atend_conta,
			cd_centro_custo,
			ie_credenciado,
			qt_hora,
			qt_idade_min,  
			qt_idade_max,            
			qt_dias_inter_inicio,   
			qt_dias_inter_final,      
			ie_tipo_anestesia,   
			hr_final_pacote,          
			pr_faturar_pacote,  
			ie_atualiza_medico,    
			ie_consiste_dias_inter,  
			ie_data_consiste_idade,
			ie_pacote_mat_ipasgo,
                        ie_carater_inter_sus
	from		pacote_tipo_acomodacao
	where		nr_seq_pacote	= nr_seq_w;

Cursor	c003 is
select	IE_TIPO_DOC,
	DS_DOCUMENTO
from 	pacote_doc
where 	nr_seq_pacote = nr_seq_w;

cursor	c004 is
	select	NR_SEQUENCIA,  
			CD_DOENCA,
			NR_SEQ_PACOTE,     
			IE_SITUACAO 
	from	pacote_cid
	where	NR_SEQ_PACOTE	= nr_seq_w
	and		NR_SEQUENCIA	= nr_sequencia_w;

begin

if (nm_tabela_p = 'PACOTE') then
	if	(ie_excluir_p = 'S') then
		delete	from pacote_tipo_acomod_proc
		where	nr_seq_pac_acomod in ( 	select	nr_sequencia 
						from 	pacote_tipo_acomodacao
						where 	nr_seq_pacote in ( select	nr_seq_pacote
									   from 	pacote
									   where	cd_convenio  =  Cd_convenio_dest_p
									   and		cd_estabelecimento =  cd_estabelecimento_p));
									   
		delete from pacote_formadores
		where  nr_seq_pacote in ( 	select	nr_seq_pacote
						from 	pacote
						where	cd_convenio  =  Cd_convenio_dest_p
						and	cd_estabelecimento =  cd_estabelecimento_p);
						
		delete	from pacote_tipo_acomod_mat
		where	nr_seq_pac_acomod in ( 	select	nr_sequencia 
						from 	pacote_tipo_acomodacao
						where 	nr_seq_pacote in ( select	nr_seq_pacote
									   from 	pacote
									   where	cd_convenio  =  Cd_convenio_dest_p
									   and		cd_estabelecimento =  cd_estabelecimento_p));
		
		delete from PACOTE a
		where	cd_convenio 		= cd_convenio_dest_p
		and	cd_estabelecimento	= cd_estabelecimento_p
		and	not exists (select 1
				    from	atendimento_pacote b
				    where	a.nr_seq_pacote	= b.nr_seq_pacote);
			 
	end if;
	open c001;
	loop
		fetch c001 into nr_seq_w;
		exit when c001%notfound;

		select pacote_seq.nextval
		into nr_sequencia_w
		from dual;	

		insert into pacote (
			nr_seq_pacote,
			cd_convenio, 
			cd_proced_pacote,  
			ie_origem_proced, 
			ie_situacao,  
			dt_atualizacao,
			nm_usuario, 
			ds_observacao, 
			cd_estabelecimento, 
			ie_agendavel, 
			ie_pacote_automatico, 
			ie_prioridade,
			cd_moeda,
			ie_exclusivo)
		select 	nr_sequencia_w, 
			cd_convenio_dest_p, 
			cd_proced_pacote,  
			ie_origem_proced, 
			ie_situacao,  
			sysdate,
			nm_usuario_p, 
			ds_observacao, 
			cd_estabelecimento_p, 
			ie_agendavel, 
			ie_pacote_automatico,
			ie_prioridade,
			cd_moeda,
			ie_exclusivo
		from 	pacote
		where 	nr_seq_pacote = nr_seq_w;
		
		insert into pacote_material (
			nr_seq_pacote, 
			nr_sequencia, 
			ie_inclui_exclui,  
			dt_atualizacao, 
			nm_usuario,  
			ie_excedente,
			cd_grupo_material, 
			cd_subgrupo_material, 
			cd_classe_material,  
			cd_material, 
			qt_limite, 
			ie_tipo_atendimento,  
			cd_setor_atendimento, 
			ie_valor,  
			vl_minimo,  
			vl_maximo,  
			pr_desconto,
			nr_seq_pac_acomod, 
			ie_tipo_setor,
			nr_seq_familia, 
			cd_centro_custo, 
			ie_ratear_item, 
			vl_negociado,
			nr_seq_estrutura,
			qt_idade_min,
			qt_idade_max,
			ie_objetivo,
			ie_considera_generico,
			pr_orcamento,
			ie_sexo,
			ie_somente_sem_rla,
			ie_setor_exclusivo,
			ie_consiste_limite_item,
			cd_local_estoque)
		select 	nr_sequencia_w, 
			nr_sequencia, 
			ie_inclui_exclui, 
			sysdate,  
			nm_usuario_p,  
			ie_excedente,
			cd_grupo_material, 
			cd_subgrupo_material, 
			cd_classe_material,  
			cd_material, 
			qt_limite, 
			ie_tipo_atendimento,  
			cd_setor_atendimento, 
			ie_valor,  
			vl_minimo,  
			vl_maximo,  
			pr_desconto,	
			nr_seq_pac_acomod, 
			ie_tipo_setor,
			nr_seq_familia, 
			cd_centro_custo, 
			ie_ratear_item, 
			vl_negociado,
			nr_seq_estrutura,
			qt_idade_min,
			qt_idade_max,
			ie_objetivo,
			ie_considera_generico,
			pr_orcamento,
			ie_sexo,
			ie_somente_sem_rla,
			ie_setor_exclusivo,
			ie_consiste_limite_item,
			cd_local_estoque
		from 	pacote_material
		where 	nr_seq_pacote = nr_seq_w;
				
			
		insert into pacote_procedimento (
			nr_seq_pacote, 
			nr_sequencia, 
			ie_inclui_exclui, 
			dt_atualizacao, 
			nm_usuario,  
			ie_excedente,
			cd_area_proced, 
			cd_especial_proced, 
			cd_grupo_proced,  
			cd_procedimento, 
			ie_origem_proced, 
			qt_limite, 
			ie_considera_honorario,  
			cd_setor_atendimento, 
			ie_tipo_atendimento, 
			ie_calcula_honorario, 
			ie_calcula_custo_oper,  
			pr_desconto, 
			nr_seq_pac_acomod, 
			qt_idade_min, 
			qt_idade_max, 
			vl_maximo, 
			vl_minimo, 
			ie_tipo_valor,
			nr_seq_proc_interno,
			ie_ratear_item,
			pr_desc,
			vl_negociado,
			ie_agendavel,
			ie_sexo,
			cd_medico,
			cd_cgc_prestador,
			cd_centro_custo,
			nr_seq_classif,
			ie_setor_exclusivo,
			cd_moeda,
			ie_valida_limite_proc,
			nr_seq_estrutura)
		select 	nr_sequencia_w, 
			nr_sequencia, 
			ie_inclui_exclui,  
			sysdate, 
			nm_usuario_p,  
			ie_excedente,
			cd_area_proced, 
			cd_especial_proced, 
			cd_grupo_proced,  
			cd_procedimento, 
			ie_origem_proced, 
			qt_limite, 
			ie_considera_honorario,  
			cd_setor_atendimento, 
			ie_tipo_atendimento, 
			ie_calcula_honorario,
			ie_calcula_custo_oper, 
			pr_desconto, 
			nr_seq_pac_acomod,    
			qt_idade_min, 
			qt_idade_max, 
			vl_maximo, 
			vl_minimo, 
			ie_tipo_valor, 
			nr_seq_proc_interno, 
			ie_ratear_item,
			pr_desc, 
			vl_negociado, 
			ie_agendavel, 
			ie_sexo, 
			cd_medico, 
			cd_cgc_prestador, 
			cd_centro_custo, 
			nr_seq_classif,
			ie_setor_exclusivo,
			cd_moeda,
			ie_valida_limite_proc,
			nr_seq_estrutura
		from 	pacote_procedimento
		where 	nr_seq_pacote = nr_seq_w;
		
		insert into pacote_formadores
			(nr_sequencia,
			nr_seq_pacote,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_procedimento,
			ie_origem_proced,
			nr_seq_proc_interno,
			qt_minimo)
		select 	pacote_formadores_seq.nextval,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_procedimento,
			ie_origem_proced,
			nr_seq_proc_interno,
			qt_minimo
		from	pacote_formadores
		where 	nr_seq_pacote = nr_seq_w;
			
		begin
		open c003;
		loop
			fetch c003 into 
				ie_tipo_doc_w,
				ds_documento_w;				
			exit when c003%notfound;
			begin
			insert	into pacote_doc(
				nr_sequencia, 
				nr_seq_pacote, 
				ie_tipo_doc, 
				dt_atualizacao, 
				nm_usuario,
				ds_documento, 
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values	(pacote_doc_seq.nextval, 
				nr_sequencia_w, 
				ie_tipo_doc_w, 
				sysdate,
				nm_usuario_p, 
				ds_documento_w, 
				sysdate, 
				nm_usuario_p);
			end;
		end loop;
		close c003;
		end;			


		begin
		open c002;
		loop
			fetch c002 into 
					ie_tipo_acomod_w,
					qt_dias_pacote_w,
					qt_dias_hospital_w,
					qt_dias_uti_w,
					ie_excedente_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					vl_pacote_w,
					vl_honorario_w,
					qt_ponto_pacote_w,
					qt_ponto_honorario_w,
					cd_estrutura_conta_w,
					ie_classificacao_w,
					cd_categoria_w,
					dt_vigencia_w,
					dt_vigencia_final_w,
					ie_exige_gabarito_w,
					ie_regra_hora_inicio_w,
					ie_regra_hora_fim_w,
					ie_ratear_repasse_w,
					nr_seq_acomod_w,
					ie_tipo_atendimento_w,
					ie_gerar_proced_negativo_w,
					vl_anestesista_w,
					ie_atend_retorno_w,
					ie_atend_acomp_w,
					nr_seq_proc_interno_w,
					cd_setor_atendimento_w,
					cd_medico_executor_w,
					ie_clinica_w,
					vl_materiais_w,
					ie_consiste_cirurgia_w,
					ie_exige_item_conta_w,
					vl_auxiliares_w,
					ds_observacao_w,
					ie_sexo_w,
					pr_acrescimo_rn_w,
					ie_lado_w,
					ie_setor_lanc_exclusivo_w,
					qt_procedimento_w,
					ie_tipo_atend_conta_w,
					cd_centro_custo_w,
					ie_credenciado_w,
					qt_hora_w,
					qt_idade_min_w,  
					qt_idade_max_w,            
					qt_dias_inter_inicio_w,   
					qt_dias_inter_final_w,      
					ie_tipo_anestesia_w,   
					hr_final_pacote_w,          
					pr_faturar_pacote_w,  
					ie_atualiza_medico_w,    
					ie_consiste_dias_inter_w,  
					ie_data_consiste_idade_w,
					ie_pacote_mat_ipasgo_w,
                                        ie_carater_inter_sus_w;
			exit when c002%notfound;
					begin
					
					select pacote_tipo_acomodacao_seq.NextVal
					into nr_seq_tipo_acomodacao_w
					from dual;
					
					insert into pacote_tipo_acomodacao (
						nr_seq_pacote, ie_tipo_acomod, dt_atualizacao, nm_usuario, qt_dias_pacote, 
						qt_dias_hospital, qt_dias_uti, ie_excedente, cd_procedimento,  ie_origem_proced,
						vl_pacote, vl_honorario,  qt_ponto_pacote,  qt_ponto_honorario, cd_estrutura_conta,
						ie_classificacao, nr_sequencia,  cd_categoria,  dt_vigencia,  dt_vigencia_final, 
						ie_exige_gabarito, ie_regra_hora_inicio, ie_regra_hora_fim, ie_situacao, 
						ie_ratear_repasse, ie_tipo_atendimento, ie_gerar_proced_negativo, vl_anestesista,
						ie_atend_retorno, ie_atend_acomp,nr_seq_proc_interno, cd_setor_atendimento, cd_medico_executor,
						ie_clinica, vl_materiais, ie_consiste_cirurgia, ie_exige_item_conta, vl_auxiliares, ds_observacao,
						ie_sexo, pr_acrescimo_rn, ie_lado, ie_setor_lanc_exclusivo, qt_procedimento, ie_tipo_atend_conta,
						cd_centro_custo, ie_credenciado,qt_hora, qt_idade_min, qt_idade_max, qt_dias_inter_inicio, 
						qt_dias_inter_final, ie_tipo_anestesia, hr_final_pacote, pr_faturar_pacote, ie_atualiza_medico,
						ie_consiste_dias_inter, ie_data_consiste_idade, ie_pacote_mat_ipasgo, ie_carater_inter_sus)
					values(
						nr_sequencia_w, 		
						ie_tipo_acomod_w,		
						sysdate,		
						nm_usuario_p,		
						qt_dias_pacote_w,
						qt_dias_hospital_w, 	
						qt_dias_uti_w, 		
						ie_excedente_w, 	
						cd_procedimento_w, 	
						ie_origem_proced_w, 
						vl_pacote_w,  		
						vl_honorario_w, 		
						qt_ponto_pacote_w, 
						qt_ponto_honorario_w, 	
						cd_estrutura_conta_w, 
						ie_classificacao_w, 	
						nr_seq_tipo_acomodacao_w,  
						null, 		 
						dt_vigencia_w,            
						dt_vigencia_final_w, 
						ie_exige_gabarito_w,	
						ie_regra_hora_inicio_w, 	
						ie_regra_hora_fim_w,  
						'A',  
						ie_ratear_repasse_w, 
						ie_tipo_atendimento_w,
						ie_gerar_proced_negativo_w, 
						vl_anestesista_w, 
						ie_atend_retorno_w, 
						ie_atend_acomp_w,
						nr_seq_proc_interno_w, 
						cd_setor_atendimento_w, 
						cd_medico_executor_w, 
						ie_clinica_w, 
						vl_materiais_w,
						ie_consiste_cirurgia_w, 
						ie_exige_item_conta_w, 
						vl_auxiliares_w,	
						ds_observacao_w, 
						ie_sexo_w,
						pr_acrescimo_rn_w, 
						ie_lado_w, 
						ie_setor_lanc_exclusivo_w, 
						qt_procedimento_w, 
						ie_tipo_atend_conta_w,
						cd_centro_custo_w, 
						ie_credenciado_w, 
						qt_hora_w,
						qt_idade_min_w,  
						qt_idade_max_w,            
						qt_dias_inter_inicio_w,   
						qt_dias_inter_final_w,      
						ie_tipo_anestesia_w,   
						hr_final_pacote_w,          
						pr_faturar_pacote_w,  
						ie_atualiza_medico_w,    
						ie_consiste_dias_inter_w,  
						ie_data_consiste_idade_w,
						ie_pacote_mat_ipasgo_w,
                                                ie_carater_inter_sus_w);
				
					update	pacote_material
					set	nr_seq_pac_acomod = nr_seq_tipo_acomodacao_w
					where	nr_seq_pac_acomod is not null
					and 	nr_seq_pacote = nr_sequencia_w
					and 	nr_seq_pac_acomod = nr_seq_acomod_w;					
		
					update	pacote_procedimento
					set	nr_seq_pac_acomod = nr_seq_tipo_acomodacao_w
					where	nr_seq_pac_acomod is not null
					and 	nr_seq_pacote = nr_sequencia_w
					and 	nr_seq_pac_acomod = nr_seq_acomod_w;
			
					insert into pacote_tipo_acomod_proc(
						nr_sequencia, dt_atualizacao, nm_usuario, nr_seq_pac_acomod, cd_procedimento,
						ie_origem_proced, dt_atualizacao_nrec, nm_usuario_nrec)
					select 	pacote_tipo_acomod_proc_seq.NextVal, sysdate, nm_usuario_p,
						nr_seq_tipo_acomodacao_w, cd_procedimento, ie_origem_proced, sysdate,
						nm_usuario_p 
					from 	pacote_tipo_acomod_proc
					where 	nr_seq_pac_acomod = nr_seq_acomod_w;	
					
					insert into pacote_tipo_acomod_mat(
						nr_sequencia, dt_atualizacao, nm_usuario, nr_seq_pac_acomod, 
						cd_material, dt_atualizacao_nrec, nm_usuario_nrec)
					select 	pacote_tipo_acomod_mat_seq.nextval, sysdate, nm_usuario_p, nr_seq_tipo_acomodacao_w, 
						cd_material, sysdate, nm_usuario_p 
					from 	pacote_tipo_acomod_mat
					where 	nr_seq_pac_acomod = nr_seq_acomod_w;
					
					end;
		end loop;
		close c002;
		
		open	c004;
		loop
		fetch	c004 into
			nr_sequencia_w,
			cd_doenca_w             ,
			nr_seq_pacote_w			,       
			ie_situacao_w           ;
		exit	when c004%notfound;
			begin        

			select	pacote_cid_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			insert	into pacote_cid
				(nr_sequencia,           
				cd_doenca,
				nr_seq_pacote,  
				dt_atualizacao,        
				nm_usuario, 
				dt_atualizacao_nrec,                 
				nm_usuario_nrec,        
				ie_situacao)                                               
			values	(nr_sequencia_w,  
				cd_doenca_w,
				nr_seq_pacote_w,  
				sysdate,        
				nm_usuario_p, 
				sysdate,                 
				nm_usuario_p,  
				ie_situacao_w);
			end;
		end loop;
		close c004;

		end;				
	end loop;
	close c001;
end if;

commit;

end Copia_Param_Conv_Pacote;
/