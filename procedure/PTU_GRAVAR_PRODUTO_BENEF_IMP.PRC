create or replace
procedure ptu_gravar_produto_benef_imp
			(	nr_seq_beneficiario_p	number,
				ie_tipo_contrato_p	ptu_intercambio.ie_tipo_contrato%type,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Gravar no beneficiario do A100 importado o produto que ele ira utilizar
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_empresa_w		number(10);
cd_plano_intercambio_w		varchar2(10);
ie_natureza_w			number(10);
ie_abrangencia_w		number(10);
qt_registros_w			number(10);
ie_repasse_w			varchar2(10);
ie_tipo_contratacao_w		varchar2(10);
ie_abrangencia_plano_w		varchar2(10);
ie_preco_w			varchar2(10);
nr_seq_plano_w			number(10);
nr_seq_segurado_w		number(10);
nr_seq_intercambio_w		number(10);
nr_seq_contrato_w		number(10);
cd_plano_origem_w		ptu_intercambio_plano.cd_plano_origem%type;
nr_seq_intercambio_plano_w	pls_intercambio_plano.nr_sequencia%type;

Cursor C01 is
	select	ie_natureza,
		ie_abrangencia
	from	ptu_intercambio_plano
	where	nr_seq_empresa		= nr_seq_empresa_w
	and	cd_plano_intercambio	= cd_plano_intercambio_w
	and	cd_plano_origem 	= cd_plano_origem_w;

Cursor C02 is
	select	ie_tipo_contratacao,
		ie_abrangencia_plano,
		ie_preco
	from	pls_regra_plano_a100
	where	((ie_natureza		= ie_natureza_w) OR (ie_natureza = 1))
	and	((ie_abrangencia	= ie_abrangencia_w) OR (ie_abrangencia = 6))
	and	((ie_repasse		= ie_repasse_w) or (ie_repasse is null))
	and	ie_situacao		= 'A'
	order by ie_natureza,
		decode(ie_abrangencia,6,-1,ie_abrangencia),
		decode(ie_repasse,null,-1,1);

Cursor C03 is
	select	nr_sequencia
	from	pls_plano
	where	cd_plano_intercambio 	= cd_plano_intercambio_w
	and	((ie_tipo_contratacao	= ie_tipo_contratacao_w and ie_tipo_contratacao is not null) or ie_tipo_contratacao is null)
	and	((ie_abrangencia	= ie_abrangencia_plano_w and ie_abrangencia is not null) or ie_abrangencia is null)
	and	ie_preco		= ie_preco_w
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_tipo_operacao	= 'T'
	order by decode(ie_tipo_contratacao,null,-1,1),
		decode(ie_abrangencia,null,-1,1);

Cursor C04 is
	select	b.nr_sequencia
	from	pls_intercambio_plano	a,
		pls_plano		b
	where	a.nr_seq_plano		= b.nr_sequencia
	and	b.cd_plano_intercambio 	= cd_plano_intercambio_w
	and	a.nr_seq_intercambio	= nr_seq_intercambio_w
	and	((b.ie_tipo_contratacao	= ie_tipo_contratacao_w and ie_tipo_contratacao is not null) or ie_tipo_contratacao is null)
	and	((b.ie_abrangencia	= ie_abrangencia_plano_w and ie_abrangencia is not null) or ie_abrangencia is null)
	and	b.ie_preco		= ie_preco_w
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	b.ie_tipo_operacao	= 'T'
	and	nvl(a.ie_situacao,'A')	= 'A'
	order by decode(b.ie_tipo_contratacao,null,-1,1),
		decode(b.ie_abrangencia,null,-1,1);
		
Cursor C05 is
	select	b.nr_sequencia nr_seq_plano
	from	pls_intercambio_plano	a,
		pls_plano		b
	where	a.nr_seq_plano		= b.nr_sequencia
	and	a.nr_seq_intercambio	= nr_seq_intercambio_w
	and	((b.ie_tipo_contratacao	= ie_tipo_contratacao_w and ie_tipo_contratacao is not null) or ie_tipo_contratacao is null)
	and	((b.ie_abrangencia	= ie_abrangencia_plano_w and ie_abrangencia is not null) or ie_abrangencia is null)
	and	b.ie_preco		= ie_preco_w
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	b.ie_tipo_operacao	= 'T'
	and	nvl(a.ie_situacao,'A')	= 'A'
	order by decode(b.ie_tipo_contratacao,null,-1,1),
		decode(b.ie_abrangencia,null,-1,1);

begin

select	b.nr_seq_empresa,
	b.cd_plano_intercambio,
	b.ie_repasse,
	b.nr_seq_benef_encontrado,
	a.nr_seq_contrato,
	b.cd_plano_origem
into	nr_seq_empresa_w,
	cd_plano_intercambio_w,
	ie_repasse_w,
	nr_seq_segurado_w,
	nr_seq_contrato_w,
	cd_plano_origem_w
from	ptu_intercambio_benef	b,
	ptu_intercambio_empresa	a
where	b.nr_seq_empresa	= a.nr_sequencia
and	b.nr_sequencia		= nr_seq_beneficiario_p;

select	count(1)
into	qt_registros_w
from	ptu_intercambio_plano
where	nr_seq_empresa		= nr_seq_empresa_w
and	cd_plano_intercambio	= cd_plano_intercambio_w;

if	(qt_registros_w > 0) then
	if	(nr_seq_segurado_w is not null) then
		select	max(nr_seq_intercambio)
		into	nr_seq_intercambio_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
	else
		nr_seq_intercambio_w	:= nr_seq_contrato_w;
	end if;
	
	open C01;
	loop
	fetch C01 into
		ie_natureza_w,
		ie_abrangencia_w;
	exit when C01%notfound;
	end loop;
	close C01;
	
	select	count(1)
	into	qt_registros_w
	from	pls_regra_plano_a100
	where	ie_situacao	= 'A'
	and	rownum		>= 1;
	
	if	(qt_registros_w > 0) then
		open C02;
		loop
		fetch C02 into
			ie_tipo_contratacao_w,
			ie_abrangencia_plano_w,
			ie_preco_w;
		exit when C02%notfound;
		end loop;
		close C02;
	else
		select	decode(ie_natureza_w,2,'I',3,'CE',4,'CA',5,'',null),
			decode(ie_abrangencia_w,1,'N',2,'GE',3,'E',4,'GM',5,'M',null)
		into	ie_tipo_contratacao_w,
			ie_abrangencia_plano_w
		from	dual;
		
		if	(ie_repasse_w = 'P') then
			ie_preco_w	:= '1';
		else
			ie_preco_w	:= '3';
		end if;
	end if;
	
	nr_seq_plano_w	:= null;
	
	if	(nr_seq_intercambio_w is not null) then
		open c04;
		loop
		fetch c04 into
			nr_seq_plano_w;
		exit when c04%notfound;
		end loop;
		close c04;
		
		if	((ie_tipo_contrato_p = 'F') and (nr_seq_plano_w is null)) then -- Se o contrato for de fundacao deve buscar o produto somente no contrato
			for r_c05_w in c05 loop
				begin
				nr_seq_plano_w	:= r_c05_w.nr_seq_plano;
				end;
			end loop;
			
			if	(nr_seq_plano_w is null) then
				select	max(nr_sequencia)
				into	nr_seq_intercambio_plano_w
				from	pls_intercambio_plano
				where	nr_seq_intercambio = nr_seq_intercambio_w;
				
				if	(nr_seq_intercambio_plano_w is not null) then
					select 	max(nr_seq_plano)
					into	nr_seq_plano_w
					from	pls_intercambio_plano
					where	nr_sequencia = nr_seq_intercambio_plano_w;
				end if;
			end if;
		end if;
	end if;

	if	((nr_seq_plano_w is null) and (ie_tipo_contrato_p <> 'F')) then
		open C03;
		loop
		fetch C03 into
			nr_seq_plano_w;
		exit when C03%notfound;
		end loop;
		close C03;
	end if;
	
	update	ptu_intercambio_benef
	set	nr_seq_plano	= nr_seq_plano_w
	where	nr_sequencia	= nr_seq_beneficiario_p;
end if;

end ptu_gravar_produto_benef_imp;
/