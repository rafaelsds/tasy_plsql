create or replace
procedure pls_gerar_sac_atend_log(
	cd_pessoa_fisica_p		varchar2,
	cd_cgc_p			varchar2,
	ds_ocorrencia_p		varchar2,
	nr_seq_atendimento_p	number,
	nr_seq_evento_atend_p	number,
	nm_usuario_p		varchar2,
	cd_estabelecimento_p	number,
	ie_origem_historico_p	varchar2) is 

begin

pls_gerar_sac_atend(
	cd_pessoa_fisica_p,
	cd_cgc_p,
	ds_ocorrencia_p,
	nr_seq_atendimento_p,
	nr_seq_evento_atend_p,
	nm_usuario_p,
	cd_estabelecimento_p);

pls_gerar_hist_log_atend(
	nr_seq_atendimento_p,
	ie_origem_historico_p,
	2000,
	nm_usuario_p,
	cd_estabelecimento_p);

end pls_gerar_sac_atend_log;
/
