create or replace
procedure pls_gravar_inc_unimed(ie_tipo_inconsistencia_p	number,
				ds_inconsistencia_p		varchar2,
				nr_seq_coop_p			number,
				nm_usuario_p			varchar2) is 

begin

insert into	pls_cad_unimed_inconsist
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ds_inconsistencia,
		ie_tipo_inconsistencia,
		nr_seq_cad_unimed)
values		(pls_cad_unimed_inconsist_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		ds_inconsistencia_p,
		ie_tipo_inconsistencia_p,
		nr_seq_coop_p);
commit;

end pls_gravar_inc_unimed;
/