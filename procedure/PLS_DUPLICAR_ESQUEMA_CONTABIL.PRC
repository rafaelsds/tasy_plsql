create or replace
procedure pls_duplicar_esquema_contabil
			(	nr_seq_esquema_p 	number,
				nm_usuario_p		varchar2) is

nr_seq_esquema_w		number(10);
nr_seq_esquema_item_w		number(10);
nm_coluna_w			varchar2(255);
ds_colunas_w			varchar2(5000);
ds_insert_w			varchar2(5000);

cursor c_coluna_esquema is
	select 	column_name
	from 	user_tab_columns
	where 	table_name = 'PLS_ESQUEMA_CONTABIL'
	and	column_name not in ('NR_SEQUENCIA','NM_USUARIO','NM_USUARIO_NREC','DT_ATUALIZACAO','DT_ATUALIZACAO_NREC','DS_REGRA');
	
cursor c_coluna_codif is
	select 	column_name
	from 	user_tab_columns
	where 	table_name = 'PLS_ESQUEMA_CONTABIL_SEG'
	and	column_name not in ('NR_SEQUENCIA','NR_SEQ_REGRA_ESQUEMA','NM_USUARIO','NM_USUARIO_NREC','DT_ATUALIZACAO','DT_ATUALIZACAO_NREC');
	
Cursor c_colunas is
	select	nr_sequencia
	from	pls_esquema_contabil_seg
	where	nr_seq_regra_esquema	= nr_seq_esquema_p;
	
begin

ds_colunas_w	:= '';

open c_coluna_esquema;
loop
fetch c_coluna_esquema into	
	nm_coluna_w;
exit when c_coluna_esquema%notfound;
	begin
		
	ds_colunas_w	:= ds_colunas_w || ', ' || nm_coluna_w;
	
	end;
end loop;
close c_coluna_esquema;

select 	pls_esquema_contabil_seq.nextval
into 	nr_seq_esquema_w
from 	dual;

ds_insert_w	:=  'insert into pls_esquema_contabil(nr_sequencia, 
							nm_usuario, 
							nm_usuario_nrec, 
							dt_atualizacao, 
							dt_atualizacao_nrec,
							ds_regra' 
							|| ds_colunas_w || ') 
						(select ' || nr_seq_esquema_w || ',' || 
							'''' || nm_usuario_p || ''','
							|| '''' || nm_usuario_p || '''' ||
							', sysdate, 
							sysdate,' ||
							'''C�pia do esquema - ''' || '|| DS_REGRA '
							|| ds_colunas_w || 	
						' from 	pls_esquema_contabil where nr_sequencia =' || nr_seq_esquema_p || ')';

exec_sql_dinamico('Tasy', ds_insert_w);
						
commit;

ds_colunas_w	:= '';

open c_coluna_codif;
loop
fetch c_coluna_codif into	
	nm_coluna_w;
exit when c_coluna_codif%notfound;
	begin
	
	ds_colunas_w	:= ds_colunas_w || ', ' || nm_coluna_w;
	
	end;
end loop;
close c_coluna_codif;

open c_colunas;
loop
fetch c_colunas into	
	nr_seq_esquema_item_w;
exit when c_colunas%notfound;
	begin
	
	ds_insert_w	:= 'insert into pls_esquema_contabil_seg(nr_sequencia, 
								nr_seq_regra_esquema, 
								nm_usuario, 
								nm_usuario_nrec, 
								dt_atualizacao, 
								dt_atualizacao_nrec' 
								|| ds_colunas_w || ') 
							(select pls_esquema_contabil_seg_seq.nextval, ' 
								|| nr_seq_esquema_w || ',' || 
								'''' || nm_usuario_p || ''',' ||
								'''' || nm_usuario_p || '''' ||
								', sysdate, 
								sysdate' 
								|| ds_colunas_w || 
							' from 	pls_esquema_contabil_seg where nr_sequencia =' || nr_seq_esquema_item_w || ')';
							
	exec_sql_dinamico('Tasy', ds_insert_w);
	
	end;
end loop;
close c_colunas;

commit;

end pls_duplicar_esquema_contabil;
/
