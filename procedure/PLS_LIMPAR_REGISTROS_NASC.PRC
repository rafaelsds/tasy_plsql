create or replace
procedure pls_limpar_registros_nasc
		(	nr_seq_conta_p		Number,
			cd_estabelecimento_p	Number,
			nm_usuario_p		Varchar2) is			
begin

update	pls_conta
set	ie_gestacao = 'N',
	ie_aborto = 'N',
	ie_parto_normal = 'N',
	ie_complicacao_puerperio = 'N',
	ie_complicacao_neonatal = 'N',
	ie_parto_cesaria = 'N',
	ie_baixo_peso = 'N',
	ie_atend_rn_sala_parto = 'N',
	ie_transtorno = 'N',
	ie_obito_mulher = null,
	qt_nasc_vivos = null,
	qt_obito_precoce = null,
	qt_obito_tardio = null,
	qt_nasc_mortos = null,
	qt_nasc_vivos_prematuros = null,	
	qt_nasc_vivos_termo = null,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_conta_p
and	cd_estabelecimento_p = cd_estabelecimento_p;

end pls_limpar_registros_nasc;
/