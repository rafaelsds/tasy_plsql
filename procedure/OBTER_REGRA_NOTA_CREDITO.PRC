create or replace
procedure obter_regra_nota_credito(
		ie_origem_p			in 	varchar2,
		dt_nota_credito_p			in	date,
		nr_seq_trans_baixa_tit_pagar_p	in out	number,
		nr_seq_trans_fin_contab_p		in out	number,
		dt_vencimento_p			in out	date) is 

nr_seq_regra_w			number(10);

nr_seq_trans_baixa_tit_pagar_w	number(10);
nr_seq_trans_fin_contab_w		number(10);
qt_dias_vencimento_w		number(5);
dt_vencimento_w			date;

cursor c01 is
select	nr_sequencia,
	nr_seq_trans_baixa_tit_pagar,
	nr_seq_trans_fin_contab,
	qt_dias_vencimento
from	regra_nota_credito
where	nvl(ie_origem, ie_origem_p) = ie_origem_p
order by ie_origem desc;

begin
open c01;
loop
fetch c01 into	
	nr_seq_regra_w,
	nr_seq_trans_baixa_tit_pagar_w,
	nr_seq_trans_fin_contab_w,
	qt_dias_vencimento_w;
exit when c01%notfound;
end loop;
close c01;

if	(qt_dias_vencimento_w > 0) then
	dt_vencimento_w := nvl(dt_vencimento_p, sysdate) + qt_dias_vencimento_w;
end if;

nr_seq_trans_baixa_tit_pagar_p	:= nvl(nr_seq_trans_baixa_tit_pagar_w, nr_seq_trans_baixa_tit_pagar_p);
nr_seq_trans_fin_contab_p		:= nvl(nr_seq_trans_fin_contab_w, nr_seq_trans_fin_contab_p);
dt_vencimento_p			:= nvl(dt_vencimento_w, dt_vencimento_p);
end obter_regra_nota_credito;
/