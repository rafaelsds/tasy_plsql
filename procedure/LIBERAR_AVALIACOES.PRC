create or replace
procedure Liberar_avaliacoes	(cd_funcao_p		number,
				ie_com_atendimento_p	varchar2,
				ie_com_prescricao_p	varchar2,
				ie_checkup_p		varchar2,
				qt_dias_p		number) is
				
nr_sequencia_w	number(10);
nm_usuario_w	varchar2(15);				
				
				
cursor	c01 is
	select	nr_sequencia,
		nm_usuario
	from	med_avaliacao_paciente
	where 	dt_liberacao is null
	and	((cd_funcao_p = '0') or (nr_seq_tipo_avaliacao in (	select	t.nr_sequencia
									from	med_tipo_avaliacao t
									where	t.cd_funcao	= cd_funcao_p)))
	and	((ie_com_atendimento_p = 'A')   or ((ie_com_atendimento_p = 'S')  and (nr_atendimento is not null))
						or ((ie_com_atendimento_p = 'N')  and (nr_atendimento is null)))
	and	((ie_com_prescricao_p = 'A')    or ((ie_com_prescricao_p  = 'S')  and (nr_prescricao is not null))
						or ((ie_com_prescricao_p  = 'N')  and (nr_prescricao is null)))
	and	((ie_checkup_p = 'A')   	or ((ie_checkup_p 	  = 'S')  and (nr_seq_checkup is not null))
						or ((ie_checkup_p 	  = 'N')  and (nr_seq_checkup is null)))
	and	dt_avaliacao <= (sysdate - qt_dias_p);
				
/*
  ie_com_atendimento,   
  ie_com_prescricao,    
  ie_com_checkup:   	S -> Sim
                	N -> N�o
			A -> Ambos
*/

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	nm_usuario_w;
exit when C01%notfound;
	begin
	liberar_avaliacao(nr_sequencia_w,nm_usuario_w);
	end;
end loop;
close C01;

/*
Update	med_avaliacao_paciente
set 	dt_liberacao 	= sysdate
where 	dt_liberacao is null
and	((cd_funcao_p = '0') or (nr_seq_tipo_avaliacao in (	select	t.nr_sequencia
								from	med_tipo_avaliacao t
								where	t.cd_funcao	= cd_funcao_p)))
and	((ie_com_atendimento_p = 'A')   or ((ie_com_atendimento_p = 'S')  and (nr_atendimento is not null))
					or ((ie_com_atendimento_p = 'N')  and (nr_atendimento is null)))
and	((ie_com_prescricao_p = 'A')    or ((ie_com_prescricao_p  = 'S')  and (nr_prescricao is not null))
					or ((ie_com_prescricao_p  = 'N')  and (nr_prescricao is null)))
and	((ie_checkup_p = 'A')   	or ((ie_checkup_p 	  = 'S')  and (nr_seq_checkup is not null))
					or ((ie_checkup_p 	  = 'N')  and (nr_seq_checkup is null)))
and	dt_avaliacao <= (sysdate - qt_dias_p);
*/


commit;

end Liberar_avaliacoes;
/