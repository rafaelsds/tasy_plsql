create or replace procedure ctb_gerar_report_gerencial( dt_inicio_p             date,
                                                        dt_fim_p                date default null,
                                                        nr_seq_crit_rateio_p    number default null,
                                                        cd_conta_contabil_p     varchar2 default null,
                                                        nm_usuario_p            varchar2) is

-- Interface 3088 - Report Gerencial
-- view utilizada na interface report_gerencial_v
-- chamada no html https://github.com/philips-emr/tasy/blob/dev/src/app/corCtb/corCtbF4/components/wdlg/exportGerencialReport/ExportGerencialReportOkClick.js

type colunas is record (
    vl_total_conta_debito   number(20,2),
    vl_total_conta_credito  number(20,2),
    nm_usuario              varchar2(15),
    dt_alteracao            date,
    nm_usuario_nrec         varchar2(15),
    dt_alteracao_nrec       date,
    nr_seq_movimento        number(10),
    cd_legal_entity         number(4),
    cd_location             varchar2(40),
    cd_department           varchar2(20),
    cd_account              varchar2(20),
    nr_modality             varchar2(10),
    nr_financial_class      number(10),
    nr_special_reporting    varchar2(5),
    nr_future               varchar2(5),
    nr_intercompany_bal     varchar2(20),
    ie_debit_credit         varchar(1),
    vl_debit                number(20,2),
    vl_credit               number(20,2),
    ds_line_description     varchar2(100),
    vl_stat_amount          number(13,2),
    ds_gl_vendor_name       varchar2(200),
    nr_gl_invoice_number    varchar2(10),
    dt_moviment             date,
    cd_externo              varchar2(100),
    cd_conta_contabil       varchar(100),
    ds_adjustment           varchar(20)
);

type rateioItens is table of colunas index by BINARY_INTEGER;

qt_centro_custo_w           number(5)       := 0;
cd_centro_custo_w           number(15,0)    := 0;
vl_movimento_w              number(20,2)    := 0;
vl_total_movi_w             number(20,2)    := 0;
vl_movi_deb_w               number(20,2)    := 0;
vl_movi_cred_w              number(20,2)    := 0;
vl_sum_debito_w             number(20,2)    := 0;
vl_sum_credito_w            number(20,2)    := 0;
vl_debito_w                 number(20,2)    := 0;
vl_credito_w                number(20,2)    := 0;
qtd_registros_w             integer         := 0;
registro_atual_w            integer         := 1;
inicio_w                    number(2);
fim_w                       number(2);
cd_classificacao_w          varchar2(255);
nr_modality_w               varchar2(1);
cd_department_w             varchar2(1);
dt_fim_w                    date;
vl_total_debito_w           dvb_report_gerencial.vl_debit%type;
vl_total_credito_w          dvb_report_gerencial.vl_credit%type;
cd_account_w                dvb_report_gerencial.cd_account%type;
nr_financial_class_w        dvb_report_gerencial.nr_financial_class%type;
cd_account_ww               dvb_report_gerencial.cd_account%type;
nr_financial_class_ww       dvb_report_gerencial.nr_financial_class%type;
pr_rateio_deb_w             ctb_criterio_rateio_item.pr_rateio%type;
pr_rateio_cred_w            ctb_criterio_rateio_item.pr_rateio%type;
dvb_report_gerencial_w      dvb_report_gerencial%rowtype;
cd_sistema_contabil_w       centro_custo.cd_sistema_contabil%type;
nr_seq_dvb_w                dvb_report_gerencial.nr_sequencia%type;
ie_debito_credito_w         dvb_report_gerencial.ie_debit_credit%type;
vet_rateio_itens            rateioItens;

cursor  c01 is
        select  1 nr_sequencia,
                v.nr_sequencia nr_seq_movimento,
                nm_usuario_p nm_usuario,
                nm_usuario_p nm_usuario_nrec,
                to_number(nvl(pj.cd_sistema_ant, 0)) cd_legal_entity,
                nvl(es.cd_interno, '0000') cd_location,
                decode(gc.ie_tipo, 'A', 'N', 'P', 'N', 'S') cd_department,
                nvl(cc.cd_sistema_contabil,'0000') cd_account,
                lpad(decode(gc.ie_tipo, 'R', 0, 'D', 0, 'C', 0, 299), 3, '0') nr_modality,
                to_number(nvl(c.cd_externo, 0)) nr_financial_class,
                '0000' nr_special_reporting,
                '0000' nr_future,
                decode(v2.ie_intercompany, 'S', ex.cd_interno, '000000') nr_intercompany_bal,
                v.ie_debito_credito ie_debit_credit,
                v.vl_debito vl_debit,
                v.vl_credito vl_credit,
                replace(replace(substr((v.ds_historico || ' - ' || v.ds_compl_historico), 1, 100),chr(10),''),chr(13),'') ds_line_description,
                0 vl_stat_amount,
                v.ds_observacao ds_gl_vendor_name,
                v.nr_seq_agrupamento nr_gl_invoice_number,
                v.dt_movimento dt_moviment,
                '000' cd_externo,
                sysdate dt_atualizacao,
                sysdate dt_atualizacao_nrec,
                v.cd_conta_contabil,
                '04.Adjustment' ds_adjustment
        from    lote_contabil lt,
                ctb_movimento_v v,
                estabelecimento es,
                empresa e,
                grupo_emp_estrutura g,
                ctb_classif_movimento c,
                ctb_movimento v2,
                estabelecimento ex,
                pessoa_juridica_estab pj,
                conta_contabil cc,
                ctb_grupo_conta gc
        where   v.cd_estabelecimento        = es.cd_estabelecimento
        and     es.cd_empresa               = e.cd_empresa
        and     c.nr_sequencia(+)           = v.nr_seq_classif_movto
        and     v.nr_sequencia              = v2.nr_sequencia
        and     v2.cd_estab_intercompany    = ex.cd_estabelecimento(+)
        and     g.nr_seq_grupo              = holding_pck.get_grupo_emp_estrut(e.cd_empresa)
        and     g.cd_empresa                = e.cd_empresa
        and     es.cd_cgc                   = pj.cd_cgc(+)
        and     es.cd_estabelecimento       = pj.cd_estabelecimento(+)
        and     cc.cd_conta_contabil        = v.cd_conta_contabil
        and     gc.cd_grupo                 = v.cd_grupo
        and     lt.nr_lote_contabil         = v.nr_lote_contabil
        and     lt.dt_atualizacao_saldo     is not null
        and     v.dt_movimento              between dt_inicio_p  and fim_dia(dt_fim_w)
        order by
                v.nr_sequencia;

c01_w   c01%rowtype;

cursor  c02 is
        select  a.cd_centro_custo,
                b.cd_sistema_contabil,
                a.vl_movimento,
                trim(b.cd_classificacao)
        from    centro_custo b,
                ctb_movto_centro_custo a
        where   nr_seq_movimento    = dvb_report_gerencial_w.nr_seq_movimento
        and     a.cd_centro_custo   = b.cd_centro_custo;

cursor  c03 is
        select  sum(vl_debit)       vl_deb_imposto,
                sum(vl_credit)      vl_cred_imposto,
                cd_account,
                nr_financial_class,
                cd_location
        from    dvb_report_gerencial
        where   cd_account = '8275'
        -- and 1 = 2
        group by
                cd_account,
                nr_financial_class,
                cd_location
        order by
                vl_cred_imposto,vl_deb_imposto;

l_c03   c03%rowtype;

cursor  c04 (ie_deb_cred_p varchar2) is
        select  sum(v.vl_credito)       vl_movto,
                count(*) over()         qt_registro,
                nvl(cc.cd_sistema_contabil,'0000') cd_account,
                to_number(nvl(c.cd_externo, 0)) nr_financial_class,
                nvl(es.cd_interno,'0000') cd_location
        from    lote_contabil           lt,
                ctb_movimento_v         v,
                estabelecimento         es,
                empresa                 e,
                grupo_emp_estrutura     g,
                ctb_classif_movimento   c,
                ctb_movimento           v2,
                estabelecimento         ex,
                pessoa_juridica_estab   pj,
                conta_contabil          cc,
                ctb_grupo_conta         gc
        where   v.cd_estabelecimento                                    = es.cd_estabelecimento
        and     ctb_obter_se_classif_sup(cc.cd_classificacao, '3.1.01') = 'S'
        and     es.cd_empresa                                           = e.cd_empresa
        and     c.nr_sequencia(+)                                       = v.nr_seq_classif_movto
        and     v.nr_sequencia                                          = v2.nr_sequencia
        and     v2.cd_estab_intercompany                                = ex.cd_estabelecimento(+)
        and     g.nr_seq_grupo                                          = holding_pck.get_grupo_emp_estrut(e.cd_empresa)
        and     g.cd_empresa                                            = e.cd_empresa
        and     es.cd_cgc                                               = pj.cd_cgc(+)
        and     es.cd_estabelecimento                                   = pj.cd_estabelecimento(+)
        and     cc.cd_conta_contabil                                    = v.cd_conta_contabil
        and     gc.cd_grupo                                             = v.cd_grupo
        and     lt.nr_lote_contabil                                     = v.nr_lote_contabil
        and     lt.dt_atualizacao_saldo                                 is not null
        and     v.dt_movimento                                          between dt_inicio_p  and fim_dia(dt_fim_w)
        and     l_c03.cd_location                                       = nvl(es.cd_interno,'0000')
        and     ie_deb_cred_p = 'D'
        group by
                nvl(cc.cd_sistema_contabil,'0000'),
                to_number(nvl(c.cd_externo, 0)),
                nvl(es.cd_interno,'0000')
        union all
        select  sum(v.vl_debito)        vl_movto,
                count(*) over()         qt_registro,
                nvl(cc.cd_sistema_contabil,'0000') cd_account,
                to_number(nvl(c.cd_externo, 0)) nr_financial_class,
                nvl(es.cd_interno,'0000') cd_location
        from    lote_contabil           lt,
                ctb_movimento_v         v,
                estabelecimento         es,
                empresa                 e,
                grupo_emp_estrutura     g,
                ctb_classif_movimento   c,
                ctb_movimento           v2,
                estabelecimento         ex,
                pessoa_juridica_estab   pj,
                conta_contabil          cc,
                ctb_grupo_conta         gc
        where   v.cd_estabelecimento                                    = es.cd_estabelecimento
        and     ctb_obter_se_classif_sup(cc.cd_classificacao, '3.1.01') = 'S'
        and     es.cd_empresa                                           = e.cd_empresa
        and     c.nr_sequencia(+)                                       = v.nr_seq_classif_movto
        and     v.nr_sequencia                                          = v2.nr_sequencia
        and     v2.cd_estab_intercompany                                = ex.cd_estabelecimento(+)
        and     g.nr_seq_grupo                                          = holding_pck.get_grupo_emp_estrut(e.cd_empresa)
        and     g.cd_empresa                                            = e.cd_empresa
        and     es.cd_cgc                                               = pj.cd_cgc(+)
        and     es.cd_estabelecimento                                   = pj.cd_estabelecimento(+)
        and     cc.cd_conta_contabil                                    = v.cd_conta_contabil
        and     gc.cd_grupo                                             = v.cd_grupo
        and     lt.nr_lote_contabil                                     = v.nr_lote_contabil
        and     lt.dt_atualizacao_saldo                                 is not null
        and     v.dt_movimento                                          between dt_inicio_p  and fim_dia(dt_fim_w)
        and     l_c03.cd_location                                       = nvl(es.cd_interno,'0000')
        and     ie_deb_cred_p = 'C'
        group by
                nvl(cc.cd_sistema_contabil,'0000'),
                to_number(nvl(c.cd_externo, 0)),
                nvl(es.cd_interno,'0000')
        order by
                vl_movto;

l_c04   c04%rowtype;

begin

exec_sql_dinamico(nm_usuario_p,'truncate table dvb_report_gerencial');

dt_fim_w := dt_inicio_p;

open c01;
loop
fetch c01 into
  c01_w;
exit when c01%notfound;
  begin

    /* OS 2149849 - Atribuindo cada coluna, para evitar erros de fetch */
    dvb_report_gerencial_w.nr_seq_movimento     := c01_w.nr_seq_movimento;
    dvb_report_gerencial_w.nm_usuario           := c01_w.nm_usuario;
    dvb_report_gerencial_w.nm_usuario_nrec      := c01_w.nm_usuario_nrec;
    dvb_report_gerencial_w.cd_legal_entity      := c01_w.cd_legal_entity;
    dvb_report_gerencial_w.cd_location          := c01_w.cd_location;
    dvb_report_gerencial_w.cd_department        := c01_w.cd_department;
    dvb_report_gerencial_w.cd_account           := c01_w.cd_account;
    dvb_report_gerencial_w.nr_modality          := c01_w.nr_modality;
    dvb_report_gerencial_w.nr_financial_class   := c01_w.nr_financial_class;
    dvb_report_gerencial_w.nr_special_reporting := c01_w.nr_special_reporting;
    dvb_report_gerencial_w.nr_future            := c01_w.nr_future;
    dvb_report_gerencial_w.nr_intercompany_bal  := c01_w.nr_intercompany_bal;
    dvb_report_gerencial_w.ie_debit_credit      := c01_w.ie_debit_credit;
    dvb_report_gerencial_w.vl_debit             := c01_w.vl_debit;
    dvb_report_gerencial_w.vl_credit            := c01_w.vl_credit;
    dvb_report_gerencial_w.ds_line_description  := c01_w.ds_line_description;
    dvb_report_gerencial_w.vl_stat_amount       := c01_w.vl_stat_amount;
    dvb_report_gerencial_w.ds_gl_vendor_name    := c01_w.ds_gl_vendor_name;
    dvb_report_gerencial_w.nr_gl_invoice_number := c01_w.nr_gl_invoice_number;
    dvb_report_gerencial_w.dt_moviment          := c01_w.dt_moviment;
    dvb_report_gerencial_w.cd_externo           := c01_w.cd_externo;
    dvb_report_gerencial_w.dt_atualizacao       := c01_w.dt_atualizacao;
    dvb_report_gerencial_w.dt_atualizacao_nrec  := c01_w.dt_atualizacao_nrec;
    dvb_report_gerencial_w.cd_conta_contabil    := c01_w.cd_conta_contabil;
    dvb_report_gerencial_w.ds_adjustment        := c01_w.ds_adjustment;
    /*Fim OS 2149849 */

    nr_modality_w                           := dvb_report_gerencial_w.cd_department;
    cd_department_w                         := dvb_report_gerencial_w.cd_department;
    dvb_report_gerencial_w.nr_modality      := '000';
    dvb_report_gerencial_w.cd_department    := '0000';

    select  count(nr_sequencia)
    into    qt_centro_custo_w
    from    ctb_movto_centro_custo
    where   nr_seq_movimento        = dvb_report_gerencial_w.nr_seq_movimento;

    begin
    select  nvl(c.cd_externo, '000')
    into    dvb_report_gerencial_w.cd_externo
    from    ctb_classif_movimento   c,
            ctb_movimento_v         v
    where   v.nr_seq_classif_movto  = c.nr_sequencia(+)
    and     v.nr_sequencia          = dvb_report_gerencial_w.nr_seq_movimento
    and     v.cd_conta_contabil     = dvb_report_gerencial_w.cd_conta_contabil;
    exception
      when others then
        dvb_report_gerencial_w.cd_externo := '000';
    end;

    begin
      for x in (select 1 qt_registro
                from   ctb_movimento a,
                       lote_contabil b
                where  a.nr_lote_contabil       = b.nr_lote_contabil
                and    a.nr_sequencia           = dvb_report_gerencial_w.nr_seq_movimento
                and    b.cd_tipo_lote_contabil  in (2,51))
      loop
        if(x.qt_registro = 1) then
          begin
            select substr(obter_nome_pf_pj(null, a.cd_cgc_emitente),1,255)
            into   dvb_report_gerencial_w.ds_gl_vendor_name
            from   nota_fiscal a,
                   ctb_movimento b
            where  a.nr_sequencia = b.nr_seq_agrupamento
            and    b.nr_sequencia = dvb_report_gerencial_w.nr_seq_movimento;
            exception
              when others then
              dvb_report_gerencial_w.ds_gl_vendor_name := '';
          end;
        end if;
      end loop;
    end;

    if (dvb_report_gerencial_w.cd_account not in ('1603', '1604')) then
        dvb_report_gerencial_w.nr_intercompany_bal  := '000000';
    end if;

    if (dvb_report_gerencial_w.cd_account = '1210') then
        dvb_report_gerencial_w.nr_special_reporting := '30129';
        dvb_report_gerencial_w.cd_externo           := '000';
    end if;

    ie_debito_credito_w                             := dvb_report_gerencial_w.ie_debit_credit;

    if  (nvl(qt_centro_custo_w,0) > 0) then
    open c02;
    loop
    fetch c02 into
      cd_centro_custo_w,
      cd_sistema_contabil_w,
      vl_movimento_w,
      cd_classificacao_w;
    exit when c02%notfound;
      begin
      dvb_report_gerencial_w.nr_modality    := '000';
      dvb_report_gerencial_w.cd_department  := '0000';

      if(nr_modality_w = 'S') then
        dvb_report_gerencial_w.nr_modality       := nvl(cd_sistema_contabil_w, '000');
      end if;

      select    instr(cd_classificacao_w, '.') + 1
      into      inicio_w
      from      dual;

      select    instr(cd_classificacao_w, '.',1,2) - 1
      into      fim_w
      from      dual;

      if ((cd_department_w = 'S') and (inicio_w <> 1 and fim_w <> -1)) then
        select  substr(cd_classificacao_w, inicio_w, (fim_w + 1) - inicio_w )
        into    dvb_report_gerencial_w.cd_department
        from    dual;
      end if;

      if(ie_debito_credito_w = 'C') then
      dvb_report_gerencial_w.vl_credit  := vl_movimento_w;
      dvb_report_gerencial_w.vl_debit   := 0;
      else
      dvb_report_gerencial_w.vl_debit   := vl_movimento_w;
      dvb_report_gerencial_w.vl_credit  := 0;
      end if;

      select    dvb_report_gerencial_seq.nextval
      into      dvb_report_gerencial_w.nr_sequencia
      from      dual;

      insert into dvb_report_gerencial
      values dvb_report_gerencial_w;
      end;
    end loop;
    close c02;

    else
    select  dvb_report_gerencial_seq.nextval
    into    dvb_report_gerencial_w.nr_sequencia
    from    dual;

    insert into dvb_report_gerencial
    values dvb_report_gerencial_w;
    end if;
end;
end loop;
close c01;
/*
vl_deb_imposto,
vl_cred_imposto,
cd_account,
nr_financial_class
*/
open c03;
loop
fetch c03 into l_c03;
exit when c03%notfound;
        registro_atual_w        := 0;
        vl_total_credito_w      := 0;
        vl_total_debito_w       := 0;

        if (nvl(l_c03.vl_cred_imposto,0) <> 0) then
                --Debito
                select  dvb_report_gerencial_seq.nextval
                into    nr_seq_dvb_w
                from    dual;

                insert into dvb_report_gerencial(
                        nr_sequencia,
                        nr_seq_movimento,
                        nm_usuario,
                        dt_atualizacao,
                        nm_usuario_nrec,
                        dt_atualizacao_nrec,
                        cd_legal_entity,
                        cd_location,
                        cd_department,
                        cd_account,
                        nr_modality,
                        nr_financial_class,
                        nr_special_reporting,
                        nr_future,
                        nr_intercompany_bal,
                        ie_debit_credit,
                        vl_debit,
                        vl_credit,
                        ds_line_description,
                        vl_stat_amount,
                        ds_gl_vendor_name,
                        nr_gl_invoice_number,
                        dt_moviment,
                        cd_externo,
                        ds_adjustment)
                values(
                        nr_seq_dvb_w,
                        0,
                        nm_usuario_p,
                        sysdate,
                        nm_usuario_p,
                        sysdate,
                        0, --dvb_report_gerencial_w.cd_legal_entity,
                        l_c03.cd_location, --dvb_report_gerencial_w.cd_location,
                        'S', --dvb_report_gerencial_w.cd_department,
                        l_c03.cd_account,
                        '000',
                        --'9', --l_c03.nr_financial_class,
                        l_c03.nr_financial_class,
                        '0000', --dvb_report_gerencial_w.nr_special_reporting,
                        '0000',
                        '000000', --dvb_report_gerencial_w.nr_intercompany_bal,
                        'D',
                        l_c03.vl_cred_imposto,
                        0,
                        'Rateio Imposto',
                        0,
                        '', --dvb_report_gerencial_w.ds_gl_vendor_name,
                        '', --dvb_report_gerencial_w.nr_gl_invoice_number,
                        dt_inicio_p, --dvb_report_gerencial_w.dt_moviment,
                        '000',
                        '04.Adjustment');

                select  sum(v.vl_debito)        vl_sum_total
                into    vl_sum_debito_w
                from    lote_contabil           lt,
                        ctb_movimento_v         v,
                        estabelecimento         es,
                        empresa                 e,
                        grupo_emp_estrutura     g,
                        ctb_classif_movimento   c,
                        ctb_movimento           v2,
                        estabelecimento         ex,
                        pessoa_juridica_estab   pj,
                        conta_contabil          cc,
                        ctb_grupo_conta         gc
                where   v.cd_estabelecimento                                    = es.cd_estabelecimento
                and     ctb_obter_se_classif_sup(cc.cd_classificacao, '3.1.01') = 'S'
                and     es.cd_empresa                                           = e.cd_empresa
                and     c.nr_sequencia(+)                                       = v.nr_seq_classif_movto
                and     v.nr_sequencia                                          = v2.nr_sequencia
                and     v2.cd_estab_intercompany                                = ex.cd_estabelecimento(+)
                and     g.nr_seq_grupo                                          = holding_pck.get_grupo_emp_estrut(e.cd_empresa)
                and     g.cd_empresa                                            = e.cd_empresa
                and     es.cd_cgc                                               = pj.cd_cgc(+)
                and     es.cd_estabelecimento                                   = pj.cd_estabelecimento(+)
                and     cc.cd_conta_contabil                                    = v.cd_conta_contabil
                and     gc.cd_grupo                                             = v.cd_grupo
                and     lt.nr_lote_contabil                                     = v.nr_lote_contabil
                and     lt.dt_atualizacao_saldo                                 is not null
                and     v.dt_movimento                                          between dt_inicio_p  and fim_dia(dt_fim_w)
                and     l_c03.cd_location                                       = nvl(es.cd_interno,'0000')
                ;

                open c04 ('C');
                loop
                fetch c04 into l_c04;
                exit when c04%notfound;
                        registro_atual_w := registro_atual_w+1;
                        vl_movi_cred_w := 0;

                        /* Se existe valor a debito na conta de receita */
                        if (l_c04.vl_movto <> 0) then
                                /* Se for o ultimo registro pega o restante */
                                if (registro_atual_w = l_c04.qt_registro) then
                                        vl_movi_cred_w := l_c03.vl_cred_imposto - vl_total_credito_w;
                                else
                                /* senao for o ultimo registro, calcula o percentual */
                                        vl_movi_cred_w := (l_c04.vl_movto/vl_sum_debito_w) * l_c03.vl_cred_imposto;
                                end if;
                                vl_total_credito_w := vl_total_credito_w + vl_movi_cred_w;
                        end if;

                        if (vl_movi_cred_w <> 0) then
                                --Contas rateio
                                select  dvb_report_gerencial_seq.nextval
                                into    nr_seq_dvb_w
                                from    dual;

                                insert into dvb_report_gerencial(
                                        nr_sequencia,
                                        nr_seq_movimento,
                                        nm_usuario,
                                        dt_atualizacao,
                                        nm_usuario_nrec,
                                        dt_atualizacao_nrec,
                                        cd_legal_entity,
                                        cd_location,
                                        cd_department,
                                        cd_account,
                                        nr_modality,
                                        nr_financial_class,
                                        nr_special_reporting,
                                        nr_future,
                                        nr_intercompany_bal,
                                        ie_debit_credit,
                                        vl_debit,
                                        vl_credit,
                                        ds_line_description,
                                        vl_stat_amount,
                                        ds_gl_vendor_name,
                                        nr_gl_invoice_number,
                                        dt_moviment,
                                        cd_externo,
                                        ds_adjustment)
                                values(
                                        nr_seq_dvb_w,
                                        dvb_report_gerencial_w.nr_seq_movimento,
                                        nm_usuario_p,
                                        sysdate,
                                        nm_usuario_p,
                                        sysdate,
                                        0 , --dvb_report_gerencial_w.cd_legal_entity,
                                        l_c04.cd_location,
                                        'S', --dvb_report_gerencial_w.cd_department,
                                        l_c04.cd_account,
                                        '000', --dvb_report_gerencial_w.nr_modality,
                                        --'789', -- nr_financial_class_w,
                                        -- nr_financial_class_ww,
                                        l_c04.nr_financial_class,
                                        '0000', --dvb_report_gerencial_w.nr_special_reporting,
                                        '0000',
                                        '000000', --dvb_report_gerencial_w.nr_intercompany_bal,
                                        'C',
                                        0,
                                        vl_movi_cred_w,
                                        'Rateio Imposto',
                                        0,
                                        '', --dvb_report_gerencial_w.ds_gl_vendor_name,
                                        '', --dvb_report_gerencial_w.nr_gl_invoice_number,
                                        dt_inicio_p, --dvb_report_gerencial_w.dt_moviment,
                                        '000',
                                        '04.Adjustment'
                                        );
                        end if;
                end loop;
                close c04;
        end if;

        if (nvl(l_c03.vl_deb_imposto,0) <> 0) then
                --Credito
                select  dvb_report_gerencial_seq.nextval
                into    nr_seq_dvb_w
                from    dual;

                insert into dvb_report_gerencial(
                        nr_sequencia,
                        nr_seq_movimento,
                        nm_usuario,
                        dt_atualizacao,
                        nm_usuario_nrec,
                        dt_atualizacao_nrec,
                        cd_legal_entity,
                        cd_location,
                        cd_department,
                        cd_account,
                        nr_modality,
                        nr_financial_class,
                        nr_special_reporting,
                        nr_future,
                        nr_intercompany_bal,
                        ie_debit_credit,
                        vl_debit,
                        vl_credit,
                        ds_line_description,
                        vl_stat_amount,
                        ds_gl_vendor_name,
                        nr_gl_invoice_number,
                        dt_moviment,
                        cd_externo,
                        ds_adjustment)
                values(
                        nr_seq_dvb_w,
                        0,
                        nm_usuario_p,
                        sysdate,
                        nm_usuario_p,
                        sysdate,
                        0, --dvb_report_gerencial_w.cd_legal_entity,
                        l_c03.cd_location,
                        'S',-- dvb_report_gerencial_w.cd_department,
                        l_c03.cd_account,
                        '000',
                        --'9', --l_c03.nr_financial_class,
                        l_c03.nr_financial_class,
                        '0000', --dvb_report_gerencial_w.nr_special_reporting,
                        '0000',
                        '000000', --dvb_report_gerencial_w.nr_intercompany_bal,
                        'C',
                        0,
                        l_c03.vl_deb_imposto,
                        'Rateio Imposto',
                        0,
                        '', --dvb_report_gerencial_w.ds_gl_vendor_name,
                        '', --dvb_report_gerencial_w.nr_gl_invoice_number,
                        dt_inicio_p, --dvb_report_gerencial_w.dt_moviment,
                        '000',
                        '04.Adjustment');

                select  sum(v.vl_credito)        vl_sum_total
                into    vl_sum_credito_w
                from    lote_contabil           lt,
                        ctb_movimento_v         v,
                        estabelecimento         es,
                        empresa                 e,
                        grupo_emp_estrutura     g,
                        ctb_classif_movimento   c,
                        ctb_movimento           v2,
                        estabelecimento         ex,
                        pessoa_juridica_estab   pj,
                        conta_contabil          cc,
                        ctb_grupo_conta         gc
                where   v.cd_estabelecimento                                    = es.cd_estabelecimento
                and     ctb_obter_se_classif_sup(cc.cd_classificacao, '3.1.01') = 'S'
                and     es.cd_empresa                                           = e.cd_empresa
                and     c.nr_sequencia(+)                                       = v.nr_seq_classif_movto
                and     v.nr_sequencia                                          = v2.nr_sequencia
                and     v2.cd_estab_intercompany                                = ex.cd_estabelecimento(+)
                and     g.nr_seq_grupo                                          = holding_pck.get_grupo_emp_estrut(e.cd_empresa)
                and     g.cd_empresa                                            = e.cd_empresa
                and     es.cd_cgc                                               = pj.cd_cgc(+)
                and     es.cd_estabelecimento                                   = pj.cd_estabelecimento(+)
                and     cc.cd_conta_contabil                                    = v.cd_conta_contabil
                and     gc.cd_grupo                                             = v.cd_grupo
                and     lt.nr_lote_contabil                                     = v.nr_lote_contabil
                and     lt.dt_atualizacao_saldo                                 is not null
                and     v.dt_movimento                                          between dt_inicio_p  and fim_dia(dt_fim_w)
                and     l_c03.cd_location                                       = nvl(es.cd_interno,'0000')
                ;

                open c04 ('D');
                loop
                fetch c04 into l_c04;
                exit when c04%notfound;
                        registro_atual_w := registro_atual_w+1;
                        vl_movi_deb_w := 0;

                        /* Se existe valor a debito na conta de receita */
                        if (l_c04.vl_movto <> 0) then
                                /* Se for o ultimo registro pega o restante */
                                if (registro_atual_w = l_c04.qt_registro) then
                                        vl_movi_deb_w := l_c03.vl_deb_imposto - vl_total_debito_w;
                                else
                                /* senao for o ultimo registro, calcula o percentual */
                                        vl_movi_deb_w := (l_c04.vl_movto/vl_sum_credito_w) * l_c03.vl_deb_imposto;
                                end if;
                                vl_total_debito_w := vl_total_debito_w + vl_movi_deb_w;
                        end if;

                        if (vl_movi_deb_w <> 0) then
                                --Contas rateio
                                select  dvb_report_gerencial_seq.nextval
                                into    nr_seq_dvb_w
                                from    dual;

                                insert into dvb_report_gerencial(
                                        nr_sequencia,
                                        nr_seq_movimento,
                                        nm_usuario,
                                        dt_atualizacao,
                                        nm_usuario_nrec,
                                        dt_atualizacao_nrec,
                                        cd_legal_entity,
                                        cd_location,
                                        cd_department,
                                        cd_account,
                                        nr_modality,
                                        nr_financial_class,
                                        nr_special_reporting,
                                        nr_future,
                                        nr_intercompany_bal,
                                        ie_debit_credit,
                                        vl_debit,
                                        vl_credit,
                                        ds_line_description,
                                        vl_stat_amount,
                                        ds_gl_vendor_name,
                                        nr_gl_invoice_number,
                                        dt_moviment,
                                        cd_externo,
                                        ds_adjustment)
                                values(
                                        nr_seq_dvb_w,
                                        dvb_report_gerencial_w.nr_seq_movimento,
                                        nm_usuario_p,
                                        sysdate,
                                        nm_usuario_p,
                                        sysdate,
                                        0, --dvb_report_gerencial_w.cd_legal_entity,
                                        l_c04.cd_location,
                                        'S', --dvb_report_gerencial_w.cd_department,
                                        l_c04.cd_account,
                                        '000', --dvb_report_gerencial_w.nr_modality,
                                        --'789', -- nr_financial_class_w,
                                        -- nr_financial_class_ww,
                                        l_c04.nr_financial_class,
                                        '0000', --dvb_report_gerencial_w.nr_special_reporting,
                                        '0000',
                                        '000000', --dvb_report_gerencial_w.nr_intercompany_bal,
                                        'D',
                                        vl_movi_deb_w,
                                        0,
                                        'Rateio Imposto',
                                        0,
                                        '', --dvb_report_gerencial_w.ds_gl_vendor_name,
                                        '', --dvb_report_gerencial_w.nr_gl_invoice_number,
                                        dt_inicio_p, --dvb_report_gerencial_w.dt_moviment,
                                        '000',
                                        '04.Adjustment');
                        end if;
                end loop;
                close c04;
        end if;
end loop;
close c03;

commit;
end ctb_gerar_report_gerencial;
/
