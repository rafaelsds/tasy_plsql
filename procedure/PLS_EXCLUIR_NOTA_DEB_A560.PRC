create or replace
procedure pls_excluir_nota_deb_a560(	nr_seq_nota_debito_p	ptu_nota_debito.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

qt_registro_w		pls_integer;

begin
select	count(qt)
into	qt_registro_w
from	(select	1 qt
	from	titulo_pagar		tp,
		ptu_nota_deb_conclusao	dc
	where	dc.nr_sequencia		= tp.nr_seq_nota_deb_conclusao
	and	dc.nr_seq_nota_debito	= nr_seq_nota_debito_p
	union all
	select	1 qt
	from	titulo_receber		tr,
		ptu_nota_deb_conclusao	dc
	where	dc.nr_sequencia		= tr.nr_seq_nota_deb_conclusao
	and	dc.nr_seq_nota_debito	= nr_seq_nota_debito_p);


if	(qt_registro_w > 0) then
	-- N�o � poss�vel excluir um arquivo A560 com t�tulo vinculado.
	wheb_mensagem_pck.exibir_mensagem_abort(1032536);
end if;

delete	ptu_nota_deb_fat_ndr
where	nr_seq_nota_debito	= nr_seq_nota_debito_p;

delete	ptu_nota_deb_dados
where	nr_seq_nota_debito	= nr_seq_nota_debito_p;

delete	ptu_nota_deb_credor_deved
where	nr_seq_nota_debito	= nr_seq_nota_debito_p;

delete	ptu_nota_deb_bol_inst
where	nr_seq_nota_deb_bol	in (	select	nr_sequencia
					from	ptu_nota_deb_bol
					where	nr_seq_nota_debito	= nr_seq_nota_debito_p);

delete	ptu_nota_deb_bol_obs
where	nr_seq_nota_deb_bol	in (	select	nr_sequencia
					from	ptu_nota_deb_bol
					where	nr_seq_nota_debito	= nr_seq_nota_debito_p);

delete	ptu_nota_deb_bol_ld
where	nr_seq_nota_deb_bol	in (	select	nr_sequencia
					from	ptu_nota_deb_bol
					where	nr_seq_nota_debito	= nr_seq_nota_debito_p);

delete	ptu_nota_deb_bol
where	nr_seq_nota_debito	= nr_seq_nota_debito_p;

delete	ptu_nota_deb_conclusao
where	nr_seq_nota_debito	= nr_seq_nota_debito_p;

delete	ptu_nota_debito
where	nr_sequencia	= nr_seq_nota_debito_p;

commit;

end pls_excluir_nota_deb_a560;
/