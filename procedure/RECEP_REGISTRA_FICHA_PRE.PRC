create or replace
procedure RECEP_REGISTRA_FICHA_PRE(
			cd_pessoa_fisica_p		number,
			cd_recepcao_p			number,
			cd_setor_atendimento_p	number,
			nm_usuario_p			Varchar2,
			cd_estabelecimento_p	number,
			cd_empresa_p			number,
			nr_seq_ficha_p	out		number) is 

begin


	
if	(cd_pessoa_fisica_p is not null
	and cd_recepcao_p is not null
	and cd_setor_atendimento_p is not null) then
	begin
			
			
	select 	recep_ficha_pre_seq.NEXTVAL
	into 	nr_seq_ficha_p	
	from	dual;
	
	insert into RECEP_FICHA_PRE(NR_SEQUENCIA,
		DT_ATUALIZACAO,
		CD_ESTABELECIMENTO,
		CD_SETOR_ATENDIMENTO,
		NM_USUARIO,
		CD_EMPRESA,
		CD_PESSOA_FISICA,
		CD_RECEPCAO,
		DT_PRE_RECEPCAO,
		IE_STATUS,
		IE_PAC_RECEPCAO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC)
	values(
		nr_seq_ficha_p,
		sysdate,
		cd_estabelecimento_p,
		cd_setor_atendimento_p,
		nm_usuario_p,
		cd_empresa_p,
		cd_pessoa_fisica_p,
		cd_recepcao_p,
		sysdate,
		1,
		'N',
		sysdate,
		nm_usuario_p);
	end;
end if;
commit;

end RECEP_REGISTRA_FICHA_PRE;
/
