create or replace
procedure	pls_gerar_anexo_aut_web(ie_tipo_anexo_p			varchar2,
					qt_dose_dia_p			number,
					qt_dose_total_p			number,
					qt_dias_previsto_p		number,
					dt_inicio_previsto_p		varchar2,
					ds_procedimento_cirurgico_p	varchar2,
					dt_real_proc_cirurgico_p	varchar2,
					ds_quimioterapia_p		varchar2,
					dt_quimioterapia_p		varchar2,
					nm_profissional_solic_p		varchar2,
					nr_telef_prof_solic_p		varchar2,
					ds_email_prof_solic_p		varchar2,
					ds_observacao_p			varchar2,
					qt_altura_p			number,
					qt_peso_p			number,
					qt_superficie_corporal_p	number,
					nr_ciclo_atual_p		number,
					dt_radioterapia_p		varchar2,
					ds_area_irradiada_p		varchar2,
					nm_usuario_p			varchar2,
					nr_seq_guia_p			number,
					ds_especificacao_p		varchar2,
					ds_justificativa_p		varchar2,
					nr_ciclo_previsto_p		number,
					qt_intervalo_ciclo_p		number,
					nr_campos_p			number,
					ie_sexo_p			varchar2,
					qt_idade_benef_p		number,
					nr_seq_anexo_p		out	number,
					nr_dia_ciclo_atual_p		number) is 
					
					
nr_sequencia_w		pls_lote_anexo_guias_aut.nr_sequencia%type;
ie_sexo_w		pls_lote_anexo_guias_aut.ie_sexo%type;
qt_idade_benef_w	pls_lote_anexo_guias_aut.qt_idade_benef%type;
nr_seq_segurado_w	pls_segurado.nr_sequencia%type;
cd_guia_w		pls_guia_plano.cd_guia%type;

begin

ie_sexo_w := ie_sexo_p;
qt_idade_benef_w := qt_idade_benef_p;

begin
	select  nr_seq_segurado,
		cd_guia
	into	nr_seq_segurado_w,
		cd_guia_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_p;

	if	(ie_sexo_w is null) then
		ie_sexo_w := pls_obter_dados_segurado(nr_seq_segurado_w, 'SXS');
		if	(ie_sexo_w = 'M') then
			ie_sexo_w := '1';
		elsif	(ie_sexo_w = 'F') then
			ie_sexo_w := '3';
		end if;
	end if;

	if	(qt_idade_benef_w is null) then
		qt_idade_benef_w :=	pls_obter_dados_segurado(nr_seq_segurado_w, 'ID');
	end if;
exception
when others then
	null;
end;

select	pls_lote_anexo_guias_aut_seq.nextval
into	nr_sequencia_w
from	dual;


insert	into	pls_lote_anexo_guias_aut(qt_dose_dia, qt_dose_total, qt_dias_previsto,
					 dt_inicio_previsto, nr_sequencia, ds_procedimento_cirurgico,
					 dt_real_proc_cirurgico, ds_quimioterapia, dt_quimioterapia,
					 nm_profissional_solic, nr_telef_prof_solic, ds_email_prof_solic,
					 ds_observacao, qt_altura, qt_peso,
					 qt_superficie_corporal, nr_ciclo_atual, dt_radioterapia,
					 ds_area_irradiada, nm_usuario, nm_usuario_nrec,
					 dt_atualizacao, dt_atualizacao_nrec, nr_seq_guia,
					 ie_tipo_anexo, ds_especificacao, ds_justificativa,
					 nr_ciclo_previsto, qt_intervalo_ciclo, nr_campos,
					 ie_sexo, qt_idade_benef, cd_guia_referencia,
					 nr_dia_ciclo_atual)
				values  (qt_dose_dia_p, qt_dose_total_p, qt_dias_previsto_p,
					 to_date(dt_inicio_previsto_p,'dd/mm/yyyy'), nr_sequencia_w, ds_procedimento_cirurgico_p,
					 to_date(dt_real_proc_cirurgico_p,'dd/mm/yyyy'), ds_quimioterapia_p, to_date(dt_quimioterapia_p,'dd/mm/yyyy'),
					 nm_profissional_solic_p, nr_telef_prof_solic_p, ds_email_prof_solic_p,
					 ds_observacao_p, qt_altura_p, qt_peso_p,
					 qt_superficie_corporal_p, nr_ciclo_atual_p, to_date(dt_radioterapia_p,'dd/mm/yyyy'),
					 ds_area_irradiada_p, nm_usuario_p, nm_usuario_p,
					 sysdate, sysdate, nr_seq_guia_p,
					 ie_tipo_anexo_p, ds_especificacao_p, ds_justificativa_p,
					 nr_ciclo_previsto_p, qt_intervalo_ciclo_p, nr_campos_p,
					 ie_sexo_w, qt_idade_benef_w, cd_guia_w,
					 nr_dia_ciclo_atual_p);


nr_seq_anexo_p := nr_sequencia_w;

commit;

end pls_gerar_anexo_aut_web;
/
