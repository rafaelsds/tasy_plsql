create or replace
procedure gerar_pedido_me(	nr_solic_compra_p		varchar2,
				nr_pedido_p			varchar2,
				nm_usuario_p			varchar2,
				ie_aprovada_p		out	varchar2,
				nr_ordem_compra_p	in out	number) is 

qt_registro_w			number(10);			
nr_seq_pedido_w			number(10);
filial_w				varchar2(50);
codigofornecedor_w		varchar2(20);
dataprevistaentrega_w		date;
codigocondicaopagamento_w		varchar2(10);
numeropedidome_w			varchar2(20);
qt_regra_fabric_mat_w		number(10);
valorfrete_w			number(13,4);
tipofrete_w			varchar2(1);
cd_estabelecimento_w		number(10);
cd_condicao_pagamento_w		number(10);
cd_comprador_w			varchar2(10);
cd_moeda_padrao_w		number(10);
nm_pessoa_contato_w		varchar2(100);
cd_pessoa_solicitante_w		varchar2(10);
ie_liberar_ordem_cotacao_w		varchar2(15);
cd_versao_atual_w			varchar2(50);
nr_seq_item_w			number(10);
codigoproduto_w			varchar2(50);
quantidade_w			number(13,4);
valorunitario_w			number(13,4);
percentualdesconto_w		number(13,4);
percentualipi_w			number(13,4);
vl_item_liquido_w			number(13,4);
vl_tributo_w			number(13,4);
codigounidade_w			varchar2(30);
observacao_w			varchar2(4000);
nr_ordem_compra_w		number(10);
cd_material_w			number(10);
cd_unidade_medida_w		varchar2(30);
nr_item_oci_w			number(10);
nr_item_solic_compra_w		number(10);
cd_local_estoque_w		number(10);
cd_centro_custo_w			number(10);
cd_conta_contabil_w		varchar2(20);
ie_tipo_conta_w			number(10);
cd_tributo_w			number(10);
ds_material_direto_w		varchar2(255);
ie_urgente_w			varchar2(1);
ie_aviso_chegada_w		varchar2(1);
dt_aprovacao_w			date;
dt_entrega_w			date;
qt_entrega_w			number(13,4);
fabricante_w			varchar2(50);
nr_seq_fabric_int_w		number(10);
nr_seq_fabric_mat_w		number(10);
ie_aprovada_w			varchar2(1) := 'N';
ie_erro_w				varchar2(1) := 'N';
nr_solic_compra_w		number(10);
nr_solic_compra_ww		number(10);
qt_diferenca_w			number(15,4);
nr_sequencia_ent_w		number(10);
ie_gera_oc_w			varchar2(1) := 'S';
qt_registros_w			number(10);

cursor c01 is
select	nr_sequencia,
	filial,
	codigofornecedor,
	dataprevistaentrega,
	codigocondicaopagamento,
	nvl(valorfrete,0),
	decode(tipofrete,'CIF','C','F')
from	w_me_pedido
where	numeropedidome = numeropedidome_w
and	nvl(ie_lido,'N')	= 'N';

cursor c02 is
select	nr_sequencia,
	codigoproduto,
	nvl(quantidade,0),
	nvl(valorunitario,0),
	nvl(percentualdesconto,0),
	nvl(percentualipi,0),
	codigounidade,
	observacao,
	fabricante,
	somente_numero(numerorequisicao)
from	w_me_pedido_itens
where	nr_seq_pedido		= nr_seq_pedido_w;

cursor c03 is
select	pkg_date_utils.start_of(data, 'DD', null),
	quantidade
from	w_me_pedido_itens_ent
where	nr_seq_item = 	nr_seq_item_w;
				
begin

ie_gera_oc_w	:= 'S';

select	substr(nr_solic_compra_p||'@',1,instr(nr_solic_compra_p||'@','@')-1)
into	nr_solic_compra_ww
from	dual;

numeropedidome_w := nr_pedido_p;

delete	log_me
where	nr_prepedido = numeropedidome_w
and	nr_solic_compra = nr_solic_compra_ww;

select	count(*)
into	qt_registro_w
from	solic_compra
where	nr_solic_compra		= nr_solic_compra_ww;

if	(qt_registro_w > 0) and
	(numeropedidome_w <> 'X') then

	select	b.cd_comprador_padrao,
		b.cd_moeda_padrao,
		a.cd_pessoa_solicitante,
		b.ie_liberar_ordem_cotacao,
		a.cd_local_estoque,
		a.cd_centro_custo,
		a.cd_conta_contabil,
		a.ie_urgente,
		nvl(a.ie_aviso_chegada,'N')
	into	cd_comprador_w,
		cd_moeda_padrao_w,
		cd_pessoa_solicitante_w,
		ie_liberar_ordem_cotacao_w,
		cd_local_estoque_w,
		cd_centro_custo_w,
		cd_conta_contabil_w,
		ie_urgente_w,
		ie_aviso_chegada_w
	from	solic_compra a,
		parametro_compras b
	where	a.cd_estabelecimento = b.cd_estabelecimento
	and	a.nr_solic_compra = nr_solic_compra_ww;

	open C01;
	loop
	fetch C01 into	
		nr_seq_pedido_w,
		filial_w,
		codigofornecedor_w,
		dataprevistaentrega_w,
		codigocondicaopagamento_w,
		valorfrete_w,
		tipofrete_w;
	exit when C01%notfound;
		begin
		
		select	obter_conversao_interna_int(null,'ESTABELECIMENTO','CD_ESTABELECIMENTO',filial_w,'ME'),
			obter_conversao_interna_int(null,'CONDICAO_PAGAMENTO','CD_CONDICAO_PAGAMENTO',codigocondicaopagamento_w,'ME')
		into	cd_estabelecimento_w,
			cd_condicao_pagamento_w
		from	dual;
		
		select	count(*)
		into	qt_registro_w
		from	estabelecimento
		where	cd_estabelecimento = cd_estabelecimento_w;
		
		if	(qt_registro_w = 0) then
			ie_erro_w := 'S';
			inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC', Wheb_mensagem_pck.get_Texto(301971, 'FILIAL_W='|| FILIAL_W), '000002', nm_usuario_p, 'G',numeropedidome_w, nr_seq_pedido_w);
		end if;								/*N�o existe convers�o v�lida para a filial #@FILIAL_W#@*/
		


		select	count(*)
		into	qt_registro_w
		from	condicao_pagamento
		where	cd_condicao_pagamento = cd_condicao_pagamento_w;
		
		if	(qt_registro_w = 0) then
			ie_erro_w := 'S';
			inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC', Wheb_mensagem_pck.get_Texto(301972, 'CODIGOCONDICAOPAGAMENTO_W='|| CODIGOCONDICAOPAGAMENTO_W), '000003', nm_usuario_p, 'G',numeropedidome_w, nr_seq_pedido_w);
		end if;								/*N�o existe convers�o v�lida para a condi��o de pagamento #@CODIGOCONDICAOPAGAMENTO_W#@*/

		select	count(*)
		into	qt_registro_w
		from	pessoa_juridica
		where	cd_cgc = codigofornecedor_w;
		
		if	(qt_registro_w = 0) then
			ie_erro_w := 'S';
			inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC', Wheb_mensagem_pck.get_Texto(301973, 'CODIGOFORNECEDOR_W='|| CODIGOFORNECEDOR_W), '000004', nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);
		end if;								/*N�o existe pessoa jur�dica cadastrada no Tasy com CNPJ #@CODIGOFORNECEDOR_W#@*/		
				
		select	count(*)
		into	qt_registro_w
		from	comprador
		where	cd_pessoa_fisica = cd_comprador_w;
		
		if	(qt_registro_w = 0) then
			ie_erro_w := 'S';
			inserir_log_me('SC',nr_solic_compra_ww, '112', 'ROC', Wheb_mensagem_pck.get_Texto(301974) , '000005',nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);
		end if;								/*'N�o existe comprador padr�o informado nos par�metros de compras'*/
		
		select	count(*)
		into	qt_registro_w
		from	moeda
		where	cd_moeda = cd_moeda_padrao_w;
		
		if	(qt_registro_w = 0) then
			ie_erro_w := 'S';
			inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC', Wheb_mensagem_pck.get_Texto(301976) , '000006', nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);
		end if;								/*N�o existe moeda padr�o informada nos par�metros de compras*/
		
		select	count(*)
		into	qt_registro_w
		from	ordem_compra
		where	nr_documento_externo = numeropedidome_w;
		--and	dt_baixa is null;
		
		if	(qt_registro_w > 0) then
			ie_gera_oc_w := 'N';
			
			/* Esse log foi retirado porque o ME n�o pode receber essa mensagem. A unica a��o nesse caso, � n�o gerar a ordem de compra
			inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC', Wheb_mensagem_pck.get_Texto(301977, 'NUMEROPEDIDOME_W='|| NUMEROPEDIDOME_W), '000007', nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);*/
											/*J� existe o pre-pedido #@NUMEROPEDIDOME_W#@ no Tasy*/
		end if;
		
	
		if	(ie_erro_w = 'N') and
			(ie_gera_oc_w = 'S') then
			
			select	nm_pessoa_contato
			into	nm_pessoa_contato_w
			from	pessoa_juridica
			where	cd_cgc = codigofornecedor_w;
			
			select	nvl(max(cd_versao_atual),'0')
			into	cd_versao_atual_w
			from	aplicacao_tasy
			where	cd_aplicacao_tasy = 'Tasy';
			
			if	(nr_ordem_compra_p is not null) then
				begin
				select	nr_ordem_compra
				into	nr_ordem_compra_w
				from	ordem_compra
				where	nr_ordem_compra = nr_ordem_compra_p;
				exception
				when others then
					nr_ordem_compra_p	:=	null;
				end;
			end if;

			if	(nr_ordem_compra_p is null) then
				
				select	ordem_compra_seq.nextval
				into	nr_ordem_compra_w
				from	dual;
				
				insert into ordem_compra(
					nr_ordem_compra,
					cd_estabelecimento,
					cd_cgc_fornecedor,
					cd_condicao_pagamento,
					cd_comprador,
					dt_ordem_compra,
					dt_atualizacao,
					nm_usuario,
					cd_moeda,
					ie_situacao,
					dt_inclusao,
					cd_pessoa_solicitante,
					ie_frete,
					vl_frete,
					ds_pessoa_contato,
					ds_observacao,
					dt_entrega,
					ie_aviso_chegada,
					ie_emite_obs,
					ie_urgente,
					vl_despesa_acessoria,
					nr_documento_externo,
					vl_desconto,
					ie_tipo_ordem,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_dados_importacao,
					ie_origem_imp)
				values(	nr_ordem_compra_w,
					cd_estabelecimento_w,
					codigofornecedor_w,
					cd_condicao_pagamento_w,
					cd_comprador_w,
					sysdate,
					sysdate,
					nm_usuario_p,
					cd_moeda_padrao_w,
					'A',
					sysdate,
					cd_pessoa_solicitante_w,
					tipofrete_w,
					valorfrete_w,
					nm_pessoa_contato_w,
					'',
					dataprevistaentrega_w,
					'N',
					'N',
					'N',
					0,
					numeropedidome_w,
					0,
					'E',
					sysdate,
					nm_usuario_p,
					cd_versao_atual_w,
					'MercadoEletronico');
			end if;
			
			nr_ordem_compra_p := nr_ordem_compra_w;
			
			open C02;
			loop
			fetch C02 into	
				nr_seq_item_w,
				codigoproduto_w,
				quantidade_w,
				valorunitario_w,
				percentualdesconto_w,
				percentualipi_w,
				codigounidade_w,
				observacao_w,
				fabricante_w,
				nr_solic_compra_w;
			exit when C02%notfound;
				begin
				
				select	nvl(obter_cod_int_material(codigoproduto_w,'ME',cd_estabelecimento_w),0)
				into	cd_material_w
				from	dual;
				
				select	count(*)
				into	qt_registro_w
				from	material
				where	cd_material = cd_material_w;
				
				if	(qt_registro_w = 0) then
					ie_erro_w := 'S';
					inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC',Wheb_mensagem_pck.get_Texto(301978, 'CODIGOPRODUTO_W='|| CODIGOPRODUTO_W) , '000008', nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);
				end if;								/*N�o existe convers�o v�lida para o c�digo do produto #@CODIGOPRODUTO_W#@*/
				
				
				if	(quantidade_w = 0 ) then
					ie_erro_w := 'S';					/*Pedido #@NUMEROPEDIDOME_W#@ gerou quantidade zero para o item #@CODIGOPRODUTO_W#@*/
					inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC',wheb_mensagem_pck.get_texto(795131,'NUMEROPEDIDOME_W='|| numeropedidome_w ||';CODIGOPRODUTO_W='|| codigoproduto_w), '000008', nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);
				end if;				
				
				select	nvl(min(nr_item_solic_compra),0)
				into	nr_item_solic_compra_w
				from	solic_compra_item
				where	nr_solic_compra		= nr_solic_compra_w
				and	cd_material		= cd_material_w;
				
				
				if	(cd_conta_contabil_w is null) then

					if	(cd_centro_custo_w is null) then
						ie_tipo_conta_w	:= 2;
					else
						ie_tipo_conta_w	:= 3;
					end if;

					define_conta_material(	cd_estabelecimento_w,
								cd_material_w,
								ie_tipo_conta_w,
								null, null, null, null, null, null, null,
								cd_local_estoque_w,
								null, sysdate,
								cd_conta_contabil_w,
								cd_centro_custo_w,null);
				end if;

				if	(ie_erro_w = 'N') and
					(fabricante_w is not null) then
				
					select	somente_numero(obter_conversao_interna_int(null,'MAT_FABRICANTE','NR_SEQUENCIA',fabricante_w,'ME'))
					into	nr_seq_fabric_int_w
					from	dual;
					
					select	count(*)
					into	qt_regra_fabric_mat_w
					from	regra_fabric_integracao
					where	cd_material = cd_material_w
					and	(cd_estabelecimento is null or cd_estabelecimento = cd_estabelecimento_w)
					and	(ie_integracao = 'S' or ie_integracao = 'ME');
					
					select	nvl(max(nr_seq_fabric),0)
					into	nr_seq_fabric_mat_w
					from	material
					where	cd_material = cd_material_w;
					
					if	(nr_seq_fabric_int_w > 0) and
						(nr_seq_fabric_mat_w > 0) and
						(nr_seq_fabric_mat_w <> nr_seq_fabric_int_w) then
						ie_erro_w := 'S';
						inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC',WHEB_MENSAGEM_PCK.get_texto(301979,'FABRICANTE_W='|| FABRICANTE_W ||';CODIGOPRODUTO_W='|| CODIGOPRODUTO_W), '000025', nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);
													/*O fabricante #@FABRICANTE_W#@ � inv�lido para o material #@CODIGOPRODUTO_W#@*/
					elsif	(qt_regra_fabric_mat_w > 0) and
						(nr_seq_fabric_mat_w = 0) and
						(nr_seq_fabric_int_w > 0) then

						select	count(*)
						into	qt_registro_w
						from	regra_fabric_integracao
						where	cd_material = cd_material_w
						and	nr_seq_fabric = nr_seq_fabric_int_w
						and	(cd_estabelecimento is null or cd_estabelecimento = cd_estabelecimento_w)
						and	(ie_integracao = 'S' or ie_integracao = 'ME');
							
						if	(qt_registro_w = 0) then
							ie_erro_w := 'S';
							inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC',WHEB_MENSAGEM_PCK.get_texto(301982,'FABRICANTE_W='|| FABRICANTE_W ||';CODIGOPRODUTO_W='|| CODIGOPRODUTO_W) , '000024', nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);
														/*O fabricante #@FABRICANTE_W#@ � inv�lido para o material #@CODIGOPRODUTO_W#@*/
						end if;
					end if;
				end if;
				
				

				
				if	(ie_erro_w = 'N') and
					(nr_item_solic_compra_w > 0) then
					
					vl_item_liquido_w		:= 0;
					vl_item_liquido_w		:= nvl(quantidade_w,0) * nvl(valorunitario_w,0);
					
					if	(percentualdesconto_w > 0) then
						vl_item_liquido_w	:= vl_item_liquido_w - dividir((percentualdesconto_w * 100),vl_item_liquido_w);
					end if;
					
					select	cd_unidade_medida_compra
					into	cd_unidade_medida_w
					from	solic_compra_item
					where	nr_solic_compra		= nr_solic_compra_w
					and	nr_item_solic_compra	= nr_item_solic_compra_w;
					
					select	substr(ds_material_direto,1,255)
					into	ds_material_direto_w
					from	solic_compra_item
					where	nr_solic_compra		= nr_solic_compra_w
					and	nr_item_solic_compra	= nr_item_solic_compra_w;
					
					select	nvl(max(nr_item_oci),0) + 1
					into	nr_item_oci_w
					from	ordem_compra_item
					where	nr_ordem_compra = nr_ordem_compra_w;
					
					insert into ordem_compra_item(
						nr_ordem_compra,
						nr_item_oci,
						cd_material,
						cd_unidade_medida_compra,
						vl_unitario_material,
						qt_material,
						dt_atualizacao,
						nm_usuario,
						ie_situacao,
						cd_local_estoque,
						ds_observacao,						
						vl_item_liquido,
						cd_centro_custo,
						cd_conta_contabil,
						nr_solic_compra,
						nr_item_solic_compra,
						qt_original,
						vl_desconto,
						pr_descontos,
						pr_desc_financ,
						vl_unit_mat_original,
						ds_material_direto,
						vl_total_item)
					values(	nr_ordem_compra_w,
						nr_item_oci_w,
						cd_material_w,
						cd_unidade_medida_w,
						valorunitario_w,
						quantidade_w,
						sysdate,
						nm_usuario_p,
						'A',
						cd_local_estoque_w,
						observacao_w,						
						vl_item_liquido_w,
						cd_centro_custo_w,
						cd_conta_contabil_w,
						nr_solic_compra_w,
						nr_item_solic_compra_w,
						quantidade_w,
						0,
						percentualdesconto_w,
						0,
						valorunitario_w,
						ds_material_direto_w,
						round((quantidade_w * valorunitario_w),4));
						
					select	quantidade_w - nvl(sum(quantidade),0),
						max(nr_sequencia)
					into	qt_diferenca_w,
						nr_sequencia_ent_w
					from	w_me_pedido_itens_ent
					where	nr_seq_item = nr_seq_item_w;
					
					if	(qt_diferenca_w <> 0) then
						update	w_me_pedido_itens_ent
						set	quantidade = quantidade + qt_diferenca_w
						where	nr_sequencia = nr_sequencia_ent_w;
					end if;
						
					open C03;
					loop
					fetch C03 into	
						dt_entrega_w,
						qt_entrega_w;
					exit when C03%notfound;
						begin
						
						select	count(*)
						into	qt_registros_w
						from	ordem_compra_item_entrega
						where	nr_ordem_compra = nr_ordem_compra_w
						and	nr_item_oci = nr_item_oci_w
						and	dt_prevista_entrega = dt_entrega_w;
						
						if	(qt_registros_w = 0) then
						
							insert into ordem_compra_item_entrega(
								nr_sequencia,
								nr_ordem_compra,
								nr_item_oci,
								dt_prevista_entrega,
								qt_prevista_entrega,
								dt_atualizacao,
								nm_usuario,
								dt_entrega_original,
								dt_entrega_limite)
							values(	ordem_compra_item_entrega_seq.nextval,
								nr_ordem_compra_w,
								nr_item_oci_w,
								dt_entrega_w,
								qt_entrega_w,
								sysdate,
								nm_usuario_p,
								dt_entrega_w,
								dt_entrega_w);
						else
							update	ordem_compra_item_entrega
							set	qt_prevista_entrega = qt_prevista_entrega + qt_entrega_w
							where	nr_ordem_compra = nr_ordem_compra_w
							and	nr_item_oci = nr_item_oci_w
							and	dt_prevista_entrega = dt_entrega_w;							
						end if;
						end;
					end loop;
					close C03;
			
						
					if	(percentualipi_w > 0) then
				
						select	nvl(min(cd_tributo),0)
						into	cd_tributo_w
						from	tributo
						where	ie_situacao		= 'A'
						and	ie_tipo_tributo		= 'IPI'
						and	ie_corpo_item		= 'I';
						
						if	(cd_tributo_w > 0) then
							
							vl_tributo_w	:= 0;
							vl_tributo_w	:= (dividir(percentualipi_w,100) * vl_item_liquido_w);
							
							insert into ordem_compra_item_trib(
								nr_ordem_compra,
								nr_item_oci,
								cd_tributo,
								pr_tributo,
								vl_tributo,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec)
							values(	nr_ordem_compra_w,
								nr_item_oci_w,
								cd_tributo_w,
								percentualipi_w,
								vl_tributo_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p);
						end if;
					end if;					
				end if;
				end;
			end loop;
			close C02;
		
			update	ordem_compra
			set	cd_local_entrega	= cd_local_estoque_w,
				ie_urgente	= ie_urgente_w,
				ie_aviso_chegada	= ie_aviso_chegada_w
			where	nr_ordem_compra	= nr_ordem_compra_w;
					
			select	count(*)
			into	qt_registro_w
			from	ordem_compra_item
			where	nr_ordem_compra = nr_ordem_compra_w;
		
			if	(qt_registro_w = 0) then
				delete from ordem_compra
				where	nr_ordem_compra = nr_ordem_compra_w;
			else
				calcular_liquido_ordem_compra(nr_ordem_compra_w, nm_usuario_p);
				gerar_ordem_compra_venc(nr_ordem_compra_w, nm_usuario_p);
				baixar_solic_compra_com_ordem(nr_ordem_compra_w);
						
				if	(ie_liberar_ordem_cotacao_w in ('A','S')) then
					liberar_ordem_ME(nr_ordem_compra_w, ie_liberar_ordem_cotacao_w, nm_usuario_p);
				
					select	dt_aprovacao
					into	dt_aprovacao_w
					from	ordem_compra
					where	nr_ordem_compra = nr_ordem_compra_w;
				
					if	(dt_aprovacao_w is not null) then
						ie_aprovada_w	:= 'S';			
					end if;
				end if;
				update	w_me_pedido
				set	ie_lido			= 'S'
				where	numerorequisicao	= nr_solic_compra_ww;
			
				update	solic_compra
				set	nr_documento_externo	= numeropedidome_w
				where	nr_solic_compra		= nr_solic_compra_ww;
			end if;
		
		end if;
		
		if	(ie_erro_w = 'S') then				
			delete	ordem_compra
			where	nr_ordem_compra = nr_ordem_compra_w;			
		end if;
		end;
	end loop;
	close C01;
else
	ie_erro_w := 'S';
	inserir_log_me('SC', nr_solic_compra_ww, '112', 'ROC',Wheb_mensagem_pck.get_Texto(301984, 'NR_SOLIC_COMPRA_P='|| nr_solic_compra_ww) ,'000001', nm_usuario_p,'G',numeropedidome_w,nr_seq_pedido_w);	
end if;								/*N�o existe no tasy a solicita��o de compra n�mero #@NR_SOLIC_COMPRA_P#@*/



if	(nr_seq_pedido_w > 0) then
	update	w_me_pedido
	set	ie_lido	= 'S'
	where	nr_sequencia = nr_seq_pedido_w;
end if;	

ie_aprovada_p := ie_aprovada_w;

commit;

end gerar_pedido_me;
/
