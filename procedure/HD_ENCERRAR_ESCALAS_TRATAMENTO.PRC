create or replace
procedure hd_encerrar_escalas_tratamento(cd_pessoa_fisica_p		number) is 

    CURSOR c01(cd_pessoa_fisica_p VARCHAR2) IS
        SELECT hed.nr_sequencia
          FROM hd_escala_dialise hed
         WHERE hed.cd_pessoa_fisica = cd_pessoa_fisica_p
           AND hed.dt_fim IS NULL;

BEGIN
    FOR i IN c01(cd_pessoa_fisica_p => cd_pessoa_fisica_p) LOOP
        hd_encerrar_escala_tratamento(nr_seq_escala_p => i.nr_sequencia);
    END LOOP;

    UPDATE hd_escala_dialise_dia a
       SET a.dt_fim_escala_dia = SYSDATE
     WHERE a.dt_fim_escala_dia IS NULL
       AND EXISTS (SELECT 1
              FROM hd_escala_dialise b
             WHERE b.nr_sequencia = a.nr_seq_escala
               AND b.cd_pessoa_fisica = cd_pessoa_fisica_p);
    COMMIT;

END hd_encerrar_escalas_tratamento;
/

