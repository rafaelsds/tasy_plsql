create or replace
procedure spa_desfazer_liberacao(	nr_seq_spa_p	Number,
					nm_usuario_p	Varchar2) is 
ie_status_w	spa.ie_status%type;

begin

select	ie_status
into 	ie_status_w
from 	spa
where 	nr_sequencia = nr_seq_spa_p;

update	spa
set	ie_status = 1,
	dt_liberacao = null,
	nm_usuario_liberacao = null,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_seq_spa_p;

delete 	from spa_aprovacao
where	nr_seq_spa = nr_seq_spa_p;

update	spa_hist_email a
set	dt_estorno_lib_spa 	= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao  	= sysdate
where	a.nr_seq_spa		= nr_seq_spa_p
and	dt_estorno_lib_spa	is null;

spa_gerar_historico(nr_seq_spa_p,nm_usuario_p,ie_status_w,1,8,'','N');

commit;

end spa_desfazer_liberacao;
/
