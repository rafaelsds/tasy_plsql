create or replace
procedure avisar_aprovacao_pendente_sme(nr_seq_ordem_serv_p	man_ordem_serv_impacto.nr_seq_ordem_serv%type,
				nr_seq_impacto_p	man_ordem_serv_impacto.nr_sequencia%type) is

ds_email_titulo_w	varchar2(255);
ds_email_corpo_w	varchar2(255);
ds_email_ori_w	usuario.ds_email%type;

cursor c01 is
	select	distinct substr(obter_nome_pf(b.cd_pessoa_fisica),1,80) nm_pessoa_fisica,
		x.ds_email
	from	man_ordem_serv_imp_resp a,
		sme_equipe_regra b,
		usuario x
	where	b.nr_seq_sme_equipe	= a.nr_seq_sme_equipe
	and	x.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nr_seq_impacto	= nr_seq_impacto_p
	and	nvl(b.ie_situacao, 'A')	= 'A'
	and	nvl(a.ie_resposta, 'N') = 'S'
	and	a.dt_aprovacao is null;
	
begin

if (nr_seq_ordem_serv_p is not null and nr_seq_impacto_p is not null) then
	for r_c01 in c01 loop 
		begin	
			select	nvl(max(x.ds_email), 'support.informatics@philips.com')
			into	ds_email_ori_w
			from	man_ordem_serv_impacto a,
				usuario x
			where 	x.nm_usuario 		= a.nm_usuario
			and	nr_seq_ordem_serv 	= nr_seq_ordem_serv_p;	
			
			if (ds_email_ori_w is not null) and (r_c01.ds_email is not null) then
			
				ds_email_titulo_w	:= obter_desc_expressao(953826);
				ds_email_corpo_w	:= replace(replace(obter_desc_expressao(953857), '#@NM_PESSOA_FISICA#@', r_c01.nm_pessoa_fisica), '#@NR_SEQ_ORDEM_SERV#@', nr_seq_ordem_serv_p);
				enviar_email(ds_email_titulo_w, ds_email_corpo_w, ds_email_ori_w, r_c01.ds_email, 'Tasy', 'M');

				gravar_log_tasy(1516, nr_seq_ordem_serv_p || '-' || r_c01.nm_pessoa_fisica || '-' || r_c01.ds_email, 'Tasy');			
			end if;
		exception
			when others then
				gravar_log_tasy(1415, r_c01.nm_pessoa_fisica, 'Tasy');
		end; 
	end loop;

	commit;
end if;

end avisar_aprovacao_pendente_sme;
/
