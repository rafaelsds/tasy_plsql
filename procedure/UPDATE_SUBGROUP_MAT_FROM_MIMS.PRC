create or replace 
procedure	update_subgroup_mat_from_mims(	cd_subgrupo_material_w in  subgrupo_material.cd_subgrupo_material%type,
						cd_imp_material_p in  imp_material.cd_material%type,
						nm_usuario_p in  varchar2) 
as
   
imp_subgrupo_material_w  imp_subgrupo_material%rowtype;
begin

select a.* 
into	imp_subgrupo_material_w 
from	imp_subgrupo_material a, 
	imp_classe_material b, 
	imp_material c 
where	c.cd_material = cd_imp_material_p 
and	c.cd_classe_material = b.cd_classe_material 
and	b.cd_subgrupo_material = a.cd_subgrupo_material;

if ( imp_subgrupo_material_w.cd_subgrupo_material is not null ) then 
	
	update	subgrupo_material a 
	set	a.dt_atualizacao = sysdate, 
		a.nm_usuario = nm_usuario_p, 
		a.ds_subgrupo_material = 
		imp_subgrupo_material_w.ds_subgrupo_material, 
		
		a.ie_situacao = imp_subgrupo_material_w.ie_situacao, 
		a.cd_sistema_ant = imp_subgrupo_material_w.cd_sistema_ant 
	where	a.cd_subgrupo_material = cd_subgrupo_material_w; 

end if;  
end;
/