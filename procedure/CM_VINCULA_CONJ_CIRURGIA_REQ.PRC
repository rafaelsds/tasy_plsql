create or replace
procedure	cm_vincula_conj_cirurgia_req(
			nr_seq_conj_cont_p	number,
			nr_cirurgia_p		number,
			nm_usuario_p		varchar2) is

nr_seq_agenda_w			number(10);
nr_seq_requisicao_w		number(10);
nr_seq_conjunto_w		number(10);
nr_sequencia_w			number(10);

begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_agenda_w
from	agenda_paciente
where	nr_cirurgia = nr_cirurgia_p;

if	(nr_seq_agenda_w <> 0) then
	begin

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_requisicao_w
	from	cm_requisicao
	where	nr_seq_agenda = nr_seq_agenda_w;

	if	(nr_seq_requisicao_w <> 0) then
		begin

		select	nr_seq_conjunto
		into	nr_seq_conjunto_w
		from	cm_conjunto_cont
		where	nr_sequencia = nr_seq_conj_cont_p
		and	nvl(ie_situacao,'A') = 'A';

		select	cm_requisicao_item_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into cm_requisicao_item(
			nr_sequencia,
			nr_seq_requisicao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_conjunto,
			qt_conjunto,
			cd_motivo_baixa,
			nm_usuario_atend,
			ie_devolucao,
			ie_emprestimo,
			ds_observacao) values (
				nr_sequencia_w,
				nr_seq_requisicao_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_conjunto_w,
				1,
				null,
				null,
				'N',
				'N',

				WHEB_MENSAGEM_PCK.get_texto(457521,'nr_cirurgia='||nr_cirurgia_p));

		cme_baixa_req_barras(nr_seq_requisicao_w,nr_seq_conj_cont_p,nm_usuario_p,1);			

		end;
	end if;

	end;
end if;

end cm_vincula_conj_cirurgia_req;
/
