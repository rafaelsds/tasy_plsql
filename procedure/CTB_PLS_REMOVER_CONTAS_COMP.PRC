create or replace
procedure ctb_pls_remover_contas_comp (	nr_seq_conta_comp_p	number) is

begin

delete	pls_processo_contas_comp
where	nr_sequencia = nr_seq_conta_comp_p;

commit;

end ctb_pls_remover_contas_comp;
/