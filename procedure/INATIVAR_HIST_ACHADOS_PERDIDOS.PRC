create or replace
procedure Inativar_hist_achados_perdidos(
			nr_seq_hist_achado_perd_p	Number,
			ds_justificativa_p		Varchar2,	
			nm_usuario_p		Varchar2) is 

begin

if 	(nr_seq_hist_achado_perd_p is not null) then

	update 	atend_achado_perd_hist
	set 	dt_inativacao 	      = sysdate,
		ds_justificativa      	      = ds_justificativa_p,
		nm_usuario_inativacao = nm_usuario_p
	where   nr_sequencia	      = nr_seq_hist_achado_perd_p; 

end if;

commit;

end Inativar_hist_achados_perdidos;
/