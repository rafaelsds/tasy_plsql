create or replace procedure ctb_gerar_orc_resultado(    nr_seq_planej_p ctb_orc_cenario.nr_sequencia%type,
                                                        nm_usuario_p varchar2) is

nr_sequencia_w          ctb_orcamento.nr_sequencia%type;
nr_seq_mes_ref_w        ctb_mes_ref.nr_sequencia%type;
dt_referencia_ww        date;
index_w                 number(10) := 0;

cursor c_planej_orc_valor is
select a.dt_referencia,
       a.cd_conta_contabil,
       a.cd_centro_custo,
       a.cd_estabelecimento,
       a.cd_empresa,
       sum(a.vl_planej) vl_planej
from   ctb_grupo_conta g,
       conta_contabil c,
       ctb_planej_orc_valor_v a
where  a.cd_conta_contabil  = c.cd_conta_contabil
and    c.cd_grupo           = g.cd_grupo
and    c.cd_empresa         = g.cd_empresa
and    g.ie_tipo in ('C','D','R')
and    a.nr_seq_planej = nr_seq_planej_p
and    a.vl_planej > 0
group by a.dt_referencia,
         a.cd_conta_contabil,
         a.cd_centro_custo,
         a.cd_estabelecimento,
         a.cd_empresa
order by a.dt_referencia;

type t_planej_orc_valor is table of c_planej_orc_valor%rowtype;
v_planej_orc_valor_w    t_planej_orc_valor;

type t_ctb_orcamento is table of ctb_orcamento%rowtype index by pls_integer;
v_ctb_orcamento_w t_ctb_orcamento;

begin

open c_planej_orc_valor;
loop fetch c_planej_orc_valor bulk collect into v_planej_orc_valor_w limit 1000;
    for i in 1 .. v_planej_orc_valor_w.count loop
    begin

        if  ((v_planej_orc_valor_w(i).dt_referencia <> dt_referencia_ww) or (dt_referencia_ww is null)) then
            dt_referencia_ww := v_planej_orc_valor_w(i).dt_referencia;

            select  max(a.nr_sequencia)
            into    nr_seq_mes_ref_w
            from    ctb_mes_ref a
            where   cd_empresa      = v_planej_orc_valor_w(i).cd_empresa
            and     dt_referencia   = dt_referencia_ww;
        end if;

        update  ctb_orcamento a
        set     a.vl_orcado         = v_planej_orc_valor_w(i).vl_planej,
                a.dt_atualizacao    = sysdate,
                a.nm_usuario        = nm_usuario_p
        where   a.cd_estabelecimento    = v_planej_orc_valor_w(i).cd_estabelecimento
        and     a.cd_conta_contabil     = v_planej_orc_valor_w(i).cd_conta_contabil
        and     a.cd_centro_custo       = v_planej_orc_valor_w(i).cd_centro_custo
        and     a.nr_seq_mes_ref        = nr_seq_mes_ref_w;

        if  (sql%notfound) then

            select  ctb_orcamento_seq.nextval
            into    nr_sequencia_w
            from    dual;

            index_w := index_w + 1;

            v_ctb_orcamento_w(index_w).nr_sequencia         := nr_sequencia_w;
            v_ctb_orcamento_w(index_w).cd_estabelecimento   := v_planej_orc_valor_w(i).cd_estabelecimento;
            v_ctb_orcamento_w(index_w).nr_seq_mes_ref       := nr_seq_mes_ref_w;
            v_ctb_orcamento_w(index_w).cd_centro_custo      := v_planej_orc_valor_w(i).cd_centro_custo;
            v_ctb_orcamento_w(index_w).cd_conta_contabil    := v_planej_orc_valor_w(i).cd_conta_contabil;
            v_ctb_orcamento_w(index_w).vl_original          := v_planej_orc_valor_w(i).vl_planej;
            v_ctb_orcamento_w(index_w).vl_orcado            := v_planej_orc_valor_w(i).vl_planej;
            v_ctb_orcamento_w(index_w).vl_realizado         := 0;
            v_ctb_orcamento_w(index_w).ie_cenario           := 'S';
            v_ctb_orcamento_w(index_w).ie_origem_orc        := 'SIS';
            v_ctb_orcamento_w(index_w).dt_atualizacao       := sysdate;
            v_ctb_orcamento_w(index_w).nm_usuario           := nm_usuario_p;
            v_ctb_orcamento_w(index_w).dt_atualizacao_nrec  := sysdate;
            v_ctb_orcamento_w(index_w).nm_usuario_nrec      := nm_usuario_p;

            if  (index_w = 1000) then
                forall j in v_ctb_orcamento_w.first .. v_ctb_orcamento_w.last
                    insert into ctb_orcamento values v_ctb_orcamento_w(j);

                index_w := 0;
                v_ctb_orcamento_w.delete;
                commit;
            end if;
        end if;
    end;
    end loop;
exit when c_planej_orc_valor%notfound;
end loop;
close c_planej_orc_valor;

if  (v_ctb_orcamento_w.count > 0) then
    forall i in v_ctb_orcamento_w.first .. v_ctb_orcamento_w.last
        insert into ctb_orcamento values v_ctb_orcamento_w(i);

    v_ctb_orcamento_w.delete;
end if;

commit;

end ctb_gerar_orc_resultado;
/
