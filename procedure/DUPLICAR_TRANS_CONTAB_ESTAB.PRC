create or replace 
procedure DUPLICAR_TRANS_CONTAB_ESTAB(
		nr_seq_trans_contab_p	number,
		nm_usuario_p		varchar2) is

begin

insert	into TRANS_FINANC_CONTAB
        (NR_SEQUENCIA,
        NR_SEQ_TRANS_FINANC,
        IE_DEBITO_CREDITO,
        IE_REGRA_CONTA ,
        IE_REGRA_CENTRO_CUSTO,
        DT_ATUALIZACAO ,
        NM_USUARIO ,
        CD_CONTA_CONTABIL,
        CD_CENTRO_CUSTO,
        NM_ATRIBUTO,
        CD_HISTORICO ,
        IE_VALOR_ABSOLUTO,
        IE_SITUACAO,
        CD_ESTABELECIMENTO ,
        DT_INICIO_VIGENCIA ,
        DT_FIM_VIGENCIA,
        NR_SEQ_RATEIO_CCUSTO ,
        NR_SEQ_PRODUTO ,
        IE_TIPO_TITULO_PAGAR,
        CD_TIPO_LOTE_CONTABIL,
		CD_TRIBUTO,
		IE_TIPO_TITULO_RECEBER,
		IE_ORIGEM_TIT_PAGAR,
		NR_SEQ_CLASSE_TP,
		IE_ORIGEM_TIT_RECEBER,
		CD_TIPO_RECEBIMENTO,
		IE_TIPO_CUSTO,
		CD_TIPO_BAIXA,
		IE_TIPO_PESSOA,
		NR_SEQ_CLASSE_TR)
select	TRANS_FINANC_CONTAB_seq.nextval,
        a.NR_SEQ_TRANS_FINANC,
        a.IE_DEBITO_CREDITO,
        a.IE_REGRA_CONTA ,
        a.IE_REGRA_CENTRO_CUSTO,
        sysdate,
        nm_usuario_p,
        a.CD_CONTA_CONTABIL,
        a.CD_CENTRO_CUSTO,
        a.NM_ATRIBUTO,
        a.CD_HISTORICO ,
        a.IE_VALOR_ABSOLUTO,
        a.IE_SITUACAO,
        c.CD_ESTABELECIMENTO ,
        a.DT_INICIO_VIGENCIA ,
        a.DT_FIM_VIGENCIA,
        a.NR_SEQ_RATEIO_CCUSTO ,
        a.NR_SEQ_PRODUTO ,
        a.IE_TIPO_TITULO_PAGAR,
        a.cd_tipo_lote_contabil,
        a.CD_TRIBUTO,
		a.IE_TIPO_TITULO_RECEBER,
		a.IE_ORIGEM_TIT_PAGAR,
		a.NR_SEQ_CLASSE_TP,
		a.IE_ORIGEM_TIT_RECEBER,
		a.CD_TIPO_RECEBIMENTO,
		a.IE_TIPO_CUSTO,
		a.CD_TIPO_BAIXA,
		a.IE_TIPO_PESSOA,
		a.NR_SEQ_CLASSE_TR
from	  estabelecimento c,
        transacao_financeira b,
        trans_financ_contab a
where	  c.cd_estabelecimento	<> a.cd_estabelecimento
and	    b.cd_empresa		= c.cd_empresa
and	    a.nr_seq_trans_financ	= b.nr_sequencia
and	    a.nr_sequencia		= nr_seq_trans_contab_p;

commit;

END DUPLICAR_TRANS_CONTAB_ESTAB;
/
