create or replace
procedure gerar_w_iss_pmc_hmd(cd_estabelecimento_p	number,
			nm_usuario_p		varchar2,
			dt_mesano_ref_p		date) is

nr_titulo_w		number(10);
nr_sequencia_w		number(10);
dt_emissao_w		date;
vl_total_nota_w		number(15,2);
vl_tributo_w		number(15,2);
nr_cpf_w			varchar2(11);
ds_razao_social_w		varchar2(80);
ds_endereco_w		varchar2(100);
nr_endereco_w		varchar2(6);
ds_complemento_w		varchar2(20);
ds_bairro_w		varchar2(50);
ds_municipio_w		varchar2(44);
sg_estado_w		varchar2(2);
cd_cep_w		varchar2(8);
nr_seq_reg_w		number(6);
tx_tributo_w		number(7,4);
ie_fora_municipio_w	varchar2(1);
ie_tipo_registrO_w		varchar2(1);
ie_tipo_documento_w	varchar2(1);
ie_tipo_recolhimento_w	varchar2(1);
ds_registro_w		varchar2(1);
nr_nota_fiscal_w		varchar2(20);
cd_serie_nf_w		varchar2(5);
cd_item_lista_serv_w	varchar2(4);
nr_inscricao_munic_w	varchar2(10);
cd_cgc_w		varchar2(255);

Cursor c01 is
/* Notas */
select	1					ie_tipo_registro,
	SUBSTR(DECODE(NVL(LPAD(SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000'),'71724460978',3,'01756209979',3,1),1,1) ie_tipo_documento,
	'N'					ie_tipo_recolhimento,
	'C'					ds_registro,
	a.dt_emissao			dt_emissao,
	a.nr_nota_fiscal				nr_nota_fiscal,
	a.cd_serie_nf				cd_serie_nf,
	decode(upper(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,80)),'CURITIBA','D','F') ie_fora_municipio,
	''					cd_item_lista_serv,
	0,
	0					vl_tributo,
	Elimina_Caracteres_Especiais(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'IM'),1,10)) nr_inscricao_municipal,
	DECODE(NVL(LPAD(SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000'),'71724460978','00000000000000','01756209979','00000000000000','00497738953','00000000000000',nvl(a.cd_cgc,'00000000000000')) cd_cnpj,
	nvl(lpad(substr(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000') nr_cpf,
	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null)),1,100) ds_razao_social,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'R'),1,50) ds_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'NR'),1,06) nr_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CO'),1,20) ds_complemento,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'B'),1,50) ds_bairro,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CI'),1,44) ds_municipio,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'UF'),1,02) sg_estado,
	substr(Elimina_Caracteres_Especiais(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CEP')),1,08) cd_cep,
	0					tx_tributo
from	nota_fiscal a
where	a.ie_situacao in ('3','9')
and	trunc(a.dt_emissao,'month') = trunc(dt_mesano_ref_p,'month')
and	length(a.nr_nota_fiscal) < 9
and	a.dt_atualizacao_estoque is not null
and	substr(obter_se_nota_entrada_saida(a.nr_sequencia),1,1) = 'S'
and	a.cd_estabelecimento	= cd_estabelecimento_p
and a.cd_operacao_nf in (3, 30)
and	((a.cd_cgc_emitente = '76562198000240' and a.cd_operacao_nf = 25) or (a.cd_operacao_nf = 3))
union
select	2					ie_tipo_registro,
	SUBSTR(DECODE(NVL(LPAD(SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000'),'71724460978',3,'01756209979',3,1),1,1) ie_tipo_documento,
	'N'					ie_tipo_recolhimento,
	'E'					ds_registro,
	a.dt_emissao			dt_emissao,
	a.nr_nota_fiscal				nr_nota_fiscal,
	a.cd_serie_nf				cd_serie_nf,
	decode(upper(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,80)),'CURITIBA','D','F') ie_fora_municipio,
	(select max(nvl(cd_darf, substr(obter_codigo_darf(x.cd_tributo, a.cd_estabelecimento, a.cd_cgc,a.cd_pessoa_fisica),1,100)))
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia) cd_item_lista_serv,	
	(select	nvl(sum(b.vl_total_item_nf),0)
	from	nota_fiscal_item b
	where	a.nr_sequencia = b.nr_sequencia) vl_total_item_nf, 
	0					vl_tributo,
	Elimina_Caracteres_Especiais(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'IM'),1,10)) nr_inscricao_municipal,
	DECODE(NVL(LPAD(SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000'),'71724460978','00000000000000','01756209979','00000000000000','00497738953','00000000000000',nvl(a.cd_cgc,'00000000000000')) cd_cnpj,
	nvl(lpad(substr(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000') nr_cpf,
	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null)),1,100) ds_razao_social,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'R'),1,50) ds_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'NR'),1,06) nr_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CO'),1,20) ds_complemento,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'B'),1,50) ds_bairro,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CI'),1,44) ds_municipio,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'UF'),1,02) sg_estado,
	substr(Elimina_Caracteres_Especiais(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CEP')),1,08) cd_cep,
	(select nvl(max(x.tx_tributo),0)
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia) tx_tributo
from	nota_fiscal a
where	substr(obter_se_nota_entrada_saida(a.nr_sequencia),1,1) = 'S'
and	trunc(a.dt_emissao,'month') = trunc(dt_mesano_ref_p,'month')
and	a.ie_situacao = '1'
and	length(a.nr_nota_fiscal) < 9
and	a.dt_atualizacao_estoque is not null
and	a.cd_estabelecimento	=  cd_estabelecimento_p
and a.cd_operacao_nf in (3, 30)
and	((a.cd_cgc_emitente = '76562198000240' and a.cd_operacao_nf = 25) or (a.cd_operacao_nf = 3))
and 1 = 2
union
select	3					ie_tipo_registro,
	SUBSTR(DECODE(NVL(LPAD(SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000'),'71724460978',3,'01756209979',3,1),1,1) ie_tipo_documento,
	decode((select nvl(max(x.tx_tributo),0)
		from	tributo t,
			nota_fiscal_trib x
		where	x.cd_tributo = t.cd_tributo
		and	t.ie_tipo_tributo = 'ISSNR'
		and	x.nr_sequencia = a.nr_sequencia), 0, 
		decode((select nvl(max(x.tx_tributo),0)
				from	tributo t,
					nota_fiscal_trib x
				where	x.cd_tributo = t.cd_tributo
				and	t.ie_tipo_tributo = 'ISSST'
				and	x.nr_sequencia = a.nr_sequencia), 0, 'N', 'S'), 'R') ie_tipo_recolhimento,
	'R'					ds_registro,
	a.dt_emissao			dt_emissao,
	a.nr_nota_fiscal				nr_nota_fiscal,
	a.cd_serie_nf				cd_serie_nf,
	decode(upper(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'CI'),1,80)),'CURITIBA','D','F') ie_fora_municipio,
	(select max(nvl(cd_darf, substr(obter_codigo_darf(x.cd_tributo, a.cd_estabelecimento, a.cd_cgc,a.cd_pessoa_fisica),1,100)))
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_sequencia = a.nr_sequencia) cd_item_lista_serv,
	(select	nvl(sum(b.vl_total_item_nf),0)
	from	nota_fiscal_item b
	where	a.nr_sequencia = b.nr_sequencia) vl_total_item_nf, 
	0					vl_tributo,
	Elimina_Caracteres_Especiais(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,a.cd_cgc,'IM'),1,10)) nr_inscricao_municipal,
	DECODE(NVL(LPAD(SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000'),'71724460978','00000000000000','01756209979','00000000000000','00497738953','00000000000000',nvl(a.cd_cgc,'00000000000000')) cd_cnpj,
	nvl(lpad(substr(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000') nr_cpf,
	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null)),1,40) ds_razao_social,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'R'),1,50) ds_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'NR'),1,06) nr_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CO'),1,20) ds_complemento,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'B'),1,50) ds_bairro,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CI'),1,44) ds_municipio,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'UF'),1,02) sg_estado,
	substr(Elimina_Caracteres_Especiais(obter_dados_pf_pj(a.cd_pessoa_fisica,decode(a.cd_pessoa_fisica,null,a.cd_cgc,null),'CEP')),1,08) cd_cep,
	(select nvl(max(x.tx_tributo),0)
	from	tributo t,
		nota_fiscal_trib x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo in ('ISS', 'ISSNR', 'ISSST')
	and	x.nr_sequencia = a.nr_sequencia)	tx_tributo
from	nota_fiscal a
where	substr(obter_se_nota_entrada_saida(a.nr_sequencia),1,1) = 'E'
and	trunc(a.dt_emissao,'month') = trunc(dt_mesano_ref_p,'month') 
and	length(a.nr_nota_fiscal) < 9
and	a.ie_situacao = '1'
and	a.dt_atualizacao_estoque is not null
and	a.cd_estabelecimento	= cd_estabelecimento_p
and a.cd_operacao_nf in (3, 30)
and	((a.cd_cgc_emitente = '76562198000240' and a.cd_operacao_nf = 25) or (a.cd_operacao_nf = 3))
union
/* Repasse */
select	3 	ie_tipo_registro,
	SUBSTR(DECODE(NVL(LPAD(SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000'),'71724460978',3,'01756209979',3,1),1,1) ie_tipo_documento,
	decode(( select	nvl(max(x.vl_imposto),0)
				from    tributo t,
				titulo_pagar_imposto x
				where   x.cd_tributo = t.cd_tributo
				and    	t.ie_tipo_tributo = 'ISSNR'
				and    	x.nr_titulo = a.nr_titulo), 0,
				decode((select	nvl(max(x.vl_imposto),0)
						from    tributo t,
						titulo_pagar_imposto x
						where   x.cd_tributo = t.cd_tributo
						and    	t.ie_tipo_tributo = 'ISSST'
						and    	x.nr_titulo = a.nr_titulo), 0, 'N', 'S'), 'R')	ie_tipo_recolhimento,
	'R'	ds_registro,
	a.dt_emissao dt_emissao,
	substr(b.nr_repasse_terceiro,1,10) nr_nota_fiscal,
	'1' cd_serie_nf,
	decode(upper(substr(obter_dados_pf_pj(a.cd_pessoa_fisica,null,'CI'),1,80)),'CURITIBA','D','F') ie_fora_municipio,
	'' cd_item_lista_serv,
	b.vl_vencimento,
	(select nvl(max(x.vl_imposto),0)
	from	tributo t,
		titulo_pagar_imposto x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo = 'ISS'
	and	x.nr_titulo = a.nr_titulo) vl_tributo,
	'' nr_inscricao_municipal,
	'' cd_cgc,
	nvl(lpad(substr(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000') nr_cpf,
	substr(obter_nome_pf_pj(a.cd_pessoa_fisica,null),1,60) ds_razao_social,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,null,'R'),1,50) ds_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,null,'NR'),1,06) nr_endereco,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,null,'CO'),1,20) ds_complemento,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,null,'B'),1,50) ds_bairro,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,null,'CI'),1,44) ds_municipio,
	substr(obter_dados_pf_pj(a.cd_pessoa_fisica,null,'UF'),1,02) sg_estado,
	substr(Elimina_Caracteres_Especiais(obter_dados_pf_pj(a.cd_pessoa_fisica,null,'CEP')),1,08) cd_cep,
	(select nvl(max(x.pr_imposto),0)
	from	tributo t,
		titulo_pagar_imposto x
	where	x.cd_tributo = t.cd_tributo
	and	t.ie_tipo_tributo in ('ISS', 'ISSNR', 'ISSST')
	and	x.nr_titulo = a.nr_titulo)	tx_tributo
from	repasse_terceiro_venc b,
	titulo_pagar a
where	a.nr_titulo	= b.nr_titulo
and	trunc(dt_emissao,'month') = trunc(dt_mesano_ref_p,'month')
and	a.nr_repasse_terceiro is not null
and	a.cd_pessoa_fisica is not null
and	a.cd_estabelecimento	= cd_estabelecimento_p
union
SELECT	3 	ie_tipo_registro,
	SUBSTR(DECODE(NVL(LPAD(SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf(a.cd_pessoa_fisica,'CPF')),1,11),11,'0'),'00000000000'),'71724460978',3,'01756209979',3,1),1,1) ie_tipo_documento,
	decode(( select	nvl(max(x.vl_imposto),0)
				from    tributo t,
				titulo_pagar_imposto x
				where   x.cd_tributo = t.cd_tributo
				and    	t.ie_tipo_tributo = 'ISSNR'
				and    	x.nr_titulo = a.nr_titulo), 0,
				decode((select	nvl(max(x.vl_imposto),0)
						from    tributo t,
						titulo_pagar_imposto x
						where   x.cd_tributo = t.cd_tributo
						and    	t.ie_tipo_tributo = 'ISSST'
						and    	x.nr_titulo = a.nr_titulo), 0, 'N', 'S'), 'R')	ie_tipo_recolhimento,
	'R'	ds_registro,
	(select x.dt_emissao from nota_fiscal x where x.nr_sequencia = a.nr_seq_nota_fiscal) dt_emissao,
	substr((select x.nr_nota_fiscal from nota_fiscal x where x.nr_sequencia = a.nr_seq_nota_fiscal), 1, 10) nr_nota_fiscal,
	(select x.cd_serie_nf from nota_fiscal x where x.nr_sequencia = a.nr_seq_nota_fiscal) cd_serie_nf,
	DECODE(UPPER(SUBSTR(obter_dados_pf_pj(NULL,a.cd_cgc,'CI'),1,80)),'CURITIBA','D','F') ie_fora_municipio,
	'' cd_item_lista_serv,
	b.vl_vencimento,
	0 vl_tributo,
	'' nr_inscricao_municipal,
	a.cd_cgc cd_cgc,
	'' nr_cpf,
	SUBSTR(obter_nome_pf_pj(NULL,a.cd_cgc),1,60) ds_razao_social,
	SUBSTR(obter_dados_pf_pj(NULL,a.cd_cgc,'R'),1,50) ds_endereco,
	SUBSTR(obter_dados_pf_pj(NULL,a.cd_cgc,'NR'),1,06) nr_endereco,
	SUBSTR(obter_dados_pf_pj(NULL,a.cd_cgc,'CO'),1,20) ds_complemento,
	SUBSTR(obter_dados_pf_pj(NULL,a.cd_cgc,'B'),1,50) ds_bairro,
	SUBSTR(obter_dados_pf_pj(NULL,a.cd_cgc,'CI'),1,44) ds_municipio,
	SUBSTR(obter_dados_pf_pj(NULL,a.cd_cgc,'UF'),1,02) sg_estado,
	SUBSTR(Elimina_Caracteres_Especiais(obter_dados_pf_pj(NULL,a.cd_cgc,'CEP')),1,08) cd_cep,
	(SELECT NVL((SUM(x.pr_imposto) * 100),0) 
	FROM	tributo t,
		titulo_pagar_imposto x
	WHERE	x.cd_tributo = t.cd_tributo
	AND	t.ie_tipo_tributo in ('ISS', 'ISSNR', 'ISSST')
	AND	x.nr_titulo = a.nr_titulo)	tx_tributo
FROM	repasse_terceiro_venc b,
	titulo_pagar a
WHERE	a.nr_titulo	= b.nr_titulo
AND	TRUNC((select x.dt_emissao from nota_fiscal x where x.nr_sequencia = a.nr_seq_nota_fiscal),'month') = TRUNC(dt_mesano_ref_p,'month')
AND	a.nr_repasse_terceiro IS NOT NULL
AND	a.cd_cgc IS NOT NULL
AND	a.cd_estabelecimento	= cd_estabelecimento_p
and (select x.cd_operacao_nf from nota_fiscal x where x.nr_sequencia = a.nr_seq_nota_fiscal) in (3, 30)
ORDER BY
	ie_tipo_registro,
	nr_nota_fiscal,
	ie_tipo_documento,
	ds_razao_social;

begin

delete	
from	w_iss_pmc
where	nm_usuario	= nm_usuario_p;

/*Header*/
insert	into	w_iss_pmc
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ie_tipo_registro,
	ds_registro,
	nr_inscricao_munic,
	cd_cgc,
	nr_cpf,
	ds_razao_social,
	ie_tipo_arquivo,
	dt_mesano_referencia)
select	w_iss_pmc_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	0,
	'H',
	substr(Elimina_Caracteres_Especiais(a.cd_inscricao_municipal),1,10),
	a.cd_cgc,
	null,
	substr(obter_nome_pf_pj(null,a.cd_cgc),1,100),
	'N',
	dt_mesano_ref_p
from	estabelecimento_v a
where	cd_estabelecimento	= cd_estabelecimento_p;

nr_seq_reg_w	:= 0;

/* Detalhes */
open c01;
loop
fetch c01 into
	ie_tipo_registro_w,
	ie_tipo_documento_w,
	ie_tipo_recolhimento_w,
	ds_registro_w,
	dt_emissao_w,
	nr_nota_fiscal_w,
	cd_serie_nf_w,
	ie_fora_municipio_w,
	cd_item_lista_serv_w,
	vl_total_nota_w,
	vl_tributo_w,
	nr_inscricao_munic_w,
	cd_cgc_w,
	nr_cpf_w,
	ds_razao_social_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_bairro_w,
	ds_municipio_w,
	sg_estado_w,
	cd_cep_w,
	tx_tributo_w;
exit when c01%notfound;

	nr_seq_reg_w	:= nr_seq_reg_w + 1;

	select	w_iss_pmc_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert	into	w_iss_pmc
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_registro,
		ds_registro,
		dt_emissao,
		ie_tipo_documento,
		vl_total_nota,
		vl_tributo,
		nr_cpf,
		ds_razao_social,
		ds_endereco,
		nr_endereco,
		ds_complemento,
		ds_bairro,
		ds_municipio,
		ds_sigla_uf,
		cd_cep,
		nr_seq_reg,
		tx_tributo,
		ie_fora_municipio,
		ie_tipo_recolhimento,
		nr_inscricao_munic,
		cd_item_lista_serv,
		cd_cgc,
		nr_nota_fiscal,
		cd_serie_nf)
	values	(nr_sequencia_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		ie_tipo_registro_w,
		ds_registro_w,
		dt_emissao_w,
		ie_tipo_documento_w,
		vl_total_nota_w,
		vl_tributo_w,
		nr_cpf_w,
		ds_razao_social_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_complemento_w,
		ds_bairro_w,
		ds_municipio_w,
		sg_estado_w,
		cd_cep_w,
		nr_seq_reg_w,
		tx_tributo_w,
		ie_fora_municipio_w,
		ie_tipo_recolhimento_w,
		nr_inscricao_munic_w,
		cd_item_lista_serv_w,
		cd_cgc_w,
		substr(nr_nota_fiscal_w,1,10),
		cd_serie_nf_w);
		
end loop;
close c01;

select	w_iss_pmc_seq.nextval
into	nr_sequencia_w
from	dual;

/* Trailler */
insert	into	w_iss_pmc
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ie_tipo_registro,
	ds_registro,
	qt_total_reg,
	vl_tot_nf_emitida,
	vl_tot_trib_emitida,
	vl_tot_nf_recebida,
	vl_tot_trib_recebida)
select	nr_sequencia_w,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	4,
	'T',
	count(*),
	nvl(sum(decode(ds_registro,'E',vl_total_nota,0)),0),
	nvl(sum(decode(ds_registro,'E',vl_tributo,0)),0),
	nvl(sum(decode(ds_registro,'R',vl_total_nota,0)),0),
	nvl(sum(decode(ds_registro,'R',vl_tributo,0)),0)
from	w_iss_pmc
where	ie_tipo_registro <> '0'
and	nm_usuario	= nm_usuario_p;

	
commit;

end gerar_w_iss_pmc_hmd;
/