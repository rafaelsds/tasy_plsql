create or replace
procedure Inserir_Itens_Grupo_Exame(
			nr_seq_agenda_p			number,
			nr_seq_grupo_exame_p	number,
			cd_convenio_p			number,
			cd_categoria_p			varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p			Varchar2) is 

cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_proc_interno_w	number(10);
nr_seq_agenda_w			number(10)	:= 0;
			
Cursor C01 is
	select	cd_procedimento,
			ie_origem_proced,
			nr_proc_interno
	from	med_exame_padrao
	where	nr_seq_grupo	= nr_seq_grupo_exame_p
	order by 1;
			
begin

select	nvl(max(nr_seq_Agenda),0)
into	nr_seq_agenda_w
from	agenda_paciente_proc
where	nr_sequencia	= nr_seq_Agenda_p;

open C01;
loop
fetch C01 into	
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_proc_interno_w;
exit when C01%notfound;
	begin
	if	(nr_seq_proc_interno_w is not null) and
		((cd_procedimento_w is null) or
		(ie_origem_proced_w is null)) then		
		obter_proc_tab_interno_conv(nr_seq_proc_interno_w, cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, null, null, cd_procedimento_w,
			ie_origem_proced_w, null, null, null, null, null, null, null, null, null, null);
	end if;
	
	nr_seq_agenda_w	:= nr_seq_agenda_w + 1;
	
	if	(cd_procedimento_w is not null) and
		(ie_origem_proced_w is not null) then
		insert into agenda_paciente_proc 
				(nr_sequencia,
				nr_seq_agenda,
				dt_atualizacao,
				nm_usuario,
				cd_procedimento,
				ie_origem_proced,
				nr_seq_proc_interno)
		values
				(nr_seq_Agenda_p,
				nr_seq_agenda_w,
				sysdate,
				nm_usuario_p,
				cd_procedimento_w,
				ie_origem_proced_w,
				nr_seq_proc_interno_w);
	end if;
	end;
end loop;
close C01;


commit;

end Inserir_Itens_Grupo_Exame;
/
