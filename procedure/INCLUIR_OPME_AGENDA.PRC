create or replace
procedure incluir_opme_agenda(	nr_sequencia_p		number,
				cd_material_p		number,
				nm_usuario_p		varchar2) is

begin

if	(nvl(nr_sequencia_p,0) > 0) and
	(nvl(cd_material_p,0) > 0) then
	insert into	agenda_pac_opme(
			nr_sequencia,
			nr_seq_agenda,
			dt_atualizacao,
			nm_usuario,
			cd_material,
			qt_material,
			ie_origem_inf,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_apres,
			ie_autorizado,
			ie_padrao)
	values		(agenda_pac_opme_seq.nextval,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			cd_material_p,
			1,
			'I',
			sysdate,
			nm_usuario_p,
			500,
			'P',
			'S');
commit;			
end if;

end incluir_opme_agenda;
/