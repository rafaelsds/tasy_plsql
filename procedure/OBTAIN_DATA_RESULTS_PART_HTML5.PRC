create or replace procedure obtain_data_results_part_HTML5(nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					dt_atualizacao_p	out date,
					nm_usuario_p		out varchar2,
					ds_observacao_p		out varchar2) is

dt_atualizacao_w	date;
nm_usuario_w 		varchar2(255);
ds_observacao_w 	varchar2(2000);
ie_existe_w		varchar2(1);

begin

ie_existe_w := existe_result_parcial(nr_prescricao_p,nr_seq_prescr_p);

if	(ie_existe_w = 'S') then

	dt_atualizacao_w 	:= obtain_data_result_HTML5(nr_prescricao_p,nr_seq_prescr_p,'D');
	nm_usuario_w 		:= obtain_data_result_HTML5(nr_prescricao_p,nr_seq_prescr_p,'U');
	ds_observacao_w 	:= obtain_data_result_HTML5(nr_prescricao_p,nr_seq_prescr_p,'O');

	dt_atualizacao_p 	:= dt_atualizacao_w;
	nm_usuario_p 		:= nm_usuario_w;
	ds_observacao_p 	:= ds_observacao_w;
end if;

end OBTAIN_DATA_RESULTS_PART_HTML5;
/