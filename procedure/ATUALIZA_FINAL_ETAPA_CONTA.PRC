create or replace
procedure Atualiza_Final_Etapa_Conta
		(nr_seq_etapa_conta_p		number,
		nm_usuario_p			varchar2) is

nr_interno_conta_w		number(10,0);
nr_seq_etapa_w			number(10,0);
ie_finalizar_etapa_w		varchar2(01);
dt_etapa_w			date;
ie_atualiza_final_etapa_w	varchar2(01);
cd_setor_atendimento_w		number(6);
begin

begin
select	a.nr_interno_conta,
	a.nr_seq_etapa,
	a.dt_etapa,
	b.ie_finalizar_etapa,
	nvl(ie_atualiza_final_etapa,'N'),
	a.cd_setor_atendimento
into	nr_interno_conta_w,
	nr_seq_etapa_w,
	dt_etapa_w,
	ie_finalizar_etapa_w,
	ie_atualiza_final_etapa_w,
	cd_setor_atendimento_w
from	fatur_etapa b,
	conta_paciente_etapa a
where	a.nr_seq_etapa	= b.nr_sequencia
and	a.nr_sequencia	= nr_seq_etapa_conta_p;
exception
	when others then
	nr_interno_conta_w:= -1;
end;

if	(ie_finalizar_etapa_w		in ('A','M')) then
	update	conta_paciente_etapa a
	set	dt_fim_etapa		= dt_etapa_w
	where	nr_interno_conta	= nr_interno_conta_w
	and	nr_sequencia		<> nr_seq_etapa_conta_p
	and	dt_fim_etapa		is null
	and	Obter_Se_Finaliza_Etapa(nr_seq_etapa_w,nr_seq_etapa)	= 'S'
	and	exists	(select 1 from fatur_etapa x
			where	x.nr_sequencia			= a.nr_seq_etapa
			and	x.ie_finalizar_etapa		<> 'E');
elsif	(ie_finalizar_etapa_w		= 'R') then
	update	conta_paciente_etapa a
	set	dt_fim_etapa		= dt_etapa_w
	where	nr_interno_conta	= nr_interno_conta_w
	and	nr_sequencia		<> nr_seq_etapa_conta_p
	and	Obter_Se_Finaliza_Etapa(nr_seq_etapa_w,nr_seq_etapa)	= 'S'
	and	dt_fim_etapa is null;
elsif	(ie_finalizar_etapa_w		= 'S') then
	update	conta_paciente_etapa a
	set	dt_fim_etapa		= dt_etapa_w
	where	nr_interno_conta	= nr_interno_conta_w
	and	nr_sequencia		<> nr_seq_etapa_conta_p
	and	Obter_Se_Finaliza_Etapa(nr_seq_etapa_w,nr_seq_etapa)	= 'S'
	and	cd_setor_atendimento	= cd_setor_atendimento_w
	and	dt_fim_etapa is null;
end if;


if	(ie_atualiza_final_etapa_w	= 'S') then
	update	conta_paciente_etapa
	set	dt_fim_etapa		= dt_etapa_w
	where	nr_sequencia		= nr_seq_etapa_conta_p
	and	dt_fim_etapa is null;
end if;


if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Atualiza_Final_Etapa_Conta;
/
