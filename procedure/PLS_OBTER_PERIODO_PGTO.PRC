create or replace
procedure pls_obter_periodo_pgto
			(	nr_seq_prestador_p		Number,
				dt_mes_competencia_p		date,
				ie_tipo_guia_p			varchar2,
				nr_seq_regra_p 		out	Number, 
				nr_seq_periodo_p 	out	Number) is

nr_seq_classificacao_w		Number(10);
nr_seq_regra_w 			Number(10);
nr_seq_periodo_w		Number(10);
nr_dia_limite_w			Number(10);

Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_periodo_pgto
	from	pls_conta_integracao_pgto	a
	where	a.ie_situacao		= 'A'
	and	((nvl(a.nr_seq_classificacao,0) = 0) or (nvl(a.nr_seq_classificacao,0) = nr_seq_classificacao_w))
	and	((nvl(to_char(a.dt_referencia),'X') = 'X') or (trunc(dt_mes_competencia_p) <= trunc(a.dt_referencia)))
	and	((nvl(ie_tipo_guia,'X') = 'X') or (nvl(ie_tipo_guia,'X') = ie_tipo_guia_p))
	order by a.dt_referencia desc,
		 nvl(a.nr_seq_classificacao,0),
		 nvl(ie_tipo_guia,'X');

begin
nr_seq_classificacao_w := pls_obter_dados_prestador(nr_seq_prestador_p, 'NRCLA');

open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	nr_seq_periodo_w;
exit when C01%notfound;	
end loop;
close C01;

nr_seq_regra_p 		:= nr_seq_regra_w;
nr_seq_periodo_p	:= nr_seq_periodo_w;

end pls_obter_periodo_pgto;
/
