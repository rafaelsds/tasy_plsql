CREATE OR REPLACE
PROCEDURE Calcular_ConPaci_Desc_Item(nr_sequencia_p	in number,
					       nm_usuario_p		varchar2) IS

nr_interno_conta_w	number(10);

vl_desconto_w		number(15,2);
vl_liquido_w		number(15,2);
vl_conta_w			number(15,2);

vl_total_item_w		number(15,2);

pr_desconto_w		number(5,2);

nr_sequencia_w		number(10);

vl_liq_item_w		number(15,2);
vl_desc_item_w		number(15,2);

pr_desc_estrut_w	number(15,2);

cd_estrutura_w		number(5);
cd_convenio_w		number(05,0);
qt_regra_estrut_w	number(10,0);

ie_ok_w			integer;

ie_pacote_w		varchar2(1); /* Incluido por Fabricio em 02/10/2008 OS 99776*/
ie_tipo_desconto_w		number(2);

qt_regra_desc_item_w	number(10);

cursor C01 is
	select 	nr_sequencia,
		cd_estrutura
	from conta_paciente_desc_item
	where nr_seq_desconto = nr_sequencia_p
	  and ie_calcula = 'S'
	order by vl_conta;

BEGIN

select nr_interno_conta
into nr_interno_conta_w
from conta_paciente_desconto
where nr_sequencia = nr_sequencia_p;

select	cd_convenio_parametro
into	cd_convenio_w
from	conta_paciente
where 	nr_interno_conta	= nr_interno_conta_w;

select count(*)
into  ie_ok_w
from  conta_paciente_v
where nr_interno_conta = nr_interno_conta_w
and   cd_motivo_exc_conta is null
and   nvl(ie_emite_conta_honor, ie_emite_conta) is null;

if (ie_ok_w > 0) then
	-- Existem itens sem estrutura. Calculo cancelado !
	Wheb_mensagem_pck.exibir_mensagem_abort(243530);
end if;

select 	vl_desconto,
	vl_liquido,
	vl_conta,
	nvl(ie_pacote,'A'),
	ie_tipo_desconto
into	vl_desconto_w,
	vl_liquido_w,
	vl_conta_w,
	ie_pacote_w,
	ie_tipo_desconto_w
from conta_paciente_desconto
where nr_sequencia = nr_sequencia_p;

select sum(vl_conta)
into vl_total_item_w
from conta_paciente_desc_item
where nr_seq_desconto = nr_sequencia_p
  and ie_calcula = 'S';

if (vl_total_item_w < vl_desconto_w) then
	-- Valor desconto maior que valor dos itens. Desconto cancelado !
	Wheb_mensagem_pck.exibir_mensagem_abort(243532);
end if;

pr_desconto_w := (vl_desconto_w * 100) / vl_total_item_w;
 
update conta_paciente_desc_item
set	pr_desconto = 0,
	vl_desconto = 0,
	vl_liquido  = vl_conta
where nr_seq_desconto = nr_sequencia_p
  and ie_calcula = 'N';

open C01;
loop
	fetch C01 into 
		nr_sequencia_w,
		cd_estrutura_w;
	exit when C01%notfound;

	update conta_paciente_desc_item
	set pr_desconto = pr_desconto_w,
	    vl_desconto = (vl_conta /100) * pr_desconto_w,
	    vl_liquido  = vl_conta - ((vl_conta /100) * pr_desconto_w)
	where nr_sequencia = nr_sequencia_w;

	if	(pr_desconto_w = 0) then
		select	nvl(max(PR_DESCONTO), 0)
		into	pr_desc_estrut_w
		from	CONVENIO_REGRA_DESC_ESTRUT
		where	cd_convenio	= cd_convenio_w
		and	cd_estrutura	= cd_estrutura_w;

	update 	conta_paciente_desc_item
	set 	pr_desconto 	= pr_desc_estrut_w,
		vl_desconto	= (vl_conta /100) * pr_desc_estrut_w,
		vl_liquido 	= vl_conta - ((vl_conta /100) * pr_desc_estrut_w)
	where	nr_sequencia 	= nr_sequencia_w;

	end if;

end loop;
close C01;

select sum(vl_liquido),
	 sum(vl_desconto)
into	vl_liq_item_w,
	vl_desc_item_w
from conta_paciente_desc_item
where nr_seq_desconto = nr_sequencia_p;

select	count(*)
into	qt_regra_estrut_w
from	CONVENIO_REGRA_DESC_ESTRUT
where	cd_convenio	= cd_convenio_w
and	cd_estrutura	= cd_estrutura_w
and	pr_desconto is not null
and	pr_desconto_w = 0;

if	(qt_regra_estrut_w = 0) and (ie_pacote_w = 'A') and (ie_tipo_desconto_w <> 7) and 
	((vl_liquido_w  <> vl_liq_item_w) or
	 (vl_desconto_w <> vl_desc_item_w)) then
	update conta_paciente_desc_item
	set	vl_desconto = vl_desconto + (vl_desconto_w - vl_desc_item_w),
		vl_liquido  = vl_liquido  + (vl_liquido_w - vl_liq_item_w)
	where nr_sequencia = nr_sequencia_w;
end if;

select	count(*)
into	qt_regra_desc_item_w
from	conv_regra_desc a,
	conv_regra_desc_item b
where	a.nr_sequencia = b.nr_seq_regra_desc
and	a.cd_convenio = cd_convenio_w;

if	(qt_regra_desc_item_w > 0) then	
	Gerar_Conv_Regra_Desc(nr_sequencia_p,nm_usuario_p);	
end if;

commit;

END Calcular_ConPaci_Desc_Item;
/
