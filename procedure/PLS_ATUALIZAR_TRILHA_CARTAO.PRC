create or replace
procedure pls_atualizar_trilha_cartao
			(	nr_seq_segurado_p	number,
				nm_usuario_p		Varchar2) is 

ds_trilha1_w		pls_segurado_carteira.ds_trilha1%type;
ds_trilha2_w		pls_segurado_carteira.ds_trilha2%type;
ds_trilha3_w		pls_segurado_carteira.ds_trilha3%type;
ds_trilha_qr_code_w	pls_segurado_carteira.ds_trilha_qr_code%type;

begin
pls_obter_trilhas_cartao(nr_seq_segurado_p, ds_trilha1_w, ds_trilha2_w, ds_trilha3_w,ds_trilha_qr_code_w, nm_usuario_p);

update	pls_segurado_carteira
set	ds_trilha1    	      = ds_trilha1_w,
	ds_trilha2    	      = ds_trilha2_w,
	ds_trilha3    	      = ds_trilha3_w,
	ds_trilha_qr_code     = ds_trilha_qr_code_w
where	nr_seq_segurado	= nr_seq_segurado_p;

end pls_atualizar_trilha_cartao;
/
