create or replace
procedure integrar_matrix_ws(	nr_seq_evento_p		number,
								nr_prescricao_p		number,
								nr_seq_prescr_p		number,
								nr_seq_exame_p		number,
								nm_usuario_p		varchar2,
								ie_commit_p			varchar2 default 'N') is 
		
ds_sep_bv_w					VARCHAR2(100);
ds_param_integr_w			varchar2(4000);
ds_log_w					varchar2(2000);
ie_envia_msg_w				varchar2(1);
		
recebePedido_w				number(3) := 249;
recebeConfirmacaoColeta_w	number(3) := 252;
cancelaPedido_w				number(3) := 414;
		
begin

ds_sep_bv_w := ';';

if	(nr_Seq_evento_p in(	recebePedido_w,
							recebeConfirmacaoColeta_w,
							cancelaPedido_w
							)) then

	if	(nr_seq_evento_p = recebePedido_w) then
	
		select	decode(count(*),0,'N','S')
		into	ie_envia_msg_w
		from	matrix_procedimento_ws_v a
		where	a.numero_atendimento = nr_prescricao_p;
		
		if	(ie_envia_msg_w = 'S') then
			ds_param_integr_w	:=	'NR_PRESCRICAO=' || to_char(nr_prescricao_p) || ds_sep_bv_w ||
									'NR_SEQUENCIA=0' || ds_sep_bv_w;	
		end if;
		
		ds_log_w				:=	'Matrix Diagnosis - NR_SEQ_EVENTO_P: '||to_char(nr_seq_evento_p)||
									' - NR_PRESCRICAO_P: '|| to_char(nr_prescricao_p)||
									' - IE_ENVIA_MSG_W: '|| ie_envia_msg_w;
		
	elsif	(nr_seq_evento_p = recebeConfirmacaoColeta_w) then
		
		select	decode(count(*), 0, 'N', 'S')
		into	ie_envia_msg_w
		from	lab_exame_equip a,
				equipamento_lab b
		where	a.cd_equipamento = b.cd_equipamento
		AND		b.ds_sigla = 'MATRIX'
		and		a.nr_seq_exame	= nr_seq_exame_p;
		
		if	(ie_envia_msg_w = 'S') then
			ds_param_integr_w 	:= 	'NR_PRESCRICAO=' || to_char(nr_prescricao_p) || ds_sep_bv_w ||
									'NR_SEQUENCIA=' || to_char(nr_seq_prescr_p) || ds_sep_bv_w;		
		end if;
		
		ds_log_w				:= 	'Matrix Diagnosis - NR_SEQ_EVENTO_P: '||to_char(nr_seq_evento_p)||' - NR_PRESCRICAO_P: '||nr_prescricao_p||' - NR_SEQUENCIA:'||to_char(nr_seq_prescr_p) || ' - IE_ENVIA_MSG_W: ' || ie_envia_msg_w;

	elsif	(nr_seq_evento_p = cancelaPedido_w) then
	
		/*select	decode(count(*),0,'N','S')
		into	ie_envia_msg_w
		from	matrix_procedimento_ws_v a
		where	a.numero_atendimento = nr_prescricao_p
		and		a.sequencia_exame = nr_seq_prescr_p
		and		nvl(a.ie_suspenso,'N') <> 'S';*/
		select	decode(count(*),0,'N','S')
		into	ie_envia_msg_w
		from	lab_exame_equip a,
				equipamento_lab b
		where	a.nr_seq_exame = nr_seq_exame_p
		and 	a.cd_equipamento = b.cd_equipamento
		and 	b.ds_sigla = 'MATRIX';
		
		if	(ie_envia_msg_w = 'S') then
			ds_param_integr_w	:=	'NR_PRESCRICAO=' || to_char(nr_prescricao_p) || ds_sep_bv_w ||
									'NR_SEQUENCIA=' || to_char(nvl(nr_seq_prescr_p,0)) || ds_sep_bv_w;
		end if;
		
		ds_log_w				:=	'Matrix Diagnosis - NR_SEQ_EVENTO_P: '||to_char(nr_seq_evento_p)||
									' - NR_PRESCRICAO_P: '|| to_char(nr_prescricao_p)||
									' - NR_SEQUENCIA:'||to_char(nr_seq_prescr_p) ||
									' - IE_ENVIA_MSG_W: '|| ie_envia_msg_w;
	
	else
	
		ds_log_w			:= 	'Matrix Diagnosis - Evento n�o informado';

	end if;
	
	gravar_log_lab(66, ds_log_w, nm_usuario_p);
	
	if	(ds_param_integr_w is not null) then		
		gravar_agend_integracao(nr_seq_evento_p, ds_param_integr_w);	
	end if;
	
	if (ie_commit_p = 'S') then
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	end if;
							
end if;

end integrar_matrix_ws;
/
