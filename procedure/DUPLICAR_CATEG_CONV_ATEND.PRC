create Or replace
PROCEDURE duplicar_categ_conv_atend(	nr_Atendimento_p   	NUMBER,
					nr_seq_interno_p   	NUMBER,
					nm_usuario_p    	VARCHAR2,
					nr_Seq_interno_novo_p 	OUT  NUMBER) IS

nr_Seq_interno_w NUMBER(10);

BEGIN

BEGIN
	SELECT 	atend_Categoria_convenio_seq.NEXTVAL
	INTO 	nr_Seq_interno_w
	FROM  	dual;

INSERT INTO atend_Categoria_convenio
	(nr_seq_interno, nr_atendimento, cd_convenio, cd_categoria, dt_inicio_vigencia, dt_final_vigencia, dt_atualizacao, cd_usuario_convenio,
	cd_empresa, nm_usuario, nr_doc_convenio, cd_tipo_acomodacao, cd_municipio_convenio, cd_convenio_glosa, cd_categoria_glosa, dt_validade_carteira,
	nr_acompanhante, cd_plano_convenio, cd_dependente, nr_seq_origem, cd_senha, ie_tipo_guia, ds_observacao, qt_dia_internacao, dt_ultimo_pagto,
	cd_complemento, dt_aceite_dif_acomod, nm_usuario_aceite, ds_observacao_aceite, qt_dieta_acomp, ie_lib_dieta, nr_doc_conv_principal,
	ie_regime_internacao, cd_usuario_conv_glosa, cd_complemento_glosa, dt_validade_cart_glosa, nm_usuario_original, nr_seq_lib_dieta_conv,
	nr_seq_tipo_lib_guia, nr_seq_cobertura, nr_seq_abrangencia, cd_senha_provisoria, cd_plano_glosa, nr_seq_regra_acomp, ds_tarja_cartao,
	ds_just_alteracao, ie_cod_usuario_mae_resp, nr_biometria_conv, nr_seq_just_biometria_conv)
SELECT  nr_seq_interno_w, nr_atendimento, cd_convenio, cd_categoria, SYSDATE, NULL, SYSDATE, cd_usuario_convenio,
	cd_empresa, nm_usuario_p, nr_doc_convenio, cd_tipo_acomodacao, cd_municipio_convenio, cd_convenio_glosa, cd_categoria_glosa, dt_validade_carteira,
	nr_acompanhante, cd_plano_convenio, cd_dependente, nr_seq_origem, cd_senha, ie_tipo_guia, ds_observacao, qt_dia_internacao, dt_ultimo_pagto,
	cd_complemento, dt_aceite_dif_acomod, nm_usuario_aceite, ds_observacao_aceite, qt_dieta_acomp, ie_lib_dieta, nr_doc_conv_principal,
	ie_regime_internacao, cd_usuario_conv_glosa, cd_complemento_glosa, dt_validade_cart_glosa, nm_usuario_original, nr_seq_lib_dieta_conv,
	nr_seq_tipo_lib_guia, nr_seq_cobertura, nr_seq_abrangencia, cd_senha_provisoria, cd_plano_glosa, nr_seq_regra_acomp, ds_tarja_cartao,
	ds_just_alteracao, ie_cod_usuario_mae_resp, nr_biometria_conv, nr_seq_just_biometria_conv
FROM  	atend_categoria_Convenio
WHERE 	nr_atendimento = nr_atendimento_p
AND  	nr_seq_interno = nr_seq_interno_p;
EXCEPTION
WHEN OTHERS THEN
	nr_Seq_interno_w := 0;
END;
nr_Seq_interno_novo_p := nr_Seq_interno_w;
COMMIT;

END duplicar_categ_conv_atend;
/
