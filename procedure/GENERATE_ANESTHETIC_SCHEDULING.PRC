create or replace
procedure generate_anesthetic_scheduling
			(	nr_seq_agenda_anestesista_p	number,
				cd_especialidade_p		varchar2,
				ie_tipo_agendamento_p		varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2,
				nr_seq_ageint out number,
				nr_seq_ageint_item out number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finality:
-------------------------------------------------------------------------------------------------------------------
Direct  calling 
[  ]  Dictionary objects [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Others:
 ------------------------------------------------------------------------------------------------------------------
Attention points:
-------------------------------------------------------------------------------------------------------------------
References:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_ageint_w		number(10,0);
nr_seq_ageint_item_w		number(10,0);
nr_telefone_w		varchar2(60);
qt_altura_cm_w		number(5,2);
dt_nascimento_w		date;
nm_paciente_w		varchar2(60);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_w		varchar2(10);
nr_seq_status_w		number(10,0);
nr_duracao_w		number(10);

begin

select	max(a.cd_pessoa_fisica),
	max(a.cd_medico),
	max(a.nr_minuto_duracao)
into	cd_pessoa_fisica_w,
	cd_medico_w,
	nr_duracao_w
from	agenda_paciente	a
where	a.nr_sequencia	= nr_seq_agenda_anestesista_p;

nr_telefone_w		:= obter_fone_pac_agenda(cd_pessoa_fisica_w);
dt_nascimento_w		:= to_date(obter_dados_pf(cd_pessoa_fisica_w,'DN'),'dd/mm/yyyy');
qt_altura_cm_w		:= obter_dados_pf(cd_pessoa_fisica_w,'AL');
nm_paciente_w		:= substr(obter_nome_pf(cd_pessoa_fisica_w),1,60);


select	min(nr_sequencia) 
into	nr_seq_status_w
from 	agenda_integrada_status 
where 	ie_situacao = 'A' 
and 	ie_Status_tasy = 'EA';

select	agenda_integrada_seq.nextval
into	nr_seq_ageint_w
from	dual;

nr_seq_ageint := nr_seq_ageint_w;

insert into agenda_integrada
	(	nr_sequencia,
		cd_pessoa_fisica,
		nm_paciente,
		nr_telefone,
		dt_nascimento,
		qt_altura_cm,
		dt_inicio_agendamento,
		cd_profissional,
		nr_seq_status,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_turno)
values	(	nr_seq_ageint_w,
		cd_pessoa_fisica_w,
		nm_paciente_w,
		nr_telefone_w,
		dt_nascimento_w,
		qt_altura_cm_w,
		sysdate,
		cd_medico_w,
		nr_seq_status_w,
		cd_estabelecimento_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'2');
		
select	agenda_integrada_item_seq.nextval
into	nr_seq_ageint_item_w
from	dual;

nr_seq_ageint_item := nr_seq_ageint_item_w;
		
insert into agenda_integrada_item(
	nr_sequencia,
	cd_especialidade,
	ie_tipo_agendamento,
	nr_minuto_duracao,
	nr_seq_agenda_int,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_agenda_anestesista)
values	(agenda_integrada_item_seq.nextval,
	cd_especialidade_p,
	ie_tipo_agendamento_p,					
	nr_duracao_w,
	nr_seq_ageint_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_agenda_anestesista_p);

commit;

end generate_anesthetic_scheduling;
/
