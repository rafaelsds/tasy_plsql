create or replace
procedure atualiza_result_san_exame_js(	vl_resultado_p		number,
                                        ds_resultado_p		varchar2,
                                        nr_seq_exame_p		number,
					nr_seq_exame_lote_p	number,
					ds_descricao_p		varchar2,
					nm_usuario_p		Varchar2) is 
				
begin

if	(vl_resultado_p > 0) then

	update	san_exame_realizado
	set   	ds_resultado   		= to_char(ds_resultado_p),
		nm_usuario     		= nm_usuario_p,
		vl_resultado   		= vl_resultado_p, 
		dt_atualizacao 		= sysdate 
	where  	nr_seq_exame      	= nr_seq_exame_p
	and    	nr_seq_exame_lote 	= nr_seq_exame_lote_p;
	
end if;

if	((ds_descricao_p <> '') or
	(ds_descricao_p is not null)) then
	
	update	san_exame_realizado
        set   	ds_resultado   		= ds_descricao_p, 
                nm_usuario     		= nm_usuario_p, 
                dt_atualizacao 		= sysdate 
	where	nr_seq_exame      	= nr_seq_exame_p
        and    	nr_seq_exame_lote 	= nr_seq_exame_lote_p;

end if;
                                        
commit;

end atualiza_result_san_exame_js;
/