create or replace 
PROCEDURE Gerar_EIS_Nascimento( dt_parametro_P   	date,
								nm_usuario_p      	varchar2) IS

dt_parametro_w			date;
dt_parametro_fim_w		date;
nr_sequencia_w			number(10);

BEGIN


Gravar_Log_Indicador(111, obter_desc_expressao(293972)/*'Nascimento'*/, sysdate, trunc(dt_parametro_p), nm_usuario_p, nr_sequencia_w);


commit;

dt_parametro_w		:= PKG_DATE_UTILS.start_of(dt_parametro_p,'month', 0);
dt_parametro_fim_w	:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_parametro_p, 'MONTH', 0), 1,0) - (1/86400);

delete from eis_Nascimento
where dt_nascimento between  dt_parametro_w and  dt_parametro_fim_w;

commit;

insert into Eis_Nascimento(
	cd_estabelecimento,
	dt_nascimento,
	dt_atualizacao,
	nm_usuario,
	cd_medico,
	cd_pediatra,
	cd_convenio,
	ie_tipo_parto,
	ie_sexo,
	qt_nasc_vivo,
	qt_nasc_morto,
	qt_gemelar,
	qt_total,
	cd_municipio_ibge,
	ie_tipo_nascimento,
	CD_PROCEDENCIA,
	IE_FAIXA_ETARIA,
	qt_sem_ig,
	QT_MORTE_MATERNA,
	ie_acompanhante,
	ie_risco_gravidez,
	qt_sem_ig_cronologica,
	cd_setor_Atendimento,
	IE_PARTO_NORMAL,
	IE_PARTO_EPISIO ,       
	IE_PARTO_CESARIA,
	IE_INFECCAO,
	QT_PESO_SALA_PARTO,
	DT_OBITO,
	QT_ALTA,
	qt_peso,
	qt_gestacoes_previas,
	qt_apgar_prim_min,
	qt_apgar_5_min,
	CD_PESSOA_FISICA)
select
	a.cd_estabelecimento,
	PKG_DATE_UTILS.start_of(n.dt_nascimento,'dd', 0),
	sysdate,
	nm_usuario_p,
	p.cd_medico,
	n.cd_pediatra,
	obter_convenio_atendimento(n.nr_atendimento),
	decode(nvl(p.ie_parto_normal,'N'),'S','1',decode(p.IE_PARTO_EPISIO,'S','1','2')),
	nvl(n.ie_sexo,'I'),
  	sum(decode(n.ie_unico_nasc_vivo, 'S', 1, 0)),
  	sum(decode(n.ie_unico_nasc_vivo, 'N', 1, 0)),
	sum(decode(nvl(p.qt_nasc_vivos,0) + nvl(p.qt_nasc_mortos,0),0,0,1,0,decode(n.ie_unico_nasc_vivo, 'S', 1, 0) +
		decode(n.ie_unico_nasc_vivo, 'N', 1, 0))),
	sum(1),
	obter_compl_pf(obter_pessoa_atendimento(p.nr_atendimento,'C'),1,'CDM'),
	n.ie_tipo_nascimento,
	a.CD_PROCEDENCIA,
	trim(substr(obter_idade_pf(a.cd_pessoa_fisica,n.dt_nascimento,'E'),1,50)),
	n.qt_sem_ig,
	sum(decode(obter_se_motivo_alta_obito(a.cd_motivo_alta),'S',1,0)),
	nvl(ie_acompanhante,'N'),
	eis_obter_risco_gravidez(n.nr_atendimento),
	p.qt_sem_ig_cronologica,
	obter_setor_atendimento(a.nr_atendimento),
	nvl(p.IE_PARTO_NORMAL,'N'),
	nvl(p.IE_PARTO_EPISIO,'N'),
	nvl(p.IE_PARTO_CESARIA,'N'),
	nvl(OBTER_SE_INFEC_ATEND(a.nr_atendimento),'N'),
	QT_PESO_SALA_PARTO,
	n.DT_OBITO,
	sum(decode(dt_alta,null,1,0)),
	n.qt_peso,
	p.qt_gestacoes,
	n.QT_APGAR_PRIM_MIN,
	n.QT_APGAR_QUINTO_MIN,
	a.CD_PESSOA_FISICA
from	atendimento_paciente a,
	parto p,
	nascimento n
where	n.dt_nascimento between dt_parametro_w and dt_parametro_fim_w
and	n.nr_atendimento	= p.nr_atendimento
and	a.nr_atendimento	= p.nr_atendimento
and	nvl(p.ie_gera_eis,'S')	= 'S'
group by
	a.cd_estabelecimento,
	PKG_DATE_UTILS.start_of(n.dt_nascimento,'dd',0),
	p.cd_medico,
	n.cd_pediatra,
	obter_convenio_atendimento(n.nr_atendimento),
	decode(nvl(p.ie_parto_normal,'N'),'S','1',decode(p.IE_PARTO_EPISIO,'S','1','2')),
	nvl(n.ie_sexo,'I'),
	obter_compl_pf(obter_pessoa_atendimento(p.nr_atendimento,'C'),1,'CDM'),
	n.ie_tipo_nascimento,
	a.CD_PROCEDENCIA,
	substr(obter_idade_pf(a.cd_pessoa_fisica,n.dt_nascimento,'E'),1,50),
	n.qt_sem_ig,
	nvl(ie_acompanhante,'N'),
	eis_obter_risco_gravidez(n.nr_atendimento),
	p.qt_sem_ig_cronologica,
	obter_setor_atendimento(a.nr_atendimento),
	nvl(p.IE_PARTO_NORMAL,'N'),
	nvl(p.IE_PARTO_EPISIO,'N'),
	nvl(p.IE_PARTO_CESARIA,'N'),
	nvl(OBTER_SE_INFEC_ATEND(a.nr_atendimento),'N'),
	QT_PESO_SALA_PARTO,
	n.DT_OBITO,
	n.qt_peso,
	p.qt_gestacoes,
	n.QT_APGAR_PRIM_MIN,
	n.QT_APGAR_QUINTO_MIN,
	a.CD_PESSOA_FISICA;





Atualizar_Log_Indicador(sysdate, nr_sequencia_w);
COMMIT;

END Gerar_EIS_Nascimento;
/
