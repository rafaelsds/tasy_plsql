create or replace procedure update_valor_tabela(ds_chaves_primarias_p varchar2, ds_tabela_p varchar2, ds_coluna_p varchar2, ds_valor_p varchar2, ds_coluna_chave_p varchar2) is

begin
  execute immediate 'update ' || ds_tabela_p || ' set ' || ds_coluna_p || ' = ' 
                    || ds_valor_p || ' where ' || ds_coluna_chave_p || ' in (' || ds_chaves_primarias_p || ')';
  commit;
end update_valor_tabela;
/