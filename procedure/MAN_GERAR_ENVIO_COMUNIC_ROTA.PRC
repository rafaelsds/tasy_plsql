create or replace
procedure man_gerar_envio_comunic_rota
			(	nr_sequencia_p		number,
				ie_evento_p		varchar2,
				nm_usuario_p		Varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar comunica��o interna de a��o sobre a rota de inspe��o da ordem de servi�o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/*campos regra*/
nr_seq_regra_w			number(10,0);
ds_titulo_w			varchar2(255);
ds_comunicacao_w			varchar2(32000);
ie_comunic_interna_w		varchar2(01);
ie_email_w			varchar2(01);
qt_existe_w			number(10,0);
nr_seq_classif_w			number(10,0);
ie_usuario_w			varchar2(15);
ds_usuario_destino_w		varchar2(2000);
ds_lista_email_destino_w		varchar2(2000);
ds_lista_email_oculta_w		varchar2(2000);
ds_email_origem_w			varchar2(255);
cd_perfil_w			number(05,0) := obter_perfil_ativo;
ds_usuario_desconsid_regra_w	varchar2(2000);
ie_fora_horario_manut_w		varchar2(15);
dt_hr_inicio_w			date;
dt_hr_fim_w			date;
ds_usuario_email_w			varchar2(15);
ds_plano_inspecao_w		varchar2(255);

/*Campos CI*/
nm_usuario_w			varchar2(2000);
nm_usuario_exec_w 		varchar2(15);

/*Campos EMAIL*/
lista_usuario_w			varchar2(2000);
tam_lista_w			number(10,0);
ie_pos_virgula_w			number(3,0);
ds_email_usuario_w			varchar2(255);
ds_lista_email_usuario_w		varchar2(2000);

ie_gerar_w			varchar2(01)	:= 'S';

ie_marcar_ci_lida_remetente_w	man_regra_envio_comunic.ie_marcar_ci_lida_remetente%type;
nr_seq_comunic_w			comunic_interna.nr_sequencia%type;
ie_lista_destino_oculta_w		man_regra_envio_comunic.ie_lista_destino_oculta%type;

cursor c01 is
	select	nr_sequencia,
		ds_titulo,
		ds_comunicacao,
		ie_comunicacao_interna,
		ie_email,
		ds_lista_email_destino,
		ds_email_origem,
		ds_usuario_desconsid_regra,
		ie_fora_horario_manut,
		decode(nvl(ie_param_cliente_email_origem,'N'),'S',null,nm_usuario_p),
		nvl(ie_marcar_ci_lida_remetente, 'N'),
		nvl(ie_lista_destino_oculta,'N')
	from	man_regra_envio_comunic
	where	ie_evento = ie_evento_p
	and	nvl(ie_situacao,'A') = 'A';

cursor c02 is
	select	ie_usuario,
		ds_usuario_destino
	from	man_usuarios_comunicacao
	where	nr_seq_regra = nr_seq_regra_w;

cursor c03 is
	select	nm_usuario_exec
	from	man_rota_insp_exec
	where	nr_seq_rota_inspecao = nr_sequencia_p
	and	nm_usuario_exec is not null;

	
begin
select	count(*)
into	qt_existe_w
from	man_regra_envio_comunic
where	ie_evento = ie_evento_p
and	nvl(ie_situacao,'A') = 'A';

if	(nr_sequencia_p > 0) then
	select	a.ds_plano_inspecao
	into	ds_plano_inspecao_w
	from	man_plano_inspecao a,
		man_rota_inspecao b
	where	b.nr_seq_plano_inspecao	= a.nr_sequencia
	and	b.nr_sequencia 		= nr_sequencia_p;
end if;

if	(qt_existe_w > 0) then
	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_regra_w,
		ds_titulo_w,
		ds_comunicacao_w,
		ie_comunic_interna_w,
		ie_email_w,
		ds_lista_email_destino_w,
		ds_email_origem_w,
		ds_usuario_desconsid_regra_w,
		ie_fora_horario_manut_w,
		ds_usuario_email_w,
		ie_marcar_ci_lida_remetente_w,
		ie_lista_destino_oculta_w;
	exit when C01%notfound;
		begin
		ie_gerar_w := 'S';

		if	(nvl(ie_gerar_w,'S') = 'S') then
			begin

			open C02;
			loop
			fetch C02 into
				ie_usuario_w,
				ds_usuario_destino_w;
			exit when C02%notfound;
				begin
				if	(ie_usuario_w = 'U') and
					(nvl(ds_usuario_destino_w,'X') <> 'X') then
					nm_usuario_w	:= substr(ds_usuario_destino_w || ', ' || nm_usuario_w,1,2000);
				
				elsif	(ie_usuario_w = 'E') then
					open C03;
					loop
					fetch C03 into
						nm_usuario_exec_w;
						exit when C03%notfound;
						begin
						if	(nvl(nm_usuario_exec_w,'X') <> 'X') then
							nm_usuario_w := substr(nm_usuario_exec_w || ', ' ||nm_usuario_w,1,2000);
						end if;
						end;
					end loop;
					close C03;
				end if;
				
				end;
			end loop;
			close C02;

			/*Macros*/
			ds_titulo_w		:= substr(replace_macro(ds_titulo_w, '@plano_inspecao', ds_plano_inspecao_w),1,255);
			ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@plano_inspecao', ds_plano_inspecao_w),1,32000);
			
			
			if	(ie_comunic_interna_w = 'S') then
				begin
				if	(nvl(nm_usuario_w,'X') <> 'X') then
					begin
					select	obter_classif_comunic('F')
					into	nr_seq_classif_w
					from	dual;
					
					select	comunic_interna_seq.nextval
					into	nr_seq_comunic_w
					from	dual;
					
					insert into comunic_interna
						(dt_comunicado,
						ds_titulo,
						ds_comunicado,
						nm_usuario,
						dt_atualizacao,
						ie_geral,
						nm_usuario_destino,
						cd_perfil,
						nr_sequencia,
						ie_gerencial,
						nr_seq_classif,
						ds_perfil_adicional,
						cd_setor_destino,
						cd_estab_destino,
						ds_setor_adicional,
						dt_liberacao,
						ds_grupo,
						nm_usuario_oculto)
					values	(sysdate,
						ds_titulo_w,
						ds_comunicacao_w,
						nm_usuario_p,
						sysdate,
						'N',
						nm_usuario_w,
						null,
						nr_seq_comunic_w,
						'N',
						nr_seq_classif_w,
						'',
						null,
						'',
						'',
						sysdate,
						'',
						'');
						
					if	(ie_marcar_ci_lida_remetente_w = 'S') then
						insert into comunic_interna_lida
							(nr_sequencia,
							nm_usuario,
							dt_atualizacao)
						values	(nr_seq_comunic_w,
							nm_usuario_p,
							sysdate);
					end if;
					end;
				end if;
				end;
			end if;

			if	(ie_email_w = 'S') then
				begin
				if	(nvl(ds_usuario_email_w,'X') = 'X') and
					(nvl(ds_email_origem_w,'X') = 'X') then
					begin
					wheb_mensagem_pck.exibir_mensagem_abort(249815,'DS_REGRA=' || substr('(' || nr_seq_regra_w || ') - ' || obter_valor_dominio(3420,ie_evento_p),1,255));
					end;
				end if;
				
				lista_usuario_w := substr(nm_usuario_w,1,2000);
				while	(lista_usuario_w is not null) and
					(trim(lista_usuario_w) <> ',') loop
					tam_lista_w	:= length(lista_usuario_w);
					ie_pos_virgula_w	:= instr(lista_usuario_w,',');

					if	(ie_pos_virgula_w <> 0) then
						nm_usuario_w	:= substr(lista_usuario_w,1,(ie_pos_virgula_w - 1));
						lista_usuario_w	:= trim(substr(lista_usuario_w,(ie_pos_virgula_w + 1), tam_lista_w));

						select	max(ds_email)
						into	ds_email_usuario_w
						from	usuario
						where	nm_usuario	= nm_usuario_w;

						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							begin
							ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
							end;
						end if;
					else
						lista_usuario_w	:= null;
					end if;
				end loop;

				if	(nvl(ds_lista_email_destino_w,'X') <> 'X') then
					begin
					if	(nvl(ie_lista_destino_oculta_w,'N') = 'N') then
						ds_lista_email_usuario_w	:= substr(ds_lista_email_destino_w || ',' || ds_lista_email_usuario_w,1,2000);
					else
						ds_lista_email_oculta_w		:= substr(ds_lista_email_destino_w,1,2000);
					end if;
					end;
				end if;
				
				if	(nvl(ds_lista_email_usuario_w,'X') <> 'X') then
					ds_lista_email_usuario_w	:= replace(ds_lista_email_usuario_w,',',';');
					ds_lista_email_oculta_w 	:= replace(ds_lista_email_oculta_w,',',';');

					enviar_email(	ds_titulo_w,
							ds_comunicacao_w,
							ds_email_origem_w,
							ds_lista_email_usuario_w,
							ds_usuario_email_w,
							'M',
							ds_lista_email_oculta_w);
				end if;
				end;
			end if;
			end;
		end if;
		end;
	end loop;
	close C01;
	end;
end if;

commit;

end man_gerar_envio_comunic_rota;
/