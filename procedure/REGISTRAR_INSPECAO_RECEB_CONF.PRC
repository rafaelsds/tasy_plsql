create or replace
procedure registrar_inspecao_receb_conf(
			nr_ordem_compra_p	number,
			nr_item_oci_p		number,
			nr_seq_conf_p		number,
			qt_inspecao_p		number,
			dt_inicio_conferencia_p	date,
			nm_usuario_p		Varchar2) is

begin

update	ordem_compra_item_conf
set	qt_conferencia = qt_inspecao_p,
	dt_inicio_conferencia = dt_inicio_conferencia_p,
	dt_fim_conferencia = sysdate,
	nm_usuario_conferencia = nm_usuario_p
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p
and	nr_sequencia = nr_seq_conf_p;

commit;

end registrar_inspecao_receb_conf;
/
