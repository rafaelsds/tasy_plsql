create or replace
procedure atualizar_atend_lista_esp(	nr_atendimento_p	number,
					nr_sequencia_p		number,
					ie_excluir_paciente_p	varchar2) is 

ie_alterar_w	varchar2(1);

begin

select 	nvl(max('S'),'N') 
into	ie_alterar_w
from 	atendimento_paciente 
where 	nr_atendimento = nr_atendimento_p;


if	(ie_alterar_w = 'S') then

	update 	paciente_espera
	set 	nr_atendimento = nr_atendimento_p
	where 	nr_sequencia = nr_sequencia_p
	and 	nr_atendimento is null;
	commit;
end if;

if	(ie_excluir_paciente_p = 'S') then
	delete 	paciente_espera
	where 	nr_sequencia = nr_sequencia_p;
	commit;
end if;

end atualizar_atend_lista_esp;
/