create or replace
procedure GSJS_IMP_TITULO_RECEBER_LIQ	(cd_estabelecimento_p 	number,
					nm_usuario_p		varchar2) is

nr_nota_fiscal_w	varchar2(20);
ds_dt_credito_w		varchar2(10);
dt_credito_w		date;
ds_vl_total_pago_w	varchar2(13);
vl_total_pago_w		number(15,2);
ds_tipo_pagto_w		varchar2(3);
ds_status_w		varchar(1);

cd_tipo_recebimento_w	tipo_recebimento.cd_tipo_recebimento%type;
cd_tipo_receb_perda_w	tipo_recebimento.cd_tipo_recebimento%type;
nr_titulo_w		titulo_receber.nr_titulo%type;

ds_vl_juros_w		varchar2(13);
vl_juros_w		number(15,2);
sql_errm_w		varchar2(4000);

nr_seq_baixa_w		number(10);
cd_transacao_w		transacao_financeira.cd_transacao%type;
nr_seq_trans_financ_w	transacao_financeira.nr_sequencia%type;
nr_seq_conta_banco_w	banco_estabelecimento.nr_sequencia%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ds_outros_acres_w	varchar2(13);
vl_outros_acres_w	titulo_receber_liq.vl_outros_acrescimos%type;

cursor c01 is
select	ltrim(trim(substr(ds_conteudo,1,20)),'0') nr_nota_fiscal,
	substr(ds_conteudo,31,10) ds_dt_credito,
	substr(ds_conteudo,93,13) ds_vl_total_pago,
	ltrim(trim(substr(ds_conteudo,106,3)),'0') ds_tipo_pagto,
	substr(ds_conteudo,119,1) ds_status,
	substr(ds_conteudo,80,13) ds_vl_juros,
	ltrim(trim(substr(ds_conteudo,120,4)),'0') cd_transacao,
	substr(ds_conteudo,124,13) ds_outros_acres
from	w_titulo_mxm
where	substr(ds_conteudo,119,1)	<> 'C';

begin

select	max(a.cd_tipo_recebimento)
into	cd_tipo_recebimento_w
from	tipo_recebimento a
where	a.ie_tipo_consistencia	= 14;

select	max(a.cd_tipo_recebimento)
into	cd_tipo_receb_perda_w
from	tipo_recebimento a
where	a.ie_tipo_consistencia	= 9;

open	c01;
loop
fetch	c01 into
	nr_nota_fiscal_w,
	ds_dt_credito_w,
	ds_vl_total_pago_w,
	ds_tipo_pagto_w,
	ds_status_w,
	ds_vl_juros_w,
	cd_transacao_w,
	ds_outros_acres_w;
exit	when c01%notfound;

	if	(trim(ds_dt_credito_w) is not null) then
		begin
			dt_credito_w	:= to_date(ds_dt_credito_w,'dd/mm/yyyy');
		exception
		when others then
			delete	from w_titulo_mxm;
			commit;
			wheb_mensagem_pck.exibir_mensagem_abort(449902);
			--r.aise_application_error(-20011,'Data do credito invalida: ' || ds_dt_credito_w);
		end;
	end if;

	begin
		vl_total_pago_w	:= to_number(ds_vl_total_pago_w) / 100;
	exception
	when others then
		delete	from w_titulo_mxm;
		commit;
		wheb_mensagem_pck.exibir_mensagem_abort(105473);
		--r.aise_application_error(-20011,'Valor pago invalido: ' || ds_vl_total_pago_w);
	end;

	begin
		vl_juros_w	:= to_number(ds_vl_juros_w) / 100;
	exception
	when others then
		delete	from w_titulo_mxm;
		commit;
		wheb_mensagem_pck.exibir_mensagem_abort(105473);
		--r.aise_application_error(-20011,'Valor de juros invalido: ' || ds_vl_juros_w);
	end;
	
	begin
		vl_outros_acres_w	:= to_number(ds_outros_acres_w) / 100;
	exception
	when others then
		delete	from w_titulo_mxm;
		commit;
		wheb_mensagem_pck.exibir_mensagem_abort(105473);
		--r.aise_application_error(-20011,'Valor de outos acrescimos invalido: ' || ds_outros_acres_w);
	end;

	begin

	select	max(b.nr_titulo),
		max(b.cd_estabelecimento)
	into	nr_titulo_w,
		cd_estabelecimento_w
	from	titulo_receber b,
		nota_fiscal a
	where	a.nr_sequencia			= b.nr_seq_nf_saida
	and	ltrim(a.nr_nota_fiscal,'0')	= nr_nota_fiscal_w;

	select	max(a.nr_sequencia)
	into	nr_seq_trans_financ_w
	from	transacao_financeira a
	where	ltrim(trim(a.cd_transacao),'0')	= cd_transacao_w;

	if	(nr_titulo_w	is not null) then

		select	nvl(max(a.cd_tipo_recebimento),decode(ds_status_w,'D',nvl(cd_tipo_receb_perda_w,cd_tipo_recebimento_w),cd_tipo_recebimento_w)),
			nvl(max(a.nr_seq_trans_fin),nr_seq_trans_financ_w)
		into	cd_tipo_recebimento_w,
			nr_seq_trans_financ_w
		from	tipo_recebimento a
		where	a.cd_tipo_recebimento	= ds_tipo_pagto_w;

		select	max(a.nr_seq_conta_banco)
		into	nr_seq_conta_banco_w
		from	parametro_contas_receber a
		where	a.cd_estabelecimento	= cd_estabelecimento_w;

		baixa_titulo_receber(	cd_estabelecimento_p,
					cd_tipo_recebimento_w,
					nr_titulo_w,
					nr_seq_trans_financ_w,
					nvl(vl_total_pago_w,0) - nvl(vl_juros_w,0),
					dt_credito_w,
					nm_usuario_p,
					0,
					null,
					nr_seq_conta_banco_w,
					0,
					0);

		atualizar_saldo_tit_rec(nr_titulo_w,nm_usuario_p);
		
		select	max(a.nr_sequencia)
		into	nr_seq_baixa_w
		from	titulo_receber_liq a
		where	a.nr_titulo	= nr_titulo_w;
		
		update	titulo_receber_liq
		set	vl_juros	= nvl(vl_juros_w,0),
			vl_outros_acrescimos = vl_outros_acres_w
		where	nr_sequencia	= nr_seq_baixa_w
		and	nr_titulo	= nr_titulo_w;
		
		gerar_movto_tit_baixa(	nr_titulo_w, 
					nr_seq_baixa_w, 
					'R', 
					nm_usuario_p, 
					'N');

	end if;

	exception
	when others then
		delete	from w_titulo_mxm;
		commit;
		sql_errm_w	:= substr(sqlerrm,1,4000);
		raise_application_error(-20011,sql_errm_w);
	end;

end	loop;
close	c01;

delete	from w_titulo_mxm;

commit;

end GSJS_IMP_TITULO_RECEBER_LIQ;
/