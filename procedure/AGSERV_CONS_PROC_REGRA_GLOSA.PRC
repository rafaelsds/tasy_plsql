create or replace
procedure Agserv_cons_proc_regra_glosa(	nr_sequencia_p 		number,
					nr_seq_agenda_p		number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nr_seq_exame_p		number,
					nr_seq_proc_interno_p	number,					
					nm_usuario_p		Varchar2,
					cd_estabelecimento_p	number) is 

					

ds_erro_w			varchar2(255);
ie_regra_glosa_conv_w		varchar2(1);
ie_glosa_w			varchar2(1);
ie_regra_w			Number(5);
ie_edicao_w                  	varchar2(1);
ie_pacote_w			varchar2(1);
cd_edicao_ajuste_w      	number(10);
qt_item_edicao_w         	number(10);
nr_seq_regra_w			number(10);
cd_convenio_w			number(5);
ie_tipo_atendimento_w		number(3);
cd_plano_w			varchar2(10);
cd_setor_atendimento_w		number(10);
cd_categoria_w			varchar2(10);
ds_irrelevante_w		varchar2(255);
cd_paciente_w			varchar2(10);
pr_glosa_w				number(15,4);
vl_glosa_w				number(15,4);
begin

select	max(a.cd_convenio),
	max(a.ie_tipo_atendimento),
	max(a.cd_plano),
	max(a.cd_setor_atendimento),
	max(a.cd_categoria),
	max(a.cd_pessoa_fisica)
into	cd_convenio_w,
	ie_tipo_atendimento_w,
	cd_plano_w,
	cd_setor_atendimento_w,
	cd_categoria_w,
	cd_paciente_w
from	agenda_consulta a
where	a.nr_sequencia = nr_sequencia_p;

ie_regra_glosa_conv_w	:=	Obter_Valor_Param_Usuario(866, 168, Obter_Perfil_Ativo, nm_usuario_p, 0);

if	(ie_regra_glosa_conv_w = 'S') and
	(cd_convenio_w is not null) and
	(cd_procedimento_p is not null) then

	ageint_consiste_plano_conv(
				null,
				cd_convenio_w,
				cd_procedimento_p,
				ie_origem_proced_p,
				sysdate,
				1,
				nvl(ie_tipo_atendimento_w,0),
				cd_plano_w,
				null,
				ds_erro_w,
				0,
				null,
				ie_regra_w,
				null,
				nr_seq_regra_w,
				nr_seq_proc_interno_p,
				cd_categoria_w,
				cd_estabelecimento_p,
				null,
				null,
				ie_glosa_w,
				cd_edicao_ajuste_w,
				null,
				ds_irrelevante_w,
				ds_irrelevante_w,
				cd_paciente_w,
				null,
				pr_glosa_w,
				vl_glosa_w);

	ie_edicao_w	:= ageint_obter_se_proc_conv(cd_estabelecimento_p, cd_convenio_w, cd_categoria_w, sysdate, 
				cd_procedimento_p, ie_origem_proced_p, nr_seq_proc_interno_p, nvl(ie_tipo_Atendimento_w,0));
	ie_pacote_w	:= obter_se_pacote_convenio(cd_procedimento_p, ie_origem_proced_p, cd_convenio_w, cd_estabelecimento_p);

	
	if	(ie_edicao_w 			= 'N') and
		(nvl(cd_edicao_ajuste_w,0) 	= 0) and
		(ie_pacote_w			= 'N') then
		ie_glosa_w        := 'T';
	end if;

	
	if	(ie_edicao_w 			= 'N') and
		(nvl(cd_edicao_ajuste_w,0) 	> 0) and
		(ie_pacote_w			= 'N') then

		
		select   count(*)
		into     qt_item_edicao_w
		from     preco_amb
		where    cd_edicao_amb = cd_edicao_ajuste_w
		and      cd_procedimento = cd_procedimento_p
		and      ie_origem_proced = ie_origem_proced_p;

		if          (qt_item_edicao_w = 0) then
			    ie_glosa_w :=    'G';
		end if;
	end if;
	update		agenda_consulta_proc
	set		ie_glosa	= ie_glosa_w,
			ie_regra	= ie_regra_w,
			nr_seq_regra	= nr_seq_regra_w
	where		nr_sequencia   	= nr_seq_agenda_p
	and		nr_seq_agenda	= nr_sequencia_p;	

end if;

commit;
end Agserv_cons_proc_regra_glosa;
/