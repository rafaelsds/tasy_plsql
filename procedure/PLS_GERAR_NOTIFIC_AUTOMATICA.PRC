create or replace
procedure pls_gerar_notific_automatica
			(	nr_seq_auditoria_p	in	Number,
				nm_usuario_p		in	Varchar2,
				ie_mostrar_mensagem_p	out	Varchar2) is

ie_tipo_guia_w					Varchar2(4);
ie_acao_notificacao_w			        Number(2);
nr_telef_celular_benef_w		        Varchar2(15);
nr_seq_regra_notif_w			        Number(10);
ds_notificacao_w				Varchar2(4000);
ie_estagio_req_w				number(3);
ie_valida_qtde_igual_regra_w	                varchar2(1) := 'S';
qt_item_w					number(10);
qt_item_igual_w					number(10);
nr_seq_requisicao_w				number(10);
nr_seq_guia_w					number(10);
ie_tipo_segurado_w                              Varchar2(3);
nr_seq_notif_w                                  number(10);
nr_seq_segurado_w                               number(10);
ie_tipo_segurado_guia_req_w                     Varchar2(3);

ie_tipo_processo_w				pls_regra_notificacao_aut.ie_tipo_processo%type;
ie_carater_atendimento_w		        pls_regra_notificacao_aut.ie_carater_atendimento%type;


Cursor C01 is
	select	ie_acao_notificacao,
		ds_notificacao,
                nr_Sequencia
	from	pls_regra_notificacao_aut
	where	ie_situacao			= 'A'
	and	(ie_tipo_guia		is null	or	ie_tipo_guia		= ie_tipo_guia_w)
	and	(ie_estagio_requisicao	is null or	ie_estagio_requisicao	= ie_estagio_req_w)
	and	(ie_celular		= 'S'	or	(ie_celular		= 'N'	and	nr_telef_celular_benef_w is not null))
	and	(ie_quantidade_igual 	is null or	ie_quantidade_igual 	= ie_valida_qtde_igual_regra_w)
	and	(ie_tipo_processo	is null	or	ie_tipo_processo        = ie_tipo_processo_w)
	and	(ie_carater_atendimento	is null or      ie_carater_atendimento  = ie_carater_atendimento_w)
	and (nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento AND pls_obter_se_controle_estab('RE') = 'S')
	UNION
	select	ie_acao_notificacao,
			ds_notificacao,
			nr_Sequencia
	from	pls_regra_notificacao_aut
	where	ie_situacao			= 'A'
	and	(ie_tipo_guia		is null	or	ie_tipo_guia		= ie_tipo_guia_w)
	and	(ie_estagio_requisicao	is null or	ie_estagio_requisicao	= ie_estagio_req_w)
	and	(ie_celular		= 'S'	or	(ie_celular		= 'N'	and	nr_telef_celular_benef_w is not null))
	and	(ie_quantidade_igual 	is null or	ie_quantidade_igual 	= ie_valida_qtde_igual_regra_w)
	and	(ie_tipo_processo	is null	or	ie_tipo_processo        = ie_tipo_processo_w)
	and	(ie_carater_atendimento	is null or      ie_carater_atendimento  = ie_carater_atendimento_w)
	and pls_obter_se_controle_estab('RE') = 'N';

Cursor	C02 is
        select  ie_tipo_segurado
        from    pls_excecao_reg_notif_aut
        where   nr_seq_regra_notif      = nr_seq_notif_w
        and     ie_situacao		= 'A';

begin
ie_mostrar_mensagem_p	:= 'N';

begin
	select	ie_tipo_guia,
		nr_telef_celular_benef,
		nr_seq_requisicao,
		nr_seq_guia
	into	ie_tipo_guia_w,
		nr_telef_celular_benef_w,
		nr_seq_requisicao_w,
		nr_seq_guia_w
	from	pls_auditoria
	where	nr_sequencia	= nr_seq_auditoria_p;
exception
when others then
	ie_tipo_guia_w			:= null;
	nr_telef_celular_benef_w	:= null;
end;

if	(nvl(nr_seq_requisicao_w,0) > 0) then
	select	ie_estagio,
		ie_carater_atendimento,
		ie_tipo_processo,
                nr_seq_segurado
	into	ie_estagio_req_w,
		ie_carater_atendimento_w,
		ie_tipo_processo_w,
                nr_seq_segurado_w
	from	pls_requisicao
	where	nr_sequencia = nr_seq_requisicao_w;
end if;

if	(nvl(nr_seq_guia_w,0) > 0) then
	select	ie_carater_internacao,
		ie_tipo_processo,
                nr_seq_segurado
	into	ie_carater_atendimento_w,
		ie_tipo_processo_w,
                nr_seq_segurado_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_w;
end if;

if      (nr_seq_segurado_w       is not null) then
        select  ie_tipo_segurado
        into    ie_tipo_segurado_guia_req_w
        from    pls_segurado
        where   nr_sequencia = nr_seq_segurado_w;
end if;


select	count(1)
into	qt_item_w
from	pls_auditoria_item
where	nr_seq_auditoria = nr_seq_auditoria_p
and	ie_status = 'A';

select	count(1)
into	qt_item_igual_w
from	pls_auditoria_item
where	nr_seq_auditoria = nr_seq_auditoria_p
and	qt_original = qt_ajuste
and	ie_status = 'A';

if	(qt_item_w <> qt_item_igual_w) then
	ie_valida_qtde_igual_regra_w := 'N';
end if;

open C01;
loop
fetch C01 into
	ie_acao_notificacao_w,
	ds_notificacao_w,
        nr_seq_notif_w;
exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into 
		ie_tipo_segurado_w;
	exit when C02%notfound;
		begin
		if	(ie_tipo_segurado_w	=	ie_tipo_segurado_guia_req_w) then
			ie_acao_notificacao_w	:= 0;
			exit;
		end if;
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;
     
if	(nvl(ie_acao_notificacao_w,0)	= 1) then
	pls_solicita_notif_atendimento(nr_seq_auditoria_p,nvl(ds_notificacao_w,'Sem descri��o'), nm_usuario_p);
elsif	(nvl(ie_acao_notificacao_w,0)	= 2) then
	ie_mostrar_mensagem_p	:= 'S';
end if;

end pls_gerar_notific_automatica;
/
