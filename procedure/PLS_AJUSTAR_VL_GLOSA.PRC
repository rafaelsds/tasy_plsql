create or replace
procedure pls_ajustar_vl_glosa
			(	nr_seq_protocolo_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_congenere_w		number(10);
qt_dia_envio_w			number(10);
nr_sequencia_w			number(10);
dt_item_w			date;
ie_item_w			varchar2(10);
vl_adic_procedimento_w		number(12,2);
nr_seq_conta_w			number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_conta
	where	nr_seq_protocolo	= nr_seq_protocolo_p;

Cursor C02 is
	select	nr_sequencia,
		dt_procedimento,
		'P'
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_w
	union all
	select	nr_sequencia,
		dt_atendimento,
		'M'
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_w;
begin

select	nr_seq_congenere
into	nr_seq_congenere_w
from	pls_protocolo_conta
where	nr_sequencia	= nr_seq_protocolo_p;

open C01;
loop
fetch C01 into	
	nr_seq_conta_w;
exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into	
		nr_sequencia_w,
		dt_item_w,
		ie_item_w;
	exit when C02%notfound;
		begin
		if (ie_item_w = 'P') then
			begin
			select	vl_adic_procedimento
			into	vl_adic_procedimento_w
			from	ptu_nota_servico
			where	nr_seq_conta_proc	= nr_sequencia_w;
			exception
			when others then
				vl_adic_procedimento_w	:= 0;
			end;

			pls_obter_data_envio_operadora(dt_item_w, nr_seq_congenere_w, null, cd_estabelecimento_p, nm_usuario_p, qt_dia_envio_w);

			if	(nvl(qt_dia_envio_w,0) <> 0) then
				if	(trunc(dt_item_w + qt_dia_envio_w) < trunc(sysdate)) then
					update	pls_conta_proc
					set	vl_glosa	= vl_adic_procedimento_w
					where	nr_sequencia	= nr_sequencia_w;
				end if;
			end if;
		elsif (ie_item_w = 'M') then
			select	vl_adic_procedimento
			into	vl_adic_procedimento_w
			from	ptu_nota_servico
			where	nr_seq_conta_mat	= nr_sequencia_w;
			
			pls_obter_data_envio_operadora(dt_item_w, nr_seq_congenere_w, null, cd_estabelecimento_p, nm_usuario_p, qt_dia_envio_w);
			if	(nvl(qt_dia_envio_w,0) <> 0) then
				if	(trunc(dt_item_w + qt_dia_envio_w) < trunc(sysdate)) then
					update	pls_conta_mat
					set	vl_glosa	= vl_adic_procedimento_w
					where	nr_sequencia	= nr_sequencia_w;
				end if;
			end if;
		end if;
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;
commit;

end pls_ajustar_vl_glosa;
/