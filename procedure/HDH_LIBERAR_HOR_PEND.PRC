create or replace
procedure hdh_liberar_hor_pend is 


nr_prescricao_w			number(14);
nm_usuario_w			varchar2(15);
ds_lista_prescricao_w		varchar2(2000);
nr_sequencia_w			number(15,0);
ds_lista_usuario_destino_w	varchar2(1000);


cursor c01 is
select	a.nr_prescricao,
	a.nm_usuario_original
from	prescr_medica a
where	exists (	select	1
			from	prescr_material x
			where	a.nr_prescricao = a.nr_prescricao)
and	a.dt_suspensao is null
and	a.dt_liberacao is not null
and	trunc(a.dt_prescricao,'dd') between trunc(sysdate,'dd') and sysdate
and	exists (select	1
		from	prescr_mat_hor z
		where	z.nr_prescricao = a.nr_prescricao
		and	z.dt_lib_horario is null)	
union all
select	a.nr_prescricao,
	a.nm_usuario_original
from	prescr_medica a
where	exists (	select	1
			from	prescr_procedimento y
			where	y.nr_prescricao = a.nr_prescricao)
and	a.dt_suspensao is null
and	a.dt_liberacao is not null
and	trunc(a.dt_prescricao,'dd') between trunc(sysdate,'dd') and sysdate
and	exists (select	1
		from	prescr_proc_hor k
		where	k.nr_prescricao = a.nr_prescricao
		and	k.dt_lib_horario is null);
begin

ds_lista_prescricao_w 		:= null;
ds_lista_usuario_destino_w	:= null;	

open C01;
loop
fetch C01 into	
	nr_prescricao_w,
	nm_usuario_w;
exit when C01%notfound;
	begin
	Gerar_prescr_mat_sem_dt_lib(nr_prescricao_w,NULL,837, 'S' ,nm_usuario_w,null);
	gerar_prescr_proc_sem_dt_lib(nr_prescricao_w, NULL,837, 'S' ,nm_usuario_w);
	Gerar_prescr_hor_sem_lib(nr_prescricao_w,null,837, 'S' ,nm_usuario_w);
	gerar_prescr_dieta_hor_sem_lib(nr_prescricao_w,null, 837, 'S', null,'N',nm_usuario_w);
	
	if	(ds_lista_prescricao_w is not null) then	
		ds_lista_prescricao_w := substr(ds_lista_prescricao_w || ', ',1,2000);
	end if;
		
	ds_lista_prescricao_w	:= substr(ds_lista_prescricao_w || nr_prescricao_w,1,2000);
	
	
	if	(ds_lista_usuario_destino_w is not null) then
		ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || ',',1,1000);
	end if;
	
	ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || nm_usuario_w,1,1000);	
	exception when others then
		null;
	end;
end loop;
close C01;

if	(ds_lista_usuario_destino_w is not null) then
	select	min(nr_sequencia) 
	into	nr_sequencia_w
	from	comunic_interna_classif 
	where	ie_tipo	= 'F';

	insert	into comunic_interna(	
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		nm_usuario_destino,
		dt_atualizacao,
		ie_geral,
		ie_gerencial,
		cd_perfil,
		nr_sequencia,
		nr_seq_classif,
		dt_liberacao)
	values(
		sysdate,
		'Perda de conex�o com o banco. Prescri��es que estavam sem hor�rios liberados.',
		'Segue abaixo todas as prescri��es que os hor�rios foram liberados pelo JOB '|| chr(13) || chr(13)|| ds_lista_prescricao_w,
		'Tasy',
		ds_lista_usuario_destino_w,
		sysdate,
		'N',
		'N',
		837,
		comunic_interna_seq.nextval,
		nr_sequencia_w,
		sysdate);	
end if;

end hdh_liberar_hor_pend;
/
