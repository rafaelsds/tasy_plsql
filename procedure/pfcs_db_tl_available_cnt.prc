create or replace procedure pfcs_db_tl_available_cnt (
					nr_seq_indicator_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p	varchar2) is

qt_available_tl_w				number(15) := 0;
pfcs_panel_detail_seq_w			pfcs_panel_detail.nr_sequencia%type;
nr_seq_panel_w					pfcs_panel_detail.nr_seq_panel%type;
qt_total_aux                	pfcs_panel.vl_indicator_aux%type := 0;
qt_total_brkn_pfcs_w            number(15) := 0;
qt_isUse_device_w            	number(15) := 0;

cursor c01_from_pfcs is
  select 		sum(sa.nr_unit_tl_capacity) nr_capacity, 
        sa.ds_setor_atendimento  ds_department
  From 		setor_atendimento sa
  where 		sa.cd_estabelecimento_base = cd_estabelecimento_p
  and 		sa.cd_classif_setor in ('1','3','4','9','11','12')
  and 		sa.ie_situacao = 'A'
  group by 	sa.ds_setor_atendimento;

begin

	for r_c01 in c01_from_pfcs loop
	    pfcs_pck_v2.pfcs_generate_results(
		    vl_indicator_p => r_c01.nr_capacity,
		    ds_reference_value_p => r_c01.ds_department,
            vl_indicator_aux_p => 0,
		    nr_seq_indicator_p => 160,
		    nr_seq_operational_level_p => cd_estabelecimento_p,
		    nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);
    end loop;

 
    qt_available_tl_w := pfcs_func_tl_device_usage('Monitor', 'A', cd_estabelecimento_p);
    qt_isUse_device_w := pfcs_func_tl_device_usage('Monitor', 'U', cd_estabelecimento_p);
    qt_total_brkn_pfcs_w :=  pfcs_func_tl_device_usage('Monitor', 'B_L_D', cd_estabelecimento_p);
    -- Total caluclation is being used to show broken and lost percentage in Dashboard LHS card
    qt_total_aux :=  qt_available_tl_w + qt_isUse_device_w + qt_total_brkn_pfcs_w; 
	
    pfcs_pck_v2.pfcs_generate_results(
      vl_indicator_p => qt_available_tl_w,
      ds_reference_value_p => '',
      vl_indicator_aux_p => qt_total_aux,
      nr_seq_indicator_p => nr_seq_indicator_p,
      nr_seq_operational_level_p => cd_estabelecimento_p,
      nm_usuario_p => nm_usuario_p,
     nr_seq_panel_p => nr_seq_panel_w);
    commit;

	pfcs_pck_v2.pfcs_update_detail(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_panel_p => nr_seq_panel_w,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p);

	   pfcs_pck_v2.pfcs_update_detail(
            nr_seq_indicator_p => 160,
        	nr_seq_panel_p => nr_seq_panel_w,
        	nr_seq_operational_level_p => cd_estabelecimento_p,
        	nm_usuario_p => nm_usuario_p);

	pfcs_pck_v2.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p);

       pfcs_pck_v2.pfcs_activate_records(
        		nr_seq_indicator_p => 160,
        		nr_seq_operational_level_p => cd_estabelecimento_p,
        		nm_usuario_p => nm_usuario_p);

end pfcs_db_tl_available_cnt;
/
