create or replace
procedure executar_evento_agenda_atend( nr_atendimento_p		number,
					ie_evento_p					varchar2,
					cd_estabelecimento_p		number,
					nm_usuario_p				varchar2,
					ie_evolucao_clinica_p		varchar2 default null,
					NR_SEQ_TIPO_ATEND_p			varchar2 default null,
					nr_seq_agenda_p				varchar2 default null,
					cd_tipo_agenda_p 			varchar2 default null) is 

nr_seq_agenda_w			number(10);
cd_agenda_w				number(10);
cd_tipo_agenda_w		number(10);
ie_agenda_w				varchar2(5);
ie_clinica_w			number(10);

begin

if ((nr_seq_agenda_p > 0) and (cd_tipo_agenda_p > 0)) then

	if (cd_tipo_agenda_p in (3, 5)) then
		
		select 	max(cd_agenda)
		into	cd_agenda_w
		from	agenda_consulta
		where 	nr_sequencia = nr_seq_agenda_p;
	
	elsif (cd_tipo_agenda_p in (1, 2) and cd_agenda_w is null) then
		
		select  max(cd_agenda)
		into	cd_agenda_w
		from    agenda_paciente
		where   nr_sequencia = nr_seq_agenda_p;
	
	end if;
	
end if;

nr_seq_agenda_w  	:= nvl(nr_seq_agenda_p, obter_agenda_atendimento(nr_atendimento_p, 'S'));
cd_agenda_w		 	:= nvl(cd_agenda_w, obter_agenda_atendimento(nr_atendimento_p, 'C'));
cd_tipo_agenda_w 	:= nvl(cd_tipo_agenda_p, obter_tipo_agenda(cd_agenda_w));
ie_clinica_w		:= Obter_Clinica_Atend(nr_atendimento_p,'C');


if (cd_tipo_agenda_w = 1) then
	ie_agenda_w := 'CI';
elsif (cd_tipo_agenda_w = 2) then
	ie_agenda_w := 'E';
elsif (cd_tipo_agenda_w = 3) then
	ie_agenda_w := 'C';
elsif (cd_tipo_agenda_w = 5) then
	ie_agenda_w := 'S';
end if;	

if (obter_se_existe_evento_agenda(cd_estabelecimento_p, ie_evento_p, ie_agenda_w ,ie_evolucao_clinica_p,ie_clinica_w) = 'S' ) then
	executar_evento_agenda(ie_evento_p, ie_agenda_w, nr_seq_agenda_w, cd_estabelecimento_p, nm_usuario_p,ie_evolucao_clinica_p ,NR_SEQ_TIPO_ATEND_p);
end if;

end executar_evento_agenda_atend;
/
