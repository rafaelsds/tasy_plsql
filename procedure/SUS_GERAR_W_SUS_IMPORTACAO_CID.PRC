create or replace
procedure sus_gerar_w_sus_importacao_cid (	ds_conteudo_p		varchar2,
					ie_tipo_import_p		varchar2,
					dt_importacao_p			date,
					qt_commit_p			number,
					nm_usuario_p		varchar2) is 

begin

if	(ds_conteudo_p is not null) then
	begin
	
	insert into w_sus_importacao_cid (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_conteudo,
		ie_tipo_importacao,
		dt_importacao)
	values(	w_sus_importacao_cid_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_conteudo_p,
		ie_tipo_import_p,
		dt_importacao_p);	
	
	end;
end if;

if	(mod(qt_commit_p,100) = 0) then
	commit;
end if;

end sus_gerar_w_sus_importacao_cid;
/
