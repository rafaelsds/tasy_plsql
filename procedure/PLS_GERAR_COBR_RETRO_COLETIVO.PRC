create or replace
procedure pls_gerar_cobr_retro_coletivo
			(	nr_seq_reajuste_p	pls_reajuste.nr_sequencia%type,
				nr_seq_cobr_retro_p	pls_reajuste_cobr_retro.nr_sequencia%type,
				dt_mensalidade_p	pls_reajuste_cobr_retro.dt_referencia_mensalidade%type,
				dt_reajuste_p		pls_reajuste_cobr_retro.dt_referencia_reajuste%type,
				qt_parcelas_p		pls_reajuste_cobr_retro.qt_parcelas%type,
				ie_recomposicao_p	pls_reajuste_cobr_retro.ie_recomposicao%type,
				ie_acao_p		varchar2,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is
begin

if	(ie_acao_p = 'G') then
	pls_reaj_cobranca_retro_pck.gerar_cobranca_coletivo(
				nr_seq_reajuste_p,
				dt_mensalidade_p,
				dt_reajuste_p,
				qt_parcelas_p,
				ie_recomposicao_p,
				cd_estabelecimento_p,
				nm_usuario_p);
elsif	(ie_acao_p = 'D') then
	pls_reaj_cobranca_retro_pck.excluir_lote_cobranca_retro(nr_seq_cobr_retro_p);
end if;

end pls_gerar_cobr_retro_coletivo;
/