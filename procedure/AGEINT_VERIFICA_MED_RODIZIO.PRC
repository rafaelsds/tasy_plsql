create or replace
procedure Ageint_verifica_med_rodizio(nr_seq_ageint_item_p	number,
					dt_agenda_p		date,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2,
					cd_pessoa_fisica_p	varchar2,
					qt_dias_rodizio_p	number,
					ie_permite_p	out varchar2) is 

cd_medico_w		varchar2(10);	
nr_seq_ageint_lib_w	number(10,0);	
ds_erro_w		varchar2(255);	
cd_agenda_w		number(10);
ie_turno_medico_w	varchar2(1);
nr_seq_medico_regra_w	number(10,0);
ie_permite_w		varchar(1);
					
Cursor C01 is
	select	--nr_sequencia,
		distinct cd_pessoa_fisica,
		cd_agenda,
		nr_seq_medico_regra
	from	ageint_lib_usuario
	where	nr_seq_ageint_item 	= nr_seq_ageint_item_p
	and	cd_pessoa_fisica	= cd_pessoa_fisica_p;	
					
begin

open C01;
loop
fetch C01 into	
	--nr_seq_ageint_lib_w,
	cd_medico_w,
	cd_agenda_w,
	nr_seq_medico_regra_w;
exit when C01%notfound;
	begin
	ie_turno_medico_w := Ageint_Obter_Se_Turno_Med_Rod(cd_medico_w, dt_agenda_p, cd_agenda_w, nm_usuario_p, nr_seq_medico_regra_w, cd_estabelecimento_p);
	if	(ie_turno_medico_w = 'S') then
		Ageint_consistir_rozidio_exame(nr_seq_ageint_item_p,
						cd_medico_w,
						dt_agenda_p,
						cd_agenda_w,
						qt_dias_rodizio_p,
						cd_estabelecimento_p,
						nm_usuario_p,
						ds_erro_w);
	
		if	(ds_erro_w is null) then
			ie_permite_w	:= 'S';
		else
			ie_permite_w	:= 'N';
		end if;
	else
		ie_permite_w	:= 'S';
	end if;
	end;
end loop;
close C01;

ie_permite_p	:= ie_permite_w;

commit;

end Ageint_verifica_med_rodizio;
/