create or replace
procedure obter_convenio_ultima_agenda	(cd_pessoa_fisica_p			varchar2,
						nr_seq_agenda_p			number,
						ie_forma_convenio_p		varchar2,
						cd_convenio_p			out	number,
						cd_categoria_p			out	varchar2,
						cd_usuario_convenio_p	out	varchar2,
						dt_validade_carteira_p	out	date,
						nr_doc_convenio_p		out	varchar2,
						cd_plano_p				out	varchar2,
						cd_tipo_acomodacao_p	out	number,
						ds_observacao_p			out	varchar2) is

dt_agenda_w			date;
nr_seq_agenda_w		number(10,0);
cd_convenio_w			number(5,0);
cd_categoria_w		varchar2(10);
cd_usuario_convenio_w	varchar2(30);
dt_validade_w			date;
nr_doc_convenio_w		varchar2(20);
cd_tipo_acomodacao_w		number(4,0);
cd_plano_w		varchar2(10);
ds_observacao_w	varchar2(4000);

begin
if	(cd_pessoa_fisica_p is not null) then
	/* obter dados agenda atual */
	select	hr_inicio
	into	dt_agenda_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;

	/* obter ultimo agendamento */
	select	nvl(max(b.nr_sequencia),0)
	into	nr_seq_agenda_w
	from	agenda a,
		agenda_paciente b
	where	a.cd_agenda = b.cd_agenda
	and	a.cd_tipo_agenda in (1,2)
	and	((ie_forma_convenio_p	= 'A') or (b.ie_status_agenda = 'E'))
	and	b.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	hr_inicio < dt_agenda_w;

	/* obter convenio */
	if	(nr_seq_agenda_w > 0) then
		select	cd_convenio,
			cd_categoria,
			cd_usuario_convenio,
			dt_validade_carteira,
			nr_doc_convenio,
			cd_tipo_acomodacao,
			cd_plano,
			ds_observacao
		into	cd_convenio_w,
			cd_categoria_w,
			cd_usuario_convenio_w,
			dt_validade_w,
			nr_doc_convenio_w,
			cd_tipo_acomodacao_w,
			cd_plano_w,
			ds_observacao_w
		from	agenda_paciente
		where	nr_sequencia = nr_seq_agenda_w;
	end if;	
end if;

cd_convenio_p			:= cd_convenio_w;
cd_categoria_p		:= cd_categoria_w;
cd_usuario_convenio_p	:= cd_usuario_convenio_w;
dt_validade_carteira_p	:= dt_validade_w;
nr_doc_convenio_p		:= nr_doc_convenio_w;
cd_tipo_acomodacao_p		:= cd_tipo_acomodacao_w;
cd_plano_p		:= cd_plano_w;
ds_observacao_p	:= ds_observacao_w;

end obter_convenio_ultima_agenda;
/