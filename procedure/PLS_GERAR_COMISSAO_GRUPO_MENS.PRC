create or replace
procedure pls_gerar_comissao_grupo_mens
			(	nr_seq_lote_p		number,
				nr_seq_canal_venda_p	number,
				nr_seq_comissao_p	number,
				qt_vidas_p	out	number,
				dt_referencia_p		date,
				nr_seq_regra_mens_p	number,
				nr_seq_equipe_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is
				
/* Campos do cursor C01 */
nr_seq_inferior_w		number(10);

qt_vidas_w			number(10);
qt_vidas_canal_w		number(10);
nr_seq_regra_mens_w		number(10);
dt_referencia_w			date;

Cursor C01 is
	select	nr_seq_vendedor
	from	pls_equipe_vend_vinculo
	where	nr_seq_canal_superior = nr_seq_canal_venda_p
	and	nr_seq_equipe = nr_seq_equipe_p
	and	dt_referencia_w between nvl(dt_inicio_vigencia, dt_referencia_w) and nvl(dt_fim_vigencia, dt_referencia_w);
	
begin

dt_referencia_w	:= trunc(dt_referencia_p,'month');

open C01;
loop
fetch C01 into	
	nr_seq_inferior_w;
exit when C01%notfound;
	begin

	select	sum(qt_vidas) /* Quantidade de vidas */
	into	qt_vidas_canal_w
	from   (select	count(distinct c.nr_sequencia) qt_vidas
		from	pls_mensalidade			a,
			pls_mensalidade_segurado 	b,
			pls_segurado			c,
			pls_plano			d,
			pls_contrato			e
		where	a.nr_sequencia		= b.nr_seq_mensalidade
		and	b.nr_seq_segurado	= c.nr_sequencia
		and	c.nr_seq_contrato	= e.nr_sequencia
		and	d.nr_sequencia		= c.nr_seq_plano
		and	a.ie_cancelamento is null
		and	c.nr_seq_vendedor_canal = nr_seq_inferior_w
		and	((trunc(c.dt_contratacao,'month') = dt_referencia_w) or
			 (trunc(c.dt_liberacao,'month') = dt_referencia_w))			
		union all
		select	count(distinct c.nr_sequencia) qt_vidas
		from	pls_mensalidade			a,
			pls_mensalidade_segurado 	b,
			pls_segurado			c,
			pls_plano			d,
			pls_contrato			e,
			pls_segurado_canal_compl	f
		where	a.nr_sequencia		= b.nr_seq_mensalidade
		and	b.nr_seq_segurado	= c.nr_sequencia
		and	c.nr_seq_contrato	= e.nr_sequencia
		and	d.nr_sequencia		= c.nr_seq_plano
		and	f.nr_seq_segurado	= c.nr_sequencia
		and	a.ie_cancelamento is null
		and	f.nr_seq_vendedor_canal = nr_seq_canal_venda_p
		and	((trunc(c.dt_contratacao,'month') = dt_referencia_w) or
			 (trunc(c.dt_liberacao,'month') = dt_referencia_w)));	
			 
	pls_gerar_lote_comissao_mensal(nr_seq_lote_p, nr_seq_inferior_w, nr_seq_comissao_p, 'S', nr_seq_regra_mens_p, nm_usuario_p, cd_estabelecimento_p);
	pls_gerar_comissao_grupo_mens(nr_seq_lote_p, nr_seq_inferior_w, nr_seq_comissao_p, qt_vidas_w, dt_referencia_p, nr_seq_regra_mens_p, nr_seq_equipe_p, nm_usuario_p, cd_estabelecimento_p);	
	qt_vidas_canal_w := nvl(qt_vidas_canal_w,0) + nvl(qt_vidas_w,0);
	
	end;
end loop;
close C01;

qt_vidas_p := nvl(qt_vidas_canal_w,0);
	
--commit;

end pls_gerar_comissao_grupo_mens;
/