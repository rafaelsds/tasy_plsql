create or replace
procedure pls_insere_grupo_auditor_fax
			(	nr_seq_grupo_p		number,
				nr_seq_auditoria_p	number,	
				nm_usuario_p		Varchar2) is 

nr_seq_ordem_w			number(10);
nr_seq_grupo_w			number(10);
qt_grupo_ordem_w		number(5);

begin
begin
	select	nr_seq_ordem
	into	nr_seq_ordem_w
	from	pls_auditoria_grupo
	where	nr_sequencia = nvl(pls_obter_grupo_analise_atual(nr_seq_auditoria_p),0);

exception
when others then
	nr_seq_ordem_w := 1;
end;

select	pls_auditoria_grupo_seq.nextval
into	nr_seq_grupo_w
from	dual;

insert into pls_auditoria_grupo
	(nr_sequencia, nr_seq_auditoria, nr_seq_grupo,
	dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
	nm_usuario_nrec, nr_seq_ordem, ie_status,
	ie_manual)
values	(nr_seq_grupo_w, nr_seq_auditoria_p, nr_seq_grupo_p,
	sysdate, nm_usuario_p, sysdate, 
	nm_usuario_p,nr_seq_ordem_w ,'U',
	'S');

select	count(*)
into	qt_grupo_ordem_w
from	pls_auditoria_grupo
where	nr_seq_ordem		= nr_seq_ordem_w
and	nr_seq_auditoria	= nr_seq_auditoria_p;	
	
if	(qt_grupo_ordem_w > 1) then
	pls_ordena_grupo_aud_fax(nr_seq_ordem_w,nr_seq_grupo_w,nr_seq_auditoria_p,nm_usuario_p);
end if;

commit;

end pls_insere_grupo_auditor_fax;
/