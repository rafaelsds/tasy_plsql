create or replace
procedure HED_GERAR_RETORNO_CAIXA_150(	nr_seq_cobr_escrit_p	number,
					nm_usuario_p	varchar2) is 

nr_seq_reg_T_w		number(10);
nr_seq_reg_U_w		number(10);
nr_titulo_w		number(10);
vl_titulo_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_abatimento_w		number(15,2);
vl_liquido_w		number(15,2);
vl_outras_despesas_w	number(15,2);
dt_liquidacao_w		varchar2(8);
ds_titulo_w		varchar2(255);
vl_cobranca_w		number(15,2);
vl_alterar_w		number(15,2);
cd_ocorrencia_w		number(10);
nr_seq_ocorrencia_ret_w	number(10);
ie_rejeitado_w		varchar2(1);
nr_sequencia_w		number(10);

cursor c01 is
	select	trim(substr(ds_string,70,60)),
		0,
		0,
		0,
		0,
		to_number(substr(ds_string,53,15))/100,
		0,
		substr(ds_string,45,8),
		substr(ds_string,68,2)
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= 'F';
begin

open C01;
loop
fetch C01 into	
	ds_titulo_w,
	vl_cobranca_w,
	vl_acrescimo_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_outras_despesas_w,
	dt_liquidacao_w,
	cd_ocorrencia_w;
exit when C01%notfound;
	begin

	nr_titulo_w	:= null;
	vl_alterar_w	:= 0;
	
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo = somente_numero(ds_titulo_w);
	
	if	(nr_titulo_w is null) then
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_titulo_externo = somente_numero(ds_titulo_w)
		and	nr_titulo_externo is not null;
	end if;
	
	if	(nr_titulo_w is null) then
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_documento = somente_numero(ds_titulo_w)
		and	nr_documento is not null;
	end if;	

	/* Se encontrou o t�tulo importa, sen�o grava no log */

	if	(nr_titulo_w is not null) then

		select	vl_titulo
		into	vl_titulo_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;


		select 	nvl(to_char(max(a.nr_sequencia)),0),
			max(ie_rejeitado)
		into	nr_seq_ocorrencia_ret_w,
			ie_rejeitado_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco = 104
		and	a.cd_ocorrencia = cd_ocorrencia_w;

		/* Tratar acrescimos/descontos */
		if	(vl_titulo_w <> vl_liquido_w) then
			vl_alterar_w	:= vl_liquido_w - vl_titulo_w;

			if	(vl_alterar_w > 0) then
				vl_acrescimo_w	:= vl_alterar_w;	
			else
				vl_desconto_w	:= abs(vl_alterar_w);
			end if;
		end if;
		
		select	titulo_receber_cobr_seq.nextval
		into	nr_sequencia_w
		from	dual;		
		
		insert	into titulo_receber_cobr (	NR_SEQUENCIA,
							NR_TITULO,
							CD_BANCO,
							VL_COBRANCA,
							VL_DESCONTO,
							VL_ACRESCIMO,
							VL_DESPESA_BANCARIA,
							DT_ATUALIZACAO,
							NM_USUARIO,
							NR_SEQ_COBRANCA,
							nr_seq_ocorrencia_ret)
					values	(	nr_sequencia_w,
							nr_titulo_w,
							104,
							vl_titulo_w,
							vl_desconto_w,
							vl_acrescimo_w,
							vl_outras_despesas_w,
							sysdate,
							nm_usuario_p,
							nr_seq_cobr_escrit_p,
							nr_seq_ocorrencia_ret_w);
		commit;
							
		if (ie_rejeitado_w = 'L') then
			update titulo_receber_cobr
			set 	vl_liquidacao = vl_liquido_w,
				dt_liquidacao = nvl(to_date(dt_liquidacao_w,'yyyymmdd'),sysdate)
			where	nr_sequencia = nr_sequencia_w;
			commit;
		end if;	
		
	else
		insert	into	log_tasy
			(nm_usuario,
			dt_atualizacao,
			cd_log,
			ds_log)
		values	(nm_usuario_p,
			sysdate,
			55760,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');
		commit;

	end if;
	
	end;
end loop;
close C01;

commit;

end HED_GERAR_RETORNO_CAIXA_150;
/