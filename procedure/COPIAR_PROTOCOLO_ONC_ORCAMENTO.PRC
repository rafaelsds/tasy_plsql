create or replace
procedure copiar_protocolo_onc_orcamento(	cd_protocolo_p		number,
						nr_sequencia_p		number,
						nr_seq_orcamento_p	number,
						nm_usuario_p		varchar2,
						nr_seq_paciente_p	number) is


indice_w			number(10,0) := 0;
ds_valido_dias_w		varchar2(255) := '0123456789D,- ';
dia_ciclo_w			varchar2(255) := '';
type campos			is record (ds_dias_aplicacao varchar2(255));
type vetor			is table of campos index by binary_integer;

ie_dispara_lanc_auto_w		varchar2(1);

dias_filtro_w			vetor;
qt_dias_w			number(30);
qt_ciclos_w			number(30);
qt_lanc_w			number(30);
indice_dias_w			number(10,0) := 0;
posicao_virg_w			number(10,0) := 0;
indice_loop_w			number(10,0) := 0;

ds_dias_aplic_filtro_w		orcamento_paciente.ds_dias_aplic_filtro%type;
ds_ciclos_aplic_filtro_w	orcamento_paciente.ds_ciclos_aplic_filtro%type;

begin

ie_dispara_lanc_auto_w	:= nvl(obter_valor_param_usuario(106, 153, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),'N');

if	(nr_seq_paciente_p is null) then

	-- Medicamento
	insert into orcamento_paciente_mat (
		nr_sequencia_orcamento,
		cd_material,
		qt_material,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		vl_desconto,
		ie_valor_informado,
		nr_sequencia,
		cd_unidade_medida,
		ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		qt_dose,
		cd_protocolo,
		nr_seq_medicacao,
		qt_dias_util,
		cd_intervalo,
		ie_via_aplicacao)
	select	nr_seq_orcamento_p,
		cd_material,
		0,
		0,
		sysdate,
		nm_usuario_p,
		0,
		'N',
		orcamento_paciente_mat_seq.nextval,
		cd_unidade_medida,
		ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		nvl(qt_dose,0),
		cd_protocolo,
		nr_sequencia_p,
		qt_dias_util,
		cd_intervalo,
		ie_via_aplicacao
	from 	protocolo_medic_material
	where	cd_protocolo = cd_protocolo_p
	and	nr_sequencia = nr_sequencia_p
	and	ds_dias_aplicacao is not null
	and	qt_dose is not null
	and	nr_agrupamento > 0
	and	nr_seq_diluicao is null
	and	nr_seq_solucao is null;

	-- Solucao
	insert into orcamento_paciente_mat (
		nr_sequencia_orcamento,
		cd_material,
		qt_material,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		vl_desconto,
		ie_valor_informado,
		nr_sequencia,
		cd_unidade_medida,
		ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		qt_dose,
		cd_protocolo,
		nr_seq_medicacao,
		qt_dias_util,
		cd_intervalo,
		ie_via_aplicacao)
	select	nr_seq_orcamento_p,
		a.cd_material,
		0,
		0,
		sysdate,
		nm_usuario_p,
		0,
		'N',
		orcamento_paciente_mat_seq.nextval,
		a.cd_unidade_medida,
		b.ds_dias_aplicacao,
		nvl(b.ds_ciclos_aplicacao, a.ds_ciclos_aplicacao),
		nvl(qt_dose,0),
		b.cd_protocolo,
		nr_sequencia_p,
		a.qt_dias_util,
		b.cd_intervalo,
		b.ie_via_aplicacao
	from 	protocolo_medic_material a,
		protocolo_medic_solucao b
	where	b.cd_protocolo	= cd_protocolo_p
	and     a.cd_protocolo	= b.cd_protocolo
        and	a.nr_sequencia	= b.nr_sequencia
	and	a.nr_sequencia	= nr_sequencia_p
	and	a.nr_seq_solucao        = b.nr_seq_solucao
	and	b.ds_dias_aplicacao is not null
	and	a.qt_dose is not null
	and	a.nr_agrupamento > 0
	and	a.nr_seq_diluicao is null;

	-- Diluicao
	insert into orcamento_paciente_mat (
		nr_sequencia_orcamento,
		cd_material,
		qt_material,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		vl_desconto,
		ie_valor_informado,
		nr_sequencia,
		cd_unidade_medida,
		ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		qt_dose,
		cd_protocolo,
		nr_seq_medicacao,
		qt_dias_util,
		cd_intervalo,
		ie_via_aplicacao)
	select	nr_seq_orcamento_p,
		a.cd_material,
		0,
		0,
		sysdate,
		nm_usuario_p,
		0,
		'N',
		orcamento_paciente_mat_seq.nextval,
		a.cd_unidade_medida,
		nvl(b.ds_dias_aplicacao,a.ds_dias_aplicacao),
		nvl(b.ds_ciclos_aplicacao,a.ds_ciclos_aplicacao),
		nvl(a.qt_dose,0),
		a.cd_protocolo,
		nr_sequencia_p,
		a.qt_dias_util,
		nvl(a.cd_intervalo,b.cd_intervalo),
		a.ie_via_aplicacao
	from 	protocolo_medic_material a,
		protocolo_medic_material b
	where	a.nr_seq_diluicao = b.nr_seq_material
	and	a.cd_protocolo = b.cd_protocolo
	and	a.nr_sequencia = b.nr_sequencia
	and	a.cd_protocolo = cd_protocolo_p
	and	a.nr_sequencia = nr_sequencia_p
	and	nvl(b.ds_dias_aplicacao,a.ds_dias_aplicacao) is not null
	and	a.qt_dose is not null
	and	b.nr_agrupamento > 0
	and	a.nr_seq_solucao is null;

else

	-- Medicamento
	insert into orcamento_paciente_mat (
		nr_sequencia_orcamento,
		cd_material,
		qt_material,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		vl_desconto,
		ie_valor_informado,
		nr_sequencia,
		cd_unidade_medida,
		ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		qt_dose,
		cd_protocolo,
		nr_seq_medicacao,
		qt_dias_util,
		cd_intervalo,
		ie_via_aplicacao)
	select	nr_seq_orcamento_p,
		cd_material,
		0,
		0,
		sysdate,
		nm_usuario_p,
		0,
		'N',
		orcamento_paciente_mat_seq.nextval,
		cd_unidade_medida,
		ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		nvl(qt_dose,0),
		cd_protocolo,
		nr_sequencia_p,
		qt_dias_util,
		cd_intervalo,
		ie_via_aplicacao
	from 	paciente_protocolo_medic
	where	nr_seq_paciente = nr_seq_paciente_p
	and	ds_dias_aplicacao is not null
	and	qt_dose is not null
	and	nr_agrupamento > 0
	and	nr_seq_diluicao is null
	and	nr_seq_solucao is null
	and	nr_seq_procedimento is null
	and	nr_seq_medic_material is null;

	-- Solucao
	insert into orcamento_paciente_mat (
		nr_sequencia_orcamento,
		cd_material,
		qt_material,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		vl_desconto,
		ie_valor_informado,
		nr_sequencia,
		cd_unidade_medida,
		ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		qt_dose,
		cd_protocolo,
		nr_seq_medicacao,
		qt_dias_util,
		cd_intervalo,
		ie_via_aplicacao)
	select	nr_seq_orcamento_p,
		a.cd_material,
		0,
		0,
		sysdate,
		nm_usuario_p,
		0,
		'N',
		orcamento_paciente_mat_seq.nextval,
		a.cd_unidade_medida,
		b.ds_dias_aplicacao,
		nvl(b.ds_ciclos_aplicacao, a.ds_ciclos_aplicacao),
		nvl(qt_dose,0),
		a.cd_protocolo,
		nr_sequencia_p,
		a.qt_dias_util,
		b.cd_intervalo,
		b.ie_via_aplicacao
	from	paciente_protocolo_medic a,
		paciente_protocolo_soluc b
	where	a.nr_seq_paciente = nr_seq_paciente_p
	and	a.nr_seq_paciente = b.nr_seq_paciente
	and	b.nr_seq_solucao = a.nr_seq_solucao
	and	b.ds_dias_aplicacao is not null
	and	qt_dose is not null
	and	b.nr_agrupamento > 0
	and	a.nr_seq_diluicao is null
	and	nr_seq_procedimento is null
	and	nr_seq_medic_material is null;

	-- Diluicao
	insert into orcamento_paciente_mat (
		nr_sequencia_orcamento,
		cd_material,
		qt_material,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		vl_desconto,
		ie_valor_informado,
		nr_sequencia,
		cd_unidade_medida,
		ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		qt_dose,
		cd_protocolo,
		nr_seq_medicacao,
		qt_dias_util,
		cd_intervalo,
		ie_via_aplicacao)
	select	nr_seq_orcamento_p,
		a.cd_material,
		0,
		0,
		sysdate,
		nm_usuario_p,
		0,
		'N',
		orcamento_paciente_mat_seq.nextval,
		a.cd_unidade_medida,
		nvl(b.ds_dias_aplicacao,a.ds_dias_aplicacao),
		nvl(b.ds_ciclos_aplicacao,a.ds_ciclos_aplicacao),
		nvl(a.qt_dose,0),
		a.cd_protocolo,
		nr_sequencia_p,
		b.qt_dias_util,
		nvl(a.cd_intervalo, b.cd_intervalo),
		b.ie_via_aplicacao
	from 	paciente_protocolo_medic a,
		paciente_protocolo_medic b
	where	a.nr_seq_diluicao = b.nr_seq_material
	and	a.nr_seq_paciente = b.nr_seq_paciente
	and	a.nr_seq_paciente = nr_seq_paciente_p
	and	nvl(b.ds_dias_aplicacao,a.ds_dias_aplicacao) is not null
	and	a.qt_dose is not null
	and	b.nr_agrupamento > 0
	and	a.nr_seq_solucao is null
	and	a.nr_seq_procedimento is null
	and	a.nr_seq_medic_material is null;

end if;

if	(ie_dispara_lanc_auto_w = 'S') then

	select	ds_dias_aplic_filtro,
		ds_ciclos_aplic_filtro,
		nr_ciclos
	into	ds_dias_aplic_filtro_w,
		ds_ciclos_aplic_filtro_w,
		qt_ciclos_w
	from 	orcamento_paciente
	where	nr_sequencia_orcamento = nr_seq_orcamento_p;

	/* Filtro de dias e ciclos de aplicacao informado no orcamento - INICIO*/
	if	(ds_dias_aplic_filtro_w is not null) then

		if	(ds_ciclos_aplic_filtro_w is not null) then
			qt_ciclos_w	:= obter_qt_ciclos_aplicacao(ds_ciclos_aplic_filtro_w);
		else
			if	(qt_ciclos_w is null) then
				select	nvl(max(nr_ciclos),1)
				into	qt_ciclos_w
				from	protocolo_medicacao
				where	nr_sequencia = nr_sequencia_p
				and	cd_protocolo = cd_protocolo_p;
			end if;
		end if;

		for indice_w in 1..length(ds_dias_aplic_filtro_w) loop
			dia_ciclo_w	:= substr(upper(ds_dias_aplic_filtro_w), indice_w, 1);
			if	(instr(ds_valido_dias_w, dia_ciclo_w) = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(1084612);
			end if;
		end loop;

		while ds_dias_aplic_filtro_w is not null loop
			ds_dias_aplic_filtro_w := substr(ds_dias_aplic_filtro_w, instr(ds_dias_aplic_filtro_w,'D') + 1, length(ds_dias_aplic_filtro_w));
			posicao_virg_w	:= instr(ds_dias_aplic_filtro_w,'D');
			indice_dias_w := indice_dias_w + 1;

			if (posicao_virg_w = 0) then
				dias_filtro_w(indice_dias_w).ds_dias_aplicacao	:= 'D' || somente_numero_real(substr(ds_dias_aplic_filtro_w,1,length(ds_dias_aplic_filtro_w)));
				for j in 1.. qt_ciclos_w loop
					begin
					gerar_lanc_orc_automatico(	null,
									nr_seq_orcamento_p,
									192,
									null,
									nm_usuario_p,
									nr_sequencia_p,
									cd_protocolo_p,
									dias_filtro_w(indice_dias_w).ds_dias_aplicacao
									);
					end;
				end loop;
				ds_dias_aplic_filtro_w := '';
			else
				dias_filtro_w(indice_dias_w).ds_dias_aplicacao	:= 'D' || somente_numero_real(substr(ds_dias_aplic_filtro_w,1,posicao_virg_w - 2));
				for k in 1.. qt_ciclos_w loop
					begin
					gerar_lanc_orc_automatico(	null,
									nr_seq_orcamento_p,
									192,
									null,
									nm_usuario_p,
									nr_sequencia_p,
									cd_protocolo_p,
									dias_filtro_w(indice_dias_w).ds_dias_aplicacao
									);
					end;
				end loop;
				ds_dias_aplic_filtro_w := substr(ds_dias_aplic_filtro_w,posicao_virg_w, length(ds_dias_aplic_filtro_w));
			end if;
			if	(instr(dias_filtro_w(indice_dias_w).ds_dias_aplicacao,'D') = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(1084612);
			end if;
			indice_loop_w := indice_loop_w + 1;
			if	(indice_loop_w > 100) then
				exit;
			end if;
		end loop;

	end if;
end if;

commit;

end copiar_protocolo_onc_orcamento;
/
