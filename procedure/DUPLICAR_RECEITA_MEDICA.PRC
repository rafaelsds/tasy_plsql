create or replace
procedure duplicar_receita_medica	
	(nr_seq_receita_p	number,
	nm_usuario_p		varchar2) is


nr_seq_receita_w		number(10,0);
ds_receita_w			varchar2(4000);


begin

select	med_receita_seq.nextval
into	nr_seq_receita_w
from dual;

select  ds_receita
into	ds_receita_w
from	med_receita
where	nr_sequencia 	= nr_seq_receita_p;


insert into med_receita 
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_atendimento,
	dt_receita,
	ds_receita,
	nr_atendimento_hosp,
	nr_seq_cliente,
	cd_pessoa_fisica,
	ie_situacao)
(select nr_seq_receita_w,
	sysdate,
	nm_usuario_p,
	nr_atendimento,
	trunc(sysdate),
	ds_receita_w,
	nr_atendimento_hosp,
	nr_seq_cliente,
	cd_pessoa_fisica,
	'A'
from	med_receita
where 	nr_sequencia	= nr_seq_receita_p);

commit;

end duplicar_receita_medica;
/
