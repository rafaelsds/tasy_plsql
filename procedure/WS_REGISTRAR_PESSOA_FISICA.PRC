create or replace
procedure ws_registrar_pessoa_fisica (	cd_pessoa_fisica_ext_p	varchar2, 
			nm_pessoa_fisica_p	varchar2,
			dt_nascimento_p		date,
			nr_cpf_p			varchar2,
			nr_identidade_p		varchar2,
			ie_sexo_p		varchar2,
			nr_ddd_celular_p		varchar2,
			nr_telefone_celular_p	varchar2,
			nr_seq_cor_pele_p		number,
			cd_nacionalidade_p	varchar2,
			nr_cartao_nac_sus_p	varchar2,
			nm_usuario_p		varchar2,
			cd_pessoa_fisica_p		out varchar2,
			cd_erro_p		out varchar2,
			ds_erro_p			out varchar2) is 

/*									
0 - SUCESSO
1 - Dados n�o informados
	1.1 - Nome do paciente n�o informado
	1.2 - Data de nascimento n�o informado
2 - Dados inconsistentes
	2.1 - C�digo da cor do paciente n�o encontrado
	2.2 - C�digo da nacionalidade n�o encontrado
3 - Erro ao cadastrar o paciente
*/

ie_raca_cor_w			varchar2(1);
ie_nacionalidade_w		varchar2(1);
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
									
begin

cd_erro_p	:= 0;

if	(nm_pessoa_fisica_p is null) then
	cd_erro_p	:=	1.1;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279621,null);
elsif	(dt_nascimento_p is null) then
	cd_erro_p	:=	1.2;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279623,null);
end if;

if	(cd_erro_p = 0) then
		
	select 	decode(count(*),0,'N','S')
	into	ie_raca_cor_w
	from	cor_pele a
	where	nr_sequencia = nr_seq_cor_pele_p;	
	
	select	decode(count(*),0,'N','S')
	into	ie_nacionalidade_w	
	from	nacionalidade a
	where	a.cd_nacionalidade = cd_nacionalidade_p;

	
	if	(cd_erro_p = 0) and
		(nr_seq_cor_pele_p is not null) and
		(ie_raca_cor_w = 'N') then
		cd_erro_p	:= 2.1;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280041,null);
	elsif	(cd_erro_p = 0) and
			(cd_nacionalidade_p is not null) and
			(ie_nacionalidade_w = 'N') then
		cd_erro_p	:= 2.2;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280042,null);
	end if;	
	
	if	(cd_erro_p = 0) then		
	
		begin
		
		select	max(a.cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	pessoa_fisica a
		where	a.nr_identidade = nr_identidade_p
		and		a.nr_cpf = nr_cpf_p;	
		
		if	(cd_pessoa_fisica_w is null) then
				
			select	pessoa_fisica_seq.NextVal
			into	cd_pessoa_fisica_w
			from	dual;
			
			insert into pessoa_fisica(	
						cd_pessoa_fisica,
						nm_pessoa_fisica,
						dt_nascimento,
						ie_sexo,
						nr_ddd_celular,
						nr_telefone_celular,
						ie_revisar,
						nr_cpf,
						nr_identidade,
						ie_tipo_pessoa, 
						dt_atualizacao,
						dt_atualizacao_nrec, 
						nm_usuario,
						nm_usuario_nrec,					
						nr_seq_cor_pele,
						cd_nacionalidade,
						nr_cartao_nac_sus)
					values	(cd_pessoa_fisica_w,
						nm_pessoa_fisica_p,
						dt_nascimento_p,
						ie_sexo_p,
						nr_ddd_celular_p,
						nr_telefone_celular_p,
						'N',
						nr_cpf_p,
						nr_identidade_p,
						2,
						sysdate,
						sysdate,
						nm_usuario_p,
						nm_usuario_p,					
						nr_seq_cor_pele_p,
						cd_nacionalidade_p,
						nr_cartao_nac_sus_p);
		else
			
			update	pessoa_fisica
			set		nm_pessoa_fisica	= nm_pessoa_fisica_p,
					dt_nascimento       = dt_nascimento_p,
					ie_sexo             = ie_sexo_p,
					nr_ddd_celular      = nr_ddd_celular_p,
					nr_telefone_celular = nr_telefone_celular_p,
					dt_atualizacao		= sysdate,
					nm_usuario          = nm_usuario_p,
					nr_seq_cor_pele     = nr_seq_cor_pele_p,
					cd_nacionalidade    = cd_nacionalidade_p,
					nr_cartao_nac_sus	= nr_cartao_nac_sus_p	
			where	cd_pessoa_fisica 	= cd_pessoa_fisica_w;
			
		end if;
		
		commit;		
		
		cd_pessoa_fisica_p	:= cd_pessoa_fisica_w;
		
		exception
		when others then
			cd_erro_p	:= 3;
			ds_erro_p	:= substr(sqlerrm,1,2000);
		end;
		
	end if;

end if;

end ws_registrar_pessoa_fisica;
/