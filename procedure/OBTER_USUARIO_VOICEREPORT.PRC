create or replace
procedure obter_usuario_voicereport( nm_usuario_tasy_p			    varchar2,
                               nm_usuario_integr_p	      out	varchar2,
                               ds_senha_usuario_integr_p	out	varchar2,
                               ds_contexto_p	            out	varchar2,
                               ds_porta_p                 out varchar2) is

nm_usuario_integr_w       varchar2(100);
ds_senha_usuario_integr_w varchar2(100);
ds_contexto_w             varchar2(100);
ds_porta_w        varchar(100);
begin
				

if	(nm_usuario_tasy_p is not null) then
	select max(nm_usuario_integr), 
	    	 max(ds_senha_usuario_integr),
		     max(ds_organizacao),
         max(nm_instituto)
    into nm_usuario_integr_w,
	   	   ds_senha_usuario_integr_w,
		     ds_contexto_w,
         ds_porta_w
	  from lab_regra_usuario_integr
	 where ds_servico = 'voiceReport'
	   and nm_usuario_tasy = nm_usuario_tasy_p;
end if;

nm_usuario_integr_p := nm_usuario_integr_w;
ds_senha_usuario_integr_p := ds_senha_usuario_integr_w;
ds_contexto_p := ds_contexto_w;
ds_porta_p := ds_porta_w;
end obter_usuario_voicereport;
/