create or replace
procedure Atualizar_dose_esp_dieta (	nr_prescricao_p		number,
					cd_perfil_p		number,
					nm_usuario_p		Varchar2) is
			
hr_dose_jejum_w			date;

begin

select	max(a.dt_fim + 1/1440)
into	hr_dose_jejum_w
from 	rep_jejum a,
	prescr_medica b 
where 	a.nr_prescricao = b.nr_prescricao
and	a.nr_prescricao	= nr_prescricao_p
and	b.dt_suspensao is null
and   	dt_fim > sysdate;

if	(hr_dose_jejum_w is not null) then
	
	update	prescr_dieta
	set	hr_dose_especial	= to_char(hr_dose_jejum_w,'hh24:mi')
	where	nr_prescricao		= nr_prescricao_p
	and	hr_dose_especial is not null;
	
	update	prescr_material
	set	hr_dose_especial	= to_char(hr_dose_jejum_w,'hh24:mi')
	where	nr_prescricao		= nr_prescricao_p
	and	ie_agrupador in(8,12)
	and	hr_dose_especial is not null;
	
	commit;
	
	Gerar_prescr_hor_sem_lib(nr_prescricao_p, null, cd_perfil_p, 'N', nm_usuario_p);
	gerar_prescr_dieta_hor_sem_lib(nr_prescricao_p, null, cd_perfil_p, 'N', null,'N',nm_usuario_p);
end if;	


end Atualizar_dose_esp_dieta;
/