CREATE OR REPLACE PROCEDURE hsp_Gerar_Ocupacao_Hospitalar(	
				dt_parametro_P    DATE) IS 

dt_atualizacao_w		date          := SYSDATE;
dt_parametro_fim_w		date;
dt_parametro_w			date;
dt_parametro_mes_w		date;
dt_entrada_w			date;
leito_utilizado_w		number;
dt_entrada_unidade_w		date;
dt_Prim_entrada_unidade_w	date;
dt_Ult_entrada_unidade_w	date;
dt_Ult_saida_unidade_w		date;
dt_saida_unidade_w		date;
ie_considera_censo_passagem_w	varchar2(1);
nr_atendimento_w		NUMBER(10);
nr_atendimento_ant_w		NUMBER(10);
cd_estabelecimento_w		NUMBER(4)     := 1;
cd_setor_atendimento_w		NUMBER(5);
cd_setor_atendimento_aux_w	NUMBER(5);
cd_procedencia_w		Number(5);
ie_tipo_atendimento_w		Number(3);
cd_pessoa_fisica_w		VARCHAR2(10);
cd_medico_w			VARCHAR2(10);
cd_unidade_basica_w		VARCHAR2(10);
cd_unidade_compl_w		VARCHAR2(10);
ie_status_unidade_w		VARCHAR2(03)  := 'L';
ie_status_paciente_w		VARCHAR2(03)  := 'L';
nr_altas_w			NUMBER(9)     := 0;
nr_pacientes_w			NUMBER(9)     := 0;
nr_paciente_unid_w		NUMBER(9)     := 0;
nr_admissoes_w			NUMBER(9)     := 0;
nr_Obitos_w			NUMBER(9)     := 0;
qt_Obito_p24h_w			NUMBER(9)     := 0;
nr_transf_Entrada_w		NUMBER(9)     := 0;
nr_Transf_Saida_w		NUMBER(9)     := 0;
qt_leito_livre_w		NUMBER(9)	  := 0;
ie_clinica_w			NUMBER(5);
cd_convenio_w			NUMBER(5);
cd_tipo_acomodacao_w		NUMBER(5);
cd_motivo_alta_w		number(5)     := 0;
dt_saida_interno_w		date;				
dt_alta_w			date;
ds_retorno_w			Varchar2(40);
ie_temporario_w			Varchar2(01);
ie_situacao_w			Varchar2(01);
hr_alta_w			Number(02)	:= 0;
ie_clinica_alta_w		Varchar2(03);
ie_clinica_padrao_w		Varchar2(03)	:= 'L';
ie_status_hist_w		varchar2(05);	
ie_obito_w			Varchar2(01);
nr_seq_origem_w			number(10,0);
nr_seq_interno_w		number(10,0);
qt_admitido_alta_dia_w		number(10,0)	:= 0;
ds_tablespace_w			Varchar2(40);
cd_cid_principal_w		varchar2(10);
ie_ok_w				varchar2(01)	:= 'N';
ie_temp_w			Varchar2(01);
qt_base_ocup_w			Number(15,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
nr_seq_hist_w			number(10,0);
nr_seq_queixa_w			number(10);
cd_estab_atend_w          	number(4,0);
cd_categoria_w			varchar2(10);
nr_seq_forma_chegada_w		number(10,0);
nr_seq_indicacao_w		number(10,0);	

CURSOR C01 IS
	select a.cd_setor_atendimento,
		a.cd_unidade_basica,
		a.cd_unidade_compl,
		NVL(a.ie_status_unidade,'L') ie_status_unidade,
		NVL(a.ie_temporario,'N'),
		decode(NVL(a.ie_situacao,'A'),'A','A',obter_se_unidade_ativa_data(dt_parametro_p,a.nr_seq_interno)),
		a.nr_seq_interno
	from	unidade_atendimento a, 
		setor_atendimento b
	where	a.cd_setor_atendimento 	= b.cd_setor_atendimento
	and	b.cd_classif_setor 		in (3,4)
	and	(((ie_considera_censo_passagem_w = 'S') and (b.ie_ocup_hospitalar = 'S')) or
		(ie_considera_censo_passagem_w = 'N')) 
	--and	NVL(ie_ocup_hospitalar,'N')	= 'S'
	and	(nvl(dt_criacao,dt_parametro_p) -1) <= dt_parametro_p
	order by 
		a.cd_setor_atendimento,
		a.cd_unidade_basica,
		a.cd_unidade_compl;
CURSOR C02 IS
	select /*+ INDEX(B ATEPACU_I1) INDEX(a atepaci_pk) */
		a.nr_atendimento,
		a.ie_tipo_atendimento,
		a.cd_procedencia,
		a.cd_estabelecimento,
		a.cd_pessoa_fisica,
		a.cd_medico_resp,
		a.ie_clinica,
		b.cd_tipo_acomodacao,
		b.dt_entrada_unidade,
		NVL(b.dt_saida_unidade, dt_parametro_fim_w + 1),
		NVL(a.dt_alta, dt_parametro_fim_w + 1),
		a.cd_motivo_alta,
		a.dt_entrada,
		a.ie_clinica_alta,
		obter_cid_atendimento(a.nr_atendimento, 'P'),
		b.dt_saida_interno,
		0,
		0,
		a.nr_seq_queixa,
		obter_categoria_atendimento(a.nr_atendimento),
		a.nr_seq_forma_chegada,
		a.nr_seq_indicacao
		/* campo_numerico(substr(obter_proc_principal(a.nr_atendimento, obter_convenio_atendimento(a.nr_atendimento), 
							a.ie_tipo_atendimento, null, 'CO'),2,9)),
						campo_numerico(substr(obter_proc_principal(a.nr_atendimento, obter_convenio_atendimento(a.nr_atendimento), 
							a.ie_tipo_atendimento, null, 'CO'),1,1)) Dalcastagne em 30/08/2007 - Retirei por motivo da lentid�o na atualiza��o */
	from	atendimento_paciente a, 
		atend_paciente_unidade b   
	where	a.nr_atendimento = b.nr_atendimento
	and	b.cd_setor_atendimento = cd_setor_atendimento_w
	and	b.cd_unidade_basica    = cd_unidade_basica_w
	and	b.cd_unidade_compl     = cd_unidade_compl_w
	and	b.dt_entrada_unidade   < dt_parametro_fim_w
	and	b.dt_saida_interno 	>= dt_parametro_w
	and	not exists 
		(select	x.cd_motivo_alta
		from	motivo_alta x 
		where	x.ie_censo_diario = 'N'
		and	a.cd_motivo_alta 	= x.cd_motivo_alta)
        order by 10 desc;
Cursor C09 is
select 				
	cd_estabelecimento	,
	cd_setor_atendimento ,        
	cd_unidade_basica    ,
	cd_unidade_compl  	,
	ie_situacao		,
	cd_pessoa_fisica,
	sum(nr_admissoes)    ,
	sum(nr_altas)        ,
	sum(nr_obitos)	,
	sum(nr_transf_entrada)  ,
	sum(nr_transf_Saida)    ,
	sum(nr_pacientes)       ,
	ie_clinica              ,
	cd_convenio	      ,
	sum(qt_leito_livre)     ,
	cd_tipo_acomodacao	,
	ie_tipo_atendimento	,
	cd_procedencia		, 
	cd_medico		,
	hr_alta,
	sum(qt_obito_apos24h),
	ie_clinica_alta,
	nr_seq_origem,
	sum(qt_admitido_alta_dia),
	cd_cid_principal,
	ie_temp,
	sum(nvl(qt_base_ocup,0)),
	cd_procedimento,
	ie_origem_proced,
	cd_motivo_alta,
	nr_seq_queixa,
	cd_categoria,
	nr_seq_forma_chegada,
	dt_entrada,
	nr_seq_indicacao
from hsp_ocupacao_hospitalar
where trunc(dt_referencia, 'month') = dt_parametro_mes_w
and	ie_periodo		= 'D'
group by	
	cd_estabelecimento	,
	cd_setor_atendimento    ,        
	cd_unidade_basica       ,
	cd_unidade_compl  	,
	ie_situacao		,
	cd_pessoa_fisica,
	ie_clinica              ,
	cd_convenio      ,
	cd_tipo_acomodacao	,
	ie_tipo_atendimento	,
	cd_procedencia		, 
	cd_medico		,
	hr_alta,
	ie_clinica_alta,
	nr_seq_origem,
	cd_cid_principal,
	ie_temp,
	cd_procedimento,
	ie_origem_proced,
	cd_motivo_alta		,
	nr_seq_queixa,
	cd_categoria		,
	nr_seq_forma_chegada	,
	dt_entrada		,
	nr_seq_indicacao	;
Cursor 	C03 is
	select	nvl(max(nr_sequencia),0),
		nvl(max(obter_estab_atend(nr_atendimento)),0)
	from	unidade_atend_hist
	where	nr_seq_unidade	= nr_seq_interno_w
	and	dt_parametro_w between trunc(dt_historico)  and fim_dia(nvl(dt_fim_historico, sysdate));

BEGIN
--Insert into Log_Tasy values (sysdate, 'Tasy', 601, 'Ocupacao Hospitalar');
--commit;
dt_parametro_fim_w                    := trunc(dt_parametro_p, 'dd') + 1 - 1/86400;
dt_parametro_w                        := Trunc(dt_parametro_p,'dd');
dt_parametro_mes_w                    := trunc(dt_parametro_w, 'month');
/* Excluir os Registros caso j� tenha sido rodada a rotina  */
if 	(dt_parametro_w > trunc(sysdate, 'dd')) then
	Wheb_mensagem_pck.exibir_mensagem_abort(262502);
end if;
--delete from hsp_ocupacao_hospitalar
--where dt_referencia = dt_parametro_w;
--COMMIT;

select	nvl(max(ie_considera_censo_passagem),'N')
into	ie_considera_censo_passagem_w
from	parametro_atendimento;

/* Obter as Unidades de interna��o  */
OPEN C01;
LOOP
FETCH C01 into 
	cd_setor_atendimento_w,
	cd_unidade_basica_w,
	cd_unidade_compl_w,
	ie_status_unidade_w,
	ie_temporario_w,
	ie_situacao_w,
	nr_seq_interno_w;
EXIT WHEN C01%NOTFOUND;
	begin
	ie_status_paciente_w		:= 'L';
	nr_paciente_unid_w		:= 0;
	nr_pacientes_w             	:= 0;
	qt_leito_livre_w		:= 0;
	qt_admitido_alta_dia_w	:= 0;
	nr_atendimento_ant_w		:= 0;
	select Obter_Se_Unidade_Temp(
			cd_setor_atendimento_w,
			dt_parametro_p,
			cd_unidade_basica_w,
			cd_unidade_compl_w)
	into	ie_temp_w
	from	dual;
	
	OPEN C02;
	LOOP
	FETCH C02 into
		nr_atendimento_w, 
		ie_tipo_atendimento_w,
		cd_procedencia_w,
		cd_estabelecimento_w,
		cd_pessoa_fisica_w,
		cd_medico_w,
		ie_clinica_w,
		cd_tipo_acomodacao_w,
		dt_entrada_unidade_w,
		dt_saida_unidade_w,
		dt_alta_w,
		cd_motivo_alta_w,
		dt_entrada_w,
		ie_clinica_alta_w,
		cd_cid_principal_w,
		dt_saida_interno_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_queixa_w,
		cd_categoria_w,
		nr_seq_forma_chegada_w,
		nr_seq_indicacao_w;
	EXIT WHEN C02%NOTFOUND;
	qt_Obito_p24h_w			:= 0;
	nr_obitos_w                  	:= 0;
	nr_transf_entrada_w          	:= 0;
	nr_transf_saida_w            	:= 0;
	nr_altas_w                   	:= 0;
	nr_admissoes_w               	:= 0;       	
	nr_pacientes_w               	:= 0;
	hr_alta_w			:= 0;
	qt_admitido_alta_dia_w		:= 0;
	select	nvl(max(ie_obito),'N')
	into	ie_obito_w
	from	motivo_alta
	where	cd_motivo_alta	= cd_motivo_alta_w;
	begin	
	select	nvl(min(a.cd_convenio),0)
	into	cd_convenio_w
	from	atend_categoria_convenio a  
	where	a.nr_atendimento = nr_atendimento_w;
	select	nvl(min(a.nr_seq_origem),0)
	into	nr_seq_origem_w
	from	atend_categoria_convenio a  
	where	a.nr_atendimento = nr_atendimento_w
	and	a.cd_convenio	 = cd_convenio_w;
--------------  Obter primeira/ultima entrada no setor do setor ------------
	select	min(a.dt_entrada_unidade),
		max(a.dt_entrada_unidade),
		max(a.dt_saida_unidade)
	into	dt_prim_entrada_unidade_w,    
		dt_ult_entrada_unidade_w,
		dt_ult_saida_unidade_w
	from	setor_atendimento b, atend_paciente_unidade a, atendimento_paciente c    
	where	a.nr_atendimento = nr_atendimento_w
	and	a.cd_setor_atendimento = b.cd_setor_atendimento
	and	a.nr_Atendimento = c.nr_Atendimento
	and b.cd_classif_setor in (3,4,8) 
	and	(((ie_considera_censo_passagem_w = 'S') and (b.ie_ocup_hospitalar = 'S')) or
		(ie_considera_censo_passagem_w = 'N'));
-----  Teste da entrada do paciente do setor ---------------------------------
	if  	dt_entrada_unidade_w >= dt_parametro_w then
	    	begin	
	    	if	dt_entrada_unidade_w = dt_prim_entrada_unidade_w then
			nr_admissoes_w       := 1;
		else
            		begin 
/* -------------------------- Obter o setor anterior ao que est� sendo analisado  */
			select	NVL(max(x.cd_setor_atendimento), cd_setor_atendimento_w)
			into	cd_setor_atendimento_aux_w
			from	atend_paciente_unidade x
                        where	x.nr_atendimento	= nr_atendimento_w
			and	x.dt_entrada_unidade	= (
				select	max(dt_entrada_unidade)
				from	setor_atendimento b, atend_paciente_unidade a    
				where	a.nr_atendimento	= nr_atendimento_w
				and	a.dt_saida_unidade	<= dt_entrada_unidade_w
				and	a.dt_saida_unidade	>= dt_parametro_w
				and	a.cd_setor_atendimento	= b.cd_setor_atendimento
				and	b.cd_classif_setor in (3,4,8));
			if  	cd_setor_atendimento_aux_w  <> cd_setor_atendimento_w then 
				nr_transf_entrada_w		:= 1;
			end if;
                        end;
		end if;
		end;
	end if;
-----  Teste da Saida do paciente do setor ---------------------------------
	if	dt_saida_unidade_w > dt_parametro_fim_w then
		begin
		--
	    	select 	count(1) 
		into 	Leito_utilizado_w  
		from	hsp_ocupacao_hospitalar
		where	dt_referencia        	= dt_parametro_w
		and 	cd_unidade_basica    	= cd_unidade_basica_w
		and 	cd_unidade_compl	= cd_unidade_compl_w
		and 	cd_setor_atendimento 	= cd_setor_atendimento_w
		and	((cd_pessoa_fisica	= cd_pessoa_fisica_w) or (cd_pessoa_fisica is null))
		and	ie_periodo		= 'D'		
		and 	nr_pacientes		> 0;
		
		if	(Leito_utilizado_w = 0) then
		   	nr_pacientes_w := 1;
		else
		   	nr_pacientes_w := 0;
		end if;   
		ie_status_paciente_w     := 'P';                            
		end;
	else
		begin
-------------	if	(dt_alta_w <= dt_parametro_fim_w) and -------------- Marcus
		qt_admitido_alta_dia_w	:= 0;
		if	(cd_motivo_alta_w is not null) and
			(dt_saida_unidade_w 	= dt_ult_saida_unidade_w) and
			(trunc(dt_entrada_w, 'dd') = trunc(dt_saida_unidade_w, 'dd')) and
			(dt_alta_w is not null) and
			(nr_atendimento_ant_w	<> nr_atendimento_w) then
			begin
			qt_admitido_alta_dia_w	:= 1;
			end;
		end if;
		
		if	(cd_motivo_alta_w is not null) and
			(trunc(dt_saida_unidade_w,'mi') = trunc(dt_ult_saida_unidade_w,'mi')) then
			hr_alta_w		:= Campo_Numerico(to_char(dt_alta_w,'hh24'));
			if  	(ie_obito_w = 'S') then
				begin
				nr_obitos_w	:= 1;
				Insert into log_tasy (dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
				values (sysdate, 'Tasy', 900, nr_atendimento_w );
				if	((dt_entrada_w + 1) < dt_alta_w) then 
					qt_Obito_p24h_w	:= 1;
				end if;
				end;
			elsif 	(dt_saida_interno_w < to_date('01/01/2999','dd/mm/yyyy')) then
 				nr_altas_w	:= 1;
				Insert into log_tasy (dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
				values (sysdate, 'Tasy', 900, nr_atendimento_w );
			end if;
		else
			begin
/* -------------------- Obter o setor seguinte ao que est� sendo analisado  */
			select	NVL(max(x.cd_setor_atendimento), cd_setor_atendimento_w)
			into	cd_setor_atendimento_aux_w
			from	atend_paciente_unidade x
			where	x.nr_atendimento       = nr_atendimento_w
			and	x.dt_entrada_unidade   = 
				(select	Min(a.dt_entrada_unidade)
				from	setor_atendimento b, atend_paciente_unidade a    
				where	a.nr_atendimento = nr_atendimento_w
				and	a.dt_entrada_unidade   > dt_entrada_unidade_w
				and	a.dt_entrada_unidade  <= dt_parametro_fim_w 
				and	a.cd_setor_atendimento = b.cd_setor_atendimento
				and	b.cd_classif_setor in (3,4,8));
			if  	cd_setor_atendimento_w <> cd_setor_atendimento_aux_w then
				nr_transf_saida_w    := 1;
			end if;
			end;
		end if;
		end;
	end if;
	nr_paciente_unid_w	:= nr_paciente_unid_w + nr_pacientes_w;
-- Inserir registro na tabela -----------------------------------------------
	if  	(nr_pacientes_w + nr_obitos_w + nr_transf_entrada_w +
		nr_transf_saida_w + nr_altas_w + nr_admissoes_w) > 0 then
		begin
			insert into hsp_ocupacao_hospitalar(
				cd_estabelecimento, dt_referencia, ie_periodo,
				cd_setor_atendimento, cd_unidade_basica, 
				cd_unidade_compl, ie_situacao, nr_admissoes,
				nr_altas, nr_obitos, nr_transf_entrada,
				nr_transf_Saida, nr_pacientes,
				qt_leito_livre, ie_clinica,
				cd_convenio ,cd_tipo_acomodacao,
				cd_pessoa_fisica, ie_tipo_atendimento,
				cd_procedencia, cd_medico, hr_alta,
				qt_obito_apos24h,
				ie_clinica_alta,
				nr_seq_origem,
				qt_admitido_alta_dia,
				cd_cid_principal,
				ie_temp,
				cd_procedimento, ie_origem_proced,
				cd_motivo_alta,
				nr_seq_queixa,
				cd_categoria,
				nr_seq_forma_chegada,
				dt_entrada,
				nr_seq_indicacao) 
			(select	cd_estabelecimento_w,
				dt_Parametro_w    	,
				'D'			,     
				cd_setor_atendimento_w  ,        
				cd_unidade_basica_w     ,
				cd_unidade_compl_w	,
				ie_status_paciente_w	,
				nr_admissoes_w          ,
				nr_altas_w              ,
				nr_obitos_w 	        ,
				nr_transf_entrada_w     ,
				nr_transf_Saida_w       ,
				nr_pacientes_w          ,
				qt_leito_livre_w	,
				ie_clinica_w            ,
				nvl(cd_convenio_w,0)    ,
				cd_tipo_acomodacao_w    ,
				cd_pessoa_fisica_w      ,
				ie_tipo_atendimento_w	,
				cd_procedencia_w	, 
				cd_medico_w		,
				hr_alta_w,
				qt_Obito_p24h_w,
				nvl(ie_clinica_alta_w,ie_clinica_padrao_w),
				nvl(nr_seq_origem_w, 0), qt_admitido_alta_dia_w, cd_cid_principal_w,
				ie_temp_w,
				cd_procedimento_w, ie_origem_proced_w,
				cd_motivo_alta_w,
				nr_seq_queixa_w,
				cd_categoria_w,
				nr_seq_forma_chegada_w,
				dt_entrada_w,
				nr_seq_indicacao_w
			from	dual);
		end;			
	end if;
	nr_atendimento_ant_w	:= nr_atendimento_w;
	end;
	END LOOP;
	CLOSE C02;
	if	(nr_paciente_unid_w = 0) and
		(ie_situacao_w = 'A') 	and
		(ie_temporario_w = 'N') then
		begin
		/* Ricardo/Marcus - 23/05/2005 - Tiramos os status ie_status_unidade_w in ('L','I', 'O')  do if e colocamos os <> 'P' */
		ie_status_unidade_w	:= 'L';
		ie_status_hist_w	:= null;		

		open	c03;
		loop
		fetch	c03 into
			nr_seq_hist_w,
			cd_estab_atend_w;			
		exit 	when c03%notfound;
			begin		

			if	(nr_seq_hist_w <> 0) then	
				select	ie_status_unidade
				into	ie_status_hist_w
				from	unidade_atend_hist
				where	nr_sequencia = nr_seq_hist_w;
			end if;
			end;
		end loop;
		close c03;
		qt_leito_livre_w	:= 1;       
		insert into hsp_ocupacao_hospitalar(
				cd_estabelecimento	,
				dt_referencia    	,     
				ie_periodo		,
				cd_setor_atendimento	,        
				cd_unidade_basica    ,
				cd_unidade_compl	,
				ie_situacao 		,
				nr_admissoes         ,
				nr_altas             ,
				nr_obitos 	       ,
				nr_transf_entrada    ,
				nr_transf_Saida      ,
				nr_pacientes         ,
				qt_leito_livre	,
				cd_medico		,
				hr_alta		,
				qt_obito_apos24h	,
				cd_convenio		,
				ie_clinica_alta,
				nr_seq_origem,
				cd_tipo_acomodacao,
				qt_admitido_alta_dia,
				cd_cid_principal,
				ie_temp,
				cd_motivo_alta,
				dt_entrada)
		(select		cd_estab_atend_w	,
				dt_Parametro_w    	,
				'D'			,     
				cd_setor_atendimento_w,        
				cd_unidade_basica_w	,
				cd_unidade_compl_w	,
				nvl(ie_status_hist_w, ie_status_unidade_w)	,
				0,
				0,
				0,
				0,
				0,
				0,
				qt_leito_livre_w, 
				null,
				0,
				0,
				0,
				ie_clinica_padrao_w, 0,
				cd_tipo_acomodacao_w,
				0, cd_cid_principal_w,
				ie_temp_w,
				cd_motivo_alta_w,
				sysdate
		from	dual);
		end;
	end if;
	end;
END LOOP;
CLOSE C01;
COMMIT;

/* Atualizar campo qt_base_ocup   */
update	hsp_ocupacao_hospitalar
set	qt_base_ocup		= decode(ie_temp,'N',1,
					decode(nr_pacientes,0,0,1))
where	dt_referencia		= dt_parametro_w
and	cd_estabelecimento	= cd_estabelecimento_w
and	ie_periodo 		= 'D';
COMMIT;

/* Excluir os Registros mensais   */
--delete from hsp_ocupacao_hospitalar
--where dt_referencia = dt_parametro_mes_w
 -- and ie_periodo = 'M';
--COMMIT;
/* Acertar valores nulos da base do mes */
update	hsp_ocupacao_hospitalar
set	cd_convenio	= nvl(cd_convenio,0),
	ie_clinica_alta	= nvl(ie_clinica_alta, ie_clinica_padrao_w),
	hr_alta		= nvl(hr_alta,0)
where	dt_referencia	between dt_parametro_mes_w
	and	add_months(dt_parametro_mes_w,1)
  and ie_periodo = 'D';
commit;
/* Gerar Acumulado Mensal  */
OPEN C09;
LOOP
FETCH C09 into 
	cd_estabelecimento_w		,
	cd_setor_atendimento_w  	,        
	cd_unidade_basica_w     	,
	cd_unidade_compl_w		,
	ie_situacao_w			,
	cd_pessoa_fisica_w		,
	nr_admissoes_w          	,
	nr_altas_w              	,
	nr_obitos_w 	   		,
	nr_transf_entrada_w     	,
	nr_transf_Saida_w       	,
	nr_pacientes_w          	,
	ie_clinica_w            	,
	cd_convenio_w           	,
	qt_leito_livre_w		,
	cd_tipo_acomodacao_w      	,
	ie_tipo_atendimento_w	,
	cd_procedencia_w		, 
	cd_medico_w			,
	hr_alta_w			,
	qt_obito_p24h_w		,
	ie_clinica_alta_w,
	nr_seq_origem_w,
	qt_admitido_alta_dia_w,
	cd_cid_principal_w,
	ie_temp_w,
	qt_base_ocup_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_motivo_alta_w		,
	nr_seq_queixa_w,
	cd_categoria_w			,
	nr_seq_forma_chegada_w,
	dt_entrada_w,
	nr_seq_indicacao_w;
EXIT WHEN C09%NOTFOUND;
insert into hsp_ocupacao_hospitalar( 
	cd_estabelecimento	,
	dt_referencia   	,     
	ie_periodo              ,
	cd_setor_atendimento  	,        
	cd_unidade_basica     	,
	cd_unidade_compl	,
	ie_situacao		,
	nr_admissoes          	,
	nr_altas              	,
	nr_obitos 	        ,
	nr_transf_entrada     	,
	nr_transf_Saida       	,
	nr_pacientes          	,
	ie_clinica            	,
	cd_convenio           	,
	qt_leito_livre		,
	cd_tipo_acomodacao      	,
	ie_tipo_atendimento		,
	cd_procedencia		, 
	cd_medico			,
	hr_alta			,
	qt_obito_apos24h		,
	ie_clinica_alta,
	cd_pessoa_fisica,
	nr_seq_origem,
	qt_admitido_alta_dia,
	cd_cid_principal,
	ie_temp,
	qt_base_ocup,
	cd_procedimento,
	ie_origem_proced,
	cd_motivo_alta		,
	nr_seq_queixa,
	cd_categoria		,
	nr_seq_forma_chegada,
	dt_entrada,
	nr_seq_indicacao)
values(
	cd_estabelecimento_w		,
	dt_Parametro_mes_w  		,     
	'M'              		,
	cd_setor_atendimento_w  	,        
	cd_unidade_basica_w     	,
	cd_unidade_compl_w		,
	ie_situacao_w			,
	nr_admissoes_w          	,
	nr_altas_w              	,
	nr_obitos_w 	   		,
	nr_transf_entrada_w     	,
	nr_transf_Saida_w       	,
	nr_pacientes_w          	,
	ie_clinica_w            	,
	cd_convenio_w           	,
	qt_leito_livre_w		,
	cd_tipo_acomodacao_w      	,
	ie_tipo_atendimento_w	,
	cd_procedencia_w		, 
	cd_medico_w			,
	hr_alta_w			,
	qt_obito_p24h_w		,
	ie_clinica_alta_w,
	cd_pessoa_fisica_w,
	nr_seq_origem_w,
	qt_admitido_alta_dia_w,
	cd_cid_principal_w,
	ie_temp_w,
	qt_base_ocup_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_motivo_alta_w		,	
	nr_seq_queixa_w,
	cd_categoria_w			,
	nr_seq_forma_chegada_w,
	dt_entrada_w,
	nr_seq_indicacao_w);
END LOOP;
CLOSE C09;
COMMIT;
--select obter_tablespace_index
--into	ds_tablespace_w
--from	dual;
--obter_valor_dinamico('Alter index EISOCU_FK_I' ||
  --         	' Rebuild tablespace ' || ds_tablespace_w, ds_retorno_w);
--Insert into Log_Tasy values (sysdate, 'Tasy', 602, 'Ocupacao Hospitalar');
/* create table hsp_ocupacao_hospitalar (DT_REFERENCIA DATE NOT NULL, CD_SETOR_ATENDIMENTO NUMBER(5) NOT NULL, CD_UNIDADE_BASICA VARCHAR2(10) NOT NULL, 
  CD_UNIDADE_COMPL VARCHAR2(10), IE_SITUACAO VARCHAR2(2) NOT NULL, NR_ADMISSOES NUMBER(9) NOT NULL, NR_ALTAS NUMBER(9) NOT NULL, NR_OBITOS NUMBER(9) NOT NULL,  
 NR_TRANSF_ENTRADA  NUMBER(9) NOT NULL, NR_TRANSF_SAIDA NUMBER(9) NOT NULL, NR_PACIENTES NUMBER(9) NOT NULL, IE_CLINICA NUMBER(5),
 CD_CONVENIO NUMBER(5), CD_TIPO_ACOMODACAO NUMBER(4), CD_PESSOA_FISICA VARCHAR2(10), CD_ESTABELECIMENTO NUMBER(4) NOT NULL,
 IE_PERIODO VARCHAR2(1), QT_LEITO_LIVRE NUMBER(9), CD_PROCEDENCIA NUMBER(5), IE_TIPO_ATENDIMENTO NUMBER(3), CD_MEDICO VARCHAR2(10),
 HR_ALTA NUMBER(2), QT_OBITO_APOS24H NUMBER(9), IE_CLINICA_ALTA VARCHAR2(3), NR_SEQ_ORIGEM NUMBER(10), QT_ADMITIDO_ALTA_DIA NUMBER(9),
 CD_CID_PRINCIPAL VARCHAR2(10), DT_FIM_MES DATE, IE_TEMP VARCHAR2(1), QT_BASE_OCUP NUMBER(15), CD_PROCEDIMENTO NUMBER(15), IE_ORIGEM_PROCED NUMBER(10),
 CD_MOTIVO_ALTA NUMBER(3), NR_SEQ_QUEIXA NUMBER(10), CD_CATEGORIA VARCHAR2(10), NR_SEQ_FORMA_CHEGADA NUMBER(10), DT_ENTRADA DATE,
 NR_SEQ_INDICACAO NUMBER, CD_CONVENIO_GLOSA NUMBER(5), CD_CATEGORIA_GLOSA VARCHAR2(100))*/

END hsp_Gerar_Ocupacao_Hospitalar;
/
