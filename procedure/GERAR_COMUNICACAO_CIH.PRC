create or replace
procedure gerar_comunicacao_cih ( 	cd_profissional_p	varchar2,
					nr_seq_tipo_p		number,
					ds_comunicado_p		varchar2,
					nr_prescricao_p		number,
					nr_seq_prescricao_p	number,
					nm_usuario_p		Varchar2) is 

ds_Comunicado_w 		varchar2(32500);
ds_Comunicado_CIH_w 	varchar2(32500):= '';

begin

ds_Comunicado_w := ds_Comunicado_p;

/*if (instr(ds_Comunicado_w,'{\rtf') = 0) then	
	ds_Comunicado_w := wheb_rtf_pck.get_cabecalho ||chr(13)||chr(10)|| replace(ds_comunicado_w, chr(13)||chr(10), chr(13)||chr(10)|| wheb_rtf_pck.get_quebra_linha) ||chr(13)||chr(10)|| wheb_rtf_pck.get_rodape;	
end if;
*/

if (obter_parametro_funcao(896, 71, nm_usuario_p) = 'S') then
	ds_Comunicado_CIH_w:= SUBSTR(obter_reprov_lib_comunic_cih(nr_prescricao_p, nr_seq_prescricao_p,nm_usuario_p),1,4000) || CHR(13) || CHR(10) || CHR(13) || CHR(10);
	ds_Comunicado_w:= wheb_rtf_pck.get_cabecalho || chr(13)|| chr(10)|| replace(ds_Comunicado_CIH_w, chr(13)||chr(10), chr(13)||chr(10)|| wheb_rtf_pck.get_quebra_linha) || replace(ds_comunicado_w, chr(13)||chr(10), chr(13)||chr(10)|| wheb_rtf_pck.get_quebra_linha) ||chr(13)||chr(10)|| wheb_rtf_pck.get_rodape;
else 
	if (instr(ds_Comunicado_w,'{\rtf') = 0) then	
		ds_Comunicado_w := wheb_rtf_pck.get_cabecalho ||chr(13)||chr(10)|| replace(ds_comunicado_w, chr(13)||chr(10), chr(13)||chr(10)|| wheb_rtf_pck.get_quebra_linha) ||chr(13)||chr(10)|| wheb_rtf_pck.get_rodape;
	end if;
end if;

	insert into 	prescr_mat_comunic_cih
			(dt_registro,
			cd_profissional,
			nr_seq_tipo,
			ds_comunicado,
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nr_sequencia,
			nr_prescricao,
			nr_seq_prescricao)
	values		(sysdate,
			cd_profissional_p,
			nr_seq_tipo_p,
			nvl(ds_Comunicado_w, ' '), 
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			sysdate,
			prescr_mat_comunic_cih_seq.nextval,
			nr_prescricao_p,
			nr_seq_prescricao_p);

commit;

end GERAR_COMUNICACAO_CIH;
/
