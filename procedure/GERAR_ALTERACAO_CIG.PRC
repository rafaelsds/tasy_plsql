create or replace
procedure gerar_alteracao_cig	(nr_seq_cig_p		number,
					ie_evento_p		number,
					ds_observacao_p	varchar2,
					nm_usuario_p		varchar2) is

nr_sequencia_w		number(10,0);
dt_atualizacao_w	date := sysdate;
dt_reinicio_w		date;

begin
if	(nr_seq_cig_p is not null) and
	(ie_evento_p is not null) and
	(ds_observacao_p is not null) and
	(nm_usuario_p is not null) then
	/* obter sequencia */
	select	atendimento_cig_evento_seq.nextval
	into	nr_sequencia_w
	from	dual;

	/* atualizar data evento (gerar evento 'reiniciar' antes do evento medicao) */
	if	(ie_evento_p = 6) then
		select	max(dt_evento - 1/86400)
		into	dt_reinicio_w
		from	atendimento_cig_evento
		where	nr_seq_cig = nr_seq_cig_p;

		if	(dt_reinicio_w is not null) then
			dt_atualizacao_w := dt_reinicio_w;
		end if;
	end if;

	/* gerar evento */
	insert into atendimento_cig_evento	(
						nr_sequencia,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_atualizacao,
						nm_usuario,
						nr_seq_cig,
						dt_evento,
						ie_evento,
						cd_pessoa_evento,
						ds_observacao
						)
					values	(
						nr_sequencia_w,
						dt_atualizacao_w,
						nm_usuario_p,
						dt_atualizacao_w,
						nm_usuario_p,
						nr_seq_cig_p,
						dt_atualizacao_w,
						ie_evento_p,
						obter_dados_usuario_opcao(nm_usuario_p, 'C'),
						ds_observacao_p
						);
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_alteracao_cig;
/
