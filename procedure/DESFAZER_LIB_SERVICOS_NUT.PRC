create or replace
procedure desfazer_lib_servicos_nut(	nr_seq_serv_dia_p	number,
					nm_usuario_p		Varchar2) is 

nr_seq_nut_serv_w	number(10);
nr_prescr_oral_w	number(14);
nr_prescr_jejum_w	number(14);
nr_prescr_compl_w	number(14);
nr_prescr_enteral_w	number(14);
nr_prescr_npt_adulta_w	number(14);
nr_prescr_npt_ped_w	number(14);
nr_prescr_npt_neo_w	number(14);
nr_prescr_leite_deriv_w	number(14);


Cursor C01 is
	
	SELECT	a.nr_sequencia
	FROM 	nut_atend_serv_dia a,
		nut_atend_serv_dia_rep b
	WHERE	b.nr_seq_serv_dia = a.nr_sequencia
	AND 	( b.nr_prescr_oral in (nr_prescr_oral_w, nr_prescr_jejum_w, nr_prescr_compl_w,nr_prescr_enteral_w, 
					nr_prescr_npt_adulta_w, nr_prescr_npt_ped_w, nr_prescr_npt_neo_w,nr_prescr_leite_deriv_w)
		OR b.nr_prescr_jejum in (nr_prescr_oral_w, nr_prescr_jejum_w, nr_prescr_compl_w,nr_prescr_enteral_w, 
					nr_prescr_npt_adulta_w, nr_prescr_npt_ped_w, nr_prescr_npt_neo_w,nr_prescr_leite_deriv_w)
		OR b.nr_prescr_compl in (nr_prescr_oral_w, nr_prescr_jejum_w, nr_prescr_compl_w,nr_prescr_enteral_w, 
					nr_prescr_npt_adulta_w, nr_prescr_npt_ped_w, nr_prescr_npt_neo_w,nr_prescr_leite_deriv_w)
		OR b.nr_prescr_enteral in (nr_prescr_oral_w, nr_prescr_jejum_w, nr_prescr_compl_w,nr_prescr_enteral_w, 
					nr_prescr_npt_adulta_w, nr_prescr_npt_ped_w, nr_prescr_npt_neo_w,nr_prescr_leite_deriv_w)
		OR b.nr_prescr_npt_adulta in (nr_prescr_oral_w, nr_prescr_jejum_w, nr_prescr_compl_w,nr_prescr_enteral_w, 
					nr_prescr_npt_adulta_w, nr_prescr_npt_ped_w, nr_prescr_npt_neo_w,nr_prescr_leite_deriv_w)
		OR b.nr_prescr_npt_ped in (nr_prescr_oral_w, nr_prescr_jejum_w, nr_prescr_compl_w,nr_prescr_enteral_w, 
					nr_prescr_npt_adulta_w, nr_prescr_npt_ped_w, nr_prescr_npt_neo_w,nr_prescr_leite_deriv_w)
		OR b.nr_prescr_npt_neo in (nr_prescr_oral_w, nr_prescr_jejum_w, nr_prescr_compl_w,nr_prescr_enteral_w, 
					nr_prescr_npt_adulta_w, nr_prescr_npt_ped_w, nr_prescr_npt_neo_w,nr_prescr_leite_deriv_w)
		OR b.nr_prescr_leite_deriv in (nr_prescr_oral_w, nr_prescr_jejum_w, nr_prescr_compl_w,nr_prescr_enteral_w, 
					nr_prescr_npt_adulta_w, nr_prescr_npt_ped_w, nr_prescr_npt_neo_w,nr_prescr_leite_deriv_w));        


begin

if (nr_seq_serv_dia_p is not null) then 

	SELECT  MAX(NVL(nr_prescr_oral,0)),         
		MAX(NVL(nr_prescr_jejum,0)),       
		MAX(NVL(nr_prescr_compl,0)),       
		MAX(NVL(nr_prescr_enteral,0)),      
		MAX(NVL(nr_prescr_npt_adulta,0)), 
		MAX(NVL(nr_prescr_npt_ped,0)),      
		MAX(NVL(nr_prescr_npt_neo,0)),      
		MAX(NVL(nr_prescr_leite_deriv,0))
	into	nr_prescr_oral_w,
		nr_prescr_jejum_w,
		nr_prescr_compl_w,
		nr_prescr_enteral_w,
		nr_prescr_npt_adulta_w,
		nr_prescr_npt_ped_w,
		nr_prescr_npt_neo_w,
		nr_prescr_leite_deriv_w
	from 	nut_atend_serv_dia_rep a
	where	nr_seq_serv_dia = nr_seq_serv_dia_p;
	

	open C01;
	loop
	fetch C01 into	
		nr_seq_nut_serv_w ;
	exit when C01%notfound;
		begin
		Liberar_Servico_Nutricao( nr_seq_nut_serv_w, nm_usuario_p, 'D');
		end;
	end loop;
	close C01;
end if;
	
commit;

end desfazer_lib_servicos_nut;
/
