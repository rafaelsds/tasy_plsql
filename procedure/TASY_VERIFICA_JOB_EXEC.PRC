create or replace
procedure tasy_verifica_job_exec(
			nm_usuario_p		Varchar2) is 

nr_seq_atualizacao_w	number(10);
ie_exec_job_w			varchar2(1);
vl_job_queue_process_w	number(10);
qt_min_total_geral_w	varchar2(30);
job_w					number(10);
comando_w				varchar2(255);
dt_atualizacao_w		date;

cursor	c01 is
	select	distinct(job),
			comando
	from	job_v
	where	prox_exec 	between to_char(dt_atualizacao_w, 'dd/mm/yy hh24:mi:ss') and to_char(sysdate, 'dd/mm/yy hh24:mi:ss');

begin

	select	max(nr_sequencia)
	into	nr_seq_atualizacao_w
	from	atualizacao_versao;
	
	select	ie_exec_job,
			vl_job_queue_process
	into	ie_exec_job_w,
			vl_job_queue_process_w
	from	atualizacao_versao
	where	nr_sequencia = nr_seq_atualizacao_w;
	
	select	dt_atualizacao
	into	dt_atualizacao_w
	from 	atualizacao_versao
	where	nr_sequencia = nr_seq_atualizacao_w;
	
	if	(ie_exec_job_w = 'S') then
		begin
		
		open c01;
		loop
		fetch c01 into
			job_w,
			comando_w;
		exit when c01%notfound;
			begin
			insert	into tasy_jobs_atualizacao(	nr_sequencia,
												nr_job,
												dt_atualizacao,
												nm_usuario,
												dt_atualizacao_nrec,
												nm_usuario_nrec,
												ds_comando,
												nr_seq_atualizacao,
												ie_executado)
										values(	nr_seq_atualizacao_w + 1,
												job_w,
												sysdate,
												nm_usuario_p,
												sysdate,
												nm_usuario_p,
												comando_w,
												nr_seq_atualizacao_w,
												'N');
			end;
		end loop;
		end;
	end if;
commit;
end tasy_verifica_job_exec;
/