Create or Replace
Procedure Man_Calcular_Preco_Atividade(	nr_sequencia_p	Number) is


cd_setor_w				Number(05,0);
dt_atividade_w				Date;
ie_calcula_valor_w				Varchar2(1);
nm_usuario_w				Varchar2(15);
nr_seq_controle_w				Number(10,0);
nr_seq_funcao_w				Number(10,0);
vl_preco_w				Number(15,2)	:= 0;
vl_venda_w				Number(15,2)	:= 0;

CURSOR C01 IS
	select	vl_preco,
		vl_preco_venda
	from 	man_preco_funcao
	where	nr_seq_tipo_funcao	= nr_seq_funcao_w
	and	dt_vigencia	<= dt_atividade_w
	and	nvl(cd_setor_atendimento,cd_setor_w) = cd_setor_w
	order by	dt_vigencia;
	
BEGIN

begin
select	nvl(c.ie_calcula_valor,'N'),
	a.nr_seq_funcao,
	c.cd_setor,
	a.dt_atividade,
	a.nm_usuario_exec
into	ie_calcula_valor_w,
	nr_seq_funcao_w,
	cd_setor_w,
	dt_atividade_w,
	nm_usuario_w
from	man_localizacao c,
	man_Ordem_servico b,
	man_ordem_serv_ativ a
where	a.nr_seq_ordem_serv 	= b.nr_sequencia
and	b.nr_seq_localizacao	= c.nr_sequencia
and	a.nr_sequencia		= nr_sequencia_p;
exception
when others then
	ie_calcula_valor_w := 'N';
end;

if	(ie_calcula_valor_w = 'S') then
	begin
	OPEN C01;
	LOOP
	FETCH C01 into
		vl_preco_w,
		vl_venda_w;
	exit when c01%notfound;
		vl_preco_w		:= vl_preco_w;
	END LOOP;
	CLOSE C01;
	update	Man_ordem_serv_ativ
	set	vl_cobranca		= vl_preco_w * nvl(qt_minuto_cobr,0) / 60,
		vl_cobranca_venda		= vl_venda_w * nvl(qt_minuto_cobr,0) / 60
	where	nr_sequencia		= nr_sequencia_p;
	end;
end if;

select	max(nr_sequencia)
into	nr_seq_controle_w
from	usuario_controle
where	nm_usuario	= nm_usuario_w
and	dt_referencia	= trunc(dt_atividade_w,'dd');

if	(nvl(nr_seq_controle_w,0) > 0) then
	atualizar_ativ_usuario(nr_seq_controle_w);
end if;

END Man_Calcular_Preco_Atividade;
/