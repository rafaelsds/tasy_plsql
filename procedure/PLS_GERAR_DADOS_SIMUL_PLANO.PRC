create or replace
procedure pls_gerar_dados_simul_plano
		(	nr_seq_simul_perfil_p	number,
			nr_seq_tabela_p		number,
			nm_usuario_p		Varchar2) is 
			
nr_seq_plano_w			number(10);
qt_idade_inicial_w		number(10);
qt_idade_final_w		number(10);
qt_beneficiario_w		number(10);
qt_total_beneficiarios_w	number(10) := 0;
vl_preco_faixa_etaria_w		number(10,2) := 0;
vl_preco_original_w		number(10,2) := 0;
vl_preco_tot_original_w		number(10,2) := 0;
vl_tot_preco_faixa_w		number(15,2) := 0;
tx_indice_perfil_w		number(7,4);
			
Cursor C01 is
	select	qt_idade_inicial,
		qt_idade_final,
		qt_beneficiario
	from	pls_simulpreco_coletivo
	where	nr_seq_simul_perfil	= nr_seq_simul_perfil_p;

begin

select	max(nr_seq_plano)
into	nr_seq_plano_w
from	pls_tabela_preco
where	nr_sequencia	= nr_seq_tabela_p;

select	max(tx_indice)
into	tx_indice_perfil_w
from	pls_simulacao_perfil
where	nr_sequencia		= nr_seq_simul_perfil_p;

if	(tx_indice_perfil_w is null) or (tx_indice_perfil_w = 0) then
	tx_indice_perfil_w	:= 1;
end if;	

open C01;
loop
fetch C01 into	
	qt_idade_inicial_w,
	qt_idade_final_w,
	qt_beneficiario_w;
exit when C01%notfound;
	begin
	
	begin
	select	max(vl_preco_atual)
	into	vl_preco_faixa_etaria_w
	from	pls_plano_preco
	where	nr_seq_tabela		= nr_seq_tabela_p
	and	qt_idade_inicial	= qt_idade_inicial_w
	and	qt_idade_final		= qt_idade_final_w;
	exception
	when others then
		vl_preco_faixa_etaria_w	:= 0;
	end;
	
	if	(vl_preco_faixa_etaria_w is null) then
		vl_preco_faixa_etaria_w	:= 0;
	end if;
	
	vl_preco_original_w	:= vl_preco_faixa_etaria_w;
	vl_preco_tot_original_w	:= vl_preco_tot_original_w + vl_preco_original_w;
	
	vl_preco_faixa_etaria_w	:= vl_preco_faixa_etaria_w * tx_indice_perfil_w;
	
	insert into pls_simulacao_plano
		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			nr_seq_plano,nr_seq_tabela,nr_seq_simul_perfil,qt_idade_inicial,qt_idade_final,
			vl_preco_faixa,qt_vidas,vl_preco_original)
	values	(	pls_simulacao_plano_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			nr_seq_plano_w,nr_seq_tabela_p,nr_seq_simul_perfil_p,qt_idade_inicial_w,qt_idade_final_w,
			vl_preco_faixa_etaria_w,qt_beneficiario_w,vl_preco_original_w);
	
	vl_tot_preco_faixa_w		:= vl_tot_preco_faixa_w + vl_preco_faixa_etaria_w * qt_beneficiario_w;
	qt_total_beneficiarios_w	:= qt_total_beneficiarios_w + qt_beneficiario_w;
	
	end;
end loop;
close C01;

insert into pls_simulacao_plano
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		nr_seq_plano,nr_seq_tabela,nr_seq_simul_perfil,vl_preco_faixa,qt_vidas,
		qt_idade_final,qt_idade_inicial,vl_preco_original)
values	(	pls_simulacao_plano_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		nr_seq_plano_w,nr_seq_tabela_p,nr_seq_simul_perfil_p,vl_tot_preco_faixa_w,qt_total_beneficiarios_w,
		0,0,vl_preco_tot_original_w);

commit;

end pls_gerar_dados_simul_plano;
/
