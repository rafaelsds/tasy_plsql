create or replace
procedure INGOH_GERAR_MOVTO_UNIMED
				(nr_seq_retorno_p	in number) is

cd_usuario_convenio_w	varchar2(255);
cd_item_w		number(15);
ds_item_w		varchar2(255);
vl_pago_w		number(15,2);
vl_cobrado_w		number(15,2);
vl_glosa_w		number(15,2);
qt_paga_w		number(15,2);
nr_doc_convenio_w	varchar2(255);
cd_estabelecimento_w	number(10);
cd_convenio_w		number(10);
nr_interno_conta_w	number(10);

/* registros pagos */
cursor c01 is
select	substr(ds_conteudo,11,9) nr_doc_convenio,
	substr(ds_conteudo,36,20) cd_usuario,
	substr(ds_conteudo,100,9) cd_item,
	substr(ds_conteudo,109,24) ds_item,
	to_number(substr(ds_conteudo,133,10)) qt_paga,
	to_number(substr(ds_conteudo,144,12)) vl_pago
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,1,2)		= '22';

/* registros glosados */
cursor	c02 is
select	substr(ds_conteudo,11,9) nr_doc_convenio,
	substr(ds_conteudo,36,20) cd_usuario,
	substr(ds_conteudo,224,9) cd_item,
	to_number(substr(ds_conteudo,233,11)) qt_paga,
	somente_numero(substr(ds_conteudo,204,13)) / 100 vl_glosado
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,1,2)		= '41';
				
begin

select	cd_estabelecimento,
	cd_convenio
into	cd_estabelecimento_w,
	cd_convenio_w
from	convenio_retorno
where	nr_sequencia	= nr_seq_retorno_p;

open C01;
loop
fetch C01 into	
	nr_doc_convenio_w,
	cd_usuario_convenio_w,
	cd_item_w,
	ds_item_w,
	qt_paga_w,	
	vl_pago_w;
exit when C01%notfound;

	if	(nvl(cd_item_w,0) <> 0) then

		select	max(a.nr_interno_conta)
		into	nr_interno_conta_w
		from	conta_paciente_guia b,
			conta_paciente a
		where	a.nr_interno_conta	= b.nr_interno_conta
		and	b.cd_autorizacao	= nr_doc_convenio_w
		and	a.cd_estabelecimento	= cd_estabelecimento_w
		and	a.cd_convenio_parametro	= cd_convenio_w;
		
		insert into convenio_retorno_movto
			(nr_sequencia,
			nr_seq_retorno,
			nr_doc_convenio,		
			dt_atualizacao,
			nm_usuario,
			cd_item,
			ds_item_retorno,
			qt_paga,
			vl_pago,
			vl_total_pago,
			nr_conta,
			cd_usuario_convenio)
		values	(convenio_retorno_movto_seq.nextval,
			nr_seq_retorno_p,
			nr_doc_convenio_w,
			sysdate,
			'TASY',
			cd_item_w,
			ds_item_w,
			qt_paga_w,
			(vl_pago_w/100),
			(vl_pago_w/100),
			nr_interno_conta_w,
			cd_usuario_convenio_w);
	
	end if;

end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_doc_convenio_w,
	cd_usuario_convenio_w,
	cd_item_w,
	qt_paga_w,
	vl_pago_w;
exit when C02%notfound;

	if	(nvl(cd_item_w,0) <> 0) then

		select	max(a.nr_interno_conta)
		into	nr_interno_conta_w
		from	conta_paciente_guia b,
			conta_paciente a
		where	a.nr_interno_conta	= b.nr_interno_conta
		and	b.cd_autorizacao	= nr_doc_convenio_w
		and	a.cd_estabelecimento	= cd_estabelecimento_w
		and	a.cd_convenio_parametro	= cd_convenio_w;
		
		insert into convenio_retorno_movto
			(nr_sequencia,
			nr_seq_retorno,
			nr_doc_convenio,
			dt_atualizacao,
			nm_usuario,
			cd_item,
			qt_paga,
			vl_glosa,
			vl_cobrado,
			nr_conta,
			cd_usuario_convenio)
		values	(convenio_retorno_movto_seq.nextval,
			nr_seq_retorno_p,
			nr_doc_convenio_w,
			sysdate,
			'TASY',
			cd_item_w,
			0,
			(vl_pago_w/100),
			(vl_pago_w/100),
			nr_interno_conta_w,
			cd_usuario_convenio_w);
	
	end if;

end loop;
close C02;

delete	from w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end INGOH_GERAR_MOVTO_UNIMED;
/