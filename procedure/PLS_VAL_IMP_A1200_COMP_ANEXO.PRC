create or replace
procedure pls_val_imp_A1200_comp_anexo (	nr_seq_anexo_p		in pls_pacote_comp_anexo.nr_sequencia%type,
						nr_seq_composicao_p	in pls_pacote_composicao.nr_sequencia%type,
						nr_seq_prestador_p	in pls_prestador.nr_sequencia%type,
						nr_seq_regra_p          in pls_pacote_tipo_acomodacao.nr_sequencia%type,
						dt_vigencia_envio_p	in pls_pacote_comp_anexo.dt_vigencia_envio%type,
						ie_tipo_anexo_p		in pls_pacote_comp_anexo.ie_tipo_anexo%type) is

qt_registro_w	pls_integer;
nr_seq_anexo_w	pls_pacote_comp_anexo.nr_sequencia%type;	
begin

nr_seq_anexo_w := nvl(nr_seq_anexo_p,-1);
qt_registro_w := 0;

-- valida o prestador
if	(qt_registro_w = 0) and (nr_seq_prestador_p is not null) and (ie_tipo_anexo_p = '4') then

	select	count(1)
	into	qt_registro_w
	from	pls_pacote_comp_anexo		a
	where	a.nr_seq_pacote_composicao	= nr_seq_composicao_p
	and	a.nr_seq_prestador		= nr_seq_prestador_p
	and	a.ie_situacao			= 'S'
	and	a.ie_tipo_anexo			= '4'
	and	a.nr_sequencia			!= nr_seq_anexo_w
	and	a.dt_vigencia_envio		between trunc(dt_vigencia_envio_p, 'MONTH') and fim_dia(last_day(dt_vigencia_envio_p));

	
end if;


-- valida a regra
if	(qt_registro_w = 0) and (nr_seq_regra_p is not null) and (ie_tipo_anexo_p = '4') then

	select	count(1)
	into	qt_registro_w
	from	pls_pacote_comp_anexo		a
	where	a.nr_seq_pacote_composicao	= nr_seq_composicao_p
	and	a.nr_seq_tipo_acomodacao	= nr_seq_regra_p
	and	a.ie_situacao			= 'S'
	and	a.ie_tipo_anexo			= '4'
	and	a.nr_sequencia			!= nr_seq_anexo_w
	and	a.dt_vigencia_envio		between trunc(dt_vigencia_envio_p, 'MONTH') and fim_dia(last_day(dt_vigencia_envio_p));

end if;


if	(qt_registro_w = 0) and (nr_seq_regra_p is null) and (nr_seq_prestador_p is null) and (ie_tipo_anexo_p = '4') then

	select	count(1)
	into	qt_registro_w
	from	pls_pacote_comp_anexo		a
	where	a.nr_seq_pacote_composicao	= nr_seq_composicao_p
	and	a.ie_situacao			= 'S'
	and	a.nr_sequencia			!= nr_seq_anexo_w
	and	a.ie_tipo_anexo			= '4'
	and	a.nr_seq_tipo_acomodacao	is null
	and	a.nr_seq_prestador		is null
	and	a.dt_vigencia_envio		between trunc(dt_vigencia_envio_p, 'MONTH') and fim_dia(last_day(dt_vigencia_envio_p));

end if;



if	(qt_registro_w > 0) then

	wheb_mensagem_pck.exibir_mensagem_abort(1124282);
end if;


end pls_val_imp_A1200_comp_anexo;
/

