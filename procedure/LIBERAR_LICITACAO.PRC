create or replace
procedure liberar_licitacao(	nr_seq_licitacao_p		number,
			nm_usuario_p		Varchar2) is 

qt_itens_w	number(5);

begin

select	count(*)
into	qt_itens_w
from	reg_lic_item
where	nr_seq_licitacao	= nr_seq_licitacao_p;
if	(qt_itens_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(266167);
	--'� necess�rio existir itens na licita��o');
end if;

update	reg_licitacao
set	dt_liberacao	= sysdate,
	nm_usuario_lib	= nm_usuario_p
where	nr_sequencia	= nr_seq_licitacao_p;

insert into reg_lic_historico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_historico,
	ds_observacao,
	nr_seq_licitacao)
values(	reg_lic_historico_seq.nextval,
	sysdate,
	nm_usuario_p,
	'L',
	WHEB_MENSAGEM_PCK.get_texto(310352), --'Libera��o da licita��o.',
	nr_seq_licitacao_p);
	
commit;

end liberar_licitacao;
/

