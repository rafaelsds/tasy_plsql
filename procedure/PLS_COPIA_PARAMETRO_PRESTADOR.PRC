create or replace
procedure pls_copia_parametro_prestador (	nr_seq_pres_origem_p		number,
						nr_seq_pres_destino_p		number,
						nm_tabela_p			varchar2,
						ie_excluir_p			varchar2,
						nm_usuario_p			varchar2) is
						
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Copiar as informacoes das tabelas de um prestadore de origem para um prestador
 de destino
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ] Objetos do dicionario [ X ] Tasy (Delphi/Java) [ ] Portal [ ] Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
	OPS - Prestadores
	Triggers sobre as tabelas utilizadas nessa rotina
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_fisica_w		varchar2(15);
qt_registro_w			number(10);

Cursor C01 is
	select	nr_sequencia nr_seq_prest_medico,
		pls_prestador_medico_seq.nextval nr_seq_regra_med
	from	pls_prestador_medico
	where	nr_seq_prestador = nr_seq_pres_origem_p;
	
begin
dbms_application_info.SET_ACTION('PLS_COPIA_PARAMETRO_PRESTADOR'); 

if	(nm_tabela_p = 'PLS_PRESTADOR_PROC') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_proc
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	begin
		insert into pls_prestador_proc
			(nr_sequencia,				dt_atualizacao,				nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador,
			cd_area_procedimento,			cd_especialidade_proc,			cd_grupo_proc,
			cd_procedimento,			ie_origem_proced,			ie_liberar,
			nr_seq_prestador_prot,			nr_seq_prestador_exec,			ie_tipo_guia,
			dt_inicio_vigencia,			dt_fim_vigencia,			nr_seq_grupo_serv,
			nr_seq_grupo_produto,			cd_prestador_princ,			ie_carater_internacao,
			ie_internado,				nr_seq_plano,				nr_seq_tipo_atendimento,
			nr_seq_tipo_prestador,			cd_especialidade,			nr_seq_cbo_saude,
			ds_observacao,				ds_regra,				nr_seq_tipo_prest_prot)
		select	pls_prestador_proc_seq.nextval,		sysdate,				nm_usuario_p,
			sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
			cd_area_procedimento,			cd_especialidade_proc,			cd_grupo_proc,
			cd_procedimento,			ie_origem_proced,			ie_liberar,
			nr_seq_prestador_prot,			nr_seq_prestador_exec,			ie_tipo_guia,
			dt_inicio_vigencia,			dt_fim_vigencia,			nr_seq_grupo_serv,
			nr_seq_grupo_produto,			cd_prestador_princ,			ie_carater_internacao,
			ie_internado,				nr_seq_plano,				nr_seq_tipo_atendimento,
			nr_seq_tipo_prestador,			cd_especialidade,			nr_seq_cbo_saude,
			ds_observacao,				ds_regra,				nr_seq_tipo_prest_prot
		from	pls_prestador_proc
		where	nr_seq_prestador = nr_seq_pres_origem_p;
	exception
	when dup_val_on_index then
		Wheb_mensagem_pck.exibir_mensagem_abort(214251);
	end;
end if;

if	(nm_tabela_p = 'PLS_REGRA_PRECO_PROC') then
	if	(ie_excluir_p = 'S') then
		delete	pls_regra_preco_proc
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_regra_preco_proc
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador,
		nr_seq_plano,				nr_seq_categoria,			nr_seq_clinica,
		nr_seq_tipo_acomodacao,			nr_seq_tipo_atendimento,		cd_edicao_amb,
		cd_procedimento,			ie_origem_proced,			cd_area_procedimento,
		cd_especialidade,			cd_grupo_proc,				tx_ajuste_geral,
		vl_ch_honorarios,			vl_ch_custo_oper,			vl_filme,
		tx_ajuste_custo_oper,			tx_ajuste_partic,			vl_proc_negociado,
		ie_situacao,				dt_inicio_vigencia,			cd_estabelecimento,
		dt_fim_vigencia,			tx_ajuste_filme,			tx_ajuste_ch_honor,
		ie_tipo_tabela,				nr_seq_outorgante,			nr_seq_contrato,
		nr_seq_congenere,			cd_convenio,				cd_categoria,
		ie_preco_informado,			vl_medico,				vl_auxiliares,
		vl_anestesista,				vl_custo_operacional,			ie_preco,
		vl_materiais,				ie_tipo_contratacao,			nr_seq_regra_ant,
		qt_dias_inter_inicio,			qt_dias_inter_final,			ie_tipo_intercambio,
		ie_franquia,				nr_seq_grupo_rec,			sg_uf_operadora_intercambio,
		nr_seq_setor_atend,			nr_seq_cbo_saude,			nr_seq_cbhpm_edicao,
		ie_tecnica_utilizada,			ie_tipo_guia,				nr_seq_tipo_acomod_prod,
		ie_acomodacao,				cd_moeda_ch_medico,			cd_moeda_ch_co, 
		ie_tipo_vinculo,			nr_seq_classificacao,			nr_seq_grupo_prestador,
		nr_seq_grupo_produto,			nr_seq_grupo_contrato,			ie_tipo_segurado,
		ds_observacao,				nr_seq_grupo_servico,			cd_moeda_filme,
		cd_moeda_anestesista,			ie_internado,				ie_cooperado,
		nr_seq_tipo_prestador,			nr_seq_ops_congenere,			ie_tipo_contrato,
		cd_prestador,				ie_autogerado,				ie_carater_internacao,
		ie_origem_procedimento,			ie_pcmso,				ie_taxa_coleta,
		ie_tipo_acomodacao_ptu,			nr_contrato,				nr_seq_congenere_prot,
		nr_seq_grupo_intercambio,		nr_seq_intercambio,			nr_seq_regra_proc_ref,
		ie_tipo_consulta,			nr_seq_grupo_med_exec,			ie_gerar_remido,
		cd_especialidade_prest,			cd_medico,				cd_moeda_tabela,
		cd_prestador_partic,			cd_prestador_prot,			cd_prestador_solic,
		cd_versao_tiss,				ds_regra,				ie_acomodacao_autorizada,
		ie_atend_pcmso,				ie_ch_padrao_anestesista,		ie_nao_gera_tx_inter,
		ie_origem_protocolo,			ie_proc_tabela,				ie_ref_guia_internacao,
		ie_tipo_atendimento,			ie_tipo_prestador,			ie_valor_autorizado,
		nr_seq_categoria_plano,			nr_seq_cbo_saude_solic,			nr_seq_cp_combinada,
		nr_seq_edicao_tuss,			nr_seq_grupo_operadora,			nr_seq_grupo_prest_int,
		nr_seq_mot_reembolso,			nr_seq_prest_inter,			nr_seq_regra_atend_cart,
		nr_seq_rp_combinada,			nr_seq_tipo_atend_princ,		nr_seq_tipo_prestador_prot,
		qt_idade_final,				qt_idade_inicial)
	select	pls_regra_preco_proc_seq.nextval,	sysdate,				nm_usuario_p, 
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p, 
		nr_seq_plano,				nr_seq_categoria,			nr_seq_clinica, 
		nr_seq_tipo_acomodacao,			nr_seq_tipo_atendimento,		cd_edicao_amb, 
		cd_procedimento,			ie_origem_proced,			cd_area_procedimento, 
		cd_especialidade,			cd_grupo_proc,				tx_ajuste_geral, 
		vl_ch_honorarios,			vl_ch_custo_oper,			vl_filme, 
		tx_ajuste_custo_oper,			tx_ajuste_partic,			vl_proc_negociado, 
		ie_situacao,				dt_inicio_vigencia,			cd_estabelecimento, 
		dt_fim_vigencia,			tx_ajuste_filme,			tx_ajuste_ch_honor, 
		ie_tipo_tabela,				nr_seq_outorgante,			nr_seq_contrato, 
		nr_seq_congenere,			cd_convenio,				cd_categoria, 
		ie_preco_informado,			vl_medico,				vl_auxiliares,
		vl_anestesista,				vl_custo_operacional,			ie_preco,
		vl_materiais,				ie_tipo_contratacao,			nr_seq_regra_ant,
		qt_dias_inter_inicio,			qt_dias_inter_final,			ie_tipo_intercambio,
		ie_franquia,				nr_seq_grupo_rec,			sg_uf_operadora_intercambio,
		nr_seq_setor_atend,			nr_seq_cbo_saude,			nr_seq_cbhpm_edicao,
		ie_tecnica_utilizada,			ie_tipo_guia,				nr_seq_tipo_acomod_prod,
		ie_acomodacao,				cd_moeda_ch_medico,			cd_moeda_ch_co, 
		ie_tipo_vinculo,			nr_seq_classificacao,			nr_seq_grupo_prestador,
		nr_seq_grupo_produto,			nr_seq_grupo_contrato,			ie_tipo_segurado,
		ds_observacao,				nr_seq_grupo_servico,			cd_moeda_filme,
		cd_moeda_anestesista,			ie_internado,				ie_cooperado,
		nr_seq_tipo_prestador,			nr_seq_ops_congenere,			ie_tipo_contrato,
		cd_prestador,				ie_autogerado,				ie_carater_internacao,
		ie_origem_procedimento,			ie_pcmso,				ie_taxa_coleta,
		ie_tipo_acomodacao_ptu,			nr_contrato,				nr_seq_congenere_prot,
		nr_seq_grupo_intercambio,		nr_seq_intercambio,			nr_seq_regra_proc_ref,
		ie_tipo_consulta,			nr_seq_grupo_med_exec,			ie_gerar_remido,
		cd_especialidade_prest,			cd_medico,				cd_moeda_tabela,
		cd_prestador_partic,			cd_prestador_prot,			cd_prestador_solic,
		cd_versao_tiss,				ds_regra,				ie_acomodacao_autorizada,
		ie_atend_pcmso,				ie_ch_padrao_anestesista,		ie_nao_gera_tx_inter,
		ie_origem_protocolo,			ie_proc_tabela,				ie_ref_guia_internacao,
		ie_tipo_atendimento,			ie_tipo_prestador,			ie_valor_autorizado,
		nr_seq_categoria_plano,			nr_seq_cbo_saude_solic,			nr_seq_cp_combinada,
		nr_seq_edicao_tuss,			nr_seq_grupo_operadora,			nr_seq_grupo_prest_int,
		nr_seq_mot_reembolso,			nr_seq_prest_inter,			nr_seq_regra_atend_cart,
		nr_seq_rp_combinada,			nr_seq_tipo_atend_princ,		nr_seq_tipo_prestador_prot,
		qt_idade_final,				qt_idade_inicial
	from	pls_regra_preco_proc
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_REGRA_PRECO_MAT') then
	if	(ie_excluir_p = 'S') then
		delete	pls_regra_preco_mat
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_regra_preco_mat
		(nr_sequencia,				cd_estabelecimento,			dt_atualizacao,
		nm_usuario,				dt_atualizacao_nrec,			nm_usuario_nrec,
		nr_seq_prestador,			dt_inicio_vigencia,			dt_fim_vigencia,
		tx_ajuste,				ie_situacao,				vl_negociado, 
		tx_ajuste_pfb,				tx_ajuste_pmc_neut,			tx_ajuste_pmc_pos,
		tx_ajuste_pmc_neg,			tx_ajuste_simpro_pfb,			tx_ajuste_simpro_pmc, 
		ie_origem_preco,			ie_tipo_despesa,			nr_seq_material_preco,
		tx_ajuste_tab_propria,			ie_tipo_tabela,				nr_seq_outorgante,
		nr_seq_contrato,			tx_ajuste_benef_lib,			nr_seq_congenere,
		nr_seq_regra_ant,			sg_uf_operadora_intercambio,		ie_tipo_intercambio,
		cd_material,				ds_observacao,				ie_tipo_vinculo,
		nr_seq_grupo_prestador,			nr_seq_classificacao,			ie_tipo_contratacao,
		ie_preco,				ie_tipo_segurado,			nr_seq_grupo_contrato,
		nr_seq_grupo_produto,			nr_seq_plano,				nr_seq_material,
		nr_seq_estrutura_mat,			ie_tabela_adicional,			cd_categoria,
		cd_convenio,				cd_prestador,				dt_base_fixo,
		ie_generico_unimed,			ie_internado,				ie_mat_autorizacao_esp,
		ie_pcmso,				ie_tipo_acomodacao_ptu,			ie_tipo_guia,
		ie_tipo_preco_brasindice,		ie_tipo_preco_simpro,			nr_contrato,
		nr_seq_categoria,			nr_seq_clinica,				nr_seq_congenere_prot,
		nr_seq_grupo_material,			nr_seq_grupo_operadora,			nr_seq_grupo_uf,
		nr_seq_tipo_acomodacao,			nr_seq_tipo_atendimento,		nr_seq_tipo_prestador,
		nr_seq_tipo_uso,			qt_idade_inicial,			qt_idade_final,
		ie_gerar_remido,			cd_especialidade_prest,			cd_prestador_prot,
		cd_prestador_solic,			cd_versao_tiss,				ds_negociacao,
		ds_regra,				ie_acomodacao_autorizada,		ie_atend_pcmso,
		ie_cooperado,				ie_med_oncologico,			ie_nao_gera_tx_inter,
		ie_origem_protocolo,			ie_ref_guia_internacao,			ie_restringe_hosp,
		ie_tipo_ajuste_pfb,			ie_tipo_atendimento,			ie_tipo_preco_tab_adic,
		ie_tipo_prestador,			ie_ult_valor_fabrica,			ie_utilizar_duas_casas,
		nr_seq_cp_combinada,			nr_seq_grupo_ajuste,			nr_seq_grupo_prest_int,
		nr_seq_intercambio,			nr_seq_mot_reembolso,			nr_seq_prestador_inter,
		nr_seq_regra_atend_cart,		nr_seq_regra_mat_ref,			nr_seq_tipo_atend_princ,
		nr_seq_tipo_prestador_prot,		pr_icms,				vl_pfb_nao_aplicavel_simpro,
		vl_pfb_negativa_brasindice,		vl_pfb_negativa_simpro,			vl_pfb_neutra_brasindice,
		vl_pfb_neutra_simpro,			vl_pfb_positiva_brasindice,		vl_pfb_positiva_simpro)
	select	pls_regra_preco_mat_seq.nextval,	cd_estabelecimento,			sysdate,
		nm_usuario_p,				sysdate,				nm_usuario_p, 
		nr_seq_pres_destino_p,			dt_inicio_vigencia,			dt_fim_vigencia,
		tx_ajuste,				ie_situacao,				vl_negociado,
		tx_ajuste_pfb,				tx_ajuste_pmc_neut,			tx_ajuste_pmc_pos,
		tx_ajuste_pmc_neg,			tx_ajuste_simpro_pfb,			tx_ajuste_simpro_pmc, 
		ie_origem_preco,			ie_tipo_despesa,			nr_seq_material_preco,
		tx_ajuste_tab_propria,			ie_tipo_tabela,				nr_seq_outorgante,
		nr_seq_contrato,			tx_ajuste_benef_lib,			nr_seq_congenere,
		nr_seq_regra_ant,			sg_uf_operadora_intercambio,		ie_tipo_intercambio,
		cd_material,				ds_observacao,				ie_tipo_vinculo,
		nr_seq_grupo_prestador,			nr_seq_classificacao,			ie_tipo_contratacao,
		ie_preco,				ie_tipo_segurado,			nr_seq_grupo_contrato,
		nr_seq_grupo_produto,			nr_seq_plano,				nr_seq_material,
		nr_seq_estrutura_mat,			ie_tabela_adicional,			cd_categoria,
		cd_convenio,				cd_prestador,				dt_base_fixo,
		ie_generico_unimed,			ie_internado,				ie_mat_autorizacao_esp,
		ie_pcmso,				ie_tipo_acomodacao_ptu,			ie_tipo_guia,
		ie_tipo_preco_brasindice,		ie_tipo_preco_simpro,			nr_contrato,
		nr_seq_categoria,			nr_seq_clinica,				nr_seq_congenere_prot,
		nr_seq_grupo_material,			nr_seq_grupo_operadora,			nr_seq_grupo_uf,
		nr_seq_tipo_acomodacao,			nr_seq_tipo_atendimento,		nr_seq_tipo_prestador,
		nr_seq_tipo_uso,			qt_idade_inicial,			qt_idade_final,
		ie_gerar_remido,			cd_especialidade_prest,			cd_prestador_prot,
		cd_prestador_solic,			cd_versao_tiss,				ds_negociacao,
		ds_regra,				ie_acomodacao_autorizada,		ie_atend_pcmso,
		ie_cooperado,				ie_med_oncologico,			ie_nao_gera_tx_inter,
		ie_origem_protocolo,			ie_ref_guia_internacao,			ie_restringe_hosp,
		ie_tipo_ajuste_pfb,			ie_tipo_atendimento,			ie_tipo_preco_tab_adic,
		ie_tipo_prestador,			ie_ult_valor_fabrica,			ie_utilizar_duas_casas,
		nr_seq_cp_combinada,			nr_seq_grupo_ajuste,			nr_seq_grupo_prest_int,
		nr_seq_intercambio,			nr_seq_mot_reembolso,			nr_seq_prestador_inter,
		nr_seq_regra_atend_cart,		nr_seq_regra_mat_ref,			nr_seq_tipo_atend_princ,
		nr_seq_tipo_prestador_prot,		pr_icms,				vl_pfb_nao_aplicavel_simpro,
		vl_pfb_negativa_brasindice,		vl_pfb_negativa_simpro,			vl_pfb_neutra_brasindice,
		vl_pfb_neutra_simpro,			vl_pfb_positiva_brasindice,		vl_pfb_positiva_simpro
	from	pls_regra_preco_mat
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_REGRA_PRECO_SERVICO') then
	if	(ie_excluir_p = 'S') then
		delete	pls_regra_preco_servico
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_regra_preco_servico
		(nr_sequencia,				cd_estabelecimento,			dt_atualizacao,
		nm_usuario,				dt_atualizacao_nrec,			nm_usuario_nrec,
		nr_seq_prestador,			dt_inicio_vigencia,			dt_fim_vigencia,
		cd_tabela_servico,			tx_ajuste,				cd_procedimento,
		ie_origem_proced,			ie_situacao,				cd_area_procedimento,
		cd_especialidade,			cd_grupo_proc,				vl_negociado,
		ie_tipo_tabela,				nr_seq_outorgante,			nr_seq_contrato,
		nr_seq_congenere,			nr_seq_regra_ant,			ie_tipo_vinculo,
		nr_seq_classificacao,			nr_seq_categoria,			ie_internado,
		nr_seq_grupo_contrato,			nr_seq_grupo_produto,			nr_seq_grupo_prestador,
		nr_seq_grupo_servico,			ie_tipo_segurado,			ds_observacao,
		ie_tipo_contratacao,			ie_preco,				nr_seq_plano,
		cd_moeda,				cd_prestador,				ie_acomodacao,
		ie_autogerado,				ie_carater_internacao,			ie_pcmso,
		ie_taxa_coleta,				ie_tipo_acomodacao_ptu,			ie_tipo_guia,
		ie_tipo_intercambio,			nr_contrato,				nr_seq_cbo_saude,
		nr_seq_clinica,				nr_seq_congenere_prot,			nr_seq_grupo_intercambio,
		nr_seq_grupo_rec,			nr_seq_intercambio,			nr_seq_referencia,
		nr_seq_serv_ref,			nr_seq_tipo_acomodacao,			nr_seq_tipo_atendimento,
		nr_seq_tipo_prestador,			tx_ajuste_preco,			qt_idade_inicial,
		qt_idade_final,				ie_gerar_remido,			cd_especialidade_prest,
		cd_prestador_prot,			cd_prestador_solic,			cd_versao_tiss,
		ds_regra,				ie_acomodacao_autorizada,		ie_atend_pcmso,
		ie_cooperado,				ie_nao_gera_tx_inter,			ie_origem_protocolo,
		ie_ref_guia_internacao,			ie_tipo_atendimento,			ie_tipo_prestador,
		ie_valor_autorizado,			nr_seq_cp_combinada,			nr_seq_grupo_operadora,
		nr_seq_grupo_prest_int,			nr_seq_mot_reembolso,			nr_seq_prestador_inter,
		nr_seq_regra_atend_cart,		nr_seq_tipo_atend_princ,		nr_seq_tipo_prestador_prot)
	select	pls_regra_preco_servico_seq.nextval,	cd_estabelecimento,			sysdate,
		nm_usuario_p,				sysdate,				nm_usuario_p,
		nr_seq_pres_destino_p,			dt_inicio_vigencia,			dt_fim_vigencia,
		cd_tabela_servico,			tx_ajuste,				cd_procedimento,
		ie_origem_proced,			ie_situacao,				cd_area_procedimento,
		cd_especialidade,			cd_grupo_proc,				vl_negociado,
		ie_tipo_tabela,				nr_seq_outorgante,			nr_seq_contrato,
		nr_seq_congenere,			nr_seq_regra_ant,			ie_tipo_vinculo,
		nr_seq_classificacao,			nr_seq_categoria,			ie_internado,
		nr_seq_grupo_contrato,			nr_seq_grupo_produto,			nr_seq_grupo_prestador,
		nr_seq_grupo_servico,			ie_tipo_segurado,			ds_observacao,
		ie_tipo_contratacao,			ie_preco,				nr_seq_plano,
		cd_moeda,				cd_prestador,				ie_acomodacao,
		ie_autogerado,				ie_carater_internacao,			ie_pcmso,
		ie_taxa_coleta,				ie_tipo_acomodacao_ptu,			ie_tipo_guia,
		ie_tipo_intercambio,			nr_contrato,				nr_seq_cbo_saude,
		nr_seq_clinica,				nr_seq_congenere_prot,			nr_seq_grupo_intercambio,
		nr_seq_grupo_rec,			nr_seq_intercambio,			nr_seq_referencia,
		nr_seq_serv_ref,			nr_seq_tipo_acomodacao,			nr_seq_tipo_atendimento,
		nr_seq_tipo_prestador,			tx_ajuste_preco,			qt_idade_inicial,
		qt_idade_final,				ie_gerar_remido,			cd_especialidade_prest,
		cd_prestador_prot,			cd_prestador_solic,			cd_versao_tiss,
		ds_regra,				ie_acomodacao_autorizada,		ie_atend_pcmso,
		ie_cooperado,				ie_nao_gera_tx_inter,			ie_origem_protocolo,
		ie_ref_guia_internacao,			ie_tipo_atendimento,			ie_tipo_prestador,
		ie_valor_autorizado,			nr_seq_cp_combinada,			nr_seq_grupo_operadora,
		nr_seq_grupo_prest_int,			nr_seq_mot_reembolso,			nr_seq_prestador_inter,
		nr_seq_regra_atend_cart,		nr_seq_tipo_atend_princ,		nr_seq_tipo_prestador_prot
	from	pls_regra_preco_servico
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_AREA') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_area
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_prestador_area
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
		sg_estado,				cd_municipio_ibge,			nr_seq_regiao)
	select	pls_prestador_area_seq.nextval,		sysdate,				nm_usuario_p,
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
		sg_estado,				cd_municipio_ibge,			nr_seq_regiao
	from	pls_prestador_area
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_PLANO') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_plano
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_prestador_plano
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
		nr_seq_plano,				ie_situacao,				ie_permite,
		dt_inicio_vigencia,			dt_fim_vigencia,			ie_tipo_atendimento,
		ie_carater_internacao,			ds_observacao)
	select	pls_prestador_plano_seq.nextval,	sysdate,				nm_usuario_p,
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
		nr_seq_plano,				ie_situacao,				ie_permite,
		dt_inicio_vigencia,			dt_fim_vigencia,			ie_tipo_atendimento,
		ie_carater_internacao,			ds_observacao
	from	pls_prestador_plano
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_MEDICO') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_tributo
		where 	nr_seq_prestador_med in	(select	nr_sequencia
						from	pls_prestador_medico
						where	nr_seq_prestador = nr_seq_pres_destino_p)
		and	nr_seq_prestador_med is not null;
		
		delete	pls_prestador_medico
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	for r_c01_w in c01 loop
		insert into pls_prestador_medico
			(nr_sequencia,				dt_atualizacao,				nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
			cd_medico,				ie_situacao,				dt_inclusao,
			dt_exclusao,				ie_tipo_endereco,			ie_guia_medico,
			ie_tipo_complemento,			ie_tipo_vinculo,			nr_seq_tipo_prestador,
			nr_seq_prestador_pgto,			nr_seq_prestador_ref,			ie_residencia_saude,
			nm_busca_guia_medico,			nr_referencia_end)
		select	r_c01_w.nr_seq_regra_med,		sysdate,				nm_usuario_p,
			sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
			cd_medico,				ie_situacao,				dt_inclusao,
			dt_exclusao,				ie_tipo_endereco,			ie_guia_medico,
			ie_tipo_complemento,			ie_tipo_vinculo,			nr_seq_tipo_prestador,
			nr_seq_prestador_pgto,			nr_seq_prestador_ref,			ie_residencia_saude,
			nm_busca_guia_medico,			nr_referencia_end
		from	pls_prestador_medico
		where	nr_sequencia = r_c01_w.nr_seq_prest_medico;
		
		insert into pls_prestador_tributo
			(nr_sequencia,				dt_atualizacao,				nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
			nr_seq_prestador_med,			cd_tributo,				vl_tributo,
			dt_referencia,				vl_base_calculo,			ds_observacao,
			ds_emp_retencao)
		select	pls_prestador_tributo_seq.nextval,	sysdate,				nm_usuario_p,
			sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
			r_c01_w.nr_seq_regra_med,		cd_tributo,				vl_tributo,
			dt_referencia,				vl_base_calculo,			ds_observacao,
			ds_emp_retencao
		from	pls_prestador_tributo
		where	nr_seq_prestador_med = r_c01_w.nr_seq_prest_medico;
	end loop;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_TRIBUTO') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_tributo
		where 	nr_seq_prestador = nr_seq_pres_destino_p
		and	nr_seq_prestador_med is null;
	end if;
	
	insert into pls_prestador_tributo
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
		nr_seq_prestador_med,			cd_tributo,				vl_tributo,
		dt_referencia,				vl_base_calculo,			ds_observacao,
		ds_emp_retencao)
	select	pls_prestador_tributo_seq.nextval,	sysdate,				nm_usuario_p,
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
		nr_seq_prestador_med,			cd_tributo,				vl_tributo,
		dt_referencia,				vl_base_calculo,			ds_observacao,
		ds_emp_retencao
	from	pls_prestador_tributo
	where 	nr_seq_prestador = nr_seq_pres_origem_p
	and	nr_seq_prestador_med is null;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_PAGTO') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_pagto
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	else
		select	count(1)
		into	qt_registro_w
		from	pls_prestador_pagto
		where	nr_seq_prestador = nr_seq_pres_destino_p;
		
		if	(qt_registro_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(264257);
		end if;
	end if;
	
	insert into pls_prestador_pagto
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
		cd_condicao_pagamento,			ie_forma_pagto,				qt_dia_vencimento,
		ie_geracao_nota_titulo,			cd_agencia_bancaria,			cd_banco,
		cd_cgc,					cd_pessoa_fisica,			ie_periodo_aprop_neg,
		ie_saldo_negativo,			nr_conta,				nr_seq_pessoa_fisica_conta,
		nr_seq_pessoa_jur_conta,		dt_inicio_vigencia,			dt_fim_vigencia,
		ds_observacao,				ie_excecao_pag_negativo,		ie_tipo_pagamento,
		nr_prazo,				qt_pag_negativo_max,			vl_minimo_tit_liq)
	select	pls_prestador_pagto_seq.nextval,	sysdate,				nm_usuario_p,
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
		cd_condicao_pagamento,			ie_forma_pagto,				qt_dia_vencimento,
		ie_geracao_nota_titulo,			cd_agencia_bancaria,			cd_banco,
		cd_cgc,					cd_pessoa_fisica,			ie_periodo_aprop_neg,
		ie_saldo_negativo,			nr_conta,				nr_seq_pessoa_fisica_conta,
		nr_seq_pessoa_jur_conta,		dt_inicio_vigencia,			dt_fim_vigencia,
		ds_observacao,				ie_excecao_pag_negativo,		ie_tipo_pagamento,
		nr_prazo,				qt_pag_negativo_max,			vl_minimo_tit_liq
	from	pls_prestador_pagto
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_PACOTE') then
	if	(ie_excluir_p = 'S') then
		delete	pls_pacote
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_pacote
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
		cd_estabelecimento,			cd_procedimento,			ds_observacao,
		ie_origem_proced,			ie_situacao,				vl_pacote,
		ie_regra_preco,				ie_baixo_risco,				ie_pacote_estadual,
		ie_pacote_genetica,			ie_pacote_local,			ie_situacao_liberacao,
		ie_status_sispac)	
	select	pls_pacote_seq.nextval,			sysdate,				nm_usuario_p,
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
		cd_estabelecimento,			cd_procedimento,			ds_observacao,
		ie_origem_proced,			ie_situacao,				vl_pacote,
		ie_regra_preco,				ie_baixo_risco,				ie_pacote_estadual,
		ie_pacote_genetica,			ie_pacote_local,			ie_situacao_liberacao,
		ie_status_sispac
	from	pls_pacote
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_MED_ESPEC') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_med_espec
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_pres_destino_p; 
	
	if	(cd_pessoa_fisica_w is not null) then
		insert into pls_prestador_med_espec
			(nr_sequencia,				dt_atualizacao,				nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
			cd_especialidade,			cd_pessoa_fisica, 			ie_guia_medico,
			ie_ptu,					ie_publicacao, 				ie_publicacao_arq_guia,
			ie_rce_atuacao,				nr_seq_area_atuacao,			dt_fim_vigencia,
			dt_inicio_vigencia,			ie_endereco_principal,			ie_especialidade_princ,
			ie_rqe_espec,				cd_titulacao_ptu,			dt_titulo_amb,
			dt_titulo_cnrm,				ie_titulo_amb,				ie_titulo_cnrm)
		select	pls_prestador_med_espec_seq.nextval,	sysdate,				nm_usuario_p,
			sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
			cd_especialidade,			cd_pessoa_fisica_w, 			ie_guia_medico,
			ie_ptu,					ie_publicacao, 				ie_publicacao_arq_guia,
			ie_rce_atuacao,				nr_seq_area_atuacao,			dt_fim_vigencia,
			dt_inicio_vigencia,			ie_endereco_principal,			ie_especialidade_princ,
			ie_rqe_espec,				cd_titulacao_ptu,			dt_titulo_amb,
			dt_titulo_cnrm,				ie_titulo_amb,				ie_titulo_cnrm
		from	pls_prestador_med_espec
		where	nr_seq_prestador = nr_seq_pres_origem_p;
	else
		insert into pls_prestador_med_espec
			(nr_sequencia,				dt_atualizacao,				nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
			cd_especialidade,			cd_pessoa_fisica, 			ie_guia_medico,
			ie_ptu,					ie_publicacao, 				ie_publicacao_arq_guia,
			ie_rce_atuacao,				nr_seq_area_atuacao,			dt_fim_vigencia,
			dt_inicio_vigencia,			ie_endereco_principal,			ie_especialidade_princ,
			ie_rqe_espec,				cd_titulacao_ptu,			dt_titulo_amb,
			dt_titulo_cnrm,				ie_titulo_amb,				ie_titulo_cnrm)
		select	pls_prestador_med_espec_seq.nextval,	sysdate,				nm_usuario_p,
			sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
			cd_especialidade,			cd_pessoa_fisica, 			ie_guia_medico,
			ie_ptu,					ie_publicacao, 				ie_publicacao_arq_guia,
			ie_rce_atuacao,				nr_seq_area_atuacao,			dt_fim_vigencia,
			dt_inicio_vigencia,			ie_endereco_principal,			ie_especialidade_princ,
			ie_rqe_espec,				cd_titulacao_ptu,			dt_titulo_amb,
			dt_titulo_cnrm,				ie_titulo_amb,				ie_titulo_cnrm
		from	pls_prestador_med_espec
		where	nr_seq_prestador = nr_seq_pres_origem_p;
	end if;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_GRUPO_SERV') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_grupo_serv 
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_prestador_grupo_serv
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
		cd_grupo_servico,			nr_seq_prest_end)
	select	pls_prestador_grupo_serv_seq.nextval,	sysdate,				nm_usuario_p,
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
		cd_grupo_servico,			null
	from	pls_prestador_grupo_serv
	where 	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_REDE_REF') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_rede_ref 
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_prestador_rede_ref
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador, 
		cd_rede)
	select	pls_prestador_rede_ref_seq.nextval,	sysdate,				nm_usuario_p,
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
		cd_rede
	from	pls_prestador_rede_ref
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

if	(nm_tabela_p = 'PLS_PRESTADOR_TIPO') then
	if	(ie_excluir_p = 'S') then
		delete	pls_prestador_tipo 
		where 	nr_seq_prestador = nr_seq_pres_destino_p;
	end if;
	
	insert into pls_prestador_tipo
		(nr_sequencia,				dt_atualizacao,				nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_prestador,
		dt_inicio_vigencia,			dt_fim_vigencia,			nr_seq_tipo,
		ie_ptu)
	select	pls_prestador_tipo_seq.nextval,		sysdate,				nm_usuario_p,
		sysdate,				nm_usuario_p,				nr_seq_pres_destino_p,
		dt_inicio_vigencia,			dt_fim_vigencia,			nr_seq_tipo,
		ie_ptu
	from	pls_prestador_tipo
	where	nr_seq_prestador = nr_seq_pres_origem_p;
end if;

dbms_application_info.SET_ACTION('');

commit;
 
end pls_copia_parametro_prestador;
/
