create or replace
procedure ws_gerar_prescr_proc_compl(	nr_prescricao_p	number,
					nr_sequencia_p	number,
					nm_usuario_p	varchar2,
					ie_opcao_p	varchar2) is 

-- IE_OPCAO_P = Domain 8503					
					
ie_registra_proc_integ_w	lab_parametro.ie_registra_proc_integ%type;
									
begin

select	nvl(max(ie_registra_proc_integ), 'N')
into	ie_registra_proc_integ_w
from	lab_parametro
where	cd_estabelecimento = (select cd_estabelecimento from prescr_medica where nr_prescricao = nr_prescricao_p);

if (nvl(ie_registra_proc_integ_w, 'N') = 'S') then
	gerar_prescr_proc_compl(nr_prescricao_p, nr_sequencia_p, nm_usuario_p, ie_opcao_p);
end if;

end ws_gerar_prescr_proc_compl;
/