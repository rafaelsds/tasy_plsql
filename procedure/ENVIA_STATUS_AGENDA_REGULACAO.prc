CREATE OR REPLACE PROCEDURE ENVIA_STATUS_AGENDA_REGULACAO (IE_STATUS_P          VARCHAR2,
                                                             NR_SEQ_AGENDA_CONS_P  NUMBER DEFAULT NULL,
                                                             NR_SEQ_AGENDA_EXAME_P NUMBER DEFAULT NULL,
                                                             IE_TRATADO_EXTERNAMENTE_P NUMBER DEFAULT NULL,
                                                             DS_MENSAGEM_REGULACAO_P VARCHAR2 DEFAULT NULL) IS

NR_SEQ_REGULACAO_W REGULACAO_ATEND.NR_SEQUENCIA%TYPE;
 IE_INTEGRACAO_W            VARCHAR2(2);
 DS_INTEGRACAO_W            CLOB;
 NM_USUARIO_W               REGULACAO_ATEND.NM_USUARIO%TYPE;
 
BEGIN

  IF(NR_SEQ_AGENDA_CONS_P IS NOT NULL) THEN
  
    SELECT MAX(D.NR_SEQ_REGULACAO)
      INTO NR_SEQ_REGULACAO_W
      FROM  AGENDA_INTEGRADA_ITEM B 
      JOIN AGENDA_INTEGRADA C ON C.NR_SEQUENCIA = B.NR_SEQ_AGENDA_INT
      JOIN AGENDA_LISTA_ESPERA D ON C.NR_SEQ_LISTA_ESPERA = D.NR_SEQUENCIA
     WHERE B.NR_SEQ_AGENDA_CONS IS NOT NULL
       AND B.NR_SEQ_AGENDA_INT IS NOT NULL
       AND C.NR_SEQ_LISTA_ESPERA IS NOT NULL
       AND D.NR_SEQ_REGULACAO IS NOT NULL
       AND B.NR_SEQ_AGENDA_CONS = NR_SEQ_AGENDA_CONS_P;
  
  ELSIF (NR_SEQ_AGENDA_EXAME_P IS NOT NULL) THEN
        
        SELECT MAX(D.NR_SEQ_REGULACAO)
          INTO NR_SEQ_REGULACAO_W
          FROM  AGENDA_INTEGRADA_ITEM B 
          JOIN AGENDA_INTEGRADA C ON C.NR_SEQUENCIA = B.NR_SEQ_AGENDA_INT
          JOIN AGENDA_LISTA_ESPERA D ON C.NR_SEQ_LISTA_ESPERA =
                                        D.NR_SEQUENCIA
         WHERE B.NR_SEQ_AGENDA_CONS IS NOT NULL
           AND B.NR_SEQ_AGENDA_INT IS NOT NULL
           AND C.NR_SEQ_LISTA_ESPERA IS NOT NULL
           AND D.NR_SEQ_REGULACAO IS NOT NULL
           AND B.NR_SEQ_AGENDA_EXAME = NR_SEQ_AGENDA_EXAME_P;
        
  END IF;
  
  
  IF (NR_SEQ_REGULACAO_W IS NOT NULL) THEN

    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    
    OBTER_PARAM_USUARIO(366, 4, OBTER_PERFIL_ATIVO, NM_USUARIO_W, NULL,  IE_INTEGRACAO_W);
    

    IF (IE_INTEGRACAO_W = 'I') THEN
      SELECT BIFROST.SEND_INTEGRATION( 'regulation.status.update',
                                      'com.philips.tasy.integration.atepac.regulation.status.request.RegulationStatusRequest',
                                      '{ "regulation" : "' ||NR_SEQ_REGULACAO_W || '",' 
                                       || nvl2(IE_STATUS_P, '"status" : "' || IE_STATUS_P ||'"' , '')
                                       || nvl2(IE_TRATADO_EXTERNAMENTE_P, '"isTreatedExternally" : ' || IE_TRATADO_EXTERNAMENTE_P ||' ,', '' )
                                       || nvl2(DS_MENSAGEM_REGULACAO_P, '"regulationMessage" : "' || DS_MENSAGEM_REGULACAO_P ||'" ', '' )
                                       || '}',
                                      NM_USUARIO_W)
        INTO DS_INTEGRACAO_W
        FROM DUAL;   
    END IF;

  END IF;

END;
/
