create or replace
procedure enviar_comunic_fim_auxilio(nr_seq_ordem_p			Varchar2,
									 nm_usuario_p			varchar2,
									 cd_estabelecimento_p	number,
									 nm_destino_p			varchar2) is 

			
ds_texto_w 		varchar2(4000);
nr_sequencia_w	number(10);
begin


ds_texto_w :=  obter_desc_expressao(781263)/*'Solicita��o realizada na O.S. '*/||nr_seq_ordem_p||'. '||' Data: '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') ;

if (nr_seq_ordem_p is not null) then
	
	select	comunic_interna_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert	into comunic_interna
			(cd_estab_destino,
			nr_sequencia,
			ds_titulo,
			dt_comunicado,
			ds_comunicado,
			nm_usuario,
			nm_usuario_destino,
			dt_atualizacao,
			ie_geral,
			ie_gerencial,
			ds_perfil_adicional,
			dt_liberacao)
	values	(
			cd_estabelecimento_p,
			nr_sequencia_w,
			obter_desc_expressao(782178)/*'Aux�lio O.S.'*/,			
			sysdate,
			ds_texto_w,
			nm_usuario_p,
			nm_destino_p,
			sysdate,
			'N',
			'N',
			null,
			sysdate);
end if;			

commit;

end enviar_comunic_fim_auxilio;
/