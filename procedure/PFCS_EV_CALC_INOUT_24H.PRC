CREATE OR REPLACE PROCEDURE PFCS_EV_CALC_INOUT_24H(
    cd_establishment_p  number,
    nm_user_p           varchar2) is

pfcs_panel_seq_w  pfcs_panel.nr_sequencia%type;
qt_admissions_w   number(10) := 0;
qt_discharges_w   number(10) := 0;

BEGIN
    select count(enc.id_encounter)
    into qt_admissions_w
    from pfcs_encounter enc
    where enc.period_start between (SYSDATE - 1) and SYSDATE
        and enc.period_end is null
        and upper(enc.si_classif) = 'INPATIENT'
        and enc.nr_seq_organization = (
            select max(org_aux.nr_sequencia)
            from pfcs_organization org_aux
            where org_aux.cd_estabelecimento = cd_establishment_p
        ) and not exists (
            select 1 from pfcs_service_request svc_aux
            where svc_aux.nr_seq_encounter = enc.nr_sequencia
                and svc_aux.nr_seq_patient = enc.nr_seq_patient
                and svc_aux.cd_service = 'E0405'
                and svc_aux.si_status = 'COMPLETED'
                and svc_aux.si_intent = 'ORDER'
        );

    select count(enc.id_encounter)
    into qt_discharges_w
    from pfcs_encounter enc,
        pfcs_service_request svc
    where upper(enc.si_classif) = 'INPATIENT'
        and enc.period_start is not null
        and enc.period_end is not null
        and enc.nr_seq_organization = (
            select aux.nr_sequencia
            from pfcs_organization aux
            where cd_estabelecimento = cd_establishment_p
        )
        and svc.nr_seq_encounter = enc.nr_sequencia
        and svc.nr_seq_patient = enc.nr_seq_patient
        and svc.cd_service = 'E0405'
        and svc.si_status = 'COMPLETED'
        and svc.si_intent = 'ORDER'
        and svc.dt_authored_on between (SYSDATE - 1) and SYSDATE;

    pfcs_pck.pfcs_generate_results(
            vl_indicator_p => qt_admissions_w,
            nr_seq_indicator_p => 105,
            nr_seq_operational_level_p => cd_establishment_p,
            nm_usuario_p => nm_user_p,
            nr_seq_panel_p => pfcs_panel_seq_w);
    pfcs_pck.pfcs_generate_results(
            vl_indicator_p => qt_discharges_w,
            nr_seq_indicator_p => 106,
            nr_seq_operational_level_p => cd_establishment_p,
            nm_usuario_p => nm_user_p,
            nr_seq_panel_p => pfcs_panel_seq_w);

    pfcs_pck.pfcs_activate_records(
            nr_seq_indicator_p => 105,
            nr_seq_operational_level_p => cd_establishment_p,
            nm_usuario_p => nm_user_p);
    pfcs_pck.pfcs_activate_records(
            nr_seq_indicator_p => 106,
            nr_seq_operational_level_p => cd_establishment_p,
            nm_usuario_p => nm_user_p);
END PFCS_EV_CALC_INOUT_24H;
/
