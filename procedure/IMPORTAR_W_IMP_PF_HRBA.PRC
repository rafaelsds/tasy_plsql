create or replace
procedure importar_w_imp_pf_hrba is

/* Estrutura cliente */

cd_ant_w		varchar2(255);
nome_w			varchar2(255);
nasc_w			varchar2(255);
sexo_w			varchar2(255);
cor_w			varchar2(255);
est_civil_w		varchar2(255);
identidade_w		varchar2(255);
cpf_w			varchar2(255);
uf_natur_w		varchar2(255);
naturalid_w		varchar2(255);
nacional_w		varchar2(255);
mae_w			varchar2(255);
pai_w			varchar2(255);
obs_w			varchar2(255);
cns_w			varchar2(255);
obs2_w			varchar2(255);
cert_nasc_w		varchar2(255);
nr_folha_w		varchar2(255);
nr_livro_w		varchar2(255);
ds_orgao_w		varchar2(255);
uf_emis_w		varchar2(255);
end_w			varchar2(255);
bairro_w		varchar2(255);
cidade_w		varchar2(255);
estado_w		varchar2(255);
pais_w			varchar2(255);
cep_w			varchar2(255);
obs_compl_w		varchar2(255);
nr_end_w		varchar2(255);
cd_cidade_w		varchar2(255);

cd_cep_nat_w		varchar2(255);
naturalidade_w		varchar2(255);
ds_naturalidade_w	varchar2(255);
nr_seq_cor_pele_w	varchar2(255);
ds_cidade_w		varchar2(255);
vl_cidade_w		varchar2(255);
nr_seq_pais_w		varchar2(255);
sg_estado_w		varchar2(255);
ds_obs_pf_w		varchar2(2000);
cd_nacionalidade_w	varchar2(255);
ie_estado_civil_w	varchar2(255);

/* Internos */
cd_pessoa_fisica_w	varchar2(10);
nr_sequencia_w		number(5);
ie_erro_compl_pf_w	varchar2(1)	:= 'N';
ie_erro_pf_w		varchar2(1)	:= 'N';
qt_registro_w		number(10);
trigger_name_w		varchar2(255);
sql_errm_w		varchar2(2000);
qt_inserido_w		number(15,0);

qt_teste_w		number(15,0);
ds_erro_w		varchar2(4000);

Cursor c01 is
select	cd_ant,
	nome,
	nasc,
	sexo,
	cor,
	est_civil,
	identidade,
	cpf,
	uf_natur,
	naturalid,
	nacional,
	mae,
	pai,
	obs,
	cns,
	obs2,
	cert_nasc,
	nr_folha,
	nr_livro,
	ds_orgao,
	uf_emis,
	end,
	bairro,
	cidade,
	estado,
	pais,
	cep,
	obs_compl,
	nr_end,
	cd_cidade
from	w_pf_hrba
order by nome;

begin

wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('N');

begin

qt_inserido_w	:= 0;

open c01;
loop
fetch c01 into
	cd_ant_w,
	nome_w,
	nasc_w,
	sexo_w,
	cor_w,
	est_civil_w,
	identidade_w,
	cpf_w,
	uf_natur_w,
	naturalid_w,
	nacional_w,
	mae_w,
	pai_w,
	obs_w,
	cns_w,
	obs2_w,
	cert_nasc_w,
	nr_folha_w,
	nr_livro_w,
	ds_orgao_w,
	uf_emis_w,
	end_w,
	bairro_w,
	cidade_w,
	estado_w,
	pais_w,
	cep_w,
	obs_compl_w,
	nr_end_w,
	cd_cidade_w;
exit when c01%notfound;

	ie_erro_pf_w		:= 'N';
	ie_erro_compl_pf_w	:= 'N';
	qt_inserido_w		:= qt_inserido_w + 1;

	select	pessoa_fisica_seq.nextval
	into	cd_pessoa_fisica_w
	from	dual;


	if 	(cpf_w is not null) then
		
		select	count(*) 
		into	qt_registro_w
		from	pessoa_fisica
		where	nr_cpf = cpf_w;
	
		if	(qt_registro_w > 0) then
			insert	into	log_tasy
				(cd_log,
				nm_usuario,
				dt_atualizacao,
				ds_log)
			values	(55715,
				'OS183332_PF',
				sysdate,
				'Erro ao inserir pessoa. ' 	|| chr(13) || chr(10) ||
				'Nome= ' || nome_w 	|| chr(13) || chr(10) ||
				'Erro= J� existe uma pessoa f�sica cadastrada com o CPF ' || cpf_w);
		
			ie_erro_pf_w	:= 'S';
		end if;
	end if;


	if	(ie_erro_pf_w = 'N') then	
		begin
		
		select	somente_numero(naturalid_w)
		into	naturalidade_w
		from	dual;
		
		if (naturalidade_w = '0') then
			ds_naturalidade_w := naturalid_w;
		else
			select	max(nm_cidade)
			into	ds_naturalidade_w
			from	w_compl_hrba
			where	cd_cidade = naturalid_w;
		end if;
		
		if (nvl(cor_w,'0') <> '0') then
		
			select	max(nr_sequencia)
			into	nr_seq_cor_pele_w
			from	cor_pele
			where	upper(ds_cor_pele) = upper(cor_w);
		end if;
		
		select	DECODE(est_civil_w,'C','2','D','3','S','1','V','5')
		into	ie_estado_civil_w
		from	dual;
		
		select	max(cd_nacionalidade)
		into	cd_nacionalidade_w
		from	nacionalidade
		where	upper(cd_externo) = upper(nacional_w);
		
		ds_obs_pf_w := naturalid_w || chr(13)|| uf_natur_w || chr(13)|| obs_w || chr(13)|| obs2_w;
		
		insert into pessoa_fisica
			(cd_pessoa_fisica,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ie_tipo_pessoa,
			cd_sistema_ant,
			nm_pessoa_fisica,
			dt_nascimento,
			ie_sexo,
			nr_seq_cor_pele,
			ie_estado_civil,
			nr_identidade,
			nr_cpf,
			ds_observacao,
			cd_nacionalidade,
			nr_cartao_nac_sus,
			nr_cert_nasc,
			nr_folha_cert_nasc,
			nr_livro_cert_nasc,
			ds_orgao_emissor_ci)
		values	(cd_pessoa_fisica_w,
			'OS183332_PF',
			sysdate,
			'OS183332_PF',
			sysdate,
			1,
			substr(cd_ant_w,1,20),
			substr(nome_w,1,60),
			to_date(nasc_w,'dd/mm/yyyy'),
			substr(sexo_w,1,1),
			to_number(substr(nr_seq_cor_pele_w,1,10)),
			substr(ie_estado_civil_w,1,2),
			substr(identidade_w,1,15),
			substr(cpf_w,1,11),
			substr(ds_obs_pf_w,1,2000),
			to_number(substr(cd_nacionalidade_w,1,10)),
			somente_numero(substr(cns_w,1,15)),
			substr(cert_nasc_w,1,20),
			to_number(substr(nr_folha_w,1,10)),
			to_number(substr(nr_livro_w,1,10)),
			substr(ds_orgao_w,1,10));

		exception
			when others then
			sql_errm_w	:= sqlerrm;
			insert	into	log_tasy
				(cd_log,
				nm_usuario,
				dt_atualizacao,
				ds_log)
			values	(55715,
				'OS183332_PF',
				sysdate,
				'Erro ao inserir pessoa. ' 	|| chr(13) || chr(10) ||
				'Nome= ' || nome_w 		|| chr(13) || chr(10) ||
				'substr(end_w,1,40),'||substr(end_w,1,40)|| chr(13) ||  chr(10) ||
				'substr(cd_ant_w,1,20),'||substr(cd_ant_w,1,20)|| chr(13) ||  chr(10) ||
				'substr(nome_w,1,60),'||substr(nome_w,1,60)|| chr(13) ||  chr(10) ||
				'nasc_w,'||nasc_w|| chr(13) ||  chr(10) ||
				'substr(sexo_w,1,1),'||substr(sexo_w,1,1)|| chr(13) ||  chr(10) ||
				'to_number(nr_seq_cor_pele_w),'||to_number(nr_seq_cor_pele_w)|| chr(13) ||  chr(10) ||
				'ie_estado_civil_w,'||ie_estado_civil_w|| chr(13) ||  chr(10) ||
				'substr(identidade_w,1,15),'||substr(identidade_w,1,15)|| chr(13) ||  chr(10) ||
				'substr(cpf_w,1,11),'||substr(cpf_w,1,11)|| chr(13) ||  chr(10) ||
				'ds_obs_pf_w,'||ds_obs_pf_w|| chr(13) ||chr(10)||
				'cd_nacionalidade_w,'||cd_nacionalidade_w|| chr(13) ||  chr(10) ||
				'to_number(substr(cns_w,1,15)),'||to_number(substr(cns_w,1,15))|| chr(13) ||  chr(10) ||
				'substr(cert_nasc_w,1,20),'||substr(cert_nasc_w,1,20)|| chr(13) ||  chr(10) ||
				'to_number(substr(nr_folha_w,1,10)),'||to_number(substr(nr_folha_w,1,10))|| chr(13) ||  chr(10) ||
				'to_number(substr(nr_livro_w,1,10)),'||to_number(substr(nr_livro_w,1,10))|| chr(13) ||  chr(10) ||
				'substr(ds_orgao_w,1,10)'||substr(ds_orgao_w,1,10)||chr(13) ||  chr(10) ||
				'Erro= ' || sql_errm_w);

			ie_erro_pf_w	:= 'S';
		end;

		if	(ie_erro_pf_w = 'N') then

			/* Residencial - sempre insere */

			if	(ie_erro_compl_pf_w = 'N') then

				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
				select	MAX(vl_dominio)
				into	sg_estado_w
				from	valor_dominio
				where	cd_dominio = 50
				and	vl_dominio = estado_w;
		
				select 	  MAX(nr_sequencia)
				into	  nr_seq_pais_w
				from	  pais
				where	  upper(nm_pais) = upper(pais_w);
		
				select	somente_numero(cidade_w)
				into	vl_cidade_w
				from	dual;
		
				if (vl_cidade_w = '0') then
					ds_cidade_w := vl_cidade_w;
				else
					select	max(nm_cidade)
					into	ds_cidade_w
					from	w_compl_hrba
					where	cd_cidade = cidade_w;
				end if;
		
				insert into compl_pessoa_fisica
					(nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					ds_endereco,
					ds_bairro,
					ds_municipio,
					sg_estado,
					nr_seq_pais,
					cd_cep,
					ds_observacao,
					nr_endereco)
				values	(nr_sequencia_w,
					cd_pessoa_fisica_w,
					1,
					'OS183332_PF',
					sysdate,
					'OS183332_PF',
					sysdate,
					substr(end_w,1,40),
					substr(bairro_w,1,40),
					substr(ds_cidade_w,1,40),
					substr(sg_estado_w,1,2),
					to_number(nr_seq_pais_w),
					substr(cep_w,1,15),
					substr(obs_compl_w,1,255),
					somente_numero(substr(nr_end_w,1,5)));
			end if;

			/* Fim residencial */	
		
			if	(mae_w is not null) then
	
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
				insert into compl_pessoa_fisica
					(nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					nm_contato)
				values	(nr_sequencia_w,
					cd_pessoa_fisica_w,
					5,
					'OS183332_PF',
					sysdate,
					'OS183332_PF',
					sysdate,
					substr(mae_w,1,60));		
				end if;

			if	(pai_w is not null) then

				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

				insert into compl_pessoa_fisica
					(nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					nm_contato)
				values	(nr_sequencia_w,
					cd_pessoa_fisica_w,
					4,
					'OS183332_PF',
					sysdate,
					'OS183332_PF',
					sysdate,
					substr(pai_w,1,60));
				end if;

		end if;

		if	(qt_inserido_w	= 3000) then
			commit;
			qt_inserido_w	:= 0;
		end if;

	end if;

end loop;
close c01;

commit;

exception
	when others then
	wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('S');

	ds_erro_w := sqlerrm;
	wheb_mensagem_pck.exibir_mensagem_abort(278182,'DS_ERRO='|| ds_erro_w||';NOME='|| nome_w);
	
	
end;
commit;

end importar_w_imp_pf_hrba;
/
