create or replace
procedure sup_altera_prior_cot_forn(
				qt_prioridade_p		number,
				ds_justificativa_p	varchar2,
				nr_sequencia_p		number,
				nm_usuario_p		varchar2) is 

ds_observacao_w		varchar2(255);
ds_observacao_original_w		varchar2(255);
				
begin

select	ds_observacao
into	ds_observacao_original_w
from	cot_compra_forn_item
where	nr_sequencia = nr_sequencia_p;

if	(ds_observacao_original_w is not null) then
	ds_observacao_w	:= substr(ds_observacao_original_w || chr(13) || chr(10) || ds_justificativa_p,1,255);
else
	ds_observacao_w	:= substr(ds_justificativa_p,1,255);
end if;

update	cot_compra_forn_item
set	qt_prioridade = qt_prioridade_p,
	ds_observacao = ds_observacao_w,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_sequencia = nr_sequencia_p;

commit;

end sup_altera_prior_cot_forn;
/