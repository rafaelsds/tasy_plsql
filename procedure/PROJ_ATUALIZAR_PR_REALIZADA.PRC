create or replace
procedure proj_atualizar_pr_realizada(
			nr_seq_cronograma_p	Number,
			nr_sequencia_p		Number,
			pr_etapa_p		Number) as

ie_fase_w		varchar2(1);
pr_calc_w		number(15,4);
pr_etapa_w		number(15,4);
pr_etapa_ww		number(15,4);
nr_seq_superior_w	number(10);
qt_hora_prev_w		number(15,2);
pai_qt_hora_prev_w	number(15,2);
pr_consultoria_w	number(15,4);
pr_consultoria_ww	number(15,4);

cursor c01 is
	select	qt_hora_prev,
		pr_etapa,
		pr_consultoria
	from	proj_cron_etapa
	where	nr_seq_cronograma = nr_seq_cronograma_p
	and	nr_seq_superior = nr_seq_superior_w
	and	nvl(qt_hora_prev,0) > 0
	and	nvl(ie_situacao,'A') = 'A';

begin

select	nvl(max(nr_seq_superior),0)
into	nr_seq_superior_w
from	proj_cron_etapa
where	nr_seq_cronograma = nr_seq_cronograma_p
and	nr_sequencia = nr_sequencia_p;


if	(nr_seq_superior_w > 0) then
	begin

	select	ie_fase,
		nvl(qt_hora_prev,0)
	into	ie_fase_w,
		pai_qt_hora_prev_w
	from	proj_cron_etapa
	where	nr_seq_cronograma = nr_seq_cronograma_p
	and	nr_sequencia = nr_seq_superior_w;

	pr_etapa_w	:= 0;
	pr_consultoria_w := 0;
	OPEN C01;
	LOOP
	FETCH C01 INTO
		qt_hora_prev_w,
		pr_etapa_ww,
		pr_consultoria_ww;
	exit when c01%notfound;
		begin

		/*pr_calc_w	:= nvl(round((dividir(nvl(qt_hora_prev_w,0), nvl(pai_qt_hora_prev_w,0)) * 100),2),0);
		pr_etapa_ww	:= nvl(round(dividir((nvl(pr_etapa_ww,0) * nvl(pr_calc_w,0)), 100),2),0);
		pr_etapa_w	:= nvl(round(nvl(pr_etapa_w,0) + nvl(pr_etapa_ww,0),2),0);*/

		if	(nvl(qt_hora_prev_w,0) > 0) and (nvl(pai_qt_hora_prev_w,0) > 0) then
			pr_calc_w:= nvl(round((nvl(qt_hora_prev_w,0)/nvl(pai_qt_hora_prev_w,0) * 100),4),0);
		else
			pr_calc_w:= 1;
		end if;
		
		pr_etapa_ww	:= nvl(round((nvl(pr_etapa_ww,0) * nvl(pr_calc_w,0))/ 100,4),0);
		pr_etapa_w	:= nvl(round(nvl(pr_etapa_w,0) + nvl(pr_etapa_ww,0),4),0);
		
		pr_consultoria_ww	:= nvl(round((nvl(pr_consultoria_ww,0) * nvl(pr_calc_w,0))/ 100,4),0);
		pr_consultoria_w	:= nvl(round(nvl(pr_consultoria_w,0) + nvl(pr_consultoria_ww,0),4),0);
		
		end;
	end loop;
	close c01;
	
	if	(pr_etapa_w > 100) then
		pr_etapa_w := 100;	
	end if;
	
	if	(pr_consultoria_w > 100) then
		pr_consultoria_w := 100;	
	end if;
	
	if	(ie_fase_w = 'S') then
		begin
		update	proj_cron_etapa
		set	pr_etapa = round(pr_etapa_w,2),
			pr_consultoria = round(pr_consultoria_w,2)
		where	nr_seq_cronograma = nr_seq_cronograma_p
		and	nr_sequencia = nr_seq_superior_w;
		end;
	end if;

	commit;
	proj_atualizar_pr_realizada(nr_seq_cronograma_p,nr_seq_superior_w,pr_etapa_w);
	end; 
end if;

commit;

end proj_atualizar_pr_realizada;
/