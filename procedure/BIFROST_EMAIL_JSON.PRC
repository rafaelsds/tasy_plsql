CREATE OR REPLACE PROCEDURE BIFROST_EMAIL_JSON(JSON_DATA_P     CLOB,
                                                 status_p 	OUT varchar2,
                                                 mensagem_p 	OUT varchar2,
                                                 sugestao_p 	OUT varchar2) IS
  JSON_DATA_W     PHILIPS_JSON;
BEGIN
  status_p := ''; 
  mensagem_p := ''; 
  sugestao_p := '';
  JSON_DATA_W := PHILIPS_JSON(JSON_DATA_P);
  IF (JSON_DATA_W IS NOT NULL
  AND JSON_DATA_W.COUNT > 0) THEN
    if(JSON_DATA_W.GET('status') is not null)then
      status_p := JSON_DATA_W.GET('status').GET_STRING();
    end if;
    
    if(JSON_DATA_W.GET('message') is not null)then
      mensagem_p := JSON_DATA_W.GET('message').GET_STRING();
    end if;
    
    if(JSON_DATA_W.GET('suggestion') is not null)then
      sugestao_p := JSON_DATA_W.GET('suggestion').GET_STRING();
    end if;
  END IF;
END;
/
