create or replace
procedure gerar_escala_nih(nr_sequencia_p		number,
				nm_usuario_p		varchar2) is

nr_atendimento_w	number(10);
ie_possui_registro_w	varchar2(1);
nr_seq_item_escala_w	number(10);
nr_sequencia_w		number(10);
cd_pessoa_fisica_w	varchar2(10);

/* Atributos da escala */

ie_nivel_consciencia_w 	varchar2(5);
ie_orientacao_w  	varchar2(5);
ie_comando_w 	varchar2(5);
ie_motricidade_ocular_w 	varchar2(5);
ie_campo_visual_w	varchar2(5);
ie_paresia_facial_w  	varchar2(5);
ie_motor_ms_w 	varchar2(5);
ie_motor_ms_d_w 	varchar2(5);
ie_motor_mi_w 	varchar2(5);
ie_motor_mi_d_w 	varchar2(5);
ie_ataxia_w	varchar2(5);
ie_sensibilidade_w  	varchar2(5);
ie_linguagem_w 	varchar2(5);
ie_disartria_w	varchar2(5);
ie_negligencia_w varchar2(5);

	function obter_valor_escala( nr_seq_item_escala_p	number) return varchar2 is

	ie_retorno_w	varchar2(15);
	nr_seq_result_escala_w	varchar2(15);
	begin
	
	select	max(f.nr_seq_result_escala)
	into	nr_seq_result_escala_w
	from	pe_prescricao a,
		pe_prescr_item_result b,
		pe_regra_item_escala c,
		pe_regra_result_escala f
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_sequencia = b.nr_seq_prescr
	and	b.nr_seq_item = c.nr_seq_item_sae
	and	c.nr_seq_item_escala = nr_seq_item_escala_p
	and	f.nr_seq_result_sae = b.nr_seq_result
	and	f.nr_seq_regra = c.nr_sequencia;
	
	select 	max (x.vl_result)
	into	ie_retorno_w
	from	pe_result_item x,
		pe_regra_result_escala f
	where	x.nr_sequencia = nr_seq_result_escala_w;
	
	return ie_retorno_w;
	end;
	
	function obter_descricao_escala_nih( nr_seq_item_escala_p	number) return varchar2 is

	ds_observacao_w	varchar2(80);
	nr_seq_result_escala_w	varchar2(15);
	begin
	
	select	max(b.ds_observacao)
	into	ds_observacao_w
	from	pe_prescricao a,
		pe_prescr_item_result b,
		pe_regra_item_escala c,
		pe_regra_result_escala f
	where	a.nr_sequencia = nr_sequencia_p
	and	a.nr_sequencia = b.nr_seq_prescr
	and	b.nr_seq_item = c.nr_seq_item_sae
	and	c.nr_seq_item_escala = nr_seq_item_escala_p
	and	f.nr_seq_result_sae = b.nr_seq_result
	and	f.nr_seq_regra = c.nr_sequencia;
	
	return ds_observacao_w;
	end;

begin

select	max(a.cd_prescritor),
	max(a.nr_atendimento)
into	cd_pessoa_fisica_w,
	nr_atendimento_w
from	pe_prescricao a,
	pe_prescr_item_result b
where	a.nr_sequencia = b.nr_seq_prescr
and	a.nr_sequencia = nr_sequencia_p;

select	nvl(max('S'),'N')
into	ie_possui_registro_w
from 	pe_prescricao a,
	pe_prescr_item_result b,
	pe_regra_item_escala c,
	pe_regra_result_escala d
where	a.nr_sequencia	= b.nr_seq_prescr
and	b.nr_seq_result = d.nr_seq_result_sae
and	b.nr_seq_item	= c.nr_seq_item_sae
and	a.nr_sequencia	= nr_sequencia_p
and	c.nr_seq_item_escala	is not null
and 	c.ie_escala = 9;

if 	(ie_possui_registro_w = 'S') then
	begin
	
	ie_nivel_consciencia_w := obter_valor_escala(186);
	ie_orientacao_w :=	obter_valor_escala(187);
	ie_comando_w := obter_valor_escala(188);
	ie_motricidade_ocular_w :=	obter_valor_escala(189);
	ie_campo_visual_w :=	obter_valor_escala(190);
	ie_paresia_facial_w  :=	obter_valor_escala(191);	
	ie_motor_ms_w := 	obter_valor_escala(192);
	ie_motor_ms_d_w :=	obter_valor_escala(193);
	ie_motor_mi_w 	:= obter_valor_escala(194);
	ie_motor_mi_d_w 	:= obter_valor_escala(195);
	ie_ataxia_w := obter_valor_escala(196);
	ie_sensibilidade_w  := obter_valor_escala(197);
	ie_linguagem_w :=	obter_valor_escala(198);
	ie_disartria_w:= obter_valor_escala(199);
	ie_negligencia_w := obter_valor_escala(200);

end;	

	insert into atend_nih (nr_sequencia,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				dt_avaliacao,
				ie_nivel_consciencia,
				ie_orientacao,
				ie_comando,
				ie_motricidade_ocular,
				ie_campo_visual,
				ie_paresia_facial,
				ie_motor_ms,
				ie_motor_mi,
				ie_ataxia,
				ie_sensibilidade,
				ie_linguagem,
				ie_disartria,
				ie_negligencia,
				dt_liberacao,
				ie_situacao,
				cd_pessoa_fisica,
				ie_motor_ms_d,
				ie_motor_mi_d)
	values	(atend_nih_seq.nextval,
			nr_atendimento_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nvl(ie_nivel_consciencia_w, 'N'),
			nvl(ie_orientacao_w, 'N'),
			nvl(ie_comando_w, 'N'),
			nvl(ie_motricidade_ocular_w, 'N'),
			nvl(ie_campo_visual_w, 'N'),
			nvl(ie_paresia_facial_w, 'N'),
			nvl(ie_motor_ms_w, 'N'),
			nvl(ie_motor_mi_w, 'N'),
			nvl(ie_ataxia_w, 'N'),
			nvl(ie_sensibilidade_w, 'N'),
			nvl(ie_linguagem_w, 'N'),
			nvl(ie_disartria_w, 'N'),
			nvl(ie_negligencia_w, 'N'),			
			sysdate,
			'A',
		   cd_pessoa_fisica_w,
		   nvl(ie_motor_ms_d_w, 'N'),
		   nvl(ie_motor_mi_d_w, 'N'));	
				

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_escala_nih;
/