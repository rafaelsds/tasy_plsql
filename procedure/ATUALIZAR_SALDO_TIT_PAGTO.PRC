CREATE OR REPLACE PROCEDURE
ATUALIZAR_SALDO_TIT_PAGTO	(nr_seq_escrit_p	number,
				nm_usuario_p		varchar2) is

nr_titulo_w	number(10);

cursor	c01 is
select	a.nr_titulo
from	titulo_pagar_escrit a
where	a.nr_seq_escrit	= nr_seq_escrit_p;

BEGIN

open	c01;
loop
fetch	c01 into
	nr_titulo_w;
exit	when c01%notfound;

	atualizar_saldo_tit_pagar(nr_titulo_w,nm_usuario_p);
	Gerar_W_Tit_Pag_imposto(nr_titulo_w,nm_usuario_p);

end	loop;
close	c01;

commit;

END ATUALIZAR_SALDO_TIT_PAGTO;
/
