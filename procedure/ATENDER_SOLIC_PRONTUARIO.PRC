CREATE OR REPLACE
PROCEDURE Atender_solic_Prontuario(
			nr_sequencia_p		Number,
			nm_usuario_p		Varchar2) IS 

qt_emprestimo_w		Number(10);
nr_sequencia_w		Number(10);
nr_prontuario_w		Number(10);
cd_estabelecimento_w	Number(4);
cd_setor_atendimento_w	Number(5);
cd_agenda_w		Number(10);

BEGIN

select	cd_estabelecimento,
	nr_prontuario,
	cd_setor_atendimento,
	cd_agenda
into	cd_estabelecimento_w,
	nr_prontuario_w,
	cd_setor_atendimento_w,
	cd_agenda_w
from	same_cpi_solic
where	nr_sequencia	= nr_sequencia_p;

select	count(*)
into	qt_emprestimo_w
from	same_cpi_emp
where	nr_prontuario	= nr_prontuario_w
and	dt_devolucao is null;

if	(qt_emprestimo_w <> 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(282253);
end if;

select	same_cpi_emp_seq.nextval
into	nr_sequencia_w
from	dual;

insert into same_cpi_emp(
	nr_sequencia,
	cd_estabelecimento,
	nr_prontuario,
	dt_atualizacao,
	nm_usuario,
	cd_setor_atendimento,
	dt_emprestimo,
	nm_usuario_empr,
	ie_operacao,
	cd_agenda,
	nr_seq_solic)
Values(
	nr_sequencia_w,
	cd_estabelecimento_w,
	nr_prontuario_w,
	sysdate,
	nm_usuario_p,
	cd_setor_atendimento_w,
	sysdate,
	nm_usuario_p,
	'E',
	cd_agenda_w,
	nr_sequencia_p);

update	same_cpi_solic
set	ie_status_solic 	= 'T',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

END Atender_solic_Prontuario;
/