create or replace
procedure Gerar_unico_cme_agenda(nm_usuario_p			Varchar2,
								nr_sequencia_p			number,
								nr_seq_proc_interno_p 	number,
								cd_estab_cme_p			number,
								cd_especialidade_p		number,
								cd_medico_p				varchar2,
								ie_GerarCMEIndividualizado_p varchar2,
								ie_tem_medico_p 		varchar2,
								ie_consiste_cme_p		varchar2,
								ds_erro_p		out		varchar2	
								) is 
			
ds_insert_w 			varchar2(1);
cd_medico_conjunto_w	varchar2(10);
NR_SEQ_CONJUNTO_W		number(10);		
qt_conjunto_w			number(10);
ie_obrigatorio_w		varchar2(1);
nr_seq_grupo_w			number(10);
DS_ERRO_CME_W			varchar2(255);
nr_sequencia_w			number(10);
qt_anos_pac_w			number(3);
qt_meses_pac_w			number(2);
qt_total_meses_pac_w	number(4);
			
			
cursor	c01 is
	select		a.nr_seq_conjunto,
			a.qt_conjunto,
			nvl(a.ie_obrigatorio,'S'),
			0 nr_seq_grupo
from		proc_interno_cme a,
		cm_conjunto b
where		a.nr_seq_conjunto = b.nr_sequencia
and		nvl(b.ie_situacao,'A') = 'A'
and		a.nr_seq_proc_interno	=	nr_seq_proc_interno_p
and 		nvl(qt_total_meses_pac_w,0) between nvl(b.qt_idade_min,0) and  nvl(b.qt_idade_max,999)
and			a.cd_medico is null
and			a.NR_SEQ_CLASSIF is null
and			a.NR_SEQ_GRUPO   is null	
and			((a.cd_estabelecimento is null) or (a.cd_estabelecimento = cd_estab_cme_p))
and			((a.cd_especialidade is null) or (a.cd_especialidade = cd_especialidade_p))
and 		not exists(	select	1
						from	agenda_pac_cme x
						where	x.nr_seq_agenda		=	nr_sequencia_p
						and	x.nr_seq_conjunto	=	a.nr_seq_conjunto)
union
select		a.nr_seq_conjunto,
			a.qt_conjunto,
			nvl(a.ie_obrigatorio,'S'),
			0 nr_seq_grupo
from		proc_interno_cme a,
		cm_conjunto b
where		a.nr_seq_conjunto = b.nr_sequencia
and		nvl(b.ie_situacao,'A') = 'A'
and		a.nr_seq_proc_interno	=	nr_seq_proc_interno_p
and			a.cd_medico		=	cd_medico_p
and 		nvl(qt_total_meses_pac_w,0) between nvl(b.qt_idade_min,0) and  nvl(b.qt_idade_max,999)
and			a.NR_SEQ_CLASSIF is null
and			a.NR_SEQ_GRUPO   is null
and			((a.cd_estabelecimento is null) or (a.cd_estabelecimento = cd_estab_cme_p))
and			((a.cd_especialidade is null) or (a.cd_especialidade = cd_especialidade_p))
and 		not exists(	select	1
						from	agenda_pac_cme x
						where	x.nr_seq_agenda		=	nr_sequencia_p
						and	x.nr_seq_conjunto	=	a.nr_seq_conjunto)
union
select		b.nr_sequencia,
			a.qt_conjunto,
			nvl(a.ie_obrigatorio,'S'),
			0 nr_seq_grupo
from		cm_conjunto b, proc_interno_cme a
where		a.nr_seq_proc_interno				= nr_seq_proc_interno_p
and 		nvl(qt_total_meses_pac_w,0) between nvl(b.qt_idade_min,0) and  nvl(b.qt_idade_max,999)
and			nvl(a.cd_medico, nvl(cd_medico_p, 'X')) 	= nvl(cd_medico_p,'X')
and			a.NR_SEQ_CLASSIF 				= b.NR_SEQ_CLASSIF
and			nvl(b.ie_situacao, 'A')				= 'A'
and			NR_SEQ_GRUPO   is null
and			((a.cd_estabelecimento is null) or (a.cd_estabelecimento = cd_estab_cme_p))
and			((a.cd_especialidade is null) or (a.cd_especialidade = cd_especialidade_p))
and 		not exists(	select	1
						from	agenda_pac_cme x
						where	x.nr_seq_agenda		=	nr_sequencia_p
						and	x.nr_seq_conjunto	=	a.nr_seq_conjunto)
union
select		b.nr_sequencia,
			a.qt_conjunto,
			nvl(a.ie_obrigatorio,'S'),
			c.NR_SEQ_GRUPO
from		cm_grupo_conjunto d,
			cm_grupo_classif c, 
			cm_conjunto b, 
			proc_interno_cme a
where		a.nr_seq_proc_interno				= nr_seq_proc_interno_p
and			c.nr_seq_grupo					= d.nr_sequencia
and 		nvl(qt_total_meses_pac_w,0) between nvl(b.qt_idade_min,0) and  nvl(b.qt_idade_max,999)
and			nvl(a.cd_medico, nvl(cd_medico_p, 'X')) 	= nvl(cd_medico_p, 'X')
and			b.NR_SEQ_CLASSIF 				= c.NR_SEQ_CLASSIFICACAO
and			a.NR_SEQ_GRUPO   				= c.NR_SEQ_GRUPO
and			nvl(d.ie_situacao, 'A')				= 'A'
and			nvl(b.ie_situacao, 'A')				= 'A'
and			((a.cd_estabelecimento is null) or (a.cd_estabelecimento = cd_estab_cme_p))
and			((a.cd_especialidade is null) or (a.cd_especialidade = cd_especialidade_p))
and 		not exists(	select	1
						from	agenda_pac_cme x
						where	x.nr_seq_agenda		=	nr_sequencia_p
						and	x.nr_seq_conjunto	=	a.nr_seq_conjunto)
and	(ie_GerarCMEIndividualizado_p = 'N')								
union
select		b.nr_sequencia,
			a.qt_conjunto,
			nvl(a.ie_obrigatorio,'S'),
			c.NR_SEQ_GRUPO
from		cm_grupo_conjunto d,
			cm_grupo_classif c, 
			cm_conjunto b, 
			proc_interno_cme a
where		a.nr_seq_proc_interno					= nr_seq_proc_interno_p
and			c.nr_seq_grupo							= d.nr_sequencia
and 		nvl(qt_total_meses_pac_w,0) between nvl(b.qt_idade_min,0) and  nvl(b.qt_idade_max,999)
and			(((ie_tem_medico_p = 'S') and (a.cd_medico = cd_medico_p)) or
			((ie_tem_medico_p = 'N') and (a.cd_medico is null))) 
and			b.NR_SEQ_CLASSIF 						= c.NR_SEQ_CLASSIFICACAO
and			a.NR_SEQ_GRUPO   						= c.NR_SEQ_GRUPO
and			nvl(d.ie_situacao, 'A')					= 'A'
and			nvl(b.ie_situacao, 'A')					= 'A'
and			((a.cd_estabelecimento is null) or (a.cd_estabelecimento = cd_estab_cme_p))
and			((a.cd_especialidade is null) or (a.cd_especialidade = cd_especialidade_p))
and 		not exists(	select	1
						from	agenda_pac_cme x
						where	x.nr_seq_agenda	=	nr_sequencia_p
						and	x.nr_seq_conjunto	=	a.nr_seq_conjunto)
and	(ie_GerarCMEIndividualizado_p = 'S');					


begin

select  max(qt_idade_paciente),
		max(qt_idade_mes)
into	qt_anos_pac_w,	
        qt_meses_pac_w	
from	agenda_paciente
where	nr_sequencia 	= 	nr_sequencia_p;

qt_total_meses_pac_w := (qt_anos_pac_w * 12) + qt_meses_pac_w;

ds_insert_w := 'N';

open c01;
	loop
		fetch c01	into
			nr_seq_conjunto_w,
			qt_conjunto_w,
			ie_obrigatorio_w,
			nr_seq_grupo_w;
		exit when c01%notfound;
		begin
		
		select	nvl(max(cd_medico),'0')
		into	cd_medico_conjunto_w
		from	cm_conjunto
		where	nr_sequencia		= nr_seq_conjunto_w;
		
		if	(cd_medico_conjunto_w 	= '0') or
			(cd_medico_conjunto_w	= nvl(cd_medico_p, cd_medico_conjunto_w)) then
			begin
		
			/* Consistir conforme o par�metro [101] */
			if	(ie_consiste_cme_p <> 'N') AND
				(nr_seq_grupo_w = 0) then
				begin
				cme_consistir_conj_agenda(nr_sequencia_p, nr_seq_conjunto_w, 'S', nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ds_erro_cme_w);
				end;
			end if;
			
			if	(ie_consiste_cme_p <> 'N') and
				(nr_seq_grupo_w > 0) then
				begin
				cme_consistir_grupo_agenda(nr_sequencia_p, nr_seq_grupo_w, 'S', nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ds_erro_cme_w);
				end;
			end if;


			if	(ie_consiste_cme_p = 'N') or
				(ie_consiste_cme_p = 'A') or
				((ie_consiste_cme_p = 'S') and (ds_erro_cme_w is null)) then
				begin
				
				select	agenda_pac_cme_seq.nextval
				into	nr_sequencia_w
				from	dual;

				insert into agenda_pac_cme (
					nr_seq_conjunto,
					qt_conjunto,
					nr_sequencia,
					nr_seq_agenda,
					dt_atualizacao,
					nm_usuario,
					ie_origem_inf,
					ie_obrigatorio,
					nr_seq_proc_interno,
					nr_seq_grupo,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values(
					nr_seq_conjunto_w,
					qt_conjunto_w,
					nr_sequencia_w,
					nr_sequencia_p,
					sysdate,
					nm_usuario_p,
					'I',
					ie_obrigatorio_w,
					nr_seq_proc_interno_p,
					decode(nr_seq_grupo_w,0,null,nr_seq_grupo_w),
					sysdate,
					nm_usuario_p);
					
				ds_insert_w := 'S';	
					
				exit;
				end;
			end if;
			end;
		end if;
		end;
	end loop;
	close c01;
	
	if (ds_insert_w = 'N') then
		ds_erro_p := wheb_mensagem_pck.get_texto(278843);
	end if;

commit;

end Gerar_unico_cme_agenda;
/
