create or replace
procedure verifica_gerar_solic_urgente(	nr_solic_compra_p				Number,
				nr_item_solic_compra_p			Number,
				ie_urgente_p			out	varchar2,
				ie_considera_ent_urgente_p 		out	varchar2) is

cd_estabelecimento_w		number(5);
ie_considera_ent_urgente_w		varchar2(3);
ie_dia_util_ent_urgente_w		varchar2(1);
qt_dia_entrega_urgente_w		number(5);
qt_dias_util_w			number(5);
dt_inicial_w			date;
dt_final_w			date;
qt_entrega_w			number(3) := 0;
ie_retorno_w			varchar2(1);
nr_item_solic_compra_w		number(5);

cursor c01 is
select	nr_item_solic_compra
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p;

begin

ie_retorno_w	:= 'N';

select	cd_estabelecimento
into	cd_estabelecimento_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

select	nvl(max(ie_considera_ent_urgente),'N'),
	nvl(max(ie_dia_util_ent_urgente),'N'),
	nvl(max(qt_dia_entrega_urgente),0)
into	ie_considera_ent_urgente_w,
	ie_dia_util_ent_urgente_w,
	qt_dia_entrega_urgente_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;

if	(ie_considera_ent_urgente_w in ('S','SC','SL','SB')) then
	begin
	if	(qt_dia_entrega_urgente_w is not null) then
		begin
		if	(ie_dia_util_ent_urgente_w = 'S') then
			dt_inicial_w	:= trunc(sysdate,'dd');
			dt_final_w	:= dt_inicial_w + qt_dia_entrega_urgente_w;

			select	obter_qt_dia_util_periodo(dt_inicial_w,dt_final_w,cd_estabelecimento_w)
			into	qt_dias_util_w
			from	dual;

			qt_dia_entrega_urgente_w	:= qt_dia_entrega_urgente_w + ((dt_final_w - dt_inicial_w) - qt_dias_util_w);
		end if;

		if	(ie_considera_ent_urgente_w <> 'SB') then
		
			select	count(*)
			into	qt_entrega_w
			from	solic_compra c,
				solic_compra_item a,
				solic_compra_item_entrega b
			where 	a.nr_solic_compra 		= c.nr_solic_compra
			and	a.nr_solic_compra 		= b.nr_solic_compra(+)
			and 	a.nr_item_solic_compra 	= b.nr_item_solic_compra(+)
			and 	a.nr_solic_compra		= nr_solic_compra_p
			and	a.nr_item_solic_compra	= nr_item_solic_compra_p
			and	((b.dt_entrega_solicitada	<= trunc(sysdate) + qt_dia_entrega_urgente_w) or
				(a.dt_solic_item		<= trunc(sysdate) + qt_dia_entrega_urgente_w))
			and	((ie_considera_ent_urgente_w	= 'S') or
				(ie_considera_ent_urgente_w	= 'SC' and c.cd_centro_custo is not null) or
				(ie_considera_ent_urgente_w	= 'SL' and c.cd_local_estoque is not null));

		elsif	(ie_considera_ent_urgente_w = 'SB') then
			
			open C01;
			loop
			fetch C01 into	
				nr_item_solic_compra_w;
			exit when C01%notfound;
				begin
				
				if	(qt_entrega_w = 0) then
				
					select	count(*)
					into	qt_entrega_w
					from	solic_compra c,
						solic_compra_item a,
						solic_compra_item_entrega b
					where 	a.nr_solic_compra 		= c.nr_solic_compra
					and	a.nr_solic_compra 		= b.nr_solic_compra(+)
					and 	a.nr_item_solic_compra 	= b.nr_item_solic_compra(+)
					and 	a.nr_solic_compra		= nr_solic_compra_p
					and	a.nr_item_solic_compra	= nr_item_solic_compra_w
					and	((b.dt_entrega_solicitada	<= trunc(sysdate) + qt_dia_entrega_urgente_w) or
						(a.dt_solic_item		<= trunc(sysdate) + qt_dia_entrega_urgente_w));
						
				end if;
				end;
			end loop;
			close C01;			
		end if;
		
		if	(qt_entrega_w > 0) then
			ie_retorno_w	:= 'S';
		end if;
	
		end;
	end if;
	end;
end if;

ie_considera_ent_urgente_p	:= ie_considera_ent_urgente_w;
ie_urgente_p 		:= ie_retorno_w;

end verifica_gerar_solic_urgente;
/
