create or replace
procedure Fa_altera_status_paciente(status_p		Varchar2,	
				    nr_sequencia_p	number) is 
				    
ie_medic_nao_entregue_w		Varchar2(1);
				    
begin

if	(nr_sequencia_p is not null) then 
	
	if (status_p = 'PR') then
		update	fa_paciente_entrega 
		set	ie_status_paciente = status_p
		where 	nr_sequencia = nr_sequencia_p
		and	ie_status_paciente <> 'EN';
	else
		update	fa_paciente_entrega 
		set	ie_status_paciente = status_p
		where 	nr_sequencia = nr_sequencia_p;

	end if;
end if;

commit;

end Fa_altera_status_paciente;
/