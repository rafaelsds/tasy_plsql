create or replace
procedure TISS_ATUALIZAR_DESPESAS
		(nr_interno_conta_p	in	varchar2,
		 nm_usuario_p		in	varchar2) is

nr_sequencia_w			number(15,0);
ie_tiss_tipo_despesa_w		varchar2(40);
ie_tiss_tipo_despesa_ant_w		varchar2(40);
ie_classificacao_w			varchar2(40);
ie_forma_apresentacao_w		varchar2(40);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10);
ie_tipo_material_w			varchar2(40);
ie_tipo_classe_w			varchar2(40);
ie_tipo_despesa_tiss_w		varchar2(4);
ie_tipo_desp_regra_w		varchar2(4);
ie_tipo_desp_proc_regra_w		varchar2(50);
ie_tipo_material_conv_w		varchar2(40);
cd_convenio_w			number(10,0);
cd_estabelecimento_w		number(10,0);
cd_material_w			number(10,0);
cd_classe_material_w		number(10,0);
cd_subgrupo_material_w		number(10,0);
cd_grupo_material_w		number(10,0);
cd_area_proc_w			number(15,0);
cd_grupo_proc_w			number(15,0);
cd_especialidade_proc_w		number(15,0);
ie_tipo_atendimento_w		number(15,0);
vl_material_unitario_w		number(15,4);
ds_versao_w			varchar2(20);
nr_doc_convenio_w		procedimento_paciente.nr_doc_convenio%type;
cd_senha_item_w			procedimento_paciente.cd_senha%type;
ie_senha_taxa_guia_proc_w	tiss_parametros_convenio.ie_senha_taxa_guia_proc%type;

cursor c01 is
select	a.nr_sequencia,
	a.cd_procedimento,
	a.ie_origem_proced,
	obter_dados_estrut_proc(a.cd_procedimento, a.ie_origem_proced, 'C', 'A') cd_area_proc,
	obter_dados_estrut_proc(a.cd_procedimento, a.ie_origem_proced, 'C', 'G') cd_grupo_proc,
	obter_dados_estrut_proc(a.cd_procedimento, a.ie_origem_proced, 'C', 'E') cd_especialidade_proc,
	b.cd_estabelecimento,
	b.cd_convenio_parametro,
	a.nr_doc_convenio,
	a.ie_tiss_tipo_despesa
from	conta_paciente b,
	procedimento_paciente a
where	a.nr_interno_conta	= b.nr_interno_conta
and	(a.ie_tiss_tipo_guia	= '7' or
	(obter_se_projeto_versao(0,12,ds_versao_w,0) = 'N' and (a.ie_tiss_tipo_despesa in ('7','8','9'))))
and	a.nr_interno_conta	= nr_interno_conta_p;

cursor c02 is
select	a.nr_sequencia,
	b.ie_tipo_material,
	c.ie_tipo_classe,
	e.cd_convenio_parametro,
	e.cd_estabelecimento,
	b.cd_material,
	c.cd_classe_material,
	c.cd_subgrupo_material,
	g.cd_grupo_material,
	f.ie_tipo_material_conv,
	a.vl_unitario,
	a.ie_tiss_tipo_despesa
from	subgrupo_material g,
	classe_material c,
	material b,
	mat_atend_pac_convenio f,
	conta_paciente e,
	material_atend_paciente a
where	a.nr_interno_conta		= e.nr_interno_conta
and	a.cd_material			= b.cd_material
and	b.cd_classe_material		= c.cd_classe_material
and	c.cd_subgrupo_material		= g.cd_subgrupo_material
and	a.nr_sequencia 			= f.nr_seq_material(+)
and	a.nr_interno_conta		= nr_interno_conta_p;

begin

-- dsantos em 23/11/09 -> pegar tipo atend para passar na TISS_OBTER_REGRA_MAT_OPM
select	max(obter_tipo_atendimento(a.nr_atendimento)),
	max(tiss_obter_versao(a.cd_convenio_parametro,a.cd_estabelecimento,a.dt_mesano_referencia)),
	nvl(max(ie_senha_taxa_guia_proc),'N')
into	ie_tipo_atendimento_w,
	ds_versao_w,
	ie_senha_taxa_guia_proc_w
from	conta_paciente a,
	tiss_parametros_convenio b
where	a.cd_convenio_parametro	= b.cd_convenio
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_interno_conta	= nr_interno_conta_p;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_area_proc_w,
	cd_grupo_proc_w,
	cd_especialidade_proc_w,
	cd_estabelecimento_w,
	cd_convenio_w,
	nr_doc_convenio_w,
	ie_tiss_tipo_despesa_ant_w;
exit when c01%notfound;

	select	TISS_OBTER_REGRA_TIPO_PROC(	cd_convenio_w,
					cd_estabelecimento_w,
					cd_area_proc_w,
					cd_especialidade_proc_w,
					cd_grupo_proc_w,
					cd_procedimento_w)
	into	ie_tipo_desp_proc_regra_w
	from	dual;
	
	if	(nvl(ie_tipo_desp_proc_regra_w,'X') <> 'X') then

		if	(ie_tipo_desp_proc_regra_w	= '1') then
			ie_tiss_tipo_despesa_w	:= '2';
		elsif	(ie_tipo_desp_proc_regra_w	= '2') then
			ie_tiss_tipo_despesa_w	:= '3';
		elsif	(ie_tipo_desp_proc_regra_w	= '3') then
			ie_tiss_tipo_despesa_w	:= '9';
		elsif	(ie_tipo_desp_proc_regra_w	= '4') then
			ie_tiss_tipo_despesa_w	:= '6';	--Aluguel
		elsif	(ie_tipo_desp_proc_regra_w	= '5') then
			ie_tiss_tipo_despesa_w	:= '5';	--Diarias
		elsif	(ie_tipo_desp_proc_regra_w	= '6') then
			ie_tiss_tipo_despesa_w	:= '4';	--Taxas
		elsif	(ie_tipo_desp_proc_regra_w	= '7') then
			ie_tiss_tipo_despesa_w	:= '1';	--Gases medicinais		
		end if;

	else

		select	ie_classificacao,
			ie_forma_apresentacao
		into	ie_classificacao_w,
			ie_forma_apresentacao_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w;

		ie_tiss_tipo_despesa_w		:= null;
		if	(nvl(ie_forma_apresentacao_w, 0) in(3, 2)) then
			ie_tiss_tipo_despesa_w	:= '1';			-- gases
		elsif	(nvl(ie_classificacao_w, '0') = '3') then
			ie_tiss_tipo_despesa_w	:= '5';			-- di�rias
		else
			ie_tiss_tipo_despesa_w	:= '4';			-- taxas
		end if;
		
		/*Este n�o tem prioridade sobre a regra, deve considerar somente se n�o existir regra TISS_OBTER_REGRA_TIPO_PROC*/
		select	max(ie_tipo_despesa_tiss)
		into	ie_tipo_despesa_tiss_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w;

		if	(ie_tipo_despesa_tiss_w is not null) then
			ie_tiss_tipo_despesa_w	:= ie_tipo_despesa_tiss_w;
		end if;

	end if;

	if	(obter_se_projeto_versao(0,12,ds_versao_w,0) = 'S') then
		if	(ie_tiss_tipo_despesa_w in ('4','6')) then --Taxas Diversas, Aluguel
			ie_tiss_tipo_despesa_w	:= '7'; --Taxas e alugu�is
		elsif	(ie_tiss_tipo_despesa_w in ('7','8','9')) then --�rtese, Pr�tese, Materiais especiais
			ie_tiss_tipo_despesa_w	:= '8'; --OPME
		end if;
	end if;
	
	if	(ie_senha_taxa_guia_proc_w = 'S') and
		(((obter_se_projeto_versao(0,12,ds_versao_w,0) = 'S') and 
		  (ie_tiss_tipo_despesa_w in ('5','7'))) or --Di�rias, Taxas e alugu�is
		 ((obter_se_projeto_versao(0,12,ds_versao_w,0) = 'N') and 
		  (ie_tiss_tipo_despesa_w in ('4','5','6')))) then --Taxas Diversas, Di�rias, Aluguel
		
		begin
			select	cd_senha
			into	cd_senha_item_w
			from	procedimento_paciente
			where	nr_interno_conta	= nr_interno_conta_p
			and	nr_doc_convenio		= nr_doc_convenio_w
			and	cd_motivo_exc_conta	is null
			and	cd_senha 		is not null
			and	rownum			= 1;			
		exception
		when others then
			cd_senha_item_w	:= null;
		end;
		
		if	(cd_senha_item_w is not null) then
		
			update	procedimento_paciente
			set	cd_senha	= cd_senha_item_w
			where	nr_sequencia	= nr_sequencia_w;
		
		end if;
	end if;

	if	(nvl(ie_tiss_tipo_despesa_ant_w,'XPTO')	<> nvl(ie_tiss_tipo_despesa_w,'XPTO')) then
	
		update	procedimento_paciente
		set	ie_tiss_tipo_despesa	= ie_tiss_tipo_despesa_w
		where	nr_sequencia		= nr_sequencia_w;
		
	end if;
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end loop;
close c01;

open c02;
loop
fetch c02 into
	nr_sequencia_w,
	ie_tipo_material_w,
	ie_tipo_classe_w,
	cd_convenio_w,
	cd_estabelecimento_w,
	cd_material_w,
	cd_classe_material_w,
	cd_subgrupo_material_w,
	cd_grupo_material_w,
	ie_tipo_material_conv_w,
	vl_material_unitario_w,
	ie_tiss_tipo_despesa_ant_w;
exit when c02%notfound;

	ie_tiss_tipo_despesa_w	:= null;
	ie_tipo_material_w	:= nvl(ie_tipo_material_conv_w, ie_tipo_material_w);

	select	max(TISS_OBTER_REGRA_MAT_OPM(	cd_convenio_w,
						cd_estabelecimento_w,
						cd_material_w,
						cd_classe_material_w,
						cd_subgrupo_material_w,
						cd_grupo_material_w,
						ie_tipo_atendimento_w,
						vl_material_unitario_w))
	into	ie_tipo_desp_regra_w
	from	dual;

	if	(nvl(ie_tipo_desp_regra_w,'X') not in ('X','N','S')) then

		if	(ie_tipo_desp_regra_w	= '1') then
			ie_tiss_tipo_despesa_w	:= '1';
		elsif	(ie_tipo_desp_regra_w	= '2') then
			ie_tiss_tipo_despesa_w	:= '2';
		elsif	(ie_tipo_desp_regra_w	= '3') then
			ie_tiss_tipo_despesa_w	:= '3';
		elsif	(ie_tipo_desp_regra_w	= '4') then
			ie_tiss_tipo_despesa_w	:= '4';
		end if;

	else

		if	(ie_tipo_classe_w = 'G') then
			ie_tiss_tipo_despesa_w		:= '1';
		elsif	(ie_tipo_classe_w = 'O') then		-- Edgar 16/01/2009, OS 123077, inclu�do tratamento de OPM
			ie_tiss_tipo_despesa_w		:= '7';
		elsif	(ie_tipo_classe_w = 'P') then
			ie_tiss_tipo_despesa_w		:= '8';
		elsif	(ie_tipo_classe_w = 'M') then
			ie_tiss_tipo_despesa_w		:= '9';
		else
			if	(ie_tipo_material_w = '1') then
				ie_tiss_tipo_despesa_w	:= '3';				-- Alterado por lhalves em 05/08/2014 OS 765853 - considerar todos os tipos de medicamento.
			elsif	(ie_tipo_material_w in ('0','2','3','6','8','9')) then 	--0 - Medicamento Manipulado, 2 - Medicamento Comercial, 3 - Medicamento Gen�rico, 6 - Medicamento sem apresenta��o,
				ie_tiss_tipo_despesa_w	:= '2';				--8 - Medicamento exclusivo para faturamento, 9 - Medicamento Similar
			else
				ie_tiss_tipo_despesa_w	:= '3';
			end if;
		end if;
	end if;

	if	(obter_se_projeto_versao(0,12,ds_versao_w,0) = 'S') then
		if	(ie_tiss_tipo_despesa_w in ('4','6')) then --Taxas Diversas, Aluguel
			ie_tiss_tipo_despesa_w	:= '7'; --Taxas e alugu�is
		elsif	(ie_tiss_tipo_despesa_w in ('7','8','9')) then --�rtese, Pr�tese, Materiais especiais
			ie_tiss_tipo_despesa_w	:= '8'; --OPME
		elsif	(nvl(ie_tipo_desp_regra_w,'X') = 'S') then --Se tiver regra dizendo que � OPME para o TISS 3, trata como 8
			ie_tiss_tipo_despesa_w	:= '8'; --OPME
		end if;
	end if;

	if	(nvl(ie_tiss_tipo_despesa_ant_w,'XPTO')	<> nvl(ie_tiss_tipo_despesa_w,'XPTO')) then
	
		update	material_atend_paciente
		set	ie_tiss_tipo_despesa	= ie_tiss_tipo_despesa_w
		where	nr_sequencia		= nr_sequencia_w;

	end if;
	
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end loop;
close c02;

end TISS_ATUALIZAR_DESPESAS;
/