
create or replace
procedure atualizar_proc_partic_cirurgia(	nr_cirurgia_p		NUMBER,
						cd_estabelecimento_p	NUMBER,
						nm_usuario_p		VARCHAR2)
						as


nr_seq_procedimento_w		NUMBER(10);
ie_funcao_w			VARCHAR2(10);
cd_pessoa_fisica_w		VARCHAR2(10);
nm_participante_w		VARCHAR2(60);
ds_observacao_w			VARCHAR2(255);
ie_existe_w			VARCHAR2(1);
dt_entrada_w			date;
dt_saida_w			date;
nr_seq_interno_w		NUMBER(10);
nr_sequencia_w			NUMBER(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_convenio_w			procedimento_paciente.cd_convenio%type;

						
						
cursor	c01 is
	SELECT 	distinct
		a.cd_procedimento,
		a.ie_origem_proced
	FROM 	procedimento b,
		procedimento_paciente a,
		conta_paciente c
	WHERE	a.cd_procedimento 	  = b.cd_procedimento
	AND	a.ie_origem_proced 	  = b.ie_origem_proced
	AND	a.nr_interno_conta	  = c.nr_interno_conta
	AND	a.nr_cirurgia 		  = nr_cirurgia_p
	AND	ie_classificacao 	  = '1'
	AND	cd_motivo_exc_conta IS NULL
	AND	c.ie_status_acerto 	  = 1
	ORDER BY 1;
	
cursor	c02 is
	select	ie_funcao,
		cd_pessoa_fisica,
		nm_participante,
		ds_observacao,
		dt_entrada,
		dt_saida,
		nr_seq_interno
	from	cirurgia_participante
	where	nr_cirurgia 		= nr_cirurgia_p;
						
begin

open c01;
loop
fetch c01 into
	cd_procedimento_w,
	ie_origem_proced_w;
exit when c01%notfound;
	begin
	SELECT 	max(a.nr_sequencia)
	into	nr_seq_procedimento_w
	FROM 	procedimento b,
		procedimento_paciente a,
		conta_paciente c
	WHERE	a.cd_procedimento 	= cd_procedimento_w
	AND	a.ie_origem_proced 	= ie_origem_proced_w
	AND	a.nr_interno_conta	  = c.nr_interno_conta
	AND	a.nr_cirurgia 		= nr_cirurgia_p
	AND	ie_classificacao 	= '1'
	AND	cd_motivo_exc_conta 	IS NULL
	AND	c.ie_status_acerto 	  = 1;
	
	open c02;
	loop
	fetch c02 into
		ie_funcao_w,
		cd_pessoa_fisica_w,
		nm_participante_w,
		ds_observacao_w,
		dt_entrada_w,
		dt_saida_w,
		nr_seq_interno_w;
	exit when c02%notfound;
		begin
		select	nvl(max('S'),'N')
		into	ie_existe_w
		from	cirurgia_participante
		where	nvl(cd_pessoa_fisica, 'XPTO') = nvl(cd_pessoa_fisica_w,'XPTO')
		and	nr_cirurgia		= nr_cirurgia_p
		and	nr_seq_procedimento	= nr_seq_procedimento_w;
		
		if	(ie_existe_w = 'N') then
			update	cirurgia_participante
			set	nr_seq_procedimento 	= nr_seq_procedimento_w
			where	nr_cirurgia 		= nr_cirurgia_p
			and	nr_seq_procedimento 	is null;
		end if;	
		
		select	nvl(max('S'),'N')
		into	ie_existe_w
		from	cirurgia_participante
		where	nvl(cd_pessoa_fisica, 'XPTO') = nvl(cd_pessoa_fisica_w,'XPTO')
		and	nr_cirurgia		= nr_cirurgia_p
		and	nr_seq_procedimento	= nr_seq_procedimento_w;
		
		if	(ie_existe_w = 'N') then
			select	nvl(max(nr_sequencia),1)+1
			into	nr_sequencia_w
			from	cirurgia_participante
			where	nr_cirurgia = nr_cirurgia_p;

			insert	into 	cirurgia_participante(
					nr_cirurgia,
					nr_sequencia,
					ie_funcao,
					nm_usuario,
					dt_atualizacao,
					cd_pessoa_fisica,
					nm_participante,
					ds_observacao,
					dt_entrada,
					dt_saida,
					nr_seq_procedimento,
					nr_seq_interno,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_situacao
					)
			select		nr_cirurgia_p,
					nr_sequencia_w,
					ie_funcao_w,
					nm_usuario_p,
					sysdate,
					cd_pessoa_fisica_w,
					nm_participante_w,
					ds_observacao_w,
					dt_entrada_w,
					dt_saida_w,
					nr_seq_procedimento_w,
					cirurgia_participante_seq.nextval,
					sysdate,
					nm_usuario_p,
					'A' from dual;
		end if;			
		end;
	end loop;
	close c02;
	end;
	
	if	(nvl(nr_seq_procedimento_w,0) > 0) then
		select	max(cd_convenio)
		into	cd_convenio_w
		from	procedimento_paciente
		where	nr_sequencia = nr_seq_procedimento_w;
		
		atualiza_preco_procedimento(nr_seq_procedimento_w, cd_convenio_w, nm_usuario_p);
	end if;
end loop;
close c01;

commit;
	
end atualizar_proc_partic_cirurgia;
/
