create or replace procedure gerar_ret_cobr_unicred_400(   nr_seq_cobr_escrit_p     number,
                      nm_usuario_p          varchar2) is

ds_titulo_w          varchar2(15);
ds_titulo_1_w          varchar2(15);
ds_dt_liquidacao_w     varchar2(6);
vl_cobranca_w          number(15,2);
vl_alterar_w          number(15,2);
vl_titulo_w          number(15,2);
vl_acrescimo_w          number(15,2);
vl_desconto_w          number(15,2);
vl_abatimento_w        number(15,2);
vl_liquido_w          number(15,2);
nr_seq_reg_T_w        number(10);
nr_seq_reg_U_w        number(10);
nr_titulo_w          number(10);
cd_ocorrencia_w        number(10);
nr_seq_ocorrencia_ret_w   number(10);
dt_liquidacao_w          date;
vl_saldo_inclusao_w  titulo_receber.vl_saldo_titulo%type;
cd_banco_w				banco_escritural.cd_banco%type;

cursor C01 is
select  trim(substr(ds_string,71,11)), /*Identifica��o do t�tulo no banco*/
    to_number(substr(ds_string,127,13))/100, /*Identifica��o do t�tulo no banco*/
    to_number(substr(ds_string,267,13))/100,/*Juros acr�scimos*/
    to_number(substr(ds_string,241,13))/100,/*desconto*/
    to_number(substr(ds_string,228,13))/100,/*Abatimento*/
    to_number(substr(ds_string,254,13))/100,/*valor pago liquido*/
    substr(ds_string,111,6),/*data cr�dito*/
    to_number(substr(ds_string,109,2))/*Identificacao da ocorrencia*/
from    w_retorno_banco
where    nr_seq_cobr_escrit  = nr_seq_cobr_escrit_p
and    substr(ds_string,1,1)  = '1';

begin
select 	max(a.cd_banco)
into	cd_banco_w
from	cobranca_escritural a
where	nr_sequencia = nr_seq_cobr_escrit_p;

open C01;
loop
fetch C01 into
  ds_titulo_w,
  vl_cobranca_w,
  vl_acrescimo_w,
  vl_desconto_w,
  vl_abatimento_w,
  vl_liquido_w,
  ds_dt_liquidacao_w,
  cd_ocorrencia_w;
exit when C01%notfound;
  begin
  vl_alterar_w  := 0;

  select  max(nr_titulo)
  into    nr_titulo_w
  from    titulo_receber
  where    nr_titulo  = ds_titulo_w;

  if  (nr_titulo_w is null) then /* Quando n�o h� t�tulo, busca pelo nosso numero */
   ds_titulo_1_w := substr(ds_titulo_w,1,11) || '%';
   
    select  max(nr_titulo)
    into    nr_titulo_w
    from    titulo_receber
    where    nr_nosso_numero  like  ds_titulo_1_w;

  end if;

  /* Se encontrou o t�tulo importa, sen�o grava no log */

  if  (nr_titulo_w is not null) then
    select  max(vl_titulo),
      max(vl_saldo_titulo)
    into  vl_titulo_w,
      vl_saldo_inclusao_w
    from  titulo_receber
    where  nr_titulo  = nr_titulo_w;

	select   nvl(to_char(max(a.nr_sequencia)),0)
    into  nr_seq_ocorrencia_ret_w
    from  banco_ocorr_escrit_ret a
    where  a.cd_banco = cd_banco_w
    and  a.cd_ocorrencia = cd_ocorrencia_w;

    /* Tratar acrescimos/descontos */
    if  (vl_titulo_w <> vl_liquido_w) then
      vl_alterar_w  := vl_liquido_w - vl_titulo_w;

      if  (vl_alterar_w > 0) then
        --vl_acrescimo_w  := vl_alterar_w;
		vl_acrescimo_w := 0;
      /*else
        vl_desconto_w  := abs(vl_alterar_w);*/
      end if;
    end if;

    if  (ds_dt_liquidacao_w  = '000000') or
      (ds_dt_liquidacao_w  = '999999') or
      (ds_dt_liquidacao_w  = '      ')then
      dt_liquidacao_w  := sysdate;
    else
      begin

      dt_liquidacao_w  := to_date(ds_dt_liquidacao_w,'ddmmyy');

      exception
      when others then

      /* Data de vencimento inv�lida!
      T�tulo: nr_titulo_w
      Data: ds_dt_liquidacao_w */
      wheb_mensagem_pck.exibir_mensagem_abort(242915,
              'NR_TITULO_W='||nr_titulo_w||';'||
              'DS_DT_LIQUIDACAO_W='||ds_dt_liquidacao_w);

      end;
    end if;

   insert  into titulo_receber_cobr
                  (  nr_sequencia,
                    nr_titulo,
                    cd_banco,
                    vl_cobranca,
                    vl_desconto,
                    vl_acrescimo,
                    vl_despesa_bancaria,
                    vl_liquidacao,
                    dt_liquidacao,
                    dt_atualizacao,
                    nm_usuario,
                    nr_seq_cobranca,
                    nr_seq_ocorrencia_ret,
                    vl_saldo_inclusao)
              values  (  titulo_receber_cobr_seq.nextval,
                    nr_titulo_w,
                    139,
                    vl_titulo_w,
                    0,--vl_desconto_w, --Andre - TI-90918
                    0,--vl_acrescimo_w, --Andre - TI-90918
                    0,
                    vl_liquido_w,
                    dt_liquidacao_w,
                    sysdate,
                    nm_usuario_p,
                    nr_seq_cobr_escrit_p,
                    nr_seq_ocorrencia_ret_w,
                    vl_saldo_inclusao_w);
  else
    /*fin_gerar_log_controle_banco(3,substr(wheb_mensagem_pck.get_texto(303847, 'DS_TITULO_W=' || ds_titulo_w),1,4000),nm_usuario_p,'S');*/
    insert into cobranca_escrit_log
                  (  nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    nr_seq_cobranca,
                    ds_log,
                    vl_saldo_titulo,
                    nr_seq_ocorrencia_ret,
                    nr_nosso_numero)
              values  (  cobranca_escrit_log_seq.nextval,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    nr_seq_cobr_escrit_p,
                    'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy',
                    vl_liquido_w,
                    nr_seq_ocorrencia_ret_w,
                    ds_titulo_w);
  end if;

  end;
end loop;
close C01;

commit;

end gerar_ret_cobr_unicred_400;

/