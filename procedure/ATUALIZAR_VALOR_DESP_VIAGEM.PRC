create or replace
procedure atualizar_valor_desp_viagem(  nr_sequencia_p		number,
										nr_seq_relat_desp_p number,
										vl_pago_terceiro_p	number,
										nm_usuario_p		varchar2) is

vl_total_desp_w			number(15,2);
ie_pago_terceiro_w		via_relat_desp.ie_pago_terceiro%type;
nr_seq_viagem_w			via_relat_desp.nr_seq_viagem%type;	
ds_retorno_w			varchar2(255);

begin

if (nr_sequencia_p is not null) then

	/*Atualizar  se foi pagou ou nao por terceiro*/
	update via_relat_desp
    set    ie_pago_terceiro 	= decode(nvl(ie_pago_terceiro, 'N'), 'N', 'S', 'N')
    where  nr_sequencia 		= nr_seq_relat_desp_p
    and    nr_seq_fech_proj 	=  nr_sequencia_p;

	 /*Pegar a situacao atualizada da GV*/
	select	max(ie_pago_terceiro)
	into	ie_pago_terceiro_w
	from	via_relat_desp
	where  nr_sequencia 		= nr_seq_relat_desp_p
    and    nr_seq_fech_proj 	=  nr_sequencia_p;

	if (ie_pago_terceiro_w = 'S') then

		/*Buscar o valor total das despesas dessa viagem para deduzir a que esta sendo paga pelo terceiro*/
		select  nvl(sum(obter_valor_desp_viagem(a.nr_sequencia,'4')),0)
		into	vl_total_desp_w
		from    via_relat_desp a
		where   a.nr_seq_fech_proj = nr_sequencia_p
		and		a.nr_sequencia <> nr_sequencia_p
		and		nvl(a.ie_pago_terceiro,'N') <> 'S';

		update 	proj_desp_viagem_mes
		set		vl_total_pagar 	= vl_total_desp_w,
				nm_usuario 		= nm_usuario_p,
				dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_sequencia_p;
		
		select	max(a.nr_seq_viagem)
		into	nr_seq_viagem_w
		from	via_relat_desp a
		where	a.nr_sequencia	=	nr_seq_relat_desp_p; 

		if (nr_seq_viagem_w is not null) then
		
			ds_retorno_w := null;
			
			gerenciar_viagem (nr_seq_viagem_w,
							  nm_usuario_p,
							  'F', 
							  ds_retorno_w,
							  'N'); --N�o para o commit. Pois commit ocorre no final dessa proc aqui..
					
			if (ds_retorno_w is not null) then
				wheb_mensagem_pck.exibir_mensagem_abort(1020093,'ds_retorno_p='||ds_retorno_w);
			end if;	

		end if;
		
	elsif (ie_pago_terceiro_w = 'N') then

		select  nvl(sum(obter_valor_desp_viagem(a.nr_sequencia,'4')),0)
		into	vl_total_desp_w
		from    via_relat_desp a
		where   a.nr_seq_fech_proj = nr_sequencia_p
		and		a.nr_sequencia <> nr_seq_relat_desp_p
		and		nvl(a.ie_pago_terceiro,'N') = 'N';

		update 	proj_desp_viagem_mes
		set		vl_total_pagar 	= nvl(vl_total_desp_w,0) + nvl(vl_pago_terceiro_p,0),
				nm_usuario 		= nm_usuario_p,
				dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_sequencia_p;
		
		/*OS 1670837 - Desfazer finaliza��o da viagem quando desfazer pago terceiro.*/
		select	max(a.nr_seq_viagem)
		into	nr_seq_viagem_w
		from	via_relat_desp a
		where	a.nr_sequencia	=	nr_seq_relat_desp_p;
		
		if (nr_seq_viagem_w is not null) then
		
				update	via_viagem
				set		ie_etapa_viagem			= '5',
						dt_fim_viagem			= null,
						nm_usuario_fim_viagem	= null,
						dt_atualizacao			= sysdate
				where	nr_sequencia			= nr_seq_viagem_w;
		
		end if;

	end if;

	commit;

end if;

end atualizar_valor_desp_viagem;
/