create or replace
procedure ctb_gerar_reg_fim_bloco_ecd(	nr_seq_controle_p		number,
					nm_usuario_p		varchar2,
					ds_separador_p		varchar2,
					cd_registro_p		varchar2,
					qt_linha_p	in out	number,
					nr_sequencia_p	in out	number) is 

qt_linhas_bloco_w				number(10);
ds_arquivo_w				varchar2(4000);
ds_compl_arquivo_w			varchar2(4000);
ds_linha_w				varchar2(8000);
nr_linha_w				number(10) 	:= qt_linha_p;
nr_seq_registro_w				number(10) 	:= nr_sequencia_p;
sep_w					varchar2(1)	:= ds_separador_p;
cd_registro_w				varchar2(15) 	:= cd_registro_p;

begin

if	(cd_registro_w = '9999') then
	begin
	select	count(*)
	into	qt_linhas_bloco_w
	from	ctb_sped_registro
	where	nr_seq_controle_sped	= nr_seq_controle_p;
	end;
else
	begin
	select	count(*)
	into	qt_linhas_bloco_w
	from	ctb_sped_registro
	where	nr_seq_controle_sped	= nr_seq_controle_p
	and	substr(cd_registro,1,1) = substr(cd_registro_w,1,1);

	if (cd_registro_w = '9990') then
	   qt_linhas_bloco_w	:= qt_linhas_bloco_w + 1;
	end if;
	
	end;
end if;

qt_linhas_bloco_w	:= qt_linhas_bloco_w + 1;

ds_linha_w		:= substr(	sep_w || cd_registro_w	|| 
				sep_w || qt_linhas_bloco_w	|| sep_w ,1,8000);

ds_arquivo_w		:= substr(ds_linha_w,1,4000);
nr_seq_registro_w		:= nr_seq_registro_w + 1;
nr_linha_w		:= nr_linha_w + 1;
		
insert into ctb_sped_registro(
	nr_sequencia,
	ds_arquivo,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_controle_sped,
	ds_arquivo_compl,
	cd_registro,
	nr_linha)
values(	nr_seq_registro_w,
	ds_arquivo_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_controle_p,
	ds_compl_arquivo_w,
	cd_registro_w,
	nr_linha_w);

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end ctb_gerar_reg_fim_bloco_ecd;
/