create or replace
procedure ajustar_qtde_proc_paciente(
			nr_interno_conta_p	number,
			cd_medico_executor_p	varchar2,
			dt_acerto_conta_p	date,
			dt_procedimento_p	date,	
			cd_procedimento_p	number,
			ie_origem_proced_p	number,
			cd_medico_req_p		varchar2,
			nr_seq_exame_p	       	number,
			ie_tecnica_utilizada_p	varchar2,
			qt_procedimento_p	number,
			cd_setor_atendimento_p	number,
			qt_devolvida_p		number,
			cd_acao_p		number,
			nm_usuario_p		varchar2,
			nr_atendimento_p	number,
			nr_seq_proc_interno_p 	number,
			ie_funcao_medico_p	varchar2,
			ie_via_acesso_p		varchar2,
			vl_procedimento_p	number) is

dt_entrada_unidade_w		date;
nr_seq_cor_exec_w		number(15)	:= 369;
nr_seq_interno_w		number(10);

begin

select  max(a.dt_entrada_unidade), 
	max(a.nr_seq_interno)
into	dt_entrada_unidade_w,
	nr_seq_interno_w
from	tipo_acomodacao b,
	setor_atendimento c,
	atend_paciente_unidade a
where	a.cd_tipo_acomodacao	= b.cd_tipo_acomodacao
and	a.cd_setor_atendimento  = c.cd_setor_atendimento
and	nr_atendimento		= nr_atendimento_p
and	a.cd_setor_atendimento  = cd_setor_atendimento_p;


insert into procedimento_paciente( 
			nr_sequencia,
			nr_atendimento,
			dt_entrada_unidade,
			dt_conta,
			cd_procedimento,
			dt_procedimento,
			ie_origem_proced,
			nr_seq_exame,
			nr_seq_proc_interno,
			qt_procedimento,
			cd_setor_atendimento,
			nr_seq_atepacu,
			nr_seq_cor_exec,
			ie_funcao_medico,
			nr_interno_conta,
			cd_medico_executor,
			dt_acerto_conta,
			cd_medico_req,
			ie_tecnica_utilizada,
			qt_devolvida,
			cd_acao,
			ie_via_acesso,
			vl_procedimento,
			dt_atualizacao,
			nm_usuario)
		values( 
			procedimento_paciente_seq.nextval,
			nr_atendimento_p,
			dt_entrada_unidade_w,
			dt_procedimento_p,
			cd_procedimento_p,
			dt_procedimento_p,
			ie_origem_proced_p,
			decode(nr_seq_exame_p, 0, null, nr_seq_exame_p),
			decode(nr_seq_proc_interno_p, 0, null, nr_seq_proc_interno_p),
			qt_procedimento_p,
			cd_setor_atendimento_p,
			nr_seq_interno_w,
			nr_seq_cor_exec_w,
			decode(ie_funcao_medico_p, '', null, ie_funcao_medico_p),
			nr_interno_conta_p,
			cd_medico_executor_p,
			dt_acerto_conta_p,
			decode(cd_medico_req_p, '', null, cd_medico_req_p),
			ie_tecnica_utilizada_p,
			qt_devolvida_p,
			cd_acao_p,
			decode(ie_via_acesso_p, '', null, ie_via_acesso_p),
			nvl(vl_procedimento_p, 0),
			sysdate,
			nm_usuario_p);

commit;

end ajustar_qtde_proc_paciente;
/