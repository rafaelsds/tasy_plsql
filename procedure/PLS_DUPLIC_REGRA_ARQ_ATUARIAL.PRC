create or replace
procedure pls_duplic_regra_arq_atuarial(	nr_seq_regra_p		in pls_atuarial_arq_regra.nr_sequencia%type,
						nm_usuario_p		in usuario.nm_usuario%type,
						nr_seq_regra_nova_p	out pls_atuarial_arq_regra.nr_sequencia%type) is

begin

pls_dados_atuariais_pck.duplicar_regra_arq(	nr_seq_regra_p,
						nm_usuario_p,
						nr_seq_regra_nova_p);

end pls_duplic_regra_arq_atuarial;
/