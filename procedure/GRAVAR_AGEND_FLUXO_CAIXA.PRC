create or replace procedure gravar_agend_fluxo_caixa(nr_documento_p		number,
							nr_docto_compl_p	number,
							ie_origem_info_p	varchar2,
							dt_referencia_info_p	date,
							ie_tipo_reg_p		varchar2,
							nm_usuario_p		varchar2,
							ie_commit_p		varchar2 default 'N') is
							
qt_registros_w			number(10) := 0;

/* ---- Tipo de registro - ie_tipo_reg_p -----
-- I - inclus�o
-- A - altera��o
-- E - exclus�o
-- C - Carga inicial do fluxo
*/

/* ---- Status do agendamento - ie_status_agend ----
-- P - Pendente
-- E - Erro
-- R - Processado
-- A - Em processamento
*/

/* -------- Origem da Informa��o -------- */
/* -- Identifica de onde vem a informa��o que ser� gerada no fluxo de caixa -- 
 XFLCX - Carga do fluxo de caixa
 TP - T�tulo a pagar
 TPB - Baixa do t�tulo a pagar
 TPBA - Baixa do t�tulo a pagar por adiantamento
 TR - T�tulo a receber
 TRB - Baixa do t�tulo a receber
 TRT - Tributos a pagar (gerados pelo t�tulo a receber)
 CR - Cheque a receber
 CP - Cheque a pagar
 CC - Cart�o
 CCB - Baixa de cart�o
 RC - Recebimento conv�nio
 CBT - Controle banc�rio e Tesouraria
 PR - Projeto recurso
 BP - Border� a pagar - Entra pelo t�tulo
 PE - Pagamento escritural - Entra pelo t�tulo
 PC - Protocolo conv�nio
 GR - Glosas a recuperar - Entra pelo t�tulo
 GEF - Gest�o de empr�stimos e financiamentos
 OC - Ordem de compra
 CO - Contrato
 PAC - Conta paciente
 RT - Repasse terceiro
 AG - Agenda
*/
/* Na carga inicial do fluxo de caixa, passar sempre o tipo de registro 'C', a origem da informa��o sempre 'XFLCX', a data de refer�ncia sysdate e o nm_usuario sempre o usu�rio que requisitou a carga, para que o mesmo possa receber um comunicado interno de finaliza��o da carga. 
     N�mero de documento deve ser passado null neste caso, pois ser�o verificados todos os registros do sistema. */

begin
if	(wheb_usuario_pck.get_ie_lote_contabil() = 'N') then
	begin

	-- Verifica se existem registros pendentes de processamento para o documento
	select	count(*)
	into	qt_registros_w
	from	fluxo_caixa_agendamento
	where	nr_documento = nr_documento_p
	and	nvl(nr_docto_compl,0) = nvl(nr_docto_compl_p,0)
	and	ie_origem_info = ie_origem_info_p
	and	ie_status_agend = 'P';
	
	-- Se houver registro n�o processado e o registro estiver sendo exclu�do, exclui o agendamento do documento
	if (qt_registros_w > 0 and ie_tipo_reg_p = 'E') then
		begin
			delete from fluxo_caixa_agendamento
			where	nr_documento = nr_documento_p
			and	nr_docto_compl = nr_docto_compl_p
			and	ie_origem_info = ie_origem_info_p
			and	ie_status_agend = 'P';
			
			
		exception when others then
			null;
		end;
	end if;
	
	-- Em caso de exclus�o do documento, excluir seus registros do fluxo de caixa
	if (ie_tipo_reg_p = 'E') then
		remove_docto_fluxo_caixa(nr_documento_p,
					nr_docto_compl_p,
					ie_origem_info_p);
	end if;
	
	-- Caso n�o exista agendamento pendente para o documento e o mesmo n�o for exclus�o, inclui no agendamento
	if (qt_registros_w = 0 and ie_tipo_reg_p <> 'E') then
		begin
			insert into fluxo_caixa_agendamento(
				nr_sequencia,
				nr_documento,
				nr_docto_compl,
				dt_referencia_info,
				dt_agendamento,
				ie_origem_info,
				ie_status_agend,
				nm_usuario,
				dt_atualizacao)
			values (fluxo_caixa_agendamento_seq.nextval,
				nr_documento_p,
				nr_docto_compl_p,
				dt_referencia_info_p,
				sysdate,
				ie_origem_info_p,
				'P',
				nm_usuario_p,
				sysdate);
			
			
		end;
	end if;
	
	if (ie_commit_p = 'S') then
		commit;
	end if;
	
	end;
end if;
end gravar_agend_fluxo_caixa;
/
