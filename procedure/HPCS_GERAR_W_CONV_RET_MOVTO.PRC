Create or replace
procedure HPCS_GERAR_W_CONV_RET_MOVTO (nr_seq_retorno_p	number) is 

nr_registro_w		varchar2(255);
cd_prestador_w		varchar2(255);
vl_total_pago_w		varchar2(255);
cd_autorizacao_w		varchar2(255);
nr_doc_convenio_w	varchar2(255);
vl_glosa_w		varchar2(255);
vl_cobrado_w		varchar2(255);
vl_total_pago_55_w	varchar2(255);
vl_glosa_55_w		varchar2(255);
nr_doc_convenio_princ_w	varchar2(255);
cd_autorizacao_princ_w	varchar2(255);
cd_usuario_convenio_w	varchar2(255);
ds_complemento_w	varchar2(255);
cd_item_w		varchar2(255);
cd_convenio_w		varchar2(255);
ds_item_w		varchar2(255);
sqlerrm_w		varchar2(255);
nr_sequencia_w		number(10,0);

cursor c01 is
select	nr_sequencia,
	substr(ds_conteudo,01,02),
	'',
	'',
	substr(ds_conteudo,49,5),
	substr(ds_conteudo,49,5),
	somente_numero(substr(ds_conteudo,54,13)),
	somente_numero(substr(ds_conteudo,67,9)),
	'',
	to_number(0),
	to_number(0),
	''
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and	substr(ds_conteudo,3,02) 	= '35'
and	substr(ds_conteudo,1,02) 	= '00'
union all
select	nr_sequencia,
	'',
	substr(ds_conteudo,3,13),
	'',
	'',
	'',
	to_number(0),
	to_number(0),
	substr(ds_conteudo,39,43),
	somente_numero(substr(ds_conteudo,145,13)),
	somente_numero(substr(ds_conteudo,158,13)),
	substr(ds_conteudo,26,8)
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and	substr(ds_conteudo,3,02) 	= '35'
and	substr(ds_conteudo,1,02) 	between '01' and '21'
union all
select	nr_sequencia,
	substr(ds_conteudo,01,02),
	'',
	substr(ds_conteudo,9,06),
	substr(ds_conteudo,47,5),
	substr(ds_conteudo,47,5),
	somente_numero(substr(ds_conteudo,52,13)),
	somente_numero(substr(ds_conteudo,65,9)),
	'',
	to_number(0),
	to_number(0),
	''
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and	(substr(ds_conteudo,3,02) = '55') 
and 	(substr(ds_conteudo,01,02) = '00')
union all
select	nr_sequencia,
	'',
	substr(ds_conteudo,3,13),
	'',
	'',
	'',
	TO_NUMBER(0),
	TO_NUMBER(0),
	substr(ds_conteudo,55,45),
	somente_numero(substr(ds_conteudo,113,13)),
	somente_numero(substr(ds_conteudo,126,09)),
	substr(ds_conteudo,18,8)
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and 	substr(ds_conteudo,01,02)  between '01' and '21'
union all
select	nr_sequencia,
	substr(ds_conteudo,14,02),
	substr(ds_conteudo,20,13),
	substr(ds_conteudo,41,06),
	substr(ds_conteudo,68,05),
	substr(ds_conteudo,68,05),
	somente_numero(substr(ds_conteudo,146,13)),
	somente_numero(substr(ds_conteudo,159,9)),
	'',
	to_number(0),
	to_number(0),
	''
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and	(substr(ds_conteudo,16,02) = '75' or substr(ds_conteudo,16,02) = '85') 
and 	(substr(ds_conteudo,14,02) = '00')
union all
select	nr_sequencia,
	'', 
	'', 
	'', 
	'', 
	'',
	TO_NUMBER(0),
	TO_NUMBER(0),
	'',
	somente_numero(substr(ds_conteudo,70,13)),
	somente_numero(substr(ds_conteudo,83,9)) ,
	substr(ds_conteudo,38,8)
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and	(substr(ds_conteudo,16,02) = '75' or substr(ds_conteudo,16,02) = '85')
and 	substr(ds_conteudo,14,02) between '00' and '53'
union all
select	nr_sequencia,
	substr(ds_conteudo,14,02),
	substr(ds_conteudo,20,13),
	substr(ds_conteudo,41,06),
	substr(ds_conteudo,68,05),
	substr(ds_conteudo,68,05),
	somente_numero(substr(ds_conteudo,159,13)),
	somente_numero(substr(ds_conteudo,172,09)), 
	'',
	to_number(0),
	to_number(0),
	''
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and	(substr(ds_conteudo,16,02) = '76' or substr(ds_conteudo,16,02) = '86') 
and	(substr(ds_conteudo,14,02) = '00') 
union all
select	nr_sequencia,
	'',
	'',
	'',
	'',
	'',
	TO_NUMBER(0),
	TO_NUMBER(0),
	'',
	somente_numero(substr(ds_conteudo,82,13)),
	somente_numero(substr(ds_conteudo,95,9)),
	''
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and	(substr(ds_conteudo,16,02) = '76' or substr(ds_conteudo,16,02) = '86') 
and	substr(ds_conteudo,14,02) between '00' and '26'
union all
select	nr_sequencia,
	substr(ds_conteudo,14,02),
	substr(ds_conteudo,20,13),
	substr(ds_conteudo,41,06),
	substr(ds_conteudo,68,05),
	substr(ds_conteudo,68,05),
	somente_numero(substr(ds_conteudo,146,13)),
	somente_numero(substr(ds_conteudo,159,09)),
	'',
	to_number(0),
	to_number(0),
	''
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
and	(substr(ds_conteudo,16,02)	= '77' or substr(ds_conteudo,16,02) = '87') 
and	(substr(ds_conteudo,14,02) = '00')
union all
select	nr_sequencia,
	'',
	'',
	'',
	'',
	'',
	TO_NUMBER(0),
	TO_NUMBER(0),
	'',
	somente_numero(substr(ds_conteudo,70,13)),
	somente_numero(substr(ds_conteudo,83,09)) ,
	substr(ds_conteudo,38,8)
from	w_conv_ret_movto
where	nr_seq_retorno		= nr_seq_retorno_p
AND	(substr(ds_conteudo,16,02)	= '77' or substr(ds_conteudo,16,02) = '87') 
and	substr(ds_conteudo,14,02) between '00' and '24'
order by 1;

begin

	select	max(cd_convenio)
	into	cd_convenio_w
	from	convenio_retorno
	where	nr_sequencia = nr_seq_retorno_p;

	
open c01;
loop
fetch c01 into
	nr_sequencia_w,
	nr_registro_w,
	cd_usuario_convenio_w,
	cd_prestador_w,
	cd_autorizacao_w,
	nr_doc_convenio_w,
	vl_total_pago_w,
	vl_glosa_w,
	ds_complemento_w,
	vl_total_pago_55_w,
	vl_glosa_55_w,
	cd_item_w;
exit when c01%notfound;
	
	begin
	
	/*select	max(a.vl_procedimento),
		MAX(substr(Obter_Desc_Prescr_Proc_exam(a.cd_procedimento, a.ie_origem_proced,a.NR_SEQ_PROC_INTERNO, a.nr_seq_exame),1,255))
	into	vl_cobrado_w,
		ds_item_w
	from	procedimento_paciente a,
		conta_paciente b
	where	b.nr_interno_conta 		= a.nr_interno_conta
	and	b.nr_conta_convenio		= nr_doc_convenio_w
	and	a.cd_procedimento_convenio		= cd_item_w
	and	a.cd_convenio			= cd_convenio_w;
	*/

	if (nr_registro_w = '00' ) then
		
		nr_doc_convenio_princ_w	:= nr_doc_convenio_w;
		cd_autorizacao_princ_w	:= cd_autorizacao_w;
		/*
		insert into convenio_retorno_movto
			(cd_usuario_convenio,
			cd_prestador,
	 		cd_autorizacao,
	 		nr_doc_convenio,
	 		vl_cobrado,
	 		vl_total_pago,
	 		vl_glosa,
			ds_complemento,
	 		dt_atualizacao,
	 		nm_usuario,
	 		nr_sequencia,
	 		nr_seq_retorno,
			cd_item,
			ds_item_retorno
			)
		values	(to_number(cd_usuario_convenio_w),
			to_number(cd_prestador_w),
			to_number(cd_autorizacao_w),
	 		to_number(nr_doc_convenio_w),
	 		to_number(vl_cobrado_w)/100,
	 		to_number(vl_total_pago_w)/100,
	 		to_number(vl_glosa_w)/100,
			ds_complemento_w,
	 		sysdate,
	 		'TASY',
	 		convenio_retorno_movto_seq.nextval,
	 		nr_seq_retorno_p,
			cd_item_w,
			ds_item_w
	 		);
		*/
	else
		insert into convenio_retorno_movto
			(vl_total_pago,	
	 		vl_glosa,
	 		dt_atualizacao,
	 		nm_usuario,
	 		nr_sequencia,
	 		nr_seq_retorno,
			nr_doc_convenio,
			cd_autorizacao,
			cd_usuario_convenio,
			ds_complemento,
			cd_item,
			ds_item_retorno
			)
		values	(to_number(vl_total_pago_55_w)/100 ,
	 	 	to_number(vl_glosa_55_w)/100 ,
		 	sysdate,
	 	 	'TASY',
	 	 	convenio_retorno_movto_seq.nextval,
	 	 	to_number(nr_seq_retorno_p),
			to_number(nr_doc_convenio_princ_w),
			to_number(cd_autorizacao_princ_w),
	 		to_number(cd_usuario_convenio_w),
			ds_complemento_w,
			cd_item_w,
			ds_item_w);
	end if;	

	exception
	when others then
		sqlerrm_w	:= sqlerrm;
		insert into convenio_retorno_movto
			(DS_COMPLEMENTO,
			nr_seq_retorno,
			nr_sequencia,
			nm_usuario,
			dt_atualizacao
			)
		values	(substr(sqlerrm_w, 1, 255),
			nr_seq_retorno_p,
			convenio_retorno_movto_seq.nextval,
			'TASY',
			sysdate);
	end;

end loop;
close c01;

delete from w_conv_ret_movto 
where nr_seq_retorno  = nr_seq_retorno_p;

commit;

end HPCS_GERAR_W_CONV_RET_MOVTO;
/
