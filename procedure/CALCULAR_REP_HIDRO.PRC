create or replace 
procedure Calcular_rep_Hidro(	
				nr_sequencia_p     		number,
				nm_usuario_p         	varchar2) is

/* Vari�veis da tabela Prescr_Rep_HE */
qt_peso_w						number(15,3);	
qt_idade_dia_w					number(15,0);
qt_idade_mes_w					number(15,0);
qt_idade_ano_w					number(15,0);
qt_altura_cm_w					number(15,0);
qt_kcal_total_w					number(15,2);
qt_kcal_kg_w					number(15,2);
qt_etapa_w						number(15,0);
qt_nec_hidrica_diaria_w			number(15,2);
qt_elem_calcio_w				number(15,4);
qt_teste_w						number(15,4);
qt_aporte_hidrico_diario_w		number(15,2);
qt_vel_inf_glicose_w			number(15,2);
qt_nec_kcal_kg_dia_w			number(15,2);
qt_nec_kcal_dia_w				number(15,2);
qt_equipo_w						number(15,2);
qt_soma_outros_w				number(15,2);
pr_conc_glic_solucao_w			number(20,6);
ie_magnesio_w					varchar2(01);
ie_calculo_auto_w				varchar2(01);
ie_correcao_w					varchar2(01);
ie_peso_calorico_w				varchar2(01);
hr_prim_horario_w				varchar2(05);
qt_gotejo_w						number(15,2);
ie_emissao_w					varchar2(01);
qt_be_w							number(15,1);
qt_vol_desconto_w				number(15,0);
qt_hora_validade_w				number(15,0);
qt_hora_fase_w					number(15,0);
nr_seq_glicose_w				number(10,0);
qt_g_glic_kg_dia_w				number(15,2);
qt_vol_glic_w					number(15,2);
qt_g_glic_dia_w					number(15,2);
ie_equilibrio_w					varchar2(01);
nr_seq_elemento_w				number(10,0);
nr_seq_pac_elem_w				number(10,0);
nr_seq_glic_troca_w				number(10,0);
qt_vol_elemento_w				number(15,4);
qt_vol_total_w					number(15,4);
qt_conversao_ml_w				number(15,4);
qt_produto_w					number(05,0);
qt_g_menor_w					number(15,4);
qt_g_maior_w					number(15,4);
qt_troca_glicose_w				number(10,0);
nr_seq_ele_rep_w				number(15,0);
qt_volume_w						number(15,2);
qt_vol_cor_w					number(15,2);
qt_vol_etapa_w					number(15,2);
qt_min_dia_w					number(15,0);
qt_gotas_w						number(15,0);
ie_processo_hidrico_w			varchar2(1);

cursor C01 is
select	a.nr_sequencia,
		b.qt_conversao_ml,
		a.nr_seq_ele_rep
from	nut_elem_material b,
		Prescr_Rep_HE_elem_mat a,
		Prescr_Rep_HE_elem e
where	a.nr_seq_ele_rep	= e.nr_sequencia
and		a.nr_seq_elem_mat	= b.nr_sequencia
and		e.nr_seq_elemento	= nr_seq_glicose_w
and		e.nr_seq_Rep_HE		= nr_sequencia_p
and		qt_g_menor_w		> 0	
and		nvl(b.ie_tipo,'NPT')	= 'NPT'
order by qt_conversao_ml desc; 

cursor C02 is
select	b.nr_sequencia
from	nut_elem_material b
where	b.nr_seq_elemento       = nr_seq_glicose_w
and		b.nr_sequencia  not in (	select	nr_seq_ele_rep
									from	Prescr_Rep_HE_elem_mat
									where	nr_seq_ele_rep = nr_seq_pac_elem_w)
and		qt_conversao_ml < qt_conversao_ml_w
and		nvl(b.ie_tipo,'NPT')	= 'NPT'
order by qt_conversao_ml;

cursor c03 is
select	b.nr_sequencia,
		b.qt_volume,
		b.qt_vol_cor
from	Prescr_Rep_HE_elem_mat b,
		Prescr_Rep_HE_elem a
where	a.nr_seq_Rep_HE	= nr_sequencia_p
and		a.nr_sequencia	= b.nr_seq_ele_rep
and		nvl(b.qt_volume,0)	> 0;

begin

Gerar_Nut_Elemento_Pac_HE(nr_sequencia_p, nm_usuario_p);

select	qt_peso,
		qt_idade_dia,
		qt_idade_mes,
		qt_idade_ano,
		qt_altura_cm,
		qt_kcal_total,
		qt_kcal_kg,
		qt_nec_hidrica_diaria,
		qt_aporte_hidrico_diario,
		qt_vel_inf_glicose,
		qt_nec_kcal_kg_dia,
		pr_conc_glic_solucao,
		qt_equipo,
		nvl(ie_correcao,'N'),
		nvl(qt_etapa,1),
		nvl(qt_hora_validade,24),
		nvl(ie_magnesio,'N'),
		nvl(ie_peso_calorico,'S'),
		nvl(ie_processo_hidrico,'N')
into	qt_peso_w,
		qt_idade_dia_w,
		qt_idade_mes_w,
		qt_idade_ano_w,
		qt_altura_cm_w,
		qt_kcal_total_w,
		qt_kcal_kg_w,
		qt_nec_hidrica_diaria_w,
		qt_aporte_hidrico_diario_w,
		qt_vel_inf_glicose_w,
		qt_nec_kcal_kg_dia_w,
		pr_conc_glic_solucao_w,
		qt_equipo_w,
		ie_correcao_w,
		qt_etapa_w,
		qt_hora_validade_w,
		ie_magnesio_w,
		ie_peso_calorico_w,
		ie_processo_hidrico_w
from	prescr_rep_he
where	nr_sequencia			= nr_sequencia_p;

if	(qt_peso_w = 0) then
	Wheb_mensagem_pck.exibir_mensagem_abort(198202);
end if;

if	(ie_peso_calorico_w = 'S') then
	if	(qt_peso_w <= 10) then
	
		update	prescr_rep_he
		set		qt_nec_hidrica_diaria	= (qt_peso_w * 100) / qt_peso_w,
				qt_nec_kcal_kg_dia	= (qt_peso_w * 100) / qt_peso_w,
				qt_nec_kcal_dia		 = (qt_peso_w * 100),
				qt_aporte_hidrico_diario = (qt_peso_w * 100)
		where	nr_sequencia		= nr_sequencia_p;
	elsif	(qt_peso_w > 10) and
		(qt_peso_w <= 20) then
	
		update	prescr_rep_he
		set		qt_nec_hidrica_diaria	= ((1000 + ((qt_peso_w - 10) * 50)) / qt_peso_w),
				qt_nec_kcal_kg_dia	= ((1000 + ((qt_peso_w - 10) * 50)) / qt_peso_w),
				qt_nec_kcal_dia		 = (1000 + ((qt_peso_w - 10) * 50)),
				qt_aporte_hidrico_diario = (1000 + ((qt_peso_w - 10) * 50))
		where	nr_sequencia		= nr_sequencia_p;
	elsif	(qt_peso_w > 20) then
	
		update	prescr_rep_he
		set		qt_nec_hidrica_diaria	= ((1500 + ((qt_peso_w - 20) * 20)) / qt_peso_w),
				qt_nec_kcal_kg_dia	= ((1500 + ((qt_peso_w - 20) * 20)) / qt_peso_w),
				qt_nec_kcal_dia		 = 1500 + ((qt_peso_w - 20) * 20),
				qt_aporte_hidrico_diario = 1500 + ((qt_peso_w - 20) * 20)
		where	nr_sequencia		= nr_sequencia_p;
	end if;	
	
	select 	qt_nec_hidrica_diaria,
			qt_aporte_hidrico_diario,
			qt_nec_kcal_kg_dia	
	into	qt_nec_hidrica_diaria_w,
			qt_aporte_hidrico_diario_w,
			qt_nec_kcal_kg_dia_w
	from	prescr_rep_he
	where	nr_sequencia			= nr_sequencia_p;
	
end if;
if	(ie_peso_calorico_w <> 'S') then
	qt_aporte_hidrico_diario_w		:= qt_nec_hidrica_diaria_w * qt_peso_w;
end if;

select	nvl(sum(obter_vol_elem_rep_he(a.nr_sequencia)),0)
into	qt_soma_outros_w
from	nut_elemento b,
		prescr_rep_he_elem a
where	a.nr_seq_rep_he		= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	<> 'C';

qt_nec_kcal_dia_w			:= qt_nec_kcal_kg_dia_w * qt_peso_w;
if (qt_vel_inf_glicose_w > 0) then
	qt_g_Glic_kg_dia_w			:= trunc(qt_vel_inf_glicose_w * 1.44,1);
else
	select	max(z.qt_dose_padrao)
	into	qt_g_Glic_kg_dia_w
	from	prescr_rep_he_elem x,
			nut_elemento z
	where	nr_seq_rep_he		= nr_sequencia_p
	and		z.nr_sequencia		= x.nr_seq_elemento
	and		z.ie_tipo_elemento	= 'C'
	and		nvl(z.ie_situacao,'A')	= 'A';
	
	qt_g_glic_dia_w			:= qt_g_glic_kg_dia_w * qt_peso_w;
	qt_vel_inf_glicose_w	:= dividir(dividir((qt_g_glic_dia_w * 1000),qt_peso_w),1440);
	
	update	prescr_rep_he
	set		qt_vel_inf_glicose	= qt_vel_inf_glicose_w
	where	nr_sequencia		= nr_sequencia_p;
	commit;
end if;
qt_vol_Glic_w				:= trunc(qt_aporte_hidrico_diario_w - qt_soma_outros_w); /* Ignora demais */
qt_g_glic_dia_w				:= qt_g_glic_kg_dia_w * qt_peso_w;

qt_kcal_total_w				:= qt_g_glic_dia_w * 4;
qt_kcal_kg_w				:= Dividir(qt_kcal_total_w, qt_peso_w);
pr_conc_glic_solucao_w		:= Dividir((qt_g_glic_dia_w * 100), qt_aporte_hidrico_diario_w);
qt_hora_fase_w				:= Dividir(qt_hora_validade_w, qt_etapa_w);

select 	nvl(max(nr_sequencia),0)
into	nr_seq_glicose_w
from	nut_elemento
where	ie_tipo_elemento		= 'C';

if	(ie_peso_calorico_w = 'S') then
	qt_g_glic_dia_w			:= qt_aporte_hidrico_diario_w	* 0.08;
	qt_g_Glic_kg_dia_w		:= dividir(qt_g_glic_dia_w, qt_peso_w);
	qt_vel_inf_glicose_w	:= dividir(dividir((qt_g_glic_dia_w * 1000),qt_peso_w),1440);
	
	update	prescr_rep_he
	set		qt_vel_inf_glicose	= qt_vel_inf_glicose_w
	where	nr_sequencia		= nr_sequencia_p;
	
end if;

update	prescr_rep_he_elem
set		qt_elem_kg_dia		= qt_g_Glic_kg_dia_w,
		qt_diaria			= qt_g_glic_dia_w
where	nr_seq_rep_he		= nr_sequencia_p
and		nr_seq_elemento		= nr_seq_glicose_w;

update	prescr_rep_he
set		qt_kcal_total				= nvl(qt_kcal_total_w,0),
		qt_kcal_kg					= nvl(qt_kcal_kg_w,0),
		qt_aporte_hidrico_diario	= decode(ie_peso_calorico_w,'S', qt_aporte_hidrico_diario, qt_aporte_hidrico_diario_w),
		qt_nec_kcal_dia				= decode(ie_peso_calorico_w,'S',qt_nec_kcal_dia,qt_nec_kcal_dia_W),
		pr_conc_glic_solucao		= pr_conc_glic_solucao_w,
		qt_etapa					= qt_etapa_w,
		qt_hora_validade			= qt_hora_validade_w,
		qt_hora_fase				= qt_hora_fase_w
where	nr_sequencia				= nr_sequencia_p;

calcular_nut_elemento_pac_he(nr_sequencia_p, nm_usuario_p);

if	(ie_magnesio_w = 'S') then
	select	max(qt_elem_kg_dia)
	into	qt_elem_calcio_w
	from	prescr_rep_he_elem x,
			nut_elemento z
	where	nr_seq_rep_he		= nr_sequencia_p
	and		z.nr_sequencia		= x.nr_seq_elemento
	and		z.ie_tipo_elemento	= 'I'
	and		nvl(z.ie_situacao,'A')	= 'A';
	
	begin
	update	prescr_rep_he_elem x
	set		qt_elem_kg_dia		= nvl(dividir(qt_elem_calcio_w,4),0)
	where	nr_seq_rep_he		= nr_sequencia_p
	and		qt_elem_calcio_w	> 0
	and		x.nr_seq_elemento	= (	select	max(z.nr_sequencia)
									from	nut_elemento z
									where	z.ie_tipo_elemento	= 'M'
									and		nvl(z.ie_situacao,'A')	= 'A');
	exception when others then
		null;
	end;
end if;

/* Equilibrar Glicose x Balan�o Hidrico */
qt_produto_w				:= 0;
qt_vol_total_w				:= 0;
ie_equilibrio_w				:= 'S';
qt_troca_glicose_w			:= 0;
qt_g_menor_w				:= 999;

select	qt_aporte_hidrico_diario
into	qt_aporte_hidrico_diario_w
from	prescr_rep_he
where	nr_sequencia = nr_sequencia_p;

open C01;
loop
fetch C01	into
	nr_seq_elemento_w,
	qt_conversao_ml_w,
	nr_seq_pac_elem_w;	
exit when C01%notfound;
	qt_produto_w			:= qt_produto_w + 1;
	if	(qt_produto_w = 1) then
		qt_vol_elemento_w	:= qt_g_glic_dia_w * qt_conversao_ml_w;
		qt_vol_total_w		:= qt_vol_elemento_w;
		qt_g_menor_w		:= qt_g_glic_dia_w;
		qt_g_maior_w		:= 0;
		nr_seq_glic_Troca_w	:= nr_seq_elemento_w;
	else
		qt_vol_elemento_w	:= 0;
	end if;
	
	update	prescr_rep_he_elem_mat
	set		qt_volume		= qt_vol_elemento_w,
			qt_vol_cor		= 0
	where	nr_sequencia	= nr_seq_elemento_w;
end loop;
close C01;

if	(qt_vol_total_w <= qt_vol_glic_w) then
	ie_equilibrio_w			:= 'S';
elsif	(qt_produto_w	= 2) then
	ie_equilibrio_w			:= 'N';
end if;
while (ie_equilibrio_w = 'N') loop
	if	(qt_vol_total_w > 0) and
		(qt_vol_total_w <= qt_vol_glic_w) then
		ie_equilibrio_w		:= 'S';
	elsif	(qt_vol_total_w = 0) and
		(qt_troca_glicose_w > 7) then
		ie_equilibrio_w		:= 'S';
	elsif	(qt_vol_total_w = 0) or
		(qt_g_menor_w < 0) then
		nr_seq_elemento_w		:= 0;
		open C02;
		loop
		fetch C02	into
			nr_seq_elemento_w;	
		exit when C02%notfound;
			nr_seq_elemento_w	:= nr_seq_elemento_w;
		end loop;
		close C02;
		if	(nr_seq_elemento_w > 0) then
			qt_troca_glicose_w	:= qt_troca_glicose_w + 1;
			update	prescr_rep_he_elem_mat
			set		nr_seq_elem_mat = nr_seq_elemento_w
			where	nr_sequencia	= nr_seq_glic_Troca_w;
		else
			ie_equilibrio_w	:= 'S';
		end if;
	else
		qt_g_menor_w		:= qt_g_menor_w - 1;
		qt_g_maior_w		:= qt_g_maior_w + 1;
		qt_vol_total_w		:= 0;
		open C01;
		loop
		fetch C01	into
			nr_seq_elemento_w,
			qt_conversao_ml_w,
			nr_seq_pac_elem_w;	
		exit when C01%notfound;
			if	(qt_vol_total_w = 0) then
				qt_vol_elemento_w	:= qt_g_menor_w * qt_conversao_ml_w;
			else
				qt_vol_elemento_w	:= qt_g_maior_w * qt_conversao_ml_w;
			end if;
			qt_vol_total_w		:= qt_vol_total_w + qt_vol_elemento_w;
			
			update	Prescr_Rep_HE_elem_mat
			set		qt_volume	= qt_vol_elemento_w
			where	nr_sequencia	= nr_seq_elemento_w;
		end loop;
		close C01;
	end if;
end loop;

select	sum(b.qt_volume)
into	qt_vol_total_w
from	Prescr_Rep_HE_elem_mat b,
		prescr_rep_he_elem a
where	a.nr_seq_Rep_HE	= nr_sequencia_p
and		a.nr_sequencia	= b.nr_seq_ele_rep;

open C03;
loop
fetch C03	into
	nr_seq_ele_rep_w,
	qt_volume_w,
	qt_vol_cor_w;	
exit when C03%notfound;
	if (nvl(qt_aporte_hidrico_diario_w,0) > 0) then
		qt_vol_cor_w		:= round(qt_volume_w * ((qt_aporte_hidrico_diario_w + qt_equipo_w) / qt_aporte_hidrico_diario_w),4);
		qt_vol_etapa_w		:= dividir(qt_vol_cor_w, nvl(qt_etapa_w,1));
	end if;
	update 	prescr_rep_he_elem_mat
	set		qt_vol_cor		= qt_vol_cor_w,
			qt_vol_etapa	= qt_vol_etapa_w
	where	nr_sequencia	= nr_seq_ele_rep_w;
end loop;
close C03;

update	prescr_rep_he_elem a
set		qt_volume_corrigido	= (	select	nvl(sum(b.qt_vol_cor),0)
								from	prescr_rep_he_elem_mat b
								where	a.nr_sequencia= b.nr_seq_ele_rep),
		qt_volume_etapa		= (	select	nvl(sum(b.qt_vol_etapa),0)
								from	prescr_rep_he_elem_mat b
								where	a.nr_sequencia= b.nr_seq_ele_rep),
		qt_volume			= (	select	nvl(sum(b.qt_volume),0)
								from	prescr_rep_he_elem_mat b
								where	a.nr_sequencia= b.nr_seq_ele_rep)
where	nr_seq_rep_he	= nr_sequencia_p; 

/* Calcular o gotejo em gotas por minuto */
qt_min_dia_w	:= 60 * qt_hora_validade_w;
qt_gotas_w		:= 20 * qt_vol_total_w;
qt_gotejo_w		:= round(dividir(qt_gotas_w, qt_min_dia_w),0);

update	prescr_rep_he
set		qt_gotejo		= qt_gotejo_w
where	nr_sequencia	= nr_sequencia_p;

commit;

if (ie_processo_hidrico_w = 'N') then
	Ajustar_Vol_Glic_TREV(nr_sequencia_p,nm_usuario_p);
end if;

end Calcular_Rep_Hidro;
/