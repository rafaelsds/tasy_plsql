create or replace
procedure alterar_status_agenda_tasy (
				cd_estabelecimento_p	number,
				nr_seq_agenda_p		number,
				ie_status_p		varchar2,
				nm_usuario_p		varchar2) is

ie_status_w	varchar2(1);
qt_recurso_w	number(10,0);
dt_agenda_w	date;

begin
if	(nr_seq_agenda_p is not null) and
	(ie_status_p is not null) then
	begin
	select	nvl(max(ie_status),ie_status_p),
		max(dt_agenda)
	into	ie_status_w,
		dt_agenda_w
	from	agenda_tasy
	where	nr_sequencia = nr_seq_agenda_p;	
	
	if	(ie_status_p = 'B') then
		begin
		if	(ie_status_w = 'B') then	
			begin
			update	agenda_tasy
			set	ie_status		= 'L',
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_agenda_p;
			end;
		else
			begin
			update	agenda_tasy
			set	ie_status		= 'B',
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_agenda_p;
			end;
		end if;
		end;
		
	elsif	(ie_status_p = 'C') then
		begin
		update	agenda_tasy
		set	ie_status		= 'C',
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_agenda_p;

		select	count(*)
		into	qt_recurso_w
		from	agenda_tasy_recurso
		where	nr_seq_agenda = nr_seq_agenda_p;

		if	(qt_recurso_w > 0) then
			begin
			enviar_comunic_recurso_agenda(cd_estabelecimento_p, nr_seq_agenda_p, nm_usuario_p);
			end;
		end if;
		
		if	(dt_agenda_w is not null) then
			begin
			desconfirmar_agenda_tasy(dt_agenda_w, nm_usuario_p);
			end;
		end if;			
		end;
		
	elsif	(ie_status_p = 'E') then
		begin
		if 	(ie_status_w = 'E') then
			begin
			update 	agenda_tasy
			set	ie_status 		= 'M',
				nm_usuario 	= nm_usuario_p
			where	nr_sequencia	= nr_seq_agenda_p;
			end;		
		else
			begin
			update	agenda_tasy
			set	ie_status		= 'E',
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_agenda_p;
			end;				
		end if;
		end;		
	end if;
	end;
end if;
commit;
end alterar_status_agenda_tasy;
/