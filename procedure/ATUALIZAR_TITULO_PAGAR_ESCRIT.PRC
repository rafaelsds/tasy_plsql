create or replace
procedure atualizar_titulo_pagar_escrit(
		cd_banco_p	number,
		nr_titulo_p	number) is 
begin
if	(nr_titulo_p is not null) then
	begin
	update 	titulo_pagar_escrit 
	set 	cd_banco 	= cd_banco_p
	where 	nr_titulo 		= nr_titulo_p 
	and 	cd_banco is null;
	commit;
	end;
end if;
end atualizar_titulo_pagar_escrit;
/