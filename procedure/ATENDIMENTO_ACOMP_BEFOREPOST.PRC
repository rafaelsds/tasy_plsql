create or replace
procedure atendimento_acomp_BeforePost(	nr_atendimento_p	number,
					cd_setor_atendimento_p	number,
					cd_unidade_basica_p	varchar2,
					cd_unidade_compl_p	varchar2,
					qt_perm_acomp_p		out number,
					qt_registros_p		out number,
					cd_estabelecimento_p	number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2) is

ie_forma_cons_acomp_w	varchar2(5);
qt_perm_acomp_w		number(3,0)	:= 0;
qt_registros_w		number(10,0)	:= 0;

begin

if	(nr_atendimento_p > 0) then
	begin
	
	Obter_Param_Usuario(44, 76, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_forma_cons_acomp_w);

	if	(ie_forma_cons_acomp_w = 'EA') then		
		select	nvl(sum(qt_max_acomp),0)
		into	qt_perm_acomp_w
		from	unidade_atendimento 
		where	cd_setor_atendimento	= cd_setor_atendimento_p
		and	cd_unidade_basica	= cd_unidade_basica_p
		and	cd_unidade_compl	= cd_unidade_compl_p;	
		
	elsif 	(ie_forma_cons_acomp_w = 'UEU') then
		select 	max(nr_acompanhante)
		into 	qt_perm_acomp_w
		from 	atend_categoria_convenio
		where 	nr_atendimento = nr_atendimento_p
		and 	nr_seq_interno = (select max(nr_seq_interno) from atend_categoria_convenio where nr_atendimento = nr_atendimento_p);
		
	else
		select	nvl(sum(nr_acompanhante),0)
		into	qt_perm_acomp_w
		from	atend_categoria_convenio
		where	nr_atendimento = nr_atendimento_p;
		
	end if;

	select	count(*)
	into	qt_registros_w
	from	atendimento_acompanhante
	where	nr_atendimento = nr_atendimento_p
	and	dt_saida is null;
	
	end;
end if;

qt_perm_acomp_p	:= qt_perm_acomp_w;
qt_registros_p	:= qt_registros_w;

end atendimento_acomp_BeforePost;
/
