create or replace
procedure pls_cancelar_item_just_web
			(	nr_seq_requisicao_p	Number,
				nr_seq_auditoria_p	Number,
				nr_seq_item_p		Number,
				ds_motivo_p		Varchar2,
				nr_seq_motivo_canc_p	Number,
				nm_usuario_p		Varchar2,
				ds_retorno_p	out	Varchar2) is

qt_registro_w		Number(10);
nr_seq_mat_w		Number(10);
nr_seq_proc_w		Number(10);
qt_reg_proc_w		Number(10);
qt_reg_mat_w		Number(10);
qt_reg_aud_w		Number(10);
ie_encerrar_ana_param_w	Varchar2(2) := 'N';
qt_itens_dif_canc_w	Number(10);
ds_historico_w		Varchar2(1000);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin

select	max(nr_seq_mat_origem),
	max(nr_seq_proc_origem)
into	nr_seq_mat_w,
	nr_seq_proc_w
from	pls_auditoria_item
where	nr_sequencia = nr_seq_item_p;

ds_retorno_p	:= 'S';

update	pls_auditoria_item
set	dt_atualizacao 	= sysdate,
	nm_usuario	= nm_usuario_p,
	ie_status	= 'C'
where 	nr_sequencia	= nr_seq_item_p;


if	(nr_seq_proc_w is not null) then

	update	pls_requisicao_proc
	set	dt_atualizacao 	= sysdate,
		nm_usuario	= nm_usuario_p,
		ie_status	= 'C',
		ie_estagio	= 'N',
		nr_seq_motivo_cancel = nr_seq_motivo_canc_p
	where 	nr_sequencia	= nr_seq_proc_w
	and	nr_seq_requisicao = nr_seq_requisicao_p;

elsif	(nr_seq_mat_w is not null) then

	update	pls_requisicao_mat
	set	dt_atualizacao 	= sysdate,
		nm_usuario	= nm_usuario_p,
		ie_status	= 'C',
		ie_estagio	= 'N',
		nr_seq_motivo_cancel = nr_seq_motivo_canc_p
	where 	nr_sequencia	= nr_seq_mat_w
	and	nr_seq_requisicao = nr_seq_requisicao_p;

end if;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	pls_requisicao
where	nr_sequencia 	= nr_seq_requisicao_p;

ie_encerrar_ana_param_w := pls_parametro_operadora_web('EAJ', cd_estabelecimento_w);

select	count(1)
into	qt_reg_proc_w
from	pls_requisicao_proc
where	nr_seq_requisicao	= nr_seq_requisicao_p
and	ie_status		<> 'C';

select	count(1)
into	qt_reg_mat_w
from	pls_requisicao_mat
where	nr_seq_requisicao	= nr_seq_requisicao_p
and		ie_status <> 'C';

qt_itens_dif_canc_w := qt_reg_proc_w + qt_reg_mat_w;

if	(qt_itens_dif_canc_w = 0) then
	-- Se todos os itens da requisi��o est�o cancelados

	update	pls_requisicao
	set	ie_status		= 'C',
		ie_estagio		= 3,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_requisicao_p;

	select	count(1)
	into	qt_reg_aud_w
	from	pls_auditoria
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	ie_status		not in('F', 'C');

	if	(qt_reg_aud_w	> 0) then
		update	pls_auditoria
		set	ie_status		= 'C',
			dt_liberacao		= sysdate,
			nr_seq_proc_interno	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_requisicao	= nr_seq_requisicao_p;
	end if;

end if;

insert into pls_requisicao_historico
	(nr_sequencia, dt_atualizacao_nrec, dt_atualizacao,
	nm_usuario, nm_usuario_nrec, nr_seq_requisicao,
	ds_historico, dt_historico, ie_origem_historico,
	ie_tipo_historico, nr_seq_item)
values	(pls_requisicao_historico_seq.nextval, sysdate, sysdate,
	nm_usuario_p, nm_usuario_p, nr_seq_requisicao_p,
	ds_motivo_p, sysdate, 'M', 'M', nr_seq_item_p);

select 	count(1)
into	qt_registro_w
from	pls_auditoria_item
where 	nr_seq_auditoria = nr_seq_auditoria_p
and	ie_status = 'J';

if	(qt_registro_w = 0) then

	select 	count(1)
	into	qt_registro_w
	from	pls_auditoria_item
	where 	nr_seq_auditoria = nr_seq_auditoria_p
	and		ie_status = 'P';

	-- Se todos os itens pendente de justificativas forem cancelados
	if	(ie_encerrar_ana_param_w = 'S' and qt_registro_w = 0) then
		update	pls_auditoria
		set	ie_status 	= 'F',
			nm_usuario 	= nm_usuario_p,
			dt_atualizacao 	= sysdate,
			dt_liberacao	= sysdate
		where 	nr_sequencia 	= nr_seq_auditoria_p;

		ds_historico_w := 'O usu�rio web '||nm_usuario_p||', encerrou a an�lise ap�s cancelar todos os itens na justificativa do prestador.';
		
		pls_gerar_hist_req_web (nr_seq_requisicao_p, 'L',ds_historico_w,
				        nm_usuario_p);
		
		if	(qt_itens_dif_canc_w > 0) then
			pls_atualiza_estagio_req(nr_seq_requisicao_p,nm_usuario_p);
		end if;
	else
		update	pls_auditoria
		set	ie_status 	= 'A',
			nm_usuario 	= nm_usuario_p,
			dt_atualizacao 	= sysdate
		where 	nr_sequencia 	= nr_seq_auditoria_p
		and	ie_status	<> 'C';
	end if;
	ds_retorno_p := 'N';
end if;

commit;

end pls_cancelar_item_just_web;
/
