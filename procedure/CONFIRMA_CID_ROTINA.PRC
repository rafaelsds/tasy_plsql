create or replace
procedure confirma_cid_rotina(	ie_insert_diag_med_p	varchar2,
				nr_atendimento_p		number,
				ie_medico_p		varchar2,
				cd_pessoa_fisica_p		varchar2,
				nr_seq_lista_cid_p		varchar2,
				dt_diagnostico_p	date,
				nr_seq_reg_elemento_p	number,
				nm_usuario_p		varchar2,
				ie_liberado_p		varchar2 default 'N') is 

cd_medico_resp_w		varchar2(10);
cd_medico_w		varchar2(10);
dt_alta_w			date;
qt_existe_reg_w		number(10,0);
dt_diag_cid_w		date;
nr_seq_lista_w		varchar2(16000);
nr_pos_virgula_w		number(10,0);	
nr_sequencia_w		varchar2(255);
cd_doenca_w		varchar2(10);
nr_seq_mentor_w		number(10);
nr_seq_interno_w	number(10);

begin

if	(dt_diagnostico_p is null) then

	select	sysdate
	into	dt_diag_cid_w
	from	dual;
else	
	dt_diag_cid_w	:= dt_diagnostico_p;
end if;

if	(nr_atendimento_p is not null) and
	(nr_atendimento_p <> 0) then
	begin
	
	if	(nr_seq_lista_cid_p is not null) then
		begin

		nr_seq_lista_w	:= nr_seq_lista_cid_p;

		while	(nr_seq_lista_w is not null) loop
			begin

			nr_pos_virgula_w	:= instr(nr_seq_lista_w,',');

			if	(nr_pos_virgula_w > 0) then
				begin

				nr_sequencia_w	:= substr(nr_seq_lista_w,1,nr_pos_virgula_w-1);
				nr_seq_lista_w	:= substr(nr_seq_lista_w,nr_pos_virgula_w+1,length(nr_seq_lista_w));

				if	(to_numbeR(nr_sequencia_w) > 0) then
					begin

					if	(ie_insert_diag_med_p = 'S') then
						begin
		
						select	count(*)
						into	qt_existe_reg_w
						from	atendimento_paciente
						where	nr_atendimento = nr_atendimento_p;						
			
						if	(qt_existe_reg_w > 0) then
							begin

							select	cd_medico_resp,
								dt_alta
							into	cd_medico_resp_w,
								dt_alta_w
							from	atendimento_paciente
							where	nr_atendimento = nr_atendimento_p;

							dt_diag_cid_w	:= dt_diag_cid_w - (1/24/60/60);

							if	(ie_medico_p = 'S') then
								cd_medico_w	:= cd_pessoa_fisica_p;
							else
								cd_medico_w	:= cd_medico_resp_w;
							end if;	

							gerar_diagnostico_medico(nr_atendimento_p, dt_diag_cid_w, cd_medico_w, nm_usuario_p);
		
							end;
						end if;
						end;
					end if;

					Gerar_diagnostico_rotina(nr_atendimento_p, dt_diag_cid_w, to_numbeR(nr_sequencia_w), nm_usuario_p,ie_liberado_p,null,null,null,null,nr_seq_mentor_w,nr_seq_interno_w);

					if	(nr_seq_reg_elemento_p <> 0) then
						begin

						select	cd_doenca
						into	cd_doenca_w
                                               				from	cid_rotina
			                                                where	nr_sequencia = to_numbeR(nr_sequencia_w);

						Ehr_Vincular_Elemento_item(nr_seq_reg_elemento_p, nr_atendimento_p, cd_doenca_w, dt_diag_cid_w, nm_usuario_p);

						end;
					end if;

					end;
				end if;
				end;
			end if;

			end;
		end loop;
		end;
	end if;	
	end;
elsif	(nr_atendimento_p = 0) then
	begin
	wheb_mensagem_pck.exibir_mensagem_abort(45322);
	end;
end if;

end confirma_cid_rotina;
/
