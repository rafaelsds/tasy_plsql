create or replace procedure UB_gerar_debito_itau_150 (	nr_seq_cobr_escrit_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 	


ds_conteudo_w		varchar2(4000);	
nr_contador_w		number(15)	:= 0;
vl_soma_debito_w		number(15,2)	:= 0;
nr_seq_apres_w 		number(10)	:= 0;

/* VARIAVEIS REGISTRO 'A' HEADER DE ARQUIVO */
ie_tipo_registro_w	varchar2(1);
ie_remessa_retorno_w	number(1);
cd_convenio_w		varchar2(13);
nm_empresa_w		varchar2(20);
cd_banco_w		varchar2(10);
nm_banco_w		varchar2(10);
dt_geracao_w		varchar2(14);
nr_seq_arquivo_w		varchar2(6)	:= 0;
nr_versao_layout_w		varchar2(2);
ds_servico_w		varchar2(17);
nr_remessa_w		number(10);

/* VARIAVEIS REGISTRO 'C' e 'D' */
--ie_tipo_registro_w	varchar2(1);
cd_agencia_debitada_w	varchar2(4);
cd_conta_debitada_w	varchar2(5);
ie_digito_deb_w		varchar2(1);
dt_debito_w		varchar2(14);
ie_tipo_movimento_w	number(1);

/* VARIAVEIS REGISTRO 'E' */
--ie_tipo_registro_w		varchar2(1);
--cd_agencia_debitada_w	varchar2(4);
--cd_conta_debitada_w	varchar2(5);
--ie_digito_deb_w		varchar2(1);
--dt_debito_w		varchar2(14);
vl_debito_w		varchar2(15);
ie_tipo_moeda_w		varchar2(2);
nr_titulo_w		varchar2(30);
vl_juros_dia_w		varchar2(15);
ds_compl_hist_w		varchar2(16);
ds_cgc_cpf_w		varchar2(14);
--ie_tipo_movimento_w	number(1);
vl_cobranca_w		number(15,2);
nr_seq_pagador_w		number(10);

/* VARIAVEIS REGISTRO 'Z' TRAILER DE ARQUIVO */
--ie_tipo_registro_w		varchar2(1);
qt_reg_arquivo_w	varchar2(6);
vl_soma_valores_w	varchar2(20);
nr_carteira_benef_w	varchar2(25);

Cursor C01 is
	select	
    rpad(nvl(substr(ub_obter_dados_pagador_fin(d.nr_seq_pagador,'A',d.dt_pagamento_previsto),1,8),'0'),4,' '),
    lpad(nvl(substr(ub_obter_dados_pagador_fin(d.nr_seq_pagador,'C',d.dt_pagamento_previsto),1,8),'0'),5,'0'),
    substr(ub_obter_dados_pagador_fin(d.nr_seq_pagador,'DC',d.dt_pagamento_previsto),1,1),
		rpad(substr(to_char(d.dt_pagamento_previsto,'yyyymmdd'),1,8),8,' '),
		rpad(d.nr_titulo,25,' '),
		replace(to_char(b.vl_cobranca, 'fm0000000000000.00'), '.', ''),
		replace(to_char(d.tx_juros, 'fm0000000000000.00'), '.', ''),
		lpad(nvl(d.cd_cgc,obter_cpf_pessoa_fisica(d.cd_pessoa_fisica)),14,'0'),
		b.vl_cobranca,
		nvl(substr(pls_obter_dados_segurado(pls_obter_segurado_pagador(e.nr_seq_pagador),'C'),1,25),'0') nr_carteira_benef,
		e.nr_seq_pagador
	from	pls_mensalidade e,
		titulo_receber d,
		banco_estabelecimento c,
		titulo_receber_cobr b,
		cobranca_escritural a,
		pls_contrato_pagador_fin g
	where	a.nr_seq_conta_banco	= c.nr_sequencia 
	and	b.nr_seq_cobranca	= a.nr_sequencia
	and	b.nr_titulo		= d.nr_titulo
	and	d.nr_seq_mensalidade	= e.nr_sequencia(+)
	and 	e.nr_seq_pagador 	= g.nr_seq_pagador(+)
	and 	trunc(to_date(e.dt_referencia),'MM') between trunc(to_date(nvl(g.dt_inicio_vigencia,sysdate)),'MM') and trunc(to_date(fim_dia(g.dt_fim_vigencia) + 360),'MM')
	and 	(g.dt_fim_vigencia is null or g.dt_fim_vigencia > e.dt_referencia)	
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin

delete from	w_envio_banco;

/*==================== REGISTRO 'A'HEADER ====================*/
nr_contador_w		:= nr_contador_w + 1;
ds_conteudo_w		:= '';
ie_tipo_registro_w	:= 'A';
ie_remessa_retorno_w	:= 1; --Remessa
dt_geracao_w		:= rpad((to_char(sysdate,'yyyymmdd')),8,' ');
nr_seq_arquivo_w	:= nr_contador_w +1;
nr_versao_layout_w	:= '04';
ds_servico_w		:= 'DEBITO AUTOMATICO';

select	rpad(nvl(c.cd_conv_banco_deb,c.cd_convenio_banco),13,' ') cd_convenio_banco,
	rpad(substr(obter_razao_social(b.cd_cgc),1,100),20,' ') nm_empresa,
	rpad(c.cd_banco,3,' '),
	rpad(substr(obter_nome_banco(a.cd_banco),1,100),10,' ') ds_banco,
	nvl(substr(a.nr_remessa,1,6),'0')
into	cd_convenio_w,
	nm_empresa_w,
	cd_banco_w,
	nm_banco_w,
	nr_remessa_w	
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	ie_tipo_registro_w	||	ie_remessa_retorno_w	||	cd_convenio_w		||	rpad(' ',7,' ')		||
			nm_empresa_w		||	cd_banco_w		||	nm_banco_w		||	rpad(' ',10,' ')	||
			dt_geracao_w		||	lpad(nr_remessa_w,6,'0')||	nr_versao_layout_w	||	ds_servico_w		||	
			rpad(' ', 52, ' ');

insert into	w_envio_banco
	(nr_sequencia,
	cd_estabelecimento,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	cd_estabelecimento_p,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	ds_conteudo_w,
	nr_seq_apres_w,
	nr_seq_apres_w);
nr_seq_apres_w := nr_seq_apres_w + 1;

/*==================== REGISTRO 'E' ====================*/
ds_conteudo_w		:= '';
ie_tipo_registro_w	:= 'E';
ds_compl_hist_w		:= rpad(' ',16,' ');
ie_tipo_movimento_w	:= 0;
ie_tipo_moeda_w		:= '03'; --Real

open C01;
loop
fetch C01 into	
	cd_agencia_debitada_w,
	cd_conta_debitada_w,
	ie_digito_deb_w,
	dt_debito_w,
	nr_titulo_w,
	vl_debito_w,
	vl_juros_dia_w,
	ds_cgc_cpf_w,
	vl_cobranca_w,
	nr_carteira_benef_w,
	nr_seq_pagador_w;
exit when C01%notfound;
	begin		
	/* Caso o pagador nao tenha carteira, buscar a carteira do titular */
	if	(nvl(nr_carteira_benef_w,'0') = '0') then		
		select	nvl(max(b.cd_usuario_plano),'0')
		into	nr_carteira_benef_w
		from	pls_segurado_carteira	b,
			pls_segurado		a
		where	a.nr_sequencia	 = b.nr_seq_segurado
		and	a.nr_seq_titular is null
		and	a.nr_seq_pagador = nr_seq_pagador_w;
	end if;

	nr_contador_w		:= nr_contador_w + 1;
	vl_soma_debito_w	:= vl_soma_debito_w + vl_cobranca_w;
	nr_carteira_benef_w	:= rpad(nr_carteira_benef_w,25,'0');

	ds_conteudo_w	:=	ie_tipo_registro_w	||	nr_carteira_benef_w	||	cd_agencia_debitada_w	||	rpad(' ',8,' ')	||
				cd_conta_debitada_w	||	ie_digito_deb_w		||	dt_debito_w		||	vl_debito_w	||
				ie_tipo_moeda_w		||	nr_titulo_w		||	rpad(' ',41,' ')	||	ds_cgc_cpf_w	||	ie_tipo_movimento_w;

	insert into	w_envio_banco
		(nr_sequencia,
		cd_estabelecimento,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		cd_estabelecimento_p,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		ds_conteudo_w,
		nr_seq_apres_w,
		nr_seq_apres_w);	
	nr_seq_apres_w := nr_seq_apres_w + 1;	

	end;
end loop;
close C01;	


/*==================== REGISTRO 'Z' TRAILER====================*/
nr_contador_w		:= nr_contador_w + 1;
ds_conteudo_w		:= '';
ie_tipo_registro_w	:= 'Z';
qt_reg_arquivo_w	:= lpad(nr_contador_w,6,'0');
vl_soma_valores_w	:= replace(to_char(vl_soma_debito_w, 'fm000000000000000.00'), '.', '');

ds_conteudo_w		:= ie_tipo_registro_w	||	qt_reg_arquivo_w	||	vl_soma_valores_w	||	rpad(' ',126,' ');

insert into	w_envio_banco
	(nr_sequencia,
	cd_estabelecimento,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	cd_estabelecimento_p,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	ds_conteudo_w,
	nr_seq_apres_w,
	nr_seq_apres_w);	
nr_seq_apres_w := nr_seq_apres_w + 1;	

commit;

end UB_gerar_debito_itau_150;
/
