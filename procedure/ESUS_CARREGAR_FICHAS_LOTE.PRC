create or replace PROCEDURE ESUS_CARREGAR_FICHAS_LOTE (
  ie_tipo_lote_esus_p varchar,
  nr_seq_lote_p	esus_lote_envio.nr_sequencia%type,
  dt_inicio_p date,
  dt_fim_p date
) AS 
BEGIN
  if	(ie_tipo_lote_esus_p = '1') then
    update 	ESUS_CAD_INDIVIDUAL set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));  
  elsif (ie_tipo_lote_esus_p = '2') then
    update 	ESUS_CAD_DOMICILIAR set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));  
  elsif (ie_tipo_lote_esus_p = '3') then
    update 	ESUS_ATEND_INDIVIDUAL set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '4') then
    update 	ESUS_ATIVIDADE_COLETIVA set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and dt_atividade between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '5') then
    update 	ESUS_FICHA_PROC_PROFIS set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '6') then
    update 	ESUS_VISITA_DOMICILIAR set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '7') then
    update 	ESUS_AVAL_ELEGIBILIDADE set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '8') then
    update 	ESUS_ATEND_DOMICILIAR set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '9') then
    update 	ESUS_ATEND_ODONTOLOG set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '10') then
    update 	ESUS_MARC_CONS_ALIMENT set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '11') then
    update 	ESUS_ZICA_MICROCEFALIA set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = '12') then
    update 	ESUS_VACINACAO set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  elsif (ie_tipo_lote_esus_p = 'T') then
    update 	ESUS_CAD_INDIVIDUAL set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_CAD_DOMICILIAR set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_ATEND_INDIVIDUAL set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_ATIVIDADE_COLETIVA set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and dt_atividade between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_FICHA_PROC_PROFIS set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_VISITA_DOMICILIAR set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_AVAL_ELEGIBILIDADE set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_ATEND_DOMICILIAR set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_ATEND_ODONTOLOG set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_MARC_CONS_ALIMENT set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
    update 	ESUS_ZICA_MICROCEFALIA set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
	update 	ESUS_VACINACAO set nr_seq_lote_envio = nr_seq_lote_p where	nr_seq_lote_envio is null and dt_liberacao is not null and trunc(dt_atendimento) between trunc(dt_inicio_p) and trunc(nvl(dt_fim_p,sysdate));
  end if;
END ESUS_CARREGAR_FICHAS_LOTE;

/
