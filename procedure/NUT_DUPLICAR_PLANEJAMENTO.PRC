create or replace
procedure nut_duplicar_planejamento (nr_sequencia_p   Number,
				     nm_usuario_p     varchar2) is

	nr_seq_composicao_w	Number(10); 
	nr_seq_receita_w       	Number(10); 
	dt_atualizacao_w        date;
	dt_atualizacao_nrec_w   date;  
	nm_usuario_nrec_w       varchar2(15); 
	nr_seq_opcao_sel_w      Number(10);  
	nr_seq_servico_dia_w    Number(10);  
	ie_receita_especial_w   varchar2(1); 
	nr_seq_acompanhante_w   Number(10);  
 	cd_acompanhante_w       varchar2(10);
	nr_seq_cardapio_dia_w   Number(10);  
	ie_tipo_cardapio_w      varchar2(2); 
	nr_seq_cardapio_w       Number(10);   
	qt_refeicao_w           Number(15);   
	cd_dieta_w              Number(10);   
	nr_seq_refeicao_w       Number(10);  
	nr_nova_seq_card_dia_w  Number(10);  


Cursor C001 is
	select  nr_seq_composicao,      
		nr_seq_receita,         
		dt_atualizacao,         
		dt_atualizacao_nrec,
		nm_usuario_nrec,        
		nr_seq_opcao_sel,       
		nr_seq_servico_dia,    
		ie_receita_especial,
		nr_seq_acompanhante,
		cd_acompanhante,        
		nr_seq_cardapio_dia,
		ie_tipo_cardapio,       
		nr_seq_cardapio,        
		qt_refeicao,            
		cd_dieta,               
		nr_seq_refeicao
	from 	nut_pac_opcao_rec  
	where   nr_seq_cardapio_dia = nr_sequencia_p;

begin
   if nr_sequencia_p > 0 then
   
      select nut_cardapio_dia_seq.nextval
      into   nr_nova_seq_card_dia_w
      from dual;		
   
   
      insert into nut_cardapio_dia (	  	
			nr_sequencia,           
			cd_estabelecimento,     
			dt_atualizacao,
			nm_usuario,             
			nr_seq_servico,         
			dt_cardapio,            
			qt_pessoa_atend,       
			dt_atualizacao_nrec,    
			nm_usuario_nrec,        
			nr_seq_opcao,           
			ie_dia_semana,          
			ie_semana,              
			dt_vigencia_inicial,    
			dt_vigencia_final,      
			cd_dieta,               
			qt_pessoa_atend_real,   
			nr_seq_local,           
			nr_seq_nome_cardapio,   
			dt_liberacao,           
			nm_usuario_liberacao,   
			ie_tipo_cardapio,       
			nr_seq_grupo_producao,
			ie_cardapio_padrao,
			nr_seq_cycle,
                                                nr_seq_cycle_day )

       (        select 	nr_nova_seq_card_dia_w,           
			cd_estabelecimento,     
			dt_atualizacao,         
			nm_usuario_p,             
			nr_seq_servico,         
			dt_cardapio,            
			qt_pessoa_atend,        
			null,    
			nm_usuario_p,        
			nr_seq_opcao,           
			ie_dia_semana,         
			ie_semana,              
			dt_vigencia_inicial,    
			dt_vigencia_final,      
			cd_dieta,               
			qt_pessoa_atend_real,   
			nr_seq_local,           
			nr_seq_nome_cardapio,   
			null,           
			null,   
			ie_tipo_cardapio,       
			nr_seq_grupo_producao,
			ie_cardapio_padrao,
                                                nr_seq_cycle,
                                                nr_seq_cycle_day
	        from	nut_cardapio_dia			
		where 	nr_sequencia = nr_sequencia_p);
	
	open C001;
	loop
	fetch C001 into	
		nr_seq_composicao_w,
		nr_seq_receita_w,
		dt_atualizacao_w,
		dt_atualizacao_nrec_w,
		nm_usuario_nrec_w,
		nr_seq_opcao_sel_w,
		nr_seq_servico_dia_w,
		ie_receita_especial_w,
		nr_seq_acompanhante_w,
		cd_acompanhante_w,
		nr_seq_cardapio_dia_w,
		ie_tipo_cardapio_w,
		nr_seq_cardapio_w,
		qt_refeicao_w,
		cd_dieta_w,
		nr_seq_refeicao_w;
	exit when C001%notfound;
		begin
		  
		  insert into nut_pac_opcao_rec (
				
			  	nr_sequencia,           
				nr_seq_composicao,      
				nr_seq_receita,         
				dt_atualizacao,         
				nm_usuario,          
				dt_atualizacao_nrec,
				nm_usuario_nrec,        
				nr_seq_opcao_sel,       
				nr_seq_servico_dia,    
				dt_liberacao,           
				ie_receita_especial,
				nr_seq_acompanhante,
				cd_acompanhante,        
				nr_seq_cardapio_dia,
				ie_tipo_cardapio,       
				nr_seq_cardapio,        
				qt_refeicao,            
				cd_dieta,               
				nr_seq_refeicao)
		  values (
			        nut_pac_opcao_rec_seq.nextval,
				nr_seq_composicao_w,
				nr_seq_receita_w,
				dt_atualizacao_w,
				nm_usuario_p,
				dt_atualizacao_nrec_w,
				nm_usuario_p,
				nr_seq_opcao_sel_w,
				nr_seq_servico_dia_w,
				null,
				ie_receita_especial_w,
				nr_seq_acompanhante_w,
				cd_acompanhante_w,
				nr_nova_seq_card_dia_w,
				ie_tipo_cardapio_w,
				nr_seq_cardapio_w,
				qt_refeicao_w,
				cd_dieta_w,
				nr_seq_refeicao_w); 
		
		end;
	end loop;
	close C001;

   
   end if;


 
end nut_duplicar_planejamento;
/
