create or replace
procedure gerar_titulo_rec_carta(	nr_seq_carta_p		carta_compromisso.nr_sequencia%type,
				ie_commit_p		varchar2 default 'N',
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is
	
vl_carta_w		carta_compromisso.vl_editado%type;
cd_cgc_w		carta_compromisso.cd_cgc_emitente%type;
cd_moeda_padrao_w	parametro_contas_receber.cd_moeda_padrao%type;
nr_seq_trans_fin_baixa_w	parametro_contas_receber.nr_seq_trans_fin_baixa%type;
nr_seq_trans_fin_carta_w	parametro_contas_receber.nr_seq_trans_fin_contab%type;
cd_portador_w		parametro_contas_receber.cd_portador%type;
cd_tipo_portador_w		parametro_contas_receber.cd_tipo_portador%type;
pr_juro_padrao_w			parametro_contas_receber.pr_juro_padrao%type;
pr_multa_padrao_w			parametro_contas_receber.pr_multa_padrao%type;
cd_tipo_taxa_juro_w			parametro_contas_receber.cd_tipo_taxa_juro%type;
cd_tipo_taxa_multa_w		parametro_contas_receber.cd_tipo_taxa_multa%type;
cd_estab_financeiro_w		estabelecimento.cd_estab_financeiro%type;
nr_titulo_w					titulo_receber.nr_titulo%type;
cd_condicao_pagamento_w 	pessoa_juridica_estab.cd_cond_pagto%type;
qt_vencimentos_w 			number(5) := 1;
ds_vencimentos_w 			varchar2(2000);
dt_vencimento_w 			titulo_receber.dt_vencimento%type;
cd_conta_financ_w 			transacao_financeira.CD_CONTA_FINANC%type;

begin

select	max(a.cd_moeda_padrao),
		max(a.nr_seq_trans_fin_baixa),
		max(a.nr_seq_trans_fin_carta),
		max(a.cd_portador),
		max(a.cd_tipo_portador),
		max(a.pr_juro_padrao),
		max(a.pr_multa_padrao),
		max(a.cd_tipo_taxa_juro),
		max(a.cd_tipo_taxa_multa)
into	cd_moeda_padrao_w,
		nr_seq_trans_fin_baixa_w,
		nr_seq_trans_fin_carta_w,
		cd_portador_w,
		cd_tipo_portador_w,
		pr_juro_padrao_w,
		pr_multa_padrao_w,
		cd_tipo_taxa_juro_w,
		cd_tipo_taxa_multa_w
from	parametro_contas_receber a
where	a.cd_estabelecimento = cd_estabelecimento_p;	

select	max(a.cd_estab_financeiro)
into	cd_estab_financeiro_w
from	estabelecimento a
where	a.cd_estabelecimento = cd_estabelecimento_p;

if	(nr_seq_carta_p is not null) then
	begin
			
		select 	max(nvl(vl_editado, vl_original)),
				max(cd_cgc_emitente)
		into	vl_carta_w,
				cd_cgc_w
		from	carta_compromisso
		where	nr_sequencia = nr_seq_carta_p;

		select 	max(cd_cond_pagto)
		into	cd_condicao_pagamento_w
		from 	pessoa_juridica_estab
		where 	cd_estabelecimento = cd_estabelecimento_p
		and		cd_cgc = cd_cgc_w;

		if	(cd_condicao_pagamento_w is not null) then
			
			Calcular_Vencimento(cd_estabelecimento_p, 
								cd_condicao_pagamento_w,
								sysdate,
								qt_vencimentos_w,
								ds_vencimentos_w);
				
			if (qt_vencimentos_w = 1) then
				dt_vencimento_w	:= To_Date(substr(ds_vencimentos_w,1,10),'dd/mm/yyyy');
			end if;
			
		end if;
		
		select	titulo_seq.nextval
		into	nr_titulo_w
		from	dual;

		insert into titulo_receber(	nr_titulo, 
									cd_cgc,
									dt_emissao,
									dt_vencimento,
									dt_pagamento_previsto,
									vl_titulo,
									vl_saldo_titulo,
									vl_saldo_juros,
									vl_saldo_multa,
									tx_desc_antecipacao,
									tx_juros,
									tx_multa,
									cd_tipo_taxa_juro,
									cd_tipo_taxa_multa,
									cd_moeda,
									cd_portador,
									cd_tipo_portador,
									nr_seq_carta,		
									ie_origem_titulo,
									ie_situacao,
									ie_tipo_emissao_titulo,
									ie_tipo_titulo,
									ie_tipo_inclusao,
									nr_seq_trans_fin_contab,
									nr_seq_trans_fin_baixa,
									ds_observacao_titulo,
									cd_estabelecimento,
									cd_estab_financeiro,
									dt_atualizacao,
									nm_usuario) 
							values(	nr_titulo_w,
									cd_cgc_w,
									sysdate,
									nvl(dt_vencimento_w, sysdate + 30),
									nvl(dt_vencimento_w, sysdate + 30),
									vl_carta_w,
									vl_carta_w,
									0, --Saldo de juros padr�o 0, pois o t�tulo est� sendo gerado na data atual, sysdate
									0, --Saldo de multa padr�o 0, pois o t�tulo est� sendo gerado na data atual, sysdate
									0, --Taxa desconto antecipa��o n�o se aplica ao processo de cartas
									nvl(pr_juro_padrao_w, 0),
									nvl(pr_multa_padrao_w, 0),
									cd_tipo_taxa_juro_w,
									cd_tipo_taxa_multa_w,
									cd_moeda_padrao_w,
									cd_portador_w,
									cd_tipo_portador_w,
									nr_seq_carta_p,					
									'9', --IE_ORIGEM_TITULO: Outros (dominio 709)
									'1', --IE_SITUACAO: aberto
									'1', --IE_TIPO_EMISSAO_TITULO: sin emisi�n de boleto bancario (dominio 702)
									'16', --IE_TIPO_TITULO: "Outros" (dominio 712)
									'2', --IE_TIPO_INCLUSAO: Autom�tica
									nr_seq_trans_fin_carta_w,
									nr_seq_trans_fin_baixa_w,
									expressao_pck.obter_desc_expressao(948270) || ' ' || nr_seq_carta_p, --T�tulo a receber gerado automaticamente para a carta de compromisso de pagamento NR_SEQ_CARTA
									cd_estabelecimento_p,
									nvl(cd_estab_financeiro_w, cd_estabelecimento_p),
									sysdate,
									nm_usuario_p);
		
		select	max(cd_conta_financ)
		into	cd_conta_financ_w
		from	transacao_financeira
		where	nr_sequencia = nr_seq_trans_fin_carta_w;
		
		inserir_classif_tit_rec(nr_titulo_w, cd_conta_financ_w, null, null, null, vl_carta_w, nm_usuario_p);
							
		update	carta_compromisso 
		set		nr_titulo_gerado = nr_titulo_w 
		where	nr_sequencia = nr_seq_carta_p;
		
	end;
end if;

if (nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;										
		
end gerar_titulo_rec_carta;
/