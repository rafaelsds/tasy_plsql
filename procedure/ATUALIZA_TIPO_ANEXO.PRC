create or replace
procedure atualiza_tipo_anexo(nm_tabela_p		varchar2,
			nr_seq_tipo_anexo_p	varchar2,
			nr_sequencia_p		varchar2) is

ds_comando_w	varchar2(2000);
ds_parametros_w	varchar2(2000);
ds_sep_bv_w	varchar2(10);							
							
begin

ds_sep_bv_w	:=	obter_separador_bv;

ds_comando_w 	:=	' update ' || nm_tabela_p ||
			' set	nr_seq_tipo_anexo = :nr_seq_tipo_anexo' ||
			' where	nr_sequencia = :nr_sequencia ';
					
ds_parametros_w	:=	'nr_seq_tipo_anexo=' || nr_seq_tipo_anexo_p || ds_sep_bv_w ||
			'nr_sequencia=' || nr_sequencia_p || ds_sep_bv_w;
					
exec_sql_dinamico_bv('Atualiza_tipo_anexo', ds_comando_w, ds_parametros_w);

commit;

end atualiza_tipo_anexo;
/