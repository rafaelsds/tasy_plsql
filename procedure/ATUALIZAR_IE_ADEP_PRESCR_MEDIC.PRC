create or replace
procedure atualizar_ie_adep_prescr_medic(
		nr_prescricao_p		number,
		cd_setor_atendimento_p	number,
		ident_prescr_adep_p	varchar2,
		nm_usuario_p		varchar2) is

ie_adep_w	varchar2(1) := 'S';		
begin
if	(nr_prescricao_p is not null) and
	(ident_prescr_adep_p is not null) then
	begin
	if	(ident_prescr_adep_p = 'DS') and
		(cd_setor_atendimento_p is not null) then
		begin
		select	decode(ie_adep,'P','N', ie_adep) 
		into	ie_adep_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_p;		
		end;
	elsif	(ident_prescr_adep_p = 'NV') or
		(ident_prescr_adep_p = 'PNV') then
		begin
		ie_adep_w := 'N';
		end;
	elsif	(ident_prescr_adep_p = 'PPN') then
		begin
		ie_adep_w := '';
		end;
	end if;
	
	update	prescr_medica
	set	ie_adep		= ie_adep_w,
		nm_usuario 	= nm_usuario_p,
		dt_atualizacao 	= sysdate
	where	nr_prescricao 	= nr_prescricao_p;	
	end;
end if;
commit;
end atualizar_ie_adep_prescr_medic;
/