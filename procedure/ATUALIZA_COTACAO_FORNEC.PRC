create or replace
procedure atualiza_cotacao_fornec(	nr_cot_compra_P			number,
					nr_seq_autor_p			number,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number	) is
			
cd_cgc_fornec_w		varchar2(14);			
nr_cot_compra_forn_w	number(10,0);
cd_condicao_pagamento_w	number(10,0);

cd_moeda_w		number(5,0);
ie_grava_cond_pag_obs_w	varchar2(1);
ie_grava_id_obs_w		varchar2(1);
ie_grava_fat_min_obs_w	varchar2(1);
ie_grava_dia_entr_obs_w	varchar2(1);
ie_grava_val_prop_obs_w	varchar2(1);

nr_cot_compra_forn_item_w	number(10,0);
nr_seq_cot_forn_w		number(10,0);
nr_item_cot_compra_w	number(5,0);
qt_material_w		number(13,4);
cd_material_w		number(10,0);
nr_seq_marca_w		number(10,0);
ds_marca_w		varchar2(30);
qt_conv_compra_estoque_w	number(13,4);
ie_libera_cot_pesq_portal_w	varchar2(1);


Cursor C01 is
	select	distinct
		cd_cgc_fornec
	from	material_autor_cirurgia
	where	nr_seq_autorizacao = nr_seq_autor_p
	and	cd_cgc_fornec is not null
	order by 1;

Cursor C02 is
	select	cd_material,
		nvl(qt_solicitada,qt_material),
		nr_seq_marca
	from	material_autor_cirurgia
	where	nr_seq_autorizacao = nr_seq_autor_p
	and	cd_cgc_fornec = cd_cgc_fornec_w
	order by 1;

begin

ie_libera_cot_pesq_portal_w := nvl(Obter_Valor_Param_Usuario(3006,60, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_p),'N');

open C01;
loop
fetch C01 into
	cd_cgc_fornec_w;
exit when C01%notfound;
	begin

	select	cot_compra_forn_seq.nextval
	into	nr_cot_compra_forn_w
	from	dual;

	select	nvl(max(cd_cond_pagto),0)
	into	cd_condicao_pagamento_w
	from	pessoa_juridica_estab
	where	cd_cgc = cd_cgc_fornec_w
	and	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(cd_condicao_pagamento_w = 0) then
		begin
		select	nvl(cd_condicao_pagamento_padrao,0)
		into	cd_condicao_pagamento_w
		from	parametro_compras b
		where	cd_estabelecimento = cd_estabelecimento_p;
		end;
	end if;
	
	if	(cd_condicao_pagamento_w = 0) then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(254753);
		end;
	end if;
	
	select	nvl(max(b.cd_moeda_padrao),0)
	into	cd_moeda_w
	from	parametro_compras b
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(cd_moeda_w = 0) then
		begin
		wheb_mensagem_pck.exibir_mensagem_abort(254752);
		end;
	end if;
	
	insert into cot_compra_forn(
		nr_sequencia,
		nr_cot_compra,
		cd_cgc_fornecedor,
		dt_atualizacao,
		nm_usuario,
		cd_condicao_pagamento,
		cd_moeda,
		ie_frete,
		ie_gerado_bionexo,
		ie_status_envio_email_lib)
	values(	nr_cot_compra_forn_w,
		nr_cot_compra_p,
		cd_cgc_fornec_w,
		sysdate,
		nm_usuario_p,
		cd_condicao_pagamento_w,
		cd_moeda_w,
		'F',
		'N',
		'N');
		
	if	(ie_libera_cot_pesq_portal_w = 'S') then
		begin
		update cot_compra_forn
		set ie_status = 'LH',
			ie_liberada_internet = 'S'
		where	nr_cot_compra = nr_cot_compra_p;
		end;
	end if;		
		
	open C02;
	loop
	fetch C02 into
		cd_material_w,
		qt_material_w,
		nr_seq_marca_w;
	exit when C02%notfound;
		begin

		select	nvl(max(qt_conv_compra_estoque),0)
		into	qt_conv_compra_estoque_w
		from	material
		where	cd_material = cd_material_w;
		
		select	cot_compra_forn_item_seq.nextval
		into	nr_cot_compra_forn_item_w
		from	dual;
		
		select	substr(obter_desc_marca(nr_seq_marca_w),1,30)
		into	ds_marca_w
		from	dual;
		
		if	(nvl(nr_seq_marca_w,0) = 0) then
			ds_marca_w	:= substr(obter_marca_material(cd_material_w,'DR'),1,30);
		end if;
		
		select	nvl(min(nr_item_cot_compra),0)
		into	nr_item_cot_compra_w
		from	cot_compra_item
		where	nr_cot_compra = nr_cot_compra_p
		and	cd_material = cd_material_w;
		
		if	(nr_item_cot_compra_w > 0) then
		
			insert into cot_compra_forn_item(
				nr_sequencia,
				nr_seq_cot_forn,
				nr_cot_compra,
				nr_item_cot_compra,
				cd_cgc_fornecedor,
				qt_material,
				vl_unitario_material,
				dt_atualizacao,
				nm_usuario,
				vl_preco_liquido,
				vl_total_liquido_item,
				ie_situacao,
				ds_marca,
				cd_material)
			values(	nr_cot_compra_forn_item_w,
				nr_cot_compra_forn_w,  --seq da tabela cot_compra_forn
				nr_cot_compra_p,
				nr_item_cot_compra_w,
				cd_cgc_fornec_w,
				qt_material_w,
				0,
				sysdate,
				nm_usuario_p,
				0,
				0,
				'A',
				substr(ds_marca_w,1,30),
				cd_material_w);
				
		end if;
		end;
	end loop;
	close C02;
	
	envia_email_fornec(nr_cot_compra_p,cd_cgc_fornec_w,nm_usuario_p,cd_estabelecimento_p);
	--envia_email_cot_usuario_regra(nr_cot_compra_p,nm_usuario_p,cd_estabelecimento_p); Retirado OS 732611
	end;
end loop;
close C01;

commit;

end atualiza_cotacao_fornec;
/
