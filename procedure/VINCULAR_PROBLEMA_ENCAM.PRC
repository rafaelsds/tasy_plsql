create or replace
procedure vincular_problema_encam	(ds_lista_problema_p varchar2,
									nr_seq_encaminhamento_p number
									) is 
									
	ds_lista_problema_w	varchar2(1000);
	nr_pos_virgula_w	number(5);
	nr_seq_problema_w	lista_problema_pac.nr_sequencia%type;
begin

	if (ds_lista_problema_p is not null
	and nr_seq_encaminhamento_p is not null) then
		
		ds_lista_problema_w := ds_lista_problema_p;
		
		if (substr(ds_lista_problema_w, length(ds_lista_problema_w)) <> ',') then 
			ds_lista_problema_w := ds_lista_problema_w ||',';
		end if;
		
		while (ds_lista_problema_w is not null) loop
			begin
				
				nr_pos_virgula_w := instr(ds_lista_problema_w,',');
				
				if (nr_pos_virgula_w <> 0) then
					nr_seq_problema_w		:= substr(ds_lista_problema_w, 1, (nr_pos_virgula_w - 1));
					ds_lista_problema_w	:= substr(ds_lista_problema_w, (nr_pos_virgula_w + 1), length(ds_lista_problema_w));
				end if;
				
				if (nr_seq_problema_w is not null) then
					
					insert into atend_encaminhamento_probl (
						nr_sequencia, 
						dt_atualizacao, 
						nm_usuario, 
						dt_atualizacao_nrec, 
						nm_usuario_nrec, 
						nr_seq_encaminhamento, 
						nr_seq_problema					
					) values (
						atend_encaminhamento_probl_seq.nextval,
						sysdate,
						wheb_usuario_pck.get_nm_usuario,
						sysdate,
						wheb_usuario_pck.get_nm_usuario,
						nr_seq_encaminhamento_p,
						nr_seq_problema_w
					);
					
				
				end if;
				
			end;
		end loop;
	
	end if;
	
end vincular_problema_encam;
/
