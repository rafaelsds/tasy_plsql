create or replace
procedure libera_estorna_portal(
			ie_liberacao_p			varchar2,
			nr_ordem_compra_p		number) is 

begin

update	ordem_compra 
set 	ie_liberacao_portal	= ie_liberacao_p
where 	nr_ordem_compra 	= nr_ordem_compra_p;

commit;

end libera_estorna_portal;
/