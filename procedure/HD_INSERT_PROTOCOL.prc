create or replace procedure hd_insert_protocol(nr_seq_proc_p      number,
                                                cd_protocolo_p     number,
                                                nr_seq_protocolo_exam_p  number,
												nr_sequencia_p		number,
                                                nm_usuario_p	varchar2,
												cd_pessoa_fisica_p varchar2,
												NR_CONTROL_P	number) is

nr_sequencia_w		hd_protocolo_exame.nr_sequencia%type := null;

begin

if(NR_CONTROL_P = 0)then

	insert into hd_protocolo_exame 	(nr_sequencia,
											nr_seq_protocolo,
											dt_atualizacao,
											nm_usuario,
											dt_atualizacao_nrec,
											nm_usuario_nrec,
											cd_pessoa_fisica)
											values(
											hd_protocolo_exame_seq.nextval,
											nr_seq_protocolo_exam_p,
											sysdate,
											nm_usuario_p,
											sysdate,
											nm_usuario_p,
											cd_pessoa_fisica_p);
											
	commit;
end if;

select 	max(nr_sequencia)
into 	nr_sequencia_w
from 	hd_protocolo_exame 
where	cd_pessoa_fisica = cd_pessoa_fisica_p
order by nr_sequencia desc;


insert into hd_protocolo_medic_proc (nr_sequencia,
												dt_atualizacao,
												nm_usuario,
												dt_atualizacao_nrec,
												nm_usuario_nrec,
												nr_seq_proc,
												cd_protocolo,
												nr_seq_protocolo_exam)
									values(nr_sequencia_p,
											sysdate,
											nm_usuario_p,
											sysdate,
											nm_usuario_p,
											nr_seq_proc_p,
											cd_protocolo_p,
											nr_sequencia_w); 
											
commit;
end hd_insert_protocol;
/
