CREATE OR REPLACE procedure WINT_PROC_INTERNO_TUSS (nr_seq_proc_interno_p number,
										            nr_seq_proc_tuss_p 	  out number) IS
										           
nr_seq_exame_w regra_proc_interno_integra.nr_seq_exame%type;

BEGIN
	
	nr_seq_proc_tuss_p := null;
	
	select	a.cd_procedimento
	into    nr_seq_proc_tuss_p
	from	proc_interno_conv_tuss a,
			regra_proc_interno_integra b
	where	a.ie_situacao = 'A'
	and b.ie_tipo_integracao = 26
	and a.nr_seq_proc_interno = nr_seq_proc_interno_p
	and a.nr_seq_proc_interno 	= b.nr_seq_proc_interno
	and	rownum = 1
	order by a.dt_atualizacao desc;
exception
when no_data_found then
	
	BEGIN
		select	a.cd_procedimento_tuss
		into    nr_seq_proc_tuss_p
		from	proc_interno a,
				regra_proc_interno_integra b
		where	a.ie_situacao = 'A'
		and	b.ie_tipo_integracao = 26
		and a.nr_sequencia = nr_seq_proc_interno_p
		and a.nr_sequencia 	= b.nr_seq_proc_interno
		and	rownum = 1
		order by a.dt_atualizacao desc;
	exception
	when no_data_found then
	
		BEGIN
			select 	nr_seq_exame
			into    nr_seq_exame_w
			from	regra_proc_interno_integra
			where ie_tipo_integracao = 26
		    and	nr_seq_proc_interno = nr_seq_proc_interno_p
		    and	rownum = 1
		    order by dt_atualizacao desc;
		exception
		when no_data_found then
	
			BEGIN
				select a.cd_procedimento
				into nr_seq_proc_tuss_p
				from exame_laboratorio c,
					 regra_proc_interno_integra d,
					 exame_lab_conv_tuss a
				where c.ie_situacao = 'A'
				and a.ie_situacao = 'A'
				and d.ie_tipo_integracao = 26
				and a.nr_seq_exame = c.nr_seq_exame
				and c.nr_seq_proc_interno = nr_seq_proc_interno_p
				and c.nr_seq_proc_interno = d.nr_seq_proc_interno
				and rownum = 1
				order by c.dt_atualizacao desc;
			   
			exception
			when no_data_found THEN

				select 	max(a.cd_procedimento_tuss)
				into	nr_seq_proc_tuss_p
				from 	exame_laboratorio a,
						regra_proc_interno_integra d
				WHERE a.ie_situacao = 'A'
				and	d.ie_tipo_integracao = 26
				and a.nr_seq_proc_interno = nr_seq_proc_interno_p
				and a.nr_seq_proc_interno = d.nr_seq_proc_interno;

		  END;
	 END;
END;

END WINT_PROC_INTERNO_TUSS;
/
