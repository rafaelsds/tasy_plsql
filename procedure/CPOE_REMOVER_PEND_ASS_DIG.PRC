create or replace procedure cpoe_remover_pend_ass_dig(
					nr_atendimento_p	prescr_medica.nr_atendimento%type,
					ds_prescricoes_p	varchar2,
					nr_seq_cpoe_p		number,
					ie_tipo_item_p		varchar2) is

ds_prescricoes_w		varchar2(500 char);
nm_tabela_rep_w			varchar2(30 char);
ie_medico_w				varchar2(1 char);
dt_assinatura_medico_w	tasy_assinatura_digital.dt_assinatura%type;

cursor c01 is
select	a.nr_prescricao,
		a.cd_prescritor,
		a.cd_estabelecimento,
		a.cd_perfil_ativo,
		a.nm_usuario,
		a.dt_liberacao_medico,
		a.cd_pessoa_fisica,
		a.nr_seq_assinatura,
		a.nr_seq_assinatura_enf,
		a.dt_liberacao
from	prescr_medica a,
		table(lista_pck.obter_lista(ds_prescricoes_w)) b
where	a.nr_atendimento = nr_atendimento_p
and		a.nr_prescricao = b.nr_registro;

begin

if (ds_prescricoes_p is not null) then
	ds_prescricoes_w := replace(replace(ds_prescricoes_p,';',','),'"','');
else
	if (ie_tipo_item_p = 'DI') then
		ds_prescricoes_w := to_char(obter_nr_prescr_seq_cpoe('D', nr_seq_cpoe_p));
	elsif (ie_tipo_item_p = 'D') then
		ds_prescricoes_w := to_char(obter_nr_prescr_seq_cpoe('DO', nr_seq_cpoe_p));
	else -- M, MAT, SOL, P, O, R, S, LD, J, HM
		ds_prescricoes_w := to_char(obter_nr_prescr_seq_cpoe(ie_tipo_item_p, nr_seq_cpoe_p));
	end if;
end if;

for r_c01 in c01 loop

	if (obter_se_existe_item_nao_ass(r_c01.nr_prescricao) = 'N') then

		wheb_assist_pck.set_informacoes_usuario(wheb_usuario_pck.get_cd_estabelecimento, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario);

		if	(r_c01.cd_prescritor is not null) and ((wheb_assist_pck.get_cd_certificado is not null) or (wheb_assist_pck.get_gerar_sem_certificado = 'S')) then
			ie_medico_w	:= obter_se_medico(r_c01.cd_prescritor, 'M');

			wheb_assist_pck.set_nr_seq_projeto_ass(1);

			if	(ie_medico_w = 'S') then
				if	(r_c01.dt_liberacao_medico is not null) then
					gerar_registro_pendente_pep(
						cd_tipo_registro_p			=> 'XREP',
						nr_sequencia_registro_p		=> r_c01.nr_prescricao,
						cd_pessoa_fisica_p			=> r_c01.cd_pessoa_fisica,
						nr_atendimento_p			=> nr_atendimento_p,
						nm_usuario_p				=> r_c01.nm_usuario,
						ie_tipo_pendencia_p			=> 'L',
						cd_funcao_pend_p			=> obter_funcao_ativa);
				end if;

				dt_assinatura_medico_w := obter_data_assinatura_digital(r_c01.nr_seq_assinatura);

				if	(dt_assinatura_medico_w is not null) or ((r_c01.dt_liberacao_medico is not null) and (not wheb_assist_pck.get_se_gera_assinatura)) then
					gerar_registro_pendente_pep(
						cd_tipo_registro_p			=> 'XREP',
						nr_sequencia_registro_p		=> r_c01.nr_prescricao,
						cd_pessoa_fisica_p			=> r_c01.cd_pessoa_fisica,
						nr_atendimento_p			=> nr_atendimento_p,
						nm_usuario_p				=> r_c01.nm_usuario,
						ie_tipo_pendencia_p			=> 'A',
						cd_funcao_pend_p			=> obter_funcao_ativa);
				end if;
			end if;

			if	(obter_data_assinatura_digital(r_c01.nr_seq_assinatura_enf) is not null) or
				((r_c01.dt_liberacao is not null) and
				 (not wheb_assist_pck.get_se_gera_assinatura)) or
				((trunc(r_c01.dt_liberacao,'mi') = trunc(r_c01.dt_liberacao_medico,'mi')) and
				 (r_c01.dt_liberacao is not null) and
				 (r_c01.dt_liberacao_medico is not null) and
				 (not wheb_assist_pck.get_se_gera_assinatura)) then
					gerar_registro_pendente_pep(
						cd_tipo_registro_p			=> 'XREP',
						nr_sequencia_registro_p		=> r_c01.nr_prescricao,
						cd_pessoa_fisica_p			=> r_c01.cd_pessoa_fisica,
						nr_atendimento_p			=> nr_atendimento_p,
						nm_usuario_p				=> r_c01.nm_usuario,
						ie_tipo_pendencia_p			=> 'A',
						cd_funcao_pend_p			=> obter_funcao_ativa);
			end if;
		end if;
	end if;
end loop;

end cpoe_remover_pend_ass_dig;
/
