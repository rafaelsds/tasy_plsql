create or replace
Procedure same_receber_devolucao (	ds_prontuario_p		varchar2,
					ie_forma_controle_p	varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is

ds_prontuario_w			varchar2(2000);
ie_contador_w			number(10,0)	:= 0;
tam_lista_w			number(10,0);
ie_pos_virgula_w		number(3,0);
nr_seq_pront_w			number(10,0);
cd_pessoa_fisica_w		varchar2(10);
cd_pessoa_solicitante_w		varchar2(10);
cd_setor_atendimento_w		number(5,0);
nr_seq_solic_pront_w		number(10,0);
nr_seq_solic_w			number(10,0);
nr_seq_same_pront_w		number(10,0);

cd_pessoa_pront_w		varchar2(10);
nr_atendimento_w		number(10);

ie_status_param_w		varchar2(1);
ie_setor_same_w			varchar2(1);

cd_setor_same_w			number(10);
cd_setor_solicitante_w		number(5);
ds_parametro_w			varchar2(10) := obter_valor_param_usuario(941,103,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
dt_periodo_inicial_w		date;
dt_periodo_final_w		date;
nr_seq_prontuario_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	same_prontuario
	where	(nr_atendimento		= nr_atendimento_w or nr_atendimento_w = 0)
	and	cd_pessoa_fisica	= cd_pessoa_pront_w
	and	dt_periodo_inicial_w	>= dt_periodo_inicial
	and	dt_periodo_final_w	<= nvl(dt_periodo_final,sysdate)
	and	((ds_parametro_w = 'N') or (cd_estabelecimento = cd_estabelecimento_p))
	order 	by nr_atendimento;

Cursor C02 is
	select	nr_sequencia
	from	same_prontuario
	where	(nr_atendimento		= nr_atendimento_w or nr_atendimento_w = 0)
	and	cd_pessoa_fisica	= cd_pessoa_pront_w
	and	((ds_parametro_w = 'N') or (cd_estabelecimento = cd_estabelecimento_p))
	order 	by nr_atendimento;

begin

ie_status_param_w	:= nvl(obter_valor_param_usuario(941, 82, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'D');
ie_setor_same_w		:= nvl(obter_valor_param_usuario(941, 113, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N');

ds_prontuario_w		:= ds_prontuario_p;

select	substr(obter_pessoa_fisica_usuario(nm_usuario_p, 'C'),1,10),
	obter_setor_usuario(nm_usuario_p)
into	cd_pessoa_fisica_w,
	cd_setor_atendimento_w
from	dual;

while	ds_prontuario_w is not null or ie_contador_w > 200 loop
	begin
	tam_lista_w		:= length(ds_prontuario_w);
	ie_pos_virgula_w	:= instr(ds_prontuario_w,',');

	if	(ie_pos_virgula_w <> 0) then
		nr_seq_pront_w		:= substr(ds_prontuario_w,1,(ie_pos_virgula_w-1));
		ds_prontuario_w		:= substr(ds_prontuario_w,(ie_pos_virgula_w+1),tam_lista_w);
	end if;
	
	if	(ie_forma_controle_p = 'N') then

		select	max(a.nr_sequencia)
		into	nr_seq_solic_pront_w
		from	same_solic_pront a
		where	a.nr_sequencia		= nr_seq_pront_w
		and	((a.ie_status		= 'D') or
			(a.ie_status		= ie_status_param_w))
		and	a.dt_receb_devolucao	is null;
		
		update	same_solic_pront
		set	dt_receb_devolucao	= sysdate,
			nm_usuario_receb_dev = nm_usuario_p,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			ie_status	= 'D'
		where	nr_sequencia	= nr_seq_solic_pront_w
		and	dt_receb_devolucao	is null;
		
		select	max(a.cd_pessoa_fisica),
			nvl(max(a.nr_atendimento),0),
			max(a.cd_pessoa_solicitante),
			max(a.cd_setor_solicitante),
			max(dt_periodo_inicial),
			max(dt_periodo_final)
		into	cd_pessoa_pront_w,
			nr_atendimento_w,
			cd_pessoa_solicitante_w,
			cd_setor_solicitante_w,
			dt_periodo_inicial_w,
			dt_periodo_final_w
		from	same_solic_pront a
		where	a.nr_sequencia = nr_seq_solic_pront_w;
		
		if (ie_setor_same_w = 'S') then			
			
			select	max(nr_sequencia)
			into	nr_seq_prontuario_w
			from	same_prontuario
			where	cd_pessoa_fisica = cd_pessoa_pront_w
			and	((nr_atendimento = nr_atendimento_w) or (nvl(nr_atendimento_w,0) = 0));
			
			cd_setor_same_w := obter_setor_same(cd_estabelecimento_p, nr_seq_prontuario_w);
			
			/*select	max(cd_Setor_Atendimento) 
			into	cd_setor_same_w
			from	setor_atendimento 
			where	cd_classif_setor = 6
			and	cd_estabelecimento_base = cd_estabelecimento_p;
			
			if (nvl(cd_setor_same_w,0) = 0) then
				select	max(cd_Setor_Atendimento) 
				into	cd_setor_same_w
				from	setor_atendimento 
				where	cd_classif_setor = 6;
			end if;*/
			
			update	same_prontuario
			set	ie_status = '1',
				cd_setor_atendimento = cd_setor_same_w
			where	cd_pessoa_fisica = cd_pessoa_pront_w
			and	((nr_atendimento = nr_atendimento_w) or (nvl(nr_atendimento_w,0) = 0));
			
		end if;
		
		if (dt_periodo_inicial_w is not null) and (dt_periodo_final_w is not null) then
			open C01;
			loop
			fetch C01 into	
				nr_seq_same_pront_w;
			exit when C01%notfound;
				begin
				Same_Gerar_Historico (	nr_seq_same_pront_w,
							31,
							cd_pessoa_solicitante_w,
							cd_setor_solicitante_w,
							nm_usuario_p);
				end;
			end loop;
			close C01;
		else
			open C02;
			loop
			fetch C02 into	
				nr_seq_same_pront_w;
			exit when C02%notfound;
				begin
				Same_Gerar_Historico (	nr_seq_same_pront_w,
							31,
							cd_pessoa_solicitante_w,
							cd_setor_solicitante_w,
							nm_usuario_p);
				end;
			end loop;
			close C02;
		end if;
		
	else
		select	max(b.nr_sequencia),
			max(b.nr_seq_solic),
			nvl(max(a.nr_atendimento),0),
			max(a.cd_pessoa_fisica),
			max(a.cd_setor_solicitante),
			max(a.cd_pessoa_solicitante)
		into	nr_seq_solic_pront_w,
			nr_seq_solic_w,
			nr_atendimento_w,
			cd_pessoa_pront_w,
			cd_setor_solicitante_w,
			cd_pessoa_solicitante_w
		from	same_solic_pront a,
			same_solic_pront_envelope b
		where	a.nr_sequencia		= b.nr_seq_solic
		and	b.nr_seq_prontuario	= nr_seq_pront_w
		and	((a.ie_status		= 'D') or
			(a.ie_status		= ie_status_param_w))
		and	b.dt_receb_devolucao	is null;

		update	same_solic_pront_envelope
		set	dt_receb_devolucao	= sysdate,
			cd_pessoa_receb_dev	= cd_pessoa_fisica_w,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_solic_pront_w
		and	dt_receb_devolucao	is null;
	
		if (nvl(nr_seq_solic_w,0) > 0) then	
		
			update	same_solic_pront
			set	ie_status = 'D'
			where	nr_sequencia = nr_seq_solic_w;		
			
			-- Atualizar status do prontuário e gerar histórico
			
			update	same_prontuario
			set	ie_status = '1'
			where	nr_sequencia = nr_seq_pront_w;
									
			Same_Gerar_Historico (	nr_seq_pront_w,
						31,
						cd_pessoa_solicitante_w,
						cd_setor_solicitante_w,
						nm_usuario_p);
			
		end if;
		
	end if;
	
	ie_contador_w	:= ie_contador_w + 1;

	end;
end loop;

commit;

end same_receber_devolucao;
/