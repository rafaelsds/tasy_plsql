create or replace
procedure pls_gerar_cd_guia_regra
			(	nr_seq_guia_p	Number,
				nm_usuario_p	Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar guia requisicao lote..
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ x]    Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_guia_w			Varchar2(255);
ie_preco_w			Varchar2(255);
nr_seq_regra_w			Number(10);
nr_seq_apresentacao_w		Number(10);
ie_tipo_campo_w			Varchar2(255);
cd_fixo_w			Varchar2(255);
ds_mascara_w			Varchar2(255);
ds_campo_w			Varchar2(255) := null;
nr_digito_w			Varchar2(255);
ie_tipo_processo_w		Varchar2(2);
ie_estagio_compl_w		Number(2);
ie_tipo_segurado_w		Varchar2(2);
ie_separador_w			Varchar2(10);
ie_tipo_intercambio_w		Varchar2(255);
ie_pagamento_automatico_w	Varchar2(3);
ie_regra_apres_aut_w		varchar2(1) := 'N';
ie_origem_solic_w		varchar2(1);
nr_seq_pgto_aut_w		number(10);
ie_origem_execucao_w		pls_guia_plano.ie_origem_execucao%type;

Cursor C01 is
	select	nr_seq_apresentacao,
		ie_tipo_campo,
		cd_fixo,
		ds_mascara
	from	pls_regra_guia_campo
	where	nr_seq_regra_guia	= nr_seq_regra_w
	order by nr_seq_apresentacao;

begin

select	b.ie_tipo_guia,
	b.ie_tipo_processo,
	nvl(b.ie_estagio_complemento,3),
	a.ie_preco,
	c.ie_tipo_segurado,
	b.ie_tipo_intercambio,
	b.ie_pagamento_automatico,
	b.ie_origem_solic,
	b.nr_seq_pgto_aut,
	b.ie_origem_execucao
into	ie_tipo_guia_w,
	ie_tipo_processo_w,
	ie_estagio_compl_w,
	ie_preco_w,
	ie_tipo_segurado_w,
	ie_tipo_intercambio_w,
	ie_pagamento_automatico_w,
	ie_origem_solic_w,
	nr_seq_pgto_aut_w,
	ie_origem_execucao_w
from	pls_segurado	c,
	pls_guia_plano	b,
	pls_plano	a
where	a.nr_sequencia	= b.nr_seq_plano
and	c.nr_sequencia	= b.nr_seq_segurado
and	b.nr_sequencia	= nr_seq_guia_p;

-- De acordo com o Diogo, deve ser verificado se a guia se encaixou em uma regra de apresentacao automatica
-- pelo campo NR_SEQ_PGTO_AUT e nao pelo campo IE_PAGAMENTO_AUTOMATICO
if	(nr_seq_pgto_aut_w is not null) then
	ie_regra_apres_aut_w := 'S';
end if;

if	(pls_obter_se_controle_estab('RE') = 'S')then
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_gerar_guia
	where	ie_situacao		= 'A'
	and	(ie_tipo_guia		is null	or ie_tipo_guia		= ie_tipo_guia_w)
	and	(ie_preco		is null	or ie_preco		= ie_preco_w)
	and	(ie_tipo_processo 	is null	or ie_tipo_processo	= ie_tipo_processo_w)
	and	(
		(ie_tipo_complemento	is null)
	or	((ie_tipo_complemento	= 1) 	and (ie_estagio_compl_w	in(1,2)))
	or	((ie_tipo_complemento	= 2)	and (ie_estagio_compl_w	= 3))
		)
	and	(ie_tipo_segurado	is null	or ie_tipo_segurado	= ie_tipo_segurado_w)
	and	(
		(ie_tipo_intercambio	is null)
	or	(ie_tipo_intercambio	= 'N')
	or	(ie_tipo_intercambio	= ie_tipo_intercambio_w)
		)
	and	(
		(ie_regra_apres_aut	is null)
	or 	(ie_regra_apres_aut	= ie_regra_apres_aut_w)
		)
	and	(ie_origem_solic	is null or ie_origem_solic 	= ie_origem_solic_w)
	and	(ie_origem_execucao	is null or ie_origem_execucao	= ie_origem_execucao_w)
	and 	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;
else
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_gerar_guia
	where	ie_situacao		= 'A'
	and	(ie_tipo_guia		is null	or ie_tipo_guia		= ie_tipo_guia_w)
	and	(ie_preco		is null	or ie_preco		= ie_preco_w)
	and	(ie_tipo_processo 	is null	or ie_tipo_processo	= ie_tipo_processo_w)
	and	(
		(ie_tipo_complemento	is null)
	or	((ie_tipo_complemento	= 1) 	and (ie_estagio_compl_w	in(1,2)))
	or	((ie_tipo_complemento	= 2)	and (ie_estagio_compl_w	= 3))
		)
	and	(ie_tipo_segurado	is null	or ie_tipo_segurado	= ie_tipo_segurado_w)
	and	(
		(ie_tipo_intercambio	is null)
	or	(ie_tipo_intercambio	= 'N')
	or	(ie_tipo_intercambio	= ie_tipo_intercambio_w)
		)
	and	(
		(ie_regra_apres_aut	is null)
	or 	(ie_regra_apres_aut	= ie_regra_apres_aut_w)
		)
	and	(ie_origem_solic	is null or ie_origem_solic 	= ie_origem_solic_w)
	and	(ie_origem_execucao	is null or ie_origem_execucao	= ie_origem_execucao_w);
end if;

begin
	select	nvl(substr(ie_separador,1,10),'.')
	into	ie_separador_w
	from	pls_regra_gerar_guia
	where	nr_sequencia	= nr_seq_regra_w;
exception
when others then
	ie_separador_w	:= '.';
end;

if	(nr_seq_regra_w	is not null) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_apresentacao_w,
		ie_tipo_campo_w,
		cd_fixo_w,
		ds_mascara_w;
	exit when C01%notfound;
		begin
		
		if	(ie_tipo_campo_w	= 'SG') then
			if	(length(ds_mascara_w)	< length(nr_seq_guia_p)) then
				if	(ds_campo_w	is null) then
					ds_campo_w	:= ds_campo_w||substr(nr_seq_guia_p,1,length(ds_mascara_w));
				else
					ds_campo_w	:= ds_campo_w||ie_separador_w||substr(nr_seq_guia_p,1,length(ds_mascara_w));
				end if;
			elsif	(length(ds_mascara_w)	> length(nr_seq_guia_p))  then
				if	(ds_campo_w	is null) then
					ds_campo_w	:= ds_campo_w||adiciona_zeros_esquerda(nr_seq_guia_p,length(ds_mascara_w));
				else
					ds_campo_w	:= ds_campo_w||ie_separador_w||adiciona_zeros_esquerda(nr_seq_guia_p,length(ds_mascara_w));
				end if;
			else
				if	(ds_campo_w	is null) then
					ds_campo_w	:= ds_campo_w||nr_seq_guia_p;
				else
					ds_campo_w	:= ds_campo_w||ie_separador_w||nr_seq_guia_p;
				end if;
			end if;
		elsif	(ie_tipo_campo_w	= 'VP') then
			if	(length(ds_mascara_w)	< length(cd_fixo_w)) then
				if	(ds_campo_w	is null) then
					ds_campo_w	:= ds_campo_w||substr(cd_fixo_w,1,length(ds_mascara_w));
				else
					ds_campo_w	:= ds_campo_w||ie_separador_w||substr(cd_fixo_w,1,length(ds_mascara_w));
				end if;
			elsif	(length(ds_mascara_w)	> length(cd_fixo_w))  then
				if	(ds_campo_w	is null) then
					ds_campo_w	:= ds_campo_w||adiciona_zeros_esquerda(cd_fixo_w,length(ds_mascara_w));
				else
					ds_campo_w	:= ds_campo_w||ie_separador_w||adiciona_zeros_esquerda(cd_fixo_w,length(ds_mascara_w));
				end if;
			else
				if	(ds_campo_w	is null) then
					ds_campo_w	:= ds_campo_w||cd_fixo_w;
				else
					ds_campo_w	:= ds_campo_w||ie_separador_w||cd_fixo_w;
				end if;
			end if;
		elsif	(ie_tipo_campo_w	= 'DV') then
			if	(cd_fixo_w	is not null) then
				if	(length(ds_mascara_w)	< length(cd_fixo_w)) then
					if	(ds_campo_w	is null) then
						ds_campo_w	:= ds_campo_w||substr(cd_fixo_w,1,length(ds_mascara_w));
					else
						ds_campo_w	:= ds_campo_w||ie_separador_w||substr(cd_fixo_w,1,length(ds_mascara_w));
					end if;
				elsif	(length(ds_mascara_w)	> length(cd_fixo_w))  then
					if	(ds_campo_w	is null) then
						ds_campo_w	:= ds_campo_w||adiciona_zeros_esquerda(cd_fixo_w,length(ds_mascara_w));
					else
						ds_campo_w	:= ds_campo_w||ie_separador_w||adiciona_zeros_esquerda(cd_fixo_w,length(ds_mascara_w));
					end if;
				else
					if	(ds_campo_w	is null) then
						ds_campo_w	:= ds_campo_w||cd_fixo_w;
					else
						ds_campo_w	:= ds_campo_w||ie_separador_w||cd_fixo_w;
					end if;
				end if;
			else
				select	substr(calcula_digito('Modulo11',ds_campo_w),1,2)
				into	nr_digito_w
				from	dual;

				if	(length(ds_mascara_w)	< length(nr_digito_w)) then
					if	(ds_campo_w	is null) then
						ds_campo_w	:= ds_campo_w||substr(nr_digito_w,1,length(ds_mascara_w));
					else
						ds_campo_w	:= ds_campo_w||ie_separador_w||substr(nr_digito_w,1,length(ds_mascara_w));
					end if;
				elsif	(length(ds_mascara_w)	> length(nr_digito_w))  then
					if	(ds_campo_w	is null) then
						ds_campo_w	:= ds_campo_w||adiciona_zeros_esquerda(nr_digito_w,length(ds_mascara_w));
					else
						ds_campo_w	:= ds_campo_w||ie_separador_w||adiciona_zeros_esquerda(nr_digito_w,length(ds_mascara_w));
					end if;
				else
					if	(ds_campo_w	is null) then
						ds_campo_w	:= ds_campo_w||nr_digito_w;
					else
						ds_campo_w	:= ds_campo_w||ie_separador_w||nr_digito_w;
					end if;
				end if;
			end if;
		end if;	
		
		end;
	end loop;
	close C01;

	update	pls_guia_plano
	set	cd_guia			= ds_campo_w,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		cd_guia_prestador	= nvl(cd_guia_prestador, ds_campo_w)
	where	nr_sequencia		= nr_seq_guia_p;

	--Atualizar o campo e guia referencia dos anexos de guia
	update	pls_lote_anexo_guias_aut
	set	cd_guia_referencia	= ds_campo_w,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_seq_guia		= nr_seq_guia_p;
	
end if;

commit;

end pls_gerar_cd_guia_regra;
/