create or replace 
procedure ATUALIZAR_AUTOR_ITEM_CONTA
				(nr_atendimento_p	in number,
				nr_seq_autorizacao_p	in number,
				cd_convenio_p		in number,
				nm_usuario_p		in varchar2) is

cd_proc_autor_w		number(15);
cd_mat_autor_w		number(15);
ie_origem_proced_w	varchar2(255);
nr_seq_material_w	number(15);
nr_seq_mat_autor_w	number(15);
nr_seq_proc_autor_w	number(15);
nr_seq_procedimento_w	number(15);

cursor	c01 is
select 	cd_procedimento,
	ie_origem_proced,
	nr_sequencia
from 	procedimento_autorizado
where	nr_sequencia_autor	= nr_seq_autorizacao_p;

cursor	c02 is
select 	cd_material,
	nr_sequencia
from 	material_autorizado
where	nr_sequencia_autor	= nr_seq_autorizacao_p;

cursor	c03 is
select 	nr_sequencia
from 	procedimento_paciente
where	cd_procedimento		= cd_proc_autor_w
and 	ie_origem_proced 	= ie_origem_proced_w
and 	nr_atendimento		= nr_atendimento_p
and 	cd_convenio		= cd_convenio_p
and	nr_interno_conta	is not null
and 	cd_motivo_exc_conta	is null;

cursor	c04 is
select 	nr_sequencia
from 	material_atend_paciente
where	cd_material		= cd_mat_autor_w
and 	nr_atendimento		= nr_atendimento_p
and 	cd_convenio_p		= cd_convenio_p
and	nr_interno_conta	is not null
and 	cd_motivo_exc_conta	is null;

begin

open c01;
loop
fetch c01 into
	cd_proc_autor_w,
	ie_origem_proced_w,
	nr_seq_proc_autor_w;
exit when c01%notfound;

	open c03;
	loop
	fetch c03 into
		nr_seq_procedimento_w;
	exit when c03%notfound;

		update	procedimento_paciente
		set	nr_seq_proc_autor	= nvl(nr_seq_proc_autor,nr_seq_proc_autor_w)
		where	nr_sequencia		= nr_seq_procedimento_w;

	end loop;
	close c03;

end loop;
close c01;


open c02;
loop
fetch c02 into
	cd_mat_autor_w,
	nr_seq_mat_autor_w;
exit when c02%notfound;

	open c04;
	loop
	fetch c04 into
		nr_seq_material_w;
	exit when c04%notfound;

		update	material_atend_paciente
		set	nr_seq_mat_autor	= nvl(nr_seq_mat_autor,nr_seq_mat_autor_w)
		where	nr_sequencia		= nr_seq_material_w;

	end loop;
	close c04;

end loop;
close c02;

commit;

end ATUALIZAR_AUTOR_ITEM_CONTA;
/