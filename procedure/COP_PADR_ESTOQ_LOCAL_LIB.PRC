create or replace
procedure cop_padr_estoq_local_lib(
		cd_local_estoque_p		number,
		cd_local_estoque_dest_p		number,
		ie_sobrescrever_regra_p		varchar2,
		nm_usuario_p			varchar2) as


nr_sequencia_w			Number(10);
ie_liberar_w			varchar2(1);
cd_local_estoque_w		number(10);
cd_grupo_material_w		number(5);
cd_subgrupo_material_w		number(5);
cd_classe_material_w		number(5);
cd_material_w			number(6);
ie_origem_documento_w		varchar2(3);
nr_seq_familia_w		number(10);

cursor c01 is
select	a.cd_local_estoque,
	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material,
	a.cd_material,
	a.ie_origem_documento,
	a.ie_liberar,
	a.nr_seq_familia
from	loc_estoque_estrut_materiais a
where	a.cd_local_estoque = cd_local_estoque_p
and not exists(
	select	1
	from	loc_estoque_estrut_materiais x
	where	x.cd_local_estoque		= cd_local_estoque_dest_p
	and	nvl(x.cd_grupo_material, 0)	= nvl(a.cd_grupo_material, 0)
	and	nvl(x.cd_subgrupo_material, 0) = nvl(a.cd_subgrupo_material, 0)
	and	nvl(x.cd_classe_material, 0)	= nvl(a.cd_classe_material, 0)
	and	nvl(x.nr_seq_familia, 0)	= nvl(a.nr_seq_familia, 0)
	and	nvl(x.cd_material, 0) 	= nvl(a.cd_material, 0)
	and	nvl(x.ie_origem_documento, 'X') = nvl(a.ie_origem_documento, 'X'));
		
BEGIN

if	(ie_sobrescrever_regra_p = 'S') then
	delete
	from	loc_estoque_estrut_materiais
	where	cd_local_estoque = cd_local_estoque_dest_p;
end if;

open c01;
loop
fetch c01 into
	cd_local_estoque_w,
	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	cd_material_w,
	ie_origem_documento_w,
	ie_liberar_w,
	nr_seq_familia_w;
exit when c01%NOTFOUND;
	begin

	select	max(nr_sequencia) + 1
	into	nr_sequencia_w
	from	loc_estoque_estrut_materiais;
	
	insert into loc_estoque_estrut_materiais(
		nr_sequencia,
		cd_local_estoque,
		dt_atualizacao,
		nm_usuario,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		nr_seq_familia,
		cd_material,
		ie_origem_documento,
		ie_liberar)
	values(	nr_sequencia_w,
		cd_local_estoque_dest_p,
		sysdate,
		nm_usuario_p,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		nr_seq_familia_w,
		cd_material_w,
		ie_origem_documento_w,
		ie_liberar_w);

	end;
end loop;
close c01;

commit;

END cop_padr_estoq_local_lib;
/