create or replace
procedure obter_resposta_ws_cotacao(	cd_estabelecimento_p		number) is 
					
nr_cot_compra_w				number(10);
qt_existe_w				number(10);	
ds_erro_w				varchar2(255);
ie_tipo_integracao_envio_w			varchar2(1);

cursor c01 is
select	nr_cot_compra
from	(
	select	a.nr_cot_compra
	from	cot_compra a
	where	nr_documento_externo is not null
	and	a.dt_geracao_ordem_compra is null
	and	a.ie_tipo_integracao_envio in(1,2,3)
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.nr_seq_motivo_cancel is null
	and	a.dt_retorno_prev <= sysdate
	and not exists(
		select	1
		from	cot_compra_forn x
		where	x.nr_cot_compra = a.nr_cot_compra)
	order by	nvl(obter_ultimo_log_integ_cot(a.nr_cot_compra, 'R'), sysdate - 365))
where	rownum <= 1;

begin

open C01;
loop
fetch C01 into	
	nr_cot_compra_w;
exit when C01%notfound;
	begin
	ds_erro_w := '';	
	
	select	count(*)
	into	qt_existe_w
	from	cliente_integracao
	where	nr_seq_inf_integracao = 143
	and	ie_situacao = 'A';
	
	if	(qt_existe_w > 0) then
	
		gerar_historico_cotacao_integr(
			nr_cot_compra_w,
			wheb_mensagem_pck.get_texto(299185),
			wheb_mensagem_pck.get_texto(299186),
			'S',
			'Job');			
		gravar_agend_integracao(98,'nr_cot_compra=' || nr_cot_compra_w || ';');		
	end if;
	end;
end loop;
close C01;

commit;

end obter_resposta_ws_cotacao;
/