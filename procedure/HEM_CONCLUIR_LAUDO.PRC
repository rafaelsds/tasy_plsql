create or replace
procedure hem_concluir_laudo(	nr_seq_proc_p		Number,
				nm_usuario_p		Varchar2) is 

begin
if	(nvl(nr_seq_proc_p,0) > 0) then

	update	hem_proc
	set	dt_digitacao	= sysdate,
		nm_usuario_dig	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_proc_p;

end if;
commit;

end hem_concluir_laudo;
/