create or replace
procedure IMPORTAR_PF(ie_tipo_pessoa_p		number,
				nm_pessoa_fisica_p		varchar2,
				nm_usuario_p			varchar2,
				dt_nascimento_p			date,
				ie_sexo_p			varchar2,
				ie_estado_civil_p		varchar2,
				nr_cpf_p			varchar2,
				nr_identidade_p			varchar2,
				nr_telefone_celular_p		varchar2,
				ie_grau_instrucao_p		number,
				nr_cep_cidade_nasc_p		varchar2,
				nr_prontuario_p			number,
				cd_religiao_p			number,
				nr_pis_pasep_p			varchar2,
				cd_nacionalidade_p		varchar2,
				ie_dependencia_sus_p		varchar2,
				qt_altura_cm_p			number,
				ie_tipo_sangue_p		varchar2,
				ie_fator_rh_p			varchar2,
				dt_obito_p			date,
				nr_iss_p			varchar2,
				nr_inss_p			varchar2,
				nr_cert_nasc_p			varchar2,
				cd_cargo_p			number,
				ds_codigo_prof_p		varchar2,
				ie_funcionario_p		varchar2,
				nr_seq_cor_pele_p		number,
				ds_orgao_emissor_ci_p		varchar2,
				nr_cartao_nac_sus_p		number,
				cd_cbo_sus_p			number,
				cd_atividade_sus_p		number,
				ie_vinculo_sus_p		number,
				cd_estabelecimento_p		number,
				cd_sistema_ant_p		varchar2,
				cd_funcionario_p		varchar2,
				nr_pager_bip_p			varchar2,
				nr_transacao_sus_p		varchar2,
				cd_medico_p			varchar2,
				nm_pessoa_pesquisa_p		varchar2,
				dt_emissao_ci_p                 date,
				nr_seq_conselho_p             	number,
				dt_admissao_hosp_p              date,
				ie_fluencia_portugues_p         varchar2,
				nr_titulo_eleitor_p             varchar2,
				nr_zona_p                       varchar2,
				nr_secao_p                      varchar2,
				nr_cartao_estrangeiro_p		varchar2,
				nr_reg_geral_estrang_p		varchar2,
				dt_chegada_brasil_p		date,
				ds_historico_p			varchar2,
				ie_tipo_prontuario_p		number,
				ds_observacao_p			varchar2,
				qt_dependente_p			number,
				nr_transplante_p		number,
				nm_pessoa_fisica_sem_acento_p	varchar2,
				cd_empresa_p			number,
				dt_revisao_p			date,
				nm_usuario_revisao_p		varchar2,
				dt_demissao_hosp_p		date,
				nr_contra_ref_sus_p		varchar2,
				cd_puericultura_p		number,
				sg_emissora_ci_p		varchar2,
				dt_naturalizacao_pf_p		date,
				nr_ctps_p			number,
				nr_serie_ctps_p			varchar2,
				uf_emissora_ctps_p		varchar2,
				dt_emissao_ctps_p		date,
				nr_portaria_nat_p		varchar2,
				ds_senha_p			varchar2,
				ds_fonetica_p			varchar2,
				ie_revisar_p			varchar2,
				cd_cnes_p			varchar2,
				nr_seq_perfil_p			number,
				nr_same_p			number,
				nr_seq_cbo_saude_p		number,
				dt_integracao_externa_p		date,
				ds_fonetica_cns_p		varchar2,
				ds_apelido_p			varchar2,
				dt_geracao_pront_p		date,
				cd_municipio_ibge_p		varchar2,
				nr_seq_pais_p			number,
				nr_cert_casamento_p		varchar2,
				nr_seq_cartorio_nasc_p		number,
				nr_seq_cartorio_casamento_p	number,
				dt_emissao_cert_nasc_p		date,
				dt_emissao_cert_casamento_p	date,
				nr_folha_cert_nasc_p		number,
				nr_folha_cert_casamento_p	number,
				nr_livro_cert_nasc_p		number,
				nr_livro_cert_casamento_p	number,
				nr_seq_nut_perfil_p		number,
				nr_registro_pls_p		varchar2,
				dt_validade_rg_p		date,
				cd_pessoa_fisica_p	out	varchar2) is 

/* Dominios - 	IE_TIPO_PESSOA_P		1 - M�dico,
						2 - Paciente,
						3 - Funcion�rios,
						4 - Outros
		
		IE_SEXO				'F' - Feminino
						'M' - Masculino
						'I' - Indeterminado
		
		IE_ESTADO_CIVIL			1 - Solteiro          
						2 - Casado            
						3 - Divorciado        
						4 - Desquitado        
						5 - Vi�vo             
						6 - Separado          
						7 - Concubinado
						9 - Outros
		
		IE_GRAU_INSTRUCAO		1 - Analfabeto                
						10 - Prim�rio                  
						11 - N�o Alfabetizado          
						12 - P�s gradua��o             
						13 - Ph.D.                     	
						2 - Primeiro Grau             
						3 - Segundo Grau              
						4 - Superior                  
						5 - Mestrado                  
						6 - Doutor                    
						7 - Primeiro  Grau  Incompleto
						8 - Segundo  Grau  Incompleto
						9 - Superior  Incompleto
			
		SG_EMISSORA_CI, UF_EMISSORA_CTPS	AC - Acre
							AL - Alagoas
							AM - Amazonas
							AP - Amap�
							BA - Bahia
							CE - Cear�
							DF - Distrito Federal
							ES - Espirito Santo
							GO - Goi�s
							IN - Outros(Internacional)
							MA - Maranh�o
							MG - Minas Gerais
							MS - Mato Grosso do Sul
							MT - Mato Grosso
							PA - Par�
							PB - Paraiba
							PE - Pernambuco
							PI - Piau�
							PR - Paran�
							RJ - Rio de Janeiro
							RN - Rio Grande do Norte
							RO - Rondonia
							RR - Roraima
							RS - Rio Grande do Sul
							SC - Santa Catarina
							SE - Sergipe
							SP - S�o Paulo
							TO - Tocantins

		IE_DEPENDENCIA_SUS			2 - Segurado
							4 - C�njuge
							6 - Filho
							8 - Outra depend�ncia

		IE_TIPO_SANGUE				B - B 
							A - A
							O - O
							AB - AB

		IE_FATOR_RH				+ - +
							- - -

		IE_FLUENCIA_PORTUGUES			A - Ausente   
							F - Fluente   
							R - Razo�vel
							S - Sofr�vel

		IE_TIPO_PRONTUARIO			1 - Completo
							2 - B�sico   

		ie_vinculo_sus				1 - Autonomo          
							2 - Desempregado
							3 - Aposentado        
							4 - Nao Segurado
							5 - Empregado         
							6 - Empregador        
							 
*/

cd_pessoa_fisica_w		varchar2(10);
qt_sexo_w			number(10);
qt_estado_civil_w		number(10);
qt_grau_instrucao_w		number(10);
qt_sg_emissora_ci_w		number(10);
qt_uf_emissora_ctps_w		number(10);
qt_dependencia_sus_w		number(10);
qt_tipo_sangue_w		number(10);
qt_fator_rh_w			number(10);
qt_fluencia_portugues_w		number(10);
qt_tipo_prontuario_w		number(10);
qt_vinculo_sus_w		number(10);
cd_sistema_ant_w		varchar2(255);




begin


select	max(cd_sistema_ant)
into	cd_sistema_ant_w
from	pessoa_fisica
where	cd_sistema_ant	= cd_sistema_ant_p;

if	(ie_sexo_p 	is not null) then
	select	count(b.vl_dominio)
	into	qt_sexo_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 4
	and	b.vl_dominio	= ie_sexo_p;
else
	qt_sexo_w := 1;
end if;

if	(ie_estado_civil_p	is not null) then
	select	count(b.vl_dominio)
	into	qt_estado_civil_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 5
	and	b.vl_dominio	= ie_estado_civil_p;
else
	qt_estado_civil_w := 1;
end if;

if	(ie_grau_instrucao_p	is not null)	then
	select	count(b.vl_dominio)
	into	qt_grau_instrucao_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 10
	and	b.vl_dominio	= ie_grau_instrucao_p;
else
	qt_grau_instrucao_w := 1;
end if;

if	(sg_emissora_ci_p	is not null)	then
	select	count(b.vl_dominio)
	into	qt_sg_emissora_ci_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 50
	and	b.vl_dominio	= sg_emissora_ci_p;
else
	qt_sg_emissora_ci_w := 1;
end if;

if	(uf_emissora_ctps_p	is not null)	then
	select	count(b.vl_dominio)
	into	qt_uf_emissora_ctps_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 50
	and	b.vl_dominio	= uf_emissora_ctps_p;
else
	qt_uf_emissora_ctps_w := 1;
end if;

if	(ie_dependencia_sus_p	is not null)	then
	select	count(b.vl_dominio)
	into	qt_dependencia_sus_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 803
	and	b.vl_dominio	= ie_dependencia_sus_p;
else
	qt_dependencia_sus_w := 1;
end if;

if	(ie_tipo_sangue_p	is not null)	then
	select	count(b.vl_dominio)
	into	qt_tipo_sangue_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 1173
	and	b.vl_dominio	= ie_tipo_sangue_p;
else
	qt_tipo_sangue_w := 1;
end if;

if	(qt_fator_rh_w	is not null)	then
	select	count(b.vl_dominio)
	into	qt_fator_rh_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 1174
	and	b.vl_dominio	= ie_fator_rh_p;
else
	qt_fator_rh_w := 1;
end if;

if	(ie_fluencia_portugues_p	is not null)	then
	select	count(b.vl_dominio)
	into	qt_fluencia_portugues_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 1343
	and	b.vl_dominio	= ie_fluencia_portugues_p;
else
	qt_fluencia_portugues_w := 1;
end if;

if	(ie_tipo_prontuario_p	is not null)	then
	select	count(b.vl_dominio)
	into	qt_tipo_prontuario_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 1528
	and	b.vl_dominio	= ie_tipo_prontuario_p;
else
	qt_tipo_prontuario_w := 1;
end if;

if	(ie_vinculo_sus_p	is not null)	then
	select	count(b.vl_dominio)
	into	qt_vinculo_sus_w
	from	dominio a,
		valor_dominio b
	where	a.cd_dominio	= b.cd_dominio
	and	a.cd_dominio	= 800
	and	b.vl_dominio	= ie_vinculo_sus_p;
else
	qt_vinculo_sus_w := 1;
end if;

select	nvl(max(cd_pessoa_fisica),0)
into	cd_pessoa_fisica_w
from	pessoa_fisica
where	nvl(nr_cpf, 'X') 		= nvl(nr_cpf_p, 'X')
and	ie_sexo				= ie_sexo_p
and	upper(nm_pessoa_fisica) like 	upper(nm_pessoa_fisica_p);

if	(cd_sistema_ant_w	is null)	Then
	if	(cd_pessoa_fisica_w	= 0)	Then
		if	(ie_tipo_pessoa_p	is not null)	then
			if	(qt_sexo_w			= 1) and
				(qt_estado_civil_w		= 1) and
				(qt_grau_instrucao_w		= 1) and
				(qt_sg_emissora_ci_w		= 1) and
				(qt_uf_emissora_ctps_w		= 1) and
				(qt_dependencia_sus_w		= 1) and
				(qt_tipo_sangue_w		= 1) and
				(qt_fator_rh_w			= 1) and
				(qt_fluencia_portugues_w	= 1) and
				(qt_tipo_prontuario_w		= 1) 	then
				
				select	pessoa_fisica_seq.nextval
				into	cd_pessoa_fisica_w
				from	dual;
			Begin	
				insert into pessoa_fisica(
					cd_pessoa_fisica,
					ie_tipo_pessoa,
					nm_pessoa_fisica,
					dt_atualizacao,
					nm_usuario,
					dt_nascimento,
					ie_sexo,
					ie_estado_civil,
					nr_cpf,
					nr_identidade,
					nr_telefone_celular,
					ie_grau_instrucao,
					nr_cep_cidade_nasc,
					nr_prontuario,
					cd_religiao,
					nr_pis_pasep,
					cd_nacionalidade,
					ie_dependencia_sus,
					qt_altura_cm,
					ie_tipo_sangue,
					ie_fator_rh,
					dt_obito,
					nr_iss,
					nr_inss,
					nr_cert_nasc,
					cd_cargo,
					ds_codigo_prof,
					ie_funcionario,
					nr_seq_cor_pele,
					ds_orgao_emissor_ci,
					nr_cartao_nac_sus,
					cd_cbo_sus,
					cd_atividade_sus,
					ie_vinculo_sus,
					cd_estabelecimento,
					cd_sistema_ant,
					cd_funcionario,
					nr_pager_bip,
					nr_transacao_sus,
					cd_medico,
					nm_pessoa_pesquisa,
					dt_emissao_ci,
					nr_seq_conselho,
					dt_admissao_hosp,
					ie_fluencia_portugues,
					nr_titulo_eleitor,
					nr_zona,
					nr_secao,
					nr_cartao_estrangeiro,
					nr_reg_geral_estrang,
					dt_chegada_brasil,
					nm_usuario_original,
					dt_cadastro_original,
					ds_historico,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_tipo_prontuario,
					ds_observacao,
					qt_dependente,
					nr_transplante,
					nm_pessoa_fisica_sem_acento,
					cd_empresa,
					dt_revisao,
					nm_usuario_revisao,
					dt_demissao_hosp,
					nr_contra_ref_sus,
					cd_puericultura,
					sg_emissora_ci,
					dt_naturalizacao_pf,
					nr_ctps,
					nr_serie_ctps,
					uf_emissora_ctps,
					dt_emissao_ctps,
					nr_portaria_nat,
					ds_senha,
					ds_fonetica,
					ie_revisar,
					cd_cnes,
					nr_seq_perfil,
					nr_same,	
					nr_seq_cbo_saude,
					dt_integracao_externa,
					ds_fonetica_cns,
					ds_apelido,
					dt_geracao_pront,
					cd_municipio_ibge,
					nr_seq_pais,
					nr_cert_casamento,
					nr_seq_cartorio_nasc,
					nr_seq_cartorio_casamento,
					dt_emissao_cert_nasc,
					dt_emissao_cert_casamento,
					nr_folha_cert_nasc,
					nr_folha_cert_casamento,
					nr_livro_cert_nasc,
					nr_livro_cert_casamento,
					nr_seq_nut_perfil,
					nr_registro_pls,
					dt_validade_rg)
				values(				
					cd_pessoa_fisica_w,
					ie_tipo_pessoa_p,
					nm_pessoa_fisica_p,
					sysdate,
					nm_usuario_p,
					dt_nascimento_p,
					ie_sexo_p,
					ie_estado_civil_p,
					nr_cpf_p,
					nr_identidade_p,
					nr_telefone_celular_p,
					ie_grau_instrucao_p,
					nr_cep_cidade_nasc_p,
					nr_prontuario_p,
					cd_religiao_p,
					nr_pis_pasep_p,
					cd_nacionalidade_p,
					ie_dependencia_sus_p,
					qt_altura_cm_p,
					ie_tipo_sangue_p,
					ie_fator_rh_p,
					dt_obito_p,
					nr_iss_p,
					nr_inss_p,
					nr_cert_nasc_p,
					cd_cargo_p,
					ds_codigo_prof_p,
					ie_funcionario_p,
					nr_seq_cor_pele_p,
					ds_orgao_emissor_ci_p,
					nr_cartao_nac_sus_p,
					cd_cbo_sus_p,
					cd_atividade_sus_p,
					ie_vinculo_sus_p,
					cd_estabelecimento_p,
					cd_sistema_ant_p,
					cd_funcionario_p,
					nr_pager_bip_p,
					nr_transacao_sus_p,
					cd_medico_p,
					nm_pessoa_pesquisa_p,
					dt_emissao_ci_p,
					nr_seq_conselho_p,
					dt_admissao_hosp_p,
					ie_fluencia_portugues_p,
					nr_titulo_eleitor_p,
					nr_zona_p,
					nr_secao_p,
					nr_cartao_estrangeiro_p,
					nr_reg_geral_estrang_p,
					dt_chegada_brasil_p,
					nm_usuario_p,
					sysdate,
					ds_historico_p,
					sysdate,
					nm_usuario_p,
					ie_tipo_prontuario_p,
					ds_observacao_p,
					qt_dependente_p,
					nr_transplante_p,
					nm_pessoa_fisica_sem_acento_p,
					cd_empresa_p,
					dt_revisao_p,
					nm_usuario_revisao_p,
					dt_demissao_hosp_p,
					nr_contra_ref_sus_p,
					cd_puericultura_p,
					sg_emissora_ci_p,
					dt_naturalizacao_pf_p,
					nr_ctps_p,
					nr_serie_ctps_p,
					uf_emissora_ctps_p,
					dt_emissao_ctps_p,
					nr_portaria_nat_p,
					ds_senha_p,
					ds_fonetica_p,
					ie_revisar_p,
					cd_cnes_p,
					nr_seq_perfil_p,
					nr_same_p,	
					nr_seq_cbo_saude_p,
					dt_integracao_externa_p,
					ds_fonetica_cns_p,
					ds_apelido_p,
					dt_geracao_pront_p,
					cd_municipio_ibge_p,
					nr_seq_pais_p,
					nr_cert_casamento_p,
					nr_seq_cartorio_nasc_p,
					nr_seq_cartorio_casamento_p,
					dt_emissao_cert_nasc_p,
					dt_emissao_cert_casamento_p,
					nr_folha_cert_nasc_p,
					nr_folha_cert_casamento_p,
					nr_livro_cert_nasc_p,
					nr_livro_cert_casamento_p,
					nr_seq_nut_perfil_p,
					nr_registro_pls_p,
					dt_validade_rg_p);

			cd_pessoa_fisica_p	:= cd_pessoa_fisica_w;
			exception
			when others then
				null;
			end;
			end if;
		end if;
	end if;
end if;	
commit;

end IMPORTAR_PF;
/
