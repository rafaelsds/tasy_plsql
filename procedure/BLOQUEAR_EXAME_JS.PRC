create or replace
procedure bloquear_exame_js(	nr_prescricao_p		number,
				nr_sequencia_p		number,
				ie_status_atend_p	number,
				nr_seq_motivo_bloq_p 	number,
				ds_observacao_p		varchar2,
				ie_forma_boqueio_p	varchar2,
				nm_usuario_p		Varchar2) is 
				
begin

if	(ie_forma_boqueio_p = 'S') then

	update	prescr_procedimento
	set 	ie_status_atend = ie_status_atend_p,
		nm_usuario 	= nm_usuario_p
	where 	nr_prescricao 	= nr_prescricao_p
	and 	nr_sequencia 	= nr_sequencia_p;
	commit;
	
elsif 	(ie_forma_boqueio_p = 'B') then

	if	(nr_seq_motivo_bloq_p > 0) then
		
		update	prescr_procedimento
		set    	ie_exame_bloqueado = 'S',
			nm_usuario 	= nm_usuario_p,
			nr_seq_motivo_bloqueio = nr_seq_motivo_bloq_p,
			ds_observacao 	= ds_observacao || ' '|| ds_observacao_p
		where  	nr_sequencia 	= nr_sequencia_p
		and    	nr_prescricao 	= nr_prescricao_p;
		commit;
	else 
		update	prescr_procedimento
		set    	ie_exame_bloqueado = 'S',
			nm_usuario 	= nm_usuario_p,
			ds_observacao 	= ds_observacao || ' '|| ds_observacao_p
		where  	nr_sequencia 	= nr_sequencia_p
		and    	nr_prescricao 	= nr_prescricao_p;	
		commit;
	end if;
end if;

end bloquear_exame_js;
/