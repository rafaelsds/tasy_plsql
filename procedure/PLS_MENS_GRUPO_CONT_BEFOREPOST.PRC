create or replace
procedure pls_mens_grupo_cont_beforepost(	nr_contrato_p		number,
					nr_seq_contrato_inter_p	number) is


nr_sequencia_w	number(10);
qt_existe_w	number(10);

begin

if	(nvl(nr_contrato_p,0) > 0) then
	
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	pls_contrato
	where	nr_contrato = nr_contrato_p;
	
	if	(nr_sequencia_w is null) then
		-- O contrato informado n�o existe. Verifique !
		wheb_mensagem_pck.exibir_mensagem_abort(149572);
	end if;

end if;

if	(nvl(nr_seq_contrato_inter_p,0) > 0) then
	
	select	count(*)
	into	qt_existe_w
	from	pls_intercambio
	where	nr_sequencia = nr_seq_contrato_inter_p;
	
	if	(qt_existe_w = 0) then
		-- O contrato de interc�mbio informado n�o existe. Verifique !
		wheb_mensagem_pck.exibir_mensagem_abort(149574);
	end if;

end if;

end pls_mens_grupo_cont_beforepost;
/