create or replace 
procedure CPOE_Consistir_Dose_Terap(
				cd_estabelecimento_p	number,
				nr_atendimento_p	number,
				cd_setor_atendimento_p	number,
				cd_pessoa_fisica_p	varchar2,
				cd_material_p		number,
				qt_dose_p		number,
				nr_unidade_terp_p	number,
				qt_peso_p		number,
				nm_usuario_p		varchar2,
				ie_acao_p	out	varchar2,
				ds_mensagem_p	out	varchar2) is


qt_dose_min_aviso_w		Number(18,6);
qt_dose_max_aviso_w		Number(18,6);
qt_dose_min_bloqueia_w		Number(18,6);
qt_dose_max_bloqueia_w		Number(18,6);
qt_existe_regra_w		Number(10);
qt_idade_w			Number(10);

cursor C01 is
	select	nvl(qt_dose_min_aviso,0),
		nvl(qt_dose_max_aviso,9999999999),
		nvl(qt_dose_min_bloqueia,0),
		nvl(qt_dose_max_bloqueia,9999999999)
	from	material_dose_terap
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_material		= cd_material_p
	and	nr_seq_dose_terap	= nr_unidade_terp_p
	and	qt_idade_w between Obter_idade_dose_ter(nr_sequencia,'MIN') and Obter_idade_dose_ter(nr_sequencia,'MAX')
	and	nvl(qt_peso_p,0) between nvl(qt_peso_minimo,0) and nvl(qt_peso_maximo,9999)
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_p)	= cd_setor_atendimento_p
	and	((cd_doenca_cid is null) or
		 (obter_se_cid_atendimento(nr_atendimento_p,cd_doenca_cid) = 'S'))
	order by 
		nvl(cd_setor_atendimento,0),
		nvl(qt_idade_minima,0),
		nvl(qt_idade_maxima,0),
		nvl(qt_peso_minimo,0),
		nvl(qt_peso_maximo,0);


begin


ie_acao_p	:= '';
ds_mensagem_p	:= '';


select	count(*)
into	qt_existe_regra_w
from	material_dose_terap
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(qt_existe_regra_w > 0) and
	(cd_pessoa_fisica_p is not null) then
	begin
	qt_dose_min_aviso_w	:= null;

	select	obter_idade(dt_nascimento,nvl(dt_obito,sysdate),'DIA')
	into	qt_idade_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

	OPEN C01;
	LOOP
	FETCH C01 into	
		qt_dose_min_aviso_w,
		qt_dose_max_aviso_w,
		qt_dose_min_bloqueia_w,
		qt_dose_max_bloqueia_w;
	EXIT WHEN C01%NOTFOUND;
		begin
		qt_dose_min_aviso_w	:= qt_dose_min_aviso_w;
		qt_dose_max_aviso_w	:= qt_dose_max_aviso_w;
		qt_dose_min_bloqueia_w	:= qt_dose_min_bloqueia_w;
		qt_dose_max_bloqueia_w	:= qt_dose_max_bloqueia_w;
		end;
	END LOOP;
	close c01;

	if	(qt_dose_min_aviso_w is not null) then
		begin
		if	(qt_dose_p < qt_dose_min_bloqueia_w) or
			(qt_dose_p > qt_dose_max_bloqueia_w) then
			begin
			ie_acao_p	:= 'B';
			-- 290366 "A dose terap�utica informada esta fora da faixa liberada pela farm�cia desta institui��o. M�nimo: #@QT_DOSE_MIN_BLOQUEIA#@ M�ximo: #@QT_DOSE_MAX_BLOQUEIA#@."
			ds_mensagem_p	:= wheb_mensagem_pck.get_texto(290366,	'QT_DOSE_MIN_BLOQUEIA='||to_char(qt_dose_min_bloqueia_w)||
										';QT_DOSE_MAX_BLOQUEIA='||to_char(qt_dose_max_bloqueia_w));
			end;
		elsif	(qt_dose_p < qt_dose_min_aviso_w) or
			(qt_dose_p > qt_dose_max_aviso_w) then
			begin
			ie_acao_p	:= 'A';
			-- 290380 "A dose terap�utica informada esta fora da faixa considerada normal pela farm�cia desta institui��o. M�nimo: #@QT_DOSE_MIN_AVISO#@ M�ximo: #@QT_DOSE_MAX_AVISO#@."
			ds_mensagem_p	:= wheb_mensagem_pck.get_texto(290380,	'QT_DOSE_MIN_AVISO='||to_char(qt_dose_min_aviso_w)||
										';QT_DOSE_MAX_AVISO='||to_char(qt_dose_max_aviso_w));
			end;
		end if;
		end;
	end if;

	end;
end if;

end CPOE_Consistir_Dose_Terap;
/