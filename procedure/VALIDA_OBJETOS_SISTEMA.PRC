CREATE OR REPLACE
PROCEDURE Valida_Objetos_Sistema IS
nr_objetos_w		number(15,0);
ds_erro_w			varchar2(1000);
qt_resultado_w     	number(5);
ds_comando_w       	varchar2(2000);
BEGIN
begin

/*INICIO ALTERA��O COELHO -  IMPEDIR DUAS EXECU��ES DESTA PROCEDURE OU SIMULT�NEA A TASY_SINCRONIZAR_BASE*/

gravar_processo_longo('','VALIDA_OBJETOS_SISTEMA',null);	

begin
	SELECT	'A procedure VALIDA_OBJETOS_SITEMA j� est� em execu��o!!' || chr(10) || 
			obter_desc_expressao(648224)/*'Programa   :'*/ || program || chr(10) ||
			obter_desc_expressao(618748)/*'Sid/Serial :'*/ || sid     || ',' || serial# || chr(10) ||
			obter_desc_expressao(328225)/*'Usu�rio    :'*/ || osuser  || CHR(10) ||
			obter_desc_expressao(289514)/*'Esta��o    :'*/ || machine
	INTO	ds_erro_w
	FROM 	v$session
	WHERE	audsid <> (select userenv('sessionid') from dual)
	AND	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	AND	action like 'VALIDA_OBJETOS_SISTEMA%'
	AND	status = 'ACTIVE';
exception
when others then
	ds_erro_w := '';
end;

if	( ds_erro_w is not null )  then
	gravar_processo_longo('','',null);
	wheb_mensagem_pck.exibir_mensagem_abort(278178,'DS_ERRO='||ds_erro_w );
end if;

begin
	SELECT	'A procedure TASY_SINCRONIZAR_BASE est� em execu��o. ' || chr(10) ||
			'A execu��o da procedure VALIDA_OBJETOS_SISTEMA foi cancelada !!!' || chr(10) ||
			obter_desc_expressao(648224)/*'Programa   :'*/ || program || chr(10) ||
			obter_desc_expressao(618748)/*'Sid/Serial :'*/ || sid     || ',' || serial# || chr(10) ||
			obter_desc_expressao(328225)/*'Usu�rio    :'*/ || osuser  || CHR(10) ||
			obter_desc_expressao(289514)/*'Esta��o    :'*/ || machine
	INTO	ds_erro_w
	FROM 	v$session
	WHERE	audsid <> (select userenv('sessionid') from dual)
	AND	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	AND	action like 'TASY_SINCRONIZAR_BASE%' 
	OR 	action like 'TASY_AJUSTAR_COLUNAS%' 
	OR	action like 'TASY_ALTERAR_COLUNA%' 
	OR	action like 'TASY_CRIAR_INDICE%' 
	OR	action like 'TASY_CRIAR_INTEGRIDADE%'
	OR	action like 'TASY_CRIAR_ALTERAR_TABELA%';
exception
when others then
	ds_erro_w := '';
end;

if	( ds_erro_w is not null ) then
	gravar_processo_longo('','',null);
	wheb_mensagem_pck.exibir_mensagem_abort(278178,'DS_ERRO='||ds_erro_w );
end if;

begin
	SELECT	'A procedure TASY_CONSISTIR_BASE est� em execu��o. ' || chr(10) ||
		'A execu��o da procedure VALIDA_OBJETOS_SISTENA foi cancelada !!!' || chr(10) ||
		obter_desc_expressao(648224)/*'Programa   :'*/ || program || chr(10) ||
		obter_desc_expressao(618748)/*'Sid/Serial :'*/ || sid     || ',' || serial# || chr(10) ||
	   	obter_desc_expressao(328225)/*'Usu�rio    :'*/ || osuser  || CHR(10) ||
	   	obter_desc_expressao(289514)/*'Esta��o    :'*/ || machine
	INTO	ds_erro_w
	FROM 	v$session
	WHERE	audsid <> (select userenv('sessionid') from dual)
	AND	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
	AND	action like 'TASY_CONSISTIR_BASE%';
exception
when others then
	ds_erro_w := '';
end;

if	( ds_erro_w is not null ) then
	gravar_processo_longo('','',null);
	wheb_mensagem_pck.exibir_mensagem_abort(278178,'DS_ERRO='||ds_erro_w );
end if;

/*FIM ALTERA��O COELHO*/

select 	count(*)
into 	nr_objetos_w
from 	objetos_invalidos_v;
if  (nr_objetos_w > 0) then
	begin	

	/*ds_comando_w := 'select count(*) ' ||
				'from aplicacao_tasy a ' ||		
		'where upper(a.cd_aplicacao_tasy) = ' || CHR(39) || 'TASY' || CHR(39) ||
		'and ie_status_aplicacao =  ' || CHR(39) || 'U' || CHR(39);

	obter_valor_dinamico_bv(ds_comando_w,null,qt_resultado_w);	

	if (qt_resultado_w > 0) and (obter_se_base_wheb = 'N') then
		exec_sql_dinamico('Tasy','alter package WHEB_USUARIO_PCK compile');
		exec_sql_dinamico('Tasy','alter package WHEB_USUARIO_PCK compile body');
	end if;*/
	
	Compila_Objetos_Invalidos;
	Compila_Objetos_Invalidos;
	Compila_Objetos_Invalidos;
	Compila_Objetos_Invalidos;
	
	end;	
end if;

update 	aplicacao_tasy
set	ie_status_aplicacao = 'A'
where	ie_status_aplicacao = 'I';

/*update 	aplicacao_tasy
set	ie_status_aplicacao = 'I'
where		cd_aplicacao_tasy in 
		(select distinct(a.ds_aplicacao) 
		from objeto_sistema a, user_objects b
		where a.nm_objeto = b.object_name
		  and	b.status	= 'INVALID')
and	ie_status_aplicacao = 'A';*/
		
end;
commit; 

gravar_processo_longo('','',0);

END Valida_Objetos_Sistema;
/
