CREATE OR REPLACE
PROCEDURE Agenda_Tasy_Livre(	nm_usuario_agenda_p	varchar2,
				dt_agenda_p		Date,
				ie_deletar_livres_p	Varchar2,
				nm_usuario_p		Varchar2) is

ie_feriado_w		Varchar2(0001)	:= 'N';
ie_dia_semana_w		Number(001,0);
hr_inicial_w		Date;
hr_final_w			Date;
hr_fim_w			Date;
hr_atual_w			Date;
nr_minuto_intervalo_w	Number(003,0);
nr_seq_tipo_agenda_w	Number(10);
qt_existe_agenda_w	Number(10,0);
qt_bloqueio_w		Number(10,0);
qt_horario_w		Number(0002,0)	:= 0;
nr_sequencia_w		Number(0010,0)	:= 0;
qt_dia_todo_w		Number(10,0)	:= 0;
ie_excluir_livres_w	varchar2(1);

CURSOR C01 IS
	select  to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || hr_inicio,'dd/mm/yyyy hh24:mi'),
                to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || hr_final,'dd/mm/yyyy hh24:mi'),
                qt_min_intervalo,
                nr_seq_tipo
	from	agenda_tasy_horario a
	where	to_date(hr_inicio,'hh24:mi') < to_date(hr_final,'hh24:mi')
	  and	nvl(qt_min_intervalo,0) > 0
	  and	ie_dia_semana		= ie_dia_semana_w 
	  and	nm_usuario_agenda	= nm_usuario_agenda_p
	  and	qt_dia_todo_w		= 0
	union
	select 	to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || hr_inicio,'dd/mm/yyyy hh24:mi'),
                to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || hr_final,'dd/mm/yyyy hh24:mi'),
                qt_min_intervalo,
                nr_seq_tipo
	from 	agenda_tasy_horario a
	where 	ie_dia_semana	= 9
	  and	to_date(hr_inicio,'hh24:mi') < to_date(hr_final,'hh24:mi')
	  and	nvl(qt_min_intervalo,0) > 0
	  and	ie_dia_semana_w between 2 and 6
	  and	nm_usuario_agenda	= nm_usuario_agenda_p
	  and	qt_dia_todo_w		= 0
	  and	not exists
		(select 1
		from	agenda_tasy_horario
		where	ie_dia_semana	= ie_dia_Semana_w
		and	nm_usuario_agenda	= nm_usuario_agenda_p)
	order by 1,2,3;

BEGIN

Obter_Param_Usuario(791,35,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_excluir_livres_w);
--ie_excluir_livres_w := 'N';

if	(ie_deletar_livres_p = 'S') then /* Rafael em 09/01/2007 OS46325 */
	if	(ie_excluir_livres_w = 'S') then
		delete	from agenda_tasy
		where 	nm_usuario_agenda	= nm_usuario_agenda_p
		  and 	ie_status		= 'L'
		  and	dt_agenda >= 	to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || '00:00:01','dd/mm/yyyy hh24:mi:ss') and
			dt_agenda <=	to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || '23:59:59','dd/mm/yyyy hh24:mi:ss');
	else					
		delete	from agenda_tasy
		where 	nm_usuario_agenda	= nm_usuario_agenda_p
		  and 	ie_status		= 'L';
	end if;	  
end if;

select pkg_date_utils.get_WeekDay(dt_agenda_p)
into ie_dia_semana_w
from dual;

select 	Count(*)
into	qt_dia_todo_w
from 	Tipo_Agenda_Tasy b,
	agenda_tasy a
where	a.nm_usuario_agenda	= nm_usuario_agenda_p
 and	a.nr_seq_tipo		= b.nr_sequencia
 and	nvl(ie_dia_todo,'N')	= 'S'
 and	trunc(a.dt_agenda,'dd')	= trunc(dt_agenda_p,'dd');

OPEN C01;
LOOP
FETCH C01 into
	hr_inicial_w,
	hr_final_w,
	nr_minuto_intervalo_w,
	nr_seq_tipo_agenda_w;
exit 	when c01%notfound;
     	BEGIN
	hr_atual_w	:= hr_inicial_w;
	while (hr_atual_w < hr_final_w) LOOP
		BEGIN
		hr_fim_w	:= hr_atual_w + (nr_minuto_Intervalo_w -1) /1440;
		select 	Count(*)
		into	qt_existe_agenda_w
		from 	agenda_tasy
		where	nm_usuario_agenda	= nm_usuario_agenda_p
		  and	((hr_atual_w between	dt_agenda and (dt_agenda + (nr_minuto_duracao - 1) /1440)) or
		  	(hr_fim_w between	dt_agenda and (dt_agenda + (nr_minuto_duracao - 1) /1440)))
	          and	ie_status <> 'C';
		if	(qt_existe_agenda_w = 0) then
			begin
			select 	count(*)
			into	qt_bloqueio_w
			from	ausencia_tasy
			where	nm_usuario_ausente	= nm_usuario_agenda_p
			  and 	dt_inicio	< (hr_atual_w + nr_minuto_Intervalo_w/1440)
		  	  and	dt_fim		> hr_atual_w;


			if	(hr_atual_w >= sysdate) and
				(qt_bloqueio_w = 0) then
				begin
				select agenda_tasy_seq.nextval
				into nr_sequencia_w
				from dual; 
				begin
				insert into agenda_tasy(
					nr_sequencia,
					dt_agenda, 
					nr_seq_tipo,
					nr_minuto_duracao,
					nm_usuario_agenda,
					dt_atualizacao, 
					nm_usuario,
					ie_status)
				values (nr_sequencia_w,
					hr_atual_w,
					nr_seq_tipo_agenda_w,
					nr_minuto_Intervalo_w,
					nm_usuario_agenda_p,
					sysdate,
					nm_usuario_p,
					'L');
				exception
					when others then
						nr_sequencia_w	:= nr_sequencia_w;
				end;
				END;
			end if;
			end;
		end if;
		hr_atual_w		:= hr_atual_w + (nr_minuto_intervalo_w / 1440);
		qt_horario_w 	:= qt_horario_w + 1;
		if	(qt_horario_w > 51 ) then
			hr_atual_w	:= hr_final_w + 1;
		end if;
		END;
	END LOOP;
	END;
END LOOP;
CLOSE C01;
END Agenda_Tasy_Livre;
/