create or replace 
procedure gerar_resumo_vime(	
				cd_paciente_p		in	Varchar2,
				nr_atendimento_p	in	Number,
				nr_prescricao_p		in	Number,
				cd_material_p		in	Number,
				ie_tipo_item_p		in	Varchar2,
				ie_atend_pac_p		in	Varchar2,
				ie_origem_inf_p		in	varchar2,
				cd_unidade_medida_p	in out	varchar2,
				dt_inicio_util_p	out	varchar2,
				dt_fim_util_p		out	varchar2,
				qt_dose_min_p		in out	Number,
				qt_dose_susp_p		in out	Number,
				qt_dose_max_p		in out	Number,
				qt_dose_med_p		in out	Number,
				qt_dose_tot_p		in out	Number,
				ds_unid_med_p		out	Varchar2,
				qt_dia_util_p		out	Number,
				qt_dia_cor_p		out	Number,
				pr_dia_intern_p		in out	Number,
				qt_dose_pend_p		in out	Number,
				dt_primeira_util_p	out	varchar2,
				dt_ultima_util_p	out	varchar2,
				qt_dose_total_medic_p	in out	number) is


cd_unidade_medida_dose_w		varchar2(30) := null;
cd_unidade_medida_dose_ww	varchar2(30) := null;
cd_unidade_medida_aux_w		varchar2(30);
qt_dose_w			number(18,6);
dt_horario_w			date;
dt_horario_ant_w		date;
nr_dose_w			number(10) := 0;
ds_unidade_medida_w		varchar2(50);
cd_pessoa_fisica_w		varchar2(10);
ds_UM_comum_w			varchar2(2000);
ie_atend_pac_w			varchar2(10) := ie_atend_pac_p;

Cursor C01 is
select	count(*),
	b.cd_unidade_medida_dose
from	prescr_mat_hor b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	(((nr_atendimento_p is not null) and
	  (a.nr_atendimento = nr_atendimento_p)) or
	 ((ie_atend_pac_w	= 'PAC') and
	  (cd_pessoa_fisica_w is not null) and	
	  (a.cd_pessoa_fisica 	= cd_pessoa_fisica_w)))
and	b.cd_material		in 	(select	distinct x.cd_material
					from	Material x
					where	ie_origem_inf_p = 'PA'
					and 	x.nr_seq_ficha_tecnica = cd_material_p
					Union All
					Select 	cd_material_p
					from 	dual
					where 	ie_origem_inf_p <> 'PA' )
and	b.dt_suspensao is null
and	b.dt_horario		< sysdate
and	b.qt_dose		> 0
and	nvl(b.ie_horario_especial,'N') = 'N'
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
group by b.cd_unidade_medida_dose
order by 1;

Cursor C02 is
select	(Obter_dose_convertida(b.cd_material,b.qt_dose, b.cd_unidade_medida_dose, cd_unidade_medida_dose_w)),
	b.dt_horario,
	b.cd_unidade_medida
from	prescr_mat_hor b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	(((nr_atendimento_p	is not null) and
	  (a.nr_atendimento	= nr_atendimento_p)) or
	 ((ie_atend_pac_w	= 'PAC') and
	  (cd_pessoa_fisica_w	is not null) and	
	  (a.cd_pessoa_fisica 	= cd_pessoa_fisica_w)))
and	b.cd_material		in 	(select	distinct x.cd_material
					from	Material x
					where	ie_origem_inf_p = 'PA'
					and 	x.nr_seq_ficha_tecnica = cd_material_p
					Union All
					Select 	cd_material_p
					from 	dual
					where 	ie_origem_inf_p <> 'PA' )
and	b.dt_suspensao is null
and	b.dt_horario		< sysdate
and	b.qt_dose		> 0
and	nvl(b.ie_horario_especial,'N') = 'N'
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
order by dt_horario;

begin
qt_dia_util_p		:= 0;
qt_dose_min_p		:= 999999;
qt_dose_max_p		:= 0;
qt_dose_tot_p		:= 0;
dt_horario_ant_w	:= sysdate + 30;

if	(nr_atendimento_p is null) then
	ie_atend_pac_w	:= 'PAC';
end if;
	
select	nvl(max(cd_pessoa_fisica),'0')
into	cd_pessoa_fisica_w
from	prescr_medica
where	(((nr_prescricao is not null) and
	(nr_prescricao = nr_prescricao_p)) or
	((nr_prescricao_p is null) and
	(nr_atendimento_p is not null) and
	(nr_atendimento = nr_atendimento_p)));

if	(cd_pessoa_fisica_w	= '0') then
	cd_pessoa_fisica_w	:= cd_paciente_p;
end if;	
	
if	(cd_unidade_medida_p is null) then
	open C01;
	loop
	fetch C01 into
		qt_dose_w,
		cd_unidade_medida_dose_ww;
	exit when C01%notfound;
		cd_unidade_medida_dose_w	:= cd_unidade_medida_dose_ww;
	end loop;
	close C01;
end if;
if	(ie_origem_inf_p = 'PA') and
	(ds_UM_comum_w is not null) and
	(((cd_unidade_medida_p is not null) and
	(instr(ds_UM_comum_w, cd_unidade_medida_p||'#0') = 0)) or
	(cd_unidade_medida_p is null)) then
	begin
	cd_unidade_medida_dose_w := substr(ds_UM_comum_w, 1, instr(ds_UM_comum_w,'#0') -1);
	end;
end if;

if	(cd_unidade_medida_dose_w is null) then
	cd_unidade_medida_dose_w := cd_unidade_medida_p;
end if;



open C02;
loop
fetch C02 into
	qt_dose_w,
	dt_horario_w,
	cd_unidade_medida_aux_w;
exit when C02%notfound;
	begin
	if	(qt_dose_min_p > qt_dose_w) then
		qt_dose_min_p	:= qt_dose_w;
	end if;

	if	(qt_dose_max_p < qt_dose_w) then
		qt_dose_max_p	:= qt_dose_w;
	end if;

	qt_dose_tot_p	:= qt_dose_tot_p + qt_dose_w;

	nr_dose_w	:= nr_dose_w + 1;

	if	(dt_inicio_util_p is null) or
		(to_date(dt_inicio_util_p, 'dd/mm/yyyy hh24:mi:ss') > dt_horario_w) then
		dt_inicio_util_p	:= to_char(dt_horario_w, 'dd/mm/yyyy hh24:mi:ss');
	end if;

	if	(dt_fim_util_p is null) or
		(to_date(dt_fim_util_p, 'dd/mm/yyyy hh24:mi:ss') < dt_horario_w) then
		dt_fim_util_p	:= to_char(dt_horario_w, 'dd/mm/yyyy hh24:mi:ss');
	end if;

	if	(trunc(dt_horario_ant_w,'dd') <> trunc(dt_horario_w,'dd')) then
		begin
		qt_dia_util_p		:= qt_dia_util_p + 1;
		dt_horario_ant_w	:= dt_horario_w;
		end;
	end if;

	end;
end loop;
close C02;

qt_dose_med_p	:= dividir(qt_dose_tot_p,nr_dose_w);

select	nvl(Obter_Unidade_Medida(cd_unidade_medida_dose_w),cd_unidade_medida_dose_w)
into	ds_unid_med_p
from	dual;

--if	(nvl(cd_unidade_medida_p,'X') = 'X') then
cd_unidade_medida_p	:= cd_unidade_medida_dose_w;
--else
--	cd_unidade_medida_p	:= cd_unidade_medida_p;
--end if;

select	min(b.dt_horario),
	max(b.dt_horario)
into	dt_horario_ant_w,
	dt_horario_w
from	prescr_mat_hor b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	(((nr_atendimento_p 	is not null) and
	  (a.nr_atendimento 	= nr_atendimento_p)) or
	 ((ie_atend_pac_w	= 'PAC') and
	  (cd_pessoa_fisica_w 	is not null) and
	  (a.cd_pessoa_fisica 	= cd_pessoa_fisica_w)))
and	b.cd_material		in (select	distinct x.cd_material
					from	Material x
					where	ie_origem_inf_p = 'PA'
					and 	x.nr_seq_ficha_tecnica = cd_material_p
					Union All
					Select 	cd_material_p
					from 	dual
					where 	ie_origem_inf_p <> 'PA' )
and	b.dt_horario		< sysdate
and	b.dt_suspensao		is null
--and	b.cd_unidade_medida_dose= cd_unidade_medida_dose_w
and	nvl(b.ie_horario_especial,'N') = 'N'
and	b.qt_dose		> 0
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';
If	(nr_atendimento_p is not null) then
	qt_dia_cor_p	:= Obter_Dias_Internacao(nr_atendimento_p);
else
	qt_dia_cor_p	:= obter_dias_entre_datas(dt_horario_ant_w,dt_horario_w) + 1;
end if;
pr_dia_intern_p	:= 100;
if	(qt_dia_cor_p > 0) then
	pr_dia_intern_p	:= round(((qt_dia_util_p * 100) / qt_dia_cor_p),2);
end if;

if	(qt_dose_min_p = 999999) then
	qt_dose_min_p := 0;
end if;

dt_primeira_util_p	:= to_char(dt_horario_ant_w, 'dd/mm/yyyy hh24:mi:ss');

select	nvl(sum(Obter_dose_convertida(b.cd_material,b.qt_dose, b.cd_unidade_medida_dose, cd_unidade_medida_dose_w)),0)
into	qt_dose_susp_p
from	prescr_mat_hor b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	(((nr_atendimento_p	is not null) and
	  (a.nr_atendimento	= nr_atendimento_p)) or
	 ((ie_atend_pac_w	= 'PAC') and
	  (cd_pessoa_fisica_w	is not null) and
	  (a.cd_pessoa_fisica 	= cd_pessoa_fisica_w)))
and	b.cd_material		in	(select	distinct x.cd_material
					from	Material x
					where	ie_origem_inf_p = 'PA'
					and 	x.nr_seq_ficha_tecnica = cd_material_p
					Union All
					Select 	cd_material_p
					from 	dual
					where 	ie_origem_inf_p <> 'PA' )
and	b.dt_suspensao		is not null
and	b.qt_dose		> 0
and	nvl(b.ie_horario_especial,'N') = 'N'
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';

SELECT	MAX(b.DT_FIM_HORARIO)
into	dt_horario_w
FROM	Prescr_mat_hor b
WHERE	b.nr_prescricao		IN 	(SELECT	a.nr_prescricao
					FROM	prescr_medica a
					WHERE	(((nr_atendimento_p	IS NOT NULL) AND
						  (a.nr_atendimento	= nr_atendimento_p)) OR
						 ((ie_atend_pac_w	= 'PAC') AND
						  (cd_pessoa_fisica_w	IS NOT NULL) AND
						  (a.cd_pessoa_fisica 	= cd_pessoa_fisica_w)))
					AND	SYSDATE BETWEEN a.DT_INICIO_PRESCR AND a.DT_VALIDADE_PRESCR
					AND	NVL(DT_LIBERACAO_MEDICO, DT_LIBERACAO) IS NOT NULL)
AND	b.cd_material		IN	(select	distinct x.cd_material
					from	Material x
					where	ie_origem_inf_p = 'PA'
					and 	x.nr_seq_ficha_tecnica = cd_material_p
					Union All
					Select 	cd_material_p
					from 	dual
					where 	ie_origem_inf_p <> 'PA' )
and	b.dt_horario		< sysdate
and	b.dt_suspensao		is null
and	b.qt_dose		> 0
and	b.DT_FIM_HORARIO is not null
and	nvl(b.ie_horario_especial,'N') = 'N'
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';

dt_ultima_util_p	:= to_char(dt_horario_w, 'dd/mm/yyyy hh24:mi:ss');

if 	(dt_ultima_util_p is not null) then
	dt_fim_util_p := null;
end if;

Select	count(*)
into	qt_dose_pend_p
from	prescr_mat_hor b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	(((nr_atendimento_p	is not null) and
	  (a.nr_atendimento	= nr_atendimento_p)) or
	 ((ie_atend_pac_w	= 'PAC') and
	  (cd_pessoa_fisica_w	is not null) and	
	  (a.cd_pessoa_fisica 	= cd_pessoa_fisica_w)))
and	b.cd_material		in (select	distinct x.cd_material
					from	Material x
					where	ie_origem_inf_p = 'PA'
					and 	x.nr_seq_ficha_tecnica = cd_material_p
					Union All
					Select 	cd_material_p
					from 	dual
					where 	ie_origem_inf_p <> 'PA' )
and	b.dt_suspensao is null
and	b.dt_fim_horario is null
and	nvl(b.ie_horario_especial,'N') = 'N'
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';

select 	nvl(sum(qt_total_dose),0)qt_total
into 	qt_dose_total_medic_p
from	(	select	trunc(sum(obter_dose_convertida(a.cd_material, a.qt_dose, nvl(a.cd_unidade_medida_dose,a.cd_unidade_medida),'mg')),2) qt_total_dose
		from	prescr_material a,
			prescr_medica	b
		where	a.nr_prescricao = b.nr_prescricao
		and	a.cd_material	= cd_material_p
		and 	a.ie_agrupador  = 1
		and	b.cd_pessoa_fisica = cd_paciente_p
		union all
		select 	trunc(sum(obter_dose_convertida(a.cd_material, a.qt_dose_especial, nvl(a.cd_unidade_medida_dose,a.cd_unidade_medida),'mg')),2) qt_total_dose
		from	prescr_material a,
			prescr_medica	b
		where	a.nr_prescricao = b.nr_prescricao
		and	a.cd_material	= cd_material_p
		and 	a.ie_agrupador  = 1
		and	b.cd_pessoa_fisica = cd_paciente_p);

end gerar_resumo_vime;
/