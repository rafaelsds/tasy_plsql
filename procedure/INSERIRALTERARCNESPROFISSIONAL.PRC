create or replace 
procedure 	inserirAlterarCnesProfissional(	ie_alterar_p varchar2,
						cd_pessoa_fisica_p varchar2,
						ie_sus_p	varchar2,
						nm_usuario_p varchar2,
						cpf_prof_p varchar2,
						pispasep_p varchar2,
						nome_prof_p varchar2,
						nome_mae_p varchar2) is


begin

if	(ie_alterar_p = 'S') then
	insert into cnes_profissional(	CD_PESSOA_FISICA,
					DT_ATUALIZACAO,
					IE_SUS,
					NM_USUARIO,
					NR_SEQUENCIA
					)
				values(	cd_pessoa_fisica_p,
					sysdate,
					ie_sus_p,
					nm_usuario_p,
					cnes_profissional_seq.nextval);
end if;					

commit;

end inserirAlterarCnesProfissional;
/