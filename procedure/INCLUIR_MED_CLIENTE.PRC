create or replace 
procedure Incluir_Med_Cliente
		(cd_pessoa_fisica_p	varchar2,
		cd_medico_p		varchar2,
		nm_usuario_p		varchar2) is


nr_sequencia_w		number(10,0) := 0;

begin

if	(length(cd_medico_p) > 0) then
	begin

	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	med_cliente
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	cd_medico		= cd_medico_p;

	if	(nr_sequencia_w	= 0) then
		begin
	
		select	med_cliente_seq.nextval
		into	nr_sequencia_w
		from	dual;


		insert	into med_cliente
			(nr_sequencia,
			cd_medico,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			dt_primeira_consulta,
			dt_ultima_consulta,
			dt_ultima_atualiz,
			cd_pessoa_sist_orig,
			cd_convenio,
			cd_usuario_convenio,
			ds_encaminhamento,
			dt_validade_carteira,
			ie_ficha_papel,
			ie_exame_consultorio,
			dt_ultima_visualiz,
			ie_exame_virtual,
			nr_seq_plano,
			ds_observacao,
			ie_gemelar,
			ie_pais_separados)
		values	(nr_sequencia_w,
			cd_medico_p,
			cd_pessoa_fisica_p,
			sysdate,
			nm_usuario_p,
			'A', null, null, null,
			null, null, null, null,
			null, 'N', 'N', null, 'N',
			null, null, 'N', 'N');

		end;
	end if;
	end;
end if;
	
commit;

end Incluir_Med_Cliente;
/