create or replace
procedure pls_consistir_se_data_valida
			(	ds_data_p	varchar2,
				ds_mascara_p	varchar2) is 

dt_valida_w	date;

begin

begin
select	to_date(ds_data_p,ds_mascara_p)
into	dt_valida_w
from	dual;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(308244, 'DT_REFERENCIA='||ds_data_p);
end;

end pls_consistir_se_data_valida;
/