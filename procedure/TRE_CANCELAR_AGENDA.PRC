create or replace
procedure tre_cancelar_agenda(	nr_sequencia_p		number,
				nr_seq_motivo_cancel_p	number,
				DS_MOTIVO_CANCEL_P	varchar2,
				nm_usuario_p		Varchar2) is 

begin

update	tre_agenda
set	dt_cancelamento		= sysdate,
	nr_seq_motivo_cancel	= nr_seq_motivo_cancel_p,
	ds_motivo_cancelamento  = ds_motivo_cancel_p,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p;

tre_ret_status_pac_lista_esp(nr_sequencia_p, null);

commit;

end tre_cancelar_agenda;
/
