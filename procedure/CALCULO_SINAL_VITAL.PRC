create or replace PROCEDURE CALCULO_SINAL_VITAL(    
    nm_usuario_p       VARCHAR2,
    nr_atendimento_p   NUMBER DEFAULT NULL)
IS

  qt_idade_w             NUMBER(10);
  cd_setor_atendimento_w NUMBER(10) := NULL;
  qt_idade_dias_w        NUMBER(6);
  qt_peso_gramas_w       NUMBER(12,3);
  ie_retorno_w           VARCHAR2(1) := 'N';
  ds_titulo_w            VARCHAR2(55);
  ds_alerta_w            VARCHAR2(255);
  ie_alerta_w            VARCHAR2(3);
  cd_pessoa_fisica_w     NUMBER;
  
  CURSOR C01
  IS
    SELECT DISTINCT c.DS_TITULO,
      c.DS_ALERTA,
      c.IE_ADVISORY_SEVERITY
    FROM GQA_PENDENCIA_PAC a,
      GQA_PENDENCIA_REGRA b,
      CALCULOS_ENGINE_ADVISORY c
    WHERE a.NR_SEQ_PEND_REGRA = b.NR_SEQUENCIA
    AND a.nr_atendimento      = nr_atendimento_p
    AND c.nr_seq_regra        = b.nr_sequencia;
    
BEGIN

  SELECT	substr(obter_pessoa_atendimento(nr_atendimento_p,'C'),1,10)
		INTO	cd_pessoa_fisica_w
  FROM	dual;

  IF ( nr_atendimento_p IS NOT NULL AND cd_pessoa_fisica_w IS NOT NULL) THEN
  
    IF(nr_atendimento_p      IS NOT NULL) THEN
      cd_setor_atendimento_w := obter_setor_atendimento(nr_atendimento_p);
    END IF;
    
    qt_idade_w       := obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A');
    qt_idade_dias_w  := obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'DIA');
    qt_peso_gramas_w := (NVL(obter_peso_pf(cd_pessoa_fisica_w),0) * 1000);
    OPEN C01;
    
    LOOP
      FETCH C01 INTO 
        ds_titulo_w, 
        ds_alerta_w, 
        ie_alerta_w;
      EXIT
    WHEN C01%notfound;
    
      BEGIN
      
        INSERT
        INTO CALCULOS_ADVISORY
          (
            nr_sequencia,
            cd_pessoa_fisica,
            nr_atendimento,
            ie_situacao,
            nm_usuario_nrec,
            ds_titulo,
            ds_alerta,
            ie_advisory_severity,
            dt_atualizacao_nrec
          )
          VALUES
          (
            calculos_advisory_seq.nextval,
            cd_pessoa_fisica_w,
            nr_atendimento_p,
            'A',
            nm_usuario_p,
            ds_titulo_w,
            ds_alerta_w,
            ie_alerta_w,
            sysdate
          );
          
        COMMIT;        
      END;
    END LOOP;
    CLOSE C01;
  END IF;
END CALCULO_SINAL_VITAL;
/
