CREATE OR REPLACE
PROCEDURE Atualizar_Seq_Agenda_Propaci  (nr_sequencia_p       number,
					 nm_usuario_p	      varchar2,
					 cd_estabelecimento_p number) is

nr_atendimento_w	number(10);
cd_procedimento_w	number(15);
dt_procedimento_w	date;
nr_cirurgia_w		number(10);
CD_PESSOA_FISICA_w	varchar2(10);
nr_Seq_agenda_w		number(10);
nr_prescricao_w		number(14);
cd_estabelecimento_w	number(10);
nm_usuario_w		varchar2(15);
ie_atualiza_seq_agen_w	varchar2(1);
cd_tipo_agenda_w	number(10);	

BEGIN

Obter_Param_Usuario(900,373,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_atualiza_seq_agen_w);

select	nr_atendimento,
	cd_procedimento,
	dt_procedimento,
	nr_cirurgia,
	nr_prescricao,
	nvl(NM_USUARIO_ORIGINAL,NM_USUARIO)
into	nr_atendimento_w,
	cd_procedimento_w,
	dt_procedimento_w,
	nr_cirurgia_w,
	nr_prescricao_w,
	nm_usuario_w
from procedimento_paciente
where nr_sequencia = nr_sequencia_p;

SELECT 	CD_PESSOA_FISICA,
	cd_estabelecimento
into	CD_PESSOA_FISICA_w,
	cd_estabelecimento_w
FROM ATENDIMENTO_PACIENTE
WHERE NR_ATENDIMENTO = nr_atendimento_w;


select 	nvl(max(nr_sequencia),0)
into	nr_Seq_agenda_w
from	agenda_paciente
where 	nr_cirurgia = nr_cirurgia_w;



IF	(nr_Seq_agenda_w > 0) THEN

	select  max(a.cd_tipo_agenda)
	into	cd_tipo_agenda_w
	from    agenda a,
		agenda_paciente b
	where   a.cd_agenda = b.cd_agenda
	and     b.nr_sequencia = nr_seq_agenda_w;
	
	
	if (cd_tipo_agenda_w = 1) then
		executar_evento_agenda('EPP', 'CI', nr_Seq_agenda_w, cd_estabelecimento_w, nm_usuario_w);
	else
		executar_evento_agenda('EPP', 'E', nr_Seq_agenda_w, cd_estabelecimento_w, nm_usuario_w);
	end if;

END IF;

begin
if 	(ie_atualiza_seq_agen_w = 'S') then
	UPDATE AGENDA_PACIENTE
	SET IE_STATUS_AGENDA = 'E'
	WHERE CD_PROCEDIMENTO = cd_procedimento_w
	and ie_status_agenda not in ('C','L','B','F','I') 
	AND CD_PESSOA_FISICA = CD_PESSOA_FISICA_w
	and nr_sequencia = nr_Seq_agenda_w;
else
	UPDATE AGENDA_PACIENTE
	SET IE_STATUS_AGENDA = 'E'
	WHERE CD_PROCEDIMENTO = cd_procedimento_w
	AND ((DT_AGENDA < dt_procedimento_w) or (ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(DT_AGENDA) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_procedimento_w)))  
	and ie_status_agenda not in ('C','L','B','F','I') 
	AND CD_PESSOA_FISICA = CD_PESSOA_FISICA_w;
end if;
exception
when others then
	Wheb_mensagem_pck.exibir_mensagem_abort(191165);
end;


commit;

END Atualizar_Seq_Agenda_Propaci;
/