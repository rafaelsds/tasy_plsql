create or replace
PROCEDURE via_cancelar_reserva(nr_seq_reserva_p		VARCHAR2,
			       ds_motivo_cancel_p	VARCHAR2,
			       ie_respo_custo_p		VARCHAR2,
			       nr_seq_tipo_motivo_p	number,
			       nm_usuario_p		VARCHAR2) IS
			       
nr_seq_reserva_trans_w	number(10);
cursor c01 is
select   b.nr_sequencia
from     via_transporte a,
         via_reserva_transporte b
where  a.nr_sequencia = b.nr_seq_transporte
and    b.nr_seq_reserva = nr_seq_reserva_p;
	
			       

BEGIN

UPDATE 	via_reserva
SET	ie_cancelamento = 'C',
	nm_usuario = nm_usuario_p,
	dt_atualizacao = SYSDATE
WHERE	nr_sequencia = nr_seq_reserva_p;


open C01;
loop
fetch C01 into	
	nr_seq_reserva_trans_w;
exit when C01%notfound;
	begin
	INSERT INTO via_reserva_cancelada (nr_sequencia,
					   dt_atualizacao,
					   nm_usuario,
					   dt_atualizacao_nrec,
					   nm_usuario_nrec,
					   nr_seq_reserva,
					   ds_motivo_cancel,
					   nm_usuario_cancel,
					   ie_responsavel_custo,
					   nr_seq_tipo_motivo,
					   nr_seq_reserva_trans)
	VALUES				  (via_reserva_cancelada_seq.NEXTVAL,
					   SYSDATE,
					   nm_usuario_p,
					   SYSDATE,
					   nm_usuario_p,
					   nr_seq_reserva_p,
					   ds_motivo_cancel_p,
					   nm_usuario_p,
					   ie_respo_custo_p,
					   nr_seq_tipo_motivo_p,
					   nr_seq_reserva_trans_w);	
	end;
end loop;
close C01;

COMMIT;

END via_cancelar_reserva;
/