Create or Replace
procedure Gerar_prescr_hor(	nr_prescricao_p     	NUMBER,
				cd_perfil_p		number,
				nm_ususario_p		VARCHAR2) IS


ds_horarios_w		varchar2(2000);
ds_horarios_padr_w	varchar2(2000);
ds_hora_w		varchar2(2000);
erro_w			varchar2(2000);
dt_liberacao_w		Date;
dt_horario_w		Date;	
k			Integer;
y			Integer;
nr_sequencia_w		Number(10);
nr_seq_prescr_w		Number(6);
qt_dose_w		Number(18,6);
qt_total_dispensar_w	Number(18,6);
cd_material_w		Number(6);
dt_primeiro_horario_w	Date;
ie_agrupador_w		Number(2);
cd_intervalo_w		varchar2(7);
ie_prescricao_dieta_w	varchar2(1);
nr_ocorrencia_w		number(15,4);
ie_esquema_alternado_w	varchar2(1);
nr_seq_solucao_w	number(6);
ie_urgencia_w		varchar2(1);

ie_agrupador_dil_w	Number(2);
nr_sequencia_dil_w	Number(10);
qt_dose_dil_w		Number(18,6);
qt_total_disp_dil_w	Number(18,6);
nr_ocorrencia_dil_w	number(15,4);
cd_material_dil_w	Number(6);
hr_prim_horario_w	varchar2(5);
ie_urgente_w		varchar2(1);
qt_conversao_w		Number(18,6);
qt_dose_especial_w	Number(18,6);
hr_dose_especial_w	varchar2(5);

/* Rafael em 12/09/06 Funcionalidades da Administracao da Prescricao (ATEPAC_PG) */
nr_seq_procedimento_w	number(6,0);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
nr_ocor_proc_w		number(15,4);
ie_proc_urgente_w	varchar2(1);
dt_prev_execucao_w	date;
ds_hora_proc_w		varchar2(2000);
dt_horario_proc_w	date;
ds_horarios_proc_w	varchar2(2000);
ds_horarios_padrao_proc_w	varchar2(2000);
cd_material_exame_w	varchar2(20);

nr_seq_recomendacao_w		number(6,0);
cd_recomendacao_w		number(10,0);
ds_hora_rec_w			varchar(2000);
dt_horario_rec_w		date;
nr_seq_classif_rec_w		number(10,0);
ds_horarios_rec_w		varchar2(2000);
ds_horarios_padrao_rec_w	varchar2(2000);
/* Fim Rafael em 12/09/06 */

/* Rafael em 07/10/2006 Funcionalidades da Administracao da Prescricao -> Identificar horarios especiais (SN, ACM e sem Horario) */
ie_se_necessario_w		varchar2(1);
ie_acm_w			varchar2(1);
ie_horario_especial_w		varchar2(1) := 'N';
qt_dieta_w			number(10,0);
cd_setor_atendimento_w		number(5);
cd_estabelecimento_w		number(4);
dt_prescricao_w			Date;
dt_prescricao_ww		Date;
qT_dia_adic_w			Number(10) := 0;
qt_registro_w			Number(10);
dt_inicio_prescr_w		Date;

nr_seq_procedimento_novo_w	number(6);
nr_seq_exame_w			number(10);
ie_status_atend_w		number(2);
ie_status_execucao_w		varchar2(3);
cd_setor_atendimento_proc_w	number(5);
cd_setor_coleta_w		number(5);
cd_setor_entrega_w        	number(5);
cd_setor_exec_fim_w       	number(5);
cd_setor_exec_inic_w      	number(5);
nr_seq_lab_w			varchar2(20);
ie_gerar_proc_intervalo_w	varchar2(1);
ie_suspenso_w			varchar2(1);
ds_observacao_w			varchar2(2000);
ds_dado_clinico_w		varchar2(2000);
ds_material_especial_w		varchar2(255);
ie_controlado_w			varchar2(1);

nr_seq_prescr_hor_w		number(6);
dt_horario_proc_prev_w		date;
ie_proc_atual_w			varchar2(1);
qt_min_agora_w			number(15);
qt_min_especial_w		number(15);
ie_classif_urgente_w		varchar2(3);
dt_limite_agora_w		date;
dt_limite_especial_w		date;
nr_seq_classif_w		number(10);
dt_liberacao_farmacia_w		date;
qt_dia_prim_hor_w		number(15);

/* Rafael em 12/09/06 - Funcionalidades da Administracao da Prescricao */
cursor c14 is
select	b.nr_sequencia,
	b.cd_recomendacao,
	b.nr_seq_classif,
	b.ds_horarios,
	--padroniza_horario(b.ds_horarios)
	padroniza_horario_prescr(b.ds_horarios, decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,b.hr_prim_horario),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss'))),
	nvl(b.ie_urgencia,'N'),
	nvl(b.ie_se_necessario,'N'),
	nvl(qt_dia_prim_hor,0)
from	prescr_recomendacao b,
	prescr_medica a
where	a.nr_prescricao	= nr_prescricao_p
and	a.nr_prescricao	= b.nr_prescricao
and	a.dt_suspensao is null
and	nvl(b.ie_suspenso,'N') <> 'S';

begin

select	nvl(dt_liberacao,dt_liberacao_medico),
	dt_primeiro_horario,
	cd_setor_atendimento,
	nvl(cd_estabelecimento,1),
	PKG_DATE_UTILS.start_of(dt_inicio_prescr, 'mi', 0),
	dt_inicio_prescr,
	dt_liberacao_farmacia
into	dt_liberacao_w,
	dt_primeiro_horario_w,
	cd_setor_atendimento_w,
	cd_estabelecimento_w,
	dt_prescricao_w,
	dt_inicio_prescr_w,
	dt_liberacao_farmacia_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

begin
obter_Param_Usuario(924, 192, cd_perfil_p, nm_ususario_p, cd_estabelecimento_w, ie_gerar_proc_intervalo_w);

qt_dia_adic_w	:= 0;

open c14;
loop
fetch c14 into	
	nr_seq_recomendacao_w,
	cd_recomendacao_w,
	nr_seq_classif_rec_w,
	ds_horarios_rec_w,
	ds_horarios_padrao_rec_w,
	ie_urgencia_w,
	ie_se_necessario_w,
	qt_dia_prim_hor_w;
exit when c14%notfound;
	begin
	
	qt_dia_adic_w	:= 0;	

	if	(ds_horarios_padrao_rec_w is not null) then
		begin
		while	ds_horarios_padrao_rec_w is not null loop
			begin
			select	instr(ds_horarios_padrao_rec_w, ' ') 
			into	k
			from	dual;

			if	(k > 1) and
				(substr(ds_horarios_padrao_rec_w, 1, k -1) is not null) then
				ds_hora_rec_w			:= substr(ds_horarios_padrao_rec_w, 1, k-1);
				ds_hora_rec_w			:= replace(ds_hora_rec_w, ' ','');
				ds_horarios_padrao_rec_w	:= substr(ds_horarios_padrao_rec_w, k + 1, 2000);
			elsif	(ds_horarios_padrao_rec_w is not null) then
				ds_hora_rec_w			:= replace(ds_horarios_padrao_rec_w,' ','');
				ds_horarios_padrao_rec_w	:= '';
			end if;
			
			qt_dia_adic_w := qt_dia_prim_hor_w;			
			if	(instr(ds_hora_rec_w,'A') > 0) and
				(qt_dia_adic_w = 0) and
				(ie_urgencia_w = 'N') and
				(qt_dia_prim_hor_w = 0) then
				qt_dia_adic_w	:= 1;
			elsif	(instr(ds_hora_rec_w,'AA') > 0) and
				(ie_urgencia_w = 'N') and
				(qt_dia_prim_hor_w = 0) then
				qt_dia_adic_w	:= qt_dia_adic_w + 1;
			end if;

			ds_hora_rec_w	:= replace(ds_hora_rec_w,'A','');
			ds_hora_rec_w	:= replace(ds_hora_rec_w,'A','');
			
			dt_horario_rec_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_inicio_prescr_w + qt_dia_adic_w, replace(ds_hora_rec_w,'A','')), 'mi');

			select	prescr_rec_hor_seq.nextval
			into	nr_sequencia_w
			from	dual;

			/* Rafael em 07/10/2006 Identificar horarios especiais */
			ie_horario_especial_w	:= ie_se_necessario_w;
	
			insert into prescr_rec_hor(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_recomendacao,
				cd_recomendacao,
				ds_horario,
				dt_horario,
				nr_seq_classificacao,
				ie_horario_especial,
				ie_aprazado,
				dt_lib_horario) 
			values	(
				nr_sequencia_w,
				sysdate,
				nm_ususario_p,
				sysdate,
				nm_ususario_p,
				nr_prescricao_p,
				nr_seq_recomendacao_w,
				cd_recomendacao_w,
				ds_hora_rec_w,
				dt_horario_rec_w,
				nr_seq_classif_rec_w,
				nvl(ie_horario_especial_w,'N'),
				'N',
				sysdate);
			end;
		end loop;
		end;
	else		
		ds_hora_rec_w		:= nvl(hr_prim_horario_w, substr(to_char(dt_primeiro_horario_w,'dd/mm/yyyy hh24:mi:ss'),12,5));
    dt_horario_rec_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_inicio_prescr_w + qt_dia_prim_hor_w, ds_hora_rec_w), 'mi');

		select	prescr_rec_hor_seq.nextval
		into	nr_sequencia_w
		from	dual;

		ie_horario_especial_w	:= 'S';
		
		insert into prescr_rec_hor(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_prescricao,
			nr_seq_recomendacao,
			cd_recomendacao,
			ds_horario,
			dt_horario,
			nr_seq_classificacao,
			ie_horario_especial,
			ie_aprazado,
			dt_lib_horario) 
		values(
			nr_sequencia_w,
			sysdate,
			nm_ususario_p,
			sysdate,
			nm_ususario_p,
			nr_prescricao_p,
			nr_seq_recomendacao_w,
			cd_recomendacao_w,
			ds_hora_rec_w,
			dt_horario_rec_w,
			nr_seq_classif_rec_w,
			nvl(ie_horario_especial_w,'S'),
			'N',
			sysdate);
	end if;
	end;
end loop;
close c14;


exception when others then
	erro_w	:= sqlerrm;
end;

select	count(*)
into	qt_dieta_w
from	dieta b,
	prescr_dieta a,
	prescr_medica k
where	b.ie_situacao = 'A'
and	b.ie_tipo_dieta = 'O'
and	b.cd_dieta = a.cd_dieta
and	k.nr_prescricao = nr_prescricao_p
and	k.nr_prescricao	= a.nr_prescricao
and	nvl(a.ie_suspenso,'N') <> 'S'
and	k.dt_suspensao is null
and	a.dt_suspensao is null;


if	(qt_dieta_w > 0) then
	gerar_prescr_dieta_hor(nr_prescricao_p, nm_ususario_p);
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Gerar_prescr_hor;
/
