create or replace procedure tiss_ins_ret_create_item(
			nr_seq_item_fatur_p	in number,
			ie_proc_desp_p		in varchar2,
			cd_item_p 		in varchar2, 
			dt_item_p 		in date, 
			vl_item_p 		in number,
			qt_item_p		in number,
			qt_item_orig_p		in number,
			cd_setor_p		in setor_atendimento.cd_setor_atendimento%type,
			vl_payment_p		in number,
			ie_funcao_medico_p	in varchar2
		) is

begin
	tiss_ins_ret_denial_pck.generate(
		nr_seq_item_fatur_p,
		ie_proc_desp_p,
		cd_item_p, 
		dt_item_p, 
		vl_item_p,
		qt_item_p, 
		qt_item_orig_p,
		cd_setor_p, 
		vl_payment_p, 
		ie_funcao_medico_p);
end tiss_ins_ret_create_item;
/
