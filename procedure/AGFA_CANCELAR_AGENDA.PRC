CREATE OR REPLACE procedure agfa_cancelar_agenda	(cd_integracao_externa_p	varchar2,
					dt_agenda_p			date,
					hr_inicio_p			date,
					cd_pessoa_fisica_p		varchar2,
					nr_seq_proc_interno_p	number,
					ie_status_p			varchar2,
					nm_usuario_p			varchar2,
					cd_exame_externo_p		varchar2,
					ds_erro_p out			varchar2,
					cd_estabelecimento_p		number) is

ds_erro_w		varchar2(255) := null;
cd_agenda_w		number(10,0);
nr_seq_agenda_w		number(10,0);
hr_cancel_w		date;
nr_seq_estagio_w	number(10);
nr_sequencia_autor_w	number(10);
hr_inicio_w		date;
ie_agenda_integrada_agfa_w	varchar2(10);
cd_procedimento_w	number(15);
ds_procedimento_w	varchar2(255);
nr_seq_proc_autor_w	number(10);
qt_registro_w		number(10);	

Cursor C01 is
	select	cd_agenda,
		nr_sequencia
	into	cd_agenda_w,
		nr_seq_agenda_w
	from	agenda_paciente
	where	cd_integracao_externa	= cd_integracao_externa_p
	and	cd_exame_externo = cd_exame_externo_p
	and	ie_status_agenda <> 'C';
	
Cursor C02 is
	select	a.nr_sequencia
	from	estagio_autorizacao b,
		autorizacao_convenio a
	where	a.nr_seq_agenda		= nr_seq_agenda_w
	and	a.nr_seq_estagio	= b.nr_sequencia;

Cursor c03 is
select	a.nr_sequencia
from	procedimento_autorizado a
where	nr_seq_agenda		= nr_seq_agenda_w;	

begin

select	nvl(max(IE_AGENDA_INTEGRADA_AGFA), 'N')
into	ie_agenda_integrada_agfa_w
from 	parametro_atendimento
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(cd_integracao_externa_p is not null) and
	(dt_agenda_p is not null) and
	(hr_inicio_p is not null) and
	(cd_pessoa_fisica_p is not null) and
	(nr_seq_proc_interno_p is not null) and
	(ie_status_p is not null) and
	(nm_usuario_p is not null) then

	/* controlar excess�es */
	begin
	
	/* obter dados agendamento */
	select	nvl(max(cd_agenda),0),
		nvl(max(nr_sequencia),0)
	into	cd_agenda_w,
		nr_seq_agenda_w
	from	agenda_paciente
	where	cd_integracao_externa	= cd_integracao_externa_p
	and	cd_exame_externo = cd_exame_externo_p
	and	ie_status_agenda <> 'C';
	--and	trunc(dt_agenda,'dd')	= trunc(dt_agenda_p,'dd')
	--and	trunc(hr_inicio,'mi')	= hr_inicio_p
	--and	cd_pessoa_fisica		= cd_pessoa_fisica_p
	--and	nr_seq_proc_interno		= nr_seq_proc_interno_p;

	/* alterar status agendamento */
	--alterar_status_agenda(2, cd_agenda_w, nr_seq_agenda_w, ie_status_p, null, null, 'N', nm_usuario_p);
	
	open C01;
	loop
	fetch C01 into	
		cd_agenda_w,
		nr_seq_agenda_w;
	exit when C01%notfound;
		begin
		
		if	(nvl(nr_seq_agenda_w,0) > 0) then
		
			select	max(hr_inicio)
			into	hr_inicio_w
			from	agenda_paciente
			where	nr_sequencia = nr_seq_agenda_w;

			select	max(hr_inicio)
			into	hr_cancel_w
			from	agenda_paciente
			where	cd_agenda = cd_agenda_w
			and	to_date(to_char(hr_inicio,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss') = to_date(to_char(hr_inicio_w,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss')
			and	ie_status_agenda = 'C';

			if	(hr_cancel_w is not null) then
				update	agenda_paciente
				set	ie_status_agenda = 'C',
					nm_usuario = nm_usuario_p,
					hr_inicio = hr_cancel_w + (1 / 86400)
				where	nr_sequencia = nr_seq_agenda_w;
			else
				update	agenda_paciente
				set	ie_status_agenda = 'C',
					nm_usuario = nm_usuario_p
					where	nr_sequencia = nr_seq_agenda_w;
			end if;
			
			select	max(nr_sequencia)
			into	nr_seq_estagio_w
			from	estagio_autorizacao
			where	cd_empresa	= 1
			and	ie_interno	= '70';

			if	(ie_agenda_integrada_agfa_w = 'N') then

				if	(nr_seq_estagio_w is not null) then
					open c02;
					loop
					fetch c02 into
						nr_sequencia_autor_w;
					exit when c02%notfound;
						atualizar_autorizacao_convenio(nr_sequencia_autor_w,nm_usuario_p,nr_seq_estagio_w,null,null,'N');
					end loop;
					close c02;
				end if;
			else
				
				/*update	agenda_integrada_item
				set	nr_seq_agenda_exame = null
				where	nr_seq_agenda_exame	= nr_seq_agenda_w;*/

				open c03;
				loop
				fetch c03 into
					nr_seq_proc_autor_w;
				exit when c03%notfound;

					if	(nr_seq_proc_autor_w is not null) then

						select	cd_procedimento,
							obter_descricao_procedimento(cd_procedimento,ie_origem_proced),
							nr_sequencia_autor
						into	cd_procedimento_w,
							ds_procedimento_w,
							nr_sequencia_autor_w
						from	procedimento_autorizado
						where	nr_sequencia	= nr_seq_proc_autor_w;
	
						delete 	from procedimento_autorizado
						where	nr_sequencia = nr_seq_proc_autor_w;

						select	count(*)
						into	qt_registro_w
						from	procedimento_autorizado
						where	nr_sequencia_autor	= nr_sequencia_autor_w;

						insert	into	autorizacao_convenio_hist
							(nr_sequencia,
							nm_usuario,
							dt_atualizacao,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							nr_sequencia_autor,
							ds_historico)
						values	(autorizacao_convenio_hist_seq.nextval,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							nr_sequencia_autor_w,
							'Procedimento ' || cd_procedimento_w || ' - ' || ds_procedimento_w || ' exclu�do da autoriza��o pela integra��o AGFA');

						if	(qt_registro_w = 0) then
							atualizar_autorizacao_convenio(nr_sequencia_autor_w,nm_usuario_p,nr_seq_estagio_w,null,null,'N');
						end if;
					end if;
				end loop;
				close c03;	
			end if;	

		end if;		
		
		end;
	end loop;
	close C01;
	
	exception
	when others then
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(277292,null) || sqlerrm;
	end;

end if;

ds_erro_p := ds_erro_w;

end agfa_cancelar_agenda;
/