create or replace
procedure gerar_auxilio_analista(
			nr_seq_ordem_p		number,
			ie_tipo_auxilio_p 	varchar2,
			nm_usuario_analista_p   varchar2,
			nm_usuario_p		varchar2,
			ds_auxilio_p		varchar2) is 

nr_sequencia_w number(10);

begin
if (nr_seq_ordem_p > 0)then
   begin   
   select  	man_solicitacao_auxilio_seq.nextval
   into		nr_sequencia_w
   from 	dual;

   insert into man_solicitacao_auxilio
		(
		nr_sequencia,
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		nr_seq_ordem_servico, 
		nm_usuario_analista, 
		ie_tipo_auxilio, 
		ie_status,
		ds_detalhamento)
   values  (
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_ordem_p,
		nm_usuario_analista_p,
		ie_tipo_auxilio_p,
		'P',
		ds_auxilio_p);
   end;
end if;   
   
commit;

end gerar_auxilio_analista;
/