create or replace
procedure diops_cad_gerar_acionquotista
			(	nr_seq_operadora_p	number,
				nr_seq_periodo_p	number,
				nm_usuario_p		varchar2) is 
			
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar as informações cadastrais do acionista
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionário [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
-------------------------------------------------------------------------------------------------------------------
Referências:
	PLS_GERAR_DIOPS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nm_acionista_w			varchar2(80);
cd_cgc_w			varchar2(14);
nr_cpf_w			varchar2(11);
cd_pessoa_fisica_w		varchar2(10);
ie_tipo_pessoa_w		varchar2(2);
ie_pulverizado_w		varchar2(1);
nr_seq_acionquotista_w		number(10);
nr_seq_cad_acion_w		number(10);
qt_total_acoes_w		number(10);
qt_acoes_quotas_w		number(10)	:= 0;

Cursor C01 is
	select	'PF' ie_tipo_pessoa,
		cd_pessoa_fisica,
		substr(obter_nome_pf(cd_pessoa_fisica),1,60) nm_acionista,
		substr(obter_dados_pf(cd_pessoa_fisica, 'CPF'),1,11) nr_cpf,
		'' cd_cgc,
		qt_acoes_quotas
	from	pls_acionista_pf
	where	nr_seq_acio_quotista	= nr_seq_acionquotista_w
	union
	select	'PJ' ie_tipo_pessoa,
		'' cd_pessoa_fisica,
		substr(obter_razao_social(cd_cgc),1,80) nm_acionista,
		'' nr_cpf,
		cd_cgc,
		qt_acoes_quotas
	from	pls_acionista_pj
	where	nr_seq_acio_quotista	= nr_seq_acionquotista_w;

begin
/* Gravar os dados do Acionista/Quotista */
begin
select	nr_sequencia,
	ie_pulverizado,
	qt_total_acoes
into	nr_seq_acionquotista_w,
	ie_pulverizado_w,
	qt_total_acoes_w
from	pls_acionista_quotista	
where	nr_seq_operadora	= nr_seq_operadora_p;
exception
when others then
	nr_seq_acionquotista_w	:= 0;
end;

if	(nr_seq_acionquotista_w	> 0) then
	select	w_diops_cad_acionquotista_seq.nextval
	into	nr_seq_cad_acion_w
	from	dual;

	insert into w_diops_cad_acionquotista
		(nr_sequencia,
		nr_seq_operadora,
		ie_pulverizado,
		qt_total_acoes,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_periodo)
	values	(nr_seq_cad_acion_w,
		nr_seq_operadora_p,
		ie_pulverizado_w,
		qt_total_acoes_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_periodo_p);

	open C01;
	loop
	fetch C01 into	
		ie_tipo_pessoa_w,
		cd_pessoa_fisica_w,
		nm_acionista_w,
		nr_cpf_w,
		cd_cgc_w,
		qt_acoes_quotas_w;
	exit when C01%notfound;
		begin
		insert into w_diops_cad_acion_pf_pj
			(nr_sequencia,
			nr_seq_acionquotista,
			cd_pessoa_fisica,
			nr_cpf,
			cd_cgc,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_acionistas,
			qt_acoes_quotas,
			nr_seq_operadora,
			nr_seq_periodo)
		values	(w_diops_cad_acion_pf_pj_seq.nextval,
			nr_seq_cad_acion_w,
			cd_pessoa_fisica_w,
			nr_cpf_w,
			cd_cgc_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nm_acionista_w,
			qt_acoes_quotas_w,
			nr_seq_operadora_p,
			nr_seq_periodo_p);
		end;
	end loop;
	close C01;
end if;

end diops_cad_gerar_acionquotista;
/