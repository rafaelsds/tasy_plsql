create or replace
procedure inserir_solicitacao_aghos_ws(
			nm_usuario_p			Varchar2,
			dat_integracao_p		date,
			idf_internacao_p		number,
			idf_prioridade_p		number,
			des_erro_p				varchar2,
			des_nome_paciente_p		varchar2) is 

begin
	insert into solicitacao_tasy_aghos ( 
		nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec,   
		nr_internacao,     
		ie_urgencia,    
		nr_seq_laudo, 
		nr_atend_original, 
		ie_situacao , 
		ie_motivo_rejeicao,  
		nr_atendimento, 
		ie_erro,          
		ds_erro, 
		ds_motivo_situacao, 
		cd_cgc, 
		nm_paciente) 
	values	(solicitacao_tasy_aghos_seq.nextval, 
		sysdate, 
		'Aghos', 
		nvl(dat_integracao_p, sysdate), 
		'Aghos', 
		idf_internacao_p, 
		idf_prioridade_p, 
		null, 
		null, 
		'A', 
		null, 
		null, 
		decode(des_erro_p, null, 'N', 'S'), 
		des_erro_p, 
		'Solicitação de outra instituição', 
		null, 
		des_nome_paciente_p); 
commit; 
end inserir_solicitacao_aghos_ws;
/