create or replace
procedure delete_cobranca_escrit_log(nr_seq_cobranca_p 	number) is 

begin
	if	(nvl(nr_seq_cobranca_p,0) > 0) then
		begin
		delete 
		from	cobranca_escrit_log
		where	nr_seq_cobranca = nr_seq_cobranca_p;
		commit;
		end;
	end if;

end delete_cobranca_escrit_log;
/