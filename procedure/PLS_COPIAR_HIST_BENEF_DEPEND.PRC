create or replace
procedure pls_copiar_hist_benef_depend(	nr_seq_historico_p	pls_segurado_historico.nr_sequencia%type,
					nr_seq_titular_p	pls_segurado.nr_sequencia%type,	
					nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Copiar o hist�rico do titular para os dependentes
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

Cursor C01 is
	select	nr_sequencia nr_seq_segurado
	from	pls_segurado
	where	nr_seq_titular	= nr_seq_titular_p;

begin

for C01_w in C01 loop
	begin
	insert into pls_segurado_historico ( 	nr_sequencia, dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_segurado, 
						dt_historico, ds_historico, ds_observacao, 
						ie_tipo_historico, ie_historico_situacao, nm_usuario_historico, 
						nr_seq_tipo_historico, ds_titulo, dt_liberacao_hist, 
						nm_usuario_liberacao, ie_envio_sib, ie_tipo_segurado, 
						ie_tipo_segurado_ant, ie_situacao_compartilhamento)
					select	pls_segurado_historico_seq.nextval, sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, C01_w.nr_seq_segurado, 
						dt_historico, ds_historico, ds_observacao, 
						ie_tipo_historico, ie_historico_situacao, nm_usuario_historico, 
						nr_seq_tipo_historico, ds_titulo, sysdate, 
						nm_usuario_liberacao, ie_envio_sib, ie_tipo_segurado, 
						ie_tipo_segurado_ant, ie_situacao_compartilhamento
					from	pls_segurado_historico
					where	nr_sequencia	= nr_seq_historico_p;		
	end;
end loop;

commit;

end pls_copiar_hist_benef_depend;
/
