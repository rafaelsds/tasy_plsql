create or replace
procedure estornar_liberacao_contrato(	nr_sequencia_p number,
					nm_usuario_p   Varchar2) is 

begin

update	contrato
set	dt_liberacao		= null,
	dt_aprovacao		= null,
	nr_seq_aprovacao	= null,
	dt_desdobr_aprov	= null,
	dt_reprovacao		= null,
	ie_motivo_reprovacao	= null,
	ds_justificativa_reprov	= null,
	nm_usuario_aprov	= null,	
	nm_usuario_lib		= null
where	nr_sequencia		= nr_sequencia_p;

commit;

end estornar_liberacao_contrato;
/
