create or replace procedure gerar_d301_segmento_zlg(nr_seq_dataset_p in number,
     			                                       nm_usuario_p		 in varchar2) is 


  ie_dataset_w       d301_dataset_envio.ie_dataset%type;

  nr_atendimento_w   d301_dataset_envio.nr_atendimento%type;  
  
  nr_interno_conta_w d301_dataset_envio.nr_interno_conta%type;


begin

  select 

    d3.ie_dataset,

    d3.nr_atendimento,
    
    d3.nr_interno_conta

  into ie_dataset_w,

       nr_atendimento_w,
       
       nr_interno_conta_w 

  from d301_dataset_envio d3

  where d3.nr_sequencia = nr_seq_dataset_p;



  if ie_dataset_w = 'AMBO' then

    gerar_d301_seg_zlg_ambo(nr_interno_conta_w,nr_seq_dataset_p,nm_usuario_p); 

  elsif ie_dataset_w = 'RECH' then

    gerar_d301_seg_zlg_rech(nr_interno_conta_w,nr_seq_dataset_p,nm_usuario_p);

  end if;

  

  

end gerar_d301_segmento_zlg;
/