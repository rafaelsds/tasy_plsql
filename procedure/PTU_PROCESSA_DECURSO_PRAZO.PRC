create or replace
procedure ptu_processa_decurso_prazo( 	nr_seq_dec_prazo_p		ptu_decurso_prazo.nr_sequencia%type,
					nm_usuario_p			usuario.nm_usuario%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nr_seq_confirmacao_p	out	ptu_confirmacao.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a valida��o do arquivo de 00700 - Decurso de Prazo do PTU 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performace
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nr_seq_execucao_w	ptu_decurso_prazo.nr_seq_guia%type;
cd_unimed_executora_w	ptu_confirmacao.cd_unimed_executora%type;

begin

cd_estabelecimento_w := cd_estabelecimento_p;

--Quando as transa��es s�o geradas pelo WebService, n�o existe estabelecimento definido, ent�o � verificado o estabeleicmento do par�metro
if	( cd_estabelecimento_w is null	) then
	cd_estabelecimento_w := ptu_obter_estab_padrao;
end if;
ptu_gestao_envio_confirmacao(nr_seq_dec_prazo_p, cd_estabelecimento_w, 'DP', nm_usuario_p);

select	cd_unimed_executora,
	nr_seq_execucao
into	cd_unimed_executora_w,
	nr_seq_execucao_w
from	ptu_decurso_prazo
where	nr_sequencia = nr_seq_dec_prazo_p;

begin
	select	nr_sequencia
	into	nr_seq_confirmacao_p
	from	ptu_confirmacao
	where	nr_seq_execucao = nr_seq_execucao_w
	and	cd_unimed_executora = cd_unimed_executora_w
	and	ie_tipo_resposta = 'DP';
exception
when others then
	nr_seq_confirmacao_p := null;
end;

end ptu_processa_decurso_prazo;
/