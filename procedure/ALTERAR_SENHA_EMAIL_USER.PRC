create or replace
procedure alterar_senha_email_user(
			nm_usuario_p		varchar2,
			ds_senha_email_p	varchar2,
			nm_usuario_atual_p	varchar2,
			cd_estabelecimento_p	varchar2,
			cd_perfil_ativo_p	number) is
      
ie_param_02	varchar2(1);

begin

update	usuario
set	ds_senha_email = ds_senha_email_p,
	dt_atualizacao = sysdate,
	nm_usuario_atual = nm_usuario_atual_p
where	nm_usuario = nm_usuario_p;

obter_param_usuario(-993, 2, cd_perfil_ativo_p, nm_usuario_p, cd_estabelecimento_p, ie_param_02);

if	(ie_param_02 = 'S') then
	update	funcao_param_usuario
	set	vl_parametro = ds_senha_email_p
	where	cd_funcao = 0
	and	nr_sequencia = 40
	and	upper(nm_usuario_param) = upper(nm_usuario_p)
	and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;
end if;
commit;

end alterar_senha_email_user;
/