create or replace
procedure liberar_com_licenca_emerg (
			nr_sequencia_p	number,
			nm_usuario_p	varchar2) is

ie_liberada_w	varchar2(1) := 'N';

begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	select	decode(count(*),0,'N','S')
	into	ie_liberada_w
	from	com_licenca_emerg
	where	nr_sequencia = nr_sequencia_p
	and	dt_liberacao is not null;
	
	if 	(ie_liberada_w = 'N') then
		begin
		update	com_licenca_emerg
		set	dt_liberacao 	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_sequencia_p;
		end;
	end if;
	end;
end if;
commit;
end liberar_com_licenca_emerg;
/