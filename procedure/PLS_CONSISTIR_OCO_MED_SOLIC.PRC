create or replace
procedure pls_consistir_oco_med_solic
			(	ie_medico_cooperado_p		Varchar2,
				cd_medico_regra_p		number,				
				nr_seq_conselho_p		number,
				nr_seq_proc_espec_p		number,	-- Procedimento X Especialidade				
				nr_seq_ocorrencia_p		number,
				cd_medico_solicitante_p		varchar2, --O da conta
				dt_referencia_p			date,
				nr_seq_conta_p			number,
				nr_seq_conta_proc_p		number,
				ie_tipo_item_p			number,
				cd_espec_medica_p		number,
				ie_gerar_ocorrencia_p	out	Varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			Varchar2	) is 
				
nr_seq_conselho_w	number(10);
nr_seq_conta_proc_w	number(10);
qt_espec_med_w		number(10);
ie_regra_cooperado_w	Varchar2(2):= 'S';
ie_regra_medico_w	Varchar2(2):= 'S';
ie_regra_conselho_w	Varchar2(2):= 'S';
ie_regra_proc_espec_w	Varchar2(2):= 'S';
ie_regra_espec_w	Varchar2(1):= 'S';
ie_cursor_w		Varchar2(1):= 'N';

Cursor C01 is
	select	nr_sequencia
	from	pls_conta_proc
	where	((nr_sequencia = nr_seq_conta_proc_p)
	or	 (nr_seq_conta = nr_seq_conta_p and nr_seq_conta_proc_p is null))
	order by 1;
				
begin

ie_gerar_ocorrencia_p := 'N';

if	(nvl(nr_seq_proc_espec_p,0) > 0) then
	
	if	(ie_tipo_item_p <> 4) then
	
		open C01;
		loop
		fetch C01 into	
			nr_seq_conta_proc_w;
		exit when C01%notfound;
			begin
					
			if	(pls_obter_se_espec_solic_oc(nr_seq_conta_proc_w, nr_seq_proc_espec_p, cd_medico_solicitante_p) = 'N') then
				/*Se for para gerar a ocorr�ncia j� gera e para o cursor. 
				   No caso de ser consist�ncia de proc n�o tem altera��o no caso de conta pouoa tempo (tendo um procedimento que entra na regra ara gerar ocorr�ncia j� gera e n�o consiste todos,*/
				/*retirado o goto pois n�o era verificado o conjunto de informa��es e verificado se o mesmo se encaichava em todos os itens  OS424163 Diogo*/
				ie_regra_proc_espec_w := 'S';
				ie_cursor_w	:=  'S';
			elsif	(ie_cursor_w = 'N') then
				ie_regra_proc_espec_w := 'N';
			end if;
			
			end;
		end loop;
		close C01;
		
	else
		/*Se for material a regra n�o � v�lida*/
		goto final;
	end if;
	
end if;

if	(nvl(ie_medico_cooperado_p,'A') <> 'A') then	
	if	(nvl(ie_medico_cooperado_p,'A') = 'C') then
		
		/*Se a regra � para cooperados e o participante n�o � cooperado a regra n�o � valida*/
		if	(pls_obter_se_cooperado_ativo(cd_medico_solicitante_p,dt_referencia_p,null) = 'N') then
			ie_regra_cooperado_w := 'N';
		end if;
	else		
		/*Se a regra � para n�o cooperados e o participante � cooperado a regra n�o � valida*/
		if	(pls_obter_se_cooperado_ativo(cd_medico_solicitante_p,dt_referencia_p,null) = 'S') then
			ie_regra_cooperado_w := 'N';
		end if;
	end if;	
end if;

if	(nvl(cd_medico_regra_p,0) > 0) then
	/*Se a regra for de um m�dico especifico e o solicitante n�o for o mesmo*/
	if	(cd_medico_regra_p <> cd_medico_solicitante_p) then
		ie_regra_medico_w := 'N';
	end if;
end if;

if	(nvl(nr_seq_conselho_p,0) > 0) then
	begin
	select	nr_seq_conselho
	into	nr_seq_conselho_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_medico_solicitante_p;
	exception
	when others then
		nr_seq_conselho_w := null;
	end;
	
	if	(nr_seq_conselho_w <> nr_seq_conselho_p) then
		ie_regra_conselho_w := 'N';
	end if;
end if;

/*Se o m�dico possui a especiidade passada por par�metro*/
if	(nvl(cd_espec_medica_p,0) > 0) then	
	
	select	count(cd_especialidade)
	into	qt_espec_med_w
	from	medico_especialidade
	where	cd_pessoa_fisica = cd_medico_solicitante_p
	and	cd_especialidade = cd_espec_medica_p
	and	rownum <= 1;	
	
	if	(qt_espec_med_w = 0) then
		ie_regra_espec_w	:= 'N';	
	end if;
	
end if;

if	(ie_regra_cooperado_w 	= 'S') and
	(ie_regra_medico_w 	= 'S') and
	(ie_regra_conselho_w 	= 'S') and
	(ie_regra_proc_espec_w	= 'S') and
	(ie_regra_espec_w	= 'S') then
	ie_gerar_ocorrencia_p := 'S';
else
	ie_gerar_ocorrencia_p := 'N';
end if;
<<final>>
if	(ie_gerar_ocorrencia_p <> 'S') then
	ie_gerar_ocorrencia_p := 'N';
end if;


end pls_consistir_oco_med_solic;
/