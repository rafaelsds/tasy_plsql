create or replace
procedure altera_proc_qt_autorizada(
			nr_sequencia_autor_p	number) is

begin

update	procedimento_autorizado
set	qt_autorizada	=	0
where	nr_sequencia_autor	=	nr_sequencia_autor_p;

commit;

end altera_proc_qt_autorizada;
/ 