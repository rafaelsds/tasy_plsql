create or replace
procedure grava_info_wtt(accnum_p		varchar2,
			paperprint_p		number,
			filmprint_p		number,
			opticmedia_p		number,			
			nm_usuario_p		varchar2,
			reportprint_p		number default null
			) is 


nr_prescricao_w		number(10);
nr_seq_prescr_w		number(10);
accnum_w		varchar2(15);
qt_registro_wtt		number(10);
max_registro_wtt	number(10);
			
begin


if	(accnum_p is not null) then

	accnum_w := lpad(accnum_p,8,'0');
	
	select	max(nr_prescricao),
		max(nr_sequencia)
	into	nr_prescricao_w,
		nr_seq_prescr_w
	from	prescr_procedimento
	where	nr_acesso_dicom = accnum_w;

	if	(nr_prescricao_w is not null) and
		(nr_seq_prescr_w is not null) then

		select 	count(nr_sequencia), nvl(max(nr_sequencia),0)
		into	qt_registro_wtt,
			max_registro_wtt
		from	prescr_proc_info_laudo
		where	nr_prescricao = nr_prescricao_w
		and	nr_seq_prescr = nr_seq_prescr_w;

		if (qt_registro_wtt = 0) then
		
			insert into prescr_proc_info_laudo 
			(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_prescricao,
			nr_seq_prescr,
			nr_paperprint,
			nr_filmprint,
			nr_opticmedia,
			nr_reportprint
			)
			values
			(
			prescr_proc_info_laudo_seq.nextVal,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_prescricao_w,
			nr_seq_prescr_w,
			paperprint_p,
			filmprint_p,
			opticmedia_p,
			reportprint_p
			);
		else 
			update	prescr_proc_info_laudo
			set	dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p,
				nr_paperprint = paperprint_p,
				nr_filmprint = filmprint_p,
				nr_opticmedia = opticmedia_p,
				nr_reportprint = reportprint_p
			where	nr_sequencia = max_registro_wtt;
		end if;
		
		commit;
			
	end if;	
			
end if;	

end grava_info_wtt;
/
