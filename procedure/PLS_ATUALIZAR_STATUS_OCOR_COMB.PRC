create or replace procedure pls_atualizar_status_ocor_comb
        (  nr_seq_ocorrencia_p  Number,
          nr_seq_ocor_combinada_p  Number,
          nr_seq_motivo_glosa_p  Number,
          nr_seq_guia_p    Number,
          nr_seq_requisicao_p  Number,
          nr_seq_execucao_p  Number,
          nr_seq_guia_proc_p  Number,
          nr_seq_guia_mat_p  Number,
          nr_seq_req_proc_p  Number,
          nr_seq_req_mat_p  Number,
          nr_seq_exec_proc_p  Number,
          nr_seq_exec_mat_p  Number,
          nm_usuario_p    Varchar2,
          cd_estabelecimento_p  Number) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar os itens da guia conforme a gera��o de ocorr~encia combinada.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_auditoria_w        Varchar2(2);
ie_glosa_w        Varchar2(2);
ie_define_se_gera_analise_w    Varchar2(255);
ie_tipo_item_w        Number(10);
nr_seq_item_w        Number(10);

begin

if  (nr_seq_guia_proc_p  is not null) then
  nr_seq_item_w  := nr_seq_guia_proc_p;
  ie_tipo_item_w  := 1;
elsif  (nr_seq_guia_mat_p  is not null) then
  nr_seq_item_w  := nr_seq_guia_mat_p;
  ie_tipo_item_w  := 2;
elsif  (nr_seq_req_proc_p  is not null) then
  nr_seq_item_w  := nr_seq_req_proc_p;
  ie_tipo_item_w  := 5;
elsif  (nr_seq_req_mat_p  is not null) then
  nr_seq_item_w  := nr_seq_req_mat_p;
  ie_tipo_item_w  := 6;
elsif  (nr_seq_exec_proc_p  is not null) then
  nr_seq_item_w  := nr_seq_exec_proc_p;
  ie_tipo_item_w  := 10;
elsif  (nr_seq_exec_mat_p  is not null) then
  nr_seq_item_w  := nr_seq_exec_mat_p;
  ie_tipo_item_w  := 11;
end if;

select  ie_auditoria
into  ie_auditoria_w
from  pls_ocorrencia
where  nr_sequencia  = nr_seq_ocorrencia_p
and  ie_situacao  = 'A';

if  (ie_tipo_item_w  in (1,2)) then
  ie_define_se_gera_analise_w := pls_obter_define_gera_analise(cd_estabelecimento_p,'A');
elsif  (ie_tipo_item_w  in (5,6)) then
  ie_define_se_gera_analise_w := pls_obter_define_gera_analise(cd_estabelecimento_p,'R');
end if;

if  (ie_auditoria_w = 'N') and
  (ie_tipo_item_w in (1,2,5,6)) then
  ie_auditoria_w := pls_obter_se_item_glosa_ocorr(nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_item_w, ie_tipo_item_w);
end if;

if  (ie_auditoria_w = 'N') then
  ie_glosa_w := pls_obter_se_item_glosa(nr_seq_item_w,ie_tipo_item_w);
else
  --Se a veriavel "ie_define_se_gera_analise_w", estiver com o valor "N", ent�o ir� verificar se para o item existe glosa definitiva, e se existir  ent�o ir� negar o item
  if  (ie_define_se_gera_analise_w = 'N') and (ie_tipo_item_w in (1,2,5,6)) then
    ie_auditoria_w := pls_obter_se_gerar_analise(nvl(nr_seq_guia_p,nr_seq_requisicao_p),  nr_seq_item_w, ie_tipo_item_w);

    select  decode(ie_auditoria_w,'N','S','N')
    into  ie_glosa_w
    from  dual;
  end if;
end if;

if  (ie_auditoria_w  = 'N') then
  if  (nr_seq_guia_p is not null) then
    ie_auditoria_w  := nvl(pls_obter_acao_ocor_comb_guia(nr_seq_guia_p, 'O'),'N');
  elsif  (nr_seq_requisicao_p is not null) then
    ie_auditoria_w  := nvl(pls_obter_acao_ocor_comb_req(nr_seq_requisicao_p,nr_seq_req_proc_p,nr_seq_req_mat_p, 'O'),'N');
  end if;
end if;

if  (ie_auditoria_w  = 'S') then
  if  (ie_tipo_item_w  = 1) then
    update  pls_guia_plano_proc
    set  ie_status  = 'A',
      qt_autorizada  = 0,
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_guia_proc_p;
  elsif  (ie_tipo_item_w  = 2) then
    update  pls_guia_plano_mat
    set  ie_status  = 'A',
      qt_autorizada  = 0,
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_guia_mat_p;
  elsif  (ie_tipo_item_w  = 5) then
    update  pls_requisicao_proc
    set  ie_status  = 'A',
      qt_procedimento  = 0,
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_req_proc_p;
  elsif  (ie_tipo_item_w  = 6) then
    update  pls_requisicao_mat
    set  ie_status  = 'A',
      qt_material  = 0,
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_req_mat_p;
  elsif  (ie_tipo_item_w  = 10) then
    update  pls_execucao_req_item
    set  ie_situacao  = 'A',
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_exec_proc_p;
  elsif  (ie_tipo_item_w  = 11) then
    update  pls_execucao_req_item
    set  ie_situacao  = 'A',
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_exec_mat_p;
  end if;
elsif  (ie_auditoria_w  = 'N') and (ie_glosa_w    = 'S') then
  if  (ie_tipo_item_w  = 1) then
    update  pls_guia_plano_proc
    set  ie_status  = 'N',
      qt_autorizada  = 0,
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_guia_proc_p;
  elsif  (ie_tipo_item_w  = 2) then
    update  pls_guia_plano_mat
    set  ie_status  = 'N',
      qt_autorizada  = 0,
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_guia_mat_p;
  elsif  (ie_tipo_item_w  = 5) then
    update  pls_requisicao_proc
    set  ie_status  = 'N',
      qt_procedimento  = 0,
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_req_proc_p;
  elsif  (ie_tipo_item_w  = 6) then
    update  pls_requisicao_mat
    set  ie_status  = 'N',
      qt_material  = 0,
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_req_mat_p;
  elsif  (ie_tipo_item_w  = 10) then
    update  pls_execucao_req_item
    set  ie_situacao  = 'N',
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_exec_proc_p;
  elsif  (ie_tipo_item_w  = 11) then
    update  pls_execucao_req_item
    set  ie_situacao  = 'N',
      dt_atualizacao  = sysdate,
      nm_usuario  = nm_usuario_p
    where  nr_sequencia  = nr_seq_exec_mat_p;
  end if;
end if;

end pls_atualizar_status_ocor_comb;
/