create or replace
procedure update_repository_file(	nm_path_p		varchar2,
					nm_usuario_p		Varchar2) is 

begin

	UPDATE FILE_REPOSITORY SET NM_PATH = nm_path_p WHERE NM_TABELA = 'GAP_PROCESSO_DOC';
	commit;

end update_repository_file;
/