create or replace
procedure atualizar_data_entrada_unid_js(	nr_prescricao_p	number,
				dt_entrada_unidade_p	date) is 

begin

if	(nr_prescricao_p is not null) then
	begin
	update 	prescr_medica
	set 	dt_entrada_unidade	= dt_entrada_unidade_p
	where  	nr_prescricao		= nr_prescricao_p;
	end;
end if;

commit;

end atualizar_data_entrada_unid_js;
/