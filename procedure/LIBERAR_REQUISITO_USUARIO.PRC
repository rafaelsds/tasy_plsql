create or replace procedure LIBERAR_REQUISITO_USUARIO(NR_SEQUENCIA_P in REQUISITO_USUARIO_PERGUNTA.NR_SEQUENCIA%type) is 

DS_PERGUNTA_w REQUISITO_USUARIO_PERGUNTA.DS_PERGUNTA%type;
DS_PERGUNTA_ING_w REQUISITO_USUARIO_PERGUNTA.DS_PERGUNTA_ING%type;
DS_EXPLICACAO_w REQUISITO_USUARIO_PERGUNTA.DS_EXPLICACAO%type;
DS_EXPLICACAO_ING_w REQUISITO_USUARIO_PERGUNTA.DS_EXPLICACAO_ING%type;
begin 

  if (NR_SEQUENCIA_P is not null) then
    select 
          DS_PERGUNTA,
          DS_PERGUNTA_ING,
          DS_EXPLICACAO,
          DS_EXPLICACAO_ING
    into 
          DS_PERGUNTA_W,
          DS_PERGUNTA_ING_w,
          DS_EXPLICACAO_W,
          DS_EXPLICACAO_ING_W
    from  REQUISITO_USUARIO_PERGUNTA 
    where NR_SEQUENCIA = NR_SEQUENCIA_P
    and   DT_LIBERACAO IS NULL;
  
    if (DS_PERGUNTA_W is not null and DS_PERGUNTA_ING_W is not null) and
       (DS_EXPLICACAO_W is not null and DS_EXPLICACAO_ING_W is not null) then
      update REQUISITO_USUARIO_PERGUNTA
      set DT_ATUALIZACAO = sysdate,
        NM_USUARIO = wheb_usuario_pck.get_nm_usuario,
        DT_LIBERACAO = sysdate,
        IE_STATUS = 1
      where 
        NR_SEQUENCIA = NR_SEQUENCIA_P;
      commit;
    else
      Wheb_mensagem_pck.exibir_mensagem_abort(1098062);
    end if;	
  end if;
end;
/