create or replace procedure recompile_objeto_release (
CD_VERSAO_P IN VARCHAR2,
NR_RELEASE_P IN NUMBER,
NM_USUARIO_P IN VARCHAR2) as

ds_comando_w	    clob;
ds_comando_linha_w	clob;
nr_index_original_w	Number(10);
nr_index_final_w	Number(10);
nr_byte_w			Number(10);
i					Number(10);
i_fim_w				Number(10);
c					number;
RES                 NUMBER(10);
script_list_w       DBMS_SQL.VARCHAR2A;

begin
FOR CC IN (SELECT DS_SCRIPT_OBJETO, NR_SEQUENCIA FROM OBJETO_RELEASE_BKP WHERE CD_VERSAO = CD_VERSAO_P AND NR_RELEASE = NR_RELEASE_P ORDER BY NR_SEQUENCIA) LOOP

    DS_COMANDO_W := CC.DS_SCRIPT_OBJETO;
    ds_comando_w:= replace(ds_comando_w,CHR(13)||CHR(10),CHR(10));
    select (length(ds_comando_w) - length(replace(ds_comando_w, CHR(10),'')))
    into	i_fim_w
    from	dual;

    i:= 0;
    nr_byte_w:= 1;
    nr_index_original_w:= 1;

    while (i < i_fim_w+1) loop

        i:= i+1;
        ds_comando_linha_w:= substr(ds_comando_w,nr_index_original_w,1000);
        nr_index_final_w:= instr(ds_comando_linha_w,CHR(10));
        if (nr_index_final_w = 0) and (i = i_fim_w+1) then
            nr_index_final_w:= 1000;
        end if;
        script_list_w(nr_byte_w) := substr(ds_comando_w,nr_index_original_w-1,nr_index_final_w);
        nr_index_original_w:= nr_index_original_w+nr_index_final_w+1;
        nr_byte_w:= nr_byte_w+1;

    end loop;

    c := DBMS_SQL.open_cursor;
    DBMS_SQL.parse(c, script_list_w, 1,nr_byte_w-1, TRUE, DBMS_SQL.NATIVE);
    res := DBMS_SQL.execute(c);
    DBMS_SQL.close_cursor(c);
    
    UPDATE OBJETO_RELEASE_BKP 
       SET NM_USUARIO = NM_USUARIO_P
         , DT_ATUALIZACAO = SYSDATE
         , IE_STATUS = 'R'
     WHERE NR_SEQUENCIA = CC.NR_SEQUENCIA;
    COMMIT;
END LOOP;
VALIDA_OBJETOS_SISTEMA;
end recompile_objeto_release;
/