create or replace
procedure SAN_Gerar_Doadores_Corresp (	nr_seq_envio_p		Number,
					nm_usuario_p		Varchar2) is

cd_pessoa_fisica_w	Varchar2(10);
dt_inicial_w		Date;
dt_final_w		Date;
ie_opcao_w		Varchar2(1);
cd_doador_w		varchar2(20);

Cursor C01 is
	select	distinct b.cd_pessoa_fisica
	from	san_doacao b
	where	b.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and	ie_opcao_w = '1'
	and	b.nr_seq_doacao_amostra is null
	and	b.dt_doacao between dt_inicial_w and fim_dia(dt_final_w)
	and	b.nr_sequencia = (select max(nr_sequencia)
			 	  from 	 san_doacao x
				  where  x.cd_pessoa_fisica = b.cd_pessoa_fisica)
	and	not exists (select 1
			from san_doacao e
			where	b.cd_pessoa_fisica = e.cd_pessoa_fisica
			and	e.dt_doacao >= b.dt_doacao
			and	e.nr_seq_doacao_amostra is not null)
	and 	exists (select 	1
			from 	san_exame_realizado c,
				san_exame_lote d,
				san_exame e
			where	d.nr_sequencia = c.nr_seq_exame_lote
			and	d.nr_seq_doacao = b.nr_sequencia
			and	c.nr_seq_exame = e.nr_sequencia
			and e.ie_exige_senha = 'S'
			and	upper(c.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')
			and	nvl(e.ie_fator_rh,'N') = 'N'
			and not exists (select 	1
					from	san_parametro p
					where	p.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
					and	p.nr_seq_exame_rh = c.nr_seq_exame))
	and	not exists (select 1
			from	san_envio_corresp_item f
			where	f.nr_seq_envio = nr_seq_envio_p
			and	f.cd_pessoa_fisica = b.cd_pessoa_fisica)
	union
	select	distinct b.cd_pessoa_fisica
	from	san_doacao b
	where	b.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and	ie_opcao_w = '2'
	and	b.nr_seq_doacao_amostra is not null
	and	b.dt_doacao between dt_inicial_w and fim_dia(dt_final_w)
	and	b.nr_sequencia = (select max(nr_sequencia)
			 	  from 	 san_doacao x
				  where  x.cd_pessoa_fisica = b.cd_pessoa_fisica)
	and 	exists (select 	1
			from 	san_exame_realizado c,
				san_exame_lote d,
				san_exame e
			where	d.nr_sequencia = c.nr_seq_exame_lote
			and	d.nr_seq_doacao = b.nr_sequencia
			and	c.nr_seq_exame = e.nr_sequencia
			and e.ie_exige_senha = 'S'
			and	upper(c.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')
			and	nvl(e.ie_fator_rh,'N') = 'N'
			and not exists (select 	1
					from	san_parametro p
					where	p.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
					and	p.nr_seq_exame_rh = c.nr_seq_exame))
	and	not exists (select 1
			from	san_envio_corresp_item f
			where	f.nr_seq_envio = nr_seq_envio_p
			and	f.cd_pessoa_fisica = b.cd_pessoa_fisica)
	union
	select 	distinct b.cd_pessoa_fisica
	from   	san_doacao b
	where  	ie_avaliacao_final = 'A'
	and	b.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and	ie_opcao_w = '3'
	and	b.dt_doacao between dt_inicial_w and fim_dia(dt_final_w)
	and	b.nr_sequencia = (select max(nr_sequencia)
			 	  from 	 san_doacao x
				  where  x.cd_pessoa_fisica = b.cd_pessoa_fisica)
	and 	not exists (select 1
			from 	san_exame_realizado c,
				san_exame_lote d,
				san_exame e
			where	d.nr_sequencia = c.nr_seq_exame_lote
			and	d.nr_seq_doacao = b.nr_sequencia
			and	c.nr_seq_exame = e.nr_sequencia
			and	upper(c.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')
			and	nvl(e.ie_fator_rh,'N') = 'N'
			and not exists (select 	1
					from	san_parametro p
					where	p.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
					and	p.nr_seq_exame_rh = c.nr_seq_exame))
	and	not exists (select 1
			from	san_envio_corresp_item f
			where	f.nr_seq_envio = nr_seq_envio_p
			and	f.cd_pessoa_fisica = b.cd_pessoa_fisica)
	union
	select 	distinct b.cd_pessoa_fisica
	from   	san_impedimento c,
		san_doacao_impedimento d,
		san_doacao b
	where  	b.nr_sequencia = d.nr_seq_doacao
	and    	d.nr_seq_impedimento = c.nr_sequencia
	and	b.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and	ie_opcao_w = '3'
	and	b.dt_doacao between dt_inicial_w and fim_dia(dt_final_w)
	and    	b.ie_avaliacao_final <> 'A'
	and    	c.ie_definitivo = 'N'
	and    ((dt_ocorrencia is null) or
		(dt_ocorrencia is not null and
		(trunc(dt_ocorrencia + nvl(nr_dias_inaptidao,0))) <= trunc(dt_final_w)))
	and    	dt_ocorrencia is not null
	and    	nr_dias_inaptidao > 0
	and	b.nr_sequencia = (select max(nr_sequencia)
				  from 	 san_doacao x
				  where  x.cd_pessoa_fisica = b.cd_pessoa_fisica)
	and 	not exists (select 1
			from 	san_exame_realizado c,
				san_exame_lote d,
				san_exame e
			where	d.nr_sequencia = c.nr_seq_exame_lote
			and	d.nr_seq_doacao = b.nr_sequencia
			and	c.nr_seq_exame = e.nr_sequencia
			and	upper(c.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')
			and	nvl(e.ie_fator_rh,'N') = 'N'
			and not exists (select 	1
					from	san_parametro p
					where	p.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
					and	p.nr_seq_exame_rh = c.nr_seq_exame))
	and	not exists (select 1
			from	san_envio_corresp_item f
			where	f.nr_seq_envio = nr_seq_envio_p
			and	f.cd_pessoa_fisica = b.cd_pessoa_fisica)
	union
	select 	distinct b.cd_pessoa_fisica
	from   	san_impedimento c,
		san_questionario d,
		san_doacao b
	where  	b.nr_sequencia = d.nr_seq_doacao
	and    	d.nr_seq_impedimento = c.nr_sequencia
	and	b.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and	ie_opcao_w = '3'
	and	b.dt_doacao between dt_inicial_w and fim_dia(dt_final_w)
	and    	b.ie_avaliacao_final <> 'A'
	and    	c.ie_definitivo = 'N'
	and    ((dt_ocorrencia is null) or
		(dt_ocorrencia is not null and
		(trunc(dt_ocorrencia + nvl(nr_dias_inaptidao,0))) <= trunc(dt_final_w)))
	and    	dt_ocorrencia is not null
	and    	nr_dias_inaptidao > 0
	and	b.nr_sequencia = (select max(nr_sequencia)
				  from 	 san_doacao x
				  where  x.cd_pessoa_fisica = b.cd_pessoa_fisica)
	and 	not exists (select 1
			from 	san_exame_realizado c,
				san_exame_lote d,
				san_exame e
			where	d.nr_sequencia = c.nr_seq_exame_lote
			and	d.nr_seq_doacao = b.nr_sequencia
			and	c.nr_seq_exame = e.nr_sequencia
			and	upper(c.ds_resultado) in ('POSITIVO','REAGENTE','INDETERMINADO')
			and	nvl(e.ie_fator_rh,'N') = 'N'
			and not exists (select 	1
					from	san_parametro p
					where	p.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
					and	p.nr_seq_exame_rh = c.nr_seq_exame))
	and    	nvl(d.ie_impede_doacao,'N') = 'S'
	and	not exists (select 1
			from	san_envio_corresp_item f
			where	f.nr_seq_envio = nr_seq_envio_p
			and	f.cd_pessoa_fisica = b.cd_pessoa_fisica)
	union
	select	distinct b.cd_pessoa_fisica
	from	san_doacao b
	where	b.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
	and	ie_opcao_w = '4'
	and	b.dt_doacao between dt_inicial_w and fim_dia(dt_final_w)
	and	b.nr_sequencia = (select max(nr_sequencia)
			 	  from 	 san_doacao x
				  where  x.cd_pessoa_fisica = b.cd_pessoa_fisica)
	and 	exists (select 	1
			from 	san_exame_realizado c,
				san_exame_lote d
			where	d.nr_sequencia = c.nr_seq_exame_lote
			and	d.nr_seq_doacao = b.nr_sequencia
			and	c.ie_andamento = 'S')
	and	not exists (select 1
			from	san_envio_corresp_item f
			where	f.nr_seq_envio = nr_seq_envio_p
			and	f.cd_pessoa_fisica = b.cd_pessoa_fisica)
	order by 1;

begin

if (nr_seq_envio_p is not null) then

	select	dt_inicial,
		dt_final,
		ie_opcao,
		cd_doador
	into	dt_inicial_w,
		dt_final_w,
		ie_opcao_w,
		cd_doador_w
	from	san_envio_correspondencia
	where	nr_sequencia = nr_seq_envio_p;

	if (dt_inicial_w is not null) and (dt_final_w is not null) and (cd_doador_w is null) then

		open C01;
		loop
		fetch C01 into
			cd_pessoa_fisica_w;
		exit when C01%notfound;
			begin

			insert into san_envio_corresp_item
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_envio,
				cd_pessoa_fisica)
			values(	san_envio_corresp_item_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_envio_p,
				cd_pessoa_fisica_w);

			end;
		end loop;
		close C01;

	elsif (cd_doador_w is not null) then
			insert into san_envio_corresp_item
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_envio,
				cd_pessoa_fisica)
			values(	san_envio_corresp_item_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_envio_p,
				cd_doador_w);
	end if;
end if;

commit;

end SAN_Gerar_Doadores_Corresp;
/