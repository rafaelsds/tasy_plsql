create or replace
procedure atualizar_itens_copia_plano(
			ie_manter_p		varchar2,
			ie_suspender_p		varchar2,
			ie_estender_p		varchar2,
			nr_seq_p		number,
			nm_usuario_p		Varchar2) is 

begin

if	(nr_seq_p <> 0) then

	update	w_copia_plano
	set    	ie_check_manter    = ie_manter_p,
		ie_check_suspender = ie_suspender_p,
		ie_check_estender  = ie_estender_p
	where	nr_sequencia       = nr_seq_p;

	commit;
end if;


end atualizar_itens_copia_plano;
/