create or replace
procedure alterar_classif_agenda (dt_agenda_p date default sysdate) is 

TYPE agendas_gerar_hor_row IS RECORD (
	cd_tipo_agenda		agenda.cd_tipo_agenda%type,
	cd_agenda 		agenda.cd_agenda%type,
	nr_tempo_alteracao	alteracao_classif_agenda.nr_tempo_alteracao%type
);
TYPE agendas_gerar_hor_table IS TABLE OF agendas_gerar_hor_row INDEX BY BINARY_INTEGER;

agendas_gerar_hor_w		agendas_gerar_hor_table;
cd_tipo_agenda_w		agenda.cd_tipo_agenda%type;
qt_reg_w			number(1);
qt_dias_gerar_w			number(10) := 1;
ds_feriado_w       		varchar2(1);
ds_retorno_w       		varchar2(4000);

cursor c01 is
	select 	ie_classif_nova, ie_classif_ant, nr_seq_classif_ant, cd_agenda, cd_tipo_agenda, nr_tempo_alteracao, ie_gerar_horarios, cd_estabelecimento
	from 	alteracao_classif_agenda
	where 	ie_situacao = 'A'
	and		nr_tempo_alteracao is not null
	and		cd_tipo_agenda is not null
	and		(ie_classif_nova is not null or nr_seq_classif_nova  is not null)	
	and 	dt_agenda_p between nvl(dt_inicio_vigencia, dt_agenda_p) and nvl(dt_fim_vigencia, dt_agenda_p)
	and	((cd_agenda is not null) or (cd_tipo_agenda is not null));

procedure gerar_horarios_agendas (cd_tipo_agenda_pp number, cd_agenda_pp number, nr_tempo_alteracao_pp number) is
	cursor agenda_c is
		select 	ag.cd_agenda,
			ag.cd_estabelecimento,
			ag.ie_gerar_sobra_horario
		from 	agenda ag
		where 	ie_situacao = 'A'
		AND 	((ag.cd_agenda = cd_agenda_pp) or (cd_agenda_pp IS NULL and cd_tipo_agenda = cd_tipo_agenda_pp))
		order by 1;
	
	begin
	
	qt_dias_gerar_w := obter_dias_entre_datas(dt_agenda_p,dt_agenda_p+nr_tempo_alteracao_pp/24/60);

	for r_agenda in agenda_c loop
		for qt_dias_w in 0..qt_dias_gerar_w loop
			if (cd_tipo_agenda_pp = 2) then
				gerar_horario_agenda_exame(r_agenda.cd_estabelecimento,
					cd_agenda_pp,
					trunc(dt_agenda_p+qt_dias_w),
					'TASY');
			elsif (cd_tipo_agenda_pp in (3,4)) then
				select decode(obter_se_feriado(r_agenda.cd_estabelecimento, trunc(dt_agenda_p+qt_dias_w)), 0, 'N', 'S')
				into ds_feriado_w
				from dual;
			
				horario_livre_consulta(r_agenda.cd_estabelecimento,
					cd_agenda_pp,
					ds_feriado_w,
					trunc(dt_agenda_p+qt_dias_w),
					'TASY',
					'S',
					r_agenda.ie_gerar_sobra_horario,
					'N',
					0,
					ds_retorno_w);
			elsif (cd_tipo_agenda_pp = 5) then
				gerar_horario_agenda_servico(r_agenda.cd_estabelecimento,
					cd_agenda_pp,
					trunc(dt_agenda_p+qt_dias_w),
					'TASY');
			end if;
		end loop;
	end loop;
	commit;
	end gerar_horarios_agendas;
	
procedure gerar_horarios_vetor is
	begin
	if (agendas_gerar_hor_w.count > 0) then
		for linha in agendas_gerar_hor_w.first .. agendas_gerar_hor_w.last loop
			if (agendas_gerar_hor_w.exists(linha)) then
				gerar_horarios_agendas(agendas_gerar_hor_w(linha).cd_tipo_agenda, agendas_gerar_hor_w(linha).cd_agenda, agendas_gerar_hor_w(linha).nr_tempo_alteracao);
			end if;
		end loop;
	end if;
	end;

procedure remover_controle_horario (cd_tipo_agenda_pp number, cd_agenda_pp number, nr_tempo_alteracao_pp number, ie_gerar_horarios_pp varchar2, 
					cd_estabelecimento_pp number, ie_classif_ant_pp varchar2, nr_seq_classif_ant_pp number) is
	-- Consultas / Servi�os
	Cursor C011 is
		select 	distinct a.cd_agenda
		from 	agenda a,
				agenda_consulta b,
				agenda_controle_horario c
		where	a.cd_agenda = b.cd_agenda
		and		a.cd_agenda = c.cd_agenda
		and		c.dt_agenda	= trunc(b.dt_agenda)
		and 	b.ie_status_agenda = 'L'
		and		nvl(a.ie_situacao,'A')	= 'A'
		and		b.ie_classif_agenda = ie_classif_ant_pp
		and 	a.cd_tipo_agenda = cd_tipo_agenda_pp
		and		(a.cd_estabelecimento = cd_estabelecimento_pp or cd_estabelecimento_pp is null)
		and		b.dt_agenda between dt_agenda_p and dt_agenda_p+nr_tempo_alteracao_pp/24/60;

	-- Exames
	Cursor C012 is
		select 	distinct a.cd_agenda 
		from 	agenda a,
				agenda_paciente b,
				agenda_controle_horario c
		where	a.cd_agenda = b.cd_agenda
		and		a.cd_agenda = c.cd_agenda
		and		c.dt_agenda	= trunc(b.dt_agenda)
		and 	b.ie_status_agenda = 'L'
		and		nvl(a.ie_situacao,'A')	= 'A'
		and		b.NR_SEQ_CLASSIF_AGENDA = nr_seq_classif_ant_pp
		and 	a.cd_tipo_agenda = cd_tipo_agenda_pp
		and		b.hr_inicio > sysdate
		and		(a.cd_estabelecimento = cd_estabelecimento_pp or cd_estabelecimento_pp is null)
		and		b.hr_inicio between dt_agenda_p and dt_agenda_p+nr_tempo_alteracao_pp/24/60;
		
	procedure adicionar_agenda_vetor (cd_agenda_vet_pp number) is
		begin
		if (cd_agenda_vet_pp is not null) and (cd_agenda_vet_pp > 0) then
			if (not agendas_gerar_hor_w.exists(cd_agenda_pp)) or
			   (nr_tempo_alteracao_pp > nvl(agendas_gerar_hor_w(cd_agenda_pp).nr_tempo_alteracao,0)) then
				agendas_gerar_hor_w(cd_agenda_pp).cd_tipo_agenda := cd_tipo_agenda_pp;
				agendas_gerar_hor_w(cd_agenda_pp).cd_agenda := cd_agenda_pp;
				agendas_gerar_hor_w(cd_agenda_pp).nr_tempo_alteracao := nvl(nr_tempo_alteracao_pp, 0);
			end if;
		end if;
		end;
	
	procedure remover_controle (cd_agenda_vet_pp number) is
		begin
		delete agenda_controle_horario a
		where dt_agenda between trunc(dt_agenda_p) and dt_agenda_p+nr_tempo_alteracao_pp/24/60
		and cd_agenda = cd_agenda_vet_pp;
		if (ie_gerar_horarios_pp = 'S') then
			adicionar_agenda_vetor(cd_agenda_pp);
		end if;
		end;
	
	begin
	if (cd_agenda_pp is null) then
		if (cd_tipo_agenda_pp in (3,4,5)) then
			for agendas in c011 loop
				remover_controle(agendas.cd_agenda);
			end loop;
		elsif (cd_tipo_agenda_pp = 2) then
			for agendas in c012 loop
				remover_controle(agendas.cd_agenda);
			end loop;
		end if;
	else
		remover_controle(cd_agenda_pp);
	end if;
	commit;
	end remover_controle_horario;

begin

for c01_w in c01 loop
	qt_reg_w := 0;
	if (c01_w.cd_tipo_agenda is null) then
		select 	cd_tipo_agenda
		into	cd_tipo_agenda_w
		from 	agenda
		where 	cd_agenda = c01_w.cd_agenda;
	else
		cd_tipo_agenda_w := c01_w.cd_tipo_agenda;
	end if;
	
	if (cd_tipo_agenda_w in (3,4,5)) then
		if (c01_w.cd_agenda is null) then
			select 	nvl(max(1),0)
			into	qt_reg_w
			from 	agenda a,
					agenda_consulta b
			where	a.cd_agenda = b.cd_agenda
			and 	b.ie_status_agenda = 'L'
			and		nvl(a.ie_situacao,'A')	= 'A'
			and		b.ie_classif_agenda = c01_w.ie_classif_ant
			and 	a.cd_tipo_agenda = c01_w.cd_tipo_agenda
			and		(a.cd_estabelecimento = c01_w.cd_estabelecimento or c01_w.cd_estabelecimento is null)
			and		dt_agenda between dt_agenda_p and dt_agenda_p+c01_w.nr_tempo_alteracao/24/60;
		else
			select 	nvl(max(1),0)
			into	qt_reg_w
			from 	agenda a,
					agenda_consulta b
			where	a.cd_agenda = b.cd_agenda
			and 	b.ie_status_agenda = 'L'
			and		nvl(a.ie_situacao,'A')	= 'A'
			and		b.ie_classif_agenda = c01_w.ie_classif_ant
			and 	a.cd_agenda = c01_w.cd_agenda
			and		dt_agenda between dt_agenda_p and dt_agenda_p+c01_w.nr_tempo_alteracao/24/60;
		end if;
	elsif (cd_tipo_agenda_w = 2) then
		if (c01_w.cd_agenda is null) then
			select 	nvl(max(1),0)
			into	qt_reg_w
			from 	agenda a,
					agenda_paciente b
			where	a.cd_agenda = b.cd_agenda
			and 	b.ie_status_agenda in ('L')
			and		nvl(a.ie_situacao,'A')	= 'A'
			and		b.NR_SEQ_CLASSIF_AGENDA = c01_w.nr_seq_classif_ant
			and 	a.cd_tipo_agenda = c01_w.cd_tipo_agenda
			and		b.hr_inicio > sysdate
			and		(a.cd_estabelecimento = c01_w.cd_estabelecimento or c01_w.cd_estabelecimento is null)
			and		b.hr_inicio between dt_agenda_p and dt_agenda_p+c01_w.nr_tempo_alteracao/24/60;
		else
			select 	nvl(max(1),0)
			into	qt_reg_w
			from 	agenda a,
					agenda_paciente b
			where	a.cd_agenda = b.cd_agenda
			and		nvl(a.ie_situacao,'A')	= 'A'
			and 	b.ie_status_agenda in ('L')
			and		b.NR_SEQ_CLASSIF_AGENDA = c01_w.nr_seq_classif_ant
			and 	a.cd_agenda = c01_w.cd_agenda
			and		b.hr_inicio > sysdate
			and		b.hr_inicio between dt_agenda_p and dt_agenda_p+c01_w.nr_tempo_alteracao/24/60;
		end if;
	end if;
	if (qt_reg_w > 0) then
		remover_controle_horario(cd_tipo_agenda_w, c01_w.cd_agenda, c01_w.nr_tempo_alteracao, c01_w.ie_gerar_horarios, c01_w.cd_estabelecimento,
						c01_w.ie_classif_ant, c01_w.nr_seq_classif_ant);
	end if;
end loop;

gerar_horarios_vetor;

commit;

end alterar_classif_agenda ;
/