create or replace
procedure gerar_ajustes_traducao(
			cd_funcao_p			funcao.cd_funcao%type,
			cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type,
			nm_usuario_p		usuario.nm_usuario%type,
			cd_expressao_p		dic_expressao.cd_expressao%type,
			nr_seq_traducao_p	dic_expressao_idioma.nr_sequencia%type,
			ds_retorno_p		out varchar2) is

ds_motivo_w	ajuste_versao.ds_motivo%type;
			
begin
	select	wheb_mensagem_pck.get_texto(1060408)
	into	ds_motivo_w
	from 	dual;
	
	inserir_traducao_ajuste_versao(
		cd_funcao_p,
		cd_pessoa_fisica_p,
		ds_motivo_w,
		nm_usuario_p,
		'3.02.1742',
		cd_expressao_p,
		nr_seq_traducao_p,
		15417,
		ds_retorno_p);
		
end gerar_ajustes_traducao;
/