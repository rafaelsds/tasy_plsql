create or replace
procedure HSL_gerar_ordem_apres_grupo (	nr_regra_ordem_p	number,
					nm_usuario_p		varchar2) is
		
nr_seq_grupo_w		number(10,0);
ie_informacao_w		varchar2(15);	
nr_seq_apres_w		number(3,0) := 999;
ie_acm_sn_final_w	varchar2(1);
		
cursor c01 is
select	nr_sequencia,
	7,
	nr_seq_apres,
	ie_acm_sn_final
from	regra_ordem_grupo_rep
where	nr_seq_regra	= nr_regra_ordem_p
and	ie_grupo	= 'M'
order by nvl(nr_seq_apres,999);
		
begin
if	(nr_regra_ordem_p is not null) then
	begin
	open c01;
	loop
	fetch c01 into
		nr_seq_grupo_w,
		ie_informacao_w,
		nr_seq_apres_w,
		ie_acm_sn_final_w;
	exit when c01%notfound;
		begin
		
		update	w_hsl_prescr_medica_hor
		set	nr_seq_apres_grupo	= nr_seq_apres_w
		where	ie_tipo			= ie_informacao_w;
		
		update	w_hsl_prescr_medica
		set	nr_seq_apres_grupo	= nr_seq_apres_w
		where	ie_tipo			= ie_informacao_w;
		
		HSL_gerar_ordem_apres_inf(nr_seq_grupo_w, ie_acm_sn_final_w, ie_informacao_w, nm_usuario_p);
		end;
	end loop;
	close c01;
	end;
end if;
end HSL_gerar_ordem_apres_grupo;
/
