create or replace
procedure agenda_paciente_beforepost_js(	ie_tipo_classif_p		varchar2,
					cd_agenda_p          	number,  
					dt_agenda_p           	date,    
					hr_inicio_p           		date,    
					nr_minuto_duracao_p    	number,  
					nr_seq_agenda_p   		number,
					cd_medico_p		varchar2,
					ds_erro_p    out  		varchar2,
					nm_usuario_p		varchar2) is 
						
ds_erro_w	varchar2(2000) := null;

begin

if	(ie_tipo_classif_p <> 'E') then
	Consistir_Duracao_Agenda_Pac(cd_agenda_p, dt_agenda_p, hr_inicio_p, nr_minuto_duracao_p, nr_seq_agenda_p,0,ds_erro_w);
end if;

if	(ds_erro_w is null) then
	consistir_horario_medico(nr_seq_agenda_p, cd_medico_p, nr_minuto_duracao_p, ds_erro_w);
else
	Wheb_mensagem_pck.exibir_mensagem_abort(261436, 'ERRO='|| substr(ds_erro_w,1,60));
end if;

ds_erro_p := ds_erro_w;

end agenda_paciente_beforepost_js;
/