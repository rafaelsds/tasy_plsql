create or replace procedure
com_update_data_mod_fun	(
nr_seq_mod_impl_p	number,
nm_usuario_p		varchar2
) is

begin

	if	(nr_seq_mod_impl_p is not null
		and	nm_usuario_p is not null)	then
		begin
			update	com_cliente_fun_impl
			set	dt_inicio_util	= sysdate,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_seq_mod_impl = nr_seq_mod_impl_p;
			
			commit;
		end;
	end if;

end com_update_data_mod_fun;
/