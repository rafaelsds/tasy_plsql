create or replace
procedure consistir_prazo_convenio_agend(cd_pessoa_fisica_p	varchar2,
					 dt_agenda_p		date,
					 cd_convenio_p		number,
				         nm_usuario_p		Varchar2,
					 cd_estabelecimento_p	number,
					 ds_mensagem_p		out varchar2) is 
	
nr_seq_regra_w		number(10,0);
ds_mensagem_w		varchar2(255);
qt_prazo_minimo_w	number(10,0);
qt_prazo_maximo_w	number(10,0);
dt_agenda_w		date;
qt_dias_w		number(10,0);
	
begin

select	max(nr_sequencia)
into	nr_seq_regra_w
from	agenda_bloq_prazo_conv
where	cd_convenio = cd_convenio_p;

if	(nr_seq_regra_w is not null) then
	select	max(ds_mensagem),
		max(qt_prazo_minimo),
		max(qt_prazo_maximo)
	into	ds_mensagem_w,
		qt_prazo_minimo_w,
		qt_prazo_maximo_w
	from	agenda_bloq_prazo_conv
	where	nr_sequencia = nr_seq_regra_w;
	
	select	max(b.dt_agenda)
	into	dt_agenda_w
	from	agenda a,
		agenda_consulta b
	where	a.cd_tipo_agenda	= 3
	and	a.cd_agenda		= b.cd_agenda
	and	b.ie_status_agenda	= 'E'
	and	b.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	b.dt_agenda < dt_agenda_p;
		
	if	(dt_agenda_w is not null) then		
		qt_dias_w := trunc(dt_agenda_p) - trunc(dt_agenda_w);
		if	(qt_dias_w > qt_prazo_minimo_w) and (qt_dias_w < qt_prazo_maximo_w) then
			ds_mensagem_p	:= ds_mensagem_w;
		end if;				
	end if;
end if;

end consistir_prazo_convenio_agend;
/