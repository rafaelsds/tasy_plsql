create or replace
procedure gerar_opme_presc_cirurgia(
			nr_cirurgia_p 		NUMBER,
			nm_usuario_p		VARCHAR,
			cd_estabelecimento_p	NUMBER,
			nr_prescricao_p		out number) is


ie_possui_prescricao_w		VARCHAR2(1);
nr_cirurgia_w			NUMBER(10);
nr_prescricao_w			NUMBER(10);
ie_tipo_pessoa_w			NUMBER(1);
cd_medico_cirurgiao_w		VARCHAR2(10);
cd_pessoa_fisica_w		VARCHAR2(10);
cd_material_w			NUMBER(10);
qt_material_w			NUMBER(18);
nr_seq_prescr_material_w		NUMBER(6);
ie_origem_inf_w			VARCHAR2(1);
cd_convenio_w			NUMBER(5);
cd_categoria_w			VARCHAR2(10);
cd_setor_atendimento_w		NUMBER(10,0);
ie_forma_adep_w		    	VARCHAR2(10);
cd_unidade_medida_w		VARCHAR2(5);
cd_intervalo_w			VARCHAR2(7);
ie_gerado_w			VARCHAR2(1)	:= 'N';
cd_cgc_w			VARCHAR2(14);
ie_adep_w		    VARCHAR2(10);
nr_atendimento_w			NUMBER(10,0);
ie_gera_opme_w			VARCHAR2(1);
nr_seq_autorizacao_w	NUMBER(10);
ie_status_opme_w		varchar2(255);
nr_seq_opme_w			number(10,0);
ie_fat_direto_w			varchar2(5);
cd_perfil_w			number(5);
nr_seq_agenda_w		number(10);
cd_setor_atendimento_ww		number(5);


cursor	c01 is
	select	a.cd_material,
		a.qt_material,
		a.ie_origem_inf,
		a.cd_cgc,
		a.nr_sequencia,
		substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_p,'UMS'),1,30),
		obter_se_fatur_direto_opme(a.nr_sequencia)
	from	agenda_pac_opme a
	where	a.nr_seq_agenda = nr_seq_agenda_w
	and 	((nvl(ie_status_opme_w,'N') = 'N') or (obter_se_contido_char(ie_autorizado,ie_status_opme_w) = 'S'))
	and	((ie_gera_opme_w = 'S') or
		 (ie_gera_opme_w = 'I' and a.ie_integracao = 'S') or
		 (ie_gera_opme_w = 'E' and nvl(a.ie_integracao, 'N') = 'N'))
	and	not exists(	select	1
				from	prescr_medica b,
					prescr_material c
				where	b.nr_prescricao = c.nr_prescricao
				and	b.nr_seq_agenda = a.nr_seq_agenda
				and	c.cd_material   = a.cd_material);

begin
cd_perfil_w	:= obter_perfil_ativo;


select	nvl(nr_atendimento,0),
		cd_convenio,
		cd_categoria,
		CD_MEDICO_CIRURGIAO,
		cd_pessoa_fisica,
		nr_cirurgia,
		nr_prescricao_espec,
		nr_seq_agenda
into	nr_atendimento_w,
		cd_convenio_w,
		cd_categoria_w,
		cd_medico_cirurgiao_w,
		cd_pessoa_fisica_w,
		nr_cirurgia_w,
		nr_prescricao_w,
		nr_seq_agenda_w
from	cirurgia
where	nr_cirurgia = nr_cirurgia_p;



if	(nr_prescricao_w	is null) then


	
	select	max(nr_prescricao)
	into	nr_prescricao_w
	from 	prescr_medica
	where	nr_seq_agenda 		= nr_seq_agenda_w
	and		ie_tipo_prescr_cirur 	= 1;

	if	(nr_prescricao_w	is null) then


		Obter_Param_Usuario(924,246,cd_perfil_w, nm_usuario_p, cd_estabelecimento_p, ie_forma_adep_w);
		Obter_Param_Usuario(871,558,cd_perfil_w, nm_usuario_p, cd_estabelecimento_p, ie_status_opme_w);
		Obter_Param_Usuario(900,336,cd_perfil_w, nm_usuario_p, cd_estabelecimento_p, ie_gera_opme_w);
		obter_param_usuario(900,548,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,cd_setor_atendimento_ww);
		
		select	prescr_medica_seq.nextval
		into	nr_prescricao_w
		from	dual;

		select	nvl(max(b.ie_tipo_pessoa),'1')
		into	ie_tipo_pessoa_w
		from	pessoa_fisica b,
			usuario a
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.nm_usuario		= nm_usuario_p;

		if	(nr_atendimento_w > 0) then
			cd_setor_atendimento_w	:=	obter_setor_atendimento(nr_atendimento_w);
		end if;	

		if	(ie_forma_adep_w = 'DS') then
			select	nvl(max(ie_adep),'N')
			into	ie_adep_w
			from	setor_atendimento
			where	cd_setor_atendimento = cd_setor_atendimento_w;
		elsif	(ie_forma_adep_w = 'NV') then
			ie_adep_w   := 'N';
		elsif	(ie_forma_adep_w = 'PV') then
			ie_adep_w := 'S';
		elsif	(ie_forma_adep_w = 'PNV') then
			ie_adep_w := 'N';
		else
			ie_adep_w := 'S';
		end if;		
		
		insert	into prescr_medica(
			dt_prescricao,
			cd_medico,
			cd_pessoa_fisica,
			cd_estabelecimento,
			nm_usuario_original,
			nr_horas_validade,
			nr_prescricao,
			nr_cirurgia,
			ie_recem_nato,
			ie_origem_inf,
			dt_primeiro_horario,
			nm_usuario,
			dt_atualizacao,
			cd_prescritor,
			ie_tipo_prescr_cirur,
			nr_seq_agenda,
			nr_atendimento,
			ie_adep,
			cd_setor_atendimento)
		values(
			sysdate,
			cd_medico_cirurgiao_w,
			cd_pessoa_fisica_w,
			cd_estabelecimento_p,
			nm_usuario_p,
			24,
			nr_prescricao_w,
			nr_cirurgia_w,
			'N',
			ie_tipo_pessoa_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			obter_dados_usuario_opcao(nm_usuario_p, 'C'),
			1,
			nr_seq_agenda_w,
			decode(nr_atendimento_w,0,null,nr_atendimento_w),
			ie_adep_w,
			cd_setor_atendimento_ww);		
		
		update	cirurgia
		set		NR_PRESCRICAO_ESPEC = nr_prescricao_w
		where	nr_cirurgia			= nr_cirurgia_w;
				
		
		
		open c01;
		loop
		fetch c01 into
			cd_material_w,
			qt_material_w,
			ie_origem_inf_w,
			cd_cgc_w,
			nr_seq_opme_w,
			cd_unidade_medida_w,
			ie_fat_direto_w;
		exit when c01%notfound;
			begin
	
			
			select 	max(cd_intervalo)
			into	cd_intervalo_w
			from 	intervalo_prescricao
			where 	ie_agora = 'S';
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_seq_prescr_material_w
			from	prescr_material
			where	nr_prescricao = nr_prescricao_w;

			insert into prescr_material
				(nr_prescricao,
				nr_sequencia,
				ie_origem_inf,
				cd_material,
				cd_unidade_medida,
				qt_unitaria,
				qt_material,
				cd_motivo_baixa,
				ie_status_cirurgia,
				cd_intervalo,
				dt_atualizacao,
				nm_usuario,
				cd_fornec_consignado,
				qt_dose,
				nr_ocorrencia,
				qt_total_dispensar,
				ds_horarios,
				dt_baixa,
				ie_utiliza_kit,
				cd_unidade_medida_dose,
				qt_conversao_dose,
				ie_urgencia,
				nr_sequencia_solucao,
				nr_sequencia_proc,
				qt_solucao,
				hr_dose_especial,
				qt_dose_especial,
				ds_dose_diferenciada,
				ie_medicacao_paciente,
				nr_sequencia_diluicao)
			values	(nr_prescricao_w,
				nr_seq_prescr_material_w,
				ie_origem_inf_w,
				cd_material_w,
				cd_unidade_medida_w,
				qt_material_w,
				qt_material_w,
				0,
				'GI',
				cd_intervalo_w,
				sysdate,
				nm_usuario_p,
				cd_cgc_w,
				qt_material_w,
				1,
				qt_material_w,
				null,
				null,
				'N',
				null,
				null,
				'N',
				null,
				null,
				null,
				null,
				null,
				null,
				'N',
				null);
				
					
				update 	autorizacao_cirurgia
				set 	nr_prescricao 	  = nr_prescricao_w,
					nm_usuario	  = nm_usuario_p,
					dt_atualizacao	  = sysdate
				where 	nr_prescricao is null
				and	nr_seq_agenda = nr_seq_agenda_w
				and	exists (select	1 
						from 	material_autor_cirurgia x 
						where	x.nr_seq_opme = nr_seq_opme_w);

						
				update 	autorizacao_convenio
				set 	nr_prescricao 	  = nr_prescricao_w,
					nm_usuario	  = nm_usuario_p,
					dt_atualizacao	  = sysdate
				where 	nr_prescricao is null
				and	nr_seq_agenda = nr_seq_agenda_w
				and	exists(	select	1 
						from 	material_autorizado x 
						where	x.nr_seq_opme = nr_seq_opme_w);

				update	material_autorizado
				set	nr_seq_prescricao = nr_seq_prescr_material_w,
					nr_prescricao     = nr_prescricao_w,
					nm_usuario	  = nm_usuario_p,
					dt_atualizacao	  = sysdate
				where	nr_seq_opme       = nr_seq_opme_w
				and	cd_material	  = cd_material_w
				and	nr_prescricao is null;
			
			end;
		end loop;
		close c01;
	else
	
		update	cirurgia
		set		NR_PRESCRICAO_ESPEC = nr_prescricao_w
		where	nr_cirurgia			= nr_cirurgia_w;

	end if;
end if;
commit;

nr_prescricao_p	:= nr_prescricao_w;

end gerar_opme_presc_cirurgia;
/
