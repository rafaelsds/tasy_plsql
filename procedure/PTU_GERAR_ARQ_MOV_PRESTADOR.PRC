create or replace
procedure ptu_gerar_arq_mov_prestador(	nr_seq_lote_p		number,
					cd_interface_p		varchar2,
					nm_arquivo_xml_p	varchar2,
					nm_usuario_p		varchar2,
					ds_local_p	out	varchar2,
					nm_arquivo_p	out	varchar2) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar arquivo A400 de acordo com a vers�o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: PTU 5.0
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

nm_arquivo_w			varchar2(255);
ds_local_w			varchar2(255) := null;
ds_local_windows_w		varchar2(255) := null;
ds_erro_w			varchar2(255);
ds_local_windows_zip_w		varchar2(4000);
nm_arquivo_zip_w		varchar2(255);
ds_arquivos_w			varchar2(255);
ds_erro_zip_w			varchar2(1000);

begin

select	ptu_obter_nome_exportacao(nr_seq_lote_p,'MCP')
into	nm_arquivo_w
from	dual;

begin
obter_evento_utl_file(3, null, ds_local_w, ds_erro_w);
pls_obter_dir_rede_utl_file(3, null, ds_local_windows_w, ds_erro_w);
exception
when others then
	ds_local_w := null;
	ds_local_windows_w	:= '';
end;

if	(ds_local_w is null) then
	obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);
	pls_obter_dir_rede_utl_file(1, null, ds_local_windows_w, ds_erro_w);
end if;

-- PTU 5.0
if	(cd_interface_p = '2477') then
	ptu_gerar_arq_mov_prestador_50(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);
	
-- PTU 6.0
elsif	(cd_interface_p = '2583') then
	ptu_gerar_arq_mov_prestador_60(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);
	
-- PTU 6.2
elsif	(cd_interface_p = '2647') then
	ptu_gerar_arq_mov_prestador_62(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);
	
-- PTU 6.3
elsif	(cd_interface_p = '2689') then
	ptu_gerar_arq_mov_prestador_63(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);

-- PTU 7.0
elsif	(cd_interface_p = '2751') then
	ptu_gerar_arq_mov_prestador_70(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);
	
-- PTU 8.0
elsif	(cd_interface_p = '2786') then
	ptu_gerar_arq_mov_prestador_80(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);
	
-- PTU 8.1
elsif	(cd_interface_p = '2831') then
	ptu_gerar_arq_mov_prestador_81(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);
	
-- PTU 10.0
elsif	(cd_interface_p = '2922') then
	ptu_gerar_arq_mov_prestad_100(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);
	
-- PTU 11.0
elsif	(cd_interface_p = '2977') then
	ptu_gerar_arq_mov_prestad_110(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);

-- PTU 11.1
elsif	(cd_interface_p = '3083') then
	ptu_gerar_arq_mov_prestad_111(nr_seq_lote_p,cd_interface_p,nm_usuario_p,ds_local_w,nm_arquivo_w);
end if;

-- ## REMOVIDA A COMPACTA��O DE ARQUIVO VIA UTL_FILE, O METODO USADO ERA VALIDO SOMENTE NO ORACLE 12C em diante
-- PLS_UTL_ZIP_PCK.COMPACTAR_ARQUIVOS

ds_local_p	:= ds_local_windows_w;
nm_arquivo_p	:= nm_arquivo_w;

end ptu_gerar_arq_mov_prestador;
/