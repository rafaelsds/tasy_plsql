create or replace 
procedure record_integration_notify(cd_person_p			           in varchar2,
                                          nr_encounter_p       in   number,
                                          ie_type_record_p     in   varchar2,
                                          nr_seq_record_p      in   number,
                                          nr_seq_record_comp_p in   number,
                                          ie_trigger_p         in   varchar2) is
                                          
nm_usuario_w   usuario.nm_usuario%type;
begin
begin
nm_usuario_w := nvl(wheb_usuario_pck.get_nm_usuario,'Tasy');

insert into integration_notifications (nr_sequencia,
                                       dt_record,
                                       dt_atualizacao,
                                       nm_usuario,
                                       cd_person,
                                       nr_encounter,
                                       ie_type_record,
                                       nr_seq_record,
                                       nr_seq_record_comp,
                                       ds_stack,
                                       ie_viewed)
values                                 (integration_notifications_seq.nextval,
                                       sysdate,
                                       sysdate,
                                       nm_usuario_w,
                                       cd_person_p,
                                       nr_encounter_p,
                                       ie_type_record_p,
                                       nr_seq_record_p,
                                       nr_seq_record_comp_p,
                                       substr(sys.dbms_utility.format_call_stack,1,1500),
                                       'N');
                                       
if (ie_trigger_p = 'N') then
   commit;
end if;   

exception
when others then
	null;
end;

end record_integration_notify;
/
