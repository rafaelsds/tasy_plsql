create or replace
procedure ws_registrar_prescr_proc_mat (	cd_barras_p			varchar2,
				nr_prescricao_p			number,
				nr_seq_prescr_p			number,
				cd_material_integracao_p		varchar2,
				dt_coleta_p			date,
				qt_volume_p			number,
				qt_tempo_p			number,
				ie_amostra_p			varchar2,
				nr_amostra_p			varchar2,
				nm_usuario_p			varchar2,
				nr_seq_grupo_mat_p		out number,
				cd_erro_p			out varchar2,
				ds_erro_p				out varchar2) is 

/*								
0 - SUCESSO
1 - Dados n�o informados
	1.1 - C�digo de barras n�o informado
	1.2 - Prescri��o n�o informada
	1.3 - Material de integra��o n�o0 informado
	1.4 - Data de coleta n�o informada
	1.5 - Volume n�o informado
	1.6 - Tempo de coleta n�o informado
2 - Dados inconsistentes
	2.1 - Equipamento n�o cadastrado
	2.2 - Prescri��o inexistente
	2.3 - Material do exame n�o encontrado
3 - Erro ao registrar
4 - Erro ao atualizar
*/											
	
ie_existe_prescricao_w		varchar2(1);

cd_equipamento_w			equipamento_lab.cd_equipamento%type;
--cd_material_exame_w			material_exame_lab.cd_material_exame%type;	
nr_seq_material_w			material_exame_lab.nr_sequencia%type;	
nr_seq_prescr_proc_mat_w	prescr_proc_material.nr_sequencia%type;
nr_seq_int_prescr_w			number(10);
ie_grupo_imp_amostra_seq_w	lab_parametro.ie_gerar_padrao_grupo_imp%type;
cd_estabelecimento_w		prescr_medica.cd_estabelecimento%type;
nr_seq_grupo_w				exame_laboratorio.nr_seq_grupo%type;
nr_seq_grupo_imp_w			exame_laboratorio.nr_seq_grupo_imp%type;

begin

cd_erro_p	:= 0;

if	(cd_barras_p is null) then
	cd_erro_p	:= 1.1;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280079,null);
elsif	(nr_prescricao_p is null) then
	cd_erro_p	:= 1.2;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277599,null);
elsif	(cd_material_integracao_p is null) then
	cd_erro_p	:= 1.3;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278279,null);
elsif	(dt_coleta_p is null) then
	cd_erro_p	:= 1.4;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280081,null);
elsif	(qt_volume_p is null) then
	cd_erro_p	:= 1.5;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280082,null);
elsif	(qt_tempo_p is null) then
	cd_erro_p	:= 1.6;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280083,null);
end if;

if	(cd_erro_p = 0) then

	select	max(cd_equipamento)
	into	cd_equipamento_w
	from	equipamento_lab a
	where	a.ds_sigla =  'WebService';

	select	decode(count(*),0,'N','S')
	into	ie_existe_prescricao_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p;
	
	select	--max(b.cd_material_exame)
			max(b.nr_sequencia)
	into	--cd_material_exame_w
			nr_seq_material_w
	from	material_exame_lab_int a,
			material_exame_lab b
	where	a.cd_equipamento = cd_equipamento_w
	and		a.cd_material_integracao = cd_material_integracao_p
	and		a.nr_seq_material = b.nr_sequencia;
	
	if	(cd_equipamento_w is null) then
		cd_erro_p	:= 2.1;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280084,null);
	elsif	(ie_existe_prescricao_w = 'N') then
		cd_erro_p	:= 2.2;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280087,'NR_PRESCRICAO='||nr_prescricao_p);
	elsif	(nr_seq_material_w is null) then
		cd_erro_p	:= 2.3;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280088,'CD_MATERIAL_INTEGRACAO='||cd_material_integracao_p);
	end if;
	
	if	(cd_erro_p = 0) then
			
		select	max(a.cd_estabelecimento)
		into	cd_estabelecimento_w
		from	prescr_medica a
		where	a.nr_prescricao = nr_prescricao_p;
			
		select	nvl(max(ie_gerar_padrao_grupo_imp),'N')
		into 	ie_grupo_imp_amostra_seq_w
		from 	lab_parametro
		where 	cd_estabelecimento = cd_estabelecimento_w;
		
		select	max(b.nr_seq_grupo),
				max(b.nr_seq_grupo_imp)
		into	nr_seq_grupo_w,
				nr_seq_grupo_imp_w
		from	prescr_procedimento a,
				exame_laboratorio b
		where	a.nr_seq_exame = b.nr_seq_exame
		and		a.nr_prescricao = nr_prescricao_p
		and		a.nr_sequencia = nr_seq_prescr_p;
		
		select	max(a.nr_sequencia)
		into	nr_seq_prescr_proc_mat_w
		from	prescr_proc_material a
		where	a.nr_prescricao = nr_prescricao_p		
		and 	(((nr_seq_grupo   = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or 
				 ((nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S')));		
		
		
		if	(nr_seq_prescr_proc_mat_w is null) then			
			begin
			select 	prescr_proc_material_seq.nextval
			into	nr_seq_prescr_proc_mat_w
			from 	dual;
			
			select 	nvl(max(nr_seq_int_prescr),0) + 1
			into	nr_seq_int_prescr_w
			from	prescr_proc_material
			where	nr_prescricao = nr_prescricao_p;
			
			insert into prescr_proc_material(nr_sequencia,
						nr_prescricao,
						nr_seq_material,
						qt_volume,
						dt_atualizacao,
						qt_tempo,
						qt_minuto,
						nm_usuario,
						dt_coleta,
						nr_seq_grupo,
						nr_amostra,
						ie_amostra,
						--nr_seq_frasco,
						nr_seq_grupo_imp,
						nr_seq_int_prescr,
						cd_barras)
				values	( nr_seq_prescr_proc_mat_w,
						nr_prescricao_p,
						nr_seq_material_w,
						qt_volume_p,
						sysdate,
						qt_tempo_p,
						0,
						nm_usuario_p,
						dt_coleta_p,
						nr_seq_grupo_w,
						nr_amostra_p,
						ie_amostra_p,
						--nr_seq_frasco_w,
						nr_seq_grupo_imp_w,
						nr_seq_int_prescr_w,
						cd_barras_p
						);
						
			commit;
			
			exception
			when others then
				cd_erro_p	:= 3;
				ds_erro_p	:= substr(sqlerrm,1,2000);
			end;		
		else
			begin
			
			update	prescr_proc_material
			set	nr_seq_material = nr_seq_material_w,
				qt_volume = qt_volume_p,
				qt_tempo = qt_tempo_p,
				dt_coleta = dt_coleta_p,					
				nr_amostra = nr_amostra_p,
				nm_usuario = nm_usuario_p,
				cd_barras = cd_barras_p
			where	nr_sequencia = nr_seq_prescr_proc_mat_w;
			
			commit;
			
			exception
			when others then
				cd_erro_p	:= 4;
				ds_erro_p	:= substr(sqlerrm,1,2000);
			end;		
			
		end if;
		
		nr_seq_grupo_mat_p	:= nr_seq_prescr_proc_mat_w;
		
	end if;
	
end if;

end ws_registrar_prescr_proc_mat;
/