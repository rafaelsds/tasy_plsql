create or replace
procedure obter_guia_conv_atend(	nr_atendimento_p			number,
					cd_convenio_p			number,
					cd_categoria_p			varchar2,
					ie_tipo_atendimento_p		number,
					cd_estabelecimento_p		number,
					ie_tipo_guia_p			varchar2,
					nr_doc_convenio_p	out	varchar2,
					cd_procedimento_p		number default null,
					ie_origem_proced_p		number default null) is

nr_doc_convenio_w	varchar2(20);
ie_forma_w		varchar2(15);
ds_adicional_w		varchar2(20);
ie_tipo_atendimento_w	number(03,0);
cd_estabelecimento_w	number(05,0);
nr_inicial_w		number(15,0);
nr_final_w		number(15,0);
nr_atual_w		number(15,0);
nr_sequencia_w		number(15,0);
ie_tipo_convenio_w		number(2);
nr_doc_mascara_w		varchar2(20) := null;
ds_mascara_w		varchar2(40) := null;
cd_perfil_comunic_w	number(5);
nr_doc_conv_comunic_w	varchar2(20);
ds_convenio_w		varchar2(255);
ds_tipo_convenio_w	varchar2(100);
ds_tipo_atendimento_w	varchar2(100);
ie_clinica_w		number(5);
nr_seq_classificacao_w	number(10);
cd_empresa_w		number(10);
ie_gerar_procedimento_w	varchar2(1);

cd_area_procedimento_w	number(15);
cd_especialidade_w      number(15);
cd_grupo_proc_w         number(15);

qt_regra_guia_proc_w	number(10);
ie_aplica_regra_proc_w	varchar2(1);

cursor c01 is
	select nr_sequencia,
		ie_forma,
		ds_adicional,
		nr_inicial,
		nr_final,
		nr_atual,
		ds_mascara,
		cd_perfil_comunic,
		nr_doc_conv_comunic,
		ie_gerar_procedimento
	from 	convenio_regra_guia
	where	nvl(cd_convenio,cd_convenio_p)			= cd_convenio_p
	and	(nvl(cd_categoria,cd_categoria_p) 		= cd_categoria_p or cd_categoria_p is null)
	and 	nvl(cd_perfil_filtro, nvl(obter_perfil_ativo,0)) 		= nvl(obter_perfil_ativo,0)
	and	nvl(ie_tipo_convenio, nvl(ie_tipo_convenio_w,0)) 		= nvl(ie_tipo_convenio_w,0)
	and	nvl(cd_estabelecimento,cd_estabelecimento_w)		= cd_estabelecimento_w
	and	nvl(ie_tipo_atendimento, ie_tipo_atendimento_w)  		= ie_tipo_atendimento_w
	and	nvl(ie_tipo_guia,nvl(ie_tipo_guia_p,'0'))			= nvl(ie_tipo_guia_p,'0')
	and 	nvl(ie_clinica,nvl(ie_clinica_w,0))                     		= nvl(ie_clinica_w,0)
	and	nvl(nr_seq_classificacao,nvl(nr_seq_classificacao_w,0))	= nvl(nr_seq_classificacao_w,0)
	and	nvl(ie_situacao,'A')		= 'A'
	and	(nvl(cd_empresa,nvl(cd_empresa_w,0)) = nvl(cd_empresa_w,0))
	order by nvl(cd_estabelecimento,0),
		nvl(cd_convenio,0),
		nvl(cd_categoria,'0'),
		nvl(ie_tipo_atendimento,0),
		nvl(cd_perfil_filtro,0),
		nvl(ie_clinica,0),
		nvl(ie_tipo_convenio,0),
		nvl(ie_tipo_guia,'0'),
		nvl(nr_seq_classificacao,0),
		nvl(cd_empresa,0);





begin

nr_doc_convenio_w	:= '';
ie_forma_w		:= '';
ie_aplica_regra_proc_w  := 'N'; 

select	max(cd_empresa)
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento = nvl(cd_estabelecimento_p,wheb_usuario_pck.get_cd_estabelecimento);


begin
select	nvl(max(ie_tipo_atendimento),0),
	nvl(max(cd_estabelecimento),cd_estabelecimento_p),
	nvl(max(ie_tipo_convenio),0),
	nvl(max(ie_clinica),0),
	nvl(max(nr_seq_classificacao),0)
into	ie_tipo_atendimento_w,
	cd_estabelecimento_w,
	ie_tipo_convenio_w,
	ie_clinica_w,
	nr_seq_classificacao_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;
exception
	when no_data_found then
		ie_tipo_atendimento_w	:= ie_tipo_atendimento_p;
		cd_estabelecimento_w	:= cd_estabelecimento_p;
end;

open c01;
loop
	fetch c01 into
		nr_sequencia_w,
		ie_forma_w,
		ds_adicional_w,
		nr_inicial_w,
		nr_final_w,
		nr_atual_w,
		ds_mascara_w,
		cd_perfil_comunic_w,
		nr_doc_conv_comunic_w,
		ie_gerar_procedimento_w;
	exit 	when c01%notfound;
	begin
	
	ie_forma_w		:= ie_forma_w;
	ds_mascara_w		:= ds_mascara_w;
	nr_sequencia_w		:= nr_sequencia_w;
	ie_gerar_procedimento_w	:= ie_gerar_procedimento_w;

	end;
end loop;
close c01;

if 	(nvl(cd_procedimento_p,0) > 0) and
	(nvl(ie_origem_proced_p,0) > 0) and
	(nvl(ie_gerar_procedimento_w,'N') = 'S') then
	
	select	count(*)
	into	qt_regra_guia_proc_w
	from 	convenio_regra_guia_proc
	where	nr_seq_conv_guia = nr_sequencia_w;
	
	if	(qt_regra_guia_proc_w > 0) then
	
		select	cd_grupo_proc,
			cd_especialidade,
			cd_area_procedimento
		into	cd_grupo_proc_w,
			cd_especialidade_w,
			cd_area_procedimento_w
		from	estrutura_procedimento_v
		where	cd_procedimento 	= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p;
		
		select	count(*)
		into	qt_regra_guia_proc_w
		from	convenio_regra_guia_proc
		where  ((cd_procedimento is null) or (nvl(ie_origem_proced, nvl(ie_origem_proced_p,0))		= nvl(ie_origem_proced_p,0)))
		and	nvl(cd_procedimento, nvl(cd_procedimento_p,0))			= nvl(cd_procedimento_p,0)
		and     nvl(cd_area_procedimento, nvl(cd_area_procedimento_w,0))	= nvl(cd_area_procedimento_w,0)
		and	nvl(cd_especialidade, nvl(cd_especialidade_w,0))		= nvl(cd_especialidade_w,0)
		and	nvl(cd_grupo_proc, nvl(cd_grupo_proc_w,0))			= nvl(cd_grupo_proc_w,0);
						
		if	(qt_regra_guia_proc_w > 0) then
			ie_aplica_regra_proc_w := 'S';
		else
			ie_aplica_regra_proc_w := 'N';
		end if;
	end if;
end if;



ie_forma_w	:= nvl(ie_forma_w,'N');

if	(nvl(cd_procedimento_p,0) > 0) and
	(nvl(ie_origem_proced_p,0) > 0) and
	(nvl(ie_gerar_procedimento_w,'N') = 'S') and
	(ie_aplica_regra_proc_w = 'N') then
	
	nr_doc_convenio_w := null;
	
elsif   (((nvl(cd_procedimento_p,0) = 0) and
	 (nvl(ie_origem_proced_p,0) = 0)) or
	((nvl(cd_procedimento_p,0) > 0) and
	(nvl(ie_origem_proced_p,0) > 0) and
	(nvl(ie_gerar_procedimento_w,'N') = 'S') and
	(ie_aplica_regra_proc_w = 'S'))) then
	

	if	(ie_forma_w = 'A') then
		nr_doc_convenio_w	:= nr_atendimento_p;
	elsif	(ie_forma_w = 'AV') then
		nr_doc_convenio_w	:= nr_atendimento_p || ds_adicional_w;
	elsif	(ie_forma_w = 'VA') then
		nr_doc_convenio_w	:= ds_adicional_w || nr_atendimento_p;
	elsif	(ie_forma_w = 'U6AV') then
		nr_doc_convenio_w	:= substr(nr_atendimento_p, length(nr_atendimento_p) - 5,6) || ds_adicional_w;
	elsif	(ie_forma_w in ('IN','INV','VIN')) and
		(nr_atual_w < nr_final_w) then
		begin
		if	(nr_atual_w < nr_inicial_w) then
			nr_atual_w	:= nr_inicial_w;
		else
			nr_atual_w	:= nr_atual_w + 1;
		end if;
		nr_doc_convenio_w	:= nr_atual_w;
		update convenio_regra_guia
		set	nr_atual	= nr_atual_w
		where	nr_sequencia	= nr_sequencia_w;
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
		if	(ie_forma_w = 'INV') then
			nr_doc_convenio_w	:= nr_doc_convenio_w||ds_adicional_w;
		end if;
		if	(ie_forma_w = 'VIN') then
			nr_doc_convenio_w	:= ds_adicional_w || nr_doc_convenio_w;
		end if;
		end;
	elsif	(ie_forma_w = 'SV') then
		nr_doc_convenio_w	:= substr(ds_adicional_w, 1, 20);
	end if;

end if;


/*Francisco - 15/06/07 - OS 59173 */
if	(nr_doc_convenio_w = nr_doc_conv_comunic_w) and
	(cd_perfil_comunic_w is not null) then
	begin
	select	substr(obter_nome_convenio(cd_convenio),1,60),
		substr(obter_valor_dominio(11,ie_tipo_convenio),1,100),
		substr(obter_valor_dominio(12,ie_tipo_atendimento),1,100)
	into	ds_convenio_w,
		ds_tipo_convenio_w,
		ds_tipo_atendimento_w
	from	convenio_regra_guia
	where	nr_sequencia	= nr_sequencia_w;

	gerar_comunic_padrao(sysdate,
			substr(obter_texto_tasy(303985, wheb_usuario_pck.get_nr_seq_idioma),1,255), 
			obter_texto_dic_objeto(303987, wheb_usuario_pck.get_nr_seq_idioma, 'DS_CONVENIO_W='||ds_convenio_w|| ';DS_TIPO_CONVENIO_W='||ds_tipo_convenio_w || ';DS_TIPO_ATENDIMENTO_W='||ds_tipo_atendimento_w),
			'Tasy',
			'N',
			null,
			'N',
			null,
			to_char(cd_perfil_comunic_w) || ',',
			cd_estabelecimento_p,
			null,
			sysdate,
			null,
			null);
	exception
		when others then
			null;
	end;

end if;

/*Francisco - 11/06/07 - OS 58581 */
if	(ds_mascara_w is not null) then
	begin
	select	trim(to_char(somente_numero(nr_doc_convenio_w),ds_mascara_w))
	into	nr_doc_mascara_w
	from	dual;
	exception
		when others then
		nr_doc_mascara_w := null;
	end;

	if	(nr_doc_mascara_w is not null) then
		nr_doc_convenio_w	:= nr_doc_mascara_w;
	end if;
end if;

nr_doc_convenio_p		:= nr_doc_convenio_w;

end;
/
