create or replace
procedure duplicar_curriculo
			(	nr_seq_curriculo_p	number,	
				nm_usuario_p		Varchar2) is 

nr_seq_curriculo_w	number(10);
/* pf_curriculo_formacao */
ie_formacao_curso_w	varchar2(1);
ie_formacao_w		varchar2(15);
ds_instituicao_w	varchar2(80);
ds_curso_w		varchar2(80);
cd_ano_w		number(4);
ds_observacao_w		varchar2(4000);
/* pf_curriculo_prof */
nm_empresa_w		varchar2(80);
ds_cidade_w		varchar2(80);
sg_estado_w		valor_dominio.vl_dominio%type;
dt_entrada_w		date;
dt_saida_w		date;
ds_cargo_w		varchar2(4000);
ds_atividade_w		varchar2(4000);
/* pf_curriculo_idioma */
nr_seq_idioma_w		number(10);
ie_escrita_w		varchar2(15);
ie_leitura_w		varchar2(15);
ie_conversacao_w	varchar2(15);
/* pf_curriculo_inf_adic */
nr_seq_inf_w		number(10);
ds_informacao_w		varchar2(32000);
nr_seq_apres_w		number(10);


Cursor C01 is
	select	ie_formacao_curso,
		ie_formacao,
		ds_instituicao,
		ds_curso,
		cd_ano,
		ds_observacao
	from	pf_curriculo_formacao
	where	nr_seq_curriculo = nr_seq_curriculo_p;
	
Cursor C02 is
	select	nm_empresa,
		ds_cidade,
		sg_estado,
		dt_entrada,
		dt_saida,
		ds_cargo,
		ds_atividade,
		ds_observacao
	from	pf_curriculo_prof
	where	nr_seq_curriculo = nr_seq_curriculo_p;

Cursor C03 is
	select	nr_seq_idioma,
		ie_escrita,
		ie_leitura,
		ie_conversacao
	from	pf_curriculo_idioma
	where	nr_seq_curriculo = nr_seq_curriculo_p;

Cursor C04 is
	select	nr_seq_inf,
		ds_informacao,
		nr_seq_apres
	from	pf_curriculo_inf_adic
	where	nr_seq_curriculo = nr_seq_curriculo_p;
begin

select	pf_curriculo_seq.nextval
into	nr_seq_curriculo_w
from	dual;

insert	into pf_curriculo (	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_curriculo,
				cd_pessoa_fisica,
				dt_liberacao,
				ie_situacao)
			(select	nr_seq_curriculo_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				dt_curriculo,
				cd_pessoa_fisica,
				null,
				'A'
			from	pf_curriculo
			where	nr_sequencia	= nr_seq_curriculo_p);

open C01;
loop
fetch C01 into	
	ie_formacao_curso_w,
	ie_formacao_w,
	ds_instituicao_w,
	ds_curso_w,
	cd_ano_w,
	ds_observacao_w;
exit when C01%notfound;
	begin
	insert into pf_curriculo_formacao (	nr_sequencia,
						nr_seq_curriculo,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_formacao_curso,
						ie_formacao,
						ds_instituicao,
						ds_curso,
						cd_ano,
						ds_observacao)
					values(	pf_curriculo_formacao_seq.nextval,
						nr_seq_curriculo_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						ie_formacao_curso_w,
						ie_formacao_w,
						ds_instituicao_w,
						ds_curso_w,
						cd_ano_w,
						ds_observacao_w);
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nm_empresa_w,
	ds_cidade_w,
	sg_estado_w,
	dt_entrada_w,
	dt_saida_w,
	ds_cargo_w,
	ds_atividade_w,
	ds_observacao_w;
exit when C02%notfound;
	begin
	insert into pf_curriculo_prof(	NR_SEQUENCIA,
					NR_SEQ_CURRICULO,
					DT_ATUALIZACAO,
					NM_USUARIO,
					DT_ATUALIZACAO_NREC,
					NM_USUARIO_NREC,
					NM_EMPRESA,
					DS_CIDADE,
					SG_ESTADO,
					DT_ENTRADA,
					DT_SAIDA,
					DS_CARGO,
					DS_ATIVIDADE,
					DS_OBSERVACAO)
				values(	pf_curriculo_prof_seq.NextVal,
					nr_seq_curriculo_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nm_empresa_w,
					ds_cidade_w,
					sg_estado_w,
					dt_entrada_w,
					dt_saida_w,
					ds_cargo_w,
					ds_atividade_w,
					ds_observacao_w);
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_idioma_w,
	ie_escrita_w,
	ie_leitura_w,
	ie_conversacao_w;
exit when C03%notfound;
	begin
	insert into pf_curriculo_idioma	(nr_sequencia,
					nr_seq_curriculo,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_idioma,
					ie_escrita,
					ie_leitura,
					ie_conversacao)
				values(	pf_curriculo_idioma_seq.NextVal,
					nr_seq_curriculo_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_idioma_w,
					ie_escrita_w,
					ie_leitura_w,
					ie_conversacao_w);
	end;
end loop;
close C03;

open C04;
loop
fetch C04 into	
	nr_seq_inf_w,
	ds_informacao_w,
	nr_seq_apres_w;
exit when C04%notfound;
	begin
	insert into pf_curriculo_inf_adic(	NR_SEQUENCIA,
						NR_SEQ_CURRICULO,
						DT_ATUALIZACAO,
						NM_USUARIO,
						DT_ATUALIZACAO_NREC,
						NM_USUARIO_NREC,
						NR_SEQ_INF,
						DS_INFORMACAO,
						nr_seq_apres)
					values(	pf_curriculo_inf_adic_seq.Nextval,
						nr_seq_curriculo_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_inf_w,
						ds_informacao_w,
						nr_seq_apres_w);
	end;
end loop;
close C04;

/*update	pf_curriculo
set	ie_situacao	= 'I'
where	nr_sequencia	= nr_seq_curriculo_p;*/

commit;

end duplicar_curriculo;
/