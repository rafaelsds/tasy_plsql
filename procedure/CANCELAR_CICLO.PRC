create or replace
procedure cancelar_ciclo(
		nr_seq_motivo_p	number,
		nr_seq_ciclo_p	number,	
		nm_usuario_p	Varchar2) is
begin
if	(nr_seq_motivo_p is not null) and
	(nr_seq_ciclo_p is not null) then
	begin

	update 	cm_ciclo
	set 	nr_seq_motivo_cancel = nr_seq_motivo_p
	where 	nr_sequencia = nr_seq_ciclo_p;
	
	end;
end if;
commit;
end cancelar_ciclo;
/