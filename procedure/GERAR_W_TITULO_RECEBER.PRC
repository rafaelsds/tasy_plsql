create or replace
procedure GERAR_W_TITULO_RECEBER(dt_referencia_p	date,
				cd_convenio_p		varchar2,
				ie_situacao_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				ie_cobranca_p		varchar2,
				ie_analise_p		varchar2,
				ie_glosa_dev_p		varchar2,
				ie_glosa_ind_c_lote_p	varchar2,
				ie_glosa_ind_s_lote_p	varchar2,
				ie_vencimento_p		varchar2,
				ie_baixa_manual_p	varchar2) is

cd_convenio_w		number(10);
cd_estabelecimento_w	number(4);
dt_pagamento_previsto_w	date;
nr_atendimento_w	number(10);
nr_interno_conta_w	number(10);
nr_seq_protocolo_w	number(10);
nr_titulo_w		number(10);
vl_conta_w		number(15,2);
vl_pendente_w		number(15,2);
nr_seq_item_ret_w	number(10);
nm_paciente_w		varchar2(60);
cd_autorizacao_w	varchar2(20);
cd_autoriza_w		varchar2(20);
vl_guia_w		number(15,2);
vl_amenor_w		number(15,2);
vl_glosa_w		number(15,2);
ie_acao_w		number(1);
ie_lote_recurso_w	varchar2(1);
qt_retornos_w		number(5);
nr_seq_ret_item_w	number(10);
vl_pago_w		number(15,2);
vl_glosado_w		number(15,2);
vl_saldo_guia_w		number(15,2);
dt_baixa_ret_w		date;
dt_baixa_lote_w		date;

vl_tot_itens_glosados_w	number(15,2);
vl_baixado_w		number(15,2);
vl_tot_baixado_w	number(15,2);
ie_status_w		varchar2(3);
vl_glosa_ret_w		number(15,2);
vl_amenor_ret_w		number(15,2);
ds_quebra_w		varchar2(255);

vl_tot_saldo_guia_w	number(15,2);

ds_lista_convenio_w	varchar2(4000);
cont_w			number(10);
ds_nao_inf_w		varchar2(255);
ds_em_cob_w		varchar2(255);

/* Contas pendentes */
Cursor c01 is
select	b.nr_interno_conta,
	a.nr_titulo,
	a.dt_pagamento_previsto,
	b.cd_convenio_parametro,
	b.nr_atendimento,
	b.nr_seq_protocolo,
	b.vl_conta,
	obter_pessoa_atendimento(b.nr_atendimento,'N')
from	conta_paciente b,
	titulo_receber a
where	b.nr_interno_conta	= a.nr_interno_conta
and	a.nr_seq_protocolo is null
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	((dt_liquidacao is null) or (dt_liquidacao > trunc(dt_referencia_p,'dd')))
AND	((ds_lista_convenio_w LIKE '% ' || nvl(a.cd_convenio_conta, obter_convenio_tit_rec(a.nr_titulo)) || ' %') OR cd_convenio_p IS NULL)
and	((a.dt_pagamento_previsto >= trunc(dt_referencia_p,'dd') and ie_vencimento_p = 'A') or
	 (a.dt_pagamento_previsto < trunc(dt_referencia_p,'dd') and ie_vencimento_p = 'V') or
	  ie_vencimento_p = 'T')
union all
select	b.nr_interno_conta,
	a.nr_titulo,
	a.dt_pagamento_previsto,
	b.cd_convenio_parametro,
	b.nr_atendimento,
	b.nr_seq_protocolo,
	b.vl_conta,
	obter_pessoa_atendimento(b.nr_atendimento,'N')
from	conta_paciente b,
	titulo_receber a
where	b.nr_seq_protocolo	= a.nr_seq_protocolo
and	a.nr_interno_conta is null
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	((dt_liquidacao is null) or (dt_liquidacao > trunc(dt_referencia_p,'dd')))
AND	((ds_lista_convenio_w LIKE '% ' || nvl(a.cd_convenio_conta, obter_convenio_tit_rec(a.nr_titulo)) || ' %') OR cd_convenio_p IS NULL)
and	((a.dt_pagamento_previsto >= trunc(dt_referencia_p,'dd') and ie_vencimento_p = 'A') or
	 (a.dt_pagamento_previsto < trunc(dt_referencia_p,'dd') and ie_vencimento_p = 'V') or
	  ie_vencimento_p = 'T')
order by
	nr_titulo;

/* Guias das contas pendentes */
Cursor c02 is
select	a.cd_autorizacao,
	sum(a.vl_guia)
from	conta_paciente_guia a
where	a.nr_interno_conta	= nr_interno_conta_w
group by
	a.cd_autorizacao
union all
select	ds_nao_inf_w,
	a.vl_conta
from	conta_paciente a
where	a.nr_interno_conta	= nr_interno_conta_w
and	not exists	(select	1
			from	conta_paciente_guia x
			where	x.nr_interno_conta	= a.nr_interno_conta);

/* Retorno das guias pendentes */
Cursor c03 is
select	a.nr_sequencia,
	nvl(b.dt_baixa_cr,b.dt_fechamento),
	nvl(a.vl_glosado,0),
	nvl(a.vl_amenor,0)
from	convenio_retorno b,
	convenio_retorno_item a
where	b.nr_sequencia	 			= a.nr_seq_retorno
and	a.nr_interno_conta			= nr_interno_conta_w
and	nvl(a.cd_autorizacao,ds_nao_inf_w)	= cd_autorizacao_w
and	trunc(nvl(b.dt_baixa_cr,b.dt_fechamento),'dd')	<= dt_referencia_p
order by
	b.dt_fechamento;

/*glosas/ historicos */
Cursor c04 is
select	c.ie_acao,
	sum(a.vl_glosa),
	decode(b.nr_seq_lote_recurso,null,'N','S') ie_lote_recurso
from	hist_audit_conta_paciente c,
	conta_paciente_ret_hist b,
	convenio_retorno_glosa a
where	c.nr_sequencia		= b.nr_seq_hist_audit
and	b.nr_sequencia		= a.nr_seq_conpaci_ret_hist
and	b.nr_seq_lote_audit	is null
and	a.nr_seq_ret_item	= nr_seq_ret_item_w
having	count(*) > 0
group by
	c.ie_acao,
	decode(b.nr_seq_lote_recurso,null,'N','S');

begin

ds_lista_convenio_w	:= ' ' || replace(replace(replace(cd_convenio_p,'(',''),')',''),',',' ') || ' ';

ds_nao_inf_w 	:= substr(wheb_mensagem_pck.get_texto(298640),1,255);
ds_em_cob_w	:= substr(wheb_mensagem_pck.get_texto(298657),1,255);
	
delete	from	w_titulo_receber
where	nm_usuario = nm_usuario_p;

vl_tot_saldo_guia_w	:= 0;

open c01;
loop
fetch c01 into
	nr_interno_conta_w,
	nr_titulo_w,
	dt_pagamento_previsto_w,
	cd_convenio_w,
	nr_atendimento_w,
	nr_seq_protocolo_w,
	vl_conta_w,
	nm_paciente_w;
exit when c01%notfound;

	open c02;
	loop
	fetch c02 into
		cd_autorizacao_w,
		vl_guia_w;
	exit when c02%notfound;

		vl_saldo_guia_w	:= vl_guia_w;

		vl_tot_itens_glosados_w	:= 0;
		vl_tot_baixado_w	:= 0;

		nr_seq_ret_item_w	:= null;

		/* Baixas */
		open c03;
		loop
		fetch c03 into
			nr_seq_ret_item_w,
			dt_baixa_ret_w,
			vl_glosa_ret_w,
			vl_amenor_ret_w;
		exit when c03%notfound;

			select	nvl(sum(a.vl_recebido + nvl(a.vl_descontos,0) + nvl(a.vl_perdas,0) + nvl(a.vl_glosa,0)),0)
			into	vl_baixado_w
			from	titulo_receber_liq a
			where	a.nr_seq_ret_item		= nr_seq_ret_item_w
			and	trunc(a.dt_recebimento,'dd')	<= dt_referencia_p;
				

			vl_tot_baixado_w	:= vl_tot_baixado_w + vl_baixado_w;

		end loop;
		close c03;
		
		if	(ie_baixa_manual_p = 'S') then
			select	nvl(sum(b.vl_recebido + nvl(b.vl_descontos,0) + nvl(b.vl_perdas,0) + nvl(b.vl_glosa,0)),0)
			into	vl_baixado_w
			from	titulo_receber_liq b,
				titulo_receber a
			where	a.nr_titulo	= b.nr_titulo
			and	a.nr_interno_conta		= nr_interno_conta_w
			and	trunc(b.dt_recebimento,'dd')	<= dt_referencia_p
			and	b.nr_seq_ret_item is null
			and	'S'				= ie_baixa_manual_p;
			
			vl_tot_baixado_w	:= vl_tot_baixado_w + vl_baixado_w;
		end if;	
		/* So buscar do ultimo retorno */
		open c04;
		loop
		fetch c04 into
			ie_acao_w,
			vl_glosa_w,
			ie_lote_recurso_w;
		exit when c04%notfound;
			ie_status_w	:= null;
			ds_quebra_w	:= substr(wheb_mensagem_pck.get_texto(298644),1,255);	-- Outros históricos

			/* Nao auditado */
			if	(ie_acao_w	= 0) then
				ie_status_w	:= 'ANA';
				ds_quebra_w	:= substr(wheb_mensagem_pck.get_texto(298649),1,255); -- Análise de Glosa
			/* Glosa devida */
			elsif	(ie_acao_w	= 3) then
				ie_status_w	:= 'DEV';
				ds_quebra_w	:= substr(wheb_mensagem_pck.get_texto(298651),1,255); -- Glosa Devida
			/* Glosa indevida */
			elsif	(ie_acao_w	= 4) then
				if	(ie_lote_recurso_w = 'N') then
					ie_status_w	:= 'INS';
					ds_quebra_w	:= substr(wheb_mensagem_pck.get_texto(298653),1,255); -- Glosa Indevida Sem Lote na Auditoria
				else
					ie_status_w	:= 'INC';
					ds_quebra_w	:= substr(wheb_mensagem_pck.get_texto(298655),1,255); -- Glosa Indevida Com Lote na Auditoria
				end if;
			elsif	(ie_acao_w	= 1) then
				ie_status_w	:= 'REC';
				ds_quebra_w	:= substr(wheb_mensagem_pck.get_texto(298656),1,255); -- Recebimento
			end if;


			vl_tot_itens_glosados_w	:= vl_tot_itens_glosados_w + vl_glosa_w;

			
			if	(ie_status_w = 'ANA' and ie_analise_p = 'S') or
				(ie_status_w = 'DEV' and ie_glosa_dev_p = 'S') or
				(ie_status_w = 'INS' and ie_glosa_ind_s_lote_p = 'S') or
				(ie_status_w = 'INC' and ie_glosa_ind_c_lote_p = 'S') or
				(ie_status_w is null) then
				
				insert	into	w_titulo_receber
					(nr_sequencia,
					cd_estabelecimento,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					cd_convenio,
					nr_atendimento,
					nr_interno_conta,
					vl_conta,
					vl_pendente,
					nr_seq_protocolo,
					nm_paciente,
					cd_autorizacao,
					dt_vencimento,
					ie_status,
					nr_titulo,
					ds_quebra)
				values	(w_titulo_receber_seq.nextval,
					cd_estabelecimento_p,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					cd_convenio_w,
					nr_atendimento_w,
					nr_interno_conta_w,
					vl_guia_w,
					vl_glosa_w,
					nr_seq_protocolo_w,
					nm_paciente_w,
					cd_autorizacao_w,
					dt_pagamento_previsto_w,
					ie_status_w,
					nr_titulo_w,
					ds_quebra_w);
			end if;
		end loop;
		close c04;

		vl_saldo_guia_w	:= vl_guia_w - vl_tot_baixado_w - vl_tot_itens_glosados_w;

		/* Insere saldo da guia */

		if	(vl_saldo_guia_w <> 0) and
			(ie_cobranca_p = 'S') then
			
			insert	into	w_titulo_receber
				(nr_sequencia,
				cd_estabelecimento,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				cd_convenio,
				nr_atendimento,
				nr_interno_conta,
				vl_conta,
				vl_pendente,
				nr_seq_protocolo,
				nm_paciente,
				cd_autorizacao,
				dt_vencimento,
				ie_status,
				nr_titulo,
				ds_quebra)
			values	(w_titulo_receber_seq.nextval,
				cd_estabelecimento_p,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				cd_convenio_w,
				nr_atendimento_w,
				nr_interno_conta_w,
				vl_guia_w,
				vl_saldo_guia_w,
				nr_seq_protocolo_w,
				nm_paciente_w,
				cd_autorizacao_w,
				dt_pagamento_previsto_w,
				'COB',
				nr_titulo_w,
				ds_em_cob_w);
		end if;

	end loop;
	close c02;

	commit;
	
end loop;
close c01;

commit;

end GERAR_W_TITULO_RECEBER;
/
