create or replace
procedure CHECK_DIC_OBJ_DEPEND
		(nr_seq_dic_objeto_p	number,
		ie_action_p		varchar2)
		is
	
ds_sql_w		varchar2(10000);
ds_comando_w		varchar2(10000);
nm_tabela_visao_w	tabela_visao.nm_tabela%type;
ie_ok_w			boolean := true;

--get dependencies from temp_view
cursor c01 is
select 	referenced_name, 
	referenced_type
from 	user_dep_mv
where 	name = 'SIS_SQL_CHECK_DEP_V'
and	referenced_type <> 'SYNONYM'
group by referenced_name, 
	referenced_type;

c01_w	c01%rowtype;

	function remove_bind_param(ds_sql_p in varchar2) return varchar2 is
	
	ds_comando_w 		varchar2(10000);
	ds_comando_p 		varchar2(10000);
	nm_param_w		varchar2(50);
	pos_w			number(10);
	ds_caracter_w		varchar2(1);
	contador_w		number(10) 	:= 1;
	caracteres_validos_w	varchar2(60)  :='abcdefghijklmnopqrstuvxywzABCDEFGHIJKLMNOPRQSTUVXYWZ_';
	ie_valido_w		boolean;
	nr_seq_w		number(10) := 1;
	
	type paramarray 	is varray(30) of varchar2(30);
	param_list 		paramarray := paramarray();

	begin
	
	if	( InStr(Upper(ds_sql_p), 'SELECT') > 0 ) then
	
		select	upper(REPLACE(REPLACE(Upper(ds_sql_p),'@RESTRICAO', ' '),'@SQL_WHERE',' '))
		into 	ds_comando_p
		from 	dual;
		
		ds_comando_w 	:= ds_comando_p;
		
		pos_w := instr(ds_comando_w,':');

		while	( pos_w > 0 ) and ( contador_w < 300 ) loop
			ds_comando_w := substr(ds_comando_w,pos_w+1,length(ds_comando_w));
			nm_param_w 	:= '';
			ie_valido_w	:= true;
			pos_w 		:= 1;
			
			--Verifica se o parametro eh valido para os casos em que o dois pontos esta em um texto
			if	(instr(caracteres_validos_w,substr(ds_comando_w,pos_w+1,1)) = 0) then
				ie_valido_w := false;
			end if;
			
			while ( ie_valido_w ) and
				( pos_w < length(ds_comando_p)) loop  /*Variavel de controle para evitar LOOP*/
				ds_caracter_w := substr(ds_comando_w,pos_w,1);
				if	( instr(caracteres_validos_w,ds_caracter_w) > 0 ) then
					nm_param_w := nm_param_w || ds_caracter_w;
				else
					ie_valido_w := false;
				end if;
				pos_w := pos_w + 1;
			end loop;
			
			nm_param_w := upper(nm_param_w);
			param_list.extend();
			param_list(nr_seq_w) := nm_param_w;
			nr_seq_w := nr_seq_w + 1;		
			ds_comando_w := substr(ds_comando_w,pos_w,length(ds_comando_w));
			pos_w := instr(ds_comando_w,':');
			contador_w := contador_w + 1;
			
		end loop;

		for i in 1 .. param_list.count loop 
			if	(trim(param_list(i)) is not null) then
				ds_comando_p := replace(ds_comando_p, ':'||param_list(i), '''X''');
			end if;
		end loop;
	end if;
	
	return ds_comando_p;
	
	end remove_bind_param;
	
begin

if	(ie_action_p = 'DELETE') then

	delete 	from dic_obj_db_dependecies
	where	nr_seq_dic_objeto = nr_seq_dic_objeto_p;


elsif	(ie_action_p = 'UPDATE') then

	delete 	from dic_obj_db_dependecies
	where	nr_seq_dic_objeto = nr_seq_dic_objeto_p;
	
	select	max(ds_sql)
	into	ds_sql_w
	from	dic_objeto
	where	nr_sequencia	= nr_seq_dic_objeto_p;
		
	if	( InStr(Upper(ds_sql_w), 'SELECT') > 0 ) then
	
		begin
			execute immediate 'drop view SIS_OBJ_CHECK_DEP_V';
		exception
		when others then
			null;
		end;
		ds_sql_w := remove_bind_param(ds_sql_w);	
		ds_comando_w := ' create or replace view SIS_SQL_CHECK_DEP_V as ' || ds_sql_w;
		
		--Create temp view with the command to map de dependencies
		begin
			execute immediate ds_comando_w;
		exception
		when others then
			ie_ok_w := false;
		end;	

		if	(ie_ok_w) then		
			open c01;
			loop
			fetch c01 into
				c01_w;
			exit when c01%notfound;
				begin				
				insert into dic_obj_db_dependecies
					(nr_seq_dic_objeto,
					referenced_name,
					referenced_type)				
				values	(nr_seq_dic_objeto_p,
					c01_w.referenced_name,
					c01_w.referenced_type);					
				end;
			end loop;
			close c01;
			
			commit;
			
			begin
				execute immediate 'drop view SIS_OBJ_CHECK_DEP_V';
			exception
			when others then
				null;
			end;
		end if;
	end if;

end if;

commit;

end CHECK_DIC_OBJ_DEPEND;
/