create or replace 
procedure OBTER_SENHA_FILA_TRANSFERENCIA (nr_atendimento_p number,
										 ds_retorno_senha out varchar2,
										 ds_retorno_fila out varchar2,
										 ds_retorno_pac_senha_fila out varchar2) is

nr_seq_pac_senha_fila_w     varchar2(10);

begin

select  nr_seq_pac_senha_fila
into    nr_seq_pac_senha_fila_w
from    atendimento_paciente
where   nr_atendimento = nr_atendimento_p;

if (nr_seq_pac_senha_fila_w > 0) then
    
	ds_retorno_pac_senha_fila := nr_seq_pac_senha_fila_w;
	ds_retorno_senha := obter_dados_senha(nr_seq_pac_senha_fila_w, 'CD');
    ds_retorno_fila := obter_dados_senha(nr_seq_pac_senha_fila_w, 'SF');
	
end if;

end obter_senha_fila_transferencia;
/
