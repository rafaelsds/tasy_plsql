create or replace
procedure eme_inserir_atend_hc( nr_seq_regulacao_p	number,
				nr_seq_resposta_p	number,
				qt_km_p			number,	
				ie_opcao_p		number,
				nm_usuario_p		Varchar2) is 

/* ie_opcao_p				
   1 = sa�da
   2 = chegada*/
				
nr_atendimento_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_paciente_w	number(10);
cd_medico_w		varchar2(10);	
nr_seq_atend_hc_w	number(10);		
begin
if	(nr_seq_regulacao_p is not null) and
	(qt_km_p is not null) then
	
	select	max(nr_atendimento),
		max(cd_pessoa_fisica)
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	eme_regulacao
	where	nr_sequencia = nr_seq_regulacao_p;				
	
	select	max(nr_sequencia)
	into	nr_seq_paciente_w
	from	paciente_home_care
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and	((trunc(dt_final) >= trunc(sysdate))
	or	(dt_final is null));
	
	
	if	(ie_opcao_p = 1) then 			
		
		select	max(cd_medico)
		into	cd_medico_w
		from	eme_reg_resposta
		where	nr_sequencia = nr_seq_resposta_p;
		
		if	(nr_seq_paciente_w is not null) then
		
			insert into paciente_hc_atend (	
				nr_sequencia,
				nr_seq_paciente,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atendimento,
				qt_km_inicial,
				nr_atendimento,
				cd_funcionario,
				ie_tipo_atendimento)
			values(	paciente_hc_atend_seq.nextVal,
				nr_seq_paciente_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				qt_km_p,
				nr_atendimento_w,
				cd_medico_w,
				'V');
		end if;
	elsif	(ie_opcao_p = 2) and
		(nr_seq_paciente_w is not null) then		
		
		select	max(nr_sequencia)
		into	nr_seq_atend_hc_w
		from	paciente_hc_atend
		where	nr_seq_paciente = nr_seq_paciente_w;
		
		if	(nr_seq_atend_hc_w is not null) then
			update	paciente_hc_atend
			set	qt_km_final = qt_km_p
			where	nr_sequencia = nr_seq_atend_hc_w;
		end if;		
		
	end if;	
end if;

commit;

end eme_inserir_atend_hc;
/