create or replace
procedure sismama_transf_laudo_atend(	nr_atendimento_p	siscolo_atendimento.nr_atendimento%type,
					nr_sequencia_p		siscolo_atendimento.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					ie_tipo_laudo_p		varchar2) is 

cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_sequencia_w			siscolo_atendimento.nr_sequencia%type := 0;

/* IE_TIPO_LAUDO_P:
C - Citopatológico
H - Histopatológico
*/

begin

begin
select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p
and	rownum = 1;
exception
when others then
	cd_pessoa_fisica_w := 0;
end;

if	(ie_tipo_laudo_p = 'C') then
	begin
	
	begin
	select	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	sismama_atendimento a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	b.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	dt_liberacao is not null
	and	exists (select	1
			from	sismama_cit_anamnese x
			where	x.nr_seq_sismama = a.nr_sequencia
			and 	rownum = 1);
	exception
	when others then
		nr_sequencia_w := 0;
	end;
	
	if	(nr_sequencia_w > 0) then
		begin
		
		insert into sismama_cit_anamnese (	
			nr_sequencia,
			nr_seq_sismama,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_nodulo,
			ie_gravida_amamentando,
			ie_risco_cancer)
		select	sismama_cit_anamnese_seq.nextval,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ie_nodulo,
			ie_gravida_amamentando,
			ie_risco_cancer
		from	sismama_cit_anamnese
		where	nr_seq_sismama = nr_sequencia_w;
		
		insert into sismama_cit_exame_clinico (	
			nr_sequencia,
			nr_seq_sismama,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_descarga_papilar,
			ie_nodulo,
			ie_localizacao,
			ie_material_enviado,
			ie_tipo_material_enviado,
			ie_tumor_residual,
			ie_tumor_solido,
			qt_laminas_enviadas,
			dt_coleta)
		select	sismama_cit_exame_clinico_seq.nextval,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,               
			ie_descarga_papilar,
			ie_nodulo,
			ie_localizacao,
			ie_material_enviado,
			ie_tipo_material_enviado,
			ie_tumor_residual,
			ie_tumor_solido,
			qt_laminas_enviadas,
			dt_coleta
		from	sismama_cit_exame_clinico
		where	nr_seq_sismama = nr_sequencia_w;
		
		commit;
		
		end;
	end if;
	
	end;
elsif	(ie_tipo_laudo_p = 'H') then
	begin
	
	begin
	select	max(a.nr_sequencia)
	into	nr_sequencia_w
	from	sismama_atendimento a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	b.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	dt_liberacao is not null
	and	exists (select	1
			from	sismama_his_dados_clinico x
			where	x.nr_seq_sismama = a.nr_sequencia
			and 	rownum = 1);
	exception
	when others then
		nr_sequencia_w := 0;
	end;
	
	if	(nr_sequencia_w > 0) then
		begin
		
		insert into sismama_his_dados_clinico (	
			dt_atualizacao,         
			dt_atualizacao_nrec,
			dt_coleta,
			ie_caracteristica_lesao,
			ie_deteccao_lesao,
			ie_diagnostico_imagem ,
			ie_gravida_amamentando,
			ie_linfonodo_axilar,
			ie_localizacao,
			ie_material_enviado,
			ie_risco_elevado,
			ie_tamanho,
			ie_tipo_exame,
			ie_tratamento_anterior,
			ie_trat_hormonio,
			ie_trat_mesma_mama,
			ie_trat_outra_mama,
			ie_trat_quimioterapia,
			ie_trat_rad_mesma_mama,
			ie_trat_rad_outra_mama,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_sismama,
			nr_sequencia)
		select	sysdate,         
			sysdate,
			dt_coleta,
			ie_caracteristica_lesao,
			ie_deteccao_lesao,
			ie_diagnostico_imagem ,
			ie_gravida_amamentando,
			ie_linfonodo_axilar,
			ie_localizacao,
			ie_material_enviado,
			ie_risco_elevado,
			ie_tamanho,
			ie_tipo_exame,
			ie_tratamento_anterior,
			ie_trat_hormonio,
			ie_trat_mesma_mama,
			ie_trat_outra_mama,
			ie_trat_quimioterapia,
			ie_trat_rad_mesma_mama,
			ie_trat_rad_outra_mama,
			nm_usuario_p,
			nm_usuario_p,
			nr_sequencia_p,
			sismama_his_dados_clinico_seq.nextval
		from	sismama_his_dados_clinico
		where	nr_seq_sismama = nr_sequencia_w;
		
		commit;
		
		end;
	end if;	
	            
	end;
end if;


end sismama_transf_laudo_atend;
/
