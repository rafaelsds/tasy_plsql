create or replace
procedure ptu_gerar_interface_uniodonto
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is

nr_seq_movimentacao_w		number(10);
dt_mes_referencia_w		date;
cd_ans_w			varchar2(20);
cd_cgc_operadora_w		varchar2(14);
nr_seq_empresa_w		number(10);
nr_seq_arquivo_w		number(10);
nr_seq_beneficiario_w		number(10);
nr_seq_segurado_w		number(10);

cd_pessoa_fisica_w		varchar2(10);
nm_segurado_w			varchar2(255);
dt_nascimento_w			date;
ie_sexo_w			number(1);
nr_pis_pasep_w			varchar2(11);
nm_mae_segurado_w		varchar2(255);
cd_cns_w			varchar2(15);
nr_identidade_w			varchar2(15);
ds_orgao_emissor_ci_w		pessoa_fisica.ds_orgao_emissor_ci%type;
cd_nacionalidade_w		varchar2(8);
nr_seq_titular_w		number(10);
ie_regulamentacao_w		varchar2(2);
cd_plano_ans_w			varchar2(20);
cd_plano_ans_pre_w		varchar2(20);
dt_liberacao_w			date;
ds_logradouro_w			varchar2(50);
ds_numero_w			varchar2(5);
ds_complemento_w		varchar2(15);
ds_bairro_w			varchar2(50);
ds_municipio_w			varchar2(50);
uf_w				varchar2(10);
cep_w				varchar2(15);
dt_mesano_referencia_w		date;
dt_reinclusao_w			date;
nr_seq_portabilidade_w		number(10);
nr_cco_w			number(10);
ie_digito_cco_w			number(2);
cd_cco_w			pls_segurado.cd_cco%type;

cd_pais_sib_w			varchar2(30);
cd_usuario_ant_w		varchar2(30);
cd_usuario_plano_w		varchar2(30);
cd_usuario_plano_tit_w		varchar2(30);
cd_cgc_estipulante_w		varchar2(14);
nr_cpf_w			varchar2(11);
cd_vinculo_benef_w		varchar2(2);
ie_carencia_temp_w		number(1);
ie_resid_brasil_w		number(1);

cd_motivo_w			number(2);
nr_prot_ans_origem_w		varchar2(20);

dt_inclusao_w			date;
dt_exclusao_w			date;

qt_tipo1_w			number(10);
qt_tipo2_w			number(10);
qt_tipo6_w			number(10);
qt_tipo7_w			number(10);
qt_tipo8_w			number(10);
qt_trailer_w			number(10);
nr_seq_reg_arquivo_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	ptu_movimentacao_produto
	where	nr_seq_lote	= nr_seq_lote_p;

Cursor C02 is
	select	nr_sequencia
	from	ptu_mov_produto_empresa
	where	nr_seq_mov_produto	= nr_seq_movimentacao_w;

Cursor C03 is
	select	nr_sequencia,
		nr_seq_segurado
	from	ptu_mov_produto_benef
	where	nr_seq_empresa	= nr_seq_empresa_w;

begin

select	cd_ans,
	cd_cgc_outorgante
into	cd_ans_w,
	cd_cgc_operadora_w
from	pls_outorgante
where	cd_estabelecimento	= cd_estabelecimento_p;

select	dt_mesano_referencia
into	dt_mes_referencia_w
from	ptu_mov_produto_lote
where	nr_sequencia	= nr_seq_lote_p;

open C01;
loop
fetch C01 into
	nr_seq_movimentacao_w;
exit when C01%notfound;
	begin

	select 	nvl(max(nr_seq_arquivo),0) + 1
	into	nr_seq_arquivo_w
	from	w_ptu_movto_produto;

	nr_seq_reg_arquivo_w	:= 1;

	insert into w_ptu_movto_produto
		(	nr_sequencia,nr_seq_reg_arquivo,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			ie_tipo_reg,dt_geracao_arquivo,cd_ans,cd_cgc,ds_constante,
			ds_modalidade,nr_seq_arquivo,nr_seq_lote_movto,nr_seq_movimentacao,dt_mesano_referencia)
	values	(	w_ptu_movto_produto_seq.nextval,nr_seq_reg_arquivo_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			0,sysdate,cd_ans_w,cd_cgc_operadora_w,'V01',
			'O',nr_seq_arquivo_w,nr_seq_lote_p,nr_seq_movimentacao_w,dt_mes_referencia_w);

	open C02;
	loop
	fetch C02 into
		nr_seq_empresa_w;
	exit when C02%notfound;
		begin

		open C03;
		loop
		fetch C03 into
			nr_seq_beneficiario_w,
			nr_seq_segurado_w;
		exit when C03%notfound;
			begin

			select	dt_inclusao,
				dt_exclusao
			into	dt_inclusao_w,
				dt_exclusao_w
			from	ptu_mov_produto_benef
			where	nr_sequencia	= nr_seq_beneficiario_w;

			cd_motivo_w		:= 00;
			nr_prot_ans_origem_w	:= '000000000';
			cd_plano_ans_w		:= '';
			cd_plano_ans_pre_w	:= '';

			select	distinct
				a.cd_pessoa_fisica,
				substr(b.nm_pessoa_fisica,1,59) nm_beficiario,
				b.dt_nascimento,
				decode(b.ie_sexo,'M',1,decode(b.ie_sexo,'F',3,0)),
				nvl(b.nr_pis_pasep,'00000000000'),
				substr(nvl(obter_compl_pf(b.cd_pessoa_fisica,5,'NPR'),obter_compl_pf(b.cd_pessoa_fisica,5,'N')),1,59) nm_mae_benef,
				nvl(substr(b.nr_cartao_nac_sus,1,15),'000000000000000') cd_cns,
				b.nr_identidade,
				b.ds_orgao_emissor_ci,
				b.cd_nacionalidade,
		        	a.nr_seq_titular,
				c.ie_regulamentacao,
				decode(ie_regulamentacao,'R','000000000',nvl(c.nr_protocolo_ans,'000000000')),
				decode(ie_regulamentacao,'R',c.cd_scpa,' '),
				a.dt_contratacao,
				substr(obter_compl_pf(b.cd_pessoa_fisica,1,'ES'),1,30),
				substr(obter_compl_pf(b.cd_pessoa_fisica,1,'NR'),1,5),
				substr(obter_compl_pf(b.cd_pessoa_fisica,1,'CO'),1,15),
				substr(obter_compl_pf(b.cd_pessoa_fisica,1,'B'),1,20),
				substr(obter_compl_pf(b.cd_pessoa_fisica,1,'CDM'),1,10) || Calcula_Digito('MODULO10',substr(obter_compl_pf(b.cd_pessoa_fisica,1,'CDM'),1,10)),
				substr(obter_compl_pf(b.cd_pessoa_fisica,1,'UF'),1,5),
				nvl(lpad(substr(obter_compl_pf(b.cd_pessoa_fisica,1,'CEP'),1,8),8,'0'),'00000000'),
				a.dt_contratacao	dt_mesano_referencia,
				a.dt_reativacao,
				nvl(a.nr_seq_portabilidade,0),
				a.cd_cco
			into	cd_pessoa_fisica_w,
				nm_segurado_w,
				dt_nascimento_w,
				ie_sexo_w,
				nr_pis_pasep_w,
				nm_mae_segurado_w,
				cd_cns_w,
				nr_identidade_w,
				ds_orgao_emissor_ci_w,
				cd_nacionalidade_w,
				nr_seq_titular_w,
				ie_regulamentacao_w,
				cd_plano_ans_w,
				cd_plano_ans_pre_w,
				dt_liberacao_w,
				ds_logradouro_w,
				ds_numero_w,
				ds_complemento_w,
				ds_bairro_w,
				ds_municipio_w,
				uf_w,
				cep_w,
				dt_mesano_referencia_w,
				dt_reinclusao_w,
				nr_seq_portabilidade_w,
				cd_cco_w
			from   	pls_segurado a,
				pls_plano c,
				pessoa_fisica b,
				pls_contrato e
			where  	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
			and	e.nr_sequencia		= a.nr_seq_contrato
			and	c.nr_sequencia		= a.nr_seq_plano
			and	a.nr_sequencia		= nr_seq_segurado_w;
			
			if	(cd_cco_w is not null) then
				nr_cco_w	:= to_number(substr(cd_cco_w,1,10));
				ie_digito_cco_w	:= to_number(substr(cd_cco_w,10,2));
			else
				nr_cco_w	:= null;
				ie_digito_cco_w	:= null;
			end if;
			
			pls_obter_dados_interf_sib(	nr_seq_segurado_w,
							cd_pais_sib_w,
							cd_usuario_ant_w,
							cd_usuario_plano_w,
							cd_usuario_plano_tit_w,
							cd_cgc_estipulante_w,
							nr_cpf_w,
							cd_vinculo_benef_w,
							ie_carencia_temp_w,
							ie_resid_brasil_w);

			if	(nvl(nr_seq_portabilidade_w,0) > 0) then --Se for de portabilidade o motivo � o 41
				cd_motivo_w	:= 41;

				select	nvl(to_char(somente_numero(max(nr_prot_ans_origem))),'000000000')
				into	nr_prot_ans_origem_w
				from	pls_segurado a,
					pls_portab_pessoa b
				where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
				and	a.nr_sequencia	= nr_seq_segurado_w;
			else
				select	nvl(max(cd_ans),15)
				into	cd_motivo_w
				from 	pls_motivo_inclusao_seg a,
					pls_segurado b
				where	b.nr_seq_motivo_inclusao	= a.nr_sequencia
				and	b.nr_sequencia			= nr_seq_segurado_w;
			end if;

			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

			--Gerar os benefici�rios de inclus�o
			if	(trunc(dt_inclusao_w,'Month')	= trunc(dt_mes_referencia_w,'Month')) and
				(dt_exclusao_w is null) then
				insert into w_ptu_movto_produto
					(	nr_sequencia,nr_seq_reg_arquivo,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						ie_tipo_reg,cd_motivo,cd_usuario_plano,nm_beneficiario,dt_nascimento,
						ie_sexo,nr_cpf,cd_usuario_plano_sup,nr_pis_pasep,nm_mae_benef,
						cd_cns,nr_identidade,ds_orgao_emissor_ci,cd_pais,cd_plano_ans,
						cd_plano_ans_pre,nr_prot_ans_origem,dt_adesao_plano,cd_vinculo_benef,ie_carencia_temp,
						ie_itens_excluid_cobertura,cd_cgc_estipulante,cd_cei,ds_logradouro,ds_numero,
						ds_complemento,ds_bairro,ds_municipio,ie_resid_brasil,cd_cep,
						nr_seq_lote_movto,nr_seq_movimentacao,nr_seq_movto_benef,nr_seq_segurado,dt_mesano_referencia,
						nm_beficiario	)
				values	(	w_ptu_movto_produto_seq.nextval,nr_seq_reg_arquivo_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
						1,cd_motivo_w,cd_usuario_plano_w,nm_segurado_w,dt_nascimento_w,
						ie_sexo_w,nr_cpf_w,cd_usuario_plano_tit_w,nr_pis_pasep_w,nm_mae_segurado_w,
						cd_cns_w,nr_identidade_w,ds_orgao_emissor_ci_w,cd_pais_sib_w,cd_plano_ans_w,
						cd_plano_ans_pre_w,nr_prot_ans_origem_w,dt_mesano_referencia_w,cd_vinculo_benef_w,ie_carencia_temp_w,
						2,cd_cgc_estipulante_w,'00000000000000',ds_logradouro_w,ds_numero_w,
						ds_complemento_w,ds_bairro_w,ds_municipio_w,ie_resid_brasil_w,cep_w,
						nr_seq_lote_p,nr_seq_movimentacao_w,nr_seq_beneficiario_w,nr_seq_segurado_w,dt_mes_referencia_w,
						nm_segurado_w	);

			--Gerar os benefici�rios de exclus�o
			elsif	(trunc(dt_exclusao_w,'Month')	= trunc(dt_mes_referencia_w,'Month')) then
				insert into w_ptu_movto_produto
					(	nr_sequencia,nr_seq_reg_arquivo,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						ie_tipo_reg,cd_motivo,cd_usuario_plano,nm_beneficiario,dt_nascimento,
						ie_sexo,nr_cpf,cd_usuario_plano_sup,nr_pis_pasep,nm_mae_benef,
						cd_cns,nr_identidade,ds_orgao_emissor_ci,cd_pais,cd_plano_ans,
						cd_plano_ans_pre,nr_prot_ans_origem,dt_adesao_plano,cd_vinculo_benef,ie_carencia_temp,
						ie_itens_excluid_cobertura,cd_cgc_estipulante,cd_cei,ds_logradouro,ds_numero,
						ds_complemento,ds_bairro,ds_municipio,ie_resid_brasil,cd_cep,
						nr_seq_lote_movto,nr_seq_movimentacao,nr_seq_movto_benef,nr_seq_segurado,dt_mesano_referencia,
						nm_beficiario,nr_cco,ie_digito_cco	)
				values	(	w_ptu_movto_produto_seq.nextval,nr_seq_reg_arquivo_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
						7,cd_motivo_w,cd_usuario_plano_w,nm_segurado_w,dt_nascimento_w,
						ie_sexo_w,nr_cpf_w,cd_usuario_plano_tit_w,nr_pis_pasep_w,nm_mae_segurado_w,
						cd_cns_w,nr_identidade_w,ds_orgao_emissor_ci_w,cd_pais_sib_w,cd_plano_ans_w,
						cd_plano_ans_pre_w,nr_prot_ans_origem_w,dt_mesano_referencia_w,cd_vinculo_benef_w,ie_carencia_temp_w,
						2,cd_cgc_estipulante_w,'00000000000000',ds_logradouro_w,ds_numero_w,
						ds_complemento_w,ds_bairro_w,ds_municipio_w,ie_resid_brasil_w,cep_w,
						nr_seq_lote_p,nr_seq_movimentacao_w,nr_seq_beneficiario_w,nr_seq_segurado_w,dt_mes_referencia_w,
						nm_segurado_w,nr_cco_w,ie_digito_cco_w);

			--Gerar os benefici�rios de altera��o
			elsif	(trunc(dt_inclusao_w,'Month')	<> trunc(dt_mes_referencia_w,'Month')) and
				(dt_exclusao_w is null)	then
					insert into w_ptu_movto_produto
					(	nr_sequencia,nr_seq_reg_arquivo,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
						ie_tipo_reg,cd_motivo,cd_usuario_plano,nm_beneficiario,dt_nascimento,
						ie_sexo,nr_cpf,cd_usuario_plano_sup,nr_pis_pasep,nm_mae_benef,
						cd_cns,nr_identidade,ds_orgao_emissor_ci,cd_pais,cd_plano_ans,
						cd_plano_ans_pre,nr_prot_ans_origem,dt_adesao_plano,cd_vinculo_benef,ie_carencia_temp,
						ie_itens_excluid_cobertura,cd_cgc_estipulante,cd_cei,ds_logradouro,ds_numero,
						ds_complemento,ds_bairro,ds_municipio,ie_resid_brasil,cd_cep,
						nr_seq_lote_movto,nr_seq_movimentacao,nr_seq_movto_benef,nr_seq_segurado,dt_mesano_referencia,
						nm_beficiario,nr_cco,ie_digito_cco	)
				values	(	w_ptu_movto_produto_seq.nextval,nr_seq_reg_arquivo_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
						2,cd_motivo_w,cd_usuario_plano_w,nm_segurado_w,dt_nascimento_w,
						ie_sexo_w,nr_cpf_w,cd_usuario_plano_tit_w,nr_pis_pasep_w,nm_mae_segurado_w,
						cd_cns_w,nr_identidade_w,ds_orgao_emissor_ci_w,cd_pais_sib_w,cd_plano_ans_w,
						cd_plano_ans_pre_w,nr_prot_ans_origem_w,dt_mesano_referencia_w,cd_vinculo_benef_w,ie_carencia_temp_w,
						2,cd_cgc_estipulante_w,'00000000000000',ds_logradouro_w,ds_numero_w,
						ds_complemento_w,ds_bairro_w,ds_municipio_w,ie_resid_brasil_w,cep_w,
						nr_seq_lote_p,nr_seq_movimentacao_w,nr_seq_beneficiario_w,nr_seq_segurado_w,dt_mes_referencia_w,
						nm_segurado_w,nr_cco_w,ie_digito_cco_w	);

			end if;

			end;
		end loop;
		close C03;
		end;
	end loop;
	close C02;

	select	sum(qt_tipo_1), --Inclus�o
		sum(qt_tipo_2), --Altera��o
		sum(qt_tipo_6), --Indica��o de inexist�ncia de benefici�rio ou de altera��o cadastral
		sum(qt_tipo_7), --Exclus�o
		sum(qt_tipo_8)  --Reinclus�o
	into	qt_tipo1_w,
		qt_tipo2_w,
		qt_tipo6_w,
		qt_tipo7_w,
		qt_tipo8_w
	from	(select	decode(ie_tipo_reg,1,1,0) qt_tipo_1,
			decode(ie_tipo_reg,2,1,0) qt_tipo_2,
			decode(ie_tipo_reg,6,1,0) qt_tipo_6,
			decode(ie_tipo_reg,7,1,0) qt_tipo_7,
			decode(ie_tipo_reg,8,1,0) qt_tipo_8
		from	w_ptu_movto_produto
		where	nr_seq_lote_movto 	= nr_seq_lote_p
		and	nr_seq_movimentacao	= nr_seq_movimentacao_w);

	select	max(nr_seq_reg_arquivo)
	into	nr_seq_reg_arquivo_w
	from	w_ptu_movto_produto
	where	nr_seq_lote_movto 	= nr_seq_lote_p
	and	nr_seq_movimentacao	= nr_seq_movimentacao_w;

	if	(nvl(nr_seq_reg_arquivo_w,0) = 0) then
		nr_seq_reg_arquivo_w	:= 1;
	else
		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w +1;
	end if;

	select	count(*)
	into	qt_trailer_w
	from	w_ptu_movto_produto
	where	nr_seq_lote_movto 	= nr_seq_lote_p
	and	nr_seq_movimentacao	= nr_seq_movimentacao_w
	and	ie_tipo_reg	= 9;

	--Gerar o trailler
	if	(qt_trailer_w	= 0) then
		insert into w_ptu_movto_produto
			(nr_sequencia,
			nr_seq_reg_arquivo,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo_reg,
			qt_tipo1,
			qt_tipo2,
			qt_tipo6,
			qt_tipo7,
			qt_tipo8,
			dt_mesano_referencia,
			nr_seq_lote_movto,
			nr_seq_movimentacao)
		values 	(w_ptu_movto_produto_seq.nextval,
			nr_seq_reg_arquivo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			9,
			qt_tipo1_w,
			qt_tipo2_w,
			qt_tipo6_w,
			qt_tipo7_w,
			qt_tipo8_w,
			dt_mes_referencia_w,
			nr_seq_lote_p,
			nr_seq_movimentacao_w);
	end if;

	end;
end loop;
close C01;

end ptu_gerar_interface_uniodonto;
/