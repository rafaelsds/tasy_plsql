create or replace
procedure Inserir_Itens_Agenda_Integrada(
			nr_seq_agenda_int_p	Number,
			ds_itens_grupo_p	Varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is

nr_seq_grupo_w		Number(10);
nr_seq_proc_interno_w	Number(10);
cd_especialidade_w	Number(5);
cd_medico_w		Varchar2(10);
ie_tipo_agendamento_w	Varchar2(15);
nr_seq_grupo_proc_w	Number(10);
nr_seq_proc_interno_ww	Number(10);
cd_medico_ww		Varchar2(10); 
cd_especialidade_ww	Number(5);
ie_tipo_agendamento_ww	Varchar2(15);
nr_minuto_duracao_w	Number(10);
nr_minuto_duracao_ww	Number(10);
ds_itens_grupo_w	Varchar2(800);
tam_lista_w		number(10);
ie_pos_virgula_w	number(3)	:= 0;
nr_seq_grupo_Real_w	Number(10);
ds_lista_grupo_w	Varchar2(800);
ie_classif_agenda_w	Varchar2(5);
ie_classif_agenda_ww	Varchar2(5);
nr_seq_exame_adic_w	number(10);
nr_seq_grupo_item_w	number(10);
qt_exame_adic_w		number(10);
nr_seq_item_agenda_w	number(10);
ie_gerar_check_list_w	varchar2(1);
nr_seq_ageint_check_list_w number(10,0);
ie_gerar_equip_w	varchar2(1);
nr_seq_regra_w		number(10);
nr_seq_item_w		number(10,0);
ds_Retorno_w		varchar2(255);
ie_forma_checklist_w	varchar2(1) := 'L';
ie_anestesia_w		varchar2(1) := 'N';
ie_verificar_anestesia_w	varchar2(1);
nr_seq_proc_item_grupo_w	number(10);
nr_seq_agrupamento_w		number(10);

Cursor C02 is 
	select	ie_tipo_agendamento,
		nr_Seq_proc_interno,
		cd_medico,
		cd_especialidade,
		nr_minuto_duracao,
		ie_classif_agenda,
		nr_sequencia
	from	ageint_grupo_proc_item
	where	nr_Seq_grupo_proc	= nr_seq_grupo_proc_w
	and	ie_situacao		= 'A';    

Cursor C01 is
	select	nr_seq_proc_interno
	from	ageint_ex_adic_grupo_item
	where	nr_seq_grupo_item	= nr_seq_grupo_real_w;	

begin

Obter_Param_Usuario(869, 24, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_equip_w);
Obter_Param_Usuario(869, 75, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_forma_checklist_w);
Obter_Param_Usuario(869, 147, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_verificar_anestesia_w);

ds_itens_grupo_w	:= ds_itens_grupo_p;
if	(ds_itens_grupo_p is not null) then
	begin
	while	(ds_itens_grupo_w is not null) LOOP
		begin

		tam_lista_w		:=	length(ds_itens_grupo_w);
		ie_pos_virgula_w		:=	instr(ds_itens_grupo_w,',');
	
		if	(ie_pos_virgula_w <> 0) then
			nr_seq_grupo_real_w	:= substr(ds_itens_grupo_w,1,(ie_pos_virgula_w - 1));
			ds_itens_grupo_w	:= substr(ds_itens_grupo_w,(ie_pos_virgula_w + 1),tam_lista_w);
		end if;
	
		select	nr_seq_grupo,
			nr_Seq_proc_interno,
			cd_especialidade,
			cd_medico,
			nr_seq_grupo_proc,
			nr_minuto_duracao,
			ie_classif_agenda,
			nr_sequencia,
			nr_seq_agrupamento
		into	nr_seq_grupo_w,
			nr_seq_proc_interno_w,
			cd_especialidade_w,
			cd_medico_w,
			nr_seq_grupo_proc_w,
			nr_minuto_duracao_w,
			ie_classif_agenda_w,
			nr_seq_item_w,
			nr_seq_agrupamento_w
		from	agenda_int_grupo_item
		where	nr_sequencia	= nr_seq_grupo_real_w
		order by nr_seq_apres;
		
		select	max(ie_tipo_agendamento)
		into	ie_tipo_agendamento_w
		from	agenda_int_grupo
		where	nr_sequencia	= nr_seq_grupo_w;
		
		if	((ie_tipo_agendamento_w	in ('E','S')) and
			(nr_seq_proc_interno_w is not null)) or 
			((ie_tipo_agendamento_w	= 'C') and
			((cd_medico_w is not null) or
			(cd_especialidade_w is not null))) then

			if	(nr_seq_proc_interno_w is not null) and
				(nr_minuto_duracao_w is null) then
				nr_minuto_duracao_w	:= ageint_obter_tempo_exame(nr_seq_proc_interno_w);
			end if;
			
			if	(nr_minuto_duracao_w is null) and
				(ie_classif_agenda_w is not null) then
				nr_minuto_duracao_w	:= Ageint_Obter_Dur_Classif(ie_classif_agenda_w, cd_estabelecimento_p);
			end if;
			
			if	(nr_seq_proc_interno_w is not null) then
				Ageint_Consistir_Regra_Ex_Adic(nr_Seq_agenda_int_p, nr_seq_proc_interno_w, cd_estabelecimento_p, nm_usuario_p, nr_seq_regra_w, ds_retorno_w);
				if	(ie_verificar_anestesia_w = 'S') then
					ie_anestesia_w	:= Ageint_Obter_Se_Proc_Anest(nr_seq_proc_interno_w,null,null,nr_seq_agenda_int_p);
				end if;
					
			end if;
			
			if	(((nvl(nr_Seq_regra_w,0)	> 0) and 
				(ds_retorno_w is null)) or
				((ds_retorno_w is null) and
				nvl(nr_Seq_regra_w,0)	= 0)) then
			
				select	agenda_integrada_item_seq.nextval
				into	nr_seq_item_agenda_w
				from 	dual;
				
				insert into agenda_integrada_item
					(nr_sequencia,
					nr_seq_agenda_int,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec, 
					nr_seq_proc_interno,
					ie_tipo_agendamento,
					cd_medico,
					cd_especialidade,
					nr_minuto_duracao,
					ie_classif_agenda,
					nr_seq_grupo_selec,
					nr_seq_regra_ex_adic,
					nr_seq_item_selec,
					ie_anestesia,
					nr_seq_agrupamento)
				values
					(nr_seq_item_agenda_w,
					nr_seq_agenda_int_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_proc_interno_w,
					ie_tipo_agendamento_w,
					cd_medico_w,
					cd_especialidade_w,
					decode(nr_minuto_duracao_w,0,'',nr_minuto_duracao_w),
					ie_classif_agenda_w,
					nr_seq_grupo_w,
					nr_seq_regra_w,
					nr_seq_item_w,
					ie_anestesia_w,
					nr_seq_agrupamento_w);
				commit;
				
				if	(nvl(nr_seq_proc_interno_w, 0)	> 0) then
					ageint_gerar_proc_Assoc(nr_seq_agenda_int_p, nr_seq_item_agenda_w, nr_seq_proc_interno_w, nm_usuario_p, nr_seq_grupo_w);
					Ageint_gerar_anexos_proc(nr_seq_agenda_int_p, nr_seq_proc_interno_w, cd_estabelecimento_p, nm_usuario_p);	
				end if;

				Ageint_Gerar_Equip_Item(nr_seq_proc_interno_w, nr_seq_item_agenda_w, cd_estabelecimento_p, nm_usuario_p);

				if	(ie_forma_checklist_w = 'L') then
				
					select	substr(ageint_obter_se_proc_check(nr_seq_proc_interno_w,nr_seq_agenda_int_p, cd_estabelecimento_p),1,1)
					into	ie_gerar_check_list_w
					from	dual;
					
					if	(ie_gerar_check_list_w = 'S') then
						select	ageint_check_list_paciente_seq.nextval
						into	nr_seq_ageint_check_list_w
						from	dual;
					
						insert into ageint_check_list_paciente (nr_sequencia,
											nr_seq_ageint,
											dt_atualizacao,
											nm_usuario,
											ie_tipo_check_list)
										values (nr_seq_ageint_check_list_w,
											nr_seq_agenda_int_p,
											sysdate,	
											nm_usuario_p,
											'I');
										
						commit;
									
						Ageint_Gerar_Check_List(nr_seq_ageint_check_list_w,nr_seq_proc_interno_w,nr_seq_agenda_int_p,nm_usuario_p, cd_estabelecimento_p);
						
					end if;
				end if;
			end if;
		end if;
		
		select	count(*)
		into	qt_exame_adic_w
		from	ageint_ex_adic_grupo_item
		where	nr_seq_grupo_item	= nr_seq_grupo_real_w;
		
		if	(qt_exame_adic_w	> 0) and
			(nr_seq_item_agenda_w	> 0) then
			open C01;
			loop
			fetch C01 into	
				nr_seq_exame_adic_w;
			exit when C01%notfound;
				begin
				insert into ageint_exame_adic_item
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_proc_interno,
						nr_seq_item,
						nr_seq_grupo_selec)
					values
						(ageint_exame_adic_item_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_exame_adic_w,
						nr_seq_item_agenda_w,
						nr_seq_grupo_w);
				end;
			end loop;
			close C01;
		end if;
		
		if	(nr_Seq_grupo_proc_w is not null) then
			open C02;
			loop
			fetch C02 into
				ie_tipo_agendamento_ww,
				nr_seq_proc_interno_ww,
				cd_medico_ww,
				cd_especialidade_ww,
				nr_minuto_duracao_ww,
				ie_classif_agenda_ww,
				nr_seq_proc_item_grupo_w;
			exit when C02%notfound;
				begin
				
				if	(nr_seq_proc_interno_w is not null) then
					Ageint_Consistir_Regra_Ex_Adic(nr_Seq_agenda_int_p, nr_seq_proc_interno_w, cd_estabelecimento_p, nm_usuario_p, nr_seq_regra_w, ds_retorno_w);
				end if;
			
				if	(((nvl(nr_Seq_regra_w,0)	> 0) and 
					(ds_retorno_w is null)) or
					((ds_retorno_w is null) and
					nvl(nr_Seq_regra_w,0)	= 0)) then
					
					insert into agenda_integrada_item
						(nr_sequencia,
						nr_seq_agenda_int,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_proc_interno,
						ie_tipo_agendamento,
						cd_medico,
						cd_especialidade, 
						nr_minuto_duracao,
						ie_classif_agenda,
						nr_seq_grupo_proc,
						nr_seq_regra_ex_adic,
						nr_seq_proc_item_grupo)
					values
						(agenda_integrada_item_seq.nextval,
						nr_seq_agenda_int_p,
						sysdate,
						nm_usuario_p,
						sysdate,	
						nm_usuario_p,
						nr_seq_proc_interno_ww,
						ie_tipo_agendamento_ww,
						cd_medico_ww,
						cd_especialidade_ww,
						nr_minuto_duracao_ww,
						ie_classif_agenda_ww,
						nr_Seq_grupo_proc_w,
						nr_seq_regra_w,
						nr_seq_proc_item_grupo_w);
					commit;
					
					if	(nvl(nr_seq_proc_interno_w, 0)	> 0) then
						ageint_gerar_proc_Assoc(nr_seq_agenda_int_p, nr_seq_item_agenda_w, nr_seq_proc_interno_w, nm_usuario_p, nr_seq_grupo_w);
						Ageint_gerar_anexos_proc(nr_seq_agenda_int_p, nr_seq_proc_interno_w, cd_estabelecimento_p, nm_usuario_p);
					end if;
					
					if	(ie_forma_checklist_w = 'L') then
					
						select	substr(ageint_obter_se_proc_check(nr_seq_proc_interno_ww,nr_seq_agenda_int_p, cd_estabelecimento_p),1,1)
						into	ie_gerar_check_list_w
						from	dual;
					
						if	(ie_gerar_check_list_w = 'S') then
				
							select	ageint_check_list_paciente_seq.nextval
							into	nr_seq_ageint_check_list_w
							from	dual;
					
							insert into ageint_check_list_paciente (nr_sequencia,
												nr_seq_ageint,
												dt_atualizacao,
												nm_usuario,
												ie_tipo_check_list)
											values (nr_seq_ageint_check_list_w, 
												nr_seq_agenda_int_p,
												sysdate,	
												nm_usuario_p,
												'I');
											
							commit;
										
							Ageint_Gerar_Check_List(nr_seq_ageint_check_list_w,nr_seq_proc_interno_ww,nr_seq_agenda_int_p,nm_usuario_p, cd_estabelecimento_p);
						
						end if;
					end if;
				end if;
				end;
			end loop;
			close C02;
		end if;
		end;		
	end loop;
	end;
end if;

commit;

end Inserir_Itens_Agenda_Integrada;
/