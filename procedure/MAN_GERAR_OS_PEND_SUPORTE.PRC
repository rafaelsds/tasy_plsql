create or replace
procedure	MAN_GERAR_OS_PEND_SUPORTE is

nr_seq_grupo_w				grupo_suporte.nr_sequencia%type;
ds_grupo_w					grupo_suporte.ds_grupo%type;
nr_seq_ordem_serv_w			man_ordem_servico.nr_sequencia%type;
nr_sequencia_w				os_pend_suporte.nr_sequencia%type;
qt_os_w						number(10);
qt_os_30_w					number(10);
ie_suporte_w				varchar2(1);
ie_30_dias_w				varchar2(1);

/* Obter cada grupo individualmente */
/* N�o � verificado se o grupo est� ativo */
/* para buscar tamb�m OS's que possam estar perdidas */
cursor	c01 is
select	nr_sequencia,
		ds_grupo
from	grupo_suporte
where	nr_seq_gerencia_sup in (5,58,196)
and		nr_sequencia not in (27,70,85)
order by 1;

/* Obter cada OS pendente de forma individual */
/* Busca todas as OS's pendentes no hor�rio da execu��o */
cursor	c02 is
select	nr_sequencia
from	(
	select	z.nr_sequencia
	from	man_ordem_servico z,
		man_estagio_processo y,
		man_ordem_serv_estagio x
	where	x.nr_seq_ordem = z.nr_sequencia
	and     y.nr_sequencia =  x.nr_seq_estagio
	and     z.nr_seq_grupo_sup = nr_seq_grupo_w
	and	z.nr_seq_estagio is not null    
	and     (x.nr_seq_estagio <> 511 or z.ie_status_ordem <> 3)
	and     nvl(z.ie_status_ordem,1) <> 3
	and     y.ie_acao = 1
	and	z.dt_ordem_servico > sysdate - 700
	and     z.nr_grupo_trabalho in (2)
	and	z.nr_seq_estagio not in(511,9,2465) /* "Encerramento Solicitado pelo Cliente" "OK - Cliente Encerrado" "Cloud - Planejamento" */
	and     (select max(a.dt_atualizacao) 
		from    man_ordem_serv_estagio a
		where   a.nr_seq_ordem = z.nr_sequencia
		and     a.dt_atualizacao < (trunc(sysdate) + 86399/86400)) = x.dt_atualizacao
		and     (y.ie_suporte = 'S' or y.ie_sup_nivel2 = 'S')
		and     x.dt_atualizacao < (trunc(sysdate) + 86399/86400)
		union
		select	z.nr_sequencia
		from	man_ordem_servico z,
			man_estagio_processo y,
			man_ordem_serv_estagio x
		where	x.nr_seq_ordem = z.nr_sequencia
		and     y.nr_sequencia =  x.nr_seq_estagio
		and     z.nr_seq_grupo_sup = nr_seq_grupo_w
		and	z.nr_seq_estagio is not null    
		and     (x.nr_seq_estagio <> 511 or z.ie_status_ordem <> 3)
		and     nvl(z.ie_status_ordem,1) <> 3
		and     y.ie_acao = 1
		and	z.dt_ordem_servico > sysdate - 700
		and     z.nr_grupo_trabalho in (262)
		and	z.nr_seq_estagio not in(511,9,2465) /* "Encerramento Solicitado pelo Cliente" "OK - Cliente Encerrado" "Cloud - Planejamento" */
		and     (select max(a.dt_atualizacao) 
			from    man_ordem_serv_estagio a
			where   a.nr_seq_ordem = z.nr_sequencia
			and     a.dt_atualizacao < (trunc(sysdate) + 86399/86400)) = x.dt_atualizacao
		and     (y.ie_sup_nivel2 = 'S' or y.ie_suporte = 'S')
		and     x.dt_atualizacao < (trunc(sysdate) + 86399/86400))
group by nr_sequencia;

begin

/* Abre CURSOR para buscar grupos do suporte */
open c01;
loop
fetch c01 into
	nr_seq_grupo_w,
	ds_grupo_w;
exit when c01%notfound;
	begin
	
	/* Zera as vari�veis a cada troca de grupo */
	qt_os_w		:= 0;
	qt_os_30_w	:= 0;
	
	/* Abre CURSOR para pegar as OS's pendentes */
	open c02;
	loop
	fetch c02 into
		nr_seq_ordem_serv_w;
	exit when c02%notfound;
		begin
		
		/* Verifica se o �ltimo est�gio da OS antes das 17:30 */
		/* pertence ao Suporte */
		select	nvl(max('S'),'N')
		into	ie_suporte_w
		from	man_ordem_serv_estagio a,
			man_estagio_processo b
		where	a.nr_seq_ordem = nr_seq_ordem_serv_w
		and	a.nr_seq_estagio = b.nr_sequencia
		and	a.nr_sequencia = (
				select	nvl(max(x.nr_sequencia),0)
				from	man_ordem_serv_estagio x
				where	x.nr_seq_ordem = a.nr_seq_ordem
				and	x.dt_atualizacao < (trunc(SYSDATE) + 17.5/24))
		and	((b.ie_suporte = 'S') or (b.ie_sup_nivel2 = 'S'))
		and	b.ie_acao = 1;
		
		/* Caso penten�a ao Suporte, considera na quantidade de OS's pendentes */
		if	(ie_suporte_w = 'S') then
		
			qt_os_w	:= qt_os_w + 1;
			
			/* Se for criada tabela filha para gravar OSs (an�litico) */
			/* a grava��o dever� ocorrer nessa parte */
			/* pois dever� gravar apenas as OSs que considerar */
			
			/* Verifica se a OS possui +30 dias */
			/* S� realiza essa verifica��o se a OS for realmente considerada como pendente */
			select	nvl(max('S'),'N')
			into	ie_30_dias_w
			from	man_ordem_servico
			where	nr_sequencia = nr_seq_ordem_serv_w
			and	round(coalesce(dt_fim_real,sysdate) - dt_ordem_servico) >= 30;
			
			/* Caso OS +30 dias, considera na quantidade do indicador */		
			if	(ie_30_dias_w = 'S') then
				qt_os_30_w	:= qt_os_30_w + 1;
			end if;
			
		end if;
		
		end;
	end loop;
	close c02;
	
	if	(qt_os_w > 0) then
		begin
	
		select	os_pend_suporte_seq.nextval
		into	nr_sequencia_w
		from 	dual;

		insert into os_pend_suporte(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,										
			dt_referencia,
			ds_grupo,
			nr_seq_grupo,
			qt_os,
			qt_mais_trinta)
		values(
			nr_sequencia_w,
			sysdate,
			'Job',
			sysdate,
			'Job',											
			trunc(sysdate),
			ds_grupo_w,
			nr_seq_grupo_w,
			qt_os_w,
			qt_os_30_w);
				
		end;
	end if;
	
	end;
end loop;
close c01;

commit;

end MAN_GERAR_OS_PEND_SUPORTE;
/