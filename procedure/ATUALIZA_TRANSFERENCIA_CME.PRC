create or replace
procedure atualiza_transferencia_cme(	nr_seq_origem_p		number,
				nr_seq_conjunto_p	number,
				nm_usuario_p		varchar2,
				ie_origem_inf_p		varchar2,
				ie_status_cme_p		varchar2) is

begin

update	agenda_pac_cme
set	ie_status_cme 	= ie_status_cme_p,
	nm_usuario 	= nm_usuario_p,
	dt_atualizacao 	= sysdate,
	ie_origem_inf	= ie_origem_inf_p
where	nr_seq_agenda	= nr_seq_origem_p
and 	nr_seq_conjunto	= nr_seq_conjunto_p;
commit;

end atualiza_transferencia_cme;
/