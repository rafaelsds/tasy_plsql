CREATE OR REPLACE
PROCEDURE Sus_Atualiza_AIH_Dados
		(	nr_atendimento_p			Number,
			cd_medico_aut_p		out	Varchar2,
			cd_orgao_emissor_aih_p	out	Varchar2) is


cd_estabelecimento_w		Number(4);
cd_medico_autorizador_w		Varchar2(10);
cd_orgao_emissor_aih_w		Varchar2(10);
cd_pessoa_fisica_w		Varchar2(10);
cd_municipio_ibge_w		Varchar2(06);


BEGIN

begin
select	cd_estabelecimento,
	cd_pessoa_fisica
into	cd_estabelecimento_w,
	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;
exception
when others then
	--R.aise_application_error(-20011,'Problema na leitura do atendimento, verifique se o mesmo � AIH.');
	wheb_mensagem_pck.exibir_mensagem_abort(263435);
end;

begin
select	cd_medico_autorizador
into	cd_medico_autorizador_w
from	sus_parametros_aih
where	cd_estabelecimento	= cd_estabelecimento_w;
exception
	when others then
	--r.aise_application_error(-20011, 'Problema na leitura dos cadastros da AIH. #@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263436);
end;

if	(cd_pessoa_fisica_w is not null) then
	begin
	select	nvl(max(cd_municipio_ibge),'0')
	into	cd_municipio_ibge_w
	from	compl_pessoa_fisica
	where	ie_tipo_complemento	= 1
	and	cd_pessoa_fisica	= cd_pessoa_fisica_w;

	if	(cd_municipio_ibge_w is not null) and
		(cd_municipio_ibge_w <> '0') then
		begin
		select	cd_orgao_emissor,
			nvl(cd_medico_autorizador, cd_medico_autorizador_w)
		into	cd_orgao_emissor_aih_w,
			cd_medico_autorizador_w
		from	sus_municipio
		where	cd_municipio_ibge	= cd_municipio_ibge_w;
		exception
			when others then
			cd_medico_autorizador_w	:= cd_medico_autorizador_w;
		end;
	end if;

	if	(cd_orgao_emissor_aih_w is null) then
		select	cd_orgao_emissor_aih
		into	cd_orgao_emissor_aih_w
		from	sus_parametros_aih
		where	cd_estabelecimento	= cd_estabelecimento_w;
	end if;

	end;
end if;

cd_medico_aut_p		:= cd_medico_autorizador_w;
cd_orgao_emissor_aih_p	:= cd_orgao_emissor_aih_w;

END Sus_Atualiza_AIH_Dados;
/