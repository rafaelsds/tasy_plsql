create or replace
procedure atualiza_dados_nfse_link(	nr_nfe_imp_p		varchar2,
				cd_verificacao_nfse_p	varchar2,
				dt_emissao_nfe_p	varchar2,
				cd_serie_nf_p		varchar2,
				nr_nota_fiscal_p	varchar2,
				cd_cgc_emitente_p	varchar2,
				ds_link_rps_p		varchar2) is

dt_emissao_nfe_w	date;
ie_situacao_w		varchar2(1);
cd_cgc_emitente_w 	nota_fiscal.cd_cgc_emitente%type;
ds_link_rps_w		nota_fiscal.ds_link_rps%type;

begin

cd_cgc_emitente_w := elimina_caractere_especial(cd_cgc_emitente_p);

ds_link_rps_w :=  nvl(ds_link_rps_p,'no_invoice_url');

select  max(ie_situacao)
into	ie_situacao_w
from 	nota_fiscal
where 	nr_nota_fiscal  = nr_nota_fiscal_p
and	cd_serie_nf	= cd_serie_nf_p
and	((cd_cgc_emitente	= cd_cgc_emitente_w) or (cd_cgc_emitente_w = 'X'))
and 	ie_tipo_nota in ('SE','SD','SF','ST')
and	ie_situacao in (1,8);

if (dt_emissao_nfe_p is not null) then
	begin
	dt_emissao_nfe_w := replace(dt_emissao_nfe_p, 'T', ' ');

	exception when others then
		begin
		dt_emissao_nfe_w		:= to_date(replace(dt_emissao_nfe_p, 'T', ' '), 'yyyy-mm-dd hh24:mi:ss');
		exception when others then
			begin
			dt_emissao_nfe_w	:= to_date(replace(dt_emissao_nfe_p, 'T', ' '), 'dd-mm-yyyy hh24:mi:ss');
			exception when others then
			--N�o foi poss�vel converter a data de emiss�o enviada no arquivo:
				Wheb_mensagem_pck.exibir_mensagem_abort(186114,'DT_EMISSAO_NFE=' || dt_emissao_nfe_p); 
			end;
		end;


	end;
end if;


if	(ie_situacao_w <> '8') then
	update	nota_fiscal
	set 	nr_nfe_imp		=  nr_nfe_imp_p,
		cd_verificacao_nfse 	= cd_verificacao_nfse_p,
		dt_emissao_nfe		= dt_emissao_nfe_w,
		ds_link_rps		= ds_link_rps_w
	where	nr_nota_fiscal		= nr_nota_fiscal_p
	and	cd_serie_nf		= cd_serie_nf_p
	and	((cd_cgc_emitente	= cd_cgc_emitente_w) or (cd_cgc_emitente_w = 'X'))
	and	ie_tipo_nota in ('SE','SD','SF','ST')
	and	ie_situacao = 1;
	
	
else
	update	nota_fiscal
	set 	nr_nfe_imp		= nr_nfe_imp_p,
		cd_verificacao_nfse 	= cd_verificacao_nfse_p,
		dt_emissao_nfe		= dt_emissao_nfe_w,
		dt_atualizacao_estoque	= sysdate,
		ie_situacao		= '1',
		ds_link_rps		= ds_link_rps_w
	where	nr_nota_fiscal		= nr_nota_fiscal_p
	and	cd_serie_nf		= cd_serie_nf_p
	and	((cd_cgc_emitente	= cd_cgc_emitente_w) or (cd_cgc_emitente_w = 'X'))
	and	ie_tipo_nota in ('SE','SD','SF','ST')
	and	ie_situacao = 8;

end if;


commit;

end atualiza_dados_nfse_link;
/
