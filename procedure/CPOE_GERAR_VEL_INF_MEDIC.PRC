create or replace procedure cpoe_gerar_vel_inf_medic(
						nr_atendimento_p		cpoe_material.nr_atendimento%type, 
						cd_material_p			cpoe_material.cd_material%type, 
						qt_dose_p				cpoe_material.qt_dose%type, 
						qt_tempo_aplicacao_p	number, 
						qt_hora_aplicacao_p		number, 
						ie_via_aplicacao_p		cpoe_material.ie_via_aplicacao%type, 
						ie_se_necessario_p		cpoe_material.ie_se_necessario%type, 
						cd_intervalo_p			cpoe_material.cd_intervalo%type, 
						ie_bomba_infusao_p		cpoe_material.ie_bomba_infusao%type, 
						ie_tipo_dosagem_p		cpoe_material.ie_tipo_dosagem%type, 
						qt_dosagem_p			cpoe_material.qt_dosagem%type, 
						cd_perfil_p				perfil.cd_perfil%type, 
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type, 
						nm_usuario_p			usuario.nm_usuario%type, 
						cd_pessoa_fisica_p		cpoe_material.cd_pessoa_fisica%type, 
						ds_retorno_p			out varchar2, 
						qt_vel_infusao_p		out number, 
						ie_tipo_dosagem_out_p	out varchar2) is 
 
ds_retorno_w				varchar2(4000) := ''; 
ds_tipo_dosagem_w			varchar2(255); 
qt_tempo_infusao_w			number(15,4); 
qt_tempo_aplicacao_w		number(15,4); 
qt_vel_infusao_w			number(15,4); 
ie_tipo_dosagem_w			varchar2(255); 
nr_atendimento_w			cpoe_material.nr_atendimento%type; 
cd_setor_atendimento_w		setor_atendimento.cd_setor_atendimento%type; 
cd_pessoa_fisica_w			pessoa_fisica.cd_pessoa_fisica%type; 
qt_peso_w					atendimento_sinal_vital.qt_peso%type; 
qt_vel_infusao_ww		    number(15,4):=0;
cd_tipo_dosagem_w           number(6):=0;
ds_comando_w                varchar(4000);
ds_erro_w				    varchar2(4000);
ds_parametros_w				varchar2(4000);
 
begin 
 
if (ie_tipo_dosagem_p is not null) then 
	ie_tipo_dosagem_w := ie_tipo_dosagem_p; 
else 
	if	(nvl(ie_bomba_infusao_p, '0') <> '0') then 
		ie_tipo_dosagem_w	:= obter_um_rep_dispositivo(ie_bomba_infusao_p, 1); 
	end if; 
 
	if (nvl(ie_tipo_dosagem_w,'X') = 'X') then 
		ie_tipo_dosagem_w	:= cpoe_obter_padrao_param_prescr( nr_atendimento_p, cd_material_p, ie_via_aplicacao_p, ie_se_necessario_p, 'TD', cd_intervalo_p, cd_estabelecimento_p, cd_perfil_p, nm_usuario_p, cd_pessoa_fisica_p); 
	end if; 
end if; 

                        
begin
	ds_comando_w :='begin  cpoe_gerar_vel_inf_medic_md(:qt_dose_p, :qt_tempo_aplicacao_p, :qt_hora_aplicacao_p, :ie_tipo_dosagem_w, :qt_dosagem_p, :qt_vel_infusao_ww, :cd_tipo_dosagem_w); end;';
	execute immediate ds_comando_w using in qt_dose_p,
										 in qt_tempo_aplicacao_p,
										 in qt_hora_aplicacao_p,
										 in ie_tipo_dosagem_w,
										 in qt_dosagem_p, 
										 out qt_vel_infusao_w, 
										 out cd_tipo_dosagem_w;
exception 
	when others then 
		ds_erro_w := substr(sqlerrm, 1, 4000);
		ds_parametros_w := substr('nr_atendimento_p= '||nr_atendimento_p||'-cd_material_p= '||cd_material_p||'-cd_pessoa_fisica_p= '||cd_pessoa_fisica_p||
								  '-qt_dose_p= '||qt_dose_p||'-qt_tempo_aplicacao_p= '||qt_tempo_aplicacao_p||'-qt_hora_aplicacao_p= '||qt_hora_aplicacao_p||
								  '-ie_tipo_dosagem_w= '||ie_tipo_dosagem_w||'-qt_dosagem_p= '||qt_dosagem_p, 1, 4000);
		gravar_log_medical_device('CPOE_GERAR_VEL_INF_MEDIC','CPOE_GERAR_VEL_INF_MEDIC_MD', ds_parametros_w, ds_erro_w, nm_usuario_p, 'S');
		cd_tipo_dosagem_w	:= null;	
		qt_vel_infusao_w 	:= null;
end;
                                    
ds_tipo_dosagem_w := substr(Wheb_mensagem_pck.get_texto(cd_tipo_dosagem_w),1,255); 

if (qt_vel_infusao_w > 0) then 
	ds_retorno_w := ' '||Wheb_mensagem_pck.get_texto(284389)||' '||lower(Wheb_mensagem_pck.get_texto(308609))||': '|| trim(to_char(nvl(qt_vel_infusao_w,0),'999990D99'))|| ' ' ||ds_tipo_dosagem_w; 
end if; 
 
ds_retorno_p := ds_retorno_w; 
qt_vel_infusao_p := qt_vel_infusao_w; 
ie_tipo_dosagem_out_p := ie_tipo_dosagem_w; 
 
end	cpoe_gerar_vel_inf_medic;
/
