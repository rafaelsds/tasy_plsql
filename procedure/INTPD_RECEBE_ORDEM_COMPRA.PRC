CREATE OR REPLACE 
procedure intpd_recebe_ordem_compra(	nr_sequencia_p in number, xml_p in xmltype) is

ie_conversao_w			intpd_eventos_sistema.ie_conversao%type;
nr_seq_projeto_xml_w		intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_sistema_w			intpd_eventos_sistema.nr_seq_sistema%type;
ie_sistema_externo_w		varchar2(15);
qt_registros_w			number(10);
nr_item_oci_w			ordem_compra_item.nr_item_oci%type;
reg_integracao_w			gerar_int_padrao.reg_integracao_conv;
nr_seq_regra_conv_w		conversao_meio_externo.nr_seq_regra%type;
ordem_compra_w			ordem_compra%rowtype;
ordem_compra_venc_w		ordem_compra_venc%rowtype;
ordem_compra_item_w		ordem_compra_item%rowtype;
ordem_compra_item_entrega_w	ordem_compra_item_entrega%rowtype;
ordem_compra_item_trib_w		ordem_compra_item_trib%rowtype;
nr_ordem_compra_delete_w		ordem_compra.nr_ordem_compra%type;
nr_documento_externo_w			ordem_compra.nr_documento_externo%type;
ds_id_origin_w			intpd_eventos_sistema.ds_id_origin%type;
qt_registros_item_w		number(10);
nr_cot_compra_w			cot_compra.nr_cot_compra%type;
nr_solic_compra_w		solic_compra.nr_solic_compra%type;
cd_estabelecimento_w	number(15); 
i				integer;
ie_erro_w		varchar2(1) := 'N';
nr_seq_motivo_cancel_ordem_w	parametro_compras.nr_seq_motivo_cancel_ordem%type;
dt_baixa_w			ordem_compra.dt_baixa%type;
param80				varchar2(1);
nr_cot_w			cot_compra.nr_cot_compra%type;


cursor c01 is
select	*
from	xmltable('/STRUCTURE/PURCHASE' passing xml_p columns
	ie_action				varchar2(15)	path	'IE_ACTION',
	cd_estabelecimento			varchar2(40)	path	'CD_ESTABLISHMENT',
	nr_documento_externo		varchar2(10)	path	'NR_EXTERNAL_DOCUMENT',
	cd_cgc_fornecedor			varchar2(40)	path	'CD_SUPPLIER',
	cd_pessoa_fisica			varchar2(40)	path	'CD_NATURAL_PERSON',
	cd_condicao_pagamento		varchar2(40)	path	'CD_PAYMENTTERM',
	cd_comprador			varchar2(40)	path	'CD_BUYER',
	cd_moeda			varchar2(40)	path	'CD_CURRENCY',
	dt_ordem_compra			varchar2(40)	path	'DT_PURCHASE_ORDER', /*???*/
	dt_entrega			varchar2(40)	path	'DT_DELIVERY', /*???*/
	dt_emissao			varchar2(40)	path	'DT_ISSUE', /*???*/
	cd_pessoa_solicitante		varchar2(40)	path	'CD_REQUESTING_PERSON',
	cd_cgc_transportador		varchar2(40)	path	'CD_TRANSPORTER',
	cd_cnpj_seguradora		varchar2(40)	path	'CD_INSURER',
	ie_frete				varchar2(40)	path	'IE_SHIPPING',
	vl_frete				varchar2(40)	path	'VL_SHIPPING', 
	vl_desconto			varchar2(40)	path	'VL_DISCOUNT', 
	vl_despesa_doc			varchar2(40)	path	'VL_INVOICE_EXPENSE', 
	vl_seguro				varchar2(40)	path	'VL_INSURANCE', 
	vl_despesa_acessoria		varchar2(40)	path	'VL_ANCILLIARY_EXPENSE', 
	vl_conv_moeda			varchar2(40)	path	'VL_CURRENCY_CONVERSION',  
	pr_multa_atraso			varchar2(40)	path	'PR_FINE_DELAY',
	pr_desconto			varchar2(40)	path	'PR_DISCOUNT',
	pr_desc_pgto_antec		varchar2(40)	path	'PR_DISCOUNT_ANTICIP_PAYMENT', 
	pr_desc_financeiro			varchar2(40)	path	'PR_FINANCIAL_DISCOUNT', 
	pr_juros_negociado			varchar2(40)	path	'PR_INTEREST_NEGOTIATED',  
	ds_pessoa_contato			varchar2(255)	path	'DS_CONTACT_PERSON',
	ds_observacao			varchar2(4000)	path	'DS_OBSERVATION',
	ds_note				varchar2(4000)	path	'DS_NOTE',
	ds_obs_interna			varchar2(4000)	path	'DS_INTERNAL_NOTE',
	ie_tipo_ordem			varchar2(15)	path	'IE_ORDER_TYPE',
	ie_aviso_chegada			varchar2(15)	path	'IE_ARRIVAL_NOTIFICATION',
	ie_emite_obs			varchar2(15)	path	'IE_ISSUES_OBSERVATION',
	ie_urgente			varchar2(15)	path	'IE_URGENT',
	ie_somente_pagto			varchar2(15)	path	'IE_ONLY_PAYMENT',
	cd_local_entrega			varchar2(40)	path	'CD_DELIVERY_SITE',
	cd_setor_atendimento		varchar2(40)	path	'CD_ENCOUNTER_DEPARTMENT',
	cd_setor_entrega			varchar2(40)	path	'CD_DELIVERY_DEPARTMENT',
	cd_convenio			varchar2(40)	path	'CD_INSURANCE',
	nr_seq_subgrupo_compra		varchar2(40)	path	'NR_PURCHASE_SUBGROUP',
	nr_seq_forma_pagto		varchar2(40)	path	'NR_PAYMENT_METHOD',
	nr_seq_tipo_compra		varchar2(40)	path	'NR_PURCHASE_TYPE',
	nr_seq_mod_compra		varchar2(40)	path	'NR_PURCHASE_MODALITY',
	nr_seq_motivo_urgente		varchar2(40)	path	'NR_URGENT_REASON',
	nr_seq_motivo_solic		varchar2(40)	path	'NR_REASON_REQUEST',
	cd_estab_transf			varchar2(40)	path	'CD_TRANSFER_ESTABLISHMENT',
	cd_local_transf			varchar2(40)	path	'CD_TRANSFER_LOCATION',
	nm_usuario			varchar2(40)	path	'NM_USER',
	gtr_estab			varchar2(40)	path	'GTR_ESTAB',
	dt_baixa			varchar2(40)	path	'DT_SETTLEMENT',
	nr_seq_motivo_cancel		varchar2(40)	path	'NR_CANCELLATION_REASON',
	ie_released					varchar2(250)	path	'IE_RELEASED',
	xml_ordem_vencimentos		xmltype		path	'EXPIRATIONS',
	xml_ordem_itens			xmltype 		path	'ITEMS');

c01_w	c01%rowtype;


cursor c02 is
select	*
from	xmltable('/EXPIRATIONS/EXPIRATION' passing c01_w.xml_ordem_vencimentos columns
	dt_vencimento				varchar2(40)	path	'DT_EXPIRATION',
	vl_vencimento				varchar2(40)	path	'VL_EXPIRATION',
	ds_observacao				varchar2(255)	path	'DS_OBSERVATION',
	ds_note					varchar2(255)	path	'DS_NOTE');

c02_w	c02%rowtype;


cursor c03 is
select	*
from	xmltable('/ITEMS/ITEM' passing c01_w.xml_ordem_itens columns
	nr_item_oci				number(5)	path	'NR_ITEM',
	cd_material				varchar2(40)	path	'CD_MATERIAL',
	cd_unidade_medida_compra			varchar2(40)	path	'CD_PURCHASE_UNIT_MEASUREMENT',
	vl_unitario_material				varchar2(40)	path	'VL_UNITARY_VALUE',
	qt_material				varchar2(40)	path	'QT_MATERIAL',
	pr_descontos				varchar2(40)	path	'PR_DISCOUNT',
	vl_desconto				varchar2(40)	path	'VL_DISCOUNT',
	cd_centro_custo				varchar2(40)	path	'CD_COST_CENTER',
	cd_conta_contabil				varchar2(40)	path	'CD_BOOKKEEPING_ACCOUNT',
	cd_local_estoque				varchar2(40)	path	'CD_STOCK_LOCATION',
	ds_material_direto				varchar2(255)	path	'DS_DIRECT_MATERIAL',
	ds_observacao				varchar2(255)	path	'DS_OBSERVATION',
	ds_note					varchar2(255)	path	'DS_NOTE',
	ds_obs_item_forn				varchar2(255)	path	'DS_SUPPLY_ITEM_OBSERVATION',
	nr_solic_compra				number(10)	path	'NR_PURCHASE_REQUEST',
	nr_item_solic_compra			number(5)	path	'NR_ITEM_PURCHASE_REQUEST',
	nr_cot_compra				number(10)	path	'NR_PURCHASE_QUOTATION',
	nr_item_cot_compra			number(5)	path	'NR_ITEM_PURCHASE_QUOTATION',
	ds_marca					varchar2(30)	path	'DS_BRAND',
	nr_seq_marca				varchar2(40)	path	'NR_BRAND',
	pr_desc_financ				varchar2(40)	path	'PR_FINANCIAL_DISCOUNT',
	nr_seq_conta_financ			varchar2(40)	path	'NR_FINANCIAL_ACCOUNT',
	nr_seq_criterio_rateio			varchar2(40)	path	'NR_APPORTIONMENT_CRITERION',
	nr_serie_material				varchar2(80)	path	'NR_MATERIAL_SERIES',
	nr_seq_conta_bco				varchar2(40)	path	'NR_BANK_ACCOUNT',
	dt_inicio_garantia				varchar2(40)	path	'DT_START_WARRANTY',
	dt_fim_garantia				varchar2(40)	path	'DT_END_WARRANTY',
	qt_dias_garantia				number(10)	path	'QT_WARRANTY_DAYS',
	ds_lote					varchar2(25)	path	'DS_SUPPLIER_BATCH',
	dt_validade				varchar2(40)	path	'DT_VALIDITY',
	ie_servico_realizado			varchar2(15)	path	'IE_SERVICE_LOCATION',
	nr_documento_externo_item		varchar2(100)	path	'NR_EXTERNAL_DOCUMENT',
	xml_ordem_itens_entregas			xmltype 		path	'DELIVERIES',
	xml_ordem_itens_tributos			xmltype 		path	'TAXES');

c03_w	c03%rowtype;


cursor c04 is
select	*
from	xmltable('/DELIVERIES/DELIVERY' passing c03_w.xml_ordem_itens_entregas columns
	dt_prevista_entrega				varchar2(40)	path	'DT_DELIVERY_SCHEDULED',
	qt_prevista_entrega				varchar2(40)	path	'QT_DELIVERY_SCHEDULED',
	ds_observacao					varchar2(255)	path	'DS_OBSERVATION',
	ds_note						varchar2(255)	path	'DS_NOTE',
	dt_cancelamento					varchar2(40)	path	'DT_CANCELLATION');

c04_w	c04%rowtype;

cursor c05 is
select	*
from	xmltable('/TAXES/TAX' passing c03_w.xml_ordem_itens_tributos columns
	cd_tributo				varchar2(40)	path	'CD_TAX',
	pr_tributo				varchar2(40)	path	'PR_TAX',
	vl_tributo				varchar2(40)	path	'VL_TAX',
	ds_observacao			varchar2(255)	path	'DS_OBSERVATION',
	ds_note				varchar2(255)	path	'DS_NOTE');

c05_w	c05%rowtype;

begin

select	id_origin
into	ds_id_origin_w
from	xmltable('/STRUCTURE' passing xml_p
	columns id_origin	varchar2(10) path 'ID_ORIGIN');

select	nvl(b.ie_conversao,'I'),
	nr_seq_sistema,
	nr_seq_projeto_xml,
	nr_seq_regra_conv,
	nvl(ds_id_origin,ds_id_origin_w)
into	ie_conversao_w,
	nr_seq_sistema_w,
	nr_seq_projeto_xml_w,
	nr_seq_regra_conv_w,
	ds_id_origin_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;

ie_sistema_externo_w	:=	nr_seq_sistema_w;

reg_integracao_w.nr_seq_fila_transmissao	:=	nr_sequencia_p;
reg_integracao_w.ie_envio_recebe		:=	'R';
reg_integracao_w.ie_sistema_externo		:=	ie_sistema_externo_w;
reg_integracao_w.ie_conversao		:=	ie_conversao_w;
reg_integracao_w.nr_seq_projeto_xml		:=	nr_seq_projeto_xml_w;
reg_integracao_w.nr_seq_regra_conversao	:=	nr_seq_regra_conv_w;


open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;
	begin
	ie_erro_w := 'N';
	c01_w.ds_observacao	:=	nvl(c01_w.ds_observacao, c01_w.ds_note);

	select	nr_seq_motivo_cancel_ordem 
	into	nr_seq_motivo_cancel_ordem_w
	from	parametro_compras
	where	cd_estabelecimento = c01_w.cd_estabelecimento;
	
	if	(c01_w.ie_action = 'DELETE') then

		nr_documento_externo_w := c01_w.nr_documento_externo;
		cd_estabelecimento_w := c01_w.cd_estabelecimento;

		select	nvl(max(nr_ordem_compra),0)
		into	nr_ordem_compra_delete_w
		from	ordem_compra
		where	nr_documento_externo = nr_documento_externo_w
		and 	cd_estabelecimento = cd_estabelecimento_w  
		and	ie_sistema_origem = ie_sistema_externo_w;

		if	(nr_ordem_compra_delete_w > 0) then

			select	count(*)
			into	qt_registros_w
			from	nota_fiscal_item a,
				nota_fiscal b
			where	a.nr_ordem_compra = nr_ordem_compra_delete_w
			and 	a.nr_sequencia = b.nr_sequencia  
			and 	b.ie_situacao = 1 /* Situacao Ativa */
			and	b.dt_atualizacao_estoque is not null; /* Data de atualizacao estoque indica calculo da nota */ 
			
			if	(qt_registros_w = 0) then
				begin
					delete from ordem_compra where	nr_ordem_compra = nr_ordem_compra_delete_w;
					exception
						when others then
						
						select nvl(max(obter_valor_param_usuario(917, 80, null, c01_w.nm_usuario, c01_w.cd_estabelecimento)),'S')
						into param80
						from dual;
						
						select	count(*) 
						into	qt_registros_w
						from 	ordem_compra_adiant_pago 
						where 	nr_ordem_compra = nr_ordem_compra_delete_w;
						
						select 	dt_baixa
						into	dt_baixa_w
						from	ordem_compra
						where	nr_ordem_compra = nr_ordem_compra_delete_w;

						if (param80 = 'N' and qt_registros_w > 0) then
							ie_erro_w := 'S';
							intpd_gravar_log_recebimento(nr_sequencia_p,wheb_mensagem_pck.get_Texto(162206),c01_w.nm_usuario);
							/* Esta ordem nao pode ser cancelada, pois possui adiantamento vinculado. Verifique o parametro [80]. */							
						elsif (dt_baixa_w is not null) then
							ie_erro_w := 'S';
							intpd_gravar_log_recebimento(nr_sequencia_p,wheb_mensagem_pck.get_Texto(162218),c01_w.nm_usuario);
							/* Esta ordem nao pode ser cancelada, pois ja esta baixada.*/
						else
							cancelar_ordem_compra(nr_ordem_compra_delete_w,
							nr_seq_motivo_cancel_ordem_w,
							'N',
							'N',
							'N',
							c01_w.nm_usuario,
							obter_desc_expressao(840203),
							'N',
							nr_cot_w);
						end if;							
				end;
				
			else
				ie_erro_w := 'S';
				intpd_gravar_log_recebimento(nr_sequencia_p,wheb_mensagem_pck.get_Texto(839690),c01_w.nm_usuario);
				/*Nao e permitido excluir essa ordem de compra, pois ela ja esta vinculada a uma nota fiscal.*/				
			end if;	
		end if;

	else

		reg_integracao_w.nm_tabela		:=	'ORDEM_COMPRA';
		reg_integracao_w.nm_elemento		:=	'PURCHASE';
		reg_integracao_w.nr_seq_visao		:=	17575;

		intpd_processar_atributo(reg_integracao_w, 'CD_ESTABELECIMENTO', c01_w.cd_estabelecimento, 'S', ordem_compra_w.cd_estabelecimento);
		intpd_processar_atributo(reg_integracao_w, 'NR_DOCUMENTO_EXTERNO', c01_w.nr_documento_externo, 'N', ordem_compra_w.nr_documento_externo);
		intpd_processar_atributo(reg_integracao_w, 'CD_CGC_FORNECEDOR', c01_w.cd_cgc_fornecedor, 'S', ordem_compra_w.cd_cgc_fornecedor);
		intpd_processar_atributo(reg_integracao_w, 'CD_PESSOA_FISICA', c01_w.cd_pessoa_fisica, 'S', ordem_compra_w.cd_pessoa_fisica);
		intpd_processar_atributo(reg_integracao_w, 'CD_CONDICAO_PAGAMENTO', c01_w.cd_condicao_pagamento, 'S', ordem_compra_w.cd_condicao_pagamento);

		if	(c01_w.cd_comprador is not null) then
			intpd_processar_atributo(reg_integracao_w, 'CD_COMPRADOR', c01_w.cd_comprador, 'S', ordem_compra_w.cd_comprador);
		else
			select	max(cd_comprador_padrao)
			into	ordem_compra_w.cd_comprador
			from	parametro_compras
			where	cd_estabelecimento = ordem_compra_w.cd_estabelecimento;
		end if;

		intpd_processar_atributo(reg_integracao_w, 'CD_MOEDA', c01_w.cd_moeda, 'S', ordem_compra_w.cd_moeda);
		intpd_processar_atributo(reg_integracao_w, 'DT_ORDEM_COMPRA', c01_w.dt_ordem_compra, 'N', ordem_compra_w.dt_ordem_compra);
		intpd_processar_atributo(reg_integracao_w, 'DT_ENTREGA', c01_w.dt_entrega, 'N', ordem_compra_w.dt_entrega);
		intpd_processar_atributo(reg_integracao_w, 'DT_EMISSAO', c01_w.dt_emissao, 'N', ordem_compra_w.dt_emissao);

		if	(c01_w.cd_pessoa_solicitante is not null) then
			intpd_processar_atributo(reg_integracao_w, 'CD_PESSOA_SOLICITANTE', c01_w.cd_pessoa_solicitante, 'S', ordem_compra_w.cd_pessoa_solicitante);
		else
			select	max(cd_pessoa_solic_padrao)
			into	ordem_compra_w.cd_pessoa_solicitante
			from	parametro_compras
			where	cd_estabelecimento = ordem_compra_w.cd_estabelecimento;
		end if;

		intpd_processar_atributo(reg_integracao_w, 'CD_CGC_TRANSPORTADOR', c01_w.cd_cgc_transportador, 'S', ordem_compra_w.cd_cgc_transportador);
		intpd_processar_atributo(reg_integracao_w, 'CD_CNPJ_SEGURADORA', c01_w.cd_cnpj_seguradora, 'S', ordem_compra_w.cd_cnpj_seguradora);
		intpd_processar_atributo(reg_integracao_w, 'IE_FRETE', c01_w.ie_frete, 'S', ordem_compra_w.ie_frete);
		intpd_processar_atributo(reg_integracao_w, 'VL_FRETE', c01_w.vl_frete, 'N', ordem_compra_w.vl_frete);
		intpd_processar_atributo(reg_integracao_w, 'VL_DESCONTO', c01_w.vl_desconto, 'N', ordem_compra_w.vl_desconto);
		intpd_processar_atributo(reg_integracao_w, 'VL_DESPESA_DOC', c01_w.vl_despesa_doc, 'N', ordem_compra_w.vl_despesa_doc);
		intpd_processar_atributo(reg_integracao_w, 'VL_SEGURO', c01_w.vl_seguro, 'N', ordem_compra_w.vl_seguro);
		intpd_processar_atributo(reg_integracao_w, 'VL_DESPESA_ACESSORIA', c01_w.vl_despesa_acessoria, 'N', ordem_compra_w.vl_despesa_acessoria);
		intpd_processar_atributo(reg_integracao_w, 'VL_CONV_MOEDA', c01_w.vl_conv_moeda, 'N', ordem_compra_w.vl_conv_moeda);
		intpd_processar_atributo(reg_integracao_w, 'PR_MULTA_ATRASO', c01_w.pr_multa_atraso, 'N', ordem_compra_w.pr_multa_atraso);
		intpd_processar_atributo(reg_integracao_w, 'PR_DESCONTO', c01_w.pr_desconto, 'N', ordem_compra_w.pr_desconto);
		intpd_processar_atributo(reg_integracao_w, 'PR_DESC_PGTO_ANTEC', c01_w.pr_desc_pgto_antec, 'N', ordem_compra_w.pr_desc_pgto_antec);
		intpd_processar_atributo(reg_integracao_w, 'PR_DESC_FINANCEIRO', c01_w.pr_desc_financeiro, 'N', ordem_compra_w.pr_desc_financeiro);
		intpd_processar_atributo(reg_integracao_w, 'PR_JUROS_NEGOCIADO', c01_w.pr_juros_negociado, 'N', ordem_compra_w.pr_juros_negociado);
		intpd_processar_atributo(reg_integracao_w, 'DS_PESSOA_CONTATO', c01_w.ds_pessoa_contato, 'N', ordem_compra_w.ds_pessoa_contato);
		intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c01_w.ds_observacao, 'N', ordem_compra_w.ds_observacao);
		intpd_processar_atributo(reg_integracao_w, 'DS_OBS_INTERNA', c01_w.ds_obs_interna, 'N', ordem_compra_w.ds_obs_interna);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO', c01_w.nm_usuario, 'N', ordem_compra_w.nm_usuario);
		intpd_processar_atributo(reg_integracao_w, 'IE_TIPO_ORDEM', c01_w.ie_tipo_ordem, 'N', ordem_compra_w.ie_tipo_ordem);
		intpd_processar_atributo(reg_integracao_w, 'IE_AVISO_CHEGADA', c01_w.ie_aviso_chegada, 'N', ordem_compra_w.ie_aviso_chegada);
		intpd_processar_atributo(reg_integracao_w, 'IE_EMITE_OBS', c01_w.ie_emite_obs, 'N', ordem_compra_w.ie_emite_obs);
		intpd_processar_atributo(reg_integracao_w, 'IE_URGENTE', c01_w.ie_urgente, 'N', ordem_compra_w.ie_urgente);
		intpd_processar_atributo(reg_integracao_w, 'IE_SOMENTE_PAGTO', c01_w.ie_somente_pagto, 'N', ordem_compra_w.ie_somente_pagto);
		intpd_processar_atributo(reg_integracao_w, 'CD_LOCAL_ENTREGA', c01_w.cd_local_entrega, 'S', ordem_compra_w.cd_local_entrega);
		intpd_processar_atributo(reg_integracao_w, 'CD_SETOR_ATENDIMENTO', c01_w.cd_setor_atendimento, 'S', ordem_compra_w.cd_setor_atendimento);
		intpd_processar_atributo(reg_integracao_w, 'CD_SETOR_ENTREGA', c01_w.cd_setor_entrega, 'S', ordem_compra_w.cd_setor_entrega);
		intpd_processar_atributo(reg_integracao_w, 'CD_CONVENIO', c01_w.cd_convenio, 'S', ordem_compra_w.cd_convenio);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_SUBGRUPO_COMPRA', c01_w.nr_seq_subgrupo_compra, 'S', ordem_compra_w.nr_seq_subgrupo_compra);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_FORMA_PAGTO', c01_w.nr_seq_forma_pagto, 'S', ordem_compra_w.nr_seq_forma_pagto);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_TIPO_COMPRA', c01_w.nr_seq_tipo_compra, 'S', ordem_compra_w.nr_seq_tipo_compra);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOD_COMPRA', c01_w.nr_seq_mod_compra, 'S', ordem_compra_w.nr_seq_mod_compra);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOTIVO_URGENTE', c01_w.nr_seq_motivo_urgente, 'S', ordem_compra_w.nr_seq_motivo_urgente);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOTIVO_SOLIC', c01_w.nr_seq_motivo_solic, 'S', ordem_compra_w.nr_seq_motivo_solic);
		intpd_processar_atributo(reg_integracao_w, 'DT_BAIXA', c01_w.dt_baixa, 'N', ordem_compra_w.dt_baixa);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOTIVO_CANCEL', c01_w.nr_seq_motivo_cancel, 'S', ordem_compra_w.nr_seq_motivo_cancel);
		intpd_processar_atributo(reg_integracao_w, 'CD_ESTAB_TRANSF', c01_w.cd_estab_transf, 'S', ordem_compra_w.cd_estab_transf);
		intpd_processar_atributo(reg_integracao_w, 'CD_LOCAL_TRANSF', c01_w.cd_local_transf, 'S', ordem_compra_w.cd_local_transf);
		intpd_processar_atributo(reg_integracao_w, 'IE_SISTEMA_ORIGEM', ie_sistema_externo_w, 'N', ordem_compra_w.ie_sistema_origem);
		ordem_compra_w.ie_origem_imp		:= ds_id_origin_w;
		ordem_compra_w.dt_atualizacao		:= sysdate;
		ordem_compra_w.nm_usuario_nrec		:= ordem_compra_w.nm_usuario;
		ordem_compra_w.dt_atualizacao_nrec		:= sysdate;
		ordem_compra_w.dt_inclusao			:= sysdate;
		ordem_compra_w.ie_situacao			:= 'A';

		if	(reg_integracao_w.qt_reg_log = 0) then

			select	nvl(max(nr_ordem_compra),0)
			into	ordem_compra_w.nr_ordem_compra
			from	ordem_compra
			where	nr_documento_externo = ordem_compra_w.nr_documento_externo
			and 	cd_estabelecimento = ordem_compra_w.cd_estabelecimento
			and	ie_sistema_origem = ordem_compra_w.ie_sistema_origem;

			if	(ordem_compra_w.nr_ordem_compra > 0) then

				delete ordem_compra_venc where nr_ordem_compra = ordem_compra_w.nr_ordem_compra;

				update	ordem_compra
				set	row = ordem_compra_w
				where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra;

				if (nvl(c01_w.ie_released,'S') = 'N') then
					select	count(*)
					into	qt_registros_w
					from	nota_fiscal y,
						nota_fiscal_item z
					where	y.nr_sequencia = z.nr_sequencia
					and 	y.nr_ordem_compra = ordem_compra_w.nr_ordem_compra
					and 	y.ie_situacao = 1 /* Situacao Ativa */
					and	y.dt_atualizacao_estoque is not null; /* Data de atualizacao estoque indica calculo da nota */

					if	(qt_registros_w = 0) then
						estornar_lib_ordem_compra(ordem_compra_w.nr_ordem_compra, null, null, ordem_compra_w.nm_usuario);
					else
						ie_erro_w := 'S';
						intpd_gravar_log_recebimento(nr_sequencia_p,wheb_mensagem_pck.get_Texto(1028674),c01_w.nm_usuario);
						/* Esta ordem de compra nao pode ser estornada, pois ja esta vinculada a uma nota fiscal. */
					end if;
				end if;
			else
				select	ordem_compra_seq.nextval
				into	ordem_compra_w.nr_ordem_compra
				from	dual;

				insert into ordem_compra values ordem_compra_w;
			end if;
		end if;

		open C02;
		loop
		fetch C02 into
			c02_w;
		exit when C02%notfound;
			begin
			c02_w.ds_observacao	:=	nvl(c02_w.ds_observacao, c02_w.ds_note);

			reg_integracao_w.nm_tabela		:=	'ORDEM_COMPRA_VENC';
			reg_integracao_w.nm_elemento	:=	'EXPIRATION';
			reg_integracao_w.nr_seq_visao	:=	16612;

			intpd_processar_atributo(reg_integracao_w, 'DT_VENCIMENTO', c02_w.dt_vencimento, 'N', ordem_compra_venc_w.dt_vencimento);
			intpd_processar_atributo(reg_integracao_w, 'VL_VENCIMENTO', c02_w.vl_vencimento, 'N', ordem_compra_venc_w.vl_vencimento);
			intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c02_w.ds_observacao, 'N', ordem_compra_venc_w.ds_observacao);
			ordem_compra_venc_w.nr_ordem_compra	:= ordem_compra_w.nr_ordem_compra;
			ordem_compra_venc_w.dt_atualizacao	:= sysdate;
			ordem_compra_venc_w.nm_usuario		:= ordem_compra_w.nm_usuario;

			if	(reg_integracao_w.qt_reg_log = 0) then

				select	nvl(max(nr_item_oc_venc),0)
				into	ordem_compra_venc_w.nr_item_oc_venc
				from	ordem_compra_venc
				where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra
				and	pkg_date_utils.start_of(dt_vencimento, 'DD', null) = pkg_date_utils.start_of(ordem_compra_venc_w.dt_vencimento, 'DD', null);

				if	(ordem_compra_venc_w.nr_item_oc_venc > 0) then

					update	ordem_compra_venc
					set	row = ordem_compra_venc_w
					where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra
					and	nr_item_oc_venc = ordem_compra_venc_w.nr_item_oc_venc;

				else

					select	nvl(max(nr_item_oc_venc),0) +1
					into	ordem_compra_venc_w.nr_item_oc_venc
					from	ordem_compra_venc
					where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra;

					insert into ordem_compra_venc values ordem_compra_venc_w;

				end if;
			end if;
			end;
		end loop;
		close C02;

		/*
			PROCESSO VERIFICA SE EXISTE RECEBIMENTO DE NOTA FISCAL PARA ALGUM DOS ITENS, CASO SIM, ENTRA NO CURSOR SOMENTE PARA ATUALIZACAO DA ENTREGA DO ITEM, NOS CASOS DE DESISTENCIA DE RECEBIMENTO.
			CASO NAO EXISTA RECEBIMENTO, EXCLUI TODOS OS ITENS DA ORDEM DE COMPRA E RECRIA DE ACORDO COM OS DADOS CONTIDOS NO XML. --> JOAO VERISIMO <--
		*/

		select	count(*)
		into	qt_registros_item_w
		from	nota_fiscal_item a,
			nota_fiscal b
		where	a.nr_ordem_compra = ordem_compra_w.nr_ordem_compra
		and 	a.nr_sequencia = b.nr_sequencia
		and 	b.ie_situacao = 1 /* Situacao Ativa */
		and	b.dt_atualizacao_estoque is not null; /* Data de atualizacao estoque indica calculo da nota */

		if	(qt_registros_item_w > 0) then 

			open C03;
			loop
			fetch C03 into
				c03_w;
			exit when C03%notfound;
				begin
				c03_w.ds_observacao	:=	nvl(c03_w.ds_observacao, c03_w.ds_note);

				reg_integracao_w.nm_tabela	:=	'ORDEM_COMPRA_ITEM';
				reg_integracao_w.nm_elemento	:=	'ITEM';
				reg_integracao_w.nr_seq_visao	:=	16711;

				intpd_processar_atributo(reg_integracao_w, 'NR_ITEM_OCI', c03_w.nr_item_oci, 'N', ordem_compra_item_w.nr_item_oci);


				open C04;
				loop
				fetch C04 into
					c04_w;
				exit when C04%notfound;
					begin
					c04_w.ds_observacao	:=	nvl(c04_w.ds_observacao, c04_w.ds_note);

					reg_integracao_w.nm_tabela		:=	'ORDEM_COMPRA_ITEM_ENTREGA';
					reg_integracao_w.nm_elemento	:=	'DELIVERY';
					reg_integracao_w.nr_seq_visao	:=	16712;

					intpd_processar_atributo(reg_integracao_w, 'DT_PREVISTA_ENTREGA', c04_w.dt_prevista_entrega, 'N', ordem_compra_item_entrega_w.dt_prevista_entrega);
					intpd_processar_atributo(reg_integracao_w, 'QT_PREVISTA_ENTREGA', c04_w.qt_prevista_entrega, 'N', ordem_compra_item_entrega_w.qt_prevista_entrega);
					intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c04_w.ds_observacao, 'N', ordem_compra_item_entrega_w.ds_observacao);
					intpd_processar_atributo(reg_integracao_w, 'DT_CANCELAMENTO', c04_w.dt_cancelamento, 'N', ordem_compra_item_entrega_w.dt_cancelamento);
					ordem_compra_item_entrega_w.nr_ordem_compra		:= ordem_compra_w.nr_ordem_compra;
					ordem_compra_item_entrega_w.nr_item_oci		:= ordem_compra_item_w.nr_item_oci;
					ordem_compra_item_entrega_w.dt_atualizacao		:= sysdate;
					ordem_compra_item_entrega_w.nm_usuario		:= ordem_compra_w.nm_usuario;
					ordem_compra_item_entrega_w.dt_entrega_original	:= ordem_compra_item_entrega_w.dt_prevista_entrega;

					if	(reg_integracao_w.qt_reg_log = 0) then


						select	nvl(max(nr_sequencia),0)
						into	ordem_compra_item_entrega_w.nr_sequencia
						from	ordem_compra_item_entrega
						where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra
						and	nr_item_oci = c03_w.nr_item_oci; 						

						if	(ordem_compra_item_entrega_w.nr_sequencia > 0) and
						    (c04_w.dt_cancelamento is not null) then

							cancelar_entrega_item(
									ordem_compra_w.nr_ordem_compra,
									c03_w.nr_item_oci,
									ordem_compra_item_entrega_w.nr_sequencia,
									'N',
									'N',
									'N',
									ordem_compra_w.nm_usuario,
									null,
									nr_seq_motivo_cancel_ordem_w,
									'N',
									'N',
									null,
									null,
									nr_cot_compra_w,
									nr_solic_compra_w);

						end if;
					end if;
					end;
				
					ordem_compra_item_entrega_w.dt_cancelamento := null;
				end loop;
				close C04;

				end;
			end loop;
			close C03;

		elsif (qt_registros_item_w = 0) then 

			compras_pck.set_is_oci_delete('N');
			
			delete from ordem_compra_item where nr_ordem_compra = ordem_compra_w.nr_ordem_compra;

			compras_pck.set_is_oci_delete('S');
 
			open C03;
			loop
			fetch C03 into
				c03_w;
			exit when C03%notfound;
				begin
				c03_w.ds_observacao	:=	nvl(c03_w.ds_observacao, c03_w.ds_note);
				
				reg_integracao_w.nm_tabela		:=	'ORDEM_COMPRA_ITEM';
				reg_integracao_w.nm_elemento	:=	'ITEM';
				reg_integracao_w.nr_seq_visao	:=	16711;

				intpd_processar_atributo(reg_integracao_w, 'NR_ITEM_OCI', c03_w.nr_item_oci, 'N', ordem_compra_item_w.nr_item_oci);
				intpd_processar_atributo(reg_integracao_w, 'CD_MATERIAL', c03_w.cd_material, 'S', ordem_compra_item_w.cd_material);
				intpd_processar_atributo(reg_integracao_w, 'CD_UNIDADE_MEDIDA_COMPRA', c03_w.cd_unidade_medida_compra, 'S', ordem_compra_item_w.cd_unidade_medida_compra);
				intpd_processar_atributo(reg_integracao_w, 'VL_UNITARIO_MATERIAL', c03_w.vl_unitario_material, 'N', ordem_compra_item_w.vl_unitario_material);
				intpd_processar_atributo(reg_integracao_w, 'QT_MATERIAL', c03_w.qt_material, 'N', ordem_compra_item_w.qt_material);
				intpd_processar_atributo(reg_integracao_w, 'PR_DESCONTOS', c03_w.pr_descontos, 'N', ordem_compra_item_w.pr_descontos);
				intpd_processar_atributo(reg_integracao_w, 'VL_DESCONTO', c03_w.vl_desconto, 'N', ordem_compra_item_w.vl_desconto);
				intpd_processar_atributo(reg_integracao_w, 'CD_CENTRO_CUSTO', nvl(bkf_obter_conv_interna(null, 'CENTRO_CUSTO', 'CD_CENTRO_CUSTO', c03_w.cd_centro_custo, null, nr_seq_regra_conv_w),c03_w.cd_centro_custo), 'S', ordem_compra_item_w.cd_centro_custo);
				intpd_processar_atributo(reg_integracao_w, 'CD_CONTA_CONTABIL', c03_w.cd_conta_contabil, 'S', ordem_compra_item_w.cd_conta_contabil);
				intpd_processar_atributo(reg_integracao_w, 'CD_LOCAL_ESTOQUE', c03_w.cd_local_estoque, 'S', ordem_compra_item_w.cd_local_estoque);
				intpd_processar_atributo(reg_integracao_w, 'DS_MATERIAL_DIRETO', c03_w.ds_material_direto, 'N', ordem_compra_item_w.ds_material_direto);
				intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c03_w.ds_observacao, 'N', ordem_compra_item_w.ds_observacao);
				intpd_processar_atributo(reg_integracao_w, 'DS_OBS_ITEM_FORN', c03_w.ds_obs_item_forn, 'N', ordem_compra_item_w.ds_obs_item_forn);
				intpd_processar_atributo(reg_integracao_w, 'DS_MARCA', c03_w.ds_marca, 'N', ordem_compra_item_w.ds_marca);
				intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MARCA', c03_w.nr_seq_marca, 'S', ordem_compra_item_w.nr_seq_marca);
				intpd_processar_atributo(reg_integracao_w, 'PR_DESC_FINANC', c03_w.pr_desc_financ, 'N', ordem_compra_item_w.pr_desc_financ);
				intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CONTA_FINANC', c03_w.nr_seq_conta_financ, 'S', ordem_compra_item_w.nr_seq_conta_financ);
				intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CRITERIO_RATEIO', c03_w.nr_seq_criterio_rateio, 'S', ordem_compra_item_w.nr_seq_criterio_rateio);
				intpd_processar_atributo(reg_integracao_w, 'NR_SERIE_MATERIAL', c03_w.nr_serie_material, 'N', ordem_compra_item_w.nr_serie_material);
				intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CONTA_BCO', c03_w.nr_seq_conta_bco, 'S', ordem_compra_item_w.nr_seq_conta_bco);
				intpd_processar_atributo(reg_integracao_w, 'DT_INICIO_GARANTIA', c03_w.dt_inicio_garantia, 'N', ordem_compra_item_w.dt_inicio_garantia);
				intpd_processar_atributo(reg_integracao_w, 'DT_FIM_GARANTIA', c03_w.dt_fim_garantia, 'N', ordem_compra_item_w.dt_fim_garantia);
				intpd_processar_atributo(reg_integracao_w, 'QT_DIAS_GARANTIA', c03_w.qt_dias_garantia, 'N', ordem_compra_item_w.qt_dias_garantia);
				intpd_processar_atributo(reg_integracao_w, 'DS_LOTE', c03_w.ds_lote, 'N', ordem_compra_item_w.ds_lote);
				intpd_processar_atributo(reg_integracao_w, 'DT_VALIDADE', c03_w.dt_validade, 'N', ordem_compra_item_w.dt_validade);
				intpd_processar_atributo(reg_integracao_w, 'IE_SERVICO_REALIZADO', c03_w.ie_servico_realizado, 'S', ordem_compra_item_w.ie_servico_realizado);
				intpd_processar_atributo(reg_integracao_w, 'NR_SOLIC_COMPRA', c03_w.nr_solic_compra, 'S', ordem_compra_item_w.nr_solic_compra);
				intpd_processar_atributo(reg_integracao_w, 'NR_COT_COMPRA', c03_w.nr_cot_compra, 'S', ordem_compra_item_w.nr_cot_compra);
				intpd_processar_atributo(reg_integracao_w, 'NR_DOCUMENTO_EXTERNO', c03_w.nr_documento_externo_item, 'S', ordem_compra_item_w.nr_documento_externo);
				ordem_compra_item_w.dt_atualizacao		:= sysdate;
				ordem_compra_item_w.nm_usuario		:= ordem_compra_w.nm_usuario;
				ordem_compra_item_w.ie_situacao		:= 'A';
				ordem_compra_item_w.nr_ordem_compra	:= ordem_compra_w.nr_ordem_compra;
				ordem_compra_item_w.cd_pessoa_solicitante	:= ordem_compra_w.cd_pessoa_solicitante;
				ordem_compra_item_w.qt_original		:= ordem_compra_item_w.qt_material;
				ordem_compra_item_w.vl_total_item	:= ordem_compra_item_w.qt_material * ordem_compra_item_w.vl_unitario_material;

				if	(nvl(ordem_compra_item_w.nr_solic_compra,0) <> 0) or
					(nvl(c03_w.nr_item_solic_compra,0) <> 0) then

					select	count(*)
					into	qt_registros_w
					from	solic_compra_item
					where	nr_solic_compra = ordem_compra_item_w.nr_solic_compra
					and	nr_item_solic_compra = c03_w.nr_item_solic_compra;

					if	(qt_registros_w > 0) then
						ordem_compra_item_w.nr_item_solic_compra := c03_w.nr_item_solic_compra;
					else
						ordem_compra_item_w.nr_solic_compra		:= null;
						ordem_compra_item_w.nr_item_solic_compra	:= null;
					end if;
				end if;

				if	(nvl(ordem_compra_item_w.nr_cot_compra,0) <> 0) or
					(nvl(c03_w.nr_item_cot_compra,0) <> 0) then

					select	count(*)
					into	qt_registros_w
					from	cot_compra_item
					where	nr_cot_compra = ordem_compra_item_w.nr_cot_compra
					and	nr_item_cot_compra = c03_w.nr_item_cot_compra;

					if	(qt_registros_w > 0) then
						ordem_compra_item_w.nr_item_cot_compra := c03_w.nr_item_cot_compra;
					else
						ordem_compra_item_w.nr_cot_compra		:= null;
						ordem_compra_item_w.nr_item_cot_compra	:= null;
					end if;
				end if;


				if	(reg_integracao_w.qt_reg_log = 0) then


					select	nvl(max(nr_item_oci),0)
					into	nr_item_oci_w
					from	ordem_compra_item
					where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra
					and		nr_item_oci = ordem_compra_item_w.nr_item_oci;


						if	(nr_item_oci_w > 0) then

							update	ordem_compra_item
							set	row = ordem_compra_item_w
							where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra
							and	nr_item_oci = nr_item_oci_w;
						else
							insert into ordem_compra_item values ordem_compra_item_w;		
						end if;

					end if;


				open C04;
				loop
				fetch C04 into
					c04_w;
				exit when C04%notfound;
					begin
					c04_w.ds_observacao	:=	nvl(c04_w.ds_observacao, c04_w.ds_note);

					reg_integracao_w.nm_tabela	:=	'ORDEM_COMPRA_ITEM_ENTREGA';
					reg_integracao_w.nm_elemento	:=	'DELIVERY';
					reg_integracao_w.nr_seq_visao	:=	16712;

					intpd_processar_atributo(reg_integracao_w, 'DT_PREVISTA_ENTREGA', c04_w.dt_prevista_entrega, 'N', ordem_compra_item_entrega_w.dt_prevista_entrega);
					intpd_processar_atributo(reg_integracao_w, 'QT_PREVISTA_ENTREGA', c04_w.qt_prevista_entrega, 'N', ordem_compra_item_entrega_w.qt_prevista_entrega);
					intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c04_w.ds_observacao, 'N', ordem_compra_item_entrega_w.ds_observacao);
					intpd_processar_atributo(reg_integracao_w, 'DT_CANCELAMENTO', c04_w.dt_cancelamento, 'N', ordem_compra_item_entrega_w.dt_cancelamento);
					ordem_compra_item_entrega_w.nr_ordem_compra		:= ordem_compra_w.nr_ordem_compra;
					ordem_compra_item_entrega_w.nr_item_oci		:= ordem_compra_item_w.nr_item_oci;
					ordem_compra_item_entrega_w.dt_atualizacao		:= sysdate;
					ordem_compra_item_entrega_w.nm_usuario		:= ordem_compra_w.nm_usuario;
					ordem_compra_item_entrega_w.dt_entrega_original	:= ordem_compra_item_entrega_w.dt_prevista_entrega;

					if	(reg_integracao_w.qt_reg_log = 0) then

						select	nvl(max(nr_sequencia),0)
						into	ordem_compra_item_entrega_w.nr_sequencia
						from	ordem_compra_item_entrega
						where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra
						and	nr_item_oci = ordem_compra_item_w.nr_item_oci
						and	pkg_date_utils.start_of(dt_prevista_entrega, 'DD', null) = pkg_date_utils.start_of(ordem_compra_item_entrega_w.dt_prevista_entrega, 'DD', null);



						if	(ordem_compra_item_entrega_w.nr_sequencia > 0) and
						    (c04_w.dt_cancelamento is not null) then -- JOAO VERISSIMO --


							cancelar_entrega_item(
									ordem_compra_w.nr_ordem_compra,
									nr_item_oci_w,
									ordem_compra_item_entrega_w.nr_sequencia,
									'N',
									'N',
									'N',
									ordem_compra_w.nm_usuario,
									null,
									nr_seq_motivo_cancel_ordem_w,
									'N',
									'N',
									null,
									null,
									nr_cot_compra_w,
									nr_solic_compra_w);


						elsif (ordem_compra_item_entrega_w.nr_sequencia > 0) and
						    (c04_w.dt_cancelamento is null) and 
						    (nvl(ordem_compra_item_entrega_w.qt_real_entrega,0) = 0) then 

							update	ordem_compra_item_entrega
							set	row = ordem_compra_item_entrega_w
							where	nr_sequencia = ordem_compra_item_entrega_w.nr_sequencia;

						else
							select	ordem_compra_item_entrega_seq.nextval
							into	ordem_compra_item_entrega_w.nr_sequencia
							from	dual;

							insert into ordem_compra_item_entrega values ordem_compra_item_entrega_w;
						end if;
					end if;
					end;
					
					ordem_compra_item_entrega_w.dt_cancelamento := null;
				end loop;
				close C04;


				open C05;
				loop
				fetch C05 into
					c05_w;
				exit when C05%notfound;
					begin
					c05_w.ds_observacao	:=	nvl(c05_w.ds_observacao, c05_w.ds_note);

					reg_integracao_w.nm_tabela		:=	'ORDEM_COMPRA_ITEM_TRIB';
					reg_integracao_w.nm_elemento	:=	'TAX';
					reg_integracao_w.nr_seq_visao	:=	16713;

					intpd_processar_atributo(reg_integracao_w, 'CD_TRIBUTO', c05_w.cd_tributo, 'S', ordem_compra_item_trib_w.cd_tributo);
					intpd_processar_atributo(reg_integracao_w, 'PR_TRIBUTO', c05_w.pr_tributo, 'N', ordem_compra_item_trib_w.pr_tributo);
					intpd_processar_atributo(reg_integracao_w, 'VL_TRIBUTO', c05_w.vl_tributo, 'N', ordem_compra_item_trib_w.vl_tributo);
					intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c05_w.ds_observacao, 'N', ordem_compra_item_trib_w.DS_OBSERVACAO);
					ordem_compra_item_trib_w.nr_ordem_compra		:= ordem_compra_w.nr_ordem_compra;
					ordem_compra_item_trib_w.nr_item_oci			:= ordem_compra_item_w.nr_item_oci;
					ordem_compra_item_trib_w.dt_atualizacao		:= sysdate;
					ordem_compra_item_trib_w.nm_usuario			:= ordem_compra_w.nm_usuario;
					ordem_compra_item_trib_w.dt_atualizacao_nrec		:= sysdate;
					ordem_compra_item_trib_w.nm_usuario_nrec		:= ordem_compra_w.nm_usuario;

					if	(reg_integracao_w.qt_reg_log = 0) and
						(ordem_compra_item_trib_w.cd_tributo > 0) then

						select	count(*)
						into	qt_registros_w
						from	ordem_compra_item_trib
						where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra
						and	nr_item_oci = ordem_compra_item_w.nr_item_oci
						and	cd_tributo = ordem_compra_item_trib_w.cd_tributo;

						if	(qt_registros_w > 0) then

							update	ordem_compra_item_trib
							set	row = ordem_compra_item_trib_w
							where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra
							and	nr_item_oci = ordem_compra_item_w.nr_item_oci
							and	cd_tributo = ordem_compra_item_trib_w.cd_tributo;

						else
							insert into ordem_compra_item_trib values ordem_compra_item_trib_w;
						end if;
					end if;
					end;
				end loop;
				close C05;

				end;
			end loop;
			close C03;

		end if; --> FECHANDO O LOOP DA CONDICAO DE NAO HAVER RECEBIMENTO DE NOTA FISCAL PARA A ORDEM DE COMPRA

	end if;
	end;
end loop;
close C01;

if	((reg_integracao_w.qt_reg_log > 0) or (ie_erro_w = 'S')) then
	begin
	rollback;

	update intpd_fila_transmissao
	set	ie_status = 'E',
		ie_tipo_erro = 'F'
	where	nr_sequencia = nr_sequencia_p;

	if (reg_integracao_w.qt_reg_log > 0) then
		for i in 0..reg_integracao_w.qt_reg_log-1 loop
			INTPD_GRAVAR_LOG_RECEBIMENTO(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');
		end loop;
	end if;
	
	end;

else

	select	count(*)
	into	qt_registros_w
	from	ordem_compra_item
	where	nr_ordem_compra = ordem_compra_w.nr_ordem_compra;

	if	((qt_registros_w > 0) and (nvl(c01_w.ie_released,'S') <> 'N')) then
		gerar_acoes_ordem_compra_imp(ordem_compra_w.nr_ordem_compra,ordem_compra_w.nm_usuario);
	end if;

	update	intpd_fila_transmissao
	set	ie_status = 'S',
		nr_seq_documento = nvl(nr_ordem_compra_delete_w,ordem_compra_w.nr_ordem_compra),
		nr_doc_externo = c01_w.nr_documento_externo
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end intpd_recebe_ordem_compra;
/
