create or replace
procedure ctb_duplicar_orcamento_linear(	nr_seq_cenario_p			number,
					nr_sequencia_p			number,
					cd_centro_custo_destino_p		number,
					nm_usuario_p			varchar2) is 

begin

insert into ctb_orc_cen_valor_linear (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_cenario,
	cd_centro_custo,
	cd_conta_contabil,
	ds_detalhe,
	cd_pessoa_fisica,
	cd_cnpj,
	vl_orcado_1,
	vl_orcado_2,
	vl_orcado_3,
	vl_orcado_4,
	vl_orcado_5,
	vl_orcado_6,
	vl_orcado_7,
	vl_orcado_8,
	vl_orcado_9,
	vl_orcado_10,
	vl_orcado_11,
	vl_orcado_12,
	cd_estabelecimento,
	ds_observacao)
(select	ctb_orc_cen_valor_linear_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	a.nr_seq_cenario,
	cd_centro_custo_destino_p,
	a.cd_conta_contabil,
	a.ds_detalhe,
	a.cd_pessoa_fisica,
	a.cd_cnpj,
	a.vl_orcado_1,
	a.vl_orcado_2,
	a.vl_orcado_3,
	a.vl_orcado_4,
	a.vl_orcado_5,
	a.vl_orcado_6,
	a.vl_orcado_7,
	a.vl_orcado_8,
	a.vl_orcado_9,
	a.vl_orcado_10,
	a.vl_orcado_11,
	a.vl_orcado_12,
	a.cd_estabelecimento,
	a.ds_observacao
from	ctb_orc_cen_valor_linear a
where	a.nr_sequencia		= nr_sequencia_p
and	a.nr_seq_cenario		= nr_seq_cenario_p);


commit;

end ctb_duplicar_orcamento_linear;
/