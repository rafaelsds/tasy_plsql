create or replace
procedure aprovar_exame_erro_js(	nr_prescricao_p	number,
				nr_seq_prescr_p	number) is 

begin

if	(nr_prescricao_p is not null)then
	begin
	
	update 	lab_gravar_result_erro
	set 	dt_geracao = sysdate	
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_seq_prescr = nr_seq_prescr_p;
	
	end;
end if;

commit;

end aprovar_exame_erro_js;
/