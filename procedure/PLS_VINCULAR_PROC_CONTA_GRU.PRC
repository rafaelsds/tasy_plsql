create or replace
procedure pls_vincular_proc_conta_gru
			(	nr_seq_proc_conta_p	pls_processo_conta.nr_sequencia%type,
				nr_seq_proc_gru_p	pls_processo_gru.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type) is 

begin

update	pls_processo_conta
set	nr_seq_proc_gru	= nr_seq_proc_gru_p
where	nr_sequencia	= nr_seq_proc_conta_p;

commit;

end pls_vincular_proc_conta_gru;
/

