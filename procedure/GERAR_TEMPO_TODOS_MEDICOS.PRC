create or replace
procedure gerar_tempo_todos_medicos(	nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is 

nr_sequencia_w	number(10,0);
			
Cursor C01 is
	select	nr_sequencia
	from	proc_interno
	where	ie_tipo_util = 'C';
	
	
begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	
	gerar_tempo_medico_proc(nr_sequencia_w, nm_usuario_p, cd_estabelecimento_p);
	
	end;
end loop;
close C01;

end gerar_tempo_todos_medicos;
/