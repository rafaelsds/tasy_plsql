create or replace procedure pls_tratar_json_rec_facial(	json_data_p     		clob,
							ie_status_p 		out varchar2,
							ie_codigo_p		out varchar2,
							ds_mensagem_p 		out varchar2,
							ds_token_p 		out varchar2,
							ds_url_p 		out varchar2,
							ds_erro_p 		out varchar2,
							ds_elegibilidade_p	out varchar2) is
  json_data_w		philips_json;
  json_data_ww		philips_json;
  ie_json_valido_w	varchar2(1);
BEGIN
ie_status_p := ''; 
ie_codigo_p := ''; 
ds_mensagem_p := '';
ds_token_p := ''; 
ds_url_p := ''; 
ds_erro_p := '';
ds_elegibilidade_p	:= '';

begin
	select	decode(substr(to_char(nvl(json_data_p,'{}')),1,255),'{}', 'N', 'S')
	into	ie_json_valido_w
	from	dual;
exception
when others then
	ie_json_valido_w := 'N';
end;
begin
	if (ie_json_valido_w = 'S') then
		JSON_DATA_W := PHILIPS_JSON(json_data_p);
		if (json_data_w.get('dados') is not null) then
			json_data_ww := PHILIPS_JSON(json_data_w.get('dados'));
		end if;
		if (json_data_w is not null and json_data_w.count > 0) then

			if (json_data_w.get('status') is not null)then
				ie_status_p := json_data_w.get('status').get_string();
			end if;

			if(json_data_w.get('codigo') is not null)then
				ie_codigo_p := json_data_w.get('codigo').get_number();
			end if;

			if(json_data_w.get('mensagem') is not null)then
				ds_mensagem_p := json_data_w.get('mensagem').get_string();
			end if;
			
			if(json_data_w.get('erros') is not null)then
				ds_erro_p := json_data_w.get('erros').get_string();
			end if;
		
			if (json_data_ww.get('token') is not null)then
				ds_token_p := json_data_ww.get('token').get_string();
			end if;

			if(json_data_ww.get('url') is not null)then
				ds_url_p := json_data_ww.get('url').get_string();
			end if;

			if(json_data_ww.get('elegibilidade') is not null)then
				ds_elegibilidade_p := json_data_ww.get('elegibilidade').get_string();
			end if;
			
		end if;
	else
		ie_codigo_p := '3';
	end if;
exception
when others then
ie_codigo_p := '3';	
end;
end;
/
