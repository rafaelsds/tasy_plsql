create or replace
procedure pat_gerar_nova_taxa_bem
			(	nr_seq_ajuste_p		number,
				nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Calcular a taxa de depreciação do bem
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
	Lei 11638
-------------------------------------------------------------------------------------------------------------------
Referências:
	Controle de patrimônio
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

tx_deprec_novo_w		number(15,4);

Cursor c_ajuste_valor_bem is
	select	nr_sequencia,	
		nr_seq_ajuste,	
		vl_original,
		vl_deprec_acum,
		tx_deprec_novo,
		qt_tempo_vida_util
	from	pat_ajuste_valor_bem
	where	nr_seq_ajuste	= nr_seq_ajuste_p;

vet01 c_ajuste_valor_bem%rowtype;

begin
open c_ajuste_valor_bem;
loop
fetch c_ajuste_valor_bem into	
	vet01;
exit when c_ajuste_valor_bem%notfound;
	begin
	tx_deprec_novo_w	:= dividir(100, vet01.qt_tempo_vida_util);
	
	update	pat_ajuste_valor_bem
	set	tx_deprec_novo	= tx_deprec_novo_w
	where	nr_sequencia	= vet01.nr_sequencia;
	end;
end loop;
close c_ajuste_valor_bem;

commit;

end pat_gerar_nova_taxa_bem;
/