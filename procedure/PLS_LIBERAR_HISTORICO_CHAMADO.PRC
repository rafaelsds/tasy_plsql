create or replace
procedure pls_liberar_historico_chamado(
	nr_sequencia_p pls_chamados_historico.nr_sequencia%type,
	nr_seq_chamados_p pls_chamados_historico.nr_seq_chamados%type,
	nm_usuario_p pls_chamados_historico.nm_usuario%type
) is
begin
	update pls_chamados_historico 
	set dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	dt_liberacao = sysdate 
	where nr_seq_chamados = nr_seq_chamados_p
		and nr_sequencia = nr_sequencia_p;
end pls_liberar_historico_chamado;
/
