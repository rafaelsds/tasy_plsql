create or replace
procedure gerar_etapa_eleven_review
			(nr_seq_obj_sup_p			number,
			 nr_seq_etapa_pai_p			number,
			 nm_usuario_p				in	usuario.nm_usuario%type,
			 ie_tipo_etapa_p			varchar2,
			 qt_hora_prev_p				number,
			 dt_inicio_prev_p			date,
			 nr_seq_cronograma_p			number,
			 cd_funcao_p				number) is

nr_seq_etapa_w	proj_cron_etapa.nr_sequencia%type;
qt_hora_prev_w	proj_cron_etapa.qt_hora_prev%type;

cursor c01 is
SELECT NVL(obter_desc_expressao(a.cd_expressao),a.ds_objeto) ds_objeto,
	nr_sequencia,
	nr_seq_obj_sch,
	ie_tipo_componente
FROM	qr_objeto_schematic a
WHERE	a.nr_seq_obj_sup		= nr_seq_obj_sup_p;

begin

for	r_c01_w in c01 loop

	if (r_c01_w.ie_tipo_componente is not null and
		r_c01_w.ie_tipo_componente not like 'WPOPUP' and
		r_c01_w.ie_tipo_componente not like 'WAEGroup') then
		
		qt_hora_prev_w := qt_hora_prev_p;		
	else	qt_hora_prev_w := 0;
	end if;
	

	inserir_proj_etapa_eleven(
				nr_seq_etapa_pai_p, 
				r_c01_w.nr_seq_obj_sch || ' - ' || r_c01_w.ds_objeto,
				nm_usuario_p,
				nr_seq_etapa_w,
				qt_hora_prev_w,
				0,
				cd_funcao_p,
				r_c01_w.nr_seq_obj_sch || ' - ' || r_c01_w.ds_objeto,
				nr_seq_cronograma_p,
				dt_inicio_prev_p);

	if (r_c01_w.ie_tipo_componente is not null) then
		
		if (ie_tipo_etapa_p = 'T') then
		
			update	qr_objeto_schematic
			set	nr_seq_etapa_test = nr_seq_etapa_w
			where	nr_sequencia = r_c01_w.nr_sequencia;
			
		elsif (ie_tipo_etapa_p = 'F') then
			
			update	qr_objeto_schematic
			set	nr_seq_etapa_fix = nr_seq_etapa_w
			where	nr_sequencia = r_c01_w.nr_sequencia;
		end if;
		
	end if;
	
	gerar_etapa_eleven_review(
		r_c01_w.nr_sequencia, 
		nr_seq_etapa_w, 
		nm_usuario_p,
		ie_tipo_etapa_p,
		qt_hora_prev_p,
		dt_inicio_prev_p,
		nr_seq_cronograma_p,
		cd_funcao_p);
	
end loop;

end gerar_etapa_eleven_review;
/