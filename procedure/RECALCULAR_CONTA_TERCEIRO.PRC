create or replace
procedure Recalcular_Conta_Terceiro(nr_seq_conta_p      	number,
                                       nm_usuario_p         varchar2) is 

nr_sequencia_w				number := 0;

cursor c01 is
select nr_sequencia
from terceiro_operacao
where nr_seq_conta = nr_seq_conta_p;

begin
open c01;
loop
    	fetch c01 into  	nr_sequencia_w;
   	Exit when c01%notfound;
	atualizar_operacao_terceiro(nr_sequencia_w, nm_usuario_p);
end loop;
	
end Recalcular_Conta_Terceiro;
/

