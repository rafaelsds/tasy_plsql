create or replace
procedure sup_limpar_oci_conf(nr_sequencia_p	number) is 

qt_existe_w	number(10);

begin
select	count(*)
into	qt_existe_w
from	ordem_compra_item_conf
where	nr_sequencia_nf = nr_sequencia_p;

if	(qt_existe_w > 0) then
	update	ordem_compra_item_conf
	set	nr_sequencia_nf = null
	where	nr_sequencia_nf = nr_sequencia_p;
end if;

commit;

end sup_limpar_oci_conf;
/