create or replace procedure reg_exclude_test_plan_ignored(nr_sequencia_p number) is
begin

  if (nr_sequencia_p is not null) then
  
    delete from w_test_plan_ignored_int
     where nr_sequencia = nr_sequencia_p;
  
  end if;

end reg_exclude_test_plan_ignored;
/
