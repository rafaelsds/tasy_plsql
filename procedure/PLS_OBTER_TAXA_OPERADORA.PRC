create or replace
procedure pls_obter_taxa_operadora
			(	ie_cobranca_pagamento_p		Varchar2,
				qt_dia_proced_receb_p		number,
				nr_seq_congenere_p		number,
				cd_estabelecimento_p		number,
				nr_seq_segurado_p			number,
				dt_referencia_p			date,
				nm_usuario_p			Varchar2,
				cd_procedimento_p			procedimento.cd_procedimento%type,
				ie_origem_proced_p		procedimento.ie_origem_proced%type,
				pr_taxa_p			out	number,
				ie_taxa_p			out	varchar2) is 

pr_taxa_w			number(15,2);
pr_taxa_util_w			number(15,2);
pr_taxa_ww			number(15,2);
nr_seq_grupo_coop_w		number(15);
nr_seq_intercambio_w		number(10);
nr_seq_plano_w			number(10);
ie_seguro_obito_w			varchar2(1);	
ie_beneficio_obito_w		varchar2(1);
ie_pcmso_w			varchar2(10);
nr_sequencia_w			number(10);
ie_tipo_regra_w			varchar2(3);
ie_tipo_segurado_w			pls_segurado.ie_tipo_segurado%type;
nr_seq_grupo_rec_w		number(15);

Cursor C01 is
	select	a.pr_taxa
	from	pls_regra_intercambio	a
	where	trunc(nvl(dt_referencia_p,sysdate),'dd') between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,sysdate+1)
	and	nvl(a.qt_dias_envio_conta,0)	>= qt_dia_proced_receb_p
	and	((pls_obter_tipo_intercambio(nr_seq_congenere_p,cd_estabelecimento_p) = nvl(a.ie_tipo_intercambio,'A')) or (nvl(a.ie_tipo_intercambio,'A') = 'A'))
	and	a.nr_seq_intercambio	= nr_seq_intercambio_w
	and	(a.nr_seq_grupo_coop_seg = nr_seq_grupo_coop_w or a.nr_seq_grupo_coop_seg is null)
	and     ((a.nr_seq_grupo_servico is null) or (nr_seq_grupo_servico in ( select  g.nr_seq_grupo
                                                                                from    pls_preco_servico       g
                                                                                where   g.cd_procedimento       = cd_procedimento_p
                                                                                and     g.ie_origem_proced      = ie_origem_proced_p)))
	and	(nvl(a.nr_seq_grupo_rec, nr_seq_grupo_rec_w) = nr_seq_grupo_rec_w)
	order by dt_inicio_vigencia,
		nvl(nr_seq_intercambio,0),
		nvl(ie_tipo_intercambio,0),
		nvl(qt_dias_envio_taxa,0),
		nvl(a.nr_seq_grupo_servico,0),
		nvl(a.nr_seq_grupo_rec, 0);
		
Cursor C02 is
	select	a.pr_taxa,
		a.ie_beneficio_obito
	from	pls_regra_intercambio	a
	where	1 = 1 
	and	trunc(nvl(dt_referencia_p,sysdate),'dd') between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,sysdate)
	and	nvl(a.qt_dias_envio_taxa,365)	>= qt_dia_proced_receb_p
	and	((pls_obter_tipo_intercambio(nr_seq_congenere_p,cd_estabelecimento_p) = nvl(a.ie_tipo_intercambio,'A')) or (nvl(a.ie_tipo_intercambio,'A') = 'A'))
	and	nvl(a.nr_seq_congenere_sup,nvl(a.nr_seq_congenere,nr_seq_congenere_p))		= nr_seq_congenere_p
	and	nvl(a.nr_seq_plano,nr_seq_plano_w) = nr_seq_plano_w
	and	ie_cobranca_pagamento	= ie_cobranca_pagamento_p
	and	(nvl(ie_pcmso,'N')		= ie_pcmso_w)
	and     ((a.nr_seq_grupo_servico is null) or (nr_seq_grupo_servico in ( select  g.nr_seq_grupo
                                                                                from    pls_preco_servico       g
                                                                                where   g.cd_procedimento       = cd_procedimento_p
                                                                                and     g.ie_origem_proced      = ie_origem_proced_p)))
	and	(a.nr_seq_grupo_coop_seg = nr_seq_grupo_coop_w or a.nr_seq_grupo_coop_seg is null)
	and	(nvl(a.nr_seq_grupo_rec, nr_seq_grupo_rec_w) = nr_seq_grupo_rec_w)
	and	(a.nr_seq_grupo_congenere is null or
		exists (select	1
			from	pls_cooperativa_grupo	x
			where	x.nr_seq_grupo = a.nr_seq_grupo_congenere
			and	x.nr_seq_congenere = nr_seq_congenere_p))
	and	(a.ie_tipo_regra is null or a.ie_tipo_regra = ie_tipo_regra_w)
	order by nvl(a.ie_tipo_intercambio,'A'),
		nvl(nr_seq_grupo_coop_seg,0),
		decode(ie_pcmso,'N',-1,1),
		nvl(nr_seq_plano,0),
		nvl(nr_seq_congenere,0),
		nvl(nr_seq_grupo_congenere,0),
		nvl(ie_cobranca_pagamento,0),
		dt_inicio_vigencia,
		nvl(qt_dias_envio_taxa,999),
		nvl(a.nr_seq_grupo_servico,0),
		nvl(a.nr_seq_grupo_rec, 0);
		
begin
/* Francisco - 30/08/2013 - OS 637684
Por default colocar o tipo de regra como cooperativa
*/
ie_tipo_regra_w	:= 'CO';

begin
	select	nr_seq_intercambio,
		--nr_seq_plano,
		pls_obter_produto_benef(nr_sequencia,dt_referencia_p),
		nvl(ie_pcmso,'N'),
		nr_seq_grupo_coop,
		ie_tipo_segurado
	into	nr_seq_intercambio_w,
		nr_seq_plano_w,
		ie_pcmso_w,
		nr_seq_grupo_coop_w,
		ie_tipo_segurado_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
exception
when others then
	nr_seq_intercambio_w	:= null;
	nr_seq_plano_w		:= null;
	ie_pcmso_w		:= 'N';
	nr_seq_grupo_coop_w	:= null;
end;

/* Francisco - 30/08/2013 - OS 637684
Se for benefici�rio assumido de OPS cong�nere, buscar regra de l�
*/
if	(ie_tipo_segurado_w = 'C') then
	ie_tipo_regra_w	:= 'CE';
end if;	

begin
select	nvl(ie_seguro_obito,'N')
into	ie_seguro_obito_w
from	pls_plano
where	nr_sequencia = nr_seq_plano_w;
exception
when others then
	ie_seguro_obito_w := 'N';
end;

begin
	select  nvl(a.nr_seq_grupo_rec, '0')
	into	nr_seq_grupo_rec_w
	from    procedimento a
	where   a.cd_procedimento       = cd_procedimento_p
	and     a.ie_origem_proced      = ie_origem_proced_p;
exception
	when others then
	nr_seq_grupo_rec_w := '0';
end;

/*aaschlote 04/01/2011 - OS 278997 - Verificar primeiramente se o benefici�rio possui contrato de interc�mbio, caso possuir, o sistema deve pegar de l�*/
if	(nr_seq_intercambio_w is not null) then
	open C01;
	loop
	fetch C01 into	
		pr_taxa_w;
	exit when C01%notfound;
	end loop;
	close C01;
end if;

/* Se n�o encontrou taxa de interc�mbio no contrato, verificar nas regras gerais da cooperativa. */
if	(nvl(pr_taxa_w,0) = 0) then
	open C02;
	loop
	fetch C02 into	
		pr_taxa_ww,
		ie_beneficio_obito_w;
	exit when C02%notfound;		
		pr_taxa_w	:= null;
		if	(ie_seguro_obito_w = 'B') then
			if	(nvl(ie_beneficio_obito_w,'N') = 'S') then
				pr_taxa_w := pr_taxa_ww;
			end if;
		else
			pr_taxa_w := pr_taxa_ww;
		end if;
		
		if	(pr_taxa_w	is not null) then
			pr_taxa_util_w := pr_taxa_w;
		end if;
	end loop;
	close C02;
end if;

ie_taxa_p	:= 'N';
pr_taxa_p	:= nvl(pr_taxa_util_w,0);

end pls_obter_taxa_operadora;
/