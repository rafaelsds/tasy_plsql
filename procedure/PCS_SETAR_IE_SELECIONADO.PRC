create or replace
procedure pcs_setar_ie_selecionado(	nr_seq_reclassif_p number default 0,
									nr_seq_registro_p number default 0,
									ie_opcao_p		varchar2,
									nm_tabela_p		varchar2,
									nm_usuario_p	Varchar2) is 

begin

if	(ie_opcao_p = 'N') and
	(nm_tabela_p = 'PCS_PARECER') and
	(nr_seq_reclassif_p > 0) then
	update	pcs_parecer
	set		ie_selecionado = 'N'
	where	nr_seq_reclassif = nr_seq_reclassif_p;
elsif	(ie_opcao_p = 'N') and
	(nm_tabela_p = 'PCS_PARECER') and
	(nr_seq_registro_p > 0) then
	update	pcs_parecer
	set		ie_selecionado = 'N'
	where	nr_seq_registro_pcs = nr_seq_registro_p;	
elsif (ie_opcao_p = 'N') and
	(nm_tabela_p = 'PCS_REGRA_ATIVIDADES') then
	update	pcs_regra_atividades
	set		ie_selecionado = 'N';
elsif (ie_opcao_p = 'S') and
	(nm_tabela_p = 'PCS_REGRA_ATIVIDADES') and
	(nr_seq_registro_p > 0) then
	update	pcs_regra_atividades
	set		ie_selecionado = 'S'
	where	nr_sequencia = nr_seq_registro_p;	
elsif (ie_opcao_p = 'N') and
	(nm_tabela_p = 'PCS_REGRA_ATIVIDADES') and
	(nr_seq_registro_p > 0) then
	update	pcs_regra_atividades
	set		ie_selecionado = 'N'
	where	nr_sequencia = nr_seq_registro_p;		
end if;	

commit;

end pcs_setar_ie_selecionado;
/
