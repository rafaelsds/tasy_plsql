create or replace
procedure delete_pep_item_pendente(	
						nr_prescricao_p		number,
						nm_usuario_p		varchar2,
						ie_tipo_item_p		varchar2,
						nr_sequencia_p		number) is

begin

delete from	pep_item_pendente_comp
where	nr_seq_item_pend = nr_sequencia_p;

delete 	from pep_item_pendente
where		nr_sequencia		= nr_sequencia_p;

commit;

end delete_pep_item_pendente;
/