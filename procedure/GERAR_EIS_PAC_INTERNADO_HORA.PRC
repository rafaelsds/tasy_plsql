create or replace
procedure gerar_eis_pac_internado_hora(	dt_parametro_p		date,
					nm_usuario_p		Varchar2) is 

nr_atendimento_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);
cd_procedencia_w	number(5,0);
ie_tipo_atendimento_w	number(3,0);
cd_medico_resp_w	varchar2(10);
cd_motivo_alta_w	number(3,0);
ie_clinica_w		number(5,0);
nr_seq_classificacao_w	number(10,0);
cd_setor_atendimento_w	number(5,0);
cd_convenio_w		number(5);
cd_categoria_w		varchar2(10);
cd_religiao_w		number(5,0);
cd_estabelecimento_w	number(4,0);
hr_internado_w		date;
nr_sequencia_w		number(10,0);										

Cursor C01 is
select	a.nr_atendimento,
	a.cd_pessoa_fisica,
	a.cd_procedencia,
	a.ie_tipo_atendimento,
	a.cd_medico_resp,
	a.cd_motivo_alta,
	a.ie_clinica,
	a.nr_seq_classificacao,
	a.cd_setor_atendimento,
	a.cd_convenio,
	a.cd_categoria,
	a.cd_religiao,
	a.cd_estabelecimento,
	trunc(a.dt_entrada_unidade,'hh')
from   	paciente_internado_v2 a
where	(nvl(a.dt_saida_unidade,a.dt_saida_interno) >= dt_parametro_p) 
and 	(a.dt_entrada_unidade < fim_dia(dt_parametro_p)) 
and 	(nvl(a.dt_saida_unidade, fim_dia(dt_parametro_p) + 1) > fim_dia(dt_parametro_p))
and 	not exists (select 1 from motivo_alta y where a.cd_motivo_alta = y.cd_motivo_alta   and y.ie_censo_diario = 'N')
and 	obter_se_setor_ocupacao_hosp(a.cd_setor_atendimento) = 'S'
and	obter_classif_setor(a.cd_setor_atendimento) in (3,4);	

begin

delete from eis_internado_hora where trunc(dt_referencia) = trunc(dt_parametro_p);
commit;

open c01;
loop
fetch c01 into 
	nr_atendimento_w,
	cd_pessoa_fisica_w,
	cd_procedencia_w,
	ie_tipo_atendimento_w,
	cd_medico_resp_w,
	cd_motivo_alta_w,
	ie_clinica_w,
	nr_seq_classificacao_w,
	cd_setor_atendimento_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_religiao_w,
	cd_estabelecimento_w,
	hr_internado_w;
exit when c01%NOTFOUND;

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	eis_internado_hora;

	insert into eis_internado_hora(
			nr_sequencia,
			dt_referencia,
			nr_atendimento,
			cd_pessoa_fisica,
			cd_procedencia,
			ie_tipo_atendimento,
			cd_medico_resp,
			cd_motivo_alta,
			ie_clinica,
			nr_seq_classificacao,
			cd_setor_atendimento,
			cd_convenio,
			cd_categoria,
			cd_religiao,
			hr_internado,
			cd_estabelecimento,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec)
		values  (nr_sequencia_w,
			trunc(dt_parametro_p),
			nr_atendimento_w,
			cd_pessoa_fisica_w,
			cd_procedencia_w,
			ie_tipo_atendimento_w,
			cd_medico_resp_w,
			cd_motivo_alta_w,
			ie_clinica_w,
			nr_seq_classificacao_w,
			cd_setor_atendimento_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_religiao_w,
			hr_internado_w,
			cd_estabelecimento_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate);
		

end loop;
close c01;

commit;

end Gerar_eis_pac_internado_hora;
/
