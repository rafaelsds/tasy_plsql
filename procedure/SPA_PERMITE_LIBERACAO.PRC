create or replace 
procedure spa_permite_liberacao( 	nr_seq_spa_p			number,		
					nm_usuario_p			Varchar2,
					ds_mensagem_p		out 	varchar2,
					ie_permite_liberar_p	out	varchar2) is
vl_negociado_w				spa_negociacao.vl_negociado%type;				 
ie_exige_valor_neg_w			spa_tipo.ie_exige_valor_neg%type;
begin

ie_permite_liberar_p 	:= 'S';
ds_mensagem_p		:= '';
if (nvl(nr_seq_spa_p,0) > 0)  then
	select  nvl(b.ie_exige_valor_neg,'N')
	into	ie_exige_valor_neg_w
	from   	spa 	 	a,
		spa_tipo 	b
	where 	b.nr_sequencia 	= a.nr_seq_tipo
	and 	a.nr_sequencia 	= nr_seq_spa_p;
	if (ie_exige_valor_neg_w = 'S') then
		select  nvl(sum(vl_negociado),0)
		into	vl_negociado_w
		from    spa_negociacao
		where   nr_seq_spa 	= nr_seq_spa_p;
		if (vl_negociado_w > 0) then
			ie_permite_liberar_p := 'S';
		else
			ie_permite_liberar_p := 'N';
			ds_mensagem_p 	     := Wheb_mensagem_pck.get_texto(802187); /*Para liberar esta SPA � necess�rio informar um valor negociado.*/
		end if;
	end if;
end if;
end spa_permite_liberacao;
/