create or replace 
procedure contagem_inicial_lote_barras(
			cd_material_p		number,
			cd_local_estoque_p	number,
			nr_seq_lote_fornec_p	number,
			qt_material_p		number,
			cd_fornecedor_p		varchar2,
			nm_usuario_p		varchar2) is

cd_fornecedor_w		varchar2(14);
			
begin

update	w_contagem_inicial_lote
set	qt_material = (qt_material + qt_material_p),
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	cd_local_estoque = cd_local_estoque_p
and	cd_material = cd_material_p
and	nr_seq_lote_fornec = nr_seq_lote_fornec_p
and	nvl(cd_fornecedor,'0') = nvl(cd_fornecedor_p,'0');
if	(sql%notfound) then
	insert into w_contagem_inicial_lote(
		nr_sequencia,
		cd_material,
		cd_local_estoque,
		nr_seq_lote_fornec,
		qt_material,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		cd_fornecedor)
	values (w_contagem_inicial_lote_seq.nextval,
		cd_material_p,
		cd_local_estoque_p,
		nr_seq_lote_fornec_p,
		qt_material_p,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		cd_fornecedor_p);
end if;
commit;
end contagem_inicial_lote_barras;
/