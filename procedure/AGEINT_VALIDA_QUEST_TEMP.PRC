CREATE OR REPLACE procedure ageint_valida_quest_temp(
							nr_seq_ageint_p		number,
							cd_estabelecimento_p	number,
							ie_sugestao_hor_p	varchar2 default 'N',
							ie_sugestao_html_p  	varchar2 default 'N',
              						ds_lista_item_bloq_p 	out varchar2) is

cd_pessoa_fisica_w		ageint_marcacao_usuario.cd_pessoa_fisica%type;
nr_seq_ageint_item_w		agenda_integrada_item.nr_sequencia%type;
cd_medico_item_w			agenda_integrada_item.cd_medico%type;
nr_seq_resp_quest_w		ageint_resp_quest_item.nr_seq_resp_quest%type;
cd_medico_quest_w		ageint_quest_utilizacao.cd_medico%type;
ie_bloquear_w			ageint_quest_regra_bloq.ie_bloquear%type;
nr_seq_resposta_w   		ageint_quest_regra_bloq.nr_seq_resposta%type;
nr_seq_resp_w   			ageint_resp_quest.nr_seq_resp%type;
nr_seq_superior_w   		ageint_quest_estrutura.nr_seq_superior%type;
ie_sugestao_hor_w			varchar2(1)	default 'N';
cd_agenda_w         		ageint_marcacao_usuario.cd_agenda%type;
hr_agenda_w         		ageint_marcacao_usuario.hr_agenda%type;
nr_minuto_duracao_w 		ageint_marcacao_usuario.nr_minuto_duracao%type;
nr_seq_ageint_w     		ageint_marcacao_usuario.nr_seq_ageint%type;
ie_encaixe_w        		ageint_marcacao_usuario.ie_encaixe%type;
nr_seq_agenda_w     		ageint_marcacao_usuario.nr_seq_agenda%type;
nr_seq_ageint_lib_w 		ageint_horarios_usuario.nr_seq_ageint_lib%type;
nm_usuario_w        		ageint_horarios_usuario.nm_usuario%type;
ie_reservado_w      		varchar2(250);
ie_principal_w      		varchar2(250);
ie_bloqueio_quest_w		varchar2(1) := 'N';
qt_marcacao_w			number(10);
ie_sugestao_html_w      	varchar2(1) := 'N';
ds_lista_item_bloq_w    	varchar2(4000) := '';

Cursor c01 is

	select
		nr_sequencia,
		cd_medico
	from
		agenda_integrada_item
	where
		nr_seq_agenda_int = nr_seq_ageint_p;

Cursor c02 is

	select
		a.nr_seq_resp_quest,
		b.cd_medico,
		c.ie_bloquear,
		nvl(c.nr_seq_resposta,0),
		d.nr_seq_superior
	from
		ageint_resp_quest_item a,
		ageint_quest_utilizacao b,
		ageint_quest_regra_bloq c,
		ageint_quest_estrutura d
	where
		a.nr_seq_quest_utilizacao = b.nr_sequencia
		and b.nr_sequencia = c.nr_seq_quest_utilizacao (+)
		and b.nr_seq_estrutura = c.nr_seq_estrutura (+)
		and b.nr_seq_estrutura = d.nr_sequencia
		and a.nr_seq_item = nr_seq_ageint_item_w
	order by
		a.nr_seq_resp_quest desc;

begin

ie_sugestao_hor_w := ie_sugestao_hor_p;
ie_sugestao_html_w := ie_sugestao_html_p;

open c01;
loop
fetch c01 into
	nr_seq_ageint_item_w,
	cd_medico_item_w;
exit when c01%NOTFOUND;
	begin

	select
		max(cd_pessoa_fisica),
    		max(cd_agenda),
    		max(nr_seq_agenda),
    		max(hr_agenda),
		max(nr_minuto_duracao),
    		max(nr_seq_ageint),
    		max(ie_encaixe)
	into
		cd_pessoa_fisica_w,
		cd_agenda_w,
		nr_seq_agenda_w,
		hr_agenda_w,
		nr_minuto_duracao_w,
		nr_seq_ageint_w,
		ie_encaixe_w
  	from
		ageint_marcacao_usuario
  	where
		nr_seq_ageint_item = nr_seq_ageint_item_w;

	open c02;
	loop
	fetch c02 into
		nr_seq_resp_quest_w,
		cd_medico_quest_w,
		ie_bloquear_w,
		nr_seq_resposta_w,
		nr_seq_superior_w;
	exit when c02%NOTFOUND;
		begin

		select
			max(nr_seq_resp)
		into
			nr_seq_resp_w
		from
			ageint_resp_quest
		where
			nr_sequencia = nr_seq_resp_quest_w;
			
		select
			max(nr_seq_ageint_lib)
		into
			nr_seq_ageint_lib_w
		from
			ageint_horarios_usuario
		where
			nr_seq_agenda = nr_seq_agenda_w
			and cd_agenda = cd_agenda_w
			and hr_agenda = hr_agenda_w
			and ie_encaixe = ie_encaixe_w
			and nr_minuto_duracao = nr_minuto_duracao_w;

		nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
    
		if (cd_medico_item_w is null
			and cd_pessoa_fisica_w is not null
			and cd_medico_quest_w = cd_pessoa_fisica_w
			and ie_bloquear_w = 'S'
			and (nr_seq_resposta_w = nr_seq_resp_w)) then

			delete from ageint_resp_quest_item where nr_seq_resp_quest = nr_seq_resp_quest_w;
			delete from ageint_resp_quest where nr_sequencia = nr_seq_resp_quest_w;
         
			if (ie_sugestao_hor_w = 'S') then
				ageint_sugerir_horarios_pck.clean_item (nr_seq_ageint_item_w, 'N', 'N');
			else
        
				atualiza_dados_marcacao(
						cd_agenda_w,
						hr_agenda_w,
						nr_seq_ageint_w,
						'D',
						nr_minuto_duracao_w,
						nm_usuario_w,
						nr_seq_ageint_item_w,
						nr_seq_ageint_lib_w,
						ie_encaixe_w,
						cd_pessoa_fisica_w,
						ie_Reservado_w,
						null,
						ie_principal_w,
						null,
						null);
			end if;

			if (Nvl(Length(ds_lista_item_bloq_w),0) = 0) then
          			ds_lista_item_bloq_w := ds_lista_item_bloq_w || nr_seq_ageint_item_w;
          		else
          			ds_lista_item_bloq_w := ds_lista_item_bloq_w || ', '|| nr_seq_ageint_item_w;
          		end if;
		else
		
			select decode(count(1), 0, 'N', 'S')
            		into ie_bloqueio_quest_w
            		from (select 1
                        	from ageint_resp_quest_item  i,
                        	     ageint_resp_quest       q,
                        	     ageint_quest_regra_bloq b,
                        	     ageint_quest_utilizacao aqu
                        	 where aqu.nr_sequencia = b.nr_seq_quest_utilizacao
                        	   and b.nr_seq_quest_utilizacao = i.nr_seq_quest_utilizacao
                        	   and b.nr_seq_estrutura = q.nr_seq_estrutura
                        	   and i.nr_seq_resp_quest = q.nr_sequencia
                        	   and b.nr_seq_resposta = q.nr_seq_resp
                        	   and b.ie_situacao = 'A'
                        	   and b.ie_bloquear = 'S'
                        	   and not exists
                        		 (select 1
                        	          from ageint_quest_regra_bl_item x
                        	         where b.nr_sequencia = x.nr_seq_regra_bloq
                        	           and x.ie_situacao = 'A')
                        		   and q.nr_seq_ageint = nr_seq_ageint_p
                        		   and i.nr_seq_item = nr_seq_ageint_item_w
                        union all
                        select 1
                        from ageint_resp_quest_item     i,
                             ageint_resp_quest          q,
                             ageint_quest_regra_bloq    b,
                             ageint_quest_regra_bl_item c,
                             ageint_quest_utilizacao    aqu
                        where aqu.nr_sequencia = b.nr_seq_quest_utilizacao
                             and b.nr_seq_quest_utilizacao = i.nr_seq_quest_utilizacao
                             and b.nr_seq_estrutura = q.nr_seq_estrutura
                             and i.nr_seq_resp_quest = q.nr_sequencia
                             and b.nr_seq_resposta = q.nr_seq_resp
                             and b.nr_sequencia = c.nr_seq_regra_bloq
                             and b.ie_situacao = 'A'
                             and b.ie_bloquear = 'S'
                             and c.ie_situacao = 'A'
                             and q.nr_seq_ageint = nr_seq_ageint_p
                             and i.nr_seq_item = nr_seq_ageint_item_w
                             and c.cd_agenda = cd_agenda_w);
                           
      			if (ie_bloqueio_quest_w = 'S') then       				
				select count(*)
				into	qt_marcacao_w
				from 	ageint_marcacao_usuario
				where	nr_seq_ageint = nr_Seq_Ageint_w
				and	nr_seq_ageint_item = nr_Seq_Ageint_item_w;
				
				if	(qt_marcacao_w > 0) then        
					atualiza_dados_marcacao(
							cd_agenda_w,
							hr_agenda_w,
							nr_seq_ageint_w,
							'D',
							nr_minuto_duracao_w,
							nm_usuario_w,
							nr_seq_ageint_item_w,
							nr_seq_ageint_lib_w,
							ie_encaixe_w,
							cd_pessoa_fisica_w,
							ie_Reservado_w,
							null,
							ie_principal_w,
							null,
							null);
				end if;		
      			end if;
		
		end if;    

		end;

	end loop;
	close c02;

	end;
end loop;
close c01;

commit;

ds_lista_item_bloq_p := ds_lista_item_bloq_w;
end ageint_valida_quest_temp;
/