create or replace
procedure san_fraci_grava_programa	(nr_seq_doacao_p	number,
					nr_seq_programa_p	number,
					nm_usuario_p		varchar2) is 
begin
if	(nr_seq_doacao_p is not null) then
	update	san_doacao
	set	nr_opcao_frac			= nr_seq_programa_p,
		dt_conferencia_integracao	= sysdate,
		nm_usuario_conf_integracao	= nm_usuario_p
	where	nr_sequencia			= nr_seq_doacao_p;
	commit;
end if;

end san_fraci_grava_programa;
/