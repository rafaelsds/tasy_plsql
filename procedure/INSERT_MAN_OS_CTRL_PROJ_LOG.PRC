create or replace
procedure insert_man_os_ctrl_proj_log(
	cd_versao_p		varchar2, 
	ds_etapa_p		varchar2, 
	nr_tempo_p		Number, 
	ie_situacao_p	varchar2, 
	ds_mensagem_p	varchar2,
	nr_ordem_p		Number) is 

begin

insert into man_os_ctrl_proj_log (cd_versao,
					ds_etapa,
					nr_tempo,
					ie_situacao,
					ds_mensagem,
					nr_ordem) 
			values (cd_versao_p, 
					ds_etapa_p, 
					nr_tempo_p, 
					ie_situacao_p, 
					ds_mensagem_p, 
					nr_ordem_p);
commit;

end insert_man_os_ctrl_proj_log;
/
