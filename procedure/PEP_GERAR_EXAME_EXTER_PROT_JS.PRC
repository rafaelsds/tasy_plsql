create or replace
procedure pep_gerar_exame_exter_prot_js (
					nr_sequencia_p	number,
					ds_lista_p	varchar2,
					nm_usuario_p	varchar2
					) is

nr_seq_protocolo_w	number(10);
tamanho_lista_w		number(10);
posicao_virgula_w 	number(3,0);
nr_max_loop_w		number(4) := 9999;
ds_lista_w 		varchar2(10000);

begin
if	(ds_lista_p is not null) then 
	begin
	tamanho_lista_w := length(ds_lista_p);
	ds_lista_w := ds_lista_p;
	
	while	(ds_lista_w is not null) and
		(nr_max_loop_w > 0) loop
		begin
		posicao_virgula_w := instr(ds_lista_w, ',');
		if	(posicao_virgula_w > 1) and
			(substr(ds_lista_w, 1, posicao_virgula_w -1) is not null) then
			begin
			nr_seq_protocolo_w := to_number(substr(ds_lista_w, 1, posicao_virgula_w -1));
			ds_lista_w := substr(ds_lista_w, posicao_virgula_w +1, tamanho_lista_w);
			end;
		elsif	(ds_lista_w is not null) then
			begin
			nr_seq_protocolo_w := replace(ds_lista_w, ',', '');
			ds_lista_w := '';
			end;
		end if;
		
		if	(nr_seq_protocolo_w is not null) and
			(nr_seq_protocolo_w > 0) then
			begin
			gerar_exame_externo_protocolo(nr_sequencia_p,nr_seq_protocolo_w,nm_usuario_p);			
			end;
		end if;
		
		nr_max_loop_w := nr_max_loop_w -1;
		end;
	end loop;
	end;
end if;

commit;
end pep_gerar_exame_exter_prot_js;
/
