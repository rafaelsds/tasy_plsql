create or replace
procedure gerar_regra_alta_tx_diaria(
			nr_atendimento_p	Number,
			dt_alta_p		Date,
			nm_usuario_p		Varchar2) is 

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
dt_entrada_w			date;
dt_alta_w			date;
ie_tipo_atendimento_w		atendimento_paciente.ie_tipo_atendimento%type;
cd_plano_w			convenio_plano.cd_plano%type;
cd_convenio_w			convenio.cd_convenio%type;
cd_categoria_w			categoria_convenio.cd_categoria%type;
nr_doc_convenio_w		atend_categoria_convenio.nr_doc_convenio%type;
ie_tipo_guia_w			atend_categoria_convenio.ie_tipo_guia%type;
cd_senha_w			atend_categoria_convenio.cd_senha%type;
nr_seq_atepacu_w		atend_paciente_unidade.nr_seq_interno%type;
dt_entrada_unidade_w		date;
cd_tipo_acomodacao_w		tipo_acomodacao.cd_tipo_acomodacao%type;
cd_setor_atendimento_w		setor_atendimento.cd_setor_atendimento%type;
hr_entrada_w			number(5,0);
hr_alta_w			number(5,0);
qt_dif_alta_entrada_w		number(5,0);
qt_procedimento_w		procedimento_paciente.qt_procedimento%type;
nr_seq_proc_int_diaria_w	proc_interno.nr_sequencia%type;
nr_seq_proc_int_taxa_w		proc_interno.nr_sequencia%type;
qt_limite_hora_diaria_w		conv_regra_alta_tx_diaria.qt_limite_hora_diaria%type;
nr_seq_proc_int_gerar_w		proc_interno.nr_sequencia%type;
cd_procedimento_w		procedimento.cd_procedimento%type;
ie_origem_proced_w		procedimento.ie_origem_proced%type;
ie_classificacao_w		procedimento.ie_classificacao%type;
nr_seq_classificacao_w		classificacao_atendimento.nr_sequencia%type;
ie_medico_executor_w		consiste_setor_proc.ie_medico_executor%type;
cd_cgc_prestador_w		pessoa_juridica.cd_cgc%type;
cd_medico_executor_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_profissional_w		pessoa_fisica.cd_pessoa_fisica%type;
ie_clinica_w			atendimento_paciente.ie_clinica%type;
cd_especialidade_medica_w	especialidade_medica.cd_especialidade%type;
ie_funcao_medico_w		funcao_medico.cd_funcao%type;
ie_via_acesso_w			regra_via_acesso.ie_via_acesso%type;
nr_seq_proc_w			procedimento_paciente.nr_sequencia%type;


Cursor C01 is
	select	nr_seq_proc_int_diaria,
		nr_seq_proc_int_taxa,
		qt_limite_hora_diaria
	from	conv_regra_alta_tx_diaria
	where	cd_estabelecimento = cd_estabelecimento_w
	and	cd_convenio = cd_convenio_w
	and	cd_tipo_acomodacao = cd_tipo_acomodacao_w
	and	ie_situacao = 'A'
	order by	qt_limite_hora_diaria desc;

begin

if	(nvl(nr_atendimento_p,0) > 0) then

	dt_alta_w	:= nvl(dt_alta_p,sysdate);

	select	max(cd_estabelecimento),
		max(dt_entrada),
		max(ie_tipo_atendimento),
		max(substr(obter_plano_atendimento(nr_atendimento,'C'),1,10)),
		max(obter_atepacu_paciente(nr_atendimento,'A'))
	into	cd_estabelecimento_w,
		dt_entrada_w,
		ie_tipo_atendimento_w,
		cd_plano_w,
		nr_seq_atepacu_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	obter_convenio_execucao(nr_atendimento_p, dt_alta_w, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);
	
	select	max(dt_entrada_unidade),
		max(cd_tipo_acomodacao),
		max(cd_setor_atendimento)
	into	dt_entrada_unidade_w,
		cd_tipo_acomodacao_w,
		cd_setor_atendimento_w
	from	atend_paciente_unidade
	where	nr_atendimento = nr_atendimento_p
	and	nr_seq_interno = nr_seq_atepacu_w;

	hr_entrada_w	:= nvl(somente_numero(to_char(dt_entrada_w,'hh24')),0);
	hr_alta_w	:= nvl(somente_numero(to_char(dt_alta_w,'hh24')),0);

	qt_dif_alta_entrada_w	:= hr_alta_w - hr_entrada_w;

	if	(qt_dif_alta_entrada_w > 0) then

		open C01;
		loop
		fetch C01 into	
			nr_seq_proc_int_diaria_w,
			nr_seq_proc_int_taxa_w,
			qt_limite_hora_diaria_w;
		exit when C01%notfound;
			begin
			nr_seq_proc_int_diaria_w	:= nr_seq_proc_int_diaria_w;
			nr_seq_proc_int_taxa_w		:= nr_seq_proc_int_taxa_w;
			qt_limite_hora_diaria_w		:= qt_limite_hora_diaria_w;
			end;
		end loop;
		close C01;
		
		if	(qt_dif_alta_entrada_w > qt_limite_hora_diaria_w) then
			nr_seq_proc_int_gerar_w	:= nr_seq_proc_int_diaria_w;
			qt_procedimento_w	:= 1;
		else
			nr_seq_proc_int_gerar_w	:= nr_seq_proc_int_taxa_w;
			qt_procedimento_w	:= qt_dif_alta_entrada_w;
		end if;
		
		if	(nvl(nr_seq_proc_int_gerar_w,0) > 0) then
		
			Obter_Proc_Tab_Interno_Conv(nr_seq_proc_int_gerar_w, cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, cd_plano_w,
				cd_setor_atendimento_w, cd_procedimento_w, ie_origem_proced_w, cd_setor_atendimento_w, dt_alta_w,
				cd_tipo_acomodacao_w, null, null, null, ie_tipo_atendimento_w, null, null, null);
				
			if	(nvl(cd_procedimento_w,0) > 0) and
				(nvl(ie_origem_proced_w,0) > 0) then
		
				select	max(ie_classificacao)
				into	ie_classificacao_w
				from	procedimento
				where	cd_procedimento		= cd_procedimento_w
				and	ie_origem_proced	= ie_origem_proced_w;
				
				-- obter m�dico executor 
				consiste_medico_executor(cd_estabelecimento_w, cd_convenio_w, cd_setor_atendimento_w, cd_procedimento_w,
					ie_origem_proced_w, ie_tipo_atendimento_w, null, nr_seq_proc_int_gerar_w, ie_medico_executor_w,
					cd_cgc_prestador_w, cd_medico_executor_w, cd_profissional_w, null, dt_alta_w, nr_seq_classificacao_w, 'N',
					null, null); 
						
				if	(nvl(ie_medico_executor_w,'Z') <> 'F') then
					cd_medico_executor_w:= '';
				end if;
				
				obter_proced_espec_medica(cd_estabelecimento_w, cd_convenio_w, cd_procedimento_w, ie_origem_proced_w, null, null,
					null, ie_clinica_w, cd_setor_atendimento_w, cd_especialidade_medica_w, ie_funcao_medico_w,
					cd_medico_executor_w, nr_seq_proc_int_gerar_w, ie_tipo_atendimento_w);
					
				if	(cd_cgc_prestador_w is null) then
					select	max(a.cd_cgc)
					into	cd_cgc_prestador_w 
					from 	estabelecimento a,
						atendimento_paciente b
					where	a.cd_estabelecimento 	= b.cd_estabelecimento
					and	b.nr_atendimento 	= nr_atendimento_p;
				end if;	
				
				select 	substr(obter_regra_via_acesso(cd_procedimento_w, ie_origem_proced_w, cd_estabelecimento_w, cd_convenio_w),1,2) 
				into	ie_via_acesso_w
				from 	dual;
				
				select 	procedimento_paciente_seq.NextVal
				into	nr_seq_proc_w
				from 	dual;
				
				insert into procedimento_paciente( 
					nr_sequencia,		nr_atendimento,		dt_entrada_unidade,	cd_procedimento,
					dt_procedimento,	cd_convenio,		cd_categoria,		nr_doc_convenio,
					ie_tipo_guia,		cd_senha,		ie_auditoria,		ie_emite_conta,
					cd_cgc_prestador,	ie_origem_proced,	nr_seq_exame,		nr_seq_proc_interno,
					qt_procedimento,	cd_setor_atendimento,	nr_seq_atepacu,		nr_seq_cor_exec,
					ie_funcao_medico,	vl_procedimento,	ie_proc_princ_atend,	ie_video,
					tx_medico,		tx_anestesia,		tx_procedimento,	ie_valor_informado,
					ie_guia_informada,	cd_situacao_glosa,	nm_usuario_original,	ds_observacao,
					dt_atualizacao,		nm_usuario,		cd_pessoa_fisica,	cd_medico_executor,
					cd_especialidade, 	ie_via_acesso)
				values( 
					nr_seq_proc_w,		nr_atendimento_p,	dt_entrada_unidade_w,	cd_procedimento_w,
					dt_alta_w,		cd_convenio_w,		cd_categoria_w,		nr_doc_convenio_w,
					ie_tipo_guia_w,		cd_senha_w,		'N',			null,
					cd_cgc_prestador_w,	ie_origem_proced_w,	null,			nr_seq_proc_int_gerar_w,
					qt_procedimento_w,	cd_setor_atendimento_w,	nr_seq_atepacu_w,	null,
					ie_funcao_medico_w,	100,			'N',			'N',
					100,			100,			100,			'N',
					'N',			0,			nm_usuario_p,		substr(wheb_mensagem_pck.get_texto(310002),1,255),
					sysdate,		nm_usuario_p,		cd_profissional_w,	cd_medico_executor_w,
					cd_especialidade_medica_w, ie_via_acesso_w);
					
				if	(ie_classificacao_w in ('1','8')) then 		
					atualiza_preco_procedimento(nr_seq_proc_w, cd_convenio_w, nm_usuario_p);
					gerar_taxa_sala_porte(nr_atendimento_p, dt_entrada_unidade_w, dt_alta_w, cd_procedimento_w, nr_seq_proc_w,
						nm_usuario_p);
				else
					atualiza_preco_servico(nr_seq_proc_w, nm_usuario_p);
				end if;
		
			end if;
		
		end if;
	
	end if;

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_regra_alta_tx_diaria;
/