create or replace
procedure pls_obter_se_qtd_ocor_exec
			(	nr_sequencia_p			Number,
				ie_tipo_item_p			Number,
				qt_liberada_p			Number,
				ie_tipo_qtde_p			Varchar2,
				qt_tipo_quantidade_p		Number,
				ie_tipo_pessoa_qtde_p		Varchar2,
				ie_regra_tipo_quant_p		Varchar2,
				ie_somar_estrutura_p		Varchar2,
				nr_seq_estrutura_p		Number,
				nr_seq_ocorrencia_p		Number,
				ie_qt_lib_posterior_p		Varchar2,
				ie_somente_solicitacao_p	varchar2,
				ie_valida_ano_contratual_p	varchar2,
				ie_valida_cod_prestador_p	varchar2,
				ie_tipo_guia_p			varchar2,
				ie_valida_local_atend_p		varchar2,
				cd_estabelecimento_p		Varchar2,
				ie_retorno_p		out	Varchar2,
				ds_observacao_p		out	Varchar2) is

/* IE_TIPO_ITEM_P
	1 - Procedimento da Autoriza��o
	2 - Material da Autoriza��o
	3 - Procedimento da Conta
	4 - Material da Conta
	5 - Procedimento da Requisi��o
	6 - Material da Requisi��o
*/

/* IE_TIPO_QTDE_P - Dom�nio 3540
	D - Dia
	M - M�s
	A - Ano
	G - Guia
	C - Conta
*/

ie_retorno_w			Varchar2(1)	:= 'S';
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
nr_seq_material_w		Number(10);
nr_seq_segurado_w		Number(10);
nr_seq_guia_plano_w		Number(10);
qt_solicitada_w			Number(9,3)	:= 0;
qt_liberacao_w			Number(9,3);
dt_liberacao_w			Date;
dt_liberacao_ww			date;
ds_observacao_w			varchar2(400);
nr_seq_exec_req_item_w		number(10);
nr_seq_guia_w			number(10);
dt_execucao_w			date;
cd_guia_w			varchar2(20);
nm_medico_requisit_w		varchar2(255);
qt_solicitacao_w		Number(10) := 0;
nr_seq_requisicao_cursor_w	number(10);
qt_item_w			number(10);
nr_seq_exec_req_cursor_w	number(10);
nr_seq_exec_req_cursor_ww	number(10);
cd_prestador_w			varchar2(30);
nr_seq_prestador_w		number(10);
nm_usuario_solic_w		varchar2(255);
nr_seq_local_atend_w		number(10)	:= 0;
ie_cursor_valida_restricao_w	varchar2(1) := 'N';
ie_tipo_processo_w		varchar2(1);
nr_seq_prestador_cursor_w	number(10);
nr_seq_segurado_cursor_w	number(10);
cd_procedimento_ww		Number(15);
ie_origem_proced_ww		Number(10);
nr_seq_material_ww		Number(10);
nr_seq_guia_cursor_w		number(10);
ie_status_w			varchar2(2);
nr_seq_prestador_exec_w		number(10);

Cursor C01 (	cd_procedimento_pc	procedimento.cd_procedimento%type,
		ie_origem_proced_pc	procedimento.ie_origem_proced%type,
		dt_liberacao_w_pc	pls_execucao_requisicao.dt_execucao%type,
		dt_liberacao_pc		pls_execucao_requisicao.dt_execucao%type,
		nr_seq_segurado_pc	pls_segurado.nr_sequencia%type) is
		--Benefici�rio (Seq Benef)
	select	a.nr_seq_guia,
		b.nr_seq_requisicao,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.qt_item,
		a.nr_sequencia,
		b.nr_seq_prestador,
		a.nr_seq_segurado,
		a.ie_situacao
	from	pls_execucao_req_item		a,
		pls_execucao_requisicao		b
	where	a.nr_seq_execucao		= b.nr_sequencia
	and	a.cd_procedimento is not null
	and	a.ie_origem_proced is not null
	and	a.nr_seq_segurado		= nr_seq_segurado_pc
	and	b.dt_execucao between dt_liberacao_w_pc and dt_liberacao_pc
	and	a.cd_procedimento 		= cd_procedimento_pc 
	and	a.ie_origem_proced 		= ie_origem_proced_pc
	and	not exists (	select	1
				from	pls_requisicao_proc		x
				where	x.nr_seq_requisicao		= b.nr_seq_requisicao
				and	x.ie_cobranca_previa_servico	= 'S');

Cursor C02 (	cd_procedimento_pc	procedimento.cd_procedimento%type,
		ie_origem_proced_pc	procedimento.ie_origem_proced%type,
		dt_liberacao_w_pc	pls_execucao_requisicao.dt_execucao%type,
		dt_liberacao_pc		pls_execucao_requisicao.dt_execucao%type,
		nr_seq_prestador_pc	pls_prestador.nr_sequencia%type) is
		--Prestador (Seq Prestador)
	select	a.nr_seq_guia,
		b.nr_seq_requisicao,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.qt_item,
		a.nr_sequencia,
		b.nr_seq_prestador,
		a.nr_seq_segurado,
		a.ie_situacao
	from	pls_execucao_req_item		a,
		pls_execucao_requisicao		b
	where	a.nr_seq_execucao		= b.nr_sequencia
	and	a.cd_procedimento is not null
	and	a.ie_origem_proced is not null
	and	b.nr_seq_prestador		= nr_seq_prestador_pc
	and	b.dt_execucao between dt_liberacao_w_pc and dt_liberacao_pc
	and	a.cd_procedimento 		= cd_procedimento_pc 
	and	a.ie_origem_proced 		= ie_origem_proced_pc
	and	not exists (	select	1
				from	pls_requisicao_proc		x
				where	x.nr_seq_requisicao		= b.nr_seq_requisicao
				and	x.ie_cobranca_previa_servico	= 'S');

Cursor C03 (	cd_procedimento_pc	procedimento.cd_procedimento%type,
		ie_origem_proced_pc	procedimento.ie_origem_proced%type,
		dt_liberacao_w_pc	pls_execucao_requisicao.dt_execucao%type,
		dt_liberacao_pc		pls_execucao_requisicao.dt_execucao%type,
		cd_prestador_pc		pls_prestador.cd_prestador%type) is
		--Prestador (Cod Prestador)
	select	a.nr_seq_guia,
		b.nr_seq_requisicao,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.qt_item,
		a.nr_sequencia,
		b.nr_seq_prestador,
		a.nr_seq_segurado,
		a.ie_situacao
	from	pls_execucao_req_item		a,
		pls_execucao_requisicao		b,
		pls_prestador			c
	where	a.nr_seq_execucao		= b.nr_sequencia
	and	b.nr_seq_prestador		= c.nr_sequencia
	and	a.cd_procedimento is not null
	and	a.ie_origem_proced is not null
	and	c.cd_prestador			= cd_prestador_pc
	and	b.dt_execucao between dt_liberacao_w_pc and dt_liberacao_pc
	and	a.cd_procedimento 		= cd_procedimento_pc 
	and	a.ie_origem_proced 		= ie_origem_proced_pc
	and	not exists (	select	1
				from	pls_requisicao_proc		x
				where	x.nr_seq_requisicao		= b.nr_seq_requisicao
				and	x.ie_cobranca_previa_servico	= 'S');

Cursor C04 (	cd_procedimento_pc	procedimento.cd_procedimento%type,
		ie_origem_proced_pc	procedimento.ie_origem_proced%type,
		dt_liberacao_w_pc	pls_execucao_requisicao.dt_execucao%type,
		dt_liberacao_pc		pls_execucao_requisicao.dt_execucao%type,
		nr_seq_segurado_pc	pls_segurado.nr_sequencia%type,
		nr_seq_prestador_pc	pls_prestador.nr_sequencia%type) is
		--Ambos (Seq Benef / Seq Prestador)
	select	a.nr_seq_guia,
		b.nr_seq_requisicao,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.qt_item,
		a.nr_sequencia,
		b.nr_seq_prestador,
		a.nr_seq_segurado,
		a.ie_situacao
	from	pls_execucao_req_item		a,
		pls_execucao_requisicao		b
	where	a.nr_seq_execucao		= b.nr_sequencia
	and	a.cd_procedimento is not null
	and	a.ie_origem_proced is not null
	and	a.nr_seq_segurado		= nr_seq_segurado_pc
	and	b.nr_seq_prestador		= nr_seq_prestador_pc
	and	b.dt_execucao between dt_liberacao_w_pc and dt_liberacao_pc
	and	a.cd_procedimento 		= cd_procedimento_pc 
	and	a.ie_origem_proced 		= ie_origem_proced_pc
	and	not exists (	select	1
				from	pls_requisicao_proc		x
				where	x.nr_seq_requisicao		= b.nr_seq_requisicao
				and	x.ie_cobranca_previa_servico	= 'S');

Cursor C05 (	cd_procedimento_pc	procedimento.cd_procedimento%type,
		ie_origem_proced_pc	procedimento.ie_origem_proced%type,
		dt_liberacao_w_pc	pls_execucao_requisicao.dt_execucao%type,
		dt_liberacao_pc		pls_execucao_requisicao.dt_execucao%type,
		nr_seq_segurado_pc	pls_segurado.nr_sequencia%type,
		cd_prestador_pc		pls_prestador.cd_prestador%type) is
		--Ambos (Seq Benef / Cod Prestador)
	select	a.nr_seq_guia,
		b.nr_seq_requisicao,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.qt_item,
		a.nr_sequencia,
		b.nr_seq_prestador,
		a.nr_seq_segurado,
		a.ie_situacao
	from	pls_execucao_req_item		a,
		pls_execucao_requisicao		b,
		pls_prestador			c
	where	a.nr_seq_execucao		= b.nr_sequencia
	and	b.nr_seq_prestador		= c.nr_sequencia
	and	a.cd_procedimento is not null
	and	a.ie_origem_proced is not null
	and	a.nr_seq_segurado		= nr_seq_segurado_pc
	and	c.cd_prestador			= cd_prestador_pc
	and	b.dt_execucao between dt_liberacao_w_pc and dt_liberacao_pc
	and	a.cd_procedimento 		= cd_procedimento_pc 
	and	a.ie_origem_proced 		= ie_origem_proced_pc
	and	not exists (	select	1
				from	pls_requisicao_proc		x
				where	x.nr_seq_requisicao		= b.nr_seq_requisicao
				and	x.ie_cobranca_previa_servico	= 'S');

Cursor C11 is
	select 	a.nr_seq_guia,
		b.nr_seq_requisicao,
		a.nr_seq_material,
		a.qt_item,
		a.nr_sequencia,
		b.nr_seq_prestador,
		a.nr_seq_segurado
	from	pls_execucao_req_item		a,
		pls_execucao_requisicao		b
	where	a.nr_seq_execucao		= b.nr_sequencia
	and	a.nr_seq_material is not null
	and	a.ie_situacao			not in ('C','G','N','U')
	and	((ie_tipo_pessoa_qtde_p		= 'B'	and	a.nr_seq_segurado		= nr_seq_segurado_w)
	or	(ie_tipo_pessoa_qtde_p 		= 'P'
	and 	((ie_valida_cod_prestador_p 	= 'N'	and 	b.nr_seq_prestador		= nr_seq_prestador_w)
	or	(ie_valida_cod_prestador_p 	= 'S'	and	pls_obter_cod_prestador(b.nr_seq_prestador,null)	= cd_prestador_w)))
	or	(ie_tipo_pessoa_qtde_p  	= 'A'	and	a.nr_seq_segurado		= nr_seq_segurado_w
	and	((ie_valida_cod_prestador_p 	= 'N'	and	b.nr_seq_prestador		= nr_seq_prestador_w)
	or	(ie_valida_cod_prestador_p 	= 'S'	and	pls_obter_cod_prestador(b.nr_seq_prestador,null)	= cd_prestador_w))))
	and	b.dt_execucao 	between dt_liberacao_ww and dt_liberacao_w
	and	not exists (	select	1
				from	pls_requisicao_mat			y
				where	y.nr_seq_requisicao			= b.nr_seq_requisicao
				and	y.ie_cobranca_previa_servico	= 'S');

begin

if	(ie_tipo_item_p	= 5) then
	select	a.cd_procedimento,
		a.ie_origem_proced,
		b.dt_requisicao,
		a.qt_solicitado,
		b.nr_seq_segurado,
		b.nr_seq_prestador,
		b.nr_seq_prestador_exec,
		b.nm_usuario_solic
	into	cd_procedimento_w,
		ie_origem_proced_w,
		dt_liberacao_w,
		qt_solicitada_w,
		nr_seq_segurado_w,
		nr_seq_prestador_w,
		nr_seq_prestador_exec_w,
		nm_usuario_solic_w
	from	pls_requisicao		b,
		pls_requisicao_proc	a
	where	a.nr_seq_requisicao	= b.nr_sequencia
	and	a.nr_sequencia		= nr_sequencia_p;
elsif	(ie_tipo_item_p	= 6) then
	select	a.nr_seq_material,
		b.dt_requisicao,
		a.qt_solicitado,
		b.nr_seq_segurado,
		b.nr_seq_prestador,
		b.nr_seq_prestador_exec,
		b.nm_usuario_solic
	into	nr_seq_material_w,
		dt_liberacao_w,
		qt_solicitada_w,
		nr_seq_segurado_w,
		nr_seq_prestador_w,
		nr_seq_prestador_exec_w,
		nm_usuario_solic_w
	from	pls_requisicao		b,
		pls_requisicao_mat	a
	where	a.nr_seq_requisicao	= b.nr_sequencia
	and	a.nr_sequencia		= nr_sequencia_p;
end if;

nr_seq_prestador_w := nvl(nr_seq_prestador_exec_w,nr_seq_prestador_w);

begin
	select	cd_prestador
	into	cd_prestador_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_w;
exception
when others then
	cd_prestador_w := null;
end;

if	(nvl(nm_usuario_solic_w,'X') <> 'X') and
	(ie_valida_local_atend_p = 'S')then
	nr_seq_local_atend_w := pls_obter_local_atend_usuario(nm_usuario_solic_w,cd_estabelecimento_p);
end if;

if	(ie_tipo_qtde_p	= 'D') then
	dt_liberacao_ww	:= trunc(dt_liberacao_w - (qt_tipo_quantidade_p - 1));
	if	(ie_qt_lib_posterior_p	= 'S') then
		dt_liberacao_w	:= trunc(dt_liberacao_w + (qt_tipo_quantidade_p - 1));
	end if;
elsif	(ie_tipo_qtde_p	= 'M') then
	dt_liberacao_ww	:= (add_months(dt_liberacao_w, -qt_tipo_quantidade_p) + 1);
	if	(ie_qt_lib_posterior_p	= 'S') then
		dt_liberacao_w	:= (add_months(dt_liberacao_w, qt_tipo_quantidade_p) + 1);
	end if;
elsif	(ie_tipo_qtde_p	= 'A') then
	dt_liberacao_ww	:= (add_months(dt_liberacao_w, -qt_tipo_quantidade_p * 12) + 1); /* Vezes 12 meses ao ano */
	if	(ie_qt_lib_posterior_p	= 'S') then
		dt_liberacao_w	:= (add_months(dt_liberacao_w, qt_tipo_quantidade_p * 12) + 1); /* Vezes 12 meses ao ano */
	end if;
elsif	(ie_tipo_qtde_p = 'G') then
	dt_liberacao_ww := sysdate;
end if;

dt_liberacao_ww	:= trunc(dt_liberacao_ww,'dd');
dt_liberacao_w	:= fim_dia(dt_liberacao_w);

qt_liberacao_w	:= 0;

if	(ie_tipo_item_p = 5) then
	if 	(cd_procedimento_w is not null) then
		if	(ie_tipo_pessoa_qtde_p 	= 'B') then
			for	r_C01_w in C01( cd_procedimento_w, ie_origem_proced_w, dt_liberacao_ww, dt_liberacao_w,	nr_seq_segurado_w) loop
				if	(r_C01_w.ie_situacao in ('C','G','N','U')) then
					goto final;
				end if;

				if (cd_procedimento_w = r_C01_w.cd_procedimento and r_C01_w.ie_origem_proced = ie_origem_proced_w) then
					ie_cursor_valida_restricao_w := 'S';
				else
					goto final;
				end if;

				if	(nvl(r_C01_w.nr_seq_guia,0) > 0) then
					begin
						select	ie_status
						into	ie_status_w
						from	pls_guia_plano
						where	nr_sequencia = r_C01_w.nr_seq_guia;

						if	(ie_status_w <> '3') then
							ie_cursor_valida_restricao_w := 'S';
						else
							goto final;
						end if;
					exception
					when others then
						ie_cursor_valida_restricao_w := 'N';
					end;
				end if;

				if	(ie_valida_local_atend_p = 'N')	 then
					ie_cursor_valida_restricao_w := 'S';			
				elsif	(ie_valida_local_atend_p = 'S') then		
					begin
						select	nm_usuario_solic,
							ie_tipo_processo
						into	nm_usuario_solic_w,
							ie_tipo_processo_w
						from	pls_requisicao
						where	nr_sequencia = r_C01_w.nr_seq_requisicao;
					exception
					when others then
						nm_usuario_solic_w := 'X';
						ie_tipo_processo_w := null;
					end;

					if 	(nr_seq_local_atend_w <> 0)		and (ie_tipo_processo_w = 'P') and
						(nvl(nm_usuario_solic_w,'X') <> 'X') 	and (pls_obter_local_atend_usuario(nm_usuario_solic_w,cd_estabelecimento_p) = nr_seq_local_atend_w) /*and
						(nr_seq_prestador_cursor_w <> nr_seq_prestador_w)*/ then
						ie_cursor_valida_restricao_w := 'S';
					else
						ie_cursor_valida_restricao_w := 'N';
					end if;
				else
					goto final;
				end if;

				if	(ie_cursor_valida_restricao_w = 'S') then
					qt_liberacao_w	:= qt_liberacao_w + r_C01_w.qt_item;
					qt_solicitacao_w := qt_solicitacao_w + 1;
					nr_seq_exec_req_cursor_ww := r_C01_w.nr_sequencia;
				end if;

				<<final>>
				ie_cursor_valida_restricao_w := 'N';			
			end loop;
		elsif	(ie_tipo_pessoa_qtde_p	= 'P') then
			if	(ie_valida_cod_prestador_p = 'N') then
				for	r_C02_w in C02( cd_procedimento_w, ie_origem_proced_w, dt_liberacao_ww, dt_liberacao_w,	nr_seq_prestador_w) loop
					if	(r_C02_w.ie_situacao in ('C','G','N','U')) then
						goto final;
					end if;

					if (cd_procedimento_w = r_C02_w.cd_procedimento and r_C02_w.ie_origem_proced = ie_origem_proced_w) then
						ie_cursor_valida_restricao_w := 'S';
					else
						goto final;
					end if;

					if	(nvl(r_C02_w.nr_seq_guia,0) > 0) then
						begin
							select	ie_status
							into	ie_status_w
							from	pls_guia_plano
							where	nr_sequencia = r_C02_w.nr_seq_guia;

							if	(ie_status_w <> '3') then
								ie_cursor_valida_restricao_w := 'S';
							else
								goto final;
							end if;
						exception
						when others then
							ie_cursor_valida_restricao_w := 'N';
						end;
					end if;

					if	(ie_valida_local_atend_p = 'N')	 then
						ie_cursor_valida_restricao_w := 'S';			
					elsif	(ie_valida_local_atend_p = 'S') then		
						begin
							select	nm_usuario_solic,
								ie_tipo_processo
							into	nm_usuario_solic_w,
								ie_tipo_processo_w
							from	pls_requisicao
							where	nr_sequencia = r_C02_w.nr_seq_requisicao;
						exception
						when others then
							nm_usuario_solic_w := 'X';
							ie_tipo_processo_w := null;
						end;

						if 	(nr_seq_local_atend_w <> 0)		and (ie_tipo_processo_w = 'P') and
							(nvl(nm_usuario_solic_w,'X') <> 'X') 	and (pls_obter_local_atend_usuario(nm_usuario_solic_w,cd_estabelecimento_p) = nr_seq_local_atend_w) /*and
							(nr_seq_prestador_cursor_w <> nr_seq_prestador_w)*/ then
							ie_cursor_valida_restricao_w := 'S';
						else
							ie_cursor_valida_restricao_w := 'N';
						end if;
					else
						goto final;
					end if;

					if	(ie_cursor_valida_restricao_w = 'S') then
						qt_liberacao_w	:= qt_liberacao_w + r_C02_w.qt_item;
						qt_solicitacao_w := qt_solicitacao_w + 1;
						nr_seq_exec_req_cursor_ww := r_C02_w.nr_sequencia;
					end if;

					<<final>>
					ie_cursor_valida_restricao_w := 'N';			
				end loop;
			elsif	(ie_valida_cod_prestador_p = 'S') then
				for	r_C03_w in C03( cd_procedimento_w, ie_origem_proced_w, dt_liberacao_ww, dt_liberacao_w,	cd_prestador_w) loop
					if	(r_C03_w.ie_situacao in ('C','G','N','U')) then
						goto final;
					end if;

					if (cd_procedimento_w = r_C03_w.cd_procedimento and r_C03_w.ie_origem_proced = ie_origem_proced_w) then
						ie_cursor_valida_restricao_w := 'S';
					else
						goto final;
					end if;

					if	(nvl(r_C03_w.nr_seq_guia,0) > 0) then
						begin
							select	ie_status
							into	ie_status_w
							from	pls_guia_plano
							where	nr_sequencia = r_C03_w.nr_seq_guia;

							if	(ie_status_w <> '3') then
								ie_cursor_valida_restricao_w := 'S';
							else
								goto final;
							end if;
						exception
						when others then
							ie_cursor_valida_restricao_w := 'N';
						end;
					end if;

					if	(ie_valida_local_atend_p = 'N')	 then
						ie_cursor_valida_restricao_w := 'S';			
					elsif	(ie_valida_local_atend_p = 'S') then		
						begin
							select	nm_usuario_solic,
								ie_tipo_processo
							into	nm_usuario_solic_w,
								ie_tipo_processo_w
							from	pls_requisicao
							where	nr_sequencia = r_C03_w.nr_seq_requisicao;
						exception
						when others then
							nm_usuario_solic_w := 'X';
							ie_tipo_processo_w := null;
						end;

						if 	(nr_seq_local_atend_w <> 0)		and (ie_tipo_processo_w = 'P') and
							(nvl(nm_usuario_solic_w,'X') <> 'X') 	and (pls_obter_local_atend_usuario(nm_usuario_solic_w,cd_estabelecimento_p) = nr_seq_local_atend_w) /*and
							(nr_seq_prestador_cursor_w <> nr_seq_prestador_w)*/ then
							ie_cursor_valida_restricao_w := 'S';
						else
							ie_cursor_valida_restricao_w := 'N';
						end if;
					else
						goto final;
					end if;

					if	(ie_cursor_valida_restricao_w = 'S') then
						qt_liberacao_w	:= qt_liberacao_w + r_C03_w.qt_item;
						qt_solicitacao_w := qt_solicitacao_w + 1;
						nr_seq_exec_req_cursor_ww := r_C03_w.nr_sequencia;
					end if;

					<<final>>
					ie_cursor_valida_restricao_w := 'N';			
				end loop;
			end if;
		elsif	(ie_tipo_pessoa_qtde_p	= 'A') then
			if	(ie_valida_cod_prestador_p = 'N') then
				for	r_C04_w in C04( cd_procedimento_w, ie_origem_proced_w, dt_liberacao_ww, dt_liberacao_w,	nr_seq_segurado_w, nr_seq_prestador_w) loop
					if	(r_C04_w.ie_situacao in ('C','G','N','U')) then
						goto final;
					end if;

					if (cd_procedimento_w = r_C04_w.cd_procedimento and r_C04_w.ie_origem_proced = ie_origem_proced_w) then
						ie_cursor_valida_restricao_w := 'S';
					else
						goto final;
					end if;

					if	(nvl(r_C04_w.nr_seq_guia,0) > 0) then
						begin
							select	ie_status
							into	ie_status_w
							from	pls_guia_plano
							where	nr_sequencia = r_C04_w.nr_seq_guia;

							if	(ie_status_w <> '3') then
								ie_cursor_valida_restricao_w := 'S';
							else
								goto final;
							end if;
						exception
						when others then
							ie_cursor_valida_restricao_w := 'N';
						end;
					end if;

					if	(ie_valida_local_atend_p = 'N')	 then
						ie_cursor_valida_restricao_w := 'S';			
					elsif	(ie_valida_local_atend_p = 'S') then		
						begin
							select	nm_usuario_solic,
								ie_tipo_processo
							into	nm_usuario_solic_w,
								ie_tipo_processo_w
							from	pls_requisicao
							where	nr_sequencia = r_C04_w.nr_seq_requisicao;
						exception
						when others then
							nm_usuario_solic_w := 'X';
							ie_tipo_processo_w := null;
						end;

						if 	(nr_seq_local_atend_w <> 0)		and (ie_tipo_processo_w = 'P') and
							(nvl(nm_usuario_solic_w,'X') <> 'X') 	and (pls_obter_local_atend_usuario(nm_usuario_solic_w,cd_estabelecimento_p) = nr_seq_local_atend_w) /*and
							(nr_seq_prestador_cursor_w <> nr_seq_prestador_w)*/ then
							ie_cursor_valida_restricao_w := 'S';
						else
							ie_cursor_valida_restricao_w := 'N';
						end if;
					else
						goto final;
					end if;

					if	(ie_cursor_valida_restricao_w = 'S') then
						qt_liberacao_w	:= qt_liberacao_w + r_C04_w.qt_item;
						qt_solicitacao_w := qt_solicitacao_w + 1;
						nr_seq_exec_req_cursor_ww := r_C04_w.nr_sequencia;
					end if;

					<<final>>
					ie_cursor_valida_restricao_w := 'N';			
				end loop;
			elsif	(ie_valida_cod_prestador_p = 'S') then
				for	r_C05_w in C05( cd_procedimento_w, ie_origem_proced_w, dt_liberacao_ww, dt_liberacao_w,	nr_seq_segurado_w, cd_prestador_w) loop
					if	(r_C05_w.ie_situacao in ('C','G','N','U')) then
						goto final;
					end if;

					if (cd_procedimento_w = r_C05_w.cd_procedimento and r_C05_w.ie_origem_proced = ie_origem_proced_w) then
						ie_cursor_valida_restricao_w := 'S';
					else
						goto final;
					end if;

					if	(nvl(r_C05_w.nr_seq_guia,0) > 0) then
						begin
							select	ie_status
							into	ie_status_w
							from	pls_guia_plano
							where	nr_sequencia = r_C05_w.nr_seq_guia;

							if	(ie_status_w <> '3') then
								ie_cursor_valida_restricao_w := 'S';
							else
								goto final;
							end if;
						exception
						when others then
							ie_cursor_valida_restricao_w := 'N';
						end;
					end if;

					if	(ie_valida_local_atend_p = 'N')	 then
						ie_cursor_valida_restricao_w := 'S';			
					elsif	(ie_valida_local_atend_p = 'S') then		
						begin
							select	nm_usuario_solic,
								ie_tipo_processo
							into	nm_usuario_solic_w,
								ie_tipo_processo_w
							from	pls_requisicao
							where	nr_sequencia = r_C05_w.nr_seq_requisicao;
						exception
						when others then
							nm_usuario_solic_w := 'X';
							ie_tipo_processo_w := null;
						end;

						if 	(nr_seq_local_atend_w <> 0)		and (ie_tipo_processo_w = 'P') and
							(nvl(nm_usuario_solic_w,'X') <> 'X') 	and (pls_obter_local_atend_usuario(nm_usuario_solic_w,cd_estabelecimento_p) = nr_seq_local_atend_w) /*and
							(nr_seq_prestador_cursor_w <> nr_seq_prestador_w)*/ then
							ie_cursor_valida_restricao_w := 'S';
						else
							ie_cursor_valida_restricao_w := 'N';
						end if;
					else
						goto final;
					end if;

					if	(ie_cursor_valida_restricao_w = 'S') then
						qt_liberacao_w	:= qt_liberacao_w + r_C05_w.qt_item;
						qt_solicitacao_w := qt_solicitacao_w + 1;
						nr_seq_exec_req_cursor_ww := r_C05_w.nr_sequencia;
					end if;

					<<final>>
					ie_cursor_valida_restricao_w := 'N';			
				end loop;
			end if;
		end if;
	end if;

elsif	(ie_tipo_item_p = 6) then
	open C11;
	loop
	fetch C11 into
		nr_seq_guia_cursor_w,
		nr_seq_requisicao_cursor_w,
		nr_seq_material_ww,
		qt_item_w,
		nr_seq_exec_req_cursor_w,
		nr_seq_prestador_cursor_w,
		nr_seq_segurado_cursor_w;
	exit when C11%notfound;
		begin
		
		if	(nr_seq_material_w = nr_seq_material_ww) then
			ie_cursor_valida_restricao_w := 'S';
		else
			goto final2;
		end if;
		
		
		if	(ie_valida_local_atend_p = 'N')	 then
			ie_cursor_valida_restricao_w := 'S';			
		elsif	(ie_valida_local_atend_p = 'S') then		
			begin
				select	nm_usuario_solic,
					ie_tipo_processo
				into	nm_usuario_solic_w,
					ie_tipo_processo_w
				from	pls_requisicao
				where	nr_sequencia = nr_seq_requisicao_cursor_w;
			exception
			when others then
				nm_usuario_solic_w := 'X';
				ie_tipo_processo_w := null;
			end;
			
			if 	(nr_seq_local_atend_w <> 0)		and (ie_tipo_processo_w = 'P') and
				(nvl(nm_usuario_solic_w,'X') <> 'X') 	and (pls_obter_local_atend_usuario(nm_usuario_solic_w,cd_estabelecimento_p) = nr_seq_local_atend_w) and
				(nr_seq_prestador_cursor_w <> nr_seq_prestador_w) then
				ie_cursor_valida_restricao_w := 'S';
			end if;
		else
			goto final2;
		end if;
		
		if	(nvl(nr_seq_guia_cursor_w,0) > 0) then
			begin
				select	ie_status
				into	ie_status_w
				from	pls_guia_plano
				where	nr_sequencia = nr_seq_guia_cursor_w;
				
				if	(ie_status_w <> '3') then
					ie_cursor_valida_restricao_w := 'S';
				else
					goto final2;
				end if;
			exception
			when others then
				ie_cursor_valida_restricao_w := 'N';
			end;
		end if;
		
		if	(ie_cursor_valida_restricao_w = 'S') then
			qt_liberacao_w	:= qt_liberacao_w + qt_item_w;
			qt_solicitacao_w := qt_solicitacao_w + 1;
			nr_seq_exec_req_cursor_ww := nr_seq_exec_req_cursor_w;
		end if;

		<<final2>>
		ie_cursor_valida_restricao_w := 'N';
		end;
	end loop;
	close C11;
end if;


/*r(-20011,qt_liberacao_w||' - '||qt_solicitada_w||' - '||qt_liberada_p||'#@#@');*/
if	(qt_liberacao_w > 0) then
	qt_liberacao_w := qt_liberacao_w + qt_solicitada_w;
end if;

if	(qt_liberacao_w	>= qt_liberada_p) then
	ie_retorno_w	:= 'N';

	begin

	select	a.nr_seq_guia,
		b.dt_execucao,
		substr(obter_nome_pessoa_fisica(a.cd_medico_requisitante, null),1,255)
	into	nr_seq_guia_w,
		dt_execucao_w,
		nm_medico_requisit_w
	from	pls_execucao_requisicao	b,
		pls_execucao_req_item	a
	where	a.nr_seq_execucao	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_exec_req_cursor_ww;

	select	cd_guia
	into	cd_guia_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_w;


	ds_observacao_w := substr('Guia: '||cd_guia_w||'. Data da execu��o: '||to_char(dt_execucao_w,'dd/mm/yyyy')||'. M�dico requisit: '||nm_medico_requisit_w,1,400);
	exception
	when others then
		ds_observacao_w := '';
	end;
end if;

ie_retorno_p	:= ie_retorno_w;
ds_observacao_p	:= ds_observacao_w;

end pls_obter_se_qtd_ocor_exec;
/