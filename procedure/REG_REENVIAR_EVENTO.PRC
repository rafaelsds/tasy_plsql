create or replace
procedure reg_reenviar_evento(	nr_seq_evento_p		Number,
				nr_seq_int_evento_gm_p	Number,
				nr_seq_log_ultima_p	Number,
				nm_usuario_p		Varchar2) is 

ds_parametros_w	Varchar2(255);

begin
if	((nvl(nr_seq_evento_p,0) > 0) and
	(nvl(nr_seq_int_evento_gm_p,0) > 0) and
	(nvl(nr_seq_log_ultima_p,0) > 0)) then
	begin

	reg_resolver_contingencia(nr_seq_log_ultima_p);
	
	select	SUBSTR(obter_texto_entre_caracter(ds_observacao||'|','Parāmetros utilizados:','|'),22,255)
	into	ds_parametros_w
	from	log_integracao_evento
	where	nr_sequencia = nr_seq_int_evento_gm_p;
	
	gravar_integracao_regulacao(nr_seq_evento_p, ds_parametros_w);
	end;
end if;
commit;

end reg_reenviar_evento;
/
