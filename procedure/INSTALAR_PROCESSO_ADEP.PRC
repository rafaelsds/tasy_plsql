create or replace
procedure instalar_processo_adep(	nr_seq_processo_p	number,
									ie_administrar_p	varchar2,
									nm_usuario_p		varchar2,
									nr_seq_assinatura_p	number) is
	
nr_seq_evento_w		number(10,0);
nr_seq_solucao_w	number(10,0);
nr_prescricao_w		number(10,0);
nr_seq_map_w		number(10,0);
nr_seq_horario_w	number(15,0);
qt_vel_infusao_w	prescr_solucao.qt_dosagem%type;
dt_horario_w		date;
nr_etapa_atual_w	number(15);
ie_regra_lanc_conta_w	varchar2(1);
ie_agrupador_w			prescr_material.ie_agrupador%type;
nr_etapa_w				adep_processo.nr_etapa%type;
nr_seq_material_w		adep_processo.nr_seq_material%type;
ie_tipo_item_w			varchar2(1);
ie_tipo_solucao_w		prescr_solucao.ie_tipo_solucao%type;
nr_seq_nut_pac_w		prescr_material.nr_seq_nut_pac%type;
cd_pessoa_fisica_w		prescr_medica.cd_pessoa_fisica%type;
ie_gerar_pend_w			boolean;
nr_atendimento_w		prescr_medica.nr_atendimento%type;
cd_material_w			prescr_mat_hor.cd_material%type;
nr_seq_lote_w			prescr_mat_hor.nr_seq_lote%type;
dt_atend_lote_w			ap_lote.dt_atend_farmacia%type;
dt_entrega_setor_w		ap_lote.dt_entrega_setor%type;
dt_recebimento_setor_w	ap_lote.dt_recebimento_setor%type;
ie_somente_com_baixa_w	varchar2(1 char);
ie_impedir_w			varchar2(1 char);
ie_gera_sem_certificado_w	varchar2(1 char);
ie_dialise_w			varchar2(1 char);
ie_atualizou_w			boolean;
nr_etapa_dialise_w		prescr_mat_hor.nr_etapa_sol%type;

cursor c01 is
select	a.nr_sequencia,
		a.ie_agrupador,
		a.nr_seq_material,
		b.nr_seq_nut_pac,
		a.cd_material,
		a.nr_seq_lote
from	prescr_mat_hor a,
		prescr_material b
where	a.nr_seq_processo	= nr_seq_processo_p
and		a.nr_prescricao = b.nr_prescricao
and		b.nr_sequencia = a.nr_seq_material
and		a.dt_inicio_horario is null
and		a.nr_seq_dialise is null
and		a.dt_suspensao 	is null
and		a.dt_fim_horario 	is null;

cursor cDialise is
select	a.rowid,
		a.nr_prescricao,
		a.nr_seq_solucao,
		a.nr_etapa_sol nr_seq_etapa
from	prescr_mat_hor a,
		prescr_solucao b
where	a.nr_seq_processo	= nr_seq_processo_p
and		a.nr_prescricao = b.nr_prescricao
and		b.nr_seq_solucao = a.nr_seq_solucao
and		a.dt_inicio_horario is null
and		b.nr_seq_dialise is not null
and		a.dt_suspensao is null
and		a.dt_fim_horario is null
and		nvl(a.ie_situacao,'A') = 'A'
and		nvl(a.ie_horario_especial,'N') <> 'S';

begin

if	(nr_seq_processo_p is not null) and
	(ie_administrar_p is not null) and
	(nm_usuario_p is not null) then
	
	obter_param_usuario(1113, 357, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_somente_com_baixa_w);
	ie_regra_lanc_conta_w	:= obter_regra_lanc_conta_adep(wheb_usuario_pck.get_cd_estabelecimento, obter_perfil_ativo, nm_usuario_p, 'AP');
	if	(ie_regra_lanc_conta_w = 'N') then
		ie_regra_lanc_conta_w	:= obter_regra_lanc_conta_adep(wheb_usuario_pck.get_cd_estabelecimento, obter_perfil_ativo, nm_usuario_p, 'CG');
	end if;

	select	max(nr_prescricao),
			max(nr_seq_solucao),
			max(dt_horario_processo),
			max(nr_etapa),
			max(nr_seq_material),
			max(nr_atendimento)
	into	nr_prescricao_w,
			nr_seq_solucao_w,
			dt_horario_w,
			nr_etapa_w,
			nr_seq_material_w,
			nr_atendimento_w
	from	adep_processo
	where	nr_sequencia = nr_seq_processo_p;
		
	if	(ie_administrar_p = 'S') then
		if	(ie_somente_com_baixa_w <> 'N') then
			for c01_w in c01 loop
				begin
					ie_impedir_w	:= obter_se_impede_adm(c01_w.cd_material);
					if	(ie_impedir_w = 'S') then
						if	(ie_somente_com_baixa_w = 'S') then
							--So e possivel administrar uma medicacao apos o atendimento da mesma no atendimento da prescricao/lote! Parametro [357]
							atualiza_status_processo_adep(nr_seq_processo_p,0,'A','RA',sysdate,nm_usuario_p);
							Wheb_mensagem_pck.Exibir_mensagem_abort(173406);
						elsif	(ie_somente_com_baixa_w = 'L') and
							(c01_w.nr_seq_lote is not null) then
							select	max(a.dt_atend_farmacia)
							into	dt_atend_lote_w
							from	ap_lote a
							where	a.nr_sequencia = c01_w.nr_seq_lote;
							if	(dt_atend_lote_w is null) then
								--so e possivel administrar uma medicacao apos o atendimento da mesma no atendimento da prescricao/lote! parametro [357]
								wheb_mensagem_pck.exibir_mensagem_abort(173406);
							end if;
						elsif	(ie_somente_com_baixa_w = 'E') and
							(c01_w.nr_seq_lote is not null) then
							select	max(a.dt_entrega_setor)
							into	dt_entrega_setor_w
							from	ap_lote a
							where	a.nr_sequencia = c01_w.nr_seq_lote;
							if	(dt_entrega_setor_w is null) then
								--so e possivel administrar uma medicacao apos a entrega da mesma no atendimento da prescricao/lote! parametro [357]
								wheb_mensagem_pck.exibir_mensagem_abort(472055);
							end if;
						elsif	(ie_somente_com_baixa_w = 'R') and
							(c01_w.nr_seq_lote is not null) then
							select	max(a.dt_recebimento_setor)
							into	dt_recebimento_setor_w
							from	ap_lote a
							where	a.nr_sequencia = c01_w.nr_seq_lote;
							if	(dt_recebimento_setor_w is null) then
								--so e possivel administrar uma medicacao, apos o recebimento da mesma no atendimento da prescricao/lote!
								wheb_mensagem_pck.exibir_mensagem_abort(819419);
							end if;
						end if;
					end if;
				end;
			end loop;
		end if;

		update	adep_processo
		set		dt_paciente		= sysdate,
				nm_usuario_paciente	= nm_usuario_p
		where	nr_sequencia		= nr_seq_processo_p
		and		dt_paciente		is null
		and		nm_usuario_paciente	is null;
		
		atualiza_status_processo_adep(nr_seq_processo_p, null, 'A', 'A', sysdate, nm_usuario_p);
		
		select 	nvl(max('S'), 'N') ie_dialise
		into	ie_dialise_w
		from	prescr_mat_hor a,
				prescr_solucao b
		where	a.nr_prescricao = b.nr_prescricao
		and		a.nr_seq_solucao = b.nr_seq_solucao
		and		a.nr_seq_processo = nr_seq_processo_p
		and		b.nr_seq_dialise is not null;

		if (ie_dialise_w <> 'S') then
			-- Verificar se o processo e de um item NPT
			if	((nvl(nr_seq_solucao_w,0) = 0) or (nvl(nr_etapa_w,0) = 0) and nr_seq_material_w is not null) then
			
				select	obter_tipo_processo_adep(nr_seq_processo_p)
				into	ie_tipo_item_w
				from	dual;
				
			end if;
			
			if	((nvl(nr_seq_solucao_w,0) > 0) or (nvl(nr_etapa_w,0) > 0) or (ie_tipo_item_w = 'N')) then
				
				ie_gera_sem_certificado_w	:= 'N';
	
				if (wheb_assist_pck.get_gerar_sem_certificado = 'S') then
					ie_gera_sem_certificado_w := adep_obter_se_assin_perfil(50513, obter_perfil_ativo); --ADEP - ADI - Eventos da solucao
				end if;
		
				ie_gerar_pend_w	:= (((wheb_assist_pck.get_cd_certificado IS NOT NULL) OR
								(ie_gera_sem_certificado_w = 'S')) AND
								(obter_data_assinatura_digital(nr_seq_assinatura_p) is null));
											
				select	max(cd_pessoa_fisica)
				into	cd_pessoa_fisica_w
				from	atendimento_paciente
				where	nr_atendimento = nr_atendimento_w;
					
				select	nvl(max(nr_etapa),0)
				into	nr_etapa_atual_w
				from	adep_processo
				where	nr_sequencia = nr_seq_processo_p;
				
				if 	(nr_etapa_atual_w = 0) then			
					nr_etapa_atual_w := Obter_etapa_atual(nr_prescricao_w, nr_seq_solucao_w) + 1;
				end if;
				
				select	nvl(max(ie_agrupador),4)
				into	ie_agrupador_w
				from	prescr_mat_hor
				where	nr_seq_processo = nr_seq_processo_p;
						
				select	prescr_solucao_evento_seq.nextval
				into	nr_seq_evento_w
				from	dual;
				
				select	nvl(max(qt_dosagem),0)
				into	qt_vel_infusao_w
				from	prescr_solucao
				where	nr_prescricao = nr_prescricao_w
				and	nr_seq_solucao = nr_seq_solucao_w;
				
				insert into prescr_solucao_evento	(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_prescricao,
					nr_seq_solucao,
					nr_seq_material,
					ie_forma_infusao,
					ie_tipo_dosagem,
					qt_dosagem,
					qt_vol_infundido,
					qt_vol_desprezado,
					cd_pessoa_fisica,
					ie_alteracao,
					dt_alteracao,
					dt_aprazamento,
					ie_evento_valido,
					nr_seq_motivo,
					ds_observacao,
					ie_tipo_solucao,
					nr_etapa_evento,
					nr_seq_assinatura)
				values	(
					nr_seq_evento_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_prescricao_w,
					decode(ie_agrupador_w, 4, nr_seq_solucao_w, null),
					nr_seq_material_w,
					null,
					null,
					qt_vel_infusao_w,
					null,
					null,
					obter_dados_usuario_opcao(nm_usuario_p, 'C'),
					35,
					sysdate,
					dt_horario_w,
					'S',
					null,
					'',
					decode(ie_agrupador_w, 4, 1, 2),
					nr_etapa_atual_w,
					decode(nvl(nr_seq_assinatura_p,0), 0, null, nr_seq_assinatura_p));
						
					begin					
									
						if	((ie_gerar_pend_w) and nvl(nr_seq_assinatura_p,0) = 0) then	
						
							if ((coalesce(ie_tipo_item_w, 'N') = 'N') and (nvl(nr_seq_solucao_w,0) > 0)) then
								
								select	decode(ie_agrupador_w, 4, 1, 2)
								into	ie_tipo_solucao_w
								from	dual;
		
								ie_tipo_item_w := obter_tipo_solucao_adep(ie_tipo_solucao_w);
							
							end if;
	
							if (ie_agrupador_w = 4) then				
								Gerar_registro_pendente_PEP('ADEP',nr_prescricao_w, cd_pessoa_fisica_w, nr_atendimento_w, nm_usuario_p, 'A', null,ie_tipo_item_w,null,null,null,null,null,null,null,null,null,nr_seq_solucao_w,nvl(nr_etapa_atual_w,0),null,null,null,nr_seq_evento_w);
							else
								Gerar_registro_pendente_PEP('ADEP',nr_prescricao_w, cd_pessoa_fisica_w, nr_atendimento_w, nm_usuario_p, 'A', null,ie_tipo_item_w,null,null,null,null,null,null,null,null,null,null,nvl(nr_etapa_atual_w,0),null,null,null,nr_seq_evento_w);
							end if;
						
						end if;		
	
					exception when others then
						null;
					end;				
									
					if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
					
				open C01;
				loop
				fetch C01 into	
					nr_seq_horario_w,
					ie_agrupador_w,
					nr_seq_material_w,
					nr_seq_nut_pac_w,
					cd_material_w,
					nr_seq_lote_w;
				exit when C01%notfound;
					
					update	prescr_mat_hor
					set		dt_inicio_horario 		= sysdate,
							nm_usuario				= nm_usuario_p,
							dt_primeira_checagem	= null
					where	nr_sequencia			= nr_seq_horario_w;
					
					if	(ie_regra_lanc_conta_w = 'S') then
						gerar_estornar_adep_map(null, nr_seq_horario_w, null, 'G', sysdate, nm_usuario_p, nr_seq_map_w,null,null,null);
					elsif	(ie_regra_lanc_conta_w = 'L') and
						(obter_se_lote_hor_conta_pac(nr_seq_horario_w) = 'S') then					
						gerar_estornar_adep_map(null, nr_seq_horario_w, null, 'G', sysdate, nm_usuario_p, nr_seq_map_w,null,null,null);
					end if;
					
				end loop;
				close C01;
							
				if	(ie_agrupador_w = 4) then
				
					update	prescr_mat_hor
					set		dt_reinicio = decode(dt_reinicio, null, decode(dt_interrupcao, null, null, sysdate), null)
					where	dt_horario = (	select	max(dt_horario)
											from	prescr_mat_hor
											where	nvl(ie_situacao,'A') = 'A'
											and		nr_etapa_sol < nr_etapa_atual_w
											and		dt_suspensao is null
											and		nr_seq_solucao = nr_seq_solucao_w
											and		nr_prescricao = nr_prescricao_w)
					and		nvl(ie_situacao,'A') = 'A'
					and		nr_etapa_sol < nr_etapa_atual_w
					and		dt_suspensao is null
					and		nr_seq_solucao = nr_seq_solucao_w
					and		nr_prescricao = nr_prescricao_w;	
					
					update 	prescr_solucao
					set 	ie_status = 'I',
							dt_status = sysdate
					where 	nr_prescricao 	= nr_prescricao_w
					and		nr_seq_solucao 	= nr_seq_solucao_w
					and 	nvl(ie_status, 'N') <> 'I';
				
				elsif	(ie_agrupador_w = 11) then
						
					update	nut_pac
					set		ie_status = 'I',
							dt_status = sysdate
					where	nr_prescricao = nr_prescricao_w
					and		nr_sequencia = nr_seq_nut_pac_w
					and		nvl(ie_status,'N') <> 'I';
				
					update	nut_paciente_hor
					set		ie_status = 'I'
					where	nr_seq_nut_protocolo	= nr_seq_nut_pac_w				
					and		nvl(ie_horario_especial,'N')	= 'N'
					and		nvl(ie_status,'N') <> 'I';
					
					update	prescr_solucao_evento
					set		nr_seq_nut_neo = nr_seq_nut_pac_w,
							ie_tipo_solucao = 6						
					where	nr_sequencia = nr_seq_evento_w;
				
				else
					update	prescr_mat_hor
					set		dt_fim_horario = sysdate
					where	dt_horario = (	select	max(dt_horario)
											from	prescr_mat_hor
											where	nvl(ie_situacao,'A') = 'A'
											and		nr_etapa_sol < nr_etapa_atual_w
											and		dt_suspensao is null
											and		nr_seq_material = nr_seq_material_w
											and		nr_prescricao = nr_prescricao_w)
					and		nvl(ie_situacao,'A') = 'A'
					and		nr_etapa_sol < nr_etapa_atual_w
					and		dt_suspensao is null
					and		nr_seq_material = nr_seq_material_w
					and		nr_prescricao = nr_prescricao_w
					and		dt_fim_horario is null
					and		nvl(ie_horario_especial,'N') <> 'S';
					
					if	(ie_agrupador_w = 8) then
						update	prescr_material	a
						set		a.ie_status 	= 'I',
								a.dt_status 	= sysdate
						where	a.nr_prescricao = nr_prescricao_w
						and		a.nr_sequencia 	= nr_seq_material_w
						and		a.ie_agrupador 	= 8;
					end if;
				end if;
				
			end if;
		else
			for cDialise_w in cDialise loop
				begin
					nr_seq_solucao_w := cDialise_w.nr_seq_solucao;
					nr_prescricao_w := cDialise_w.nr_prescricao;
					nr_etapa_dialise_w := cDialise_w.nr_seq_etapa;

					update	prescr_mat_hor a
					set		a.dt_inicio_horario = sysdate
					where	a.rowid = cDialise_w.rowid;
					
					ie_atualizou_w := (sql%rowcount > 0);
				end;
			end loop;
			
			if (ie_atualizou_w) then
				
				update	prescr_solucao
				set		ie_status		= 'II',
						dt_status 		= sysdate
				where	nr_seq_solucao 	= nr_seq_solucao_w
				and		nr_prescricao 	= nr_prescricao_w;

				INSERT INTO hd_prescricao_evento (
					nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_prescricao,
					nr_seq_solucao,
					ie_evento,
					dt_evento,
					cd_pessoa_evento,
					nr_etapa_evento,
					nr_seq_assinatura)
				VALUES (
					hd_prescricao_evento_seq.nextval,
					SYSDATE,
					nm_usuario_p,
					SYSDATE,
					nm_usuario_p,
					nr_prescricao_w,
					nr_seq_solucao_w,
					'II',
					SYSDATE,
					SUBSTR(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10),
					nr_etapa_dialise_w,
					nr_seq_assinatura_p);				
			end if;
		end if;
	elsif	(ie_administrar_p = 'N') then
		atualiza_status_processo_adep(nr_seq_processo_p, null, 'A', 'RA', sysdate, nm_usuario_p);
	end if;
	
	update	adep_processo
	set		dt_primeira_checagem = null
	where	nr_sequencia = nr_seq_processo_p
	and		dt_paciente is not null
	and		dt_primeira_checagem is not null;

end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end instalar_processo_adep;
/
