create or replace
procedure vincular_lote_hist_conv_glosa (	nr_seq_conpaci_ret_hist_p	number,
					nr_sequencia_p	number) is
begin
	if(nr_sequencia_p > 0)then
		update 	convenio_retorno_glosa 
		set 	nr_seq_conpaci_ret_hist = nr_seq_conpaci_ret_hist_p
		where 	nr_sequencia = nr_sequencia_p;
		commit;
	end if;
end vincular_lote_hist_conv_glosa;
/