create or replace
procedure consist_componente_kit_barra(
			cd_material_p			number,
			nr_seq_kit_estoque_p		number,
			qt_material_p			number,
			nm_usuario_p			varchar2,
			ie_consiste_quant_p		varchar2,
			ie_consiste_material_p		varchar2,
			nr_seq_lote_fornec_p		number,
			nr_seq_item_kit_p			number,
			ie_tipo_barras_p			varchar2,
			ds_erro_p			out	varchar2) is

nr_sequencia_w			number(10);
cd_kit_material_w			number(5);
qt_material_kit_w			number(11,4);
cd_estabelecimento_w		number(4);
cd_local_estoque_w		number(5);
ie_baixa_estoque_w		varchar2(1);
qt_estoque_w			number(22,4);
ds_material_w			varchar2(255);
ds_erro_w			varchar2(255);
ie_disp_comp_kit_estoque_w		varchar2(1);
ie_disp_reg_kit_estoque_w		varchar2(1);
ie_consistir_w			varchar2(1);
nr_seq_lote_fornec_w		number(10,0);
qt_existe_w			number(10,0);
qt_total_mat_w			number(11,4);
nr_seq_mat_qtde_w		number(10,0);
cd_material_w			number(6);
nr_seq_solic_kit_w		number(10);
ie_consignado_w			material.ie_consignado%type;
cd_fornec_consignado_w		kit_estoque_comp.cd_fornec_consignado%type;
ie_tipo_saldo_w			varchar2(15);

begin

cd_material_w := cd_material_p;

select	nvl(max(cd_estabelecimento),0),
	nvl(max(cd_local_estoque),0),
	max(nr_seq_solic_kit)
into	cd_estabelecimento_w,
	cd_local_estoque_w,
	nr_seq_solic_kit_w
from	kit_estoque
where	nr_sequencia = nr_seq_kit_estoque_p;

select	nvl(Obter_Valor_Param_Usuario(143, 81, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w), 'N')
into	ie_consistir_w
from 	dual;

if	(nvl(nr_seq_lote_fornec_p,0) > 0) then
	nr_seq_lote_fornec_w	:= nr_seq_lote_fornec_p;
end if;

ie_disp_comp_kit_estoque_w		:= 'N';
ie_disp_reg_kit_estoque_w		:= 'N';

if	(ie_consistir_w = 'S') and
	(cd_estabelecimento_w > 0) and
	(cd_local_estoque_w > 0) then
	select	nvl(max(ie_disp_comp_kit_estoque), 'N'),
		nvl(max(ie_disp_reg_kit_estoque),'N')
	into	ie_disp_comp_kit_estoque_w,
		ie_disp_reg_kit_estoque_w
	from	parametro_estoque
	where	cd_estabelecimento = cd_estabelecimento_w;
end if;

if	(ie_consiste_quant_p = 'S') or
	(ie_consiste_material_p = 'S') then
	begin
	select	max(cd_kit_material)
	into	cd_kit_material_w
	from	kit_estoque
	where	nr_sequencia = nr_seq_kit_estoque_p;
	
	select	substr(cd_material || ' - ' || ds_material,1,255)
	into	ds_material_w
	from	material
	where	cd_material = cd_material_p;	

	select	nvl(max(cd_material),cd_material_p)
	into	cd_material_w
	from	componente_kit
	where	nr_sequencia		= nr_seq_item_kit_p
	and	cd_kit_material 	= cd_kit_material_w
	and	((cd_estab_regra is null) or (cd_estab_regra = cd_estabelecimento_w));
	end;
end if;

if	(ds_erro_w is null) then
	begin
	select	count(*),
		sum(qt_material)
	into	qt_existe_w,
		qt_total_mat_w
	from	kit_estoque_comp
	where	nr_seq_kit_estoque 	= nr_seq_kit_estoque_p
	and	nr_seq_item_kit		= nr_seq_item_kit_p
	--and	(cd_material	= cd_material_p or verifica_material_similar(cd_material,cd_material_p) = 'S')
	and 	((ie_gerado_barras = 'N') or (ie_gerado_barras = 'S' and ie_tipo_barras_p = 'CA'))
	and	(((nr_seq_lote_fornec is null) or (ie_gerado_barras = 'N')) or (ie_tipo_barras_p = 'CA'));
	
	if	(qt_existe_w > 0) then
		begin
		select	max(nr_sequencia)
		into	nr_seq_mat_qtde_w
		from	kit_estoque_comp
		where	nr_seq_kit_estoque 	= nr_seq_kit_estoque_p
		and	nr_seq_item_kit		= nr_seq_item_kit_p
		--and	(cd_material	= cd_material_p or verifica_material_similar(cd_material,cd_material_p) = 'S')
		and	qt_material		= qt_material_p
		and 	((ie_gerado_barras = 'N') or (ie_gerado_barras = 'S' and ie_tipo_barras_p = 'CA'))
		and	(((nr_seq_lote_fornec is null) or (ie_gerado_barras = 'N')) or (ie_tipo_barras_p = 'CA'));

		select	max(ie_consignado)
		into	ie_consignado_w
		from	material
		where	cd_material = cd_material_p;
	
		if	(ie_consignado_w = '1') and (nvl(nr_seq_lote_fornec_w,0) > 0) then
			select	max(cd_cgc_fornec)
			into	cd_fornec_consignado_w
			from	material_lote_fornec
			where	nr_sequencia = nr_seq_lote_fornec_w;
		
		elsif	(ie_consignado_w = '2') and (nvl(nr_seq_lote_fornec_w,0) > 0) then
			obter_fornec_consig_ambos(cd_estabelecimento_w, cd_material_p, nr_seq_lote_fornec_w, cd_local_estoque_w, ie_tipo_saldo_w, cd_fornec_consignado_w);
		end if;		
		
		if	(nvl(nr_seq_mat_qtde_w,0) > 0) then
			update	kit_estoque_comp
			set	nr_seq_lote_fornec 	= nr_seq_lote_fornec_w,
				ie_gerado_barras	= 'S',
				cd_material		= cd_material_p,
				cd_fornec_consignado	= cd_fornec_consignado_w
			where	nr_seq_kit_estoque 	= nr_seq_kit_estoque_p
			and	nr_sequencia		= nr_seq_mat_qtde_w;
		elsif	(qt_material_p <= qt_total_mat_w) and
			(nvl(nr_seq_lote_fornec_w,0) > 0) then
			begin
			/*se bipei quantidade menor mas com lote fornecedor, desdobra o material*/
			update	kit_estoque_comp
			set	qt_material		= nvl(qt_material_p,0),
				ie_gerado_barras	= 'S',
				nr_seq_lote_fornec 	= nr_seq_lote_fornec_w,
				cd_material		= cd_material_p,
				cd_fornec_consignado	= cd_fornec_consignado_w
			where	nr_seq_kit_estoque 	= nr_seq_kit_estoque_p
			and	nr_seq_item_kit		= nr_seq_item_kit_p
			and	((nr_seq_lote_fornec is null) or (ie_gerado_barras = 'N'))
			and rownum < 2;
			
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
			
			delete from kit_estoque_comp
			where	nr_seq_kit_estoque 	= nr_seq_kit_estoque_p
			and 	nr_seq_item_kit		= nr_seq_item_kit_p
			and	ie_gerado_barras 	= 'N';				
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	kit_estoque_comp
			where	nr_seq_kit_estoque = nr_seq_kit_estoque_p;
			
									
			/*gera um novo componente com a quantidade restante*/
			
			if (qt_material_p < qt_total_mat_w) then
				begin
				insert into kit_estoque_comp(
					nr_seq_kit_estoque,
					nr_sequencia,
					cd_material,
					dt_atualizacao,
					nm_usuario,
					qt_material,
					ie_gerado_barras,
					nr_seq_lote_fornec,
					nr_seq_item_kit)
				values(	nr_seq_kit_estoque_p,
					nr_sequencia_w,
					cd_material_w,
					sysdate,
					nm_usuario_p,
					(nvl(qt_total_mat_w,0) - nvl(qt_material_p,0)),
					'N',
					null,
					nr_seq_item_kit_p);
				end;	
			end if;		
			end;	
		elsif	(qt_material_p <= qt_total_mat_w) and
			(nvl(nr_seq_lote_fornec_w,0) = 0) then
			begin
			select	count(*),
				sum(qt_material)
			into	qt_existe_w,
				qt_total_mat_w
			from	kit_estoque_comp
			where	nr_seq_kit_estoque 	= nr_seq_kit_estoque_p
			--and	(cd_material	= cd_material_p or verifica_material_similar(cd_material,cd_material_p) = 'S')
			and 	nr_seq_item_kit		= nr_seq_item_kit_p
			and 	ie_gerado_barras = 'N';
	
	
			update	kit_estoque_comp
			set	qt_material		= nvl(qt_material_p,0),
				ie_gerado_barras 	= 'S',
				cd_material		= cd_material_p,
				nr_seq_lote_fornec	= null
			where	nr_seq_kit_estoque 	= nr_seq_kit_estoque_p
			--and	(cd_material	= cd_material_p or verifica_material_similar(cd_material,cd_material_p) = 'S')
			and 	nr_seq_item_kit		= nr_seq_item_kit_p
			and	ie_gerado_barras 	= 'N'
			and rownum < 2;
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
			
			delete from kit_estoque_comp
			where	nr_seq_kit_estoque 	= nr_seq_kit_estoque_p
			--and	(cd_material	= cd_material_p or verifica_material_similar(cd_material,cd_material_p) = 'S')
			and 	nr_seq_item_kit		= nr_seq_item_kit_p
			and	ie_gerado_barras 	= 'N';	
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	kit_estoque_comp
			where	nr_seq_kit_estoque = nr_seq_kit_estoque_p;
			
			/*gera um novo componente com a quantidade restante*/
			if (qt_material_p < qt_total_mat_w) then
				begin
				insert into kit_estoque_comp(
					nr_seq_kit_estoque,
					nr_sequencia,
					cd_material,
					dt_atualizacao,
					nm_usuario,
					qt_material,
					ie_gerado_barras,
					nr_seq_lote_fornec,
					nr_seq_item_kit)
				values(	nr_seq_kit_estoque_p,
					nr_sequencia_w,
					cd_material_w,
					sysdate,
					nm_usuario_p,
					(nvl(qt_total_mat_w,0) - nvl(qt_material_p,0)),
					'N',
					null,
					nr_seq_item_kit_p);
				end;	
			end if;
			end;
		end if;
		
		/*OS 586013*/
		if	(nr_seq_solic_kit_w is not null) then
			update	kit_estoque
			set	nm_usuario_montagem = nm_usuario_p,
				dt_montagem = sysdate
			where	nr_sequencia = nr_seq_kit_estoque_p;
		end if;
		
		end;
	end if;
	
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	end;
end if;

ds_erro_p	:= substr(ds_erro_w,1,255);

END consist_componente_kit_barra;
/
