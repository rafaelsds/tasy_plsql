create or replace
procedure vincula_nf_xml_consist(
			nr_sequencia_p			number,
			nr_nota_fiscal_arq_p		varchar2,
			cd_serie_nf_arq_p		varchar2,
			cd_rfc_arq_p			varchar2,
			vl_total_nf_arq_p		varchar2,
			ie_retorno_p		out	varchar2,
			ds_erro_p		out	varchar2
			) is 

nr_nota_fiscal_w	nota_fiscal.nr_nota_fiscal%type;	
cd_serie_nf_w		nota_fiscal.cd_serie_nf%type;	
vl_total_nota_w		nota_fiscal.vl_total_nota%type;
cd_rfc_emitente_w	varchar2(255);

begin

ie_retorno_p := 'S';
ds_erro_p    := '';

select	max(nr_nota_fiscal),
	max(cd_serie_nf),	
	substr(decode(max(ie_tipo_nota),'EF',obter_dados_pf_pj(max(cd_pessoa_fisica),null,'RFC'),obter_dados_pf_pj(null,max(cd_cgc_emitente),'RFC')),1,255),
	max(vl_total_nota)
into	nr_nota_fiscal_w,
	cd_serie_nf_w,
	cd_rfc_emitente_w,
	vl_total_nota_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

if	(nr_nota_fiscal_w <> trim(nr_nota_fiscal_arq_p)) then
	begin
	ie_retorno_p := 'N';
	ds_erro_p    := substr(WHEB_MENSAGEM_PCK.get_texto(352400, 'NR_NOTA_FISCAL_ARQ='||trim(nr_nota_fiscal_arq_p)||';'||'NR_NOTA_FISCAL='||nr_nota_fiscal_w) || chr(13) || chr(10),1,255);
	end;
end if;							

if	(cd_serie_nf_w <> trim(cd_serie_nf_arq_p)) then
	begin
	ie_retorno_p := 'N';
	ds_erro_p    := substr(ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(352401, 'CD_SERIE_NF_ARQ='||trim(cd_serie_nf_arq_p)||';'||'CD_SERIE_NF='||cd_serie_nf_w) || chr(13) || chr(10),1,255);
	end;
end if;


if	(cd_rfc_emitente_w <> trim(cd_rfc_arq_p)) then
	begin
	ie_retorno_p := 'N';
	ds_erro_p    := substr(ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(352402, 'CD_RFC_ARQ='||trim(cd_rfc_arq_p)||';'||'CD_RFC='||cd_rfc_emitente_w) || chr(13) || chr(10),1,255);
	end;
end if;

begin
if	(vl_total_nota_w <> to_number(replace(vl_total_nf_arq_p,'.',','))) then
	begin
	ie_retorno_p := 'N';
	ds_erro_p    := substr(ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(352403, 'VL_TOTAL_NOTA_ARQ='||replace(vl_total_nf_arq_p,'.',',')||';'||'VL_TOTAL_NOTA='||vl_total_nota_w) || chr(13) || chr(10),1,255);
	end;
end if;

exception
	when others then
	ds_erro_p := ds_erro_p;
end;

commit;

end vincula_nf_xml_consist;
/