create or replace
procedure diops_fin_gerar_solvencia
			(	nr_seq_operadora_p		number,
				nr_seq_transacao_p		number,
				nr_seq_periodo_p		number,
				nm_usuario_p			varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar as informa��es de valores de receita conforme as regras e o per�odo
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	PLS_GERAR_DIOPS_FINANCEIRO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
				
ds_solvencia_w			varchar2(255);
cd_solvencia_w			varchar2(20);
cd_conta_contabil_w		varchar2(20);
ie_tipo_outorgante_w		varchar2(2);
ie_tipo_solvencia_w		varchar2(1)	:= 'O';
ie_tipo_solvencia_ww		varchar2(1);
ie_normal_encerramento_w	varchar2(1);
vl_saldo_w			number(15,2)	:= 0;
cd_estabelecimento_w		number(4);
dt_periodo_inicial_w		date;
dt_periodo_final_w		date;
dt_periodo_inicial_12_w		date;
dt_periodo_final_12_w		date;
dt_periodo_inicial_36_w		date;
dt_periodo_final_36_w		date;
dt_periodo_inicial_60_w		date;
dt_periodo_final_60_w		date;

Cursor C01 is
	select	a.cd_solvencia,
		a.ds_solvencia,
		b.cd_conta_contabil,
		nvl(c.vl_movimento,0),
		ie_tipo_solvencia
	from	ctb_mes_ref			d,
		ctb_balancete_v			c,
		diops_trans_conta_solv		b,
		diops_conta_ans_solvencia	a
	where	a.nr_sequencia			= b.nr_seq_trans_conta
	and	b.cd_conta_contabil		= c.cd_conta_contabil
	and	c.nr_seq_mes_ref		= d.nr_sequencia
	and	a.nr_seq_transacao		= nr_seq_transacao_p
	and	c.ie_normal_encerramento	= 'E'
	and	a.ie_tipo_solvencia		= ie_tipo_solvencia_w
	and	a.cd_solvencia in ('LNR_OPER', 'RAN_OPER','AEE_OPER','LNR_SEG')
	and	b.ds_origem_conta is null
	and	d.dt_referencia between dt_periodo_inicial_w and dt_periodo_final_w
	union all
	select	a.cd_solvencia,
		a.ds_solvencia,
		b.cd_conta_contabil,
		diops_obter_valor_solvencia(nr_seq_operadora_p, nr_seq_transacao_p, nr_seq_periodo_p, b.nr_sequencia) vl_movimento,
		ie_tipo_solvencia
	from	diops_trans_conta_solv		b,
		diops_conta_ans_solvencia	a
	where	a.nr_sequencia			= b.nr_seq_trans_conta
	and	a.nr_seq_transacao		= nr_seq_transacao_p
	and	a.ie_tipo_solvencia		= ie_tipo_solvencia_w
	and	a.cd_solvencia			= 'AEE_OPER'
	and	b.ds_origem_conta is not null
	union all
	select	a.cd_solvencia,
		a.ds_solvencia,
		b.cd_conta_contabil,
		nvl(c.vl_movimento,0),
		ie_tipo_solvencia
	from	ctb_mes_ref			d,
		ctb_balancete_v			c,
		diops_trans_conta_solv		b,
		diops_conta_ans_solvencia	a
	where	a.nr_sequencia			= b.nr_seq_trans_conta
	and	b.cd_conta_contabil		= c.cd_conta_contabil
	and	c.nr_seq_mes_ref		= d.nr_sequencia
	and	a.nr_seq_transacao		= nr_seq_transacao_p
	and	c.ie_normal_encerramento	= 'E'
	and	a.ie_tipo_solvencia		= ie_tipo_solvencia_w
	and	a.cd_solvencia in ('CPL_PRE_OPER', 'CPL_POS_OPER')
	and	d.dt_referencia between dt_periodo_inicial_12_w and dt_periodo_final_12_w
	union all
	select	a.cd_solvencia,
		a.ds_solvencia,
		b.cd_conta_contabil,
		nvl(c.vl_movimento,0),
		ie_tipo_solvencia
	from	ctb_mes_ref			d,
		ctb_balancete_v			c,
		diops_trans_conta_solv		b,
		diops_conta_ans_solvencia	a
	where	a.nr_sequencia			= b.nr_seq_trans_conta
	and	b.cd_conta_contabil		= c.cd_conta_contabil
	and	c.nr_seq_mes_ref		= d.nr_sequencia
	and	a.nr_seq_transacao		= nr_seq_transacao_p
	and	c.ie_normal_encerramento	= 'E'
	and	a.ie_tipo_solvencia		= ie_tipo_solvencia_w
	and	a.cd_solvencia in ('EIL_PRE_OPER', 'EIL_POS_OPER', 'PRR_PRE_SEG', 'PRR_POS_SEG')
	and	d.dt_referencia between dt_periodo_inicial_36_w and dt_periodo_final_36_w
	union all
	select	a.cd_solvencia,
		a.ds_solvencia,
		b.cd_conta_contabil,
		nvl(c.vl_movimento,0),
		ie_tipo_solvencia
	from	ctb_mes_ref			d,
		ctb_balancete_v			c,
		diops_trans_conta_solv		b,
		diops_conta_ans_solvencia	a
	where	a.nr_sequencia			= b.nr_seq_trans_conta
	and	b.cd_conta_contabil		= c.cd_conta_contabil
	and	c.nr_seq_mes_ref		= d.nr_sequencia
	and	a.nr_seq_transacao		= nr_seq_transacao_p
	and	c.ie_normal_encerramento	= 'E'
	and	a.ie_tipo_solvencia		= ie_tipo_solvencia_w
	and	a.cd_solvencia in ('SIR_PRE_SEG', 'SIR_POS_SEG')
	and	d.dt_referencia between dt_periodo_inicial_60_w and dt_periodo_final_60_w
	union all
	select	cd_solvencia,
		ds_solvencia,
		'' cd_conta_contabil,
		0 vl_saldo,
		ie_tipo_solvencia
	from 	diops_conta_ans_solvencia
	where 	nr_seq_transacao		= nr_seq_transacao_p
	order by 
		1,
		3;

Cursor C02 is
	select	cd_solvencia,
		ds_solvencia,
		ie_tipo_solvencia,
		nvl(sum(vl_saldo),0)
	from	w_diops_fin_solvencia
	where	nr_seq_periodo	= nr_seq_periodo_p
	group by
		cd_solvencia,
		ds_solvencia,
		ie_tipo_solvencia;
	
begin
begin /* Obter o per�odo trimestral do DIOPS */
select	nvl(a.dt_periodo_inicial, ''),
	nvl(a.dt_periodo_final, ''),
	nvl(a.ie_normal_encerramento, 'N')
into	dt_periodo_inicial_w,
	dt_periodo_final_w,
	ie_normal_encerramento_w
from	diops_periodo	a
where	a.nr_sequencia	= nr_seq_periodo_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(174234, 'NR_SEQ_OPERADORA=' || nr_seq_operadora_p || ';' ||
							'NR_SEQ_PERIODO=' || nr_seq_periodo_p);
end;	

select	nvl(max(a.ie_tipo_outorgante), '')
into	ie_tipo_outorgante_w
from	pls_outorgante	a
where	a.nr_sequencia	= nr_seq_operadora_p;

if	(ie_tipo_outorgante_w = '8') then
	ie_tipo_solvencia_w	:= 'S';
end if;

/* Obter o estabelecimento da operadora */
begin
select	a.cd_estabelecimento
into	cd_estabelecimento_w
from	pls_outorgante	a
where	a.nr_sequencia	= nr_seq_operadora_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(183309, 'NR_SEQ_OPERADORA=' || nr_seq_operadora_p);
end;

/* 	Para as solv�ncias CPL_PRE_OPER e CPL_POS_OPER deve ser pego os valores de 12 meses antes ao per�odo inicial do DIOPS 
	Para as solv�ncias EIL_PRE_OPER e EIL_POS_OPER deve ser pego os valores de 36 meses antes ao per�odo inicial do DIOPS 
	Para as solv�ncias SIR_PRE_SEG e SIR_POS_SEG deve ser pego os valores de 36 meses antes ao per�odo inicial do DIOPS 	
*/
/* Felip e - OS 178537 - 13/11/2009 - Considerar o per�odo que est� sendo enviado o DIOPS */
select	pkg_date_utils.start_of(pkg_date_utils.add_month(dt_periodo_final_w, -11,0), 'MONTH',0),
	dt_periodo_final_w,
	pkg_date_utils.start_of(pkg_date_utils.add_month(dt_periodo_final_w, -35,0), 'MONTH',0),
	dt_periodo_final_w,
	pkg_date_utils.start_of(pkg_date_utils.add_month(dt_periodo_final_w, -59,0), 'MONTH',0),
	dt_periodo_final_w
into	dt_periodo_inicial_12_w,
	dt_periodo_final_12_w,
	dt_periodo_inicial_36_w,
	dt_periodo_final_36_w,
	dt_periodo_inicial_60_w,
	dt_periodo_final_60_w
from 	dual;

open C01;
loop
fetch C01 into	
	cd_solvencia_w,
	ds_solvencia_w,
	cd_conta_contabil_w,
	vl_saldo_w,
	ie_tipo_solvencia_ww;
exit when C01%notfound;
	begin
	insert into w_diops_fin_solvencia
		(nr_sequencia,
		nr_seq_operadora,
		nr_seq_transacao,
		cd_solvencia,
		ds_solvencia,
		vl_saldo,
		cd_conta_contabil,
		nr_seq_periodo,
		dt_atualizacao,
		nm_usuario,
		ie_tipo_solvencia)
	values	(w_diops_fin_solvencia_seq.nextval,
		nr_seq_operadora_p,
		nr_seq_transacao_p,
		cd_solvencia_w,
		ds_solvencia_w,
		vl_saldo_w,
		cd_conta_contabil_w,
		nr_seq_periodo_p,
		sysdate,
		nm_usuario_p,
		ie_tipo_solvencia_ww);
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	cd_solvencia_w,
	ds_solvencia_w,
	ie_tipo_solvencia_ww,
	vl_saldo_w;
exit when C02%notfound;
	begin
	insert into diops_fin_solvencia
		(nr_sequencia,
		cd_estabelecimento,
		nr_seq_periodo,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_solvencia,
		ds_solvencia,
		vl_saldo,
		ie_tipo_solvencia,
		nr_seq_operadora)
	values	(diops_fin_solvencia_seq.nextval,
		cd_estabelecimento_w,
		nr_seq_periodo_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_solvencia_w,
		ds_solvencia_w,
		vl_saldo_w,
		ie_tipo_solvencia_ww,
		nr_seq_operadora_p);
	end;
end loop;
close C02;

end diops_fin_gerar_solvencia;
/