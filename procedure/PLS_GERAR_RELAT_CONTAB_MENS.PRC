create or replace
procedure pls_gerar_relat_contab_mens
				(	dt_inicial_p		date,
					dt_final_p		date,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 
			
nr_seq_OUTORGANTE_w			number(10);
IE_TIPO_OPERACAO_w			varchar2(10);
ie_tipo_contrato_w			varchar2(10);
ie_preco_w				varchar2(10);

vl_item_w				number(15,2);
vl_ato_cooperado_prin_w			number(15,2);
vl_ato_cooperado_aux_w			number(15,2);
vl_ato_nao_cooperado_w			number(15,2);
vl_tot_item_w				number(15,2);
vl_tot_ato_cooperado_prin_w		number(15,2);
vl_tot_ato_cooperado_aux_w		number(15,2);
vl_tot_ato_nao_cooperado_w		number(15,2);
vl_tot_outros_w				number(15,2);

ie_tipo_mensalidade_w			varchar2(10);
nr_vetor_w				number(10);
i					number(10);
nr_seq_relat_cont_w			number(10);
dt_inicial_w				date;
dt_final_w				date;

type itens_mensalidade is record (ie_tipo_item varchar2(10));
type vetor is table of itens_mensalidade index by binary_integer;
vetor_w				vetor;	


Cursor C01 is
	select	IE_TIPO_OPERACAO
	from	PLS_TIPO_OPERACAO
	where	nr_seq_OUTORGANTE	= nr_seq_OUTORGANTE_w;
	
Cursor C02 is
	select	'PF'
	from	dual
	union all
	select	'PJ'
	from	dual;
	
Cursor C03 is
	select	VL_DOMINIO
	from	VALOR_DOMINIO
	where	cd_dominio	= 1669;
	
Cursor C04 is
	select	VL_DOMINIO
	from	VALOR_DOMINIO
	where	cd_dominio	= 1930;
	
Cursor C05 is
	select	nvl(c.VL_ITEM,0),
		nvl(c.VL_ATO_COOPERADO,0),
		nvl(c.VL_ATO_AUXILIAR,0),
		nvl(c.VL_ATO_NAO_COOPERADO,0)
	from	pls_mensalidade_seg_item	c,
		pls_mensalidade_segurado	b,
		pls_mensalidade			a,
		pls_plano			d,
		pls_lote_mensalidade	e
	where	c.nr_seq_mensalidade_seg	= b.nr_sequencia
	and	b.nr_seq_mensalidade		= a.nr_sequencia
	and	b.nr_seq_plano			= d.nr_sequencia
	and	a.nr_seq_lote			= e.nr_sequencia
	and	a.ie_cancelamento is null
	and	a.dt_referencia			>= dt_inicial_w
	and	a.dt_referencia			<= dt_final_w
	and	c.IE_TIPO_ITEM			= ie_tipo_mensalidade_w
	and	d.ie_tipo_operacao		= ie_tipo_operacao_w
	and	d.ie_preco			= ie_preco_W
	and	d.ie_tipo_contratacao		= 'I'
	and e.ie_Status				= 2;
	
Cursor C06 is
	select	nvl(c.VL_ITEM,0),
		nvl(c.VL_ATO_COOPERADO,0),
		nvl(c.VL_ATO_AUXILIAR,0),
		nvl(c.VL_ATO_NAO_COOPERADO,0)
	from	pls_mensalidade_seg_item	c,
		pls_mensalidade_segurado	b,
		pls_mensalidade			a,
		pls_plano			d,
		pls_lote_mensalidade	e
	where	c.nr_seq_mensalidade_seg	= b.nr_sequencia
	and	b.nr_seq_mensalidade		= a.nr_sequencia
	and	b.nr_seq_plano			= d.nr_sequencia
	and a.nr_seq_lote			 = e.nr_sequencia
	and	a.ie_cancelamento is null
	and	a.dt_referencia			>= dt_inicial_w
	and	a.dt_referencia			<= dt_final_w
	and	c.IE_TIPO_ITEM			= ie_tipo_mensalidade_w
	and	d.ie_tipo_operacao		= ie_tipo_operacao_w
	and	d.ie_preco			= ie_preco_W
	and	d.ie_tipo_contratacao		<> 'I'
	and e.ie_status				= 2;
	
begin

delete	w_pls_relat_contab_mens
where	nm_usuario	= nm_usuario_p;

select	max(nr_sequencia)
into	nr_seq_OUTORGANTE_w
from	PLS_OUTORGANTE
where	cd_estabelecimento	= cd_estabelecimento_p;

dt_inicial_w	:= trunc(dt_inicial_p,'dd');
dt_final_w	:= fim_dia(dt_final_p);

open C04;
loop
fetch C04 into	
	ie_tipo_mensalidade_w;
exit when C04%notfound;
	begin
	
	nr_vetor_w	:= vetor_w.count+1;
	vetor_w(nr_vetor_w).ie_tipo_item	:= ie_tipo_mensalidade_w;
	
	end;
end loop;
close C04;

open C01;
loop
fetch C01 into	
	ie_tipo_operacao_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		ie_tipo_contrato_w;
	exit when C02%notfound;
		begin
		
		open C03;
		loop
		fetch C03 into	
			ie_preco_w;
		exit when C03%notfound;
			begin			
			i			:= 0;
			
			for i in 1..vetor_w.count loop
				ie_tipo_mensalidade_w	:= vetor_w(i).ie_tipo_item;
				
				vl_tot_item_w			:= 0;
				vl_tot_ato_cooperado_prin_w	:= 0;
				vl_tot_ato_cooperado_aux_w	:= 0;
				vl_tot_ato_nao_cooperado_w	:= 0;
				
				if	(ie_tipo_contrato_w = 'PF') then
					open C05;
					loop
					fetch C05 into	
						vl_item_w,
						vl_ato_cooperado_prin_w,
						vl_ato_cooperado_aux_w,
						vl_ato_nao_cooperado_w;
					exit when C05%notfound;
						begin
						
						vl_tot_item_w			:= vl_tot_item_w + vl_item_w;
						vl_tot_ato_cooperado_prin_w	:= vl_tot_ato_cooperado_prin_w +  vl_ato_cooperado_prin_w;
						vl_tot_ato_cooperado_aux_w	:= vl_tot_ato_cooperado_aux_w  + vl_ato_cooperado_aux_w;
						vl_tot_ato_nao_cooperado_w	:= vl_tot_ato_nao_cooperado_w + vl_ato_nao_cooperado_w;
						
						end;
					end loop;
					close C05;
				
				elsif	(ie_tipo_contrato_w = 'PJ') then
					open C06;
					loop
					fetch C06 into	
						vl_item_w,
						vl_ato_cooperado_prin_w,
						vl_ato_cooperado_aux_w,
						vl_ato_nao_cooperado_w;
					exit when C06%notfound;
						begin
						
						vl_tot_item_w			:= vl_tot_item_w + vl_item_w;
						vl_tot_ato_cooperado_prin_w	:= vl_tot_ato_cooperado_prin_w +  vl_ato_cooperado_prin_w;
						vl_tot_ato_cooperado_aux_w	:= vl_tot_ato_cooperado_aux_w  + vl_ato_cooperado_aux_w;
						vl_tot_ato_nao_cooperado_w	:= vl_tot_ato_nao_cooperado_w + vl_ato_nao_cooperado_w;
						
						end;
					end loop;
					close C06;
				end if;
				
				
				if	(nvl(vl_tot_item_w,0) <> 0) then
					vl_tot_outros_w	:= nvl(vl_tot_item_w,0) - nvl(vl_tot_ato_cooperado_prin_w,0) - nvl(vl_tot_ato_cooperado_aux_w,0) - nvl(vl_tot_ato_nao_cooperado_w,0);
					
					select	W_PLS_RELAT_CONTAB_MENS_seq.nextval
					into	nr_seq_relat_cont_w
					from	dual;
					
					insert into W_PLS_RELAT_CONTAB_MENS
						(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
							ie_tipo_item,ie_preco,ie_tipo_operacao,ie_tipo_contrato,vl_ato_cooperado_principal,
							vl_ato_cooperado_aux,vl_ato_nao_cooperado,vl_outros)
					values	(	nr_seq_relat_cont_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
							vetor_w(i).ie_tipo_item,ie_preco_W,ie_tipo_operacao_w,ie_tipo_contrato_w,nvl(vl_tot_ato_cooperado_prin_w,0),
							nvl(vl_tot_ato_cooperado_aux_w,0),nvl(vl_tot_ato_nao_cooperado_w,0),vl_tot_outros_w);
				end if;
			end loop;
			
			end;
		end loop;
		close C03;
		
		end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

commit;

end pls_gerar_relat_contab_mens;
/
