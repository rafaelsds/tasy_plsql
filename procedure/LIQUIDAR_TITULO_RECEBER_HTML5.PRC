create or replace
procedure liquidar_titulo_receber_html5(	nm_usuario_p		varchar2,
				nr_titulo_p		number,
				dt_liquidacao_p		date) is

vl_saldo_titulo_w	titulo_receber.vl_saldo_titulo%type;

begin

select	nvl(max(vl_saldo_titulo),0)
into	vl_saldo_titulo_w
from	titulo_receber
where	nr_titulo		= nr_titulo_p;

if	(vl_saldo_titulo_w <> 0) then
	wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(174075);
end if;


LIQUIDAR_TITULO_RECEBER(nm_usuario_p, nr_titulo_p, dt_liquidacao_p);

end liquidar_titulo_receber_html5;
/