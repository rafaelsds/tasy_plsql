create or replace
procedure pls_verif_reg_retorno_aud_orig
				(	nr_seq_requisicao_p		Number,
					nm_usuario_p			Varchar2,
					nr_seq_regra_retorno_p	out	Number) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar os itens da guia conforme a gera��o de ocorr~encia combinada.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_regra_w				Varchar2(255);
ie_tipo_item_w				Varchar2(2);
ie_status_w				Varchar2(2);
nr_seq_regra_ret_orig_w			Number(10)	:= 0;
nr_seq_req_proc_w			Number(10);
nr_seq_grupo_w				Number(10);
nr_seq_auditoria_w			Number(10);
nr_seq_ordem_w				Number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	ie_status		= 'S';

begin

open C01;
loop
fetch C01 into
	nr_seq_req_proc_w;
exit when C01%notfound;
	begin
	nr_seq_regra_ret_orig_w	:= pls_obter_reg_retorno_aud_orig(nr_seq_req_proc_w, null, null);
	
	if	(nr_seq_regra_ret_orig_w	> 0) then		
		update	pls_auditoria_item
		set	ie_status		= 'A',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_proc_origem	= nr_seq_req_proc_w;
		
		update	pls_requisicao_proc
		set	ie_status		= 'A',
			qt_procedimento		= 0,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_req_proc_w;
	end if;
	end;
end loop;
close C01;

if	(nr_seq_regra_ret_orig_w	> 0) then
	begin
		select	nr_seq_grupo_auditor,
			substr(ds_regra,1,255)
		into	nr_seq_grupo_w,
			ds_regra_w
		from	pls_retorno_aud_origem
		where	nr_sequencia	= nr_seq_regra_ret_orig_w;
	exception
	when others then
		nr_seq_grupo_w	:= null;
		ds_regra_w	:= null;
	end;

	if	(nr_seq_grupo_w	is not null) then
		select	max(nr_sequencia)
		into	nr_seq_auditoria_w
		from	pls_auditoria
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	dt_liberacao		is null;

		if	(nr_seq_auditoria_w	is not null) then
			update	pls_auditoria_grupo
			set	ie_status		= 'P',
				dt_liberacao		= sysdate,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_seq_auditoria	= nr_seq_auditoria_w
			and	dt_liberacao		is null;
			
			select	nvl(max(nr_seq_ordem),0) + 1
			into	nr_seq_ordem_w
			from	pls_auditoria_grupo
			where	nr_seq_auditoria	= nr_seq_auditoria_w;
			
			insert into pls_auditoria_grupo
				(nr_sequencia, nr_seq_auditoria, nr_seq_grupo,
				dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
				nm_usuario_nrec, nr_seq_ordem, ie_status,
				ie_manual)
			values	(pls_auditoria_grupo_seq.nextval, nr_seq_auditoria_w, nr_seq_grupo_w,
				sysdate, nm_usuario_p, sysdate,
				nm_usuario_p, nr_seq_ordem_w, 'U',
				'N');
		end if;
	end if;

	pls_requisicao_gravar_hist(nr_seq_requisicao_p,'L',substr('An�lise n�o finalizada devido a exist�ncia de regra de convers�o na fun��o OPS - Controle dos Grupos de An�lise > Regras > Autoriza��o > Regra retorno > Regra retorno auditoria origem benefic�rio > '||ds_regra_w,1,4000),null,nm_usuario_p);
end if;

nr_seq_regra_retorno_p	:= nr_seq_regra_ret_orig_w;

end pls_verif_reg_retorno_aud_orig;
/