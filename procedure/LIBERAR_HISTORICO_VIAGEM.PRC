create or replace
procedure liberar_historico_viagem( nr_sequencia_p	number) as

begin

if	(nr_sequencia_p > 0) then

	Update	via_viagem_hist  
	set	dt_liberacao = sysdate
       	where	nr_sequencia = nr_sequencia_p;
	commit;

end if;

end liberar_historico_viagem;
/
