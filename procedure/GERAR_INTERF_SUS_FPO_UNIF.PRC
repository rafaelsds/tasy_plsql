create or replace
procedure gerar_interf_sus_fpo_unif(	nr_seq_protocolo_p	number ,
				nm_usuario_p		varchar2 ) as

dt_competencia_w			date;
cd_cnes_w			varchar2(7);
cd_procedimento_w		varchar2(9);
ie_tipo_financiamento_w		varchar2(1);
ie_nivel_apuracao_w		varchar2(1);
qt_orcada_w			number(8);
vl_unitario_w			number(15,2);
vl_total_w			number(15,2);					

cursor c01 is
	select	a.dt_mesano_referencia dt_competencia,
		obter_cnes_estab_cnpj(obter_cgc_estabelecimento(a.cd_estabelecimento)) cd_cnes,
		substr(b.cd_procedimento,1,9) cd_procedimento,
		decode(obter_tipo_financ_proc(b.cd_procedimento,b.ie_origem_proced,1),'01',1,'04',3,2) ie_tipo_financiamento,
		sus_obter_nivel_apur_fpo(b.cd_procedimento,b.ie_origem_proced) ie_nivel_apuracao,
		sum(b.qt_procedimento) qt_orcada,
		(sum(b.vl_procedimento)/sum(b.qt_procedimento)) vl_unitario,
		sum(b.vl_procedimento) vl_total
	from	procedimento_paciente b,
		conta_paciente a
	where	a.nr_interno_conta = b.nr_interno_conta
	and	a.nr_seq_protocolo = nr_seq_protocolo_p
	group by	a.dt_mesano_referencia,
		obter_cnes_estab_cnpj(obter_cgc_estabelecimento(a.cd_estabelecimento)),
		substr(b.cd_procedimento,1,9),
		decode(obter_tipo_financ_proc(b.cd_procedimento,b.ie_origem_proced,1),'01',1,'04',3,2),
		sus_obter_nivel_apur_fpo(b.cd_procedimento,b.ie_origem_proced);
					
begin

delete from w_interf_sus_fpo_unif
where	nr_seq_protocolo = nr_seq_protocolo_p;

open c01;
loop
fetch c01 into	
	dt_competencia_w,
	cd_cnes_w,
	cd_procedimento_w,
	ie_tipo_financiamento_w,
	ie_nivel_apuracao_w,
	qt_orcada_w,
	vl_unitario_w,
	vl_total_w;
exit when c01%notfound;
	begin
	
	insert into w_interf_sus_fpo_unif(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_competencia,
			cd_cnes,
			cd_procedimento,
			ie_tipo_financiamento,
			ie_nivel_apuracao,
			qt_orcada,
			vl_unitario,
			vl_total,
			nr_seq_protocolo)
		values(	w_interf_sus_fpo_unif_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			dt_competencia_w,
			cd_cnes_w,
			cd_procedimento_w,
			ie_tipo_financiamento_w,
			ie_nivel_apuracao_w,
			qt_orcada_w,
			vl_unitario_w,
			vl_total_w,
			nr_seq_protocolo_p);
	
	end;
end loop;
close c01;

commit;

end gerar_interf_sus_fpo_unif;
/
