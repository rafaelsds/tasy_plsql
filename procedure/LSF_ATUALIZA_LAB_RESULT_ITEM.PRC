create or replace
procedure LSF_Atualiza_Lab_Result_Item (nr_prescricao_p		varchar2,
			       nr_seq_exame_p	varchar2,
			       cd_analito_p		varchar2,
			       ds_resultado_p		varchar2,
			       ds_unidade_medida_p 	varchar2,
			       ds_linha_resultado_p 	number,
			       ie_normalidade_p	varchar2,
			       nm_usuario_p		Varchar2,
			       ds_erro_p	out	varchar2) is 

nr_seq_prescricao_w 	number(10);				       
ds_resultado_w		varchar2(4000);
qt_resultado_w		number(15,4);
qt_resultado_ww		varchar2(4000);
ds_referencia_w 		varchar2(4000);
ds_resultado_ww		varchar2(4000);
cd_analito_w		varchar2(20);
				       
begin

-- Busca a sequ�ncia da prescri��o
select 	MAX(nr_sequencia)
into 	nr_seq_prescricao_w
from 	prescr_procedimento
where 	nr_prescricao = nr_prescricao_p
and   	nr_seq_exame = nr_seq_exame_p;

if 	(nr_seq_prescricao_w is null) then
	ds_erro_p := wheb_mensagem_pck.get_texto(279845,'NR_SEQ_EXAME='|| nr_seq_exame_p||';NR_PRESCRICAO='|| nr_prescricao_p);
end if;

-- Busca pelo c�digo do analito
select	max(nvl(e.cd_exame_integracao, e.cd_exame))
into	cd_analito_w
from	exame_laboratorio e
where	e.nr_seq_exame = (select nr_seq_exame 
			  from equipamento_lab b,
				lab_exame_equip a
			  WHERE	a.cd_equipamento = b.cd_equipamento
			  AND	a.cd_exame_equip = cd_analito_p
			  AND	upper(b.ds_sigla) = 'LSFRANCO'
			  AND	a.nr_seq_exame = nr_seq_exame_p);


if	(cd_analito_w is null) then

	select	max(nvl(e.cd_exame_integracao, e.cd_exame))
	into	cd_analito_w
	from	exame_laboratorio e
	where 	e.nr_seq_exame = (select nr_seq_exame 
				from equipamento_lab b,
				     lab_exame_equip a
				WHERE	a.cd_equipamento = b.cd_equipamento
				AND	a.cd_exame_equip = cd_analito_p
				AND	upper(b.ds_sigla) = 'LSFRANCO'
				AND	a.nr_seq_exame = e.nr_seq_exame)
	start with nr_seq_superior = nr_seq_exame_p
	connect by prior nr_seq_exame = nr_seq_superior;


end if;

-- Tratamento para inserir o resultado nos campos corretos dependento do formato do resultado
qt_resultado_ww  	:= trim(replace(ds_resultado_p, '.', ','));
ds_resultado_w		:= null;

begin
select 	to_number(nvl(qt_resultado_ww,0))
into 	qt_resultado_w
from 	dual;

exception
	when others then
	begin
	
	select to_number(nvl(replace(qt_resultado_ww, ',', '.'),0))
	into qt_resultado_w
	from dual;
	
	exception
	when others then	
		ds_resultado_w := ds_resultado_p;
	end;
end;

if (nr_seq_prescricao_w is not null) then
	begin
	--  Concatena o resultado com resultado j� existente quando a quantidade de linhas do resultado for maior que 1
	if 	(ds_linha_resultado_p > 1) then
		select  MAX(a.ds_resultado)
		into	ds_resultado_ww
		from 	exame_lab_result_item a,
			exame_lab_resultado b
		where 	a.nr_seq_resultado = b.nr_seq_resultado
		and	a.nr_seq_prescr    = nr_seq_prescricao_w
		and	b.nr_prescricao	   = nr_prescricao_p;
		
		ds_resultado_w := ds_resultado_ww || ds_resultado_w;
	end if;

		Atualizar_Lab_Result_Item(	nr_prescricao_p,
						nr_seq_prescricao_w,
						cd_analito_w,
						qt_resultado_w,
						null,
						ds_resultado_w,
						' ',
						null,
						null,
						nm_usuario_p,
						null,
						' ',
						ds_unidade_medida_p,
						null,
						null,
						ds_erro_p);
	end;
end if;
					
commit;

end LSF_Atualiza_Lab_Result_Item;
/