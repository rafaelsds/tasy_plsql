create or replace
procedure Qua_inserir_pessoa_doc_trein (
			nr_seq_treinamento_p		Number,
			cd_pessoa_fisica_p		Varchar2,
			ie_faltou_p			Varchar2,
			vl_nota_p			Number,
			ds_observacao_p			Varchar2,
			dt_confirmacao_p		Date,
			nm_usuario_confirmacao_p	Varchar2,
			cd_setor_atendimento_p		Number,
			nm_pessoa_externo_p		Varchar2,
			nm_usuario_p			Varchar2) is 

begin

insert	into qua_doc_trein_pessoa(
	nr_sequencia,
	nr_seq_treinamento,
	cd_pessoa_fisica,
	ie_faltou,
	vl_nota,
	ds_observacao,
	dt_confirmacao,
	nm_usuario_confirmacao,
	cd_setor_atendimento,
	nm_pessoa_externo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec)
values (qua_doc_trein_pessoa_seq.NextVal,
	nr_seq_treinamento_p,
	cd_pessoa_fisica_p,
	ie_faltou_p,
	vl_nota_p,
	ds_observacao_p,
	dt_confirmacao_p,
	nm_usuario_confirmacao_p,
	cd_setor_atendimento_p,
	nm_pessoa_externo_p,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p);

commit;

end Qua_inserir_pessoa_doc_trein;
/