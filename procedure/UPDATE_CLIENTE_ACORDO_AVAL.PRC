create or replace
procedure Update_Cliente_Acordo_Aval(
			nr_seq_ordem_p		number,
			nr_sequencia_p          number) is 

begin
update	com_cliente_acordo_aval 
set	nr_seq_ordem_serv = nr_seq_ordem_p 
where	nr_sequencia = nr_sequencia_p;
commit;

end Update_Cliente_Acordo_Aval;
/