CREATE OR REPLACE
PROCEDURE Atualiza_motivo_glosa(
				nr_seq_ret_item_p	Number,
				nm_usuario_p		Varchar2,
				ie_commit_p		varchar2) is

/*	Atualiza motivo da glosa conforme
	"Motivo Glosa", do DPB CONVENIO_RETORNO_ITEM */

cd_motivo_glosa_w	number(5);

begin

select 	a.cd_motivo_glosa
into	cd_motivo_glosa_w
from	convenio_retorno_item a
where	a.nr_sequencia		= nr_seq_ret_item_p;

if	(cd_motivo_glosa_w is not null) then

	update	convenio_retorno_glosa
	set	dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		cd_motivo_glosa	= cd_motivo_glosa_w
	where	nr_seq_ret_item	= nr_seq_ret_item_p
	and	cd_motivo_glosa is null;

end if;

commit;

end Atualiza_motivo_glosa;
/