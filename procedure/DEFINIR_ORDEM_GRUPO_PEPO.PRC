CREATE OR REPLACE
PROCEDURE definir_ordem_grupo_pepo(	nr_cirurgia_p		number,
									nr_seq_pepo_p      number,
									nm_usuario_p		varchar2) IS
					
nr_seq_item_grafico_w	number(10) := 0;
ie_modo_adm_w		varchar2(15);
/*
AA             |Agente anestésico
GP             |Ganhos e perdas
H              |Hemoderivados
M              |Medicamentos
MR             |Monitoração respiratória
SV             |Sinal vital
TH             |Terapia hidroeletrolitica
*/				
nr_seq_apresent_w 	number(10) := 0;
ie_ordem_tipo_w 		varchar2(15);
ie_ordem_modo_adm_w 	varchar2(15);
ie_grupo_selecionado_w 	varchar2(15);
ie_grupo_item_pepo_w 	varchar2(15);
nr_ordem_w		number(10) := null;
cd_estabelecimento_w	number(10) := wheb_usuario_pck.get_cd_estabelecimento;

cursor c01 is
	select	ie_grupo_item_pepo,
			ie_ordem_tipo,	
			ie_ordem_modo_adm,
			nvl(ie_grupo_selecionado,'S'),
			nr_seq_apresent * 100
	from 	pepo_grupo_item_grafico
	where	nm_usuario 	= nm_usuario_p
	order by nr_seq_apresent;

cursor c02 is --AA
	SELECT 	a.nr_sequencia,
			b.ie_modo_adm			
	FROM   	pepo_item_grafico a,
			cirurgia_agente_anestesico b, 
			agente_anestesico c  
	WHERE  	a.nr_seq_cirur_agente 	= b.nr_sequencia
	AND		b.nr_seq_agente			= c.nr_sequencia
	AND	(((NVL(nr_seq_pepo_p,0) > 0) AND (a.nr_seq_pepo = nr_seq_pepo_p))
	OR	((NVL(nr_cirurgia_p,0) > 0) AND (a.nr_cirurgia = nr_cirurgia_p)))
	AND		b.ie_tipo				= 1;

cursor c03 is --TH
	SELECT 	a.nr_sequencia,
			b.ie_modo_adm			
	FROM   	pepo_item_grafico a,
			cirurgia_agente_anestesico b, 
			agente_anestesico c  
	WHERE  	a.nr_seq_cirur_agente 	= b.nr_sequencia
	AND		b.nr_seq_agente			= c.nr_sequencia
	AND	(((NVL(nr_seq_pepo_p,0) > 0) AND (a.nr_seq_pepo = nr_seq_pepo_p))
	OR	((NVL(nr_cirurgia_p,0) > 0) AND (a.nr_cirurgia = nr_cirurgia_p)))
	AND		b.ie_tipo				= 2;
	
cursor c04 is --M	
	SELECT 	a.nr_sequencia,
			b.ie_modo_adm	
	FROM   	pepo_item_grafico a,
			cirurgia_agente_anestesico b, 
			agente_anestesico c  
	WHERE  	a.nr_seq_cirur_agente 	= b.nr_sequencia
	AND		b.nr_seq_agente			= c.nr_sequencia
	AND	(((NVL(nr_seq_pepo_p,0) > 0) AND (a.nr_seq_pepo = nr_seq_pepo_p))
	OR	((NVL(nr_cirurgia_p,0) > 0) AND (a.nr_cirurgia = nr_cirurgia_p)))
	AND		b.ie_tipo				= 3;

cursor C05 is --H	
	SELECT 	a.nr_sequencia,
			b.ie_modo_adm
	FROM   	pepo_item_grafico a,
			cirurgia_agente_anestesico b, 
			san_derivado c  
	WHERE  	a.nr_seq_cirur_agente 	= b.nr_sequencia
	AND		b.nr_seq_derivado		= c.nr_sequencia
	AND	(((NVL(nr_seq_pepo_p,0) > 0) AND (a.nr_seq_pepo = nr_seq_pepo_p))
	OR	((NVL(nr_cirurgia_p,0) > 0) AND (a.nr_cirurgia = nr_cirurgia_p)))
	AND		b.ie_tipo				= 5;
	
begin
open C01;
loop
fetch C01 into	
	ie_grupo_item_pepo_w,
	ie_ordem_tipo_w,
	ie_ordem_modo_adm_w,
	ie_grupo_selecionado_w,
	nr_seq_apresent_w;
exit when C01%notfound;
	begin
		if (ie_grupo_item_pepo_w = 'AA') then			
			open C02;
			loop
			fetch C02 into	
				nr_seq_item_grafico_w,
				ie_modo_adm_w;
			exit when C02%notfound;
			begin											
				update	pepo_item_grafico
				set	nr_seq_apresentacao 	= decode(ie_ordem_tipo_w,'GM',decode(ie_modo_adm_w, 'CO', nr_seq_apresent_w, nr_seq_apresent_w + 50),decode(ie_modo_adm_w, 'CO', (nr_seq_apresent_w/100) + 1, (nr_seq_apresent_w/100) + 10)),
				ie_grafico		= ie_grupo_selecionado_w
				where nr_sequencia	= nr_seq_item_grafico_w
				and	nm_usuario		= nm_usuario_p;
			end;
			end loop;
			close C02;					
		elsif (ie_grupo_item_pepo_w = 'TH') then
			open C03;
			loop
			fetch C03 into	
				nr_seq_item_grafico_w,
				ie_modo_adm_w;
			exit when C03%notfound;
			begin
				update	pepo_item_grafico
				set	nr_seq_apresentacao 	= decode(ie_ordem_tipo_w,'GM',decode(ie_modo_adm_w, 'CO', nr_seq_apresent_w, nr_seq_apresent_w + 50),decode(ie_modo_adm_w, 'CO', (nr_seq_apresent_w/100) + 1, (nr_seq_apresent_w/100) + 10)),
				ie_grafico		= ie_grupo_selecionado_w
				where nr_sequencia	= nr_seq_item_grafico_w
				and	nm_usuario		= nm_usuario_p;
			end;
			end loop;
			close C03;
		elsif (ie_grupo_item_pepo_w = 'M') then
			open C04;
			loop
			fetch C04 into	
				nr_seq_item_grafico_w,
				ie_modo_adm_w;
			exit when C04%notfound;
			begin
				update	pepo_item_grafico
				set	nr_seq_apresentacao 	= decode(ie_ordem_tipo_w,'GM',decode(ie_modo_adm_w, 'CO', nr_seq_apresent_w, nr_seq_apresent_w + 50),decode(ie_modo_adm_w, 'CO', (nr_seq_apresent_w/100) + 1, (nr_seq_apresent_w/100) + 10)),
				ie_grafico		= ie_grupo_selecionado_w
				where nr_sequencia	= nr_seq_item_grafico_w
				and	nm_usuario		= nm_usuario_p;
			end;
			end loop;
			close C04;
		elsif (ie_grupo_item_pepo_w = 'H') then
			open C05;
			loop
			fetch C05 into	
				nr_seq_item_grafico_w,
				ie_modo_adm_w;
			exit when C05%notfound;
			begin
				update	pepo_item_grafico
				set	nr_seq_apresentacao 	= decode(ie_ordem_tipo_w,'GM',decode(ie_modo_adm_w, 'CO', nr_seq_apresent_w, nr_seq_apresent_w + 50),decode(ie_modo_adm_w, 'CO', (nr_seq_apresent_w/100) + 1, (nr_seq_apresent_w/100) + 10)),
				ie_grafico		= ie_grupo_selecionado_w
				where nr_sequencia	= nr_seq_item_grafico_w
				and	nm_usuario		= nm_usuario_p;
			end;
			end loop;
			close C05;
		elsif (ie_grupo_item_pepo_w = 'SV') then
			update	pepo_item_grafico a
			set		a.nr_seq_apresentacao 	= decode(ie_ordem_tipo_w,'GM',nr_seq_apresent_w, nr_seq_apresent_w + 100),
					a.ie_grafico		= ie_grupo_selecionado_w
			where   exists (select 1 
							from pepo_sinal_vital b 
							where a.nr_seq_sinal_vital = b.nr_sequencia 
							and b.cd_estabelecimento = cd_estabelecimento_w
							and nvl(b.ie_situacao,'A') 	= 'A'
							and b.nr_sequencia  not in (select nr_sequencia 
									         		    from pepo_sinal_vital
									         		     where ((ie_sinal_vital = 'E') or
															   (ie_sinal_vital = 'HI') or
															   (ie_sinal_vital = 'HE') or
															   (ie_sinal_vital = 'CAM') or
															   (ie_sinal_vital = 'QTVCI') or
															   (ie_sinal_vital = 'QTFREQVENT') or
															   (ie_sinal_vital = 'QTPIP') or
															   (ie_sinal_vital = 'QTFIO2') or
															   (ie_sinal_vital = 'QTOXIGINS') or
															   (ie_sinal_vital = 'QTNITROSO') or
									         				   (ie_sinal_vital = 'PEEP'))))
			and		a.nm_usuario		= nm_usuario_p;
		elsif (ie_grupo_item_pepo_w = 'MR') then										
			update	pepo_item_grafico a
			set		a.nr_seq_apresentacao 	= decode(ie_ordem_tipo_w,'GM',nr_seq_apresent_w, nr_seq_apresent_w + 100),
					a.ie_grafico		= ie_grupo_selecionado_w
			where   exists (select 1 
							from pepo_sinal_vital b 
							where a.nr_seq_sinal_vital 	= b.nr_sequencia
							and b.cd_estabelecimento 	= cd_estabelecimento_w		
							and nvl(b.ie_situacao,'A') 	= 'A'
							and b.nr_sequencia in (select nr_sequencia 
												   from pepo_sinal_vital
												   where ((ie_sinal_vital = 'E') or
														  (ie_sinal_vital = 'HI') or
														  (ie_sinal_vital = 'HE') or
														  (ie_sinal_vital = 'CAM') or
														  (ie_sinal_vital = 'QTVCI') or
														  (ie_sinal_vital = 'QTFREQVENT') or
														  (ie_sinal_vital = 'QTPIP') or
														  (ie_sinal_vital = 'QTFIO2') or
														  (ie_sinal_vital = 'QTOXIGINS') or
														  (ie_sinal_vital = 'QTNITROSO') or
														  (ie_sinal_vital = 'PEEP'))))
			and		a.nm_usuario		= nm_usuario_p;	
		elsif (ie_grupo_item_pepo_w = 'GP') then
			update	pepo_item_grafico
			set		nr_seq_apresentacao 	= decode(ie_ordem_tipo_w,'GM',nr_seq_apresent_w, nr_seq_apresent_w + 100),
					ie_grafico		= ie_grupo_selecionado_w
			where	nr_seq_tipo 		is not null
			and		nm_usuario		= nm_usuario_p;
		end if;								
	end;
end loop;
close C01;

commit;

end definir_ordem_grupo_pepo;
/
