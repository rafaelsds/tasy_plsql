create or replace
procedure excluir_administracao_agente(	nr_seq_agente_p	number)
					is

begin

delete 	from cirurgia_agente_anest_ocor
where 	nr_seq_cirur_agente 	= nr_seq_agente_p
and	nvl(ie_situacao,'A')	= 'A'
and	dt_liberacao is null;

commit;

end excluir_administracao_agente;
/