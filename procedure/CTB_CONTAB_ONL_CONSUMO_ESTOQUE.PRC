create or replace
procedure ctb_contab_onl_consumo_estoque(   nm_usuario_p    varchar2,
                                            cd_estabelecimento_p    number,
                                            dt_referencia_p         date ) is
/*
===============================================================================
Purpose: Indentacao

Remarks: n/a

Who         When            What
----------  -----------     --------------------------------------------------
chjreinert  13/mai/2021     OS 2444975 - Indentacao

===============================================================================
*/

ie_centro_custo_w               conta_contabil.ie_centro_custo%type;
cd_classe_material_w            estrutura_material_v.cd_classe_material%type;
cd_conta_contabil_w             conta_contabil.cd_conta_contabil%type;
cd_conta_debito_w               conta_contabil.cd_conta_contabil%type;
cd_conta_receita_w              conta_contabil.cd_conta_contabil%type;
cd_empresa_w                    empresa.cd_empresa%type;
cd_estab_movto_w                estabelecimento.cd_estabelecimento%type;
cd_grupo_material_w             estrutura_material_v.cd_grupo_material%type;
cd_historico_debito_w           historico_padrao.cd_historico%type;
cd_subgrupo_material_w          estrutura_material_v.cd_subgrupo_material%type;
cd_tipo_lote_contabil_w         tipo_lote_contabil.cd_tipo_lote_contabil%type:= 3;
ctb_movimento_doc_w             ctb_online_pck.ctb_movimento_doc;
ctb_movimento_w                 ctb_movimento%rowtype;
ctb_param_lote_consumo_w        ctb_param_lote_consumo%rowtype;
ds_classe_material_w            estrutura_material_v.ds_classe_material%type;
ds_grupo_material_w             estrutura_material_v.ds_grupo_material%type;
ds_material_w                   material.ds_material%type;
nr_lote_contabil_w              lote_contabil.nr_lote_contabil%type;
nr_seq_agrupamento_w            w_movimento_contabil.nr_seq_agrupamento%type;
cd_unidade_medida_consumo_w     varchar2(30);
ds_compl_historico_w            varchar2(255)   := '';
ds_erro_w                       varchar2(255) := null;
ds_mes_ano_w                    varchar2(10) := to_char(dt_referencia_p,'MMYYYY');
ds_mesano_referencia_w          varchar2(30);
ds_subgrupo_material_w          varchar2(240);
nm_agrupador_w                  varchar2(255);
nm_estabelecimento_w            varchar2(255);
dt_atualizacao_saldo_w          date;
dt_mesano_referencia_w          date    := trunc(dt_referencia_p,'mm');
dt_movimento_w                  date;
qt_registro_w                   number(10);
sequencia_w                     dbms_sql.number_table;
cd_sequencia_parametro_w        number(10);

/* Cursor para ler os Movimentos a serem contabilizados */
cursor c_dados is
    select  cd_material,
            cd_centro_custo,
            a.cd_local_estoque,
            a.cd_operacao_estoque,
            b.ds_operacao ds_operacao_estoque,
            sum(decode(b.ie_entrada_saida,'S', vl_estoque, vl_estoque * -1)) vl_movimento,
            '' ds_local_destino,
            '' ie_debito_credito,
            0 cd_hist_transf,
            substr(obter_desc_local_estoque(a.cd_local_estoque),1,80) ds_local_consumo,
            nvl(sum(a.qt_consumo),0) qt_consumo
    from    Operacao_estoque b,
            resumo_movto_estoque a
    where   a.dt_mesano_referencia          = dt_mesano_referencia_w
    and     a.cd_estabelecimento            = cd_estabelecimento_p
    and     a.cd_centro_custo               is not null
    and     (nvl(a.ie_consignado,'N')       = 'N' or ctb_param_lote_consumo_w.ie_contab_consignado  = 'S')
    and     a.cd_operacao_estoque   = b.cd_operacao_estoque
    group by
            cd_material,
            cd_centro_custo,
            a.cd_local_estoque,
            a.cd_operacao_estoque,
            b.ds_operacao,
            substr(obter_desc_local_estoque(a.cd_local_estoque),1,80)
    union all
    select  cd_material,
            cd_centro_custo,
            a.cd_local_estoque,
            a.cd_operacao_estoque,
            b.ds_operacao ds_operacao_estoque,
            sum(vl_estoque) vl_movimento,
            substr(obter_desc_local_estoque(cd_local_destino),1,80) ds_local_destino,
            decode(b.ie_entrada_saida,'S','C','D'),
            decode(b.ie_entrada_saida,'S', ctb_param_lote_consumo_w.cd_hist_transf_saida, ctb_param_lote_consumo_w.cd_hist_transf_entrada),
            '' ds_local_consumo,
            nvl(sum(a.qt_consumo),0) qt_consumo
    from    Operacao_estoque b,
            resumo_movto_estoque a
    where   a.dt_mesano_referencia  = dt_mesano_referencia_w
    and     a.cd_estabelecimento            = cd_estabelecimento_p
    and     a.cd_local_destino              is not null
    and     (nvl(a.ie_consignado,'N')       = 'N' or ctb_param_lote_consumo_w.ie_contab_consignado  = 'S')
    and     a.cd_operacao_estoque   = b.cd_operacao_estoque
    and     ctb_param_lote_consumo_w.cd_hist_transf_entrada is not null
    and     ctb_param_lote_consumo_w.cd_hist_transf_saida   is not null
    and     b.ie_tipo_requisicao    = '2'
    group by
            cd_material,
            cd_centro_custo,
            a.cd_local_estoque,
            a.cd_operacao_estoque,
            b.ds_operacao,
            substr(obter_desc_local_estoque(cd_local_destino),1,80),
            b.ie_entrada_saida;

dados_w c_dados%RowType;

procedure get_sequence is

begin
ctb_movimento_doc_w.nr_sequencia        := null;
if  (sequencia_w.count > 0) then
    ctb_movimento_doc_w.nr_sequencia        := sequencia_w(sequencia_w.count);
    sequencia_w.delete(sequencia_w.count);
end if;
exception when others then
    ctb_movimento_doc_w.nr_sequencia        := null;
end get_sequence;

Begin
cd_sequencia_parametro_w := null;
/* Busca LOTE CONTABIL  */
nr_lote_contabil_w := ctb_online_pck.get_lote_contabil( cd_tipo_lote_contabil_w,
                                                        cd_estabelecimento_p,
                                                        dt_referencia_p,
                                                        nm_usuario_p);

select  count(nr_sequencia)
into    qt_registro_w
from    ctb_movimento
where   nr_lote_contabil        = nr_lote_contabil_w;

select  dt_atualizacao_saldo
into    dt_atualizacao_saldo_w
from    lote_contabil
where   nr_lote_contabil        = nr_lote_contabil_w;

if      (dt_atualizacao_saldo_w is not null) then
        ctb_desatualizar_lote(nr_lote_contabil_w, nm_usuario_p, ds_erro_w);
end if;

if  (qt_registro_w > 0) then
    begin
    qt_registro_w   := sequencia_w.count;

    for vet in (select a.nr_sequencia
                from    ctb_movimento a
                where   a.nr_lote_contabil  = nr_lote_contabil_w
                order by 1 desc) loop
            begin
            qt_registro_w   := qt_registro_w + 1;
            sequencia_w(qt_registro_w)  := vet.nr_sequencia;
            end;
    end loop;

    delete  ctb_movimento
    where   nr_lote_contabil    = nr_lote_contabil_w;

    commit;
    end;
end if;

select  max(nm_fantasia_estab)
into    nm_estabelecimento_w
from    estabelecimento
where   cd_estabelecimento  = cd_estabelecimento_p;

cd_empresa_w      := obter_empresa_estab(cd_estabelecimento_p);

/* Parametros Lote Consumo */
select  a.*
into    ctb_param_lote_consumo_w
from    ctb_param_lote_consumo a
where   a.cd_empresa    = cd_empresa_w
and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_p) = cd_estabelecimento_p;

dt_movimento_w          := last_day(trunc(dt_mesano_referencia_w,'dd'));
ds_mesano_referencia_w  := substr(obter_desc_mes_ano(dt_mesano_referencia_w, ctb_param_lote_consumo_w.ie_mes_contab),1,30);
nm_agrupador_w          := nvl(trim(obter_agrupador_contabil(3)),'DS_MES_ANO');

open c_dados;
loop
fetch c_dados into
        dados_w;
exit when c_dados%notfound;

ds_compl_historico_w    := ds_mesano_referencia_w;

begin
select  cd_estabelecimento
into    cd_estab_movto_w
from    centro_custo
where   cd_centro_custo = dados_w.cd_centro_custo;
exception when others then
        cd_estab_movto_w        := cd_estabelecimento_p;
end;

begin
select  cd_grupo_material,
        cd_subgrupo_material,
        cd_classe_material,
        ds_grupo_material,
        ds_subgrupo_material,
        ds_classe_material,
        ds_material
into    cd_grupo_material_w,
        cd_subgrupo_material_w,
        cd_classe_material_w,
        ds_grupo_material_w,
        ds_subgrupo_material_w,
        ds_classe_material_w,
        ds_material_w
from    estrutura_material_v
where   cd_material     = dados_w.cd_material;

cd_unidade_medida_consumo_w     := nvl(substr(obter_dados_material(dados_w.cd_material, 'UMC'),1,30),'');
exception when others then
        cd_grupo_material_w     := null;
        cd_subgrupo_material_w  := null;
        cd_classe_material_w    := null;
        ds_grupo_material_w     := null;
        ds_subgrupo_material_w  := null;
        ds_classe_material_w    := null;
end;

if      (nm_agrupador_w = 'CD_CLASSE_MATERIAL') then
        nr_seq_agrupamento_w    := cd_classe_material_w;
elsif   (nm_agrupador_w = 'CD_SUBGRUPO_MATERIAL') then
        nr_seq_agrupamento_w    := cd_subgrupo_material_w;
elsif   (nm_agrupador_w = 'CD_GRUPO_MATERIAL') then
        nr_seq_agrupamento_w    := cd_grupo_material_w;
elsif   (nm_agrupador_w = 'CD_OPERACAO_ESTOQUE')         then
        nr_seq_agrupamento_w :=  dados_w.cd_operacao_estoque;
elsif   (nm_agrupador_w = 'DS_MES_ANO')  then
        nr_seq_agrupamento_w :=  somente_numero(substr(ds_mes_ano_w,1,8));
elsif   (nm_agrupador_w = 'CD_MATERIAL')         then
        nr_seq_agrupamento_w :=  dados_w.cd_material;
end if;

if (nvl(nr_seq_agrupamento_w,0) = 0)then
        nr_seq_agrupamento_w := ds_mes_ano_w;
end if;

/* Atributos Historico */
ctb_online_pck.definir_atrib_compl(cd_tipo_lote_contabil_w);
ctb_online_pck.set_value_compl_hist('DT_MESANO_REFERENCIA',dt_mesano_referencia_w);
ctb_online_pck.set_value_compl_hist('DS_LOCAL_ESTOQUE', dados_w.ds_local_destino);
ctb_online_pck.set_value_compl_hist('DS_MESANO_REFERENCIA', ds_mesano_referencia_w);
ctb_online_pck.set_value_compl_hist('DS_MATERIAL', ds_material_w);
ctb_online_pck.set_value_compl_hist('CD_OPERACAO_ESTOQUE', dados_w.cd_operacao_estoque);
ctb_online_pck.set_value_compl_hist('DS_LOCAL_CONSUMO', dados_w.ds_local_consumo);
ctb_online_pck.set_value_compl_hist('DS_OPERACAO_ESTOQUE', dados_w.ds_operacao_estoque);
ctb_online_pck.set_value_compl_hist('NM_ESTABELECIMENTO', nm_estabelecimento_w);
ctb_online_pck.set_value_compl_hist('DS_GRUPO_MATERIAL', ds_grupo_material_w);
ctb_online_pck.set_value_compl_hist('DS_SUBGRUPO_MATERIAL', ds_subgrupo_material_w);
ctb_online_pck.set_value_compl_hist('DS_CLASSE_MATERIAL', ds_classe_material_w);
ctb_online_pck.set_value_compl_hist('CD_UNIDADE_MEDIDA_CONSUMO', cd_unidade_medida_consumo_w);
ctb_online_pck.set_value_compl_hist('QT_CONSUMO', dados_w.qt_consumo);
ctb_online_pck.set_value_compl_hist('NR_MOVIMENTO_CONTABIL', null); /*Este complemento n?o ? mais utilizado */
ctb_online_pck.set_value_compl_hist('CD_MATERIAL', dados_w.cd_material);

/* Obtem Historico */
ds_compl_historico_w := ctb_online_pck.ctb_obter_compl_Historico( cd_tipo_lote_contabil_w , ctb_param_lote_consumo_w.cd_historico);

cd_conta_debito_w       := nvl(obter_conta_hist_oper_estoque(cd_empresa_w,dados_w.cd_operacao_estoque,'C'),'');
cd_historico_debito_w   := nvl(obter_conta_hist_oper_estoque(cd_empresa_w,dados_w.cd_operacao_estoque,'H'),'');

if (dados_w.cd_centro_custo is not null) then
    begin
    define_conta_material(
        cd_estabelecimento_p,
        dados_w.cd_material,
        2, null, 0, '0', 0, null, null, null,
        dados_w.cd_local_estoque,
        dados_w.cd_operacao_estoque,
        dt_movimento_w,
        cd_conta_contabil_w,
        dados_w.cd_centro_custo,
        null);
    cd_sequencia_parametro_w := philips_contabil_pck.get_parametro_conta_contabil();

    /* CREDITO      */
    get_sequence;
    ctb_movimento_doc_w := null;
    ctb_movimento_doc_w.nr_lote_contabil            := nr_lote_contabil_w;
    ctb_movimento_doc_w.cd_tipo_lote_contabil       := cd_tipo_lote_contabil_w;
    ctb_movimento_doc_w.cd_estabelecimento          := cd_estabelecimento_p;
    ctb_movimento_doc_w.dt_movimento                := dt_movimento_w;
    ctb_movimento_doc_w.vl_movimento                := dados_w.vl_movimento;
    ctb_movimento_doc_w.cd_conta_credito            := cd_conta_contabil_w;
    ctb_movimento_doc_w.cd_historico                := ctb_param_lote_consumo_w.cd_historico;
    ctb_movimento_doc_w.nr_seq_agrupamento          := nr_seq_agrupamento_w;
    ctb_movimento_doc_w.ds_compl_historico          := ds_compl_historico_w;
    ctb_movimento_doc_w.ie_transitorio              := 'N';
    ctb_movimento_doc_w.cd_centro_custo             := null;
    ctb_movimento_doc_w.cd_sequencia_parametro      := cd_sequencia_parametro_w;

    /* Grava Movimentos Contabeis                   */
    ctb_online_pck.ctb_gravar_movto( ctb_movimento_doc_w, nm_usuario_p,'S');

    cd_conta_contabil_w     := null;
    define_conta_material(
            cd_estabelecimento_p,
            dados_w.cd_material,
            3, null, 0, '0',0, null, null, null,
            dados_w.cd_local_estoque,
            dados_w.cd_operacao_estoque,
            dt_movimento_w,
            cd_conta_contabil_w,
            dados_w.cd_centro_custo,
            null);

    cd_sequencia_parametro_w := philips_contabil_pck.get_parametro_conta_contabil();
    cd_conta_contabil_w     := nvl(cd_conta_debito_w,cd_conta_contabil_w);

    begin
    select  ie_centro_custo
    into    ie_centro_custo_w
    from    conta_contabil
    where   cd_conta_contabil       = cd_conta_contabil_w;
    exception when others then
            ie_centro_custo_w       := 'N';
    end;

    /* DEBITO */
    get_sequence;
    ctb_movimento_doc_w := null;
    ctb_movimento_doc_w.nr_lote_contabil            := nr_lote_contabil_w;
    ctb_movimento_doc_w.cd_tipo_lote_contabil       := cd_tipo_lote_contabil_w;
    ctb_movimento_doc_w.cd_estabelecimento          := cd_estab_movto_w;
    ctb_movimento_doc_w.dt_movimento                := dt_movimento_w;
    ctb_movimento_doc_w.vl_movimento                := dados_w.vl_movimento;
    ctb_movimento_doc_w.cd_conta_debito             := cd_conta_contabil_w;
    ctb_movimento_doc_w.cd_historico                := nvl(cd_historico_debito_w, ctb_param_lote_consumo_w.cd_historico);
    ctb_movimento_doc_w.nr_seq_agrupamento          := nr_seq_agrupamento_w;
    ctb_movimento_doc_w.ds_compl_historico          := ds_compl_historico_w;
    ctb_movimento_doc_w.ie_transitorio              := 'N';
    ctb_movimento_doc_w.cd_sequencia_parametro      := cd_sequencia_parametro_w;

    if (ie_centro_custo_w = 'S') then
            ctb_movimento_doc_w.cd_centro_custo     := dados_w.cd_centro_custo;
    end if;

    /* Grava Movimentos Contabeis                   */
    ctb_online_pck.ctb_gravar_movto( ctb_movimento_doc_w,
                                                    nm_usuario_p,
                                                    'S');
    define_conta_material(
            cd_estabelecimento_p,
            dados_w.cd_material,
            9, null, 0, '0',0, null, null, null,
    dados_w.cd_local_estoque,
            dados_w.cd_operacao_estoque,
            dt_movimento_w,
            cd_conta_contabil_w,
            dados_w.cd_centro_custo,
            null);

    if (nvl(cd_conta_contabil_w,'0') <> '0') then

        define_conta_material(
                cd_estabelecimento_p,
                dados_w.cd_material,
                3, null, 0, '0',0, null, null, null,
                dados_w.cd_local_estoque,
                dados_w.cd_operacao_estoque,
                dt_movimento_w,
                cd_conta_receita_w,
                dados_w.cd_centro_custo,
                null);
        cd_sequencia_parametro_w := philips_contabil_pck.get_parametro_conta_contabil();

            if (nvl(cd_conta_receita_w,'0') <> '0') then

                begin
                select  ie_centro_custo
                into    ie_centro_custo_w
                from    conta_contabil
                where   cd_conta_contabil       = cd_conta_contabil_w;
                exception when others then
                        ie_centro_custo_w       := 'N';
                end;

                /* DEBITO */
                get_sequence;
                ctb_movimento_doc_w := null;
                ctb_movimento_doc_w.nr_lote_contabil            := nr_lote_contabil_w;
                ctb_movimento_doc_w.cd_tipo_lote_contabil       := cd_tipo_lote_contabil_w;
                ctb_movimento_doc_w.cd_estabelecimento          := cd_estab_movto_w;
                ctb_movimento_doc_w.dt_movimento                := dt_movimento_w;
                ctb_movimento_doc_w.vl_movimento                := dados_w.vl_movimento;
                ctb_movimento_doc_w.cd_conta_debito             := cd_conta_contabil_w;
                ctb_movimento_doc_w.cd_historico                := nvl(cd_historico_debito_w, ctb_param_lote_consumo_w.cd_historico);
                ctb_movimento_doc_w.nr_seq_agrupamento          := nr_seq_agrupamento_w;
                ctb_movimento_doc_w.ds_compl_historico          := ds_compl_historico_w;
                ctb_movimento_doc_w.ie_transitorio              := 'N';
                ctb_movimento_doc_w.cd_sequencia_parametro      := cd_sequencia_parametro_w;

                if (ie_centro_custo_w = 'S') then
                        ctb_movimento_doc_w.cd_centro_custo     := dados_w.cd_centro_custo;
                end if;

                /* Grava Movimentos Contabeis                   */
                ctb_online_pck.ctb_gravar_movto( ctb_movimento_doc_w,
                                        nm_usuario_p,
                                        'S');
                begin
                select  ie_centro_custo
                into    ie_centro_custo_w
                from    conta_contabil
                where   cd_conta_contabil       = cd_conta_receita_w;
                exception when others then
                        ie_centro_custo_w       := 'N';
                end;

                /* CREDITO */
                get_sequence;
                ctb_movimento_doc_w := null;
                ctb_movimento_doc_w.nr_lote_contabil            := nr_lote_contabil_w;
                ctb_movimento_doc_w.cd_tipo_lote_contabil       := cd_tipo_lote_contabil_w;
                ctb_movimento_doc_w.cd_estabelecimento          := cd_estabelecimento_p;
                ctb_movimento_doc_w.dt_movimento                := dt_movimento_w;
                ctb_movimento_doc_w.vl_movimento                := dados_w.vl_movimento;
                ctb_movimento_doc_w.cd_conta_credito            := cd_conta_receita_w;
                ctb_movimento_doc_w.cd_historico                := ctb_param_lote_consumo_w.cd_historico;
                ctb_movimento_doc_w.nr_seq_agrupamento          := nr_seq_agrupamento_w;
                ctb_movimento_doc_w.ds_compl_historico          := ds_compl_historico_w;
                ctb_movimento_doc_w.ie_transitorio              := 'N';
                if      (ie_centro_custo_w = 'S') then
                        ctb_movimento_doc_w.cd_centro_custo     := dados_w.cd_centro_custo;
                end if;

                /* Grava Movimentos Contabeis                   */
                ctb_online_pck.ctb_gravar_movto( ctb_movimento_doc_w, nm_usuario_p,'S');
            end if;
    end if;
    end;
else
    begin
    cd_conta_contabil_w     := null;
    define_conta_material(
            cd_estabelecimento_p,
            dados_w.cd_material,
            2, null, 0, '0',0, null, null, null,
            dados_w.cd_local_estoque,
            dados_w.cd_operacao_estoque,
            dt_movimento_w,
            cd_conta_contabil_w,
            dados_w.cd_centro_custo,
            null);
    cd_sequencia_parametro_w := philips_contabil_pck.get_parametro_conta_contabil();

        if (dados_w.ie_debito_credito = 'D') then
                cd_conta_contabil_w     := nvl(cd_conta_debito_w,cd_conta_contabil_w);
                dados_w.cd_hist_transf  := nvl(cd_historico_debito_w,dados_w.cd_hist_transf);
        end if;
        begin
        select  ie_centro_custo
        into    ie_centro_custo_w
        from    conta_contabil
        where   cd_conta_contabil       = cd_conta_contabil_w;
        exception when others then
                ie_centro_custo_w       := 'N';
        end;
        /* Carrega CTB_MOVIMENTO                        */
        get_sequence;
        ctb_movimento_doc_w := null;
        ctb_movimento_doc_w.nr_lote_contabil            := nr_lote_contabil_w;
        ctb_movimento_doc_w.cd_tipo_lote_contabil       := cd_tipo_lote_contabil_w;
        ctb_movimento_doc_w.dt_movimento                := dt_movimento_w;
        ctb_movimento_doc_w.vl_movimento                := dados_w.vl_movimento;
        ctb_movimento_doc_w.cd_conta_debito             := cd_conta_contabil_w;
        ctb_movimento_doc_w.cd_historico                := dados_w.cd_hist_transf;
        ctb_movimento_doc_w.nr_seq_agrupamento          := nr_seq_agrupamento_w;
        ctb_movimento_doc_w.ds_compl_historico          := ds_compl_historico_w;
        ctb_movimento_doc_w.ie_transitorio              := 'N';
        ctb_movimento_doc_w.cd_sequencia_parametro      := cd_sequencia_parametro_w;

        if (ie_centro_custo_w = 'S') then
                ctb_movimento_doc_w.cd_centro_custo     := dados_w.cd_centro_custo;
        end if;

        /* Grava Movimentos Contabeis*/
        ctb_online_pck.ctb_gravar_movto( ctb_movimento_doc_w,
                                nm_usuario_p,
                                'S');
        end;
    end if;
end loop;
close c_dados;

/* Atualizar LOTE CONTABIL */
update  lote_contabil
set     ie_situacao      = 'A',
        dt_geracao_lote  = sysdate,
        dt_integracao    = sysdate
where   nr_lote_contabil = nr_lote_contabil_w;

/* Atualiza SALDO Movimento */
begin
ctb_atualizar_saldo(  nr_lote_contabil_w,
                'S',
                nm_usuario_p,
                'N',
                ds_erro_w,
                'S');
exception when others then
        null;
end;
commit;
end ctb_contab_onl_consumo_estoque;
/