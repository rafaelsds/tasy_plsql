create or replace
procedure HD_suspender_item_dialise(nr_seq_horario_p	number,
			nm_usuario_p		Varchar2) is 

begin

update	prescr_mat_hor
set	dt_suspensao	= sysdate,
	nm_usuario_adm	= nm_usuario_p
where	nr_sequencia	= nr_seq_horario_p
and	dt_fim_horario	is null
and	dt_suspensao	is null;
commit;

end HD_suspender_item_dialise;
/