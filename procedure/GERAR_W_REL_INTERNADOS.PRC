create or replace
procedure  gerar_w_rel_internados(dt_inical_p	date,
				dt_final_p      date,
				ie_radio_p	NUMBER) IS

nm_paciente_w  		varchar2(255);
ds_convenio_w		varchar2(255);
ds_pagador_w		varchar2(255);
cd_unidade_w		varchar2(50);
dt_entrada_w		varchar2(10);
nr_dias_internado_w	number(15);
nr_idade_w		number(15);
nm_medico_w		varchar2(255);
dt_inclusao_w		varchar2(10);
dt_alta_w		date;
nr_atendimento_w	number(15);
ds_carater_w		varchar2(255);
dt_contratacao_w	date;
cd_convenio_w		number(5);



CURSOR C01 IS
	SELECT 	a.nm_paciente,
		a.ds_convenio,
		SUBSTR(obter_estipulante_atend(a.cd_pessoa_fisica,'CD'),1,255) ds_pagador,
		a.cd_unidade,
		TO_CHAR(a.dt_entrada,'dd/mm/yy') dt_entrada,
		(TRUNC(TRUNC(NVL(a.dt_alta, SYSDATE)) - TRUNC(a.dt_entrada))) nr_dias_internado,
		TRUNC((SYSDATE - a.dt_nascimento) /365, 0) nr_idade,
		a.nm_medico,
		TO_CHAR(TO_DATE(obter_data_inclusao(a.cd_pessoa_fisica),'dd/mm/yyyy'),'dd/mm/yy') dt_inclusao,
		a.dt_alta,
		a.nr_atendimento,
		SUBSTR(Sus_Obter_Desc_Carater_Int(obter_carater_atend(a.nr_atendimento)),1,255) ds_carater,
		to_date(obter_estipulante_atend(a.cd_pessoa_fisica,'DT'),'dd/mm/yy') dt_contratacao,
		a.cd_convenio
	FROM 	paciente_internado_v2 a
	WHERE 	((a.dt_alta BETWEEN dt_inical_p AND fim_dia(dt_final_p))
	OR	(a.dt_entrada_unidade BETWEEN dt_inical_p AND fim_dia(dt_final_p)))
	AND	Obter_Atepacu_paciente(a.nr_atendimento,'A') = a.NR_SEQ_ATEPACU
	AND	 ie_radio_p = 1
	ORDER BY  a.nm_paciente;

CURSOR C02 IS
	SELECT 	a.nm_paciente,
		a.ds_convenio,
		SUBSTR(obter_estipulante_atend(a.cd_pessoa_fisica,'CD'),1,255) ds_pagador,
		a.cd_unidade,
		TO_CHAR(a.dt_entrada,'dd/mm/yy') dt_entrada,
		(TRUNC(TRUNC(NVL(a.dt_alta, SYSDATE)) - TRUNC(a.dt_entrada))) nr_dias_internado,
		TRUNC((SYSDATE - a.dt_nascimento) /365, 0) nr_idade,
		a.nm_medico,
		TO_CHAR(TO_DATE(obter_data_inclusao(a.cd_pessoa_fisica),'dd/mm/yyyy'),'dd/mm/yy') dt_inclusao,
		a.dt_alta,
		a.nr_atendimento,
		SUBSTR(Sus_Obter_Desc_Carater_Int(obter_carater_atend(a.nr_atendimento)),1,255) ds_carater,
		to_date(obter_estipulante_atend(a.cd_pessoa_fisica,'DT'),'dd/mm/yy') dt_contratacao,
		a.cd_convenio
	FROM 	paciente_internado_v2 a
	WHERE 	a.dt_alta IS NULL
	AND	Obter_Atepacu_paciente(a.nr_atendimento,'A') = a.NR_SEQ_ATEPACU
	AND	ie_radio_p = 2
	ORDER BY  a.nm_paciente;

CURSOR C03 IS
	SELECT 	a.nm_paciente,
		a.ds_convenio,
		SUBSTR(obter_estipulante_atend(a.cd_pessoa_fisica,'CD'),1,255) ds_pagador,
		a.cd_unidade,
		TO_CHAR(a.dt_entrada,'dd/mm/yy') dt_entrada,
		(TRUNC(TRUNC(NVL(a.dt_alta, SYSDATE)) - TRUNC(a.dt_entrada))) nr_dias_internado,
		TRUNC((SYSDATE - a.dt_nascimento) /365, 0) nr_idade,
		a.nm_medico,
		TO_CHAR(TO_DATE(obter_data_inclusao(a.cd_pessoa_fisica),'dd/mm/yyyy'),'dd/mm/yy') dt_inclusao,
		a.dt_alta,
		a.nr_atendimento,
		SUBSTR(Sus_Obter_Desc_Carater_Int(obter_carater_atend(a.nr_atendimento)),1,255) ds_carater,
		to_date(obter_estipulante_atend(a.cd_pessoa_fisica,'DT'),'dd/mm/yy') dt_contratacao,
		a.cd_convenio
	FROM 	paciente_internado_v2 a
	WHERE 	a.dt_alta BETWEEN dt_inical_p AND fim_dia(dt_final_p)
	AND	Obter_Atepacu_paciente(a.nr_atendimento,'A') = a.NR_SEQ_ATEPACU
	AND	 ie_radio_p = 3
	ORDER BY  a.nm_paciente;

BEGIN


DELETE	w_pac_internados;
COMMIT;

IF	(ie_radio_p = 1) THEN
	BEGIN
		OPEN C01;
		LOOP
		FETCH C01 INTO
			nm_paciente_w,
			ds_convenio_w,
			ds_pagador_w,
			cd_unidade_w,
			dt_entrada_w,
			nr_dias_internado_w,
			nr_idade_w,
			nm_medico_w,
			dt_inclusao_w,
			dt_alta_w,
			nr_atendimento_w,
			ds_carater_w,
			dt_contratacao_w,
			cd_convenio_w;
		EXIT WHEN C01%NOTFOUND;
			BEGIN
			INSERT INTO w_pac_internados(
				nm_paciente,
				ds_convenio,
				ds_pagador,
				cd_unidade,
				dt_entrada,
				nr_dias_internado,
				nr_idade,
				nm_medico,
				dt_inclusao,
				dt_alta,
				nr_atendimento,
				ds_carater,
				dt_contratacao,
				cd_convenio,
				nr_sequencia,
				dt_atualizacao,
				nm_usuario)
			VALUES( nm_paciente_w,
				ds_convenio_w,
				ds_pagador_w,
				cd_unidade_w,
				dt_entrada_w,
				nr_dias_internado_w,
				nr_idade_w,
				nm_medico_w,
				dt_inclusao_w,
				dt_alta_w,
				nr_atendimento_w,
				ds_carater_w,
				dt_contratacao_w,
				cd_convenio_w,
				w_pac_internados_seq.nextval,
				sysdate,
				Obter_Usuario_Ativo);
			END;
		END LOOP;
		CLOSE C01;
	END;
END IF;

IF	(ie_radio_p = 2) THEN
	BEGIN
		OPEN C02;
		LOOP
		FETCH C02 INTO
			nm_paciente_w,
			ds_convenio_w,
			ds_pagador_w,
			cd_unidade_w,
			dt_entrada_w,
			nr_dias_internado_w,
			nr_idade_w,
			nm_medico_w,
			dt_inclusao_w,
			dt_alta_w,
			nr_atendimento_w,
			ds_carater_w,
			dt_contratacao_w,
			cd_convenio_w;
		EXIT WHEN C02%NOTFOUND;
			BEGIN
			INSERT INTO w_pac_internados(
				nm_paciente,
				ds_convenio,
				ds_pagador,
				cd_unidade,
				dt_entrada,
				nr_dias_internado,
				nr_idade,
				nm_medico,
				dt_inclusao,
				dt_alta,
				nr_atendimento,
				ds_carater,
				dt_contratacao,
				cd_convenio,
				nr_sequencia,
				dt_atualizacao,
				nm_usuario)
			VALUES( nm_paciente_w,
				ds_convenio_w,
				ds_pagador_w,
				cd_unidade_w,
				dt_entrada_w,
				nr_dias_internado_w,
				nr_idade_w,
				nm_medico_w,
				dt_inclusao_w,
				dt_alta_w,
				nr_atendimento_w,
				ds_carater_w,
				dt_contratacao_w,
				cd_convenio_w,
				w_pac_internados_seq.nextval,
				sysdate,
				Obter_Usuario_Ativo);
			END;
		END LOOP;
		CLOSE C02;
	END;
END IF;

IF	(ie_radio_p = 3) THEN
	BEGIN
		OPEN C03;
		LOOP
		FETCH C03 INTO
			nm_paciente_w,
			ds_convenio_w,
			ds_pagador_w,
			cd_unidade_w,
			dt_entrada_w,
			nr_dias_internado_w,
			nr_idade_w,
			nm_medico_w,
			dt_inclusao_w,
			dt_alta_w,
			nr_atendimento_w,
			ds_carater_w,
			dt_contratacao_w,
			cd_convenio_w;
		EXIT WHEN C03%NOTFOUND;
			BEGIN
			INSERT INTO w_pac_internados(
				nm_paciente,
				ds_convenio,
				ds_pagador,
				cd_unidade,
				dt_entrada,
				nr_dias_internado,
				nr_idade,
				nm_medico,
				dt_inclusao,
				dt_alta,
				nr_atendimento,
				ds_carater,
				dt_contratacao,
				cd_convenio,
				nr_sequencia,
				dt_atualizacao,
				nm_usuario)
			VALUES( nm_paciente_w,
				ds_convenio_w,
				ds_pagador_w,
				cd_unidade_w,
				dt_entrada_w,
				nr_dias_internado_w,
				nr_idade_w,
				nm_medico_w,
				dt_inclusao_w,
				dt_alta_w,
				nr_atendimento_w,
				ds_carater_w,
				dt_contratacao_w,
				cd_convenio_w,
				w_pac_internados_seq.nextval,
				sysdate,
				Obter_Usuario_Ativo);
			END;
		END LOOP;
		CLOSE C03;
	END;
END IF;

COMMIT;

END gerar_w_rel_internados;
/