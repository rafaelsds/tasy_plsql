create or replace
procedure ctb_liberar_orcamento_mes(	cd_empresa_p		number,
				cd_estabelecimento_p	number,
				nr_seq_mes_ref_p		number,
				ie_operacao_p		varchar2,
				nm_usuario_p		Varchar2) is 

nr_sequencia_w				number(10);

/* Registros pendentes para libera��o */
cursor c01 is
select	a.nr_sequencia
from	ctb_orcamento a
where	a.cd_estabelecimento	= nvl(cd_estabelecimento_p,a.cd_estabelecimento)
and	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
and	a.dt_liberacao is null;

/* Registros liberados */
cursor c02 is
select	a.nr_sequencia
from	ctb_orcamento a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.nr_seq_mes_ref	= nr_seq_mes_ref_p
and	a.dt_liberacao is not null;

begin

if	(ie_operacao_p	= 'L') then
	begin
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w;
	exit when C01%notfound;
		begin
	
		update	ctb_orcamento
		set	dt_liberacao	= sysdate,
			nm_usuario_lib	= nm_usuario_p
		where	nr_sequencia	= nr_sequencia_w;
	
		end;
	end loop;
	close C01;
	end;
elsif	(ie_operacao_p = 'E') then
	begin
	open C02;
	loop
	fetch C02 into	
		nr_sequencia_w;
	exit when C02%notfound;
		begin
		
		update	ctb_orcamento
		set	dt_liberacao	= null,
			nm_usuario_lib	= ''
		where	nr_sequencia	= nr_sequencia_w;
		
		end;
	end loop;
	close C02;
	end;
end if;

commit;

end ctb_liberar_orcamento_mes;
/