create or replace
procedure Gerar_leite_protocolo(nr_prescricao_p		number,
				cd_protocolo_p		number,
				nr_seq_protocolo_p	number,
				nm_usuario_p		varchar2,
				cd_perfil_p		number) IS

cd_intervalo_w		varchar2(7);
ie_se_necessario_w	varchar2(1);
hr_prim_horario_w	varchar2(5);
hr_prim_hor_prescr_w	varchar2(5);
ie_via_aplicacao_w	varchar2(15);
qt_volume_oral_w	number(5);
qt_volume_sonda_w	number(5);
nr_seq_disp_succao_w	number(10);
nr_seq_leite_deriv_w	number(10);
qt_volume_total_w	number(10);
ds_horarios_w		varchar2(255);
ds_horarios_ww		varchar2(255);
dt_prim_hor_prescr_w	date;
dt_inicio_prescr_w	date;
nr_horas_validade_w	number(5);
nr_ocorrencia_w		number(10);
cd_material_w		number(10);
nr_seq_prot_leite_w	number(10);
nr_seq_prod_w		number(10);
nr_seq_prod_adic_w	number(10);
nr_agrupamento_w	number(7,1);
nr_sequencia_w		number(10);
nr_sequencia_dil_w	number(10);
cd_diluente_w		number(10);
qt_porcentagem_w	prescr_material.qt_porcentagem%type;
cd_unidade_medida_w	varchar2(30);
cd_unidade_med_dil_w	varchar2(30);
ds_erro_w		varchar2(255);

cursor	c01 is
select	cd_intervalo,
	nvl(ie_se_necessario,'N'),
	hr_prim_horario,
	ie_via_aplicacao,
	qt_volume_oral,
	qt_volume_sonda,
	nr_seq_disp_succao,
	nr_sequencia
from	protocolo_medic_leite
where	cd_protocolo		= cd_protocolo_p
and	nr_seq_protocolo	= nr_seq_protocolo_p;

cursor	c02 is
select	cd_material,
	nr_sequencia
from	protocolo_medic_produto
where	nr_seq_prot_leite	= nr_seq_prot_leite_w;

cursor	c03 is
select	cd_material,
	qt_porcentagem,
	cd_unidade_medida_dose
from	protocolo_medic_prod_adic
where	nr_seq_produto	=	nr_seq_prod_w;

cursor	c04 is
select	nr_sequencia,
	nr_seq_leite_deriv
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	ie_agrupador	= 16
and	nr_seq_protocolo = nr_seq_protocolo_p
and	cd_protocolo	= cd_protocolo_p;

begin

select	dt_inicio_prescr,
	nvl(nr_horas_validade,24),
	to_char(dt_primeiro_horario,'hh24:mi')
into	dt_inicio_prescr_w,
	nr_horas_validade_w,
	hr_prim_hor_prescr_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

open C01;
loop
fetch C01 into	
	cd_intervalo_w,
	ie_se_necessario_w,
	hr_prim_horario_w,
	ie_via_aplicacao_w,
	qt_volume_oral_w,
	qt_volume_sonda_w,
	nr_seq_disp_succao_w,
	nr_seq_prot_leite_w;
exit when C01%notfound;
	begin
	
	if	(cd_intervalo_w is not null) then
		if	(nvl(hr_prim_horario_w,'  :  ') = '  :  ') then
			hr_prim_horario_w	:= null;
		end if;
	
		dt_prim_hor_prescr_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_inicio_prescr_w, coalesce(hr_prim_horario_w, hr_prim_hor_prescr_w,'00:00')), 'mi');

		if	(dt_inicio_prescr_w	> dt_prim_hor_prescr_w) then
			dt_prim_hor_prescr_w	:= dt_prim_hor_prescr_w + 1;
		end if;	
		
		qt_volume_total_w	:= nvl(qt_volume_oral_w,0)	+ nvl(qt_volume_sonda_w,0);

		Calcular_Horario_Prescricao(nr_prescricao_p, cd_intervalo_w, dt_prim_hor_prescr_w, dt_prim_hor_prescr_w, nr_horas_validade_w, null, 
					    0, 0, nr_ocorrencia_w, ds_horarios_w, ds_horarios_ww, 'N', null, null, null, null, null);

		ds_horarios_w	:= eliminar_horarios_vigencia(ds_horarios_w, cd_intervalo_w, dt_prim_hor_prescr_w, dt_inicio_prescr_w, 0, 0, nr_prescricao_p);			    
					    
		select	prescr_leite_deriv_seq.nextval
		into	nr_seq_leite_deriv_w
		from	dual;
		
		insert into	prescr_leite_deriv	(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_prescricao,
							cd_intervalo,
							ie_via_aplicacao,
							qt_volume_oral,
							qt_volume_sonda,
							qt_volume_total,
							nr_seq_disp_succao,
							ie_se_necessario,
							ds_horarios,
							hr_prim_horario)
		values	(nr_seq_leite_deriv_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_prescricao_p,
			cd_intervalo_w,
			ie_via_aplicacao_w,
			qt_volume_oral_w,
			qt_volume_sonda_w,
			qt_volume_total_w,
			nr_seq_disp_succao_w,
			ie_se_necessario_w,
			ds_horarios_w,
			hr_prim_horario_w);					
		
		open C02;
		loop
		fetch C02 into	
			cd_material_w,
			nr_seq_prod_w;
		exit when C02%notfound;
			begin
			
			select	max(cd_unidade_medida_consumo)
			into	cd_unidade_medida_w
			from	material
			where	cd_material	= cd_material_w;
			
			select	nvl(max(nr_agrupamento),0) + 1
			into	nr_agrupamento_w
			from	prescr_material
			where	nr_prescricao		= nr_prescricao_p
			and	nr_seq_leite_deriv	= nr_seq_leite_deriv_w
			and	ie_agrupador		= 16;
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	prescr_material
			where	nr_prescricao	= nr_prescricao_p;
			
			insert	into prescr_material 	(nr_sequencia,
							nr_prescricao,
							cd_material,
							cd_unidade_medida_dose,
							cd_unidade_medida,
							dt_atualizacao,
							ie_origem_inf,
							nm_usuario,
							ie_agrupador,
							qt_material,
							qt_unitaria,
							nr_seq_leite_deriv,
							nr_agrupamento,
							nr_seq_protocolo,
							cd_protocolo,
							ie_via_leite)
					values		(nr_sequencia_w,
							nr_prescricao_p,
							cd_material_w,
							cd_unidade_medida_w,
							cd_unidade_medida_w,					
							sysdate,
							'1',
							nm_usuario_p,
							16,
							0,
							0,
							nr_seq_leite_deriv_w,
							nr_agrupamento_w,
							nr_seq_protocolo_p,
							cd_protocolo_p,
							ie_via_aplicacao_w);
							
			open C03;
			loop
			fetch C03 into	
				cd_diluente_w,
				qt_porcentagem_w,
				cd_unidade_med_dil_w;
			exit when C03%notfound;
				begin

				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_dil_w
				from	prescr_material
				where	nr_prescricao	= nr_prescricao_p;
				
				insert	into prescr_material 	(nr_sequencia,
								nr_prescricao,
								cd_material,
								cd_unidade_medida_dose,
								cd_unidade_medida,
								dt_atualizacao,
								ie_origem_inf,
								nm_usuario,
								ie_agrupador,
								qt_material,
								qt_unitaria,
								nr_seq_leite_deriv,
								nr_agrupamento,
								nr_sequencia_diluicao,
								nr_seq_protocolo,
								cd_protocolo,
								qt_porcentagem)
						values		(nr_sequencia_dil_w,
								nr_prescricao_p,
								cd_diluente_w,
								cd_unidade_med_dil_w,
								cd_unidade_med_dil_w,
								sysdate,
								'1',
								nm_usuario_p,
								17,
								0,
								0,
								nr_seq_leite_deriv_w,
								nr_agrupamento_w,
								nr_sequencia_w,
								nr_seq_protocolo_p,
								cd_protocolo_p,
								qt_porcentagem_w);			
				
				end;
			end loop;
			close C03;				
			
			end;
		end loop;
		close C02;
		atualiza_produto_lactario(nr_seq_leite_deriv_w, nm_usuario_p, cd_perfil_p);	
	end if;
	end;
end loop;
close C01;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if; 

open C04;
loop
fetch C04 into	
	nr_sequencia_w,
	nr_seq_leite_deriv_w;
exit when C04%notfound;
	begin
	consistir_prod_leites_deriv(nr_prescricao_p, nr_sequencia_w, nr_seq_leite_deriv_w, nm_usuario_p, cd_perfil_p, ds_erro_w);	
	end;
end loop;
close C04;

end Gerar_leite_protocolo;
/
