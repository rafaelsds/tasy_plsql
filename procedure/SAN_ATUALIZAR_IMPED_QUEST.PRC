create or replace
procedure san_atualizar_imped_quest	(nr_seq_doacao_p	number,
					nm_usuario_p		varchar2) is 

nr_sequencia_w		number(10);
ie_impede_doacao_w	varchar2(1);

Cursor C01 is
	select 	nr_sequencia
	from	san_questionario
	where	nr_seq_doacao = nr_seq_doacao_p;

begin
if	(nr_seq_doacao_p is not null) then
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w;
	exit when C01%notfound;
		begin
		select  decode(count(*),0, 'N','S')
		into	ie_impede_doacao_w
		from	san_questionario a,
			san_impedimento x
		where	x.nr_sequencia 		= a.nr_seq_impedimento
		and	a.ie_resposta 		<> NVL(x.ie_resposta_correta,'N')
		and 	a.ie_resposta is not null
		and	nvl(x.ie_pergunta,'N')  = 'N'
		and	a.nr_seq_doacao 	= nr_seq_doacao_p
		and	a.nr_sequencia 		= nr_sequencia_w
		and	((x.ie_definitivo = 'S') or (x.ie_definitivo = 'I') or ((a.dt_ocorrencia is null) or (trunc(a.dt_ocorrencia + nvl(x.nr_dias_inaptidao,0)) >= trunc(sysdate))));

		update	san_questionario
		set	ie_impede_doacao	=  ie_impede_doacao_w
		where	nr_sequencia		=  nr_sequencia_w;

		end;
	end loop;
	close C01;

end if;

commit;

end san_atualizar_imped_quest;
/