create or replace
procedure exec_regra_fim_evento(	nr_cirurgia_p		number,
									nr_seq_evento_dif_p	number,
							nm_usuario_p		Varchar2) is 
							
qt_procedimento_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
dt_registro_w			date;
dt_inicio_real_w		date;
nr_atendimento_w		number(10);
qt_regra_w			number(10);
nr_seq_proc_interno_w		number(10);
nr_seq_evento_w			number(10);
cd_tipo_anestesia_w		varchar2(10);

cursor	c01 is							
	select	nr_seq_proc_interno,
		nr_seq_evento
	from	regra_lanc_tempo_evento
	where	nr_seq_evento_dif 				= nr_seq_evento_dif_p
	and	nvl(cd_tipo_anestesia,cd_tipo_anestesia_w)	= cd_tipo_anestesia_w;


begin
select	count(*)
into	qt_regra_w
from	regra_lanc_tempo_evento
where	nr_seq_evento_dif = nr_seq_evento_dif_p;

if	(qt_regra_w > 0) and 
	(nvl(nr_cirurgia_p,0) > 0) then
	
	select	max(c.nr_atendimento),
		nvl(max(c.cd_tipo_anestesia),'XPTO')
	into	nr_atendimento_w,
		cd_tipo_anestesia_w
	from	cirurgia c
	where	c.nr_cirurgia = nr_cirurgia_p;
	
	/*
	select	max(nr_seq_proc_interno),
		max(nr_seq_evento)
	into	nr_seq_proc_interno_w,
		nr_seq_evento_w
	from	regra_lanc_tempo_evento
	where	nr_seq_evento_dif 				= nr_seq_evento_dif_p
	and	nvl(cd_tipo_anestesia,cd_tipo_anestesia_w)	= cd_tipo_anestesia_w;
	*/
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_proc_interno_w,
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		if	(nr_seq_proc_interno_w is not null) then
			Obter_Proc_Tab_Interno(	nr_seq_proc_interno_w,
						null,
						nr_atendimento_w,
						null,
						cd_procedimento_w,
						ie_origem_proced_w,
						null,
						null);
			
									
			select	max(dt_registro)
			into	dt_inicio_real_w
			from	evento_cirurgia_paciente
			where	nr_seq_evento	= nr_seq_evento_w
			and	nr_cirurgia	= nr_cirurgia_p
			and	nvl(ie_situacao,'A') = 'A';
			
			
			select	max(dt_registro)
			into	dt_registro_w
			from	evento_cirurgia_paciente
			where	nr_seq_evento	= nr_seq_evento_dif_p
			and	nr_cirurgia	= nr_cirurgia_p
			and	nvl(ie_situacao,'A') = 'A';

			select	max(nvl(obter_qte_proced_cirurgia(cd_procedimento_w, ie_origem_proced_w, dt_registro_w, dt_inicio_real_w),0))
			into	qt_procedimento_w
			from	dual;

			gerar_lancto_automatico_conta(	nr_atendimento_w,
							nr_cirurgia_p,
							null,
							null,
							null,
							nr_seq_proc_interno_w,
							cd_procedimento_w,
							ie_origem_proced_w,
							qt_procedimento_w,
							nm_usuario_p,
							null,
							null,
							null);
											
			commit;
		end if;	
		end;
	end loop;
	close C01;
end if;

end exec_regra_fim_evento;
/