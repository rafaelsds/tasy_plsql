CREATE OR REPLACE PROCEDURE "INSERT_COMUNIC_PRIVACY_NOTICE"(                                                         
dt_initial_vigencia_P    date,                                                       
nm_usuario_p         varchar2,                                                      
IE_NOTIFICATION_p    varchar2,                                                       
nr_seq_comunic_atual_p  number,                                                        
nr_seq_comunic_p   out  number )
IS
nr_sequencia_w  number(10);
  -- AUTHOR : ANAND MENTHE
BEGIN
  IF (IE_NOTIFICATION_p = 'S') THEN
    SELECT comunic_interna_seq.nextval INTO nr_sequencia_w FROM dual;
    
    INSERT
    INTO comunic_interna
      (
        nr_sequencia,
        dt_comunicado,
        ds_titulo,
        ds_comunicado,
        nm_usuario,
        dt_atualizacao,
        ie_geral,
        ie_gerencial,
        dt_agendamento,
        dt_liberacao
      )
      VALUES
      (
        nr_sequencia_w ,
        dt_initial_vigencia_p,
        wheb_mensagem_pck.get_texto(1033331),
        wheb_mensagem_pck.get_texto(1033332),
        nm_usuario_p,
        sysdate,
        'S',
        'N',
        dt_initial_vigencia_p,
        NULL
      );
  END IF;
  
  nr_seq_comunic_p  := nr_sequencia_w;
  
  IF (IE_NOTIFICATION_P = 'N') THEN
  
    DELETE  from comunic_interna
    WHERE   nr_sequencia = nr_seq_comunic_atual_p
    and     dt_liberacao is null;
    
    nr_seq_comunic_p := null;
 END IF;
  
  END INSERT_COMUNIC_PRIVACY_NOTICE;
/