create or replace
procedure ptu_processa_resp_cont_benef(	nr_seq_pedido_cont_p	ptu_pedido_contagem_benef.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					nr_seq_ret_cont_p   out ptu_resp_contagem_benef.nr_sequencia%type) is
						
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a valida��o do arquivo de  00430 - Requisi��o Contagem Benef.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_unimed_destino_w		ptu_pedido_contagem_benef.cd_unimed_destino%type;
cd_unimed_requisitante_w	ptu_pedido_contagem_benef.cd_unimed_requisitante%type;
dt_ano_referencia_w		ptu_pedido_contagem_benef.ds_ano_referencia%type;
nr_versao_w			ptu_pedido_contagem_benef.nr_versao%type;
qt_registros_w			number(15);
ie_status_retorno_w		number(15);
dt_referencia_w			date;
qt_benef_pf_co_loc_w		ptu_resp_contagem_benef.qt_ben_pf_co_loc%type;
qt_benef_pf_pre_loc_w		ptu_resp_contagem_benef.qt_ben_pf_pp_loc%type;
qt_benef_pf_co_rec_w		ptu_resp_contagem_benef.qt_ben_pf_rec_co%type;
qt_benef_pf_pre_rec_w		ptu_resp_contagem_benef.qt_ben_pf_rec_pp%type;
qt_benef_pf_co_rep_w		ptu_resp_contagem_benef.qt_ben_pf_rep_co%type;
qt_benef_pf_pre_rep_w		ptu_resp_contagem_benef.qt_ben_pf_rep_pp%type;
qt_benef_pj_co_loc_w		ptu_resp_contagem_benef.qt_ben_pj_co_loc%type;
qt_benef_pj_pre_loc_w		ptu_resp_contagem_benef.qt_ben_pj_pp_loc%type;
qt_benef_pj_co_rec_w		ptu_resp_contagem_benef.qt_ben_pj_rec_co%type;
qt_benef_pj_pre_rec_w		ptu_resp_contagem_benef.qt_ben_pj_rec_pp%type;
qt_benef_pj_co_rep_w		ptu_resp_contagem_benef.qt_ben_pj_rep_co%type;
qt_benef_pj_pre_rep_w		ptu_resp_contagem_benef.qt_ben_pj_rep_pp%type;

begin

select	cd_unimed_destino,
	cd_unimed_requisitante, 
	ds_ano_referencia,
	nr_versao
into	cd_unimed_destino_w,
	cd_unimed_requisitante_w,
	dt_ano_referencia_w,
	nr_versao_w
from	ptu_pedido_contagem_benef
where	nr_sequencia	= nr_seq_pedido_cont_p;

--dt_mes_referencia e dt_ano_referencia, s�o iguais e completos(com m�s e ano).
dt_referencia_w	:= to_date(dt_ano_referencia_w, 'dd/mm/rrrr');

select	count(1)
into	qt_registros_w
from	dual
where	trunc(dt_referencia_w,'month') between add_months(trunc(sysdate,'month'), - 3) and 	(trunc(sysdate,'month'));

if	(qt_registros_w	> 0) then
	select	count(1)
	into	qt_benef_pf_co_loc_w
	from	pls_contrato		a,
		pls_contrato_plano	b,
		pls_plano		c,
		pls_segurado		d
	where	a.nr_sequencia		= b.nr_seq_contrato
	and	b.nr_seq_plano		= c.nr_sequencia
	and	a.nr_sequencia		= d.nr_seq_contrato
	and	c.ie_preco 		in (2,3)
	and	a.cd_pf_estipulante	is not null
	and	a.cd_cgc_estipulante	is null
	and	d.ie_tipo_segurado	= 'B'
	and	d.ie_situacao_atend	= 'A'
	and	dt_contratacao		<= dt_referencia_w;
	
	select	count(1)
	into	qt_benef_pf_pre_loc_w
	from	pls_contrato		a,
		pls_contrato_plano	b,
		pls_plano		c,
		pls_segurado		d
	where	a.nr_sequencia		= b.nr_seq_contrato
	and	b.nr_seq_plano		= c.nr_sequencia
	and	a.nr_sequencia		= d.nr_seq_contrato
	and	c.ie_preco 		= 1
	and	a.cd_pf_estipulante	is not null
	and	a.cd_cgc_estipulante	is null
	and	d.ie_tipo_segurado	= 'B'
	and	d.ie_situacao_atend	= 'A'
	and	dt_contratacao		<= dt_referencia_w;
	
	select	count(1)
	into	qt_benef_pj_co_loc_w
	from	pls_contrato		a,
		pls_contrato_plano	b,
		pls_plano		c,
		pls_segurado		d
	where	a.nr_sequencia		= b.nr_seq_contrato
	and	b.nr_seq_plano		= c.nr_sequencia
	and	a.nr_sequencia		= d.nr_seq_contrato
	and	c.ie_preco 		in (2,3)
	and	a.cd_pf_estipulante	is null
	and	a.cd_cgc_estipulante	is not null
	and	d.ie_tipo_segurado	= 'B'
	and	d.ie_situacao_atend	= 'A'
	and	dt_contratacao		<= dt_referencia_w;
	
	select	count(1)
	into	qt_benef_pj_pre_loc_w
	from	pls_contrato		a,
		pls_contrato_plano	b,
		pls_plano		c,
		pls_segurado		d
	where	a.nr_sequencia		= b.nr_seq_contrato
	and	b.nr_seq_plano		= c.nr_sequencia
	and	a.nr_sequencia		= d.nr_seq_contrato
	and	c.ie_preco 		= 1
	and	a.cd_pf_estipulante	is null
	and	a.cd_cgc_estipulante	is not null
	and	d.ie_tipo_segurado	= 'B'
	and	d.ie_situacao_atend	= 'A'
	and	dt_contratacao		<= dt_referencia_w;

	select	count(1)
	into	qt_benef_pf_co_rep_w
	from	pls_contrato		a,
		pls_contrato_plano	b,
		pls_plano		c,
		pls_segurado		d,
		pls_segurado_repasse	e
	where	a.nr_sequencia		= b.nr_seq_contrato
	and	b.nr_seq_plano		= c.nr_sequencia
	and	a.nr_sequencia		= d.nr_seq_contrato
	and	d.nr_sequencia		= e.nr_seq_segurado
	and	a.cd_pf_estipulante	is not null
	and	a.cd_cgc_estipulante	is null
	and	d.ie_tipo_segurado	= 'R'
	and	e.ie_tipo_repasse	= 'C'
	and	d.ie_situacao_atend	= 'A'
	and	trunc(dt_referencia_w,'month') between trunc(e.dt_repasse,'month') and trunc(nvl(e.dt_fim_repasse,sysdate),'month')
	and	trunc(dt_referencia_w,'month')	>= trunc(e.dt_liberacao,'month');
	
	select	count(1)
	into	qt_benef_pf_pre_rep_w
	from	pls_contrato		a,
		pls_contrato_plano	b,
		pls_plano		c,
		pls_segurado		d,
		pls_segurado_repasse	e
	where	a.nr_sequencia		= b.nr_seq_contrato
	and	b.nr_seq_plano		= c.nr_sequencia
	and	a.nr_sequencia		= d.nr_seq_contrato
	and	d.nr_sequencia		= e.nr_seq_segurado
	and	a.cd_pf_estipulante	is not null
	and	a.cd_cgc_estipulante	is null
	and	d.ie_tipo_segurado	= 'R'
	and	e.ie_tipo_repasse	= 'P'
	and	d.ie_situacao_atend	= 'A'
	and	trunc(dt_referencia_w,'month') between trunc(e.dt_repasse,'month') and trunc(nvl(e.dt_fim_repasse,sysdate),'month')
	and	trunc(dt_referencia_w,'month')	>= trunc(e.dt_liberacao,'month');
	
	select	count(1)
	into	qt_benef_pj_co_rep_w
	from	pls_contrato		a,
		pls_contrato_plano	b,
		pls_plano		c,
		pls_segurado		d,
		pls_segurado_repasse	e
	where	a.nr_sequencia		= b.nr_seq_contrato
	and	b.nr_seq_plano		= c.nr_sequencia
	and	a.nr_sequencia		= d.nr_seq_contrato
	and	d.nr_sequencia		= e.nr_seq_segurado
	and	a.cd_pf_estipulante	is null
	and	a.cd_cgc_estipulante	is not null
	and	d.ie_tipo_segurado	= 'R'
	and	e.ie_tipo_repasse	= 'C'
	and	d.ie_situacao_atend	= 'A'
	and	trunc(dt_referencia_w,'month') between trunc(e.dt_repasse,'month') and trunc(nvl(e.dt_fim_repasse,sysdate),'month')
	and	trunc(dt_referencia_w,'month')	>= trunc(e.dt_liberacao,'month');
	
	select	count(1)
	into	qt_benef_pj_pre_rep_w
	from	pls_contrato		a,
		pls_contrato_plano	b,
		pls_plano		c,
		pls_segurado		d,
		pls_segurado_repasse	e
	where	a.nr_sequencia		= b.nr_seq_contrato
	and	b.nr_seq_plano		= c.nr_sequencia
	and	a.nr_sequencia		= d.nr_seq_contrato
	and	d.nr_sequencia		= e.nr_seq_segurado
	and	a.cd_pf_estipulante	is null
	and	a.cd_cgc_estipulante	is not null
	and	d.ie_tipo_segurado	= 'R'
	and	e.ie_tipo_repasse	= 'P'
	and	d.ie_situacao_atend	= 'A'
	and	trunc(dt_referencia_w,'month') between trunc(e.dt_repasse,'month') and trunc(nvl(e.dt_fim_repasse,sysdate),'month')
	and	trunc(dt_referencia_w,'month')	>= trunc(e.dt_liberacao,'month');
	
	select	count(1)
	into	qt_benef_pf_co_rec_w
	from	pls_intercambio		a,
		pls_segurado		b
	where	a.nr_sequencia		= b.nr_seq_intercambio
	and	a.cd_pessoa_fisica	is not null
	and	a.cd_cgc		is null
	and	b.ie_tipo_segurado	in ('C','T')
	and	b.ie_tipo_repasse	= 'C'
	and	b.ie_situacao_atend	= 'A'
	and	dt_contratacao		<= dt_referencia_w;
	
	select	count(1)
	into	qt_benef_pf_pre_rec_w
	from	pls_intercambio		a,
		pls_segurado		b
	where	a.nr_sequencia		= b.nr_seq_intercambio
	and	a.cd_pessoa_fisica	is not null
	and	a.cd_cgc		is null
	and	b.ie_tipo_segurado	in ('C','T')
	and	b.ie_tipo_repasse	= 'P'
	and	b.ie_situacao_atend	= 'A'
	and	dt_contratacao		<= dt_referencia_w;
	
	select	count(1)
	into	qt_benef_pj_co_rec_w
	from	pls_intercambio		a,
		pls_segurado		b
	where	a.nr_sequencia		= b.nr_seq_intercambio
	and	a.cd_pessoa_fisica	is null
	and	a.cd_cgc		is not null
	and	b.ie_tipo_segurado	in ('C','T')
	and	b.ie_tipo_repasse	= 'C'
	and	b.ie_situacao_atend	= 'A'
	and	dt_contratacao		<= dt_referencia_w;
	
	select	count(1)
	into	qt_benef_pj_pre_rec_w
	from	pls_intercambio		a,
		pls_segurado		b
	where	a.nr_sequencia		= b.nr_seq_intercambio
	and	a.cd_pessoa_fisica	is null
	and	a.cd_cgc		is not null
	and	b.ie_tipo_segurado	in ('C','T')
	and	b.ie_tipo_repasse	= 'P'
	and	b.ie_situacao_atend	= 'A'
	and	dt_contratacao		<= dt_referencia_w;

	ie_status_retorno_w	:= 1;
else
	ie_status_retorno_w	:= 3;
end if;

insert	into ptu_resp_contagem_benef
	(nr_sequencia, cd_transacao, cd_unimed_destino,
	cd_unimed_requisitante, ie_status_retorno, ie_tipo_cliente,
	nr_seq_contagem_benef, qt_ben_pf_co_loc, qt_ben_pf_pp_loc,
	qt_ben_pf_rec_co, qt_ben_pf_rec_pp, qt_ben_pf_rep_co,
	qt_ben_pf_rep_pp, qt_ben_pj_co_loc, qt_ben_pj_pp_loc,
	qt_ben_pj_rec_co, qt_ben_pj_rec_pp, qt_ben_pj_rep_co,
	qt_ben_pj_rep_pp, dt_atualizacao, dt_atualizacao_nrec,
	nm_usuario, nm_usuario_nrec, nr_versao)
values	(ptu_resp_contagem_benef_seq.nextval, '00431', cd_unimed_destino_w,
	cd_unimed_requisitante_w, ie_status_retorno_w, 'U',
	nr_seq_pedido_cont_p, qt_benef_pf_co_loc_w, qt_benef_pf_pre_loc_w,
	qt_benef_pf_co_rec_w, qt_benef_pf_pre_rec_w, qt_benef_pf_co_rep_w,
	qt_benef_pf_pre_rep_w, qt_benef_pj_co_loc_w, qt_benef_pj_pre_loc_w,
	qt_benef_pj_co_rec_w, qt_benef_pj_pre_rec_w, qt_benef_pj_co_rep_w,
	qt_benef_pj_pre_rep_w, sysdate, sysdate,
	nm_usuario_p, nm_usuario_p, nr_versao_w) returning nr_sequencia into nr_seq_ret_cont_p;

commit;

end ptu_processa_resp_cont_benef;
/