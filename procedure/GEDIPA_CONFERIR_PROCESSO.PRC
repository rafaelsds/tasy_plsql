create or replace
procedure gedipa_conferir_processo (
				cd_estabelecimento_p	number,
				cd_perfil_p		number,
				nr_processo_p		number,
				nr_seq_area_p		number,
				ie_conferir_p		varchar2,
				nm_usuario_p		varchar2) is
				
nr_sequencia_w	number(10,0);
				
begin
if	(nr_processo_p is not null) and
	(ie_conferir_p is not null) and
	(nm_usuario_p is not null) then
	
	if	(ie_conferir_p = 'S') then
	
		/*
		update	adep_processo
		set	dt_leitura		= sysdate,
			nm_usuario_leitura	= nm_usuario_p
		where	nr_sequencia		= nr_processo_p
		and	dt_leitura		is null
		and	nm_usuario_leitura	is null;
		*/
		
		atualiza_status_processo_adep(nr_processo_p, nr_seq_area_p, 'G', 'C', sysdate, nm_usuario_p);
				
	end if;
	
end if;

commit;

end gedipa_conferir_processo;
/
