create or replace
procedure atualizar_comunic_contrato_js(	ds_comunicado_p		varchar2,
					nr_sequencia_p		number) is 

begin
if	(ds_comunicado_p is not null) and
	(nr_sequencia_p	is not null) then
	begin
	update	comunic_interna
	set	ds_comunicado 	= ds_comunicado_p
	where	nr_sequencia 	= nr_sequencia_p;
	end;
end if;

commit;

end atualizar_comunic_contrato_js;
/