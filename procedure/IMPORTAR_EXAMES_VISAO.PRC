CREATE OR REPLACE
PROCEDURE importar_exames_visao(	cd_medico_p		varchar,
					nr_prescricao_p		number,
					nr_seq_presc_p		number,
					nr_seq_cliente_p	number,
					dt_resultado_p		date,
					nr_atendimento_p	number,
					nm_usuario_p		varchar)
					IS


nr_atendimento_w	number(10);
qt_resultado_w		number(15,4);
ds_resultado_w		varchar2(255);
pr_resultado_w		number(9,4);
nr_seq_exame_w		number(10);
nr_seq_exame_padrao_w	number(10);
nr_sequencia_w		number(10);
dt_exame_w		date;	
ie_existe_w		varchar2(1);

					
CURSOR C01 IS
	select	c.qt_resultado,
		substr(c.ds_resultado,1,255),
		c.pr_resultado,
		c.nr_seq_exame,
		d.nr_sequencia
	from 
		prescr_procedimento a, 
		exame_lab_resultado b, 
		exame_lab_result_item c,
		med_exame_padrao d	  
	where 	a.nr_prescricao		= b.nr_prescricao
	and 	b.nr_seq_resultado 	= c.nr_seq_resultado 
	and 	a.nr_sequencia 	   	= c.nr_seq_prescr
	and		c.nr_seq_exame	   	= d.nr_seq_exame
	and		d.cd_medico	   		= cd_medico_p
	and		d.ie_resultado	   	= 'S'
	and		d.ie_padrao			= 'S'
	and 	a.nr_prescricao    	= nr_prescricao_p
	and 	a.nr_sequencia 	   	= nr_seq_presc_p
	and	not exists (select 	1
					from 	prescr_medica f,
							med_result_exame b
					where	f.nr_prescricao 	= b.nr_prescricao
					and 	b.nr_seq_cliente	= nr_seq_cliente_p
					and		b.nr_seq_exame		= d.nr_sequencia
					and		trunc(nvl(b.dt_exame,f.dt_prescricao))	= trunc(dt_resultado_p));
				
cursor C04 is
	SELECT 	a.dt_exame,
		b.nr_sequencia
	FROM   	med_result_exame a,
		med_exame_padrao b				
	WHERE  	a.nr_seq_exame 	  	= b.nr_sequencia
	AND	a.nr_seq_cliente   	= nr_seq_cliente_p
	and	a.dt_exame 		= trunc(dt_resultado_p)
	and	a.nr_prescricao		= nr_prescricao_p
	and	a.nr_seq_prescricao	= nr_seq_presc_p
	and	nvl(a.ie_calculado,'N') = 'N';
	
begin

nr_atendimento_w		:= nr_atendimento_p;
if	(nr_atendimento_w = 0) then
	nr_atendimento_w	:= null;	
end if;

-- Exames da prescri��o que possuem vinculo com o cadastro de exames do m�dico no TasyMed somente ser�o importados.
open c01;
loop
fetch c01 into
	qt_resultado_w,
	ds_resultado_w,
	pr_resultado_w,
	nr_seq_exame_w,
	nr_seq_exame_padrao_w;
exit when c01%notfound;
	select 	nvl(max('S'),'N')
	into	ie_existe_w	
	from 	med_result_exame b
	where	b.nr_seq_cliente	= nr_seq_cliente_p
	and	b.nr_seq_exame		= nr_seq_exame_w
	and	trunc(b.dt_exame)	= trunc(dt_resultado_p)
	and 	((vl_exame 		= qt_resultado_w) or 
		(vl_exame 		= pr_resultado_w) or 
		(ds_valor_exame 	= ds_resultado_w));
	
	if 	(ie_existe_w = 'N') then 
		select	med_result_exame_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert	into med_result_exame
			(nr_sequencia,
			nr_atendimento,
			nr_seq_exame,
			dt_exame,
			dt_atualizacao,
			nm_usuario,
			ds_valor_exame,
			vl_exame,
			nr_seq_cliente,
			nr_prescricao,
			nr_seq_prescricao,
			nr_seq_exame_lab)
		values	(nr_sequencia_w,
			nr_atendimento_w,
			nr_seq_exame_padrao_w,
			trunc(dt_resultado_p),
			sysdate,
			nm_usuario_p,
			null,
			null,
			nr_seq_cliente_p,
			nr_prescricao_p,
			nr_seq_presc_p,
			nr_seq_exame_w);
		
		IF	(NVL(qt_resultado_w,0) <> 0) THEN
			update	med_result_exame
			set	vl_exame	= qt_resultado_w
			where	nr_sequencia	= nr_sequencia_w;
												
			Med_Altera_Resultado_Exame(nr_sequencia_w,qt_resultado_w,nr_seq_cliente_p,nr_seq_exame_padrao_w);

		elsif	(nvl(pr_resultado_w,0) <> 0) then
			update	med_result_exame
			set	vl_exame	= pr_resultado_w
			where	nr_sequencia	= nr_sequencia_w;
			
			med_altera_resultado_exame(nr_sequencia_w,pr_resultado_w,nr_seq_cliente_p,nr_seq_exame_padrao_w);
			
		elsif	(ds_resultado_w is not null) then
			update	med_result_exame
			set	ds_valor_exame	= ds_resultado_w
			where	nr_sequencia	= nr_sequencia_w;
		end if;	
	
		commit;
	end if;
	
end loop;
close c01;

-- Cursor para gerar exames provenientes de c�lculo - Campo DS_CALCULO da tabela MED_EXAME_PADRAO
open c04;
loop
fetch c04 into
	dt_exame_w,
	nr_seq_exame_padrao_w;
exit when c04%notfound;
	med_atualiza_exame_interno(nr_seq_cliente_p,dt_exame_w,nr_seq_exame_padrao_w);
end loop;
close c04;

commit;

end	importar_exames_visao;
/