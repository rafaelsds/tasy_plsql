create or replace
procedure tiss_gerar_dem_conta(	nr_seq_lote_p		number,
				nr_guia_prestador_p	varchar2,
				nr_guia_operadora_p	varchar2,
				cd_usuario_convenio_p	varchar2,
				nm_usuario_convenio_p	varchar2,
				ds_plano_p		varchar2,
				dt_realizacao_p		date,
				vl_processado_p		number,
				vl_liberado_p		number,
				vl_glosado_p		number,
				vl_informado_p		number,
				nm_usuario_p		Varchar2,
				nr_seq_conta_p		out number,
				dt_inicio_fat_p		date default null,
				dt_hora_ini_fat_p	varchar2 default null,
				dt_fim_fat_p		date default null,
				dt_hora_fim_fat_p	varchar2 default null,
				cd_senha_p		varchar2 default null) is 

nr_sequencia_w			number(10);			
dt_ini_fat_w			varchar2(20);
dt_fim_fat_w			varchar2(20);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.get_cd_Estabelecimento;

begin	

if	(dt_inicio_fat_p is not null and
	dt_fim_fat_p is not null and
	Obter_Valor_Param_Usuario(27, 296, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w) = 'S') then
	
	dt_ini_fat_w := to_char(dt_inicio_fat_p,'dd/mm/yyyy')||' ' || dt_hora_ini_fat_p;
	dt_fim_fat_w := to_char(dt_fim_fat_p,'dd/mm/yyyy')|| ' ' || dt_hora_fim_fat_p;
end if;	

select	tiss_dem_conta_seq.nextval
into	nr_sequencia_w
from 	dual;



insert into tiss_dem_conta    
          (nr_sequencia,      
          dt_atualizacao,     
          nm_usuario,         
          nr_seq_lote,        
          nr_guia_prestador,  
          nr_guia_operadora,  
          cd_usuario_convenio,
          nm_usuario_convenio,
          ds_plano,           
          dt_realizacao,      
          vl_processado,      
          vl_liberado,        
          vl_glosado,
	  vl_informado,
	  dt_importacao,
	  dt_inicio_faturamento,
	  dt_fim_faturamento,
	  cd_senha)         
	values(
	  nr_sequencia_w,
	  sysdate,
	  nm_usuario_p,
	  nr_seq_lote_p,
	  nr_guia_prestador_p,
	  nr_guia_operadora_p,
	  cd_usuario_convenio_p,
	  nm_usuario_convenio_p,
	  ds_plano_p,
	  dt_realizacao_p,
	  vl_processado_p,
	  vl_liberado_p,
	  vl_glosado_p,
	  vl_informado_p,
	  null,
	  nvl(to_date(dt_ini_fat_w,'dd/mm/yyyy hh24:mi:ss'),null),
	  nvl(to_date(dt_fim_fat_w,'dd/mm/yyyy hh24:mi:ss'),null),
	  cd_senha_p);
	  
nr_seq_conta_p	:= nr_sequencia_w;	  
	  
commit;

end tiss_gerar_dem_conta;
/