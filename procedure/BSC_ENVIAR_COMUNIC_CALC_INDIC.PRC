create or replace
procedure bsc_enviar_comunic_calc_indic(	nr_seq_regra_comunic_p	number,
					cd_periodo_p		number,
					cd_ano_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is 

					

nm_indicador_w			varchar2(255);
nm_usuario_w			varchar2(255);
nm_usuario_dest_w			varchar2(4000);
nr_seq_regra_dest_w		number(10);
nr_seq_indicador_w			number(10);
ds_titulo_w			varchar2(255)	:= ' ';
ds_titulo_ww			varchar2(255);
ds_comunicado_w			long;
ds_comunicado_ww			long;
dt_referencia_w			date;
ds_cabecalho_comunicado_w	varchar2(255);
ds_setor_adicional_w		varchar2(255);
ds_perfil_adicional_w		varchar2(255);
ie_enviar_responsavel_w		varchar2(1);
ie_gerar_w			varchar2(1);
qt_meta_w			number(15,2);
qt_limite_w			number(15,2);
qt_real_w			number(15,2);
nm_usuario_registro_w		varchar2(4000);			
nm_usuario_ant_w		varchar2(4000);
ind				integer;
ie_existe_w		varchar2(1);

type reg_usuario is record(	nm_usuario varchar2(15),
				ie_usuario number(2)); /* 0 - sao os usuarios destino cadastrados fixos na regra e 
							1 - Usuarios responsaveis do indicador */

type tab_usuario is table of reg_usuario index by binary_integer;

usuario_dest_w			tab_usuario;

cursor	c01 is
select	b.nr_seq_indicador,
	b.nm_indicador
from	bsc_calc_indicador_v b,
	bsc_regra_comunic_calc a
where	b.nr_seq_indicador	= nvl(a.nr_seq_indicador,b.nr_seq_indicador)
and	b.nr_seq_result = nvl(a.nr_seq_result, b.nr_seq_result)
and	a.nr_sequencia		= nr_seq_regra_comunic_p
and	b.cd_ano		= cd_ano_p
and	b.cd_periodo		= cd_periodo_p;
	

	
cursor	c02 is
select	a.nr_sequencia,
	a.cd_setor_atendimento,
	a.cd_perfil,
	a.ie_enviar_responsavel
from	bsc_regra_com_calc_dest a
where	a.nr_seq_regra_comunic	= nr_seq_regra_comunic_p
order by	nvl(nm_usuario_dest, 'A'),
		nvl(cd_perfil, 0),
		nvl(cd_setor_atendimento, 0);
	
	
cursor	c03 is
select	DISTINCT(a.nm_usuario)
from	bsc_ind_resp b,
	usuario a
where	a.cd_pessoa_fisica		= b.cd_pessoa_fisica
and	b.nr_seq_indicador		= nr_seq_indicador_w
and	a.ie_situacao		= 'A'
and	nvl(b.ie_receb_comunic,'S')	= 'S'
and	(b.dt_inicio_vigencia	<= dt_referencia_w and nvl(b.dt_fim_vigencia, dt_referencia_w) >= dt_referencia_w);


cursor	c04 is
select	distinct
	a.nm_usuario_dest
from	bsc_regra_com_calc_dest a
where	a.nr_seq_regra_comunic	= nr_seq_regra_comunic_p
and	a.nm_usuario_dest is not null;


begin

select	a.ds_comunicado,
	a.ds_titulo
into	ds_comunicado_w,
	ds_titulo_w
from	bsc_regra_comunic_calc a
where	a.nr_sequencia	= nr_seq_regra_comunic_p;

dt_referencia_w	:= sysdate;

ind	:= 0;

open c04;
loop
fetch c04 into	
	nm_usuario_registro_w;
exit when c04%notfound;
	begin
	
	ind			:= ind + 1;
	usuario_dest_w(ind).nm_usuario := nm_usuario_registro_w;
	usuario_dest_w(ind).ie_usuario := 0;
	
end;
end loop;
close c04;

open c01;
loop
fetch c01 into	
	nr_seq_indicador_w,
	nm_indicador_w;
exit when c01%notfound;
	begin
	ds_titulo_ww		:= substr(ds_titulo_w || ' - ' ||nr_seq_indicador_w|| ' - ' || nm_indicador_w || ' - ' || Wheb_mensagem_pck.get_texto(799311) || ' ' || to_char(cd_periodo_p) || '/' || to_char(cd_ano_p), 1, 255);
	ds_comunicado_ww	:= ds_comunicado_w;
	nm_usuario_dest_w	:= '';
	
	select	nvl(max(qt_meta),0),
		nvl(max(qt_limite),0),
		nvl(max(qt_real),0)	
	into	qt_meta_w,
		qt_limite_w,
		qt_real_w
	from	BSC_IND_INF
	where	nr_seq_indicador = nr_seq_indicador_w
	and	cd_ano		 = cd_ano_p
	and	cd_periodo	 = cd_periodo_p;
	  	 
	if	(nvl(ds_comunicado_ww,'X') <> 'X') then
		
		ds_comunicado_ww	:= replace_macro_long(ds_comunicado_ww,'@META',campo_mascara_virgula(qt_meta_w));
		ds_comunicado_ww	:= replace_macro_long(ds_comunicado_ww,'@LIMITE',campo_mascara_virgula(qt_limite_w));
		ds_comunicado_ww	:= replace_macro_long(ds_comunicado_ww,'@REAL',campo_mascara_virgula(qt_real_w));
		ds_comunicado_ww	:= replace_macro_long(ds_comunicado_ww,'@INDICADOR',nm_indicador_w);
	end if;
	
	for i in 1..usuario_dest_w.Count loop
		begin
		if	(usuario_dest_w(i).ie_usuario = 1) then
			usuario_dest_w.delete(i);
		end if;
		end;
	end loop;
	
	ind	:= usuario_dest_w.Count;
	
	open c02;
	loop
	fetch c02 into	
		nr_seq_regra_dest_w,
		ds_setor_adicional_w,
		ds_perfil_adicional_w,
		ie_enviar_responsavel_w;
	exit when c02%notfound;
		begin
	

		
		if	(ie_enviar_responsavel_w = 'S') then
		
			open c03;
			loop
			fetch c03 into	
				nm_usuario_w;
			exit when c03%notfound;
				begin
				ie_existe_w	:= 'N';
				
				for i in 1..usuario_dest_w.Count loop
					begin
					if	(upper(usuario_dest_w(i).nm_usuario) = upper(nm_usuario_w)) then
						ie_existe_w	:= 'S';
						exit;
					end if;
					end;
				end loop;
				
				if	(ie_existe_w = 'N') then
					ind			:= ind + 1;
					usuario_dest_w(ind).nm_usuario := nm_usuario_w;
					usuario_dest_w(ind).ie_usuario := 1;
				end if;
				
				end;
			end loop;
			close c03;

		end if;
		
		for i in 1..usuario_dest_w.Count loop
			begin
			nm_usuario_dest_w	:= substr(nm_usuario_dest_w || usuario_dest_w(i).nm_usuario || ', ', 1, 4000);
			end;
		end loop;
		
		end;
	end loop;
	close c02;
	
	
	gerar_comunic_padrao(	sysdate,
					ds_titulo_ww,														
					ds_comunicado_ww,
					nm_usuario_p,							
					null,
					nm_usuario_dest_w,
					'N',
					null,
					ds_perfil_adicional_w,
					cd_estabelecimento_p,
					ds_setor_adicional_w,
					sysdate,
					null,
					null);
	end;
end loop;
close c01;

commit;

end bsc_enviar_comunic_calc_indic;
/
