create or replace
procedure obter_dados_peso_altura_js(	nr_prescricao_p		number,
									nr_sequencias_p		varchar2,
					qt_peso_p		out number,
					qt_altura_p		out number,
					ds_obs_coleta_peso_p out varchar2) is 

qt_peso_w	number(6,3);
qt_altura_cm_w	number(4,1);
ds_obs_coleta_peso_w varchar2(1);

begin

select	max(qt_peso),
	max(qt_altura_cm)
into	qt_peso_w,
	qt_altura_cm_w
from 	prescr_medica
where 	nr_prescricao = nr_prescricao_p;


qt_peso_p	:= qt_peso_w;
qt_altura_p	:= qt_altura_cm_w;
ds_obs_coleta_peso_p := lab_obter_exige_observ_col(nr_prescricao_p,nr_sequencias_p);


end obter_dados_peso_altura_js;
/
