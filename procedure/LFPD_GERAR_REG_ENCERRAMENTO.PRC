create or replace
procedure lfpd_gerar_reg_encerramento(	nr_seq_controle_p	number,
					nm_usuario_p		varchar2,
					cd_registro_p		varchar2,
					ds_separador_p		varchar2,
					qt_linha_p		in out number,
					nr_sequencia_p		in out number) is
					
/*
Se o identificador de movimento for igual a 'S', ent�o o bloco possui movimento e deve ser informado zero.
Caso contr�rio, n�a possui movimento e deve ser informado um.
*/
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10) := qt_linha_p;
nr_seq_registro_w	number(10) := nr_sequencia_p;
qt_gerados_w		number(10);
sep_w			varchar2(1):= ds_separador_p;
					
begin

select	count(*) + 1
into	qt_gerados_w
from	fis_lfpd_arquivo
where	substr(cd_registro,1,1) = substr(cd_registro_p,1,1)
and		nm_usuario = nm_usuario_p;

ds_linha_w := sep_w || cd_registro_p || sep_w || qt_gerados_w || sep_w ;

ds_arquivo_w		:= substr(ds_linha_w,1,4000);
ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
nr_seq_registro_w	:= nr_seq_registro_w + 1;
nr_linha_w		:= nr_linha_w + 1;

insert into fis_lfpd_arquivo	(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_linha,
					ds_arquivo,
					ds_arquivo_compl,
					cd_registro,
					nr_seq_controle_lfpd)
			values	(	fis_lfpd_arquivo_seq.NextVal,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_linha_w,
					ds_arquivo_w,
					ds_arquivo_compl_w,
					cd_registro_p,
					nr_seq_controle_p);
commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end lfpd_gerar_reg_encerramento;
/