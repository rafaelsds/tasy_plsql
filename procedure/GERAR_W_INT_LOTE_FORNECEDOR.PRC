create or replace procedure gerar_w_int_lote_fornecedor(
					cd_cgc_fornec_p			varchar2,                   
					cd_estabelecimento_p		varchar2,              
					cd_material_p			varchar2,                     
					ds_lote_fornec_p		varchar2,                  
					ds_observacao_p			varchar2,                             
					dt_fabricacao_p			varchar2,                  
					dt_validade_p			varchar2,                  
					ie_situacao_p			varchar2,                     
					ie_validade_indeterminada_p	varchar2,                       
					nr_seq_local_p			varchar2,                    
					nr_seq_marca_p			varchar2,                                    
					nr_serie_material_p		varchar2,               
					qt_material_p			varchar2, 
					nm_usuario_p			varchar2,
					nr_seq_lote_fornecedor_p	out varchar2) is
					
					
  					
				
				
nr_seq_lote_fornecedor_w	number(10);	
ds_erro_w	varchar2(4000);			

begin

select	w_int_lote_fornecedor_seq.nextval
into	nr_seq_lote_fornecedor_w
from	dual;

insert into w_int_lote_fornecedor(
			nr_sequencia,           
			cd_cgc_fornec,                   
			cd_estabelecimento,              
			cd_material,                     
			ds_lote_fornec,                  
			ds_observacao,                             
			dt_fabricacao,                   
			dt_validade,                     
			ie_situacao,                     
			ie_validade_indeterminada,                       
			nr_seq_local,                    
			nr_seq_marca,                                    
			nr_serie_material,               
			qt_material,    
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,          
			dt_atualizacao_nrec)
values( nr_seq_lote_fornecedor_w,
	cd_cgc_fornec_p,                   
	cd_estabelecimento_p,              
	cd_material_p,                     
	ds_lote_fornec_p,                  
	ds_observacao_p,                             
	to_date(dt_fabricacao_p,'dd/mm/yyyy'),                   
	to_date(dt_validade_p,'dd/mm/yyyy'),                     
	ie_situacao_p,                     
	ie_validade_indeterminada_p,                       
	nr_seq_local_p,                    
	nr_seq_marca_p,                                    
	nr_serie_material_p,               
	to_number(qt_material_p),      
	nm_usuario_p,
	nm_usuario_p,
	sysdate,
	sysdate); 
		
nr_seq_lote_fornecedor_p := nr_seq_lote_fornecedor_w;	

commit;


end gerar_w_int_lote_fornecedor;
/