create or replace
procedure gravar_anexo_via_reserva_anexo(	nr_seq_reserva_p		number,
					ds_arquivo_p		varchar2,
					nm_usuario_p		varchar2) is

begin
insert into via_reserva_anexo(
	nr_sequencia,
	nr_seq_reserva,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_arquivo)
values(	via_reserva_anexo_seq.nextval,
	nr_seq_reserva_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	ds_arquivo_p);

commit;

end gravar_anexo_via_reserva_anexo;
/