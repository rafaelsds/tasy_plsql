create or replace
procedure lab_fechar_lote_exame(nr_seq_lote_p	number,
				ie_etapa_p	number,
				ie_lote_p	varchar2,
				nm_usuario_p	varchar2) is

nr_prescricao_w		number(14);
nr_Seq_prescr_w		number(6);

Cursor C01 is
	select 	a.nr_Seq_prescr,
		a.nr_prescricao
	from	lab_lote_exame_item a,
		lab_lote_exame b
	where 	b.nr_sequencia = a.nr_seq_lote
	and	a.nr_seq_lote = nr_seq_lote_p;

begin

update lab_lote_exame 
set ie_status = 'F'
where nr_sequencia = nr_seq_lote_p;

if ((nvl(ie_etapa_p,0) > 0) and (ie_lote_p = 'I')) then

	OPEN C01;
	LOOP
	FETCH C01 into
		nr_Seq_prescr_w,
		nr_prescricao_w;
	exit when c01%notfound;

		update	prescr_procedimento
		set 	ie_status_atend = ie_etapa_p
		where	nr_prescricao = nr_prescricao_w
		and	nr_Sequencia = nr_Seq_prescr_w
		and	ie_status_atend >= 35;

	END LOOP;
	CLOSE C01;

end if; 
end;
/