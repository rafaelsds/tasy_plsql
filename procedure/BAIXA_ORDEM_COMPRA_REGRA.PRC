create or replace
procedure baixa_ordem_compra_regra(cd_estabelecimento_p		number) is

cd_local_estoque_w		number(10);
qt_dias_w			number(10);
nr_ordem_compra_w		number(10);
ds_assunto_w			varchar2(255);
ds_mensagem_w			varchar2(255);
ds_email_origem_w		varchar2(255);
ds_email_destino_w		varchar2(255);
nm_usuario_w			varchar2(255);
ie_param_email_w		varchar2(1);
nr_seq_motivo_baixa_aut_w	number(10);

cursor c01 is
select	nvl(cd_local_estoque,0),
	qt_dias
from	regra_baixa_ordem_compra
where	cd_estabelecimento = cd_estabelecimento_p
order by cd_local_estoque;

cursor c02 is
/*baixa as ordens do local informado na regra*/
select	distinct
	a.nr_ordem_compra
from 	ordem_compra a,
	ordem_compra_item_entrega b
where 	a.nr_ordem_compra = b.nr_ordem_compra
and	a.cd_estabelecimento = cd_estabelecimento_p
and	((nvl(b.qt_prevista_entrega,0) > nvl(b.qt_real_entrega,0)) and b.dt_baixa is null)
and	trunc(b.dt_prevista_entrega,'dd') < (trunc(sysdate,'dd') - qt_dias_w)
and	b.dt_real_entrega is null
and	a.dt_baixa is null
and	a.cd_local_entrega = cd_local_estoque_w
and	cd_local_estoque_w > 0
union
/*	Quando a regra n�o tem local informado, que significa que s�o as ordens de todos os locais
	menos os locais que tem na regra, pois esses far� update no select acima*/
select	distinct
	a.nr_ordem_compra
from 	ordem_compra a,
	ordem_compra_item_entrega b
where 	a.nr_ordem_compra = b.nr_ordem_compra
and	a.cd_estabelecimento = cd_estabelecimento_p
and	((nvl(b.qt_prevista_entrega,0) > nvl(b.qt_real_entrega,0)) and b.dt_baixa is null)
and	trunc(b.dt_prevista_entrega,'dd') < (trunc(sysdate,'dd') - qt_dias_w)
and	b.dt_real_entrega is null
and	a.dt_baixa is null
and	cd_local_estoque_w = 0
and	((a.cd_local_entrega is null) or
	((a.cd_local_entrega is not null) and (a.cd_local_entrega not in (
						select	nvl(cd_local_estoque,0)
						from	regra_baixa_ordem_compra
						where	cd_estabelecimento = cd_estabelecimento_p))));/*Para fazer somente dos locais que nao tem regra*/
begin

open C01;
loop
fetch C01 into
	cd_local_estoque_w,
	qt_dias_w;
exit when C01%notfound;
	begin

	open C02;
	loop
	fetch C02 into
		nr_ordem_compra_w;
	exit when C02%notfound;
		begin

		select	nr_seq_motivo_baixa_aut
		into	nr_seq_motivo_baixa_aut_w
		from	parametro_compras
		where	cd_estabelecimento = cd_estabelecimento_p;

		update	ordem_compra
		set	dt_baixa = sysdate,
			nr_seq_motivo_baixa = nr_seq_motivo_baixa_aut_w
		where	nr_ordem_compra = nr_ordem_compra_w;

		/*gnicoselli 28/03/2012 OS 419842*/
		select	ie_envia_email_baixa_oc
		into	ie_param_email_w
		from	parametro_compras
		where	cd_estabelecimento = cd_estabelecimento_p;

		if	(ie_param_email_w = 'S') then
			select	substr(sup_obter_dados_comprador(cd_estabelecimento, cd_comprador, 'E'),1,255)
			into 	ds_email_origem_w
			from	ordem_compra
			where 	nr_ordem_compra = nr_ordem_compra_w;

			begin
			select	b.ds_email
			into	ds_email_destino_w
			from   	ordem_compra a,
				pessoa_juridica_estab b
			where 	a.cd_cgc_fornecedor = b.cd_cgc		
			and	a.nr_ordem_compra = nr_ordem_compra_w
			and	a.cd_estabelecimento = b.cd_estabelecimento;			
			exception
			when others then
				ds_email_destino_w := null;
			end;
			

			select	substr(obter_usuario_pessoa(cd_comprador),1,255)
			into	nm_usuario_w
			from	ordem_compra
			where	nr_ordem_compra = nr_ordem_compra_w;

			ds_assunto_w	:= wheb_mensagem_pck.get_texto(297976);
			
			/*A ordem de compra n�mero #@NR_ORDEM_COMPRA#@ foi baixada pois est� pendente por mais de #@QT_DIAS#@ dias.*/
			ds_mensagem_w	:= wheb_mensagem_pck.get_texto(297977, 'NR_ORDEM_COMPRA='||nr_ordem_compra_w||';QT_DIAS='||qt_dias_w);

			if	(ds_email_destino_w is not null) and
				(ds_email_origem_w is not null) then
				begin
				enviar_email(
					ds_assunto_w,
					ds_mensagem_w,
					ds_email_origem_w,
					ds_email_destino_w,
					nm_usuario_w,
					'A');
				exception
				when others then
					ds_assunto_w := ds_assunto_w;
				end;
			end if;
		end if;
		inserir_historico_ordem_compra(
				nr_ordem_compra_w,
				'S',
				wheb_mensagem_pck.get_texto(297978) ,
				wheb_mensagem_pck.get_texto(297979),
				'Tasy');
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

commit;

end baixa_ordem_compra_regra;
/
