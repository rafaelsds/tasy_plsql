create or replace procedure qt_enviar_ci_exclusao_trat(
			nr_seq_Atendimento_p	number,
			dt_tratamento_p		date,
			nm_usuario_p		varchar2) is 

cd_perfil_w				number(5);
ds_lista_perfil_w		varchar2(4000);
ds_titulo_w				varchar2(255);
ds_comunicado_w			varchar2(4000);
count_w					number(10);
nm_paciente_w			varchar2(60);
ds_pessoa_destino_w		varchar2(4000);
ds_setor_destino_w		varchar2(4000);
qt_regra_w				number(10);
cd_pessoa_fisica_w		varchar2(15);
cd_setor_atendimento_w 	number(10);

cursor c01 is
select	a.cd_perfil
from	funcao_perfil a,
		perfil b
where	a.cd_perfil	= b.cd_perfil
and		b.ie_situacao	= 'A'
and		cd_funcao	= 865;

Cursor C02 is
	select	obter_usuario_pf(cd_pessoa_fisica),
			cd_perfil,
			cd_setor_atendimento
	from	qt_envio_ci_exclusao;
		
begin

select	count(*)
into	qt_regra_w
from	qt_envio_ci_exclusao;

if	(qt_regra_w	> 0) then
	open C02;
	loop
	fetch C02 into	
		cd_pessoa_fisica_w,
		cd_perfil_w,
		cd_setor_atendimento_w;
	exit when C02%notfound;
		begin

		if	(cd_pessoa_fisica_w is not null) then
			ds_pessoa_destino_w	:= substr(ds_pessoa_destino_w ||cd_pessoa_fisica_w||', ',1,4000);
		end if;

		if	(cd_perfil_w is not null) then
			ds_lista_perfil_w	:= substr(ds_lista_perfil_w ||cd_perfil_w||', ',1,4000);
		end if;

		if	(cd_setor_atendimento_w is not null) then
			ds_setor_destino_w	:= substr(ds_setor_destino_w ||cd_setor_atendimento_w||', ',1,4000);
		end if;
		
		end;
	end loop;
	close C02;
else
	open c01;
	loop
	fetch c01 into
		cd_perfil_w;
	exit when c01%notfound;
		begin
		
		if	(ds_lista_perfil_w is not null) then	
			ds_lista_perfil_w := substr(ds_lista_perfil_w || ', ',1,4000);
		end if;	
		
		ds_lista_perfil_w	:= substr(ds_lista_perfil_w || cd_perfil_w,1,4000);	
		
		end;
	end loop;
	close c01;
end if;

if	(ds_lista_perfil_w is not null) or
	(qt_regra_w	> 0)	then
	
	select	max(SUBSTR(OBTER_NOME_PF(a.CD_PESSOA_FISICA),0,60))
	into	nm_paciente_w
	from	pessoa_Fisica a,
			paciente_atendimento b,
			paciente_setor c
	where	a.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and		b.nr_seq_paciente	= c.nr_seq_paciente
	and		b.nr_seq_atendimento	= nr_seq_atendimento_p;

	insert  into comunic_interna(   
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		nr_sequencia,
		ie_gerencial, 
		dt_liberacao,		
		ds_perfil_adicional,
		ds_setor_adicional,
		nm_usuario_destino)
	values (sysdate,
		' ' || wheb_mensagem_pck.get_texto(795686),
		' ' || wheb_mensagem_pck.get_texto(795690) || ' ' || to_char(dt_tratamento_p,'dd/mm/yyyy') || chr(10) ||
		' ' || wheb_mensagem_pck.get_texto(795685) || ' ' || nm_paciente_w || chr(10) ||
		' ' || wheb_mensagem_pck.get_texto(795691) || ' ' || nm_usuario_p || chr(10),
		nm_usuario_p,
		sysdate,
		comunic_interna_seq.nextval,
		'N',
		sysdate,
		ds_lista_perfil_w,
		ds_setor_destino_w,
		ds_pessoa_destino_w);
end if;

commit;

end qt_enviar_ci_exclusao_trat;
/
