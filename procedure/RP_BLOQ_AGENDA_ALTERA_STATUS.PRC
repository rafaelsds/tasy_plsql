create or replace
procedure rp_bloq_agenda_altera_status( 	ie_status_p		number,
				nr_seq_pac_reab_p	number,
				cd_estabelecimento_p	number) is 

cd_pessoa_fisica_w		varchar2(10);		
nr_seq_pac_reab_bloqueio_w	number(10);
ie_status_agenda_susp_w		varchar2(3);
nr_seq_modelo_w			number(10);
nr_seq_ind_w			number(10);
				
Cursor C01 is
	select distinct	b.nr_sequencia
	from	rp_pac_modelo_agendamento a,
		rp_pac_modelo_agend_item b
	where	a.dt_fim_tratamento is null
	and	a.ie_situacao <> 'I'
	and	a.nr_seq_pac_reab   = nr_seq_pac_reab_p
	and	b.nr_seq_modelo_pac = a.nr_sequencia
	order by 1;
	
Cursor C02 is
	select	nr_sequencia
	from	rp_pac_agend_individual
	where	dt_fim_tratamento is null
	and	nr_seq_pac_reab = nr_seq_pac_reab_p 
	order by 1;
begin

select	max(nr_seq_pac_reab_bloqueio),
	max(ie_status_agenda_susp) 
into	nr_seq_pac_reab_bloqueio_w,
	ie_status_agenda_susp_w
from	rp_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

if	(nr_seq_pac_reab_bloqueio_w = ie_status_p) and
	(ie_status_agenda_susp_w is not null) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_modelo_w;
	exit when C01%notfound;
		begin
		update	agenda_consulta
		set	ie_status_agenda 	= ie_status_agenda_susp_w
		where	nr_seq_rp_mod_item 	= nr_seq_modelo_w
		and	ie_status_agenda	= 'N'
		and	trunc(dt_agenda)	>= trunc(sysdate);	
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into	
		nr_seq_ind_w;
	exit when C02%notfound;
		begin
		update	agenda_consulta
		set	ie_status_agenda 	= ie_status_agenda_susp_w
		where	nr_seq_rp_item_ind 	= nr_seq_ind_w
		and	ie_status_agenda	= 'N'
		and	trunc(dt_agenda)	>= trunc(sysdate);
		end;
	end loop;
	close C02;

end if;

commit;

end rp_bloq_agenda_altera_status;
/