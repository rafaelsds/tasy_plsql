create or replace
procedure desvincular_cod_barras(	nr_titulo_p	number,
					nr_sequencia_p	number,
					nm_usuario_p	varchar2) is 

begin
	update	titulo_pagar 
	set 	nr_bloqueto 	= null,
			ie_bloqueto 	= 'N'
	where 	nr_titulo 	= nr_titulo_p;
	
	update 	banco_escrit_barras 
	set 	nr_titulo = null
	where 	nr_sequencia 	= nr_sequencia_p;

commit;

end desvincular_cod_barras;
/