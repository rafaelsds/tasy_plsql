create or replace procedure obter_dados_futuro_change_js(	ie_asa_estado_paciente_p	varchar2,
					nr_atend_origem_p		number,
					cd_medico_p			number,
					ds_status_asa_pac_p		out number,
					ds_texto_p			out varchar2,
					cd_pessoa_fisica_p		out number,
					nr_crm_p			out varchar2) is

ds_status_asa_pac_w	number(10,0);
ds_texto_w		varchar2(100);
cd_pessoa_fisica_w	number(10,0);
nr_crm_w		varchar2(25);

begin

select	nvl(max(nr_seq_status),0)
into	ds_status_asa_pac_w
from	regra_status_atend_futuro
where 	ie_asa_estado_paciente = ie_asa_estado_paciente_p;

ds_texto_w	:= obter_texto_tasy(61584, wheb_usuario_pck.get_nr_seq_idioma);

select 	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from 	atendimento_paciente
where 	nr_atendimento = nr_atend_origem_p;

select 	nvl(max(substr(obter_crm_medico(cd_pessoa_fisica), 1, 255)),null)
into	nr_crm_w
from 	medico
where 	cd_pessoa_fisica = cd_medico_p;

ds_status_asa_pac_p	:= ds_status_asa_pac_w;
ds_texto_p		:= ds_texto_w;
cd_pessoa_fisica_p	:= cd_pessoa_fisica_w;
nr_crm_p		:= nr_crm_w;

end obter_dados_futuro_change_js;
/