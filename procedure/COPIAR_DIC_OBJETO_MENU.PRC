create or replace
procedure copiar_dic_objeto_menu (
		cd_funcao_p	number,
		nr_seq_objeto_p	number,
		nm_usuario_p	varchar2,
		nr_new_obj_p out number) is
		
nr_seq_objeto_w	number(10,0);
		
begin
if	(cd_funcao_p is not null) and
	(nr_seq_objeto_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	dic_objeto_seq.nextval
	into	nr_seq_objeto_w
	from	dual;
	
	insert into dic_objeto (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_objeto,
		nm_objeto,
		cd_funcao,
		nr_seq_menu_sup,
		NR_SEQ_OBJ_ORIGEM)
	select	nr_seq_objeto_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_tipo_objeto,
		nm_objeto,
		cd_funcao_p,
		nr_seq_menu_sup,
		nr_sequencia
	from	dic_objeto
	where	nr_sequencia = nr_seq_objeto_p;
	
	insert into dic_objeto (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_objeto,
		nm_objeto,
		ds_texto,
		nr_seq_obj_sup,
		ds_informacao,
		nr_seq_apres,
		ie_atalho,
		ie_ctrl,
		ie_shift,
		ie_alt,
		nr_seq_mi_sup,
		ie_objeto_nulo,
		ie_tipo_obj_menu,
		cd_funcao,
		cd_exp_texto,
		ie_versao_objeto,
		ie_configuravel,
		ie_visible,
		cd_exp_informacao,
		ie_situacao,
		vl_default,
		NR_SEQ_OBJ_ORIGEM)
	select	dic_objeto_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_tipo_objeto,
		nm_objeto,
		ds_texto,
		nr_seq_objeto_w,
		ds_informacao,
		nr_seq_apres,
		ie_atalho,
		ie_ctrl,
		ie_shift,
		ie_alt,
		nr_seq_mi_sup,
		ie_objeto_nulo,
		ie_tipo_obj_menu,
		cd_funcao_p,
		cd_exp_texto,
		ie_versao_objeto,
		ie_configuravel,
		ie_visible,
		cd_exp_informacao,
		ie_situacao,
		vl_default,
		nr_sequencia
	from	dic_objeto
	where	nr_seq_obj_sup = nr_seq_objeto_p;
	end;
end if;
nr_new_obj_p := nr_seq_objeto_w;
commit;
end copiar_dic_objeto_menu;
/
