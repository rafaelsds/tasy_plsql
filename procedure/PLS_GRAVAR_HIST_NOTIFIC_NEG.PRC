create or replace procedure pls_gravar_hist_notific_neg
			(	nr_seq_notificacao_p	number,
				ds_historico_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

begin

insert into pls_notific_hist_atend_neg
	(nr_sequencia, nr_seq_notificacao, nm_usuario_nrec,
	nm_usuario, dt_historico, dt_atualizacao_nrec,
	dt_atualizacao, ds_historico, cd_estabelecimento)
values	(pls_notific_hist_atend_neg_seq.nextval, nr_seq_notificacao_p, nm_usuario_p,
	nm_usuario_p, sysdate, sysdate,
	sysdate, substr(ds_historico_p,1,4000), cd_estabelecimento_p);

commit;

end pls_gravar_hist_notific_neg;
/
