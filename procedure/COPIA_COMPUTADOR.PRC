CREATE OR REPLACE
procedure copia_computador(	nr_seq_origem_p		Number,
				nr_seq_destino_p	Number,
				nm_usuario_p		Varchar2) IS

BEGIN


insert into perfil_computador
	(NR_SEQUENCIA       ,    
	CD_PERFIL            ,  
	NR_SEQ_COMPUTADOR     , 
	DT_ATUALIZACAO         ,
	NM_USUARIO             )
	select	perfil_computador_seq.nextval,
		a.cd_perfil,
		nr_seq_destino_p,
		sysdate,
		nm_usuario_p
	from	perfil_computador a
	where	a.nr_seq_computador = nr_seq_origem_p
	and not exists(	select	1
			from	perfil_computador c
			where	c.nr_seq_computador = nr_seq_destino_p
			and	a.cd_perfil = c.cd_perfil);
commit;

END copia_computador;
/