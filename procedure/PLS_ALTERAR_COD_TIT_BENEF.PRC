create or replace
procedure pls_alterar_cod_tit_benef
			(	nr_seq_segurado_p	number,
				ie_titularidade_p	varchar2,
				ie_consistir_p		varchar2,
				nm_usuario_p		varchar2) is

ie_titularidade_ant_w	pls_segurado.ie_titularidade%type;
qt_registro_w		number(10);
cd_matricula_familia_w	pls_segurado.cd_matricula_familia%type;
nr_seq_contrato_w	pls_segurado.nr_seq_contrato%type;

begin

select	max(ie_titularidade),
	max(cd_matricula_familia),
	max(nr_seq_contrato)
into	ie_titularidade_ant_w,
	cd_matricula_familia_w,
	nr_seq_contrato_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

if (ie_consistir_p = 'S') then
	select 	count(1)
	into	qt_registro_w
	from	pls_segurado
	where	cd_matricula_familia = cd_matricula_familia_w
	and	nr_seq_contrato = nr_seq_contrato_w
	and	nr_sequencia <> nr_seq_segurado_p
	and	ie_titularidade = ie_titularidade_p;

	if (qt_registro_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort('N�o � permitido c�digo de titularidade duplicado para benefici�rios que tenham o mesmo c�digo de matr�cula familiar.');
	end if;
end if;

if	(ie_titularidade_p <> ie_titularidade_ant_w) then
	update	pls_segurado
	set	ie_titularidade	= ie_titularidade_p
	where	nr_sequencia	= nr_seq_segurado_p;
			
	pls_gerar_segurado_historico(	nr_seq_segurado_p, '87', sysdate, 'C�digo anterior: '||ie_titularidade_ant_w||' - C�digo alterado: '||ie_titularidade_p,
					'pls_alterar_cod_tit_benef', null, null, null,
					null, sysdate, null, null,
					null, null, null, null,
					nm_usuario_p, 'N');			
end if;

commit;

end pls_alterar_cod_tit_benef;
/
