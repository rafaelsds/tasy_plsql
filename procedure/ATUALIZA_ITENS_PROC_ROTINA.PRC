create or replace
procedure atualiza_itens_proc_rotina(
							nr_sequencia_p			number,
							nm_usuario_p			varchar2,
							ie_item_atualiza_p		varchar2,
							ds_valor_p				varchar2,
							qt_valor_p				number) is 

begin
if (ie_item_atualiza_p = 'J') then
	update	w_proc_rotina_esp_t
	set		ds_justificativa = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif (ie_item_atualiza_p = 'CID') then
	update	w_proc_rotina_esp_t
	set		cd_doenca_cid = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif (ie_item_atualiza_p = 'IND') then
	update	w_proc_rotina_esp_t
	set		ds_indicacao = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif (ie_item_atualiza_p = 'RES') then
	update	w_proc_rotina_esp_t
	set		ds_forma = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif (ie_item_atualiza_p = 'LADO') then
	update	w_proc_rotina_esp_t
	set		ie_lado = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif (ie_item_atualiza_p = 'TOPO') then
	update	w_proc_rotina_esp_t
	set		nr_seq_topografia = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'EXA') then
	update	w_proc_rotina_esp_t
	set		nr_seq_exame = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'PINT') then
	update	w_proc_rotina_esp_t
	set		nr_seq_proc_interno = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'ORIG') then
	update	w_proc_rotina_esp_t
	set		ie_origem_proced = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'PROC') then
	update	w_proc_rotina_esp_t
	set		cd_procedimento = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'MED') then
	update	w_proc_rotina_esp_t
	set		cd_medico_exec = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'ANE') then
	update	w_proc_rotina_esp_t
	set		ie_anestesia = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'URG') then
	update	w_proc_rotina_esp_t
	set		ie_urgencia = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'KIT') then
	update	w_proc_rotina_esp_t
	set		ds_lista_kit = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'INT') then
	update	w_proc_rotina_esp_t
	set		cd_intervalo = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'OBS') then
	update	w_proc_rotina_esp_t
	set		ds_observacao = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'CON') then
	update	w_proc_rotina_esp_t
	set		nr_seq_contraste = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'COND') then
	update	w_proc_rotina_esp_t
	set		nr_seq_condicao_exame = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'MAEX') then
	update	w_proc_rotina_esp_t
	set		cd_material_exame = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;	
elsif  (ie_item_atualiza_p = 'LMAT') then
	update	w_proc_rotina_esp_t
	set		ds_lista_mat_exame = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'PGLI') then
	update	w_proc_rotina_esp_t
	set		nr_seq_prot_glic = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'FRASE') then
	update	w_proc_rotina_esp_t
	set		ds_frase = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'TIPO') then
	update	w_proc_rotina_esp_t
	set		ie_forma_exame = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'DMAT') then
	update	w_proc_rotina_esp_t
	set		ds_material_especial = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'SETOR') then
	update	w_proc_rotina_esp_t
	set		cd_setor_atendimento = qt_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'LEITO') then
	update	w_proc_rotina_esp_t
	set		ie_beira_leito = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'PMAT') then
	update	w_proc_rotina_esp_t
	set		ds_lista_material = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'LPROC') then
	update	w_proc_rotina_esp_t
	set		ds_lista_proced = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
elsif  (ie_item_atualiza_p = 'CGC') then
	update	w_proc_rotina_esp_t
	set		cd_cgc_externo = ds_valor_p
	where	nr_sequencia = nr_sequencia_p
	and		nm_usuario = nm_usuario_p;
	commit;
end if;
end atualiza_itens_proc_rotina;
/