create or replace
procedure Lab_verifica_conta_definitiva(nr_prescricao_p		Number,
					nm_usuario_p		Varchar2) is 
					
qt_conta_prov_w		number(5);
qt_conta_def_w		number(5);
nr_atendimento_w	number(10);

begin	
			
select	max(nr_atendimento)
into	nr_atendimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;
			
select	count(*)
into	qt_conta_prov_w
from	conta_paciente
where	nr_atendimento		= nr_atendimento_w
and	ie_status_acerto	= 1;

select	count(*)
into	qt_conta_def_w
from	conta_paciente
where	nr_atendimento		= nr_atendimento_w
and	ie_status_acerto	= 2;

if	(qt_conta_prov_w = 0) and (qt_conta_def_w > 0) then
	--(-20011, 'A conta do atendimento est� com status definitivo. O procedimento n�o pode ser executado!');
	wheb_mensagem_pck.exibir_mensagem_abort(263994);
end if;

end Lab_verifica_conta_definitiva;
/
