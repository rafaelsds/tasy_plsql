create or replace
procedure pls_proposta_dados_contrato_js(
		nr_seq_proposta_p	number,
		nr_seq_contrato_p	number,
		nm_usuario_p	varchar2) is 

nr_seq_contrato_w	number(10);

begin

nr_seq_contrato_w	:= pls_obter_seq_contrato(nr_seq_contrato_p);

pls_proposta_dados_contrato(
	nr_seq_proposta_p,
	nr_seq_contrato_w,
	'S',
	nm_usuario_p);

end pls_proposta_dados_contrato_js;
/
