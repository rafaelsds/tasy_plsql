create or replace
procedure atualizar_status_equip_HC(
		nr_seq_equipamento_p	number,
		nm_usuario_p		varchar2,
		ie_tipo_status_p	varchar2) is 

nr_status_w	hc_parametro.nr_status_ini_equip%type;
		
begin

select	max(decode(ie_tipo_status_p, 'I', nr_status_ini_equip, nr_status_fim_equip))
into	nr_status_w
from	hc_parametro;

if(nr_status_w is not null) then

	update 	man_equipamento
	set	nr_seq_status = nr_status_w,
		nm_usuario    = nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia = nr_seq_equipamento_p;

end if;

end atualizar_status_equip_HC;
/