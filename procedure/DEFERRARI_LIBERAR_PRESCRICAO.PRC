create or replace
procedure deferrari_liberar_prescricao(	nr_prescricao_p		number,
					cd_erro_p		out number,
					ds_erro_p		out varchar2) is 

/*
0 - SUCESSO
1 - Campo n�o informado
	1.1 - Prescri��o n�o informada	
2 - Registro inexistente
	2.1 - Prescri��o n�o encontrada
	2.2 - Atendimento n�o encontrado
3 - Erro ao liberar a prescri��o
	3.1 - Exce��o na procedure liberar_prescricao
	3.2 - Inconsist�ncia da prescri��o
*/

ie_existe_prescricao_w		varchar2(1);
nr_atendimento_w		number(10);
ds_erro_lib_prescr_w		varchar2(255);

begin
cd_erro_p	:= 0;

select	decode(count(*),0,'N','S')
into	ie_existe_prescricao_w
from	prescr_medica a
where	a.nr_prescricao = nr_prescricao_p;

select	max(a.nr_atendimento)
into	nr_atendimento_w
from	prescr_medica a
where	a.nr_prescricao = nr_prescricao_p;

if	(nr_prescricao_p is null) then
	cd_erro_p	:= 1.1;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277599,null);
elsif	(ie_existe_prescricao_w = 'N') then
	cd_erro_p	:= 2.1;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277254,null);
elsif	(nr_atendimento_w is null) then
	cd_erro_p	:= 2.2;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278155,null);
end if;

if 	(cd_erro_p = 0) then
	begin
	liberar_prescricao(	nr_prescricao_p,
				nr_atendimento_w,
				'S',
				obter_perfil_ativo,
				'DEFERRARI',
				'S',
				ds_erro_lib_prescr_w);
	exception
	when others then
		cd_erro_p	:= 3.1;
		ds_erro_p	:= substr(WHEB_MENSAGEM_PCK.get_texto(278157,null)||sqlerrm,1,2000);
	end;	
	
	if	(ds_erro_lib_prescr_w is not null) then
		cd_erro_p	:= 3.2;
		ds_erro_p	:= substr(WHEB_MENSAGEM_PCK.get_texto(278158,null)||ds_erro_lib_prescr_w,1,2000);
	end if;
end if;

commit;

end deferrari_liberar_prescricao;
/