create or replace
procedure gerar_procedimento_ama(
		nr_sequencia_p		number,
		cd_pessoa_fisica_p	varchar2,
		cd_perfil_p		number,
		cd_funcao_p		number,
		nm_usuario_p		varchar2,
		ie_proced_prescr_p out	varchar2) is

ie_proced_prescr_w	varchar2(1) := 'N';
		
begin
ie_proced_prescr_p := obter_se_proc_exec_ama(nr_sequencia_p);

if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) and
	(cd_pessoa_fisica_p is not null) and
	(cd_perfil_p is not null) and
	(cd_funcao_p is not null) then
	begin

	gerar_proced_ama(
		nr_sequencia_p,
		cd_pessoa_fisica_p,
		nm_usuario_p,
		cd_perfil_p,
		cd_funcao_p);	
	end;
end if;

commit;
end gerar_procedimento_ama;
/
