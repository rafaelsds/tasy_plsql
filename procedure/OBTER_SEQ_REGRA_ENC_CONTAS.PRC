create or replace
procedure obter_seq_regra_enc_contas(	ie_tipo_pessoa_p	in	Varchar2,
					nr_seq_regra_p		out	Number) is 

Cursor C01 is
	select	a.nr_sequencia
	from	regra_receb_enc_contas	a
	where	a.ie_tipo_pessoa	= ie_tipo_pessoa_p
	order by
		a.nr_sequencia desc;		
begin
open C01;
loop
fetch C01 into	
	nr_seq_regra_p;
exit when C01%notfound;
	begin
	null;
	end;
end loop;
close C01;

commit;

end obter_seq_regra_enc_contas;
/