create or replace
procedure consiste_atend_agenda_paciente(
		cd_agenda_p		number,
		nr_atendimento_p	number) is

nr_atend_alta_w		number(10,0);
ie_tipo_atendimento_w	number(3,0);
ie_tipo_atend_agenda_w	varchar2(1);
ie_restringe_atend_w	varchar2(1);

begin

Obter_Param_Usuario(871,179,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_restringe_atend_w);

if	(nr_atendimento_p is not null) then
	begin
	select	nvl(nr_atend_alta, 0),
		ie_tipo_atendimento
	into	nr_atend_alta_w,
		ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	if	(ie_tipo_atendimento_w is not null)
	 then		
		begin
		if	(ie_restringe_atend_w = 'S') and 
			(nr_atend_alta_w = 0) and
			(ie_tipo_atendimento_w = 3) then
			begin
			wheb_mensagem_pck.Exibir_Mensagem_Abort(83357, wheb_usuario_pck.get_nr_seq_idioma);
			end;
		end if;
		if	(cd_agenda_p is not null) then
			begin
			ie_tipo_atend_agenda_w := obter_se_tipo_aten_agenda(ie_tipo_atendimento_w, cd_agenda_p);
			if	(ie_tipo_atend_agenda_w = 'S') then
				begin
				wheb_mensagem_pck.Exibir_Mensagem_Abort(83360, wheb_usuario_pck.get_nr_seq_idioma);
				end;
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;
end consiste_atend_agenda_paciente;
/