create or replace
procedure cancelar_nf_titulo_protocolo(	nr_seq_protocolo_p	number,
					nm_usuario_p		varchar2,
					ie_desvincular_p		varchar2,
					ie_desvincular_NF_p	varchar2,
					ie_desvincular_conta_p	varchar2,
					nr_seq_mot_cancel_p	varchar2,
					ds_motivo_estorno_p	varchar2) is

nr_titulo_w		number(10,0);
nr_seq_nf_w		number(10,0);
nr_nota_fiscal_w		Varchar2(255);
qt_nf_w			number(05,0);
qt_tit_w			number(05,0);
dt_atualizacao_estoque_w	date;
cd_estabelecimento_w	number(15);
ie_forma_canc_tit_prot_w	varchar2(4);
nr_seq_titulo_w		number(15);
cd_tipo_recebimento_w	number(15);
cd_moeda_w		number(15);
vl_saldo_titulo_w		number(15,2);
nr_seq_trans_fin_w		number(15);

ds_mensagem_w		varchar2(255)	:= null;
nr_titulo_contab_w		number(10);
ds_titulos_contab_w	varchar2(4000);
nr_titulo_pagar_w       titulo_pagar.nr_titulo%type;
ie_tit_tributo_paciente_w	varchar2(1);
ds_erro_w			varchar2(4000);
qt_reg_w			number(10);
cd_convenio_w		protocolo_convenio.cd_convenio%type;

cursor c01 is
select	/*+ index (a titrece_proconv_fk_i) */
	nr_titulo,
	cd_moeda,
	vl_saldo_titulo
from	titulo_receber a
where	(nr_seq_protocolo_p <> 0) 
and	(nr_seq_protocolo = nr_seq_protocolo_p)
order by 1;

cursor c02 is
select	nr_sequencia,
	nr_nota_fiscal
from	nota_fiscal
where	nr_seq_protocolo = nr_seq_protocolo_p
and	ie_situacao in ('1','8')
order by 1;

cursor c03 is
select tp.nr_titulo 
from   titulo_pagar tp
where  tp.nr_seq_nota_fiscal = nr_seq_nf_w;

begin

select	count(*)
into	qt_tit_w
from	titulo_receber a
where	(nr_seq_protocolo_p <> 0) 
and	(nr_seq_protocolo = nr_seq_protocolo_p)
order by 1;

select	count(*)
into	qt_nf_w
from	nota_fiscal
where	nr_seq_protocolo = nr_seq_protocolo_p
order by 1;

select	max(a.cd_estabelecimento),
		max(a.cd_convenio)
into	cd_estabelecimento_w,
		cd_convenio_w
from	protocolo_convenio a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p;

Select	nvl(max(ie_forma_canc_tit_prot), 'A')
into	ie_forma_canc_tit_prot_w
from	parametro_contas_receber
where	cd_estabelecimento = cd_estabelecimento_w;

select	nvl(min(cd_tipo_recebimento),4)
into	cd_tipo_recebimento_w
from	tipo_recebimento
where	ie_tipo_consistencia = 4
and     ((cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento is null));

select	max(nr_seq_trans_fin)
into	nr_seq_trans_fin_w
from	tipo_recebimento
where	cd_tipo_recebimento	= cd_tipo_recebimento_w;

if	(qt_tit_w = 0) and
	(qt_nf_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(182360);
	/*r.aise_application_error(-20011,'Este protocolo nao possui titulos ou notas a serem canceladas.');*/
end if;

ds_titulos_contab_w	:= null;

open c01;
loop
	fetch c01 into
		nr_titulo_w,
		cd_moeda_w,
		vl_saldo_titulo_w;
	exit when c01%notfound;
	if 	(ie_forma_canc_tit_prot_w = 'A') then
		Cancelar_Titulo_Receber(nr_titulo_w, nm_usuario_p, 'N',sysdate);
GRAVAR_LOG_TASY(1119,'Passou 01  - nr_titulo_w' || nr_titulo_w, 'Tasy');		
	else
		Select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_titulo_w
		from	titulo_receber_liq
		where	nr_titulo = nr_titulo_w;
		
		/*OS 1608928 - Verificar se existem baixas com adiantamentos que nao foram estornadas*/
		select	count(*)
		into	qt_reg_w
		from	titulo_receber_liq a
		where	a.nr_titulo = nr_titulo_w
		and		a.nr_adiantamento is not null
		and    a.nr_seq_liq_origem is null
		and not exists ( select 1
						 from titulo_receber_liq x
						where x.nr_titulo = a.nr_titulo
						and  x.nr_seq_liq_origem = a. nr_sequencia);
		
		if (qt_reg_w > 0) then
			--Existem baixas com adiantamento no titulo que nao foram estornadas.
			wheb_mensagem_pck.exibir_mensagem_abort(1040217);
		end if;

		insert into Titulo_Receber_liq
			(nr_titulo,
			nr_sequencia, 		
			dt_recebimento, 
			vl_recebido, 		
			vl_descontos,			
			vl_juros, 
			vl_multa, 		
			cd_moeda, 			
			dt_atualizacao, 
			nm_usuario,		
			cd_tipo_recebimento, 	
			ie_acao, 
			vl_rec_maior, 		
			vl_glosa,		
			ie_lib_caixa,
			nr_seq_trans_fin,
			nr_lote_contab_antecip,
			nr_lote_contab_pro_rata,
			nr_lote_contabil)
		values (nr_titulo_w, 		
			nr_seq_titulo_w, 		
			sysdate, 
			vl_saldo_titulo_w,		
			0,				
			0,
			0, 			
			cd_moeda_w, 			
			sysdate, 
			nm_usuario_p, 	
			cd_tipo_recebimento_w,	
			'I', 
			0, 			
			0,				
			'S',
			nr_seq_trans_fin_w,
			0,
			0,
			0);

			nr_titulo_contab_w	:= null;
			
			gerar_titulo_rec_liq_cc(cd_estabelecimento_w, null, nm_usuario_p, nr_titulo_w, nr_seq_titulo_w);
			pls_gerar_tit_rec_liq_mens(nr_titulo_w,	nr_seq_titulo_w, nm_usuario_p,nr_titulo_contab_w);
			atualizar_saldo_tit_rec(nr_titulo_w,nm_usuario_p);

			update	titulo_receber
			set 	ie_situacao		= '3'
			where	nr_titulo			= nr_titulo_w
			and	vl_saldo_titulo		= 0
			and	ie_situacao		in ('2', '4');
		end if;
		
		
		if	(nr_titulo_contab_w is not null) then
			ds_titulos_contab_w	:= substr(ds_titulos_contab_w || nr_titulo_contab_w || ', ',1,4000); 
		end if;

end loop;
close c01;

	if	(ds_titulos_contab_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(236517, 'NR_TITULO=' || ds_titulos_contab_w);
	end if;


open c02;
loop
	fetch c02 into
		nr_seq_nf_w,
		nr_nota_fiscal_w;
	exit when c02%notfound;

	begin

	select	max(dt_atualizacao_estoque)
	into	dt_atualizacao_estoque_w
	from	nota_fiscal
	where	nr_sequencia = nr_seq_nf_w;

	/*Anderson 15/05/2007 OS56769 - Para cancelar as notas que nao estejam calculadas, para as calculadas estornar*/
	if	(dt_atualizacao_estoque_w is not null) then
		estornar_nota_fiscal(nr_seq_nf_w,nm_usuario_p);
	else
		cancelar_nf(nr_seq_nf_w,nm_usuario_p);
	end if;
	
	grava_motivo_estorno_nf(nr_seq_nf_w, nr_seq_mot_cancel_p, ds_motivo_estorno_p);	

	end;
end loop;
close c02;


        begin 
        obter_param_usuario(-80,111, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_tit_tributo_paciente_w);
        if (nvl(ie_tit_tributo_paciente_w,'N') = 'S') then
	   if (nr_seq_nf_w > 0) Then
	       open c03;
	       loop
	       fetch c03 into	
			nr_titulo_pagar_w;
GRAVAR_LOG_TASY(1119,'parametro S' || nr_titulo_pagar_w, 'Tasy');				
		exit when c03%notfound;
			if (nr_titulo_pagar_w > 0) then			
				cancelar_titulo_pagar(nr_titulo_pagar_w, nm_usuario_p, sysdate);
				atualizar_Saldo_Tit_Pagar(nr_titulo_pagar_w, nm_usuario_p);		
			end if;	 
	       end loop;
	       close c03;
	   end if; 	 
	end if; 
	exception when others then
	 ds_erro_w := substr(sqlerrm(sqlcode),1,1000);
	 gravar_log_tasy(6789,wheb_mensagem_pck.get_texto(794210,'DS_ERRO_W='||ds_erro_w), nm_usuario_p);
	end;
	
if	(ie_desvincular_p = 'S') then
	desvincular_titulo_protocolo(nr_seq_protocolo_p,nm_usuario_p,ds_mensagem_w);
end if;

if	(ie_desvincular_NF_p = 'S') then
	Desvincular_nf_protocolo(nr_seq_protocolo_p,nm_usuario_p);
elsif	(ie_desvincular_nf_p = 'C') then
	desvincular_nf_conta_prot(nr_seq_protocolo_p, nm_usuario_p);
end if;

if	(ctb_online_pck.get_modo_lote(6,cd_estabelecimento_w) = 'S') and 
	(ctb_online_pck.get_geracao_lote_receita(cd_convenio_w, cd_estabelecimento_w, nm_usuario_p, obter_tipo_convenio(cd_convenio_w)) in ('ENF', 'ETR')) then
		ctb_contab_onl_lote_receita(nr_seq_protocolo_p, null, nm_usuario_p, 2, 'N');
end if;

end cancelar_nf_titulo_protocolo;
/
