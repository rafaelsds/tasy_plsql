create or replace
procedure Vincular_MatMed_Cir_Proc_Adic
		(nr_seq_matpaci_p	number,
		nr_seq_propaci_p	number,
		nm_usuario_p		varchar2) is


dt_procedimento_w	date;

begin

select	max(dt_procedimento)
into	dt_procedimento_w
from	procedimento_paciente
where	nr_sequencia	= nr_seq_propaci_p;

update	material_atend_paciente
set	dt_atendimento		= dt_procedimento_w,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p,
	nr_seq_proc_princ	= nr_seq_propaci_p
where	nr_sequencia		= nr_seq_matpaci_p;

commit;

end Vincular_MatMed_Cir_Proc_Adic;
/