create or replace
procedure vincular_cheque_bordero_tit(	nr_seq_cheque_p	number,
					nr_bordero_p	number,
					nr_titulo_p	number,
					nm_usuario_p	varchar2) is

cd_agencia_bancaria_w	varchar2(255);
vl_cheques_w		number(15,2);
vl_cheque_w		number(15,2);
vl_titulo_w		number(15,2);
cd_estabelecimento_w	number(10,0);
vl_saldo_titulo_w	number(15,2);
ie_soma_cheque_maior_w	varchar2(255);
ie_valida_vl_titulo_w	varchar2(1);
/* Projeto Multimoeda - Vari�veis */
cd_moeda_cheque_w	number(5);
cd_moeda_bordero_w	number(5);

begin

if	(nr_seq_cheque_p is not null) and
	((nr_titulo_p is not null) or (nr_bordero_p is not null)) then

	select	cd_agencia_bancaria,
		vl_cheque,
		cd_estabelecimento,
		cd_moeda
	into	cd_agencia_bancaria_w,
		vl_cheque_w,
		cd_estabelecimento_w,
		cd_moeda_cheque_w
	from	cheque
	where	nr_sequencia	= nr_seq_cheque_p;

	obter_param_usuario(127,38,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_soma_cheque_maior_w);
	obter_param_usuario(127,50,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_valida_vl_titulo_w);
	if	(ie_soma_cheque_maior_w	= 'N') and (nvl(nr_titulo_p,0) > 0) then

		select	vl_titulo,
			vl_saldo_titulo
		into	vl_titulo_w,
			vl_saldo_titulo_w
		from	titulo_pagar
		where	nr_titulo	= nr_titulo_p;

		select	nvl(sum(c.vl_cheque),0)
		into	vl_cheques_w
		from	cheque c,
			cheque_bordero_titulo b
		where	c.nr_sequencia 	= b.nr_seq_cheque
		and	b.nr_titulo	= nr_titulo_p
		and	c.dt_cancelamento is null;

		/* Valida pelo saldo do t�tulo quando indicado no par�metro 50 da digita��o de cheques */
		if (nvl(ie_valida_vl_titulo_w,'T') = 'S') then
			if	((vl_cheques_w + nvl(vl_cheque_w,0)) > vl_saldo_titulo_w) then
				-- A soma dos cheques vinculados ao t�tulo #@NR_TITULO#@ supera o saldo do t�tulo! Par�metro [38]
				wheb_mensagem_pck.exibir_mensagem_abort(361924, 'NR_TITULO_P=' || nr_titulo_p);
			end if;
		else
			if	((vl_cheques_w + nvl(vl_cheque_w,0)) > vl_titulo_w) then
				-- A soma dos cheques vinculados ao t�tulo #@NR_TITULO#@ supera o valor do t�tulo! Par�metro [38]
				wheb_mensagem_pck.exibir_mensagem_abort(266898, 'NR_TITULO=' || nr_titulo_p);
			end if;
		end if;
	end if;
	
	/* Projeto Multimoeda - Busca a moeda do border� e verifica se a moeda do cheque e do border� s�o diferentes,
			caso positivo n�o deixa vincular o cheque ao border�.*/
	if (nr_bordero_p is not null) then
		select max(cd_moeda)
		into cd_moeda_bordero_w
		from bordero_pagamento
		where nr_bordero = nr_bordero_p;
		
		if (cd_moeda_cheque_w is not null) and (cd_moeda_bordero_w is not null)
			and (cd_moeda_cheque_w <> cd_moeda_bordero_w) then
			-- A moeda do cheque deve ser a mesma do border�.
			wheb_mensagem_pck.exibir_mensagem_abort(343694);
		end if;
	end if;

	if	(cd_agencia_bancaria_w is not null) then

		insert	into	cheque_bordero_titulo
			(nr_sequencia,
			cd_banco,
			cd_agencia_bancaria,
			nr_cheque,
			dt_atualizacao,
			nm_usuario,
			nr_bordero,
			nr_titulo,
			nr_seq_cheque)
		select	cheque_bordero_titulo_seq.nextval,
			cd_banco,
			cd_agencia_bancaria,
			nr_cheque,
			sysdate,
			nm_usuario_p,
			nr_bordero_p,
			nr_titulo_p,
			nr_sequencia
		from	cheque
		where	nr_sequencia	= nr_seq_cheque_p;
	end if;
end if;

commit;

end vincular_cheque_bordero_tit;
/
