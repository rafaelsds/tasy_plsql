CREATE OR REPLACE PROCEDURE transfereOrdensAcordoHTML5(
    nr_seq_acordo_p DESENV_ACORDO_OS.NR_SEQ_ACORDO%type,
    nm_usuario_p VARCHAR2)
IS
  nr_seq_desenv_acordo_os DESENV_ACORDO_OS.nr_sequencia%type;
  CURSOR c1(nr_seq_acordo_v DESENV_ACORDO_OS.NR_SEQ_ACORDO%type)
  IS
    SELECT NR_SEQUENCIA
    FROM DESENV_ACORDO_OS
    WHERE NR_SEQ_ACORDO = nr_seq_acordo_v;
BEGIN
  OPEN c1(nr_seq_acordo_p);
  LOOP
    FETCH c1 INTO nr_seq_desenv_acordo_os;
    ATUALIZA_DESENV_ACORDO_OS(nr_seq_acordo_p,nr_seq_desenv_acordo_os,nm_usuario_p);
    EXIT
  WHEN c1%notfound;
  END LOOP;
END;
/