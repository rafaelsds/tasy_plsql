create or replace
procedure atualizar_hora_aplic_prescr(	nr_prescricao_p	number,
					nr_sequencia_p	number,			
					nm_usuario_p	Varchar2) is 

begin

if	((nr_prescricao_p > 0) and (nr_sequencia_p > 0)) then
	update	prescr_material
	set	qt_min_aplicacao	= null,
		qt_hora_aplicacao	= null
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		= nr_sequencia_p
	and	ie_aplic_bolus		= 'N'
	and	ie_aplic_lenta		= 'N';

	commit;
end if;

end atualizar_hora_aplic_prescr;
/