create or replace 
procedure liberar_pedido_cirurgico(	nr_sequencia_p		number,
					ie_type         	number,
					ds_justificativa_p	varchar2,
                                        nm_usuario_p	   	varchar2) is 
		
nr_seq_lista_espera_w  		agenda_lista_espera.nr_sequencia%type;	
nr_seq_age_lista_w 		pedido_age_cirurgia.nr_seq_age_lista%type;
qt_proc_sec  number (10);
begin

if(ie_type = 1)then
	
	
	select  nvl(nr_seq_age_lista,0)
	into	nr_seq_age_lista_w
	from    pedido_age_cirurgia
	where   nr_sequencia = nr_sequencia_p;
	
	if (nr_seq_age_lista_w = 0) then

		select	agenda_lista_espera_seq.nextval
		into	nr_seq_lista_espera_w
		from	dual;

		insert into agenda_lista_espera ( 
			nr_sequencia,
			ie_status_espera,
			nm_usuario_agenda,
			dt_agendamento,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_medico_exec,
			cd_departamento_medico,
			nr_seq_proc_interno,
			cd_pessoa_fisica,
			ie_origem_proced,
			cd_procedimento,
			ds_observacao,
			dt_desejada,
			nr_minuto_duracao,
			nr_seq_classif_agenda,
			ds_cirurgia,
			ie_anestesia,
			ie_lado,
			cd_cid,
			cd_departamento_medico_exec
			)
			select	 nr_seq_lista_espera_w,
				'P',
				 nm_usuario_p,
				 sysdate,
				 nm_usuario,
				 dt_atualizacao,
				 nm_usuario_nrec,
				 dt_atualizacao_nrec,
				 cd_medico_exec,
				 cd_departamento_medico,
				 nr_seq_proc_interno,
				 cd_pessoa_fisica,
				 ie_origem_proced,
				 cd_procedimento,
				 ds_observacao,
				 dt_desejada,
				 nr_minuto_duracao,
				 nr_seq_classif_agenda,				
				 ds_cirurgia_des,
				 ie_anestesia,
				 ie_lado,
				 cd_doenca_cid,
				 cd_departamento_medico_exec
			from 	 pedido_age_cirurgia
			where    nr_sequencia         = nr_sequencia_p;
			
                commit;			
			
		select  count(*)
		into  	qt_proc_sec
		from	pedido_age_cirurgia_proc
		where 	nr_seq_pedido = nr_sequencia_p;
		
		if(nvl(qt_proc_sec,0) > 0)then
		
			insert into agenda_lista_espera_proc(
				cd_categoria,
				ie_autorizacao,
				ds_indicacao_clinica,
				cd_topografia_proced,
				nr_seq_lista,
				ds_observacao,
				cd_convenio,
				nr_sequencia,
				nm_usuario,
				nm_usuario_cancel,
				nr_seq_proc_interno,
				qt_min_proc,
				qt_procedimento,
				cd_medico_req,
				cd_pessoa_indicacao,
				cd_medico,
				cd_externo,
				ie_lado,
				ie_cobertura_conv,
				dt_atualizacao,
				dt_agenda_fim_externa,
				dt_agenda_externa,
				cd_plano,
				cd_procedimento,
				ie_origem_proced,
				cd_procedimento_tuss
			) select cd_categoria,
				 ie_autorizacao,
				 ds_indicacao_clinica,
				 cd_topografia_proced,
				 nr_seq_lista_espera_w,
				 ds_observacao,
				 cd_convenio,
				 agenda_lista_espera_proc_seq.nextval,
				 nm_usuario,
				 nm_usuario_cancel,
				 nr_seq_proc_interno,
				 qt_min_proc,
				 qt_procedimento,
				 cd_medico_req,
				 cd_pessoa_indicacao,
				 cd_medico,
				 cd_externo,
				 ie_lado,
				 ie_cobertura_conv,
				 dt_atualizacao,
				 dt_agenda_fim_externa,
				 dt_agenda_externa,
				 cd_plano,
				 cd_procedimento,
				 ie_origem_proced,			
				 cd_procedimento_tuss
			from	 pedido_age_cirurgia_proc
			where 	 nr_seq_pedido = nr_sequencia_p;			
		
		end if;
			
		update	pedido_age_cirurgia
		set	dt_liberacao         = sysdate,
			nr_seq_age_lista     = nr_seq_lista_espera_w,
			nm_usuario_liberacao = nm_usuario_p
		where   nr_sequencia         = nr_sequencia_p;		

		commit;
	end if;

elsif (ie_type = 2)then

	update	pedido_age_cirurgia
	set	dt_inativacao         = sysdate,
	        ds_justificativa      = ds_justificativa_p,
		nm_usuario_inativacao = nm_usuario_p
	where   nr_sequencia          = nr_sequencia_p;
	
	select	max(nr_seq_age_lista)
	into	nr_seq_lista_espera_w
	from	pedido_age_cirurgia
	where   nr_sequencia          = nr_sequencia_p;
	
	if (nvl(nr_seq_lista_espera_w,0) > 0)then	
		update	agenda_lista_espera	
		set     ie_status_espera = 'C'
		where  	nr_sequencia = nr_seq_lista_espera_w;
	end if;
	
	commit;
end if;

end liberar_pedido_cirurgico;
/
