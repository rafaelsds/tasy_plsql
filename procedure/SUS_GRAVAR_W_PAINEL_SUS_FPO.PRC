create or replace
procedure sus_gravar_w_painel_sus_fpo (	dt_competencia_p	date,
					cd_estabelecimento_p	number,	
					nm_usuario_p		Varchar2) is 

nr_seq_estrut_regra_w		w_painel_sus_fpo.nr_seq_estrut_regra%type;
nr_seq_forma_visual_w		w_painel_sus_fpo.nr_seq_forma_visual%type;
ds_estrutura_w			w_painel_sus_fpo.ds_estrutura%type;
ds_forma_visual_w		w_painel_sus_fpo.ds_forma_visual%type;
qt_resultado_fisico_w		w_painel_sus_fpo.qt_resultado_fisico%type;
vl_result_orcamento_w		w_painel_sus_fpo.vl_result_orcamento%type;
qt_result_fisico_prot_w		w_painel_sus_fpo.qt_result_fisico_prot%type;
vl_result_orca_prot_w		w_painel_sus_fpo.vl_result_orca_prot%type;
vl_saldo_fisico_w		w_painel_sus_fpo.vl_saldo_fisico%type;
vl_saldo_orcament_w		w_painel_sus_fpo.vl_saldo_orcament%type;
nr_seq_regra_fpo_w		sus_fpo_regra.nr_sequencia%type;
qt_fisico_w			sus_fpo_regra.qt_fisico%type;
vl_orcamento_w			sus_fpo_regra.vl_orcamento%type;
dt_competencia_w		sus_fpo_regra.dt_competencia%type;
qt_procedimento_w		procedimento_paciente.qt_procedimento%type;
vl_procedimento_w		procedimento_paciente.vl_procedimento%type;
cd_estabelecimento_w	        sus_fpo_regra.cd_estabelecimento%type;
cd_procedimento_w	        sus_fpo_regra.cd_procedimento%type;
ie_origem_proced_w	        sus_fpo_regra.ie_origem_proced%type;
nr_seq_grupo_w		        sus_fpo_regra.nr_seq_grupo%type;
nr_seq_subgrupo_w	        sus_fpo_regra.nr_seq_subgrupo%type;
nr_seq_forma_org_w	        sus_fpo_regra.nr_seq_forma_org%type;
ie_tipo_atendimento_w	        sus_fpo_regra.ie_tipo_atendimento%type;
ie_tipo_financiamento_w	        sus_fpo_regra.ie_tipo_financiamento%type;
ie_complexidade_w		sus_fpo_regra.ie_complexidade%type;
cd_cbo_w		        sus_fpo_regra.cd_cbo%type;
cd_convenio_sus_w	        parametro_faturamento.cd_convenio_sus%type;
qt_fisico_exec_w		sus_fpo_regra_externo.qt_fisico_exec%type;
vl_orcamento_exec_w	        sus_fpo_regra_externo.vl_orcamento_exec%type;
cd_carater_internacao_w	        sus_fpo_regra.cd_carater_internacao%type;
cd_procedencia_w	        sus_fpo_regra.cd_procedencia%type;
qt_regra_setor_w                number(10);
ie_soma_proc_w                  varchar2(1);
qt_regra_desconsid_w            number(10);
qt_regra_proc_w                 number(10);
qt_regra_cbo_w                  number(10);
qt_sus_aih_unif_w               number(10);
					
Cursor C01 is
	select	nr_seq_estrutura_regra,
		nr_seq_forma_visual,
		nr_sequencia,
		qt_fisico,
		vl_orcamento,
		dt_competencia,
                cd_estabelecimento,
		cd_cbo,
		cd_procedimento,
		nvl(ie_origem_proced,7) ie_origem_proced,
		ie_complexidade,
		ie_tipo_atendimento,
		ie_tipo_financiamento,
		nr_seq_forma_org,
		nr_seq_grupo,
		nr_seq_subgrupo,
		cd_carater_internacao,
		cd_procedencia
	from	sus_fpo_regra
	where	trunc(dt_competencia,'month') = trunc(dt_competencia_p,'month')	
	and	cd_estabelecimento = cd_estabelecimento_p;
        				
Cursor C02 is
        select	nvl(a.qt_procedimento,0) qt_procedimento,
                nvl(a.vl_procedimento,0) vl_procedimento,
                a.nr_sequencia,
                a.cd_setor_atendimento,
                a.cd_cbo,
                a.cd_procedimento,
                a.ie_origem_proced,
                d.nr_seq_forma_org,
                d.nr_seq_grupo,
                d.nr_seq_subgrupo,
                b.nr_interno_conta
        from	procedimento_paciente a,
                sus_estrutura_procedimento_v d,
                atendimento_paciente c,
                conta_paciente b,
                sus_aih_unif e
        where	a.cd_procedimento		= nvl(cd_procedimento_w,a.cd_procedimento) 
        and     a.ie_origem_proced		= nvl(ie_origem_proced_w,a.ie_origem_proced)
        and	nvl(a.cd_cbo,'0') 		= nvl(cd_cbo_w,nvl(a.cd_cbo,'0'))
        and	((nvl(cd_carater_internacao_w,'X') = 'X') or
                ((nvl(cd_carater_internacao_w,'X') <> 'X')  and ((	select	count(1)
                                                                                from	sus_aih_unif s
                                                                                where	s.nr_interno_conta 	= b.nr_interno_conta
                                                                                and	s.cd_carater_internacao	= cd_carater_internacao_w) > 0
                                                                                and	rownum = 1)) or
                ((nvl(cd_carater_internacao_w,'X') <> 'X') and (c.ie_carater_inter_sus = cd_carater_internacao_w)))
        and	d.ie_complexidade		= nvl(ie_complexidade_w,d.ie_complexidade)
        and	((nvl(ie_tipo_atendimento_w,'X') = 'X') or
                (obter_se_contido(c.ie_tipo_atendimento,ie_tipo_atendimento_w) = 'S'))
        and	((nvl(cd_procedencia_w,'X') = 'X') or
                (obter_se_contido(c.cd_procedencia, cd_procedencia_w) = 'S'))
        and	b.nr_interno_conta		= e.nr_interno_conta(+)
        and	(e.ie_tipo_financiamento = nvl(ie_tipo_financiamento_w,e.ie_tipo_financiamento) or 
                (e.ie_tipo_financiamento is null and d.ie_tipo_financiamento = nvl(ie_tipo_financiamento_w,d.ie_tipo_financiamento)))
        and	d.nr_seq_forma_org		= nvl(nr_seq_forma_org_w,d.nr_seq_forma_org)
        and	d.nr_seq_grupo			= nvl(nr_seq_grupo_w,d.nr_seq_grupo)
        and	d.nr_seq_subgrupo		= nvl(nr_seq_subgrupo_w,d.nr_seq_subgrupo)
        and	a.nr_interno_conta 		= b.nr_interno_conta
        and	b.nr_atendimento 		= c.nr_atendimento
        and	a.cd_procedimento		= d.cd_procedimento
        and	nvl(a.ie_origem_proced,7)	= d.ie_origem_proced
        and	b.cd_estabelecimento	= nvl(cd_estabelecimento_w,b.cd_estabelecimento)
        and	trunc(a.dt_procedimento,'month') = trunc(dt_competencia_w,'month')
        and	a.cd_motivo_exc_conta is null
        and	b.cd_convenio_parametro	= cd_convenio_sus_w;
     
Cursor C03 is               
        select	nvl(a.qt_procedimento,0) qt_procedimento,
                nvl(a.vl_procedimento,0) vl_procedimento,
                a.nr_sequencia,
                a.cd_setor_atendimento,
                a.cd_cbo,
                a.cd_procedimento,
                a.ie_origem_proced,
                d.nr_seq_forma_org,
                d.nr_seq_grupo,
                d.nr_seq_subgrupo,
                b.nr_interno_conta
        from	procedimento_paciente a,
                sus_estrutura_procedimento_v d,
                atendimento_paciente c,
                conta_paciente b,
                sus_aih_unif e
        where	a.cd_procedimento		= nvl(cd_procedimento_w,a.cd_procedimento) 
        and     a.ie_origem_proced		= nvl(ie_origem_proced_w,a.ie_origem_proced)
        and	nvl(a.cd_cbo,'0') 			= nvl(cd_cbo_w,nvl(a.cd_cbo,'0'))
        and	((nvl(cd_carater_internacao_w,'X') = 'X') or
                ((nvl(cd_carater_internacao_w,'X') <> 'X')  and ((	select	count(1)
                                                                        from	sus_aih_unif s
                                                                        where	s.nr_interno_conta 	= b.nr_interno_conta
                                                                        and	s.cd_carater_internacao	= cd_carater_internacao_w
                                                                        and	rownum = 1) > 0)) or
                ((nvl(cd_carater_internacao_w,'X') <> 'X') and (c.ie_carater_inter_sus = cd_carater_internacao_w)))
        and	b.ie_status_acerto		= 2
        and	b.nr_interno_conta		= e.nr_interno_conta(+)
        and	d.ie_complexidade		= nvl(ie_complexidade_w,d.ie_complexidade)
        and	((nvl(ie_tipo_atendimento_w,'X') = 'X') or
                (obter_se_contido(c.ie_tipo_atendimento,ie_tipo_atendimento_w) = 'S'))
        and	((nvl(cd_procedencia_w,'X') = 'X') or
                (obter_se_contido(c.cd_procedencia, cd_procedencia_w) = 'S'))		
        and	(e.ie_tipo_financiamento = nvl(ie_tipo_financiamento_w,e.ie_tipo_financiamento) or 
                (e.ie_tipo_financiamento is null and d.ie_tipo_financiamento = nvl(ie_tipo_financiamento_w,d.ie_tipo_financiamento)))
        and	d.nr_seq_forma_org		= nvl(nr_seq_forma_org_w,d.nr_seq_forma_org)
        and	d.nr_seq_grupo			= nvl(nr_seq_grupo_w,d.nr_seq_grupo)
        and	d.nr_seq_subgrupo		= nvl(nr_seq_subgrupo_w,d.nr_seq_subgrupo)
        and	b.nr_seq_protocolo is not null
        and	a.cd_motivo_exc_conta is null
        and	a.nr_interno_conta 		= b.nr_interno_conta
        and	b.nr_atendimento 		= c.nr_atendimento
        and	a.cd_procedimento		= d.cd_procedimento
        and	nvl(a.ie_origem_proced,7)	= d.ie_origem_proced
        and	b.cd_estabelecimento		= nvl(cd_estabelecimento_w,b.cd_estabelecimento)
        and	trunc(b.dt_mesano_referencia,'month') = trunc(dt_competencia_w,'month')
        and	b.cd_convenio_parametro		= cd_convenio_sus_w;
              
begin

exec_sql_dinamico('Tasy','Truncate table w_painel_sus_fpo');

select	nvl(max(cd_convenio_sus),0)
into	cd_convenio_sus_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_p;
	
for C01_w in C01 loop
        begin
        nr_seq_estrut_regra_w		:= C01_w.nr_seq_estrutura_regra;
        nr_seq_forma_visual_w		:= C01_w.nr_seq_forma_visual;
        nr_seq_regra_fpo_w		:= C01_w.nr_sequencia;
        qt_fisico_w			:= nvl(C01_w.qt_fisico,0);
        vl_orcamento_w			:= nvl(C01_w.vl_orcamento,0);
        ds_estrutura_w			:= substr(nvl(sus_obter_desc_estrutura(nr_seq_estrut_regra_w),''),1,255);
        ds_forma_visual_w		:= substr(nvl(sus_obter_desc_forma_visual(nr_seq_forma_visual_w),''),1,255);
        cd_estabelecimento_w            := C01_w.cd_estabelecimento;
        dt_competencia_w                := C01_w.dt_competencia;
        cd_cbo_w                        := C01_w.cd_cbo;
        cd_procedimento_w               := C01_w.cd_procedimento;
        ie_origem_proced_w              := C01_w.ie_origem_proced;
        ie_complexidade_w               := C01_w.ie_complexidade;
        ie_tipo_atendimento_w           := C01_w.ie_tipo_atendimento;
        ie_tipo_financiamento_w         := C01_w.ie_tipo_financiamento;
        nr_seq_forma_org_w              := C01_w.nr_seq_forma_org;
        nr_seq_grupo_w                  := C01_w.nr_seq_grupo;
        nr_seq_subgrupo_w               := C01_w.nr_seq_subgrupo;
        cd_carater_internacao_w         := C01_w.cd_carater_internacao;
        cd_procedencia_w                := C01_w.cd_procedencia;
        qt_resultado_fisico_w           := 0;
        vl_result_orcamento_w           := 0;
        qt_result_fisico_prot_w         := 0;
        vl_result_orca_prot_w           := 0;
        qt_fisico_exec_w                := 0;
        vl_orcamento_exec_w             := 0;
        qt_regra_setor_w                := 0;
        qt_regra_desconsid_w            := 0;
        qt_regra_proc_w                 := 0;
        qt_regra_cbo_w                  := 0;
        qt_sus_aih_unif_w               := 0;
					
        
        begin
        select	sum(qt_fisico_exec),
                sum(vl_orcamento_exec)
        into	qt_fisico_exec_w,
                vl_orcamento_exec_w
        from	sus_fpo_regra_externo
        where 	nr_seq_fpo_regra = nr_seq_regra_fpo_w;  
        exception
        when others then
                qt_fisico_exec_w        := 0;
                vl_orcamento_exec_w     := 0;
        end;
        
        begin
        select	1
        into    qt_regra_proc_w
        from 	sus_fpo_regra_proc w
        where	w.nr_seq_regra	= nr_seq_regra_fpo_w;
        exception
        when others then
                qt_regra_proc_w        := 0;
        end;
        
        begin
        select	1
        into    qt_regra_setor_w
        from    sus_fpo_regra_setor y
        where	y.nr_seq_regra	= nr_seq_regra_fpo_w
        and	y.ie_situacao 	= 'A';
        exception
        when others then
                qt_regra_setor_w        := 0;
        end;
        
        begin
        select	1
        into    qt_regra_cbo_w        
        from	sus_fpo_regra_cbo z
        where	z.nr_seq_fpo_regra	= nr_seq_regra_fpo_w;
        exception
        when others then
                qt_regra_cbo_w        := 0;
        end;
        
        begin
        select	1
        into    qt_regra_desconsid_w
        from	sus_fpo_regra_desconsid x
        where	x.nr_seq_fpo_regra	= nr_seq_regra_fpo_w;
        exception
        when others then
                qt_regra_desconsid_w        := 0;
        end;

        for C02_w in C02 loop
                begin
                
                ie_soma_proc_w := 'S';
                
                if (qt_regra_proc_w > 0) then
                
                        begin
                        select	1
                        into    qt_regra_proc_w
                        from 	sus_fpo_regra_proc w
                        where	w.nr_seq_regra	= nr_seq_regra_fpo_w
                        and	w.cd_procedimento	= C02_w.cd_procedimento
                        and	w.ie_origem_proced	= C02_w.ie_origem_proced
                        and	w.ie_situacao 	= 'A';
                        exception
                        when others then
                                qt_regra_proc_w        := 0;
                        end;
                        
                        if      (qt_regra_proc_w = 0) then
                                ie_soma_proc_w := 'N';                                     
                        end if;
                end if;
                
                if      (qt_regra_setor_w > 0) then
                
                        begin
                        select	1
                        into    qt_regra_setor_w
                        from	sus_fpo_regra_setor q
                        where	q.nr_seq_regra	= nr_seq_regra_fpo_w
                        and	q.ie_situacao	= 'A'
                        and	q.cd_setor_atendimento = C02_w.cd_setor_atendimento;
                        exception
                        when others then
                                qt_regra_setor_w        := 0;
                        end;
                        
                        if (qt_regra_setor_w = 0) then
                                ie_soma_proc_w := 'N';
                        end if;
                end if;
                
                if      (qt_regra_cbo_w > 0) then
                
                        begin
                        select	1
                        into    qt_regra_cbo_w
                        from	sus_fpo_regra_cbo z
                        where	z.cd_cbo		= nvl(C02_w.cd_cbo,'0')
                        and	z.nr_seq_fpo_regra	= nr_seq_regra_fpo_w;
                        exception
                        when others then
                                qt_regra_cbo_w        := 0;
                        end;
                        
                        if      (qt_regra_cbo_w = 0) then
                                ie_soma_proc_w := 'N';       
                        end if;
                end if;
                
                if      (qt_regra_desconsid_w > 0) then
                
                        begin
                        select	1
                        into    qt_regra_desconsid_w
                        from	sus_fpo_regra_desconsid x
                        where	x.nr_seq_fpo_regra	= nr_seq_regra_fpo_w
                        and	nvl(C02_w.cd_cbo,'0')	= nvl(x.cd_cbo,nvl(C02_w.cd_cbo,'0'))
                        and	C02_w.cd_procedimento	= nvl(x.cd_procedimento,C02_w.cd_procedimento)
                        and	C02_w.ie_origem_proced	= nvl(x.ie_origem_proced,C02_w.ie_origem_proced)
                        and	C02_w.nr_seq_forma_org	= nvl(x.nr_seq_forma_org,C02_w.nr_seq_forma_org)
                        and	C02_w.nr_seq_grupo	= nvl(x.nr_seq_grupo,C02_w.nr_seq_grupo)
                        and	C02_w.nr_seq_subgrupo	= nvl(x.nr_seq_subgrupo,C02_w.nr_seq_subgrupo);
                        exception
                        when others then
                                qt_regra_desconsid_w        := 0;
                        end;
                        
                        if (qt_regra_desconsid_w > 0) then
                                ie_soma_proc_w := 'N';
                        end if;
                end if;
                
                if      (ie_soma_proc_w = 'S') then
                        begin
                                qt_resultado_fisico_w		:= (qt_resultado_fisico_w + (C02_w.qt_procedimento + nvl(qt_fisico_exec_w,0)));
                                vl_result_orcamento_w		:= (vl_result_orcamento_w + (C02_w.vl_procedimento + nvl(vl_orcamento_exec_w,0)));
                        end;
                end if;
                
                end;
        end loop;
        
          for C03_w in C03 loop
                begin
                
                ie_soma_proc_w := 'S';
                
                if (qt_regra_proc_w > 0) then
                
                        begin
                        select	1
                        into    qt_regra_proc_w
                        from 	sus_fpo_regra_proc w
                        where	w.nr_seq_regra	= nr_seq_regra_fpo_w
                        and	w.cd_procedimento	= C03_w.cd_procedimento
                        and	w.ie_origem_proced	= C03_w.ie_origem_proced
                        and	w.ie_situacao 	= 'A';
                        exception
                        when others then
                                qt_regra_proc_w        := 0;
                        end;
                        
                        if (qt_regra_proc_w = 0) then
                                ie_soma_proc_w := 'N';   
                        end if;
                end if;
                
                if      (qt_regra_setor_w > 0) then
                
                        begin
                        select	1
                        into    qt_regra_setor_w
                        from	sus_fpo_regra_setor q
                        where	q.nr_seq_regra	= nr_seq_regra_fpo_w
                        and	q.ie_situacao	= 'A'
                        and	q.cd_setor_atendimento = C03_w.cd_setor_atendimento;
                        exception
                        when others then
                                qt_regra_setor_w        := 0;
                        end;
                        
                        if (qt_regra_setor_w = 0) then
                                ie_soma_proc_w := 'N';
                        end if;
                end if;
                
                if      (qt_regra_cbo_w > 0) then
                
                        begin
                        select	1
                        into    qt_regra_cbo_w
                        from	sus_fpo_regra_cbo z
                        where	z.cd_cbo		= nvl(C03_w.cd_cbo,'0')
                        and	z.nr_seq_fpo_regra	= nr_seq_regra_fpo_w;
                        exception
                        when others then
                                qt_regra_cbo_w        := 0;
                        end;
                        
                        if (qt_regra_cbo_w = 0) then
                                ie_soma_proc_w := 'N';       
                        end if;
                end if;
                
                if(qt_regra_desconsid_w > 0) then
                
                        begin
                        select	1
                        into    qt_regra_desconsid_w
                        from	sus_fpo_regra_desconsid x
                        where	x.nr_seq_fpo_regra	= nr_seq_regra_fpo_w
                        and	nvl(C03_w.cd_cbo,'0')	= nvl(x.cd_cbo,nvl(C03_w.cd_cbo,'0'))
                        and	C03_w.cd_procedimento	= nvl(x.cd_procedimento,C03_w.cd_procedimento)
                        and	C03_w.ie_origem_proced	= nvl(x.ie_origem_proced,C03_w.ie_origem_proced)
                        and	C03_w.nr_seq_forma_org	= nvl(x.nr_seq_forma_org,C03_w.nr_seq_forma_org)
                        and	C03_w.nr_seq_grupo	= nvl(x.nr_seq_grupo,C03_w.nr_seq_grupo)
                        and	C03_w.nr_seq_subgrupo	= nvl(x.nr_seq_subgrupo,C03_w.nr_seq_subgrupo);
                        exception
                        when others then
                                qt_regra_desconsid_w        := 0;
                        end;
                        
                        if (qt_regra_desconsid_w > 0) then
                                ie_soma_proc_w := 'N';
                        end if;
                end if;
                
                if      (ie_soma_proc_w = 'S') then
                begin   
                        qt_result_fisico_prot_w		:= (qt_result_fisico_prot_w + (C03_w.qt_procedimento + nvl(qt_fisico_exec_w,0)));
                        vl_result_orca_prot_w		:= (vl_result_orca_prot_w + (C03_w.vl_procedimento + nvl(vl_orcamento_exec_w,0)));
                end;
                end if;
        end;
        end loop;
        
        vl_saldo_fisico_w		:= nvl((qt_fisico_w - qt_resultado_fisico_w),0);
        vl_saldo_orcament_w		:= nvl((vl_orcamento_w - vl_result_orcamento_w),0);		                


        insert into w_painel_sus_fpo (	
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                nr_seq_estrut_regra,
                nr_seq_forma_visual,
                ds_estrutura,
                ds_forma_visual,
                qt_resultado_fisico,
                vl_result_orcamento,
                qt_result_fisico_prot,
                vl_result_orca_prot,
                vl_saldo_fisico,
                vl_saldo_orcament,
                dt_competencia,
                cd_estabelecimento)
        values(	w_painel_sus_fpo_seq.nextval,
                sysdate,
                nm_usuario_p,
                nr_seq_estrut_regra_w,
                nr_seq_forma_visual_w,
                ds_estrutura_w,
                ds_forma_visual_w,
                qt_resultado_fisico_w,
                vl_result_orcamento_w,
                qt_result_fisico_prot_w,
                vl_result_orca_prot_w,
                vl_saldo_fisico_w,
                vl_saldo_orcament_w,
                dt_competencia_w,
                cd_estabelecimento_p);
        end;
		
end loop;
commit;

end sus_gravar_w_painel_sus_fpo;
/
