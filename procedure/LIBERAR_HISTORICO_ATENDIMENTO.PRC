create or replace
procedure liberar_historico_atendimento(
					nm_usuario_p		Varchar2,
					nr_sequencia_p		number,
					nr_atendimento_p	number) is 

begin

if	(nvl(nr_sequencia_p,0) > 0) and 
        (nvl(nr_atendimento_p,0) >0) then

	update	atendimento_hist_paciente
	set	nm_usuario_liberacao = nm_usuario_p,
		dt_liberacao	     = sysdate	
	where	nr_sequencia   = nr_sequencia_p
	and	nr_atendimento = nr_atendimento_p;
end if;

commit;

end liberar_historico_atendimento;
/