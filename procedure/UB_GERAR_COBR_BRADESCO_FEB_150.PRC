create or replace
procedure ub_gerar_cobr_bradesco_feb_150
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 
/*	Vers�o 05
	Data: 11/11/2011	
OS - 421020 */

/* Geral */
ds_conteudo_w			varchar2(150);
nr_seq_registro_w			number(10)	:= 0;
nr_seq_apres_w			number(10)	:= 0;
vl_total_w			number(17)	:= 0;

/* Header A*/
nm_empresa_w			varchar2(20);
dt_geracao_w			varchar2(8);
nr_seq_arquivo_w			varchar2(6);
nm_banco_w			varchar2(20);
cd_convenio_w			varchar2(20);
cd_banco_w			varchar2(3);

/* Transa��es C - D - E */
cd_agencia_bancaria_w		varchar2(4);
ds_ocorrencia_1_w			varchar2(40);
ds_ocorrencia_2_w			varchar2(40);
ds_ocorrencia_w			varchar2(60);
dt_vencimento_w			varchar2(8);
vl_titulo_w			varchar2(15);
ds_mensagen_w			varchar2(26);
ds_uso_empresa_w			varchar2(60);
ds_ident_cliente_emp_w		varchar2(25);
ds_ident_cliente_emp_atual_w	varchar2(25);
ds_ident_cliente_banco_w		varchar2(14);
ie_tipo_inscricao_w			varchar2(1);
nr_inscricao_w			varchar2(15);
nr_carteira_benef_w		varchar2(25);
nr_seq_pagador_w			number(10);

/* Brancos */
ds_brancos_52_w			varchar2(52);
ds_brancos_25_w			varchar2(25);
ds_brancos_20_w			varchar2(20);
ds_brancos_126_w			varchar2(126);
ds_brancos_8_w			varchar2(8);
ds_brancos_4_w			varchar2(4);

Cursor C01 is
	select	lpad(substr(decode(g.cd_agencia_bancaria,null,f.cd_agencia_bancaria,g.cd_agencia_bancaria),1,4),4,'0') cd_agencia_bancaria,
		lpad(' ',40,' ') ds_ocorrencia_1,
		lpad(' ',40,' ') ds_ocorrencia_2,
		lpad(' ',60,' ') ds_ocorrencia,
		substr(to_char(b.dt_vencimento,'YYYYMMDD'),1,8) dt_vencimento,
		lpad(replace(to_char(b.vl_titulo, 'fm00000000000.00'),'.',''),15,'0') vl_titulo,
		rpad(b.nr_titulo,60,' ') ds_uso_empresa,
		lpad(' ',26,' ') ds_mensagen,
		lpad(' ',25,' ') ds_ident_cliente_emp,  /* identificacao do cliente na empresa */
		lpad(' ',25,' ') ds_ident_cliente_emp_atual, /* identificacao do cliente na empresa atual */
		rpad(nvl(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'C'),1,8) || 
			substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DC'),1,1), ' '),14,' '), /* falta o c�digo da opera��o da conta */			
		decode(b.cd_cgc,null,'1','2') ie_tipo_inscricao, 
		lpad(nvl(b.cd_cgc_cpf,'0'),15,'0') nr_inscricao,
		nvl(substr(pls_obter_dados_segurado(pls_obter_segurado_pagador(d.nr_seq_pagador),'C'),1,25),'0') nr_carteira_benef,
		d.nr_seq_pagador
	from	banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v		b,
		titulo_receber_cobr		c,
		cobranca_escritural		a,
		pls_contrato_pagador_fin	g
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo			= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador		= f.nr_sequencia(+)
	and 	d.nr_seq_pagador 		= g.nr_seq_pagador(+)		
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin
delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	lpad(' ',52,' '),
	lpad(' ',25,' '),
	lpad(' ',20,' '),
	lpad(' ',126,' '),
	lpad(' ',8,' '),
	lpad(' ',4,' ')
into	ds_brancos_52_w,
	ds_brancos_25_w,
	ds_brancos_20_w,
	ds_brancos_126_w,
	ds_brancos_8_w,
	ds_brancos_4_w
from	dual;

/* Header */
select	rpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,20))),20,' '),
	to_char(sysdate,'YYYYMMDD'),
	lpad(to_char(nvl(a.nr_remessa,a.nr_sequencia)),6,'0'),
	rpad(substr(obter_nome_banco(a.cd_banco),1,20),20,' ') nm_banco,
	rpad(nvl(substr(nvl(c.cd_conv_banco_deb,c.cd_convenio_banco),1,20),' '),20,' ') cd_convenio,     /* precisa verificar */
	lpad(nvl(substr(a.cd_banco,1,3),'0'),3,'0') cd_banco
into	nm_empresa_w,
	dt_geracao_w,
	nr_seq_arquivo_w,
	nm_banco_w,
	cd_convenio_w,
	cd_banco_w
from	estabelecimento		b,
	cobranca_escritural	a,
	banco_estabelecimento	c
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:= 	'A'|| '1' || cd_convenio_w || nm_empresa_w || cd_banco_w || nm_banco_w || dt_geracao_w || lpad(nr_seq_arquivo_w,6,'0') || '05' ||
			rpad('DEBITO AUTOMATICO',17,' ') || ds_brancos_52_w;  
	
nr_seq_apres_w	:= nr_seq_apres_w + 1;
	
insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w);
/* Fim Header */

/* Transa��o */

open C01;
loop
fetch C01 into	
	cd_agencia_bancaria_w,
	ds_ocorrencia_1_w,
	ds_ocorrencia_2_w,
	ds_ocorrencia_w,
	dt_vencimento_w,
	vl_titulo_w,
	ds_uso_empresa_w,
	ds_mensagen_w,
	ds_ident_cliente_emp_w,
	ds_ident_cliente_emp_atual_w,
	ds_ident_cliente_banco_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nr_carteira_benef_w,
	nr_seq_pagador_w;
exit when C01%notfound;
	begin
	/* Caso o pagador n�o tenha carteira, buscar a carteira do titular */
	if	(nvl(nr_carteira_benef_w,'0') = '0') then		
		select	nvl(max(b.cd_usuario_plano),'0')
		into	nr_carteira_benef_w
		from	pls_segurado_carteira	b,
			pls_segurado		a
		where	a.nr_sequencia	 = b.nr_seq_segurado
		and	a.nr_seq_titular is null
		and	a.nr_seq_pagador = nr_seq_pagador_w;
	end if;
	
	nr_carteira_benef_w	:= rpad(rpad('000' || substr(nr_carteira_benef_w,1,13),16,'0'),25,' ');
	
	nr_seq_apres_w	:= nr_seq_apres_w + 1;
	
	/* TIPO E */
	ds_conteudo_w	:= 	'E' || nr_carteira_benef_w || cd_agencia_bancaria_w || ds_ident_cliente_banco_w || dt_vencimento_w ||
				vl_titulo_w || '03' || ds_uso_empresa_w || ie_tipo_inscricao_w || nr_inscricao_w || ds_brancos_4_w || '0';
	
	insert into w_envio_banco
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
		values	(	w_envio_banco_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_apres_w);
	/* FIM TIPO E */
	
	vl_total_w := vl_total_w + vl_titulo_w;
	end;				
end loop;
close C01;
/* Fim Transa��o */

/* Trailler */
select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;

nr_seq_apres_w	:= nr_seq_apres_w + 1;

ds_conteudo_w	:= 	'Z' || lpad(nr_seq_apres_w,6,'0') || lpad(vl_total_w,17,'0') || ds_brancos_126_w;

insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w);
/* Fim Trailler*/

commit;

end ub_gerar_cobr_bradesco_feb_150;
/
