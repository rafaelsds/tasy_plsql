create or replace
procedure ppm_metrica_hpi(
		nr_seq_metrica_p			number,
		nr_seq_objetivo_metrica_p		number,
		cd_pessoa_fisica_p			varchar2,
		nm_usuario_p			varchar2,
		dt_referencia_p			date) is
			
dt_ref_inicio_w 		date;
dt_ref_fim_w 		date;

resultado_w		number(15,4);
hr_prev_w			number(15,4);
hr_real_w			number(15,4);

nm_usuario_exec_w	varchar2(15);
nr_seq_diretoria_w		ppm_objetivo.nr_seq_diretoria%type;
nr_seq_gerencia_w		ppm_objetivo.nr_seq_gerencia%type;
nr_seq_grupo_w		ppm_objetivo.nr_seq_grupo%type;

nm_usuario_grupo_w	varchar2(15);

nr_seq_acumulado_w	ppm_objetivo_result.nr_sequencia%type;
vl_montante_w		number(15,4);
vl_individual_w		number(15,4);
vl_calculado_w		number(15,4);
dt_fim_ano_w		date;

begin

select	max(nr_seq_gerencia),
	max(nr_seq_grupo),
	max(nr_seq_diretoria)
into	nr_seq_gerencia_w,
	nr_seq_grupo_w,
	nr_seq_diretoria_w
from	ppm_objetivo_metrica a,
	ppm_objetivo_meta b,
	ppm_objetivo c
where	a.nr_sequencia	= nr_seq_objetivo_metrica_p
and	a.nr_seq_meta	= b.nr_sequencia
and	b.nr_seq_objetivo	= c.nr_sequencia;

dt_ref_inicio_w := pkg_date_utils.start_of(dt_referencia_p,'MONTH');
dt_ref_fim_w 	:= pkg_date_utils.end_of(last_day(dt_referencia_p),'DAY');

if	(nr_seq_diretoria_w is not null) then
	select	round(avg(pmo_obter_dados_bsc(y.nr_sequencia, 'HPI', 'PROJ')), 2) hpi
	into	resultado_w
	from	dashboard_pmo_proj_graf x,
		proj_projeto y
	where	x.nr_seq_proj = y.nr_sequencia
	and	trunc(x.dt_referencia,'dd') between dt_ref_inicio_w and dt_ref_fim_w
	and	y.nr_seq_gerencia in 
		(select	x.nr_sequencia
		from	gerencia_wheb x
		where	x.nr_seq_diretoria	= nr_seq_diretoria_w)
	and	y.nr_seq_estagio <> 44;

elsif	(nr_seq_gerencia_w is not null) then
	select	round(avg(pmo_obter_dados_bsc(y.nr_sequencia, 'HPI', 'PROJ')), 2) hpi
	into	resultado_w
	from	dashboard_pmo_proj_graf x,
		proj_projeto y
	where	x.nr_seq_proj = y.nr_sequencia
	and	trunc(x.dt_referencia,'dd') between dt_ref_inicio_w and dt_ref_fim_w
	and	y.nr_seq_gerencia = nr_seq_gerencia_w
	and	y.nr_seq_estagio <> 44;

elsif	(nr_seq_grupo_w is not null) then
	select	round(avg(pmo_obter_dados_bsc(y.nr_sequencia, 'HPI', 'PROJ')), 2) hpi
	into	resultado_w
	from	dashboard_pmo_proj_graf x,
		proj_projeto y
	where	x.nr_seq_proj = y.nr_sequencia
	and	trunc(x.dt_referencia,'dd') between dt_ref_inicio_w and dt_ref_fim_w
	and	y.nr_seq_grupo_des = nr_seq_grupo_w
	and	y.nr_seq_estagio <> 44;

elsif	(cd_pessoa_fisica_p is not null) then
	nm_usuario_grupo_w	:=	substr(obter_usuario_pessoa(cd_pessoa_fisica_p),1,15);

	select	round(avg(pmo_obter_dados_bsc(y.nr_sequencia, 'HPI', 'PROJ')), 2) hpi
	into	resultado_w
	from	dashboard_pmo_proj_graf x,
		proj_projeto y			
	where	x.nr_seq_proj = y.nr_sequencia
	and	exists (select	1 
			from	proj_ordem_servico a,
				man_ordem_serv_ativ b 
			where	y.nr_sequencia = a.nr_seq_proj
			and	a.nr_seq_ordem = b.nr_seq_ordem_serv 
			and	b.nm_usuario_exec = nm_usuario_grupo_w
			and exists (	select	1
					from	proj_equipe pe,
						proj_equipe_papel pp
					where	y.nr_sequencia = pe.nr_seq_proj
					and	pe.nr_sequencia = pp.nr_seq_equipe
					and	pp.cd_pessoa_fisica = cd_pessoa_fisica_p))
	and	trunc(x.dt_referencia,'dd') between dt_ref_inicio_w and dt_ref_fim_w       
	and	y.nr_seq_estagio <> 44;

end if;

hr_real_w := 0;
hr_prev_w := 0;

ppm_gravar_resultado(nr_seq_objetivo_metrica_p,dt_referencia_p,resultado_w, hr_real_w, hr_prev_w, nm_usuario_p);

--'Recalculo do acumulado
dt_fim_ano_w	:= pkg_date_utils.end_of(to_date('31/12/' || to_char(dt_referencia_p,'yyyy'),'dd/mm/yyyy'),'DAY');

select	max(nr_sequencia)
into	nr_seq_acumulado_w
from	ppm_objetivo_result a
where	a.nr_seq_metrica	= nr_seq_objetivo_metrica_p
and	dt_referencia	= dt_fim_ano_w;

if	(nr_seq_acumulado_w > 0) then
	select	nvl(sum(vl_montante),0),
		nvl(sum(vl_individual),0)
	into	vl_montante_w,
		vl_individual_w
	from	ppm_objetivo_result a
	where	a.nr_seq_metrica = nr_seq_objetivo_metrica_p
	and	trunc(a.dt_referencia,'year') = trunc(dt_referencia_p,'year')
	and	a.nr_sequencia	<> nr_seq_acumulado_w;

	vl_calculado_w	:= dividir(vl_individual_w, vl_montante_w);

	update	ppm_objetivo_result
	set	vl_resultado_calc 	= nvl(vl_calculado_w,0),
		vl_montante	= nvl(vl_montante_w,0),
		vl_individual	= nvl(vl_individual_w,0)
	where	nr_sequencia 	= nr_seq_acumulado_w;
end if;

/*'Ja tinha commit na ppm_gravar_resultado'*/
commit; 

end ppm_metrica_hpi;
/
