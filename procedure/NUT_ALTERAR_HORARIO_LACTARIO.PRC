create or replace
procedure Nut_Alterar_Horario_Lactario(	dt_horario_p		Date,
					nr_seq_item_p		Number,
					nm_usuario_p		Varchar2) is 

dt_horario_ant_w	Date;
nr_seq_prod_lac_w	number;

begin


if (nr_seq_item_p is not null) then

	select	dt_horario,
		nr_seq_prod_lac
	into	dt_horario_ant_w,
		nr_seq_prod_lac_w
	from	nut_producao_lactario_item
	where	nr_sequencia = nr_seq_item_p;
		
	
	
	if(dt_horario_ant_w is null) then
	begin		
		select	dt_producao
		into	dt_horario_ant_w
		from	nut_producao_lactario
		where	nr_sequencia = nr_seq_prod_lac_w;
	end;
	end if;
	
	update	nut_producao_lactario_item
	set	dt_horario = to_date(to_char(dt_horario_ant_w,'dd/mm/yyyy ')||to_char(dt_horario_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_seq_item_p;
	
	insert into nut_prod_alt_item (	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_alteracao,
					dt_horario_ant,
					dt_horario_atual,
					nr_seq_nut_prod_lac_item)
				values(	nut_prod_alt_item_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					dt_horario_ant_w,
					to_date(to_char(dt_horario_ant_w,'dd/mm/yyyy ')||to_char(dt_horario_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
					nr_seq_item_p);

end if;

commit;

end Nut_Alterar_Horario_Lactario;
/