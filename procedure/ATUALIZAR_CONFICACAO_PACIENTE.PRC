create or replace
procedure atualizar_conficacao_paciente(
			nr_atendimento_p	number,
			nr_case_p			number,
			ie_insert_update_p	varchar2,
			nm_usuario_p		Varchar2) is 
cd_convenio_w		atend_categoria_convenio.cd_convenio%type;
cd_categoria_w		atend_categoria_convenio.cd_categoria%type;
nr_interno_conta_w	conta_paciente.nr_interno_conta%type;
nr_atendimento_w	conta_paciente.nr_interno_conta%type;

begin
	nr_atendimento_w := nr_atendimento_p;
	if (ie_insert_update_p = 'S') then
		update	patient_coding
		set		dt_end_coding = sysdate,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate,
				ie_status = 'F'
		where	1 = 1
		and		(nr_atendimento_w is null or nr_encounter = nr_atendimento_w)
		and		(nr_case_p is null or nr_seq_episode = nr_case_p);
	
		insert into	patient_coding (
			nr_sequencia,
			nr_encounter,
			nr_seq_episode,
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,
			dt_atualizacao_nrec,
			dt_start_coding,
			ie_status)
		values (
			patient_coding_seq.nextval,
			nr_atendimento_w,
			nr_case_p,
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			sysdate,
			sysdate,
			ie_insert_update_p
		);
		
		commit;
	elsif (ie_insert_update_p = 'F') then
		if (nr_atendimento_w is null and nr_case_p is not null) then
			nr_atendimento_w := obter_atendimento_episodio(nr_case_p);
		end if;
		select  d.cd_convenio,
				d.cd_categoria
		into	cd_convenio_w,
				cd_categoria_w
		from    convenio c,
				atend_categoria_convenio d
		where 	d.nr_atendimento    = nr_atendimento_w
		and     d.cd_convenio        = c.cd_convenio
		and    d.dt_inicio_vigencia    = (
						select	max(dt_inicio_vigencia)
						from 	atend_categoria_convenio b
						where   nr_atendimento= nr_atendimento_w
						and    	sysdate between    b.dt_inicio_vigencia and nvl(b.dt_final_vigencia,sysdate + 30));
						
		select  max(a.nr_interno_conta)
		into    nr_interno_conta_w
		from    convenio         c,
				conta_paciente         a
		where   a.cd_convenio_calculo        = c.cd_convenio
		and     a.nr_atendimento            = nr_atendimento_w
		and     a.cd_estabelecimento        = wheb_usuario_pck.get_cd_estabelecimento()
		and     a.cd_convenio_parametro        = cd_convenio_w
		and     a.cd_categoria_parametro    = cd_categoria_w
		and     a.ie_status_acerto            = 1
		and     a.nr_seq_protocolo is null
		and     sysdate between    a.dt_periodo_inicial and a.dt_periodo_final;
	
		update	patient_coding
		set		dt_end_coding = sysdate,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate,
				ie_status = 'F'
		where	1 = 1
		and		(nr_atendimento_p is null or nr_encounter = nr_atendimento_w)
		and		(nr_case_p is null or nr_seq_episode = nr_case_p);
		
		gerar_etapa_conta(nr_interno_conta_w, 4, nm_usuario_p);
	end if;

commit;

end atualizar_conficacao_paciente;
/