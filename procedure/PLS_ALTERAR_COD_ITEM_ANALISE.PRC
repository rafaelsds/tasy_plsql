create or replace
procedure pls_alterar_cod_item_analise
			(	nr_seq_item_p		number,
				cd_item_p		varchar2,
				ds_item_p		varchar2,
				ie_tipo_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

cd_material_w		varchar2(20);
nr_seq_analise_w	number(10);
nr_seq_conta_mat_w	number(10);
nr_seq_conta_proc_w	number(10);
nr_seq_item_w		number(10);

begin
select	max(a.nr_seq_analise),
	max(a.nr_seq_conta_proc),
	max(a.nr_seq_conta_mat),
	max(a.nr_seq_item)
into	nr_seq_analise_w,
	nr_seq_conta_proc_w,
	nr_seq_conta_mat_w,
	nr_seq_item_w
from	w_pls_resumo_conta	a
where	a.nr_sequencia	= nr_seq_item_p;

if	(nr_seq_analise_w is not null) then
	if	(ie_tipo_p = 'P') then
		update	w_pls_resumo_conta
		set	cd_item_convertido	= cd_item_p,
			ds_item_convertido	= ds_item_p
		where	nr_sequencia		= nr_seq_item_p;
		
		update	pls_conta_pos_estabelecido
		set	cd_item_convertido	= cd_item_p,
			ds_item_convertido	= ds_item_p,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_conta_proc	= nvl(nr_seq_conta_proc_w,nr_seq_item_w)
		and	((ie_situacao		= 'A') or (ie_situacao	is null));
	else
		update	w_pls_resumo_conta
		set	cd_item_convertido	= cd_item_p,
			ds_item_convertido	= ds_item_p
		where	nr_sequencia		= nr_seq_item_p;
		
		update	pls_conta_pos_estabelecido
		set	cd_item_convertido	= cd_item_p,
			ds_item_convertido	= ds_item_p,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_conta_mat	= nvl(nr_seq_conta_mat_w,nr_seq_item_w)
		and	((ie_situacao		= 'A') or (ie_situacao	is null));
	end if;

	insert into pls_hist_analise_conta
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_analise,
		ie_tipo_historico,
		ds_observacao,
		ds_call_stack)
	values	(pls_hist_analise_conta_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_analise_w,
		34,
		'C�digo do item alterado por: ' || nm_usuario_p,
		substr(dbms_utility.format_call_stack,1,4000));
end if;

commit;

end pls_alterar_cod_item_analise;
/