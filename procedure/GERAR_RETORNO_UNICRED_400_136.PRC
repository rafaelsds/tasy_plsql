create or replace
procedure GERAR_RETORNO_UNICRED_400_136(  nr_seq_cobr_escrit_p  number,
          nm_usuario_p  varchar2) is 

nr_seq_reg_T_w        number(10);
nr_seq_reg_U_w        number(10);
nr_titulo_w        number(10);
vl_titulo_w        number(15,2);
vl_acrescimo_w        number(15,2);
vl_desconto_w        number(15,2);
vl_abatimento_w        number(15,2);
vl_liquido_w        number(15,2);
vl_outras_despesas_w      number(15,2);
dt_liquidacao_w        varchar2(6);
ds_titulo_w        varchar2(255);
vl_cobranca_w        number(15,2);
vl_alterar_w        number(15,2);
cd_ocorrencia_w        varchar2(10);
nr_seq_ocorrencia_ret_w      number(10);
nr_nosso_numero_w      varchar2(17);
cd_banco_w				banco_escritural.cd_banco%type;
dt_liquidacao_ajustado_w	date;
vl_saldo_titulo_w		titulo_receber.vl_saldo_titulo%type;

cursor c01 is
  select  trim(substr(ds_string,280,26)),
      to_number(substr(ds_string,153,13))/100,
      to_number(substr(ds_string,267,13))/100,
      to_number(substr(ds_string,241,13))/100,
      to_number(substr(ds_string,228,13))/100,
      to_number(substr(ds_string,254,13))/100,
      to_number(substr(ds_string,182,7))/100,
      substr(ds_string,111,6),
      substr(ds_string,109,2),
      trim(substr(ds_string,46,17))
  from  w_retorno_banco
  where  nr_seq_cobr_escrit  = nr_seq_cobr_escrit_p
  and  substr(ds_string,1,1)  = '1';
begin

select	max(a.cd_banco)
into	cd_banco_w
from	banco_estabelecimento a,
	cobranca_escritural b
where	a.nr_sequencia = b.nr_seq_conta_banco
and	b.nr_sequencia = nr_seq_cobr_escrit_p;

open C01;
loop
fetch C01 into  
	ds_titulo_w,
	vl_cobranca_w,
	vl_acrescimo_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_outras_despesas_w,
	dt_liquidacao_w,
	cd_ocorrencia_w,
	nr_nosso_numero_w;
exit when C01%notfound;
  begin

  vl_alterar_w  := 0;

  select  max(a.nr_titulo) /*Primeiro tenta achar o nr_titulo pelas 10 posicoes  */
  into  nr_titulo_w
  from  titulo_receber a
  where  a.nr_titulo  = somente_numero(ds_titulo_w); 


  if (nr_titulo_w is null) then --Se nao achou o titulo pelo numero do titulo
  
    select  max(a.nr_titulo)
    into  nr_titulo_w
    from  titulo_receber a
    where  nr_nosso_numero  = ds_titulo_w; -- Procura pelo titulo que tenha o nosso numero igual ao titulo retornado pelo banco.
  
    if (nr_titulo_w is null) then /*se ainda nao axou, procurar  pelo nosso numero do arquivo com titulos que tenham esse nosso numero*/
    
      select  max(a.nr_titulo)
      into  nr_titulo_w
      from  titulo_receber a  
      where  nr_nosso_numero  = nr_nosso_numero_w;
      
    end if;

  end if;

  /* Se encontrou o titulo importa, senao grava no log */

  if  (nr_titulo_w is not null) then

    select  NVL(MAX(vl_titulo),0),
		max(vl_saldo_titulo)
    into  vl_titulo_w,
		vl_saldo_titulo_w
    from  titulo_receber
    where  nr_titulo  = nr_titulo_w;


    select   to_char(max(a.nr_sequencia))
    into  nr_seq_ocorrencia_ret_w
    from  banco_ocorr_escrit_ret a
    where  a.cd_banco = cd_banco_w
    and  a.cd_ocorrencia = cd_ocorrencia_w;

    /* Tratar acrescimos/descontos */
    if  (vl_titulo_w <> vl_liquido_w) then
      vl_alterar_w  := vl_liquido_w - vl_titulo_w;

      if  (vl_alterar_w > 0) then
        vl_acrescimo_w  := vl_alterar_w;  
      else
        vl_desconto_w  := abs(vl_alterar_w);
      end if;
    end if;
	
	if (dt_liquidacao_w is null) or (dt_liquidacao_w = '000000') or (dt_liquidacao_w = '      ') then
		dt_liquidacao_ajustado_w := null; 
	else
		begin
			dt_liquidacao_ajustado_w := to_date(dt_liquidacao_w,'dd/mm/rrrr');
		exception when others then	
			dt_liquidacao_ajustado_w := sysdate;
		end;
	end if;

    insert  into titulo_receber_cobr (  NR_SEQUENCIA,
		      NR_TITULO,
		      CD_BANCO,
		      VL_COBRANCA,
		      VL_DESCONTO,
		      VL_ACRESCIMO,
		      VL_DESPESA_BANCARIA,
		      VL_LIQUIDACAO,
		      DT_LIQUIDACAO,
		      DT_ATUALIZACAO,
		      NM_USUARIO,
		      NR_SEQ_COBRANCA,
		      nr_seq_ocorrencia_ret,
		      vl_saldo_inclusao)
          values  (  titulo_receber_cobr_seq.nextval,
		      nr_titulo_w,
		      cd_banco_w,
		      vl_titulo_w,
		      vl_desconto_w,
		      vl_acrescimo_w,
		      vl_outras_despesas_w,
		      vl_liquido_w,
		      dt_liquidacao_ajustado_w,
		      sysdate,
		      nm_usuario_p,
		      nr_seq_cobr_escrit_p,
		      nr_seq_ocorrencia_ret_w,
		      vl_saldo_titulo_w);
  else
    --fin_gerar_log_controle_banco(3,substr('nao foi importado o titulo ' || ds_titulo_w || ', pois nao foi encontrado no Tasy',1,4000),nm_usuario_p,'N');
    --fin_gerar_log_controle_banco(3,substr(wheb_mensagem_pck.get_texto(303945,'DS_TITULO=' || ds_titulo_w),1,4000),nm_usuario_p,'N');
    gerar_log_escritural('C',nr_seq_cobr_escrit_p,nm_usuario_p,'O titulo ' || ds_titulo_w || ' nao consta no sistema!','S');
  end if;
  
  end;
end loop;
close C01;

commit;

end GERAR_RETORNO_UNICRED_400_136;
/