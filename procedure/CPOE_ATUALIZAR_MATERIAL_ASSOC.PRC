create or replace
procedure CPOE_atualizar_material_assoc( nr_seq_procedimento_p	 number) is
																				
cd_material_w				material.cd_material%type;
qt_dose_w					prescr_material.qt_dose%type;
cd_unidade_medida_w			unidade_medida.cd_unidade_medida%type;
ds_horarios_w				varchar2(4000);

cursor C01 is
select	a.cd_material,
		b.cd_unidade_medida_consumo, --unidade_medida
		nvl(a.qt_dose,0),
		a.ds_horarios
FROM	material b,
		cpoe_material a
where a.nr_seq_procedimento = nr_seq_procedimento_p
  and a.cd_material = b.cd_material
  and a.nr_ocorrencia is null
  and a.qt_unitaria is null;
	
begin

open C01;
loop
fetch C01 into	
	cd_material_w,
	cd_unidade_medida_w,
	qt_dose_w,
	ds_horarios_w;
exit when C01%notfound;	

	update cpoe_material 
	   set qt_unitaria   = obter_conversao_unid_med_cons(cd_material_w, cd_unidade_medida_w, qt_dose_w),
		   nr_ocorrencia = obter_ocorrencias_horarios_rep(ds_horarios_w)
	 where cd_material   = cd_material_w
	   and nr_seq_procedimento = nr_seq_procedimento_p;
	   
end loop;
close C01;

commit;
	
end CPOE_atualizar_material_assoc;
/