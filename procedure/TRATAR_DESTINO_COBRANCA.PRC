create or replace
procedure tratar_destino_cobranca
			(	nr_seq_cobranca_p	number,
				nr_seq_grupo_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is
cd_cgc_w		varchar2(14);
cd_pessoa_fisica_w	varchar2(10);
ie_tipo_pessoa_w	varchar2(3)	:= 'N';
vl_cobranca_w		number(15,2);
nr_seq_regra_w		number(10);
nr_seq_grupo_cobr_w	number(10);
nr_seq_cobrador_w	number(10);
nr_seq_cobr_ant_w	number(10);
qt_cobranca_w		number(10);
nr_seq_lote_w		number(10);
qt_regra_w		number(10);
nr_seq_notif_regra_w	number(10);
qt_total_cobranca_w	number(10);
nr_seq_cobr_pessoa_w	number(10)	:= null;
qt_cobranca_grupo_w	number(10);
qt_cobrador_grupo_w	number(10);
nr_seq_cobrador_ant_w	number(10);
pr_rateio_w		number(7,4);
pr_atual_cobrador_w	number(7,4);
pr_atual_grupo_w	number(7,4);
qt_cobradores_w		number(5);
dt_inclusao_w		date;
ie_cobrador_atual_w	varchar2(3);
nr_seq_notific_pagador_w number(10);
nr_seq_notificacao_w	number(10);

Cursor C01 is /* Regras */
	select	/* INDEX(REGRACO_PLSNORE_FK_I) */
		a.nr_sequencia
	from	regra_rateio_cobrador a
	where	dt_inclusao_w between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_inclusao_w)
	and	(a.ie_tipo_pessoa = ie_tipo_pessoa_w or a.ie_tipo_pessoa = 'T')
	and	vl_cobranca_w between nvl(a.vl_minimo,vl_cobranca_w) and nvl(a.vl_maximo,vl_cobranca_w)
	and	((a.nr_seq_notif_regra = nvl(nr_seq_notif_regra_w,a.nr_seq_notif_regra)) or (a.nr_seq_notif_regra is null))
	order by
		nvl(a.vl_maximo,0),
		nvl(a.vl_minimo,0),
		nvl(a.ie_tipo_pessoa,'X') desc,
		nvl(a.nr_seq_notif_regra,0);
		
Cursor C02 is /* Destinos */
	select	a.nr_seq_grupo_cobr,
		a.nr_seq_cobrador,
		a.pr_rateio
	from	regra_rateio_cobr_dest a
	where	a.nr_seq_regra		= nr_seq_regra_w
	and	((a.nr_seq_grupo_cobr 	= nvl(nr_seq_grupo_p,a.nr_seq_grupo_cobr)) or (a.nr_seq_grupo_cobr is null));
	
Cursor C03 is /* Cobradores do grupo */
	select	a.nr_seq_cobrador
	from	grupo_cobranca_membro a
	where	a.nr_seq_grupo		= nr_seq_grupo_cobr_w;
	
begin
select	count(*)
into	qt_regra_w
from	regra_rateio_cobrador	a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

if	(nr_seq_cobranca_p is not null) and
	(qt_regra_w > 0) then	
	select	cd_pessoa_fisica,
		cd_cgc,
		trunc(dt_inclusao, 'dd'),
		vl_cobranca,
		ie_tipo_pessoa,
		nr_seq_lote,
		nr_seq_cobrador,
		nr_sequencia
	into	cd_pessoa_fisica_w,
		cd_cgc_w,
		dt_inclusao_w,
		vl_cobranca_w,
		ie_tipo_pessoa_w,
		nr_seq_lote_w,
		nr_seq_cobrador_ant_w,
		nr_seq_notific_pagador_w
	from	(select	b.cd_pessoa_fisica,
			b.cd_cgc,
			decode(d.nr_seq_lote, null, a.dt_inclusao, sysdate) dt_inclusao,
			a.vl_acobrar vl_cobranca,
			decode(b.cd_pessoa_fisica,null,'PJ','PF') ie_tipo_pessoa,
			d.nr_seq_lote,
			a.nr_seq_cobrador,
			d.nr_sequencia
		from	pls_notificacao_item	c,
			pls_notificacao_pagador	d,
			titulo_receber		b,
			cobranca		a
		where	c.nr_seq_notific_pagador	= d.nr_sequencia(+)
		and	a.nr_seq_notific_item		= c.nr_sequencia(+)
		and	a.nr_titulo			= b.nr_titulo
		and	a.nr_sequencia			= nr_seq_cobranca_p
		union
		select	b.cd_pessoa_fisica,
			b.cd_cgc,
			decode(d.nr_seq_lote, null, a.dt_inclusao, sysdate) dt_inclusao,
			a.vl_acobrar vl_cobranca,
			decode(b.cd_pessoa_fisica,null,'PJ','PF') ie_tipo_pessoa,
			d.nr_seq_lote,
			a.nr_seq_cobrador,
			d.nr_sequencia
		from	pls_notificacao_item	c,
			pls_notificacao_pagador	d,
			cheque_cr		b,
			cobranca		a
		where	c.nr_seq_notific_pagador	= d.nr_sequencia(+)
		and	a.nr_seq_notific_item		= c.nr_sequencia(+)
		and	a.nr_seq_cheque			= b.nr_seq_cheque
		and	a.nr_sequencia			= nr_seq_cobranca_p);
		
	/* Primeiro limpar o cobrador da cobran�a */
	update	cobranca
	set	nr_seq_cobrador	= null
	where	nr_sequencia	= nr_seq_cobranca_p;
	
	if	(nr_seq_lote_w is not null) then
		select	max(a.nr_seq_regra)
		into	nr_seq_notif_regra_w
		from	pls_notificacao_lote a
		where	a.nr_sequencia	= nr_seq_lote_w;
	end if;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_regra_w;
	exit when C01%notfound;
		begin	
		select	count(*) /* Obter a quantidade total de cobran�as da regra */
		into	qt_total_cobranca_w
		from	pls_notificacao_pagador	c,
			pls_notificacao_item	b,
			cobranca		a
		where	b.nr_seq_notific_pagador	= c.nr_sequencia(+)
		and	a.nr_seq_notific_item		= b.nr_sequencia(+)
		and	a.nr_seq_regra_rat_cobr		= nr_seq_regra_w
		and	((c.nr_seq_lote			= nr_seq_lote_w) or (nr_seq_lote_w is null))
		and	a.ie_status			= 'P';
		
		/* Obter quantidade de cobradores da regra */
		select	count(*)
		into	qt_cobradores_w
		from	regra_rateio_cobr_dest a
		where	a.nr_seq_regra	= nr_seq_regra_w;
		
		select	nvl(max(a.ie_cobrador_atual),'S')
		into	ie_cobrador_atual_w
		from	regra_rateio_cobrador a
		where	a.nr_sequencia	= nr_seq_regra_w;
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_grupo_cobr_w,
			nr_seq_cobrador_w,
			pr_rateio_w;
		exit when C02%notfound;
			begin			
			if 	(ie_cobrador_atual_w = 'S') then
				if	(cd_pessoa_fisica_w is not null) then /* Verificar se a pessoa da cobran�a j� est� sendo cobrada por outro cobrador */					
					/* verifica se j� existe notifica��o para o pagador dentro do lote */
					select 	max(a.nr_sequencia)
					into 	nr_seq_notificacao_w
					from 	pls_notificacao_pagador a
					where	a.nr_seq_lote 	   = nr_seq_lote_w
					and	a.cd_pessoa_fisica = cd_pessoa_fisica_w
					and 	a.nr_sequencia	   <> nr_seq_notific_pagador_w;
					
					/* buscar a o cobrador da notifi��o encontrada dentro do lote */
					if	(nr_seq_notificacao_w is not null) then
						nr_seq_cobr_pessoa_w := to_number(pls_obter_dados_notif_pagador(nr_seq_notific_pagador_w,'CD'));
					end if;
					/*
					select	max(a.nr_seq_cobrador)
					into	nr_seq_cobr_pessoa_w
					from	cobranca	a,
						titulo_receber	b
					where	a.nr_titulo		= b.nr_titulo
					and	a.ie_status		= 'P'
					and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w;
					if	(nr_seq_cobr_pessoa_w is null) then
						select	max(a.nr_seq_cobrador)
						into	nr_seq_cobr_pessoa_w
						from	cobranca	a,
							cheque_cr	b
						where	a.nr_seq_cheque		= b.nr_seq_cheque
						and	a.ie_status		= 'P'
						and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w;
					end if;
					*/
				else
					/* verifica se j� existe notifica��o para o pagador dentro do lote */
					select 	max(a.nr_sequencia)
					into 	nr_seq_notificacao_w
					from 	pls_notificacao_pagador a
					where	a.nr_seq_lote 	= nr_seq_lote_w
					and	a.cd_cgc 	= cd_cgc_w
					and 	a.nr_sequencia	<> nr_seq_notific_pagador_w;
					
					/* buscar a o cobrador da notifi��o encontrada dentro do lote */
					if	(nr_seq_notificacao_w is not null) then
						nr_seq_cobr_pessoa_w := to_number(pls_obter_dados_notif_pagador(nr_seq_notific_pagador_w,'CD'));
					end if;
					/*
					select	max(a.nr_seq_cobrador)
					into	nr_seq_cobr_pessoa_w
					from	cobranca	a,
						titulo_receber	b
					where	a.nr_titulo	= b.nr_titulo
					and	a.ie_status	= 'P'
					and	b.cd_cgc	= cd_cgc_w;
					if	(nr_seq_cobr_pessoa_w is null) then
						select	max(a.nr_seq_cobrador)
						into	nr_seq_cobr_pessoa_w
						from	cobranca	a,
							cheque_cr	b
						where	a.nr_seq_cheque	= b.nr_seq_cheque
						and	a.ie_status	= 'P'
						and	b.cd_cgc	= cd_cgc_w;
					end if;
					*/
				end if;
			else 
				nr_seq_cobr_pessoa_w := null;
			end if;
		
			if	(nr_seq_cobr_pessoa_w is not null) then /* Se j� existe cobran�a em aberto para a pessoa, o mesmo cobrador dever� cobr�-la */			
				pls_inserir_cobr_alt_cobrador(	nr_seq_cobranca_p, null, nr_seq_cobr_pessoa_w, 'N', nm_usuario_p );
					
				update	cobranca
				set	nr_seq_regra_rat_cobr	= nr_seq_regra_w,
					nr_seq_cobrador		= nr_seq_cobr_pessoa_w
				where	nr_sequencia		= nr_seq_cobranca_p;
				exit;
			else	
				if	(nr_seq_grupo_cobr_w is not null) then /* Identificar para qual grupo dever� ir primeiro */
					if	(pr_rateio_w is null) then
						pr_rateio_w	:= 100 / qt_cobradores_w;
					end if;
					
					select	count(*)
					into	qt_cobrador_grupo_w
					from	grupo_cobranca_membro a
					where	a.nr_seq_grupo	= nr_seq_grupo_cobr_w;
					
					/* Verificar a quantidade cobran�as destinadas para o grupo */
					select	count(*)
					into	qt_cobranca_grupo_w
					from	pls_notificacao_item	b,
						pls_notificacao_pagador	c,
						cobranca		a
					where	b.nr_seq_notific_pagador	= c.nr_sequencia(+)
					and	a.nr_seq_notific_item		= b.nr_sequencia(+)
					and	a.ie_status			= 'P'
					and	(c.nr_seq_lote			= nr_seq_lote_w or nr_seq_lote_w is null)
					and	a.nr_seq_regra_rat_cobr		= nr_seq_regra_w
					and	(exists	(select	1
							from	grupo_cobranca_membro x
							where	x.nr_seq_cobrador	= a.nr_seq_cobrador
							and	x.nr_seq_grupo		= nr_seq_grupo_cobr_w));
							
					pr_atual_grupo_w := round(dividir_sem_round(qt_cobranca_grupo_w,qt_total_cobranca_w) * 100, 2);
					
					if	(pr_atual_grupo_w <= pr_rateio_w) then
						/* Obter qual o cobrador destino */
						open C03;
						loop
						fetch C03 into	
							nr_seq_cobrador_w;
						exit when C03%notfound;
							begin
							select	count(*)
							into	qt_cobranca_w
							from	pls_notificacao_item	b,
								pls_notificacao_pagador	c,
								cobranca		a
							where	b.nr_seq_notific_pagador	= c.nr_sequencia(+)
							and	a.nr_seq_notific_item		= b.nr_sequencia(+)
							and	a.ie_status			= 'P'
							and	(c.nr_seq_lote			= nr_seq_lote_w or nr_seq_lote_w is null)
							and	a.nr_seq_cobrador		= nr_seq_cobrador_w
							and	a.nr_seq_regra_rat_cobr		= nr_seq_regra_w;
							
							pr_rateio_w		:= 100 / qt_cobrador_grupo_w;
							pr_atual_cobrador_w	:= dividir_sem_round(qt_cobranca_w,qt_cobranca_grupo_w) * 100;
							
							if	(pr_atual_cobrador_w <= pr_rateio_w) then
								pls_inserir_cobr_alt_cobrador(	nr_seq_cobranca_p, null, nr_seq_cobrador_w, 'N', nm_usuario_p );
							
								update	cobranca
								set	nr_seq_cobrador		= nr_seq_cobrador_w,
									nr_seq_regra_rat_cobr	= nr_seq_regra_w
								where	nr_sequencia		= nr_seq_cobranca_p;
								exit;
							end if;
							end;
						end loop;
						close C03;
					end if;
				else
					if	(pr_rateio_w is null) then
						pr_rateio_w	:= 100 / qt_cobradores_w;
					end if;
					
					/* Verificar a quantidade cobran�as destinadas para o cobrador */
					select	count(*)
					into	qt_cobranca_w
					from	pls_notificacao_item	b,
						pls_notificacao_pagador	c,
						cobranca		a
					where	b.nr_seq_notific_pagador	= c.nr_sequencia(+)
					and	a.nr_seq_notific_item		= b.nr_sequencia(+)
					and	a.ie_status			= 'P'
					and	(c.nr_seq_lote			= nr_seq_lote_w or nr_seq_lote_w is null)
					and	a.nr_seq_cobrador		= nr_seq_cobrador_w
					and	a.nr_seq_regra_rat_cobr		= nr_seq_regra_w;
					
					pr_atual_cobrador_w	:= dividir_sem_round(qt_cobranca_w,qt_total_cobranca_w) * 100;
					
					if	(pr_atual_cobrador_w <= pr_rateio_w) then
						pls_inserir_cobr_alt_cobrador(	nr_seq_cobranca_p, null, nr_seq_cobrador_w, 'N', nm_usuario_p );
					
						update	cobranca
						set	nr_seq_cobrador		= nr_seq_cobrador_w,
							nr_seq_regra_rat_cobr	= nr_seq_regra_w
						where	nr_sequencia		= nr_seq_cobranca_p;
						exit;
					end if;
				end if;
			end if;
			end;
		end loop;
		close C02;
		end;
	end loop;
	close C01;
end if;
/* N�o pode dar commit nesta procedure, � chamada por outras */
end tratar_destino_cobranca;
/