create or replace
procedure atualizar_emissao_rec
	(nr_requisicao_p	number)  is

begin

update	requisicao_material 
set	dt_emissao_loc_estoque = sysdate
where	nr_requisicao = nr_requisicao_p;

commit;

end atualizar_emissao_rec;
/