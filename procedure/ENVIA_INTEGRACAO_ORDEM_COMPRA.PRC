create or replace
procedure	envia_integracao_ordem_compra(
			nr_ordem_compra_p		number,
			nm_usuario_p			varchar2) is

nr_seq_integracao_w		number(10);
nr_seq_regra_w			number(10);
ie_momento_integ_w		varchar2(2);

dt_liberacao_w			date;
dt_aprovacao_w			date;
cd_local_entrega_w		number(4);
cd_estabelecimento_w		number(5);
cd_cgc_fornecedor_w		varchar2(14);
cd_condicao_pagamento_w		number(10);
cd_comprador_w			varchar2(10);
dt_ordem_compra_w		date;
cd_moeda_w			number(5);
cd_pessoa_solicitante_w		varchar2(10);
cd_cgc_transportador_w		varchar2(14);
ie_frete_w			varchar2(1);
vl_frete_w			number(15,2);
pr_desconto_w			number(13,4);
pr_desc_pgto_antec_w		number(13,4);
pr_juros_negociado_w		number(13,4);
ds_pessoa_contato_w		varchar2(255);
ds_observacao_w			varchar2(4000);
dt_entrega_w			date;
ie_aviso_chegada_w		varchar2(1);
vl_despesa_acessoria_w		number(13,2);
nr_seq_subgrupo_compra_w		number(10);
pr_desc_financeiro_w		number(13,4);
cd_pessoa_fisica_w		varchar2(255);
ie_urgente_w			varchar2(1);
nr_seq_forma_pagto_w		number(10);
nr_documento_externo_w		number(10);
vl_desconto_w			number(13,2);
cd_centro_custo_w		number(8);

begin

select	dt_liberacao,
	dt_aprovacao,
	cd_local_entrega
into	dt_liberacao_w,
	dt_aprovacao_w,
	cd_local_entrega_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

select	max(nr_sequencia)
into	nr_seq_integracao_w
from	sup_parametro_integracao a
where	a.ie_evento = 'OC'
and	a.ie_forma = 'E'
and	a.ie_situacao = 'A';

select	nvl(max(nr_sequencia),0)
into	nr_seq_regra_w
from	sup_int_regra_oc
where	nr_seq_integracao = nr_seq_integracao_w
and	cd_local_entrega = cd_local_entrega_w
and	ie_situacao = 'A';

if	(nr_seq_regra_w <> 0) then
	begin

	select	nvl(max(ie_lib_aprov),'L')
	into	ie_momento_integ_w
	from	sup_int_regra_oc
	where	nr_sequencia = nr_seq_regra_w;

	end;
else
	begin
		
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_regra_w
	from	sup_int_regra_oc
	where	nr_seq_integracao = nr_seq_integracao_w
	and	ie_situacao = 'A';

	select	nvl(max(ie_lib_aprov),'L')
	into	ie_momento_integ_w
	from	sup_int_regra_oc
	where	nr_sequencia = nr_seq_regra_w;

	end;
end if;

if	(nr_seq_regra_w <> 0) and
	(((ie_momento_integ_w = 'A') and
	(dt_aprovacao_w is not null)) or
	((ie_momento_integ_w = 'L') and
	(dt_liberacao_w is not null))) then
	begin

	select	cd_estabelecimento,
		cd_cgc_fornecedor,
		cd_condicao_pagamento,
		cd_comprador,
		dt_ordem_compra,
		cd_moeda,
		cd_pessoa_solicitante,
		cd_cgc_transportador,
		ie_frete,
		vl_frete,
		pr_desconto,
		pr_desc_pgto_antec,
		pr_juros_negociado,
		ds_pessoa_contato,
		ds_observacao,
		dt_entrega,
		ie_aviso_chegada,
		vl_despesa_acessoria,
		nr_seq_subgrupo_compra,
		pr_desc_financeiro,
		cd_pessoa_fisica,
		ie_urgente,
		nr_seq_forma_pagto,
		nr_documento_externo,
		vl_desconto,
		cd_centro_custo
	into	cd_estabelecimento_w,
		cd_cgc_fornecedor_w,
		cd_condicao_pagamento_w,
		cd_comprador_w,
		dt_ordem_compra_w,
		cd_moeda_w,
		cd_pessoa_solicitante_w,
		cd_cgc_transportador_w,
		ie_frete_w,
		vl_frete_w,
		pr_desconto_w,
		pr_desc_pgto_antec_w,
		pr_juros_negociado_w,
		ds_pessoa_contato_w,
		ds_observacao_w,
		dt_entrega_w,
		ie_aviso_chegada_w,
		vl_despesa_acessoria_w,
		nr_seq_subgrupo_compra_w,
		pr_desc_financeiro_w,
		cd_pessoa_fisica_w,
		ie_urgente_w,
		nr_seq_forma_pagto_w,
		nr_documento_externo_w,
		vl_desconto_w,
		cd_centro_custo_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_p;

	envia_sup_int_oc(
		nr_seq_regra_w,
		nr_ordem_compra_p,
		cd_estabelecimento_w,
		cd_cgc_fornecedor_w,
		cd_condicao_pagamento_w,
		cd_comprador_w,
		dt_ordem_compra_w,
		cd_moeda_w,
		cd_pessoa_solicitante_w,
		cd_cgc_transportador_w,
		ie_frete_w,
		vl_frete_w,
		pr_desconto_w,
		pr_desc_pgto_antec_w,
		pr_juros_negociado_w,
		ds_pessoa_contato_w,
		ds_observacao_w,
		cd_local_entrega_w,
		dt_entrega_w,
		ie_aviso_chegada_w,
		vl_despesa_acessoria_w,
		nr_seq_subgrupo_compra_w,
		pr_desc_financeiro_w,
		cd_pessoa_fisica_w,
		ie_urgente_w,
		nr_seq_forma_pagto_w,
		nr_documento_externo_w,
		vl_desconto_w,
		cd_centro_custo_w,
		nm_usuario_p,
		'','','','');	

	end;
end if;

end envia_integracao_ordem_compra;
/
