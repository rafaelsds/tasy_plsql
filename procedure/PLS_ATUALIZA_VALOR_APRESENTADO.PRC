create or replace
procedure pls_atualiza_valor_apresentado(        nr_seq_lote_conta_p    pls_lote_protocolo_conta.nr_sequencia%type,
						nr_seq_protocolo_p    	pls_protocolo_conta.nr_sequencia%type,
						nr_seq_lote_processo_p  pls_cta_lote_processo.nr_sequencia%type,
						nr_seq_conta_p      	pls_conta.nr_sequencia%type,
						nr_seq_conta_proc_p    	pls_conta_proc.nr_sequencia%type,
						cd_estabelecimento_p    estabelecimento.cd_estabelecimento%type,
						nm_usuario_p        	usuario.nm_usuario%type,
						nr_seq_conta_mat_p    	pls_conta_mat.nr_sequencia%type default null
                        ) is

tb_vl_unitario_imp_w    	dbms_sql.number_table;
tb_vl_procedimento_w    	dbms_sql.number_table;
tb_seq_procedimento_w   	dbms_sql.number_table;
tb_vl_material_w      		dbms_sql.number_table;
tb_seq_material_w      		dbms_sql.number_table;
nr_indexador_w        		pls_integer;
ie_atualiza_val_apres_w   	pls_regra_val_apres.ie_atualiza_val_apres%type;
ie_atualizar_valor_apresent_w  	pls_parametros.ie_atualizar_valor_apresent%type;
ie_regra_valida_w      		varchar2(1);
ie_val_apres_conta_princ_w  	pls_regra_val_apres.ie_val_apres_conta_princ%type;
pr_proporcao_w        		number(10,5);
ie_manter_vl_apresentado_w  	pls_parametros.ie_manter_vl_apresentado%type;
ie_atual_vl_apres_e_mantem_w	pls_parametros.ie_atualiza_vl_apres_e_mantem%type;
 
cursor C01(  	nr_seq_lote_pc    	pls_lote_protocolo_conta.nr_sequencia%type,
		nr_seq_protocolo_pc  	pls_protocolo_conta.nr_sequencia%type,
		nr_seq_lote_processo_pc pls_cta_lote_processo.nr_sequencia%type,
		nr_seq_conta_pc    	pls_conta.nr_sequencia%type,
		nr_seq_conta_proc_pc  	pls_conta_proc.nr_sequencia%type,
		cd_estabelecimento_pc  	estabelecimento.cd_estabelecimento%type) is

	select  a.nr_sequencia,
		a.vl_procedimento,
		a.qt_procedimento_imp,
		(select max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and  ie_situacao = 'A'
		and  cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select   nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		(  select   max(vl_procedimento)
		from   pls_conta_proc b
		where   b.nr_sequencia = a.nr_seq_proc_ref) vl_calc_proc_princ,
		(  select  max(vl_participante)
		from  pls_proc_participante
		where  nr_sequencia = nr_seq_participante_hi) vl_participante,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_proc = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_procedimento_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_proc_v a
	where   a.nr_sequencia = nr_seq_conta_proc_pc
	and     ((a.vl_procedimento_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		 ((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_procedimento 	> 0
	union all
	select  a.nr_sequencia,
		a.vl_procedimento,
		a.qt_procedimento_imp,
		(  select max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select   nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		(  select   vl_procedimento
		from  pls_conta_proc b
		where  b.nr_sequencia = a.nr_seq_proc_ref) vl_calc_proc_princ,
		(  select  max(vl_participante)
		from  pls_proc_participante
		where  nr_sequencia = nr_seq_participante_hi) vl_participante,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_proc = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_procedimento_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_proc_v a
	where  	a.nr_seq_conta = nr_seq_conta_pc
	and    	nr_seq_conta_proc_pc   is null
	and     ((a.vl_procedimento_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_procedimento 	> 0
	union all
	select  a.nr_sequencia,
		a.vl_procedimento,
		a.qt_procedimento_imp,
		(  select   max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select   nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		(  select   vl_procedimento
		from  pls_conta_proc b
		where  b.nr_sequencia = a.nr_seq_proc_ref) vl_calc_proc_princ,
		(  select  max(vl_participante)
		from  pls_proc_participante
		where  nr_sequencia = nr_seq_participante_hi) vl_participante,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_proc = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_procedimento_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_proc_v a
	where  	a.nr_seq_protocolo = nr_seq_protocolo_pc
	and    	nr_seq_conta_pc   is null
	and    	nr_seq_conta_proc_pc  is null
	and     ((a.vl_procedimento_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_procedimento 	> 0
	union all
	select  a.nr_sequencia,
		a.vl_procedimento,
		a.qt_procedimento_imp,
		(  select   max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select   nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		(  select   vl_procedimento
		from  pls_conta_proc b
		where  b.nr_sequencia = a.nr_seq_proc_ref) vl_calc_proc_princ,
		(  select  max(vl_participante)
		from  pls_proc_participante
		where  nr_sequencia = nr_seq_participante_hi) vl_participante,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_proc = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_procedimento_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_proc_v a
	where  	a.nr_seq_lote_conta = nr_seq_lote_pc
	and    	nr_seq_conta_proc_pc  is null
	and    	nr_seq_conta_pc   is null
	and    	nr_seq_protocolo_pc   is null
	and    	((a.vl_procedimento_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_procedimento 	> 0
	union all
	select  a.nr_sequencia,
		a.vl_procedimento,
		a.qt_procedimento_imp,
		(  select   max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select   nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		(  select   vl_procedimento
		from   pls_conta_proc b
		where   b.nr_sequencia = a.nr_seq_proc_ref) vl_calc_proc_princ,
		(  select    max(vl_participante)
		from    pls_proc_participante
		where    nr_sequencia = nr_seq_participante_hi) vl_participante,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_proc = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_procedimento_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_proc_v a
	where  	nr_seq_conta_pc   is null
	and  	nr_seq_conta_proc_pc  is null
	and  	nr_seq_protocolo_pc   is null
	and  	nr_seq_lote_pc     is null
	and     ((a.vl_procedimento_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_procedimento 	> 0
	and  	exists (  select  1
			from  pls_cta_lote_proc_conta p
			where  p.nr_seq_lote_processo = nr_seq_lote_processo_pc
			and    p.nr_seq_conta = a.nr_seq_conta);

cursor C02(  nr_seq_lote_pc    	pls_lote_protocolo_conta.nr_sequencia%type,
	nr_seq_protocolo_pc  	pls_protocolo_conta.nr_sequencia%type,
	nr_seq_lote_processo_pc pls_cta_lote_processo.nr_sequencia%type,
	nr_seq_conta_pc    	pls_conta.nr_sequencia%type,
	nr_seq_conta_mat_pc  	pls_conta_mat.nr_sequencia%type,
	cd_estabelecimento_pc  	estabelecimento.cd_estabelecimento%type) is

	select  a.nr_sequencia,
		a.vl_material,
		a.qt_material_imp,
		(  select   max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select  nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_mat = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_material_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_mat_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from   	pls_conta_mat_v a
	where   a.nr_sequencia = nr_seq_conta_mat_pc
	and     ((a.vl_material_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_material > 0
	union all
	select  a.nr_sequencia,
		a.vl_material,
		a.qt_material_imp,
		(  select   max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select   nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_mat = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_material_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_mat_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_mat_v a
	where  	a.nr_seq_conta = nr_seq_conta_pc
	and    	nr_seq_conta_mat_pc   is null
	and     ((a.vl_material_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_material > 0
	union all
	select  a.nr_sequencia,
		a.vl_material,
		a.qt_material_imp,
		(  select max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and  ie_situacao = 'A'
		and  cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and  nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_mat = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_material_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_mat_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_mat_v a
	where  	a.nr_seq_protocolo = nr_seq_protocolo_pc
	and    	nr_seq_conta_pc   	is null
	and    	nr_seq_conta_mat_pc  	is null
	and    	((a.vl_material_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_material > 0
	union all
	select  a.nr_sequencia,
		a.vl_material,
		a.qt_material_imp,
		(  select max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select   nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_mat = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_material_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_mat_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_mat_v a
	where  	a.nr_seq_lote_conta = nr_seq_lote_pc
	and    	nr_seq_conta_mat_pc  	is null
	and    	nr_seq_conta_pc   	is null
	and    	nr_seq_protocolo_pc   	is null
	and     ((a.vl_material_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and	a.vl_material > 0
	union all
	select  a.nr_sequencia,
		a.vl_material,
		a.qt_material_imp,
		(  select max(ie_atualiza_val_apres)
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_atualiza_val_apres,
		(  select   nvl(min(ie_gerada_cta_hon),'A')
		from  pls_regra_val_apres b
		where  b.ie_origem_protocolo = a.ie_origem_protocolo
		and    ie_situacao = 'A'
		and    nvl(ie_gerada_cta_hon,'A') in('A','S')
		and    cd_estabelecimento = cd_estabelecimento_pc) ie_gerada_cta_hon,
		nr_seq_regra_conta_auto,
		(   select   max(nvl(nr_lote_contabil_prov,0))
		from  pls_conta_medica_resumo x
		where   nr_seq_conta = a.nr_seq_conta
		and    nr_seq_conta_mat = a.nr_sequencia) nr_lote_contabil,
		nr_lote_contabil_prov,
		a.ie_origem_conta,
		a.vl_material_imp,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_mat_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		a.dt_fim_consistencia
	from  	pls_conta_mat_v a
	where  	nr_seq_conta_pc   	is null
	and    	nr_seq_conta_mat_pc  	is null
	and    	nr_seq_protocolo_pc   	is null
	and    	nr_seq_lote_pc     	is null
	and	a.vl_material > 0
	and     ((a.vl_material_imp    = 0) or (ie_vl_apresentado_sistema = 'S') or
		((a.ie_vl_apres_sist_regra = 'S') and ( ie_atual_vl_apres_e_mantem_w = 'S')))
	and  	exists (  select  1
			from  pls_cta_lote_proc_conta p
			where  p.nr_seq_lote_processo = nr_seq_lote_processo_pc
			and    p.nr_seq_conta = a.nr_seq_conta);


procedure atualiza_valores_procedimentos(  	tb_nr_seq_p      dbms_sql.number_table,
						tb_vl_proc_p     dbms_sql.number_table,
						tb_vl_unit_p     dbms_sql.number_table
          ) is
begin

	if  (tb_nr_seq_p.count > 0) then
		forall i in tb_nr_seq_p.first..tb_nr_seq_p.last
			update  pls_conta_proc
			set    	vl_procedimento_imp             = tb_vl_proc_p(i),
				ie_vl_apresentado_sistema       = 'S',
				vl_unitario_imp                 = tb_vl_unit_p(i)
			where  	nr_sequencia  = tb_nr_seq_p(i);

		commit;
	end if;
	
	if  (tb_nr_seq_p.count > 0) then
		forall i in tb_nr_seq_p.first..tb_nr_seq_p.last
			update  pls_conta_proc_regra
			set    	ie_vl_apresentado_sistema       = 'S'
			where  	nr_sequencia  = tb_nr_seq_p(i);

		commit;
	end if;
	
end atualiza_valores_procedimentos;

procedure atualiza_valores_materiais(  	tb_nr_seq_p  dbms_sql.number_table,
					tb_vl_mat_p  dbms_sql.number_table,
					tb_vl_unit_p dbms_sql.number_table
         ) is
begin
	if  (tb_nr_seq_p.count > 0) then
		forall i in tb_nr_seq_p.first..tb_nr_seq_p.last
			update 	pls_conta_mat
			set    	vl_material_imp               = tb_vl_mat_p(i),
				ie_vl_apresentado_sistema     = 'S',
				vl_unitario_imp               = tb_vl_unit_p(i)
	    where  		nr_sequencia  		      = tb_nr_seq_p(i);

	  commit;
	end if;
	
	if  (tb_nr_seq_p.count > 0) then
		forall i in tb_nr_seq_p.first..tb_nr_seq_p.last
			update 	pls_conta_mat_regra
			set    	ie_vl_apresentado_sistema     = 'S'
	    where  		nr_sequencia  		      = tb_nr_seq_p(i);

	  commit;
	end if;
	
end atualiza_valores_materiais;

begin

select  nvl(ie_atualizar_valor_apresent,'N'),
	nvl(ie_manter_vl_apresentado, 'N'),
	nvl(ie_atualiza_vl_apres_e_mantem, 'N')
into  	ie_atualizar_valor_apresent_w,
	ie_manter_vl_apresentado_w,
	ie_atual_vl_apres_e_mantem_w
from  	table(pls_parametros_pck.f_retorna_param(cd_estabelecimento_p));

if  (nr_seq_conta_mat_p is null) then

	nr_indexador_w := 0;
	tb_vl_unitario_imp_w.delete;
	tb_vl_procedimento_w.delete;
	tb_seq_procedimento_w.delete;

	for r_C01_w in  C01(nr_seq_lote_conta_p, nr_seq_protocolo_p, nr_seq_lote_processo_p, nr_seq_conta_p, nr_seq_conta_proc_p, cd_estabelecimento_p) loop

		ie_regra_valida_w := 'N';
		ie_val_apres_conta_princ_w := 'N';
		pr_proporcao_w := 1;

		if  (nvl(r_C01_w.ie_gerada_cta_hon,'A') = 'A') then
			ie_regra_valida_w := 'S';
		elsif  ((r_C01_w.ie_gerada_cta_hon = 'S') and (r_C01_w.nr_seq_regra_conta_auto is not null)) then
			ie_regra_valida_w := 'S';
		else
			if   (r_C01_w.nr_seq_regra_conta_auto is null) then
				ie_regra_valida_w := 'S';
			end if;

		end if;
	
		if ((ie_manter_vl_apresentado_w = 'N') or ((ie_manter_vl_apresentado_w = 'S') and (nvl(r_C01_w.nr_lote_contabil,0) = 0) and (nvl(r_c01_w.nr_lote_contabil_prov,0) = 0))) then

			if (ie_regra_valida_w = 'S') then
			
				if  (ie_atualizar_valor_apresent_w = 'S' or (ie_atualizar_valor_apresent_w = 'R' and r_C01_w.ie_atualiza_val_apres = 'S')) then

					if  (r_C01_w.nr_seq_regra_conta_auto is not null) then
						select   max(ie_val_apres_conta_princ)
						into  	ie_val_apres_conta_princ_w
						from  	pls_regra_val_apres
						where  	ie_situacao = 'A'
						and  	nvl(ie_gerada_cta_hon,'A') = r_C01_w.ie_gerada_cta_hon
						and  	cd_estabelecimento = cd_estabelecimento_p;

						if  (ie_val_apres_conta_princ_w = 'S') then
							pr_proporcao_w := dividir(r_C01_w.vl_participante, r_C01_w.vl_calc_proc_princ);
						end if;

					end if;

					if	(pls_obter_se_a504_glosado(r_C01_w.ie_a520, r_C01_w.ie_origem_conta, r_C01_w.vl_procedimento_imp) = 'N') then
					
						if  not (ie_atual_vl_apres_e_mantem_w = 'S' and r_C01_w.dt_fim_consistencia is not null) then
					
							tb_vl_unitario_imp_w(nr_indexador_w)    := dividir((r_C01_w.vl_procedimento * pr_proporcao_w), r_C01_w.qt_procedimento_imp);
							tb_vl_procedimento_w(nr_indexador_w)  := r_C01_w.vl_procedimento * pr_proporcao_w;
							tb_seq_procedimento_w(nr_indexador_w)   := r_C01_w.nr_sequencia;
							
						else
							tb_vl_unitario_imp_w(nr_indexador_w)    := dividir((r_C01_w.vl_procedimento_imp * pr_proporcao_w), r_C01_w.qt_procedimento_imp);
							tb_vl_procedimento_w(nr_indexador_w)  := r_C01_w.vl_procedimento_imp * pr_proporcao_w;
							tb_seq_procedimento_w(nr_indexador_w)   := r_C01_w.nr_sequencia;
						end if;

						if  (nr_indexador_w >= pls_util_cta_pck.qt_registro_transacao_w) then

							atualiza_valores_procedimentos(tb_seq_procedimento_w, tb_vl_procedimento_w, tb_vl_unitario_imp_w);

							nr_indexador_w := 0;
							tb_vl_unitario_imp_w.delete;
							tb_vl_procedimento_w.delete;
							tb_seq_procedimento_w.delete;
						else
							nr_indexador_w := nr_indexador_w + 1;
						end if;
					end if;
					
				end if;
			end if;
		end if;

	end loop;

	atualiza_valores_procedimentos(tb_seq_procedimento_w, tb_vl_procedimento_w, tb_vl_unitario_imp_w);
end if;

if  	(nr_seq_conta_proc_p is null) then

	nr_indexador_w := 0;
	tb_vl_unitario_imp_w.delete;
	tb_vl_material_w.delete;
	tb_seq_material_w.delete;

	for r_C02_w in  C02(nr_seq_lote_conta_p, nr_seq_protocolo_p, nr_seq_lote_processo_p, nr_seq_conta_p, nr_seq_conta_mat_p, cd_estabelecimento_p) loop

		ie_regra_valida_w := 'N';
		ie_val_apres_conta_princ_w := 'N';

		if  (nvl(r_C02_w.ie_gerada_cta_hon,'A') = 'A') then
			ie_regra_valida_w := 'S';
		elsif  ((r_C02_w.ie_gerada_cta_hon = 'S') and (r_C02_w.nr_seq_regra_conta_auto is not null)) then
			ie_regra_valida_w := 'S';
		else
			if   (r_C02_w.nr_seq_regra_conta_auto is null) then
			ie_regra_valida_w := 'S';
			end if;
		end if;

		if 	((ie_manter_vl_apresentado_w = 'N') or ((ie_manter_vl_apresentado_w = 'S')) and (nvl(r_C02_w.nr_lote_contabil,0) = 0) and (nvl(r_c02_w.nr_lote_contabil_prov,0) = 0)) then
			if (ie_regra_valida_w = 'S') then

				if  	(ie_atualizar_valor_apresent_w = 'S' or (ie_atualizar_valor_apresent_w = 'R' and r_C02_w.ie_atualiza_val_apres = 'S')) then
				

					if	(pls_obter_se_a504_glosado(r_C02_w.ie_a520, r_C02_w.ie_origem_conta, r_C02_w.vl_material_imp) = 'N') then
					
						if  not (ie_atual_vl_apres_e_mantem_w = 'S' and r_C02_w.dt_fim_consistencia is not null) then
						
							tb_vl_unitario_imp_w(nr_indexador_w)    := dividir(r_C02_w.vl_material, r_C02_w.qt_material_imp);
							tb_vl_material_w(nr_indexador_w)  := r_C02_w.vl_material;
							tb_seq_material_w(nr_indexador_w)   := r_C02_w.nr_sequencia;
							
						else
							tb_vl_unitario_imp_w(nr_indexador_w)    := dividir(r_C02_w.vl_material_imp, r_C02_w.qt_material_imp);
							tb_vl_material_w(nr_indexador_w)  := r_C02_w.vl_material_imp;
							tb_seq_material_w(nr_indexador_w)   := r_C02_w.nr_sequencia;
							
						end if;

						if  (nr_indexador_w >= pls_util_cta_pck.qt_registro_transacao_w) then
							atualiza_valores_materiais(tb_seq_material_w, tb_vl_material_w, tb_vl_unitario_imp_w);

							nr_indexador_w := 0;
							tb_vl_unitario_imp_w.delete;
							tb_vl_material_w.delete;
							tb_seq_material_w.delete;
						else
							nr_indexador_w := nr_indexador_w + 1;
						end if;
					end if;
					
				end if;
			end if;
		end if;

	end loop;

	atualiza_valores_materiais(tb_seq_material_w, tb_vl_material_w, tb_vl_unitario_imp_w);
end if;

end pls_atualiza_valor_apresentado;
/
