create or replace
procedure pls_obter_restr_envio_fat_coop(nr_seq_cooperativa_p	number,
					nr_seq_congenere_p	number,
					ie_tipo_intercambio_p	varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2,
					ie_permite_p		out varchar2) is 

ie_fatura_congenere_coop_w	varchar2(1);
					
Cursor C01 is
	select	ie_permite
	from	pls_congenere_rec_fat
	where	nvl(nr_seq_cooperativa,nr_seq_cooperativa_p)	= nr_seq_cooperativa_p
	and	(ie_tipo_intercambio = ie_tipo_intercambio_p or ie_tipo_intercambio = 'A' or ie_tipo_intercambio_p = 'A')
	and	nr_seq_congenere = nr_seq_congenere_p;
					
begin
begin
select	ie_fatura_congenere_coop
into	ie_fatura_congenere_coop_w
from	pls_congenere
where	nr_sequencia = nr_seq_congenere_p;
exception
when others then
	ie_fatura_congenere_coop_w := 'N';
end;

if	(nvl(ie_fatura_congenere_coop_w,'N') = 'S') then
	open C01;
	loop
	fetch C01 into	
		ie_permite_p;
	exit when C01%notfound;
		begin
		null;
		end;
	end loop;
	close C01;
else
	ie_permite_p := 'S';
end if;
	
end pls_obter_restr_envio_fat_coop;
/