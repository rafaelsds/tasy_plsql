create or replace
procedure obter_regra_atributo_wfiltro(
				nr_seq_wfiltro_p	number,
				cd_estabelecimento_p	number,
				cd_perfil_p		number,
				cd_setor_atendimento_p	number,
				cd_funcao_p		number,
				nm_usuario_regra_p	varchar2,
				ds_retorno_p	out	varchar2) is 

nm_atributo_w	varchar2(50);
ie_readonly_w	varchar2(1);
vl_padrao_w	varchar2(255);

Cursor C01 is
	select	b.nm_atributo,
		a.ie_readonly,
		a.vl_padrao
	from	atrib_regra_wfiltro a,
		dic_objeto_filtro b
	where	a.nr_seq_wfiltro = nr_seq_wfiltro_p
	and	a.nr_seq_atrib_wfiltro = b.nr_sequencia
	and	nvl(a.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
	and	nvl(a.cd_perfil, cd_perfil_p) = cd_perfil_p
	and	nvl(a.cd_setor_atendimento, cd_setor_atendimento_p) = cd_setor_atendimento_p
	and	nvl(a.cd_funcao, cd_funcao_p) = cd_funcao_p
	and	nvl(a.nm_usuario_regra, nm_usuario_regra_p) = nm_usuario_regra_p;
	
begin

ds_retorno_p := '<?xml version="1.0" encoding="ISO-8859-1"?><regras>';

open C01;
loop
fetch C01 into	
	nm_atributo_w,
	ie_readonly_w,
	vl_padrao_w;
exit when C01%notfound;
	begin
	
	ds_retorno_p := ds_retorno_p || '<regra>';
	
	ds_retorno_p := ds_retorno_p || '<nmAtributo>' || nm_atributo_w || '</nmAtributo>';
	ds_retorno_p := ds_retorno_p || '<vlPadrao>' || vl_padrao_w || '</vlPadrao>';
	ds_retorno_p := ds_retorno_p || '<ieReadonly>' || ie_readonly_w || '</ieReadonly>';
	
	ds_retorno_p := ds_retorno_p || '</regra>';
	
	end;
end loop;
close C01;

ds_retorno_p := ds_retorno_p || '</regras>';

end obter_regra_atributo_wfiltro;
/