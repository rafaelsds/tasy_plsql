create or replace
procedure mprev_gerar_ciclo_atend(	nr_seq_participante_p		number,
									nr_seq_prog_partic_modulo_p	number,
									dt_referencia_p				date,
									nm_usuario_p				varchar2,
									is_score_flex_p				varchar2 default 'N') is 
			
nr_seq_plano_atend_w			mprev_plano_atendimento.nr_sequencia%type;
ie_tipo_periodo_ciclo_w			mprev_plano_atendimento.ie_tipo_periodo_ciclo%type;
qt_total_periodo_ciclo_w		mprev_plano_atendimento.qt_total_periodo_ciclo%type;
nr_seq_ciclo_atend_w			mprev_partic_ciclo_atend.nr_sequencia%type;
dt_fim_ciclo_ant_w				date;
dt_fim_ciclo_w					date;
dt_inicio_ciclo_w				date;
dt_prevista_dia_util_w			date;
nr_atendimento_ciclo_w			mprev_plano_atend_item.nr_atendimento_ciclo%type;
dt_referencia_w					date;
nr_seq_programa_w				mprev_programa.nr_sequencia%type;
nr_seq_partic_ciclo_item_w		mprev_partic_ciclo_item.nr_sequencia%type;
nr_seq_partic_ciclo_item_ww		mprev_partic_ciclo_item.nr_sequencia%type;
nm_programa_w					mprev_programa.nm_programa%type;
nm_modulo_w						mprev_modulo_atend.ds_modulo%type;
nr_seq_ciclo_atend_ant_w		mprev_partic_ciclo_atend.nr_sequencia%type;
ds_erro_w						varchar2(1000);
dt_saida_modulo_w				date;
qt_cic_vigentes_w				pls_integer;
ie_gerar_profi_prog_partic_w	mprev_prog_partic_modulo.ie_gerar_profi_prog_partic%type;

Cursor	c01(nr_seq_participante_pc			mprev_participante.nr_sequencia%type, 
			nr_seq_prog_partic_modulo_pc	mprev_prog_partic_modulo.nr_sequencia%type, 
			dt_referencia_pc				date) is			
	select	a.nr_sequencia nr_seq_prog_partic_mod,
		a.nr_seq_classificacao nr_seq_classificacao,
		b.nr_seq_modulo nr_seq_modulo,
		a.dt_entrada dt_entrada_modulo,
		b.nr_seq_programa nr_seq_programa,
		b.nr_sequencia nr_seq_prog_modulo,
		a.ie_cancelamento ie_cancelamento,
		d.nr_seq_agrupamento nr_seq_agrupamento,
		a.ie_data_final ie_data_final,
		a.dt_saida dt_saida_modulo,
		a.nr_seq_programa_partic
	from	mprev_prog_partic_modulo a,
		mprev_programa_modulo b,
		mprev_programa_partic c,
		mprev_modulo_atend d
	where	a.nr_seq_programa_partic	= c.nr_sequencia
	and	b.nr_sequencia			= a.nr_seq_prog_modulo
	and	b.nr_seq_modulo			= d.nr_sequencia
	and	c.nr_seq_participante		= nr_seq_participante_pc
	and	((a.nr_sequencia		= nr_seq_prog_partic_modulo_pc) or (nr_seq_prog_partic_modulo_pc is null))
	/* Se passar o modulo, deixar gerar com data futura */
	and	(dt_referencia_pc between a.dt_entrada and nvl(a.dt_saida,dt_referencia_pc) or nvl(nr_seq_prog_partic_modulo_pc,0) > 0)
	/* Porem n?o pode ficar gerando mais do que um dai */
	and	(not exists	(	select	1
						from	mprev_partic_ciclo_atend x
						where	x.nr_seq_prog_partic_mod = a.nr_sequencia) or nvl(nr_seq_prog_partic_modulo_pc,0) = 0);

Cursor c02( nr_seq_plano_atend_pc mprev_plano_atendimento.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_plano_atend_item,
			b.nr_atendimento_ciclo nr_atendimento_ciclo,
			b.ie_tipo_intervalo ie_tipo_intervalo,
			b.qt_intervalo_anterior qt_intervalo_anterior,
			b.ie_forma_atend_prev ie_forma_atend_prev
	from	mprev_plano_atend_item b,
			mprev_plano_atendimento c
	where 	b.nr_seq_plano_atend  = c.nr_sequencia
	and		c.nr_sequencia	= nr_seq_plano_atend_pc
	order by b.nr_atendimento_ciclo;
	
begin
dt_referencia_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate);

/*
Caso queira testar data especifica
dt_referencia_w	:= to_date('01/02/2013','dd/mm/yyyy');
*/

/* Obter a classificac?o do participante no modulo, para obter qual o plano de atendimento
que se encaixa, cursor para passar por todos os programas */


for r_c01 in c01(	nr_seq_participante_p, 
			nr_seq_prog_partic_modulo_p, 
			dt_referencia_w) loop
			
	begin
		/* Verificar se existe outro ciclo vigente para o mesmo modulo e mesmo participante */
		select	count(*)
		into	qt_cic_vigentes_w
		from	MPREV_PROG_PARTIC_MODULO a
		where	nr_sequencia <> nr_seq_prog_partic_modulo_p
		and		DT_CANCELAMENTO is null
		and		DT_GERACAO_PRIM_CICLO is not null
		and		NR_SEQ_PROGRAMA_PARTIC = r_c01.nr_seq_programa_partic
		and		NR_SEQ_PROG_MODULO = r_c01.nr_seq_prog_modulo
		and		r_c01.dt_entrada_modulo
				between pkg_date_utils.start_of(DT_ENTRADA, 'DAY') and nvl(DT_SAIDA, sysdate);
					
		select	a.nr_sequencia,
				a.ie_tipo_periodo_ciclo,
				a.qt_total_periodo_ciclo
		into	nr_seq_plano_atend_w,
				ie_tipo_periodo_ciclo_w,
				qt_total_periodo_ciclo_w	
		from	mprev_plano_atendimento a
		where	a.nr_seq_modulo	= r_c01.nr_seq_modulo
		and		(a.nr_seq_classificacao = r_c01.nr_seq_classificacao or a.nr_seq_classificacao is null)
		and		ie_situacao = 'A';
		
		exception
			when others then
				ie_tipo_periodo_ciclo_w		:= null;
				qt_total_periodo_ciclo_w	:= null;
	end;
	
	/* A mensagem foi colocada do lado de fora do for, pois o cursor tem exception, que n?o estouraria a mensagem */
	if (nvl(is_score_flex_p, 'N') = 'N' and qt_cic_vigentes_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1068817);
	end if;
		
	if	( nr_seq_plano_atend_w is not null) then
		/* Verificar se a data atual ja esta dentro de um ciclo do participante, caso contrario tem que gerar um novo */
		select	max(a.nr_sequencia)
		into	nr_seq_ciclo_atend_w
		from	mprev_partic_ciclo_atend a
		where	a.nr_seq_prog_partic_mod = r_c01.nr_seq_prog_partic_mod
		and		a.nr_seq_plano_atend	= nr_seq_plano_atend_w
		and		dt_referencia_w between a.dt_inicio_ciclo and a.dt_fim_ciclo;
		
		/* Se n?o achou, criar um novo ciclo */
		if	(nr_seq_ciclo_atend_w is null) then		
			/* A data de inicio do ciclo vai ser a data de fim do ciclo anterior mais um dia
			Se n?o tiver ciclo anterior, considera sysdate */
			select	max(a.dt_fim_ciclo)
			into	dt_fim_ciclo_ant_w
			from	mprev_partic_ciclo_atend a
			where	a.nr_seq_prog_partic_mod = r_c01.nr_seq_prog_partic_mod;
			
			if	(dt_fim_ciclo_ant_w is null) then
				dt_inicio_ciclo_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(r_c01.dt_entrada_modulo);
			else
				dt_inicio_ciclo_w	:= dt_fim_ciclo_ant_w;
			end if;
			
			dt_inicio_ciclo_w := obter_proximo_dia_util(wheb_usuario_pck.get_cd_estabelecimento, dt_inicio_ciclo_w);
			
			/*--Busca data fim do ciclo conforme o periodo cadastrado para o plano de atendimento*/
			dt_fim_ciclo_w	:= mprev_obter_data_fim_ciclo(	ie_tipo_periodo_ciclo_w, 
									dt_inicio_ciclo_w, 
									qt_total_periodo_ciclo_w );
			
			select	mprev_partic_ciclo_atend_seq.nextval
			into	nr_seq_ciclo_atend_w
			from	dual;
		
			insert into mprev_partic_ciclo_atend
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_prog_partic_mod,
				nr_seq_plano_atend,
				dt_inicio_ciclo,
				dt_fim_ciclo)
			values	(nr_seq_ciclo_atend_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				r_c01.nr_seq_prog_partic_mod,
				nr_seq_plano_atend_w,
				dt_inicio_ciclo_w,
				dt_fim_ciclo_w);
						
			nr_atendimento_ciclo_w	:= 0;
			
			/*--Grava data de gerac?o do primeiro ciclo */
			update	mprev_prog_partic_modulo
			set	dt_geracao_prim_ciclo = sysdate
			where	nr_sequencia = r_c01.nr_seq_prog_partic_mod
			and	dt_geracao_prim_ciclo is null;	
		
			/*Busca a sequencia do ciclo de atendimento anterior ao que esta gerando. Verifica se e do mesmo modulo ou o modulo pertence ao mesmo agrupamento*/
			nr_seq_ciclo_atend_ant_w	:= mprev_obter_seq_ciclo_ant(	r_c01.nr_seq_prog_partic_mod);
			
			if	(nr_seq_ciclo_atend_ant_w is not null) or 
                                (r_c01.ie_data_final = 'DF') then
			
				/*--Grava a sequencia do ciclo do atendimento anterior no novo ciclo. */
				update	mprev_partic_ciclo_atend
				set	nr_seq_ciclo_ant = nr_seq_ciclo_atend_ant_w
				where	nr_sequencia = nr_seq_ciclo_atend_w;
				
				/*--Cancela os atendimentos do ciclo do atendimento anterior. */
				mprev_cancelar_atend_ciclo(	nr_seq_ciclo_atend_ant_w,
								r_c01.nr_seq_modulo,		
								dt_inicio_ciclo_w, 	
								nr_seq_participante_p,	
								r_c01.nr_seq_agrupamento,
								r_c01.ie_cancelamento,
								null,
								nm_usuario_p );

				/*--Grava data de saida do modulo de atendimento conforme radiogroup "Cancelamento de atendimentos previstos anteriores" da tabela MPREV_PROG_PARTIC_MODULO
				que por padr?o e carregado com os valores do mesmo campo contido na tabela MPREV_MODULO_ATEND
				(N,DF,DA)(N?o carregar, Data de fim do ciclo de atendimento gerado, Data de fim do ciclo atendimento anterior.*/
				mprev_gravar_data_saida_modulo (	r_c01.nr_seq_prog_partic_mod,
									nr_seq_ciclo_atend_ant_w,
									r_c01.ie_data_final,
									dt_fim_ciclo_w );
				
			end if;
			
			/* Atualizar data de saida do modulo */
			select	a.dt_saida,
					a.ie_gerar_profi_prog_partic
			into	dt_saida_modulo_w,
					ie_gerar_profi_prog_partic_w
			from	mprev_prog_partic_modulo a
			where	a.nr_sequencia	= r_c01.nr_seq_prog_partic_mod;
			
			if	(dt_saida_modulo_w is not null) then
				update	mprev_partic_ciclo_atend
				set	dt_fim_ciclo	= dt_saida_modulo_w
				where	nr_sequencia	= nr_seq_ciclo_atend_w;
			end if;
			
			for r_c02 in c02( nr_seq_plano_atend_w ) loop
				
				if	(nr_seq_partic_ciclo_item_ww is not null) then
					select	dt_prevista
					into	dt_inicio_ciclo_w
					from	mprev_partic_ciclo_item
					where	nr_sequencia = nr_seq_partic_ciclo_item_ww;
				end if;
				/*--Retorna  a data pervista de inicio de cada item do plano de atendimento. Retorna dia util. */
				dt_prevista_dia_util_w	:= mprev_obter_data_prev_ciclo( r_c02.ie_tipo_intervalo, 
											dt_inicio_ciclo_w, 
											r_c02.qt_intervalo_anterior );
											
				/* Consistir se n?o supera a data de saida do modulo, se n?o nem ira gerar */
				if	(dt_prevista_dia_util_w <= dt_saida_modulo_w) or
					(dt_saida_modulo_w is null) then
				
					/*--Verifica se a data presita e maior que a dara final do fim do ciclo. */
					if	( dt_prevista_dia_util_w > dt_fim_ciclo_w ) then
						
						--Busca nome do modulo					
						nm_modulo_w	:= mprev_obter_desc_prog_modulo( r_c01.nr_seq_prog_modulo );
						--Busca nome do programa
						select	a.nm_programa
						into	nm_programa_w
						from 	mprev_programa a
						where 	a.nr_sequencia = r_c01.nr_seq_programa;
						
						/*Atendimento n?. #@NR_ATENDIMENTO_CICLO#@ do modulo #@NM_MODULO#@ DO PROGRAMA #@NM_PROGRAMA#@ 
						previsto para dia #@DT_INICIO_CICLO#@ esta fora do intervalo do ciclo que termina em #@DT_FIM_CICLO#@. 
						Favor verificar na func?o HDM - Cadastros Gerais -> Modulo de atendimento -> Plano de atendimento.*/
						ds_erro_w	:= obter_texto_dic_objeto(	338831, 
												wheb_usuario_pck.get_nr_seq_idioma, 
												'NR_ATENDIMENTO_CICLO='||r_c02.nr_atendimento_ciclo||
												';NM_MODULO='||nm_modulo_w||
												';NM_PROGRAMA='||nm_programa_w||
												';DT_INICIO_CICLO='||dt_prevista_dia_util_w||
												';DT_FIM_CICLO='||dt_fim_ciclo_w);
						
						/*Se nr_seq_prog_partic_modulo_p ser null e porque a procedure foi chamada pela JOB, grava log de erro.*/
						if	(nr_seq_prog_partic_modulo_p is null) then
							gravar_log_tasy(47,ds_erro_w,nm_usuario_p);
						else
							wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_w);
						end if;
					end if;
				
					select	mprev_partic_ciclo_item_seq.nextval
					into	nr_seq_partic_ciclo_item_ww
					from	dual;
					
					insert into mprev_partic_ciclo_item
						(nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						dt_prevista, 
						ie_status,
						nr_atendimento_ciclo, 
						nr_seq_partic_ciclo_atend, 
						nr_seq_plano_atend_item,
						ie_forma_atend_prev)
					values	(nr_seq_partic_ciclo_item_ww,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						dt_prevista_dia_util_w, 
						'P',
						r_c02.nr_atendimento_ciclo, 
						nr_seq_ciclo_atend_w, 
						r_c02.nr_seq_plano_atend_item,
						r_c02.ie_forma_atend_prev);
						
					mprev_gerar_ciclo_atend_resp(nr_seq_partic_ciclo_item_ww, nm_usuario_p, ie_gerar_profi_prog_partic_w);
					mprev_gerar_ciclo_atend_ativ(nr_seq_partic_ciclo_item_ww, nm_usuario_p);

				end if; -- Consistencia data saida modulo
			end loop;			
		end if;
	else
		/* N?o foi possivel encontrar um plano de atendimento para a classificac?o do paciente neste modulo */
		ds_erro_w	:= obter_texto_dic_objeto(264555, wheb_usuario_pck.get_nr_seq_idioma, null);
		
		if	(nr_seq_prog_partic_modulo_p is null) then
			gravar_log_tasy(47,ds_erro_w,nm_usuario_p);
		else
			wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_w);	
		end if;
	end if;
end loop;

end mprev_gerar_ciclo_atend;
/
