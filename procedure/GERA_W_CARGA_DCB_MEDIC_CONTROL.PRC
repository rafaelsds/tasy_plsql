create or replace
procedure gera_w_carga_dcb_medic_control(	cd_dcb_p	varchar2,
					ds_dcb_p		varchar2,
					nr_cas_p		varchar2,
					ie_deleta_p	varchar2,
					nm_usuario_p	varchar2) is 

begin

if	(ie_deleta_p = 'S') then
	delete from w_carga_dcb_medic_control;
	gravar_carga_dcb_historico(
		'Limpeza na tabela W_CARGA_DCB_MEDIC_CONTROL',
		'A procedure GERA_W_CARGA_DCB_MEDIC_CONTROL limpou a tabela W_CARGA_DCB_MEDIC_CONTROL.',
		nm_usuario_p);
end if;

insert into w_carga_dcb_medic_control(
	cd_dcb,
	ds_dcb,
	nr_cas,
	dt_atualizacao,
	nm_usuario)
values(	substr(cd_dcb_p,1,20),
	substr(ds_dcb_p,1,255),
	substr(nr_cas_p,1,20),
	sysdate,
	nm_usuario_p);

commit;

end gera_w_carga_dcb_medic_control;
/