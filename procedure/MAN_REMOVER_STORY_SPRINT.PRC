create or replace
procedure man_remover_story_sprint(
			nr_sprint_p		number,
			nr_story_p		number,
			nm_usuario_p		varchar2) is 

cd_status_w	desenv_story_sprint.cd_status%type;
			
begin

select	max(cd_status)
into	cd_status_w
from	desenv_story_sprint
where	nr_story = nr_story_p
and	nr_sprint = nr_sprint_p
and	nvl(ie_cancelado_sprint,'N') <> 'S';

if	(cd_status_w = 4) then
	wheb_mensagem_pck.exibir_mensagem_abort(351399);
end if;

update	desenv_story_sprint
set	ie_cancelado_sprint = decode(nvl(ie_cancelado_sprint,'N'),'N','S','N'),
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_sprint = nr_sprint_p
and	nr_story = nr_story_p;

commit;

end man_remover_story_sprint;
/