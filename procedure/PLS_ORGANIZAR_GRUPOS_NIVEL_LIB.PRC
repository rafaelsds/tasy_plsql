create or replace
procedure pls_organizar_grupos_nivel_lib
			(	nr_seq_auditoria_p	number,
				nm_usuario_p		Varchar2) is 
				
nr_seq_grupo_w			number(10);
nr_maior_nivel_w		number(10);
nr_nivel_grupo_w		number(10);
nr_seq_grupo_auditoria_w	number(10);
qt_grupos_externo_w		number(5);
				
Cursor C01 is
	select	nr_sequencia,
		nr_seq_grupo
	from	pls_auditoria_grupo
	where	nr_seq_auditoria = nr_seq_auditoria_p
	order by nr_sequencia;

begin

select	count(*)
into	qt_grupos_externo_w
from	pls_auditoria_grupo a,
	pls_grupo_auditor b
where	a.nr_seq_grupo 		= b.nr_sequencia
and	b.ie_tipo_auditoria 	= 2
and	a.nr_seq_auditoria 	= nr_seq_auditoria_p;

if	(qt_grupos_externo_w > 0) then
	begin
		select	max(pls_obter_nivel_regra_grup_aud(nr_seq_grupo))
		into	nr_maior_nivel_w
		from	pls_auditoria_grupo
		where	nr_seq_auditoria = nr_seq_auditoria_p;
	exception
	when others then
		nr_maior_nivel_w := 0;
	end;

	if	(nvl(nr_maior_nivel_w,0) > 0) then 
		open C01;
		loop
		fetch C01 into	
			nr_seq_grupo_auditoria_w,
			nr_seq_grupo_w;
		exit when C01%notfound;
			begin		
			
			select	pls_obter_nivel_regra_grup_aud(nr_seq_grupo_w)
			into	nr_nivel_grupo_w
			from	dual;
			
			if	(nvl(nr_nivel_grupo_w,0) > 0) then
				if	(nr_nivel_grupo_w < nr_maior_nivel_w) then
					delete from pls_auditoria_grupo
					where	nr_sequencia = nr_seq_grupo_auditoria_w;
				end if;
			end if;
			end;
		end loop;
		close C01;
	end if;
end if;

commit;

end pls_organizar_grupos_nivel_lib;
/