create or replace procedure eclipse_insurance_receipt (	ie_type_message_p	varchar2,
					nr_seq_transaction_p	varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is

nr_seq_dvr_w			dvr_response.nr_sequencia%type;
vl_paid_w			dvr_response.vl_paid%type;
cd_claim_w			dvr_response.cd_claim%type;
vl_charge_w			dvr_response.vl_charge%type;
nr_seq_request_w		dvr_response.nr_seq_request%type;
nr_seq_transaction_w		dvr_response.nr_seq_transaction%type;
nr_patient_account_w		dva_claim.nr_interno_conta%type;

nr_seq_return_w 		convenio_retorno.nr_sequencia%type;

nr_seq_ret_item_w		convenio_retorno_item.nr_sequencia%type;
vl_denied_w			convenio_retorno_item.vl_glosado%type;
vl_additional_w			convenio_retorno_item.vl_adicional%type;
vl_deposit_w			convenio_receb.vl_deposito%type;

cd_convenio_parametro_w		conta_paciente.cd_convenio_parametro%type;

vl_form_w			conta_paciente_guia.vl_guia%type;
cd_authorization_w		conta_paciente_guia.cd_autorizacao%type;

nr_seq_item_denied_w		convenio_retorno_glosa.nr_sequencia%type;
ds_notes_w			convenio_retorno_glosa.ds_observacao%type;

ds_code_w			eclipse_status_codes.ds_code%type;
nr_seq_conta_banco_w		convenio_receb.nr_seq_conta_banco%type;
nr_seq_trans_fin_conv_receb_w	parametro_contas_receber.nr_seq_trans_fin_conv_receb%type;
cd_trans_w 			dpy_request.nr_seq_transaction%type;
count_w				pls_integer;
-- Expression constants.
form_not_informed_w	varchar2(200) := wheb_mensagem_pck.get_texto(1097738);

cursor cDVYResponse (nr_seq_transaction_p varchar) is
	select	a.nr_sequencia,
		a.dt_atualizacao,
		a.nm_usuario,
		a.dt_atualizacao_nrec,
		a.nm_usuario_nrec,
		a.nm_bank_account,
		a.cd_bank_account,
		a.cd_bsb,
		a.vl_deposit,
		a.dt_payment,
		a.nr_payment,
		a.nr_seq_request
	from	dvy_response a
	where a.nr_seq_transaction = nr_seq_transaction_p;
	
cursor cDPYResponse (nr_seq_transaction_p varchar) is
	select	a.nr_sequencia,
		a.dt_atualizacao,
		a.nm_usuario,
		a.dt_atualizacao_nrec,
		a.nm_usuario_nrec,
		a.nm_bank_account,
		a.cd_bank_account,
		a.cd_bsb,
		a.vl_deposit,
		a.dt_payment,
		a.nr_payment,
		a.nr_seq_request
	from	dpy_response a
	where a.nr_seq_request = nr_seq_transaction_p;

cursor	cERAResponse(cd_seq_transaction_p varchar) is
	select	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_bank_account,
			cd_bank_account,
			cd_bsb,
			vl_deposit,
			dt_payment
	from	era_report_response
	where 	cd_transaction = cd_seq_transaction_p;

/*
ie_type_message_p
IHC -> IHC-ERA
DVA -> DVY
DBS -> DBY
IMC -> IMC-ERA
OVS -> OVS-ERA
*/

begin

if	(ie_type_message_p = 'DVY') then

select	nr_interno_conta
into 	nr_patient_account_w
from 	dva_claim
where 	nr_seq_transaction = nr_seq_transaction_p;

select	max(cd_convenio_parametro)
into	cd_convenio_parametro_w
from	conta_paciente
where	nr_interno_conta = nr_patient_account_w;

	for rDVYResponse in cDVYResponse (nr_seq_transaction_p) loop

		--NR_SEQ_CONTA_BANCO -->
		select	max(nr_sequencia)
			--ds_conta ds
		into	nr_seq_conta_banco_w

		from	banco_estabelecimento_v a
		where	(a.cd_estabelecimento is null or a.cd_estabelecimento = cd_estabelecimento_p or obter_estab_financeiro(a.cd_estabelecimento) = cd_estabelecimento_p)
		AND 	a.cd_agencia_bancaria = to_char(rDVYResponse.cd_bsb)
		--AND	CD_BANCO = NM_BANK ???
		AND	a.cd_conta = rDVYResponse.cd_bank_account;

		--NR_SEQ_TRANS_FIN -->
		select	max(nr_seq_trans_fin_conv_receb)
		into    nr_seq_trans_fin_conv_receb_w
		from    parametro_contas_receber
		where 	cd_estabelecimento = cd_estabelecimento_p;


		/*select	count(*)
		into	qt_exist_receivable_w
		from	convenio_receb
		where	cd_convenio = cd_convenio_parametro_w
		and	nr_sistema_externo = r.nr_sequencia
		and	vl_deposito = r.vl_deposit*/
		insert into  convenio_receb (	nr_sequencia,
				cd_convenio,
				dt_recebimento,
				vl_recebimento,
				dt_atualizacao,
				nm_usuario,
				ie_status,
				nr_seq_conta_banco,
				nr_seq_trans_fin,
				nr_lote_contabil,
				ds_observacao,
				vl_despesa_bancaria,
				vl_deposito,
				dt_fluxo_caixa,
				cd_estabelecimento,
				dt_liberacao,
				ie_integrar_cb_fluxo,
				ie_tipo_glosa,
				nr_adiantamento,
				nr_seq_cobranca,
				ie_deduzir_adic,
				vl_moeda_original,
				cd_moeda,
				tx_cambial,
				nr_sistema_externo,
				nr_sistema_externo_aux
			)
		values	(	convenio_receb_seq.nextval,
				cd_convenio_parametro_w,
				sysdate,
				to_number(rDVYResponse.vl_deposit),  -- saving vl_depoit  value of dvy_response table into vl_recebimento field
				sysdate,
				nm_usuario_p,
				'N',	-- ie_Status (Mandatory field so hard coding to N) ,
				to_number(nr_seq_conta_banco_w),
				nr_seq_trans_fin_conv_receb_w,
				null,
				wheb_mensagem_pck.get_texto(1108659) || ':' || rDVYResponse.nm_bank_account || ' ' ||  wheb_mensagem_pck.get_texto(1109142) || ':' || nr_seq_transaction_p , --  ds_observacao
				null,
				to_number(rDVYResponse.vl_deposit),
				null,
				cd_estabelecimento_p,
				null,
				'S',	-- Mandatory field , so hard coding to 'S'
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				rDVYResponse.nr_sequencia,
				null
			);

end loop;

elsif	(ie_type_message_p = 'DPY') then

select nr_seq_transaction
into cd_trans_w
from dpy_request
where NR_SEQUENCIA = nr_seq_transaction_p;


select	nr_seq_account
into 	nr_patient_account_w
from 	dbs_claim
where 	nr_seq_transaction = cd_trans_w;

select	max(cd_convenio_parametro)
into	cd_convenio_parametro_w
from	conta_paciente
where	nr_interno_conta = nr_patient_account_w;
	for rDPYResponse in cDPYResponse (nr_seq_transaction_p) loop
	--NR_SEQ_CONTA_BANCO -->
		select	max(nr_sequencia)
			
		into	nr_seq_conta_banco_w

		from	banco_estabelecimento_v a
		where	(a.cd_estabelecimento is null or a.cd_estabelecimento = cd_estabelecimento_p or obter_estab_financeiro(a.cd_estabelecimento) = cd_estabelecimento_p)
		AND 	a.cd_agencia_bancaria = to_char(rDPYResponse.cd_bsb)
		
		AND	a.cd_conta = rDPYResponse.cd_bank_account;

		--NR_SEQ_TRANS_FIN -->
		select	max(nr_seq_trans_fin_conv_receb)
		into    nr_seq_trans_fin_conv_receb_w
		from    parametro_contas_receber
		where 	cd_estabelecimento = cd_estabelecimento_p;

		insert into  convenio_receb (	nr_sequencia,
				cd_convenio,
				dt_recebimento,
				vl_recebimento,
				dt_atualizacao,
				nm_usuario,
				ie_status,
				nr_seq_conta_banco,
				nr_seq_trans_fin,
				nr_lote_contabil,
				ds_observacao,
				vl_despesa_bancaria,
				vl_deposito,
				dt_fluxo_caixa,
				cd_estabelecimento,
				dt_liberacao,
				ie_integrar_cb_fluxo,
				ie_tipo_glosa,
				nr_adiantamento,
				nr_seq_cobranca,
				ie_deduzir_adic,
				vl_moeda_original,
				cd_moeda,
				tx_cambial,
				nr_sistema_externo,
				nr_sistema_externo_aux
			)
		values	(	convenio_receb_seq.nextval,
				cd_convenio_parametro_w,
				sysdate,
				to_number(rDPYResponse.vl_deposit),  -- saving vl_depoit  value of dvy_response table into vl_recebimento field
				sysdate,
				nm_usuario_p,
				'N',	-- ie_Status (Mandatory field so hard coding to N) ,
				to_number(nr_seq_conta_banco_w),
				nr_seq_trans_fin_conv_receb_w,
				null,
				wheb_mensagem_pck.get_texto(1108659) || ':' || rDPYResponse.nm_bank_account || ' ' ||  wheb_mensagem_pck.get_texto(1109142) || ':' || nr_seq_transaction_p , --  ds_observacao
				null,
				to_number(rDPYResponse.vl_deposit),
				null,
				cd_estabelecimento_p,
				null,
				'S',	-- Mandatory field , so hard coding to 'S'
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				rDPYResponse.nr_sequencia,
				null
			);
	
	
	end loop;

elsif	(ie_type_message_p = 'IHC-ERA' or ie_type_message_p = 'IMC-ERA') then

	for rERAResponse in cERAResponse (nr_seq_transaction_p) loop
		select 	nvl(count(nr_sequencia),0)
		into 	count_w
		from 	convenio_receb
		where nr_sistema_externo = rERAResponse.nr_sequencia;

		if (count_w = 0) then
			select	nvl(max(nr_account),0)
			into 	nr_patient_account_w
			from 	ihc_claim
			where 	cd_transaction = (	select 	max(cd_transaction)
										from 	era_response_details
										where 	nr_seq_response = rERAResponse.nr_sequencia);

			if (nr_patient_account_w = 0) then
				select	nr_seq_account
				into 	nr_patient_account_w
				from 	imc_claim
				where 	nr_seq_transaction = (	select 	max(cd_transaction)
												from 	era_response_details
												where 	nr_seq_response = rERAResponse.nr_sequencia);
			end if;

			select	max(cd_convenio_parametro)
			into	cd_convenio_parametro_w
			from	conta_paciente
			where	nr_interno_conta = nr_patient_account_w;

			select	max(nr_sequencia)
				--ds_conta ds
			into	nr_seq_conta_banco_w
			from	banco_estabelecimento_v a
			where	(a.cd_estabelecimento is null or a.cd_estabelecimento = cd_estabelecimento_p or obter_estab_financeiro(a.cd_estabelecimento) = cd_estabelecimento_p)
			AND 	a.cd_agencia_bancaria = to_char(rERAResponse.cd_bsb)
			--AND	CD_BANCO = NM_BANK ???
			AND	a.cd_conta = to_char(rERAResponse.cd_bank_account);

			select	max(nr_seq_trans_fin_conv_receb)
			into	nr_seq_trans_fin_conv_receb_w
			from	parametro_contas_receber
			where	cd_estabelecimento = cd_estabelecimento_p;

			vl_deposit_w := rERAResponse.vl_deposit / 100;

			ds_notes_w := obter_expressao_idioma(1108659,5) || ':' || rERAResponse.nm_bank_account || ' ' ||  obter_expressao_idioma(1109142,5) || ':' || nr_seq_transaction_p;

			/*select	count(*)
			into	qt_exist_receivable_w
			from	convenio_receb
			where	cd_convenio = cd_convenio_parametro_w
			and	nr_sistema_externo = r.nr_sequencia
			and	vl_deposito = r.vl_deposit*/

			insert into  convenio_receb (	nr_sequencia,
					cd_convenio,
					dt_recebimento,
					vl_recebimento,
					dt_atualizacao,
					nm_usuario,
					ie_status,
					nr_seq_conta_banco,
					nr_seq_trans_fin,
					nr_lote_contabil,
					ds_observacao,
					vl_despesa_bancaria,
					vl_deposito,
					dt_fluxo_caixa,
					cd_estabelecimento,
					dt_liberacao,
					ie_integrar_cb_fluxo,
					ie_tipo_glosa,
					nr_adiantamento,
					nr_seq_cobranca,
					ie_deduzir_adic,
					vl_moeda_original,
					cd_moeda,
					tx_cambial,
					nr_sistema_externo,
					nr_sistema_externo_aux
				)
			values	(	convenio_receb_seq.nextval,
					cd_convenio_parametro_w,
					sysdate,
					vl_deposit_w,  -- saving vl_depoit  value of dvy_response table into vl_recebimento field
					sysdate,
					nm_usuario_p,
					'N',	-- ie_Status (Mandatory field so hard coding to N) ,
					nr_seq_conta_banco_w,
					nr_seq_trans_fin_conv_receb_w,
					null,
					ds_notes_w,
					null,
					vl_deposit_w,
					null,
					cd_estabelecimento_p,
					null,
					'S',	-- Mandatory field , so hard coding to 'S'
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					rERAResponse.nr_sequencia,
					null
				);
		end if;
	end loop;
end if;
commit;
end eclipse_insurance_receipt;
/
