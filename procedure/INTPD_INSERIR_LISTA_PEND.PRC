create or replace
procedure intpd_inserir_lista_pend(
			nr_sequencia_p number,
			nr_seq_superior_p number,
			nm_usuario_p varchar2) is 

nr_ordenacao_w intpd_lista_pend_superior.nr_ordenacao%type;
			
begin

select	nvl(max(nr_ordenacao),'0') + 1
into	nr_ordenacao_w
from	intpd_lista_pend_superior
where	nr_seq_fila_superior = nr_seq_superior_p;

insert into intpd_lista_pend_superior (nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_fila,
			nr_seq_fila_superior,
			nr_ordenacao)
values (intpd_lista_pend_superior_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_sequencia_p,
			nr_seq_superior_p,
			nr_ordenacao_w);

end intpd_inserir_lista_pend;
/