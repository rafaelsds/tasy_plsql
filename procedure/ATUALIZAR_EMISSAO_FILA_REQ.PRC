create or replace
procedure atualizar_emissao_fila_req(nr_sequencia_p		number,
					nm_usuario_p		varchar2) is 

begin

update	requisicao_impressao_pda
set	dt_impressao = sysdate,
	ie_pendente = 'N'
where	nr_sequencia = nr_sequencia_p;

commit;

end atualizar_emissao_fila_req;
/