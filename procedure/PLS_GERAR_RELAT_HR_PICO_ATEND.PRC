create or replace
procedure pls_gerar_relat_hr_pico_atend
			(	dt_inicio_pesquisa_p	Date,
				dt_fim_pesquisa_p	Date,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p    number) is 

qt_horas_w			Number(2)	:= 0;
qt_horas_ww			Varchar2(3);
qt_atendimento_w		Number(10);
ie_tipo_atend_w			Varchar2(2)	:= 'R';
ie_gestao_call_center_w		varchar2(1);

begin

delete	from w_call_center_relatorio 
where	nm_usuario = nm_usuario_p;


select	nvl(max(ie_gestao_call_center),'N')
into	ie_gestao_call_center_w
from	pls_controle_estab;

while	(qt_horas_w	<= 23) loop
	begin
		
	if	(length(qt_horas_w)	= 1) then
		qt_horas_ww	:= adiciona_zeros_esquerda(qt_horas_w,1);
	else
		qt_horas_ww	:= to_char(qt_horas_w);
	end if;
	
	if(ie_gestao_call_center_w = 'S') then
		select	count(1)
		into	qt_atendimento_w
		from	pls_atendimento
		where	ie_tipo_atendimento		= ie_tipo_atend_w
		and	trunc(dt_inicio)		between (dt_inicio_pesquisa_p) and (dt_fim_pesquisa_p)
		and	to_char(dt_inicio,'hh24')	= qt_horas_ww
		and	cd_estabelecimento = cd_estabelecimento_p;	
	else
		select	count(1)
		into	qt_atendimento_w
		from	pls_atendimento
		where	ie_tipo_atendimento		= ie_tipo_atend_w
		and	trunc(dt_inicio)		between (dt_inicio_pesquisa_p) and (dt_fim_pesquisa_p)
		and	to_char(dt_inicio,'hh24')	= qt_horas_ww;	
	end if;
	
	
	insert	into w_call_center_relatorio
		(nr_sequencia, qt_ocorrencia, hr_ocorrencia,
		 ie_tipo_atendimento, nm_usuario, dt_atualizacao)
	values	(w_call_center_relatorio_seq.NextVal, qt_atendimento_w, qt_horas_w,
		 ie_tipo_atend_w, nm_usuario_p, sysdate);
	
	qt_horas_w	:= qt_horas_w + 1;
	
	if	(qt_horas_w	= 24) and (ie_tipo_atend_w	= 'R') then
		ie_tipo_atend_w	:= 'A';
		qt_horas_w	:= 0;
	end if;
	end;
end loop;
	
commit;

end pls_gerar_relat_hr_pico_atend;
/
