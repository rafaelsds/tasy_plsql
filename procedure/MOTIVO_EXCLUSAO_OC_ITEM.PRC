create or replace
procedure motivo_exclusao_oc_item(
			nr_ordem_compra_p	number,
			nr_item_oci_p		number) is 

begin

delete 	from ordem_compra_item 
where 	nr_ordem_compra		= nr_ordem_compra_p 
and 	nr_item_oci		= nr_item_oci_p;

commit;

end motivo_exclusao_oc_item;
/
	
	