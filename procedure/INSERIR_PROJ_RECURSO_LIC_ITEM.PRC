create or replace
procedure inserir_proj_recurso_lic_item(	nr_seq_licitacao_p				number,
				nr_seq_proj_rec_p				number,
				nm_usuario_p				Varchar2) is 

nr_seq_lic_item_w			reg_lic_item.nr_seq_lic_item%type;
ds_lista_itens_w			varchar2(2000);
ds_projeto_w				projeto_recurso.ds_projeto%type;

cursor c01 is
select	nr_seq_lic_item
from	reg_lic_item
where	nr_seq_licitacao = nr_seq_licitacao_p
order by	nr_seq_lic_item;
					
begin

update	reg_lic_item
set	nr_seq_proj_rec	= nr_seq_proj_rec_p
where	nr_seq_licitacao	= nr_seq_licitacao_p;

open C01;
loop
fetch C01 into	
	nr_seq_lic_item_w;
exit when C01%notfound;
	begin
	if	(ds_lista_itens_w is null) then
		ds_lista_itens_w	:= nr_seq_lic_item_w;
	else
		ds_lista_itens_w	:= substr(ds_lista_itens_w || ',' || nr_seq_lic_item_w,1,2000);
	end if;
	end;
end loop;
close C01;

select	ds_projeto
into	ds_projeto_w
from	projeto_recurso
where	nr_sequencia = nr_seq_proj_rec_p;


insert into reg_lic_historico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_historico,
	ds_observacao,
	nr_seq_licitacao)
values(	reg_lic_historico_seq.nextval,
	sysdate,
	nm_usuario_p,
	'ALR',
	wheb_mensagem_pck.get_texto(796859, 'DS_LISTA_ITENS_W=' || ds_lista_itens_w || ';DS_PROJETO_W=' || ds_projeto_w), --Alterado o projeto recurso dos itens (#@DS_LISTA_ITENS_W#@) para #@DS_PROJETO_W#@.
	nr_seq_licitacao_p);


commit;

end inserir_proj_recurso_lic_item;
/