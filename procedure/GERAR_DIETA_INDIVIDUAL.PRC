create or replace
procedure gerar_dieta_individual
			(	nr_atendimento_p	number,
				cd_refeicao_p		varchar2,
				dt_dieta_p		date,
				nm_usuario_p		varchar2,
				ie_destino_dieta_p	varchar2,
				qt_horas_p		number,
				ds_erro_p		out varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
	Gerar o dieta para paciente/acompanhantes
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	1000 - [85] - Permite gerar dieta individual para mais de um acompanhante
	1000 - [90] - Permite gerar a dieta individual para o acompanhante, sem a libera��o na Entrada �nica
	1000 - [137] - Obter dieta da prescri��o automaticamente ao gerar dieta individual
	1000 - [138] - Dieta padr�o do paciente individual
	1000 - [5] - Dieta padr�o para o acompanhante
	
	IE_LIB_DIETA_W - Dom�nio 1396
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_erro_w			varchar2(255) := null;
cd_setor_atendimento_w		number(05,0);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_sequencia_w			number(10,0);
nr_atendimento_acomp_w		number(10,0);
nr_atendimento_w		number(10,0);
ie_gera_acomp_sem_acomod_w	varchar2(1);
qt_itens_w			number(10,0);
ds_refeicao_w			varchar2(255);
ie_gera_dieta_w			varchar2(1);
ie_lib_dieta_w			varchar2(1);
qt_dieta_acomp_w		number(3);
qt_dieta_w			number(3);
nr_seq_atecaco_w		number(10);
ie_parametro_w			varchar2(1);
ie_gera_dieta_sem_lib_eup_w	varchar2(2);	
cd_dieta_padrao_acomp_w		number(10);
ie_regra_ref_lib_w		varchar2(1);
cd_setor_atend_pac_w		setor_atendimento.cd_setor_atendimento%type;
nr_seq_mapa_w			mapa_dieta.nr_sequencia%type;
ie_obter_dieta_pac_w		varchar2(1);
cd_dieta_padrao_pac_w		number(10);

cursor c010 is
	/**/
	select	a.cd_setor_atendimento,
		a.cd_unidade_basica,
		a.cd_unidade_compl,
		a.cd_paciente_reserva,
		nr_atendimento,
		nr_atendimento_acomp
	from  	unidade_atendimento	a
	where 	a.ie_status_unidade	= 'M'
	and	a.nr_atendimento	= nr_atendimento_p
	and	a.cd_paciente_reserva is not null
	and	not exists	(select	cd_pessoa_fisica
				from	mapa_dieta c
				where	a.cd_setor_atendimento	= c.cd_setor_atendimento
				and	a.cd_unidade_basica	= c.cd_unidade_basica
				and	a.cd_unidade_compl	= c.cd_unidade_compl
				and	c.dt_dieta		= dt_dieta_p
				and	c.cd_refeicao		= cd_refeicao_p)
	and	ie_destino_dieta_p = 'A'
	union
	/*Quando h� acompanhante vinculado ao atendimento*/
	select	a.cd_setor_atendimento,
		a.cd_unidade_basica,
		a.cd_unidade_compl,
		c.cd_pessoa_fisica,
		b.nr_atendimento,
		null
	from  	atendimento_paciente		b,
		unidade_atendimento		a,
		atendimento_acompanhante	c
	where 	a.nr_atendimento	= b.nr_atendimento
	and 	b.nr_atendimento	= c.nr_atendimento
	and	a.ie_status_unidade	= 'P'
	and	b.nr_atendimento	= nr_atendimento_p
	and	c.cd_pessoa_fisica is not null
	and	c.dt_saida is null
	and	not exists	(select 1
				from	mapa_dieta	d
				where	a.cd_setor_atendimento	= d.cd_setor_atendimento
				and	a.cd_unidade_basica	= d.cd_unidade_basica
				and	a.cd_unidade_compl	= d.cd_unidade_compl
				and	d.dt_dieta		= dt_dieta_p
				and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica
				and	d.cd_refeicao		= cd_refeicao_p)
	and	ie_gera_acomp_sem_acomod_w = 'S'
	and	ie_destino_dieta_p = 'A';

begin
ds_refeicao_w			:= substr(obter_valor_dominio(99,cd_refeicao_p),1,255);
ie_parametro_w			:= Obter_Valor_Param_Usuario(1000, 85, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
ie_gera_dieta_sem_lib_eup_w	:= nvl(Obter_Valor_Param_Usuario(1000, 90, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),'S');
cd_dieta_padrao_acomp_w		:= Obter_Valor_Param_Usuario(1000, 5, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
ie_obter_dieta_pac_w		:= Obter_Valor_Param_Usuario(1000, 137, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
cd_dieta_padrao_pac_w		:= Obter_Valor_Param_Usuario(1000, 138, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);

/*Verifica se j� h� dieta gerada para o atendimento*/
if	(ie_parametro_w = 'S') then
	begin
	select	max(WHEB_MENSAGEM_PCK.get_texto(280054,'DS_REFEICAO='||ds_refeicao_w))
	into	ds_erro_w
	from	mapa_dieta
	where	nr_atendimento		= nr_atendimento_p
	and	dt_dieta		= dt_dieta_p
	and	cd_refeicao		= cd_refeicao_p
	and	ie_destino_dieta	= ie_destino_dieta_p
	and	ie_destino_dieta_p	= 'P';
	exception
		when others then
		ds_erro_w	:= null;
	end;
else
	begin
	select	max(WHEB_MENSAGEM_PCK.get_texto(280054,'DS_REFEICAO='||ds_refeicao_w))
	into	ds_erro_w
	from	mapa_dieta
	where	nr_atendimento		= nr_atendimento_p
	and	dt_dieta		= dt_dieta_p
	and	cd_refeicao		= cd_refeicao_p
	and	ie_destino_dieta	= ie_destino_dieta_p;
	exception
		when others then
		ds_erro_w	:= null;
	end;
end if;

ds_erro_p	:= ds_erro_w;

ie_gera_dieta_w		:= 'N';
nr_seq_atecaco_w	:= obter_atecaco_atendimento(nr_atendimento_p);

select	max(ie_lib_dieta),
	max(qt_dieta_acomp)
into	ie_lib_dieta_w,
	qt_dieta_acomp_w
from	atend_categoria_convenio
where	nr_atendimento	= nr_atendimento_p
and	nr_seq_interno = nr_seq_atecaco_w;

begin			
select	count(*)
into	qt_dieta_w
from	mapa_dieta
where	nr_atendimento		= nr_atendimento_p
and	ie_destino_dieta	= 'A'
and	((cd_refeicao		= cd_refeicao_p) or (ie_lib_dieta_w = 'S'))		
and	trunc(dt_dieta,'dd') 	= trunc(dt_dieta_p,'dd');
exception
	when no_data_found then
		qt_dieta_w 	:= 0;
end;

/*Verifica se possui libera��o para a dieta selecionada conforme regra de libera��o da categoria do conv�nio*/
if	((ie_lib_dieta_w = 'T')) or
	((ie_lib_dieta_w = 'C') and (cd_refeicao_p = 'D')) or
	((ie_lib_dieta_w = 'B') and (cd_refeicao_p = 'A')) or
	((ie_lib_dieta_w = 'E') and (cd_refeicao_p = 'J')) or
	((ie_lib_dieta_w = 'G') and (cd_refeicao_p = 'D') and (cd_refeicao_p = 'J')) or
	((ie_lib_dieta_w = 'L') and ((cd_refeicao_p = 'D') or (cd_refeicao_p = 'A') or (cd_refeicao_p = 'J'))) or
	((ie_lib_dieta_w = 'D') and ((cd_refeicao_p = 'D') or (cd_refeicao_p = 'L') or (cd_refeicao_p = 'C'))) or
	((ie_lib_dieta_w = 'A') and ((cd_refeicao_p = 'A') or (cd_refeicao_p = 'J'))) or
	((ie_lib_dieta_w = 'F') and ((cd_refeicao_p = 'D') or (cd_refeicao_p = 'A'))) or
	((ie_lib_dieta_w = 'S') and (qt_dieta_w < qt_dieta_acomp_w)) or
	((ie_gera_dieta_sem_lib_eup_w = 'S') or (ie_gera_dieta_sem_lib_eup_w = 'DI')) then
	ie_gera_dieta_w	:= 'S';
end if;

/*Regra de libera��o da refei��o para acompanhante(Shift+F11)*/
ie_regra_ref_lib_w := Obter_se_refeicao_lib_acomp(nr_atendimento_p);

if	(ie_regra_ref_lib_w = 'N') then
	ie_gera_dieta_w	:= 'N';
end if;	

select	nvl(obter_valor_param_usuario(1000, 36, obter_perfil_ativo, nm_usuario_p,1),'S')
into	ie_gera_acomp_sem_acomod_w
from	dual;

if	(ds_erro_w is null) then
	/*Gera dieta para o paciente*/
	if	(ie_destino_dieta_p = 'P') then
		cd_setor_atend_pac_w	:= obter_setor_atendimento(nr_atendimento_p);
		cd_pessoa_fisica_w	:= obter_pessoa_atendimento(nr_atendimento_p,'C');
		
		insert into mapa_dieta
			(	nr_sequencia,
				cd_pessoa_fisica,
				dt_dieta,
				cd_refeicao,
				ie_destino_dieta,
				ie_status,
				dt_atualizacao,
				nm_usuario,
				cd_setor_atendimento,
				cd_unidade_basica,
				cd_unidade_compl,
				cd_dieta,
				ds_observacao,
				nr_atendimento,
				qt_parametro,
				ds_observacao_tec,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				ie_forma_geracao)
		values	(	mapa_dieta_seq.nextval,
				cd_pessoa_fisica_w,
				dt_dieta_p,
				cd_refeicao_p,
				'P',
				'A',
				sysdate,
				nm_usuario_p,
				cd_setor_atend_pac_w,
				obter_unidade_atendimento(nr_atendimento_p,'A','UB'),
				obter_unidade_atendimento(nr_atendimento_p,'A','UC'),
				cd_dieta_padrao_pac_w,
				null,
				nr_atendimento_p,
				null,
				null,
				nm_usuario_p,
				sysdate,
				'M')
		returning nr_sequencia into nr_seq_mapa_w;

		/*Buscar e gravar a dieta vigente da prescir��o do pasciente no mapa de dietas gerado. */
		if	(ie_obter_dieta_pac_w = 'S') then
			obter_dieta_prescricao_pac(	cd_setor_atend_pac_w,
							qt_horas_p,
							dt_dieta_p,
							cd_refeicao_p,
							nm_usuario_p,
							cd_pessoa_fisica_w,
							nr_seq_mapa_w);
		end if;
		
	end if;

	open c010;

	qt_itens_w	:= 0;
	
	loop
	fetch c010 into
		cd_setor_atendimento_w,
		cd_unidade_basica_w,
		cd_unidade_compl_w,
		cd_pessoa_fisica_w,
		nr_atendimento_w,
		nr_atendimento_acomp_w;
	exit when c010%notfound;
	
		qt_itens_w	:= 1;

		if	(ie_gera_dieta_w = 'S') then
			select mapa_dieta_seq.nextval
			into nr_sequencia_w
			from dual;

			insert into mapa_dieta
				(	cd_pessoa_fisica,
					nr_sequencia,
					dt_dieta,
					cd_refeicao,
					dt_atualizacao,
					nm_usuario,
					cd_setor_atendimento,
					cd_unidade_basica,
					cd_unidade_compl,
					ie_destino_dieta,
					cd_dieta,
					nr_atendimento,
					ie_status,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					ie_forma_geracao)
			values	(	cd_pessoa_fisica_w,
					nr_sequencia_w,
					dt_dieta_p,
					cd_refeicao_p,
					sysdate,
					nm_usuario_p,
					cd_setor_atendimento_w,
					cd_unidade_basica_w,
					cd_unidade_compl_w,
					'A',
					cd_dieta_padrao_acomp_w, --null,
					nr_atendimento_p,
					'A',
					nm_usuario_p,
					sysdate,
					'M');
		end if;

	end loop;
	close c010;
	
	/*Se n�o houver informa��es do acompanhante gera a dieta do acompanhante conforme atendimento do paciente*/
	if	(qt_itens_w = 0) then
		select	max(a.cd_setor_atendimento),
			max(a.cd_unidade_basica),
			max(a.cd_unidade_compl),
			max(b.cd_pessoa_fisica),
			max(b.nr_atendimento)
		into	cd_setor_atendimento_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w,
			cd_pessoa_fisica_w,
			nr_atendimento_w
		from  	atendimento_paciente b,
			atend_paciente_unidade a
		where 	a.nr_seq_interno	= obter_atepacu_paciente(b.nr_atendimento, 'A')
		  and	b.nr_atendimento	= nr_atendimento_p;		
		  
		if	((nr_atendimento_w > 0) and
			(ie_gera_dieta_w = 'S')) then
			insert into mapa_dieta
				(	cd_pessoa_fisica,
					nr_sequencia,
					dt_dieta,
					cd_refeicao,
					dt_atualizacao,
					nm_usuario,
					cd_setor_atendimento,
					cd_unidade_basica,
					cd_unidade_compl,
					ie_destino_dieta,
					cd_dieta,
					nr_atendimento,
					ie_status,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					ie_forma_geracao)
			values	(	cd_pessoa_fisica_w,
					mapa_dieta_seq.nextval,
					dt_dieta_p,
					cd_refeicao_p,
					sysdate,
					nm_usuario_p,
					cd_setor_atendimento_w,
					cd_unidade_basica_w,
					cd_unidade_compl_w,
					'A',
					cd_dieta_padrao_acomp_w,
					nr_atendimento_p,
					'A',
					nm_usuario_p,
					sysdate,
					'M');	
		end if;
	end if;
	
	commit;
end if;

end gerar_dieta_individual;
/