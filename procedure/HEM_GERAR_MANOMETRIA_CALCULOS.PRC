create or replace
procedure hem_gerar_manometria_calculos(	nr_seq_hem_calc_p	Number,
						ie_avaliacao_p		Varchar2,
						nm_usuario_p		Varchar2) is 

lista_camaras_w		dbms_sql.varchar2_table;
ds_camaras_w		varchar2(255);
						
begin
if	(nvl(nr_seq_hem_calc_p, 0) > 0) then
	
	if	(ie_avaliacao_p = 'SI') then
			ds_camaras_w := 'AE,AD,VD,TP,CP,VE,AO,VC';
			
	elsif	(ie_avaliacao_p = 'F') then
			ds_camaras_w := 'AD,VD,TP,CP,VE,AO';	
			
	elsif	(ie_avaliacao_p = 'AV') then
			ds_camaras_w := 'AE,AD,VD,TP,CP,VE,AO';			
	end if;
	
	lista_camaras_w := obter_lista_string(ds_camaras_w, ',');
	
	for	i in lista_camaras_w.first..lista_camaras_w.last loop
		begin
		
		insert into hem_manometria_completa (
			nr_sequencia, 
			dt_atualizacao, 
			nm_usuario, 
			dt_atualizacao_nrec, 
			nm_usuario_nrec,
			ie_tipo,
			nr_seq_hem_calc 
		) values (
			hem_manometria_completa_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			lista_camaras_w(i),
			nr_seq_hem_calc_p);
		end;
	end loop;
	
end if;
commit;

end hem_gerar_manometria_calculos;
/
