create or replace
procedure pls_consistir_regra_atend_cart
			(	nr_seq_segurado_p		in	number,
				nr_seq_regra_atend_cart_p	in	number,
				ie_glosou_p			out	varchar2) is 

cd_usuario_plano_w		varchar2(30);
cd_faixa_carteira_w		varchar2(30);
ie_glosou_w			varchar2(1)	:= 'N';
nr_seq_oc_atend_cart_regra_w	number(10);
qt_pos_inicio_w			number(10);
qt_pos_final_w			number(10);

cursor C01 is
	select	a.nr_sequencia,
		a.qt_pos_inicio,
		a.qt_pos_final
	from	pls_oc_atend_cart_regra	a
	where	a.nr_seq_regra	= nr_seq_regra_atend_cart_p;
	
cursor C02 is
	select	a.cd_carteira
	from	pls_oc_atend_cart_cod	a
	where	a.nr_seq_regra_cart	= nr_seq_oc_atend_cart_regra_w
	and	a.cd_carteira 		= cd_faixa_carteira_w;

begin

if	(nr_seq_segurado_p is not null) then
	select	pls_obter_dados_segurado(a.nr_sequencia,'C')
	into	cd_usuario_plano_w
	from	pls_segurado	a
	where	a.nr_sequencia	= nr_seq_segurado_p;

	open C01;
	loop
	fetch C01 into	
		nr_seq_oc_atend_cart_regra_w,
		qt_pos_inicio_w,
		qt_pos_final_w;
	exit when C01%notfound;
		begin

		cd_faixa_carteira_w	:= substr(cd_usuario_plano_w,qt_pos_inicio_w,(qt_pos_final_w - qt_pos_inicio_w)+1);
		for r_c02_w in C02() loop
			begin
			
				ie_glosou_w	:= 'S';
				exit;
			
			end;
		end loop;
		
		if	(ie_glosou_w = 'S') then
			exit;
		end if;
		end;
	end loop;
	close c01;
end if;

ie_glosou_p	:= ie_glosou_w;

end pls_consistir_regra_atend_cart;
/