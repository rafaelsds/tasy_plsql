create or replace
procedure pls_analise_desfazer_solic
			(	nr_seq_analise_p	Number,
				nm_usuario_p		Varchar2) is 
				
nr_seq_solicitacao_w		number(10);
nr_seq_proposta_w		number(10);
qt_propostas_w			number(10);
				
Cursor C01 is
	select	nr_sequencia
	from	pls_analise_solicitacao
	where	nr_seq_analise	= nr_seq_analise_p;
				

begin

select	nr_seq_proposta
into	nr_seq_proposta_w
from	pls_analise_adesao
where	nr_sequencia = nr_seq_analise_p;

select	count(*)
into	qt_propostas_w
from	pls_proposta_adesao
where	nr_sequencia = nr_seq_proposta_w
and	ie_status	in  ('A','C');

/*Proposta da an�lise foi aprovada ou contratada. N�o pode ser desfeito as solicita��es da an�lise!*/
if	(qt_propostas_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(256652);
end if;

open C01;
loop
fetch C01 into	
	nr_seq_solicitacao_w;
exit when C01%notfound;
	begin
	
	update	pls_analise_solicitacao
	set	dt_conclusao	= null,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_solicitacao_w;
	
	end;
end loop;
close C01;

update	pls_analise_adesao
set	ie_status	= 'B'
where	nr_sequencia	= nr_seq_analise_p;

commit;

end pls_analise_desfazer_solic;
/
