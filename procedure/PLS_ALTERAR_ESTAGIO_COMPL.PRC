create or replace
procedure  pls_alterar_estagio_compl
			(	nr_seq_conta_p			Number,
				nr_seq_protocolo_p		Number,
				ie_estagio_complemento_p	Varchar2,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number) is

begin

if	(nr_seq_conta_p is not null) then
	update	pls_conta
	set	ie_estagio_complemento = ie_estagio_complemento_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		dt_lib_complemento = sysdate
	where	nr_sequencia = nr_seq_conta_p;
end if;

commit;

end  pls_alterar_estagio_compl;
/
