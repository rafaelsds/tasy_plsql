create or replace
procedure pls_gerar_mensalidade_limite
			(	nr_seq_segurado_p	number,
				nr_seq_mensalidade_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X] Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_contrato_w		number(10);
nr_seq_plano_w			number(10);
nr_seq_pagador_w		number(10);
nr_seq_intercambio_w		number(10);
nr_seq_lote_w			number(10);
nr_seq_regra_w			number(10);
nr_seq_segurado_preco_w		number(10);
nr_seq_mensalidade_w		number(10);
nr_seq_mensalidade_seg_lim_w	number(10);
nr_parcela_contrato_w		number(10);
nr_parcela_seg_w		number(10);
qt_idade_w			number(3);
qt_meses_parametro_w		number(10);
qt_dias_vencimento_w		number(10);
qt_regra_rescisao_w		number(10);
qt_reg_mensalidade_w		number(10);
qt_dias_mes_w			number(10);
qt_dias_pro_rata_w		number(10);
qt_interv_venc_w		number(10);
vl_preco_atual_w		number(15,2);
vl_mens_limite_w		Number(15,2);
vl_limite_w			number(15,2);
dt_adesao_w			date;
dt_rescisao_w			date;
dt_inicio_movimentacao_w	date;
dt_fim_movimentacao_w		date;
dt_base_adesao_w		date;
dt_mes_competencia_w		date;
dt_vencimento_w			date;
ie_primeira_mensalidade_w	varchar2(1);
ie_tipo_data_limite_w		Varchar2(2);
ie_mensalidade_mes_rescisao_w	varchar2(1);
ie_agrupar_valor_w		varchar2(1);
ie_primeira_mens_regra_w	varchar2(1);
ie_data_base_adesao_w		varchar2(1);
ie_tipo_contrato_w		varchar2(2);
ie_data_base_proporcional_w	varchar2(1);
ds_dia_limite_movimentacao_w	varchar2(10);
dt_referencia_lote_mens_w	date;
ie_valor_apropriado_w		varchar2(1);
---------------------------------------------------------------------------------
dt_mesano_referencia_aux_w	pls_mensalidade_segurado.dt_mesano_referencia%type;
nr_seq_segurado_preco_aux_w	pls_mensalidade_segurado.nr_seq_segurado_preco%type;
nr_seq_mensalidade_aux_w	pls_mensalidade_segurado.nr_seq_mensalidade%type;
nr_parcela_aux_w		pls_mensalidade_segurado.nr_parcela%type;
nr_parcela_contrato_aux_w	pls_mensalidade_segurado.nr_parcela_contrato%type;
nr_serie_mensalidade_w		pls_mensalidade.nr_serie_mensalidade%type;
nr_seq_centro_apropriacao_w	pls_centro_apropriacao.nr_sequencia%type;
vl_apropriacao_w		pls_segurado_preco_aprop.vl_apropriacao%type;
vl_preco_nao_subsid_desc_w	pls_segurado_preco.vl_preco_nao_subsid_desc%type;
nr_parcela_segurado_w		number(10);
dt_inicio_cobertura_w		date;
dt_fim_cobertura_w		date;
tx_limite_1a_parc_w		number(7,4);
ie_pag_complementar_w		varchar2(1);
nr_seq_pagador_benef_w		pls_contrato_pagador.nr_sequencia%type;
nr_seq_pagador_item_w		pls_pagador_item_mens.nr_seq_pagador_item%type;
nr_seq_tabela_w			pls_segurado.nr_seq_tabela%type;
qt_registros_w			pls_integer	:= 0;
nr_seq_reajuste_w		pls_segurado_preco.nr_seq_reajuste%type;
vl_reajuste_indice_w		number(15,2);
nr_seq_lote_reajuste_w		pls_segurado_preco.nr_seq_lote_reajuste%type;
ds_mensagem_reajuste_w		varchar2(4000);
nr_seq_item_w			pls_mensalidade_seg_item.nr_sequencia%type;
tx_reajuste_w			pls_reajuste.tx_reajuste%type;
ds_oficio_ans_w			varchar2(255);
nr_protocolo_ans_w		varchar2(60);
dt_periodo_inicial_w		date;
dt_periodo_final_w		date;
dt_autorizacao_ans_w		date;
dt_reajuste_w			date;
ds_plano_w			varchar2(80);
nr_protocolo_ans_plano_w	varchar2(20);
cd_scpa_w			varchar2(20);
dt_vencimento_mens_w		date;
ie_calculo_proporcional_w	pls_contrato_pagador.ie_calculo_proporcional%type;
ie_tipo_mensalidade_w		varchar2(3);
nr_seq_preco_indice_w		pls_segurado_preco.nr_sequencia%type;
dt_mes_competencia_trunc_w 	date;
qt_pagador_vigente_w		pls_integer;
nr_seq_pagador_ww		pls_contrato_pagador.nr_sequencia%type;
ie_preco_w			varchar2(2);
ie_tipo_formacao_preco_w	varchar2(1);
nr_seq_pagador_fin_w		pls_contrato_pagador_fin.nr_sequencia%type;
nr_seq_item_mensalidade_w	pls_mensalidade_seg_item.nr_sequencia%type;
tx_proporcional_rescisao_w 	number(15,8) := 0;
ie_gerou_item_pre_w		varchar2(1);

Cursor C01 is
	select	trunc(dt_mes_competencia,'month')
	from	pls_competencia
	where	dt_mes_competencia between dt_inicio_movimentacao_w and dt_fim_movimentacao_w
	order by dt_mes_competencia;

Cursor C02 is
	select	nr_seq_centro_apropriacao,
		vl_apropriacao
	from	pls_segurado_preco_aprop
	where	nr_seq_segurado_preco	= nr_seq_segurado_preco_w;

begin
qt_meses_parametro_w		:= pls_store_data_mens_pck.get_qt_meses_parametro;

nr_seq_contrato_w		:= pls_store_data_mens_pck.get_nr_seq_contrato;
dt_adesao_w			:= pls_store_data_mens_pck.get_dt_contratacao;
nr_seq_plano_w			:= pls_store_data_mens_pck.get_nr_seq_plano;
nr_seq_pagador_w		:= pls_store_data_mens_pck.get_nr_seq_pagador;
nr_seq_intercambio_w		:= pls_store_data_mens_pck.get_nr_seq_intercambio;
dt_rescisao_w			:= pls_store_data_mens_pck.get_dt_rescisao;

nr_seq_lote_w			:= pls_store_data_mens_pck.get_nr_seq_lote_mensalidade;
ie_primeira_mensalidade_w	:= pls_store_data_mens_pck.get_ie_primeira_mensalidade;
dt_referencia_lote_mens_w	:= pls_store_data_mens_pck.get_dt_referencia_lote;

dt_mesano_referencia_aux_w	:= pls_store_data_mens_pck.get_dt_mesano_referencia;
nr_seq_segurado_preco_aux_w	:= pls_store_data_mens_pck.get_nr_seq_segurado_preco;
nr_seq_mensalidade_aux_w	:= pls_store_data_mens_pck.get_nr_seq_mensalidade;
nr_parcela_aux_w		:= pls_store_data_mens_pck.get_nr_parcela;
nr_parcela_contrato_aux_w	:= pls_store_data_mens_pck.get_nr_parcela_contrato;
nr_serie_mensalidade_w		:= pls_store_data_mens_pck.get_nr_serie_mensalidade;
ie_pag_complementar_w		:= pls_store_data_mens_pck.get_ie_pag_complementar;
ie_tipo_mensalidade_w		:= pls_store_data_mens_pck.get_ie_tipo_mensalidade;
nr_seq_pagador_benef_w		:= pls_store_data_mens_pck.get_nr_seq_pagador_benef;
				
dt_inicio_movimentacao_w	:= trunc(add_months(dt_referencia_lote_mens_w,-qt_meses_parametro_w),'month');
dt_fim_movimentacao_w		:= last_day(add_months(dt_referencia_lote_mens_w,-1)) + 86399/86400;

if	(nvl(nr_seq_contrato_w,0) <> 0) then
	select	max(a.nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_mens_contrato	a,
		pls_contrato		b
	where	a.nr_seq_contrato	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_contrato_w
	and	a.ie_tipo_regra		= 'L'
	and	trunc(dt_referencia_lote_mens_w,'dd')	>= trunc(a.dt_inicio_vigencia,'dd')
	and	trunc(dt_referencia_lote_mens_w,'dd')	<= trunc(nvl(a.dt_fim_vigencia,dt_referencia_lote_mens_w),'dd');
	
	select	decode(max(cd_pf_estipulante),null,'PJ','PF')
	into	ie_tipo_contrato_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
elsif	(nvl(nr_seq_intercambio_w,0) <> 0) then
	select	max(a.nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_mens_contrato	a,
		pls_intercambio		b
	where	a.nr_seq_intercambio	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_intercambio_w
	and	a.ie_tipo_regra		= 'L'
	and	trunc(dt_referencia_lote_mens_w,'dd')	>= trunc(a.dt_inicio_vigencia,'dd')
	and	trunc(dt_referencia_lote_mens_w,'dd')	<= trunc(nvl(a.dt_fim_vigencia,dt_referencia_lote_mens_w),'dd');
	
	select	decode(max(cd_pessoa_fisica),null,'PJ','PF')
	into	ie_tipo_contrato_w
	from	pls_intercambio
	where	nr_sequencia	= nr_seq_intercambio_w;
end if;

if	(nvl(nr_seq_regra_w,0) = 0) then
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_mens_contrato
	where	nr_seq_contrato is null
	and	nr_seq_intercambio is null
	and	ie_tipo_regra		= 'L'
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	trunc(dt_referencia_lote_mens_w,'dd')	>= trunc(dt_inicio_vigencia,'dd')
	and	trunc(dt_referencia_lote_mens_w,'dd')	<= trunc(nvl(dt_fim_vigencia,dt_referencia_lote_mens_w),'dd')
	and	((nvl(ie_pessoa_contrato,'A') = ie_tipo_contrato_w) or (ie_pessoa_contrato = 'A'));
end if;

if	(nvl(nr_seq_regra_w,0)	> 0) then
	select	lpad(to_char(nvl(a.dt_limite_movimentacao,'0')),2,0),
		a.ie_tipo_data_limite,
		nvl(a.ie_mensalidade_mes_rescisao,'N'),
		nvl(a.ie_agrupar_valor,'S'),
		qt_dias_vencimento,
		nvl(ie_primeira_mensalidade,'N'),
		nvl(ie_data_base_adesao,'A')
	into	ds_dia_limite_movimentacao_w,
		ie_tipo_data_limite_w,
		ie_mensalidade_mes_rescisao_w,
		ie_agrupar_valor_w,
		qt_dias_vencimento_w,
		ie_primeira_mens_regra_w,
		ie_data_base_adesao_w
	from	pls_regra_mens_contrato a
	where	a.nr_sequencia	= nr_seq_regra_w;
	
	if	(ds_dia_limite_movimentacao_w = '00') then
		pls_gerar_mens_log_erro(nr_seq_lote_w,nr_seq_pagador_w,nr_seq_segurado_p, nr_seq_mensalidade_p,
					'A regra de data limite n�o possui o dia de limite para a movimenta��o informado. Verifique a regra do contrato e da operadora!',
					cd_estabelecimento_p,nm_usuario_p);
	end if;
	
	if	(pls_store_data_mens_pck.get_dt_reativacao is not null) then
		dt_base_adesao_w	:= pls_store_data_mens_pck.get_dt_reativacao;
	else
		if  (nvl(ie_data_base_adesao_w,'A') = 'L') then
			dt_base_adesao_w  := pls_store_data_mens_pck.get_dt_liberacao;
		else
			dt_base_adesao_w  := dt_adesao_w;
		end if;
	end if;
	
	if	(((dt_base_adesao_w between dt_inicio_movimentacao_w and dt_fim_movimentacao_w) or (ie_tipo_mensalidade_w = 'MR') or (dt_adesao_w > add_months(dt_referencia_lote_mens_w,-pls_store_data_mens_pck.get_qt_parcelas_meses_ant))) and
		((ie_primeira_mens_regra_w = 'N') or ((ie_primeira_mens_regra_w = 'S') and (ie_primeira_mensalidade_w = 'S')))) then
		
		select	max(ie_data_base_proporcional)
		into	ie_data_base_proporcional_w
		from	pls_parametros
		where	cd_estabelecimento	= cd_estabelecimento_p;
		ie_data_base_proporcional_w	:= nvl(ie_data_base_proporcional_w,'U');
		
		if	(nvl(nr_seq_contrato_w,0) <> 0) then
			select	count(1)
			into	qt_regra_rescisao_w
			from	pls_regra_mens_contrato	a,
				pls_contrato		b
			where	a.nr_seq_contrato	= b.nr_sequencia
			and	b.nr_sequencia		= nr_seq_contrato_w
			and	a.ie_tipo_regra		= 'G'
			and	nvl(a.ie_mensalidade_mes_rescisao,'N') = 'S'
			and	trunc(dt_referencia_lote_mens_w,'dd') between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_referencia_lote_mens_w);
		else
			select	count(1)
			into	qt_regra_rescisao_w
			from	pls_regra_mens_contrato	a,
				pls_intercambio		b
			where	a.nr_seq_intercambio	= b.nr_sequencia
			and	b.nr_sequencia		= nr_seq_intercambio_w
			and	a.ie_tipo_regra		= 'G'
			and	nvl(a.ie_mensalidade_mes_rescisao,'N') = 'S'
			and	trunc(dt_referencia_lote_mens_w,'dd') between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_referencia_lote_mens_w);
		end if;
		
		if	(nvl(qt_regra_rescisao_w,0) = 0) then
			select	count(1)
			into	qt_regra_rescisao_w
			from	pls_regra_mens_contrato
			where	nr_seq_contrato	is null
			and	nr_seq_intercambio is null
			and	ie_tipo_regra		= 'G'
			and	nvl(ie_mensalidade_mes_rescisao,'N') = 'S'
			and	cd_estabelecimento	= cd_estabelecimento_p
			and	trunc(dt_referencia_lote_mens_w,'dd') between dt_inicio_vigencia and nvl(dt_fim_vigencia,dt_referencia_lote_mens_w)
			and	((nvl(ie_pessoa_contrato,'A') = ie_tipo_contrato_w) or (ie_pessoa_contrato = 'A'));
		end if;
		
		if	(trunc(dt_base_adesao_w,'month') > dt_inicio_movimentacao_w) then
			dt_inicio_movimentacao_w	:= trunc(dt_base_adesao_w,'month');
		end if;
		
		select	max(b.ie_preco)
		into	ie_preco_w
		from	pls_contrato_plano	a,
			pls_plano		b
		where	a.nr_seq_plano		= b.nr_sequencia
		and	a.nr_seq_contrato	= nr_seq_contrato_w;
		
		if	(ie_preco_w in (1,4)) then
			ie_tipo_formacao_preco_w	:= 'R';
		elsif	(ie_preco_w in (2,3)) then
			ie_tipo_formacao_preco_w	:= 'P';
		end if;
		
		open C01;
		loop
		fetch C01 into	
			dt_mes_competencia_w;
		exit when C01%notfound;
			begin
			nr_seq_preco_indice_w 	:= null;
			vl_reajuste_indice_w	:= null;
			
			select	max(nr_seq_pagador)
			into	nr_seq_pagador_ww
			from	pls_segurado_pagador
			where	dt_mes_competencia_w between dt_inicio_vigencia and nvl(dt_fim_vigencia,dt_mes_competencia_w)
			and	nr_seq_segurado = nr_seq_segurado_p;
			
			select	count(1)
			into	qt_reg_mensalidade_w
			from	pls_mensalidade_segurado a,
				pls_segurado		b,
				pls_mensalidade		c
			where	a.nr_seq_segurado	= b.nr_sequencia
			and	a.nr_seq_mensalidade	= c.nr_sequencia
			and	b.nr_sequencia		= nr_seq_segurado_p
			and	c.ie_cancelamento is null
			and	trunc(a.dt_mesano_referencia,'month') = dt_mes_competencia_w
			and	exists (	select  1
						from    pls_mensalidade_seg_item y
						where   y.nr_seq_mensalidade_seg	= a.nr_sequencia
						and     y.ie_tipo_item  in ('1','6','7','8'));
			
			if	(qt_reg_mensalidade_w = 0) and
				(nvl(nr_seq_pagador_ww,0) > 0) and
				((dt_rescisao_w is null) or 	((((qt_regra_rescisao_w = 0) and
								(trunc(dt_rescisao_w,'month') > dt_mes_competencia_w))  or
								((qt_regra_rescisao_w > 0) and
								(trunc(dt_rescisao_w,'month') >= dt_referencia_lote_mens_w) and /* OS 586842 */
								(trunc(dt_rescisao_w,'month') >= dt_mes_competencia_w))) and
								(trunc(dt_rescisao_w,'dd') <> trunc(dt_adesao_w,'dd'))
								)) then
				select	max(a.nr_sequencia)
				into	nr_seq_segurado_preco_w
				from	pls_segurado_preco	a,
					pls_segurado		b
				where	a.nr_seq_segurado	= b.nr_sequencia
				and	a.dt_liberacao	is not null
				and	b.nr_sequencia		= nr_seq_segurado_p
				and	(((trunc(dt_mes_competencia_w,'month') >= trunc(a.dt_reajuste, 'month')) and (a.cd_motivo_reajuste	<> 'E'))
					or	((a.cd_motivo_reajuste	= 'E') and (trunc(dt_mes_competencia_w,'month') >= trunc(add_months(a.dt_reajuste,1), 'month'))));
				
				if	(nvl(nr_seq_segurado_preco_w,0) > 0) then
					select	nvl(vl_preco_atual,0) - nvl(vl_desconto,0),
						nvl(vl_preco_nao_subsid_desc,0),
						qt_idade
					into	vl_preco_atual_w,
						vl_preco_nao_subsid_desc_w,
						qt_idade_w
					from	pls_segurado_preco
					where	nr_sequencia	= nr_seq_segurado_preco_w;
				end if;
				
				if  	((nvl(qt_dias_vencimento_w,0) <> 0) and
					(trunc(dt_adesao_w, 'month') = trunc(dt_mes_competencia_w,'month')))	then
					dt_vencimento_w    := dt_adesao_w  + qt_dias_vencimento_w;
				else
					dt_vencimento_w    := null;
				end if;

				qt_interv_venc_w  := months_between(dt_mes_competencia_w,dt_referencia_lote_mens_w);
					    
				select	nvl(dt_vencimento_w,dt_vencimento) 
				into	dt_vencimento_w
				from	pls_mensalidade
				where	nr_sequencia = nr_seq_mensalidade_p;
				
				if	((nr_seq_pagador_ww = nr_seq_pagador_w) and
					(ie_agrupar_valor_w = 'N')) then
					select	max(nr_sequencia)
					into	nr_seq_mensalidade_w
					from	pls_mensalidade
					where	nr_seq_lote	= nr_seq_lote_w
					and	nr_seq_pagador	= nr_seq_pagador_w
					and	trunc(dt_referencia,'month')	= dt_mes_competencia_w;
					
					if	(nvl(nr_seq_mensalidade_w,0) = 0) then
						select	pls_mensalidade_seq.nextval
						into	nr_seq_mensalidade_w
						from	dual;
						
						insert	into	pls_mensalidade
							(	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_pagador,
								dt_referencia,
								vl_mensalidade,
								nr_seq_lote,
								ds_observacao,
								dt_vencimento,
								nr_seq_forma_cobranca,
								cd_banco,
								cd_agencia_bancaria,
								ie_digito_agencia,
								cd_conta,
								ie_digito_conta,
								ie_endereco_boleto,
								nr_seq_conta_banco,
								nr_seq_compl_pf_tel_adic,
								nr_seq_pagador_fin,
								ie_nota_titulo,
								ie_tipo_formacao_preco,
								ie_apresentacao,
								nr_seq_compl_pj,
								ie_gerar_cobr_escrit,
								ie_tipo_geracao_mens,
								nr_seq_conta_banco_deb_aut,
								nr_serie_mensalidade)
							select	nr_seq_mensalidade_w,
								sysdate,
								nm_usuario_p,
								nr_seq_pagador,
								dt_mes_competencia_w,
								0,
								nr_seq_lote,
								'',
								nvl(dt_vencimento_w, add_months(dt_vencimento,qt_interv_venc_w)),
								nr_seq_forma_cobranca,
								cd_banco,
								cd_agencia_bancaria,
								ie_digito_agencia,
								cd_conta,
								ie_digito_conta,
								ie_endereco_boleto,
								nr_seq_conta_banco,
								nr_seq_compl_pf_tel_adic,
								nr_seq_pagador_fin,
								ie_nota_titulo,
								ie_tipo_formacao_preco,
								1,
								nr_seq_compl_pj,
								ie_gerar_cobr_escrit,
								'DL', -- data limite
								nr_seq_conta_banco_deb_aut,
								nr_serie_mensalidade
							from	pls_mensalidade
							where	nr_sequencia	= nr_seq_mensalidade_p;
					end if;
				elsif	(ie_agrupar_valor_w = 'S') then
					nr_seq_mensalidade_w	:= nr_seq_mensalidade_p;
				else
					select	max(nr_sequencia)
					into  	nr_seq_mensalidade_w
					from  	pls_mensalidade
					where  	nr_seq_lote  = nr_seq_lote_w
					and  	nr_seq_pagador  = nr_seq_pagador_ww
					and  	trunc(dt_referencia,'month')  = dt_mes_competencia_w;	
			
					select	max(nr_sequencia)
					into	nr_seq_pagador_fin_w
					from	pls_contrato_pagador_fin
					where	nr_seq_pagador = nr_seq_pagador_ww
					and     ((dt_fim_vigencia is null) or (dt_fim_vigencia >= to_date(sysdate)));
					
					if  (nvl(nr_seq_mensalidade_w,0) = 0) then
						select  pls_mensalidade_seq.nextval
						into  nr_seq_mensalidade_w
						from  dual;
						  
						insert  into  	pls_mensalidade
							(  	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_pagador,
								dt_referencia,
								vl_mensalidade,
								nr_seq_lote,
								ds_observacao,
								dt_vencimento,
								nr_seq_forma_cobranca,
								cd_banco,
								cd_agencia_bancaria,
								ie_digito_agencia,
								cd_conta,
								ie_digito_conta,
								ie_endereco_boleto,
								nr_seq_conta_banco,
								nr_seq_compl_pf_tel_adic,
								nr_seq_pagador_fin,
								ie_nota_titulo,
								ie_tipo_formacao_preco,
								ie_apresentacao,
								nr_seq_compl_pj,
								ie_gerar_cobr_escrit,
								ie_tipo_geracao_mens,
								nr_seq_conta_banco_deb_aut,
								nr_serie_mensalidade)
							select	nr_seq_mensalidade_w,
								sysdate,
								nm_usuario_p,
								nr_seq_pagador_ww,
								dt_mes_competencia_w,
								0,
								nr_seq_lote_w,
								'',
								dt_vencimento_w,
								a.nr_seq_forma_cobranca,
								a.cd_banco,
								a.cd_agencia_bancaria,
								a.ie_digito_agencia,
								a.cd_conta,
								a.ie_digito_conta,
								b.ie_endereco_boleto,
								a.nr_seq_conta_banco,
								b.nr_seq_compl_pf_tel_adic,
								a.nr_sequencia,
								a.ie_geracao_nota_titulo,
								ie_tipo_formacao_preco_w,
								1,
								b.nr_seq_compl_pj,
								a.ie_gerar_cobr_escrit,
								'DL',
								a.nr_seq_conta_banco_deb_aut,
								pls_store_data_mens_pck.get_nr_serie_mensalidade
							from  	pls_contrato_pagador_fin a,
								pls_contrato_pagador b
							where  	a.nr_seq_pagador = b.nr_sequencia
							and    	a.nr_sequencia = nr_seq_pagador_fin_w;
					end if;		  
				end if;
				
				nr_parcela_segurado_w	:= pls_obter_parcela_segurado(nr_seq_segurado_p, dt_mes_competencia_w, dt_mes_competencia_w);
				
				/* Obter a data de cobertura da mensalidade do benefici�rio */
				if	(nr_parcela_segurado_w = 1) and 
					(pls_store_data_mens_pck.get_ie_calc_primeira_mens = 'P') then
					dt_inicio_cobertura_w	:= dt_adesao_w;
					dt_fim_cobertura_w	:= last_day(dt_adesao_w);
				elsif	(nr_parcela_segurado_w = 1) and 
					(pls_store_data_mens_pck.get_ie_calc_primeira_mens = 'I') then
					dt_inicio_cobertura_w	:= dt_adesao_w;
					dt_fim_cobertura_w	:= add_months(dt_adesao_w,1)-1;
				elsif	(nr_parcela_segurado_w > 1) then
					if	(pls_store_data_mens_pck.get_ie_mensalidade_proporc = 'S') then
						dt_inicio_cobertura_w	:= trunc(dt_mes_competencia_w,'month');
						dt_fim_cobertura_w	:= last_day(dt_mes_competencia_w);
					else
						begin
						dt_inicio_cobertura_w	:= to_date(to_char(dt_adesao_w,'dd') || '/'|| to_char(dt_mes_competencia_w,'mm/yyyy'));
						exception
						when others then
							dt_inicio_cobertura_w	:= last_day(dt_mes_competencia_w);
						end;
						
						dt_fim_cobertura_w	:= add_months(dt_inicio_cobertura_w,1)-1;
					end if;
				end if;
				
				select	pls_obter_parcela_segurado(nr_seq_segurado_p, dt_mes_competencia_w, dt_mes_competencia_w),
					pls_obter_parcela_contrato(nr_seq_contrato_w, dt_mes_competencia_w)
				into	nr_parcela_seg_w,
					nr_parcela_contrato_w
				from	dual;				
				/*Insere o registro na tabela PLS_MENSALIDADE_SEGURADO*/
				pls_store_data_mens_pck.insert_record_mensalidade(nm_usuario_p, nr_seq_segurado_p, nr_seq_mensalidade_w, qt_idade_w,
							dt_mes_competencia_w, nr_parcela_seg_w, nr_seq_plano_w, nr_seq_contrato_w, nr_parcela_contrato_w,
							pls_store_data_mens_pck.get_nr_seq_reajuste, null, null, dt_inicio_cobertura_w, dt_fim_cobertura_w, pls_store_data_mens_pck.get_nr_seq_titular,
							dt_rescisao_w, pls_store_data_mens_pck.get_nr_seq_parentesco,
							pls_store_data_mens_pck.get_nr_seq_subestipulante,pls_store_data_mens_pck.get_nr_seq_localizacao_benef);
				
				nr_seq_mensalidade_seg_lim_w	:= pls_store_data_mens_pck.get_nr_seq_mensalidade_seg;
				
				qt_dias_mes_w	:= 0;
				
				select	max(nr_seq_tabela)
				into	nr_seq_tabela_w
				from	pls_segurado
				where	nr_sequencia = nr_seq_segurado_p;
				
				pls_store_data_mens_pck.calcular_vls_reajuste_indice(nr_seq_tabela_w,null,nr_seq_segurado_p);
				nr_seq_preco_indice_w := pls_store_data_mens_pck.get_nr_seq_preco_indice;
				nr_seq_reajuste_w   := pls_store_data_mens_pck.get_nr_seq_reajuste;
				
				if	(nr_seq_reajuste_w is not null) then
					select	trunc(max(dt_reajuste),'month')
					into	dt_reajuste_w
					from	pls_reajuste_preco
					where	nr_sequencia = nr_seq_reajuste_w;
					
					dt_mes_competencia_trunc_w := trunc(dt_mes_competencia_w,'month');
					
					if	(dt_mes_competencia_trunc_w >= dt_reajuste_w) then
						vl_reajuste_indice_w := pls_store_data_mens_pck.get_vl_reajuste_indice;
					end if;
				end if;
				
				if	(vl_reajuste_indice_w > 0) then
					vl_preco_atual_w	:= nvl(vl_preco_atual_w,0) - nvl(vl_reajuste_indice_w,0);
					ds_mensagem_reajuste_w  := pls_store_data_mens_pck.get_ds_mensagem_reajuste;
				end if;
				
				if	(pls_store_data_mens_pck.get_ie_calc_primeira_mens	= 'I') or
					(nr_parcela_seg_w > 1) then
					vl_limite_w	:= nvl(vl_preco_atual_w,0);
				else
					vl_mens_limite_w	:= nvl(vl_preco_atual_w,0);
					
					select	max(ie_calculo_proporcional)
					into	ie_calculo_proporcional_w
					from	pls_contrato_pagador
					where	nr_sequencia = pls_store_data_mens_pck.get_nr_seq_pagador;
					
					--M�s calendario ades�o
					if	(ie_calculo_proporcional_w	= 'A') then
						dt_vencimento_mens_w	:= last_day(dt_adesao_w);
					else
						begin
						dt_vencimento_mens_w	:= to_date(to_char(pls_store_data_mens_pck.get_dt_dia_vencimento) || '/' || to_char(dt_adesao_w,'mm/yyyy'));
						exception
						when others then
							dt_vencimento_mens_w	:= last_day(dt_adesao_w);
						end;
					end if;
					
					if	(dt_vencimento_mens_w < dt_adesao_w) then
						dt_vencimento_mens_w	:= last_day(dt_vencimento_mens_w);
					end if;
					
					qt_dias_pro_rata_w	:= obter_dias_entre_datas(dt_adesao_w,dt_vencimento_mens_w) + 1;
					
					if	(ie_data_base_proporcional_w = 'T') then
						qt_dias_mes_w	:= 30;
						--tratamento m�s de fevereiro.
						if	(to_char(dt_vencimento_mens_w,'mm') = 02) then
							qt_dias_mes_w	:= to_char(last_day(dt_vencimento_mens_w),'dd');
						end if;
					else
						qt_dias_mes_w	:= to_number(to_char(last_day(dt_vencimento_mens_w),'dd'));
					end if;
					
					if	(dt_rescisao_w is not null) then
						tx_proporcional_rescisao_w := (qt_dias_pro_rata_w / qt_dias_mes_w);	
					end if;
					
					tx_limite_1a_parc_w 	:= (qt_dias_pro_rata_w / qt_dias_mes_w);
					vl_limite_w		:= tx_limite_1a_parc_w * vl_mens_limite_w;
					vl_preco_nao_subsid_desc_w := tx_limite_1a_parc_w * vl_preco_nao_subsid_desc_w;
					vl_reajuste_indice_w	:= tx_limite_1a_parc_w * vl_reajuste_indice_w;
					
					update	pls_segurado
					set	ie_mensalidade_proporcional	= 'S'
					where	nr_sequencia = nr_seq_segurado_p;
					
					pls_store_data_mens_pck.set_ie_mensalidade_proporc('S');
				end if;
				
				if	(nvl(vl_limite_w,0) <> 0) then
					if	(vl_preco_nao_subsid_desc_w > 0) then
						if	(nvl(pls_store_data_mens_pck.get_seq_pagador_subsid, pls_store_data_mens_pck.get_nr_seq_pagador_benef) = nr_seq_pagador_w) then
							vl_limite_w	:= vl_limite_w - vl_preco_nao_subsid_desc_w;
							if	(vl_limite_w	< 0) then
								vl_limite_w	:= 0;
							end if;
							
							nr_seq_item_mensalidade_w := null;

							pls_insert_mens_seg_item('12',	nm_usuario_p,	null,
										null,	null,	null,
										null,	null,	null,	
										'N',	null,	null,		
										null,	null,	null,			
										null,	null,	nr_seq_mensalidade_seg_lim_w,	
										null,	null,	null,			
										null,	null,	null,	
										null,	null,	null,		
										null,	null,	null,		
										null,	vl_preco_nao_subsid_desc_w,	nr_seq_item_mensalidade_w);
						end if;
					end if;
					
					if	(nvl(pls_store_data_mens_pck.get_seq_pagador_preco_pre, pls_store_data_mens_pck.get_nr_seq_pagador_benef) = nr_seq_pagador_w) then
						
						ie_gerou_item_pre_w	  := 'N';
						nr_seq_item_mensalidade_w := null;
						
						pls_insert_mens_seg_item('1',	nm_usuario_p,	null,
									null,	null,	null,
									null,	null,	null,	
									'N',	null,	null,		
									null,	null,	null,			
									null,	null,	nr_seq_mensalidade_seg_lim_w,	
									null,	null,	null,			
									null,	null,	null,	
									null,	null,	null,		
									null,	null,	null,		
									null,	vl_limite_w,	nr_seq_item_mensalidade_w);
									
						if	(nr_seq_item_mensalidade_w is not null) then
							ie_gerou_item_pre_w := 'S';
							pls_store_data_mens_pck.set_nr_seq_mens_item_pre(nr_seq_item_mensalidade_w);
							pls_inserir_apropriacao_preco('1', nr_seq_item_mensalidade_w, nr_seq_segurado_preco_w, vl_limite_w, 0, tx_limite_1a_parc_w, tx_proporcional_rescisao_w, nm_usuario_p); 
						end if;
					end if;
				end if;
				
				if	((nvl(vl_reajuste_indice_w,0)	> 0) and (ie_gerou_item_pre_w = 'S')) then
					if	(ie_pag_complementar_w = 'N') then
						select	max(nr_seq_pagador_item)
						into	nr_seq_pagador_item_w
						from	pls_pagador_item_mens
						where	nr_seq_pagador	= nr_seq_pagador_w
						and	ie_tipo_item	= '25';
					else
						select	max(nr_seq_pagador_item)
						into	nr_seq_pagador_item_w
						from	pls_pagador_item_mens
						where	nr_seq_pagador_item	= nr_seq_pagador_w
						and	nr_seq_pagador		= nr_seq_pagador_benef_w
						and	ie_tipo_item	= '25';
					end if;
					
					if	(((nvl(nr_seq_pagador_item_w,nr_seq_pagador_w) = nr_seq_pagador_w) and (ie_pag_complementar_w = 'N')) or
						((ie_pag_complementar_w = 'S') and (nr_seq_pagador_item_w is not null))) then
						
						nr_seq_item_mensalidade_w := null;
						
						pls_insert_mens_seg_item('25',	nm_usuario_p,	null,
									ds_mensagem_reajuste_w,	null,	null,
									null,	null,	null,	
									'N',	null,	null,		
									null,	null,	null,			
									null,	null,	nr_seq_mensalidade_seg_lim_w,	
									null,	null,	null,			
									null,	nr_seq_reajuste_w,	null,	
									null,	null,	null,		
									null,	null,	null,		
									null,	vl_reajuste_indice_w,	nr_seq_item_mensalidade_w);

						if	(nr_seq_item_mensalidade_w is not null) then
							pls_inserir_apropriacao_preco('25', nr_seq_item_mensalidade_w, nr_seq_preco_indice_w, vl_reajuste_indice_w, 
										0, tx_limite_1a_parc_w, tx_proporcional_rescisao_w, nm_usuario_p);
						end if;
					end if;	
				end if;
				
				if	(nvl(pls_store_data_mens_pck.get_ie_taxa_inscricao,'N') = 'S') and
					(ie_pag_complementar_w = 'N') then
					pls_gerar_mens_taxa_insc(nr_seq_mensalidade_seg_lim_w,nm_usuario_p);
				end if;
				
				if	(ie_pag_complementar_w = 'N') then
					begin
					pls_gerar_mens_sca(nr_seq_mensalidade_seg_lim_w,ie_mensalidade_mes_rescisao_w,nm_usuario_p);
					exception
					when others then
						pls_gerar_mens_log_erro(nr_seq_lote_w,nr_seq_pagador_w,nr_seq_segurado_p,nr_seq_mensalidade_p,
									'Erro ao gerar os valores de retroativos de SCA da mensalidade, com regra de data limite'||chr(13)||chr(10)||sqlerrm,
									cd_estabelecimento_p,nm_usuario_p);
					end;
				end if;
				
				if	(ie_pag_complementar_w = 'N') then
					/* VIA ADICIONAL */
					begin
					pls_gerar_mens_via_adicional(nr_seq_mensalidade_seg_lim_w, nm_usuario_p);
					exception
					when others then
						pls_gerar_mens_log_erro(nr_seq_lote_w,nr_seq_pagador_w,nr_seq_segurado_p,nr_seq_mensalidade_p,
									'Erro ao gerar os valores adicionais da mensalidade'||chr(13)||chr(10)||sqlerrm,cd_estabelecimento_p,nm_usuario_p);
					end;
				end if;
				
				if	(nvl(pls_store_data_mens_pck.get_seq_pagador_bonificacao, pls_store_data_mens_pck.get_nr_seq_pagador_benef) = nr_seq_pagador_w) then
					/* Gerar bonifica��o */
					begin
					pls_gerar_mens_bonificacao(nr_seq_mensalidade_seg_lim_w, nr_seq_intercambio_w, nr_seq_contrato_w, nr_seq_segurado_p, nm_usuario_p);
					exception
					when others then
						pls_gerar_mens_log_erro(nr_seq_lote_w,nr_seq_pagador_w,nr_seq_segurado_p,nr_seq_mensalidade_p,
									'Erro ao gerar as bonifica��o da mensalidade, com regra de data limite'||chr(13)||chr(10)||sqlerrm,cd_estabelecimento_p,nm_usuario_p);
					end;
				end if;
				
				pls_gerar_mens_lanc_prog(nr_seq_mensalidade_seg_lim_w,null,nm_usuario_p);
				/*
				pls_gerar_mens_coparticipacao(nr_seq_mensalidade_seg_lim_w, nm_usuario_p, cd_estabelecimento_p);
				pls_gerar_mens_pos_estab(nr_seq_mensalidade_seg_lim_w, null, 'A', nm_usuario_p);
				pls_gerar_mens_co(nr_seq_mensalidade_seg_lim_w, nm_usuario_p,cd_estabelecimento_p);
				*/
				
				pls_atualiza_valor_mens_seg(nr_seq_mensalidade_seg_lim_w, nm_usuario_p);
			end if;
			end;
		end loop;
		close C01;
	end if;
end if;

pls_store_data_mens_pck.clear_pls_mensalidade_segurado;
pls_store_data_mens_pck.set_dt_mesano_referencia(dt_mesano_referencia_aux_w);
pls_store_data_mens_pck.set_nr_seq_segurado_preco(nr_seq_segurado_preco_aux_w);
pls_store_data_mens_pck.set_nr_seq_mensalidade(nr_seq_mensalidade_aux_w);
pls_store_data_mens_pck.set_nr_parcela(nr_parcela_aux_w);
pls_store_data_mens_pck.set_nr_parcela_contrato(nr_parcela_contrato_aux_w);

end pls_gerar_mensalidade_limite;
/