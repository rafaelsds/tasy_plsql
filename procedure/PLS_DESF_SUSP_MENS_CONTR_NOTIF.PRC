create or replace
procedure pls_desf_susp_mens_contr_notif
			(	nr_seq_lote_p			number,
				nr_seq_pagador_p		number,
				dt_fim_susp_p			date,
				nm_usuario_p			Varchar2,
				ie_cobrar_periodo_susp_p	varchar2,
				nr_seq_etapa_cobr_p		etapa_cobranca.nr_sequencia%type) is

cursor c01 is
	select	nr_seq_pagador,
		nr_sequencia nr_seq_notificacao_pagador
	from	pls_notificacao_pagador
	where	nr_seq_lote	= nr_seq_lote_p
	and	(nr_seq_pagador_p = 0 or nr_seq_pagador = nr_seq_pagador_p);

Cursor C02 (	nr_seq_notific_pagador_pc	pls_notificacao_pagador.nr_sequencia%type) is
	select	obter_cobranca_titulo(a.nr_titulo,'S') nr_seq_cobranca
	from	pls_notificacao_item a
	where	a.nr_seq_notific_pagador	= nr_seq_notific_pagador_pc;

begin

for r_c01_w in C01 loop
	begin
	
	update	pls_contrato_susp_mens
	set	dt_fim_suspensao	= dt_fim_susp_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		ie_cobrar_periodo_susp	= ie_cobrar_periodo_susp_p
	where	nr_seq_pagador		= r_c01_w.nr_seq_pagador
	and	nr_seq_notificacao	= nr_seq_lote_p
	and	dt_fim_suspensao is null;
	
	update	pls_notificacao_pagador
	set	dt_susp_mens 		= null,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= r_c01_w.nr_seq_notificacao_pagador;
	
	for r_c02_w in C02(r_c01_w.nr_seq_notificacao_pagador) loop
		begin
		if	(r_c02_w.nr_seq_cobranca is not null) then
			if	(obter_etapa_cobranca(r_c02_w.nr_seq_cobranca) = nr_seq_etapa_cobr_p) then
				delete	from	cobranca_etapa
				where	nr_seq_cobranca	= r_c02_w.nr_seq_cobranca
				and	nr_seq_etapa	= nr_seq_etapa_cobr_p;
				
				atualizar_cobranca_etapa(r_c02_w.nr_seq_cobranca, nm_usuario_p);
			end if;
		end if;
		end;
	end loop;
	
	end;
end loop;

update	pls_notificacao_lote
set	dt_susp_mens		= null,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_lote_p;

commit;

end pls_desf_susp_mens_contr_notif;
/