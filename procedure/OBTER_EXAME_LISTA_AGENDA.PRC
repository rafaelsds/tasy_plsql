create or replace
function obter_exame_lista_agenda (
	nr_sequencia_p	number)
		return varchar2 is

ds_exame_w		varchar2(255);
nr_seq_exame_w		number(10);
ds_exame_sem_cad_w		varchar2(255);
nr_seq_exame_lab_w		number(10);
nr_proc_interno_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);

begin

select	nr_seq_exame,
	ds_exame_sem_cad,
	nr_seq_exame_lab,
	nr_seq_proc_interno,
	cd_procedimento,
	ie_origem_proced
into	nr_seq_exame_w,
	ds_exame_sem_cad_w,
	nr_seq_exame_lab_w,
	nr_proc_interno_w,
	cd_procedimento_w,
	ie_origem_proced_w
from	agenda_lista_espera
where	nr_sequencia = nr_sequencia_p;

if	(nr_seq_exame_w > 0) then
	begin
	select	ds_exame
	into	ds_exame_w
	from	med_exame_padrao
	where	nr_sequencia = nr_seq_exame_w;
	end;
elsif	(ds_exame_sem_cad_w is not null) then
	begin
	ds_exame_w := ds_exame_sem_cad_w;
	end;
elsif	(nr_seq_exame_lab_w is not null) then
	begin
	ds_exame_w := substr(obter_desc_exame(nr_seq_exame_lab_w),1,255);
	end;
elsif	(nr_proc_interno_w is not null) then
	begin
	ds_exame_w := substr(obter_desc_proc_interno(nr_proc_interno_w),1,255);
	end;
elsif	(cd_procedimento_w is not null) and
	(ie_origem_proced_w is not null) then
	begin
	ds_exame_w := substr(obter_descricao_procedimento(cd_procedimento_w,ie_origem_proced_w),1,255);
	end;
end if;

return ds_exame_w;

end obter_exame_lista_agenda;
/