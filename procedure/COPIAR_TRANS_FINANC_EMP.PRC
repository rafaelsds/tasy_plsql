create or replace
procedure copiar_trans_financ_emp(	cd_empresa_origem_p	number,
															cd_empresa_destino_p number,
															nm_usuario_p		varchar2) is 

cd_estab_matriz_origem_w  estabelecimento.cd_estabelecimento%type;
cd_estab_matriz_destino_w estabelecimento.cd_estabelecimento%type;
qt_reg_orig_w	number(10);
qt_reg_dest_w	number(10);

cursor c01 is
	select	nr_sequencia
	from		transacao_financeira
	where	cd_empresa = cd_empresa_origem_p;

c01_w		c01%rowtype;

begin
	begin
		select 	cd_estabelecimento
	    	into	cd_estab_matriz_origem_w
		from 	estabelecimento
	    	where 	ie_tipo_estab = 'M'
	    	and	cd_empresa = cd_empresa_origem_p;
	exception
		when no_data_found then
	      		wheb_mensagem_pck.exibir_mensagem_abort(1099799);
		when too_many_rows THEN
	      	wheb_mensagem_pck.exibir_mensagem_abort(956422);
	end;
	
  	begin
		select 	cd_estabelecimento
		into 	cd_estab_matriz_destino_w
		from 	estabelecimento
	    	where 	ie_tipo_estab = 'M'
    		and 	cd_empresa = cd_empresa_destino_p;
	exception
	when no_data_found then
		wheb_mensagem_pck.exibir_mensagem_abort(1099800);
	when too_many_rows THEN
		wheb_mensagem_pck.exibir_mensagem_abort(956422);
	end;
	
	select	count(*)
	into	qt_reg_orig_w
	from	transacao_financeira
	where	cd_empresa = cd_empresa_origem_p;
	
	select	count(*)
	into	qt_reg_dest_w
	from	transacao_financeira
	where	cd_empresa = cd_empresa_destino_p;
	
	if	(dividir((qt_reg_dest_w * 100),  qt_reg_orig_w) <= 10 and qt_reg_orig_w > 0) then
		open c01;
		loop
		fetch c01 into	
			c01_w;
		exit when c01%notfound;
			copiar_transacao_empresa(	c01_w.nr_sequencia,
													cd_empresa_destino_p,
													cd_estab_matriz_destino_w,
													cd_estab_matriz_origem_w,
													nm_usuario_p,
													'S',
													null,
													'N');
		end loop;
		close c01;
	end if;

commit;

end copiar_trans_financ_emp;
/