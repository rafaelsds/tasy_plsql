create or replace
procedure Atualizar_cirurgia_mat_log		( nr_prescricao_p	number,
						  nr_cirurgia_p		number,
						  nm_usuario_p		Varchar2) is 

						  
ie_log_incl_excl_w	varchar2(1);
						  
begin

ie_log_incl_excl_w	:= nvl(obter_valor_param_usuario(900, 205, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'N');

if  (ie_log_incl_excl_w = 'S') and
    (nvl(nr_prescricao_p,0) > 0) and
    (nvl(nr_cirurgia_p,0) > 0) then
    
    update mat_atend_pac_log
    set    nr_cirurgia = nr_cirurgia_p
    where  nr_prescricao = nr_prescricao_p
    and    nr_cirurgia is null;	

commit;
    
end if;    


end Atualizar_cirurgia_mat_log;
/