create or replace 
procedure material_ficha_tecnica_prod(	productcode_p		number,
					nm_usuario_p		Varchar2,
					nr_seq_ficha_tecnica_p	out number,
          nr_mims_version_p  number) is 
			
qt_reg_w		number(10);
produc_code_w		number(10);
nr_seq_ficha_tecnica_w	number(10);
nr_seq_superior_w	number(10);
gen_code_superior_w	number(10);
ds_superior_w		varchar2(255);
ie_unico_w		boolean;
cd_atc_w		varchar2(255);
ie_varios_w		varchar2(10);
mims_ficha_prod_w	mims_ficha_prod%rowtype;

	
Cursor C02 is
	select	distinct b.*,
		nvl(b.approved,b.GENCODE) generic_code
	from	ActiveComposition a,
		GENDAT b
	where	a.genericcode = b.GENCODE
	and 	a.ProductCode = produc_code_w;
	
	
Cursor C03 is
	select	 a.gencode, max(a.genericsort) genericsort, b.generic
	from	GMDAT a,
		GENDAT b
	where	a.prodcode = produc_code_w
	and	b.gencode = a.gencode
	group by a.gencode,  b.generic
	order by genericsort;
	
	
cursor	c04 is
	select	a.nr_sequencia nr_seq_ficha_tecnica
	from	medic_ficha_tecnica a,
		DCB_MEDIC_CONTROLADO b
	where	a.nr_seq_dcb	= b.nr_sequencia
	and	ie_varios_w = 'N'
	and	a.nr_seq_superior is null
	and	exists	(	select	1
				from	ActiveComposition x,
					GENDAT y
				where	x.genericcode = y.GENCODE
				and 	x.ProductCode = produc_code_w
				and	nvl(to_char(y.approved),to_char(y.GENCODE)) = b.CD_DCB)
	union	
	select	a.nr_seq_superior nr_seq_ficha_tecnica
	from	medic_ficha_tecnica a,
		DCB_MEDIC_CONTROLADO b
	where	a.nr_seq_dcb	= b.nr_sequencia
	and	a.nr_seq_superior is not null
	and	ie_varios_w = 'S'
	and	exists	(	select	1
				from	ActiveComposition x,
					GENDAT y
				where	x.genericcode = y.GENCODE
				and 	x.ProductCode = produc_code_w
				and	nvl(to_char(y.approved),to_char(y.GENCODE)) = b.CD_DCB);
	
begin

select	max(nr_seq_ficha_tecnica)
into	nr_seq_superior_w
from	mims_ficha_prod
where	productcode	= productcode_p
-- and nr_seq_mims_version is null;
and NR_MAIN_TABLES_REF_VERSION = nr_mims_version_p;


if	(nr_seq_superior_w is null) then
	produc_code_w	:= productcode_p;

	select	decode(count(distinct genericcode),1,'N','S')
	into	ie_varios_w
	from	ActiveComposition
	where	productcode = produc_code_w;
	


	for r_c04 in c04 loop
		begin
		select	count(1)
		into	qt_reg_w
		from	(	select	distinct to_char(nvl(y.approved,y.GENCODE))
				from	ActiveComposition x,
					GENDAT y
				where	x.genericcode = y.GENCODE
				and 	x.ProductCode = produc_code_w
				minus
				select	z.cd_dcb
				from	(	select	b.CD_DCB
						from	medic_ficha_tecnica a,
							DCB_MEDIC_CONTROLADO b
						where	a.nr_seq_dcb	= b.nr_sequencia
						and	ie_varios_w = 'N'
						and	a.nr_sequencia = r_c04.nr_seq_ficha_tecnica
						and	a.nr_seq_superior is null
						union	
						select	b.CD_DCB
						from	medic_ficha_tecnica a,
							DCB_MEDIC_CONTROLADO b
						where	a.nr_seq_dcb	= b.nr_sequencia
						and	a.nr_seq_superior = r_c04.nr_seq_ficha_tecnica
						and	ie_varios_w = 'S') z
			);
		
		if	(qt_reg_w	=0) then
			select	count(1)
			into	qt_reg_w
			from	(	select	z.CD_DCB 
					from	(
							select	b.CD_DCB
							from	medic_ficha_tecnica a,
								DCB_MEDIC_CONTROLADO b
							where	a.nr_seq_dcb	= b.nr_sequencia
							and	ie_varios_w = 'N'
							and	a.nr_sequencia = r_c04.nr_seq_ficha_tecnica
							and	a.nr_seq_superior is null
							union	
							select	b.CD_DCB
							from	medic_ficha_tecnica a,
								DCB_MEDIC_CONTROLADO b
							where	a.nr_seq_dcb	= b.nr_sequencia
							and	a.nr_seq_superior = r_c04.nr_seq_ficha_tecnica
							and	ie_varios_w = 'S')  z
					minus
					select	distinct to_char(nvl(y.approved,y.GENCODE))
					from	ActiveComposition x,
						GENDAT y
					where	x.genericcode = y.GENCODE
					and 	x.ProductCode = produc_code_w
				); 

				
				if	(qt_reg_w	= 0) then
					nr_seq_superior_w	:= r_c04.nr_seq_ficha_tecnica;
					exit;
				end if;
			
		end if;
		
	
		end;
	end loop;
	

	if	(nr_seq_superior_w is null) then
		begin
		nr_seq_ficha_tecnica_w	:= null;
		ds_superior_w		:= null;
		
		
		if	(produc_code_w	is not null)  then
			
			select	count(distinct genericcode)
			into	qt_reg_w
			from	ActiveComposition
			where	productcode = produc_code_w;
			
			ie_unico_w	:= (qt_reg_w = 1); 
			
			if	(not ie_unico_w) then
				
				for r_c03 in c03 loop
					begin
					
					if	(ds_superior_w is null) or
						(length (ds_superior_w || ' + '|| r_c03.generic ) < 80) then
						if	(ds_superior_w is null) then
							ds_superior_w	:= r_c03.generic;
						else
							ds_superior_w	:= ds_superior_w || ' + '|| r_c03.generic;
						end if;
						
					else
						ds_superior_w	:= substr(ds_superior_w || ' + '||'etc.' ,1,80);
						exit;
					end if;
					
					
					end;
				end loop;
			
				material_imp_ficha_tec(	null,
							ds_superior_w,
							null,
							nr_seq_superior_w,
							null,
							null,
							nm_usuario_p
              );
				
				
			end if;
					

			for r_c02 in c02 loop
				begin
				
				material_imp_ficha_tec(	r_c02.generic_code,
							r_c02.GENERIC,
							nr_seq_superior_w,
							nr_seq_ficha_tecnica_w,
							r_c02.generic_code,
							null,
							nm_usuario_p
              );
				
				null;
				
				end;
			end loop;
			
			nr_seq_superior_w	:= nvl(nr_seq_superior_w,nr_seq_ficha_tecnica_w);
			
					
		end if;
		
		end;
	end if;
	
	if	(nr_seq_superior_w is not null) then
		
		mims_ficha_prod_w.PRODUCTCODE		:= productcode_p;
		mims_ficha_prod_w.NR_SEQ_FICHA_TECNICA	:= nr_seq_superior_w;
		mims_ficha_prod_w.NR_MAIN_TABLES_REF_VERSION := nr_mims_version_p;
		
		insert into mims_ficha_prod values mims_ficha_prod_w;
		
	end if;

end if;

nr_seq_ficha_tecnica_p	:= nr_seq_superior_w;

end material_ficha_tecnica_prod;
/