create or replace
procedure atualizar_local_estoque_prescr(
		cd_local_estoque_p	number,
		nr_prescricao_p		number,
		nm_usuario_p		varchar2) is

begin
if	(cd_local_estoque_p is not null) and
	(nr_prescricao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	update	prescr_medica
	set	cd_local_estoque	= cd_local_estoque_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_prescricao		= nr_prescricao_p;
	end;
end if;
commit;
end atualizar_local_estoque_prescr;
/