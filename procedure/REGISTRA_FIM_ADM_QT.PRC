create or replace
procedure REGISTRA_FIM_ADM_QT (		nr_seq_atendimento_p 	number,
					nr_seq_p		number,
					ie_tipo_seq_p		varchar2,
					cd_profissional_p	varchar2,
					nm_usuario_p		Varchar2) is 

nr_seq_material_w		number(10);
cd_profissional_w       varchar2(10);
					
Cursor C01 is
	select	nr_seq_material
	from 	paciente_atend_medic
	where	nr_seq_atendimento	=	nr_seq_atendimento_p
	and	nr_agrupamento		=	nr_seq_p;
	
Cursor C02 is
	select	nr_seq_material
	from 	paciente_atend_medic
	where	nr_seq_atendimento	=	nr_seq_atendimento_p
	and	nr_seq_solucao		=	nr_seq_p;
	
begin

if (cd_profissional_p is null) then
	cd_profissional_w := substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10);
else
	cd_profissional_w := cd_profissional_w;
end if;

if (ie_tipo_seq_p = 'A') then  -- Agrupador
	open C01;
	loop
	fetch C01 into	
		nr_seq_material_w;
	exit when C01%notfound;
		begin
		
		if 	(nr_seq_atendimento_p is not null) and
			(nr_seq_material_w is not null) then
			
			update	paciente_atend_medic_adm
			set	dt_fim_administracao 	= sysdate,
				cd_profissional_termino = cd_profissional_w,
				nm_usuario 		= nm_usuario_p
			where	nr_seq_atendimento 	= nr_seq_atendimento_p
			and	nr_seq_material 	= nr_seq_material_w;
            
            update	paciente_atend_medic
            set		ie_administracao	=	'A'
            where	nr_seq_atendimento	=	nr_seq_atendimento_p
            and		nr_agrupamento		=	nr_seq_p;
			
		end if;
		end;
	end loop;
	close C01;

elsif (ie_tipo_seq_p = 'S') then  -- Solucao
	open C02;
	loop
	fetch C02 into	
		nr_seq_material_w;
	exit when C02%notfound;
		begin
		
		if 	(nr_seq_atendimento_p is not null) and
			(nr_seq_material_w is not null) then
			
			update	paciente_atend_medic_adm
			set	dt_fim_administracao 	= sysdate,
				cd_profissional_termino = cd_profissional_w,
				nm_usuario 		= nm_usuario_p
			where	nr_seq_atendimento 	= nr_seq_atendimento_p
			and	nr_seq_material 	= nr_seq_material_w
			and	dt_fim_administracao is null;
            
            update	paciente_atend_medic
            set		ie_administracao	=	'A'
            where	nr_seq_atendimento	=	nr_seq_atendimento_p
            and		nr_seq_solucao	is not null;
            
		end if;
		end;
	end loop;
	close C02;

elsif (ie_tipo_seq_p = 'M') then  -- Material
	if 	(nr_seq_atendimento_p is not null) and
		(nr_seq_p is not null) then
		
		update	paciente_atend_medic_adm
		set	dt_fim_administracao 	= sysdate,
			cd_profissional_termino = cd_profissional_w,
			nm_usuario 		= nm_usuario_p
		where	nr_seq_atendimento 	= nr_seq_atendimento_p
		and	nr_seq_material 	= nr_seq_p
		and	dt_fim_administracao is null;
        
    update  paciente_atend_medic
    set     ie_administracao	=	'A'
    where   nr_seq_atendimento	=	nr_seq_atendimento_p
    and     nr_seq_material	=	nr_seq_p;

	end if;
end if;

commit;

end REGISTRA_FIM_ADM_QT;
/

