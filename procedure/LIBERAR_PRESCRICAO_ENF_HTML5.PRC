create or replace
procedure liberar_prescricao_enf_html5(nr_prescricao_p			in prescr_medica.nr_prescricao%type,
										nr_atendimento_p		in prescr_medica.nr_atendimento%type,
										ie_tipo_pessoa_p		in varchar2,
										cd_perfil_p				in perfil.cd_perfil%type,
										nm_usuario_p			in usuario.nm_usuario%type,
										ie_prescricao_p			in varchar2,
										ds_erro_p				out varchar2,
										nr_prescricao_erro_p	out prescr_medica.nr_prescricao%type) is

ds_erro_w				varchar2(4000 char);
ds_log_w				varchar2(4000 char);
ds_observacao_copia_w	prescr_medica.ds_observacao_copia%type;
cd_pessoa_fisica_w		prescr_medica.cd_pessoa_fisica%type;
dt_liberacao_enf_w		prescr_medica.dt_liberacao%type;

cursor c01 (ds_observacao_copia_pc		in prescr_medica.ds_observacao_copia%type) is
select		regexp_substr(ds_observacao_copia_pc, '[A-Z]{1,2},[0-9]+;', 1, level) as ds_item_liberar_w
from		dual
connect by	regexp_substr(ds_observacao_copia_pc, '[A-Z]{1,2},[0-9]+;', 1, level) is not null;

cursor c02 (ds_item_liberar_pc		in prescr_medica.ds_observacao_copia%type) is
select	a.nr_prescricao
from	prescr_medica a
where	a.nr_prescricao > nr_prescricao_p
and		a.nr_atendimento = nr_atendimento_p
and		a.cd_funcao_origem = 2314
and		a.dt_liberacao is null
and		a.ds_observacao_copia like '%' || ds_item_liberar_pc || '%';

cursor c03 (ds_item_liberar_pc		in prescr_medica.ds_observacao_copia%type) is
select	a.nr_prescricao
from	prescr_medica a
where	a.nr_prescricao > nr_prescricao_p
and		a.cd_pessoa_fisica = cd_pessoa_fisica_w
and		a.cd_funcao_origem = 2314
and		a.dt_liberacao is null
and		a.ds_observacao_copia like '%' || ds_item_liberar_pc || '%';

begin

	select	max(a.ds_observacao_copia),
			max(cd_pessoa_fisica),
			max(dt_liberacao)
	into	ds_observacao_copia_w,
			cd_pessoa_fisica_w,
			dt_liberacao_enf_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p
	and		a.cd_funcao_origem = 2314;

	if (dt_liberacao_enf_w is null) then

		liberar_prescricao(nr_prescricao_p => nr_prescricao_p,
							nr_atendimento_p => nr_atendimento_p,
							ie_tipo_pessoa_p => ie_tipo_pessoa_p,
							cd_perfil_p => cd_perfil_p,
							nm_usuario_p => nm_usuario_p,
							ie_prescricao_p => ie_prescricao_p,
							ds_erro_p => ds_erro_w);

		if (ds_erro_w is not null) then
			ds_erro_p := ds_erro_w;
			nr_prescricao_erro_p := nr_prescricao_p;
		end if;
	end if;
	
	if (ds_observacao_copia_w is null or
		ds_erro_w is not null) then

		ds_log_w := substr(obter_desc_expressao(942996) || ' / liberar_prescricao_enf_html5 = ' || chr(13)
							|| ' 01 - linha'
							|| ' nr_prescricao_p: '			|| nr_prescricao_p
							|| ' nr_atendimento_p: '		|| nr_atendimento_p 
							|| ' ds_observacao_copia_w: '	|| ds_observacao_copia_w
							|| ' ds_erro_w: '				|| ds_erro_w, 1, 4000);

		gerar_log_prescricao(	nr_prescricao_p => nr_prescricao_p,
								nr_seq_item_p => null,
								ie_agrupador_p => null,
								nr_seq_horario_p => null,
								ie_tipo_item_p => null,
								ds_log_p => ds_log_w,
								nm_usuario_p => nm_usuario_p,
								nr_seq_objeto_p => 90457,
								ie_commit_p => 'N');
	end if;

	if (ds_observacao_copia_w is not null and
		ds_erro_w is null) then
		for r_c01_w in c01 (ds_observacao_copia_w)
		loop
			if (r_c01_w.ds_item_liberar_w is not null) then

				ds_log_w := substr(obter_desc_expressao(942996) || ' / liberar_prescricao_enf_html5 = ' || chr(13)
									|| ' 02 - linha'
									|| ' nr_prescricao_p: '		|| nr_prescricao_p
									|| ' nr_atendimento_p: '	|| nr_atendimento_p 
									|| ' ds_item_liberar_w: '	|| r_c01_w.ds_item_liberar_w
									|| ' cd_pessoa_fisica_w: '	|| cd_pessoa_fisica_w
									|| ' cd_perfil_p: '			|| cd_perfil_p, 1, 4000);

				gerar_log_prescricao(	nr_prescricao_p => nr_prescricao_p,
										nr_seq_item_p => null,
										ie_agrupador_p => null,
										nr_seq_horario_p => null,
										ie_tipo_item_p => null,
										ds_log_p => ds_log_w,
										nm_usuario_p => nm_usuario_p,
										nr_seq_objeto_p => 90457,
										ie_commit_p => 'N');
				
				if (nr_atendimento_p is not null) then
					for r_c02_w in c02 (r_c01_w.ds_item_liberar_w)
					loop
						liberar_prescricao(	nr_prescricao_p => r_c02_w.nr_prescricao,
											nr_atendimento_p => nr_atendimento_p,
											ie_tipo_pessoa_p => ie_tipo_pessoa_p,
											cd_perfil_p => cd_perfil_p,
											nm_usuario_p => nm_usuario_p,
											ie_prescricao_p => ie_prescricao_p,
											ds_erro_p => ds_erro_w);

						if (ds_erro_w is not null) then
							ds_erro_p := ds_erro_w;
							nr_prescricao_erro_p := r_c02_w.nr_prescricao;
						end if;
					end loop;
				else
					for r_c03_w in c03 (r_c01_w.ds_item_liberar_w)
					loop
						liberar_prescricao(	nr_prescricao_p => r_c03_w.nr_prescricao,
											nr_atendimento_p => nr_atendimento_p,
											ie_tipo_pessoa_p => ie_tipo_pessoa_p,
											cd_perfil_p => cd_perfil_p,
											nm_usuario_p => nm_usuario_p,
											ie_prescricao_p => ie_prescricao_p,
											ds_erro_p => ds_erro_w);

						if (ds_erro_w is not null) then
							ds_erro_p := ds_erro_w;
							nr_prescricao_erro_p := r_c03_w.nr_prescricao;
						end if;
					end loop;
				end if;
			end if;
		end loop;
	end if;

end liberar_prescricao_enf_html5;
/
