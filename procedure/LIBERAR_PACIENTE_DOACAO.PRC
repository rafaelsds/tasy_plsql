create or replace
procedure liberar_paciente_doacao(nr_seq_doacao_p 	number) is 

nr_seq_status_doacao_w	number(10,0);

begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_status_doacao_w
from    san_status_doacao
where   ie_status_doacao = 3
and     nvl(ie_situacao,'A') = 'A';

if	(nr_seq_status_doacao_w > 0) then
	
	update	san_doacao
	set	dt_atualizacao = sysdate,
	    dt_triagem_clinica = sysdate,
		nr_Seq_status = nr_seq_status_doacao_w
	where	nr_Sequencia = nr_Seq_Doacao_p;
	
end if;

commit;

end liberar_paciente_doacao;
/