create or replace
procedure transferir_conta_auditoria(
			nr_sequencia_p		number,
			ie_tipo_item_p		number,
			nm_usuario_p		Varchar2) is

cd_convenio_parametro_w		number(8);
ie_guia_transf_conta_w		varchar2(10);
ie_guia_transf_conta_ww		varchar2(1);
cd_estabelecimento_w		number(10);
nr_doc_convenio_w		varchar2(20);
nr_interno_conta_w		number(15);

			

begin

if	(ie_tipo_item_p = 1) then

	select	max(a.cd_convenio_parametro),
		max(a.cd_estabelecimento)
	into	cd_convenio_parametro_w,
		cd_estabelecimento_w
	from	conta_paciente a,
		procedimento_paciente b
	where	a.nr_interno_conta 	= b.nr_interno_conta
	and	b.nr_sequencia		= nr_sequencia_p;
	
	select	nvl(max(ie_guia_transf_conta),'N')
	into	ie_guia_transf_conta_w
	from	convenio_estabelecimento
	where	cd_convenio		= cd_convenio_parametro_w
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
	select	nvl(max(ie_guia_transf_conta),'R')
	into	ie_guia_transf_conta_ww
	from	parametro_faturamento
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(ie_guia_transf_conta_ww = 'R') then
		
		update	procedimento_paciente
		set	nr_doc_convenio	= decode(ie_guia_transf_conta_w,'S',null,nr_doc_convenio)
		where	nr_sequencia = nr_sequencia_p;
	
	elsif	(ie_guia_transf_conta_ww = 'T') then
		
		update	procedimento_paciente
		set	nr_doc_convenio	= null
		where	nr_sequencia = nr_sequencia_p;
	end if;
	
	if	(((ie_guia_transf_conta_w	= 'S') and (ie_guia_transf_conta_ww = 'R')) or
		(ie_guia_transf_conta_ww = 'T')) then
		atualiza_preco_procedimento(nr_sequencia_p,cd_convenio_parametro_w,nm_usuario_p);
	end if;
	
else

	select	max(a.cd_convenio_parametro),
		max(a.cd_estabelecimento)
	into	cd_convenio_parametro_w,
		cd_estabelecimento_w
	from	conta_paciente a,
		material_atend_paciente b
	where	a.nr_interno_conta 	= b.nr_interno_conta
	and	b.nr_sequencia		= nr_sequencia_p;
	
	select	nvl(max(ie_guia_transf_conta),'N')
	into	ie_guia_transf_conta_w
	from	convenio_estabelecimento
	where	cd_convenio		= cd_convenio_parametro_w
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
	select	nvl(max(ie_guia_transf_conta),'R')
	into	ie_guia_transf_conta_ww
	from	parametro_faturamento
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(ie_guia_transf_conta_ww = 'R') then
		
		update	material_atend_paciente
		set	nr_doc_convenio	= decode(ie_guia_transf_conta_w,'S',null,nr_doc_convenio)
		where	nr_sequencia	= nr_sequencia_p;
	
	elsif	(ie_guia_transf_conta_ww = 'T') then
	
		update	material_atend_paciente
		set	nr_doc_convenio	= null
		where	nr_sequencia	= nr_sequencia_p;
	end if;
	
	select	nr_doc_convenio,
		nr_interno_conta
	into	nr_doc_convenio_w,
		nr_interno_conta_w
	from	material_atend_paciente
	where	nr_sequencia	= nr_sequencia_p;
	
	if	(((ie_guia_transf_conta_w	= 'S') and (ie_guia_transf_conta_ww = 'R')) or
		(ie_guia_transf_conta_ww = 'T')) then
		atualiza_preco_material(nr_sequencia_p,nm_usuario_p);
	end if;
	
	select	nr_doc_convenio,
		nr_interno_conta
	into	nr_doc_convenio_w,
		nr_interno_conta_w
	from	material_atend_paciente
	where	nr_sequencia	= nr_sequencia_p;

end if;

commit;

end transferir_conta_auditoria;
/

