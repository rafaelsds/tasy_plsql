create or replace
procedure rop_liberar_hitorico_roupa(	nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

begin

update	rop_roupa_hist
set	dt_liberacao	= sysdate,
	nm_usuario_lib	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;


commit;

end rop_liberar_hitorico_roupa;
/