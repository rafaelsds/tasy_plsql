create or replace
procedure lote_ent_atua_local_armazena(
					nr_seq_lote_p			number,
					nr_seq_ficha_p			number,
					nr_seq_local_p 			number,
					nr_seq_local_caixa_p	number,
					nm_usuario_p			Varchar2) is 

nr_seq_ficha_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	lote_ent_sec_ficha
	where	nr_seq_lote_sec = nr_seq_lote_p
	order by 	1;
					
begin

if 	(nr_seq_lote_p is not null) then
	
	if	(nvl(nr_seq_ficha_p,0) > 0) then

		update	lote_ent_sec_ficha
		set		nr_seq_local = nr_seq_local_p,
				nr_seq_local_caixa = nr_seq_local_caixa_p
		where	nr_sequencia = nr_seq_ficha_p
		and		nr_seq_lote_sec = nr_seq_lote_p;

		insert into lote_ent_sec_ficha_hist (
			NR_SEQUENCIA,
			DT_ATUALIZACAO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO,
			NM_USUARIO_NREC,
			NR_SEQ_FICHA,
			NR_SEQ_LOCAL,
			NR_SEQ_LOCAL_CAIXA,
			NR_SEQ_LOTE_SEC           
		) values (
			lote_ent_sec_ficha_hist_seq.nextval,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			nr_seq_ficha_p,
			nr_seq_local_p,
			nr_seq_local_caixa_p,
			nr_seq_lote_p
		);
		
	end if;

	if (nvl(nr_seq_ficha_p,0) = 0) then
	
		open C01;
		loop
		fetch C01 into	
			nr_seq_ficha_w;
		exit when C01%notfound;
			begin
			
			update	lote_ent_sec_ficha
			set		nr_seq_local = nr_seq_local_p,
					nr_seq_local_caixa = nr_seq_local_caixa_p
			where	nr_sequencia = nr_seq_ficha_w
			and		nr_seq_lote_sec = nr_seq_lote_p;
			
			
			insert into lote_ent_sec_ficha_hist (
				NR_SEQUENCIA,
				DT_ATUALIZACAO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO,
				NM_USUARIO_NREC,
				NR_SEQ_FICHA,
				NR_SEQ_LOCAL,
				NR_SEQ_LOCAL_CAIXA,
				NR_SEQ_LOTE_SEC           
			) values (
				lote_ent_sec_ficha_hist_seq.nextval,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p,
				nr_seq_ficha_w,
				nr_seq_local_p,
				nr_seq_local_caixa_p,
				nr_seq_lote_p
			);
		
			end;
		end loop;
		close C01;
	
	end if;
	
end if;

commit;

end lote_ent_atua_local_armazena;
/