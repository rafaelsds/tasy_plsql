create or replace
procedure vincular_agendas_atend(ds_lista_agenda_p		 varchar2,
			nr_atendimento_p		 number,
			cd_pessoa_atendimento_p		 varchar2,					
			nm_usuario_p			 varchar2,
			cd_estabelecimento_p		 number) is
		
ds_lista_agenda_w	varchar2(255);	
tam_lista_w		varchar2(255);	
ie_pos_virgula_w	varchar2(255);
nr_sequencia_w		number(15);
nr_seq_agenda_w		varchar2(255);
nr_seq_agenda_lis_w	varchar2(255);


begin

ds_lista_agenda_w := substr(ds_lista_agenda_p,1,255);

while	(ds_lista_agenda_w is not null) and
	(trim(ds_lista_agenda_w) <> ',') loop
	tam_lista_w		:= length(ds_lista_agenda_w);
	ie_pos_virgula_w	:= instr(ds_lista_agenda_w,',');
	if	(ie_pos_virgula_w <> 0) then
		nr_seq_agenda_w		:= substr(ds_lista_agenda_w,1,(ie_pos_virgula_w - 1));
		ds_lista_agenda_w	:= trim(substr(ds_lista_agenda_w,(ie_pos_virgula_w + 1), tam_lista_w));
		vinc_atendimento_agenda_EUP(nr_seq_agenda_w,nr_atendimento_p,cd_pessoa_atendimento_p,nm_usuario_p,cd_estabelecimento_p);
	end if;
end loop;



commit;

end vincular_agendas_atend;
/