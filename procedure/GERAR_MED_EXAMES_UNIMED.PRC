create or replace
procedure Gerar_Med_Exames_Unimed(  	nr_seq_cliente_p 		Number,
					nr_seq_protocolo_p	 	Number,
					nm_usuario_p		Varchar2,
					nr_sequencia_p out		Number ) is
					


nr_sequencia_w		Number(10);
nr_seq_protocolo_w	Number(10);
nr_seq_exame_w		Number(10);	
nr_seq_apresent_w		Number(10);
ds_dados_clinicos_w	Varchar2(255);
ds_exame_anterior_w	Varchar2(255);
ds_justificativa_w 	Varchar2(255);
					
Cursor C01 is
	select	nr_seq_protocolo,
		nr_seq_exame,
		nr_seq_apresent
	from	med_exame_protocolo
	where	nr_seq_protocolo  = nr_seq_protocolo_p
	and	nr_seq_exame is not null;
begin

select 	ds_dados_clinicos,
	ds_exame_anterior,
	ds_justificativa
into	
	ds_dados_clinicos_w,
	ds_exame_anterior_w,
	ds_justificativa_w
from	med_protocolo_exame
where	nr_sequencia 	=	nr_seq_protocolo_p;		
	
select 	med_pedido_exame_seq.nextVal 
into	nr_sequencia_w 
from 	dual;	
	
insert into med_pedido_exame( nr_sequencia,
			 dt_solicitacao,
			 ds_solicitacao,
			 ds_exame_ant,
			 ds_dados_clinicos,
			 ds_justificativa,
			 ie_ficha_unimed,
			 nr_seq_cliente,
			 dt_atualizacao,
			 nm_usuario )
		values ( nr_sequencia_w,
			 sysdate,
			 ' ',
			 ds_exame_anterior_w,
			 ds_dados_clinicos_w,
			 ds_justificativa_w,
			 'S',
			 nr_seq_cliente_p,
			 sysdate,
			 nm_usuario_p );
			 

open C01;
loop
fetch C01 into	
	nr_seq_protocolo_w,
	nr_seq_exame_w,
	nr_seq_apresent_w;
exit when C01%notfound;
	begin
		insert into med_ped_exame_cod(	nr_sequencia,
						nr_seq_pedido,
						nr_seq_exame,
						qt_exame,
						dt_atualizacao,
						nm_usuario )
					values( med_ped_exame_cod_seq.nextVal,
						med_pedido_exame_seq.currval,
						nr_seq_exame_w,
						1,
						sysdate,
						nm_usuario_p
					       );
	end;
end loop;
close C01;

commit;

nr_sequencia_p := nr_sequencia_w;

end Gerar_Med_Exames_Unimed;
/