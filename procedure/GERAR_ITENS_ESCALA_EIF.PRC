create or replace
procedure gerar_itens_escala_eif(	nr_seq_escala_p	number,
				nr_sequencia_p	number,
				nm_usuario_p	varchar2,
				ie_commitar_p	varchar2 default 'S') is 

nr_seq_sessao_w	ESCALA_EIF_ITEM.nr_seq_sessao%type := 0;

Cursor C01 is
	select	nr_sequencia,
		nr_seq_escala,
		ds_item,
		nr_seq_apres,
		nvl(ie_titulo,'N') ie_titulo
	from	eif_escala_item
	where	nr_seq_escala = nr_seq_escala_p
	and	nvl(ie_situacao,'A') = 'A'
	order by	nr_seq_apres;

begin

for itens in c01 loop

	begin

	if (itens.ie_titulo = 'S') then
		nr_seq_sessao_w := nr_seq_sessao_w + 1;
	end if;
	
	insert into ESCALA_EIF_ITEM(
		nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		nr_seq_item, 
		ie_resultado,
		nr_seq_escala,
		ie_habilitado,
		ie_titulo,
		nr_seq_sessao)
	values (
		escala_eif_item_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		itens.nr_sequencia,
		'N',
		nr_sequencia_p,
		'S',
		itens.ie_titulo,
		nr_seq_sessao_w);
	end;

end loop;

if (ie_commitar_p = 'S') then
   commit;
end if;

end gerar_itens_escala_eif;
/