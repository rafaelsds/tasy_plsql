create or replace
procedure desenv_liberar_avaliacao(
			nr_sequencia_p		number,
			nm_usuario_p		varchar2) is
begin
if	(nr_sequencia_p is not null) then
	begin
	update 	man_ordem_serv_avaliacao 
	set 	nm_usuario = nm_usuario_p, 
			dt_liberacao = sysdate 
	where 	nr_sequencia = nr_sequencia_p;
	end;
	commit;
end if;
end desenv_liberar_avaliacao;
/		