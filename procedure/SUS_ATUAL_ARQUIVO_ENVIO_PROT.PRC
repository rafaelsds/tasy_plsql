create or replace
procedure sus_atual_arquivo_envio_prot(	nr_seq_protocolo_p	protocolo_convenio.nr_seq_protocolo%type,
					ds_arquivo_envio_p	protocolo_convenio.ds_arquivo_envio%type,
					ds_diretorio_p		protocolo_convenio.ds_arquivo_envio%type,
					nm_usuario_p		usuario.nm_usuario%type) is

		
begin

if (nr_seq_protocolo_p is not null) then
	begin
	if	(ds_diretorio_p = '')  then
		begin
		update	protocolo_convenio
		set	dt_envio = sysdate,
			nm_usuario = nm_usuario_p,
			ds_arquivo_envio = ds_arquivo_envio_p
		where	nr_seq_protocolo = nr_seq_protocolo_p;
		end;
	else
		update	protocolo_convenio
		set	dt_envio = sysdate,
			nm_usuario = nm_usuario_p,
			ds_arquivo_envio = ds_diretorio_p
		where	nr_seq_protocolo = nr_seq_protocolo_p;
	end if;
	
	commit;
	
	end;
end if;

end sus_atual_arquivo_envio_prot;
/