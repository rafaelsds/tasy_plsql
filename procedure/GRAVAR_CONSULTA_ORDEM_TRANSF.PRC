create or replace
procedure gravar_consulta_ordem_transf(
			cd_estabelecimento_p       		number,
			cd_grupo_material_p		number,
			cd_subgrupo_material_p		number,
			cd_classe_material_p		number,
           			cd_local_estoque_p     		number,
	    		cd_local_atende_p        		number,
			cd_estab_atende_p			number,
            			nm_usuario_p            		varchar2,
			ie_prescr_cirurgia_p			varchar2,
			ie_lancado_cirurgia_p		varchar2,
			ie_agenda_cirurgica_p		varchar2,
			ie_material_comercial_p		varchar2,
			ie_saldo_maior_minimo_p		varchar2,
			ie_multiplo_minimo_p		varchar2,
			ie_considera_req_pend_p		varchar2,	
			ie_curva_p			varchar2,
			ds_familia_p			varchar2,
			ds_localizacao_p			varchar2,
			ie_receita_p			varchar2,
			ie_somente_zerados_p		varchar2,
			ie_considera_emprestimo_p		varchar2,
			cd_unidade_medida_p		varchar2,
			ie_req_pend_lib_p			varchar2 default 'N') is

dt_atualizacao_w            		date         := sysdate;
dt_mesano_referencia_w      		date;
cd_material_w               		number(6)    := 0;
nr_ordem_transf_w             		number(10,0) := 0;
nr_sequencia_w              		number(5)    := 0;
qt_estoque_minimo_w         		number(15,4) := 0;
qt_estoque_maximo_w         		number(15,4) := 0;
qt_estoque_w                		number(15,4) := 0;
qt_estoque_req_w            		number(15,4) := 0;
qt_multiplo_w			number(15,4) := 0;
qt_minimo_multiplo_solic_w  		number(15,4) := 0;
qt_requisitada_w            		number(15,4) := 0;
qt_requisicao_pendente_w            	number(15,4) := 0;
qt_prescr_cirurgia_w           		number(18,6) := 0;
qt_lancado_cirurgia_w		number(15,4) := 0;
qt_agenda_cirurgia_w		number(18,6) := 0;
qt_conv_estoque_consumo_w   	number(13,4) := 0;
cd_unidade_medida_consumo_w 	varchar2(30)  := '';
cd_unidade_medida_estoque_w 	varchar2(30)  := '';
cd_unidade_medida_solic_w		varchar2(30)  := '';
ie_situacao_op_w			varchar2(1);
nr_seq_req_w			number(10);
	
ie_gerar_familia_w			varchar2(01) := 'S';
nr_seq_familia_w			number(10);
qt_diferente_w			number(3);
ds_familia_w			varchar2(500);
ie_gerar_local_w			varchar2(01) := 'S';
nr_seq_localizacao_w		number(10);
qt_dif_local_w			number(3);
ds_localizacao_w			varchar2(500);

qt_estoque_req_acum_w		number(15,4);
cd_centro_custo_w			number(10,0);
qt_emprestimo_w			number(13,4) := 0;
ie_somente_liberado_w		varchar2(1);

cursor c01 is
select	a.cd_material,
	nvl(a.qt_estoque_minimo,0),
	nvl(a.qt_estoque_maximo,0),
	c.qt_minimo_multiplo_solic,
	nvl(c.qt_conv_estoque_consumo,1),
	c.cd_unidade_medida_solic,
	substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo,
	substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
	c.nr_seq_familia,
	c.nr_seq_localizacao
from	estrutura_material_v e,
	material c,
	regra_ressup_estab a
where	(a.cd_material		= c.cd_material)
and	(a.cd_material		= e.cd_material)
and	(e.cd_grupo_material	= cd_grupo_material_p or nvl(cd_grupo_material_p, 0) = 0)
and	(e.cd_subgrupo_material	= cd_subgrupo_material_p or nvl(cd_subgrupo_material_p, 0) = 0)
and	(e.cd_classe_material	= cd_classe_material_p or nvl(cd_classe_material_p, 0) = 0)
and	(substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_p,'UMS'),1,30) = cd_unidade_medida_p or nvl(cd_unidade_medida_p, '0') = '0')
and	(ie_somente_liberado_w = 'N' or substr(nvl(sup_obter_local_valido(cd_estabelecimento_p,cd_local_estoque_p,a.cd_material,'2'),'S'),1,1) = 'S')
and	(a.cd_estabelecimento	= cd_estabelecimento_p)
and	(a.cd_local_estoque	= cd_local_estoque_p)
and	(a.cd_local_atende	= cd_local_atende_p)
and	(a.cd_estab_atende 	= cd_estab_atende_p)
and	(c.ie_consignado in ('0','2'))
and	(c.ie_situacao		= 'A')
and	(a.ie_situacao 		= 'A')
and	(c.ie_receita		= ie_receita_p or ie_receita_p = 'A')
and	((nvl(ie_curva_p, 'X') = 'X') or (ie_curva_p = obter_curva_abc_estab(a.cd_estabelecimento, a.cd_material, 'N', pkg_date_utils.start_of(sysdate,'MONTH', 0))))
order by c.ds_material;

begin
delete from w_requisicao_material
where nm_usuario = nm_usuario_p;
commit;	

qt_diferente_w	:= instr(upper(ds_familia_p),'NOT');
ds_familia_w	:= substr(ds_familia_p,instr(ds_familia_p,'('),500);

qt_dif_local_w	:= instr(upper(ds_localizacao_p),'NOT');
ds_localizacao_w	:= substr(ds_localizacao_p,instr(ds_localizacao_p,'('),500);
ie_somente_liberado_w	:= substr(nvl(obter_valor_param_usuario(146, 7, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1);

open c01;
loop
fetch	c01 into
	cd_material_w,
	qt_estoque_minimo_w,
	qt_estoque_maximo_w,
	qt_minimo_multiplo_solic_w,
	qt_conv_estoque_consumo_w,
	cd_unidade_medida_solic_w,
	cd_unidade_medida_consumo_w,
	cd_unidade_medida_estoque_w,
	nr_seq_familia_w,
	nr_seq_localizacao_w;
exit when c01%notfound;	
	begin
	qt_requisicao_pendente_w := 0;
	
	if	(ds_familia_w is not null) then
		if	(qt_diferente_w	= 0) then
			ie_gerar_familia_w	:= substr(obter_se_contido(nr_seq_familia_w, ds_familia_w),1,1);
		else	/* Tratamento NOT IN*/
			select	decode(substr(obter_se_contido(nr_seq_familia_w, ds_familia_w),1,1),'N','S','S','N')
			into	ie_gerar_familia_w
			from	dual;
		end if;
	end if;
	
	if	(ds_localizacao_w is not null) then
		if	(qt_dif_local_w	= 0) then
			ie_gerar_local_w	:= substr(obter_se_contido(nr_seq_localizacao_w, ds_localizacao_w),1,1);
		else	/* Tratamento NOT IN*/
			select	decode(substr(obter_se_contido(nr_seq_localizacao_w, ds_localizacao_w),1,1),'N','S','S','N')
			into	ie_gerar_local_w
			from	dual;
		end if;
	end if;
		
	obter_saldo_estoque(cd_estabelecimento_p, cd_material_w, cd_local_estoque_p, null, qt_estoque_w);
	qt_prescr_cirurgia_w	:= 0;
	qt_lancado_cirurgia_w	:= 0;

	if	(ie_prescr_cirurgia_p = 'S') then
		begin
		select /*+ index(a presmat_i1) */
			nvl(sum(qt_total_dispensar),0)
		into 	qt_prescr_cirurgia_w
		from 	setor_atendimento c,
			cirurgia b,
			prescr_material a,
			prescr_medica m
		where	a.cd_motivo_baixa	= 0
		and	a.cd_material		= cd_material_w
		and	m.nr_prescricao		= b.nr_prescricao
		and	m.nr_prescricao		= a.nr_prescricao
		and	b.cd_setor_atendimento	= c.cd_setor_atendimento
		and	c.cd_local_estoque	= cd_local_estoque_p
		and	a.ie_status_cirurgia	in ('CB','AD');

		qt_prescr_cirurgia_w		:= dividir(qt_prescr_cirurgia_w, qt_conv_estoque_consumo_w);
		end;
	end if;
		
	/*para identificar a quantidade que ja existe em requisi��es pedentes de transferencia*/
	if	(ie_considera_req_pend_p = 'S') then
		begin			
		qt_requisicao_pendente_w := obter_qt_pendente_transf_etq(null,cd_material_w,cd_local_estoque_p,ie_req_pend_lib_p);
		end;
	end if;
		
	qt_emprestimo_w := 0;
	/*para identificar a quantidade de empr�stimo*/
	if (ie_considera_emprestimo_p = 'S') then
		select	/*+ index(b empmate_i1) index (c emprest_pk) */ 
			nvl(sum(decode(c.ie_tipo,'S', qt_material * -1, qt_material)),0)
		into	qt_emprestimo_w
		from	emprestimo c,
			emprestimo_material b			
		where	c.cd_local_estoque		= cd_local_estoque_p
		and	c.dt_liberacao		is not null
		and	b.nr_emprestimo		= c.nr_emprestimo
		and	b.qt_material		> 0
		and	exists (
			select 1 from material a
			where	a.cd_material_estoque	= cd_material_w
		  	and	a.cd_material 		= b.cd_material);
	end if;		

	/*para identificar as quantidades que estejam em cirurgia, por�m ainda n�o consistidas por barras, ou seja: for�m somente lan�ados*/
	if	(ie_lancado_cirurgia_p = 'S') then
		begin
		select /*+ index(a presmat_i1) */
			nvl(sum(qt_total_dispensar),0)
		into 	qt_lancado_cirurgia_w
		from 	setor_atendimento c,
			cirurgia b,
			prescr_material a,
			prescr_medica m
		where	a.cd_motivo_baixa		= 0
		and	a.cd_material		= cd_material_w
		and	m.nr_prescricao		= b.nr_prescricao
		and	m.nr_prescricao		= a.nr_prescricao
		and	b.cd_setor_atendimento	= c.cd_setor_atendimento
		and	c.cd_local_estoque		= cd_local_estoque_p
		and	a.ie_status_cirurgia	in ('GI');

		qt_lancado_cirurgia_w		:= dividir(qt_lancado_cirurgia_w , qt_conv_estoque_consumo_w);
		end;
	end if;

	/*para identificar as quantidades lan�adas e consistidas na gest�o da agenda cirurgica*/
	if	(ie_agenda_cirurgica_p = 'S') then
		select	nvl(sum(qt_total_dispensar),0)
		into	qt_agenda_cirurgia_w
		from	setor_atendimento c,
			material x,
			prescr_medica b,
			prescr_material a
		where	a.cd_motivo_baixa		= 0
		and	a.cd_material		= x.cd_material
		and	x.cd_material_estoque	= cd_material_w
		and	a.nr_prescricao		= b.nr_prescricao
		and	b.cd_setor_atendimento	= c.cd_setor_atendimento
		and	c.cd_local_estoque		= cd_local_estoque_p
		and	a.ie_status_cirurgia		= 'CB'
		and	b.nr_seq_agenda		IS NOT NULL
		and	b.nr_cirurgia		IS NULL
		and not exists(
			select	1
			from 	cirurgia c
			where	c.nr_prescricao = a.nr_prescricao);
			
		qt_agenda_cirurgia_w		:= dividir(qt_agenda_cirurgia_w , qt_conv_estoque_consumo_w);		
	end if;
	
	qt_estoque_w	:= qt_estoque_w - (qt_prescr_cirurgia_w + qt_lancado_cirurgia_w + qt_agenda_cirurgia_w - qt_requisicao_pendente_w - qt_emprestimo_w);
		
	if	(qt_estoque_w < qt_estoque_minimo_w) or
		((qt_estoque_w >= qt_estoque_minimo_w) and (qt_estoque_w < qt_estoque_maximo_w) and (ie_saldo_maior_minimo_p = 'S')) then
      	begin
			
		qt_estoque_req_w	 	:= round(qt_estoque_maximo_w - qt_estoque_w,0);
		if	(qt_estoque_req_w	= 0) then
			qt_estoque_req_w	:= 1;
		end if;
			
		if (ie_somente_zerados_p = 'S') then
			begin
			if (qt_estoque_w = 0) then
				qt_estoque_req_w	:=	qt_estoque_maximo_w;
			else
				qt_estoque_req_w	:= 0;
			end if;
			end;
		end if;

		/*para gerar a quantidade pelo multiplo do minimo do cadastro de materiais*/
		if	(ie_multiplo_minimo_p = 'S') then
			begin
			if	(cd_unidade_medida_consumo_w <> cd_unidade_medida_estoque_w) then
				qt_minimo_multiplo_solic_w	:= dividir(qt_minimo_multiplo_solic_w, qt_conv_estoque_consumo_w);
			end if;
			/*decode((trunc(dividir(:qt, 5)) * 5), :qt, :qt, ((trunc(dividir(:qt, 5)) * 5)) + 5) qt_comprar*/
			select	((trunc(dividir(qt_estoque_req_w, qt_minimo_multiplo_solic_w)) * qt_minimo_multiplo_solic_w))
			into	qt_multiplo_w
			from	dual;

			if	(qt_multiplo_w = qt_estoque_req_w) then
				qt_estoque_req_w	:= qt_multiplo_w;
			else
				qt_estoque_req_w	:= qt_multiplo_w + qt_minimo_multiplo_solic_w;
			end if;
			end;
		end if;

		qt_requisitada_w	:= qt_estoque_req_w * qt_conv_estoque_consumo_w;
		
		if	(qt_estoque_req_w > 0) and
			((ie_gerar_familia_w = 'S') and (ie_gerar_local_w = 'S')) then
			begin
			if	(ie_material_comercial_p = 'S') then
				cd_material_w	:= obter_mat_comercial(cd_material_w,'C');
			end if;

  			nr_sequencia_w   	:= (nr_sequencia_w + 1);					
			select	nvl(max(nr_sequencia),0) +1
			into	nr_seq_req_w
			from	w_requisicao_material
			where	nm_usuario = nm_usuario_p;
							
			insert into w_requisicao_material(
				nr_sequencia,
				cd_estabelecimento,
				cd_material,
				qt_material_requisitada,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida,
				ie_acao,
				cd_motivo_baixa,
				qt_estoque,
				cd_unidade_medida_estoque)
			values(	nr_seq_req_w,
				cd_estabelecimento_p,
				cd_material_w,
				qt_requisitada_w,
				dt_atualizacao_w,
				nm_usuario_p,
				cd_unidade_medida_consumo_w,
				'1',
				0,
				qt_estoque_req_w,
				cd_unidade_medida_estoque_w);
			end;
		end if;
		end;
	end if;
	end;
end loop;
close c01;

commit;
end gravar_consulta_ordem_transf;
/
