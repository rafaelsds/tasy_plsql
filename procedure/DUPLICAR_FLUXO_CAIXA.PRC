create or replace
procedure DUPLICAR_FLUXO_CAIXA
			(CD_ESTABELECIMENTO_p	in	number,
			DT_REFERENCIA_p		in	date,
			CD_CONTA_FINANC_p	in	number,
			IE_CLASSIF_FLUXO_p	in	varchar2,
			IE_PERIODO_p		in	varchar2,
			dt_inicial_p		in	date,
			dt_final_p		in	date,
			nm_usuario_p		in	varchar2,
			cd_empresa_p		in	number) is

/*--------------------------------------------------------------- ATENCAO ----------------------------------------------------------------*/
/* Cuidado ao realizar alteracoes no fluxo de caixa. Toda e qualquer alteracao realizada em qualquer uma das      						  */
/* procedures do fluxo de caixa deve ser cuidadosamente verificada e realizada no fluxo de caixa em lote.           					  */
/* Devemos garantir que os dois fluxos de caixa tragam os mesmos valores no resultado, evitando assim que           					  */
/* existam diferencas entre os fluxos de caixa. Fluxo de caixa em lote parametro 43 = "L"                                                 */
/*----------------------------------- AO ALTERAR O FLUXO DE CAIXA ALTERAR TAMBEM O FLUXO DE CAIXA EM LOTE --------------------------------*/

dt_referencia_w		date;
cd_moeda_empresa_w	number(5);

begin

dt_referencia_w		:= dt_inicial_p;

/* Projeto Multimoeda - Busca a moeda padrao da empresa para gravar no fluxo. */
select	obter_moeda_padrao_empresa(cd_estabelecimento_p,'E')
into	cd_moeda_empresa_w
from 	dual;

while	pkg_date_utils.start_of(dt_referencia_w, 'MONTH',0) <= pkg_date_utils.start_of(dt_final_p, 'MONTH',0)
loop
	if	(pkg_date_utils.start_of(dt_referencia_w, 'MONTH',0) <> pkg_date_utils.start_of(dt_referencia_p, 'MONTH',0)) then
		begin
		insert into fluxo_caixa
			(CD_ESTABELECIMENTO,
			DT_REFERENCIA,
			CD_CONTA_FINANC,
			IE_CLASSIF_FLUXO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			VL_FLUXO,
			IE_ORIGEM,
			IE_PERIODO,
			IE_INTEGRACAO,
			DS_OBSERVACAO,
			cd_empresa,
			cd_moeda)
		select	CD_ESTABELECIMENTO_p,
			dt_referencia_w,
			CD_CONTA_FINANC_p,
			IE_CLASSIF_FLUXO_p,
			sysdate,
			nm_usuario_p,
			vl_fluxo,
			ie_origem,
			ie_periodo_p,
			ie_integracao,
			DS_OBSERVACAO,
			cd_empresa,
			nvl(cd_moeda,cd_moeda_empresa_w)
		from	fluxo_caixa
		where	nvl(CD_ESTABELECIMENTO, 0)	= nvl(CD_ESTABELECIMENTO_p, nvl(CD_ESTABELECIMENTO, 0))
		and	DT_REFERENCIA		= DT_REFERENCIA_p
		and	CD_CONTA_FINANC	= CD_CONTA_FINANC_p
		and	IE_CLASSIF_FLUXO	= IE_CLASSIF_FLUXO_p
		and	IE_PERIODO		= IE_PERIODO_p
		and	cd_empresa		= cd_empresa_p;
		exception when DUP_VAL_ON_INDEX then
			wheb_mensagem_pck.exibir_mensagem_abort(991947);
		end;

	end if;

	dt_referencia_w		:= pkg_date_utils.add_month(dt_referencia_w, 1,0);

end loop;

commit;

end;
/
