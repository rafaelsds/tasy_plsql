create or replace
procedure ptu_respstatus_serv_v50_ws	(	cd_transacao_p			ptu_resp_pedido_status.cd_transacao%type,
						nr_seq_transacao_p		ptu_pedido_compl_aut.nr_seq_guia%type,
						nr_seq_pedido_p			ptu_resp_pedido_status.nr_sequencia%type,
						ie_tipo_transacao_p		varchar2,
						nr_seq_execucao_p		ptu_resp_pedido_status.nr_seq_execucao%type,
						ie_tipo_tabela_p		ptu_resp_servico_status.ie_tipo_tabela%type,
						cd_servico_p			ptu_resp_servico_status.cd_servico%type,
						ds_servico_p			ptu_resp_servico_status.ds_servico%type,
						qt_autorizado_p			ptu_resp_servico_status.qt_autorizado%type,
						ie_autorizado_p			ptu_resp_servico_status.ie_autorizado%type,
						cd_mens_espec_1_p		ptu_inconsistencia.cd_inconsistencia%type,
						cd_mens_espec_2_p		ptu_inconsistencia.cd_inconsistencia%type,
						cd_mens_espec_3_p		ptu_inconsistencia.cd_inconsistencia%type,
						cd_mens_espec_4_p		ptu_inconsistencia.cd_inconsistencia%type,
						cd_mens_espec_5_p		ptu_inconsistencia.cd_inconsistencia%type,
						ds_mens_espec_p			ptu_intercambio_consist.ds_observacao%type,
						nm_usuario_p			Usuario.nm_usuario%type,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type) is 

begin

ptu_imp_scs_ws_pck.ptu_imp_resp_serv_status_trans (	cd_transacao_p, nr_seq_transacao_p, nr_seq_pedido_p, ie_tipo_transacao_p,
							nr_seq_execucao_p, ie_tipo_tabela_p, cd_servico_p, ds_servico_p,
							qt_autorizado_p, ie_autorizado_p, cd_mens_espec_1_p, cd_mens_espec_2_p,
							cd_mens_espec_3_p, cd_mens_espec_4_p, cd_mens_espec_5_p, ds_mens_espec_p,
							nm_usuario_p, cd_estabelecimento_p );

end ptu_respstatus_serv_v50_ws;
/