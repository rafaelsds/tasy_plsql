create or replace
procedure pls_liberar_todos_grupos
			(	nr_seq_auditoria_p		number,
				nm_usuario_p			varchar2,
				ie_existe_item_pendente_p out	varchar2) is 
				
nr_seq_grupo_auditoria_w	number(10);
nr_seq_grupo_w			number(10);
ie_pertence_grupo_w		varchar2(1);
ie_existe_item_pendente_w	varchar2(1) := 'N';
qt_grupo_w			number(5);
qt_item_pendente_w		number(5);
			
Cursor C01 is
	select	nr_sequencia,
		nr_seq_grupo
	from	pls_auditoria_grupo
	where	nr_seq_auditoria	= nr_seq_auditoria_p
	and	dt_liberacao is null
	order	by nr_seq_ordem;

begin

open C01;
loop
fetch C01 into	
	nr_seq_grupo_auditoria_w,
	nr_seq_grupo_w;
exit when C01%notfound;
	begin
	ie_pertence_grupo_w :=	pls_obter_se_auditor_grupo(nr_seq_grupo_w,nm_usuario_p);

	if	(ie_pertence_grupo_w = 'S') then
		select	count(*)
		into	qt_grupo_w
		from	pls_auditoria_grupo
		where	nr_seq_auditoria	= nr_seq_auditoria_p
		and	dt_liberacao is null;

		select	count(*)
		into	qt_item_pendente_w
		from	pls_auditoria_item
		where	nr_seq_auditoria	= nr_seq_auditoria_p
		and	ie_status = 'P';
		
		if	(qt_grupo_w = 1) and
			(qt_item_pendente_w > 0) then
			ie_existe_item_pendente_w := 'S';
		else
			update	pls_auditoria_grupo
			set	dt_inicio_auditoria = sysdate,
				dt_liberacao	= sysdate,
				ie_status	= 'P',
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate,
				nm_usuario_exec	= nm_usuario_p
			where	nr_sequencia	= nr_seq_grupo_auditoria_w;
		end if;
	else
		goto final;
		nr_seq_grupo_w := 0;
	end if;
	
	end;
end loop;
close C01;

<<final>>

ie_existe_item_pendente_p := ie_existe_item_pendente_w;


end pls_liberar_todos_grupos;
/
