create or replace
procedure duplic_organograma( nr_sequencia_p	number,
			nm_usuario_p		Varchar2) is 

nr_seq_dep_philips_w	number(10);			
nr_seq_grupo_w		number(10);
nr_seq_dep_novo_w	number(10);
nr_seq_gerencia_w	number(10);

Cursor C01 is
	select	nr_sequencia
	from	departamento_philips
	where	NR_GRUPO_DEP_PHILIPS = nr_sequencia_p;
	
Cursor C02 is
	select	nr_sequencia
	from	depto_gerencia_philips
	where	nr_seq_departamento = nr_seq_dep_philips_w;
			
begin
if	(nr_sequencia_p is not null) then
	
	select	grupo_dep_philips_seq.nextVal
	into	nr_seq_grupo_w
	from 	dual;
	
	insert into grupo_dep_philips (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nm_grupo_dep_philips,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	select	nr_seq_grupo_w,
		sysdate,
		nm_usuario_p,
		nm_grupo_dep_philips,
		sysdate,
		nm_usuario_p
	from	grupo_dep_philips
	where	nr_sequencia = nr_sequencia_p;

	
	open C01;
	loop
	fetch C01 into	
		nr_seq_dep_philips_w;
	exit when C01%notfound;
		begin
		
		select	departamento_philips_seq.nextVal
		into	nr_seq_dep_novo_w
		from	dual;
		
		insert into departamento_philips (	
			nr_sequencia,
			cd_pessoa_fisica,
			ie_situacao,
			dt_atualizacao,
			nm_usuario,
			cd_classificacao,
			nm_departamento,
			cd_cnpj,
			nr_seq_superior,
			ie_apoio,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_departamento,
			nr_grupo_dep_philips)
		select	nr_seq_dep_novo_w,
			cd_pessoa_fisica,
			ie_situacao,
			sysdate,
			nm_usuario_p,
			cd_classificacao,
			nm_departamento,
			cd_cnpj,
			nr_seq_superior,
			ie_apoio,
			sysdate,
			nm_usuario_p,
			ds_departamento,
			nr_seq_grupo_w
		from	departamento_philips
		where	nr_sequencia = nr_seq_dep_philips_w;
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_gerencia_w;
		exit when C02%notfound;
			begin
			insert into depto_gerencia_philips (	
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_gerencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_departamento)
			select	depto_gerencia_philips_seq.nextVal,
				sysdate,
				nm_usuario_p,
				nr_seq_gerencia,
				sysdate,
				nm_usuario_p,
				nr_seq_dep_novo_w
			from	depto_gerencia_philips
			where	nr_sequencia = 	nr_seq_gerencia_w;	
								
			end;
		end loop;
		close C02;
		
		end;
	end loop;
	close C01;

end if;

commit;

end duplic_organograma;
/