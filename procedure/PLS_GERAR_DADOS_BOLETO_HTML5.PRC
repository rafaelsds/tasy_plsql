create or replace
procedure pls_gerar_dados_boleto_html5(	nr_seq_lote_p			pls_lote_boleto.nr_sequencia%type,
							cd_estabelecimento_p	number,
							nm_usuario_p			varchar2,
							ds_erro_p	out			varchar2) is 

begin

pls_gerar_boletos_mens_fat_pck.pls_gerar_dados_boleto(nr_seq_lote_p, cd_estabelecimento_p, nm_usuario_p, ds_erro_p);


end pls_gerar_dados_boleto_html5;
/