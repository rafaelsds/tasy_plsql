create or replace
procedure regulacao_atendimento_grava(	nr_atendimento_p	number,
					nr_seq_laudo_p		number,
					cd_cidadao_p		varchar2,
					nr_regulacao_p		number,
                                        cd_cnes_solicitante_p   number,
					cd_cnes_executante_p    number,
					nm_pessoa_fisica_p	varchar2,
					nm_mae_p		varchar2,
					dt_nascimento_p		date, 
					nr_cartao_nac_sus_p     varchar2 default null, 
                                        nr_cpf_p                varchar2 default null, 
                                        ie_sexo_p               varchar2 default null, 
					ds_endereco_p           varchar2 default null								
					) is

cd_pessoa_fisica_w	varchar2(10);
qt_registro_reg_w	number(3);
qt_registro_pf_w	number(3);
qt_registro_usuario_w	number(3);
nm_usuario_w		varchar2(15);
nr_sequencia_w		compl_pessoa_fisica.nr_sequencia%type;

begin
if	(nvl(nr_regulacao_p,0) > 0) then

	nm_usuario_w := 'tasy'; --obter_usuario_ativo;

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;

	if	(cd_pessoa_fisica_w is null) then

		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	pessoa_fisica
		where	cd_pessoa_cross = cd_cidadao_p;

		if	(cd_pessoa_fisica_w is null) then
			select	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	pessoa_fisica
			where	trunc(dt_nascimento) = trunc(dt_nascimento_p)
			and	regulacao_obter_nome_pesquisa(nm_pessoa_fisica) = regulacao_obter_nome_pesquisa(nm_pessoa_fisica_p)
			and	regulacao_obter_nome_pesquisa(obter_nome_mae_pf(cd_pessoa_fisica)) = regulacao_obter_nome_pesquisa(nm_mae_p);
		end if;

		if	(cd_pessoa_fisica_w is null) then
			select	pessoa_fisica_seq.nextval
			into	cd_pessoa_fisica_w
			from	dual;

			insert into pessoa_fisica (
				cd_pessoa_fisica,
				ie_tipo_pessoa,
				nm_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				dt_nascimento, 
				nr_cartao_nac_sus,
                                nr_cpf,
                                ie_sexo
			) values (
				cd_pessoa_fisica_w,
				2,
				nm_pessoa_fisica_p,
				sysdate,
				nm_usuario_w,
				dt_nascimento_p, 
				nr_cartao_nac_sus_p,
                                nr_cpf_p,
                                ie_sexo_p
				);

			-- insere o complemento m�e
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica = cd_pessoa_fisica_w;

			insert into compl_pessoa_fisica (
				cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				nm_usuario,
				dt_atualizacao,
				nm_contato, 
				ds_endereco
			) values (
				cd_pessoa_fisica_w,
				nr_sequencia_w,
				5,  -- mae
				nm_usuario_w,
				sysdate,
				nm_mae_p, 
				ds_endereco_p
				);
		end if;

	end if;

	select	count(*)
	into	qt_registro_reg_w
	from	regulacao_atendimento
	where	nr_regulacao = nr_regulacao_p;

	if	(cd_cidadao_p is not null) and
		(cd_pessoa_fisica_w is not null) then

		select	count(*)
		into	qt_registro_pf_w
		from	pessoa_fisica
		where	cd_pessoa_cross	= cd_cidadao_p
		and	cd_pessoa_fisica = cd_pessoa_fisica_w;

		if	(qt_registro_pf_w = 0) then
			update 	pessoa_fisica
			set	cd_pessoa_cross	= cd_cidadao_p,
				nm_usuario	= nm_usuario_w,
				dt_atualizacao  = sysdate
			where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		end if;
	end if;

	if	(qt_registro_reg_w = 0) then

		insert into regulacao_atendimento (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_regulacao,
				nr_atendimento_original,
				cd_pessoa_fisica,
				nr_seq_laudo,
				ie_status_regulacao,
                                cd_cnes_solicitante,
				cd_cnes_executante,
				nm_paciente_regulacao
			) values (
				regulacao_atendimento_seq.nextval,
				sysdate,
				nm_usuario_w,
				sysdate,
				nm_usuario_w,
				nr_regulacao_p,
				nr_atendimento_p,
				cd_pessoa_fisica_w,
				nr_seq_laudo_p,
				'1',
                                cd_cnes_solicitante_p,
				cd_cnes_executante_p,
				decode(cd_pessoa_fisica_w, null, nm_pessoa_fisica_p, null)
			);
			commit;
	end if;
end if;

commit;

end regulacao_atendimento_grava;
/
