create or replace
procedure pls_obter_medico_vinculo_web  (nr_crm_p		varchar2,
					 nr_seq_prestador_p	number,
					 nm_medico_p	    out	varchar2,
					 cd_pessoa_fisica_p out varchar2)  is
					 
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Retornar os dados do m�dico, e verificar se o mesmo est� vinculado ao prestador
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relat�rios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/
		
ds_retorno_w		Varchar2(60);
cd_pessoa_fisica_w	Varchar2(20);			
qt_registros_w		Number(10);			

begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	medico
where	nr_crm	= nr_crm_p;

if	(cd_pessoa_fisica_w is not null) then
	select  count(1)
	into	qt_registros_w
	from	pls_prestador_medico
	where	nr_seq_prestador = nr_seq_prestador_p
	and	cd_medico = cd_pessoa_fisica_w;
		
	if	(qt_registros_w = 0) then
		select  count(1)
		into	qt_registros_w
		from	pls_prestador
		where	nr_sequencia = nr_seq_prestador_p
		and	cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		if	(qt_registros_w = 0) then
			cd_pessoa_fisica_w := null;
		end if;
	end if;
end if;

if	(cd_pessoa_fisica_w is not null) then
	select	max(nm_pessoa_fisica)
	into	ds_retorno_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
end if;

nm_medico_p := ds_retorno_w;
cd_pessoa_fisica_p := cd_pessoa_fisica_w;

end pls_obter_medico_vinculo_web;
/