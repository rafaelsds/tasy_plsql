create or replace
procedure gerar_historico_cotacao_integr(	nr_cot_compra_p		number,
				ds_titulo_p		varchar2,
				ds_historico_p		varchar2,
				ie_origem_p		varchar2,
				nm_usuario_p		Varchar2) is 
begin
insert into cot_compra_hist(
	nr_sequencia,
	nr_cot_compra,
	dt_atualizacao,
	nm_usuario,
	dt_historico,
	ds_titulo,
	ds_historico,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_origem,
	ie_tipo,
	ie_tipo_log_integr)
values(	cot_compra_hist_seq.nextval,
	nr_cot_compra_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	substr(ds_titulo_p,1,80),
	substr(ds_historico_p,1,4000),
	sysdate,
	nm_usuario_p,
	ie_origem_p,
	'H',
	'R');
commit;

end gerar_historico_cotacao_integr;
/
