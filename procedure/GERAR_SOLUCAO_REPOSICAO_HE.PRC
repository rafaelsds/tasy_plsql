create or replace
procedure Gerar_solucao_reposicao_he(nr_prescricao_p		number,
								     nr_sequencia_p			number,
								     nm_usuario_p			Varchar2) is 
					
nr_etapas_w					number(3);
cd_material_w				number(6);
nr_seq_solucao_w			number(6);
nr_agrupamento_w			number(6);
qt_hora_validade_w			number(15);
nr_seq_w					number(15);
qt_hora_fase_w				number(15,4);
qt_dosagem_w				number(15,4);
qt_vol_etapa_w				number(15,4);	
qt_solucao_total_w			number(15,4);
hr_prim_horario_w			varchar2(5);
ie_bomba_infusao_w			varchar2(50);
ds_horarios_w				varchar2(2000);
ds_horarios_2w				varchar2(2000);
dt_primeiro_horario_w		date;

Cursor C01 is
select	d.cd_material,
	a.qt_vol_etapa
from	prescr_rep_he_elem_mat a,
	prescr_rep_he_elem b,
	prescr_rep_he c,
	nut_elem_material d
where	a.nr_seq_ele_rep = b.nr_sequencia
and	b.nr_seq_rep_he  = c.nr_sequencia
and	a.nr_seq_elem_mat = d.nr_sequencia
and	c.nr_sequencia = nr_sequencia_p
and	c.nr_prescricao = nr_prescricao_p
and	nvl(a.qt_vol_etapa,0) > 0;

begin

if	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) then
	
	select	prescr_solucao_seq.nextval
	into	nr_seq_solucao_w
	from	dual;
	
	select	to_char(dt_primeiro_horario,'hh24:mi'),
			nvl(dt_inicio_prescr,dt_primeiro_horario)
	into	hr_prim_horario_w,
			dt_primeiro_horario_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	
	select	nvl(max(nr_agrupamento),0) + 1
	into	nr_agrupamento_w
	from	prescr_solucao
	where	nr_prescricao = nr_prescricao_p;
	
	select	sum(a.qt_vol_etapa)
	into	qt_solucao_total_w
	from	prescr_rep_he_elem_mat a,
		prescr_rep_he_elem b,
		prescr_rep_he c
	where	a.nr_seq_ele_rep = b.nr_sequencia
	and	b.nr_seq_rep_he  = c.nr_sequencia
	and	c.nr_sequencia = nr_sequencia_p
	and	c.nr_prescricao = nr_prescricao_p;
	
	select	max(qt_hora_validade),
		max(qt_etapa),
		max(qt_hora_fase),
		max(ie_bomba_infusao)
	into	qt_hora_validade_w,
		nr_etapas_w,
		qt_hora_fase_w,
		ie_bomba_infusao_w
	from	prescr_rep_he
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia  = nr_sequencia_p;
	
	Calcula_Horarios_Etapas(to_date(to_char(dt_primeiro_horario_w,'dd/mm/yyyy ')||hr_prim_horario_w,'dd/mm/yyyy hh24:mi')
					,nr_etapas_w,dividir(qt_hora_validade_w,nr_etapas_w),nm_usuario_p,'N',qt_hora_validade_w,ds_horarios_w,ds_horarios_2w,null,'N');
	
	insert into prescr_solucao(nr_prescricao,
							   nr_seq_solucao,
							   ie_via_aplicacao,
							   dt_atualizacao,
							   nm_usuario,
							   cd_unidade_medida,
							   ie_tipo_dosagem,
							   qt_dosagem,
							   qt_solucao_total,
							   qt_tempo_aplicacao,
							   qt_volume,
							   nr_etapas,
							   ie_bomba_infusao,
							   ie_suspenso,
							   ie_calc_aut,
							   ie_acm,
							   qt_hora_fase,
							   ds_horarios,
							   hr_prim_horario,
							   ie_urgencia,
							   ds_solucao,
							   ie_solucao_especial,
							   ie_se_necessario,
							   ds_orientacao,
							   ie_tipo_sol,
							   ie_esquema_alternado,
							   nr_agrupamento)
	(select	nr_prescricao,
			nr_seq_solucao_w,
			OBTER_VIA_USUARIO('IV'),
			sysdate,
			nm_usuario_p,
			'mlh',
			'mlh',
			null,
			qt_solucao_total_w * qt_etapa,
			qt_hora_validade,
			null,
			qt_etapa,
			nvl(ie_bomba_infusao_w,'N'),
			'N',
			'S',
			null,
			qt_hora_fase, 
			ds_horarios_w,
			hr_prim_horario_w,
			'N',
			obter_desc_expressao(782498),--'Terapia de reidratação endovenosa (TREV)',
			'N',
			'N',
			null,
			null,
			'N',
			nr_agrupamento_w
	from		prescr_rep_he
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia  = nr_sequencia_p);
	
	commit;
	
	open C01;
	loop
	fetch C01 into	
		cd_material_w,
		qt_vol_etapa_w;
	exit when C01%notfound;
		
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_p;
		
		insert into prescr_material(nr_sequencia,
									nr_prescricao,
									cd_material,
									ie_agrupador,
									cd_unidade_medida,
									cd_unidade_medida_dose,
									nr_sequencia_solucao,
									qt_dose,
									qt_unitaria,
									qt_material,
									qt_solucao,
									ie_origem_inf,
									dt_atualizacao,
									nm_usuario)
				values (nr_seq_w,
						nr_prescricao_p,
						cd_material_w,
						4,
						OBTER_UNID_MED_USUA('ml'),
						OBTER_UNID_MED_USUA('ml'),
						nr_seq_solucao_w,
						qt_vol_etapa_w,
						1,
						1,
						qt_vol_etapa_w,
						'N',
						sysdate,
						nm_usuario_p);
				
			commit;
		
	end loop;
	close C01;
	
	Calcular_Volume_Total_Solucao(OBTER_UNID_MED_USUA('ml'), qt_hora_validade_w, qt_solucao_total_w, null, nr_prescricao_p, nr_seq_solucao_w, 1, qt_dosagem_w, 'S', nr_etapas_w, 'N', qt_hora_fase_w);

	if	(qt_dosagem_w is not null) and
		(qt_dosagem_w > 0) then
		update	prescr_solucao
		set	qt_dosagem = qt_dosagem_w
		where	nr_prescricao = nr_prescricao_p
		and	nr_seq_solucao = nr_seq_solucao_w;
	end if;
	
end if;

commit;

end Gerar_solucao_reposicao_he;
/
