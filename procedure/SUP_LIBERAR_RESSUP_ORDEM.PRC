create or replace
procedure sup_liberar_ressup_ordem(
			nr_requisicao_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 

begin

update	requisicao_material
set	dt_liberacao = sysdate,
	dt_aprovacao = sysdate
where	nr_requisicao = nr_requisicao_p;

commit;

end sup_liberar_ressup_ordem;
/