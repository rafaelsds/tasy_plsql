create or replace
Procedure Atualiza_MesAno_Refer_Conta(	dt_parametro_p			date,
					cd_convenio_p			number,
					nm_usuario_p			varchar2,
					ie_protocolo_provisorio_p	varchar2,
					ie_protocolo_auditoria_p	varchar2,
					cd_estab_p			number,
					ie_tipo_convenio_p		number default null)	is



nr_interno_conta_w		Number(14,0);
nr_seq_protocolo_w		Number(14,0);
ie_status_protocolo_w		varchar2(01);
dt_mesano_protocolo_w		date;
nr_atendimento_w		number(10);
dt_mesano_referencia_w		date;
cd_convenio_parametro_w		number(5,0);
ie_atualiza_usuario_w		varchar2(1);
ie_tipo_convenio_w		number(2);
ds_titulo_w			varchar2(200);
ie_atualiza_conta_w		varchar2(1);
ie_atualiza_conta_part_w	varchar2(1);
ie_conta_cancel_estorno_w	varchar2(1);
ie_cancelamento_w		varchar2(1);
dt_parametro_start_w            conta_paciente.dt_mesano_referencia%type;
dt_parametro_end_w              conta_paciente.dt_mesano_referencia%type;
dt_mesano_add_month_w           protocolo_convenio.dt_mesano_referencia%type;
dt_parametro_start_add_month_w  conta_paciente.dt_mesano_referencia%type;

Cursor C01 is
	select	a.nr_interno_conta,
		nvl(a.nr_seq_protocolo, -1),
		a.nr_atendimento,
		a.dt_mesano_referencia,
		a.cd_convenio_parametro,
		a.ie_cancelamento
	from	conta_paciente a,
		convenio b
	where	a.dt_mesano_referencia between 
			dt_parametro_start_w and
			dt_parametro_end_w
	and	(((ie_protocolo_provisorio_p = 'S') or (ie_protocolo_auditoria_p = 'S'))  or (a.nr_seq_protocolo is null))
	and	((nvl(cd_convenio_p,0) = 0) or (cd_convenio_p = a.cd_convenio_parametro))
	and	(cd_estabelecimento	= cd_estab_p or cd_estab_p is null)
	and 	a.cd_convenio_parametro = b.cd_convenio
	and 	((b.ie_tipo_convenio = ie_tipo_convenio_p) or (nvl(ie_tipo_convenio_p,0) = 0));

BEGIN


ie_atualiza_usuario_w	 := nvl(obter_valor_param_usuario(79, 4, obter_perfil_ativo, nm_usuario_p, WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO),'S');
ie_atualiza_conta_part_w := nvl(obter_valor_param_usuario(79, 5, obter_perfil_ativo, nm_usuario_p, WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO),'N');
ie_conta_cancel_estorno_w:= nvl(obter_valor_param_usuario(79, 6, obter_perfil_ativo, nm_usuario_p, WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO),'N');

dt_parametro_start_w    := PKG_DATE_UTILS.start_of(dt_parametro_p,'month',0);
dt_parametro_end_w      := PKG_DATE_UTILS.END_OF(dt_parametro_p,'MONTH',0);

OPEN C01;
LOOP
FETCH C01 into 	
	nr_interno_conta_w,
	nr_seq_protocolo_w,
	nr_atendimento_w,
	dt_mesano_referencia_w,
	cd_convenio_parametro_w,
	ie_cancelamento_w;
EXIT WHEN C01%NOTFOUND;

select	nvl(max(ie_status_protocolo), '1'),
        max(dt_mesano_referencia)
into	ie_status_protocolo_w,
        dt_mesano_protocolo_w
from	protocolo_convenio
where	nr_seq_protocolo	= nr_seq_protocolo_w;

dt_mesano_add_month_w           := PKG_DATE_UTILS.ADD_MONTH(dt_mesano_protocolo_w,1,0);
dt_mesano_protocolo_w           := PKG_DATE_UTILS.start_of(dt_mesano_protocolo_w,'month',0);
dt_parametro_start_add_month_w  := PKG_DATE_UTILS.ADD_MONTH(dt_parametro_start_w,1,0);

if	(ie_status_protocolo_w 	= '1') and
        (ie_protocolo_provisorio_p = 'S') and
        (dt_mesano_protocolo_w < dt_parametro_start_add_month_w) then
        update	protocolo_convenio
        set	dt_mesano_referencia 	= dt_mesano_add_month_w,
                nm_usuario 		= nm_usuario_p,
                dt_atualizacao 		= sysdate
        where	nr_seq_protocolo	= nr_seq_protocolo_w;
end if;

-- Protocolo com status em Auditoria OS 165654
if	(ie_status_protocolo_w 	= '3') and
        (ie_protocolo_auditoria_p = 'S') and
        (dt_mesano_protocolo_w < dt_parametro_start_add_month_w) then
        update	protocolo_convenio
        set	dt_mesano_referencia 	= dt_mesano_add_month_w,
                nm_usuario 		= nm_usuario_p,
                dt_atualizacao 		= sysdate
        where	nr_seq_protocolo	= nr_seq_protocolo_w;
end if;

ie_atualiza_conta_w	:= 'S';

if	(ie_atualiza_conta_part_w = 'S') then

        select	nvl(max(ie_tipo_convenio),3)
        into	ie_tipo_convenio_w
        from 	convenio
        where	cd_convenio	= cd_convenio_parametro_w;
        
        if	(ie_tipo_convenio_w = 1) and
                (nr_seq_protocolo_w < 0) then
        
                select	substr(obter_titulo_conta_protocolo(0, nr_interno_conta_w),1,100)
                into	ds_titulo_w
                from 	dual;
                
                if	(ds_titulo_w is not null) then
                
                        ie_atualiza_conta_w	:= 'N';
                        
                end if;
                
        end if;
        
end if;

if	(ie_conta_cancel_estorno_w = 'S') and 
        (ie_atualiza_conta_w = 'S') and
        (ie_cancelamento_w is not null) then
        ie_atualiza_conta_w:= 'N';
end if;

update	conta_paciente
set	dt_mesano_referencia 	= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia,1,0),
        nm_usuario 		= decode(ie_atualiza_usuario_w, 'N', nm_usuario, nm_usuario_p),
        dt_atualizacao 		= sysdate
where	nr_interno_conta	= nr_interno_conta_w
and	(((ie_protocolo_provisorio_p = 'S') or (ie_protocolo_auditoria_p = 'S')) or (ie_status_protocolo_w	= '1'))
and	ie_atualiza_conta_w 	= 'S';

dt_mesano_add_month_w := PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_w,1,0);
        
insert	into log_alteracao_mesano_ref
        (cd_convenio,
        dt_atualizacao,
        dt_atualizacao_nrec,
        dt_mesano_refer_ant,
        dt_mesano_referencia,
        nm_usuario,
        nm_usuario_nrec,
        nr_atendimento,
        nr_interno_conta,
        nr_sequencia,
        vl_conta)
values	(cd_convenio_parametro_w,
        sysdate,
        sysdate,
        dt_mesano_referencia_w,
        dt_mesano_add_month_w,
        nm_usuario_p,
        nm_usuario_p,
        nr_atendimento_w,
        nr_interno_conta_w,
        log_alteracao_mesano_ref_seq.nextval,
        obter_valor_conta(nr_interno_conta_w,0));

END LOOP;
CLOSE C01;

commit;

end Atualiza_MesAno_Refer_Conta;
/