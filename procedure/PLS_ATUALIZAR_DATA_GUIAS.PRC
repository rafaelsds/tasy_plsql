create or replace
procedure pls_atualizar_data_guias(
		ds_lista_data_p	varchar2,
		dt_liberacao_p	date,
		nm_usuario_p	varchar2) is 

ds_lista_data_w		varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_sequencia_w		number(10);
nr_controle_loop_w	number(3,0) := 0;
			
begin
if	(ds_lista_data_p is not null) and
	(dt_liberacao_p is not null) and
	(nm_usuario_p is not null) then
	begin		
	ds_lista_data_w := ds_lista_data_p;
		
	while 	(ds_lista_data_w is not null) and
		(nr_controle_loop_w < 100) loop
		begin
		nr_pos_virgula_w := instr(ds_lista_data_w,',');
	
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_sequencia_w	:= to_number(substr(ds_lista_data_w,0,nr_pos_virgula_w-1));
			ds_lista_data_w	:= substr(ds_lista_data_w,nr_pos_virgula_w+1,length(ds_lista_data_w));			
			end;
		else
			begin
			nr_sequencia_w	:= to_number(ds_lista_data_w);
			ds_lista_data_w	:= null;
			end;
		end if;
		
		if	(nr_sequencia_w > 0) then
			begin
			update	pls_guia_plano_proc
			set	dt_liberacao = dt_liberacao_p,
				nm_usuario   = nm_usuario_p
			where   nr_sequencia = nr_sequencia_w;

			update	pls_guia_plano_mat
			set	dt_liberacao = dt_liberacao_p,
				nm_usuario   = nm_usuario_p
			where   nr_sequencia = nr_sequencia_w; 
			end;
		end if;
		nr_controle_loop_w := nr_controle_loop_w + 1;
		end;
	end loop;		
	end;
end if;
commit;
end  pls_atualizar_data_guias;
/