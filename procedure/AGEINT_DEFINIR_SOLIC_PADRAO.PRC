create or replace
procedure Ageint_Definir_Solic_Padrao(
			cd_medico_req_p		varchar2,
			nr_seq_ageint_p		number,
			ds_itens_p		varchar2,
			nm_usuario_p		Varchar2) is 

begin

update	agenda_integrada_item
set	cd_medico_req	= cd_medico_req_p
where	ds_itens_p like '%'||nr_sequencia||'%'
and	nr_seq_agenda_int	= nr_seq_ageint_p;

commit;

end Ageint_Definir_Solic_Padrao;
/