create or replace
procedure pls_define_preco_pacote
			(	cd_estabelecimento_p	      in		Number,
				nr_seq_prestador_p	      in		Number,
				nr_seq_tipo_acomod_p	      in		Number,
				dt_pacote_p		      in		Date,
				cd_procedimento_p	      in		Number,
				ie_origem_proced_p	      in		Number,
				ie_internado_p		      in		Varchar2,
				nr_seq_plano_p		      in		Varchar2,
				nr_contrato_p		      in		Number,
				nr_seq_congenere_p	      in		Number,
				nm_usuario_p		      in		Varchar2,
				ie_origem_conta_p	      in		Varchar2,
				ie_tipo_intercambio_p	      in		Varchar2,
				nr_seq_pacote_p		      out		Number,
				nr_seq_regra_pacote_p	      out		Number,
				cd_proc_pacote_p	      out		Number,
				ie_origem_pacote_p	      out		Number,
				vl_pacote_p		      out		Number,
				vl_medico_p		      out		Number,
				vl_anestesista_p	      out		Number,
				vl_auxiliares_p		      out		Number,
				vl_custo_operacional_p	      out		Number,
				vl_materiais_p		      out		Number,
				nr_seq_intercambio_p	      in		Number,
				nr_seq_classificacao_prest_p  in 		Number,
				nr_seq_procedimento_p	      in		Number,
				nr_seq_segurado_p	      in		Number,
				ie_gerar_log_p		      in		Varchar2,
				ie_conta_medica_p	      in		Varchar2,
				nr_seq_prest_inter_p	      in		pls_conta_v.nr_seq_prest_inter%type default null,
				ie_tipo_tabela_p	      in		Number default 1) is
 
nr_seq_categoria_w		Number(10);
vl_pacote_w			Number(15,2)	:= 0;
vl_medico_w			Number(15,2)	:= 0;
vl_anestesista_w		Number(15,2)	:= 0; 
vl_auxiliares_w			Number(15,2)	:= 0;
vl_custo_operacional_w		Number(15,2)	:= 0;
vl_materiais_w			Number(15,2)	:= 0;
dt_pacote_w			Date;
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
ie_preco_w			Varchar2(10);
ie_tipo_contratacao_w		Varchar2(10);
ie_regulamentacao_w		Varchar2(10);
nr_Seq_regra_preco_w		number(10);
ie_excecao_w			varchar2(1);
nr_seq_regra_pacote_ww		Number(10);
qt_regra_w			Number(10);
nr_seq_regra_pacote_w		Number(10);
nr_seq_pacote_w			Number(10);
nr_seq_grupo_prestador_w	Number(10);
ie_grupo_prestador_w		Varchar2(1)	:= 'S';	
cd_prestador_w			Varchar2(30);
nr_seq_grupo_produto_w		Number(10);
nr_seq_grupo_contrato_w		NUmber(10);
nr_seq_contrato_w		Number(10);
nr_seq_intercambio_w		Number(10);
ie_grupo_contrato_w		Varchar2(2);
ie_grupo_produto_w		Varchar2(2);
nr_seq_regra_w			Number(10);
ds_observacao_log_w		varchar2(4000);
ie_restringe_estab_w		Varchar2(2);
ie_tipo_segurado_w		Varchar2(255);
ie_tipo_repasse_w		Varchar2(255)	:= 'X';
nr_seq_congenere_w  		pls_segurado.nr_seq_congenere%type;
ie_grupo_operadora_w    	varchar2(1);
nr_seq_grupo_operadora_w    	pls_pacote_tipo_acomodacao.nr_seq_grupo_operadora%type;
ie_acomodacao_valida_w		varchar2(1);
ie_acomodacao_regra_w		pls_pacote_tipo_acomodacao.ie_acomodacao%type;
nr_seq_tipo_acomod_benef_w	pls_segurado.nr_seq_tipo_acomodacao%type;
ie_tipo_acomodacao_w		pls_tipo_acomodacao.ie_tipo_acomodacao%type;
ie_acomodacao_plano_w		pls_plano.ie_acomodacao%type;

Cursor C01 is	
	select	b.nr_sequencia,
		a.nr_sequencia,
		a.cd_procedimento,
		a.ie_origem_proced,
		nvl(a.nr_seq_grupo_prestador,0),
		nvl(a.vl_pacote,0),
		nvl(a.vl_medico,0),
		nvl(a.vl_anestesista,0),
		nvl(a.vl_auxiliares,0),
		nvl(a.vl_custo_operacional,0),
		nvl(a.vl_materiais,0),
		nvl(a.nr_seq_grupo_contrato,0),
		nvl(a.nr_seq_grupo_produto,0),
		a.nr_seq_grupo_operadora,
		nvl(a.ie_acomodacao,'N') ie_acomodacao
	from	pls_pacote			b,
		pls_pacote_tipo_acomodacao	a 
	where	a.nr_seq_pacote		= b.nr_sequencia
	and	a.ie_situacao = 'A'
	and	((ie_restringe_estab_w	= 'N') or (ie_restringe_estab_w is null)
	or	((ie_restringe_estab_w	= 'S') and (a.cd_estabelecimento = cd_estabelecimento_p)))
	and	(((a.nr_seq_prestador is null) or a.nr_seq_prestador = nvl(nr_seq_prestador_p,0)))
	and	((a.cd_prestador = nvl(cd_prestador_w,'0')) or (a.cd_prestador is null))
	and	nvl(b.cd_procedimento, cd_procedimento_p)	= cd_procedimento_p
	and	nvl(b.ie_origem_proced,ie_origem_proced_p)	= ie_origem_proced_p
	--and 	nvl(dt_pacote_w,sysdate) between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_pacote_w+1)
	and	nvl(dt_pacote_w,sysdate) between a.dt_inicio_vigencia AND ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(nvl(a.dt_fim_vigencia,dt_pacote_w+1)) 
	and	((a.nr_seq_tipo_acomodacao is null) or (nr_seq_tipo_acomod_p is null) or (a.nr_seq_tipo_acomodacao = nr_seq_tipo_acomod_p))
	and	((a.nr_seq_categoria_acomod is null) or (nr_seq_categoria_w is null) or (a.nr_seq_categoria_acomod = nr_seq_categoria_w))
	and	(
		(nvl(a.ie_preco,nvl(ie_preco_w,0)) = nvl(ie_preco_w,0) and ie_tipo_repasse_w = 'X') or
		/*Preco produto: 1 - Pre, 2,3 - Pos  ----- Tipo repasse PTU: C - Custo, P - Pre pagamento    */
		((ie_tipo_repasse_w <> 'X') and
		((a.ie_preco in ('2','3') and ie_tipo_repasse_w = 'C') or (a.ie_preco  = '1' and ie_tipo_repasse_w = 'P') or (a.ie_preco	is null))
		)
		)
	and	((a.ie_tipo_contratacao is null) or (a.ie_tipo_contratacao = nvl(ie_tipo_contratacao_w,0)))
	and	((nvl(a.ie_internado,'N') = 'N' ) or (a.ie_internado	= ie_internado_p))
	and	((nvl(a.ie_tipo_intercambio,'A') = 'A') or (nvl(a.ie_tipo_intercambio,'A') = nvl(ie_tipo_intercambio_p,'A')))
	and	(nvl(a.nr_contrato,nvl(nr_contrato_p,0)) = nvl(nr_contrato_p,0))
	and	((a.nr_seq_plano is null) or (a.nr_seq_plano = nr_seq_plano_p))
	and	((nvl(nr_seq_congenere,0) = nvl(nr_seq_congenere_p,0)) or (nr_seq_congenere is null))
	and	((nvl(nr_seq_intercambio,0) = nvl(nr_seq_intercambio_p,0)) or (nr_seq_intercambio is null))
	and	((nvl(ie_regulamentacao,0) = nvl(ie_regulamentacao_w,0)) or (ie_regulamentacao is null))
	and	((nvl(a.ie_origem_conta, ie_origem_conta_p) = ie_origem_conta_p) or (nvl(ie_origem_conta_p,'X') = 'X'))
	and	(ie_tipo_repasse	is null or ie_tipo_repasse	= ie_tipo_repasse_w)
	and	((ie_tipo_segurado 	is null) or (ie_tipo_segurado 	= ie_tipo_segurado_w))
	and	((ie_conta_medica	is null) or (ie_conta_medica	= ie_conta_medica_p) or (ie_conta_medica = 'S'))
	and	(nvl(ie_tipo_tabela,1) = ie_tipo_tabela_p)
	and	((a.nr_seq_prest_inter is null)  or (nr_seq_prest_inter_p = a.nr_seq_prest_inter))
	order by nvl(a.nr_seq_prestador,0),
		nvl(a.cd_prestador,'0'),
		nvl(a.cd_procedimento,0),
		nvl(a.nr_seq_plano,0),        
		nvl(a.nr_seq_grupo_produto,0),
		nvl(a.ie_tipo_intercambio,'A'),
		nvl(a.nr_seq_grupo_prestador,0), 
		nvl(a.nr_seq_grupo_contrato,0),
		nvl(a.nr_seq_categoria_acomod,0),
		nvl(a.nr_seq_tipo_acomodacao,0),
		nvl(a.nr_contrato,0),     
		a.dt_inicio_vigencia,         
		nvl(a.ie_tipo_contratacao,'Z') desc,  
		nvl(a.ie_tipo_segurado,'A'),
		nvl(a.ie_preco,'0'),          
		nvl(a.ie_internado,'N'),
		nvl(a.nr_seq_intercambio,0),  
		nvl(a.nr_seq_congenere,0),
		nvl(a.ie_acomodacao,'N');
		
begin
dt_pacote_w	:= nvl(dt_pacote_p, sysdate);

begin
	select	ie_tipo_segurado,
		nr_seq_congenere,
		nr_seq_tipo_acomodacao
	into	ie_tipo_segurado_w,
		nr_seq_congenere_w,
		nr_seq_tipo_acomod_benef_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
exception
when others then
	ie_tipo_segurado_w	:= 'X';
end;

if	(nr_seq_tipo_acomod_benef_w is not null) then
	select 	max(ie_tipo_acomodacao)
	into	ie_tipo_acomodacao_w
	from	pls_tipo_acomodacao
	where	nr_sequencia = nr_seq_tipo_acomod_benef_w;
end if;

if	(nvl(ie_tipo_segurado_w,'X')	in ('I','C','T','H')) then /* Se responsabilidade assumida, verifica a formacao de preco do beneficiario */
	begin
		select	nvl(ie_tipo_repasse,'X')
		into	ie_tipo_repasse_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_p;
	exception
	when others then
		ie_tipo_repasse_w	:= 'X';
	end;
end if;

begin
	select	ie_preco
	into	ie_preco_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_p;
exception
when others then
	ie_preco_w	:= 0;
end;

begin
	select	ie_tipo_contratacao,
		ie_regulamentacao,
		ie_acomodacao
	into	ie_tipo_contratacao_w,
		ie_regulamentacao_w,
		ie_acomodacao_plano_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_p;
exception
when others then
	ie_tipo_contratacao_w	:= '';
	ie_regulamentacao_w	:= '';
end;

select	max(nr_seq_categoria)
into	nr_seq_categoria_w
from	pls_regra_categoria
where	nr_seq_tipo_acomodacao	= nr_seq_tipo_acomod_p;

select	max(cd_prestador)
into	cd_prestador_w
from	pls_prestador
where	nr_sequencia	= nr_seq_prestador_p;

ie_restringe_estab_w	:= pls_obter_se_controle_estab('GP');


if	(nvl(ie_gerar_log_p,'N') = 'S') then
	ds_observacao_log_w	:=	';cd_estabelecimento_p='||chr(39)||cd_estabelecimento_p||chr(39)||
					';nr_seq_prestador_p='||chr(39)||nr_seq_prestador_p||chr(39)||
					';cd_prestador_w='||chr(39)||cd_prestador_w||chr(39)||
					';cd_procedimento_p='||chr(39)||cd_procedimento_p||chr(39)||
					';ie_origem_proced_p='||chr(39)||ie_origem_proced_p||chr(39)||
					';dt_pacote_w='||chr(39)||dt_pacote_w||chr(39)||
					';nr_seq_tipo_acomod_p='||chr(39)||nr_seq_tipo_acomod_p||chr(39)||
					';nr_seq_categoria_w='||chr(39)||nr_seq_categoria_w||chr(39)||
					';ie_preco_w='||chr(39)||ie_preco_w||chr(39)||
					';ie_tipo_contratacao_w='||chr(39)||ie_tipo_contratacao_w||chr(39)||
					';ie_internado_p='||chr(39)||ie_internado_p||chr(39)||
					';ie_tipo_intercambio_p='||chr(39)||ie_tipo_intercambio_p||chr(39)||
					';nr_contrato_p='||chr(39)||nr_contrato_p||chr(39)||
					';nr_seq_plano_p='||chr(39)||nr_seq_plano_p||chr(39)||
					';nr_seq_congenere_p='||chr(39)||nr_seq_congenere_p||chr(39)||
					';nr_seq_intercambio_p='||chr(39)||nr_seq_intercambio_p||chr(39)||
					';ie_regulamentacao_w='||chr(39)||ie_regulamentacao_w||chr(39)||
					';ie_origem_conta_p='||chr(39)||ie_origem_conta_p||chr(39);

	pls_gravar_log_calculo_proc(	'S', nr_seq_procedimento_p, cd_estabelecimento_p,
					'Inicio da definicao do preco', ds_observacao_log_w, 'pls_define_preco_pacote', 
					0, 0, 0, 
					0, 0, 0,
					0, nr_seq_regra_w, 0,
					0,null,null, 'Log');
end if;

open C01;
loop
fetch C01 into	
	nr_seq_pacote_w,
	nr_seq_regra_pacote_ww,
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_grupo_prestador_w,
	vl_pacote_w,
	vl_medico_w,
	vl_anestesista_w,
	vl_auxiliares_w,
	vl_custo_operacional_w,
	vl_materiais_w,
	nr_seq_grupo_contrato_w,
	nr_seq_grupo_produto_w,
	nr_seq_grupo_operadora_w,
	ie_acomodacao_regra_w;	
exit when C01%notfound;
	begin
	
	ie_acomodacao_valida_w := 'S';
	
	if	(ie_acomodacao_regra_w <> 'N') then
			
		if	(ie_acomodacao_regra_w <> ie_tipo_acomodacao_w or ie_tipo_acomodacao_w is null) and
			(ie_acomodacao_regra_w <> ie_acomodacao_plano_w or ie_acomodacao_plano_w is null) then
			ie_acomodacao_valida_w := 'N';
		end if;	
	end if;
	
	if	(ie_acomodacao_valida_w = 'S') then
	
		ie_excecao_w	:= 'N'; 
		ie_grupo_produto_w		:= 'S';
		ie_grupo_contrato_w		:= 'S';
		ie_grupo_prestador_w		:= 'S';
		ie_grupo_operadora_w    := 'S';

		select 	count(1)	
		into 	qt_regra_w
		from	pls_pacote_preco_Excecao
		where	nr_seq_regra_preco	= nr_seq_regra_pacote_ww;

		if 	(qt_regra_w > 0)	then 
			pls_excecao_preco_pacote(nr_seq_regra_pacote_ww, nr_seq_congenere_p,'', nr_seq_intercambio_p,nr_seq_prestador_p,ie_excecao_w,nr_seq_congenere_w);
		end if ;
		
		if	(nvl(nr_seq_grupo_contrato_w,0) > 0) then
			if	((nvl(nr_contrato_p,0)	> 0) or
				(nvl(nr_seq_intercambio_p,0) > 0)) then
				ie_grupo_contrato_w	:= pls_se_grupo_preco_contrato(nr_seq_grupo_contrato_w, nr_contrato_p, nr_seq_intercambio_p);
			else
				ie_grupo_contrato_w	:= 'N';
			end if;
		end if;
		/* Grupo de produtos */
		if	(nvl(nr_seq_grupo_produto_w,0) > 0) then
			ie_grupo_produto_w	:= pls_se_grupo_preco_produto(nr_seq_grupo_produto_w, nr_seq_plano_p);
		end if;
	    
		if	(nvl(nr_seq_grupo_prestador_w,0) > 0) then
			ie_grupo_prestador_w	:= pls_se_grupo_preco_prestador(nr_seq_grupo_prestador_w, nr_seq_prestador_p,nvl(nr_seq_classificacao_prest_p,''));
		end if;
	  
		if	(ie_excecao_w = 'N') then		
			nr_seq_regra_pacote_w	:=	nr_seq_regra_pacote_ww;
		end if;	
	    
	    if (nr_seq_grupo_operadora_w is not null) then
		ie_grupo_operadora_w := pls_se_grupo_preco_operadora(nr_seq_grupo_operadora_w,nvl(nr_seq_congenere_w,nr_seq_congenere_p));
		end if;
		
		if	(nvl(ie_grupo_prestador_w,'N') = 'S') and
			(nvl(ie_grupo_produto_w,'N') = 'S') and
			(nvl(ie_grupo_contrato_w,'N') = 'S') and
			(nvl(ie_grupo_operadora_w,'N') = 'S') and
			(nvl(nr_seq_regra_pacote_w,0) > 0) and
			(ie_excecao_w = 'N') then	
			nr_seq_regra_pacote_p	:= nr_seq_regra_pacote_w;
			nr_seq_pacote_p		:= nr_seq_pacote_w;
			cd_proc_pacote_p	:= cd_procedimento_w;
			ie_origem_pacote_p	:= ie_origem_proced_w;
			vl_pacote_p		:= vl_pacote_w;
			vl_medico_p		:= vl_medico_w;
			vl_anestesista_p	:= vl_anestesista_w;
		
		end if;	
	end if;
    
    end;
end loop;
close C01;

if	(nvl(ie_gerar_log_p,'N') = 'S') then
	ds_observacao_log_w	:= 'Regra: '||nr_seq_regra_pacote_w||' / Seq. pacote : '||nr_seq_pacote_w;

	pls_gravar_log_calculo_proc(	'S', nr_seq_procedimento_p, cd_estabelecimento_p,
					'Preco informado', ds_observacao_log_w, 'pls_define_preco_pacote', 
					vl_pacote_w, 0, 0, 
					vl_medico_w, 0, vl_anestesista_w,
					1, nr_seq_regra_pacote_w, 0,
					0,null, null, 'Log');
end if;

 /*Felipe - OS 84642 - 10/09/2008
vl_auxiliares_p		:= vl_auxiliares_w;
vl_custo_operacional_p		:= vl_custo_operacional_w;
vl_materiais_p		:= vl_materiais_w;
*/

end pls_define_preco_pacote;
/
