create or replace
procedure Gerar_prescr_fut_exame_vanco(	nr_prescr_origem_p			number,
										nr_atendimento_p			number,
										ie_tipo_pessoa_p			varchar2,
										cd_perfil_p					number,
										nm_usuario_p				varchar2,
										nr_prescr_proc_nsv_p	out	number) is

ds_erro_w				varchar2(4000);
qt_hor_exame_vanco_w	number(15);
dt_prev_execucao_w		date;
dt_inicio_prescr_w		date;
cd_estabelecimento_w	number(15);
cd_setor_atendimento_w	number(15);

begin
if	(nr_prescr_origem_p is not null) then

	select	count(*)
	into	qt_hor_exame_vanco_w
	from	prescr_material
	where	dt_suspensao is null
	and		obter_se_mat_vancomicina(cd_material) = 'S'
	and		ie_agrupador = 1
	and		nr_prescricao = nr_prescr_origem_p;
	
	if	(qt_hor_exame_vanco_w = 0) then
		update	prescr_medica
		set		qt_hor_exame_vanco = null
		where	nr_prescricao = nr_prescr_origem_p;
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

		return;
	end if;

	select	nvl(max(qt_hor_exame_vanco),0),
			nvl(max(cd_estabelecimento),1)
	into	qt_hor_exame_vanco_w,
			cd_estabelecimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescr_origem_p;

	if	(qt_hor_exame_vanco_w > 0) then
		
		select	max(cd_setor_atendimento)
		into	cd_setor_atendimento_w
		from	prescr_medica
		where	nr_prescricao = nr_prescr_origem_p;

		select	prescr_medica_seq.nextval
		into	nr_prescr_proc_nsv_p
		from	dual;

		insert into prescr_medica (
				nr_prescricao,
				cd_pessoa_fisica,
				nr_atendimento,
				cd_medico,
				dt_prescricao,
				dt_atualizacao,
				nm_usuario,
				nr_horas_validade,
				dt_primeiro_horario,
				ie_origem_inf,
				ie_recem_nato,
				nm_usuario_original,
				cd_protocolo,
				nr_seq_protocolo,
				cd_estabelecimento,
				cd_prescritor,
				qt_altura_cm,
				qt_peso,
				ie_adep,
				ie_prescr_emergencia,
				ie_prescricao_alta,
				cd_setor_atendimento,
				ie_hemodialise,
				ie_prescritor_aux,
				cd_setor_entrega,
				dt_entrega,
				nr_seq_pend_pac_acao,
				ds_observacao,
				cd_funcao_origem)
		select	nr_prescr_proc_nsv_p,
				cd_pessoa_fisica,
				nr_atendimento,
				cd_medico,
				dt_prescricao,
				dt_atualizacao,
				nm_usuario,
				24,
				dt_primeiro_horario,
				ie_origem_inf,
				ie_recem_nato,
				nm_usuario_original,
				cd_protocolo,
				nr_seq_protocolo,
				cd_estabelecimento,
				cd_prescritor,
				qt_altura_cm,
				qt_peso,
				ie_adep,
				ie_prescr_emergencia,
				ie_prescricao_alta,
				cd_setor_atendimento_w,
				ie_hemodialise,
				ie_prescritor_aux,
				cd_setor_entrega,
				dt_entrega,
				nr_seq_pend_pac_acao,
				obter_desc_expressao(729102) || nr_prescricao,
				cd_funcao_origem
		from	prescr_medica
		where	nr_prescricao = nr_prescr_origem_p;
		
		Gerar_protocolo_vancomicina(	cd_estabelecimento_w,
										cd_perfil_p,
										nm_usuario_p,
										nr_prescr_origem_p,
										nr_prescr_proc_nsv_p,
										'N',
										'S',
										qt_hor_exame_vanco_w);
									
		select	max(dt_prev_execucao)
		into	dt_prev_execucao_w
		from	prescr_procedimento
		where	obter_se_exame_vancocinemia(nr_seq_exame) = 'S'
		and		nr_prescricao = nr_prescr_proc_nsv_p;
		
		if	(dt_prev_execucao_w is not null) then
			if	(cd_setor_atendimento_w is null) then
				update	prescr_medica
				set		dt_prescricao = PKG_DATE_UTILS.start_of(dt_prev_execucao_w, 'hh24', 0) -1/24,
						nr_horas_validade = 2,
						dt_primeiro_horario = trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_primeiro_horario, dt_prev_execucao_w), 'hh24') - 3/24
				where	nr_prescricao = nr_prescr_proc_nsv_p;
			else
				dt_inicio_prescr_w	:= to_date(to_char(dt_prev_execucao_w,'dd/mm/yyyy ') || to_char(obter_prim_hor_setor(cd_setor_atendimento_w), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');
				if	(dt_inicio_prescr_w > dt_prev_execucao_w) then
					dt_inicio_prescr_w	:= dt_inicio_prescr_w - 1;
				end if;
				
				update	prescr_medica
				set		dt_prescricao = dt_inicio_prescr_w,
						nr_horas_validade = 24,
						dt_primeiro_horario = trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_primeiro_horario, dt_inicio_prescr_w), 'hh24'),
						dt_inicio_prescr = dt_inicio_prescr_w
				where	nr_prescricao = nr_prescr_proc_nsv_p;
			end if;

			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
		end if;
		
		update	prescr_medica
		set		qt_hor_exame_vanco = null
		where	nr_prescricao = nr_prescr_origem_p;
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	end if;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Gerar_prescr_fut_exame_vanco;
/
