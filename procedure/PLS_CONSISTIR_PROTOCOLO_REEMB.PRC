create or replace
procedure pls_consistir_protocolo_reemb
			(	nr_seq_protocolo_p	number,
				dt_protocolo_p		date,
				dt_mes_competencia_p	date,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2	) is 

dt_mes_competencia_w	date;
dt_protocolo_w		date;
ie_param_mes_w		varchar2(2);
dt_ref_inicial_w		date;
dt_ref_final_w		date;
ds_tipo_lote_contabil_w	tipo_lote_contabil.ds_tipo_lote_contabil%type;

begin
if	(nvl(nr_seq_protocolo_p,0) > 0) then
	select	trunc(dt_mes_competencia, 'month'),
		trunc(dt_protocolo, 'month')
	into	dt_mes_competencia_w,
		dt_protocolo_w
	from	pls_protocolo_conta
	where	nr_sequencia = nr_seq_protocolo_p;
else	
	dt_mes_competencia_w	:= trunc(dt_mes_competencia_p, 'month');
	dt_protocolo_w 		:= trunc(dt_protocolo_p, 'month');
end if;

ie_param_mes_w := obter_valor_param_usuario(1229, 20, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p);

if	(nvl(ie_param_mes_w,'N') = 'S') then
	if	(dt_protocolo_w <> dt_mes_competencia_w) then
		wheb_mensagem_pck.exibir_mensagem_abort( 267022, null); /* The dates from the reference month and protocol aren't within the same month. 
									(Parameter [20] - Require the protocol date and the reference date to be within the same month) */
	end if;
end if;

if	(dt_protocolo_w	> dt_mes_competencia_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(345369,null); /* The date/month of the protocol is bigger than the competency date of the protocol!*/
end if;

/* Check if there already is an accounting batch of the type HPMS - Expense - Reimbursement for the actual month of competency */
dt_ref_inicial_w	:= trunc(trunc(dt_mes_competencia_p,'month'),'dd');
dt_ref_final_w		:= fim_dia(fim_mes(dt_ref_inicial_w));

select	max(b.ds_tipo_lote_contabil)
into	ds_tipo_lote_contabil_w
from	lote_contabil		a,
	tipo_lote_contabil	b
where	a.cd_tipo_lote_contabil	= b.cd_tipo_lote_contabil
and	b.cd_tipo_lote_contabil	= 23 -- HPMS - Expenses - Reimbursement
and	a.dt_referencia between dt_ref_inicial_w and dt_ref_final_w
and	nvl(a.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
and	a.dt_geracao_lote is not null;

if	(ds_tipo_lote_contabil_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(345368, 'DS_TIPO_LOTE_CONTABIL=' || ds_tipo_lote_contabil_w || ';' || 'DT_REFERENCIA=' || to_char(dt_mes_competencia_p,'mm/yyyy'));
end if;

end pls_consistir_protocolo_reemb;
/