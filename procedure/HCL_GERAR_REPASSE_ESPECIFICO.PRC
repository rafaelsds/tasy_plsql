create or replace
procedure HCL_GERAR_REPASSE_ESPECIFICO(nr_repasse_terceiro_p	number) is

vl_repasse_w		number(15,4);
vl_rateio_w		number(15,4);
nr_sequencia_item_w	number(15);

begin

vl_repasse_w	:= obter_valor_repasse(nr_repasse_Terceiro_p, 'R');
vl_rateio_w	:= dividir_sem_round(dividir_sem_round(((vl_repasse_w-19200) * 45),100),2);

if	(nvl(vl_rateio_w, 0) > 0) then

	select	nvl(max(nr_sequencia_item),0) + 1
	into	nr_sequencia_item_w
	from	repasse_terceiro_item
	where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

	insert into repasse_terceiro_item
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		vl_repasse,
		nr_repasse_terceiro,
		nr_sequencia_item,
		cd_medico,
		ds_observacao)
	values	(repasse_terceiro_item_seq.nextval,
		sysdate,
		'Tasy',
		vl_rateio_w,
		nr_repasse_terceiro_p,
		nr_sequencia_item_w,
		726,
		'Repasse gerado pela regra de repasse específico. Procedure: HCL_GERAR_REPASSE_ESPECIFICO');

	select	nvl(max(nr_sequencia_item),0) + 1
	into	nr_sequencia_item_w
	from	repasse_terceiro_item
	where	nr_repasse_terceiro	= nr_repasse_terceiro_p;
	
	insert into repasse_terceiro_item
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		vl_repasse,
		nr_repasse_terceiro,
		nr_sequencia_item,
		cd_medico,
		ds_observacao)
	values	(repasse_terceiro_item_seq.nextval,
		sysdate,
		'Tasy',
		vl_rateio_w,
		nr_repasse_terceiro_p,
		nr_sequencia_item_w,
		22451,
		'Repasse gerado pela regra de repasse específico. Procedure: HCL_GERAR_REPASSE_ESPECIFICO');

end if;

commit;

end	HCL_GERAR_REPASSE_ESPECIFICO;
/