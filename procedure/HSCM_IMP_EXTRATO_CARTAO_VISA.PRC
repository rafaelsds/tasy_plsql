create or replace
procedure HSCM_IMP_EXTRATO_CARTAO_VISA(nr_seq_extrato_p	number,
				nm_usuario_p		varchar2) is
dt_processamento_w	date;
dt_inicial_w		date;
dt_final_w		date;
nr_extrato_w		number(7);
nr_resumo_w		varchar2(20);
cd_banco_w		number(4);
cd_agencia_w		varchar2(8);
cd_conta_w		varchar2(15);
nr_seq_conta_banco_w	number(10);
qt_cv_aceito_w		number(10);
qt_cv_rejeitado_w	number(10);
vl_bruto_w		number(15,2);
vl_comissao_w		number(15,2);
vl_rejeitado_w		number(15,2);
vl_liquido_w		number(15,2);
cd_estabelecimento_w	number(4);
vl_parcela_w		number(15,2);
dt_compra_w		date;
qt_parcelas_w		number(10);
nr_cartao_w		varchar2(20);		
nr_autorizacao_w	varchar2(40);
ds_comprovante_w	varchar2(100);
ds_rejeicao_w		varchar2(255);
nr_parcela_w		number(10);
nr_seq_extrato_res_w	number(10);
ie_tipo_registro_w	varchar2(2);
nr_sequencia_w		number(10);
dt_min_parcela_w	date;
dt_max_parcela_w	date;
nr_seq_bandeira_w	number(10);
ie_status_w		varchar2(2);

/* Cursor Resumos */
Cursor c01 is
select	nr_sequencia, 
	substr(ds_conteudo,1,1) ie_tipo_registro,
	substr(ds_conteudo,37,2) ie_status,
	substr(ds_conteudo,12,7) nr_resumo,
	somente_numero(substr(ds_conteudo,113,4)) cd_banco,
	somente_numero(substr(ds_conteudo,117,5)) cd_agencia,
	somente_numero(substr(ds_conteudo,122,14)) cd_conta,
	substr(ds_conteudo,136,6) qt_cv_aceito,
	substr(ds_conteudo,143,6) qt_cv_rejeitado,
	decode(substr(ds_conteudo,57,1),'-',-1,1) * to_number(substr(ds_conteudo,58,11) || ',' || substr(ds_conteudo,69,2)) vl_bruto,
	decode(substr(ds_conteudo,71,1),'-',-1,1) * to_number(substr(ds_conteudo,72,11) || ',' || substr(ds_conteudo,83,2)) vl_comissao, 
	decode(substr(ds_conteudo,85,1),'-',-1,1) * to_number(substr(ds_conteudo,86,11) || ',' || substr(ds_conteudo,97,2)) vl_rejeitado, 
	decode(substr(ds_conteudo,99,1),'-',-1,1) * to_number(substr(ds_conteudo,100,11) || ',' || substr(ds_conteudo,111,2)) vl_liquido,
	to_number(null) vl_parcela,
	to_date(substr(ds_conteudo,45,6),'yymmdd') dt_compra,
	to_number(null) nr_parcela,
	to_number(null) qt_parcelas,
	to_char(null) nr_cartao,
	to_char(null) nr_autorizacao,
	to_char(null) ds_comprovante,
	to_char(null) ds_rejeicao
from	w_extrato_cartao_cr
where	nr_seq_extrato			= nr_seq_extrato_p
and	substr(ds_conteudo,1,1) 	= '1' -- Resumo
union all
select	nr_sequencia,
	substr(ds_conteudo,1,1) ie_tipo_registro,
	to_char(null) ie_status,
	to_char(null) nr_resumo,
	to_number(null) cd_banco,
	to_number(null) cd_agencia,
	to_number(null) cd_conta,
	to_char(null) qt_cv_aceito,
	to_char(null) qt_cv_rejeitado,
	to_number(null) vl_bruto,
	to_number(null) vl_comissao, 
	to_number(null) vl_rejeitado, 
	to_number(null) vl_liquido,	
	to_number(decode(substr(ds_conteudo,46,1),'-',-1,1) * to_number(substr(ds_conteudo,47,11) || ',' || substr(ds_conteudo,58,2))) vl_parcela,
	to_date(substr(ds_conteudo,38,8),'yyyymmdd') dt_compra,
	to_number(substr(ds_conteudo,60,2)) nr_parcela,
	somente_numero(substr(ds_conteudo,62,2)) qt_parcelas,
	substr(ds_conteudo,19,19) nr_cartao,
	substr(ds_conteudo,94,6) nr_autorizacao,
	substr(ds_conteudo,140,6) ds_comprovante,
	substr(ds_conteudo,64,30) ds_rejeicao
from	w_extrato_cartao_cr
where	nr_seq_extrato			= nr_seq_extrato_p
and	substr(ds_conteudo,1,1) 	= '2' --Detalhe
order by nr_sequencia;

begin
select	cd_estabelecimento,
	nr_seq_bandeira
into	cd_estabelecimento_w,
	nr_seq_bandeira_w
from	extrato_cartao_cr
where	nr_sequencia	= nr_seq_extrato_p;

/* Header */
begin
select	somente_numero(substr(ds_conteudo,39,7)) nr_extrato,
	to_date(substr(ds_conteudo,12,8),'yyyymmdd') dt_processamento,
	to_date(substr(ds_conteudo,20,8),'yyyymmdd') dt_inicial,
	to_date(substr(ds_conteudo,28,8),'yyyymmdd') dt_final
into	nr_extrato_w,
	dt_processamento_w,
	dt_inicial_w,
	dt_final_w
from	w_extrato_cartao_cr
where	nr_seq_extrato	= nr_seq_extrato_p
and	substr(ds_conteudo,1,1) = '0';
exception
when no_data_found then
	/* Arquivo n�o importado!
	Verifique se a interface 1340 - "Interface geral de importa��o extrato operadora cart�o (procedure)" est� ativa! */
	wheb_mensagem_pck.exibir_mensagem_abort(262126);
end;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	ie_tipo_registro_w,
	ie_status_w,
	nr_resumo_w,
	cd_banco_w,
	cd_agencia_w,
	cd_conta_w,
	qt_cv_aceito_w,
	qt_cv_rejeitado_w,
	vl_bruto_w,
	vl_comissao_w,
	vl_rejeitado_w,
	vl_liquido_w,
	vl_parcela_w,
	dt_compra_w,
	nr_parcela_w,
	qt_parcelas_w,
	nr_cartao_w,
	nr_autorizacao_w,
	ds_comprovante_w,
	ds_rejeicao_w;
exit when c01%notfound;
	/* Francisco - 03/03/2008 - S� importar o que for cr�dito, iclui o ie_debito no if abaixo */
	if	(ie_tipo_registro_w = '1') then
		
		/* Liquidacao */
		if	(ie_status_w = '14') then

			select	to_number(obter_valor_bandeira_estab(nr_sequencia,cd_estabelecimento_w,'NR_SEQ_CONTA_BANCO'))
			into	nr_seq_conta_banco_w
			from	bandeira_cartao_cr
			where	nr_sequencia	= nr_seq_bandeira_w;

			if	(nr_seq_conta_banco_w is null) then
				/* N�o foi encontrada a conta banc�ria de refer�ncia no cadastro da bandeira do extrato! */
				wheb_mensagem_pck.exibir_mensagem_abort(262127);
			end if;

			select	extrato_cartao_cr_res_seq.nextval
			into	nr_seq_extrato_res_w
			from	dual;

			insert	into	extrato_cartao_cr_res
				(nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nr_seq_extrato,
				nr_resumo,
				nr_seq_conta_banco,
				qt_cv_aceito,
				qt_cv_rejeitado,
				vl_bruto,
				vl_comissao,
				vl_rejeitado,
				vl_liquido,
				dt_prev_pagto)
			values	(nr_seq_extrato_res_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_extrato_p,
				nr_resumo_w,
				nr_seq_conta_banco_w,
				qt_cv_aceito_w,
				qt_cv_rejeitado_w,
				vl_bruto_w,
				vl_comissao_w,
				vl_rejeitado_w,
				vl_liquido_w,
				dt_compra_w);
				
		/* Aluguel de POS (Equipamento) */
		elsif	(ie_status_w = '12') then
		
			insert	into	extrato_cartao_cr_desp
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_extrato,
				nr_resumo,
				vl_bruto,
				vl_liquido,
				dt_prev_pagto)
			values	(extrato_cartao_cr_desp_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_extrato_p,
				nr_resumo_w,
				abs(vl_bruto_w),
				abs(vl_liquido_w),
				dt_compra_w);
		end if;

	/* Francisco - 03/03/2008 - S� importar o que for cr�dito, iclui o ie_debito no if abaixo */
	elsif	(ie_tipo_registro_w = '2')  then

		insert	into	extrato_cartao_cr_movto
			(nr_sequencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_atualizacao,
			nm_usuario,
			nr_seq_extrato,
			nr_seq_extrato_res,
			vl_parcela,
			qt_parcelas,
			nr_cartao,
			nr_autorizacao,
			ds_comprovante,
			ds_rejeicao,
			dt_compra,
			nr_parcela,
			ie_pagto_indevido,
			vl_saldo_concil_cred)
		values	(extrato_cartao_cr_movto_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_extrato_p,
			nr_seq_extrato_res_w,
			vl_parcela_w,
			qt_parcelas_w,
			nr_cartao_w,
			nr_autorizacao_w,
			ds_comprovante_w,
			ds_rejeicao_w,
			dt_compra_w,
			nr_parcela_w,
			'N',
			vl_parcela_w);
	end if;	
end loop;
close c01;

select	min(dt_compra),
	max(dt_compra)
into	dt_min_parcela_w,
	dt_max_parcela_w
from	extrato_cartao_cr_movto
where	nr_seq_extrato	=	nr_seq_extrato_p;  

update	extrato_cartao_cr
set	nr_extrato		= nr_extrato_w,
	dt_importacao		= sysdate,
	dt_processamento	= dt_processamento_w, 
	dt_inicial		= dt_min_parcela_w,
	dt_final		= dt_max_parcela_w
where	nr_sequencia		= nr_seq_extrato_p;

end HSCM_IMP_EXTRATO_CARTAO_VISA;
/