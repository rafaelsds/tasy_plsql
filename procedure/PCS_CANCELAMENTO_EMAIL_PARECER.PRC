create or replace
procedure pcs_cancelamento_email_parecer(	nr_seq_reclassif_item_p number,
											nr_seq_parecer_p	number,
											nm_usuario_p		Varchar2) is 

ds_mensagem_w			varchar2(4000);
ds_assunto_w			varchar2(255);
nm_usuario_origem_w		varchar2(255);
ds_email_origem_w		varchar2(255);
ds_email_w				varchar2(255);
ds_email_remetente_w	varchar2(255);
ds_parecer_w			varchar2(4000);
cd_pessoa_dest_w		varchar2(10);
cd_pessoa_grupo_w		varchar2(10);
nr_seq_parecer_w		number(10);
cd_material_w			number(10);
ds_material_w			varchar2(255);
ds_tipo_parecer_w		varchar2(255);
ds_destinatarios_w		varchar2(255);
nr_seq_grupo_w			number(10);
ie_comunic_interna_w	varchar2(1);
ie_email_w				varchar2(1);
nr_seq_comunic_w		number(10);
nm_usuario_dest_w		varchar2(255);
qt_selecionados_w		number(5);
nm_destinatarios_w		varchar2(4000);


Cursor C01 is
	select	cd_pessoa_grupo
	from  	pcs_pessoas_grupo_dest
	where 	nr_seq_grupo = nr_seq_grupo_w
	and		cd_pessoa_grupo <> cd_pessoa_dest_w
	and		cd_pessoa_grupo is not null
	group by cd_pessoa_grupo;	
			
begin

select	max(ds_email)
into	ds_email_remetente_w
from	usuario 
where	nm_usuario = nm_usuario_p;	

select  b.cd_pessoa_dest,
		b.nr_seq_grupo_dest
into	cd_pessoa_dest_w,
		nr_seq_grupo_w		
from	pcs_reclassif_curva_item a,
		pcs_parecer b
where	a.nr_sequencia = b.nr_seq_reclassif
and		a.nr_sequencia = nr_seq_reclassif_item_p
and		b.nr_sequencia = nr_seq_parecer_p
and 	b.cd_pessoa_dest is not null
and		((ie_comunic_interna = 'S') or (ie_email = 'S'));


select	max(ds_email) || ';',
		max(nm_usuario) || ','
into	ds_destinatarios_w,
		nm_usuario_dest_w
from	usuario 
where	cd_pessoa_fisica = cd_pessoa_dest_w;	

if (nm_usuario_dest_w <> '') then
	nm_destinatarios_w := nm_usuario_dest_w;
end if;

ds_mensagem_w := null;
ds_assunto_w := null;

open C01;
loop
fetch C01 into	
	cd_pessoa_grupo_w;
exit when C01%notfound;
	begin			
	
	select	max(ds_email),
			max(nm_usuario)
	into	ds_email_w,
			nm_usuario_dest_w
	from	usuario 
	where	cd_pessoa_fisica = cd_pessoa_grupo_w;	
			
	ds_destinatarios_w := ds_destinatarios_w || ds_email_w || ';';
	nm_destinatarios_w := nm_destinatarios_w || nm_usuario_dest_w || ';';
	--enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_remetente_w,ds_email_w,'Maicon','M');
	end;
end loop;
close C01;	


select  b.ds_parecer,
		a.cd_material,
		obter_desc_material(a.cd_material),
		b.nr_sequencia,
		substr(obter_descricao_padrao('PCS_TIPO_PARECER','DS_TIPO_PARECER',NR_SEQ_TIPO_PARECER),1,255),
		ie_comunic_interna,
		ie_email
into	ds_parecer_w,
		cd_material_w,
		ds_material_w,
		nr_seq_parecer_w,
		ds_tipo_parecer_w,
		ie_comunic_interna_w,
		ie_email_w
from	pcs_reclassif_curva_item a,
		pcs_parecer b
where	a.nr_sequencia = b.nr_seq_reclassif
and		a.nr_sequencia = nr_seq_reclassif_item_p
and		b.nr_sequencia = nr_seq_parecer_p
and		((ie_comunic_interna = 'S') or (ie_email = 'S'))
order by b.nr_sequencia;	

ds_assunto_w := substr(wheb_mensagem_pck.get_Texto(296106) || ' ' || cd_material_w || ' - ' || ds_material_w,1,255);		
ds_mensagem_w := ds_mensagem_w || chr(13) || chr(10) || nr_seq_parecer_w || ' - ' || ds_tipo_parecer_w;		
ds_mensagem_w := substr(ds_mensagem_w || chr(13) || chr(10) || ds_parecer_w || chr(13) || chr(10),1,4000);		
ds_email_w := null;

	-- Parecer do item : 		
if (nvl(ie_email_w,'N') = 'S') then
	enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_remetente_w,ds_destinatarios_w,nm_usuario_p,'M'); --coloquei o usuario maicon s� pra testar, por causa do SMTP
	--enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_remetente_w,'maurici@wheb.com.br','Maicon','M'); --coloquei o usuario maicon s� pra testar, por causa do SMTP
end if;

if (nvl(ie_comunic_interna_w,'N') = 'S') then
	
		select	comunic_interna_seq.nextval
		into	nr_seq_comunic_w
		from	dual;

		insert	into comunic_interna(
					dt_comunicado,	
					ds_titulo,	
					ds_comunicado,	
					nm_usuario,
					dt_atualizacao,	
					ie_geral,			
					nm_usuario_destino,	
					nr_sequencia, 
					ie_gerencial,					
					dt_liberacao)
			values(	sysdate,	
					ds_assunto_w,
					ds_mensagem_w,	
					nm_usuario_p,
					sysdate,	
					'N',				
					nm_destinatarios_w,		
					nr_seq_comunic_w,
					'N',
					sysdate);	
end if;

commit;

end pcs_cancelamento_email_parecer;
/
