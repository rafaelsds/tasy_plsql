create or replace
procedure pls_gerar_auditoria_guia
			(	nr_seq_guia_p		number,
				nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_observacao_w			varchar2(4000);
ds_indicacao_clinica_w		varchar2(4000);
cd_guia_w			varchar2(20);
nr_telefone_celular_w		varchar2(15);
cd_medico_solicitante_w		varchar2(10);
ie_tipo_despesa_w		varchar2(10);
ie_regra_preco_w		varchar2(3);
ie_tipo_guia_w			varchar2(2);
ie_status_w			varchar2(2);
ie_envia_email_aud_externo_w	varchar2(2);
ie_tipo_processo_w		varchar2(2);
ie_pacote_w			varchar2(1);
ie_utiliza_nivel_w		varchar2(1);
vl_material_w			number(15,2);
vl_procedimento_w		number(15,2);
vl_total_pacote_w		number(15,2);
nr_seq_prestador_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_auditoria_w		number(10);
nr_seq_regra_liberacao_w	number(10);
nr_seq_proc_guia_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_mat_guia_w		number(10);
nr_seq_material_w		number(10);
qt_solicitado_mat_w		number(10);
nr_seq_auditoria_item_w		number(10);
nr_seq_grupo_w			number(10);
nr_seq_fluxo_w			number(10);
nr_seq_ordem_atual_w		number(10);
nr_seq_prest_fornec_w		number(10);
cd_estabelecimento_w		number(10);
nr_seq_regra_preco_pac_w	number(10);
nr_seq_pacote_w			number(10);
ie_estagio_guia_w		number(10);
nr_seq_material_forn_w		number(10);
qt_solicitada_proc_w		number(7,3);
qt_grupo_w			number(5);
qt_fluxo_w			number(5);
qt_grupo_auditor_w		number(5);
qt_auditoria_w			number(3);
dt_solicitacao_w		date;
nr_seq_ocorrencia_w		number(10);
ie_auditoria_w			varchar2(1);
ie_anexo_quimioterapia_w	pls_auditoria.ie_anexo_quimioterapia%type;
ie_anexo_radioterapia_w		pls_auditoria.ie_anexo_radioterapia%type;
ie_anexo_opme_w			pls_auditoria.ie_anexo_opme%type;
ie_anexo_guia_w			pls_auditoria.ie_anexo_guia%type;
ie_pacote_ptu_w			pls_guia_plano_proc.ie_pacote_ptu%type;
ie_agrupa_grupo_aud_w		pls_param_analise_aut.ie_agrupa_grupo_aud%type;
ie_recem_nascido_w		pls_guia_plano.ie_recem_nascido%type;
nm_recem_nascido_w		pls_guia_plano.nm_recem_nascido%type;
dt_nasc_recem_nascido_w		pls_guia_plano.dt_nasc_recem_nascido%type;
ie_tipo_anexo_w			pls_auditoria_item.ie_tipo_anexo%type;

Cursor C01 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_solicitada,
		ie_status,
		vl_procedimento,
		vl_total_pacote,
		ie_pacote_ptu,
		ie_tipo_anexo
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p
	and	qt_solicitada 		is not null
	and	nr_seq_motivo_exc	is null;
	
Cursor C02 is
	select	nr_sequencia,
		nr_seq_material,
		qt_solicitada,
		ie_status,
		nr_seq_prest_fornec,
		vl_material,		
		nr_seq_material_forn,
		ie_tipo_anexo
	from	pls_guia_plano_mat
	where	nr_seq_guia		= nr_seq_guia_p
	and	nr_seq_motivo_exc	is null;

Cursor C05 is
	select	nr_seq_grupo,
		nr_seq_fluxo,
		nr_seq_ocorrencia
	from	pls_ocorrencia_grupo
	where	nr_seq_ocorrencia in(	select	nr_seq_ocorrencia
					from	pls_ocorrencia_benef
					where	nr_seq_guia_plano	= nr_seq_guia_p)
	and	ie_autorizacao	= 'S'
	and	ie_situacao	= 'A'
	group by nr_seq_fluxo, nr_seq_grupo, nr_seq_ocorrencia
	order by
		nr_seq_fluxo;
	
begin
if	(pls_obter_se_scs_ativo	<> 'A') then
	select	count(1)
	into	qt_auditoria_w
	from	pls_auditoria
	where	nr_seq_guia = nr_seq_guia_p;
elsif	(pls_obter_se_scs_ativo	= 'A') then
	select	count(1)
	into	qt_auditoria_w
	from	pls_auditoria
	where	nr_seq_guia 	= nr_seq_guia_p
	and	dt_liberacao	is null;
end if;

if	(qt_auditoria_w = 0) then
	select	cd_guia,
		nr_seq_prestador,
		nr_seq_segurado,
		ie_tipo_guia,
		dt_solicitacao,
		cd_medico_solicitante,
		ie_tipo_processo,
		ds_observacao,
		cd_estabelecimento,
		substr(pls_obter_dados_segurado(nr_seq_segurado, 'TCD'),0,15),
		ds_indicacao_clinica,
		ie_estagio,
		ie_anexo_quimioterapia,
		ie_anexo_radioterapia,
		ie_anexo_opme,
		ie_anexo_guia,
		ie_recem_nascido, 
		nm_recem_nascido,
		dt_nasc_recem_nascido
	into	cd_guia_w,
		nr_seq_prestador_w,
		nr_seq_segurado_w,
		ie_tipo_guia_w,
		dt_solicitacao_w,
		cd_medico_solicitante_w,
		ie_tipo_processo_w,
		ds_observacao_w,
		cd_estabelecimento_w,
		nr_telefone_celular_w,
		ds_indicacao_clinica_w,
		ie_estagio_guia_w,
		ie_anexo_quimioterapia_w,
		ie_anexo_radioterapia_w,
		ie_anexo_opme_w,
		ie_anexo_guia_w,
		ie_recem_nascido_w, 
		nm_recem_nascido_w,
		dt_nasc_recem_nascido_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_p;

	select 	pls_auditoria_seq.nextval
	into	nr_seq_auditoria_w
	from	dual;

	insert into pls_auditoria
		(nr_sequencia, dt_auditoria, dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nr_seq_guia, ie_tipo_auditoria, cd_guia, 
		nr_seq_prestador, nr_seq_segurado, ie_tipo_guia,
		dt_solicitacao,	cd_medico_solicitante, ie_status,
		ds_observacao, cd_estabelecimento, nr_telef_celular_benef,
		ds_indicacao_clinica, ie_anexo_quimioterapia, ie_anexo_radioterapia,
		ie_anexo_opme, ie_anexo_guia, ie_recem_nascido, 
		nm_recem_nascido, dt_nasc_recem_nascido, ie_retorno_justific, ie_tipo_processo)
	values	(nr_seq_auditoria_w, sysdate, sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		nr_seq_guia_p, 'G', cd_guia_w, 
		nr_seq_prestador_w, nr_seq_segurado_w, ie_tipo_guia_w, 
		dt_solicitacao_w, cd_medico_solicitante_w,'A',
		ds_observacao_w, cd_estabelecimento_w, nr_telefone_celular_w,
		ds_indicacao_clinica_w, ie_anexo_quimioterapia_w, ie_anexo_radioterapia_w,
		ie_anexo_opme_w, ie_anexo_guia_w, ie_recem_nascido_w, 
		nm_recem_nascido_w, dt_nasc_recem_nascido_w, 'N', ie_tipo_processo_w);
	
	
	nr_seq_grupo_w	:= null;
	nr_seq_fluxo_w	:= null;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_proc_guia_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_solicitada_proc_w,
		ie_status_w,
		vl_procedimento_w,
		vl_total_pacote_w,
		ie_pacote_ptu_w,
		ie_tipo_anexo_w;
	exit when C01%notfound;
		begin
		if	(ie_status_w in ('L','P','S')) then
			ie_status_w := 'A';
		elsif	(ie_status_w in ('A','I')) then
			ie_status_w := 'P';
		elsif	(ie_status_w in ('M','N')) then
			ie_status_w := 'N';
		end if;
		
		if	(ie_estagio_guia_w = 11) and
			(pls_obter_se_scs_ativo	= 'A') and
			(ie_status_w <> 'P') then
			goto final;
		end if;
		
		select	pls_auditoria_item_seq.nextval
		into	nr_seq_auditoria_item_w
		from	dual;
		
		select 	nvl(max(ie_classificacao), 1)
		into	ie_tipo_despesa_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w;

		if	(nr_seq_prestador_w > 0) then
			select	max(nr_sequencia)
			into	nr_seq_pacote_w
			from	pls_pacote
			where	nvl(nr_seq_prestador,nvl(nr_seq_prestador_w,0))	= nvl(nr_seq_prestador_w,0)
			and	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w
			and	ie_situacao		= 'A';
			
			/* Francisco - 16/05/2012 - OS 447352 */
			if	(nr_seq_pacote_w is not null) then
				select	nvl(ie_regra_preco,'N')
				into	ie_regra_preco_w
				from	pls_pacote a
				where	a.nr_sequencia	= nr_seq_pacote_w;
				
				if	(ie_regra_preco_w = 'S') then
					pls_obter_regra_preco_pacote(	cd_procedimento_w,
									ie_origem_proced_w,
									'G',
									nr_seq_proc_guia_w,
									nm_usuario_p,
									nr_seq_pacote_w,
									nr_seq_regra_preco_pac_w);
				end if;
			end if;
			
			if	(nr_seq_pacote_w is not null) then
				ie_tipo_despesa_w	:= '4';
			end if;
		end if;
			
		insert into pls_auditoria_item
			(nr_sequencia, nr_seq_auditoria, cd_procedimento,
			ie_origem_proced, qt_original, nr_seq_proc_origem,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, qt_ajuste, ie_status_solicitacao,
			ie_status, ie_tipo_despesa,vl_item,
			vl_total_pacote, ie_pacote_ptu, vl_original,
			ie_tipo_anexo)
		values	(nr_seq_auditoria_item_w, nr_seq_auditoria_w, cd_procedimento_w,
			ie_origem_proced_w, qt_solicitada_proc_w, nr_seq_proc_guia_w,
			sysdate, nm_usuario_p, sysdate,
			nm_usuario_p, qt_solicitada_proc_w, ie_status_w,
			ie_status_w, ie_tipo_despesa_w,vl_procedimento_w,
			vl_total_pacote_w, ie_pacote_ptu_w, vl_procedimento_w,
			ie_tipo_anexo_w);

		<<final>>
		nr_seq_auditoria_item_w	:= null;
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into	
		nr_seq_mat_guia_w,
		nr_seq_material_w,
		qt_solicitado_mat_w,
		ie_status_w,
		nr_seq_prest_fornec_w,
		vl_material_w,
		nr_seq_material_forn_w,
		ie_tipo_anexo_w;
	exit when C02%notfound;
		begin	
		if	(ie_status_w in ('L','P','S')) then
			ie_status_w := 'A';
		elsif	(ie_status_w in ('A','I')) then
			ie_status_w := 'P';
		elsif	(ie_status_w in ('M','N')) then
			ie_status_w := 'N';
		end if;
		
		if	(ie_estagio_guia_w = 11) and
			(pls_obter_se_scs_ativo	= 'A') and
			(ie_status_w <> 'P') then
			goto final;
		end if;
		
		select	pls_auditoria_item_seq.nextval
		into	nr_seq_auditoria_item_w
		from	dual;
		
		select 	max(ie_tipo_despesa) 
		into	ie_tipo_despesa_w
		from	pls_material
		where	nr_sequencia = nr_seq_material_w;
		
		insert into pls_auditoria_item
			(nr_sequencia, nr_seq_auditoria, qt_original,
			nr_seq_mat_origem, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, qt_ajuste,
			nr_seq_material, ie_status_solicitacao, ie_status,
			ie_tipo_despesa, nr_seq_prest_fornec, vl_item,
			nr_seq_material_forn, vl_original, ie_tipo_anexo)
		values	(nr_seq_auditoria_item_w, nr_seq_auditoria_w, qt_solicitado_mat_w,
			nr_seq_mat_guia_w, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, qt_solicitado_mat_w,
			nr_seq_material_w, ie_status_w, ie_status_w,
			ie_tipo_despesa_w, nr_seq_prest_fornec_w, vl_material_w,
			nr_seq_material_forn_w, vl_material_w, ie_tipo_anexo_w);

		<<final>>
		nr_seq_auditoria_item_w	:= null;
		end;
	end loop;
	close C02;
	
	if	(ie_estagio_guia_w = 11) and
		(pls_obter_se_scs_ativo = 'A') then
		goto final2;
	else
		begin		
			select	nvl(ie_agrupa_grupo_aud, 'S')
			into	ie_agrupa_grupo_aud_w
			from	pls_param_analise_aut;
		exception
		when others then
			ie_agrupa_grupo_aud_w	:= 'S';
		end;

		open C05;
		loop
		fetch C05 into	
			nr_seq_grupo_w,
			nr_seq_fluxo_w,
			nr_seq_ocorrencia_w;
		exit when C05%notfound;
			begin
			
			begin
				select	ie_auditoria
				into	ie_auditoria_w
				from	pls_ocorrencia
				where	nr_sequencia	= nr_seq_ocorrencia_w;
			exception
			when others then
				ie_auditoria_w	:= 'N';
			end;
			
			if	(ie_auditoria_w	= 'S') then
				if	(ie_agrupa_grupo_aud_w = 'S') then
					select	count(1)
					into	qt_grupo_w
					from	pls_auditoria_grupo
					where	nr_seq_auditoria 	= nr_seq_auditoria_w
					and	nr_seq_grupo 		= nr_seq_grupo_w;
					
					if	(qt_grupo_w = 0) then
						select	count(1)
						into	qt_fluxo_w
						from	pls_auditoria_grupo
						where	nr_seq_auditoria 	= nr_seq_auditoria_w
						and	nr_seq_ordem 		= nr_seq_fluxo_w;
						
						if	(qt_fluxo_w	> 0) then
							select	max(nr_seq_ordem) + 1
							into	nr_seq_ordem_atual_w
							from	pls_auditoria_grupo
							where	nr_seq_auditoria 	= nr_seq_auditoria_w;
							
							nr_seq_fluxo_w	:= nr_seq_ordem_atual_w;
						end if;
						
						insert into pls_auditoria_grupo
							(nr_sequencia, nr_seq_auditoria, nr_seq_grupo,
							dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
							nm_usuario_nrec, nr_seq_ordem, ie_status,
							ie_manual)
						values	(pls_auditoria_grupo_seq.nextval, nr_seq_auditoria_w, nr_seq_grupo_w,
							sysdate, nm_usuario_p, sysdate,
							nm_usuario_p, nr_seq_fluxo_w, 'U',
							'N');
					end if;
				elsif	(ie_agrupa_grupo_aud_w = 'N') then
					select	count(1)
					into	qt_fluxo_w
					from	pls_auditoria_grupo
					where	nr_seq_auditoria 	= nr_seq_auditoria_w
					and	nr_seq_ordem 		= nr_seq_fluxo_w;
					
					if	(qt_fluxo_w	> 0) then
						select	max(nr_seq_ordem) + 1
						into	nr_seq_ordem_atual_w
						from	pls_auditoria_grupo
						where	nr_seq_auditoria 	= nr_seq_auditoria_w;
						
						nr_seq_fluxo_w	:= nr_seq_ordem_atual_w;
					end if;
					
					insert into pls_auditoria_grupo
						(nr_sequencia, nr_seq_auditoria, nr_seq_grupo,
						dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
						nm_usuario_nrec, nr_seq_ordem, ie_status,
						ie_manual)
					values	(pls_auditoria_grupo_seq.nextval, nr_seq_auditoria_w, nr_seq_grupo_w,
						sysdate, nm_usuario_p, sysdate,
						nm_usuario_p, nr_seq_fluxo_w, 'U',
						'N');
				end if;
			end if;
			end;
		end loop;
		close C05;
	end if;
	
	<<final2>>
	
	commit;
	/* OS - 308929 - Devera sempre rodar essa rotina ap�s gerar os grupos, para eliminar os grupos conforme a regra */
	pls_organizar_grupos_nivel_lib(nr_seq_auditoria_w,nm_usuario_p);
	
	/* OS - 421626 - Rotina para gerar as ocorr�ncias e glosas na tabela "pls_analise_ocor_glosa_aut" */
	ie_utiliza_nivel_w := pls_obter_se_uti_nivel_lib_aut(cd_estabelecimento_w); 
	if	(ie_utiliza_nivel_w = 'S') then
		pls_gerar_ocorr_glosa_aud_aut(nr_seq_auditoria_w,nm_usuario_p);
	end if;
	
	ie_envia_email_aud_externo_w	:= obter_valor_param_usuario(1270, 12, Obter_Perfil_Ativo, nm_usuario_p, 0);
	
	select	count(1)
	into	qt_grupo_auditor_w
	from	pls_auditoria_grupo
	where	nr_seq_auditoria = nr_seq_auditoria_w;
	
	if	(ie_tipo_processo_w <> 'P') and 
		(ie_envia_email_aud_externo_w = 'S')  and
		(qt_grupo_auditor_w > 0) then
		pls_gerar_email_aud_externo(nr_seq_auditoria_w, nm_usuario_p);
	end if;
end if;

begin
	pls_gerar_alerta_evento(2, nr_seq_auditoria_w, null, null, nm_usuario_p);
exception
when others then
	null;
end;


end pls_gerar_auditoria_guia;
/
