create or replace
procedure pls_gerar_regra_alt_acomod
			(	nr_seq_segurado_p		number,
				nr_seq_plano_ant_p	number,
				dt_alteracao_p		date,
				ie_origem_alteracao_p	varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
				
/*ie_origem_alteracao_p
M - Migracao de contrato
A - Alteracao do produto
*/
	
ie_acomodacao_ant_w		varchar2(10);
ie_acomodacao_nova_w		varchar2(10);
nr_seq_plano_atual_w		number(10);
nr_seq_carencia_w		number(10);
nr_seq_regra_alt_carn_acom_w	number(10);
ie_carencia_especifica_acom_w	varchar2(10);
ie_acao_regra_w			varchar2(10);
dt_alteracao_carencia_w		date;
dt_inclusao_operadora_w		date;
cd_pessoa_fisica_w		varchar2(10);
dt_inicio_vigencia_ant_w	date;
qt_dias_w			number(10);
dt_contratacao_w		date;
nr_seq_plano_primeiro_seg_w	number(10);

Cursor C01 is
	select	b.nr_sequencia,
		b.dt_inicio_vigencia,
		b.qt_dias
	from	pls_carencia			b,
		pls_tipo_carencia		a
	where	b.nr_seq_tipo_carencia		= a.nr_sequencia
	and	b.nr_seq_segurado		= nr_seq_segurado_p
	and	b.ie_cpt			= 'N'
	and	a.ie_carencia_especifica_acomod	= ie_carencia_especifica_acom_w;

begin

select	max(nr_sequencia)
into	nr_seq_regra_alt_carn_acom_w
from	pls_regra_alt_caren_acomod
where	((ie_tipo_alteracao	= ie_origem_alteracao_p) or (ie_tipo_alteracao = 'T'));

if	(nr_seq_regra_alt_carn_acom_w is not null) then
	select	max(ie_acomodacao)
	into	ie_acomodacao_ant_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_ant_p;

	begin
	select	b.ie_acomodacao,
		pls_obter_produto_benef(a.nr_sequencia,dt_alteracao_p),
		--a.nr_seq_plano,
		a.dt_inclusao_operadora,
		a.cd_pessoa_fisica,
		a.dt_contratacao
	into	ie_acomodacao_nova_w,
		nr_seq_plano_atual_w,
		dt_inclusao_operadora_w,
		cd_pessoa_fisica_w,
		dt_contratacao_w
	from	pls_segurado	a,
		pls_plano	b
	where	a.nr_seq_plano	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_segurado_p;
	exception
	when others then
		goto final;
	end;

	if	(ie_acomodacao_ant_w <> ie_acomodacao_nova_w) then
		select	ie_carencia_especifica_acomd,
			ie_acao_regra
		into	ie_carencia_especifica_acom_w,
			ie_acao_regra_w
		from	pls_regra_alt_caren_acomod
		where	nr_sequencia	= nr_seq_regra_alt_carn_acom_w;
		
		if	(ie_acao_regra_w = '1') then
			if	(ie_origem_alteracao_p = 'A') then
				select	min(dt_alteracao)
				into	dt_alteracao_carencia_w
				from	pls_segurado_alt_plano
				where	nr_seq_segurado		= nr_seq_segurado_p
				and	ie_situacao		= 'A'
				and	nr_seq_plano_atual	= nr_seq_plano_atual_w;
				
				select	max(nr_seq_plano_ant)
				into	nr_seq_plano_primeiro_seg_w
				from	pls_segurado_alt_plano	
				where	nr_sequencia		= ( 	select 	min(nr_sequencia)
									from	pls_segurado_alt_plano
									where	nr_seq_segurado		= nr_seq_segurado_p
									and	ie_situacao = 'A');
				
				/*Caso nao houver registro de alteracao para esse produto antigo*/
				if	(dt_alteracao_carencia_w is null) then
					dt_alteracao_carencia_w	:= dt_alteracao_p;
				else
					if	(dt_contratacao_w < dt_alteracao_carencia_w) and
						(nr_seq_plano_primeiro_seg_w = nr_seq_plano_atual_w) then
						dt_alteracao_carencia_w	:= dt_contratacao_w;
					end if;
				end if;
			elsif	(ie_origem_alteracao_p = 'M') then
				select	min(dt_contratacao)
				into	dt_alteracao_carencia_w
				from	pls_segurado
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w
				and	pls_obter_produto_benef(nr_sequencia,dt_alteracao_p)	= nr_seq_plano_ant_p;
				--and	NR_SEQ_PLANO		= nr_seq_plano_ant_p;
			end if;
		elsif	(ie_acao_regra_w = '2') then
			dt_alteracao_carencia_w	:= dt_alteracao_p;
		elsif	(ie_acao_regra_w = '3') then
			dt_alteracao_carencia_w	:= dt_inclusao_operadora_w;
		end if;
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_carencia_w,
			dt_inicio_vigencia_ant_w,
			qt_dias_w;
		exit when C01%notfound;
			begin
			
			update	pls_carencia
			set	dt_inicio_vigencia	= dt_alteracao_carencia_w,
				dt_fim_vigencia		= dt_alteracao_carencia_w + qt_dias_w,
				nr_seq_regra_alt_acomod	= nr_seq_regra_alt_carn_acom_w
			where	nr_sequencia		= nr_seq_carencia_w;
			
			insert into pls_log_carencia_seg
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					nr_seq_segurado,nr_seq_plano,nr_seq_regra_alt_acomod,nr_seq_carencia,dt_iniciio_vigencia,
					qt_dias)
			values	(	pls_log_carencia_seg_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					nr_seq_segurado_p,nr_seq_plano_atual_w,nr_seq_regra_alt_carn_acom_w,nr_seq_carencia_w,dt_inicio_vigencia_ant_w,
					qt_dias_w);
			
			end;
		end loop;
		close C01;
	end if;
end if;

<<final>>
dt_alteracao_carencia_w	:= null;

end pls_gerar_regra_alt_acomod;
/
