create or replace
procedure FECHAR_SALDO_DIARIO_BANCO (
			nr_seq_banco_saldo_p	number,
			dt_referencia_p		date,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is 

nr_seq_conta_banco_w	banco_saldo.nr_seq_conta%type;
dt_inicial_w		date;
dt_final_w		date;
dt_fechamento_w		fechamento_banco.dt_fechamento%type;
vl_saldo_emprestimo_w	fechamento_banco.vl_saldo_emprestimo%type;
vl_emprestimo_w		movto_trans_financ.vl_transacao%type;

begin

gerar_lancamento_banco(nr_seq_banco_saldo_p,nm_usuario_p,'N','DB',dt_referencia_p);

select	max(a.nr_seq_conta)
into	nr_seq_conta_banco_w
from	banco_saldo a
where	a.nr_sequencia = nr_seq_banco_saldo_p;

/* atualizar o saldo de empréstimo */

select	max(a.dt_fechamento)
into	dt_fechamento_w
from	fechamento_banco a
where	a.nr_seq_conta_banco	= nr_seq_conta_banco_w
and	nvl(a.cd_estabelecimento,nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0);

select	max(a.vl_saldo_emprestimo)
into	vl_saldo_emprestimo_w
from	fechamento_banco a
where	a.dt_fechamento		= dt_fechamento_w
and	a.nr_seq_conta_banco	= nr_seq_conta_banco_w
and	nvl(a.cd_estabelecimento,nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0);

dt_inicial_w	:= trunc(dt_referencia_p,'dd');
dt_final_w	:= fim_dia(dt_referencia_p);

select	sum(decode(nvl(b.ie_emprestimo,'N'),'S',a.vl_transacao,'D',a.vl_transacao * -1,0))
into	vl_emprestimo_w
from	transacao_financeira b,
	movto_trans_financ a
where	a.nr_seq_trans_financ	= b.nr_sequencia
and	a.dt_transacao		between dt_inicial_w and dt_final_w
and	a.nr_seq_saldo_banco	= nr_seq_banco_saldo_p;

/* FIM - atualizar o saldo de empréstimo */

insert into fechamento_banco (
	nr_sequencia,
	nm_usuario,
	nm_usuario_nrec,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nr_seq_conta_banco,
	dt_fechamento,
	cd_estabelecimento,
	vl_saldo_emprestimo)
values (fechamento_banco_seq.nextval,
	nm_usuario_p,
	nm_usuario_p,
	sysdate,
	sysdate,
	nr_seq_conta_banco_w,
	dt_referencia_p,
	cd_estabelecimento_p,
	nvl(vl_saldo_emprestimo_w,0) + nvl(vl_emprestimo_w,0));

commit;

end FECHAR_SALDO_DIARIO_BANCO;
/