create or replace
procedure man_baixar_doc_tranfs(
	nr_sequencia_p		number,
	nm_usuario_p		Varchar2) is 

begin

update	man_doc_transferencia
set	dt_transferencia = sysdate, 
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_sequencia_p;

commit;

end man_baixar_doc_tranfs;
/