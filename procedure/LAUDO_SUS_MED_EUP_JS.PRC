create or replace
procedure laudo_sus_med_eup_js(
			cd_estabelecimento_p	number,
			nr_atendimento_p	number,
			cd_procedimento_p	number,
			cd_convenio_conta_p	out number,
			cd_convenio_parametro_p	out number,
			ie_cid_secundario_p	out varchar2) is 

cd_convenio_conta_w	number(5);
cd_convenio_param_w	number(5);
ie_cid_sec_w		varchar2(1);
			
begin

if	(nvl(nr_atendimento_p,0) > 0) then
	select	max(cd_convenio_parametro) 
	into	cd_convenio_conta_w
	from	conta_paciente
	where	nr_atendimento = nr_atendimento_p;

end if;

if	(nvl(cd_estabelecimento_p,1) > 0) then
	select	max(cd_convenio_sus)
	into	cd_convenio_param_w
	from	parametro_faturamento
	where	cd_estabelecimento = cd_estabelecimento_p;
end if;

ie_cid_sec_w :=	sus_obter_se_proc_cid_sec(cd_procedimento_p,7);

cd_convenio_conta_p	:= cd_convenio_conta_w;
cd_convenio_parametro_p	:= cd_convenio_param_w;
ie_cid_secundario_p	:= ie_cid_sec_w;

	
commit;

end laudo_sus_med_eup_js;
/