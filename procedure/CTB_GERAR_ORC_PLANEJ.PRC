create or replace procedure ctb_gerar_orc_planej(   nr_seq_planej_p ctb_orc_cenario.nr_sequencia%type,
                                                    nm_usuario_p varchar2) is

begin

ctb_gerar_orc_capex(nr_seq_planej_p, nm_usuario_p);

ctb_gerar_orc_resultado(nr_seq_planej_p, nm_usuario_p);

ctb_gerar_planej_orc_rec(nr_seq_planej_p, nm_usuario_p);

ctb_gerar_planej_orc_gpi(nr_seq_planej_p, nm_usuario_p);

update ctb_orc_cenario a
set    a.dt_geracao_orc = sysdate,
       a.nm_usuario     = nm_usuario_p
where  a.nr_sequencia   = nr_seq_planej_p;

commit;

end ctb_gerar_orc_planej;
/
