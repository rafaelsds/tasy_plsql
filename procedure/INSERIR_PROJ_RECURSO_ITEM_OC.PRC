create or replace
procedure inserir_proj_recurso_item_oc(	nr_ordem_compra_p			number,
				nr_seq_proj_rec_p				number,
				nr_seq_conta_bco_p			number,
				nm_usuario_p				Varchar2) is 

nr_item_oci_w			number(5);					
ds_lista_itens_w			varchar2(2000);
ds_historico_w			varchar2(2000);
ds_projeto_w			varchar2(80);
nr_seq_conta_bco_w		number(10);
ds_conta_bancaria_w		varchar2(255);

cursor c01 is
select	nr_item_oci
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
order by	nr_item_oci;
					
begin

update	ordem_compra_item
set	nr_seq_proj_rec	= nr_seq_proj_rec_p,
	nr_seq_conta_bco	= decode(nr_seq_conta_bco_p,0,null,nr_seq_conta_bco_p)
where	nr_ordem_compra	= nr_ordem_compra_p;

open C01;
loop
fetch C01 into	
	nr_item_oci_w;
exit when C01%notfound;
	begin
	if	(ds_lista_itens_w is null) then
		ds_lista_itens_w	:= nr_item_oci_w;
	else
		ds_lista_itens_w	:= substr(ds_lista_itens_w || ',' || nr_item_oci_w,1,2000);
	end if;
	end;
end loop;
close C01;

select	ds_projeto
into	ds_projeto_w
from	projeto_recurso
where	nr_sequencia = nr_seq_proj_rec_p;

if	(nvl(nr_seq_conta_bco_p,0) = 0) then
	ds_historico_w := WHEB_MENSAGEM_PCK.get_texto(303105,'DS_LISTA_ITENS_W='|| DS_LISTA_ITENS_W ||';DS_PROJETO_W='|| DS_PROJETO_W);
			/*Alterado o projeto recurso dos itens (#@DS_LISTA_ITENS_W#@) para #@DS_PROJETO_W#@.*/

else
	select	ds_conta
	into	ds_conta_bancaria_w
	from	banco_estabelecimento_v
	where	nr_sequencia = nr_seq_conta_bco_p;
	
	ds_historico_w := WHEB_MENSAGEM_PCK.get_texto(303106,'DS_LISTA_ITENS_W='|| DS_LISTA_ITENS_W ||';DS_PROJETO_W='|| DS_PROJETO_W ||';DS_CONTA_BANCARIA_W='|| DS_CONTA_BANCARIA_W);
				/*Alterado nos itens (#@DS_LISTA_ITENS_W#@) o projeto recurso para #@DS_PROJETO_W#@ e tamb�m a conta banc�ria para #@DS_CONTA_BANCARIA_W#@.*/

end if;

inserir_historico_ordem_compra(	nr_ordem_compra_p,
				'S',
				Wheb_mensagem_pck.get_Texto(303107), /*'Altera��o no projeto recurso dos itens.',*/
				ds_historico_w,
				nm_usuario_p);

commit;

end inserir_proj_recurso_item_oc;
/
