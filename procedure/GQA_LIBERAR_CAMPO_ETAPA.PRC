create or replace procedure gqa_liberar_campo_etapa (
  nr_seq_etapa_p      gqa_protocolo_etapa_pac.nr_sequencia%type,
  forcar              varchar2
) is

nm_usuario_w              gqa_protocolo_pac.nm_usuario%type;

nr_seq_score_flex_ii_w    gqa_protocolo_etapa_pac.nr_seq_score_flex_ii%type;
nr_seq_avaliacao_w        gqa_protocolo_etapa_pac.nr_seq_avaliacao%type;
nr_seq_evolucao_w         gqa_protocolo_etapa_pac.nr_seq_evolucao%type;
nr_seq_etapa_w            gqa_protocolo_etapa_pac.nr_seq_etapa%type;
nr_seq_acao_w             gqa_protocolo_etapa_pac.nr_seq_acao%type;

ie_tipo_acao_w            gqa_acao.ie_tipo_acao%type;
ie_permite_alterar_w      gqa_pendencia_regra.ie_permite_alterar%type;

begin
  select obter_usuario_ativo into nm_usuario_w from dual;

  select
          nr_seq_etapa,
          nr_seq_acao,
          nr_seq_score_flex_ii,
          nr_seq_avaliacao,
          nr_seq_evolucao
  into
          nr_seq_etapa_w,
          nr_seq_acao_w,
          nr_seq_score_flex_ii_w,
          nr_seq_avaliacao_w,
          nr_seq_evolucao_w
  from gqa_protocolo_etapa_pac
  where nr_sequencia = nr_seq_etapa_p;

  select ie_permite_alterar into ie_permite_alterar_w from gqa_pendencia_regra where nr_sequencia = nr_seq_etapa_w;
  
  if (ie_permite_alterar_w = 'N' or forcar = 'S') then
    select ie_tipo_acao into ie_tipo_acao_w from gqa_acao where nr_sequencia = nr_seq_acao_w;
  
    if(ie_tipo_acao_w = 'ES') then
      update escala_eif_ii set dt_liberacao = sysdate where nr_sequencia = nr_seq_score_flex_ii_w and dt_liberacao is null;
    elsif(ie_tipo_acao_w = 'AV') then
      update med_avaliacao_paciente set dt_liberacao = sysdate, nm_usuario_lib = nm_usuario_w where nr_sequencia = nr_seq_avaliacao_w and dt_liberacao is null;
    elsif(ie_tipo_acao_w = 'NC') then
      update evolucao_paciente set dt_liberacao = sysdate where cd_evolucao = nr_seq_evolucao_w and dt_liberacao is null;
    end if;
  end if;

end gqa_liberar_campo_etapa;
/
