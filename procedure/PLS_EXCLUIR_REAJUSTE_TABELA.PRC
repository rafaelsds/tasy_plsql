create or replace
procedure pls_excluir_reajuste_tabela
			(	nr_seq_reajuste_p 	number,
				dt_mes_inicio_p		date,
				dt_mes_final_p		date,
				ie_opcao_p		number,
				nm_usuario_p		varchar2) is 

/*
	1 - Fim dia
	2 - M�s
	3- Ano
	4 - Reajuste
*/

begin

if	(ie_opcao_p	= 1) then
	delete from pls_reajuste_tabela 
	where	nr_seq_reajuste	= nr_seq_reajuste_p 
	and trunc(dt_inicio_vigencia)  between trunc(dt_mes_inicio_p) and trunc(dt_mes_final_p);
elsif	(ie_opcao_p	= 2) then
	delete from pls_reajuste_tabela
	where	nr_seq_reajuste	= nr_seq_reajuste_p 
	and trunc(dt_inicio_vigencia, 'month') = trunc(dt_mes_final_p, 'month');
elsif	(ie_opcao_p	= 3) then
	delete from pls_reajuste_tabela 
	where	nr_seq_reajuste	= nr_seq_reajuste_p 
	and trunc(dt_inicio_vigencia, 'yyyy') = trunc(dt_mes_inicio_p, 'yyyy');
elsif	(ie_opcao_p	= 4) then
	delete from pls_reajuste_tabela 
	where	nr_seq_reajuste	= nr_seq_reajuste_p;
	
	pls_excluir_reaj_copartic(nr_seq_reajuste_p);
	pls_excluir_reaj_inscricao(nr_seq_reajuste_p);
end if;

commit;	

end pls_excluir_reajuste_tabela;
/