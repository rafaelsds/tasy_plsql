create or replace
procedure consiste_sus_compat_proc_cir (nr_seq_agenda_p		in	number,
					nr_cirurgia_p		in	number,
					cd_convenio_p		in	number,
					cd_procedimento_p 	in	number,
					ie_origem_proced_p	in	number,
					ds_retorno_p		out	varchar) 
					as

cd_procedimento_adic_w	number(15);	
ie_origem_proced_w	number(10);
ie_permite_w		varchar2(15);				
					
cursor	c01 is
	select	cd_procedimento,
		nvl(ie_origem_proced,ie_origem_proced_p)
	from	agenda_paciente_proc
	where	nr_sequencia = nr_seq_agenda_p
	and	nvl(nr_seq_agenda_p,0) > 0
	and	((cd_convenio is null) or (obter_tipo_convenio(cd_convenio) = '3'))
	union
	select	a.cd_procedimento,
		nvl(a.ie_origem_proced,ie_origem_proced_p)
	from	prescr_procedimento a,
		cirurgia b
	where	a.nr_prescricao = b.nr_prescricao
	and	b.nr_cirurgia	= nr_cirurgia_p
	and	(nvl(nr_cirurgia_p,0) > 0)
	and	((a.cd_convenio is null) or (obter_tipo_convenio(a.cd_convenio) = '3'));
	
	

begin
ds_retorno_p := null;
if	(cd_convenio_p is not null) and	(obter_tipo_convenio(cd_convenio_p) = 3) then
	open C01;
	loop
	fetch C01 into	
		cd_procedimento_adic_w,
		ie_origem_proced_w;
	exit when C01%notfound;
		begin
		--Verifica se os procedimentos adicionais s�o compat�veis com o principal
		if	(ie_origem_proced_w = 7) and (ie_origem_proced_p = 7) then
			select	substr(nvl(max(sus_verifica_se_compat_proc(cd_procedimento_p,ie_origem_proced_p,cd_procedimento_adic_w)),'S'),1,15)
			into	ie_permite_w
			from	dual;
			if	(ie_permite_w = 'N') then
				
				--N�o � poss�vel alterar o conv�nio para SUS. Os procedimentos adicionais s�o incompat�veis com este procedimento. 
				--� necess�rio realizar a exclus�o dos procedimentos adicionais para realizar a troca do conv�nio/procedimento�.
				
				ds_retorno_p := wheb_mensagem_pck.get_texto(338965, null);
			end if;
		end if;	
		
		--Verifica se procedimento principal � compat�vel com todos os procedimentos adicionais
		if	(ds_retorno_p is null) and (ie_origem_proced_w = 7) and (ie_origem_proced_p = 7) then
			select	substr(nvl(max(sus_verifica_se_compat_proc(cd_procedimento_adic_w,ie_origem_proced_w,cd_procedimento_p)),'S'),1,15)
			into	ie_permite_w
			from	dual;
			
			if	(ie_permite_w = 'N') then
				
				--N�o � poss�vel alterar o conv�nio para SUS. Os procedimentos adicionais s�o incompat�veis com este procedimento. 
				--� necess�rio realizar a exclus�o dos procedimentos adicionais para realizar a troca do conv�nio/procedimento�.
				
				ds_retorno_p := wheb_mensagem_pck.get_texto(338965, null);
			end if;
		end if;	
		end;
	end loop;
	close C01;
end if;	

end consiste_sus_compat_proc_cir;
/