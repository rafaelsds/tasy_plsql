create or replace procedure gerar_regulacao_atend(	nr_sequencia_p				number,
									ie_tipo_p 					varchar2,
									ie_informacao_p 			varchar2,
									nr_seq_informacao_p			number default null,
									cd_evolucao_p				number default null,
									nr_seq_parecer_p			number default null,
									nr_seq_resp_parecer_p		number default null,
									nr_seq_grupo_regulacao_p	number default null,
									nr_seq_reg_atend_p	out number) is


regulacao_atend_w 			regulacao_atend%rowtype;
nr_seq_grupo_regulacao_w	regra_regulacao.nr_seq_grupo_regulacao%type;
nr_seq_prioridade_w			regulacao_atend.nr_seq_prioridade%type;
reg_integracao_w			gerar_int_padrao.reg_integracao;
ie_classificacao_w			paciente_home_care.ie_classificacao%type;
nr_seq_equip_control_w		hc_pac_equipamento.nr_seq_equip_control%type;

nr_seq_exame_cad_w		number(10);
nr_seq_exame_w			number(10);
cd_material_exame_w		varchar2(20);
cd_procedimento_w		number(15);
nr_seq_proc_interno_w	number(10);
ie_origem_proced_w		number(10);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_cpoe_w			number(10);
qt_reg_w				number(10);
cd_convenio_w			number(10);
nr_seq_requisicao_w		number(10);
cd_cpf_medico_w			varchar2(20);
nr_cpf_pf_w				varchar2(20);
nr_seq_segurado_w		number(10);
dt_nasc_benef_w			date;
ie_sexo_benef_w			varchar2(1);
cd_nacionalidade_w		varchar2(10);
nm_beneficiario_w		varchar2(255);
nm_mae_benef_w			varchar2(255);
cd_prestador_w			varchar2(30);
cd_cgc_w				varchar2(15);
ie_integracao_w			varchar2(1);
ds_parecer_w			varchar2(4000);
nr_parecer_w			number(10);
nr_seq_motivo_solic_ext_w	number(10);
cd_material_w			number(10);
cd_proc_envio_w			varchar2(255);
cd_mat_envio_w			varchar2(255);
ie_origem_proced_envio_w   number(10);
cd_tipo_solicitacao_w	varchar2(255);
cd_tipo_vaga_w			varchar2(255);
ds_retorno_integracao_w	varchar2(4000);
qt_exame_w				number(10);
nr_receita_amb_w  number(10);
qt_dose_w number(10);

ie_confirma_encaminhamento_w	varchar2(1);
ie_confirma_regra_w				varchar2(1);
nr_seq_fa_rec_farmacia_w	number(10);

begin

if (nr_sequencia_p is not null) then

  select	regulacao_atend_seq.nextval
  into		regulacao_atend_w.nr_sequencia
  from		dual;

  regulacao_atend_w.dt_atualizacao := sysdate;
  regulacao_atend_w.nm_usuario := wheb_usuario_pck.get_nm_usuario;
  regulacao_atend_w.dt_atualizacao_nrec := sysdate;  
  regulacao_atend_w.ie_situacao := 'A';
  regulacao_atend_w.ie_tipo := ie_tipo_p;
  regulacao_atend_w.dt_liberacao := sysdate;
  regulacao_atend_w.dt_inativacao := null;
  regulacao_atend_w.ds_justificativa := null;
  regulacao_atend_w.cd_evolucao_ori := cd_evolucao_p;
  regulacao_atend_w.nr_seq_parecer_ori := nr_seq_parecer_p;
  regulacao_atend_w.nr_seq_resp_parecer_ori := nr_seq_resp_parecer_p;
  regulacao_atend_w.nr_seq_grupo_regulacao	:= nr_seq_grupo_regulacao_p;


	if ( ie_tipo_p = 'EN') then

		select	max(nr_atendimento),
				max(cd_pessoa_fisica),
				max(cd_especialidade),
				max(cd_medico_dest)
		into	nr_atendimento_w,
				cd_pessoa_fisica_w,
				regulacao_atend_w.cd_especialidade,
				regulacao_atend_w.cd_profissional_dest
		from	atend_encaminhamento
		where	nr_sequencia = nr_sequencia_p;

		regulacao_atend_w.nr_seq_encaminhamento := nr_sequencia_p;

		select	obter_procedimento_regulacao(ie_tipo_p, regulacao_atend_w.cd_especialidade, regulacao_atend_w.cd_profissional_dest)
		into	cd_proc_envio_w
		from	dual;

		select	obter_procedimento_regulacao(ie_tipo_p, regulacao_atend_w.cd_especialidade, regulacao_atend_w.cd_profissional_dest,null,null,null,null,null,null,null,null,null,null,'I')
		into	ie_origem_proced_envio_w
		from	dual;

		select	Obter_conf_Regulacao(ie_tipo_p, regulacao_atend_w.cd_especialidade, regulacao_atend_w.cd_profissional_dest,null,null,null,null,null,null,null,null,null,null,'C')
		into	ie_confirma_regra_w
		from	dual;

		regulacao_atend_w.cd_procedimento_envio := cd_proc_envio_w;
		regulacao_atend_w.ie_origem_proced := ie_origem_proced_envio_w;
		regulacao_atend_w.qt_solicitado := 1;


	elsif ( ie_tipo_p = 'EV') then

		SELECT	MAX(a.nr_atendimento),
				MAX(a.cd_pessoa_fisica),
				MAX(a.cd_especialidade),
				MAX(a.cd_medico_parecer)
		into	nr_atendimento_w,
				cd_pessoa_fisica_w,
				regulacao_atend_w.cd_especialidade,
				regulacao_atend_w.cd_profissional_dest
		FROM	evolucao_paciente a
		WHERE	a.cd_evolucao = nr_sequencia_p;

		select	obter_procedimento_regulacao(ie_tipo_p, regulacao_atend_w.cd_especialidade, regulacao_atend_w.cd_profissional_dest)
		into	cd_proc_envio_w
		from	dual;

		select	obter_procedimento_regulacao(ie_tipo_p, regulacao_atend_w.cd_especialidade, regulacao_atend_w.cd_profissional_dest,null,null,null,null,null,null,null,null,null,null,'I')
		into	ie_origem_proced_envio_w
		from	dual;

		select	Obter_conf_Regulacao(ie_tipo_p, regulacao_atend_w.cd_especialidade, regulacao_atend_w.cd_profissional_dest,null,null,null,null,null,null,null,null,null,null,'C')
		into	ie_confirma_regra_w
		from	dual;

		regulacao_atend_w.cd_procedimento_envio := cd_proc_envio_w;
		regulacao_atend_w.ie_origem_proced := ie_origem_proced_envio_w;

	elsif ( ie_tipo_p = 'SE') then

		regulacao_atend_w.nr_seq_pedido := nr_sequencia_p;
		regulacao_atend_w.nr_seq_pedido_item := nr_seq_informacao_p;


		select	max(a.nr_seq_exame),
				max(a.nr_seq_exame_lab),
				max(a.cd_material_exame),
				max(a.cd_procedimento),
				max(nvl(a.nr_seq_proc_int_sus,a.nr_proc_interno)),
				max(a.ie_origem_proced),
				max(b.nr_atendimento),
				max(b.cd_pessoa_fisica),
				max(a.NR_SEQ_PRIORIDADE),
				max(a.qt_exame)
		into	nr_seq_exame_cad_w,
				nr_seq_exame_w,
				cd_material_exame_w,
				cd_procedimento_w,
				nr_seq_proc_interno_w,
				ie_origem_proced_w,
				nr_atendimento_w,
				cd_pessoa_fisica_w,
				nr_seq_prioridade_w,
				qt_exame_w
		from	pedido_exame_externo_item a,
				pedido_exame_externo b
		where	b.nr_sequencia = a.nr_seq_pedido
		and		a.nr_sequencia = nr_seq_informacao_p;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, cd_material_exame_w, nr_seq_exame_cad_w, ie_origem_proced_w,null,null,nr_seq_proc_interno_w,nr_seq_exame_w,cd_procedimento_w)
		into	cd_proc_envio_w
		from	dual;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, cd_material_exame_w, nr_seq_exame_cad_w, ie_origem_proced_w,null,null,nr_seq_proc_interno_w,nr_seq_exame_w,cd_procedimento_w,null,null,'I')
		into	ie_origem_proced_envio_w
		from	dual;

		select	Obter_conf_Regulacao(ie_tipo_p, null, null, cd_material_exame_w, nr_seq_exame_cad_w, ie_origem_proced_w,null,null,nr_seq_proc_interno_w,nr_seq_exame_w,cd_procedimento_w,null,null,'C')
		into	ie_confirma_regra_w
		from	dual;

		regulacao_atend_w.cd_procedimento_envio := nvl(cd_proc_envio_w,cd_procedimento_w);
		regulacao_atend_w.ie_origem_proced := nvl(nvl(ie_origem_proced_envio_w,ie_origem_proced_w),1);

		regulacao_atend_w.qt_solicitado := nvl(qt_exame_w,nvl(qt_dose_w,1));

		if ( ie_informacao_p =  'SEC' ) then

			regulacao_atend_w.nr_seq_exame_cad := 	nr_seq_exame_cad_w;

		elsif ( ie_informacao_p =  'SE' ) then

			regulacao_atend_w.nr_seq_exame 		:= 	nr_seq_exame_w;
			regulacao_atend_w.cd_material_exame 	:= 	cd_material_exame_w;
			regulacao_atend_w.ie_origem_proced 	:= 	 nvl(nvl(ie_origem_proced_envio_w,ie_origem_proced_w),1);

		elsif ( ie_informacao_p =  'SP' ) then

			regulacao_atend_w.cd_procedimento 		:= 	cd_procedimento_w;
			regulacao_atend_w.ie_origem_proced 		:= 	ie_origem_proced_w;

		elsif ( ie_informacao_p =  'SPI' ) then

			regulacao_atend_w.nr_seq_proc_interno 		:= 	nr_seq_proc_interno_w;


		end if;

	elsif ( ie_tipo_p = 'TC') then


		select	max(obter_pessoa_atendimento(a.nr_atendimento,'C')),
				max(a.nr_atendimento),
				max(a.nr_seq_motivo_solic_externa)
		into	cd_pessoa_fisica_w,
				nr_atendimento_w,
				nr_seq_motivo_solic_ext_w
		from	solic_transf_externa a
		where	a.nr_sequencia = nr_sequencia_p;

		regulacao_atend_w.nr_seq_solic_transf := nr_sequencia_p;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, nr_seq_motivo_solic_ext_w)
		into	cd_proc_envio_w
		from	dual;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, nr_seq_motivo_solic_ext_w,null, null, null, null, null,null,'I')
		into	ie_origem_proced_envio_w
		from	dual;

		select	Obter_conf_Regulacao(ie_tipo_p, null, null, null, null, null, nr_seq_motivo_solic_ext_w,null, null, null, null, null,null,'C')
		into	ie_confirma_regra_w
		from	dual;

		regulacao_atend_w.cd_procedimento_envio := cd_proc_envio_w;
		regulacao_atend_w.ie_origem_proced 	:= 	 nvl(nvl(ie_origem_proced_envio_w,ie_origem_proced_w),1);

	elsif ( ie_tipo_p = 'EQ') then

		if (ie_informacao_p = 'HC') then
		
			select 	max(b.cd_pessoa_fisica),
					max(b.nr_atendimento_origem),
					max(a.nr_seq_equip_control)
			into	cd_pessoa_fisica_w,
					nr_atendimento_w,
					nr_seq_equip_control_w
			from 	hc_pac_equipamento a,
					paciente_home_care b
			where 	a.nr_sequencia = nr_sequencia_p
					and a.nr_seq_paciente = b.nr_sequencia;

			regulacao_atend_w.nr_seq_equip_home := nr_sequencia_p;
									
			select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, null, null,'P',null,nr_seq_equip_control_w)
			into 	cd_proc_envio_w
			from	dual;

			select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, null, null, 'I',null,nr_seq_equip_control_w)
			into 	ie_origem_proced_envio_w
			from	dual;

			select	Obter_conf_Regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, null, null, 'C',null,nr_seq_equip_control_w)
			into 	ie_confirma_regra_w
			from	dual;
			
			regulacao_atend_w.cd_procedimento_envio := cd_proc_envio_w;
			regulacao_atend_w.ie_origem_proced 	:= 	 nvl(nvl(ie_origem_proced_envio_w,ie_origem_proced_w),1);
			
			
			
		else
			select	max(a.cd_pessoa_solicitante),
					max(a.nr_atendimento)
			into	cd_pessoa_fisica_w,
					nr_atendimento_w
			from	requisicao_item a
			where	a.nr_sequencia = nr_sequencia_p;

			regulacao_atend_w.nr_seq_requisicao_item := nr_sequencia_p;
		end if;

	elsif ( ie_tipo_p = 'ME') then
	
		select	max(a.cd_pessoa_fisica),
				max(a.nr_atendimento),
				max(a.cd_material),
        max(a.NR_SEQ_PRIORIDADE),
        nvl(max(a.NR_SEQ_RECEITA_AMB),0),
        max(a.qt_dose)
		into	cd_pessoa_fisica_w,
				nr_atendimento_w,
				cd_material_w,
        nr_seq_prioridade_w,
        nr_receita_amb_w,
        qt_dose_w
		from	cpoe_material a
		where	a.nr_sequencia = nr_sequencia_p;

		regulacao_atend_w.nr_seq_cpoe := nr_sequencia_p;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, null, cd_material_w)
		into	cd_mat_envio_w
		from	dual;

		select	Obter_conf_Regulacao(ie_tipo_p, null, null, null, null, null, null, cd_material_w, null, null, null, null, null,'C')
		into	ie_confirma_regra_w
		from	dual;
				 
		regulacao_atend_w.NR_SEQ_RECEITA_AMB := NR_RECEITA_AMB_W;
		regulacao_atend_w.cd_material 		 := cd_material_w;
		regulacao_atend_w.cd_material_envio	 := cd_mat_envio_w;
		
    regulacao_atend_w.qt_solicitado := nvl(qt_dose_w, 1);
    
	elsif ( ie_tipo_p =  'CPOE') then

		select	max(a.cd_pessoa_fisica),
				max(a.nr_atendimento),
				max(a.nr_seq_proc_interno),
				max(a.cd_material_exame)
		into	cd_pessoa_fisica_w,
				nr_atendimento_w,
				nr_seq_proc_interno_w,
				cd_material_exame_w
		from	cpoe_procedimento a
		where	a.nr_sequencia = nr_sequencia_p;

		regulacao_atend_w.ie_tipo := 'SE';
		regulacao_atend_w.nr_seq_cpoe_proc := nr_sequencia_p;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, cd_material_exame_w, null, null, null, null, nr_seq_proc_interno_w)
		into 	cd_proc_envio_w
		from	dual;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, cd_material_exame_w, null, null, null, null, nr_seq_proc_interno_w, null, null, null, null,'I')
		into 	ie_origem_proced_envio_w
		from	dual;

		select	Obter_conf_Regulacao(ie_tipo_p, null, null, cd_material_exame_w, null, null, null, null, nr_seq_proc_interno_w, null, null, null, null,'C')
		into 	ie_confirma_regra_w
		from	dual;

		regulacao_atend_w.cd_procedimento_envio := cd_proc_envio_w;
		regulacao_atend_w.ie_origem_proced 	:= 	 nvl(nvl(ie_origem_proced_envio_w,ie_origem_proced_w),1);

	elsif ( ie_tipo_p = 'SV') then

		select	max(ie_solicitacao),
				max(ie_tipo_vaga),
				max(cd_pessoa_fisica),
				max(nr_atendimento),
				max(nr_seq_prioridade)
		into	cd_tipo_solicitacao_w,
				cd_tipo_vaga_w,
				cd_pessoa_fisica_w,
				nr_atendimento_w,
				nr_seq_prioridade_w
		from	gestao_vaga
		where	nr_sequencia = nr_sequencia_p;

		regulacao_atend_w.nr_seq_gestao_vaga := nr_sequencia_p;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, cd_tipo_solicitacao_w, cd_tipo_vaga_w)
		into 	cd_proc_envio_w
		from	dual;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, cd_tipo_solicitacao_w, cd_tipo_vaga_w, 'I')
		into 	ie_origem_proced_envio_w
		from	dual;

		select	Obter_conf_Regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, cd_tipo_solicitacao_w, cd_tipo_vaga_w, 'C')
		into 	ie_confirma_regra_w
		from	dual;

		regulacao_atend_w.cd_procedimento_envio := cd_proc_envio_w;
		regulacao_atend_w.ie_origem_proced 	:= 	 nvl(nvl(ie_origem_proced_envio_w,ie_origem_proced_w),1);

	elsif ( ie_tipo_p = 'FA') then		
	
		select  max(b.nr_sequencia),
				max(b.cd_pessoa_fisica),
				max(a.cd_material),
				max(a.qt_dose),
				max(b.nr_atendimento),
				max(a.nr_seq_prioridade)
		into	nr_seq_fa_rec_farmacia_w,
				cd_pessoa_fisica_w,
				cd_material_w,
				qt_dose_w,
				nr_atendimento_w,
				nr_seq_prioridade_w
		from	fa_receita_farmacia_item a,
				fa_receita_farmacia b
		where 	a.nr_seq_receita = b.nr_sequencia
		and     a.nr_sequencia = nr_sequencia_p;
		
		select	obter_procedimento_regulacao('ME', null, null, null, null, null, null, cd_material_w)
		into	cd_mat_envio_w
		from	dual;

		select	Obter_conf_Regulacao('ME', null, null, null, null, null, null, cd_material_w, null, null, null, null, null,'C')
		into	ie_confirma_regra_w
		from	dual;
		
		regulacao_atend_w.ie_tipo := 'ME';
		regulacao_atend_w.nr_seq_receita_amb := nr_seq_fa_rec_farmacia_w;
		regulacao_atend_w.nr_seq_receita_item := nr_sequencia_p;
		regulacao_atend_w.cd_material 		 := cd_material_w;
		regulacao_atend_w.cd_material_envio	 := cd_mat_envio_w;
		regulacao_atend_w.qt_solicitado	:= nvl(qt_dose_w, 1);
		
	elsif ( ie_tipo_p = 'SH') then

		select	max(ie_classificacao),				
				max(cd_pessoa_fisica),
				max(nr_atendimento_origem)
		into	ie_classificacao_w,
				cd_pessoa_fisica_w,
				nr_atendimento_w
		from	paciente_home_care
		where	nr_sequencia = nr_sequencia_p;

		regulacao_atend_w.nr_seq_home_care := nr_sequencia_p;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, null, null,'P',ie_classificacao_w)
		into 	cd_proc_envio_w
		from	dual;

		select	obter_procedimento_regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, null, null, 'I',ie_classificacao_w)
		into 	ie_origem_proced_envio_w
		from	dual;

		select	Obter_conf_Regulacao(ie_tipo_p, null, null, null, null, null, null, null, null, null, null, null, null, 'C',ie_classificacao_w)
		into 	ie_confirma_regra_w
		from	dual;

		regulacao_atend_w.cd_procedimento_envio := cd_proc_envio_w;
		regulacao_atend_w.ie_origem_proced 	:= 	 nvl(nvl(ie_origem_proced_envio_w,ie_origem_proced_w),1);
		
	end if;

	if ( nr_atendimento_w is null) then

		Select  max(nr_atendimento)
		into	nr_atendimento_w
		from    atendimento_paciente
		where   cd_pessoa_fisica = cd_pessoa_fisica_w
		and     sysdate between dt_entrada and nvl(dt_alta,sysdate);

	end if;

	select	max(obter_convenio_atendimento(nr_atendimento)),
			max(obter_dados_pf(cd_medico_resp, 'CPF'))
	into	cd_convenio_w,
			cd_cpf_medico_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;

	select	nvl(max(ie_integracao),'N'),
			nvl(max(ie_confirma_encaminhamento),'N')
	into	ie_integracao_w,
			ie_confirma_encaminhamento_w
	from	convenio_regulacao
	where	cd_convenio = cd_convenio_w
	and		ie_situacao = 'A';

	regulacao_atend_w.nr_atendimento := nr_atendimento_w;
	regulacao_atend_w.cd_pessoa_fisica := cd_pessoa_fisica_w;
	regulacao_atend_w.ie_status := 'EN';
	regulacao_atend_w.nr_seq_prioridade := nvl(nr_seq_prioridade_w,1);
	regulacao_atend_w.ie_integracao := ie_integracao_w;

	insert into regulacao_atend values regulacao_atend_w;

	commit;


	if ((ie_confirma_encaminhamento_w = 'N' and  ((ie_confirma_regra_w = 'N') or (ie_confirma_regra_w = 'X')) ) or
	   ( ie_confirma_encaminhamento_w = 'S' and  ie_confirma_regra_w = 'N')) then

		gerar_requisicao_regulacao(	regulacao_atend_w.nr_sequencia,
									NVL(regulacao_atend_w.ie_tipo,ie_tipo_p),
									cd_pessoa_fisica_w,
									nr_atendimento_w,
									regulacao_atend_w.nm_usuario,
									ie_integracao_w,
									nr_seq_prioridade_w,
									nvl(regulacao_atend_w.cd_procedimento_envio,cd_procedimento_w),
									nvl(nvl(regulacao_atend_w.ie_origem_proced,ie_origem_proced_w),1),
									nvl(regulacao_atend_w.qt_solicitado,1),
                  cd_material_w,
                  qt_dose_w);


	else

		Alterar_status_regulacao(regulacao_atend_w.nr_sequencia, 'AE', '', null,null,null,null,ie_integracao_w);

		if	(nr_seq_prioridade_w is not null) then
			Alterar_prioridade_regulacao(regulacao_atend_w.nr_sequencia, nr_seq_prioridade_w,' ',' ',null,null,null,null,null,null,null,ie_integracao_w,null);
		end if;

	end if;

	nr_seq_reg_atend_p := regulacao_atend_w.nr_sequencia;

end if;

end gerar_regulacao_atend;
/
