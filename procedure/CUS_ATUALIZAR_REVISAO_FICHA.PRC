create or replace
procedure cus_atualizar_revisao_ficha
			(	nr_ficha_tecnica_p		number,
				ie_revisado_p			varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	IE_REVISADO
		Dom�nio 6909
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin
update	ficha_tecnica
set	ie_revisado		= ie_revisado_p,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_ficha_tecnica	= nr_ficha_tecnica_p;

commit;

end cus_atualizar_revisao_ficha;
/