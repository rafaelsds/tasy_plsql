create or replace
procedure Consiste_Homonimos_Protocolo
		(nr_atendimento_p		in 	number,
		nr_seq_protocolo_p		in	number,
		ds_consistencia_p		out	varchar2) is


nr_atendimento_w		varchar2(10);
ds_atendimentos_w		varchar2(4000);
cd_pessoa_fisica_w		varchar2(10);

cursor	c01 is
	select 	to_char(a.nr_atendimento)
	from 	atendimento_paciente b,
		conta_paciente a
	where 	a.nr_atendimento	= b.nr_atendimento
	and 	b.cd_pessoa_fisica	= cd_pessoa_fisica_w
	and 	a.nr_seq_protocolo	= nr_seq_protocolo_p
	group by a.nr_atendimento;

begin

ds_atendimentos_w		:= null;

select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento		= nr_atendimento_p;

open	c01;
loop
fetch	c01 into nr_atendimento_w;
	begin
exit	when c01%notfound;

	if	(ds_atendimentos_w is null) then
		ds_atendimentos_w	:= ds_atendimentos_w || nr_atendimento_w;
	else
		ds_atendimentos_w	:= ds_atendimentos_w || ',' || nr_atendimento_w;
	end if;

	end;
end loop;
close	c01;

if	(length(ds_atendimentos_w) > 0) then
	ds_consistencia_p	:= substr(wheb_mensagem_pck.get_texto(304198,'DS_ATENDIMENTOS_P='|| ds_atendimentos_w ),1,2000); 
	-- Este paciente j� possui atendimento(s) vinculado(s) ao protocolo. (#@DS_ATENDIMENTOS_P#@)
end if;

end Consiste_Homonimos_Protocolo;
/
