create or replace
procedure load_line_xls_translation_file(	nr_seq_lote_p		number,
											si_reset_p			varchar2,
											nr_seq_idioma_p		number,
											cd_expressao_p		number,
											ds_expressao_p		varchar2,
											nm_usuario_p		Varchar2,
											DS_GLOSSARIO_p		Varchar2 default null,
											DS_REVISAO_p	Varchar2 default null) is
											
nm_field_w	varchar2(30)	:= null;
nr_seq_idioma_w	number(10);
ds_locale_w	varchar2(10);

begin
if	(si_reset_p = 'Y') then
	if	(nr_seq_lote_p is not null) then
		delete from lote_imp_traducao_exp where nr_seq_lote_imp = nr_seq_lote_p;
		commit;
	end if;
else
	if	(nr_seq_lote_p is not null) then

		if	(nr_seq_idioma_p is null) then
			select	ds_locale
			into	ds_locale_w
			from	lote_imp_traducao
			where	nr_sequencia	= nr_seq_lote_p;
			
			if	(ds_locale_w = 'pt_BR') then
				nm_field_w	:= 'DS_EXPRESSAO_BR';
			elsif	(ds_locale_w = 'en_US') then
				nm_field_w	:= 'DS_EXPRESSAO_US';
			elsif	(ds_locale_w = 'es_MX') then
				nm_field_w	:= 'DS_EXPRESSAO_MX';
			elsif	(ds_locale_w = 'de_DE') then
				nm_field_w	:= 'DS_EXPRESSAO_DE';					
			elsif	(ds_locale_w = 'en_AU') then
				nm_field_w	:= 'DS_EXPRESSAO_AS';				
			elsif	(ds_locale_w = 'pl_PL') then
				nm_field_w	:= 'DS_EXPRESSAO_PL';				
			end if;				
			
		else
			nr_seq_idioma_w	:= nr_seq_idioma_p;
			
			if	(nr_seq_idioma_w = 2) then
				nm_field_w	:= 'DS_EXPRESSAO_DE';
			elsif	(nr_seq_idioma_w = 7) then
				nm_field_w	:= 'DS_EXPRESSAO_US';
			elsif	(nr_seq_idioma_w = 4) then
				nm_field_w	:= 'DS_EXPRESSAO_AS';	
			elsif	(nr_seq_idioma_w = 3) then
				nm_field_w	:= 'DS_EXPRESSAO_MX';	
			elsif	(nr_seq_idioma_w = 1) then
				nm_field_w	:= 'DS_EXPRESSAO_BR';
			end if;			

		end if;

		if	(nm_field_w is not null) and
			(cd_expressao_p is not null) then
			
			insert into lote_imp_traducao_exp
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_lote_imp,
				nm_atributo_import,
				cd_expressao,
				ds_expressao,
				DS_GLOSSARIO,
				DS_REVISAO)
			values	(lote_imp_traducao_exp_seq.nextval,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nr_seq_lote_p,
					nm_field_w,
					cd_expressao_p,
					ds_expressao_p,
					DS_GLOSSARIO_p,
					DS_REVISAO_p);

			commit;
		end if;
	end if;
end if;

end load_line_xls_translation_file;
/
