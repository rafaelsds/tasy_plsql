create or replace
procedure Gerar_hist_bordero_pagto (
			nr_bordero_p		number,
			cd_motivo_exclusao_p	number,
			ds_justificativa_p	varchar2,
			nm_usuario_p		Varchar2) is
			
ds_titulo_w		varchar2(255);
ds_motivo_w		varchar2(255);
ds_historico_w		varchar2(4000);

begin

if	(cd_motivo_exclusao_p > 0) then
	select	ds_motivo
	into	ds_motivo_w
	from	motivo_alt_financeiro
	where	nr_sequencia = cd_motivo_exclusao_p;
end if;

ds_titulo_w := obter_texto_dic_objeto(196895, wheb_usuario_pck.get_nr_seq_idioma, null);

if	(ds_justificativa_p is not null) then
	/*ds_historico_w := substr('Motivo: ' || ds_motivo_w || chr(13) || chr(10) || chr(13) || chr(10) || 
			'Justificativa: ' || ds_justificativa_p,1,4000);*/
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(302788,'DS_MOTIVO_W='||ds_motivo_w) || chr(13) || chr(10) || chr(13) || chr(10) || 
			wheb_mensagem_pck.get_texto(302789,'DS_JUSTIFICATIVA_P='||ds_justificativa_p),1,4000);
else
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(302788,'DS_MOTIVO_W='||ds_motivo_w),1,4000);
end if;

Gravar_hist_bordero_pagto(nr_bordero_p, ds_titulo_w, ds_historico_w, nm_usuario_p); 

end Gerar_hist_bordero_pagto;
/