create or replace
procedure cus_atualizar_tipo_gasto(	cd_estabelecimento_p	number,
					nr_seq_tabela_p		number,
				cd_tabela_custo_p		number) is

cd_centro_controle_w		number(8);
cd_natureza_gasto_w		number(20);
ie_tipo_gasto_w			varchar2(2);
ie_tipo_gasto_gng_w		varchar2(2);
nr_sequencia_w			number(15);
nr_seq_ng_w			number(10);
cd_estabelecimento_w	orcamento_custo.cd_estabelecimento%type;
cursor c01 is
select	nr_sequencia,
	cd_centro_controle,
	cd_natureza_gasto,
	nr_seq_ng,
	cd_estabelecimento
from	orcamento_custo
where	nr_seq_tabela		= nr_seq_tabela_p;

BEGIN

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_centro_controle_w,
	cd_natureza_gasto_w,
	nr_seq_ng_w,
	cd_estabelecimento_w;
exit when c01%notfound;
	begin
	
	select	ie_tipo_gasto
	into	ie_tipo_gasto_gng_w
	from	natureza_gasto b,
		grupo_natureza_gasto a
	where	a.nr_sequencia			= b.nr_seq_gng
	and	nvl(a.cd_estabelecimento, cd_estabelecimento_w)	= cd_estabelecimento_w
	and	b.nr_sequencia			= nr_seq_ng_w;
		
	ie_tipo_gasto_w	:= substr(cus_obter_tipo_gasto_centro(cd_estabelecimento_w, cd_centro_controle_w, null, cd_natureza_gasto_w, nr_seq_ng_w),1,1);
	
	if	(ie_tipo_gasto_w <> 'M') then
		update	orcamento_custo
		set	ie_tipo_gasto	= ie_tipo_gasto_w
		where	nr_sequencia	= nr_sequencia_w;
	end if;
	end;
end loop;
close c01;
commit;
END cus_atualizar_tipo_gasto;
/
