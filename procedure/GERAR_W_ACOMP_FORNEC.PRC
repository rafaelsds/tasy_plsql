create or replace
procedure gerar_w_acomp_fornec(	cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_final_p		date,
				ie_sintetico_p		varchar2,
				ie_quebra_p		number,
				cd_fornecedor_p		varchar2,
				cd_grupo_material_p	varchar2,
				cd_subgrupo_material_p	varchar2,
				cd_classe_material_p	varchar2,
				cd_material_p		varchar2,
				cd_comprador_p		varchar2,
				ie_urgente_p		varchar2,
				ie_padronizado_p	varchar2) is

cd_fornecedor_w			varchar2(14);
ds_quebra_w			varchar2(255);
cd_material			number(6);
nr_ordem_compra			number(10);
dt_ordem_compra			date;
dt_entrega_limite_w		date;
qt_material_w			number(13,4);
vl_unitario_material_w		number(13,4);
vl_item_liquido_w		number(15,2);
dt_real_entrega_w		date;
qt_real_entrega_w		number(13,4);
qt_entrega_pontual_w		number(13,4);
qt_total_resumo_w		number(22,4);
qt_pontual_resumo_w		number(22,4);
ie_tipo_informacao_w		number(1);
cd_lista_fornecedor_w		varchar2(4000);
cd_lista_grupo_w		varchar2(4000);
cd_lista_subgrupo_w		varchar2(4000);
cd_lista_classe_w		varchar2(4000);
cd_lista_material_w		varchar2(4000);
cd_lista_comprador_w		varchar2(4000);
cd_material_w			number(20);

cursor c01 is
select	a.cd_cgc_fornecedor,
	b.cd_material,
	a.nr_ordem_compra,
	a.dt_ordem_compra,
	nvl(c.dt_entrega_limite,c.dt_prevista_entrega),
	c.qt_prevista_entrega,
	b.vl_unitario_material,
	b.vl_item_liquido,
	nvl(obter_dt_entrada_nf_item_oc(a.nr_ordem_compra, b.nr_item_oci, c.dt_prevista_entrega),c.dt_real_entrega),
	c.qt_real_entrega
from	ordem_compra a,
	ordem_compra_item b,
	ordem_compra_item_entrega c,
	estrutura_material_v e
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_seq_motivo_cancel is null
and	b.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_item_oci = c.nr_item_oci
and	b.cd_material = e.cd_material
and	nvl(c.dt_entrega_limite,c.dt_prevista_entrega) between dt_inicio_p and fim_dia(dt_final_p)
and	a.cd_estabelecimento = cd_estabelecimento_p
and	obter_se_ordem_possui_nota(a.nr_ordem_compra) > 0
and	c.dt_cancelamento is null
and	a.cd_cgc_fornecedor is not null
and	((ie_padronizado_p = 'T') or
	((ie_padronizado_p = 'S') and (obter_se_material_padronizado(cd_estabelecimento_p,b.cd_material) = 'S')) or
	((ie_padronizado_p = 'N') and (obter_se_material_padronizado(cd_estabelecimento_p,b.cd_material) = 'N')))
and	((ie_urgente_p = 'T') or
	((ie_urgente_p = 'S') and (a.ie_urgente = 'S')) or
	((ie_urgente_p = 'N') and (a.ie_urgente = 'N')))
and	((cd_fornecedor_p is null) or
	((cd_fornecedor_p is not null) and (obter_se_contido_char(a.cd_cgc_fornecedor,cd_lista_fornecedor_w) = 'S')))
and	((cd_grupo_material_p is null) or
	((cd_grupo_material_p is not null) and (obter_se_contido_char(e.cd_grupo_material,cd_lista_grupo_w) = 'S')))
and	((cd_subgrupo_material_p is null) or
	((cd_subgrupo_material_p is not null) and (obter_se_contido_char(e.cd_subgrupo_material,cd_lista_subgrupo_w) = 'S')))
and	((cd_classe_material_p is null) or
	((cd_classe_material_p is not null) and (obter_se_contido_char(e.cd_classe_material,cd_lista_classe_w) = 'S')))
and	((cd_comprador_p is null) or
	((cd_comprador_p is not null) and (obter_se_contido_char(a.cd_comprador,cd_lista_comprador_w) = 'S')))
and	((cd_material_p is null) or
	((cd_material_p is not null) and (obter_se_contido_char(b.cd_material,cd_lista_material_w) = 'S')));
	
cursor c02 is			
select	1 ie_tipo,
	decode(ie_quebra_p,
		0,e.ds_grupo_material,
		1,substr(obter_nome_pf_pj(null,w.cd_fornecedor),1,100)) ds_quebra,
	sum(w.qt_material) qt_entrega,
	sum(w.qt_entrega_pontual) qt_pontual,
	e.cd_material
from	w_acomp_fornecedor w,
	estrutura_material_v e
where	w.cd_material = e.cd_material
group by	decode(ie_quebra_p,
		0,e.ds_grupo_material,
		1,substr(obter_nome_pf_pj(null,w.cd_fornecedor),1,100)),
		e.cd_material
union all
SELECT	2 ie_tipo,
	DECODE(ie_quebra_p,
		0,x.ds_grupo_material,
		1,SUBSTR(obter_nome_pf_pj(NULL,w.cd_fornecedor),1,100)) ds_quebra,
	COUNT(*) qt_entrega,
	MAX((	SELECT	COUNT(*)
		FROM	w_acomp_fornecedor a,
			estrutura_material_v e
		WHERE	a.cd_material = e.cd_material
		and	a.cd_material = x.cd_material
		AND	((ie_quebra_p = 0 AND obter_estrutura_material(a.cd_material,'G') = x.cd_grupo_material) OR (ie_quebra_p = 1 AND a.cd_fornecedor = w.cd_fornecedor))
		AND	a.qt_entrega_pontual <> 0)) qt_pontual,
	x.cd_material	
FROM	w_acomp_fornecedor w,
	estrutura_material_v x
WHERE	w.cd_material = x.cd_material
GROUP BY	DECODE(ie_quebra_p,
		0,x.ds_grupo_material,
		1,SUBSTR(obter_nome_pf_pj(NULL,w.cd_fornecedor),1,100)),
		x.cd_material
union all
select	3 ie_tipo,
	decode(ie_quebra_p,
		0,substr(obter_desc_grupo_mat(e.cd_grupo_material),1,100),
		1,substr(obter_nome_pf_pj(null,a.cd_cgc_fornecedor),1,100)) ds_quebra,
	sum(c.qt_prevista_entrega) qt_entrega,
	sum((	select	nvl(sum(x.qt_real_entrega),0) 
		from   	ordem_compra_item_entrega x 
		where  	c.nr_sequencia = x.nr_sequencia 
		and	trunc(nvl(obter_dt_entrada_nf_item_oc(x.nr_ordem_compra, x.nr_item_oci, X.dt_prevista_entrega),x.dt_real_entrega),'dd') <= trunc(nvl(x.dt_entrega_limite,x.dt_prevista_entrega),'dd'))) qt_pontual,
	e.cd_material	
from	ordem_compra a,
	ordem_compra_item b,
	ordem_compra_item_entrega c,
	estrutura_material_v e
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_seq_motivo_cancel is null
and	b.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_item_oci = c.nr_item_oci
and	b.cd_material = e.cd_material
and	nvl(c.dt_entrega_limite,c.dt_prevista_entrega) between trunc(dt_inicio_p,'yy') and fim_dia(dt_final_p)
and	a.cd_estabelecimento = cd_estabelecimento_p
and	obter_se_ordem_possui_nota(a.nr_ordem_compra) > 0
and	a.cd_cgc_fornecedor is not null
and	c.dt_cancelamento is null
and	((ie_padronizado_p = 'T') or
	((ie_padronizado_p = 'S') and (obter_se_material_padronizado(cd_estabelecimento_p,b.cd_material) = 'S')) or
	((ie_padronizado_p = 'N') and (obter_se_material_padronizado(cd_estabelecimento_p,b.cd_material) = 'N')))
and	((ie_urgente_p = 'T') or
	((ie_urgente_p = 'S') and (a.ie_urgente = 'S')) or
	((ie_urgente_p = 'N') and (a.ie_urgente = 'N')))
and	((cd_fornecedor_p is null) or
	((cd_fornecedor_p is not null) and (obter_se_contido_char(a.cd_cgc_fornecedor,cd_lista_fornecedor_w) = 'S')))
and	((cd_grupo_material_p is null) or
	((cd_grupo_material_p is not null) and (obter_se_contido_char(e.cd_grupo_material,cd_lista_grupo_w) = 'S')))
and	((cd_subgrupo_material_p is null) or
	((cd_subgrupo_material_p is not null) and (obter_se_contido_char(e.cd_subgrupo_material,cd_lista_subgrupo_w) = 'S')))
and	((cd_classe_material_p is null) or
	((cd_classe_material_p is not null) and (obter_se_contido_char(e.cd_classe_material,cd_lista_classe_w) = 'S')))
and	((cd_comprador_p is null) or
	((cd_comprador_p is not null) and (obter_se_contido_char(a.cd_comprador,cd_lista_comprador_w) = 'S')))	
and	((cd_material_p is null) or
	((cd_material_p is not null) and (obter_se_contido_char(b.cd_material,cd_lista_material_w) = 'S')))
and	(((ie_quebra_p = 1) and (exists(
	select	1
	from	w_acomp_fornecedor w
	where	a.cd_cgc_fornecedor = w.cd_fornecedor))) 
or	((ie_quebra_p = 0) and (exists(
	select	1
	from	w_acomp_fornecedor w,
		estrutura_material_v y
	where	w.cd_material = y.cd_material
	and	y.cd_grupo_material = e.cd_grupo_material))))
group by	decode(ie_quebra_p,
		0,substr(obter_desc_grupo_mat(e.cd_grupo_material),1,100),
		1,substr(obter_nome_pf_pj(null,a.cd_cgc_fornecedor),1,100)),
		e.cd_material
union all
select 	4 ie_tipo,
	decode(ie_quebra_p,
		0,substr(obter_desc_grupo_mat(e.cd_grupo_material),1,100),
		1,substr(obter_nome_pf_pj(null,a.cd_cgc_fornecedor),1,100)) ds_quebra,
	count(*) qt_entrega,
	sum((	select	count(*)
		from	ordem_compra_item_entrega t
		where	t.nr_sequencia = c.nr_sequencia	
		and	trunc(nvl(obter_dt_entrada_nf_item_oc(t.nr_ordem_compra, t.nr_item_oci, t.dt_prevista_entrega),t.dt_real_entrega),'dd') <= trunc(nvl(dt_entrega_limite,dt_prevista_entrega),'dd'))) qt_pontual,
	e.cd_material	
from	ordem_compra a,
	ordem_compra_item b,
	ordem_compra_item_entrega c,
	estrutura_material_v e
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_seq_motivo_cancel is null
and	b.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_item_oci = c.nr_item_oci(+)
and	b.cd_material = e.cd_material
and	nvl(dt_entrega_limite,c.dt_prevista_entrega) between trunc(dt_inicio_p,'yy') and fim_dia(dt_final_p)
and	a.cd_estabelecimento = cd_estabelecimento_p
and	obter_se_ordem_possui_nota(a.nr_ordem_compra) > 0
and	c.dt_cancelamento is null
and	a.cd_cgc_fornecedor is not null
and	((ie_padronizado_p = 'T') or
	((ie_padronizado_p = 'S') and (obter_se_material_padronizado(cd_estabelecimento_p,b.cd_material) = 'S')) or
	((ie_padronizado_p = 'N') and (obter_se_material_padronizado(cd_estabelecimento_p,b.cd_material) = 'N')))
and	((ie_urgente_p = 'T') or
	((ie_urgente_p = 'S') and (a.ie_urgente = 'S')) or
	((ie_urgente_p = 'N') and (a.ie_urgente = 'N')))
and	((cd_fornecedor_p is null) or
	((cd_fornecedor_p is not null) and (obter_se_contido_char(a.cd_cgc_fornecedor,cd_lista_fornecedor_w) = 'S')))
and	((cd_grupo_material_p is null) or
	((cd_grupo_material_p is not null) and (obter_se_contido_char(e.cd_grupo_material,cd_lista_grupo_w) = 'S')))
and	((cd_subgrupo_material_p is null) or
	((cd_subgrupo_material_p is not null) and (obter_se_contido_char(e.cd_subgrupo_material,cd_lista_subgrupo_w) = 'S')))
and	((cd_classe_material_p is null) or
	((cd_classe_material_p is not null) and (obter_se_contido_char(e.cd_classe_material,cd_lista_classe_w) = 'S')))
and	((cd_comprador_p is null) or
	((cd_comprador_p is not null) and (obter_se_contido_char(a.cd_comprador,cd_lista_comprador_w) = 'S')))	
and	((cd_material_p is null) or
	((cd_material_p is not null) and (obter_se_contido_char(b.cd_material,cd_lista_material_w) = 'S')))	
and	(((ie_quebra_p = 1) and (exists(
	select	1
	from	w_acomp_fornecedor w
	where	a.cd_cgc_fornecedor = w.cd_fornecedor))) 
or	((ie_quebra_p = 0) and (exists(
	select	1
	from	w_acomp_fornecedor w,
		estrutura_material_v y
	where	w.cd_material = y.cd_material
	and	y.cd_grupo_material = e.cd_grupo_material))))
group by	decode(ie_quebra_p,
		0,substr(obter_desc_grupo_mat(e.cd_grupo_material),1,100),
		1,substr(obter_nome_pf_pj(null,a.cd_cgc_fornecedor),1,100)),
		e.cd_material
order by ie_tipo,
	ds_quebra;
				
begin

cd_lista_fornecedor_w		:= replace(replace(replace(cd_fornecedor_p,'(',''),')',''),' ','');
cd_lista_grupo_w		:= replace(replace(replace(cd_grupo_material_p,'(',''),')',''),' ','');
cd_lista_subgrupo_w		:= replace(replace(replace(cd_subgrupo_material_p,'(',''),')',''),' ','');
cd_lista_classe_w		:= replace(replace(replace(cd_classe_material_p,'(',''),')',''),' ','');
cd_lista_material_w		:= replace(replace(replace(cd_material_p,'(',''),')',''),' ','');
cd_lista_comprador_w		:= replace(replace(replace(cd_comprador_p,'(',''),')',''),' ','');

delete from w_acomp_fornecedor;
delete from w_acomp_fornec_resumo;

open C01;
loop
fetch C01 into	
	cd_fornecedor_w,
	cd_material,
	nr_ordem_compra,
	dt_ordem_compra,
	dt_entrega_limite_w,
	qt_material_w,
	vl_unitario_material_w,
	vl_item_liquido_w,
	dt_real_entrega_w,
	qt_real_entrega_w;
exit when C01%notfound;
	begin
	
	if	(trunc(dt_real_entrega_w,'dd') <= trunc(dt_entrega_limite_w,'dd')) then
		qt_entrega_pontual_w := qt_real_entrega_w;
	else
		qt_entrega_pontual_w := 0;
	end if;
	
	insert into w_acomp_fornecedor(
		cd_fornecedor,
		cd_material,
		nr_ordem_compra,
		dt_ordem_compra,
		dt_prevista_entrega,
		qt_material,
		vl_unitario_material,
		vl_item_liquido,
		dt_real_entrega,
		qt_real_entrega,
		qt_entrega_pontual)
	values(	cd_fornecedor_w,
		cd_material,
		nr_ordem_compra,
		dt_ordem_compra,
		dt_entrega_limite_w,
		qt_material_w,
		vl_unitario_material_w,
		vl_item_liquido_w,
		dt_real_entrega_w,
		qt_real_entrega_w,
		qt_entrega_pontual_w);
	end;
end loop;
close C01;

if	(ie_sintetico_p = 'S') then
	
	open C02;
	loop
	fetch C02 into	
		ie_tipo_informacao_w,
		ds_quebra_w,
		qt_total_resumo_w,
		qt_pontual_resumo_w,
		cd_material_w;
	exit when C02%notfound;
		begin
		insert into w_acomp_fornec_resumo(
			ds_quebra,
			ie_tipo_informacao,
			qt_total,
			qt_pontual,
			cd_material)
		values(	ds_quebra_w,
			ie_tipo_informacao_w,
			qt_total_resumo_w,
			qt_pontual_resumo_w,
			cd_material_w);	
		end;
	end loop;
	close C02;
end if;
commit;

end gerar_w_acomp_fornec;
/