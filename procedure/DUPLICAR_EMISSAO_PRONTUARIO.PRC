create or replace procedure duplicar_emissao_prontuario(nr_sequencia_p	number,

														nr_seq_gerado_p	out number,

														nm_usuario_p		Varchar2) is

nr_sequencia_w	number(10);

begin

select	prontuario_emissao_seq.nextval

into	nr_sequencia_w

from	dual;

insert into prontuario_emissao (

			nr_sequencia,

			cd_pessoa_fisica,

			dt_atualizacao,

			nm_usuario,

			dt_inicial,

			dt_final,

			cd_pessoa_retirou,

			nm_pessoa_receb,

			cd_setor_atendimento,

			ie_anamnese,

			ie_evolucao,

			ie_diagnostico,

			ie_atestado,

			ie_sinais_vitais,

			ie_prescricao,

			ie_exame_lab,

			ie_laudo,

			ie_historico_saude,

			ie_oftalmologia,

			ie_orientacao_alta,

			ie_parecer_medico,

			ie_receita,

			ie_sae,

			ie_solic_exame_ext,

			ie_declaracao_receb,

			ie_consentimento,

			ie_consulta_amb,

			ie_ganho_perda,

			ie_obstetricia_nasc,

			ie_avaliacao,

			ie_checkup,

			ie_diag_tumor,

			ie_trat_onco,

			nr_atendimento,

			ie_agentes_anestesicos,

			ie_medicamentos,

			ie_tecnica_descricao,

			ie_terapia_hidroeletrolitica,

			ie_hemoderivados,

			ie_posicao,

			ie_materiais,

			ie_pele,

			ie_incisao,

			ie_curativo,

			ie_apae,

			ie_resumo_descricao,

			ie_faturamento_medico,

			ie_liberacao_sra,

			ie_template,

			cd_estabelecimento,

			ie_avaliacao_lacrimal,

			ie_gonioscopia,

			ie_curva_tensional,

			ie_biometria,

			ie_biomicroscopia,

			ie_fundoscopia,

			ie_tomografia_olho,

			ie_fotocoagulacao,

			ie_capsulotomia,

			ie_ultrassonografia,

			ie_tomografia_coerencia,

			ie_refracao,

			ie_auto_refracao,

			ie_pupilometria,

			ie_microscopia_especular,

			ie_motilidade_ocular_completa,

			ie_angiofluoresceinografia,

			ie_motilidade_ocular,

			ie_acuidade_visual,

			ie_tonometria_pneumatica,

			ie_anamnese_inicial,

			ie_receita_oculos,

			ie_exame_ocular_externo,

			ie_tonometria_aplanacao,

			ie_conduta,

			ie_paquimetria,

			ie_campimetria,

			ie_ceratometria,

			ie_potencial_acuidade,

			ie_iridectomia,

			ie_daltonismo,

			ie_sobrecarga_hidrica,

			ie_mapeamento_retina,

			ie_retinografia,

			ie_distancia_pupilar,

			ie_dispositivo,

			ie_ppr,

			ie_alertas,

			ie_escala,

			ie_entlassusmanagement,

			ie_kv_form,

			nr_seq_episodio,

			ie_utilizados_cirurgia,

			ie_aberrometria_ocular ,

			ie_care_directive,

			ie_wound_mngmnt,

			ie_ecm,

			ie_infection_control,

			ie_estimated_discharge,

			ie_dschrg_tnsfr_sumry,

			ie_events,

			ie_justf_requests,

			ie_call_history,

			ie_nursing_actions,

			ie_icunsw,

			ie_death_certf,

			ie_time_motion,

			ie_instrument,

			ie_intraoperative_care,

			ie_attach_assmnt,

			ie_attach_consents,

			ie_attach_just_req,

			ie_attach_care_dir,

			ie_attach_death_cert,

			ie_attach_mpi,

			ie_attach_pathology,

			ie_attach_imaging_diag,

			ie_tourniquet,

            ie_diathermy,

            ie_spec_path,

            ie_demarcation,

            ie_demarcation_conf,

			ie_equipment)

    select	nr_sequencia_w,

			cd_pessoa_fisica,

			sysdate,

			nm_usuario_p,

			dt_inicial,

			dt_final,

			cd_pessoa_retirou,

			nm_pessoa_receb,

			cd_setor_atendimento,

			ie_anamnese,

			ie_evolucao,

			ie_diagnostico,

			ie_atestado,

			ie_sinais_vitais,

			ie_prescricao,

			ie_exame_lab,

			ie_laudo,

			ie_historico_saude,

			ie_oftalmologia,

			ie_orientacao_alta,

			ie_parecer_medico,

			ie_receita,

			ie_sae,

			ie_solic_exame_ext,

			ie_declaracao_receb,

			ie_consentimento,

			ie_consulta_amb,

			ie_ganho_perda,

			ie_obstetricia_nasc,

			ie_avaliacao,

			ie_checkup,

			ie_diag_tumor,

			ie_trat_onco,

			nr_atendimento,

			ie_agentes_anestesicos,

			ie_medicamentos,

			ie_tecnica_descricao,

			ie_terapia_hidroeletrolitica,

			ie_hemoderivados,

			ie_posicao,

			ie_materiais,

			ie_pele,

			ie_incisao,

			ie_curativo,

			ie_apae,

			ie_resumo_descricao,

			ie_faturamento_medico,

			ie_liberacao_sra,

			ie_template,

			nvl(CD_ESTABELECIMENTO,obter_estabelecimento_ativo),

			ie_avaliacao_lacrimal,

			ie_gonioscopia,

			ie_curva_tensional,

			ie_biometria,

			ie_biomicroscopia,

			ie_fundoscopia,

			ie_tomografia_olho,

			ie_fotocoagulacao,

			ie_capsulotomia,

			ie_ultrassonografia,

			ie_tomografia_coerencia,

			ie_refracao,

			ie_auto_refracao,

			ie_pupilometria,

			ie_microscopia_especular,

			ie_motilidade_ocular_completa,

			ie_angiofluoresceinografia,

			ie_motilidade_ocular,

			ie_acuidade_visual,

			ie_tonometria_pneumatica,

			ie_anamnese_inicial,

			ie_receita_oculos,

			ie_exame_ocular_externo,

			ie_tonometria_aplanacao,

			ie_conduta,

			ie_paquimetria,

			ie_campimetria,

			ie_ceratometria,

			ie_potencial_acuidade,

			ie_iridectomia,

			ie_daltonismo,

			ie_sobrecarga_hidrica,

			ie_mapeamento_retina,

			ie_retinografia,

			ie_distancia_pupilar,

			ie_dispositivo,

			ie_ppr,

			ie_alertas,

			ie_escala,

			ie_entlassusmanagement,

			ie_kv_form,

			nr_seq_episodio,

			ie_utilizados_cirurgia,

		    ie_aberrometria_ocular,
		  
			ie_care_directive,

			ie_wound_mngmnt,

			ie_ecm,

			ie_infection_control,

			ie_estimated_discharge,

			ie_dschrg_tnsfr_sumry,

			ie_events,

			ie_justf_requests,

			ie_call_history,

			ie_nursing_actions,

			ie_icunsw,

			ie_death_certf,

			ie_time_motion,

			ie_instrument,

			ie_intraoperative_care,

			ie_attach_assmnt,

			ie_attach_consents,

			ie_attach_just_req,

			ie_attach_care_dir,

			ie_attach_death_cert,

			ie_attach_mpi,

			ie_attach_pathology,

			ie_attach_imaging_diag,

			ie_tourniquet,

            ie_diathermy,

            ie_spec_path,

            ie_demarcation,

            ie_demarcation_conf,

			ie_equipment

	from	prontuario_emissao

	where	nr_sequencia = nr_sequencia_p;

commit;

nr_seq_gerado_p	:= nr_sequencia_w;

end duplicar_emissao_prontuario;
/