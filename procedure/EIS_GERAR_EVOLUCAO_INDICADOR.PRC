create or replace
procedure Eis_Gerar_Evolucao_Indicador
			(
				cd_estabelecimento_p		number,
				ie_periodo_p			varchar2,
				nr_seq_indicador_p		number,
				dt_referencia_p			date,
				nr_seq_dimensao_p		number,
				nr_seq_informacao_p		number,
				nr_seq_data_p			number,
				nm_usuario_p			varchar2,
				ds_sql_filtro_p			varchar2,
				ds_sql_drill_p			varchar2,
				ie_tipo_data_p			number,
				ie_etapa_p			number,
				ie_restringe_estab_p		varchar2 default 'S') is

			
ie_tipo_data_w			varchar2(01);
nm_tabela_indicador_w		varchar2(50);
ds_sql_where_ind_w		varchar2(2000);
ds_sql_mascara_w		varchar2(50);
nm_tabela_referencia_w		indicador_gestao_atrib.nm_tabela_referencia%type;
nm_atrib_dimensao_w		indicador_gestao_atrib.nm_atributo%type;
nm_atributo_referencia_w	indicador_gestao_atrib.nm_atributo_referencia%type;
nm_atributo_drill_w		indicador_gestao_atrib.nm_atributo_drill%type;
ds_sql_where_w			varchar2(4000);
ds_sql_data_w			varchar2(700);
nm_atrib_data_w			indicador_gestao_atrib.nm_atributo%type;
ds_select_data_w		varchar2(200);
ds_comando_w			varchar2(4000);
nm_atrib_inf_w			varchar2(255);
ie_alias_w			varchar2(2); /* Elemar - 29/04/04 */
ds_mascara_grid_w		varchar2(30);
nr_seq_wheb_w			number(10,0);
sql_nulo_w			varchar2(300);
IE_MOSTRA_NULO_w		indicador_gestao_atrib.ie_mostra_nulo%type;
ds_sql_where_ww			indicador_gestao_atrib.ds_sql_where%type;
ds_sql_where_inf_w		indicador_gestao_atrib.ds_sql_where%type;
cd_empresa_w			number(4,0);
ie_formato_tot_linha_w		varchar2(1);
nm_atributo_divisor_w		varchar2(50);
nm_atributo_dividendo_w		varchar2(50);
ds_media_divisao_w		varchar2(255);
nm_tablespace_w			varchar2(50);
ds_sql_filtro_W			varchar2(4000);
ds_comando_orig_w		varchar2(2000);
ds_comando_from_w		varchar2(2000);
ds_filtro_padrao_w		indicador_gestao_atrib.ds_filtro_padrao%type;
i				number(10);
ds_comando_aux_w		clob;
ds_novo_sql_where_w		varchar2(255);

begin

ds_sql_filtro_W	:= replace(ds_sql_filtro_p, 'cd_unidade', 'UPPER(CD_UNIDADE)');


select	' TABLESPACE '||obter_tablespace_tab_temp
into	nm_tablespace_w
from 	dual;


ds_sql_data_w			:= '';
ds_sql_mascara_w		:= '';
cd_empresa_w			:= obter_empresa_estab(cd_estabelecimento_p);

select	nvl(nr_seq_wheb,nr_sequencia)
into	nr_seq_wheb_w
from 	indicador_gestao
where	nr_sequencia = nr_seq_indicador_p;

select	ds_tabela_visao,
	ds_sql_where,
	ie_tipo_data
into	nm_tabela_indicador_w,
	ds_sql_where_ind_w,
	ie_tipo_data_w
from	indicador_gestao
where	nr_sequencia	= nr_seq_indicador_p;

select	nm_atributo_drill,
	nm_tabela_referencia,
	nm_atributo_referencia,
	nm_atributo,
	ie_mostra_nulo,
	ds_sql_where
into	nm_atributo_drill_w,
	nm_tabela_referencia_w,
	nm_atributo_referencia_w,
	nm_atrib_dimensao_w,
	ie_mostra_nulo_w,
	ds_sql_where_ww
from	indicador_gestao_atrib
where	nr_sequencia		= nr_seq_dimensao_p
and	nr_seq_ind_gestao	= nr_seq_indicador_p;

select	nm_atributo
into	nm_atrib_data_w
from	indicador_gestao_atrib
where	nr_sequencia		= nr_seq_data_p
and	nr_seq_ind_gestao	= nr_seq_indicador_p;

select	nm_atributo,
	ds_mascara_grid,
	ds_sql_where,
	ie_formato_tot_linha,
	ie_divisor,
	ie_dividendo,
	ds_filtro_padrao
into	nm_atrib_inf_w,
	ds_mascara_grid_w,
	ds_sql_where_inf_w,
	ie_formato_tot_linha_w,
	nm_atributo_divisor_w,
	nm_atributo_dividendo_w,
	ds_filtro_padrao_w
from	indicador_gestao_atrib
where	nr_sequencia		= nr_seq_informacao_p
and	nr_seq_ind_gestao	= nr_seq_indicador_p;

if 	(nr_seq_wheb_w in (1851, 1863, 1868, 1880, 1894, 1898, 1912, 1916, 1917, 1921, 1923, 1924, 1927, 1932, /*3090,*/ 3100)) then
	ds_sql_filtro_W := replace(upper(ds_sql_filtro_W), 'AND DS_FORM', 'AND UPPER(DS_FORM)');
	ds_sql_filtro_W := replace(upper(ds_sql_filtro_W), 'AND NM_PROFESSIONAL', 'AND UPPER(NM_PROFESSIONAL)');
	ds_sql_filtro_W := replace(upper(ds_sql_filtro_W), 'AND NM_GROUP', 'AND UPPER(NM_GROUP)');
	ds_sql_filtro_W := replace(upper(ds_sql_filtro_W), 'AND NM_AGE_RANGE', 'AND UPPER(NM_AGE_RANGE)');
	ds_sql_filtro_W := replace(upper(ds_sql_filtro_W), 'AND NM_EPISODE_PLACE', 'AND UPPER(NM_EPISODE_PLACE)');
	ds_sql_filtro_W := replace(upper(ds_sql_filtro_W), 'AND DS_REASON_CANCEL', 'AND UPPER(DS_REASON_CANCEL)');
	ds_sql_filtro_W := replace(upper(ds_sql_filtro_W), 'AND NM_RESPONSABILITY', 'AND UPPER(NM_RESPONSABILITY)');
elsif	(nr_seq_wheb_w = 91) then
	ds_sql_filtro_W	:= replace(upper(ds_sql_filtro_W), 'DS_MOTIVO_ALTA', 'UPPER(DS_MOTIVO_ALTA)');
end if;

if	(instr(upper(nm_atrib_inf_w),':DT_FIM') > 0) then   /*Anderson 01/08/2007 - OS63683*/

	select	replace(upper(nm_atrib_inf_w),':DT_FIM','last_day(add_months(trunc(' || ' to_date( ' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || 
			chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || '),' || chr(39) || 'YEAR' || chr(39) || '),11))')
	into	nm_atrib_inf_w
	from	indicador_gestao_atrib
	where	nr_sequencia		= nr_seq_informacao_p
	and	nr_seq_ind_gestao	= nr_seq_indicador_p;

	select	replace(upper(nm_atrib_inf_w),':CD_ESTABELECIMENTO',cd_estabelecimento_p)
	into	nm_atrib_inf_w
	from	indicador_gestao_atrib
	where	nr_sequencia		= nr_seq_informacao_p
	and	nr_seq_ind_gestao	= nr_seq_indicador_p;

	select	replace(upper(nm_atrib_inf_w),':DT_INICIO','to_date( ' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || chr(39) || ','
						|| chr(39) || 'dd/mm/yyyy' || chr(39) || ')')
	into	nm_atrib_inf_w
	from	indicador_gestao_atrib
	where	nr_sequencia		= nr_seq_informacao_p
	and	nr_seq_ind_gestao	= nr_seq_indicador_p;
end if;

ds_sql_where_w	:= ' from ';

if( nr_seq_wheb_w = 71 ) then
	nm_atrib_inf_w	:= replace(upper(nm_atrib_inf_w), ':CD_DIMENSAO', nm_atributo_drill_w);
end if;		

if	(ds_mascara_grid_w is not null) then
	ds_sql_mascara_w	:= ', ' || chr(39) || ds_mascara_grid_w || chr(39) || ' ds_mascara ';
else
	ds_sql_mascara_w	:= ', ' || chr(39) || ' ' || chr(39) || ' ds_mascara ';
end if;

ie_alias_w			:= '';
if	(nm_tabela_referencia_w is not null) then
	ds_sql_where_w	:= ds_sql_where_w || nm_tabela_referencia_w || ' b, ';
	ie_alias_w		:= 'b.';
end if;

ds_sql_where_w	:= ds_sql_where_w || nm_tabela_indicador_w || ' a ' ||
			' where 1 = 1 ';
			
if	(nm_tabela_referencia_w is not null) and
	(nm_atributo_referencia_w is not null) then
	ds_sql_where_w	:= ds_sql_where_w || ' and b.' || nm_atributo_referencia_w || '(+) = a.' || nm_atributo_drill_w;
end if;

if	(ie_restringe_estab_p = 'S') then
	ds_sql_where_w	:= ds_sql_where_w || ' ' || replace(upper(ds_sql_where_ind_w), ':CD_ESTABELECIMENTO', cd_estabelecimento_p) ||
			ds_sql_filtro_W || ' ' || ds_sql_drill_p;			
else
	ds_sql_where_w	:= ds_sql_where_w || ' ' || replace(upper(ds_sql_where_ind_w), ':CD_ESTABELECIMENTO', 'A.CD_ESTABELECIMENTO') ||
			ds_sql_filtro_W || ' ' || ds_sql_drill_p;
end if;

			
ds_sql_where_w	:= replace(ds_sql_where_w, ':CD_EMPRESA', cd_empresa_w);


if	(ds_sql_where_ww is not null) then
	ds_sql_where_w	:=	ds_sql_where_w || ' ' || ds_sql_where_ww;
end if;

if	(ds_sql_where_inf_w is not null) then
	ds_sql_where_w	:=	ds_sql_where_w || ' ' || ds_sql_where_inf_w;
end if;

if 	(ie_etapa_p = 1) then
	ds_sql_where_w	:=	ds_sql_where_w || ' ' || ' and ie_etapa = ' || chr(39) || 'C' || chr(39);
elsif	(ie_etapa_p = 2) then
	ds_sql_where_w	:=	ds_sql_where_w || ' ' || ' and ie_etapa = ' || chr(39) || 'T' || chr(39);
elsif	(ie_etapa_p = 3) then
	ds_sql_where_w	:=	ds_sql_where_w || ' ' || ' and ie_etapa = ' || chr(39) || 'P' || chr(39);
end if;

if	(ie_tipo_data_w	= 'P') then
	ds_sql_data_w	:= ' and a.ie_periodo = ' || Chr(39) || ie_periodo_p || chr(39);
end if;

if	(nr_seq_wheb_w	= 71) and (nm_atrib_inf_w = 'NR_PAC_DIA') then
	ds_sql_data_w	:= ' and a.ie_periodo = ' || Chr(39) || 'D' || chr(39);		
end if;

if (nr_seq_indicador_p = 402) and
   (nr_seq_dimensao_p = 1311) and
   (nr_seq_informacao_p = 5184) then
    nm_atrib_data_w := 'dt_atend';
end if;

if	(nr_seq_wheb_w	not in (81,181)) then
	begin
	if	(ie_periodo_p	= 'M') then
		ds_sql_data_w	:= ds_sql_data_w || ' and a.' || nm_atrib_data_w || ' between to_date( ' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') 

|| 
					chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') and last_day(add_months(trunc(' ||
					' to_date( ' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || chr(39) || ','
					|| chr(39) || 'dd/mm/yyyy' || chr(39) || '),' || chr(39) || 'YEAR' || chr(39) || '),11))';
	elsif	(nr_seq_wheb_w = 499) then
		ds_sql_data_w	:= ds_sql_data_w || ' and a.' || nm_atrib_data_w || ' between to_date( ' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') 
			|| 		chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') and last_day( ' || ' to_date( ' || 
					chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || '))+86399/86400';
	else
		
		ds_sql_data_w	:= ds_sql_data_w || ' and trunc(a.' || nm_atrib_data_w || ',' || chr(39) ||'dd' || chr(39) || ') between to_date( ' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') 
				|| chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ') and last_day( ' || ' to_date( ' || 
					chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || '))';
	end if;
	end;
end if;

if	(instr(Upper(nm_atrib_inf_w), 'COUNT(') = 0) and
	(instr(Upper(nm_atrib_inf_w), 'SUM(') = 0) and
	(instr(Upper(nm_atrib_inf_w), 'AVG(') = 0) and
	(instr(Upper(nm_atrib_inf_w), 'MAX(') = 0) and
	(instr(Upper(nm_atrib_inf_w), 'MIN(') = 0) and
	(instr(Upper(nm_atrib_inf_w), 'MEDIAN(') = 0) then
	nm_atrib_inf_w	:= 'sum(' || nm_atrib_inf_w || ')';
end if;

if	(ie_periodo_p = 'M') then
	ds_select_data_w := ' trunc(a.' || nm_atrib_data_w || ',' || chr(39) || 'MM' || chr(39) || ') '; 
elsif	(ie_tipo_data_p	= 1) and
	(nr_seq_wheb_w  = 181)then
	ds_select_data_w	 := ' trunc(a.dt_receita   ,' || chr(39) || 'MM' || chr(39) || ') '; 
else
	ds_select_data_w := ' trunc(a.' || nm_atrib_data_w || ',' || chr(39) || 'DD' || chr(39) || ') '; 
end if;

/*if	(ie_tipo_data_p	= 1) and
	(nr_seq_wheb_w  = 181)then
	ds_select_data_w	 := ' trunc(a.dt_receita   ,' || chr(39) || 'MM' || chr(39) || ') '; 
else
	ds_select_data_w := ' trunc(a.' || nm_atrib_data_w || ',' || chr(39) || 'DD' || chr(39) || ') '; 
end if;*/

exec_sql_dinamico('TASY', ' drop table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', ''));

if	(IE_MOSTRA_NULO_w = 'N') then
	sql_nulo_w	:= ' and ' || nm_atrib_dimensao_w || ' is not null ';
end if;

ds_media_divisao_w	:= '';
if	(ie_formato_tot_linha_w = 'D') and
	(nm_atributo_dividendo_w is not null) and
	(nm_atributo_divisor_w is not null) then
	ds_media_divisao_w	:= nm_atributo_dividendo_w || ' qt_dividendo, ' || nm_atributo_divisor_w || ' qt_divisor, ';
end if;


ds_comando_w	:= ' select nvl(substr(' || ie_alias_w || nm_atrib_dimensao_w || ',1,255), ' || chr(39) || WHEB_MENSAGEM_PCK.get_texto(304108)  || chr(39) || ') ds_dimensao,
		substr(a.' || nm_atributo_drill_w || ',1,255) cd_dimensao, ' || nm_atrib_inf_w || ' vl_data, ' || 
		ds_media_divisao_w ||		
		ds_select_data_w || 'dt_data ' || ds_sql_mascara_w || ds_sql_where_w || ds_sql_data_w || sql_nulo_w; 
			
if not	(nr_seq_wheb_w in (1880,1898) and
	(instr(Upper(nm_atrib_inf_w), 'STATS_MODE') != 0 or instr(Upper(nm_atrib_inf_w), 'AVG') != 0)) then
	ds_comando_w := ds_comando_w || ' group by substr(' || ie_alias_w || nm_atrib_dimensao_w || ',1,255), substr(a.' || nm_atributo_drill_w || ',1,255), ' || ds_select_data_w;
end if;
	
/* Francisco - 943635 - Indicador de sinais vitais precisa de subselect */
if	(nr_seq_wheb_w = 1837/*2030*/) and
	(ds_filtro_padrao_w is not null) then
	ds_comando_orig_w	:=	' select nvl(substr(' || ie_alias_w || nm_atrib_dimensao_w || ',1,255), ' || chr(39) || WHEB_MENSAGEM_PCK.get_texto(304108)  || chr(39) || ') ds_dimensao,
		substr(a.' || nm_atributo_drill_w || ',1,255) cd_dimensao, ' || ds_filtro_padrao_w || ', ' || 
		ds_media_divisao_w ||		
		ds_select_data_w || 'dt_data ' || ds_sql_mascara_w || ds_sql_where_w || ds_sql_data_w || sql_nulo_w;
		
	ds_comando_from_w	:= ' select ds_dimensao, cd_dimensao, ' || nm_atrib_inf_w || ' vl_data, ' || 
			ds_media_divisao_w || ' dt_data ' || ds_sql_mascara_w;
			
	ds_comando_w	:= ds_comando_from_w || ' from (' || ds_comando_orig_w || ') a group by ds_dimensao, cd_dimensao, dt_data';
end if;

ds_comando_w	:= replace(ds_comando_w, ':NR_DIAS_CENSO', '30');
ds_comando_w	:= replace(ds_comando_w, ':CD_ESTAB_EVENTO', cd_estabelecimento_p);
ds_comando_w	:= replace(ds_comando_w, ':IE_RESTRINGE_ESTAB', chr(39) || ie_restringe_estab_p || chr(39));

if	(nr_seq_wheb_w in (1851, 1863, 1868)) then  
	ds_comando_w	:= replace(upper(ds_comando_w), 'A.'||upper(nm_atrib_data_w), ' to_date( ' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || chr(39) || ',' || chr(39) || 'dd/mm/yyyy' || chr(39) || ')');
	
	select	replace(upper(ds_comando_w),
		'TO_DATE(:DT_FIM, ' || chr(39) || 'DD/MM/YYYY HH24:MI:SS' || chr(39) || ')',
		'FIM_DIA(LAST_DAY(TO_DATE(' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'DD/MM/YYYY HH24:MI:SS'|| chr(39) ||')))' )
	into	ds_comando_w
	from	dual;

	select	replace(upper(ds_comando_w),
		':DT_FIM',
		'FIM_DIA(LAST_DAY(TO_DATE(' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'DD/MM/YYYY HH24:MI:SS'|| chr(39) ||')))' )
	into	ds_comando_w
	from	dual;
	
	select	replace(upper(ds_comando_w),
		':DT_INICIO',
		'TO_DATE(' || chr(39) || to_char(dt_referencia_p,'dd/mm/yyyy') || chr(39) || ', ' || chr(39) || 'DD/MM/YYYY HH24:MI:SS'|| chr(39) ||')' )
	into	ds_comando_w
	from	dual;
	
	ds_comando_aux_w	:= ds_comando_w;
	for i in 1..11 loop
		ds_comando_aux_w	:= ds_comando_aux_w || ' UNION ' || replace(ds_comando_w, '01/01/', '01/'|| TRIM(to_char(i+1,'00')) ||'/');
	end loop;
	exec_sql_dinamico('TASY', ' create table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', '') || nm_tablespace_w || ' as ' || ds_comando_aux_w);
elsif	(nr_seq_wheb_w in (1880,1898)  and
	instr(Upper(nm_atrib_inf_w), 'STATS_MODE') != 0) then

	ds_comando_aux_w := '	SELECT	A.DS_DIMENSAO DS_DIMENSAO,
					A.CD_DIMENSAO CD_DIMENSAO,
					STATS_MODE(A.VL_DATA) VL_DATA,
					A.DT_DATA
					' || ds_sql_mascara_w || ' 
				FROM	(' || ds_comando_w || ' ) A 
				GROUP BY A.DS_DIMENSAO, A.CD_DIMENSAO, A.DT_DATA ';
	exec_sql_dinamico('TASY', ' create table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', '') || nm_tablespace_w || ' as ' || ds_comando_aux_w);
	
elsif	(nr_seq_wheb_w in (1880,1898)  and
	instr(Upper(nm_atrib_inf_w), 'AVG') != 0) then

	ds_comando_aux_w := '	SELECT	A.DS_DIMENSAO DS_DIMENSAO,
					A.CD_DIMENSAO CD_DIMENSAO,
					AVG(A.VL_DATA) VL_DATA,
					A.DT_DATA
					' || ds_sql_mascara_w || ' 
				FROM	(' || ds_comando_w || ' ) A 
				GROUP BY A.DS_DIMENSAO, A.CD_DIMENSAO, A.DT_DATA ';
	exec_sql_dinamico('TASY', ' create table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', '') || nm_tablespace_w || ' as ' || ds_comando_aux_w);

elsif	(nr_seq_wheb_w in (1894,1898)  and
	nm_atrib_dimensao_w = 'DS_UNIQUE' and
	instr(Upper(nm_atrib_inf_w), 'TARGET_POPULATION') != 0) then
	ds_comando_w := replace(upper(ds_comando_w), ' WHERE 1 = 1 ', ' WHERE 1 = 1 AND SI_TARGET_POPULATION = ''S'' ');
	exec_sql_dinamico('TASY', ' create table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', '') || nm_tablespace_w || ' as ' || ds_comando_w);
elsif	(nr_seq_wheb_w in (1924)  and
	instr(Upper(nm_atrib_inf_w), 'PERCENT') != 0)then
	ds_comando_aux_w := '	SELECT	A.DS_DIMENSAO DS_DIMENSAO,
					A.CD_DIMENSAO CD_DIMENSAO,
					B.VL_DATA / A.VL_DATA * 100 VL_DATA,
					A.DT_DATA
					' || ds_sql_mascara_w || ' 
				FROM	(' || ds_comando_w || ') A,
					(' || replace(ds_comando_w, CHR(39)||'P'||CHR(39) , CHR(39)||'R'||CHR(39)) || ') B
				WHERE	A.DS_DIMENSAO = B.DS_DIMENSAO
				AND	A.DT_DATA = B.DT_DATA ';

	exec_sql_dinamico('TASY', ' create table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', '') || nm_tablespace_w || ' as ' || ds_comando_aux_w);
elsif	(nr_seq_wheb_w in (1917)  and
	instr(Upper(nm_atrib_inf_w), 'PERCENT') != 0)then
	if 	(instr(Upper(nm_atrib_inf_w), 'CANCELED') != 0) then
		ds_comando_aux_w := '	SELECT	A.DS_DIMENSAO DS_DIMENSAO,
						A.CD_DIMENSAO CD_DIMENSAO,
						B.VL_DATA / A.VL_DATA * 100 VL_DATA,
						A.DT_DATA
						' || ds_sql_mascara_w || ' 
					FROM	(' || ds_comando_w || ') A,
						(' || replace(ds_comando_w, 'WHERE 1 = 1' , ' WHERE 1 = 1 AND SI_CANCELLED = '|| CHR(39)||'S'||CHR(39)) || ') B
					WHERE	A.DS_DIMENSAO = B.DS_DIMENSAO
					AND	A.DT_DATA = B.DT_DATA ';
	elsif 	(instr(Upper(nm_atrib_inf_w), 'MISSED') != 0) then
		ds_comando_aux_w := '	SELECT	A.DS_DIMENSAO DS_DIMENSAO,
						A.CD_DIMENSAO CD_DIMENSAO,
						B.VL_DATA / A.VL_DATA * 100 VL_DATA,
						A.DT_DATA
						' || ds_sql_mascara_w || ' 
					FROM	(' || ds_comando_w || ') A,
						(' || replace(ds_comando_w, 'COUNT(DISTINCT NR_SEQ_FACT)' , ' SUM(QT_ABS_BY_FACT) ') || ') B
					WHERE	A.DS_DIMENSAO = B.DS_DIMENSAO
					AND	A.DT_DATA = B.DT_DATA ';
	end if;
	exec_sql_dinamico('TASY', ' create table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', '') || nm_tablespace_w || ' as ' || ds_comando_aux_w);	
elsif	(nr_seq_wheb_w in (1927)  and
	instr(Upper(nm_atrib_inf_w), 'PERCENT') != 0) then
	if 	(instr(Upper(nm_atrib_inf_w), 'MISSED') != 0) then
		ds_novo_sql_where_w := ' WHERE 1 = 1 AND SI_PRESENT = '|| CHR(39)||'N'||CHR(39);
	elsif	(instr(Upper(nm_atrib_inf_w), 'PRESENT') != 0) then
		ds_novo_sql_where_w := ' WHERE 1 = 1 AND SI_PRESENT = '|| CHR(39)||'S'||CHR(39);
	elsif	(instr(Upper(nm_atrib_inf_w), 'FIRST') != 0) then
		ds_novo_sql_where_w := ' WHERE 1 = 1 AND SI_FIRST_ENCOUNTER = '|| CHR(39)||'S'||CHR(39);
	elsif	(instr(Upper(nm_atrib_inf_w), 'LAST') != 0) then
		ds_novo_sql_where_w := ' WHERE 1 = 1 AND SI_LAST_ENCOUNTER = '|| CHR(39)||'S'||CHR(39);
	elsif	(instr(Upper(nm_atrib_inf_w), 'GIVE_UP') != 0) then
		ds_novo_sql_where_w := ' WHERE 1 = 1 AND PR_CONCLUSION < 25 ';
	elsif	(instr(Upper(nm_atrib_inf_w), 'BENEF') != 0) then
		ds_novo_sql_where_w := ' WHERE 1 = 1 AND EXISTS (SELECT 1 FROM PLS_SEGURADO WHERE CD_PESSOA_FISICA = NR_DIF_PERSON) ';
	end if;
	ds_comando_aux_w := '	SELECT	A.DS_DIMENSAO DS_DIMENSAO,
						A.CD_DIMENSAO CD_DIMENSAO,
						B.VL_DATA / A.VL_DATA * 100 VL_DATA,
						A.DT_DATA
						' || ds_sql_mascara_w || ' 
					FROM	(' || ds_comando_w || ') A,
						(' || replace(ds_comando_w, 'WHERE 1 = 1' , ds_novo_sql_where_w) || ') B
					WHERE	A.DS_DIMENSAO = B.DS_DIMENSAO
					AND	A.DT_DATA = B.DT_DATA ';
	exec_sql_dinamico('TASY', ' create table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', '') || nm_tablespace_w || ' as ' || ds_comando_aux_w);	
else
	exec_sql_dinamico('TASY', ' create table w_eis_evol_' || replace(elimina_caractere_especial(nm_usuario_p), ' ', '') || nm_tablespace_w || ' as ' || ds_comando_w);

end if;

end Eis_Gerar_Evolucao_Indicador;
/
