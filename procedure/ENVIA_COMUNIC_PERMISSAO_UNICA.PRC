create or replace
procedure envia_comunic_permissao_unica(
				nr_sequencia_p	number,
				nm_usuario_p	varchar2) is
				
nm_usuario_destino_w	comunic_interna.nm_usuario_destino%type;
ds_perfil_adicional_w	comunic_interna.ds_perfil_adicional%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
cd_pessoa_fisica_w	medico_permissao_unica.cd_pessoa_fisica%type;
dt_inicio_w		medico_permissao_unica.dt_inicio%type;
dt_liberacao_w		medico_permissao_unica.dt_liberacao%type;
nm_usuario_liberacao_w	medico_permissao_unica.nm_usuario_liberacao%type;

cursor c01 is
	select	a.nr_sequencia,
		a.ds_titulo,
		a.ds_mensagem
	from	medico_permissao_comunic a
	where	a.ie_situacao = 'A'
	and	a.cd_estabelecimento = cd_estabelecimento_w;
c01_w	c01%rowtype;
	
cursor c02 is
	select	a.nm_usuario_regra
	from	medico_perm_com_usuario a
	where	a.nr_seq_com = c01_w.nr_sequencia
	order by 	1;
c02_w	c02%rowtype;
	
cursor c03 is
	select	a.cd_perfil,
		obter_desc_perfil(a.cd_perfil) ds_perfil
	from	medico_perm_com_perfil a
	where	a.nr_seq_com = c01_w.nr_sequencia
	order by 	ds_perfil desc;
c03_w	c03%rowtype;
	
begin

select	max(a.cd_pessoa_fisica),
	max(a.dt_inicio),
	max(a.dt_liberacao),
	max(a.nm_usuario_liberacao)
into	cd_pessoa_fisica_w,
	dt_inicio_w,
	dt_liberacao_w,
	nm_usuario_liberacao_w
from	medico_permissao_unica a
where	a.nr_sequencia = nr_sequencia_p;

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin

	nm_usuario_destino_w := null;
	ds_perfil_adicional_w := null;

	open c02;
	loop
	fetch c02 into
		c02_w;
	exit when c02%notfound;
	
		if	(nvl(c02_w.nm_usuario_regra,'X') <> 'X') then
			nm_usuario_destino_w := substr(nm_usuario_destino_w || c02_w.nm_usuario_regra || ', ',1,4000);
		end if;

	end loop;
	close c02;
	
	open c03;
	loop
	fetch c03 into
		c03_w;
	exit when c03%notfound;
	
		if	(nvl(c03_w.cd_perfil,0) > 0) then
			ds_perfil_adicional_w := substr(ds_perfil_adicional_w || c03_w.cd_perfil || ', ',1,4000);
		end if;

	end loop;
	close c03;	

	c01_w.ds_mensagem := substr(replace_macro(c01_w.ds_mensagem, '@MEDICO', obter_nome_pf(cd_pessoa_fisica_w)),1,4000);
	c01_w.ds_mensagem := substr(replace_macro(c01_w.ds_mensagem, '@DATA_INICIO', to_char(dt_inicio_w,'dd/mm/yyyy hh24:mi:ss')),1,4000);
	c01_w.ds_mensagem := substr(replace_macro(c01_w.ds_mensagem, '@DATA_LIBERACAO', to_char(dt_liberacao_w,'dd/mm/yyyy hh24:mi:ss')),1,4000);
	c01_w.ds_mensagem := substr(replace_macro(c01_w.ds_mensagem, '@USUARIO_LIBERACAO', obter_nome_usuario(nm_usuario_liberacao_w)),1,4000);
	
	insert into comunic_interna(	nr_sequencia,
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				ie_gerencial,
				dt_liberacao,
				nm_usuario_destino,
				ds_perfil_adicional,
				dt_atualizacao,
				nm_usuario)
			values(	comunic_interna_seq.nextval,
				sysdate,
				c01_w.ds_titulo,
				c01_w.ds_mensagem,
				'N',
				sysdate,
				nm_usuario_destino_w,
				ds_perfil_adicional_w,
				sysdate,
				nm_usuario_p);
								
	end;
end loop;
close c01;

commit;

end envia_comunic_permissao_unica;
/