Create or replace
procedure Glosar_retorno_proporcional(nr_seq_retorno_p	number,
					vl_glosa_p		number,
					ie_opcao_p		varchar2,
					nm_usuario_p		varchar2,
					ie_tipo_glosa_p		number,
					ie_libera_repasse_p	varchar2,
					ie_pago_glosa_p		varchar2,
					ie_gerar_item_glosa_p	varchar2,
					ds_lista_item_p		varchar2,
					cd_motivo_glosa_p	number,
					ie_agrupado_p           varchar2,
					ie_atualizar_motivo_glosa_p	varchar2) is

vl_guia_w		number(15,2);
nr_sequencia_w		number(10);
vl_pago_w		number(15,2);
vl_glosado_w		number(15,2) := 0;
vl_glosa_total_w	number(15,2) := 0;
vl_glosa_w		number(15,2) := 0;
ds_campo_w		varchar2(20);
vl_glosa_gerado_w	number(15,2);
ds_erro_w		varchar2(4000);
vl_pago_atual_w		number(15,2) := 0;
vl_glosado_atual_w	number(15,2) := 0;
ie_acrescenta_pago_w	varchar2(5) := 'N';
ie_acresc_glosa_w	varchar2(5) := 'N';

/*
ie_pago_glosa_p
P - Valor Pago
G - Valor de Glosa
*/

/*
ie_tipo_glosa_p
0	Aceita
1	Pendente
2	N�o
*/

cursor c01 is
	select	nr_sequencia,
		vl_pago
	from	convenio_retorno_item
	where	nr_seq_retorno		= nr_seq_retorno_p
	and	((' ' || ds_lista_item_p || ' ' like '% ' || nr_sequencia || ' %') or nvl(ds_lista_item_p,'X') = 'X');
	
cursor c02 is
	select	nr_sequencia
	from	convenio_retorno_item
	where	nr_seq_retorno		= nr_seq_retorno_p
	and	ie_gerar_item_glosa_p 	= 'S'
	and	((' ' || ds_lista_item_p || ' ' like '% ' || nr_sequencia || ' %') or nvl(ds_lista_item_p,'X') = 'X');	

begin
begin
dbms_application_info.SET_ACTION('GLOSAR_RETORNO_PROPORCIONAL'); 

select	nvl(sum(vl_pago),0)
into	vl_guia_w
from	convenio_retorno_item
where	nr_seq_retorno		= nr_seq_retorno_p
and	((' ' || ds_lista_item_p || ' ' like '% ' || nr_sequencia || ' %') or nvl(ds_lista_item_p,'X') = 'X');

vl_glosa_w	:= nvl(vl_glosa_p,0);

if	(vl_guia_w	> 0) and
	(vl_glosa_p	> 0) and
	(vl_guia_w	>= vl_glosa_p) then

	vl_glosa_gerado_w :=	vl_glosa_w * dividir_sem_round(vl_glosa_w, vl_guia_w);

	if	(dividir_sem_round(vl_glosa_w, vl_guia_w)	> 0) then
		open c01;
		loop
		fetch c01 into	nr_sequencia_w,
				vl_pago_w;
		exit when c01%notfound;
			begin

			vl_glosado_w	:= vl_pago_w * dividir_sem_round(vl_glosa_w, vl_guia_w);

			if (ie_tipo_glosa_p = 0) then
			
				update	convenio_retorno_item
				  set	vl_pago			= decode(ie_pago_glosa_p,'P', vl_glosado_w ,vl_pago - vl_glosado_w),
					vl_glosado		= decode(ie_pago_glosa_p,'P', vl_pago - vl_glosado_w ,vl_glosado + vl_glosado_w),
					ie_glosa			= 'S',
					ie_libera_repasse		= nvl(ie_libera_repasse_p, ie_libera_repasse),
					cd_motivo_glosa		= nvl(cd_motivo_glosa_p,cd_motivo_glosa) 
				where	nr_sequencia		= nr_sequencia_w
				and	nr_seq_retorno		= nr_seq_retorno_p;

			else
				update	convenio_retorno_item
				  set	vl_pago			= decode(ie_pago_glosa_p,'P', vl_glosado_w ,vl_pago - vl_glosado_w),
					vl_amenor		= decode(ie_pago_glosa_p,'P', vl_pago - vl_glosado_w ,vl_amenor + vl_glosado_w),
					ie_glosa			= decode(ie_tipo_glosa_p,1,'P','N'),
					ie_libera_repasse		= nvl(ie_libera_repasse_p, ie_libera_repasse),
					cd_motivo_glosa		= nvl(cd_motivo_glosa_p,cd_motivo_glosa)
				where	nr_sequencia		= nr_sequencia_w
				and	nr_seq_retorno		= nr_seq_retorno_p;
			end if;

			vl_glosa_total_w	:= vl_glosa_total_w + vl_glosado_w;

			end;
		end loop;
		close c01;
	end if;
	
	if	(vl_glosa_w	> vl_glosa_total_w) or
		(vl_glosa_gerado_w	> vl_glosa_total_w) or
		((vl_glosa_total_w > vl_glosa_w)) then

		if (vl_glosa_total_w > vl_glosa_w) then
		
			vl_glosa_total_w := vl_glosa_total_w - vl_glosa_w;
		else
			vl_glosa_total_w	:= vl_glosa_w - vl_glosa_total_w;
		end if;
		
		
		select	max(nr_sequencia)
		into	nr_sequencia_w
		from	convenio_retorno_item
		where	nr_seq_retorno		= nr_seq_retorno_p
		and	((' ' || ds_lista_item_p || ' ' like '% ' || nr_sequencia || ' %') or nvl(ds_lista_item_p,'X') = 'X');
		
		select	sum(vl_pago),
			sum(vl_glosado)
		into	vl_pago_atual_w,
			vl_glosado_atual_w	
		from	convenio_retorno_item
		where	nr_seq_retorno		= nr_seq_retorno_p
		and	((' ' || ds_lista_item_p || ' ' like '% ' || nr_sequencia || ' %') or nvl(ds_lista_item_p,'X') = 'X');
		
		
		if	(vl_pago_atual_w < vl_glosa_w) and
			(ie_pago_glosa_p = 'P') then
			ie_acrescenta_pago_w := 'S';
			ie_acresc_glosa_w := 'N';
		elsif	(vl_pago_atual_w > vl_glosa_w) and
			(ie_pago_glosa_p = 'P') then
			ie_acrescenta_pago_w := 'N';
			ie_acresc_glosa_w := 'S';
		elsif	(vl_glosado_atual_w < vl_glosa_w) and
			(ie_pago_glosa_p = 'G') then
			ie_acresc_glosa_w := 'S';
			ie_acrescenta_pago_w := 'N';
		elsif	(vl_glosado_atual_w > vl_glosa_w) and
			(ie_pago_glosa_p = 'G') then
			ie_acresc_glosa_w := 'N';
			ie_acrescenta_pago_w := 'S';
		end if;

		if (ie_tipo_glosa_p = 0) then

			update	convenio_retorno_item
			  set	vl_pago			= decode(ie_acrescenta_pago_w,'S',vl_pago + vl_glosa_total_w,vl_pago - vl_glosa_total_w),
				vl_glosado		= decode(ie_acresc_glosa_w,'S',vl_glosado + vl_glosa_total_w,vl_glosado - vl_glosa_total_w),
				ie_glosa			= 'S',
				ie_libera_repasse		= nvl(ie_libera_repasse_p, ie_libera_repasse),
				cd_motivo_glosa		= nvl(cd_motivo_glosa_p,cd_motivo_glosa)
			where	nr_sequencia		= nr_sequencia_w
			and	nr_seq_retorno		= nr_seq_retorno_p;
		else

			update	convenio_retorno_item
			  set	vl_pago			= decode(ie_pago_glosa_p,'P',vl_pago - vl_glosa_total_w,vl_pago - vl_glosa_total_w),
				vl_amenor		= decode(ie_pago_glosa_p,'P',vl_amenor + vl_glosa_total_w,vl_amenor + vl_glosa_total_w),
				ie_glosa			= decode(ie_tipo_glosa_p,1,'P','N'),
				ie_libera_repasse		= nvl(ie_libera_repasse_p, ie_libera_repasse),
				cd_motivo_glosa		= nvl(cd_motivo_glosa_p,cd_motivo_glosa)
			where	nr_sequencia		= nr_sequencia_w
			and	nr_seq_retorno		= nr_seq_retorno_p;
		end if;

	end if;
	
end if;

wheb_usuario_pck.set_ie_executar_trigger('N');

open c02;
loop
fetch c02 into	nr_sequencia_w;
exit when c02%notfound;
begin
	Gerar_Retorno_Glosa_Conta (nr_sequencia_w,nm_usuario_p,'G',ie_agrupado_p,1,'S','S','S','S','S','S','S',null,cd_motivo_glosa_p,null,null);
end;
end loop;
close c02;

if	(ie_atualizar_motivo_glosa_p = 'S') then

	update    convenio_retorno_glosa a
	set       a.cd_motivo_glosa 	= cd_motivo_glosa_p
	where     a.nr_seq_ret_item 	in 
					(select	x.nr_sequencia		
					from	convenio_retorno_item x
					where	x.nr_seq_retorno	= nr_seq_retorno_p
					and	((' ' || ds_lista_item_p || ' ' like '% ' || x.nr_sequencia || ' %') or nvl(ds_lista_item_p,'X') = 'X'));	

end if;

wheb_usuario_pck.set_ie_executar_trigger('S');

dbms_application_info.SET_ACTION('');

exception
	when others then
	dbms_application_info.SET_ACTION('');
	ds_erro_w	:= sqlerrm;
	--r.aise_application_error(-20011,sqlerrm);
	wheb_mensagem_pck.exibir_mensagem_abort(263395,'ds_erro_w='||ds_erro_w);
end;


commit;

end Glosar_retorno_proporcional;
/