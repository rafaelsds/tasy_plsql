create or replace
procedure pls_baixar_tit_rec_lote_pag
		(nr_seq_lote_p			pls_lote_pagamento.nr_sequencia%type,
		 nr_seq_pagamento_prest_p	pls_pagamento_prestador.nr_sequencia%type,
		 ie_acao_p			varchar2,
		 nm_usuario_p			usuario.nm_usuario%type) is

/*
ie_acao_p
	'F' - Fechamento do lote
	'A' - Reabertura do Lote
*/

cd_tipo_receb_pgto_w			pls_parametro_pagamento.cd_tipo_receb_pgto%type;
nr_seq_trans_fin_rec_pgto_w		pls_parametro_pagamento.nr_seq_trans_fin_rec_pgto%type;
cd_estabelecimento_w			pls_parametro_pagamento.cd_estabelecimento%type;
nr_seq_baixa_w				titulo_receber_liq.nr_sequencia%type;
nr_seq_liq_origem_w			titulo_receber_liq.ie_origem_baixa%type;

Cursor C01 (	nr_seq_lote_cp			pls_lote_pagamento.nr_sequencia%type) is
		select	b.nr_sequencia
		from	pls_pagamento_item 	b,
			pls_pagamento_prestador a
		where	a.nr_sequencia		= b.nr_seq_pagamento
		and	a.nr_seq_lote		= nr_seq_lote_cp;

Cursor C02 (	nr_seq_pagamento_item_cp	pls_pagamento_item.nr_sequencia%type,
		nr_seq_lote_cp			pls_lote_pagamento.nr_sequencia%type) is
		select	a.nr_titulo,
			decode(ie_acao_p, 'F', a.vl_titulo, a.vl_titulo * -1) vl_titulo
		from	pls_pagamento_prestador 	c,
			pls_pagamento_item 		b,
			titulo_receber			a
		where	a.nr_seq_pag_conta_evento	= nr_seq_pagamento_item_cp
		and	a.nr_seq_pag_conta_evento	= b.nr_sequencia
		and	b.nr_seq_pagamento		= c.nr_sequencia
		and	c.nr_sequencia			= nvl(nr_seq_pagamento_prest_p, c.nr_sequencia)
		and	c.ie_cancelamento		is null
		union all
		select	b.nr_titulo,
			decode(ie_acao_p, 'F', abs(a.vl_movimento), abs(a.vl_movimento) * -1) vl_titulo
		from	pls_pagamento_prestador 	d,
			pls_pagamento_item 		c,
			titulo_receber			b,
			pls_evento_movimento 		a
		where	b.nr_seq_pag_conta_evento	is null
		and	a.nr_tit_rec_vinculado		= b.nr_titulo
		and	a.nr_seq_pagamento_item		= c.nr_sequencia
		and	c.nr_seq_pagamento		= d.nr_sequencia
		and	d.nr_sequencia			= nvl(nr_seq_pagamento_prest_p, d.nr_sequencia)
		and	d.ie_cancelamento		is null
		and	a.nr_seq_lote_pgto		= nr_seq_lote_cp
		union all
		select	c.nr_titulo,
			decode(ie_acao_p, 'F', abs(b.vl_item), abs(b.vl_item) * -1) vl_titulo
		from	pls_pagamento_prestador a,
			pls_pagamento_item 	b,
			titulo_receber 		c
		where 	a.nr_sequencia 		= b.nr_seq_pagamento
		and   	c.nr_titulo 		= b.nr_tit_receber_origem
		and   	a.nr_sequencia 		= nvl(nr_seq_pagamento_prest_p, a.nr_sequencia)
		and	a.nr_seq_lote 		= nr_seq_lote_cp
		and not exists(	select 	1
				from   	pls_evento_movimento x
				where  	x.nr_seq_pagamento_item = b.nr_sequencia)
		and   	a.ie_cancelamento is null
		and	c.nr_seq_pag_conta_evento is null;

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	pls_lote_pagamento
where	nr_sequencia		= nr_seq_lote_p;

select	max(cd_tipo_receb_pgto),
	max(nr_seq_trans_fin_rec_pgto)
into	cd_tipo_receb_pgto_w,
	nr_seq_trans_fin_rec_pgto_w
from	pls_parametro_pagamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(cd_tipo_receb_pgto_w is null) or 
	(nr_seq_trans_fin_rec_pgto_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(312374);
	-- N�o � poss�vel realizar este processo!
	-- Verifique se os campos "Trans fin receb pag produ��o" e "Tipo receb pag produ��o" est�o informados nos par�metros do pagamento de produ��o (OPS - Gest�o de Operadoras -> Par�metros OPS -> Pagamento produ��o m�dica).
end if;

for r_c01_w in C01(nr_seq_lote_p) loop

	for r_c02_w in C02(r_c01_w.nr_sequencia, null) loop

		baixa_titulo_receber
			(cd_estabelecimento_w,
			cd_tipo_receb_pgto_w,
			r_c02_w.nr_titulo,
			nr_seq_trans_fin_rec_pgto_w,
			r_c02_w.vl_titulo,
			sysdate,
			nm_usuario_p,
			0,
			null, 
			null,
			0,
			0);
		
		-- Atualiza a observa��o da baixa informando o lote de pagamento que gerou a mesma
		begin
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_baixa_w
			from	titulo_receber_liq
			where	nr_titulo	= r_c02_w.nr_titulo;
			
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_liq_origem_w
			from	titulo_receber_liq
			where	nr_titulo = r_c02_w.nr_titulo
			and	ie_origem_baixa <> 'ET'
			and	nr_sequencia <> nr_seq_baixa_w;
			
			update	titulo_receber_liq
			set	ds_observacao		= 'Baixa gerada atrav�s do lote, ' || nr_seq_lote_p || ', de pagamento de produ��o m�dica.',
				nr_seq_liq_origem	= decode(ie_acao_p, 'A', nr_seq_liq_origem_w, nr_seq_liq_origem),
				ie_origem_baixa		= decode(ie_acao_p, 'A', 'ET', ie_origem_baixa)
			where	nr_sequencia		= nr_seq_baixa_w
			and	nr_titulo		= r_c02_w.nr_titulo;			
		exception
		when others then
			null;
		end;
		
		Atualizar_Saldo_Tit_Rec(r_c02_w.nr_titulo, nm_usuario_p);

	end loop;

end loop;

for r_c02_w in C02(null, nr_seq_lote_p) loop

	baixa_titulo_receber
		(cd_estabelecimento_w,
		cd_tipo_receb_pgto_w,
		r_c02_w.nr_titulo,
		nr_seq_trans_fin_rec_pgto_w,
		r_c02_w.vl_titulo,
		sysdate,
		nm_usuario_p,
		0,
		null, 
		null,
		0,
		0);

	-- Atualiza a observa��o da baixa informando o lote de pagamento que gerou a mesma
	begin
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_baixa_w
		from	titulo_receber_liq
		where	nr_titulo	= r_c02_w.nr_titulo;
		
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_liq_origem_w
		from	titulo_receber_liq
		where	nr_titulo = r_c02_w.nr_titulo
		and	ie_origem_baixa <> 'ET'
		and	nr_sequencia <> nr_seq_baixa_w;
		
		update	titulo_receber_liq
		set	ds_observacao		= 'Baixa gerada atrav�s do lote, ' || nr_seq_lote_p || ', de pagamento de produ��o m�dica.',
			nr_seq_liq_origem	= decode(ie_acao_p, 'A', nr_seq_liq_origem_w, nr_seq_liq_origem),
			ie_origem_baixa		= decode(ie_acao_p, 'A', 'ET', ie_origem_baixa)
		where	nr_sequencia		= nr_seq_baixa_w
		and	nr_titulo		= r_c02_w.nr_titulo;
	exception
	when others then
		null;
	end;
		
	Atualizar_Saldo_Tit_Rec(r_c02_w.nr_titulo, nm_usuario_p);
end loop;

end pls_baixar_tit_rec_lote_pag;
/