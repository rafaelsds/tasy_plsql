create or replace
procedure INTCOM_GERAR_ENTREGA_ITEM_COT(
			nr_cot_compra_p			number,
			nr_item_cot_compra_p		number,
			nr_solic_compra_p		number,
			nr_item_solic_compra_p		number,
			cd_cnpj_p			varchar2, /*CNPJ do Item que est� sendo inserido (CNPJ da Tag Resposta)*/
			qt_item_p				number,
			dt_entrega_p			date,
			ie_forma_calc_entrega_p		varchar2,
			nm_usuario_p			varchar2) is
			
/*ie_forma_calc_entrega_p
A               Data atual + Dias entrega fornecedior
F               Data limite cota��o
L               Data limite cota��o + Dias entrega fornecedor*/

nr_seq_cot_forn_item_w		cot_compra_forn_item.nr_sequencia%type;
nr_seq_cot_forn_item_uniq_w	cot_compra_forn_item.nr_sequencia%type;
qt_conv_compra_estoque_w		number(13,4);
qt_item_w			number(13,4);
cd_estabelecimento_w		number(10,0);
cd_estab_solic_w		number(10,0);
cd_material_w			number(10);
qt_erro_w			number(10) := 0;
ds_erro_w			varchar2(2000) := '';
nr_seq_registro_w		number(10);
ds_login_w			varchar2(255);
ds_senha_w			varchar2(255);
nr_documento_externo_w		varchar2(100);
ds_historico_w			varchar2(2000);
nr_seq_entrega_w		number(10);
qt_material_w			number(13,4);
qt_prevista_entrega_w		number(13,4);
qt_existe_w			number(10);

begin

qt_item_w		:= qt_item_p;
ds_historico_w		:= 	wheb_mensagem_pck.get_texto(303449,'NR_ITEM_COT_COMPRA_P='||nr_item_cot_compra_p||';CD_CNPJ_P='||cd_cnpj_p||';QT_ITEM_P='||qt_item_p||';DT_ENTREGA_P='||dt_entrega_p);

gerar_historico_cotacao(	nr_cot_compra_p,
				wheb_mensagem_pck.get_texto(303453),
				ds_historico_w,
				'U',
				nm_usuario_p);


select	cd_estabelecimento,
	nr_documento_externo
into	cd_estabelecimento_w,
	nr_documento_externo_w
from	cot_compra
where	nr_cot_compra	= nr_cot_compra_p;

if	(nr_cot_compra_p > 0) and
	(nr_item_cot_compra_p > 0) then

	begin
	
	select	nvl(max(cd_material),0)
	into	cd_material_w
	from	cot_compra_item
	where	nr_cot_compra		= nr_cot_compra_p
	and	nr_item_cot_compra	= nr_item_cot_compra_p;
	
	if	(cd_material_w > 0) then
	
		select	nvl(max(qt_conv_compra_estoque),0)
		into	qt_conv_compra_estoque_w
		from	material
		where	cd_material = cd_material_w;
	
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_cot_forn_item_w
		from	cot_compra_forn_item
		where	nr_cot_compra		= nr_cot_compra_p
		and	nr_item_cot_compra	= nr_item_cot_compra_p
		and	cd_cgc_fornecedor	= cd_cnpj_p;
		
		if	(dt_entrega_p is not null) then
		
			select	nvl(min(nr_sequencia),0)
			into	nr_seq_cot_forn_item_uniq_w
			from	cot_compra_forn_item
			where	nr_cot_compra		= nr_cot_compra_p
			and	nr_item_cot_compra	= nr_item_cot_compra_p;
		
			if	((nr_seq_cot_forn_item_uniq_w = 0) or
					((nr_seq_cot_forn_item_uniq_w > 0) and
					(nr_seq_cot_forn_item_w = nr_seq_cot_forn_item_uniq_w))) and
				(nr_solic_compra_p > 0) and
				(nr_item_solic_compra_p > 0) then
				
				begin
				update	solic_compra_item
				set	nr_cot_compra		= nr_cot_compra_p,
					nr_item_cot_compra	= nr_item_cot_compra_p
				where	nr_solic_compra		= nr_solic_compra_p
				and	nr_item_solic_compra	= nr_item_solic_compra_p;
				exception
				when others then
					qt_erro_w	:= 1;
					ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(303454) || sqlerrm,1,2000);
				end;
				
				select	nvl(max(cd_estabelecimento),0)
				into	cd_estab_solic_w
				from	solic_compra
				where	nr_solic_compra = nr_solic_compra_p;
				
				select	count(*)
				into	qt_existe_w
				from	cot_compra_solic_agrup
				where	nr_solic_compra = nr_solic_compra_p
				and	nr_item_solic_compra = nr_item_solic_compra_p
				and	nr_cot_compra = nr_cot_compra_p
				and	nr_item_cot_compra = nr_item_cot_compra_p;
				
				
				if	(qt_existe_w > 0) then
					
					update	cot_compra_solic_agrup
					set	qt_material = qt_material + qt_item_w
					where	nr_solic_compra = nr_solic_compra_p
					and	nr_item_solic_compra = nr_item_solic_compra_p
					and	nr_cot_compra = nr_cot_compra_p
					and	nr_item_cot_compra = nr_item_cot_compra_p;
				
				elsif	(qt_existe_w = 0) and
					(cd_estab_solic_w > 0) and
					(qt_erro_w = 0) then
			
					begin
					insert into cot_compra_solic_agrup(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_cot_compra,
						nr_item_cot_compra,
						nr_solic_compra,
						nr_item_solic_compra,
						qt_material,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_estabelecimento)
					values(	cot_compra_solic_agrup_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_cot_compra_p,
						nr_item_cot_compra_p,
						nr_solic_compra_p,
						nr_item_solic_compra_p,
						qt_item_w,
						sysdate,
						nm_usuario_p,
						cd_estab_solic_w);
					exception
					when others then
						qt_erro_w	:= 1;
						ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(303456) || sqlerrm,1,2000);
					end;					
				end if;
				
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_entrega_w
				from	cot_compra_item_entrega
				where	nr_cot_compra = nr_cot_compra_p
				and	nr_item_cot_compra = nr_item_cot_compra_p
				and	trunc(dt_entrega,'dd') = trunc(dt_entrega_p,'dd');
				
				
				select	nvl(max(qt_material),0)
				into	qt_material_w
				from	cot_compra_item
				where	nr_cot_compra		= nr_cot_compra_p
				and	nr_item_cot_compra	= nr_item_cot_compra_p;
				
				select	nvl(sum(qt_entrega),0)
				into	qt_prevista_entrega_w
				from	cot_compra_item_entrega
				where	nr_cot_compra		= nr_cot_compra_p
				and	nr_item_cot_compra	= nr_item_cot_compra_p;
				
				if	(qt_prevista_entrega_w < qt_material_w) then				
					begin
				
					if	(nr_seq_entrega_w = 0) and
						(qt_erro_w = 0) then
						begin
						insert into cot_compra_item_entrega(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_cot_compra,
							nr_item_cot_compra,
							dt_entrega,
							qt_entrega)
						values(	cot_compra_item_entrega_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_cot_compra_p,
							nr_item_cot_compra_p,
							dt_entrega_p,
							qt_item_w);
						exception
						when others then
							qt_erro_w	:= 1;
							ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(303457) || sqlerrm,1,2000);
						end;
						
					else
						begin
						update	cot_compra_item_entrega
						set	qt_entrega = qt_entrega + qt_item_w
						where	nr_sequencia = nr_seq_entrega_w;
						exception
						when others then
							qt_erro_w	:= 1;
							ds_erro_w	:= substr( wheb_mensagem_pck.get_texto(303457) || sqlerrm,1,2000);
						end;
					end if;
					end;					
				end if;
			end if;	
		
			if	(obter_dados_parametro_compras(cd_estabelecimento_w, 24) = 'S') and
				(qt_conv_compra_estoque_w > 0) then
				qt_item_w := dividir(qt_item_p, qt_conv_compra_estoque_w);
			end if;
			
			if	(nr_seq_cot_forn_item_w > 0) then
				
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_entrega_w
				from	cot_compra_forn_item_en
				where	nr_seq_cot_item_forn = nr_seq_cot_forn_item_w
				and	trunc(dt_entrega,'dd') = trunc(dt_entrega_p,'dd');
			
				if	(nr_seq_entrega_w = 0) then
				
					begin
					insert into cot_compra_forn_item_en (
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_cot_item_forn,
						dt_entrega,
						qt_entrega,
						nr_cot_compra,
						nr_item_cot_compra)
					values(	cot_compra_forn_item_en_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_cot_forn_item_w,
						dt_entrega_p,
						qt_item_w,
						nr_cot_compra_p,
						nr_item_cot_compra_p);
					exception
					when others then
						qt_erro_w	:= 1;
						ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(303458) || sqlerrm,1,2000);
					end;
				else
					begin
					update	cot_compra_forn_item_en
					set	qt_entrega = qt_entrega + qt_item_w
					where	nr_seq_cot_item_forn = nr_seq_cot_forn_item_w
					and	trunc(dt_entrega,'dd') = trunc(dt_entrega_p,'dd');				
					exception
					when others then
						qt_erro_w	:= 1;
						ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(303458) || sqlerrm,1,2000);
					end;
				end if;
			end if;
		end if;	
	end if;
	end;
end if;	

if	(qt_erro_w > 0) then
	
	rollback;
	select	registro_integr_compras_seq.nextval
	into	nr_seq_registro_w
	from	dual;

	select	a.ds_login_integr_compras_ws,
		a.ds_senha_integr_compras_ws
	into	ds_login_w,
		ds_senha_w
	from	parametro_compras a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;

	insert into registro_integr_compras(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_operacao)
	values(	nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,	
		'WDG');

	insert into registro_integr_com_xml(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_registro,
		ie_status,
		ie_operacao,
		ie_sistema_origem,
		ds_erro,
		ds_retorno,
		ie_tipo_operacao,
		ds_login,
		ds_senha,
		id_pdc)
	values(	registro_integr_com_xml_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_registro_w,
		'E',
		'R',
		'TASY',
		ds_erro_w,
		null,
		'WDG',
		ds_login_w,
		ds_senha_w,
		nr_documento_externo_w);
	commit;
	
	wheb_mensagem_pck.exibir_mensagem_abort(182482,'DS_ERRO_P='||DS_ERRO_W);
end if;

commit;

end intcom_gerar_entrega_item_cot;
/