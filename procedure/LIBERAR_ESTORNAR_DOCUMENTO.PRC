create or replace
procedure liberar_estornar_documento
			(	nr_seq_doc_p		number,
				ie_opcao_p		varchar2,
				nm_usuario_p		varchar2) is 

/*
ie_opcao_p
LD - Liberar registro Documento.
ED - Estornar Documento;
*/
	
begin
if	(ie_opcao_p = 'LD') then
	update	gestao_documento
	set	dt_liberacao 	= sysdate,
		nm_usuario 	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia 	= nr_seq_doc_p;
elsif	(ie_opcao_p = 'ED') then
	update	gestao_documento
	set	dt_liberacao 	= null,
		nm_usuario 	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia 	= nr_seq_doc_p;
end if;

commit;

end liberar_estornar_documento;
/