create or replace procedure insere_ehr_template_philips(nr_seq_template_p ehr_template.nr_sequencia%type,
                                                        ds_titulo_p    ehr_template_philips.ds_titulo%type,
                                                        ds_descricao_p ehr_template_philips.ds_descricao%type,
                                                        ds_template_p  ehr_template_philips.ds_template%type,
                                                        nm_usuario_p   ehr_template_philips.nm_usuario%type default wheb_usuario_pck.get_nm_usuario) is
nr_seq_philips_w   ehr_template_philips.nr_seq_philips%type;

begin

        select  nr_seq_philips
        into    nr_seq_philips_w
        from    ehr_template
        where   nr_sequencia = nr_seq_template_p;

        insert into ehr_template_philips
        (nr_sequencia,
         dt_atualizacao_nrec,
         nm_usuario_nrec,
         ds_titulo,
         ds_descricao,
         ds_template,
         nr_seq_philips)
        values
        (ehr_template_philips_seq.nextval,
         sysdate,
         nm_usuario_p,
         ds_titulo_p,
         ds_descricao_p,
         ds_template_p,
         nr_seq_philips_w);
        
        commit;
end;
/
