create or replace
procedure pls_verificar_eventos_atend(	nr_seq_atendimento_p		pls_atendimento.nr_sequencia%type,
					nr_seq_evento_atend_p		pls_atendimento_evento.nr_sequencia%type,
					nm_usuario_p			Varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a valida��o e o update do �ltimo evento do atendimento que esteja com o est�gio "Em atendimento"
quando for criado um novo evento.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [   ] Portal [  ]  Relat�rios [   ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
					
nr_seq_evento_atend_w		pls_atendimento_evento.nr_sequencia%type;
qt_pendencia_w			number(5);

begin

select	max(nr_sequencia)
into	nr_seq_evento_atend_w
from	pls_atendimento_evento
where	nr_seq_atendimento	= nr_seq_atendimento_p
and	nr_sequencia		<> nr_seq_evento_atend_p
and	ie_status		= 'A';

if	(nr_seq_evento_atend_w is not null) then

	select	count(1)
	into	qt_pendencia_w
	from	pls_atend_evt_pend_resp
	where	nr_seq_atend_evento	= nr_seq_evento_atend_w
	and	dt_solucao		is null;
	
	if	(qt_pendencia_w > 0) then
		update	pls_atendimento_evento
		set	ie_status	= 'P',
			ds_observacao	= ds_observacao|| ' Evento alterado para Pendente pelo sistema.',
			dt_fim_evento 	= sysdate,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_evento_atend_w;
	else	
		update	pls_atendimento_evento
		set	ie_status	= 'C',
			ds_observacao	= ds_observacao|| ' Evento conclu�do pelo sistema.',
			dt_fim_evento 	= sysdate,
			dt_conclusao	= sysdate,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_evento_atend_w;
	end if;
	
	
end if;

commit;

end pls_verificar_eventos_atend;
/
