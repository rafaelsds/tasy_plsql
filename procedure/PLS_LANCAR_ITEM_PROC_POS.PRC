create or replace
procedure pls_lancar_item_proc_pos
				(	nr_seq_conta_p		pls_conta.nr_sequencia%type,
					nm_usuario_p		Varchar2) is 
					
nr_seq_conta_proc_w			pls_conta.nr_sequencia%type;
nr_seq_conta_proc_nova_w		pls_conta.nr_sequencia%type;
nr_seq_prestador_prot_w			pls_protocolo_conta.nr_seq_prestador%type;
nr_seq_prestador_exec_w			pls_conta.nr_seq_prestador_exec%type;
ie_tipo_guia_w				pls_conta.ie_tipo_guia%type;   
cd_medico_exec_w			pls_conta.cd_medico_executor%type;
cd_medico_solicitante_w			pls_conta.cd_medico_solicitante%type;
nr_seq_prestador_solic_w		pls_conta.nr_seq_prestador_solic_ref%type;
ie_preco_w				pls_plano.ie_preco%type;
dt_atendimento_referencia_w		pls_conta.dt_atendimento_referencia%type;
qt_liberada_w				pls_conta_proc.qt_procedimento%type;
dt_procedimento_w			pls_conta_proc.dt_procedimento%type;
dt_inicio_proc_w			pls_conta_proc.dt_inicio_proc%type;
dt_fim_proc_w				pls_conta_proc.dt_fim_proc%type;
ie_tipo_despesa_w			pls_conta_proc.ie_tipo_despesa%type;
ie_medico_exec_coope_w			varchar2(10);
ie_medico_solic_coope_w			varchar2(10);
cd_medico_proc_w			pls_proc_participante.cd_medico%type;
nr_seq_regra_cooperado_w		pls_conta_proc.nr_seq_regra_cooperado%type;
ie_ato_cooperado_w			pls_conta_proc.ie_ato_cooperado%type;
qt_registros_w				pls_integer;
qt_participantes_w			pls_integer;
ie_insere_particpante_w			varchar2(10);
nr_seq_grau_partic_anest_w		pls_proc_participante.nr_seq_grau_partic%type;
ie_repassa_medico_w			pls_conta_proc.ie_repassa_medico%type;
nr_seq_honorario_crit_w			pls_conta_proc.nr_seq_honorario_crit%type;
ie_criterio_horario_w			pls_conta_proc.ie_criterio_horario%type;
tx_item_w				pls_conta_proc.tx_item%type;
nr_seq_analise_w			pls_conta.nr_seq_analise%type;
nr_seq_prestador_w			pls_protocolo_conta.nr_seq_prestador%type;
nr_seq_congenere_w			pls_segurado.nr_seq_congenere%type;
ie_nao_lanca_w				boolean;
nr_seq_segurado_w			pls_segurado.nr_sequencia%type;
nr_seq_contrato_w			pls_contrato.nr_sequencia%type;
nr_seq_intercambio_w			pls_intercambio.nr_sequencia%type;
nr_seq_grau_partic_w			pls_conta.nr_seq_grau_partic%type;
ie_tipo_protocolo_w			pls_protocolo_conta.ie_tipo_protocolo%type;
ie_origem_conta_w			pls_conta.ie_origem_conta%type;
nr_seq_ajuste_fat_w			pls_conta.nr_seq_ajuste_fat%type;

Cursor c_regra (dt_atendimento_referencia_pc		pls_conta.dt_atendimento_referencia%type,
		nr_seq_contrato_pc			pls_contrato.nr_sequencia%type,
		nr_seq_intercambio_pc			pls_intercambio.nr_sequencia%type) is
	select	a.nr_sequencia,
		a.nr_seq_grupo_prestador,
		a.nr_seq_grupo_operadora,
		a.ie_considera_todos_itens,
		(select count(1) 
		from	pls_conv_item_princ b
		where 	b.nr_seq_regra = a.nr_sequencia) qt_itens
	from	pls_conv_item_fat a
	where	dt_atendimento_referencia_pc between dt_inicio_vigencia_ref and dt_fim_vigencia_ref
	and	((a.nr_seq_contrato is null) or (a.nr_seq_contrato = nr_seq_contrato_pc))
	and	((a.nr_seq_intercambio is null) or (a.nr_seq_intercambio = nr_seq_intercambio_pc))
	order by 
		qt_itens desc,
		nr_seq_contrato,
		nr_seq_intercambio,
		nr_seq_grupo_prestador,
		nr_seq_grupo_operadora;		

Cursor C00 (	nr_seq_conta_pc		pls_conta.nr_sequencia%type,
		cd_procedimento_pc	pls_conta_proc.cd_procedimento%type,
		ie_origem_proced_pc	pls_conta_proc.ie_origem_proced%type) is
	select	cd_procedimento,
		ie_origem_proced,
		tx_item,
		trunc(dt_procedimento_referencia,'dd') dt_procedimento
	from	pls_conta_proc
	where	nr_seq_conta		= nr_seq_conta_pc
	and	ie_status		in ('L','S')
	and	cd_procedimento		= cd_procedimento_pc
	and	ie_origem_proced	= ie_origem_proced_pc
	and	nr_seq_regra_conv 	is null
	group by cd_procedimento,
		ie_origem_proced,
		tx_item,
		trunc(dt_procedimento_referencia,'dd');

Cursor C01 ( 	nr_seq_analise_pc	pls_conta.nr_seq_analise%type,
		nr_seq_regra_pc		pls_conv_item_fat.nr_sequencia%type) is
	select	a.cd_procedimento,
		a.ie_origem_proced,
		a.ie_desc_grau_anest,
		a.nr_seq_regra,
		a.nr_seq_grupo_material
	from	pls_conv_item_princ	a
	where	a.nr_seq_regra		= nr_seq_regra_pc
	and	exists (select	1
			from	pls_conta_proc_v	x
			where	x.nr_seq_analise	= nr_seq_analise_pc
			and	x.ie_status		in ('L','S')
			and	x.cd_procedimento	= a.cd_procedimento
			and	x.ie_origem_proced	= a.ie_origem_proced);
				
Cursor C02 (nr_seq_regra_pc	pls_conv_item_fat.nr_sequencia%type) is
	select	ie_origem_proced,
		cd_procedimento,
		nr_seq_grau_partic,
		ie_qt_item_princ
	from	pls_conv_item_conv
	where	nr_seq_regra	= nr_seq_regra_pc;
	
Cursor C03 (	cd_procedimento_pc	pls_conta_proc.cd_procedimento%type,
		ie_origem_proced_pc	pls_conta_proc.ie_origem_proced%type) is
	select	nr_sequencia,
		nr_seq_grau_partic_conta
	from	pls_conta_proc_v
	where	nr_seq_analise			= nr_seq_analise_w
	and	ie_status			in ('L','S')
	and	cd_procedimento			= cd_procedimento_pc
	and	ie_origem_proced		= ie_origem_proced_pc
	and	dt_procedimento_trunc		= dt_procedimento_w
	and	tx_item				= tx_item_w
	and	nr_sequencia			<> nr_seq_conta_proc_w;
	
Cursor C04  (nr_seq_regra_pc	pls_conv_item_fat.nr_sequencia%type) is
	select	nr_seq_grupo_prestador,
		nr_seq_grupo_operadora
	from	pls_conv_item_fat_exce
	where	nr_seq_regra	= nr_seq_regra_pc;
	
cursor c05 (nr_seq_grupo_material_pc	pls_preco_material.nr_seq_grupo%type) is
	select	nr_seq_material
	from	pls_preco_material
	where	nr_seq_grupo = nr_seq_grupo_material_pc
	and	nr_seq_material is not null;
	
Cursor C06 (	nr_seq_analise_pc	pls_conta.nr_seq_analise%type,
		nr_seq_regra_pc		pls_conv_item_fat.nr_sequencia%type) is
	select	(select	count(1)
		 from	pls_conta_proc_v	x
		 where	x.nr_seq_analise	= nr_seq_analise_pc
		 and	x.ie_status		in ('L','S')
		 and	x.cd_procedimento	= a.cd_procedimento
		 and	x.ie_origem_proced	= a.ie_origem_proced) qt_itens_regra,
		a.cd_procedimento
	from	pls_conv_item_princ a
	where	a.nr_seq_regra		= nr_seq_regra_pc;
	
begin

select 	max(nr_seq_ajuste_fat)
into	nr_seq_ajuste_fat_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_p;

--Se a conta for oriunda do ajuste de faturamento, ent?o n?o exclui os registros e nem o procedimento
if	(nr_seq_ajuste_fat_w is  null) then

	delete 	pls_analise_fluxo_ocor
	where 	NR_SEQ_FLUXO_ITEM     in (	select 	nr_sequencia
						from	pls_analise_fluxo_item
						where	nr_seq_conta_proc	in (    select 	nr_sequencia
											from	pls_conta_proc
											where	ie_status	= 'M'
											and	nr_seq_conta	= nr_seq_conta_p
											and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null))));
				
	delete	pls_analise_fluxo_item
	where	nr_seq_conta_proc	in (    select 	nr_sequencia
						from	pls_conta_proc
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)));
				
	delete	pls_ocorrencia_benef
	where	nr_seq_conta_proc	in (    select 	nr_sequencia
						from	pls_conta_proc
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)));

	delete	pls_fatura_proc 
	where	nr_seq_conta_proc	in (    select 	nr_sequencia
						from	pls_conta_proc
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)));

	delete 	pls_fatura_mat		
	where	nr_seq_conta_mat	in (    select 	nr_sequencia
						from	pls_conta_mat
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)));
						
	delete	pls_hist_analise_conta
	where	nr_seq_conta_proc	in (    select 	nr_sequencia
						from	pls_conta_proc
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)));
						
	delete	pls_hist_analise_conta
	where	nr_seq_conta_mat	in (    select 	nr_sequencia
						from	pls_conta_mat
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)));
						
	delete 	pls_analise_fluxo_ocor
	where 	NR_SEQ_FLUXO_ITEM     in (	select 	nr_sequencia
						from	pls_analise_fluxo_item
						where	nr_seq_conta_mat	in (    select 	nr_sequencia
											from	pls_conta_mat
											where	ie_status	= 'M'
											and	nr_seq_conta	= nr_seq_conta_p
											and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null))));
				
	delete	pls_analise_fluxo_item
	where	nr_seq_conta_mat	in (    select 	nr_sequencia
						from	pls_conta_mat
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)));
				
	delete	pls_ocorrencia_benef
	where	nr_seq_conta_mat	in (    select 	nr_sequencia
						from	pls_conta_mat
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)));


	delete	pls_conta_pos_estab_contab	a
	where	a.nr_seq_conta_proc in (  select 	nr_sequencia
					from	pls_conta_proc
					where	ie_status	= 'M'
					and	nr_seq_conta	= nr_seq_conta_p
					and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)))
	and	a.nr_seq_conta	= nr_seq_conta_p
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido	x
				where	x.nr_sequencia			= a.nr_seq_conta_pos
				and	x.ie_status_faturamento		= 'A'
				and	x.ie_cobrar_mensalidade		= 'A');
					
	delete	pls_conta_pos_estab_contab	a
	where	a.nr_seq_conta_mat in (	select 	nr_sequencia
					from	pls_conta_mat
					where	ie_status	= 'M'
					and	nr_seq_conta	= nr_seq_conta_p
					and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)))
	and	a.nr_seq_conta	= nr_seq_conta_p
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido	x
				where	x.nr_sequencia			= a.nr_seq_conta_pos
				and	x.ie_status_faturamento		= 'A'
				and	x.ie_cobrar_mensalidade		= 'A');
	
	delete	pls_conta_pos_estab_taxa
	where	nr_seq_conta_pos_estab in (	select	nr_sequencia
						from	pls_conta_pos_estabelecido
						where	nr_seq_conta_proc in (  select 	nr_sequencia
										from	pls_conta_proc
										where	ie_status	= 'M'
										and	nr_seq_conta	= nr_seq_conta_p
										and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)))
						and	ie_status_faturamento		!= 'A'
						and	ie_cobrar_mensalidade		!= 'A');
										
	delete	pls_conta_pos_estab_taxa
	where	nr_seq_conta_pos_estab in (	select	nr_sequencia
						from	pls_conta_pos_estabelecido
						where	nr_seq_conta_mat in (  select 	nr_sequencia
										from	pls_conta_mat
										where	ie_status	= 'M'
										and	nr_seq_conta	= nr_seq_conta_p
										and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)))
						and	ie_status_faturamento		!= 'A'
						and	ie_cobrar_mensalidade		!= 'A');
	
	delete 	pls_conta_pos_estabelecido
	where	nr_seq_conta_proc in (  select 	nr_sequencia
					from	pls_conta_proc
					where	ie_status	= 'M'
					and	nr_seq_conta	= nr_seq_conta_p
					and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)))
	and	ie_status_faturamento		!= 'A'
	and	ie_cobrar_mensalidade		!= 'A';

	delete 	pls_conta_pos_estabelecido
	where	nr_seq_conta_mat in (  select 	nr_sequencia
					from	pls_conta_mat
					where	ie_status	= 'M'
					and	nr_seq_conta	= nr_seq_conta_p
					and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)))
	and	ie_status_faturamento		!= 'A'
	and	ie_cobrar_mensalidade		!= 'A';
	
	delete 	pls_conta_proc	p
	where	p.nr_seq_proc_princ	in (	select	nr_sequencia
						from	pls_conta_proc
						where	ie_status	= 'M'
						and	nr_seq_conta	= nr_seq_conta_p
						and	((ie_lanc_manual_pos is null) or (ie_lanc_manual_pos = 'N') or (nr_seq_regra_conv is not null)))
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido	x
				where	x.nr_seq_conta_proc		= p.nr_sequencia
				and	x.nr_seq_conta			= p.nr_seq_conta
				and	x.ie_status_faturamento		= 'A'
				and	x.ie_cobrar_mensalidade		= 'A');
	delete 	pls_conta_proc	p
	where	p.ie_status	= 'M'
	and	p.nr_seq_conta	= nr_seq_conta_p
	and	((p.ie_lanc_manual_pos is null) or (p.ie_lanc_manual_pos = 'N') or (p.nr_seq_regra_conv is not null))
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido	x
				where	x.nr_seq_conta_proc		= p.nr_sequencia
				and	x.nr_seq_conta			= p.nr_seq_conta
				and	x.ie_status_faturamento		= 'A'
				and	x.ie_cobrar_mensalidade		= 'A')
	and not exists (select 1
					from	pls_conta_pos_estab_prev x
					where 	nr_seq_conta_proc = p.nr_sequencia);

	delete 	pls_conta_mat	m
	where	m.ie_status	= 'M'
	and	m.nr_seq_conta	= nr_seq_conta_p
	and	((m.ie_lanc_manual_pos is null) or (m.ie_lanc_manual_pos = 'N') or (m.nr_seq_regra_conv is not null))
	and	not exists (	select	1
				from	pls_conta_pos_estabelecido	x
				where	x.nr_seq_conta_mat		= m.nr_sequencia
				and	x.nr_seq_conta			= m.nr_seq_conta
				and	x.ie_status_faturamento		= 'A'
				and	x.ie_cobrar_mensalidade		= 'A')
	and not exists (select 1
					from	pls_conta_pos_estab_prev x
					where 	nr_seq_conta_mat = m.nr_sequencia);
end if;
	
	
select	max(b.ie_preco)
into	ie_preco_w
from	pls_conta a,
	pls_plano b,
	pls_segurado c
where	a.nr_seq_segurado = c.nr_sequencia
and 	a.nr_sequencia 	= nr_seq_conta_p
and	c.nr_seq_plano 	= b.nr_sequencia;

--Apenas para p?s-estabelecido
if	(ie_preco_w in ('2','3')) then

	--Busca os dados da conta
	select	nr_seq_prestador_prot,
		nvl(nr_seq_prestador_exec,nr_seq_prestador),
		ie_tipo_guia,
		cd_medico_executor,
		cd_medico_solicitante,
		nr_seq_prestador_solic,
		dt_atendimento,
		nr_seq_analise,
		nr_seq_prestador_prot,
		nr_seq_congenere_seg,
		nr_seq_segurado,
		nr_seq_grau_partic,
		ie_tipo_protocolo,
		ie_origem_conta
	into	nr_seq_prestador_prot_w,
		nr_seq_prestador_exec_w,
		ie_tipo_guia_w,
		cd_medico_exec_w,
		cd_medico_solicitante_w,
		nr_seq_prestador_solic_w,
		dt_atendimento_referencia_w,
		nr_seq_analise_w,
		nr_seq_prestador_w,
		nr_seq_congenere_w,
		nr_seq_segurado_w,
		nr_seq_grau_partic_w,
		ie_tipo_protocolo_w,
		ie_origem_conta_w
	from	pls_conta_v
	where	nr_sequencia	= nr_seq_conta_p;
	
	update 	pls_conta_proc
	set	nr_seq_regra_conv	= null
	where	nr_seq_conta 		= nr_seq_conta_p
	and	ie_status		!= 'M';
	
	if	(nr_seq_segurado_w is not null) then
		select	nr_seq_contrato,
			nr_seq_intercambio
		into	nr_seq_contrato_w,
			nr_seq_intercambio_w
		from	pls_segurado
		where	nr_sequencia = nr_seq_segurado_w;
	end if;
	
	ie_medico_exec_coope_w	:= pls_obter_se_cooperado_ativo(cd_medico_exec_w,dt_atendimento_referencia_w,null); 

	if (ie_medico_exec_coope_w = 'S') then
		ie_medico_exec_coope_w	:= 'C';
	end if;

	ie_medico_solic_coope_w	:= pls_obter_se_cooperado_ativo(cd_medico_solicitante_w,dt_atendimento_referencia_w,null);

	if (ie_medico_solic_coope_w = 'S') then
		ie_medico_solic_coope_w	:= 'C';
	end if;
	
	for r_c_regra in c_regra (dt_atendimento_referencia_w, nr_seq_contrato_w, nr_seq_intercambio_w) loop
		
		ie_nao_lanca_w	:= false;
		
		if	(r_c_regra.nr_seq_grupo_prestador is not null) and
			(pls_obter_se_prestador_grupo(r_c_regra.nr_seq_grupo_prestador, nr_seq_prestador_w) = 'N') then
			ie_nao_lanca_w := true;
		end if;
		
		if	(r_c_regra.nr_seq_grupo_operadora is not null) and
			(pls_se_grupo_preco_operadora(r_c_regra.nr_seq_grupo_operadora, nr_seq_congenere_w) = 'N') then
			ie_nao_lanca_w := true;
		end if;
		
		if	(not ie_nao_lanca_w) then
		
			if	(r_c_regra.ie_considera_todos_itens = 'S') then			
				for r_C06_w in C06 (nr_seq_analise_w, r_c_regra.nr_sequencia) loop
				
					if	(r_C06_w.qt_itens_regra = 0) then
						ie_nao_lanca_w := true;
						exit;
					end if;
				end loop;
				if	(C06%ISOPEN) then
					close C06;
				end if;
			end if;
			
			if	(not ie_nao_lanca_w) then		
				--Abre o curso da regra			
				for r_C01_w in C01(nr_seq_analise_w, r_c_regra.nr_sequencia) loop
				
					--Verifica o grupo de material
					if	(r_C01_w.nr_seq_grupo_material is not null) then
					
						for r_c05_w in C05 (r_C01_w.nr_seq_grupo_material) loop
							
							select	count(1)
							into	qt_registros_w
							from	pls_conta_mat_v		x
							where	x.nr_seq_analise	= nr_seq_analise_w
							and	x.ie_status		in ('L','S')
							and	x.nr_seq_material	= r_c05_w.nr_seq_material;
							
							if	(qt_registros_w = 0) then
								ie_nao_lanca_w	:= true;
								exit;
							end if;
						
						end loop;
					end if;
					
					if	(C05%ISOPEN) then
						close C05;
					end if;	
				
					select	count(1)
					into	qt_registros_w
					from	pls_conv_item_fat_exce
					where	nr_seq_regra	= r_c01_w.nr_seq_regra;
					
					--Verifica regra de exce??o
					if	(qt_registros_w	> 0) then
						for r_c04_w in C04(r_C01_w.nr_seq_regra) loop
						
							if	(r_c04_w.nr_seq_grupo_operadora is not null) then
								if	(pls_se_grupo_preco_operadora(r_c04_w.nr_seq_grupo_operadora,nr_seq_congenere_w) = 'S') then
									ie_nao_lanca_w	:= true;
									exit;
								end if;
							end if;
							
							if	(r_c04_w.nr_seq_grupo_prestador is not null) then				
								if	(pls_obter_se_prestador_grupo(r_c04_w.nr_seq_grupo_prestador,nr_seq_prestador_w) = 'S') then
									ie_nao_lanca_w	:= true;
									exit;
								end if;				
							end if;
						end loop;
					end if;
					
					if	(C04%ISOPEN) then
						close C04;
					end if;	
					
					if	(not ie_nao_lanca_w) then
						for r_C00_w in C00 (nr_seq_conta_p,r_C01_w.cd_procedimento,r_C01_w.ie_origem_proced) loop
						
							--Busca o procedimento
							-- tratamento realizado para quando tem mais de um procedimento e dentre eles um anestesisa, e necessario considerar primeiro os itens sem anestesista por conta do check de desconsiderar anestesista
							select	max(x.nr_sequencia),
								sum(qt_procedimento)
							into	nr_seq_conta_proc_w,
								qt_liberada_w
							from	pls_conta_proc_v	x
							where	x.nr_seq_conta			= nr_seq_conta_p
							and	x.ie_status			in ('L','S')
							and	x.cd_procedimento		= r_C00_w.cd_procedimento
							and	x.ie_origem_proced		= r_C00_w.ie_origem_proced
							and	dt_procedimento_trunc		= r_C00_w.dt_procedimento
							and	x.tx_item			= r_C00_w.tx_item
							and	(	select	count(1)
									from	pls_proc_participante	a,
										pls_grau_participacao	b
									where	a.nr_seq_grau_partic	= b.nr_sequencia
									and	a.nr_seq_conta_proc	= x.nr_sequencia
									and	b.ie_anestesista	= 'S') = 0;						
							
							if	(nr_seq_conta_proc_w is null) then
							--Busca o procedimento
								select	max(x.nr_sequencia),
									sum(qt_procedimento)
								into	nr_seq_conta_proc_w, --580268952
									qt_liberada_w
								from	pls_conta_proc_v	x
								where	x.nr_seq_conta			= nr_seq_conta_p
								and	x.ie_status			in ('L','S')
								and	x.cd_procedimento		= r_C00_w.cd_procedimento
								and	x.ie_origem_proced		= r_C00_w.ie_origem_proced
								and	dt_procedimento_trunc		= r_C00_w.dt_procedimento
								and	x.tx_item			= r_C00_w.tx_item;
							end if; 							
							
							if	(nr_seq_conta_proc_w is not null) then
								--Busca os dados do procedimento
								select	trunc(dt_procedimento_referencia,'dd'),
									dt_inicio_proc,
									dt_fim_proc,
									ie_tipo_despesa,
									ie_repassa_medico,
									nr_seq_honorario_crit,
									ie_criterio_horario,
									tx_item
								into	dt_procedimento_w,
									dt_inicio_proc_w,
									dt_fim_proc_w,
									ie_tipo_despesa_w,
									ie_repassa_medico_w,
									nr_seq_honorario_crit_w,
									ie_criterio_horario_w,
									tx_item_w
								from	pls_conta_proc
								where	nr_sequencia	= nr_seq_conta_proc_w;
							end if;
							
							ie_insere_particpante_w	:= 'N';

							--Se tiver marcado para anestesista, verifica quantos participantes tem na conta
							if	(r_C01_w.ie_desc_grau_anest = 'S') then
								select	count(1)
								into	qt_participantes_w
								from	pls_proc_participante	a
								where	a.nr_seq_conta_proc	= nr_seq_conta_proc_w;
								
								--Se tiver mais de participante no item, ent?o verifica se existe um anestesista
								if	(qt_participantes_w > 1) then
									select	count(1)
									into	qt_participantes_w
									from	pls_proc_participante	a,
										pls_grau_participacao	b
									where	a.nr_seq_grau_partic	= b.nr_sequencia
									and	a.nr_seq_conta_proc	= nr_seq_conta_proc_w
									and	b.ie_anestesista	= 'S';
									
									--Se tiver, ent?o deve criar um anestesista
									if	(qt_participantes_w > 0) then
										ie_insere_particpante_w	:= 'S';
									end if;
								--Se tiver um participante na conta, ent?o verifica se ele ? anestesista
								elsif	(qt_participantes_w = 1) then	
									--Se for anestesista, ent?o n?o faz nada
									select	count(1)
									into	qt_participantes_w
									from	pls_proc_participante	a,
										pls_grau_participacao	b
									where	a.nr_seq_grau_partic	= b.nr_sequencia
									and	a.nr_seq_conta_proc	= nr_seq_conta_proc_w
									and	b.ie_anestesista	= 'S';
									
									if	(qt_participantes_w > 0) then
										nr_seq_conta_proc_w	:= null;
									end if;
								elsif	(qt_participantes_w	= 0 ) then
					                                select	count(1)
									into	qt_participantes_w
									from	pls_grau_participacao	b
									where	nr_seq_grau_partic_w	= b.nr_sequencia
									and	b.ie_anestesista	= 'S';
						
						                        if	(qt_participantes_w > 0) then
										nr_seq_conta_proc_w	:= null;
									end if;
					
								end if;
							end if;
							
							if	(nr_seq_conta_proc_w is not null) then
							
								nr_seq_conta_proc_nova_w	:= null;

								for r_C02_w in C02(r_C01_w.nr_seq_regra) loop
								
									--Busca os dados do m?dico participante
									select	max(cd_medico)
									into	cd_medico_proc_w
									from	pls_proc_participante
									where	nr_seq_conta_proc	= nr_seq_conta_proc_w
									and	nr_seq_grau_partic	= r_C02_w.nr_seq_grau_partic
									and	cd_medico is not null;
									
									if	(cd_medico_proc_w is null) then
										select	max(cd_medico)
										into	cd_medico_proc_w
										from	pls_proc_participante
										where	nr_seq_conta_proc	= nr_seq_conta_proc_w
										and	cd_medico is not null;				
									end if;
									
									if	(cd_medico_proc_w is null) then
										cd_medico_proc_w	:= cd_medico_exec_w;
									end if;
									
									if	(cd_medico_proc_w is null) then
										cd_medico_proc_w	:= cd_medico_solicitante_w;
									end if;
								
									--Tratamento de duplicidade
									select	count(1)
									into	qt_registros_w
									from	pls_conta_proc_v	x
									where	x.nr_seq_analise		= nr_seq_analise_w
									and	x.cd_procedimento		= r_C02_w.cd_procedimento
									and	x.ie_origem_proced		= r_C02_w.ie_origem_proced
									and	x.nr_seq_regra_conv		= r_C01_w.nr_seq_regra
									and	dt_procedimento_trunc		= dt_procedimento_w
									and	tx_item				= tx_item_w
									and	((r_C02_w.nr_seq_grau_partic is null) or ( exists	(select	1
																from	pls_proc_participante a
																where	a.nr_seq_conta_proc	= x.nr_sequencia
																and	a.nr_seq_grau_partic	= r_C02_w.nr_seq_grau_partic)))
									and	not exists(	select	1
												from	pls_conta_pos_estabelecido	a
												where	a.nr_seq_conta_proc		= x.nr_sequencia
												and	a.ie_status_faturamento		= 'A'
												and	a.ie_cobrar_mensalidade		= 'A')
									and not exists (select 1
													from	pls_conta_pos_estab_prev
													where 	nr_seq_conta_proc = x.nr_sequencia);
									
									if	(qt_registros_w = 0) then
										select	pls_conta_proc_seq.nextval
										into	nr_seq_conta_proc_nova_w
										from	dual;
										
										--Insere o registro de procedimento
										insert	into pls_conta_proc
											(nr_sequencia, dt_atualizacao, nm_usuario,dt_atualizacao_nrec, nm_usuario_nrec, 
											nr_seq_conta,cd_procedimento, ie_origem_proced, qt_procedimento,qt_procedimento_imp, 
											ie_status, nr_seq_proc_princ,ie_situacao, ie_via_acesso, vl_unitario_imp,
											vl_procedimento_imp, vl_unitario, vl_liberado,vl_medico, vl_anestesista, 
											vl_auxiliares,vl_custo_operacional, vl_materiais, vl_procedimento,vl_glosa, 
											vl_beneficiario, nr_seq_regra_conv,ie_lanc_manual_pos, ie_apenas_pagamento, dt_procedimento,
											dt_inicio_proc,dt_fim_proc,ie_repassa_medico,nr_seq_honorario_crit,ie_criterio_horario,tx_item)
										values	(nr_seq_conta_proc_nova_w, sysdate, nm_usuario_p,sysdate, nm_usuario_p, 
											nr_seq_conta_p,r_C02_w.cd_procedimento, r_C02_w.ie_origem_proced, decode(r_C02_w.ie_qt_item_princ,'N',1,qt_liberada_w),decode(r_C02_w.ie_qt_item_princ,'N',1,qt_liberada_w), 
											'M', nr_seq_conta_proc_w,'D', 'U',0,
											0,0,0,0,0,
											0,0,0,0,0,
											0, r_C01_w.nr_seq_regra,'S', 'N', dt_procedimento_w,
											dt_inicio_proc_w,dt_fim_proc_w,ie_repassa_medico_w,nr_seq_honorario_crit_w,ie_criterio_horario_w,tx_item_w);
										
										pls_cta_proc_mat_regra_pck.cria_registro_regra_proc(nr_seq_conta_proc_nova_w, nm_usuario_p);
											
										--Insere o registro de participante
										if	(r_C02_w.nr_seq_grau_partic is not null) then
											insert into pls_proc_participante
												(	nr_sequencia,dt_atualizacao, nm_usuario,dt_atualizacao_nrec, nm_usuario_nrec,
													nr_seq_conta_proc,nr_seq_grau_partic,qt_liberada,vl_calculado,vl_calculado_ant,
													vl_glosa,vl_honorario_medico,vl_participante,ie_insercao_manual,ie_status,
													ie_status_pagamento,cd_medico)
											values	(	pls_proc_participante_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
													nr_seq_conta_proc_nova_w,r_C02_w.nr_seq_grau_partic,decode(r_C02_w.ie_qt_item_princ,'N',1,qt_liberada_w),0,0,
													0,0,0,'N','L',
													'N',cd_medico_proc_w);
										
										end if;
										
										--Define o ato cooperado
										pls_obter_tipo_ato_cooperado (	r_C02_w.cd_procedimento, r_C02_w.ie_origem_proced, nr_seq_conta_proc_nova_w, 
														'P', nr_seq_prestador_exec_w, nr_seq_prestador_solic_w, 
														nr_seq_prestador_prot_w, null, ie_medico_solic_coope_w,	
														ie_medico_exec_coope_w, null, ie_tipo_protocolo_w, 
														ie_tipo_guia_w,	ie_tipo_despesa_w, null, 
														nr_seq_regra_cooperado_w, ie_ato_cooperado_w);
										
										if	(ie_origem_conta_w	= 'A')   and
											(ie_ato_cooperado_w	is null) then
											ie_ato_cooperado_w := '1';
										end if;
										
										update 	pls_conta_proc
										set	nr_seq_regra_cooperado	= nr_seq_regra_cooperado_w,
											ie_ato_cooperado	= ie_ato_cooperado_w
										where	nr_sequencia 		= nr_seq_conta_proc_nova_w;
										
										--Grava o Log
										pls_gravar_log_conta(	nr_seq_conta_p, nr_seq_conta_proc_nova_w, null,
													'Procedimento '||nr_seq_conta_proc_nova_w||' gerado a partir da regra de lan?amentos de p?s-estabelecidos! ', nm_usuario_p);
													
									end if;
									
									update 	pls_conta_proc
									set	nr_seq_regra_conv	= r_C01_w.nr_seq_regra
									where	nr_sequencia 		= nr_seq_conta_proc_w;
																		
									if	(r_C01_w.nr_seq_grupo_material is not null) then
										for r_c05_w in C05 (r_C01_w.nr_seq_grupo_material) loop
										
											update	pls_conta_mat	x
											set	x.nr_seq_regra_conv	= r_C01_w.nr_seq_regra
											where	x.nr_seq_conta		in (	select	a.nr_sequencia
																from	pls_conta	a
																where	a.nr_seq_analise	= nr_seq_analise_w)
											and	x.nr_seq_material	= r_c05_w.nr_seq_material
											and	x.nr_seq_regra_conv is null
											and	rownum			= 1; --Apenas em 1 item do material
										end loop;
									end if;									

									select	count(1)
									into	qt_registros_w
									from	pls_conta_proc_v
									where	nr_seq_analise			= nr_seq_analise_w
									and	ie_status			in ('L','S')
									and	cd_procedimento			= r_C01_w.cd_procedimento
									and	ie_origem_proced		= r_C01_w.ie_origem_proced
									and	dt_procedimento_trunc		= dt_procedimento_w
									and	tx_item				= tx_item_w
									and	nr_sequencia			<> nr_seq_conta_proc_w;
									
									if	(qt_registros_w > 0) then
										for r_C03_w in C03(r_C01_w.cd_procedimento,r_C01_w.ie_origem_proced) loop
											qt_participantes_w	:= 0;
								                        if	(r_C01_w.ie_desc_grau_anest = 'S') then
								                            	select	count(1)
						                       				into	qt_participantes_w
												from	pls_grau_participacao	b
												where	r_c03_w.nr_seq_grau_partic_conta = b.nr_sequencia
												and	b.ie_anestesista		= 'S';
											end if;
								
						                                        if	(qt_participantes_w = 0) then
								        			update 	pls_conta_proc
												set	nr_seq_regra_conv	= r_C01_w.nr_seq_regra
												where	nr_sequencia		= r_C03_w.nr_sequencia;
											end if;
										end loop;
									end if;

								end loop;
								
								
								if	(ie_insere_particpante_w = 'S') then
									
									select	pls_conta_proc_seq.nextval
									into	nr_seq_conta_proc_nova_w
									from	dual;
									
									--Insere o registro de procedimento
									insert	into pls_conta_proc
										(nr_sequencia, dt_atualizacao, nm_usuario,dt_atualizacao_nrec, nm_usuario_nrec, 
										nr_seq_conta,cd_procedimento, ie_origem_proced, qt_procedimento,qt_procedimento_imp, 
										ie_status, nr_seq_proc_princ,ie_situacao, ie_via_acesso, vl_unitario_imp,
										vl_procedimento_imp, vl_unitario, vl_liberado,vl_medico, vl_anestesista, 
										vl_auxiliares,vl_custo_operacional, vl_materiais, vl_procedimento,vl_glosa, 
										vl_beneficiario, nr_seq_regra_conv,ie_lanc_manual_pos, ie_apenas_pagamento, dt_procedimento,
										dt_inicio_proc,dt_fim_proc,ie_repassa_medico,nr_seq_honorario_crit,ie_criterio_horario,
										nr_seq_regra_cooperado,ie_ato_cooperado)
									(select	nr_seq_conta_proc_nova_w, sysdate, nm_usuario_p,sysdate, nm_usuario_p, 
										nr_seq_conta_p,cd_procedimento, ie_origem_proced, qt_procedimento,qt_procedimento_imp, 
										'M', nr_seq_conta_proc_w,'D', 'U',0,
										0,0,0,0,0,
										0,0,0,0,0,
										0, r_C01_w.nr_seq_regra,'S', 'N', dt_procedimento,
										dt_inicio_proc,dt_fim_proc,ie_repassa_medico,nr_seq_honorario_crit,ie_criterio_horario,
										nr_seq_regra_cooperado,ie_ato_cooperado
									from	pls_conta_proc
									where	nr_sequencia	= nr_seq_conta_proc_w);
									
									pls_cta_proc_mat_regra_pck.cria_registro_regra_proc(nr_seq_conta_proc_nova_w, nm_usuario_p);
								
									select	max(a.cd_medico),
										max(a.nr_seq_grau_partic)
									into	cd_medico_proc_w,
										nr_seq_grau_partic_anest_w
									from	pls_proc_participante	a,
										pls_grau_participacao	b
									where	a.nr_seq_grau_partic	= b.nr_sequencia
									and	a.nr_seq_conta_proc	= nr_seq_conta_proc_w
									and	b.ie_anestesista	= 'S';
								
									insert into pls_proc_participante
										(	nr_sequencia,dt_atualizacao, nm_usuario,dt_atualizacao_nrec, nm_usuario_nrec,
											nr_seq_conta_proc,nr_seq_grau_partic,qt_liberada,vl_calculado,vl_calculado_ant,
											vl_glosa,vl_honorario_medico,vl_participante,ie_insercao_manual,ie_status,
											ie_status_pagamento,cd_medico)
									values	(	pls_proc_participante_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
											nr_seq_conta_proc_nova_w,nr_seq_grau_partic_anest_w,0,0,0,
											0,0,0,'N','L',
											'N',cd_medico_proc_w);
								end if;
							end if;
						end loop;
					end if;
				end loop;
			end if;
		end if;
	end loop;
	
	pls_cta_proc_mat_regra_pck.gera_seq_tiss_conta_proc(nr_seq_conta_p, nm_usuario_p);
	
end if;

end pls_lancar_item_proc_pos;
/