create or replace
procedure desvincular_atendimento_pac(	nr_sequencia_p	number,
					nm_usuario_p	varchar2) is

begin

	update	fa_paciente_entrega
	set	nr_atendimento = null
	where	nr_sequencia = nr_sequencia_p;

commit;

end desvincular_atendimento_pac;
/