create or replace
procedure qua_estorna_fechamento_audit(	
	nr_sequencia_p		number) is 

begin

update	Qua_auditoria
set	dt_fechamento = null
where	nr_sequencia = nr_sequencia_p;
commit;

end qua_estorna_fechamento_audit;
/