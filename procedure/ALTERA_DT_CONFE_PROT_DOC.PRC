create or replace
procedure altera_dt_confe_prot_doc(
			nr_documento_p		number,
			nr_sequencia_p		number,
			nm_usuario_p		varchar2,
			nm_usuario_conf_p	varchar2) is 

begin

if 	(nr_documento_p is not null)
	and (nr_sequencia_p is not null) then
	begin
	update	protocolo_doc_item
	set    	dt_conferencia = sysdate,
		nm_usuario_conf = nm_usuario_conf_p
	where  	nr_documento = nr_documento_p
	and    	nr_sequencia = nr_sequencia_p;
	commit;
	end;
end if;

end altera_dt_confe_prot_doc;
/
