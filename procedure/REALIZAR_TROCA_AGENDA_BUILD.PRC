create or replace
procedure realizar_troca_agenda_build(nm_usuario_p Varchar2,
			NR_SEQUENCIA_P	number,
			IE_OPCAO_P	varchar2) is 
			
qt_registros_w	Number(10);
dt_agendamento_w	date;
ie_aplicacao_w	varchar2(2);
ie_periodo_w	varchar2(20);
qt_agenda_w		Number(10);
nr_sequencia_troca_w	Number(10);
dt_agendamento_new_w	date;
ie_periodo_new_w	varchar2(20);
ie_parar_w		varchar2(2):= 'N';
qt_lacuna_w		Number(10):= 0;

Cursor C01 is
	select	a.nr_sequencia
	from	agendamento_build a
	where	((TRUNC(a.dt_agendamento) > TRUNC(dt_agendamento_w)) OR
			((TRUNC(a.dt_agendamento) = TRUNC(dt_agendamento_w)) AND (a.ie_periodo IN ('B','V','M','A')) AND (ie_periodo_w = 'A')) OR
			((TRUNC(a.dt_agendamento) = TRUNC(dt_agendamento_w)) AND (a.ie_periodo IN ('B','V','M')) AND (ie_periodo_w = 'M')) OR
			((TRUNC(a.dt_agendamento) = TRUNC(dt_agendamento_w)) AND (a.ie_periodo IN ('B','V')) AND (ie_periodo_w = 'V')) OR 
			((TRUNC(a.dt_agendamento) = TRUNC(dt_agendamento_w)) AND (a.ie_periodo = 'B') AND (ie_periodo_w = 'B')))
	and		ie_status = 'BA'
	and		a.ie_periodo in ('V','M','A','B')
	order by a.dt_agendamento, ie_periodo;

begin

select	a.dt_agendamento,
	a.ie_aplicacao,
	a.ie_periodo
into	dt_agendamento_w,
	ie_aplicacao_w,
	ie_periodo_w
from	agendamento_build a
where	a.nr_sequencia = nr_sequencia_p;

if (ie_opcao_p = 'C') then

	update	agendamento_build a
	set	ie_status = 'C'
	where	trunc(a.dt_agendamento) = trunc(dt_agendamento_w)
	and	a.ie_periodo = ie_periodo_w
	and	ie_status = 'BA';
	commit;
	
elsif (ie_opcao_p = 'P') then

	select	count(*)
	into	qt_agenda_w
	from	agendamento_build a
	where	trunc(a.dt_agendamento) = trunc(dt_agendamento_w)
	and		a.ie_periodo = ie_periodo_w
	and		ie_status = 'BA';	
	
	if (qt_agenda_w > 0) then
	
		select	max(a.nr_sequencia)
		into	nr_sequencia_troca_w
		from	agendamento_build a
		where	trunc(a.dt_agendamento) = trunc(dt_agendamento_w)
		and		a.ie_periodo = ie_periodo_w
		and		ie_status = 'BA';

		agendar_prox_data_build(nr_sequencia_troca_w, nm_usuario_p, 'S', dt_agendamento_new_w, ie_periodo_new_w, ie_parar_w);

	end if;
	
elsif (ie_opcao_p = 'R') then
	
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_troca_w;
	exit when C01%notfound;
		begin
			
			if (qt_lacuna_w = 0) then			
				agendar_prox_data_build(nr_sequencia_troca_w, nm_usuario_p, 'N', dt_agendamento_new_w, ie_periodo_new_w, ie_parar_w);
			end if;
			
			if (ie_parar_w = 'S') then
				qt_lacuna_w:=qt_lacuna_w+1;
			end if;	
			
		end;
	end loop;
	close C01;
	
end if;

update agendamento_build
set ie_status = 'BA'
where nr_sequencia = nr_sequencia_p;
commit;

end realizar_troca_agenda_build;
/
