create or replace
procedure hemoderivados_beforepost	
		(nr_atendimento_p		number,
		 
		 ie_derivado_exame_p            number,
		 nr_sequencia_p                 number,
		 cd_estabelecimento_p           number,
		 cd_setor_p                     in out number,
		 cd_procedimento_p              in out number,
		 ie_origem_proced_p             in out number,
		 nr_seq_proc_interno_p          in out number ) is

cd_convenio_w		number(5,0);		
cd_setor_w           	number(5,0);
cd_procedimento_w	number(15,0);
ie_origem_proced_w   	number(10,0);
nr_seq_proc_interno_w	number(10,0);
cd_categoria_w		varchar2(10);
		 
begin

if	(nr_atendimento_p	is not null) and
	(ie_derivado_exame_p	is not null) and  
	(nr_sequencia_p       	is not null) and    
	(cd_estabelecimento_p  	is not null) then
	begin
	cd_convenio_w	:= obter_convenio_atendimento(nr_atendimento_p);
	cd_categoria_w	:= obter_categoria_atendimento(nr_atendimento_p);
	
	obter_proced_sangue(	ie_derivado_exame_p,
				nr_sequencia_p,
				cd_estabelecimento_p,
				obter_tipo_atendimento(nr_atendimento_p),
				obter_tipo_convenio(cd_convenio_w),
				cd_convenio_w,
				cd_categoria_w,
				cd_setor_w,
				cd_procedimento_w,
				ie_origem_proced_w, 
				nr_seq_proc_interno_w );
	end;
end if;

cd_setor_p   		:= cd_setor_w;          
cd_procedimento_p	:= cd_procedimento_w;
ie_origem_proced_p     	:= ie_origem_proced_w;
nr_seq_proc_interno_p	:= nr_seq_proc_interno_w;

commit;

end hemoderivados_beforepost;
/ 