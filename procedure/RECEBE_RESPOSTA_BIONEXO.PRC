create or replace
procedure recebe_resposta_bionexo(	nr_cot_compra_p			number,
				ie_status_p			number,
				ds_string_retorno_p			varchar2,
				nm_usuario_p			varchar2) is

nr_cot_compra_w		number(10);
ds_enter_w		varchar2(10) := chr(13) || chr(10);
					
begin

if	(nr_cot_compra_p > 0) then
	if	(ie_status_p = 1 and ds_string_retorno_p is null) then
		gerar_historico_cotacao(
			nr_cot_compra_p,
			WHEB_MENSAGEM_PCK.get_texto(306490),
			substr(WHEB_MENSAGEM_PCK.get_texto(306493),1,4000),
			'S',
			nm_usuario_p);

	elsif	(ie_status_p = 0) then
		gerar_historico_cotacao(
			nr_cot_compra_p,
			WHEB_MENSAGEM_PCK.get_texto(306495),
			substr(WHEB_MENSAGEM_PCK.get_texto(306496),1,4000),
			'S',
			nm_usuario_p);

	elsif	(ie_status_p < 0 or ds_string_retorno_p is not null) then
		gerar_historico_cotacao(
			nr_cot_compra_p,
			WHEB_MENSAGEM_PCK.get_texto(306497),
			substr(WHEB_MENSAGEM_PCK.get_texto(306500,'DS_STRING_RETORNO_P='|| substr(ds_string_retorno_p,1,2000)),1,4000),
			'S',
			nm_usuario_p);
	end if;
end if;


commit;

end recebe_resposta_bionexo;
/