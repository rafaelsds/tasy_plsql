create or replace
procedure baixar_titulo_rec_cobr
			(	nr_seq_tit_rec_cobr_p	number,
				ie_acao_p		varchar2,
				nm_usuario_p		varchar2,
				dt_recebimento_p	date,
				nr_seq_movto_trans_fin_p number default null) is

ds_erro_w			varchar2(4000);
ie_desfazer_baixa_w		varchar2(255);
cd_cgc_w			varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
ie_acao_w			varchar2(3);
ie_acresc_cobr_w		varchar2(2);
ie_dt_baixa_cobr_escrit_w	varchar2(1);
ie_trans_tit_cobr_escrit_w	varchar2(1);
ie_estornar_perda_w		varchar2(1);
ie_rejeitado_w			varchar2(1);
ie_gerar_nc_pag_duplic_w	varchar2(1)	:= 'N';
ie_movto_bco_cobr_escrit_w	varchar2(1);
ie_apropriar_w			varchar2(1);
vl_baixa_w			number(15,2);
vl_desconto_w			number(15,2);
vl_acrescimo_w			number(15,2);
vl_juro_w			number(15,2);
vl_multa_w			number(15,2);
vl_credito_w			number(15,2);
vl_titulos_nc_w			number(15,2);
vl_saldo_titulo_w		number(15,2);
vl_juros_w			number(15,2);
vl_rec_maior_w			number(15,2);
nr_seq_trans_financ_w		number(10)	:= null;
nr_seq_conta_banco_w		number(10);
nr_seq_carteira_cobr_w		number(10);
cd_tipo_recebimento_w		number(10);
nr_seq_cobranca_w		number(10);
nr_titulo_w			number(10);
qt_tit_nc_w			number(10);
nr_titulo_pagar_w		number(10);
nr_seq_trans_fin_abat_w		number(10);
nr_seq_trans_fin_abat_pag_w	number(10);
nr_seq_liq_w			number(10);
nr_seq_regra_w			number(10);
nr_seq_baixa_estorno_w		number(10);
nr_seq_classe_w			number(10);
vl_Recebido_w			number(15,2);
pr_multa_padrao_w		number(7,4);
nr_seq_baixa_w			number(6)	:= null;
cd_convenio_w			number(5);
cd_moeda_padrao_w		number(5);
cd_tipo_receb_abat_w		number(5);
cd_tipo_baixa_abat_w		number(5);
cd_estabelecimento_w		number(4);
cd_banco_w			number(3);
dt_liq_cobr_w			date;
dt_baixa_w			date;
dt_liq_titulo_cobr_w		date;
vl_despesa_bancaria_w		number(15,2);
ie_valor_desp_cobr_w		varchar2(1) := 'N';
vl_amortizacao_w		number(15,2) := 0;
nr_titulo_contab_w		number(10);
dt_credito_bancario_w	cobranca_escritural.dt_credito_bancario%type;
ie_aprop_mens_w				varchar2(1);
ie_pf_pj_w					varchar2(1);
ie_juros_multa_mens_w		pls_parametros_cr.ie_juros_multa_mens%type;
ie_origem_titulo_w			titulo_receber.ie_origem_titulo%type;
ie_apropriar_juros_multa_w	titulo_receber_liq.ie_apropriar_juros_multa%type := 'N';
cd_tipo_receb_escrit_w		parametro_contas_receber.cd_tipo_receb_escrit%type;	
dt_remessa_retorno_w		cobranca_escritural.dt_remessa_retorno%type;
nr_seq_cob_previa_w		titulo_receber.nr_seq_cob_previa%type;
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;

cursor C01 is
	select	nr_titulo_pagar
	from	titulo_receber_nc
	where	nr_titulo_rec	= nr_titulo_w;

cursor	C02 is
	select	a.nr_sequencia
	from	tipo_recebimento b,
		titulo_receber_liq a
	where	a.nr_titulo		= nr_titulo_w
	and	a.cd_tipo_recebimento	= b.cd_tipo_recebimento
	and	b.ie_tipo_consistencia	= 9
	and	(a.vl_recebido > 0 or a.vl_perdas > 0);

begin

select	b.cd_banco,
	d.nr_seq_conta_banco,		-- Edgar 25/11/2008, OS 118259, troquei o a.nr_seq_conta_banco pelo d.nr_seq_conta_banco
	a.nr_seq_carteira_cobr,
	a.cd_estabelecimento,
	a.cd_pessoa_fisica,
	a.cd_cgc,
	decode(obter_convenio_tit_rec(a.nr_titulo),0,null,obter_convenio_tit_rec(a.nr_titulo)),
	d.nr_sequencia,
	d.cd_estabelecimento,
	c.dt_liquidacao,
	nvl(c.vl_liquidacao,0) + nvl(c.vl_despesa_bancaria,0) - nvl(c.vl_acrescimo,0) - 
		decode(nvl(c.vl_juros,0),
			0,
			decode(nvl(b.ie_cobr_juro_multa_calc,'N'),'S',nvl(c.vl_juros_calc,0),0),
			c.vl_juros) - 
		decode(nvl(c.vl_multa,0),
			0,
			decode(nvl(b.ie_cobr_juro_multa_calc,'N'),'S',nvl(c.vl_multa_calc,0),0),
			c.vl_multa), --Alterado para obter o valor de juros e multa calculados, caso o valor de juros e multa n�o estiver preenchido - OS 435423
	nvl(c.vl_desconto,0),
	nvl(c.vl_acrescimo,0),
	nvl(c.vl_liquidacao,0),
	a.nr_titulo,
	decode(nvl(c.vl_juros,0),
		0,
		decode(nvl(b.ie_cobr_juro_multa_calc,'N'),'S',nvl(c.vl_juros_calc,0),0),
		c.vl_juros), --Alterado para obter o valor de juros calculado, caso o valor de juros e multa n�o estiver preenchido - OS 435423
	decode(nvl(c.vl_multa,0),
		0,
		decode(nvl(b.ie_cobr_juro_multa_calc,'N'),'S',nvl(c.vl_multa_calc,0),0),
		c.vl_multa), --Alterado para obter o valor de multa calculado, caso o valor de juros e multa n�o estiver preenchido - OS 435423
	e.ie_rejeitado,
	nvl(c.vl_despesa_bancaria,0),
	c.nr_seq_trans_financ,
	d.dt_credito_bancario,
	d.dt_remessa_retorno
into	cd_banco_w,
	nr_seq_conta_banco_w,
	nr_seq_carteira_cobr_w,
	cd_estabelecimento_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	cd_convenio_w,
	nr_seq_cobranca_w,
	cd_estabelecimento_w,
	dt_liq_cobr_w,
	vl_baixa_w,
	vl_desconto_w,
	vl_acrescimo_w,
	vl_credito_w,
	nr_titulo_w,
	vl_juros_w,
	vl_multa_w,
	ie_rejeitado_w,
	vl_despesa_bancaria_w,
	nr_seq_trans_financ_w,
	dt_credito_bancario_w,
	dt_remessa_retorno_w
from	banco_ocorr_escrit_ret	e,
	cobranca_escritural	d,
	banco_estabelecimento	b,
	titulo_receber		a,
	titulo_receber_cobr	c
where	c.nr_seq_ocorrencia_ret	= e.nr_sequencia(+)
and	a.nr_seq_conta_banco	= b.nr_sequencia(+)
and	c.nr_titulo		= a.nr_titulo
and	c.nr_seq_cobranca	= d.nr_sequencia
and	c.nr_sequencia		= nr_seq_tit_rec_cobr_p;

select	max(ie_valor_desp_cobr)
into	ie_valor_desp_cobr_w
from	banco_estabelecimento
where	nr_sequencia = nr_seq_conta_banco_w;

if	(nvl(ie_valor_desp_cobr_w,'N') = 'N') then
	vl_despesa_bancaria_w := 0;
else
	vl_baixa_w := vl_baixa_w - vl_despesa_bancaria_w;
end if;

Obter_Param_Usuario(815, 17, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_desfazer_baixa_w);
Obter_Param_Usuario(815, 19, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_estornar_perda_w);
Obter_Param_Usuario(815, 48, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_aprop_mens_w); -- AAMFIRMO OS 1204964.

if	(ie_estornar_perda_w = 'S') and (ie_rejeitado_w = 'L') and (ie_acao_p <> 'E') then
	open C02;
	loop
	fetch C02 into
		nr_seq_liq_w;
	exit when C02%notfound;

		estornar_tit_receber_liq(nr_titulo_w,nr_seq_liq_w,sysdate,nm_usuario_p,'N',WHEB_MENSAGEM_PCK.get_texto(278931),'S');

	end loop;
	close C02;
end if;

select	cd_moeda_padrao,
	nvl(ie_dt_baixa_cobr_escrit,'A'),
	pr_multa_padrao,
	ie_acresc_cobr,
	nvl(ie_trans_tit_cobr_escrit,'N'),
	nr_seq_trans_fin_abat,
	cd_tipo_receb_abat,
	ie_movto_bco_cobr_escrit,
	nvl(vl_maximo_amortizacao,0)
into	cd_moeda_padrao_w,
	ie_dt_baixa_cobr_escrit_w,
	pr_multa_padrao_w,
	ie_acresc_cobr_w,
	ie_trans_tit_cobr_escrit_w,
	nr_seq_trans_fin_abat_w,
	cd_tipo_receb_abat_w,
	ie_movto_bco_cobr_escrit_w,
	vl_amortizacao_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_w;

/*Coloquei separado aqui pois no select acima nenhum campo tem max*/
select	max(a.cd_tipo_receb_escrit)
into	cd_tipo_receb_escrit_w
from	parametro_contas_receber a
where	cd_estabelecimento	= cd_estabelecimento_w;	

select	min(cd_tipo_recebimento)
into	cd_tipo_recebimento_w
from	tipo_recebimento
where	ie_tipo_consistencia = 1
and     ((cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento is null))
and 	ie_situacao = 'A';

if	(nr_seq_trans_financ_w	is null) then

	if	(ie_trans_tit_cobr_escrit_w = 'S') then

		select	max(nr_seq_trans_fin_baixa)
		into	nr_seq_trans_financ_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

	else

		obter_trans_fin_regra('ETR',
				cd_estabelecimento_w,
				cd_pessoa_fisica_w,
				cd_cgc_w,
				cd_convenio_w,
				cd_banco_w,
				null,
				nr_seq_conta_banco_w,
				nr_seq_carteira_cobr_w,		
				null,
				nr_seq_trans_financ_w);

	end if;

	if	(nr_seq_trans_financ_w is null) then

		select	nr_seq_trans_cobr_escrit
		into	nr_seq_trans_financ_w
		from	parametro_contas_receber
		where	cd_estabelecimento	= cd_estabelecimento_w;

		if	(nr_seq_trans_financ_w is null) then

			select	min(nr_sequencia)
			into	nr_seq_trans_financ_w
			from	transacao_financeira
			where	ie_acao	= 1
			and	cd_estabelecimento	= cd_estabelecimento_w;

		end if;

	end if;

end if;

select	nvl(max(nr_sequencia),0) + 1
into	nr_seq_baixa_w
from	titulo_receber_liq
where	nr_titulo	= nr_titulo_w;

if	(ie_dt_baixa_cobr_escrit_w = 'A') then
	dt_baixa_w	:= dt_liq_cobr_w;
elsif	(ie_dt_baixa_cobr_escrit_w = 'L') then
	dt_baixa_w	:= dt_recebimento_p;
elsif (ie_dt_baixa_cobr_escrit_w = 'R') then
	dt_baixa_w	:= dt_remessa_retorno_w;
end if;

if	(ie_acresc_cobr_w = 'S' and nvl(vl_acrescimo_w,0) > 0) then -- afstringari - OS165149 - 14/09/2009
	vl_multa_w	:= ((vl_baixa_w + vl_acrescimo_w) * pr_multa_padrao_w) / 100;
	vl_juro_w	:= vl_acrescimo_w - vl_multa_w;
else
	vl_juro_w	:= nvl(vl_acrescimo_w,0) + nvl(vl_juros_w,0);
end if;

if	(ie_acao_p = 'E') then
	vl_baixa_w	:= vl_baixa_w * -1;
	vl_desconto_w	:= vl_desconto_w * -1;
	vl_acrescimo_w	:= vl_acrescimo_w * -1;
	vl_multa_w	:= vl_multa_w * -1;
	vl_juro_w	:= vl_juro_w * -1;
end if;

if	(nr_seq_baixa_w = 1) and
	(vl_baixa_w < 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(181574,'NR_TITULO=' || nr_titulo_w);
end if;

if (nvl(ie_aprop_mens_w,'N') = 'S') then
	
	/*Mesmo select do objeto 368737 utilizado no evento DmCorCre_dm.Titulo_Receber_liq_qBeforePost
	Verificar se o t�tulo � de PF*/
	select 	decode(max(a.cd_pessoa_fisica),null,'N','S') 
	into	ie_pf_pj_w
	from 	titulo_receber a
	where 	a.nr_titulo = nr_titulo_w;
	
	/*Mesmo select do objeto 368735 utilizado no evento DmCorCre_dm.Titulo_Receber_liq_qBeforePost
	Verificar como est� o parametro para apropria no OPS*/
	select	nvl(max(a.ie_juros_multa_mens),'S')
	into	ie_juros_multa_mens_w
	from	pls_parametros_cr a
	where	cd_estabelecimento	= nvl(obter_estabelecimento_ativo, cd_estabelecimento_w);
	
	/*Mesmo select do objeto 369134 utilizado no evento DmCorCre_dm.Titulo_Receber_liq_qBeforePost
	Verificar a origem do t�tulo*/	
	select	max(a.ie_origem_titulo)
	into	ie_origem_titulo_w
	from	titulo_receber a
	where	a.nr_titulo = nr_titulo_w;
	
	if ((ie_juros_multa_mens_w = 'S') or ((ie_juros_multa_mens_w = 'PF') and (ie_pf_pj_w = 'S'))) and (ie_origem_titulo_w = '3') then
		ie_apropriar_juros_multa_w := 'S';
	else
		ie_apropriar_juros_multa_w := 'N'; 
	end if;

end if;

/* Se for estorno, estornar a baixa que j� havia sido lan�ada e n�o gerar uma nova, devido aos tratamentos que s�o feitos na baixa original */
if	(ie_acao_p = 'E') then
	/* Buscar a baixa anterior */
	select	max(a.nr_sequencia)
	into	nr_seq_baixa_w
	from	titulo_receber_liq a
	where	not exists
		(select	1
		from	titulo_receber_liq x
		where	x.nr_seq_liq_origem	= a.nr_sequencia
		and	x.nr_titulo		= a.nr_titulo)
	and	a.nr_seq_liq_origem	is null
	and	a.nr_titulo		= nr_titulo_w
	and	a.nr_seq_cobranca	= nr_seq_cobranca_w;

	if(nr_seq_baixa_w is not null) then
	estornar_tit_receber_liq(nr_titulo_w,nr_seq_baixa_w,nvl(dt_baixa_w, trunc(sysdate,'dd')),nm_usuario_p,'N',null,ie_movto_bco_cobr_escrit_w);
	end if;

	select	max(nr_sequencia)
	into	nr_seq_baixa_estorno_w
	from	titulo_receber_liq a
	where	a.nr_titulo	= nr_titulo_w;
	
	if	(nr_seq_baixa_estorno_w is not null) then
		update	titulo_receber_liq
		set	nr_seq_cobranca	= nr_seq_cobranca_w,
			nr_seq_movto_trans_fin = nr_seq_movto_trans_fin_p
		where	nr_titulo	= nr_titulo_w
		and	nr_sequencia	= nr_seq_baixa_estorno_w;
	end if;
else
	insert into titulo_receber_liq
		(nr_titulo,
		nr_sequencia,
		dt_recebimento,
		vl_recebido,
		vl_descontos,
		vl_juros,
		vl_multa,
		vl_rec_maior,
		vl_glosa,
		cd_moeda,
		dt_atualizacao,
		nm_usuario,
		cd_tipo_recebimento,
		ie_acao,
		nr_lote_contabil,
		nr_seq_trans_fin,
		ie_lib_caixa,
		nr_seq_conta_banco,
		nr_seq_cobranca,
		nr_lote_contab_antecip,
		nr_lote_contab_pro_rata,
		vl_despesa_bancaria,
		nr_seq_movto_trans_fin,
		dt_credito_banco,
		dt_real_recebimento,
		ie_apropriar_juros_multa)
	values	(nr_titulo_w,
		nr_seq_baixa_w,
		nvl(dt_baixa_w, trunc(sysdate,'dd')),
		vl_baixa_w,
		vl_desconto_w,
		vl_juro_w,
		vl_multa_w,
		0,
		0,
		cd_moeda_padrao_w,
		sysdate,
		nm_usuario_p,
		nvl(cd_tipo_receb_escrit_w,cd_tipo_recebimento_w), 
		'I', 
		0, 
		nr_seq_trans_financ_w, 
		'S',
		nr_seq_conta_banco_w,
		nr_seq_cobranca_w,
		0,
		0,
		vl_despesa_bancaria_w,
		nr_seq_movto_trans_fin_p,
		dt_credito_bancario_w,
		dt_credito_bancario_w,
		nvl(ie_apropriar_juros_multa_w,'N'));
end if;			
	
/* Elton em 22/02/2010 OS 195860 - Criado para baixar as notas de cr�dito do t�tulo a receber ao realizar a baixa pela cobran�a escritural */	
select	vl_saldo_titulo
into	vl_saldo_titulo_w
from	titulo_receber
where	nr_titulo	= nr_titulo_w;

select	cd_tipo_baixa_abat,
	nr_seq_trans_fin_abat
into	cd_tipo_baixa_abat_w,
	nr_seq_trans_fin_abat_pag_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

select	nvl(sum(a.vl_saldo_titulo),0)
into	vl_titulos_nc_w
from	titulo_receber_nc b,
	titulo_pagar a
where	a.nr_titulo		= b.nr_titulo_pagar
and	b.nr_titulo_rec		= nr_titulo_w;

if	((vl_credito_w + vl_titulos_nc_w) = vl_saldo_titulo_w) then
	open C01;
	loop
	fetch C01 into
		nr_titulo_pagar_w;
	exit when C01%notfound;	

		if (nr_titulo_pagar_w is not null) then
			BAIXAR_TITULOS_ABATIMENTO(0,
						nr_titulo_w,
						nr_titulo_pagar_w,
						cd_tipo_baixa_abat_w,
						cd_tipo_receb_abat_w,
						nr_seq_trans_fin_abat_pag_w,
						nr_seq_trans_fin_abat_w,
						nm_usuario_p,
						dt_recebimento_p,
						'',
						'');
		end if;				
	end loop;
	close C01;
end if;
/* Fim Elton OS 195860*/

/* Francisco - 10/02/2010 - OS 194892 */
if	(vl_amortizacao_w = 0) then
	pls_gerar_amortizacao_regra(nr_titulo_w, nr_seq_baixa_w, nm_usuario_p, 'N');
end if;

obter_regra_acao_pag_duplic(dt_recebimento_p, cd_estabelecimento_w, nm_usuario_p, nr_seq_regra_w, ie_acao_w);
	
if	(ie_acao_w in ('NC','NCM')) then
	tratar_baixa_duplicidade_tit(nr_seq_tit_rec_cobr_p,null,nr_seq_baixa_w,ie_acao_p,dt_recebimento_p,nm_usuario_p, ie_apropriar_w);
end if;

select	max(a.nr_seq_cob_previa)
into	nr_seq_cob_previa_w
from	titulo_receber	a
where	a.nr_titulo	= nr_titulo_w;

if	(nr_seq_cob_previa_w is not null) and
	(vl_baixa_w = vl_saldo_titulo_w) then
	begin
		pls_atualiza_situacao_cobranca(	nr_seq_cob_previa_w, nm_usuario_p, 'N');
	exception
	when others then
		select	max(nr_seq_requisicao)
		into	nr_seq_requisicao_w
		from	pls_cobranca_previa_serv
		where	nr_sequencia	= nr_seq_cob_previa_w;
		
		if (nr_seq_requisicao_w is not null) then
		pls_requisicao_gravar_hist(	nr_seq_requisicao_w,
					'L',
					'Titulo '||nr_titulo_w||' da requisi��o '||nr_seq_requisicao_w||' n�o finalizado. Erro:'|| sqlerrm ||'.',
					null,
					nm_usuario_p);
		end if;
	end;
end if;

/* Francisco - 28/09/2010 */
/* Diether - 26/08/2011 -  OS  344588 - Alterado para chamar a rotina ap�s tratar a baixa em duplicidade pois neste momento se chamar a atualizar_saldo_tit_rec a baixa j� ter� o valor ajustado e nota de cr�dito gerada */
if	(nvl(ie_apropriar_w,'S') = 'S') then
	pls_apropriar_juros_multa_mens(nr_titulo_w,nr_seq_baixa_w,nm_usuario_p,cd_estabelecimento_w,'N',ie_acao_p);
end if;

update	titulo_receber_cobr
set	dt_liquidacao	= trunc(sysdate,'dd')
where	dt_liquidacao	is null
and	nr_sequencia	= nr_seq_tit_rec_cobr_p;

begin
Atualizar_Saldo_Tit_Rec(nr_titulo_w, nm_usuario_p);
exception
when others then
	ds_erro_w	:= sqlerrm;
	if	(nvl(ie_desfazer_baixa_w, 'N') = 'S') then
		delete	from titulo_receber_liq
		where	nr_titulo	= nr_titulo_w
		and	nr_sequencia	= nr_seq_baixa_w;

		update	titulo_receber_cobr
		set	dt_liquidacao		= null,
			ds_inconsistencia	= ds_erro_w
		where	dt_liquidacao		is not null
		and	nr_sequencia		= nr_seq_tit_rec_cobr_p;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(181577,'DS_ERRO=' || ds_erro_w || ';' ||'NR_TITULO_W=' || nr_titulo_w);
	end if;
end;

if	(ie_acao_p <> 'E') then
	/* ebcabral - 10/09/2012 - OS 470103 - Alterado para gerar a classifica��o da baixa ap�s executar as rotinas que realizam tratamentos na baixa */ 
	gerar_titulo_rec_liq_cc
			(cd_estabelecimento_w,
			null,
			nm_usuario_p,
			nr_titulo_w,
			nr_seq_baixa_w);
			
	pls_gerar_tit_rec_liq_mens(nr_titulo_w,	nr_seq_baixa_w, nm_usuario_p,nr_titulo_contab_w);
	
	if	(nr_titulo_contab_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(236517, 'NR_TITULO=' || nr_titulo_contab_w);
	end if;	
end if;

end baixar_titulo_rec_cobr;
/
