CREATE OR REPLACE PROCEDURE limpar_relatorio_dinamico(
    nr_seq_relatorio_p relatorio.nr_sequencia%type
    )
IS
BEGIN

  DELETE
    FROM banda_relat_campo brc
   WHERE brc.nr_seq_banda IN (
          SELECT DISTINCT nr_sequencia
            FROM banda_relatorio br
           WHERE br.nr_seq_relatorio = nr_seq_relatorio_p
       );

  DELETE
    FROM banda_relatorio br
   WHERE br.nr_seq_relatorio = nr_seq_relatorio_p;

  UPDATE parametro_relat_dinamico prd
     SET prd.nr_seq_relatorio_parametro = NULL
   WHERE prd.nr_seq_relatorio = nr_seq_relatorio_p;
   
  DELETE
    FROM relatorio_parametro rp
   WHERE rp.nr_seq_relatorio = nr_seq_relatorio_p;

   COMMIT;
END;
/
