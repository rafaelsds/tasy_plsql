create or replace procedure ctb_gerar_interf_k115_ecd(nr_seq_controle_p	number,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number,
						dt_inicio_p		date,
						dt_fim_p		date,
						cd_empresa_p		number,
						dt_situacao_p		date,
						qt_linha_p		in out number,
						nr_sequencia_p		in out number) is

ds_arquivo_w		varchar2(4000);
ds_compl_arquivo_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10) := qt_linha_p;
nr_seq_registro_w	number(10) := nr_sequencia_p;
sep_w			varchar2(1) := '|';
tp_registro_w		varchar2(15) := 'K115';
nr_emp_cod_part_w	number(4) := cd_empresa_p;
nr_cond_part_w		number(4);
nr_per_evt_w		number(8,4);

cursor c01 is
	select	sit.ie_condicao_empresa,
		sit.pr_evento_empresa
	from	ctb_sit_especial_empresa sit
	where	sit.dt_situacao = dt_situacao_p
	and	sit.cd_empresa = cd_empresa_p
	order by sit.dt_situacao, sit.pr_evento_empresa;
begin

open C01;
loop
fetch C01 into	
	nr_cond_part_w,
	nr_per_evt_w;
exit when C01%notfound;
	begin
	
	ds_linha_w	:= substr(sep_w || tp_registro_w  ||
				sep_w || nr_emp_cod_part_w || 
				sep_w || nr_cond_part_w ||
				sep_w || nr_per_evt_w || sep_w,1,8000);

	ds_arquivo_w := substr(ds_linha_w,1,4000);
	ds_compl_arquivo_w := substr(ds_linha_w,4001,4000);
	nr_seq_registro_w := nr_seq_registro_w + 1;
	nr_linha_w := nr_linha_w + 1;

	insert into ctb_sped_registro(
			nr_sequencia,
			ds_arquivo,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_controle_sped,
			ds_arquivo_compl,
			cd_registro,
			nr_linha)
		values(
			nr_seq_registro_w,
			ds_arquivo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_controle_p,
			ds_compl_arquivo_w,
			tp_registro_w,
			nr_linha_w);

	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end ctb_gerar_interf_k115_ecd;
/