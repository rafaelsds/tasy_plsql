create or replace
procedure Ageint_Consistir_Marcacao(
						nr_Seq_ageint_p			number,
						nr_seq_ageint_item_p	number,
						cd_estab_marcacao_p		number,
						cd_estabelecimento_p	number,
						nm_usuario_p			varchar2,
						ds_mensagem_p	out 	varchar2,
						ie_bloqueia_p	out 	varchar2) is
	
qt_outro_estab_w	number(10);
ie_glosa_w		varchar2(1);
	
begin

select	count(*)
into	qt_outro_estab_w
from	agenda b,
		ageint_marcacao_usuario a
where	a.cd_agenda				= b.cd_agenda
and		a.nr_seq_ageint			= nr_seq_ageint_p
and		b.cd_estabelecimento	<> cd_estabelecimento_p;

select	max(ie_glosa)
into	ie_glosa_w
from	agenda_integrada_item
where	nr_sequencia = nr_seq_ageint_item_p;

if	(ie_glosa_w = 'T') then
	ds_mensagem_p := wheb_mensagem_pck.get_texto(790835);
	ie_bloqueia_p	:= 'S';
elsif	(qt_outro_estab_w	> 0) then
	ds_mensagem_p := wheb_mensagem_pck.get_texto(790836);
	ie_bloqueia_p	:= 'N';
end if;

end Ageint_Consistir_Marcacao;
/
