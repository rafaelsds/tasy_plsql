create or replace
procedure atualizar_setor_triagem(	nr_atendimento_p		number,
			nm_usuario_p		Varchar2) is 

cd_setor_atendimento_w			number(10);
nr_seq_triagem_w				number(10);

begin
select	nvl(max(cd_setor_atendimento),0),
		max(nr_sequencia)
into	cd_setor_atendimento_w,
		nr_seq_triagem_w
from	triagem_pronto_atend
where	nr_atendimento = nr_atendimento_p;

if	(cd_setor_atendimento_w = 0) then
	select	nvl(max(substr(obter_setor_atendimento(nr_atendimento_p),1,5)),0)
	into	cd_setor_atendimento_w
	from	dual;
	
	if	(cd_setor_atendimento_w > 0) then
		update	triagem_pronto_atend
		set		cd_setor_atendimento = cd_setor_atendimento_w
		where	nr_sequencia = nr_seq_triagem_w;
	end if;
end if;

commit;

end atualizar_setor_triagem;
/