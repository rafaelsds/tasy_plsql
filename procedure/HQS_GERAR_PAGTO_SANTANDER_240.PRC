create or replace
procedure HQS_GERAR_PAGTO_SANTANDER_240 
				(nr_seq_envio_p		number,
				nm_usuario_p		varchar2) is

ie_registro_w			varchar2(255);
tp_registro_w			number(15);
nr_inscricao_w			varchar2(255);
nr_contrato_w			varchar2(255);
nr_seq_envio_w			number(15);
nr_lote_w                       number(15);
cd_agencia_w			varchar2(255);
cd_conta_w			varchar2(255);
nr_digito_conta_w		varchar2(255);
nm_empresa_w			varchar2(255);
dt_arquivo_w			date;
hr_arquivo_w			varchar2(255);
ie_tipo_servico_w		varchar2(255);
ie_forma_lancamento_w		varchar2(255);
ds_mensagem_w			varchar2(255);
ds_endereco_w			varchar2(255);
nr_endereco_w			varchar2(255);
ds_complemento_w		varchar2(255);
ds_cidade_w			varchar2(255);
cd_cep_w			varchar2(255);
cd_compl_cep_w			varchar2(255);
sg_estado_w			varchar2(255);
ie_emissao_lote_w		varchar2(255);
ie_tipo_movimento_W		varchar2(255);
cd_banco_w			varchar2(255);
cd_compensacao_w		varchar2(255);
nm_favorecido_w			varchar2(255);
nr_documento_w			number(15);
dt_lancamento_w			date;
vl_saldo_titulo_w		number(15,2);
ie_emissao_individual_w		varchar2(255);
vl_pagamento_w			number(15,2);
dt_pagamento_w			date;
qt_registros_w			number(15)	:= 0;
vl_total_pagto_w		number(15,2);
cd_moedas_w			varchar2(255);
cd_barras_W			varchar2(255);
dt_vencimento_W			date;
vl_titulo_w			number(15,2);
vl_desconto_W			number(15,2);
vl_acrescimo_W			number(15,2);
vl_pagto_w			number(15,2);
qt_moeda_w			number(15);
qt_lotes_arquivos_w		number(15)	:= 0;
qt_total_registros_W		number(15);
ds_bairro_W			varchar2(255);
tp_incrisao_w			varchar2(255);
cd_cgc_fornecedor_w		varchar2(255);
nr_titulo_w			number(15);
nr_registro_w			number(15);
nr_remessa_W			varchar2(255);
cd_convenio_w			varchar2(25);
ie_union_w			number(15);
ie_union_ant_w			number(15);
nr_lote_cont_w			number(15);
nr_seq_apres_w			number(10)	:= 0;
nr_ordem_w			number(10);

cursor c01 is
select	1						ie_union,
	1						ie_registro,
	0						ie_tipo_registro,
	a.cd_cgc					nr_inscricao,
	g.cd_interno					nr_contrato,
	e.nr_sequencia					nr_seq_envio,
	somente_numero(g.cd_agencia_bancaria)		cd_agencia,
	to_number(substr(g.cd_conta,1,15))		cd_conta,
	substr(g.ie_digito_conta,1,1)			nr_digito_conta,
	upper(p.ds_razao_social)			nm_empresa,
	sysdate						dt_arquivo,
	to_char(e.dt_remessa_retorno,'hh24miss')	hr_arquivo,
	0						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	' '						ds_endereco,
	' '						nr_endereco,
	' '						ds_complemento,
	' '						ds_cidade,
	' '						cd_cep,
	' '						cd_compl_cep,
	' '						sg_estado,
	' '						ie_emissao_lote,
	0						ie_tipo_movimento,
	0						cd_banco,
	0						cd_compensacao,
	' '						nm_favorecido,
	-2						nr_documento,
	sysdate						dt_lancamento,
	0						vl_saldo_titulo,
	' '						ie_emissao_individual,
	0						vl_pagamento,
	sysdate						dt_pagamento,
	0						vl_total_pagto,
	0						cd_moeda,
	' '						cd_barras,
	sysdate						dt_vencimento,
	0						vl_titulo,
	0						vl_desconto,
	0						vl_acrescimo,
	0						vl_pagto,
	0						qt_moeda,
	0						qt_total_registros,
	' '						ds_bairro,
	' '		 				tp_inscricao,
	' '						cd_cgc_fornecedor,
	-2						nr_titulo,
	e.nr_remessa					nr_remessa,
	g.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	-2						nr_ordem
from	estabelecimento a,
	banco_estabelecimento_v g,
	pessoa_juridica p,
	banco_escritural e
where	e.cd_estabelecimento   	= a.cd_estabelecimento
  and	a.cd_cgc		= p.cd_cgc
  and	g.nr_sequencia		= e.nr_seq_conta_banco
  and	g.ie_tipo_relacao      	in ('EP','ECC')
  and	e.nr_sequencia		= nr_seq_envio_p
union
/*	Header do Lote		*/
select	distinct 
	2						ie_union,
	2						ie_registro,
	1						ie_tipo_registro,
	e.cd_cgc					nr_inscricao,
	b.cd_interno					nr_contrato,
	a.nr_sequencia					nr_seq_envio,
	somente_numero(b.cd_agencia_bancaria)		cd_agencia,
	to_number(substr(b.cd_conta,1,15))		cd_conta,
	substr(b.ie_digito_conta,1,1)			nr_digito_conta,
	upper(j.ds_razao_social)			nm_empresa,
	sysdate						dt_arquivo,
	to_char(sysdate,'hh24miss')			hr_arquivo,
	20						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	upper(j.ds_endereco)				ds_endereco,
	' '						nr_endereco,
	j.ds_complemento				ds_complemento,
	j.ds_municipio					ds_cidade,
	substr(j.cd_cep,1,5)				cd_cep,
	substr(j.cd_cep,6,3)				cd_compl_cep,
	j.sg_estado					sg_estado,
	'S'						ie_emissao_lote,
	0						ie_tipo_movimento,
	0						cd_banco,
	0						cd_compensacao,
	' '						nm_favorecido,
	-1						nr_documento,
	sysdate						dt_lancamento,
	0						vl_saldo_titulo,
	' '						ie_emissao_individual,
	0						vl_pagamento,
	sysdate						dt_pagamento,
	0						vl_total_pagto,
	0						cd_moeda,
	' '						cd_barras,
	sysdate						dt_vencimento,
	0						vl_titulo,
	0						vl_desconto,
	0						vl_acrescimo,
	0						vl_pagto,
	0						qt_moeda,
	0						qt_total_registros,
	' '						ds_bairro,
	' '		 				tp_inscricao,
	' '						cd_cgc_fornecedor,
	-1						nr_titulo,
	a.nr_remessa					nr_remessa,
	b.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	-1						nr_ordem
from	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_escrit c
where	a.nr_sequencia		= c.nr_seq_escrit
  and	e.cd_cgc		= j.cd_cgc
  and	a.cd_estabelecimento    = e.cd_estabelecimento
  and	b.ie_tipo_relacao       in ('EP','ECC')
  and	b.nr_sequencia		= a.nr_seq_conta_banco
  and	a.nr_sequencia		= nr_seq_envio_p
union
/*	Detalhe Documento - Segmento A	*/
select	distinct 
	3						ie_union,
	3						ie_registro,
	2						ie_tipo_registro,
	' '						nr_inscricao,
	0						nr_contrato,
	a.nr_sequencia					nr_seq_envio,
	somente_numero(c.cd_agencia_bancaria)		cd_agencia,
	to_number(d.nr_conta_favorecido)		cd_conta,
	substr(d.ie_dig_conta_favorecido,1,1)		nr_digito_conta,
	' '						nm_empresa,
	sysdate						dt_arquivo,
	to_char(sysdate,'hh24miss')			hr_arquivo,
	0						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	' '						ds_endereco,
	' '						nr_endereco,
	' '						ds_complemento,
	' '						ds_cidade,
	' '						cd_cep,
	' '						cd_compl_cep,
	' '						sg_estado,
	' '						ie_emissao_lote,
	0						ie_tipo_movimento,
	c.cd_banco					cd_banco,
	decode(c.ie_tipo_pagamento,null,cd_camara_compensacao,'DOC',700,18)	cd_compensacao,
	d.nm_favorecido					nm_favorecido,
	d.nr_titulo					nr_documento,
	d.dt_emissao					dt_lancamento,
	nvl(d.vl_saldo_titulo,0)			vl_saldo_titulo,
	'S'						ie_emissao_individual,
	c.vl_escritural					vl_pagamento,
	sysdate						dt_pagamento,
	0						vl_total_pagto,
	0						cd_moeda,
	' '						cd_barras,
	d.dt_vencimento_atual				dt_vencimento,
	0						vl_titulo,
	0						vl_desconto,
	0						vl_acrescimo,
	0						vl_pagto,
	0						qt_moeda,
	0						qt_total_registros,
	' '						ds_bairro,
	' '		 				tp_inscricao,
	d.cd_favorecido 				cd_cgc_fornecedor,
	d.nr_titulo					nr_titulo,
	a.nr_remessa					nr_remessa,
	b.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	d.nr_titulo					nr_ordem
from	estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
where	c.nr_titulo		= d.nr_titulo
and	a.nr_sequencia		= c.nr_seq_escrit
and	a.cd_estabelecimento	= e.cd_estabelecimento
and 	b.ie_tipo_relacao	in ('EP','ECC')
and	b.nr_sequencia		= a.nr_seq_conta_banco
and	a.nr_sequencia		= nr_seq_envio_p
Union
/*	Detalhe Documento - Segmento B	*/
select	distinct 
	4						ie_union,
	3						ie_registro,
	3						ie_tipo_registro,
	' '						nr_inscricao,
	0						nr_contrato,
	a.nr_sequencia					nr_seq_envio,
	0						cd_agencia,
	0						cd_conta,
	' '		 				nr_digito_conta,
	' '						nm_empresa,
	sysdate						dt_arquivo,
	to_char(sysdate,'hh24miss')			hr_arquivo,
	0						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	upper(d.ds_endereco)				ds_endereco,
	' '						nr_endereco,
	' '						ds_complemento,
	d.ds_cidade					ds_cidade,
	d.cd_cep					cd_cep,
	' '						cd_compl_cep,
	d.ds_estado					sg_estado,
	' '						ie_emissao_lote,
	0						ie_tipo_movimento,
	0						cd_banco,
	0						cd_compensacao,
	' '						nm_favorecido,
	d.nr_documento					nr_documento,
	sysdate						dt_lancamento,
	d.vl_saldo_titulo				vl_saldo_titulo,
	' '						ie_emissao_individual,
	0						vl_pagamento,
	sysdate						dt_pagamento,
	0						vl_total_pagto,
	0						cd_moeda,
	' '						cd_barras,
	d.dt_vencimento_atual				dt_vencimento,
	d.vl_titulo					vl_titulo,
	c.vl_desconto					vl_desconto,
	c.vl_acrescimo					vl_acrescimo,
	0						vl_pagto,
	0						qt_moeda,
	0						qt_total_registros,
	d.ds_bairro					ds_bairro,
	d.ie_tipo_favorecido 				tp_inscricao,
	d.cd_favorecido					cd_cgc_fornecedor,
	d.nr_titulo					nr_titulo,
	a.nr_remessa					nr_remessa,
	b.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	d.nr_titulo					nr_ordem
from	estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
where	c.nr_titulo		= d.nr_titulo
and	a.nr_sequencia		= c.nr_seq_escrit
and	a.cd_estabelecimento	= e.cd_estabelecimento
and 	b.ie_tipo_relacao	in ('EP','ECC')
and	b.nr_sequencia		= a.nr_seq_conta_banco
and	a.nr_sequencia		= nr_seq_envio_p
Union
/*	Trailler do Lote		*/
select	distinct 
	5						ie_union,
	4						ie_registro,
	4						ie_tipo_registro,
	' '						nr_inscricao,
	0						nr_contrato,
	e.nr_sequencia					nr_seq_envio,
	0						cd_agencia,
	0						cd_conta,
	' '		 				nr_digito_conta,
	' '						nm_empresa,
	sysdate						dt_arquivo,
	to_char(sysdate,'hh24miss')			hr_arquivo,
	0						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	' '						ds_endereco,
	' '						nr_endereco,
	' '						ds_complemento,
	' '						ds_cidade,
	' '						cd_cep,
	' '						cd_compl_cep,
	' '						sg_estado,
	' '						ie_emissao_lote,
	0						ie_tipo_movimento,
	0						cd_banco,
	0						cd_compensacao,
	' '						nm_favorecido,
	9999995						nr_documento,
	sysdate						dt_lancamento,
	0						vl_saldo_titulo,
	' '						ie_emissao_individual,
	0						vl_pagamento,
	sysdate						dt_pagamento,
	sum(nvl(c.vl_escritural,0) - nvl(c.vl_desconto,0) + nvl(c.vl_acrescimo,0)) vl_total_pagto,
	0						cd_moeda,
	' '						cd_barras,
	sysdate						dt_vencimento,
	0						vl_titulo,
	0						vl_desconto,
	0						vl_acrescimo,
	0						vl_pagto,
	0						qt_moeda,
	0						qt_total_registros,
	' '						ds_bairro,
	' '		 				tp_inscricao,
	' '						cd_cgc_fornecedor,
	999995						nr_titulo,
	e.nr_remessa					nr_remessa,
	b.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	999995						nr_ordem
from	banco_estabelecimento_v b,
	estabelecimento a,
	banco_escritural e,
	titulo_pagar_escrit c
where	e.nr_sequencia		= c.nr_seq_escrit
and	e.cd_estabelecimento    = a.cd_estabelecimento
and	b.ie_tipo_relacao      	in ('EP','ECC')
and	b.nr_sequencia		= e.nr_seq_conta_banco
and	e.nr_sequencia		= nr_seq_envio_p
group by	e.nr_sequencia,
		e.nr_remessa,
		b.cd_convenio_banco
union
/*	Header do Lote - Bloquetos	*/
select	distinct 
	6						ie_union,
	5						ie_registro,
	5						ie_tipo_registro,
	e.cd_cgc					nr_inscricao,
	b.cd_interno					nr_contrato,
	a.nr_sequencia					nr_seq_envio,
	somente_numero(b.cd_agencia_bancaria)		cd_agencia,
	to_number(substr(b.cd_conta,1,15))		cd_conta,
	substr(b.ie_digito_conta,1,1)			nr_digito_conta,
	upper(j.ds_razao_social)			nm_empresa,
	sysdate						dt_arquivo,
	to_char(sysdate,'hh24miss')			hr_arquivo,
	01						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	upper(j.ds_endereco)				ds_endereco,
	j.nr_endereco					nr_endereco,
	j.ds_complemento				ds_complemento,
	j.ds_municipio					ds_cidade,
	substr(j.cd_cep,1,5)				cd_cep,
	substr(j.cd_cep,6,3)				cd_compl_cep,
	j.sg_estado					sg_estado,
	'S'						ie_emissao_lote,
	0						ie_tipo_movimento,
	0						cd_banco,
	0						cd_compensacao,
	upper(j.ds_razao_social)			nm_favorecido,
	9999996						nr_documento,
	sysdate						dt_lancamento,
	0						vl_saldo_titulo,
	' '						ie_emissao_individual,
	0						vl_pagamento,
	sysdate						dt_pagamento,
	0						vl_total_pagto,
	0						cd_moeda,
	' '						cd_barras,
	sysdate						dt_vencimento,
	0						vl_titulo,
	0						vl_desconto,
	0						vl_acrescimo,
	0						vl_pagto,
	0						qt_moeda,

	0						qt_total_registros,
	' '						ds_bairro,
	' '		 				tp_inscricao,
	' '						cd_cgc_fornecedor,
	999996						nr_titulo,
	a.nr_remessa					nr_remesssa,
	b.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	999996						nr_ordem
from	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar k,
	titulo_pagar_escrit c
where	e.cd_cgc			= j.cd_cgc
  and	a.nr_sequencia          	= c.nr_seq_escrit
  and	a.cd_estabelecimento    	= e.cd_estabelecimento
  and	b.ie_tipo_relacao       	in ('EP','ECC')
  and	a.nr_seq_conta_banco		= b.nr_sequencia
  and	c.nr_titulo			= k.nr_titulo
  and 	c.ie_tipo_pagamento		= 'BLQ'
  and	a.nr_sequencia			= nr_seq_envio_p
union
/*	Detalhe Bloquetos	*/
select	distinct 
	7						ie_union,
	6						ie_registro,
	6						ie_tipo_registro,
	' '						nr_inscricao,
	0						nr_contrato,
	a.nr_sequencia					nr_seq_envio,
	somente_numero(c.cd_agencia_bancaria)		cd_agencia,
	to_number(c.nr_conta)				cd_conta,
	substr(c.ie_digito_conta,1,1)			nr_digito_conta,
	' '						nm_empresa,
	sysdate						dt_arquivo,
	to_char(sysdate,'hh24miss')			hr_arquivo,
	0						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	' '						ds_endereco,
	' '						nr_endereco,
	' '						ds_complemento,
	' '						ds_cidade,
	' '						cd_cep,
	' '						cd_compl_cep,
	' '						sg_estado,
	'S'						ie_emissao_lote,
	0						ie_tipo_movimento,
	c.cd_banco					cd_banco,
	0						cd_compensacao,
	d.nm_favorecido					nm_favorecido,
	9999997						nr_documento,
	sysdate						dt_lancamento,
	0						vl_saldo_titulo,
	' '						ie_emissao_individual,
	0						vl_pagamento,
	sysdate						dt_pagamento,
	0						vl_total_pagto,
	k.cd_moeda					cd_moeda,
	d.nr_bloqueto					cd_barras,
	d.dt_vencimento_atual				dt_vencimento,
	nvl(d.vl_titulo,0)				vl_titulo,
	nvl(c.vl_desconto,0)				vl_desconto,
	nvl(c.vl_acrescimo,0)				vl_acrescimo,
	nvl(c.vl_escritural,0)				vl_pagto,
	0						qt_moeda,	
	0						qt_total_registros,
	' '						ds_bairro,
	' '		 				tp_inscricao,
	' '						cd_cgc_fornecedor,
	C.NR_TITULO					nr_titulo,
	a.nr_remessa					nr_remessa,
	b.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	999997						nr_ordem
from	estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_v k,
	titulo_pagar_escrit c
where	c.nr_titulo			= d.nr_titulo
and	k.nr_titulo			= d.nr_titulo
and	a.nr_sequencia			= c.nr_seq_escrit
and	a.cd_estabelecimento		= e.cd_estabelecimento
and 	b.ie_tipo_relacao		in ('EP','ECC')
and	a.nr_seq_conta_banco		= b.nr_sequencia
and 	c.ie_tipo_pagamento		= 'BLQ'
and	a.nr_sequencia			= nr_seq_envio_p
Union
/*	Trailler do Lote - Bloquetos	*/
select	distinct 
	8						ie_union,
	7						ie_registro,
	7						ie_tipo_registro,
	' '						nr_inscricao,
	0						nr_contrato,
	e.nr_sequencia					nr_seq_envio,
	0						cd_agencia,
	0						cd_conta,
	' '						nr_digito_conta,
	' '						nm_empresa,
	sysdate						dt_arquivo,
	to_char(sysdate,'hh24miss')			hr_arquivo,
	0						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	' '						ds_endereco,
	' '						nr_endereco,
	' '						ds_complemento,
	' '						ds_cidade,
	' '						cd_cep,
	' '						cd_compl_cep,
	' '						sg_estado,
	' '						ie_emissao_lote,
	0						ie_tipo_movimento,
	0						cd_banco,
	0						cd_compensacao,
	' '						nm_favorecido,
	9999998						nr_documento,
	sysdate						dt_lancamento,
	0						vl_saldo_titulo,
	'S'						ie_emissao_individual,
	0						vl_pagamento,
	sysdate						dt_pagamento,
	sum(nvl(c.vl_escritural,0) - nvl(c.vl_desconto,0) + nvl(c.vl_acrescimo,0)) vl_total_pagto,
	0						cd_moeda,
	' '						cd_barras,
	sysdate						dt_vencimento,
	0						vl_titulo,
	0						vl_desconto,
	0						vl_acrescimo,
	0						vl_pagto,
	0						qt_moeda,
	0						qt_total_registros,
	' '						ds_bairro,
	' '		 				tp_inscricao,
	' '						cd_cgc_fornecedor,
	999998						nr_titulo,
	e.nr_remessa					nr_remessa,
	b.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	999998						nr_ordem
from	estabelecimento a,
	banco_estabelecimento_v b,
	banco_escritural e,
	titulo_pagar_v2 d,
	titulo_pagar_v k,
	titulo_pagar_escrit c
where	c.nr_titulo			= d.nr_titulo
and	k.nr_titulo			= d.nr_titulo
and	e.nr_sequencia			= c.nr_seq_escrit
and	e.cd_estabelecimento		= a.cd_estabelecimento
and 	b.ie_tipo_relacao		in ('EP','ECC')
and	e.nr_seq_conta_banco		= b.nr_sequencia
and 	c.ie_tipo_pagamento		= 'BLQ'
and	e.nr_sequencia			= nr_seq_envio_p
group by e.nr_sequencia,
	e.nr_remessa,
	b.cd_convenio_banco
union
/*	Trailler do Arquivo	*/
select	distinct 
	9						ie_union,
	8						ie_registro,
	8						ie_tipo_registro,
	' '						nr_inscricao,
	0						nr_contrato,
	e.nr_sequencia					nr_seq_envio,
	0						cd_agencia,
	0						cd_conta,
	' '						nr_digito_conta,
	' '						nm_empresa,
	sysdate						dt_arquivo,
	to_char(sysdate,'hh24miss')			hr_arquivo,
	0						ie_tipo_servico,
	'03'						ie_forma_lancamento,
	' '						ds_mensagem,
	' '						ds_endereco,
	' '						nr_endereco,
	' '						ds_complemento,
	' '						ds_cidade,
	' '						cd_cep,
	' '						cd_compl_cep,
	' '						sg_estado,
	' '						ie_emissao_lote,
	0						ie_tipo_movimento,
	0						cd_banco,
	0						cd_compensacao,
	' '						nm_favorecido,
	9999999						nr_documento,
	sysdate						dt_lancamento,
	0						vl_saldo_titulo,
	' '						ie_emissao_individual,
	0						vl_pagamento,
	sysdate						dt_pagamento,
	0						vl_total_pagto,
	0						cd_moeda,
	' '						cd_barras,
	sysdate						dt_vencimento,
	0						vl_titulo,
	0						vl_desconto,
	0						vl_acrescimo,
	0						vl_pagto,
	0						qt_moeda,
	count(*)					qt_total_registros,
	' '						ds_bairro,
	' '		 				tp_inscricao,
	' '						cd_cgc_fornecedor,
	999999						nr_titulo,
	e.nr_remessa					nr_remessa,
	b.cd_convenio_banco				cd_convenio,
	0						nr_lote,
	999999						nr_ordem
from	banco_estabelecimento_v b,
	estabelecimento a,
	banco_escritural e,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
where	c.nr_titulo		= d.nr_titulo
and	e.nr_sequencia		= c.nr_seq_escrit
and	e.cd_estabelecimento    = a.cd_estabelecimento
and	b.ie_tipo_relacao       in ('EP','ECC')
and	b.nr_sequencia		= e.nr_seq_conta_banco
and	e.nr_sequencia		= nr_seq_envio_p
group by e.nr_sequencia,
	e.nr_remessa,
	b.cd_convenio_banco
order by	nr_ordem,
	ie_tipo_registro;

begin

delete from w_interf_itau;

nr_registro_w	:= 0;
ie_union_ant_w	:= null;
qt_lotes_arquivos_w  := 0;
nr_lote_cont_w	:= 0;

open c01;
loop
fetch c01 into
	ie_union_w,
	ie_registro_w,
	tp_registro_w,
	nr_inscricao_w,
	nr_contrato_w,
	nr_seq_envio_w,
	cd_agencia_w,
	cd_conta_w,
	nr_digito_conta_w,
	nm_empresa_w,
	dt_arquivo_w,
	hr_arquivo_w,
	ie_tipo_servico_w,
	ie_forma_lancamento_w,
	ds_mensagem_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_cidade_w,
	cd_cep_w,
	cd_compl_cep_w,
	sg_estado_w,
	ie_emissao_lote_w,
	ie_tipo_movimento_W,
	cd_banco_w,
	cd_compensacao_w,
	nm_favorecido_w,
	nr_documento_w,
	dt_lancamento_w,
	vl_saldo_titulo_w,
	ie_emissao_individual_w,
	vl_pagamento_w,
	dt_pagamento_w,
	vl_total_pagto_w,
	cd_moedas_w,
	cd_barras_W,
	dt_vencimento_W,
	vl_titulo_w,
	vl_desconto_W,
	vl_acrescimo_W,
	vl_pagto_w,
	qt_moeda_w,
	qt_total_registros_W,
	ds_bairro_W,
	tp_incrisao_w,
	cd_cgc_fornecedor_w,
	nr_titulo_w,
	nr_remessa_W,
	cd_convenio_w,
	nr_lote_w,
	nr_ordem_w;
	exit when c01%notfound;
	nr_seq_apres_w	:= nr_seq_apres_w + 1;

	if (length(cd_barras_W) = 48) then
		cd_barras_W := substr(cd_barras_W, 1, 11) || substr(cd_barras_W, 13, 11) || substr(cd_barras_W, 25, 11) || substr(cd_barras_W, 37, 11);
	end if;
	insert into w_interf_itau 
		(nr_sequencia,
		cd_agencia_bancaria,
		cd_banco_fornec,         
		cd_barras,          
		cd_camara_compensacao,                
		cd_cep,
		cd_cep_compl,
		cd_cgc_fornecedor,       
		cd_conta,                
		cd_convenio_banco,
		cd_moeda,
		ds_bairro,
		ds_cidade,
		ds_complemento,
		ds_brancos,
		ds_endereco,
		cd_carteira,
		dt_arquivo,
		dt_geracao,
		dt_pagto,
		dt_vencimento,
		ie_digito_conta,
		ie_emissao_individual,
		ie_forma_lancamento,
		ie_registro,
		ie_tipo_inscricao,
		ie_tipo_movimento,
		ie_tipo_servico,
		ie_tipo_registro,
		nm_empresa,
		nm_fornecedor,
		nm_usuario,
		nr_contrato,
		nr_documento,
		nr_endereco,
		nr_inscricao,
		nr_remessa,
		nr_registro,
		nr_seq_apres,
		nr_seq_envio,
		nr_titulo,
		nr_lote,
		qt_lote_arquivo,
		qt_moeda,
		qt_registros,
		Qt_tot_reg,
		sg_estado,
		vl_acrescimo,
		vl_desconto,
		vl_pagto,
		vl_previsto,
		vl_saldo_titulo,
		vl_titulo,
		vl_total_pagto,
		dt_atualizacao)
	values (w_interf_itau_seq.nextval,
		cd_agencia_w,
		cd_banco_w,
		cd_barras_w,
		cd_compensacao_w,
		cd_cep_w,
		cd_compl_cep_w,
		cd_cgc_fornecedor_w,
		cd_conta_w,
		cd_convenio_w,	
		cd_moedas_w,
		substr(ds_bairro_w,1,39),
		substr(ds_cidade_w,1,39),
		ds_complemento_w,
		ds_mensagem_w,
		ds_endereco_w,
		hr_arquivo_w,
		dt_arquivo_w,
		dt_lancamento_w,
		dt_pagamento_w,
		dt_vencimento_w,
		nr_digito_conta_w,
		ie_emissao_individual_w,
		ie_forma_lancamento_w,
		ie_registro_w,
		tp_incrisao_w,
		ie_tipo_movimento_w,
		ie_tipo_servico_w,
		tp_registro_w,
		substr(nm_empresa_w,1,79),
		substr(nm_favorecido_w,1,79),
		'TASY',
		nr_contrato_w,
		nr_documento_w,
		nr_endereco_w,
		nr_inscricao_w,
		nr_remessa_w,
		nr_registro_W,
		nr_seq_apres_w,
		nr_seq_envio_w,
		nr_titulo_w,
		substr(nr_lote_w,1,3),
		qt_lotes_arquivos_w,
		qt_moeda_w,
		qt_registros_w,
		qt_total_registros_w,
		sg_estado_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_pagto_w,
		vl_pagamento_w,
		vl_saldo_titulo_W,
		vl_titulo_w,
		vl_total_pagto_w,
		sysdate);
	
	if	(tp_registro_w in (2,3,6,7)) then
		nr_registro_w		:= nr_registro_w + 1;
	end if;
	
	if	(tp_registro_w in (4,8)) then
		nr_lote_w		:= nr_lote_w + 1;
		nr_registro_w		:= 1;
		qt_registros_w  	:= 1;
	end if;	
	
	if	(tp_registro_w in (4,8)) then
		qt_registros_w  	:= 1;
	end if;	
	
	if	(tp_registro_w = 1) then
		qt_lotes_arquivos_w 	:= qt_lotes_arquivos_w + 1;
	end if;
	
	if	(tp_registro_w in (1,2,3,5,6)) then
		qt_registros_w	:= qt_registros_w + 1;			
	end if;

end loop;
close c01;

commit;	

end HQS_GERAR_PAGTO_SANTANDER_240;
/
