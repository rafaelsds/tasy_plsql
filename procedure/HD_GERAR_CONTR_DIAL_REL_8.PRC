create or replace procedure hd_gerar_contr_dial_rel_8(	nr_seq_protocolo_p	Number,
					ds_itens_p		Varchar2,
					nm_usuario_p		Varchar2,
					nr_seq_unidade_p	Number,
					nr_seq_turno_p		Number,
					nr_seq_tratamento_p	Varchar2,
					ie_dia_semana_p		Varchar2,
					ie_pac_ativos_p		Varchar2,
					ie_hemodialise_fim_p	Varchar2,
					ie_pac_hemodialise_p	Varchar2,
					dt_referencia_p		date,
					ie_diabetico_p		Varchar2,
					ie_hipertenso_p		Varchar2,
					qt_faixa_inicial_p	Number,
					qt_faixa_final_p	Number,
					nr_seq_exame_p		number,
					qt_result_exame_1_p	number,
					qt_result_exame_2_p	number) is


/*Ordem:
1 -> Titulo
5 -> Data
*/

/* vetor */
type colunas is record (nm_coluna_w varchar2(100));
type vetor is table of colunas index by binary_integer;


dt_referencia_w		date;
/* globais */
vetor_w			vetor;
vetor1_w		vetor;
ds_item_w		varchar2(2000);
ds_item_ant_w		varchar2(2000);
ivet			integer;
ind			integer;
nr_seq_nova_w		number(10);
ds_formula_w		varchar2(2000);
ds_cores_w		varchar2(2000);
ds_cor_item_w		varchar2(20);
ds_cor_w		varchar2(20);
nr_seq_exame_w		number(10);
nr_seq_apresent_w	number(10);
nr_sequencia_w		number(10);
qt_casas_decimais_w	number(10);
qt_valor_minimo_w	number(15,4);
qt_valor_maximo_w	number(15,4);
ds_cor_fora_padrao_w	varchar2(15);
nr_seq_exame_grid_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ie_exame_grid_controle_w varchar2(1);
nr_seq_item_cont_w	number(10);
ie_exclui_reg_w		varchar2(1) := 'N';


cursor c01 is
	select	ds_item
	from	hd_item_controle_exame
	where	rownum < 50
	and	((nvl(nr_seq_protocolo_p,0) = 0) or (Hd_obter_se_item_protocolo(nr_seq_protocolo_p,nr_sequencia) = 'S'))
	and	((ds_itens_p is null) or (OBTER_SE_CONTIDO(nr_sequencia,ds_itens_p) = 'S'))
	and 	nr_sequencia = ds_itens_p
	order by nr_seq_apresent, nr_sequencia;

cursor 	c02 is
	select	ds_formula,
		nr_seq_exame,
		nr_seq_apresent,
		ds_cor,
		nvl(qt_casas_decimais,2) qt_casas_decimais,
		qt_valor_minimo,
		qt_valor_maximo	,
		ds_cor_fora_padrao,
		nr_sequencia
	from	hd_item_controle_exame
	where	rownum < 50
	and	((nvl(nr_seq_protocolo_p,0) = 0) or (Hd_obter_se_item_protocolo(nr_seq_protocolo_p,nr_sequencia) = 'S'))
	and	((ds_itens_p is null) or (OBTER_SE_CONTIDO(nr_sequencia,ds_itens_p) = 'S'))
	and 	nr_sequencia = ds_itens_p
	order by nr_seq_apresent, nr_sequencia;

cursor	c03 is
	select	distinct
		dt_referencia
	from	HD_EXAME_GRID_CONTROLE
	where	nm_usuario = nm_usuario_p
	and	ie_ordem = 5
	and	dt_referencia is not null
	and 	dt_referencia = dt_referencia_w
	order by dt_referencia desc;

cursor c04 is
	select	cd_pessoa_fisica
	from	hd_pac_renal_cronico
	where	(nvl(hd_obter_unidade_prc(CD_PESSOA_FISICA,'C'),NR_SEQ_UNID_DIALISE) = NR_SEQ_UNIDADE_p or nvl(NR_SEQ_UNIDADE_p,0) = 0)
	and 	(nvl(nr_seq_turno_p,0) = 0
	or	HD_Obter_Turno_PRC_data(cd_pessoa_fisica,sysdate, 'C') = nr_seq_turno_p
	or 	hd_obter_se_pac_dia_turno(cd_pessoa_fisica,nr_seq_turno_p) = 'S')
	and	(nr_seq_tratamento_p is null
	or	obter_se_paciente_tratamento (cd_pessoa_fisica, nr_seq_tratamento_p) = 'S')
	and 	(ie_dia_semana_p is null
	or	hd_obter_se_pac_dia_dialise(cd_pessoa_fisica, ie_dia_semana_p, 'C') = 'S'
	or   	hd_obter_se_pac_dia_semana(cd_pessoa_fisica, ie_dia_semana_p) = 'S' )
	and	(ie_pac_ativos_p = 'N' or substr(hd_obter_se_paciente_ativo(cd_pessoa_fisica),1,1) = 'S')
	and	(ie_hemodialise_fim_p = 'S' or hd_obter_se_em_dialise(cd_pessoa_fisica, 'F') = 'N')
	and	(ie_pac_hemodialise_p = 'N' or hd_obter_se_em_dialise(cd_pessoa_fisica, 'E') = 'S')
	and	(ie_diabetico_p = 'N' or ie_diabetico = ie_diabetico_p)
	and	(ie_hipertenso_p = 'N' or ie_hipertenso = ie_hipertenso_p)
	and	Obter_Idade_PF(cd_pessoa_fisica, sysdate, 'A') >= nvl(qt_faixa_inicial_p,0)
	and	Obter_Idade_PF(cd_pessoa_fisica, sysdate, 'A') <= nvl(qt_faixa_final_p,999)
	order by cd_pessoa_fisica;

begin

dt_referencia_w	:= trunc(dt_referencia_p,'month');

ivet	:= 0;
begin
for c01_w in c01 loop
	begin
	ds_item_w := c01_w.ds_item;
	ivet := ivet + 1;
	vetor_w(ivet).nm_coluna_w := ds_item_w;
	end;
end loop;
end;

ind := ivet;
while	(ind < 50) loop
	begin
	ind := ind + 1;
	vetor_w(ind).nm_coluna_w := null;
	end;
end loop;

select	HD_GRID_CONTROLE_DIALISE_seq.nextval
into	nr_sequencia_w
from	dual;

insert into HD_GRID_CONTROLE_DIALISE(
	nr_sequencia,
	nm_usuario,
	ie_ordem,
	dt_referencia,
	ds_resultado1,
	ds_resultado2,
	ds_resultado3,
	ds_resultado4,
	ds_resultado5,
	ds_resultado6,
	ds_resultado7,
	ds_resultado8,
	ds_resultado9,
	ds_resultado10,
	ds_resultado11,
	ds_resultado12,
	ds_resultado13,
	ds_resultado14,
	ds_resultado15,
	ds_resultado16,
	ds_resultado17,
	ds_resultado18,
	ds_resultado19,
	ds_resultado20,
	ds_resultado21,
	ds_resultado22,
	ds_resultado23,
	ds_resultado24,
	ds_resultado25,
	ds_resultado26,
	ds_resultado27,
	ds_resultado28,
	ds_resultado29,
	ds_resultado30,
	ds_resultado31,
	ds_resultado32,
	ds_resultado33,
	ds_resultado34,
	ds_resultado35,
	ds_resultado36,
	ds_resultado37,
	ds_resultado38,
	ds_resultado39,
	ds_resultado40,
	ds_resultado41,
	ds_resultado42,
	ds_resultado43,
	ds_resultado44,
	ds_resultado45,
	ds_resultado46,
	ds_resultado47,
	ds_resultado48,
	ds_resultado49,
	ds_resultado50,
	dt_atualizacao)
Values( nr_sequencia_w,
	nm_usuario_p,
	1,
	dt_referencia_w,
	vetor_w(1).nm_coluna_w,
	vetor_w(2).nm_coluna_w,
	vetor_w(3).nm_coluna_w,
	vetor_w(4).nm_coluna_w,
	vetor_w(5).nm_coluna_w,
	vetor_w(6).nm_coluna_w,
	vetor_w(7).nm_coluna_w,
	vetor_w(8).nm_coluna_w,
	vetor_w(9).nm_coluna_w,
	vetor_w(10).nm_coluna_w,
	vetor_w(11).nm_coluna_w,
	vetor_w(12).nm_coluna_w,
	vetor_w(13).nm_coluna_w,
	vetor_w(14).nm_coluna_w,
	vetor_w(15).nm_coluna_w,
	vetor_w(16).nm_coluna_w,
	vetor_w(17).nm_coluna_w,
	vetor_w(18).nm_coluna_w,
	vetor_w(19).nm_coluna_w,
	vetor_w(20).nm_coluna_w,
	vetor_w(21).nm_coluna_w,
	vetor_w(22).nm_coluna_w,
	vetor_w(23).nm_coluna_w,
	vetor_w(24).nm_coluna_w,
	vetor_w(25).nm_coluna_w,
	vetor_w(26).nm_coluna_w,
	vetor_w(27).nm_coluna_w,
	vetor_w(28).nm_coluna_w,
	vetor_w(29).nm_coluna_w,
	vetor_w(30).nm_coluna_w,
	vetor_w(31).nm_coluna_w,
	vetor_w(32).nm_coluna_w,
	vetor_w(33).nm_coluna_w,
	vetor_w(34).nm_coluna_w,
	vetor_w(35).nm_coluna_w,
	vetor_w(36).nm_coluna_w,
	vetor_w(37).nm_coluna_w,
	vetor_w(38).nm_coluna_w,
	vetor_w(39).nm_coluna_w,
	vetor_w(40).nm_coluna_w,
	vetor_w(41).nm_coluna_w,
	vetor_w(42).nm_coluna_w,
	vetor_w(43).nm_coluna_w,
	vetor_w(44).nm_coluna_w,
	vetor_w(45).nm_coluna_w,
	vetor_w(46).nm_coluna_w,
	vetor_w(47).nm_coluna_w,
	vetor_w(48).nm_coluna_w,
	vetor_w(49).nm_coluna_w,
	vetor_w(50).nm_coluna_w,
	sysdate);

for c04_w in c04 loop
	begin
	cd_pessoa_fisica_w := c04_w.cd_pessoa_fisica;

	select	decode(count(*),0,'N','S')
	into	ie_exame_grid_controle_w
	from	HD_EXAME_GRID_CONTROLE
	where	nm_usuario = nm_usuario_p
	and	ie_ordem = 5
	and	dt_referencia is not null
	and	dt_referencia = dt_referencia_w;

	if (ie_exame_grid_controle_w = 'N') then
		insert into HD_EXAME_GRID_CONTROLE(
			nr_sequencia,
			nm_usuario,
			ie_ordem,
			dt_referencia,
			cd_pessoa_fisica,
			dt_atualizacao)
		Values( HD_EXAME_GRID_CONTROLE_seq.nextval,
			nm_usuario_p,
			5,
			dt_referencia_w,
			cd_pessoa_fisica_w,
			sysdate);
	end if;

	select 	HD_GRID_CONTROLE_DIALISE_seq.nextval
	into	nr_seq_nova_w
	from 	dual;

	insert into HD_GRID_CONTROLE_DIALISE(
		nr_sequencia,
		nm_usuario,
		ie_ordem,
		dt_referencia,
		cd_pessoa_fisica,
		dt_atualizacao)
	Values( nr_seq_nova_w,
		nm_usuario_p,
		5,
		dt_referencia_w,
		cd_pessoa_fisica_w,
		sysdate);

	ind := 0;
	ds_cores_w	:= null;

	for c02_w in c02 loop
		begin
		ds_formula_w           := c02_w.ds_formula;
		nr_seq_exame_w         := c02_w.nr_seq_exame;
		nr_seq_apresent_w      := c02_w.nr_seq_apresent;
		ds_cor_item_w          := c02_w.ds_cor;
		qt_casas_decimais_w    := c02_w.qt_casas_decimais;
		qt_valor_minimo_w      := c02_w.qt_valor_minimo;
		qt_valor_maximo_w      := c02_w.qt_valor_maximo;
		ds_cor_fora_padrao_w   := c02_w.ds_cor_fora_padrao;
		nr_seq_item_cont_w     := c02_w.nr_sequencia;

		ds_item_w	:= 0;

		for c03_w in c03 loop
			begin
			dt_referencia_w := c03_w.dt_referencia;

			select	max(nr_sequencia)
			into	nr_seq_exame_grid_w
			from	HD_EXAME_GRID_CONTROLE
			where	nm_usuario = nm_usuario_p
			and	ie_ordem = 5
			and	dt_referencia = dt_referencia_w;

			if	(nr_seq_exame_w is not null) then

				ds_item_w	:= 	obter_ult_resultado_data_lab(cd_pessoa_fisica_w, nr_seq_exame_grid_w, nr_seq_exame_w);
				ds_item_ant_w	:= 	ds_item_w;

				begin
				select	campo_mascara_virgula_casas(to_number(ds_item_w), qt_casas_decimais_w)
				into	ds_item_w
				from	dual;
				exception
					when others then
					ds_item_w := ds_item_ant_w;
				end;

			elsif	(ds_formula_w is not null) then

				ds_item_w	:= 	hd_obter_formula(cd_pessoa_fisica_w, nr_seq_exame_grid_w, ds_formula_w,qt_casas_decimais_w,'N');

			end if;

			begin
			if (trim(ds_item_w) <> '-') and (nvl(hd_somente_numero(ds_item_w),-1) <> 0) then
				exit;
			end if;
			exception
			when others then
				if (nvl(obter_somente_numero(ds_item_w),-1) <> 0) then
					exit;
				end if;
			end;


			end;
		end loop;

		ind := ind + 1;
		vetor1_w(ind).nm_coluna_w := ds_item_w;

		begin
		select	decode(length(to_char(ind)),1,'0'||to_char(ind),to_char(ind))
		into	ds_cor_w
		from	dual;



		if	(to_number(ds_item_w) < qt_valor_minimo_w) or (to_number(ds_item_w) > qt_valor_maximo_w) and (ds_cor_fora_padrao_w is not null) then
			ds_cores_w	:= ds_cores_w || ds_cor_w || ds_cor_fora_padrao_w ||',';
		else
			ds_cores_w	:= ds_cores_w || ds_cor_w || ds_cor_item_w ||',';
		end if;
		exception
			WHEN OTHERS THEN
				ds_cores_w	:= ds_cores_w || ds_cor_w || ds_cor_item_w ||',';
		end;


		if	(nr_seq_exame_p is not null) and
			(qt_result_exame_1_p is not null) and
			(qt_result_exame_2_p is not null) and
			(nr_seq_exame_p = nr_seq_item_cont_w) and
			(obter_somente_numero(ds_item_w) is not null) and
			(hd_somente_numero(ds_item_w) >= qt_result_exame_1_p) and
			(hd_somente_numero(ds_item_w) <= qt_result_exame_2_p)then
			ie_exclui_reg_w := 'N';
		elsif	(nr_seq_exame_p = nr_seq_item_cont_w) and
			(nr_seq_exame_p is not null) and
			(qt_result_exame_1_p is not null) and
			(qt_result_exame_2_p is not null) then
			ie_exclui_reg_w := 'S';
		end if;



		end;
	end loop;

	ind := ind;
	while	(ind < 50) loop
		begin
		ind := ind + 1;
		vetor1_w(ind).nm_coluna_w := null;
		end;
	end loop;

	UPDATE 	HD_GRID_CONTROLE_DIALISE
	SET	ds_cor		= ds_cores_w,
		ds_resultado1	= vetor1_w(1).nm_coluna_w,
		ds_resultado2	= vetor1_w(2).nm_coluna_w,
		ds_resultado3	= vetor1_w(3).nm_coluna_w,
		ds_resultado4	= vetor1_w(4).nm_coluna_w,
		ds_resultado5	= vetor1_w(5).nm_coluna_w,
		ds_resultado6	= vetor1_w(6).nm_coluna_w,
		ds_resultado7	= vetor1_w(7).nm_coluna_w,
		ds_resultado8	= vetor1_w(8).nm_coluna_w,
		ds_resultado9	= vetor1_w(9).nm_coluna_w,
		ds_resultado10	= vetor1_w(10).nm_coluna_w,
		ds_resultado11	= vetor1_w(11).nm_coluna_w,
		ds_resultado12	= vetor1_w(12).nm_coluna_w,
		ds_resultado13	= vetor1_w(13).nm_coluna_w,
		ds_resultado14	= vetor1_w(14).nm_coluna_w,
		ds_resultado15	= vetor1_w(15).nm_coluna_w,
		ds_resultado16	= vetor1_w(16).nm_coluna_w,
		ds_resultado17	= vetor1_w(17).nm_coluna_w,
		ds_resultado18	= vetor1_w(18).nm_coluna_w,
		ds_resultado19	= vetor1_w(19).nm_coluna_w,
		ds_resultado20	= vetor1_w(20).nm_coluna_w,
		ds_resultado21	= vetor1_w(21).nm_coluna_w,
		ds_resultado22	= vetor1_w(22).nm_coluna_w,
		ds_resultado23	= vetor1_w(23).nm_coluna_w,
		ds_resultado24	= vetor1_w(24).nm_coluna_w,
		ds_resultado25	= vetor1_w(25).nm_coluna_w,
		ds_resultado26	= vetor1_w(26).nm_coluna_w,
		ds_resultado27	= vetor1_w(27).nm_coluna_w,
		ds_resultado28	= vetor1_w(28).nm_coluna_w,
		ds_resultado29	= vetor1_w(29).nm_coluna_w,
		ds_resultado30	= vetor1_w(30).nm_coluna_w,
		ds_resultado31	= vetor1_w(31).nm_coluna_w,
		ds_resultado32	= vetor1_w(32).nm_coluna_w,
		ds_resultado33	= vetor1_w(33).nm_coluna_w,
		ds_resultado34	= vetor1_w(34).nm_coluna_w,
		ds_resultado35	= vetor1_w(35).nm_coluna_w,
		ds_resultado36	= vetor1_w(36).nm_coluna_w,
		ds_resultado37	= vetor1_w(37).nm_coluna_w,
		ds_resultado38	= vetor1_w(38).nm_coluna_w,
		ds_resultado39	= vetor1_w(39).nm_coluna_w,
		ds_resultado40	= vetor1_w(40).nm_coluna_w,
		ds_resultado41	= vetor1_w(41).nm_coluna_w,
		ds_resultado42	= vetor1_w(42).nm_coluna_w,
		ds_resultado43	= vetor1_w(43).nm_coluna_w,
		ds_resultado44	= vetor1_w(44).nm_coluna_w,
		ds_resultado45	= vetor1_w(45).nm_coluna_w,
		ds_resultado46	= vetor1_w(46).nm_coluna_w,
		ds_resultado47	= vetor1_w(47).nm_coluna_w,
		ds_resultado48	= vetor1_w(48).nm_coluna_w,
		ds_resultado49	= vetor1_w(49).nm_coluna_w,
		ds_resultado50	= vetor1_w(50).nm_coluna_w
	WHERE	NR_SEQUENCIA = nr_seq_nova_w;
	end;
end loop;

commit;
end;
/