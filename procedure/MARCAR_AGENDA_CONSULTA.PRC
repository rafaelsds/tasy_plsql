create or replace
procedure Marcar_Agenda_Consulta
		(nr_seq_agenda_p	number,
		nm_usuario_p		varchar2) is

nr_sequencia_w		number(10,0);
hr_inicio_w		date;
cd_convenio_w		number(05,0);
cd_pessoa_fisica_w	varchar2(10);
nr_minuto_duracao_w	number(10);	
nr_seq_age_cons_w	number(10);	
cd_agenda_cons_w	number(10);
cd_medico_w		varchar2(10);
cd_estabelecimento_w	number(10,0);
dt_agenda_w		date;

begin

select	a.cd_pessoa_fisica,
	a.hr_inicio,
	a.cd_convenio,
	a.nr_minuto_duracao,
	a.cd_medico,
	nvl(a.nr_seq_age_cons,0),
	b.cd_estabelecimento
into	cd_pessoa_fisica_w,
	hr_inicio_w,
	cd_convenio_w,
	nr_minuto_duracao_w,
	cd_medico_w,
	nr_seq_age_cons_w,
	cd_estabelecimento_w
from	agenda b, agenda_paciente a
where	a.cd_agenda	= b.cd_agenda
and	a.nr_sequencia	= nr_seq_agenda_p;

if	(nr_seq_age_cons_w = 0) then
	begin

	select	nvl(max(a.cd_agenda),0)
	into	cd_agenda_cons_w
	from   	pessoa_fisica b,
	       	medico c,
       		agenda a
	where  a.cd_pessoa_fisica   = b.cd_pessoa_fisica
	  and  a.cd_pessoa_fisica   = c.cd_pessoa_fisica(+)
	  and  a.cd_pessoa_fisica   = cd_medico_w
	  and  a.cd_estabelecimento = cd_estabelecimento_w
	  and  nvl(a.ie_situacao,'A') = 'A'
	  and  cd_tipo_agenda       = 3;

	if	(cd_agenda_cons_w > 0) then
		begin

		select	nvl(max(nr_sequencia),0),
			max(dt_agenda)
		into	nr_sequencia_w,
			dt_agenda_w
		from	agenda_consulta
		where	cd_agenda	= cd_agenda_cons_w
		and	trunc(dt_agenda, 'mi')	= trunc(hr_inicio_w, 'mi')
		and	ie_status_agenda <> 'L';	

		select	agenda_consulta_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert 	into agenda_consulta
			(NR_SEQUENCIA           ,
			CD_AGENDA              ,
			DT_AGENDA              ,
			NR_MINUTO_DURACAO      ,
			IE_STATUS_AGENDA       ,
			IE_CLASSIF_AGENDA      ,
			DT_ATUALIZACAO         ,	
			NM_USUARIO             ,
			CD_CONVENIO            ,
			CD_PESSOA_FISICA       ,
			NM_PESSOA_CONTATO      ,
			DS_OBSERVACAO          ,
			IE_STATUS_PACIENTE     ,
			NR_SEQ_CONSULTA        ,
			NM_PACIENTE            ,
			NR_ATENDIMENTO         ,
			DT_CONFIRMACAO         ,
			DS_CONFIRMACAO         ,
			NR_TELEFONE            ,
			QT_IDADE_PAC           ,
			NR_SEQ_PLANO           ,
			NR_SEQ_CLASSIF_MED     ,
			NM_USUARIO_ORIGEM      ,
			IE_NECESSITA_CONTATO,
			cd_turno   )
		values	(nr_sequencia_w,
			cd_agenda_cons_w,
			decode(nr_sequencia_w, 0, hr_inicio_w, dt_agenda_w + 1/86400),
			nr_minuto_duracao_w,
			'N', 'C',
			sysdate, nm_usuario_p,
			cd_convenio_w,
			cd_pessoa_fisica_w,
			null, null, 'N',
			null, null, null, null,
			null, null, null, null,
			null, null, 'N', 0);

		update	agenda_paciente
		set	nr_seq_age_cons = nr_sequencia_w
		where	nr_sequencia	= nr_seq_agenda_p;
		end;
	end if;	
	
	end;
end if;	
commit;

end Marcar_Agenda_Consulta;
/