create or replace
procedure GERAR_RETORNO_LOTE_PROT	(                                             
					nr_seq_lote_protocolo_p	number,                                            
					nr_seq_retorno_p		number,                                                  
					nr_seq_protocolo_p	number,                                                 
					nr_seq_prot_adic_p	number,                                                 
					ie_liberar_repasse_p	varchar2,                                             
					ie_analisada_p		varchar2,                                                  
					ie_restringe_data_p	varchar2,                                              
					nr_convenio_p		number,                                                     
					nm_usuario_p		varchar2,                                                    
					ie_gerar_com_retorno_p	varchar2                                            
					) is                                                                       
                                                                                
ds_mensgem_w		varchar2(4000);                                                   
nr_seq_protocolo_w	number(15);                                                  
                                                                                
cursor c01 is                                                                   
select	nr_seq_protocolo                                                         
from	protocolo_convenio                                                         
where	nr_seq_lote_protocolo	= nr_seq_lote_protocolo_p;                          
                                                                                
begin                                                                           

open c01;                                                                       
loop                                                                            
fetch c01 into                                                                  
	nr_seq_protocolo_w;                                                            
exit when c01%notfound;                                                         
                                                                                
	gerar_retorno_protocolo	(                                                      
				nr_seq_retorno_p,                                                           
				nr_seq_protocolo_w,                                                         
				nr_seq_prot_adic_p,                                                         
				ie_liberar_repasse_p,                                                       
				ie_analisada_p,                                                             
				ie_restringe_data_p,                                                        
				nr_convenio_p,                                                              
				nm_usuario_p,                                                               
				ie_gerar_com_retorno_p,                                                     
				ds_mensgem_w                                                                
				);                                                                          
                                                                                
end loop;                                                                       
close c01;                                                                      

commit;                                                                         
                                                                                
end	GERAR_RETORNO_LOTE_PROT;                                                    
/