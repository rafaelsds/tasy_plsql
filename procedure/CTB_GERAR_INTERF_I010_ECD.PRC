create or replace
procedure ctb_gerar_interf_I010_ecd(
			nr_seq_controle_p		number,
			nm_usuario_p			Varchar2,
			cd_estabelecimento_p		number,
			dt_inicio_p			date,
			dt_fim_p			date,
			cd_empresa_p			number,
			qt_linha_p		in out	number,
			nr_sequencia_p		in out	number) is 


ie_ind_escriturizacao_w		varchar2(1);
ds_arquivo_w			varchar2(4000);
ds_compl_arquivo_w		varchar2(4000);
ds_linha_w			varchar2(8000);
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
cd_versao_w			ctb_regra_sped.cd_versao%type;

cursor c01 is
select 	a.ie_forma_escrituracao,
	nvl(a.cd_versao, '9.0')
from	ctb_regra_sped a,
	ctb_sped_controle b
where	a.nr_sequencia	= b.nr_seq_regra_sped
and	b.nr_sequencia	= nr_seq_controle_p;
begin


open C01;
loop
fetch C01 into	
	ie_ind_escriturizacao_w,
	cd_versao_w;
exit when C01%notfound;
	begin	
	ds_linha_w	:= substr('|' || 'I010' || '|' || ie_ind_escriturizacao_w || '|' || rpad(cd_versao_w,4,'0') || '|',1,8000);
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;	
	insert into ctb_sped_registro(
			nr_sequencia,                   
			ds_arquivo,                     
			dt_atualizacao,                 
			nm_usuario,                     
			dt_atualizacao_nrec,            
			nm_usuario_nrec,                
			nr_seq_controle_sped,           
			ds_arquivo_compl,               
			cd_registro,
			nr_linha)
		values(
			nr_seq_registro_w,
			ds_arquivo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_controle_p,
			ds_compl_arquivo_w,
			'I010',
			nr_linha_w);
	end;
end loop;
close C01;

commit;
qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;
end ctb_gerar_interf_I010_ecd;
/
