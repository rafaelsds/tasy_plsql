create or replace
procedure pcs_alterar_data_prevista	(nr_seq_ordem_p	number,
									 nr_seq_atividade_p	number,
									 dt_prevista_p		date,
									 nm_usuario_p		Varchar2) is 

begin

if (dt_prevista_p is not null) then
	update	man_ordem_ativ_prev
	set 	dt_prevista = dt_prevista_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
	where	nr_seq_ordem_serv = nr_seq_ordem_p
	and		nr_sequencia = nr_seq_atividade_p;
end if;	
commit;

end pcs_alterar_data_prevista;
/