create or replace
procedure	atualiza_revisao (	nr_sequencia_p	Number,
								nm_usuario_p	Varchar2) is 
begin

update	com_faq
set		ie_revisado = 'S',
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
where	nr_sequencia = nr_sequencia_p;

commit;

end atualiza_revisao;
/