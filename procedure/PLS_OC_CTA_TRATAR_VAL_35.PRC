create or replace
procedure pls_oc_cta_tratar_val_35(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
				nr_id_transacao_p		pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
				nm_usuario_p		usuario.nm_usuario%type) is 
			
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o das datas da conta.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

i			pls_integer;
dados_tb_sel_w		pls_tipos_ocor_pck.dados_table_selecao_ocor;
ie_gera_ocorrencia_w	varchar2(1);
dt_liberacao_w		pls_guia_plano.dt_liberacao%type;
count_w			integer;
nr_dia_diferenca_w	pls_oc_cta_val_data_conta.nr_dia_diferenca%type;

-- Informa��es da valida��o de n�o-utiliza��o de item autorizado
cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.ie_data_conta ie_data_conta,
		nvl(a.nr_dia_diferenca,0) nr_dia_diferenca
	from	pls_oc_cta_val_data_conta a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;
	
cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type) is
	select 	trunc(b.dt_atendimento,'dd') dt_atendimento,
		trunc(b.dt_atendimento_imp,'dd') dt_atendimento_imp,
		b.dt_alta_imp,
		b.dt_alta,
		b.dt_entrada_imp,
		b.dt_entrada,
		sysdate dt_atual,
		trunc(sysdate,'dd') dt_atual_trunc,
		trunc(c.dt_protocolo_imp,'dd') dt_protocolo_imp,
		trunc(c.dt_protocolo,'dd') dt_protocolo,
		trunc(c.dt_recebimento,'dd') dt_recebimento,
		fim_dia(c.dt_mes_competencia) dt_competencia,
		b.nr_sequencia nr_seq_conta,
		b.nr_seq_guia,
		b.dt_atendimento dt_atendimento_referencia,
		b.dt_atendimento_imp dt_atendimento_referencia_imp
	from 	pls_conta_ocor_v b,
		pls_protocolo_conta_v c
	where 	c.nr_sequencia = b.nr_seq_protocolo  
	and	exists (select	1
			from	pls_selecao_ocor_cta a
			where	a.nr_id_transacao = nr_id_transacao_pc
			and	a.ie_valido = 'S'
			and	a.nr_seq_conta = b.nr_sequencia);
	
begin

-- Deve ter a informa��o da regra para que seja aplicada a valida��o.
if	(dados_regra_p.nr_sequencia is not null)  then
	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_tipos_ocor_pck.atualiza_campo_auxiliar('V', nr_id_transacao_p, null, dados_regra_p);
	pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);
	i := 0;
	
	for	r_C01_w in C01( dados_regra_p.nr_sequencia) loop
		
		nr_dia_diferenca_w := r_C01_w.nr_dia_diferenca;		
		if	(nr_dia_diferenca_w < 0) then
			nr_dia_diferenca_w := 0;			
		end if;
		
		if	( r_C01_w.ie_data_conta is not null) then
		
			for	r_C02_w in C02( nr_id_transacao_p)loop
			

				ie_gera_ocorrencia_w := 'N';
				
				case (r_C01_w.ie_data_conta)
					--Data da alta menor que a data da entrada(Se for maior ou igual, n�o gera ocorr�ncia)
					when 1 then
						if 	( dados_regra_p.ie_evento = 'IMP') then
							if	( r_C02_w.dt_alta_imp < r_C02_w.dt_entrada_imp) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							if	( r_C02_w.dt_alta < r_C02_w.dt_entrada) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					--Data da alta maior que a data atual(Se for menor ou igual, n�o gera ocorr�ncia)
					when 2 then
						if 	( dados_regra_p.ie_evento = 'IMP') then
							if	( r_C02_w.dt_alta_imp > r_C02_w.dt_atual) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							if	( r_C02_w.dt_alta > r_C02_w.dt_atual) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					--Data da emiss�o da conta maior que a data atual(Se for menor ou igual, n�o gera ocorr�ncia)
					when 3 then
						if 	( dados_regra_p.ie_evento = 'IMP') then
							if	( r_C02_w.dt_atendimento_imp > r_C02_w.dt_atual_trunc) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							if	( r_C02_w.dt_atendimento > r_C02_w.dt_atual_trunc) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					--Data da entrada maior que a data atual(Se for menor ou igual, n�o gera ocorr�ncia)
					when 4 then
						if 	( dados_regra_p.ie_evento = 'IMP') then
							if	( r_C02_w.dt_entrada_imp > r_C02_w.dt_atual) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							if	( r_C02_w.dt_entrada > r_C02_w.dt_atual) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					--Data da emiss�o da conta maior que a data do protocolo(Se for maior ou igual, n�o gera ocorr�ncia)
					when 5 then
						if 	( dados_regra_p.ie_evento = 'IMP') then
							if	( r_C02_w.dt_atendimento_imp > fim_dia(r_C02_w.dt_protocolo_imp)) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							if	( r_C02_w.dt_atendimento > fim_dia(r_C02_w.dt_protocolo)) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					--Data de refer�ncia superior a data de recebimento do protocolo (Se for maior ou igual, n�o gera ocorr�ncia)
					when 6 then
						if 	( dados_regra_p.ie_evento = 'IMP') then
							if	( r_C02_w.dt_atendimento_imp > r_C02_w.dt_recebimento) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							if	( r_C02_w.dt_atendimento > r_C02_w.dt_recebimento) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					-- Diferen�a entre data de emiss�o e data atual maior que o permitido
					when 7 then
						if	((r_C02_w.dt_atual_trunc - r_C02_w.dt_atendimento) > nr_dia_diferenca_w) then
							ie_gera_ocorrencia_w := 'S';
						end if;
					-- Data de atendimento da conta superior a data de competencia do protocolo
					when 8 then
						if 	( dados_regra_p.ie_evento = 'IMP') then
							if	( r_C02_w.dt_atendimento_imp > r_C02_w.dt_competencia) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							if	( r_C02_w.dt_atendimento > r_C02_w.dt_competencia) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					-- Data de refer�ncia anterior a data de autoriza��o
					when 9 then
						if (r_C02_w.nr_seq_guia is not null) then
							
							select 	count(1)
							into	count_w
							from	pls_guia_plano
							where	nr_seq_guia_principal = r_C02_w.nr_seq_guia;
							
							if (count_w > 0) then
								select 	dt_liberacao
								into	dt_liberacao_w
								from	pls_guia_plano
								where	nr_seq_guia_principal = r_C02_w.nr_seq_guia;
								
								if 	(dt_liberacao_w is not null) then
									if 	( dados_regra_p.ie_evento = 'IMP') then
										if	( r_C02_w.dt_atendimento_referencia_imp < dt_liberacao_w) then
											ie_gera_ocorrencia_w := 'S';
										end if;
									else
										if	( r_C02_w.dt_atendimento_referencia < dt_liberacao_w) then
											ie_gera_ocorrencia_w := 'S';
										end if;
									end if;
								end if;
							end if;
						end if;
					--Diferen�a de dias entre data de alta e data atual maior que a permitida
					when 10 then
						if 	( dados_regra_p.ie_evento = 'IMP') then
							if	( r_C02_w.dt_alta_imp > (r_C02_w.dt_atual_trunc + nr_dia_diferenca_w)) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							if	( r_C02_w.dt_alta > (r_C02_w.dt_atual_trunc + nr_dia_diferenca_w)) then
								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					else 
						null;
				end case;
				
				if 	( ie_gera_ocorrencia_w = 'S') then
					--Passa nr_seq_conta ao inv�z do nr_seq_selecao, pois ser� feito valida��o a n�vel de conta
					dados_tb_sel_w.nr_seq_selecao(i) := r_C02_w.nr_seq_conta;
					dados_tb_sel_w.ds_observacao(i) := null;
					dados_tb_sel_w.ie_valido(i) := 'S';
					
					if	( i >= pls_util_pck.qt_registro_transacao_w) then
					
						pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_sel_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
												'SEQ_CONTA', dados_tb_sel_w.ds_observacao, dados_tb_sel_w.ie_valido, nm_usuario_p, nr_id_transacao_p);
						pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);
						i := 0;
					else
						i := i + 1;
					end if;
				end if;
			end loop; -- C02		
		end if;
	end loop; -- C01
	
	pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_sel_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
							'SEQ_CONTA', dados_tb_sel_w.ds_observacao, dados_tb_sel_w.ie_valido, nm_usuario_p, nr_id_transacao_p);
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;
end pls_oc_cta_tratar_val_35;
/
