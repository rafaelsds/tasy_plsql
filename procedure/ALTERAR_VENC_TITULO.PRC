create or replace
procedure ALTERAR_VENC_TITULO (nr_titulo_p	in	number,
				dt_vencimento_p	in	date,
				ie_tipo_titulo_p	in	varchar2,
				dt_referencia_p	in	date,
				cd_conta_financ_p	in	number,
				ie_classif_fluxo_p	in	varchar2,
				cd_estabelecimento_p	in	number,
				ie_periodo_p		in	varchar2,
				vl_fluxo_p		in	number,
				nm_usuario_p		in	varchar2,
				ds_observacao_p	in	varchar2) is

/*
ie_tipo_titulo_p:
'CR' : TITULO_RECEBER
'CP' : TITULO_PAGAR
'D' : FLUXO_CAIXA (Digitado)
*/

dt_vencimento_atual_w		date;

begin
if (ie_tipo_titulo_p = 'CR') then
	update	titulo_receber
	set	dt_pagamento_previsto = dt_vencimento_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_titulo = nr_titulo_p;
elsif (ie_tipo_titulo_p = 'CP') then

	select	max(dt_vencimento_atual)
	into	dt_vencimento_atual_w
	from	titulo_pagar
	where	nr_titulo = nr_titulo_p;

	update	titulo_pagar
	set	dt_vencimento_atual = dt_vencimento_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_titulo = nr_titulo_p;

	insert into titulo_pagar_alt_venc (
				NR_TITULO      ,
				NR_SEQUENCIA   ,
				DT_ANTERIOR    ,
				DT_VENCIMENTO  ,
				DT_ATUALIZACAO ,
				NM_USUARIO     ,
				DT_ALTERACAO   ,
				DS_OBSERVACAO)
		(select	nr_titulo_p, 
				nvl(max(nr_sequencia),0)+1, 
				dt_vencimento_atual_w,
				dt_vencimento_p,
				sysdate,
				nm_usuario_p,
				trunc(sysdate,'dd'),
				substr(ds_observacao_p,1,255)
			from	titulo_pagar_alt_venc 
			where	nr_titulo = nr_titulo_p);

elsif (ie_tipo_titulo_p = 'D') then
	update	fluxo_caixa
	set	dt_referencia 	= dt_vencimento_p,
		nm_usuario 		= nm_usuario_p,
		dt_atualizacao 	= sysdate,
		vl_fluxo 		= vl_fluxo_p
	where	dt_referencia 	= dt_referencia_p
	and	cd_conta_financ 	= cd_conta_financ_p
	and	ie_classif_fluxo 	= ie_classif_fluxo_p
	and	cd_estabelecimento 	= cd_estabelecimento_p
	and	ie_periodo 		= ie_periodo_p;
end if;

commit;

end ALTERAR_VENC_TITULO;
/
