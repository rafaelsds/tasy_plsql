create or replace
procedure GERAR_RELATORIO_DINAMICO_QB
  (
	nr_seq_filter_p filter_query_dar.nr_sequencia%type,
	nm_usuario_p varchar2,
	cd_relatorio_p out relatorio.cd_relatorio%type
	)
is
	ds_titulo_w relatorio.ds_titulo%type;
	nr_seq_relatorio_w relatorio.nr_sequencia%type;
	cd_relatorio_w relatorio.cd_relatorio%type;
	ds_sql_w dar_tables_control.ds_sql%type;
	contador_w integer := 0;

Cursor C01 is
	select 	  a.*
	from 	  dar_tab_control_fields a, 
		  dar_tables_control b
	where 	  a.nr_seq_table_control = b.nr_sequencia
	and       b.nr_seq_sql = nr_seq_filter_p;

begin

	select nm_query
	into ds_titulo_w
	from filter_query_dar
	where nr_sequencia = nr_seq_filter_p;

	select ds_sql
	into ds_sql_w
	from dar_tables_control
	where nr_seq_sql = nr_seq_filter_p;

	select  nvl(max(cd_relatorio),0) +1
	into cd_relatorio_w
	from relatorio
	where cd_classif_relat = 'CAQB';

	cd_relatorio_p := cd_relatorio_w;

	insert
	into relatorio
	(
	  CD_CLASSIF_RELAT,
	  CD_RELATORIO,
	  DS_TITULO,
	  IE_BORDA_DIR,
	  IE_BORDA_ESQ,
	  IE_BORDA_INF,
	  IE_BORDA_SUP,
	  IE_ETIQUETA,
	  IE_FILTRO_EXCEL,
	  IE_FILTRO_HTML,
	  IE_FILTRO_TEXTO,
	  IE_FILTRO_WORD,
	  IE_IMPRIME_VAZIO,
	  IE_ORIENTACAO,
	  IE_TIPO_PAPEL,
	  NM_USUARIO,
	  NR_SEQUENCIA,
	  QT_MARGEM_DIR,
	  QT_MARGEM_ESQ,
	  QT_MARGEM_INF,
	  QT_MARGEM_SUP,
	  DT_ATUALIZACAO,
	  DS_COR_FUNDO
	)
	values
	(
	  'CAQB',
	  cd_relatorio_w,
	  ds_titulo_w,
	  'S', 'S', 'S', 'S',
	  'N', 'N',
	  'R',
	  'S', 'S', 'S',
	  'R',
	  'A4',
	  nm_usuario_p,
	  relatorio_seq.nextval,
	  10, 10,
	  20, 
	  10,
	  sysdate,
	  'clWhite'
	)
	returning nr_sequencia into nr_seq_relatorio_w;

for r_c01_w in C01 loop
	begin
	contador_w := contador_w + 1;
	insert
	into relatorio_dinamico
	(
	  NR_SEQUENCIA,
	  DT_ATUALIZACAO,
	  NM_USUARIO,
	  DT_ATUALIZACAO_NREC,
	  NM_USUARIO_NREC,
	  CD_RELATORIO,
	  CD_TIPO,
	  NR_ALTURA,
	  NR_COMPRIMENTO,
	  NR_EIXO_Y,
	  NR_EIXO_X,
	  NR_TIPO_BANDA,
	  NR_SEQ_RELATORIO,
	  DS_LABEL,
	  DS_COR_FUNDO_LABEL,
	  DS_COR_FUNDO_DESCRICAO,
	  NM_LABEL,
	  IE_AJUSTAR_TAMANHO,
	  NR_SEQ_APRES,
	  IE_TIPO_FONTE,
	  NR_TAM_FONTE,
	  IE_ITALICO,
	  IE_NEGRITO,
	  IE_IMAGEM_CAMPO,
	  QT_ROTACAO,
	  QT_TAMANHO_BANDA,
	  QT_ALTURA_BANDA,
	  IE_IMAGEM_CAMPO2,
	  NR_SEQ_DOC_RELAT_DINAMICO,
	  DS_SQL
	)
	values
	(
	  relatorio_dinamico_seq.nextval,
	  sysdate,
	  nm_usuario_p,
	  sysdate,
	  nm_usuario_p,
	  cd_relatorio_w,
	  '2',
	  '20',
	  '40',
	  '1',
	  '1',
	  '2',
	  nr_seq_relatorio_w,
	  obter_desc_expressao(r_c01_w.ds_expressao_campo),
	  null,
	  null,
	  r_c01_w.nr_identificador,
	  'S',
	  contador_w,
	  null,8,null,null, null,
	  '0',
	  '500',
	  '200',
	  null, null,
	  ds_sql_w
	);
	end;
end loop;
commit;

end GERAR_RELATORIO_DINAMICO_QB;
/