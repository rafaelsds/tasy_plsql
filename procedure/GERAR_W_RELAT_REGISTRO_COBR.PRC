create or replace
procedure gerar_w_relat_registro_cobr
			(	cd_estabelecimento_p		number,
				nm_usuario_p			varchar2,
				cd_setor_atendimento_p		number,
				dt_competencia_p		date,
				ie_quebra_1_p			varchar2,
				ie_quebra_2_p			varchar2,
				ie_regulamentacao_p		varchar2,
				ie_tipo_p			varchar2,
				nr_seq_cobrador_p		number,
				nr_seq_historico_p		number,
				nr_seq_grupo_p			number,
				nr_seq_plano_p			number,
				qt_meses_p			number) is
				
ds_resultado_w			varchar2(255);
nr_seq_resultado_w		number(10);
cd_resultado_w			varchar2(10);

qt_parc_aberta_w		number(10);
vl_cobranca_w			number(15,2);
nm_devedor_w			varchar2(80);
cd_usuario_plano_w		varchar2(40);
dt_registro_w			date;
qt_contrato_w			number(10);
vl_pago_w			number(15,2);
vl_emitido_w			number(15,2);
vl_saldo_w			number(15,2);
ds_resultado_2_w		varchar2(255);

ds_resultado_3_w		varchar2(255);
nr_seq_resultado_3_w		number(10);
cd_resultado_3_w		varchar2(10);

qt_existente_w			number(10);

ie_quebra_w			varchar2(1);
ie_quebra_2_w			varchar2(1);
ie_quebra_1_w			varchar2(1);
nm_quebra_w			varchar2(255);
ds_tit_quebra_w			varchar2(255);
cd_quebra_w			varchar2(20);
ds_tit_quebra_1_w		varchar2(255);
cd_quebra_1_w			varchar2(20);
ds_quebra_1_w			varchar2(255);
ds_tit_quebra_2_w		varchar2(255);
cd_quebra_2_w			varchar2(20);
ds_quebra_2_w			varchar2(255);
nr_seq_registro_w		number(10);
nr_seq_cobrador_w		number(10);
ds_quebra_w			varchar2(255);

qt_quebra_w			number(10);

nm_operador_w			varchar2(255);

cursor C01 is
	select	c.nr_sequencia,
		e.nr_seq_cobrador,
		d.nr_titulo qt_parc_aberta,
		substr(obter_nome_pf_pj(d.cd_pessoa_fisica,d.cd_cgc),1,80) nm_devedor,
		d.vl_titulo vl_cobranca,
		substr(pls_obter_carteira_segurado(pls_obter_segurado_pagador(b.nr_sequencia)),1,255) cd_usuario,
		c.dt_registro,
		b.nr_sequencia nr_sequencia,
		d.vl_titulo - d.vl_saldo_titulo vl_pago
	from	titulo_receber d,
		cobranca e,
		registro_cobr_item a,
		pls_contrato_pagador b,
		registro_cobranca c
	where	((b.cd_pessoa_fisica = c.cd_pessoa_fisica) or (b.cd_cgc = d.cd_cgc))
	and	e.nr_titulo = d.nr_titulo
	and	a.nr_seq_registro = c.nr_sequencia
	and	a.nr_seq_cobranca = e.nr_sequencia
	and	exists (select	1
			from	registro_cobranca x
			where 	((x.cd_pessoa_fisica = d.cd_pessoa_fisica) or (x.cd_cgc = d.cd_cgc))
			and	x.dt_registro = (select	max(y.dt_registro) 
						from 	registro_cobranca y 
						where	(y.cd_pessoa_fisica = x.cd_pessoa_fisica or y.cd_cgc = x.cd_cgc)
						)
			)
	and	ie_tipo_p = 'A'
	and	(c.nr_seq_historico = nr_seq_historico_p or nr_seq_historico_p is null)
	and	(trunc(d.dt_vencimento,'mm') = trunc(dt_competencia_p,'month') or dt_competencia_p is null)
	and	(e.nr_seq_cobrador = nr_seq_cobrador_p or nr_seq_cobrador_p is null)
	and	(exists (select 1
			from	grupo_cobranca_membro x 
			where	x.nr_seq_cobrador = e.nr_seq_cobrador
			and x.nr_seq_grupo = nr_seq_grupo_p)  
		or nr_seq_grupo_p is null)
	and	(exists (select 1
			from	pls_segurado x,
				pls_plano y
			where 	b.nr_sequencia = x.nr_seq_pagador
			and 	y.nr_sequencia = x.nr_seq_plano 
			and 	y.ie_regulamentacao = ie_regulamentacao_p)
		or ie_regulamentacao_p = '0');
		
cursor C00 is
	select	a.nm_guerra ds_resultado,
		a.nr_sequencia nr_seq_resultado,
		null cd_resultado
	from	cobrador  a
	where	a.ie_situacao = 'A'
	and	a.nr_sequencia		= decode(nr_seq_cobrador_p,null,a.nr_sequencia,nr_seq_cobrador_p)
	and	a.cd_setor_atendimento	= decode(cd_setor_atendimento_p,null,a.cd_setor_atendimento,cd_setor_atendimento_p)
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_quebra_1_p = 'C'
	union all
	select	substr(ds_valor_dominio,1,60) ds_resultado,
		null nr_seq_resultado,
		vl_dominio cd_resultado
	from	valor_dominio
	where	cd_dominio = 2157
	and	vl_dominio = decode(ie_regulamentacao_p,null,vl_dominio,ie_regulamentacao_p)
	and	ie_quebra_1_p = 'R'
	union all
	select	a.ds_plano ds_resultado,
		a.nr_sequencia nr_seq_resultado,
		null cd_resultado
	from	pls_plano a
	where	a.ie_situacao = 'A'
	and	a.nr_sequencia = decode(nr_seq_plano_p,null,a.nr_sequencia,nr_seq_plano_p)
	and	a.ie_tipo_operacao	= 'B'
	and	ie_quebra_1_p		= 'P'
	union all
	select	a.ds_grupo ds_resultado,
		a.nr_sequencia nr_seq_resultado,
		null cd_resultado
	from	grupo_cobranca a
	where	a.ie_situacao	= 'A'
	and	ie_quebra_1_p	= 'E'
	and	a.nr_sequencia = decode(nr_seq_grupo_p,null,a.nr_sequencia,nr_seq_grupo_p)
	union all
	select	a.ds_historico ds_resultado,
		a.nr_sequencia nr_seq_resultado,
		null cd_resultado
	from	tipo_hist_cob a
	where	a.ie_situacao = 'A'
	and	ie_quebra_1_p = 'H'
	and	a.nr_sequencia = decode(nr_seq_historico_p,null,a.nr_sequencia,nr_seq_historico_p)
	order by ds_resultado;
		
Cursor C03 is
	select	a.qt_cobranca,
		decode(a.ds_resultado,'R',WHEB_MENSAGEM_PCK.get_texto(818774) /*'Contratos antigos'*/,'P',WHEB_MENSAGEM_PCK.get_texto(818775) /*'Contratos novos'*/,a.ds_resultado) ds_resultado,
		a.vl_emitido,
		a.qt_contrato,
		(a.vl_emitido - a.vl_saldo) vl_saldo,
		substr(obter_dados_registro_cobranca(a.nr_seq_registro,'NO'),1,255) nm_operador
	from	registro_cobranca_relat_v a
	where	(a.nr_seq_historico = nvl(nr_seq_resultado_w,nr_seq_historico_p) or (a.nr_seq_historico is null))
	and	ie_tipo_p = 'S'
	and	trunc(a.dt_vencimento,'mm') = decode(dt_competencia_p,'0',trunc(a.dt_vencimento,'mm'),dt_competencia_p)
	and	a.nr_seq_cobrador = decode(nr_seq_cobrador_p,'0',a.nr_seq_cobrador,nr_seq_cobrador_p)
	and	((((a.nr_seq_grupo = nr_seq_grupo_p) or nr_seq_grupo_p is null)) or (a.nr_seq_grupo is null))
	and	((a.nr_seq_plano = decode(nr_seq_plano_p,null,a.nr_seq_plano,nr_seq_plano_p)) or (a.nr_seq_plano is null))
	and	((a.ds_resultado = ie_regulamentacao_p) or (ie_regulamentacao_p = '0'))
	and	ie_quebra_2_p = a.ie_modo
	and	ie_quebra_1_p = 'H'
	and	((a.qt_cobranca = qt_meses_p) or (qt_meses_p is null))
	union all
	select	a.qt_cobranca,
		decode(a.ds_resultado,'R',WHEB_MENSAGEM_PCK.get_texto(818774) /*'Contratos antigos'*/,'P',WHEB_MENSAGEM_PCK.get_texto(818775) /*'Contratos novos'*/,a.ds_resultado) ds_resultado,
		a.vl_emitido,
		a.qt_contrato,
		(a.vl_emitido - a.vl_saldo) vl_saldo,
		substr(obter_dados_registro_cobranca(a.nr_seq_registro,'NO'),1,255) nm_operador
	from	registro_cobranca_relat_v a
	where	(a.nr_seq_historico = decode(nr_seq_historico_p,'0',a.nr_seq_historico,nr_seq_historico_p) or (a.nr_seq_historico is null))
	and	ie_tipo_p = 'S'
	and	trunc(a.dt_vencimento,'mm') = decode(dt_competencia_p,null,trunc(a.dt_vencimento,'mm'),dt_competencia_p)
	and	a.nr_seq_cobrador = decode(nr_seq_cobrador_p,null,a.nr_seq_cobrador,nr_seq_cobrador_p)
	and	((((a.nr_seq_grupo = nvl(nr_seq_resultado_w,nr_seq_grupo_p)) or nvl(nr_seq_resultado_w,nr_seq_grupo_p) is null)) or (a.nr_seq_grupo is null))
	and	((a.nr_seq_plano = decode(nr_seq_plano_p,null,a.nr_seq_plano,nr_seq_plano_p)) or (a.nr_seq_plano is null))
	and	((a.ds_resultado = ie_regulamentacao_p) or (ie_regulamentacao_p = '0'))
	and	ie_quebra_2_p = a.ie_modo
	and	ie_quebra_1_p = 'E'
	and	((a.qt_cobranca = qt_meses_p) or (qt_meses_p is null))
	union all
	select	a.qt_cobranca,
		decode(a.ds_resultado,'R',WHEB_MENSAGEM_PCK.get_texto(818774) /*'Contratos antigos'*/,'P',WHEB_MENSAGEM_PCK.get_texto(818775) /*'Contratos novos'*/,a.ds_resultado) ds_resultado,
		a.vl_emitido,
		a.qt_contrato,
		(a.vl_emitido - a.vl_saldo) vl_saldo,
		substr(obter_dados_registro_cobranca(a.nr_seq_registro,'NO'),1,255) nm_operador
	from	registro_cobranca_relat_v a
	where	(a.nr_seq_historico = decode(nr_seq_historico_p,null,a.nr_seq_historico,nr_seq_historico_p) or (a.nr_seq_historico is null))
	and	ie_tipo_p = 'S'
	and	trunc(a.dt_vencimento,'mm') = decode(dt_competencia_p,null,trunc(a.dt_vencimento,'mm'),dt_competencia_p)
	and	a.nr_seq_cobrador = decode(nr_seq_cobrador_p,null,a.nr_seq_cobrador,nr_seq_cobrador_p)
	and	((((a.nr_seq_grupo = nr_seq_grupo_p) or nr_seq_grupo_p is null)) or (a.nr_seq_grupo is null))
	and	((a.nr_seq_plano = nvl(nr_seq_resultado_w,nr_seq_plano_p)) or (a.nr_seq_plano is null))
	and	((a.ds_resultado = ie_regulamentacao_p) or (ie_regulamentacao_p ='0'))
	and	ie_quebra_2_p = a.ie_modo
	and	ie_quebra_1_p = 'P'
	and	((a.qt_cobranca = qt_meses_p) or (qt_meses_p is null))
	union all
	select	a.qt_cobranca,
		decode(a.ds_resultado,'R',WHEB_MENSAGEM_PCK.get_texto(818774) /*'Contratos antigos'*/,'P',WHEB_MENSAGEM_PCK.get_texto(818775) /*'Contratos novos'*/,a.ds_resultado) ds_resultado,
		a.vl_emitido,
		a.qt_contrato,
		(a.vl_emitido - a.vl_saldo) vl_saldo,
		substr(obter_dados_registro_cobranca(a.nr_seq_registro,'NO'),1,255) nm_operador
	from	registro_cobranca_relat_v a
	where	(a.nr_seq_historico = decode(nr_seq_historico_p,null,a.nr_seq_historico,nr_seq_historico_p) or (a.nr_seq_historico  is null))
	and	ie_tipo_p = 'S'
	and	trunc(a.dt_vencimento,'mm') = decode(dt_competencia_p,null,trunc(a.dt_vencimento,'mm'),dt_competencia_p)
	and	a.nr_seq_cobrador = decode(nr_seq_cobrador_p,null,a.nr_seq_cobrador,nr_seq_cobrador_p)
	and	((((a.nr_seq_grupo = nr_seq_grupo_p) or nr_seq_grupo_p is null)) or (a.nr_seq_grupo is null))
	and	((a.nr_seq_plano = decode(nr_seq_plano_p,null,a.nr_seq_plano,nr_seq_plano_p)) or (a.nr_seq_plano is null))
	and	((a.ds_resultado = nvl(cd_resultado_w,ie_regulamentacao_p)) or (nvl(cd_resultado_w,ie_regulamentacao_p) = '0'))
	and	ie_quebra_2_p = a.ie_modo
	and	ie_quebra_1_p = 'R'
	and	((a.qt_cobranca = qt_meses_p) or (qt_meses_p is null))
	union all
	select	a.qt_cobranca,
		decode(a.ds_resultado,'R',WHEB_MENSAGEM_PCK.get_texto(818774) /*'Contratos antigos'*/,'P',WHEB_MENSAGEM_PCK.get_texto(818775) /*'Contratos novos'*/,a.ds_resultado) ds_resultado,
		a.vl_emitido,
		a.qt_contrato,
		(a.vl_emitido - a.vl_saldo) vl_saldo,
		substr(obter_dados_registro_cobranca(a.nr_seq_registro,'NO'),1,255) nm_operador
	from	registro_cobranca_relat_v a
	where	(a.nr_seq_historico = decode(nr_seq_historico_p,null,a.nr_seq_historico,nr_seq_historico_p) or (a.nr_seq_historico is null))
	and	ie_tipo_p = 'S'
	and	trunc(a.dt_vencimento,'mm') = decode(dt_competencia_p,null,trunc(a.dt_vencimento,'mm'),dt_competencia_p)
	and	a.nr_seq_cobrador = nvl(nr_seq_resultado_w,nr_seq_cobrador_p)
	and	((((a.nr_seq_grupo = nr_seq_grupo_p) or nr_seq_grupo_p is null)) or (a.nr_seq_grupo is null))
	and	((a.nr_seq_plano = decode(nr_seq_plano_p,null,a.nr_seq_plano,nr_seq_plano_p)) or (a.nr_seq_plano is null))
	and	((a.ds_resultado = ie_regulamentacao_p) or (ie_regulamentacao_p = '0'))
	and	ie_quebra_2_p = a.ie_modo
	and	ie_quebra_1_p = 'C'
	and	((a.qt_cobranca = qt_meses_p) or (qt_meses_p is null));
		
begin
delete	from	w_relat_registro_cobr where	nm_usuario	= nm_usuario_p;

if	(ie_tipo_p = 'A') then
	open C01;
	loop
	fetch C01 into	
		nr_seq_registro_w,
		nr_seq_cobrador_w,
		qt_parc_aberta_w,
		nm_devedor_w,
		vl_cobranca_w,
		cd_usuario_plano_w,
		dt_registro_w,
		qt_contrato_w,
		vl_pago_w;
	exit when C01%notfound;
		begin
		qt_quebra_w	:= 1;
		while	(qt_quebra_w	<= 2) loop
			if	(qt_quebra_w = 1) then
				ie_quebra_w	:= ie_quebra_1_p;
			else
				ie_quebra_w	:= ie_quebra_2_p;
			end if;
			
			/* Equipe */
			if	(ie_quebra_w = 'E') then
				select	max(b.nr_sequencia),
					max(b.ds_grupo),
					WHEB_MENSAGEM_PCK.get_texto(818776) /*'Equipe de cobrança'*/
				into	cd_quebra_w,
					ds_quebra_w,
					ds_tit_quebra_w
				from	grupo_cobranca b,
					grupo_cobranca_membro c,
					cobrador a
				where	c.nr_seq_grupo	= b.nr_sequencia
				and	c.nr_seq_cobrador = a.nr_sequencia
				and	a.nr_sequencia	= nr_seq_cobrador_w;
			/*Cobrador*/
			elsif	(ie_quebra_w = 'C') then
				select	max(a.nr_sequencia),
					max(obter_nome_pf(a.cd_pessoa_fisica)),
					WHEB_MENSAGEM_PCK.get_texto(818777) /*'Cobrador'*/
				into	cd_quebra_w,
					ds_quebra_w,
					ds_tit_quebra_w
				from	cobrador a
				where	a.nr_sequencia	= nr_seq_cobrador_w;
			elsif	(ie_quebra_w = 'H') then
				select	max(a.nr_sequencia),
					max(a.ds_historico),
					WHEB_MENSAGEM_PCK.get_texto(818778) /*'Histórico'*/
				into	cd_quebra_w,
					ds_quebra_w,
					ds_tit_quebra_w
				from	tipo_hist_cob a,
					registro_cobranca b
				where	a.nr_sequencia	= b.nr_seq_historico
				and	b.nr_sequencia	= nr_seq_registro_w;
			elsif	(ie_quebra_w = 'P') then
				select	max(e.nr_sequencia),
					max(e.ds_plano),
					WHEB_MENSAGEM_PCK.get_texto(818779) /*'Produto'*/
				into	cd_quebra_w,
					ds_quebra_w,
					ds_tit_quebra_w
				from	pls_plano e,
					pls_contrato_plano d,
					pls_contrato c,
					pls_contrato_pagador a,
					registro_cobranca b
				where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
				and	a.nr_seq_contrato = c.nr_sequencia
				and	d.nr_seq_contrato = c.nr_sequencia
				and	d.dt_fim_vigencia is null
				and	d.nr_seq_plano = e.nr_sequencia
				and	b.nr_sequencia	= nr_seq_registro_w;
				
				if	(cd_quebra_w is null) then
					select	max(e.nr_sequencia),
						max(e.ds_plano),
						WHEB_MENSAGEM_PCK.get_texto(818779) /*'Produto'*/
					into	cd_quebra_w,
						ds_quebra_w,
						ds_tit_quebra_w
					from	pls_plano e,
						pls_contrato_plano d,
						pls_contrato c,
						pls_contrato_pagador a,
						registro_cobranca b
					where	a.cd_cgc = b.cd_cgc
					and	a.nr_seq_contrato = c.nr_sequencia
					and	d.nr_seq_contrato = c.nr_sequencia
					and	d.dt_fim_vigencia is null
					and	d.nr_seq_plano = e.nr_sequencia
					and	b.nr_sequencia	= nr_seq_registro_w;
				end if;
			elsif	(ie_quebra_w = 'R') then
				select	max(e.ie_regulamentacao),
					max(substr(obter_valor_dominio(2157, ie_regulamentacao),1,80)),
					WHEB_MENSAGEM_PCK.get_texto(818780) /*'Regulamentação'*/
				into	cd_quebra_w,
					ds_quebra_w,
					ds_tit_quebra_w
				from	pls_plano e,
					pls_contrato_plano d,
					pls_contrato c,
					pls_contrato_pagador a,
					registro_cobranca b
				where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
				and	a.nr_seq_contrato = c.nr_sequencia
				and	d.nr_seq_contrato = c.nr_sequencia
				and	d.dt_fim_vigencia is null
				and	d.nr_seq_plano = e.nr_sequencia
				and	b.nr_sequencia	= nr_seq_registro_w;
				
				if	(cd_quebra_w is null) then
					select	max(e.ie_regulamentacao),
						max(substr(obter_valor_dominio(2157, ie_regulamentacao),1,80)),
						WHEB_MENSAGEM_PCK.get_texto(818780) /*'Regulamentação'*/
					into	cd_quebra_w,
						ds_quebra_w,
						ds_tit_quebra_w
					from	pls_plano e,
						pls_contrato_plano d,
						pls_contrato c,
						pls_contrato_pagador a,
						registro_cobranca b
					where	a.cd_cgc = b.cd_cgc
					and	a.nr_seq_contrato = c.nr_sequencia
					and	d.nr_seq_contrato = c.nr_sequencia
					and	d.dt_fim_vigencia is null
					and	d.nr_seq_plano = e.nr_sequencia
					and	b.nr_sequencia	= nr_seq_registro_w;
				end if;
			end if;
			
			if	(qt_quebra_w = 1) then
				ds_tit_quebra_1_w	:= ds_tit_quebra_w;
				cd_quebra_1_w		:= cd_quebra_w;
				ds_quebra_1_w		:= ds_quebra_w;	
			elsif	(qt_quebra_w = 2) then
				ds_tit_quebra_2_w	:= ds_tit_quebra_w;
				cd_quebra_2_w		:= cd_quebra_w;
				ds_quebra_2_w		:= ds_quebra_w;
			end if;
			qt_quebra_w := qt_quebra_w+1;
		end loop;
		
		insert into w_relat_registro_cobr
			(	nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				cd_quebra_1,
				ds_quebra_1,
				ds_tit_quebra_1,
				cd_quebra_2,
				ds_quebra_2,
				ds_tit_quebra_2,
				vl_pago,
				vl_emitido,
				qt_parc_aberta,
				nm_devedor,
				dt_registro,
				cd_usuario_plano,
				qt_contrato)
		values	(	w_relat_registro_cobr_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				cd_quebra_1_w,
				ds_quebra_1_w,
				ds_tit_quebra_1_w,
				cd_quebra_2_w,
				ds_quebra_2_w,
				ds_tit_quebra_2_w,
				vl_pago_w,
				vl_cobranca_w,
				qt_parc_aberta_w,
				nm_devedor_w,
				dt_registro_w,
				cd_usuario_plano_w,
				qt_contrato_w);
		end;
	end loop;
	close C01;
elsif (ie_tipo_p = 'S') then
	open C00;
	loop
	fetch C00 into	
		ds_resultado_w,
		nr_seq_resultado_w,
		cd_resultado_w;
	exit when C00%notfound;
		begin
		insert into w_relat_registro_cobr
				(	nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					cd_quebra_1,
					ds_quebra_1,
					ds_tit_quebra_1)
			values	(	w_relat_registro_cobr_seq.nextval,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nvl(cd_resultado_w,to_char(nr_seq_resultado_w)),
					ds_resultado_w,
					ie_tipo_p);
					
		open C03;
		loop
		fetch C03 into	
			qt_parc_aberta_w,
			ds_resultado_2_w,
			vl_emitido_w,
			qt_contrato_w,
			vl_saldo_w,
			nm_operador_w;
		exit when C03%notfound;
			begin
			insert into w_relat_registro_cobr
					(	nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						cd_quebra_1,
						cd_quebra_2,
						ds_quebra_2,
						vl_emitido,
						qt_parc_aberta,
						qt_contrato,
						ds_tit_quebra_1,
						vl_saldo,
						nm_operador)
				values	(	w_relat_registro_cobr_seq.nextval,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nvl(cd_resultado_w,to_char(nr_seq_resultado_w)),
						substr(nm_devedor_w,1,20),
						ds_resultado_2_w,
						vl_emitido_w,
						qt_parc_aberta_w,
						qt_contrato_w,
						'S',
						vl_saldo_w,
						nm_operador_w);
			end;
		end loop;
		close C03;
		end;
	end loop;
	close C00;
end if;


commit;            

end gerar_w_relat_registro_cobr;
/