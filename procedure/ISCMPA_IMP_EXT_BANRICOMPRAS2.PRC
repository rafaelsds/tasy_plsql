create or replace 
procedure ISCMPA_IMP_EXT_BANRICOMPRAS2(	nr_seq_extrato_p	number,
					nm_usuario_p		varchar2) is

ie_tipo_registro_w	varchar2(2);
nr_seq_resumo_w		number(10);
nr_seq_conta_banco_w	number(10);
nr_seq_bandeira_w	number(10);
cd_estabelecimento_w	number(4);
vl_lancamento_w		number(15,2);
vl_parcela_w		number(15,2);
dt_compra_w		date;
dt_parcela_w		date;
nr_parcela_w		number(10);
dt_min_parcela_w	date;
dt_max_parcela_w	date;
nr_sequencia_w		number(10);
ds_comprovante_w	varchar2(100);
dt_prev_pagto_w		date;

cursor	c01 is
select	a.nr_sequencia,
	substr(a.ds_conteudo,1,2) ie_tipo_registro,
	somente_numero(substr(a.ds_conteudo,71,15)) / 100 vl_lancamento,
	0 vl_parcela,
	null dt_parcela,
	0 nr_parcela,
	null dt_compra,
	null ds_comprovante,
	to_date(substr(a.ds_conteudo,29,8),'yyyymmdd') dt_prev_pagto
from	w_extrato_cartao_cr a
where	substr(a.ds_conteudo,1,2)	= '10'
and	a.nr_seq_extrato = nr_seq_extrato_p
union
select	a.nr_sequencia,
	substr(a.ds_conteudo,1,2) ie_tipo_registro,
	0 vl_lancamento,
	somente_numero(substr(a.ds_conteudo,61,15)) / 100 vl_parcela,
	to_date(substr(a.ds_conteudo,43,8),'yyyymmdd') dt_parcela,
	somente_numero(substr(a.ds_conteudo,59,2)) nr_parcela,
	to_date(substr(a.ds_conteudo,29,8),'yyyymmdd') dt_compra,
	trim(substr(a.ds_conteudo,51,8)) ds_comprovante,
	null dt_prev_pagto
from	w_extrato_cartao_cr a
where	substr(a.ds_conteudo,1,2)	= '20'
and	a.nr_seq_extrato = nr_seq_extrato_p
order by	nr_sequencia;

begin

select	max(a.nr_seq_bandeira),
	max(a.cd_estabelecimento)
into	nr_seq_bandeira_w,
	cd_estabelecimento_w
from	extrato_cartao_cr a
where	nr_sequencia	= nr_seq_extrato_p;

select	to_number(obter_valor_bandeira_estab(a.nr_sequencia,cd_estabelecimento_w,'NR_SEQ_CONTA_BANCO'))
into	nr_seq_conta_banco_w
from	bandeira_cartao_cr a
where	a.nr_sequencia	= nr_seq_bandeira_w;

open	c01;
loop
fetch	c01 into
	nr_sequencia_w,
	ie_tipo_registro_w,
	vl_lancamento_w,
	vl_parcela_w,
	dt_parcela_w,
	nr_parcela_w,
	dt_compra_w,
	ds_comprovante_w,
	dt_prev_pagto_w;
exit	when c01%notfound;

	if	(ie_tipo_registro_w	= '10') then

		select	extrato_cartao_cr_res_seq.nextval
		into	nr_seq_resumo_w
		from	dual;

		insert	into extrato_cartao_cr_res
			(dt_atualizacao,
			nm_usuario,
			nr_seq_conta_banco,
			nr_seq_extrato,
			nr_sequencia,
			vl_bruto,
			vl_comissao,
			vl_liquido,
			vl_rejeitado,
			dt_prev_pagto)
		values	(sysdate,
			nm_usuario_p,
			nr_seq_conta_banco_w,
			nr_seq_extrato_p,
			nr_seq_resumo_w,
			vl_lancamento_w,
			0,
			vl_lancamento_w,
			0,
			dt_prev_pagto_w);

	elsif	(ie_tipo_registro_w	= '20') then

		insert	into extrato_cartao_cr_movto
			(ds_comprovante,
			dt_atualizacao,
			dt_compra,
			dt_parcela,
			ie_pagto_indevido,
			nm_usuario,
			nr_parcela,
			nr_seq_extrato,
			nr_seq_extrato_res,
			nr_sequencia,
			vl_parcela,
			vl_saldo_concil_cred)
		values	(ds_comprovante_w,
			sysdate,
			dt_compra_w,
			dt_parcela_w,
			'N',
			nm_usuario_p,
			nr_parcela_w,
			nr_seq_extrato_p,
			nr_seq_resumo_w,
			extrato_cartao_cr_movto_seq.nextval,
			vl_parcela_w,
			vl_parcela_w);

	end if;

end	loop;
close	c01;

select	min(dt_compra),
	max(dt_compra)
into	dt_min_parcela_w,
	dt_max_parcela_w
from	extrato_cartao_cr_movto
where	nr_seq_extrato	= nr_seq_extrato_p;

update	extrato_cartao_cr
set	dt_importacao	= sysdate,
	dt_inicial	= dt_min_parcela_w,
	dt_final	= dt_max_parcela_w,
	nr_seq_grupo = null
where	nr_sequencia	= nr_seq_extrato_p;

commit;

end ISCMPA_IMP_EXT_BANRICOMPRAS2;
/

