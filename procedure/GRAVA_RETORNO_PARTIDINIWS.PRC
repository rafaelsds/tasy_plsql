create or replace
procedure grava_retorno_partidiniws (cd_identificacao_p 	 varchar2,
                                     nm_usuario_p          varchar2,
                                     cd_laboratorio_p      varchar2,
                                     cd_lote_laboratorio_p varchar2,
                                     cd_lote_apoio_p       number, 
                                     dt_lote_apoio_p       date,
                                     hr_lote_apoio_p       date,
                                     nr_prescricao_p       number,   
                                     cd_exame_integrado_p  varchar2,
                                     cd_exame_p            varchar2,
                                     cd_barras_apoio_p     varchar2,
                                     ds_etiqueta1_p        varchar2,
                                     ds_etiqueta2_p        varchar2,
                                     cd_pedido_apoio_p     varchar2) is

c001			      INTEGER;
retorno_w	      NUMBER(5);
ds_sql_w	      varchar2(5000);
nr_sequencia_w  number;
nr_seq_prescr_w number;
ds_infor_w      varchar2(2000);
begin
	select lab_etiq_prescr_integracao_seq.NextVal
   	into nr_sequencia_w
    from dual;

  select max(a.nr_sequencia)
    into nr_seq_prescr_w
    from prescr_procedimento a
         inner join exame_laboratorio c on c.nr_seq_exame = a.nr_seq_exame
         inner join prescr_medica	d on a.nr_prescricao	= d.nr_prescricao         
         inner join atendimento_paciente g on d.nr_atendimento = g.nr_atendimento AND g.cd_estabelecimento = d.cd_estabelecimento             
   where a.nr_prescricao = nr_prescricao_p 
     and a.nr_seq_lote_externo = cd_lote_laboratorio_p
     and case when (select nvl(max(h.ie_utiliza_cod_equip), 'N') 
                      from empresa_integr_dados h 
                     where h.nr_seq_empresa_integr = 157 
                       and h.ie_situacao = 'A') = 'S' then lab_obter_codigo_exame_equip('PARDINIWS', c.nr_seq_exame, g.ie_tipo_atendimento, NULL, NULL, 'CEXE') 
                                                      else c.CD_EXAME end = cd_exame_p;

	C001 := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(C001, ' insert into lab_etiq_prescr_integracao (nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, cd_identificacao, cd_laboratorio, cd_lote_laboratorio, cd_lote_apoio, dt_lote_apoio, hr_lote_apoio, nr_prescricao, nr_seq_prescr, cd_exame_integracao, cd_exame, cd_barras_apoio, ds_etiqueta) '||
                                                        'values (:nr_sequencia, sysdate, :nm_usuario, sysdate, :nm_usuario, :cd_identificacao, :cd_laboratorio, :cd_lote_laboratorio, :cd_lote_apoio, :dt_lote_apoio, :hr_lote_apoio, :nr_prescricao, :nr_seq_prescr, :cd_exame_integracao, :cd_exame, :cd_barras_apoio, :ds_etiqueta) ', dbms_sql.Native);
	DBMS_SQL.BIND_VARIABLE(C001, 'nr_sequencia', nr_sequencia_w);
	DBMS_SQL.BIND_VARIABLE(C001, 'nm_usuario', nm_usuario_p);	
  DBMS_SQL.BIND_VARIABLE(C001, 'cd_identificacao', cd_identificacao_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'cd_laboratorio', cd_laboratorio_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'cd_lote_laboratorio', cd_lote_laboratorio_p);
  DBMS_SQL.BIND_VARIABLE(C001, 'cd_lote_apoio', cd_lote_apoio_p);
  DBMS_SQL.BIND_VARIABLE(C001, 'dt_lote_apoio', dt_lote_apoio_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'hr_lote_apoio', hr_lote_apoio_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'nr_prescricao', nr_prescricao_p);
  DBMS_SQL.BIND_VARIABLE(C001, 'nr_seq_prescr', nr_seq_prescr_w);
  DBMS_SQL.BIND_VARIABLE(C001, 'cd_exame_integracao', cd_exame_integrado_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'cd_exame', cd_exame_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'cd_barras_apoio', cd_barras_apoio_p);
  DBMS_SQL.BIND_VARIABLE(C001, 'ds_etiqueta', (ds_etiqueta1_p||ds_etiqueta2_p));  

	retorno_w := DBMS_SQL.EXECUTE(c001);
	DBMS_SQL.CLOSE_CURSOR(C001);
  commit;

  lab_atualiza_ds_pedido_externo(nr_prescricao_p, nr_seq_prescr_w, cd_pedido_apoio_p, nm_usuario_p, ds_infor_w);

end grava_retorno_partidiniws;
/