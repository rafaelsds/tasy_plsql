create or replace
procedure vivale_importacao_same is 

--cd_estabelecimento_w	number;
--ie_digitalizado_w		varchar2(1);
--ie_microfilmado_w		varchar2(1);
cd_pessoa_fisica_w		varchar(10);
nr_atendimento_w		number;
nr_seq_caixa_w			number;
nr_seq_local_w			number;
dt_periodo_inicial_w	date;
dt_periodo_final_w		date;
ie_tipo_w				number;
ie_status_w				number;
cd_setor_atendimento_w	number;

cursor c01 is
	select	nvl(obter_pessoa_atendimento(n2, 'C'),0) cd_pessoa_fisica,
			n2 nr_atendimento,
			n4 NR_SEQ_CAIXA,
			n6 nr_seq_local,
			to_date(to_char(n7,'00000000')) DT_PERIODO_INICIAL,
			to_date(to_char(n8,'00000000')) DT_PERIODO_final,
			n9 IE_TIPO,
			n10 IE_STATUS,
			n11 CD_SETOR_ATENDIMENTO
	from	w_imp_same;	


begin
open c01;
loop
fetch c01 into	cd_pessoa_fisica_w,
				nr_atendimento_w,
				nr_seq_caixa_w,
				nr_seq_local_w,
				dt_periodo_inicial_w,
				dt_periodo_final_w,
				ie_tipo_w,
				ie_status_w,
				cd_setor_atendimento_w;
	exit when c01%notfound;


	insert into same_prontuario(nr_sequencia,
								cd_estabelecimento,
								ie_digitalizado,
								ie_microfilmado,
								dt_atualizacao,
								nm_usuario,
								cd_pessoa_fisica,
								nr_atendimento,
								nr_seq_caixa,
								nr_seq_local,
								dt_periodo_inicial,
								dt_periodo_final,
								ie_tipo,
								ie_status,
								cd_setor_atendimento)
				values			(same_prontuario_seq.nextval,
								1,
								'N',
								'N',
								sysdate,
								'Tasy 12/09',
								cd_pessoa_fisica_W,
								nr_atendimento_w,
								nr_seq_caixa_w,
								nr_seq_local_w,
								dt_periodo_inicial_w,
								dt_periodo_final_w,
								ie_tipo_w,
								ie_status_w,
								cd_setor_atendimento_w);
end loop;
close c01;
commit;

end vivale_importacao_same;
/