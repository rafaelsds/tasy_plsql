create or replace
procedure GERAR_COBR_CAIXA_240_V045
		(	nr_seq_cobr_escrit_p		number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is 
			
ds_conteudo_w			varchar2(240);
nr_remessa_w			varchar2(8);
qt_lote_arquivo_w		varchar2(6) := 0;
qt_reg_lote_w			varchar2(6) := 0;
qt_registro_lote_w		varchar2(6) := 0;
nr_seq_arquivo_w		varchar2(6) := 0;
nr_seq_registro_w		number(10) := 0;
nm_empresa_w			varchar2(30);
cd_cgc_w			varchar2(14);
nm_banco_w			varchar2(30);
dt_geracao_w			varchar2(8);
hr_geracao_w			varchar2(6);
nr_lote_w_w			varchar2(4) := 0;
nr_lote_w			varchar2(4);
cd_convenio_banco_w		varchar2(16);
cd_agencia_banco_w		varchar2(5);
ie_dig_agencia_banco_w		varchar2(1);
cd_conta_banco_w		varchar2(12);
ie_dig_conta_banco_w		varchar2(1);

/* Segmentos */
nr_nosso_numero_w		varchar2(20);
dt_vencimento_w			varchar2(8);
vl_titulo_w			varchar2(15);
dt_emissao_w			varchar2(8);
cd_agencia_w			varchar2(5);
ie_digito_agencia_w		varchar2(1);
nr_conta_w			varchar2(12);
ie_digito_conta_w		varchar2(1);
cd_conta_cobr_w			varchar2(9);
ie_conta_cobr_w			varchar2(1);
nr_seu_numero_w			varchar2(15);
ie_tipo_inscricao_w		varchar2(1);
nr_inscricao_w			varchar2(15);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
ds_bairro_sacado_w		varchar2(15);
cd_cep_sacado_w			varchar2(8);
ds_municipio_sacado_w		varchar2(15);
ds_estado_sacado_w		varchar2(2);
cd_mov_remessa_w		varchar2(2);
cd_transmissao_w		varchar2(15);
nm_cedente_w			varchar2(30);
nr_seq_pagador_w		number(10);
vl_multa_w			varchar2(15);

nr_titulo_w			varchar2(15);
ds_mensagem_4_w			varchar2(40);
vl_mora_w			varchar2(15);
nr_seq_mensalidade_w		number(10);
ds_carteira_w			varchar2(11);
nm_segurado_w			varchar2(29);
ds_idade_w			varchar2(4);
vl_mensalidade_w		varchar2(11);
ds_tipo_item_w			varchar2(31);
ds_produto_w			varchar2(62);
vl_juros_w			number(15,2);
vl_desconto_w			varchar2(15);
cd_juros_w			varchar2(1);
vl_juros_dia_w			varchar2(15);

/* Segmento P */
cursor	C01 is
select	lpad(substr(nvl(b.cd_agencia_bancaria,'0'),1,5),5,'0') cd_agencia,
	lpad(substr(nvl(b.ie_digito,'0'),1,1),1,'0') ie_digito_agencia,
	lpad(substr(nvl(a.nr_conta,'0'),1,12),12,'0') nr_conta,
	lpad(substr(nvl(a.ie_digito_conta,'0'),1,1),1,'0') ie_digito_conta,
	lpad(substr(decode(nvl(c.nr_nosso_numero,'0'),'0',82 || lpad(c.nr_titulo,17,'0') || calcula_digito('Modulo11',(82 || lpad(c.nr_titulo,17,'0'))),c.nr_nosso_numero),1,20),20,' ') nr_nosso_numero,
	nvl(a.nr_titulo,0) nr_titulo,
	lpad(nvl(to_char(c.dt_pagamento_previsto,'ddmmyyyy'),'0'),8,'0') dt_vencimento,
	lpad(nvl(somente_numero(to_char(a.vl_cobranca,'9999999999990.00')),0),15,'0') vl_titulo,
	lpad(nvl(to_char(c.dt_emissao,'ddmmyyyy'),'0'),8,'0') dt_emissao,
	decode(c.cd_cgc,null,'1','2') ie_tipo_inscricao,
	lpad(nvl(c.cd_cgc,nvl(d.nr_cpf,'0')),15,'0') nr_inscricao,
	rpad(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,40),40,' ') nm_sacado,
	rpad(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'E'),1,40),40,' ') ds_endereco_sacado,
	rpad(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'B'),1,15),15,' ') ds_bairro_sacado,
	lpad(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CEP'),1,8),8,'0') cd_cep_sacado,
	rpad(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CI'),1,15),15,' ') ds_municipio_sacado,
	rpad(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'UF'),1,2),2,' ') ds_estado_sacado,
	lpad(nvl(somente_numero(to_char(nvl(a.vl_multa,0),'9999999999990.00')),0),15,'0') vl_multa,
	c.nr_seq_mensalidade,
	nvl(a.vl_juros,0) vl_juros,
	lpad(nvl(somente_numero(to_char(nvl(a.vl_desconto,0),'9999999999990.00')),0),15,'0') vl_desconto,
	lpad(nvl(somente_numero(to_char((nvl(a.vl_cobranca,0) * (nvl(c.tx_juros,0) / 100)) / decode(nvl(e.ie_tipo_taxa,'M'),'M',30,1),'9999999999990.00')),0),15,'0') vl_juros_dia
from	tipo_taxa e,
	pessoa_fisica d,
	titulo_receber c,
	agencia_bancaria b,
	titulo_receber_cobr a
where	c.cd_tipo_taxa_juro	= e.cd_tipo_taxa(+)
and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	a.nr_titulo		= c.nr_titulo
and	a.cd_agencia_bancaria	= b.cd_agencia_bancaria
and	a.cd_banco		= b.cd_banco
and	a.nr_seq_cobranca	= nr_seq_cobr_escrit_p;

cursor	c02 is
select	rpad(nvl(decode(d.ie_tipo_contratacao,'I',substr(pls_obter_dados_segurado(b.nr_seq_segurado,'C'),1,11),' '),' '),11,' ') ds_carteira,
	rpad(nvl(decode(d.ie_tipo_contratacao, 'I', substr(pls_obter_dados_segurado(b.nr_seq_segurado,'N'),1,29),' '),' '),29,' ') nm_segurado,
	rpad(nvl(decode(d.ie_tipo_contratacao, 'I', to_char(b.qt_idade),' '),' '),4,' ') ds_idade,
	lpad(to_char(somente_numero(to_char(nvl(decode(d.ie_tipo_contratacao, 'I', e.vl_item,0),0),'999999990.00'))),11,0) vl_mensalidade,
	rpad(nvl(decode(d.ie_tipo_contratacao, 'I', substr(obter_valor_dominio(1930,e.ie_tipo_item),1,31),' '),' '),31,' ') ds_tipo_item,
	rpad(nvl(substr(decode(d.ie_tipo_contratacao, 'I', decode(e.ie_tipo_item,'3','Guia: ' || f.cd_guia || ' - Prestador: ' || substr(pls_obter_dados_prestador(g.nr_seq_prestador,'N'),1,255),'Produto: ' || c.nr_seq_plano || ' - ' || pls_obter_dados_produto(c.nr_seq_plano,'S')),' '),1,62),' '),62,' ') ds_produto
from	pls_protocolo_conta g,
	pls_conta f,
	pls_mensalidade_seg_item e,
	pls_plano d,
	pls_segurado c,
	pls_mensalidade_segurado b,
	pls_mensalidade a
where	d.ie_tipo_contratacao	= 'I'
and	f.nr_seq_protocolo      = g.nr_sequencia(+)
and	e.nr_seq_conta          = f.nr_sequencia(+)
and	b.nr_sequencia          = e.nr_seq_mensalidade_seg
and	c.nr_seq_plano		= d.nr_sequencia
and	b.nr_seq_segurado	= c.nr_sequencia
and	a.nr_sequencia		= b.nr_seq_mensalidade
and	a.nr_sequencia		= nr_seq_mensalidade_w;

begin

delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

/* Header Arquivo*/
select	rpad(elimina_acentuacao(substr(obter_nome_pf_pj(null,b.cd_cgc),1,30)),30,' ') nm_empresa,
	rpad(upper(substr(obter_nome_banco(c.cd_banco),1,30)),30,' ') nm_banco,
	lpad(b.cd_cgc,14,'0') cd_cgc,
	lpad(to_char(nvl(a.nr_remessa,a.nr_sequencia)),6,'0') nr_seq_arquivo,
	to_char(sysdate,'ddmmyyyy') dt_geracao,
	to_char(sysdate,'hh24miss') hr_geracao,
	lpad(substr(nvl(c.cd_transmissao,'0'),1,15),15,'0') cd_transmissao,
	lpad(substr(nvl(c.cd_convenio_banco,'0'),1,16),16,'0') cd_convenio_banco,
	lpad(substr(nvl(e.cd_agencia_bancaria,'0'),1,5),5,'0') cd_agencia_banco,
	lpad(substr(nvl(e.ie_digito,'0'),1,1),1,'0') ie_dig_agencia_banco,
	lpad(substr(nvl(c.cd_conta,'0'),1,12),12,'0') cd_conta_banco,
	lpad(substr(nvl(c.ie_digito_conta,'0'),1,1),1,'0') ie_dig_conta_banco,
	lpad(substr(nvl(a.nr_remessa,a.nr_sequencia),1,8),8,'0') nr_remessa
into	nm_empresa_w,
	nm_banco_w,
	cd_cgc_w,
	nr_seq_arquivo_w,
	dt_geracao_w,
	hr_geracao_w,
	cd_transmissao_w,
	cd_convenio_banco_w,
	cd_agencia_banco_w,
	ie_dig_agencia_banco_w,
	cd_conta_banco_w,
	ie_dig_conta_banco_w,
	nr_remessa_w
from	agencia_bancaria e,
	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	c.cd_agencia_bancaria	= e.cd_agencia_bancaria
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	'104' ||
			'0000' ||
			'0' ||
			rpad(' ',9,' ') ||
			'2' ||
			cd_cgc_w ||
			cd_convenio_banco_w ||
			rpad(' ',4,' ') ||
			cd_agencia_banco_w ||
			ie_dig_agencia_banco_w ||
			cd_conta_banco_w ||
			ie_dig_conta_banco_w ||
			' ' ||
			nm_empresa_w || 
			nm_banco_w ||
			rpad(' ',10,' ') ||
			'1' ||
			dt_geracao_w ||
			hr_geracao_w ||
			nr_seq_arquivo_w ||
			'030' ||
			'00000' ||
			rpad(' ',20,' ') ||
			rpad(' ',20,' ') ||
			rpad(' ',29,' ');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	1,
	1);

qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
/* Fim Header Arquivo */

/* Header Lote */
nr_lote_w_w	:= nvl(nr_lote_w_w,0) + 1;
nr_lote_w	:= lpad(nr_lote_w_w,4,'0');

ds_conteudo_w	:=	'104' ||
			nr_lote_w ||
			'1' ||
			'R' ||
			'01' ||
			'  ' ||
			'045' ||
			' ' ||
			'2' ||
			lpad(cd_cgc_w,15,'0') ||
			cd_convenio_banco_w ||
			rpad(' ',4,' ') ||
			cd_agencia_banco_w ||
			ie_dig_agencia_banco_w ||
			cd_conta_banco_w ||
			ie_dig_conta_banco_w ||
			' ' ||
			nm_empresa_w ||
			rpad(' ', 40, ' ') ||
			rpad(' ', 40, ' ') ||
			nr_remessa_w ||
			dt_geracao_w ||
			'00000000' ||
			rpad(' ', 33, ' ');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	2,
	2);
	
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
qt_registro_lote_w	:= qt_registro_lote_w + 1;
qt_reg_lote_w		:= qt_reg_lote_w + 1;
/* Fim Header Lote */

/* Inicio Segmentos */
open	C01;
loop
fetch	C01 into	
	cd_agencia_w,
	ie_digito_agencia_w,
	nr_conta_w,
	ie_digito_conta_w,
	nr_nosso_numero_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	ds_bairro_sacado_w,
	cd_cep_sacado_w,
	ds_municipio_sacado_w,
	ds_estado_sacado_w,
	vl_multa_w,
	nr_seq_mensalidade_w,
	vl_juros_w,
	vl_desconto_w,
	vl_juros_dia_w;
exit	when C01%notfound;

	/* Segmento P */
	if	(vl_juros_w	> 0) then
		cd_juros_w	:= '1';
	else
		cd_juros_w	:= '3';
	end if;

	nr_seq_registro_w	:= nr_seq_registro_w + 1;

	ds_conteudo_w	:=	'104' ||
				nr_lote_w ||
				'3' ||
				lpad(nvl(nr_seq_registro_w,0), 5, '0') ||
				'P' ||
				' ' ||
				'01' || 
				cd_agencia_w ||
				ie_digito_agencia_w ||
				nr_conta_w ||
				ie_digito_conta_w ||
				' ' ||
				lpad(nvl(nr_nosso_numero_w,'0'),20,' ') ||
				'1' ||
				'1' ||
				'2' ||
				'1' ||
				'1' ||
				rpad(nr_titulo_w,15,' ') ||
				dt_vencimento_w ||
				vl_titulo_w ||
				'00000' ||
				'0' ||
				'99' ||
				'N' ||
				dt_emissao_w ||
				nvl(cd_juros_w,'0') ||
				dt_vencimento_w || 
				vl_juros_dia_w ||
				'1' ||
				dt_vencimento_w ||
				vl_desconto_w ||
				lpad('0', 15, '0') ||
				lpad('0', 15, '0') ||
				rpad(nr_titulo_w,25,' ') ||
				'0' ||
				'00' ||
				'1' ||
				'000' ||
				'09' || 
				rpad('0', 10, '0') ||
				' ';
	
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		3,
		nr_seq_registro_w);
	/* Fim segmento P */
	
	qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
	qt_registro_lote_w	:= qt_registro_lote_w + 1;

	/* Segmento Q */
	ds_conteudo_w		:= null;
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	
	ds_conteudo_w	:=	'104' ||
				nr_lote_w ||
				'3' ||
				lpad(nvl(nr_seq_registro_w,0), 5, '0') ||
				'Q' ||
				' ' ||
				'01' ||
				ie_tipo_inscricao_w ||
				nr_inscricao_w ||
				nm_sacado_w ||
				ds_endereco_sacado_w ||
				ds_bairro_sacado_w ||
				cd_cep_sacado_w ||
				ds_municipio_sacado_w ||
				ds_estado_sacado_w ||
				lpad('0', 16, '0') ||
				rpad(' ', 40, ' ') ||
				lpad('0',3,'0') ||
				rpad(' ', 20, ' ') ||
				rpad(' ', 8, ' ');
				
	insert	into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		3,
		nr_seq_registro_w);
		
	/* Fim segmento Q */
	qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
	qt_registro_lote_w	:= qt_registro_lote_w + 1;
	
	/* Segmento R */
	ds_conteudo_w		:= null;
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	
	ds_conteudo_w	:=	'104' ||
				nr_lote_w ||
				'3' ||
				lpad(nvl(nr_seq_registro_w,0), 5, '0') ||
				'R' ||
				' ' ||
				'01' ||
				rpad(' ',48,' ') ||
				'2' ||
				dt_vencimento_w ||
				vl_multa_w ||
				rpad(' ',10,' ') ||
				rpad(' ',40,' ') ||
				rpad(' ',40,' ') ||
				rpad(' ',20,' ') ||
				lpad('0',8,'0') ||
				lpad('0',3,'0') ||
				lpad('0',5,'0') ||
				' ' ||
				lpad('0',12,'0') ||
				' ' ||
				' ' ||
				'0' ||
				rpad(' ',9,' '); 
				
	insert	into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		3,
		nr_seq_registro_w);
		
	/* Fim segmento R */
	qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
	qt_registro_lote_w	:= qt_registro_lote_w + 1;
	
	open	C02;
	loop
	fetch	C02 into	
		ds_carteira_w,
		nm_segurado_w,
		ds_idade_w,
		vl_mensalidade_w,
		ds_tipo_item_w,
		ds_produto_w;
	exit	when C02%notfound;
		begin

		/* Segmento S */
		ds_conteudo_w		:= null;
		nr_seq_registro_w	:= nr_seq_registro_w + 1;

		ds_conteudo_w	:=	'104' ||
					nr_lote_w ||
					'3' ||
					lpad(nvl(nr_seq_registro_w,0), 5, '0') ||
					'S' ||
					' ' ||
					'01' ||
					'3' ||
					ds_carteira_w ||
					nm_segurado_w ||
					ds_idade_w ||
					vl_mensalidade_w ||
					ds_tipo_item_w ||
					rpad(' ',12,' ') ||
					ds_produto_w ||
					rpad(' ',40,' ') ||
					rpad(' ',22,' ');
					
		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres,
			nr_seq_apres_2)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			3,
			nr_seq_registro_w);
			
		/* Fim segmento S */
		qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
		qt_registro_lote_w	:= qt_registro_lote_w + 1;

		end;

	end loop;
	close C02;

end	loop;
close	C01;
/*Fim Segmentos */

/* Trailler Lote*/
begin
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
qt_registro_lote_w	:= qt_registro_lote_w + 1;
	
ds_conteudo_w	:=	'104' ||
			nr_lote_w ||
			'5' ||
			lpad(' ', 9, ' ') ||
			lpad(qt_registro_lote_w, 6, '0') ||
			lpad('0',6,'0') ||
			lpad('0',17,'0') ||
			lpad('0',6,'0') ||
			lpad('0',17,'0') ||
			lpad('0',6,'0') ||
			lpad('0',17,'0') ||
			lpad('0',6,'0') ||
			lpad('0',17,'0') ||
			rpad(' ',8, ' ') ||
			rpad(' ',117, ' ');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	5,
	5);
end;
/* Fim Trailler Lote*/


/* Trailler Arquivo*/
begin
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;

ds_conteudo_w	:=	'104' ||
			'9999' ||
			'9' ||
			lpad(' ', 9, ' ') ||
			lpad(qt_reg_lote_w, 6, '0') ||
			lpad(qt_lote_arquivo_w, 6, '0') ||
			rpad('0', 6, '0') ||
			rpad(' ', 205, ' ');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	6,
	6);
end;
/* Fim Trailler Arquivo*/

commit;

end GERAR_COBR_CAIXA_240_V045;
/