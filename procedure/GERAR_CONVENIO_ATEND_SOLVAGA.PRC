create or replace
procedure Gerar_Convenio_Atend_SolVaga(	nr_atendimento_p	number,
										nr_seq_gestao_p		number, 
										nm_usuario_p		varchar2) is

nr_seq_diag_autor_w		number(10);
					
cursor c01 is
	select	a.nr_sequencia, a.dt_atualizacao, a.nm_usuario, a.dt_atualizacao_nrec, a.nm_usuario_nrec, a.nr_sequencia_autor, 
	        a.dt_diagnostico, a.ie_tipo_diagnostico, a.cd_medico, a.ds_diagnostico
      from	autor_diag_medico a,
			autorizacao_convenio b
	 where	a.nr_sequencia_autor = b.nr_sequencia
	   and	b.nr_seq_gestao      = nr_seq_gestao_p
	   and	not exists( select 1 from diagnostico_medico x 
	                     where x.nr_atendimento = b.nr_atendimento 
						   and x.dt_diagnostico = a.dt_diagnostico);

cursor c03 is
	select	nr_sequencia, dt_atualizacao_nrec, nm_usuario_nrec, dt_atualizacao, nm_usuario, nr_seq_autor_diag, 
	        cd_doenca, ds_diagnostico, ie_classificacao_doenca, ie_tipo_doenca, ie_unidade_tempo, 
			qt_tempo
	  from	autor_diag_doenca 
	 where	nr_seq_autor_diag = nr_seq_diag_autor_w;
	
c01_w				c01%rowtype;
c03_w				c03%rowtype;	
	
begin

if	(nr_atendimento_p > 0) and 
	(nr_seq_gestao_p > 0) then
	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		begin
		nr_seq_diag_autor_w	:= c01_w.nr_sequencia;
		
		insert 	into diagnostico_medico	
			(nr_atendimento,
			dt_diagnostico,
			ie_tipo_diagnostico,
			cd_medico,
			dt_atualizacao,
			nm_usuario,
			ds_diagnostico)
		values
			(nr_atendimento_p, 
			c01_w.dt_diagnostico,
			c01_w.ie_tipo_diagnostico,
			c01_w.cd_medico,
			sysdate,
			nm_usuario_p,
			c01_w.ds_diagnostico);		
		commit;
		
		open C03;
		loop
		fetch C03 into	
			c03_w;
		exit when C03%notfound;
			begin
			insert	into Diagnostico_Doenca
				(NR_ATENDIMENTO,
				DT_DIAGNOSTICO,
				CD_DOENCA,
				DT_ATUALIZACAO,
				NM_USUARIO,
				DS_DIAGNOSTICO,
				IE_CLASSIFICACAO_DOENCA)
			values
				(nr_atendimento_p,   /*Integridade  NR_ATENDIMENTO + DT_DIAGNOSTICO*/
				c01_w.dt_diagnostico,/*Integridade  NR_ATENDIMENTO + DT_DIAGNOSTICO*/
				c03_w.CD_DOENCA,
				sysdate,
				nm_usuario_p,
				c03_w.DS_DIAGNOSTICO, 
				c03_w.IE_CLASSIFICACAO_DOENCA);			
			end;
		end loop;
		close C03;		
		
		end;
	end loop;
	close C01;
end if;

commit;

end Gerar_Convenio_Atend_SolVaga;
/
