create or replace
procedure obter_valores_cheques_neg_js(
		nr_sequencia_p			number,
		ie_cheque_cr_p		out	varchar2,
		cd_tipo_portador_p	out	number,
		cd_portador_p		out	number,
		ds_erro_1_p		out	varchar2,
		ds_erro_2_p		out	varchar2,
		ds_erro_3_p		out	varchar2) is
		
begin

	select	nvl(ie_cheque_cr,'N') ie_cheque_cr,
		cd_tipo_portador,
		cd_portador
	into	ie_cheque_cr_p,
		cd_tipo_portador_p,
		cd_portador_p
	from	transacao_financeira
	where	nr_sequencia = nr_sequencia_p;
	
	ds_erro_1_p :=	obter_texto_tasy(98469, wheb_usuario_pck.get_nr_seq_idioma);
	ds_erro_2_p :=	obter_texto_tasy(98470, wheb_usuario_pck.get_nr_seq_idioma);
	ds_erro_3_p :=	obter_texto_tasy(98471, wheb_usuario_pck.get_nr_seq_idioma);

end obter_valores_cheques_neg_js;
/