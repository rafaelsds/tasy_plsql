create or replace
procedure dis_gerar_arq_moviment(nr_atendimento_p number) is

arq_texto_w		utl_file.file_type;
ds_linha_arquivo_w		varchar2(2000);
nm_arquivo_w		varchar2(100);
ds_diretorio_w		varchar2(255);
qt_itens_w		number(6);
nm_pessoa_fisica_w      	varchar2(60);
nr_atendimento_w       	number(10);
ds_setor_atendimento_w	varchar2(100);
ds_unidade_atendimento_w  	varchar2(100);
dt_entrada_w           	date;
ie_sexo_w               		varchar2(1);
dt_nascimento_w    	     	date;
cd_estabelecimento_w	number(4);
nm_usuario_w		varchar2(15);
ie_forma_integracao_w	varchar2(1);


Cursor C01 is
	SELECT  'EPA' || 
        '|' || 
        TO_CHAR(SYSDATE,'MMDDYYYYHHMMSS') || 
        '|' ||
        SUBSTR(c.NM_PESSOA_FISICA, 0, 29) ||
        '|' ||
        a.nr_atendimento ||
        '||' || -- C�digo alternativo de identifica��o do paciente
        '|' || -- Sem descri��o
	obter_ds_descricao_setor(obter_setor_atendimento(nr_atendimento_p)) ||
        '|' ||
        LTRIM(RTRIM(obter_unidade_atendimento(a.nr_atendimento, 'IAR', 'U'))) ||
        '|01' ||
        '|' ||
        '||' ||
        a.nr_atendimento ||
        '|' ||
        TO_CHAR(dt_entrada,'MMDDYYYYHHMMSS') ||
        '|||||||||||||' ||
        c.ie_sexo ||
        '|' ||
        TO_CHAR(c.dt_nascimento,'YYYYMMDD') || 
        '|||||' ||
        'SHS' ||
        '|||'
	from    ocupacao_hospitalar a, 
	        atendimento_paciente b, 
	        pessoa_fisica c
	where   a.cd_setor_atendimento in (select cd_setor_atendimento from dis_regra_setor)
	and     a.nr_atendimento = b.nr_atendimento
	and     a.nr_atendimento = nr_atendimento_p
	and     b.cd_pessoa_fisica = c.cd_pessoa_fisica
	order by c.nm_pessoa_fisica;

Cursor C02 is
	select	substr(c.nm_pessoa_fisica,1,60) nm_pessoa_fisica,
		a.nr_atendimento, 
		substr(obter_ds_descricao_setor(obter_setor_atendimento(nr_atendimento_p)),1,100) ds_setor_atendimento,
		substr(obter_unidade_atendimento(a.nr_atendimento, 'IAR', 'U'),1,100)ds_unidade_atendimento,
		c.ie_sexo,
		b.dt_entrada,
		c.dt_nascimento
	from    ocupacao_hospitalar a, 
		atendimento_paciente b, 
		pessoa_fisica c
	where   a.cd_setor_atendimento in (select cd_setor_atendimento from dis_regra_setor)
	and     a.nr_atendimento = b.nr_atendimento
	and     a.nr_atendimento = nr_atendimento_p
	and     b.cd_pessoa_fisica = c.cd_pessoa_fisica
	order by c.nm_pessoa_fisica; 
	
--pragma autonomous_transaction;

BEGIN

select	cd_estabelecimento,
	nm_usuario 
into	cd_estabelecimento_w,
	nm_usuario_w	
from	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

select  nvl(max(ie_forma_integracao),'A')
into	ie_forma_integracao_w
from	dis_parametros_int
where	cd_estabelecimento = cd_estabelecimento_w;

select	substr(obter_valor_param_usuario(3111, 74, Obter_perfil_ativo, '', ''),1,255)
into	ds_diretorio_w
from	dual;

if	(ds_diretorio_w is not null)  and (ie_forma_integracao_w = 'A')then

	select	count(*)
	into	qt_itens_w
	from    ocupacao_hospitalar a, 
	        atendimento_paciente b, 
	        pessoa_fisica c
	where   a.cd_setor_atendimento in(select cd_setor_atendimento from dis_regra_setor)
	and     a.nr_atendimento = b.nr_atendimento
	and     a.nr_atendimento = nr_atendimento_p
	and     b.cd_pessoa_fisica = c.cd_pessoa_fisica
	order by c.nm_pessoa_fisica;

	nm_arquivo_w	:= to_char(sysdate,'MMDDYYYYHHMMSS') || '.pyx';

	if	(qt_itens_w > 0) then
		arq_texto_w 	:= utl_file.fopen(ds_diretorio_w,nm_arquivo_w,'W');

		open C01;
		loop
		fetch C01 into	
			ds_linha_arquivo_w;
		exit when C01%notfound;
			--gera uma nova linha no arquivo		
		
		utl_file.put_line(arq_texto_w,ds_linha_arquivo_w);
		utl_file.fflush(arq_texto_w);
		end loop;
		close C01;
		--fecha e libera o arquivo
		utl_file.fclose(arq_texto_w);
		
	end if;
end if;

if	(ie_forma_integracao_w = 'B') then
	begin
	open C02;
	loop
	fetch C02 into	
		nm_pessoa_fisica_w,
		nr_atendimento_w,
		ds_setor_atendimento_w,
		ds_unidade_atendimento_w,
		ie_sexo_w,
		dt_entrada_w,
		dt_nascimento_w;
	exit when C02%notfound;
		begin
		insert into dis_entrada_paciente(
				nr_sequencia,         
				dt_atualizacao,
				nm_usuario,   
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nm_pessoa_fisica,
				nr_atendimento,
				ds_setor_atendimento,
				ds_unidade_atendimento,
				dt_entrada,
				ie_sexo,
				dt_nascimento,
				ie_status)
		values	(	dis_entrada_paciente_seq.nextval,
				sysdate,
				nm_usuario_w,
				sysdate,
				nm_usuario_w,
				nm_pessoa_fisica_w,
				nr_atendimento_w,	
				ds_setor_atendimento_w,
				ds_unidade_atendimento_w,
				dt_entrada_w,
				ie_sexo_w,
				dt_nascimento_w,
				'0');
		end;
		
	end loop;
	close C02;
	end;
end if;
	
end dis_gerar_arq_moviment;
/