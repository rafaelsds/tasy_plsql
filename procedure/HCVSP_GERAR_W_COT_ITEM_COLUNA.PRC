create or replace
procedure hcvsp_gerar_w_cot_item_coluna(	nr_cot_compra_p	number) is

nr_seq_cot_item_w			number(10);
nr_seq_cot_col_w			number(10);
nr_item_cot_compra_w		number(5);
cd_material_w			number(6);
cd_fornecedor_w			varchar2(14);
qt_material_w			number(13,4);
vl_unitario_material_w		number(13,4);
nr_seq_coluna_w			number(5);
ds_marca_w			varchar2(30);
vl_ipi_w			number(15,4);
cd_condicao_pagamento_w		number(10);
vl_frete_w			number(15,2);
qt_dias_entrega_w		number(5);
vl_unitario_inicial_w		number(13,4);
vl_liquido_w			number(13,4);
ie_vencedor_w			varchar2(1);
nr_seq_cot_item_forn_w		number(10);
ds_observacao_w			varchar2(255);
ds_observacao_item_w		varchar2(255);
cursor c01 is
		select	nr_item_cot_compra,
			cd_material
		from	cot_compra_item
		where	nr_cot_compra = nr_cot_compra_p
		union all
		select nr_item_cot_compra,
			cd_material
		from COT_COMPRA_ITEM_ACEITE
		where nr_cot_compra = nr_cot_compra_p;

cursor c02 is
		select	a.nr_sequencia,
		f.cd_cgc_fornecedor,
		f.cd_condicao_pagamento,
		f.vl_previsto_frete,
		f.qt_dias_entrega,
		a.qt_material,
		a.vl_unitario_material,
		a.vl_unitario_liquido,
		a.vl_unitario_inicial,
		a.ds_marca,
		substr(nvl(obter_se_fornec_venc_cotacao(nr_cot_compra_p,a.nr_item_cot_compra,f.nr_sequencia),'N'),1,1) ie_vencedor,
		f.ds_observacao,
		a.ds_observacao
	from	cot_compra_forn_item a,
			cot_compra_forn f
	where	f.nr_sequencia		= a.nr_seq_cot_forn
	and	a.nr_cot_compra 	= nr_cot_compra_p
	and	a.nr_item_cot_compra 	= nr_item_cot_compra_w
	and	a.cd_material		= cd_material_w
	order by substr(obter_dados_pf_pj(null, f.cd_cgc_fornecedor,'N'),1,200);

BEGIN

delete from w_cotacao_item_coluna;
delete from w_cotacao_item;

open c01;
loop
fetch c01 into
	nr_item_cot_compra_w,
	cd_material_w;
exit when c01%notfound;

	select	w_cotacao_item_seq.nextval
	into	nr_seq_cot_item_w
	from	dual;

	insert into w_cotacao_item(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_material,
		nr_item_cot_compra)
	values(	nr_seq_cot_item_w,
		sysdate,
		'Tasy',
		cd_material_w,
		nr_item_cot_compra_w);
	commit;
	
	open c02;
	loop
	fetch c02 into
		nr_seq_cot_item_forn_w,
		cd_fornecedor_w,
		cd_condicao_pagamento_w,
		vl_frete_w,
		qt_dias_entrega_w,
		qt_material_w,
		vl_unitario_material_w,
		vl_liquido_w,
		vl_unitario_inicial_w,
		ds_marca_w,
		ie_vencedor_w,
		ds_observacao_w,
		ds_observacao_item_w;
		exit when c02%notfound;

			select	w_cotacao_item_coluna_seq.nextval
			into	nr_seq_cot_col_w
			from	dual;

			select	nvl(max(nr_seq_coluna),0)
			into	nr_seq_coluna_w
			from	w_cotacao_item_coluna
			where	cd_fornecedor = cd_fornecedor_w;

			if	(nr_seq_coluna_w = 0) then
				select	nvl(max(nr_seq_coluna),0) + 1
				into	nr_seq_coluna_w
				from	w_cotacao_item_coluna;
			end if;
			
			select	nvl(sum(b.vl_tributo),0)
			into	vl_ipi_w
			from	cot_compra_forn_item_tr b,
				tributo c
			where	b.cd_tributo = c.cd_tributo
			and	c.ie_tipo_tributo = 'IPI'
			and	b.nr_seq_cot_item_forn = nr_seq_cot_item_forn_w;
			
			insert into w_cotacao_item_coluna(
				nr_sequencia,
				nr_seq_coluna,
				dt_atualizacao,
				nm_usuario,
				nr_seq_cotacao_item,
				cd_fornecedor,
				qt_fornecedor,
				vl_fornecedor,
				vl_liquido,
				vl_unitario_inicial,
				ds_marca,
				vl_ipi,
				cd_condicao_pagamento,
				vl_frete,
				qt_dias_entrega,
				ie_vencedor,
				ds_observacao,
				ds_observacao_item)
			values(	nr_seq_cot_col_w,
				nr_seq_coluna_w,
				sysdate,
				'Tasy',
				nr_seq_cot_item_w,
				cd_fornecedor_w,
				qt_material_w,
				vl_unitario_material_w,
				vl_liquido_w,
				vl_unitario_inicial_w,
				ds_marca_w,
				vl_ipi_w,
				cd_condicao_pagamento_w,
				vl_frete_w,
				qt_dias_entrega_w,
				ie_vencedor_w,
				ds_observacao_w,
				ds_observacao_item_w);
			commit;
	end loop;
	close c02;
end loop;
close c01;

end hcvsp_gerar_w_cot_item_coluna;
/