create or replace procedure	update_clinical_pathway(nr_sequencia_p   number)
as

begin

update protocolo_integrado
set IE_SITUACAO = 'E', 
    DS_JUSTIFICATIVA = null
where nr_sequencia = nr_sequencia_p;

commit;

end update_clinical_pathway;
/
