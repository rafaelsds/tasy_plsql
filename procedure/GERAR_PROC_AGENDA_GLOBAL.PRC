create or replace
procedure gerar_proc_agenda_global(	nm_usuario_p		varchar2,
				nr_sequencia_p		number,
				cd_agenda_p		number) is

nr_sequencia_w		number(10);
nr_sequencia_val_w	number(10) := 0;
nr_seq_classif_w		number(10) := 0;
					
begin

select	nr_sequencia,
	nr_seq_classif
into	nr_sequencia_w,
	nr_seq_classif_w
from	proc_interno
where	nr_sequencia = nr_sequencia_p;

select	agenda_global_seq.NextVal
into	nr_sequencia_val_w
from	dual;

insert into agenda_global
(	nr_sequencia,
	cd_agenda,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_area_proc,
	cd_especialidade,
	cd_grupo_proc,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_proc_interno,
	hr_inicio,
	hr_final,
	nr_seq_classif)
values	(nr_sequencia_val_w,
	cd_agenda_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	null,
	null,
	null,
	null,
	null,
	nr_sequencia_w,
	null,
	null,
	nr_seq_classif_w);
		
commit;

end gerar_proc_agenda_global;
/