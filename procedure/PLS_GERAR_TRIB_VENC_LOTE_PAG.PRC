create or replace
procedure pls_gerar_trib_venc_lote_pag(	nr_seq_lote_venc_p	number,
					nm_usuario_p		varchar2) is
					
cd_estabelecimento_w		number(5,0);
vl_tributo_w			number(15,2);
vl_base_calculo_w		number(15,2);
vl_minimo_tributo_w		number(15,2);
vl_base_retido_outro_w		number(15,2);
cd_tributo_w			number(10,0);
dt_emissao_w			date;
dt_vencimento_w			date;
dt_venc_titulo_w		date;
pr_aliquota_w			number(15,4);
cd_beneficiario_w		varchar2(14);
cd_cond_pagto_w			number(10,0);
ie_tipo_tributacao_w		varchar2(255);
cd_cgc_w			varchar2(14);
qt_venc_w			number(05,0);
ds_venc_w			varchar2(2000);
vl_trib_acum_w			number(15,2);
nr_seq_trans_reg_w		number(10,0);
nr_seq_trans_baixa_w		number(10,0);
cd_conta_financ_w		number(10,0);
vl_base_calculo_paga_w		number(15,2);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_prestador_w		number(10,0);
ie_vencimento_w			varchar2(01);
ie_tipo_tributo_w		varchar2(15);
ie_acumulativo_w		varchar2(1);
vl_soma_trib_nao_retido_w	number(15,2);
vl_soma_base_nao_retido_w	number(15,2);
vl_soma_trib_adic_w		number(15,2);
vl_soma_base_adic_w		number(15,2);
vl_minimo_base_w		number(15,2);
vl_trib_adic_w			number(15,2);
vl_trib_nao_retido_w		number(15,2);
vl_base_adic_w			number(15,2);
vl_base_nao_retido_w		number(15,2);
vl_teto_base_w			number(15,2);
vl_trib_anterior_w		number(15,2);
vl_vencimento_w			number(15,2);
vl_total_base_w			number(15,2);
vl_inss_w			number(15,2)	:= 0;
ie_irpf_w			varchar2(3);
ie_apuracao_piso_w		varchar2(3);
dt_tributo_w			date;
vl_reducao_w			number(15,2);
vl_desc_dependente_w		number(15,2);
qt_dependente_w			number(2);
cd_darf_w			varchar2(20);
cd_variacao_w			varchar2(50);
ie_periodicidade_w		varchar2(50);
ie_cnpj_w			varchar2(50);
cd_cnpj_raiz_w			varchar2(50);
vl_base_pago_adic_base_w	number(15,2);
cd_tributo_pf_w			varchar2(10);
vl_base_tributo_pf_w		number(15,2);
vl_tributo_pf_w			number(15,2);
cd_pessoa_fisica_pf_w		varchar2(10);
qt_registro_w			number(5);
ie_tipo_data_w			varchar2(5);
qt_imposto_mes_w		number(10);
dt_ref_tributo_w		date;
cd_tributo_ret_w		number(10);
vl_base_calculo_ret_w		number(15,2);
ie_pago_prev_lote_pag_ops_w	varchar2(5);
ds_irrelevante_w		varchar2(4000);
ie_restringe_estab_w		varchar2(1);
cd_empresa_w			number(4);
nr_seq_regra_irpf_w		regra_calculo_irpf.nr_sequencia%type;
nr_seq_tipo_prestador_w		pls_tipo_prestador.nr_sequencia%type;
nr_seq_classificacao_w		pls_prestador.nr_seq_classificacao%type;

Cursor C01 is
	select	cd_tributo,
		ie_vencimento,
		ie_tipo_tributo,
		ie_apuracao_piso,
		ie_cnpj,
		ie_restringe_estab
	from	tributo
	where	ie_conta_pagar = 'S'
	and	ie_situacao = 'A'
	and	((ie_pf_pj = 'A') or ((ie_pf_pj = 'PF') and (cd_pessoa_fisica_w is not null)) or ((ie_pf_pj = 'PJ') and (cd_cgc_w is not null)))
	and	((nvl(ie_tipo_tributacao_w, 'X') <> '0') or (nvl(ie_super_simples, 'S') = 'S'))
	and	(nvl(ie_baixa_titulo,'N') = 'N')
	order	by decode(ie_tipo_tributo, 'INSS',1,2);
	
Cursor C02 is
	select 	b.cd_tributo,
		b.vl_base_calculo,
		b.vl_tributo,
		b.cd_pessoa_fisica,
		b.ie_tipo_data,
		nvl(b.ie_pago_prev_lote_pag_ops,'R')
	from	pessoa_fisica_trib b,
		tributo	a
	where	b.cd_tributo = a.cd_tributo
	and	b.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	((b.cd_estabelecimento = cd_estabelecimento_w) or (b.cd_estabelecimento is null))
	and	decode(b.ie_tipo_data,'E',dt_emissao_w,'V',dt_venc_titulo_w) between b.dt_inicio_vigencia and dt_fim_vigencia;
	
Cursor c03 is
	select	trunc(dt_referencia,'dd'),
		cd_tributo,
		vl_base_calculo
	from	pls_prestador_tributo
	where	nr_seq_prestador = nr_seq_prestador_w
	and	trunc(dt_referencia,'month') = trunc(dt_emissao_w, 'month');
	
begin
select	a.vl_vencimento,
	b.cd_estabelecimento,
	a.dt_vencimento,
	nvl(b.dt_emissao,b.dt_mes_competencia),
	c.cd_cgc,
	c.cd_pessoa_fisica,
	b.nr_seq_prestador,
	a.dt_vencimento,
	obter_cnpj_raiz(c.cd_cgc),
	d.cd_empresa,
	c.nr_seq_tipo_prestador,
	c.nr_seq_classificacao
into	vl_vencimento_w,
	cd_estabelecimento_w,
	dt_venc_titulo_w,
	dt_emissao_w,
	cd_cgc_w,
	cd_pessoa_fisica_w,
	nr_seq_prestador_w,
	dt_tributo_w,
	cd_cnpj_raiz_w,
	cd_empresa_w,
	nr_seq_tipo_prestador_w,
	nr_seq_classificacao_w
from	estabelecimento d,
	pls_prestador c,
	pls_lote_protocolo b,
	pls_lote_protocolo_venc a
where	b.cd_estabelecimento = d.cd_estabelecimento
and	a.nr_sequencia = nr_seq_lote_venc_p
and	a.nr_seq_lote = b.nr_sequencia
and	b.nr_seq_prestador = c.nr_sequencia;

select	max(ie_tipo_tributacao)
into	ie_tipo_tributacao_w
from	pessoa_juridica
where	cd_cgc = cd_cgc_w;

open C02;
loop
fetch C02 into 
	cd_tributo_pf_w,
	vl_base_tributo_pf_w,
	vl_tributo_pf_w,
	cd_pessoa_fisica_pf_w,
	ie_tipo_Data_w,
	ie_pago_prev_lote_pag_ops_w;
exit when C02%notfound;
	begin
	select	count(*)
	into	qt_registro_w
	from	pls_lote_prot_venc_trib a,
		pls_lote_protocolo_venc b,
		pls_lote_protocolo c,
		pls_prestador d
	where	a.nr_seq_lote_venc = b.nr_sequencia
	and	b.nr_seq_lote = c.nr_sequencia
	and	c.nr_seq_prestador = d.nr_sequencia
	and	a.cd_tributo = cd_tributo_pf_w
	and	d.cd_pessoa_fisica = cd_pessoa_fisica_pf_w
	and	trunc(a.dt_imposto,'month') = trunc(dt_emissao_w,'month');
	
	if	(qt_registro_w = 0) then
		insert into pls_lote_prot_venc_trib(
				nr_sequencia, 				cd_tributo,			dt_atualizacao, 
				dt_atualizacao_nrec,			ie_pago_prev,			nm_usuario,
				nm_usuario_nrec,			nr_seq_lote_venc,		dt_imposto,
				pr_tributo,				vl_base_adic,			vl_base_calculo,
				vl_base_nao_retido, 			vl_imposto,			vl_nao_retido,
				vl_trib_adic)	
			values	(pls_lote_prot_venc_trib_seq.nextval, 	cd_tributo_pf_w,		sysdate,
				sysdate,				ie_pago_prev_lote_pag_ops_w,	nm_usuario_p,
				nm_usuario_p,				nr_seq_lote_venc_p,		trunc(decode(ie_tipo_data_w,'E',dt_emissao_w,'V',dt_venc_titulo_w),'dd'),
				0,					0,				vl_base_tributo_pf_w,
				0,					vl_tributo_pf_w,		0,
				0);
	end if;
	end;
end loop;
close C02;

/* OS 207298 -  Tratar valores retidos  do prestador */
select	count(*)
into	qt_imposto_mes_w
from	pls_prestador_tributo
where	nr_seq_prestador = nr_seq_prestador_w
and	trunc(dt_referencia,'month') = trunc(dt_emissao_w, 'month');

if 	(qt_imposto_mes_w > 0) then
	open c03;
	loop
	fetch c03 into
		dt_ref_tributo_w,
		cd_tributo_ret_w,
		vl_base_calculo_ret_w;
	exit when c03%notfound;
		begin
		insert into pls_lote_prot_venc_trib
			(nr_sequencia, 				nr_seq_lote_venc,		cd_tributo,
			ie_pago_prev, 				dt_atualizacao, 		nm_usuario,
			dt_imposto, 				vl_base_calculo, 		vl_imposto,
			vl_nao_retido, 				vl_base_nao_retido,		vl_trib_adic, 
			vl_base_adic,				pr_tributo) 
		values	(pls_lote_prot_venc_trib_seq.nextval, 	nr_seq_lote_venc_p, 		cd_tributo_ret_w,
			'R', 					sysdate,			nm_usuario_p,
			dt_ref_tributo_w, 			vl_base_calculo_ret_w, 		0,
			0, 					0,				0, 
			0,					0);
		end;
	end loop;
	close c03;
end if;

vl_trib_acum_w := 0;

open C01;
loop
fetch C01 into 
	cd_tributo_w,
	ie_vencimento_w,
	ie_tipo_tributo_w,
	ie_apuracao_piso_w,
	ie_cnpj_w,
	ie_restringe_estab_w;
exit when c01%notfound;

	vl_base_calculo_w := vl_vencimento_w;
	pr_aliquota_w := 0;
	
	obter_dados_trib_tit_pagar(	cd_tributo_w,
					cd_estabelecimento_w,
					cd_cgc_w,
					cd_pessoa_fisica_w,
					cd_beneficiario_w,
					pr_aliquota_w,
					cd_cond_pagto_w,
					cd_conta_financ_w,
					nr_seq_trans_reg_w,
					nr_seq_trans_baixa_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					ie_acumulativo_w,
					vl_teto_base_w,
					vl_desc_dependente_w,
					cd_darf_w,
					dt_tributo_w,
					cd_variacao_w,
					ie_periodicidade_w,
					null,
					null,
					null,
					null,
					null,
					null,
					ds_irrelevante_w,
					null,
					0,
					ds_irrelevante_w,
					ds_irrelevante_w,
					vl_base_calculo_w,
					'N',
					null,
					null,
					nr_seq_tipo_prestador_w,
					nr_seq_classificacao_w);
					
	if	(pr_aliquota_w > 0) then
		ie_irpf_w := 'N';
		if	(ie_tipo_tributo_w in ('IR', 'IRPF')) then -- Calcular Redu��o base IRPF e saldo menos INSS  
			if 	(cd_pessoa_fisica_w is not null) then
				ie_irpf_w := 'S';
				
				select	nvl(qt_dependente,0)
				into	qt_dependente_w
				from	pessoa_fisica
				where	cd_pessoa_fisica = cd_pessoa_fisica_w;
				
				select	nvl(sum(a.vl_imposto),0)
				into	vl_inss_w
				from	tributo b,
					pls_lote_prot_venc_trib a
				where	a.cd_tributo = b.cd_tributo
				and	b.ie_tipo_tributo = 'INSS'
				and	a.nr_seq_lote_venc = nr_seq_lote_venc_p
				and	a.ie_pago_prev = 'V';
			end if;
			
			vl_base_calculo_w := vl_base_calculo_w - vl_inss_w;
		end if;
		
		vl_tributo_w := vl_base_calculo_w * pr_aliquota_w / 100;
		
		if	(ie_vencimento_w = 'V') then
			dt_vencimento_w	:= nvl(dt_venc_titulo_w,sysdate);
		elsif	(ie_vencimento_w = 'C') then
			dt_vencimento_w	:= nvl(dt_venc_titulo_w,nvl(dt_emissao_w,sysdate));
		else
			dt_vencimento_w	:= nvl(dt_emissao_w,sysdate);
		end if;
		
		dt_tributo_w := dt_vencimento_w;
		
		if	(cd_cond_pagto_w is not null) then
			calcular_vencimento(cd_estabelecimento_w, cd_cond_pagto_w, dt_vencimento_w, qt_venc_w, ds_venc_w);
			
			if	(qt_venc_w = 1) then
				dt_vencimento_w	:= to_date(substr(ds_venc_w,1,10),'dd/mm/yyyy');
			end if;
		end if;
		
		select	/*+ USE_CONCAT */ -- Edgar 08/09/2009, OS 160898, coloquei o use_concat para transformar os OR em UNION
			nvl(sum(vl_soma_trib_nao_retido),0),
			nvl(sum(vl_soma_base_nao_retido),0),
			nvl(sum(vl_soma_trib_adic),0),
			nvl(sum(vl_soma_base_adic),0),
			nvl(sum(vl_tributo),0),
			nvl(sum(vl_total_base),0),
			nvl(sum(vl_reducao),0)
		into	vl_soma_trib_nao_retido_w,
			vl_soma_base_nao_retido_w,
			vl_soma_trib_adic_w,
			vl_soma_base_adic_w,
			vl_trib_anterior_w,
			vl_total_base_w,
			vl_reducao_w
		from	valores_tributo_v
		where	nvl(cd_empresa,nvl(cd_empresa_w,0)) = nvl(cd_empresa_w,0)
		and	cd_tributo = cd_tributo_w
		and	(ie_origem_valores = 'LP'  or ie_apuracao_piso_w = 'S')
		and	nvl(cd_pessoa_fisica, decode(ie_cnpj_w, 'Estab', cd_cgc, cd_cnpj_raiz))	= nvl(cd_pessoa_fisica_w, decode(ie_cnpj_w, 'Estab', cd_cgc_w, cd_cnpj_raiz_w))
		and	trunc(dt_tributo, 'month') = trunc(dt_tributo_w, 'month')
		and	((ie_restringe_estab_w = 'N') or (cd_estabelecimento = cd_estabelecimento_w) or (cd_estab_financeiro = cd_estabelecimento_w))
		and	((ie_apuracao_piso_w = 'N') or (ie_apuracao_piso_w = ie_base_calculo))
		and	ie_baixa_titulo	= 'N';
		
		select	nvl(sum(vl_base_calculo),0)
		into	vl_base_calculo_paga_w
		from	pls_lote_prot_venc_trib
		where	nr_seq_lote_venc = nr_seq_lote_venc_p
		and	cd_tributo = cd_tributo_w
		and	ie_pago_prev = 'P';
		
		select	nvl(sum(vl_base_calculo),0)
		into	vl_base_retido_outro_w
		from	pls_lote_prot_venc_trib
		where	nr_seq_lote_venc = nr_seq_lote_venc_p
		and	cd_tributo = cd_tributo_w
		and	ie_pago_prev = 'R';
		
		select	nvl(sum(vl_base_calculo),0)
		into	vl_base_pago_adic_base_w
		from	pls_lote_prot_venc_trib
		where	nr_seq_lote_venc = nr_seq_lote_venc_p
		and	cd_tributo = cd_tributo_w
		and	ie_pago_prev = 'S';
		
		obter_valores_tributo(	ie_acumulativo_w,
					pr_aliquota_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					vl_soma_trib_nao_retido_w,
					vl_soma_trib_adic_w,
					vl_soma_base_nao_retido_w,
					vl_soma_base_adic_w,
					vl_base_calculo_w,
					vl_tributo_w,
					vl_trib_nao_retido_w,
					vl_trib_adic_w,
					vl_base_nao_retido_w,
					vl_base_adic_w,
					vl_teto_base_w,
					vl_trib_anterior_w,
					ie_irpf_w,
					vl_total_base_w,
					vl_reducao_w,
					vl_desc_dependente_w,
					qt_dependente_w,
					vl_base_calculo_paga_w,
					vl_base_pago_adic_base_w,
					vl_base_retido_outro_w,
					obter_outras_reducoes_irpf(cd_pessoa_fisica_w, cd_estabelecimento_w, dt_tributo_w),
					sysdate,
					nr_seq_regra_irpf_w);
					
		if	(vl_tributo_w >= 0) then
			insert into pls_lote_prot_venc_trib
				(nr_sequencia, 				nr_seq_lote_venc, 			cd_tributo,
				dt_atualizacao, 			nm_usuario, 				dt_imposto, 
				vl_base_calculo, 			vl_imposto,				pr_tributo, 
				vl_nao_retido,				vl_base_nao_retido, 			vl_trib_adic, 
				vl_base_adic,				ie_pago_prev,				vl_reducao,
				vl_desc_base,				cd_darf,				dt_atualizacao_nrec,
				nm_usuario_nrec,			ie_periodicidade,			cd_variacao,
				nr_seq_trans_reg,			nr_seq_trans_baixa)				
			values	(pls_lote_prot_venc_trib_seq.nextval, 	nr_seq_lote_venc_p, 			cd_tributo_w,
				sysdate, 				nm_usuario_p, 				dt_vencimento_w, 
				vl_base_calculo_w, 			vl_tributo_w,				pr_aliquota_w, 
				vl_trib_nao_retido_w, 			vl_base_nao_retido_w, 			vl_trib_adic_w, 
				vl_base_adic_w,				'V',					vl_reducao_w,
				vl_desc_dependente_w,			cd_darf_w,				sysdate,
				nm_usuario_p,				ie_periodicidade_w,			cd_variacao_w,
				nr_seq_trans_reg_w,			nr_seq_trans_baixa_w);
				
			/* Francisco - 09/01/2012 - Tratar tipo de opera��o do tributo com rela��o ao vencimento */
			select	nvl(sum(decode(nvl(a.ie_soma_diminui,'D'), 'S', (nvl(vl_tributo_w,0) * - 1),'D', nvl(vl_tributo_w,0),0)),0)
			into	vl_tributo_w
			from	tributo a
			where	a.cd_tributo = cd_tributo_w;
			vl_trib_acum_w := vl_trib_acum_w + vl_tributo_w;
		end if;
	end if;
end loop;
close C01;

update	pls_lote_protocolo_venc
set	vl_liquido = vl_vencimento - vl_ir - vl_imposto_munic - vl_trib_acum_w
where	nr_sequencia = nr_seq_lote_venc_p;

end pls_gerar_trib_venc_lote_pag;
/