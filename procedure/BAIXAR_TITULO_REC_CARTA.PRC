create or replace
procedure baixar_titulo_rec_carta(	nr_seq_carta_p		carta_compromisso.nr_sequencia%type,
				nr_titulo_pf_p		titulo_receber.nr_titulo%type,
				ie_commit_p		varchar2 default 'N',
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

cd_moeda_padrao_w		parametro_contas_receber.cd_moeda_padrao%type;
cd_tipo_recebimento_w		parametro_contas_receber.cd_tipo_receb_carta%type;
nr_seq_trans_fin_w			transacao_financeira.nr_sequencia%type;
vl_carta_w			carta_compromisso.vl_editado%type;
cd_cgc_w			carta_compromisso.cd_cgc_emitente%type;
nr_titulo_gerado_w			carta_compromisso.nr_titulo_gerado%type;
nr_seq_baixa_w			titulo_receber_liq.nr_sequencia%type;

begin	

select	max(cd_tipo_receb_carta),
	max(cd_moeda_padrao)
into	cd_tipo_recebimento_w,
	cd_moeda_padrao_w
from	parametro_contas_receber
where	cd_estabelecimento = cd_estabelecimento_p;

if	(cd_tipo_recebimento_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1085872);
	/*	N�o h� tipo de recebimento padr�o cadastrado para o processo de cartas de compromisso de pagamento. 
		Verifique na fun��o "Par�metros do Contas a Receber", aba "T�tulo".*/
end if;
		
select	max(nr_seq_trans_fin)
into	nr_seq_trans_fin_w
from	tipo_recebimento
where	cd_tipo_recebimento = cd_tipo_recebimento_w;

if	(nr_seq_carta_p is not null) then
	begin
		
		select 	max(nvl(vl_editado, vl_original)),
			max(cd_cgc_emitente),
			max(nr_titulo_gerado)
		into	vl_carta_w,
			cd_cgc_w,
			nr_titulo_gerado_w
		from	carta_compromisso
		where	nr_sequencia = nr_seq_carta_p;
		
		select	nvl(max(nr_sequencia),0) + 1 
		into	nr_seq_baixa_w
		from	titulo_receber_liq
		where	nr_titulo = nr_titulo_pf_p;

		insert into titulo_receber_liq(	nr_titulo, 								
					nr_sequencia,
					dt_recebimento,
					vl_recebido,
					cd_tipo_recebimento,
					nr_seq_trans_fin,
					nr_seq_carta,
					ds_observacao,
					cd_moeda,
					ie_acao,
					vl_descontos,
					vl_rec_maior,
					vl_glosa,
					vl_juros,
					vl_multa,
					ie_lib_caixa,
					dt_atualizacao,
					nm_usuario) 
				values(	nr_titulo_pf_p,
					nr_seq_baixa_w,
					sysdate,
					vl_carta_w,
					cd_tipo_recebimento_w, 
					nr_seq_trans_fin_w,
					nr_seq_carta_p,
					expressao_pck.obter_desc_expressao(948268) || ': ' || nr_titulo_gerado_w, -- T�tulo a receber gerado para a institui��o emissora da carta
					cd_moeda_padrao_w,
					'I',
					0,
					0,
					0,
					0,
					0,
					'S',
					sysdate,
					nm_usuario_p);
		
		Atualizar_Saldo_Tit_Rec(nr_titulo_pf_p, nm_usuario_p);
		
		update 	carta_compromisso 
		set 	nr_titulo_baixa = nr_titulo_pf_p,
			nr_seq_baixa = nr_seq_baixa_w
		where 	nr_sequencia = nr_seq_carta_p;

	end;
end if;

if (nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;										
			
end baixar_titulo_rec_carta;
/