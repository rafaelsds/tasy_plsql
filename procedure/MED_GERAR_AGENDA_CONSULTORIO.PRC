create or replace
procedure med_gerar_agenda_consultorio(	nr_seq_agenda_p		number,
					cd_medico_p		varchar2,
					cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		varchar2) is

begin
if	(nr_seq_agenda_p is not null) and
	(cd_medico_p is not null) and
	(cd_pessoa_fisica_p is not null) then
	begin
	
	update	agenda_consulta
	set	cd_pessoa_fisica	= cd_pessoa_fisica_p,
		ie_necessita_contato	= 'N',		
		ie_status_agenda	= 'N',
		ie_status_paciente	= '1',
		nm_usuario_origem	= nm_usuario_p
	where	nr_sequencia		= nr_seq_agenda_p;
	
	end;	
end if;

commit;

end med_gerar_agenda_consultorio;
/