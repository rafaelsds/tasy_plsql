create or replace
procedure atualizar_dt_lib_auto_cir_hist(
		nr_seq_autor_cir_hist_p	number,
		nm_usuario_p		varchar2) is 

begin
if	(nr_seq_autor_cir_hist_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	update	autorizacao_cirurgia_hist
        set	dt_liberacao	= sysdate,
		nm_usuario_lib	= nm_usuario_p
        where	nr_sequencia	= nr_seq_autor_cir_hist_p;
	
	commit;
	
	end;
end if;
end atualizar_dt_lib_auto_cir_hist;
/