create or replace
procedure ctb_integracao_techone(	nr_lote_contabil_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 


begin

wheb_usuario_pck.set_nm_usuario(nm_usuario_p);
wheb_usuario_pck.set_cd_estabelecimento(cd_estabelecimento_p);

techone_pck.send_integration(	nr_lote_contabil_p,nm_usuario_p);

end ctb_integracao_techone;
/