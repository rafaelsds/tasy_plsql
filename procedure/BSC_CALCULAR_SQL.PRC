create or replace
procedure bsc_calcular_sql(	nr_seq_indicador_p	number,
			nr_seq_ind_inf_p	number,
			nr_seq_regra_ind_p	number,
			qt_real_p	out	number) is 

				
cd_ano_w			number(4);
cd_periodo_w			number(2);
dt_inicial_w			date;
dt_final_w			date;
dt_referencia_w			date;
ds_parametros_sql_w		varchar2(255);
ds_sql_w			varchar2(4000);
ie_periodo_w			varchar2(1);
qt_real_w			number(15,2);
cd_estabelecimento_w		number(5);
cd_empresa_w			number(5);
cd_setor_atendimento_w		number(10);
ds_procedure_w			varchar2(255);
ds_comando_w			varchar2(4000);
nm_usuario_w			varchar2(15);
BEGIN

/* Tipo de periodo do indicador */
select	ie_periodo,
	cd_setor_atendimento
into	ie_periodo_w,
	cd_setor_atendimento_w
from	bsc_indicador
where	nr_sequencia	= nr_seq_indicador_p;

/* Ano e periodo da sequencia de informacao que esta sendo caclulado a Quantidade real*/
select	cd_ano,
	cd_periodo,
	cd_estabelecimento,
	cd_empresa
into	cd_ano_w,
	cd_periodo_w,
	cd_estabelecimento_w,
	cd_empresa_w
from	bsc_ind_inf
where	nr_sequencia	= nr_seq_ind_inf_p;

/*Obter o usu�rio*/

begin
nm_usuario_w	:= wheb_usuario_pck.GET_NM_USUARIO;
exception when others then
nm_usuario_w	:= '';	
end;

/* Obter o comando SQL*/
select	max(ds_procedure),
	max(ds_sql)
into	ds_procedure_w,
	ds_sql_w
from	bsc_regra_calc_ind
where	nr_sequencia	= nr_seq_regra_ind_p;



/* Atualiza as variaveis de data do select de acordo com o per�odo que esta sendo calculado*/
if	(ie_periodo_w = 'A') then
	dt_referencia_w	:= PKG_DATE_UTILS.get_Date(cd_ano_w, 01, 01);		
elsif	(ie_periodo_w = 'M') then
	dt_referencia_w	:= PKG_DATE_UTILS.get_Date(cd_ano_w, cd_periodo_w, 01);
	dt_inicial_w	:= dt_referencia_w;
	dt_final_w	:= fim_mes(dt_referencia_w);
elsif	(ie_periodo_w = 'S') then
	dt_final_w	:= fim_dia(PKG_DATE_UTILS.END_OF(PKG_DATE_UTILS.get_Date(cd_ano_w, cd_periodo_w * 6, 01), 'MONTH', 0));
	dt_inicial_w	:= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_final_w,-5,0),'month',0);
elsif	(ie_periodo_w = 'T') then
	dt_final_w	:= fim_dia(PKG_DATE_UTILS.END_OF(PKG_DATE_UTILS.get_Date(cd_ano_w, cd_periodo_w * 3, 01), 'MONTH', 0));
	dt_inicial_w	:= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_final_w,-2,0),'month',0);
end if;

if	(ds_procedure_w is not null) then
	begin
	ds_comando_w	:= 'BEGIN' || chr(13) || chr(10) || ds_procedure_w || ';' || chr(13) || chr(10) || 'END;';
	if	((instr(upper(ds_procedure_w),':DT_REFERENCIA')) > 0) then
		ds_parametros_sql_w	:= 'dt_referencia=' || dt_referencia_w || ';';
	end if;	
	if	((instr(upper(ds_procedure_w),':DT_INICIAL')) > 0) then
		ds_parametros_sql_w	:= ds_parametros_sql_w || 'dt_inicial=' || dt_inicial_w || ';'; 
	end if;
	if	((instr(upper(ds_procedure_w),':DT_FINAL')) > 0) then
		ds_parametros_sql_w	:= ds_parametros_sql_w || 'dt_final=' || dt_final_w || ';'; 
	end if;
	if	((instr(upper(ds_procedure_w),':CD_ESTABELECIMENTO')) > 0) then
		ds_parametros_sql_w	:= ds_parametros_sql_w || 'cd_estabelecimento=' || cd_estabelecimento_w || ';'; 
	end if;
	if	((instr(upper(ds_procedure_w),':CD_EMPRESA')) > 0) then
		ds_parametros_sql_w	:= ds_parametros_sql_w || 'cd_empresa=' || cd_empresa_w || ';'; 
	end if;
	if	((instr(upper(ds_procedure_w),':CD_SETOR_ATENDIMENTO')) > 0) then
		ds_parametros_sql_w	:= ds_parametros_sql_w || 'cd_setor_atendimento=' || cd_setor_atendimento_w || ';'; 
	end if;
	if	((instr(upper(ds_procedure_w),':NM_USUARIO')) > 0) then
		ds_parametros_sql_w	:= ds_parametros_sql_w || 'nm_usuario=' || nm_usuario_w || ';'; 
	end if;
	
	exec_sql_dinamico_bv('Tasy',ds_comando_w, ds_parametros_sql_w);
	
	end;
end if;
ds_parametros_sql_w	:= '';
if	((instr(upper(ds_sql_w),':DT_REFERENCIA')) > 0) then
	ds_parametros_sql_w	:= 'dt_referencia=' || dt_referencia_w || ';';
end if;	
if	((instr(upper(ds_sql_w),':DT_INICIAL')) > 0) then
	ds_parametros_sql_w	:= ds_parametros_sql_w || 'dt_inicial=' || dt_inicial_w || ';'; 
end if;
if	((instr(upper(ds_sql_w),':DT_FINAL')) > 0) then
	ds_parametros_sql_w	:= ds_parametros_sql_w || 'dt_final=' || dt_final_w || ';'; 
end if;
if	((instr(upper(ds_sql_w),':CD_ESTABELECIMENTO')) > 0) then
	ds_parametros_sql_w	:= ds_parametros_sql_w || 'cd_estabelecimento=' || cd_estabelecimento_w || ';'; 
end if;
if	((instr(upper(ds_sql_w),':CD_EMPRESA')) > 0) then
	ds_parametros_sql_w	:= ds_parametros_sql_w || 'cd_empresa=' || cd_empresa_w || ';'; 
end if;
if	((instr(upper(ds_sql_w),':CD_SETOR_ATENDIMENTO')) > 0) then
	ds_parametros_sql_w	:= ds_parametros_sql_w || 'cd_setor_atendimento=' || cd_setor_atendimento_w || ';'; 
end if;
if	((instr(upper(ds_sql_w),':NM_USUARIO')) > 0) then
	ds_parametros_sql_w	:= ds_parametros_sql_w || 'nm_usuario=' || nm_usuario_w || ';'; 
end if;


obter_valor_dinamico_bv(ds_sql_w,ds_parametros_sql_w,qt_real_w);
qt_real_p	:= nvl(qt_real_w,0);

END bsc_calcular_sql;
/