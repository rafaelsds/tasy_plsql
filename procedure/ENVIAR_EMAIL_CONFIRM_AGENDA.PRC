create or replace
procedure enviar_email_confirm_agenda(
			ds_titulo_p		varchar2,
			ds_remetente_p		varchar2,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number) is 

ds_texto_agenda_exame_w		varchar2(2000);	
ds_texto_agenda_cons_w		varchar2(2000);
ds_texto_agenda_serv_w		varchar2(2000);				
ds_mensagem_w		varchar2(32000);
nm_paciente_w		varchar2(255);
dt_agenda_w		date;
hr_agenda_w		date;
cd_estabelecimento_w	number(4,0);
nm_medico_w		varchar2(255);
nm_medico_req_w		varchar2(255);
cd_pessoa_fisica_w	varchar2(10);	
ds_email_destino_w	varchar2(255);
ds_procedimento_w	varchar2(255);
cd_agenda_w		number(10,0);
ie_erro_w		varchar2(1);
ds_tit_email_confirm_w	varchar2(255);
ds_rem_email_confirm_w	varchar2(255);
ds_orientacao_w		varchar2(4000);
ds_orientacao_proced_w	varchar2(4000);
ie_cons_perm_env_sms_pac_w	varchar(1);
ie_perm_receb_sms_email_w	varchar(1);
nr_seq_agenda_cons_w       agenda_Consulta.nr_sequencia%TYPE;
ds_url_telemedicina_w      agenda_consulta_adic.ds_url_telemedicina%TYPE;
ds_exp_envio_conf_w 	varchar2(255);
ds_exp_agenda_w 	varchar2(255);
ds_exp_sequencia_w 	varchar2(255);
ds_exp_paciente_w 	varchar2(255);
ds_exp_data_agendamento_w 	varchar2(255);
ds_log_mov_w 		varchar2(4000);
nr_seq_agenda_exam_w       agenda_paciente.nr_sequencia%TYPE;

	
Cursor C01 is
	select	b.nm_paciente,
		b.hr_inicio,
		substr(obter_nome_medico(cd_medico_exec,'N'),1,255) nm_medico,
		trunc(hr_inicio),
		a.cd_estabelecimento,
		b.cd_pessoa_fisica,
		substr(obter_exame_agenda(b.cd_procedimento, b.ie_origem_proced, b.nr_seq_proc_interno),1,240),
		a.cd_agenda,
		substr(obter_orient_procedimento(b.cd_procedimento, b.ie_origem_proced),1,4000),
		b.nr_sequencia
	from	agenda a,
		agenda_paciente b
	where	a.cd_agenda = b.cd_agenda
	and	a.cd_tipo_agenda = 2
	and	nvl(a.ie_enviar_email_confirmacao,'N') = 'S'
	and	b.cd_pessoa_fisica is not null
	and	b.ie_status_agenda <> 'C'
	and	b.dt_agenda between 	trunc(sysdate+2) and trunc(sysdate+2) + 86399/86400
	and  	b.dt_confirmacao is null;
	

cursor c02 is	
select 	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255),
	b.dt_agenda,
	trunc(b.dt_agenda),
	a.cd_estabelecimento,
	b.cd_pessoa_fisica,
	a.cd_agenda,
	substr(Obter_Orientacao_Agenda(a.cd_agenda),1,4000),
	substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
	substr(obter_nome_pf(b.cd_medico_req),1,255),
	b.nr_sequencia
from    	agenda_consulta_v2 b, 
	agenda a,
	agenda_classif c
where   	a.cd_agenda = b.cd_agenda
and	b.ie_classif_agenda = c.cd_classificacao
and	b.cd_pessoa_fisica is not null
and	b.ie_status_agenda <> 'C'
and	nvl(a.ie_enviar_email_confirmacao,'N') = 'S'
and	nvl(c.ie_email_confirmacao,'N') = 'S'
and	b.dt_agenda between trunc(sysdate+2) and trunc(sysdate+2) + 86399/86400
and not 	a.cd_tipo_agenda = 5
and 	b.dt_confirmacao is null;


cursor c03 is	
select 	substr(obter_nome_pf(b.cd_pessoa_fisica),1,255),
	b.dt_agenda,
	trunc(b.dt_agenda),
	a.cd_estabelecimento,
	b.cd_pessoa_fisica,
	a.cd_agenda,
	substr(Obter_Orientacao_Agenda(a.cd_agenda),1,4000),
	substr(obter_nome_pf(a.cd_pessoa_fisica),1,255),
	substr(obter_nome_pf(b.cd_medico_req),1,255),
	b.nr_sequencia
from     	agenda_consulta_v2 b, 
	agenda a,
	agenda_classif c
where   	a.cd_agenda = b.cd_agenda
and	b.ie_classif_agenda = c.cd_classificacao
and	b.cd_pessoa_fisica is not null
and	b.ie_status_agenda <> 'C'
and	nvl(a.ie_enviar_email_confirmacao,'N') = 'S'
and	nvl(c.ie_email_confirmacao,'N') = 'S'
and	b.dt_agenda between trunc(sysdate+2) and trunc(sysdate+2) + 86399/86400
and 	a.cd_tipo_agenda = 5
and 	b.dt_confirmacao is null;

	
begin

ds_exp_envio_conf_w := wheb_mensagem_pck.get_texto(331605) ||' '|| Lower(wheb_mensagem_pck.get_texto(802562));
ds_exp_agenda_w := wheb_mensagem_pck.get_texto(799621);
ds_exp_sequencia_w := wheb_mensagem_pck.get_texto(795194);
ds_exp_paciente_w := wheb_mensagem_pck.get_texto(791350);
ds_exp_data_agendamento_w := wheb_mensagem_pck.get_texto(767027);

Obter_Param_Usuario(821, 395, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_cons_perm_env_sms_pac_w);
	
/* Script utilizado pelo HSC */

open C01;
loop
fetch C01 into	
	nm_paciente_w,
	hr_agenda_w,
	nm_medico_w,
	dt_agenda_w,
	cd_estabelecimento_w,
	cd_pessoa_fisica_w,
	ds_procedimento_w,
	cd_agenda_w,
	ds_orientacao_proced_w,
	nr_seq_agenda_exam_w;
exit when C01%notfound;
	begin
	
	ds_email_destino_w	:= '';
	
	if	(cd_pessoa_fisica_w is not null) then	
		select	max(b.ds_email),
			nvl(MAX(a.ie_perm_sms_email),'N')
		into	ds_email_destino_w,
			ie_perm_receb_sms_email_w
		from	pessoa_fisica a,
			compl_pessoa_fisica b
		where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
		and	b.ie_tipo_complemento 	= 1
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
	end if;
	
	if (ie_cons_perm_env_sms_pac_w <> 'E') then 
		ie_perm_receb_sms_email_w := 'S';
	end if;
	
	if	(ie_perm_receb_sms_email_w <> 'N') then

		select	max(ds_remetente_email_confirm)
		into	ds_rem_email_confirm_w
		from	agenda
		where	cd_agenda = cd_agenda_w;
		
		if	(ds_rem_email_confirm_w is null) then
			select	max(ds_rem_email_confirm)
			into	ds_rem_email_confirm_w
			from	parametro_agenda
			where	cd_estabelecimento = cd_estabelecimento_w;
		end if;
				
		select	max(ds_texto_agenda_exame),
			max(ds_tit_email_confirm)
		into	ds_texto_agenda_exame_w,
			ds_tit_email_confirm_w
		from	parametro_agenda
		where	cd_estabelecimento = cd_estabelecimento_w;

		if	(ds_texto_agenda_exame_w is not null) then
		
			
			ds_mensagem_w   := ds_texto_agenda_exame_w;
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Medico', nm_medico_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Procedimento', ds_procedimento_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Paciente', nm_paciente_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Dia', to_char(dt_agenda_w,'dd/mm/yyyy')),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Hora', to_char(hr_agenda_w,'hh24:mi')),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Orientacao_proc', ds_orientacao_proced_w),1,32000);
			ds_mensagem_w   := substr(replace_macro(ds_mensagem_w, '@URL_TELEMEDICINA', NULL), 1, 32000);
		
		end if;
		
		if	(instr(ds_email_destino_w,'@') > 0) and
			(ds_tit_email_confirm_w is not null) and
			(ds_rem_email_confirm_w is not null) then
			
			begin
			
			ds_log_mov_w := ds_exp_envio_conf_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_agenda_w || ': ' || cd_agenda_w;
			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_sequencia_w || ': ' || nr_seq_agenda_exam_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_paciente_w || ': ' || cd_pessoa_fisica_w || ' - ' || nm_paciente_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_data_agendamento_w || ': ' || hr_agenda_w;
			ds_log_mov_w := SubStr(ds_log_mov_w || ' ' || Dbms_Utility.format_call_stack,1,4000);  
  
  			INSERT INTO LOG_MOV (
  			  CD_LOG, 
  			  DS_LOG, 
  			  DT_ATUALIZACAO, 
  			  NM_USUARIO) 
  			VALUES (
  			  2195763, 
  			  ds_log_mov_w,
  			  SYSDATE,
  			  Nvl(wheb_usuario_pck.get_nm_usuario,'Tasy'));
  
  			COMMIT; 
			
			enviar_email(ds_tit_email_confirm_w, ds_mensagem_w, ds_rem_email_confirm_w, ds_email_destino_w, nm_usuario_p, 'A');
			exception
			when others then
			ie_erro_w	:= 'S';	
			end;
				
		end if;
	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nm_paciente_w,
	hr_agenda_w,
	dt_agenda_w,
	cd_estabelecimento_w,
	cd_pessoa_fisica_w,
	cd_agenda_w,
	ds_orientacao_w,
	nm_medico_w,
	nm_medico_req_w,
	nr_seq_agenda_cons_w;
exit when C02%notfound;
	begin
	
	ds_email_destino_w	:= '';
	
	if	(cd_pessoa_fisica_w is not null) then	
		select	max(b.ds_email),
			nvl(MAX(a.ie_perm_sms_email),'N')
		into	ds_email_destino_w,
			ie_perm_receb_sms_email_w
		from	pessoa_fisica a,
			compl_pessoa_fisica b
		where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
		and	b.ie_tipo_complemento 	= 1
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
	end if;
	
	if (ie_cons_perm_env_sms_pac_w <> 'E') then 
		ie_perm_receb_sms_email_w := 'S';
	end if;
	
	if	(ie_perm_receb_sms_email_w <> 'N') then
	
		select	max(ds_remetente_email_confirm)
		into	ds_rem_email_confirm_w
		from	agenda
		where	cd_agenda = cd_agenda_w;
		
		if	(ds_rem_email_confirm_w is null) then
			select	max(ds_rem_email_confirm)
			into	ds_rem_email_confirm_w
			from	parametro_agenda
			where	cd_estabelecimento = cd_estabelecimento_w;
		end if;
					
		select	max(DS_TEXTO_CONFIR_AGENDA),
			max(ds_tit_email_confirm)
		into	ds_texto_agenda_cons_w,
			ds_tit_email_confirm_w
		from	parametro_agenda
		where	cd_estabelecimento = cd_estabelecimento_w;

		if	(ds_texto_agenda_cons_w is not null) then
			SELECT MAX(ds_url_telemedicina)
			  INTO ds_url_telemedicina_w
			  FROM agenda_consulta_adic
			 WHERE nr_seq_agenda = nr_seq_agenda_cons_w;
		
			
			ds_mensagem_w   := ds_texto_agenda_cons_w;
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Medico', nm_medico_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Paciente', nm_paciente_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Dia', to_char(dt_agenda_w,'dd/mm/yyyy')),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Hora', to_char(hr_agenda_w,'hh24:mi')),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Orientacao', ds_orientacao_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Requisitante', nm_medico_req_w),1,32000);
			ds_mensagem_w   := substr(replace_macro(ds_mensagem_w, '@URL_TELEMEDICINA', ds_url_telemedicina_w), 1, 32000);
		
		end if;
		
		if	(instr(ds_email_destino_w,'@') > 0) and
			(ds_tit_email_confirm_w is not null) and
			(ds_rem_email_confirm_w is not null) and
			(ds_texto_agenda_cons_w is not null) then
			
			begin
			
			ds_log_mov_w := ds_exp_envio_conf_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_agenda_w || ': ' || cd_agenda_w;
			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_sequencia_w || ': ' || nr_seq_agenda_cons_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_paciente_w || ': ' || cd_pessoa_fisica_w || ' - ' || nm_paciente_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_data_agendamento_w || ': ' || hr_agenda_w;
			ds_log_mov_w := SubStr(ds_log_mov_w || ' ' || Dbms_Utility.format_call_stack,1,4000);  
  
  			INSERT INTO LOG_MOV (
  			  CD_LOG, 
  			  DS_LOG, 
  			  DT_ATUALIZACAO, 
  			  NM_USUARIO) 
  			VALUES (
  			  2195763, 
  			  ds_log_mov_w,
  			  SYSDATE,
  			  Nvl(wheb_usuario_pck.get_nm_usuario,'Tasy'));
  
  			COMMIT; 
			
			enviar_email(ds_tit_email_confirm_w, ds_mensagem_w, ds_rem_email_confirm_w, ds_email_destino_w, nm_usuario_p, 'A');
			Inserir_historico_envio_email(cd_agenda_w, nr_seq_agenda_cons_w, 'Tasy', cd_pessoa_fisica_w, nm_paciente_w, hr_agenda_w);
			exception
			when others then
			ie_erro_w	:= 'S';	
			end;
				
		end if;
	end if;
	
	end;
end loop;
close C02;


open C03;
loop
fetch C03 into	
	nm_paciente_w,
	hr_agenda_w,
	dt_agenda_w,
	cd_estabelecimento_w,
	cd_pessoa_fisica_w,
	cd_agenda_w,
	ds_orientacao_w,
	nm_medico_w,
	nm_medico_req_w,
	nr_seq_agenda_cons_w;
exit when C03%notfound;
	begin
	
	ds_email_destino_w	:= '';
	
	if	(cd_pessoa_fisica_w is not null) then	
		select	max(b.ds_email),
			nvl(MAX(a.ie_perm_sms_email),'N')
		into	ds_email_destino_w,
			ie_perm_receb_sms_email_w
		from	pessoa_fisica a,
			compl_pessoa_fisica b
		where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
		and	b.ie_tipo_complemento 	= 1
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
	end if;
	
	if (ie_cons_perm_env_sms_pac_w <> 'E') then 
		ie_perm_receb_sms_email_w := 'S';
	end if;
	
	if	(ie_perm_receb_sms_email_w <> 'N') then
	
		select	max(ds_remetente_email_confirm)
		into	ds_rem_email_confirm_w
		from	agenda
		where	cd_agenda = cd_agenda_w;
		
		if	(ds_rem_email_confirm_w is null) then
			select	max(ds_rem_email_confirm)
			into	ds_rem_email_confirm_w
			from	parametro_agenda
			where	cd_estabelecimento = cd_estabelecimento_w;
		end if;
					
		select	max(DS_TEXTO_CONFIR_AG_SERV),
			max(ds_tit_email_confirm)
		into	ds_texto_agenda_serv_w,
			ds_tit_email_confirm_w
		from	parametro_agenda
		where	cd_estabelecimento = cd_estabelecimento_w;

		if	(ds_texto_agenda_serv_w is not null) then
			SELECT MAX(ds_url_telemedicina)
			  INTO ds_url_telemedicina_w
			  FROM agenda_consulta_adic
			 WHERE nr_seq_agenda = nr_seq_agenda_cons_w;		
			
			ds_mensagem_w   := ds_texto_agenda_serv_w;
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Medico', nm_medico_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Paciente', nm_paciente_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Dia', to_char(dt_agenda_w,'dd/mm/yyyy')),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Hora', to_char(hr_agenda_w,'hh24:mi')),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Orientacao', ds_orientacao_w),1,32000);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@Requisitante', nm_medico_req_w),1,32000);
			ds_mensagem_w   := substr(replace_macro(ds_mensagem_w, '@URL_TELEMEDICINA', ds_url_telemedicina_w), 1, 32000);
		
		
		end if;
		
		if	(instr(ds_email_destino_w,'@') > 0) and
			(ds_tit_email_confirm_w is not null) and
			(ds_rem_email_confirm_w is not null) and
			(ds_texto_agenda_serv_w is not null) then
			
			begin
			
			ds_log_mov_w := ds_exp_envio_conf_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_agenda_w || ': ' || cd_agenda_w;
			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_sequencia_w || ': ' || nr_seq_agenda_cons_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_paciente_w || ': ' || cd_pessoa_fisica_w || ' - ' || nm_paciente_w;
  			ds_log_mov_w := ds_log_mov_w || ' - ' || ds_exp_data_agendamento_w || ': ' || hr_agenda_w;
			ds_log_mov_w := SubStr(ds_log_mov_w || ' ' || Dbms_Utility.format_call_stack,1,4000);  
  
  			INSERT INTO LOG_MOV (
  			  CD_LOG, 
  			  DS_LOG, 
  			  DT_ATUALIZACAO, 
  			  NM_USUARIO) 
  			VALUES (
  			  2195763, 
  			  ds_log_mov_w,
  			  SYSDATE,
  			  Nvl(wheb_usuario_pck.get_nm_usuario,'Tasy'));
  
  			COMMIT; 
			
			enviar_email(ds_tit_email_confirm_w, ds_mensagem_w, ds_rem_email_confirm_w, ds_email_destino_w, nm_usuario_p, 'A');
			Inserir_historico_envio_email(cd_agenda_w, nr_seq_agenda_cons_w, 'Tasy', cd_pessoa_fisica_w, nm_paciente_w, hr_agenda_w);
			exception
			when others then
			ie_erro_w	:= 'S';	
			end;
				
		end if;
	end if;
	end;
end loop;
close C03;

commit;

end Enviar_email_confirm_agenda;
/
