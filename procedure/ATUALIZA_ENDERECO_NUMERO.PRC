create or replace
procedure atualiza_endereco_numero(	cd_pessoa_fisica_p		varchar2,
				ie_tipo_endereco_p		number default 1,
				nr_endereco_p		varchar2 default '0',
				nm_usuario_p		varchar2) is 

nr_sequencia_w		number(10);
qt_existe_cadastrado_w	number(10);
nr_endereco_w		number(5);
tag_pais_w		varchar2(15);

begin

nr_endereco_w := substr(nr_endereco_p, 1, 5);

begin
	select	1
	into	qt_existe_cadastrado_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	ie_tipo_complemento = ie_tipo_endereco_p
	and	rownum = 1;
exception
	when	no_data_found then
		qt_existe_cadastrado_w := 0;
end;

select	max(ds_locale) 
into	tag_pais_w
from	user_locale 
where	nm_user = nm_usuario_p;
	
If	(qt_existe_cadastrado_w = 0) then
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	if(tag_pais_w in ('de_DE', 'de_AT'))then
		insert into compl_pessoa_fisica
			(nr_sequencia,
			cd_pessoa_fisica,
			ie_tipo_complemento,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nm_usuario,
			dt_atualizacao,
			ds_compl_end)
		values	(nr_sequencia_w,
			cd_pessoa_fisica_p,
			ie_tipo_endereco_p,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_endereco_w);
	else
		insert into compl_pessoa_fisica
			(nr_sequencia,
			cd_pessoa_fisica,
			ie_tipo_complemento,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nm_usuario,
			dt_atualizacao,
			nr_endereco)
		values	(nr_sequencia_w,
			cd_pessoa_fisica_p,
			ie_tipo_endereco_p,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_endereco_w);
	end if;
else
	if(tag_pais_w in ('de_DE', 'de_AT'))then
		update	compl_pessoa_fisica
		set	ds_compl_end = nr_endereco_w,
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	ie_tipo_complemento = ie_tipo_endereco_p;
	else
		update	compl_pessoa_fisica
		set	nr_endereco = nr_endereco_w,
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	ie_tipo_complemento = ie_tipo_endereco_p;
	end if;
end if;

commit;

end atualiza_endereco_numero;
/
