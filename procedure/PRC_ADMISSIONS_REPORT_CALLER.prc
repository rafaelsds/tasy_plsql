CREATE OR REPLACE PROCEDURE PRC_ADMISSIONS_REPORT_CALLER(
  dtinicio_p        DATE,
  dtfim_p           DATE,
  estabelecimento_p NUMBER,
  setor_p           NUMBER) IS
BEGIN
  --
  PKG_ADMISSIONS_REPORT.RUN(dtinicio_p, dtfim_p, NVL(estabelecimento_p, 0), NVL(setor_p, 0));
  --
END;
/
