create or replace
procedure pls_copiar_item_prospeccao
		(	nr_seq_item_p		number,
			cd_perfil_p		number,
			nm_usuario_p		Varchar2) is

begin

insert	into	pls_prospeccao_item_perfil
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		cd_perfil,nr_seq_item,ie_controle)
values	(	pls_solic_lead_item_perfil_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		cd_perfil_p,nr_seq_item_p,'T');

commit;

end pls_copiar_item_prospeccao;
/