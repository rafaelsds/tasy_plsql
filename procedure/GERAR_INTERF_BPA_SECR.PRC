create or replace
procedure gerar_interf_bpa_secr
		(	nr_seq_protocolo_p		number,
			nr_folha_bpa_p		number,
			nr_folha_bpi_p		number,
			nm_usuario_p		varchar2) is


nr_sequencia_w		number(10);
nr_interno_conta_w		number(10);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_pessoa_fisica_ww	varchar2(10);
dt_mesano_referencia_w	date;
qt_linhas_w		number(6)	:= 1;
qt_folha_bpa_w		number(6)	:= 0;
nm_orgao_responsavel_w	varchar2(30);
cd_orgao_responsavel_w	varchar2(6);
cd_cgc_responsavel_w	varchar2(14);
nm_orgao_destino_w	varchar2(40);
ie_orgao_destino_w		varchar2(1);
cd_cnes_hospital_w	number(7);
cd_registro_w		number(2);
dt_procedimento_w		date;
cd_cns_medico_exec_w	varchar2(15);
cd_cns_medico_exec_ww	varchar2(15)	:= '0';
cd_cbo_w		varchar2(6);
cd_cbo_ww		varchar2(6)	:= '0';
dt_atend_proc_w		date;
nr_folha_bpa_w		number(3)	:= 0;
nr_folha_bpa_con_w	number(3)	:= 0;
nr_folha_bpa_ind_w	number(3)	:= 0;
nr_linha_folha_w		number(2)	:= 0;
nr_linha_folha_con_w	number(2)	:= 0;
nr_linha_folha_ind_w	number(2)	:= 0;
cd_procedimento_w	number(10);
ie_origem_proced_w	number(10);
cd_cns_paciente_w	varchar2(15);
ie_sexo_pac_w		varchar2(1);
cd_municipio_ibge_w	varchar2(6);
cd_cid_proc_w		varchar2(4);
nr_idade_pac_w		number(3);
qt_procedimento_w		number(6);
cd_carater_atend_w	varchar2(2);
ds_autorizacao_w		varchar2(13);
nm_paciente_w		varchar2(30);
dt_nascimento_w		varchar2(10);
ie_tipo_bpa_w		varchar2(1);
qt_proc_consolidado_w	number(10)	:= 0;
qt_proc_individua_w	number(10)	:= 0;
cd_dominio_w		number(10)	:= 0;
cd_medico_executor_w	varchar2(10);
cd_raca_cor_w		varchar2(2)	:= 99;
cd_cnes_hosp_proc_w	number(7);
cd_estabelecimento_w	number(4);
cd_exec_exp_w		varchar2(10);
cd_cbo_exec_exp_w	varchar2(6);
ie_considerar_cns_ant_w	parametro_faturamento.ie_considerar_cns_ant%type;
nr_cartao_nac_sus_ant_w cnes_profissional.nr_cartao_nac_sus_ant%type;	


/* Obter dados do detalhe */
cursor c01 is
	select	sus_obter_tiporeg_proc(sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced),7, 'C', 1),
		0 nr_interno_conta,
		0 nr_atendimento,
		' ' cd_pessoa_fisica,
		c.cd_pessoa_fisica cd_pessoa_fisica_c,
		' ' cd_cns_medico_exec,
		to_date(null) dt_atendimento,
		sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced) cd_procedimento,
		7 ie_origem_proced,
		' ' cd_cns_paciente,
		' ' ie_sexo_pac,
		' ' cd_municipio_ibge,
		' ' cd_cid_proc,
		somente_numero(substr(sus_obter_idade_pac_bpa(b.cd_pessoa_fisica, sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced), 7, c.dt_procedimento, 'A'),1,3)) nr_idade_pac,
		sum(c.qt_procedimento) qt_procedimento,
		' ' cd_carater_atendimento,
		' ' ds_autorizacao,
		' ' nm_paciente,
		'' dt_nascimento,
		'C' ie_tipo_bpa,
		' ' cd_profissional,
		' ' cd_raca_cor_w,
		nvl(c.cd_cbo, sus_obter_cbo_medico(nvl(c.cd_medico_executor,c.cd_pessoa_fisica),sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced),sysdate,0)) cd_cbo,
		sus_obter_substituir_cnes(cd_cnes_hospital_w, c.cd_setor_atendimento, b.cd_estabelecimento,b.ie_tipo_atendimento)
	from	procedimento_paciente	c,
		atendimento_paciente	b,
		conta_paciente		a
	where	a.nr_seq_protocolo	= nr_seq_protocolo_p
	and	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_interno_conta	= c.nr_interno_conta
	and	c.cd_motivo_exc_conta	is null
	and     sus_obter_tiporeg_proc(sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced), 7, 'C', 1) = 1
	and	sus_consiste_proc_comp(b.nr_atendimento, a.nr_interno_conta, c.cd_procedimento) = 'S'
	group by	sus_obter_tiporeg_proc(sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced),7, 'C', 1),
		0, 0, ' ', c.cd_pessoa_fisica, ' ', 
		to_date(null), 
		sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced),
		7, ' ', ' ', ' ', ' ', 
		somente_numero(substr(sus_obter_idade_pac_bpa(b.cd_pessoa_fisica, sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced), 7, c.dt_procedimento, 'A'),1,3)),
		' ', ' ', ' ', '', 'C', ' ', ' ', 
		nvl(c.cd_cbo, sus_obter_cbo_medico(nvl(c.cd_medico_executor,c.cd_pessoa_fisica),sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced),sysdate,0)),
		sus_obter_substituir_cnes(cd_cnes_hospital_w, c.cd_setor_atendimento, b.cd_estabelecimento,b.ie_tipo_atendimento)
	union all
	select	sus_obter_tiporeg_proc(sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced), 7, 'C', 1),
		a.nr_interno_conta,
		b.nr_atendimento,
		b.cd_pessoa_fisica,
		c.cd_pessoa_fisica cd_pessoa_fisica_c,
		substr(obter_dados_pf(nvl(c.cd_medico_executor,c.cd_pessoa_fisica),'CNS'),1,15) cd_cns_medico_exec,
		c.dt_procedimento dt_atendimento,
		sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced) cd_procedimento,
		7 ie_origem_proced,
		substr(obter_dados_pf(b.cd_pessoa_fisica,'CNS'),1,15) cd_cns_paciente,
		substr(obter_sexo_pf(b.cd_pessoa_fisica, 'C'),1,1) ie_sexo_pac,
		substr(obter_compl_pf(b.cd_pessoa_fisica, 1, 'CDM'),1,6) cd_municipio_ibge,
		c.cd_doenca_cid cd_cid_proc,
		somente_numero(substr(obter_idade_pf(b.cd_pessoa_fisica, c.dt_procedimento, 'A'),1,3)) nr_idade_pac,
		c.qt_procedimento,
		nvl(d.cd_carater_atendimento,ie_carater_inter_sus) ie_carater_inter_sus,
		nvl(d.nr_bpa,' ') ds_autorizacao,
		substr(obter_nome_pf(b.cd_pessoa_fisica),1,30) nm_paciente,
		substr(obter_dados_pf(b.cd_pessoa_fisica,'DN'),1,15) dt_nascimento,
		'I' ie_tipo_bpa,
		nvl(c.cd_medico_executor, c.cd_pessoa_fisica),
		lpad(sus_obter_cor_pele(b.cd_pessoa_fisica, 'C'),2,'0') cd_raca_cor_w,
		nvl(c.cd_cbo, sus_obter_cbo_medico(nvl(c.cd_medico_executor, c.cd_pessoa_fisica),sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced),c.dt_procedimento,0)) cd_cbo,
		sus_obter_substituir_cnes(cd_cnes_hospital_w, c.cd_setor_atendimento, b.cd_estabelecimento,b.ie_tipo_atendimento)
	from	procedimento_paciente	c,
		atendimento_paciente	b,
		conta_paciente		a,
		sus_bpa_unif		d
	where	a.nr_seq_protocolo	= nr_seq_protocolo_p
	and	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_interno_conta	= c.nr_interno_conta
	and	a.nr_interno_conta	= d.nr_interno_conta(+)
	and	c.cd_motivo_exc_conta	is null
	and     sus_obter_tiporeg_proc(sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced), 7, 'C', 1) = 2
	and	sus_consiste_proc_comp(b.nr_atendimento, a.nr_interno_conta, sus_obter_conversao_amb(c.cd_procedimento, c.ie_origem_proced)) = 'S'
	order by cd_cns_medico_exec, cd_procedimento, cd_cbo, nr_idade_pac;


begin

/* Limpar tabela */
delete	from w_susbpa_interf
where	nr_seq_protocolo	= nr_seq_protocolo_p;

/* Obter dados do cabe�alho */
select	a.dt_mesano_referencia,
	substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,30),
	b.cd_orgao_responsavel,
	substr(obter_cgc_estabelecimento(b.cd_estabelecimento),1,14),
	b.nm_orgao_destino,
	b.ie_orgao_destino,
	sus_obter_substituir_cnes(b.cd_cnes_hospital, a.cd_setor_atendimento, b.cd_estabelecimento,0),
	b.cd_estabelecimento
into	dt_mesano_referencia_w,
	nm_orgao_responsavel_w,
	cd_orgao_responsavel_w,
	cd_cgc_responsavel_w,
	nm_orgao_destino_w,
	ie_orgao_destino_w,
	cd_cnes_hospital_w,
	cd_estabelecimento_w
from	sus_parametros_bpa	b,
	protocolo_convenio	a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.cd_estabelecimento	= b.cd_estabelecimento;

/* Obter o valor do campo DOMINIO, o calculo � feito pelo MOD da soma dos c�digos dos procedimentos com suas respectivas quantidades mais 1111 */
select	mod((sum(c.cd_procedimento) + sum(c.qt_procedimento)),1111) + 1111
into	cd_dominio_w
from	procedimento_paciente	c,
	atendimento_paciente	b,
	conta_paciente		a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.nr_atendimento	= b.nr_atendimento
and	a.nr_interno_conta	= c.nr_interno_conta
and	c.ie_origem_proced	= 7;

select	w_susbpa_interf_seq.nextval
into	nr_sequencia_w
from	dual;

/* Inserir o registro do cabecalho */
insert into w_susbpa_interf(
		nr_sequencia,
		nr_seq_protocolo,
		nr_interno_conta,
		nr_atendimento,
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_registro,
		dt_mesano_referencia,
		qt_linhas,
		qt_folha_bpa,
		nm_orgao_responsavel,
		cd_orgao_responsavel,
		cd_cgc_responsavel,
		nm_orgao_destino,
		ie_orgao_destino,
		ds_versao,
		ie_cabecalho,
		cd_dominio)
	values(	nr_sequencia_w,
		nr_seq_protocolo_p,
		nr_interno_conta_w,
		nr_atendimento_w,
		cd_pessoa_fisica_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'*BPA*',
		dt_mesano_referencia_w,
		qt_linhas_w,
		qt_folha_bpa_w,
		nm_orgao_responsavel_w,
		cd_orgao_responsavel_w,
		cd_cgc_responsavel_w,
		nm_orgao_destino_w,
		ie_orgao_destino_w,
		'*BPABeta4*',
		'C',
		substr(cd_dominio_w,1,4));

/* Contador de procedimentos consolidados */
select	count(b.cd_procedimento)
into	qt_proc_consolidado_w
from	sus_procedimento_registro	c,
	procedimento_paciente		b,
	conta_paciente 			a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.nr_interno_conta 	= b.nr_interno_conta
and	b.cd_procedimento	= c.cd_procedimento
and	b.ie_origem_proced	= c.ie_origem_proced
and	c.cd_registro		= 1
and	nvl(sus_obter_reg_proc_bpa(c.cd_procedimento,c.ie_origem_proced),c.cd_registro) = 1;

if	(qt_proc_consolidado_w	> 0) then
	nr_folha_bpa_con_w	:= 1;
end if;

/* Contador de procedimentos individualizado */
/*select	count(b.cd_procedimento)
into	qt_proc_individua_w
from	sus_procedimento_registro	c,
	procedimento_paciente		b,
	conta_paciente 			a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p
and	a.nr_interno_conta 	= b.nr_interno_conta
and	b.cd_procedimento	= c.cd_procedimento
and	b.ie_origem_proced	= c.ie_origem_proced
and	c.cd_registro		= 2
and	nvl(sus_obter_reg_proc_bpa(c.cd_procedimento,c.ie_origem_proced),c.cd_registro) = 2;

if	(qt_proc_individua_w	> 0) then
	nr_folha_bpa_ind_w	:= 1;
end if;
*/

if	(nr_folha_bpa_p > 0) then
	nr_folha_bpa_con_w	:= nr_folha_bpa_p + 1;
end if;
if	(nr_folha_bpi_p > 0) then
	nr_folha_bpa_ind_w 	:= nr_folha_bpi_p;
end if;

open c01;
loop
fetch c01 into
	cd_registro_w,
	nr_interno_conta_w,
	nr_atendimento_w,
	cd_pessoa_fisica_w,
	cd_pessoa_fisica_ww,
	cd_cns_medico_exec_w,
	dt_atend_proc_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_cns_paciente_w,
	ie_sexo_pac_w,
	cd_municipio_ibge_w,
	cd_cid_proc_w,
	nr_idade_pac_w,
	qt_procedimento_w,
	cd_carater_atend_w,
	ds_autorizacao_w,
	nm_paciente_w,
	dt_nascimento_w,
	ie_tipo_bpa_w,
	cd_medico_executor_w,
	cd_raca_cor_w,
	cd_cbo_w,
	cd_cnes_hosp_proc_w;
exit when c01%notfound;
	begin
	
	begin
	select	nvl(ie_considerar_cns_ant, 'N')
	into	ie_considerar_cns_ant_w
	from	parametro_faturamento
	where	cd_estabelecimento=wheb_usuario_pck.get_cd_estabelecimento;
	exception
		when others then
		ie_considerar_cns_ant_w :='N';
	end;
	
	if (ie_considerar_cns_ant_w = 'S') then
		begin
			select	nvl(b.nr_cartao_nac_sus_ant, ' ')
			into	nr_cartao_nac_sus_ant_w
			from 	cnes_profissional 	b
			where	b.cd_pessoa_fisica = nvl(cd_medico_executor_w,cd_pessoa_fisica_ww)
			and 	rownum = 1;
			exception
		when others then
			nr_cartao_nac_sus_ant_w := ' ';
		end;
		if (nr_cartao_nac_sus_ant_w <> ' ') then
			cd_cns_medico_exec_w := nr_cartao_nac_sus_ant_w;
		end if;
		
		begin
			select	nvl(b.nr_cartao_nac_sus_ant, ' ')
			into	nr_cartao_nac_sus_ant_w
			from 	cnes_profissional 	b
			where	b.cd_pessoa_fisica = cd_pessoa_fisica_w
			and 	rownum = 1;
			exception
		when others then
			nr_cartao_nac_sus_ant_w := ' ';
		end;
		if (nr_cartao_nac_sus_ant_w <> ' ') then
			cd_cns_paciente_w := nr_cartao_nac_sus_ant_w;
		end if;
	end if;
	
	/* Obter dados da BPA Unificada */
	if	(ie_tipo_bpa_w	= 'I') then
		begin
		select	nvl(nr_bpa, ''),
			nvl(cd_carater_atendimento, cd_carater_atend_w)
		into	ds_autorizacao_w,
			cd_carater_atend_w
		from	sus_bpa_unif
		where	nr_interno_conta	= nr_interno_conta_w;
		exception
			when others then
			ds_autorizacao_w	:= ds_autorizacao_w;
			cd_carater_atend_w	:= cd_carater_atend_w;
		end;

		cd_carater_atend_w	:= lpad(cd_carater_atend_w,2,'0');
	end if;

	sus_obter_exec_exp_aih(cd_procedimento_w, 7, cd_medico_executor_w, cd_cbo_w, cd_estabelecimento_w, 'N', 'S', 'N', cd_exec_exp_w, cd_cbo_exec_exp_w);

	if	(cd_cbo_exec_exp_w <> '') and
		(cd_exec_exp_w <> '') then
		begin
		cd_medico_executor_w	:= cd_exec_exp_w;
		cd_cbo_w		:= cd_cbo_exec_exp_w;
		end;
	end if;

	if	(ie_tipo_bpa_w	= 'C') then
		begin
		cd_medico_executor_w	:= ' ';
		end;
	end if;

	/* Cada folha possui 20 linhas, se atingir esse limite ou ser outro profissional BPI deve ser criada uma nova folha */
	if	(ie_tipo_bpa_w	= 'C') then
		if	(nr_linha_folha_w	= 20) then
			nr_linha_folha_con_w	:= 0;
			nr_folha_bpa_con_w	:= nr_folha_bpa_con_w + 1;
		end if;
	elsif	(ie_tipo_bpa_w	= 'I') then
		if	(nr_linha_folha_w	= 20) or
			(cd_cns_medico_exec_w	<> cd_cns_medico_exec_ww) or
			(cd_cbo_w 		<> cd_cbo_ww) then
			nr_linha_folha_ind_w	:= 0;
			nr_folha_bpa_ind_w	:= nr_folha_bpa_ind_w + 1;
		end if;
	end if;

	/* Contador de linhas do arquivo */
	qt_linhas_w		:= qt_linhas_w + 1; /* N�mero de linhas do BPA gravadas */

	/* Se o procedimento for consolidado ent�o ser� inserida as folhas e o numero da linha do consolidado se n�o do Individualizado */
	if	(ie_tipo_bpa_w	= 'C') then
		nr_linha_folha_con_w	:= nr_linha_folha_con_w + 1;
		nr_linha_folha_w	:= nr_linha_folha_con_w;
		nr_folha_bpa_w		:= nr_folha_bpa_con_w;
	elsif	(ie_tipo_bpa_w	= 'I') then
		nr_linha_folha_ind_w	:= nr_linha_folha_ind_w + 1;
		nr_linha_folha_w	:= nr_linha_folha_ind_w;
		nr_folha_bpa_w		:= nr_folha_bpa_ind_w;
	end if;

	/* Felipe - 28/02/2008 - OS 80604 - Esse c�digo � feito de 7 em 7 pacientes */
	if	(cd_procedimento_w	= 0301080160) then
		qt_procedimento_w	:= trunc(dividir(qt_procedimento_w,7));
	end if;

	select	w_susbpa_interf_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into w_susbpa_interf(
			nr_sequencia,
			nr_seq_protocolo,
			nr_interno_conta,
			nr_atendimento,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_registro,
			dt_mesano_referencia,
			qt_linhas,
			qt_folha_bpa,
			nm_orgao_responsavel,
			cd_orgao_responsavel,
			cd_cgc_responsavel,
			nm_orgao_destino,
			ie_orgao_destino,
			ds_versao,
			cd_ups,
			dt_competencia,
			cd_cns_medico_exec,
			cd_cbo,
			dt_procedimento,
			nr_folha_bpa,
			nr_linha_folha,
			cd_procedimento,
			cd_cns_paciente,
			ie_sexo_pac,
			cd_municipio_ibge,
			cd_cid_proc,
			nr_idade_pac,
			qt_procedimento,
			cd_carater_atendimento,
			ds_autorizacao,
			ds_origem_informacao,
			nm_paciente,
			dt_nascimento,
			ie_tipo_bpa,
			ie_cabecalho,
			cd_dominio,
			cd_raca_cor)
	values(		nr_sequencia_w,
			nr_seq_protocolo_p,
			nr_interno_conta_w,
			nr_atendimento_w,
			cd_pessoa_fisica_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'*BPA*',
			dt_mesano_referencia_w,
			qt_linhas_w,
			qt_folha_bpa_w,
			nm_orgao_responsavel_w,
			cd_orgao_responsavel_w,
			cd_cgc_responsavel_w,
			nm_orgao_destino_w,
			ie_orgao_destino_w,
			'*BPABeta4*',
			nvl(cd_cnes_hosp_proc_w, cd_cnes_hospital_w),
			dt_mesano_referencia_w,
			cd_cns_medico_exec_w,
			cd_cbo_w,
			dt_atend_proc_w,
			nr_folha_bpa_w,
			nr_linha_folha_w,
			cd_procedimento_w,
			cd_cns_paciente_w,
			ie_sexo_pac_w,
			cd_municipio_ibge_w,
			cd_cid_proc_w,
			to_number(nr_idade_pac_w),
			qt_procedimento_w,
			cd_carater_atend_w,
			ds_autorizacao_w,
			'BPA',
			nm_paciente_w,
			to_date(dt_nascimento_w,'dd/mm/yyyy'),
			ie_tipo_bpa_w,
			'P',
			substr(cd_dominio_w,1,4),
			cd_raca_cor_w);

	cd_cns_medico_exec_ww	:= cd_cns_medico_exec_w;
	cd_cbo_ww		:= cd_cbo_w;

	end;
end loop;
close c01;

/* Somat�rio da quantidade total de folhas do arquivo */
qt_folha_bpa_w	:= nr_folha_bpa_con_w + nr_folha_bpa_ind_w;

/* Atualizar na tabela tempor�ria a quantidade linhas e de folhas do arquivo */
update	w_susbpa_interf
set	qt_linhas		= qt_linhas_w,
	qt_folha_bpa	= qt_folha_bpa_w
where	nr_seq_protocolo	= nr_seq_protocolo_p;

commit;

end gerar_interf_bpa_secr;
/
