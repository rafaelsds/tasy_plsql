create or replace
procedure pls_pp_inserir_anexo (	nr_seq_lote_p		pls_lote_pagamento.nr_sequencia%type,
					nr_seq_prestador_p	pls_prestador.nr_sequencia%type,
					ds_titulo_p		pls_pagamento_anexo.ds_titulo%type,
					ds_arquivo_p		pls_pagamento_anexo.ds_arquivo%type,
					nm_usuario_p		usuario.nm_usuario%type,
					ie_funcao_pagamento_p	varchar2) is
					
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ie_funcao_pagamento_p
1 - OPS - Pagamentos de Producao Medica (pagamento antigo)
2 - OSP - Pagamento de prestadores (pagamento novo)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
					
nr_seq_pagamento_w	pls_pagamento_anexo.nr_seq_pagamento%type;
ds_titulo_w		pls_pagamento_anexo.ds_titulo%type;

begin
ds_titulo_w := nvl(ds_titulo_p, ds_arquivo_p);

if 	(ie_funcao_pagamento_p = '2') and 
	(ds_arquivo_p is not null) then
		
	insert into pls_pp_prest_anexo	(	nr_sequencia, nm_usuario, nm_usuario_nrec, 
						dt_atualizacao, dt_atualizacao_nrec, nr_seq_prestador, 
						nr_seq_lote, ds_arquivo, ds_titulo)
				values	(	pls_pp_prest_anexo_seq.nextval, nm_usuario_p, nm_usuario_p,
						sysdate, sysdate, nr_seq_prestador_p,
						nr_seq_lote_p, ds_arquivo_p, ds_titulo_w);			
else
	select	max(nr_sequencia)
	into	nr_seq_pagamento_w
	from	pls_pagamento_prestador
	where	nr_seq_lote		= nr_seq_lote_p
	and	nr_seq_prestador	= nr_seq_prestador_p;
	
	if	(nr_seq_pagamento_w is not null) and
		(ds_arquivo_p is not null) then	
		
		insert into pls_pagamento_anexo (	nr_sequencia, dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_pagamento,
							ds_titulo, ds_arquivo, dt_liberacao)
					values (	pls_pagamento_anexo_seq.nextval, sysdate, nm_usuario_p,
							sysdate, nm_usuario_p, nr_seq_pagamento_w,
							ds_titulo_w, ds_arquivo_p, null);		
	end if;				
end if;

commit;

end pls_pp_inserir_anexo;
/