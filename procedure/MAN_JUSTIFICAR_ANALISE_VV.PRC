create or replace
procedure man_justificar_analise_vv(
			nm_usuario_p		Varchar2,
			nr_seq_p			number,
			ds_justificativa_p	varchar2) is 

begin

update	man_ordem_serv_analise_vv
set		dt_justificativa = sysdate,
		ds_justificativa = ds_justificativa_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
where	nr_sequencia = nr_seq_p;		

commit;

end man_justificar_analise_vv;
/