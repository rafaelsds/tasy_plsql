create or replace
procedure AgeCons_importar_solic_ext_PEP (
		nr_seq_agenda_p		number,
		nr_seq_solicitacao_p		number,
		nm_usuario_p		varchar2,
		qt_nao_permitido_p  out number,
		ds_exame_bloq_p		out varchar2) is 

nr_sequencia_w		number(10,0);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
qt_procedimento_w	number(10);
nr_seq_exame_w		number(10);
ie_exame_lab_w		varchar2(1);
cd_cid_w			pedido_exame_externo.cd_doenca%type;
nr_atendimento_w	pedido_exame_externo.nr_Atendimento%type;
ie_atual_cid_w		varchar2(1) := 'N';
ie_dependente_w		varchar2(1);
cd_tipo_agenda_w	agenda.cd_tipo_agenda%type;
cd_agenda_w         agenda.cd_agenda%type;
dt_agenda_w         agenda_consulta.dt_agenda%type;
ds_mensagem_w 		ageserv_proced_permissao.ds_mensagem%type;
ie_permissao_w 		ageserv_proced_permissao.ie_permissao%type;
qt_nao_permitido_w  number(10) := 0;
nr_proc_interno_w   pedido_exame_externo_item.nr_proc_interno%type;
cd_convenio_w       agenda_consulta.cd_convenio%type;
ie_tipo_atendimento_w agenda_consulta.ie_tipo_atendimento%type;
ds_proc_exame_w		varchar2(255);
ds_exame_bloq_w		varchar2(2000) := '';

Cursor	c01 is
	select	b.cd_procedimento,
		b.ie_origem_proced,
		b.qt_exame,
		b.nr_seq_exame_lab,
		a.cd_doenca,
		nvl(a.nr_atendimento,0),
		b.nr_proc_interno,
		Obter_Desc_Exame_Lab(b.nr_seq_exame_lab,null,b.cd_procedimento,b.ie_origem_proced) ds_proc_exame
	from	pedido_exame_externo a,
			pedido_exame_externo_item b
	where	b.nr_seq_pedido = nr_seq_solicitacao_p
	and		b.cd_procedimento is not null
	and		a.nr_sequencia = b.nr_seq_pedido
	and	((b.nr_seq_exame_lab is not null and ie_exame_lab_w = 'S') or 
		 (b.nr_seq_exame_lab is null and ie_exame_lab_w = 'N') or
		 (ie_exame_lab_w = 'A'));
		 
begin
obter_param_usuario(866, 301, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_exame_lab_w);

select 	max(a.cd_tipo_agenda),
		max(a.cd_agenda),
		max(dt_agenda),
		nvl(max(b.cd_convenio),0),
		max(b.ie_tipo_atendimento)
into	cd_tipo_agenda_w,
	    cd_agenda_w,
		dt_agenda_w,
		cd_convenio_w,
		ie_tipo_atendimento_w
from 	agenda a, 
	agenda_consulta b
where 	a.cd_agenda = b.cd_agenda
and 	b.nr_sequencia = nr_seq_agenda_p;

open c01;
loop
fetch 	c01 into 
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_procedimento_w,
	nr_seq_exame_w,
	cd_cid_w,
	nr_atendimento_w,
	nr_proc_interno_w,
	ds_proc_exame_w;
exit 	when c01%notfound;
	begin
	
	if (nvl(nr_proc_interno_w, 0) > 0) then
		Obter_Proc_Tab_Inter_Agenda(nr_proc_interno_w,
									0,
									cd_convenio_w,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									ie_tipo_atendimento_w,
									null,
									null,
									null,
									cd_procedimento_w,
									ie_origem_proced_w);
	end if;
	
	ageserv_consiste_perm_proced(cd_agenda_w, nr_seq_exame_w, nr_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, 'S', nm_usuario_p, obter_estabelecimento_ativo, dt_agenda_w, ds_mensagem_w, ie_permissao_w);
	
	if (ie_permissao_w <> 'N' or ie_permissao_w is null) then
		if	(cd_cid_w is null) and (nr_atendimento_w > 0) then
			cd_cid_w	:= nvl(Obter_Ultimo_Cid_Atend_Pac(nr_atendimento_w,'P','C',0), Obter_Ultimo_Cid_Atend_Pac(nr_atendimento_w,'S','C',0));
		end if;

		ie_dependente_w := obter_se_existe_dependente (nr_seq_agenda_p, nr_seq_exame_w, cd_procedimento_w, ie_origem_proced_w);
		
		if (ie_dependente_w = 'N') then
			insert into agenda_consulta_proc (
				nr_sequencia,
				nr_seq_agenda,
				cd_procedimento,
				ie_origem_proced,
				dt_atualizacao,
				nm_usuario,
				qt_procedimento,
				ie_executar_proc,
				nr_seq_exame,
				nr_seq_proc_interno)
			values	(
				agenda_consulta_proc_seq.nextval,
				nr_seq_agenda_p,
				cd_procedimento_w,
				ie_origem_proced_w,
				sysdate,
				nm_usuario_p,
				nvl(qt_procedimento_w,1),
				'S',
				nr_seq_exame_w,
				nr_proc_interno_w);
			
			if	(cd_cid_w is not null) and (ie_atual_cid_w = 'N') then
				update	agenda_consulta
				set		cd_cid	= cd_cid_w
				where	nr_sequencia	= nr_seq_agenda_p;
				ie_atual_cid_w	:= 'S';
			end if;
		end if;
	else
	
		if(length(ds_exame_bloq_w||cd_procedimento_w||' - '||ds_proc_exame_w||',') <= 2000) then
			ds_exame_bloq_w := ds_exame_bloq_w||cd_procedimento_w||' - '||ds_proc_exame_w||',';
		end if;
		
		qt_nao_permitido_w := qt_nao_permitido_w + 1;
	end if;
	
	exception
	when 	others then
		qt_nao_permitido_w := qt_nao_permitido_w + 1;
		nr_sequencia_w := null; 
	end;
end loop;
close c01;

gerar_dependente_proc_adic ( cd_tipo_agenda_w, nm_usuario_p, nr_seq_agenda_p );

commit;

qt_nao_permitido_p := qt_nao_permitido_w;
ds_exame_bloq_p := SUBSTR(ds_exame_bloq_w,1,LENGTH(ds_exame_bloq_w)-1);

end AgeCons_Importar_solic_ext_PEP;
/