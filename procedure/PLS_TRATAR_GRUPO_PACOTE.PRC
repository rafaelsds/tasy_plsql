create or replace
procedure pls_tratar_grupo_pacote
			(	nr_seq_grupo_p			Number,
				nr_seq_conta_p			Number,
				nr_seq_guia_p			number,
				nr_seq_requisicao_p		number,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				ie_regra_p		out	Varchar2) is 

cd_procedimento_w		Number(15);
ie_origem_proc_w		Number(10);
qt_procedimento_w		Number(10);
qt_procedimento_ww		Number(10);
qt_reg_w			number(10);
qt_pacote_w			Number(10);
nr_seq_conversao_pacote_w	Number(10);
nr_seq_grupo_proc_w		Number(10);
qt_pacote_proc_w		Number(10);
nr_seq_conta_proc_w		Number(10);
nr_seq_guia_proc_w		Number(10);
cd_proc_guia_w			Number(15);
ie_orig_proc_guia_w		Number(10);
qt_proc_grupo_w			Number(10);
nr_seq_guia_pacote_w		Number(10);
qt_proc_regra_w			Number(10);
nr_seq_req_proc_w		Number(10);
cd_proc_req_w			Number(10);
ie_orig_proc_req_w		Number(10);

Cursor C01 is
	select	a.cd_procedimento,
		a.ie_origem_proced
	from	pls_pacote_grupo_proc		a,
		pls_pacote_cad_grupo_proc	b
	where	a.nr_seq_grupo	= b.nr_sequencia
	and	b.ie_situacao	= 'A'
	and	a.ie_situacao	= 'A'
	and	b.nr_sequencia	= nr_seq_grupo_p;
	
	
Cursor C02 is
	select	nr_seq_conversao_pacote,
		nr_sequencia
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_p
	and	nr_seq_conversao_pacote is not null
	and	ie_tipo_despesa = 4;
	
/*Cursor C03 is
	select	nr_seq_conversao_pacote,
		nr_sequencia
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p
	and	nr_seq_conversao_pacote	is not null
	and	nr_seq_pacote		is not null;*/
Cursor C03 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p
	and	nr_seq_pacote		is null;

Cursor C04 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	nr_seq_pacote		is null;

begin
ie_regra_p	:= 'N';
if	(nr_seq_conta_p is not null) then
	open C01;
	loop
	fetch C01 into	
		cd_procedimento_w,
		ie_origem_proc_w;
	exit when C01%notfound;
		begin
		begin
		select	count(nr_sequencia)
		into	qt_procedimento_ww
		from	pls_conta_proc
		where	nr_seq_conta		= nr_seq_conta_p
		and	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proc_w;
		exception
		when others then
			qt_procedimento_ww	:= 0;
		end;
		
		if	(qt_procedimento_ww <> 0) then
			qt_procedimento_ww	:= 1;
		end if;
		
		qt_procedimento_w	:= nvl(qt_procedimento_w,0) + qt_procedimento_ww;
		
		select	count(1)
		into	qt_pacote_w
		from	pls_pacote_grupo_proc		a,
			pls_pacote_cad_grupo_proc	b
		where	a.nr_seq_grupo	= b.nr_sequencia
		and	b.ie_situacao	= 'A'
		and	a.ie_situacao	= 'A'
		and	b.nr_sequencia	= nr_seq_grupo_p;
		if	(qt_procedimento_w	<> 0) and
			(qt_pacote_w		<= qt_procedimento_w) then
			ie_regra_p	:= 'S';
		end if;
		end;
	end loop;
	close C01;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_conversao_pacote_w,
		nr_seq_conta_proc_w;
	exit when C02%notfound;
		begin
		select	max(nr_seq_grupo_proc)
		into	nr_seq_grupo_proc_w
		from	pls_regra_conversao_pacote
		where	nr_sequencia	= nr_seq_conversao_pacote_w;
		
		select	count(1)
		into	qt_pacote_proc_w
		from	pls_pacote_grupo_proc	a
		where	a.nr_seq_grupo	= nr_seq_grupo_proc_w
		and	exists (	select	1
					from	pls_pacote_grupo_proc	x
					where	x.nr_seq_grupo		= nr_seq_grupo_p
					and	x.cd_procedimento	= a.cd_procedimento 
					and	x.ie_origem_proced	= x.ie_origem_proced);
					
		if	(qt_pacote_proc_w	= 0) then
			delete	pls_conta_proc
			where	nr_sequencia	= nr_seq_conta_proc_w;
		end if;
		end;
	end loop;
	close C02;
elsif	(nr_seq_guia_p is not null) then
		
	select	count(1)
	into	qt_reg_w
	from	pls_pacote_grupo_proc		a,
		pls_pacote_cad_grupo_proc	b
	where	a.nr_seq_grupo		= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_grupo_p
	and	b.ie_situacao		= 'A'
	and	a.ie_situacao		= 'A'
	and	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;
	
	if	(qt_reg_w > 0) then
		open C01;
		loop
		fetch C01 into	
			cd_procedimento_w,
			ie_origem_proc_w;
		exit when C01%notfound;
			begin
			begin
			select	count(nr_sequencia)
			into	qt_procedimento_ww
			from	pls_guia_plano_proc
			where	nr_seq_guia		= nr_seq_guia_p
			and	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proc_w;
			exception
			when others then
				qt_procedimento_ww	:= 0;
			end;
			
			if	(qt_procedimento_ww <> 0) then
				qt_procedimento_ww	:= 1;
			end if;
			
			qt_procedimento_w	:= nvl(qt_procedimento_w,0) + qt_procedimento_ww;
			
			select	count(1)
			into	qt_pacote_w
			from	pls_pacote_grupo_proc		a,
				pls_pacote_cad_grupo_proc	b
			where	a.nr_seq_grupo	= b.nr_sequencia
			and	b.ie_situacao	= 'A'
			and	a.ie_situacao	= 'A'
			and	b.nr_sequencia	= nr_seq_grupo_p;
			
			if	(qt_procedimento_w	<> 0) and
				(qt_pacote_w		<= qt_procedimento_w) then
				ie_regra_p	:= 'S';
			end if;		
			end;
		end loop;
		close C01;
	
		select	count(distinct(cd_procedimento))
		into	qt_pacote_proc_w
		from	pls_guia_plano_proc	a
		where	a.nr_seq_guia	= nr_seq_guia_p
		and	nr_seq_pacote	is null
		and	exists (	select	1
					from	pls_pacote_grupo_proc	x
					where	x.nr_seq_grupo		= nr_seq_grupo_p
					and	x.cd_procedimento	= a.cd_procedimento 
					and	x.ie_origem_proced	= a.ie_origem_proced);
		
		select	count(1)
		into	qt_proc_regra_w
		from	pls_pacote_grupo_proc
		where	nr_seq_grupo		= nr_seq_grupo_p;
		
		if	(qt_pacote_proc_w	= qt_proc_regra_w) then
			-- Cursor que traz os procedimentos da guia que n�o s�o pacotes
			open C03;
			loop
			fetch C03 into	
				nr_seq_guia_proc_w,
				cd_proc_guia_w,
				ie_orig_proc_guia_w;
			exit when C03%notfound;
				begin
				select	count(1)
				into	qt_proc_grupo_w
				from	pls_pacote_grupo_proc	x
				where	x.nr_seq_grupo		= nr_seq_grupo_p
				and	x.cd_procedimento	= cd_proc_guia_w 
				and	x.ie_origem_proced	= ie_orig_proc_guia_w;
				
				if	(qt_proc_grupo_w	> 0) then
					select	max(nr_sequencia)
					into	nr_seq_guia_pacote_w
					from	pls_guia_plano_proc
					where	nr_seq_proc_princ	= nr_seq_guia_proc_w;
					
					if	(nr_seq_guia_pacote_w	is not null) then
						delete	pls_guia_plano_proc
						where	nr_sequencia	= nr_seq_guia_pacote_w;
					end if;
				end if;
				
				end;
			end loop;
			close C03;
		end if;
		
		/*open C03;
		loop
		fetch C03 into	
			nr_seq_conversao_pacote_w,
			nr_seq_guia_proc_w;
		exit when C03%notfound;
			begin
			select	max(nr_seq_grupo_proc)
			into	nr_seq_grupo_proc_w
			from	pls_regra_conversao_pacote
			where	nr_sequencia	= nr_seq_conversao_pacote_w;
			
			if	(nr_seq_grupo_proc_w	is not null) then
				select	count(1)
				into	qt_pacote_proc_w
				from	pls_pacote_grupo_proc	a
				where	a.nr_seq_grupo	= nr_seq_grupo_proc_w
				and	exists (	select	1
							from	pls_pacote_grupo_proc	x
							where	x.nr_seq_grupo		= nr_seq_grupo_p
							and	x.cd_procedimento	= a.cd_procedimento 
							and	x.ie_origem_proced	= x.ie_origem_proced);
							
				if	(qt_pacote_proc_w	= 0) then
					delete	pls_guia_plano_proc
					where	nr_sequencia	= nr_seq_guia_proc_w;
				end if;
			elsif	(nr_seq_grupo_proc_w	is null) then
				select	count(1)
				into	qt_pacote_proc_w
				from	pls_guia_plano_proc	a
				where	a.nr_seq_guia	= nr_seq_guia_p
				and	nr_seq_pacote	is null
				and	exists (	select	1
							from	pls_pacote_grupo_proc	x
							where	x.nr_seq_grupo		= nr_seq_grupo_p
							and	x.cd_procedimento	= a.cd_procedimento 
							and	x.ie_origem_proced	= a.ie_origem_proced);

				if	(qt_pacote_proc_w	= 0) then
					delete	pls_guia_plano_proc
					where	nr_sequencia	= nr_seq_guia_proc_w;
				end if;
			end if;
			end;
		end loop;
		close C03;*/
	end if;
elsif	(nr_seq_requisicao_p is not null) then
		
	select	count(1)
	into	qt_reg_w
	from	pls_pacote_grupo_proc		a,
		pls_pacote_cad_grupo_proc	b
	where	a.nr_seq_grupo		= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_grupo_p
	and	b.ie_situacao		= 'A'
	and	a.ie_situacao		= 'A'
	and	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;
	
	if	(qt_reg_w > 0) then
		open C01;
		loop
		fetch C01 into	
			cd_procedimento_w,
			ie_origem_proc_w;
		exit when C01%notfound;
			begin
			begin
			select	count(nr_sequencia)
			into	qt_procedimento_ww
			from	pls_requisicao_proc
			where	nr_seq_requisicao	= nr_seq_requisicao_p
			and	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proc_w;
			exception
			when others then
				qt_procedimento_ww	:= 0;
			end;
			
			if	(qt_procedimento_ww <> 0) then
				qt_procedimento_ww	:= 1;
			end if;
			
			qt_procedimento_w	:= nvl(qt_procedimento_w,0) + qt_procedimento_ww;
			
			select	count(1)
			into	qt_pacote_w
			from	pls_pacote_grupo_proc		a,
				pls_pacote_cad_grupo_proc	b
			where	a.nr_seq_grupo	= b.nr_sequencia
			and	b.ie_situacao	= 'A'
			and	a.ie_situacao	= 'A'
			and	b.nr_sequencia	= nr_seq_grupo_p;
			
			if	(qt_procedimento_w	<> 0) and
				(qt_pacote_w		<= qt_procedimento_w) then
				ie_regra_p	:= 'S';
			end if;		
			end;
		end loop;
		close C01;
	
		select	count(distinct(cd_procedimento))
		into	qt_pacote_proc_w
		from	pls_requisicao_proc	a
		where	a.nr_seq_requisicao	= nr_seq_requisicao_p
		and	nr_seq_pacote	is null
		and	exists (	select	1
					from	pls_pacote_grupo_proc	x
					where	x.nr_seq_grupo		= nr_seq_grupo_p
					and	x.cd_procedimento	= a.cd_procedimento 
					and	x.ie_origem_proced	= a.ie_origem_proced);
		
		select	count(1)
		into	qt_proc_regra_w
		from	pls_pacote_grupo_proc
		where	nr_seq_grupo		= nr_seq_grupo_p;
		
		if	(qt_pacote_proc_w	= qt_proc_regra_w) then
			-- Cursor que traz os procedimentos da requisi��o que n�o s�o pacotes
			open C04;
			loop
			fetch C04 into	
				nr_seq_req_proc_w,
				cd_proc_req_w,
				ie_orig_proc_req_w;
			exit when C04%notfound;
				begin
				select	count(1)
				into	qt_proc_grupo_w
				from	pls_pacote_grupo_proc	x
				where	x.nr_seq_grupo		= nr_seq_grupo_p
				and	x.cd_procedimento	= cd_proc_req_w 
				and	x.ie_origem_proced	= ie_orig_proc_req_w;
				
				if	(qt_proc_grupo_w	> 0) then
					select	max(nr_sequencia)
					into	nr_seq_guia_pacote_w
					from	pls_requisicao_proc
					where	nr_seq_proc_princ	= nr_seq_req_proc_w;
					
					if	(nr_seq_guia_pacote_w	is not null) then
						delete	pls_requisicao_proc
						where	nr_sequencia	= nr_seq_guia_pacote_w;
					end if;
				end if;
				
				end;
			end loop;
			close C04;
		end if;
	end if;
end if;

end pls_tratar_grupo_pacote;
/