create or replace
procedure delete_dashboard_cards(nr_seq_dashboard_card_p number) is

begin 
	delete from dashboard_cards where nr_seq_dashboard = nr_seq_dashboard_card_p; 
	commit;
end delete_dashboard_cards;
/