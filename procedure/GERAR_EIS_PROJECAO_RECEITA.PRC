create or replace
procedure gerar_eis_projecao_receita(
			dt_parametro_p		date,
			nm_usuario_p		varchar2) as
	
cd_empresa_w			empresa.cd_empresa%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_estabelecimento_ww		estabelecimento.cd_estabelecimento%type;
cd_conta_contabil_w		eis_projecao_receita.cd_conta_contabil%type;
cd_centro_custo_w		eis_projecao_receita.cd_centro_custo%type;
dt_mesano_referencia_w		date;
dt_parametro_ww			date;
dt_parametro_w			date;
dt_final_mes_w			date;
dt_ano_anterior_w			date;
vl_projecao_w			eis_projecao_receita.vl_projecao%type;
vl_faturado_w			eis_projecao_receita.vl_faturado%type;
vl_faturado_ant_w		eis_projecao_receita.vl_faturado_ant%type;
vl_orcado_w			eis_projecao_receita.vl_orcado%type;
ie_periodo_proj_receita_w		varchar2(1);
qt_dias_decoridos_w		number(5);
qt_dias_do_mes_w			number(5);
qt_dias_uteis_decoridos_w		number(5);
qt_dias_uteis_do_mes_w		number(5);
nr_sequencia_w			number(10,0);



-- Somente as contas de receita
cursor c01 is
select	a.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	a.cd_centro_custo,
	sum(a.vl_total)
from	ctb_grupo_conta c,
	conta_contabil b,
	eis_Resultado_v a
where	a.cd_conta_contabil	= b.cd_conta_contabil(+)
and	b.cd_grupo		= c.cd_grupo
and	c.ie_tipo		in ('R','C')
and	a.dt_receita		= dt_mesano_referencia_w
and	somente_numero(a.ie_protocolo) >= '2'
and	somente_numero(a.ie_protocolo) <= '10'
group by
	a.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	a.cd_centro_custo
order by
	a.cd_estabelecimento;

	
begin
delete from eis_projecao_receita;
commit;

Gravar_Log_Indicador(615, 'Projecao_Receita', sysdate, PKG_DATE_UTILS.start_of(dt_parametro_p, 'dd', 0), nm_usuario_p, nr_sequencia_w);
	
cd_estabelecimento_ww	:= 0;
dt_mesano_referencia_w	:= PKG_DATE_UTILS.start_of(dt_parametro_p, 'MONTH', 0);
dt_parametro_ww		:= dt_mesano_referencia_w;
dt_final_mes_w		:= PKG_DATE_UTILS.GET_DATETIME(pkg_date_utils.end_of(dt_parametro_p, 'MONTH', 0), NVL(dt_parametro_p, PKG_DATE_UTILS.GET_TIME('00:00:00')));
dt_ano_anterior_w		:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_parametro_p,'MONTH', 0), -12,0);

/*Se a atualiza��o � hoje, considero um dia a menos... devido a atualiza��o da job por exemplo.
Devido ao dia ainda n�o terminou os lan�amentos e contar este dia maios na proje��o*/
dt_parametro_w		:= PKG_DATE_UTILS.start_of(dt_parametro_p,'dd', 0);
if	(PKG_DATE_UTILS.start_of(dt_parametro_p,'dd', 0) = PKG_DATE_UTILS.start_of(sysdate,'dd', 0)) then
	dt_parametro_w	:= PKG_DATE_UTILS.start_of(dt_parametro_p - 1,'dd', 0);
end if;


qt_dias_decoridos_w	:= PKG_DATE_UTILS.extract_field('DAY', dt_parametro_w);
qt_dias_do_mes_w	:= PKG_DATE_UTILS.extract_field('DAY', dt_final_mes_w);

OPEN C01;
LOOP
FETCH C01 into
	cd_empresa_w,
	cd_estabelecimento_w,
	cd_conta_contabil_w,
	cd_centro_custo_w,
	vl_faturado_w;
EXIT WHEN C01%NOTFOUND;
	begin

	if	(cd_estabelecimento_w <> cd_estabelecimento_ww) then
		begin
		qt_dias_uteis_do_mes_w		:= 0;
		qt_dias_uteis_decoridos_w	:= 0;
		while	(dt_parametro_ww <= dt_final_mes_w) loop 
			begin
			if	(substr(obter_se_dia_util(dt_parametro_ww, cd_estabelecimento_w),1,1) = 'S') then
				qt_dias_uteis_do_mes_w	:= qt_dias_uteis_do_mes_w + 1;
				if	(dt_parametro_ww <= dt_parametro_w) then
					qt_dias_uteis_decoridos_w	:= qt_dias_uteis_decoridos_w + 1;
				end if;
			end if;	
			dt_parametro_ww	:= dt_parametro_ww + 1;
			end;
		end loop;	
		end;
	end if;
	cd_estabelecimento_ww	:= cd_estabelecimento_w;

	select	nvl(max(ie_periodo_proj_receita), 'U')
	into	ie_periodo_proj_receita_w
	from	centro_custo
	where	cd_centro_custo = cd_centro_custo_w;

	if	(ie_periodo_proj_receita_w = 'C') then
		vl_projecao_w	:= round((dividir_sem_round(vl_faturado_w, qt_dias_decoridos_w)  * qt_dias_do_mes_w), 2);
	elsif	(ie_periodo_proj_receita_w = 'U') then
		vl_projecao_w	:= round((dividir_sem_round(vl_faturado_w, qt_dias_uteis_decoridos_w) * qt_dias_uteis_do_mes_w), 2);
	end if;

	
	/*Valor orcado do mes*/
	select	nvl(sum(decode(ie_debito_credito, 'C', vl_orcado, vl_orcado * -1)),0)
	into	vl_orcado_w
	from	ctb_grupo_conta y,
		conta_contabil t,
		ctb_orcamento x
	where	x.cd_conta_contabil	= t.cd_conta_contabil
	and	x.cd_estabelecimento	= cd_estabelecimento_w
	and	x.cd_conta_contabil	= cd_conta_contabil_w
	and	x.cd_centro_custo	= cd_centro_custo_w
	and	y.cd_empresa 		= cd_empresa_w
	and	y.cd_grupo		= t.cd_grupo
	and	y.ie_tipo		in ('R','C')
	and	x.nr_seq_mes_ref	= (
		select	max(nr_sequencia)
		from	ctb_mes_ref
		where	cd_empresa	= cd_empresa_w
		and	dt_referencia	= PKG_DATE_UTILS.start_of(dt_parametro_p,'month', 0));



	/*Valor orcado do mesmo mes no ano anterior*/
	select	nvl(sum(decode(ie_debito_credito, 'C', vl_realizado, vl_realizado * -1)),0)
	into	vl_faturado_ant_w
	from	ctb_grupo_conta y,
		conta_contabil t,
		ctb_orcamento x
	where	x.cd_conta_contabil	= t.cd_conta_contabil
	and	x.cd_estabelecimento	= cd_estabelecimento_w
	and	x.cd_conta_contabil	= cd_conta_contabil_w
	and	x.cd_centro_custo	= cd_centro_custo_w
	and	y.cd_empresa 		= cd_empresa_w
	and	y.cd_grupo		= t.cd_grupo
	and	y.ie_tipo		in ('R','C')
	and	x.nr_seq_mes_ref	= (
		select	max(nr_sequencia)
		from	ctb_mes_ref
		where	cd_empresa	= cd_empresa_w
		and	dt_referencia	= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_parametro_p,-12,0),'MONTH', 0));


	insert into eis_projecao_receita(
		dt_atualizacao,
		nm_usuario,
		cd_estabelecimento,
		cd_conta_contabil,
		cd_centro_custo,
		dt_referencia,
		qt_dias,
		vl_faturado,
		vl_faturado_ant,
		vl_orcado,
		vl_projecao)
	values(	sysdate,
		nm_usuario_p,
		cd_estabelecimento_w,
		cd_conta_contabil_w,
		cd_centro_custo_w,
		dt_final_mes_w,
		decode(ie_periodo_proj_receita_w, 'C', qt_dias_decoridos_w, qt_dias_uteis_decoridos_w),
		vl_faturado_w,
		vl_faturado_ant_w,
		vl_orcado_w,
		vl_projecao_w);


	end;
end loop;
close c01;

	
Atualizar_Log_Indicador(sysdate, nr_sequencia_w);

end gerar_eis_projecao_receita;
/