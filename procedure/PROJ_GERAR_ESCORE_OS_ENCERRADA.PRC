create or replace
procedure proj_gerar_escore_OS_Encerrada
			(	nr_seq_item_p		Number,
				nr_seq_proj_p		Number,
				qt_escore_p		Number,
				nm_usuario_p		Varchar2) is 

qt_ponto_w			Number(15,0);
qt_peso_w			Number(15,0);
qt_escore_w			Number(15,0);
qt_escore_real_w		Number(15,0);
pr_escore_real_w		Number(15,0);
nr_seq_cliente_w		Number(10,0);
dt_inicio_real_w		Date;
dt_fim_real_w			Date;
cd_cnpj_w			Varchar2(14);
nr_seq_loc_w			Number(15,0);

begin

select	nr_seq_cliente,
	nvl(dt_inicio_real, dt_inicio_prev),
	nvl(dt_fim_real, sysdate)
into	nr_seq_cliente_w,
	dt_inicio_real_w,
	dt_fim_real_w
from	proj_projeto
where	nr_sequencia		= nr_seq_proj_p;

select	cd_cnpj
into	cd_cnpj_w
from	com_cliente
where	nr_sequencia		= nr_seq_cliente_w;

select	min(nr_sequencia)
into	nr_seq_loc_w
from	man_localizacao
where	cd_cnpj			= cd_cnpj_w;

select	count(*)
into	qt_peso_w
from	man_ordem_servico a
where	a.nr_seq_localizacao	= nr_seq_loc_w
and	a.dt_ordem_servico between dt_inicio_real_w and dt_fim_real_w;

select	count(*)
into	qt_ponto_w
from	man_ordem_servico a
where	a.nr_seq_localizacao	= nr_seq_loc_w
and	a.dt_ordem_servico between dt_inicio_real_w and dt_fim_real_w
and	a.ie_status_ordem		= 3
and	a.nr_seq_tipo_solucao	= 81;

if	(qt_peso_w >0)  then
	if	(qt_ponto_w > qt_peso_w) then
		qt_escore_real_w	:= 0;
	else
		select	qt_escore
		into	qt_escore_w
		from	proj_escore_item
		where	nr_sequencia	= nr_seq_item_p;
		qt_escore_real_w	:= qt_escore_w * ((qt_peso_w - qt_ponto_w) / qt_peso_w);
	end if;
	pr_escore_real_w		:= trunc(dividir(qt_escore_real_w * 100, qt_escore_p),0);
else
	qt_escore_real_w	:= qt_escore_p;
	pr_escore_real_w	:= 100;
end if;

update	proj_escore_item
set	qt_nota_maxima	= qt_peso_w,
	qt_nota		= qt_ponto_w,
	qt_escore_real	= qt_escore_real_w,
	pr_escore_real	= pr_escore_real_w
where	nr_sequencia	= nr_seq_item_p;

end proj_gerar_escore_OS_Encerrada;
/
