create or replace 
procedure bft_insert_hi_audit_log(      cd_pessoa_fisica_p		hi_audit_log.cd_pessoa_fisica%type,                                                                      
                                        	           ds_operation_p          		hi_audit_log.ds_operation%type,
                                        	           nr_message_p            		hi_audit_log.nr_message%type,
                                        	           nr_batch_p              		hi_audit_log.nr_batch%type,
                                        	           ds_version_p            		hi_audit_log.ds_version%type,
                                                          ds_service_provider_p   	hi_audit_log.ds_service_provider%type,
                                                          dt_generation_p         		hi_audit_log.dt_generation%type,
                                                          ie_data_type_p          		hi_audit_log.ie_data_type%type,
                                                          cd_estabelecimento_p    	hi_audit_log.cd_estabelecimento%type,
                                                          nm_usuario_p            		hi_audit_log.nm_usuario%type,
                                                          nr_provider_hpi_p          		hi_audit_log.nr_provider_hpi%type) is

nr_seq_person_hi_w       number(10);
nr_provider_hpi_w        hi_audit_log.nr_provider_hpi%type;

begin

if (cd_pessoa_fisica_p is not null) then

	select    max(nr_sequencia)
	into      nr_seq_person_hi_w
	from      person_ihi
	where   	cd_pessoa_fisica     = cd_pessoa_fisica_p
	and        ie_data_type     = ie_data_type_p;
	
	select 	get_person_ihi( obter_pessoa_fisica_usuario(nm_usuario_p, 'C') ,'P')
	into	nr_provider_hpi_w
	from dual;
	


                insert into hi_audit_log(
                                nr_sequencia,
                                cd_estabelecimento,
                                nm_usuario,
                                nm_usuario_nrec,
                                dt_atualizacao,
                                dt_atualizacao_nrec,
                                cd_pessoa_fisica,
                                ds_operation,
                                nr_message,
                                nr_batch,
                                ds_version,
                                ds_service_provider,
                                dt_generation,
                                ie_data_type,
                                nr_seq_person_hi,
								nr_provider_hpi)
                values( 
                                hi_audit_log_seq.nextval,
                                cd_estabelecimento_p,
                                nm_usuario_p,
                                nm_usuario_p,
                                sysdate,
                                sysdate,
                                cd_pessoa_fisica_p,                       
                                ds_operation_p,
                                nr_message_p,
                                nr_batch_p,
                                ds_version_p,
                                ds_service_provider_p,                 
                                dt_generation_p,
                                ie_data_type_p,
                                nr_seq_person_hi_w, 
                                nr_provider_hpi_w );
                commit;
end if;

end bft_insert_hi_audit_log;
/