create or replace
procedure Lab_criar_result_lab_pdf( nr_prescricao_p	varchar2,
				     nr_seq_prescr_p	varchar2,
				     ds_pdf_serial_p	long,	
				     nm_usuario_p	Varchar2,
				     ds_erro_p		out varchar2,
					 nr_seq_result_pdf_p	out number) is 

nr_seq_result_lab_w	number(10);				
nr_seq_exame_w		number(10);
ie_formato_texto_w	number(2);
nr_seq_result_pdf_w	result_laboratorio_pdf.nr_sequencia%type;
/*Semelhante a procedure: Lab_inserir_result_lab_pdf , por�m, altera o resultado do exame na tabela result_laboratorio*/
begin

select 	MAX(nr_seq_exame)
into	nr_seq_exame_w
from	prescr_procedimento
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia = nr_seq_prescr_p;

select 	MAX(nr_sequencia),
		MAX(nvl(ie_formato_texto,1))
into 	nr_seq_result_lab_w,
		ie_formato_texto_w
from 	result_laboratorio
where	nr_prescricao	= nr_prescricao_p
and		nr_seq_prescricao = nr_seq_prescr_p;


if	(nr_seq_result_lab_w is not null) then
	begin
		
	select 	result_laboratorio_pdf_seq.nextval
	into	nr_seq_result_pdf_w
	from 	dual;
	
	insert into result_laboratorio_pdf (	nr_sequencia,
						nr_prescricao, 
						dt_atualizacao, 
						nm_usuario, 
						dt_atualizacao_nrec, 
						nm_usuario_nrec, 
						nr_seq_prescricao, 
						nr_seq_resultado, 
						nr_seq_exame, 
						ds_pdf_serial		
						)
	values				   (nr_seq_result_pdf_w	,
						nr_prescricao_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_prescr_p,
						nr_seq_result_lab_w,
						nr_seq_exame_w,
						ds_pdf_serial_p
						);
						
						
	nr_seq_result_pdf_p := nr_seq_result_pdf_w;
		
	exception
		when others then
			ds_erro_p := SQLERRM(SQLCode);
		end;

end if;

commit;

end Lab_criar_result_lab_pdf;
/
