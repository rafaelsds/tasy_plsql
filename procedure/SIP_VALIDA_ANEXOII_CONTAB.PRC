create or replace
procedure sip_valida_anexoii_contab
			(	nr_seq_lote_sip_p	Number,
				nm_usuario_p		Varchar2) is 

dt_periodo_inicial_w		Date;
dt_periodo_final_w		Date;
ie_tipo_despesa_w		Varchar2(3);
cd_estrutura_sip_w		Varchar2(40);
ie_tipo_beneficiario_w		Varchar2(3);
ie_tipo_plano_w			Varchar2(3);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
vl_despesa_w			Number(15,2)	:= 0;
nr_seq_conta_w			Number(10);
nr_seq_procedimento_w		Number(10);
ie_tipo_protocolo_w		Varchar2(1);
				
Cursor C01 is
	select	b.nr_sequencia,
		c.nr_sequencia,
		a.ie_tipo_protocolo,
		cd_procedimento,
		ie_origem_proced,
		nvl(c.vl_liberado,0) vl_despesa
	from	pls_conta_proc		c,
		pls_conta		b,
		pls_protocolo_conta	a
	where	a.nr_sequencia	= b.nr_seq_protocolo
	and	b.nr_sequencia	= c.nr_seq_conta
	and	a.ie_status	= 3
	and	a.dt_mes_competencia between dt_periodo_inicial_w and fim_dia(dt_periodo_final_w)
	and	c.cd_conta_deb is null
	and	nvl(b.ie_tipo_segurado,'B')	= 'B'
	and	exists (select	1
			from	sip_anexo_ii_procedimento	x
			where	x.cd_procedimento	= c.cd_procedimento
			and	x.ie_origem_proced	= c.ie_origem_proced
			and	x.nr_seq_lote_sip	= nr_seq_lote_sip_p);

begin

delete	from pls_sip_valor_contabil
where	nr_seq_lote_sip	= nr_seq_lote_sip_p;

begin
select	dt_periodo_inicial,
	nvl(dt_periodo_final, sysdate)
into	dt_periodo_inicial_w,
	dt_periodo_final_w
from	pls_lote_sip
where	nr_sequencia	= nr_seq_lote_sip_p;	
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort( 266994, 'NR_SEQ_LOTE_SIP='||nr_seq_lote_sip_p);
end;

open C01;
loop
fetch C01 into	
	nr_seq_conta_w, 
	nr_seq_procedimento_w,
	ie_tipo_protocolo_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	vl_despesa_w;
exit when C01%notfound;
	begin
	/* Obter a estrutura SIP cadastrada para o procedimento, caso n�o encontre ent�o busca na procedure SIP_OBTER_ESTRUTURA_PROCED */
	select	nvl(max(cd_estrutura_sip),'X')
	into	cd_estrutura_sip_w
	from	sip_anexo_ii_procedimento
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w
	and	nr_seq_lote_sip		= nr_seq_lote_sip_p;
	if	(cd_estrutura_sip_w	= 'X') then
		cd_estrutura_sip_w	:= sip_obter_estrutura_proced(cd_procedimento_w, ie_origem_proced_w, 'C');
	end if;
	if	(cd_procedimento_w	= 0) then
		cd_procedimento_w	:= null;
		ie_origem_proced_w	:= null;
	end if;
	insert into pls_sip_valor_contabil(
		nr_sequencia, cd_estrutura, cd_procedimento,
		ie_origem_proced, dt_atualizacao, nm_usuario,
		vl_despesa_sip, ie_tipo_beneficiario, ie_tipo_plano,
		nr_seq_lote_sip, nr_seq_conta, nr_seq_procedimento,
		ie_tipo_protocolo)
	values(	pls_sip_valor_contabil_seq.nextval, cd_estrutura_sip_w, cd_procedimento_w,
		ie_origem_proced_w, sysdate, nm_usuario_p,
		vl_despesa_w, ie_tipo_beneficiario_w, ie_tipo_plano_w,
		nr_seq_lote_sip_p, nr_seq_conta_w, nr_seq_procedimento_w,
		ie_tipo_protocolo_w);
	end;
end loop;
close C01;

commit;

end sip_valida_anexoii_contab;
/