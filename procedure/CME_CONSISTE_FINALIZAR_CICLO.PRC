create or replace
procedure cme_consiste_finalizar_ciclo(	nr_sequencia_p		number,
					nm_usuario_p		varchar2,
					ds_erro_p	out	varchar2) is

ds_erro_w			varchar2(80) := '';
qt_tempo_esterelizacao_w	number(10);
dt_inicio_w			date;

begin

if	(nvl(nr_sequencia_p,0) > 0) then

	select	max(a.qt_tempo_esterelizacao)
	into	qt_tempo_esterelizacao_w
	from	cm_conjunto_cont b,
		cm_conjunto a
	where	a.nr_sequencia = b.nr_seq_conjunto
	and	b.nr_seq_ciclo = nr_sequencia_p;
	
	if	(qt_tempo_esterelizacao_w > 0) then
		
		select	max(dt_inicio)
		into	dt_inicio_w
		from	cm_ciclo
		where	nr_sequencia = nr_sequencia_p;
		
		if	((dt_inicio_w + (qt_tempo_esterelizacao_w / 1440)) > sysdate) then
			ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(280874) || ' ' ||  qt_tempo_esterelizacao_w || ' ' || WHEB_MENSAGEM_PCK.get_texto(280875);
		end if;
		
	end if;

end if;

ds_erro_p := ds_erro_w;

end cme_consiste_finalizar_ciclo;
/
