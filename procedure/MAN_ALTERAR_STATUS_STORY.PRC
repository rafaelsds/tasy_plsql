create or replace
procedure man_alterar_status_story(
			nr_story_p			number,
			nr_sprint_p			number,
			cd_status_p			number,
			cd_pessoa_fisica_p		number,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number,
			ds_erro_p		out	varchar2,
			nr_ordem_servico_p	out	number,
			ie_commit_p			varchar2 default 'S') is 

nr_art_w		desenv_art.nr_sequencia%type;
cd_papel_w		desenv_member.cd_papel%type;
ie_desenvolvedor_w	desenv_member.ie_desenvolvedor%type;
ie_testador_w		desenv_member.ie_testador%type;
ie_business_owner_w	desenv_member.ie_business_owner%type;
ie_scrum_master_w	desenv_member.ie_scrum_master%type;
ie_product_owner_w	desenv_member.ie_product_owner%type;
ie_rte_w		desenv_member.ie_rte%type;
cd_status_atual_w	desenv_story.cd_status%type;
nr_membro_w		desenv_member.nr_sequencia%type;
nr_team_w		desenv_team.nr_sequencia%type;
ie_usa_ordem_servico_w	desenv_team.ie_usa_ordem_servico%type;
qt_atribuicao_w		number(10);
qt_story_desenv_w	number(10);
ie_atribuido_usuario_w	varchar2(1);
nr_seq_estagio_w	desenv_status_story_os.nr_seq_estagio%type;
nr_ordem_servico_w	desenv_story_sprint.nr_ordem_servico%type;
			
begin

select	max(c.nr_sequencia)
into	nr_art_w
from	desenv_sprint a,
	desenv_release b,
	desenv_art c
where	a.nr_sequencia = nr_sprint_p
and	a.nr_release = b.nr_sequencia
and	b.nr_art = c.nr_sequencia;

select	max(cd_papel),
	nvl(max(ie_desenvolvedor),'N'),
	nvl(max(ie_testador),'N'),
	nvl(max(ie_business_owner),'N'),
	nvl(max(ie_scrum_master),'N'),
	nvl(max(ie_product_owner),'N'),
	nvl(max(ie_rte),'N'),
	max(a.nr_sequencia),
	max(b.nr_sequencia),
	max(b.ie_usa_ordem_servico)
into	cd_papel_w,
	ie_desenvolvedor_w,
	ie_testador_w,
	ie_business_owner_w,
	ie_scrum_master_w,
	ie_product_owner_w,
	ie_rte_w,
	nr_membro_w,
	nr_team_w,
	ie_usa_ordem_servico_w
from	desenv_member a,
	desenv_team b
where	a.nr_team = b.nr_sequencia
and	b.nr_art = nr_art_w
and	a.cd_pessoa_fisica = cd_pessoa_fisica_p;

select	nvl(max(cd_status),0)
into	cd_status_atual_w
from	desenv_story_sprint
where	nr_story = nr_story_p
and	nr_sprint = nr_sprint_p;

/*if	(cd_status_p > cd_status_atual_w) and
	((cd_status_p - cd_status_atual_w) > 1) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336605);--N�o � poss�vel pular nenhum est�gio.
	return;
end if;

if	(cd_status_atual_w <> 0) and
	(cd_status_p = 0) and
	((cd_papel_w not in (2,3)) and (ie_scrum_master_w <> 'S') and (ie_product_owner_w <> 'S')) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336627);--Somente Scrum Masters ou Product Owners podem retornar Stories para A Fazer.
	return;
end if;

if	(cd_status_atual_w = 2) and
	(cd_status_p = 1) and
	((cd_papel_w <> 4) and (ie_testador_w <> 'S')) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336632);--Somente Testadores podem retornar Stories para Desenvolvimento.
	return;
end if;

if	(cd_status_atual_w = 3) and
	(cd_status_p < 3) and
	((cd_papel_w <> 3) and (ie_product_owner_w <> 'S')) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336633);--Somente Product Owners podem retornar Stories quando em Aceita��o.
	return;
end if;

if	(cd_status_atual_w = 0) and
	(cd_status_p = 1) and
	((cd_papel_w not in (1,2)) and (ie_desenvolvedor_w <> 'S') and (ie_scrum_master_w <> 'S')) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336606);--Somente Desenvolvedores ou Scrum Masters podem colocar Stories em Desenvolvimento.
	return;
end if;

if	(cd_status_atual_w = 1) and
	(cd_status_p = 2) and
	((cd_papel_w not in (1,2)) and (ie_desenvolvedor_w <> 'S') and (ie_scrum_master_w <> 'S')) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336620);--Somente Desenvolvedores ou Scrum Masters podem colocar Stories em Verifica��o.
	return;
end if;

if	(cd_status_atual_w = 2) and
	(cd_status_p = 3) and
	((cd_papel_w <> 4) and (ie_testador_w <> 'S')) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336621);--Somente Testers podem colocar Stories em Aceita��o.
	return;
end if;

if	(cd_status_atual_w = 3) and
	(cd_status_p = 4) and
	((cd_papel_w <> 3) and (ie_product_owner_w <> 'S')) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336630);--Somente Product Owners podem colocar Stories em Pronto.
	return;
end if;*/

select	count(nr_sequencia),
	nvl(max(decode(nr_membro,nr_membro_w,'S',null)),'N')
into	qt_atribuicao_w,
	ie_atribuido_usuario_w
from	desenv_story_atribuicao
where	nr_story = nr_story_p;

if	(cd_status_p = 1) and
	(cd_status_atual_w = 0) and
	(qt_atribuicao_w > 0) and
	(ie_atribuido_usuario_w <> 'S') then
	ds_erro_p := wheb_mensagem_pck.get_texto(336607);--Esta Story est� atribu�da a outro usu�rio.
	return;
end if;

if	(qt_atribuicao_w = 0) then
	insert into desenv_story_atribuicao(
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_membro,
		nr_story)
	values(	desenv_story_atribuicao_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_membro_w,
		nr_story_p);
end if;

if	(cd_status_atual_w = 0) and
	(cd_status_p = 1) and
	(ie_usa_ordem_servico_w <> 'N') then
	man_gerar_os_story(nr_ordem_servico_p,nr_story_p,nr_sprint_p,nm_usuario_p,cd_estabelecimento_p,nr_art_w);
end if;

select	max(nr_seq_estagio)
into	nr_seq_estagio_w
from	desenv_status_story_os
where	nr_art = nr_art_w
and	cd_status = cd_status_p;

if	(nr_seq_estagio_w is not null) then
	select	max(nr_ordem_servico)
	into	nr_ordem_servico_w
	from	desenv_story_sprint
	where	nr_story = nr_story_p
	and	nr_sprint = nr_sprint_p;
	
	update	man_ordem_servico
	set	nr_seq_estagio = nr_seq_estagio_w
	where	nr_sequencia = nr_ordem_servico_w;
end if;

update	desenv_story_sprint
set	cd_status = cd_status_p,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_story = nr_story_p
and	nr_sprint = nr_sprint_p;

if	(nvl(ie_commit_p,'S') <> 'N') then
	commit;
end if;

end man_alterar_status_story;
/