create or replace 
procedure copiar_relatorio(	cd_estabelecimento_p	number,
				nr_seq_relatorio_p	number,
				ds_banco_p		varchar2,
				nm_usuario_p		varchar2,
				nr_seq_novo_relat_p	out number,
				ie_copia_titulo_p	varchar2 default 'S') is

nr_seq_relatorio_w		number(10,0) := 0;
nr_seq_banda_w			number(10,0) := 0;
nr_seq_banda_atual_w		number(10,0) := 0;
nr_seq_campo_w			number(10,0) := 0;
nr_seq_campo_atual_w		number(10,0) := 0;
cd_relatorio_w			number(05,0) := 0;
cd_classif_relat_w		varchar2(04) := '';
nr_seq_parametro_atual_w	number(10,0) := 0;
nr_seq_parametro_w		number(10,0) := 0;
cd_relatorio_wheb_w		number(05,0) := null;
cd_cgc_cliente_w		varchar2(15) := '';
nr_seq_banda_superior_w		number(10,0) := null;
nr_seq_apresentacao_w		number(10,0) := null;
ds_banda_w					varchar2(255) := null;
CD_CLASSIF_RELAT_WHEB_w		varchar2(50);


cursor c01 is
	select 	a.nr_sequencia,
		b.nr_seq_apresentacao,
		b.ds_banda
	from 	banda_relatorio b,
		banda_relatorio a
	where 	a.nr_seq_relatorio	= nr_seq_relatorio_p
	and	a.nr_seq_banda_superior = b.nr_sequencia (+)
	order by a.nr_seq_apresentacao;

cursor c02 is
	select	nr_sequencia
	from	banda_relat_campo
	where	nr_seq_banda = nr_seq_banda_w;

cursor c03 is
	select	nr_sequencia
	from	relatorio_parametro
	where	nr_seq_relatorio = nr_seq_relatorio_p;

begin

select	relatorio_seq.nextval
into	nr_seq_relatorio_w
from	dual;

select	a.cd_relatorio, 
	a.cd_classif_relat,
	a.cd_relatorio_wheb
into	cd_relatorio_w,
	cd_classif_relat_w,
	cd_relatorio_wheb_w
from	relatorio a
where	nr_sequencia = nr_seq_relatorio_p;

select	cd_cgc
into	cd_cgc_cliente_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

/*Coelho 18/10/2007 Manter CGC do cliente quando duplicar um relatorio da Wheb e o mesmo for C*/
if	( Upper(ds_banco_p) = '@WHEB') and 
	( cd_classif_relat_w like 'C%') then
	cd_cgc_cliente_w := null;
end if;

if	(Upper(ds_banco_p) <> '@WHEB') and (substr(cd_classif_relat_w,1,1) = 'W') then
	cd_relatorio_wheb_w	:= cd_relatorio_w;	
	CD_CLASSIF_RELAT_WHEB_w	:= cd_classif_relat_w;
end	if;

select	Obter_Menor_Codigo_Relat('C')
into	cd_relatorio_w
from	dual;

cd_classif_relat_w  := 'C' || substr(cd_classif_relat_w, 2, 3);	

insert into relatorio(
	nr_sequencia,
 	ds_titulo,
 	dt_atualizacao,
 	nm_usuario,
 	ie_borda_sup,
 	ie_borda_inf,
 	ie_borda_esq,
 	ie_borda_dir,
 	ie_orientacao,
 	ie_imprime_vazio,
 	ie_filtro_html,
 	ie_filtro_word,
 	ie_tipo_papel,
 	ie_filtro_excel,
 	ie_filtro_texto,
 	ds_cor_fundo,
 	qt_margem_sup,
 	qt_margem_inf,
 	qt_margem_esq,
 	qt_margem_dir,
 	qt_espaco_coluna,
 	qt_coluna,
 	nm_tabela,
 	ds_sql,
 	nr_seq_modulo,
 	ie_chamada_direta,
 	cd_classif_relat,
 	cd_relatorio,
 	ie_etiqueta,
 	cd_relatorio_wheb,
 	cd_cgc_cliente,
	ds_regra,
	ds_procedure,
	ie_gerar_relatorio,
	qt_altura,
	qt_largura,
	CD_CLASSIF_RELAT_WHEB)
(select 
	nr_seq_relatorio_w,
	decode(nvl(ie_copia_titulo_p,'S'),'S',substr(Obter_desc_expressao(342086) /*'Copia '*/ || nm_usuario_p || ' ' || ds_titulo,1,80),ds_titulo),
        sysdate,
        nm_usuario_p,
        ie_borda_sup,
        ie_borda_inf, 
        ie_borda_esq, 
        ie_borda_dir,
        ie_orientacao, 
        ie_imprime_vazio, 
        ie_filtro_html, 
        ie_filtro_word,
        ie_tipo_papel, 
        ie_filtro_excel, 
        ie_filtro_texto, 
        ds_cor_fundo,
        qt_margem_sup, 
        qt_margem_inf, 
        qt_margem_esq, 
        qt_margem_dir,
        qt_espaco_coluna, 
        qt_coluna, 
        nm_tabela, 
        ds_sql,
        nr_seq_modulo,
        ie_chamada_direta,
        cd_classif_relat_w, 
        cd_relatorio_w,
        ie_etiqueta,
        cd_relatorio_wheb_w,
        nvl(cd_cgc_cliente_w,cd_cgc_cliente),
	ds_regra,
	ds_procedure,
	ie_gerar_relatorio,
	qt_altura,
	qt_largura,
	nvl(CD_CLASSIF_RELAT_WHEB_w, CD_CLASSIF_RELAT_WHEB)
from	relatorio
where	nr_sequencia = nr_seq_relatorio_p);

insert into etiqueta(
	nr_seq_relatorio,       
	nr_seq_impressora,      
	dt_atualizacao,         
	nm_usuario,             
	ds_conteudo)
select	
	nr_seq_relatorio_w,       
	nr_seq_impressora,      
	sysdate,         
	nm_usuario_p,             
	ds_conteudo
from	etiqueta
where	nr_seq_relatorio	= nr_seq_relatorio_p;
	
open c01;
loop
fetch c01 into 
		nr_seq_banda_w,
		nr_seq_apresentacao_w,
		ds_banda_w;
exit when c01%notfound;
	begin
	select	banda_relatorio_seq.nextval
	into	nr_seq_banda_atual_w
	from	dual;

	select	max(nr_sequencia)
	into 	nr_seq_banda_superior_w
	from 	banda_relatorio a
	where	nr_seq_relatorio	= nr_seq_relatorio_w
        and	nr_seq_apresentacao	= nr_seq_apresentacao_w
		and ds_banda			= ds_banda_w
        and	nr_seq_apresentacao_w	is not null;

	insert into banda_relatorio
		(nr_sequencia, 
         	ie_tipo_banda, 
         	ds_banda, 
         	dt_atualizacao,
	 	nm_usuario,
         	qt_altura, 
         	ds_cor_fundo, 
         	ie_quebra_pagina,
	 	ie_reimprime_nova_pagina, 
         	ie_alterna_cor_fundo, 
         	ie_imprime_vazio,
	 	ie_imprime_primeiro, 
         	ie_borda_sup, 
         	ie_borda_inf, 
         	ie_borda_esq,
	 	ie_borda_dir, 
         	nr_seq_banda_superior, 
         	nr_seq_relatorio, 
         	nm_tabela,
	 	ds_sql, 
         	ds_expressao, 
         	nr_seq_apresentacao, 
         	ds_cor_header, 
         	ds_cor_footer,
         	ds_cor_quebra, 
         	ds_regra,
         	ie_banda_padrao,
		qt_max_registro,
		ds_observacao,
		ie_direction)
	(select 
		nr_seq_banda_atual_w, 
	 	ie_tipo_banda, 
		ds_banda, 
		sysdate,
	 	nm_usuario_p, 
		qt_altura, 
		ds_cor_fundo, 
		ie_quebra_pagina,
	 	ie_reimprime_nova_pagina, 
		ie_alterna_cor_fundo, 
		ie_imprime_vazio,
	 	ie_imprime_primeiro, 
		ie_borda_sup, 
		ie_borda_inf, 
		ie_borda_esq,
	 	ie_borda_dir, 
		nr_seq_banda_superior_w, 
		nr_seq_relatorio_w,
		nm_tabela,
	 	ds_sql,
		ds_expressao, 
		nr_seq_apresentacao, 
		ds_cor_header, 
		ds_cor_footer,
         	ds_cor_quebra, 
		ds_regra,
		ie_banda_padrao,
		qt_max_registro,
		ds_observacao,
		ie_direction
	 from banda_relatorio
	 where nr_sequencia = nr_seq_banda_w);

	open c02;
	loop
	fetch c02 into nr_seq_campo_w;
	exit when c02%notfound;
	begin

	select	banda_relat_campo_seq.nextval
	into	nr_seq_campo_atual_w
	from	dual;

	insert into banda_relat_campo
	       (nr_sequencia, 
		nr_seq_banda, 
		ds_campo, 
		nr_seq_apresentacao, 
		ie_tipo_campo,
	 	dt_atualizacao, 
		nm_usuario, 
		ie_borda_sup, 
		ie_borda_inf, 
		ie_borda_esq,
	 	ie_borda_dir, 
		ie_totalizar, 
		ie_campo_quebra, 
		ie_quebra_pagina, 
		ie_zera_apos_imprimir,
	 	ie_transparente, 
		ie_alinhamento, 
		ie_ajustar_tamanho, 
		ie_estender, 
		ie_alinhar_banda,
  	 	qt_altura, 
		qt_tamanho, 
		qt_topo,
		qt_esquerda, 
		ds_label, 
		nm_atributo, 
		ds_conteudo,
         	ds_expressao, 
		ds_mascara, 
		ds_sql, 
		ds_cor_fundo, 
		qt_tam_fonte, 
		ds_cor_fonte, 
		ds_tipo_fonte,
	 	ie_tipo_barcode, 
		ie_humano_barcode, 
		pr_altura_barcode, 
		qt_fonte_barcode, 
		ds_estilo_fonte, 
		ds_cor_label,
		ie_altera_valor,
		ds_observacao,
		ie_posicao_legenda,
		ie_metodo_estender)
	(select nr_seq_campo_atual_w, 
		nr_seq_banda_atual_w, 
		ds_campo, 
		nr_seq_apresentacao, 
		ie_tipo_campo,
	 	sysdate, 	
		nm_usuario_p, 
		ie_borda_sup, 
		ie_borda_inf, 
		ie_borda_esq,
	 	ie_borda_dir, 
		ie_totalizar, 
		ie_campo_quebra, 
		ie_quebra_pagina, 
		ie_zera_apos_imprimir,
	 	ie_transparente, 
		ie_alinhamento, 
		ie_ajustar_tamanho, 
		ie_estender, 
		ie_alinhar_banda,
  	 	qt_altura, 
		qt_tamanho, 
		qt_topo, 
		qt_esquerda, 
		ds_label, 
		nm_atributo,
		ds_conteudo,
         	ds_expressao, 
		ds_mascara, 
		ds_sql, 
		ds_cor_fundo, 
		qt_tam_fonte, 
		ds_cor_fonte, 
		ds_tipo_fonte,
	 	ie_tipo_barcode, 
		ie_humano_barcode, 
		pr_altura_barcode, 
		qt_fonte_barcode, 
		ds_estilo_fonte, 
		ds_cor_label,
		ie_altera_valor,
		ds_observacao,
		ie_posicao_legenda,
		ie_metodo_estender
	 from	banda_relat_campo
	 where	nr_sequencia = nr_seq_campo_w);

	 insert into banda_relat_campo_longo (
		   nr_sequencia, 
		   nr_seq_banda_relat_campo, 
		   dt_atualizacao, 
		   nm_usuario, 
		   dt_atualizacao_nrec, 
		   nm_usuario_nrec, 
		   ds_conteudo)
	 select banda_relat_campo_longo_seq.nextval, 
		   nr_seq_campo_atual_w, 
		   dt_atualizacao, 
		   nm_usuario, 
		   dt_atualizacao_nrec, 
		   nm_usuario_nrec, 
		   ds_conteudo
	 from  banda_relat_campo_longo
	where  nr_seq_banda_relat_campo = nr_seq_campo_w;

	end;
	end loop;
	close c02;

	end;
end loop;
close c01;

open c03;
loop
fetch c03 into 
	nr_seq_parametro_w;
exit when c03%notfound;
begin
	select	relatorio_parametro_seq.nextval
	into	nr_seq_parametro_atual_w
	from	dual;

insert into relatorio_parametro
       (nr_sequencia,
	cd_parametro,
	ds_parametro,
	ie_tipo_atributo,
	nr_seq_relatorio,
	ie_origem_informacao,
	dt_atualizacao,
	nm_usuario,
	cd_dominio,
	ds_sql,
	vl_padrao,
	ie_forma_apresent,
	qt_tamanho_campo,
	ds_valor_parametro,
	ie_forma_passagem,
	ds_alias,
	ds_mascara,
	ie_multi_linha,
	nr_seq_apresent,
	nr_seq_localizar,
	ds_observacao,
	qt_desloc_direita,
	ie_obrigatorio)
(select nr_seq_parametro_atual_w,
	cd_parametro,
	ds_parametro,
	ie_tipo_atributo,
	nr_seq_relatorio_w,
	ie_origem_informacao,
	sysdate,
	nm_usuario_p,
	cd_dominio,
	ds_sql,
	vl_padrao,
	ie_forma_apresent,
	qt_tamanho_campo,
	ds_valor_parametro,
	ie_forma_passagem,
	ds_alias,
	ds_mascara,
	ie_multi_linha,
	nr_seq_apresent,
	nr_seq_localizar,
	ds_observacao,
	qt_desloc_direita,
	ie_obrigatorio
from	relatorio_parametro
where	nr_sequencia = nr_seq_parametro_w);
end;
end loop;

close c03;
commit;

nr_seq_novo_relat_p	:= nr_seq_relatorio_w;

end copiar_relatorio;
/
