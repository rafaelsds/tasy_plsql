create or replace
procedure acertar_duplic_comprador(	cd_pessoa_origem_p	varchar2, 
					cd_pessoa_destino_p	varchar2) is

qt_comprador_w	number(10,0);

begin

if	(cd_pessoa_origem_p is not null) and
	(cd_pessoa_destino_p is not null) then

	select	count(*)
	into	qt_comprador_w
	from	comprador
	where	cd_pessoa_fisica = cd_pessoa_destino_p;

	if	(qt_comprador_w = 0)  then
		insert into comprador (
			cd_pessoa_fisica,
			nm_guerra,
			dt_atualizacao,
			nm_usuario,
			nr_telefone,
			ds_email,
			cd_estabelecimento,
			ie_situacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_fax)
		select	cd_pessoa_destino_p,
			nm_guerra,
			dt_atualizacao,
			nm_usuario,
			nr_telefone,
			ds_email,
			cd_estabelecimento,
			ie_situacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_fax
		from	comprador
		where	cd_pessoa_fisica = cd_pessoa_origem_p;

	end if;

	update	ordem_compra
	set	cd_comprador = cd_pessoa_destino_p
	where	cd_comprador = cd_pessoa_origem_p;

	update	cot_compra
	set	cd_comprador = cd_pessoa_destino_p
	where	cd_comprador = cd_pessoa_origem_p;

	update	local_estoque
	set	cd_comprador_consig = cd_pessoa_destino_p
	where	cd_comprador_consig = cd_pessoa_origem_p;

	update	autorizacao_cirurgia
	set	cd_comprador = cd_pessoa_destino_p
	where	cd_comprador = cd_pessoa_origem_p;

	update	parametro_compras
	set	cd_comprador_padrao = cd_pessoa_destino_p
	where	cd_comprador_padrao = cd_pessoa_origem_p;
	
	update	parametro_compras
	set	cd_comprador_consig = cd_pessoa_destino_p
	where	cd_comprador_consig = cd_pessoa_origem_p;

	delete	
	from	comprador
	where	cd_pessoa_fisica = cd_pessoa_origem_p;
end if;

end acertar_duplic_comprador;
/