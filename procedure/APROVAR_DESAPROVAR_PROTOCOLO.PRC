create or replace
procedure aprovar_desaprovar_protocolo(nr_seq_protocolo_p		number,
									   nm_usuario_p				varchar2,
									   ie_status_p				varchar2 default null,
									   ds_texto_comunicacao_p	varchar2 default null)
is
qt_aprovado_w			number(2,0);
ds_destino_comunic_w	varchar2(255);
ds_texto_comunic_w		varchar2(4000);
ds_subtipo_protocolo_w	varchar2(255);
ds_protocolo_w			varchar2(255);
ds_tipo_protocolo_w		varchar2(255);
ds_texto_aux_w			varchar2(255);

begin

if	(ie_status_p is null) then
	select	count(*)
	into	qt_aprovado_w
	from	protocolo_medicacao
	where	nr_seq_interna	=	nr_seq_protocolo_p
	and	dt_aprovacao is not null;	

	if	(qt_aprovado_w = 0) then
		update	protocolo_medicacao
		set	nm_usuario_aprov	=	nm_usuario_p,
			dt_aprovacao		=	sysdate
		where	nr_seq_interna		=	nr_seq_protocolo_p;
	else
		update	protocolo_medicacao
		set	nm_usuario_aprov	=	null,
			dt_aprovacao		=	null
		where	nr_seq_interna		=	nr_seq_protocolo_p;
	end if;
elsif	(ie_status_p = 'PA') then
	update	protocolo_medicacao
	set	nm_usuario_aprov	=	null,
		dt_aprovacao		=	null,
		ie_status		=	'PA'
	where	nr_seq_interna		=	nr_seq_protocolo_p;
else
	update	protocolo_medicacao
	set	nm_usuario_aprov	=	nm_usuario_p,
		dt_aprovacao		=	sysdate,
		ie_status		=	ie_status_p
	where	nr_seq_interna		=	nr_seq_protocolo_p;

	if	(ie_status_p in ('PC','NA')) and
		(ds_texto_comunicacao_p is not null) then
		select	a.nm_usuario,
			a.nm_medicacao,
			b.nm_protocolo,
			c.ds_tipo_protocolo			
		into	ds_destino_comunic_w,
			ds_subtipo_protocolo_w,
			ds_protocolo_w,
			ds_tipo_protocolo_w
		from	protocolo_medicacao a,
			protocolo b,
			tipo_protocolo c
		where	a.nr_seq_interna = nr_seq_protocolo_p
		and	b.cd_protocolo = a.cd_protocolo
		and	c.cd_tipo_protocolo = b.cd_tipo_protocolo;

		ds_texto_aux_w		:=	obter_pf_usuario(nm_usuario_p,'N');
		if	(ds_texto_aux_w is not null) then
			ds_texto_aux_w	:=	obter_desc_expressao(658562)/*'Responsável pela aprovação: '*/	||': ' 	|| ds_texto_aux_w || chr(13);
		end if;
		
		if	(ie_status_p = 'PC') then
			ds_texto_aux_w		:=	wheb_mensagem_pck.get_texto(983130)||' ' || chr(13) || chr(13) || ds_texto_aux_w;
		else
			ds_texto_aux_w		:=	wheb_mensagem_pck.get_texto(983131)||' ' || chr(13) || chr(13) || ds_texto_aux_w;
		end if;
		
		
		ds_texto_comunic_w	:= 	ds_texto_aux_w  ||
						obter_desc_expressao(299692)/*'Tipo de Protocolo: '*/	||': '	|| ds_tipo_protocolo_w || chr(13) ||
						obter_desc_expressao(328829)/*'Protocolo: '*/ 			|| ds_protocolo_w || chr(13) ||
						obter_desc_expressao(309812)/*'Sub-tipo do Protocolo: '*/ ||': ' 	|| ds_subtipo_protocolo_w || chr(13) ||
						chr(13) ||
						ds_texto_comunicacao_p;
		
		insert	into comunic_interna(	dt_comunicado,
						ds_titulo,
						ds_comunicado,
						nm_usuario,
						dt_atualizacao,
						ie_geral,
						nm_usuario_destino,
						cd_perfil,
						nr_sequencia,
						ie_gerencial,
						nr_seq_classif,
						ds_perfil_adicional,
						cd_setor_destino,
						cd_estab_destino,
						ds_setor_adicional,
						dt_liberacao,
						ds_grupo)
		values				(
						sysdate,
						decode(ie_status_p,'PC',obter_desc_expressao(799026),obter_desc_expressao(799030)),
						ds_texto_comunic_w,
						nm_usuario_p,
						sysdate,
						'N',
						ds_destino_comunic_w || ', ' || nm_usuario_p,
						null,
						comunic_interna_seq.nextval,
						'N',
						null,
						null,
						null,
						wheb_usuario_pck.get_cd_estabelecimento,
						null,
						sysdate,
						null);
		
	end if;
end if;

commit;

end aprovar_desaprovar_protocolo;
/