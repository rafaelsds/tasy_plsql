create or replace
procedure cus_gerar_ficha_tecnica(	cd_estabelecimento_p		number,
					nr_item_principal_p		varchar2,
					ds_mat_componente_p		varchar2,
					ds_proc_componente_p		varchar2,
					nr_ficha_tecnica_p	out 	number,
					nr_interno_conta_p		number,
					nm_usuario_p			Varchar2) is

nr_item_principal_w	varchar2(20);
ds_mat_componente_w	varchar2(32767);
ds_proc_componente_w	varchar2(32767);
cd_procedimento_w	number(15);
cd_material_w		number(6);
ie_origem_proced_w	number(10);
nr_ficha_tecnica_w	FICHA_TECNICA.NR_FICHA_TECNICA%Type;
nr_pos_sep_w		number(10);
nr_sequencia_w		number(15);
nr_seq_componente_w	number(15);
qt_fixa_w		number(15,4);
qt_registros_w		number(15,4);

begin
ds_mat_componente_w	:= ds_mat_componente_p;
ds_proc_componente_w := ds_proc_componente_p;
nr_item_principal_w	:=	substr(nr_item_principal_p,2,length(nr_item_principal_p));

if 	(substr(nr_item_principal_p,1,1) = 'P') then
	begin
	select	cd_procedimento,
		ie_origem_proced
	into	cd_procedimento_w,
		ie_origem_proced_w
	from	procedimento_paciente
	where	nr_sequencia	= nr_item_principal_w;
	end;
else
	begin
	select	cd_material
	into	cd_material_w
	from	material_atend_paciente
	where	nr_sequencia	= nr_item_principal_w;
	end;
end if;

select	ficha_tecnica_seq.nextval
into	nr_ficha_tecnica_w
from	dual;

if	(nvl(cd_material_w,0) != 0) and
	(nvl(cd_procedimento_w,0) = 0 ) then
	begin
	select	count(*)
	into	qt_registros_w
	from	ficha_tecnica
	where 	cd_estabelecimento 	= cd_estabelecimento_p
	and	cd_material  		= cd_material_w;
	end;
end if;

if	(nvl(cd_material_w,0) = 0) and
	(nvl(cd_procedimento_w,0) != 0 ) then
	begin
	select	count(*)
	into	qt_registros_w
	from	ficha_tecnica
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_origem_proced 	= ie_origem_proced_w
	and	cd_procedimento		= cd_procedimento_w;
	end;
end if;

if	(qt_registros_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(856944);
end if;

insert into ficha_tecnica(
	nr_ficha_tecnica,
	cd_estabelecimento,
	cd_material,
	cd_procedimento,
	ie_origem_proced,
	ds_observacao,
	dt_alteracao,
	dt_atualizacao,
	ie_calcula_conta,
	ie_situacao,
	ie_solicita_componente,
	ie_tipo_ficha,
	nm_usuario,
	nr_nivel_estrutura,
	nr_seq_proc_interno,
	cd_centro_controle,
	qt_lote_padrao,
	dt_atualizacao_nrec,
	nm_usuario_nrec)
values(	nr_ficha_tecnica_w,
	cd_estabelecimento_p,
	cd_material_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	substr(wheb_mensagem_pck.get_texto(300012, 'NR_INTERNO_CONTA_P=' || nr_interno_conta_p),1,2000),
	sysdate,
	sysdate,
	'S',
	'A',
	'N',
	'C',
	nm_usuario_p,
	1,
	null,
	null,
	1,
	sysdate,
	nm_usuario_p);
		
nr_seq_componente_w	:= 0;

while (ds_mat_componente_w is not null) loop
	begin
	nr_pos_sep_w	:= nvl(instr(ds_mat_componente_w,','),0);
	if	(nr_pos_sep_w = 0) then
		nr_sequencia_w		:= somente_numero(ds_mat_componente_w);
		ds_mat_componente_w	:= null;
	else
		nr_sequencia_w		:= somente_numero(substr(ds_mat_componente_w,1,nr_pos_sep_w -1));
		ds_mat_componente_w	:= substr(ds_mat_componente_w,nr_pos_sep_w+1,12000);
	end if;

	if	(nvl(nr_sequencia_w,0) <> 0) then

		select	cd_material,
			qt_material
		into	cd_material_w,
			qt_fixa_w
		from	material_atend_paciente
		where	nr_sequencia	= nr_sequencia_w;

		select	count(*)
		into	qt_registros_w
		from	ficha_tecnica_componente
		where	cd_material = cd_material_w
		and	nr_ficha_tecnica = nr_ficha_tecnica_w;

		if (qt_registros_w > 0) then
			select	max(nr_seq_componente)
			into	nr_seq_componente_w
			from	ficha_tecnica_componente
			where	cd_material = cd_material_w
			and	nr_ficha_tecnica = nr_ficha_tecnica_w;

			update	ficha_tecnica_componente
			set	qt_fixa = qt_fixa + qt_fixa_w
			where	nr_ficha_tecnica =  nr_ficha_tecnica_w
			and	nr_seq_componente = nr_seq_componente_w
			and	cd_material = cd_material_w;


		else
			nr_seq_componente_w	:= nr_seq_componente_w + 1;

			insert into ficha_tecnica_componente(
				nr_ficha_tecnica,
				nr_seq_componente,
				dt_alteracao,
				cd_estabelecimento,
				nm_usuario,
				dt_atualizacao,
				ie_situacao,
				ie_tipo_componente,
				cd_material,
				cd_procedimento,
				ie_origem_proced,
				cd_centro_controle,
				qt_fixa,
				qt_variavel,
				pr_quebra_variavel,
				qt_dose,
				cd_unidade_medida_dose,
				qt_material,
				ie_necessita_disp,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	nr_ficha_tecnica_w,
				nr_seq_componente_w,
				sysdate,
				cd_estabelecimento_p,
				nm_usuario_p,
				sysdate,
				'A',
				1,
				cd_material_w,
				null,
				null,
				null,
				qt_fixa_w,
				0,
				0,
				null,
				null,
				null,
				'S',
				sysdate,
				nm_usuario_p);
		end if;
	end if;

	end;
end loop;

nr_sequencia_w	:= null;

while (ds_proc_componente_w is not null) loop
	begin
	nr_pos_sep_w	:= nvl(instr(ds_proc_componente_w,','),0);
	if	(nr_pos_sep_w = 0) then
		nr_sequencia_w		:= somente_numero(ds_proc_componente_w);
		ds_proc_componente_w	:= null;
	else
		nr_sequencia_w		:= somente_numero(substr(ds_proc_componente_w,1,nr_pos_sep_w -1));
		ds_proc_componente_w	:= substr(ds_proc_componente_w,nr_pos_sep_w+1,12000);
	end if;

	if	(nvl(nr_sequencia_w,0) <> 0) then

		select	cd_procedimento,
			ie_origem_proced,
			qt_procedimento
		into	cd_procedimento_w,
			ie_origem_proced_w,
			qt_fixa_w
		from	procedimento_paciente
		where	nr_sequencia	= nr_sequencia_w;


		select	count(*)
		into	qt_registros_w
		from	ficha_tecnica_componente
		where	nr_ficha_tecnica = nr_ficha_tecnica_w
		and	cd_procedimento = cd_procedimento_w
		and	ie_origem_proced = ie_origem_proced_w;

		if (qt_registros_w > 0 ) then
			select	nr_seq_componente
			into	nr_seq_componente_w
			from	ficha_tecnica_componente
			where	nr_ficha_tecnica = nr_ficha_tecnica_w
			and	cd_procedimento = cd_procedimento_w
			and	ie_origem_proced = ie_origem_proced_w;

			update	ficha_tecnica_componente
			set	qt_fixa = qt_fixa + qt_fixa_w
			where	nr_ficha_tecnica =  nr_ficha_tecnica_w
			and	nr_seq_componente = nr_seq_componente_w
			and	cd_procedimento = cd_procedimento_w
			and	ie_origem_proced = ie_origem_proced_w;
		else

			nr_seq_componente_w	:= nr_seq_componente_w + 1;

			insert into ficha_tecnica_componente(
				nr_ficha_tecnica,
				nr_seq_componente,
				dt_alteracao,
				cd_estabelecimento,
				nm_usuario,
				dt_atualizacao,
				ie_situacao,
				ie_tipo_componente,
				cd_material,
				cd_procedimento,
				ie_origem_proced,
				cd_centro_controle,
				qt_fixa,
				qt_variavel,
				pr_quebra_variavel,
				qt_dose,
				cd_unidade_medida_dose,
				qt_material,
				ie_necessita_disp,
				nm_usuario_nrec,
				dt_atualizacao_nrec)
			values(	nr_ficha_tecnica_w,
				nr_seq_componente_w,
				sysdate,
				cd_estabelecimento_p,
				nm_usuario_p,
				sysdate,
				'A',
				2,
				null,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				qt_fixa_w,
				0,
				0,
				null,
				null,
				null,
				'S',
				nm_usuario_p,
				sysdate);
		end if;
	end if;


	end;
end loop;

commit;

end cus_gerar_ficha_tecnica;
/
