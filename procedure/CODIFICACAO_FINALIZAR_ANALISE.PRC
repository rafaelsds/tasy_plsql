create or replace procedure codificacao_finalizar_analise ( 	nr_sequencia_p number,
						nm_usuario_p varchar ) is

ie_tipo_item_w			codificacao_atend_item.ie_tipo_item%type;
cd_doenca_cid_w			codificacao_atend_item.cd_doenca_cid%type;
nr_seq_proc_pac_w		codificacao_atend_item.nr_seq_proc_pac%type;
nr_seq_diag_pac_w 		codificacao_atend_item.nr_seq_diag_pac%type;
ie_classificacao_w		codificacao_atend_item.ie_classificacao%type;
nr_sequencia_w			codificacao_atend_item.nr_sequencia%type;
cd_cid_oncologia_w		codificacao_atend_item.cd_cid_oncologia%type;
qt_class_w				number(2);
qt_classi_w				number(2);
qt_diag_pac_w			number(2);
qt_diag_and_proc_w		number(2);
qt_proc_and_diag_w		number(2);
qt_diag_val_w			number(10);
qt_prod_val_w			number(10);
ie_classif_w			codificacao_atend_item.ie_classificacao%type;
cd_doenca_w				diagnostico_doenca.cd_doenca%type;
nr_seq_grupo_w			codificacao_grupo_perfil.nr_seq_grupo%type;
cd_grupo_atual_w		codificacao_atendimento.cd_grupo_atual%type;
cd_grupo_inicial_w		codificacao_atendimento.cd_grupo_inicial%type;
nr_ordem_atual_w		codificacao_grupo.nr_ordem%type;
nr_ordem_set_w			codificacao_grupo.nr_ordem%type;
cd_doenca_cid_set_w		codificacao_regra_lib.cd_doenca_cid%type;
cd_grupo_set_w			codificacao_regra_lib.cd_doenca_cid%type;
exige_cid_w				number(2);
nm_responsavel_w		codificacao_atendimento.nm_responsavel%type;
cd_grupo_anterior_w		codificacao_atendimento.cd_grupo_anterior%type;
dt_inicio_analise_w		codificacao_atendimento.dt_inicio_analise%type;
ie_permite_liberar_w	codificacao_grupo.ie_libera%type;
cd_espec_set_w			codificacao_regra_lib.cd_especialidade_cid%type;
cd_categoria_set_w		codificacao_regra_lib.cd_categoria_cid%type;

ie_regra_cid_w			varchar2(1) := 'N';
ie_regra_espec_w		varchar2(1) := 'N';
ie_regra_categ_w		varchar2(1) := 'N';


cd_proximo_grupo_w		codificacao_atendimento.cd_grupo_atual%type;
nr_ordem_prox_grupo_w	codificacao_grupo.nr_ordem%type;

cursor c01 is
select		ie_tipo_item,
		cd_doenca_cid,
		nr_seq_proc_pac,
		nr_seq_diag_pac,
		ie_classificacao,
		nr_sequencia,
		cd_cid_oncologia
from		codificacao_atend_item
where		nr_seq_codificacao = nr_sequencia_p;

begin

	select	max(a.nr_seq_grupo),
		nvl(max(b.ie_libera), 'N')
	into	nr_seq_grupo_w,
		ie_permite_liberar_w
	from	codificacao_grupo_perfil a,
		codificacao_grupo b
	where	a.cd_perfil = wheb_usuario_pck.get_cd_perfil
	and	a.nr_seq_grupo = b.nr_sequencia;

	select	max(a.cd_grupo_atual),
		max(a.cd_grupo_inicial),
		max(b.nr_ordem),
		max(a.nm_responsavel),
		max(a.cd_grupo_anterior),
		max(a.dt_inicio_analise)
	into	cd_grupo_atual_w,
		cd_grupo_inicial_w,
		nr_ordem_atual_w,
		nm_responsavel_w,
		cd_grupo_anterior_w,
		dt_inicio_analise_w
	from	codificacao_atendimento a,
		codificacao_grupo b
	where	a.nr_sequencia = nr_sequencia_p
	and	b.nr_sequencia = a.cd_grupo_atual;

	select	count(*)
	into	qt_class_w
	from	codificacao_atend_item
	where	cd_doenca_cid is not null
	and	ie_classificacao is not null
	and	nr_seq_codificacao = nr_sequencia_p
	and 	dt_inativacao is null;

	if	(qt_class_w = 0 )then--// se caso nenhum dos itens do paciente tiver cid e classifica��o verificar se tem nr_seq_diag_pac

		select	count(*)
		into	qt_diag_pac_w
		from	codificacao_atend_item
		where	nr_seq_codificacao = nr_sequencia_p
		and	nr_seq_diag_pac is  not null;

		if	(qt_diag_pac_w = 0 ) then
			wheb_mensagem_pck.exibir_mensagem_abort(1043250);--� necess�rio classificar pelo menos um item da codifica��o como principal.
		end if;
	end if;

		select	count(*)
		into  	qt_diag_val_w
		from  	codificacao_atend_item a
		where 	a.nr_seq_codificacao = nr_sequencia_p
		and   	a.ie_tipo_item = 'D'
		and   	a.dt_inativacao is null
		and   	Nvl(a.ie_classificacao, 'S') = 'P'
		and   	exists (select 1 from codificacao_atend_item b where b.nr_seq_codificacao = a.nr_seq_codificacao and b.dt_inativacao is null)
		and   	not exists (select 1 from codificacao_atend_item c where c.nr_seq_codificacao = a.nr_seq_codificacao and c.dt_inativacao is null and c.ie_classificacao = 'D' and c.ie_tipo_item = 'P');

		if	(qt_diag_val_w <> 1 ) then
			wheb_mensagem_pck.exibir_mensagem_abort(1060244);--� necess�rio classificar pelo menos um diagn�stico da codifica��o como principal.
		end if;
		
		select count(*)
		into 	qt_classi_w
		from	codificacao_atend_item a
		where	a.nr_seq_codificacao = nr_sequencia_p
		and	a.ie_tipo_item = 'P'
		and	a.dt_inativacao is null
		and	nvl(a.ie_classificacao, 'S') <> 'P'
		and	exists (select 1 from codificacao_atend_item b where b.nr_seq_codificacao = a.nr_seq_codificacao and b.dt_inativacao is null)
		and	not exists (select 1 from codificacao_atend_item c where c.nr_seq_codificacao = a.nr_seq_codificacao and c.dt_inativacao is null and c.ie_classificacao = 'P' and c.ie_tipo_item = 'P');

		if	(qt_classi_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1060179);--Deve haver pelo menos um procedimento classificado como "Principal" para finalizar a an�lise.
		end if;

		select	count(*)
		into 	qt_prod_val_w
		from 	codificacao_atend_item
		where	nr_seq_codificacao = nr_sequencia_p
		and 	ie_tipo_item = 'P'
		and 	dt_inativacao is null
		and 	ie_classificacao is null;

		if	(qt_prod_val_w > 0 ) then
			wheb_mensagem_pck.exibir_mensagem_abort(1060242);--Favor informar a classifica��o de todos os procedimentos.
		end if;

	open c01;
	loop
	fetch c01 into
		ie_tipo_item_w,
		cd_doenca_cid_w,
		nr_seq_proc_pac_w,
		nr_seq_diag_pac_w,
		ie_classificacao_w,
		nr_sequencia_w,
		cd_cid_oncologia_w;
	exit when c01%notfound;
	begin

		if (cd_doenca_cid_w is null and nr_seq_diag_pac_w is not null) then--buscar o valor do cid doenca caso n�o tenha.

			select	cd_doenca
			into	cd_doenca_w
			from	diagnostico_doenca
			where	nr_seq_interno = nr_seq_diag_pac_w
			and	cd_doenca is not null;

			if	(cd_doenca_w is not null) then

				update	codificacao_atend_item
				set	cd_doenca_cid = cd_doenca_w
				where	nr_sequencia = nr_sequencia_w
				and	nr_seq_codificacao = nr_sequencia_p;

			end if;
		end if;

		if (ie_classificacao_w is null and nr_seq_diag_pac_w is not null) then -- buscar o valor da classifica��o caso n�o tenha

			select	ie_classificacao_doenca
			into	ie_classif_w
			from	diagnostico_doenca
			where	nr_seq_interno = nr_seq_diag_pac_w
			and	ie_classificacao_doenca is not null;

			if	(ie_classif_w is not null) then

				update	codificacao_atend_item
				set	ie_classificacao = ie_classif_w
				where	nr_sequencia = nr_sequencia_w
				and	nr_seq_codificacao = nr_sequencia_p;

			end if;
		end if;

		if	(cd_doenca_cid_w is not null and ie_classificacao_w is not null) then

			select 	count(*)
			into	exige_cid_w
			from	codificacao_regra_lib
			where	(((cd_doenca_cid = cd_doenca_cid_w )
			or	(obter_categoria_cid(cd_doenca_cid_w) = cd_categoria_cid) or (obter_especialidade_cid(cd_doenca_cid_w) = cd_especialidade_cid))
			and	nvl(ie_exige_cid, 'N') = 'S'
			and	nr_seq_grupo = nr_seq_grupo_w)
			or	((cd_doenca_cid  is null and cd_categoria_cid is null and cd_especialidade_cid is null)
			and	nvl(ie_exige_cid, 'N') = 'S'
			and	nr_seq_grupo = nr_seq_grupo_w);

			if	(exige_cid_w > 0 and cd_cid_oncologia_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(1040817, 'DS_CAMPO='||'CD_CID_ONCOLOGIA');--O campo #@DS_CAMPO#@ deve ser preenchido.
			end if;

			select	max(cg.nr_ordem),
				max(cg.nr_sequencia),
				max(rg.cd_doenca_cid),
				max(rg.cd_especialidade_cid),
				max(rg.cd_categoria_cid)
			into	nr_ordem_set_w,
				cd_grupo_set_w,
				cd_doenca_cid_set_w,
				cd_espec_set_w,
				cd_categoria_set_w
			from	codificacao_grupo cg,
				codificacao_regra_lib rg
			where	cg.nr_sequencia = rg.nr_seq_grupo
			and	((rg.cd_doenca_cid is null and rg.cd_categoria_cid is null and rg.cd_especialidade_cid is null)
			or 	((rg.cd_doenca_cid = cd_doenca_cid_w) or (obter_categoria_cid(cd_doenca_cid_w) = rg.cd_categoria_cid) or (obter_especialidade_cid(cd_doenca_cid_w) = rg.cd_especialidade_cid)))
			and	cg.nr_ordem > nr_ordem_atual_w
			and	ie_permite_liberar_w = 'N'
			and 	rownum = 1
			order by cd_doenca_cid, cg.nr_ordem desc;

			if	(cd_proximo_grupo_w is null) then

				cd_proximo_grupo_w := cd_grupo_set_w;
				nr_ordem_prox_grupo_w := nr_ordem_set_w;

				if	(cd_doenca_cid_set_w is not null) then
					ie_regra_cid_w := 'S';
				elsif	(cd_espec_set_w is not null) then
					ie_regra_espec_w := 'S';
				elsif	(cd_categoria_set_w is not null) then
					ie_regra_categ_w := 'S';
				end if;

			elsif	((ie_regra_categ_w = 'N' and ie_regra_espec_w = 'N' and ie_regra_cid_w = 'N') and
				(cd_doenca_cid_set_w is not null or cd_espec_set_w is not null or cd_categoria_set_w is not null)) then

				cd_proximo_grupo_w := cd_grupo_set_w;
				nr_ordem_prox_grupo_w := nr_ordem_set_w;

				if ( cd_doenca_cid_set_w is not null) then

					ie_regra_cid_w := 'S';
					ie_regra_espec_w := 'N';
					ie_regra_categ_w := 'N';

				elsif ( cd_espec_set_w is not null) then

					ie_regra_cid_w := 'N';
					ie_regra_espec_w := 'S';
					ie_regra_categ_w := 'N';

				elsif ( cd_categoria_set_w is not null) then

					ie_regra_cid_w := 'N';
					ie_regra_espec_w := 'N';
					ie_regra_categ_w := 'S';

				end if;

			elsif	(ie_regra_cid_w = 'S' and cd_doenca_cid_set_w is not null
				and nr_ordem_set_w < nr_ordem_prox_grupo_w) then

				cd_proximo_grupo_w := cd_grupo_set_w;
				nr_ordem_prox_grupo_w := nr_ordem_set_w;

			elsif	((ie_regra_cid_w = 'N') and
				(ie_regra_espec_w = 'S' and cd_espec_set_w is not null)
				and (nr_ordem_set_w < nr_ordem_prox_grupo_w)) then

				cd_proximo_grupo_w := cd_grupo_set_w;
				nr_ordem_prox_grupo_w := nr_ordem_set_w;

			elsif	((ie_regra_cid_w = 'N') and (ie_regra_espec_w = 'N') and
				(ie_regra_categ_w = 'S' and cd_categoria_set_w is not null)
				and (nr_ordem_set_w < nr_ordem_prox_grupo_w)) then

				cd_proximo_grupo_w := cd_grupo_set_w;
				nr_ordem_prox_grupo_w := nr_ordem_set_w;

			elsif ((ie_regra_cid_w = 'N') and (ie_regra_espec_w = 'N') and (ie_regra_categ_w = 'N') and
				(cd_doenca_cid_set_w is null and cd_espec_set_w is null and cd_categoria_set_w is null)
				and (nr_ordem_set_w < nr_ordem_prox_grupo_w)) then

				cd_proximo_grupo_w := cd_grupo_set_w;
				nr_ordem_prox_grupo_w := nr_ordem_set_w;

			end if;
		end if;
	end;
	end loop;
	close C01;

	if	(cd_proximo_grupo_w is null) then
		cd_proximo_grupo_w := cd_grupo_inicial_w;
	end if;

	if	(ie_permite_liberar_w = 'N') then

		update	codificacao_atendimento
		set	cd_grupo_atual			= cd_proximo_grupo_w,
			cd_grupo_anterior		= cd_grupo_atual_w,
			nm_responsavel_atual	= null,
			dt_inicio_analise		= null,
			ie_status				= null
		where	nr_sequencia 			= nr_sequencia_p;

		gerar_codificacao_atend_log(nr_sequencia_p, obter_desc_expressao(892380) || ' ' || obter_desc_expressao(310214) || ' '
		|| Obter_Descricao_Padrao('CODIFICACAO_GRUPO', 'DS_GRUPO', cd_grupo_atual_w)  || ' ' || obter_desc_expressao(727816) || ' '
		|| Obter_Descricao_Padrao('CODIFICACAO_GRUPO', 'DS_GRUPO', cd_proximo_grupo_w), nm_usuario_p);

	end if;

	if ((cd_grupo_set_w is null and cd_grupo_atual_w = cd_grupo_inicial_w  and nm_usuario_p = nm_responsavel_w and nr_sequencia_p is not null and cd_grupo_anterior_w is null)
	or (cd_grupo_atual_w = cd_grupo_inicial_w and cd_grupo_anterior_w is not null and  nm_usuario_p = nm_responsavel_w)
	or (ie_permite_liberar_w = 'S')) then

		update	codificacao_atendimento
	set		ie_status 			= 'AF',
			dt_inicio_analise	= dt_inicio_analise_w,
			dt_fim_analise 		= sysdate,
			dt_liberacao		= sysdate,
			dt_atualizacao		= sysdate,
			nm_usuario			= nm_usuario_p,
			cd_grupo_atual		= cd_grupo_atual_w,
			cd_grupo_anterior	= cd_grupo_anterior_w
		where	nr_sequencia 		= nr_sequencia_p;

	end if;

	commit;

end codificacao_finalizar_analise;
/
