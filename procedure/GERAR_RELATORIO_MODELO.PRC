create or replace
procedure	gerar_relatorio_modelo(	nr_seq_relatorio_p	number,
					nr_seq_modelo_p		number,
					nm_tabela_p		varchar2) is
						
nr_sequencia_w		number(10);
nr_campo_w		number(5);
nr_seq_apres_banda_w	number(5);
nr_seq_apres_w		number(4);
nr_seq_rel_param_w	number(10);
ds_sql_w		varchar2(4000);
nm_atributo_w		varchar2(30);
ds_prefixo_w		varchar2(3);
nr_seq_banda_w		number(10);
nr_seq_campo_w		number(10);
qt_const_w		integer	:= 6;
ds_item_w		varchar(255);
ie_tipo_atributo_w	varchar2(10);
	
cursor c01 is
	select	nr_seq_pergunta, --nr_sequencia,
		nvl(ds_item, obter_desc_expressao(346967,'Informar Label')),
		ie_tipo_atributo
	from	modelo_conteudo_v
	where	nr_seq_modelo = nr_seq_modelo_p	
	and	ie_componente <> 'Visual'
	order  by nr_seq_apres; 

begin

delete	
from	banda_relatorio
where	nr_seq_relatorio = nr_seq_relatorio_p;
	
delete	
from	relatorio_parametro
where	nr_seq_relatorio = nr_seq_relatorio_p;
	
insert into banda_relatorio(
	nr_sequencia, 			ie_tipo_banda, 
	ds_banda, 			dt_atualizacao, 
	nm_usuario,			qt_altura,
	ds_cor_fundo,			ie_quebra_pagina, 			
	ie_reimprime_nova_pagina,	ie_alterna_cor_fundo,
	ie_imprime_vazio,		ie_imprime_primeiro, 
	ie_borda_sup,			ie_borda_inf,
	ie_borda_esq, 			ie_borda_dir, 			
	nr_seq_relatorio,        	nr_seq_apresentacao,
	ds_cor_header,			ds_cor_footer,
	ds_cor_quebra,			ie_banda_padrao)
values
	(banda_relatorio_seq.nextval, 	'C', 
	obter_desc_expressao(486585,'Cabe�alho'),		 	sysdate, 	
	'Tasy',				70,
	'clWhite', 			'N', 			
	'N',				'N', 
	'N',				'N',
	'N',				'S',
	'N',				'N',
	nr_seq_relatorio_p, 		1,
	'clSilver',			'clWhite',
	'$00E5E5E5',			'S');
		
-- Rodap�
insert into banda_relatorio(
	nr_sequencia, 			ie_tipo_banda, 
	ds_banda, 			dt_atualizacao, 
	nm_usuario,			qt_altura,
	ds_cor_fundo,			ie_quebra_pagina, 			
	ie_reimprime_nova_pagina,	ie_alterna_cor_fundo,
	ie_imprime_vazio,		ie_imprime_primeiro, 
	ie_borda_sup,			ie_borda_inf,
	ie_borda_esq, 			ie_borda_dir, 			
	nr_seq_relatorio,		nr_seq_apresentacao,
	ds_cor_header,			ds_cor_footer,
	ds_cor_quebra,			ie_banda_padrao)
values
	(banda_relatorio_seq.nextval, 	'R', 
	obter_desc_expressao(486586,'Rodap�'), 			sysdate, 
	'Tasy',				17,
	'clWhite', 			'N', 			
	'N',				'N', 
	'N',				'N',
	'S',				'N',
	'N',				'N',
	nr_seq_relatorio_p, 		9999,
	'clSilver',			'clWhite',
	'$00E5E5E5',			'S');	

nr_campo_w		:= 0;
nr_seq_apres_banda_w	:= 0;
nr_seq_apres_w		:= 0;

commit;

open c01;
loop
     fetch c01 into
     	nr_sequencia_w,
	ds_item_w,
	ie_tipo_atributo_w;	
     exit when c01%notfound;
	begin	
	
	select	max(nr_seq_apresentacao)
	into	nr_seq_apres_banda_w
	from	banda_relatorio
	where	nr_seq_relatorio = nr_seq_relatorio_p
	and	nr_seq_apresentacao <> 9999;
			
	if	(nr_seq_apres_w = qt_const_w) then
		nr_seq_apres_w	:= 1;
	else
		nr_seq_apres_w	:= nr_seq_apres_w + 1;
	end	if;
		
	if	(mod(nr_campo_w,qt_const_w) = 0) or
		(nr_seq_apres_banda_w = 0) then
		
		if	(mod(nr_campo_w,qt_const_w) = 0) and
			(nr_seq_banda_w > 0) then			
			update	banda_relatorio
			set 	ds_sql =	'select	' || substr(ds_sql_w,1,length(ds_sql_w)-1) || 'from dual '					
			where	nr_sequencia	= nr_seq_banda_w;			
		end	if;
		
		nr_seq_apres_banda_w	:= nr_seq_apres_banda_w + 1;	
		select	banda_relatorio_seq.nextval
		into	nr_seq_banda_w
		from	dual;		
	
		-- Subdetalhe			
		insert into banda_relatorio(
			nr_sequencia, 			ie_tipo_banda, 
			ds_banda, 			dt_atualizacao, 
			nm_usuario,			qt_altura,
			ds_cor_fundo,			ie_quebra_pagina, 			
			ie_reimprime_nova_pagina,	ie_alterna_cor_fundo,
			ie_imprime_vazio,		ie_imprime_primeiro, 
			ie_borda_sup,			ie_borda_inf,
        		ie_borda_esq, 			ie_borda_dir, 			
			nr_seq_relatorio,		ds_sql,
			nr_seq_apresentacao,
			ds_cor_header,			ds_cor_footer,
			ds_cor_quebra,			ie_banda_padrao)
		values
        		(nr_seq_banda_w,	 	'S', 
			obter_desc_expressao(284236,'Banda'),		 	sysdate, 
			'Tasy',				18,
        		'clWhite', 			'N', 			
			'N',				'N', 
			'N',				'N',
			'N',				'N',
			'N',				'N',
			nr_seq_relatorio_p, 		null, 
			nr_seq_apres_banda_w,
			'clSilver',			'clWhite',
			'$00E5E5E5',			'N');
				
		ds_sql_w := '';	
					
	end	if;	
	
	ds_prefixo_w	:= '';
	if	(ie_tipo_atributo_w = 'VARCHAR2') then
		ds_prefixo_w	:= 'ds_';
	elsif	(ie_tipo_atributo_w = 'NUMBER') then
		ds_prefixo_w	:= 'vl_';
	elsif	(ie_tipo_atributo_w = 'DATE') then
		ds_prefixo_w	:= 'dt_';
	end	if;
	
	nm_atributo_w	:= ds_prefixo_w || nr_sequencia_w;		
	
	Criar_Campo_Relatorio(	nr_seq_banda_w,	substr(ds_item_w,1,19), 
				nr_seq_apres_w, 	0, 
				1, 			(((nr_seq_apres_w - 1) * 5) + 5) + ((nr_seq_apres_w - 1) * 100),	
				100,			nm_atributo_w, 
				'Tasy',
				nr_seq_campo_w);
	update	banda_relat_campo
	set	ds_label = substr(ds_item_w,1,19)
	where	nr_sequencia = nr_seq_campo_w;
	
	commit;
	
	ds_sql_w := ds_sql_w || '	substr(modelo_vlr(:nr_seq_declaracao_p,' || nr_sequencia_w || ',' || chr(39) || nm_tabela_p ||chr(39) ||'),1,254) ' || nm_atributo_w || chr(13) || chr(10) || ',';	
	nr_campo_w	:= nr_campo_w + 1;
	
	end;
end loop;
close c01;

if	(ds_sql_w is not null) then
	update	banda_relatorio
	set 	ds_sql =	'select	' || substr(ds_sql_w,1,length(ds_sql_w)-1) || 'from dual '
	where	nr_sequencia	= nr_seq_banda_w;
end	if; 
	
	
select	RELATORIO_PARAMETRO_seq.nextval
into	nr_seq_rel_param_w
from	dual;

insert into RELATORIO_PARAMETRO (
	nr_sequencia, cd_parametro, 
	ds_parametro, ie_tipo_atributo, 
	nr_seq_relatorio, ie_origem_informacao,
	dt_atualizacao, nm_usuario, 
	ie_forma_apresent, qt_tamanho_campo, ie_forma_passagem, 
	ie_multi_linha,nr_seq_apresent)
values
	(relatorio_parametro_seq.nextval, 'NR_SEQ_DECLARACAO_P', 
	obter_desc_expressao(312150,'Seq. Registro'), 'NUMBER', 
	nr_seq_relatorio_p, 'P', 
	SYSDATE, 'Tasy', 
	'ed', 80, 'P',
	'N', 1);
	
commit;

end;
/
