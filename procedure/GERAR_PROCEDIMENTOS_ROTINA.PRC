create or replace
procedure gerar_procedimentos_rotina(
		nr_seq_agenda_p		number,
		ds_lista_proced_p		varchar2,
		ds_erro_p	out		varchar2,
		nm_usuario_p		varchar2,
		cd_medico_p	varchar2 default null) is
		
ds_lista_proced_w		varchar2(4000);
ds_proced_w		varchar2(255);
nr_seq_grupo_item_w 	number(10,0);
qt_tam_lista_w		number(10,0);
ie_pos_virgula_w 		number(5,0);
ie_pos_separador_w	number(5,0);
ds_erro_w		varchar2(255);
ds_pos_separador_w	varchar2(255);
qt_procedimento_w	number(5,0);
ie_lado_w			varchar2(1);

begin
if	(nr_seq_agenda_p is not null) and
	(ds_lista_proced_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	ds_lista_proced_w := ds_lista_proced_p;
	
	while	(ds_lista_proced_w is not null) loop
		begin
		qt_tam_lista_w		:= length(ds_lista_proced_w);
		ie_pos_virgula_w	:= instr(ds_lista_proced_w,',');
		

		if	(ie_pos_virgula_w <> 0) then
			begin
				ds_proced_w		:= substr(ds_lista_proced_w,1,(ie_pos_virgula_w - 1));
				ds_lista_proced_w	:= substr(ds_lista_proced_w,(ie_pos_virgula_w + 1),qt_tam_lista_w);
				
				
				ie_pos_separador_w	:= instr(ds_proced_w,'-');
				nr_seq_grupo_item_w	:= to_number(substr(ds_proced_w,1,(ie_pos_separador_w - 1)));
			
				ds_pos_separador_w 	:= substr(ds_proced_w,ie_pos_separador_w + 1,(ie_pos_virgula_w - 1));
				ie_pos_separador_w	:= instr(ds_pos_separador_w ,'-');
						
				begin
					ie_lado_w	:= substr(ds_pos_separador_w,1,(ie_pos_separador_w - 1));
					if	(ie_lado_w = 'null') then
						ie_lado_w := null;
					end if;
				exception
					when others then
					ie_lado_w := null;
				end;
				
				
				ds_pos_separador_w 	:= substr(ds_pos_separador_w,ie_pos_separador_w + 1,(ie_pos_virgula_w - 1));
				ie_pos_separador_w	:= instr(ds_pos_separador_w ,'-');
				
				begin
					qt_procedimento_w	:= to_number(substr(ds_pos_separador_w,1,(ie_pos_separador_w - 1)));
					if	(qt_procedimento_w = 0) then
						qt_procedimento_w := 1;
					end if;
				exception
					when others then
					qt_procedimento_w := 1;
				end;			
			end;
		else
			begin
				ie_pos_separador_w	:= instr(ds_lista_proced_w,'-');
				nr_seq_grupo_item_w	:= to_number(substr(ds_lista_proced_w,1,(ie_pos_separador_w - 1)));
				
				
				ds_pos_separador_w 	:= substr(ds_proced_w,ie_pos_separador_w + 1,(ie_pos_virgula_w - 1));
				ie_pos_separador_w	:= instr(ds_pos_separador_w,'-');
						
				begin
					ie_lado_w	:= substr(ds_pos_separador_w,1,(ie_pos_separador_w - 1));
					if	(ie_lado_w = 'null') then
						ie_lado_w := null;
					end if;
				exception
					when others then
					ie_lado_w := null;
				end;

				
				ds_pos_separador_w 	:= substr(ds_pos_separador_w,ie_pos_separador_w + 1,(ie_pos_virgula_w - 1));
				ie_pos_separador_w	:= instr(ds_pos_separador_w ,'-');
				
				begin
					qt_procedimento_w	:= to_number(substr(ds_pos_separador_w,1,(ie_pos_separador_w - 1)));
					if	(qt_procedimento_w = 0) then
						qt_procedimento_w := 1;
					end if;
				exception
					when others then
					qt_procedimento_w := 1;
				end;
					
				
				ds_lista_proced_w	:= null;
			end;
		end if;
		
		if	(nr_seq_grupo_item_w is not null) and
			(nr_seq_grupo_item_w > 0) then
			begin			
			gerar_proc_rotina_adic_exam(
				nr_seq_agenda_p,
				nr_seq_grupo_item_w,
				nm_usuario_p,
				ds_erro_w,
				cd_medico_p,
				qt_procedimento_w,
				ie_lado_w);
			end;
		end if;
		end;
	end loop;
	end;
end if;
commit;
ds_erro_p		:= ds_erro_w;
end gerar_procedimentos_rotina;
/