create or replace
procedure liquidar_tit_rec_perdas
			(nr_titulo_p 			number,
			nr_seq_trans_financ_p		number,
			cd_tipo_recebimento_p		varchar2,
			dt_recebimento_p		date,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number,
			cd_motivo_desc_p		number,
			cd_centro_custo_p		number,
			cd_pessoa_fisica_desc_p		varchar2,
			cd_cgc_pessoa_desc_p		varchar2,
			ie_tipo_perda_p			varchar2,
			vl_baixa_p			number,
			NR_SEQ_MOTIVO_PERDA_P		number) is 

nr_seq_desc_w		number(10);

cd_moeda_padrao_w		number(05,0)	:= 0;
nr_seq_baixa_w			number(05,0)	:= 0;
vl_juros_w			number(15,2)	:= 0;
vl_desconto_w			number(15,2)	:= 0;
vl_multa_w			number(15,2)	:= 0;
cd_centro_custo_desc_w		number(10,0);
nr_seq_motivo_desc_w		number(10,0);
vl_rec_maior_w			number(15,2)	:= 0;
nr_seq_solic_desc_w		number(10) 	:= null;
ds_observacao_bordero_w		varchar2(80);
vl_nota_credito_w		number(15,2);
nr_seq_regra_w			number(10);
ie_acao_w			varchar2(5);
ie_acao_baixa_w			varchar2(5);
ie_apropriar_w			varchar2(1);
ie_controle_perdas_w		varchar2(1);
nr_titulo_contab_w		number(10);

begin
select	cd_moeda_padrao,
	ie_controle_perdas
into	cd_moeda_padrao_w,
	ie_controle_perdas_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_p;

select	nvl(max(nr_sequencia),0) + 1
into	nr_seq_baixa_w
from	titulo_receber_liq
where	nr_titulo	= nr_titulo_p;

insert	into titulo_receber_liq
	(nr_titulo,
	nr_sequencia,
	dt_recebimento,
	vl_recebido,
	vl_descontos,
	vl_juros,
	vl_multa,
	cd_moeda,
	dt_atualizacao,
	nm_usuario,
	cd_tipo_recebimento,
	ie_acao,
	cd_serie_nf_devol,
	nr_nota_fiscal_devol,
	cd_banco,
	cd_agencia_bancaria,
	nr_documento,
	nr_lote_banco,
	cd_cgc_emp_cred,
	nr_cartao_cred,
	nr_adiantamento,
	nr_lote_contabil,
	nr_seq_trans_fin,
	ie_lib_caixa,
	cd_centro_custo_desc,
	nr_seq_motivo_desc,
	vl_perdas,
	ds_observacao,
	vl_nota_credito,
	nr_lote_contab_antecip,
	nr_lote_contab_pro_rata,
	vl_rec_maior,
	vl_glosa,
	ie_tipo_perda)
values	(nr_titulo_p,
	nr_seq_baixa_w,
	nvl(dt_recebimento_p, trunc(sysdate,'dd')),
	0,
	vl_desconto_w,
	vl_juros_w,
	vl_multa_w,
	cd_moeda_padrao_w,
	sysdate,
	nm_usuario_p,
	cd_tipo_recebimento_p, 
	'I', 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null,
	null, 
	null, 
	0, 
	nr_seq_trans_financ_p, 
	'S',
	cd_centro_custo_p,
	cd_motivo_desc_p,
	vl_baixa_p,
	ds_observacao_bordero_w,
	vl_nota_credito_w,
	0,
	0,
	0,
	0,
	ie_tipo_perda_p);
	/* Edgar 19/03/2008, OS 86492, alterei para chamar esta procedure sempre que lan�ada a baixa */
	
gerar_titulo_rec_liq_cc
		(CD_ESTABELECIMENTO_P,
		NULL,
		NM_USUARIO_P,
		nr_titulo_p,
		NR_SEQ_BAIXA_W);
		
if	(ie_controle_perdas_w = 'S') then
	gerar_perda_contas_receber(
		cd_estabelecimento_p, 
		null, 
		nr_titulo_p, 
		nr_seq_baixa_w, 
		vl_baixa_p, 
		ie_tipo_perda_p, 
		nm_usuario_p, 
		NR_SEQ_MOTIVO_PERDA_P,
		nvl(dt_recebimento_p, trunc(sysdate,'dd')));
end if;
		
pls_gerar_tit_rec_liq_mens(nr_titulo_p,	nr_seq_baixa_w, nm_usuario_p,nr_titulo_contab_w);	

	if	(nr_titulo_contab_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(236517, 'NR_TITULO=' || nr_titulo_contab_w);
	end if;
		
		
if	(cd_cgc_pessoa_desc_p is not null) or 
	(cd_pessoa_fisica_desc_p is not null) then
	begin
	select	titulo_receber_liq_desc_seq.nextval
	into	nr_seq_desc_w
	from	dual;
		
	insert   into titulo_receber_liq_desc
		(cd_centro_custo, 
		cd_cgc, 
		cd_pessoa_fisica, 
		dt_atualizacao, 
		nm_usuario, 
		nr_seq_motivo_desc, 
		nr_sequencia, 
		nr_titulo,
		nr_seq_liq) 
	values  (cd_centro_custo_p,
		cd_cgc_pessoa_desc_p,
		cd_pessoa_fisica_desc_p,
		sysdate,
		nm_usuario_p,
		cd_motivo_desc_p,
		nr_seq_desc_w,
		nr_titulo_p,
		nr_seq_baixa_w);
	end;
end if;

obter_regra_acao_pag_duplic(dt_recebimento_p, cd_estabelecimento_p, nm_usuario_p, nr_seq_regra_w, ie_acao_w);				
	
atualizar_saldo_tit_rec(nr_titulo_p, nm_usuario_p);
	
commit;

end liquidar_tit_rec_perdas;
/
