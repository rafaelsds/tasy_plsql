create or replace
procedure pls_proposta_inserir_benef
			(	nr_seq_proposta_p	number,
				nr_seq_segurado_p	number,
				nm_usuario_p		varchar2) is

cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_plano_w			pls_plano.nr_sequencia%type;
nr_seq_tabela_w			pls_tabela_preco.nr_sequencia%type;
nr_seq_pagador_w		pls_contrato_pagador.nr_sequencia%type;
nr_seq_contrato_w		pls_contrato.nr_sequencia%type;
nr_seq_titular_w		pls_segurado.nr_sequencia%type;
nr_seq_parentesco_w		grau_parentesco.nr_sequencia%type;
cd_pessoa_pagador_w		varchar2(20);
cd_pf_titular_w			pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_tit_cont_prop_w		pls_proposta_pagador.nr_sequencia%type;
nr_seq_motivo_inclusao_w	pls_parametros.nr_seq_motivo_inclusao%type;
qt_pessoa_proposta_w		pls_integer;
nm_beneficiario_w		varchar2(255);
ie_tipo_proposta_w		pls_proposta_adesao.ie_tipo_proposta%type;

begin

select	count(1)
into	qt_pessoa_proposta_w
from	pls_proposta_beneficiario
where	nr_seq_beneficiario	= nr_seq_segurado_p
and	nr_seq_proposta		= nr_seq_proposta_p;

if	(qt_pessoa_proposta_w = 0) then
	
	select	max(nr_seq_motivo_inclusao)
	into	nr_seq_motivo_inclusao_w
	from	pls_parametros
	where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;
	
	select	max(ie_tipo_proposta)
	into	ie_tipo_proposta_w
	from 	pls_proposta_adesao
	where 	nr_sequencia = nr_seq_proposta_p;
	
	select	cd_pessoa_fisica,
		nr_seq_plano,
		nr_seq_tabela,
		nr_seq_pagador,
		nr_seq_contrato,
		nvl(nr_seq_titular,0),
		nr_seq_parentesco,
		obter_nome_pf(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w,
		nr_seq_plano_w,
		nr_seq_tabela_w,
		nr_seq_pagador_w,
		nr_seq_contrato_w,
		nr_seq_titular_w,
		nr_seq_parentesco_w,
		nm_beneficiario_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
	
	if (ie_tipo_proposta_w = 5) then
		pls_proposta_sca_contrato(nr_seq_proposta_p, nr_seq_contrato_w, nr_seq_segurado_p, nm_usuario_p);	
	end if;	
	
	if	(nr_seq_pagador_w is not null) then
		select	nvl(cd_pessoa_fisica,cd_cgc)
		into	cd_pessoa_pagador_w
		from	pls_contrato_pagador
		where	nr_sequencia	= nr_seq_pagador_w;
		
		select	max(nr_sequencia)
		into	nr_seq_pagador_w
		from	pls_proposta_pagador
		where	((cd_pagador	= cd_pessoa_pagador_w) or (cd_cgc_pagador = cd_pessoa_pagador_w))
		and	nr_seq_proposta = nr_seq_proposta_p;
	end if;
	
	if	(nr_seq_titular_w > 0) then
		select	cd_pessoa_fisica
		into	cd_pf_titular_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_titular_w;
		
		begin
			select	nr_sequencia
			into	nr_seq_tit_cont_prop_w
			from	pls_proposta_beneficiario
			where	cd_beneficiario	= cd_pf_titular_w
			and	nr_seq_proposta	= nr_seq_proposta_p;
		exception
		when others then
			nr_seq_tit_cont_prop_w := null;
		end;
	end if;
	
	insert	into	pls_proposta_beneficiario
		(	nr_sequencia,				dt_atualizacao,			nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,		cd_beneficiario,
			nr_seq_plano,				nr_seq_tabela,			nr_seq_pagador,
			nr_seq_proposta,			nr_seq_titular,			nr_seq_parentesco,
			nr_seq_beneficiario,			nr_seq_motivo_inclusao,		ie_copiar_sca_plano)
	values	(	pls_proposta_beneficiario_seq.nextval, 	sysdate,			nm_usuario_p,
			sysdate, 				nm_usuario_p,			cd_pessoa_fisica_w,
			nr_seq_plano_w,				nr_seq_tabela_w,		nr_seq_pagador_w,
			nr_seq_proposta_p,			nr_seq_tit_cont_prop_w,		nr_seq_parentesco_w,
			nr_seq_segurado_p,			nr_seq_motivo_inclusao_w,	'S');
	
	commit;
end if;

end pls_proposta_inserir_benef;
/
