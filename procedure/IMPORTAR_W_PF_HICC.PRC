create or replace
procedure importar_w_pf_HICC is

/* Estrutura cliente */

prontuario_w	number(15,0);
nm_pacient_w	varchar2(255);
dt_nascime_w	date;
cd_sexo_w	varchar2(10);
estado_civ_w	number;
nu_rg_w		number;
nu_cgc_cpf_w	number;
cd_profiss_w	number;
religiao_w	number;
cor_w		number;
nacionalid_w	NUMBER;
cd_cns_w	varchar2(255);
nm_mae_w	varchar2(255);
nm_pai_w	varchar2(255);
grau_instr_w	number;
cd_uf_w		varchar2(255);
nm_municip_w	varchar2(255);
nm_enderec_w	varchar2(255);
nu_enderec_w	number;
complement_w	varchar2(255);
bairro_w	varchar2(255);
nu_cep_w	varchar2(255);
nr_telefone_w	varchar2(255);

/* Internos */
cd_pessoa_fisica_w	varchar2(10);
nr_sequencia_w		number(5);
ie_erro_compl_pf_w	varchar2(1)	:= 'N';
ie_erro_pf_w		varchar2(1)	:= 'N';
qt_registro_w		number(10);
trigger_name_w		varchar2(255);
sql_errm_w		varchar2(2000);
qt_inserido_w		number(15,0);

qt_teste_w		number(15,0);

Cursor c01 is
select	cd_pacient,
	nm_pacient,
	dt_nascime,
	cd_sexo,
	estado_civ,
	nu_rg,
	nu_cgc_cpf,
	cd_profiss,
	religiao,
	cor,
	nacionalid,
	cd_cns,
	nm_mae,
	nm_pai,
	grau_instr,
	cd_uf,
	nm_municip,
	nm_enderec,
	nu_enderec,
	complement,
	bairro,
	nu_cep,
	nr_telefone
from	W_IMP_PF_HICC
order by 2;

cursor c02 is
select	trigger_name
from	user_triggers
where	table_name = 'PESSOA_FISICA'
union all
select	trigger_name
from	user_triggers
where	table_name = 'COMPL_PESSOA_FISICA'
order by
	1;

cursor c03 is
select	trigger_name
from	user_triggers
where	status = 'DISABLED'
order by
	1;

begin

open c02;
loop
fetch c02 into
	trigger_name_w;
exit when c02%notfound;
	begin
	exec_sql_dinamico('OS124305_PF', 'alter trigger ' || trigger_name_w || ' disable');
	end;
end loop;
close c02;

begin

qt_inserido_w	:= 0;

open c01;
loop
fetch c01 into
	prontuario_w,
	nm_pacient_w,
	dt_nascime_w,
	cd_sexo_w,
	estado_civ_w,
	nu_rg_w,
	nu_cgc_cpf_w,
	cd_profiss_w,
	religiao_w,
	cor_w,
	nacionalid_w,
	cd_cns_w,
	nm_mae_w,
	nm_pai_w,
	grau_instr_w,
	cd_uf_w,
	nm_municip_w,
	nm_enderec_w,
	nu_enderec_w,
	complement_w,
	bairro_w,
	nu_cep_w,
	nr_telefone_w;
exit when c01%notfound;

	ie_erro_pf_w		:= 'N';
	ie_erro_compl_pf_w	:= 'N';
	qt_inserido_w		:= qt_inserido_w + 1;

	select	count(*)
	into	qt_registro_w
	from	nacionalidade
	where	cd_nacionalidade	= nacionalid_w;

	if	(qt_registro_w = 0) and
		(nvl(nacionalid_w,0) <> 0) then
		insert into log_tasy
			(cd_log,
			nm_usuario,
			dt_atualizacao,
			ds_log)
		values	(55715,
			'OS124305_PF',
			sysdate,
			'Erro ao inserir pessoa. ' || chr(13) || chr(10) ||
			'Nome = ' || nm_pacient_w || chr(13) || chr(10) ||
			'Erro = N�o foi localizada a nacionalidade ' || nacionalid_w);

		ie_erro_pf_w	:= 'S';
	end if;

	select	count(*)
	into	qt_registro_w
	from	cor_pele
	where	nr_sequencia	= cor_w;

	if	(qt_registro_w = 0) and
		(nvl(cor_w,0) <> 0) then
		insert into log_tasy
			(cd_log,
			nm_usuario,
			dt_atualizacao,
			ds_log)
		values	(55715,
			'OS124305_PF',
			sysdate,
			'Erro ao inserir pessoa. ' || chr(13) || chr(10) ||
			'Nome = ' || nm_pacient_w || chr(13) || chr(10) ||
			'Erro = N�o foi localizada a cor ' || cor_w);

		ie_erro_pf_w	:= 'S';
	end if;

	select	count(*)
	into	qt_registro_w
	from	religiao
	where	cd_religiao	= religiao_w;

	if	(qt_registro_w = 0) and
		(nvl(religiao_w,0) <> 0) then
		insert into log_tasy
			(cd_log,
			nm_usuario,
			dt_atualizacao,
			ds_log)
		values	(55715,
			'OS124305_PF',
			sysdate,
			'Erro ao inserir pessoa. ' || chr(13) || chr(10) ||
			'Nome = ' || nm_pacient_w || chr(13) || chr(10) ||
			'Erro = N�o foi localizada a religi�o ' || religiao_w);

		ie_erro_pf_w	:= 'S';
	end if;

	if	(ie_erro_pf_w = 'N') then	
		begin

		select	pessoa_fisica_seq.nextval
		into	cd_pessoa_fisica_w
		from	dual;	

		insert into pessoa_fisica
			(cd_pessoa_fisica,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ie_tipo_pessoa,
			nm_pessoa_fisica,
			nr_prontuario,
			nr_identidade,
			nr_cpf,
			dt_nascimento,
			ie_sexo,
			ie_estado_civil,
			nr_telefone_celular,
			cd_nacionalidade,
			cd_religiao,
			nr_seq_cor_pele,
			cd_cnes,
			IE_GRAU_INSTRUCAO)
		values	(cd_pessoa_fisica_w,
			'OS124305_PF',
			sysdate,
			'OS124305_PF',
			sysdate,
			1,
			nm_pacient_w,
			prontuario_w,
			substr(somente_numero(nu_rg_w),1,15),
			lpad(somente_numero(nu_cgc_cpf_w),11,0),
			dt_nascime_w,
			cd_sexo_w,
			estado_civ_w,
			nr_telefone_w,
			nacionalid_w,
			religiao_w,
			cor_w,
			cd_cns_w,
			grau_instr_w);

		exception
			when others then

			sql_errm_w	:= sqlerrm;

			insert	into	log_tasy
				(cd_log,
				nm_usuario,
				dt_atualizacao,
				ds_log)
			values	(55715,
				'OS124305_PF',
				sysdate,
				'Erro ao inserir pessoa. ' || chr(13) || chr(10) ||
				'C�digo= ' || prontuario_w || chr(13) || chr(10) ||
				'Nome= ' || nm_pacient_w || chr(13) || chr(10) ||
				'Erro= ' || sql_errm_w);

			ie_erro_pf_w	:= 'S';
		end;

		if	(ie_erro_pf_w = 'N') then

			/* Residencial - sempre insere */

			if	(cd_uf_w is not null) then

				select	count(*)
				into	qt_registro_w
				from	valor_dominio
				where	cd_dominio	= 50
				and	vl_dominio	= cd_uf_w;

				if	(qt_registro_w = 0) then
					insert	into	log_tasy
						(cd_log,
						nm_usuario,
						dt_atualizacao,
						ds_log)
					values	(55715,
						'OS124305_PF',
						sysdate,
						'Erro ao inserir compl tipo 1. ' || chr(13) || chr(10) ||
						'Nome= ' || nm_pacient_w || chr(13) || chr(10) ||
						'Erro= N�o foi localizada a sigla do estado: ' || cd_uf_w);

					ie_erro_compl_pf_w	:= 'S';
				end if;
			end if;

			if	(cd_profiss_w is not null) then

				select	count(*)
				into	qt_registro_w
				from	profissao
				where	cd_profissao	= cd_profiss_w;

				if	(qt_registro_w = 0) then
					insert into log_tasy
						(cd_log,
						nm_usuario,
						dt_atualizacao,
						ds_log)
					values	(55715,
						'OS124305_PF',
						sysdate,
						'Erro ao inserir pessoa. ' || chr(13) || chr(10) ||
						'Nome = ' || nm_pacient_w || chr(13) || chr(10) ||
						'Erro = N�o foi localizada a profiss�o ' || cd_profiss_w);

						ie_erro_compl_pf_w	:= 'S';
					end if;
			end if;

			if	(ie_erro_compl_pf_w = 'N') then

				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

begin
		
				insert into compl_pessoa_fisica
					(nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					cd_cep,
					ds_endereco,
					nr_endereco,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					sg_estado,
					cd_profissao)
				values	(nr_sequencia_w,
					cd_pessoa_fisica_w,
					1,
					'OS124305_PF',
					sysdate,
					'OS124305_PF',
					sysdate,
					nu_cep_w,
					nm_enderec_w,
					somente_numero(substr(nu_enderec_w,1,5)),
					complement_w,
					bairro_w,
					nm_municip_w,
					cd_uf_w,
					cd_profiss_w);

exception when others then
	null;
end;
			end if;

			/* Fim residencial */

			if	(nm_mae_w is not null) then
	
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
				insert into compl_pessoa_fisica
					(nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					nm_contato)
				values	(nr_sequencia_w,
					cd_pessoa_fisica_w,
					5,
					'OS124305_PF',
					sysdate,
					'OS124305_PF',
					sysdate,
					nm_mae_w);	
				end if;

			if	(nm_pai_w is not null) then

				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

				insert into compl_pessoa_fisica
					(nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					nm_contato)
				values	(nr_sequencia_w,
					cd_pessoa_fisica_w,
					4,
					'OS124305_PF',
					sysdate,
					'OS124305_PF',
					sysdate,
					nm_pai_w);
			end if;

		end if;

		if	(qt_inserido_w	= 3000) then
			commit;
			qt_inserido_w	:= 0;
		end if;

	end if;

end loop;
close c01;

commit;

exception
	when others then

	open c03;
	loop
	fetch c03 into
		trigger_name_w;
	exit when c03%notfound;
		begin
		exec_sql_dinamico('OS124305_PF', 'alter trigger ' || trigger_name_w || ' enable');
		end;
	end loop;
	close c03;

end;

open c03;
loop
fetch c03 into
	trigger_name_w;
exit when c03%notfound;
	begin
	exec_sql_dinamico('OS124305_PF', 'alter trigger ' || trigger_name_w || ' enable');
	end;
end loop;
close c03;

end importar_w_pf_HICC;
/
