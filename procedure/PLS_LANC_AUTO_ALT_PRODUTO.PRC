create or replace
procedure pls_lanc_auto_alt_produto
			(	nr_seq_beneficiario_p		Number,
				ie_origem_alt_prod_p		Varchar2,
				ie_copiar_carencia_prod_novo_p	Varchar2,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number) is 

nr_seq_regra_w			Number(10)	:= 0;
nr_seq_tipo_carencia_w		Number(10);
qt_dias_carencia_w		Number(5);
ie_inicio_vig_carencia_w	Varchar2(3);
nr_seq_plano_ant_w		Number(10);
nr_seq_plano_atual_w		Number(10);
ie_acomodacao_antigo_w		Varchar2(1);
ie_acomodacao_atual_w		Varchar2(1);
dt_inicio_vigencia_w		Date;
ie_tipo_regra_w			Varchar2(2);
vl_mensalidade_w		Number(15,2);
ie_preco_atual_w		varchar2(10);
ie_preco_ant_w			varchar2(10);
nr_seq_segurado_ant_w		number(10);
--------------------------------------------------------------------------------
ie_adaptar_carencia_rede_w	Varchar2(10);
ie_mes_posterior_w		varchar2(10);
nr_seq_grupo_carencia_w		number(10);
qt_registros_w			number(10);
nr_seq_contrato_w		number(10);
ie_consistir_carencia_rede_w	varchar2(10) := 'N';
nr_seq_tipo_carencia_ww		number(10);
qt_dias_w			number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_regra_lanc_automatico
	where	ie_evento		= 1
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_situacao		= 'A'
	and	((nvl(ie_acomodacao_origem,'N') = 'N') or (ie_acomodacao_origem = ie_acomodacao_antigo_w))
	and	((nvl(ie_acomodacao_destino,'N') = 'N') or (ie_acomodacao_destino = ie_acomodacao_atual_w))
	and	((ie_preco	= ie_preco_atual_w) or (ie_preco is null))
	and	((ie_preco_ant	= ie_preco_ant_w) or (ie_preco is null))
	and	((ie_origem_alt_produto	= ie_origem_alt_prod_p) or (ie_origem_alt_produto = 'T'))
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate);
	
Cursor C02 is
	select	nvl(nr_seq_tipo_carencia,0),
		nvl(qt_dias_carencia,0),
		nvl(ie_inicio_vig_carencia,'N'),
		nvl(vl_mensalidade,0),
		ie_tipo_regra,
		nvl(ie_adaptar_carencia_rede,'N')
	from	pls_regra_lanc_aut_item
	where	nr_seq_regra	= nr_seq_regra_w
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate);

begin
/*Caso a acao venha da alteracao manual do produto*/
if	(ie_origem_alt_prod_p	= 'A') then
	/* Obter o produto antigo e atual do beneficiario */
	begin
	select	nr_seq_plano_ant,
		nr_seq_plano_atual
	into	nr_seq_plano_ant_w,
		nr_seq_plano_atual_w
	from	pls_segurado_alt_plano
	where	nr_seq_segurado	= nr_seq_beneficiario_p
	and	nr_sequencia	=	(	select	max(nr_sequencia) 
						from 	pls_segurado_alt_plano 
						where 	nr_seq_segurado = nr_seq_beneficiario_p
						and	ie_situacao = 'A'
						and	trunc(dt_atualizacao) = trunc(sysdate));
	exception
		when others then
		nr_seq_plano_ant_w	:= 0;
		nr_seq_plano_atual_w	:= 0;
	end;
/*Caso a alteracao venha da migracao de contratos*/
elsif	(ie_origem_alt_prod_p	= 'M') then
	select	nr_seq_segurado_ant,
		nr_seq_plano
	into	nr_seq_segurado_ant_w,
		nr_seq_plano_atual_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_beneficiario_p;
	
	select	nr_seq_plano
	into	nr_seq_plano_ant_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_ant_w;
end if;

/*Caso o o beneficiario nao tiver um produto atual*/
if	((nr_seq_plano_atual_w is null) or
	(nr_seq_plano_atual_w = 0)) then
	goto final;
end if;

/* Obter dados do produto antigo */
select	ie_acomodacao,
	ie_preco
into	ie_acomodacao_antigo_w,
	ie_preco_ant_w
from	pls_plano
where	nr_sequencia	= nr_seq_plano_ant_w;

/* Obter dados do produto atual */
select	ie_acomodacao,
	ie_preco
into	ie_acomodacao_atual_w,
	ie_preco_atual_w
from	pls_plano
where	nr_sequencia	= nr_seq_plano_atual_w;

open C01;
loop
fetch C01 into	
	nr_seq_regra_w;
exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into	
		nr_seq_tipo_carencia_w,
		qt_dias_carencia_w,
		ie_inicio_vig_carencia_w,
		vl_mensalidade_w,
		ie_tipo_regra_w,
		ie_adaptar_carencia_rede_w;
	exit when C02%notfound;
		begin
		if	(ie_tipo_regra_w = 'C') then
			if	(ie_adaptar_carencia_rede_w = 'N') then
				pls_lanc_auto_item_carencia(nr_seq_beneficiario_p, nr_seq_regra_w, nr_seq_tipo_carencia_w,
							qt_dias_carencia_w, ie_inicio_vig_carencia_w, nm_usuario_p);
			else
				select	max(nr_seq_contrato)
				into	nr_seq_contrato_w
				from	pls_segurado
				where	nr_sequencia	= nr_seq_beneficiario_p;

				if	(nr_seq_contrato_w is not null) then
					select	nvl(ie_consistir_carencia_rede,'N')
					into	ie_consistir_carencia_rede_w
					from	pls_contrato
					where	nr_sequencia	= nr_seq_contrato_w;
				end if;
				if	(ie_consistir_carencia_rede_w = 'S') then
					
					if	(ie_inicio_vig_carencia_w	= 'A') then
						dt_inicio_vigencia_w		:= sysdate;
					elsif	(ie_inicio_vig_carencia_w	= 'I') then
						select	nvl(max(dt_inclusao_operadora),'')
						into	dt_inicio_vigencia_w
						from	pls_segurado
						where	nr_sequencia	= nr_seq_beneficiario_p;
					elsif	(ie_inicio_vig_carencia_w	= 'P') then
						if	(ie_origem_alt_prod_p	= 'A') then
							select	dt_alteracao
							into	dt_inicio_vigencia_w
							from	pls_segurado_alt_plano
							where	nr_seq_segurado	= nr_seq_beneficiario_p
							and	nr_sequencia	=	(	select	max(nr_sequencia) 
												from 	pls_segurado_alt_plano 
												where 	nr_seq_segurado = nr_seq_beneficiario_p
												and	ie_situacao = 'A'
												and	trunc(dt_atualizacao) = trunc(sysdate));
						else
							dt_inicio_vigencia_w		:= sysdate;
						end if;
					end if;
					
					pls_adaptar_rede_atend_produto(nr_seq_plano_atual_w,nr_seq_plano_ant_w,nr_seq_beneficiario_p,dt_inicio_vigencia_w,nr_seq_regra_w,cd_estabelecimento_p,nm_usuario_p);
				end if;
			end if;
		elsif	(ie_tipo_regra_w = 'M') then
			pls_lanc_auto_item_mensalidade(nr_seq_beneficiario_p, vl_mensalidade_w, cd_estabelecimento_p, nm_usuario_p);
		end if;
		end;
	end loop;
	close C02;	
	end;
end loop;
close C01;

/*Copiar as carencias para o beneficiario quando for alteracao de produto, migracao nao e necessario*/
if	(ie_origem_alt_prod_p	= 'A') and
	(ie_copiar_carencia_prod_novo_p = 'S') then
	pls_copiar_carencia_alt_prod(nr_seq_beneficiario_p,nr_seq_plano_atual_w,nr_seq_plano_ant_w,cd_estabelecimento_p,'N',nm_usuario_p);
end if;

<<final>>
nr_seq_plano_atual_w	:= nr_seq_plano_atual_w;

end pls_lanc_auto_alt_produto;
/
