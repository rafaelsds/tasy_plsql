create or replace
procedure	INSERT_RULE_FUNC_POPUP(
				nr_seq_lote_p				number,
				cd_funcao_p					number,
				nr_seq_parametro_p			number,
				vl_param_origem_p			varchar2,
				cd_estab_param_p			number,
				cd_perfil_param_p			number,
				nm_usuario_param_p			varchar2,
				nr_seq_menu_item_p			number,
				vl_parametro_p				varchar2,
				nm_usuario_p				varchar2) is
				
nr_sequencia_w				funcao_popup_regra.nr_sequencia%type;				

begin

select	FUNCAO_POPUP_REGRA_seq.nextval
into	nr_sequencia_w
from	dual;

insert into FUNCAO_POPUP_REGRA(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nm_usuario_regra,
		cd_perfil,
		nr_seq_objeto,
		ie_enable,
		ie_visible)
values
		(nr_sequencia_w,
		cd_estab_param_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nm_usuario_param_p,
		cd_perfil_param_p,
		nr_seq_menu_item_p,
		vl_parametro_p,
		vl_parametro_p);

html_param_converter_log('FUNCAO_POPUP_REGRA',
						nr_sequencia_w,
						cd_estab_param_p,
						cd_perfil_param_p,
						nm_usuario_param_p,
						cd_funcao_p,
						nr_seq_parametro_p,
						vl_param_origem_p,
						nr_seq_lote_p,
						nm_usuario_p);		

end INSERT_RULE_FUNC_POPUP;
/