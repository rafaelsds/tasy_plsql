create or replace
procedure Gerar_itens_score_flex_2(	nr_sequencia_p		number,
					nr_seq_escala_p		number,					
					nr_atendimento_p	number,
					nm_usuario_p		Varchar2) is 

nr_seq_item_w	Number(10);					

Cursor C01 is
	select	nr_sequencia	
	from	eif_escala_ii_item
	where	nr_seq_escala = nr_seq_escala_p
	and	ie_situacao = 'A'
	order by nr_sequencia;

begin

open C01;
loop
fetch C01 into	
	nr_seq_item_w;
exit when C01%notfound;
	
	Insert into Escala_eif_II_item (nr_sequencia,
					nr_seq_escala,
					nr_seq_item,
					nm_usuario,
					nm_usuario_nrec,
					dt_atualizacao,
					dt_atualizacao_nrec
					)
				values( 
					Escala_eif_II_item_seq.nextval,
					nr_sequencia_p,
					nr_seq_item_w,
					nm_usuario_p,
					nm_usuario_p,
					sysdate,
					sysdate
					);	
	
end loop;
close C01;

commit;

end Gerar_itens_score_flex_2;
/