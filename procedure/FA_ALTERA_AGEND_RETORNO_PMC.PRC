create or replace
procedure fa_altera_agend_retorno_pmc(nr_seq_paciente_pmc_p	number,
					nr_seq_agenda_p		varchar2) is 
					
dt_agendamento_w	date;

begin

if (nr_seq_agenda_p <> 0) and (nr_seq_agenda_p is not null) then
	select	dt_agenda
	into	dt_agendamento_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_p;
		
	if (dt_agendamento_w is not null) then
		update	fa_paciente_pmc
		set	dt_retorno = dt_agendamento_w
		where	nr_sequencia = nr_seq_paciente_pmc_p;
	end if;		
end if;

commit;

end fa_altera_agend_retorno_pmc;
/
