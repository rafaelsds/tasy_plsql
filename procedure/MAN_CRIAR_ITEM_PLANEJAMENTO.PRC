create or replace
procedure man_criar_item_planejamento(nr_sprint_p	number,
			nr_time_p	number,
			nr_release_p	number,
			ds_item_p	varchar2,
			nm_usuario_p	varchar2,
			nr_sequencia_p	out	number) is 

begin

select	desenv_item_planejamento_seq.nextval
into	nr_sequencia_p
from	dual;

insert into desenv_item_planejamento(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_item,
	nr_release,
	nr_sprint,
	nr_time)
values(	nr_sequencia_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	ds_item_p,
	nr_release_p,
	nr_sprint_p,
	nr_time_p);

commit;

end man_criar_item_planejamento;
/