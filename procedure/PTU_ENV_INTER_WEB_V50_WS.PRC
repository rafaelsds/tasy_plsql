create or replace
procedure ptu_env_inter_web_v50_ws (	nr_seq_guia_p				pls_guia_plano.nr_sequencia%type,
					nr_seq_req_p				pls_requisicao.nr_sequencia%type,					
					ds_transacao_p				Varchar2,
					ds_observacao_p				Varchar2,
					nm_usuario_p				usuario.nm_usuario%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nr_seq_execucao_p		out	ptu_cancelamento.nr_seq_execucao%type) is 

begin

ptu_envio_scs_ws_pck.ptu_envio_intercambio_portal(	nr_seq_guia_p, nr_seq_req_p, ds_transacao_p,
							ds_observacao_p, nm_usuario_p, cd_estabelecimento_p,
							nr_seq_execucao_p);

end ptu_env_inter_web_v50_ws;
/