create or replace
procedure inventario_item_ambos(
				nr_inventario_p		number,
				cd_material_p		number,
				cd_mat_estoque_p		number,
				qt_diferenca_p		number,
				qt_inventario_p		number,
				cd_unidade_est_p		varchar2,
				nr_seq_lote_fornec_p	number,
				cd_local_estoque_p	number,
				dt_mesano_ref_p		date,
				ds_observacao_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

cd_operacao_saida_w		operacao_estoque.cd_operacao_estoque%type;
qt_material_estoque_w		movimento_estoque.qt_estoque%type;
qt_estoque_consig_w		movimento_estoque.qt_estoque%type;
qt_dif_estoque_w			movimento_estoque.qt_estoque%type;
nr_movimento_w			movimento_estoque.nr_movimento_estoque%type;
cd_cgc_fornec_ant_w		pessoa_juridica.cd_cgc%type;
cd_cgc_fornec_w			pessoa_juridica.cd_cgc%type;

ds_erro_w			varchar2(2000);

begin
sup_verifica_cria_operacao(cd_estabelecimento_p, 18, nm_usuario_p, cd_operacao_saida_w);

qt_material_estoque_w	:= qt_diferenca_p;
qt_dif_estoque_w		:= 0;
cd_cgc_fornec_ant_w	:= '0';

while (qt_material_estoque_w > 0) loop
	begin
	cd_cgc_fornec_w	:= substr(obter_fornecedor_regra_consig(cd_estabelecimento_p, cd_mat_estoque_p, '1', cd_local_estoque_p),1,14);
	obter_saldo_estoque_consignado(cd_estabelecimento_p, cd_cgc_fornec_w, cd_mat_estoque_p, cd_local_estoque_p, sysdate, qt_estoque_consig_w);

	if (qt_estoque_consig_w > 0) then
		if (qt_estoque_consig_w < qt_material_estoque_w) then
			qt_dif_estoque_w		:= qt_material_estoque_w - qt_estoque_consig_w;
			qt_material_estoque_w	:= qt_estoque_consig_w;
		end if;
		
		select	movimento_estoque_seq.nextval
		into	nr_movimento_w
		from	dual;

		insert into movimento_estoque(
			nr_movimento_estoque,
			cd_estabelecimento,
			cd_local_estoque,
			dt_movimento_estoque,
			cd_operacao_estoque,
			cd_acao,
			cd_material,
			dt_mesano_referencia,
			qt_movimento,
			dt_atualizacao,
			nm_usuario,
			ie_origem_documento,
			ie_origem_proced,
			nr_documento,
			cd_unidade_medida_estoque,
			qt_estoque,
			dt_processo,
			cd_unidade_med_mov,
			cd_material_estoque,
			qt_inventario,
			cd_fornecedor,
			ie_movto_consignado,
			ds_observacao)
		values(nr_movimento_w,
			cd_estabelecimento_p,
			cd_local_estoque_p,
			sysdate,
			cd_operacao_saida_w,
			1,
			cd_material_p,
			dt_mesano_ref_p,
			qt_material_estoque_w,
			sysdate,
			nm_usuario_p,
			5,
			1,
			nr_inventario_p,
			cd_unidade_est_p,
			qt_material_estoque_w,
			null,
			cd_unidade_est_p,
			cd_mat_estoque_p,
			qt_inventario_p,
			cd_cgc_fornec_w,
			'C',
			ds_observacao_p);
			
		qt_material_estoque_w := qt_material_estoque_w - qt_estoque_consig_w;
		if (qt_dif_estoque_w > 0) then
			qt_material_estoque_w := qt_dif_estoque_w;
			qt_dif_estoque_w := 0;
		end if;
	else
		if	(cd_cgc_fornec_ant_w <> cd_cgc_fornec_w) then
			cd_cgc_fornec_ant_w := cd_cgc_fornec_w;
		else
			exit;
		end if;
	end if;
	exception
		when others then
			ds_erro_w := substr(sqlerrm,1,2000);
	end;
end loop;

end inventario_item_ambos;
/