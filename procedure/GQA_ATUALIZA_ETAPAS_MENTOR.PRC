CREATE OR REPLACE PROCEDURE gqa_atualiza_etapas_mentor(nr_atendimento_p IN NUMBER) IS

  CURSOR c_etapas_abertas IS
    SELECT e.nr_sequencia
          ,a.ie_tipo_acao
      FROM gqa_protocolo_pac       p
          ,gqa_protocolo_etapa_pac e
          ,gqa_acao                a
     WHERE p.nr_sequencia = e.nr_seq_prot_pac
       AND e.nr_seq_acao = a.nr_sequencia
       AND p.nr_atendimento = nr_atendimento_p
       AND p.ie_situacao = 'A'
       AND p.dt_liberacao IS NOT NULL
       AND p.dt_inativacao IS NULL
       AND p.dt_termino IS NULL
       AND e.dt_cancelar_usuario IS NULL
       AND e.dt_fim IS NULL;

  prescr_executada_w VARCHAR2(1);
BEGIN
  FOR r1 IN c_etapas_abertas LOOP
    IF r1.ie_tipo_acao = 'EP' THEN
      prescr_executada_w := obter_se_prescricao_executada(r1.nr_sequencia);
    
      IF (prescr_executada_w = 'S') THEN
        gqa_calc_resultado_acao(r1.nr_sequencia, NULL, NULL);
        gqa_acao_etapa(r1.nr_sequencia, NULL, NULL, 'F');
      END IF;
    END IF;
  END LOOP;
END gqa_atualiza_etapas_mentor;
/
