CREATE OR REPLACE
PROCEDURE Simular_Valor_Conta(
		nr_interno_conta_p 	Number,
		cd_convenio_p		Number,
		cd_categoria_p		Varchar2,
		nm_usuario_p		Varchar2) IS

nr_seq_valor_w			number(10)		:= 0;
cd_convenio_w			number(5)		:= 0;
cd_categoria_w			varchar2(10);
nr_sequencia_w			number(10)		:= 0;
vl_material_w			number(15,4)		:= 0;
cd_estabelecimento_w		number(4,0)		:= 0;
dt_conta_w				date			:= sysdate;
cd_material_w			number(6)		:= 0;
vl_preco_material_w		number(15,4)		:= 0;
dt_ultima_vigencia_w		date;
cd_tab_preco_mat_w		number(4)		:= 0;
ie_origem_preco_w			number(2)		:= 0;
qt_material_w			number(15,4)		:= 0;
ie_valor_informado_w		varchar2(1);
nr_atendimento_w			number(10,0)		:= 0;
dt_atendimento_w			date;
ie_tipo_convenio_w		number(1)		:= 0;
ie_classif_contabil_w		varchar2(1);
cd_autorizacao_w			varchar2(20);
cd_senha_w				varchar2(20);
QT_AUTORIZADA_W           	NUMBER(11,3) := 0;
NR_SEQ_AUTORIZACAO_W      	NUMBER(10);
IE_GLOSA_W            	  	VARCHAR2(1);
NM_RESPONSAVEL_W          	VARCHAR2(40);
CD_SITUACAO_GLOSA_W       	NUMBER(2)	   := 0;
IE_COBRA_PACIENTE_W       	VARCHAR2(1)  := 'N';
CD_ACAO_W            	  	VARCHAR2(1);
NR_PRESCRICAO_W	  	  	NUMBER(10);
NR_SEQUENCIA_PRESCRICAO_W 	NUMBER(6);
CD_MATERIAL_PRESCRICAO_W  	NUMBER(6);
VL_MATERIAL_ORIG_W        	NUMBER(15,4) := 0;
VL_UNITARIO_ORIG_W        	NUMBER(15,4) := 0;
DT_ULT_VIGENCIA_W         	DATE         := SYSDATE;
VL_TOTAL_MATERIAL_W       	NUMBER(15,4) := 0;
VL_UNITARIO_W             	NUMBER(15,4) := 0;
CD_TIPO_ACOMODACAO_W          NUMBER(4);
IE_TIPO_ATENDIMENTO_W         NUMBER(3);
CD_SETOR_ATENDIMENTO_W        NUMBER(5);
cd_cgc_fornecedor_w		varchar2(14) := '';
pr_glosa_w			number(7,4);
vl_glosa_w			number(15,4);
cd_motivo_exc_conta_w	NUmber(15,0);
ie_autor_particular_w		varchar2(1)	:= 'N';

cd_convenio_glosa_ww		number(5,0) := 0;
cd_categoria_glosa_ww		varchar2(10):= ' ';
nr_seq_regra_ajuste_ww		number(10,0):= 0;
nr_seq_bras_preco_w		number(10,0);
nr_seq_mat_bras_w		number(10,0);
nr_seq_conv_bras_w		number(10,0);
nr_seq_conv_simpro_w		number(10,0);
nr_seq_mat_simpro_w		number(10,0);
nr_seq_simpro_preco_w		number(10,0);
nr_seq_ajuste_mat_w		number(10,0);
nr_seq_classif_atend_w		atendimento_paciente.nr_seq_classificacao%type;


Cursor c01 is
select 	a.nr_sequencia,
		a.cd_material,
		a.qt_material,
		nvl(a.dt_conta,nvl(a.dt_prescricao,a.dt_atendimento)),
		nvl(a.ie_valor_informado,'N'),
		a.nr_atendimento,
		a.dt_atendimento,
		b.ie_cobra_paciente,
		a.cd_acao,
		a.nr_prescricao,
		a.nr_sequencia_prescricao,
		a.cd_material_prescricao,
		nvl(a.vl_material,0),
		nvl(a.vl_unitario,0),
		cd_cgc_fornecedor
from 		material b,
		material_atend_paciente a
where 	a.cd_material		= b.cd_material
and 		a.nr_interno_conta 	= nr_interno_conta_p
order by	a.cd_material,
		nvl(a.dt_conta,nvl(a.dt_prescricao,a.dt_atendimento));

BEGIN
CD_TIPO_ACOMODACAO_W          := NULL;
IE_TIPO_ATENDIMENTO_W         := NULL;
CD_SETOR_ATENDIMENTO_W        := NULL;

cd_convenio_w	:= cd_convenio_p;
cd_categoria_w	:= cd_categoria_p;

begin
delete from Mat_atend_paciente_valor
where ie_tipo_valor = 2
and	nr_seq_material in(select nr_sequencia from material_atend_paciente
				where nr_interno_conta = nr_interno_conta_p);
end;
commit;

select	a.cd_estabelecimento,
	a.nr_seq_classificacao
into	cd_estabelecimento_w,
	nr_seq_classif_atend_w
from	atendimento_paciente a,
	conta_paciente b
where	a.nr_atendimento = b.nr_atendimento
and	b.nr_interno_conta = nr_interno_conta_p; 

OPEN C01;
LOOP
FETCH C01 into	nr_sequencia_w,
			cd_material_w,
			qt_material_w,
			dt_conta_w,
			ie_valor_informado_w,
			nr_atendimento_w,
			dt_atendimento_w,
			ie_cobra_paciente_w,
			cd_acao_w,
			nr_prescricao_w,
			nr_sequencia_prescricao_w,
			cd_material_prescricao_w,
			vl_material_w,
			vl_unitario_w,
			cd_cgc_fornecedor_w;
EXIT WHEN C01%NOTFOUND;
	BEGIN
	vl_preco_material_w	:= 0;
	cd_convenio_w		:= cd_convenio_p;
	cd_categoria_w		:= cd_categoria_p;
	Glosa_Material
				(CD_ESTABELECIMENTO_W,
				NR_ATENDIMENTO_W,
           			DT_ATENDIMENTO_W,
           			CD_MATERIAL_W,
           			QT_MATERIAL_W,
				CD_TIPO_ACOMODACAO_W,
				IE_TIPO_ATENDIMENTO_W,
				CD_SETOR_ATENDIMENTO_W,0,null, null,null,null,
           			CD_CONVENIO_W,
           			CD_CATEGORIA_W,
				IE_TIPO_CONVENIO_W,
				IE_CLASSIF_CONTABIL_W,
           			CD_AUTORIZACAO_W,
				NR_SEQ_AUTORIZACAO_W,
           			QT_AUTORIZADA_W,
           			CD_SENHA_W,
           			NM_RESPONSAVEL_W,
				IE_GLOSA_W,
				CD_SITUACAO_GLOSA_W,
				pr_glosa_w,
				vl_glosa_w,
				cd_motivo_exc_conta_w,
				ie_autor_particular_w,
				cd_convenio_glosa_ww,
				cd_categoria_glosa_ww,
				nr_seq_regra_ajuste_ww,
				0);

	if  	(IE_COBRA_PACIENTE_W = 'S') 	then
		Define_Preco_Material
      	      	(CD_ESTABELECIMENTO_W,
	      		CD_CONVENIO_W,
            		CD_CATEGORIA_W,
            		DT_CONTA_W,
            		CD_MATERIAL_W,
				CD_TIPO_ACOMODACAO_W,
				IE_TIPO_ATENDIMENTO_W,
				CD_SETOR_ATENDIMENTO_W,
				cd_cgc_fornecedor_w,
				0, 0, null,null, null, null, null, nr_seq_classif_atend_w, null, null,
	            		VL_PRECO_MATERIAL_W,
        	    		DT_ULT_VIGENCIA_W,
				CD_TAB_PRECO_MAT_W,
				IE_ORIGEM_PRECO_W,
				nr_seq_bras_preco_w,
				nr_seq_mat_bras_w,
				nr_seq_conv_bras_w,
				nr_seq_conv_simpro_w,
				nr_seq_mat_simpro_w,
				nr_seq_simpro_preco_w,
				nr_seq_ajuste_mat_w);
	end if;

	VL_TOTAL_MATERIAL_W := (VL_PRECO_MATERIAL_W * QT_MATERIAL_W); 

	begin
	select nvl(max(nr_sequencia),0) + 1
	into 	nr_seq_valor_w
	from Mat_atend_paciente_valor
	where nr_seq_material 	= nr_sequencia_w;

     	Insert into Mat_atend_paciente_valor
		(nr_seq_material,
		nr_sequencia,
		ie_tipo_valor,
		dt_atualizacao,
		nm_usuario,
		vl_material,
		cd_convenio,
		cd_categoria)
	values
		(nr_sequencia_w,
		nr_seq_valor_w,
		2,
		sysdate,
		nm_usuario_p,
		vl_total_material_w,
		cd_convenio_w,
		cd_categoria_w);
	commit;
	end;
	END;
END LOOP;
close c01;

END Simular_Valor_Conta;
/