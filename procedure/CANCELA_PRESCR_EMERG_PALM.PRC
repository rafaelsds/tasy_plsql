create or replace
procedure cancela_prescr_emerg_palm(nr_prescricao_p		number) is 

qt_item_prescricao_w	number(10);

begin

if 	(nr_prescricao_p is not null) then

	select 	count(*)
	into	qt_item_prescricao_w
	from	prescr_material
	where	nr_prescricao	=	nr_prescricao_p;
	
	if (qt_item_prescricao_w = 0) then
		delete 	from prescr_medica 
		where	nr_prescricao 	=	nr_prescricao_p;
	end if;
end if;

commit;

end cancela_prescr_emerg_palm;
/