create or replace
procedure pls_consist_lim_ocorr_req_guia
			(	nr_seq_segurado_p	number,
				nr_seq_requisicao_p	number,
				nr_seq_guia_p		number,
				ie_tipo_intercambio_p	varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Consistir liminar ocorrência requisição ou guia
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_auditoria_w		number(10);
nr_seq_regra_liminar_w		number(10);
ie_utiliza_nivel_w		varchar2(10);

begin

if	(nr_seq_guia_p is not null) then
	pls_gerar_ocorr_liminar_guia(nr_seq_segurado_p, null, nr_seq_guia_p, nm_usuario_p, nr_seq_regra_liminar_w);

	begin
	select	nr_sequencia
	into	nr_seq_auditoria_w
	from	pls_auditoria
	where	nr_seq_guia	= nr_seq_guia_p;
	exception
	when others then
		nr_seq_auditoria_w	:= null;
	end;

	if	(nr_seq_regra_liminar_w	is not null) and
		(nr_seq_auditoria_w is not null) and
		(nvl(ie_tipo_intercambio_p,'X') <> 'I') then
		-- OS - 421626 - Rotina para gerar as ocorrências e glosas na tabela "pls_analise_ocor_glosa_aut"
		ie_utiliza_nivel_w := pls_obter_se_uti_nivel_lib_aut(cd_estabelecimento_p);
		if	(ie_utiliza_nivel_w = 'S') then
			pls_gerar_ocor_glosa_aud_limi(nr_seq_auditoria_w, 0, nr_seq_guia_p, nr_seq_regra_liminar_w, nm_usuario_p);
		end if;
	end if;
elsif	(nr_seq_requisicao_p is not null) then
	pls_gerar_ocorr_liminar_judic(	nr_seq_segurado_p,
					null,
					nr_seq_requisicao_p,
					nm_usuario_p,
					nr_seq_regra_liminar_w,
					cd_estabelecimento_p);
	
	begin
	select	nr_sequencia
	into	nr_seq_auditoria_w
	from	pls_auditoria
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	exception
	when others then
		nr_seq_auditoria_w	:= null;
	end;

	if	(nr_seq_regra_liminar_w	is not null) and
		(nr_seq_auditoria_w is not null)  and
		(nvl(ie_tipo_intercambio_p,'X') <> 'I') then
		-- OS - 421626 - Rotina para gerar as ocorrências e glosas na tabela "pls_analise_ocor_glosa_aut" 
		ie_utiliza_nivel_w	:= pls_obter_se_uti_nivel_lib_aut(cd_estabelecimento_p); 
		
		if	(ie_utiliza_nivel_w = 'S') then
			pls_gerar_ocor_glosa_aud_limi(	nr_seq_auditoria_w,
							nr_seq_requisicao_p,
							0,
							nr_seq_regra_liminar_w,
							nm_usuario_p);
		end if;
	end if;
end if;

end pls_consist_lim_ocorr_req_guia;
/