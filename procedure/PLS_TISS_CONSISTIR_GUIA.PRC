create or replace
procedure pls_tiss_consistir_guia
			(	nr_sequencia_p		number,
				ie_tipo_glosa_p		varchar2,
				ie_evento_p		varchar2,
				nr_seq_prestador_p	number,
				nr_seq_ocorrencia_p	number,
				ds_parametro_um_p	varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 
				
/* IE_TIPO_GLOSA_P
	C - Conta
	CP - Conta procedimento
	CM - Conta material
	A - Autoriza��o
	AP - Autoriza��o procedimento
	AM - Autoriza��o material */

nm_prestador_imp_w		varchar2(255);
nm_prestador_w			varchar2(255);
ds_observacao_w			varchar2(255)	:= null;
cd_guia_w			varchar2(20);
cd_guia_referencia_w		varchar2(20);
cd_guia_referencia_ww		varchar2(20);
ie_guia_indevida_w		varchar2(1)	:= 'S';
ie_internado_w			varchar2(1);
ds_retorno_w			varchar2(1);
nr_seq_segurado_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_protocolo_w		number(10);
nr_seq_fatura_w			number(10);
dt_validade_senha_w		date;
dt_emissao_w			date;
dt_emissao_imp_w		date;
dt_procedimento_w		date;
dt_emissao_conta_glosa_w	date;
cd_guia_pesquisa_w		varchar2(20);
nr_seq_lote_w			number(10);
qt_guia_duplic_a500_w		number(10);
nr_protocolo_prestador_w	varchar2(20);
qt_consistir_guia_w		number(10);

nr_seq_conta_glosa_w		pls_conta.nr_sequencia%type;

begin
if	(ie_tipo_glosa_p = 'C') then
	/* Obter dados da conta */
	begin
		select	nvl(cd_guia,cd_guia_imp),
			nvl(cd_guia_referencia,cd_guia_solic_imp),
			nvl(cd_guia_referencia, cd_guia),
			nr_seq_segurado,
			trunc(dt_atendimento_referencia),
			dt_emissao_imp,
			nr_seq_fatura
		into	cd_guia_w,
			cd_guia_referencia_ww,
			cd_guia_referencia_w,
			nr_seq_segurado_w,
			dt_emissao_w,
			dt_emissao_imp_w,
			nr_seq_fatura_w
		from	pls_conta
		where	nr_sequencia	= nr_sequencia_p;
	exception
	when others then
		nr_seq_segurado_w	:= null;
	end;	
	
	ie_internado_w	:= pls_obter_se_internado(nr_sequencia_p,'C');
	
	if	(ie_evento_p = 'IA') then
		cd_guia_referencia_w	:= nvl(cd_guia_referencia_ww, cd_guia_w);
		dt_emissao_w		:= dt_emissao_imp_w;
	end if;

	select	max(trunc(dt_validade_senha))
	into	dt_validade_senha_w
	from	pls_guia_plano
	where	cd_guia		= cd_guia_referencia_w 
	and	nr_seq_segurado = nr_seq_segurado_w;

	/* Obter a quantidade de guias iguais apresentadas para o mesmo benefici�rio e prestador executor 
	OS 279251 - Vagner informou que deve considera a guia e o benefici�rio para a contagem de guias j� apresentadas
	*/
	/* Felipe - 28/03/2011 - OS 279251 - Hist�rico 23/03/2011 15:05:58 */
	
	cd_guia_pesquisa_w	:= pls_converte_cd_guia_pesquisa(cd_guia_w);
	
	begin
		select	b.nr_seq_lote_conta,
			b.nr_protocolo_prestador
		into	nr_seq_lote_w,
			nr_protocolo_prestador_w
		from	pls_protocolo_conta b,
			pls_conta a
		where	a.nr_seq_protocolo 	= b.nr_sequencia
		and	a.nr_sequencia		= nr_sequencia_p;
	exception
		when others then
		nr_seq_lote_w			:= null;
		nr_protocolo_prestador_w	:= null;
	end;
	
	select	count(1)
	into	qt_consistir_guia_w
	from	pls_ocorrencia_conta_web a
	where	a.ie_consistir_cod_guia = 'S';
	
	/* Francisco - 30/01/2013 - Tratamento provis�rio at� criarmos uma glosa
	para garantir a n�o duplicidade de guias */
	if	(qt_consistir_guia_w > 0) then
		select	max(a.nr_sequencia)
		into	nr_seq_conta_w
		from 	pls_protocolo_conta	b,
			pls_conta		a
		where	b.nr_sequencia		= a.nr_seq_protocolo
		and	a.cd_guia_pesquisa 	= cd_guia_pesquisa_w
		and	a.nr_seq_segurado	= nr_seq_segurado_w
		and 	(
			((b.nr_seq_lote_conta is null) or (b.nr_seq_lote_conta != nr_seq_lote_w)) or
			((b.nr_protocolo_prestador is null) or (b.nr_protocolo_prestador != nr_protocolo_prestador_w)) 
			)
		and	a.ie_status 	<> 'C'
		and	b.ie_situacao 	<> 'RE'
		and	a.nr_sequencia 	<> nr_sequencia_p;
	end if;
		
	if	(nvl(nr_seq_conta_w,0) = 0) then
		if	(ie_internado_w = 'S') then
			select	max(a.nr_sequencia)
			into	nr_seq_conta_w
			from	pls_conta	a
			where	a.nr_sequencia		!= nr_sequencia_p
			and	a.cd_guia_referencia	= cd_guia_referencia_ww	
			and	a.cd_guia		= cd_guia_w
			and	a.nr_seq_segurado	= nr_seq_segurado_w
			and	a.ie_status		!= 'C'
			and	(a.ie_glosa <> 'S' 	or a.ie_glosa is null)
			and	((not exists	(select	1
						from	ptu_fatura	x
						where	x.nr_sequencia	= a.nr_seq_fatura
						and	x.ie_status	= 'CA'
						and	not exists(	select	1
										from	ptu_nota_cobranca	z
										where	x.nr_sequencia = z.nr_seq_fatura
										and	cd_excecao not in('I','E')))) or
				(a.nr_seq_fatura is null));
		else
			--Incluido tratamento para verificar tamb�m se a guia refer�ncia � igual a existente em outra conta OS 654274 dgkorz
			select	max(a.nr_sequencia)
			into	nr_seq_conta_w
			from	pls_conta	a
			where	a.nr_sequencia		!= nr_sequencia_p
			and	((a.cd_guia_referencia	= cd_guia_referencia_ww) or 
				(a.cd_guia_referencia	is null and cd_guia_referencia_ww is null))
			and	a.cd_guia		= cd_guia_w
			and	a.nr_seq_segurado	= nr_seq_segurado_w
			and	a.ie_status		!= 'C'
			and	(a.ie_glosa <> 'S' or a.ie_glosa is null)
			and	((not exists	(	select	1
							from	ptu_fatura	x
							where	x.nr_sequencia	= a.nr_seq_fatura
							and	x.ie_status	= 'CA'
							and	not exists(	select	1
										from	ptu_nota_cobranca	z
										where	x.nr_sequencia = z.nr_seq_fatura
										and	cd_excecao not in('I','E')))) or
				(a.nr_seq_fatura is null));
		end if;
		
		/*Tratamento para verificar se a guia de intercambio A500 � uma reapresenta��o*/
		
		if	(ie_evento_p = 'I5')	then
			select 	count(1)
			into	qt_guia_duplic_a500_w
			from	pls_conta		a,
				ptu_fatura		b,
				ptu_nota_cobranca	c
			where	c.nr_seq_fatura = b.nr_sequencia
			and	a.nr_seq_fatura = b.nr_sequencia
			and	a.nr_sequencia 	= nr_sequencia_p
			and	c.cd_excecao	in('E','I');
			
			if	(qt_guia_duplic_a500_w	> 0)	then
				nr_seq_conta_w := null;
			end if;
		end if;
	end if;

	if	(nvl(nr_seq_conta_w,0) <> 0) then
		begin
			select	nvl(dt_atendimento_referencia,dt_emissao_imp),
				nr_seq_protocolo
			into	dt_emissao_conta_glosa_w,
				nr_seq_protocolo_w
			from	pls_conta
			where	nr_sequencia	= nr_seq_conta_w;
		exception
		when others then
			nr_seq_protocolo_w	:= null;
		end;
		
		select	max(nm_prestador_imp),
			max(pls_obter_dados_prestador(nr_seq_prestador,'N'))
		into	nm_prestador_imp_w,
			nm_prestador_w
		from	pls_protocolo_conta
		where	nr_sequencia	= nr_seq_protocolo_w;
		
		nm_prestador_w	:= nvl(nm_prestador_w,nm_prestador_imp_w);		
	end if;
elsif	(ie_tipo_glosa_p = 'CP') then
	begin
		begin
		select	nvl(dt_procedimento,dt_procedimento_imp),
			nr_seq_conta
		into	dt_procedimento_w,
			nr_seq_conta_glosa_w
		from	pls_conta_proc
		where	nr_sequencia = nr_sequencia_p;
	exception
	when others then
		dt_procedimento_w	:= null;
	end;
	
	ie_guia_indevida_w	:= pls_obter_se_proc_tipo_guia(nr_sequencia_p,ie_tipo_glosa_p);
	ds_observacao_w		:= 'Conta: ' || nr_sequencia_p;
	ds_retorno_w		:= ie_guia_indevida_w;
	end;	
elsif	(ie_tipo_glosa_p = 'AP') then
	ie_guia_indevida_w	:= pls_obter_se_proc_tipo_guia(nr_sequencia_p,ie_tipo_glosa_p);
	ds_retorno_w		:= ie_guia_indevida_w;
end if;

if	(ie_tipo_glosa_p = 'C') then 
	if	(nr_seq_conta_w > 0) then /* 1308 - Guia j� apresentada*/
		pls_gravar_glosa_tiss('1308',
			'Prestador: '||nm_prestador_w||'; Protocolo: '||nr_seq_protocolo_w||'; Conta: '||nr_seq_conta_w||'; Dt. emiss�o:' ||dt_emissao_conta_glosa_w, ie_evento_p,
			nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
			nr_seq_ocorrencia_p, '', nm_usuario_p,
			cd_estabelecimento_p, null);
	end if;	

	if	(nr_seq_prestador_p = 0) then /* 1312 - Prestador contratado n�o informado*/
		pls_gravar_glosa_tiss('1312', 
			'Conta: ' || nr_sequencia_p, ie_evento_p,
			nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
			nr_seq_ocorrencia_p, '', nm_usuario_p,
			cd_estabelecimento_p, null);
	end if;

	if	(dt_validade_senha_w < dt_emissao_w) then /* 1321 - Validade da guia expirada*/
		pls_gravar_glosa_tiss('1321', 
			'Conta: ' || nr_sequencia_p, ie_evento_p,
			nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
			nr_seq_ocorrencia_p, '', nm_usuario_p,
			cd_estabelecimento_p, null);
	end if;
elsif	(ie_tipo_glosa_p = 'CP') then	
	if	(dt_procedimento_w is null) then /* 1317 - Guia sem data do atendimento*/
		pls_gravar_glosa_tiss('1317', 
			null, ie_evento_p,
			nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
			nr_seq_ocorrencia_p, '', nm_usuario_p,
			cd_estabelecimento_p, nr_seq_conta_glosa_w);
	end if;
end if;

/*1304 - Cobran�a em guia indevida*/
if	(ds_retorno_w = 'N') then
	pls_gravar_glosa_tiss('1304', ds_observacao_w, ie_evento_p, nr_sequencia_p, 
	ie_tipo_glosa_p, nr_seq_prestador_p, nr_seq_ocorrencia_p, '', nm_usuario_p,	
	cd_estabelecimento_p, nr_seq_conta_glosa_w);	
end if;

--commit;

end pls_tiss_consistir_guia;
/