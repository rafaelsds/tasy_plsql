create or replace
procedure gpi_excluir_importar_cron_js(	nr_seq_cronograma_p	number,
				nm_usuario_p		varchar2,
				ds_erro_p			out varchar2) is 

				
ds_titulo_w		varchar2(255)	:= '';
dt_aprovacao_w		date;
ds_erro_w		varchar2(255)	:= '';
nr_seq_crono_w		number(10);
qt_registro_w		number(10);

cursor c01 is
	select	a.nr_sequencia
	from	gpi_cron_etapa a
	where	a.nr_seq_cronograma = nr_seq_crono_w
	and	a.nr_seq_superior is null;

c01_w		c01%rowtype;

cursor c02 is
	select	a.nr_sequencia,
		level
	from	gpi_cron_etapa a
	connect by prior a.nr_sequencia = a.nr_seq_superior
	start with a.nr_sequencia  = c01_w.nr_sequencia
	order by level desc;
	

c02_w		c02%rowtype;

begin

select	max(ds_titulo),
	max(dt_aprovacao)
into	ds_titulo_w,
	dt_aprovacao_w
from	gpi_cronograma
where	nr_sequencia		= nr_seq_cronograma_p;

if	(dt_aprovacao_w is not null) then

	ds_erro_w		:=  substr(obter_texto_tasy(94772, null),1,255);
else
	qt_registro_w := 0;
	open c01;
	loop
	fetch c01 into	
		c01_w;
	exit when c01%notfound;
		begin
		open c02;
		loop
		fetch c02 into	
			c02_w;
		exit when c02%notfound;
			begin
			qt_registro_w := qt_registro_w + 1;
			delete from gpi_cron_etapa where nr_sequencia = c02_w.nr_sequencia;
			
			if	(qt_registro_w >= 100) then
				begin
				commit;
				qt_registro_w := 0;
				end;
			end if;
			end;
		end loop;
		close c02;
		end;
	end loop;
	close c01;
	commit;
end if;

ds_erro_p	:= ds_erro_w;

end gpi_excluir_importar_cron_js;
/