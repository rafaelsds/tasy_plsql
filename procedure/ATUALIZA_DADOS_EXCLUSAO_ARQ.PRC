create or replace
procedure atualiza_dados_exclusao_arq(
			ds_motivo_p			varchar2,
			lista_arquivos_p		varchar2,
			lista_cd_pessoa_fisica_p	varchar2,
			dt_filtro_ini_p			date,
			dt_filtro_fim_p			date) is 

/*	necess�rio passar um dos dois parametros:
		. lista_arquivos_p:
			informacoes registros separados por ponto e virgula no seguinte formato: nome da tabela + '=' + sequencia (pk) dos registros separados por virgula 
			exemplo: ANEXO_AGENDA=1,2,3;ANEXO_AGENDA_CONS=1,5,7;TRANSCRICAO_PRESCRICAO_ARQ=2,10,55
		. lista_cd_pessoa_fisica_p:
			codigos de pessoas separados por virgula
			exemplo: 1,23,456
			
	parametros dt_filtro_ini_p e dt_filtro_fim_p somente serao utilizados com o parametro lista_cd_pessoa_fisica_p
	
*/

cd_pessoa_fisica_w	varchar2(255);
nm_tabela_w		varchar2(255);
nr_sequencias_w		varchar2(255);
nm_usuario_w		usuario.nm_usuario%type;
ds_comando_w		varchar2(1000);
ds_parametros_w		varchar2(1000);

Cursor C01 is -- este select retorna um registro para cada tabela, um campo eh o nome da tabela, e o outro eh a lista de sequencias (pk) separados por virgula
	select substr(vlr, 0, instr(vlr,'=')-1) tabela, substr(vlr, instr(vlr,'=')+1) sequencias
	from (	select	regexp_substr(lista_arquivos_p, '[^; ]+', 1, level) vlr
		from	dual
		connect by regexp_substr(lista_arquivos_p, '[^; ]+', 1, level) is not null);
	
Cursor C02 is -- este cursor retorna uma linha para cada cd_pessoa_fisica
	select	regexp_substr(lista_cd_pessoa_fisica_p, '[^, ]+', 1, level) cd_pessoa_fisica
	from	dual
	connect by regexp_substr(lista_cd_pessoa_fisica_p, '[^, ]+', 1, level) is not null;

begin

nm_usuario_w := obter_usuario_ativo;

if	(lista_arquivos_p is not null) then

	open C01;
	loop
	fetch C01 into	
		nm_tabela_w,
		nr_sequencias_w;
	exit when C01%notfound;
		begin
		
		ds_comando_w := 'update	' || nm_tabela_w || ' set ' ||
				'	nm_usuario_exclusao = :nm_usuario_p, ' ||
				'	dt_exclusao = sysdate, ' ||
				'	ds_motivo_exclusao = :ds_motivo_p ' ||
				' where	dt_exclusao is null ' ||
				' and	nr_sequencia in (' || nr_sequencias_w || ')';
		
		ds_parametros_w := 	'NM_USUARIO_P='		|| nm_usuario_w ||';'||
					'DS_MOTIVO_P='		|| ds_motivo_p ||';';
		
		exec_sql_dinamico_bv('atualiza_dados_exclusao_arq', ds_comando_w, ds_parametros_w);


		
		end;
	end loop;
	close C01;
	
elsif	(lista_cd_pessoa_fisica_p is not null) then

	open C02;
	loop
	fetch C02 into	
		cd_pessoa_fisica_w;
	exit when C02%notfound;
		begin
			--Agenda de Exames
			update	ANEXO_AGENDA a
			set	a.nm_usuario_exclusao = nm_usuario_w,
				a.dt_exclusao = sysdate,
				a.ds_motivo_exclusao = ds_motivo_p
			where	a.dt_exclusao is null
			and	a.dt_atualizacao between dt_filtro_ini_p and dt_filtro_fim_p
			and	exists (	select	1
						from	AGENDA_PACIENTE b
						where	b.nr_sequencia = a.nr_seq_agenda
						and	b.cd_pessoa_fisica = cd_pessoa_fisica_w);

			--Agenda de Consultas e Servi�os
			update	ANEXO_AGENDA_CONS a
			set	a.nm_usuario_exclusao = nm_usuario_w,
				a.dt_exclusao = sysdate,
				a.ds_motivo_exclusao = ds_motivo_p
			where	a.dt_exclusao is null
			and	a.dt_atualizacao between dt_filtro_ini_p and dt_filtro_fim_p
			and	exists (	select	1
						from	AGENDA_CONSULTA b
						where	b.nr_sequencia = a.nr_seq_agenda_cons
						and	b.cd_pessoa_fisica = cd_pessoa_fisica_w);

			--Entrada �nica de Pacientes
			update	ATENDIMENTO_PACIENTE_ANEXO a
			set	a.nm_usuario_exclusao = nm_usuario_w,
				a.dt_exclusao = sysdate,
				a.ds_motivo_exclusao = ds_motivo_p
			where	a.dt_exclusao is null
			and	a.dt_atualizacao between dt_filtro_ini_p and dt_filtro_fim_p
			and	exists (	select	1
						from	ATENDIMENTO_PACIENTE b
						where	b.nr_atendimento = a.nr_atendimento
						and	b.cd_pessoa_fisica = cd_pessoa_fisica_w);

			--Laudo Paciente
			update	LAUDO_PACIENTE_IMAGEM a
			set	a.nm_usuario_exclusao = nm_usuario_w,
				a.dt_exclusao = sysdate,
				a.ds_motivo_exclusao = ds_motivo_p
			where	a.dt_exclusao is null
			and	a.dt_atualizacao between dt_filtro_ini_p and dt_filtro_fim_p
			and	exists (	select	1
						from	LAUDO_PACIENTE b
						where	b.nr_sequencia = a.nr_sequencia
						and	b.cd_pessoa_fisica = cd_pessoa_fisica_w);
			
			--Gest�o de Transcri��o
			update	TRANSCRICAO_PRESCRICAO_ARQ a
			set	a.nm_usuario_exclusao = nm_usuario_w,
				a.dt_exclusao = sysdate,
				a.ds_motivo_exclusao = ds_motivo_p
			where	a.dt_exclusao is null
			and	a.dt_atualizacao between dt_filtro_ini_p and dt_filtro_fim_p
			and	exists (	select	1
						from	TRANSCRICAO_PRESCRICAO b,
							ATENDIMENTO_PACIENTE c
						where	b.nr_sequencia = a.nr_seq_transcricao
						and	c.nr_atendimento = b.nr_atendimento
						and	c.cd_pessoa_fisica = cd_pessoa_fisica_w);
			
			--Cadastro Completo de Pessoas - Documenta��o
			update	PESSOA_DOCUMENTACAO a
			set	a.nm_usuario_exclusao = nm_usuario_w,
				a.dt_exclusao = sysdate,
				a.ds_motivo_exclusao = ds_motivo_p
			where	a.dt_exclusao is null
			and	a.dt_atualizacao between dt_filtro_ini_p and dt_filtro_fim_p
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_w;

		end;
	end loop;
	close C02;
	
end if;

commit;

end atualiza_dados_exclusao_arq;
/