create or replace
procedure eup_atualizar_tipo_convenio	(	nr_atendimento_p	Number,						
						cd_convenio_p		Number) is 

ie_atualizar_tipo_conv_atend_w	varchar2(1) := 'S';
ie_tipo_convenio_w		number(2)   := 2;

begin

if	(nvl(nr_atendimento_p,0) > 0) and
	(nvl(cd_convenio_p,0) > 0) then
	begin

	select	nvl(max(ie_atualizar_tipo_conv_atend),'S')
	into	ie_atualizar_tipo_conv_atend_w
	from	parametro_faturamento
	where	cd_estabelecimento = obter_estabelecimento_ativo;
	
	if	(ie_atualizar_tipo_conv_atend_w = 'S') then
		begin
		
		select	max(ie_tipo_convenio)
		into	ie_tipo_convenio_w
		from	convenio
		where	cd_convenio = cd_convenio_p;
		
		if	(nvl(ie_tipo_convenio_w,0) > 0) then
		
			update	atendimento_paciente
			set	ie_tipo_convenio = ie_tipo_convenio_w
			where	nr_atendimento 	 = nr_atendimento_p;

			commit;
		end if;
		end;
	end if;
	end;
end if;

end eup_atualizar_tipo_convenio;
/
