create or replace
procedure hef_importar_conta_contabil is 

cd_conta_contabil_w	varchar2(20);
cd_conta_contabil_ww	varchar2(20);
ds_conta_contabil_w	varchar2(255);
ds_conta_contabil_ww	varchar2(255)	:= '';
ie_centro_custo_w		varchar2(1);
ie_compensacao_w		varchar2(1);
ie_situacao_w		varchar2(1);
ie_tipo_w			varchar2(1);
cd_classificacao_w		varchar2(40);
cd_classif_ecd_w		varchar2(40);
cd_grupo_w		number(10);
ds_erro_w		varchar2(500);
qt_registro_w		number(10);

cursor c01 is
select	cd_conta,
	ds_conta,
	ie_centro,
	ie_sit	,
	cd_grupo,
	cd_classif,
	ie_tipo,
	ie_comp,
	cd_classi2
from	w_hef_conta_contabil;

begin


open C01;
loop
fetch C01 into	
	cd_conta_contabil_w,
	ds_conta_contabil_w,
	ie_centro_custo_w,
	ie_situacao_w,
	cd_grupo_w,
	cd_classificacao_w,
	ie_tipo_w,
	ie_compensacao_w,
	cd_classif_ecd_w;
exit when C01%notfound;
	begin
	
		
	cd_classificacao_w		:= replace(cd_classificacao_w, ' ', '');
	cd_conta_contabil_ww	:= substr(cd_conta_contabil_w,1,20);
	ds_conta_contabil_ww	:= substr(ltrim(ds_conta_contabil_w),1,255);
	
		
	begin
	insert into conta_contabil( 
		cd_conta_contabil,
		ds_conta_contabil,
		ie_situacao, 
		dt_atualizacao,
		nm_usuario,
		ie_centro_custo,
		dt_atualizacao_nrec,
		nm_usuario_nrec, 
		cd_grupo, 
		ie_tipo,
		cd_classificacao,
		cd_empresa,
		ie_compensacao,
		cd_classif_ecd,
		ie_ecd_reg_dre,
		ie_ecd_reg_bp)
	values(	cd_conta_contabil_ww,
		ds_conta_contabil_ww,
		ie_situacao_w,
		sysdate,
		'Importacao',
		ie_centro_custo_w, 
		sysdate,
		'Importacao',
		cd_grupo_w,
		ie_tipo_w,
		cd_classificacao_w, 
		7,
		ie_compensacao_w,
		cd_classif_ecd_w,
		'N',
		'N');
	exception when others then
		ds_erro_w := SQLERRM(sqlcode);
	
		insert 	into log_tasy(
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values(	sysdate,
			'Importacao',
			55755,
			'Conta: '|| cd_conta_contabil_w ||' Erro: '|| ds_erro_w);
	end;
	end;
end loop;
close C01;

commit;

end hef_importar_conta_contabil;
/