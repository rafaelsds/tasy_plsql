CREATE OR REPLACE PROCEDURE reg_undo_submit_pr_manager_vv
(
	nr_seq_product_req_p reg_product_requirement.nr_sequencia%TYPE,
	nm_usuario_p reg_product_requirement.nm_liberou_ger_vv%TYPE
) AS 
BEGIN
	UPDATE	reg_product_requirement
	SET	dt_liberou_ger_vv = null,
		nm_liberou_ger_vv = null,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	WHERE	nr_sequencia = nr_seq_product_req_p;
	
	COMMIT;
END reg_undo_submit_pr_manager_vv;
/