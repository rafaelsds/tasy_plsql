create or replace
procedure pls_gerar_usuario_web_envio
			(	nm_usuario_p		varchar2,
				nr_seq_usuario_web_p	number,
				ds_email_destino_p	varchar2,
				ds_observacao_p		varchar2) is 
				
begin

insert into pls_usuario_web_envio
	(nr_sequencia, dt_atualizacao, nm_usuario,
	nr_seq_usuario_web, dt_envio, ds_destino, 
	ds_observacao)
values (pls_usuario_web_envio_seq.nextval, sysdate, nm_usuario_p, 
	nr_seq_usuario_web_p, sysdate, ds_email_destino_p, 
	ds_observacao_p);

commit;	

end pls_gerar_usuario_web_envio;
/