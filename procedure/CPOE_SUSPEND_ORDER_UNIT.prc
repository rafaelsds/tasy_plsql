create or replace procedure CPOE_SUSPEND_ORDER_UNIT(
	nr_seq_cpoe_order_unit_p 	cpoe_order_unit.nr_sequencia%type, 
	nr_seq_cpoe_rp_p			cpoe_rp.nr_sequencia%type,
	nm_usuario_p 				cpoe_material.nm_usuario%type) as

nr_sequencia_w		cpoe_material.nr_sequencia%type;
ie_tipo_item_w		varchar(3);
nr_atendimento_w	cpoe_material.nr_atendimento%type;
ds_liberacao_w		varchar2(1);
ds_lista_w			varchar(2000);

ds_lista_itens_ret_w	varchar2(2000);
ds_lista_itens_ex_w		varchar2(2000);
ds_lista_sol_consistir_w	varchar2(2000);



cursor c01 is
select 	nr_sequencia,
		ie_tipo_item_cpoe,
  		nr_atendimento,
  		decode(dt_liberacao, null, 'N','S') ds_liberacao
from	cpoe_itens_v 
where 	NR_SEQ_CPOE_ORDER_UNIT = nr_seq_cpoe_order_unit_p 
or 		nr_seq_cpoe_rp = nr_seq_cpoe_rp_p;

begin
	ds_lista_w := '';

	open c01;
	loop
	fetch c01 into	nr_sequencia_w,
					ie_tipo_item_w,	
					nr_atendimento_w,
					ds_liberacao_w;
	exit when c01%notfound;
	begin
		if (ds_lista_w is not null) then
			ds_lista_w := ds_lista_w || ',';
		end if;
		ds_lista_w := ds_lista_w || nr_sequencia_w || ';' || ie_tipo_item_w;
	end;
	end loop;
	close c01;

	if (ds_lista_w is not null) then
		ds_lista_w := '[' || ds_lista_w || ']';

		cpoe_ajustar_itens_susp(ds_lista_w,
								sysdate,
								nm_usuario_p,
								ds_lista_itens_ret_w,
								'I',
								ds_lista_sol_consistir_w,
								ds_lista_itens_ex_w);
	end if;

	if (nr_seq_cpoe_order_unit_p is not null) then
		-- order unit
		update	cpoe_order_unit
		set 	dt_suspensao = sysdate,
				nm_usuario_susp = nm_usuario_p
		where	nr_sequencia = nr_seq_cpoe_order_unit_p;
	else
		-- rp
		update	cpoe_rp
		set 	dt_suspensao = sysdate,
				nm_usuario_susp = nm_usuario_p
		where	nr_sequencia = nr_seq_cpoe_rp_p;
	end if;

	commit;
end;
/
