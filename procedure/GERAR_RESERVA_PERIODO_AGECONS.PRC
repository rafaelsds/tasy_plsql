create or replace
procedure Gerar_reserva_periodo_agecons(cd_agenda_p		number,
					ds_dias_p		varchar2,
					hr_reserva_inicial_p	date,
					hr_reserva_final_p	date,
					dt_inicial_p		date,
					dt_final_p		date,
					cd_estabelecimento_p	number,
					nr_seq_motivo_reserva_p	number,
					nm_usuario_p		Varchar2) is 

dt_atual_w		date;
dt_reserva_inicial_w	date;	
dt_reserva_final_w	date;	
dt_dia_semana_w		number(1,0);
ie_gerar_dia_w		varchar2(1);
ds_dias_w		varchar2(255);
qt_agendamento_w	number(10,0);
nr_seq_agecons_w	number(10,0);
cd_tipo_agenda_w	number(10,0);
ie_feriado_w		varchar2(1);
ie_gerar_sobra_w	varchar2(1);
cd_estabelecimento_w	number(4,0);
ds_retorno_w		varchar2(255);	


Cursor C01 is
	select	nr_sequencia
	from	agenda_consulta
	where	dt_agenda between dt_reserva_inicial_w and dt_reserva_final_w
	and	cd_agenda = cd_agenda_p
	and	ie_status_agenda = 'L';
					
begin

dt_atual_w	:= dt_inicial_p;

if	(ds_dias_p is not null) then
	select	substr(ds_dias_p,1,length(ds_dias_p) -2)
	into	ds_dias_w
	from	dual;
end if;	

while (trunc(dt_atual_w) <= trunc(dt_final_p)) loop 
	begin
		
	dt_dia_semana_w	:= obter_cod_dia_semana(dt_atual_w);		
	
	ie_gerar_dia_w	:= 'S';
	
	if	(ds_dias_w is not null) then
		ie_gerar_dia_w	:=  obter_se_contido(dt_dia_semana_w,ds_dias_w);
	end if;
	
	if	(ie_gerar_dia_w = 'S') then
		
		dt_reserva_inicial_w := pkg_date_utils.get_DateTime(dt_atual_w, hr_reserva_inicial_p);
		
		dt_reserva_final_w := pkg_date_utils.get_DateTime(dt_atual_w, hr_reserva_final_p);
		
		select	count(*)
		into	qt_agendamento_w
		from	agenda_consulta
		where	cd_agenda = cd_agenda_p
		and	dt_agenda between dt_atual_w and trunc(dt_atual_w) + 86399/86400;

		if	(qt_agendamento_w = 0) then
		
			select	max(cd_tipo_agenda),
				max(ie_feriado),
				max(ie_gerar_sobra_horario),
				max(cd_estabelecimento)
			into	cd_tipo_agenda_w,
				ie_feriado_w,
				ie_gerar_sobra_w,
				cd_estabelecimento_w
			from	agenda
			where	cd_agenda = cd_agenda_p;
		
			horario_livre_consulta(cd_estabelecimento_w, 
						cd_agenda_p, 
						ie_feriado_w, 
						dt_atual_w, 
						nm_usuario_p, 
						'S', 
						ie_gerar_sobra_w, 
						'N', 
						null,
						ds_retorno_w);
		end if;
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_agecons_w;
		exit when C01%notfound;
			begin
			alterar_status_agecons(cd_agenda_p,
						nr_seq_agecons_w,
						'R',
						null,
						null,
						'N',
						nm_usuario_p,
						null);
						
			if	(nr_seq_motivo_reserva_p is not null) then
				update	agenda_consulta
				set	nr_seq_motivo_reserva 	= nr_seq_motivo_reserva_p
				where	nr_sequencia		= nr_seq_agecons_w;			
			end if;
			end;
		end loop;
		close C01;
	
	end if;
	
	dt_atual_w := dt_atual_w + 1;
		
	end;
end loop;

commit;

end Gerar_reserva_periodo_agecons;
/
