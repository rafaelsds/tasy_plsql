Create or Replace
Procedure Atualiza_Conta_NF_Saida (
			nr_sequencia_p	Number,
			ie_sobrepor_p	Varchar2) Is

cd_estabelecimento_w	Number(05,0);
cd_material_w		Number(06,0);
cd_centro_custo_w		Number(08,0);
cd_centro_custo_ww	Number(08,0);
cd_conta_contabil_w	Varchar2(20);
cd_local_estoque_w	Number(05,0);
nr_sequencia_w		Number(10,0);
nr_item_nf_w		Number(05,0);
ie_tipo_conta_w		Number(02,0);
cd_procedimento_w	Number(15,0);
ie_origem_proced_w	Number(10,0);
dt_entrada_saida_w	Date;
nr_seq_proc_interno_w	nota_fiscal_item.nr_seq_proc_interno%type;

CURSOR C01 IS
select	a.cd_estabelecimento,
	b.nr_sequencia,
	b.nr_item_nf,
	b.cd_conta_contabil,
	b.cd_centro_custo,
	b.cd_local_estoque,
	b.cd_material,
	b.cd_procedimento,
	b.ie_origem_proced,
	a.dt_entrada_saida,
	b.nr_seq_proc_interno
from 	operacao_nota c,
	nota_fiscal_item b,
	nota_fiscal a
where	a.nr_sequencia	= nr_sequencia_p
and	a.nr_sequencia	= b.nr_sequencia
and	a.cd_operacao_nf	= c.cd_operacao_nf
and	c.ie_operacao_fiscal	= 'S'
and	((b.cd_conta_contabil is null) or (ie_sobrepor_p = 'S'));

BEGIN

OPEN C01;
LOOP
FETCH C01 into	
	cd_estabelecimento_w,
	nr_sequencia_w,
	nr_item_nf_w,
	cd_conta_contabil_w,
	cd_centro_custo_w,
	cd_local_estoque_w,
	cd_material_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	dt_entrada_saida_w,
	nr_seq_proc_interno_w;
EXIT WHEN C01%NOTFOUND;
	begin
	ie_tipo_conta_w		:= 1; ---- Receita
	if	(cd_procedimento_w is not null) then
		define_conta_procedimento(
			cd_estabelecimento_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			ie_tipo_conta_w, 
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			dt_entrada_saida_w,
			cd_conta_contabil_w,
			cd_centro_custo_w,
			null,
			'N',
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			nr_seq_proc_interno_w);			
	else
		define_conta_material(
			cd_estabelecimento_w,
			cd_material_w, 
			ie_tipo_conta_w,
			null, 
			0, null, 
			0, 
			0,0,0,
			cd_local_estoque_w, 
			Null,
			dt_entrada_saida_w,
			cd_conta_contabil_w, 
			cd_centro_custo_w,
			'');
	end if;

	if	(cd_conta_contabil_w is not null) then
		select	cd_centro_custo
		into	cd_centro_custo_ww
		from	conta_contabil
		where	cd_conta_contabil	= cd_conta_contabil_w;
		cd_centro_custo_w		:= nvl(cd_centro_custo_w, cd_centro_custo_ww);
		update	nota_fiscal_item
		set 	cd_conta_contabil	= cd_conta_contabil_w,
			cd_centro_custo	= cd_centro_custo_w,
			dt_atualizacao	= sysdate
		where	nr_sequencia		= nr_sequencia_w
		and	nr_item_nf		= nr_item_nf;
	end if;
	end;
END LOOP;
close c01;
commit;
END;
/
