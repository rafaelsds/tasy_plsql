create or replace
procedure swisslog_movimentacao_pac(
				nr_atendimento_p	number,
				cd_setor_new_p		number,
				cd_setor_old_p		number,
				nr_opcao_p		number,
				nm_usuario_p		varchar2) is

/*
nr_opcao_p
1 - Transfer�ncia
2 - Alta
3 - Interna��o
*/

ie_setor_new_swisslog_w		varchar2(1) := 'N';
ie_setor_old_swisslog_w		varchar2(1) := 'N';
ie_prescr_vigente_w		varchar2(1) := 'N';
ds_param_integ_hl7_w		varchar2(4000) := '';
qt_registro_w			number(5);
dt_entrada_unidade_w		date;
cd_setor_w			setor_atendimento.cd_setor_atendimento%type;
nr_seq_empresa_w		empresa_integracao.nr_sequencia%type;
nr_prescricao_w			prescr_medica.nr_prescricao%type;
cd_setor_function		setor_atendimento.cd_setor_atendimento%type;

cursor c01 is
	select	nr_prescricao
	from	prescr_medica
	where	dt_validade_prescr > sysdate
	and	cd_setor_atendimento = cd_setor_old_p
	and	nr_atendimento = nr_atendimento_p;

cursor c02 is
	select	a.nr_prescricao
	from	prescr_medica a,
		prescr_mat_hor b,
		(select	nvl(nr_seq_estrut_int,0) nr_seq_estrut_int,
			cd_estabelecimento
		from	parametros_farmacia) c
	where	b.nr_seq_lote is not null
	and	a.dt_validade_prescr > sysdate
	and	consistir_se_mat_contr_estrut(c.nr_seq_estrut_int,b.cd_material) = 'S'
	and	obter_status_processo(b.nr_seq_processo) in ('G','X')
	and exists( select 1 from ap_lote x where x.nr_sequencia = b.nr_seq_lote and x.dt_atend_farmacia is null)
	and	b.qt_dispensar_hor > 0
	and	a.cd_setor_atendimento = cd_setor_old_p
	and	a.nr_atendimento = nr_atendimento_p
	and	a.cd_estabelecimento = c.cd_estabelecimento
	and	a.nr_prescricao = b.nr_prescricao
	group by a.nr_prescricao;

cursor c03 is
	select	a.nr_prescricao
	from	prescr_medica a,
		prescr_mat_hor b,
		(select	nvl(nr_seq_estrut_int,0) nr_seq_estrut_int,
			cd_estabelecimento
		from	parametros_farmacia) c
	where	b.nr_seq_lote is not null
	and	a.dt_validade_prescr > sysdate
	and	consistir_se_mat_contr_estrut(c.nr_seq_estrut_int,b.cd_material) = 'S'
	and	obter_status_processo(b.nr_seq_processo) in ('G','X')
	and exists( select 1 from ap_lote x where x.nr_sequencia = b.nr_seq_lote and x.dt_atend_farmacia is null)
	and	b.qt_dispensar_hor > 0
	and	a.cd_setor_atendimento = cd_setor_new_p
	and	a.nr_atendimento = nr_atendimento_p
	and	a.cd_estabelecimento = c.cd_estabelecimento
	and	a.nr_prescricao = b.nr_prescricao
	group by a.nr_prescricao;

begin
begin
select	nr_sequencia
into	nr_seq_empresa_w
from	empresa_integracao
where	upper(nm_empresa) = 'SWISSLOG'
and	rownum = 1;

select	decode(count(*),0,'N','S')
into	ie_setor_new_swisslog_w
from	far_setores_integracao
where	nr_seq_empresa_int = nr_seq_empresa_w
and	cd_setor_atendimento = cd_setor_new_p
and	rownum = 1;

select	decode(count(*),0,'N','S')
into	ie_setor_old_swisslog_w
from	far_setores_integracao
where	nr_seq_empresa_int = nr_seq_empresa_w
and	cd_setor_atendimento = cd_setor_old_p
and	rownum = 1;
exception
when others then
	nr_seq_empresa_w := 0;
	ie_setor_new_swisslog_w := 'N';
	ie_setor_old_swisslog_w := 'N';
end;

/*insert into log_tasy values (sysdate, nm_usuario_p, -55, '1 SMP - At='||nr_atendimento_p||' | Setor new='||cd_setor_new_p||
							 ' | Setor old='||cd_setor_old_p||' | Op='||nr_opcao_p);*/

-- Op��o 4 > Gera prescri��o no novo setor do paciente.
if	(nr_opcao_p = 4) then --Movimenta��o > ATUALIZAR_SETOR_PRESCRICAO
	begin
	select	count(*)
	into	qt_registro_w
	from	w_swisslog_mensagem
	where	dt_atualizacao < (sysdate -1);
	
	if	(qt_registro_w > 0) then
		delete	w_swisslog_mensagem
		where	dt_atualizacao < (sysdate -1);
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	end if;
	
	/*insert into log_tasy values (sysdate, nm_usuario_p, -55, '2 SMP - At='||nr_atendimento_p||' | ie_setor_old_swisslog_w='||ie_setor_old_swisslog_w||
							 ' | ie_setor_new_swisslog_w='||ie_setor_new_swisslog_w||' | Op='||nr_opcao_p);*/
	
	-- Swisslog X Swisslog
	if	(ie_setor_old_swisslog_w = 'S') and (ie_setor_new_swisslog_w = 'S') then
		begin
		select	'S'
		into	ie_prescr_vigente_w
		from	prescr_medica a,
			prescr_mat_hor b,
			(select	nvl(nr_seq_estrut_int,0) nr_seq_estrut_int,
				cd_estabelecimento
			from	parametros_farmacia) c
		where	b.nr_seq_lote is not null
		and	a.dt_validade_prescr > sysdate
		and	consistir_se_mat_contr_estrut(c.nr_seq_estrut_int,b.cd_material) = 'S'
		and	obter_status_processo(b.nr_seq_processo) in ('G','X')
		and exists( select 1 from ap_lote x where x.nr_sequencia = b.nr_seq_lote and x.dt_atend_farmacia is null)
		and	b.qt_dispensar_hor > 0
		and	a.cd_setor_atendimento = cd_setor_new_p
		and	a.nr_atendimento = nr_atendimento_p
		and	a.cd_estabelecimento = c.cd_estabelecimento
		and	a.nr_prescricao = b.nr_prescricao
		and	rownum = 1;
		exception
		when others then
			ie_prescr_vigente_w := 'N';
		end;
		
		if	(ie_prescr_vigente_w = 'S') then
			open c03;
			loop
			fetch c03 into	
				nr_prescricao_w;
			exit when c03%notfound;
				begin
				/*insert into log_tasy values (sysdate, nm_usuario_p, -55, '3 SMP - At='||nr_atendimento_p||
							' | nr_prescricao_w='||nr_prescricao_w||' | Op='||nr_opcao_p);*/
							
				insert into w_swisslog_mensagem (nr_evento, nr_prescricao, nm_usuario) values (434,nr_prescricao_w,'Swisslog');
				if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
				
				ds_param_integ_hl7_w := 'nr_prescricao=' || nr_prescricao_w || obter_separador_bv || 
							'nr_atendimento=' || nr_atendimento_p || obter_separador_bv ||
							'ie_aprazado=' || 'GPMH' || obter_separador_bv ||
							'ie_movimento=' || 'MVP' ||  obter_separador_bv ||
							'ie_origem=' || 'SMP' || obter_separador_bv || 
							'nr_seq_horario_p=' || null || obter_separador_bv;
				
				-- Gera��o da prescri��o com os itens pendentes
				swisslog_gerar_integracao(434, ds_param_integ_hl7_w);
				end;
			end loop;
			close c03;
		else
			-- Envio as informa��es do paciente
			ds_param_integ_hl7_w := 'nr_atendimento=' || nr_atendimento_p || obter_separador_bv ||
									'ie_movimento=' || 'MVP' ||  obter_separador_bv;
			swisslog_gerar_integracao(432, ds_param_integ_hl7_w);
		end if;
	end if;
	-- Swisslog X Swisslog
	
	-- N�o Swisslog X Swisslog
	if	(ie_setor_old_swisslog_w = 'N') and (ie_setor_new_swisslog_w = 'S') then
		begin
		select	'S'
		into	ie_prescr_vigente_w
		from	prescr_medica a,
			prescr_mat_hor b,
			(select	nvl(nr_seq_estrut_int,0) nr_seq_estrut_int,
				cd_estabelecimento
			from	parametros_farmacia) c
		where	b.nr_seq_lote is not null
		and	a.dt_validade_prescr > sysdate
		and	consistir_se_mat_contr_estrut(c.nr_seq_estrut_int,b.cd_material) = 'S'
		and	obter_status_processo(b.nr_seq_processo) in ('G','X')
		and exists( select 1 from ap_lote x where x.nr_sequencia = b.nr_seq_lote and x.dt_atend_farmacia is null)
		and	b.qt_dispensar_hor > 0
		and	a.cd_setor_atendimento = cd_setor_new_p
		and	a.nr_atendimento = nr_atendimento_p
		and	a.cd_estabelecimento = c.cd_estabelecimento
		and	a.nr_prescricao = b.nr_prescricao
		and	rownum = 1;
		exception
		when others then
			ie_prescr_vigente_w := 'N';
		end;
		
		if	(ie_prescr_vigente_w = 'S') then
			open c03;
			loop
			fetch c03 into
				nr_prescricao_w;
			exit when c03%notfound;
				begin
				select	cd_setor_atendimento
				into	cd_setor_function
				from	prescr_medica
				where	nr_prescricao = nr_prescricao_w;
				
				/*insert into log_tasy values (sysdate, nm_usuario_p, -55, '4 SMP - At='||nr_atendimento_p||' | Op='||nr_opcao_p||
											 ' | Setor pr='||cd_setor_function);*/
				
				insert into w_swisslog_mensagem (nr_evento, nr_prescricao, nm_usuario) values (434,nr_prescricao_w,'Swisslog');
				if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
				
				ds_param_integ_hl7_w := 'nr_prescricao=' || nr_prescricao_w || obter_separador_bv || 
							'nr_atendimento=' || nr_atendimento_p || obter_separador_bv ||
							'ie_aprazado=' || 'GPMH' || obter_separador_bv ||
							'ie_movimento=' || 'MVP' ||  obter_separador_bv ||
							'ie_origem=' || 'SMP' || obter_separador_bv;
				
				-- Gera��o da prescri��o com os itens pendentes
				swisslog_gerar_integracao(434, ds_param_integ_hl7_w);
				end;
			end loop;
			close c03;
		end if;
	end if;
	-- N�o Swisslog X Swisslog
	end;
-- Op��o 1 > Gera cancelamento das prescri��es no antigo setor do paciente.
elsif	(nr_opcao_p = 1) then --Movimenta��o > GERAR_TRANSFERENCIA_PACIENTE
	begin
	/*insert into log_tasy values (sysdate, nm_usuario_p, -55, '2 SMP - At='||nr_atendimento_p||' | ie_setor_old_swisslog_w='||ie_setor_old_swisslog_w||
							 ' | ie_setor_new_swisslog_w='||ie_setor_new_swisslog_w||' | Op='||nr_opcao_p);*/
	
	-- Swisslog X N�o Swisslog
	if	(ie_setor_old_swisslog_w = 'S') and (ie_setor_new_swisslog_w = 'N') then
		ds_param_integ_hl7_w := 'nr_atendimento=' || nr_atendimento_p || obter_separador_bv ||
								'ie_movimento=' || 'MVP' ||  obter_separador_bv;
							 
		-- Envio das informa��es do paciente para gerar a alta
		swisslog_gerar_integracao(435, ds_param_integ_hl7_w);
	end if;
	-- Swisslog X N�o Swisslog
	
	-- Swisslog X Swisslog
	if	(ie_setor_old_swisslog_w = 'S') and (ie_setor_new_swisslog_w = 'S') then
		begin
		select	'S'
		into	ie_prescr_vigente_w
		from	prescr_medica a,
			prescr_mat_hor b,
			(select	nvl(nr_seq_estrut_int,0) nr_seq_estrut_int,
				cd_estabelecimento
			from	parametros_farmacia) c
		where	b.nr_seq_lote is not null
		and	a.dt_validade_prescr > sysdate
		and	consistir_se_mat_contr_estrut(c.nr_seq_estrut_int,b.cd_material) = 'S'
		and	obter_status_processo(b.nr_seq_processo) in ('G','X')
		and exists( select 1 from ap_lote x where x.nr_sequencia = b.nr_seq_lote and x.dt_atend_farmacia is null)
		and	b.qt_dispensar_hor > 0
		and	a.cd_setor_atendimento = cd_setor_old_p
		and	a.nr_atendimento = nr_atendimento_p
		and	a.cd_estabelecimento = c.cd_estabelecimento
		and	a.nr_prescricao = b.nr_prescricao
		and	rownum = 1;
		exception
		when others then
			ie_prescr_vigente_w := 'N';
		end;
		
		if	(ie_prescr_vigente_w = 'S') then
			open c02;
			loop
			fetch c02 into	
				nr_prescricao_w;
			exit when c02%notfound;
				begin
				ds_param_integ_hl7_w := 'nr_prescricao=' || nr_prescricao_w || obter_separador_bv ||
										'ie_movimento=' || 'MVP' ||  obter_separador_bv;
				
				/*insert into log_tasy values (sysdate, nm_usuario_p, -55, '3 SMP - At='||nr_atendimento_p||
							' | nr_prescricao_w='||nr_prescricao_w||' | Op='||nr_opcao_p);*/
				
				-- Cancelamento da prescri��o
				swisslog_gerar_integracao(431, ds_param_integ_hl7_w);
				end;
			end loop;
			close c02;
		end if;
	end if;
	-- Swisslog X Swisslog
	end;
-- Op��o 3 > Gera admiss�o do paciente na integra��o.
elsif	(nr_opcao_p = 3) and (ie_setor_old_swisslog_w = 'N') and (ie_setor_new_swisslog_w = 'S') then --Interna��o > ATEND_PACIENTE_UNIDADE_INSERT
	begin
	begin
	cd_setor_w := obter_setor_atendimento(nr_atendimento_p);
	
	select	nr_sequencia
	into	nr_seq_empresa_w
	from	empresa_integracao
	where	upper(nm_empresa) = 'SWISSLOG'
	and	rownum = 1;
	
	select	decode(count(*),0,'N','S')
	into	ie_setor_old_swisslog_w
	from	far_setores_integracao
	where	nr_seq_empresa_int = nr_seq_empresa_w
	and	cd_setor_atendimento = cd_setor_w
	and	rownum = 1;
	exception
	when others then
		ie_setor_old_swisslog_w := 'N';
	end;
	
	if	(ie_setor_old_swisslog_w = 'N') then
		begin
		select	'S'
		into	ie_prescr_vigente_w
		from	prescr_medica a,
			prescr_mat_hor b,
			(select	nvl(nr_seq_estrut_int,0) nr_seq_estrut_int,
				cd_estabelecimento
			from	parametros_farmacia) c
		where	b.nr_seq_lote is not null
		and	a.dt_validade_prescr > sysdate
		and	consistir_se_mat_contr_estrut(c.nr_seq_estrut_int,b.cd_material) = 'S'
		and	obter_status_processo(b.nr_seq_processo) in ('G','X')
		and exists( select 1 from ap_lote x where x.nr_sequencia = b.nr_seq_lote and x.dt_atend_farmacia is null)
		and	b.qt_dispensar_hor > 0
		and	a.cd_setor_atendimento = cd_setor_w
		and	a.nr_atendimento = nr_atendimento_p
		and	a.cd_estabelecimento = c.cd_estabelecimento
		and	a.nr_prescricao = b.nr_prescricao
		and	rownum = 1;
		exception
		when others then
			ie_prescr_vigente_w := 'N';
		end;
		
		if	(ie_prescr_vigente_w = 'N') then
			ds_param_integ_hl7_w := 'nr_atendimento=' || nr_atendimento_p || obter_separador_bv || 
									'ie_movimento=' || 'MVP' ||  obter_separador_bv;
			
			-- Envio das informa��es do paciente (Admiss�o)
			swisslog_gerar_integracao(430, ds_param_integ_hl7_w);
		end if;
	end if;
	end;
-- Op��o 2 > Gera alta do paciente na integra��o.
elsif	(nr_opcao_p = 2) and (ie_setor_old_swisslog_w = 'S') then --Alta > GERAR_ESTORNAR_ALTA
	begin
	ds_param_integ_hl7_w := 'nr_atendimento=' || nr_atendimento_p || obter_separador_bv ||
							'ie_movimento=' || 'MVP' ||  obter_separador_bv;
	
	-- Envio das informa��es do paciente para gerar a alta
	swisslog_gerar_integracao(435, ds_param_integ_hl7_w);
	end;
end if;

end swisslog_movimentacao_pac;
/
