create or replace 
procedure inserir_medical_guidance_order (	nr_atendimento_p	number,
						nr_seq_proc_interno_p	number,
						cd_estabelecimento_p 	number,
						nm_usuario_p 		varchar2) is

nr_seq_proc_interno_w   med_guidance_prescr.nr_seq_proc_interno%type;
ie_origem_proced_w      med_guidance_prescr.ie_origem_proced%type;
cd_procedimento_w       med_guidance_prescr.cd_procedimento%type;
tx_procedimento_w       med_guidance_prescr.tx_procedimento%type;
cd_pessoa_fisica_w      atendimento_paciente.cd_pessoa_fisica%type;
nr_seq_w                med_guidance_prescr.nr_seq_med_guid%type;
cd_evolucao_w number(10);
ie_gerar_clinical_notes_w varchar2(1) := 'N';
nr_sequencia_w number;

begin
obter_param_usuario(281, 1630, Obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_clinical_notes_w);

select	nr_seq_proc_interno,
        ie_origem_proced,
        cd_procedimento,
	tx_procedimento
into	nr_seq_proc_interno_w,
        ie_origem_proced_w,
        cd_procedimento_w,
	tx_procedimento_w
from	med_guidance_prescr
where	nr_sequencia = nr_seq_proc_interno_p;

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;  

select	medical_guidance_order_seq.nextval
into	nr_seq_w
from	dual;

insert into medical_guidance_order (
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
                        dt_atualizacao_nrec,
                        nm_usuario_nrec,
			nr_atendimento,
			nr_seq_proc_interno,
                        ie_origem_proced,
                        cd_procedimento,
                        qt_score,
                        ie_situacao,
			dt_liberacao,
			cd_pessoa_fisica,
			dt_guidance,            
			dt_calculation)
	values	(       nr_seq_w,
			cd_estabelecimento_p,
			sysdate,
			nm_usuario_p,
                        sysdate,
                        nm_usuario_p,
			nr_atendimento_p,
			nr_seq_proc_interno_w,
                        ie_origem_proced_w,
                        cd_procedimento_w,
                        tx_procedimento_w,
                        'A',
			sysdate,
			cd_pessoa_fisica_w,
			sysdate,
			sysdate);

update	med_guidance_prescr
set	si_approval = 'S',
	nr_seq_med_guid = nr_seq_w
where	nr_sequencia = nr_seq_proc_interno_p;

select max(nr_sequencia)
             into nr_sequencia_w
FROM     medical_guidance_order
WHERE     nr_atendimento = nr_atendimento_p   ;

if( ie_gerar_clinical_notes_w = 'S'  )then
  begin
 clinical_notes_pck.gerar_soap(nr_atendimento_p, nr_sequencia_w, 'MED_GUIDANCE', NULL, 'P',1, cd_evolucao_w);
            UPDATE MEDICAL_GUIDANCE_ORDER
                SET
                    cd_evolucao = cd_evolucao_w
                WHERE
                    NR_SEQUENCIA = nr_sequencia_w;
  end;
  end if;
commit;

end inserir_medical_guidance_order;
/
