create or replace
procedure pls_desfazer_lote_import
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2) is
				
nr_seq_lanc_prog_imp_w		pls_lanc_prog_importacao.nr_sequencia%type;
				
Cursor C01 is
	select	nr_sequencia
	from	pls_lanc_prog_importacao
	where	nr_seq_lote = nr_seq_lote_p;

begin

open C01;
loop
fetch C01 into	
	nr_seq_lanc_prog_imp_w;
exit when C01%notfound;
	begin
	
	delete	pls_lanc_prog_imp_inco
	where	nr_seq_lanc_prog_imp	= nr_seq_lanc_prog_imp_w;
	
	delete	pls_lanc_prog_importacao
	where	nr_sequencia		= nr_seq_lanc_prog_imp_w;
	
	end;
end loop;
close C01;

update	pls_lanc_prog_import_lote
set	nm_arquivo		= null,
	nr_seq_tipo_imp_lanc	= null
where	nr_sequencia		= nr_seq_lote_p;

commit;

end pls_desfazer_lote_import;
/
