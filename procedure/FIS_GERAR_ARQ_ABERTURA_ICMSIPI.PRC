create or replace
procedure fis_gerar_arq_abertura_icmsipi(	
				nr_seq_controle_p	number,
				cd_registro_p		varchar2,
				nr_linha_p			in out number	) is 

-- VARIABLES
ds_linha_w			varchar2(8000);
ie_gerou_w			varchar2(1);
ie_ind_mov_w		varchar2(1);
ds_sep_w			varchar2(1) := '|';

-- USUARIO
nm_usuario_w		usuario.nm_usuario%type;
				
-- FIS_EFD_ICMSIPI_ARQUIVO			
nr_linha_w			fis_efd_icmsipi_arquivo.nr_linha%type := 0;
ds_arquivo_w		fis_efd_icmsipi_arquivo.ds_arquivo%type;
ds_arquivo_compl_w	fis_efd_icmsipi_arquivo.ds_arquivo_compl%type;

begin
nm_usuario_w := Obter_Usuario_Ativo;
nr_linha_w := nr_linha_p;

if (substr(cd_registro_p,1,1) = '0') then
	select	ie_mov_0
	into	ie_gerou_w
	from 	fis_efd_icmsipi_controle
	where 	nr_sequencia = nr_seq_controle_p;
elsif (substr(cd_registro_p,1,1) = 'B') then
	select	ie_mov_b
	into	ie_gerou_w
	from 	fis_efd_icmsipi_controle
	where 	nr_sequencia = nr_seq_controle_p;
elsif (substr(cd_registro_p,1,1) = 'C') then
	select	ie_mov_c
	into	ie_gerou_w
	from 	fis_efd_icmsipi_controle
	where 	nr_sequencia = nr_seq_controle_p;
elsif (substr(cd_registro_p,1,1) = 'D') then
	select	ie_mov_d
	into	ie_gerou_w
	from 	fis_efd_icmsipi_controle
	where 	nr_sequencia = nr_seq_controle_p;
elsif (substr(cd_registro_p,1,1) = 'E') then
	select	ie_mov_e
	into	ie_gerou_w
	from 	fis_efd_icmsipi_controle
	where 	nr_sequencia = nr_seq_controle_p;
elsif (substr(cd_registro_p,1,1) = 'G') then
	select	ie_mov_g
	into	ie_gerou_w
	from 	fis_efd_icmsipi_controle
	where 	nr_sequencia = nr_seq_controle_p;
elsif (substr(cd_registro_p,1,1) = 'H') then
	select	ie_mov_h
	into	ie_gerou_w
	from 	fis_efd_icmsipi_controle
	where 	nr_sequencia = nr_seq_controle_p;
elsif (substr(cd_registro_p,1,1) = 'K') then
	select	ie_mov_k
	into	ie_gerou_w
	from 	fis_efd_icmsipi_controle
	where 	nr_sequencia = nr_seq_controle_p;
elsif (substr(cd_registro_p,1,1) = '1') then
	ie_gerou_w := 'S';
elsif (substr(cd_registro_p,1,1) = '9') then
	ie_gerou_w := 'S';
end if;

if (ie_gerou_w = 'S') then
	ie_ind_mov_w := '0';
else
	ie_ind_mov_w := '1';
end if;

nr_linha_w := nr_linha_w + 1;
ds_linha_w := substr(ds_sep_w || cd_registro_p || 
				ds_sep_w || ie_ind_mov_w ||ds_sep_w,1,8000);	
ds_arquivo_w		:= substr(ds_linha_w,1,4000);
ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);				
				
insert into fis_efd_icmsipi_arquivo(
		nr_sequencia,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,
		nr_seq_controle,
		nr_linha,
		ds_arquivo,
		ds_arquivo_compl,
		cd_registro)
	values(
		fis_efd_icmsipi_arquivo_seq.nextval,
		sysdate,
		nm_usuario_w,
		sysdate,
		nm_usuario_w,
		nr_seq_controle_p,
		nr_linha_w,
		ds_arquivo_w,
		ds_arquivo_compl_w,
		cd_registro_p);		
		
nr_linha_p	:= nr_linha_w;

end fis_gerar_arq_abertura_icmsipi;
/
