create or replace
procedure cus_gerar_centros_destino(	cd_estabelecimento_p	number,
						cd_tabela_custo_p	number,
						nr_seq_tabela_p		number,
						nm_usuario_p		varchar2) is

cd_sequencia_criterio_w			number(10);

cursor c01 is
select	cd_sequencia_criterio
from	criterio_distr_orc
where	nr_seq_tabela		= nr_seq_tabela_p
and	ie_criterio_centro	= 'C'
order by 1;
BEGIN

open c01;
loop
fetch c01 into
	cd_sequencia_criterio_w;
exit when c01%notfound;
	cus_gerar_criterio_consulta(cd_estabelecimento_p, cd_tabela_custo_p, cd_sequencia_criterio_w, nr_seq_tabela_p, nm_usuario_p);
end loop;
close c01;
END cus_gerar_centros_destino;
/