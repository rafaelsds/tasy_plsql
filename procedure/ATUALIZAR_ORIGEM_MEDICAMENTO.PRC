create or replace
procedure Atualizar_origem_medicamento(	nr_seq_origem_p		number,
					nr_prescricao_p		number,
					nr_seq_material_p	number,
					ie_atualiza_composto_p	varchar2,
					ie_atualiza_diluicao_p	varchar2,
					ie_atualiza_kit_p	varchar2,
					nm_usuario_p		Varchar2,
					ie_medic_paciente_p 	varchar2 default 'S') is 

cd_motivo_baixa_w	number(10,0);
nr_agrupamento_w	number(7,1);
					
begin

if	(nr_seq_origem_p > 0) then
	begin
	select	max(cd_motivo_baixa)
	into	cd_motivo_baixa_w
	from	origem_medic_paciente
	where	nr_sequencia	= nr_seq_origem_p;

	update	prescr_material
	set	ie_medicacao_paciente	= ie_medic_paciente_p,
		nr_seq_origem_medic	= nr_seq_origem_p,
		cd_motivo_baixa		= cd_motivo_baixa_w
	where  	nr_prescricao         	= nr_prescricao_p
	and    	nr_sequencia          	= nr_seq_material_p;

	select	max(nr_agrupamento)
	into	nr_agrupamento_w
	from	prescr_material
	where	nr_prescricao         	= nr_prescricao_p
	and	nr_sequencia          	= nr_seq_material_p;

	if	(ie_atualiza_composto_p	= 'S') then
		update	prescr_material
		set	ie_medicacao_paciente	= ie_medic_paciente_p,
			nr_seq_origem_medic	= nr_seq_origem_p,
			cd_motivo_baixa		= cd_motivo_baixa_w
		where	nr_prescricao         	= nr_prescricao_p
		and    	nr_agrupamento		= nr_agrupamento_w;
	end if;

	if	(ie_atualiza_diluicao_p	= 'S') then
		update	prescr_material
		set	ie_medicacao_paciente	= ie_medic_paciente_p,
			nr_seq_origem_medic	= nr_seq_origem_p,
			cd_motivo_baixa		= cd_motivo_baixa_w
		where	nr_prescricao         	= nr_prescricao_p
		and    	nr_sequencia_diluicao	= nr_seq_material_p
		and	ie_agrupador		in(3,7,9);
	end if;

	if	(ie_atualiza_kit_p	= 'S') then
		update	prescr_material
		set	ie_medicacao_paciente	= ie_medic_paciente_p,
			nr_seq_origem_medic	= nr_seq_origem_p,
			cd_motivo_baixa		= cd_motivo_baixa_w
		where	nr_prescricao         	= nr_prescricao_p
		and    	nr_seq_kit		= nr_seq_material_p;
	end if;
	end;
else
	begin
	update	prescr_material
	set	ie_medicacao_paciente	= ie_medic_paciente_p,
		nr_seq_origem_medic	= null,
		cd_motivo_baixa		= 0,
		ie_regra_disp		= 'S'
	where  	nr_prescricao         	= nr_prescricao_p
	and    	nr_sequencia          	= nr_seq_material_p;

	select	max(nr_agrupamento)
	into	nr_agrupamento_w
	from	prescr_material
	where	nr_prescricao         	= nr_prescricao_p
	and	nr_sequencia          	= nr_seq_material_p;

	if	(ie_atualiza_composto_p	= 'S') then
		update	prescr_material
		set	ie_medicacao_paciente	= ie_medic_paciente_p,
			nr_seq_origem_medic	= null,
			cd_motivo_baixa		= 0,
			ie_regra_disp		= 'S'
		where	nr_prescricao         	= nr_prescricao_p
		and    	nr_agrupamento		= nr_agrupamento_w;
	end if;

	if	(ie_atualiza_diluicao_p	= 'S') then
		update	prescr_material
		set	ie_medicacao_paciente	= ie_medic_paciente_p,
			nr_seq_origem_medic	= null,
			cd_motivo_baixa		= 0,
			ie_regra_disp		= 'S'
		where	nr_prescricao         	= nr_prescricao_p
		and    	nr_sequencia_diluicao	= nr_seq_material_p
		and	ie_agrupador		in(3,7,9);
	end if;

	if	(ie_atualiza_kit_p	= 'S') then
		update	prescr_material
		set	ie_medicacao_paciente	= ie_medic_paciente_p,
			nr_seq_origem_medic	= null,
			cd_motivo_baixa		= 0,
			ie_regra_disp		= 'S'
		where	nr_prescricao         	= nr_prescricao_p
		and    	nr_seq_kit		= nr_seq_material_p;
	end if;
	end;
end if;
	
commit;

end Atualizar_origem_medicamento;
/
