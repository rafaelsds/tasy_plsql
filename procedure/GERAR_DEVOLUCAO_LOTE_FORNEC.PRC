create or replace
procedure gerar_devolucao_lote_fornec ( nr_atendimento_p number,
					nm_usuario_p		Varchar2) is 

dt_entrada_unidade_w		date;
cd_setor_atendimento_w		number(5);
nr_devolucao_w			number(10);
cd_pessoa_fisica_devol_w		varchar2(10);			
nr_prescricao_w			number(14);
cd_material_w			number(10);
qt_material_w			number(17,4);
nr_seq_lote_fornec_w		number(10); 
dt_atendimento_w			date;
cd_unidade_medida_w		varchar2(30);
nr_sequencia_prescricao_w		number(6);
nr_sequencia_w			number(10);
cd_local_estoque_w		number(4);
qt_mat_w				number(17,4);
ds_retorno_w			varchar2(4000);
nr_seq_mat_atend_pac_w		number(10);

--cursor 4
nr_seq_item_w			number(10);
qt_mat_baixa_w			number(13,4);
cd_local_estoque_baixa_w	number(4);
dt_recebimento_w		date;

--baixa devolu��o
ie_consiste_devolucao_w		varchar2(1);
cd_tipo_baixa_w			varchar2(5);
cd_estabelecimento_w		number(4);
ie_baixa_auto_w			varchar2(1);
			
Cursor C01 is
	select	distinct
		to_date(obter_data_internacao(nr_atendimento),'dd/mm/yyyy hh24:mi:ss') dt_entrada_unidade,
		substr(obter_unidade_atendimento(nr_atendimento,'P', 'CS'),1,60) cd_setor_atendimento,
		obter_pessoa_atendimento(nr_atendimento,'C') cd_pessoa_fisica_devol
	from   	w_devolucao_material
	where	nr_atendimento = nr_atendimento_p
	group by nr_atendimento;

cursor C02 is
	select	cd_material,
		nr_seq_lote_fornec,
		nr_seq_mat_atend_pac
	from	w_devolucao_material
	where	nr_atendimento = nr_atendimento_p
	group by cd_material,
		nr_seq_lote_fornec,
		nr_seq_mat_atend_pac;

cursor c03 is
	select	a.dt_atendimento,
		a.cd_unidade_medida,
		a.nr_sequencia_prescricao,
		a.nr_sequencia,
		a.cd_local_estoque,
		a.qt_material,
		a.nr_prescricao
	from	material_atend_paciente a
	where 	a.nr_atendimento = nr_atendimento_p
	and	a.cd_material = cd_material_w
	and	a.nr_seq_lote_fornec = nr_seq_lote_fornec_w
	and	a.nr_sequencia = nr_seq_mat_atend_pac_w;

Cursor C04 is
	select	nr_sequencia,
		qt_material,
		cd_local_estoque,
		dt_recebimento
	from	item_devolucao_material_pac
	where	nr_devolucao = nr_devolucao_w;

begin
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

obter_param_usuario(42,18,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_consiste_devolucao_w);
obter_param_usuario(42,22,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,cd_tipo_baixa_w);
obter_param_usuario(42,84,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_baixa_auto_w);

open C01;
loop
fetch C01 into	
	dt_entrada_unidade_w,
	cd_setor_atendimento_w,
	cd_pessoa_fisica_devol_w;
exit when C01%notfound;
	begin
	select	devolucao_material_pac_seq.nextval
	into	nr_devolucao_w
	from 	dual;
	
	insert 	into	devolucao_material_pac(
			nr_devolucao,
			nr_atendimento,
			dt_entrada_unidade,
			dt_devolucao,
			cd_pessoa_fisica_devol,
			cd_setor_atendimento, 
			nm_usuario_devol,
			dt_atualizacao,
			nm_usuario_baixa,
			dt_liberacao_baixa,
			dt_baixa_total,
			nm_usuario,
			dt_impressao,
			dt_entrega_fisica)
	values		(nr_devolucao_w,
			nr_atendimento_p,
			dt_entrada_unidade_w,
			sysdate,
			cd_pessoa_fisica_devol_w,
			cd_setor_atendimento_w,
			nm_usuario_p,
			sysdate,
			null,
			null,
			null,
			nm_usuario_p,
			null, 
			null);
		end;

	open C02;
	loop
	fetch C02 into	
		cd_material_w,
		nr_seq_lote_fornec_w,
		nr_seq_mat_atend_pac_w;
	exit when C02%notfound;
		begin
		
		open C03;
		loop
		fetch C03 into	
			dt_atendimento_w,
			cd_unidade_medida_w,
			nr_sequencia_prescricao_w,
			nr_sequencia_w,
			cd_local_estoque_w,
			qt_mat_w,
			nr_prescricao_w ;
		exit when C03%notfound;
			begin
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	item_devolucao_material_pac
			where	nr_devolucao = nr_devolucao_w;

			insert into ITEM_DEVOLUCAO_MATERIAL_PAC(
				nr_devolucao,
				nr_sequencia,
				cd_material,
				dt_atendimento,
				cd_local_estoque,
				cd_unidade_medida,
				cd_motivo_devolucao,
				dt_atualizacao,
				nm_usuario_devol,
				qt_material,
				nr_prescricao,
				nr_sequencia_prescricao,
				ie_tipo_baixa_estoque,
				dt_entrada_unidade,
				nm_usuario,
				nr_seq_lote_fornec,
				nr_seq_atendimento)
			values(	nr_devolucao_w,
				nr_sequencia_w,
				cd_material_w,
				dt_atendimento_w,
				cd_local_estoque_w,
				cd_unidade_medida_w,
				1,
				sysdate,
				nm_usuario_p,
				qt_mat_w,
				nr_prescricao_w,
				nr_sequencia_prescricao_w,
				0,
				dt_entrada_unidade_w,
				nm_usuario_p,
				nr_seq_lote_fornec_w,
				nr_seq_mat_atend_pac_w);
			end;
		end loop;
		close C03;
	
		end;
	end loop;
	close C02;	
		
end loop;
close C01;	

delete from w_devolucao_material;

-- Tratamento para gerar a baixa da devolu��o
if	(nvl(ie_baixa_auto_w,'N') = 'S') then
		
	if	(nvl(ie_consiste_devolucao_w,'N') = 'S') then
		consistir_devolucao_material(nr_devolucao_w,cd_estabelecimento_w,nm_usuario_p, ds_retorno_w);
	end if;

	if	(nvl(ds_retorno_w,'') <> '') then
		wheb_mensagem_pck.exibir_mensagem_abort(265993);
	else

		update	devolucao_material_pac
		set	dt_liberacao_baixa = sysdate,
			nm_usuario_devol = nm_usuario_p
		where	nr_devolucao = nr_devolucao_w;

		open C04;
		loop
		fetch C04 into	
			nr_seq_item_w,
			qt_mat_baixa_w,
			cd_local_estoque_baixa_w,
			dt_recebimento_w;
		exit when C04%notfound;
			begin

			if	(dt_recebimento_w is null) then
			
				baixa_devolucao_med(	nr_devolucao_w,
							nr_seq_item_w,
							nm_usuario_p,
							cd_tipo_baixa_w,
							qt_mat_baixa_w,
							cd_pessoa_fisica_devol_w,
							cd_local_estoque_baixa_w,
							'B',
							cd_estabelecimento_w);
			end if;

			end;
		end loop;
		close C04;

		update	devolucao_material_pac
		set	dt_baixa_total = sysdate,
			nm_usuario_baixa = nm_usuario_p
		where	nr_devolucao = nr_devolucao_w;

	end if;
end if;

end gerar_devolucao_lote_fornec;
/