create or replace
procedure GERAR_W_MOVTO_CARTAO_CR is

/*
create	table w_movto_cartao_cr
	(cd_estabelecimento	number(4),
	ds_comprovante		varchar2(100),
	dt_transacao		date,
	ie_lib_caixa		varchar2(1),
	ie_situacao		varchar2(1),
	ie_tipo_cartao		varchar2(1),
	nr_autorizacao		varchar2(40),
	nr_seq_bandeira		number(10),
	nr_seq_forma_pagto	number(10),
	vl_transacao		number(15,2));
*/

cd_estabelecimento_w	number(4);
ds_comprovante_w	varchar2(100);
dt_transacao_w		date;
ie_lib_caixa_W		varchar2(1);
ie_situacao_w		varchar2(1);
ie_tipo_cartao_w	varchar2(1);
nr_autorizacao_w	varchar2(40);
nr_seq_bandeira_w	number(10);
nr_seq_forma_pagto_w	number(10);
vl_transacao_w		number(15,2);

qt_cont_w		number(10);

cursor	c01 is
select	a.cd_estabelecimento,
	a.ds_comprovante,
	a.dt_transacao,
	a.ie_lib_caixa,
	a.ie_situacao,
	a.ie_tipo_cartao,
	a.nr_autorizacao,
	a.nr_seq_bandeira,
	a.nr_seq_forma_pagto,
	a.vl_transacao
from	w_movto_cartao_cr a;

cursor	c02 is
select	distinct
	a.cd_estabelecimento
from	w_movto_cartao_cr a;

cursor	c03 is
select	distinct
	a.nr_seq_bandeira
from	w_movto_cartao_cr a;

cursor	c04 is
select	distinct
	a.nr_seq_forma_pagto
from	w_movto_cartao_cr a;

begin

delete	from log_tasy
where	cd_log	= 99890;

open	c02;
loop
fetch	c02 into
	cd_estabelecimento_w;
exit	when c02%notfound;

	select	count(*)
	into	qt_cont_w
	from	estabelecimento a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;

	if	(qt_cont_w	= 0) then
		insert	into log_tasy
			(cd_log,
			ds_log,
			dt_atualizacao,
			nm_usuario)
		values	(99890,
			'O estabelecimento de c�digo ' || cd_estabelecimento_w || ' n�o foi localizado no Tasy!',
			sysdate,
			'Tasy');
	end if;

end	loop;
close	c02;

open	c03;
loop
fetch	c03 into
	nr_seq_bandeira_w;
exit	when c03%notfound;

	select	count(*)
	into	qt_cont_w
	from	bandeira_cartao_cr a
	where	a.nr_sequencia	= nr_seq_bandeira_w;

	if	(qt_cont_w	= 0) then
		insert	into log_tasy
			(cd_log,
			ds_log,
			dt_atualizacao,
			nm_usuario)
		values	(99890,
			'A bandeira de sequ�ncia ' || nr_seq_bandeira_w || ' n�o foi localizada no Tasy!',
			sysdate,
			'Tasy');
	end if;

end	loop;
close	c03;

open	c04;
loop
fetch	c04 into
	nr_seq_forma_pagto_w;
exit	when c04%notfound;

	select	count(*)
	into	qt_cont_w
	from	forma_pagto_cartao_cr a
	where	a.nr_sequencia	= nr_seq_forma_pagto_w;

	if	(qt_cont_w	= 0) then
		insert	into log_tasy
			(cd_log,
			ds_log,
			dt_atualizacao,
			nm_usuario)
		values	(99890,
			'A forma de pagamento de sequ�ncia ' || nr_seq_forma_pagto_w || ' n�o foi localizada no Tasy!',
			sysdate,
			'Tasy');
	end if;

end	loop;
close	c04;

select	count(*)
into	qt_cont_w
from	log_tasy a
where	a.cd_log	= 99890;

if	(qt_cont_w	> 0) then
	delete	from w_movto_cartao_cr;
	commit;
	/* Foram encontradas inconsist�ncias no arquivo! Verifique o log de c�digo 99890! */
	wheb_mensagem_pck.exibir_mensagem_abort(263167);
end if;

open	c01;
loop
fetch	c01 into
	cd_estabelecimento_w,
	ds_comprovante_w,
	dt_transacao_w,
	ie_lib_caixa_w,
	ie_situacao_w,
	ie_tipo_cartao_w,
	nr_autorizacao_w,
	nr_seq_bandeira_w,
	nr_seq_forma_pagto_w,
	vl_transacao_w;
exit	when c01%notfound;

	insert	into movto_cartao_cr
		(cd_estabelecimento,
		ds_comprovante,
		ds_observacao,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_transacao,
		ie_lib_caixa,
		ie_situacao,
		ie_tipo_cartao,
		nm_usuario,
		nm_usuario_nrec,
		nr_autorizacao,
		nr_seq_bandeira,
		nr_seq_forma_pagto,
		nr_sequencia,
		vl_transacao)
	values	(cd_estabelecimento_w,
		ds_comprovante_w,
		'Movimento importado de arquivo externo.',
		sysdate,
		sysdate,
		dt_transacao_w,
		ie_lib_caixa_w,
		ie_situacao_w,
		ie_tipo_cartao_w,
		'Tasy',
		'Tasy',
		nr_autorizacao_w,
		nr_seq_bandeira_w,
		nr_seq_forma_pagto_w,
		movto_cartao_cr_seq.nextval,
		vl_transacao_w);

end	loop;
close	c01;

delete	from w_movto_cartao_cr;

commit;

end GERAR_W_MOVTO_CARTAO_CR;
/