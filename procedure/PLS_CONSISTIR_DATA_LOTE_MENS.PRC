create or replace
procedure pls_consistir_data_lote_mens
			(	dt_mesano_ref_p		in	date,
				dt_contabil_p		in	date,
				ds_retorno_p		out	varchar2,
				cd_estabelecimento_p	in	number,
				nm_usuario_p		in	varchar2) is 

ds_erro_w			varchar2(255);
ie_permitido_w			varchar2(1);
ie_lote_futuro_w		varchar2(1);
ie_contab_posterior_w		varchar2(1);
dt_contabil_w			date;
qt_meses_ant_w			number(10);
ie_antecipacao_geracao_w	varchar2(1);

begin
ds_erro_w	:= '';

/* Se data de refer�ncia do lote for maior que o m�s atual + 1 ent�o � obrigat�rio informar a data de contabiliza��o */
select	to_number(obter_valor_param_usuario(1205,13,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p))
into	qt_meses_ant_w
from	dual;

if	(to_char(dt_contabil_p,'yyyy') = '1899') then
	dt_contabil_w	:= null;
else
	dt_contabil_w	:= dt_contabil_p;
end if;

/* Comentado conforme OS 562939
select	max(ie_antecipacao_geracao)
into	ie_antecipacao_geracao_w
from	pls_parametro_contabil
where	cd_estabelecimento	= cd_estabelecimento_p;

begin
select	'N'
into	ie_permitido_w
from	dual
where	trunc(dt_mesano_ref_p,'month') > trunc(add_months(sysdate,1),'month')
and		dt_contabil_w is null;
exception
	when no_data_found then
	ie_permitido_w   := 'S';
end;

if	(ie_permitido_w = 'N') and
	(nvl(ie_antecipacao_geracao_w,'N') = 'N')then
	ds_erro_w	:= '� necess�rio informar a data de antecipa��o do lote.' || chr(13);
end if;*/

/* Data de refer�ncia n�o pode ser menor que 2 meses anterior a data atual */
begin
select	'N'
into	ie_lote_futuro_w
from	dual
where	trunc(dt_mesano_ref_p,'month') < add_months(trunc(sysdate,'month'), -qt_meses_ant_w);
exception
	when no_data_found then
	ie_lote_futuro_w   := 'S';
end;

if	(ie_lote_futuro_w = 'N') then
	ds_erro_w	:= ds_erro_w || 'A data de refer�ncia n�o pode ser menor que ' || to_char(add_months(trunc(sysdate,'month'), -qt_meses_ant_w),'mm/yyyy') || chr(13)||'Favor verifique o par�metro 13!';
end if;

/* A data de contabiliza��o n�o pode ser maior que m�s atual + 2 */
begin
select	'N'
into	ie_contab_posterior_w
from	dual
where	trunc(dt_contabil_w,'month') > trunc(add_months(sysdate,1),'month');
exception
	when no_data_found then
	ie_contab_posterior_w   := 'S';
end;

if	(ie_contab_posterior_w = 'N') then
	ds_erro_w	:= ds_erro_w || 'A data de contabiliza��o n�o pode ser maior que o m�s ' || to_char(trunc(add_months(sysdate,1),'month'),'mm/yyyy') || chr(13);
end if; 

/* A data de contabiliza��o n�o pode ser menor que 2 meses anterior a data atual */
if	(trunc(dt_contabil_w,'month') < add_months(trunc(sysdate,'month'), -qt_meses_ant_w)) then
	ds_erro_w	:= ds_erro_w || 'A data de contabiliza��o n�o pode ser anterior ao m�s ' || to_char(add_months(trunc(sysdate,'month'), -qt_meses_ant_w),'mm/yyyy') || chr(13)||'. Favor verifique o par�metro 13!';
end if;

/* A data de contabiliza��o n�o pode ser maior que a data de refer�ncia */
if	(trunc(dt_contabil_w,'month') > trunc(dt_mesano_ref_p,'month')) then
	ds_erro_w	:= ds_erro_w || 'A data de contabiliza��o n�o pode ser maior que o m�s de refer�ncia ' || chr(13);
end if;

/* A data de contabiliza��o n�o pode ser igual a data de refer�ncia */
if	(trunc(dt_contabil_w,'month') = trunc(dt_mesano_ref_p,'month')) then
	ds_erro_w	:= ds_erro_w || 'A data de contabiliza��o n�o pode ser igual ao m�s de refer�ncia ' || chr(13);
end if;

ds_retorno_p	:= ds_erro_w;

commit;

end pls_consistir_data_lote_mens;
/