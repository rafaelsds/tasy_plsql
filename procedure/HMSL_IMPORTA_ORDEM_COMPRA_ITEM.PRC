create or replace
procedure HMSL_IMPORTA_ORDEM_COMPRA_ITEM is 


qt_existe_w				number(10);

nr_ordem_compra_w			number(10);
nr_ordem_compra_ww			number(10);
nr_item_oci_w				number(10);
cd_material_w				number(10);
cd_unidade_medida_compra_w		varchar2(30);
vl_unitario_material_w			number(13,4);
qt_material_w				number(13,4);
cd_pessoa_solicitante_w			varchar2(10);
qt_material_entregue_w			number(13,4);
pr_descontos_w				number(13,4);
cd_local_estoque_w			number(10);
ds_material_direto_w			varchar2(255);
ds_observacao_ww			varchar2(32000);
ds_observacao_w				varchar2(255);
ds_marca_w				varchar2(30);
vl_item_liquido_w			number(15,2);
dt_aprovacao_w				date;
cd_centro_custo_w			number(10);
cd_conta_contabil_w			varchar2(20);
nr_solic_compra_w			number(10);
nr_item_solic_compra_w			number(10);
qt_conv_unid_fornec_w			number(9,4);
ie_geracao_solic_w			varchar2(1);
pr_desc_financ_w			number(7,4);
nr_seq_conta_financ_w			number(10);
qt_original_w				number(13,4);
dt_validade_w				date;
nr_seq_marca_w				number(10);
dt_reprovacao_w				date;
nr_seq_unidade_adic_w			number(10);
nr_serie_material_w			varchar2(80);
vl_desconto_w				number(13,2);
dt_aprovacao_orig_w			date;
dt_reprovacao_orig_w			date;
nr_contrato_w				number(10);
dt_inicio_garantia_w			date;
dt_fim_garantia_w			date;
nr_id_integracao_w			varchar2(255);
nr_seq_conta_bco_w			number(10);
nr_empenho_w				varchar2(255);
dt_empenho_w				date;
nr_ordem_agrup_w			number(10);
vl_unit_mat_original_w			number(13,4);
nr_atendimento_w			number(10);
qt_dias_garantia_w			number(10);

cursor c01 is
select	nr_ordem_compra nr_ordem_compra,
	nr_item_oci nr_item_oci,
	cd_material cd_material,
	cd_unidade_medida_compra cd_unidade_medida_compra,
	vl_unitario_material vl_unitario_material,
	qt_material qt_material,
	cd_pessoa_solicitante cd_pessoa_solicitante,
	qt_material_entregue qt_material_entregue,
	nvl(pr_descontos,0) pr_descontos,
	cd_local_estoque cd_local_estoque,
	ds_material_direto ds_material_direto,
	ds_observacao ds_observacao,
	ds_marca ds_marca,
	vl_item_liquido vl_item_liquido,
	dt_aprovacao dt_aprovacao,
	cd_centro_custo cd_centro_custo,
	cd_conta_contabil cd_conta_contabil,	
	qt_conv_unid_fornec qt_conv_unid_fornec,
	nvl(ie_geracao_solic,'U') ie_geracao_solic,
	nvl(pr_desc_financ,0) pr_desc_financ,
	nr_seq_conta_financ nr_seq_conta_financ,
	qt_original qt_original,
	dt_validade dt_validade,
	nr_seq_marca nr_seq_marca,
	dt_reprovacao dt_reprovacao,
	nr_seq_unidade_adic nr_seq_unidade_adic,
	nr_serie_material nr_serie_material,
	nvl(vl_desconto,0) vl_desconto,
	dt_aprovacao_orig dt_aprovacao_orig,
	dt_reprovacao_orig dt_reprovacao_orig,
	nr_contrato nr_contrato,
	dt_inicio_garantia dt_inicio_garantia,
	dt_fim_garantia dt_fim_garantia,
	nr_id_integracao nr_id_integracao,
	nr_seq_conta_bco nr_seq_conta_bco,
	nr_empenho nr_empenho,
	dt_empenho dt_empenho,
	nr_ordem_agrup nr_ordem_agrup,
	vl_unit_mat_original vl_unit_mat_original,
	nr_atendimento nr_atendimento,
	qt_dias_garantia qt_dias_garantia
from	oci_hmsl a
where	not exists(	select	1
			from	hmsl_log_importacao_ordem x
			where	x.nr_ordem_compra = a.nr_ordem_compra);


begin

delete from hmsl_log_importacao_ordem_item;

open C01;
loop
fetch C01 into	
	nr_ordem_compra_w,
	nr_item_oci_w,
	cd_material_w,
	cd_unidade_medida_compra_w,
	vl_unitario_material_w,
	qt_material_w,
	cd_pessoa_solicitante_w,
	qt_material_entregue_w,
	pr_descontos_w,
	cd_local_estoque_w,
	ds_material_direto_w,
	ds_observacao_ww,
	ds_marca_w,
	vl_item_liquido_w,
	dt_aprovacao_w,
	cd_centro_custo_w,
	cd_conta_contabil_w,
	qt_conv_unid_fornec_w,
	ie_geracao_solic_w,
	pr_desc_financ_w,
	nr_seq_conta_financ_w,
	qt_original_w,
	dt_validade_w,
	nr_seq_marca_w,
	dt_reprovacao_w,
	nr_seq_unidade_adic_w,
	nr_serie_material_w,
	vl_desconto_w,
	dt_aprovacao_orig_w,
	dt_reprovacao_orig_w,
	nr_contrato_w,
	dt_inicio_garantia_w,
	dt_fim_garantia_w,
	nr_id_integracao_w,
	nr_seq_conta_bco_w,
	nr_empenho_w,
	dt_empenho_w,
	nr_ordem_agrup_w,
	vl_unit_mat_original_w,
	nr_atendimento_w,
	qt_dias_garantia_w;
exit when C01%notfound;
	begin
	ds_observacao_w := substr(ds_observacao_ww,1,255);

	nr_ordem_compra_ww := nr_ordem_compra_w;

	select	max(nr_ordem_compra)
	into	nr_ordem_compra_w
	from	ordem_compra
	where	nr_documento_externo = nr_ordem_compra_ww;

	
	if	(nvl(nr_ordem_compra_ww,0) = 0) then
		insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (null,null,'N�o possui numero da ordem de compra. Campo obrigat�rio');
	else
		select	count(*)
		into	qt_existe_w
		from	ordem_compra
		where	nr_ordem_compra = nr_ordem_compra_w;
		
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'Esta OC n�o existe no Tasy ' || nr_ordem_compra_w);
		end if;
	end if;

	select	count(*)
	into	qt_existe_w
	from	ordem_compra_item
	where	nr_ordem_compra = nvl(nr_ordem_compra_w,0)
	and	cd_material = cd_material_w;

	if	(qt_existe_w > 0) then
		insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'J� existe o material ' || cd_material_w || ' na OC');
	else
		select	nvl(max(nr_item_oci),0) + 1
		into	nr_item_oci_w
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_w;
	end if;
	
	if	(nvl(cd_material_w,0) = 0) then
		insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o possui material informado. Campo obrigat�rio');
	else
		select	count(*)
		into	qt_existe_w
		from	material
		where	cd_material = cd_material_w;
		
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo do material ' || cd_material_w);
		end if;
	end if;
	
	if	(nvl(vl_unitario_material_w,0) = 0) then
		insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o possui valor unitario informado. Campo obrigat�rio');
	end if;
	
	if	(nvl(qt_material_w,0) = 0) then
		insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o possui quantidade material informada. Campo obrigat�rio');
	end if;
	
	if	(nvl(cd_unidade_medida_compra_w,'0') = '0') then
		insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o possui unidade de medida. Campo obrigat�rio');
	else
		select	count(*)
		into	qt_existe_w
		from	unidade_medida
		where	upper(cd_unidade_medida) = upper(cd_unidade_medida_compra_w);
		
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo da un. medida ' || cd_unidade_medida_compra_w);
		end if;
	end if;
	
	if	(nvl(cd_pessoa_solicitante_w,'0') > '0') then		
		select	count(*)
		into	qt_existe_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_solicitante_w;
		
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy a pessoa solicitante ' || cd_pessoa_solicitante_w);
		end if;
	end if;
	
	if	(nvl(cd_local_estoque_w,0) > 0) then
		select	count(*)
		into	qt_existe_w
		from	local_estoque
		where	cd_local_estoque = cd_local_estoque_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo do local estoque ' || cd_local_estoque_w);
		end if;
	end if;
	
	if	(cd_centro_custo_w = 0) then
		cd_centro_custo_w := null;
	end if;

	if	(nvl(cd_centro_custo_w,0) > 0) then
		select	count(*)
		into	qt_existe_w
		from	centro_custo
		where	cd_centro_custo = cd_centro_custo_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo do centro custo ' || cd_centro_custo_w);
		end if;
	end if;	
	
	if	(nvl(cd_conta_contabil_w,'0') > '0') then
		select	count(*)
		into	qt_existe_w
		from	conta_contabil
		where	cd_conta_contabil = cd_conta_contabil_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo da conta contabil ' || cd_conta_contabil_w);
		end if;
	end if;
	
	if	(nvl(nr_seq_conta_financ_w,0) > 0) then
		select	count(*)
		into	qt_existe_w
		from	conta_financeira
		where	cd_conta_financ = nr_seq_conta_financ_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo da conta financeira ' || nr_seq_conta_financ_w);
		end if;
	end if;
	
	if	(nvl(nr_seq_marca_w,0) > 0) then
		select	count(*)
		into	qt_existe_w
		from	marca
		where	nr_sequencia = nr_seq_marca_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo da marca ' || nr_seq_marca_w);
		end if;
	end if;
	
	if	(nvl(nr_seq_unidade_adic_w,0) > 0) then
		select	count(*)
		into	qt_existe_w
		from	unidade_medida_adic_compra
		where	nr_sequencia = nr_seq_unidade_adic_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo da unidade medida acic ' || nr_seq_unidade_adic_w);
		end if;
	end if;
	
	if	(nvl(nr_contrato_w,0) > 0) then
		select	count(*)
		into	qt_existe_w
		from	contrato
		where	nr_sequencia = nr_contrato_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo do contrato ' || nr_contrato_w);
		end if;
	end if;
	
	if	(nvl(nr_seq_conta_bco_w,0) > 0) then
		select	count(*)
		into	qt_existe_w
		from	BANCO_ESTABELECIMENTO
		where	nr_Sequencia = nr_seq_conta_bco_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo do banco estabelecimento ' || nr_seq_conta_bco_w);
		end if;
	end if;
	
	if	(nvl(nr_atendimento_w,0) > 0) then
		select	count(*)
		into	qt_existe_w
		from	ATENDIMENTO_PACIENTE
		where	nr_atendimento = nr_atendimento_w;
	
		if	(qt_existe_w = 0) then
			insert into hmsl_log_importacao_ordem_item(nr_ordem_compra,nr_item_oci, ds_log) values (nr_ordem_compra_ww,nr_item_oci_w,'N�o existe no Tasy o c�digo do atendimento ' || nr_atendimento_w);
		end if;
	end if;
	
	commit;
	
	select	count(*)
	into	qt_existe_w
	from	hmsl_log_importacao_ordem_item
	where	nr_ordem_compra = nvl(nr_ordem_compra_ww,0)
	and	nr_item_oci = nvl(nr_item_oci_w,0);
	
	if	(qt_existe_w = 0) and
		(nvl(nr_ordem_compra_w,0) > 0) and
		(nvl(nr_item_oci_w,0) > 0) then
	
		insert into ordem_compra_item(
			nr_ordem_compra,
			nr_item_oci,
			cd_material,
			cd_unidade_medida_compra,
			vl_unitario_material,
			qt_material,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			cd_pessoa_solicitante,
			qt_material_entregue,
			pr_descontos,
			cd_local_estoque,
			ds_material_direto,
			ds_observacao,
			ds_marca,
			vl_item_liquido,
			dt_aprovacao,
			cd_centro_custo,
			cd_conta_contabil,
			qt_conv_unid_fornec,
			ie_geracao_solic,
			pr_desc_financ,
			nr_seq_conta_financ,
			qt_original,
			dt_validade,
			nr_seq_marca,
			dt_reprovacao,
			nr_seq_unidade_adic,
			nr_serie_material,
			vl_desconto,
			dt_aprovacao_orig,
			dt_reprovacao_orig,
			nr_contrato,
			dt_inicio_garantia,
			dt_fim_garantia,
			nr_id_integracao,
			nr_seq_conta_bco,
			nr_empenho,
			dt_empenho,
			nr_ordem_agrup,
			vl_unit_mat_original,
			nr_atendimento,
			qt_dias_garantia)
		values(	nr_ordem_compra_w,
			nr_item_oci_w,
			cd_material_w,
			cd_unidade_medida_compra_w,
			vl_unitario_material_w,
			qt_material_w,
			sysdate,
			'Importacao',
			'A',
			cd_pessoa_solicitante_w,
			qt_material_entregue_w,
			pr_descontos_w,
			cd_local_estoque_w,
			ds_material_direto_w,
			ds_observacao_w,
			ds_marca_w,
			vl_item_liquido_w,
			dt_aprovacao_w,
			cd_centro_custo_w,
			cd_conta_contabil_w,
			qt_conv_unid_fornec_w,
			ie_geracao_solic_w,
			pr_desc_financ_w,
			nr_seq_conta_financ_w,
			qt_original_w,
			dt_validade_w,
			nr_seq_marca_w,
			dt_reprovacao_w,
			nr_seq_unidade_adic_w,
			nr_serie_material_w,
			vl_desconto_w,
			dt_aprovacao_orig_w,
			dt_reprovacao_orig_w,
			nr_contrato_w,
			dt_inicio_garantia_w,
			dt_fim_garantia_w,
			nr_id_integracao_w,
			nr_seq_conta_bco_w,
			nr_empenho_w,
			dt_empenho_w,
			nr_ordem_agrup_w,
			vl_unit_mat_original_w,
			nr_atendimento_w,
			qt_dias_garantia_w);
			
	end if;	
	end;
end loop;
close C01;

commit;

end HMSL_IMPORTA_ORDEM_COMPRA_ITEM;
/