create or replace
procedure validacao_exclusao_orgao_cobr(
			nr_titulo_p		number,
			nr_seq_cheque_p		number,
			nr_seq_cobranca_p	number) is 

nr_titulo_w		number(10);
nr_cheque_w		number(10);
nr_outros_w		number(10);

begin

select	count(*)
into	nr_titulo_w
from	titulo_receber_orgao_cobr
where	nr_titulo = nr_titulo_p
and	nr_seq_cobranca	= nr_seq_cobranca_p;

select	count(*)
into	nr_cheque_w
from	cheque_cr_orgao_cobr
where	nr_seq_cheque = nr_seq_cheque_p
and	nr_seq_cobranca	= nr_seq_cobranca_p;

select	count(*)
into	nr_outros_w
from	outros_orgao_cobr
where	nr_seq_cobranca	= nr_seq_cobranca_p;

if	(nr_titulo_w > 0) or
	(nr_cheque_w > 0) or
	(nr_outros_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(337218);
end if;

commit;

end validacao_exclusao_orgao_cobr;
/