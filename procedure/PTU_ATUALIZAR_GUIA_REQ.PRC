create or replace
procedure ptu_atualizar_guia_req( 	nr_seq_pedido_aut_p  	ptu_pedido_autorizacao.nr_sequencia%type,
					ie_possui_pedido_p	varchar2,
					nm_usuario_p		usuario.nm_usuario%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is

cursor C01(nr_seq_pedido_aut_pc ptu_pedido_autorizacao.nr_sequencia%type) is
	select	a.ds_observacao,
		a.nr_seq_guia,
		a.nr_seq_requisicao,
		a.dt_atendimento,
		a.cd_unimed_executora,
		a.ie_alto_custo,
		(select	b.nr_sequencia
		from    pls_segurado_carteira c,
			pls_segurado b
		where   c.nr_seq_segurado	= b.nr_sequencia
		and	c.cd_usuario_plano	= lpad(a.cd_unimed,4,0)||a.cd_usuario_plano) nr_seq_segurado,
		(select	c.nr_sequencia
		from	ptu_controle_execucao c
		where	c.nr_seq_pedido_aut = a.nr_sequencia) nr_seq_exec_control,
		(select	max(nr_sequencia)
		from	ptu_resposta_autorizacao c
		where	c.nr_seq_execucao	= a.nr_seq_execucao
		and	c.cd_unimed_executora	= a.cd_unimed_executora) nr_seq_resp_ped_aut
	from	ptu_pedido_autorizacao a
	where	a.nr_sequencia = nr_seq_pedido_aut_pc;

begin

--Carrega os anexos importados no pedido de autoriza��o
for c01_w in C01( nr_seq_pedido_aut_p ) loop

	-- Se j� existia um pedido de autoriza��o na base, o sistema n�o pode gerar outro
	if	( ie_possui_pedido_p = 'N' ) then
		--Realiza a consist�ncia do pedido
		ptu_atualiza_imp_ped_autor_scs(	c01_w.nr_seq_guia, c01_w.nr_seq_requisicao, c01_w.nr_seq_segurado,
						null, c01_w.ds_observacao, null,
						c01_w.ie_alto_custo, c01_w.dt_atendimento, cd_estabelecimento_p,
						nm_usuario_p, c01_w.cd_unimed_executora, 'A');
	end if;

	--Se j� existir uma resposta para o pedido de autoriza��o, n�o � necess�rio gerar novamente
	if	 ( c01_w.nr_seq_resp_ped_aut is null ) then
		--Gera a resposta do pedido de autoriza��o
		ptu_gestao_env_resp_pedido_aut( c01_w.nr_seq_exec_control, cd_estabelecimento_p, nm_usuario_p);
	end if;
end loop;

end ptu_atualizar_guia_req;
/