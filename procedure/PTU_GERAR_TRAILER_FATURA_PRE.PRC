create or replace
procedure ptu_gerar_trailer_fatura_pre
			(	nr_seq_fatura_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
			
vl_tot_mensalidade_original_w	number(12,2);
vl_tot_inscricao_original_w	number(12,2);
vl_tot_mensalidade_cobrado_w	number(12,2);
vl_tot_inscricao_cobrado_w	number(12,2);
qt_tot_beneficiarios_w		number(10);
vl_tot_total_ajuste_w		number(12,2);

begin

select	sum(vl_mensalidade_original),
	sum(vl_inscricao_original),
	sum(vl_mensalidade_cobrado),
	sum(vl_inscricao_cobrado),
	sum(vl_ajuste_cobrado)
into	vl_tot_mensalidade_original_w,
	vl_tot_inscricao_original_w,
	vl_tot_mensalidade_cobrado_w,
	vl_tot_inscricao_cobrado_w,
	vl_tot_total_ajuste_w
from	ptu_fatura_pre_cobranca
where	nr_seq_fatura	= nr_seq_fatura_p;

select	count(*)
into	qt_tot_beneficiarios_w
from	ptu_fatura_pre_cobranca
where	nr_seq_fatura	= nr_seq_fatura_p;

insert into ptu_fatura_pre_total
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		nr_seq_fatura,vl_tot_mens_original,vl_tot_insc_original,vl_total_mensalidade,vl_total_inscricao,
		vl_total_ajuste,qt_total_r802,tp_registro	)
values	(	ptu_fatura_pre_total_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		nr_seq_fatura_p,nvl(vl_tot_mensalidade_original_w,0),nvl(vl_tot_inscricao_original_w,0),nvl(vl_tot_mensalidade_cobrado_w,0),nvl(vl_tot_inscricao_cobrado_w,0),
		nvl(vl_tot_total_ajuste_w,0),qt_tot_beneficiarios_w,9);

end ptu_gerar_trailer_fatura_pre;
/
