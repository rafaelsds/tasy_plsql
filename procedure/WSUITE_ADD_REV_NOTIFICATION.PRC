create or replace
procedure wsuite_add_rev_notification(
			nm_usuario_p		varchar2,
			cd_pessoa_fisica_p	varchar2) is 

vl_param_14_w			varchar2(255);
dt_last_update_pf_w		date;
dt_last_update_compl_w		date;
qt_days_last_update_w		number(10);
nr_seq_last_unread_not_w	wsuite_notification.nr_sequencia%type;
nr_seq_last_unread_rev_not_w	wsuite_notification.nr_sequencia%type;

begin

select	tws_obter_param_tipo_login('1', '9111' ,'14')
into	vl_param_14_w
from	dual;

if (vl_param_14_w is not null) and (obter_se_somente_numero(vl_param_14_w) = 'S') then

	select	max(nr_sequencia)
	into	nr_seq_last_unread_rev_not_w
	from	wsuite_notification a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.ie_notification_type = 'RC'
	and	a.ie_notification_status in ('U', 'V');

	select	max(nr_sequencia)
	into	nr_seq_last_unread_not_w
	from	wsuite_notification a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.ie_notification_type <> 'RC'
	and	a.ie_notification_status in ('U', 'V');
	
	if (nr_seq_last_unread_rev_not_w is null) or (nr_seq_last_unread_not_w > nr_seq_last_unread_rev_not_w) then

		if (nr_seq_last_unread_not_w > nr_seq_last_unread_rev_not_w) then
			update	wsuite_notification
			set	ie_notification_status = 'R'
			where	cd_pessoa_fisica = cd_pessoa_fisica_p
			and	ie_notification_type = 'RC'
			and	ie_notification_status in ('U', 'V');
			commit;
		end if;
	
		select	max(dt_atualizacao)
		into	dt_last_update_pf_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_p;

		if (dt_last_update_pf_w is not null) then
			select	max(dt_atualizacao)
			into	dt_last_update_compl_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica = cd_pessoa_fisica_p
			and	ie_tipo_complemento = 1;

			if (dt_last_update_compl_w is null) then
				select	trunc(sysdate - dt_last_update_pf_w)
				into	qt_days_last_update_w
				from	dual;
			else
				select	trunc(sysdate - greatest(dt_last_update_pf_w, dt_last_update_compl_w))
				into	qt_days_last_update_w
				from	dual;
			end if;

			if (qt_days_last_update_w >= somente_numero(vl_param_14_w)) then
				wsuite_add_notification('RC', null, null, nm_usuario_p, cd_pessoa_fisica_p, null);
				commit;
			end if;
		end if;
	else
		update	wsuite_notification
		set	ie_notification_status = 'U'
		where	nr_sequencia = nr_seq_last_unread_rev_not_w
		and	ie_notification_status in ('R', 'V');
		commit;
	end if;
end if;

end wsuite_add_rev_notification;
/
