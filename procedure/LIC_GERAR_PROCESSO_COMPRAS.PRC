create or replace
procedure lic_gerar_processo_compras(		cd_estabelecimento_p		number,
					nr_seq_licitacao_p			number,
					nr_cot_compra_p			number,
					nm_usuario_p			varchar2) is

qt_existe_w			number(10);
ds_erro_w			varchar2(255) := '';
ds_observacao_solic_w		varchar2(2000);
ie_fechar_compras_lic_w		varchar2(15);

begin

select	nvl(ie_fechar_compras_lic,'N')
into	ie_fechar_compras_lic_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

select	count(*)
into	qt_existe_w
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_p
and	lic_obter_tipo_compra(nr_seq_tipo_compra) = 'D';

if	(qt_existe_w > 0) then
	lic_consiste_gerar_proc_compra(cd_estabelecimento_p,nr_cot_compra_p,nm_usuario_p);
	
	if	(ie_fechar_compras_lic_w = 'N') then	
		begin
		lic_gerar_compra_direta(cd_estabelecimento_p,nr_cot_compra_p,nm_usuario_p,ds_observacao_solic_w);
		exception
		when others then
			/*'Erro ao gerar compra direta (procedure LIC_GERAR_COMPRA_DIRETA) '*/
			ds_erro_w := substr(wheb_mensagem_pck.get_texto(303620) || SQLERRM(sqlcode),1,255);
		end;
	end if;
	
	if	(ie_fechar_compras_lic_w = 'M') then	
		begin
		lic_gerar_compra_direta_manter(cd_estabelecimento_p,nr_cot_compra_p,nm_usuario_p,ds_observacao_solic_w);
		exception
		when others then
			/*'Erro ao gerar compra direta (procedure LIC_GERAR_COMPRA_DIRETA_MANTER) '*/
			ds_erro_w := substr(wheb_mensagem_pck.get_texto(303622) || SQLERRM(sqlcode),1,255);
		end;
	end if;	
end if;

if	(ds_erro_w is null) then

	select	count(*)
	into	qt_existe_w
	from	cot_compra_item
	where	nr_cot_compra = nr_cot_compra_p
	and	lic_obter_tipo_compra(nr_seq_tipo_compra) = 'L';

	if	(qt_existe_w > 0) then
		
		if	(ie_fechar_compras_lic_w = 'N') then
			begin
			lic_gerar_compra_licitacao(cd_estabelecimento_p,nr_cot_compra_p,nr_seq_licitacao_p,nm_usuario_p,ds_observacao_solic_w);
			exception
			when others then
				/*'Erro ao gerar compra licita��o (procedure LIC_GERAR_COMPRA_LICITACAO) '*/
				ds_erro_w := substr(wheb_mensagem_pck.get_texto(303623) || SQLERRM(sqlcode),1,255);
			end;
		end if;
		
		if	(ie_fechar_compras_lic_w = 'M') then
			begin
			lic_gerar_compra_licit_manter(cd_estabelecimento_p,nr_cot_compra_p,nr_seq_licitacao_p,nm_usuario_p,ds_observacao_solic_w);
			exception
			when others then
				/*'Erro ao gerar compra licita��o (procedure LIC_GERAR_COMPRA_LICIT_MANTER) '*/
				ds_erro_w := substr( wheb_mensagem_pck.get_texto(303625) || SQLERRM(sqlcode),1,255);
			end;
		end if;
	end if;
end if;

if	(ds_erro_w is not null) then
	lic_estornar_processo_compras(nr_cot_compra_p,nm_usuario_p);
	wheb_mensagem_pck.exibir_mensagem_abort(193436,'DS_ERRO_W='||ds_erro_w);
end if;

commit;

end lic_gerar_processo_compras;
/