create or replace
procedure atualiza_emissao_prescr(nr_prescricao_p		number) is 

begin
update	prescr_dieta
set 	dt_emissao_dieta = sysdate
where 	nr_prescricao = nr_prescricao_p;

update 	rep_jejum 
set 	dt_emissao = sysdate 
where 	nr_prescricao = nr_prescricao_p;

update 	nut_paciente
set 	dt_emissao = sysdate
where 	nr_prescricao = nr_prescricao_p;

commit;

end atualiza_emissao_prescr;
/


