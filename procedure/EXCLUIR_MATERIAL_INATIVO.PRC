create or replace
PROCEDURE excluir_material_inativo is

/*				ATEN��O
Devido a necessidade de remo��o das grava��es na tabela LOG_xxxxxTASY, esta procedure foi alterada para exibir
os erros atrav�s da sa�da do console (DBMS_OUTPUT). Antes de execut�-la no SQLPlus, utilize o comando "set serverout on".
*/

cd_material_w			Number(15,0);
i				Number(10,0);
ds_material_w			Varchar2(255);

cursor c01 is
	select	cd_material,
		ds_material
	from	material
	where	ie_situacao = 'I';

BEGIN

FOR i IN 1..3 LOOP 
	begin
	OPEN C01;
	LOOP
	FETCH C01 into
		cd_material_w,
		ds_material_w;
	EXIT WHEN C01%NOTFOUND;
		BEGIN
		
		delete	from material
		where	cd_material	= cd_material_w;

		EXCEPTION
			when others then
			if	(i = 3) then
				dbms_output.put_line(WHEB_MENSAGEM_PCK.get_texto(819439, 'CD_MATERIAL_W=' || cd_material_w) || ' - ' || sqlerrm); /* 'N�o foi poss�vel excluir: #@CD_MATERIAL_W#@' */
			end if;
		END;
	END LOOP;
	close c01;

	commit;
       end;
END LOOP;


END excluir_material_inativo;
/