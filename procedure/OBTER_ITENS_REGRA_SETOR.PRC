create or replace	
procedure obter_itens_regra_setor	(ie_tipo_atendimento_p	number,
					cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					ds_retorno_regra_p	out varchar2) is

dt_nascimento_w			Date;
dt_obito_w			Date;
ds_setor_atendimento_w		varchar2(70);
ds_lista_w			varchar2(2000);
qt_idade_w			number(15,4);
ie_mostra_regra_w		varchar2(1);
qt_itens_w			number(5);
	
cursor c01 is
	select  substr(obter_nome_setor(b.cd_setor_atendimento),1,70)
	from	regra_setor a,
		setor_atendimento b
	where	a.cd_setor_atendimento = b.cd_setor_atendimento
	and	qt_idade_w between a.QT_IDADE_MIN and a.QT_IDADE_MAX
	and	cd_classif_setor = 1;

begin	
ds_retorno_regra_p	:= null;

Obter_Param_Usuario(916,261,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_mostra_regra_w);

select	max(a.dt_nascimento),
	max(nvl(a.dt_obito,sysdate))
into	dt_nascimento_w,
	dt_obito_w
from	pessoa_fisica a
where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;

select	max(obter_idade(dt_nascimento_w,dt_obito_w,'A'))
into	qt_idade_w
from	dual;

select  	count(*)
into	qt_itens_w
from	regra_setor a,
	setor_atendimento b
where	a.cd_setor_atendimento = b.cd_setor_atendimento
and	qt_idade_w between a.QT_IDADE_MIN and a.QT_IDADE_MAX
and	cd_classif_setor = 1;

if	(ie_tipo_atendimento_p = 3) and (ie_mostra_regra_w = 'S') and (qt_itens_w > 1) then

	select	a.dt_nascimento,
		nvl(a.dt_obito,sysdate)
	into	dt_nascimento_w,
		dt_obito_w
	from	pessoa_fisica a
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
	select	obter_idade(dt_nascimento_w,dt_obito_w,'A')
	into	qt_idade_w
	from	dual;
	
	
	open C01;
	loop
	fetch C01 into	
		ds_setor_atendimento_w;
	exit when C01%notfound;
		begin
		
		ds_lista_w	:= ds_lista_w || ds_setor_atendimento_w || chr(10);
		
		end;
	end loop;
	close C01;
	
	if	(ds_lista_w is not null) then
		ds_lista_w := OBTER_DESC_EXPRESSAO(756508,'De acordo com a idade do paciente o mesmo utilizar o(s) seguinte(s) setores:') ||  chr(10) || ds_lista_w;
	end if;

	ds_retorno_regra_p	:= substr(ds_lista_w,1,255);

end if;
				
end;
/