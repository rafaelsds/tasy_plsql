create or replace
procedure man_registra_lib_canc_ocorr	(	ie_tipo_reg_p		varchar2,
											ds_mensagem_p		varchar2,
											nr_sequencia_p		number,
											nr_seq_ordem_p		number,
											nm_usuario_p		varchar2,
											ie_tipo_usuario_p	varchar2) is 
ie_tipo_usuario_w		varchar2(1);

begin

/*
Criei o tratamento para o par�metro IE_TIPO_USUARIO_P, porque no JAVA
a consulta do tipo de usuario era feito no fonte. Isso pode ser feito diretamente
dentro do objeto
*/

ie_tipo_usuario_w	:= nvl(ie_tipo_usuario_p,'X');

if	(ie_tipo_usuario_w = 'X') then
	begin

	select	decode(count(*),0,'L','G')
	into	ie_tipo_usuario_w
	from	gerencia_wheb
	where	nm_usuario = nm_usuario_p;

	end;
end if;

if (ie_tipo_reg_p = 'L') then

	update	man_ordem_serv_ocorrencia
	set 	ds_compl_lib = ds_mensagem_p,
			dt_liberacao = sysdate,
			nm_usuario_liberacao = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p
	and		nr_seq_ordem_serv = nr_seq_ordem_p;

elsif (ie_tipo_reg_p = 'C') then

	if (ie_tipo_usuario_w = 'G') then
	
		update	man_ordem_serv_ocorrencia
		set 	ds_justif_cancel = ds_mensagem_p,
				dt_cancelamento = sysdate,
				nm_usuario_cancel = nm_usuario_p
		where	nr_sequencia = nr_sequencia_p
		and		nr_seq_ordem_serv = nr_seq_ordem_p;

	elsif (ie_tipo_usuario_w = 'L') then
	
		update	man_ordem_serv_ocorrencia
		set 	ds_justif_cancel = ds_mensagem_p,
				dt_cancelamento = sysdate,
				nm_usuario_cancel = nm_usuario_p
		where	nr_sequencia = nr_sequencia_p
		and		nr_seq_ordem_serv = nr_seq_ordem_p;
	
	end if;
			
end if;

commit;

end man_registra_lib_canc_ocorr;
/