create or replace PROCEDURE p_integrar_limites_bb
  (
    nr_atendimento_p        atend_paciente_unidade.nr_atendimento%type,
    cd_setor_atendimento_p  atend_paciente_unidade.cd_setor_atendimento%TYPE,
    cd_pessoa_fisica_p      pessoa_fisica.cd_pessoa_fisica%TYPE
  )

IS

  json_limits               philips_json;
  clob_limits               clob;
  envio_integracao_bb       clob;
  retorno_integracao_bb     clob;
  
BEGIN
  IF (OBTER_SE_INTEGRACAO_ATIVA(973, 245) = 'S') THEN
    p_buscar_limites_bb(nr_atendimento_p, cd_setor_atendimento_p, cd_pessoa_fisica_p, null, clob_limits);
    json_limits := philips_json(clob_limits);
    dbms_lob.createtemporary(envio_integracao_bb, TRUE);
    json_limits.to_clob(envio_integracao_bb);
    
    SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Patient_Limits',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
  END IF;
END;
/