CREATE OR REPLACE PROCEDURE UPDATE_GQA_PENDENCIA_REGRA (
    NR_SEQ_PLANO_P protocolo_integrado_evento.NR_SEQ_PLANO%TYPE,
    NR_SEQ_EVENTO_P protocolo_integrado_evento.NR_SEQUENCIA%TYPE,
    NR_SEQ_PROT_INT_P protocolo_integrado.NR_SEQUENCIA%TYPE
) AS
    QT_SHARED_RULES_W NUMBER := 0;
    QT_PROT_PACIENTE_W NUMBER := 0;
BEGIN

    SELECT
        COUNT(1)
    INTO QT_PROT_PACIENTE_W
    FROM protocolo_int_paciente a 
    WHERE
        a.nr_sequencia_protocolo = NR_SEQ_PROT_INT_P;
    
    IF (QT_PROT_PACIENTE_W = 0) THEN
    
        SELECT 
            COUNT(1) 
        INTO QT_SHARED_RULES_W
        FROM PROTOCOLO_INTEGRADO_EVENTO a
        INNER JOIN PROTOCOLO_INTEGRADO_ETAPA b ON
            a.nr_seq_etapa = b.NR_SEQUENCIA
        WHERE 
            a.nr_seq_plano = NR_SEQ_PLANO_P AND
            a.nr_sequencia <> NR_SEQ_EVENTO_P;
        
        IF (QT_SHARED_RULES_W = 0 and NR_SEQ_PLANO_P is not null) then
    
            UPDATE GQA_PENDENCIA_REGRA
            SET ie_situacao = 'I'
            WHERE 
                nr_sequencia = NR_SEQ_PLANO_P AND 
                ie_situacao = 'A';
                
            COMMIT; 
        END IF;
    END IF;
END UPDATE_GQA_PENDENCIA_REGRA;
/