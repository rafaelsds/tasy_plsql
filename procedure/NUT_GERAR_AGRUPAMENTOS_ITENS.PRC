create or replace
procedure Nut_gerar_agrupamentos_itens (nr_seq_prod_cond_p	Number,
					nm_usuario_p		Varchar2) is 
			
qt_dose_real_w		Number(15,3);
nr_sequencia_w		Number(10);
ie_agrupador_w		Number(5);
			
Cursor C01 is
	select	distinct a.qt_dose
	from	nut_producao_conduta_item a,
		nut_atend_serv_dia_dieta b
	where	a.nr_seq_dieta = b.nr_sequencia 
	and	a.nr_seq_prod_cond = nr_seq_prod_cond_p;

Cursor C02 is
	select 	a.nr_sequencia
	from	nut_producao_conduta_item a,
		nut_atend_serv_dia_dieta b
	where	a.nr_seq_dieta = b.nr_sequencia 
	and	a.nr_seq_prod_cond = nr_seq_prod_cond_p
	and	a.qt_dose = qt_dose_real_w
	order by a.nr_atendimento;

begin

if	(nvl(nr_seq_prod_cond_p,0) > 0) then
	
	ie_agrupador_w := 1;
	open C01;
	loop
	fetch C01 into	
		qt_dose_real_w;
	exit when C01%notfound;
		begin
		open C02;
		loop
		fetch C02 into	
			nr_sequencia_w;
		exit when C02%notfound;
			begin
			
			update	nut_producao_conduta_item
			set	ie_agrupador = ie_agrupador_w
			where	nr_sequencia = nr_sequencia_w;
			
			end;
		end loop;
		close C02;
		
		ie_agrupador_w := ie_agrupador_w +1;
		
		end;
	end loop;
	close C01;


end if;


commit;

end Nut_gerar_agrupamentos_itens;
/