CREATE OR REPLACE
PROCEDURE Gerar_comunic_script (ds_titulo_p		in	varchar2,
                              ds_comunicado_p   in	varchar2,
                              nm_usuario_p		in	varchar2,
                              nm_usuario_destino_p in	varchar2) is

ds_Comunicado_w varchar2(32500);
		
BEGIN 

if	(length(ds_Comunicado_p) > 32000) then
	wheb_mensagem_pck.exibir_mensagem_abort(189142);
end if;

ds_Comunicado_w := ds_Comunicado_p;

if (instr(ds_Comunicado_w,'{\rtf') = 0) then	
	ds_Comunicado_w := wheb_rtf_pck.get_cabecalho ||chr(13)||chr(10)|| replace(ds_comunicado_w, chr(13)||chr(10), chr(13)||chr(10)|| wheb_rtf_pck.get_quebra_linha) ||chr(13)||chr(10)|| wheb_rtf_pck.get_rodape;	
end if;

insert into corp.comunic_interna@whebl01_dbcorp(  dt_comunicado, 
                              ds_titulo,
                              ds_comunicado,
                              nm_usuario,
                              dt_atualizacao,
                              nr_sequencia,
                              ie_gerencial,
                              nm_usuario_destino,
                              dt_liberacao)
                    values (  sysdate,
                              ds_titulo_p,
                              ds_comunicado_p,
                              nm_usuario_p,
                              sysdate,
                              corp.comunic_interna_seq.nextval@whebl01_dbcorp,
                              'N',
                              nm_usuario_destino_p,
                              sysdate);

commit;

END Gerar_comunic_script;
/