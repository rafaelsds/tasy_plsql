CREATE OR REPLACE
PROCEDURE Gerar_Dados_Pos_Checkup(	nr_atendimento_p		Number,
					dt_referencia_p			date,
					nm_usuario_p			Varchar2) is

nr_sequencia_w	number(15,0);
qt_w		number(15,0);

begin

select	count(*)
into	qt_w
from	checkup_dado_estat
where	nr_atendimento	= nr_atendimento_p
and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_referencia) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_referencia_p);
   
if	(nvl(qt_w,0) > 0) then
	-- Nao e possivel inserir dois pos checkup para um mesmo dia!
	Wheb_mensagem_pck.exibir_mensagem_abort(264456);
end if;

insert into checkup_dado_estat(
	nr_sequencia,
	nr_atendimento,
	dt_atualizacao,
	nm_usuario,
	nr_seq_tipo_dado,
	cd_pessoa_fisica,
	dt_registro,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_apres,
	ie_pos_checkup,
	qt_dado,
	ds_dado,
	dt_referencia)
select	checkup_dado_estat_seq.nextval,
	nr_atendimento_p,
	sysdate,
	nm_usuario_p,
	a.nr_seq_tipo_dado,
	obter_Dados_Usuario_Opcao(nm_usuario_p,'C'),
	dt_referencia_p,
	sysdate,
	nm_usuario_p,
	a.nr_seq_apres,
	'S',
	null qt_dado,
	null ds_dado,
	dt_referencia_p
from	checkup_dado_estat a
where	nr_atendimento	= nr_atendimento_p;

commit;

end Gerar_Dados_Pos_Checkup;
/