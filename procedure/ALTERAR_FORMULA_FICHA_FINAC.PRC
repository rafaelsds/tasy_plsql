create or replace
procedure	alterar_formula_ficha_finac(nr_sequencia_p	number,
					ds_formula_p  varchar2,
					nm_usuario_p varchar2) is 
			
qt_formula_w	number(10);
			
begin


update 	ficha_financ_formula
set 	ds_formula = ds_formula_p, 
	nm_usuario = nm_usuario_p, 
	dt_atualizacao = sysdate
where	nr_sequencia = nr_sequencia_p;



end alterar_formula_ficha_finac;
/

