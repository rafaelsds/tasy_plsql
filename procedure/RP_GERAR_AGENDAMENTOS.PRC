create or replace procedure rp_gerar_agendamentos( nr_seq_modelo_p		varchar2,
			cd_pessoa_fisica_p		varchar2,
			dt_inicial_p		date,
			dt_final_p			date,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number,
			ds_erro_p			out varchar2,
			ie_job_p			varchar2  default 'N') is 

cd_agenda_w			number(10,0);
cd_medico_exec_w		varchar2(10);
dt_horario_w			date;
ie_dia_semana_w			number(1,0);
dt_atual_w			date;
nr_sequencia_w			varchar2(10);
nr_seq_agenda_w			number(10,0);
cd_perfil_w			number(5,0);
nm_pessoa_fisica_w		varchar2(100);
ds_agenda_w			varchar2(100);
dt_nascimento_w			date;
qt_idade_w			number(3,0);
ds_enter_w			varchar2(20)	:= chr(10)||chr(13);
nr_seq_agend_item_w		number(10,0);
ie_classif_agenda_w		varchar2(5);
nr_seq_pac_reab_w		number(10,0);
ie_status_agenda_w		varchar2(2);
ds_motivo_status_w		varchar2(255);
qt_agendado_w			number(10,0);
ie_feriado_w			varchar2(1);
nr_seq_status_pac_w		number(10,0);
nr_seq_status_agendamento_w	number(10,0);
ie_status_agenda_prof_aus_w	varchar2(3);
nr_seq_agendado_w		number(10,0);
cd_medico_agendado_w		varchar2(10);

ds_motivo_licenca_w		varchar2(255);
ds_motivo_licenca_prof_w 		varchar2(255);
nr_seq_licenca_w			number(10,0);
cd_classificacao_w			varchar2(05)	:= 'E';
nr_seq_hora_w			NUMBER(0010,0)    := 0;
ds_observacao_w			varchar2(255);

ie_data_trat_w			varchar2(1);
nr_seq_ausencia_prof_w		number(10,0);
cd_motivo_cancel_prof_aus_w	varchar2(3);
cd_motivo_cancelamento_w		varchar2(3) := '';
nr_seq_status_trat_w		number(10,0);
nr_seq_tratamento_w		number(10,0);
nr_seq_pac_reab_bloqueio_w		number(10,0);
nr_seq_status_reab_w		number(10,0);
ie_status_agenda_susp_w 		varchar2(3);

cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
cd_plano_w			varchar2(10);
cd_tipo_acomodacao_w		number(4);
dt_validade_w			date;
cd_usuario_convenio_w		varchar2(30);
nr_doc_convenio_w		varchar2(20);
nr_seq_room_w			rp_pac_modelo_agendamento.nr_seq_room%type;

ie_atualiza_conv_atend_w		varchar2(1);
ie_justificada_w			varchar2(1);
ie_dia_semana_int_w		number(1,0);
ie_continua_w			varchar2(1);
ie_count_dia_semana_w		number(1);
ie_nao_permite_iguais_w		 varchar2(1);
ie_utiliza_regra_turno_w		varchar2(1);
ie_manter_medico_exec_w		varchar2(1);

ie_consistencia_w	VARCHAR2(255);
ie_agenda_w		VARCHAR2(1);

--verifica numero maximo de registro da reabilitacao
permite_adicionar_agenda_w		varchar2(1);
cd_substituto_w			varchar2(15);

Cursor C01 is
	select	a.cd_agenda,		
		nvl(a.cd_medico_exec,b.cd_profissional_padrao),
		a.ie_dia_semana,
		a.dt_horario,
		a.nr_sequencia,
		ie_classif_agenda
	from	rp_pac_modelo_agend_item a,
		rp_item_modelo_agenda b
	where	a.nr_seq_item_modelo = b.nr_sequencia
	and	a.nr_seq_modelo_pac  = nr_seq_modelo_p
	and	a.dt_fim_tratamento is null
	and	nvl(b.ie_situacao,'A') = 'A'	
	order by cd_agenda;
	
Cursor C02 is
	select	nr_sequencia,
		substr(rp_obter_desc_mot_licenca(nr_seq_motivo_licenca),1,255),
		substr(ds_observacao,1,255),
		ie_justificada
	from	rp_licenca
	where	nr_seq_pac_reab = nr_seq_pac_reab_w
	and	dt_atual_w between dt_periodo_inicial and dt_periodo_final
	and	nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
	and	dt_liberacao is not null
	order by dt_periodo_inicial;
	

Cursor C03 is
	select 	 nr_sequencia,
		 substr(rp_obter_desc_mot_licenca(a.nr_seq_motivo_licenca),1,255),
		 cd_substituto
	from   	 rp_licenca_profissional a
	where  	 a.cd_pessoa_fisica = cd_medico_exec_w
	and	 dt_atual_w between dt_periodo_inicial and dt_periodo_final
	and	 a.dt_liberacao is not null;

Cursor C04 is
	select 	nr_sequencia,
		nr_seq_status
	from   	rp_tratamento
	where	nr_seq_pac_reav = nr_seq_pac_reab_w
	and	dt_fim_tratamento is null;
	
begin


ds_erro_p	:= 	'';
dt_atual_w	:=	trunc(dt_inicial_p,'dd');
ie_status_agenda_susp_w := null;
obter_param_usuario(866,15,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_atualiza_conv_atend_w);
Obter_Param_Usuario(9091,48,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_nao_permite_iguais_w);	
Obter_Param_Usuario(9091,69,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_utiliza_regra_turno_w);
Obter_Param_Usuario(9091,79,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_manter_medico_exec_w);


if	(ie_job_p = 'S') then
	select	max(nr_seq_pac_reab_bloqueio)
	into	nr_seq_pac_reab_bloqueio_w
	from	rp_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;

	select	max(nr_seq_status)
	into	nr_seq_status_reab_w
	from	rp_paciente_reabilitacao
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;

	if	(nr_seq_pac_reab_bloqueio_w = nr_seq_status_reab_w) then
		select	max(ie_status_agenda_susp)
		into	ie_status_agenda_susp_w
		from	rp_parametros
		where	cd_estabelecimento = cd_estabelecimento_p;
end if;	
	
	
end if;

if	(ie_atualiza_conv_atend_w = 'S') then
	define_convenio_atend_agenda(cd_pessoa_fisica_p,nm_usuario_p,cd_convenio_w,cd_categoria_w,dt_validade_w,cd_usuario_convenio_w,nr_doc_convenio_w,cd_tipo_acomodacao_w,cd_plano_w);
end if;	

select	max(a.nr_seq_pac_reab),
	max(b.nr_seq_status),
	max(a.cd_convenio),
	max(a.cd_categoria),
	max(a.cd_plano),
	max(a.cd_tipo_acomodacao),
	max(a.nr_seq_room)
into	nr_seq_pac_reab_w,
	nr_seq_status_pac_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w,
	cd_tipo_acomodacao_w,
	nr_seq_room_w
from	rp_pac_modelo_agendamento a,
	rp_paciente_reabilitacao b
where	a.nr_seq_pac_reab = b.nr_sequencia
and	a.nr_sequencia = nr_seq_modelo_p;

select	max(nr_seq_pac_reab_agenda),
	max(ie_status_agenda_prof_aus),
	max(cd_motivo_cancelamento)
into	nr_seq_status_agendamento_w,
	ie_status_agenda_prof_aus_w,
	cd_motivo_cancel_prof_aus_w
from	rp_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

while	(dt_atual_w <= dt_final_p) loop
	begin
		
	open C01;
	loop
	fetch C01 into	
		cd_agenda_w,
		cd_medico_exec_w,
		ie_dia_semana_int_w,
		dt_horario_w,
		nr_seq_agend_item_w,
		ie_classif_agenda_w;
	exit when C01%notfound;
		begin
		
		select RP_OBTER_DISP_AGENDA(cd_agenda_w,cd_pessoa_fisica_p)
		into permite_adicionar_agenda_w	
		from dual;
		

		if (permite_adicionar_agenda_w = 'S') then
			
			ie_continua_w := 'S';
			ie_count_dia_semana_w := 1;
			while 	(ie_continua_w = 'S') loop 
				begin
				ie_count_dia_semana_w := ie_count_dia_semana_w + 1;
				ie_dia_semana_w := ie_dia_semana_int_w;
				if	(ie_dia_semana_int_w = 9) then			
					ie_dia_semana_w := ie_count_dia_semana_w;
				end if;
				
				open C02;
				loop
				fetch C02 into	
					nr_seq_licenca_w,
					ds_motivo_licenca_w,
					ds_observacao_w,
					ie_justificada_w;
				exit when C02%notfound;
				end loop;
				close C02;
				
				if	(nr_seq_licenca_w > 0) then
					if	(ie_justificada_w = 'J') then
						ie_status_agenda_w	:= 'F';
					elsif	(ie_justificada_w = 'N') then
						ie_status_agenda_w	:= 'I';
					end if;
					ds_motivo_status_w	:= ds_motivo_licenca_w;
				else
					ie_status_agenda_w		:= 'N';
					ds_motivo_status_w		:= '';
					cd_motivo_cancelamento_w	:= '';
					ds_observacao_w := '';
				end if;
				
				if	(ie_status_agenda_prof_aus_w is not null) then
					
					nr_seq_ausencia_prof_w	:= 0;
					cd_substituto_w		:= '';
					open C03;
					loop
					fetch C03 into	
						nr_seq_ausencia_prof_w,
						ds_motivo_licenca_prof_w,
						cd_substituto_w;
					exit when C03%notfound;
					end loop;
					close C03;
										
					if	(nr_seq_ausencia_prof_w > 0) and
						(cd_substituto_w is null) then
						ie_status_agenda_w	:= ie_status_agenda_prof_aus_w;
						ds_motivo_status_w	:= ds_motivo_licenca_prof_w;
						if	(ie_status_agenda_w	= 'C') then
							cd_motivo_cancelamento_w	:= cd_motivo_cancel_prof_aus_w;
						else
							cd_motivo_cancelamento_w	:= '';
						end if;					
					elsif	(ie_status_agenda_w not in ('F','I')) then
						ie_status_agenda_w	:= 'N';
						ds_motivo_status_w	:= '';
						cd_motivo_cancelamento_w := '';
					end if;		
				
				end if;
				
				select	substr(obter_nome_pf(cd_pessoa_fisica_p),1,100),
					substr(obter_nome_agenda(cd_agenda_w),1,100),				
					obter_idade_pf(cd_pessoa_fisica_p,sysdate,'A')
				into	nm_pessoa_fisica_w,
					ds_agenda_w,				
					qt_idade_w
				from	dual;
				
				select 	max(dt_nascimento)
				into 	dt_nascimento_w
				from 	pessoa_fisica
				where 	cd_pessoa_fisica = cd_pessoa_fisica_p;					
							
				
				/* Consistencia se o paciente ja possui agendamento no dia */
				select	count(*)
				into	qt_agendado_w
				from	agenda_consulta
				where	trunc(dt_agenda,'dd')   = trunc(dt_atual_w)
				and	cd_pessoa_fisica	= cd_pessoa_fisica_p
				and	ie_status_agenda 	<> 'C'
				and	((cd_agenda 		= cd_agenda_w) or (ie_nao_permite_iguais_w = 'S'))
				and	to_char(dt_agenda,'hh24:mi') = to_char(dt_horario_w,'hh24:mi')
				and	nr_seq_rp_mod_item is not null;
				
				if	(qt_agendado_w > 0) then
					/*Alteracao profissional*/
				
					select	max(nr_sequencia),
						max(cd_medico)
					into	nr_seq_agendado_w,
						cd_medico_agendado_w
					from	agenda_consulta
					where	trunc(dt_agenda,'dd')   = trunc(dt_atual_w)
					and	cd_pessoa_fisica	= cd_pessoa_fisica_p
					and	ie_status_agenda 	<> 'C'
					and	to_char(dt_agenda,'hh24:mi') = to_char(dt_horario_w,'hh24:mi')
					and	nr_seq_rp_mod_item 	= nr_seq_agend_item_w;
					
					if	(nvl(cd_substituto_w,cd_medico_exec_w) <> cd_medico_agendado_w) and
						(ie_manter_medico_exec_w <> 'S') then
						update	agenda_consulta
						set	cd_medico 	= nvl(cd_substituto_w,cd_medico_exec_w)
						where	nr_sequencia 	= nr_seq_agendado_w;
					end if;			
					
				end if;
				
				
				/* Verificando se o dia e feriado */
				select	decode(count(*),0,'N','S')
				into	ie_feriado_w
				from 	feriado a,
					agenda b
				where 	a.cd_estabelecimento = obter_estab_agenda(cd_agenda_w)
				and	a.dt_feriado = trunc(dt_atual_w)
				and 	b.cd_agenda = cd_agenda_w
				and	nvl(b.ie_feriado,'S') = 'N';
							
				if	(obter_cod_dia_semana(dt_atual_w) = ie_dia_semana_w) and 
					(qt_agendado_w = 0) then
					begin												
			
					if	((ie_feriado_w <> 'S') or (obter_se_agenda_feriado(cd_agenda_w) = 'S')) then
						begin
						
						select	decode(count(*),0,'N','S')
						into	ie_data_trat_w
						from	rp_tratamento 
						where	nr_seq_pac_reav = nr_seq_pac_reab_w
						and	dt_inicio_tratamento is not null;
						
						if	(ie_data_trat_w = 'N') then
				
							update	rp_tratamento
							set	dt_inicio_tratamento 	= dt_atual_w
							where	nr_seq_pac_reav 	= nr_seq_pac_reab_w;

						end if;						
						
						Gerar_Horario_Agenda_Servico(cd_estabelecimento_p,cd_agenda_w,dt_atual_w,nm_usuario_p);
					
						select	nvl(max(nr_sequencia),0)
						into	nr_sequencia_w
						from	agenda_consulta
						where	cd_agenda = cd_agenda_w
						and	trunc(dt_agenda,'dd') = dt_atual_w
						and	to_char(dt_agenda,'hh24:mi') = to_char(dt_horario_w,'hh24:mi')
						and	nvl(ie_classif_agenda,'X') = nvl(nvl(ie_classif_agenda_w,ie_classif_agenda),'X')
						and	ie_status_agenda = 'L';
						
						if(PKG_I18N.get_user_locale = 'ja_JP') then								
														
							consistir_classif_agecon(cd_estabelecimento_p,
								cd_pessoa_fisica_p,
								to_date(to_char(dt_atual_w,'dd/mm/yyyy')||' '||to_char(dt_horario_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
								cd_agenda_w,
								cd_convenio_w,
								null,
								null,
								null,
								ie_classif_agenda_w,
								nr_sequencia_w,
								ie_consistencia_w,			
								ie_agenda_w,
                                dt_inicial_p,
                                dt_final_p,
                                cd_medico_exec_w);
								
								if	(ie_agenda_w = 'N') or (ie_consistencia_w is not null) then								
									ds_erro_p := ie_consistencia_w;
								end if;

						end if;
					
						if	(nr_sequencia_w > 0 and nvl(ie_agenda_w,'S') <> 'N') then
							begin
							
							if (ie_manter_medico_exec_w = 'S') then
								begin
								update	agenda_consulta
								set	cd_pessoa_fisica	= cd_pessoa_fisica_p,
									dt_atualizacao		= sysdate,
									nm_usuario		= nm_usuario_p,
									nr_seq_rp_mod_item 	= nr_seq_agend_item_w,
									qt_idade_pac		= qt_idade_w,
									dt_nascimento_pac	= dt_nascimento_w,
									ie_status_agenda	= decode(ie_status_agenda_susp_w,null,ie_status_agenda_w,ie_status_agenda_susp_w),
									ds_motivo_status	= ds_motivo_status_w,
									cd_motivo_cancelamento	= cd_motivo_cancelamento_w,
									cd_convenio		= nvl(cd_convenio_w,cd_convenio),
									cd_categoria		= nvl(cd_categoria_w,cd_categoria),
									cd_plano		= nvl(cd_plano_w,cd_plano),
									cd_tipo_acomodacao	= nvl(cd_tipo_acomodacao_w,cd_tipo_acomodacao),
									cd_usuario_convenio	= nvl(cd_usuario_convenio_w,cd_usuario_convenio),
									DS_OBSERVACAO = nvl(ds_observacao_w, DS_OBSERVACAO),
									dt_validade_carteira    = nvl(dt_validade_w,dt_validade_carteira),
									nr_seq_sala		= nr_seq_room_w
								where	nr_sequencia		= nr_sequencia_w;
								end;
							else
								begin
								update	agenda_consulta
								set	cd_pessoa_fisica	= cd_pessoa_fisica_p,
									cd_medico		= nvl(cd_substituto_w,cd_medico_exec_w),
									dt_atualizacao		= sysdate,
									nm_usuario		= nm_usuario_p,
									nr_seq_rp_mod_item 	= nr_seq_agend_item_w,
									qt_idade_pac		= qt_idade_w,
									dt_nascimento_pac	= dt_nascimento_w,
									ie_status_agenda	= decode(ie_status_agenda_susp_w,null,ie_status_agenda_w,ie_status_agenda_susp_w),
									ds_motivo_status	= ds_motivo_status_w,
									cd_motivo_cancelamento	= cd_motivo_cancelamento_w,
									cd_convenio		= nvl(cd_convenio_w,cd_convenio),
									cd_categoria		= nvl(cd_categoria_w,cd_categoria),
									cd_plano		= nvl(cd_plano_w,cd_plano),
									cd_tipo_acomodacao	= nvl(cd_tipo_acomodacao_w,cd_tipo_acomodacao),
									cd_usuario_convenio	= nvl(cd_usuario_convenio_w,cd_usuario_convenio),
									DS_OBSERVACAO = nvl(ds_observacao_w, DS_OBSERVACAO),
									dt_validade_carteira    = nvl(dt_validade_w,dt_validade_carteira),
									nr_seq_sala		= nr_seq_room_w
								where	nr_sequencia		= nr_sequencia_w;
								end;

							end if;
								RP_GERAR_AGENDA_PROCEDIMENTO(
										nr_seq_agend_consulta_p => nr_sequencia_w,
										nr_seq_rp_pac_agend_p => nr_seq_agend_item_w,
										ie_agend_ind_p => 'N'
									);
							end;
						elsif	(ie_utiliza_regra_turno_w = 'N' and nvl(ie_agenda_w,'S') <> 'N') then
							begin
							
							select nvl(max(cd_classificacao), 'E')
							into	cd_classificacao_w
							from 	agenda_turno_classif;
							
							select	agenda_consulta_seq.nextval
							into	nr_seq_agenda_w
							from	dual;
							
							select	nvl(max(nr_seq_hora),0) + 1
							into	nr_seq_hora_w
							from	agenda_consulta
							where	cd_agenda	= cd_agenda_w
							and	dt_agenda	= to_date(to_char(dt_atual_w,'dd/mm/yyyy')||' '||to_char(dt_horario_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
							
							insert into agenda_consulta(
									nr_sequencia,
									cd_agenda,
									dt_agenda,
									nr_minuto_duracao,
									cd_pessoa_fisica,								
									ie_status_agenda,
									ie_classif_agenda,
									dt_atualizacao,
									nm_usuario,
									cd_medico,
									ie_encaixe,
									dt_agendamento,
									nm_usuario_origem,
									nr_seq_rp_mod_item,
									ds_motivo_status,
									nr_seq_hora,
									nm_paciente,
									cd_motivo_cancelamento,
									cd_convenio,	
									cd_categoria,
									cd_plano,	
									cd_tipo_acomodacao,
									cd_usuario_convenio,
									DS_OBSERVACAO,
									dt_validade_carteira,
									nr_seq_sala)
								values (
									nr_seq_agenda_w,
									cd_agenda_w,
									to_date(to_char(dt_atual_w,'dd/mm/yyyy')||' '||to_char(dt_horario_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),
									0,
									cd_pessoa_fisica_p,								
									decode(ie_status_agenda_susp_w,null,ie_status_agenda_w,ie_status_agenda_susp_w),
									nvl(ie_classif_agenda_w,cd_classificacao_w),
									sysdate,
									nm_usuario_p,
									nvl(cd_substituto_w,cd_medico_exec_w),
									'S',
									sysdate,
									nm_usuario_p,
									nr_seq_agend_item_w,
									ds_motivo_status_w,
									nr_seq_hora_w,
									substr(obter_nome_pf(cd_pessoa_fisica_p),1,100),
									cd_motivo_cancelamento_w,
									cd_convenio_w,							
									cd_categoria_w,							
									cd_plano_w,	
									cd_tipo_acomodacao_w,
									cd_usuario_convenio_w,
									ds_observacao_w,
									dt_validade_w,
									nr_seq_room_w);
								RP_GERAR_AGENDA_PROCEDIMENTO(
									nr_seq_agend_consulta_p => nr_seq_agenda_w,
									nr_seq_rp_pac_agend_p => nr_seq_agend_item_w,
									ie_agend_ind_p => 'N'
								);
							
							if	(dt_nascimento_w is not null) then
								update	agenda_consulta
								set	dt_nascimento_pac = dt_nascimento_w,
									qt_idade_pac = obter_idade(dt_nascimento_w,sysdate,'A')
								where	nr_sequencia = nr_seq_agenda_w;									
							end if;									
							
							end;
						end if;
						
						if	(nr_seq_status_agendamento_w is not null) and
							(nr_seq_status_pac_w <> nr_seq_status_agendamento_w) and
							(ie_status_agenda_susp_w is null) then
							
							update	rp_paciente_reabilitacao
							set	nr_seq_status	= nr_seq_status_agendamento_w
							where	nr_sequencia	= nr_seq_pac_reab_w;
											
						end if;
										
						if	(nr_seq_status_agendamento_w is not null) and (ie_status_agenda_susp_w is null) then
							open C04;
							loop
							fetch C04 into	
								nr_seq_tratamento_w,
								nr_seq_status_trat_w;
							exit when C04%notfound;
								begin
								if	((nr_seq_status_trat_w is null) or (nr_seq_status_trat_w <> nr_seq_status_agendamento_w)) then
									update	rp_tratamento
									set	nr_seq_status	= nr_seq_status_agendamento_w
									where	nr_sequencia	= nr_seq_tratamento_w;
								end if;
								
								end;
							end loop;
							close C04;
						end if;
						
						end;					
					end if;
													
					nr_seq_licenca_w   := 0;
					nr_seq_ausencia_prof_w	:= 0;
				end;
				end if;
				
				if	(ie_count_dia_semana_w > 7) then
					ie_continua_w := 'N';
				end if;
				
				if	(ie_dia_semana_int_w <> 9) or
					((ie_dia_semana_int_w = 9) and
					(ie_dia_semana_w = 6))then
					ie_continua_w := 'N';
				end if;				
				end;
			end loop;
		else
		   ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(281540,null);
		end if;
		end;
	end loop;
	close C01;
	
	nr_seq_licenca_w	:= 0;
	nr_seq_ausencia_prof_w	:= 0;
	dt_atual_w		:= dt_atual_w + 1;
	end;
end loop;


commit;

end rp_gerar_agendamentos;
/
