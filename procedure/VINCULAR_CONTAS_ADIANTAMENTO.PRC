create or replace 
procedure VINCULAR_CONTAS_ADIANTAMENTO
		(nr_adiantamento_p	in	number,
		ds_lista_contas_p	in	varchar2,
		nm_usuario_p		in	varchar2,
		ds_erro_p		out	varchar2) is

nr_interno_conta_w	number(10);
nr_sequencia_w		number(10);
vl_contas_w		number(15,2);
vl_adiantamento_w	number(15,2);
cont_w			number(5);
vl_saldo_w		adiantamento.vl_saldo%type;

cursor c01 is
select	nr_interno_conta
from	conta_paciente
where	' ' || ds_lista_contas_p || ' ' like '% ' || nr_interno_conta || ' %'
and	ds_lista_contas_p is not null;

begin
ds_erro_p	:= null;

if	(ds_lista_contas_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265609,'');
	--Nenhuma conta foi selecionada!
end if;

if	(ds_lista_contas_p is not null) then

	open c01;
	loop
	fetch c01 into
		nr_interno_conta_w;
	exit when c01%notfound;

		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_w
		from	conta_paciente_adiant
		where	nr_interno_conta	= nr_interno_conta_w
		and	nr_adiantamento		= nr_adiantamento_p;
		
		/*OS 1496628, cfme analise da OS*/	
		select	max(vl_saldo)
		into	vl_saldo_w
		from	adiantamento
		where	nr_adiantamento = nr_adiantamento_p;

		insert	into conta_paciente_adiant(nr_interno_conta,
						   nr_adiantamento,
						   vl_adiantamento,
						   dt_atualizacao,
						   nm_usuario,
						   nr_sequencia,
						   ie_tipo_adiant)
					values	  (nr_interno_conta_w,
						   nr_adiantamento_p,
						   nvl(vl_saldo_w,0), /*OS 1496628, cfme analise da OS*/
						   sysdate,
						   nm_usuario_p,
						   nr_sequencia_w,
						   null);

	end loop;
	close c01;

	select	count(*)
	into	cont_w
	from	conta_paciente_adiant
	where	obter_titulo_conta(nr_interno_conta) is null
	and	nr_adiantamento	= nr_adiantamento_p;

	if	(cont_w = 0) then

		select	nvl(sum(b.vl_conta),0)
		into	vl_contas_w
		from	conta_paciente b,
			conta_paciente_adiant a
		where	a.nr_interno_conta	= b.nr_interno_conta
		and	a.nr_adiantamento	= nr_adiantamento_p;

		select	nvl(vl_adiantamento,0)
		into	vl_adiantamento_w
		from	adiantamento
		where	nr_adiantamento	= nr_adiantamento_p;

		if	(vl_contas_w > vl_adiantamento_w) then
			ds_erro_p	:= wheb_mensagem_pck.get_texto(280102);
		end if;

	end if;
end if;

if	(ds_erro_p is null) then
	commit;
else
	rollback;
end if;

end VINCULAR_CONTAS_ADIANTAMENTO;
/