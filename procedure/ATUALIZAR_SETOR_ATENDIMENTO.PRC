create or replace
procedure atualizar_setor_atendimento(
			nr_prescricao_p		number,
			cd_setor_atendimento_p	number,
			nm_usuario_p		Varchar2) is 

begin

if	(nr_prescricao_p is not null and 
	cd_setor_atendimento_p is not null) then
	update	prescr_medica
	set	cd_setor_atendimento = cd_setor_atendimento_p
	where	nr_prescricao = nr_prescricao_p;
end if;

commit;

end atualizar_setor_atendimento;
/