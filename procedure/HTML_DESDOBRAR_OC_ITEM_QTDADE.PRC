create or replace
procedure HTML_DESDOBRAR_OC_ITEM_QTDADE(nr_ordem_compra_p	number,
					nr_item_oci_p		number,
					nm_usuario_p		varchar2) is
qt_material_w		number(13,4);
ds_erro_w		varchar2(2000);
ie_update_insert_w	varchar2(1);

cursor c01 is
select	qt_material
from	w_ordem_compra_item_desdob
where	nm_usuario = nm_usuario_p
and	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p;

begin

select	nvl(sum(qt_material),0)
into	qt_material_w
from	w_ordem_compra_item_desdob
where	nm_usuario = nm_usuario_p
and	nr_ordem_compra = nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_p;

CONSISTIR_DESDOBR_ITEM_OC(nr_ordem_compra_p, nr_item_oci_p, qt_material_w, ds_erro_w);

if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(798669,'DS_ERRO='||ds_erro_w);
end if;

ie_update_insert_w := 'U';

open C01;
loop
fetch C01 into	
	qt_material_w;
exit when C01%notfound;
	begin
	
	DESDOBRAR_QT_ITEM_OCI(nr_ordem_compra_p, nr_item_oci_p, qt_material_w, ie_update_insert_w, nm_usuario_p);
	ie_update_insert_w := 'I';	
	
	end;
end loop;
close C01;

commit;

end HTML_DESDOBRAR_OC_ITEM_QTDADE;
/