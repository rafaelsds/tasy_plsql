create or replace
procedure proj_gerar_escore_cliente
			(	nr_seq_item_p		Number,
				nr_seq_proj_p		Number,
				qt_escore_p		Number,
				nm_usuario_p		Varchar2) is 

qt_ponto_w			Number(15,0);
qt_peso_w			Number(15,0);
qt_escore_real_w			Number(15,0);
pr_escore_real_w			Number(15,0);

begin

select	sum(b.qt_peso) qt_peso,
	sum(c.qt_peso) qt_ponto
into	qt_peso_w,
	qt_ponto_w
from	proj_aval_opcao c,
	proj_avaliacao b,
	proj_avaliacao_quesito a
where	a.nr_seq_escore_item	= nr_seq_item_p
and	a.nr_seq_avaliacao		= b.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_avaliacao
and	c.nr_sequencia		= a.nr_seq_opcao;


if	(qt_peso_w >0)  then
	qt_escore_real_w		:= qt_escore_p * (qt_ponto_w / qt_peso_w);
	pr_escore_real_w		:= dividir(qt_escore_real_w * 100, qt_escore_p);
	update	proj_escore_item
	set	qt_nota_maxima	= qt_peso_w,
		qt_nota		= qt_ponto_w,
		qt_escore_real	= qt_escore_real_w,
		pr_escore_real	= pr_escore_real_w
	where	nr_sequencia	= nr_seq_item_p;
end if;

/*Raise_application_error(-20011,'proj_gerar_escore_cliente#@#@');*/

end proj_gerar_escore_cliente;
/
