create or replace
procedure pls_obter_dados_regra_auditor(
		nr_seq_auditoria_p				number,
		ie_tipo_item_p				varchar2,
		nr_seq_item_p				number,
		ie_tipo_despesa_p				number,
		ie_permite_aparecer_itens_p			varchar2,
		ie_auditar_sem_usu_exec_p			varchar2,
		ie_status_solicitacao_p			varchar2,
		nm_usuario_p				varchar2,
		ie_auditor_grupo_p			out	varchar2,
		ie_auditor_regra_despesa_p		out	varchar2,
		ie_auditor_grupo_item_p		out	varchar2,
		ie_grupo_assumido_p		out	varchar2) is 

ie_auditor_grupo_w		varchar2(4);
ie_auditor_regra_despesa_w	varchar2(1);
ie_auditor_grupo_item_w	varchar2(1);
ie_grupo_assumido_w	varchar2(1);

begin
if	(nr_seq_auditoria_p is not null) and
	(ie_permite_aparecer_itens_p is not null) and
	(ie_auditar_sem_usu_exec_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	select	nvl(pls_obter_liberacao_auditor(nr_seq_auditoria_p,nm_usuario_p), 'N')
	into	ie_auditor_grupo_w
	from	dual;
	
	select	nvl(pls_obter_regra_despesa_aud(nr_seq_auditoria_p,ie_tipo_item_p,ie_tipo_despesa_p),'N')
	into	ie_auditor_regra_despesa_w
	from	dual;	
	
	if	(ie_permite_aparecer_itens_p = 'S') and
		(nr_seq_item_p is not null) and
		(ie_status_solicitacao_p = 'P') then
		begin
		select	nvl(pls_obter_se_item_auditor(nr_seq_auditoria_p,nr_seq_item_p,ie_tipo_item_p,nm_usuario_p),'N')
		into	ie_auditor_grupo_item_w
		from	dual;
		end;
	else	
		ie_auditor_grupo_item_w := 'S';
	end if;

	if	(ie_auditar_sem_usu_exec_p = 'S') and
		(ie_auditor_grupo_w = 'S') then
		begin
		select	nvl(pls_obter_se_grupo_assumido(nr_seq_auditoria_p,nm_usuario_p),'N')
		into	ie_grupo_assumido_w
		from	dual;
		end;
	else
		begin
		ie_grupo_assumido_w := 'S';
		end;
	end if;
	
	ie_auditor_grupo_p		:= ie_auditor_grupo_w;
	ie_auditor_regra_despesa_p	:= ie_auditor_regra_despesa_w;
	ie_auditor_grupo_item_p	:= ie_auditor_grupo_item_w;
	ie_grupo_assumido_p	:= ie_grupo_assumido_w;
	
	end;
end if;

end pls_obter_dados_regra_auditor;
/