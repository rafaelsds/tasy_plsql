create or replace procedure gerar_lanc_auto_taxa_diaria(nr_atendimento_p    atendimento_paciente.nr_atendimento%type,
                                                        qt_dias_pagamento_p pessoa_fisica_taxa.qt_dias_pagamento%type) is

  nm_usuario_c    constant usuario.nm_usuario%type := 'tasy';
  nr_seq_evento_c constant regra_lanc_automatico.nr_seq_evento%type := 593;

  dt_entrada_w		  atendimento_paciente_v.dt_entrada%type;
  ie_tipo_atendimento_w   atendimento_paciente_v.ie_tipo_atendimento%type;
  cd_estabelecimento_w    atendimento_paciente_v.cd_estabelecimento%type;
  cd_setor_atendimento_w  atendimento_paciente_v.cd_setor_atendimento%type;
  cd_convenio_w           atendimento_paciente_v.cd_convenio%type;
  cd_categoria_w	  atendimento_paciente_v.cd_categoria%type;
  ie_tipo_convenio_w      convenio.ie_tipo_convenio%type;
  dt_entrada_unidade_w    atendimento_paciente_v.dt_entrada_unidade%type;
  dt_alta_w               atendimento_paciente_v.dt_alta%type;
  nr_seq_atepacu_w        atendimento_paciente_v.nr_seq_atepacu%type;
  ie_clinica_w            atendimento_paciente_v.ie_clinica%type;
  cd_tipo_acomod_unid_w   atendimento_paciente_v.cd_tipo_acomod_unid%type;
  cd_tipo_acomod_conv_w   atendimento_paciente_v.cd_tipo_acomod_conv%type;
  nr_anos_w               atendimento_paciente_v.nr_anos%type;
  nr_seq_classificacao_w  atendimento_paciente_v.nr_seq_classificacao%type;
  cd_plano_convenio_w     atendimento_paciente_v.cd_plano_convenio%type;
  ie_tipo_guia_w          atendimento_paciente_v.ie_tipo_guia%type;
  cd_pessoa_fisica_w      atendimento_paciente_v.cd_pessoa_fisica%type;
  cd_procedencia_w        atendimento_paciente_v.cd_procedencia%type;
  ie_sexo_w               atendimento_paciente_v.ie_sexo%type;
  nr_seq_forma_chegada_w  atendimento_paciente_v.nr_seq_forma_chegada%type;
  nr_seq_classif_medico_w atendimento_paciente.nr_seq_classif_medico%type;
  nr_seq_queixa_w         atendimento_paciente.nr_seq_queixa%type;
  nr_seq_triagem_w        atendimento_paciente.nr_seq_triagem%type;
  nr_seq_forma_laudo_w    atendimento_paciente.nr_seq_forma_laudo%type;
  ie_carater_atend_w      regra_lanc_automatico.ie_carater_inter%type;
  ie_dia_internacao_rla_w parametro_faturamento.ie_dia_internacao_rla%type;
  nr_sequencia_w          procedimento_paciente.nr_sequencia%type;
  nr_seq_regra_w          regra_lanc_aut_pac.nr_seq_regra%type;

  qt_dias_internacao_w number(6);
  dia_feriado_w        varchar2(1);
  dia_semana_w         number(1);
  ie_data_vig_atual_w  varchar2(1);
  dt_atual_trunc_w     date := trunc(sysdate);
  dt_execucao_w        date := trunc(sysdate);
  qt_idade_w           number(15, 2);
  ie_dependente_w      varchar2(1);
  nr_soma_dias_w       number(2);
  nr_seq_acao_regra_w  Number(10) 	:= 0;
  qt_procedimento_w    Number(9,3)	:= 0;
  ie_situacao_w        varchar2(1);  

  cursor c01 is
    select a.nr_sequencia,
           a.cd_setor_atendimento,
           a.ie_periodo,
           a.qt_max_taxas
      from regra_lanc_automatico a
     where a.nr_seq_evento = nr_seq_evento_c
       and a.ie_situacao = 'A'
       and a.cd_estabelecimento = cd_estabelecimento_w
       and nvl(a.cd_convenio, cd_convenio_w) = cd_convenio_w
       and nvl(a.ie_tipo_convenio, ie_tipo_convenio_w) = ie_tipo_convenio_w
       and (nvl(a.ie_tipo_atendimento, ie_tipo_atendimento_w) =
           ie_tipo_atendimento_w or ie_tipo_atendimento_w is null)
       and ((ie_clinica is null) or (ie_clinica = ie_clinica_w))
       and ((cd_tipo_acomodacao is null) or (cd_tipo_acomod_unid_w is null) or
           (cd_tipo_acomodacao =decode(nvl(ie_origem_tipo_acomod, 1), 1,
                                       cd_tipo_acomod_unid_w,
                                       cd_tipo_acomod_conv_w)))          
       and nvl(nr_seq_classificacao, nvl(nr_seq_classificacao_w, 0)) = nvl(nr_seq_classificacao_w, 0)
       and nvl(cd_plano_convenio, cd_plano_convenio_w) = cd_plano_convenio_w          
       and (nvl(nr_seq_queixa, nvl(nr_seq_queixa_w, 0)) = nvl(nr_seq_queixa_w, 0))          
       and (nvl(nr_seq_triagem, nvl(nr_seq_triagem_w, 0)) = nvl(nr_seq_triagem_w, 0)) 
       and (nvl(cd_procedencia, cd_procedencia_w) = cd_procedencia_w)          
       and (nvl(ie_feriado, 'N') = 'N' or nvl(ie_feriado, 'N') = dia_feriado_w)          
       and ((nvl(dt_dia_semana, dia_semana_w) = dia_semana_w) or (dt_dia_semana = 9))          
       and nvl(ie_tipo_guia, nvl(ie_tipo_guia_w, '0')) = nvl(ie_tipo_guia_w, '0')          
       and dt_atual_trunc_w between nvl(dt_inicio_vigencia, dt_atual_trunc_w) and fim_dia(nvl(dt_final_vigencia, dt_atual_trunc_w))
       and nvl(ie_sexo, nvl(ie_sexo_w, 'I')) = nvl(ie_sexo_w, 'I')          
       and (nvl(nr_seq_classif_medico, nvl(nr_seq_classif_medico_w, 0)) = nvl(nr_seq_classif_medico_w, 0))          
       and qt_dias_internacao_w between nvl(qt_dias_inter_inicio, qt_dias_internacao_w) and nvl(qt_dias_inter_final, qt_dias_internacao_w)
       and ((nvl(ie_primeiro_dia_mes, 'N') = 'N') or (pkg_date_utils.extract_field('DAY', dt_execucao_w) = '01'))          
       and (nvl(dt_execucao_w, sysdate) between pkg_date_utils.get_datetime(nvl(dt_execucao_w, sysdate),
                                                                            nvl(hr_inicial,                                            
                                                                            pkg_date_utils.get_time(00, 00, 00))) and           
                                                                            pkg_date_utils.get_datetime(nvl(dt_execucao_w, sysdate),                                        
                                                                            nvl(hr_final,                                            
                                                                            pkg_date_utils.get_time(23,                                                                    
                                                                            59,                                                                    
                                                                            59))))          
       and nvl(nr_seq_forma_chegada, nvl(nr_seq_forma_chegada_w, 0)) = nvl(nr_seq_forma_chegada_w, 0)          
       and nvl(qt_idade_w, 1) between nvl(obter_idade_lancto_auto(a.nr_sequencia, 'MIN'), 0) 
       and nvl(obter_idade_lancto_auto(a.nr_sequencia, 'MAX'), 9999999)          
       and ((nvl(a.ie_dependente, 'N') = 'N') or (a.ie_dependente = ie_dependente_w))          
       and (nvl(nr_seq_forma_laudo, nr_seq_forma_laudo_w) = nr_seq_forma_laudo_w);

  cursor c02 is  
    select cd_procedimento, 
           ie_origem_proced,
	   nr_seq_lanc	   
      from regra_lanc_aut_pac    
     where nr_seq_regra = nr_seq_regra_w          
       and nr_seq_regra_w <> 0          
       and (((cd_procedimento is not null) and           
             (ie_origem_proced is not null)) or           
             (nr_seq_proc_interno is not null) or (nr_seq_exame is not null))          
       and ((nr_anos_w = 999) or (nr_anos_w between nvl(qt_ano_min, 0) and nvl(qt_ano_max, 999)));

  c01_rec c01%rowtype;
  c02_rec c02%rowtype;

begin
  begin  
     select a.dt_entrada,
	   a.ie_tipo_atendimento,           
           a.cd_estabelecimento,
	   a.cd_setor_atendimento,
           a.cd_convenio,
	   a.cd_categoria,
           c.ie_tipo_convenio,           
           a.dt_entrada_unidade,           
           a.dt_alta,           
           a.nr_seq_atepacu,           
           a.ie_clinica,           
           a.cd_tipo_acomod_unid,           
           a.cd_tipo_acomod_conv,           
           nvl(nr_anos, 999),           
           a.nr_seq_classificacao,           
           nvl(a.cd_plano_convenio, '0'),           
           ie_tipo_guia,           
           cd_pessoa_fisica,           
           ie_carater_inter_sus,           
           nvl(cd_procedencia, 0),           
           nvl(a.ie_sexo, 'I'),           
           nvl(a.nr_seq_forma_chegada, 0)    
      into dt_entrada_w,
	   ie_tipo_atendimento_w,           
           cd_estabelecimento_w,       
	   cd_setor_atendimento_w,
           cd_convenio_w, 
	   cd_categoria_w,
           ie_tipo_convenio_w,           
           dt_entrada_unidade_w,           
           dt_alta_w,           
           nr_seq_atepacu_w,           
           ie_clinica_w,           
           cd_tipo_acomod_unid_w,           
           cd_tipo_acomod_conv_w,           
           nr_anos_w,           
           nr_seq_classificacao_w,           
           cd_plano_convenio_w,           
           ie_tipo_guia_w,           
           cd_pessoa_fisica_w,           
           ie_carater_atend_w,           
           cd_procedencia_w,           
           ie_sexo_w,           
           nr_seq_forma_chegada_w    
      from estabelecimento b, 
           convenio c, 
           atendimento_paciente_v a    
     where a.cd_estabelecimento = b.cd_estabelecimento          
       and c.cd_convenio = a.cd_convenio          
       and a.nr_atendimento = nr_atendimento_p;  
  exception  
    when others then    
      return;    
  end;

  begin  
    select nvl(max(nr_seq_classif_medico), 0),           
           decode(ie_dia_internacao_rla_w, 'S', trunc(max(trunc(nvl(dt_alta, sysdate)) - trunc(dt_entrada))), trunc(max(nvl(dt_alta, sysdate) - dt_entrada))) qt_dias_internacao,
           nvl(max(nr_seq_queixa), 0),
           nvl(max(nr_seq_triagem), 0),
           nvl(max(nr_seq_forma_laudo), 0)    
      into nr_seq_classif_medico_w,           
           qt_dias_internacao_w,           
           nr_seq_queixa_w,           
           nr_seq_triagem_w,           
           nr_seq_forma_laudo_w    
      from atendimento_paciente    
     where nr_atendimento = nr_atendimento_p;  
  exception  
    when others then    
      nr_seq_classif_medico_w := 0;    
      qt_dias_internacao_w := 0;    
      nr_seq_queixa_w := 0;    
      nr_seq_triagem_w := 0;    
      nr_seq_forma_laudo_w := 0;    
  end;

  /* obter feriado */
  begin  
    select 'S'    
      into dia_feriado_w    
      from feriado    
     where cd_estabelecimento = cd_estabelecimento_w          
       and to_char(dt_feriado, 'dd/mm/yyyy') = to_char(nvl(dt_execucao_w, sysdate), 'dd/mm/yyyy');  
  exception  
    when others then    
      dia_feriado_w := 'N';    
  end;

  dia_semana_w := pkg_date_utils.get_weekday(nvl(dt_execucao_w, sysdate));

  ie_data_vig_atual_w := nvl(obter_valor_param_usuario(905, 9, obter_perfil_ativo, nm_usuario_c, cd_estabelecimento_w), 'S');

  if (ie_data_vig_atual_w = 'N') then  
    dt_atual_trunc_w := trunc(dt_execucao_w);  
  end if;

  begin  
    select max(obter_idade(b.dt_nascimento, nvl(b.dt_obito, sysdate), 'DIA')),           
           nvl(max(b.ie_dependente), 'N')    
      into qt_idade_w, 
           ie_dependente_w    
      from pessoa_fisica b    
     where b.cd_pessoa_fisica = cd_pessoa_fisica_w;  
  exception  
    when others then    
      qt_idade_w := 0;    
      ie_dependente_w := 'N';    
  end;

  select nvl(max(ie_dia_internacao_rla), 'N')  
    into ie_dia_internacao_rla_w  
    from parametro_faturamento  
   where cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

  nr_seq_atepacu_w := obter_atepacu_paciente(nr_atendimento_p, 'A');

  open c01;
  loop  
  fetch c01 into 
        c01_rec;  
  exit when c01%notfound;  
        begin    
        nr_seq_regra_w := c01_rec.nr_sequencia;
    
        open c02;    
        loop      
        fetch c02 into 
              c02_rec;
      
        exit when c02%notfound;

             begin
             
             if ( c02_rec.cd_procedimento is not null) then
                select 	nvl(max(ie_situacao),'A')
                into	ie_situacao_w
                from	procedimento
                where	cd_procedimento	 = c02_rec.cd_procedimento
                and	ie_origem_proced = c02_rec.ie_origem_proced;
		
                if	(ie_situacao_w = 'I') then
                  goto continue_loop;
                end if;
             end if;             
             
             if c01_rec.ie_periodo = 'Y' then          
                select	count(pp.nr_sequencia)
		into	qt_procedimento_w            
		from	procedimento_paciente pp            
		where	pp.nr_atendimento = nr_atendimento_p  
		and	pp.cd_procedimento = c02_rec.cd_procedimento
		and	pp.ie_origem_proced = c02_rec.ie_origem_proced
		and	to_char(pp.dt_procedimento, 'yyyy') = to_char(sysdate, 'yyyy');
		
		nr_soma_dias_w	:= qt_procedimento_w + qt_dias_pagamento_p;
          
		if 	(c01_rec.qt_max_taxas > nr_soma_dias_w) then
			select procedimento_paciente_seq.nextval
			into nr_sequencia_w
			from dual;
			
			/* Se � a primeira vez que est� sendo gerado o procedimento, gerar com a quantidade desde a entrada do paciente
			Casos OutPatient que foram transformados em InPatient */
			if	(qt_procedimento_w = 0) then
				qt_procedimento_w	:= dt_execucao_w - dt_entrada_w;
			end if;
            
                      insert into procedimento_paciente              
                             (nr_sequencia,                 
                              nr_atendimento,                 
                              dt_entrada_unidade,                 
                              cd_procedimento,
                              ie_origem_proced,                 
                              dt_procedimento,                 
                              qt_procedimento,
                              dt_atualizacao,
                              nm_usuario,                 
                              nr_seq_atepacu,                 
                              cd_setor_atendimento,                 
                              cd_convenio,
			      cd_categoria,
			      nm_usuario_original,
			      ds_observacao,
			      nr_interno_conta,
			      nr_seq_regra_lanc,
			      nr_seq_lanc_acao)          
                      values (nr_sequencia_w,                 
                             nr_atendimento_p,
                             nvl(dt_entrada_unidade_w, sysdate),                 
                             c02_rec.cd_procedimento,                 
                             c02_rec.ie_origem_proced,                 
                             nvl(dt_execucao_w, nvl(dt_alta_w, sysdate)),                 
                             qt_procedimento_w,                 
                             dt_execucao_w,                 
                             nm_usuario_c,                 
                             nr_seq_atepacu_w,                 
                             cd_setor_atendimento_w,
                             cd_convenio_w,
			     cd_categoria_w,
			     nm_usuario_c,
			     obter_desc_expressao(346476) || ' ' || nr_seq_regra_w || ' ' || obter_desc_expressao(614660) || ' ' || 593,
			     null,
			     nr_seq_regra_w,
			     c02_rec.nr_seq_lanc); 			     
                   end if;
          
             elsif c01_rec.ie_periodo = 'A' then
                   select count(pp.nr_sequencia)
                     into qt_procedimento_w            
                     from procedimento_paciente pp            
                    where pp.nr_atendimento = nr_atendimento_p;     

		    nr_soma_dias_w	:= qt_procedimento_w + qt_dias_pagamento_p;		    

                    if	(c01_rec.qt_max_taxas > nr_soma_dias_w) then
                       select procedimento_paciente_seq.nextval
                         into nr_sequencia_w
                         from dual;		 
			 
			 /* Se � a primeira vez que est� sendo gerado o procedimento, gerar com a quantidade desde a entrada do paciente
			Casos OutPatient que foram transformados em InPatient */
			if	(qt_procedimento_w = 0) then
				qt_procedimento_w	:= dt_execucao_w - dt_entrada_w;
			end if;
			 
                       insert into procedimento_paciente              
                              (nr_sequencia,                 
                              nr_atendimento,                 
                              dt_entrada_unidade,                 
                              cd_procedimento,
                              ie_origem_proced,                 
                              dt_procedimento,                 
                              qt_procedimento,
                              dt_atualizacao,                 
                              nm_usuario,                 
                              nr_seq_atepacu,                 
                              cd_setor_atendimento,                 
                              cd_convenio,
			      cd_categoria,
			      nm_usuario_original,
			      ds_observacao,
			      nr_interno_conta,
			      nr_seq_regra_lanc,
			      nr_seq_lanc_acao)              
                       values (nr_sequencia_w,                 
                              nr_atendimento_p,                 
                              nvl(dt_entrada_unidade_w, sysdate),                 
                              c02_rec.cd_procedimento,                 
                              c02_rec.ie_origem_proced,    
			      nvl(dt_execucao_w, nvl(dt_alta_w, sysdate)),                 
                              qt_procedimento_w,                 
                              dt_execucao_w,                 
                              nm_usuario_c,                 
                              nr_seq_atepacu_w,                 
			      cd_setor_atendimento_w,
                              cd_convenio_w,
			      cd_categoria_w,
			      nm_usuario_c,
			      obter_desc_expressao(346476) || ' ' || nr_seq_regra_w || ' ' || obter_desc_expressao(614660) || ' ' || 593,
			      null,
			      nr_seq_regra_w,
			      c02_rec.nr_seq_lanc);         
                    end if;          
             end if;
        
             if (nr_sequencia_w > 0) then          
                atualiza_preco_procedimento(nr_sequencia_w,
                                            cd_convenio_w,
                                            nm_usuario_c);          
             end if;        
             end;
	     <<continue_loop>>
	     ie_situacao_w := null;
       end loop;    
       close c02;    
       end;  
  end loop;
  close c01;
end gerar_lanc_auto_taxa_diaria;
/
