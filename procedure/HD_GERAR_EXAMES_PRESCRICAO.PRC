create or replace
procedure HD_Gerar_Exames_Prescricao (	cd_pessoa_fisica_p	Varchar2,
					ds_regra_exames_p	Varchar2,
					nm_usuario_p		Varchar2) is 

ds_regra_exames_w		varchar2(4000);
ds_codigo_temp_w		varchar2(4000);
ds_codigo_aux_w			varchar2(2);
k				number(4);
i				number(4);

nm_pessoa_fisica_w		varchar2(80);
ds_inconsistencia_w		varchar2(4000);
ds_prescricao_gerada_w		varchar2(4000);
cd_unid_dialise_w		number(10,0);
ie_unidade_propria_w		varchar2(1);
cd_estabelecimento_w		number(4,0);
nr_atendimento_w		number(10,0);
cd_medico_responsavel_w		varchar2(10);
qt_altura_cm_w			number(4,1);
qt_peso_w			number(6,3);
nr_horas_validade_w		number(5,0);
dt_prescricao_w			date;
cd_setor_atendimento_w		number(5,0);
dt_primeiro_horario_w		date;
ie_tipo_pessoa_w		number(1,0);
ie_funcao_prescritor_w		varchar2(3);
nr_prescricao_w			number(14,0);
ie_possui_cad_medico_w		varchar2(1);
nr_seq_proc_interno_w		number(10,0);
ds_enter_w			varchar2(20)	:= chr(10)||chr(13);
cd_perfil_w			number(5,0);
ds_unid_dialise_w		varchar2(255);
qt_convenio_w			number(5,0);
nr_seq_exame_w			number(10);
cd_material_exame_w		varchar2(20);
qt_proced_w			number(5);
cd_interv_padrao_prescr_w	varchar2(255);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10,0);
nr_sequencia_w			number(10);
nr_agrupamento_w		number(6);
ds_erro_w			varchar2(255);
ie_gerar_sequencia_w		varchar2(1);
ie_gera_amostra_coleta_w 	varchar2(1);
ds_log_w			varchar2(2000);
				
begin

nr_prescricao_w		:= 0;
nr_atendimento_w	:= 0;
ds_inconsistencia_w	:= '';
ds_prescricao_gerada_w	:= '';
nm_pessoa_fisica_w	:= obter_nome_pf(cd_pessoa_fisica_p);

/* obter o unidade de hemodi�lise do paciente */
select	campo_numerico(hd_obter_unidade_prc(cd_pessoa_fisica_p,'C')),
	substr(hd_obter_unidade_prc(cd_pessoa_fisica_p,'D'),1,255)
into	cd_unid_dialise_w,
	ds_unid_dialise_w
from	dual;

if	(cd_unid_dialise_w > 0) then

	/* obter estabelecimento da unidade de hemodi�lise */
	select	cd_estabelecimento,
		ie_propria
	into	cd_estabelecimento_w,
		ie_unidade_propria_w
	from	hd_unidade_dialise
	where	nr_sequencia	= cd_unid_dialise_w;

	if	(ie_unidade_propria_w = 'S') then
		if	(cd_estabelecimento_w > 0) then
			
			/* obter o m�dico respons�vel pelo paciente */
			select	substr(hd_obter_profissional_paciente(cd_pessoa_fisica_p, 'M', 'C'),1,10)
			into	cd_medico_responsavel_w
			from	dual;

			if	(cd_medico_responsavel_w is not null) then

				select	nvl(max('S'),'N')
				into	ie_possui_cad_medico_w
				from	medico
				where	cd_pessoa_fisica	= cd_medico_responsavel_w
				and	ie_situacao		= 'A';

				if	(ie_possui_cad_medico_w = 'S') then

					/* verifica se o paciente tem atendimento gerado */
					select	nvl(max(nr_atendimento),0)
					into	nr_atendimento_w
					from	atendimento_paciente a
					where	cd_pessoa_fisica	= cd_pessoa_fisica_p
					and	cd_estabelecimento	= cd_estabelecimento_w
					and	ie_fim_conta		<> 'F'
					and	dt_alta			is null
					and	exists (select 1 from HD_LOG_GERACAO_ATEND x where a.nr_atendimento = x.NR_ATENDIMENTO_NOV);
					
					if	(nr_atendimento_w = 0) then
						select	nvl(max(nr_atendimento),0)
						into	nr_atendimento_w
						from	atendimento_paciente
						where	cd_pessoa_fisica	= cd_pessoa_fisica_p
						and	cd_estabelecimento	= cd_estabelecimento_w
						and	ie_fim_conta		<> 'F'
						and	dt_alta			is null;
						
					end if;
					

					if	(nr_atendimento_w > 0) then

						select 	count(*)
						into	qt_convenio_w
						from 	atend_categoria_convenio
						where	nr_atendimento = nr_atendimento_w;

						if 	(qt_convenio_w > 0) then

							dt_prescricao_w		:= sysdate;

							/* obter horas de validade */
							select	obter_valor_param_usuario(924, 155, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)
							into	nr_horas_validade_w
							from	dual;

							/* obter peso do paciente */
							select	obter_sinal_vital(nr_atendimento_w,'Peso')
							into	qt_peso_w
							from 	dual;

							/* obter altura do paciente */
							select	obter_sinal_vital(nr_atendimento_w,obter_desc_expressao(283402))
							into	qt_altura_cm_w
							from 	dual;

							/* obter setor do atendimento */
							select	obter_setor_atendimento(nr_atendimento_w)
							into	cd_setor_atendimento_w
							from	dual;

							/* obter primeiro hor�rio da prescri��o */
							select	obter_prim_horario_prescricao(nr_atendimento_w, cd_setor_atendimento_w,dt_prescricao_w, nm_usuario_p,'R')
							into	dt_primeiro_horario_w
							from	dual;

							/* obter tipo da pessoa do usu�rio */
							select	obter_tipo_pessoa(cd_medico_responsavel_w)
							into	ie_tipo_pessoa_w
							from	dual;

							/* obter fun��o prescritor */
							select	substr(obter_funcao_usuario_orig(obter_usuario_pessoa(cd_medico_responsavel_w)),1,3)
							into	ie_funcao_prescritor_w
							from	dual;

							/* gera n�mero da prescri��o */
							select	prescr_medica_seq.nextval
							into	nr_prescricao_w
							from	dual;

							insert into prescr_medica (
								nr_prescricao,
								cd_pessoa_fisica,
								nr_atendimento,
								cd_medico,
								dt_prescricao,
								dt_atualizacao,
								nm_usuario,
								ds_observacao,
								nr_horas_validade,
								dt_primeiro_horario,
								cd_setor_atendimento,
								ie_recem_nato,
								ie_origem_inf,
								nm_usuario_original,
								qt_altura_cm,
								qt_peso,
								cd_estabelecimento,
								cd_prescritor,
								ie_funcao_prescritor,
								ie_prescricao_alta,
								ie_prescr_emergencia,
								ie_adep,
								ie_hemodialise
							) values (
								nr_prescricao_w,
								cd_pessoa_fisica_p,
								nr_atendimento_w,
								cd_medico_responsavel_w,
								dt_prescricao_w,
								sysdate,
								nm_usuario_p,
								'',
								nr_horas_validade_w,
								dt_primeiro_horario_w,
								cd_setor_atendimento_w,
								'N',
								ie_tipo_pessoa_w,
								nm_usuario_p,
								qt_altura_cm_w,
								qt_peso_w,
								cd_estabelecimento_w,
								cd_medico_responsavel_w,
								ie_funcao_prescritor_w,
								'N',
								'N',
								'N',
								'N'
							);

							/*gravar a prescri��o na tabela  hd_protocolo_exame_prescr*/
							insert into hd_protocolo_exame_prescr(	nr_sequencia,
												dt_atualizacao,
												nm_usuario,
												dt_atualizacao_nrec,
												nm_usuario_nrec,
												nr_prescricao,
												cd_pessoa_fisica,
												ie_manual)
							values(	hd_protocolo_exame_prescr_seq.nextval,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_prescricao_w,
								cd_pessoa_fisica_p,
								'S');
							
							/* gerar exames na prescri��o*/
							Obter_Param_Usuario(924,35,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,cd_interv_padrao_prescr_w);
							ds_regra_exames_w := ds_regra_exames_p;
							k := 1;
							for i in k..length(ds_regra_exames_w) loop
								begin

								ds_codigo_aux_w	:= substr(ds_regra_exames_w,i,1);
								
								if (ds_codigo_aux_w = ',') then
									
									select	nr_seq_exame,
										nr_seq_proc_interno,
										cd_material_exame,
										qt_material
									into	nr_seq_exame_w,
										nr_seq_proc_interno_w,
										cd_material_exame_w,
										qt_proced_w
									from	hd_regra_gerar_exame
									where	nr_sequencia = ds_codigo_temp_w;
									
									Obter_Proc_Tab_Interno(nr_seq_proc_interno_w,nr_prescricao_w,nr_atendimento_w,null,cd_procedimento_w, ie_origem_proced_w,null,null);
									
									select 	nvl(max(nr_sequencia),0),
										nvl(max(nr_agrupamento),0)
									into	nr_sequencia_w,
										nr_agrupamento_w
									from prescr_procedimento
									where nr_prescricao = nr_prescricao_w;
									
									begin
									nr_agrupamento_w := nr_agrupamento_w + 1;
									insert into prescr_procedimento 
										(nr_prescricao, 
										nr_sequencia,
										nr_agrupamento, 
										cd_procedimento, 
										qt_procedimento, 
										dt_atualizacao,
										nm_usuario,  
										cd_motivo_baixa, 
										ie_origem_proced, 
										cd_intervalo,
										ie_urgencia, 
										ie_suspenso, 
										cd_setor_atendimento, 
										dt_prev_execucao,
										cd_material_exame, 
										nr_seq_exame, 
										ie_status_atend, 
										ie_amostra, 
										ie_origem_inf,
										ie_executar_leito,
										ie_se_necessario,
										nr_seq_interno,
										nr_seq_proc_interno,
										ie_avisar_result)
									values (nr_prescricao_w, 
										nr_sequencia_w + 1,
										nr_agrupamento_w, 
										cd_procedimento_w, 
										qt_proced_w, 
										sysdate,
										nm_usuario_p,  
										0, 
										ie_origem_proced_w, 
										cd_interv_padrao_prescr_w, 
										'N', 
										'N', 
										cd_setor_atendimento_w,
										dt_prescricao_w, 
										cd_material_exame_w, 
										nr_seq_exame_w, 
										5, 
										'N', 
										'1',
										'N',
										'N',
										prescr_procedimento_seq.NextVal,
										nr_seq_proc_interno_w,
										'N');

									consistir_prescr_procedimento(nr_prescricao_w, nr_sequencia_w + 1, nm_usuario_p, 0, ds_erro_w);

									if ((ds_erro_w is not null) and  ( ds_erro_w <> 0)) then
										Wheb_mensagem_pck.exibir_mensagem_abort(207161,'DS_ERRO='||ds_erro_w);
									end if;
									
									exception
										when others then
											ds_erro_w	:= SQLERRM(sqlcode);
											Wheb_mensagem_pck.exibir_mensagem_abort(207163,'DS_ERRO='||ds_erro_w); 
									end;
									
									ds_codigo_temp_w := '';
								else
									ds_codigo_temp_w := ds_codigo_temp_w||ds_codigo_aux_w;
								end if;
								end;
							end loop;
							
							update 	prescr_procedimento
							set	cd_setor_atendimento 	= cd_setor_atendimento_w
							where	nr_prescricao 		= nr_prescricao_w
							and	cd_setor_atendimento is null;

							update 	prescr_procedimento
							set	ie_status_atend = 10
							where	nr_prescricao 		= nr_prescricao_w
							and	ie_status_atend = 5;
							
							liberar_prescricao(nr_prescricao_w,
									nr_atendimento_w,
									'S',
									obter_perfil_ativo,
									nm_usuario_p,
									'N',
									ds_log_w);
							
							/*select  nvl(max(a.ie_gerar_sequencia),'P'),
								nvl(MAX(a.ie_gera_amostra_coleta),'N')
							into 	ie_gerar_sequencia_w,
								ie_gera_amostra_coleta_w	
							from 	lab_parametro a
							where 	a.cd_estabelecimento = cd_estabelecimento_w;

							begin
							Gerar_Prescr_Proc_Seq_Lab(nr_prescricao_w, nm_usuario_p, 'P');
							if	((ie_gerar_sequencia_w <> 'C') or
								(ie_gera_amostra_coleta_w <> 'S'))  then
								gerar_prescr_proc_mat_item (nr_prescricao_w, nm_usuario_p, cd_estabelecimento_w);
							end if;
							exception
								when others then
								cd_perfil_w	:= 0;
							end;*/
							
						else
							ds_inconsistencia_w	:= ds_inconsistencia_w||'-> '||obter_desc_expressao(775705) || nr_atendimento_w || ds_enter_w;
						end if;
					else
						ds_inconsistencia_w	:= ds_inconsistencia_w||'-> '||obter_desc_expressao(775707)||ds_enter_w;
					end if;
				else
						ds_inconsistencia_w	:= ds_inconsistencia_w||'-> '||obter_desc_expressao(775709)||ds_enter_w;
				end if;
			else
				ds_inconsistencia_w	:= ds_inconsistencia_w||'-> '||obter_desc_expressao(775711)||ds_enter_w;
			end if;
		else
			ds_inconsistencia_w	:= ds_inconsistencia_w||'-> '||obter_desc_expressao(775713)||ds_enter_w;
		end if;
	else
		ds_inconsistencia_w	:= ds_inconsistencia_w||'-> '||obter_desc_expressao(775715)||ds_enter_w;
	end if;
else
	ds_inconsistencia_w	:= ds_inconsistencia_w||'-> '||obter_desc_expressao(775717)||ds_enter_w;
end if;

if	(nr_prescricao_w > 0) then
	ds_prescricao_gerada_w	:= ds_prescricao_gerada_w || SUBSTR(wheb_mensagem_pck.get_texto(807567,'NR_PRESCRICAO='||nr_prescricao_w||';CD_PESSOA_FISICA='||cd_pessoa_fisica_p||' - '||'NM_PESSOA_FISICA='||nm_pessoa_fisica_w),1,2000) || ds_enter_w || obter_desc_expressao(327416)||' ' || ds_unid_dialise_w;
else
	ds_prescricao_gerada_w	:= SUBSTR(wheb_mensagem_pck.get_texto(807566,'CD_PESSOA_FISICA='||cd_pessoa_fisica_p||'; - '||';NM_PESSOA_FISICA='||nm_pessoa_fisica_w),1,2000) ||ds_enter_w||ds_inconsistencia_w||obter_desc_expressao(775725) || ds_enter_w || obter_desc_expressao(327416)||' ' || ds_unid_dialise_w;
end if;

if	(ds_prescricao_gerada_w is not null) then

	begin
	select	cd_perfil
	into	cd_perfil_w
	from	hd_parametro
	where	cd_estabelecimento	= cd_estabelecimento_w;
	exception
		when others then
		cd_perfil_w	:= 0;
	end;

	insert into comunic_interna (
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nm_usuario_destino,
		ds_perfil_adicional,
		nr_sequencia,
		ie_gerencial,
		dt_liberacao,
		cd_estab_destino
	) values (
		sysdate,
		obter_desc_expressao(775727)||' '||cd_pessoa_fisica_p||' - '||nm_pessoa_fisica_w,
		ds_prescricao_gerada_w,
		nm_usuario_p,
		sysdate,
		'N',
		'',
		cd_perfil_w||', ',
		comunic_interna_seq.nextval,
		'N',
		sysdate,
		cd_estabelecimento_w
	);
	
	if	(cd_pessoa_fisica_p is not null) and
		(nr_prescricao_w = 0) then	
		commit;
		Wheb_mensagem_pck.exibir_mensagem_abort(194483,'DS_MENSAGEM='||ds_prescricao_gerada_w);
	end if;

end if;

commit;

end HD_Gerar_Exames_Prescricao;
/