create or replace
procedure dis_gerar_romaneio_pedido(	nr_pedido_p 	number,
				nm_usuario_p	Varchar2) is 

ordem_w				number(3);
cd_material_w			number(6);
cd_material_ww			number(6);
qt_conv_compra_estoque_w		number(13,4);
qt_conv_compra_estoque_ww	number(13,4);
qt_conversao_w			number(13,4);
nr_sequencia_item_w		number(10);
nr_sequencia_entrega_w		number(10);
dt_prevista_entrega_w		date;
qt_prevista_entrega_w		number(13,4);
nr_seq_romaneio_w			number(10);
nr_seq_romaneio_item_w		number(10);
ie_agrupa_romaneio_w		varchar2(1);

qt_acumulado_w			number(13,4);
qt_gerar_w			number(13,4);
qt_gerar_ww			number(13,4);
qt_diferenca_w			number(13,4);
			
Cursor C01 is
	select	b.nr_sequencia,
		c.nr_sequencia,
		nvl(b.ie_agrupa_romaneio,'S'),
		b.cd_material,	
		c.dt_prevista_entrega,
		nvl(sum(c.qt_prevista_entrega),0)
	from	dis_pedido a,
		dis_pedido_item b,
		dis_pedido_item_entrega c
	where	a.nr_sequencia = b.nr_seq_pedido
	and	b.nr_sequencia = c.nr_sequencia_item
	and	b.nr_seq_motivo_cancel is null
	and	a.nr_sequencia = nr_pedido_p
	group by b.nr_sequencia,
		c.nr_sequencia,
		nvl(b.ie_agrupa_romaneio,'S'),
		b.cd_material,	
		c.dt_prevista_entrega
	order by b.nr_sequencia;
	
Cursor C02 is
	select	a.cd_material,
		nvl(a.qt_conv_compra_estoque,1) qt_conv_compra_estoque,
		1 ordem
	from	material a
	where	a.cd_material = cd_material_w
	union all
	select	b.cd_material,
		nvl(b.qt_conv_compra_estoque,1) qt_conv_compra_estoque,
		2 ordem
	from	material a,
		material b
	where	a.cd_material = cd_material_w
	and	b.cd_material = a.cd_material_generico
	and	a.cd_material <> b.cd_material
	and	b.ie_situacao = 'A'
	union all
	select	b.cd_material,
		nvl(b.qt_conv_compra_estoque,1) qt_conv_compra_estoque,
		3 ordem
	from	material a,
		material b
	where	a.cd_material = cd_material_w
	and	b.cd_material_generico = a.cd_material_generico
	and	a.cd_material <> b.cd_material
	and	b.cd_material <> a.cd_material_generico
	and	b.ie_situacao = 'A'
	order by qt_conv_compra_estoque asc, ordem;
	
begin
open C01;
loop
fetch C01 into	
	nr_sequencia_item_w,
	nr_sequencia_entrega_w,
	ie_agrupa_romaneio_w,
	cd_material_w,
	dt_prevista_entrega_w,
	qt_prevista_entrega_w;
exit when C01%notfound;
	begin
	select	nvl(a.qt_conv_compra_estoque,1)
	into	qt_conv_compra_estoque_w
	from	material a
	where	a.cd_material = cd_material_w;
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_romaneio_w
	from	dis_romaneio
	where	nr_seq_pedido = nr_pedido_p
	and	dt_prevista_entrega = dt_prevista_entrega_w;
	
	if	(nr_seq_romaneio_w = 0) then
		begin
		select	dis_romaneio_seq.nextval
		into	nr_seq_romaneio_w
		from	dual;
		
		insert into dis_romaneio
			(nr_sequencia,
			nr_seq_pedido,
			dt_prevista_entrega,
			dt_geracao,
			dt_atualizacao_nrec,
			dt_atualizacao,
			nm_usuario_nrec,
			nm_usuario)
		values	
			(nr_seq_romaneio_w,
			nr_pedido_p,
			dt_prevista_entrega_w,
			sysdate,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p);
		end;
	end if;
	
	qt_acumulado_w := 0;
	if	(ie_agrupa_romaneio_w = 'S') then
		begin
		open C02;
		loop
		fetch C02 into	
			cd_material_ww,
			qt_conv_compra_estoque_ww,
			ordem_w;
		exit when (C02%notfound) or (qt_acumulado_w >= qt_prevista_entrega_w);
			begin
			qt_conversao_w := dividir(qt_conv_compra_estoque_w,qt_conv_compra_estoque_ww);
			qt_gerar_w := (qt_prevista_entrega_w - qt_acumulado_w);
			qt_gerar_ww := dividir(qt_gerar_w,qt_conversao_w) /*Utilizando para tratar diferenša*/;
			qt_gerar_w := trunc(dividir(qt_gerar_w,qt_conversao_w));
			qt_gerar_ww	:= (qt_gerar_ww - qt_gerar_w) /*Utilizando para tratar diferenša*/;
			
			if (qt_gerar_w >= 1) then
				begin
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_romaneio_item_w
				from	dis_romaneio_item
				where	nr_seq_romaneio = nr_seq_romaneio_w
				and	nr_seq_pedido = nr_pedido_p
				and	cd_material = cd_material_ww
				and	nr_sequencia_item = nr_sequencia_item_w
				and	nr_sequencia_entrega = nr_sequencia_entrega_w
				and	nr_seq_motivo_cancel is null;
				
				if	(nr_seq_romaneio_item_w > 0) then
					update	dis_romaneio_item
					set	qt_material = (qt_material + qt_gerar_w),
						nm_usuario = nm_usuario_p,
						dt_atualizacao = sysdate
					where	nr_sequencia = nr_seq_romaneio_item_w;
				else
					begin
					select	dis_romaneio_item_seq.nextval
					into	nr_seq_romaneio_item_w
					from	dual;
					
					insert into dis_romaneio_item
						(nr_sequencia,
						nr_seq_romaneio,
						nr_seq_pedido,
						nr_sequencia_item,
						nr_sequencia_entrega,
						cd_material,
						qt_material,
						dt_atualizacao_nrec,
						dt_atualizacao,
						nm_usuario_nrec,
						nm_usuario)
					values 
						(nr_seq_romaneio_item_w,
						nr_seq_romaneio_w,
						nr_pedido_p,
						nr_sequencia_item_w,
						nr_sequencia_entrega_w,
						cd_material_ww,
						qt_gerar_w,
						sysdate,
						sysdate,
						nm_usuario_p,
						nm_usuario_p);
					end;
				end if;
				qt_acumulado_w := (qt_acumulado_w + (qt_gerar_w * qt_conversao_w));
				end;
			end if;
			end;
		end loop;
		close C02;
		qt_diferenca_w := (qt_prevista_entrega_w - qt_acumulado_w);
		if	(qt_diferenca_w > 0) then		
			update	dis_romaneio_item
			set	qt_material = (qt_material + qt_gerar_ww),
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where nr_sequencia = nr_seq_romaneio_item_w;
		end if;
		end;
	else
		begin
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_romaneio_item_w
		from	dis_romaneio_item
		where	nr_seq_romaneio = nr_seq_romaneio_w
		and	nr_seq_pedido = nr_pedido_p
		and	cd_material = cd_material_w
		and	nr_sequencia_item = nr_sequencia_item_w
		and	nr_sequencia_entrega = nr_sequencia_entrega_w
		and	nr_seq_motivo_cancel is null;
			
		if	(nr_seq_romaneio_item_w > 0) then
			update	dis_romaneio_item
			set	qt_material = (qt_material + qt_prevista_entrega_w),
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate
			where	nr_sequencia = nr_seq_romaneio_item_w;
		else
			insert into dis_romaneio_item
				(nr_sequencia,
				nr_seq_romaneio,
				nr_seq_pedido,
				nr_sequencia_item,
				nr_sequencia_entrega,
				cd_material,
				qt_material,
				dt_atualizacao_nrec,
				dt_atualizacao,
				nm_usuario_nrec,
				nm_usuario)
			values 
				(dis_romaneio_item_seq.nextval,
				nr_seq_romaneio_w,
				nr_pedido_p,
				nr_sequencia_item_w,
				nr_sequencia_entrega_w,
				cd_material_w,
				qt_prevista_entrega_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p);
		end if;
		end;
	end if;
	commit;
	end;
end loop;
close C01;

update	dis_pedido
set	dt_geracao_romaneio = sysdate
where	nr_sequencia = nr_pedido_p;
commit;
end dis_gerar_romaneio_pedido;
/
