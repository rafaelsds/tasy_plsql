create or replace
procedure Atualizar_paciente_triagem(	nr_seq_triagem_p	number,
										nm_usuario_p		Varchar2) is 

begin
update	triagem_pronto_atend
set		ie_status_paciente = 'T',
		dt_fim_triagem = sysdate
where	nr_sequencia = nr_seq_triagem_p;

commit;

end Atualizar_paciente_triagem;
/