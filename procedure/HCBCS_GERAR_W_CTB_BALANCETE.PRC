create or replace
procedure hcbcs_gerar_w_ctb_balancete (	cd_estab_p			number,
					nr_seq_mes_ref_p			number,
					ie_normal_encerramento_p		varchar2,
					cd_empresa_parametro_p		number,
					cd_classificacao_p			varchar2,
					nr_nivel_p			varchar2,
					nm_usuario_p			varchar2,
					ie_conta_saldo_zerado_p		varchar2) is


nr_seq_mes_ref_w			number(10);
ie_normal_encerramento_w		varchar2(1);
cd_empresa_w			number(4);
cd_conta_contabil_w		varchar2(20);
cd_classificacao_w			varchar2(40);
cd_classif_conta_atual_w		varchar2(40);
cd_grupo_w			number(10);
ie_tipo_w				varchar2(1);
ie_nivel_w			number(10);
ie_centro_custo_w			varchar2(1);
dt_referencia_w			date;
ds_conta_apres_w			varchar2(100);
vl_debito_w			number(15,2);
vl_credito_w			number(15,2);
vl_saldo_w			number(15,2);
vl_movimento_w			number(15,2);
vl_saldo_ant_w			number(15,2);
qt_conta_w			number(10);
ie_gerar_w			varchar2(1);
qt_movimento_w			number(10);			

cursor c01 is
select	a.cd_conta_contabil,
	a.cd_classificacao,
	a.cd_grupo,
	a.ie_tipo,
	ctb_obter_nivel_classif_conta(a.cd_classificacao),
	a.ie_centro_custo,
	substr(a.ds_conta_apres,1,100),
	sum(a.vl_debito),
	sum(a.vl_credito),
	sum(a.vl_movimento)
from	ctb_balancete_v a
where 	a.nr_seq_mes_ref		= nr_seq_mes_ref_p
and	(cd_classificacao_p is null or substr(CTB_Obter_Se_Conta_Classif_Sup(a.cd_conta_contabil, cd_classificacao_p),1,1) = 'S')
and	a.cd_empresa			= cd_empresa_parametro_p
and	((nvl(cd_estab_p, 0) = 0) or (a.cd_estabelecimento = cd_estab_p))
and	(a.ie_normal_encerramento	= ie_normal_encerramento_p)
and	ctb_obter_nivel_classif_conta(cd_classificacao) <= nr_nivel_p
group by
	a.cd_grupo,
	a.ie_tipo,
	a.cd_conta_contabil,
	a.nr_seq_mes_ref,
	a.cd_classificacao,
	a.ds_conta_apres,
	a.ie_centro_custo;

begin



qt_conta_w	:= 0;
delete from w_ctb_balancete
where nm_usuario = nm_usuario_p;
commit;


open  c01;
loop
fetch c01 into
	cd_conta_contabil_w,
	cd_classificacao_w,
	cd_grupo_w,
	ie_tipo_w,
	ie_nivel_w,
	ie_centro_custo_w,
	ds_conta_apres_w,
	vl_debito_w,
	vl_credito_w,
	vl_movimento_w;
exit when c01%notfound;
	begin
	
	ie_gerar_w	:= 'S';
	
	select	nvl(sum(vl_saldo_ant),0)
	into	vl_saldo_ant_w 
	from	ctb_balancete_v a
	where	nr_seq_mes_ref		= nr_seq_mes_ref_p
	and	ie_normal_encerramento	= ie_normal_encerramento_p
	and	((a.cd_estabelecimento	= cd_estab_p) or (nvl(cd_estab_p,0) = 0))
	and	a.cd_empresa		= cd_empresa_parametro_p
	and	cd_conta_contabil		= cd_conta_contabil_w;


	select	nvl(sum(vl_saldo),0)
	into	vl_saldo_w
	from	ctb_balancete_v a
	where	nr_seq_mes_ref		= nr_seq_mes_ref_p
	and	ie_normal_encerramento	= ie_normal_encerramento_p
	and	a.cd_estabelecimento	= nvl(cd_estab_p, a.cd_estabelecimento)
	and	a.cd_empresa		= cd_empresa_parametro_p
	and	cd_conta_contabil		= cd_conta_contabil_w;
	
	if	(nvl(ie_conta_saldo_zerado_p, 'S') = 'N') then
		begin
		
		if	(vl_saldo_w = 0) then
		
			ie_gerar_w	:= 'N';
		end if;
		
		if	(ie_gerar_w = 'N') and
			(ie_tipo_w = 'T') then
			begin
			
			select	ctb_obter_classif_conta(cd_conta_contabil_w, cd_classificacao_w, sysdate)
			into	cd_classif_conta_atual_w
			from	dual;
			
			
			select	count(*)
			into	qt_movimento_w
			from	ctb_balancete_v a
			where	nr_seq_mes_ref		= nr_seq_mes_ref_p
			and	ie_normal_encerramento	= ie_normal_encerramento_p
			and	a.cd_estabelecimento	= nvl(cd_estab_p, a.cd_estabelecimento)
			and	a.cd_empresa		= cd_empresa_parametro_p
			and	substr(ctb_obter_se_classif_sup(a.cd_classificacao, cd_classif_conta_atual_w), 1,1) = 'S';
			
			if	(qt_movimento_w > 0) then
				
				ie_gerar_w	:= 'S';
			end if;
			end;
		end if;
		
		end;
	end if;
	
	if	(ie_gerar_w = 'S') then
		begin
			qt_conta_w	:= qt_conta_w + 1;
		if	(qt_conta_w = 2000) then
			commit;
		end if;

		insert	into w_ctb_balancete(
			ie_normal_encerramento,
			cd_empresa,
			cd_estabelecimento,
			cd_conta_contabil,
			cd_classificacao,
			cd_grupo,
			ie_tipo,
			ie_nivel,
			ie_centro_custo,
			ds_conta_apres,
			vl_debito,
			vl_credito,
			vl_saldo,
			vl_movimento,
			vl_saldo_ant,
			nm_usuario,
			dt_atualizacao)
		values(	ie_normal_encerramento_p,
			cd_empresa_parametro_p,
			cd_estab_p,
			cd_conta_contabil_w,
			cd_classificacao_w,
			cd_grupo_w,
			ie_tipo_w,
			ie_nivel_w,
			ie_centro_custo_w,
			ds_conta_apres_w,
			vl_debito_w,
			vl_credito_w,
			vl_saldo_w,
			vl_movimento_w,
			vl_saldo_ant_w,
			nm_usuario_p,
			sysdate);
		end;
	end if;
	
	end;
end loop;
close c01;

commit;

end hcbcs_gerar_w_ctb_balancete;
/