create or replace
procedure atualizar_assinatura_evento(
						nr_seq_item_pend_p		number,
						ie_tipo_item_p		varchar2,
						nr_seq_assinatura_p	number) is

nr_seq_evento_w 	number(10);
begin

if (nvl(nr_seq_item_pend_p, 0) > 0) then

	if (ie_tipo_item_p = 'O') then
	
		select 	max(nr_seq_prescr_gas)
		into 	nr_seq_evento_w
		from 	pep_item_pendente_comp
		where 	nr_seq_item_pend 	= nr_seq_item_pend_p;
		
		if (nr_seq_evento_w is null) then
			Gravar_Hora_assinat_pendente(	nr_atendimento_p => null,
											nr_seq_interno_p => nr_seq_item_pend_p,
											nr_seq_assinatura_p => nr_seq_assinatura_p);
		else
			update 	prescr_gasoterapia_evento
			set 	nr_seq_assinatura = nr_seq_assinatura_p
			where 	nr_sequencia 	= nr_seq_evento_w;
		end if;

	elsif (ie_tipo_item_p = 'DI') then
	
		select 	max(nr_seq_presc_alter)
		into 	nr_seq_evento_w
		from 	pep_item_pendente_comp
		where 	nr_seq_item_pend 	= nr_seq_item_pend_p;
		
		if (nr_seq_evento_w is null) then
			Gravar_Hora_assinat_pendente(	nr_atendimento_p => null,
											nr_seq_interno_p => nr_seq_item_pend_p,
											nr_seq_assinatura_p => nr_seq_assinatura_p);
		else
			update 	hd_prescricao_evento
			set 	nr_seq_assinatura = nr_seq_assinatura_p
			where 	nr_sequencia 	= nr_seq_evento_w;
		end if;


	elsif (ie_tipo_item_p in ('IA', 'ME', 'IAG', 'IAH', 'LD', 'MAT', 'M', 'D', 'P', 'G', 'C', 'I', 'L', 'R', 'E')) then
		
		
		select 	max(nr_seq_mat_alter)
		into 	nr_seq_evento_w
		from 	pep_item_pendente_comp
		where 	nr_seq_item_pend 	= nr_seq_item_pend_p;
		
		if (nr_seq_evento_w is null) then
			Gravar_Hora_assinat_pendente(	nr_atendimento_p => null,
											nr_seq_interno_p => nr_seq_item_pend_p,
											nr_seq_assinatura_p => nr_seq_assinatura_p);
		else
			update 	prescr_mat_alteracao
			set 	nr_seq_assinatura = nr_seq_assinatura_p
			where 	nr_sequencia 	= nr_seq_evento_w;
		end if;

	else
		
		select 	max(nr_seq_prescr_sol)
		into 	nr_seq_evento_w
		from 	pep_item_pendente_comp
		where 	nr_seq_item_pend 	= nr_seq_item_pend_p;
		
		if (nr_seq_evento_w is null) then
			Gravar_Hora_assinat_pendente(	nr_atendimento_p => null,
											nr_seq_interno_p => nr_seq_item_pend_p,
											nr_seq_assinatura_p => nr_seq_assinatura_p);
		else
			update 	prescr_solucao_evento
			set 	nr_seq_assinatura = nr_seq_assinatura_p
			where 	nr_sequencia 	= nr_seq_evento_w;
		end if;

	end if;

end if;

commit;

end atualizar_assinatura_evento;
/
