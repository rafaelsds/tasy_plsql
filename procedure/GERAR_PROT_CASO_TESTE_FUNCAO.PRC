create or replace
procedure gerar_prot_caso_teste_funcao(		nr_sequencia_p		number,
						nm_usuario_p		varchar2) is
					
nr_sequencia_w			number(10);
nr_seq_item_w			number(10);
nr_seq_item_aux_w			number(10);
cd_funcao_w			number(5);
ds_item_w			varchar2(80);
ds_acao_w			varchar2(2000);
ds_result_w			varchar2(2000);

cursor c01 is
	select	nr_sequencia
	from	teste_funcao
	where	cd_funcao = cd_funcao_w	
	and	ie_situacao = 'A';

cursor c02 is
	select	nr_sequencia,
		ds_item
	from	teste_funcao_item
	where	nr_seq_teste = nr_sequencia_w
	and	ie_situacao = 'A';
	
cursor c03 is
	select	a.ds_acao,
		b.ds_resultado
	from	teste_func_item_result b,
		teste_funcao_item_acao a
	where	b.nr_seq_acao = a.nr_sequencia
	and	a.nr_seq_item = nr_seq_item_aux_w
	and	a.ie_situacao = 'A'
	and	b.ie_situacao = 'A';

begin

if	(nr_sequencia_p is not null) then
	begin
	select	cd_funcao
	into	cd_funcao_w
	from	teste_caso_teste
	where	nr_sequencia = nr_sequencia_p;
	
	open c01;
	loop
	fetch c01 into
		nr_sequencia_w;
	exit when c01%notfound;
		begin
		open c02;
		loop
		fetch c02 into
			nr_seq_item_aux_w,
			ds_item_w;
		exit when c02%notfound;
			begin
			select	teste_caso_teste_item_seq.nextval
			into	nr_seq_item_w
			from	dual;
			
			insert into teste_caso_teste_item(
					nr_sequencia,
					nr_seq_caso_teste,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_apres,
					ds_item)
				values(	nr_seq_item_w,
					nr_sequencia_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					null,
					ds_item_w);
			open c03;
			loop
			fetch c03 into
				ds_acao_w,
				ds_result_w;
			exit when c03%notfound;
				begin
				insert into teste_caso_teste_it_acao(
					nr_sequencia,
					nr_seq_item,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_apres,
					ds_premissas,
					ds_acao,
					ds_result_esperado,
					ds_observacao,
					cd_perfil)
				values(	teste_caso_teste_it_acao_seq.nextval,
					nr_seq_item_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					null,
					null,
					ds_acao_w,
					ds_result_w,
					null,
					null);
				end;
			end loop;
			close c03;
			end;
		end loop;
		close c02;
		end;
	end loop;
	close c01;
	commit;
	end;
end if;

end gerar_prot_caso_teste_funcao;
/