create or replace
procedure fis_reg_ecf_L990(	nr_seq_controle_p	varchar2,
				ds_separador_p		varchar2,
				cd_estabelecimento_p	varchar2,
				nm_usuario_p		varchar2,
				cd_empresa_p		varchar2,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is 

nr_seq_registro_w		number(10) := nr_sequencia_p;	
nr_linha_w			number(10) := qt_linha_p;
ds_arquivo_w			varchar2(4000);	
ds_arquivo_compl_w		varchar2(4000);	
ds_linha_w			varchar2(4000);
sep_w				varchar2(2) := ds_separador_p;
qt_linhas_w			number(10);
begin

select	(select 	count(*) + 1
	from	fis_ecf_arquivo
	where	nm_usuario 		= nm_usuario_p
	and	nr_seq_controle_ecf 	= nr_seq_controle_p	
	and	substr(cd_registro,1,1)	= 'L') qt_linhas
into	qt_linhas_w	
from	dual;

ds_linha_w	:= substr(sep_w || 'L990' || sep_w || qt_linhas_w || sep_w,1,8000);

ds_arquivo_w		:= substr(ds_linha_w,1,4000);
ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
nr_seq_registro_w	:= nr_seq_registro_w + 1;
nr_linha_w		:= nr_linha_w + 1;

insert into fis_ecf_arquivo(
	nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	nr_seq_controle_ecf,
	nr_linha,
	cd_registro,
	ds_arquivo,
	ds_arquivo_compl)
values(	nr_seq_registro_w,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nr_seq_controle_p,
	nr_linha_w,
	'L990',
	ds_arquivo_w,
	ds_arquivo_compl_w);
commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end fis_reg_ecf_L990;
/