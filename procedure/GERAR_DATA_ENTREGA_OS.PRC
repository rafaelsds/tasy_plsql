create or replace
procedure gerar_data_entrega_os(nr_seq_gerencia_p	number,
				nr_seq_fila_p	number) is 

type campos is 
	record  (
		nr_seq_ordem_w			number(10),
		nr_ordem_serv_w			number(10),
		ie_prioridade_w			number(10),
		dt_ordem_serv_w			date,
		qt_min_prev_w			number(15),
		nr_seq_lp_w			number(10),
		nr_seq_ativ_prev_w 		number(10));
		
	type vetor is 
		table of campos index by binary_integer;
	
vetor_regra_w			vetor;				
				
nr_seq_ordem_w			number(10); 
nr_ordem_serv_w			number(10);
ie_prioridade_w			number(10);
dt_ordem_serv_w			date;
nm_usuario_w			varchar2(20);
nr_seq_prevista_w 			number(10);
nr_seq_ativ_prev_w 		number(10);
qt_registro_w			number(10);
nr_seq_ordem_antiga_w 		number(10);
nr_seq_fila_w			number(10);
nm_usuario_fila_w			varchar2(15);
qt_min_prev_w			number(15);
nr_seq_lp_w			number(10);
dt_prevista_ww			date;
dt_prevista_www			date;
/*dados para tabela da fila*/
dt_referencia_w			date	:= trunc(sysdate,'dd');
qt_integrantes_w			number(10)	:= 0;	
qt_min_capac_prod_diaria_w		number(15)	:= 0;
qt_min_previsto_dia_w		number(15)	:= 0;
qt_min_realizado_dia_w		number(10)	:= 0;
nr_seq_exec_w			number(10);
dt_prev_entrega_w			date;
nr_seq_gerencia_w			number(10)	:= nr_seq_gerencia_p;
nr_seq_pacote_entrega_w		number(10);
qt_min_prev_usuarios_fila_w		number(10);
ie_dia_util_w			varchar2(1);
ie_atualiza_dt_prev_w		varchar2(1);
ie_pacote_entrega_w		varchar2(1);
qt_min_medio_prev_w		number(10);
dt_inicial_w			date;
dt_final_w			date;
ie_os_adicional_pacote_w		varchar2(1);
qt_pacote_atual_w			number(10);
ie_calc_score_w			cadastro_fila.ie_calc_score%type;
ie_capacidade_skill_w		cadastro_fila.ie_capacidade_skill%type;
i				integer;

ie_prox_w			varchar2(1);
nm_usuario_lista_w			lista_usuario_os.nm_usuario_lista%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_seq_w				number(10) := 0;
	
cursor c01 is
select 	1 nr_seq_ordem,
	a.nr_sequencia,
	c.ie_prioridade_desen,
	null dt_ordem_servico,
	c.qt_min_prev,
	c.nr_seq_lp,
	c.nr_sequencia nr_seq_ativ_prev
from   	man_ordem_servico a,
	man_ordem_servico_exec b,
	man_ordem_ativ_prev c
where  	a.nr_sequencia = b.nr_seq_ordem
and	a.nr_sequencia = c.nr_seq_ordem_serv
and	b.dt_fim_execucao is null
and	b.nm_usuario_exec = nm_usuario_fila_w
and	c.nm_usuario_prev = nm_usuario_fila_w
and	c.ie_prioridade_desen > 1
and	a.nr_seq_estagio not in (2,9,791,1234,41,1071) --desenvolvimento aguardando programa��o
and	((ie_atualiza_dt_prev_w = 'S') or ((ie_atualiza_dt_prev_w = 'N') and (c.dt_prevista is null)))
union
select 	2 nr_seq_ordem,
	a.nr_sequencia,
	c.ie_prioridade_desen,
	a.dt_ordem_servico dt_ordem_servico,
	c.qt_min_prev,
	c.nr_seq_lp,
	c.nr_sequencia nr_seq_ativ_prev
from   	man_ordem_servico a,
    	man_ordem_servico_exec b,
	man_ordem_ativ_prev c
where  	a.nr_sequencia = b.nr_seq_ordem
and	a.nr_sequencia = c.nr_seq_ordem_serv
and	b.dt_fim_execucao is null
and	b.nm_usuario_exec = nm_usuario_fila_w
and	c.nm_usuario_prev = nm_usuario_fila_w
and	c.ie_prioridade_desen = 1
and	a.nr_seq_estagio not in (2,9,791,1234,41,1071) --desenvolvimento aguardando programa��o
--and	a.nr_seq_estagio in (1191) --desenvolvimento aguardando programa��o
and	a.ie_classificacao = 'E'
and	((ie_atualiza_dt_prev_w = 'S') or ((ie_atualiza_dt_prev_w = 'N') and (c.dt_prevista is null)))
union
select 	3 nr_seq_ordem,
	a.nr_sequencia,
	null ie_prioridade_desen,
	a.dt_ordem_servico,
	c.qt_min_prev,
	c.nr_seq_lp,
	c.nr_sequencia nr_seq_ativ_prev
from   	man_ordem_servico a,
    	man_ordem_servico_exec b,
	man_ordem_ativ_prev c
where  	a.nr_sequencia = b.nr_seq_ordem
and	a.nr_sequencia = c.nr_seq_ordem_serv
and	b.dt_fim_execucao is null
and	b.nm_usuario_exec = nm_usuario_fila_w
and	c.nm_usuario_prev = nm_usuario_fila_w
and	c.ie_prioridade_desen = 1
and	a.ie_classificacao <> 'E' --defeito
and	a.nr_seq_estagio not in (2,9,791,1234,41,1071) --desenvolvimento aguardando programa��o
--and	a.nr_seq_estagio in (1191) --desenvolvimento aguardando programa��o
and	((ie_atualiza_dt_prev_w = 'S') or ((ie_atualiza_dt_prev_w = 'N') and (c.dt_prevista is null)))
order by	nr_seq_ordem, ie_prioridade_desen desc, dt_ordem_servico;

cursor c02 is
select 	a.nm_usuario_lista
from	lista_usuario_os a
where	nr_seq_fila = nr_seq_fila_w
order by	a.nm_usuario_lista;

cursor c03 is
select	a.nr_sequencia,
	nr_seq_gerencia,
	nm_usuario_fila,
	nvl(ie_atualiza_dt_prev,'N') ie_atualiza_dt_prev,
	ie_pacote_entrega,
	a.qt_min_medio_prev,
	nvl(a.ie_calc_score,'N'),
	nvl(ie_capacidade_skill,'N'),
	nvl(b.cd_estabelecimento,1)
from	cadastro_fila a,
	gerencia_wheb b
where	a.nr_seq_gerencia = b.nr_sequencia
and	a.nr_seq_gerencia	= nr_seq_gerencia_p
and	a.nr_sequencia	= nvl(nr_seq_fila_p, a.nr_sequencia)
and	a.ie_situacao = 'A';	
	
cursor c05 is
select 	d.nr_ordem,
	a.nr_sequencia,
	d.nr_prioridade,
	a.dt_ordem_servico,
	c.qt_min_prev,
	c.nr_seq_lp,
	c.nr_sequencia nr_seq_ativ_prev
from   	man_ordem_servico a,
    	man_ordem_servico_exec b,
	man_ordem_ativ_prev c,
	fila_execucao_os d
where  	a.nr_sequencia = b.nr_seq_ordem
and	a.nr_sequencia = c.nr_seq_ordem_serv
and	b.dt_fim_execucao is null
and	b.nm_usuario_exec = nm_usuario_fila_w
and	c.nm_usuario_prev = nm_usuario_fila_w
--and	a.nr_seq_estagio not in (2,9,791,1234,41,1071)
and	d.nr_seq_ordem_serv = a.nr_sequencia
and	d.dt_referencia		= dt_referencia_w
order by	d.nr_ordem, des_obter_score_ordem_serv(a.nr_sequencia) desc, a.dt_ordem_servico;

cursor c06 is
select	nm_usuario_lista
from	(
select	a.nm_usuario_lista,
	max((select count(*) from skill_desenv_usuario x where x.nm_usuario_des = a.nm_usuario_lista)) qt_skill,
	max(c.ie_nivel_skill) ie_nivel_skill
from	lista_usuario_os a,
	man_ordem_serv_skill b,
	skill_desenv_usuario c
where	a.nm_usuario_lista = c.nm_usuario_des
and	b.nr_seq_skill = c.nr_seq_skill 
and	b.nr_seq_ordem_serv = nr_ordem_serv_w
and	a.nr_seq_fila = nr_seq_fila_w
and	ie_situacao = 'A'
and	ie_calculo_prev = 'S'
and	not exists(	select	1
      	  	from	man_ordem_servico o,
			man_ordem_serv_skill x
		where	o.nr_sequencia = x.nr_seq_ordem_serv
		and	o.nr_sequencia = nr_ordem_serv_w
		and	not exists(	select	1
		 	     	from	skill_desenv_usuario y
				where	y.nm_usuario_des = a.nm_usuario_lista
				and	x.nr_seq_skill = y.nr_seq_skill
				and	(nvl(o.nr_seq_complex,0) <> 4 or decode(o.nr_seq_complex,4,'A','B') = decode(y.ie_nivel_skill,1,'A','B'))))
group by	a.nm_usuario_lista)
order by	qt_skill, ie_nivel_skill;
		
begin

open c03;
loop
fetch c03 into
	nr_seq_fila_w,
	nr_seq_gerencia_w,
	nm_usuario_fila_w,
	ie_atualiza_dt_prev_w,
	ie_pacote_entrega_w,
	qt_min_medio_prev_w,
	ie_calc_score_w,
	ie_capacidade_skill_w,
	cd_estabelecimento_w;
exit when c03%notfound;	
	begin
		
	nr_seq_exec_w	:= 0;
	
	select	count(*)
	into	qt_integrantes_w
	from	lista_usuario_os a
	where	a.nr_seq_fila	= nr_seq_fila_w
	and	ie_calculo_prev	= 'S';
	
	/* disponibilidade da fila de execu��o  qtd de integrantes * tempo previsto de 300 minutos */
	qt_min_capac_prod_diaria_w	:= qt_integrantes_w * 360;
	qt_min_previsto_dia_w		:= 0;
	dt_prevista_ww			:= trunc(sysdate,'dd');
	
	if	(ie_atualiza_dt_prev_w = 'N') then
		begin
		select	trunc(max(dt_prevista),'dd')
		into	dt_prevista_ww
		from	fila_execucao_os a
		where	a.nr_seq_fila	= nr_seq_fila_w
		and	dt_referencia	= trunc(sysdate);
		
		/*limpar as datas previstas das os do �ltimo dia previsto - (filas que n�o reatualizam) */
		
		update	fila_execucao_os
		set	dt_prev_entrega	= null,
			dt_prevista	= null
		where	nr_seq_fila	= nr_seq_fila_w
		and	dt_referencia	= dt_referencia_w
		and	dt_prevista	= dt_prevista_ww;
		
		/* gravar dt_prev_entrega_w na man_ordem_ativ_prev */
		
		update	man_ordem_ativ_prev a
		set	dt_prevista	= null,
			dt_prev_entrega	= null
		where	nm_usuario_prev	= nm_usuario_fila_w
		and	dt_real is null
		and	pr_atividade is null
		and	dt_prevista	= dt_prevista_ww
		and	exists(	select	1
				from	man_ordem_servico_exec y
				where	y.nr_seq_ordem	= a.nr_seq_ordem_serv
				and	y.nm_usuario_exec	= nm_usuario_fila_w
				and	y.dt_fim_execucao is null);
		end;		
	end if;

	if	(nr_seq_gerencia_p = 4) and
		(nr_seq_fila_p = 8) and
		(ie_atualiza_dt_prev_w = 'N') then
		dt_prevista_ww	:= dt_prevista_ww;
	else
		dt_prevista_ww	:= obter_proximo_dia_util(cd_estabelecimento_w, dt_referencia_w);
	end if;
				
	select	nvl(sum(a.qt_min_prev),0)
	into	qt_min_previsto_dia_w
	from	man_ordem_servico c,
		lista_usuario_os b,
		man_ordem_ativ_prev a
	where	c.nr_sequencia	= a.nr_seq_ordem_serv
	and	a.nm_usuario_prev	= b.nm_usuario_lista
	and	a.dt_prevista	= dt_prevista_ww
	and	b.nr_seq_fila	= nr_seq_fila_w
	and	not exists(	select	1
			from	man_ordem_servico_exec y
			where	y.nr_seq_ordem		= c.nr_sequencia
			and	y.nm_usuario_exec	= nm_usuario_fila_w);
	
	vetor_regra_w.delete;
	
	if	(ie_calc_score_w = 'S') then			
		open c05;
		loop
		fetch c05 into	
			nr_seq_ordem_w,
			nr_ordem_serv_w,
			ie_prioridade_w,
			dt_ordem_serv_w,
			qt_min_prev_w,
			nr_seq_lp_w,
			nr_seq_ativ_prev_w;
		exit when c05%notfound;
			begin					
			i:= nvl(vetor_regra_w.count,0) + 1;
			vetor_regra_w(i).nr_seq_ordem_w 	:= nr_seq_ordem_w;
			vetor_regra_w(i).nr_ordem_serv_w	:= nr_ordem_serv_w;
			vetor_regra_w(i).ie_prioridade_w 	:= ie_prioridade_w;
			vetor_regra_w(i).dt_ordem_serv_w	:= dt_ordem_serv_w;
			vetor_regra_w(i).qt_min_prev_w	:= qt_min_prev_w;
			vetor_regra_w(i).nr_seq_lp_w 		:= nr_seq_lp_w;
			vetor_regra_w(i).nr_seq_ativ_prev_w 	:= nr_seq_ativ_prev_w;
			end;
		end loop;
		close c05;			
	else
		open c01;
		loop
		fetch c01 into	
			nr_seq_ordem_w,
			nr_ordem_serv_w,
			ie_prioridade_w,
			dt_ordem_serv_w,
			qt_min_prev_w,
			nr_seq_lp_w,
			nr_seq_ativ_prev_w;
		exit when c01%notfound;
			begin				
			i := nvl(vetor_regra_w.count,0) + 1;
			vetor_regra_w(i).nr_seq_ordem_w 	:= nr_seq_ordem_w;
			vetor_regra_w(i).nr_ordem_serv_w	:= nr_ordem_serv_w;
			vetor_regra_w(i).ie_prioridade_w 	:= ie_prioridade_w;
			vetor_regra_w(i).dt_ordem_serv_w	:= dt_ordem_serv_w;
			vetor_regra_w(i).qt_min_prev_w	:= qt_min_prev_w;
			vetor_regra_w(i).nr_seq_lp_w 		:= nr_seq_lp_w;	
			vetor_regra_w(i).nr_seq_ativ_prev_w 	:= nr_seq_ativ_prev_w;				
			end;
		end loop;
		close c01;
	end if;
	
	if	(ie_capacidade_skill_w = 'S') then
		delete	des_pacote_recurso a
		where	a.dt_referencia >= dt_referencia_w
		and	exists(	select	1
					from	des_pacote_versao x
					where	x.nr_sequencia = a.nr_seq_pacote
					and	x.nr_seq_fila = nr_seq_fila_w);
	end if;
	
	/*
	create table log_sid(nr_seq number(10),ds_log varchar2(255));
	delete log_sid;
	commit;
	*/
	
	for i in 1..vetor_regra_w.count loop
		begin
		nr_seq_ordem_w		:=	vetor_regra_w(i).nr_seq_ordem_w;
		nr_ordem_serv_w		:= 	vetor_regra_w(i).nr_ordem_serv_w;
		ie_prioridade_w		:= 	vetor_regra_w(i).ie_prioridade_w;
		dt_ordem_serv_w		:= 	vetor_regra_w(i).dt_ordem_serv_w;
		qt_min_prev_w		:= 	vetor_regra_w(i).qt_min_prev_w;
		nr_seq_lp_w		:= 	vetor_regra_w(i).nr_seq_lp_w;
		nr_seq_ativ_prev_w	:= 	vetor_regra_w(i).nr_seq_ativ_prev_w;
		qt_min_prev_w		:=	nvl(qt_min_medio_prev_w, qt_min_prev_w);
		qt_min_previsto_dia_w	:=	qt_min_previsto_dia_w + qt_min_prev_w;
		nr_seq_exec_w		:=	nr_seq_exec_w + 1;
		
		if	(ie_capacidade_skill_w = 'S') then
			begin			
			dt_prevista_ww		:= null;
			dt_prev_entrega_w	:= null;
			
			dt_prevista_www	:=	obter_proximo_dia_util(cd_estabelecimento_w, trunc(sysdate,'dd'));
			ie_prox_w	:=	'N';
			
			while	((dt_prevista_ww is null) and (ie_prox_w = 'N')) loop 
				begin	
				nm_usuario_lista_w	:=	null;
				
				/*
				nr_seq_w	:= nr_seq_w+1;			
				insert into log_sid(nr_seq, ds_log) values (nr_seq_w,'A-'||nr_ordem_serv_w||'-'||to_char(dt_prevista_www,'dd/mm/yyyy'));
				commit;
				*/
				
				open c06;
				loop
				fetch c06 into	
					nm_usuario_lista_w;
				exit when ((c06%notfound) or (ie_prox_w = 'S'));
					begin
					/*
					nr_seq_w	:= nr_seq_w+1;			
					insert into log_sid(nr_seq, ds_log) values (nr_seq_w,'B-'||nr_ordem_serv_w||'-'||to_char(dt_prevista_www,'dd/mm/yyyy')||'-nm_usuario_lista_w='||nm_usuario_lista_w);
					commit;
					*/
					
					begin
					select	qt_min_capac,
						qt_min_prev
					into	qt_min_capac_prod_diaria_w,
						qt_min_previsto_dia_w
					from	des_pacote_recurso
					where	dt_referencia = dt_prevista_www
					and	nm_usuario_recurso = nm_usuario_lista_w;
					
					if	((qt_min_previsto_dia_w + qt_min_prev_w) <= qt_min_capac_prod_diaria_w) then
						begin						
						update	des_pacote_recurso
						set	qt_min_prev = qt_min_prev + qt_min_prev_w
						where	dt_referencia = dt_prevista_www
						and	nm_usuario_recurso = nm_usuario_lista_w;
						
						dt_prevista_ww	:=	dt_prevista_www;
						ie_prox_w	:=	'S';
						end;
					end if;
					exception
					when others then
						begin
						des_obter_pacote_versao(
							obter_proxima_versao2(dt_prevista_www),	'Pacote de entrega das OS em fila de programa��o',
							nr_seq_gerencia_w,			nr_seq_fila_w,
							nm_usuario_fila_w,				nr_seq_pacote_entrega_w);
							
						select	nvl(sum(a.qt_min_prev),0)
						into	qt_min_previsto_dia_w
						from	man_ordem_servico c,
							man_ordem_ativ_prev a
						where	c.nr_sequencia	= a.nr_seq_ordem_serv
						and	a.nm_usuario_prev	= nm_usuario_lista_w
						and	a.dt_prevista	= dt_prevista_www;
						
						qt_min_capac_prod_diaria_w	:=	360;
						
						insert into des_pacote_recurso(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_pacote,
							nm_usuario_recurso,
							dt_referencia,
							qt_min_capac,
							qt_min_prev)
						values(	des_pacote_recurso_seq.nextval,
							sysdate,
							nm_usuario_fila_w,
							sysdate,
							nm_usuario_fila_w,
							nr_seq_pacote_entrega_w,
							nm_usuario_lista_w,
							dt_prevista_www,
							qt_min_capac_prod_diaria_w,
							qt_min_previsto_dia_w);
							
						if	((qt_min_previsto_dia_w + qt_min_prev_w) <= qt_min_capac_prod_diaria_w) then
							begin						
							update	des_pacote_recurso
							set	qt_min_prev = qt_min_prev + qt_min_prev_w
							where	dt_referencia = dt_prevista_www
							and	nm_usuario_recurso = nm_usuario_lista_w;
							
							dt_prevista_ww	:=	dt_prevista_www;
							ie_prox_w	:=	'S';
							end;
						end if;
						commit;
						end;
					end;
					
					/*
					nr_seq_w	:= nr_seq_w+1;			
					insert into log_sid(nr_seq, ds_log) values (nr_seq_w,'C-'||nr_ordem_serv_w||'-'||to_char(dt_prevista_www,'dd/mm/yyyy')||'-nm_usuario_lista_w='||nm_usuario_lista_w);
					commit;
					*/
					end;
				end loop;
				close c06;
				
				if	(nvl(nm_usuario_lista_w,'X') = 'X') then
					ie_prox_w	:=	'S';
				end if;
				
				if	(dt_prevista_ww is null) then
					dt_prevista_www	:=	obter_proximo_dia_util(cd_estabelecimento_w, trunc(dt_prevista_www+1,'dd'));
				end if;
				
				/*
				nr_seq_w	:= nr_seq_w+1;			
				insert into log_sid(nr_seq, ds_log) values (nr_seq_w,'D-'||nr_ordem_serv_w||'-'||to_char(dt_prevista_www,'dd/mm/yyyy'));
				commit;
				*/
				end;
			end loop;
			
			if	(dt_prevista_ww is not null) then
				dt_prev_entrega_w	:= obter_proxima_versao2(dt_prevista_ww);
			end if;			
			end;
		else
			begin		
			if	(qt_min_previsto_dia_w > qt_min_capac_prod_diaria_w) then
				dt_prevista_ww		:= obter_proximo_dia_util(cd_estabelecimento_w, trunc(dt_prevista_ww,'dd') + 1);			
				qt_min_previsto_dia_w	:= 0;			
				
				select	nvl(sum(a.qt_min_prev),0)
				into	qt_min_previsto_dia_w
				from	man_ordem_servico c,
					lista_usuario_os b,
					man_ordem_ativ_prev a
				where	c.nr_sequencia	= a.nr_seq_ordem_serv
				and	a.nm_usuario_prev	= b.nm_usuario_lista
				and	a.dt_prevista	= dt_prevista_ww
				and	b.nr_seq_fila	= nr_seq_fila_w
				and	not exists(	select	1
						from	man_ordem_servico_exec y
						where	y.nr_seq_ordem		= c.nr_sequencia
						and	y.nm_usuario_exec	= nm_usuario_fila_w);
				
				qt_min_previsto_dia_w	:= qt_min_previsto_dia_w + qt_min_prev_w;
			end if;			
			dt_prev_entrega_w	:= obter_proxima_versao2(dt_prevista_ww);
			end;
		end if;
		
		/*
		nr_seq_w	:= nr_seq_w+1;			
		insert into log_sid(nr_seq, ds_log) values (nr_seq_w,'E-'||nr_ordem_serv_w||'-'||to_char(dt_prevista_ww,'dd/mm/yyyy'));
		commit;
		*/
		
		if	(ie_atualiza_dt_prev_w = 'S') then
			begin
			update	fila_execucao_os
			set	dt_prev_entrega	= dt_prev_entrega_w,
				dt_prevista	= dt_prevista_ww
			where	nr_seq_ordem_serv	= nr_ordem_serv_w
			and	nr_seq_fila	= nr_seq_fila_w
			and	dt_referencia	= dt_referencia_w;
			
			/* gravar dt_prev_entrega_w na man_ordem_ativ_prev */				
			update	man_ordem_ativ_prev
			set	dt_prevista	= dt_prevista_ww,
				dt_prev_entrega	= dt_prev_entrega_w
			where	nr_sequencia	= nr_seq_ativ_prev_w
			and	nr_seq_ordem_serv	= nr_ordem_serv_w
			and	nm_usuario_prev	= nm_usuario_fila_w
			and	dt_real is null
			and	pr_atividade is null;
			end;
		elsif	(ie_atualiza_dt_prev_w = 'N') then
			begin
			update	fila_execucao_os
			set	dt_prev_entrega	= dt_prev_entrega_w,
				dt_prevista	= dt_prevista_ww
			where	nr_seq_ordem_serv	= nr_ordem_serv_w
			and	nr_seq_fila	= nr_seq_fila_w
			and	dt_referencia	= dt_referencia_w
			and	dt_prevista is null;
			
			/* gravar dt_prev_entrega_w na man_ordem_ativ_prev */				
			update	man_ordem_ativ_prev
			set	dt_prevista	= dt_prevista_ww,
				dt_prev_entrega	= dt_prev_entrega_w,
				nm_usuario	= nm_usuario_fila_w,
				dt_atualizacao	= sysdate
			where	nr_sequencia	= nr_seq_ativ_prev_w
			and	nr_seq_ordem_serv	= nr_ordem_serv_w
			and	nm_usuario_prev	= nm_usuario_fila_w
			and	dt_real is null
			and	pr_atividade is null
			and	dt_prevista is null;
			end;
		end if;
		
		/*if	(dt_prev_entrega_w is not null) then
			begin
			update	man_ordem_servico
			set	dt_versao_prev	= dt_prev_entrega_w
			where	nr_sequencia	= nr_ordem_serv_w;
			exception when others then
				null;
			end;
		end if;*/
		
		if	(ie_pacote_entrega_w = 'S') and 
			(dt_prev_entrega_w is not null) then
			begin
			des_obter_pacote_versao(
				dt_prev_entrega_w,	'Pacote de entrega das OS em fila de programa��o',
				nr_seq_gerencia_w,	nr_seq_fila_w,
				nm_usuario_fila_w,	nr_seq_pacote_entrega_w);
				
			if	(nr_seq_pacote_entrega_w is not null) then
				begin
				-- verifica se o pacote que se est� incluindo a os � o pacote atual de entrega, desta forma a os ser� considerada adicional
				select	count(*)
				into	qt_pacote_atual_w
				from	des_pacote_versao a
				where	a.nr_sequencia = nr_seq_pacote_entrega_w
				and	sysdate between a.dt_inicial and a.dt_final;

				ie_os_adicional_pacote_w := 'N';
				
				if	(qt_pacote_atual_w > 0) then
					ie_os_adicional_pacote_w := 'S';
				end if;

				select	count(*)
				into	qt_registro_w
				from	des_pacote_ordem_serv
				where	nr_seq_pacote	 	= nr_seq_pacote_entrega_w
				and	nr_seq_ordem_serv	= nr_ordem_serv_w;	
				
				if	(qt_registro_w = 0) then
					begin
					insert into des_pacote_ordem_serv(
						nr_sequencia,
						nr_seq_pacote,
						nr_seq_ordem_serv,
						nm_usuario,
						dt_atualizacao,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						ie_os_adicional)
					values(	des_pacote_ordem_serv_seq.nextval,
						nr_seq_pacote_entrega_w,
						nr_ordem_serv_w,
						nm_usuario_fila_w,
						sysdate,
						nm_usuario_fila_w,
						sysdate,
						ie_os_adicional_pacote_w);
					end;
				end if;
				
				end;
			end if;
			end;
		end if;
		commit;
		end;
	end loop;
	end;
end loop;
close c03;

commit;

end gerar_data_entrega_os;
/
