create or replace
procedure inserir_cobranca_escritural(
			nr_seq_cobranca_p	number, 
			nr_titulo_p		number, 
			vl_cobranca_p		number,
			cd_banco_p		number, 
			vl_acrescimo_p		number, 
			vl_desconto_p		number, 
			cd_ocorrencia_p		varchar2,
			qt_dias_instrucao_p	number, 
			nm_usuario_p		varchar2,
			ds_mensagem_p	out	varchar2) is 

cd_agencia_bancaria_w	varchar2(8);
cd_conta_w		banco_estabelecimento.cd_conta%type;
ie_digito_conta_w	varchar2(2);
			
begin

	select	a.cd_agencia_bancaria,
		a.cd_conta,
		nvl(a.ie_digito_conta,null) ie_digito_conta
	into 	cd_agencia_bancaria_w,
		cd_conta_w,
		ie_digito_conta_w
	from	banco_estabelecimento_v a,
		cobranca_escritural b 
	where	b.nr_seq_conta_banco = a.nr_sequencia
	and	b.nr_sequencia = nr_seq_cobranca_p;

	insert into 	titulo_receber_cobr
			(nr_seq_cobranca, 
			nr_titulo, 
			vl_cobranca,
			dt_atualizacao,
			nm_usuario, 
			cd_banco, 
			vl_acrescimo, 
			vl_desconto, 
			cd_ocorrencia,
			cd_agencia_bancaria, 
			nr_conta, 
			ie_digito_conta, 
			nr_seq_ocorrencia_ret, 
			qt_dias_instrucao, 
			nr_sequencia) 
		values	(nr_seq_cobranca_p, 
			nr_titulo_p, 
			vl_cobranca_p,
			sysdate,
			nm_usuario_p, 
			cd_banco_p, 
			vl_acrescimo_p, 
			vl_desconto_p, 
			cd_ocorrencia_p,
			cd_agencia_bancaria_w, 
			cd_conta_w, 
			ie_digito_conta_w, 
			null, 
			qt_dias_instrucao_p, 
			TITULO_RECEBER_COBR_seq.nextval);

commit;

ds_mensagem_p := obter_texto_tasy(175196, wheb_usuario_pck.get_nr_seq_idioma);

end inserir_cobranca_escritural;
/

