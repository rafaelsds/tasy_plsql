create or replace
procedure gerar_conv_meio_externo(
	cd_cgc_p		varchar2,
	nm_tabela_p		varchar2,
	nm_atributo_p		varchar2,
	cd_interno_p		varchar2,
	cd_externo_p		varchar2,
	ie_sistema_externo_p	varchar2,
	nr_seq_regra_p		number,
	ie_envio_receb_p		varchar2,
	nm_usuario_p		varchar2) is 

conversao_meio_externo_w	conversao_meio_externo%rowtype;
begin

begin
select	*
into	conversao_meio_externo_w
from	conversao_meio_externo
where	nm_tabela = nm_tabela_p
and	nm_atributo = nm_atributo_p
and	nr_seq_regra = nr_seq_regra_p
and	ie_envio_receb = ie_envio_receb_p
and	cd_interno = cd_interno_p
and	rownum = 1;
exception
when others then
	conversao_meio_externo_w.nr_sequencia	:=	null;
end;

conversao_meio_externo_w.cd_cgc			:=	cd_cgc_p;
conversao_meio_externo_w.nm_tabela			:=	nm_tabela_p;
conversao_meio_externo_w.nm_atributo			:=	nm_atributo_p;
conversao_meio_externo_w.cd_interno			:=	cd_interno_p;
conversao_meio_externo_w.cd_externo			:=	cd_externo_p;
conversao_meio_externo_w.dt_atualizacao		:=	sysdate;
conversao_meio_externo_w.nm_usuario			:=	nm_usuario_p;
conversao_meio_externo_w.ie_sistema_externo		:=	ie_sistema_externo_p;
conversao_meio_externo_w.nr_seq_regra		:=	nr_seq_regra_p;
conversao_meio_externo_w.ie_envio_receb		:=	ie_envio_receb_p;
conversao_meio_externo_w.nm_apresentacao_ext	:=	null;
conversao_meio_externo_w.cd_dominio			:=	null;

if	(conversao_meio_externo_w.nr_sequencia is not null) then
	update	conversao_meio_externo
	set	row = conversao_meio_externo_w
	where	nr_sequencia = conversao_meio_externo_w.nr_sequencia;
else
	begin
	select	conversao_meio_externo_seq.nextval
	into	conversao_meio_externo_w.nr_sequencia
	from	dual;
	
	insert into conversao_meio_externo values conversao_meio_externo_w;
	end;
end if;

/*n�o pode ter commit*/
end gerar_conv_meio_externo;
/