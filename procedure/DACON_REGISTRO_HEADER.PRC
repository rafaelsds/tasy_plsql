create or replace
procedure DACON_REGISTRO_HEADER (nm_usuario_p	varchar2,
				nr_seq_dacon_p	number)  is

ds_arquivo_w	varchar2(2000);

begin

select	substr('DACONMS ',1,8) --ds_dacon,
	||lpad(0,4,0)--ds_reservado,
	||to_number(to_char(dt_referencia, 'YYYY')) --dt_ano_competencia,
	||lpad(0,4,0) --ds_reservado,
	||ie_indicador
	||substr(obter_cgc_estabelecimento(cd_estabelecimento),1,14)--nr_identificacao
	||'2'--ie_nr_identificacao
	||lpad(nr_versao_pgd,3,0)
	||nvl(rpad(obter_razao_social(obter_cgc_estabelecimento(cd_estabelecimento)),60,' '), (rpad(' ',60,' ')))
	||substr(nfe_obter_dados_pj(obter_cgc_estabelecimento(cd_estabelecimento),'UF'),1,2)
	||lpad(0,10,0)
	||lpad(' ',250,' ')
	||lpad(0,3,0)
	||lpad(0,10,0)
into	ds_arquivo_w
from	dacon
where	nr_sequencia = nr_seq_dacon_p;

insert into w_dacon
	   (NR_SEQUENCIA, 
	   DT_ATUALIZACAO, 
	   NM_USUARIO, 
	   DT_ATUALIZACAO_NREC, 
	   NM_USUARIO_NREC, 
	   DS_ARQUIVO, 
	   NR_LINHA, 
	   IE_TIPO_REGISTRO)
values (w_dacon_seq.nextval,
	    sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_arquivo_w,
		1,
		05 );

commit;

end DACON_REGISTRO_HEADER;
/