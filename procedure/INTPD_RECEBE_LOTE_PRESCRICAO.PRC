create or replace
procedure intpd_recebe_lote_prescricao(
	nr_sequencia_p	in	number,
	xml_p		in	xmltype) is

ap_lote_item_w			ap_lote_item%rowtype;
reg_integracao_w		gerar_int_padrao.reg_integracao_conv;
ie_conversao_w			intpd_eventos_sistema.ie_conversao%type;
nr_seq_projeto_xml_w	intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_sistema_w		intpd_eventos_sistema.nr_seq_sistema%type;
nr_seq_regra_w			conversao_meio_externo.nr_seq_regra%type;
nr_seq_material_w		prescr_material.nr_sequencia%type;
nr_prescricao_w			prescr_medica.nr_prescricao%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
qt_existe_w				number(10);
ie_sistema_externo_w	varchar2(15);
ds_erro_w				varchar2(4000);
i						integer;
cd_barras_w				varchar(255);
qt_minima_regra_w		number(10);
cd_local_estoque_w		ap_lote.cd_local_estoque%type;
cd_motivo_baixa_int_w		number(3);
nr_seq_atepacu_w		atend_paciente_unidade.nr_seq_interno%type;
dt_entrada_unidade_w		date;
nr_seq_regra_subs_w		number(10);
cd_material_w			material.cd_material%type;
qt_material_w			number(13,4);
nr_seq_lote_fornec_w		material_lote_fornec.nr_sequencia%type;
nr_seq_loteagrup_w		number(10);
cd_kit_mat_w			number(5);
ds_validade_w			varchar2(10);
ds_material_w			varchar2(255);
cd_unid_med_w			varchar2(30);
nr_etiqueta_lp_w		varchar2(10);
cd_perfil_w			perfil.cd_perfil%type;
nm_usuario_w			usuario.nm_usuario%type;
nr_seq_item_w			ap_lote_item.nr_sequencia%type;
qt_dispensar_w			ap_lote_item.qt_dispensar%type;
cd_mat_barras_w			varchar2(255);
cd_mat_item_w			ap_lote_item.cd_material%type;
ie_permite_generico_w		varchar2(1);
ie_mesmo_generico_w		varchar2(1);

/*'Efetua a consulta transformando o elemento XML num tipo de tabela'*/
cursor c01 is
select	*
from	xmltable('/STRUCTURE/BATCH' passing xml_p columns
	IE_ACAO				varchar2(1)	path	'IE_ACTION',
	NR_SEQUENCIA			number(10)	path	'NR_SEQUENCE',
	cd_perfil				number(10)	path	'CD_PROFILE',
	xml_itens_lote			xmltype		path	'ITENS_BATCH');

c01_w	c01%rowtype;

/*'Efetua a consulta transformando o elemento XML num tipo de tabela'*/
cursor c02 is
select	*
from	xmltable('/ITENS_BATCH/ITEM_BATCH' passing c01_w.xml_itens_lote columns
	CD_MATERIAL				number(30)			path	'CD_MATERIAL',
	NR_SEQ_MAT_HOR			number(10)			path	'NR_SEQ_MAT_TIME',
	QT_MATERIAL_ATENDIDA	varchar2(40)		path	'QT_SETTLEMENT',
	CD_FORNECEDOR			number(10)			path	'CD_SUPPLIER',
	NR_SEQ_LOTE_FORNEC		varchar2(255)		path	'CD_SUPPLIER_BATCH',
	CD_BARRAS				varchar2(4000)		path	'CD_BAR_CODE',
	CD_MAT_SUBST			number(30)			path	'CD_MAT_SUBSTITUTE');
	
c02_w	c02%rowtype;

begin
/*'Atualiza o status da fila para Em processamento'*/
update	intpd_fila_transmissao
set	ie_status = 'R'
where	nr_sequencia = nr_sequencia_p;
/*'Realiza o commit para nao alterar o status de processamento em caso de rollback por existir consistencia. Existe tratamento de excecao abaixo para colocar o status de erro em caso de falha'*/
commit;

nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
cd_perfil_w := wheb_usuario_pck.get_cd_perfil;
wheb_usuario_pck.set_nm_usuario('WMS');
/*'Inicio de controle de falha'*/
begin

/*'Busca os dados da regra do registro da fila que esta em processamento'*/
select	nvl(b.ie_conversao,'I'),
	nr_seq_sistema,
	nr_seq_projeto_xml,
	nr_seq_regra_conv
into	ie_conversao_w,
	nr_seq_sistema_w,
	nr_seq_projeto_xml_w,
	nr_seq_regra_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;

ie_sistema_externo_w	:=	nr_seq_sistema_w;

/*'Alimenta as informacoes iniciais de controle e consistencia de cada atributo do XML'*/
reg_integracao_w.nr_seq_fila_transmissao	:= nr_sequencia_p;
reg_integracao_w.ie_envio_recebe		:= 'R';
reg_integracao_w.ie_sistema_externo		:= ie_sistema_externo_w;
reg_integracao_w.ie_conversao		:= ie_conversao_w;
reg_integracao_w.nr_seq_projeto_xml		:= nr_seq_projeto_xml_w;
reg_integracao_w.nr_seq_regra_conversao	:= nr_seq_regra_w;
reg_integracao_w.qt_reg_log			:= 0;
reg_integracao_w.intpd_log_receb.delete;


open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin
	wheb_usuario_pck.set_cd_perfil(c01_w.cd_perfil);
	cd_perfil_w := wheb_usuario_pck.get_cd_perfil;
	
	ie_permite_generico_w := nvl(obter_vlr_param_perfil(7029,c01_w.cd_perfil,13),'N');
	ie_mesmo_generico_w := nvl(obter_vlr_param_perfil(7029,c01_w.cd_perfil,77),'N');
	
	consiste_alta_paciente(c01_w.nr_sequencia, null);

	open c02;
	loop
	fetch c02 into	
		c02_w;
	exit when C02%notfound;
		begin
		c02_w.qt_material_atendida := to_number(somente_numero_virg_char(replace(c02_w.qt_material_atendida, '.', ',')));
		cd_mat_barras_w := nvl(c02_w.nr_seq_lote_fornec, nvl(c02_w.cd_barras, nvl(c02_w.cd_mat_subst, c02_w.cd_material)));
		
		select	max(cd_material)
		into	cd_mat_item_w
		from	ap_lote_item
		where 	nr_seq_lote = c01_w.nr_sequencia
		and	nr_seq_mat_hor = c02_w.nr_seq_mat_hor;
		
		select	nvl(max(nr_sequencia),0),
			nvl(sum(qt_dispensar),0)
		into	nr_seq_item_w,
			qt_dispensar_w
		from	ap_lote_item
		where 	nr_seq_lote = c01_w.nr_sequencia
		and 	(cd_material =  c02_w.cd_material
		or 	(ie_permite_generico_w = 'S' and cd_material = Obter_Mat_Generico(c02_w.cd_material))
		or 	(ie_permite_generico_w = 'S' and ie_mesmo_generico_w = 'S' and obter_se_generico_igual(c02_w.cd_material, cd_material) = 'S'));
		
		if	(nr_seq_item_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(179911,'DS_MEDICAMENTO=' ||  nvl(c02_w.cd_mat_subst, c02_w.cd_material));
		elsif	(qt_dispensar_w < c02_w.qt_material_atendida) then
			wheb_mensagem_pck.exibir_mensagem_abort(176853, 'DS_MATERIAL=' || c02_w.cd_material || ';QT_MATERIAL=' || qt_dispensar_w);
		end if;
		
		select	max(cd_local_estoque),
			nvl(max(cd_tipo_baixa),1)
		into	cd_local_estoque_w,
			cd_motivo_baixa_int_w
		from	ap_lote
		where	nr_sequencia = c01_w.nr_sequencia;
		
		converte_codigo_barras(cd_mat_barras_w, 1, null, cd_local_estoque_w,cd_material_w, qt_material_w, nr_seq_lote_fornec_w, nr_seq_loteagrup_w,
					cd_kit_mat_w, ds_validade_w, ds_material_w, cd_unid_med_w, nr_etiqueta_lp_w, ds_erro_w, 7029, null);
		
		if	(ds_erro_w is null) and
			(c02_w.cd_barras is null) and
			(obter_se_leitura_barras(1,cd_material_w,7029) = 'S') then
			wheb_mensagem_pck.exibir_mensagem_abort(271248);
		end if;
		
		if	(ds_erro_w is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_w);
		end if;		
		
		select	max(nr_seq_material),
			max(nr_prescricao)
		into	nr_seq_material_w,
			nr_prescricao_w
		from	prescr_mat_hor
		where	nr_sequencia = c02_w.nr_seq_mat_hor;

		select	max(nr_atendimento)
		into	nr_atendimento_w
		from	prescr_medica
		where	nr_prescricao = nr_prescricao_w;
		
		nr_seq_atepacu_w := obter_atepacu_paciente(nr_atendimento_w,'A');
		
		select 	max(a.dt_entrada_unidade)
		into	dt_entrada_unidade_w
		from	atend_paciente_unidade a
		where	a.nr_seq_interno = nr_seq_atepacu_w;
		
		consiste_material_barras_lote(nr_prescricao_w, nvl(c02_w.cd_mat_subst, c02_w.cd_material), c02_w.qt_material_atendida, nr_seq_lote_fornec_w, 1, cd_local_estoque_w, dt_entrada_unidade_w, cd_motivo_baixa_int_w, c01_w.nr_sequencia, 'Tasy', 'B', ds_erro_w);
		
		if	(ds_erro_w is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_w);
		end if;
		
		if	(nvl(c02_w.cd_mat_subst,0) <> 0) then
			select	count(*)
			into	qt_existe_w
			from	regra_mat_subst_atend
			where 	cd_material = c02_w.cd_material
			and 	cd_material_subst = c02_w.cd_mat_subst;

			if	(qt_existe_w = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(140024);
			end if;
		end if;
		end;
	end loop;
	close c02;
	
	/*'Alimenta as informacoes de controle e consistencia referente ao Elemento a ser processado no momento. E importante manter dentro do cursor e nao fora.'*/	
	reg_integracao_w.nm_tabela		:=	'AP_LOTE';
	reg_integracao_w.nm_elemento	:=	'BATCH';
	reg_integracao_w.nr_seq_visao	:=	null;	
	
	/*'Consiste cada atributo do XML'*/	
	intpd_processar_atributo(reg_integracao_w, 'NR_SEQUENCIA', c01_w.nr_sequencia, 'N', ap_lote_item_w.nr_seq_lote);
		
	open c02;
	loop
	fetch c02 into	
		c02_w;
	exit when c02%notfound;
		begin
		
		select	max(cd_local_estoque),
			nvl(max(cd_tipo_baixa),1)
		into	cd_local_estoque_w,
			cd_motivo_baixa_int_w
		from	ap_lote
		where	nr_sequencia = c01_w.nr_sequencia;
			
		converte_codigo_barras(nvl(c02_w.nr_seq_lote_fornec, nvl(c02_w.cd_barras, nvl(c02_w.cd_mat_subst, c02_w.cd_material))), 1, null, cd_local_estoque_w, 
				cd_material_w, qt_material_w, nr_seq_lote_fornec_w, nr_seq_loteagrup_w,
				cd_kit_mat_w, ds_validade_w, ds_material_w, cd_unid_med_w, nr_etiqueta_lp_w,
				ds_erro_w,7029, null);		
		
		/*'Alimenta as informacoes de controle e consistencia referente ao Elemento a ser processado no momento. E importante manter dentro do cursor e nao fora.'*/
		reg_integracao_w.nm_tabela		:=	'AP_LOTE_ITEM';
		reg_integracao_w.nm_elemento	:=	'ITEM_BATCH';
		reg_integracao_w.nr_seq_visao	:=	null;		
		c02_w.qt_material_atendida := to_number(somente_numero_virg_char(replace(c02_w.qt_material_atendida, '.', ',')));
		
		/*'Consiste cada atributo do XML'*/		
		intpd_processar_atributo(reg_integracao_w, 'CD_MATERIAL', c02_w.cd_material, 'N', ap_lote_item_w.cd_material);
		intpd_processar_atributo(reg_integracao_w, 'QT_MATERIAL_ATENDIDA', c02_w.qt_material_atendida, 'N', ap_lote_item_w.qt_dispensar);
		
		if	(nvl(c02_w.cd_mat_subst,0) <> 0) then
			
			select	max(nr_seq_material)
			into	nr_seq_material_w
			from	prescr_mat_hor
			where	nr_sequencia = c02_w.nr_seq_mat_hor;
			
			select	max(nr_sequencia)
			into	nr_seq_regra_subs_w
			from	regra_mat_subst_atend
			where	cd_material = c02_w.cd_material
			and	cd_material_subst = c02_w.cd_mat_subst
			and	nvl(ie_lote, 'S') = 'S';
			
			substituir_material_ap_lote(c02_w.cd_material,c02_w.cd_mat_subst, c01_w.nr_sequencia, c02_w.qt_material_atendida, nr_seq_regra_subs_w, 'Tasy', nr_seq_lote_fornec_w, null, nr_seq_material_w);
			
			select	max(qt_dispensar)
			into	c02_w.qt_material_atendida
			from	ap_lote_item
			where	nr_seq_lote = c01_w.nr_sequencia
			and	nr_seq_mat_hor = c02_w.nr_seq_mat_hor;
			
		end if;
		
		if	(reg_integracao_w.qt_reg_log = 0) then
			begin
			
			select	max(nr_seq_material),
					max(nr_prescricao)
			into	nr_seq_material_w,
					nr_prescricao_w
			from	prescr_mat_hor
			where	nr_sequencia = c02_w.nr_seq_mat_hor;
		
			select	max(nr_atendimento)
			into	nr_atendimento_w
			from	prescr_medica
			where	nr_prescricao = nr_prescricao_w;
					
			nr_seq_atepacu_w := obter_atepacu_paciente(nr_atendimento_w,'A');
			
			select 	max(a.dt_entrada_unidade)
			into	dt_entrada_unidade_w
			from	atend_paciente_unidade a
			where	a.nr_seq_interno = nr_seq_atepacu_w;
			
			baixar_material_barras_lote(nr_prescricao_w, nvl(c02_w.cd_mat_subst, c02_w.cd_material), c02_w.qt_material_atendida, nr_seq_lote_fornec_w, 1, cd_local_estoque_w, 
					dt_entrada_unidade_w, cd_motivo_baixa_int_w, 'S', c01_w.nr_sequencia, nr_seq_material_w, 'B', 'Tasy', null, 7029);
			
			if	(ds_erro_w is null) then
				update	intpd_fila_transmissao
				set	ie_status = 'S',
					nr_seq_documento = ap_lote_item_w.nr_seq_lote
				where	nr_sequencia = nr_sequencia_p;
			else
				reg_integracao_w.intpd_log_receb(reg_integracao_w.qt_reg_log).ds_log	:=	substr(ds_erro_w,1,4000);
				reg_integracao_w.qt_reg_log						:=	reg_integracao_w.qt_reg_log + 1;
			end if;
			
			end;
		end if;
		
		end;
	end loop;
	close c02;
	end;
end loop;
close c01;

atualizar_lote_prescricao(c01_w.nr_sequencia,'AF',nvl(nm_usuario_w,'Tasy'),'WMS');

exception
when others then
	begin
	ds_erro_w	:=	substr(sqlerrm,1,4000);
	rollback;
	update	intpd_fila_transmissao
	set	ie_status = 'E',
		ds_log = ds_erro_w
	where	nr_sequencia = nr_sequencia_p;
	end;
end;

if	(reg_integracao_w.qt_reg_log > 0) then
	begin
	rollback;
	
	update	intpd_fila_transmissao
	set	ie_status = 'E',
		ds_log = ds_erro_w
	where	nr_sequencia = nr_sequencia_p;
	
	for i in 0..reg_integracao_w.qt_reg_log-1 loop
		intpd_gravar_log_recebimento(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');
	end loop;
	end;
end if;

commit;

wheb_usuario_pck.set_nm_usuario(nm_usuario_w);
wheb_usuario_pck.set_cd_perfil(cd_perfil_w);

end intpd_recebe_lote_prescricao;
/
