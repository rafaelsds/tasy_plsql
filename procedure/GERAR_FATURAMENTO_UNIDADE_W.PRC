CREATE OR REPLACE PROCEDURE GERAR_FATURAMENTO_UNIDADE_W (	cd_setor_atendimento_p	number,
									cd_convenio_p			number,
									dt_inicial_p			date,
									dt_final_p			date )

IS

nr_atendimento_w		number(10);
cd_setor_atendimento_w		number(5);
cd_convenio_w			number(5,0);
cd_proc_mat_w			number(15);
ie_origem_proced_w		number(10);
ds_proc_mat_w			procedimento.ds_procedimento%TYPE;
qt_proc_mat_w			number(9,3);
vl_proc_mat_w			number(15,2);
vl_custo_w			number(15,3);
vl_diferenca_w			number(15,3);
ds_comando_w			varchar2(2000);
ie_proc_mat_w			varchar2(1);
ie_insere_w			varchar(1);

Cursor C01 is
Select	'P' ie_proc_mat,
	b.nr_atendimento,
	obter_convenio_atendimento(b.nr_atendimento) cd_convenio,
	c.cd_procedimento cd_proc_mat,
	c.ie_origem_proced,
	obter_descricao_procedimento(c.cd_procedimento,c.ie_origem_proced) ds_proc_mat,
	sum(nvl(c.qt_procedimento,0)) qt_proc_mat,
	sum(nvl(c.vl_procedimento,0)) vl_proc_mat,
	0 vl_custo,
	sum(nvl(c.vl_procedimento,0)) vl_diferenca
from	procedimento_paciente c,
	atend_paciente_unidade a,
	atendimento_paciente b
where	a.nr_atendimento = b.nr_atendimento - 0
and	b.nr_atendimento = c.nr_atendimento
and	a.cd_setor_atendimento = 12
and	b.dt_alta is not null
and	b.dt_alta >= dt_inicial_p
and	b.dt_alta <= dt_final_p
and	((obter_convenio_atendimento(b.nr_atendimento) = cd_convenio_p) or (cd_convenio_p is null))
group by	b.nr_atendimento,
		c.cd_procedimento,
		c.ie_origem_proced
union
Select	'M' ie_proc_mat,
	b.nr_atendimento,
	obter_convenio_atendimento(b.nr_atendimento) cd_convenio,
	c.cd_material cd_proc_mat,
	0 ie_origem_proced,
	obter_desc_material(c.cd_material) ds_proc_mat,
	sum(nvl(c.qt_material,0)) qt_proc_mat,
	sum(nvl(c.vl_material,0)) vl_proc_mat,
	nvl(obter_valor_ultima_compra(b.cd_estabelecimento,365,c.cd_material,null,'N'),0) vl_custo,
	nvl((sum(c.vl_material) - sum(obter_valor_ultima_compra(b.cd_estabelecimento,365,c.cd_material,null,'N'))),0) vl_diferenca
from	material_atend_paciente c,
	atend_paciente_unidade a,
	atendimento_paciente b
where	a.nr_atendimento = b.nr_atendimento - 0
and	b.nr_atendimento = c.nr_atendimento
and	a.cd_setor_atendimento = 12
and	b.dt_alta is not null
and	b.dt_alta >= dt_inicial_p
and	b.dt_alta <= dt_final_p
and	((obter_convenio_atendimento(b.nr_atendimento) = cd_convenio_p) or (cd_convenio_p is null))
and	c.cd_material is not null
and	obter_tipo_material(c.cd_material,'C') = 1
group by	b.nr_atendimento,
		c.cd_material,
		b.cd_estabelecimento
order by	2;

BEGIN

begin

EXEC_SQL_DINAMICO('TASY','drop table faturamento_unidade_w');
EXEC_SQL_DINAMICO('TASY','create table faturamento_unidade_W (nr_atendimento number(10), cd_setor_atendimento number(5),
			  cd_convenio number(5), ie_proc_mat varchar2(1), cd_proc_mat number(15), ie_origem_proced number(1), ds_proc_mat varchar2(240),
			  qt_proc_mat number (9,3), vl_proc_mat number (15,2), vl_custo number (15,2), vl_diferenca number(15,2))');
end;

OPEN C01;
LOOP
	fetch C01 into	ie_proc_mat_w,
			nr_atendimento_w,
			cd_convenio_w,
			cd_proc_mat_w,
			ie_origem_proced_w,
			ds_proc_mat_w,
			qt_proc_mat_w,
			vl_proc_mat_w,
			vl_custo_w,
			vl_diferenca_w;
	exit when C01%notfound;

/* Setor atendimento atual */
Select	obter_setor_atepacu(obter_atepacu_paciente(nr_atendimento_w,'IAA'),0)
into	cd_setor_atendimento_w
from	dual;

	if	(cd_setor_atendimento_p is not null) and (cd_setor_atendimento_w <> cd_setor_atendimento_p) then
		ie_insere_w	:= 'N';
	else
		ie_insere_w	:= 'S';
	end if;

	if	(ie_insere_w = 'S') then
		ds_comando_w		:= 	'insert into faturamento_unidade_w values (' || nr_atendimento_w ||', '|| cd_setor_atendimento_w || ', ' ||
						cd_convenio_w || ', ' || chr(39) || ie_proc_mat_w || chr(39) || ', ' ||cd_proc_mat_w || ', ' || ie_origem_proced_w
						|| ', ' || chr(39) || ds_proc_mat_w || chr(39) || ', ' || qt_proc_mat_w || ', ' || replace(to_char(vl_proc_mat_w),',','.')
						|| ', ' || replace(to_char(vl_custo_w),',','.') || ', ' || replace(to_char(vl_diferenca_w),',','.') || ')';

		EXEC_SQL_DINAMICO('TASY',ds_comando_w);
	end if;

end loop;
close C01;

commit;

END GERAR_FATURAMENTO_UNIDADE_W;
/
