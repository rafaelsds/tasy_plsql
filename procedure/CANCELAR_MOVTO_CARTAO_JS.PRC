create or replace
procedure cancelar_movto_cartao_js(
		dt_cancelamento_p		date,
		nr_sequencia_p		number,
		nm_usuario_p		varchar2) is 
begin
update  movto_cartao_cr 
set    	dt_cancelamento 	=  to_date(to_char(dt_cancelamento_p, 'dd/mm/yyyy'), 'dd/mm/yyyy'),
        	nm_usuario	=  nm_usuario_p,
	dt_atualizacao  	=  sysdate,
	ds_observacao  	=  wheb_mensagem_pck.get_texto(304652,'DT_PARAMETRO='||to_char(sysdate,'dd/mm/yyyy')) --'Cancelada movimentação em ' || to_char(sysdate,'dd/mm/yyyy')
where  	nr_sequencia   	=  nr_sequencia_p;
commit;
end cancelar_movto_cartao_js;
/