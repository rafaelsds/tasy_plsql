create or replace
procedure integracao_informatec_cinout(		nr_seq_interno_p	number,
						ie_tipo_comando_p	varchar2,
						nr_atendimento_p	number,
						nr_ramal_p		number,
						nm_usuario_p		varchar2) is 

arq_texto_w		utl_file.file_type;
nm_local_grav_w		varchar2(60);		  --Caminho onde ser� salvo o arquivo   **** CAMINHO V�LIDO, HABILITADO NO ORACLE ****			
nm_pessoa_fisica_w	varchar2(60);
ds_espacamento_w	varchar2(1) := ' ';
nm_arquivo_w		varchar2(20);

ie_aberto_w		varchar2(1);
cd_estabelecimento_w	number(4);

qt_tentativas_w		number(6) := 0;

/*
ie_tipo_comando_p =       CIN   ou   COUT
*/
				
begin

if 	(nr_atendimento_p > 0) then

	select	max(substr(obter_nome_pf(cd_pessoa_fisica),1,31)),
		max(cd_estabelecimento)
	into	nm_pessoa_fisica_w,
		cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento 		= nr_atendimento_p;


	select	Obter_Valor_Param_Usuario(3111, 74, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)
	into	nm_local_grav_w
	from	dual;


	if 	(ie_tipo_comando_p = 'CIN') then
		ds_espacamento_w	:= '0';
		nm_arquivo_w		:= 'CInCOut.txt';
	elsif 	(ie_tipo_comando_p = 'COUT') then
		ds_espacamento_w	:= ' ';
		nm_arquivo_w		:= 'CInCOut.txt';
	end if;

	ie_aberto_w := 'N';

	while 	(ie_aberto_w = 'N') and (qt_tentativas_w < 500) loop
		begin
		qt_tentativas_w := qt_tentativas_w + 1;
		
		--abre o arquivo
		arq_texto_w := utl_file.fopen(nm_local_grav_w,nm_arquivo_w , 'A');
			
		ie_aberto_w := 'S';
		exception
		when others then
		
		ie_aberto_w := 'N';
		end;
			
	end loop;


	if 	(qt_tentativas_w < 500) then

		--gera uma nova linha no arquivo
		utl_file.put_line(arq_texto_w, 	Completa_tam_campo(ie_tipo_comando_p,' ',4)||' '||		--Comando	
						Completa_tam_campo_ant(to_char(nvl(nr_ramal_p,0)),'0',5)||' '||	--Ramal do h�spede
						'1'||' '||							--Idioma = 1 = portugu�s
						Completa_tam_campo(nm_pessoa_fisica_w,' ',32)||				--Nome do h�spede
						Completa_tam_campo('1',' ',5)||' '||				--Grupo para despertador
						Completa_tam_campo(' ',' ',6)||' '||				--Senha para caixa postal
						' '||' '||							--Tipo de aviso sobre mensagem nova 
						'1'||' '||							-- Tipo do ramal  - 1 = h�spede
						'N'||' '||							--Apagar (A) ou n�o apagar (N) a mensagenm automaticamente ap�s ser ouvida
						'N'||' '||							--Usar (S) ou n�o usar (N) sauda��o bilingue
						'N'||' '||							--Bypassar (S) ou n�o-bypassar (N) aviso de n�o-perturbe
						'     '||' '||							--Ramal associado 1
						'     '||' '||							--Ramal associado 2
						'0'||' '||							--Indica se o ramal � VIP 
						Completa_tam_campo_ant(to_char(nr_seq_interno_p),'0',10)||' '||	--N�mero sequencial do registro, dado pelo software externo
						'N'||								--Transferir  (S) ou n�o (N) a liga��o para o transbordo quando for ramal de h�spede e n�o houver recado na caixa postal
						'N'||' '||							--Usar (S) ou n�o (N) o time stamp automatico quando apresentar os recados
						Completa_tam_campo(' ',' ',15)||				--Espa�o em branco
						Completa_tam_campo_ant(' ',' ',5)||' '||			--N�mero da caixa postal (ser� suposto igual a RRRRR se n�o constar do comando-texto)
						Completa_tam_campo_ant('1',ds_espacamento_w,2)||' '||		--Tipo da transfer�ncia (direta para ramal, direta para CP etc) quando CVOZ for atendedor
						'   '||' '||							--Categoria a ser usada no check-in (serve para se programar categoria no PABX)
						'   '||' '||							--Categoria a ser usada no check-out (serve para se programar categoria no PABX)
						Completa_tam_campo_ant(' ',' ',7)||' '||			--Ramal pessoal do servi�o de quarto (copa)
						Completa_tam_campo_ant(' ',' ',10)||' '||			--N�mero do apartamento
						'     '||' '||							--Ramal associado 3
						'     '||' '||							--Ramal associado 4
						'     '||' '||							--Ramal associado 5
						'     '||' '||							--Ramal associado 6
						'N'||' '||							--Como h�spede pode utilizar a facilidade de Check-Out Expresso: permitido (S), n�o-permitido (N) ou apenas consulta (C)
						chr(13));
					
		--fecha e libera o arquivo
		utl_file.fclose(arq_texto_w);
	
	end if;

end if;


end integracao_informatec_cinout;
/