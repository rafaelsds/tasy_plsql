create or replace procedure NUT_LIB_SERV_REFEICAO_HTML( nr_seq_cardapios_p	varchar2,
						nm_usuario_p	varchar2) is
  
ds_cardapios_w		VARCHAR2(4000);
nr_cardapio_w		VARCHAR2(20);
nr_posicao_str_w 	number(10);

begin

if	( nr_seq_cardapios_p is not null ) then

	ds_cardapios_w := nr_seq_cardapios_p;

	while	( ds_cardapios_w is not null or trim(ds_cardapios_w) <> '' ) loop

		nr_posicao_str_w := inStr(ds_cardapios_w,', ');

		if(nr_posicao_str_w = 0) then
			nr_cardapio_w := trim(ds_cardapios_w);
			ds_cardapios_w := null;
		else
			nr_cardapio_w := substr(ds_cardapios_w, 1, nr_posicao_str_w - 1);
			ds_cardapios_w := substr(ds_cardapios_w, nr_posicao_str_w + 2);
		end if;

		update	nut_cardapio_dia
			set  dt_liberacao  = sysdate,
			nm_usuario_liberacao  = nm_usuario_p
		where 	nr_sequencia  = nr_cardapio_w;
		
		update	nut_pac_opcao_rec
			set dt_liberacao  = sysdate
		where 	nr_seq_cardapio_dia  = nr_cardapio_w;
 
	end loop;

end if;

commit;

end NUT_LIB_SERV_REFEICAO_HTML;
/
