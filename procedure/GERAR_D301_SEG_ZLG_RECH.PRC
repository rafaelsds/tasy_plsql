create or replace 
procedure gerar_d301_seg_zlg_rech( 	nr_conta_interna_p  in conta_paciente.nr_interno_conta%type,
					nr_seq_dataset_p       number,
					nm_usuario_p           varchar2) is 

nr_seq_301_indic_pag_w	d301_segmento_zlg.nr_seq_301_indic_pag%type;
nr_seq_dataset_ret_w 	d301_segmento_zlg.nr_seq_dataset_ret%type := null;

cursor c01 is
select 	nvl(cp.dt_mesano_referencia,cp.dt_acerto_conta) as dt_pagamento,
	nvl(sum(pp.vl_procedimento),0)                  as vl_coparticipacao,
	cp.ie_status_acerto,
	pp.cd_pessoa_fisica,
	rla.qt_max_taxas
from 	procedimento_paciente pp,
	regra_lanc_automatico rla,
	conta_paciente cp
where 	cp.nr_interno_conta	= nr_conta_interna_p
and 	cp.nr_atendimento    	= pp.nr_atendimento
and 	pp.nr_seq_regra_lanc 	= rla.nr_sequencia
and 	rla.nr_seq_evento    	= 593
group by nvl(cp.dt_mesano_referencia,cp.dt_acerto_conta),
	cp.ie_status_acerto,
	pp.cd_pessoa_fisica,
	rla.qt_max_taxas;
 
c01_w c01%rowtype;

begin

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	nr_seq_301_indic_pag_w := obter_301_pagamento_copart(	c01_w.cd_pessoa_fisica,
								c01_w.dt_pagamento,
								c01_w.vl_coparticipacao,
								c01_w.ie_status_acerto,
								c01_w.qt_max_taxas);

	insert into d301_segmento_zlg 
		(nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		vl_coparticipacao, 
		nr_seq_301_indic_pag, 
		nr_seq_dataset, 
		nr_seq_dataset_ret) 
	values (d301_segmento_zlg_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		c01_w.vl_coparticipacao,
		nr_seq_301_indic_pag_w,
		nr_seq_dataset_p,
		nr_seq_dataset_ret_w); 

end loop;
close C01;
  
end gerar_d301_seg_zlg_rech;
/