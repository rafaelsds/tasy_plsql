create or replace
procedure inserir_metodo_nf_cred(nr_seq_nf_credito_p	number,
				   nr_sequencia_p	number,
				   nm_usuario_p         varchar2) is 


cd_tipo_recebimento_w		tipo_recebimento.cd_tipo_recebimento%type;
nr_seq_nf_orig_w   		nf_credito.nr_seq_nf_orig%TYPE;
ie_tipo_relacao_w               nf_credito.ie_tipo_relacao%TYPE;
qtd_w				number(10);

CURSOR  c05 IS
SELECT  cd_tipo_recebimento
FROM	fis_metodo_pagamento
WHERE   nr_seq_nota = nr_seq_nf_orig_w;

begin

select count(*)
into   qtd_w
from   fis_metodo_pagamento
where  nr_seq_nota = nr_sequencia_p;


if (qtd_w = 0) then

	SELECT	max(ie_tipo_relacao),
		max(nr_seq_nf_orig)
	INTO	ie_tipo_relacao_w,
		nr_seq_nf_orig_w
	FROM	nf_credito
	WHERE	nr_sequencia = nr_seq_nf_credito_p;

	IF ((ie_tipo_relacao_w IS NULL) OR (ie_tipo_relacao_w <> '07')) THEN

		OPEN	
		c05;
		LOOP
		FETCH c05 INTO
		cd_tipo_recebimento_w;
		EXIT WHEN c05%NOTFOUND;
		BEGIN

		inserir_metodo_pagamento(cd_tipo_recebimento_w,nr_sequencia_p,nm_usuario_p);

		END;
		END LOOP;

	CLOSE c05;

	end if;
end if;

commit;

end inserir_metodo_nf_cred;
/
