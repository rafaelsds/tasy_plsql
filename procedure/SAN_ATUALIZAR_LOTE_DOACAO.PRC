create or replace
procedure san_atualizar_lote_doacao(	nr_sequencia_p  number,
					nr_seq_lote_p  	number,
					nm_usuario_p  	varchar2,
					ie_opcao_p	varchar2 default 'D') is

dt_fabricacao_w			date;
dt_validade_w  			date;
ds_lote_fornec_w		varchar(20);
qt_dias_param_w			number(10);
cd_material_w			number(10);
qt_bolsa_w     			number(10);
nr_seq_antic_w			number(4)  := null;
qt_volume_antic_w		number(4)  := null;
nr_sequencia_bolsa_w  		number(10) := null;
ie_tipo_bolsa_w			san_marca_bolsa.ie_tipo_bolsa%type;
ie_atualiza_dados_bolsa_w	varchar2(1);
ie_tipo_bolsa_cadastro_w	san_doacao.ie_tipo_bolsa%type;
ie_atualiza_kit_aferese_w	varchar2(1);

begin

Obter_Param_Usuario(450,359,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,qt_dias_param_w);
--Permitir atualiza��es dos dados da bolsa, com materiais que n�o est�o vinculados no cadastro "SAN - Marca Bolsa".
Obter_Param_Usuario(450,481,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_atualiza_dados_bolsa_w);
--[296] - Permite atualizar o kit com as informa��es da bolsa na coleta do tipo af�rese
Obter_Param_Usuario(450,296,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_atualiza_kit_aferese_w);

if (ie_opcao_p = 'D') then --Atualiza na san_doacao, forma como sempre foi utilizado

	if (nr_sequencia_p is not null) then

		if (nr_seq_lote_p is not null) then
			
			select  max(dt_fabricacao),
				max(dt_validade),
				max(ds_lote_fornec),
				max(cd_material)
			into 	dt_fabricacao_w,
				dt_validade_w,
				ds_lote_fornec_w,
				cd_material_w
			from 	material_lote_fornec
			where 	nr_sequencia = nr_seq_lote_p;
			
		end if;
		
		select	max(ie_tipo_bolsa)
		into	ie_tipo_bolsa_cadastro_w
		from	san_doacao
		where	nr_sequencia = nr_sequencia_p;
		
		if (cd_material_w is not null) then

			select	count(nr_sequencia)
			into	qt_bolsa_w 
			from	san_marca_bolsa 
			where	cd_material = cd_material_w
			and	nvl(ie_tipo_bolsa, 0) = nvl(ie_tipo_bolsa_cadastro_w, 0)
			and		ie_situacao = 'A';
			
			if (qt_bolsa_w = 1) then
				select	nr_sequencia,
					nr_seq_antic, 
					qt_volume_antic,
					ie_tipo_bolsa
				into	nr_sequencia_bolsa_w,
					nr_seq_antic_w,
					qt_volume_antic_w,
					ie_tipo_bolsa_w
				from	san_marca_bolsa 
				where	cd_material = cd_material_w
				and		ie_situacao = 'A';
			end if;	
			
		end if;	
						
		--Se o parametro [481] estiver para 'N' , Se existir material e n�o existir bolsa vai abortar e Se n?o existir material n�o vai existir bolsa ent�o vai abortar.
		if	(nvl(qt_bolsa_w,0) = 0) and
			(nvl(ie_atualiza_dados_bolsa_w,'S') = 'N') then
			/*N�o � poss�vel atualizar os dados da bolsa com materiais que n�o est�o vinculados
			no cadastro geral "Bolsas de sangue". Par�metro [481].*/
			wheb_mensagem_pck.exibir_mensagem_abort(453041);
		end if;
		
		if	(qt_dias_param_w is null) then	
			update	san_doacao
			set	dt_fabricacao_bolsa = dt_fabricacao_w,
				dt_validade_bolsa = dt_validade_w,
				nr_lote_bolsa = ds_lote_fornec_w,
				nr_marca_bolsa = nvl(nr_sequencia_bolsa_w, nr_marca_bolsa),
				nr_seq_antic = nvl(nr_seq_antic_w, nr_seq_antic),
				qt_volume_antic = nvl(qt_volume_antic_w, qt_volume_antic),
				ie_tipo_bolsa = nvl(ie_tipo_bolsa_w, ie_tipo_bolsa)
			where 	nr_sequencia = nr_sequencia_p;
		elsif	(qt_dias_param_w >= 0) then
			update	san_doacao
			set	dt_fabricacao_bolsa = dt_fabricacao_w,
				dt_validade_bolsa = dt_fabricacao_w + qt_dias_param_w,
				nr_lote_bolsa = ds_lote_fornec_w,
				nr_marca_bolsa = nvl(nr_sequencia_bolsa_w, nr_marca_bolsa),
				nr_seq_antic = nvl(nr_seq_antic_w, nr_seq_antic),
				qt_volume_antic = nvl(qt_volume_antic_w, qt_volume_antic),
				ie_tipo_bolsa = nvl(ie_tipo_bolsa_w, ie_tipo_bolsa)
			where 	nr_sequencia = nr_sequencia_p;
		end if;	
		
		--[296] - Permite atualizar o kit com as informa��es da bolsa na coleta do tipo af�rese
		if	(nvl(ie_atualiza_kit_aferese_w,'N') = 'S') then
			update	san_doacao
			set	nr_lote_kit = nvl(nr_lote_kit, nr_lote_bolsa),
				dt_fabricacao_kit = nvl(dt_fabricacao_kit, dt_fabricacao_bolsa),
				dt_validade_kit_aferese = nvl(dt_validade_kit_aferese, dt_validade_bolsa)
			where 	nr_sequencia = nr_sequencia_p;
		end if;
	end if;
end if;
if (ie_opcao_p = 'L') then
	san_insere_lote_bolsa(nr_sequencia_p,nr_seq_lote_p,null,null,nm_usuario_p,'C');	
end if;	
commit;
end san_atualizar_lote_doacao;
/
