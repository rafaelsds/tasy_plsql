create or replace
procedure aprazar_etapa_solucao (
				ie_tipo_solucao_p		number,
				nr_prescricao_p			number,
				nr_seq_solucao_p		number,
				dt_inicio_p			date,
				nr_seq_motivo_p			number,
				ds_observacao_p			varchar2,
				nm_usuario_p			varchar2,
				ie_somente_gedipa_p		varchar2,
				cd_funcao_origem_p		number,
				ie_gedipa_p			varchar2,
				nr_seq_etapa_p			number,
				ie_gera_kit_sol_acm_p		varchar2,
				nr_seq_assinatura_p			number default 0) is

nr_sequencia_w				number(10,0);
nr_seq_horario_w			number(10,0);
nr_seq_material_w			number(6,0);
cd_material_w				number(6,0);
qt_dispensar_w				number(18,6);
qt_dispensar_hor_w			number(18,6);
cd_unidade_medida_w			varchar2(30);
qt_dose_w					number(18,6);
cd_unidade_medida_dose_w	varchar2(30);
ie_gerar_necessidade_disp_w	varchar2(15);
nr_atendimento_w			number(10,0);
nr_seq_lote_w				number(10,0);
cd_estab_prescr_w			number(4,0);
cd_setor_prescr_w			number(5,0);
nr_prescr_atual_w			number(14,0) := 0;
cd_local_estoque_baixa_w	number(4);
cd_local_estoque_w			number(4,0);
nr_seq_classif_w			number(10,0);
dt_liberacao_w				date;
dt_fim_cpoe_w				date;
dt_suspensao_cpoe_w			date;
dt_limite_especial_w		date;
dt_horario_w				date;
dt_limite_agora_w			date;
ie_classif_nao_padrao_w		varchar2(15);
qt_min_agora_w				number(15,0);
qt_min_especial_w			number(15,0);
ie_classif_urgente_w		varchar2(3);
ie_padronizado_w			varchar2(1);
ie_controlado_w				varchar2(1);
varie_cursor_w				varchar2(1) := 'N';
nr_seq_processo_w			number(10,0);
qt_horas_etapa_w			number(15,4);
dt_etapa_w					date;
dt_inicio_prescr_w			date;
dt_validade_prescr_w		date;
ie_registro_w				number(10,0);
cd_estabelecimento_w		number(10,0);
ie_horario_acm_w			varchar2(2);
ie_horario_sn_w				varchar2(2);
ajustar_disp_hor_farm_w		varchar2(1) := 'N';
ie_acm_w					varchar2(1) := 'N';
ie_sn_w						varchar2(1) := 'N';
dt_prescricao_w				date;
nr_seq_classif_param_w		number(10);
dt_hora_atual_w				date;
ie_gerar_classif_agora_w 	varchar2(1);
ie_dose_especial_w			varchar2(1);
ie_local_estoque_mat_hor_w	varchar2(1);
nr_seq_item_w				number(10);
ds_horario_w				varchar2(4000);
ie_agora_impressao_w		varchar2(15);
nr_seq_turno_hor_ag_w		number(10);
hr_turno_agora_w			varchar2(15);
hr_final_turno_agora_w		varchar2(15);
qt_min_antes_atend_w		number(5);
ie_gerar_processo_w 		varchar2(1);
ie_gerar_todos_sol_w		varchar2(1);
ds_msg_w					varchar2(255);
nr_ocorrencia_w				prescr_material.nr_ocorrencia%type;
cd_pessoa_fisica_w			atendimento_paciente.cd_pessoa_fisica%type;
ie_setor_gedipa_w			varchar2(1);
nr_etapa_sol_w				prescr_mat_hor.nr_etapa_sol%type;
ie_regra_disp_w				prescr_material.ie_regra_disp%type;
ie_gera_sem_certificado_w	varchar2(1 char);
ie_gerar_nec_disp_kit_w		varchar2(1);
qt_horario_w                prescr_mat_hor.qt_horario%type;
ie_tipo_sol_reg_w			varchar2(3 char);
ie_apraz_regra_w			varchar2(1 char);
nr_atendimento_reg_w		prescr_medica.nr_atendimento%type;
dt_inicio_cpoe_w			cpoe_material.dt_liberacao%type;

cursor c01 is
select	1 ie_registro,
	c.nr_sequencia,
	d.cd_material,
	d.qt_dispensar,
	d.qt_dispensar_hor,
	d.cd_unidade_medida,
	d.qt_dose,
	d.cd_unidade_medida_dose,
	a.nr_atendimento,
	a.cd_estabelecimento,
	a.cd_setor_atendimento,
	c.cd_local_estoque,
	c.nr_ocorrencia,
	c.ie_regra_disp
from	prescr_mat_hor d,
	prescr_material c,
	prescr_solucao b,
	prescr_medica a
where	d.nr_prescricao		= c.nr_prescricao
and	d.nr_seq_material	= c.nr_sequencia
and	d.nr_prescricao		= a.nr_prescricao
and	c.nr_prescricao		= b.nr_prescricao
and	c.nr_sequencia_solucao	= b.nr_seq_solucao
and	c.nr_prescricao		= a.nr_prescricao
and	b.nr_prescricao		= a.nr_prescricao
and	b.nr_seq_solucao	= nr_seq_solucao_p
and	a.nr_prescricao		= nr_prescricao_p
and	c.ie_agrupador		= 4
and	nvl(c.ie_suspenso,'N')	<> 'S'
and	b.dt_suspensao		is null
and	a.dt_suspensao		is null
and	Obter_se_horario_liberado(d.dt_lib_horario, d.dt_horario) = 'S'
group by
	c.nr_sequencia,
	d.cd_material,
	d.qt_dispensar,
	d.qt_dispensar_hor,
	d.cd_unidade_medida,
	d.qt_dose,
	d.cd_unidade_medida_dose,
	a.nr_atendimento,
	a.cd_estabelecimento,
	a.cd_setor_atendimento,
	c.cd_local_estoque,
	c.nr_ocorrencia,
	c.ie_regra_disp
union
select	2 ie_registro,
	c.nr_sequencia,
	c.cd_material,
	c.qt_total_dispensar,
	null,--c.qt_dispensar_hor,
	c.cd_unidade_medida,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	a.nr_atendimento,
	a.cd_estabelecimento,
	a.cd_setor_atendimento,
	c.cd_local_estoque,
	c.nr_ocorrencia,
	c.ie_regra_disp
from	prescr_material c,
	prescr_solucao b,
	prescr_medica a
where	a.nr_prescricao		= b.nr_prescricao
and	c.nr_prescricao		= b.nr_prescricao
and	c.nr_sequencia_solucao	= b.nr_seq_solucao
and	c.nr_prescricao		= a.nr_prescricao
and	b.nr_prescricao		= a.nr_prescricao
and	b.nr_seq_solucao	= nr_seq_solucao_p
and	a.nr_prescricao		= nr_prescricao_p
and	not exists (	
		select	1
		from	prescr_mat_hor x
		where	x.nr_prescricao		= c.nr_prescricao
		and	x.nr_seq_material	= c.nr_sequencia)
and	(((b.ie_acm = 'S') and
	  (nvl(ie_horario_acm_w,'S') = 'N')) or
	 ((b.ie_se_necessario = 'S') and
	  (nvl(ie_horario_sn_w,'S') = 'N')))
and	c.ie_agrupador		= 4
and	nvl(c.ie_suspenso,'N')	<> 'S'
and	b.dt_suspensao		is null
and	a.dt_suspensao		is null
group by 
	c.nr_sequencia,
	c.cd_material,
	c.qt_total_dispensar,
	c.cd_unidade_medida,
	c.qt_dose,
	c.cd_unidade_medida_dose,
	a.nr_atendimento,
	a.cd_estabelecimento,
	a.cd_setor_atendimento,
	c.cd_local_estoque,
	c.nr_ocorrencia,
	c.ie_regra_disp;
	
Cursor C02 is
	select	nr_sequencia,
		ie_classif_urgente,
		ie_controlado,
		ie_padronizado
	from	classif_lote_disp_far
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_situacao = 'A'
	order by ie_classif_urgente,
		ie_controlado desc,
		ie_padronizado desc;

Cursor C21 is
	select	a.nr_sequencia,
		b.cd_local_estoque,
		a.ds_horario,
		a.nr_seq_material,
		c.cd_estabelecimento,
		c.cd_setor_atendimento
	from	prescr_mat_hor a,
		prescr_material b,
		prescr_medica c
	where	a.nr_prescricao	= nr_prescricao_p
	and	a.nr_prescricao = b.nr_prescricao
	and	a.nr_seq_material = b.nr_sequencia
	and	a.nr_prescricao = c.nr_prescricao
	and	a.dt_lib_horario is not null;

begin
if	(ie_tipo_solucao_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) and
	(dt_inicio_p is not null) and
	(nr_seq_etapa_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	if (ie_tipo_solucao_p in (1, 4, 5)) then
		if (ie_tipo_solucao_p = 1) then
			ie_tipo_sol_reg_w := 'SOL';
		elsif (ie_tipo_solucao_p = 4) then
			ie_tipo_sol_reg_w := 'NAN';
		else
			ie_tipo_sol_reg_w := 'NPP';
		end if;
		
		select 	max(a.nr_atendimento)
		into	nr_atendimento_reg_w
		from	prescr_medica a
		where	a.nr_prescricao = nr_prescricao_p;

		ie_apraz_regra_w := obter_apraz_reapraz_regra(	ie_tipo_item_p => ie_tipo_sol_reg_w, 
														nr_atendimento_p => nr_atendimento_reg_w,
														ie_acao_p => 'A',
														nm_usuario_p => nm_usuario_p);
		if (ie_apraz_regra_w <> 'S') then
			-- Voce nao tem permissao para efetuar essa acao. Cadastros gerais / ADEP - Regra permissao de aprazamento/reaprazamento
			wheb_mensagem_pck.exibir_mensagem_abort(1134375);
		end if;
	end if;

	select	dt_inicio_prescr,
			dt_validade_prescr,
			cd_estabelecimento,
			dt_liberacao,
			dt_prescricao,
			nr_atendimento
	into	dt_inicio_prescr_w,
			dt_validade_prescr_w,
			cd_estabelecimento_w,
			dt_liberacao_w,
			dt_prescricao_w,
			nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	
	ie_setor_gedipa_w	:= obter_se_setor_processo_gedipa(obter_unidade_atendimento(nr_atendimento_w,'IAA','CS'));
	
	select	max(a.dt_suspensao),
			max(a.dt_fim),
			max(a.dt_liberacao)
	into	dt_suspensao_cpoe_w,
			dt_fim_cpoe_w,
			dt_inicio_cpoe_w
	from	cpoe_material a,
			prescr_material b
	where	a.nr_sequencia = b.nr_seq_mat_cpoe
	and		b.nr_prescricao = nr_prescricao_p
	and		nr_sequencia_solucao = nr_seq_solucao_p
	and 	nr_seq_substituto is null
	and		ie_agrupador = 4
	and     cd_funcao_origem = 2314;
	
	select	nvl(max(ie_horario_acm),'S'),
			nvl(max(ie_horario_sn),'S'),
			nvl(max(qt_min_agora),0),
			nvl(max(qt_min_especial),0),
			max(ie_classif_urgencia),
			nvl(max(ie_forma_agora), 'N'),
			nvl(max(ie_gerar_todos_sol),'S')
	into	ie_horario_acm_w,
			ie_horario_sn_w,
			qt_min_agora_w,
			qt_min_especial_w,
			ie_classif_nao_padrao_w,
			ie_agora_impressao_w,
			ie_gerar_todos_sol_w
	from	parametros_farmacia
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	obter_param_usuario(1113, 70, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_necessidade_disp_w);
	obter_param_usuario(1113, 498, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, nr_seq_classif_param_w);	
	Obter_Param_Usuario(1113, 529, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_classif_agora_w);
	obter_param_usuario(924,851,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_local_estoque_mat_hor_w);
	obter_param_usuario(1113, 279, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_processo_w);
	obter_param_usuario(1113, 393, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_nec_disp_kit_w);
	
	if	(ie_tipo_solucao_p = 1) and
		(nr_prescricao_p is not null) and
		(nr_seq_solucao_p is not null) then
		begin
		select 	max(ie_acm),
				max(ie_se_necessario)
		into	ie_acm_w,
				ie_sn_w
		from	prescr_solucao
		where	nr_prescricao = nr_prescricao_p
		and		nr_seq_solucao = nr_seq_solucao_p;
		
		end;
	end if;
	
	if	((ie_acm_w = 'S') or
		(ie_sn_w = 'S')) then
		begin
		if	(dt_inicio_p < dt_prescricao_w) and
			(dt_inicio_cpoe_w is null or (dt_inicio_p < dt_inicio_cpoe_w)) then
			--Nao e permitido aprazar o inicio de uma solucao para um horario anterior a data da prescricao
			wheb_mensagem_pck.exibir_mensagem_abort(174199);
		elsif	(dt_inicio_p > dt_validade_prescr_w) then
			--Nao e permitido aprazar o inicio de uma solucao para um horario posterior ao termino da validade da prescricao
			wheb_mensagem_pck.exibir_mensagem_abort(174200);
		elsif (dt_suspensao_cpoe_w is not null) and
			  (dt_inicio_p > dt_suspensao_cpoe_w) then			  
			  -- A data do aprazamento nao pode ser superior a data de suspensao do item: Suspenso em 
			  ds_msg_w	:= wheb_mensagem_pck.get_Texto(820331, null) || ' ' || to_char(dt_suspensao_cpoe_w, pkg_date_formaters.localize_mask('timestamp', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_p)));
			  wheb_mensagem_pck.exibir_mensagem_abort(ds_msg_w);
		elsif (dt_fim_cpoe_w is not null) and
			  (dt_inicio_p > dt_fim_cpoe_w) then			  
			  --O horario informado esta fora do periodo de validade do item na CPOE. Data de fim:
			  ds_msg_w	:= wheb_mensagem_pck.get_Texto(820333, null) || ' ' || to_char(dt_fim_cpoe_w, pkg_date_formaters.localize_mask('timestamp', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_p)));
			  wheb_mensagem_pck.exibir_mensagem_abort(ds_msg_w);
		end if;
		end;	
	else
		begin	
		if	(dt_inicio_p < dt_inicio_prescr_w) then
			--'Nao e permitido aprazar o inicio de uma solucao para um horario anterior ao inicio da validade da prescricao'
			wheb_mensagem_pck.exibir_mensagem_abort(174201);
		elsif	(dt_inicio_p > dt_validade_prescr_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(174202);
		elsif (dt_suspensao_cpoe_w is not null) and
			  (dt_inicio_p > dt_suspensao_cpoe_w) then
			  -- A data do aprazamento nao pode ser superior a data de suspensao do item: Suspenso em 
			  ds_msg_w	:= wheb_mensagem_pck.get_Texto(820331, null) || ' ' || to_char(dt_suspensao_cpoe_w, pkg_date_formaters.localize_mask('timestamp', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_p)));
		elsif (dt_fim_cpoe_w is not null) and
			  (dt_inicio_p > dt_fim_cpoe_w) then
			--O horario informado esta fora do periodo de validade do item na CPOE. Data de fim:
			  ds_msg_w	:= wheb_mensagem_pck.get_Texto(820333, null) || ' ' || to_char(dt_fim_cpoe_w, pkg_date_formaters.localize_mask('timestamp', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_p)));
    		  wheb_mensagem_pck.exibir_mensagem_abort(ds_msg_w);
		end if;
		end;
	end if;
	
	dt_horario_w 		:= to_date(to_char(dt_inicio_p,'dd/mm/yyyy hh24:mi') || ':00','dd/mm/yyyy hh24:mi:ss');
	dt_limite_agora_w	:= dt_liberacao_w + qt_min_agora_w/1440; 
	dt_limite_especial_w	:= dt_liberacao_w + qt_min_especial_w/1440;
	
	if	(nvl(ie_gerar_classif_agora_w,'N') = 'S') then
		dt_hora_atual_w := sysdate ;
		dt_limite_agora_w	:=  dt_hora_atual_w + qt_min_agora_w/1440; 			
	end if;

	if	(ie_tipo_solucao_p = 1) then		
		begin
		dt_etapa_w := dt_inicio_p;
		dt_horario_w := dt_etapa_w;
		
		open c01;
		loop
		fetch c01 into
			ie_registro_w,
			nr_seq_material_w,
			cd_material_w,
			qt_dispensar_w,
			qt_dispensar_hor_w,
			cd_unidade_medida_w,
			qt_dose_w,
			cd_unidade_medida_dose_w,
			nr_atendimento_w,
			cd_estab_prescr_w,
			cd_setor_prescr_w,
			cd_local_estoque_w,
			nr_ocorrencia_w,
			ie_regra_disp_w;
		exit when c01%notfound;
			begin
			varie_cursor_w	:= 'S';
            
            select  nvl(max(a.qt_horario), 0)
            into    qt_horario_w
            from    prescr_mat_hor a,
                    prescr_material b
            where   a.nr_prescricao = b.nr_prescricao
            and     a.nr_seq_material = b.nr_sequencia
            and     b.nr_prescricao = nr_prescricao_p
            and     b.nr_sequencia_solucao = nr_seq_solucao_p
			and		b.cd_material = cd_material_w;
			
			select	nvl(max(substr(Obter_se_medic_controlado(cd_material_w),1,1)),'N'),
					nvl(max(substr(obter_se_material_padronizado(cd_estab_prescr_w,cd_material_w),1,1)),'N')
			  into	ie_controlado_w,
					ie_padronizado_w
			  from	dual;
			
			ie_classif_urgente_w := definir_ie_classif_urgente(dt_horario_w,
                                                      cd_local_estoque_w,
                                                      cd_estabelecimento_w,
                                                      cd_setor_prescr_w,
                                                      dt_inicio_prescr_w,
                                                      ie_classif_nao_padrao_w,
                                                      ie_padronizado_w,
                                                      dt_limite_especial_w,
                                                      dt_limite_agora_w,
                                                      ie_agora_impressao_w);
			
			cd_local_estoque_baixa_w := null;
			
			if	(nvl(cd_setor_prescr_w,0) > 0) then
				select	nvl(max(cd_local_estoque),0)
				into	cd_local_estoque_baixa_w
				from	setor_local
				where	cd_setor_atendimento = cd_setor_prescr_w
				and	ie_loca_estoque_pac = 'S';
				
				if	(cd_local_estoque_baixa_w = 0) then
					cd_local_estoque_baixa_w := null;
				end if;
			end if;
			
			select	nvl(max(nr_etapa_sol),0)
			into	nr_etapa_sol_w
			from	prescr_mat_hor a
			where	a.nr_prescricao = nr_prescricao_p
			and		a.nr_seq_solucao = nr_seq_solucao_p
			and		nvl(a.ie_horario_especial, 'N') <> 'S'
			and		nr_seq_material = nr_seq_material_w;
			
			
			select	prescr_mat_hor_seq.nextval
			into	nr_seq_horario_w
			from	dual;
			
			insert into prescr_mat_hor(
				nr_sequencia,
				nr_seq_digito,
				nr_prescricao,
				nr_seq_material,
				ie_agrupador,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_material,
				qt_dose,
				qt_dispensar,
				qt_dispensar_hor,
				ds_horario,
				dt_horario,
				cd_unidade_medida,
				cd_unidade_medida_dose,
				ie_urgente,
				dt_emissao_farmacia,
				dt_fim_horario,
				dt_suspensao,
				ie_horario_especial,
				qt_hor_reaprazamento,
				nm_usuario_reaprazamento,
				nr_seq_turno,
				dt_disp_farmacia,
				ie_aprazado,
				nr_seq_solucao,
				nr_etapa_sol,
				ie_classif_urgente,
				ie_padronizado,
				ie_controlado,
				dt_lib_horario,
				nr_atendimento,
				cd_local_estoque_baixa,
				qt_horario)
			values (
				nr_seq_horario_w,
				calcula_digito('MODULO11',nr_seq_horario_w),
				nr_prescricao_p,
				nr_seq_material_w, --nr_seq_solucao_p,
				4,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_material_w,
				qt_dose_w,
				qt_dispensar_w,
				nvl(qt_dispensar_hor_w,dividir(qt_dispensar_w,nr_ocorrencia_w)),
				to_char(dt_etapa_w,'hh24:mi'),
				trunc(dt_etapa_w,'mi'),
				cd_unidade_medida_w,
				cd_unidade_medida_dose_w,
				'N',
				null,
				null,
				null,
				'N',
				null,
				null,
				obter_turno_horario_prescr(cd_estab_prescr_w, cd_setor_prescr_w, to_char(dt_etapa_w,'hh24:mi'), cd_local_estoque_w),
				sysdate,
				'S',
				nr_seq_solucao_p,
				nr_seq_etapa_p,
				ie_classif_urgente_w,
				ie_padronizado_w,
				ie_controlado_w,
				sysdate,
				nr_atendimento_w,
				cd_local_estoque_baixa_w,
				qt_horario_w);
						
			update 	prescr_solucao
			set	    nr_etapas = nvl(nr_etapas,0) + 1
			where	nr_prescricao = nr_prescricao_p
			and	    nr_seq_solucao = nr_seq_solucao_p
			and     nvl(ie_acm,'N') = 'S'
			and	    nvl(ie_solucao_especial,'N') = 'N'			
			and		nvl(nr_etapas,0) = nr_etapa_sol_w
			and	    nvl(nr_etapas,0) > 0;
				
			if	(nvl(ie_gera_kit_sol_acm_p, 'N') = 'S') then
				aprazar_itens_dependentes(cd_estabelecimento_w, cd_setor_prescr_w, nr_atendimento_w, nr_prescricao_p, nr_seq_material_w, 0, trunc(dt_etapa_w,'mi'), 'S', nm_usuario_p, null, null, null, null, 'AES');
			end if;
			
			if	(nvl(nr_seq_classif_param_w ,0) > 0)	then
					begin
					update	prescr_mat_hor
					set	nr_seq_classif		= nr_seq_classif_param_w
					where	nr_prescricao		= nr_prescricao_p	
					and	nr_sequencia		= nr_seq_horario_w;
					end;
			else	
				open C02;
				loop
				fetch C02 into	
					nr_seq_classif_w,
					ie_classif_urgente_w,
					ie_controlado_w,
					ie_padronizado_w;
				exit when C02%notfound;
					begin
				
					if	(ie_controlado_w = 'A') and
						(ie_padronizado_w = 'A') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p	
						and	nr_sequencia		= nr_seq_horario_w
						--and	nr_seq_lote		is not null
						and	ie_classif_urgente	= ie_classif_urgente_w;
					elsif	(ie_controlado_w = 'A') and
						(ie_padronizado_w = 'S') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p
						and	ie_padronizado		= 'S'
						and	nr_sequencia		= nr_seq_horario_w
						--and	nr_seq_lote		is not null
						and	ie_classif_urgente	= ie_classif_urgente_w;
					elsif	(ie_controlado_w = 'A') and
						(ie_padronizado_w = 'N') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p
						and	ie_padronizado		= 'N'
						and	nr_sequencia		= nr_seq_horario_w
						--and	nr_seq_lote		is not null
						and	ie_classif_urgente	= ie_classif_urgente_w;
					elsif	(ie_controlado_w = 'N') and
						(ie_padronizado_w = 'A') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p
						and	ie_controlado		= 'N'
						and	nr_sequencia		= nr_seq_horario_w;
						--and	nr_seq_lote		is not null
						--and	ie_classif_urgente	= ie_classif_urgente_w;
					elsif	(ie_controlado_w = 'N') and
						(ie_padronizado_w = 'N') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p
						and	ie_controlado		= 'N'
						and	ie_padronizado		= 'N'
						and	nr_sequencia		= nr_seq_horario_w
						--and	nr_seq_lote		is not null
						and	ie_classif_urgente	= ie_classif_urgente_w;
					elsif	(ie_controlado_w = 'N') and
						(ie_padronizado_w = 'S') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p
						and	ie_controlado		= 'N'
						and	ie_padronizado		= 'S'
						--and	nr_seq_lote		is not null
						and	nr_sequencia		= nr_seq_horario_w
						and	ie_classif_urgente	= ie_classif_urgente_w;
					elsif	(ie_controlado_w = 'S') and
						(ie_padronizado_w = 'A') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p
						and	ie_controlado		= 'S'
						--and	nr_seq_lote		is not null
						and	nr_sequencia		= nr_seq_horario_w
						and	ie_classif_urgente	= ie_classif_urgente_w;
					elsif	(ie_controlado_w = 'S') and
						(ie_padronizado_w = 'N') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p
						and	ie_controlado		= 'S'
						and	ie_padronizado		= 'N'
						--and	nr_seq_lote		is not null
						and	nr_sequencia		= nr_seq_horario_w
						and	ie_classif_urgente	= ie_classif_urgente_w;
					elsif	(ie_controlado_w = 'S') and
						(ie_padronizado_w = 'S') then
						update	prescr_mat_hor
						set	nr_seq_classif		= nr_seq_classif_w
						where	nr_prescricao		= nr_prescricao_p
						and	ie_controlado		= 'S'
						and	ie_padronizado		= 'S'
						--and	nr_seq_lote		is not null
						and	nr_sequencia		= nr_seq_horario_w
						and	ie_classif_urgente	= ie_classif_urgente_w;
					end if;
					end;
				end loop;
				close C02;
			end if;	
				
			/*if	(nr_prescr_atual_w <> nr_prescricao_p) and  OS159707 
				(nvl(nr_seq_horario_w,0) > 0) then
				begin
				Gerar_Lote_Atend_Prescricao(nr_prescricao_p, nr_seq_material_w, nr_seq_horario_w, 'N', nm_usuario_p, 'AIP');
				end;
			end if;*/

			if	(ie_registro_w = 2) and
				((nvl(ie_horario_acm_w,'S') = 'N') or
				(nvl(ie_horario_sn_w,'S') = 'N')) then
				begin
				select	nvl(max(obter_valor_param_usuario(924, 179, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)),'N')
				into	ajustar_disp_hor_farm_w
				from	dual;
				exception
					when others then
					ajustar_disp_hor_farm_w := 'N';
				end;

				begin
				if	(ajustar_disp_hor_farm_w <> 'N') then
					begin
					calcular_dispensar_horario(nr_prescricao_p, nr_seq_horario_w);
					end;
				end if;
				exception
					when others then
					ajustar_disp_hor_farm_w := 'N';
				end;
			end if;			
			/* atualizar prescricao */
			nr_prescr_atual_w := nr_prescricao_p;

			if	((ie_gerar_necessidade_disp_w = 'S') or (ie_regra_disp_w <> 'N')) then
				update	prescr_medica
				set		dt_emissao_farmacia	= null,
						nm_usuario_imp_far	= null
				where	nr_prescricao		= nr_prescricao_p;

				update	prescr_material
				set		cd_motivo_baixa	= 0,
						dt_baixa	= null
				where	nr_prescricao	= nr_prescricao_p
				and		nr_sequencia	= nr_seq_material_w;
				
				if	(ie_gerar_nec_disp_kit_w = 'S') then
					update	prescr_material
					set		cd_motivo_baixa	= 0,
							dt_baixa		= null
					where	nr_prescricao			= nr_prescricao_p
					and		nr_sequencia_diluicao	= nr_seq_material_w;

					update	prescr_material
					set		cd_motivo_baixa	= 0,
							dt_baixa		= null
					where	nr_prescricao	= nr_prescricao_p
					and		nr_seq_kit		= nr_seq_material_w;
				end if;
			end if;

			end;
		end loop;
		close c01;
		
		open C21;
		loop
		fetch C21 into	
			nr_seq_horario_w,
			cd_local_estoque_w,
			ds_horario_w,
			nr_seq_item_w,
			cd_estab_prescr_w,
			cd_setor_prescr_w;
		exit when C21%notfound;
			if	(ie_local_estoque_mat_hor_w	= 'S') then
				
				define_disp_prescr_mat_hor(nr_seq_horario_w, nr_prescricao_p, nr_seq_item_w, obter_perfil_ativo, nm_usuario_p);
			
				select	nvl(max(cd_local_estoque),cd_local_estoque_w)
				into	cd_local_estoque_w
				from	prescr_mat_hor
				where	nr_sequencia	= nr_seq_horario_w;
				
				update	prescr_mat_hor
				set	nr_seq_turno	= nvl(Obter_turno_horario_prescr(cd_estab_prescr_w, cd_setor_prescr_w, ds_horario_w, cd_local_estoque_w),nr_seq_turno)
				where	nr_sequencia	= nr_seq_horario_w;
			end if;
		end loop;
		close C21;

		Gerar_Lote_Atend_Prescricao(nr_prescricao_p, null, 0, 'S', nm_usuario_p, 'AIP');
		
		if	(varie_cursor_w = 'S') and (ie_gerar_processo_w = 'S') then
			begin
			adep_gerar_area_prep(nr_prescricao_p, null, nm_usuario_p);
			gerar_processo_adep_sol(nr_atendimento_w, nr_prescricao_p, nr_seq_solucao_p, nr_seq_etapa_p, trunc(dt_etapa_w,'mi'), nm_usuario_p, nr_seq_processo_w, ie_somente_gedipa_p, cd_funcao_origem_p, 'AIS', ie_setor_gedipa_w, null,null);

			update	prescr_mat_hor a
			set	a.nr_seq_processo	= nr_seq_processo_w
			where	a.nr_prescricao		= nr_prescricao_p
			and	a.dt_horario		= trunc(dt_etapa_w,'mi')
			and	nvl(a.ie_horario_especial,'N') <> 'S'
			and	a.ie_agrupador		= 4
			and 	exists (
					select	1
					from	prescr_material b
					where	b.nr_prescricao		= a.nr_prescricao
					and	b.nr_sequencia		= a.nr_seq_material
					and	b.ie_agrupador		= 4
					and	b.nr_sequencia_solucao	= nr_seq_solucao_p);

			atual_proc_area_kit_comp_sol(nr_seq_processo_w, nr_prescricao_p, nr_seq_solucao_p, trunc(dt_etapa_w,'mi'),null,'N');
			if	(ie_setor_gedipa_w = 'S') then
				begin
				gerar_frac_proc_sol(nr_seq_processo_w, nr_prescricao_p, nr_seq_solucao_p,'S', nm_usuario_p,null,'N');
				end;
			end if;
			end;
		end if;
		dt_etapa_w := dt_etapa_w + qt_horas_etapa_w / 24;
		end;
	end if;
	
	select	max(nr_seq_lote)
	into	nr_seq_lote_w
	from	prescr_mat_hor a
	where	a.nr_prescricao		= nr_prescricao_p
	and	nr_seq_lote		is not null
	and	nr_etapa_sol		= nr_seq_etapa_p
	and 	exists (select	1
			from	prescr_material b
			where	b.nr_prescricao		= a.nr_prescricao
			and	b.nr_sequencia		= a.nr_seq_material
			and	b.ie_agrupador		= 4
			and	b.nr_sequencia_solucao	= nr_seq_solucao_p);
	
	select	prescr_solucao_evento_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into prescr_solucao_evento (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_prescricao,
		nr_seq_solucao,
		nr_seq_material,
		nr_seq_procedimento,
		nr_seq_nut,
		nr_seq_nut_neo,
		ie_forma_infusao,
		ie_tipo_dosagem,
		qt_dosagem,
		qt_vol_infundido,
		qt_vol_desprezado,
		cd_pessoa_fisica,
		ie_alteracao,
		dt_alteracao,
		ie_evento_valido,
		nr_seq_motivo,
		ds_observacao,
		ie_tipo_solucao,
		DT_APRAZAMENTO,
		nr_etapa_evento,
		nr_seq_lote,
		nr_seq_assinatura)
	values (
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_prescricao_p,
		decode(ie_tipo_solucao_p,1,nr_seq_solucao_p,null),
		decode(ie_tipo_solucao_p,2,nr_seq_solucao_p,null),
		decode(ie_tipo_solucao_p,3,nr_seq_solucao_p,null),
		decode(ie_tipo_solucao_p,4,nr_seq_solucao_p,null),
		decode(ie_tipo_solucao_p,5,nr_seq_solucao_p,null),
		null,
		null,
		null,
		null,
		null,
		obter_dados_usuario_opcao(nm_usuario_p, 'C'),
		16,
		sysdate,
		'S',
		nr_seq_motivo_p,
		ds_observacao_p,
		ie_tipo_solucao_p,
		dt_inicio_p,
		nr_seq_etapa_p,
		nr_seq_lote_w,
		nr_seq_assinatura_p);
		
		ie_gera_sem_certificado_w	:= 'N';
		
		if (wheb_assist_pck.get_gerar_sem_certificado = 'S') then
			ie_gera_sem_certificado_w := adep_obter_se_assin_perfil(50592, obter_perfil_ativo); --ADEP - Aprazamento de itens
		end if;
		
		if	((wheb_assist_pck.get_cd_certificado IS NOT NULL) OR
			(ie_gera_sem_certificado_w = 'S')) AND				
			(obter_data_assinatura_digital(nr_seq_assinatura_p) is null) then
			
	        select	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_w;
	
			Gerar_registro_pendente_PEP('ADEP',nr_prescricao_p, cd_pessoa_fisica_w, nr_atendimento_w, nm_usuario_p, 'A', nr_seq_horario_w, 'SOL',null,null,null,null,null,null,null,null,null,null,null,null,null,null,nr_sequencia_w);

		end if;	
		
	end;
end if;
commit;
end aprazar_etapa_solucao;
/
