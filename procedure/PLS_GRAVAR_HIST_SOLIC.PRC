create or replace
procedure pls_gravar_hist_solic
				(nr_seq_solic_lib_p	number,
				 ie_tipo_historico_p	number,
				 ds_observacao_p	varchar2,
				 nm_usuario_p		Varchar2) is 

begin

if 	(ie_tipo_historico_p is not null) then
	
	insert into pls_solic_lib_historico 	(nr_sequencia, 
						nr_seq_solic_lib, 
						dt_atualizacao, 
						nm_usuario, 
						ds_observacao, 
						dt_historico, 
						ie_tipo_historico) 
					values (pls_solic_lib_historico_seq.nextval,
						nr_seq_solic_lib_p,
						sysdate,
						nm_usuario_p,
						ds_observacao_p,
						sysdate,
						ie_tipo_historico_p);
end if;

commit;

end pls_gravar_hist_solic;
/