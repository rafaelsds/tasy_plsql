create or replace
procedure mob_alterar_comunic_interna( nr_sequencia_p		number,
									ds_titulo_p				varchar2,
									ds_comunicado_p			varchar2,
									nm_usuario_destino_p	varchar2,
									ds_perfil_adicional_p	varchar2,
									ds_setor_adicional_p	varchar2,
									ds_grupo_p				varchar2) is

begin

if (nr_sequencia_p is not null) then

	update 	comunic_interna
	set		ds_titulo = ds_titulo_p,
			ds_comunicado = ds_comunicado_p,
			nm_usuario_destino = nm_usuario_destino_p,
			ds_perfil_adicional = ds_perfil_adicional_p,
			ds_setor_adicional = ds_setor_adicional_p,
			ds_grupo = ds_grupo_p
	where	nr_sequencia = nr_sequencia_p;

	commit;	
end if;

end mob_alterar_comunic_interna;
/
