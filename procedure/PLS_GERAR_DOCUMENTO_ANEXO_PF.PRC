create or replace 
procedure pls_gerar_documento_anexo_pf (	
		nr_seq_tipo_doc_pf_p	number,
		ds_arquivo_p		varchar2,
		nm_usuario_p		varchar2) is

begin
if	(nr_seq_tipo_doc_pf_p is not null) and
	(ds_arquivo_p is not null) and
	(nm_usuario_p is not null) then
	begin
	insert into pls_documento_pf (
		nr_seq_tipo_documento, 
		ds_arquivo, 
		cd_pessoa_fisica, 
		nr_sequencia, 
		nr_seq_tipo_doc_pf, 
		dt_atualizacao, 
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values (null,
		ds_arquivo_p, 
		null, 
		pls_documento_pf_seq.nextval,
		nr_seq_tipo_doc_pf_p,
		sysdate,
		nm_usuario_p, 
		sysdate,
		nm_usuario_p); 
	end;
end if;
commit;
end pls_gerar_documento_anexo_pf;
/