create or replace
procedure obter_se_bo_atrasado  (lista_usuarios_p	varchar2)is 

lista_usuarios_w	dbms_sql.varchar2_table;
nr_sequencia_w		sac_boletim_ocorrencia.nr_sequencia%type;	

Cursor C01 is
	select	a.nr_sequencia
	from	sac_boletim_ocor_analise b,
			sac_boletim_ocorrencia a
	where	a.nr_sequencia = b.nr_seq_boletim_ocorrencia
	and		b.dt_liberacao is null
	and		trunc(a.dt_atualizacao_nrec) <= trunc(sysdate-7)
	and		to_number(to_char(a.dt_atualizacao_nrec,'yyyy')) >= 2013;

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin

	lista_usuarios_w := obter_lista_string(lista_usuarios_p, ',');

	for	i in lista_usuarios_w.first..lista_usuarios_w.last loop
				
		insert  into comunic_interna(   
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao,
				nm_usuario_destino)
			values (sysdate,
				'An�lise do Bo '||nr_sequencia_w||' atrasada',
				'O Boletim Nr�'||nr_sequencia_w||' n�o possui data de libera��o informada.',
				'Tasy',
				sysdate,
				comunic_interna_seq.nextval,
				'N',
				sysdate,
				lista_usuarios_w(i));
				
	end loop;
	
	end;
end loop;
close C01;

commit;

end obter_se_bo_atrasado;
/