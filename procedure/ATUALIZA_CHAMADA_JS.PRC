create or replace
procedure atualiza_chamada_js (	nr_atendimento_p	Number,
				nm_usuario_p		Varchar2) is 

begin

update atendimento_paciente              
set    ie_chamado      = 'S'           
where  nr_atendimento  = nr_atendimento_p;

commit;

end atualiza_chamada_js;
/