create or replace
procedure sus_consiste_numero_bpa
		(	nr_interno_conta_p	number,
			nr_bpa_p		varchar2,
			nr_seq_bpa_p		number,
			nm_usuario_p		varchar2,
			ie_tipo_consiste_p	number,
			ds_erro_p		out varchar2) is


cd_estabelecimento_w		Number(4);
ds_detalhe_w			Varchar2(255)	:= '';
nr_aih_consiste13_w		Number(13);
ds_digito_aih13_w		Varchar2(1);
ds_erro_w			Varchar2(255)   := ''; 
qt_digitos_bpa_w		Number(5)	:= 0;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
nr_atend_atual_w		atendimento_paciente.nr_atendimento%type;
cd_pessoa_fisica_w		atendimento_paciente.cd_pessoa_fisica%type;
cd_conv_sus_w			convenio.cd_convenio%type;
ie_consiste_pf_igual_w		varchar2(15) := 'N';
cd_procedimento_w		procedimento.cd_procedimento%type;
cd_proced_atual_w		procedimento.cd_procedimento%type;
ds_texto_atend_w		varchar2(100) := wheb_mensagem_pck.Get_texto(297915);
ds_texto_proc_w			varchar2(100) := wheb_mensagem_pck.Get_texto(281195);
ds_texto_conta_w		varchar2(100) := wheb_mensagem_pck.Get_texto(280625);
nr_doc_convenio_w		procedimento_paciente.nr_doc_convenio%type;
nr_interno_conta_w		procedimento_paciente.nr_interno_conta%type;


Cursor C01 is
	select	cd_procedimento,
		nr_doc_convenio
	from	procedimento_paciente a
	where	nr_interno_conta = nr_interno_conta_p
	and	nvl(a.nr_doc_convenio,'0') <> '0'
	and	a.cd_motivo_exc_conta is null;

BEGIN

/* Obter dados da conta e do procedimento */
select	nvl(max(cd_estabelecimento),1)
into	cd_estabelecimento_w
from	conta_Paciente
where	nr_interno_conta	= nr_interno_conta_p;

begin
select	nvl(qt_digitos_bpa,0)
into	qt_digitos_bpa_w
from	sus_parametros_bpa
where	cd_estabelecimento	= cd_estabelecimento_w;
exception
	when others then
	qt_digitos_bpa_w	:= 0;
end; 

cd_conv_sus_w		:= obter_dados_param_faturamento(cd_estabelecimento_w,'CSUS');
ie_consiste_pf_igual_w 	:= nvl(Obter_valor_param_usuario(1125,107, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');

/* 4 - N�mero da BPA inv�lido */
if	(Sus_Obter_Inco_Ativa(71)) and
	(nr_bpa_p is not null) and
	(qt_digitos_bpa_w <> 0) and
	(length(nr_bpa_p) <> qt_digitos_bpa_w) then
	if      (ie_tipo_consiste_p = 1) then
		--ds_detalhe_w	:= 'BPA: ' || nr_bpa_p || ' - ' || nr_seq_bpa_p;
		ds_detalhe_w	:= wheb_mensagem_pck.get_texto(299669, 'NR_BPA=' ||  nr_bpa_p || ';NR_SEQ_BPA=' || nr_seq_bpa_p);
		sus_gravar_inconsistencia(nr_interno_conta_p, 71, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
	elsif   (ie_tipo_consiste_p = 2) then
        	ds_erro_w	:= ds_erro_w || '71, ';
	end if;
end if;

if	(sus_obter_inco_atv_conta(298,nr_interno_conta_p,null)) then
	begin
	
	begin
	select	nr_atendimento
	into	nr_atend_atual_w
	from	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;	
	exception
	when others then
		nr_atend_atual_w := 0;
	end;	
	
	cd_pessoa_fisica_w := obter_pessoa_atendimento(nr_atendimento_w,'C');
		
	begin
	select	c.nr_atendimento,
		a.cd_procedimento
	into	nr_atendimento_w,
		cd_procedimento_w
	from    procedimento_paciente a,
		conta_paciente b,
		atendimento_paciente c
	where   a.nr_interno_conta = b.nr_interno_conta 	
	and     b.nr_atendimento = c.nr_atendimento
	and	a.cd_motivo_exc_conta is null
	and	c.ie_tipo_atendimento <> 1
	and     a.nr_doc_convenio  = nr_bpa_p 
	and     nvl(a.nr_doc_convenio,'0') <> '0'
	and	b.cd_convenio_parametro = cd_conv_sus_w
	and     c.nr_atendimento <> nr_atend_atual_w 
	and     ((c.cd_pessoa_fisica <> cd_pessoa_fisica_w) or 
		(ie_consiste_pf_igual_w = 'S'))
	and	rownum = 1;	
	exception
	when others then
		nr_atendimento_w	:= 0;
		cd_procedimento_w	:= 0;
	end;	
	
	if	(nvl(nr_atendimento_w,0) = 0) then
		begin
		
		open C01;
		loop
		fetch C01 into	
			cd_proced_atual_w,
			nr_doc_convenio_w;
		exit when C01%notfound;
			begin
			
			begin
			select	c.nr_atendimento,
				a.cd_procedimento,
				b.nr_interno_conta
			into	nr_atendimento_w,
				cd_procedimento_w,
				nr_interno_conta_w
			from    procedimento_paciente a,
				conta_paciente b,
				atendimento_paciente c
			where   a.nr_interno_conta = b.nr_interno_conta 
			and     b.nr_atendimento = c.nr_atendimento
			and	a.cd_motivo_exc_conta is null
			and	b.nr_interno_conta = nr_interno_conta_p
			and	a.cd_procedimento <> cd_proced_atual_w
			and     a.nr_doc_convenio  = nr_doc_convenio_w 
			and     nvl(a.nr_doc_convenio,'0') <> '0'
			and	b.cd_convenio_parametro = cd_conv_sus_w
			and     c.nr_atendimento = nr_atend_atual_w 	
			and	rownum = 1;			
			exception
			when others then
				begin
				select	c.nr_atendimento,
					a.cd_procedimento,
					b.nr_interno_conta
				into	nr_atendimento_w,
					cd_procedimento_w,
					nr_interno_conta_w
				from    procedimento_paciente a,
					conta_paciente b,
					atendimento_paciente c
				where   a.nr_interno_conta = b.nr_interno_conta 	
				and     b.nr_atendimento = c.nr_atendimento
				and	a.cd_motivo_exc_conta is null
				and	b.nr_interno_conta <> nr_interno_conta_p
				and     a.nr_doc_convenio  = nr_doc_convenio_w 
				and     nvl(a.nr_doc_convenio,'0') <> '0'
				and	b.cd_convenio_parametro = cd_conv_sus_w
				and     c.nr_atendimento = nr_atend_atual_w 	
				and	rownum = 1;	
				exception
				when others then
					nr_atendimento_w	:= 0;
					cd_procedimento_w	:= 0;
				end;
			end;
			
			if	(nvl(nr_atendimento_w,0) = 0) then
				begin
				
				begin
				select	c.nr_atendimento,
					b.nr_interno_conta,
					a.cd_procedimento
				into	nr_atendimento_w,
					nr_interno_conta_w,
					cd_procedimento_w
				from    procedimento_paciente a,
					conta_paciente b,
					atendimento_paciente c
				where   a.nr_interno_conta = b.nr_interno_conta 	
				and     b.nr_atendimento = c.nr_atendimento
				and	a.cd_motivo_exc_conta is null
				and	c.ie_tipo_atendimento <> 1
				and     a.nr_doc_convenio  = nr_doc_convenio_w 
				and     nvl(a.nr_doc_convenio,'0') <> '0'
				and	b.cd_convenio_parametro = cd_conv_sus_w
				and     c.nr_atendimento <> nr_atend_atual_w 
				and     ((c.cd_pessoa_fisica <> cd_pessoa_fisica_w) or 
					(ie_consiste_pf_igual_w = 'S'))
				and	rownum = 1;	
				exception
				when others then
					nr_atendimento_w	:= 0;
					cd_procedimento_w	:= 0;
				end;
				
				end;
			end if;				
			
			end;
		end loop;
		close C01;
		
		end;
	else
		begin
		select	a.cd_procedimento
		into	cd_proced_atual_w
		from	procedimento_paciente a		
		where	a.nr_interno_conta = nr_interno_conta_p
		and     a.nr_doc_convenio  = nr_bpa_p;
		end;
	end if;
	
	
	if	(nvl(nr_atendimento_w,0) > 0) then
		begin
		if      (ie_tipo_consiste_p = 1) then
			ds_detalhe_w	:= substr(ds_texto_atend_w || nr_atendimento_w ||','|| ds_texto_conta_w || nr_interno_conta_w || ',' || ds_texto_proc_w || cd_procedimento_w || ',' || ds_texto_proc_w || cd_proced_atual_w,1,255);
			sus_gravar_inconsistencia(nr_interno_conta_p, 298, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
		elsif   (ie_tipo_consiste_p = 2) then
			ds_erro_w	:= ds_erro_w || '298, ';
		end if;
		end;
	end if;
		
	end;
end if;

ds_erro_p	:= ds_erro_w;

end sus_consiste_numero_bpa;
/
