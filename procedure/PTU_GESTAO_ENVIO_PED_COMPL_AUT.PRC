create or replace
procedure ptu_gestao_envio_ped_compl_aut
			(	nr_seq_guia_p		Number,
				nr_seq_requisicao_p	Number,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 

nr_versao_ptu_w			Varchar2(255);

begin

--dbms_application_info.SET_ACTION('TASY_SCS');

nr_versao_ptu_w	:= pls_obter_versao_scs;

if	(nr_versao_ptu_w	= '035') then
	ptu_gerar_pedido_compl_auto(nr_seq_guia_p, nr_seq_requisicao_p, cd_estabelecimento_p, nm_usuario_p);
elsif	(nr_versao_ptu_w	= '040') then
	ptu_gerar_pedido_compl_aut_v40(nr_seq_guia_p, nr_seq_requisicao_p, cd_estabelecimento_p, nm_usuario_p);
elsif	(nr_versao_ptu_w	= '050') then
	ptu_gerar_pedido_compl_aut_v50(nr_seq_guia_p, nr_seq_requisicao_p, cd_estabelecimento_p, nm_usuario_p);
end if;

--dbms_application_info.SET_ACTION('');

end ptu_gestao_envio_ped_compl_aut;
/
