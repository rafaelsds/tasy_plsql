create or replace
procedure verifica_inicio_analise(nr_sequencia_p		number,
								ie_tipo_item_p		varchar2,
								nr_atendimento_p	number,
								cd_pessoa_fisica_p	varchar2,
								cd_estabelecimento_p	number,
								nm_usuario_p	varchar2) is

nr_prescricao_w				prescr_medica.nr_prescricao%type;
dt_analise_w				varchar(20);
ie_tipo_usuario_w			varchar(1);

begin
Wheb_assist_pck.set_informacoes_usuario(cd_estabelecimento_p, obter_perfil_ativo, nm_usuario_p);

ie_tipo_usuario_w  	:= Wheb_assist_pck.obterParametroFuncao(252,1);
nr_prescricao_w	:= obter_prescr_item_cpoe(nr_sequencia_p,ie_tipo_item_p, nr_atendimento_p, cd_pessoa_fisica_p);

if	(nr_prescricao_w is not null) then
	if (ie_tipo_usuario_w = 'E') then
		dt_analise_w := cpoe_obter_dado_prescr(nr_sequencia_p, ie_tipo_item_p, 'DIAE', cd_pessoa_fisica_p, nr_atendimento_p);
		
		if (dt_analise_w is null) then
			gerar_prescr_medica_compl(	nr_prescricao_w, nm_usuario_p, nm_usuario_p, sysdate);
		end if;
	
	elsif (ie_tipo_usuario_w = 'F') then
		dt_analise_w := cpoe_obter_dado_prescr(nr_sequencia_p, ie_tipo_item_p, 'DIAF', cd_pessoa_fisica_p, nr_atendimento_p);
		
		if (dt_analise_w is null) then
			update 	prescr_medica
			set 	dt_inicio_analise_farm = sysdate,
					nm_usuario_analise_farm = nm_usuario_p
			where 	nr_prescricao = nr_prescricao_w;

			commit;
		end if;
		
	end if;	
end if;

end verifica_inicio_analise;
/