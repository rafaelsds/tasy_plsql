create or replace procedure km_refazer_avaliacao (
	ie_tipo_conhecimento_p		km_questionario.ie_tipo_conhecimento%type,
	cd_funcao_p			funcao.cd_funcao%type default null,
	nr_seq_habilidade_p		km_habilidade_tecnica.nr_sequencia%type default null,
	cd_setor_atendimento_p		setor_atendimento.cd_setor_atendimento%type,
	nm_usuario_p			usuario.nm_usuario%type)
is

nr_seq_questionario_w	km_questionario.nr_sequencia%type;

begin

select	km_obter_ultimo_quest_lib(ie_tipo_conhecimento_p,
		cd_funcao_p,
		nr_seq_habilidade_p,
		cd_setor_atendimento_p,
		nm_usuario_p)
into	nr_seq_questionario_w
from	dual;

km_gerar_avaliacao_usuario(nr_seq_questionario_w, nm_usuario_p);

end km_refazer_avaliacao;
/
