create or replace
procedure	man_insere_exec_padrinho(
			nr_seq_ordem_p		number,
			nm_usuario_p		varchar2) is

nm_usuario_exec_w	varchar2(15);

begin

if	(nr_seq_ordem_p is not null) then

	select	substr(obter_usuario_pessoa(a.cd_pessoa_vinculo),1,200)
	into	nm_usuario_exec_w
	from	com_canal_consultor a,
		man_ordem_servico b
	where	a.cd_pessoa_fisica = b.cd_pessoa_solicitante
	and	b.nr_sequencia = nr_seq_ordem_p;

	insert into man_ordem_servico_exec(
		nr_sequencia,
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		nm_usuario_exec) values (
			man_ordem_servico_exec_seq.nextval,
			nr_seq_ordem_p,
			sysdate,
			nm_usuario_p,
			nm_usuario_exec_w);

	commit;

end if;

end man_insere_exec_padrinho;
/