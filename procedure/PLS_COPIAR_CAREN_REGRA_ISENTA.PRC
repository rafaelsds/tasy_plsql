create or replace
procedure pls_copiar_caren_regra_isenta
		(	nr_seq_regra_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 
			
nr_seq_tipo_carencia_w	number(10);
qt_dias_padrao_w	number(10);
qt_registros_w		number(10);		

Cursor C01 is
	select	nr_sequencia,
		qt_dias_padrao	
	from	pls_tipo_carencia
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_situacao		= 'A'
	and	ie_tipo_operacao	= 'P'
	and	ie_padrao		= 'S';
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_tipo_carencia_w,
	qt_dias_padrao_w;
exit when C01%notfound;
	begin
	
	if	(qt_dias_padrao_w is null) then
		qt_dias_padrao_w	:= 0;
	end if;	
	
	select	count(*)
	into	qt_registros_w
	from	pls_regra_carencia_dias
	where	nr_seq_regra		= nr_seq_regra_p
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w;
	
	if	(qt_registros_w = 0) then
		insert into pls_regra_carencia_dias
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_regra,nr_seq_tipo_carencia,qt_dias)
		values	(	pls_regra_carencia_dias_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
				nr_seq_regra_p,nr_seq_tipo_carencia_w,qt_dias_padrao_w);
	end if;
	end;
end loop;
close C01;

commit;

end pls_copiar_caren_regra_isenta;
/
