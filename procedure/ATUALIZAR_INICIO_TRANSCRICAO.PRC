create or replace
procedure atualizar_inicio_transcricao (
				nr_transcricao_p	number,
				nm_usuario_p		varchar2,
				ie_iniciar_transcr_p	varchar2) is

nr_prescr_w	   prescr_medica.nr_prescricao%type;
cd_funcao_w	   funcao.cd_funcao%type; 	
				
begin
if	(nr_transcricao_p is not null) and
	(nm_usuario_p is not null) and
	(ie_iniciar_transcr_p is not null) then

	cd_funcao_w		:= nvl(obter_funcao_ativa,0);
	
	select nvl(max(nr_prescricao),0)
	into  nr_prescr_w
	from  prescr_medica
	where nr_seq_transcricao = 	nr_transcricao_p;

	if	(nr_prescr_w <> 0) or
		(ie_iniciar_transcr_p = 'N') or
		(cd_funcao_w <> 1700) then
		
		update	transcricao_prescricao
		set	nm_usuario_transcricao	= decode(ie_iniciar_transcr_p, 'S', nm_usuario_p, null),
			dt_transcricao		    = decode(ie_iniciar_transcr_p, 'S', sysdate, null)
		where	nr_sequencia		= nr_transcricao_p;
		
	end if;
end if;

commit;

end atualizar_inicio_transcricao;
/
