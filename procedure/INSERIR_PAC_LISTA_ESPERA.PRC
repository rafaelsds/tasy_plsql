create or replace
procedure inserir_pac_lista_espera
		(nr_seq_agenda_p		number,
		nm_usuario_p			varchar2) is

nr_seq_espera_w		number(10,0);
cd_pessoa_fisica_w	varchar2(10);
cd_convenio_w		number(05,0);
cd_medico_w		varchar2(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10,0);
dt_agenda_w date;

begin

select	cd_pessoa_fisica,
	cd_convenio,
	cd_medico,
	cd_procedimento,
	ie_origem_proced,
	to_date(to_char(dt_agenda,'dd/mm/yyyy')||' ' ||to_char(hr_inicio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
into	cd_pessoa_fisica_w,
	cd_convenio_w,
	cd_medico_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	dt_agenda_w
from	agenda_paciente
where	nr_sequencia	= nr_seq_agenda_p;


select	paciente_espera_seq.nextval
into	nr_seq_espera_w
from	dual;

insert	into paciente_espera
	(nr_sequencia,
	nr_seq_agenda,
	cd_pessoa_fisica,
	nr_prioridade,
	dt_inclusao,
	dt_atualizacao,
	nm_usuario,
	cd_convenio,
	cd_setor_atendimento,	
	cd_unidade_basica,
	cd_unidade_compl,
	ie_clinica,
	cd_medico,
	ie_situacao,
	cd_procedimento,
	ie_origem_proced,
	dt_prevista_internacao,
	cd_tipo_acomodacao,
	ds_observacao)
values	(nr_seq_espera_w,
    nr_seq_agenda_p,
	cd_pessoa_fisica_w,
	1, sysdate,
	sysdate,
	nm_usuario_p,
	cd_convenio_w,
	null, null,
	null, null,
	cd_medico_w,
	'A', cd_procedimento_w,
	ie_origem_proced_w,
	dt_agenda_w,
	null, null);	

commit;

end inserir_pac_lista_espera;
/
