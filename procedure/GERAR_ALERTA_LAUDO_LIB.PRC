create or replace
procedure gerar_alerta_laudo_lib ( nr_seq_laudo_p		number,
				   nr_atendimento_p		number,				   
				   nm_usuario_p			varchar2,
				   ie_commit_p			varchar2) is 
				   
nr_seq_evento_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
				   
cursor c01 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.ie_evento_disp = 'LIBLA'
	and	nvl(a.ie_situacao,'A') = 'A'
               and a.cd_estabelecimento = WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO;
begin
select  substr(nvl(max(cd_pessoa_fisica),''),1,10)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

open C01;
loop
fetch C01 into	
	nr_seq_evento_w;
exit when C01%notfound;
	begin	
	gerar_evento_paciente(	nr_seq_evento_w, 
				nr_atendimento_p, 
				cd_pessoa_fisica_w, 
				nr_seq_laudo_p,
				nm_usuario_p,
				null,
				null,
        			null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				ie_commit_p);
				
	end;
end loop;
close C01;

if (ie_commit_p = 'S') then
	commit;
end if;
end gerar_alerta_laudo_lib;
/