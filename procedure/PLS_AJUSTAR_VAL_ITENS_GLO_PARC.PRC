create or replace
procedure pls_ajustar_val_itens_glo_parc( 	nr_seq_analise_p		pls_analise_conta.nr_sequencia%type,
						nr_seq_conta_proc_p		w_pls_itens_glosa_analise.nr_seq_conta_proc%type,
						nr_seq_conta_mat_p		w_pls_itens_glosa_analise.nr_seq_conta_mat%type,
						vl_liberado_p			w_pls_itens_glosa_analise.vl_liberado%type,
						nr_id_transacao_p		w_pls_itens_glosa_analise.nr_id_transacao%type,
						nr_seq_ocorrencia_p  		pls_ocorrencia.nr_sequencia%type,
						nr_seq_motivo_glosa_p  		tiss_motivo_glosa.nr_sequencia%type,
						nr_seq_grupo_atual_p    	pls_auditoria_conta_grupo.nr_sequencia%type,
						ds_observacao_p         	pls_conta_glosa.ds_observacao%type,
						qt_liberado_p			w_pls_itens_glosa_analise.qt_liberado%type,
						vl_liberado_hi_p		w_pls_itens_glosa_analise.vl_liberado_hi%type,
						vl_liberado_material_p		w_pls_itens_glosa_analise.vl_liberado_material%type,
						vl_liberado_co_p		w_pls_itens_glosa_analise.vl_liberado_co%type,											
						ie_manter_glosas_p		varchar2,
						nm_usuario_p			usuario.nm_usuario%type,
						cd_estabelecimento_p    	estabelecimento.cd_estabelecimento%type) is 

nr_seq_conta_w 		pls_conta.nr_sequencia%type;		
vl_liberado_w       	w_pls_itens_glosa_analise.vl_liberado%type;
vl_liberado_hi_w     	w_pls_itens_glosa_analise.vl_liberado_hi%type;
vl_liberado_material_w  w_pls_itens_glosa_analise.vl_liberado_material%type;
vl_liberado_co_w    	w_pls_itens_glosa_analise.vl_liberado_co%type;

begin

if 	(qt_liberado_p = 0) then
	vl_liberado_w       := 0;
	vl_liberado_hi_w     := 0;
	vl_liberado_material_w   := 0;
	vl_liberado_co_w    := 0;
else
	vl_liberado_w       := vl_liberado_p;
	vl_liberado_hi_w     := vl_liberado_hi_p;
	vl_liberado_material_w   := vl_liberado_material_p;
	vl_liberado_co_w    := vl_liberado_co_p;
end if;

if (nr_seq_conta_proc_p is not null) then

	select   max(nr_seq_conta)
	into  	nr_seq_conta_w
	from  	pls_conta_proc
	where   nr_sequencia = nr_seq_conta_proc_p;

else
	select   max(nr_seq_conta)
	into  	nr_seq_conta_w
	from  	pls_conta_mat
	where   nr_sequencia = nr_seq_conta_mat_p;
end if;

pls_cta_alt_valor_pck.pls_gerencia_alt_valor( 	'1', nr_seq_analise_p,
						nr_seq_conta_w, nr_seq_conta_proc_p, nr_seq_conta_mat_p,
						null, nr_seq_ocorrencia_p, nr_seq_motivo_glosa_p,
						ds_observacao_p, qt_liberado_p, (vl_liberado_hi_w + vl_liberado_material_w + vl_liberado_co_w),
						null, vl_liberado_hi_w, vl_liberado_material_w,
						vl_liberado_co_w, null, null,
						null, null, null,
						null, null, null,
						nr_seq_grupo_atual_p, ie_manter_glosas_p, nr_id_transacao_p,
						cd_estabelecimento_p, nm_usuario_p, 'S');

	
commit;  

end pls_ajustar_val_itens_glo_parc;
/