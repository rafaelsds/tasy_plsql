create or replace
procedure pls_atualizar_arquivo_sib_html(nr_seq_lote_p		number,
					 nr_seq_arquivo_p 	number,
					 ds_caminho_p 		varchar2,
					 nm_usuario_p		Varchar2) is 

begin
	update   pls_sib_arquivo 
        set      ds_arquivo = ds_caminho_p,        
		 dt_geracao = sysdate,
		 nm_usuario_geracao = nm_usuario_p
        where    nr_sequencia = nr_seq_arquivo_p
                 and      nr_seq_lote = nr_seq_lote_p;

commit;

end pls_atualizar_arquivo_sib_html;
/