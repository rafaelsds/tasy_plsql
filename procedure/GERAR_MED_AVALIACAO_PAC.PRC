CREATE OR REPLACE PROCEDURE gerar_med_avaliacao_pac (
                                                    nr_sequencia_laudo_p   IN   laudo_paciente.nr_sequencia%TYPE
) IS

    nr_sequencia_laudo_w	protocolo_tipo_avaliacao.nr_seq_tipo_avaliacao%TYPE;
	nr_sequencia_w			med_avaliacao_paciente.nr_sequencia%TYPE;
	nr_atendimento_w		laudo_paciente.nr_atendimento%TYPE;
	nr_prescricao_w			laudo_paciente.nr_prescricao%TYPE;
	nr_seq_prescricao_w		laudo_paciente.nr_seq_prescricao%TYPE;
	cd_pessoa_fisica_w		atendimento_paciente.cd_pessoa_fisica%TYPE;
	cd_medico_w				med_avaliacao_paciente.cd_medico%TYPE;
	nm_usuario_w			med_avaliacao_paciente.nm_usuario%TYPE;

BEGIN

nr_sequencia_laudo_w := obter_nr_seq_tipo_avaliacao(nr_sequencia_laudo_p);

IF (nr_sequencia_laudo_w IS NOT NULL) THEN

	SELECT
		med_avaliacao_paciente_seq.NEXTVAL
	INTO
		nr_sequencia_w
	FROM
		dual;

	SELECT
		MAX(nr_atendimento),
		MAX(nr_prescricao),
		MAX(nr_seq_prescricao)
	INTO
		nr_atendimento_w,
		nr_prescricao_w,
		nr_seq_prescricao_w
	FROM
		laudo_paciente
	WHERE
		nr_sequencia = nr_sequencia_laudo_p;
		
	cd_pessoa_fisica_w := obter_pf_atendimento(nr_atendimento_w,'C');
	cd_medico_w := obter_pf_usuario_ativo;
	nm_usuario_w := obter_usuario_ativo;
	
	INSERT INTO med_avaliacao_paciente (
		nr_sequencia,
		nr_atendimento,
		nr_prescricao,
		nr_seq_prescr,
		cd_pessoa_fisica,
		cd_medico,
		dt_avaliacao,
		dt_atualizacao,
		nm_usuario,
		nr_seq_tipo_avaliacao
	) VALUES (
		nr_sequencia_w,
		nr_atendimento_w,
		nr_prescricao_w,
		nr_seq_prescricao_w,
		cd_pessoa_fisica_w,
		cd_medico_w,
		SYSDATE,
		SYSDATE,
		nm_usuario_w,
		nr_sequencia_laudo_w
	);
END IF;
END gerar_med_avaliacao_pac;
/
