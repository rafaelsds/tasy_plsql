create or replace
PROCEDURE fleury_lab_duplic_exame_interf(	nr_prescricao_p		NUMBER,
											nr_seq_origem_p		NUMBER,
											nr_seq_exame_p		VARCHAR2,									
											nm_usuario_p		VARCHAR2,
											nr_sequencia_p	OUT	NUMBER) IS 
					
nr_sequencia_w		NUMBER(10,0);
cd_intervalo_w		VARCHAR2(7);
dt_prev_execucao_w	DATE;
cd_material_exame_w	VARCHAR2(20);
dt_prescricao_w		DATE;
nr_atendimento_w	NUMBER(10);
cd_convenio_w		NUMBER(5);
cd_categoria_w		VARCHAR2(10);
ie_tipo_atendimento_w	NUMBER(3);
cd_estab_w		NUMBER(4);
ie_tipo_convenio_w	NUMBER(2);
cd_setor_atend_w	NUMBER(5);
cd_setor_atend_ww	VARCHAR2(255);
cd_procedimento_w	NUMBER(15);
ie_origem_proced_w	NUMBER(10);
ds_erro_w		VARCHAR2(255);
cd_setor_coleta_w	VARCHAR2(255);
cd_setor_entrega_w	VARCHAR2(255);
qt_dia_entrega_w	NUMBER(3);
ie_emite_mapa_w		VARCHAR2(1);
ie_data_resultado_w	VARCHAR2(1);
ds_hora_fixa_w		VARCHAR2(2);
dt_coleta_w		DATE;
ie_amostra_w		VARCHAR2(1);
nr_seq_lab_w		VARCHAR2(20);
--nr_seq_grupo_imp_w	NUMBER(10);
nr_seq_proc_interno_w	NUMBER(10);
nr_seq_proc_interno_aux_w NUMBER(10);
ie_gerar_sequencia_w		VARCHAR2(1);
ie_gera_amostra_coleta_w 	VARCHAR2(1);
ie_status_atend_w		NUMBER(2);
cd_plano_convenio_w		VARCHAR2(10);
BEGIN

IF (nr_seq_exame_p > 0) THEN

	SELECT	MAX(a.nr_sequencia)
	INTO	nr_sequencia_w
	FROM	prescr_procedimento a
	WHERE	a.nr_prescricao = nr_prescricao_p
	AND		NVL(a.nr_seq_origem,a.nr_sequencia) = nr_seq_origem_p
	AND		a.nr_seq_exame = nr_seq_exame_p;

	IF (NVL(nr_sequencia_w,0) = 0) THEN
		
		SELECT	NVL(MAX(nr_sequencia),0) + 1
		INTO	nr_sequencia_w
		FROM	prescr_procedimento
		WHERE 	nr_prescricao = nr_prescricao_p;
		
		SELECT	NVL(MAX(nr_atendimento), 0),
			NVL(MAX(dt_prescricao), SYSDATE)
		INTO	nr_atendimento_w,
			dt_prescricao_w
		FROM	prescr_medica
		WHERE	nr_prescricao = nr_prescricao_p;
		
		IF	(NVL(nr_atendimento_w, 0) > 0) THEN
			BEGIN
			
			SELECT	a.ie_tipo_convenio,
				a.ie_tipo_atendimento,
				b.cd_convenio,
				b.cd_categoria,
				a.cd_estabelecimento,
				b.cd_plano_convenio
			INTO	ie_tipo_convenio_w,
				ie_tipo_atendimento_w,
				cd_convenio_w,
				cd_categoria_w,
				cd_estab_w,
				cd_plano_convenio_w
			FROM	atend_categoria_convenio b,
				atendimento_paciente a
			WHERE	a.nr_atendimento	= nr_atendimento_w
			AND	b.nr_atendimento	= a.nr_atendimento
			AND	b.nr_seq_interno	= OBTER_ATECACO_ATENDIMENTO(A.NR_ATENDIMENTO);
			
			EXCEPTION	
				WHEN NO_DATA_FOUND THEN
				--rai_application_error(-20011,'Faltam informa��es do conv�nio na Entrada �nica.');
				wheb_mensagem_pck.exibir_mensagem_abort(192526);
			END;
			
			SELECT	MAX(nr_seq_proc_interno),
				MAX(cd_material_exame)
			INTO	nr_seq_proc_interno_w,
				cd_material_exame_w
			FROM	Prescr_procedimento
			WHERE	nr_prescricao = nr_prescricao_p
			AND	nr_sequencia = nr_seq_origem_p;
			
			obter_exame_lab_convenio(nr_seq_exame_p, cd_convenio_w, cd_categoria_w, ie_tipo_atendimento_w,
						 cd_estab_w, ie_tipo_convenio_w, nr_seq_proc_interno_w, cd_material_exame_w, cd_plano_convenio_w,
						 cd_setor_atend_w, cd_procedimento_w, ie_origem_proced_w, ds_erro_w, nr_seq_proc_interno_aux_w);
						 
		END IF;
	--	obter_setor_exame_lab(	nr_prescricao_p, nr_seq_exame_w, cd_setor_atendimento_p, cd_material_exame_w, dt_coleta_w,'N', cd_setor_atend_ww, cd_setor_coleta_w, cd_setor_entrega_w, 
	--						qt_dia_entrega_w, ie_emite_mapa_w, ds_hora_fixa_w, ie_data_resultado_w);

		/*SELECT 	MAX(NVL(lab_obter_grupo_imp_estab(cd_estab_w,a.nr_seq_exame, cd_convenio_w),a.nr_seq_grupo_imp))
		INTO	nr_seq_grupo_imp_w
		FROM	exame_laboratorio a
		WHERE	a.nr_seq_exame =  nr_seq_exame_p;*/

		INSERT INTO prescr_procedimento (
			nr_prescricao, nr_sequencia, cd_procedimento, qt_procedimento, dt_atualizacao,
			nm_usuario, cd_motivo_baixa, ie_origem_proced, cd_intervalo,
			ie_urgencia, ie_suspenso, cd_setor_atendimento, dt_prev_execucao,
			ie_status_atend, ie_origem_inf, ie_executar_leito, ie_se_necessario, 
			ie_acm, nr_ocorrencia, ds_observacao,
			nr_seq_interno, nr_seq_proc_interno, ie_avisar_result, nr_seq_exame, cd_material_exame, 
			ie_amostra, dt_resultado, nr_seq_lab,
			cd_setor_coleta,cd_setor_entrega, dt_integracao, nr_seq_origem)
		SELECT	nr_prescricao_p, nr_sequencia_w, cd_procedimento_w, 1, SYSDATE,
			nm_usuario_p, 0, ie_origem_proced_w, NULL, 
			'N', 'N', a.cd_setor_atendimento, NVL(a.dt_prev_execucao,SYSDATE) ,
			a.ie_status_atend, 'L','N','N',
			'N',1, 'Fleury - Procedimento dependente gerado pela integra��o:  '||nm_usuario_p||' em '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss'),
			prescr_procedimento_seq.NEXTVAL, NULL, 'N', nr_seq_exame_p, a.cd_material_exame,
			a.ie_amostra, dt_prescricao_w + qt_dia_entrega_w, NULL,
			a.cd_setor_coleta,a.cd_setor_entrega, a.dt_integracao, nr_seq_origem_p	
		FROM	prescr_procedimento a
		WHERE	nr_prescricao = nr_prescricao_p
		AND	nr_sequencia = nr_seq_origem_p;

		SELECT  NVL(MAX(a.ie_gerar_sequencia),'P'),
			NVL(MAX(a.ie_gera_amostra_coleta),'N')
		INTO 	ie_gerar_sequencia_w,
			ie_gera_amostra_coleta_w	
		FROM 	lab_parametro a,
			prescr_medica b
		WHERE 	a.cd_estabelecimento = b.cd_estabelecimento
		AND	b.nr_prescricao  =  nr_prescricao_p;
		SELECT 	MAX(ie_status_atend)
		INTO	ie_status_atend_w
		FROM 	prescr_procedimento
		WHERE	nr_prescricao 	= nr_prescricao_p
		AND	nr_sequencia	= nr_seq_origem_p;
		
		Gerar_Prescr_Proc_Seq_Lab(nr_prescricao_p, nm_usuario_p, 'P');
		
		IF 	((ie_gerar_sequencia_w <> 'C') OR
			(ie_gera_amostra_coleta_w <> 'S')) OR
			((ie_gerar_sequencia_w = 'C') AND
			(ie_gera_amostra_coleta_w = 'S') AND
			(ie_status_atend_w >= 20))  THEN
			gerar_prescr_proc_mat_item (nr_prescricao_p, nm_usuario_p, cd_estab_w);
		END IF;
	end if;
	
	nr_sequencia_p	:= nr_sequencia_w;

END IF;

END fleury_lab_duplic_exame_interf;
/