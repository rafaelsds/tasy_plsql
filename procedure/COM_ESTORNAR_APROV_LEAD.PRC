create or replace
procedure com_estornar_aprov_lead(	nr_sequencia_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

cd_empresa_w		number(04,0);
cd_cnpj_w		varchar2(14);
ie_status_lead_w		varchar2(03);

begin

select	ie_status_lead,
	cd_cnpj
into	ie_status_lead_w,
	cd_cnpj_w
from	com_solic_lead
where	nr_sequencia = nr_sequencia_p;

if	(ie_status_lead_w = 'A') then
	begin
	select	obter_empresa_estab(cd_estabelecimento_p)
	into	cd_empresa_w
	from	dual;
	
	update	com_solic_lead
	set	dt_aprov_reprov = null,
		nm_usuario	= nm_usuario_p,
		ie_status_lead	= 'P',
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	
	delete from pessoa_juridica_compl
	where	cd_cgc 		= cd_cnpj_w;
	
	delete from pessoa_juridica_estab
	where	cd_cgc 		= cd_cnpj_w
	and	cd_estabelecimento = cd_estabelecimento_p;
	
	delete from com_cliente
	where	cd_cnpj 		= cd_cnpj_w
	and	cd_empresa 	= cd_empresa_w;
	
	delete from pessoa_juridica
	where	cd_cgc		= cd_cnpj_w;
	end;
end if;

commit;

end com_estornar_aprov_lead;
/