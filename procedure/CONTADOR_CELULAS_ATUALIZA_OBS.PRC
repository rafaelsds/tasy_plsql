create or replace
procedure contador_celulas_atualiza_obs(
					nr_seq_exame_p		number,
					nr_seq_resultado_p	number,
					nr_seq_prescr_p	number,
					ds_obs_p			varchar2) is 

begin

if 	(nr_seq_exame_p	is not null) and
	(nr_seq_resultado_p	is not null) and
	(nr_seq_prescr_p is not null) then
	
	update	exame_lab_result_item
	set		ds_resultado = ds_obs_p
	where	nr_seq_resultado = nr_seq_resultado_p
	and		nr_seq_prescr = nr_seq_prescr_p
	and		nr_seq_exame = nr_seq_exame_p;

end if;

commit;

end contador_celulas_atualiza_obs;
/