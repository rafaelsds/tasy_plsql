create or replace 
procedure VERIFICAR_RELEASE(nr_seq_ajuste_versao_p IN NUMBER, ds_retorno_p IN OUT VARCHAR2) is

qt_registro_w	NUMBER(10);
ie_ccb_appr_w VARCHAR2(1):= 'N';
nr_seq_ordem_serv_w	NUMBER(10);
ie_permite_liberar_w	VARCHAR2(1):= 'S';

begin

    SELECT	a.nr_seq_ordem_serv
    INTO	nr_seq_ordem_serv_w
    FROM	AJUSTE_VERSAO a
    WHERE	a.nr_sequencia = nr_seq_ajuste_versao_p;

    SELECT	COUNT(*)
    INTO	qt_registro_w
    FROM	corp.MAN_ORDEM_SERVICO@whebl01_dbcorp X
    WHERE	X.nr_sequencia = nr_seq_ordem_serv_w
    AND		X.ie_classificacao = 'E';

    ds_retorno_p:= 'Warning, The service order needs to attend the requirements.';
    IF (qt_registro_w = 0) THEN
        ie_permite_liberar_w:= 'N';
        ds_retorno_p := ds_retorno_p || ' Defect SO classification;';
    END IF;

    qt_registro_w := 0;

    --Verify if the SO have a potential risk
    select	count(*)
    INTO	qt_registro_w
    from 	corp.man_ordem_serv_tecnico@whebl01_dbcorp a
    where	a.nr_seq_ordem_serv = nr_seq_ordem_serv_w
    and		a.nr_seq_tipo = 212
    and not exists (select	1
                from	corp.man_ordem_serv_tecnico@whebl01_dbcorp x
                where	x.nr_seq_ordem_serv = nr_seq_ordem_serv_w
                and		X.nr_seq_tipo = 213
                and		a.dt_atualizacao < x.dt_atualizacao)
    order by 1;

    IF (qt_registro_w > 0) THEN
        ie_permite_liberar_w:= 'N';
        ds_retorno_p := ds_retorno_p || ' No potential risk (CH) and CCB.';
    END IF;

    --Verify if the SO have the ccb
    select	corp.man_obter_se_ccb_os_aprov@whebl01_dbcorp(nr_seq_ordem_serv_w)
    into	ie_ccb_appr_w
    from	dual;

    IF (ie_ccb_appr_w = 'N') THEN
        ie_permite_liberar_w:= 'N';
        ds_retorno_p := ds_retorno_p || ' CCB.';
    END IF;


    -------------
    --Verify if the Severiry have a potential risk
    select	count(*)
    INTO	qt_registro_w
    from 	corp.man_ordem_servico@whebl01_dbcorp a
    where	a.nr_sequencia = nr_seq_ordem_serv_w
    and		a.nr_seq_severidade_wheb not in (2,3,4);

    IF (qt_registro_w > 0) THEN
        ie_permite_liberar_w:= 'N';
        ds_retorno_p := ds_retorno_p || ' Severity SO is not allowed.';
    END IF;

    IF (ie_permite_liberar_w = 'S') THEN
        ds_retorno_p := null;
    END IF;

end VERIFICAR_RELEASE;
/
