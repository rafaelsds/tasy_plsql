create or replace
procedure pls_inserir_dma_demons_analise(	nm_usuario_p				usuario.nm_usuario%type,
						cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
						ds_arquivo_p			in	pls_dma_demons_analise.ds_arquivo%type,
						ds_hash_p			in	pls_dma_demons_analise.ds_hash%type,
						nr_seq_dma_demons_analise_p	out	pls_dma_demons_analise.nr_sequencia%type) is 

qt_hash_w	pls_integer;

begin

select	count(1)
into	qt_hash_w
from	pls_dma_demons_analise
where	ds_hash	= upper(ds_hash_p);

if	(nvl(qt_hash_w, 0) > 0) then

	wheb_mensagem_pck.exibir_mensagem_abort(857329);
end if;

	
insert	into pls_dma_demons_analise
	(nr_sequencia, dt_atualizacao, nm_usuario,
	 dt_atualizacao_nrec, nm_usuario_nrec, cd_estabelecimento,
	 dt_importacao, dt_geracao_rec, nm_usuario_imp, ds_arquivo, ds_hash )
values	(pls_dma_demons_analise_seq.nextval, sysdate, nm_usuario_p,
	 sysdate, nm_usuario_p, cd_estabelecimento_p,
	 sysdate, null, nm_usuario_p, ds_arquivo_p, upper(ds_hash_p)	) returning nr_sequencia into nr_seq_dma_demons_analise_p;
	
commit;

end pls_inserir_dma_demons_analise;
/
