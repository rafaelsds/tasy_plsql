CREATE OR REPLACE PROCEDURE EXEC_SUBMIT_JOB_COMMAND(DS_COMMAND_P       VARCHAR2,
                                                    NM_USUARIO_P       VARCHAR2,
                                                    SET_USER_SESSION_P BOOLEAN DEFAULT TRUE,
                                                    NEXT_DATE_P        DATE DEFAULT NULL,
                                                    INTERVAL_P         VARCHAR2 DEFAULT NULL) IS
  JOBNO        NUMBER;
  DS_COMANDO_W VARCHAR2(4000);
BEGIN
  IF (SET_USER_SESSION_P) THEN
    DS_COMANDO_W := '
BEGIN
  PHILIPS_PARAM_PCK.SET_NR_SEQ_IDIOMA(' || NVL(NVL(PHILIPS_PARAM_PCK.GET_NR_SEQ_IDIOMA(), OBTER_NR_SEQ_IDIOMA(NM_USUARIO_P)), 1) || ');
  WHEB_USUARIO_PCK.SET_CD_ESTABELECIMENTO(' || OBTER_ESTABELECIMENTO_ATIVO || ');
  WHEB_USUARIO_PCK.SET_NM_USUARIO(''' || NM_USUARIO_P || ''');

  ' || DS_COMMAND_P || '
END;';
  ELSE
    DS_COMANDO_W := DS_COMMAND_P;
  END IF;

  DBMS_JOB.SUBMIT(JOBNO,
                  DS_COMANDO_W,
                  NVL(NEXT_DATE_P, SYSDATE + INTERVAL '1' SECOND),
                  INTERVAL_P);
END;
/
