create or replace
procedure gerar_prescr_proc_mat_item ( 	nr_prescricao_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p 	number,
					nr_seq_prescr_p		number default null) is

qt_registros_w      number(10);
nr_registro_w       number(10);
nr_seq_prescr_w			number(6);
nr_seq_exame_w			number(10);
nr_seq_grupo_w			number(10);
nr_seq_grupo_imp_w		number(10);
nr_seq_origem_w 		number(6);
nr_seq_material_w		number(10);
qt_coleta_w			number(3);
qt_volume_padrao_w		number(5);
qt_volume_frasco_w		number(5);
qt_frascos_w         number(5);
qt_volume_sobra_w number(5);
qt_volume_w number(5);
qt_volume_material_w  number(5);
qt_volume_somado_w  number(5);
qt_tempo_padrao_w		number(4,2);
ie_padrao_amostra_w		Varchar2(5);
nr_seq_prescr_proc_mat_w 	number(10);
ie_status_atend_w		number(2);
nr_seq_lab_w			varchar2(20);
dt_liberacao_w			varchar2(8);
dt_liberacao_ww			varchar2(8);
cd_barras_w			varchar(100);
ie_amostra_w    		varchar2(1);
qt_exames_amostra_exame_w	number(10);
qt_exames_amostra_w		number(10);
nr_seq_frasco_w			number(10);
qt_exames_amostra_grupo_w	number(10);
ie_grupo_imp_amostra_seq_w	varchar2(1);
--nr_identificador_w	number(10);
nr_seq_barras_w			number(10);
cd_identificador_w		varchar2(5);
nr_seq_material_integracao_w	varchar2(3);
nr_seq_int_prescr_w		number(10);
ie_agrupa_amostra_horario_w		varchar2(1);
dt_prev_execucao_w date;
ie_considera_vol_amostra_w varchar2(1);
ie_nova_amostra_w varchar2(1);
nr_seq_frasco_ant_w			number(10);
nr_seq_material_ant_w    number(10);
nr_seq_grupo_ant_w			number(10);
nr_seq_grupo_imp_ant_w		number(10);
nr_seq_origem_ant_w 		number(6);
nr_seq_prescr_ant_w			number(6);    
qt_tempo_padrao_ant_w		number(4,2);
qt_coleta_ant_w			number(3);
ie_amostra_ant_w    		varchar2(1);
nr_seq_lab_ant_w			varchar2(20);
ie_status_atend_ant_w		number(2);
dt_prescricao_w         varchar2(8);
dt_liberacao_medico_w varchar2(8);
dt_liberacao_medico_ww varchar2(8);
ie_rule_age_w   		varchar2(1);
ie_ordena_amostra_w		lab_parametro.ie_ordena_amostra%type;

nr_seq_motivo_recoleta_w	prescr_procedimento.nr_seq_recoleta%type;
ie_urgencia_w			prescr_procedimento.ie_urgencia%type;

ie_pendente_amostra_w prescr_procedimento.ie_pendente_amostra%type;
ie_cons_pend_amostra_w lab_parametro.IE_CONS_PEND_AMOSTRA%type;

cursor c01 is
	        select COUNT (*) OVER (), 
                a.nr_sequencia,
		a.nr_seq_exame,
		b.nr_seq_grupo,
		b.nr_seq_grupo_imp,
		nvl(a.nr_seq_origem,0),
		d.nr_sequencia,
		nvl(c.qt_coleta,1),
		nvl(d.qt_volume_padrao,decode(ie_considera_vol_amostra_w, 'S', coalesce(c.qt_volume_referencia,0),0)),
		nvl(d.qt_tempo_padrao,0),
		nvl(a.ie_status_atend,10),
		a.nr_seq_lab,
		a.ie_amostra,
		nr_seq_frasco,
		a.DT_PREV_EXECUCAO,
		a.nr_seq_recoleta,
		nvl(a.ie_urgencia,'N'),
		decode(ie_rule_age_w,'S',lab_obter_volume_frasco_age(nr_prescricao_p),lab_obter_volume_frasco(nr_seq_frasco)),
		a.ie_pendente_amostra
    	from	exame_lab_material c,
		material_exame_lab d,
		exame_laboratorio b,
		prescr_procedimento a,
		prescr_medica k
	where	k.nr_prescricao = nr_prescricao_p
	and	k.nr_prescricao = a.nr_prescricao
	and	a.nr_seq_exame = b.nr_seq_exame
	and	a.nr_seq_exame is not null
	and	d.nr_sequencia = Obter_Mat_Exame_Lab_prescr(a.nr_prescricao, a.nr_sequencia, 1)
	and	c.nr_seq_material = d.nr_sequencia
	and	c.nr_seq_exame = a.nr_seq_exame
	and	((nr_seq_prescr_p is null) or (nr_seq_prescr_p = a.nr_sequencia))
	and	(d.ie_volume_tempo = 'S' or nvl(c.qt_coleta,1) > 0)
	and	not exists (select 1 from prescr_proc_mat_item w where a.nr_prescricao = w.nr_prescricao and a.nr_sequencia = w.nr_seq_prescr)
	order by d.nr_sequencia, nvl(c.qt_coleta,1) desc, b.nr_seq_grupo,decode(nvl(ie_ordena_amostra_w,'N'),'S',b.nr_seq_apresent,1), nvl(a.nr_seq_origem,0), a.nr_sequencia;

begin
  qt_volume_somado_w :=0;
  nr_registro_w      :=1;
  nr_seq_frasco_ant_w:=0;  

select  max(to_char(nvl(dt_liberacao,dt_liberacao_medico), 'ddmmyyyy')), 
max(to_char(dt_prescricao,'ddmmyy')),
max(to_char(nvl(dt_liberacao_medico,dt_liberacao), 'ddmmyyyy')),
max(to_char(nvl(dt_liberacao,dt_liberacao_medico), 'ddmmyy')),
max(to_char(nvl(dt_liberacao_medico,dt_liberacao), 'ddmmyy'))
into	dt_liberacao_w, dt_prescricao_w, dt_liberacao_medico_w, dt_liberacao_ww, dt_liberacao_medico_ww
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

select  nvl(max(ie_padrao_amostra),'PM'),
		nvl(max(qt_exames_amostra),0),
		nvl(max(ie_gerar_padrao_grupo_imp),'N'),
		nvl(max(ie_agrupa_amostra_horario),'N'),
		nvl(max(IE_CONSIDERA_VOL_AMOSTRA),'N'),
		nvl(max(ie_ordena_amostra),'N'),
		nvl(max(ie_cons_pend_amostra),'N'),
                nvl(max(ie_rule_years_old_vol),'N')
into 	ie_padrao_amostra_w,
		qt_exames_amostra_exame_w,
		ie_grupo_imp_amostra_seq_w,
		ie_agrupa_amostra_horario_w,
		ie_considera_vol_amostra_w,
		ie_ordena_amostra_w,
		ie_cons_pend_amostra_w,
                ie_rule_age_w
from 	lab_parametro
where 	cd_estabelecimento = cd_estabelecimento_p;

select	max(a.cd_identificador)
into	cd_identificador_w
from  	empresa_integr_dados a
where	a.cd_identificador is not null
and		a.nr_seq_empresa_integr = 34
and		a.cd_estabelecimento = cd_estabelecimento_p;

open C01;
loop
fetch c01 into	
qt_registros_w,
nr_seq_prescr_w,
nr_seq_exame_w,
nr_seq_grupo_w,
nr_seq_grupo_imp_w,
nr_seq_origem_w,
nr_seq_material_w,
qt_coleta_w,
qt_volume_padrao_w,
qt_tempo_padrao_w,
ie_status_atend_w,
nr_seq_lab_w,
ie_amostra_w,
nr_seq_frasco_w,
dt_prev_execucao_w,
nr_seq_motivo_recoleta_w,
ie_urgencia_w,
qt_volume_frasco_w,
ie_pendente_amostra_w;
exit when c01%notfound;
	
		
        select 	nvl(max(qt_exames_amostra),0)
        into	qt_exames_amostra_grupo_w
        from 	grupo_exame_lab
        where	nr_sequencia = nr_seq_grupo_w;

        if	(qt_exames_amostra_grupo_w > 0) then
                qt_exames_amostra_exame_w	:= qt_exames_amostra_grupo_w;
        end if;
    
        for i in 1..qt_coleta_w loop
                if  (nr_seq_motivo_recoleta_w is null) then
                
                        if (ie_agrupa_amostra_horario_w = 'U') then
                        
                                SELECT 	NVL(MAX(NR_SEQ_PRESCR_PROC_MAT),0) 
                                into	nr_seq_prescr_proc_mat_w          
                                FROM   	prescr_procedimento a,  
                                        prescr_proc_material m,
                                        prescr_proc_mat_item b 
                                where ((((nr_seq_grupo = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or
                                        ((nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S'))) or
                                        (ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
                                and	((nvl(a.nr_seq_frasco,0) = nvl(nr_seq_frasco_w,0)) or (ie_padrao_amostra_w not in ('AM11F','AM10F')))
                                and	(nr_amostra = i)
                                AND nr_seq_material = nr_seq_material_w
                                AND a.nr_prescricao = b.nr_prescricao
                                AND b.NR_SEQ_PRESCR_PROC_MAT = m.nr_sequencia
                                AND a.nr_sequencia = b.nr_seq_prescr  
                                AND a.nr_prescricao = nr_prescricao_p
                                AND a.nr_sequencia <> nr_seq_prescr_w       
                                and a.ie_urgencia  =  ie_urgencia_w 
                                and ((nr_seq_origem_w = 0) or (nr_seq_origem = nr_seq_origem_w))
                                and ((ie_cons_pend_amostra_w = 'S' and a.ie_pendente_amostra = ie_pendente_amostra_w) or ie_cons_pend_amostra_w = 'N');

                        elsif (ie_agrupa_amostra_horario_w = 'S') then

                                SELECT 	NVL(MAX(NR_SEQ_PRESCR_PROC_MAT),0) 
                                into	nr_seq_prescr_proc_mat_w          
                                FROM  prescr_procedimento a,  
                                prescr_proc_material m,
                                prescr_proc_mat_item b 
                                where ((((nr_seq_grupo = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or
                                ((nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S'))) or
                                (ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
                                and	((nvl(a.nr_seq_frasco,0) = nvl(nr_seq_frasco_w,0)) or (ie_padrao_amostra_w not in ('AM11F','AM10F')))
                                and	(nr_amostra = i)
                                and ((ie_agrupa_amostra_horario_w = 'S') or (nr_seq_origem_w = 0))
                                AND nr_seq_material = nr_seq_material_w
                                AND a.nr_prescricao = b.nr_prescricao
                                AND b.NR_SEQ_PRESCR_PROC_MAT = m.nr_sequencia
                                AND a.nr_sequencia = b.nr_seq_prescr  
                                AND a.nr_prescricao = nr_prescricao_p
                                AND a.nr_sequencia <> nr_seq_prescr_w         
                                AND (PKG_DATE_UTILS.start_of(a.dt_prev_execucao, 'mi', 0) = PKG_DATE_UTILS.start_of(dt_prev_execucao_w, 'mi', 0))
                                                and ((ie_cons_pend_amostra_w = 'S' and a.ie_pendente_amostra = ie_pendente_amostra_w) or ie_cons_pend_amostra_w = 'N');

			else
				if (ie_cons_pend_amostra_w = 'S') then
		 
					select 	nvl(max(m.NR_SEQUENCIA),0)
					into	nr_seq_prescr_proc_mat_w
					from 	prescr_procedimento a,  
							prescr_proc_material m,
							prescr_proc_mat_item b 
					where 	a.nr_prescricao = nr_prescricao_p
					and 	m.nr_seq_material = nr_seq_material_w
					and 	((((m.nr_seq_grupo = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or
						((m.nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S'))) or
						(ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
					and	((nvl(m.nr_seq_frasco,0) = nvl(nr_seq_frasco_w,0)) or (ie_padrao_amostra_w not in ('AM11F','AM10F')))
					and	m.nr_amostra = i
					AND 	a.nr_prescricao = b.nr_prescricao
					AND 	b.nr_seq_prescr_proc_mat = m.nr_sequencia
					AND 	a.nr_sequencia = b.nr_seq_prescr  
					AND 	a.nr_sequencia <> nr_seq_prescr_w         
					and ((ie_agrupa_amostra_horario_w = 'S') or (nr_seq_origem_w = 0))							
					and ((ie_cons_pend_amostra_w = 'S' and a.ie_pendente_amostra = ie_pendente_amostra_w) or ie_cons_pend_amostra_w = 'N');
				
				else 
					
					select 	nvl(max(NR_SEQUENCIA),0)
					into	nr_seq_prescr_proc_mat_w
					from 	prescr_proc_material
					where 	nr_prescricao = nr_prescricao_p
					and 	nr_seq_material = nr_seq_material_w
					and 	((((nr_seq_grupo = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or
						((nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S'))) or
						(ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
					and	((nvl(nr_seq_frasco,0) = nvl(nr_seq_frasco_w,0)) or (ie_padrao_amostra_w not in ('AM11F','AM10F')))
					and	nr_amostra = i
					and ((ie_agrupa_amostra_horario_w = 'S') or (nr_seq_origem_w = 0));
					
				end if;
					
			end if;
		else
			select 	nvl(max(m.NR_SEQUENCIA),0)
			into	nr_seq_prescr_proc_mat_w
			from 	prescr_procedimento a,  
			        prescr_proc_material m,
			        prescr_proc_mat_item b 
			where 	a.nr_prescricao = nr_prescricao_p
			and 	m.nr_seq_material = nr_seq_material_w
			and 	((((m.nr_seq_grupo = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or
				((m.nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S'))) or
				(ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
			and	((nvl(m.nr_seq_frasco,0) = nvl(nr_seq_frasco_w,0)) or (ie_padrao_amostra_w not in ('AM11F','AM10F')))
			and	m.nr_amostra = i
			AND 	a.nr_prescricao = b.nr_prescricao
			AND 	b.nr_seq_prescr_proc_mat = m.nr_sequencia
			AND 	a.nr_sequencia = b.nr_seq_prescr  
			AND 	a.nr_sequencia <> nr_seq_prescr_w         
			and 	a.nr_seq_recoleta = nr_seq_motivo_recoleta_w
			and ((ie_agrupa_amostra_horario_w = 'S') or (nr_seq_origem_w = 0))							
			and ((ie_cons_pend_amostra_w = 'S' and a.ie_pendente_amostra = ie_pendente_amostra_w) or ie_cons_pend_amostra_w = 'N');
			
		
		end if;
                
                if ((ie_considera_vol_amostra_w = 'S') and (ie_padrao_amostra_w IN ('AM11F','AM10F')) and
                (nr_seq_frasco_w is not null) and (qt_volume_frasco_w > 0)) then    
    
                        if ((nr_seq_frasco_ant_w <> 0) and (nr_seq_frasco_ant_w <> nr_seq_frasco_w)) then
                                update prescr_proc_material set qt_volume = qt_volume_somado_w
                                where nr_seq_material = nr_seq_material_ant_w
                                and ((((nr_seq_grupo = nr_seq_grupo_ant_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or
                                        ((nr_seq_grupo_imp = nr_seq_grupo_imp_ant_w) and (ie_grupo_imp_amostra_seq_w = 'S'))) or
                                        (ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
                                and ((nvl(nr_seq_frasco,0) = nvl(nr_seq_frasco_ant_w,0)) or (ie_padrao_amostra_w not in ('AM11F','AM10F')))
                                and nr_amostra = i
                                AND nr_sequencia = nr_seq_prescr_proc_mat_w
                                and ((ie_agrupa_amostra_horario_w = 'S') or (nr_seq_origem_ant_w = 0));             
                                                
                                gerar_prescr_proc_mat_frasco (nr_prescricao_p, nm_usuario_p, cd_estabelecimento_p, nr_seq_prescr_ant_w, nr_seq_material_ant_w, qt_volume_somado_w, qt_tempo_padrao_ant_w, qt_coleta_ant_w, nr_seq_grupo_ant_w, i, 
                                                                ie_amostra_ant_w, nr_seq_frasco_ant_w, nr_seq_grupo_imp_ant_w, ie_padrao_amostra_w, nr_seq_lab_ant_w, dt_liberacao_w, cd_identificador_w, ie_status_atend_ant_w);            
                                qt_volume_somado_w := 0;
                        end if;
    
                        qt_volume_somado_w := qt_volume_somado_w + qt_volume_padrao_w;

                        if (qt_volume_somado_w <= qt_volume_frasco_w) then          
                                ie_nova_amostra_w  := 'N';
                        else  
                                update prescr_proc_material set qt_volume = qt_volume_frasco_w
                                where nr_seq_material = nr_seq_material_w
                                and ((((nr_seq_grupo = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or
                                        ((nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S'))) or
                                        (ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
                                and ((nvl(nr_seq_frasco,0) = nvl(nr_seq_frasco_w,0)) or (ie_padrao_amostra_w not in ('AM11F','AM10F')))
                                and nr_amostra = i
                                AND nr_sequencia = nr_seq_prescr_proc_mat_w
                                and ((ie_agrupa_amostra_horario_w = 'S') or (nr_seq_origem_w = 0));             

                                qt_frascos_w:=trunc(qt_volume_somado_w/qt_volume_frasco_w); 

                                for x in 1..qt_frascos_w loop
                                        qt_volume_sobra_w:= qt_volume_somado_w - qt_volume_frasco_w;          
                                        qt_volume_w:= qt_volume_somado_w - qt_volume_sobra_w;
                                        
                                        update prescr_proc_material set qt_volume = qt_volume_w
                                        where nr_seq_material = nr_seq_material_w
                                        and ((((nr_seq_grupo = nr_seq_grupo_w) and (ie_grupo_imp_amostra_seq_w = 'N')) or
                                                ((nr_seq_grupo_imp = nr_seq_grupo_imp_w) and (ie_grupo_imp_amostra_seq_w = 'S'))) or
                                                (ie_padrao_amostra_w in ('PM','PM11','PMR11','PM13')))
                                        and ((nvl(nr_seq_frasco,0) = nvl(nr_seq_frasco_w,0)) or (ie_padrao_amostra_w not in ('AM11F','AM10F')))
                                        and nr_amostra = i
                                        AND nr_sequencia = nr_seq_prescr_proc_mat_w
                                        and ((ie_agrupa_amostra_horario_w = 'S') or (nr_seq_origem_w = 0));             
                                        
                                        if (qt_volume_somado_w >= qt_volume_frasco_w) then
                                                gerar_prescr_proc_mat_frasco (nr_prescricao_p, nm_usuario_p, cd_estabelecimento_p, nr_seq_prescr_w, nr_seq_material_w, qt_volume_w, qt_tempo_padrao_w, qt_coleta_w, nr_seq_grupo_w, i, 
                                                                                ie_amostra_w, nr_seq_frasco_w, nr_seq_grupo_imp_w, ie_padrao_amostra_w, nr_seq_lab_w, dt_liberacao_w, cd_identificador_w, ie_status_atend_w);            
                                        end if;                                 
                                                                        
                                        qt_volume_somado_w:= qt_volume_somado_w - qt_volume_w;                                             
                                end loop;
                                
                                if (qt_volume_sobra_w > 0) then
                                ie_nova_amostra_w  := 'N';            
                                qt_volume_somado_w:= qt_volume_sobra_w;
                                else  
                                ie_nova_amostra_w  := 'S';     
                                qt_volume_somado_w:= 0;
                                end if;  
                        end if;
       
                        if (qt_volume_somado_w < qt_volume_frasco_w) then
                                ie_nova_amostra_w  := 'N';
                        end if;

                        if ((nr_seq_prescr_proc_mat_w <> 0) and (nr_registro_w = qt_registros_w) and (qt_volume_somado_w > 0)) then
                                gerar_prescr_proc_mat_frasco (nr_prescricao_p, nm_usuario_p, cd_estabelecimento_p, nr_seq_prescr_w, nr_seq_material_w, qt_volume_somado_w, qt_tempo_padrao_w, qt_coleta_w, nr_seq_grupo_w, i, 
                                                                ie_amostra_w, nr_seq_frasco_w, nr_seq_grupo_imp_w, ie_padrao_amostra_w, nr_seq_lab_w, dt_liberacao_w, cd_identificador_w, ie_status_atend_w);
                        end if;       
                else
                        if ((ie_considera_vol_amostra_w = 'S') and (ie_padrao_amostra_w IN ('AM11F','AM10F'))) then
                                if (nr_seq_frasco_w is null) then    
                                        gravar_log_lab(49,obter_desc_expressao(775681)||': Nr_Prescricao:'||nr_prescricao_p||' - Nr_Seq_Exame:'||nr_seq_exame_w||' - Nr_Seq_Material:'||nr_seq_material_w,nm_usuario_p); 
                                end if;   
                                
                                if ((nr_seq_frasco_w is not null) and (qt_volume_frasco_w < 1)) then    
                                        gravar_log_lab(49,obter_desc_expressao(775691)||': Nr_Prescricao:'||nr_prescricao_p||' - Nr_Seq_Exame:'||nr_seq_exame_w||' - Nr_Seq_Material:'||nr_seq_material_w||' - Nr_Seq_Frasco:'||nr_seq_frasco_w,nm_usuario_p); 
                                end if;              
                        end if; 
                
                        ie_nova_amostra_w  := 'N';
                end if;

		if ((nr_seq_prescr_proc_mat_w = 0 and ie_considera_vol_amostra_w = 'N') or ((ie_considera_vol_amostra_w = 'S') and (ie_nova_amostra_w ='S'))) then
                        select 	prescr_proc_material_seq.nextval
                        into	nr_seq_prescr_proc_mat_w
                        from 	dual;

                        select 	nvl(max(nr_seq_int_prescr),0) + 1
                        into	nr_seq_int_prescr_w
                        from	prescr_proc_material
                        where	nr_prescricao = nr_prescricao_p;

                        insert into prescr_proc_material
                                        (nr_sequencia,
                                        nr_prescricao,
                                        nr_seq_material,
                                        qt_volume,
                                        dt_atualizacao,
                                        qt_tempo,
                                        qt_minuto,
                                        nm_usuario,
                                        dt_coleta,
                                        nr_seq_grupo,
                                        nr_amostra,
                                        ie_amostra,
                                        nr_seq_frasco,
                                        nr_seq_grupo_imp,
                                        nr_seq_int_prescr)
                        values (nr_seq_prescr_proc_mat_w,
                                nr_prescricao_p,
                                nr_seq_material_w,
                                qt_volume_padrao_w, 
                                sysdate,
                                qt_tempo_padrao_w,
                                0,
                                nm_usuario_p,
                                decode(qt_coleta_w,1,sysdate,null),
                                nr_seq_grupo_w,
                                i,
                                ie_amostra_w,
                                nr_seq_frasco_w,
                                nr_seq_grupo_imp_w,
                                nr_seq_int_prescr_w);

				
                        if (ie_padrao_amostra_w IN ('AMO9','AMO10','AMO11','AM11F','AM10F','AMO13','WEB')) then
                                cd_barras_w := nr_seq_prescr_proc_mat_w;
                        elsif (ie_padrao_amostra_w = 'EAM10') then
                                cd_barras_w := lpad(substr(nvl(cd_estabelecimento_p,1),1,2),2,0) || nr_seq_prescr_proc_mat_w;
                        elsif (ie_padrao_amostra_w = 'DGIS') then
                                cd_barras_w := dt_liberacao_w || lpad(nr_seq_grupo_imp_w,2,'0') || lpad(nr_seq_lab_w,20,'0') || i;
                        elsif (ie_padrao_amostra_w = 'DS4GI') then
                                cd_barras_w := dt_liberacao_w || lpad(nr_seq_lab_w,20,'0') || lpad(nr_seq_grupo_imp_w,2,'0') || i;
                        elsif (ie_padrao_amostra_w = 'DGS5') then
                                cd_barras_w := nvl(dt_liberacao_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,5,0);
                        elsif (ie_padrao_amostra_w = 'DGS6') then
                                cd_barras_w := nvl(dt_liberacao_ww,dt_prescricao_w)|| lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,6,0);
                        elsif (ie_padrao_amostra_w = 'DGS7') then
                                cd_barras_w := nvl(dt_liberacao_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,7,0);
                        elsif (ie_padrao_amostra_w = 'DGSL') then
                                cd_barras_w:= nvl(dt_liberacao_medico_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) ||  lpad(nr_seq_lab_w,4,0);
                        elsif (ie_padrao_amostra_w = 'DGSL') then
                                cd_barras_w :=  dt_liberacao_ww || lpad(nr_seq_grupo_w,2,'0') || lpad(nr_seq_lab_w,20,'0') || i;
                        elsif (ie_padrao_amostra_w = 'DAGS4') then
                                if ('LE' = lab_obter_valor_parametro(722,373)) then
                                        cd_barras_w := nvl(dt_liberacao_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,4,0);      
                                else
                                        cd_barras_w := nvl(dt_liberacao_medico_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,4,0);
                                end if;  
                        elsif (ie_padrao_amostra_w = 'SISB') then

                                select	substr(max(ds_material_integracao),1,3)
                                into	nr_seq_material_integracao_w
                                from	material_exame_lab
                                where	nr_sequencia = nr_seq_material_w;

                                cd_barras_w := substr(cd_identificador_w,1,1)||lpad(to_char(nr_prescricao_p),8,'0')||'M'||lpad(substr(nr_seq_material_integracao_w,1,3),3,'0');


                        else
                                cd_barras_w := dt_liberacao_w || lpad(nr_seq_grupo_w,2,'0') || lpad(nr_seq_lab_w,20,'0') || i;
                        end if;

                        begin
                        update 	prescr_proc_material
                        set 	cd_barras = cd_barras_w
                        where 	nr_sequencia = nr_seq_prescr_proc_mat_w;

                        exception
                        when others then

                                update 	prescr_procedimento
                                set 	nr_seq_lab = null
                                where 	nr_prescricao = nr_prescricao_p
                                and	nr_sequencia = nr_seq_prescr_w;

                        end;

		else

                        select 	count(*)
                        into	qt_exames_amostra_w
                        from	prescr_proc_mat_item
                        where	nr_seq_prescr_proc_mat = nr_seq_prescr_proc_mat_w;

                        if	(qt_exames_amostra_exame_w > 0) and
                                (ie_padrao_amostra_w IN ('AMO9','AMO10','AMO11','AM11F','AM10F','AMO13','EAM10','WEB')) and
                                (qt_exames_amostra_w >= qt_exames_amostra_exame_w) then

                                select 	prescr_proc_material_seq.nextval
                                into	nr_seq_prescr_proc_mat_w
                                from 	dual;

                                select 	nvl(max(nr_seq_int_prescr),0)
                                into	nr_seq_int_prescr_w
                                from	prescr_proc_material
                                where	nr_prescricao = nr_prescricao_p;


                                insert into prescr_proc_material
                                                (nr_sequencia,
                                                nr_prescricao,
                                                nr_seq_material,
                                                qt_volume,
                                                dt_atualizacao,
                                                qt_tempo,
                                                qt_minuto,
                                                nm_usuario,
                                                dt_coleta,
                                                nr_seq_grupo,
                                                nr_amostra,
                                                ie_amostra,
                                                nr_seq_frasco,
                                                nr_seq_grupo_imp,
                                                nr_seq_int_prescr)
                                values (nr_seq_prescr_proc_mat_w,
                                        nr_prescricao_p,
                                        nr_seq_material_w,
                                        qt_volume_padrao_w,
                                        sysdate,
                                        qt_tempo_padrao_w,
                                        0,
                                        nm_usuario_p,
                                        decode(qt_coleta_w,1,sysdate,null),
                                        nr_seq_grupo_w,
                                        i,
                                        ie_amostra_w,
                                        nr_seq_frasco_w,
                                        nr_seq_grupo_imp_w,
                                        nr_seq_int_prescr_w);

                                if (ie_padrao_amostra_w IN ('AMO9','AMO10','AMO11','AM11F','AM10F','AMO13','WEB')) then
                                        cd_barras_w := nr_seq_prescr_proc_mat_w;
                                elsif (ie_padrao_amostra_w = 'EAM10') then
                                        cd_barras_w := lpad(substr(nvl(cd_estabelecimento_p,1),1,2),2,0) || nr_seq_prescr_proc_mat_w;
                                elsif (ie_padrao_amostra_w = 'DGIS') then
                                        cd_barras_w := dt_liberacao_w || lpad(nr_seq_grupo_imp_w,2,'0') || lpad(nr_seq_lab_w,20,'0') || i;
                                elsif (ie_padrao_amostra_w = 'DS4GI') then
                                        cd_barras_w := dt_liberacao_w || lpad(nr_seq_lab_w,20,'0') || lpad(nr_seq_grupo_imp_w,2,'0') || i;
                                elsif (ie_padrao_amostra_w = 'DGS5') then
                                        cd_barras_w := nvl(dt_liberacao_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,5,0);
                                elsif (ie_padrao_amostra_w = 'DGS6') then
                                        cd_barras_w := nvl(dt_liberacao_ww,dt_prescricao_w)|| lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,6,0);
                                elsif (ie_padrao_amostra_w = 'DGS7') then
                                        cd_barras_w := nvl(dt_liberacao_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,7,0);
                                elsif (ie_padrao_amostra_w = 'DGSL') then
                                        cd_barras_w:= nvl(dt_liberacao_medico_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) ||  lpad(nr_seq_lab_w,4,0);
                                elsif (ie_padrao_amostra_w = 'DGS') then
                                        cd_barras_w :=  dt_liberacao_ww || lpad(nr_seq_grupo_w,2,'0') || lpad(nr_seq_lab_w,20,'0') || i;
                                elsif (ie_padrao_amostra_w = 'DAGS4') then
                                        if ('LE' = lab_obter_valor_parametro(722,373)) then
                                                cd_barras_w := nvl(dt_liberacao_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,4,0);      
                                        else
                                                cd_barras_w := nvl(dt_liberacao_medico_ww,dt_prescricao_w) || lpad(nr_seq_grupo_w,2,0) || lpad(nr_seq_lab_w,4,0);
                                        end if;  
                                elsif (ie_padrao_amostra_w = 'SISB') then

                                        select	substr(max(ds_material_integracao),1,3)
                                        into	nr_seq_material_integracao_w
                                        from	material_exame_lab
                                        where	nr_sequencia = nr_seq_material_w;

                                        cd_barras_w := substr(cd_identificador_w,1,1)||lpad(to_char(nr_prescricao_p),8,'0')||'M'||lpad(substr(nr_seq_material_integracao_w,1,3),3,'0');
                                else
                                        cd_barras_w := dt_liberacao_w || lpad(nr_seq_grupo_w,2,'0') || lpad(nr_seq_lab_w,20,'0') || i;
                                end if;

                                begin
                                update prescr_proc_material
                                set cd_barras = cd_barras_w
                                where nr_sequencia = nr_seq_prescr_proc_mat_w;

                                exception
                                when others then

                                        update 	prescr_procedimento
                                        set 	nr_seq_lab = null
                                        where 	nr_prescricao = nr_prescricao_p
                                        and	nr_sequencia = nr_seq_prescr_w;

                                end;


			else

                                select 	max(cd_barras)
                                into	cd_barras_w
                                from 	prescr_proc_material
                                where 	nr_sequencia = nr_seq_prescr_proc_mat_w;
			end if;

		end if;

                insert into prescr_proc_mat_item
                                        (nr_sequencia,
                                        dt_atualizacao,
                                        nm_usuario,
                                        dt_atualizacao_nrec,
                                        nm_usuario_nrec,
                                        nr_prescricao,
                                        nr_seq_prescr,
                                        nr_seq_prescr_proc_mat,
                                        ie_status,
                                        cd_barras,
                                        nr_seq_frasco)
                values			(prescr_proc_mat_item_seq.nextval,
                                        sysdate,
                                        nm_usuario_p,
                                        sysdate,
                                        nm_usuario_p,
                                        nr_prescricao_p,
                                        nr_seq_prescr_w,
                                        nr_seq_prescr_proc_mat_w,
                                        ie_status_atend_w,
                                        cd_barras_w,
                                        nr_seq_frasco_w);

	end loop;
        nr_registro_w          := nr_registro_w + 1;
        nr_seq_frasco_ant_w    := nr_seq_frasco_w;
        nr_seq_material_ant_w  := nr_seq_material_w;
        nr_seq_grupo_ant_w     := nr_seq_grupo_w;
        nr_seq_grupo_imp_ant_w := nr_seq_grupo_imp_w;    
        nr_seq_origem_ant_w    := nr_seq_origem_w;
        nr_seq_prescr_ant_w    := nr_seq_prescr_w;
        qt_tempo_padrao_ant_w  := qt_tempo_padrao_w;
        qt_coleta_ant_w        := qt_coleta_w;
        ie_amostra_ant_w       := ie_amostra_w;
        nr_seq_lab_ant_w       := nr_seq_lab_w;
        ie_status_atend_ant_w  := ie_status_atend_w;
end loop;
close c01;

lab_gerar_etiq_externa(nr_prescricao_p, nm_usuario_p, cd_estabelecimento_p);

end gerar_prescr_proc_mat_item;
/
