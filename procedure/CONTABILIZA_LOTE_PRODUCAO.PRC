create or replace
procedure contabiliza_lote_producao(nr_lote_contabil_p          number,
                                nm_usuario_p                    varchar2,
                                ds_retorno_p            in out  varchar2,
                                ie_exclusao_p           in      varchar2) is

cd_estabelecimento_w            lote_contabil.cd_estabelecimento%type;
dt_mesano_referencia_w          date;
cd_tipo_lote_contabil_w         lote_contabil.cd_tipo_lote_contabil%type;
nr_lote_producao_w              lote_producao.nr_lote_producao%type;
dt_movimento_w                  date;
nr_sequencia_movto_w            number(10);
ie_valor_lote_prod_w            varchar(5);
cd_historico_w                  number(10);
vl_contabil_w                   number(15,2);
cd_conta_debito_w               varchar2(20);
cd_conta_credito_w              varchar2(20);
cd_local_estoque_w              number(4);
cd_centro_custo_w               number(8);
ds_compl_historico_w            varchar2(255);
ds_conteudo_w                   varchar2(4000);
cd_material_w                   number(6);
vl_retorno_w                    number(15,2);
ie_compl_hist_w                 varchar2(1);
ds_material_w                   varchar2(255);
qt_real_w                       number(15,4);
qt_conv_estoque_consumo_w       number(13,4);
cd_oper_baixa_producao_w        number(10);
cd_conta_oper_baixa_prod_w      varchar2(20);
cd_empresa_w                    number(10);
nr_documento_ww                 movimento_contabil.nr_documento%type;
ie_origem_documento_w           movimento_contabil.ie_origem_documento%type;
dt_mesano_fm_w                  date;
nm_agrupador_w                  varchar2(255);
nr_seq_agrupamento_w            movimento_contabil.nr_seq_agrupamento%type;
ie_centro_custo_w               conta_contabil.ie_centro_custo%type;
ie_data_contab_lote_prod_w      parametro_estoque.ie_data_contab_lote_prod%type;
nm_atributo_w                   w_movimento_contabil.nm_atributo%type;
nm_tabela_w                     w_movimento_contabil.nm_tabela%type;
cd_sequencia_parametro_es_w     number(10);
cd_sequencia_parametro_pd_w     number(10);
cd_operacao_w                   number(10);

cursor c01 is
        select  a.nr_lote_producao,
                a.dt_confirmacao,
                obter_custo_medio_material(a.cd_estabelecimento, trunc(a.dt_mesano_referencia, 'mm'), b.cd_material_estoque) vl_movimento,
                a.qt_real,
                a.cd_local_estoque,
                a.cd_material
        from    material        b,
                lote_producao   a
        where   a.cd_material           = b.cd_material
        --and   ie_exclusao_p           != 'S'
        and     ie_valor_lote_prod_w    = 'CM'
        and     a.nr_lote_contabil      = nr_lote_contabil_p
        union all
        select  a.nr_lote_producao,
                a.dt_confirmacao,
                a.vl_custo_material vl_movimento,
                a.qt_real,
                a.cd_local_estoque,
                a.cd_material
        from    material        b,
                lote_producao   a
        where   a.cd_material           = b.cd_material
        --and   ie_exclusao_p           != 'S'
        and     ie_valor_lote_prod_w    = 'VC'
        and     a.nr_lote_contabil      = nr_lote_contabil_p
        union all
        select  b.nr_lote_producao,
                b.dt_confirmacao,
                a.vl_estoque vl_movimento,
                b.qt_real,
                b.cd_local_estoque,
                b.cd_material
        from    movto_estoque_prod_v    a,
                lote_producao           b
        where   b.nr_lote_producao      = a.nr_documento
        --and   ie_exclusao_p           != 'S'
        and     ie_valor_lote_prod_w    = 'MV'
        and     b.nr_lote_contabil      = nr_lote_contabil_p;

begin
ds_retorno_p            := null;

/* Identifica o Estabelecimento do Lote Contabil */
select  cd_estabelecimento,
        trunc(dt_referencia,'mm'),
        cd_tipo_lote_contabil
into    cd_estabelecimento_w,
        dt_mesano_referencia_w,
        cd_tipo_lote_contabil_w
from    lote_contabil
where   nr_lote_contabil        = nr_lote_contabil_p;

if      (ie_exclusao_p = 'S') then
        delete  from movimento_contabil
        where   nr_lote_contabil        = nr_lote_contabil_p;

        update lote_contabil
        set      vl_credito             = 0,
                 vl_debito              = 0
        where    nr_lote_contabil       = nr_lote_contabil_p;

        update  lote_producao
        set     nr_lote_contabil        = 0
        where   nr_lote_contabil        = nr_lote_contabil_p;
else
        select  max(a.cd_hist_contab_prod),
                max(a.cd_oper_baixa_producao),
                max(a.ie_valor_lote_prod),
                nvl(max(a.ie_data_contab_lote_prod),'R')
        into    cd_historico_w,
                cd_oper_baixa_producao_w,
                ie_valor_lote_prod_w,
                ie_data_contab_lote_prod_w
        from    parametro_estoque       a
        where   a.cd_estabelecimento    = cd_estabelecimento_w;

        cd_historico_w                  := nvl(cd_historico_w, 0);
        cd_oper_baixa_producao_w        := nvl(cd_oper_baixa_producao_w, 0);
        ie_valor_lote_prod_w            := nvl(ie_valor_lote_prod_w, 'CM');
        cd_empresa_w                    := obter_empresa_estab(cd_estabelecimento_w);

        if      (cd_historico_w = 0) then
                wheb_mensagem_pck.exibir_mensagem_abort(192337);
                /*'Deve ser informado o historico padrao para contabilizacao da producao, nos parametros de estoque'|| '#@#@');*/
        end if;

        delete  w_movimento_contabil
        where   nr_lote_contabil        = nr_lote_contabil_p;

        dt_mesano_fm_w  := fim_dia(fim_mes(dt_mesano_referencia_w));

        update  lote_producao
        set     nr_lote_contabil                = nr_lote_contabil_p
        where   cd_estabelecimento              = cd_estabelecimento_w
        and     nvl(nr_lote_contabil, 0)        = 0
        and     decode(ie_data_contab_lote_prod_w,'R',dt_mesano_referencia,'C',dt_confirmacao) between dt_mesano_referencia_w and dt_mesano_fm_w
        and     dt_confirmacao is not null;
        commit;

        if      (ie_valor_lote_prod_w = 'CM') then
                nm_tabela_w     := 'LOTE_PRODUCAO';
                nm_atributo_w   := 'VL_CUSTO_MEDIO';
        elsif   (ie_valor_lote_prod_w = 'VC') then
                nm_tabela_w     := 'LOTE_PRODUCAO';
                nm_atributo_w   := 'VL_CUSTO_MATERIAL';
        elsif   (ie_valor_lote_prod_w = 'MV') then
                nm_tabela_w     := 'MOVTO_ESTOQUE_PROD_V';
                nm_atributo_w   := 'VL_ESTOQUE';
        end if;


        begin
        ie_compl_hist_w := obter_se_compl_tipo_lote(cd_estabelecimento_w, cd_tipo_lote_contabil_w);

        exception
        when others then
                ie_compl_hist_w := 'N';
        end;

        nr_sequencia_movto_w            := 0;
        ds_compl_historico_w            := '';

        nm_agrupador_w  := nvl(trim(obter_agrupador_contabil(cd_tipo_lote_contabil_w)),'NR_LOTE_PRODUCAO');

        open c01;
        loop
        fetch c01 into
                nr_lote_producao_w,
                dt_movimento_w,
                vl_contabil_w,
                qt_real_w,
                cd_local_estoque_w,
                cd_material_w;
        exit when c01%notfound;
                ds_compl_historico_w    := '';
                ds_conteudo_w           := '';
                dt_movimento_w          := trunc(dt_movimento_w,'dd');
                nr_sequencia_movto_w    := nr_sequencia_movto_w + 1;
                ds_material_w           := substr(obter_desc_material(cd_material_w),1,255);
                cd_conta_debito_w       := '';
                cd_conta_credito_w      := '';
                cd_sequencia_parametro_es_w     := null;
                cd_sequencia_parametro_pd_w     := null;

                select  qt_conv_estoque_consumo
                into    qt_conv_estoque_consumo_w
                from    material
                where   cd_material     = cd_material_w;

                if      (ie_valor_lote_prod_w = 'CM')    then
                        qt_real_w       := qt_real_w * qt_conv_estoque_consumo_w;
                        vl_contabil_w   := vl_contabil_w * qt_real_w;
                end if;

                if      (nvl(cd_oper_baixa_producao_w,0) <> 0) then
                        cd_operacao_w := cd_oper_baixa_producao_w;
                        begin
                        cd_conta_oper_baixa_prod_w      := substr(obter_conta_hist_oper_estoque(cd_empresa_w,cd_oper_baixa_producao_w,'C'),1,20);
                        exception when others then
                                cd_conta_oper_baixa_prod_w      := '';
                        end;
                else
                        cd_operacao_w := null;
                end if;

                if (nm_agrupador_w = 'NR_LOTE_PRODUCAO')then
                        nr_seq_agrupamento_w := nr_lote_producao_w;
                end if;

                if  (nvl(nr_seq_agrupamento_w,0) = 0)then
                        nr_seq_agrupamento_w := nr_lote_producao_w;
                end if;

                define_conta_material(  cd_estabelecimento_w,
                                        cd_material_w,
                                        2,
                                        null,
                                        0,
                                        '0',
                                        0,
                                        null,
                                        null,
                                        null,
                                        cd_local_estoque_w,
                                        cd_operacao_w,
                                        dt_movimento_w,
                                        cd_conta_debito_w,
                                        cd_centro_custo_w,
                                        null);

                cd_sequencia_parametro_es_w := philips_contabil_pck.get_parametro_conta_contabil();

                define_conta_material(  cd_estabelecimento_w,
                                        cd_material_w,
                                        3,
                                        null,
                                        0,
                                        '0',
                                        0,
                                        null,
                                        null,
                                        null,
                                        cd_local_estoque_w,
                                        cd_operacao_w,
                                        dt_movimento_w,
                                        cd_conta_credito_w,
                                        cd_centro_custo_w,
                                        null);

                cd_sequencia_parametro_pd_w := philips_contabil_pck.get_parametro_conta_contabil();
                cd_conta_credito_w      := nvl(cd_conta_oper_baixa_prod_w, cd_conta_credito_w);

                select  nvl(max(cd_centro_custo), 0)
                into    cd_centro_custo_w
                from    local_estoque
                where   cd_local_estoque        = cd_local_estoque_w;

                if      (ie_compl_hist_w = 'S') then
                        ds_conteudo_w   := substr(      nr_lote_producao_w      || '#@' ||
                                                        ds_material_w           || '#@' ||
                                                        cd_material_w,1,4000);

                        begin
                        ds_compl_historico_w    := substr(obter_compl_historico(cd_tipo_lote_contabil_w, cd_historico_w, ds_conteudo_w),1,255);
                        exception
                        when others then
                                ds_compl_historico_w    := null;
                        end;
                end if;

                nr_documento_ww         := nr_lote_producao_w;
                ie_origem_documento_w   := 9;

                insert into w_movimento_contabil
                        (nr_lote_contabil,
                        nr_sequencia,
                        cd_conta_contabil,
                        ie_debito_credito,
                        cd_historico,
                        dt_movimento,
                        vl_movimento,
                        cd_centro_custo,
                        ds_compl_historico,
                        ds_doc_agrupamento,
                        nr_seq_agrupamento,
                        nr_seq_trans_fin,
                        cd_cgc,
                        cd_pessoa_fisica,
                        nr_documento,
                        ie_transitorio,
                        ie_origem_documento,
                        nr_seq_tab_orig,
                        nm_tabela,
                        nm_atributo,
                        nr_seq_info,
                        cd_sequencia_parametro)
                values  (nr_lote_contabil_p,
                        nr_sequencia_movto_w,
                        cd_conta_debito_w,
                        'D',
                        cd_historico_w,
                        dt_movimento_w,
                        vl_contabil_w,
                        null,
                        ds_compl_historico_w,
                        null,
                        nr_seq_agrupamento_w,
                        null,
                        null,
                        null,
                        nr_documento_ww,
                        'N',
                        ie_origem_documento_w,
                        nr_lote_producao_w,
                        nm_tabela_w,
                        nm_atributo_w,
                        9,
                        cd_sequencia_parametro_es_w);

                ie_centro_custo_w       := '';

                if      (nvl(cd_centro_custo_w,0) != 0) and
                        (nvl(cd_conta_credito_w,'0') != '0')then
                        begin
                        select  nvl(a.ie_centro_custo,'N')
                        into    ie_centro_custo_w
                        from    conta_contabil a
                        where   a.cd_conta_contabil     = cd_conta_credito_w;
                        exception when others then
                                ie_centro_custo_w       := 'N';
                        end;
                end if;

                insert into w_movimento_contabil
                        (nr_lote_contabil,
                        nr_sequencia,
                        cd_conta_contabil,
                        ie_debito_credito,
                        cd_historico,
                        dt_movimento,
                        vl_movimento,
                        cd_centro_custo,
                        ds_compl_historico,
                        ds_doc_agrupamento,
                        nr_seq_agrupamento,
                        nr_seq_trans_fin,
                        cd_cgc,
                        cd_pessoa_fisica,
                        nr_documento,
                        ie_transitorio,
                        ie_origem_documento,
                        nr_seq_tab_orig,
                        nm_tabela,
                        nm_atributo,
                        nr_seq_info,
                        cd_sequencia_parametro)
                values  (nr_lote_contabil_p,
                        nr_sequencia_movto_w,
                        cd_conta_credito_w,
                        'C',
                        cd_historico_w,
                        dt_movimento_w,
                        vl_contabil_w,
                        decode(ie_centro_custo_w,'S',cd_centro_custo_w,'N',null),
                        ds_compl_historico_w,
                        null,
                        nr_seq_agrupamento_w,
                        null,
                        null,
                        null,
                        nr_documento_ww,
                        'N',
                        ie_origem_documento_w,
                        nr_lote_producao_w,
                        nm_tabela_w,
                        nm_atributo_w,
                        9,
                        cd_sequencia_parametro_pd_w);
        end loop;
        close c01;

        agrupa_movimento_contabil(nr_lote_contabil_p, nm_usuario_p);
end if;

if      (ds_retorno_p is null) then
        update  lote_contabil
        set     ie_situacao             = 'A',
                dt_geracao_lote         = sysdate
        where   nr_lote_contabil        = nr_lote_contabil_p;

        if      (ie_exclusao_p = 'S') then
                ds_retorno_p            := wheb_mensagem_pck.get_texto(165188);

                ctb_gravar_log_lote(    nr_lote_contabil_p,
                                        2,
                                        '',
                                        nm_usuario_p);
        else
                ds_retorno_p            := wheb_mensagem_pck.get_texto(165187);

                ctb_gravar_log_lote(    nr_lote_contabil_p,
                                        1,
                                        '',
                                        nm_usuario_p);
        end if;
        commit;
else
        rollback;
end if;

ds_retorno_p    := substr(ds_retorno_p,1,255);

end contabiliza_lote_producao;
/
