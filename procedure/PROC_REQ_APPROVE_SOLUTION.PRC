CREATE OR REPLACE
PROCEDURE PROC_REQ_APPROVE_SOLUTION(
    nr_sequencia_p  in LATAM_REQUISITO.NR_SEQUENCIA%TYPE,
    nm_usuario_p    in LATAM_REQUISITO.NM_USUARIO%TYPE
) IS

ie_liberar_desenv_old_w LATAM_REQUISITO.IE_LIBERAR_DESENV%type;
dt_atual_s              date:= SYSDATE;
BEGIN
    IF (nr_sequencia_p IS NOT NULL AND nm_usuario_p IS NOT NULL) THEN
        UPDATE  LATAM_REQUISITO a 
        SET     a.DT_APROVACAO_CONSULT  = dt_atual_s,
                a.DT_ATUALIZACAO        = dt_atual_s,
                a.NM_USUARIO            = nm_usuario_p,
                a.IE_SITUACAO           = 'J'
        WHERE   a.NR_SEQUENCIA          = nr_sequencia_p
        RETURNING IE_LIBERAR_DESENV INTO ie_liberar_desenv_old_w;
        
        generate_latam_log(nr_sequencia_p, ie_liberar_desenv_old_w, 1060326);
        COMMIT;
    END IF;
END PROC_REQ_APPROVE_SOLUTION;
/
