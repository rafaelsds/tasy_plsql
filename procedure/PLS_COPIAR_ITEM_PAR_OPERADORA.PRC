create or replace
procedure pls_copiar_item_par_operadora
		(	nr_seq_item_p		number,
			cd_perfil_p		number,
			nm_usuario_p		Varchar2) is 
			
NR_SEQ_APRESENTACAO_w	number(10);

begin

select	max(NR_SEQ_APRESENTACAO)
into	NR_SEQ_APRESENTACAO_w
from	PLS_PARAMETROS_ITEM_PERFIL
where	cd_perfil	= cd_perfil_p;

NR_SEQ_APRESENTACAO_w	:= nvl(NR_SEQ_APRESENTACAO_w,0)+1;

insert into PLS_PARAMETROS_ITEM_PERFIL
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		cd_perfil,nr_seq_item,ie_controle,NR_SEQ_APRESENTACAO)
values	(	PLS_PARAMETROS_ITEM_PERFIL_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		cd_perfil_p,nr_seq_item_p,'T',NR_SEQ_APRESENTACAO_w);

commit;

end pls_copiar_item_par_operadora;
/
