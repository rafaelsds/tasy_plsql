create or replace
procedure man_vincular_os_proj_projeto(	nr_seq_projeto_p		number,
					nr_seq_ordem_p		number,
					nm_usuario_p		Varchar2) is


nr_seq_ordem_serv_w	number(10,0);
qt_existe_os_w		number(10,0);
ds_titulo_w		varchar2(80);
ds_relat_tecnico_w	long;

begin

select	count(*)
into	qt_existe_os_w
from	man_ordem_servico
where	nr_sequencia	= nr_seq_ordem_p;

if	(qt_existe_os_w = 0) then
	/*(-20011,'N�o existe uma ordem de servi�o com este n�mero, verifique.');*/
	wheb_mensagem_pck.exibir_mensagem_abort(263165);
end if;

select	nr_seq_ordem_serv
into	nr_seq_ordem_serv_w
from	proj_projeto
where	nr_sequencia	= nr_seq_projeto_p;

select	count(*)
into	qt_existe_os_w
from	proj_projeto
where	nr_seq_ordem_serv = nr_seq_ordem_p;
  
if	(nvl(nr_seq_ordem_serv_w,0) > 0) or
	(qt_existe_os_w > 0) then
	/*(-20011,'O projeto j� possui uma ordem de servi�o vinculada � ele, ou a ordem de servi�o j� est� vinculada a outro projeto.');*/
	wheb_mensagem_pck.exibir_mensagem_abort(263166);
end if;

update	proj_projeto
set	nr_seq_ordem_serv		= nr_seq_ordem_p,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_projeto_p;

select	ds_titulo
into	ds_titulo_w
from	proj_projeto
where	nr_sequencia	= nr_seq_projeto_p;

ds_relat_tecnico_w	:= wheb_mensagem_pck.get_texto(303189,'DS_TITULO=' || ds_titulo_w);

insert into man_ordem_serv_tecnico(
	nr_sequencia,
	nr_seq_ordem_serv,
	dt_atualizacao,
	nm_usuario,
	ds_relat_tecnico,
	dt_historico,
	ie_origem,
	dt_liberacao,
	nm_usuario_lib)
values(	man_ordem_serv_tecnico_seq.nextval,
	nr_seq_ordem_p,
	sysdate,
	nm_usuario_p,
	ds_relat_tecnico_w,
	sysdate,
	'I',
	sysdate,
	nm_usuario_p);
commit;

end man_vincular_os_proj_projeto;
/