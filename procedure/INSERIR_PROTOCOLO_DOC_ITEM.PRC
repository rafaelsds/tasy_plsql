create or replace 
procedure inserir_protocolo_doc_item(
					nr_seq_interno_p number,
					nm_usuario_p varchar2,
					nr_sequencia_p number
				     ) is

nr_seq_item_w number(5);
nr_seq_tipo_item_w	number(10,0);
ie_tipo_documento_w	varchar2(1);
  
begin
obter_param_usuario(290, 106, obter_perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_tipo_documento_w);

if	(ie_tipo_documento_w = 'S') then
	select	nr_seq_tipo_doc_item
	into	nr_seq_tipo_item_w
	from	protocolo_documento
	where	nr_sequencia = nr_sequencia_p;
end if;

select	nvl(max(nr_seq_item),0) +1
into	nr_seq_item_w
from 	protocolo_doc_item
where 	nr_sequencia = nr_sequencia_p; 

insert into protocolo_doc_item(
			nr_sequencia,  
			nr_seq_interno,
			nr_seq_item,
			nm_usuario,
			dt_atualizacao,
			nr_seq_tipo_item)
values   		(nr_sequencia_p,
			nr_seq_interno_p,
			nvl(nr_seq_item_w,0),
			nm_usuario_p,
			sysdate,
			nr_seq_tipo_item_w);

commit;

end inserir_protocolo_doc_item;
/