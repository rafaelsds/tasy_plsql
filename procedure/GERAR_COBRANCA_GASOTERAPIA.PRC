create or replace
procedure Gerar_cobranca_gasoterapia(	nr_seq_gas_p		number,
					nr_prescricao_p		number,
					nr_sequencia_p		number,
					dt_inicio_p		date,
					dt_fim_p		date,
					nm_usuario_p		varchar2,
					ie_evento_p		varchar2,
					ie_estorno_p		varchar2 default 'N') is 

cd_setor_atendimento_w	number(15,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
qt_procedimento_w		number(9,3);
qt_minutos_regra_w		number(10,0);
qt_min_aplic_gas_w		number(18,6);
cd_convenio_w			number(10,0);
qt_minutos_aux_w		number(10,0);
nr_atendimento_w		number(10,0);
cd_estabelecimento_w	number(10,0);
qt_proced_regra_w		number(9,3) := 0;
qt_dose_w				number(18,6);
qt_min_aplic_gas_ww		number(18,6);
cd_material_w			number(6,0);
cd_local_estoque_w		number(4,0);
nr_seq_atepacu_w		number(10,0);
nr_seq_atualiza_prec_w	number(15,0);
nr_seq_proc_interno_w	number(15,0);
ie_controle_w			number(10,0);
dt_prescricao_w			date;
ie_cobra_conv_w			varchar2(1);
ie_sair_w				varchar2(1);
ie_data_evento_w		varchar2(1);
ie_respiracao_w			varchar2(15);
IE_DISP_RESP_ESP_w		varchar2(15);
ie_quant_proporcional_w	varchar2(1);
ie_momento_geracao_w	varchar2(1);
ie_divide_conta_w		varchar2(1);
ie_somar_w				varchar2(1);
ie_evento_w				varchar2(10);
ie_arredondar_w			varchar2(1);
ie_unidade_medida_w		varchar2(15);
ie_unidade_med_w		varchar2(15);
qt_gasoterapia_w		number(8,3);
qt_gas_w				number(8,3);
dt_referencia_w			date;
dt_periodo_final_w		date;
dt_inicio_w				date;
dt_evento_w				date;
nr_interno_conta_w		number(15,0);
cd_modalidade_vent_w	varchar2(15);
ie_regra_restritiva_w	varchar2(1);
cd_categoria_w			varchar2(10);
ie_alerta_w				varchar2(1);
ie_quant_validade_w		varchar2(1);
nr_horas_validade_w		number(5);
ie_lanca_itens_assoc_w  varchar2(1);
cd_setor_atend_w		number(15,0);
qt_minuto_tolerancia_w	number(15,4);
cd_setor_conta_w		regra_cobranca_gas.cd_setor_conta%type;
ie_regra_setor_w		varchar2(1);
cd_setor_atend_prescr_w	prescr_medica.cd_setor_atendimento%type;
cd_unid_med_dose_w		prescr_material.cd_unidade_medida_dose%type;
qt_gasAcumulado_w		number(9,3);
dt_alteracao_vel_w		date;
qt_gaso_nova_w			number(8,3);
ie_calcula_w			boolean	:= true;
qt_lancado_w			number(15,4);
ie_ultimo_evento_w		prescr_gasoterapia_evento.ie_evento%type;
dt_ultimo_evento_w		prescr_gasoterapia_evento.dt_evento%type;
qt_gas_event_w			number(9,3);
nr_seq_equipamento_w  regra_cobranca_gas.nr_seq_equipamento%type;
nr_seq_equip_restr_w  regra_cobranca_gas.nr_seq_equipamento%type;

cursor c01 is
select	cd_procedimento,
	ie_origem_proced,
	qt_procedimento,
	qt_minutos_regra,
	nvl(ie_divide_conta,'N'),
	nvl(ie_momento_geracao, 'T'),
	nvl(ie_arredondar,'N'),
	nvl(ie_quant_proporcional,'N'),
	nvl(qt_minuto_tolerancia,0),
	nr_seq_proc_interno,
	nvl(ie_quantidade_validade, 'N') ie_quant_validade,
	nvl(cd_setor_conta,0)
from	regra_cobranca_gas
where	((cd_convenio	= cd_convenio_w) or
		 (cd_convenio is null))
and		((cd_setor_atendimento	= cd_setor_atend_w) or
		 (cd_setor_atendimento is null))
and		((ie_respiracao is null) or (ie_respiracao = ie_respiracao_w))
and		((ie_disp_resp_esp is null) or (ie_disp_resp_esp = ie_disp_resp_esp_w))
and		((cd_modalidade_vent is null) or (cd_modalidade_vent = cd_modalidade_vent_w))
and		((qt_minutos_partir is null) or (qt_min_aplic_gas_w >= qt_minutos_partir))
and		((nvl(qt_minutos_ate,0)	= 0) or
		 (nvl(qt_minutos_ate,0)	>= qt_min_aplic_gas_w))
and		nr_seq_gas	= nr_seq_gas_p
and   (nr_seq_equipamento is null or nr_seq_equipamento_w is null) or  
      (nr_seq_equipamento = nr_seq_equip_restr_w)
order by
	cd_convenio,
	ie_respiracao,
	ie_disp_resp_esp,
	cd_modalidade_vent,
	qt_minutos_partir,
	qt_minutos_ate;
	 
Cursor C02 is
select	a.cd_material,
		a.qt_dose,
		a.cd_unidade_medida_dose
from	prescr_material a,
		prescr_gasoterapia b
where	a.ie_agrupador 	= 15
and		a.nr_seq_gasoterapia = b.nr_sequencia
and		b.nr_seq_gas 	= nr_seq_gas_p
and		a.nr_prescricao = b.nr_prescricao
and		a.nr_prescricao = nr_prescricao_p;

cursor c03 is
select	b.dt_evento,
		b.ie_evento,
		nvl(b.ie_unidade_medida,a.ie_unidade_medida),
		nvl(b.qt_gasoterapia,a.qt_gasoterapia),
    nvl(b.nr_seq_equipamento, 0)
from	prescr_gasoterapia_evento b,
		prescr_gasoterapia a
where	a.nr_sequencia	= b.nr_seq_gasoterapia(+)
and		a.nr_seq_gas	= nr_seq_gas_p
and		a.nr_prescricao = nr_prescricao_p
and		nvl(b.ie_evento_valido,'S') = 'S'
--and	b.ie_evento 		<> 'I'
order by b.dt_evento;

begin

--Se estiver chamando ao liberar a prescricao ou ao iniciar a solucao.
if	(ie_evento_p	= 'P') or (ie_evento_p	= 'X') then
	qt_min_aplic_gas_w	:= 60;
else
	qt_min_aplic_gas_w	:= 0;
end if;	

dt_inicio_w	:= dt_inicio_p;
ie_somar_w	:= 'S';
qt_gasAcumulado_w	:= 0;

open C03;
loop
fetch C03 into	
	dt_evento_w,
	ie_evento_w,
	ie_unidade_medida_w,
	qt_gasoterapia_w,
  nr_seq_equipamento_w;
exit when C03%notfound;

	if	(ie_evento_p = 'I') then
		if	(ie_evento_w	in ('R','I')) then
			dt_inicio_w	:= dt_evento_w;
		elsif	(ie_evento_w	= 'IN') then
			qt_min_aplic_gas_w	:= obter_min_entre_datas(dt_inicio_w, dt_evento_w, 1);
		end if;
	else
		if	(ie_evento_w	in ('R','I')) then
			dt_inicio_w	:= dt_evento_w;
			ie_somar_w	:= 'S';
		elsif	(ie_evento_w	in ('IN','T','TE')) and
			(ie_somar_w	= 'S') then
			qt_min_aplic_gas_w	:= qt_min_aplic_gas_w + obter_min_entre_datas(dt_inicio_w, dt_evento_w, 1);
			ie_somar_w	:= 'N';
			if (nvl(qt_gasAcumulado_w, 0) > 0 and
				ie_ultimo_evento_w = 'V' and
				ie_evento_w = 'IN') then
				qt_gasAcumulado_w := qt_gasAcumulado_w + (qt_gaso_nova_w * obter_min_entre_datas(dt_ultimo_evento_w, dt_evento_w, 1));
			end if;
		end if;
	end if;

	if	(qt_gas_w is null) then
		ie_unidade_med_w	:= ie_unidade_medida_w;
		qt_gas_w			:= qt_gasoterapia_w;
	elsif ((ie_evento_w = 'V') and (ie_unidade_medida_w = 'lpm')) then	
		--  qt_gasAcumulado_w vai receber o total inalado com a quantidade antes da alteracao de velocidade
		-- Este if e necessario pro caso de nao ter nenhum evento de interrupcao antes da alteracao de velocidade.
		if (qt_min_aplic_gas_w > 0 and
			qt_gasAcumulado_w = 0) then
			qt_gasAcumulado_w	:= qt_gas_w * qt_min_aplic_gas_w;
		elsif (qt_gasAcumulado_w = 0) then
			qt_gasAcumulado_w	:= qt_gas_w * obter_min_entre_datas(dt_inicio_w, dt_evento_w, 1);
		else
			if (ie_ultimo_evento_w = 'V') then
				qt_gasAcumulado_w	:= qt_gasAcumulado_w + (qt_gaso_nova_w * obter_min_entre_datas(dt_alteracao_vel_w, dt_evento_w, 1));
			else
				qt_gasAcumulado_w	:= qt_gasAcumulado_w + (qt_gas_event_w * obter_min_entre_datas(dt_ultimo_evento_w, dt_evento_w, 1));
			end if;
		end if;
		
		-- Salvar a qtde mais recente e a data que foi alterada a essa qtde		
		dt_alteracao_vel_w	:= dt_evento_w;
		qt_gaso_nova_w		:= qt_gasoterapia_w;
		
	elsif ((ie_evento_w = 'T') and (qt_gasAcumulado_w > 0)) then	
		--Quando terminar soma o que ja tem na 'qt_gasAcumulado_w'	 com o restante, considerando a ultima alteracao de velocidade
		if (ie_ultimo_evento_w = 'V') then
			qt_gasAcumulado_w	:= qt_gasAcumulado_w + (qt_gaso_nova_w * obter_min_entre_datas(dt_alteracao_vel_w, dt_evento_w, 1));
			ie_calcula_w		:= false;
		elsif (ie_ultimo_evento_W <> 'T') then
			qt_gasAcumulado_w	:= qt_gasAcumulado_w + (qt_gas_event_w * obter_min_entre_datas(dt_ultimo_evento_w, dt_evento_w, 1));
			ie_calcula_w		:= false;
		end if;
	end if;

	ie_ultimo_evento_w := ie_evento_w;
	dt_ultimo_evento_w := dt_evento_w;
	qt_gas_event_w := qt_gasoterapia_w;
	
  if (ie_evento_w in ('R', 'I')) then
    nr_seq_equip_restr_w := nr_seq_equipamento_w;
  end if;
end loop;
close C03;

if (nvl(nr_sequencia_p, 0) > 0) then
	select 	max(a.ie_respiracao),
			max(a.ie_disp_resp_esp),
			max(a.cd_modalidade_vent)
	into	ie_respiracao_w,
			ie_disp_resp_esp_w,
			cd_modalidade_vent_w
	from	prescr_gasoterapia a
	where	a.nr_sequencia	= nr_sequencia_p
	and		a.nr_prescricao = nr_prescricao_p;
else
	select 	max(a.ie_respiracao),
			max(a.ie_disp_resp_esp),
			max(a.cd_modalidade_vent)
	into	ie_respiracao_w,
			ie_disp_resp_esp_w,
			cd_modalidade_vent_w
	from	prescr_gasoterapia a
	where	a.nr_seq_gas	= nr_seq_gas_p
	and		a.nr_prescricao = nr_prescricao_p;
end if;

select	max(cd_setor_atendimento),
		max(nr_atendimento),
		max(cd_local_estoque),
		max(dt_prescricao),
		max(cd_estabelecimento),
		max(nr_horas_validade)
into	cd_setor_atend_prescr_w,
		nr_atendimento_w,
		cd_local_estoque_w,
		dt_prescricao_w,
		cd_estabelecimento_w,
		nr_horas_validade_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	max(obter_Convenio_atendimento(nr_atendimento_w)),
		max(obter_categoria_atendimento(nr_atendimento_w)),
		max(obter_setor_atendimento(nr_atendimento_w))
into	cd_convenio_w,
		cd_categoria_w,
		cd_setor_atend_w
from	dual;

select	Obter_Atepacu_paciente(nr_atendimento_w, 'IAA')
into	nr_seq_atepacu_w
from 	dual;

Obter_Param_Usuario(1113,502,Obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_data_evento_w);
Obter_Param_Usuario(1113,663, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_lanca_itens_assoc_w);

if	(ie_data_evento_w = 'S') then
	dt_prescricao_w	:= nvl(nvl(dt_fim_p, dt_inicio_p),sysdate);
end if;

select	nvl(max(ie_regra_restritiva),'N')
into	ie_regra_restritiva_w
from	gas
where	nr_sequencia = nr_seq_gas_p;


if	(ie_regra_restritiva_w = 'N') then

	open C01;
	loop
	fetch C01 into	
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_procedimento_w,
		qt_minutos_regra_w,
		ie_divide_conta_w,
		ie_momento_geracao_w,
		ie_arredondar_w,		
		ie_quant_proporcional_w,
		qt_minuto_tolerancia_w,
		nr_seq_proc_interno_w,
		ie_quant_validade_w,
		cd_setor_conta_w;
	exit when C01%notfound;

		ie_regra_setor_w	:= 'N';
		cd_setor_atendimento_w := cd_setor_atend_prescr_w;
	
		if	(ie_momento_geracao_w = 'P') and
			(ie_quant_validade_w = 'S') and
			(nr_horas_validade_w is not null) then
			qt_procedimento_w	:= nr_horas_validade_w;
		end if;
	
		if	(nr_seq_proc_interno_w is not null) then
			Obter_Proc_Tab_Interno(nr_seq_proc_interno_w,null,nr_atendimento_w, 0, cd_procedimento_w, ie_origem_proced_w,null,null);
		end if;
		
		select	sum(a.qt_procedimento)
		into	qt_lancado_w
		from	conta_paciente b,
				procedimento_paciente a
		where	a.nr_prescricao		= nr_prescricao_p
		and		a.nr_interno_conta	= b.nr_interno_conta
		and		a.cd_procedimento	= cd_procedimento_w
		and		a.ie_origem_proced	= ie_origem_proced_w
		and		b.nr_atendimento	= nr_atendimento_w;
		
		if	((nvl(ie_momento_geracao_w,'T')	= 'T') and
			 (ie_evento_p			= 'T')) or
			((nvl(ie_momento_geracao_w,'T')	= 'I') and
			 (ie_evento_p			= 'I')) or
			((nvl(ie_momento_geracao_w,'T')	= 'X') and
			 (ie_evento_p			= 'X')) or
			((nvl(ie_momento_geracao_w,'T')	= 'P') and
			 (ie_evento_p			= 'P')) then
			
			if	(qt_min_aplic_gas_w > qt_minuto_tolerancia_w) then
				qt_min_aplic_gas_ww	:= qt_min_aplic_gas_w - qt_minuto_tolerancia_w;
				if	(ie_arredondar_w = 'S') then
					if	(qt_min_aplic_gas_w > 60) then
						qt_min_aplic_gas_ww	:= mod(qt_min_aplic_gas_w,60);
						if 	(qt_min_aplic_gas_ww > 0) then
							qt_min_aplic_gas_ww := qt_min_aplic_gas_w + (60 - qt_min_aplic_gas_ww);
						else
							qt_min_aplic_gas_ww	:= qt_min_aplic_gas_w;
						end if;
					elsif	(qt_min_aplic_gas_w < 60) then
						qt_min_aplic_gas_ww	:= 60;
					end if;
				end if;
						
				if	(nvl(ie_unidade_med_w,'lpm') = 'lpm') and
					(qt_gas_w > 0) and
					(ie_quant_proporcional_w = 'S') then 
					
					if ((ie_calcula_w) or (ie_evento_p <> 'T'))  then
						qt_procedimento_w	:= qt_gas_w * qt_minutos_regra_w;						
					else
						qt_procedimento_w 	:= qt_gasAcumulado_w;			
						qt_minutos_regra_w	:= 0;						
					end if;
					
				end if;

				if	(nvl(ie_divide_conta_w,'N') = 'N') then
					begin
					if	(nvl(qt_minutos_regra_w,0) = 0) then
						begin	

						if	(ie_estorno_p = 'S') then
							qt_procedimento_w	:= qt_procedimento_w * -1;
						end if;
						ie_alerta_w := 'S';
						
						if	(nr_atendimento_w is not null) then
														
							if	(cd_setor_conta_w > 0)then
								ie_regra_setor_w	:= 'S';
								cd_setor_atendimento_w	:= cd_setor_conta_w;
							end if;
														
							Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, null,null, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_procedimento_w, cd_setor_atendimento_w, 1, dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p, null,null,null,null,ie_regra_setor_w);
						end if;
						
						
						if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
							open C02;
							loop
							fetch C02 into	
								cd_material_w,
								qt_dose_w,
								cd_unid_med_dose_w;
							exit when C02%notfound;
								begin
								qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
								Inserir_Material_Atend_Pac(nr_atendimento_w, null, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
							
								atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);
							
							
								end;
							end loop;
							close C02;
						end if;
						end;
					else

						qt_minutos_aux_w	:= qt_min_aplic_gas_ww;
						qt_proced_regra_w	:= 0;

						while	(qt_minutos_aux_w > 0) loop
							qt_proced_regra_w	:= qt_proced_regra_w	+ qt_procedimento_w;
							qt_minutos_aux_w	:= qt_minutos_aux_w	- qt_minutos_regra_w;	
						end loop;
						
						qt_proced_regra_w := qt_proced_regra_w - nvl(qt_lancado_w,0);

						if	(qt_proced_regra_w > 0) then
							begin			
							if	(nr_atendimento_w is not null) then
							
								if	(cd_setor_conta_w > 0)then
									ie_regra_setor_w	:= 'S';
									cd_setor_atendimento_w	:= cd_setor_conta_w;
								end if;
							
								Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, null,null, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_proced_regra_w, cd_setor_atendimento_w, 1, dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p, null, null, null, null, ie_regra_setor_w);
							end if;
							if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
							   open C02;
							   loop
							   fetch C02 into	
								   cd_material_w,
								   qt_dose_w,
								   cd_unid_med_dose_w;
							   exit when C02%notfound;
								   begin			
								   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
								   Inserir_Material_Atend_Pac(nr_atendimento_w, null, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
								   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);				
								   end;
							   end loop;
							   close C02;
							end if;						   
							end;	
						end if;

					end if;
					end;
				else

					if	(nvl(qt_minutos_regra_w,0) = 0) then
						begin		

						if	(ie_estorno_p = 'S') then
							qt_procedimento_w	:= qt_procedimento_w * -1;
						end if;
						
						ie_alerta_w := 'S';
						if	(nr_atendimento_w is not null) then
							if	(cd_setor_conta_w > 0)then
								ie_regra_setor_w	:= 'S';
								cd_setor_atendimento_w	:= cd_setor_conta_w;
							end if;
						
							Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null,null, null, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_procedimento_w, cd_setor_atendimento_w, 1,dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p,null, null, null, null, ie_regra_setor_w);
						end if;
						
						
						if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
						   open C02;
						   loop
						   fetch C02 into	
							   cd_material_w,
							   qt_dose_w,
							   cd_unid_med_dose_w;
						   exit when C02%notfound;
							   begin
							   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
							   Inserir_Material_Atend_Pac(nr_atendimento_w, null, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
							   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);
							   end;
						   end loop;
						   close C02;
						end if;
						end;
					else

						select	max(nr_interno_conta)
						into	nr_interno_conta_w
						from	conta_paciente
						where	nr_atendimento		= nr_atendimento_w
						and	ie_status_acerto 	= 1
						and	Obter_Tipo_Convenio(cd_convenio_parametro) <> 1
						and	dt_inicio_p		>= dt_periodo_inicial
						and	dt_fim_p		<= dt_periodo_final;
						
						if	(nr_interno_conta_w is null) then
							select	max(nr_interno_conta)
							into	nr_interno_conta_w
							from	conta_paciente
							where	nr_atendimento		= nr_atendimento_w
							and	ie_status_acerto 	= 1
							and	dt_inicio_p		>= dt_periodo_inicial
							and	dt_fim_p		<= dt_periodo_final;
						end if;
						
						/*Verificar se possui uma conta que se encaixe em todo o periodo da gasoterapia, entao lancar nesta conta!*/
						if	(nr_interno_conta_w is not null) then

							qt_minutos_aux_w	:= qt_min_aplic_gas_ww;
							qt_proced_regra_w	:= 0;
							while	(qt_minutos_aux_w > 0) loop
								qt_proced_regra_w	:= qt_proced_regra_w + qt_procedimento_w;
								qt_minutos_aux_w	:= qt_minutos_aux_w - qt_minutos_regra_w;	
							end loop;
							
							qt_proced_regra_w := qt_proced_regra_w - nvl(qt_lancado_w,0);

							if	(qt_proced_regra_w > 0) then

								ie_alerta_w := 'S';
								if	(nr_atendimento_w is not null) then
								
									if	(cd_setor_conta_w > 0)then
										ie_regra_setor_w	:= 'S';
										cd_setor_atendimento_w	:= cd_setor_conta_w;
									end if;
								
									Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, null,nr_interno_conta_w, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_proced_regra_w, cd_setor_atendimento_w, 1, dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p,null,null,null,null,ie_regra_setor_w);
								end if;
								
								if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
								   open C02;
								   loop
								   fetch C02 into	
									   cd_material_w,
									   qt_dose_w,
									   cd_unid_med_dose_w;
								   exit when C02%notfound;
									   begin			
									   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
									   Inserir_Material_Atend_Pac(nr_atendimento_w, nr_interno_conta_w, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
									   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);				
									   end;
								   end loop;
								   close C02;
								end if;
							end if;
						else

							dt_referencia_w		:= dt_inicio_p;
							qt_minutos_aux_w	:= qt_min_aplic_gas_ww;
							/*Lancar um procedimento para as quantidades de acordo com os periodos da conta.*/
							
							select	max(nr_interno_conta),
								max(dt_periodo_final)
							into	nr_interno_conta_w,
								dt_periodo_final_w
							from	conta_paciente
							where	nr_atendimento	= nr_atendimento_w
							and	ie_status_acerto = 1
							and	Obter_Tipo_Convenio(cd_convenio_parametro) <> 1
							and	dt_referencia_w between dt_periodo_inicial and dt_periodo_final;
							
							if	(nr_interno_conta_w is null) then
								select	max(nr_interno_conta),
									max(dt_periodo_final)
								into	nr_interno_conta_w,
									dt_periodo_final_w
								from	conta_paciente
								where	nr_atendimento	= nr_atendimento_w
								and	ie_status_acerto = 1
								and	dt_referencia_w between dt_periodo_inicial and dt_periodo_final;
								
								if	(dt_periodo_final_w is null) then
									dt_periodo_final_w := sysdate;
								end if;
							end if;
							
							ie_controle_w	:= 0;
							ie_sair_w	:= 'N';
							qt_proced_regra_w := 0;
							while	((qt_minutos_aux_w > 0) or
								(ie_sair_w	= 'N')) and
								(ie_controle_w	 < 5000) loop						
								ie_controle_w	:= ie_controle_w +1;
								if	(dt_referencia_w <= dt_periodo_final_w) and
									(qt_minutos_aux_w > 0) then
									dt_referencia_w		:= dt_referencia_w + qt_minutos_regra_w/1440;
									qt_proced_regra_w	:= qt_proced_regra_w + qt_procedimento_w;
									qt_minutos_aux_w	:= qt_minutos_aux_w - qt_minutos_regra_w;
									ie_sair_w		:= 'N';
								else		
									if	(qt_minutos_aux_w <= 0) then
										ie_sair_w		:= 'S';
									end if;
								
									if	(qt_proced_regra_w > 0) then

										ie_alerta_w := 'S';
										if	(nr_atendimento_w is not null) then
											if	(cd_setor_conta_w > 0)then
												ie_regra_setor_w	:= 'S';
												cd_setor_atendimento_w	:= cd_setor_conta_w;
											end if;
										
											Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, null,nr_interno_conta_w, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_proced_regra_w, cd_setor_atendimento_w, 1, dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p,null,null,null,null,ie_regra_setor_w);
										end if;
										
										qt_proced_regra_w	:= 0;
										
										if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
										   open C02;
										   loop
										   fetch C02 into	
											   cd_material_w,
											   qt_dose_w,
											   cd_unid_med_dose_w;
										   exit when C02%notfound;
											   begin			
											   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
											   Inserir_Material_Atend_Pac(nr_atendimento_w, nr_interno_conta_w, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
											   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);				
											   end;
										   end loop;
										   close C02;
										end if;   
									end if;
									
									select	max(nr_interno_conta),
										max(dt_periodo_final)
									into	nr_interno_conta_w,
										dt_periodo_final_w
									from	conta_paciente
									where	nr_atendimento	= nr_atendimento_w
									and	ie_status_acerto = 1
									and	Obter_Tipo_Convenio(cd_convenio_parametro) <> 1
									and	dt_referencia_w between dt_periodo_inicial and dt_periodo_final;
									
									if	(nr_interno_conta_w is null) then
										select	max(nr_interno_conta),
											max(dt_periodo_final)
										into	nr_interno_conta_w,
											dt_periodo_final_w
										from	conta_paciente
										where	nr_atendimento	= nr_atendimento_w
										and	ie_status_acerto = 1
										and	dt_referencia_w between dt_periodo_inicial and dt_periodo_final;
									end if;							
								end if;
							end loop;
						
						end if;
					
					end if;
				
				end if;
			end if;
		end if;
		
	end loop;
	close C01;
else
	begin
	
	open c01;
	loop
	fetch c01 into	
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_procedimento_w,
		qt_minutos_regra_w,
		ie_divide_conta_w,
		ie_momento_geracao_w,
		ie_arredondar_w,
		ie_quant_proporcional_w,
		qt_minuto_tolerancia_w,
		nr_seq_proc_interno_w,
		ie_quant_validade_w,
		cd_setor_conta_w;
	exit when c01%notfound;
		Exit;
	end loop;
	close c01;
	
	ie_regra_setor_w	:= 'N';
							
	if	(ie_momento_geracao_w = 'P') and
		(ie_quant_validade_w = 'S') and
		(nr_horas_validade_w is not null) then
		qt_procedimento_w	:= nr_horas_validade_w;
	end if;
	
	if	(cd_procedimento_w > 0) or
		(nr_seq_proc_interno_w > 0) then
		if	(nr_seq_proc_interno_w is not null) then
			Obter_Proc_Tab_Interno(nr_seq_proc_interno_w,null,nr_atendimento_w, 0, cd_procedimento_w, ie_origem_proced_w,null,null);
		end if;
		
		select	sum(a.qt_procedimento)
		into	qt_lancado_w
		from	conta_paciente b,
				procedimento_paciente a
		where	a.nr_prescricao		= nr_prescricao_p
		and		a.nr_interno_conta	= b.nr_interno_conta
		and		a.cd_procedimento	= cd_procedimento_w
		and		a.ie_origem_proced	= ie_origem_proced_w
		and		b.nr_atendimento	= nr_atendimento_w;

		if	((nvl(ie_momento_geracao_w,'T')	= 'T') and
			 (ie_evento_p			= 'T')) or
			((nvl(ie_momento_geracao_w,'T')	= 'I') and
			 (ie_evento_p			= 'I')) or
			((nvl(ie_momento_geracao_w,'T')	= 'X') and
			 (ie_evento_p			= 'X')) or
			((nvl(ie_momento_geracao_w,'T')	= 'P') and
			 (ie_evento_p			= 'P')) then

			if	(qt_min_aplic_gas_w > qt_minuto_tolerancia_w) then
				qt_min_aplic_gas_ww	:= qt_min_aplic_gas_w - qt_minuto_tolerancia_w;
				if	(ie_arredondar_w = 'S') then
					if	(qt_min_aplic_gas_w > 60) then
						qt_min_aplic_gas_ww	:= mod(qt_min_aplic_gas_w,60);
						if 	(qt_min_aplic_gas_ww > 0) then
							qt_min_aplic_gas_ww := qt_min_aplic_gas_w + (60 - qt_min_aplic_gas_ww);
						else
							qt_min_aplic_gas_ww	:= qt_min_aplic_gas_w;
						end if;
					elsif	(qt_min_aplic_gas_w < 60) then
						qt_min_aplic_gas_ww	:= 60;
					end if;
				end if;
			
			
				if	(ie_unidade_med_w = 'lpm') and
					(qt_gas_w > 0) and
					(ie_quant_proporcional_w = 'S') then
										
					if ((ie_calcula_w) or (ie_evento_p <> 'T'))  then
						qt_procedimento_w	:= qt_gas_w * qt_minutos_regra_w;					
					else
						qt_procedimento_w := qt_gasAcumulado_w;
						qt_minutos_regra_w	:= 0;						
					end if;
					
				end if;

				if	(nvl(ie_divide_conta_w,'N') = 'N') then

					if	(nvl(qt_minutos_regra_w,0) = 0) then
						begin	
						if	(ie_estorno_p = 'S') then
							qt_procedimento_w	:= qt_procedimento_w * -1;
						end if;
						ie_alerta_w := 'S';
						if	(nr_atendimento_w is not null) then
							if	(cd_setor_conta_w > 0)then
								ie_regra_setor_w	:= 'S';
								cd_setor_atendimento_w	:= cd_setor_conta_w;
							end if;
						
							Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, null,null, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_procedimento_w, cd_setor_atendimento_w, 1, dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p,null,null,null,null,ie_regra_setor_w);
						end if;
						
						if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
						   open C02;
						   loop
						   fetch C02 into	
							   cd_material_w,
							   qt_dose_w,
							   cd_unid_med_dose_w;
						   exit when C02%notfound;
							   begin
							   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
							   Inserir_Material_Atend_Pac(nr_atendimento_w, null, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
							
							   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);
							
							
							   end;
						   end loop;
						   close C02;
						end if;
						end;
					else

						qt_minutos_aux_w	:= qt_min_aplic_gas_ww;
						qt_proced_regra_w	:= 0;

						while	(qt_minutos_aux_w > 0) loop
							qt_proced_regra_w	:= qt_proced_regra_w	+ qt_procedimento_w;
							qt_minutos_aux_w	:= qt_minutos_aux_w	- qt_minutos_regra_w;	
						end loop;

						qt_proced_regra_w := qt_proced_regra_w - nvl(qt_lancado_w,0);

						if	(qt_proced_regra_w > 0) then
							begin			
							
							if	(nr_atendimento_w is not null) then
								if	(cd_setor_conta_w > 0)then
									ie_regra_setor_w	:= 'S';
									cd_setor_atendimento_w	:= cd_setor_conta_w;
								end if;
							
								Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, null,null, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_proced_regra_w, cd_setor_atendimento_w, 1, dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p,null,null,null,null,ie_regra_setor_w);
							end if;
							
							if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
							   open C02;
							   loop
							   fetch C02 into	
								   cd_material_w,
								   qt_dose_w,
								   cd_unid_med_dose_w;
							   exit when C02%notfound;
								   begin			
								   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
								   Inserir_Material_Atend_Pac(nr_atendimento_w, null, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
								   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);				
								   end;
							   end loop;
							   close C02;
							end if;						   
							end;	
						end if;

					end if;
				else

					if	(nvl(qt_minutos_regra_w,0) = 0) then
						begin		

						if	(ie_estorno_p = 'S') then
							qt_procedimento_w	:= qt_procedimento_w * -1;
						end if;
						ie_alerta_w := 'S';
						
						if	(nr_atendimento_w is not null) then
							if	(cd_setor_conta_w > 0)then
								ie_regra_setor_w	:= 'S';
								cd_setor_atendimento_w	:= cd_setor_conta_w;
							end if;
							
							Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null,null, null, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_procedimento_w, cd_setor_atendimento_w, 1,dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p,null,null,null,null,ie_regra_setor_w);
						end if;
						
						if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
						   open C02;
						   loop
						   fetch C02 into	
							   cd_material_w,
							   qt_dose_w,
							   cd_unid_med_dose_w;
						   exit when C02%notfound;
							   begin
							   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
							   Inserir_Material_Atend_Pac(nr_atendimento_w, null, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
							   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);
							   end;
						   end loop;
						   close C02;
						end if;
						end;
					else

						select	max(nr_interno_conta)
						into	nr_interno_conta_w
						from	conta_paciente
						where	nr_atendimento		= nr_atendimento_w
						and	ie_status_acerto 	= 1
						and	Obter_Tipo_Convenio(cd_convenio_parametro) <> 1
						and	dt_inicio_p		>= dt_periodo_inicial
						and	dt_fim_p		<= dt_periodo_final;
						
						if	(nr_interno_conta_w is null) then
							select	max(nr_interno_conta)
							into	nr_interno_conta_w
							from	conta_paciente
							where	nr_atendimento		= nr_atendimento_w
							and	ie_status_acerto 	= 1
							and	dt_inicio_p		>= dt_periodo_inicial
							and	dt_fim_p		<= dt_periodo_final;
						end if;
						
						/*Verificar se possui uma conta que se encaixe em todo o periodo da gasoterapia, entao lancar nesta conta!*/
						if	(nr_interno_conta_w is not null) then

							qt_minutos_aux_w	:= qt_min_aplic_gas_ww;
							qt_proced_regra_w	:= 0;
							while	(qt_minutos_aux_w > 0) loop
								qt_proced_regra_w	:= qt_proced_regra_w + qt_procedimento_w;
								qt_minutos_aux_w	:= qt_minutos_aux_w - qt_minutos_regra_w;	
							end loop;

							qt_proced_regra_w := qt_proced_regra_w - nvl(qt_lancado_w,0);

							if	(qt_proced_regra_w > 0) then
								if	(ie_estorno_p = 'S') then
									qt_proced_regra_w	:= qt_proced_regra_w * -1;
								end if;
								ie_alerta_w := 'S';
								
								if	(nr_atendimento_w is not null) then
									if	(cd_setor_conta_w > 0)then
										ie_regra_setor_w	:= 'S';
										cd_setor_atendimento_w	:= cd_setor_conta_w;
									end if;
									Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, null,nr_interno_conta_w, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_proced_regra_w, cd_setor_atendimento_w, 1, dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p,null,null,null,null,ie_regra_setor_w);
								end if;
								
								if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
								   open C02;
								   loop
								   fetch C02 into	
									   cd_material_w,
									   qt_dose_w,
									   cd_unid_med_dose_w;
								   exit when C02%notfound;
									   begin			
									   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
									   Inserir_Material_Atend_Pac(nr_atendimento_w, nr_interno_conta_w, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
									   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);				
									   end;
								   end loop;
								   close C02;
								end if;
							end if;
						else

							dt_referencia_w		:= dt_inicio_p;
							qt_minutos_aux_w	:= qt_min_aplic_gas_ww;
							/*Lancar um procedimento para as quantidades de acordo com os periodos da conta.*/
							
							select	max(nr_interno_conta),
								max(dt_periodo_final)
							into	nr_interno_conta_w,
								dt_periodo_final_w
							from	conta_paciente
							where	nr_atendimento	= nr_atendimento_w
							and	ie_status_acerto = 1
							and	Obter_Tipo_Convenio(cd_convenio_parametro) <> 1
							and	dt_referencia_w between dt_periodo_inicial and dt_periodo_final;
							
							if	(nr_interno_conta_w is null) then
								select	max(nr_interno_conta),
									max(dt_periodo_final)
								into	nr_interno_conta_w,
									dt_periodo_final_w
								from	conta_paciente
								where	nr_atendimento	= nr_atendimento_w
								and	ie_status_acerto = 1
								and	dt_referencia_w between dt_periodo_inicial and dt_periodo_final;
								
								if	(dt_periodo_final_w is null) then
									dt_periodo_final_w := sysdate;
								end if;
							end if;
							
							ie_controle_w	:= 0;
							ie_sair_w	:= 'N';
							qt_proced_regra_w := 0;
							while	((qt_minutos_aux_w > 0) or
								(ie_sair_w	= 'N')) and
								(ie_controle_w	 < 5000) loop						
								ie_controle_w	:= ie_controle_w +1;
								if	(dt_referencia_w <= dt_periodo_final_w) and
									(qt_minutos_aux_w > 0) then
									dt_referencia_w		:= dt_referencia_w + qt_minutos_regra_w/1440;
									qt_proced_regra_w	:= qt_proced_regra_w + qt_procedimento_w;
									qt_minutos_aux_w	:= qt_minutos_aux_w - qt_minutos_regra_w;
									ie_sair_w		:= 'N';
								else		
									if	(qt_minutos_aux_w <= 0) then
										ie_sair_w		:= 'S';
									end if;
								
									if	(qt_proced_regra_w > 0) then
										if	(ie_estorno_p = 'S') then
											qt_proced_regra_w	:= qt_proced_regra_w * -1;
										end if;
										ie_alerta_w := 'S';
										
										if	(nr_atendimento_w is not null) then
											if	(cd_setor_conta_w > 0)then
												ie_regra_setor_w	:= 'S';
												cd_setor_atendimento_w	:= cd_setor_conta_w;
											end if;
										
											Gerar_Proc_Pac_Item_Prescr(nr_prescricao_p, null, null,nr_interno_conta_w, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_proced_regra_w, cd_setor_atendimento_w, 1, dt_prescricao_w, nm_usuario_p, null, null, null, null,nr_seq_gas_p,null,null,null,null,ie_regra_setor_w);
										end if;
										
										qt_proced_regra_w	:= 0;
										if (nvl(ie_lanca_itens_assoc_w,'S')	= 'S') then
										   open C02;
										   loop
										   fetch C02 into	
											   cd_material_w,
											   qt_dose_w,
											   cd_unid_med_dose_w;
										   exit when C02%notfound;
											   begin			
											   qt_dose_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_dose_w , qt_dose_w);
											   Inserir_Material_Atend_Pac(nr_atendimento_w, nr_interno_conta_w, cd_material_w, sysdate, cd_convenio_w, cd_categoria_w, nr_seq_atepacu_w, nm_usuario_p, qt_dose_w, cd_local_estoque_w, '1', null, nr_seq_atualiza_prec_w, null, null);
											   atualiza_preco_material(nr_seq_atualiza_prec_w, nm_usuario_p);				
											   end;
										   end loop;
										   close C02;
										end if;   
									end if;
									
									select	max(nr_interno_conta),
										max(dt_periodo_final)
									into	nr_interno_conta_w,
										dt_periodo_final_w
									from	conta_paciente
									where	nr_atendimento	= nr_atendimento_w
									and	ie_status_acerto = 1
									and	Obter_Tipo_Convenio(cd_convenio_parametro) <> 1
									and	dt_referencia_w between dt_periodo_inicial and dt_periodo_final;
									
									if	(nr_interno_conta_w is null) then
										select	max(nr_interno_conta),
											max(dt_periodo_final)
										into	nr_interno_conta_w,
											dt_periodo_final_w
										from	conta_paciente
										where	nr_atendimento	= nr_atendimento_w
										and	ie_status_acerto = 1
										and	dt_referencia_w between dt_periodo_inicial and dt_periodo_final;
									end if;							
								end if;
							end loop;
						
						end if;
					
					end if;
				
				end if;
			end if;
		end if;
		
	end if;
	end;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Gerar_cobranca_gasoterapia;
/
