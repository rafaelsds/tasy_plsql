create or replace
procedure create_indicator(
		nr_seq_dashboard_p		number,
		nr_seq_indicator_p		number,
		nm_usuario_p			varchar2,
		nr_seq_information_p		number default null) is

nr_sequencia_w					dashboard_indicator.nr_sequencia%type;
begin

select	dashboard_indicator_seq.nextval
into	nr_sequencia_w
from	dual;

insert into dashboard_indicator (
			nr_sequencia,
			nr_seq_dashboard,
			nr_seq_indicator,
			dt_atualizacao,
			nm_usuario)
values(			nr_sequencia_w,
			nr_seq_dashboard_p,
			nr_seq_indicator_p,
			sysdate,
			nm_usuario_p);

commit;

if	(nvl(nr_seq_information_p, '0') != '0') then
	create_information(nr_seq_dashboard_p, nr_sequencia_w, nr_seq_information_p, nm_usuario_p);
end if;

commit;

end create_indicator;
/