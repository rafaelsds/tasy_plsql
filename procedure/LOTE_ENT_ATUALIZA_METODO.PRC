create or replace
procedure lote_ent_atualiza_metodo(
			nr_seq_exame_p		number,
			nr_seq_prescr_p		number,
			nr_seq_resultado_p	number,
			nr_seq_metodo_p		number) is 

begin

if 	(nr_seq_exame_p is not null) and
	(nr_seq_prescr_p is not null) and
	(nr_seq_resultado_p is not null) then

	update	exame_lab_result_item
	set		nr_seq_metodo = nr_seq_metodo_p
	where	nr_seq_exame = nr_seq_exame_p
	and		nr_seq_resultado = nr_seq_resultado_p
	and		nr_seq_prescr = nr_seq_prescr_p;
	
end if;

commit;

end lote_ent_atualiza_metodo;
/