create or replace
procedure Gerar_transf_agendamentos(ds_lista_p		varchar2,
				    cd_agenda_p		number,
				    dt_agenda_p		date,
				    ie_feriado_p	varchar2,
				    ie_sobra_horario_p	varchar2,
				    nm_usuario_p	Varchar2,
				    cd_estabelecimento_p number,
				    nr_seq_atendimento_p number,
				    nr_seq_evento_atend_p number,
				    ds_erro_p		out varchar2) is 

ds_lista_w			varchar2(4000);
tam_lista_w			number(10,0);
ie_pos_virgula_w		number(3,0);
nr_seq_agendamento_w		number(10,0);

nr_seq_livre_w			number(10,0);
nr_seq_encaixe_w		number(10,0);
hr_agenda_w			date;
dt_agenda_w			date;
nr_minuto_duracao_w		number(10,0);
ds_erro_w			varchar2(255);
				    
begin

ds_lista_w 	:= ds_lista_p;
ds_erro_p	:= '';

while	(ds_lista_w is not null)  loop
	begin
	tam_lista_w		:= length(ds_lista_w);
	ie_pos_virgula_w	:= instr(ds_lista_w,',');

	if	(ie_pos_virgula_w <> 0) then
		nr_seq_agendamento_w	:= to_number(substr(ds_lista_w,1,(ie_pos_virgula_w - 1)));
		ds_lista_w		:= substr(ds_lista_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;

	horario_livre_consulta(
			cd_estabelecimento_p,           
			cd_agenda_p,                    
			ie_feriado_p,                   
			dt_agenda_p,                    
			nm_usuario_p,                   
			'S',                    
			ie_sobra_horario_p,             
			'N',              
			0,
			ds_erro_w);
			
	
	select	max(dt_agenda),
		max(dt_agenda),
		max(nr_minuto_duracao)
	into	dt_agenda_w,
		hr_agenda_w,
		nr_minuto_duracao_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agendamento_w;

	
	select	max(nr_sequencia)
	into	nr_seq_livre_w
	from	agenda_consulta
	where	cd_agenda = cd_agenda_p
	and	dt_agenda = to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(dt_agenda_w,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss')
	and	ie_status_agenda = 'L';
	
	if	(nr_seq_livre_w > 0) then
		copiar_transferir_agenda_cons(
					cd_estabelecimento_p,
					nr_seq_agendamento_w,
					nr_seq_livre_w,
					'T',
					null,
					null,
					nm_usuario_p,
					nr_seq_atendimento_p,
					nr_seq_evento_atend_p,
					ds_erro_w);
	else
		ds_erro_p := ds_erro_p || to_char(hr_agenda_w,'hh24:mi') ||',';		
	end if;

	end;
	end loop;
	
commit;

ds_erro_p	:= substr(ds_erro_p,1,length(ds_erro_p)-1);

end Gerar_transf_agendamentos;
/
