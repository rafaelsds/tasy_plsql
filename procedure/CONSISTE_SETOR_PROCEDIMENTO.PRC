CREATE OR REPLACE PROCEDURE consiste_setor_procedimento (
    nr_seq_propaci_p     IN   NUMBER,
    nm_usuario_regra_p   IN   VARCHAR2,
    ie_medico_executor_p OUT NOCOPY VARCHAR2,
    cd_cgc_p OUT NOCOPY VARCHAR2,
    cd_medico_executor_p OUT NOCOPY VARCHAR2,
    cd_pessoa_fisica_p OUT NOCOPY VARCHAR2
) IS

    cd_convenio_w             convenio.cd_convenio%TYPE;
    cd_procedimento_w         procedimento.cd_procedimento%TYPE;
    ie_origem_proced_w        procedimento.ie_origem_proced%TYPE;
    cd_setor_atendimento_w    procedimento_paciente.cd_setor_atendimento%TYPE;
    ie_tipo_atendimento_w     atendimento_paciente.ie_tipo_atendimento%TYPE;
    nr_seq_exame_w            procedimento_paciente.nr_seq_exame%TYPE;
    nr_seq_proc_interno_w     procedimento_paciente.nr_seq_proc_interno%TYPE;
    dt_prescricao_w           procedimento_paciente.dt_prescricao%TYPE;
    nr_seq_classificacao_w    atendimento_paciente.nr_seq_classificacao%TYPE;
    ie_origem_inf_w           prescr_procedimento.ie_origem_inf%TYPE;
    cd_cgc_laboratorio_w      prescr_procedimento.cd_cgc_laboratorio%TYPE;
    cd_setor_prescr_w         prescr_medica.cd_setor_atendimento%TYPE;
    cd_medico_exec_filtro_w   procedimento_paciente.cd_pessoa_fisica%TYPE;
BEGIN
    SELECT
        a.cd_convenio,
        a.cd_setor_atendimento,
        a.cd_procedimento,
        a.ie_origem_proced,
        d.ie_tipo_atendimento,
        a.nr_seq_exame,
        a.nr_seq_proc_interno,
        nvl(a.cd_pessoa_fisica, a.cd_medico_executor),
        a.dt_prescricao,
        d.nr_seq_classificacao,
        b.ie_origem_inf,
        b.cd_cgc_laboratorio,
        c.cd_setor_atendimento
    INTO
        cd_convenio_w,
        cd_setor_atendimento_w,
        cd_procedimento_w,
        ie_origem_proced_w,
        ie_tipo_atendimento_w,
        nr_seq_exame_w,
        nr_seq_proc_interno_w,
        cd_medico_exec_filtro_w,
        dt_prescricao_w,
        nr_seq_classificacao_w,
        ie_origem_inf_w,
        cd_cgc_laboratorio_w,
        cd_setor_prescr_w
    FROM
        procedimento_paciente   a
        INNER JOIN prescr_procedimento     b ON a.nr_prescricao = b.nr_prescricao
                                            AND a.nr_sequencia_prescricao = b.nr_sequencia
        INNER JOIN prescr_medica           c ON b.nr_prescricao = c.nr_prescricao
        INNER JOIN atendimento_paciente    d ON c.nr_atendimento = d.nr_atendimento
    WHERE
        a.nr_sequencia = nr_seq_propaci_p;

    consiste_medico_executor(cd_estabelecimento_p => wheb_usuario_pck.get_cd_estabelecimento,
        cd_convenio_p => cd_convenio_w, 
        cd_setor_atendimento_p => cd_setor_atendimento_w,
        cd_procedimento_p => cd_procedimento_w, 
        ie_origem_proced_p => ie_origem_proced_w, 
        ie_tipo_atendimento_p => ie_tipo_atendimento_w, 
        nr_seq_exame_p => nr_seq_exame_w, 
        nr_seq_proc_interno_p => nr_seq_proc_interno_w, 
        ie_medico_executor_p => ie_medico_executor_p, 
        cd_cgc_p => cd_cgc_p, 
        cd_medico_executor_p => cd_medico_executor_p, 
        cd_pessoa_fisica_p => cd_pessoa_fisica_p, 
        cd_medico_exec_filtro_p => nm_usuario_regra_p, 
        dt_execucao_p => dt_prescricao_w, 
        nr_seq_classificacao_p => nr_seq_classificacao_w,
        ie_origem_inf_p => ie_origem_inf_w, 
        cd_cgc_laboratorio_p => cd_cgc_laboratorio_w, 
        cd_setor_prescricao_p => cd_setor_prescr_w);
    EXCEPTION
        WHEN no_data_found THEN
            gravar_log_lab_pragma(1079, 'Sequence of table "PROCEDIMENTO_PACIENTE" not found. CALLSTACK: ' || dbms_utility.format_call_stack, nm_usuario_regra_p, nr_seq_propaci_p, NULL);
        WHEN too_many_rows THEN
            gravar_log_lab_pragma (1079, 'Multiple records was found in the object "CONSISTE_SETOR_PROCEDIMENTO". CALLSTACK: ' || dbms_utility.format_call_stack, nm_usuario_regra_p, nr_seq_propaci_p, null);
        WHEN others THEN
            gravar_log_lab_pragma(1079, 'Failed to execute "CONSISTE_MEDICO_EXECUTOR" procedure. CALLSTACK: ' || dbms_utility.format_call_stack, nm_usuario_regra_p, nr_seq_propaci_p, NULL);
END consiste_setor_procedimento;
/
