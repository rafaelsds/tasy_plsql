create or replace
procedure substituir_medicamento (	
					nr_prescricao_p					number,
					nr_seq_medic_p					number,
					cd_estabelecimento_p			number,
					nm_usuario_p					varchar2,
					nr_seq_medic_novo_p			out	number) is

qt_registro_w					number(5,0);
cd_motivo_baixa_w				varchar2(3);
nr_seq_medic_w					number(6,0);
nr_agrup_medic_w				number(7,1);
nr_sequencia_w					number(6,0);
ie_reg_alter_medico_farm_w		varchar2(1);
cd_pessoa_alteracao_w			varchar2(10);
ie_agrupador_w					number(2);
nr_seq_motivo_w					number(10,0) := null;
ie_agora_w						varchar2(1);
ie_acm_w						varchar2(1);
ie_se_necessario_w				varchar2(1);
qt_item_w						number(10);
ie_acm_sn_w						varchar2(1);
nr_seq_alteracao_w				number(10);
nr_atendimento_w				number(10);
cd_material_w					number(10);
ie_gera_dilui_rescont_w			varchar2(1);
nr_agrupamento_w				prescr_material.nr_agrupamento%type;
cd_perfil_ativo_w				perfil.cd_perfil%type;
ie_reordenar_medicamentos_w		varchar2(1 char);


cursor C01 is
select	nr_sequencia,
		ie_agrupador
from	prescr_material
where	nr_prescricao		= nr_prescricao_p
and		nr_sequencia		= nr_seq_medic_p
order by
		nr_sequencia;

begin

cd_perfil_ativo_w := obter_perfil_ativo;

obter_param_usuario(7010, 48, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_reg_alter_medico_farm_w);
obter_param_usuario(7010, 57, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, nr_seq_motivo_w);
obter_param_usuario(7010, 147, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_gera_dilui_rescont_w);

obter_param_usuario(924, 157, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, ie_reordenar_medicamentos_w);
obter_param_usuario(924, 194, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_p, cd_motivo_baixa_w);

/* Verifica se o material ja foi atendido */
begin
select	count(*)
into	qt_registro_w
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and		nr_sequencia	= nr_seq_medic_p
and		cd_motivo_baixa	<> 0
and		dt_baixa	is not null;
exception
	when others then
	qt_registro_w	:= 0;
end;

if	(qt_registro_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(201232);
	--r a i s e _ application_error(-20011, 'este medicamento ja foi atendido e nao pode ser substituido.#@#@');
end if;

if	(cd_motivo_baixa_w is null) or
	(somente_numero(cd_motivo_baixa_w) = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(201233);
	--r a i s e _ application_error(-20011, 'Para poder substituir medicamentos, e necessario configurar o parametro 194 da funcao Prescricao Eletronica Paciente - REP.#@#@');
end if;

nr_seq_medic_w		:= null;

open C01;
loop
fetch C01 into
	nr_sequencia_w,
	ie_agrupador_w;
exit when C01%notfound;
	begin
	
	select	max(nr_sequencia)+1
	into	nr_seq_medic_w
	from	prescr_material
	where	nr_prescricao	= nr_prescricao_p;
	
	/* Gera agrupamento */
	select	max(nr_agrupamento)+1
	into	nr_agrup_medic_w
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p;

	select  nvl(max(a.ie_agora),'N'),
			nvl(max(a.ie_acm),'N'),
			nvl(max(a.ie_se_necessario),'N'),
			nvl(max(qt_dose),0)
	into	ie_agora_w,
			ie_acm_w,
			ie_se_necessario_w,
			qt_item_w
	from  	intervalo_prescricao a,
			prescr_material b
	where	b.nr_prescricao			= nr_prescricao_p
	and		b.nr_sequencia			= nr_sequencia_w
	and 	a.cd_intervalo 			= b.cd_intervalo
	and 	nvl(a.ie_situacao,'A') 	= 'A';

	select 	max(nr_atendimento)
	into	nr_atendimento_w
	from	prescr_medica
	where 	nr_prescricao = nr_prescricao_p;

	/* Insere o substituto */
	insert into prescr_material (
			nr_prescricao,
			nr_sequencia,
			ie_origem_inf,
			cd_material,
			cd_unidade_medida,
			qt_dose,
			qt_unitaria,
			qt_material,
			dt_atualizacao,
			nm_usuario,
			cd_intervalo,
			ds_horarios,
			ds_observacao,
			ds_observacao_far,
			ie_via_aplicacao,
			nr_agrupamento,
			cd_motivo_baixa,
			dt_baixa,
			ie_utiliza_kit,
			cd_unidade_medida_dose,
			qt_conversao_dose,
			ie_urgencia,
			nr_ocorrencia,
			qt_total_dispensar,
			cd_fornec_consignado,
			nr_sequencia_solucao,
			nr_sequencia_proc,
			qt_solucao,
			hr_dose_especial,
			qt_dose_especial,
			ds_dose_diferenciada,
			ie_medicacao_paciente,
			nr_sequencia_diluicao,
			hr_prim_horario,
			nr_dia_util,
			nr_sequencia_dieta,
			ie_agrupador,
			dt_emissao_setor_atend,
			ie_suspenso,
			ds_justificativa,
			qt_dias_solicitado,
			qt_dias_liberado,
			nm_usuario_liberacao,
			dt_liberacao,
			ie_se_necessario,
			qt_min_aplicacao,
			nr_seq_lote_fornec,
			ie_status_cirurgia,
			ie_bomba_infusao,
			ie_aplic_bolus,
			ie_aplic_lenta,
			ie_acm,
			cd_material_baixa,
			nr_seq_avaliacao,
			nr_seq_ordem_prod,
			qt_baixa_especial,
			ie_objetivo,
			qt_hora_aplicacao,
			ie_erro,
			cd_topografia_cih,
			ie_origem_infeccao,
			cd_amostra_cih,
			cd_microorganismo_cih,
			ie_cultura_cih,
			ie_antibiograma,
			ie_uso_antimicrobiano,
			cd_kit_material,
			cd_local_estoque,
			nr_seq_kit,
			dt_suspensao,
			nm_usuario_susp,
			qt_vel_infusao,
			cd_protocolo,
			nr_seq_protocolo,
			nr_seq_mat_protocolo,
			dt_alteracao_local,
			nm_usuario_alt_local,
			ie_recons_diluente_fixo,
			qt_dias_util,
			dt_fim_item,
			nr_seq_atend_medic,
			nr_seq_kit_estoque,
			cd_convenio,
			cd_categoria,
			ie_sem_aprazamento,
			ie_novo_ciclo_ccih,
			nr_receita,
			dt_status,
			ie_status,
			qt_volume_adep,
			ie_supera_limite_uso,
			ie_cobra_paciente,
			ie_forma_infusao,
			ie_indicacao,
			ie_dose_espec_agora,
			ie_tipo_medic_hd,
			cd_setor_exec_inic,
			cd_setor_exec_fim,
			nr_seq_pe_proc,
			ie_intervalo_dif,
			ds_reprov_ccih,
			qt_horas_estabilidade,
			dt_proxima_dose,
			ie_administrar,
			qt_total_dias_lib,
			ie_regra_disp,
			nr_seq_substituto,
			dt_inicio_medic,
			ie_item_superior,
			ie_kit_gerado)
	select	nr_prescricao,
			nr_seq_medic_w,	/* Nova sequencia da prescricao */
			ie_origem_inf,
			cd_material,
			cd_unidade_medida,
			qt_dose,
			qt_unitaria,
			qt_material,
			sysdate, 					/* Hora de substituicao */
			nm_usuario_p, 				/* Usuario que substituiu */
			cd_intervalo,
			ds_horarios,
			ds_observacao,
			ds_observacao_far,
			ie_via_aplicacao,
			nr_agrup_medic_w,			/* Novo agrupamento */
			0, 							/* Motivo de baixa */
			null, 						/* Data da baixa */
			ie_utiliza_kit,
			cd_unidade_medida_dose,
			qt_conversao_dose,
			ie_urgencia,
			nr_ocorrencia,
			qt_total_dispensar,
			cd_fornec_consignado,
			nr_sequencia_solucao,
			nr_sequencia_proc,
			qt_solucao,
			hr_dose_especial,
			qt_dose_especial,
			ds_dose_diferenciada,
			ie_medicacao_paciente,
			null,
			hr_prim_horario,
			nr_dia_util,
			nr_sequencia_dieta,
			ie_agrupador,
			dt_emissao_setor_atend,
			ie_suspenso,
			ds_justificativa,
			qt_dias_solicitado,
			qt_dias_liberado,
			nm_usuario_liberacao,
			dt_liberacao,
			ie_se_necessario,
			qt_min_aplicacao,
			nr_seq_lote_fornec,
			ie_status_cirurgia,
			ie_bomba_infusao,
			ie_aplic_bolus,
			ie_aplic_lenta,
			ie_acm,
			cd_material_baixa,
			nr_seq_avaliacao,
			nr_seq_ordem_prod,
			qt_baixa_especial,
			ie_objetivo,
			qt_hora_aplicacao,
			ie_erro,
			cd_topografia_cih,
			ie_origem_infeccao,
			cd_amostra_cih,
			cd_microorganismo_cih,
			ie_cultura_cih,
			ie_antibiograma,
			ie_uso_antimicrobiano,
			cd_kit_material,
			cd_local_estoque,
			nr_seq_kit,
			dt_suspensao,
			nm_usuario_susp,
			qt_vel_infusao,
			cd_protocolo,
			nr_seq_protocolo,
			nr_seq_mat_protocolo,
			dt_alteracao_local,
			nm_usuario_alt_local,
			ie_recons_diluente_fixo,
			qt_dias_util,
			dt_fim_item,
			nr_seq_atend_medic,
			nr_seq_kit_estoque,
			cd_convenio,
			cd_categoria,
			ie_sem_aprazamento,
			ie_novo_ciclo_ccih,
			nr_receita,
			dt_status,
			ie_status,
			qt_volume_adep,
			ie_supera_limite_uso,
			ie_cobra_paciente,
			ie_forma_infusao,
			ie_indicacao,
			ie_dose_espec_agora,
			ie_tipo_medic_hd,
			cd_setor_exec_inic,
			cd_setor_exec_fim,
			nr_seq_pe_proc,
			ie_intervalo_dif,
			ds_reprov_ccih,
			qt_horas_estabilidade,
			null,
			ie_administrar,
			qt_total_dias_lib,
			ie_regra_disp,
			null,				/* Sequencia do substituto */
			dt_inicio_medic,
			ie_item_superior,
			'N'
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and		nr_sequencia		= nr_sequencia_w;
	
	/* Atualiza informacoes do medicamento pai */
	update	prescr_material
	set		cd_motivo_baixa		= somente_numero(cd_motivo_baixa_w),
			dt_baixa			= sysdate,
			nr_seq_substituto	= nr_seq_medic_w,
			nm_usuario			= nm_usuario_p,
			ie_urgencia			= ie_agora_w,
			ie_acm				= ie_acm_w,
			ie_se_necessario 	= ie_se_necessario_w
	where	nr_prescricao		= nr_prescricao_p
	and		nr_sequencia		= nr_sequencia_w;

	/* Atualizar o numero do agrupamento do medicamento que substituiu o antigo */
	select	max(nr_agrupamento)
	into	nr_agrupamento_w
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and		nr_sequencia		= nr_sequencia_w;
	
	update	prescr_material
	set		nr_agrupamento 		= nr_agrupamento_w
	where	nr_prescricao		= nr_prescricao_p
	and		nr_agrupamento		= nr_agrup_medic_w;

	select  nvl(max(cd_material),null)
	into	cd_material_w
	from	prescr_material b
	where	b.nr_prescricao						= nr_prescricao_p
	and		b.nr_sequencia						= nr_sequencia_w;

	select	max(OBTER_SE_ACM_SN(ie_acm, ie_se_necessario))
	into	ie_acm_sn_w
	from	prescr_material
	where	nr_prescricao 	= nr_prescricao_p
	and		nr_sequencia	= nr_sequencia_w;

	select	prescr_mat_alteracao_seq.nextval
	into	nr_seq_alteracao_w
	from	dual;

	insert into prescr_mat_alteracao	(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_prescricao,
						nr_seq_prescricao,
						dt_alteracao,
						cd_pessoa_fisica,
						ie_alteracao,
						nr_atendimento,
						qt_dose_original,
						ie_acm_sn,
						cd_item,
						ie_mostra_adep,
						ie_tipo_item
						)
					values	(
						nr_seq_alteracao_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_prescricao_p,
						nr_sequencia_w,
						sysdate,
						obter_dados_usuario_opcao(nm_usuario_p,'C'),
						28,
						nr_atendimento_w,
						qt_item_w,
						ie_acm_sn_w,
						cd_material_w,
						'S',
						'M'
						);

						
	Suspender_item_Prescricao(nr_prescricao_p,nr_sequencia_w,nr_seq_motivo_w,null,'PRESCR_MATERIAL',nm_usuario_p,'S',924,null,'S');
	
	Gerar_prescr_mat_sem_dt_lib(nr_prescricao_p, nr_seq_medic_w, obter_perfil_ativo, 'N', nm_usuario_p);

	if	(ie_reg_alter_medico_farm_w = 'S') and
		(ie_agrupador_w = 1) then
		
		select		max(cd_pessoa_fisica)
		into		cd_pessoa_alteracao_w
		from		usuario
		where		nm_usuario = nm_usuario_p;

		insert into	prescr_material_alteracao(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_prescricao,
					nr_seq_prescricao,
					ie_acao,
					cd_pessoa_alteracao)
				values(
					prescr_material_alteracao_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_prescricao_p,
					nr_seq_medic_w,
					'SM',
					cd_pessoa_alteracao_w);
	end if;

	end;
end loop;
close C01;

if	(nvl(nr_seq_medic_w,0) > 0) then
	insert	into prescr_material_cid (
			nr_sequencia, 
			cd_doenca_cid, 
			nr_prescricao, 
			nr_sequencia_medic, 
			dt_atualizacao, 
			nm_usuario, 
			dt_atualizacao_nrec, 
			nm_usuario_nrec)
	select 	prescr_material_cid_seq.nextval,
			cd_doenca_cid, 
			nr_prescricao_p,
			nr_seq_medic_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p
	from	prescr_material_cid
	where	nr_prescricao 	   = nr_prescricao_p
	and		nr_sequencia_medic = nr_seq_medic_p;
end if;		

if (ie_reordenar_medicamentos_w <> 'N') then
	/* Reordena os medicamentos */
	reordenar_medicamento(nr_prescricao_p);
end if;

/* Retorna a nova sequencia gerada */
nr_seq_medic_novo_p		:= nr_seq_medic_w;

commit;

end substituir_medicamento;
/
