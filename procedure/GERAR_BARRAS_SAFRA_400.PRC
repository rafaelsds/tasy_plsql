create or replace
procedure GERAR_BARRAS_SAFRA_400
		(nr_seq_banco_escrit_p	in	number,
		nm_usuario_p		in	varchar2,
		nr_seq_lote_p		in	number) is

cd_banco_w				varchar2(3);
cd_barras_w				varchar2(255);
ie_tipo_inscricao_w		varchar2(2);
nr_inscricao_w			varchar2(15);
cd_cgc_w				varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
nr_nosso_numero_w		varchar2(12);
ie_forma_importacao_w	varchar2(1);
cd_estabelecimento_w	number(4);
nr_seq_banco_escrit_w	number(10);
nm_pessoa_externo_w		varchar2(30);
dt_emissao_w			date;
cd_lancamento_w			varchar2(5);
dt_limite_desconto_w	date;
ds_limite_desconto_w	varchar2(8);
ds_sacador_avalista_w	varchar2(40);
nr_documento_w			varchar2(15);
nr_pagamento_w			varchar2(15);
nr_titulo_w				number(10);
nr_seq_escrit_barras_w	number(10);
cd_banco_lote_w			number(3);
vl_desconto_w			number(15,2);
vl_juros_w					number(15,2);
vl_multa_w					number(15,2);
ie_alt_tit_bloq_barras_w		varchar2(1);
ie_vincular_tit_bloq_barras_w	varchar2(1);
dt_vencimento_barras_w			date;
ie_sem_venc_w					varchar2(1);
ie_importar_vencidos_w			varchar2(1) := 'S';
qt_barras_lote_w				number(10);

cursor c01 is
/* Registro de transacao */
select	substr(a.ds_conteudo,144,3) cd_banco,
	substr(a.ds_conteudo,2,2) ie_tipo_inscricao,
	substr(a.ds_conteudo,63,14) nr_inscricao,
	substr(a.ds_conteudo,80,10) nr_nosso_numero,
	somente_numero(substr(a.ds_conteudo,117,15)) nr_pagamento,
	substr(a.ds_conteudo,192,30) nm_pessoa_externo,
	to_date(substr(a.ds_conteudo,91,8),'ddmmyyyy') dt_emissao,
	substr(a.ds_conteudo,135,3) cd_lancamento,
	substr(a.ds_conteudo,301,44) cd_barras,
	substr(a.ds_conteudo,222,30) ds_sacador_avalista,
	substr(a.ds_conteudo,282,6) ds_limite_desconto,
	somente_numero(substr(a.ds_conteudo,288,13)) / 100 vl_desconto,
	somente_numero(substr(a.ds_conteudo,166,13)) / 100 vl_juros,
	somente_numero(substr(a.ds_conteudo,179,13)) / 100 vl_multa
from	w_barras a
where	substr(a.ds_conteudo,1,1)	= '1'     
order by	a.nr_sequencia;

begin

begin


if	(nvl(nr_seq_lote_p,0)	= 0) then

	select	max(a.cd_estabelecimento),
		max(a.cd_banco)
	into	cd_estabelecimento_w,
		cd_banco_lote_w
	from	banco_escritural a
	where	a.nr_sequencia		= nr_seq_banco_escrit_p;

else

	select	max(a.cd_estabelecimento),
		max(b.cd_banco)
	into	cd_estabelecimento_w,
		cd_banco_lote_w
	from	banco_estabelecimento b,
		banco_escrit_lote a
	where	a.nr_seq_conta_banco	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_lote_p;

end if;

obter_param_usuario(857, 32, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_forma_importacao_w);
obter_param_usuario(857, 60, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_alt_tit_bloq_barras_w);
obter_param_usuario(857, 66, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_vincular_tit_bloq_barras_w);
obter_param_usuario(857, 67, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_sem_venc_w);

if	(ie_forma_importacao_w = '2') then
	nr_seq_banco_escrit_w	:= null;
else
	nr_seq_banco_escrit_w	:= nr_seq_banco_escrit_p;
end if;

open	c01;
loop
fetch	c01 into
	cd_banco_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nr_nosso_numero_w,
	nr_pagamento_w,
	nm_pessoa_externo_w,
	dt_emissao_w,
	cd_lancamento_w,
	cd_barras_w,
	ds_sacador_avalista_w,
	ds_limite_desconto_w,
	vl_desconto_w,
	vl_juros_w,
	vl_multa_w;
exit	when c01%notfound;

	if	(ie_tipo_inscricao_w = '02') then	/* CPF */

		select	max(a.cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	pessoa_fisica a
		where	somente_numero(a.nr_cpf)	= somente_numero(nr_inscricao_w);

	elsif	(ie_tipo_inscricao_w = '01') then	/* CNPJ */

		select	max(a.cd_cgc)
		into	cd_cgc_w
		from	pessoa_juridica a
		where	somente_numero(a.cd_cgc)	= somente_numero(nr_inscricao_w);

	end if;

	if ( nvl(ie_sem_venc_w,'S') = 'N') and (cd_barras_w is not null) then
	
		select	to_date(obter_dados_cod_barras(cd_barras_w,'DT'),'dd/mm/yyyy')
		into	dt_vencimento_barras_w
		from	dual;
			
		if ( to_date(dt_vencimento_barras_w,'dd/mm/RR') < to_date(sysdate,'dd/mm/RR') ) or (dt_vencimento_barras_w is null) then
			ie_importar_vencidos_w := 'N';
		else
			ie_importar_vencidos_w := 'S';
		end if;
	else
		ie_importar_vencidos_w := 'S';
	end if;

	if	(cd_barras_w is not null) and (nvl(ie_importar_vencidos_w,'S') = 'S') then
	
		select 	count(1) 
		into	qt_barras_lote_w
		from	banco_escrit_barras
		where	cd_barras = cd_barras_w
		and		decode(ie_forma_importacao_w,'2',0,nr_seq_banco_escrit) = decode(ie_forma_importacao_w,'2',0,nr_seq_banco_escrit_w);

		if (qt_barras_lote_w = 0) then	

			if	(ds_limite_desconto_w <> '000000') then
				dt_limite_desconto_w	:= to_date(ds_limite_desconto_w,'ddmmyy');
			else
				dt_limite_desconto_w	:= null;
			end if;

			select	banco_escrit_barras_seq.nextval
			into	nr_seq_escrit_barras_w
			from	dual;

			insert	into banco_escrit_barras
				(cd_barras,
				dt_atualizacao,
				nm_usuario,
				nr_seq_banco_escrit,
				nr_sequencia,
				cd_cgc,
				cd_pessoa_fisica,
				nr_seq_lote,
				cd_pessoa_externo,
				nm_pessoa_externo,
				nr_pagamento,
				dt_emissao,
				cd_lancamento,
				dt_limite_desconto,
				ds_sacador_avalista,
				nr_nosso_numero,
				vl_desconto,
				vl_acrescimo)
			values	(cd_barras_w,
				sysdate,
				nm_usuario_p,
				nr_seq_banco_escrit_w,
				nr_seq_escrit_barras_w,
				cd_cgc_w,
				cd_pessoa_fisica_w,
				nr_seq_lote_p,
				nr_inscricao_w,
				nm_pessoa_externo_w,
				nr_pagamento_w,
				dt_emissao_w,
				cd_lancamento_w,
				dt_limite_desconto_w,
				ds_sacador_avalista_w,
				nr_nosso_numero_w,
				vl_desconto_w,
				vl_juros_w + vl_multa_w);

			obter_titulo_regra_barras(cd_banco_lote_w,nr_documento_w,cd_cgc_w,cd_barras_w,nr_titulo_w,'N',nr_seq_escrit_barras_w);

			update	banco_escrit_barras
			set	nr_titulo	= nr_titulo_w
			where	nr_sequencia	= nr_seq_escrit_barras_w;

			if	(nvl(nr_titulo_w,0)	<> 0) and (cd_barras_w is not null) then
			begin
				if (nvl(ie_vincular_tit_bloq_barras_w, 'N') = 'S') then 
					update	titulo_pagar
					set	nr_bloqueto	= cd_barras_w,
						nm_usuario	= nm_usuario_p,
						ie_bloqueto     = 'S'   
					where	nr_titulo	= nr_titulo_w;			
				end if;			
				
				if (nvl(ie_alt_tit_bloq_barras_w, 'N') = 'S') then   			
					update	titulo_pagar
					set	ie_tipo_titulo	= '1', 
						nm_usuario	= nm_usuario_p
					where	nr_titulo	= nr_titulo_w;
				end if;

				definir_banco_tit_escritural('CP',nr_titulo_w,nr_seq_banco_escrit_w,nm_usuario_p,'N');
			end;
			end if;

		end if;
	end if;

end	loop;
close	c01;

delete	from w_barras;

commit;

exception
when others then

	delete	from w_barras;
	commit;

end;

end GERAR_BARRAS_SAFRA_400;
/
