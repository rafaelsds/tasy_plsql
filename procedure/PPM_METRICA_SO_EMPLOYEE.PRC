create or replace
procedure PPM_METRICA_SO_EMPLOYEE(
			nr_seq_metrica_p		number,
			nr_seq_objetivo_metrica_p	number,
			cd_pessoa_fisica_p		varchar2,
			nm_usuario_p			varchar2,
			dt_referencia_p			date) is
			
dt_ref_inicio_w 	date;
dt_ref_fim_w 		date;
resultado_w			number;
qt_defeito_w		number;
qt_os_recebida_w	number;

begin

dt_ref_inicio_w := pkg_date_utils.start_of(dt_referencia_p,'MONTH');
dt_ref_fim_w 	:= pkg_date_utils.end_of(last_day(dt_referencia_p),'DAY');
	
select	count(1)
into	qt_os_recebida_w
from	MAN_ORDEM_SERVICO c
where	c.dt_fim_Real between dt_ref_inicio_w and dt_ref_fim_w
and	c.ie_plataforma		= 'H'
/*and	g.ie_area_gerencia = 'PDES'*/
and	(exists	(select	1
		from	usuario y,
			man_ordem_serv_ativ x
		where	x.nm_usuario_exec = y.nm_usuario
		and	x.nr_seq_ordem_serv = c.nr_sequencia
		and	y.cd_pessoa_fisica = cd_pessoa_fisica_p) or cd_pessoa_fisica_p is null);

PPM_GRAVAR_RESULTADO(nr_seq_objetivo_metrica_p,dt_referencia_p,qt_os_recebida_w, qt_os_recebida_w, qt_os_recebida_w, nm_usuario_p);

end PPM_METRICA_SO_EMPLOYEE;
/