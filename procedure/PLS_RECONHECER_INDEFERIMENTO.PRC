create or replace
procedure pls_reconhecer_indeferimento
			(	nr_seq_defesa_p		number,
				nm_usuario_p		Varchar2) is 

nr_seq_impugnacao_w		number(10);
nr_seq_conta_w			number(10);
ie_status_conta_w		varchar2(3);
ie_instancia_conta_w		varchar2(3);
vl_pendente_w			number(15,2)	:= 0;
ie_tipo_impugnacao_w		pls_processo_conta.ie_tipo_impugnacao%type;
vl_procedimento_w		pls_processo_procedimento.vl_procedimento%type;

begin

select	a.nr_seq_impugnacao,
	b.nr_seq_conta,
	c.ie_status_conta,
	c.ie_instancia_conta,
	nvl(c.ie_tipo_impugnacao,'C')
into	nr_seq_impugnacao_w,
	nr_seq_conta_w,
	ie_status_conta_w,
	ie_instancia_conta_w,
	ie_tipo_impugnacao_w
from	pls_impugnacao_defesa  a,
	pls_impugnacao b,
	pls_processo_conta c
where	a.nr_seq_impugnacao	= b.nr_sequencia
and	b.nr_seq_conta		= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_defesa_p;

update	pls_impugnacao
set	ie_status_impugnacao	= 'F'
where	nr_sequencia	= nr_seq_impugnacao_w;

if	(ie_tipo_impugnacao_w = 'P') then

	select	nvl(sum(vl_procedimento),0)
	into	vl_procedimento_w
	from	pls_processo_procedimento
	where	nr_seq_conta	= nr_seq_conta_w;
	
	/*
	update	pls_processo_conta
	set	ie_status_conta		= 'I',
		ie_status_pagamento	= 'P',
		vl_ressarcir		= vl_procedimento_w,
		vl_pendente		= 0
	where	nr_sequencia		= nr_seq_conta_w;
	*/
else
	select	nvl(vl_pendente,0)
	into	vl_pendente_w
	from	pls_processo_conta
	where	nr_sequencia	= nr_seq_conta_w;

	/*
	update	pls_processo_conta
	set	ie_status_conta		= 'I',
		ie_status_pagamento	= 'P',
		vl_ressarcir		= vl_pendente_w,
		vl_pendente		= 0
	where	nr_sequencia		= nr_seq_conta_w;
	*/

end if;

insert into pls_processo_alteracao(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_conta,
					ie_status_impugnacao,
					ie_status_conta,
					ie_instancia_conta,
					ie_status_pagamento,
					vl_deferido,
					vl_pendente,
					vl_ressarcir,
					ds_acao)
				values(	pls_processo_alteracao_seq.NextVal,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_conta_w,
					'F',
					ie_status_conta_w,
					ie_instancia_conta_w,
					'P',
					0,
					0,
					vl_pendente_w,
					'Reconhecer indeferimento');

commit;

end pls_reconhecer_indeferimento;
/
