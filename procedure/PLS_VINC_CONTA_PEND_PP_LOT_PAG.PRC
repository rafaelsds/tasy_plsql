create or replace
procedure pls_vinc_conta_pend_pp_lot_pag(	nr_seq_conta_p		pls_conta.nr_sequencia%type,
						nr_seq_pp_lote_p	pls_pp_lote.nr_sequencia%type,
						nm_usuario_p		usuario.nm_usuario%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
						
nr_seq_lote_w			pls_pp_lote.nr_sequencia%type;
dt_fechamento_w			pls_pp_lote.dt_fechamento%type;
qt_registros_w			pls_integer;
nr_seq_pp_prestador_w		pls_pp_prestador.nr_sequencia%type;
ie_origem_venc_titulo_w		pls_pp_prestador.ie_origem_venc_titulo%type;
nr_seq_regra_venc_titulo_w	pls_pp_prestador.nr_seq_regra_venc_titulo%type;
ie_origem_comp_pag_w		pls_pp_prestador.ie_origem_comp_pag%type;	
dt_comp_pag_w			pls_pp_prestador.dt_comp_pag%type;
dt_venc_titulo_w		pls_pp_prestador.dt_venc_titulo%type;
ie_acao_pgto_negativo_w		pls_pp_prestador.ie_acao_pgto_negativo%type;
qt_pag_negativo_max_w		pls_pp_prestador.qt_pag_negativo_max%type;
vl_minimo_tit_liq_w		pls_pp_prestador.vl_minimo_tit_liq%type;
nr_seq_pp_prest_evento_valor_w	pls_pp_prest_evento_valor.nr_sequencia%type;

cursor C01 (	nr_seq_pp_lote_pc	pls_pp_lote.nr_sequencia%type,
		nr_seq_conta_pc		pls_conta.nr_sequencia%type) is
	select	nr_sequencia nr_seq_resumo,
		nr_seq_prestador_pgto,
		nr_seq_pp_evento,
		nr_seq_conta,
		nvl(vl_liberado,0) vl_liberado,
		nvl(vl_glosa,0) vl_glosa,
		nvl(vl_liberado,0) vl_liquido,
		dt_item,
		ie_tipo_contratacao
	from	pls_conta_medica_resumo
	where	nr_seq_conta = nr_seq_conta_pc
	and	nr_seq_pp_lote = nr_seq_pp_lote_pc
	and	nr_seq_prestador_pgto is not null
	and	nr_seq_pp_evento is not null
	and	ie_situacao = 'A';
		
begin

if	(nr_seq_conta_p is not null) then
	select	max(nr_sequencia),
		max(dt_fechamento)
	into	nr_seq_lote_w,
		dt_fechamento_w
	from	pls_pp_lote
	where	nr_sequencia = nr_seq_pp_lote_p;
	
	if	(nr_seq_lote_w is null) then	
		wheb_mensagem_pck.exibir_mensagem_abort(208549,'NR_SEQ_LOTE_PAG_P=' || nr_seq_pp_lote_p);
	end if;
	
	if	(dt_fechamento_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(212300,'NR_SEQ_LOTE_PAG_P=' || nr_seq_pp_lote_p);
	end if;
	
	select	count(1)
	into	qt_registros_w
	from	pls_conta_medica_resumo
	where	nr_seq_conta = nr_seq_conta_p
	and	nr_seq_lote_pgto is null
	and	nr_seq_pp_lote is null
	and	nr_seq_pp_evento is not null
	and	nr_seq_prestador_pgto is not null
	and	ie_situacao = 'A'
	and	rownum = 1;
	
	if	(qt_registros_w > 0) then
		update	pls_conta_medica_resumo
		set	nr_seq_pp_lote = nr_seq_pp_lote_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_seq_conta = nr_seq_conta_p
		and	nr_seq_lote_pgto is null
		and	nr_seq_pp_lote is null
		and	nr_seq_pp_evento is not null
		and	nr_seq_prestador_pgto is not null
		and	ie_situacao = 'A';
		
		for r_c01_w in c01 (nr_seq_pp_lote_p, nr_seq_conta_p) loop
			select	max(nr_sequencia)
			into	nr_seq_pp_prestador_w
			from	pls_pp_prestador
			where	nr_seq_lote = nr_seq_pp_lote_p
			and	nr_seq_prestador = r_c01_w.nr_seq_prestador_pgto
			and	ie_cancelado = 'N';
			
			if	(nr_seq_pp_prestador_w is null) then
				
				pls_pp_lote_pagamento_pck.carrega_parametros(nr_seq_pp_lote_p, cd_estabelecimento_p);
				
				dt_venc_titulo_w	:= pls_pp_lote_pagamento_pck.obter_data_vencimento_prest(r_c01_w.nr_seq_prestador_pgto, cd_estabelecimento_p, ie_origem_venc_titulo_w, nr_seq_regra_venc_titulo_w, dt_comp_pag_w, ie_origem_comp_pag_w);
				
				pls_pp_lote_pagamento_pck.obter_dados_forma_pgto_prest(r_c01_w.nr_seq_prestador_pgto, ie_acao_pgto_negativo_w, qt_pag_negativo_max_w, vl_minimo_tit_liq_w);
				
				insert into pls_pp_prestador (	nr_sequencia,				nr_seq_lote,				nr_seq_prestador,
								nm_usuario,				dt_atualizacao,				nm_usuario_nrec,
								dt_atualizacao_nrec,			ie_cancelado,				ie_exibe_portal,
								vl_apropriado,				vl_desconto,				vl_glosa,
								vl_liquido,				vl_provento,				vl_titulo_pagar,
								vl_titulo_receber,			vl_tributo,				ie_origem_venc_titulo,
								nr_seq_regra_venc_titulo,		ie_origem_comp_pag,			dt_comp_pag,
								dt_venc_titulo,				ie_acao_pgto_negativo,			qt_pag_negativo_max,
								vl_minimo_tit_liq)		
						values (	pls_pp_prestador_seq.nextval,		nr_seq_pp_lote_p,			r_c01_w.nr_seq_prestador_pgto,
								nm_usuario_p,				sysdate,				nm_usuario_p,
								sysdate,				'N',					'N',
								0,					0,					0,
								0,					0,					0,
								0,					0,					ie_origem_venc_titulo_w,
								nr_seq_regra_venc_titulo_w,		ie_origem_comp_pag_w,			dt_comp_pag_w,
								dt_venc_titulo_w,			ie_acao_pgto_negativo_w,		qt_pag_negativo_max_w,
								vl_minimo_tit_liq_w) returning nr_sequencia into nr_seq_pp_prestador_w;
			end if;
			
			if	(nr_seq_pp_prestador_w is not null) then
				select	max(nr_sequencia)
				into	nr_seq_pp_prest_evento_valor_w
				from	pls_pp_prest_evento_valor
				where	nr_seq_lote = nr_seq_pp_lote_p
				and	nr_seq_prestador = r_c01_w.nr_seq_prestador_pgto
				and	nr_seq_evento = r_c01_w.nr_seq_pp_evento
				and	ie_cancelado = 'N';
				
				if	(nr_seq_pp_prest_evento_valor_w is null) then
					insert into pls_pp_prest_evento_valor (	nr_sequencia,				nr_seq_lote,				nr_seq_prestador,
										nr_seq_evento,				nm_usuario,				dt_atualizacao,
										nm_usuario_nrec,			dt_atualizacao_nrec,			ie_cancelado,
										vl_glosa,				vl_item,				vl_liquido,
										vl_tributo,				vl_acao_negativo)
								values (	pls_pp_prest_evento_valor_seq.nextval,	nr_seq_pp_lote_p,			r_c01_w.nr_seq_prestador_pgto,
										r_c01_w.nr_seq_pp_evento,		nm_usuario_p,				sysdate,
										nm_usuario_p,				sysdate,				'N',
										0,					0,					0,
										0,					0) returning nr_sequencia into nr_seq_pp_prest_evento_valor_w;
										
				end if;
				
				select	count(1)
				into	qt_registros_w
				from	pls_pp_prest_event_prest
				where	nr_seq_pp_prest	= nr_seq_pp_prestador_w
				and	nr_seq_pp_prest_even_val = nr_seq_pp_prest_evento_valor_w;
				
				if	(qt_registros_w = 0) then
					insert into pls_pp_prest_event_prest(	nr_sequencia,
										nr_seq_pp_prest,
										nr_seq_pp_prest_even_val)
								values (	pls_pp_prest_event_prest_seq.nextval,
										nr_seq_pp_prestador_w,
										nr_seq_pp_prest_evento_valor_w);
				end if;
				
				if	(nr_seq_pp_prest_evento_valor_w is not null) then
					insert into pls_pp_item_lote (	nr_sequencia,				nr_seq_lote,				nr_seq_prestador,
									nr_seq_evento,				nm_usuario,				dt_atualizacao,
									nm_usuario_nrec,			dt_atualizacao_nrec,			ie_cancelado,
									ie_tipo_item,				nr_seq_prestador_origem,		vl_glosa,
									vl_item,				vl_liquido,				nr_seq_conta,
									nr_seq_resumo,				dt_item,				ie_tipo_contratacao,
									ie_acao_negativo, 			vl_desconto_tributo,			vl_acao_negativo)
							values (	pls_pp_item_lote_seq.nextval,		nr_seq_pp_lote_p,			r_c01_w.nr_seq_prestador_pgto,
									r_c01_w.nr_seq_pp_evento,		nm_usuario_p,				sysdate,
									nm_usuario_p,				sysdate,				'N',
									'1',					r_c01_w.nr_seq_prestador_pgto,		r_c01_w.vl_glosa,
									r_c01_w.vl_liberado,			r_c01_w.vl_liquido,			r_c01_w.nr_seq_conta,
									r_c01_w.nr_seq_resumo,			r_c01_w.dt_item,			r_c01_w.ie_tipo_contratacao,
									'N',					0,					0);
									
					pls_atualizar_valor_lote_pp(nr_seq_pp_prestador_w, nm_usuario_p);
				end if;
			end if;
		end loop;
	end if;
	
	commit;
end if;

end pls_vinc_conta_pend_pp_lot_pag;
/