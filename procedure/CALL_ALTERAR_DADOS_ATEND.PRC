CREATE OR REPLACE PROCEDURE CALL_ALTERAR_DADOS_ATEND(nr_seq_atendimento_p	NUMBER,
							ie_origem_atendimento_p	VARCHAR2,
							ie_origem_atendimento_novo_p	VARCHAR2,
							nm_usuario_p VARCHAR2) IS

DS_HISTORICO_W	 		VARCHAR2(4000);
DS_ORIGEM_NOVO_W		VARCHAR2(255);
DS_ORIGEM_ANTERIOR_W		VARCHAR2(255);
NR_HIST_GERADO_SISTEMA_W	NUMBER(10,2); 
							
BEGIN

	if (nr_seq_atendimento_p is not null) then

		SELECT SUBSTR(obter_desc_expressao(cd_exp_valor_dominio, ds_valor_dominio), 1, 254) 
   		  INTO DS_ORIGEM_NOVO_W 
		  FROM valor_dominio
		 WHERE cd_dominio = 3180
		   AND vl_dominio = ie_origem_atendimento_novo_p;
		   
		SELECT	MAX(nr_sequencia)
		INTO	NR_HIST_GERADO_SISTEMA_W
		FROM	PLS_TIPO_HISTORICO_ATEND
		WHERE	ie_gerado_sistema	= 'S'
		AND	ie_situacao		= 'A';
		   
		SELECT SUBSTR(obter_desc_expressao(cd_exp_valor_dominio, ds_valor_dominio), 1, 254) 
   		  INTO DS_ORIGEM_ANTERIOR_W 
		  FROM valor_dominio
		 WHERE cd_dominio = 3180
		   AND vl_dominio = ie_origem_atendimento_p;
	
	
		DS_HISTORICO_W := substr(wheb_mensagem_pck.get_texto(383077)||chr(13)||chr(10)||
					 wheb_mensagem_pck.get_texto(383084)||chr(13)||chr(10)||
						chr(9)||wheb_mensagem_pck.get_texto(383093)||DS_ORIGEM_ANTERIOR_W||chr(13)||chr(10)||
						chr(9)||wheb_mensagem_pck.get_texto(383094)||DS_ORIGEM_NOVO_W,1,4000);		


		UPDATE PLS_ATENDIMENTO
		SET    IE_ORIGEM_ATENDIMENTO = ie_origem_atendimento_novo_p,
		       DT_ATUALIZACAO = SYSDATE,
		       NM_USUARIO = nm_usuario_p
		WHERE  NR_SEQUENCIA = nr_seq_atendimento_p;

		INSERT INTO PLS_ATENDIMENTO_HISTORICO (
			NR_SEQUENCIA
			,NR_SEQ_ATENDIMENTO
			,DS_HISTORICO_LONG
			,IE_GERADO_SISTEMA
			,DT_ATUALIZACAO
			,NM_USUARIO
			,DT_ATUALIZACAO_NREC
			,NM_USUARIO_NREC
			,NR_SEQ_TIPO_HISTORICO
			,DT_HISTORICO)
		VALUES (PLS_ATENDIMENTO_HISTORICO_SEQ.NEXTVAL
			,NR_SEQ_ATENDIMENTO_P
			,DS_HISTORICO_W
			,'S'
			,SYSDATE
			,nm_usuario_p
			,SYSDATE
			,nm_usuario_p
			,NR_HIST_GERADO_SISTEMA_W
			,SYSDATE);
		
		COMMIT;
	END IF;

END CALL_ALTERAR_DADOS_ATEND;
/
