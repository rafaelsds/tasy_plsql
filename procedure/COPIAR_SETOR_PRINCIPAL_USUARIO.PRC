create or replace
procedure 	copiar_setor_principal_usuario	(nm_usuario_p		varchar2,
						 nm_usuario_destino_p	varchar2,
						 nm_usuario_origem_p	varchar2) is 

cd_setor_atendimento_w	number(5);

begin
select	cd_setor_atendimento
into	cd_setor_atendimento_w
from	usuario
where	nm_usuario = nm_usuario_origem_p;

update	usuario
set	cd_setor_atendimento = cd_setor_atendimento_w,
	nm_usuario_atual = nm_usuario_p
where	nm_usuario = nm_usuario_destino_p;

commit;

end copiar_setor_principal_usuario;
/