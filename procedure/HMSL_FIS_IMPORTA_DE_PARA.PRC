create or replace
procedure HMSL_FIS_IMPORTA_DE_PARA is

cursor c01 is
	select 	NR_SEQ_REGRA,
		NM_TABELA,
		NM_ATRIBUTO,
		CD_INTERNO,
		CD_EXTERNO,
		IE_SISTEMA_EXTERNO,
		IE_ENVIO_RECEB
	from   	W_CONVERSAO_MEIO_EXTERNO
	order by NR_SEQ_REGRA;
vet01	c01%rowtype;

begin

open c01;
loop
fetch c01 into	
	vet01;
exit when c01%notfound;
	begin
	
	insert into CONVERSAO_MEIO_EXTERNO(
		NR_SEQUENCIA,
		IE_SISTEMA_EXTERNO,
		NR_SEQ_REGRA,
		IE_ENVIO_RECEB,
		NM_APRESENTACAO_EXT,
		CD_DOMINIO,
		NM_ATRIBUTO,
		CD_EXTERNO,
		NM_USUARIO,
		CD_CGC,
		NM_TABELA,
		DT_ATUALIZACAO,
		CD_INTERNO)
		values(
		CONVERSAO_MEIO_EXTERNO_SEQ.nextval,
		vet01.IE_SISTEMA_EXTERNO,
		vet01.NR_SEQ_REGRA,
		vet01.IE_ENVIO_RECEB,
		null,
		null,
		vet01.NM_ATRIBUTO,
		vet01.CD_EXTERNO,
		'Tasy',
		null,
		vet01.NM_TABELA,
		sysdate,
		vet01.CD_INTERNO);	
	end;
end loop;
close c01;	

commit;

end HMSL_FIS_IMPORTA_DE_PARA;
/
