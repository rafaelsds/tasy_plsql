create or replace procedure autor_mat_esp_fim_cotacao(nr_sequencia_autor_p	number,
				nm_usuario_p	varchar2)
				is

ie_estagio_autor_w varchar2(1);
ie_estagio_novo_w varchar(255);
ie_estagios_param_w varchar2(255);
pos_w number(4);

begin

obter_param_usuario(3006, 17, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_estagios_param_w);

update	autorizacao_cirurgia
set	dt_fim_cotacao = sysdate,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_sequencia	= nr_sequencia_autor_p;

select ie_estagio_autor
into ie_estagio_autor_w 
from autorizacao_cirurgia
where	nr_sequencia	= nr_sequencia_autor_p;

if  (instr(ie_estagios_param_w,',') > 1) then  
  ie_estagio_novo_w := null;
  pos_w := 1;
  loop
    ie_estagio_novo_w := substr(ie_estagios_param_w, pos_w, instr(ie_estagios_param_w, ',', pos_w) - pos_w);
    pos_w := instr(ie_estagios_param_w, ',', pos_w) + 1;
    if(ie_estagio_novo_w = ie_estagio_autor_w and pos_w > 1) then
      ie_estagio_novo_w := substr(ie_estagios_param_w, pos_w, 1);
      if(instr(ie_estagios_param_w, ',', pos_w) > 0) then
        ie_estagio_novo_w := substr(ie_estagios_param_w, pos_w, instr(ie_estagios_param_w, ',', pos_w) - pos_w);         
      end if;
      exit;
    else
      ie_estagio_novo_w := null;
    end if;    
    exit when pos_w <= 1;
  end loop;
end if;

if(ie_estagio_novo_w is not null) then
  atualizar_estagio_autor_cir(ie_estagio_novo_w, nr_sequencia_autor_p, nm_usuario_p);
end if;


commit;

end autor_mat_esp_fim_cotacao;
/