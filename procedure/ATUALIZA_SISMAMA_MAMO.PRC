create or replace 
procedure atualiza_sismama_mamo(
				ds_arquivo_p			varchar2,		
				nr_seq_protocolo_p		number,
				nm_usuario_p			varchar2) is
begin
	if	(ds_arquivo_p is not null) and
		(nm_usuario_p is not null) and
		(nr_seq_protocolo_p is not null) then
		update	protocolo_convenio 
		set	dt_envio = sysdate , 
			nm_usuario_envio = nm_usuario_p, 
			ds_arquivo_envio = ds_arquivo_p 
		where	nr_seq_protocolo = nr_seq_protocolo_p;
		commit;
	end if;
end atualiza_sismama_mamo;
/