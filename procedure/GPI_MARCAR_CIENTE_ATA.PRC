create or replace
procedure gpi_marcar_ciente_ata(nr_sequencia_p	number,
			nm_usuario_p		Varchar2) is 

begin

update	proj_ata_participante
set	dt_ciente_ata	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

end gpi_marcar_ciente_ata;
/