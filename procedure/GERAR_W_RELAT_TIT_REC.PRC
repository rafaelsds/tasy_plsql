create or replace
procedure gerar_w_relat_tit_rec( dt_inicial_p				date,
				 dt_final_p				date,
				 ds_lista_procedimento_p		Varchar2,
				 ds_situacao_titulo_p			Varchar2,
				 ie_dados_p				varchar2,
				 ie_data_p				number,
				 cd_pessoa_fisica_p			varchar2,
				 cd_cgc_p				varchar2,
				 ie_tipo_titulo_p			varchar2,
				 cd_estabelecimento_p			varchar2,
				 cd_estrutura_p				varchar2,
				 nm_usuario_p				varchar2,
				 dt_pc_p				date) is 
/*
dt_pc_p = Data Posi��o Cont�bil
*/
CD_CGC_W 		varchar2(14);         
CD_GRUPO_PROC_W    	number(15);
CD_PESSOA_FISICA_W	varchar2(10);
CD_PROCEDIMENTO_W 	number(15);
DT_EMISSAO_W      	date;
DT_INCLUSAO_W     	date;
DT_VENCIMENTO_W   	date;
NM_USUARIO_W      	varchar2(15);
NR_SEQ_NOTA_W     	number(15);
NR_TITULO_W       	number(15);
VL_PAGO_W         	number(15,2);
VL_SALDO_W        	number(15,2);
VL_TITULO_W       	number(15,2);
DT_LIQUIDACAO_W		date;
IE_ORIGEM_PROCED_W	number(10);
DT_PAGAMENTO_PREVISTO_W date;

Cursor C01 is
select  distinct a.nr_titulo,
	a.cd_pessoa_fisica,
	a.cd_cgc,
	a.nr_seq_nf_saida,
	a.dt_emissao,
	a.dt_vencimento,
	a.dt_liquidacao,
	a.vl_titulo,
        nvl((select sum(b.vl_recebido)
	 from   titulo_receber_liq b
	 where  a.nr_titulo = b.nr_titulo),0) vl_pago,	
	a.vl_saldo_titulo,
	d.cd_grupo_proc,
	d.cd_procedimento,
	d.IE_ORIGEM_PROCED,
	a.dt_pagamento_previsto
from	titulo_receber a,
	nota_fiscal b,
	nota_fiscal_item c,
	procedimento d
where	a.nr_seq_nf_saida	= b.nr_sequencia(+)
and	b.nr_sequencia   	= c.nr_sequencia(+)
and	c.cd_procedimento 	= d.cd_procedimento(+)
and	( (cd_estrutura_p is null) 		or (obter_se_contido(d.cd_grupo_proc, cd_estrutura_p) = 'S')) 
and	( (ds_lista_procedimento_p is null) 	or (obter_se_contido(d.cd_procedimento, ds_lista_procedimento_p) = 'S')) 
and	( ((ie_data_p = 0) and (a.dt_vencimento	between dt_inicial_p and fim_dia(dt_final_p))) or
	  ((ie_data_p = 1) and (a.dt_emissao	between dt_inicial_p and fim_dia(dt_final_p))) or
	  ((ie_data_p = 2) and (a.dt_liquidacao	between dt_inicial_p and fim_dia(dt_final_p))) or
	  ((ie_data_p = 3) and ( (nvl(a.dt_liquidacao, sysdate+1000) >= dt_pc_p ) and   
				(a.dt_contabil <= dt_pc_p) and   
				( (a.dt_liquidacao is null) OR (a.dt_liquidacao > dt_pc_p))  )) or 
	  (ie_data_p = 4) and (a.dt_contabil	between dt_inicial_p and fim_dia(dt_final_p)) or
	  (ie_data_p = 5) or 
	  (ie_data_p = 6) and (a.dt_pagamento_previsto	between dt_inicial_p and fim_dia(dt_final_p)))
and	( (ds_situacao_titulo_p is null)	or (obter_se_contido(a.ie_situacao, ds_situacao_titulo_p) = 'S') )
and	( (cd_cgc_p is null) 			or (Obter_Se_Contido_char(a.cd_cgc, REPLACE(cd_cgc_p,' ','')) = 'S') )
and	( (cd_pessoa_fisica_p is null)		or (Obter_Se_Contido_char(a.cd_pessoa_fisica, REPLACE(cd_pessoa_fisica_p,' ','')) = 'S') )
and     ( (ie_tipo_titulo_p is null) 		or (obter_se_contido(a.ie_tipo_titulo, ie_tipo_titulo_p) = 'S') )     	  
and	( (cd_estabelecimento_p is null) 	or (obter_se_contido(a.cd_estabelecimento, cd_estabelecimento_p) = 'S'));	  
	  		
begin

if ( ie_data_p = 3 ) and ( dt_pc_p is null ) then
	--Quando a data de refer�ncia for Pos���o Cont�bil, � necess�rio informar a data de posi��o cont�bil nos filtros do relat�rio
	wheb_mensagem_pck.exibir_mensagem_abort(237837);	
	
end if;

delete	from W_REL_TITULO_RECEBER
where 	nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into	
	NR_TITULO_W,
	CD_PESSOA_FISICA_W,
	CD_CGC_W, 
	NR_SEQ_NOTA_W, 
	DT_EMISSAO_W, 
	DT_VENCIMENTO_W,
	DT_LIQUIDACAO_W,
	VL_TITULO_W,	
	VL_PAGO_W, 
	VL_SALDO_W, 		        
	CD_GRUPO_PROC_W, 	
	CD_PROCEDIMENTO_W,
	IE_ORIGEM_PROCED_W,
	DT_PAGAMENTO_PREVISTO_W;
exit when C01%notfound;
	begin
	insert into W_REL_TITULO_RECEBER (	CD_CGC,          
				CD_GRUPO_PROC,   
				CD_PESSOA_FISICA,
				CD_PROCEDIMENTO, 
				DT_EMISSAO,      
				DT_INCLUSAO,     
				DT_VENCIMENTO, 
				DT_LIQUIDACAO,
				NM_USUARIO,      
				NR_SEQ_NOTA,     
				NR_TITULO,       
				VL_PAGO,         
				VL_SALDO,        
				VL_TITULO,
				IE_ORIGEM_PROCED,
				dt_atualizacao,
				DT_PAGAMENTO_PREVISTO)      
			values ( CD_CGC_W, 		        
				CD_GRUPO_PROC_W, 
				CD_PESSOA_FISICA_W,	
				CD_PROCEDIMENTO_W, 
				DT_EMISSAO_W,  
				sysdate, 
				DT_VENCIMENTO_W,
				DT_LIQUIDACAO_W,
				NM_USUARIO_P,     
				NR_SEQ_NOTA_W,     	
				NR_TITULO_W,       
				VL_PAGO_W,         
				VL_SALDO_W,        	
				VL_TITULO_W,
				IE_ORIGEM_PROCED_W,
				sysdate,
				DT_PAGAMENTO_PREVISTO_W);
				
	end;
end loop;
close C01;

commit;

end gerar_w_relat_tit_rec;
/
