create or replace
procedure tornar_urgente_solic_compra (
		nr_seq_motivo_urgente_p	number,
		nr_solic_compra_p	number,
		nm_usuario_p		Varchar2) is 

begin
if	(( nr_solic_compra_p <> null) and ( nr_seq_motivo_urgente_p <> null))then
	begin
	update	solic_compra 
	set	nr_seq_motivo_urgente = nr_seq_motivo_urgente_p,
		nm_usuario = nm_usuario_p
	where	nr_solic_compra = nr_solic_compra_p ;
	end;
end if;

commit;

end tornar_urgente_solic_compra ;
/