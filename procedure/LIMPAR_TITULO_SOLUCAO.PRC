create or replace
procedure Limpar_titulo_solucao(nr_prescricao_p		number,
				nr_seq_solucao_p	number) is 
				
ds_erro_w	varchar2(2000);

begin

update	prescr_solucao
set 	ds_solucao = null
where 	nr_prescricao = nr_prescricao_p
and	nr_seq_solucao = nr_seq_solucao_p;

commit;

Consistir_prescr_solucao(nr_prescricao_p, nr_seq_solucao_p, wheb_usuario_pck.get_cd_estabelecimento, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, ds_erro_w);

end Limpar_titulo_solucao;
/
