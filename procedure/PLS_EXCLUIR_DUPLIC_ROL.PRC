create or replace
procedure pls_excluir_duplic_rol
			(	nr_seq_rol_p		Number,
				nm_usuario_p		Varchar2) is 

qt_registros_w			number(10);

begin

select	count(*)
into	qt_registros_w
from	pls_rol
where	ie_tipo_rol	= 'M'
and	nr_sequencia	= nr_seq_rol_p;

if	(qt_registros_w > 0) then
	delete from pls_rol_mat_med
	where	nr_seq_rol_grupo in (	select	x.nr_sequencia
					from	PLS_ROL_GRUPO_MAT x,
						PLS_ROL_SUBGRUPO y,
						pls_rol_grupo z
					where	x.nr_seq_subgrupo = y.nr_sequencia
					and	y.nr_seq_grupo	= z.nr_sequencia
					and	z.nr_seq_rol	= nr_seq_rol_p);
	
	delete from pls_rol_grupo_mat
	where	nr_seq_subgrupo in (	select	x.nr_sequencia
					from	pls_rol_subgrupo x,
						pls_rol_grupo y
					where	x.nr_seq_grupo = y.nr_sequencia
					and	y.nr_seq_rol	= nr_seq_rol_p);
	
	delete	from pls_rol_segmentacao
	where	nr_seq_rol_grupo in (	select	x.nr_sequencia
					from	pls_rol_grupo_proc x,
						pls_rol_grupo y
					where	x.nr_seq_grupo	= y.nr_sequencia
					and	y.nr_seq_rol	= nr_seq_rol_p);

	delete	from pls_rol_procedimento
	where	nr_seq_rol_grupo in (	select	x.nr_sequencia
					from	pls_rol_grupo_proc x,
						pls_rol_grupo y
					where	x.nr_seq_grupo	= y.nr_sequencia
					and	y.nr_seq_rol	= nr_seq_rol_p);

	delete	from pls_rol_grupo_proc
	where	nr_seq_grupo in (select	x.nr_sequencia
				from	pls_rol_grupo_proc x,
					pls_rol_grupo y
				where	x.nr_seq_grupo	= y.nr_sequencia
				and	y.nr_seq_rol	= nr_seq_rol_p);

	delete	from pls_rol_subgrupo
	where	nr_seq_grupo in (select	x.nr_sequencia
				from	pls_rol_grupo_proc x,
					pls_rol_grupo y
				where	x.nr_seq_grupo	= y.nr_sequencia
				and	y.nr_seq_rol	= nr_seq_rol_p);

	delete	from pls_rol_grupo
	where	nr_seq_rol	= nr_seq_rol_p;

	delete	from pls_rol_capitulo
	where	nr_seq_rol	= nr_seq_rol_p;

	delete	from pls_rol
	where	nr_sequencia	= nr_seq_rol_p;
end if;

commit;

end pls_excluir_duplic_rol;
/
