create or replace procedure cpoe_obter_dados_antimicrob(	cd_material_p			in	Number,
										cd_perfil_p				in	Number,
										cd_estabelecimento_p	in	Number,
										nm_usuario_p			in	Varchar2,
										ie_objetivo_filtro_p	in	Varchar2,
										cd_intervalo_p			varchar2 default null,
										ie_justificativa_p 		out	Varchar2,
										ie_objetivo_p 			out	Varchar2,
										ie_dia_utilizacao_p 	out	Varchar2,
										ie_topografia_p			out	Varchar2,
										ie_amostra_p 			out	Varchar2,
										ie_microorganismo_p 	out	Varchar2,
										ie_origem_p 			out	Varchar2,
										ie_exige_indicacao_p	out	varchar2,
										qt_dias_prev_p			out	number,
										ie_obj_regra_p			out	varchar2,
										ie_lib_auto_p			out	varchar2,
										ie_possui_regra_p		out	varchar2,
										ie_regra_intervalo_p	out varchar2,
										qt_dias_lib_padrao_p	out Number) is

ie_justificativa_antimicrob_w	char(1);
ie_objetivo_antimicrob_w		char(1);
ie_dia_utilizacao_antimicrob_w	char(1);
ie_topografia_antimicrob_w		char(1);
ie_amostra_antimicrob_w			char(1);
ie_microorganismo_antimicrob_w	char(1);
ie_origem_antimicrob_w			char(1);
ie_ctrl_medico_antimicrob_w		char(1);
ie_indicacao_clinica_w			char(1);
ie_existe_w						char(1);
hr_hora_atual_w					char(8);
ie_dia_semana_w					number(1);
ie_feriado_w 					number(10);
qt_dias_prev_w					regra_antimicrobiano.qt_dias_prev%type;
ie_objetivo_filtro_w			regra_antimicrobiano.ie_objetivo_filtro%type;
ie_lib_auto_w					regra_antimicrobiano.ie_lib_auto%type;
cd_grupo_material_w				grupo_material.cd_grupo_material%type;
cd_subgrupo_w					subgrupo_material.cd_subgrupo_material%type;
nr_seq_familia_w				material_familia.nr_sequencia%type;
cd_classe_material_w			classe_material.cd_classe_material%type;
cd_material_estoque_w			material.cd_material%type;
nr_seq_ficha_tecnica_w			material.nr_seq_ficha_tecnica%type;
cd_intervalo_w					cpoe_material.cd_intervalo%type;
qt_dias_lib_padrao_w			parametro_medico.qt_dias_lib_padrao%type;

cursor c01 is
select	ie_justificativa,
		nvl(ie_objetivo,'N'),
		ie_dia_utilizacao,
		ie_topografia,
		ie_amostra,
		ie_microorganismo,
		ie_origem,
		nvl(ie_indicacao,'N'),
		qt_dias_prev,
		ie_objetivo_filtro,
		nvl(ie_lib_auto,'N'),
    decode(cd_intervalo, null, 'N', 'S')
from	regra_antimicrobiano
where	cd_regra = ie_ctrl_medico_antimicrob_w
and		cd_estabelecimento	= cd_estabelecimento_p
and		((cd_perfil is null) or (cd_perfil = cd_perfil_p))
and		((cd_intervalo is null) or (cd_intervalo = cd_intervalo_p))
and		nvl(cd_grupo_material,cd_grupo_material_w) = cd_grupo_material_w
and		nvl(cd_subgrupo_material,cd_subgrupo_w) = cd_subgrupo_w
and		nvl(nr_seq_familia,nr_seq_familia_w) = nr_seq_familia_w
and		nvl(cd_classe_material,cd_classe_material_w) = cd_classe_material_w
and		nvl(cd_material,cd_material_p) = cd_material_p
and		nvl(cd_material_estoque, cd_material_estoque_w) = cd_material_estoque_w
and		nvl(nr_seq_ficha_tecnica, nr_seq_ficha_tecnica_w) = nr_seq_ficha_tecnica_w
and		((dt_hora_inicio is null) or (to_char(dt_hora_inicio,'hh24:mi:ss') < hr_hora_atual_w))
and		((dt_hora_fim is null) or (to_char(dt_hora_fim,'hh24:mi:ss') > hr_hora_atual_w))
and		(((ie_dia_semana = 9) or (coalesce(ie_dia_semana,ie_dia_semana_w) = ie_dia_semana_w))
		or ((ie_feriado_w > 0) and (ie_dia_semana = 0)))
and		((ie_objetivo_filtro = ie_objetivo_filtro_p) or (ie_objetivo_filtro is null))
order by
		nvl(cd_material,0),
		nvl(cd_intervalo, 0),
		nvl(cd_material_estoque,0),
		nvl(nr_seq_ficha_tecnica,0),
		nvl(nr_seq_familia,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_perfil,0),
		nvl(ie_dia_semana,0),
		nvl(to_char(dt_hora_inicio,'hh24:mi:ss'),'00:00:00'),
		nvl(to_char(dt_hora_fim,'hh24:mi:ss'),'00:00:00'),
		nvl(ie_objetivo_filtro,'X') desc;

function valida_retorno(ie_param_p varchar2) return varchar2 is
begin
	if (ie_param_p = 'N') then
		return 'X';
	else
		return ie_param_p;
	end if;
end;

begin
if	(coalesce(cd_material_p,0) = 0) then
	return;
end if;

select	nvl(max(qt_dias_lib_padrao),0)
into	qt_dias_lib_padrao_w
from	parametro_medico
where   cd_estabelecimento = cd_estabelecimento_p;

select	nvl(max('S'),'N')
into	ie_existe_w
from	regra_antimicrobiano
where	rownum = 1;

if	(ie_existe_w = 'S') then

	ie_feriado_w 				:= nvl(obter_se_feriado(cd_estabelecimento_p, sysdate),0);   
	ie_dia_semana_w				:= pkg_date_utils.get_WeekDay(sysdate);
	hr_hora_atual_w				:= to_char(sysdate,'hh24:mi:ss');
	ie_ctrl_medico_antimicrob_w	:= substr(obter_ctrl_antimicrobiano_mat(cd_material_p),1,1);	
	
	select	nvl(max(cd_grupo_material),0),
			nvl(max(cd_subgrupo_material),0),
			nvl(max(cd_classe_material),0),
			nvl(max(nr_seq_familia),0),
			nvl(max(cd_material_estoque),0),
			nvl(max(nr_seq_ficha_tecnica),0)
	into	cd_grupo_material_w,
			cd_subgrupo_w,
			cd_classe_material_w,
			nr_seq_familia_w,
			cd_material_estoque_w,
			nr_seq_ficha_tecnica_w			
	from	estrutura_material_v
	where 	cd_material	= cd_material_p;

	ie_possui_regra_p := 'N';

	open c01;
	loop
	fetch c01 into	
			ie_justificativa_antimicrob_w,
			ie_objetivo_antimicrob_w,
			ie_dia_utilizacao_antimicrob_w,
			ie_topografia_antimicrob_w,
			ie_amostra_antimicrob_w,
			ie_microorganismo_antimicrob_w,
			ie_origem_antimicrob_w,
			ie_indicacao_clinica_w,
			qt_dias_prev_w,
			ie_objetivo_filtro_w,
			ie_lib_auto_w,
      cd_intervalo_w;
	exit when c01%notfound;
		begin

		ie_justificativa_p 		:= valida_retorno(ie_justificativa_antimicrob_w);
		ie_objetivo_p 			:= valida_retorno(ie_objetivo_antimicrob_w);	
		ie_dia_utilizacao_p 	:= valida_retorno(ie_dia_utilizacao_antimicrob_w);	
		ie_topografia_p			:= valida_retorno(ie_topografia_antimicrob_w);		
		ie_amostra_p 			:= valida_retorno(ie_amostra_antimicrob_w);			
		ie_microorganismo_p 	:= valida_retorno(ie_microorganismo_antimicrob_w);	
		ie_origem_p 			:= valida_retorno(ie_origem_antimicrob_w);			
		ie_exige_indicacao_p	:= valida_retorno(ie_indicacao_clinica_w);			
		qt_dias_prev_p			:= valida_retorno(qt_dias_prev_w);	
		ie_obj_regra_p		 	:= valida_retorno(ie_objetivo_filtro_w);
		ie_lib_auto_p			:= valida_retorno(ie_lib_auto_w);
		ie_possui_regra_p		:= 'S';
		ie_regra_intervalo_p	:= cd_intervalo_w;
		qt_dias_lib_padrao_p	:= qt_dias_lib_padrao_w;
		
		end;
	end loop;
	close c01;
end if;

commit;

end cpoe_obter_dados_antimicrob;
/
