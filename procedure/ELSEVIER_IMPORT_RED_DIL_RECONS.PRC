create or replace procedure elsevier_import_red_dil_recons( cd_protocolo_p		       in   protocolo_medicacao.cd_protocolo%type,
                                                            nr_seq_protocolo_p	       in   protocolo_medicacao.nr_sequencia%type,
                                                            ie_agrupador_p	       in   protocolo_medic_material.ie_agrupador%type,
                                                            nm_usuario_p		       in   intpd_fila_transmissao.nm_usuario%type,
                                                            cd_material_p              in   protocolo_medic_material.cd_material%type,
                                                            cd_unidade_medida_p        in   protocolo_medic_material.cd_unidade_medida%type, 
                                                            qt_dose_p                  in   protocolo_medic_material.qt_dose%type) is

ds_sql_w varchar2(4000 char);
nr_seq_visao_w number(10);
ds_fields_default varchar2(4000 char);
ds_values_fields_defaults varchar2(4000 char);
ds_field_w  varchar2(255 char);

c001		Integer;
retorno_w	Number(5);

nr_seq_material_w	protocolo_medic_material.nr_seq_material%type;
nr_agrupamento_w	protocolo_medic_material.nr_agrupamento%type;
nr_seq_iterno_w		protocolo_medic_material.nr_seq_interno%type;

--30119 html5  - atepac_pt - diluente ie_agrupador = 3
--30117 html5 - atepac_pt - rediluente ie_agrupador 7
--30032 html5 - atepac_pt - reconstituinte ie_agrupador 9

function add_string(vl VARCHAR2) return varchar2 is
ds_caracter_w varchar2(1 char);

begin 
    ds_caracter_w := chr(39);
    return ds_caracter_w || vl || ds_caracter_w;
end;

begin

    select	obter_seq_mat_prot_medic_mat(cd_protocolo_p, nr_seq_protocolo_p),
            obter_agrup_prot_medic_mat(cd_protocolo_p, nr_seq_protocolo_p, 0)
    into	nr_seq_material_w,
        nr_agrupamento_w
    from	dual;

    ds_fields_default := '';
    ds_values_fields_defaults := '';
    
    case 
        when ie_agrupador_p = 3 then
                nr_seq_visao_w := 30119;
        when ie_agrupador_p = 7 then
                nr_seq_visao_w := 30117;
        when ie_agrupador_p = 9 then
                nr_seq_visao_w := 30032;
    else
          nr_seq_visao_w := 0;
    end case;

    if(nr_seq_visao_w <> 0)then

        for record_vision in (
                select  nm_atributo,
                    vl_padrao,
                    substr(obter_tipo_atributo(nr_sequencia,nm_atributo),1,15) ie_tipo
                from    tabela_visao_atributo
                where vl_padrao is not null
                and   nr_sequencia = nr_seq_visao_w
        )
        loop
            if(lower(record_vision.ie_tipo) = 'varchar2') then
                ds_field_w := add_string(record_vision.vl_padrao);
            else
                ds_field_w := record_vision.vl_padrao;
            end if;

            ds_fields_default := ds_fields_default || record_vision.nm_atributo || ',';
            ds_values_fields_defaults := ds_values_fields_defaults || ds_field_w || ',';
        end loop;
        
       select	protocolo_medic_material_seq.nextval
       into	nr_seq_iterno_w
       from	dual;

       ds_sql_w := 'insert into protocolo_medic_material (' ||
                   ' cd_protocolo,
                    nr_sequencia,
                    nr_seq_diluicao,
                    nr_seq_interno,
                    nr_seq_material,
                    nr_agrupamento,
                    nm_usuario,
                    dt_atualizacao,
                    cd_material,
                    cd_unidade_medida,
                    qt_dose,
                    '  ||
                     substr(ds_fields_default, 0, length(ds_fields_default) - 1) ||
                    ')
                     values'                ||  
                    ' ('                    || 
                        cd_protocolo_p      || ',' ||
                        nr_seq_protocolo_p  || ',' ||
                        nr_seq_protocolo_p  || ',' ||
                        nr_seq_iterno_w     || ',' ||
                        nr_seq_material_w   || ',' ||
                        nr_agrupamento_w    || ',' ||
                        add_string(nm_usuario_p) || ',' ||
                        'sysdate'             || ',' ||
                        cd_material_p       || ',' ||
                        add_string(cd_unidade_medida_p) || ',' ||
                        qt_dose_p           || ',' ||
                        substr(ds_values_fields_defaults, 0, length(ds_values_fields_defaults) - 1)
                        ||  ')'; 

        begin  
            c001 := DBMS_SQL.OPEN_CURSOR;
            DBMS_SQL.PARSE(c001, ds_sql_w, dbms_sql.native);
            retorno_w := DBMS_SQL.execute(c001); 
            DBMS_SQL.CLOSE_CURSOR(c001);
        exception
        when others then
            DBMS_SQL.CLOSE_CURSOR(c001);
            Raise_application_error(-20011, 'ERROR in elsevier_import_red_dil_recons : ' || SQLERRM(sqlcode));
        end;

    end if;

end elsevier_import_red_dil_recons;
/
