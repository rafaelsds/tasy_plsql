create or replace
procedure pls_atual_proc_ptu_partic
			(	nr_seq_conta_proc_p	number,
				nr_seq_grupo_atual_p	number,
				nr_seq_analise_p	number,
				ie_comitar_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number	) is

ie_tipo_despesa_w	varchar2(1);
nr_seq_conta_w		number(10);
nr_seq_item_criado_w	number(10);
nr_seq_item_w		number(10);
cd_medico_w		number(10);
nr_seq_conselho_w	number(10);
nr_seq_grau_partic_w	number(10);
nr_seq_prestador_w	number(10);
nr_seq_prestador_pgto_w	number(10);
ie_tipo_item_w		varchar2(1);
nm_participante_w	varchar2(255);
ds_grau_participacao_w	varchar2(255);
nr_seq_apres_w		number(10);
nr_ordem_w		number(10);
qt_item_w		number(10);
dt_inicio_proc_w	date;
dt_procedimento_w	date;

begin

select	a.ie_tipo_despesa,
	a.nr_seq_conta,
	a.dt_procedimento,
	a.dt_inicio_proc
into	ie_tipo_despesa_w,
	nr_seq_conta_w,
	dt_procedimento_w,
	dt_inicio_proc_w
from	pls_conta_proc	a
where	a.nr_sequencia = nr_seq_conta_proc_p;

if	(ie_tipo_despesa_w = '1') then /* Atualizar o valor do procedimento */ 
	pls_atualiza_valor_proc(nr_seq_conta_proc_p, 'S', nm_usuario_p,'S',null,null);
elsif	(ie_tipo_despesa_w in ('2','3')) then /* Atualizar os valores das taxas e diarias */
	pls_atualiza_valor_servico(nr_seq_conta_proc_p, 'S', nm_usuario_p,'S'); 
elsif	(ie_tipo_despesa_w = '4') then /* Atualizar os valores dos pacotes */
	pls_atualiza_valor_pacote(nr_seq_conta_proc_p, 'C', nm_usuario_p, 'S', 'N');
end if;

pls_atualizar_proc_ref(nr_seq_conta_proc_p, cd_estabelecimento_p, nm_usuario_p);

pls_consistir_conta_proc(nr_seq_conta_proc_p, cd_estabelecimento_p,'N', nm_usuario_p);

/*Rotina para vincular regra de liberacao de valores para conta no procedimento, quando inserido manualmente pela analise. */
pls_obter_regra_valor_conta(nr_seq_conta_w, nr_seq_conta_proc_p,null,null,nm_usuario_p);

pls_atualizar_glosa_oc_analise(	nr_seq_conta_proc_p, null, nr_seq_analise_p, 
				cd_estabelecimento_p, nm_usuario_p);				
				
select	max(nr_sequencia)
into	nr_seq_item_w
from	w_pls_resumo_conta
where	nr_seq_item	= nr_seq_conta_proc_p
and	ie_tipo_item	in ('P', 'R');

/*Para os casos em que o procedimento ja existe so vai ser incluido os participante*/
if	(nvl(nr_seq_item_w,0) > 0) then
				
	/*Se ja existir o registro eh so atualizar para conter o partic.*/
	select	a.cd_medico,				
		a.nr_seq_conselho,		
		a.nr_seq_grau_partic,				
		a.nr_seq_prestador,
		a.nr_seq_prestador_pgto,		
		'R',				
		decode(a.nr_seq_prestador,null,nvl(Obter_nome_medico(a.cd_medico,'N'), a.nm_medico_executor_imp),pls_obter_dados_prestador(a.nr_Seq_prestador,'N')),
		pls_obter_grau_participacao(a.nr_seq_grau_partic),				
		b.nr_seq_apres,
		c.dt_procedimento,
		c.dt_inicio_proc
	into	cd_medico_w,
		nr_seq_conselho_w,			
		nr_seq_grau_partic_w,
		nr_seq_prestador_w,
		nr_seq_prestador_pgto_w,
		ie_tipo_item_w,
		nm_participante_w,
		ds_grau_participacao_w,
		nr_seq_apres_w,
		dt_procedimento_w,
		dt_inicio_proc_w
	from	pls_proc_participante	a,
		pls_grau_participacao	b,				
		pls_conta_proc		c		
	where	a.nr_seq_grau_partic 	= b.nr_sequencia(+)
	and	a.nr_seq_conta_proc	= c.nr_sequencia	
	and	c.nr_sequencia		= nr_seq_conta_proc_p;

	select	decode(ie_tipo_item_w || ie_tipo_despesa_w,'P1','1','P2','3','P3','2','P4','4','M1','6','M2','5','M3','7','M7','8', 'R1','1','R2','3','R3','2','R4','4')
	into	nr_ordem_w
	from	dual;
	
	update	w_pls_resumo_conta
	set	ie_tipo_item		= 'R',
		nr_seq_apres_prof	= nr_seq_apres_w,
		ds_grau_participacao	= ds_grau_participacao_w,
		nm_participante		= nm_participante_w,
		ds_tipo_despesa		= ie_tipo_item_w || ie_tipo_despesa_w,		
		nr_seq_prestador_pgto	= nr_seq_prestador_pgto_w,
		nr_seq_grau_partic	= nr_seq_grau_partic_w,	
		nr_ordem		= nr_ordem_w,
		dt_inicio_item		= dt_inicio_proc_w,
		dt_item			= dt_procedimento_w
	where	nr_sequencia		= nr_seq_item_w;
else		
	/*Gerar os dados do resumo para ser usado na funcao OPS - Analise de Producao Medica */
	pls_gerar_w_resumo_conta_ptu(	nr_seq_conta_w, nr_seq_conta_proc_p, null, 
					null, nr_seq_analise_p, nm_usuario_p, 
					nr_seq_item_criado_w);

end if;
			
update	pls_conta_proc
set	ie_status = 'A'
where	nr_sequencia = nr_seq_conta_proc_p
and	ie_status <> 'D';

update	w_pls_resumo_conta
set	nr_seq_grupo_item 	= nr_seq_grupo_atual_p
where	nr_sequencia 		= nr_seq_item_criado_w;
		 
if	(nvl(ie_comitar_p,'S') = 'S') then
	commit;		
end if;

end pls_atual_proc_ptu_partic;
/