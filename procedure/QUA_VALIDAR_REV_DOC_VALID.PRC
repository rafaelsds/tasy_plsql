create or replace
procedure qua_validar_rev_doc_valid(	nr_seq_revisao_p		number,
				cd_pessoa_validacao_p	varchar2,
				dt_validacao_p		date,
				nm_usuario_p		Varchar2) is 

qt_existe_w		number(05,0);
qt_existe_usu_valid_w		number(05,0);
nr_seq_valid_w		number(10,0);
cd_pessoa_doc_w		varchar2(10);
cd_cargo_w		number(10,0);
nr_seq_ordem_w		number(10);
nm_user_w		varchar2(50);
sequence_document_w number(10);

begin

select	max(nr_seq_ordem_serv)
into	nr_seq_ordem_w
from	qua_doc_revisao
where	nr_sequencia = nr_seq_revisao_p;

select	count(*)
into	qt_existe_w
from	qua_doc_revisao_validacao
where	nr_seq_doc_revisao		= nr_seq_revisao_p
and	dt_validacao is null;

select	max(a.cd_cargo)
into	cd_cargo_w
from	pessoa_fisica a,
	usuario b
where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
and	b.nm_usuario 	= nm_usuario_p;

select	username
into	nm_user_w
from	user_users;

/*Se no existir mais de um validador faz o processo normal*/
if	(qt_existe_w = 0) then
	begin
	update	qua_doc_revisao 
	set	dt_atualizacao		= sysdate, 
		nm_usuario		= nm_usuario_p, 
		dt_validacao		= dt_validacao_p
	where	nr_sequencia 		= nr_seq_revisao_p;
	end;
else
	begin
	/*Se existir mais de um validador, busca a sequencia de validao da pessoa que est validando*/
	select	count(*),
		max(nr_sequencia)
	into	qt_existe_w,
		nr_seq_valid_w
	from	qua_doc_revisao_validacao
	where	nr_seq_doc_revisao	= nr_seq_revisao_p
	and	((cd_pessoa_validacao	= cd_pessoa_validacao_p) or
		(cd_cargo		= cd_cargo_w))
	and	dt_validacao is null;
	
	if	(qt_existe_w > 0) and
		(nvl(nr_seq_valid_w,0) > 0) then
		update	qua_doc_revisao_validacao
		set	dt_atualizacao	= sysdate, 
			nm_usuario	= nm_usuario_p, 
			dt_validacao	= dt_validacao_p
		where	nr_sequencia 	= nr_seq_valid_w;
		
		/*V se existe alguma validao pendente ainda, seno, valida o documento*/
		select	count(*)
		into	qt_existe_w
		from	qua_doc_revisao_validacao
		where	nr_seq_doc_revisao	= nr_seq_revisao_p
		and	dt_validacao is null; 
		
		select	count(*) 
		into 	qt_existe_usu_valid_w
		from 	qua_doc_revisao 
		where 	nr_sequencia = nr_seq_revisao_p 
		and 	dt_validacao is null 
		and 	cd_pessoa_validacao = cd_pessoa_validacao_p;

		if	(qt_existe_w = 0) and ((qt_existe_usu_valid_w > 0) or (nm_user_w != 'CORP'))then
			begin
        update	qua_doc_revisao 
        set	dt_atualizacao		= sysdate, 
          nm_usuario		= nm_usuario_p, 
          dt_validacao		= dt_validacao_p
        where	nr_sequencia 		= nr_seq_revisao_p;

        select	nvl(max(a.nr_seq_doc),0)
        into	  sequence_document_w
        from	  qua_doc_revisao a
        where	  a.nr_sequencia = nr_seq_revisao_p;

        qua_gerar_envio_comunicacao(sequence_document_w,
                                    '0',
                                    nm_usuario_p,
                                    '16',
                                    wheb_usuario_pck.get_cd_estabelecimento,
                                    null,
                                    null,
                                    'N');
			
			end;
		end if;
	end if;	
	end;	
end if;

select	count(*)
into	qt_existe_w
from	qua_doc_revisao_validacao
where	nr_seq_doc_revisao	= nr_seq_revisao_p
and	dt_validacao is null;
		
if (qt_existe_w = 0 and nr_seq_ordem_w is not null) then
	Qua_Inserir_Hist_Validacao(nm_usuario_p,  wheb_mensagem_pck.get_texto(669984), nr_seq_ordem_w);
end if;

commit;

end qua_validar_rev_doc_valid;
/
