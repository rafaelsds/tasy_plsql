create or replace
procedure pls_gerar_analise_pacotes_guia
			(	nr_seq_guia_p			number,
				nr_seq_segurado_p		number,
				nr_seq_ocorrencia_p		number,
				nr_seq_regra_p			number,
				nm_usuario_p			varchar2) is
				
ie_proc_guia_lib_w		varchar2(2)	:= 'N';
ie_status_w			varchar2(2);
ie_finalizar_analise_w		varchar2(1);
qt_pacotes_guia_w		number(10);
nr_seq_guia_proc_w		number(10);


Cursor C01 is
	select	nr_sequencia,
		ie_status
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_p;

begin
select	nvl(max(a.ie_finalizar_analise),'S')
into	ie_finalizar_analise_w
from	pls_ocorrencia	a
where	a.nr_sequencia	= nr_seq_ocorrencia_p;

update	pls_guia_plano_proc
set	ie_status	= 'A'
where	nr_seq_guia	= nr_seq_guia_p;

open C01;
loop
fetch C01 into	
	nr_seq_guia_proc_w,
	ie_status_w;
exit when C01%notfound;
	begin
	insert	into pls_ocorrencia_benef	
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ie_auditoria,
		ie_fechar_conta,
		nr_seq_guia_plano,
		nr_seq_proc,
		nr_seq_ocorrencia,
		nr_seq_regra, 
		nr_seq_segurado,
		ie_finalizar_analise)
	values	(pls_ocorrencia_benef_seq.nextval,
		sysdate,
		nm_usuario_p,
		'S',
		'N',
		nr_seq_guia_p,
		nr_seq_guia_proc_w,
		nr_seq_ocorrencia_p,
		nr_seq_regra_p,
		nr_seq_segurado_p,
		ie_finalizar_analise_w);
	end;
end loop;
close C01;

--commit;

end pls_gerar_analise_pacotes_guia;
/