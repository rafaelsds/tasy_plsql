create or replace
procedure valid_consult_hor_ocorr_rep(
		ds_horarios_p		varchar2,
		cd_intervalo_p		varchar2,
		dt_inicio_prescr_p	date,
		dt_prim_horario_p	date,
		qt_hora_intervalo_p	number,
		qt_min_intervalo_p	number,
		nr_prescricao_p		number,
		ie_urgencia_p		varchar2,
		dt_prev_execucao_p	date,
		ds_horarios_out_p out	varchar2,
		nr_ocorrencia_p out	number) is

ds_horarios_w	varchar2(2000);
nr_ocorrencia_w	number(15,4);
begin
if	(ds_horarios_p is not null) and
	(cd_intervalo_p is not null) and
	(dt_inicio_prescr_p is not null) and	
	(dt_prim_horario_p is not null) and	
	(nr_prescricao_p is not null) and
	(ie_urgencia_p is not null) and
	(dt_prev_execucao_p is not null) then
	begin
		
	ds_horarios_w := eliminar_hor_vigencia_proc(
				ds_horarios_p,
				cd_intervalo_p,
				dt_inicio_prescr_p,
				dt_prim_horario_p,
				qt_hora_intervalo_p,
				qt_min_intervalo_p,
				nr_prescricao_p,
				ie_urgencia_p,
				dt_prev_execucao_p);
				
	if	(ds_horarios_w is not null) then
		begin
		nr_ocorrencia_w := obter_ocorrencias_horarios_rep(ds_horarios_w);
		end;
	end if;
	end;
end if;
ds_horarios_out_p	:= ds_horarios_w;
nr_ocorrencia_p		:= nr_ocorrencia_w;
end valid_consult_hor_ocorr_rep;
/