create or replace
procedure pls_gerar_grupo_aud_disc
			(	nr_seq_conta_p		number,
				nr_seq_analise_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 

ds_conta_w			varchar2(4000);
ie_tipo_conta_w			varchar2(10);
ie_intercambio_w		varchar2(10) := 'N';
ie_conta_medica_w		varchar2(10) := 'N';
ie_tipo_contestacao_w		varchar2(3);
ie_pre_analise_w		varchar2(2);
ie_tipo_item_w			varchar2(1);
ie_origem_conta_w		varchar2(1);
ie_tipo_item_ww			varchar2(1);
ie_fluxo_contest_w		varchar2(1);
ie_despesa_w			varchar2(1)	:= 'N';
ie_tipo_w			varchar2(1);
ie_tipo_despesa_w		varchar2(1);
nr_seq_grupo_w			number(10);
nr_seq_fluxo_w			number(10);		
ie_existe_grupo_analise_w	number(10);
nr_seq_grupo_auditor_w		number(10);
nr_ie_tipo_item_w		number(10);
nr_seq_ocorrencia_w		number(10);
nr_seq_grupo_ww			number(10);
nr_seq_item_w			number(10);
nr_seq_conta_proc_w		number(10);
nr_seq_conta_mat_w		number(10);
nr_seq_oc_benef_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_conta_proc_ww		number(10);
nr_seq_conta_mat_ww		number(10);
nr_seq_ocorr_grupo_w		number(10);
qt_auditoria_w			pls_integer;
ie_origem_analise_w		pls_analise_conta.ie_origem_analise%type;
/*Obter os itens contestados*/
Cursor C01 is
	select	nr_seq_item,
		ie_tipo_item,
		nr_seq_conta
	from	w_pls_discussao_item
	where	nr_seq_analise	= nr_seq_analise_p;

/*Obter as ocorrencias destes itens contestados*/
Cursor C02 is	
	select	nr_seq_ocorrencia,
		nr_seq_proc,
		nr_seq_mat
	from	pls_ocorrencia_benef
	where	((nr_seq_conta = nr_seq_conta_w) and ((nr_seq_proc = nr_seq_conta_proc_w) or (nr_seq_mat = nr_seq_conta_mat_w))) --Ocorr�ncias do item
	or	((nr_seq_conta = nr_seq_conta_w) and (nr_seq_proc is null) and (nr_seq_mat is null));				 --Ocorr�ncias da conta
	
/*Obter os grupos responsaveis por estas ocorrencias*/
Cursor C03 is
	select	a.nr_seq_grupo,
		a.nr_seq_fluxo,
		a.nr_sequencia
	from	pls_ocorrencia_grupo	a
	where	a.nr_seq_ocorrencia 		= nr_seq_ocorrencia_w
	and	a.ie_intercambio		= 'S'
	and	nvl(a.ie_origem_conta,nvl(ie_origem_conta_w,'0')) = nvl(ie_origem_conta_w,'0')
	and	((a.ie_tipo_analise	is null) or (a.ie_tipo_analise = 'A') or (a.ie_tipo_analise = 'C' and ie_origem_analise_w in ('1','3','4','5','6')) or
		(a.ie_tipo_analise	= 'P' and ie_origem_analise_w in ('2','7'))); /*Diego OS 310737*/ /*Robson - para contas de interc�mbio, fatura 2320*/
	
Cursor C04 is
	select	nr_seq_grupo,
		nr_seq_fluxo
	from	pls_fluxo_analise_contest
	where	cd_estabelecimento				= cd_estabelecimento_p
	and	nvl(ie_situacao,'A')				= 'A'
	and	nvl(ie_tipo_contestacao,ie_tipo_contestacao_w)	= ie_tipo_contestacao_w
	order by 2;	

begin
/* Verificar qual o tipo da contesta��o */
select	decode(max(ie_envio_recebimento),'E','P','F')
into	ie_tipo_contestacao_w
from	pls_contestacao			c,
	pls_lote_contestacao		b,
	pls_contestacao_discussao	a
where	a.nr_seq_contestacao	= c.nr_sequencia
and	c.nr_seq_lote		= b.nr_sequencia
and	a.nr_seq_analise	= nr_seq_analise_p;
	
begin
select	nvl(ie_fluxo_contest,'O')
into	ie_fluxo_contest_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;
exception
when others then
	ie_fluxo_contest_w := 'O';
end;

select	max(ie_origem_analise)
into	ie_origem_analise_w
from	pls_analise_conta
where	nr_sequencia	= nr_seq_analise_p;
/*Se for novo fluxo � criado o mesmo com base no declarado em regras na fun��o do A500*/
if	(ie_fluxo_contest_w = 'N') then
	open C04;
	loop
	fetch C04 into	
		nr_seq_grupo_w,
		nr_seq_fluxo_w;
	exit when C04%notfound;
		begin	
		select	count(1)
		into	qt_auditoria_w
		from	pls_auditoria_conta_grupo
		where	nr_seq_analise	= nr_seq_analise_p
		and	nr_seq_grupo	= nr_seq_grupo_w;
		
		if	(qt_auditoria_w = 0) then
			insert into pls_auditoria_conta_grupo
				(nr_sequencia,
				nr_seq_analise,
				nr_seq_grupo,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_ordem,
				ds_conta,
				ie_pre_analise)
			values	(pls_auditoria_conta_grupo_seq.nextval,
				nr_seq_analise_p,
				nr_seq_grupo_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_fluxo_w,
				'',
				'N');
		end if;
		end;
	end loop;
	close C04;
else
	begin
	select	ie_origem_conta,
		ie_tipo_conta
	into	ie_origem_conta_w,
		ie_tipo_conta_w
	from	pls_conta
	where	nr_sequencia	= nr_seq_conta_p;
	exception
	when others then
		ie_origem_conta_w	:= NULL;
		ie_tipo_conta_w		:= 'O';
	end;

	if	(ie_tipo_conta_w = 'I') then
		ie_intercambio_w	:= 'S';
		ie_conta_medica_w	:= null;
	else
		ie_intercambio_w	:= null;
		ie_conta_medica_w	:= 'S';
	end if;

	open C01;
	loop
	fetch C01 into	
		nr_seq_item_w,
		ie_tipo_item_ww,
		nr_seq_conta_w;
	exit when C01%notfound;
		begin
		if	(ie_tipo_item_ww = 'P') then
			nr_seq_conta_proc_w 	:= nr_seq_item_w;
			nr_seq_conta_mat_w	:= null;
		elsif	(ie_tipo_item_ww = 'M') then
			nr_seq_conta_proc_w	:= null;
			nr_seq_conta_mat_w 	:= nr_seq_item_w;
		end if;
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_ocorrencia_w,
			nr_seq_conta_proc_ww,
			nr_seq_conta_mat_ww;
		exit when C02%notfound;
			begin
			ie_despesa_w	:= 'N';
			
			if	(nr_seq_conta_proc_ww is null) and
				(nr_seq_conta_mat_ww is null) then
				ie_despesa_w	:= 'S';
			else
				if	(nr_seq_conta_mat_ww is not null) then
					select	max(a.ie_tipo_despesa),
						'M'
					into	ie_tipo_despesa_w,
						ie_tipo_w
					from	pls_conta_mat	a
					where	a.nr_sequencia	= nr_seq_conta_mat_ww;
				else
					select	max(a.ie_tipo_despesa),
						'P'
					into	ie_tipo_despesa_w,
						ie_tipo_w
					from	pls_conta_proc	a
					where	a.nr_sequencia	= nr_seq_conta_proc_ww;
				end if;
			end if;
			
			open C03;
			loop
			fetch C03 into	
				nr_seq_grupo_w,
				nr_seq_fluxo_w,
				nr_seq_ocorr_grupo_w;
			exit when C03%notfound;
				begin
				if	(ie_despesa_w = 'N') then
					ie_despesa_w	:= pls_obter_se_grupo_tipo_desp(nr_seq_ocorr_grupo_w,ie_tipo_despesa_w,ie_tipo_w);
				end if;
				
				if	(nvl(ie_despesa_w,'S') = 'S') then
					/*Verifica se existe o grupo na analise*/
					select	count(a.nr_sequencia)
					into	ie_existe_grupo_analise_w
					from	pls_auditoria_conta_grupo a,
						pls_analise_conta	  b
					where	b.nr_sequencia		= a.nr_seq_analise	
					and	a.nr_seq_grupo		= nr_seq_grupo_w
					and	b.nr_sequencia		= nr_seq_analise_p;
					--and	a.nr_seq_analise	= nr_seq_analise_p;		
					
					if	(ie_existe_grupo_analise_w  = 0) then
						if	(nr_seq_fluxo_w is null) then
							select	max(a.nr_seq_fluxo_padrao)
							into	nr_seq_fluxo_w
							from	pls_grupo_auditor	a
							where	a.nr_sequencia	= nr_seq_grupo_w;
						end if;
		
						select	min(nr_sequencia)
						into	nr_seq_grupo_ww
						from 	pls_auditoria_conta_grupo
						where	nr_seq_analise	= nr_seq_analise_p
						and	nr_seq_ordem	= nr_seq_fluxo_w;
						
						while	(nvl(nr_seq_grupo_ww,0) <> 0) loop 
							begin
							nr_seq_fluxo_w	:= nr_seq_fluxo_w + 1;
							
							select	min(nr_sequencia)
							into	nr_seq_grupo_ww
							from 	pls_auditoria_conta_grupo
							where	nr_seq_analise	= nr_seq_analise_p
							and	nr_seq_ordem	= nr_seq_fluxo_w;
							end;
						end loop;
						
						select	pls_auditoria_conta_grupo_seq.nextval
						into	nr_seq_grupo_auditor_w
						from	dual;
						
						/*Se n�o existir cria-se o grupo*/		
						insert into pls_auditoria_conta_grupo
							(nr_sequencia,
							nr_seq_analise,
							nr_seq_grupo,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_ordem,
							ds_conta,
							ie_pre_analise)
						values	(nr_seq_grupo_auditor_w,
							nr_seq_analise_p,
							nr_seq_grupo_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_fluxo_w,
							'',
							'N');	
					end if;
				end if;
				end;
			end loop;
			close C03;	
			end;
		end loop;
		close C02;	
		end;
	end loop;
	close C01;
end if;

pls_atualizar_grupo_penden(nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);

end pls_gerar_grupo_aud_disc;
/