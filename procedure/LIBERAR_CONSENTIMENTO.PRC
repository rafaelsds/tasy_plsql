create or replace
procedure Liberar_Consentimento
		(nr_sequencia_p		number,
		nm_usuario_p		varchar2) is

nr_atendimento_w	number(10,0);	
ie_gerar_evolucao_consent_w     varchar2(1) := 'N';
cd_evolucao_w                   number(10);

begin
obter_param_usuario(281, 1631, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_evolucao_consent_w);

update	pep_pac_ci
set	dt_liberacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

begin
	select 	nvl(max(nr_atendimento),0)
	into	nr_atendimento_w
	from 	pep_pac_ci
	where 	nr_sequencia = nr_sequencia_p;
exception
	when others then
	nr_atendimento_w:= 0;
end;

gerar_lancamento_automatico(nr_atendimento_w, null, 551, nm_usuario_p, null,null,null,null,null,null);
if ( ie_gerar_evolucao_consent_w = 'S' ) THEN
    begin
        clinical_notes_pck.gerar_soap(nr_atendimento_w, nr_sequencia_p, 'CONSENT', null, 'P',
                                                       1, cd_evolucao_w);
         update pep_pac_ci
                set
                    cd_evolucao = cd_evolucao_w
                where
                    nr_sequencia = nr_sequencia_p;
    end;
end if;


commit;

end Liberar_Consentimento;
/
