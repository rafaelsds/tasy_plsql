CREATE OR REPLACE
PROCEDURE Excluir_proc_conta_audit(nr_sequencia_p 	 	number,
				   nr_seq_propaci_p	 	number,
				   cd_motivo_exc_conta_p 	number,
				   ds_compl_motivo_excon_p 	varchar2,
				   nr_seq_motivo_auditoria_p	number,
				   nm_usuario_p			varchar) is
				   
nr_atendimento_w	Number(10,0);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.get_cd_estabelecimento;
ie_altera_item_repasse_w	varchar2(1) := 'S';			
BEGIN

obter_param_usuario(1116,189,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_altera_item_repasse_w);

select	max(nr_atendimento)
into	nr_atendimento_w	
from	procedimento_paciente
where	nr_sequencia = nr_seq_propaci_p;

Update	procedimento_paciente a
set 	a.cd_motivo_exc_conta = cd_motivo_exc_conta_p,
	a.ds_compl_motivo_excon = ds_compl_motivo_excon_p,
	a.nm_usuario = nm_usuario_p,
	a.dt_atualizacao = sysdate,
	a.DT_ACERTO_CONTA = null,
	a.NR_INTERNO_CONTA = null
where 	a.nr_sequencia = nr_seq_propaci_p
and	not exists(	select	1
			from	procedimento_repasse x
			where	x.nr_seq_procedimento = a.nr_sequencia
			and	nvl(ie_altera_item_repasse_w,'S') = 'N');

/*insert into log_xxxxxtasy(cd_log, ds_log, dt_atualizacao, nm_usuario)
	values (66985, 'Item Exclu�do pela fun��o Auditoria, Seq = ' || nr_seq_propaci_p || ', Atendimento = ' || nr_atendimento_w, sysdate, nm_usuario_p); */

update	auditoria_propaci a
set 	a.ie_tipo_auditoria = 'X',
	a.qt_ajuste = decode(ds_compl_motivo_excon_p,'999#', a.qt_ajuste, null),
	a.nm_usuario = nm_usuario_p,
	a.dt_atualizacao = sysdate,
	a.nr_seq_motivo = decode(nr_seq_motivo_auditoria_p, 0, a.nr_seq_motivo, nr_seq_motivo_auditoria_p)
where 	a.nr_sequencia = nr_sequencia_p
and	not exists(	select	1
			from	procedimento_repasse x
			where	x.nr_seq_procedimento = a.nr_seq_propaci
			and	nvl(ie_altera_item_repasse_w,'S') = 'N');

commit;

END Excluir_proc_conta_audit;
/