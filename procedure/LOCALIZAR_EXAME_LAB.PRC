CREATE OR REPLACE
PROCEDURE Localizar_Exame_Lab(ds_localizar_p		in varchar2,
					nr_seq_localizar_p	in number,
					cd_estabelecimento_p	in number,
					nm_usuario_p		in varchar2,					
					ds_exames_p		out varchar2,
					ds_grupos_p		out varchar2) IS

nr_sequencia_w 		number(10);
nr_seq_exame_w 		number(10);
nr_seq_superior_w		number(10);
nr_seq_superior_w2	number(10);
nr_seq_grupo_w		number(10);
nr_seq_grupo_w2		number(10);
nr_seq_localizar_w	number(2);
nm_exame_w			VARCHAR2(255);
nr_seq_grupo_pesquisa_w	NUMBER(10);
nr_seq_exame_pesquisa_w	NUMBER(10);
nr_seq_superior_pesq_w	NUMBER(10);
ie_sair_cursor_w	Varchar2(1);
ie_gerar_pai_filho_w	Varchar2(1);


cursor c001 is
	select a.nr_seq_exame,
		 a.nr_seq_superior,
		 a.nr_seq_grupo
	from grupo_exame_lab b,
		exame_laboratorio a
	where a.nr_seq_grupo =  b.nr_sequencia
	  and elimina_acentuacao(upper(a.nm_exame)) like '%'|| elimina_acentuacao(upper(ds_localizar_p)) || '%'
	order by b.ds_grupo_exame_lab,
		   a.nr_seq_superior,
		   a.nm_exame;
		   
CURSOR c003 IS
	SELECT a.nr_seq_exame,
		 a.nr_seq_superior,
		 a.nr_seq_grupo,
		 a.nm_exame
	FROM 	grupo_exame_lab b,
		exame_laboratorio a
	WHERE a.nr_seq_grupo =  b.nr_sequencia
	AND  a.nr_seq_superior IS NULL
	ORDER BY b.ds_grupo_exame_lab,
		   NVL(a.nr_seq_superior,0),
		   a.nm_exame;



CURSOR C002 IS
	SELECT a.nr_seq_exame,
		 a.nr_seq_superior,
		 a.nr_seq_grupo,
		 a.nm_exame
	FROM grupo_exame_lab b,
		exame_laboratorio a
	WHERE a.nr_seq_grupo =  b.nr_sequencia
	AND a.nr_seq_superior IS NOT NULL
	START WITH nr_seq_exame = nr_seq_exame_w
	CONNECT BY a.nr_seq_superior = PRIOR nr_seq_exame
	ORDER BY DECODE(nr_seq_superior, nr_seq_exame_w, a.nM_exame, OBTER_DESC_EXAME(nr_seq_superior)), DECODE(nr_seq_superior, nr_seq_exame_w, nr_Seq_exame, nr_seq_superior), LEVEL;


BEGIN

nr_seq_localizar_w := nr_seq_localizar_p;
nr_sequencia_w	 := 0;
Obter_Param_Usuario(746,12,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gerar_pai_filho_w);

if	(ie_gerar_pai_filho_w = 'N') then

	open c001;
	loop
	fetch c001 into
		nr_seq_exame_w,
		nr_seq_superior_w,
		nr_seq_grupo_w;
	exit when c001%notfound;
	nr_sequencia_w := nr_sequencia_w + 1;
	if (nr_sequencia_w > nr_seq_localizar_p) then
		begin
		ds_exames_p := to_char(nr_seq_exame_w);
		nr_seq_localizar_w := nr_seq_localizar_p + 1;
		exit;
		end;
	end if;
	end loop;
	close c001;

	if 	(ds_exames_p is not null) and
		(nr_seq_superior_w is not null) then
		begin
		ds_exames_p := to_char(nr_seq_superior_w) || ',' || ds_exames_p;
		nr_seq_superior_w2 := nr_seq_superior_w;
		while nr_seq_superior_w2 is not null loop
			begin
			select nr_seq_superior
			into	 nr_seq_superior_w2
			from exame_laboratorio
			where nr_seq_exame = nr_seq_superior_w;
			if (nr_seq_superior_w2 is not null) and (length(to_char(nr_seq_superior_w2) || ',' || ds_exames_p) < 255) then
				ds_exames_p := to_char(nr_seq_superior_w2) || ',' || ds_exames_p;
			end if;
			nr_seq_superior_w := nr_seq_superior_w2;
			end;
		end loop;
		end;
	end if;
	if 	(nr_seq_grupo_w is not null) then
		begin
		ds_grupos_p := to_char(nr_seq_grupo_w);
		nr_seq_grupo_w2 := nr_seq_grupo_w;
		while nr_seq_grupo_w2 is not null loop
			begin
			select nr_seq_superior
			into	 nr_seq_grupo_w2
			from grupo_exame_lab
			where nr_sequencia = nr_seq_grupo_w;
			if (nr_seq_grupo_w2 is not null) then
				ds_grupos_p := to_char(nr_seq_grupo_w2) || ',' || ds_grupos_p;
			end if;
			nr_seq_grupo_w := nr_seq_grupo_w2;
			end;
		end loop;
		end;
	else
		nr_seq_localizar_w := 0;
	end if;
else

	OPEN c003;
	LOOP
	FETCH c003 INTO
		nr_seq_exame_w,
		nr_seq_superior_w,
		nr_seq_grupo_w,
		nm_exame_w;
	EXIT WHEN c003%NOTFOUND;
		BEGIN
		if	(ie_sair_cursor_w	= 'S') then
			exit;
		end if;
		
		ie_sair_cursor_w	:= 'N';

		IF	(elimina_acentuacao(UPPER(nm_exame_w)) LIKE '%'|| elimina_acentuacao(UPPER(ds_localizar_p)) || '%') THEN

			nr_sequencia_w := nr_sequencia_w + 1;

			IF 	(nr_sequencia_w > nr_seq_localizar_p) THEN

				nr_seq_localizar_w := nr_seq_localizar_p + 1;
				ds_exames_p := TO_CHAR(nr_seq_exame_w);
				nr_seq_grupo_pesquisa_w := nr_seq_grupo_w;
				nr_seq_exame_pesquisa_w	:= nr_seq_exame_w;
				nr_seq_superior_pesq_w	:= nr_seq_superior_w;
				EXIT;
			END IF;
		END IF;

		
		OPEN C002;
		LOOP
		FETCH C002 INTO
			nr_seq_exame_w,
			nr_seq_superior_w,
			nr_seq_grupo_w,
			nm_exame_w;
		EXIT WHEN C002%NOTFOUND;
			BEGIN	

			IF	(elimina_acentuacao(UPPER(nm_exame_w)) LIKE '%'|| elimina_acentuacao(UPPER(ds_localizar_p)) || '%') THEN		
				nr_sequencia_w := nr_sequencia_w + 1;		
				IF 	(nr_sequencia_w > nr_seq_localizar_p) THEN			
					nr_seq_localizar_w := nr_seq_localizar_p + 1;
					ds_exames_p := TO_CHAR(nr_seq_exame_w);
					nr_seq_grupo_pesquisa_w := nr_seq_grupo_w;
					nr_seq_exame_pesquisa_w	:= nr_seq_exame_w;
					nr_seq_superior_pesq_w	:= nr_seq_superior_w;
					ie_sair_cursor_w	:= 'S';
					EXIT;
				END IF;
			END IF;
			END;
		END LOOP;
		CLOSE C002;


		END;
	END LOOP;
	CLOSE c003;

	IF 	(ds_exames_p IS NOT NULL) AND
		(nr_seq_superior_pesq_w IS NOT NULL) THEN
		BEGIN
		ds_exames_p := TO_CHAR(nr_seq_superior_pesq_w) || ',' || ds_exames_p;
		nr_seq_superior_w2 := nr_seq_superior_pesq_w;
		
		WHILE nr_seq_superior_w2 IS NOT NULL LOOP
			BEGIN
			SELECT nr_seq_superior
			INTO	 nr_seq_superior_w2
			FROM exame_laboratorio
			WHERE nr_seq_exame = nr_seq_superior_pesq_w;
			IF (nr_seq_superior_w2 IS NOT NULL) THEN
				ds_exames_p := TO_CHAR(nr_seq_superior_w2) || ',' || ds_exames_p;
			END IF;
			nr_seq_superior_pesq_w := nr_seq_superior_w2;
			END;
		END LOOP;
		END;
	END IF;

	IF 	( nr_seq_grupo_pesquisa_w IS NOT NULL) THEN
		BEGIN
		ds_grupos_p := TO_CHAR(nr_seq_grupo_pesquisa_w);
		nr_seq_grupo_w2 := nr_seq_grupo_pesquisa_w;
		WHILE nr_seq_grupo_w2 IS NOT NULL LOOP
			BEGIN
			SELECT nr_seq_superior
			INTO	 nr_seq_grupo_w2
			FROM grupo_exame_lab
			WHERE nr_sequencia =nr_seq_grupo_pesquisa_w;
			IF (nr_seq_grupo_w2 IS NOT NULL) THEN
				ds_grupos_p := TO_CHAR(nr_seq_grupo_w2) || ',' || ds_grupos_p;
			END IF;
			nr_seq_grupo_pesquisa_w := nr_seq_grupo_w2;
			END;
		END LOOP;
		END;
	ELSE
		nr_seq_localizar_w := 0;
	END IF;	
end if;

END Localizar_Exame_Lab;
/
