create or replace
procedure pls_vincular_titulo_lote_pag
			(	ds_lista_titulo_p		varchar2,
				ie_tipo_titulo_p		varchar2,
				nr_seq_evento_p		number,
				nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

cd_conta_contabil_w		varchar2(20);
cd_cgc_w			varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
ie_final_w				varchar2(1);
vl_saldo_titulo_w			number(15,2);
nr_titulo_w			number(10);
nr_seq_evento_movto_w		number(10);
nr_seq_prestador_w		number(10)	:= null;
qt_titulo_w			number(10);
nr_seq_pagamento_w		number(10);
nr_seq_pag_prest_w		number(10);
dt_vencimento_w			date;

Cursor C01 is
	select	nr_titulo
	from	titulo_pagar
	where	Obter_Se_Contido(nr_titulo,ds_lista_titulo_p) = 'S'
	and	ie_tipo_titulo_p	= 'P'
	union 	all
	select	nr_titulo
	from	titulo_receber
	where	Obter_Se_Contido(nr_titulo,ds_lista_titulo_p) = 'S'
	and	ie_tipo_titulo_p	= 'R';
	
begin
open C01;
loop
fetch C01 into	
	nr_titulo_w;
exit when C01%notfound;
	begin
	if	(ie_tipo_titulo_p = 'P') then
		select	max(cd_pessoa_fisica),
			max(cd_cgc),
			max(vl_saldo_titulo),
			max(dt_vencimento_original)
		into	cd_pessoa_fisica_w,
			cd_cgc_w,
			vl_saldo_titulo_w,
			dt_vencimento_w
		from	titulo_pagar
		where	nr_titulo	= nr_titulo_w;
		
		if	(cd_pessoa_fisica_w is not null) then
			select	max(nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		elsif	(cd_cgc_w is not null) then
			select	max(nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador
			where	cd_cgc	= cd_cgc_w;
		else
			nr_seq_prestador_w	:= null;
		end if;
		
		select	count(1)
		into	qt_titulo_w
		from	pls_pagamento_item b,
			pls_pagamento_prestador a
		where	a.nr_sequencia		= b.nr_seq_pagamento
		and	b.nr_tit_pagar_origem   = nr_titulo_w
		and	a.nr_seq_prestador	= nr_seq_prestador_w
		and	rownum = 1;
		
		if	(qt_titulo_w > 0) or
			(nr_seq_prestador_w is null) then
			goto final;
		end if;
	else
		select	max(cd_pessoa_fisica),
			max(cd_cgc),
			max(vl_saldo_titulo),
			max(dt_vencimento)
		into	cd_pessoa_fisica_w,
			cd_cgc_w,
			vl_saldo_titulo_w,
			dt_vencimento_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;
		
		if	(cd_pessoa_fisica_w is not null) then
			select	max(nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		elsif	(cd_cgc_w is not null) then
			select	max(nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador
			where	cd_cgc	= cd_cgc_w;
		else
			nr_seq_prestador_w	:= null;
		end if;
		
		select	count(1)
		into	qt_titulo_w
		from	pls_pagamento_item b,
			pls_pagamento_prestador a
		where	a.nr_sequencia		= b.nr_seq_pagamento
		and	b.nr_tit_receber_origem = nr_titulo_w
		and	a.nr_seq_prestador	= nr_seq_prestador_w
		and	rownum = 1;
		
		if	(qt_titulo_w > 0) or
			(nr_seq_prestador_w is null) then
			goto final;
		end if;
	end if;
	
	select	max(nr_sequencia)
	into	nr_seq_pagamento_w
	from	pls_pagamento_prestador
	where	nr_seq_prestador 	= nr_seq_prestador_w
	and	nr_seq_lote		= nr_seq_lote_p;
	
	if	(nr_seq_pagamento_w is null) then
		select	pls_pagamento_prestador_seq.nextval
		into	nr_seq_pag_prest_w
		from	dual;
	
		insert into pls_pagamento_prestador
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_lote,
			nr_seq_prestador,
			vl_pagamento)
		values	(nr_seq_pag_prest_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_lote_p,
			nr_seq_pag_prest_w,
			vl_saldo_titulo_w);
	else
		nr_seq_pag_prest_w := nr_seq_pagamento_w;		
	end if;	
	
	insert into pls_pagamento_item
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_evento,
		nr_seq_pagamento,
		vl_item,
		nr_tit_receber_origem,
		nr_tit_pagar_origem)
	values	(pls_pagamento_item_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_evento_p,
		nr_seq_pag_prest_w,
		vl_saldo_titulo_w,
		decode(ie_tipo_titulo_p,'R',nr_titulo_w,null),
		decode(ie_tipo_titulo_p,'P',nr_titulo_w,null));
	
	<<final>>
	ie_final_w	:= 'S';
	end;
end loop;
close C01;

commit;

end pls_vincular_titulo_lote_pag;
/