create or replace
procedure copiar_grupo_usuario(nm_usuario_orig_p	Varchar2,
			       nm_usuario_dest_p	varchar2,
			       nm_usuario_p		varchar2) is

nr_seq_grupo_w	number(10);
nr_seq_usu_grupo_w	number(10);


Cursor C01 is
	Select 	a.nr_sequencia,
		b.nr_sequencia
	from	grupo_usuario a,
		usuario_grupo b
	where	a.nr_sequencia = b.nr_seq_grupo
	and	b.nm_usuario_grupo = nm_usuario_orig_p;

begin


open C01;
loop
fetch C01 into
	nr_seq_grupo_w,
	nr_seq_usu_grupo_w;
exit when C01%notfound;
	begin
		insert into usuario_grupo select usuario_grupo_seq.nextval,
						 nr_seq_grupo_w,
						 sysdate,
						 nm_usuario_p,
						 nm_usuario_dest_p,
						 sysdate,
						 nm_usuario_p,
						 ie_situacao
					 from 	 usuario_grupo
					 where	 nr_seq_grupo = nr_seq_grupo_w
					 and	 nr_sequencia = nr_seq_usu_grupo_w;
	end;
end loop;
close C01;


commit;

end copiar_grupo_usuario;
/