create or replace
procedure Ag_exame_insere_Obs_Bloq(	cd_agenda_p		number,
					dt_agenda_p		date,
					ds_observacao_p		out varchar2) is 

					
ds_observacao_w	varchar2(255) := '';

Cursor C01 is
	SELECT 	a.ds_observacao	
	FROM 	(	SELECT	b.nr_sequencia,
				b.ds_observacao
			FROM   	agenda_bloqueio b,
				agenda_paciente a
			WHERE  	b.cd_agenda    = cd_agenda_p
			AND	a.cd_agenda    = b.cd_agenda
			AND   	dt_agenda_p BETWEEN b.dt_inicial AND b.dt_final
			AND 	((obter_cod_dia_semana(dt_agenda_p) = b.ie_dia_semana) OR ((b.ie_dia_semana = 9) and b.ie_dia_semana not in (1,7)) or (b.ie_dia_semana IS NULL))
			AND substr(obter_valor_dominio(1007, b.ie_motivo_bloqueio),1,200) = (select max(substr(obter_valor_dominio(1007, IE_MOTIVO_BLOQUEIO),1,200)) 
																				from   agenda_bloqueio
																				WHERE  	cd_agenda    = cd_agenda_p
																				AND   	dt_agenda_p BETWEEN dt_inicial AND dt_final)
			ORDER BY	b.nr_sequencia DESC) a
	WHERE
	ROWNUM <= 1;

begin

open C01;
loop
fetch C01 into	
	ds_observacao_w;
exit when C01%notfound;
	begin
	ds_observacao_p := ds_observacao_w;
	end;
end loop;
close C01;


end Ag_exame_insere_Obs_Bloq;
/
