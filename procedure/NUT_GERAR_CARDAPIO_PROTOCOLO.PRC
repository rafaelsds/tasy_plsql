create or replace
procedure nut_gerar_cardapio_protocolo(	nr_seq_protocolo_p 	number,
					ie_terceirizado_p  	varchar2,
					dt_vigencia_inicial_p 	varchar2,
					dt_vigencia_final_p  	varchar2,
					ie_semana_p 		varchar2) is
					
cd_dia_semana_w			number(3);			
nr_seq_nut_cardapio_dia_novo_w	nut_cardapio_dia.nr_sequencia%type;
			
nr_seq_prot_opcao_w		nut_cardapio_prot_opcao.nr_sequencia%type;
nr_seq_grupo_producao_w		nut_cardapio_prot_opcao.nr_seq_grupo_producao%type;
cd_dieta_w			nut_cardapio_prot_opcao.cd_dieta%type;
nr_seq_opcao_w			nut_cardapio_prot_opcao.nr_seq_opcao%type;
nr_seq_local_w			nut_cardapio_prot_opcao.nr_seq_local%type;
nr_seq_servico_w		nut_cardapio_prot_opcao.nr_seq_servico%type;
qt_refeicao_opcao_w		nut_cardapio_prot_opcao.qt_refeicao%type;
ie_cardapio_padrao_w		nut_cardapio_prot_opcao.ie_cardapio_padrao%type;
ie_dia_semana_w			nut_cardapio_prot_opcao.ie_dia_semana%type;

nr_seq_prot_cardapio_w		nut_cardapio_prot_cardapio.nr_sequencia%type;
nr_seq_comp_w			nut_cardapio_prot_cardapio.nr_seq_comp%type;
nr_seq_receita_w		nut_cardapio_prot_cardapio.nr_seq_receita%type;
qt_refeicao_w			nut_cardapio_prot_cardapio.qt_refeicao%type;

nr_seq_prot_refeicao_w		nut_cardapio_prot_refeicao.nr_sequencia%type;
nr_seq_refeicao_w		nut_cardapio_prot_refeicao.nr_seq_refeicao%type;

cursor c01 is
	select 	nr_seq_grupo_producao,
		cd_dieta,
		nr_seq_opcao,
		nr_seq_local,
		nr_seq_servico,
		qt_refeicao,
		ie_cardapio_padrao,
		nr_sequencia,
		nvl(ie_dia_semana,1)
	from   	nut_cardapio_prot_opcao
	where  	nr_seq_protocolo = nr_seq_protocolo_p;

cursor c02 is
	select	nr_seq_comp,
		nr_seq_receita,
		qt_refeicao 
	from  	nut_cardapio_prot_cardapio
	where	nr_seq_prot_opcao = nr_seq_prot_opcao_w;	
	
cursor c03 is
	select	nr_seq_refeicao
	from	nut_cardapio_prot_refeicao
	where	nr_seq_prot_opcao = nr_seq_prot_opcao_w;
	
begin

if(nr_seq_protocolo_p is not null)then
		
	open	c01;
		loop
		fetch	c01 into
			nr_seq_grupo_producao_w,
			cd_dieta_w,
			nr_seq_opcao_w,
			nr_seq_local_w,
			nr_seq_servico_w,
			qt_refeicao_opcao_w,
			ie_cardapio_padrao_w,
			nr_seq_prot_opcao_w,
			ie_dia_semana_w;
		exit	when	c01%notfound;

		select	nut_cardapio_dia_seq.nextval
		into	nr_seq_nut_cardapio_dia_novo_w
		from	dual;
		
		insert into nut_cardapio_dia(
			nr_sequencia,
			cd_estabelecimento,
			nr_seq_grupo_producao,
			cd_dieta,
			nr_seq_opcao,
			nr_seq_local,
			nr_seq_servico,
			qt_pessoa_atend,
			ie_cardapio_padrao,
			ie_dia_semana,
			ie_semana,
			dt_vigencia_inicial,
			dt_vigencia_final,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_atualizacao,
			nm_usuario)
		values(	nr_seq_nut_cardapio_dia_novo_w,
			wheb_usuario_pck.get_cd_estabelecimento,
			nr_seq_grupo_producao_w,
			cd_dieta_w,
			nr_seq_opcao_w,
			nr_seq_local_w,
			nr_seq_servico_w,
			qt_refeicao_opcao_w,
			ie_cardapio_padrao_w,
			ie_dia_semana_w,
			ie_semana_p,
			dt_vigencia_inicial_p,
			dt_vigencia_final_p,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			wheb_usuario_pck.get_nm_usuario);
		
		if(ie_terceirizado_p = 'S') then
			open	c03;
				loop
				fetch	c03 into
					nr_seq_refeicao_w;
				exit	when	c03%notfound;
				
				insert into nut_cardapio_refeicao
					(nr_sequencia,
					nr_seq_cardapio_dia,
					nr_seq_refeicao,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario)
				values(	nut_cardapio_refeicao_seq.nextval,
					nr_seq_nut_cardapio_dia_novo_w,
					nr_seq_refeicao_w,
					sysdate,
					wheb_usuario_pck.get_nm_usuario,
					sysdate,
					wheb_usuario_pck.get_nm_usuario);
				
			end loop;
			close c03;
		else 
			open	c02;
				loop
				fetch	c02 into
					nr_seq_comp_w,
					nr_seq_receita_w,
					qt_refeicao_w;
				exit	when	c02%notfound;
				
				insert into nut_cardapio
					(nr_sequencia,
					nr_seq_card_dia,
					nr_seq_comp,
					nr_seq_receita,
					qt_refeicao,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario)
				values(	nut_cardapio_seq.nextval,
					nr_seq_nut_cardapio_dia_novo_w,
					nr_seq_comp_w,
					nr_seq_receita_w,
					qt_refeicao_w,
					sysdate,
					wheb_usuario_pck.get_nm_usuario,
					sysdate,
					wheb_usuario_pck.get_nm_usuario);
				
			end loop;
			close c02;
		end if;

	end loop;
	close c01;

end if;

commit;

end nut_gerar_cardapio_protocolo;
/