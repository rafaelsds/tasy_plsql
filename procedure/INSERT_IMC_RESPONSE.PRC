 create or replace
procedure insert_imc_response(	nr_seq_claim_p			in imc_response.nr_seq_claim%type,
								nr_account_p			in imc_response.nr_account%type,
								cd_process_status_p		in imc_response.cd_process_status%type,
								cd_claim_exp_p			in imc_response.cd_claim_exp%type,
								ds_claim_exp_p			in imc_response.ds_claim_exp%type,
								cd_claim_p				in imc_response.cd_claim%type,
								nm_first_p				in imc_response.nm_first%type,
								cd_medicare_card_p		in imc_response.cd_medicare_card%type,
								nr_patient_ref_p		in imc_response.nr_patient_ref%type,
								cd_fund_status_p		in imc_response.cd_fund_status%type,
								ie_medicare_code_p		in imc_response.ie_medicare_code%type,
								cd_medicare_status_p	in imc_response.cd_medicare_status%type,
								cd_status_p				in imc_response.cd_status%type,
								cd_transaction_p		in imc_response.cd_transaction%type,
								cd_clm_asses_fund_p		in imc_response.cd_clm_asses_fund%type,
								nm_usuario_p			in imc_claim.nm_usuario%type) is
								
nr_seq_w		imc_response.nr_sequencia%type;

begin

select	imc_response_seq.nextval
into	nr_seq_w
from	dual;

insert into imc_response (
						dt_atualizacao,
						nr_seq_claim,
						nr_sequencia,
						nm_usuario,
						nr_account,
						cd_process_status,
						cd_claim_exp,
						ds_claim_exp,
						cd_claim,
						nm_first,
						cd_medicare_card,
						nr_patient_ref,
						cd_fund_status,
						ie_medicare_code,
						cd_medicare_status,
						cd_status,
						cd_transaction,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						cd_clm_asses_fund)
			values (
						sysdate,
						nr_seq_claim_p,
						nr_seq_w,
						nm_usuario_p,
						nr_account_p,
						cd_process_status_p,
						cd_claim_exp_p,
						ds_claim_exp_p,
						cd_claim_P,
						nm_first_p,
						cd_medicare_card_p,
						nr_patient_ref_p,
						cd_fund_status_p,
						ie_medicare_code_p,
						cd_medicare_status_p,
						cd_status_p,
						cd_transaction_p,
						nm_usuario_P,
						sysdate,
						cd_clm_asses_fund_p);

commit;

end insert_imc_response;
/
