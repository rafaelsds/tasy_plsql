create or replace
procedure hdjf_importar_conta_ctb is 

ds_erro_w			varchar2(2000);
ie_numero_w			number(20);
cd_conta_contabil_W		varchar2(20);


Cursor C01 is
select	a.cd_empresa,
	a.cd_estabelecimento,
	a.cd_conta_contabil,
	a.ie_tipo,
	a.cd_classificacao,
	a.ds_conta_contabil,
	a.cd_classif_ecd,
	a.ie_centro_custo
from	hdjf_conta_contabil a;

vet01	c01%rowtype;

begin

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	
	begin
	ie_numero_w	:= somente_numero(vet01.cd_conta_contabil);
	
	if	(ie_numero_w <> 0) then
		
		cd_conta_contabil_w	:=	'00'||vet01.cd_conta_contabil;
		
		insert into conta_contabil (	
			cd_conta_contabil,
			ds_conta_contabil,
			ie_situacao,
			dt_atualizacao,
			nm_usuario,
			ie_centro_custo,
			ie_tipo,
			cd_classificacao,
			cd_empresa,
			ie_compensacao,
			cd_classif_ecd,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	cd_conta_contabil_w,
			vet01.ds_conta_contabil,
			'A',
			sysdate,
			'Importacao',
			'N',
			vet01.ie_tipo,
			vet01.cd_classificacao,
			vet01.cd_empresa,
			'N',
			vet01.cd_classif_ecd,
			sysdate,
			'Importacao');
		
	end if;
	exception
		when others then
		
		ds_erro_w	:= substr(sqlerrm(sqlcode),1,2000);
		
		insert into log_tasy (	
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values(	sysdate,
			'Importacao',
			1009,
			'Conta: '||cd_conta_contabil_w||' '||ds_erro_w);
	end;
	
end loop;
close C01;

commit;

end hdjf_importar_conta_ctb;
/