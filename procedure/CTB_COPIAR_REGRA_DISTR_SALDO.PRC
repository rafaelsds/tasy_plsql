create or replace
procedure ctb_copiar_regra_distr_saldo( nr_seq_regra_grupo_distrib_p	number,
					cd_empresa_origem_p		number,
					cd_estab_origem_p		number,
					cd_estab_destino_p		number,			
					ie_copiar_todos_estab_p		varchar2,
					nm_usuario_p			varchar2) is					   
				
Cursor c_estabelecimento is
	select	cd_estabelecimento
	from	estabelecimento
	where	cd_empresa = cd_empresa_origem_p
	and	((cd_estabelecimento = cd_estab_destino_p
	and	ie_copiar_todos_estab_p = 'N')
	or	(ie_copiar_todos_estab_p = 'S'
	and	cd_estabelecimento <> cd_estab_origem_p));
		
vet_estabelecimento c_estabelecimento%rowtype;			   
							   
begin

open c_estabelecimento;
loop
fetch c_estabelecimento into
	vet_estabelecimento;
exit when c_estabelecimento%notfound;
	begin
	
	insert into ctb_regra_rateio_grupo(	nr_sequencia,
		nm_usuario,
		nm_usuario_nrec,
		dt_atualizacao,
		dt_atualizacao_nrec,
		cd_estabelecimento,
		dt_fim_vigencia,
		dt_inicio_vigencia,
		nr_seq_grupo,
		pr_rateio)
	(select ctb_regra_rateio_grupo_seq.nextval,
		nm_usuario_p,
		nm_usuario_p,
		sysdate,
		sysdate,
		vet_estabelecimento.cd_estabelecimento,
		dt_fim_vigencia,
		dt_inicio_vigencia,
		nr_seq_grupo,
		pr_rateio
	from	ctb_regra_rateio_grupo
	where	nr_sequencia = nr_seq_regra_grupo_distrib_p);
	end;
			
end loop;
close c_estabelecimento;

commit;

end ctb_copiar_regra_distr_saldo;
/