create or replace
procedure liberar_estornar_controle_lav(		nr_sequencia_p		number,
					ie_opcao_p		number,
					nm_usuario_p		Varchar2) is 

/* ie_opcao_p
0 - Liberar
1 - Estornar libera��o
*/						
						
begin

if	(ie_opcao_p = 0) then
	update	rop_ciclo_lavagem
	set	dt_liberacao	= sysdate,
		nm_usuario_lib	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
elsif	(ie_opcao_p = 1) then
	update	rop_ciclo_lavagem
	set	dt_liberacao	= null,
		nm_usuario_lib	= null
	where	nr_sequencia	= nr_sequencia_p;
end if;

commit;

end liberar_estornar_controle_lav;
/