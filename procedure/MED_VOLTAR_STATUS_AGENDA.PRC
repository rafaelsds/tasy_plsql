create or replace
procedure med_voltar_status_agenda (	nr_seq_agenda_p		number,
				ie_status_agenda_p	varchar2,
				nm_usuario_p		varchar2) is


				
begin
if	(nr_seq_agenda_p is not null) and
	(ie_status_agenda_p is not null) then
	
	update	agenda_consulta
	set	ie_status_agenda	= ie_status_agenda_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p		
	where	nr_sequencia	= nr_seq_agenda_p;	
	
end if;

commit;

end med_voltar_status_agenda;
/