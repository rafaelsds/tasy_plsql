create or replace
procedure create_acc_batch_receivable(	cd_estabelecimento_p	number,
					ie_tipo_lote_p		varchar2,
					nr_seq_classif_finan_p	number,
					nm_usuario_p		varchar2) is 

					
nr_lote_contabil_w	number(10);
parametro_w	techone_pck.batchparameters_tp;

					
begin

wheb_usuario_pck.set_nm_usuario(nm_usuario_p);
wheb_usuario_pck.set_cd_estabelecimento(cd_estabelecimento_p);
philips_param_pck.set_nr_seq_idioma(9);

nr_lote_contabil_w	:= techone_pck.create_batch(	cd_estabelecimento_p,
							5,
							sysdate,
							nm_usuario_p);
							
if	(nr_lote_contabil_w	is not null) then
					
	
	parametro_w.DS_VALOR_PARAMETRO	:= nvl(ie_tipo_lote_p,'A');
	techone_pck.update_parameters(	nr_lote_contabil_w,
					1,
					parametro_w);
	parametro_w.DS_VALOR_PARAMETRO	:= null;			
	
	
	
	if	(nr_seq_classif_finan_p	is not null) then
		parametro_w.VL_PARAMETRO	:= nr_seq_classif_finan_p;
		techone_pck.update_parameters(	nr_lote_contabil_w,
						2,
						parametro_w);
		parametro_w.VL_PARAMETRO	:= null;
	end if;

					
	techone_pck.Generate_batch(	nr_lote_contabil_w,
					nm_usuario_p,
					'N');		

					
	techone_pck.send_integration(	nr_lote_contabil_w,
					nm_usuario_p);
	
end if;

commit;

end create_acc_batch_receivable;
/