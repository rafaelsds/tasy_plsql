create or replace
procedure copiar_estimativa_proposta (
		nr_seq_prop_origem_p	number,
		nr_seq_prop_destino_p	number,
		nm_usuario_p		varchar2) is
		
begin
if	(nr_seq_prop_origem_p is not null) and
	(nr_seq_prop_destino_p is not null) and
	(nm_usuario_p is not null) then
	begin
	insert into com_cliente_prop_estim(
		nr_sequencia,
		nr_seq_proposta,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_apres,
		nr_seq_mod_impl,
		ds_complemento,
		qt_hora,
		nr_seq_fase,
		nr_seq_estimativa)
	select	com_cliente_prop_estim_seq.nextval,
		nr_seq_prop_destino_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_apres,
		nr_seq_mod_impl,
		ds_complemento,
		qt_hora,
		nr_seq_fase,
		nr_seq_estimativa
	from	com_cliente_prop_estim
	where	nr_seq_proposta = nr_seq_prop_origem_p;
	end;
end if;
commit;
end copiar_estimativa_proposta;
/