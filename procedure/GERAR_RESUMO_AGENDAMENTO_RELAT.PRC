create or replace
procedure Gerar_Resumo_Agendamento_Relat(
				dt_inicio_p		date,
				dt_final_p		date,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is
				
nr_sequencia_w		number(10);
dt_inicio_agendamento_w	date;
dt_fim_agendamento_w	date;
cd_profissional_w	varchar2(10);
qt_consulta_w		number(10);
qt_exame_w		number(10);
qt_agendamento_w	number(10);
qt_confirmado_w		Number(10);
qt_cancelamento_w	number(10);
ie_status_tasy_w	varchar2(15);
qt_min_duracao_w	number(15,2);
qt_transf_cons_w	number(10);
qt_transf_exame_w	number(10);
qt_transferido_w	number(10);
ie_consid_agend_cons_w	varchar2(1);
qt_canc_consulta_w	number(10,0);	
qt_canc_exame_w		number(10,0);
dt_confirmacao_w	date;
dt_cancelamento_w	date;
dt_transferencia_w	date;
nr_seq_consulta_w	number(10);
nr_seq_exame_w		number(10);
nr_seq_proc_interno_w	number(10);
nr_seq_transferido_w	number(10);


	
Cursor C01 is
select	a.nr_sequencia,
	a.dt_inicio_agendamento,
	a.dt_fim_agendamento,
	a.cd_profissional,
	b.ie_status_tasy
from	agenda_integrada_status b,
	agenda_integrada a
where	a.dt_inicio_agendamento	>= dt_inicio_p
and	a.dt_fim_agendamento	<= fim_dia(dt_final_p)
and	a.nr_seq_status		= b.nr_sequencia
and	nvl(a.cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p;
--and	((a.cd_profissional	= cd_profissional_p) or (cd_profissional_p is null));


Cursor C02 is --AGENDA EXAMES
	select	c.nr_sequencia,
		a.dt_confirmacao,
		c.cd_profissional
	from	agenda_paciente a,
		agenda_integrada_item b,
		agenda_integrada c
	where	a.nr_sequencia = b.nr_seq_agenda_exame
	and	b.nr_seq_agenda_int = c.nr_sequencia
	and	a.dt_confirmacao 	between trunc(dt_inicio_p) and trunc(dt_final_p) + 83699/86400
	--and	((c.cd_profissional	= cd_profissional_p) or (cd_profissional_p is null))
	and	a.ie_status_agenda	<> 'C';
	
Cursor C03 is --AGENDA CONSULTA
	select	c.nr_sequencia,
		a.dt_confirmacao,
		c.cd_profissional
	from	agenda_consulta a,
		agenda_integrada_item b,
		agenda_integrada c
	where	a.nr_sequencia = b.nr_seq_agenda_cons
	and	b.nr_seq_agenda_int = c.nr_sequencia
	and	a.dt_confirmacao between trunc(dt_inicio_p) and trunc(dt_final_p) + 83699/86400
	--and	((c.cd_profissional	= cd_profissional_p) or (cd_profissional_p is null))
	and	a.ie_status_agenda	<> 'C';
	
Cursor C04 is --CANCELADOS AGENDA EXAMES
	select	c.nr_sequencia,
		a.dt_cancelamento,
		c.cd_profissional
	from	agenda_paciente a,
		agenda_integrada_item b,
		agenda_integrada c
	where	a.nr_sequencia = b.nr_seq_agenda_exame
	and	b.nr_seq_agenda_int = c.nr_sequencia
	and	a.dt_cancelamento 	between trunc(dt_inicio_p) and trunc(dt_final_p) + 83699/86400
	--and	((c.cd_profissional	= cd_profissional_p) or (cd_profissional_p is null))
	and	a.ie_status_agenda	= 'C';


Cursor C05 is --CANCELADOS AGENDA CONSULTA
	select	c.nr_sequencia,
		a.dt_cancelamento,
		c.cd_profissional
	from	agenda_consulta a,
		agenda_integrada_item b,
		agenda_integrada c
	where	a.nr_sequencia = b.nr_seq_agenda_cons
	and	b.nr_seq_agenda_int = c.nr_sequencia
	and	a.dt_cancelamento 	between trunc(dt_inicio_p) and trunc(dt_final_p) + 83699/86400
	--and	((c.cd_profissional	= cd_profissional_p) or (cd_profissional_p is null))
	and	a.ie_status_agenda	= 'C';
		
Cursor C06 is
	select	b.cd_profissional,
		a.nr_sequencia,
		a.dt_transferencia
	from	agenda_integrada_item a,
		agenda_integrada b
	where	b.nr_sequencia 		= a.nr_seq_agenda_int
	and	a.dt_transferencia	between	trunc(dt_inicio_p) and trunc(dt_final_p) + 83699/86400;
	--and	((b.cd_profissional	= cd_profissional_p) or (cd_profissional_p is null));
	

begin

Obter_Param_Usuario(869, 189, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_consid_agend_cons_w);

delete	w_resumo_agendamento
where	nm_usuario	= nm_usuario_p;

commit;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	dt_inicio_agendamento_w,
	dt_fim_agendamento_w,
	cd_profissional_w,
	ie_status_tasy_w;
exit when C01%notfound;
	begin
	qt_transferido_w	:= 0;	
	qt_agendamento_w	:= 1;
	qt_confirmado_w		:= 0;
	qt_cancelamento_w	:= 0;
	qt_min_duracao_w	:= Obter_Min_Entre_Datas(dt_inicio_agendamento_w,dt_fim_agendamento_w,1);
	if	(ie_status_tasy_w = 'AG') then
	
		if	(ie_consid_agend_cons_w = 'S') then
			begin
			select	max(nr_seq_proc_interno)
			into	nr_seq_proc_interno_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int	= nr_sequencia_w
			and	ie_tipo_agendamento 	= 'E';
			
			
			select	sum(decode(ie_tipo_agendamento,'C', decode(Obter_classif_tasy_Agecons(ie_classif_agenda), 'N', 1, null, 1))),
				sum(decode(ie_tipo_agendamento,'E',1,decode(Obter_classif_tasy_Agecons(ie_classif_agenda),'X',1,0)))
			into	qt_consulta_w, 
				qt_exame_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int	= nr_sequencia_w;		
			

			select	count(*)
			into	qt_agendamento_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int	= nr_sequencia_w
			and	((nr_seq_agenda_exame is not null) or (nr_seq_agenda_cons is not null));

			
			if	(nr_seq_proc_interno_w = 6) then
				qt_consulta_w 		:= qt_consulta_w	- 1;
				qt_agendamento_w	:= qt_agendamento_w	- 1;
			end if;

			
			end;
		elsif	(ie_consid_agend_cons_w = 'N') then
			
			select	max(nr_seq_proc_interno)
			into	nr_seq_proc_interno_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int	= nr_sequencia_w
			and	ie_tipo_agendamento 	= 'E';
		
			select	sum(decode(ie_tipo_agendamento,'C',1,0)),
				sum(decode(ie_tipo_agendamento,'E',1,0))
			into	qt_consulta_w,
				qt_exame_w
			from	agenda_integrada_item
			where	nr_seq_agenda_int	= nr_sequencia_w;
			
			if	(nr_seq_proc_interno_w = 6) then
				qt_consulta_w 		:= qt_consulta_w	- 1;
				qt_agendamento_w	:= qt_agendamento_w	- 1;
			end if;
			
		end if;		
		
	end if;
	
		
	insert into w_resumo_agendamento(
		dt_atualizacao,
		nm_usuario,
		cd_profissional,
		qt_agendamento,
		qt_confirmado,
		qt_cancelamento,
		qt_min_duracao,
		qt_exame,
		qt_consulta,
		dt_agendamento,
		qt_transferido)
	values(	sysdate,
		nm_usuario_p,
		cd_profissional_w,
		qt_agendamento_w,
		qt_confirmado_w,
		qt_cancelamento_w,
		qt_min_duracao_w,
		qt_exame_w,
		qt_consulta_w,
		trunc(dt_inicio_agendamento_w),
		qt_transferido_w);

end;
	
end loop;
close C01;


open C02;
loop
fetch C02 into	
	nr_seq_consulta_w,
	dt_confirmacao_w,
	cd_profissional_w;
exit when C02%notfound;
	begin
	qt_confirmado_w	:= 1;

	insert into w_resumo_agendamento(
		dt_atualizacao,
		nm_usuario,
		cd_profissional,
		qt_agendamento,
		qt_confirmado,
		qt_cancelamento,
		qt_min_duracao,
		qt_exame,
		qt_consulta,
		dt_agendamento,
		qt_transferido)
	values(	sysdate,
		nm_usuario_p,
		cd_profissional_w,
		0,
		qt_confirmado_w,
		0,
		0,
		0,
		0,
		trunc(dt_confirmacao_w),
		0);
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_exame_w,
	dt_confirmacao_w,
	cd_profissional_w;
exit when C03%notfound;
	begin
	qt_confirmado_w	:= 1;

	insert into w_resumo_agendamento(
		dt_atualizacao,
		nm_usuario,
		cd_profissional,
		qt_agendamento,
		qt_confirmado,
		qt_cancelamento,
		qt_min_duracao,
		qt_exame,
		qt_consulta,
		dt_agendamento,
		qt_transferido)
	values(	sysdate,
		nm_usuario_p,
		cd_profissional_w,
		0,
		qt_confirmado_w,
		0,
		0,
		0,
		0,
		trunc(dt_confirmacao_w),
		0);	
	end;
end loop;
close C03;


open C04;
loop
fetch C04 into	
	nr_seq_exame_w,
	dt_cancelamento_w,
	cd_profissional_w;
exit when C04%notfound;
	begin
	qt_cancelamento_w := 1;

	insert into w_resumo_agendamento(
		dt_atualizacao,
		nm_usuario,
		cd_profissional,
		qt_agendamento,
		qt_confirmado,
		qt_cancelamento,
		qt_min_duracao,
		qt_exame,
		qt_consulta,
		dt_agendamento,
		qt_transferido)
	values(	sysdate,
		nm_usuario_p,
		cd_profissional_w,
		0,
		0,
		qt_cancelamento_w,
		0,
		0,
		0,
		trunc(dt_cancelamento_w),
		0);	
	end;
end loop;
close C04;


open C05;
loop
fetch C05 into	
	nr_seq_exame_w,
	dt_cancelamento_w,
	cd_profissional_w;
exit when C05%notfound;
	begin
	qt_cancelamento_w := 1;

	insert into w_resumo_agendamento(
		dt_atualizacao,
		nm_usuario,
		cd_profissional,
		qt_agendamento,
		qt_confirmado,
		qt_cancelamento,
		qt_min_duracao,
		qt_exame,
		qt_consulta,
		dt_agendamento,
		qt_transferido)
	values(	sysdate,
		nm_usuario_p,
		cd_profissional_w,
		0,
		0,
		qt_cancelamento_w,
		0,
		0,
		0,
		trunc(dt_cancelamento_w),
		0);	
	end;
end loop;
close C05;


open C06;
loop
fetch C06 into	
	cd_profissional_w,
	nr_seq_transferido_w,
	dt_transferencia_w;
exit when C06%notfound;
	begin
	qt_transferido_W := 1;
	/*select	count(distinct a.nr_sequencia)
	into	qt_transferido_w
	from	agenda_integrada_item b,
		agenda_integrada a
	where	b.nr_seq_agenda_int = a.nr_sequencia
	and	b.dt_transferencia	between trunc(dt_inicio_p) and trunc(dt_final_p) + 83699/86400
	and	a.cd_profissional = cd_profissional_w;*/
		
	
	if	(qt_transferido_w > 0) then
	
		begin
		insert into w_resumo_agendamento(
		dt_atualizacao,
		nm_usuario,
		cd_profissional,
		qt_agendamento,
		qt_confirmado,
		qt_cancelamento,
		qt_min_duracao,
		qt_exame,
		qt_consulta,
		dt_agendamento,
		qt_transferido)
	values(	sysdate,
		nm_usuario_p,
		cd_profissional_w,
		0,
		0,
		0,
		0,
		0,
		0,
		trunc(dt_transferencia_w),
		qt_transferido_w);
		end;
	
	end if;
	
	end;
end loop;
close C06;

commit;

end Gerar_Resumo_Agendamento_Relat;
/