create or replace
procedure pls_atualizar_carteira_plano	(nr_atendimento_p		number,
					dt_inicio_vigencia_p		date,
					cd_convenio_p			number,
					cd_categoria_p			varchar2,
					ie_tipo_atendimento_p		number,
					cd_pessoa_fisica_p		varchar2,
					dt_atendimento_p		date,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number,
					carteiras_selec_p	out varchar2) is

ds_procedure_w		varchar2(255);
cd_usuario_plano_w	varchar2(255);
dt_validade_w		date;

begin

if	(nvl(cd_convenio_p,0) <> 0) then

	Obter_Regra_Valid_Usuario_Conv
			(cd_convenio_p,
			cd_categoria_p,
			ie_tipo_atendimento_p,
			ds_procedure_w);

	if	(upper(ds_procedure_w) = 'PLS_OBTER_USUARIO_PLANO') then

		pls_obter_usuario_plano
				(cd_pessoa_fisica_p,
				dt_atendimento_p,
				cd_estabelecimento_p,
				cd_usuario_plano_w,
				dt_validade_w);

		if	(nvl(cd_usuario_plano_w,'X') <> 'X') and
			(instr(cd_usuario_plano_w,',') = 0) then

			update	atend_categoria_convenio
			set	cd_usuario_convenio	= cd_usuario_plano_w,
				dt_validade_carteira	= dt_validade_w,
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_atendimento		= nr_atendimento_p
			and	cd_convenio		= cd_convenio_p
			and	cd_categoria		= cd_categoria_p
			and	dt_inicio_vigencia	= dt_inicio_vigencia_p;
			
			select	null
			into	carteiras_selec_p
			from dual;
			
		else
			
			select	cd_usuario_plano_w
			into	carteiras_selec_p
			from dual;
			
		end if;
	end if;
end if;

commit;

end pls_atualizar_carteira_plano;
/
