
create or replace
procedure dmed_mensal_adiant_sem_tit
			(	nr_seq_dmed_mensal_p	number,
				cd_estabelecimento_p	number,
				dt_referencia_p		date,
				ie_nota_fiscal_p	varchar2,
				ie_conta_paciente_p	varchar2,
				ie_cpf_p		varchar2,
				ie_idade_p		varchar2, 
				ie_estrangeiro_p	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	GERAR_DMED_MENSAL_PRESTADOR
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_responsavel_w		varchar2(10);
cd_pessoa_beneficiario_w	varchar2(10);
nr_documento_w			number(10);
vl_documento_w			number(15,2);
dt_documento_w			date;
contador_w			number(10);
idade_w				number(10);

cursor c01 is
	select  a.nr_adiantamento nr_documento,
		c.cd_pessoa_fisica cd_pessoa_responsavel,
		null cd_pessoa_beneficiario,
		a.vl_saldo vl_documento,
		a.dt_contabil dt_documento
	from 	adiantamento	a,
		pessoa_fisica	c
	where 	a.cd_pessoa_fisica	= c.cd_pessoa_fisica
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.vl_saldo > 0
	and	(((ie_cpf_p = 'SC') and ( c.nr_cpf is null)) or
		(( ie_cpf_p = 'CC') and ( c.nr_cpf is not null)) or
		(ie_cpf_p = 'AM'))
	and	exists	(select	1
			from	dmed_regra_adiantamento	w
			where	w.cd_tipo_recebimento	= a.cd_tipo_recebimento)
	and	to_char(a.dt_contabil,'mm/yyyy')	= to_char(dt_referencia_p, 'mm/yyyy');

begin

select 	nvl(max(nr_idade),16) 
into 	idade_w
from 	dmed_regra_geral;

open c01;
loop
fetch C01 into
	nr_documento_w,
	cd_pessoa_responsavel_w,
	cd_pessoa_beneficiario_w,
	vl_documento_w,
	dt_documento_w;
exit when c01%notfound;
	begin
	contador_w	:= contador_w + 1;
	
	if	(cd_pessoa_beneficiario_w is null) then
		cd_pessoa_beneficiario_w	:= cd_pessoa_responsavel_w;
	end if;	
	
	insert into dmed_titulos_mensal
		(nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		nr_seq_dmed_mensal, 
		nr_documento, 
		ie_tipo_documento, 
		cd_pessoa_titular, 
		cd_pessoa_beneficiario, 
		vl_pago, 
		dt_liquidacao,
		ie_prestadora_ops)
	values	(dmed_titulos_mensal_seq.NextVal,
		sysdate,
		'Tasy',
		sysdate,
		'Tasy',
		nr_seq_dmed_mensal_p,
		nr_documento_w,
		'AS',
		cd_pessoa_responsavel_w,
		cd_pessoa_beneficiario_w,
		vl_documento_w,
		dt_documento_w,
		'P');
		
	if	(mod(contador_w,100) = 0) then
		commit;
	end if;
	end;
end loop;
close c01;

commit;

end dmed_mensal_adiant_sem_tit;
/
