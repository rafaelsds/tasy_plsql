create or replace
procedure desfazer_liberacao_img_ge(	nr_prescricao_p		Number,
					nr_sequencia_p		Number,
					nm_usuario_p		Varchar2) is 

begin

update prescr_procedimento 
set dt_lib_imagem = null 
where nr_prescricao = nr_prescricao_p 
and   nr_sequencia = nr_sequencia_p; 

commit;

end desfazer_liberacao_img_ge;
/