create or replace
procedure pls_inativar_usuario_eventual
			(	nr_seq_segurado_p	Number,
				nr_seq_motivo_p		Number,
				dt_rescisao_p		Date,
				ds_observacao_p		Varchar2,
				nm_usuario_p		Varchar2,
				dt_limite_utilizacao_p	Date) is 

ie_situacao_atend_w	varchar2(1);

begin

if	(dt_limite_utilizacao_p > sysdate) then
	ie_situacao_atend_w	:= 'A';
elsif	(dt_limite_utilizacao_p <= sysdate) then
	ie_situacao_atend_w	:= 'I';
end if;

update	pls_segurado
set	dt_rescisao			= dt_rescisao_p,
	nm_usuario			= nm_usuario_p,
	dt_atualizacao			= sysdate,
	nr_seq_motivo_cancelamento 	= nr_seq_motivo_p,
	dt_limite_utilizacao		= dt_limite_utilizacao_p,
	ie_situacao_atend		= ie_situacao_atend_w
where	nr_sequencia	= nr_seq_segurado_p;
		
pls_gerar_segurado_historico(	nr_seq_segurado_p, '45', dt_rescisao_p, 'Rescis�o',
				ds_observacao_p, null, null, null,
				nr_seq_motivo_p, dt_rescisao_p, null, null,
				null, null, null, null,
				nm_usuario_p, 'N');

commit;

end pls_inativar_usuario_eventual;
/
