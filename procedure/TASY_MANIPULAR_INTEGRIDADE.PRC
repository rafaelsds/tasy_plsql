CREATE OR REPLACE
PROCEDURE Tasy_Manipular_Integridade 	(
				nm_tabela_p     	Varchar2,
				ds_acao_p		Varchar2) is


nm_tabela_w			Varchar2(50);
nm_integridade_w		Varchar2(50);
ds_comando_w			Varchar2(2000);

CURSOR c01 is
	select nm_tabela, nm_integridade_referencial
	from integridade_referencial
	where nm_tabela_referencia	= nm_tabela_p;


BEGIN


OPEN C01;
LOOP
FETCH C01 into
	nm_tabela_w,
	nm_integridade_w;
exit when c01%notfound;
	begin

	ds_comando_w	:= 'Alter table ' || nm_tabela_W || ' ' || ds_acao_p ||
			' constraint ' || nm_integridade_w;
	Exec_Sql_Dinamico(nm_integridade_w, ds_comando_w);
	end;
END LOOP;
CLOSE c01;


END Tasy_Manipular_Integridade;
/