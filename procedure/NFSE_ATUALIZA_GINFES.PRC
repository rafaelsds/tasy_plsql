create or replace
procedure nfse_atualiza_ginfes(	nr_nfe_imp_p		varchar2,
				cd_verificacao_nfse_p	varchar2,
				dt_emissao_nfe_p	varchar2,
				cd_serie_nf_p		varchar2,
				nr_nota_fiscal_p	varchar2,
				dt_emissao_rps_p	varchar2) is

dt_emissao_nfe_w	date;
ie_situacao_w		varchar2(1);
dt_emissao_rps_w	date;

begin

if (dt_emissao_rps_p <> 'X') then
	dt_emissao_rps_w := to_date(dt_emissao_rps_p, 'yyyy-mm-dd');
end if;

select  ie_situacao
into	ie_situacao_w
from 	nota_fiscal
where 	nr_nota_fiscal  	= nr_nota_fiscal_p
and	cd_serie_nf	= cd_serie_nf_p
and 	ie_tipo_nota in ('SE','SD','SF','ST')
and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'))
and	ie_situacao <> '2';

if (dt_emissao_nfe_p is not null) then
	begin
	dt_emissao_nfe_w := replace(dt_emissao_nfe_p, 'T', ' ');

	exception when others then
		begin
		dt_emissao_nfe_w		:= to_date(replace(dt_emissao_nfe_p, 'T', ' '), 'yyyy-mm-dd hh24:mi:ss');
		exception when others then
			begin
			dt_emissao_nfe_w	:= to_date(replace(dt_emissao_nfe_p, 'T', ' '), 'dd-mm-yyyy hh24:mi:ss');
			exception when others then
			--N�o foi poss�vel converter a data de emiss�o enviada no arquivo:
				Wheb_mensagem_pck.exibir_mensagem_abort(186114,'DT_EMISSAO_NFE=' || dt_emissao_nfe_p); 
			end;
		end;


	end;
end if;


if	(ie_situacao_w <> '8') then
	update	nota_fiscal
	set 	nr_nfe_imp		=  nr_nfe_imp_p,
		cd_verificacao_nfse 		= cd_verificacao_nfse_p,
		dt_emissao_nfe			= dt_emissao_nfe_w
	where	nr_nota_fiscal		= nr_nota_fiscal_p
	and	cd_serie_nf		= cd_serie_nf_p
	and 	ie_tipo_nota in ('SE','SD','SF','ST')
	and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'))
	and	ie_situacao in ('1','3');
else
	update	nota_fiscal
	set 	nr_nfe_imp		= nr_nfe_imp_p,
		cd_verificacao_nfse 	= cd_verificacao_nfse_p,
		dt_emissao_nfe		= dt_emissao_nfe_w,
		dt_atualizacao_estoque	= sysdate,
		ie_situacao		= '1'
	where	nr_nota_fiscal		= nr_nota_fiscal_p
	and	cd_serie_nf		= cd_serie_nf_p
	and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'))
	and 	ie_tipo_nota in ('SE','SD','SF','ST');

end if;

commit;

end nfse_atualiza_ginfes;
/
