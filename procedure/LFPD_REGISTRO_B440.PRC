create or replace
procedure lfpd_registro_B440(	nr_seq_controle_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				ds_separador_p		varchar2,
				qt_linha_p		in out number,
				nr_sequencia_p		in out number) is

contador_w		number(10)	:= 0;
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10)	:= qt_linha_p;
nr_seq_registro_w		number(10)	:= nr_sequencia_p;
sep_w			varchar2(1)	:= ds_separador_p;

cd_cgc_w		varchar2(14);
cd_tributo_iss_w		number(3);
nr_seq_regra_w		number(10);
nr_seq_regra_tributo_w	number(10);
pr_imposto_w		number(7,4);
vl_base_calculo_w	number(13,2);

cursor c01 is
	select	'B440'													cd_registro,
		'0'									 				ie_operacao,
		nvl(n.cd_pessoa_fisica, n.cd_cgc)									cd_participante,
		replace(Campo_Mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),2),'.',',')	vl_contabil_nota,
		(replace(Campo_Mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),2),'.',',')
		- replace(Campo_Mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'D', 'ISS')),2),'.',','))	vl_base_calculo,
		replace(Campo_Mascara(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ISS'),2),'.',',')		pr_aliquota_iss,
		replace(Campo_Mascara(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),2),'.',',')	vl_retido
	from	nota_fiscal n,
		operacao_nota o
	where	n.cd_operacao_nf = o.cd_operacao_nf
	and 	(nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'E')
	and 	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and 	o.ie_servico = 'S'
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	exists (select	1
			from	nota_fiscal_item x
			where	x.nr_sequencia = n.nr_sequencia
			and 	x.cd_material is not null
			and	rownum = 1
			)
	group by 	nvl(n.cd_pessoa_fisica, n.cd_cgc),
		replace(Campo_Mascara(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'X', 'ISS'),2),'.',',')
	union
	select	'B440'													cd_registro,
		'1'									 				ie_operacao,
		nvl(n.cd_pessoa_fisica, n.cd_cgc)									cd_participante,
		replace(Campo_Mascara(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'B', 'ISS')),2),'.',',')		vl_contabil_nota,
		(replace(Campo_Mascara(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'B', 'ISS')),2),'.',',')
		- replace(Campo_Mascara(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'D', 'ISS')),2),'.',','))	vl_base_calculo,
		replace(Campo_Mascara(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'X', 'ISS'),2),'.',',')		pr_aliquota_iss,
		replace(Campo_Mascara(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'V', 'ISS')),2),'.',',')		vl_retido
	from	nota_fiscal n,
		operacao_nota o
	where	n.cd_operacao_nf = o.cd_operacao_nf
	and 	(nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S')
	and 	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and 	o.ie_servico = 'S'
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	exists (select	1
			from	nota_fiscal_item x
			where	x.nr_sequencia = n.nr_sequencia
			and 	x.cd_procedimento is not null
			and	rownum = 1
			)
	group by 	nvl(n.cd_pessoa_fisica, n.cd_cgc),
		replace(Campo_Mascara(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'X', 'ISS'),2),'.',',');

vet01	c01%RowType;

begin

select	nr_seq_regra_lfpd
into	nr_seq_regra_w
from	fis_lfpd_controle
where	nr_sequencia = nr_seq_controle_p;

select	cd_tributo_iss
into	cd_tributo_iss_w
from	fis_lfpd_regra
where	nr_sequencia = nr_seq_regra_w;

select	max(nr_sequencia)
into	nr_seq_regra_tributo_w
from	regra_calculo_imposto
where	cd_tributo = cd_tributo_iss_w;

select	pr_imposto
into	pr_imposto_w
from	regra_calculo_imposto
where	nr_sequencia = nr_seq_regra_tributo_w;

open C01;
loop
fetch C01 into
	vet01;
exit when C01%notfound;
	begin

	contador_w := contador_w + 1;
	if(vet01.vl_retido > 0 and vet01.pr_aliquota_iss > 0) then
		vl_base_calculo_w := to_number(vet01.vl_base_calculo);
	else
		vl_base_calculo_w := to_number('0,00');
	end if;

	ds_linha_w := substr(	sep_w	||	vet01.cd_registro		|| sep_w ||
						vet01.ie_operacao		|| sep_w ||
						vet01.cd_participante	|| sep_w ||
						vet01.vl_contabil_nota	|| sep_w ||
						replace(Campo_Mascara(vl_base_calculo_w,2),'.',',')	|| sep_w ||
						vet01.pr_aliquota_iss	|| sep_w ||
						vet01.vl_retido		|| sep_w, 1, 8000);

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_lfpd_arquivo	(	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_linha,
						ds_arquivo,
						ds_arquivo_compl,
						cd_registro,
						nr_seq_controle_lfpd)
				values	(	fis_lfpd_arquivo_seq.NextVal,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_linha_w,
						ds_arquivo_w,
						ds_arquivo_compl_w,
						vet01.cd_registro,
						nr_seq_controle_p);

	if (mod(contador_w,100) = 0) then
		commit;
	end if;

	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end lfpd_registro_B440;
/
