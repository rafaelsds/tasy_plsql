create or replace procedure UPDATE_AI_SYNONYM_MIMS
is
nr_mims_version		NUMBER := 0;
ds_sinonimo_w	MEDIC_FICHA_TECNICA.DS_SINONIMO%type;
nm_usuario_w	MEDIC_FICHA_TECNICA.NM_USUARIO_NREC%type := 'MIMSUPLOAD';
nr_outer_count_w	NUMBER := 1;
nr_inner_count_w	NUMBER := 0;

CURSOR C01 IS 
SELECT * FROM MEDIC_FICHA_TECNICA;

CURSOR C02(p_ds_substancia VARCHAR2)  IS
SELECT A.GENERIC ds_synonym_w 
	FROM GENDAT A, GENDAT B 
	WHERE B.GENERIC = p_ds_substancia
	AND A.APPROVED = B.GENCODE;

BEGIN

    SELECT max(version) 
	into nr_mims_version
	from GENDAT;

	nr_outer_count_w := 1;

	FOR r01 IN c01 LOOP
		begin
			nr_inner_count_w := 0;
			ds_sinonimo_w := 'NA';

			FOR r02 IN c02(r01.DS_SUBSTANCIA) loop --GENDAT
				if (nr_inner_count_w = 0) then
					ds_sinonimo_w := r02.ds_synonym_w;
				else
					ds_sinonimo_w := substr(ds_sinonimo_w||', '||r02.ds_synonym_w, 1, 255);
				end if;
				nr_inner_count_w := nr_inner_count_w + 1;
			end loop;

			if(ds_sinonimo_w != 'NA' AND NVL(r01.ds_sinonimo,'NA') != ds_sinonimo_w) then
				UPDATE MEDIC_FICHA_TECNICA 
				SET DS_SINONIMO = ds_sinonimo_w,
					DT_ATUALIZACAO_NREC = sysdate,
					NM_USUARIO_NREC = nm_usuario_w
				WHERE NR_SEQUENCIA = r01.NR_SEQUENCIA;

				nr_outer_count_w := nr_outer_count_w + 1;
			end if;

			if(mod(nr_outer_count_w,100)=0)then
				commit;
			end if;

		exception when others then
			null;
		end;
	end loop;
	commit;
END UPDATE_AI_SYNONYM_MIMS;
/
