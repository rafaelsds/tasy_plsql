create or replace
procedure definir_itens_marca(	nr_sequencia_p			number,
				cd_estabelecimento_p		number,
				cd_grupo_material_p		number,
				cd_subgrupo_material_p		number,
				cd_classe_material_p		number,
				nr_seq_familia_p			number,
				nm_usuario_p			varchar2) is

cd_material_w			number(6);


cursor c01 is
select	distinct(a.cd_material)
from	estrutura_material_v a
where	a.cd_classe_material	= nvl(cd_classe_material_p, a.cd_classe_material)
and	a.cd_grupo_material	= nvl(cd_grupo_material_p, a.cd_grupo_material)
and	a.cd_subgrupo_material	= nvl(cd_subgrupo_material_p, a.cd_subgrupo_material)
and	(a.nr_seq_familia		= nr_seq_familia_p or nvl(nr_seq_familia_p, 0) = 0)
and	a.ie_situacao		= 'A'
and not exists(
	select	1
	from	material_marca b
	where	b.nr_sequencia	= nr_sequencia_p
	and	a.cd_material	= b.cd_material);

begin

open c01;
loop
fetch c01 into
	cd_material_w;
exit when c01%notfound;
	begin
	
	insert into material_marca(
		nr_sequencia,
		cd_material,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		qt_prioridade,
		ie_situacao,
		cd_estabelecimento)
	values(	nr_sequencia_p,
		cd_material_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		10,
		'A',
		cd_estabelecimento_p);	
	end;
end loop;
close C01;	

end definir_itens_marca;
/