create or replace
procedure atualiza_motivo_conv_ret_glosa(	cd_motivo_glosa_p	number,
						cd_setor_responsavel_p	number,
						cd_resposta_p		number,
						nr_sequencia_p		number,
						nm_usuario_p		varchar2,
						ie_acao_glosa_p		varchar2) is 

ie_acao_glosa_w		varchar2(10);
cd_setor_responsavel_w	number(5);
cd_resposta_w		number(10);

begin

if	(nr_sequencia_p is not null) then

begin

	if	(cd_setor_responsavel_p is null) then
		select	cd_setor_responsavel
		into	cd_setor_responsavel_w
		from	convenio_retorno_glosa
		where	nr_sequencia		= nr_sequencia_p;
	else
		cd_setor_responsavel_w	:= cd_setor_responsavel_p;
	end if;
	
	if	(cd_resposta_p is null) then
		select	cd_resposta
		into	cd_resposta_w
		from	convenio_retorno_glosa
		where	nr_sequencia		= nr_sequencia_p;
	else
		cd_resposta_w	:= cd_resposta_p;
	end if;
	
	if	(nvl(ie_acao_glosa_p,'N') = 'S') then
		select	ie_acao_glosa
		into	ie_acao_glosa_w
		from	motivo_glosa
		where	cd_motivo_glosa		= cd_motivo_glosa_p;
	end if;
	
	update	convenio_retorno_glosa 
	set	cd_motivo_glosa		= cd_motivo_glosa_p,
		nm_usuario		= nm_usuario_p,
		cd_setor_responsavel	= cd_setor_responsavel_w,
		cd_resposta		= cd_resposta_w,
		dt_atualizacao		= sysdate,
		ie_acao_glosa		= nvl(ie_acao_glosa_w,ie_acao_glosa)
	where	nr_sequencia		= nr_sequencia_p;

	commit;

end;

end if;

end atualiza_motivo_conv_ret_glosa;
/