create or replace
procedure Consistir_qt_aprazamento(	cd_perfil_p			number,
									cd_item_p			number,
									nr_prescricao_p		number,
									nr_seq_item_p		number) is 

qt_w		number(10);
qt_maxima_w	number(10);
ie_existe_regra_w	varchar2(1 char);
			
begin

select	nvl(max(qt_maxima),0),
		nvl(max('S'), 'N')
into	qt_maxima_w,
		ie_existe_regra_w
from	regra_quant_aprazamento
where	((cd_perfil is null) or (cd_perfil = cd_perfil_p))
and		cd_material	= cd_item_p;

if (ie_existe_regra_w = 'S') then
	select	count(*)
	into	qt_w
	from	prescr_mat_hor
	where	nr_prescricao	= nr_prescricao_p
	and		nr_seq_material	= nr_seq_item_p
	and		nvl(ie_horario_especial,'N') = 'N';
	
	if	(qt_w >= qt_maxima_w) then
		Wheb_mensagem_pck.exibir_mensagem_abort(218875,'QTD='||to_char(qt_maxima_w));
	end if;
end if;

end Consistir_qt_aprazamento;
/
