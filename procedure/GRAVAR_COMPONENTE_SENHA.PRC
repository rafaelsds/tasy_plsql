create or replace
PROCEDURE gravar_componente_senha(	nm_componente_p		VARCHAR2,					
					qt_esquerda_p		NUMBER,
					qt_topo_p		NUMBER,
					qt_altura_p		NUMBER,
					qt_largura_p		NUMBER,
					ds_fonte_p		VARCHAR2,
					ie_italico_p		VARCHAR2,
					ie_negrito_p		VARCHAR2,
					ie_sublinhado_p		VARCHAR2,
					ie_riscado_p		VARCHAR2,
					ie_centralizar_p		VARCHAR2,
					ds_cor_p			VARCHAR2,
					qt_tamanho_fonte_p	NUMBER,
					nm_atributo_p		varchar2,
					ie_componente_adicional_p	varchar2 default 'N',
					ie_quebra_linha_p		varchar2 default 'N',
					nm_usu_salvar_p		VARCHAR2 DEFAULT 'X',
					nm_maquina_p		VARCHAR2 DEFAULT 'X',
					cd_estab_salvar_p		NUMBER DEFAULT 0,
					cd_perfil_salvar_p		NUMBER DEFAULT 0,
					ie_tipo_componente_p	NUMBER DEFAULT 0,
					ie_tipo_informacao_p	NUMBER DEFAULT 0) IS 
					
nm_componente_w		VARCHAR2(100);
qt_esquerda_w		NUMBER(5);
qt_topo_w		NUMBER(5);
qt_altura_w		NUMBER(5);
qt_largura_w		NUMBER(5);
ds_fonte_w		VARCHAR2(20);
ie_italico_w		VARCHAR2(1);
ie_negrito_w		VARCHAR2(1);
ie_sublinhado_w		VARCHAR2(1);
ie_riscado_w		VARCHAR2(1);
ie_centralizar_w		VARCHAR2(1);
ds_cor_w			VARCHAR2(20);
qt_tamanho_fonte_w	NUMBER(4);

BEGIN

SELECT	MAX(nm_componente)
INTO	nm_componente_w
FROM	monitor_senha_custom
WHERE	NVL(nm_usuario, 'X') = NVL(nm_usu_salvar_p, 'X')
AND	NVL(nm_maquina, 'X') = NVL(nm_maquina_p, 'X')
AND	NVL(cd_perfil, 0) = NVL(cd_perfil_salvar_p, 0)
AND	NVL(cd_estabelecimento, 0) = NVL(cd_estab_salvar_p, 0)
AND	nm_componente = nm_componente_p;

IF	(nm_componente_w IS NOT NULL) THEN

	UPDATE	monitor_senha_custom
	SET	qt_esquerda = qt_esquerda_p,
		qt_topo = qt_topo_p,
		qt_altura = qt_altura_p,
		qt_largura = qt_largura_p,
		ds_fonte = ds_fonte_p,
		ie_italico = ie_italico_p,
		ie_negrito = ie_negrito_p,
		ie_sublinhado = ie_sublinhado_p,
		ie_riscado = ie_riscado_p,
		ie_centralizar = ie_centralizar_p,
		ds_cor = ds_cor_p,
		qt_tamanho_fonte = qt_tamanho_fonte_p,
		ie_componente_adicional = ie_componente_adicional_p,
		nm_atributo = nm_atributo_p,
		ie_quebra_linha = ie_quebra_linha_p,
		ie_tipo_informacao = ie_tipo_informacao_p
	WHERE	NVL(nm_usuario, 'X') = NVL(nm_usu_salvar_p, 'X')
	AND	NVL(nm_maquina, 'X') = NVL(nm_maquina_p, 'X')
	AND	NVL(cd_perfil, 0) = NVL(cd_perfil_salvar_p, 0)
	AND	NVL(cd_estabelecimento, 0) = NVL(cd_estab_salvar_p, 0)
	AND	nm_componente = nm_componente_p;
ELSE
	INSERT	INTO	monitor_senha_custom
		(nm_componente,
		nm_usuario,
		nm_maquina,
		cd_perfil,
		cd_estabelecimento,
		qt_esquerda,
		qt_topo,
		qt_altura,
		qt_largura,
		ds_fonte,
		ie_italico,
		ie_negrito,
		ie_sublinhado,
		ie_riscado,
		ie_centralizar,
		ds_cor,
		qt_tamanho_fonte,
		ie_componente_adicional,
		nm_atributo,
		ie_quebra_linha,
		ie_tipo_componente,
		ie_tipo_informacao)
	VALUES	(nm_componente_p,
		nm_usu_salvar_p,
		nm_maquina_p,
		cd_perfil_salvar_p,
		cd_estab_salvar_p,
		qt_esquerda_p,
		qt_topo_p,
		qt_altura_p,
		qt_largura_p,
		ds_fonte_p,
		ie_italico_p,
		ie_negrito_p,
		ie_sublinhado_p,
		ie_riscado_p,
		ie_centralizar_p,
		ds_cor_p,
		qt_tamanho_fonte_p,
		ie_componente_adicional_p,
		nm_atributo_p,
		ie_quebra_linha_p,
		ie_tipo_componente_p,
		ie_tipo_informacao_p);
END IF;

COMMIT;

END gravar_componente_senha;
/