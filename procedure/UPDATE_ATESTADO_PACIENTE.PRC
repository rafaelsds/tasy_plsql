create or replace procedure update_atestado_paciente(
			nr_sequencia_p		number,
			ds_atestado_p		long) is 

begin


update	atestado_paciente
set    	ds_atestado		= ds_atestado_p 
where  	nr_sequencia      	= nr_sequencia_p;
commit;

end update_atestado_paciente; 
/