create or replace
procedure qt_consistir_agendamento(dt_agenda_p		date,
				   cd_estabelecimento_p	number,
				   cd_pessoa_fisica_p	varchar2,
				   nm_usuario_p		Varchar2,
				   ie_permite_p		out varchar2) is 

cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);			   
hr_inicio_w		date;
cd_area_proced_w	number(15,0);
cd_espec_proced_w	number(15,0);
cd_grupo_proced_w	number(15,0);
nr_seq_regra_w		number(10,0);
qt_tempo_w		number(10,0);
ie_permite_w		varchar2(1);
				   				   
Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		hr_inicio
	from	agenda_paciente
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400
	AND	ie_status_agenda NOT IN ('B','C','L');
	
Cursor C02 is
	select	nvl(nr_sequencia,0)
	from	qt_tempo_entre_exame 
	where	((cd_area_procedimento = cd_area_proced_w) or (cd_area_procedimento is null))
	and	((cd_especialidade = cd_espec_proced_w) or (cd_especialidade is null))
	and	((cd_grupo_proc = cd_grupo_proced_w) or (cd_grupo_proc is null))
	and	((cd_procedimento = cd_procedimento_w) or (cd_procedimento is null))
	and	((cd_procedimento is null) or ((ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null)))
	and	((nr_seq_proc_interno = nr_seq_proc_interno_w) or (nr_seq_proc_interno is null))
	order by nvl(cd_procedimento,0), 
		nvl(nr_seq_proc_interno,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0);
		
Cursor C03 is
	select	b.cd_procedimento,
		b.ie_origem_proced,
		b.nr_seq_proc_interno,
		a.dt_agenda
	from	agenda_consulta a,
		agenda_consulta_proc b
	where	a.nr_sequencia = b.nr_seq_agenda
	and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.dt_agenda between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 86399/86400
	AND	ie_status_agenda NOT IN ('B','C','L');
				   
begin
ie_permite_w	:= 'S';

open C01;
loop
fetch C01 into	
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_proc_interno_w,
	hr_inicio_w;
exit when C01%notfound;
	begin
	
	select	nvl(max(cd_area_procedimento),0),
		nvl(max(cd_especialidade),0),
		nvl(max(cd_grupo_proc),0)
	into	cd_area_proced_w,
		cd_espec_proced_w,
		cd_grupo_proced_w
	from	estrutura_procedimento_v
	where	cd_procedimento = cd_procedimento_w
	and	ie_origem_proced = ie_origem_proced_w;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_regra_w;
	exit when C02%notfound;
		begin
		nr_seq_regra_w := nr_seq_regra_w;
		end;
	end loop;
	close C02;
	
	if	(nr_seq_regra_w > 0) then
		select	max(qt_tempo_entre_exame)
		into	qt_tempo_w
		from	qt_tempo_entre_exame
		where	nr_sequencia  = nr_seq_regra_w;
		if	(qt_tempo_w > 0) then
			qt_tempo_w := qt_tempo_w - 1;
			if	((dt_agenda_p < (hr_inicio_w - qt_tempo_w/1440)) or (dt_agenda_p > (hr_inicio_w + qt_tempo_w/1440))) then
				ie_permite_w := 'S';
			else
				ie_permite_w := 'N';
			end if;
		else
			ie_permite_w := 'S';
		end if;	
	else
		ie_permite_w	:= 'S';
	end if;
		
	end;
end loop;
close C01;

if	(ie_permite_w = 'S') then
	open C03;
	loop
	fetch C03 into	
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		hr_inicio_w;
	exit when C03%notfound;
		begin
		
		select	nvl(max(cd_area_procedimento),0),
			nvl(max(cd_especialidade),0),
			nvl(max(cd_grupo_proc),0)
		into	cd_area_proced_w,
			cd_espec_proced_w,
			cd_grupo_proced_w
		from	estrutura_procedimento_v
		where	cd_procedimento = cd_procedimento_w
		and	ie_origem_proced = ie_origem_proced_w;
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_regra_w;
		exit when C02%notfound;
			begin
			nr_seq_regra_w := nr_seq_regra_w;
			end;
		end loop;
		close C02;
		
		if	(nr_seq_regra_w > 0) then
			select	max(qt_tempo_entre_exame)
			into	qt_tempo_w
			from	qt_tempo_entre_exame
			where	nr_sequencia  = nr_seq_regra_w;
			if	(qt_tempo_w > 0) then
				qt_tempo_w := qt_tempo_w - 1;
				if	((dt_agenda_p < (hr_inicio_w - qt_tempo_w/1440)) or (dt_agenda_p > (hr_inicio_w + qt_tempo_w/1440))) then
					ie_permite_w := 'S';
				else
					ie_permite_w := 'N';
				end if;
			else
				ie_permite_w := 'S';
			end if;	
		else
			ie_permite_w	:= 'S';
		end if;
			
		end;
	end loop;
	close C03;
end if;

ie_permite_p	:= ie_permite_w;

end qt_consistir_agendamento;
/

