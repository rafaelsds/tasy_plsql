create or replace
procedure gravar_usuario_alt_regulacao(nr_sequencia_p	number,
					nm_usuario_p	varchar2) is 

begin

if (nr_sequencia_p > 0) then
	if	(nm_usuario_p is null) or
		(nm_usuario_p = '') then
		update 	eme_regulacao
		set		nm_usuario_alt	= nm_usuario_p,
				dt_bloqueio	= null
		where	nr_sequencia	= nr_sequencia_p;
	else
		update 	eme_regulacao
		set		nm_usuario_alt	= nm_usuario_p,
				dt_bloqueio	= sysdate
		where	nr_sequencia	= nr_sequencia_p;
	end if;	
	
end if;

commit;

end gravar_usuario_alt_regulacao;
/