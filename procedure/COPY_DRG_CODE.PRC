CREATE OR REPLACE
PROCEDURE copy_drg_code (nr_atendimento_from_p	atendimento_paciente.nr_atendimento%type,
			 nr_atendimento_to_p	atendimento_paciente.nr_atendimento%type,
			 cd_convenio_p		convenio.cd_convenio%type,
			 nm_usuario_p    	varchar2) is

nr_sequencia_w			episodio_paciente_drg.nr_sequencia%type;
nr_seq_drg_proc_w 		episodio_paciente_drg.nr_seq_drg_proc%type;
ie_tipo_drg_w 			episodio_paciente_drg.ie_tipo_drg%type;
vl_base_w 			episodio_paciente_drg.vl_base%type;
tx_departamento_w 		episodio_paciente_drg.tx_departamento%type;
nr_seq_drg_w 			episodio_paciente_drg.nr_seq_drg%type;
tx_ajuste_w 			episodio_paciente_drg.tx_ajuste%type;
nr_seq_episodio_paciente_w 	episodio_paciente_drg.nr_seq_episodio_paciente%type;
vl_liquido_w 			episodio_paciente_drg.vl_liquido%type;
vl_desconto_w 			episodio_paciente_drg.vl_desconto%type;
vl_adicional_dia_w 		episodio_paciente_drg.vl_adicional_dia%type;
vl_adicional_w 			episodio_paciente_drg.vl_adicional%type;
vl_desconto_dia_w 		episodio_paciente_drg.vl_desconto_dia%type;
qt_dias_excedido_w 		episodio_paciente_drg.qt_dias_excedido%type;
vl_taxa_w 			episodio_paciente_drg.vl_taxa%type;
qt_dias_abaixo_w 		episodio_paciente_drg.qt_dias_abaixo%type;
qt_estadia_min_w 		episodio_paciente_drg.qt_estadia_min%type;
qt_estadia_max_w 		episodio_paciente_drg.qt_estadia_max%type;
qt_estadia_drg_w 		episodio_paciente_drg.qt_estadia_drg%type;
ie_situacao_w 			episodio_paciente_drg.ie_situacao%type;
nr_seq_propaci_w 		episodio_paciente_drg.nr_seq_propaci%type;
episodio_paciente_drg_seq_w	episodio_paciente_drg.nr_sequencia%type;

nr_seq_drg_ww 			drg_procedimento.nr_sequencia%type;
nr_seq_edition_w		drg_procedimento.nr_seq_edition%type;
nr_conv_sequencia_w		health_fund_drg.nr_sequencia%type;
cd_medico_resp_w		atendimento_paciente.cd_medico_resp%type;
dt_diagnostico_w  		diagnostico_doenca.dt_diagnostico%type := null;
ie_tipo_diagnostico_w 		number;
qt_doctor_diagnosis_w		number;

 cursor	c01 is
	select  nr_seq_propaci
	from    episodio_paciente_drg
	where   nr_atendimento = nr_atendimento_to_p
	and     ie_situacao = 'A';

cursor c02 is
	select	ie_lado,
		cd_doenca,
		ie_classificacao_doenca,
		dt_diagnostico,
		ie_tipo_diagnostico,
		dt_liberacao,
		dt_inativacao,
		dt_atualizacao,
		nm_usuario_inativacao,
		ds_justificativa,
		cd_doenca_superior,
		nm_usuario,
		ie_tipo_doenca,
		nr_seq_classif_adic,
		dt_cid,
		dt_inicio,
		dt_fim,
		dt_manifestacao,
		qt_tempo,
		ie_unidade_tempo,
		nr_atendimento
	from   	diagnostico_doenca dd
	where  	dd.nr_atendimento = nr_atendimento_from_p
	and    dd.ie_situacao = 'A';

cursor c03 is
	select	nr_sequencia,
		cd_procedimento,
		qt_procedimento,
		ie_lado,
		dt_procedimento,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		ie_origem_proced,
		cd_setor_atendimento,
		ie_proc_princ,
		ie_proc_adicional,
		dt_liberacao,
		nm_usuario_inativacao,
		dt_inativacao,
		ds_justificativa,
		cd_departamento,
		nr_atendimento
	from   	procedimento_pac_medico
	where  	nr_atendimento = nr_atendimento_from_p
	and    	ie_situacao = 'A';

begin

select 	nr_sequencia
into	nr_sequencia_w
from 	episodio_paciente_drg
where 	nr_atendimento = nr_atendimento_from_p
and 	ie_situacao = 'A';

if	(nr_sequencia_w is not null) then
	begin

	if	(nr_atendimento_from_p = nr_atendimento_to_p) then
		Wheb_Mensagem_Pck.Exibir_Mensagem_Abort(1091029);
	end if;

	select 	nr_seq_drg_proc,
		ie_tipo_drg,
		vl_base,
		tx_departamento,
		nr_seq_drg,
		tx_ajuste,
		nr_seq_episodio_paciente,
		vl_liquido,
		vl_desconto,
		vl_adicional_dia,
		vl_adicional,
		vl_desconto_dia,
		qt_dias_excedido,
		vl_taxa,
		qt_dias_abaixo,
		qt_estadia_min,
		qt_estadia_max,
		qt_estadia_drg,
		ie_situacao,
		nr_seq_propaci
	into	nr_seq_drg_proc_w,
		ie_tipo_drg_w,
		vl_base_w,
		tx_departamento_w,
		nr_seq_drg_w,
		tx_ajuste_w,
		nr_seq_episodio_paciente_w,
		vl_liquido_w,
		vl_desconto_w,
		vl_adicional_dia_w,
		vl_adicional_w,
		vl_desconto_dia_w,
		qt_dias_excedido_w,
		vl_taxa_w,
		qt_dias_abaixo_w,
		qt_estadia_min_w,
		qt_estadia_max_w,
		qt_estadia_drg_w,
		ie_situacao_w,
		nr_seq_propaci_w
	from 	episodio_paciente_drg
	where 	nr_sequencia = nr_sequencia_w;

	select  max(b.nr_sequencia)
	into    nr_conv_sequencia_w
	from    convenio a,
		health_fund_drg b
	where   a.cd_convenio = b.cd_health_fund
	and     a.cd_convenio = cd_convenio_p
	and     sysdate between b.dt_start_validity and nvl(b.dt_end_validity,sysdate);

	select  max(nr_seq_edition)
	into    nr_seq_edition_w
	FROM    health_fund_drg
	WHERE   nr_sequencia = nr_conv_sequencia_w;

	select  max(b.nr_sequencia)
	into    nr_seq_drg_ww
	from    edition_drg a,
		drg_procedimento b
	where   b.nr_seq_edition = a.nr_sequencia
	and     nr_seq_edition = nr_seq_edition_w
	and     b.cd_drg = OBTER_DADOS_DRG(nr_seq_drg_proc_w,'C');

	if	(nr_seq_drg_ww is null) then
		Wheb_Mensagem_Pck.Exibir_Mensagem_Abort(1075947);
	end if;

	for r7 in C01 loop
		begin
		excluir_proc_pac_drg(r7.nr_seq_propaci,nm_usuario_p);
		end;
	end loop;
  
	select  nvl(max(2),1)
	into    ie_tipo_diagnostico_w
	from    diagnostico_medico x
	where   x.nr_atendimento    = nr_atendimento_to_p;

	select  count(*)
	into    qt_doctor_diagnosis_w
	from    diagnostico_medico x
	where   x.nr_atendimento = nr_atendimento_to_p
	and   	x.dt_diagnostico = dt_diagnostico_w;

	select 	cd_medico_resp
	into	cd_medico_resp_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_to_p;
 
	if 	(qt_doctor_diagnosis_w = 0) then
		dt_diagnostico_w := sysdate;
		
		insert into diagnostico_medico(
			nr_atendimento,
			dt_diagnostico,
			ie_tipo_diagnostico,
			cd_medico,
			dt_atualizacao,
			nm_usuario,
			ds_diagnostico)
		values(
			nr_atendimento_to_p,
			dt_diagnostico_w,
			ie_tipo_diagnostico_w,
			cd_medico_resp_w,
			sysdate,
			nm_usuario_p,
			null);
	end if;
  
	update  diagnostico_doenca d
	set     d.ie_situacao = 'I',
		d.nm_usuario = nm_usuario_p,
		d.dt_atualizacao = sysdate,
		d.dt_inativacao = sysdate,
		d.nm_usuario_inativacao = nm_usuario_p
	where   d.nr_atendimento = nr_atendimento_to_p;

	for r8 in C02 loop
		begin
		insert into diagnostico_doenca (
			ie_lado,
			cd_doenca,
			ie_classificacao_doenca,
			dt_diagnostico,
			ie_tipo_diagnostico,
			dt_liberacao,
			dt_atualizacao,
			cd_doenca_superior,
			nm_usuario,
			ie_tipo_doenca,
			nr_seq_classif_adic,
			dt_cid,
			dt_inicio,
			dt_fim,
			dt_manifestacao,
			qt_tempo,
			nr_atendimento,
			ie_situacao)
		values(
			r8.ie_lado,
			r8.cd_doenca,
			r8.ie_classificacao_doenca,
			dt_diagnostico_w,
			r8.ie_tipo_diagnostico,
			sysdate,
			sysdate,
			r8.cd_doenca_superior,
			nm_usuario_p,
			r8.ie_tipo_doenca,
			r8.nr_seq_classif_adic,
			r8.dt_cid,
			r8.dt_inicio,
			r8.dt_fim,
			r8.dt_manifestacao,
			r8.qt_tempo,
			nr_atendimento_to_p,
			'A');
		end;
	end loop;
  
	update  procedimento_pac_medico ppm
	set     ppm.ie_situacao = 'I',
		ppm.dt_atualizacao = sysdate,
		ppm.nm_usuario = nm_usuario_p,
		ppm.dt_inativacao = sysdate,
		ppm.nm_usuario_inativacao = nm_usuario_p
	where   ppm.nr_atendimento = nr_atendimento_to_p;

	for r9 in c03 loop
		begin
		INSERT INTO procedimento_pac_medico (
			nr_sequencia,
			cd_procedimento,
			qt_procedimento,
			ie_lado,
			dt_procedimento,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			ie_origem_proced,
			cd_setor_atendimento,
			ie_proc_princ,
			ie_proc_adicional,
			dt_liberacao,
			cd_departamento,
			nr_atendimento)
		values(
			procedimento_pac_medico_seq.nextval,
			r9.cd_procedimento,
			r9.qt_procedimento,
			r9.ie_lado,
			r9.dt_procedimento,
			sysdate,
			nm_usuario_p,
			'A', -- Active
			r9.ie_origem_proced,
			r9.cd_setor_atendimento,
			r9.ie_proc_princ,
			r9.ie_proc_adicional,
			null,
			r9.cd_departamento,
			nr_atendimento_to_p);
		end;
	end loop;

	update  episodio_paciente_drg
	set     Ie_Situacao = 'I'
	where   nr_atendimento = nr_atendimento_to_p;

	select 	episodio_paciente_drg_seq.nextval
	into	episodio_paciente_drg_seq_w
	from 	dual;

	insert into episodio_paciente_drg(		
		nr_sequencia,
		nr_seq_episodio_paciente,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_drg,
		ie_tipo_drg,
		nr_seq_drg_proc,
		tx_departamento,
		tx_ajuste,
		vl_base,
		vl_liquido,
		vl_desconto,
		vl_adicional,
		vl_adicional_dia,
		vl_desconto_dia,
		vl_taxa,
		qt_dias_excedido,
		qt_dias_abaixo,
		qt_estadia_min,
		qt_estadia_max,
		qt_estadia_drg,
		ie_situacao,
		nr_seq_propaci,
		nr_atendimento
	)
	values(
		episodio_paciente_drg_seq_w,
		nr_seq_episodio_paciente_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_drg_w,
		ie_tipo_drg_w,
		nr_seq_drg_proc_w,
		tx_departamento_w,
		tx_ajuste_w,
		vl_base_w,
		vl_liquido_w,
		vl_desconto_w,
		vl_adicional_w,
		vl_adicional_dia_w,
		vl_desconto_dia_w,
		vl_taxa_w,
		qt_dias_excedido_w,
		qt_dias_abaixo_w,
		qt_estadia_min_w,
		qt_estadia_max_w,
		qt_estadia_drg_w,
		ie_situacao_w,
		null,
		nr_atendimento_to_p
	);

	GERAR_DRG_PROC_PAC_CONTA(nr_atendimento_to_p, null, episodio_paciente_drg_seq_w, null, nm_usuario_p,'N');
	end;
end if;

commit;

end copy_drg_code;
/
