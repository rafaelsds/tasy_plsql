create or replace
procedure confirmar_recebimento_etapa(	nr_sequencia_p	number,
					nm_usuario_p	varchar2) is 

dt_recebimento_w	date;

begin

select	max(dt_recebimento)
into	dt_recebimento_w
from	conta_paciente_etapa
where	nr_sequencia = nr_sequencia_p;

if	(dt_recebimento_w is null) then
	controlar_recebimento_etapa(nr_sequencia_p, nm_usuario_p, 'R');
else
	controlar_recebimento_etapa(nr_sequencia_p, nm_usuario_p, 'D');
end if;

end confirmar_recebimento_etapa;
/
