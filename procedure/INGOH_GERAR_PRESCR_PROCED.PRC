create or replace
procedure ingoh_gerar_prescr_proced(	nr_prescricao_p		number,
					nr_sequencia_p		number,
					cd_procedimento_p	varchar2,					
					cd_material_exame_p	varchar2,
					cd_setor_atendimento_p	varchar2,
					ds_retorno_p		out varchar2) is 
					
ie_origem_proced_w	number(10);
nr_atendimento_w	number(10);
ie_tipo_atendimento_w	number(10);
ie_tipo_convenio_w	number(10);
cd_convenio_w		number(10);
cd_categoria_w		varchar2(10);
nr_doc_convenio_w	varchar2(30);
cd_estabelecimento_w	number(10);
nr_seq_exame_w		number(10);
sql_err_w		varchar2(2000);
cd_material_exame_w	varchar2(255);
cd_setor_atendimento_w	number(10);
cd_setor_atend_w	number(10);

begin

if 	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) and
	(cd_procedimento_p is not null) and	
	(cd_material_exame_p is not null) then
	
	select	max(a.nr_atendimento)
	into	nr_atendimento_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p;
	
	select	max(nvl(nr_atendimento,0)),
		max(nvl(ie_tipo_atendimento,0)),
		max(nvl(cd_estabelecimento,0)),
		max(nvl(ie_tipo_convenio,0))
	into	nr_atendimento_w,
		ie_tipo_atendimento_w,
		cd_estabelecimento_w,
		ie_tipo_convenio_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;
	
	
	select  max(Obter_Origem_Proced(cd_estabelecimento_w,ie_tipo_atendimento_w,ie_tipo_convenio_w,cd_convenio_w))
	into	ie_origem_proced_w
	from 	dual;
	
	select	max(nvl(cd_convenio,0)),
		max(nvl(cd_categoria,0)),
		max(nvl(nr_doc_convenio,0))
	into	cd_convenio_w,
		cd_categoria_w,
		nr_doc_convenio_w
	from	atend_categoria_convenio
	where	nr_atendimento = nr_atendimento_w
	and	nr_seq_interno = obter_atecaco_atendimento(nr_atendimento);
	
	select	max(obter_exame_procedimento(cd_procedimento_p, ie_origem_proced_w, nr_atendimento_w))
	into	nr_seq_exame_w
	from	dual;	
	
	select	max(a.cd_material_exame)
	into    cd_material_exame_w
	from	material_exame_lab a,
		material_exame_lab_int b,
		equipamento_lab c
	where   a.nr_sequencia = b.nr_seq_material
	and     c.cd_equipamento = b.cd_equipamento
	and     c.ds_sigla = 'SOFTLAB'
	and     b.cd_material_integracao = cd_material_exame_p;

	select	max(a.cd_setor_atendimento)
	into	cd_setor_atendimento_w
	from	setor_atendimento a
	where	a.cd_setor_externo = cd_setor_atendimento_p;
	
	select	obter_setor_atendimento(nr_atendimento_w)
	into	cd_setor_atend_w
	from	dual;

	if (cd_setor_atendimento_w is null) then
		ds_retorno_p		:= substr(sql_err_w || ' - ' || to_char(nr_prescricao_p)||' - '||to_char(nr_sequencia_p)||' - '||to_char(cd_procedimento_p)||' - '||to_char(cd_material_exame_p),1,2000);
		insert into log_tasy values (sysdate,'SOFTLAB', 88937, sql_err_w || ' - ' || to_char(nr_prescricao_p)||' - '||to_char(nr_sequencia_p)||' - '||to_char(cd_procedimento_p)||' - '||to_char(cd_material_exame_p));
		commit;	
	else	
		begin
		insert into prescr_procedimento	(cd_procedimento,
						cd_material_exame,
						nr_doc_convenio,					
						nr_prescricao,
						dt_atualizacao,
						nr_sequencia,
						qt_procedimento,
						nm_usuario,
						ie_origem_proced,
						ie_origem_inf,
						nr_seq_exame,
						cd_motivo_baixa,
						cd_setor_atendimento,
						cd_setor_coleta,
						cd_setor_entrega,
						dt_prev_execucao,
						ie_amostra,
						ie_urgencia,						
						ie_suspenso,
						ie_emite_mapa,
						ie_executar_leito,
						ie_se_necessario,
						ie_acm,
						ie_externo,
						ie_autorizacao,
						ie_avisar_result,
						ie_cobra_paciente,
						ie_exame_bloqueado,
						ie_filtrado,
						ie_aliquotado,
						ie_irradiado,
						ie_lavado,
						ie_aprovacao_execucao,
						ie_alterar_horario,
						ie_mostrar_web,
						ie_modificado,
						ie_descricao_cirurgica) 
				values		(cd_procedimento_p,
						cd_material_exame_w,
						nr_doc_convenio_w,
						nr_prescricao_p,
						sysdate,
						nr_sequencia_p,
						1,
						'SOFTLAB',
						ie_origem_proced_w,
						1,
						nr_seq_exame_w,
						0,
						cd_setor_atendimento_w,
						cd_setor_atend_w,
						cd_setor_atend_w,
						sysdate,
						'N',
						'N',
						'N',
						'S',
						'N',
						'N',
						'N',
						'N',
						'A',
						'N',
						'N',
						'N',
						'N',
						'N',
						'N',
						'N',
						'S',
						'S',
						'N',
						'N',
						'N');
		exception
		when others then	
			rollback;		
			sql_err_w 		:= substr(SQLERRM, 1, 1800);
			ds_retorno_p		:= substr(sql_err_w || ' - ' || to_char(nr_prescricao_p)||' - '||to_char(nr_sequencia_p)||' - '||to_char(cd_procedimento_p)||' - '||to_char(cd_material_exame_p),1,2000);
			insert into log_tasy values (sysdate,'SOFTLAB', 88937, sql_err_w || ' - ' || to_char(nr_prescricao_p)||' - '||to_char(nr_sequencia_p)||' - '||to_char(cd_procedimento_p)||' - '||to_char(cd_material_exame_p));
			commit;
		end;
	end if;
else
	rollback;
	ds_retorno_p	:= substr(' Um ou mais campos obrigat�rios n�o foram informados! '|| to_char(nr_prescricao_p)||' - '||to_char(nr_sequencia_p)||' - '||to_char(cd_procedimento_p)||' - '||to_char(cd_material_exame_p),1,2000);
	insert into log_tasy values (sysdate,'SOFTLAB',88932, substr(' Um ou mais campos obrigat�rios n�o foram informados! '|| to_char(nr_prescricao_p)||' - '||to_char(nr_sequencia_p)||' - '||to_char(cd_procedimento_p)||' - '||to_char(cd_material_exame_p),1,2000));
	commit;
end if;

commit;

end ingoh_gerar_prescr_proced;
/