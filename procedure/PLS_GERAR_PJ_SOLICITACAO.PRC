create or replace
procedure pls_gerar_pj_solicitacao
			(	nr_seq_solicitacao_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				cd_cgc_p	out	varchar2) is

cd_cgc_solicitacao_w		varchar2(14);
qt_registros_w			number(10);

nm_pessoa_fisica_w		pls_solicitacao_comercial.nm_pessoa_fisica%type;
cd_cep_w			varchar2(15);
ds_endereco_w			varchar2(40);
ds_municipio_w			varchar2(40);
sg_uf_municipio_w		pls_solicitacao_comercial.sg_uf_municipio%type;
ds_complemento_w		varchar2(255);
nr_telefone_w			varchar2(15);
nr_endereco_w			varchar2(10);
ds_email_w			varchar2(60);
nm_contato_w			varchar2(255);
cd_tipo_pessoa_w		number(3);
nr_ddi_w			varchar2(3);
nr_ddd_w			varchar2(3);

begin

select	max(cd_cgc)
into	cd_cgc_solicitacao_w
from	pls_solicitacao_comercial
where	nr_sequencia	= nr_seq_solicitacao_p;

if	(nvl(cd_cgc_solicitacao_w,'0') = '0') then /* N�o � poss�vel gerar a pessoa jur�dica. Favor informar o CNPJ na solicita��o de lead. */
	wheb_mensagem_pck.exibir_mensagem_abort( 192753, null );
end if;

select	count(*)
into	qt_registros_w
from	pessoa_juridica
where	cd_cgc	= cd_cgc_solicitacao_w;

if	(qt_registros_w > 0) then /* J� existe um cadastro com o CNPJ ' || cd_cgc_solicitacao_w || '. Verifique. */
	wheb_mensagem_pck.exibir_mensagem_abort( 192754, 'CD_CGC_SOLICITACAO=' || cd_cgc_solicitacao_w );
else	
	select	nm_pessoa_fisica,
		cd_cep,
		ds_endereco,
		substr(obter_desc_municipio_ibge(cd_municipio_ibge),1,40),
		sg_uf_municipio,
		ds_complemento,
		nr_telefone,
		nr_endereco,
		ds_email,
		nm_contato,
		nr_ddi,
		nr_ddd
	into	nm_pessoa_fisica_w,
		cd_cep_w,
		ds_endereco_w,
		ds_municipio_w,
		sg_uf_municipio_w,
		ds_complemento_w,
		nr_telefone_w,
		nr_endereco_w,
		ds_email_w,
		nm_contato_w,
		nr_ddi_w,
		nr_ddd_w
	from	pls_solicitacao_comercial
	where	nr_sequencia	= nr_seq_solicitacao_p;
	
	if	(cd_cep_w is null) then /* Favor informar o CEP para a solicita��o de lead! */
		wheb_mensagem_pck.exibir_mensagem_abort( 192748, null );
	end if;
	
	if	(sg_uf_municipio_w is null) then /* Favor informar o UF para a solicita��o de lead! */
		wheb_mensagem_pck.exibir_mensagem_abort( 192749, null );
	end if;
	
	if	(ds_municipio_w is null) then /* Favor informar o munic�pio para a solicita��o de lead! */
		wheb_mensagem_pck.exibir_mensagem_abort( 192750, null );
	end if;
	
	if	(ds_endereco_w is null) then /* Favor informar o endere�o para a solicita��o de lead! */
		wheb_mensagem_pck.exibir_mensagem_abort( 192751, null );
	end if;
	
	select	max(cd_tipo_pessoa)
	into	cd_tipo_pessoa_w
	from	tipo_pessoa_juridica
	where	ie_situacao	= 'A';
	
	insert into pessoa_juridica(	cd_cgc,
					ds_razao_social,
					nm_fantasia,
					cd_cep,
					ds_endereco,
					ds_bairro,
					ds_municipio,
					sg_estado,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_complemento,
					nr_telefone,
					nr_endereco,
					ds_email,
					nm_pessoa_contato,
					cd_tipo_pessoa,
					ie_prod_fabric,
					ie_situacao,
					nr_ddd_telefone,
					nr_ddi_telefone)
				values(	cd_cgc_solicitacao_w,
					nm_pessoa_fisica_w,
					nm_pessoa_fisica_w,
					cd_cep_w,
					ds_endereco_w,
					' ',
					ds_municipio_w,
					sg_uf_municipio_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					ds_complemento_w,
					nr_telefone_w,
					nr_endereco_w,
					ds_email_w,
					nm_contato_w,
					cd_tipo_pessoa_w,
					'N',
					'A',
					nr_ddd_w,
					nr_ddi_w);
end if;

cd_cgc_p	:= cd_cgc_solicitacao_w;

commit;

end pls_gerar_pj_solicitacao;
/