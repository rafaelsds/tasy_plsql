create or replace procedure lancar_proced_pflegegrad(nr_atendimento_p	number,
						ie_pflegegrad_old_p	number,
						ie_pflegegrad_new_p	number,
						nm_usuario_p		varchar2,
						dt_liberacao_p		paciente_pflegegrad.dt_liberacao%type,
						nr_seq_pflegegrad_p	number,
						ie_erfolgter_old_p	varchar2,
						ie_erfolgter_new_p	varchar2) is

nr_seq_tipo_admissao_fat_w	tipo_admissao_fat.nr_sequencia%type;
cd_procedimento_old_w		procedimento.cd_procedimento%type; 
ie_origem_proced_old_w		procedimento.ie_origem_proced%type;
cd_procedimento_new_w		procedimento.cd_procedimento%type;
ie_origem_proced_new_w		procedimento.ie_origem_proced%type;
nr_seq_proc_paciente_w		procedimento_paciente.nr_sequencia%type;
cd_setor_atendimento_w		procedimento_paciente.cd_setor_atendimento%type;
nr_seq_atepacu_w		atend_paciente_unidade.nr_seq_interno%type;
cd_convenio_w			convenio.cd_convenio%type;
cd_categoria_w			categoria_convenio.cd_categoria%type;
nr_doc_convenio_w		procedimento_paciente.nr_doc_convenio%type;
ie_tipo_guia_w			procedimento_paciente.ie_tipo_guia%type;
cd_senha_w			procedimento_paciente.cd_senha%type;
cd_estabelecimento_w		atendimento_paciente.cd_estabelecimento%type;
dt_entrada_unidade_w		date;
nr_seq_episodio_w			procedimento_pac_medico.nr_seq_episodio%type;
nr_seq_proc_pac_medico_w	procedimento_pac_medico.nr_sequencia%type;

begin

	if (nr_atendimento_p is not null) then
		/* Busca o tipo de visita do atendimento do paciente para filtrar o procedimento pflegegrad.*/
		select	max(nr_seq_tipo_admissao_fat),
			max(cd_estabelecimento)
		into	nr_seq_tipo_admissao_fat_w,
			cd_estabelecimento_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;
		
		/* Se o pflegegrad foi alterado, verifica se existe procedimento para o pflegegrad antigo e exclui.*/
		if (ie_pflegegrad_old_p is not null or ie_erfolgter_old_p is not null) then
			select	max(cd_procedimento),
				max(ie_origem_proced)
			into	cd_procedimento_old_w,
				ie_origem_proced_old_w
			from	procedimento_pflegegrad
			where	((ie_pflegegrad = ie_pflegegrad_old_p) or (ie_pflegegrad is null and ie_erfolgter = ie_erfolgter_old_p))
			and	nvl(nr_seq_tipo_admissao_fat,nvl(nr_seq_tipo_admissao_fat_w,0)) = nvl(nr_seq_tipo_admissao_fat_w,0);
			
			if (cd_procedimento_old_w is not null and ie_origem_proced_old_w is not null) then
				select	max(nr_sequencia)
				into	nr_seq_proc_paciente_w
				from	procedimento_paciente
				where	nr_atendimento = nr_atendimento_p
				and	cd_procedimento = cd_procedimento_old_w
				and	ie_origem_proced = ie_origem_proced_old_w;
				
				if (nr_seq_proc_paciente_w is not null) then
					delete from procedimento_paciente
					where nr_sequencia = nr_seq_proc_paciente_w;
				end if;
			end if;
		end if;
		
		/* Verifica se existe procedimento cadastrado para o pflegegrad do paciente para lan�ar no atendimento.*/
		if ((ie_pflegegrad_new_p is not null or ie_erfolgter_new_p is not null) and dt_liberacao_p is not null) then
		
			select	max(cd_procedimento),
				max(ie_origem_proced)
			into	cd_procedimento_new_w,
				ie_origem_proced_new_w
			from	procedimento_pflegegrad
			where	((ie_pflegegrad = ie_pflegegrad_new_p) or (ie_pflegegrad is null and ie_erfolgter = ie_erfolgter_new_p))
			and	nvl(nr_seq_tipo_admissao_fat,nvl(nr_seq_tipo_admissao_fat_w,0)) = nvl(nr_seq_tipo_admissao_fat_w,0);
			
			nr_seq_episodio_w := obter_episodio_atendimento(nr_atendimento_p);
			
			if (nr_seq_episodio_w > 0 and cd_procedimento_new_w is not null and ie_origem_proced_new_w is not null) then
				
				select	obter_setor_atendimento(nr_atendimento_p)
				into	cd_setor_atendimento_w
				from	dual;
				
				select	max(nr_seq_interno)
				into	nr_seq_atepacu_w
				from	atend_paciente_unidade
				where	nr_atendimento = nr_atendimento_p
				and	cd_setor_atendimento = cd_setor_atendimento_w;
				
				select	max(dt_entrada_unidade)
				into	dt_entrada_unidade_w
				from	atend_paciente_unidade
				where	nr_atendimento = nr_atendimento_p
				and	nr_seq_interno = nr_seq_atepacu_w;
				
				obter_convenio_execucao(nr_atendimento_p, sysdate, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);
				
				select	procedimento_pac_medico_seq.nextval
				into	nr_seq_proc_pac_medico_w
				from	dual;
				
				
				
				insert into procedimento_pac_medico (	
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_atendimento,
					ie_situacao,
					dt_liberacao,
					dt_inativacao,
					nm_usuario_inativacao,
					ds_justificativa,
					cd_procedimento,
					ie_origem_proced,
					qt_procedimento,
					dt_procedimento,
					ie_lado,
					cd_departamento,
					nr_seq_propaci,
					cd_setor_atendimento,
					nr_seq_episodio,
					ie_proc_princ,
					nr_seq_proc_interno,
					ie_proc_adicional,
					nr_seq_pflegegrad)
				values(nr_seq_proc_pac_medico_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_atendimento_p,
					'A',
					null,
					null,
					null,
					null,
					cd_procedimento_new_w,
					ie_origem_proced_new_w,
					1,
					sysdate,
					null,
					null,
					null,
					cd_setor_atendimento_w,
					decode(nr_seq_episodio_w,0,null,nr_seq_episodio_w),
					null,
					null,
					null,
					nr_seq_pflegegrad_p);
					
				liberar_proc_pac_medic(nr_seq_proc_pac_medico_w,nm_usuario_p);					
			end if;
		end if;
	end if;
	
end lancar_proced_pflegegrad;
/