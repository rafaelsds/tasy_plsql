CREATE OR REPLACE PROCEDURE LOC_OBJ_MARCA_OBJ 
(
  NM_USUARIO_P IN VARCHAR2
, CD_FUNCAO_P IN NUMBER 
, NR_SEQ_OBJ_SUP_P IN NUMBER 
, NR_SEQ_P IN NUMBER 
, IE_MARCADO_P IN VARCHAR2 
) AS 
BEGIN
  UPDATE w_localizador_objetos
  SET    ie_marcado = IE_MARCADO_P
  WHERE  nm_usuario = NM_USUARIO_P
     AND cd_funcao = CD_FUNCAO_P
     AND nr_seq_obj_sup = NR_SEQ_OBJ_SUP_P
     AND COALESCE(nr_seq_obj, nr_seq_dic_obj, nr_seq_evt) = NR_SEQ_P; 
END LOC_OBJ_MARCA_OBJ;
/