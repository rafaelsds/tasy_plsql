create or replace
procedure gerar_w_bionexo_item_resposta(
					cd_cnpj_p		varchar2,                 
					ds_comentario_p		varchar2,           
					ds_embalagem_p		varchar2,            
					ds_fabricante_p		varchar2,       
					nr_seq_item_p		varchar2,             
					vl_preco_total_p	varchar2,          
					vl_preco_unitario_p	varchar2,
					ie_selecionado_p	varchar2,  
					nm_usuario_p		varchar2) is
				

nr_sequencia_w	number(10);

			
begin
--apapandini OS623800
select	w_bionexo_item_resposta_seq.nextval
into	nr_sequencia_w
from	dual;


insert into w_bionexo_item_resposta(	
			cd_cnpj,                 
			ds_comentario,           
			ds_embalagem,            
			ds_fabricante,       
			nr_seq_item,             
			nr_sequencia,            
			vl_preco_total,          
			vl_preco_unitario, 
			ie_selecionado,
			nm_usuario,              
			nm_usuario_nrec,          
			dt_atualizacao,          
			dt_atualizacao_nrec  )
values(	cd_cnpj_p,                 
	ds_comentario_p,           
	ds_embalagem_p,            
	ds_fabricante_p,       
	nr_seq_item_p,             
	nr_sequencia_w,            
	vl_preco_total_p,          
	vl_preco_unitario_p, 
	ie_selecionado_p,
	nm_usuario_p,              
	nm_usuario_p, 
	sysdate,          
	sysdate);
	
commit;

end gerar_w_bionexo_item_resposta;
/
				
