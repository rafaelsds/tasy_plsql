create or replace
PROCEDURE Import_Loinc_Comp(loinc_Num_p varchar2,
                            component_p varchar2,
                            property_p varchar2,
                            time_Aspct_p varchar2,
                            system_p varchar2,
                            scale_Type_p varchar2,
                            method_Type_p varchar2,
                            loinc_Class_p varchar2,
                            class_Type_p varchar2,
                            long_Common_Name_p varchar2,
                            short_Name_p varchar2,
                            ext_Copyright_Notice_p varchar2,
                            status_p varchar2,
                            vers_First_Released_p varchar2,
                            vers_Last_Changed_p varchar2,
                            nm_usuario_p varchar2,
                            ie_localizacao_p varchar2) is

nr_seq_loinc_w       number(10);
nr_versao_w          number(10);
nr_seq_idioma_w      number(10);
nr_seq_escala_w      number(10);
nr_seq_metodo_w      number(10);
nr_seq_propriedade_w number(10);
nr_seq_sistema_w     number(10);
nr_seq_tempo_w       number(10);
nr_seq_dados_reg_w   number(10);

BEGIN
  select max(lld.nr_sequencia), max(to_number(cd_versao_ult_alt))
    into nr_seq_loinc_w, nr_versao_w
    from lab_loinc_dados lld
   inner join lab_loinc_versao llv on llv.nr_seq_loinc_dados = lld.nr_sequencia  
   where lld.cd_loinc = loinc_Num_p; 

  if ((nr_seq_loinc_w is null) or 
      (nr_versao_w < to_number(vers_Last_Changed_p))) then
    select max(nr_sequencia)
      into nr_seq_idioma_w
      from tasy_idioma
     where ds_language_tag = ie_localizacao_p;
  end if;

  if (nr_seq_loinc_w is null) then
    select lab_loinc_dados_seq.nextval 
      into nr_seq_loinc_w
      from dual;

    insert into lab_loinc_dados (nr_sequencia,
                                 cd_loinc,    
                                 ds_classtype,
                                 ds_ext_copyrig_not,
                                 ds_status,
                                 dt_atualizacao,
                                 dt_atualizacao_nrec,                               
                                 nm_usuario,
                                 nm_usuario_nrec)
                         values (nr_seq_loinc_w,
                                 loinc_Num_p,
                                 class_type_p,
                                 ext_copyright_notice_p,
                                 status_p,
                                 sysdate,
                                 sysdate,
                                 nm_usuario_p,
                                 nm_usuario_p);
    commit;  

    insert into LAB_LOINC_VERSAO (nr_sequencia,
                                  nr_seq_loinc_dados,
                                  cd_versao_release,
                                  cd_versao_ult_alt,
                                  dt_atualizacao,
                                  dt_atualizacao_nrec,
                                  nm_usuario,
                                  nm_usuario_nrec)
                          values (LAB_LOINC_VERSAO_seq.nextval,
                                  nr_seq_loinc_w,
                                  vers_First_Released_p,
                                  vers_Last_Changed_p,
                                  sysdate,
                                  sysdate,
                                  nm_usuario_p,
                                  nm_usuario_p);         

    if (scale_Type_p is not null) then
      valida_escala_loinc(scale_Type_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_escala_w);
    end if;

    if (method_Type_p is not null) then
      valida_metodo_loinc(method_Type_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_metodo_w);
    end if;

    if (property_p is not null) then      
      valida_propriedade_loinc(property_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_propriedade_w);    
    end if;

    if (system_p is not null) then      
      valida_sistema_loinc(system_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_sistema_w);    
    end if;      
    
    if (time_Aspct_p is not null) then      
      valida_tempo_loinc(time_Aspct_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_tempo_w);    
    end if;      

    insert into LAB_LOINC_DADOS_REG (nr_sequencia,
                                     nr_seq_loinc_dados,
                                     ds_classe,
                                     ds_componente,
                                     ds_long_name,                                    
                                     ds_short_name,                                     
                                     dt_atualizacao,
                                     dt_atualizacao_nrec,
                                     nm_usuario,
                                     nm_usuario_nrec, 
                                     nr_seq_idioma,
                                     nr_seq_escala,
                                     nr_seq_metodo,
                                     nr_seq_propriedade,
                                     nr_seq_sistema,
                                     nr_seq_tempo)
                             values (LAB_LOINC_DADOS_REG_seq.nextval,
                                     nr_seq_loinc_w,
                                     loinc_Class_p,
                                     component_p,
                                     long_Common_Name_p,
                                     short_Name_p,      
                                     sysdate,
                                     sysdate,
                                     nm_usuario_p,
                                     nm_usuario_p,
                                     nr_seq_idioma_w,
                                     nr_seq_escala_w,
                                     nr_seq_metodo_w,
                                     nr_seq_propriedade_w,
                                     nr_seq_sistema_w,
                                     nr_seq_tempo_w);

  elsif (nr_versao_w < to_number(vers_Last_Changed_p)) then  
  
    update lab_loinc_dados set ds_classtype = class_type_p,
                               ds_ext_copyrig_not = ext_copyright_notice_p,
                               ds_status = status_p,
                               dt_atualizacao = sysdate,
                               nm_usuario = nm_usuario_p
    where nr_sequencia = nr_seq_loinc_w;

    update LAB_LOINC_VERSAO set cd_versao_release = vers_First_Released_p,
                                cd_versao_ult_alt = vers_Last_Changed_p,
                                dt_atualizacao = sysdate,
                                nm_usuario = nm_usuario_p
    where nr_seq_loinc_dados = nr_seq_loinc_w;    

    if (scale_Type_p is not null) then
      valida_escala_loinc(scale_Type_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_escala_w);
    end if;

    if (method_Type_p is not null) then
      valida_metodo_loinc(method_Type_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_metodo_w);
    end if;

    if (property_p is not null) then      
      valida_propriedade_loinc(property_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_propriedade_w);    
    end if;

    if (system_p is not null) then      
      valida_sistema_loinc(system_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_sistema_w);    
    end if;      

    if (time_Aspct_p is not null) then      
      valida_tempo_loinc(time_Aspct_p, nm_usuario_p, vers_First_Released_p, vers_Last_Changed_p, 'Completo', nr_seq_loinc_w, nr_seq_tempo_w);    
    end if;    
   
   select nr_sequencia
     into nr_seq_dados_reg_w
     from LAB_LOINC_DADOS_REG
    where nr_seq_loinc_dados = nr_seq_loinc_w and 
          nr_seq_idioma = nr_seq_idioma_w;

    update LAB_LOINC_DADOS_REG set ds_classe = loinc_Class_p,
                                   ds_componente = component_p,
                                   ds_long_name = long_Common_Name_p,
                                   ds_short_name = short_Name_p,
                                   dt_atualizacao = sysdate,
                                   nm_usuario = nm_usuario_p,
                                   nr_seq_idioma = nr_seq_idioma_w,
                                   nr_seq_escala = nr_seq_escala_w,
                                   nr_seq_metodo = nr_seq_metodo_w,
                                   nr_seq_propriedade = nr_seq_propriedade_w,
                                   nr_seq_sistema = nr_seq_sistema_w,
                                   nr_seq_tempo = nr_seq_tempo_w
     where nr_sequencia = nr_seq_dados_reg_w;
  end if;  

  commit;  
END Import_Loinc_Comp;
/