create or replace
procedure liberacao_solic_compras(	nr_solic_compra_p	number,
					ie_opcao_p		varchar2,
					nm_usuario_p		Varchar2) is 										
/*
ie_opcao_p :
L - Liberacao
A - Aprova��o
*/					

begin

if	(ie_opcao_p = 'L') then

	update	solic_compra
	set 	dt_liberacao 	= sysdate,
		nm_usuario 	= nm_usuario_p
	where 	nr_solic_compra = nr_solic_compra_p;
	commit;
elsif	(ie_opcao_p = 'A') then

	update	solic_compra
	set 	dt_autorizacao = sysdate,
		nm_usuario 	= nm_usuario_p
	where 	nr_solic_compra = nr_solic_compra_p;
	commit;
end if;

end liberacao_solic_compras;
/