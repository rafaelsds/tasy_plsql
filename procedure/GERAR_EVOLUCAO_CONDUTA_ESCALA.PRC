create or replace
procedure gerar_evolucao_conduta_escala(nr_seq_conduta_p number,
										ds_respostas_p   varchar2,
										nr_Atendimento_p number,
										nm_usuario_p 	 varchar2 ) is
										
qt_reg_w            number(10);
ds_perguntas_w      varchar2(32000);
ds_conteudo_w	    varchar2(32000);
ds_cabecalho_w	    varchar2(32000);
ds_rodape_w		    varchar2(255);
nr_tam_fonte_w	    number(5,0);
ds_fonte_w		    varchar2(100);
qt_controle_chr_w	number(10);
ds_pos_inicio_rtf_w	number(10);
p_linha_w           varchar2(10) := chr(13) || chr(10);

Cursor c01 is
	with respostas as  
			(select distinct regexp_substr (ds_respostas_p,'[^;]+',1,level) resp, rownum id
			 from   dual
			 connect by   regexp_substr (ds_respostas_p,'[^;]+',1,level) is not null),
		perguntas as 
			(select a.ds_pergunta, 
					a.ie_resposta, 
					a.ie_resposta_ant, 
					decode(nvl(a.ie_mostrar_mensagem,'N'),'S',a.ds_orientacao, '') ds_orientacao, 
					decode(nvl(a.ie_mostrar_mensagem_negativa,'N'), 'S', a.ds_orientacao_negativa, '') ds_orientacao_negativa
			 from pep_conduta_condicao a
			 where a.nr_seq_conduta = nr_seq_conduta_p
			 order by a.nr_seq_ordem)
select p.ds_pergunta, p.ie_resposta, 
      decode(p.ie_resposta,'A', p.ds_orientacao, decode(r.resp, 'S', p.ds_orientacao, p.ds_orientacao_negativa)) ds_orientacao,  
	  decode(r.resp,'S',obter_desc_expressao(719927,'Sim'),obter_desc_expressao(327114,'N�o')) ds_resposta
from (select rownum id, p.* from perguntas p) p join respostas r on p.id = r.id;

begin

select count(1) 
into   qt_reg_w
from   TIPO_EVOLUCAO
where  cd_tipo_evolucao = 'CES';

if (qt_reg_w > 0) then

	select '\b '||Obter_Valor_Dominio(1799, a.IE_ESCALA) ||' \b0 : '
	into   ds_perguntas_w
	from   pep_conduta_escala a, pep_conduta b
	where  a.nr_sequencia = b.nr_seq_conduta_esc
	and    b.nr_sequencia = nr_seq_conduta_p;

	for perguntas in c01 loop
		ds_perguntas_w := ds_perguntas_w || p_linha_w || p_linha_w || obter_desc_expressao(295480,'Pergunta') ||': '|| perguntas.ds_pergunta 
			|| p_linha_w || obter_desc_expressao(297781,'Resposta') ||': '|| perguntas.ds_resposta;
		
		if (perguntas.ds_orientacao <> ' ') then
			ds_perguntas_w := ds_perguntas_w || p_linha_w || obter_desc_expressao(294894,'Orienta��o') ||': '||perguntas.ds_orientacao;
		end if;
	end loop;

	/*Gera��o RTF*/	
	
	/*inicio substituir os caracteres chr(13) e chr(10) pelo \par que que representa o enter no rtf*/
	qt_controle_chr_w :=0;
		
	while (instr(ds_perguntas_w, chr(13)) > 0) and (qt_controle_chr_w < 100) loop
		ds_perguntas_w := replace(ds_perguntas_w, chr(13), '\par ');
		qt_controle_chr_w := qt_controle_chr_w + 1;
	end loop;
	qt_controle_chr_w := 0;

	while (instr(ds_perguntas_w, chr(10)) > 0) and (qt_controle_chr_w < 100) loop
		ds_perguntas_w := replace(ds_perguntas_w, chr(10), '');
		qt_controle_chr_w := qt_controle_chr_w + 1;
	end loop;
	
	/*fim substituir os caracteres chr(13) e chr(10) pelo \par que que representa o enter no rtf*/
	/*pega o cabecalho do rtf*/
	ds_cabecalho_w := '{\rtf1\ansi\deff0{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}}' ||
						'{\colortbl ;\red0\green0\blue0;}' ||
						'\viewkind4\uc1\pard\cf1\lang1046\fs20  \fs16 \f1 ';
	ds_pos_inicio_rtf_w := instr(ds_cabecalho_w, 'lang') + 8;
	ds_conteudo_w := substr(ds_cabecalho_w, 1, ds_pos_inicio_rtf_w) || 'fs20 ';
	
	/*acrecenta conteudo texto livre*/
	ds_conteudo_w := ds_conteudo_w || ds_perguntas_w;
	
	/*acrecenta resto do conteudo do rtf*/
	ds_rodape_w   := '}';
	ds_conteudo_w := ds_conteudo_w || '\par ' || substr(ds_cabecalho_w, ds_pos_inicio_rtf_w, length(ds_cabecalho_w));
	ds_conteudo_w  := ds_conteudo_w || ' ' || ds_rodape_w; 
	
	insert into evolucao_paciente (
		cd_evolucao,
		dt_evolucao,
		ie_tipo_evolucao,
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		ds_evolucao,
		cd_medico,
		dt_liberacao,
		ie_evolucao_clinica,
		ie_situacao)
	values (evolucao_paciente_seq.nextval,
		sysdate,
		OBTER_FUNCAO_USUARIO(nm_usuario_p),
		OBTER_PESSOA_ATENDIMENTO(nr_Atendimento_p,'C'),
		sysdate,
		nm_usuario_p,
		nr_Atendimento_p,
		ds_conteudo_w,
		Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C'),
		sysdate,
		'CES',
		'A');
		
	commit;
end if;

end;
/