create or replace
procedure usjrp_gerar_mensagem_cob_flash
			(ds_conteudo_p			varchar2,
			nr_seq_cobranca_p		number,
			nr_titulo_p			number,
			ie_opcao_p			varchar2,
			ie_elimina_caract_esp_p		varchar2,
			ie_elimina_acentuacao_p		varchar2,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number,
			ie_origem_mensagem_p		varchar2) is

/*Finalidade: Rotina utilizada para a gera��o das mensagens FLASH - muito cuidado ao alterar
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Abrir os arquvios de remessa - na nossa base           \\192.168.0.230\UTLFILE

Rotinas que utilizam:
	USJRP_GER_COB_PARTICULAR_ITAU
*/
	
ds_conteudo_w		varchar2(4000);
ds_conteudo_msg_w	varchar2(4000);
nr_sequencia_w		number(10);
	
begin
if	(ie_opcao_p = '1') then
	ds_conteudo_msg_w := ds_conteudo_p;
	
	if	(nvl(ie_elimina_caract_esp_p,'N') = 'S') then
		ds_conteudo_msg_w	:= elimina_caractere_especial(ds_conteudo_msg_w);
	end if;
	
	if	(nvl(ie_elimina_acentuacao_p,'N') = 'S') then
		ds_conteudo_msg_w	:= elimina_acentuacao(ds_conteudo_msg_w);
	end if;

	select	w_pls_mensagem_tit_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into w_pls_mensagem_tit
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_conteudo,
		nr_seq_cobranca,
		nr_titulo,
		ie_tipo_mensagem,
		ie_origem_mensagem)
	values	(nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_conteudo_msg_w,
		nr_seq_cobranca_p,
		nr_titulo_p,
		'7',
		ie_origem_mensagem_p);
end if;
	
end usjrp_gerar_mensagem_cob_flash;
/