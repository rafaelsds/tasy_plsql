Create or Replace
PROCEDURE CTB_Consistir_Classif_Conta(
			cd_conta_contabil_p	Varchar2,
			cd_empresa_p		Number,
			cd_grupo_p		Number,
			ie_tipo_conta_p		Varchar2,
			cd_classif_conta_p		Varchar2,
			ds_erro_p		OUT	Varchar2) is

cd_classif_grupo_w		Varchar2(40);
cd_classif_sup_w		Varchar2(40);
cd_classif_w		Varchar2(40);
ie_sistema_ctb_w		Varchar2(01);
ie_classif_conta_w		Varchar2(01);
ie_consiste_mascara_w	Varchar2(02);
i			Integer;
ds_erro_w		Varchar2(255);
qt_reg_w		Number(10,0);
ie_separador_conta_w	empresa.ie_sep_classif_conta_ctb%type;
cd_classif_conta_w conta_contabil_classif.cd_classificacao%type;
BEGIN

cd_classif_conta_w      := ctb_obter_classif_conta(cd_conta_contabil_p,cd_classif_conta_p,sysdate);
ie_separador_conta_w	:= philips_contabil_pck.get_separador_conta;

if	(cd_empresa_p is null)  then
	wheb_mensagem_pck.exibir_mensagem_abort(231711);
end if;

ds_erro_w		:= '';

select	ie_consiste_mascara_contabil,
	ie_classif_conta
into	ie_consiste_mascara_w,
	ie_classif_conta_w
from	empresa
where	cd_empresa	= cd_empresa_p;

if	(ie_classif_conta_w = 'N') then
	select	count(*)
	into	qt_reg_w
	from	conta_contabil
	where	cd_classificacao		= cd_classif_conta_w
	and	cd_conta_contabil		<> cd_conta_contabil_p
	and	ie_situacao = 'A'
	and	substr(obter_se_conta_vigente2(cd_conta_contabil, dt_inicio_vigencia, dt_fim_vigencia, sysdate),1,1) = 'S'
	and	cd_empresa 		= cd_empresa_p;
	if	(qt_reg_w > 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280446);
	end if;
end if;

if	(ie_consiste_mascara_w in ('CO','A')) then
	begin
	cd_classif_w		:= cd_classif_conta_p;
	select	nvl(max(cd_mascara), 'x')
	into	cd_classif_grupo_w
	from	ctb_grupo_conta
	where	cd_grupo	= cd_grupo_p
	and	cd_empresa	= cd_empresa_p;

	if	(ie_tipo_conta_p is null)  then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280447);
	elsif	(cd_grupo_p is null) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280448);
	elsif	(cd_classif_conta_p is null) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280455);
	elsif	(cd_classif_grupo_w = 'x') then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280457);
	else
		begin
		FOR i IN 1..Length(cd_classif_w) LOOP
			begin
			if	(substr(cd_classif_w,i,1) = ie_separador_conta_w) and
				(substr(cd_classif_grupo_w,i,1) <> ie_separador_conta_w) then
				ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280458) ||
						cd_classif_grupo_w || ')';
			end if;
 			if	(substr(cd_classif_w,i,1) in ('1','2','3','4','5','6','7','8','9','0')) and
				(substr(cd_classif_grupo_w,i,1) not in ('1','2','3','4','5','6','7','8','9','0')) then
				ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280458) ||
						cd_classif_grupo_w || ')';
			end if;
        		end;
		END LOOP;
		if	(ds_erro_w is null) and
			(length(cd_classif_w)  <> length(cd_classif_grupo_w)) and
			(substr(cd_classif_grupo_w, length(cd_classif_w) + 1, 1) <> ie_separador_conta_w) then
			ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280459) ||
					cd_classif_grupo_w || ')';
		end if;
		select	ctb_obter_classif_conta_sup(cd_classif_conta_w, null, cd_empresa_p)
		into	cd_classif_sup_w
		from	dual;
		
		if	(ds_erro_w is null) and
			(cd_classif_sup_w is not null) then
			select	decode(count(*),0,WHEB_MENSAGEM_PCK.get_texto(280460), 1, '', WHEB_MENSAGEM_PCK.get_texto(280461) || cd_classif_sup_w)
			into	ds_erro_w
			from	conta_contabil
			where	ctb_obter_classif_conta(cd_conta_contabil, cd_classificacao, sysdate)	= cd_classif_sup_w
			and	substr(obter_se_conta_vigente2(cd_conta_contabil, dt_inicio_vigencia, dt_fim_vigencia, sysdate),1,1) = 'S'
			and	cd_empresa	= cd_empresa_p
			and	ie_situacao	= 'A';
		end if;
		end;
	end if;
	end;
end if;
ds_erro_p		:= ds_erro_w;

END CTB_Consistir_Classif_Conta;
/