create or replace
procedure pls_envio_email_resc_html
			(	nr_seq_solicitacao_p	number,
				ds_mensagem_p		varchar2,
				ds_assunto_p		varchar2,
				ds_destinatario_p	varchar2,
				ds_anexo_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

cd_prioridade_w			pls_email_parametros.cd_prioridade%type;
nr_seq_email_w			pls_email.nr_sequencia%type;
vl_parametro_w			varchar2(255);
ds_anexo_w			varchar2(4000);
ds_item_anexo_w			varchar2(4000);

begin
--Origem : 2 - OPS - Gest�o de Rescis�o de Contrato
select	nvl(max(cd_prioridade),5)
into	cd_prioridade_w
from	pls_email_parametros
where	ie_origem 		= 2
and	cd_estabelecimento 	= cd_estabelecimento_p
and	ie_situacao 		= 'A';

Obter_Param_Usuario(268, 10, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);

if	(nr_seq_solicitacao_p is not null) then
	insert into pls_email (	
				nr_sequencia,
				nr_seq_solic_rescisao,
				cd_estabelecimento,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nm_usuario,
				dt_atualizacao,
				ie_tipo_mensagem,
				ie_status,
				ie_origem,
				ds_remetente,
				ds_mensagem,
				ds_destinatario,
				ds_assunto,
				cd_prioridade)
	values(			pls_email_seq.nextval,
				nr_seq_solicitacao_p,
				cd_estabelecimento_p,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				2,	--Envio de Dados da Solicita��o de rescis�o
				'P',	--Pendente
				2,	--OPS - Gest�o de Rescis�o de Contrato
				vl_parametro_w, 	--Remetente
				ds_mensagem_p,
				ds_destinatario_p,
				ds_assunto_p,
				cd_prioridade_w
				)
		returning nr_sequencia into nr_seq_email_w;
	
	if	(ds_anexo_p is not null) then
		ds_anexo_w	:= ds_anexo_p || ',';
		
		while	(ds_anexo_w is not null) loop
			ds_item_anexo_w	:= substr(ds_anexo_w,1,instr(ds_anexo_w,',')-1);
			
			insert into pls_email_anexo (
						nr_sequencia,
						nr_seq_email,
						nm_usuario,
						nm_usuario_nrec,
						dt_atualizacao,
						dt_atualizacao_nrec,
						ds_arquivo,
						ie_tipo_anexo )
			values(			pls_email_anexo_seq.nextval,
						nr_seq_email_w,
						nm_usuario_p,
						nm_usuario_p,
						sysdate,
						sysdate,
						ds_item_anexo_w,
						'A'
						);
			
			ds_anexo_w	:= substr(ds_anexo_w, instr(ds_anexo_w,',')+1 ,length(ds_anexo_w));
		end loop;
	end if;
	
	commit;
end if;

end pls_envio_email_resc_html;
/