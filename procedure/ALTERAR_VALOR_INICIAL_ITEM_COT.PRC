create or replace
procedure alterar_valor_inicial_item_cot(		nr_sequencia_p		number,
					vl_incial_p		number,
					ds_justificativa_p		varchar2,
					nm_usuario_p		Varchar2) is 

ds_razao_social_w			varchar2(100);
nr_seq_cot_forn_w			number(10);
vl_unitario_inicial_w			number(13,4);
nr_item_cot_compra_w		number(10);
historico_w			varchar2(4000);
cd_material_w			number(6);
ds_material_w			varchar2(255);
nr_cot_compra_w			number(10);
begin

select	nr_cot_compra,
	nr_seq_cot_forn,
	vl_unitario_inicial,
	nr_item_cot_compra,
	cd_material,
	substr(obter_desc_material(cd_material),1,255)
into	nr_cot_compra_w,
	nr_seq_cot_forn_w,
	vl_unitario_inicial_w,
	nr_item_cot_compra_w,
	cd_material_w,
	ds_material_w
from	cot_compra_forn_item
where	nr_sequencia = nr_sequencia_p;

select	substr(obter_nome_pf_pj(null,cd_cgc_fornecedor),1,100)
into	ds_razao_social_w
from	cot_compra_forn
where	nr_sequencia = nr_seq_cot_forn_w;

update	cot_compra_forn_item
set	vl_unitario_inicial = vl_incial_p
where	nr_sequencia = nr_sequencia_p;

historico_w := substr(WHEB_MENSAGEM_PCK.get_texto(297134,
					'NR_ITEM_COT_COMPRA_W='||nr_item_cot_compra_w||
					';CD_MATERIAL_W='||cd_material_w||
					';DS_MATERIAL_W='||ds_material_w||
					';DS_RAZAO_SOCIAL_W='||ds_razao_social_w||
					';VL_UNITARIO_INICIAL_W='||campo_mascara(vl_unitario_inicial_w,4)||
					';VL_INCIAL_P='||campo_mascara(vl_incial_p,4)||
					';DS_JUSTIFICATIVA_P='||ds_justificativa_p),1,4000);

insert into cot_compra_hist (nr_sequencia,
	nr_cot_compra,
	dt_atualizacao,
	nm_usuario,
	dt_historico,
	ds_titulo,
	ds_historico,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_origem,
	ie_tipo)
values 	(cot_compra_hist_seq.nextval,
	nr_cot_compra_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	WHEB_MENSAGEM_PCK.get_texto(297138),
	historico_w,
	sysdate,
	nm_usuario_p,
	'S','H');

commit;

end alterar_valor_inicial_item_cot;
/