CREATE OR REPLACE PROCEDURE valida_item_repasse (
                                                nr_interno_conta_p NUMBER
) IS

    nr_sequencia_item_ww      repasse_terceiro_item.nr_sequencia_item%TYPE;
    ie_status_w               repasse_terceiro.ie_status%TYPE;
    qt_repasse_vencimento_w   NUMBER(10, 0) := 0;
    vl_repasse_w              repasse_terceiro_item.vl_repasse%TYPE;
    CURSOR c01 IS
    SELECT
        r.nr_repasse_terceiro,
        r.nr_sequencia_item,
        r.vl_repasse,
        r.ds_observacao,
        r.nr_seq_terceiro,
        r.cd_medico,
        r.dt_liberacao,
        r.cd_centro_custo,
        c.ie_status_acerto
    FROM
        repasse_terceiro_item   r,
        conta_paciente          c
    WHERE
        r.nr_interno_conta = c.nr_interno_conta
        AND r.nr_interno_conta = nr_interno_conta_p;

BEGIN
    FOR item IN c01 LOOP
        IF ( item.nr_sequencia_item IS NOT NULL ) THEN
            IF ( item.nr_repasse_terceiro IS NOT NULL ) THEN
                SELECT
                    ie_status
                INTO ie_status_w
                FROM
                    repasse_terceiro
                WHERE
                    nr_repasse_terceiro = item.nr_repasse_terceiro;

                SELECT
                    COUNT(*)
                INTO qt_repasse_vencimento_w
                FROM
                    repasse_terceiro_venc
                WHERE
                    nr_repasse_terceiro = item.nr_repasse_terceiro;

                IF ( ie_status_w = 'F' OR qt_repasse_vencimento_w > 0 ) THEN
                    IF ( item.ie_status_acerto = 1 ) THEN
                        UPDATE repasse_terceiro_item
                        SET
                            nr_interno_conta = NULL,
                            ds_observacao = item.ds_observacao
                                            || ' '
                                            || obter_desc_expressao(1058714)
                        WHERE
                            nr_sequencia_item = item.nr_sequencia_item
                            AND nr_repasse_terceiro = item.nr_repasse_terceiro;

                    END IF;

                    vl_repasse_w := item.vl_repasse * -1;
                    SELECT
                        nvl(MAX(nr_sequencia_item), 0) + 1
                    INTO nr_sequencia_item_ww
                    FROM
                        repasse_terceiro_item
                    WHERE
                        nr_repasse_terceiro IS NULL;

                    INSERT INTO repasse_terceiro_item (
                        nr_sequencia,
                        dt_atualizacao,
                        nm_usuario,
                        vl_repasse,
                        ds_observacao,
                        nr_repasse_terceiro,
                        nr_sequencia_item,
                        nr_seq_terceiro,
                        dt_lancamento,
                        cd_medico,
                        dt_liberacao,
                        cd_centro_custo
                    ) VALUES (
                        repasse_terceiro_item_seq.NEXTVAL,
                        sysdate,
                        wheb_usuario_pck.get_nm_usuario,
                        vl_repasse_w,
                        item.ds_observacao
                        || ' '
                        || obter_desc_expressao(1058714),
                        NULL,
                        nr_sequencia_item_ww,
                        item.nr_seq_terceiro,
                        sysdate,
                        item.cd_medico,
                        item.dt_liberacao,
                        item.cd_centro_custo
                    );

                ELSE
                    INSERT INTO repasse_terceiro_item_log (
                        nr_sequencia,
                        dt_atualizacao,
                        nm_usuario,
                        dt_atualizacao_nrec,
                        nm_usuario_nrec,
                        vl_repasse,
                        ds_observacao,
                        nr_interno_conta
                    ) VALUES (
                        repasse_terceiro_item_log_seq.NEXTVAL,
                        sysdate,
                        wheb_usuario_pck.get_nm_usuario,
                        sysdate,
                        wheb_usuario_pck.get_nm_usuario,
                        item.vl_repasse,
                        item.ds_observacao
                        || ' '
                        || obter_desc_expressao(1058714),
                        nr_interno_conta_p
                    );

                    DELETE FROM repasse_terceiro_item
                    WHERE
                        nr_sequencia_item = item.nr_sequencia_item
                        AND nr_interno_conta = nr_interno_conta_p
                        AND nr_repasse_terceiro = item.nr_repasse_terceiro;

                END IF;

            ELSE
                INSERT INTO repasse_terceiro_item_log (
                    nr_sequencia,
                    dt_atualizacao,
                    nm_usuario,
                    dt_atualizacao_nrec,
                    nm_usuario_nrec,
                    vl_repasse,
                    ds_observacao,
                    nr_interno_conta
                ) VALUES (
                    repasse_terceiro_item_log_seq.NEXTVAL,
                    sysdate,
                    wheb_usuario_pck.get_nm_usuario,
                    sysdate,
                    wheb_usuario_pck.get_nm_usuario,
                    item.vl_repasse,
                    item.ds_observacao
                    || ' '
                    || obter_desc_expressao(1058714),
                    nr_interno_conta_p
                );

                DELETE FROM repasse_terceiro_item
                WHERE
                    nr_sequencia_item = item.nr_sequencia_item
                    AND nr_interno_conta = nr_interno_conta_p;

            END IF;

        END IF;
    END LOOP;
END valida_item_repasse;
/
