create or replace
procedure pls_impugnar_proced
			(	nr_seq_processo_proc_p	in	pls_processo_procedimento.nr_sequencia%type,
				nr_seq_impugnacao_p	in	pls_impugnacao.nr_sequencia%type,
				nm_usuario_p		in	usuario.nm_usuario%type) is 


begin

insert	into pls_impugnacao_proc
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_impugnacao,
	nr_seq_processo_proc)
select	pls_impugnacao_proc_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_impugnacao_p,
	nr_seq_processo_proc_p
from	dual;

commit;

end pls_impugnar_proced;
/
