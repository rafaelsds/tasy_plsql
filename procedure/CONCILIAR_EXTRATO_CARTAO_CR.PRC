create or replace
procedure CONCILIAR_EXTRATO_CARTAO_CR(	nr_seq_extrato_p	number,
					ie_opcao_p		varchar2,
					nm_usuario_p		varchar2) is

qt_regra_w		number(10);
nr_seq_bandeira_w	number(10);
cd_estabelecimento_w	number(4);

begin

select	max(a.nr_seq_bandeira),
	max(a.cd_estabelecimento)
into	nr_seq_bandeira_w,
	cd_estabelecimento_w
from	extrato_cartao_cr a
where	a.nr_sequencia	= nr_seq_extrato_p;

select	count(*)
into	qt_regra_w
from	band_cartao_cr_regra_atrib b,
	bandeira_cartao_cr_regra a
where	a.nr_sequencia		= b.nr_seq_regra_bandeira
and	nvl(a.cd_estabelecimento,nvl(cd_estabelecimento_w,0))	= nvl(cd_estabelecimento_w,0)
and	sysdate			between nvl(a.dt_inicio_vigencia,sysdate) and nvl(a.dt_fim_vigencia,sysdate)
and	a.nr_seq_bandeira	= nr_seq_bandeira_w;

if	(nvl(qt_regra_w,0)	> 0) then
	conciliar_ext_cartao_cr_regra(nr_seq_extrato_p,ie_opcao_p,nm_usuario_p);
else
	conciliar_ext_cartao_cr_comum(nr_seq_extrato_p,ie_opcao_p,nm_usuario_p);
end if;

end CONCILIAR_EXTRATO_CARTAO_CR;
/