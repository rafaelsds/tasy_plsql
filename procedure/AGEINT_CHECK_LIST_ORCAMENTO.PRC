create or replace
procedure ageint_check_list_orcamento(nr_seq_agenda_int_p	number,
					nm_usuario_p		Varchar2,
					cd_estabelecimento_p	number,
					ie_gerado_p		out varchar2) is 

		
nr_seq_proc_interno_w		number(10,0);
ie_gerar_check_list_w		varchar2(1);	
nr_seq_ageint_check_list_w	number(10,0);
ie_gerado_w			varchar2(1) := 'N';
		
Cursor C01 is
	select	nr_seq_proc_interno
	from	agenda_integrada_item
	where	nr_seq_agenda_int = nr_seq_agenda_int_p
	order by 1;			
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_proc_interno_w;
exit when C01%notfound;
	begin

	ie_gerar_check_list_w	:= substr(ageint_obter_se_proc_check(nr_seq_proc_interno_w,nr_seq_agenda_int_p, cd_estabelecimento_p),1,1);
				
	if	(ie_gerar_check_list_w = 'S') then
		
		select	ageint_check_list_paciente_seq.nextval
		into	nr_seq_ageint_check_list_w
		from	dual;
				
		insert into ageint_check_list_paciente (nr_sequencia,
							nr_seq_ageint,
							dt_atualizacao,
							nm_usuario,
							ie_tipo_check_list)
						values (nr_seq_ageint_check_list_w,
							nr_seq_agenda_int_p,
							sysdate,	
							nm_usuario_p,
							'I');
									
					commit;
		ie_gerado_w	:= 'S';
								
		Ageint_Gerar_Check_List(nr_seq_ageint_check_list_w,nr_seq_proc_interno_w,nr_seq_agenda_int_p,nm_usuario_p, cd_estabelecimento_p);

	end if;
	
	end;
end loop;
close C01;

commit;

ie_gerado_p	:= ie_gerado_w;

end ageint_check_list_orcamento;
/