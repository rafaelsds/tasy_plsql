create or replace
PROCEDURE verifica_dupl_dia_PA(	cd_material_p		number,
				nr_prescricao_p		number,
				nr_seq_material_p	number,
				nm_usuario_p		varchar2,
				ds_retorno_p	Out	Varchar2) is

cd_pessoa_fisica_w	atendimento_paciente.cd_pessoa_fisica%type;
dt_prescricao_w		date;
ie_consitir_susp_w	Varchar2(10 char);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nr_seq_ficha_tecnica_w	number(10,0);
ie_validade_prescr_w	varchar2(1);
dt_inicio_prescr_w		date;


cursor	C01 is
	select	distinct
			nr_prescricao,
			nm_medico,
			ie_suspenso
	from	(
		select	a.nr_prescricao,
				substr(obter_nome_medico(b.cd_medico,'N'),1,60) nm_medico,
				decode(a.ie_suspenso,'S',UPPER(obter_desc_expressao(298991)),'') ie_suspenso
		from	material c,
				prescr_material	a,
				prescr_medica b
		where	a.nr_prescricao		= b.nr_prescricao
		and		a.cd_material		= c.cd_material
		and		c.nr_seq_ficha_tecnica	= nr_seq_ficha_tecnica_w
		and		b.cd_pessoa_fisica	= cd_pessoa_fisica_w
		and		ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(b.dt_prescricao) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_prescricao_w) 
		and 	ie_validade_prescr_w 	= 'S'
		and		a.ie_agrupador		not in (3,7,9)
		and		a.nr_seq_kit 		is null
		and		((b.nr_prescricao <> nr_prescricao_p) or (a.nr_sequencia <> nr_seq_material_p))
		and		nvl(a.ie_modificado,'N') <> 'S' 
		and		((ie_consitir_susp_w	= 'S') or (nvl(a.ie_suspenso,'N') = 'N'))
		union all
		select	a.nr_prescricao,
				substr(obter_nome_medico(b.cd_medico,'N'),1,60) nm_medico,
				decode(a.ie_suspenso,'S',UPPER(obter_desc_expressao(298991)),'') ie_suspenso
		from	material c,
				prescr_material	a,
				prescr_medica b
		where	a.nr_prescricao		= b.nr_prescricao
		and		a.cd_material		= c.cd_material
		and		c.nr_seq_ficha_tecnica	= nr_seq_ficha_tecnica_w
		and		b.cd_pessoa_fisica	= cd_pessoa_fisica_w
		and		dt_inicio_prescr_w between b.dt_inicio_prescr and b.dt_validade_prescr
		and 	ie_validade_prescr_w	= 'C'
		and		a.ie_agrupador		not in (3,7,9)
		and		a.nr_seq_kit 		is null
		and		((b.nr_prescricao <> nr_prescricao_p) or (a.nr_sequencia <> nr_seq_material_p))
		and		nvl(a.ie_modificado,'N') <> 'S' 
		and		((ie_consitir_susp_w	= 'S') or (nvl(a.ie_suspenso,'N') = 'N'))
	);

begin

select	max(nr_seq_ficha_tecnica)
into	nr_seq_ficha_tecnica_w
from	material
where	cd_material	= cd_material_p;

if	(nr_seq_ficha_tecnica_w > 0) then

	select	dt_prescricao,
			cd_pessoa_fisica,
			cd_estabelecimento,
			dt_inicio_prescr
	into	dt_prescricao_w,
			cd_pessoa_fisica_w,
			cd_estabelecimento_w,
			dt_inicio_prescr_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;

	Obter_Param_Usuario(924, 307, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, ie_consitir_susp_w);
	Obter_Param_Usuario(924, 308, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, ie_validade_prescr_w);

	if (nvl(ie_validade_prescr_w, 'N') <> 'N') then

		for r_c01_w in C01
		loop
			ds_retorno_p:= substr(ds_retorno_p || r_c01_w.ie_suspenso || ' ' || r_c01_w.nr_prescricao || ' ' || r_c01_w.nm_medico || chr(10) || chr(13),1,255);
		end loop;

	end if;
end if;

end verifica_dupl_dia_PA;
/