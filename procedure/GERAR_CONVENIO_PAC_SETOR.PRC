create or replace
procedure Gerar_Convenio_Pac_Setor(	nr_seq_paciente_p	number,
					nm_usuario_p		Varchar2) is 

	
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);
cd_convenio_w			number(10);
cd_categoria_w			varchar2(10);
cd_plano_w			varchar2(10);
cd_usuario_convenio_w		varchar2(60);
qt_reg_w			number(10);
ie_tipo_atendimento_w		number(3);
ie_restringe_estabelecimento_w varchar2(1);
cd_estabelecimento_w number(4);

	
Cursor C01 is
	select	a.cd_convenio,
		a.cd_categoria,
		a.cd_plano_convenio,
		a.cd_usuario_convenio,
		a.ie_tipo_atendimento
	from	atendimento_paciente_v a
	where	a.cd_pessoa_fisica        = cd_pessoa_fisica_w
	and 	((ie_restringe_estabelecimento_w = 'N') or ((a.cd_estabelecimento = cd_estabelecimento_w) and (ie_restringe_estabelecimento_w = 'S')))
	order by nr_atendimento;	
	
	
cursor c02 is
	select	cd_convenio,
		cd_categoria,
		CD_PLANO,
		CD_USUARIO_CONVENIO,
		ie_tipo_atendimento
	from	agenda_consulta a
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	order by nr_sequencia;
					
begin
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
obter_param_usuario(281, 1546 , obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,cd_estabelecimento_w , ie_restringe_estabelecimento_w);
select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	PACIENTE_SETOR
where	nr_seq_paciente	= nr_seq_paciente_p;



if	(nvl(nr_atendimento_w,0)	<> 0) then
	
	select	a.cd_convenio,
		a.cd_categoria,
		a.cd_plano_convenio,
		a.cd_usuario_convenio
	into	cd_convenio_w,
		cd_categoria_w,
		cd_plano_w,
		cd_usuario_convenio_w
	from	atend_categoria_convenio a
	where	a.nr_atendimento        = nr_atendimento_w
	and	 a.nr_seq_interno        = obter_atecaco_atendimento(a.nr_atendimento);
	
else
	open C01;
	loop
	fetch C01 into	
		cd_convenio_w,
		cd_categoria_w,
		cd_plano_w,
		cd_usuario_convenio_w,
		ie_tipo_atendimento_w;
	exit when C01%notfound;
	end loop;
	close C01;
end if;

if	(cd_convenio_w	is null) then
	open C02;
	loop
	fetch C02 into	
		cd_convenio_w,
		cd_categoria_w,
		cd_plano_w,
		cd_usuario_convenio_w,
		ie_tipo_atendimento_w;
	exit when C02%notfound;
	end loop;
	close C02;
end if;

select	count(*)
into	qt_reg_w
from	paciente_setor_convenio
where	nr_seq_paciente	= nr_seq_paciente_p;

if	(cd_convenio_w	is not null) and
	(cd_categoria_w	is not null) and
	(qt_reg_w	= 0) then
	
	select	count(*)
	into	qt_reg_w
	from	convenio_plano
	where	cd_convenio = cd_convenio_w
	and	cd_plano = cd_plano_w;
	
	if	(qt_reg_w	= 0) then
		cd_plano_w	:= null;
	end if;
	
	
	insert into paciente_setor_convenio	(	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_convenio,
							cd_categoria,
							cd_plano,
							cd_usuario_convenio,
							nr_seq_paciente,
							ie_tipo_atendimento)
				values		(	paciente_setor_convenio_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_convenio_w,
							cd_categoria_w,
							cd_plano_w,
							cd_usuario_convenio_w,
							nr_seq_paciente_p,
							ie_tipo_atendimento_w);
	commit;
	
end if;



end Gerar_Convenio_Pac_Setor;
/