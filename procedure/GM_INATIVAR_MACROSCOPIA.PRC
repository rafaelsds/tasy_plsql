create or replace
procedure gm_inativar_macroscopia(	nr_seq_prescr_macroscopia_p		number,
					nm_usuario_p				Varchar2) is 

begin


if	(nr_seq_prescr_macroscopia_p is not null) and
	(nm_usuario_p is not null) then

	update 	prescr_proc_macroscopia
	set	dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p
	where	nr_sequencia = nr_seq_prescr_macroscopia_p;
	
	commit;
	
end if;	

end gm_inativar_macroscopia;
/