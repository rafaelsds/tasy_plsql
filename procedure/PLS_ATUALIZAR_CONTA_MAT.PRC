create or replace
procedure pls_atualizar_conta_mat
		(	nr_seq_conta_mat_p	number,
			nm_usuario_p		Varchar2) is 

qt_glosa_w			number(15);
vl_material_w			number(15,2);
nr_seq_conta_w			number(10);

begin

select	count(*)
into	qt_glosa_w
from	pls_conta_glosa
where	nr_seq_conta_mat	= nr_seq_conta_mat_p
and	ie_situacao = 'A';

update	pls_conta_mat
set	vl_liberado	= vl_material_w,
	ie_status	= 'P'
where	nr_sequencia	= nr_seq_conta_mat_p;

update	pls_conta
set	ie_status	= 'P'
where	nr_sequencia	= nr_seq_conta_w;

if	(qt_glosa_w = 0) then

	select	vl_material,
		nr_seq_conta
	into	vl_material_w,
		nr_seq_conta_w
	from	pls_conta_mat
	where	nr_sequencia	= nr_seq_conta_mat_p;

	update	pls_conta_mat
	set	vl_liberado	= vl_material_w,
		ie_status	= 'S'
	where	nr_sequencia	= nr_seq_conta_mat_p;

	update	pls_conta
	set	ie_status	= 'P'
	where	nr_sequencia	= nr_seq_conta_w;
	
	select	count(*)
	into	qt_glosa_w
	from	pls_conta_glosa
	where	nr_seq_conta	= nr_seq_conta_w;
	if	(qt_glosa_w = 0) then
		select	count(*)
		into	qt_glosa_w
		from	pls_conta_glosa     a,
			pls_conta_proc      b
		where	a.nr_seq_conta_proc	= b.nr_sequencia
		and	b.nr_seq_conta		= nr_seq_conta_w
		and	a.ie_situacao = 'A';
	end if;

	if	(qt_glosa_w = 0) then
		select	count(*)
		into	qt_glosa_w
		from	pls_conta_glosa  a,
			pls_conta_mat    b
		where	a.nr_seq_conta_mat	= b.nr_sequencia
		and	b.nr_seq_conta		= nr_seq_conta_w
		and	a.ie_situacao = 'A';
	end if;
	if	(qt_glosa_w	= 0) then
		update	pls_conta
		set	ie_status	= 'L'
		where	nr_sequencia	= nr_seq_conta_w;
	end if;

end if;
commit;

end pls_atualizar_conta_mat;
/