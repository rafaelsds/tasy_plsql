create or replace
procedure fleury_ws_suspender_prescr(	nr_ficha_p	varchar2,
					cd_unidade_p	varchar2) is 

cd_estabelecimento_w	number(10);
ie_agrupa_w		varchar2(1);
nr_prescricao_w		number(10) := null;					
					
begin

select	fleury_obter_dados_unidade(cd_unidade_p, 'E'),
	fleury_obter_dados_unidade(cd_unidade_p, 'AF')
into	cd_estabelecimento_w,
	ie_agrupa_w
from	dual;

if (ie_agrupa_w = 'S') then
	select	max(nr_prescricao)
	into	nr_prescricao_w
	from	prescr_procedimento
	where	nr_controle_ext = nr_ficha_p;
else
	select	max(nr_prescricao)
	into	nr_prescricao_w
	from	prescr_medica
	where	nr_controle = nr_ficha_p;
	
	if (nr_prescricao_w is null) then
		select	max(b.nr_prescricao)
		into	nr_prescricao_w
		from	prescr_procedimento a, prescr_medica b
		where	a.nr_prescricao = b.nr_prescricao
		and	a.nr_controle_ext = nr_ficha_p
		and	((b.cd_estabelecimento = cd_estabelecimento_w) or (cd_estabelecimento_w = 0));
	end if;
	
end if;

Suspender_Prescricao(nr_prescricao_w, 0, null, 'FLEURY', 'N');

commit;

end fleury_ws_suspender_prescr;
/
