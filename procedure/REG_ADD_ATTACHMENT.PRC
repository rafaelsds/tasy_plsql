CREATE OR REPLACE
PROCEDURE REG_ADD_ATTACHMENT(	ds_attachment_path_p VARCHAR2,
				nr_seq_reg_pr_p reg_product_requirement.nr_sequencia%type,
				nr_seq_reg_cr_p reg_customer_requirement.nr_sequencia%type,
				nm_user_p reg_attachment.nm_usuario%type,
				nr_seq_attachment_p out reg_attachment.nr_sequencia%type) AS 
BEGIN
	SELECT	reg_attachment_seq.nextVal
	INTO	nr_seq_attachment_p
	FROM	dual;
	INSERT INTO reg_attachment (	NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO,
					DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NR_SEQ_REG_PR,
					NR_SEQ_REG_CR, NR_SEQ_REG_TC, NR_SEQ_REG_EVT,
					DS_ATTACHMENT)
			
			VALUES (	nr_seq_attachment_p, sysdate, nm_user_p,
					sysdate, nm_user_p, nr_seq_reg_pr_p,
					nr_seq_reg_cr_p, null, null,
					ds_attachment_path_p);
	COMMIT;
END REG_ADD_ATTACHMENT;
/