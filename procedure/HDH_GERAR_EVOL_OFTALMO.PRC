create or replace
procedure HDH_gerar_evol_oftalmo(	nr_sequencia_p 	number,
										nm_usuario_p	varchar2) is
PRAGMA AUTONOMOUS_TRANSACTION;

ds_retorno_w			 varchar2(32000) := '';
ds_consulta_w			 varchar2(32000);
nr_atendimento_w		 number(10);
cd_evolucao_w			 number(10);
nr_seq_prescr_w			 number(10);
ds_intervencao_w		 varchar(255);
dt_liberacao_w			 date;
cd_pessoa_fisica_w		 varchar2(10);
cd_profissional_w		 varchar2(10);
ie_rn_w				 	 varchar2(1);
nr_sequencia_oft_w		 varchar2(10);
ie_tipo_evolucao_w		 varchar2(3);
ds_cabecalho_w			 varchar2(32000);
ds_rodape_w		 	 	 varchar2(255);
ds_conteudo_w	 		 varchar2(32000);
ds_fonte_w				 varchar2(100);
ds_tam_fonte_w			 varchar2(10);
nr_tam_fonte_w			 number(5,0);
cd_estabelecimento_w	 number(4,0);
cd_perfil_ativo_w		 number(10);
qt_reg_w		 		 varchar2(1);
nr_seq_apres_w	 		 number(10);
dt_consulta_w			varchar2(30);
nm_medico_w				varchar2(80);
ie_dinamica_w			varchar2(1);
ie_estatica_w			varchar2(1);
ie_receita_w			varchar2(1);
ie_adicao_w				varchar2(1);
vl_de_rd_od_w			varchar2(10);
vl_dc_rd_od_w			varchar2(10);
vl_eixo_rd_od_w			varchar2(10);
ds_visao_rd_od_w		varchar2(40);
vl_de_rd_oe_w			varchar2(10);
vl_dc_rd_oe_w			varchar2(10);
vl_eixo_rd_oe_w			varchar2(10);
ds_visao_rd_oe_w		varchar2(40);
vl_de_re_od_w			varchar2(10);
vl_dc_re_od_w			varchar2(10);
vl_eixo_re_od_w			varchar2(10);
ds_visao_re_od_w		varchar2(40);
vl_de_re_oe_w			varchar2(10);
vl_dc_re_oe_w			varchar2(10);
vl_eixo_re_oe_w			varchar2(10);
ds_visao_re_oe_w		varchar2(40);
vl_de_receita_od_w		varchar2(10);
vl_dc_receita_od_w		varchar2(10);
vl_eixo_receita_od_w	varchar2(10);
ds_visao_receita_od_w	varchar2(40);
vl_de_receita_oe_w		varchar2(10);
vl_dc_receita_oe_w		varchar2(10);
vl_eixo_receita_oe_w	varchar2(10);
ds_visao_receita_oe_w	varchar2(40);
vl_adicao_od_w			varchar2(10);
vl_adicao_oe_w			varchar2(10);
vl_soma_de_rd_od_w		varchar2(10);
vl_soma_de_rd_oe_w		varchar2(10);
ds_queixa_paciente_w	varchar2(4000);
ds_fundoscopia_w		varchar2(4000);
ds_biomicroscopia_w		varchar2(4000);
ds_outros_exames_w		varchar2(4000);
ds_conduta_w			varchar2(4000);
vl_pressao_od_w			varchar2(40);
vl_pressao_oe_w			varchar2(40);
hr_pressao_w			varchar2(10);
ie_tipo_fundoscopia_w	varchar2(1);
ds_av_sc_od_w			varchar2(40);
ds_av_cc_od_w			varchar2(40);
ds_av_sc_oe_w			varchar2(40);
ds_av_cc_oe_w			varchar2(40);
ds_meo_w				varchar2(60);
vl_visao_rd_od_w		varchar2(3);
vl_visao_rd_oe_w		varchar2(3);
vl_visao_re_od_w		varchar2(3);
vl_visao_re_oe_w		varchar2(3);
vl_visao_receita_od_w	varchar2(3);
vl_visao_receita_oe_w	varchar2(3);
ds_obs_receita_w		varchar2(255);
nr_seq_cons_med_w		Number(10);	
ds_hip_diag_w			varchar2(4000);
cd_medico_w				varchar2(10);
ds_crm_w				varchar2(20);
ds_enter_w				varchar2(10)	:=  chr(13) || chr(10);
ds_impressao1_w			varchar2(10);
ds_impressao2_w			varchar2(10);
qt_ref_cons_w			number(10);

begin
	
	select	to_char(dt_consulta,'dd/mm/yyyy hh24:mi:ss'),
			substr(obter_nome_pf(cd_profissional),1,80),
			nr_atendimento,
			ie_dinamica,
			ie_estatica,
			ie_receita,
			ie_adicao,
			vl_de_rd_od,
			campo_mascara_virgula(abs(vl_dc_rd_od)),
			to_char(abs(vl_eixo_rd_od)),
			ds_visao_rd_od,
			vl_de_rd_oe,
			campo_mascara_virgula(abs(vl_dc_rd_oe)),
			to_char(abs(vl_eixo_rd_oe)),
			ds_visao_rd_oe,
			vl_de_re_od,
			campo_mascara_virgula(abs(vl_dc_re_od)),
			to_char(abs(vl_eixo_re_od)),
			ds_visao_re_od,
			vl_de_re_oe,
			campo_mascara_virgula(abs(vl_dc_re_oe)),
			to_char(abs(vl_eixo_re_oe)),
			ds_visao_re_oe,
			vl_de_receita_od,
			campo_mascara_virgula(abs(vl_dc_receita_od)),
			to_char(abs(vl_eixo_receita_od)),
			ds_visao_receita_od,
			vl_de_receita_oe,
			campo_mascara_virgula(abs(vl_dc_receita_oe)),
			to_char(abs(vl_eixo_receita_oe)),
			ds_visao_receita_oe,
			campo_mascara_virgula(abs(vl_adicao_od)),
			campo_mascara_virgula(abs(vl_adicao_oe)),
			ds_queixa_paciente,
			ds_fundoscopia,        
			ds_biomicroscopia,
			ds_outros_exames,
			ds_conduta,
			vl_pressao_od,
			vl_pressao_oe,
			to_char(hr_pressao,'hh24:mi'),
			ie_tipo_fundoscopia,
			ds_av_sc_od,
			ds_av_cc_od,
			ds_av_sc_oe,
			ds_av_cc_oe,
			ds_meo,
			to_char(vl_visao_rd_od),
			to_char(vl_visao_rd_oe),
			to_char(vl_visao_re_od),
			to_char(vl_visao_re_oe),
			to_char(vl_visao_receita_od),
			to_char(vl_visao_receita_oe),
			ds_obs_receita,
			nr_sequencia,
			ds_hip_diag,
			cd_profissional
	into
			dt_consulta_w,	
			nm_medico_w,
			nr_atendimento_w,
			ie_dinamica_w,
			ie_estatica_w,
			ie_receita_w,
			ie_adicao_w,
			vl_de_rd_od_w,
			vl_dc_rd_od_w,
			vl_eixo_rd_od_w,
			ds_visao_rd_od_w,
			vl_de_rd_oe_w,
			vl_dc_rd_oe_w,
			vl_eixo_rd_oe_w,
			ds_visao_rd_oe_w,
			vl_de_re_od_w,
			vl_dc_re_od_w,
			vl_eixo_re_od_w,
			ds_visao_re_od_w,
			vl_de_re_oe_w,
			vl_dc_re_oe_w,
			vl_eixo_re_oe_w,
			ds_visao_re_oe_w,
			vl_de_receita_od_w,
			vl_dc_receita_od_w,
			vl_eixo_receita_od_w,
			ds_visao_receita_od_w,
			vl_de_receita_oe_w,
			vl_dc_receita_oe_w,
			vl_eixo_receita_oe_w,
			ds_visao_receita_oe_w,
			vl_adicao_od_w,
			vl_adicao_oe_w,
			ds_queixa_paciente_w,
			ds_fundoscopia_w,         
			ds_biomicroscopia_w,
			ds_outros_exames_w,
			ds_conduta_w,
			vl_pressao_od_w,
			vl_pressao_oe_w,
			hr_pressao_w,
			ie_tipo_fundoscopia_w,
			ds_av_sc_od_w,
			ds_av_cc_od_w,
			ds_av_sc_oe_w,
			ds_av_cc_oe_w,
			ds_meo_w,
			vl_visao_rd_od_w,
			vl_visao_rd_oe_w,
			vl_visao_re_od_w,
			vl_visao_re_oe_w,
			vl_visao_receita_od_w,
			vl_visao_receita_oe_w,
			ds_obs_receita_w,
			nr_seq_cons_med_w,
			ds_hip_diag_w,
			cd_medico_w
	from	oft_consulta_medica
	where	nr_sequencia	= nr_sequencia_p;
		
	select	evolucao_paciente_seq.nextval
	into	cd_evolucao_w
	from	dual;
	
	select	max(ie_tipo_evolucao)
	into	ie_tipo_evolucao_w
	from	usuario
	where	nm_usuario = nm_usuario_p;

	select	nvl(max(cd_estabelecimento), 0),
			max(cd_pessoa_fisica)
	into	cd_estabelecimento_w,
			cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w; 

	cd_perfil_ativo_w := obter_perfil_ativo;
	Obter_Param_Usuario(281, 5, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, ds_fonte_w); 
	Obter_Param_Usuario(281, 6, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, ds_tam_fonte_w);
	nr_tam_fonte_w := somente_numero(ds_tam_fonte_w) * 2;
		
	Select substr(max(nvl(obter_dados_medico(cd_medico_w,'SGCRM'),obter_dados_pf(cd_medico_w,'CON')) || ':' || 
		   nvl(obter_dados_medico(cd_medico_w,'CRM'),obter_dados_pf(cd_medico_w,'CPR'))),1,20)
	into	ds_crm_w
	from	dual;
		
	if	(ds_crm_w is not null) then
		nm_medico_w:= substr(nm_medico_w||' ('||ds_crm_w||')',1,80);
	end if;
	
	ds_consulta_w	:= ds_consulta_w ||LPAD(upper(wheb_mensagem_pck.get_texto(307162)) || ': ',15,' ')	||nm_medico_w		||ds_enter_w; -- M�DICO(A)
	ds_consulta_w	:= ds_consulta_w ||LPAD(upper(wheb_mensagem_pck.get_texto(307163)) || ': ',19,' ')	||dt_consulta_w		||ds_enter_w; -- DATA CONSULTA
	ds_consulta_w	:= ds_consulta_w ||LPAD(upper(wheb_mensagem_pck.get_texto(307164)) || ': ',22,' ')	||ds_queixa_paciente_w	||ds_enter_w||ds_enter_w; -- HIST�RIA CL�NICA
	
	if	(ds_av_sc_od_w is not null) or
		(ds_av_sc_oe_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307166) || '  ' || wheb_mensagem_pck.get_texto(307169) || ': ',22,' ')||ds_av_sc_od_w ||ds_enter_w; -- AC VISUAL SC  OD
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307171) || ': ',38,' ')||ds_av_sc_oe_w ||ds_enter_w; -- OE
	end if;
	
	if	(ds_av_cc_od_w is not null) or
		(ds_av_cc_oe_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307172) || '  ' || wheb_mensagem_pck.get_texto(307169) || ': ',22,' ')||ds_av_cc_od_w ||ds_enter_w; -- AC VISUAL CC  OD
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307171) || ': ',38,' ')||ds_av_cc_oe_w ||ds_enter_w; -- OE
	end if;
	
	if	(ds_meo_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307173) || ': ',36,' ') ||ds_meo_w ||ds_enter_w; -- MEO
	end if;
	
	if	(vl_de_rd_od_w is not null) or (vl_dc_rd_od_w is not null) or (vl_eixo_rd_od_w is not null) or (vl_visao_rd_od_w is not null) or (ds_visao_rd_od_w is not null) or
		(vl_de_rd_oe_w is not null) or (vl_dc_rd_oe_w is not null) or (vl_eixo_rd_oe_w is not null) or (vl_visao_rd_oe_w is not null) or (ds_visao_rd_oe_w is not null) or
		(vl_de_re_od_w is not null) or (vl_dc_re_od_w is not null) or (vl_eixo_re_od_w is not null) or (vl_visao_re_od_w is not null) or (ds_visao_re_od_w is not null) or
		(vl_de_re_oe_w is not null) or (vl_dc_re_oe_w is not null) or (vl_eixo_re_oe_w is not null) or (vl_visao_re_oe_w is not null) or (ds_visao_re_oe_w is not null) or
		(vl_dc_receita_od_w is not null) or (vl_de_receita_od_w is not null) or (vl_eixo_receita_od_w is not null) or (vl_visao_receita_od_w is not null) or (ds_visao_receita_od_w is not null) or
		(vl_dc_receita_oe_w is not null) or (vl_de_receita_oe_w is not null) or (vl_eixo_receita_oe_w is not null) or (vl_visao_receita_oe_w is not null) or (ds_visao_receita_oe_w is not null) or
		(vl_adicao_od_w is not null) or (vl_adicao_oe_w is not null) then
			ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307528) || ': ',14,' ')||ds_enter_w; -- REFRA��O
	end if;
	
	if	(vl_de_rd_od_w is not null) or (vl_dc_rd_od_w is not null) or (vl_eixo_rd_od_w is not null) or (vl_visao_rd_od_w is not null) or (ds_visao_rd_od_w is not null) or
		(vl_de_rd_oe_w is not null) or (vl_dc_rd_oe_w is not null) or (vl_eixo_rd_oe_w is not null) or (vl_visao_rd_oe_w is not null) or (ds_visao_rd_oe_w is not null) then				
		
		if	(vl_de_rd_od_w is not null) then
			select	decode(obter_se_negativo(vl_de_rd_od_w),'S', LPAD(campo_mascara_virgula(vl_de_rd_od_w),12,' '),LPAD('+'||campo_mascara_virgula(vl_de_rd_od_w),7,' '))
			into	ds_impressao1_w
			from	dual;
		else
			ds_impressao1_w	:= LPAD(' ',7,' ');
		end if;	
				
		if	(vl_de_rd_oe_w is not null) then
			select	decode(obter_se_negativo(vl_de_rd_oe_w),'S', LPAD(campo_mascara_virgula(vl_de_rd_oe_w),7,' '),LPAD('+'||campo_mascara_virgula(vl_de_rd_oe_w),7,' '))
			into	ds_impressao2_w
			from	dual;
		else
			ds_impressao2_w	:= LPAD(' ',7,' ');
		end if;	
																-- OBJ/SUB  										OD									
		ds_consulta_w	:= ds_consulta_w ||LPAD(upper(wheb_mensagem_pck.get_texto(307175)) || '  ' || wheb_mensagem_pck.get_texto(307169) || ':',27,' ') || ds_impressao1_w ||' / '||
						LPAD('-'||vl_dc_rd_od_w,7,' ') ||' / '||LPAD(vl_eixo_rd_od_w,3,' ') ||'     20/'||vl_visao_rd_od_w ||' '||ds_visao_rd_od_w ||ds_enter_w; 
		
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307171) || ':',37,' ')|| ds_impressao2_w ||' / '||LPAD('-'||vl_dc_rd_oe_w,7,' ') ||' / '||
						LPAD(vl_eixo_rd_oe_w,3,' ') ||'     20/'||vl_visao_rd_oe_w||' '||ds_visao_rd_oe_w ||ds_enter_w;
	
		end if;
						
		if	(vl_de_re_od_w is not null) or (vl_dc_re_od_w is not null) or (vl_eixo_re_od_w is not null) or (vl_visao_re_od_w is not null) or (ds_visao_re_od_w is not null) or
			(vl_de_re_oe_w is not null) or (vl_dc_re_oe_w is not null) or (vl_eixo_re_oe_w is not null) or (vl_visao_re_oe_w is not null) or (ds_visao_re_oe_w is not null) then				
		
		if	(vl_de_re_od_w is not null) then
			select	decode(obter_se_negativo(vl_de_re_od_w),'S', LPAD(campo_mascara_virgula(vl_de_re_od_w),7,' '),LPAD('+'||campo_mascara_virgula(vl_de_re_od_w),7,' '))
			into	ds_impressao1_w
			from	dual;
		else
			ds_impressao1_w	:= LPAD(' ',7,' ');
		end if;
	
		if	(vl_de_re_oe_w is not null) then
			select	decode(obter_se_negativo(vl_de_re_oe_w),'S', LPAD(campo_mascara_virgula(vl_de_re_oe_w),7,' '),LPAD('+'||campo_mascara_virgula(vl_de_re_oe_w),7,' '))
			into	ds_impressao2_w
			from	dual;
		else
			ds_impressao2_w	:= LPAD(' ',7,' ');
		end if;
					
		ds_consulta_w	:= ds_consulta_w ||LPAD(upper(wheb_mensagem_pck.get_texto(307177)) || '  ' || wheb_mensagem_pck.get_texto(307169) || ':',22,' ')|| ds_impressao1_w ||' / '||LPAD('-'||vl_dc_re_od_w,7,' ') ||' / '||LPAD -- CICLOPLEGIA  OD
						(vl_eixo_re_od_w,3,' ') ||'     20/'||vl_visao_re_od_w||' '||ds_visao_re_od_w ||ds_enter_w;
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307171) || ':',37,' ')|| ds_impressao2_w ||' / '||LPAD('-'||vl_dc_re_oe_w,7,' ') ||' / '||LPAD -- OE
						(vl_eixo_re_oe_w,3,' ') ||'     20/'||vl_visao_re_oe_w||' '||ds_visao_re_oe_w ||ds_enter_w;
	end if;		
	
	if	(vl_dc_receita_od_w is not null) or (vl_de_receita_od_w is not null) or (vl_eixo_receita_od_w is not null) or (vl_visao_receita_od_w is not null) or (ds_visao_receita_od_w is not null) or
		(vl_dc_receita_oe_w is not null) or (vl_de_receita_oe_w is not null) or (vl_eixo_receita_oe_w is not null) or (vl_visao_receita_oe_w is not null) or (ds_visao_receita_oe_w is not null) then				
				
		if	(vl_de_receita_od_w is not null) then
			select	decode(obter_se_negativo(vl_de_receita_od_w),'S', LPAD(campo_mascara_virgula(vl_de_receita_od_w),7,' '),LPAD('+'||campo_mascara_virgula(vl_de_receita_od_w),7,' '))
					into	ds_impressao1_w
					from	dual;
		else
			ds_impressao1_w	:= LPAD(' ',7,' ');
		end if;
				
		if	(vl_de_receita_oe_w is not null) then
			select	decode(obter_se_negativo(vl_de_receita_oe_w),'S', LPAD(campo_mascara_virgula(vl_de_receita_oe_w),7,' '),LPAD('+'||campo_mascara_virgula(vl_de_receita_oe_w),7,' '))
					into	ds_impressao2_w
					from	dual;
		else
			ds_impressao2_w	:= LPAD(' ',7,' ');
		end if;
				
		ds_consulta_w	:= ds_consulta_w ||LPAD(upper(wheb_mensagem_pck.get_texto(307182)) || '  ' || wheb_mensagem_pck.get_texto(307169) || ':',27,' ')|| ds_impressao1_w ||' / '||
						   LPAD('-'||vl_dc_receita_od_w,7,' ') ||' / '||LPAD(vl_eixo_receita_od_w,3,' ') ||'     20/'||vl_visao_receita_od_w||' '||ds_visao_receita_od_w ||ds_enter_w; -- RECEITA  OD
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307171) || ':',37,' ')|| ds_impressao2_w ||' / '||LPAD('-'||vl_dc_receita_oe_w,7,' ') ||' / '||
						   LPAD(vl_eixo_receita_oe_w,3,' ') ||'     20/'||vl_visao_receita_oe_w||' '||ds_visao_receita_oe_w ||ds_enter_w; -- OE			
	end if;
	
	if	(vl_adicao_od_w is not null) or (vl_adicao_oe_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||LPAD(upper(wheb_mensagem_pck.get_texto(307185)) || '  ' || wheb_mensagem_pck.get_texto(307169) || ':',28,' ')||LPAD('+'||vl_adicao_od_w,7,' ') ||ds_enter_w; -- ADI��O  OD
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307171) || ':',37,' ')||LPAD('+'||vl_adicao_oe_w,7,' ') ||ds_enter_w; -- OE
	end if;
	
	if	(ds_obs_receita_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307532) || ': ',26,' ')|| ds_obs_receita_w || ds_enter_w; -- OBS RECEITA
	end if;
	
	if	(vl_pressao_od_w is not null) or
		(vl_pressao_oe_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||LPAD(wheb_mensagem_pck.get_texto(307187) || ': ',38,' ')||LPAD(wheb_mensagem_pck.get_texto(307169) || ': ',4,' ')||vl_pressao_od_w ||'   ' ||wheb_mensagem_pck.get_texto(307171) || ': '||vl_pressao_oe_w||'  ' || wheb_mensagem_pck.get_texto(307189) || ': '||hr_pressao_w ||ds_enter_w;
											-- PIO															-- OD																			--OE																	-- HORA
	end if;
	
	if	(ds_biomicroscopia_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||ds_enter_w||LPAD(upper(wheb_mensagem_pck.get_texto(307202)) || ': ',22,' ')||ds_biomicroscopia_w||ds_enter_w; -- BIOMICROSCOPIA
	end if;
		
	
	if	(ds_fundoscopia_w is not null) then
		begin
		ds_consulta_w	:= ds_consulta_w ||ds_enter_w||LPAD(wheb_mensagem_pck.get_texto(307201)||': ',39,' ')||ds_fundoscopia_w	||ds_enter_w; -- FO
		end;
	end if;
	
	
	if	(ds_outros_exames_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||ds_enter_w||LPAD(upper(wheb_mensagem_pck.get_texto(307203)) || ': ',21,' ')||ds_outros_exames_w||ds_enter_w; -- OUTROS EXAMES
	end if;
	
	if	(ds_hip_diag_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||ds_enter_w||LPAD(upper(wheb_mensagem_pck.get_texto(307534)) || ': ',22,' ')||ds_hip_diag_w||ds_enter_w; -- HIP�TESE DIAGN�STICA
	end if;
	
	if	(ds_conduta_w is not null) then
		ds_consulta_w	:= ds_consulta_w ||ds_enter_w||LPAD(upper(wheb_mensagem_pck.get_texto(307204)) || ': ',30,' ')||ds_conduta_w||ds_enter_w; -- CONDUTA
	end if;
	
	ds_cabecalho_w := '{\rtf1\ansi\ansicpg1252\deff0\deflang1046{\fonttbl{\f0\fswiss\fcharset0 ' || ds_fonte_w || ';}}' ||
								'{\*\generator Msftedit 5.41.15.1507;}\viewkind4\uc1\pard\f0\fs' || nr_tam_fonte_w || ' ';
	ds_rodape_w    := '}';
	ds_consulta_w   := replace(ds_consulta_w, chr(13) || chr(10), ' \par ');			
	ds_conteudo_w  := ds_cabecalho_w || ds_consulta_w || ds_rodape_w;
		
	insert into evolucao_paciente (
				cd_evolucao,
				dt_evolucao,
				ie_tipo_evolucao,
				cd_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				nr_atendimento,
				ds_evolucao,
				cd_medico,
				dt_liberacao,
				ie_evolucao_clinica,
				ie_recem_nato,
				ie_situacao,
				nr_seq_sae)
	values (cd_evolucao_w,
				sysdate,
				ie_tipo_evolucao_w,
				cd_pessoa_fisica_w,
				sysdate,
				nm_usuario_p,
				nr_atendimento_w,
				ds_conteudo_w,
				cd_medico_w,
				sysdate,
				'OFT',
				'N',
				'A',
				null);
	commit;

end HDH_gerar_evol_oftalmo;
/