create or replace
procedure adep_obter_gas (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p		number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p		date,					
		dt_validade_limite_p		date,					
		ie_exibir_hor_realizados_p	varchar2,
		ie_exibir_hor_suspensos_p	varchar2,
		ie_regra_inclusao_p		varchar2,
		ie_data_lib_prescr_p		varchar2,
		ie_lib_pend_rep_p		varchar2,
		ie_exibir_suspensos_p		varchar2,
		ie_agrupar_acm_sn_p		varchar2,
		ie_prescr_setor_p		varchar2,
		cd_setor_paciente_p		number) is

nr_seq_wadep_w		number(10,0);
nr_prescricao_w		number(14,0);
nr_seq_gasoterapia_w	number(10,0);
nr_seq_gas_w		number(10,0);
ds_gas_w		varchar2(255);
ie_acm_sn_w		varchar2(1);
cd_intervalo_w		varchar2(7);
qt_gasoterapia_w	number(8,3);
ds_prescricao_w		varchar2(100);
ds_config_gas_w		varchar2(255);
ie_lib_pend_rep_w	varchar2(1);
ie_status_gas_w		varchar2(10);
					
cursor c01 is
select	--decode(ie_lib_pend_rep, null, decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_gasoterapia,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), null), nr_prescricao),
	--decode(ie_lib_pend_rep, null, decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_gasoterapia,ie_agrupar_acm_sn_p), 'N', nr_seq_gasoterapia, null), null), nr_seq_gasoterapia),
	decode(ie_lib_pend_rep, null, decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_gasoterapia,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), nr_prescricao), nr_prescricao),
	decode(ie_lib_pend_rep, null, decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_gasoterapia,ie_agrupar_acm_sn_p), 'N', nr_seq_gasoterapia, null), nr_seq_gasoterapia), nr_seq_gasoterapia),
	nr_seq_gas,
	ds_gas,
	ie_acm_sn,	
	cd_intervalo,
	qt_gasoterapia,
	ds_prescricao,
	ds_config_gas,
	ie_lib_pend_rep,
	ie_status_item
from	(
	select	a.nr_prescricao,
		x.nr_sequencia nr_seq_gasoterapia,
		x.nr_seq_gas,
		y.ds_gas,
		'N' ie_acm_sn,		
		x.cd_intervalo,
		x.qt_gasoterapia,
		x.qt_gasoterapia || ' ' || w.ds_valor_dominio || ' ' || z.ds_intervalo ds_prescricao,
		substr(adep_obter_config_gas(x.cd_modalidade_vent,x.ie_respiracao,x.ie_modo_adm),1,255) ds_config_gas,
		decode(a.dt_liberacao,null,substr(adep_obter_lib_pend_rep_gestao(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1),null) ie_lib_pend_rep,
		substr(Obter_Status_Gasoterapia(x.nr_sequencia, 'C'),1,10) ie_status_item
	from	intervalo_prescricao z,
		valor_dominio w,
		gas y,
		prescr_gasoterapia x,
		prescr_medica a
	where	w.cd_dominio(+) = 1580
	and	w.vl_dominio(+) = x.ie_unidade_medida
	and	z.cd_intervalo(+) = x.cd_intervalo
	and	y.nr_sequencia = x.nr_seq_gas
	and	x.nr_prescricao = a.nr_prescricao
	and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_p) = 'S'
	and	((x.dt_prev_execucao is null) or (x.dt_prev_execucao between dt_inicial_horarios_p and dt_final_horarios_p))
	and	((ie_regra_inclusao_p = 'S') or
		 ((ie_regra_inclusao_p = 'R') and (adep_obter_regra_inclusao(	'OX', 
																		cd_estabelecimento_p, 
																		cd_setor_usuario_p, 
																		cd_perfil_p, 
																		null, 
																		null, 
																		null, 
																		null,
																		a.cd_Setor_atendimento,
																		null,
																		null, -- adep_obter_regra_inclusao
																		null) = 'S')))	 -- nr_seq_exame_p
	and	((ie_exibir_hor_realizados_p = 'S') or ((ie_exibir_hor_realizados_p = 'N') and nvl(substr(Obter_Status_Gasoterapia(x.nr_sequencia, 'C'),1,10),'N') <> 'T'))
	and	((ie_exibir_hor_suspensos_p = 'S') or ((ie_exibir_hor_suspensos_p = 'N') and nvl(substr(Obter_Status_Gasoterapia(x.nr_sequencia, 'C'),1,10),'N') <> 'S'))
	and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = cd_setor_paciente_p)))	
	group by
		a.nr_prescricao,
		x.nr_sequencia,
		x.nr_seq_gas,
		y.ds_gas,
		'N',
		x.cd_intervalo,
		x.qt_gasoterapia,
		w.ds_valor_dominio,
		z.ds_intervalo,
		x.cd_modalidade_vent,
		x.ie_respiracao,
		x.ie_modo_adm,
		a.dt_liberacao_medico,
		a.dt_liberacao,
		a.dt_liberacao_farmacia,
		substr(Obter_Status_Gasoterapia(x.nr_sequencia, 'C'),1,10)
	)
group by
	--decode(ie_lib_pend_rep, null, decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_gasoterapia,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), null), nr_prescricao),
	--decode(ie_lib_pend_rep, null, decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_gasoterapia,ie_agrupar_acm_sn_p), 'N', nr_seq_gasoterapia, null), null), nr_seq_gasoterapia),
	decode(ie_lib_pend_rep, null, decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_gasoterapia,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), nr_prescricao), nr_prescricao),
	decode(ie_lib_pend_rep, null, decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_gasoterapia,ie_agrupar_acm_sn_p), 'N', nr_seq_gasoterapia, null), nr_seq_gasoterapia), nr_seq_gasoterapia),
	nr_seq_gas,
	ds_gas,
	ie_acm_sn,	
	cd_intervalo,
	qt_gasoterapia,
	ds_prescricao,
	ds_config_gas,
	ie_lib_pend_rep,
	ie_status_item;
	
begin
open c01;
loop
fetch c01 into	nr_prescricao_w,
		nr_seq_gasoterapia_w,
		nr_seq_gas_w,
		ds_gas_w,
		ie_acm_sn_w,		
		cd_intervalo_w,
		qt_gasoterapia_w,
		ds_prescricao_w,
		ds_config_gas_w,
		ie_lib_pend_rep_w,
		ie_status_gas_w;
exit when c01%notfound;
	begin
	select	w_adep_t_seq.nextval
	into	nr_seq_wadep_w
	from	dual;
	
	insert into w_adep_t (
		nr_sequencia,
		nm_usuario,
		ie_tipo_item,
		nr_prescricao,
		nr_seq_item,		
		cd_item,
		ds_item,
		ie_acm_sn,		
		cd_intervalo,
		qt_item,
		ds_prescricao,
		ds_diluicao,
		nr_agrupamento,
		ie_diferenciado,
		ie_pendente_liberacao,
		ie_status_item)
	values (
		nr_seq_wadep_w,
		nm_usuario_p,
		'O',
		nr_prescricao_w,
		nr_seq_gasoterapia_w,
		nr_seq_gas_w,
		ds_gas_w,
		ie_acm_sn_w,
		cd_intervalo_w,
		qt_gasoterapia_w,
		ds_prescricao_w,
		ds_config_gas_w,
		0,
		'N',
		ie_lib_pend_rep_w,
		ie_status_gas_w);
	end;
end loop;
close c01;
end adep_obter_gas;
/
