create or replace
procedure softlab_ws_consistir_result (	nr_prescricao_p		number,
				nr_seq_prescr_p		number,
				cd_exame_p		varchar2,
				cd_material_exame_p	varchar2,
				cd_erro_p		out varchar2,
				ds_erro_p		out varchar2) is 

nr_seq_exame_w	exame_laboratorio.nr_seq_exame%type;
ie_existe_w		varchar2(1);										
										
begin
cd_erro_p	:= 0;
ds_erro_p	:= null;

ie_existe_w	:= 'S';

if		(nr_prescricao_p is null) then	
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278906,null);
	cd_erro_p	:= 412;
elsif	(nr_seq_prescr_p is null) then	
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280576,null);
	cd_erro_p	:= 412;
elsif	(cd_exame_p is null) then	
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280578,null);
	cd_erro_p	:= 412;
else	
	
	select	max(a.nr_seq_exame)
	into	nr_seq_exame_w
	from	lab_exame_equip a,
			equipamento_lab b
	where	a.cd_equipamento = b.cd_equipamento
	and		b.ds_sigla = 'SOFTLABWS'
	and		a.cd_exame_equip = cd_exame_p;

	if	(nr_seq_exame_w is null) then		
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280581,null)||cd_exame_p;
		cd_erro_p	:= 412;
	else			
		/*select	decode(count(*),0,'N','S')
		into	ie_existe_w
		from	prescr_procedimento a
		where	a.nr_prescricao = nr_prescricao_p
		and		a.nr_sequencia = nr_seq_prescr_p
		and		nr_seq_exame = nr_seq_exame_w;*/
		
		SELECT	DECODE(COUNT(*),0,'N','S')
		INTO	ie_existe_w
		FROM	prescr_procedimento a,
			lab_exame_equip b,
			equipamento_lab c
		WHERE	a.nr_seq_exame = b.nr_seq_exame
		and	b.cd_equipamento = c.cd_equipamento
		and	c.ds_sigla = 'SOFTLABWS'
		and	a.nr_prescricao = nr_prescricao_p
		AND	a.nr_sequencia = nr_seq_prescr_p		
		and	b.cd_exame_equip = cd_exame_p;
		
		
		if	(ie_existe_w = 'N') then		
			ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(280582,'NR_PRESCRICAO='||to_char(nr_prescricao_p)||';NR_SEQ_PRESCR='||to_char(nr_seq_prescr_p)||';CD_EXAME='||cd_exame_p);
			cd_erro_p	:= 411;
		end if;
	end if;	
end if;

/*if	(ie_existe_w = 'N') then
	cd_erro_p	:= 412;
	ds_erro_p	:= 'C�digo de procedimento n�o reconhecido (Problema de mapeamento) - '||ds_erro_p;
end if;*/

commit;

end softlab_ws_consistir_result;
/