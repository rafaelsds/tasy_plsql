create or replace procedure verifica_lote_negativo  ( nr_seq_nf_entrada_p number) is 

qt_compra_w number;

nr_item_nf_w            nota_fiscal_item.nr_item_nf%type;
nr_seq_lote_fornec_w    material_lote_fornec.ds_lote_fornec%type;

cursor c01 is
select  a.nr_item_nf,
	b.nr_seq_lote_fornec
from	nota_fiscal_item a,
	nota_fiscal_item_lote b,
	material_lote_fornec c
where	a.nr_sequencia		= b.nr_seq_nota
and 	a.nr_item_nf		= b.nr_item_nf
and	b.nr_seq_lote_fornec	= c.nr_sequencia
and 	b.nr_item_nf		= c.nr_item_nf
and	a.nr_sequencia		= nr_seq_nf_entrada_p;

begin

open c01;
loop
fetch c01 into
	nr_item_nf_w,
	nr_seq_lote_fornec_w;
exit when c01%notfound;
	begin
	
	select obter_saldo_lote_fornec(nr_seq_lote_fornec_w)
	into qt_compra_w
	from dual;

	if (qt_compra_w < 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1100619, 'NR_ITEM_NF_W='||nr_item_nf_w||';NR_SEQ_LOTE_FORNEC_W='||nr_seq_lote_fornec_w);			
	end if;
	
	end;
end loop;
close c01;

end verifica_lote_negativo;
/