create or replace
procedure desfazer_lib_repasse_parcial	(nr_seq_proc_repasse_p	number,
				nr_seq_mat_repasse_p	number) is
						
nr_seq_material_w			number(15);
nr_seq_procedimento_w		number(15);
vl_original_repasse_w		number(15,4);
						
cursor	c01 is
select	a.nr_sequencia
from	procedimento_repasse a
where	a.nr_seq_origem	= nr_seq_proc_repasse_p;

cursor c02 is
select	b.nr_sequencia
from	material_repasse b
where	b.nr_seq_origem	= nr_seq_mat_repasse_p;

begin

if	(nvl(nr_seq_proc_repasse_p,0) > 0) then

	select	max(a.vl_original_repasse)
	into	vl_original_repasse_w
	from	procedimento_repasse a
	where	a.nr_sequencia	= nr_seq_proc_repasse_p;

	if	(vl_original_repasse_w is null) then
		--r.aise_application_error(-20011, 'Valor original n�o encontrado!');
		wheb_mensagem_pck.exibir_mensagem_abort(266523);
	end if;
	
	update	procedimento_repasse
	set	ie_status			= 'A',
		vl_repasse		= vl_original_repasse_w,
		vl_liberado		= 0,
		dt_liberacao		= null,
		nr_seq_ret_glosa		= null,
		nr_seq_item_retorno	= null
	where	nr_sequencia		= nr_seq_proc_repasse_p;
	
	open c01;
	loop
	fetch c01 into
		nr_seq_procedimento_w;
	exit when c01%notfound;
	
		delete	from procedimento_repasse
		where	nr_sequencia	= nr_seq_procedimento_w;
	
	end loop;
	close c01;
	
elsif	(nvl(nr_seq_mat_repasse_p,0) > 0) then

	select	max(a.vl_original_repasse)
	into	vl_original_repasse_w
	from	material_repasse a
	where	a.nr_sequencia	= nr_seq_mat_repasse_p;

	if	(vl_original_repasse_w is null) then
		--r.aise_application_error(-20011, 'Valor original n�o encontrado!');
		wheb_mensagem_pck.exibir_mensagem_abort(266523);
	end if;
	
	update	material_repasse
	set	ie_status			= 'A',
		vl_repasse		= vl_original_repasse_w,
		vl_liberado		= 0,
		dt_liberacao		= null,
		nr_seq_ret_glosa		= null,
		nr_seq_item_retorno	= null
	where	nr_sequencia		= nr_seq_mat_repasse_p;
	
	open c02;
	loop
	fetch c02 into
		nr_seq_material_w;
	exit when c02%notfound;
	
		delete	from material_repasse
		where	nr_sequencia	= nr_seq_material_w;
	
	end loop;
	close c02;

end if;

end	desfazer_lib_repasse_parcial;
/