create or replace
procedure far_atualizar_total_item(	nr_seq_item_p	number,
					nm_usuario_p	varchar2) is

vl_total_trib_w		far_venda_item.vl_liquido%type;
vl_total_w		far_venda_item.vl_total%type;
vl_desconto_w		far_venda_item.vl_desconto%type;
nr_seq_venda_w		far_venda.nr_sequencia%type;

begin

if	(nr_seq_item_p is not null) then

	select	max(nr_seq_venda) nr_seq_venda
	into	nr_seq_venda_w
	from	far_venda_item
	where	nr_sequencia = nr_seq_item_p;

	-- TEM QUE CHAMAR PARA C�LCULAR O IMPOSTO DO ITEM
	far_gerar_tributo_item(nr_seq_venda_w,nr_seq_item_p,nm_usuario_p);

	select	nvl(sum(decode(b.ie_soma_diminui,'S',a.vl_tributo,'D',a.vl_tributo*-1,0)),0) vl_tributo
	into	vl_total_trib_w
	from	far_venda_item_trib a,
		tributo b
	where	a.cd_tributo = b.cd_tributo
	and	a.nr_seq_item = nr_seq_item_p;

	select	(qt_material * vl_unitario) vl_total, -- ANTES BUSCAVA PELO VALOR TOTAL (VL_TOTAL), ONDE J� EST� DIMINUIDO O VALOR DO DESCONTO, COM ISSO CALCULANDO DUAS VEZES O DESCONTO
			vl_desconto
	into	vl_total_w,
		vl_desconto_w
	from	far_venda_item
	where	nr_sequencia = nr_seq_item_p;

	update	far_venda_item
	set	vl_liquido = vl_total_w + vl_total_trib_w - vl_desconto_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_seq_item_p;

end if;

end far_atualizar_total_item;
/