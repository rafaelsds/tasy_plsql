create or replace procedure afip_atualizar_exames_anatomo (nr_prescricao_p 		number,
						nr_Seq_prescr_p 		number,
						nr_seq_result_pdf_p	out number,
						nm_usuario_p 		varchar2) is


ie_formato_texto_w  	result_laboratorio.ie_formato_texto%type;
nr_prescricao_w			prescr_procedimento.nr_prescricao%type;
nr_seq_result_lab_w		result_laboratorio.nr_sequencia%type;
cd_procedimento_w 		procedimento.cd_procedimento%type;
ie_origem_proced_w		procedimento.ie_origem_proced%type;
cd_estabelecimento_w	prescr_medica.cd_estabelecimento%type;
nr_seq_exame_w			prescr_procedimento.nr_seq_exame%type;
nr_seq_result_pdf_w		result_laboratorio_pdf.nr_sequencia%type;


begin
if (nr_prescricao_p is not null) and
   (nr_seq_prescr_p is not null) then

	select  MAX(ie_formato_texto),
			MAX(nr_prescricao),
			MAX(nr_sequencia)
	into	ie_formato_texto_w,
			nr_prescricao_w,
			nr_seq_result_lab_w
	from    result_laboratorio
	where   nr_prescricao = nr_prescricao_p
	and		nr_seq_prescricao =nr_seq_prescr_p;

	select	cd_procedimento,
			ie_origem_proced,
			nr_seq_exame
	into	cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_exame_w
	from	prescr_procedimento
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_sequencia  = nr_seq_prescr_p;

	select	max(nvl(cd_estabelecimento,1))
	into	cd_estabelecimento_w
	from	prescr_medica
	where 	nr_prescricao = nr_prescricao_p;


	if (nr_prescricao_w is null) then

		insert into result_laboratorio 	(nr_prescricao, nr_seq_prescricao,
									nm_usuario, ie_cobranca, ie_origem_proced,
									cd_procedimento,ds_resultado, dt_coleta,
									ie_formato_texto, nr_seq_exame, ie_final)
		values 	  					(nr_prescricao_p,nr_seq_prescr_p,
									nm_usuario_p, 'N', ie_origem_proced_w,
									cd_procedimento_w, 'Resultado PDF', null,
									3, nr_seq_exame_w, null);

		commit;


		select  MAX(nr_sequencia)
		into	nr_seq_result_lab_w
		from    result_laboratorio
		where   nr_prescricao = nr_prescricao_p
		and		nr_seq_prescricao = nr_seq_prescr_p;

	else
		if (ie_formato_texto_w <> 3) then
			update 	result_laboratorio
			set	   	dt_atualizacao = sysdate,
					ie_formato_texto = 3,
					nm_usuario = nm_usuario_p
			where 	nr_prescricao = nr_prescricao_p
			and		nr_seq_prescricao = nr_seq_prescr_p
			and		nr_sequencia = nr_seq_result_lab_w;
		end if;

	end if;

	select result_laboratorio_pdf_seq.nextval
	into nr_seq_result_pdf_w
	from dual;

	insert into result_laboratorio_pdf (	nr_sequencia,
						nr_prescricao,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_prescricao,
						nr_seq_resultado,
						nr_seq_exame,
						ds_pdf_serial
						)
		values			(nr_seq_result_pdf_w,
						nr_prescricao_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_prescr_p,
						nr_seq_result_lab_w,
						nr_seq_exame_w,
						' '
						);



	commit;
end if;

nr_seq_result_pdf_p := nr_seq_result_pdf_w;

end afip_atualizar_exames_anatomo;
/