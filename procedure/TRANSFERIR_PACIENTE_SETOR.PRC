create or replace
procedure transferir_paciente_setor (
		nr_atendimento_p				number,
		cd_setor_atendimento_p			number,
		cd_classif_setor_p				number,
		nr_acompanhantes_p				number,
		cd_tipo_acomodacao_p			number,
		cd_estabelecimento_p			number,
		nr_seq_motivo_transf_p			number,
		dt_entrada_unidade_p			date,
		cd_unidade_basica_p		in out	varchar2,
		cd_unidade_compl_p		in out	varchar2,
		ie_mov_centro_cirurg_p			varchar2,
		ie_setor_sem_acomodacao_p		varchar2,
		ie_ativar_local_destino_p		varchar2,
		ie_isolar_paciente_p			varchar2,
		ds_observacao_p					varchar2,
		ie_trocar_setor_prescricoes_p	varchar2,
		ie_bloquear_p					varchar2,
		ie_restringe_tipo_atend_p		varchar2,
		ie_restringe_tipo_atend_274_p	varchar2,
		ie_encerrar_cig_p				varchar2,
		ie_transf_prescr_pend_p 		varchar2,-- OS 267552
		ds_classif_setores_transf_p		varchar2,-- OS 267552
		ds_classif_setores_origem_p		varchar2,-- OS 267552
		ie_atualizar_turno_prescr_p		varchar2,-- OS 265314
		cd_motivo_permanencia_p			number,
		ds_consist_tipo_acomod_p 	out	varchar2,
		ie_atendimento_disp_p		out	varchar2,
		ie_leito_radioterapia_p		out	varchar2,
		cd_setor_anterior_p			out	number,
		cd_setor_atend_p			out	number,
		ie_permite_inicio_higien_p 	out	varchar2,
		ds_erro_p					out	varchar2,
		nm_usuario_p					varchar2,
		cd_perfil_p						number,
		ie_tipo_movimentacao_lote_p		varchar2,
		ie_gerar_transf_paciente_p		varchar2,
		nr_seq_classif_esp_p in number default null) is

cd_setor_anterior_w		number(5) := 0;
cd_acomodacao_w			varchar2(1);
ie_mov_centro_cirurg_w		varchar2(1);
ie_permite_passagem_w		varchar2(1);
ie_restringe_tipo_w		varchar2(1);
ie_restringe_tipo_274_w		varchar2(1);
ie_leito_radioterapia_w		varchar2(1) := 'N';
ds_erro_w			varchar2(2000);
ie_classif_setor_origem_w	varchar2(1);--OS 267552
ie_classif_setor_transf_w	varchar2(1);--OS 267552
cd_classif_setor_origem_w	number(5);--OS 267552
cd_setor_atend_w		number(5,0);
cd_unidade_basica_w		varchar2(255);
cd_unidade_compl_w		varchar2(255);
qt_minuto_max_w			number(10,0);
qt_min_perm_setor_w		number(20,0);
ie_permite_inicio_higien_w	varchar2(1);
ie_inat_Unid_Agrup_w varchar2(1);
ie_situacao_w		unidade_atendimento.ie_situacao%type;
nr_seq_interno_w	unidade_atendimento.nr_seq_interno%type;
ds_origem_w			varchar2(1800);
ds_unidade_log_w			varchar2(4000);
nr_agrupamento_acom_w		number(5);
cd_unid_comp_acom_w		varchar2(10);
cd_unidade_basica_acom_w	varchar2(10);

cursor c01 is
select	a.nr_atend_acompanhante
		from	episodio_acompanhante a
		where	obter_se_permite_mov_cc('S', a.nr_atend_acompanhante, cd_setor_atendimento_p) = 'S'
		and a.nr_atend_paciente = nr_atendimento_p
		and a.nr_atend_acompanhante in(
					select	b.nr_atendimento
					from	atendimento_paciente b
					where	b.nr_atendimento = a.nr_atend_acompanhante 
					and b.dt_alta is null);

begin
if	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	obter_param_usuario(916, 737, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_inat_Unid_Agrup_w);
	
	ie_permite_passagem_w	:= obter_se_permite_passagem(nr_atendimento_p,dt_entrada_unidade_p,cd_estabelecimento_p);
	
	if	(ie_permite_passagem_w = 'N') then
		begin
		ds_erro_w := wheb_mensagem_pck.get_texto(278988);
		end;
	end if;
	
	if	(ds_erro_w is null) then
		begin
		select	to_number(nvl(obter_unidade_atendimento(nr_atendimento_p,'IAA','CS'),0))
		into	cd_setor_anterior_w
		from	dual; 
	
		--OS 267552
		cd_classif_setor_origem_w := to_number(nvl(obter_classif_setor(cd_setor_anterior_w), 0));
		
		ie_classif_setor_transf_w := obter_se_contido_char(cd_classif_setor_p, ds_classif_setores_transf_p);
		ie_classif_setor_origem_w := obter_se_contido_char(cd_classif_setor_origem_w, ds_classif_setores_origem_p);
	
		begin
		select	obter_se_sem_acomodacao(cd_tipo_acomodacao)
		into	cd_acomodacao_w
		from	atend_paciente_unidade
		where	nr_seq_interno = ( 	
					select	max(nr_seq_interno)
					from	atend_paciente_unidade
					where	nr_atendimento = nr_atendimento_p 
					and 	dt_saida_unidade is null);
		exception
			when others then
			cd_acomodacao_w := 'X';
		end;
		end;
	end if;
	
	if	(ds_erro_w is null) and
		(ie_setor_sem_acomodacao_p = 'N') and
		((cd_acomodacao_w = '') or (cd_acomodacao_w = 'S')) and
		(cd_classif_setor_p in (3,4,8)) then
		begin
		ds_erro_w := wheb_mensagem_pck.get_texto(278989);
		end;		
	end if;
	
	if	(ds_erro_w is null) and
		(ie_mov_centro_cirurg_p = 'S') and
		(obter_se_permite_mov_cc('S',nr_atendimento_p,cd_setor_atendimento_p) = 'N') then
		begin
		ds_erro_w := wheb_mensagem_pck.get_texto(278991);
		end;
	end if;
	
	if	(ds_erro_w is null) and
		(((ie_restringe_tipo_atend_p     is not null) and (ie_restringe_tipo_atend_p     <> '') and (cd_classif_setor_p in (3,4,8))) or
		 ((ie_restringe_tipo_atend_274_p is not null) and (ie_restringe_tipo_atend_274_p <> '') and (cd_classif_setor_p = 4))) then
		begin
		
		select	obter_se_contido_char(ie_tipo_atendimento, ie_restringe_tipo_atend_p),
			obter_se_contido_char(ie_tipo_atendimento, ie_restringe_tipo_atend_274_p)
		into	ie_restringe_tipo_w,
			ie_restringe_tipo_274_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;
		
		if	(ie_restringe_tipo_w = 'S') then			
			ds_erro_w := wheb_mensagem_pck.get_texto(278992);
		elsif (ie_restringe_tipo_274_w = 'S') then
			ds_erro_w := wheb_mensagem_pck.get_texto(278993);		
		end if;
		end;		
	end if;
	
	if 	(ds_erro_w is null) then
		begin
		if	(ie_ativar_local_destino_p = 'S') then
			begin
			inativar_ativar_unidade_atend(cd_setor_atendimento_p,cd_unidade_basica_p,cd_unidade_compl_p,'A',nm_usuario_p);
			end;
		end if;		
		
		cd_setor_atend_w		:= obter_unidade_atendimento(nr_atendimento_p,'A','CS');
		cd_unidade_basica_w		:= obter_unidade_atendimento(nr_atendimento_p,'A','UB');
		cd_unidade_compl_w		:= obter_unidade_atendimento(nr_atendimento_p,'A','UC');
		
		if	(cd_setor_atend_w is not null) then
			begin
						
			consistir_regra_transf_setor(
				'T',
				cd_setor_anterior_w,
				cd_setor_atendimento_p,
				nr_atendimento_p,
				ds_erro_w);
			
				if	(ds_erro_w is null) then
					consistir_transf_setores(
						cd_setor_atend_w,
						cd_setor_atendimento_p,
						cd_estabelecimento_p,
						nr_atendimento_p,
						ds_erro_w);
				end if;
			end;
		end if;
		end;
	end if;
	
	if (ie_inat_Unid_Agrup_w = 'S') then
	select 	max(ie_situacao),
			max(nr_seq_interno)
	into	ie_situacao_w,
			nr_seq_interno_w
	from	unidade_atendimento
	where	cd_setor_atendimento = cd_setor_atendimento_p
	and		cd_unidade_basica	 = cd_unidade_basica_p
	and		cd_unidade_compl	 = cd_unidade_compl_p;
	
	
	ATUALIZAR_UNID_ATEND_SIT_EUP(nr_seq_interno_w, ie_situacao_w, nm_usuario_p, cd_estabelecimento_p, ds_erro_w);
	
	end if;
	
	if	(nvl(ie_gerar_transf_paciente_p,'S') = 'S') then
		begin
	
		if	(ds_erro_w is null) then
			begin
		
			if	(cd_setor_atend_w is not null) then
				begin
				select	qt_minuto_max
				into	qt_minuto_max_w
				from	setor_atendimento
				where	cd_setor_atendimento	= cd_setor_atend_w;
			
				end;
			else
				qt_minuto_max_w	:= 0;
			end if;
		
			if	(qt_minuto_max_w <> 0) then
				begin
				qt_min_perm_setor_w	:= obter_min_permanencia_setor(nr_atendimento_p,cd_setor_atend_w,cd_unidade_basica_w,cd_unidade_compl_w);  
						
				if	(qt_min_perm_setor_w > qt_minuto_max_w) and (cd_motivo_permanencia_p is null) then
					begin
					Wheb_mensagem_pck.exibir_mensagem_abort(198475);
					end;
				end if;
			
				end;
			
			end if;	
		
			if 	(cd_motivo_permanencia_p is not null) then
				begin			
				inserir_motivo_permanencia(
					nm_usuario_p,
					nr_atendimento_p,
					cd_setor_atend_w,
					cd_unidade_basica_w,
					cd_unidade_compl_w,
					cd_motivo_permanencia_p );
				end;
			end if;
			--INICIO Transferencia do acompanhante BDM3 FR6 Austria
			if (Obter_se_leito_livre_acomp(nr_atendimento_p, cd_unidade_basica_p, cd_unidade_compl_p, cd_setor_atendimento_p) = 'S') then
				begin
					for c01_w in c01 loop
						begin
							--Escolha na unidade complementar
							select	max(cd_unidade_compl)
							into	cd_unid_comp_acom_w
							from	unidade_atendimento
							where   cd_unidade_basica	 = cd_unidade_basica_p
							and     cd_unidade_compl	 <> cd_unidade_compl_p
							and	    cd_setor_atendimento = cd_setor_atendimento_p
							and	    ie_status_unidade in ('L');
							
							if	(cd_unid_comp_acom_w is null) then
							begin
								select 	a.nr_agrupamento
								into	nr_agrupamento_acom_w
								from	unidade_atendimento a
								where	a.cd_unidade_basica	= cd_unidade_basica_p
								and a.cd_unidade_compl	= cd_unidade_compl_p
								and	a.cd_setor_atendimento	= cd_setor_atendimento_p;
											
								select	max(cd_unidade_basica)
								into	cd_unidade_basica_acom_w
								from 	unidade_atendimento
								where	cd_unidade_basica	<> cd_unidade_basica_p
								and	nr_agrupamento	= nr_agrupamento_acom_w
								and	cd_setor_atendimento	= cd_setor_atendimento_p
								and ie_status_unidade IN ('L');
										
								gerar_transferencia_paciente(
								c01_w.nr_atend_acompanhante, 
								cd_setor_atendimento_p, 
								cd_unidade_basica_acom_w, 
								cd_unid_comp_acom_w, 
								cd_tipo_acomodacao_p, 
								nr_acompanhantes_p, 
								nr_seq_motivo_transf_p, 
								ds_observacao_p, 
								nm_usuario_p, 
								dt_entrada_unidade_p,
								nr_seq_classif_esp_p);
											
							end;
							end if;
							
							if	(cd_unid_comp_acom_w is not null) then
							begin
											
								gerar_transferencia_paciente(
								c01_w.nr_atend_acompanhante, 
								cd_setor_atendimento_p, 
								cd_unidade_basica_p, 
								cd_unid_comp_acom_w, 
								cd_tipo_acomodacao_p, 
								nr_acompanhantes_p, 
								nr_seq_motivo_transf_p, 
								ds_observacao_p, 
								nm_usuario_p, 
								dt_entrada_unidade_p,
								nr_seq_classif_esp_p);
										
							end;
							
							else
							Wheb_mensagem_pck.exibir_mensagem_abort(261610);
							end if;
						
						end;
					end loop;
				end;
			end if;
			-- FIM Transferencia do acompanhante BDM3 FR6 Austria

			gerar_transferencia_paciente(
				nr_atendimento_p, 
				cd_setor_atendimento_p, 
				cd_unidade_basica_p, 
				cd_unidade_compl_p, 
				cd_tipo_acomodacao_p, 
				nr_acompanhantes_p, 
				nr_seq_motivo_transf_p, 
				ds_observacao_p, 
				nm_usuario_p, 
				dt_entrada_unidade_p,
				nr_seq_classif_esp_p);
				

			begin
			ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
			ds_unidade_log_w := substr(
					'ds_origem_w'||' '||ds_origem_w||' - '
					||' '||'nr_atendimento_p: '||nr_atendimento_p
					||' '||'cd_setor_atendimento_p: '||cd_setor_atendimento_p
					||' '||'cd_unidade_basica_p: '||cd_unidade_basica_p
					||' '||'cd_unidade_compl_p: '||cd_unidade_compl_p
					||' '||'cd_setor_anterior_w: '||cd_setor_anterior_w
					||' '||'cd_setor_atend_w: '||cd_setor_atend_w
					||' '||'cd_estabelecimento_p: '||cd_estabelecimento_p
					||' '||'ie_classif_setor_origem_w: '||ie_classif_setor_origem_w
					||' '||'ie_classif_setor_transf_w: '||ie_classif_setor_transf_w
					||' '||'ie_transf_prescr_pend_p: '||ie_transf_prescr_pend_p
					||' '||'ie_atualizar_turno_prescr_p: '||ie_atualizar_turno_prescr_p
					||' '||'ie_tipo_movimentacao_lote_p: '||ie_tipo_movimentacao_lote_p
					||' '||'dt_entrada_unidade_p: '||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_entrada_unidade_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)
					||' '||'nm_usuario_p: '|| nm_usuario_p
					||' '||'obter_funcao_ativa: '|| obter_funcao_ativa
					||' '||'obter_perfil_ativo: '|| obter_perfil_ativo
					||' '||'obter_usuario_ativo: '|| obter_usuario_ativo
					,1,4000);

			insert 	into log_mov(
				DT_ATUALIZACAO, 
				NM_USUARIO, 
				CD_LOG, 
				DS_LOG
			) values	(
				sysdate, 
				nm_usuario_p, 
				55841,
				ds_unidade_log_w
			);
					 
			exception
			when others then
				null;
			end;
				
			if 	(cd_classif_setor_p in (3,4)) then
				if	(ie_isolar_paciente_p = 'S')  then
					begin
					update	atendimento_paciente
					set 	ie_paciente_isolado = 'S',
						dt_atualizacao = sysdate,
						nm_usuario = nm_usuario_p
					where	nr_atendimento = nr_atendimento_p;
					end;
				end if;
			end if;
				
			if	(ie_classif_setor_origem_w = 'N') and
				(ie_classif_setor_transf_w = 'S') and
				(ie_transf_prescr_pend_p = 'S') then
				begin
				if	(cd_setor_anterior_w > 0) then
					atualizar_setor_prescricao(nr_atendimento_p,cd_setor_atendimento_p,cd_setor_anterior_w,nm_usuario_p);
				else 	
					atualizar_setor_prescricao(nr_atendimento_p,cd_setor_atendimento_p,null,nm_usuario_p);
				end if;
				end;
			end if;
			
		
			if	(ie_atualizar_turno_prescr_p = 'S') then
				begin
				Atualizar_turno_hor_prescr(nr_atendimento_p,cd_setor_atendimento_p,cd_estabelecimento_p,nm_usuario_p);
				end;
			end if;
			
			if (ie_tipo_movimentacao_lote_p = 'S' or
				(ie_tipo_movimentacao_lote_p = 'T' and cd_setor_atend_w <> cd_setor_atendimento_p)) then
				begin
				gerar_ajustes_ap_lote('M',nr_atendimento_p,nm_usuario_p);
				end;
			end if;
		
			if	(ie_bloquear_p = 'S') then
				begin
				consiste_tipo_acomodacao(nr_atendimento_p,ds_consist_tipo_acomod_p);
				end;
			end if;
		
			select	nvl(max(ie_radioterapia),'N')
			into	ie_leito_radioterapia_w
			from	unidade_atendimento
			where	cd_setor_atendimento	= cd_setor_atendimento_p
			and	cd_unidade_basica	= cd_unidade_basica_p
			and	cd_unidade_compl	= cd_unidade_compl_p;
		
			if	(ie_encerrar_cig_p = 'S') and
				(cd_setor_anterior_w <> cd_setor_atendimento_p) then
				begin
				encerar_cig_setor_pac(
					nr_atendimento_p,
					cd_setor_atendimento_p,
					nm_usuario_p);
				end;
			end if;
		
			ie_permite_inicio_higien_w	:= obter_se_regra_lib_setor_higie(cd_setor_anterior_w, cd_perfil_p, nm_usuario_p, 0);
		
			gerar_reconciliacao_paciente(nm_usuario_p, nr_atendimento_p, obter_perfil_ativo, 'T', 'N', null,'N');
		
		
		/*obter_param_usuario(3111, 137, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_aguard_higien_w);
		obter_param_usuario(3111, 167, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_considerar_hig_estr_w);
		
		if	(ie_aguard_higien_w = 'S')then
			begin
			
			ie_controla_hig_w	:= obter_se_higieniza_leito(cd_setor_atend_w,cd_unidade_basica_w,cd_unidade_compl_w);
			
			if	(ie_controla_hig_w = 'H')and
				(ie_considerar_hig_estr_w = 'S')then
				begin
				
				higienizar_js(8, cd_setor_atend_w, cd_unidade_basica_w, cd_unidade_compl_w, '', cd_perfil_p, nm_usuario_p, cd_estabelecimento_p);
				
				end;
			end if;
			
			end;
		end if;*/
		
			end;
		else
			begin
			Wheb_mensagem_pck.exibir_mensagem_abort(214322, 'DS_ERRO=' || ds_erro_w);
			end;
		end if;	
		end;
	end if;
	end;
end if;

ie_leito_radioterapia_p 	:= ie_leito_radioterapia_w;
cd_setor_anterior_p		:= cd_setor_anterior_w;
cd_setor_atend_p		:= cd_setor_atend_w;
cd_unidade_basica_p		:= cd_unidade_basica_w;
cd_unidade_compl_p		:= cd_unidade_compl_w;
ie_permite_inicio_higien_p	:= ie_permite_inicio_higien_w;
ds_erro_p 			:= ds_erro_w;

if (nvl(wheb_usuario_pck.get_ie_commit, 's') = 's') then commit; end if;

end transferir_paciente_setor;
/
 