CREATE OR REPLACE PROCEDURE VERIF_DOSEPRESC_MAX_PROT_INT( nr_seq_paciente_p    Number,
                                                          nr_sequencia_p            number,
                                                          ie_utiliza_horarios_p    varchar2,
                                                          ds_erro_p            out  varchar2,
                                                          ds_erro2_p              out  varchar2,
                                                          ds_mensagem_p          out varchar2,
                                                          cd_material_p            number      default null,
                                                          cd_unid_med_prescr_p       varchar2    default null,
                                                          qt_dose_prescr_p            number    default null,
                                                          ie_via_aplicacao_p          varchar2    default null,
                                                          ds_observacao_p             varchar2   default null,
                                                          ds_ciclos_aplicacao_p       varchar2    default null,
                                                          is_paciente_medic_update   varchar2    default null,
                                                          qt_dias_util_p            number      default null,
                                                          ie_local_adm_p             varchar2    default null,
                                                          ie_uso_continuo_p         varchar2    default null,
                                                          cd_intervalo_p            varchar2    default null,
                                                          qt_dose_p                 number      default null,
                                                          ds_justificativa_p         varchar2    default null,
					                                                cd_unid_med_dose_p 	      varchar2 default null
                                                          ) is


  qt_max_prescricao_w      Number(18,6);
  cd_unidade_medida_consumo_w  Varchar2(30);
  cd_unid_med_limite_w    Varchar2(30);
  qt_limite_pessoa_w      Number(18,6);
  qt_conversao_dose_w      Number(18,6);
  qt_conversao_dose_limite_w  Number(18,6);
  qt_dose_w          Number(18,6);
  qt_dose_ww          Number(18,6);
  qt_dose_limite_w      Number(18,6);
  cd_unidade_medida_dose_w  Varchar2(30);
  cd_pessoa_fisica_w      Varchar2(30);
  cd_material_w        Number(6);
  ie_dose_limite_w      Varchar2(15);
  nr_ocorrencia_w        Number(15,4);
  ie_via_aplicacao_w      varchar2(5);
  ie_justificativa_w      varchar2(5);
  ds_justificativa_w      varchar2(2000);
  cd_prescritor_w        varchar2(50);
  cd_setor_atendimento_w    number(5,0);
  qt_regra_w          number(10,0);
  qt_idade_w          number(10,0);
  qt_peso_w          number(6,3);
  qt_limite_peso_w      number(18,6);
  nr_seq_paciente_w      number(10,0);
  qt_sc_w            number(10,2) := 0;
  qt_altura_w          number(10,2);
  nr_ciclos_w          number(10);
  cd_protocolo_w        number(10);
  nr_seq_medicacao_w      number(6);
  ds_mensagem_regra_w      varchar2(255);
  qtd_dias_aplicacao_w      number(10);
  ds_dias_aplicacao_w      varchar2(2000);
  dt_cancelamento_w           paciente_atendimento.dt_cancelamento%type;
  ie_dose_cumulativa_w   varchar2(1);
  valida_comulativa_w    varchar2(1);
  ds_mensagem_w          varchar2(32000);
  cd_perfil_w		         number(10);
  nr_seq_erro_w			        number(10);
  cd_intervalo_adm_w        varchar(50);
  qt_adminstracao_w         Number(15,4);
  cd_unidade_cumulativa_w   varchar2(10); 
  dose_cumulativa_w         number(15, 6); 
 
cursor c01 is
  select nvl(qt_limite_pessoa, 0),
         nvl(ie_dose_limite, 'DOSE'),
         cd_unid_med_limite,
         nvl(ie_justificativa, 'S'),
         ds_mensagem_regra,
         ie_dose_cumulativa,
         dose_cumulativa,
         cd_unidade_cumulativa
    from material_prescr
   where cd_material = cd_material_w
     and nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w, 0)) =
         nvl(cd_setor_atendimento_w, 0)
     and nvl(ie_via_aplicacao, nvl(ie_via_aplicacao_w, 0)) =
         nvl(ie_via_aplicacao_w, 0)
     and (qt_limite_pessoa is not null or dose_cumulativa is not null)
     and nvl(qt_idade_w, 1) between
         nvl(Obter_idade_param_prescr(nr_sequencia, 'MIN'), 0) and
         nvl(Obter_idade_param_prescr(nr_sequencia, 'MAX'), 9999999)
     and qt_peso_w between nvl(qt_peso_min, 0) and nvl(qt_peso_max, 999)
     and nvl(cd_protocolo, nvl(cd_protocolo_w, 0)) = nvl(cd_protocolo_w, 0)
     and nvl(nr_seq_medicacao, nvl(nr_seq_medicacao_w, 0)) =
         nvl(nr_seq_medicacao_w, 0)
     and ie_tipo = '2'
     and ((cd_especialidade is null) or
         (obter_se_especialidade_medico(cd_prescritor_w, cd_especialidade) = 'S'))
     and (nvl(ie_tipo_item, 'TOD') in ('OUT', 'TOD'))
     and ((cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento) OR
         (cd_estabelecimento IS NULL))
   order by nr_sequencia;

BEGIN
  ds_erro_p := '';
  ds_erro2_p := '';
  cd_perfil_w := obter_perfil_ativo;

if('S' = is_paciente_medic_update) then

  cd_material_w := cd_material_p;
  cd_unidade_medida_dose_w := cd_unid_med_prescr_p;
  qt_dose_ww := QT_DOSE_PRESCR_P;
  nr_ocorrencia_w := null;
  ie_via_aplicacao_w := ie_via_aplicacao_p;
  ds_justificativa_w := ds_justificativa_p;
  cd_intervalo_adm_w := cd_intervalo_p;

  select nvl(length(DS_CICLOS_APLICACAO_P) -
             length(replace(DS_CICLOS_APLICACAO_P, 'C')),
             1)
    into nr_ciclos_w
    from dual;

else

  select cd_material,
         cd_unid_med_prescr,
         qt_dose_prescr,
         null,
         ie_via_aplicacao,
         ds_observacao,
         nvl(length(DS_CICLOS_APLICACAO) -
             length(replace(DS_CICLOS_APLICACAO, 'C')),
             1),
         cd_intervalo
    into cd_material_w,
         cd_unidade_medida_dose_w,
         qt_dose_ww,
         nr_ocorrencia_w,
         ie_via_aplicacao_w,
         ds_justificativa_w,
         nr_ciclos_w,
         cd_intervalo_adm_w
    from paciente_protocolo_medic
   where nr_seq_paciente = nr_seq_paciente_p
     and nr_seq_material = nr_sequencia_p;
   
end if;

if(cd_intervalo_adm_w is not null)then
  select qt_operacao
    into qt_adminstracao_w
    from intervalo_prescricao
   where CD_INTERVALO = cd_intervalo_adm_w;
end if;

select nvl(qt_peso, 0),
       nvl(qt_altura, 0),
       nvl(round(obter_superficie_corp_red_ped(qt_peso,
                                               qt_altura,
                                               qt_redutor_sc,
                                               cd_pessoa_fisica,
                                               nm_usuario,
                                               IE_FORMULA_SUP_CORPOREA),
                 obter_numero_casas_sc),
           0)
  into qt_peso_w, qt_altura_w, qt_sc_w
  from paciente_setor
 where nr_seq_paciente = nr_seq_paciente_p;

select cd_medico_resp,
       cd_setor_atendimento,
       cd_pessoa_fisica,
       cd_protocolo,
       nr_seq_medicacao
  into cd_prescritor_w,
       cd_setor_atendimento_w,
       cd_pessoa_fisica_w,
       cd_protocolo_w,
       nr_seq_medicacao_w
  from paciente_setor
 where nr_seq_paciente = nr_seq_paciente_p;

select max(obter_idade(dt_nascimento, nvl(dt_obito, sysdate), 'DIA'))
  into qt_idade_w
  from pessoa_fisica
 where cd_pessoa_fisica = cd_pessoa_fisica_w;

if  (nvl(cd_material_w,0) > 0) and
  (cd_unidade_medida_dose_w is not null) and
  (nvl(qt_dose_ww,0) > 0) then
  begin

  Select count(*)
    into qt_regra_w
    from material
   where cd_material = cd_material_w
     and qt_limite_pessoa is not null;
  
  if  (qt_regra_w > 0) then
    begin
      Select nvl(qt_max_prescricao, 0),
             cd_unidade_medida_consumo,
             cd_unid_med_limite,
             nvl(qt_limite_pessoa, 0),
             nvl(ie_dose_limite, 'DOSE')
        into qt_max_prescricao_w,
             cd_unidade_medida_consumo_w,
             cd_unid_med_limite_w,
             qt_limite_pessoa_w,
             ie_dose_limite_w
        from material
       where cd_material = cd_material_w;
            /* Pega o setor de atendimento da prescricao e a idade do paciente */

      if  (cd_unidade_medida_consumo_w = cd_unidade_medida_dose_w) then
        qt_conversao_dose_w  := 1;
      else
        begin
        select  nvl(max(qt_conversao),1)
        into  qt_conversao_dose_w
        from  material_conversao_unidade
        where  cd_material    = cd_material_w
        and  cd_unidade_medida  = cd_unidade_medida_dose_w;
        exception
          when others then
          qt_conversao_dose_w  := 1;
        end;
      end if;

      qt_dose_w  := (trunc(qt_dose_ww * 1000 / qt_conversao_dose_w)/ 1000);

      if (qt_max_prescricao_w > 0) and (qt_max_prescricao_w < nvl(qt_dose_w,0) or 
          (qt_max_prescricao_w <= nvl(qt_dose_w,0) and ie_dose_limite_w = 'CA')) then
        ds_erro_p  := wheb_mensagem_pck.get_texto(277286, 'QT_MAX_PRESCRICAO_P=' || qt_max_prescricao_w || 
                      ';CD_UNIDADE_MEDIDA_CONSUMO_P=' || cd_unidade_medida_consumo_w);
      end if;

      if  (cd_unidade_medida_consumo_w = cd_unid_med_limite_w) then
        qt_conversao_dose_limite_w  := 1;
      else
        begin
        select  nvl(max(qt_conversao),1)
        into  qt_conversao_dose_limite_w
        from  material_conversao_unidade
        where  cd_material    = cd_material_w
        and  cd_unidade_medida  = cd_unid_med_limite_w;
        exception
          when others then
            qt_conversao_dose_limite_w  := 1;
        end;
      end if;

      qt_dose_w    := (trunc(dividir(qt_dose_ww * 1000, qt_conversao_dose_w))/ 1000);
      qt_dose_limite_w  := (trunc(dividir(qt_limite_pessoa_w * 1000, qt_conversao_dose_limite_w))/ 1000);

      if  (ie_dose_limite_w = 'DIA') then
        begin          
        qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);          
        end;

      elsif  (ie_dose_limite_w = 'AT') then
        begin      
        qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);
        end;

      elsif  (ie_dose_limite_w = 'PF') then
        begin
        qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1); /* Pessoa Fisica*/
        end;

      elsif  (qt_peso_w > 0) and 
        (ie_dose_limite_w = 'KG/DIA') then
        begin    
        qt_dose_w  := qt_dose_w / nvl(qt_peso_w,1);/*por KG*/ /*por dia*/  
        qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);    
        end;

      elsif  (qt_sc_w > 0) and
        (ie_dose_limite_w = 'SC') then
        begin
        qt_dose_w  := qt_dose_w / nvl(qt_sc_w,1);  /*por SC*/
        qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);
        end;

      elsif  (qt_peso_w > 0) and 
             (ie_dose_limite_w = 'CI') then
        begin /*por Ciclo*/ 

          select ds_dias_aplicacao
            into ds_dias_aplicacao_w
            from paciente_protocolo_medic
           where nr_seq_paciente = nr_seq_paciente_p
             and cd_material = cd_material_w;

          qtd_dias_aplicacao_w := (length(ds_dias_aplicacao_w) - length(replace(ds_dias_aplicacao_w, 'D')));
          
          qt_dose_w  := qt_dose_w * nvl(qtd_dias_aplicacao_w,1);
          end;

      elsif  (qt_peso_w > 0) and 
        (ie_dose_limite_w = 'CA') then
        begin    /*por Ciclo agrupado*/ -- Total do paciente  em todos os protocolos
			    qt_dose_w   :=  nvl(obter_dose_total_medic_pf_onc(cd_pessoa_fisica_w, cd_material_w),0);
        end;

      elsif  (qt_peso_w > 0) and 
        (ie_dose_limite_w = 'KG/D') then
        begin      
        qt_dose_w  := qt_dose_w / nvl(qt_peso_w,1);/*por KG*/ /*por dose*/
        qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);    
        end;

      end if;

    if (qt_dose_limite_w > 0) and (qt_dose_limite_w < nvl(qt_dose_w,0) or
        (qt_dose_limite_w <= nvl(qt_dose_w,0) and ie_dose_limite_w = 'CA')) then
      ds_erro2_p  := wheb_mensagem_pck.get_texto(277256, 'QT_LIMITE_PESSOA_P=' || qt_limite_pessoa_w || 
                    ';CD_UNID_MED_LIMITE_P=' || cd_unid_med_limite_w || 
                    ';DS_DOSE_LIMITE_P=' || lower(ie_dose_limite_w));
    end if;
    end;
  else  
    begin

    /* Verifica se tem alguma regra para os dados informados */
    select count(*)
      into qt_regra_w
      from material_prescr
     where cd_material = cd_material_w
       and nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w, 0)) =
           nvl(cd_setor_atendimento_w, 0)
       and nvl(ie_via_aplicacao, nvl(ie_via_aplicacao_w, 0)) =
           nvl(ie_via_aplicacao_w, 0)
       and nvl(qt_idade_w, 1) between
           nvl(Obter_idade_param_prescr(nr_sequencia, 'MIN'), 0) and
           nvl(Obter_idade_param_prescr(nr_sequencia, 'MAX'), 9999999)
       and qt_peso_w between nvl(qt_peso_min, 0) and nvl(qt_peso_max, 999)
       and (qt_limite_pessoa is not null or dose_cumulativa is not null)
       and ((cd_especialidade is null) or
           (obter_se_especialidade_medico(cd_prescritor_w,
                                           cd_especialidade) = 'S'))
       and (nvl(ie_tipo_item, 'TOD') in ('OUT', 'TOD'))
       and ie_tipo = '2';
    
    /* Caso haja alguma regra, faz as consistencias */
    if  (qt_regra_w > 0) then
      open c01;
      loop
      fetch c01 into
        qt_limite_pessoa_w,
        ie_dose_limite_w,
        cd_unid_med_limite_w,
        ie_justificativa_w,
        ds_mensagem_regra_w,
        ie_dose_cumulativa_w,
         dose_cumulativa_w,
         cd_unidade_cumulativa_w;
      exit when c01%notfound;
        begin

        select nvl(qt_max_prescricao, 0), cd_unidade_medida_consumo
          into qt_max_prescricao_w, cd_unidade_medida_consumo_w
          from material
         where cd_material = cd_material_w;

        if  (cd_unidade_medida_consumo_w = cd_unidade_medida_dose_w) then
          qt_conversao_dose_w  := 1;
        else
          begin
          select nvl(max(qt_conversao), 1)
            into qt_conversao_dose_w
            from material_conversao_unidade
           where cd_material = cd_material_w
             and cd_unidade_medida = cd_unidade_medida_dose_w;
          exception  
            when others then
              qt_conversao_dose_w  := 1;
          end;
        end if;

        qt_dose_w  := (trunc(qt_dose_ww * 1000 / qt_conversao_dose_w)/ 1000);

        if (qt_max_prescricao_w > 0) and (qt_max_prescricao_w < nvl(qt_dose_w,0) or 
            (qt_max_prescricao_w <= nvl(qt_dose_w,0) and ie_dose_limite_w = 'CA')) then
          ds_erro_p  := wheb_mensagem_pck.get_texto(277286, 'QT_MAX_PRESCRICAO_P=' || qt_max_prescricao_w || 
                        ';CD_UNIDADE_MEDIDA_CONSUMO_P=' || cd_unidade_medida_consumo_w);
        end if;

        if  (cd_unidade_medida_consumo_w = cd_unid_med_limite_w) then
          qt_conversao_dose_limite_w  := 1;
        else
          begin
          select nvl(max(qt_conversao), 1)
            into qt_conversao_dose_limite_w
            from material_conversao_unidade
           where cd_material = cd_material_w
             and cd_unidade_medida = cd_unid_med_limite_w;
          exception
            when others then
              qt_conversao_dose_limite_w  := 1;
          end;
        end if;

        qt_dose_w    := (trunc(qt_dose_ww * 1000 / qt_conversao_dose_w)/ 1000);
        qt_dose_limite_w  := (trunc(qt_limite_pessoa_w * 1000 / qt_conversao_dose_limite_w)/ 1000);

        if (ie_dose_cumulativa_w = 'S') then
          if ('S' = valida_dose_comulativa(cd_material_w, 
                                           qt_dose_ww,
                                           cd_unidade_medida_dose_w, 
                                           cd_pessoa_fisica_w, 
                                           nvl(qt_adminstracao_w,1),
                                           dose_cumulativa_w,
                                           cd_unidade_cumulativa_w,
                                           ds_mensagem_w)) then
		        gerar_erro_pac_prot(nr_seq_paciente_p,nr_sequencia_p,1,null,cd_perfil_w,wheb_usuario_pck.get_nm_usuario,nr_seq_erro_w, ds_mensagem_w);
          end if;
        end if;
              
        if  (ie_dose_limite_w = 'DIA') then

          qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);

        elsif  (qt_peso_w > 0) and 
          (ie_dose_limite_w = 'KG') then
          qt_dose_w  := qt_dose_w / nvl(qt_peso_w,1);
        elsif  (ie_dose_limite_w = 'AT') then
          begin
          qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);
          end;
        elsif  (qt_sc_w >0) and
          (ie_dose_limite_w = 'SC') then
          begin

          qt_dose_w  := qt_dose_w / nvl(qt_sc_w,1);  /*por SC*/
          qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);
          end;

        elsif  (qt_peso_w > 0) and 
              (ie_dose_limite_w = 'CI') then
          begin /*por Ciclo*/ 

          select ds_dias_aplicacao
            into ds_dias_aplicacao_w
            from paciente_protocolo_medic
           where nr_seq_paciente = nr_seq_paciente_p
             and cd_material = cd_material_w;

          qtd_dias_aplicacao_w := (length(ds_dias_aplicacao_w) - length(replace(ds_dias_aplicacao_w, 'D')));

          qt_dose_w  := qt_dose_w * nvl(qtd_dias_aplicacao_w,1);
          end;

       elsif  (qt_peso_w > 0) and 
             (ie_dose_limite_w = 'CA') then
        begin    /*por Ciclo agrupado*/ -- Total do paciente  em todos os protocolos
			    qt_dose_w   :=  nvl(obter_dose_total_medic_pf_onc(cd_pessoa_fisica_w, cd_material_w),0);
        end;

        elsif  (ie_dose_limite_w = 'PF') then
          begin            
          qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1); /* Pessoa Fisica*/
          end;

        elsif  (qt_peso_w > 0) and 
          (ie_dose_limite_w = 'KG/DIA') then
          begin
          /*por KG*/
          qt_dose_w  := qt_dose_w / nvl(qt_peso_w,1);          
          qt_dose_w  := qt_dose_w * nvl(nr_ocorrencia_w,1);
          end;

        end if;

        if (qt_dose_limite_w > 0) and (qt_dose_limite_w < nvl(qt_dose_w,0) or
            (qt_dose_limite_w <= nvl(qt_dose_w,0) and ie_dose_limite_w = 'CA')) and
            ((ie_justificativa_w = 'S') or (ds_justificativa_w is null)) then
          ds_erro2_p  := wheb_mensagem_pck.get_texto(277256, 'QT_LIMITE_PESSOA_P=' || qt_limite_pessoa_w || 
                        ';CD_UNID_MED_LIMITE_P=' || cd_unid_med_limite_w || 
                        ';DS_DOSE_LIMITE_P=' || lower(ie_dose_limite_w));

          if  (ds_mensagem_regra_w is not null) then
            ds_mensagem_p := ds_mensagem_regra_w;
          end if;

        end if;

        commit;

        end;
      end loop;
      close c01;
    end if;
    end;
  end if;

  end;  
end if;

END VERIF_DOSEPRESC_MAX_PROT_INT;
/
