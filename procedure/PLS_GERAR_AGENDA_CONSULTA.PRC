create or replace
procedure pls_gerar_agenda_consulta
			(	nr_seq_atendimento_p	number,
				nr_seq_evento_p		number,
				cd_pessoa_fisica_p	varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	number) is 
				
cd_cgc_outorgante_w		varchar2(15);
cd_convenio_w			varchar2(15);
nm_pessoa_w			varchar2(80);
begin

select	cd_cgc_outorgante
into 	cd_cgc_outorgante_w
from	pls_outorgante
where	cd_estabelecimento = cd_estabelecimento_p;

begin
select	cd_convenio
into	cd_convenio_w
from	convenio
where	cd_cgc = cd_cgc_outorgante_w;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(261813,'');
	--Falta informação do convenio!'
end;

select	substr(obter_nome_pf(cd_pessoa_fisica_p),1,80)
into	nm_pessoa_w 
from	dual;

insert into agenda_consulta
	(nr_sequencia, cd_convenio, dt_agenda, 
	nr_minuto_duracao, ie_status_agenda, ie_classif_agenda,
	dt_atualizacao, nm_usuario, cd_turno,
	cd_pessoa_fisica, nr_atend_pls, nr_seq_evento_atend,
	nm_paciente)
values	(agenda_consulta_seq.nextval, cd_convenio_w, sysdate,
	60, 'O', 'T',
	sysdate, nm_usuario_p, 0,
	cd_pessoa_fisica_p, nr_seq_atendimento_p, nr_seq_evento_p,
	nm_pessoa_w);
	
commit;

end pls_gerar_agenda_consulta;
/
