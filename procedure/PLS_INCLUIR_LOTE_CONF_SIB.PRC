/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Incluir o lote de conferência em um lote de movimentação do SIB
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_incluir_lote_conf_sib
		(	nr_seq_lote_sib_p	number,
			nr_seq_lote_conf_p	number,
			nm_usuario_p		Varchar2) is 

begin

update	pls_lote_sib
set	NR_SEQ_LOTE_CONFERENCIA	= nr_seq_lote_conf_p
where	nr_sequencia		= nr_seq_lote_sib_p;

commit;

end pls_incluir_lote_conf_sib;
/
