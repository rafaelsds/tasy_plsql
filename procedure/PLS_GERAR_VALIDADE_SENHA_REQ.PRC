create or replace
procedure pls_gerar_validade_senha_req
			(	nr_seq_requisicao_p		Number,
				nr_seq_segurado_p		Number,
				dt_requisicao_p			Date,
				ie_tipo_guia_p			Varchar2,
				nm_usuario_p			Varchar2) is 

dt_rescisao_w			Date;
dt_limite_utilizacao_w		Date;
qt_dias_param_w			Number(3);
dt_validade_senha_w		Date;
cd_senha_w			Varchar2(20);
ie_tipo_processo_w		Varchar2(2);
ie_tipo_intercambio_w		Varchar2(2);
ie_scs_w			Varchar2(2)	:= 'N';
nr_seq_regra_senha_w		pls_aut_regra_gera_senha.nr_sequencia%type;
dt_validade_senha_ww		Date;
nr_seq_motivo_cancelamento_w		pls_segurado.nr_seq_motivo_cancelamento%type;
ie_iniciativa_beneficiario_w		pls_motivo_cancelamento.ie_iniciativa_beneficiario%type := 'N';
ie_validar_cancelamento_w		pls_param_requisicao.ie_validar_motivo_cancelamento%type := 'N';
cd_estabelecimento_w			pls_requisicao.cd_estabelecimento%type;
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Valida senha na requisi��o.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ x]    Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

/*Verifica��o da data de recis�o do segurado*/
begin
	select	dt_rescisao,
		dt_limite_utilizacao,
		nr_seq_motivo_cancelamento
	into	dt_rescisao_w,
		dt_limite_utilizacao_w,
		nr_seq_motivo_cancelamento_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
exception
when others then
	dt_rescisao_w		:= null;
	dt_limite_utilizacao_w	:= null;
end;

if	(dt_limite_utilizacao_w is not null) then
	dt_rescisao_w := dt_limite_utilizacao_w;
end if;

begin
	select	ie_tipo_processo,
		ie_tipo_intercambio,
		dt_validade_senha,
		cd_estabelecimento
	into	ie_tipo_processo_w,
		ie_tipo_intercambio_w,
		dt_validade_senha_ww,
		cd_estabelecimento_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_p;
exception
when others then
	ie_tipo_processo_w	:= 'X';
	ie_tipo_intercambio_w	:= 'X';
end;

if	(nvl(ie_tipo_processo_w,'X')	= 'I') and (nvl(ie_tipo_intercambio_w,'X')	= 'E') then
	ie_scs_w	:= 'S';
end if;

qt_dias_param_w	:= obter_valor_param_usuario(1271, 6, Obter_Perfil_Ativo, nm_usuario_p, 0);

if	(qt_dias_param_w	is null) and (ie_scs_w	<> 'S') then
	qt_dias_param_w	:= nvl(to_number(pls_obter_param_padrao_funcao(6, 1271)),0);
elsif	(ie_scs_w	= 'S') then
	qt_dias_param_w	:= 0;
end if;

select	nvl(max(ie_validar_motivo_cancelamento),'N')
into	ie_validar_cancelamento_w
from	pls_param_requisicao
where	cd_estabelecimento = cd_estabelecimento_w;

if	(ie_validar_cancelamento_w = 'S') then
	if	(nr_seq_motivo_cancelamento_w is not null) then
		begin
			select	nvl(ie_iniciativa_beneficiario, 'N')
			into	ie_iniciativa_beneficiario_w
			from	pls_motivo_cancelamento
			where	nr_sequencia	= nr_seq_motivo_cancelamento_w;
		exception
		when others then
			ie_iniciativa_beneficiario_w	:= 'N';
		end;

		if	(ie_iniciativa_beneficiario_w = 'N') then
			dt_rescisao_w := null;
		end if;
	end if;
end if;

if	(qt_dias_param_w	<> 0) then
	dt_validade_senha_w	:= dt_requisicao_p + qt_dias_param_w;
	if	(dt_rescisao_w	is not null) then
		if	(dt_validade_senha_w	> dt_rescisao_w) then
			--Djavan OS 478404 -  Solicita��o da Unimed S�o Jos� do Rio Preto para tirar o dt_rescisao_w - 1
			dt_validade_senha_w	:= dt_rescisao_w;
		end if;
	end if;
elsif	(qt_dias_param_w	= 0) then
	dt_validade_senha_w	:= to_date(pls_obter_regra_validade_senha('R',nr_seq_requisicao_p,dt_requisicao_p,nr_seq_segurado_p,ie_tipo_guia_p),'DD/MM/YYYY');
	if	(dt_rescisao_w	is not null) then
		if	(dt_validade_senha_w	> dt_rescisao_w) then
			--Djavan OS 478404 -  Solicita��o da Unimed S�o Jos� do Rio Preto para tirar o dt_rescisao_w - 1
			dt_validade_senha_w	:= dt_rescisao_w;
		end if;	
	end if;
end if;

if	(dt_validade_senha_w is not null) then
	if	(pls_obter_se_controle_estab('RE') = 'S') then
		select 	max(nr_sequencia)
		into	nr_seq_regra_senha_w
		from	pls_aut_regra_gera_senha
		where	dt_inicio_vigencia		<= sysdate
		and	nvl(dt_fim_vigencia,sysdate)	>= sysdate
		and	(cd_estabelecimento = nvl(wheb_usuario_pck.get_cd_estabelecimento, cd_estabelecimento_w));
	else
		select 	max(nr_sequencia)
		into	nr_seq_regra_senha_w
		from	pls_aut_regra_gera_senha
		where	dt_inicio_vigencia		<= sysdate
		and	nvl(dt_fim_vigencia,sysdate)	>= sysdate;
	end if;

	if	(nr_seq_regra_senha_w	is not null) then
	
		pls_gerar_senha_aut_regra(nr_seq_regra_senha_w, null, nr_seq_requisicao_p, nm_usuario_p, cd_senha_w);
		
		if 	(cd_senha_w	= '') then
			cd_senha_w := Obter_Numero_Randomico;
		end if;	
	else
		cd_senha_w := Obter_Numero_Randomico;
	end if;
end if;

update	pls_requisicao
set	cd_senha		= cd_senha_w,
	dt_validade_senha	= decode(dt_validade_senha_ww, null, dt_validade_senha_w, dt_validade_senha_ww),
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_requisicao_p;	

end pls_gerar_validade_senha_req;
/