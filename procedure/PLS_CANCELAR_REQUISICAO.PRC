create or replace
procedure pls_cancelar_requisicao
			(	nr_seq_requisicao_p	Number,
				nr_seq_motivo_cancel_p	Number,
				ds_observacao_p		Varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

nr_requisicao_proc_w		Number(10);
nr_requisicao_mat_w		Number(10);
ie_cancela_trat_w		varchar2(1) := 'N';
qt_reg_w			Number(10);
ds_motivo_cancel_w		varchar2(255);
qt_reg_aud_w			Number(10);
qt_reg_proc_aud_w		Number(10);
qt_reg_mat_aud_w		Number(10);
ds_estagio_w			varchar2(255);
dt_fim_evento_w			pls_atendimento_evento.dt_fim_evento%type;
nr_seq_atend_pls_w		pls_requisicao.nr_seq_atend_pls%type;
nr_seq_evento_atend_w		pls_requisicao.nr_seq_evento_atend%type;
nr_seq_auditoria_w		pls_auditoria.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_requisicao_proc
	where	nr_seq_requisicao = nr_seq_requisicao_p;
	
Cursor C02 is
	select	nr_sequencia
	from	pls_requisicao_mat
	where	nr_seq_requisicao = nr_seq_requisicao_p;

Cursor C03 is
	select	nr_sequencia
	from	pls_execucao_requisicao
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	
begin
begin
	select	substr(obter_valor_dominio(3431, ie_estagio),1,255)
	into	ds_estagio_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_p;
exception
when others then
	ds_estagio_w	:= '';
end;

update	pls_requisicao
set	ie_status		= 'C',
	ie_estagio		= 3,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_fim_processo_req	= sysdate,
	nr_seq_motivo_cancel	= nr_seq_motivo_cancel_p
where	nr_sequencia		= nr_seq_requisicao_p;

open C01;
loop
fetch C01 into	
	nr_requisicao_proc_w;
exit when C01%notfound;
	begin
	update	pls_requisicao_proc
	set	ie_status	= 'C',
		ie_estagio	= 'N',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_requisicao_proc_w;
	
	select	count(1)
	into	qt_reg_proc_aud_w
	from	pls_auditoria		b,
		pls_auditoria_item	a
	where	a.nr_seq_proc_origem	= nr_requisicao_proc_w
	and	a.nr_seq_auditoria	= b.nr_sequencia
	and	b.ie_status		not in('F', 'C');
	
	if	(qt_reg_proc_aud_w	> 0) then
		update	pls_auditoria_item
		set	ie_status		= 'C',
			ie_status_solicitacao	= 'C',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_proc_origem	= nr_requisicao_proc_w;
	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_requisicao_mat_w;
exit when C02%notfound;
	begin
	update	pls_requisicao_mat
	set	ie_status	= 'C',
		ie_estagio	= 'N',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_requisicao_mat_w;
	
	select	count(1)
	into	qt_reg_mat_aud_w
	from	pls_auditoria		b,
		pls_auditoria_item	a
	where	a.nr_seq_mat_origem	= nr_requisicao_mat_w
	and	a.nr_seq_auditoria	= b.nr_sequencia
	and	b.ie_status		not in('F', 'C');
	
	if	(qt_reg_mat_aud_w	> 0) then
		update	pls_auditoria_item
		set	ie_status		= 'C',
			ie_status_solicitacao	= 'C',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_mat_origem	= nr_requisicao_mat_w;
	end if;
	end;
end loop;
close C02;

begin
select	substr(ds_motivo_cancelamento,1,255)
into	ds_motivo_cancel_w
from	pls_guia_motivo_cancel
where	ie_situacao 		= 'A'
and	cd_estabelecimento 	= cd_estabelecimento_p
and	nr_sequencia		= nr_seq_motivo_cancel_p;
exception
when others then
	ds_motivo_cancel_w	:= '';
end;

pls_requisicao_gravar_hist(	nr_seq_requisicao_p, 'L', 
				substr('Requisi��o cancelada em '||to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss')||' pelo usu�rio '||nm_usuario_p||'.'||chr(13)||
				'Motivo: '||ds_motivo_cancel_w||'.'||chr(13)||'Est�gio anterior da requisi��o: '||ds_estagio_w
				||chr(13)||'Observa��o: '||ds_observacao_p,1,4000),
				'',nm_usuario_p);

select	count(1)
into	qt_reg_w
from	pls_tratamento_benef
where	nr_seq_requisicao	= nr_seq_requisicao_p;

ie_cancela_trat_w	:= obter_valor_param_usuario(1333, 1, Obter_Perfil_Ativo, nm_usuario_p, 0);

if	(qt_reg_w > 0) and
	(ie_cancela_trat_w = 'S') then
	pls_cancelar_tratamento_benef(nr_seq_requisicao_p,nm_usuario_p);
end if;

select	count(1)
into	qt_reg_aud_w
from	pls_auditoria
where	nr_seq_requisicao	= nr_seq_requisicao_p
and	ie_status		not in('F', 'C');

if	(qt_reg_aud_w	> 0) then
	update	pls_auditoria
	set	ie_status		= 'C',
		dt_liberacao		= sysdate,
		nr_seq_proc_interno	= null,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
end if;

--OS 1308565, cancela a an�lise da execu��o
for C03_w in C03 loop
	begin
	update	pls_execucao_requisicao
	set	ie_situacao	= 5,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= C03_w.nr_sequencia
	and	ie_situacao	= 2;
	
	update	pls_execucao_req_item
	set	ie_situacao	= 'C',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_seq_execucao	= C03_w.nr_sequencia
	and	ie_situacao	= 'A';
	
	begin
		select 	nr_sequencia
		into	nr_seq_auditoria_w
		from 	pls_auditoria
		where	nr_seq_execucao	= C03_w.nr_sequencia;
	exception
	when others then
		nr_seq_auditoria_w	:= null;
	end;
	
	if (nr_seq_auditoria_w is not null) then
		update	pls_auditoria
		set	ie_status		= 'C',
			dt_liberacao		= sysdate,
			nr_seq_proc_interno	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_auditoria_w;
		
		update	pls_auditoria_item
		set	ie_status		= 'C',
			ie_status_solicitacao	= 'C',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_auditoria	= nr_seq_auditoria_w;
	end if;
	end;
end loop;

begin
	select	nr_seq_atend_pls,
		nr_seq_evento_atend
	into	nr_seq_atend_pls_w,
		nr_seq_evento_atend_w
	from	pls_requisicao
	where	nr_sequencia = nr_seq_requisicao_p;
exception
when others then
	nr_seq_atend_pls_w := null;
	nr_seq_evento_atend_w := null;
end;

begin
	select	dt_fim_evento
	into	dt_fim_evento_w
	from	pls_atendimento_evento
	where	nr_sequencia = nr_seq_evento_atend_w;
exception
when others then
	dt_fim_evento_w := null;
end;

if	(nr_seq_atend_pls_w is not null) and
	(dt_fim_evento_w is not null) then
	pls_finalizar_atendimento(	nr_seq_atend_pls_w,
					nr_seq_evento_atend_w,
					null,
					null,
					nm_usuario_p);
end if;

commit;

end pls_cancelar_requisicao;
/
