create or replace procedure vincular_convenio_titulo_receb (
    nr_titulo_p         convenio_receb_titulo.nr_titulo%type,
    nr_bordero_p        convenio_receb_titulo.nr_bordero%type,
    vl_recebido_p       convenio_receb_titulo.vl_recebido%type,
    nr_seq_convenio_p   convenio_receb_titulo.nr_seq_receb%type,
    nm_usuario_p        convenio_receb_titulo.nm_usuario%type,
    ie_opcao_p             varchar2
) is

begin

    if (ie_opcao_p = 'V') then
        insert into convenio_receb_titulo (
            nr_sequencia,
            dt_atualizacao,
            dt_atualizacao_nrec,
            nm_usuario,
            nm_usuario_nrec,
            nr_seq_receb,
            nr_titulo,
            vl_recebido,
            nr_bordero
        ) values (
            convenio_receb_titulo_seq.nextval,
            sysdate,
            sysdate,
            nm_usuario_p,
            nm_usuario_p,
            nr_seq_convenio_p, --> sequencia do recebimento de convenio que foi vinculado ao titulo
            nr_titulo_p, --> titulo no qual foi efetuado o botao direito para vinculo
            vl_recebido_p, --> todo o valor recebido do titulo neste bordero, similar aos totalizadores da wdlg
            nr_bordero_p --> numero do bordero
        );

        update bordero_tit_rec 
        set nr_seq_receb = nr_seq_convenio_p
        where nr_bordero = nr_bordero_p
        and nr_titulo = nr_titulo_p;

    elsif (ie_opcao_p = 'D') then

        update bordero_tit_rec
        set nr_seq_receb = null
        where nr_bordero = nr_bordero_p
        and nr_titulo = nr_titulo_p;

        delete from convenio_receb_titulo
        where nr_titulo = nr_titulo_p
        and nr_bordero = nr_bordero_p
        and nr_seq_receb = nr_seq_convenio_p;

    end if;

    commit;
end vincular_convenio_titulo_receb;
/