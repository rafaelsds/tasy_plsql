create or replace
procedure pls_aplicar_reaj_inscricao
			(	nr_seq_reajuste_p	number,
				ie_opcao_p		varchar2,
				dt_aplicacao_p		date, 
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

nr_seq_inscricao_w		number(10);
tx_coparticipacao_w		number(7,4);
tx_reajuste_inscricao_w		number(7,4);
vl_inscricao_reajuste_w		number(15,2);
tx_inscricao_reajuste_w		number(7,4);
vl_base_w			number(15,2);
tx_base_w			number(7,4);
dt_reajuste_w			date;
nr_seq_contrato_w		number(10);
nr_seq_plano_w			number(10);
qt_parcela_inicial_w		number(10);
qt_parcela_final_w		number(10);
vl_inscricao_w			number(15,2);
tx_inscricao_w			number(7,4);
ie_tipo_contratacao_w		varchar2(2);
ie_grau_dependencia_w		varchar2(2);
ie_acao_contrato_w		varchar2(2);
nr_seq_regra_inscr_w		number(10);
nr_seq_intercambio_w		number(10);
nr_seq_reaj_inscricao_w		number(10);
-----------------------------------------------------------------------------
nr_seq_reajuste_princ_w		number(10);
nr_seq_regra_atual_ant_w	number(10);
ie_tipo_reajuste_w		varchar2(10);
ie_tipo_lote_w			varchar2(10);
TX_DEFLATOR_w			number(7,4);

Cursor C01 is
	select	c.nr_sequencia,
		a.tx_reajuste,
		c.nr_seq_plano,
		a.nr_sequencia
	from	pls_reajuste_inscricao	a,
		pls_lote_reaj_inscricao	b,
		pls_regra_inscricao	c
	where	a.nr_seq_lote		= b.nr_sequencia
	and	a.nr_seq_regra_inscricao = c.nr_sequencia
	and	b.nr_sequencia		= nr_seq_reajuste_p;
begin

if	(ie_opcao_p = 'L') then /* Liberar reajuste */
	update	pls_lote_reaj_inscricao
	set	dt_liberacao	= sysdate
	where	nr_sequencia	= nr_seq_reajuste_p;
elsif	(ie_opcao_p = 'A') then /* Aplicar reajuste */
	select	dt_referencia,
		nr_seq_contrato,
		nr_seq_intercambio,
		nr_seq_reajuste
	into	dt_reajuste_w,
		nr_seq_contrato_w,
		nr_seq_intercambio_w,
		nr_seq_reajuste_princ_w
	from	pls_lote_reaj_inscricao
	where	nr_sequencia	= nr_seq_reajuste_p;
	
	select	ie_tipo_reajuste,
		nvl(ie_tipo_lote,'A'),
		nvl(TX_DEFLATOR,0)
	into	ie_tipo_reajuste_w,
		ie_tipo_lote_w,
		TX_DEFLATOR_w
	from	pls_reajuste
	where	nr_sequencia	= nr_seq_reajuste_princ_w;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_inscricao_w,
		tx_reajuste_inscricao_w,
		nr_seq_plano_w,
		nr_seq_reaj_inscricao_w;
	exit when C01%notfound;
		begin
		vl_base_w		:= 0;
		vl_inscricao_reajuste_w	:= 0;
		tx_base_w		:= 0;
		tx_inscricao_reajuste_w	:= 0;
		
		select	qt_parcela_inicial,
			qt_parcela_final,
			vl_inscricao,
			tx_inscricao,
			ie_tipo_contratacao,
			ie_grau_dependencia,
			ie_acao_contrato
		into	qt_parcela_inicial_w,
			qt_parcela_final_w,
			vl_inscricao_w,
			tx_inscricao_w,
			ie_tipo_contratacao_w,
			ie_grau_dependencia_w,
			ie_acao_contrato_w
		from	pls_regra_inscricao
		where	nr_sequencia	= nr_seq_inscricao_w;
		
		/*aaschlote 03/11/2012 - Para quando for desfazer lotes de reajuste de inscri��o, buscar os valores do lote anterior*/
		if	((ie_tipo_reajuste_w = 'I') and
			(ie_tipo_lote_w		= 'D') or
			(ie_tipo_reajuste_w = 'C') and
			(nvl(tx_deflator_w,0) <> 0)) then
			
			select	max(b.NR_SEQ_REGRA_INSCRICAO)
			into	nr_seq_regra_atual_ant_w
			from	pls_reajuste_inscricao	b,
				pls_lote_reaj_inscricao	a
			where	b.NR_SEQ_LOTE		= a.nr_sequencia
			and	a.nr_seq_contrato	= nr_seq_contrato_w
			and	nr_seq_regra_gerada	= nr_seq_inscricao_w;
			
			if	(NR_SEQ_REGRA_ATUAL_ant_w is not null) then
				select	vl_inscricao,
					tx_inscricao
				into	vl_inscricao_reajuste_w,
					tx_inscricao_reajuste_w
				from	pls_regra_inscricao
				where	nr_sequencia	= NR_SEQ_REGRA_ATUAL_ant_w;
			end if;
		else		
			vl_base_w		:= dividir_sem_round(vl_inscricao_w * tx_reajuste_inscricao_w,100);
			vl_inscricao_reajuste_w	:= vl_inscricao_w + vl_base_w;	
			
			tx_base_w		:= dividir_sem_round(tx_inscricao_w * tx_reajuste_inscricao_w,100);
			tx_inscricao_reajuste_w	:= tx_inscricao_w + tx_base_w;
		end if;
		
		if	(nvl(nr_seq_plano_w,0) = 0) then /* Se for regra do produto, n�o pode inativar a regra */
			update	pls_regra_inscricao
			set	dt_fim_vigencia	= dt_aplicacao_p - 1
			where	nr_sequencia	= nr_seq_inscricao_w;
		end if;
		
		select	pls_regra_inscricao_seq.nextval
		into	nr_seq_regra_inscr_w
		from	dual;
		
		insert	into	pls_regra_inscricao
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				dt_inicio_vigencia, nr_seq_contrato, nr_seq_lote_reajuste,
				vl_inscricao, tx_inscricao, ie_tipo_contratacao, ie_acao_contrato,
				qt_parcela_inicial, qt_parcela_final, ie_grau_dependencia,
				nr_seq_intercambio)
		values	(	nr_seq_regra_inscr_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
				dt_aplicacao_p, nr_seq_contrato_w, nr_seq_reajuste_p,
				vl_inscricao_reajuste_w, tx_inscricao_reajuste_w, ie_tipo_contratacao_w, ie_acao_contrato_w,
				qt_parcela_inicial_w, qt_parcela_final_w, ie_grau_dependencia_w,
				nr_seq_intercambio_w);
				
		update	pls_reajuste_inscricao
		set	nr_seq_regra_gerada	= nr_seq_regra_inscr_w
		where	nr_sequencia		= nr_seq_reaj_inscricao_w;
		end;
	end loop;
	close C01;
	
	update	pls_lote_reaj_inscricao
	set	dt_aplicacao	= dt_aplicacao_p
	where	nr_sequencia	= nr_seq_reajuste_p;
end if;

commit;

end pls_aplicar_reaj_inscricao;
/
