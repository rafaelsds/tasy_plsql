create or replace
procedure alterar_dt_prevista_cobranca(
			dt_prevista_p		date,
			nr_sequencia_p		number) is 

begin

update  cobranca 
set     dt_previsao_cobranca = trunc(dt_prevista_p) 
where   nr_sequencia = nr_sequencia_p; 

commit;

end alterar_dt_prevista_cobranca;
/