create or replace
procedure liberar_repasse_regra(	nr_repasse_terceiro_p	number,
				nm_usuario_p		varchar2) is

nr_seq_regra_w		number(10);
qt_dias_ref_w		number(10);
cd_estabelecimento_w	number(4);
nr_seq_terceiro_w		number(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
cd_area_procedimento_w	number(8);
cd_especialidade_w	number(8);
cd_grupo_proc_w		number(15);
ie_status_regra_w		varchar2(1);
dt_procedimento_w	date;
nr_seq_proc_repasse_w	number(10);
vl_repasse_w		number(15,2);
cd_setor_atendimento_w	number(15);
ie_tipo_atendimento_w	number(15);
ie_tipo_convenio_w	number(2);
cd_medico_executor_w	varchar2(10);
cd_convenio_w		number(5);
dt_sysdate_w		date;
dt_inicial_w		date;
dt_final_w			date;

Cursor c01 is
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_area_procedimento,
	a.cd_especialidade,
	a.cd_grupo_proc,
	a.nr_sequencia,
	a.qt_dias_ref,
	a.cd_setor_atendimento,
	a.ie_tipo_atendimento,
	a.ie_tipo_convenio,
	a.cd_medico_executor,
	a.cd_convenio
from	regra_lib_repasse a
where	nvl(a.nr_seq_terceiro, nr_seq_terceiro_w)		= nr_seq_terceiro_w
and	a.cd_estabelecimento					= cd_estabelecimento_w;

Cursor c02 is
select	ie_status
from	regra_lib_rep_status a
where	a.nr_seq_regra		= nr_seq_regra_w;

Cursor c03 is
select	a.nr_sequencia,
	b.dt_procedimento,
	a.vl_repasse
from	atendimento_paciente f,
		convenio e,
	conta_paciente d,
	estrutura_procedimento_v c,
	procedimento_paciente b,
	procedimento_repasse a
where	nvl(b.cd_convenio,d.cd_convenio_parametro)			= e.cd_convenio
and	b.nr_interno_conta						= d.nr_interno_conta(+)
and	a.nr_seq_procedimento						= b.nr_sequencia
and	b.cd_procedimento						= c.cd_procedimento
and	b.ie_origem_proced						= c.ie_origem_proced
and	d.nr_atendimento						= f.nr_atendimento
and	nvl(nvl(cd_medico_executor_w,b.cd_medico_executor),'0')		= nvl(b.cd_medico_executor,'0')
and	nvl(nvl(ie_tipo_convenio_w,e.ie_tipo_convenio),0)		= nvl(e.ie_tipo_convenio,0)
and	nvl(cd_procedimento_w,b.cd_procedimento)			= b.cd_procedimento
and	nvl(ie_origem_proced_w,b.ie_origem_proced)			= b.ie_origem_proced
and	nvl(cd_area_procedimento_w,c.cd_area_procedimento)		= c.cd_area_procedimento
and	nvl(cd_especialidade_w,c.cd_especialidade)			= c.cd_especialidade
and	nvl(cd_grupo_proc_w,c.cd_grupo_proc)				= c.cd_grupo_proc
and	nvl(cd_convenio_w,e.cd_convenio)				= e.cd_convenio
and		nvl(null, b.cd_setor_atendimento)			= b.cd_setor_atendimento
and	nvl(ie_tipo_atendimento_w, f.ie_tipo_atendimento)	= f.ie_tipo_atendimento
and	b.dt_procedimento	between dt_inicial_w	and fim_dia(dt_final_w)			
and	a.nr_seq_terceiro						= nr_seq_terceiro_w
and	a.nr_repasse_terceiro 						is null
and	b.nr_interno_conta 						is not null
and	a.ie_status							= ie_status_regra_w;

begin

if	(nr_repasse_terceiro_p is not null) then

	dt_sysdate_w	:= trunc(sysdate,'dd');
	
	select	nr_seq_terceiro,
		cd_estabelecimento,
		dt_periodo_inicial,
		dt_periodo_final
	into	nr_seq_terceiro_w,
		cd_estabelecimento_w,
		dt_inicial_w,
		dt_final_w
	from	repasse_terceiro
	where	nr_repasse_terceiro			= nr_repasse_terceiro_p;
	
	open c01;
	loop
	fetch c01 into
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		nr_seq_regra_w,
		qt_dias_ref_w,
		cd_setor_atendimento_w,
		ie_tipo_atendimento_w,
		ie_tipo_convenio_w,
		cd_medico_executor_w,
		cd_convenio_w;
	exit when c01%notfound;

		open c02;
		loop
		fetch c02 into
			ie_status_regra_w;
		exit when c02%notfound;
			
			open c03;
			loop
			fetch c03 into
				nr_seq_proc_repasse_w,
				dt_procedimento_w,
				vl_repasse_w;
			exit when c03%notfound;
				
				Liberar_ProcMat_Repasse
					(nr_seq_proc_repasse_w,
					null,
					'L',
					vl_repasse_w,
					null,       
					sysdate,
					null,
					nm_usuario_p,
					null);

				update	procedimento_repasse   --'Liberado automaticamente por regra (' || nr_seq_regra_w || ') ' || ' ao gerar repasse. '								
				set	ds_observacao	= wheb_mensagem_pck.get_texto(308367,'nr_seq_regra_w='||nr_seq_regra_w)							
				where	nr_sequencia	= nr_seq_proc_repasse_w;
			end loop;
			close c03;
		end loop;
		close c02;
	end loop;
	close c01;
end if;


/* Nao pode ter commit nessa procedure */

end liberar_repasse_regra;
/