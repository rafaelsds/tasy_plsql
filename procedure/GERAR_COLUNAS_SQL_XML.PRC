create or replace
procedure gerar_colunas_sql_xml(nr_sequencia_p	number,
				nm_usuario_p	varchar2) is


col_cnt		pls_integer;
campos_w	dbms_sql.desc_tab;	
cursor_w	integer;
i		integer;
ds_sql_w	varchar2(10000);
ds_erro_w	varchar2(2000);
ds_tipo_dado_w	varchar2(10);
ds_mascara_w	varchar2(12);

begin

select	ds_sql
into	ds_sql_w
from	xml_elemento
where	nr_sequencia = nr_sequencia_p;

begin
	cursor_w := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(cursor_w, ds_sql_w, dbms_sql.native);
	dbms_sql.describe_columns(cursor_w,col_cnt,campos_w);
exception when others then
	ds_erro_w := SQLERRM(SQLCODE);
	Wheb_mensagem_pck.exibir_mensagem_abort(279345, 'DS_ERRO_P=' || ds_erro_w);
end;
	for i in 1 .. col_cnt loop
		begin
		ds_mascara_w	:= '';
		
		if	(campos_w(i).col_type = 1) then
			ds_tipo_dado_w	:= 'VARCHAR2';
		elsif	(campos_w(i).col_type = 2) then
			ds_tipo_dado_w	:= 'NUMBER';
		elsif	(campos_w(i).col_type = 12) then
			ds_tipo_dado_w	:= 'DATE';
			ds_mascara_w	:= 'dd/mm/yyyy';
		end	if;		

		
		insert into xml_atributo(
			NR_SEQUENCIA, 
			NR_SEQ_ELEMENTO,
			NR_SEQ_APRESENTACAO,    
			NM_ATRIBUTO_XML,        
			NM_ATRIBUTO,            
			IE_CRIAR_NULO,          
			IE_OBRIGATORIO,         
			IE_TIPO_ATRIBUTO,
			NM_USUARIO,             
			DT_ATUALIZACAO,         
			NM_USUARIO_NREC,             
			DT_ATUALIZACAO_NREC,         
			IE_CRIAR_ATRIBUTO,
			IE_CONTROLE_PB,
			DS_MASCARA)
		values (
			xml_atributo_seq.nextval,
			nr_sequencia_p,
			i * 5,
			campos_w(i).col_name,
			campos_w(i).col_name,
			'S',
			'S',
			ds_tipo_dado_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			'S',
			'N',
			ds_mascara_w);		
		end;
	end loop;

dbms_sql.close_cursor(cursor_w);

Commit;

end	gerar_colunas_sql_xml;
/