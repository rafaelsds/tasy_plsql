create or replace
procedure pls_gerar_proc_partic_aut_web	(	nr_seq_conta_proc_p	number,
						cd_medico_p		varchar2,
						nr_seq_grau_partic_p	number,
						nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
finalidade:
iserir automaticamente o participante no procedimento, na digita��o de conbtas m�dicas
-------------------------------------------------------------------------------------------------------------------
locais de chamada direta: 
[  ]  objetos do dicion�rio [ ] tasy (delphi/java) [  x ] portal [  ]  relat�rios [ ] outros:
 ------------------------------------------------------------------------------------------------------------------
pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

uf_conselho_w		medico.uf_crm%type;
nr_seq_conselho_w	pessoa_fisica.nr_seq_conselho%type;
nr_seq_cbo_saude_w	number(10);

begin
if	(cd_medico_p is not null) and
	(nr_seq_grau_partic_p is not null) then
	begin
		select	a.uf_crm,
			(select	b.nr_seq_conselho
			from	pessoa_fisica b
			where	b.cd_pessoa_fisica = cd_medico_p) nr_seq_conselho,
			pls_obter_cbo_medico(cd_medico_p) cbo_medico
		into 	uf_conselho_w,
			nr_seq_conselho_w,	
			nr_seq_cbo_saude_w		
		from	medico a
		where	a.cd_pessoa_fisica = cd_medico_p;
	exception
	when others then
		uf_conselho_w 		:= null;
		nr_seq_conselho_w	:= null;	
		nr_seq_cbo_saude_w	:= null;	
	end;
		
	pls_gerar_proc_partic_web(	nr_seq_conta_proc_p, 'I', cd_medico_p,
					nr_seq_grau_partic_p, nr_seq_cbo_saude_w, 0,
					null, nr_seq_conselho_w, nm_usuario_p,
					uf_conselho_w);	

end if;	

end pls_gerar_proc_partic_aut_web;
/