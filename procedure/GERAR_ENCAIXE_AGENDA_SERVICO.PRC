create or replace
procedure gerar_encaixe_agenda_servico
			(cd_agenda_p			number,
			cd_pessoa_fisica_p		varchar2,
			dt_inicio_p				date,
			qt_dias_p				number,
			nm_usuario_p			varchar2,
			cd_classificacao_p		varchar2,
			cd_convenio_p			number,
			ie_seq_dia_p			varchar2,
			cd_medico_p				varchar2,
			nm_paciente_p			varchar2,
			ds_observacao_p			varchar2,
			nr_sessao_total_p		number,
			nr_sessao_atual_p		number,
			ds_erro_p out 			varchar2,
			cd_estabelecimento_p	varchar2,
			cd_procedimento_p 		number,
			ie_origem_proced_p 		number,
			nr_seq_proc_interno_p 	number,
			cd_medico_solic_p		varchar2,
			cd_cid_p				varchar2,
			nr_seq_agenda_cir_p		number,
			ds_dias_p				varchar2,
			ie_tipo_atendimento_p		number default null,
			ie_carater_inter_sus_p		varchar2 default null,
			nr_seq_encaixe_p 	out varchar2,
			nr_seq_segurado_p	NUMBER	DEFAULT NULL,
			cd_categoria_p		VARCHAR2 DEFAULT NULL,
			cd_usuario_convenio_p	VARCHAR2 DEFAULT NULL,
			dt_validade_carteira_p	DATE DEFAULT NULL,
			cd_plano_p		VARCHAR2 DEFAULT NULL,
			cd_setor_atendimento_p	number	default null) is

		
qt_dia_atual_w				number(10,0)		:= 0;
nr_seq_hora_w				number(0010,0)  	:= 0;
nr_sequencia_w				number(0010,0)  	:= 0;
hr_atual_w					date;
cd_classificacao_w			varchar2(05)		:= 'E';
cd_turno_w					varchar2(01);
ie_dia_semana_w				number(10,0);
nr_seq_turno_w				number(10,0);
cd_tipo_agenda_w			number(10,0);
qt_turno_dia_w				number(10,0);
cont						number(10,0)		:= 0;
qt_dias_w					number(10,0);
ie_gerar_solic_pront_w		varchar2(1);
ie_gerar_solic_pront_gp_w	varchar2(1);
ie_bloqueio_w				varchar2(1);
ds_erro_w					varchar2(255);
ds_erro_w2					varchar2(255);
ds_erro_w3					varchar2(255);
ds_erro_w4					varchar2(255);
ds_consistencia_w			varchar2(255);
ds_erro_bloqueio_w			varchar2(255);
ds_erro_regra_w				varchar2(255);
ds_erro_duplicado_w			varchar2(255);
ds_erro_regra_conv_w		varchar2(255);
ie_encaixe_bloq_w			varchar(1);
nr_sessao_atual_w			number(3);
ie_considera_classif_w		varchar2(1);
qt_permitida_regra_w		number(10,0);
hr_inicial_regra_w			date;
hr_final_regra_w			date;
qt_encaixe_existe_w			number(10,0);
ie_permite_w				varchar2(1)	:= 'S';	
ie_duplicado_w				varchar2(1)	:= 'N';	
ds_enter_w                  varchar2(10) := chr(13) || chr(10);
qt_agendado_w				number(10,0);
ie_permite_agendar_dupl_w	varchar2(1);
nr_seq_regra_encaixe_w		number(10,0);
cd_setor_atendimento_w		number(10,0);
nr_controle_secao_w			number(10,0);
qt_proc_w					number(10,0);
cd_convenio_regra_w			number(5,0);
hr_inicial_w				date;
hr_final_w					date;
ie_busca_inf_ult_agend_w	varchar2(1);
nr_telefone_w				varchar2(80);
nr_doc_convenio_w			varchar2(20);
cd_senha_w					varchar2(20);
dt_valida_carteira_w		date;
cd_usuario_convenio_w		varchar2(30);
cd_setor_atendimento_ww		number(10,0);
cd_convenio_w				number(5,0);
cd_convenio_ww				convenio.cd_convenio%type;
cd_categoria_w				varchar2(10);
cd_tipo_acomodacao_w		number(4,0);
nr_seq_agendamento_w		number(10);
ds_dias_w					varchar2(255);
dt_dia_semana_w				number(1);
ie_gerar_dia_w				varchar2(1);
ds_erro_ww					varchar2(255);
qt_turno_final_semana_w		number(10);
nr_seq_ref_w			agenda_consulta.nr_sequencia%type;
ie_utiliza_cont_w	Varchar2(1);
cd_setor_insert_w	setor_atendimento.cd_setor_atendimento%type;
nr_seq_bloq_geral_w			agenda_bloqueio_geral.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,
			nvl(hr_inicial, to_date('30/12/1899 00:00:01', 'dd/mm/yyyy hh24:mi:ss')),
			nvl(hr_final, to_date('30/12/1899 23:59:59', 'dd/mm/yyyy hh24:mi:ss'))
	from	agenda_regra_encaixe
	where	cd_agenda = cd_agenda_p	
	and		obter_cod_dia_semana(hr_atual_w) = ie_dia_semana		
	and		to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || to_char(dt_inicio_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			between to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_inicial,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
			to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_final,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss')
	and		((cd_convenio 	= cd_convenio_p) or (cd_convenio_p is null))
	order by 	nvl(cd_convenio,0),
				2,
				3;
				
Cursor C02 is
	select	nr_sequencia,
			nvl(hr_inicial, to_date('30/12/1899 00:00:01', 'dd/mm/yyyy hh24:mi:ss')),
			nvl(hr_final, to_date('30/12/1899 23:59:59', 'dd/mm/yyyy hh24:mi:ss'))
	from	agenda_regra_encaixe
	where	cd_agenda = cd_agenda_p
	and		((ie_dia_semana = obter_cod_dia_semana(hr_atual_w)) or ((ie_dia_semana = 9) and (obter_cod_dia_semana(hr_atual_w) not in (7,1))))
	and		to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || to_char(dt_inicio_p,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
			between to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_inicial,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
			to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_final,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss')
	and		((cd_convenio = cd_convenio_p) or (cd_convenio is null))
	order by 	nvl(cd_convenio,0),
				2,
				3;
				
				
begin

select	count(*)
into	qt_proc_w
from	procedimento
where	cd_procedimento = cd_procedimento_p
and	ie_origem_proced = ie_origem_proced_p;

if	(qt_proc_w = 0) and
	(nvl(cd_procedimento_p,0) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(280190);
end if;

obter_param_usuario(866, 68, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_considera_classif_w);

obter_param_usuario(866, 91, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_permite_agendar_dupl_w);

obter_param_usuario(866, 104, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_utiliza_cont_w);

obter_param_usuario(866, 228, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_busca_inf_ult_agend_w);

nr_sessao_atual_w	:= nr_sessao_atual_p;

select	cd_tipo_agenda,
		nvl(ie_gerar_solic_pront,'S'),
		cd_setor_agenda,
		nvl(ie_gerar_solic_pront_gp, 'S')
into	cd_tipo_agenda_w,
		ie_gerar_solic_pront_w,
		cd_setor_atendimento_w,
		ie_gerar_solic_pront_gp_w
from	agenda
where	cd_agenda	= cd_agenda_p;

qt_dia_atual_w	:= 0;
hr_atual_w		:= dt_inicio_p;
qt_dias_w		:= qt_dias_p;

select	pkg_date_utils.get_WeekDay(dt_inicio_p)
into	ie_dia_semana_w
from	dual;

select	nvl(max(nr_sequencia),0)
into	nr_seq_turno_w
from	agenda_turno
where	cd_agenda	= cd_agenda_p
and		((ie_dia_semana	= ie_dia_semana_w)	or	(ie_dia_semana	= 9))
and		to_date('01/01/1900 ' || to_char(dt_inicio_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') between
		to_date('01/01/1900 ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') and
		to_date('01/01/1900 ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');

if	(cd_classificacao_p is null) then
	select nvl(max(cd_classificacao), 'E')
	into	cd_classificacao_w
	from 	agenda_turno_classif
	where	nr_seq_turno	= nr_seq_turno_w;
else
	cd_classificacao_w	:= cd_classificacao_p;
end if;

if (ie_utiliza_cont_w = 'S') then

	select	nvl(max(nr_sequencia), 0)
	into 	nr_seq_ref_w
	from	agenda_consulta
	where	cd_agenda		= cd_agenda_p
	and	ie_status_agenda not in ('B','C')
	and	((cd_pessoa_fisica = cd_pessoa_fisica_p) or (nm_paciente = nm_paciente_p))
	and	(((cd_procedimento  = cd_procedimento_p) or (cd_procedimento_p is null))
	or ((nr_seq_proc_interno = nr_seq_proc_interno_p) or (nr_seq_proc_interno_p is null)));

	if (nr_seq_ref_w <> 0) then
		select 	max(nr_controle_secao)
		into	nr_controle_secao_w
		from 	agenda_consulta
		where 	nr_sequencia = nr_seq_ref_w;
	end if;
end if;	

while	(qt_dia_atual_w	< qt_dias_w) loop
	begin

	cont	:= cont + 1;
	ie_permite_w	:= 'S';
	ie_duplicado_w	:= 'N';
	ie_gerar_dia_w := 'S';
	
	if	(ds_dias_p is not null) then
		select	substr(ds_dias_p,1,length(ds_dias_p) -2)
		into	ds_dias_w
		from	dual;
		
		/*Consiste se existe turno cadastrado para os finais de semana.*/
		select	count(*)
		into	qt_turno_final_semana_w
		from	agenda_turno
		where	cd_agenda	= cd_agenda_p
		and		((ie_dia_semana	= 7 and Obter_Se_Contido(7,ds_dias_w) = 'S')
		or		(ie_dia_semana	= 1 and Obter_Se_Contido(1,ds_dias_w) = 'S'));
		
		/* obter dia semana */
		select	obter_cod_dia_semana(hr_atual_w)
		into	dt_dia_semana_w
		from	dual;
			
		ie_gerar_dia_w	:=  obter_se_contido(dt_dia_semana_w,ds_dias_w);
			
	end if;	
	
	select	count(*)
	into	qt_turno_dia_w
	from	agenda_turno
	where	cd_agenda	= cd_agenda_p
	and		ie_dia_semana	= pkg_date_utils.get_WeekDay(hr_atual_w)
	and		pkg_date_utils.is_business_day(hr_atual_w) = 0;			
	
	
	if 	(ds_dias_w is not null) and	
		(qt_turno_final_semana_w = 0) and
		(Obter_se_contido_lista('2,3,4,5,6',ds_dias_w) = 'N') then
		goto final;
	end if;
					
	qt_dia_atual_w	:= qt_dia_atual_w + 1;

	
	
	if	((pkg_date_utils.is_business_day(hr_atual_w) = 1) or
		(qt_turno_dia_w > 0))  and (ie_gerar_dia_w = 'S') then
		begin
		
		
		select	nvl(max(nr_seq_hora),0) + 1
		into	nr_seq_hora_w
		from	agenda_consulta
		where	cd_agenda	= cd_agenda_p
		and	dt_agenda	= hr_atual_w;

		select	agenda_consulta_seq.nextval
		into 	nr_sequencia_w
		from 	dual;

		cd_turno_w		:= 0;

		if	(to_number(to_char(hr_atual_w,'hh24')) > 12) then
			cd_turno_w	:= 1;
		end if;

		/* leitura do par�metro 19 da agenda de servi�os - ivan em 02/06/2007 os58624 */
		obter_param_usuario(866, 19, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_encaixe_bloq_w);


		/* ivan em 02/06/2007 os58624 */
		if	(ie_encaixe_bloq_w = 'N') then
			/* rafael em 25/05/2007 os57425 */
			if	(cd_tipo_agenda_w = 5) then
				if	(ie_considera_classif_w	= 'N') then
					consistir_bloqueio_agenda(cd_agenda_p, hr_atual_w, obter_cod_dia_semana(hr_atual_w), ie_bloqueio_w);
				else
					consistir_bloq_agenda_servico(cd_agenda_p, hr_atual_w, obter_cod_dia_semana(hr_atual_w), cd_classificacao_p, ie_bloqueio_w);
				end if;
				if	(ie_bloqueio_w = 'N') then
					select	consistir_agenda_hor_bloq_serv(cd_agenda_p, hr_atual_w)
					into	ie_bloqueio_w
					from	dual;
				end if;
			else
				ie_bloqueio_w := 'N';
			end if;
		else
			ie_bloqueio_w := 'N';
		end if;
		
		select	count(*)
		into	qt_agendado_w
		from	agenda_consulta
		where	cd_agenda		= cd_agenda_p
		and	trunc(dt_agenda,'dd')   = trunc(hr_atual_w)
		and	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	ie_status_agenda 	<> 'C';

		if	(ie_bloqueio_w = 'N') then
		
			if	(nr_sessao_atual_w > nr_sessao_total_p) then
				wheb_mensagem_pck.exibir_mensagem_abort(280191,'NR_SESSAO_ATUAL='|| nr_sessao_atual_w ||';NR_SESSAO_TOTAL='|| nr_sessao_total_p);
			end if;
			
			open C01;
			loop
			fetch C01 into					
				nr_seq_regra_encaixe_w,
				hr_inicial_w,
				hr_final_w;
			exit when C01%notfound;
				begin 				
				nr_seq_regra_encaixe_w := nr_seq_regra_encaixe_w;													
				end;
			end loop;
			close C01;
			
			select	nvl(max(qt_permitida),0),
					max(hr_inicial),
					max(hr_final),
					max(cd_convenio)
			into	qt_permitida_regra_w,
					hr_inicial_regra_w,
					hr_final_regra_w,
					cd_convenio_regra_w
			from	agenda_regra_encaixe
			where	nr_sequencia = nr_seq_regra_encaixe_w;	

			
						
			if	(hr_inicial_regra_w is null) then
			
				open C02;
				loop
				fetch C02 into	
					nr_seq_regra_encaixe_w,
					hr_inicial_w,
					hr_final_w;
				exit when C02%notfound;
					begin
					nr_seq_regra_encaixe_w	:= nr_seq_regra_encaixe_w;					
					end;
				end loop;
				close C02;		
								
				select	nvl(max(qt_permitida),0),
						max(hr_inicial),
						max(hr_final),
						max(cd_convenio)
				into	qt_permitida_regra_w,
						hr_inicial_regra_w,
						hr_final_regra_w,
						cd_convenio_regra_w
				from	agenda_regra_encaixe
				where	nr_sequencia = nr_seq_regra_encaixe_w;
							
			end if;
				
			if	(qt_permitida_regra_w > 0 ) then
				begin								
				if	(cd_convenio_regra_w is null) then					
					begin
					
					select	count(*)
					into	qt_encaixe_existe_w
					from	agenda_consulta
					where	ie_encaixe = 'S'
					and		cd_agenda = cd_agenda_p
					and		to_date(to_char(dt_agenda,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
							between to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_inicial_regra_w,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
							to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_final_regra_w,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss') 	
					and		ie_status_agenda <> 'C';
					
					end;
				else
					begin
					
					select	count(*)
					into	qt_encaixe_existe_w
					from	agenda_consulta
					where	ie_encaixe = 'S'
					and		cd_agenda = cd_agenda_p
					and		to_date(to_char(dt_agenda,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
							between to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_inicial_regra_w,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
							to_date(to_char(hr_atual_w,'dd/mm/yyyy') || ' ' || nvl(to_char(hr_final_regra_w,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss') 	
					and		ie_status_agenda <> 'C'
					and		cd_convenio		= cd_convenio_regra_w;
					
					end;
					
				end if;
							
				if	(qt_encaixe_existe_w >= qt_permitida_regra_w) then
					begin
					ie_permite_w	:= 'N';
					ds_erro_w2	:= substr((ds_erro_w2 || to_char(hr_atual_w,'dd/mm hh24:mi') || ', '), 1, 255);
					end;
				end if;
				end;
			elsif	(qt_permitida_regra_w = 0) and (nr_seq_regra_encaixe_w is not null) then
				begin							
				ie_permite_w	:= 'N';
				ds_erro_w2	:= substr((ds_erro_w2 || to_char(hr_atual_w,'dd/mm hh24:mi') || ', '), 1, 255);
				end;
			end if;
				
			if	(qt_agendado_w > 0) and (ie_permite_agendar_dupl_w = 'N') then
				begin
				ie_duplicado_w	:= 'S';
				ds_erro_w3	:= substr((ds_erro_w3 || to_char(hr_atual_w,'dd/mm hh24:mi') || ', '), 1, 255);
				end;
			end if;						
			
			if (ie_utiliza_cont_w = 'S') and (nvl(nr_controle_secao_w, 0) = 0) then
				nr_controle_secao_w	:= obter_controle_secao_agserv(nvl(nr_seq_ref_w, nr_sequencia_w));
			end if;
			
			Consiste_lib_conv_ageserv(cd_agenda_p, cd_convenio_p, hr_atual_w, 'S', null, ds_consistencia_w);
			if	(ds_consistencia_w is not null) then
				ie_permite_w	:= 'N';
				ds_erro_w4	:= substr((ds_erro_w4 || to_char(hr_atual_w,'dd/mm hh24:mi') || ', '), 1, 255);
			end if;	

			select	max(a.nr_sequencia)
			into	nr_seq_agendamento_w
			from	agenda_consulta a,
					agenda b
			where	a.cd_agenda		= b.cd_agenda
			and	b.cd_tipo_agenda	= 5
			and	b.cd_estabelecimento	= cd_estabelecimento_p
			and	b.ie_situacao		= 'A'	
			and	a.nr_sequencia		= (	select	max(x.nr_sequencia)
										from	agenda_consulta x,
												agenda z
										where	x.cd_agenda		= z.cd_agenda
										and		z.cd_tipo_agenda	= 5
										and		z.cd_estabelecimento	= cd_estabelecimento_p
										and		x.cd_pessoa_fisica	= cd_pessoa_fisica_p
										and		x.dt_agenda		< dt_inicio_p
										and		(((ie_busca_inf_ult_agend_w = 'S') and (x.ie_status_agenda = 'E'))
											or	((ie_busca_inf_ult_agend_w = 'A') and (z.cd_agenda = cd_agenda_p) and (x.ie_status_agenda = 'E'))
											or	(ie_busca_inf_ult_agend_w = 'I')));
											
											
		
			if	(ie_busca_inf_ult_agend_w <> 'N') and
				(nr_seq_agendamento_w is not null)	then
			
				select	a.nr_telefone,
						a.nr_doc_convenio,
						a.cd_senha,
						a.dt_validade_carteira,
						a.cd_usuario_convenio,
						a.cd_setor_atendimento,
						nvl(a.cd_convenio, cd_convenio_p),
						a.cd_categoria,
						a.cd_tipo_acomodacao	
				into	nr_telefone_w,
						nr_doc_convenio_w,
						cd_senha_w,
						dt_valida_carteira_w,
						cd_usuario_convenio_w,
						cd_setor_atendimento_ww,
						cd_convenio_w,
						cd_categoria_w,
						cd_tipo_acomodacao_w
				from	agenda_consulta a,
						agenda b
				where	a.cd_agenda		= b.cd_agenda
				and	a.nr_sequencia = nr_seq_agendamento_w;							
			end if;

			if (ie_busca_inf_ult_agend_w = 'S') then
				cd_setor_insert_w := cd_setor_atendimento_ww;
			else
				cd_setor_insert_w := nvl(cd_setor_atendimento_p,cd_setor_atendimento_w);
			end if;
			nr_seq_bloq_geral_w := obter_se_bloq_geral_agenda (cd_estabelecimento_p,
						cd_agenda_p,
						cd_classificacao_w,
						null, --nr_seq_classif_agenda_p
						null, --cd_espec_agendamento_p
						cd_setor_insert_w,
						nr_seq_proc_interno_p,
						cd_procedimento_p,
						ie_origem_proced_p,
						cd_medico_p,
						hr_atual_w,
						'N',
						'N');
			if (nr_seq_bloq_geral_w > 0) then
				Wheb_mensagem_pck.exibir_mensagem_abort(obter_mensagem_bloq_geral_age(nr_seq_bloq_geral_w));
			end if;

			if	(ie_permite_w = 'S') and (ie_duplicado_w = 'N') then
				cd_convenio_ww := cd_convenio_p;
				if (nvl(nr_seq_segurado_p, 0) <> 0) then
					cd_categoria_w := cd_categoria_p;
					cd_usuario_convenio_w := cd_usuario_convenio_p;
					dt_valida_carteira_w := dt_validade_carteira_p;
				elsif (ie_busca_inf_ult_agend_w = 'S') then
					cd_convenio_ww := cd_convenio_w;				
				end if;
				
				insert into agenda_consulta
						(
						nr_sequencia,
						cd_agenda,
						dt_agenda,
						nr_minuto_duracao,
						cd_pessoa_fisica,
						nm_paciente,
						dt_nascimento_pac,
						qt_idade_pac,
						ie_status_agenda,
						ie_classif_agenda,
						dt_atualizacao,
						nm_usuario,
						cd_turno,
						nr_seq_hora,
						cd_medico,
						ds_observacao,
						cd_convenio,
						ie_encaixe,
						dt_agendamento,
						qt_total_secao,
						nr_secao,
						nm_usuario_origem,
						cd_procedimento,
						ie_origem_proced,
						nr_seq_proc_interno,
						cd_setor_atendimento,
						nr_controle_secao,
						cd_medico_solic,
						cd_cid,
						nr_seq_agepaci,
						nr_telefone,
						nr_doc_convenio,
						cd_senha,
						dt_validade_carteira,
						cd_usuario_convenio,												
						cd_categoria,
						cd_tipo_acomodacao,
						IE_CARATER_INTER_SUS, 
						IE_TIPO_ATENDIMENTO,
						nr_seq_segurado,
						cd_plano
						)
				values
						(
						nr_sequencia_w,
						cd_agenda_p,
						hr_atual_w,
						decode(cd_tipo_agenda_w, 5, 0, 30),
						cd_pessoa_fisica_p,
						decode(cd_pessoa_fisica_p, null, nm_paciente_p, obter_nome_pf(cd_pessoa_fisica_p)),
						to_date(substr(obter_dados_pf(cd_pessoa_fisica_p,'DN'),1,10),'dd/mm/yyyy'),
						decode(obter_dados_pf(cd_pessoa_fisica_p,'DN'), null, null, obter_idade(to_date(obter_dados_pf(cd_pessoa_fisica_p,'DN'),'dd/mm/yyyy'),sysdate,'A')),
						'N',
						cd_classificacao_w,
						sysdate,
						nm_usuario_p,
						cd_turno_w,
						decode(cd_tipo_agenda_w, 5, nr_seq_hora_w, 0),
						cd_medico_p,
						ds_observacao_p,						
						cd_convenio_ww, 
						'S',
						sysdate,
						nr_sessao_total_p,
						nr_sessao_atual_w,
						nm_usuario_p,
						cd_procedimento_p,
						ie_origem_proced_p,
						nr_seq_proc_interno_p,
						cd_setor_insert_w,
						nr_controle_secao_w,
						cd_medico_solic_p,
						cd_cid_p,
						nr_seq_agenda_cir_p,
						nr_telefone_w,
						nr_doc_convenio_w,
						cd_senha_w,
						dt_valida_carteira_w,
						cd_usuario_convenio_w,												
						cd_categoria_w,
						cd_tipo_acomodacao_w,
						ie_carater_inter_sus_p,
						ie_tipo_atendimento_p,
						nr_seq_segurado_p,
						cd_plano_p
						);
						
						
				if	(cd_tipo_agenda_w = 5) and (ie_gerar_solic_pront_w = 'S') then
					begin
					gerar_solic_pront_agenda
						(cd_pessoa_fisica_p,
						nr_sequencia_w,
						cd_agenda_p,
						hr_atual_w,
						nm_usuario_p);
					exception
					when others then			
					insert into log_tasy_agenda(
							cd_log,
							dt_atualizacao,
							nm_usuario,
							ds_log)
						values	(4228,
							sysdate,
							nm_usuario_p,
							substr(	'CD_PESSOA_FISICA: '||cd_pessoa_fisica_p||
								', NR_SEQUENCIA: '||nr_sequencia_w||
								', CD_AGENDA: '||cd_agenda_p||
								', DT_AGENDA: '||to_char(hr_atual_w, 'dd/mm/yyyy hh24:mi:ss')||
								', CALLSTACK: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,4000));								
					end;	
				end if;
				
				if	(cd_tipo_agenda_w = 5) and (ie_gerar_solic_pront_gp_w = 'S') and
					(cd_pessoa_fisica_p is not null)then
					Gerar_Solic_Pront_Agenda_GP(cd_pessoa_fisica_p, nr_sequencia_w, cd_agenda_p, hr_atual_w, nm_usuario_p, null, null, null);
				end if;
						
			end if;
			
			
		else
			ds_erro_w := substr((ds_erro_w || to_char(hr_atual_w,'dd/mm hh24:mi') || ', '), 1, 255);
		end if;
		end;
		if	(nr_sessao_atual_w > 0) then
			nr_sessao_atual_w	:= nr_sessao_atual_w + 1;
		end if;
	else
		qt_dias_w	:= qt_dias_w + 1;
	end if;

	if	(ie_seq_dia_p = 'D') then
		hr_atual_w		:= hr_atual_w + 7;
	else
		hr_atual_w		:= hr_atual_w + 1;
	end if;
	
	end;
end loop;

<<final>>
	begin
	ds_erro_ww := '';
	end;

commit;

/* enviar sequencia inserida, atender param 78 agenda servico*/

if	(ds_erro_w is not null) then
	ds_erro_bloqueio_w := wheb_mensagem_pck.get_texto(280198,'DS_ERRO_W='|| substr(ds_erro_w,1,100))||ds_enter_w;
end if;


if	(ds_erro_w2 is not null) then
	ds_erro_regra_w := wheb_mensagem_pck.get_texto(280194,'DS_ERRO_W2='|| substr(ds_erro_w2,1,100));
end if;

if	(ds_erro_w3 is not null) then
	ds_erro_duplicado_w := wheb_mensagem_pck.get_texto(280195,'DS_ERRO_W3='|| substr(ds_erro_w3,1,100))||ds_enter_w;
end if;

if	(ds_erro_w4 is not null) then
	ds_erro_regra_conv_w := wheb_mensagem_pck.get_texto(280197,'DS_ERRO_W4='|| substr(ds_erro_w4,1,100)||';DS_CONSISTENCIA_W='|| ds_consistencia_w)||ds_enter_w;
end if;

ds_erro_p := substr(ds_erro_duplicado_w||ds_erro_bloqueio_w||ds_erro_regra_w||ds_erro_regra_conv_w,1,255);

if 	(nr_sequencia_w <> 0) and 
	(nr_sequencia_w is not null) and
	(ds_erro_p is null) then
	nr_seq_encaixe_p := nr_sequencia_w;
end if;

end gerar_encaixe_agenda_servico;
/