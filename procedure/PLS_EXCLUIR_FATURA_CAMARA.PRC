create or replace
procedure pls_excluir_fatura_camara(	nr_seq_lote_p		number,
					nr_titulo_pagar_p	number,
					nr_titulo_receber_p	number,
					nr_seq_motivo_p		number,
					ds_observacao_p		varchar2,
					dt_exclusao_p		date,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is

ie_acao_w		varchar2(3);
nr_seq_fatura_w		number(10);
nr_seq_protocolo_w	number(10);				

begin

if	(nr_seq_lote_p is not null) and
	((nr_titulo_pagar_p is not null) or (nr_titulo_receber_p is not null))then	
	
	if	(nr_seq_motivo_p is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(265552,'');
		--Mensagem: Deve ser informado um motivo de exclus�o!
	elsif	(dt_exclusao_p is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(265553,'');
		--Deve ser informada a data de exclus�o!
	end if;
	
	if	(nr_titulo_pagar_p is not null) then
		select	a.ie_acao
		into	ie_acao_w
		from	pls_motivo_excl_camara a
		where	a.nr_sequencia	= nr_seq_motivo_p;
		
		select	max(a.nr_sequencia)
		into	nr_seq_fatura_w
		from	ptu_fatura a
		where	a.nr_titulo	= nr_titulo_pagar_p;
		
		if	(nr_seq_fatura_w is not null) then
			select	max(a.nr_seq_protocolo)
			into	nr_seq_protocolo_w
			from	ptu_fatura a
			where	a.nr_sequencia	= nr_seq_fatura_w;
		end if;
		
		if	(ie_acao_w = 'ET') then
			cancelar_titulo_pagar(nr_titulo_pagar_p,nm_usuario_p,dt_exclusao_p);
		elsif	(ie_acao_w = 'EC') and
			(nr_seq_protocolo_w is not null) then
			pls_excluir_protocolo_imp(nr_seq_protocolo_w,nm_usuario_p,cd_estabelecimento_p);
		elsif	(ie_acao_w = 'ECT') and
			(nr_seq_protocolo_w is not null) then
			cancelar_titulo_pagar(nr_titulo_pagar_p,nm_usuario_p,dt_exclusao_p);
			pls_excluir_protocolo_imp(nr_seq_protocolo_w,nm_usuario_p,cd_estabelecimento_p);
		end if;
	end if;
	
	insert into pls_lote_camara_exclusao
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_lote,
		nr_titulo_pagar,
		nr_titulo_receber,
		dt_exclusao,
		nr_seq_motivo_exc,
		ds_observacao)
	values	(pls_lote_camara_exclusao_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_lote_p,
		nr_titulo_pagar_p,
		nr_titulo_receber_p,
		dt_exclusao_p,
		nr_seq_motivo_p,
		ds_observacao_p);
	
	delete from pls_titulo_lote_camara
	where	nr_seq_lote_camara = nr_seq_lote_p
	and	nr_titulo_receber = nr_titulo_receber_p;
	
	delete from pls_titulo_lote_camara
	where	nr_seq_lote_camara = nr_seq_lote_p
	and	nr_titulo_pagar = nr_titulo_pagar_p;
end if;

commit;

end pls_excluir_fatura_camara;
/
