create or replace
procedure excluir_material_cartao_eme(
		nr_seq_lancamento_p		number,
		cd_material_p			number,
		nr_seq_lote_p			number,
		qt_material_p			number,
		nm_usuario_p			varchar2,
		ds_erro_p	out		varchar2) is

qt_existe_w		number(10);
qt_material_w		number(9,3);
cd_material_w		number(6);
nr_seq_item_w		number(10);
ds_erro_w		varchar2(255);

begin

ds_erro_w	:= '';

select	count(*)
into	qt_existe_w
from	sup_lanc_cartao
where	nr_sequencia = nr_seq_lancamento_p
and	nr_atendimento is not null;

if	(qt_existe_w > 0) then
	ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(277807);
else
	begin

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_item_w
	from	sup_lanc_cartao_item
	where	nr_seq_lancamento = nr_seq_lancamento_p
	and	cd_material = cd_material_p
	and	nvl(nr_seq_lote_fornec,0) = nvl(nr_seq_lote_p,0);

	if	(nr_seq_item_w = 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(277808);	
	else
		begin
		
		select	cd_material,
			nvl(max(qt_material),0)
		into	cd_material_w,
			qt_material_w
		from	sup_lanc_cartao_item
		where	nr_sequencia = nr_seq_item_w
		and	nr_seq_lancamento = nr_seq_lancamento_p
		group by cd_material;

		if	(qt_material_w = qt_material_p) then
			begin

			delete
			from	sup_lanc_cartao_item
			where	nr_sequencia = nr_seq_item_w
			and	nr_seq_lancamento = nr_seq_lancamento_p;

			/*gravar_log__tasy(44001,'Exclu�do o material ' || cd_material_w || ' do lan�amento ' || 
					nr_seq_lancamento_p || ' do cart�o emergencial',nm_usuario_p);*/

			end;
	
		elsif	(qt_material_w > qt_material_p) then
			begin

			update	sup_lanc_cartao_item
			set	qt_material = (qt_material_w - qt_material_p)
			where	nr_sequencia = nr_seq_item_w
			and	nr_seq_lancamento = nr_seq_lancamento_p;

			/*gravar_log__tasy(44001,'Diminu�do a quantidade do material ' || cd_material_w || ' do lan�amento ' || 
					nr_seq_lancamento_p || ' do cart�o emergencial de ' || 
					qt_material_w || ' para ' || qt_material_p,nm_usuario_p);*/

			end;
		else
			ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(277809);
		
		end if;

		end;
	end if;

	end;
end if;

if	(ds_erro_w = '') then
	commit;
end if;

ds_erro_p	:= ds_erro_w;

end excluir_material_cartao_eme;
/