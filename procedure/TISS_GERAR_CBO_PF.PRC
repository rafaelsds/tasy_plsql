create or replace
procedure TISS_GERAR_CBO_PF(
			cd_pessoa_fisica_p	in varchar2,
			ie_versao_p		in varchar2,
			nr_seq_cbo_saude_p	in number,
			nm_usuario_p		in varchar2) is

count_w		number(15);
nr_sequencia_w	number(15);

begin

select	count(*)
into	count_w
from	tiss_cbo_saude
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	nr_seq_cbo_saude	= nr_seq_cbo_saude_p
and	ie_versao		= ie_versao_p
and	cd_especialidade	is null;

if	(count_w = 0) then

	select	tiss_cbo_saude_seq.nextval
	into	nr_sequencia_w
	from 	dual;

	insert into tiss_cbo_saude
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_cbo_saude,
		ie_versao,
		cd_pessoa_fisica)
	values	(nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_cbo_saude_p,
		ie_versao_p,
		cd_pessoa_fisica_p);

	commit;	

end if;

end TISS_GERAR_CBO_PF;
/