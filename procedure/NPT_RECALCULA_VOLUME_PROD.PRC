create or replace
procedure NPT_recalcula_volume_prod(	
					nr_sequencia_p			number,                     
					qt_valor_p				number,                                                   
					qt_valor_anterior_p		number,                                            
					qt_vct_p				number,                                                     
					qt_vct_anterior_p		number,
					nm_usuario_p			varchar2,
					qt_volume_diario_p	out number) is                                       

qt_porcento_w				number(10,4);                                                    
qt_porcento_adic_w			number(10,4);                                                
qt_volume_produto_w			number(15,4);                                               
qt_volume_diario_w			number(15,4);                                                
nr_seq_produto_w			number(10);                                                    
qt_diferenca_w				number(10,4);
qt_diferenca2_w				number(10,4);                                                   
qt_valor_w					number(15,4);                                                      
qt_sem_redistribuicao_w		number(15,4);                                            
qt_adicional_w				number(15,4);                                                   
qt_adicionais_w				number(15,4);
qt_kcal_kg_w				number(15,2);
qt_valor_ww					number(15,4);
qt_volume_original_w		number(15,4);                                               
qt_distribuir_nao_distrib_w number(15,4);                                         
qt_total_restribuir_w		number(10);
qt_vol_recalcular_w			number(15,4);
qt_protocolo_w				number(15,4);
qt_prot_prescr_pac_w		number(15,4);
count_w						number(15);
qt_kcal_total_origem_w		number(15,1);                                                
ie_recalcular_w				varchar2(1) := 'N';
qt_peso_w					number(15,3);
qt_valor_anterior_w			number(15,4);
cd_material_aux_w			number(10);

cursor	c01 is                                                                     
select	a.nr_sequencia,
		a.qt_protocolo
from	nut_pac_elemento d,
		nut_elem_material c,                                                         
		nut_elemento b,                                                                 
		nut_pac_elem_mat a                                                              
where	d.nr_seq_nut_pac = a.nr_seq_nut_pac
and		b.nr_sequencia = d.nr_seq_elemento
and		d.nr_seq_elemento = c.nr_seq_elemento
and		a.cd_material	= c.cd_material                                             
and		a.nr_seq_nut_pac	= nr_sequencia_p                                            
and		c.nr_seq_elemento	= b.nr_sequencia                                           
and		nvl(c.ie_tipo,'NPT')	= 'NPT'                                                 
and		nvl(b.ie_redistribuir,'S') = 'S';

cursor c02 is 
select	a.nr_sequencia,                                                            
        round(a.qt_protocolo,4),
		b.qt_vol_recalcular
from	nut_pac_elemento d,
		nut_elem_material c,                                                         
		nut_elemento b,                                                                 
		nut_pac_elem_mat a                                                              
where	d.nr_seq_nut_pac = a.nr_seq_nut_pac
and		b.nr_sequencia = d.nr_seq_elemento
and		d.nr_seq_elemento = c.nr_seq_elemento
and		a.cd_material	= c.cd_material                                                 
and		a.nr_seq_nut_pac	= nr_sequencia_p                                            
and		c.nr_seq_elemento	= b.nr_sequencia                                           
and		nvl(c.ie_tipo,'NPT')	= 'NPT'
and		nvl(b.ie_redistribuir,'S') = 'N'
and		b.qt_vol_recalcular is not null
and		b.qt_vol_recalcular > qt_valor_w;                                                 

cursor c03 is
select	a.nr_sequencia,                                                            
        a.qt_protocolo
from	nut_pac_elemento d,
		nut_elem_material c,                                                         
		nut_elemento b,                                                                 
		nut_pac_elem_mat a                                                              
where	d.nr_seq_nut_pac = a.nr_seq_nut_pac
and		b.nr_sequencia = d.nr_seq_elemento
and		d.nr_seq_elemento = c.nr_seq_elemento
and		a.cd_material	= c.cd_material                                                 
and		a.nr_seq_nut_pac	= nr_sequencia_p                                            
and		c.nr_seq_elemento	= b.nr_sequencia 
and		nvl(c.ie_tipo,'NPT')	= 'NPT'
and		nvl(b.ie_redistribuir,'S') = 'N'
and		b.qt_vol_recalcular is not null
and		b.qt_vol_recalcular <=  qt_valor_w;

cursor c04 is
select	distinct a.cd_material,
		nvl(round(a.qt_protocolo,2),0)
from	nut_pac_elem_mat a,
		nut_elem_material b,
		nut_pac_elemento c,
		nut_elemento d
where	a.cd_material 		= b.cd_material
and		a.nr_seq_nut_pac 	= c.nr_seq_nut_pac
and		b.nr_seq_elemento 	= c.nr_seq_elemento
and		b.nr_seq_elemento 	= d.nr_sequencia
and		c.nr_seq_elemento 	= d.nr_sequencia
and		a.nr_seq_nut_pac 	= nr_sequencia_p
and		nvl(b.ie_tipo,'NPT')	= 'NPT'
and		nvl(d.ie_redistribuir,'S') = 'S';
 
 

begin

qt_valor_w			:= qt_valor_p;                                                                                                                             
qt_valor_anterior_w := qt_valor_anterior_p;
qt_protocolo_w 		:= 0;
open C04;
loop
fetch C04 into cd_material_aux_w,
			   qt_adicionais_w;
exit when C04%notfound;
	begin
	qt_protocolo_w := qt_protocolo_w + qt_adicionais_w;
	end;
end loop;
close C04;

ie_recalcular_w := 'N';
if	(nvl(qt_vct_p,0) > 0) then   
                                                  
	select	max(qt_kcal_total_origem),
			max(nvl(qt_peso_ajustado, qt_peso)),
			max(qt_volume_princ)
	into	qt_kcal_total_origem_w,
			qt_peso_w,
			qt_valor_anterior_w
	from	nut_pac
	where	nr_sequencia = nr_sequencia_p;
	
	qt_valor_w := (((qt_protocolo_w * qt_vct_p) / qt_kcal_total_origem_w));
	qt_volume_diario_p := qt_valor_w;
	ie_recalcular_w := 'S';
	
	update	nut_pac
	set		qt_kcal_kg = qt_vct_p /qt_peso_w,
			qt_kcal_total = qt_vct_p,
			ie_alterou_vct = 'S',
			qt_volume_princ = qt_valor_w
	where	nr_sequencia = nr_sequencia_p;
	
	commit;
		
end if;  

if	(qt_valor_anterior_w	> 0) or
	(ie_recalcular_w = 'S') then                                                 
		
        open C01;                                                                        
        loop                                                                             
        fetch C01 into                                                                   
        	nr_seq_produto_w,
			qt_prot_prescr_pac_w;                                                          
        exit when C01%notfound;                                                          
        	begin
			
			qt_adicional_w	:= ((qt_prot_prescr_pac_w * qt_valor_w)/ qt_protocolo_w);

        	update	nut_pac_elem_mat                                                         
        	set		qt_volume		= qt_adicional_w,
					qt_dose			= Obter_dose_convertida(cd_material,qt_adicional_w,obter_unid_med_usua('ml'),cd_unidade_medida)
        	where	nr_sequencia	= nr_seq_produto_w;
			
        	end;                                                                            
        end loop;                                                                        
        close C01;
end if;

select	count(*)
into	count_w
from	nut_elem_material c,                                                         
		nut_elemento b,                                                                 
		nut_pac_elem_mat a                                                              
where	a.cd_material	= c.cd_material                                               
and		a.nr_seq_nut_pac	= nr_sequencia_p                                            
and		c.nr_seq_elemento	= b.nr_sequencia                                           
and		nvl(c.ie_tipo,'NPT')	= 'NPT'
and		b.qt_vol_recalcular is not null
and		b.qt_vol_recalcular > qt_valor_w;

if 	(count_w > 0) then

	open c02;                                                                        
	loop                                                                             
	fetch c02 into                                                                   
		nr_seq_produto_w,                                                               
		qt_volume_produto_w,
		qt_vol_recalcular_w;                                                           
	exit when c02%notfound;
		begin
		qt_valor_ww		:=	((qt_valor_w / 1000)* qt_volume_produto_w);

		update	nut_pac
		set		ie_alterou_vct = 'N'
		where	nr_sequencia = nr_sequencia_p;
		
		update	nut_pac_elem_mat                                                         
		set		qt_volume	= qt_valor_ww,
				qt_dose			= Obter_dose_convertida(cd_material,qt_valor_ww,obter_unid_med_usua('ml'),cd_unidade_medida)
		where	nr_sequencia	= nr_seq_produto_w;  
		end;
		
	end loop;
	close c02;
else
	open c03;                                                                        
	loop                                                                             
	fetch c03 into                                                                   
		nr_seq_produto_w,                                                               
		qt_volume_produto_w;                                                           
	exit when c03%notfound;
		begin

		update	nut_pac
		set		ie_alterou_vct = 'N'
		where	nr_sequencia = nr_sequencia_p;		

		update	nut_pac_elem_mat                                                         
		set		qt_volume		= qt_volume_produto_w,
				qt_dose			= Obter_dose_convertida(cd_material,qt_volume_produto_w,obter_unid_med_usua('ml'),cd_unidade_medida)
		where	nr_sequencia	= nr_seq_produto_w;  
		end;
		
	end loop;
	close c03;
end if;

commit;

calcular_nut_pac_adulto(nr_sequencia_p, nm_usuario_p);
if	(nvl(qt_vct_p,0) > 0) then   
	
	qt_kcal_kg_w	:= dividir(qt_vct_p,qt_peso_w);
	
	update	nut_pac
	set		qt_kcal_kg = qt_kcal_kg_w
	where	nr_sequencia = nr_sequencia_p;
	
	commit;
end if;  

end NPT_recalcula_volume_prod;
/
