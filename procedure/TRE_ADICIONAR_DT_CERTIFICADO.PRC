create or replace
procedure tre_adicionar_dt_certificado
			(	nr_seq_evento_p		Number,
				nm_usuario_p		Varchar2) is 

nr_seq_inscrito_pre_w		Number(10);

Cursor C01 is
	select	b.nr_sequencia
	from	tre_inscrito		a,
		tre_inscrito_presenca	b,
		tre_evento		c
	where	a.nr_sequencia = b.nr_seq_inscrito
	and	c.nr_sequencia = nr_seq_evento_p
	and	a.nr_seq_evento = c.nr_sequencia
	and	b.ie_presente = 'S';

begin

open C01;
loop
fetch C01 into	
	nr_seq_inscrito_pre_w;
exit when C01%notfound;
	begin
	
	update	tre_inscrito_presenca
	set	dt_certificado = sysdate
	where	nr_sequencia = nr_seq_inscrito_pre_w;
		
	end;
end loop;
close C01;

commit;

end tre_adicionar_dt_certificado;
/