create or replace
procedure INCLUIR_COBRADOR_META
		(nr_seq_cobrador_p		in	number,
		 dt_mesano_referencia_p	in	date,
		 pr_meta_p		in	number,
		 nm_usuario_p		in	varchar2) is


begin

insert into cobrador_meta 
		(nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		nr_seq_cobrador, 
		dt_mesano_referencia,
		pr_meta) 
values		(cobrador_meta_seq.nextval, 
		sysdate, 
		nm_usuario_p,
		sysdate ,
		nm_usuario_p, 
		nr_seq_cobrador_p, 
		trunc(dt_mesano_referencia_p, 'month'),
		pr_meta_p) ;

commit;

end INCLUIR_COBRADOR_META;
/
