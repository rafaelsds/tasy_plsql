create or replace
procedure pls_obter_caminho_arquivo_fat(nr_seq_fatura_p		in	pls_fatura.nr_sequencia%type,
					ds_hash_p		in	varchar2,
					ie_tipo_interface_p	in	pls_regra_arquivo_fatura.ie_tipo_interface%type,
					nm_usuario_p		in	usuario.nm_usuario%type,
					ds_caminho_arquivo_p	out	pls_regra_arquivo_fatura.ds_caminho_arquivo%type,
					ie_tipo_arquivo_p	out	pls_regra_arquivo_fatura.ie_tipo_arquivo%type,
					cd_interface_p		out	pls_regra_arquivo_fatura.cd_interface%type,
					ie_compactar_p		out	pls_regra_arquivo_fatura.ie_compactar%type,
					nr_seq_esquema_xml_p	out	pls_regra_arquivo_fatura.nr_seq_esquema_xml%type,
					nm_arquivo_p		out	pls_regra_arquivo_fatura.nm_arquivo%type,
					nr_seq_regra_p		out	pls_regra_arquivo_fatura.nr_sequencia%type,
					ds_mascara_p		out	pls_regra_arquivo_fatura.nm_mascara_arquivo%type,
					ds_mascara_zip_p	out	pls_regra_arquivo_fatura.nm_mascara_arquivo_zip%type,
					ds_interface_p		out	pls_regra_arquivo_fatura.ds_interface%type,
					ie_tipo_compactar_p	out	pls_regra_arquivo_fatura.ie_tipo_compactar%type,
					ds_mascara_adic_p	out	pls_regra_arquivo_fatura.nm_mascara_arquivo_adic%type) is
					
ie_ptu_vigente_w		varchar2(1);
ie_tipo_faturamento_w		varchar2(2);
ie_tipo_arquivo_w		varchar2(3);
cd_cooperativa_w		varchar2(10);
nm_arquivo_w			varchar2(255);
ds_caminho_arquivo_w		varchar2(255);
cd_interface_w			number(5);
ie_operadora_w			number(5);
ie_cooperativa_w		number(5);
nr_seq_cooperativa_w		number(10);
nr_seq_pagador_w		number(10);
nr_seq_contrato_w		number(10);
nr_seq_camara_w			number(10);
nr_contrato_w			number(10);
nr_seq_lote_w			number(10);
nr_titulo_w			number(10);
nr_seq_intercambio_w		number(10);
cd_operadora_empresa_w		number(10);
nr_seq_regra_fat_w		number(10);
					
Cursor	C01 is
	select	a.ds_caminho_arquivo,
		a.ie_ptu_vigente,
		a.ie_tipo_arquivo,
		a.cd_interface,
		a.ie_compactar,
		a.nr_seq_esquema_xml,
		a.nm_arquivo,
		a.nr_sequencia,
		a.nm_mascara_arquivo,
		a.nm_mascara_arquivo_zip,
		a.ds_interface,
		nvl(a.ie_tipo_compactar, 'A') ie_tipo_compactar,
		a.nm_mascara_arquivo_adic
	from	pls_regra_arquivo_fatura a
	where	(a.ie_tipo_faturamento	= ie_tipo_faturamento_w 	or a.ie_tipo_faturamento is null)
	and	(a.nr_seq_cooperativa 	= nr_seq_cooperativa_w		or a.nr_seq_cooperativa is null)
	and	(a.nr_seq_contrato 	= nr_seq_contrato_w		or a.nr_seq_contrato is null)
	and	(a.cd_operadora_empresa = cd_operadora_empresa_w	or a.cd_operadora_empresa is null)
	and	(a.nr_seq_regra_fat	= nr_seq_regra_fat_w		or a.nr_seq_regra_fat is null)
	and	(a.ie_tipo_interface	= ie_tipo_interface_p)
	order by
		nvl(a.cd_operadora_empresa,0),
		nvl(a.nr_seq_contrato,0),
		nvl(a.nr_seq_cooperativa,0),
		nvl(a.nr_seq_regra_fat,0),
		nvl(a.ie_tipo_faturamento,0);
		
begin
if	(nr_seq_fatura_p is not null) then
	select	nr_seq_congenere,
		nr_seq_pagador,
		nr_seq_lote
	into	nr_seq_cooperativa_w,
		nr_seq_pagador_w,
		nr_seq_lote_w
	from	pls_fatura
	where	nr_sequencia = nr_seq_fatura_p;
	
	select	max(nr_seq_regra_fat)
	into	nr_seq_regra_fat_w
	from	pls_lote_faturamento
	where	nr_sequencia = nr_seq_lote_w;
	
	select	max(nr_seq_contrato),
		max(nr_seq_intercambio),
		max(cd_operadora_empresa)
	into	nr_seq_contrato_w,
		nr_seq_intercambio_w,
		cd_operadora_empresa_w
	from	pls_segurado
	where	nr_seq_pagador = nr_seq_pagador_w;
	
	if	(cd_operadora_empresa_w is null) then
		select	max(cd_operadora_empresa)
		into	cd_operadora_empresa_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;
	end if;
	
	ie_tipo_faturamento_w	:= 'P'; /* Padr�o p�s */
	
	if	(nr_seq_intercambio_w is not null) then
		select	count(1)
		into	ie_cooperativa_w
		from	pls_intercambio
		where	nr_sequencia = nr_seq_intercambio_w
		and	nr_seq_congenere is not null;
		
		if	(ie_cooperativa_w = 0) then
			select	count(1)
			into	ie_operadora_w
			from	pls_intercambio
			where	nr_sequencia = nr_seq_intercambio_w
			and	nr_seq_oper_congenere is not null;
			
			if	(ie_operadora_w > 0) then
				ie_tipo_faturamento_w := 'PO'; /* Operadoras cong�neres */
			end if;
		else
			ie_tipo_faturamento_w := 'PI'; /* Cooperativas m�dicas */
		end if;
	end if;
	
	if	(nr_seq_pagador_w is not null) and
		(nr_seq_cooperativa_w is not null) then		
		ie_tipo_faturamento_w	:= 'IC';
	end if;
	
	open C01;
	loop
	fetch C01 into
		ds_caminho_arquivo_w,
		ie_ptu_vigente_w,
		ie_tipo_arquivo_w,
		cd_interface_w,
		ie_compactar_p,
		nr_seq_esquema_xml_p,
		nm_arquivo_p,
		nr_seq_regra_p,
		ds_mascara_p,
		ds_mascara_zip_p,
		ds_interface_p,
		ie_tipo_compactar_p,
		ds_mascara_adic_p;
	exit when C01%notfound;
		begin
		null;
		end;
	end loop;
	close C01;
	
	if	(ie_ptu_vigente_w = 'S') then
		cd_interface_w 		:= '';
		ie_tipo_arquivo_w	:= 'TXT';
	end if;
	
	if	(nr_seq_cooperativa_w is not null) then
		select	cd_cooperativa
		into	cd_cooperativa_w
		from	pls_congenere
		where	nr_sequencia = nr_seq_cooperativa_w;
	end if;
	
	if	(nr_seq_contrato_w is not null) then
		select	nr_contrato
		into	nr_contrato_w
		from	pls_contrato
		where	nr_sequencia = nr_seq_contrato_w;
	end if;
	
	if	(substr(ds_caminho_arquivo_w,length(ds_caminho_arquivo_w)-1,length(ds_caminho_arquivo_w)) = '\') or 
		(substr(ds_caminho_arquivo_w,length(ds_caminho_arquivo_w)-1,length(ds_caminho_arquivo_w)) = '/')then
		ds_caminho_arquivo_w	:= substr(ds_caminho_arquivo_w,1,length(ds_caminho_arquivo_w)-1);
	end if;

	ds_caminho_arquivo_w	:= substr(replace_macro(ds_caminho_arquivo_w,'@CD_COOPERATIVA',cd_cooperativa_w),1,255);
	ds_caminho_arquivo_w	:= substr(replace_macro(ds_caminho_arquivo_w,'@NR_CONTRATO',nr_contrato_w),1,255);
	ds_caminho_arquivo_w	:= substr(replace_macro(ds_caminho_arquivo_w,'@ANO', to_char(sysdate,'yyyy')),1,255);
	ds_caminho_arquivo_w	:= substr(replace_macro(ds_caminho_arquivo_w,'@MES', to_char(sysdate,'mm')),1,255);
	ds_caminho_arquivo_w	:= substr(replace_macro(ds_caminho_arquivo_w,'@DIA', to_char(sysdate,'dd')),1,255);
	
	ds_caminho_arquivo_p	:= ds_caminho_arquivo_w;
	cd_interface_p 		:= cd_interface_w;
	ie_tipo_arquivo_p	:= ie_tipo_arquivo_w;
end if;
end	pls_obter_caminho_arquivo_fat;
/