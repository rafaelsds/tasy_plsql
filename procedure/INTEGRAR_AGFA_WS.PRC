create or replace
procedure integrar_agfa_ws( nr_atendimento_p 	number,
							nr_prescricao_p 	number,
                            nr_seq_prescr_p 	number,
                            nm_usuario_p 		varchar2,
                            ie_cancelamento_p 	varchar2,
                            ie_commit_p 		varchar2) is
                            
ds_sep_bv_w				varchar2(1);
ds_param_integ_xml_w	varchar2(4000);

enviarProntuario_w		number(3) := 535;
enviarProfissional_w	number(3) := 536;
enviarAtendimento_w		number(3) := 537;
enviarCancelarPedido_w	number(3) := 541;

ie_envia_mensagem_w		varchar2(1);
ds_log_w				varchar2(2000);

begin
	begin
		ie_envia_mensagem_w := 'N';
		ds_log_w := '';
		ds_sep_bv_w := ';';
		
		if (upper(ie_cancelamento_p) = 'N') then
			
			select	decode(count(*),0,'N','S')
			into	ie_envia_mensagem_w
			from	prescr_procedimento a,
					lab_exame_equip b,
					equipamento_lab c,
					material_exame_lab d
			where	a.nr_seq_exame    = b.nr_seq_exame
			and		b.cd_equipamento  = c.cd_equipamento	
			and 	a.cd_material_exame = d.cd_material_exame
			and		a.nr_prescricao   = nr_prescricao_p
			and		(b.nr_seq_material = d.nr_sequencia or b.nr_seq_material is null)
			and		c.ds_sigla = 'AGFAWS'
			and		(nr_seq_prescr_p is null or a.nr_sequencia = nr_seq_prescr_p)
			and		a.ie_suspenso = 'N';
		
			if (ie_envia_mensagem_w = 'S') then
			
				/* Foi requisitado pela AGFA que o Tasy enviasse as mensagens de prontu�rio, profissional, atendimento e pedido nessa ordem e o Tasy deve esperar a primeira mensagem ser processada para s� ent�o enviar a segunda,
				 * Ficou decidido que seria gerado o agendamento_integracao apenas do prontu�rio, e no envio do prontu�rio seria feito o envio das mensagens de profissional, atendimento e pedido, isso foi tratado diretamente no c�digo Java.
				 * OS 984162
				 */
				if (wheb_usuario_pck.is_evento_ativo(enviarProntuario_w) = 'S' and wheb_usuario_pck.is_evento_ativo(enviarProfissional_w) = 'S' and wheb_usuario_pck.is_evento_ativo(enviarAtendimento_w) = 'S' and wheb_usuario_pck.is_evento_ativo(enviarCancelarPedido_w) = 'S') then
					ds_param_integ_xml_w := 'nr_atendimento=' || to_char(nr_atendimento_p) || ds_sep_bv_w || 'nr_prescricao=' || to_char(nr_prescricao_p) || ds_sep_bv_w || 'tipo_operacao_pedido=I' || ds_sep_bv_w|| 'tipo_operacao_exame=I' || ds_sep_bv_w;
					ds_log_w := substr('AGFA - nr_seq_evento_p: ' || to_char(enviarProntuario_w) || ' - ' || ds_param_integ_xml_w, 1, 2000);
					gravar_log_lab(66, ds_log_w, nm_usuario_p, nr_prescricao_p, 'AGFA');
					gravar_agend_integracao(enviarProntuario_w, ds_param_integ_xml_w);	

					if (ie_commit_p = 'S') then
						if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
					end if;
				end if;
			end if;	
			  
		elsif (upper(ie_cancelamento_p) = 'S') then
			-- Cancela no AGFA os itens que est�o suspensos.
			if (wheb_usuario_pck.is_evento_ativo(enviarCancelarPedido_w) = 'S') then
				ds_param_integ_xml_w := 'nr_prescricao=' || to_char(nr_prescricao_p) || ds_sep_bv_w || 'nr_seq_prescr=' || to_char(nr_seq_prescr_p) || ds_sep_bv_w || 'tipo_operacao_pedido=A' || ds_sep_bv_w || 'tipo_operacao_exame=E' || ds_sep_bv_w;
				ds_log_w := substr('AGFA - nr_seq_evento_p: ' || to_char(enviarCancelarPedido_w) || ' - ' || ds_param_integ_xml_w, 1, 2000);
				gravar_log_lab(66, ds_log_w, nm_usuario_p, nr_prescricao_p, 'AGFA');
				gravar_agend_integracao(enviarCancelarPedido_w, ds_param_integ_xml_w);
				
				if (ie_commit_p = 'S') then
					if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
				end if;
			end if;
		end if;
	exception when others then
		 gravar_log_lab(66, 'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM, nm_usuario_p, nr_prescricao_p, 'AGFA');
    end; 
end integrar_agfa_ws;
/
