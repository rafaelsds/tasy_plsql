create or replace
procedure inativar_informacao(	nm_tabela_p		varchar2,
				nm_chave_p		varchar2,
				qt_chave_p		number,
				ds_justificativa_p	varchar2,
				nm_usuario_p		varchar2) is

ds_sep_bv_w		varchar2(10);
ds_comando_w		varchar2(300);
nr_atendimento_w	number(10);
qt_isol_w		number(5);
ie_libera_isol_w	varchar2(2);
ds_observacao_w		varchar2(255);
nr_sequencia_w		number(10);
ie_tipo_w		varchar2(10);
nr_interno_conta_w	number(10);
cd_motivo_exc_conta_w	number(10);
cd_estabelecimento_w	number(10);
qt_ie_situacao_w		number(10);
ds_parametros_w		varchar2(4000);
QT_TAMANHO_w		number(10);
ds_justificativa_w	varchar2(4000)	:= ds_justificativa_p;
qt_worklist_w		varchar2(1);
qt_regulacao_w		varchar2(1);
cd_pessoa_fisica_w		wl_worklist.cd_pessoa_fisica%type;
ie_escala_w				wl_worklist.ie_escala%type;
nr_seq_regra_w			wl_worklist.nr_seq_regra%type;
ie_tipo_diagnostico_w	wl_worklist.ie_tipo_diagnostico%type;
nr_seq_classif_diag_w	wl_worklist.nr_seq_classif_diag%type;
cd_doenca_w				diagnostico_doenca.cd_doenca%type;
dt_diagnostico_w		diagnostico_doenca.dt_diagnostico%type;
ie_gerar_aprovacao_w number(1);

cursor	c01 is
	select	'P' ie_tipo,
		a.nr_sequencia,
		a.nr_interno_conta
	from	procedimento_paciente a,
		conta_paciente b
	where	a.nr_interno_conta 	= b.nr_interno_conta
	and	b.nr_Atendimento	= nr_atendimento_w
	and	b.ie_status_acerto	= 1
	and	a.cd_motivo_exc_conta is null
	and	a.ds_observacao = 'PEP PROC:'||qt_chave_p;



begin


select	nvl(max(QT_TAMANHO),0)
into	QT_TAMANHO_w
from	tabela_atributo
where	nm_tabela	= nm_tabela_p
and	nm_atributo	= 'DS_JUSTIFICATIVA';

if	(QT_TAMANHO_w	> 0) then
	ds_justificativa_w	:= substr(ds_justificativa_w,1,QT_TAMANHO_w);
end if;


select 	obter_separador_bv
into	ds_sep_bv_w
from 	dual;

if	(QT_TAMANHO_w	> 0) then
	ds_parametros_w	:= 'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ||
								   'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ||
								   'ds_justificativa='||ds_justificativa_w|| ds_sep_bv_w;
else
	ds_parametros_w	:= 'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ||
								   'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ;
end if;


obter_param_usuario(281, 963, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_libera_isol_w);

if	(QT_TAMANHO_w =  0) then

        ds_comando_w :=	' update ' || nm_tabela_p ||
			' set dt_inativacao = sysdate, ' ||
			' ie_situacao = ' || '''I''' || ',' ||
			' nm_usuario_inativacao = :nm_usuario ' ||
			' where ' || nm_chave_p || ' = :qt_chave ';

elsif	(nm_tabela_p = 'POSICAO_CIRURGIA') or
	(nm_tabela_p = 'CIRURGIA_ITEM_CONTROLE') or
	(nm_tabela_p = 'AVAL_PRE_ANESTESICA') then
	ds_comando_w :=	' update ' || nm_tabela_p ||
			' set ie_situacao = ' || '''I''' || ',' ||
			' dt_inativacao = sysdate, ' ||
			' nm_usuario_inativacao = :nm_usuario, ' ||
			' ds_justificativa_inativacao = :ds_justificativa ' ||
			' where ' || nm_chave_p || ' = :qt_chave ';
	ds_parametros_w	:= 'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ||
				 'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ||
				 'ds_justificativa='||ds_justificativa_w|| ds_sep_bv_w;
elsif	(nm_tabela_p = 'PACIENTE_JUSTIFICATIVA') then
	ds_comando_w :=	' update ' || nm_tabela_p ||
			' set ie_situacao = ' || '''I''' || ',' ||
			' dt_inativacao = sysdate, ' ||
			' nm_usuario_inativacao = :nm_usuario, ' ||
			' ds_justificativa_inativacao = :ds_justificativa ' ||
			' where ' || nm_chave_p || ' = :qt_chave ';
	ds_parametros_w	:= 'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ||
								   'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ||
								   'ds_justificativa='||ds_justificativa_w|| ds_sep_bv_w;


elsif	(nm_tabela_p = 'CIH_PAC_FAT_RISCO') then
	ds_comando_w :=	' update ' || nm_tabela_p ||
			' set dt_inativacao = sysdate, ' ||
			' nm_usuario_inativacao = :nm_usuario, ' ||
			' ds_justificativa = :ds_justificativa ' ||
			' where ' || nm_chave_p || ' = :qt_chave ';

elsif	(nm_tabela_p = 'CIRURGIA_PARTICIPANTE') then
	ds_comando_w :=	' update ' || nm_tabela_p ||
            ' set ie_situacao = ' || '''I''' || ',' ||
			' dt_inativacao = sysdate, ' ||
			' nm_usuario_inativacao = :nm_usuario, ' ||
			' ds_justificativa = :ds_justificativa ' ||
			' where ' || nm_chave_p || ' = :qt_chave ';

elsif	(nm_tabela_p = 'EV_EVENTO_PACIENTE')   then
	ds_comando_w :=	' update ' || nm_tabela_p ||
				' set ie_situacao = ' || '''I'''   ||
				' where ' || nm_chave_p || ' = :qt_chave ';
	ds_parametros_w	:= 'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ;
elsif	(nm_tabela_p = 'TX_ENCAMINHAMENTO')   then
	ds_comando_w :=	' update ' || nm_tabela_p ||
		' set dt_inativacao = sysdate, ' ||
		' nm_usuario_inativacao = :nm_usuario ' ||
		' where ' || nm_chave_p || ' = :qt_chave ';
	ds_parametros_w	:= 'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ||
			    'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ;
elsif	(nm_tabela_p = 'TX_LIBERACAO_LISTA')   then
	ds_comando_w :=	' update ' || nm_tabela_p ||
		' set dt_inativacao = sysdate, ' ||' ie_situacao = ' || '''I'''   || ','||
		' nm_usuario_inativacao = :nm_usuario ' ||
		' where ' || nm_chave_p || ' = :qt_chave ';
	ds_parametros_w	:= 'qt_chave='|| to_char(qt_chave_p) 	|| ds_sep_bv_w ||
			    'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ;
elsif	(nm_tabela_p = 'CIH_HIGIENIZACAO_MAOS') then
	ds_comando_w :=	' update ' || nm_tabela_p ||
		' set dt_inativacao = sysdate, ' ||
		' nm_usuario_inativo = :nm_usuario ' ||
		' where ' || nm_chave_p || ' = :qt_chave ';
	ds_parametros_w	:= 'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ||
			    'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ;
elsif	(nm_tabela_p = 'AGENDA_CONS_HIST')  then
	ds_comando_w :=	' update ' || nm_tabela_p ||
			' set ie_situacao = ' || '''I''' || ',' ||
			' dt_inativacao = sysdate, ' ||
			' nm_usuario_inativacao = :nm_usuario ' ||
			' where ' || nm_chave_p || ' = :qt_chave ';
elsif	(upper(nm_tabela_p)	= 'ATEND_ESCUTA_INICIAL') then

	ds_comando_w :=	' update ' || nm_tabela_p ||
			' set ie_situacao = ' || '''I''' || ',' ||
			' dt_inativacao = sysdate, ' ||
			' ds_justificativa = :ds_justificativa, ' ||
			' nm_usuario_inativacao = :nm_usuario ' ||
			' where ' || nm_chave_p || ' = :qt_chave ';

	Liberar_inativar_acolhimento(qt_chave_p, nm_usuario_p, 'I',ds_justificativa_p);
elsif	(nm_tabela_p = 'MPREV_REGRA_CUBO')  then
	ds_comando_w :=	' update ' || nm_tabela_p ||
			' set dt_inativacao = sysdate, ' ||
			' nm_usuario_inativacao = :nm_usuario ' ||
			' where ' || nm_chave_p || ' = :qt_chave ';

elsif	(upper(nm_tabela_p)	= 'DIRETRIZ_ATENDIMENTO') then
	ds_comando_w	:=	' update ' || nm_tabela_p ||
				' set dt_inativacao = sysdate, ' ||
				' ie_status = :ie_status, ' ||
                ' ie_situacao = ' || '''I''' || ',' ||
				' ds_justificativa = :ds_justificativa, ' ||
				' dt_atualizacao = sysdate, ' ||
				' nm_usuario_inativacao = :nm_usuario, ' ||
				' nm_usuario = :nm_usuario ' ||
				' where ' || nm_chave_p || ' = :qt_chave ';

	ds_parametros_w := 'ie_status='|| 'I' || ds_sep_bv_w ||
				'ds_justificativa='|| ds_justificativa_w || ds_sep_bv_w ||
				'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ||
				'nm_usuario='|| nm_usuario_p || ds_sep_bv_w;

elsif	(upper(nm_tabela_p)	= 'DOCUMENTO_FARMACIA') then
	ds_comando_w	:=	' update ' || nm_tabela_p ||
				' set dt_inativacao = sysdate, ' ||
				' ie_situacao = ' || '''I''' || ',' ||
				' dt_atualizacao = sysdate, ' ||
				' nm_usuario_inativacao = :nm_usuario, ' ||
				' nm_usuario = :nm_usuario ' ||
				' where ' || nm_chave_p || ' = :qt_chave ';

	ds_parametros_w := 	'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w ||
				'nm_usuario='|| nm_usuario_p || ds_sep_bv_w;

elsif	(upper(nm_tabela_p)	= 'OFT_ACUIDADE_VISUAL') or
	(upper(nm_tabela_p)	= 'OFT_DNP') or
	(upper(nm_tabela_p)	= 'OFT_ANGIO_RETINO') or
	(upper(nm_tabela_p)	= 'OFT_BIOMICROSCOPIA') or
	(upper(nm_tabela_p)	= 'OFT_AUTO_REFRACAO') or
	(upper(nm_tabela_p)	= 'OFT_OLHO_SECO') or
	(upper(nm_tabela_p)	= 'OFT_CAMPIMETRIA') or
	(upper(nm_tabela_p)	= 'OFT_CORRECAO_ATUAL') or
	(upper(nm_tabela_p)	= 'OFT_DALTONISMO') or
	(upper(nm_tabela_p)	= 'OFT_FOTOCOAGULACAO_LASER') or
	(upper(nm_tabela_p)	= 'OFT_FUNDOSCOPIA') or
	(upper(nm_tabela_p)	= 'OFT_POTENCIAL_ACUIDADE') or
	(upper(nm_tabela_p)	= 'OFT_GONIOSCOPIA') or
	(upper(nm_tabela_p)	= 'OFT_IRIDECTOMIA') or
	(upper(nm_tabela_p)	= 'OFT_MICROSCOPIA_ESPECULAR') or
	(upper(nm_tabela_p)	= 'OFT_MOTILIDADE_OCULAR') or
	(upper(nm_tabela_p)	= 'OFT_PAQUIMETRIA') or
	(upper(nm_tabela_p)	= 'OFT_BIOMETRIA') or
	(upper(nm_tabela_p)	= 'OFT_EXAME_EXTERNO') or
	(upper(nm_tabela_p)	= 'OFT_TONOMETRIA') or
	(upper(nm_tabela_p)	= 'OFT_CAPSULOTOMIA') or
	(upper(nm_tabela_p)	= 'OFT_CERASTOCOPIA') or
	(upper(nm_tabela_p)	= 'OFT_TOMOGRAFIA_OLHO') or
	(upper(nm_tabela_p)	= 'OFT_PUPILOMETRIA') or
	(upper(nm_tabela_p)	= 'OFT_CURVA_TENCIONAL') or
	(upper(nm_tabela_p)	= 'OFT_TOPOGRAFIA_CORNEANA') or
	(upper(nm_tabela_p)	= 'OFT_OCT') or
	(upper(nm_tabela_p)	= 'OFT_REFRACAO') then

	ds_comando_w	:=	' update ' || nm_tabela_p ||
				' set dt_inativacao = sysdate, ' ||
				' ie_situacao = ' || '''I''' || ',' ||
				' nm_usuario_inativacao = :nm_usuario ' ||
				' where ' || nm_chave_p || ' = :qt_chave ';

	ds_parametros_w := 'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ||
				'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w;

elsif	(upper(nm_tabela_p)	= 'PRE_PESSOA_JURIDICA') then

	ds_comando_w	:=	' update ' || nm_tabela_p ||
				' set dt_inativacao = sysdate, ' ||
				' ie_situacao = ' || '''I''' || ',' ||
				' nm_usuario_inativacao = :nm_usuario ' ||
				' where ' || nm_chave_p || ' = :qt_chave ';

	ds_parametros_w := 'nm_usuario='|| nm_usuario_p || ds_sep_bv_w ||
				'qt_chave='|| to_char(qt_chave_p) || ds_sep_bv_w;

else
	select	count(1)
	into	qt_ie_situacao_w
	from	user_tab_columns a
	where	a.table_name = nm_tabela_p
	and		a.column_name = 'IE_SITUACAO';

	if	(qt_ie_situacao_w = 0) then
		ds_comando_w :=	' update ' || nm_tabela_p ||
				' set dt_inativacao = sysdate, ' ||
				' nm_usuario_inativacao = :nm_usuario, ' ||
				' ds_justificativa = :ds_justificativa ' ||
				' where ' || nm_chave_p || ' = :qt_chave ';
	else
		ds_comando_w :=	' update ' || nm_tabela_p ||
				' set ie_situacao = ' || '''I''' || ',' ||
				' dt_inativacao = sysdate, ' ||
				' nm_usuario_inativacao = :nm_usuario, ' ||
				' ds_justificativa = :ds_justificativa ' ||
				' where ' || nm_chave_p || ' = :qt_chave ';
	end if;
end if;

if	(ie_libera_isol_w in ('S','I')) and
	(nm_tabela_p = 'ATENDIMENTO_PRECAUCAO') then

	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	atendimento_precaucao
	where	nr_sequencia = qt_chave_p;

	select	count(*)
	into	qt_isol_w
	from	atendimento_precaucao a
	where	a.nr_atendimento = nr_atendimento_w
	and	a.dt_final_precaucao is null
	and	a.dt_liberacao is not null
	and	a.dt_inativacao is null
	and	((a.dt_termino is null) or (sysdate between nvl(a.dt_inicio,sysdate-1) and a.dt_termino))
	and	a.nr_sequencia	<> qt_chave_p;

	gerar_motivo_isolamento(nr_atendimento_w,0,nm_usuario_p,'T',null,null,null,null,null,null,qt_chave_p);

	if	(qt_isol_w = 0) then
		update	atendimento_paciente
		set	ie_paciente_isolado	= 'N'
		where	nr_atendimento = nr_atendimento_w;

		if	(ie_libera_isol_w = 'I') then
			update	atend_paciente_hist
			set		dt_inativacao = sysdate,
					nm_usuario_inativacao = nm_usuario_p
			where	nr_sequencia = (select	max(nr_sequencia)
									from	atend_paciente_hist
									where	nr_atendimento = nr_atendimento_w
									and		dt_final is not null
									and		ie_evento = 'I');

		end if;
	end if;
elsif	(nm_tabela_p = 'PACIENTE_PFLEGEGRAD') then

	update 	paciente_pflegegrad
	set 	dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p,
		ds_justificativa = ds_justificativa_w,
		ie_situacao = 'I'
	where 	nr_sequencia = qt_chave_p;

	update 	procedimento_pac_medico
	set	dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p,
		ds_justificativa = ds_justificativa_w,
		ie_situacao = 'I'
	where 	nr_seq_pflegegrad = qt_chave_p;

	select	max(a.nr_interno_conta),
			max(a.nr_sequencia),
			max(b.nr_atendimento)
	into	nr_interno_conta_w,
			nr_sequencia_w,
			nr_atendimento_w
	from	procedimento_paciente a,
			procedimento_pac_medico b
	where	b.nr_seq_propaci	= a.nr_sequencia
	and		b.nr_seq_pflegegrad	= qt_chave_p;

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;

	select	max(cd_motivo_exc_conta)
	into	cd_motivo_exc_conta_w
	from	parametro_faturamento
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(nr_interno_conta_w is not null) and
		(nr_sequencia_w is not null) then
		excluir_matproc_conta(nr_sequencia_w,nr_interno_conta_w,cd_motivo_exc_conta_w, obter_desc_expressao(726883), 'P', wheb_usuario_pck.get_nm_usuario);
	end if;

elsif	(nm_tabela_p	= 'PROC_PAC_DESCRICAO') then

	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	PROC_PAC_DESCRICAO
	where	nr_sequencia = qt_chave_p;

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;

	select	max(cd_motivo_exc_conta)
	into	cd_motivo_exc_conta_w
	from	parametro_faturamento
	where	cd_estabelecimento	= cd_estabelecimento_w;

	open C01;
	loop
	fetch C01 into
		ie_tipo_w,
		nr_sequencia_w,
		nr_interno_conta_w;
	exit when C01%notfound;
		begin
		excluir_matproc_conta(nr_sequencia_w,nr_interno_conta_w,cd_motivo_exc_conta_w, obter_desc_expressao(726883), ie_tipo_w,nm_usuario_p);   --'Inativacao Proc PEP'
		end;
	end loop;
	close C01;

elsif	(nm_tabela_p	= 'PROCEDIMENTO_PAC_MEDICO') then

	select	max(a.nr_interno_conta),
		max(a.nr_sequencia),
		max(b.nr_atendimento)
	into	nr_interno_conta_w,
		nr_sequencia_w,
		nr_atendimento_w
	from	procedimento_paciente a,
		procedimento_pac_medico b
	where	b.nr_seq_propaci	= a.nr_sequencia
	and	b.nr_sequencia		= qt_chave_p;

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;

	select	max(cd_motivo_exc_conta)
	into	cd_motivo_exc_conta_w
	from	parametro_faturamento
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(nr_interno_conta_w is not null) and
		(nr_sequencia_w is not null) then
		excluir_matproc_conta(nr_sequencia_w,nr_interno_conta_w,cd_motivo_exc_conta_w, obter_desc_expressao(726883), 'P',nm_usuario_p);
	end if;

	INATIVAR_DRG_EPISODIO_PACIENTE(nr_atendimento_w, qt_chave_p, upper(nm_tabela_p), nm_usuario_p, 'IN');

end if;

if	(upper(nm_tabela_p)	= 'EHR_REGISTRO') then
	inativar_registro_template(qt_chave_p,nm_usuario_p,ds_justificativa_p);
elsif	(upper(nm_tabela_p)	= 'GPI_PROJETO_HIST') then
	GPI_INATIVAR_HISTORICO(qt_chave_p,nm_usuario_p);
else
	/*Inativacao itens PEP*/
	exec_sql_dinamico_bv(Wheb_mensagem_pck.get_Texto(303954), ds_comando_w, ds_parametros_w);

	delete
	from	w_jornada_pac_problema
	where	NR_SEQ_REGISTRO in (select 	nr_sequencia
								from	w_jornada_pac_registro
								where	nr_seq_origem = qt_chave_p);
	commit;

	delete
	from	w_jornada_pac_registro
	where	nr_seq_origem = qt_chave_p;

	commit;
end if;


select	decode(count(*), 0, 'N', 'S')
into	qt_worklist_w
from	wl_worklist;

if (qt_worklist_w = 'S') then
	-- Clinical notes
	select	nvl2(max(nr_sequencia), 'S','N'),
			max(nr_atendimento),
			max(cd_pessoa_fisica)
	into 	qt_worklist_w,
			nr_atendimento_w,
			cd_pessoa_fisica_w
	from	wl_worklist
	where	cd_evolucao = qt_chave_p;

	if (qt_worklist_w = 'S') then
		wl_gerar_finalizar_tarefa('CN','R',nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p,sysdate,'S',null,null,null,null,null,null,null,null,null,null,null,null,null,qt_chave_p);
	end if;

	-- Scores
	select	nvl2(max(nr_sequencia), 'S','N'),
			max(nr_atendimento),
			max(cd_pessoa_fisica),
			max(ie_escala)
	into 	qt_worklist_w,
			nr_atendimento_w,
			cd_pessoa_fisica_w,
			ie_escala_w
	from	wl_worklist
	where	nr_seq_escala = qt_chave_p
	and		nm_tabela_escala = nm_tabela_p;

	if (qt_worklist_w = 'S') then
		wl_gerar_finalizar_tarefa('S','R',nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p,null,'S',null,null,null,null,null,null,ie_escala_w,null,null,null,null,null,qt_chave_p,null,null,nm_tabela_p);
	end if;

	-- Estimated discharge
	select	nvl2(max(nr_sequencia), 'S','N'),
			max(nr_atendimento),
			max(cd_pessoa_fisica),
			max(nr_seq_regra)
	into 	qt_worklist_w,
			nr_atendimento_w,
			cd_pessoa_fisica_w,
			nr_seq_regra_w
	from	wl_worklist
	where	nr_seq_prev_alta = qt_chave_p;

	if (qt_worklist_w = 'S') then
		wl_gerar_finalizar_tarefa('ED','R',nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p,sysdate,'S',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,null,null,null,null,qt_chave_p);
	end if;

	-- Diagnosis
	select	nvl2(max(nr_sequencia), 'S','N'),
			max(nr_atendimento),
			max(cd_pessoa_fisica),
			max(ie_tipo_diagnostico),
			max(nr_seq_classif_diag)
	into 	qt_worklist_w,
			nr_atendimento_w,
			cd_pessoa_fisica_w,
			ie_tipo_diagnostico_w,
			nr_seq_classif_diag_w
	from	wl_worklist
	where	nr_seq_diagnostico = qt_chave_p;

	if (qt_worklist_w = 'S') then
		wl_gerar_finalizar_tarefa('DG','R',nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p,sysdate,'S',null,null,null,null,null,null,null,null,qt_chave_p,null,ie_tipo_diagnostico_w,null,null,null,null,null,nr_seq_classif_diag_w);
	end if;

	-- Workload
	select	nvl2(max(nr_sequencia), 'S','N'),
			max(nr_atendimento),
			max(cd_pessoa_fisica)
	into 	qt_worklist_w,
			nr_atendimento_w,
			cd_pessoa_fisica_w
	from	wl_worklist
	where	nr_seq_sae = qt_chave_p;

	if (qt_worklist_w = 'S') then
		wl_gerar_finalizar_tarefa('WO','R',nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p,sysdate,'S',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,qt_chave_p);
	end if;

end if;

delete_assinat_inativa(nm_tabela_p,nm_chave_p,qt_chave_p);

begin
select	decode(count(*), 0, 'N', 'S')
into	qt_regulacao_w
from	regulacao_atend;

if ( qt_regulacao_w = 'S') then

	gerar_conv_status_regulacao(qt_chave_p, nm_tabela_p, 'CA', nm_usuario_p, ds_justificativa_p);

end if;

if	(upper(nm_tabela_p) in ('ESCALA_SAPS','TISS_INTERV_TERAPEUTICA_10')) then
	begin
	ie_escala_w := obter_item_escala(nm_tabela_p);
    mdc_gerar_episodio_pac_escala(null, qt_chave_p, ie_escala_w, nm_usuario_p, 'S');
    end;
end if;

if	(upper(nm_tabela_p) = 'DIAGNOSTICO_DOENCA') THEN

	select	max(nr_atendimento),
			max(cd_doenca),
			max(dt_diagnostico)
	into	nr_atendimento_w,
			cd_doenca_w,
			dt_diagnostico_w
	from	diagnostico_doenca
	where	nr_seq_interno	= qt_chave_p;

	INATIVAR_DRG_EPISODIO_PACIENTE(nr_atendimento_w, qt_chave_p, upper(nm_tabela_p), nm_usuario_p, 'IN');

		controlar_diag_pac_filho(cd_doenca_w,null,nr_atendimento_w,dt_diagnostico_w,null,nm_usuario_p,'INATIVAR','S');
end if;

select count(1)
  into ie_gerar_aprovacao_w
  from mult_aprov_funcao
 where ie_situacao = 'A'
 and cd_funcao = wheb_usuario_pck.get_cd_funcao;

if(ie_gerar_aprovacao_w > 0) then
	inativar_aprovacao_documento(nm_tabela_p, nm_chave_p,qt_chave_p, nm_usuario_p);
end if;

if	(upper(nm_tabela_p) = 'EVENTO_CIRURGIA_PACIENTE') THEN

inativar_dependencias_evento(qt_chave_p, ds_justificativa_w, nm_usuario_p);

end if;

exception
	when others then
		qt_regulacao_w := 0;
end;

commit;

end inativar_informacao;
/
