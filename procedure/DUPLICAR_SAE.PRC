create or replace
procedure duplicar_sae (
		nr_seq_prescr_p	   number,
		cd_prescritor_p	   varchar2,
		nr_atendimento_p   number,
		nm_usuario_p       varchar2,
		nr_sequencia_sae_p out number,
		ie_from_sdc_p		varchar2 default 'N'
				) is

nr_sequencia_w						number(10);
nr_seq_prescr_w 					number(10);
nr_seq_prescr_diag_w				number(10);
nr_seq_diag_w						number(10);
nr_seq_pe_prescr_proc_w				number(10);
nr_seq_prescr_proc_ant_w			number(10);
cd_setor_atendimento_w				number(5) := Obter_Setor_Atendimento(nr_atendimento_p);
CD_PERFIL_PRESCRICAO_W 				pe_prescricao.cd_perfil_ativo%type := obter_perfil_ativo;
DT_PRIMEIRO_HORARIO_PRESC_W			pe_prescricao.dt_primeiro_horario%type;
IE_AGORA_PRESCRICAO_W				pe_prescricao.ie_agora%type;
DS_HORARIOS_W						pe_prescr_proc.ds_horarios%type;
HR_PRIM_HORARIO_W					pe_prescr_proc.hr_prim_horario%type;
DT_HORARIO_ESPEC_W					pe_prescr_proc.dt_horario_espec%type;
DT_PRIMEIRO_HORARIO_W				pe_prescr_proc.dt_primeiro_horario%type;
nr_prescr_gerada_w					pe_prescricao.nr_prescricao%type;
ie_duplica_res_esp_w			varchar2(1);

cursor c01 is
	select	dt_atualizacao,
		nm_usuario,
		nr_seq_item,
		nr_seq_result,
		nr_seq_prescr,
		ds_observacao,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_topografia,
		ie_lado,
		qt_ponto
	from	pe_prescr_item_result
	where	nr_seq_prescr = nr_seq_prescr_p;
	
cursor c02 is
	select	a.nr_seq_item,
		a.dt_atualizacao,
		a.nm_usuario,
		a.dt_atualizacao_nrec,
		a.nm_usuario_nrec,
		a.nr_seq_topografia,
		a.ie_lado
	from	pe_prescr_item_result_topo a,
		pe_prescr_item_result b
	where	a.nr_seq_item = b.nr_sequencia
	and	b.nr_seq_prescr = nr_seq_prescr_p;
	
cursor c03 is
	select	dt_atualizacao,
		nm_usuario,
		nr_seq_diag,
		nr_seq_prescr,
		nr_seq_evolucao_diag,
		qt_pontuacao,
		pr_likert,
		qt_ponto_atual,
		qt_ponto_total,
		ie_origem,
		nr_sequencia
	from	pe_prescr_diag
	where	nr_seq_prescr = nr_seq_prescr_p;

cursor c04 is
	select	a.nr_seq_diag,
			a.dt_atualizacao,
			a.nm_usuario,
			a.dt_atualizacao_nrec,
			a.nm_usuario_nrec,
			a.nr_seq_fat_rel,
			a.nr_seq_compl
	from	pe_prescr_diag_fat_rel a
	where	a.nr_seq_diag = nr_seq_diag_w;


cursor c05 is
	select	a.dt_atualizacao,
		a.nm_usuario,
		a.dt_atualizacao_nrec,
		a.nm_usuario_nrec,
		a.nr_seq_diag_result,
		a.nr_seq_diag,
		a.ds_observacao
	from	pe_prescr_diag_res_esp a
	where	a.nr_seq_diag = nr_seq_diag_w;
	
cursor c06 is
	select	nr_prescricao,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_probl_colab
	from	pe_prescr_probl_col
	where	nr_prescricao = nr_seq_prescr_p;
	
cursor c07 is
	select	dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_proc,
		nr_seq_prescr,
		qt_pontuacao,
		ds_origem,
		nr_seq_apres,
		cd_intervalo,
		ds_horarios,
		qt_intervencao,
		hr_prim_horario,
		nr_seq_topografia,
		ie_se_necessario,
		ds_observacao,
		ie_suspenso,
		ie_adep,
		ie_lado,
		nr_ocorrencia,
		ie_permite_exclusao,
		nr_seq_diag,
		nr_seq_item,
		nr_seq_result,
		ie_faose,
		ie_profissional,
		nr_sequencia,
		NR_SEQ_PROT_PROC,
		IE_AUXILIAR,
		IE_ENCAMINHAR,
		IE_FAZER,
		IE_ORIENTAR,
		IE_SUPERVISIONAR,
		IE_ACM,
		IE_AGORA,
		IE_INTERV_ESPEC_AGORA,
		DT_HORARIO_ESPEC,
		DT_PRIMEIRO_HORARIO
	from	pe_prescr_proc
	where	nr_seq_prescr = nr_seq_prescr_p;

	
cursor c09 is
	select	dt_atualizacao,
		nm_usuario,
		nr_seq_prescr,
		ds_comentario
	from	pe_prescr_comentario
	where	nr_seq_prescr = nr_seq_prescr_p;
	
cursor c10 is
	select	a.dt_atualizacao,
		a.nm_usuario,
		a.dt_atualizacao_nrec,
		a.nm_usuario_nrec,
		a.cd_profissional,
		a.dt_registro,
		a.nr_seq_prescr_proc,
		a.ie_orientado_paciente,
		a.ie_orientado_pai,
		a.ie_orientado_mae,
		a.ie_orientado_conjuge,
		a.ie_orientado_filho,
		a.ie_orientado_cuidador,
		a.ie_orientado_outros,
		a.ie_metodo_demonstracao,
		a.ie_metodo_verbal,
		a.ie_metodo_folheto,
		a.ie_metodo_audio_visual,
		a.ie_metodo_outro,
		a.ie_tipo_evolucao,
		a.dt_liberacao,
		a.ie_entendimento,
		a.ie_objetivo_atingido,
		a.nr_seq_assinatura,
		a.nr_seq_assinat_inativacao
	from	pe_prescr_proc_educ a,
		pe_prescr_proc b
	where	a.nr_seq_prescr_proc = b.nr_sequencia
	and	b.nr_seq_prescr = nr_seq_prescr_p;
	
cursor c11 is
	SELECT	b.dt_atualizacao,
			b.nm_usuario,
			b.dt_atualizacao_nrec,
			b.nm_usuario_nrec,
			b.nr_seq_prescr_proc,
			b.nr_seq_atividade	
	FROM    pe_prescr_proc_atividade b
	WHERE   b.nr_seq_prescr_proc = nr_seq_prescr_proc_ant_w;
				
begin
obter_param_usuario(281,1610,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_duplica_res_esp_w);

if ( nvl(ie_from_sdc_p,'N') = 'S' ) then

	select 	a.dt_primeiro_horario,
			a.ie_agora
	into	DT_PRIMEIRO_HORARIO_PRESC_W,
			IE_AGORA_PRESCRICAO_W
	from  	pe_prescricao a
	where 	nr_sequencia	= nr_seq_prescr_p
	and	nvl(ie_situacao,'A') = 'A';
	
	
	if (DT_PRIMEIRO_HORARIO_PRESC_W is not null) then
			      
		if (nvl(IE_AGORA_PRESCRICAO_W, 'N') = 'N') then
  
			select ESTABLISHMENT_TIMEZONE_UTILS.todayAtTime(nvl(DT_PRIMEIRO_HORARIO_PRESC_W, '00:00'))
			into DT_PRIMEIRO_HORARIO_PRESC_W
			from dual;				
			
		elsif ( nvl(IE_AGORA_PRESCRICAO_W, 'N') = 'S') then
			
			DT_PRIMEIRO_HORARIO_PRESC_W := sysdate;
	  
		end if;
	end if;
	

end if; 



insert into pe_prescricao (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_prescricao,
	cd_prescritor,
	nr_atendimento,
	cd_pessoa_fisica,
	dt_liberacao,
	dt_atualizacao_nrec,
	nm_usuario_nrec,        
	nr_seq_modelo,
	qt_horas_validade,
	ie_situacao,
	cd_setor_atendimento,
	nr_sae_origem,
	ie_rn,
	nr_recem_nato,
	dt_primeiro_horario,
	dt_validade_prescr,
	ds_observacao,
	ie_tipo,
	ie_nivel_compl,
	IE_AGORA,
	CD_PERFIL_ATIVO
	)
select	pe_prescricao_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	cd_prescritor_p,
	case when nvl(nr_atendimento_p,nr_atendimento) = 0 then null else nvl(nr_atendimento_p,nr_atendimento) end,
	nvl(obter_pessoa_atendimento(nvl(nr_atendimento_p,nr_atendimento),'C'),cd_pessoa_fisica),
	decode(ie_from_sdc_p,'S', sysdate, null),
	sysdate,
	nm_usuario_p,        
	nr_seq_modelo,
	qt_horas_validade,
	'A',
	cd_setor_atendimento_w,
	nvl(nr_sae_origem, nr_seq_prescr_p),
	ie_rn,
	nr_recem_nato,
	decode(ie_from_sdc_p,'S',DT_PRIMEIRO_HORARIO_PRESC_W,dt_primeiro_horario),
	dt_validade_prescr,
	ds_observacao,
	ie_tipo,
	ie_nivel_compl,
	decode(ie_from_sdc_p,'S',IE_AGORA_PRESCRICAO_W, null),
	decode(ie_from_sdc_p,'S',CD_PERFIL_PRESCRICAO_W, null)
from	pe_prescricao
where	nr_sequencia = nr_seq_prescr_p
and	nvl(ie_situacao,'A') = 'A';
commit;


select	nvl(max(nr_sequencia),0)
into	nr_seq_prescr_w
from	pe_prescricao
where	nr_atendimento = nr_atendimento_p;


if ( nvl(ie_from_sdc_p,'N') = 'S' ) then
	
		gerar_prescricao_sae(nr_seq_prescr_w, nm_usuario_p);
		
		select 	nvl(max(nr_prescricao),0)
		into	nr_prescr_gerada_w
		from	pe_prescricao
		where 	nr_atendimento = nr_atendimento_p;
		


else

	for item_result in c01 loop
		insert into pe_prescr_item_result (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_item,
			nr_seq_result,
			nr_seq_prescr,
			ds_observacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_topografia,
			ie_lado,
			qt_ponto)
		values	(pe_prescr_item_result_seq.nextval,
			item_result.dt_atualizacao,
			item_result.nm_usuario,
			item_result.nr_seq_item,
			item_result.nr_seq_result,
			nr_seq_prescr_w,
			item_result.ds_observacao,
			item_result.dt_atualizacao_nrec,
			item_result.nm_usuario_nrec,
			item_result.nr_seq_topografia,
			item_result.ie_lado,
			item_result.qt_ponto);
	end loop;

	for item_topo in c02 loop
		insert into pe_prescr_item_result_topo (
			nr_sequencia,
			nr_seq_item,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_topografia,
			ie_lado)
		values	(pe_prescr_item_result_topo_seq.nextval,
			item_topo.nr_seq_item,
			item_topo.dt_atualizacao,
			item_topo.nm_usuario,
			item_topo.dt_atualizacao_nrec,
			item_topo.nm_usuario_nrec,
			item_topo.nr_seq_topografia,
			item_topo.ie_lado);
	end loop;

	for prescr_diag in c03 loop

		nr_seq_diag_w	:= prescr_diag.nr_sequencia;

		select	pe_prescr_diag_seq.nextval
		into	nr_seq_prescr_diag_w
		from	dual;

		insert into pe_prescr_diag (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_diag,
			nr_seq_prescr,
			nr_seq_evolucao_diag,
			qt_pontuacao,
			pr_likert,
			qt_ponto_atual,
			qt_ponto_total,
			ie_origem)
		values	(nr_seq_prescr_diag_w,
			prescr_diag.dt_atualizacao,
			prescr_diag.nm_usuario,
			prescr_diag.nr_seq_diag,
			nr_seq_prescr_w,
			prescr_diag.nr_seq_evolucao_diag,
			prescr_diag.qt_pontuacao,
			prescr_diag.pr_likert,
			prescr_diag.qt_ponto_atual,
			prescr_diag.qt_ponto_total,
			prescr_diag.ie_origem);
			
			for diag_fat_rel in c04 loop
				insert into pe_prescr_diag_fat_rel (
					nr_sequencia,
					nr_seq_diag,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_fat_rel,
					nr_seq_compl)
				values	(pe_prescr_diag_fat_rel_seq.nextval,
					nr_seq_prescr_diag_w,
					diag_fat_rel.dt_atualizacao,
					diag_fat_rel.nm_usuario,
					diag_fat_rel.dt_atualizacao_nrec,
					diag_fat_rel.nm_usuario_nrec,
					diag_fat_rel.nr_seq_fat_rel,
					diag_fat_rel.nr_seq_compl);
			end loop;

			if (nvl(ie_duplica_res_esp_w,'S') = 'S') then
				for diag_res_esp in c05 loop
					begin
					insert into pe_prescr_diag_res_esp (
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_diag_result,
						nr_seq_diag,
						ds_observacao)
					values	(pe_prescr_diag_res_esp_seq.nextval,
						diag_res_esp.dt_atualizacao,
						diag_res_esp.nm_usuario,
						diag_res_esp.dt_atualizacao_nrec,
						diag_res_esp.nm_usuario_nrec,
						diag_res_esp.nr_seq_diag_result,
						nr_seq_prescr_diag_w,
						diag_res_esp.ds_observacao);
					end;
				end loop;
			end if;
	end loop;

	for probl_col in c06 loop
		insert into pe_prescr_probl_col (	
			nr_sequencia,
			nr_prescricao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_probl_colab)
		values	(pe_prescr_probl_col_seq.nextval,
			nr_seq_prescr_w,
			probl_col.dt_atualizacao,
			probl_col.nm_usuario,
			probl_col.dt_atualizacao_nrec,
			probl_col.nm_usuario_nrec,
			probl_col.nr_seq_probl_colab);
	end loop;
	
	
	for prescr_coment in c09 loop
	insert into pe_prescr_comentario (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_prescr,
		ds_comentario)
	values	(pe_prescr_comentario_seq.nextval,
		prescr_coment.dt_atualizacao,
		prescr_coment.nm_usuario,
		nr_seq_prescr_w,
		prescr_coment.ds_comentario);
	end loop;


	for proc_educ in c10 loop
	insert into pe_prescr_proc_educ (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_profissional,
		dt_registro,
		nr_seq_prescr_proc,
		ie_orientado_paciente,
		ie_orientado_pai,
		ie_orientado_mae,
		ie_orientado_conjuge,
		ie_orientado_filho,
		ie_orientado_cuidador,
		ie_orientado_outros,
		ie_metodo_demonstracao,
		ie_metodo_verbal,
		ie_metodo_folheto,
		ie_metodo_audio_visual,
		ie_metodo_outro,
		ie_tipo_evolucao,
		dt_liberacao,
		ie_entendimento,
		ie_objetivo_atingido,
		cd_setor_atendimento,
		nr_seq_assinatura,
		nr_seq_assinat_inativacao)
	values	(pe_prescr_proc_educ_seq.nextval,
		proc_educ.dt_atualizacao,
		proc_educ.nm_usuario,
		proc_educ.dt_atualizacao_nrec,
		proc_educ.nm_usuario_nrec,
		proc_educ.cd_profissional,
		proc_educ.dt_registro,
		proc_educ.nr_seq_prescr_proc,
		proc_educ.ie_orientado_paciente,
		proc_educ.ie_orientado_pai,
		proc_educ.ie_orientado_mae,
		proc_educ.ie_orientado_conjuge,
		proc_educ.ie_orientado_filho,
		proc_educ.ie_orientado_cuidador,
		proc_educ.ie_orientado_outros,
		proc_educ.ie_metodo_demonstracao,
		proc_educ.ie_metodo_verbal,
		proc_educ.ie_metodo_folheto,
		proc_educ.ie_metodo_audio_visual,
		proc_educ.ie_metodo_outro,
		proc_educ.ie_tipo_evolucao,
		proc_educ.dt_liberacao,
		proc_educ.ie_entendimento,
		proc_educ.ie_objetivo_atingido,
		cd_setor_atendimento_w,
		proc_educ.nr_seq_assinatura,
		proc_educ.nr_seq_assinat_inativacao);
	end loop;
	

end if;

<<cursor7>>
for prescr_proc in c07 loop

	begin
	select	pe_prescr_proc_seq.nextval
	into	nr_seq_pe_prescr_proc_w
	from	dual;
	
	
	if ((nvl(ie_from_sdc_p, 'N') = 'S') ) then
	
		if ( nvl(prescr_proc.IE_AGORA,'N') = 'S' ) then
		
			DS_HORARIOS_W := to_char(sysdate, 'hh24:mi');
			HR_PRIM_HORARIO_W := DS_HORARIOS_W;
			DT_PRIMEIRO_HORARIO_W := sysdate;

		end if;
		
		if ( nvl(prescr_proc.IE_INTERV_ESPEC_AGORA,'N') = 'S' ) then
			
			select ESTABLISHMENT_TIMEZONE_UTILS.todayAtTime(nvl(prescr_proc.DT_HORARIO_ESPEC, '00:00'))
			into DT_HORARIO_ESPEC_W
			from dual;
			
		end if;
		
	end if;
	
		
	insert into pe_prescr_proc (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_proc,
		nr_seq_prescr,
		qt_pontuacao,
		ds_origem,
		nr_seq_apres,
		cd_intervalo,
		ds_horarios,
		qt_intervencao,
		hr_prim_horario,
		nr_seq_topografia,
		ie_se_necessario,
		ds_observacao,
		ie_suspenso,
		ie_adep,
		ie_lado,
		nr_ocorrencia,
		ie_permite_exclusao,
		nr_seq_diag,
		nr_seq_item,
		nr_seq_result,
		ie_faose,
		ie_profissional,
		NR_SEQ_PROT_PROC,
		IE_AUXILIAR,
		IE_ENCAMINHAR,
		IE_FAZER,
		IE_ORIENTAR,
		IE_SUPERVISIONAR,
		IE_ACM,
		IE_AGORA,
		IE_INTERV_ESPEC_AGORA,
		DT_HORARIO_ESPEC,
		DT_PRIMEIRO_HORARIO)
	values	(nr_seq_pe_prescr_proc_w,
		decode(ie_from_sdc_p,'S',sysdate,prescr_proc.dt_atualizacao),
		prescr_proc.nm_usuario,
		prescr_proc.dt_atualizacao_nrec,
		prescr_proc.nm_usuario_nrec,
		prescr_proc.nr_seq_proc,
		nr_seq_prescr_w,
		prescr_proc.qt_pontuacao,
		prescr_proc.ds_origem,
		prescr_proc.nr_seq_apres,
		prescr_proc.cd_intervalo,
		decode(ie_from_sdc_p,'S',nvl(DS_HORARIOS_W, prescr_proc.ds_horarios),prescr_proc.ds_horarios),
		prescr_proc.qt_intervencao,
		decode(ie_from_sdc_p,'S',nvl(HR_PRIM_HORARIO_W,prescr_proc.hr_prim_horario),prescr_proc.hr_prim_horario),
		prescr_proc.nr_seq_topografia,
		prescr_proc.ie_se_necessario,
		prescr_proc.ds_observacao,
		prescr_proc.ie_suspenso,
		prescr_proc.ie_adep,
		prescr_proc.ie_lado,
		prescr_proc.nr_ocorrencia,
		prescr_proc.ie_permite_exclusao,
		prescr_proc.nr_seq_diag,
		prescr_proc.nr_seq_item,
		prescr_proc.nr_seq_result,
		prescr_proc.ie_faose,
		prescr_proc.ie_profissional,
		decode(ie_from_sdc_p,'S',prescr_proc.NR_SEQ_PROT_PROC,null),
		decode(ie_from_sdc_p,'S',prescr_proc.IE_AUXILIAR,null),
		decode(ie_from_sdc_p,'S',prescr_proc.IE_ENCAMINHAR,null),
		decode(ie_from_sdc_p,'S',prescr_proc.IE_FAZER,null),
		decode(ie_from_sdc_p,'S',prescr_proc.IE_ORIENTAR,null),
		decode(ie_from_sdc_p,'S',prescr_proc.IE_SUPERVISIONAR,null),
		decode(ie_from_sdc_p,'S',prescr_proc.IE_ACM,null),
		decode(ie_from_sdc_p,'S',prescr_proc.IE_AGORA,null),
		decode(ie_from_sdc_p,'S',prescr_proc.IE_INTERV_ESPEC_AGORA,null),
		decode(ie_from_sdc_p,'S', nvl(DT_HORARIO_ESPEC_W, prescr_proc.DT_HORARIO_ESPEC),null),
		decode(ie_from_sdc_p,'S',nvl(DT_PRIMEIRO_HORARIO_W, prescr_proc.DT_PRIMEIRO_HORARIO), null)
		);
		
	
	DS_HORARIOS_W := null;
	HR_PRIM_HORARIO_W := null;
	DT_HORARIO_ESPEC_W := null;
	DT_PRIMEIRO_HORARIO_W := null;
	
	nr_seq_prescr_proc_ant_w := prescr_proc.nr_sequencia;
	
	
	<<cursor11>>
	for prescr_proc_atividade in c11 loop
	
		insert into pe_prescr_proc_atividade (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_prescr_proc,
			nr_seq_atividade)
		values	(
			pe_prescr_proc_atividade_seq.nextval,
			decode(ie_from_sdc_p,'S',sysdate,prescr_proc_atividade.dt_atualizacao),
			prescr_proc_atividade.nm_usuario,
			decode(ie_from_sdc_p,'S',sysdate,prescr_proc_atividade.dt_atualizacao_nrec),
			prescr_proc_atividade.nm_usuario_nrec,
			nr_seq_pe_prescr_proc_w,
			prescr_proc_atividade.nr_seq_atividade);
	
	end loop cursor11;
	
	

	if ( (nvl(ie_from_sdc_p,'N') = 'S') ) then	
		begin
			
		insert into prescr_material(
			nr_prescricao,
			nr_sequencia,
			ie_origem_inf,
			cd_material,
			cd_unidade_medida,
			qt_dose,
			qt_unitaria,
			qt_material,
			dt_atualizacao,
			nm_usuario,
			ie_via_aplicacao,
			cd_motivo_baixa,
			ie_utiliza_kit,
			cd_unidade_medida_dose,
			ie_urgencia,
			nr_ocorrencia,
			qt_total_dispensar,
			ie_medicacao_paciente,
			ie_agrupador,
			ie_suspenso,
			ie_se_necessario,
			ie_status_cirurgia,
			ie_bomba_infusao,
			ie_aplic_bolus,
			ie_aplic_lenta,
			ie_acm,
			qt_baixa_especial,
			ie_erro,
			ie_cultura_cih,
			ie_antibiograma,
			ie_uso_antimicrobiano,
			ie_recons_diluente_fixo,
			ie_sem_aprazamento,
			ie_cobra_paciente,
			ie_dose_espec_agora,
			ie_tipo_medic_hd,
			nr_seq_pe_proc,
			ie_intervalo_dif,
			ds_horarios,
			cd_intervalo
		)
		SELECT 
			nr_prescr_gerada_w,
			nr_sequencia,
			ie_origem_inf,
			cd_material,
			cd_unidade_medida,
			qt_dose,
			qt_unitaria,
			qt_material,
			sysdate,
			nm_usuario_p,
			ie_via_aplicacao,
			nvl(cd_motivo_baixa,0),
			ie_utiliza_kit,
			cd_unidade_medida_dose,
			ie_urgencia,
			nr_ocorrencia,
			qt_total_dispensar,
			ie_medicacao_paciente,
			ie_agrupador,
			ie_suspenso,
			ie_se_necessario,
			ie_status_cirurgia,
			ie_bomba_infusao,
			ie_aplic_bolus,
			ie_aplic_lenta,
			ie_acm,
			qt_baixa_especial,
			ie_erro,
			ie_cultura_cih,
			ie_antibiograma,
			ie_uso_antimicrobiano,
			ie_recons_diluente_fixo,
			ie_sem_aprazamento,
			ie_cobra_paciente,
			ie_dose_espec_agora,
			ie_tipo_medic_hd,
			nr_seq_pe_prescr_proc_w,
			ie_intervalo_dif,
			decode(ie_urgencia,'S',to_char(sysdate,'hh24:mi'), ds_horarios),
			cd_intervalo
		FROM	prescr_material c
		WHERE	c.nr_seq_pe_proc = prescr_proc.nr_sequencia
		and 	c.nr_seq_kit is null
		and		nvl(c.IE_SUSPENSO, 'N') = 'N'
			and not exists	( 	select 1
								from	prescr_material x
								where 	x.nr_prescricao = nr_prescr_gerada_w
								and		x.cd_material	= c.cd_material
								and		x.nr_seq_pe_proc= nr_seq_pe_prescr_proc_w
							);
		
		end;		
		end if;
		
		
	end;
end loop cursor7;




commit;
nr_sequencia_sae_p := nr_seq_prescr_w;
end duplicar_sae;
/
