create or replace
procedure GRG_IMPORTAR_GUIAS_ANALISE
				(nr_seq_lote_hist_p	in number,
				nm_usuario_p		in varchar2) is
				
nr_interno_conta_w	number(10);
cd_autorizacao_w	varchar2(255);
cd_convenio_w		number(10);
nr_seq_guia_w		number(10);
nr_seq_propaci_w	number(10);
nr_seq_matpaci_w	number(10);
vl_pago_w		number(15,2);
vl_glosa_w		number(15,2);
qt_item_w		number(19,3);
vl_saldo_w		number(15,2);
cd_motivo_glosa_w 		varchar2(10);
cd_motivo_glosa_insert_w	convenio_retorno_movto.cd_motivo%type;
cd_motivo_tiss_insert_w		convenio_retorno_movto.cd_motivo_glosa_tiss%type;
ie_acao_glosa_w			varchar2(10);
ds_complemento_w		lote_audit_hist_imp.ds_complemento%type;
cd_resposta_glosa_w		lote_audit_hist_item.cd_resposta%type;
count_w				integer;
ie_motivo_glosa_convenio_w	varchar2(1) := 'N';
nr_seq_w			lote_audit_hist_item.nr_sequencia%type;
vl_amenor_w			lote_audit_hist_item.vl_amenor%type;

				
cursor	c01 is
select	a.nr_interno_conta,
	trim(substr(a.nr_doc_convenio,1,20))
from	lote_audit_hist_imp a,
	conta_paciente b
where	a.nr_interno_conta	= b.nr_interno_conta
and	b.cd_convenio_parametro	= cd_convenio_w
and	a.nr_seq_lote_audit	= nr_seq_lote_hist_p
and	a.nr_interno_conta is not null
and	a.nr_doc_convenio is not null
group by a.nr_interno_conta,
	a.nr_doc_convenio;

cursor	c02 is	
select	nr_seq_propaci,
	nr_seq_matpaci,
	nvl(sum(vl_pago),0),
	nvl(sum(vl_glosa),0),
	nvl(sum(qt_item),0),
	cd_motivo_glosa_imp,
	ie_acao_glosa_imp,
	ds_complemento
from	lote_audit_hist_imp
where	nr_seq_guia		= nr_seq_guia_w
and	nr_seq_lote_audit	= nr_seq_lote_hist_p
and	(nr_seq_propaci is not null or nr_seq_matpaci is not null)
group by nr_seq_propaci,
	nr_seq_matpaci,
	cd_motivo_glosa_imp,
	ie_acao_glosa_imp,
	ds_complemento;	
				
begin

select	max(a.cd_convenio)	
into	cd_convenio_w	
from	lote_auditoria a,
	lote_audit_hist b
where	a.nr_sequencia	= b.nr_seq_lote_audit
and	b.nr_sequencia	= nr_seq_lote_hist_p;

select	count(1)
into	count_w
from	convenio_motivo_glosa
where	cd_convenio = cd_convenio_w;

if (count_w > 0)  then
	ie_motivo_glosa_convenio_w := 'S';
end if;

open C01;
loop
fetch C01 into	
	nr_interno_conta_w,
	cd_autorizacao_w;
exit when C01%notfound;
	begin
	
	vl_saldo_w	:= to_number(obter_saldo_conpaci(nr_interno_conta_w,cd_autorizacao_w));
	
	--if	(vl_saldo_w > 0) then
	
	select	lote_audit_hist_guia_seq.nextval
	into	nr_seq_guia_w
	from	dual;

	insert into lote_audit_hist_guia
		(cd_autorizacao,
		dt_atualizacao,
		nm_usuario,
		nr_interno_conta,
		nr_seq_lote_hist,
		nr_sequencia,
		vl_saldo_guia)
	values	(cd_autorizacao_w,
		sysdate,
		nm_usuario_p,
		nr_interno_conta_w,
		nr_seq_lote_hist_p,
		nr_seq_guia_w,
		vl_saldo_w);
		
	update	lote_audit_hist_imp
	set	nr_seq_guia		= nr_seq_guia_w
	where	nr_interno_conta	= nr_interno_conta_w
	and	nr_doc_convenio		= cd_autorizacao_w;	

	open C02;
	loop
	fetch C02 into	
		nr_seq_propaci_w,
		nr_seq_matpaci_w,
		vl_pago_w,
		vl_glosa_w,
		qt_item_w,
		cd_motivo_glosa_w,
		ie_acao_glosa_w,
		ds_complemento_w;
	exit when C02%notfound;
		begin
		
		begin
			if (ie_motivo_glosa_convenio_w = 'S') then
				select	x.cd_motivo_glosa,
					y.cd_motivo_tiss,
					z.cd_resposta
				into	cd_motivo_glosa_insert_w,
					cd_motivo_tiss_insert_w,
					cd_resposta_glosa_w
				from	convenio_motivo_glosa x,
					tiss_motivo_glosa y,
					motivo_glosa z
				where	x.cd_motivo_glosa 	= y.cd_motivo_glosa (+)
				and	x.cd_motivo_glosa 	= z.cd_motivo_glosa (+)
				and	x.cd_glosa_convenio 	= cd_motivo_glosa_w
				and	x.cd_convenio 		= cd_convenio_w;
			else
				select	z.cd_motivo_glosa,
					y.cd_motivo_tiss,
					z.cd_resposta
				into	cd_motivo_glosa_insert_w,
					cd_motivo_tiss_insert_w,
					cd_resposta_glosa_w
				from	tiss_motivo_glosa y,
					motivo_glosa z
				where	z.cd_motivo_glosa 	= y.cd_motivo_glosa (+)
				and	z.cd_motivo_glosa 	= cd_motivo_glosa_w;			
			end if;
		exception
		when others then
			cd_motivo_glosa_insert_w := null;
			cd_motivo_tiss_insert_w	:= null;
			cd_resposta_glosa_w := null;
		end;		
		
		insert into lote_audit_hist_item
			(dt_atualizacao,
			dt_atualizacao_nrec,
			dt_historico,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_guia,				
			nr_seq_lote,
			nr_seq_matpaci,
			nr_seq_propaci,
			nr_sequencia,
			qt_item,
			vl_adicional,
			vl_amenor,
			vl_glosa,
			vl_glosa_informada,
			vl_pago,
			vl_saldo,
			vl_saldo_amenor,
			cd_motivo_glosa,
			cd_motivo_glosa_tiss,
			cd_resposta,
			ie_acao_glosa,
			ds_observacao)
		values	(sysdate,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			nr_seq_guia_w,
			nr_seq_lote_hist_p,
			nr_seq_matpaci_w,
			nr_seq_propaci_w,
			lote_audit_hist_item_seq.nextval,
			qt_item_w,
			0, -- vl_adicional
			0, --vl_amenor
			0, --vl_glosa
			vl_glosa_w,
			vl_pago_w,
			vl_glosa_w, --vl_saldo
			vl_glosa_w, --vl_saldo_amenor
			cd_motivo_glosa_insert_w,
			cd_motivo_tiss_insert_w,
			cd_resposta_glosa_w,
			ie_acao_glosa_w,
			ds_complemento_w)
		returning nr_sequencia into nr_seq_w;
		
		vl_amenor_w 	:= to_number(obter_dados_lote_audit_item(nr_seq_w,'VO')) - vl_glosa_w - vl_pago_w;

		if (ie_acao_glosa_w = 'P') then

			update	lote_audit_hist_item
			set 	vl_amenor =  vl_amenor_w,
				vl_glosa = vl_glosa_w
			where	nr_sequencia = nr_seq_w;
			
		elsif (ie_acao_glosa_w = 'R') then
			
			update	lote_audit_hist_item
			set 	vl_amenor =  vl_amenor_w,
				vl_glosa_informada = vl_amenor_w,
				vl_glosa = 0
			where	nr_sequencia = nr_seq_w;
			
		elsif (ie_acao_glosa_w = 'A') then
			
			update	lote_audit_hist_item
			set 	vl_amenor =  0,
				vl_glosa = vl_glosa_w
			where	nr_sequencia = nr_seq_w;	
			
		end if;
			
		end;
	end loop;
	close C02;		
	
	/*else
		
		update	lote_audit_hist_imp
		set	ds_inconsistencia	= 'Guia n�o possui saldo!'
		where	nr_interno_conta	= nr_interno_conta_w
		and	nr_doc_convenio		= cd_autorizacao_w;		
	
	end if;	*/
	
	end;
end loop;
close C01;

commit;

end GRG_IMPORTAR_GUIAS_ANALISE;
/