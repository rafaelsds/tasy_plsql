create or replace 
procedure cancelar_titulo_receber (	nr_titulo_p		number,
					nm_usuario_p		varchar2,
					ie_commit_p		varchar2,
					dt_cancelamento_p	date default null) is

vl_saldo_w			number(15,2);
vl_titulo_w			number(15,2);
cd_moeda_w			number(5);
nr_sequencia_w			number(5);
nr_lote_contabil_w			number(10) := 0;
nr_lote_contabil_liq_w		number(10) := 0;
cd_estabelecimento_w		number(10);
vl_recebido_w			number(10);
ie_canc_tit_contab_prov_w		varchar2(255);
vl_recebido_baixa_w		number(15,2);
ie_cancel_tit_contabil_w		varchar2(1);
qt_regra_etapa_tit_w		number(10);
nr_seq_titulo_lote_w		number(10);
ds_observacao_w			alteracao_valor.ds_observacao%type := null;
nr_seq_mensalidade_w		titulo_receber.nr_seq_mensalidade%type := null;
nr_seq_lote_reem_rec_w		pls_lote_reemb_recuperacao.nr_sequencia%type;
nr_seq_tf_alt_vl_canc_w		parametro_contas_receber.nr_seq_trans_fin_alt_vl_canc%type;
ie_origem_w			titulo_receber.ie_origem_titulo%type;
ie_cancela_tit_a600		varchar2(1);
ie_a600_w			varchar2(1) := 'N'; 
nr_titulo_pagar_imposto_w		titulo_pagar.nr_titulo%type;

/*OS 1965380 - Cursor para buscar os titulos a pagar gerados para os tributos do titulo que esta sendo cancelado, para cancelar esses titulos tambem*/
Cursor C01 is
	select	a.nr_titulo
	from	titulo_pagar a,
			titulo_receber_trib b
	where	a.nr_seq_tit_rec_trib 	= b.nr_sequencia
	and		b.nr_titulo				= nr_titulo_p;

begin

select	nvl(sum(vl_recebido),0)
into	vl_recebido_w
from	titulo_receber_liq
where	nr_titulo = nr_titulo_p
and	nvl(ie_lib_caixa, 'S') = 'S';
--Incluido o numero do titulo para facilitar a identificacao do mesmo na funcao OPS -Faturamento OS 655308 dgkorz
if	(vl_recebido_w <> 0) then
	
	wheb_mensagem_pck.exibir_mensagem_abort(265038,'NR_TITULO='||nr_titulo_p);
end if;
--OS 2200833, ja tem essa consistencia acima, nao tem pq fazer de novo aqui e considerar baixas feitas pelo caixa de recebimentos que nao foram confirmados..
/*select	nvl(sum(vl_recebido),0)
into	vl_recebido_baixa_w
from	titulo_receber_liq
where	nr_titulo = nr_titulo_p;*/

select	nvl(max(nr_lote_contabil),0)
into	nr_lote_contabil_liq_w
from	titulo_receber_liq
where	nr_titulo	= nr_titulo_p;

select	max(vl_titulo),
	max(vl_saldo_titulo),
	max(cd_moeda),
	nvl(max(nr_lote_contabil),0),
	max(cd_estabelecimento)
into	vl_titulo_w,
	vl_saldo_w,
	cd_moeda_w,
	nr_lote_contabil_w,
	cd_estabelecimento_w
from	titulo_receber
where	nr_titulo = nr_titulo_p;

select	nvl(max(ie_canc_tit_contab_prov),'N'),
	nvl(max(ie_cancel_tit_contabil),'N'),
	max(nr_seq_trans_fin_alt_vl_canc)
into	ie_canc_tit_contab_prov_w,
	ie_cancel_tit_contabil_w,
		nr_seq_tf_alt_vl_canc_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_cancel_tit_contabil_w = 'N') and
	(nr_lote_contabil_liq_w <> 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(265048,'NR_LOTE_CONTABIL='||nr_lote_contabil_liq_w);
end if;	

if	(ie_canc_tit_contab_prov_w = 'N') and (nr_lote_contabil_w <> 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(265050,'NR_LOTE_CONTABIL='||nr_lote_contabil_w);
end if;

--OS 2200833, ja tem essa consistencia acima, nao tem pq fazer de novo aqui e considerar baixas feitas pelo caixa de recebimentos que nao foram confirmados..
/*if	(nvl(vl_recebido_baixa_w,0) <> 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(265051,'NR_TITULO='||nr_titulo_p);
end if;	*/

select	max(a.nr_sequencia)
into	nr_seq_titulo_lote_w
from	pls_titulo_lote_camara a,
	pls_lote_camara_comp b
where	b.nr_sequencia		= a.nr_seq_lote_camara
and	a.nr_titulo_receber	= nr_titulo_p
and	b.dt_baixa is null;

select	ie_origem_titulo
into	ie_origem_w
from	titulo_receber
where	nr_titulo = nr_titulo_p;

if	((ie_origem_w = '13') or
	 (ie_origem_w = '11')) then
	 
	select	max(nvl(ie_permite_canc_tit_vinc_a600, 'S'))
	into	ie_cancela_tit_a600
	from	pls_parametros_camara
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(ie_cancela_tit_a600 = 'N') then
		ie_a600_w := 'S';
	end if;
end if;	

if	(nr_seq_titulo_lote_w is not null) and
	(ie_a600_w = 'N')then -- Desvincular da camara de compensacao
	pls_desvinc_tit_lote_camara( nr_seq_titulo_lote_w, nm_usuario_p, 'N');
end if;

select	max(nr_sequencia)
into	nr_seq_lote_reem_rec_w
from	pls_lote_reemb_recuperacao
where	nr_titulo	= nr_titulo_p;

--aaschlote OS 913591 09/09/2015
if	(nr_seq_lote_reem_rec_w is not null) then
	begin
	update 	pls_lote_reemb_recuperacao
	set	nr_titulo 		= null,
		dt_geracao_titulo	= null,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_lote_reem_rec_w;
	
	ds_observacao_w	:= OBTER_DESC_EXPRESSAO(726871) || ' ' || nr_seq_lote_reem_rec_w;	
	exception
	when others then
		ds_observacao_w	:= '';
	end;
end if;

select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	alteracao_valor
where	nr_titulo	= nr_titulo_p;

select 	max(nr_seq_mensalidade)
into	nr_seq_mensalidade_w
from	titulo_receber
where	nr_titulo = nr_titulo_p;

if	(nr_seq_mensalidade_w is not null) then
	begin
	select 	b.ds_motivo || ' - ' || a.ds_obs_cancelamento
	into	ds_observacao_w
	from 	pls_mensalidade a,
		pls_motivo_canc_mens b
	where	a.nr_seq_motivo_canc = b.nr_sequencia
	and	a.nr_sequencia = nr_seq_mensalidade_w;
	exception
		when others then
			ds_observacao_w := null;
	end;
end if;

if	(vl_saldo_w > 0) then
	insert into alteracao_valor (
		nr_titulo,
		ds_observacao,
		nr_sequencia,
		dt_alteracao,
		vl_anterior,
		vl_alteracao,
		cd_motivo,
		cd_moeda,
		ie_aumenta_diminui,
		dt_atualizacao,
		nm_usuario,
		nr_lote_contabil,
		nr_seq_trans_fin)
	values (nr_titulo_p,
		nvl(ds_observacao_w,OBTER_DESC_EXPRESSAO(322155)),
		nr_sequencia_w,
		nvl(dt_cancelamento_p,sysdate),
		vl_saldo_w,
		vl_saldo_w,
		3,
		cd_moeda_w,
		'D',
		sysdate,
		nm_usuario_p,
		null,
		nr_seq_tf_alt_vl_canc_w);
end if;

update	titulo_receber
set	vl_saldo_titulo	= 0,
	dt_liquidacao	= nvl(dt_cancelamento_p,sysdate),
	ie_situacao	= 3,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_titulo		= nr_titulo_p;

atualizar_saldo_tit_rec(nr_titulo_p, nm_usuario_p);

/*OS 1965380 - Cursor para buscar os titulos a pagar gerados para os tributos do titulo que esta sendo cancelado, para cancelar esses titulos tambem*/
open C01;
loop
fetch C01 into	
	nr_titulo_pagar_imposto_w;
exit when C01%notfound;
	begin
	cancelar_titulo_pagar(nr_titulo_pagar_imposto_w, nm_usuario_p, nvl(dt_cancelamento_p,sysdate));
	end;
end loop;
close C01;

select	count(*)
into	qt_regra_etapa_tit_w
from	fatur_etapa_alta
where	ie_evento = 'V';

if	(nvl(qt_regra_etapa_tit_w,0) > 0) then
	gerar_etapa_titulo(nr_titulo_p, 'V', nm_usuario_p,ie_commit_p);
end if;

if	(ie_commit_p = 'S') and (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S')  then
	commit;
end if;

end cancelar_titulo_receber;
/