create or replace
procedure estorna_utilizacao_kit_estoque(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 
					
nr_prescricao_w		number(14);
qt_existe_w		number(10);
ie_consiste_baixa_prescr_w	varchar2(1);

begin
if (obter_funcao_ativa = 143) then
	obter_param_usuario(143, 253, obter_perfil_ativo , nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_consiste_baixa_prescr_w);
else
	obter_param_usuario(50, 57, obter_perfil_ativo , nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_consiste_baixa_prescr_w);
end if;

if	(ie_consiste_baixa_prescr_w = 'S') then
	begin
	select	nvl(max(nr_prescricao),0)
	into	nr_prescricao_w
	from	kit_estoque
	where	nr_sequencia	= nr_sequencia_p;
	
	if	(nr_prescricao_w > 0) then
		begin
		select	count(*)
		into	qt_existe_w
		from	prescr_material
		where	dt_baixa is not null
		and	nr_seq_kit_estoque = nr_sequencia_p
		and	nr_prescricao = nr_prescricao_w;

		if	(qt_existe_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(265987);
			--'A utiliza��o deste kit estoque n�o pode ser estornada pois j� possui itens baixados na prescri��o ' || nr_prescricao_w;
		end if;
		end;
	end if;
	end;
end if;

update	kit_estoque
set	dt_utilizacao	= '',
	nm_usuario_util	= '',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	nr_atendimento	= null,
	nr_prescricao	= null
where	nr_sequencia	= nr_sequencia_p;
	
commit;

end estorna_utilizacao_kit_estoque;
/