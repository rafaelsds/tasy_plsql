create or replace
procedure paf_inserir_forma_pagto(
			nr_seq_forma_pgto_p	number,
			nr_seq_cupom_fiscal_p	number,			
			vl_pago_p		number,
			nr_nsu_p		varchar2,
			ie_tipo_pagto_cartao_p	varchar2,
			nm_usuario_p		varchar2) is 

begin

insert into paf_forma_pgto_cupom (	
	nr_sequencia,           
	dt_atualizacao,         
	nm_usuario,             
	dt_atualizacao_nrec,
	nm_usuario_nrec,        
	vl_pago,                
	nr_seq_cupom,           
	nr_seq_forma_pgto,
	nr_nsu,
	ie_situacao,
	ie_tipo_pagto_cartao)
values(	paf_forma_pgto_cupom_seq.nextval,           
	sysdate,         
	nm_usuario_p,             
	sysdate,
	nm_usuario_p,        
	vl_pago_p,                
	nr_seq_cupom_fiscal_p,           
	nr_seq_forma_pgto_p,
	nr_nsu_p,
	'A',
	ie_tipo_pagto_cartao_p);

commit;

end paf_inserir_forma_pagto;
/