create or replace
procedure qua_gerar_envio_comunic_ata(	nr_sequencia_p		number,
					ds_valor_p		varchar2,
					nm_usuario_p		varchar2,
					ie_evento_p		varchar2,
					cd_estabelecimento_p	number,
					ie_tipo_usuario_dest_p	varchar2 default null) is

dt_prev_solucao_w			date;
nm_resp_pend_ata_w		varchar2(60);
nm_usuario_resp_pend_ata_w	varchar2(15);
dt_conclusao_real_w		date;
ds_classif_ata_w			varchar2(255);
dt_ata_w				date;
ds_ata_w				varchar2(255);
dt_liberacao_w			date;
nm_usuario_resp_ata_w		varchar2(15);
ds_pendencia_w			varchar2(255);
nr_seq_pend_w			number(10);
nm_resp_ata_w			varchar2(60);
nm_usuario_particip_ata_w		varchar2(15);
nr_seq_ata_w			number(10);
qt_existe_w			number(10);
nr_seq_regra_w			number(10);
ds_titulo_w			varchar2(255);
ds_comunicacao_w			varchar2(32000);
ie_usuario_w			varchar2(2);
ds_usuario_destino_w		varchar2(2000);
nm_usuario_w			varchar2(2000);
tam_lista_w			number(10,0);
ie_pos_virgula_w			number(3,0);
lista_usuario_w			varchar2(2000);
ds_observacao_w			varchar2(255);
ie_comunic_interna_w		varchar2(1);
ie_email_w			varchar2(1);
ds_email_usuario_w			varchar2(255);
ds_lista_email_usuario_w		varchar2(2000);
ds_email_origem_w			varchar2(255);
nm_usuario_origem_w		varchar2(15);
nm_usuario_original_w		varchar2(15);
nr_seq_classif_w		comunic_interna_classif.nr_sequencia%type;
nr_seq_classif_ci_w		comunic_interna_classif.nr_sequencia%type;
nr_seq_classif_padrao_w		comunic_interna_classif.nr_sequencia%type;

Cursor c01 is
	select	nr_sequencia,
		ds_titulo,
		ds_comunicacao,
		ie_comunicacao_interna,
		ie_email,
		ds_email_origem,
		nm_usuario_origem,
		nvl(nr_seq_classif_ci,0)
	from	qua_regra_envio_comunic
	where	ie_evento = ie_evento_p
	and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

Cursor c02 is	
	select	ie_usuario,
		ds_usuario_destino
	from	qua_usuarios_comunicacao
	where	nr_seq_regra = nr_seq_regra_w
	and	ie_usuario = nvl(ie_tipo_usuario_dest_p,ie_usuario);

Cursor c03 is
	select	distinct a.nm_usuario
	from	proj_ata_participante b,
		usuario a
	where	a.cd_pessoa_fisica = b.cd_pessoa_participante
	and	b.nr_seq_ata = nr_seq_ata_w;

begin

/*24 - ATA - Conclus�o de pend�ncia*/
if	(ie_evento_p in ('24')) then
	select	nr_seq_ata
	into	nr_seq_ata_w
	from	proj_ata_pendencia
	where	nr_sequencia = nr_sequencia_p;
else
	nr_seq_ata_w := nr_sequencia_p;
end if;

select	count(*)
into	qt_existe_w
from	qua_regra_envio_comunic
where	ie_evento = ie_evento_p
and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

if	(qt_existe_w > 0) then
	begin

	select	obter_classif_comunic('F')
	into	nr_seq_classif_padrao_w
	from	dual;

	open c01;
	loop
	fetch c01 into
		nr_seq_regra_w,
		ds_titulo_w,
		ds_comunicacao_w,
		ie_comunic_interna_w,
		ie_email_w,
		ds_email_origem_w,
		nm_usuario_origem_w,
		nr_seq_classif_ci_w;
	exit when c01%notfound;
		begin

		if	(nvl(nr_seq_classif_ci_w,0) <> 0) then
			nr_seq_classif_w	:= nr_seq_classif_ci_w;
		else
			nr_seq_classif_w	:= nr_seq_classif_padrao_w;
		end if;		

		nm_usuario_w		:= null;
		nm_usuario_original_w	:= nvl(nm_usuario_origem_w, nm_usuario_p);
		
		select	substr(obter_nome_pf(cd_consultor),1,60),
			ds_ata,
			dt_ata,
			dt_liberacao,
			substr(obter_descricao_padrao('PROJ_ATA_CLASSIFICACAO','DS_CLASSIFICACAO',nr_seq_classif),1,255)
		into	nm_resp_ata_w,
			ds_ata_w,
			dt_ata_w,
			dt_liberacao_w,
			ds_classif_ata_w
		from	proj_ata
		where	nr_sequencia = nr_seq_ata_w;
		
		open c02;
		loop
		fetch c02 into
			ie_usuario_w,
			ds_usuario_destino_w;
		exit when c02%notfound;
			begin
			/*	24 - ATA - Conclus�o de pend�ncia
				30 - ATA - Comunicar participantes	*/
			if	(ie_evento_p in ('24','30')) then
				begin
				if	(ie_evento_p = '24') and
					(ie_usuario_w = 'RP') then	/* Respons�vel pela pend�ncia da ATA */
					begin
					select	substr(obter_usuario_ativo_pf(cd_pessoa_resp),1,15)
					into	nm_usuario_resp_pend_ata_w
					from	proj_ata_pendencia
					where	nr_sequencia = nr_sequencia_p;		
					
					if	(nvl(nm_usuario_resp_pend_ata_w,'X') <> 'X') then
						nm_usuario_w	:= substr(nm_usuario_resp_pend_ata_w || ', ' || nm_usuario_w,1,2000);
					end if;
					end;
				end if;

				if	(ie_usuario_w = 'PA') then	/* Participantes da ATA */
					begin
					open c03;
					loop
					fetch c03 into
						nm_usuario_particip_ata_w;
					exit when c03%notfound;
						if	(nvl(nm_usuario_particip_ata_w,'X') <> 'X') then
							nm_usuario_w	:= substr(nm_usuario_particip_ata_w || ', ' || nm_usuario_w,1,2000);
						end if;
					end loop;
					close c03;
					end;
				elsif	(ie_usuario_w = 'RA') then	/* Respons�vel pela ATA */
					begin
					select	substr(obter_usuario_ativo_pf(cd_consultor),1,15)
					into	nm_usuario_resp_ata_w
					from	proj_ata
					where	nr_sequencia = nr_seq_ata_w;
					
					if	(nvl(nm_usuario_resp_ata_w,'X') <> 'X') then
						nm_usuario_w	:= substr(nm_usuario_resp_ata_w || ', ' || nm_usuario_w,1,2000);
					end if;
					end;
				end if;
				end;
			end if;
			
			if	(ie_usuario_w = 'UI') and
				(nvl(ds_usuario_destino_w,'0') <> '0') then
				nm_usuario_w := substr(ds_usuario_destino_w || ', ' || nm_usuario_w,1,2000);
			end if;
			end;	
		end loop;
		close c02;

		/* Macros */
		ds_titulo_w		:= substr(replace_macro(ds_titulo_w, '@nr_seq_ata', nr_seq_ata_w),1,255);		
		ds_titulo_w		:= substr(replace_macro(ds_titulo_w, '@ds_ata', ds_ata_w),1,255);
		ds_titulo_w		:= substr(replace_macro(ds_titulo_w, '@dt_reuniao_ata', to_char(dt_ata_w,'dd/mm/yyyy hh24:mi:ss')),1,255);
		
		ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@nr_seq_ata', nr_seq_ata_w),1,32000);
		ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@ds_ata', ds_ata_w),1,32000);
		ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@dt_reuniao_ata', to_char(dt_ata_w,'dd/mm/yyyy hh24:mi:ss')),1,32000);
		ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@nm_responsavel_ata', nm_resp_ata_w),1,32000);
		ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@ds_classif_ata', ds_classif_ata_w),1,32000);
		ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@dt_liberacao_ata', dt_liberacao_w),1,32000);

		if	(ie_evento_p = '24') then /* ATA - Conclus�o de pend�ncia */
			begin
			select	nr_sequencia,
				substr(obter_nome_pf(cd_pessoa_resp),1,60),
				ds_observacao,
				ds_pendencia,
				dt_prev_solucao,
				dt_conclusao_real
			into	nr_seq_pend_w,
				nm_resp_pend_ata_w,
				ds_observacao_w,
				ds_pendencia_w,
				dt_prev_solucao_w,
				dt_conclusao_real_w
			from	proj_ata_pendencia
			where	nr_sequencia = nr_sequencia_p;
			
			ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@nr_seq_pend_ata', nr_seq_pend_w),1,32000);
			ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@ds_pendencia_ata', ds_pendencia_w),1,32000);
			ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@nm_resp_pend_ata', nm_resp_pend_ata_w),1,32000);
			ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@ds_obs_pend_ata', ds_observacao_w),1,32000);
			ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@dt_prev_soluc_pend_ata', dt_prev_solucao_w),1,32000);
			ds_comunicacao_w	:= substr(replace_macro(ds_comunicacao_w, '@dt_conclusao_pend_ata', dt_conclusao_real_w),1,32000);
			end;
		end if;


		if 	(nvl(nm_usuario_w,'X') <> 'X') then
			begin
			if	(ie_comunic_interna_w = 'S') then
				begin
				
				insert into comunic_interna(
						dt_comunicado,
						ds_titulo,
						ds_comunicado,
						nm_usuario,
						dt_atualizacao,
						ie_geral,
						nm_usuario_destino,
						cd_perfil,
						nr_sequencia,
						ie_gerencial,
						nr_seq_classif,
						ds_perfil_adicional,
						cd_setor_destino,
						cd_estab_destino,
						ds_setor_adicional,
						dt_liberacao,
						ds_grupo,
						nm_usuario_oculto)
					values(	sysdate,
						ds_titulo_w,
						ds_comunicacao_w,
						nm_usuario_original_w,
						sysdate,
						'N',
						nm_usuario_w,
						null,
						comunic_interna_seq.nextval,
						'N',
						nr_seq_classif_w,
						'',
						null,
						'',
						'',
						sysdate,
						'',
						'');

				if	(nvl(nm_usuario_w,'X') <> 'X') then
					insert into proj_ata_envio(
							nr_sequencia,
							nr_seq_ata,
							dt_atualizacao,
							dt_atualizacao_nrec,
							nm_usuario,
							nm_usuario_nrec,
							dt_envio,
							ds_destino,
							ds_observacao)
						values(	proj_ata_envio_seq.nextval,
							nr_seq_ata_w,
							sysdate,
							sysdate,
							nm_usuario_p,
							nm_usuario_p,
							sysdate,
							substr(nm_usuario_w,1,255),
							substr(ds_comunicacao_w,1,4000));
				end if;
				end;
			end if;

			if	(ie_email_w = 'S') then
				begin
				lista_usuario_w := substr(nm_usuario_w,1,2000);

				while	(lista_usuario_w is not null) and
					(trim(lista_usuario_w) <> ',') loop
					tam_lista_w	:= length(lista_usuario_w);
					ie_pos_virgula_w	:= instr(lista_usuario_w,',');

					if	(ie_pos_virgula_w <> 0) then
						nm_usuario_w	:= substr(lista_usuario_w,1,(ie_pos_virgula_w - 1));
						lista_usuario_w	:= trim(substr(lista_usuario_w,(ie_pos_virgula_w + 1), tam_lista_w));

						select	max(ds_email)
						into	ds_email_usuario_w
						from	usuario
						where	nm_usuario = nm_usuario_w;

						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							begin
							ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
							end;
						end if;
					else
						lista_usuario_w	:= null;
					end if;
				end loop;

				if	(nvl(ds_lista_email_usuario_w,'X') <> 'X') then
					ds_lista_email_usuario_w := replace(ds_lista_email_usuario_w,',',';');
					enviar_email(	ds_titulo_w,
							ds_comunicacao_w,
							ds_email_origem_w,
							ds_lista_email_usuario_w,
							nm_usuario_original_w,
							'M');
				end if;

				if	(nvl(ds_lista_email_usuario_w,'X') <> 'X') then
					insert into proj_ata_envio(
							nr_sequencia,
							nr_seq_ata,
							dt_atualizacao,
							dt_atualizacao_nrec,
							nm_usuario,
							nm_usuario_nrec,
							dt_envio,
							ds_destino,
							ds_observacao)
						values(	proj_ata_envio_seq.nextval,
							nr_seq_ata_w,
							sysdate,
							sysdate,
							nm_usuario_p,
							nm_usuario_p,
							sysdate,
							substr(ds_lista_email_usuario_w,1,255),
							substr(ds_comunicacao_w,1,4000));
				end if;
				end;
			end if;
			end;
		end if;
		end;
	end loop;
	close c01;
	end;
end if;

commit;

end qua_gerar_envio_comunic_ata;
/