CREATE OR REPLACE PROCEDURE ageint_api_sugestao_horario( --******* agenda_integrada
                                                        cd_pessoa_fisica_p   VARCHAR2,
                                                        nm_paciente_p        VARCHAR2,
                                                        qt_idade_pac_p       NUMBER,
                                                        qt_peso_p            NUMBER,
                                                        qt_altura_cm_p       NUMBER,
                                                        ie_sexo_p            VARCHAR2,
                                                        nr_seq_status_p      NUMBER,
                                                        cd_convenio_p        NUMBER,
                                                        cd_categoria_p       VARCHAR2,
                                                        cd_plano_p           VARCHAR2,
                                                        cd_estabelecimento_p NUMBER,
                                                        nm_usuario_nrec_p    VARCHAR2,
                                                        --****** agenda_integrada_item
                                                        cd_medico_p           VARCHAR2,
                                                        cd_especialidade_p    NUMBER,
                                                        cd_procedimento_p     NUMBER,
                                                        nr_minuto_duracao_p   NUMBER,
                                                        nr_seq_item_selec_p   NUMBER,
                                                        ie_tipo_agendamento_p VARCHAR2,
                                                        ie_genero_prof_p      VARCHAR2,
                                                        nr_seq_proc_interno_p NUMBER,
                                                        ie_lado_p             VARCHAR2,
                                                        dt_inicio_p           DATE DEFAULT NULL,
                                                        dt_final_p            DATE DEFAULT NULL) IS

    /*
        --******* agenda_integrada
        cd_pessoa_fisica_p   VARCHAR2(10) := 1020621614;
        nm_paciente_p        VARCHAR2(60) := 'Ary Gabriel Albano Nunes Dias';
        qt_idade_pac_p       NUMBER := 33;
        qt_peso_p            NUMBER := 130;
        qt_altura_cm_p       NUMBER := 180;
        ie_sexo_p            VARCHAR2(1) := 'M';
        nr_seq_status_p      NUMBER := 2;
        cd_convenio_p        NUMBER := NULL;
        cd_categoria_p       VARCHAR2(10) := 632;
        cd_plano_p           VARCHAR2(10) := 'Teste Ary';
        cd_estabelecimento_p NUMBER := 1;
        nm_usuario_nrec_p    VARCHAR2(15) := 'msoliveira';
        --****** agenda_integrada_item
        cd_medico_p           VARCHAR2(10) := '548713';
        cd_especialidade_p    NUMBER := 59;
        cd_procedimento_p     NUMBER := 10014;
        nr_minuto_duracao_p   NUMBER := 20;
        nr_seq_item_selec_p   NUMBER := 270;
        ie_tipo_agendamento_p VARCHAR2(15) := 'C';
        ie_genero_prof_p      VARCHAR2(1) := 'A';
        nr_seq_proc_interno_p NUMBER := 234;
        ie_lado_p             VARCHAR2(1) := 'A';
    */
    --********************
    nr_seq_w      NUMBER(10, 0);
    nr_seq_item_w NUMBER(10, 0);
    dt_inicio_w   DATE := NVL(dt_inicio_p,SYSDATE);
    dt_final_w    DATE := NVL(dt_final_p, dt_inicio_w + 15);
    qt_periodo_w  NUMBER := trunc(dt_final_w - dt_inicio_w);
    cd_perfil_w   parametro_agenda_integrada.cd_perfil_sugestao%TYPE;

BEGIN

    SELECT nvl(MAX(cd_perfil_sugestao), 1)
	INTO cd_perfil_W
	FROM parametro_agenda_integrada
	WHERE cd_estabelecimento IS NULL
	AND cd_perfil_sugestao IS NOT NULL;
	
    ----- Chamada da funcao da Sugestao da Agenda ----

    wheb_usuario_pck.set_nm_usuario(nm_usuario_nrec_p);
    wheb_usuario_pck.set_cd_perfil(cd_perfil_w); -- Perfil da Agenda
    wheb_usuario_pck.set_cd_funcao(869); -- Agenda Integrada
    wheb_usuario_pck.set_cd_estabelecimento(cd_estabelecimento_p);
    ---------------------------------

    SELECT agenda_integrada_seq.nextval INTO nr_seq_w FROM dual;

    --dbms_output.put_line('Agenda:' || nr_seq_w);

    -- Insere AGENDA_INTEGRADA
    INSERT INTO agenda_integrada
        (nr_sequencia,
         dt_atualizacao,
         nm_usuario,
         dt_atualizacao_nrec,
         nm_usuario_nrec,
         dt_inicio_agendamento,
         cd_pessoa_fisica,
         nm_paciente,
         qt_idade_pac,
         qt_peso,
         qt_altura_cm,
         ie_sexo,
         nr_seq_status,
         cd_convenio,
         cd_categoria,
         cd_plano,
         cd_estabelecimento)
    ---------
    VALUES
        (nr_seq_w,
         SYSDATE,
         nm_usuario_nrec_p,
         SYSDATE,
         nm_usuario_nrec_p,
         SYSDATE,
         cd_pessoa_fisica_p,
         nm_paciente_p,
         qt_idade_pac_p,
         qt_peso_p,
         qt_altura_cm_p,
         ie_sexo_p,
         nr_seq_status_p,
         cd_convenio_p,
         cd_categoria_p,
         cd_plano_p,
         cd_estabelecimento_p);

    --****************
    -- Pega a sequencia da agenda integrada item que sera criada
    SELECT agenda_integrada_item_seq.nextval INTO nr_seq_item_w FROM dual;

    -- Insere AGENDA_INTEGRADA_ITEM
    INSERT INTO agenda_integrada_item
        (nr_sequencia,
         nr_seq_agenda_int,
         dt_atualizacao,
         nm_usuario,
         dt_atualizacao_nrec,
         nm_usuario_nrec,
         ie_tipo_agendamento,
         cd_medico,
         cd_especialidade,
         cd_procedimento,
         nr_minuto_duracao,
         nr_seq_item_selec,
         ie_genero_prof,
         nr_seq_proc_interno,
         ie_lado)
    ---------
    VALUES
        (nr_seq_item_w,
         nr_seq_w,
         SYSDATE,
         nm_usuario_nrec_p,
         SYSDATE,
         nm_usuario_nrec_p,
         ie_tipo_agendamento_p,
         cd_medico_p,
         cd_especialidade_p,
         cd_procedimento_p,
         nr_minuto_duracao_p,
         nr_seq_item_selec_p,
         ie_genero_prof_p,
         nr_seq_proc_interno_p,
         ie_lado_p);

    --  COMMIT;

/*    IF ie_tipo_agendamento_p = 'C' THEN
        inserir_proc_regra_ageint_cons(cd_especialidade_p   => cd_especialidade_p,
                                       nr_seq_ageint_item_p => nr_seq_item_w,
                                       ie_classif_agenda_p  => NULL \*nvl(ie_classif_agenda_p, ie_classif_agenda_w)*\,
                                       cd_convenio_p        => cd_convenio_p,
                                       cd_categoria_p       => cd_categoria_p,
                                       cd_estabelecimento_p => cd_estabelecimento_p,
                                       nm_usuario_p         => nm_usuario_nrec_p);
    
        gerar_ajust_proc_grupo_ageint(cd_especialidade_p    => cd_especialidade_p,
                                      ie_classif_agenda_p   => NULL \*nvl(ie_classif_agenda_p, ie_classif_agenda_w)*\,
                                      nr_seq_item_ageint_p  => nr_seq_item_w,
                                      nr_seq_ageint_p       => nr_seq_w,
                                      nr_seq_grupo_p        => NULL \*nr_seq_grupo_w*\,
                                      nr_seq_proc_interno_p => NULL \*nr_seq_proc_interno_w *\,
                                      ie_lado_p             => ie_lado_p,
                                      nm_usuario_p          => nm_usuario_nrec_p,
                                      cd_estabelecimento_p  => cd_estabelecimento_p);
    END IF;*/
    COMMIT;

    ageint_sugerir_horarios_pck.clean_cache();
    -- Insere os itens no vetor
    ageint_sugerir_horarios_pck.set_ie_ignora_questionario();
    ageint_sugerir_horarios_pck.iniciar_itens_ageint(nr_seq_ageint_p => nr_seq_w);
    --  info_agenda_horario_w
    -- Gera os horarios do item para o periodo

    ageint_sugerir_horarios_pck.gerar_horarios_itens_vetor(nr_seq_ageint_p => nr_seq_w,
                                                           dt_inicio_ger_p => dt_inicio_w,
                                                           qt_dias_p       => qt_periodo_w);
    -- Gera as sugestoes do item (e os horarios do periodo se precisar junto)

    ageint_sugerir_horarios_pck.gerar_sugestao_horario(nr_seq_combo_p    => 1,
                                                       ie_manha_p        => 'SSSSSSS',
                                                       ie_tarde_p        => 'SSSSSSS',
                                                       ie_noite_p        => 'SSSSSSS',
                                                       ie_ordenacao_p    => 'V', -- ordenacao por (T) Tempo ou (V) Valor
                                                       nr_seq_ageint_p   => nr_seq_w,
                                                       dt_agenda_p       => dt_inicio_w,
                                                       cd_estab_agenda_p => cd_estabelecimento_p,
                                                       qt_periodo_p      => qt_periodo_w);

END;
/
