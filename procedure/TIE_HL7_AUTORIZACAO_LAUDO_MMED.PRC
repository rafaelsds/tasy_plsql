create or replace 
PROCEDURE TIE_HL7_AUTORIZACAO_LAUDO_MMED(
                    nr_sequencia_p            IN  laudo_paciente.nr_sequencia%TYPE, 
                    ie_tipo_p                 IN  NUMBER, 
                    cd_medico_aprovacao       IN  laudo_paciente.cd_medico_resp%TYPE,
                    ie_liberado_digitador_p   IN  VARCHAR2,
                    ie_html_p                 IN  VARCHAR2 DEFAULT 'N') IS 
                           

nr_prescricao_w             prescr_procedimento.nr_prescricao%TYPE; 
nr_sequencia_prescricao_w   prescr_procedimento.nr_sequencia%TYPE;
alterar_medico_conta_w      VARCHAR2(2 CHAR) := 'N'; 
alterar_medico_exec_conta_w VARCHAR2(2 CHAR) := 'N'; 
aprovar_laudo_1_etapa_w	    VARCHAR2(1 CHAR) := 'N';
remove_lista_medica_w	    VARCHAR2(1 CHAR) := 'N';
ie_liberar_primeira_ass_w   VARCHAR2(1 CHAR) := 'N';
dt_inicio_digitacao_w       laudo_paciente.dt_inicio_digitacao%TYPE;
nm_usuario_digitacao_w      usuario.nm_usuario%TYPE;
nr_atendimento_w            laudo_paciente.nr_atendimento%type;
cd_pessoa_fisica_w          laudo_paciente.cd_pessoa_fisica%type;
cd_resp_seg_aprov_w         laudo_paciente.cd_resp_seg_aprov%type;
ie_exist_pend_w             PLS_INTEGER;
ie_have_sec_aprov_w         PLS_INTEGER;
nm_usuario_w                usuario.nm_usuario%TYPE;
cd_estabelecimento_w        usuario.cd_estabelecimento%TYPE;

  /* 
						 
    2 - Segunda aprovacao 
						   
						   
  */ 
BEGIN 

    select count(nr_sequencia)
    into ie_have_sec_aprov_w
    from laudo_paciente
    where cd_resp_seg_aprov is not null
    and nr_sequencia = nr_sequencia_p;
    
    SELECT nm_usuario, cd_estabelecimento
    INTO nm_usuario_w, cd_estabelecimento_w
    FROM usuario
    WHERE cd_pessoa_fisica = cd_medico_aprovacao;

    if(ie_have_sec_aprov_w > 0) then

      select max(nr_atendimento),
             max(cd_pessoa_fisica),
             max(cd_resp_seg_aprov)
      into nr_atendimento_w,
           cd_pessoa_fisica_w,
           cd_resp_seg_aprov_w
      from laudo_paciente
      where nr_sequencia = nr_sequencia_p;

      select count(nr_sequencia)
      into ie_exist_pend_w
      from wl_worklist
      where dt_final_real is null
      and cd_profissional = cd_resp_seg_aprov_w
      and nr_seq_laudo = nr_sequencia_p;
          
    end if;
  
  IF(nvl(ie_html_p, 'N') = 'S') THEN 
   
	 obter_param_usuario(99010, 55, null, nm_usuario_w, 
	 cd_estabelecimento_w, alterar_medico_conta_w); 

	 obter_param_usuario(99010, 56, null, nm_usuario_w, 
	 cd_estabelecimento_w, alterar_medico_exec_conta_w); 
	 
	 obter_param_usuario(99010, 89, null, nm_usuario_w, 
	 cd_estabelecimento_w, aprovar_laudo_1_etapa_w); 
	 
     obter_param_usuario(99010, 110, null, nm_usuario_w, 
     cd_estabelecimento_w, ie_liberar_primeira_ass_w); 
  END IF;
  
  obter_param_usuario(99010, 94, null, nm_usuario_w, 
  cd_estabelecimento_w, remove_lista_medica_w); 
  
  IF ( ie_tipo_p = 2 ) THEN 
  
    SELECT nr_prescricao, nr_seq_prescricao
    INTO nr_prescricao_w, nr_sequencia_prescricao_w
    FROM laudo_paciente
    WHERE nr_sequencia = nr_sequencia_p;
    
    UPDATE laudo_paciente 
    SET    dt_seg_aprovacao = SYSDATE, 
           dt_aprovacao = SYSDATE, 
           nm_usuario_seg_aprov = nm_usuario_w, 
           nm_usuario_liberacao = nm_usuario_w, 
           nm_usuario = nm_usuario_w, 
           ie_liberado_digitador = ie_liberado_digitador_p 
    WHERE  nr_sequencia = nr_sequencia_p; 
	
    update_situacao_procedimento(nr_prescricao_w, nr_sequencia_prescricao_w, -1, nm_usuario_w);
  
    if(ie_exist_pend_w > 0) then
        wl_gerar_finalizar_tarefa('MR', 'F', nr_atendimento_w, cd_pessoa_fisica_w, nm_usuario_w, null, 'N', null, null, null, null, null, null, null, null, null, null,
                                 null, null, null, null, null, null, null, sysdate, null, null, cd_resp_seg_aprov_w, null, null, null, null, nr_sequencia_p);  
    end if;
    
    IF ( remove_lista_medica_w = 'S' ) THEN 
       delete_exame_lista_medico(nr_prescricao_w, nr_sequencia_prescricao_w); 
    END IF;
	  
    IF ( alterar_medico_conta_w = 'S' ) THEN 
      atualizar_propaci_medico_laudo(nr_sequencia_p, 'EX', nm_usuario_w); 
    END IF; 

    IF ( alterar_medico_exec_conta_w <> 'N' ) THEN 
      atualizar_propaci_medico_laudo(nr_sequencia_p, 'EXC', nm_usuario_w); 
    END IF;
  END IF; 
  
  SELECT dt_inicio_digitacao,
         nm_usuario_digitacao
  INTO   dt_inicio_digitacao_w,
         nm_usuario_digitacao_w
  FROM   laudo_paciente 
  WHERE  nr_sequencia = nr_sequencia_p; 
  
  IF (nm_usuario_digitacao_w IS NULL) THEN
    UPDATE laudo_paciente 
    SET    dt_inicio_digitacao = SYSDATE, 
           nm_usuario_digitacao = nm_usuario_w
    WHERE  nr_sequencia = nr_sequencia_p;
  END IF;
  
  COMMIT; 
END TIE_HL7_AUTORIZACAO_LAUDO_MMED;
/