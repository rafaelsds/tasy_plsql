create or replace
procedure pls_inserir_motivo_glosa_aud
			(	nr_seq_guia_p		Number,
				nr_seq_requisicao_p	Number,
				nr_seq_execucao_p	Number,
				nr_seq_proc_guia_p	Number,
				nr_seq_mat_guia_p	Number,
				nr_seq_proc_req_p	Number,
				nr_seq_mat_req_p	Number,
				nr_seq_proc_exec_p	Number,
				nr_seq_mat_exec_p	Number,
				nr_seq_motivo_glosa_p	Number,
				nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir glosas para os procedimentos/materiais atrav�s da auditoria
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performance
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

if	(nr_seq_guia_p	is not null) then

	if	(nr_seq_proc_guia_p	is not null) and (nr_seq_proc_guia_p > 0) then

		insert into pls_guia_glosa (
			nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_guia,
			nr_seq_guia_proc, nr_seq_guia_mat, nr_seq_motivo_glosa,
			ds_observacao, ie_origem, nr_seq_regra,
			ds_origem_glosa, nr_seq_ocorrencia, nr_seq_guia_anexo,
			nr_seq_guia_anexo_proc, nr_seq_guia_anexo_mat)
		values	(
			pls_guia_glosa_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, null,
			nr_seq_proc_guia_p, null, nr_seq_motivo_glosa_p,
			'', null, null,
			'', null, null,
			null, null);
			
	elsif	(nr_seq_mat_guia_p	is not null) and (nr_seq_mat_guia_p > 0) then
		insert into pls_guia_glosa (
			nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_guia,
			nr_seq_guia_proc, nr_seq_guia_mat, nr_seq_motivo_glosa,
			ds_observacao, ie_origem, nr_seq_regra,
			ds_origem_glosa, nr_seq_ocorrencia, nr_seq_guia_anexo,
			nr_seq_guia_anexo_proc, nr_seq_guia_anexo_mat)
		values	(
			pls_guia_glosa_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, null,
			null, nr_seq_mat_guia_p, nr_seq_motivo_glosa_p,
			'', null, null,
			'', null, null,
			null, null);
	end if;
	
elsif	(nr_seq_requisicao_p	is not null) then
	
	if 	(nr_seq_proc_req_p	is not null) and (nr_seq_proc_req_p > 0) then
	
		insert into pls_requisicao_glosa (
			nr_sequencia, nr_seq_motivo_glosa, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_requisicao, ds_observacao, nr_seq_req_proc,
			nr_seq_req_mat, nr_seq_ocorrencia, nr_seq_execucao,
			nr_seq_exec_proc, nr_seq_exec_mat)
		values	(
			pls_requisicao_glosa_seq.nextval, nr_seq_motivo_glosa_p, sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			null, '', nr_seq_proc_req_p,
			null, null, null,
			null, null);
	
	elsif	(nr_seq_mat_req_p	is not null) and (nr_seq_mat_req_p > 0) then
	
		insert into pls_requisicao_glosa (
			nr_sequencia, nr_seq_motivo_glosa, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_requisicao, ds_observacao, nr_seq_req_proc,
			nr_seq_req_mat, nr_seq_ocorrencia, nr_seq_execucao,
			nr_seq_exec_proc, nr_seq_exec_mat)
		values	(
			pls_requisicao_glosa_seq.nextval, nr_seq_motivo_glosa_p, sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			null, '', null,
			nr_seq_mat_req_p, null, null,
			null, null);	
	end if;
	
elsif	(nr_seq_execucao_p	is not null) then

	if 	(nr_seq_proc_exec_p	is not null) and (nr_seq_proc_exec_p > 0) then

		insert into pls_requisicao_glosa (
			nr_sequencia, nr_seq_motivo_glosa, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_requisicao, ds_observacao, nr_seq_req_proc,
			nr_seq_req_mat, nr_seq_ocorrencia, nr_seq_execucao,
			nr_seq_exec_proc, nr_seq_exec_mat)
		values	(
			pls_requisicao_glosa_seq.nextval, nr_seq_motivo_glosa_p, sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			null, '', null,
			null, null, nr_seq_execucao_p,
			nr_seq_proc_exec_p, null);
	
	elsif	(nr_seq_mat_exec_p	is not null) and (nr_seq_mat_exec_p > 0) then
	
		insert into pls_requisicao_glosa (
			nr_sequencia, nr_seq_motivo_glosa, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_requisicao, ds_observacao, nr_seq_req_proc,
			nr_seq_req_mat, nr_seq_ocorrencia, nr_seq_execucao,
			nr_seq_exec_proc, nr_seq_exec_mat)
		values	(
			pls_requisicao_glosa_seq.nextval, nr_seq_motivo_glosa_p, sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			null, '', null,
			null, null, nr_seq_execucao_p,
			null, nr_seq_mat_exec_p);	
	end if;

end if;


commit;

end pls_inserir_motivo_glosa_aud;
/
