create or replace 
procedure atualiza_dados_conta_prot
		(nr_seq_protocolo_p	number,
		dt_mesano_referencia_p	date,
		nm_usuario_p		varchar2,
		ie_conta_contabil_p	varchar2,
		ie_grupo_receita_p		varchar2,
		ie_especialidade_medica_p	varchar2,
		ie_codigo_convenio_p	varchar2,
		ie_proc_princ_interno_p	varchar2,
		ie_funcao_medico_p	varchar2,
		cd_convenio_p		number,
		ie_atualiza_medico_conv_p	varchar2,
		ie_resumo_protocolo_p	varchar2,
		ie_atualiza_emite_conta_p	varchar2) is
		

nr_interno_conta_w	number(10);
nr_seq_protocolo_w	number(10);
ie_exibe_mensagem_w	varchar2(1);
ds_mensagem_w		varchar2(255);
		
cursor c01 is
	select	nr_interno_conta
	from	conta_paciente
	where	nr_seq_protocolo	= nr_seq_protocolo_p;
	
cursor c02 is
	select	nr_seq_protocolo
	from	protocolo_convenio
	where	nr_seq_protocolo	= nr_seq_protocolo_p
	union
	select	nr_seq_protocolo
	from	protocolo_convenio
	where	trunc(dt_mesano_referencia,'Month')	= trunc(dt_mesano_referencia_p,'Month');	

begin

ie_exibe_mensagem_w := 'N';

if	(ie_conta_contabil_p	= 'S') or
	(ie_grupo_receita_p	= 'S') then
	atualizar_dados_conta_pac
		(nr_seq_protocolo_p,
		dt_mesano_referencia_p,
		ie_conta_contabil_p,
		ie_grupo_receita_p,
		nm_usuario_p,
		nvl(cd_convenio_p,0));
		ie_exibe_mensagem_w := 'S';
end if;

if	(ie_especialidade_medica_p = 'S') then
	reatualizar_especialidade
		(nr_seq_protocolo_p,
		dt_mesano_referencia_p,
		nm_usuario_p);
		ie_exibe_mensagem_w := 'S';
end if;

if	(ie_codigo_convenio_p = 'S') then
	reatualiza_codigo_convenio
		(nr_seq_protocolo_p,
		dt_mesano_referencia_p,
		nm_usuario_p);
		ie_exibe_mensagem_w := 'S';
end if;

if	(ie_proc_princ_interno_p = 'S') then
	reatualizar_proc_princ_interno(
		nr_seq_protocolo_p,
		null,
		dt_mesano_referencia_p,
		nm_usuario_p);
		ie_exibe_mensagem_w := 'S';
end if;

if	(ie_funcao_medico_p = 'S') then
	atualizar_funcao_medic_prot
			(nr_seq_protocolo_p);
		ie_exibe_mensagem_w := 'S';
end if;

if	(ie_atualiza_emite_conta_p = 'S') then
	open c02;
	loop
	fetch c02 into	
		nr_seq_protocolo_w;
	exit when c02%notfound;
		begin
		atualizar_emite_conta(null, nr_seq_protocolo_w, 0);
		end;
	end loop;
		ie_exibe_mensagem_w := 'S';
	close c02;
end if;

if	(ie_atualiza_medico_conv_p	= 'S') then
	open c01;
	loop
	fetch c01 into	
		nr_interno_conta_w;
	exit when c01%notfound;
		begin
		atualiza_medico_convenio(nr_interno_conta_w);
		end;
	end loop;
		ie_exibe_mensagem_w := 'S';
	close c01;
end if;

if	(ie_resumo_protocolo_p	= 'S') then
	open c02;
	loop
	fetch c02 into	
		nr_seq_protocolo_w;
	exit when c02%notfound;
		begin
		recalcular_protocolo(nr_seq_protocolo_w,nm_usuario_p,'N','S');
		end;
	end loop;
	close c02;
	ie_exibe_mensagem_w := 'S';
end if;

if	(nvl(ie_exibe_mensagem_w,'N') = 'S') then
	ds_mensagem_w := substr(wheb_mensagem_pck.get_texto(498961),1,255);
	wheb_mensagem_pck.exibir_mensagem_abort(ds_mensagem_w);
end if;

end atualiza_dados_conta_prot;
/