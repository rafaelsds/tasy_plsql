create or replace
procedure inserir_motivo_suspensao(	nr_seq_motivo_susp_p	number,
					nr_seq_atendimento_p	number,	
					nm_usuario_p		varchar2) is 

begin

if 	(nr_seq_motivo_susp_p is not null) and 
	(nr_seq_atendimento_p is not null) then 
	update 	paciente_atendimento
	set 	nr_seq_motivo_susp = nr_seq_motivo_susp_p
	where 	nr_seq_atendimento = nr_seq_atendimento_p;
end if;

commit;

end inserir_motivo_suspensao;
/ 