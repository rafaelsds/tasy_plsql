create or replace procedure gerar_cobranca_cecred_150(	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is

/* Geral */
ds_conteudo_w			varchar2(150);
nr_seq_registro_w			number(10)	:= 0;
nr_seq_apres_w			number(10)	:= 0;
qt_registros_w			number(10)	:= 0;
vl_total_w			number(15,2)	:= 0;

/* Header A*/
nm_empresa_w			varchar2(20);
dt_geracao_w			varchar2(8);
nr_seq_arquivo_w			varchar2(6);
nm_banco_w			varchar2(20);
cd_convenio_w			varchar2(20);
ds_conta_comprom_w		varchar2(16);
cd_banco_w			varchar2(3);

/* Transacoes C - D - E */
cd_agencia_bancaria_w		varchar2(4);
ds_ocorrencia_1_w			varchar2(40);
ds_ocorrencia_2_w			varchar2(40);
ds_ocorrencia_w			varchar2(60);
dt_vencimento_w			varchar2(8);
vl_titulo_w			number(15,2);
cd_moeda_w			varchar2(2);
ds_mensagen_w			varchar2(26);
ds_uso_empresa_w			varchar2(60);
ds_ident_cliente_emp_w		varchar2(25);
ds_ident_cliente_emp_atual_w	varchar2(25);
ds_ident_cliente_banco_w		varchar2(14);

/* Brancos */
ds_brancos_27_w			varchar2(27);
ds_brancos_119_w			varchar2(119);
ds_brancos_19_w			varchar2(19);
ds_brancos_8_w			varchar2(8);
ds_brancos_1_w			varchar2(1);
ds_brancos_14_w			varchar2(14);
ds_brancos_52_w			varchar2(52);
ds_brancos_25_w			varchar2(25);
ds_brancos_20_w			varchar2(20);
ds_brancos_126_w			varchar2(126);

Cursor C01 is
	select	lpad(substr(decode(g.cd_agencia_bancaria,null,f.cd_agencia_bancaria,g.cd_agencia_bancaria),1,4),4,'0') cd_agencia_bancaria,
		lpad(' ',40,' ') ds_ocorrencia_1,
		lpad(' ',40,' ') ds_ocorrencia_2,
		lpad(' ',60,' ') ds_ocorrencia,
		substr(to_char(b.dt_pagamento_previsto,'YYYYMMDD'),1,8) dt_vencimento,    
		b.vl_saldo_titulo vl_titulo,
    rpad(nvl(b.cd_moeda,0),2,' ') cd_moeda,
		rpad(b.nr_titulo,49,' ') || lpad(' ',11,' ') ds_uso_empresa,
		lpad(' ',26,' ') ds_mensagen,
    (case 
        when f.cd_cgc is not null then  lpad(substr(pls_obter_dados_segurado(h.nr_seq_segurado,'C'),1,8),14,'0')                                                                                                        
        else substr(pls_obter_dados_segurado(h.nr_seq_segurado,'C'),1,14) 
        end) ds_ident_cliente_emp,		
		lpad(' ',25,' ') ds_ident_cliente_emp_atual,
     lpad(nvl(nvl(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'C'),1,11),'0') ||
		substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DC'),1,1),'0'),12,'0') ds_ident_cliente_banco
	from	banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v		b,
		titulo_receber_cobr		c,
		cobranca_escritural		a,
		pls_contrato_pagador_fin	g,
		pls_mensalidade_segurado h
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo			= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador		= f.nr_sequencia(+)
	and 	d.nr_seq_pagador 		= g.nr_seq_pagador(+)
	and d.nr_sequencia = h.nr_seq_mensalidade
	and g.dt_fim_vigencia is null  
	and h.nr_sequencia = (select min (x.nr_sequencia) from pls_mensalidade_segurado x
                       where x.nr_seq_mensalidade = d.nr_sequencia)  
	and	a.nr_sequencia	= nr_seq_cobr_escrit_p;

begin
delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	lpad(' ',27,' '),
	lpad(' ',1,' '),
	lpad(' ',19,' '),
	lpad(' ',14,' '),
	lpad(' ',8,' '),
	lpad(' ',119,' '),
	lpad(' ',52,' '),
	lpad(' ',25,' '),
	lpad(' ',20,' '),
	lpad(' ',126,' ')
into	ds_brancos_27_w,
	ds_brancos_1_w,
	ds_brancos_19_w,
	ds_brancos_14_w,
	ds_brancos_8_w,
	ds_brancos_119_w,
	ds_brancos_52_w,
	ds_brancos_25_w,
	ds_brancos_20_w,
	ds_brancos_126_w
from	dual;

/* Header */
select	rpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,20))),20,' '),
	to_char(sysdate,'YYYYMMDD'),
	lpad(to_char(nvl(a.nr_remessa,a.nr_sequencia)),6,'0'),
	rpad(substr(obter_nome_banco(a.cd_banco),1,20),20,' ') nm_banco,
	nvl(rpad(substr(c.cd_conv_banco_deb,1,20),20,' '),rpad('200149',20,' ')) cd_convenio,
	substr(c.cd_agencia_bancaria,1,4) || ' ' || substr(c.cd_conta,1,8) || substr(c.ie_digito_conta,1,1),
	lpad(substr(a.cd_banco,1,3),3,'0') cd_banco
into	nm_empresa_w,
	dt_geracao_w,
	nr_seq_arquivo_w,
	nm_banco_w,
	cd_convenio_w,
	ds_conta_comprom_w,
	cd_banco_w
from	estabelecimento		b,
	cobranca_escritural		a,
	banco_estabelecimento	c
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:= 	'A'|| 
					'1' || 
					cd_convenio_w || 
					upper(nm_empresa_w) || 
					cd_banco_w || 
					upper(nm_banco_w) || 
					dt_geracao_w ||
					lpad(nr_seq_arquivo_w,6,'0') || 
					'04' ||	
					rpad('DEBITO AUTOMATICO',17,' ') || 
					ds_brancos_52_w;

qt_registros_w	:= qt_registros_w + 1;

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	1);
nr_seq_apres_w	:= nr_seq_apres_w + 1;
/* Fim Header */

/* Transacao */
open C01;
loop
fetch C01 into
	cd_agencia_bancaria_w,
	ds_ocorrencia_1_w,
	ds_ocorrencia_2_w,
	ds_ocorrencia_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_moeda_w,
	ds_uso_empresa_w,
	ds_mensagen_w,
	ds_ident_cliente_emp_w,
	ds_ident_cliente_emp_atual_w,
	ds_ident_cliente_banco_w;
exit when C01%notfound;
	begin
	nr_seq_apres_w	:= nr_seq_apres_w + 1;
	qt_registros_w	:= qt_registros_w + 1;
	vl_total_w	:= vl_total_w + vl_titulo_w;

  ds_ident_cliente_emp_w := rpad('00000' || substr(ds_ident_cliente_emp_w,1,14),25,' ');
  
	/* TIPO E */
	ds_conteudo_w	:= 	'E' || 
						ds_ident_cliente_emp_w || 
						cd_agencia_bancaria_w || 
						ds_ident_cliente_banco_w ||'  '||
						dt_vencimento_w || 
						lpad(replace(to_char(vl_titulo_w, 'fm00000000000.00'),'.',''),15,'0') || '03' ||
						ds_uso_empresa_w || 
						ds_brancos_20_w ||'0';

	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
	/* FIM TIPO E */
	end;
end loop;
close C01;
/* Fim Transacao */

/* Trailler */
select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;

nr_seq_apres_w	:= nr_seq_apres_w + 1;
qt_registros_w	:= qt_registros_w + 1;

ds_conteudo_w	:= 	'Z' || lpad(qt_registros_w,6,'0') || lpad(replace(to_char(vl_total_w, 'fm00000000000.00'),'.',''),17,'0') ||
			ds_brancos_126_w;

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_apres_w);
/* Fim Trailler*/

commit;

end gerar_cobranca_cecred_150;
/
