create or replace
procedure HC_transferir_agendamento(
			nm_usuario_p		Varchar2,
			nr_seq_agenda_origem_p	number,
			nr_seq_agenda_destino_p	number,
			cd_pessoa_fisica_p		Varchar2,
			ie_periodo_destino_p	Varchar2,
			dt_inicial_p		date,
			dt_final_p			date,
			nr_motivo_transf_p number default null) is 
			
nr_sequencia_w		number(10);	
cd_profissional_w	varchar2(10);
nr_seq_prof_hc_w	number(10);

Cursor C01 is
	select	nr_sequencia
	from	agenda_hc_paciente
	where	nr_seq_agenda = nr_seq_agenda_origem_p
	and	((cd_pessoa_fisica = cd_pessoa_fisica_p) or (cd_pessoa_fisica_p = '0'))
	and	((ie_periodo_destino_p = 'S' and trunc(dt_agenda) >= trunc(sysdate)) or
		(ie_periodo_destino_p = 'N' and trunc(dt_agenda) between trunc(dt_inicial_p) and trunc(dt_final_p)))
	and	ie_status_agenda = 'A'
	order by 1;
begin

if ( nr_seq_agenda_origem_p is not null) and ( nr_seq_agenda_destino_p is not null) then


select	max(cd_pessoa_resp)
into	cd_profissional_w
from	agenda_home_care
where	nr_sequencia = nr_seq_agenda_destino_p;

select	max(nr_sequencia)
into	nr_seq_prof_hc_w
from	hc_profissional
where	cd_pessoa_fisica = cd_profissional_w
and		ie_situacao = 'A';

open C01;
loop
fetch C01 into
	nr_sequencia_w;
exit when C01%notfound;
	begin
	
	update	agenda_hc_paciente
	set		nr_seq_agenda = nr_seq_agenda_destino_p,
			nr_seq_motivo_transf = nr_motivo_transf_p 
	where	nr_sequencia = nr_sequencia_w;

	update	hc_agenda_prof
	set		nr_seq_prof_hc = nr_seq_prof_hc_w
	where	nr_seq_agenda = nr_sequencia_w;

	
	end;
end loop;
close C01;	

end if;	

commit;

end HC_transferir_agendamento;
/
