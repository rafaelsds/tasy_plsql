create or replace
procedure pls_finalizar_analise_grupo
			(	nr_seq_analise_p		in	number,
				nm_usuario_p			in	varchar2,
				nr_seq_grupo_atual_p		in	number,
				ie_consistir_pendencias_p	in	varchar2,
				ie_alerta_confirmado_p		in	varchar2,
				cd_estabelecimento_p		in	number,
				ie_commit_p			in	varchar2,
				ie_lib_automatic_p		in	Varchar2,
				ie_glosa_atend_p		in	varchar2,
				ie_origem_finalizacao_p		in	varchar2,
				ds_mensagem_retorno_p		out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Finalizar a an�lise do grupo auditor atual (An�lise Nova)
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

-------------------------------------------------------------------------------------------------------------------
Par�metro
	ie_origem_finalizacao_p - Utilizado para identificar quando a procedure foi 
	chamada diretamente na Gest�o de An�lise, com o par�metro [20]

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */  

ds_mensagem_retorno_w		varchar2(4000);
nm_usuario_exec_w		varchar2(255);
ie_forma_final_analise_w	varchar2(3);
ie_fechar_contas_w		varchar2(3);
ie_existe_grupo_final_w		varchar2(1);
ie_existe_final_anali_w		varchar2(1);
ie_pendencia_grupo_w		varchar2(1)	:= 'N';
ie_fluxo_ant_w			varchar2(1);
ie_analise_grupo_w		number(10);
nr_seq_grupo_analise_w		number(10);
nr_seq_grupo_w			number(10);
ie_existe_usuario_grupo_w	number(10);
ie_existe_grupos_abertos_w	number(10);
nr_seq_regra_w			number(10);
qt_grupos_analise_w		number(10);
nr_seq_fatura_w			number(10);
qt_sem_fluxo_w			number(10);
nr_seq_grupo_final_w		number(10);
nr_seq_solic_pedido_w		number(10);
nr_seq_parecer_pedido_w		number(10);
qt_ocor_impede_finalizacao_w	number(10);
nr_seq_ordem_atual_w		number(10);
nr_seq_glo_ocor_grupo_w		number(10);
nr_seq_ocor_benef_w		number(10);
dt_finalizacao_w		date;
nr_seq_ordem_grupo_atual_w	Number(10);
qt_consistir_w			pls_integer	:= 0;
qt_grupo_pre_analise_w		pls_integer	:= 0;
qt_grupo_dif_pre_analise_w	pls_integer	:= 0;
qt_grupo_analise_w		pls_integer	:= 0;
qt_glosa_conta_analise_w	pls_integer	:= 0;
ie_consiste_analise_w		pls_param_analise_conta.ie_consiste_analise%type;
nr_seq_grupo_pre_analise_w	pls_parametros.nr_seq_grupo_pre_analise%type;
ie_param_51_w			funcao_parametro.vl_parametro_padrao%type := 'N';	
ie_param_30_w			funcao_parametro.vl_parametro_padrao%type := 'S';
ie_analise_encerrada_w		varchar2(1) := 'N';
qt_proc_pendente_w		pls_integer := 0;
qt_mat_pendente_w		pls_integer := 0;
qt_status_encerrada_w		pls_integer;
ie_status_w			pls_analise_conta.ie_status%type;

Cursor C01 is
	select	b.nr_sequencia,
		a.nr_sequencia
	from	pls_ocorrencia			c,
		pls_ocorrencia_benef		a,
		pls_analise_glo_ocor_grupo	b
	where	c.nr_sequencia		= a.nr_seq_ocorrencia
	and	a.nr_sequencia		= b.nr_seq_ocor_benef
	and	b.ie_status		= 'P'
	and	c.ie_auditoria_conta	= 'N'
	and	b.nr_seq_analise	= nr_seq_analise_p
	and	b.nr_seq_grupo		= nr_seq_grupo_atual_p;

Cursor C02 is
	select	nr_sequencia
	from	pls_conta
	where	nr_seq_analise	= nr_seq_analise_p;
begin
--Necess�rio verificar se o status da an�lise j� n�o est� cancelado para n�o retorceder o mesmo
select	max(ie_status)
into	ie_status_w
from	pls_analise_conta
where 	nr_sequencia	= nr_seq_analise_p;

if	(ie_status_w	!= 'C') then
	if	(nr_seq_grupo_atual_p is not null) then

		select	max(a.nr_seq_ordem)
		into	nr_seq_ordem_grupo_atual_w
		from	pls_auditoria_conta_grupo a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.nr_seq_grupo		= nr_seq_grupo_atual_p;

	else
		nr_seq_ordem_grupo_atual_w	:= null;
	end if;

	open C01;
	loop
	fetch C01 into	
		nr_seq_glo_ocor_grupo_w,
		nr_seq_ocor_benef_w;
	exit when C01%notfound;
		begin
		update	pls_analise_glo_ocor_grupo
		set	ie_status	= 'M',
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_glo_ocor_grupo_w;
		
		update	pls_ocorrencia_benef
		set	ie_situacao		= 'I',
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			ie_forma_inativacao	= decode(ie_forma_inativacao,'S','US','US','US','U')
		where	nr_sequencia		= nr_seq_ocor_benef_w;
		end;
	end loop;
	close C01;

	select	max(ie_consiste_analise)
	into	ie_consiste_analise_w
	from	pls_param_analise_conta
	where	cd_estabelecimento	= cd_estabelecimento_p;

	ie_consiste_analise_w	:= nvl(ie_consiste_analise_w,'N');

	obter_param_usuario(1365,3,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_forma_final_analise_w);
	/*	ie_forma_final_analise_w
		I - Impedir
		A - Alertar e depois glosar
		G - Glosar itens pendentes */

	ds_mensagem_retorno_p	:= null;

	if	(nvl(ie_consistir_pendencias_p,'S') = 'S') then
		select	max(a.nr_seq_ordem)
		into	nr_seq_ordem_atual_w
		from	pls_auditoria_conta_grupo	a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.nr_seq_grupo		= nr_seq_grupo_atual_p
		and	a.dt_liberacao is null;
		
		select	decode(count(1),0,'N','S')
		into	ie_fluxo_ant_w
		from	pls_analise_pedido_parecer	a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.nr_seq_grupo_atual	= nr_seq_grupo_atual_p
		and	a.dt_finalizacao is null
		and	a.nr_seq_novo_fluxo < nr_seq_ordem_atual_w;
		
		if	(ie_fluxo_ant_w = 'S') then
			select	count(1)
			into	qt_ocor_impede_finalizacao_w
			from	pls_ocorrencia_benef b,
				pls_conta a
			where	a.nr_sequencia		= b.nr_seq_conta
			and	a.nr_seq_analise	= nr_seq_analise_p
			and	ie_encaminhamento = 'S'
			and	dt_encaminhamento is null
			and	rownum			= 1;

			if	(qt_ocor_impede_finalizacao_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(210282);
			end if;
		end if;
		
		/*Se houver ocorrencias a corrigir n�o � possivel liberar*/
		if	(nvl(ie_lib_automatic_p,'N') = 'N') then
			if	(pls_obter_se_grupo_fim_analise(nr_seq_analise_p,null,null,null,null,nr_seq_grupo_atual_p) = 'S') then
				/*Existem ocorr�ncias ainda pendentes que n�o permitem finalizar a an�lise (Status de an�lise vermelho).
				� necess�rio corrigi-las primeiro. */
				wheb_mensagem_pck.exibir_mensagem_abort(205161, null, -20012);
			end if;
		end if;
		/* Glosar primeiro para consistir depois */
		if	(nvl(ie_glosa_atend_p,'N') = 'S') or /*Parametro para quando glosar o atendimento  glosar os itens que est�o pendentes*/
			((ie_forma_final_analise_w = 'G') or
			(ie_forma_final_analise_w = 'A' and ie_alerta_confirmado_p = 'S')) then
			pls_analise_glosar_itens_pend(nr_seq_analise_p,nr_seq_grupo_atual_p,cd_estabelecimento_p,nm_usuario_p, ie_glosa_atend_p);
		end if;
		
		if	(nvl(ie_lib_automatic_p,'N') = 'N') then
			
			select 	count(1)
			into	qt_consistir_w
			from	pls_conta
			where	nr_seq_analise	= nr_seq_analise_p
			and	ie_consistir_conta_analise = 'S';
			
			
			--Verifica se � para consistir e se o par�metro n�o est� para autom�tico
			if	(qt_consistir_w	> 0) and
				(ie_consiste_analise_w = 'N') then
				wheb_mensagem_pck.exibir_mensagem_abort(306851);
			end if;
			
			ie_pendencia_grupo_w	:= pls_obter_pend_grupo_analise(nr_seq_analise_p,null,null, null, null, nr_seq_grupo_atual_p, 'N');
			if	(ie_pendencia_grupo_w = 'S') then
				/* A an�lise n�o p�de ser finalizada pois existem ocorr�ncias ainda pendentes para seu grupo de an�lise (Status de an�lise do item amarelo).
				Voc� pode marcar a op��o "Pendentes" para verificar quais s�o estes itens. */
				if	(ie_forma_final_analise_w = 'A' and ie_alerta_confirmado_p = 'N') then
					ds_mensagem_retorno_p	:= 'Aten��o! H� itens pendentes de an�lise para seu grupo.' || 
									chr(13) || chr(10) ||
									'Se voc� finalizar a an�lise todos esses itens ser�o glosados. Confirma a finaliza��o?';
				elsif	(ie_forma_final_analise_w = 'I') then		--
					wheb_mensagem_pck.exibir_mensagem_abort(205163);
				end if;
			end if;
		end if;
		
		/* Se for o �ltimo grupo do fluxo, devem verificar se h� itens sem fluxo ainda pendentes */
		if	(pls_obter_se_fim_fluxo_analise(nr_seq_analise_p,nr_seq_grupo_atual_p) = 'S') then
			
			--Caso for o �ltimo grupo da an�lise, ent�o consiste caso tenha pend�ncia
			if	(qt_consistir_w	> 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(306851);
			end if;
		
			/*Se houver an�lises de ocorrencia a fazer n�o � possivel liberar*/
			select	max(a.nr_sequencia)
			into	qt_sem_fluxo_w
			from	pls_ocorrencia c,
				pls_ocorrencia_benef a,
				pls_conta b
			where	a.nr_seq_ocorrencia = c.nr_sequencia
			and	b.nr_sequencia = a.nr_seq_conta
			and	b.nr_seq_analise	= nr_seq_analise_p
			and	(a.ie_situacao		= 'A' or a.ie_situacao is null)
			and	(c.ie_auditoria_conta = 'S' or c.ie_auditoria_conta is null)
			and	((a.ie_auditoria = 'S') or (a.ie_auditoria is null))
			and     not exists (	select 	1
						from    pls_analise_glo_ocor_grupo g
						where   g.nr_seq_ocor_benef = a.nr_sequencia
						and	(g.ie_status <> 'P' or g.ie_status is null))
			and	not exists (	select	1
						from	pls_conta_proc x
						where	x.nr_sequencia = a.nr_seq_conta_proc
						and	x.ie_status = 'D')
			and	(ie_forma_final_analise_w = 'A' and ie_alerta_confirmado_p = 'N')
			and	not exists	(select	1
						from	tiss_motivo_glosa y,
							pls_conta_glosa x
						where	x.nr_seq_motivo_glosa = y.nr_sequencia
						and	((x.nr_sequencia = a.nr_seq_glosa) or (x.nr_seq_ocorrencia_benef = a.nr_sequencia))
						and	y.cd_motivo_tiss in ('1705','1706'))
			and	(c.ie_glosar_pagamento = 'S' or c.ie_glosar_pagamento is null)
			and	not exists (	select	1
						from	pls_conta_pos_estabelecido	x,
							pls_conta_proc			y
						where	x.nr_seq_conta_proc		= y.nr_sequencia
						and	a.nr_seq_conta_proc		= y.nr_sequencia
						and	y.ie_status			= 'M'
						and	x.ie_status_faturamento		= 'A'
						union all
						select	1
						from	pls_conta_pos_estabelecido	x,
							pls_conta_mat			y
						where	x.nr_seq_conta_mat		= y.nr_sequencia
						and	a.nr_seq_conta_mat		= y.nr_sequencia
						and	y.ie_status			= 'M'
						and	x.ie_status_faturamento		= 'A');
			
			
			if	(qt_sem_fluxo_w > 0) then
			
				if	(ie_forma_final_analise_w = 'A' and ie_alerta_confirmado_p = 'N') then
					ds_mensagem_retorno_p	:= '(01) Aten��o! H� itens sem fluxo de auditoria definido e ainda pendentes.' || 
									chr(13) || chr(10) ||
									'Se voc� finalizar a an�lise todos esses itens ser�o glosados. Confirma a finaliza��o?';
				else
					wheb_mensagem_pck.exibir_mensagem_abort(205163);
				end if;
			else	
				select	count(1)
				into	qt_sem_fluxo_w
				from	pls_conta b
				where	b.nr_seq_analise	= nr_seq_analise_p
				and	((exists		(select	1
							 from	pls_conta_proc	x
							 where	x.nr_seq_conta = b.nr_sequencia 
							 and	x.ie_status not in ('D','L','S','M'))) or
					(exists		(select	1
							 from	pls_conta_mat	x
							 where	x.nr_seq_conta = b.nr_sequencia 
							 and	x.ie_status not in ('D','L','S','M'))))		 
				and	rownum < 2
				and	(ie_forma_final_analise_w = 'A' and ie_alerta_confirmado_p = 'N');
			
				if	(qt_sem_fluxo_w > 0) then
					for r_c02_w in C02() loop
						begin
						-- trata a libera��o autom�tica do item
						pls_liberar_item_automatic(	r_c02_w.nr_sequencia, null, null, nm_usuario_p);
						commit;
						--Necess�rio rodar o processo para que os valores utilizados para pagamento sejam atualizados corretamente
						pls_atualiza_lib_conta(	r_c02_w.nr_sequencia,'A',nm_usuario_p);
						commit;
						end;
					end loop;
					
					select	count(1)
					into	qt_sem_fluxo_w
					from	pls_conta b
					where	b.nr_seq_analise	= nr_seq_analise_p
					and	((exists		(select	1
								 from	pls_conta_proc	x
								 where	x.nr_seq_conta = b.nr_sequencia 
								 and	x.ie_status not in ('D','L','S','M'))) or
						(exists		(select	1
								 from	pls_conta_mat	x
								 where	x.nr_seq_conta = b.nr_sequencia 
								 and	x.ie_status not in ('D','L','S','M'))))		 
					and	rownum < 2
					and	(ie_forma_final_analise_w = 'A' and ie_alerta_confirmado_p = 'N');
				
					if	(qt_sem_fluxo_w > 0) then
						if	(ie_forma_final_analise_w = 'A' and ie_alerta_confirmado_p = 'N') then
							ds_mensagem_retorno_p	:= '(02) Aten��o! H� itens ainda pendentes de libera��o.' || 
											chr(13) || chr(10) ||
											'Se voc� finalizar a an�lise todos esses itens ser�o glosados. Confirma a finaliza��o?';
						else
							wheb_mensagem_pck.exibir_mensagem_abort(205163);
						end if;
					end if;
				elsif	(nvl(ie_glosa_atend_p,'N')	!= 'S') then
						select	count(1)
						into	qt_sem_fluxo_w
						from	pls_conta_glosa	glosa,
								pls_conta	conta
						where	glosa.nr_seq_conta	= conta.nr_sequencia
						and	glosa.ie_situacao	= 'A'
						and	conta.ie_status		!= 'C'
						and	conta.nr_seq_analise	= nr_seq_analise_p
								--Caso retornar maior que zero, ent�o quer dizer que estaria analisada a glosa, independente da mesma estar ativa ou inavita. Ent�o
								--verifica se igual a zero, para que o select da consist�ncia de glosa pendente possa alertar o fato de ter item pendente ainda
						and (	(select	count(1)
								from	pls_analise_glo_ocor_grupo a
								where	a.nr_seq_analise		= nr_seq_analise_p
								and		a.nr_seq_conta_glosa	= glosa.nr_sequencia
								and		((a.ie_fluxo_adic	is null) or (a.ie_fluxo_adic	= 'N'))) = 0								
								)
						and	not exists (	select	1
									from	pls_conta_proc x
									where	x.nr_sequencia = glosa.nr_seq_conta_proc
									and	x.ie_status in ('D', 'M')
									union all
									select	1
									from	pls_conta_mat x
									where	x.nr_sequencia = glosa.nr_seq_conta_mat
									and	x.ie_status in ('D', 'M'))
						and	not exists	(select	1
									from	tiss_motivo_glosa y
									where	glosa.nr_seq_motivo_glosa = y.nr_sequencia
									and	y.cd_motivo_tiss in ('1705','1706'))
						and     not exists 	(select 	1
									from    pls_analise_glo_ocor_grupo g
									where   g.nr_seq_conta_glosa = glosa.nr_sequencia
									and	(g.ie_status != 'P' or g.ie_status is null))									
						and	not exists	(select	1
									from	pls_ocorrencia_benef	ocor
									where	1 = 1
									and	((ocor.nr_sequencia 	= glosa.nr_seq_ocorrencia_benef) or
										(ocor.nr_seq_glosa 	= glosa.nr_sequencia)));
						if	(qt_sem_fluxo_w > 0)	then			
							if	(ie_forma_final_analise_w = 'A' and ie_alerta_confirmado_p = 'N') then
								ds_mensagem_retorno_p	:= '(03) Aten��o! H� itens sem fluxo de auditoria definido e ainda pendentes.' || 
												chr(13) || chr(10) ||
												'Se voc� finalizar a an�lise todos esses itens ser�o glosados. Confirma a finaliza��o?';
							else
								
								wheb_mensagem_pck.exibir_mensagem_abort(205163);
							end if;
						end if;		
				end if;
			end if;
		end if;
	end if;

	if	(ds_mensagem_retorno_p is null) then
		pls_analise_gerar_grupos_enc(nr_seq_analise_p, nr_seq_grupo_atual_p, 'N', cd_estabelecimento_p,1,null,null,nm_usuario_p);
		select	count(1)
		into	ie_existe_grupos_abertos_w
		from	pls_auditoria_conta_grupo a		
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.dt_liberacao is null
		and	nvl(ie_pre_analise,'N')	 = 'N'
		and	rownum = 1;
			
		--Verifica se � para consistir e se o par�metro est� para autom�tico
		if	(qt_consistir_w > 0) and
			(ie_consiste_analise_w = 'S') and
			(ie_existe_grupos_abertos_w = 1) then
			insert into pls_cta_analise_cons
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					nr_seq_analise,ie_status)
			values	(	pls_cta_analise_cons_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					nr_seq_analise_p,'P');
					
			update	pls_analise_conta
			set	ie_status	= 'D'
			where	nr_sequencia	= nr_seq_analise_p;
		elsif	(qt_consistir_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(306851);
		end if;

		select	max(nr_sequencia)
		into	nr_seq_parecer_pedido_w
		from	pls_analise_pedido_parecer
		where	nr_seq_grupo_parecer	= nr_seq_grupo_atual_p
		and	nr_seq_analise		= nr_seq_analise_p;

		if	(nr_seq_parecer_pedido_w is not null) then
			update	pls_analise_pedido_parecer
			set	dt_finalizacao	= sysdate
			where	nr_sequencia	= nr_seq_parecer_pedido_w;
		end if;

		update	pls_auditoria_conta_grupo
		set	dt_liberacao 		= sysdate,
			dt_final_analise	= sysdate,
			ie_origem_finalizacao	= ie_origem_finalizacao_p,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			nm_auditor_atual	= decode(ie_pre_analise, 'S', nm_usuario_p, nm_auditor_atual)
		where	nr_sequencia		= 	(select	max(nr_sequencia) 
							from	pls_auditoria_conta_grupo
							where	nr_seq_grupo 		= nr_seq_grupo_atual_p	
							and	nr_seq_analise		= nr_seq_analise_p
							and	dt_liberacao 		is null		
							and	nr_seq_ordem 		=	(select	min(nr_seq_ordem)		 
												from	pls_auditoria_conta_grupo
												where	nr_seq_grupo 		= nr_seq_grupo_atual_p	
												and	nr_seq_analise		= nr_seq_analise_p
												and	dt_liberacao		is null	));

		pls_gravar_inicio_fim_analise(nr_seq_analise_p, nr_seq_grupo_atual_p, 'F', nm_usuario_p);

		select	count(1)
		into	ie_existe_grupos_abertos_w
		from	pls_auditoria_conta_grupo a		
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.dt_liberacao is null
		and	nvl(ie_pre_analise,'N')	 = 'N'
		and	rownum = 1;
		
		
		select 	count(1)
		into	qt_status_encerrada_w
		from	pls_analise_conta
		where 	nr_sequencia 	= nr_seq_analise_p
		and	ie_status	= 'T';
		
		if	(qt_status_encerrada_w	> 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1024241);
		end if;
		/*Fazer verifica��o se existe grupos de analise ainda em aberto se n�o houver  fechar a analise*/
		if	(ie_existe_grupos_abertos_w = 0) then
			/*Obter o grupo responsavel por fechar a conta*/
			pls_obter_grupo_fechar_analise(nr_seq_analise_p, cd_estabelecimento_p, nr_seq_regra_w, nr_seq_grupo_w);
			
			if	(nvl(nr_seq_grupo_w,0) > 0) then
				ie_existe_grupo_final_w	:= 'S';
			
				/*obter se este grupo j� foi inserido na an�lise*/
				select	decode(count(nr_sequencia),0,'N','S')
				into	ie_existe_final_anali_w
				from	pls_auditoria_conta_grupo
				where	nr_seq_grupo	= nr_seq_grupo_w
				and	nr_seq_analise	= nr_seq_analise_p
				and	rownum = 1;
			else		
				/*Caso n�o haja regra de grupo de finaliza��o*/
				ie_existe_grupo_final_w	:= 'N';
			end if;	
			
			/*Se n�o existe grupo de finaliza��o na an�lise esta � encerrada*/
			if	(ie_existe_grupo_final_w = 'N') then
				ie_analise_encerrada_w	:= 'N';
				--par�metro 51 Encerrar atendimento ao finalizar fluxo de pr�-an�lise(Caso n�o tiver nenhum fluxo que n�o seja o vinculado ao grupo de pr�-an�lise)
				Obter_Param_Usuario(1293, 51, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_param_51_w);
				if	(ie_param_51_w = 'S') then
					/*Se existir somente o grupo do auditor ent�o � liberado a an�lise.*/
					select	max(a.nr_seq_grupo_pre_analise)
					into	nr_seq_grupo_pre_analise_w
					from	pls_parametros	a
					where	a.cd_estabelecimento	= cd_estabelecimento_p;
					
					--Se chegou at� aqui � por que tem apenas um grupo na an�lise e caso esse grupo for o grupo
					--da pr�-analise
					select 	count(1)
					into	qt_grupo_pre_analise_w
					from	pls_auditoria_conta_grupo a
					where 	a.nr_seq_grupo = nr_seq_grupo_pre_analise_w
					and	a.nr_seq_analise = nr_seq_analise_p;
					
					select 	count(1)
					into	qt_grupo_dif_pre_analise_w
					from	pls_auditoria_conta_grupo a
					where 	a.nr_seq_grupo <> nr_seq_grupo_pre_analise_w
					and	a.nr_seq_analise = nr_seq_analise_p;
				end if;
				
				pls_alterar_status_analise_cta(nr_seq_analise_p, 'L', 'PLS_FINALIZAR_ANALISE_GRUPO', nm_usuario_p, cd_estabelecimento_p);
				
				-- [30] - Liberar autom�ticamente an�lise quando n�o houver glosa e ocorr�ncia (OPS - Controle de Produ��o M�dica)
				Obter_Param_Usuario(1285, 30, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_param_30_w);
				
				--Se somente tiver um fluxo na an�lise e o mesmo for vinculado ao grupo auditor da pr�-an�lise e o par�metro 51 ad fun��o A500 estiver apontando que 
				--deve ser encerrado atendimento, ent�o o faz.
				--OBS: Mantenho a chamada para a rotina pls_alterar_status_analise_cta passando par�metro L, para que fa�a atualiza��o da pls_auditoria_conta_grupo e aqui nesse momento, chamo ela 
				--novamente passando 'T' Atendimento encerrado,  que somente atualizar� o status na pls_analise_conta
				if	(qt_grupo_pre_analise_w > 0 and qt_grupo_dif_pre_analise_w = 0 and ie_param_51_w = 'S') then
					pls_alterar_status_analise_cta(nr_seq_analise_p, 'T', 'PLS_FINALIZAR_ANALISE_GRUPO', nm_usuario_p, cd_estabelecimento_p);
					ie_analise_encerrada_w	:= 'S';
				elsif	(ie_param_30_w = 'S') then						
					select 	count(1)
					into	qt_grupo_analise_w
					from	pls_auditoria_conta_grupo a
					where 	a.nr_seq_analise = nr_seq_analise_p;
									
					select	count(1)
					into	qt_glosa_conta_analise_w
					from	pls_conta_glosa	glosa,
						pls_conta	conta
					where	glosa.nr_seq_conta	= conta.nr_sequencia
					and	glosa.ie_situacao	= 'A'
					and	conta.ie_status		<> 'C'
					and	conta.nr_seq_analise	= nr_seq_analise_p;
					
					select	count(1)
					into	qt_proc_pendente_w
					from	pls_conta_proc a,
						pls_conta b
					where	b.nr_sequencia = a.nr_seq_conta
					and	b.nr_seq_analise = nr_seq_analise_p
					and	a.ie_status not in ('L','S','A','D','M');
					
					select	count(1)
					into	qt_mat_pendente_w
					from	pls_conta_mat a,
						pls_conta b
					where	b.nr_sequencia = a.nr_seq_conta
					and	b.nr_seq_analise = nr_seq_analise_p
					and	a.ie_status not in ('L','S','A','D','M');
										
					if	(qt_grupo_analise_w = 0) and 
						(qt_glosa_conta_analise_w = 0) and
						(qt_proc_pendente_w = 0) and
						(qt_mat_pendente_w = 0) then
						pls_alterar_status_analise_cta(nr_seq_analise_p, 'T', 'PLS_FINALIZAR_ANALISE_GRUPO', nm_usuario_p, cd_estabelecimento_p);
						ie_analise_encerrada_w	:= 'S';
					end if;
				end if;
				
				update	pls_analise_conta
				set	dt_liberacao_analise	= sysdate,
					dt_final_analise	= sysdate
				where	nr_sequencia		= nr_seq_analise_p;
			else			
				/*Se o grupo de finaliza��o existir na an�lise.*/
				if	(ie_existe_final_anali_w = 'S') then
					select	count(nr_sequencia)
					into	qt_grupos_analise_w
					from	pls_auditoria_conta_grupo
					where	nr_seq_analise = nr_seq_analise_p;
					
					/*Se existir mais de uma grupo de analise*/
					if	(qt_grupos_analise_w > 1) and 
						(pls_obter_se_auditor_grupo(nr_seq_grupo_w, nm_usuario_p) = 'N') then
						/*Se o grupo de finaliza��o existir na an�lise ent�o seu sua libera��o � desfeita. Permitindo que o mesmo se torne o fluxo da vez. */
						pls_desf_final_grupo_analise(nr_seq_analise_p, nr_seq_grupo_w, null, nm_usuario_p, cd_estabelecimento_p,'N');
					else
						/*Se existir somente o grupo do auditor ent�o � liberado a an�lise.*/
						pls_alterar_status_analise_cta(nr_seq_analise_p, 'L', 'PLS_FINALIZAR_ANALISE_GRUPO', nm_usuario_p, cd_estabelecimento_p);
						
						update	pls_analise_conta
						set	dt_liberacao_analise	= sysdate,
							dt_final_analise	= sysdate
						where	nr_sequencia 		= nr_seq_analise_p;
					end if;
				else
					/*Se o grupo de finaliza��o n�o existir na an�lise este � acrescentado*/
					pls_inserir_grupo_analise(nr_seq_analise_p, nr_seq_grupo_w, 'Grupo inserido atrav�s da regra de finaliza��o '||nr_seq_regra_w,
								nr_seq_grupo_atual_p, 'N', nm_usuario_p, cd_estabelecimento_p);
				end if;
			end if;	 
		end if;

		pls_inserir_hist_analise(null, nr_seq_analise_p, 7, null, null, null, null, null, nr_seq_grupo_atual_p, nm_usuario_p, cd_estabelecimento_p);

		update	pls_analise_conta
		set	ie_status_pre_analise	= decode(ie_pre_analise,'S','F',ie_status_pre_analise)
		where	nr_sequencia		= nr_seq_analise_p;

		pls_atualizar_grupo_penden(nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);

		select  max(a.nr_sequencia)
		into    nr_seq_fatura_w
		from    ptu_fatura              a,
			pls_conta               b
		where   b.nr_seq_fatura         = a.nr_sequencia
		and     b.nr_seq_analise        = nr_seq_analise_p;

		/* Atualizar valores PTU Fatura*/
		pls_atualizar_valor_ptu_fatura(nr_seq_fatura_w,'N');
		
		if	(ie_commit_p = 'S') then
			pls_tratar_cont_fluxo_analise(nr_seq_analise_p, nr_seq_grupo_atual_p, cd_estabelecimento_p, nm_usuario_p, nr_seq_grupo_final_w, ie_fechar_contas_w);
		end if;
		
		if	(nr_seq_grupo_final_w is not null) then
			pls_finalizar_analise_grupo(nr_seq_analise_p, nm_usuario_p, nr_seq_grupo_final_w, 'N', 'N', 
				cd_estabelecimento_p, 'N',ie_lib_automatic_p,ie_glosa_atend_p,ie_origem_finalizacao_p, ds_mensagem_retorno_w);
		end if;
		
		if	(ie_fechar_contas_w = 'S') or
			(ie_analise_encerrada_w	= 'S')then
			pls_analise_fechar_contas(nr_seq_analise_p, nr_seq_grupo_final_w, cd_estabelecimento_p, nm_usuario_p, 'N');
		end if;
		
		if	(nvl(ie_commit_p,'S') = 'S') then
			commit;
		end if;
	end if;
end if;

end pls_finalizar_analise_grupo;
/
