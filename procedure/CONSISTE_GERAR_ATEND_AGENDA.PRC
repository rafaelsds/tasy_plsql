create or replace
procedure Consiste_Gerar_Atend_Agenda
		(nr_seq_agenda_p	in	number,
		ds_consistencia_p	out	varchar2) is

cd_pessoa_fisica_w	varchar2(10);
dt_nascimento_w		date;
ie_sexo_w		varchar2(01);

begin

select	nvl(max(cd_pessoa_fisica),'0')
into	cd_pessoa_fisica_w
from 	agenda_paciente
where	nr_sequencia	= nr_seq_agenda_p;

if	(cd_pessoa_fisica_w <> '0') then
	begin


	select	max(dt_nascimento),
		max(ie_sexo)
	into	dt_nascimento_w,
		ie_sexo_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

	if	(dt_nascimento_w is null) then
		ds_consistencia_p	:= ds_consistencia_p || wheb_mensagem_pck.get_texto(796354) || ' ' || chr(13) || chr(10);		
	end if;

	if	(ie_sexo_w is null) then
		ds_consistencia_p	:= ds_consistencia_p || wheb_mensagem_pck.get_texto(796355) || ' ' || chr(13) || chr(10);		
	end if;

	end;
end if;

end Consiste_Gerar_Atend_Agenda;
/