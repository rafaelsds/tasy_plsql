create or replace
procedure HSM_IMPORTA_PESSOA_JURIDICA is 

cd_tipo_pessoa_w		varchar2(50);
cd_cgc_w			varchar2(50);
ds_razao_social_w		pessoa_juridica.ds_razao_social%type;
nm_fantasia_w			varchar2(255);
cd_cep_w			varchar2(50);
ds_endereco_w			varchar2(255);
nr_endereco_w			varchar2(50);
ds_bairro_w			varchar2(255);
ds_municipio_w			varchar2(255);
sg_estado_w			varchar2(255);
nr_telefone_w			varchar2(50);
nr_inscricao_estadual_w		varchar2(50);
nr_inscricao_municipal_w	varchar2(50);
ds_site_w			varchar2(255);
cd_ans_w			varchar2(255);
qt_existe_w			varchar2(255);
qt_posicoes_w			varchar2(255);
cd_estabelecimento_w		varchar2(255);
cd_sistema_anti_w		varchar2(50);
cd_cnes_w			varchar2(50);
cd_banco_w			varchar2(50);
cd_agencia_w			varchar2(50);
cd_dig_verificador_w		varchar2(50);
cd_conta_corrente_w		varchar2(50);
cd_conta_contabil_w		varchar2(50);
sql_errm_w			varchar(255);

cursor c01 is
select	replace(replace(replace(cpfcnpj,'/',''),'.',''),'-',''),
	substr(razao_soci,1,255),
	decode(cep,'0','',cep),
	substr(nvl(logradouro,' '),1,40),
	substr(numero,1,50),
	substr(bairro,1,255),
	substr(nvl(municipio,'X'),1,255),
	substr(nvl(estado,''),1,2),
	decode(INSCRICAOE,'0','',substr(INSCRICAOE,1,50)) INSCRICAOE,
	substr(INSCRICAOm,1,50) INSCRICAOm,
	1,
	substr(nome_fanta,1,255) nome_fanta,
	substr(cd_sistema,1,50),
	substr(cnes,1,50),
	substr(banco,1,50),
	substr(agencia_ag,1,50),
	substr(agencia_dv,1,50),
	substr(contacorre,1,50),
	substr(C_D_C__CON,1,50)
from	W_PJ_OS252133;

begin

wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('N');

begin
open C01;
loop
fetch C01 into	
	cd_cgc_w,
	ds_razao_social_w,
	cd_cep_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_bairro_w,
	ds_municipio_w,
	sg_estado_w,
	nr_inscricao_estadual_w,
	nr_inscricao_municipal_w,
	cd_tipo_pessoa_w,
	nm_fantasia_w,
	cd_sistema_anti_w,
	cd_cnes_w,
	cd_banco_w,
	cd_agencia_w,
	cd_dig_verificador_w,
	cd_conta_corrente_w,
	cd_conta_contabil_w;
exit when C01%notfound;
	begin

	select	count(*)
	into	qt_existe_w
	from	pessoa_juridica
	where	cd_cgc = cd_cgc_w;
	

	if	(qt_existe_w = 0) and
		(cd_cgc_w <> '0') then
		begin
		insert into pessoa_juridica (
				cd_cgc,
				ds_razao_social,
				nm_fantasia,
				cd_cep,
				ds_endereco,
				nr_endereco,
				ds_bairro,
				ds_municipio,
				sg_estado,
				dt_atualizacao,
				nm_usuario,
				nr_telefone,
				nr_inscricao_estadual,
				nr_inscricao_municipal,
				cd_tipo_pessoa,
				ie_prod_fabric,
				ie_situacao,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_ans,
				cd_sistema_ant,
				cd_cnes)
			values(	lpad(cd_cgc_w,14,0),
				ds_razao_social_w,
				nvl(nm_fantasia_w,ds_razao_social_w),
				cd_cep_w,
				ds_endereco_w,
				nr_endereco_w,
				ds_bairro_w,
				ds_municipio_w,
				sg_estado_w,
				sysdate,
				'TASY_OS252133',
				null,
				nr_inscricao_estadual_w,
				nr_inscricao_municipal_w,
				cd_tipo_pessoa_w,
				'N',
				'A',
				sysdate,
				'TASY_OS252133',
				cd_ans_w,
				cd_sistema_anti_w,
				cd_cnes_w);
		qt_existe_w	:= 1;
		exception
			when others then
			sql_errm_w	:= sqlerrm;
			insert	into	log_tasy
				(cd_log,
				nm_usuario,
				dt_atualizacao,
				ds_log)
			values	(55870,
				'TASY_OS252133',
				sysdate,
				'Erro ao inserir pessoa juridica. ' 	|| chr(13) || chr(10) ||
				'Nome= ' || ds_razao_social_w 	|| chr(13) || chr(10) ||
				'Erro= ' || sql_errm_w);
			end;
		end if;
		
		if ( nvl(cd_banco_w,0) > 0 ) and (qt_existe_w = 1) then
			begin
			insert into pessoa_juridica_conta(
				cd_banco,
				cd_agencia_bancaria,
				cd_codigo_identificacao,
				nr_conta,
				cd_cgc,
				dt_atualizacao,
				nm_usuario,
				ie_situacao,
				ie_conta_pagamento)
			values( cd_banco_w,
				cd_agencia_w,
				cd_dig_verificador_w,
				cd_conta_corrente_w,
				lpad(cd_cgc_w,14,0),
				sysdate,
				'TASY_OS252133',
				'A',
				'N' );
			qt_existe_w	:= 1;
			exception
				when others then
				sql_errm_w	:= sqlerrm;
				insert	into	log_tasy
					(cd_log,
					nm_usuario,
					dt_atualizacao,
					ds_log)
				values	(55870,
					'TASY_OS252133',
					sysdate,
					'Erro ao inserir pessoa juridica. ' 	|| chr(13) || chr(10) ||
					'Nome= ' || ds_razao_social_w 	|| chr(13) || chr(10) ||
					'Erro= ' || sql_errm_w);
			end;
		end if;

		if (cd_conta_contabil_w is not null) and (qt_existe_w = 1) then
			begin
			insert into PESSOA_JUR_CONTA_CONT(
				cd_conta_contabil,
				cd_cgc,
				cd_empresa,
				dt_atualizacao,
				ie_tipo_conta,
				nm_usuario,
				nr_sequencia)
			values	(cd_conta_contabil_w,
				lpad(cd_cgc_w,14,0),
				1,
				sysdate,
				'P',
				'TASY_OS252133',
				PESSOA_JUR_CONTA_CONT_seq.nextval);
			exception
				when others then
				sql_errm_w	:= sqlerrm;
				insert	into	log_tasy
					(cd_log,
					nm_usuario,
					dt_atualizacao,
					ds_log)
				values	(55870,
					'TASY_OS252133',
					sysdate,
					'Erro ao inserir pessoa juridica. ' 	|| chr(13) || chr(10) ||
					'Nome= ' || ds_razao_social_w 	|| chr(13) || chr(10) ||
					'Erro= ' || sql_errm_w);
			end;
		end if;	 
	end;

	commit;
end loop;
close C01;

exception
	when others then
	wheb_usuario_pck.SET_IE_EXECUTAR_TRIGGER('S');
	/*#@DS_ERRO#@
	Pessoa = #@DS_RAZAO_SOCIAL#@ */
	wheb_mensagem_pck.exibir_mensagem_abort(267046, 'DS_ERRO=' || sqlerrm || ';DS_RAZAO_SOCIAL=' || ds_razao_social_w);
end;
commit;

end HSM_IMPORTA_PESSOA_JURIDICA ;
/
