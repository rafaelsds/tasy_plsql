create or replace
procedure gerar_w_nota_fiscal(
			dt_inicio_p		date,
			dt_final_p		date,
			nm_usuario_p		varchar2,
			ie_setor_usuario_p	varchar2,
			nr_nota_fiscal_p	number,
			cd_estabelecimento_p	number,
			ds_filtro_p		varchar2,
			ie_entrada_saida_P	varchar2,
			ie_usuario_filtro_p	varchar2 default 'N') is 
			
c001				integer;			
cd_setor_nota_w			number(5);
cd_setor_usuario_w		number(5);
retorno_w			number(5);	
qt_notas_rps_w			number(10);
nr_seq_lote_w			number(10,0) := 0;
ie_grava_w			varchar2(1);
ds_comando_w			varchar2(2000) := '';
ie_nota_cancelada_w		varchar2(01);
ie_nota_estornada_w		varchar2(01);
ie_somente_nota_calculada_w	varchar2(01);
ie_tratamento_estorno_w		varchar2(01);
ie_entrada_saida_w		varchar2(01);
nr_seq_nota_fiscal_w		varchar2(50);
nm_usuario_w			varchar2(50);
ie_primeira_vez_w		varchar2(1) := 'S';
ie_webservice_w			varchar2(1);
nr_seq_exportada_w		number(10) := 0;

begin

if	(ie_usuario_filtro_p = 'S') then
	delete from w_nota_fiscal where nm_usuario = nm_usuario_p;	
else
	delete from w_nota_fiscal;		
end if;

ie_entrada_saida_w:= ie_entrada_saida_p;
if (ie_entrada_saida_w is null) then
	ie_entrada_saida_w:= 'S';
end if;

ie_webservice_w			:= substr(nvl(obter_valor_param_usuario(40, 176, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'N'), 1, 1);
ie_nota_cancelada_w		:= substr(nvl(obter_valor_param_usuario(40, 110, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'S'), 1, 1);
ie_nota_estornada_w		:= substr(nvl(obter_valor_param_usuario(40, 142, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'S'), 1, 1);
ie_somente_nota_calculada_w	:= substr(nvl(obter_valor_param_usuario(40, 124, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'S'), 1, 1);
ie_tratamento_estorno_w		:= substr(nvl(obter_valor_param_usuario(40, 181, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'S'), 1, 1);
qt_notas_rps_w			:= nvl(obter_valor_param_usuario(40, 156, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 999999);

ds_comando_w 	:= ' select nr_sequencia, nm_usuario from nota_fiscal ' || 
		   ' where 1 = 1 ' ||		   
		   ' and dt_emissao between :dt_inicio and fim_dia(:dt_final) ' || 
		   ' and nvl(ie_status_envio, :ie_status_envio) = :ie_status_envio ' ||
		   ' and cd_estabelecimento = :cd_estabelecimento ' ||
		   ' and rownum <= :qt_notas_rps ' || ' ' || ds_filtro_p;

if	(nvl(nr_nota_fiscal_p,0) > 0) then
	ds_comando_w	:= ds_comando_w || ' and nr_nota_fiscal = :nr_nota_fiscal ';
end if;

if	(ie_nota_cancelada_w = 'N') then
	ds_comando_w	:= ds_comando_w || ' and ie_situacao <> :ie_situacao ';
end if;

if	(ie_nota_estornada_w = 'N') then
	ds_comando_w	:= ds_comando_w || ' and ie_situacao <> :ie_situacao_um ';	
	ds_comando_w	:= ds_comando_w || ' and ie_situacao <> :ie_situacao_dois ';		
elsif	(ie_tratamento_estorno_w = 'S') then
	ds_comando_w	:= ds_comando_w || ' and obter_nf_estorno_periodo(nr_sequencia, :dt_inicio_est, fim_dia(:dt_final_est)) = :ie_opcao ';	
elsif   	(ie_nota_estornada_w = 'S') and (ie_tratamento_estorno_w = 'N') then
	ds_comando_w	:= ds_comando_w || ' and ie_situacao <> :ie_situacao_um ';	
end if;

if	(ie_somente_nota_calculada_w = 'S') then
	ds_comando_w	:= ds_comando_w || ' and dt_atualizacao_estoque is not null ';
end if;		   

if	(ie_entrada_saida_w <> 'A') then
	ds_comando_w	:= ds_comando_w || ' and (substr(obter_se_nota_entrada_saida(nr_sequencia,''N''),1,1) = :ie_entrada_saida) ';
end if;		   
		   
c001 := dbms_sql.open_cursor;

dbms_sql.parse(c001, ds_comando_w, dbms_sql.native);

dbms_sql.define_column(c001, 1, nr_seq_nota_fiscal_w, 50);		
dbms_sql.define_column(c001, 2, nm_usuario_w, 50);	

dbms_sql.bind_variable(c001, 'DT_INICIO', dt_inicio_p, 50);
dbms_sql.bind_variable(c001, 'DT_FINAL', dt_final_p, 50);
dbms_sql.bind_variable(c001, 'IE_STATUS_ENVIO', 'N', 50);
dbms_sql.bind_variable(c001, 'CD_ESTABELECIMENTO', cd_estabelecimento_p, 50);
dbms_sql.bind_variable(c001, 'QT_NOTAS_RPS', qt_notas_rps_w, 50);

if	(nvl(nr_nota_fiscal_p,0) > 0) then
	dbms_sql.bind_variable(c001, 'NR_NOTA_FISCAL', nr_nota_fiscal_p, 50);
end if;

if	(ie_nota_cancelada_w = 'N') then
	dbms_sql.bind_variable(c001, 'IE_SITUACAO', '9', 50);	
end if;

if	(ie_nota_estornada_w = 'N') then
	dbms_sql.bind_variable(c001, 'IE_SITUACAO_UM', '3', 50);		
	dbms_sql.bind_variable(c001, 'IE_SITUACAO_DOIS', '2', 50);				
elsif	(ie_tratamento_estorno_w = 'S') then	
	dbms_sql.bind_variable(c001, 'DT_INICIO_EST', dt_inicio_p, 50);			
	dbms_sql.bind_variable(c001, 'DT_FINAL_EST', dt_final_p, 50);	
	dbms_sql.bind_variable(c001, 'IE_OPCAO', 'S', 50);				
elsif	(ie_nota_estornada_w = 'S') and (ie_tratamento_estorno_w = 'N') then
	dbms_sql.bind_variable(c001, 'IE_SITUACAO_UM', '3', 50);		
end if;

if	(ie_entrada_saida_w <> 'A') then
	dbms_sql.bind_variable(c001, 'IE_ENTRADA_SAIDA', ie_entrada_saida_w, 50);
end if;	

retorno_w := dbms_sql.execute(c001);

while	(dbms_sql.fetch_rows(c001) > 0 ) loop
	begin
	
	dbms_sql.column_value(c001, 1, nr_seq_nota_fiscal_w);
	dbms_sql.column_value(c001, 2, nm_usuario_w);
		
	ie_grava_w	:= 'S';

	select	nvl(max(obter_setor_usuario(nm_usuario_w)),0)
	into	cd_setor_nota_w
	from	dual;

	/*Se quem estiver exportando tiver algum setor que o usuario da nota tambem tenha*/
	if	(ie_setor_usuario_p = 'S') then
		
		select	nvl(max(obter_setor_usuario(nm_usuario_p)),0)
		into	cd_setor_usuario_w
		from	dual;
		/*Se o usuario da nota, ou usuario da exportação nao tiver o setor, nao exporta. Nesse caso ie_setor_usuario_p deve ser N*/
		if	(cd_setor_usuario_w <> cd_setor_nota_w) then
			ie_grava_w	:= 'N';
		end if;
		
	end if;

	if	(ie_grava_w = 'S') then
		begin		
		
		if	(ie_webservice_w = 'S') then -- se for webservice (NFS-e)			
		
			if	(nr_seq_lote_w = 0) then
				select	nota_fiscal_lote_nfe_seq.nextval
				into	nr_seq_lote_w
				from	dual;
			end if;
				
			if (ie_primeira_vez_w = 'S') then
				insert into nota_fiscal_lote_nfe 
					(nr_sequencia,
					cd_estabelecimento,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_geracao,
					dt_envio)
				values	(nr_seq_lote_w,
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					sysdate);
			end if;
			ie_primeira_vez_w := 'N';	

			select	nota_fiscal_exportada_seq.nextval 				
			into	nr_seq_exportada_w
			from	dual;						
			
			insert into NOTA_FISCAL_EXPORTADA(
				NR_SEQUENCIA,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_arquivo)
			values	(nr_seq_exportada_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				'');

			update	nota_fiscal 
			set 	nr_seq_nf_lote_nfe = nr_seq_lote_w,
				ie_status_envio = 'E',
				nr_seq_exportada = nr_seq_exportada_w 
			where	nr_sequencia = nr_seq_nota_fiscal_w;	
			
		else
		
			if	(nr_seq_lote_w = 0) then
				select	nota_fiscal_exportada_seq.nextval 				
				into	nr_seq_lote_w
				from	dual;
			end if;
						
		end if;
						
		insert into w_nota_fiscal(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_nota_fiscal,
			nr_seq_lote,
			dt_inicial,
			dt_final)
		values(	w_nota_fiscal_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_nota_fiscal_w,
			nr_seq_lote_w,
			dt_inicio_p,
			dt_final_p);
		
		end;
	end if;		
	
	end;	
end loop; 

dbms_sql.close_cursor(c001);

commit;

end gerar_w_nota_fiscal;
/
