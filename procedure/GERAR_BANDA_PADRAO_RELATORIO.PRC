CREATE OR REPLACE PROCEDURE Gerar_Banda_Padrao_Relatorio(
    nr_sequencia_p NUMBER,
    nm_usuario_p   VARCHAR2)
IS
  qt_registro_w        NUMBER(10);
  query_dominio        VARCHAR2(550);
  qt_tamanho_banda_w   NUMBER;
  qt_altura_banda_w    NUMBER;
  nm_atributo_w        VARCHAR2(60);
  ds_conteudo_w        VARCHAR2(60);
  ds_label_w_v         VARCHAR2(60);
  ie_tipo_campo_w_v    VARCHAR2(10);
  nr_sequencia_w       NUMBER(15);
  
  nm_label_w           VARCHAR2(80);
  ds_label_w           VARCHAR2(255);
  nr_seq_apres_w       NUMBER(10);
  ie_ajustar_tamanho_w VARCHAR2(38);
  nr_altura_w          NUMBER(38);
  nr_comprimento_w     NUMBER(38);
  nr_eixo_y_w          NUMBER(38);
  nr_eixo_x_w          NUMBER(38);
  ie_tipo_fonte_w      VARCHAR2(1);
  cd_tipo_w            VARCHAR2(10);
  nr_tamanho_fonte_w   NUMBER(4,0);
  nr_seq_banda_w       banda_relatorio.nr_sequencia%type;
  
  CURSOR C01
  IS
    SELECT a.nm_label,
      a.ds_label,
      a.nr_seq_apres,
      a.ie_ajustar_tamanho,
      a.nr_altura,
      a.nr_comprimento,
      a.nr_eixo_y,
      a.nr_eixo_x,
      a.ie_tipo_fonte,
      a.cd_tipo,
      a.nr_tam_fonte
    FROM relatorio_dinamico a
    WHERE a.nr_tipo_banda      = 2;

BEGIN
  limpar_relatorio_dinamico(nr_sequencia_p);

  SELECT COUNT(*)
  INTO qt_registro_w
  FROM banda_relatorio
  WHERE nr_seq_relatorio = nr_sequencia_p
  AND ie_tipo_banda      = 'C';
 
  IF (qt_registro_w      = 0) THEN
    INSERT
    INTO banda_relatorio
      (
        nr_sequencia,             -- 1
        ie_tipo_banda,            -- 2
        ds_banda,                 -- 3
        dt_atualizacao,           -- 4
        nm_usuario,               -- 5
        qt_altura,                -- 6
        ds_cor_fundo,             -- 7
        ie_quebra_pagina,         -- 8
        ie_reimprime_nova_pagina, -- 9
        ie_alterna_cor_fundo,     -- 10
        ie_imprime_vazio,         -- 11
        ie_imprime_primeiro,      -- 12
        ie_borda_sup,             -- 13
        ie_borda_inf,             -- 14
        ie_borda_esq,             -- 15
        ie_borda_dir,             -- 16
        nr_seq_relatorio,         -- 17
        nr_seq_apresentacao,      -- 18
        ds_cor_header,            -- 19
        ds_cor_footer,            -- 20
        ds_cor_quebra,            -- 21
        ie_banda_padrao           -- 22
      )
      VALUES
      (
        banda_relatorio_seq.nextval,              -- 1
        'C',                                      -- 2
        obter_desc_expressao(486585,'Cabecalho'), -- 3
        sysdate,                                  -- 4
        nm_usuario_p,                             -- 5
        70,                                       -- 6
        'clwhite',                                -- 7
        'N',                                      -- 8
        'N',                                      -- 9
        'N',                                      -- 10
        'N',                                      -- 11
        'N',                                      -- 12
        'N',                                      -- 13
        'S',                                      -- 14
        'N',                                      -- 15
        'N',                                      -- 16
        nr_sequencia_p,                           -- 17
        1,                                        -- 18
        'clsilver',                               -- 19
        'clwhite',                                -- 20
        '$00E5E5E5',                              -- 21
        'S'                                       -- 22
      );                                          
  END IF;
  
  SELECT COUNT(*)
  INTO qt_registro_w
  FROM banda_relatorio
  WHERE nr_seq_relatorio = nr_sequencia_p
  AND ie_tipo_banda      = 'R';
  
  IF (qt_registro_w      = 0) THEN
    INSERT
    INTO banda_relatorio
      (
        nr_sequencia,             -- 1
        ie_tipo_banda,            -- 2
        ds_banda,                 -- 3
        dt_atualizacao,           -- 4
        nm_usuario,               -- 5
        qt_altura,                -- 6
        ds_cor_fundo,             -- 7
        ie_quebra_pagina,         -- 8
        ie_reimprime_nova_pagina, -- 9
        ie_alterna_cor_fundo,     -- 10
        ie_imprime_vazio,         -- 11
        ie_imprime_primeiro,      -- 12
        ie_borda_sup,             -- 13
        ie_borda_inf,             -- 14
        ie_borda_esq,             -- 15
        ie_borda_dir,             -- 16
        nr_seq_relatorio,         -- 17
        nr_seq_apresentacao,      -- 18
        ds_cor_header,            -- 19
        ds_cor_footer,            -- 20
        ds_cor_quebra,            -- 21
        ie_banda_padrao           -- 22
      )
      VALUES
      (
        banda_relatorio_seq.nextval,
        'R',
        obter_desc_expressao(486586,'Rodape'),
        sysdate,
        nm_usuario_p,
        18,
        'clwhite',
        'N',
        'N',
        'N',
        'N',
        'N',
        'S',
        'N',
        'N',
        'N',
        nr_sequencia_p,
        10,
        'clsilver',
        'clwhite',
        '$00E5E5E5',
        'S'
      );
  END IF;
  COMMIT;
end Gerar_Banda_Padrao_Relatorio;
/
