create or replace
procedure GERAR_CID_EUP_FROM_GV(
			nm_usuario_p		Varchar2,
			nr_seq_gestao_p	Varchar2,
			nr_atendimento_p	number,
			cd_medico_resp_p	number) is 

cd_cid_principal_w	gestao_vaga.cd_cid_principal%type;
dt_diagnostico_w	date;

begin

if (nr_seq_gestao_p is not null) then

	begin
		select 	cd_cid_principal 
		into 	cd_cid_principal_w
		from 	gestao_vaga 
		where 	nr_sequencia = nr_seq_gestao_p
		and 	nr_seq_agenda is null;

	exception
	when others then
		cd_cid_principal_w := null;
	end;
	
	if (cd_cid_principal_w is not null) then
	
	dt_diagnostico_w := sysdate;
	
		insert into  diagnostico_medico(
			nr_atendimento,                 
			dt_diagnostico,                 
			ie_tipo_diagnostico,           
			cd_medico,                      
			dt_atualizacao,                 
			nm_usuario)
		values 	(
			nr_atendimento_p,
			dt_diagnostico_w,		
			1,
			cd_medico_resp_p,
			sysdate,
			nm_usuario_p);			
				
		insert into diagnostico_doenca(
			nr_atendimento,         
			dt_diagnostico,          
			cd_doenca,              
			dt_atualizacao,
			nm_usuario)        
		values (
			nr_atendimento_p,
			dt_diagnostico_w,
			cd_cid_principal_w,
			sysdate,
			nm_usuario_p);

	end if;
end if;

commit;

end GERAR_CID_EUP_FROM_GV;
/