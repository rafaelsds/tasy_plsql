create or replace
procedure insert_treatment_plan_team(
			nr_sequencia_p		number,
			cd_funcao_medico_p	number,
			cd_pessoa_fisica_p	varchar2,
			cd_person_type_p	varchar2,
			nm_usuario_p		varchar2) is 

cd_person_w		number(10,0);
cd_physician_w	number(10,0);
cd_cargo_w		number(10,0);

begin
	cd_physician_w := null;
	cd_person_w := null;
	
	if (cd_person_type_p = '1' or cd_person_type_p = '2') then
		cd_physician_w := cd_pessoa_fisica_p;
	
	else
		cd_person_w := cd_pessoa_fisica_p;
	end if;
	
	select max(cd_cargo)
	into cd_cargo_w
	from funcao_medico
	where cd_funcao = cd_funcao_medico_p;
	
	insert into med_treatment_plan_team (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_med_treat_plan,
		cd_physician,
		cd_person,
		cd_position,
		si_person_type)
	values (
		med_treatment_plan_team_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_p,
		cd_physician_w,
		cd_person_w,
		cd_cargo_w,
		cd_person_type_p);
	
	commit;

end insert_treatment_plan_team;
/
