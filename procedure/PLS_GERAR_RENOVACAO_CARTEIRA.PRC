create or replace
procedure pls_gerar_renovacao_carteira
			(	nr_seq_contrato_p	number,
				nr_seq_segurado_p	number,
				ie_opcao_p		varchar2,
				nm_usuario_p		varchar2) is

qt_meses_renovacao_contrato_w	pls_carteira_renovacao.qt_meses_renovacao%type	:= 0;
qt_meses_regra_operadora_w	pls_param_regra_carteira.qt_dias_renovacao%type	:= 0;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_gerar_validade_cartao_w	pls_parametros.ie_gerar_validade_cartao%type;
dt_adesao_w			pls_segurado.dt_contratacao%type;
dt_validade_carteira_w		date;
qt_meses_renovacao_w		number(10)	:= 0;
ie_tipo_contrato_w		varchar2(2);
ie_sem_vencimento_w		varchar2(10);
ie_altera_validade_w		varchar2(10) := 'N';
ie_titularidade_w		varchar2(10);
ie_limite_dependencia_w		varchar2(10);
dt_fim_dependencia_w		date;
qt_registros_w			number(10);
nr_seq_titular_w		number(10);
ie_renovacao_cartao_dt_atual_w	varchar2(10);
ie_venc_cart_fim_mes_w		varchar2(10);
ie_dependente_venc_titular_w	varchar2(10);
dt_base_calculo_cart_w		date;
dt_rescisao_w			date;
dt_validade_carteira_tit_w	date;
nr_seq_seg_carteira_w		number(10);
nr_via_cartao_w			number(10);
ie_tipo_segurado_w		varchar2(10);
ie_venc_cartao_rescisao_w	varchar2(1);
dt_validade_w			date;
dt_contrato_w			date;
dt_base_carteira_w		date;
dt_validade_cart_param_int_w	pls_parametros.dt_base_validade_carteira%type;
dt_validade_atual_w		pls_segurado_carteira.dt_validade_carteira%type;
dt_rescisao_programada_w	date;

Cursor C01 is
	select	max(qt_meses_renovacao),
		max(ie_sem_vencimento),
		max(ie_limite_dependencia),
		max(ie_dependente_venc_titular)
	from	pls_carteira_renovacao
	where	nr_seq_contrato				= nr_seq_contrato_p
	and	nvl(ie_tipo_renovacao,ie_opcao_p)	= ie_opcao_p
	and	((nvl(ie_titularidade,'A')		= ie_titularidade_w) or (nvl(ie_titularidade,'A') = 'A'))
	and	((nvl(ie_limite_dependencia,'N')	= 'S' and dt_fim_dependencia_w is not null) or (nvl(ie_limite_dependencia,'N') = 'N'))
	order by nvl(ie_tipo_renovacao, ' ');

Cursor C02 is
	select	qt_dias_renovacao,
		nvl(ie_sem_vencimento,'N'),
		ie_limite_dependencia,
		ie_dependente_venc_titular
	from	pls_param_regra_carteira
	where	nvl(ie_tipo_renovacao,ie_opcao_p)	= ie_opcao_p
	and	((ie_tipo_contrato	= ie_tipo_contrato_w) or (ie_tipo_contrato = 'A'))
	and	((ie_contrato		= 'O') or (ie_contrato is null))
	and	((nvl(ie_titularidade,'A')	= ie_titularidade_w) or (nvl(ie_titularidade,'A') = 'A'))
	and	((ie_tipo_segurado	= ie_tipo_segurado_w) or (ie_tipo_segurado is null))
	order by nvl(ie_tipo_renovacao, ' '),
		ie_tipo_contrato;

begin

select	cd_estabelecimento,
	dt_contrato,
	dt_base_carteira,
	dt_base_validade_carteira
into	cd_estabelecimento_w,
	dt_contrato_w,
	dt_base_carteira_w,
	dt_validade_cart_param_int_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_p;

select	dt_contratacao,
	decode(nr_seq_titular,null,'T','D'),
	decode(cd_estabelecimento_w,null,cd_estabelecimento,cd_estabelecimento_w),
	dt_rescisao,
	nr_seq_titular,
	ie_tipo_segurado
into	dt_adesao_w,
	ie_titularidade_w,
	cd_estabelecimento_w,
	dt_rescisao_w,
	nr_seq_titular_w,
	ie_tipo_segurado_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

select	ie_gerar_validade_cartao,
	ie_renovacao_cartao_dt_atual,
	ie_venc_cart_fim_mes,
	nvl(ie_venc_cartao_rescisao,'N'),
	nvl(nvl(dt_validade_cart_param_int_w,dt_base_validade_carteira),'B')
into	ie_gerar_validade_cartao_w,
	ie_renovacao_cartao_dt_atual_w,
	ie_venc_cart_fim_mes_w,
	ie_venc_cartao_rescisao_w,
	dt_validade_cart_param_int_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_w;

begin
select	max(dt_validade_carteira),
	max(nr_sequencia),
	max(nr_via_solicitacao)
into	dt_validade_atual_w,
	nr_seq_seg_carteira_w,
	nr_via_cartao_w
from	pls_segurado_carteira
where	nr_seq_segurado		= nr_seq_segurado_p;
exception
when others then
	dt_validade_atual_w	:= sysdate;
	nr_seq_seg_carteira_w	:= null;
end;

dt_validade_carteira_w	:= dt_validade_atual_w;

dt_base_calculo_cart_w	:= dt_adesao_w;

-- Caso nao existir data de validade, ou a data de validade seja conforme data e ano do contrato, deve sempre gerar com a data base do contrato
if	((dt_validade_carteira_w is null) or (dt_validade_cart_param_int_w = 'A')) then
	dt_validade_carteira_w := pls_obter_validade_cart_benef(dt_adesao_w, dt_contrato_w, dt_base_carteira_w, dt_validade_cart_param_int_w, cd_estabelecimento_w);
	
	if	(dt_validade_carteira_w is null) then
		dt_validade_carteira_w	:= sysdate;
	end if;
end if;

-- aaschlote 28/05/2012 - OS 453034 - Caso o tipo venha da origem de via adicional a contagem de dias deve ser atraves da data atual
if	(ie_opcao_p in('V','R')) and
	(ie_renovacao_cartao_dt_atual_w	= 'S') then
	dt_base_calculo_cart_w	:= sysdate;
end if;

dt_fim_dependencia_w	:= pls_obter_dt_limite_depend(nr_seq_segurado_p);

-- Verificar na funcao OPS - Gestao de Operadoras/Parametros da OPS, na pasta Contratos e Beneficiario, se pode gerar a validade da carteira
if	(nvl(ie_gerar_validade_cartao_w,'S')	= 'S') then
	open C01;
	loop
	fetch C01 into
		qt_meses_renovacao_contrato_w,
		ie_sem_vencimento_w,
		ie_limite_dependencia_w,
		ie_dependente_venc_titular_w;
	exit when C01%notfound;
	end loop;
	close C01;
	
	ie_sem_vencimento_w		:= nvl(ie_sem_vencimento_w,'N');
	ie_limite_dependencia_w		:= nvl(ie_limite_dependencia_w,'N');
	ie_dependente_venc_titular_w	:= nvl(ie_dependente_venc_titular_w,'N');
	
	-- aaschlote 12/09/2011 - Verifica se possui dias para renovacao ou sera nao gera renovacao
	if	(nvl(qt_meses_renovacao_contrato_w,-1) <> -1) or
		(ie_sem_vencimento_w = 'S') or
		(ie_limite_dependencia_w = 'S') or
		(ie_dependente_venc_titular_w = 'S') then
		qt_meses_renovacao_w	:= qt_meses_renovacao_contrato_w;
	else
		begin
		select	decode(cd_cgc_estipulante,null,'PF','PJ')
		into	ie_tipo_contrato_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_p;
		exception
		when others then
			ie_tipo_contrato_w	:= null;
		end;
		
		open C02;
		loop
		fetch C02 into
			qt_meses_regra_operadora_w,
			ie_sem_vencimento_w,
			ie_limite_dependencia_w,
			ie_dependente_venc_titular_w;
		exit when C02%notfound;
		end loop;
		close C02;
		
		qt_meses_renovacao_w	:= qt_meses_regra_operadora_w;
	end if;
	
	if	(ie_limite_dependencia_w = 'S') then
		dt_validade_carteira_w	:= dt_fim_dependencia_w;
		ie_altera_validade_w	:= 'S';
	elsif	(ie_dependente_venc_titular_w = 'S') and
		(nr_seq_titular_w is not null) then
		begin
		select	max(dt_validade_carteira)
		into	dt_validade_carteira_tit_w
		from	pls_segurado_carteira
		where	nr_seq_segurado	= nr_seq_titular_w;
		exception
		when others then
			dt_validade_carteira_tit_w	:= sysdate;
		end;
		
		dt_validade_carteira_w	:= dt_validade_carteira_tit_w;
		ie_altera_validade_w	:= 'S';
	else
		if	(nvl(qt_meses_renovacao_w,0) <> 0) then
			if	(ie_venc_cartao_rescisao_w = 'S') and
				(dt_rescisao_w is not null) then
				dt_validade_carteira_w	:= dt_rescisao_w;
			elsif	(ie_opcao_p in('V','R')) and
				(ie_renovacao_cartao_dt_atual_w	= 'S') then
				dt_validade_carteira_w	:= dt_base_calculo_cart_w + qt_meses_renovacao_w;
			else
				dt_validade_carteira_w	:= dt_validade_carteira_w + qt_meses_renovacao_w;
			
				while	(trunc(dt_base_calculo_cart_w,'dd') >= trunc(dt_validade_carteira_w,'dd')) loop
					dt_validade_carteira_w	:= dt_validade_carteira_w + qt_meses_renovacao_w;
				end loop;
				
				if	(dt_validade_cart_param_int_w = 'A') then
					while	(trunc(dt_validade_atual_w,'dd') >= trunc(dt_validade_carteira_w,'dd')) loop
						dt_validade_carteira_w	:= dt_validade_carteira_w + qt_meses_renovacao_w;
					end loop;
				end if;
			end if;
			
			ie_altera_validade_w	:= 'S';	
			if	(ie_venc_cart_fim_mes_w  = 'S') then
				dt_validade_carteira_w	:= last_day(dt_validade_carteira_w);
			end if;
		elsif	(nvl(qt_meses_renovacao_w,0) = 0) and
			(ie_sem_vencimento_w = 'S') then
			dt_validade_carteira_w	:= null;
			ie_altera_validade_w	:= 'S';
		end if;
	end if;
	
	if	(ie_altera_validade_w	= 'S') then
		if	(dt_validade_carteira_w is not null) and
			(ie_opcao_p <> 'A') and 
			(dt_validade_cart_param_int_w <> 'B') then
			select	count(1)
			into	qt_registros_w
			from	pls_segurado		d,
				pls_segurado_carteira	c,
				pls_carteira_vencimento	b,
				pls_contrato		e,
				pls_lote_carteira	a
			where	c.nr_seq_segurado	= d.nr_sequencia
			and	b.nr_seq_seg_carteira	= c.nr_sequencia
			and	b.nr_seq_lote		= a.nr_sequencia
			and	d.nr_seq_contrato	= e.nr_sequencia
			and	e.nr_sequencia		= nr_seq_contrato_p
			and	a.ie_situacao		= 'D'
			and	dt_validade_carteira_w between a.dt_inicial and a.dt_final
			and	rownum			<= 1;
			
			-- aaschlote 24/05/2012 - OS - 444827 - Caso possuir um beneficiario no contrato que tenha feito vencimento no perioro da validade entao adiciona mais quantidade de dias
			if	(qt_registros_w > 0) and
				(nvl(qt_meses_renovacao_w,0) <> 0) then
				dt_validade_carteira_w	:= dt_validade_carteira_w + qt_meses_renovacao_w;
				
				if	(ie_venc_cartao_rescisao_w = 'S') and
					(dt_rescisao_w is not null) then
					dt_validade_carteira_w	:= dt_rescisao_w;
				end if;
				
				if	(ie_venc_cart_fim_mes_w  = 'S') then
					dt_validade_carteira_w	:= last_day(dt_validade_carteira_w);
				end if;
			end if;
		end if;
		
		-- aaschlote 08/08/20121 OS - 478735 - Nao permitir que a validade seja maior que a rescisao do beneficiario
		if	(dt_rescisao_w is not null) and
			(dt_validade_carteira_w is not null) and
			(dt_validade_carteira_w > dt_rescisao_w) then
			dt_validade_carteira_w	:= dt_rescisao_w;
		end if;
		
		-- Verificar se existe resisao programada
		begin
		dt_rescisao_programada_w	:= pls_obter_data_rescisao_seg(nr_seq_segurado_p, ie_venc_cartao_rescisao_w, 'S');
		exception
		when others then
			dt_rescisao_programada_w	:= null;
		end;
		if	(dt_rescisao_programada_w is not null) and
			(dt_rescisao_programada_w < dt_validade_carteira_w) then
			dt_validade_carteira_w	:= dt_rescisao_programada_w;
		end if;	
		
		update	pls_segurado_carteira
		set	dt_validade_carteira	= dt_validade_carteira_w,
			ie_situacao 		=  decode(ie_opcao_p,'A',ie_situacao,'P'),
			nm_usuario 		= nm_usuario_p,
			dt_atualizacao 		= sysdate
		where	nr_seq_segurado		= nr_seq_segurado_p;
		
		if	(nr_seq_seg_carteira_w is not null) then
			select	count(1)
			into	qt_registros_w
			from	pls_segurado_cart_estagio
			where	nr_seq_cartao_seg	= nr_seq_seg_carteira_w
			and	nr_via			= nr_via_cartao_w
			and	ie_estagio_emissao	= '1'
			and	dt_validade_carteira	<> dt_validade_carteira_w
			and	rownum			<= 1;
			
			if	(qt_registros_w > 0) then
				update	pls_segurado_cart_estagio
				set	dt_validade_carteira	= dt_validade_carteira_w
				where	nr_seq_cartao_seg	= nr_seq_seg_carteira_w
				and	nr_via			= nr_via_cartao_w
				and	ie_estagio_emissao	= '1'
				and	dt_validade_carteira	<> dt_validade_carteira_w;
			end if;
		end if;
	end if;
	
	--OS 274241 aaschlote  - 11/12/2010 - Ao gerar a renovacao, atualiar a trilha do cartao
	pls_atualizar_trilha_cartao(nr_seq_segurado_p,nm_usuario_p);
end if;	

end pls_gerar_renovacao_carteira;
/
