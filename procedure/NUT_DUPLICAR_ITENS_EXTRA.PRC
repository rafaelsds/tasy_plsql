create or replace
procedure Nut_Duplicar_Itens_Extra(
			nr_atendimento_p	number,
			dt_servico_base_p	date,
			dt_servico_inicio_p	date,
			dt_servico_final_p	date,
			cd_setor_atendimento_p	number,
			nm_usuario_p		Varchar2,
			nr_seq_servico_p	number) is 


nr_seq_servico_w	number(10);
nr_seq_composicao_w	number(10);
nr_seq_receita_w	number(10);
ie_tipo_requisicao_w	varchar2(1);
ie_tipo_pessoa_w	varchar2(1);
nr_seq_servico_extra_w	number(10);
nr_seq_gen_alim_w	number(10);
nr_seq_servico_novo_w	number(10);
nr_seq_nut_atend_w	number(10);
qt_inserido_w		number(10);
			
			
Cursor C01 is
	select	nr_seq_servico,
		nr_sequencia
	from	NUT_ATEND_SERV_DIA
	where	nr_atendimento		= nr_atendimento_p
	and	cd_setor_atendimento	= cd_setor_atendimento_p
	and	dt_servico between  trunc(dt_servico_inicio_p) and trunc(dt_servico_final_p) + 86399/86400
	and	((nr_seq_servico = nr_seq_servico_p) or (nr_seq_servico_p is null))
	order by 1;			
			
Cursor C02 is
	select	b.nr_seq_servico,
		b.nr_seq_composicao,
		b.nr_seq_receita,
		b.ie_tipo_requisicao,
		b.ie_tipo_pessoa,
		b.nr_seq_servico_extra,
		b.nr_seq_gen_alim
	from	NUT_ATEND_SERV_DIA a,
		NUT_ITEM_EXTRA b
	where	a.nr_sequencia		= b.nr_seq_servico
	and	a.nr_seq_servico	= nr_seq_servico_novo_w
	and	cd_setor_atendimento	= cd_setor_atendimento_p
	and	a.nr_atendimento	= nr_atendimento_p
	and	a.dt_servico		between  trunc(dt_servico_base_p) and trunc(dt_servico_base_p) + 86399/86400
	order by 1;			 
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_servico_novo_w,
	nr_seq_nut_atend_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_servico_w,
		nr_seq_composicao_w,
		nr_seq_receita_w,
		ie_tipo_requisicao_w,
		ie_tipo_pessoa_w,
		nr_seq_servico_extra_w,
		nr_seq_gen_alim_w;
	exit when C02%notfound;
		begin
		select	count(*)
		into	qt_inserido_w
		from	NUT_ITEM_EXTRA
		where	nr_seq_servico		= nr_seq_nut_atend_w
		and	(nvl(nr_seq_servico_extra, nr_seq_servico_extra_w)	= nr_seq_servico_extra_w or (nr_seq_servico_extra_w is null))
		and	(nvl(nr_Seq_composicao, nr_seq_composicao_w)		= nr_seq_composicao_w or (nr_seq_composicao_w is null))
		and	(nvl(nr_seq_receita, nr_seq_receita_w)			= nr_seq_receita_w or (nr_seq_receita_w is null))
		and	(nvl(nr_seq_gen_alim, nr_seq_gen_alim_w)		= nr_seq_gen_alim_w or (nr_seq_gen_alim_w is null))
		and	(nvl(ie_tipo_pessoa, ie_tipo_pessoa_w)			= ie_tipo_pessoa_w or (ie_tipo_pessoa_w is null))
		and	(nvl(ie_tipo_Requisicao, ie_tipo_requisicao_w)		= ie_tipo_requisicao_w or (ie_tipo_requisicao_w is null));

		if	(qt_inserido_w	= 0) then
			insert into NUT_ITEM_EXTRA
					(NR_SEQUENCIA,
					DT_ATUALIZACAO,
					NM_USUARIO,
					DT_ATUALIZACAO_NREC,
					NM_USUARIO_NREC,
					NR_SEQ_SERVICO,
					NR_SEQ_COMPOSICAO,
					NR_SEQ_RECEITA,
					IE_TIPO_REQUISICAO,
					DT_LIBERACAO,
					NM_USUARIO_LIBERACAO,
					DT_ENTREGA,
					NM_USUARIO_ENTREGA,
					IE_TIPO_PESSOA,
					NR_SEQ_SERVICO_EXTRA,
					NR_SEQ_GEN_ALIM)
				values
					(NUT_ITEM_EXTRA_seq.NextVal,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_nut_atend_w,
					nr_seq_composicao_w,
					nr_seq_receita_w,
					ie_tipo_requisicao_w,
					null,
					null,
					null,
					null,
					ie_tipo_pessoa_w,
					nr_seq_servico_extra_w,
					nr_seq_gen_alim_w);
		end if;
		
		end;
	end loop;
	close C02;
	
	
	end;
end loop;
close C01;

commit;

end Nut_Duplicar_Itens_Extra;
/
