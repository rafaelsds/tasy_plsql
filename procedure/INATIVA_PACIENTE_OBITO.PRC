create or replace
procedure Inativa_paciente_obito(nr_sequencia_p	       number,
			ds_justificativa_p     varchar2,
			nm_usuario_p		Varchar2) is 

dt_obito_pf_w		date;
dt_obito_do_w		date;
cd_pessoa_fisica_w	pessoa_fisica.CD_PESSOA_FISICA%type;
begin
	begin
		select	a.dt_obito,
			y.dt_obito,
			a.cd_pessoa_fisica
		into	dt_obito_pf_w,
			dt_obito_do_w,
			cd_pessoa_fisica_w
		from	pessoa_fisica a,
			atendimento_paciente x, 
			declaracao_obito y
		where 	a.cd_pessoa_fisica = x.cd_pessoa_fisica
		and	x.nr_atendimento = y.nr_atendimento 
		and 	y.nr_sequencia = nr_sequencia_p
		and	not exists(	select 	1
					from 	declaracao_obito z
					where	z.nr_atendimento = x.nr_atendimento
					and	z.nr_sequencia <> y.nr_sequencia
					and	z.dt_obito = y.dt_obito);
		
	exception
	when others then
		dt_obito_pf_w 		:= null;
		cd_pessoa_fisica_w	:= null;
	end;
					
					
	update 	declaracao_obito
        set 	dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p,
		ds_justificativa = ds_justificativa_p,
                ie_situacao = 'I'
	where  	nr_sequencia = nr_sequencia_p;
	
	if (dt_obito_pf_w is not null) and 
	   (dt_obito_do_w is not null) and
	   (cd_pessoa_fisica_w is not null) and 
	   (nvl(nr_sequencia_p,0) > 0) then
			
			update	pessoa_fisica
			set	dt_obito = null
			where	cd_pessoa_fisica = cd_pessoa_fisica_w
			and	dt_obito 	 = dt_obito_do_w;
			
	end if;
commit;

end Inativa_paciente_obito;
/

