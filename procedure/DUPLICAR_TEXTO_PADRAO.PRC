create or replace
procedure duplicar_texto_padrao(nr_seq_texto_padrao_p	number,
				ie_commit_p	varchar2 default 'N') is 

med_texto_padrao_w			med_texto_padrao%rowtype;
med_texto_padrao_imagem_w	med_texto_padrao_imagem%rowtype;

cursor c01 is
	select	*
	from	med_texto_padrao_imagem
	where	nr_seq_texto = nr_seq_texto_padrao_p;
			
begin

	select	*
	into	med_texto_padrao_w
	from	med_texto_padrao
	where	nr_sequencia = nr_seq_texto_padrao_p;

	select	med_texto_padrao_seq.nextval
	into	med_texto_padrao_w.nr_sequencia
	from 	dual;

	select	med_texto_padrao_w.ds_titulo || '_' || obter_desc_expressao(347811)
	into	med_texto_padrao_w.ds_titulo
	from 	dual;
	
	med_texto_padrao_w.dt_atualizacao := sysdate;
	med_texto_padrao_w.nm_usuario := wheb_usuario_pck.get_nm_usuario;
	med_texto_padrao_w.dt_atualizacao_nrec := sysdate;
	med_texto_padrao_w.nm_usuario_nrec := wheb_usuario_pck.get_nm_usuario;
	
	insert into med_texto_padrao values med_texto_padrao_w;
	commit;
	
	for c01_w in c01 loop
		select	*
		into	med_texto_padrao_imagem_w
		from	med_texto_padrao_imagem
		where	nr_sequencia = c01_w.nr_sequencia;
		
		med_texto_padrao_imagem_w.nr_seq_texto := med_texto_padrao_w.nr_sequencia;

		select	med_texto_padrao_imagem_seq.nextval
		into	med_texto_padrao_imagem_w.nr_sequencia
		from 	dual;
	
		med_texto_padrao_imagem_w.dt_atualizacao := sysdate;
		med_texto_padrao_imagem_w.nm_usuario := wheb_usuario_pck.get_nm_usuario;
		med_texto_padrao_imagem_w.dt_atualizacao_nrec := sysdate;
		med_texto_padrao_imagem_w.nm_usuario_nrec := wheb_usuario_pck.get_nm_usuario;
		
		insert into med_texto_padrao_imagem values med_texto_padrao_imagem_w;
		commit;
	end loop;

	if	(nvl(ie_commit_p,'N') = 'S') and (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then
		commit;
	end if;

end duplicar_texto_padrao;
/
