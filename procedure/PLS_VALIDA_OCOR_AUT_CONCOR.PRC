/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Validar as regras de concorrentes para as ocorr�ncia combinadas
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
create or replace
procedure pls_valida_ocor_aut_concor
			(	nr_seq_ocor_combinada_p		Number,
				nr_seq_ocorrencia_p		Number,
				nr_seq_segurado_p		Number,
				nr_seq_motivo_glosa_p		Number,
				nr_seq_guia_p			Number,
				nr_seq_requisicao_p		Number,
				nr_seq_execucao_p		Number,
				ie_utiliza_filtro_p		Varchar2,
				nr_seq_param1_p			Number,
				nr_seq_param2_p			Number,
				nr_seq_param3_p			Number,
				nr_seq_param4_p			Number,
				nr_seq_param5_p			Number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number) is


ie_regra_w			Varchar2(255);
ie_gerar_ocorrencia_w		Varchar2(255);
ie_gerou_ocor_cabecalho_w	Varchar2(255);
ie_tipo_ocorrencia_w		Varchar2(255);
nr_seq_oc_benef_w		Number(10);
cd_prestador_w			varchar2(30);
cd_medico_solic_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;


Cursor C01 is
	select	nr_seq_concorrente,
		ie_concorrente_anterior,
		ie_valida_prestador,
		ie_valida_execucao,
		qt_dias_considerar,
		nvl(ie_valida_medico_solic, 'N') ie_valida_medico_solic,
		ie_valida_proc_princ,
		ie_tipo_qtde
	from	pls_validacao_aut_conc
	where	nr_seq_ocor_aut_combinada	= nr_seq_ocor_combinada_p
	and	ie_situacao			= 'A';
	
Cursor C02 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p;
	
Cursor C03 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C04 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	nr_seq_material		is null;

begin

if	(nr_seq_guia_p	is not null) then
	begin
		select	nr_seq_prestador,
			cd_medico_solicitante
		into	nr_seq_prestador_w,
			cd_medico_solic_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;
	exception
	when others then
		nr_seq_prestador_w	:= null;
		cd_medico_solic_w	:= null;
	end;
elsif	(nr_seq_requisicao_p	is not null) then
	begin
		select	nr_seq_prestador,
			cd_medico_solicitante
		into	nr_seq_prestador_w,
			cd_medico_solic_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;
	exception
	when others then
		nr_seq_prestador_w	:= null;
		cd_medico_solic_w	:= null;
	end;
elsif	(nr_seq_execucao_p	is not null) then
	begin
		select	a.nr_seq_prestador,
			b.cd_medico_solicitante
		into	nr_seq_prestador_w,
			cd_medico_solic_w
		from	pls_execucao_requisicao a,
			pls_requisicao b
		where	b.nr_sequencia	= a.nr_seq_requisicao
		and	a.nr_sequencia	= nr_seq_execucao_p;
	exception
	when others then
		nr_seq_prestador_w	:= null;
		cd_medico_solic_w	:= null;
	end;
end if;

begin
	select	nvl(pls_obter_cod_prestador(nr_seq_prestador_w,null),'0')
	into	cd_prestador_w
	from	dual;
exception
when others then
	cd_prestador_w := '0';
end;

for r_C01_w  in C01 loop
		begin	
		if	(nr_seq_guia_p	is not null) then
			for r_C02_w  in C02 loop
				begin
				ie_gerar_ocorrencia_w	:= 'S';

				if	(ie_utiliza_filtro_p	= 'S') then
					/* Tratamento para filtros */
					pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
									nr_seq_execucao_p, r_C02_w.nr_sequencia, null,
									null, null, null,
									r_C02_w.cd_procedimento, r_C02_w.ie_origem_proced, null,
									ie_gerou_ocor_cabecalho_w,nr_seq_prestador_w, null,
									null, null, null,
									nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
									
					if	(ie_regra_w	= 'S') then
						ie_gerar_ocorrencia_w	:= 'S';
					elsif	(ie_regra_w	in ('E','N')) then
						ie_gerar_ocorrencia_w	:= 'N';
					end if;
				end if;
				
				if	(ie_gerar_ocorrencia_w	= 'S') then
				
					ie_gerar_ocorrencia_w	:= pls_obter_se_proc_concorrente(r_C01_w.nr_seq_concorrente, nr_seq_guia_p, nr_seq_requisicao_p,
												nr_seq_execucao_p, nr_seq_prestador_w, r_C02_w.cd_procedimento,
												r_C02_w.ie_origem_proced, r_C01_w.ie_concorrente_anterior, r_C01_w.ie_valida_prestador,
												cd_prestador_w, null, null, 
												r_C01_w.qt_dias_considerar, r_C01_w.ie_valida_medico_solic, cd_medico_solic_w,
												r_C01_w.ie_valida_proc_princ, r_C01_w.ie_tipo_qtde);
													
					if	(ie_gerar_ocorrencia_w	= 'S') then
					
						pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
									nr_seq_guia_p, null, r_C02_w.nr_sequencia, 
									null, nr_seq_ocor_combinada_p, nm_usuario_p, 
									null, nr_seq_motivo_glosa_p, 1, 
									cd_estabelecimento_p, 'N' ,null,
									nr_seq_oc_benef_w, null,
									null, null, null);
								
						pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
										nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
										r_C02_w.nr_sequencia, null, null,
										null, null, null,
										nm_usuario_p, cd_estabelecimento_p);
					end if;
				end if;
				end;
			end loop;
		elsif	(nr_seq_requisicao_p	is not null) then
			for r_C03_w  in C03 loop
				begin
				ie_gerar_ocorrencia_w	:= 'S';

				if	(ie_utiliza_filtro_p	= 'S') then
					/* Tratamento para filtros */
					pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
									nr_seq_execucao_p, null, null,
									r_C03_w.nr_sequencia, null, null,
									r_C03_w.cd_procedimento, r_C03_w.ie_origem_proced, null,
									ie_gerou_ocor_cabecalho_w,nr_seq_prestador_w, null,
									null, null, null,
									nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
									
					if	(ie_regra_w	= 'S') then
						ie_gerar_ocorrencia_w	:= 'S';
					elsif	(ie_regra_w	in ('E','N')) then
						ie_gerar_ocorrencia_w	:= 'N';
					end if;
				end if;
					
				if	(ie_gerar_ocorrencia_w	= 'S') then
					ie_gerar_ocorrencia_w	:= pls_obter_se_proc_concorrente(r_C01_w.nr_seq_concorrente, nr_seq_guia_p, nr_seq_requisicao_p,
												nr_seq_execucao_p, nr_seq_prestador_w, r_C03_w.cd_procedimento,
												r_C03_w.ie_origem_proced, r_C01_w.ie_concorrente_anterior, r_C01_w.ie_valida_prestador,
												cd_prestador_w, nvl(r_C01_w.ie_valida_execucao,'N'), null, 
												r_C01_w.qt_dias_considerar, r_C01_w.ie_valida_medico_solic, cd_medico_solic_w,
												r_C01_w.ie_valida_proc_princ, r_C01_w.ie_tipo_qtde);
							
					if	(ie_gerar_ocorrencia_w	= 'S') then
						pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
									null, null, r_C03_w.nr_sequencia, 
									null, nr_seq_ocor_combinada_p, nm_usuario_p, 
									null, nr_seq_motivo_glosa_p, 5, 
									cd_estabelecimento_p, 'N' ,null,
									nr_seq_oc_benef_w, null,
									null, null, null);
								
						pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
										nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
										null, null, r_C03_w.nr_sequencia,
										null, null, null,
										nm_usuario_p, cd_estabelecimento_p);
					end if;
				end if;
				end;
			end loop;
		elsif	(nr_seq_execucao_p	is not null) then
			for r_C04_w  in C04 loop
				begin
				ie_gerar_ocorrencia_w	:= 'S';

				if	(ie_utiliza_filtro_p	= 'S') then
					/* Tratamento para filtros */
					pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
									nr_seq_execucao_p, null, null,
									null, null, r_C04_w.nr_sequencia,
									r_C04_w.cd_procedimento, r_C04_w.ie_origem_proced, null,
									ie_gerou_ocor_cabecalho_w,nr_seq_prestador_w, null,
									null, null, null,
									nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
									
					if	(ie_regra_w	= 'S') then
						ie_gerar_ocorrencia_w	:= 'S';
					elsif	(ie_regra_w	in ('E','N')) then
						ie_gerar_ocorrencia_w	:= 'N';
					end if;
				end if;
				
				if	(ie_gerar_ocorrencia_w	= 'S') then
					ie_gerar_ocorrencia_w	:= pls_obter_se_proc_concorrente(r_C01_w.nr_seq_concorrente, nr_seq_guia_p, nr_seq_requisicao_p,
												nr_seq_execucao_p, nr_seq_prestador_w, r_C04_w.cd_procedimento,
												r_C04_w.ie_origem_proced, r_C01_w.ie_concorrente_anterior, r_C01_w.ie_valida_prestador,
												cd_prestador_w, null, null, 
												r_C01_w.qt_dias_considerar, r_C01_w.ie_valida_medico_solic, cd_medico_solic_w,
												r_C01_w.ie_valida_proc_princ, r_C01_w.ie_tipo_qtde);
													
					if	(ie_gerar_ocorrencia_w	= 'S') then
						pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
									null, null, r_C04_w.nr_sequencia, 
									null, nr_seq_ocor_combinada_p, nm_usuario_p, 
									null, nr_seq_motivo_glosa_p, 10, 
									cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
									nr_seq_oc_benef_w, null,
									null, null, null);
							
						pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
										nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
										null, null, null,
										null, r_C04_w.nr_sequencia, null,
										nm_usuario_p, cd_estabelecimento_p);
					end if;
				end if;
				end;
			end loop;
		end if;
	end;
end loop;

commit;

end pls_valida_ocor_aut_concor;
/
