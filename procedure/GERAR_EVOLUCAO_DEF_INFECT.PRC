create or replace
procedure gerar_evolucao_def_infect ( 	nr_atendimento_p 	number,
					nr_seq_def_infect_p 	number,
					cd_profissional_p 	varchar2,
				        ie_tipo_evolucao_p 	varchar2,
				        nm_usuario_p  		Varchar2) is

ds_evolucao_w  			varchar2(4000);
cd_pessoa_fisica_w 	 	varchar2(10);
ie_liberar_evolucao_aut_w 	varchar2(1);
ie_evolucao_clinica_w		varchar2(20);

ds_definicao_w 			varchar2(255);
ds_medicamento_w 		varchar2(255);
ds_material_w  			varchar2(255);
ds_via_aplicacao_w 		varchar2(255);
qt_dose_w  			varchar2(255);
ds_intervalo_w  		varchar2(255);
qt_dias_lib_w  			varchar2(255);
ds_unid_medida_w 		varchar2(255);
ds_horarios_subst_w 		varchar2(255);
ds_observacao_w  		varchar2(2000);

begin
ie_liberar_evolucao_aut_w := Obter_Valor_Param_Usuario(7043, 20, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
ie_evolucao_clinica_w     := Obter_Valor_Param_Usuario(7043, 21, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

select	obter_pessoa_atendimento(nr_atendimento_p,'C')
into 	cd_pessoa_fisica_w
from 	dual;

select 	substr(obter_valor_dominio(2988,ie_definicao),1,255)    ds_definicao      ,
	substr(obter_desc_medic_cih(cd_medicamento),1,255)  	ds_medicamento    ,
	substr( obter_desc_material( cd_material ) ,1,255)  	ds_material       ,
	substr(obter_desc_via(ie_via_aplicacao),1,255)   	ds_via_aplicacao  ,
	substr(qt_dose,1,255)      				qt_dose   	  ,
	substr(obter_desc_intervalo_prescr(cd_intervalo),1,255) ds_intervalo      ,
	substr(qt_dia_lib,1,255)     				qt_dias_l  	  ,
	substr(obter_desc_unid_med(cd_unidade_medida),1,255)    ds_unid_medida    ,
	substr(ds_horarios,1,255)     				ds_horarios_subst ,
	substr(ds_observacao,1,2000)     			ds_observacao
into 	ds_definicao_w,
	ds_medicamento_w,
	ds_material_w,
	ds_via_aplicacao_w,
	qt_dose_w,
	ds_intervalo_w,
	qt_dias_lib_w,
	ds_unid_medida_w,
	ds_horarios_subst_w,
	ds_observacao_w
from 	cih_def_infect
where 	nr_sequencia = nr_seq_def_infect_p;


ds_evolucao_w := WHEB_RTF_PCK.GET_TEXTO_RTF(substr(
  wheb_mensagem_pck.get_texto(802174) || ': ' || ds_definicao_w      || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802177) || ': ' || ds_medicamento_w    || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802179) || ': ' || ds_material_w       || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802180) || ': ' || ds_via_aplicacao_w  || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802181) || ': ' || qt_dose_w          || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802182) || ': ' || ds_intervalo_w      || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802183) || ': ' || qt_dias_lib_w       || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802184) || ': ' || ds_unid_medida_w    || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802185) || ': ' || ds_horarios_subst_w || chr(10) || chr(13) ||' \par '||
  wheb_mensagem_pck.get_texto(802186) || ': ' || ds_observacao_w,1,4000));   	
				
insert into evolucao_paciente ( cd_evolucao   			,
				dt_evolucao   			,
				ie_tipo_evolucao  		,
				ie_evolucao_clinica 		,
				cd_pessoa_fisica  		,
				dt_atualizacao   		,
				nm_usuario   			,
				cd_medico   			,
				nr_atendimento   		,
				ds_evolucao			,
				ie_situacao			,
				dt_liberacao			)
		values    ( 	evolucao_paciente_seq.nextval 	,	
				sysdate    			,
				ie_tipo_evolucao_p  		,
				ie_evolucao_clinica_w		,
				cd_pessoa_fisica_w  		,
				sysdate    			,
				nm_usuario_p   			,
				cd_profissional_p  		,
				nr_atendimento_p  		,
				ds_evolucao_w			,
				'A'				,
				decode(ie_liberar_evolucao_aut_w,'S',sysdate,null));

commit;

end gerar_evolucao_def_infect;
/
