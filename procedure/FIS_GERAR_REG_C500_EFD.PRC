create or replace
procedure fis_gerar_reg_C500_efd(	nr_seq_controle_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				ds_separador_p		varchar2,
				qt_linha_p	in out	number,
				nr_sequencia_p	in out	number) is 

cd_modelo_w			varchar2(2);
nr_seq_regra_efd_w		number(10);
nr_versao_efd_w			varchar2(5);
tp_registro_w			varchar2(4);
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1)	:= ds_separador_p;
nr_sequencia_w			number(10);
dt_fim_w			date;
ie_tipo_titulo_w		varchar2(2);
qt_registros_w			number(10);



cursor c01 is
select	'C500' tp_registro, 
	substr(decode(Obter_se_nota_entrada_saida(a.nr_sequencia),'E',0,'S',1),1,1) ie_operacao,
/*quando o tipo da nota �  "Entrada(Emiss�o pr�pria)" o ie_emissao_nf deve ser 0*/
	decode(Obter_Se_Nota_Entrada_Saida(a.nr_sequencia), 'S', 0, 'E', decode(ie_tipo_nota, 'EP', 0, 1)) ie_emissao_nf,
/*se for nota de entrada pega o emitente (pessoa f�sica ou jur�dica), se for de sa�da pega o cd_cgc */
	decode(Obter_Se_Nota_Entrada_Saida(a.nr_sequencia), 'E', nvl(a.cd_cgc_emitente, a.cd_pessoa_fisica), cd_cgc) cd_participante,
/* nvl(cd_pessoa_fisica, cd_cgc_emitente) cd_participante, */
	 lpad(m.cd_modelo_nf, 2, 0) cd_modelo,
/* situa��o: 01 Documento regular extempor�neo , 03 Documento cancelado extempor�neo, 07 Documento Fiscal Complementar extempor�neo. */
	substr(decode(a.ie_situacao, 1, '00', 2, '02', 3, '02', 9, '02', '07'), 1, 2) cd_situacao,
	substr(trim(a.cd_serie_nf), 1, 3) cd_serie_nf,
	'' sub_serie,
	'01' cod_cons,
	substr(nvl(a.nr_nfe_imp, a.nr_nota_fiscal), 1, 9) nr_nota_fiscal,
	substr(to_char(a.dt_emissao, 'ddmmyyyy'),1,8) dt_emissao,
	substr(to_char(a.dt_entrada_saida, 'ddmmyyyy'),1,8) dt_entrada_saida,
	a.vl_total_nota vl_total_nota,
	(obter_dados_nota_fiscal(a.nr_sequencia, '37') + a.vl_descontos) vl_descontos, 
	a.vl_total_nota vl_forn,
	0 vl_serv_nt, 
	0 vl_terc,
	0 vl_da,
	nfe_obter_valores_totais_num(a.nr_sequencia, 'ICMS', 'BC')  vl_base_calc_icms,
	nfe_obter_valores_totais_num(a.nr_sequencia, 'ICMS', 'VL')  vl_icms,
	nfe_obter_valores_totais_num(a.nr_sequencia, 'ICMSST', 'BC')  vl_base_calc_icms_st,
	nfe_obter_valores_totais_num(a.nr_sequencia, 'ICMSST', 'VL')  vl_icms_retido_st,
	'' cod_inf,
	nfe_obter_valores_totais_num(a.nr_sequencia, 'PIS', 'VL')  vl_pis,
	nfe_obter_valores_totais_num(a.nr_sequencia, 'COFINS', 'VL')  vl_cofins,
	1 tp_ligacao,
	12 cod_grupo_tensao,
	a.nr_sequencia
from	nota_fiscal a,
	modelo_nota_fiscal m
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	m.nr_sequencia		= a.nr_seq_modelo (+)
  -- nota  fiscal  de  servi�o  de  transporte  (c�digo  07)  e  conhecimentos de transporte rodovi�rio de cargas  (c�digo  08),  conhecimentos  de  transporte  de  cargas  avulso  (c�digo  8b),  aquavi�rio de cargas  (c�digo  09), a�reo  (c�digo  10),  ferrovi�rio  de  cargas  (c�digo  11)  e multimodal  de  cargas  (c�digo  26),  
  --nota  fiscal  de  transporte  ferrovi�rio  de  carga  (  c�digo  27)  e conhecimento de transporte eletr�nico � ct-e (c�digo 57)
and	m.nr_sequencia in (	select nr_seq_modelo_nf
				from	fis_regra_efd_C500
				where 	nr_seq_regra_efd = nr_seq_regra_efd_w)   --and	m.cd_modelo_nf in ('06', '28', '29') 
--and	a.cd_operacao_nf in (30, 44, 4, 31, 25, 35, 23, 1, 2, 29, 42, 41, 21, 28)
and	(((Obter_se_nota_entrada_saida(a.nr_sequencia)= 'E') and (a.dt_entrada_saida between dt_inicio_p and dt_fim_w) ) or --nota de entrada
 	((Obter_se_nota_entrada_saida(a.nr_sequencia)= 'S') and (a.dt_emissao between dt_inicio_p and dt_fim_w))) --nota de saida
and	a.dt_atualizacao_estoque is not null
and 	a.ie_situacao = 1
order by	2;

vet01	C01%RowType;

begin
dt_fim_w := fim_dia(dt_fim_p); --coloca a hora junto ex: 01/01/2013 23:59:59

select nr_seq_regra_efd
into nr_seq_regra_efd_w
from fis_efd_controle
where nr_sequencia = nr_seq_controle_p;

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	nr_sequencia_w	:= vet01.nr_sequencia;
	cd_modelo_w	:= vet01.cd_modelo;
	
	if (vet01.cd_situacao = '00') then
		ds_linha_w	:= substr(	sep_w || vet01.tp_registro	 						|| 
						sep_w || vet01.ie_operacao						||
						sep_w || vet01.ie_emissao_nf						||
						sep_w || vet01.cd_participante	 				||
						sep_w || vet01.cd_modelo						||
						sep_w || vet01.cd_situacao						||
						sep_w || vet01.cd_serie_nf 						||
						sep_w || vet01.sub_serie 						||						
						sep_w || vet01.cod_cons							||
						sep_w || vet01.nr_nota_fiscal	 					||
						sep_w || vet01.dt_emissao						||
						sep_w || vet01.dt_entrada_saida					||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_total_nota), 1, 30)		||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_descontos), 1, 30) 		||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_forn), 1, 30)		||
						sep_w || vet01.vl_serv_nt					||
						sep_w || vet01.vl_terc					||
						sep_w || vet01.vl_da						||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_base_calc_icms), 1, 30)	||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_icms), 1, 30)		||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_base_calc_icms_st), 1, 30)	||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_icms_retido_st), 1, 30)	||
						sep_w || vet01.cod_inf					||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_pis), 1, 30)		||
						sep_w || substr(sped_obter_campo_valor(vet01.vl_cofins), 1, 30)		||
						sep_w || vet01.tp_ligacao					|| 
						sep_w || vet01.cod_grupo_tensao					|| 
						sep_w,1,8000);
	else --informar apenas os campos: reg, ind_oper, ind_emit, cod_mod, cod_sit, ser, num_doc e dt_doc
		ds_linha_w	:= substr(	sep_w || vet01.tp_registro	 						|| 
						sep_w || vet01.ie_operacao						||
						sep_w || vet01.ie_emissao_nf						||
						sep_w || ''	 				||
						sep_w || vet01.cd_modelo						||
						sep_w || vet01.cd_situacao						||
						sep_w || vet01.cd_serie_nf 						||
						sep_w || '' 						||						
						sep_w || ''							||
						sep_w || vet01.nr_nota_fiscal	 					||
						sep_w || vet01.dt_emissao						||
						sep_w || ''					||
						sep_w || ''		||
						sep_w || ''		||
						sep_w || ''	||
						sep_w || ''					||
						sep_w || ''					||
						sep_w || ''						||
						sep_w || ''	||
						sep_w || ''		||
						sep_w || ''	||
						sep_w || ''	||
						sep_w || ''					||
						sep_w || ''		||
						sep_w || ''		||
						sep_w || ''					|| 
						sep_w || ''				|| 
						sep_w,1,8000);	
	end if;
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_efd_arquivo(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_efd,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values(	nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		vet01.tp_registro,
		ds_arquivo_w,
		ds_arquivo_compl_w);
		

	if (vet01.cd_situacao = '00') then
		fis_gerar_reg_C590_efd(	nr_seq_controle_p,
					nm_usuario_p,
					cd_estabelecimento_p,
					dt_inicio_p,
					dt_fim_p,
					cd_empresa_p,
					ds_separador_p,
					nr_sequencia_w,
					nr_linha_w,
					nr_seq_registro_w);
	end if;			
	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;	

end fis_gerar_reg_C500_efd;
/