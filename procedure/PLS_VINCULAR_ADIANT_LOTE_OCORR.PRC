create or replace
procedure pls_vincular_adiant_lote_ocorr(	ds_lista_adiant_p	varchar2,
						nr_seq_evento_p		pls_evento.nr_sequencia%type,
						nr_seq_lote_p		pls_lote_evento.nr_sequencia%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
						nm_usuario_p		usuario.nm_usuario%type,
						nr_seq_lote_pagamento_p	pls_lote_pagamento.nr_sequencia%type) is 

cd_conta_contabil_w		pls_evento_movimento.cd_conta_contabil%type;
cd_cgc_w			adiantamento_pago.cd_cgc%type;
cd_pessoa_fisica_w		adiantamento_pago.cd_pessoa_fisica%type;
vl_saldo_w			adiantamento_pago.vl_saldo%type;
nr_adiantamento_w		adiantamento_pago.nr_adiantamento%type;
nr_seq_evento_movto_w		pls_evento_movimento.nr_sequencia%type;
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
qt_adiantamentos_w		pls_integer;
nr_seq_pag_item_w		pls_pagamento_item.nr_sequencia%type;
nr_seq_pagamento_prest_w	pls_pagamento_prestador.nr_sequencia%type;
dt_adiantamento_w		date;
nr_seq_lote_evento_w		pls_lote_evento.nr_sequencia%type;
dt_mes_competencia_w		pls_lote_pagamento.dt_mes_competencia%type;
ie_lote_novo_w			varchar2(5) := 'N';

cursor c01 is
	select	a.nr_adiantamento,
		a.cd_pessoa_fisica,
		a.cd_cgc,
		a.vl_saldo,
		a.dt_adiantamento
	from	adiantamento_pago a
	where	obter_se_contido(a.nr_adiantamento,ds_lista_adiant_p) = 'S';
	
begin

open C01;
loop
fetch C01 into	
	nr_adiantamento_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	vl_saldo_w,
	dt_adiantamento_w;
exit when C01%notfound;

	-- entra aqui s� no primeiro registro que retornar do cursor
	if	(nr_seq_lote_evento_w is null) then

		-- se n�o foi passado um lote de evento insere um
		if	(nr_seq_lote_p is null) then
			
			-- Identificar que foi gerado um novo lote de evento movimento
			ie_lote_novo_w := 'S';
			
			select	max(a.dt_mes_competencia)
			into	dt_mes_competencia_w
			from	pls_lote_pagamento a
			where	a.nr_sequencia = nr_seq_lote_pagamento_p;

			insert into pls_lote_evento(
				cd_estabelecimento, dt_atualizacao, dt_atualizacao_nrec, 
				dt_competencia, ie_origem, nm_usuario, 
				nm_usuario_nrec, nr_seq_lote_pagamento, nr_sequencia
			) values (
				cd_estabelecimento_p, sysdate, sysdate,
				dt_mes_competencia_w, 'A', nm_usuario_p,
				nm_usuario_p, nr_seq_lote_pagamento_p, pls_lote_evento_seq.nextval
			) returning nr_sequencia into nr_seq_lote_evento_w;
		else
			nr_seq_lote_evento_w := nr_seq_lote_p;
		end if;
	end if;

	select	sum(qt)
	into	qt_adiantamentos_w
	from	(
		select	count(1) qt
		from	pls_evento_movimento
		where	nr_adiant_pago = nr_adiantamento_w
		and	nr_seq_lote = nr_seq_lote_evento_w
		and	nr_seq_lote_pgto = nr_seq_lote_pagamento_p
		and	ie_cancelamento is null
		union all
		select	count(1) qt
		from	pls_evento_movimento
		where	nr_seq_lote_pagamento_p is null
		and	nr_adiant_pago = nr_adiantamento_w
		and	nr_seq_lote = nr_seq_lote_evento_w
		and	ie_cancelamento is null
	);

	if	(qt_adiantamentos_w > 0) then
		goto final;
	end if;
	
	if	(nr_seq_lote_pagamento_p is not null) then
		if	(cd_pessoa_fisica_w is not null) then

			select	max(b.nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador b,
				pls_pagamento_prestador a
			where	a.nr_seq_prestador = b.nr_sequencia
			and	b.cd_pessoa_fisica = cd_pessoa_fisica_w
			and	a.nr_seq_lote = nr_seq_lote_pagamento_p;

		elsif	(cd_cgc_w is not null) then

			select	max(b.nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador b,
				pls_pagamento_prestador a
			where	a.nr_seq_prestador = b.nr_sequencia
			and	b.cd_cgc = cd_cgc_w
			and	a.nr_seq_lote = nr_seq_lote_pagamento_p;
		else
			nr_seq_prestador_w := null;
		end if;
		
		if	(nr_seq_prestador_w is null) then
			goto final;
		end if;
	else
		if	(cd_pessoa_fisica_w is not null) then

			select	max(nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador
			where	cd_pessoa_fisica = cd_pessoa_fisica_w;

		elsif	(cd_cgc_w is not null) then

			select	max(nr_sequencia)
			into	nr_seq_prestador_w
			from	pls_prestador
			where	cd_cgc = cd_cgc_w;
		else
			nr_seq_prestador_w := null;
		end if;
		
		if	(nr_seq_prestador_w is null) then
			goto final;
		end if;
	end if;	

	insert into pls_evento_movimento(
		nr_sequencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, dt_movimento,
		nr_seq_evento, nr_seq_lote, nr_seq_prestador,
		vl_movimento, ie_forma_pagto, nr_tit_rec_vinculado,
		nr_tit_pagar_vinculado, nr_adiant_pago
	) values (
		pls_evento_movimento_seq.nextval, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, trunc(dt_adiantamento_w,'dd'),
		nr_seq_evento_p, nr_seq_lote_evento_w, nr_seq_prestador_w,
		vl_saldo_w, 'P', null,
		null, nr_adiantamento_w
	) returning nr_sequencia into nr_seq_evento_movto_w;
		
	pls_obter_conta_contab_eve_fin(	nr_seq_evento_movto_w, cd_conta_contabil_w);

	update	pls_evento_movimento
	set	cd_conta_contabil = cd_conta_contabil_w
	where	nr_sequencia = nr_seq_evento_movto_w;
	
	insert	into pls_evento_movimento_log(
		nr_sequencia, nr_titulo_pagar, nr_titulo_receber,
		nr_adiant_pago, nr_seq_lote, nm_usuario,
		dt_atualizacao, nm_usuario_nrec, dt_atualizacao_nrec
	) values (
		pls_evento_movimento_log_seq.nextval, null,  null,
		nr_adiantamento_w, nr_seq_lote_evento_w, nm_usuario_p,
		sysdate, nm_usuario_p, sysdate
	);

	if	(nr_seq_lote_pagamento_p is not null) then

		select	max(a.nr_sequencia)
		into	nr_seq_pagamento_prest_w
		from	pls_pagamento_prestador a
		where	a.nr_seq_prestador = nr_seq_prestador_w
		and	a.nr_seq_lote = nr_seq_lote_pagamento_p;

		insert into pls_pagamento_item(
			nr_sequencia, nm_usuario, dt_atualizacao,
			nm_usuario_nrec, dt_atualizacao_nrec, nr_seq_evento,
			nr_seq_pagamento, vl_item, nr_tit_receber_origem,
			nr_tit_pagar_origem, nr_adiant_pago_origem
		) values (
			pls_pagamento_item_seq.nextval, nm_usuario_p, sysdate,
			nm_usuario_p, sysdate, nr_seq_evento_p,
			nr_seq_pagamento_prest_w, vl_saldo_w*-1, null,
			null, nr_adiantamento_w
		) returning nr_sequencia into nr_seq_pag_item_w;

		pls_atualizar_valor_lote_pag(	nr_seq_pagamento_prest_w, nm_usuario_p);

		insert into pls_pagamento_item_log(
			nr_sequencia, nr_titulo_pagar, nr_titulo_receber,
			nr_adiant_pago, nr_seq_pag_prest, nr_seq_lote_pagamento,
			ie_tipo, nm_usuario, dt_atualizacao,
			nm_usuario_nrec, dt_atualizacao_nrec
		) values (
			pls_pagamento_item_log_seq.nextval, null, null,
			nr_adiantamento_w, nr_seq_pagamento_prest_w, nr_seq_lote_pagamento_p,
			'I', nm_usuario_p, sysdate,
			nm_usuario_p, sysdate
		);
	end if;

	<<final>>
	null;
end loop;
close C01;

if	(ie_lote_novo_w = 'S') then
	pls_liberar_lote_evento( nr_seq_lote_evento_w, 'L', nm_usuario_p, null, 'N');
end if;

commit;

end pls_vincular_adiant_lote_ocorr;
/