create or replace
procedure gerar_atendimento_rn(	nr_atendimento_p	in 	number,
				nm_usuario_p		in	varchar2,
				nr_seq_nascimento_p	in	number,
				cd_setor_atendimento_p	in	number,
				cd_acomodacao_p		in	number,
				cd_unidade_basica_p	in	varchar2,
				cd_unidade_compl_p	in	varchar2,
				ie_html_p		in	varchar2 default null,
				nr_atend_rn_p		out	number) is

cd_pessoa_fisica_w		varchar2(10);
cd_pessoa_rn_w			varchar2(10);
cd_pf_rn_w			varchar2(10);
nr_atend_rn_w			number(10,0);
nr_seq_interno_w		number(10,0);
ie_gerar_ci_w			varchar2(01)	:= 'N';
cd_setor_usuario_w		number(10,0);
cd_estabelecimento_w		number(10,0);
nr_sequencia_w			number(10,0);
ie_sexo_w			varchar2(01);
is_name_feature_w		varchar2(1);
nr_seq_nasc_w			number(10,0);
dt_nascimento_rn_w		date;
ie_sexo_rn_w			varchar2(1);
nr_cor_pele_rn_w		number(10,0);
qt_altura_rn_w			number(4,0);
qt_peso_nasc_w			number(4,0);
qt_peso_sala_parto_w NUMBER;
qt_peso_nasc_pf_w    NUMBER;
qt_peso_pf_w			number(6,3);
nr_seq_unid_rn_w		number(10,0);
ie_setor_w			number(10,0);
cd_setor_passagem_w		number(15,0);
cd_setor_rn_w			number(5,0);
cd_unidade_bas_rn_w		varchar2(10);
cd_unidade_comp_rn_w		varchar2(10);
cd_acomod_rn_w			number(4,0);
qt_max_acomp_rn_w		number(3,0);
cd_nacionalidade_w		varchar2(8);
qt_atendimentos_mae_w		number(3,0);
qt_atendimentos_mae_ww		number(3,0);
ds_rn_w				varchar2(80);
qt_nasc_vivos_w			number(2,0);
cd_perfil_w			number(5);
nm_usuario_original_w		varchar2(15);
ie_medico_rn_w			varchar2(2);
cd_medico_resp_w		varchar2(10);
ie_compl_mae_w			varchar2(1);
nr_seq_compl_w			number(3);
nr_seq_perfil_pep_w		varchar2(10);
cid_rn_w			varchar2(10);
ie_clinica_rn_w			number(5);
dt_final_vigencia_w		date;
ie_tipo_atend_rn_w		number(5);
nr_seq_classif_rn_w		number(3);
ie_gera_lancto_auto_rn_w	varchar2(1);
cd_setor_paciente_w		number(10);
qt_idade_w			number(10);
nr_seq_evento_w			number(10);
dt_liberacao_w			date;
nr_seq_atepacu_w		number(10);
cd_setor_atendimento_w		number(10);
cd_unidade_basica_w		varchar2(20);
cd_unidade_compl_w		varchar2(20);
cd_convenio_w			number(10);
ie_trat_conta_rn_w		varchar2(10);
ie_gerar_passagem_setor_w	varchar2(1);
ie_classif_doenca_rn_w		varchar2(1);
ie_pront_pf_w				varchar2(1);
nr_prontuario_w				number(10);
qt_reg_w					number(10);
ie_numeracao_rn_w			varchar(1);
cd_municipio_ibge_w			varchar(10);
nr_ddd_celular_w			varchar(10);
nr_telefone_celular_w		varchar(40);
NR_SEQ_CLASSIF_W		number(10);
IE_DATA_NASCIMENTO_w	varchar(40);
ie_recen_nascido_w		varchar2(2);	
cd_procedencia_rn_w		number(5);
nr_seq_person_name_w	number(10);
nr_seq_person_name_ww	pessoa_fisica.nr_seq_person_name%type;
ds_given_name_w			person_name.ds_given_name%type;
ds_name_surename_w		varchar2(256);
ie_import_pagador_w	varchar2(1);
ie_rn_vigencia_null_w	varchar2(1);
IE_STATUS_CPF_W VARCHAR2(1);
nr_seq_justificativa_taxa_rn_w	pessoa_taxa_justificativa.nr_sequencia%type;
pessoa_fisica_taxa_w		pessoa_fisica_taxa%rowtype;

Cursor C01 is
	select	nr_seq_evento
	from 	regra_envio_sms
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_evento_disp 	=	'GARN'
	and	qt_idade_w between nvl(qt_idade_min,-1)	and nvl(qt_idade_max,9999)
	and	((nvl(nr_seq_classif,nr_seq_classif_w) = nr_seq_classif_w) or (obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = 'S'))	
	and	nvl(cd_setor_atendimento,cd_setor_paciente_w) = cd_setor_paciente_w
	and	nvl(ie_situacao,'A') = 'A';
	
Cursor C02 is
	select	cd_pessoa_fisica, 
			cd_cgc, 
			nr_seq_grau_parentesco, 
			ds_acordo, 
			ds_condicao
	from	ATENDIMENTO_PAGADOR
	where	nr_atendimento = nr_atendimento_p
	order by nr_sequencia;

begin

select	nvl(max(nr_atend_rn),0)
into	nr_atend_rn_w
from	nascimento
where	nr_sequencia	= nr_seq_nascimento_p
and	nr_atendimento	= nr_atendimento_p;

select 	nvl(max(ie_numeracao_rn),'R'),
		max(IE_DATA_NASCIMENTO)
into	ie_numeracao_rn_w,
		IE_DATA_NASCIMENTO_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nr_atend_rn_w = 0) then
	begin
	obter_param_usuario(281, 377, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_medico_rn_w);
	obter_param_usuario(5, 67, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, nr_seq_perfil_pep_w);
	obter_param_usuario(3111, 98, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, cd_setor_passagem_w);
	obter_param_usuario(281, 1020, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gerar_passagem_setor_w);
	obter_param_usuario(5, 138, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_pront_pf_w);
	obter_param_usuario(916, 1196, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_import_pagador_w);
	obter_param_usuario(916, 1204, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_rn_vigencia_null_w);

	/* verifica se deve enviar comunicacao interna */
	select	nvl(max(obter_valor_param_usuario(916, 122, obter_perfil_ativo, nm_usuario_p, 0)), 'N'),
		nvl(max(obter_valor_param_usuario(916, 505, obter_perfil_ativo, nm_usuario_p, 0)), 'N')
	into	ie_gerar_ci_w,
		ie_gera_lancto_auto_rn_w
	from 	dual;

	/* pega o setor do usuario */
	select	nvl(max(cd_setor_atendimento),0)
	into	cd_setor_usuario_w
	from 	usuario
	where	nm_usuario	= nm_usuario_p;

	/* verifica dados da mae */
	select	nvl(max(a.cd_pessoa_fisica), '0'),
			max(b.ie_sexo),
			max(Obter_se_recem_nasc(a.nr_atendimento)),
			max(nr_seq_person_name),
      max(b.cd_estabelecimento)
	into	cd_pessoa_fisica_w,
			ie_sexo_w,
			ie_recen_nascido_w,
			nr_seq_person_name_ww,
      cd_estabelecimento_w
	from	pessoa_fisica b,
			atendimento_paciente a
	where	nr_atendimento		= nr_atendimento_p
	and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;
	
	if	((nr_seq_person_name_ww is not null) and (nvl((pkg_i18n.get_user_locale), 'pt_BR') = 'es_MX')) then
	
		select  ds_given_name,
				DS_FAMILY_NAME||' '||DS_COMPONENT_NAME_1
		into	ds_given_name_w,
				ds_name_surename_w
		from    person_name
		where   nr_sequencia = nr_seq_person_name_ww
		and     ds_type = 'main';
		
	end if;	
	
	/* pega a nacionalidade brasileira para os recem-nascidos */
	select	max(cd_nacionalidade)
	into	cd_nacionalidade_w
	from 	nacionalidade
	where	ie_brasileiro = 'S'
	and	nvl(ie_situacao,'A') = 'A';

	if	(ie_sexo_w <> 'F') then
		wheb_mensagem_pck.exibir_mensagem_abort(Wheb_mensagem_pck.get_texto(308476)/*'O paciente nao e do sexo feminino! Por favor verifique o cadastro.*/);
	end if;

	/* verifica sequencia do nascimento */
	if	(nr_seq_nascimento_p = 0) then
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_nasc_w
		from	nascimento
		where	nr_atendimento		= nr_atendimento_p
		and	nr_atend_rn is null;
	else
		nr_seq_nasc_w	:= nr_seq_nascimento_p;
	end if;

	/* nao deixa inserir se nao tiver registro de nascimento */
	if	(nr_seq_nasc_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(Wheb_mensagem_pck.get_texto(308477)/*'Este atendimento nao possui registro de nascimento informado no item Nascimentos da funcao Prontuario Eletronico de Paciente.*/);
	end if;
	
	/* Verificar se esta sendo gerado um atendiment de recem nascido para um paciente co m  menos de 28 dias  */	
	
	if	(ie_recen_nascido_w = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(Wheb_mensagem_pck.get_texto(445264)/*'O paciente possui menos de 28 dias de nascimento. Verifique o cadastro de pessoa fisica.*/);
	end if;
	

	if 	(nvl(cd_setor_atendimento_p,0) = 0) or
		(nvl(cd_acomodacao_p,-1) = -1) or
		(cd_unidade_basica_p = '') then

		
		
		nr_seq_atepacu_w	:= Obter_Atepacu_paciente(nr_atendimento_p, 'A');
		nr_seq_unid_rn_w	:= 0;
		select	max(cd_setor_atendimento),
			max(cd_unidade_basica),
			max(cd_unidade_compl)
		into	cd_setor_atendimento_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w
		from	atend_paciente_unidade
		where	NR_SEQ_INTERNO	= nr_seq_atepacu_w;
		
		
		if	(cd_unidade_basica_w	is not null) and
			(cd_unidade_compl_w is not null) then
			begin
			select	nvl(nr_seq_unidade_rn, 0)
			into	nr_seq_unid_rn_w
			from	unidade_atendimento
			where	cd_setor_atendimento	= cd_setor_atendimento_w
			and	cd_unidade_basica	= cd_unidade_basica_w
			and	cd_unidade_compl	= cd_unidade_compl_w;
			exception
				when others then
				nr_seq_unid_rn_w	:= 0;
			end;
		end if;
		
		if	(nr_seq_unid_rn_w	= 0) then
			begin
			select	nvl(nr_seq_unidade_rn, 0)
			into	nr_seq_unid_rn_w
			from	setor_atendimento
			where	cd_setor_atendimento	= cd_setor_atendimento_w;
			exception
				when others then
				nr_seq_unid_rn_w	:= 0;
			end;
		end if;
		if	(nr_seq_unid_rn_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort( Wheb_mensagem_pck.get_texto(308478)/*'O setor do paciente nao tem unidade para recem-nascidos informada.*/);
		end if;

	end if;

	if	(cd_pessoa_fisica_w <> '0') then
		begin

		/* le dados do recem nascido para complementar o cadastro */
		select	dt_nascimento,
			ie_sexo,
			nr_cor_pele,
			qt_altura,
			nvl(cd_pessoa_rn,'0'),
			qt_peso,
         qt_peso_sala_parto
		into	dt_nascimento_rn_w,
			ie_sexo_rn_w,
			nr_cor_pele_rn_w,
			qt_altura_rn_w,
			cd_pf_rn_w,
			qt_peso_nasc_w,
         qt_peso_sala_parto_w
		from	nascimento
		where	nr_atendimento	= nr_atendimento_p
		and	nr_sequencia	= nr_seq_nasc_w;
		
		if	(nvl(qt_peso_nasc_w,0) > 0) then
			qt_peso_pf_w	:= (qt_peso_nasc_w / 1000);
		end if;
      IF (nvl(qt_peso_sala_parto_w, 0) > 0) THEN
         qt_peso_nasc_pf_w := (qt_peso_sala_parto_w / 1000);
      END IF;		
		if	(ie_medico_rn_w = 'S') then
			select	max(cd_pediatra_rn)
			into	cd_medico_resp_w
			from 	nascimento
			where	nr_atendimento = nr_atendimento_p
			and	nr_sequencia	= nr_seq_nasc_w;
		else
			select	max(cd_medico_resp)
			into	cd_medico_resp_w
			from 	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;

		end if;

		if	(cd_medico_resp_w	is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(Wheb_mensagem_pck.get_texto(308479) /*'O medico responsavel pelo atendimento nao foi cadastrado.*/);
		end if;

		if	(cd_pf_rn_w = '0') then
			select	pessoa_fisica_seq.nextval
			into	cd_pessoa_rn_w
			from	dual;

			select	nvl(max(qt_nasc_vivos),0)
			into	qt_nasc_vivos_w
			from	parto
			where	nr_atendimento = nr_atendimento_p;

			select	count(*)
			into	qt_atendimentos_mae_w
			from	atendimento_paciente
			where	nr_atendimento_mae = nr_atendimento_p;
			
			select	count(*)
			into	qt_atendimentos_mae_ww
			from	nascimento
			where	nr_atendimento	= nr_atendimento_p;

			ds_rn_w := Wheb_mensagem_pck.get_texto(308831)|| ' '; --'RN de ';
			if	(qt_nasc_vivos_w > 1) or (qt_atendimentos_mae_ww > 1)then
					if	(ie_numeracao_rn_w = 'R') then
						ds_rn_w := Wheb_mensagem_pck.get_texto(308832) || ' ' /*'RN '*/ || converte_numero_romano(nr_seq_nascimento_p) || ' ' || Wheb_mensagem_pck.get_texto(308483) || ' '; --' de ';
					else
						ds_rn_w := Wheb_mensagem_pck.get_texto(308832) || ' ' /*'RN '*/ || nr_seq_nascimento_p || ' ' || Wheb_mensagem_pck.get_texto(308483) || ' '; --' de ';
					end if;
			end if;
			
			nr_prontuario_w	:= null;
			if	(ie_pront_pf_w	= 'S') then
				select	count(*)
				into	qt_reg_w
				from	pessoa_fisica
				where	nr_prontuario	= cd_pessoa_rn_w;
				
				if	(qt_reg_w	= 0) then
					nr_prontuario_w	:= cd_pessoa_rn_w;
				end if;
				
				
			end if;
			
			
			select	max(b.cd_municipio_ibge)
			into	cd_municipio_ibge_w
			from	estabelecimento a,
					pessoa_juridica b
			where	a.CD_CGC = b.CD_CGC
			and		a.cd_estabelecimento = obter_estabelecimento_ativo;
		
		
		is_name_feature_w := obter_valor_param_Usuario(0, 213, null, null, null);
		
		if	(is_name_feature_w <> 'N') then
			begin
			INSERT INTO person_name (
					 nr_sequencia
					,dt_atualizacao
					,nm_usuario
					,dt_atualizacao_nrec
					,nm_usuario_nrec
					,ds_type
					,ds_given_name
					,ds_family_name
					,ds_component_name_1
					,ds_component_name_2
					,ds_component_name_3
					)
				VALUES (
					person_name_seq.nextval
					,sysdate
					,nm_usuario_p
					,sysdate
					,nm_usuario_p
					,'main'
					,case when (ds_given_name_w is not null) then substr(ds_rn_w || ds_given_name_w,1,60) else substr(ds_rn_w || (select nm_pessoa_fisica from pessoa_fisica where cd_pessoa_fisica	= cd_pessoa_fisica_w),1,60) end
					--,substr(ds_rn_w || (select nm_pessoa_fisica from pessoa_fisica where cd_pessoa_fisica	= cd_pessoa_fisica_w),1,60)
					,case when (ds_name_surename_w is not null) then ds_name_surename_w else '' end
					,''
					,''
					,''
					) returning nr_sequencia
			INTO nr_seq_person_name_w;
			end;
		end if;
      	     
			insert	into pessoa_fisica
				(cd_pessoa_fisica,
				ie_tipo_pessoa,
				nm_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				dt_nascimento,
				qt_altura_cm,
				qt_peso,
        qt_peso_nasc,
				nr_seq_cor_pele,
				ie_sexo,
				cd_nacionalidade,
				nr_seq_perfil,
				ie_funcionario,
				nr_prontuario,
				ie_estado_civil,
				IE_GRAU_INSTRUCAO,
				CD_MUNICIPIO_IBGE,
				nr_ddd_celular,
				nr_telefone_celular,
				CD_PESSOA_MAE,
				cd_religiao,
        NR_SEQ_PERSON_NAME,
        cd_estabelecimento)
			(select	cd_pessoa_rn_w,
				ie_tipo_pessoa,
				substr(ds_rn_w || nm_pessoa_fisica,1,60),
				sysdate,
				nm_usuario_p,
				dt_nascimento_rn_w,
				qt_altura_rn_w,
				qt_peso_pf_w,
        qt_peso_nasc_pf_w,
				nr_cor_pele_rn_w,
				ie_sexo_rn_w,
				cd_nacionalidade_w,
				nr_seq_perfil_pep_w,
				'N',
				nr_prontuario_w,
				1,
				11,
				cd_municipio_ibge_w,
				nr_ddd_celular,
				nr_telefone_celular,
				cd_pessoa_fisica,
				cd_religiao,
        nr_seq_person_name_w,
        cd_estabelecimento_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w);
    
			commit; /*incluido o commit por conta da integracao da agfa no samaritano. pedido elemar.*/

				select	nvl(max(nr_sequencia),0) + 1
				into	nr_sequencia_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_rn_w;
				
				
			/* cria o complemento dela */
			insert into compl_pessoa_fisica (
				cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				nm_usuario,
				dt_atualizacao,
				nm_contato,
				cd_cep,
				ds_endereco,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				cd_municipio_ibge,
				sg_estado,
				nr_seq_pais,
				nr_telefone,
				nr_ramal,
				ds_fax,
				ds_fone_adic,
				ds_observacao,
				nr_ddd_telefone,
				cd_tipo_logradouro,
				ds_compl_end
			) select
				cd_pessoa_rn_w,
				nr_sequencia_w,
				ie_tipo_complemento,
				nm_usuario_p,
				sysdate,
				nm_contato,
				cd_cep,
				ds_endereco,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				cd_municipio_ibge,
				sg_estado,
				nr_seq_pais,
				nr_telefone,
				nr_ramal,
				ds_fax,
				ds_fone_adic,
				ds_observacao,
				nr_ddd_telefone,
				cd_tipo_logradouro,
				ds_compl_end
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	ie_tipo_complemento	= 1
			and	rownum			= 1;
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_rn_w;		
			
			insert into compl_pessoa_fisica (
				cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				nm_usuario,
				dt_atualizacao,
				cd_cep,
				ds_endereco,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				cd_municipio_ibge,
				sg_estado,
				nr_telefone,
				nr_ramal,
				ds_fax,
				ds_fone_adic,
				ds_observacao,
				nm_contato,
				nr_ddd_telefone,
				cd_tipo_logradouro,
				nr_seq_pais
			) select
				cd_pessoa_rn_w,
				nr_sequencia_w,
				5,
				nm_usuario_p,
				sysdate,
				cd_cep,
				ds_endereco,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				cd_municipio_ibge,
				sg_estado,
				nr_telefone,
				nr_ramal,
				ds_fax,
				ds_fone_adic,
				ds_observacao,
				obter_nome_pf(cd_pessoa_fisica_w),
				nr_ddd_telefone,
				cd_tipo_logradouro,
				nr_seq_pais
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	ie_tipo_complemento	= 1
			and	rownum			= 1;
			
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_rn_w;	
			
			insert into compl_pessoa_fisica (
				cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				nm_usuario,
				dt_atualizacao,
				cd_cep,
				ds_endereco,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				cd_municipio_ibge,
				sg_estado,
				nr_telefone,
				nr_ramal,
				ds_fax,
				ds_fone_adic,
				ds_observacao,
				nm_contato,
				nr_ddd_telefone,
				cd_tipo_logradouro,
				nr_seq_pais
			) select
				cd_pessoa_rn_w,
				nr_sequencia_w,
				3,
				nm_usuario_p,
				sysdate,
				cd_cep,
				ds_endereco,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				cd_municipio_ibge,
				sg_estado,
				nr_telefone,
				nr_ramal,
				ds_fax,
				ds_fone_adic,
				ds_observacao,
				obter_nome_pf(cd_pessoa_fisica_w),
				nr_ddd_telefone,
				cd_tipo_logradouro,
				nr_seq_pais
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	ie_tipo_complemento	= 1
			and	rownum			= 1;	

			select nvl(max('S'),'N')
			into	ie_compl_mae_w
			from	compl_pessoa_fisica
			where	ie_tipo_complemento = 5
			and	cd_pessoa_fisica = cd_pessoa_rn_w;

			if (ie_compl_mae_w = 'N') then
				select	count(*)
				into	nr_seq_compl_w
				from	compl_pessoa_fisica
				where	cd_pessoa_fisica	= cd_pessoa_rn_w
				and		ie_tipo_complemento	= 1;

				if (nr_seq_compl_w = 0) then
					nr_seq_compl_w := 1;
				else
					select	max(nr_sequencia)
					into	nr_seq_compl_w
					from	compl_pessoa_fisica
					where	cd_pessoa_fisica	= cd_pessoa_rn_w
					and		ie_tipo_complemento	= 1;
				end if;

				insert into compl_pessoa_fisica (
							cd_pessoa_fisica,
							nr_sequencia,
							ie_tipo_complemento,
							nm_usuario,
							dt_atualizacao,
							nm_contato
						)  select	cd_pessoa_rn_w,
							nr_seq_compl_w,
							5,
							nm_usuario_p,
							sysdate,
							obter_nome_pf(cd_pessoa_fisica_w)
							from	dual;
			end if;
			
		else
			cd_pessoa_rn_w := cd_pf_rn_w;
		end if;
		
		ATUALIZA_MOTIVO_SEM_CPF_PF(cd_pessoa_rn_w, '1', NM_USUARIO_P); 
		
		select	atendimento_paciente_seq.nextval
		into	nr_atend_rn_w
		from	dual;

		select	max(cd_estabelecimento)
		into	cd_estabelecimento_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_p;


		select	max(cd_doenca_rn),
			max(ie_clinica_rn),
			max(ie_tipo_atend_rn),
			max(NR_SEQ_CLASSIF_RN),
			max(ie_classif_doenca_rn),
			max(cd_procedencia_rn)
		into	cid_rn_w,
			ie_clinica_rn_w,
			ie_tipo_atend_rn_w,
			nr_seq_classif_rn_w,
			ie_classif_doenca_rn_w,
			cd_procedencia_rn_w
		from	parametro_medico
		where	cd_estabelecimento	= cd_estabelecimento_w;
		
		cd_convenio_w	:= obter_convenio_atendimento(nr_atendimento_p);
		
		select	max(ie_trat_conta_rn)
		into	ie_trat_conta_rn_w
		from	setor_atendimento
		where	cd_setor_atendimento	= cd_setor_atendimento_p;
		
		if (ie_trat_conta_rn_w is null) then
			select	nvl(max(ie_trat_conta_rn),Wheb_mensagem_pck.get_texto(308480) /*'Mae'*/)
			into	ie_trat_conta_rn_w
			from	convenio_estabelecimento
			where	cd_convenio	= cd_convenio_w
			and	cd_estabelecimento = cd_estabelecimento_w;
		end if;
		
		if	(IE_DATA_NASCIMENTO_w	= 'S') then
			dt_nascimento_rn_w	:= sysdate;
		end if;
		
		commit; /*incluido o commit por conta da integracao da agfa no samaritano. pedido elemar.*/

		insert	into atendimento_paciente
			(nr_atendimento,
			cd_pessoa_fisica,
			cd_estabelecimento,
			cd_procedencia,
			dt_entrada,
			ie_tipo_atendimento,
			dt_atualizacao,
			nm_usuario,
			cd_medico_resp,
			cd_motivo_alta,
			ds_sintoma_paciente,
			ds_observacao,
			dt_alta,
			ie_clinica,
			nm_usuario_atend,
			ie_responsavel,
			dt_fim_conta,
			ie_fim_conta,
			nr_cat,
			ds_causa_externa,
			cd_cgc_seguradora,
			nr_bilhete,
			nr_serie_bilhete,
			ie_carater_inter_sus,
			ie_vinculo_sus,
			ie_tipo_convenio,
			ie_tipo_atend_bpa,
			ie_grupo_atend_bpa,
			cd_medico_atendimento,
			dt_alta_interno,
			nr_seq_unid_atual,
			nr_seq_unid_int,
			nr_atend_original,
			qt_dia_longa_perm,
			dt_inicio_atendimento,
			ie_permite_visita,
			ie_status_atendimento,
			dt_previsto_alta,
			nm_usuario_alta,
			cd_pessoa_responsavel,
			dt_atend_medico,
			dt_fim_consulta,
			dt_medicacao,
			dt_saida_real,
			ie_clinica_alta,
			dt_lib_medico,
			nr_seq_regra_funcao,
			nr_seq_local_pa,
			nr_seq_tipo_acidente,
			dt_ocorrencia,
			ds_pend_autorizacao,
			cd_motivo_alta_medica,
			nr_seq_forma_laudo,
			nr_seq_check_list,
			dt_fim_triagem,
			nr_reserva_leito,
			ie_paciente_isolado,
			ie_permite_visita_rel,
			ds_senha,
			ie_probabilidade_alta,
			nr_seq_forma_chegada,
			nr_seq_indicacao,
			cd_pessoa_indic,
			ds_obs_alta,
			nm_medico_externo,
			nr_gestante_pre_natal,
			dt_alta_medico,
			nr_atendimento_mae,
			ie_trat_conta_rn,
			nr_seq_classificacao)
		(select	nr_atend_rn_w,
			cd_pessoa_rn_w,
			cd_estabelecimento,
			nvl(cd_procedencia_rn_w,cd_procedencia),
			nvl(dt_nascimento_rn_w,dt_entrada),
			nvl(ie_tipo_atend_rn_w,ie_tipo_atendimento),
			sysdate,
			nm_usuario_p,
			cd_medico_resp_w,
			null,
			ds_sintoma_paciente,
			ds_observacao,
			null,
			nvl(ie_clinica_rn_w,ie_clinica),
			nm_usuario_atend,
			5,
			dt_fim_conta,
			ie_fim_conta,
			nr_cat,
			ds_causa_externa,
			cd_cgc_seguradora,
			nr_bilhete,
			nr_serie_bilhete,
			ie_carater_inter_sus,
			ie_vinculo_sus,
			ie_tipo_convenio,
			ie_tipo_atend_bpa,
			ie_grupo_atend_bpa,
			cd_medico_atendimento,
			null,
			nr_seq_unid_atual,
			nr_seq_unid_int,
			nr_atend_original,
			qt_dia_longa_perm,
			dt_inicio_atendimento,
			ie_permite_visita,
			ie_status_atendimento,
			null,
			null,
			cd_pessoa_fisica_w,
			dt_atend_medico,
			dt_fim_consulta,
			dt_medicacao,
			dt_saida_real,
			null,
			dt_lib_medico,
			nr_seq_regra_funcao,
			nr_seq_local_pa,
			nr_seq_tipo_acidente,
			dt_ocorrencia,
			ds_pend_autorizacao,
			null,
			nr_seq_forma_laudo,
			nr_seq_check_list,
			dt_fim_triagem,
			nr_reserva_leito,
			'N',
			ie_permite_visita_rel,
			ds_senha,
			null,
			nr_seq_forma_chegada,
			nr_seq_indicacao,
			cd_pessoa_indic,
			null,
			nm_medico_externo,
			nr_gestante_pre_natal,
			null,
			nr_atendimento,
			nvl(ie_trat_conta_rn_w,Wheb_mensagem_pck.get_texto(308480) /*'Mae'*/),
			nr_seq_classif_rn_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atendimento_p);

		begin
		select	*
		into	pessoa_fisica_taxa_w
		from	pessoa_fisica_taxa
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	nr_atendimento		= nr_atendimento_p
		and	rownum = 1;
		exception
		when others then
			pessoa_fisica_taxa_w.nr_sequencia := null;
		end;
		
		if	(pessoa_fisica_taxa_w.nr_sequencia is not null) then
		
			select	max(nr_sequencia)
			into	nr_seq_justificativa_taxa_rn_w
			from	pessoa_taxa_justificativa
			where	nvl(ie_situacao,'A') 	= 'A'
			and	ie_atendimento_rn	= 'S';
			
			--Se existe uma justifica padrao, entao utiliza esta, caso contrario utiliza os valores do atendimento da Mae
			if	(nvl(nr_seq_justificativa_taxa_rn_w ,0) > 0) then
			
				pessoa_fisica_taxa_w.nr_seq_justificativa 	:= nr_seq_justificativa_taxa_rn_w;
				pessoa_fisica_taxa_w.qt_dias_pagamento		:= null;
				pessoa_fisica_taxa_w.ie_obriga_pag_adicional	:= 'N';
			
			end if;
			
			select	pessoa_fisica_taxa_seq.nextval
			into	pessoa_fisica_taxa_w.nr_sequencia
			from	dual;
			
			pessoa_fisica_taxa_w.nr_atendimento	:= nr_atend_rn_w;
			pessoa_fisica_taxa_w.cd_pessoa_fisica	:= cd_pessoa_rn_w;
			pessoa_fisica_taxa_w.dt_atualizacao	:= sysdate;
			pessoa_fisica_taxa_w.nm_usuario		:= nm_usuario_p;
			pessoa_fisica_taxa_w.dt_atualizacao_nrec := sysdate;
			pessoa_fisica_taxa_w.nm_usuario_nrec	:= nm_usuario_p;			
			
			insert into pessoa_fisica_taxa values pessoa_fisica_taxa_w;
		end if;		
		
		IF	(cd_setor_passagem_w <> 0)THEN
			select	count(*)
			into	ie_setor_w
			from	atend_paciente_unidade
			where	nr_atendimento		= nr_atendimento_p
			and	cd_setor_atendimento	= cd_setor_passagem_w;

			IF	(ie_setor_w = 0) THEN
				gerar_passagem_setor_atend(nr_atendimento_p, cd_setor_passagem_w, SYSDATE, 'N', nm_usuario_p);
			END IF;
		END IF;
		
		if (ie_import_pagador_w = 'S') then
			for c02_r in C02
			loop
				insert into ATENDIMENTO_PAGADOR (
					nr_sequencia, 
					dt_atualizacao, 
					nm_usuario, 
					dt_atualizacao_nrec, 
					nm_usuario_nrec, 
					cd_pessoa_fisica, 
					cd_cgc, 
					nr_atendimento, 
					nr_seq_grau_parentesco, 
					ds_acordo, 
					ds_condicao
				) values (
					atendimento_pagador_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					c02_r.cd_pessoa_fisica,
					c02_r.cd_cgc,
					nr_atend_rn_w,
					c02_r.nr_seq_grau_parentesco,
					c02_r.ds_acordo,
					c02_r.ds_condicao
				);
			end loop;
		end if;
		
		update nascimento set nr_atend_rn = nr_atend_rn_w
		where	nr_atendimento	= nr_atendimento_p	and	nr_sequencia	= nr_seq_nasc_w;
		
		select	max(dt_final_vigencia)
		into	dt_final_vigencia_w
		from	atendimento_paciente_v
		where	nr_atendimento	= nr_atendimento_p;
		

		if	(dt_final_vigencia_w	is not null) and
			(nvl(dt_nascimento_rn_w, sysdate)	> dt_final_vigencia_w) then
			dt_final_vigencia_w	:= nvl(dt_nascimento_rn_w, sysdate);
		end if;
		
		if (ie_rn_vigencia_null_w = 'S') then
			dt_final_vigencia_w := null;
		end if;
		
		insert	into atend_categoria_convenio
			(nr_seq_interno,
			nr_atendimento,
			cd_convenio,
			cd_categoria,
			dt_inicio_vigencia,
			dt_final_vigencia,
			dt_atualizacao,
			cd_usuario_convenio,
			cd_empresa,
			nm_usuario,
			nr_doc_convenio,
			cd_tipo_acomodacao,
			cd_municipio_convenio,
			cd_convenio_glosa,
			cd_categoria_glosa,
			dt_validade_carteira,
			nr_acompanhante,
			cd_plano_convenio,
			cd_dependente,
			nr_seq_origem,
			cd_senha,
			ie_tipo_guia,
			ds_observacao,
			qt_dia_internacao,
			dt_ultimo_pagto,
			cd_complemento,
			dt_aceite_dif_acomod,
			nm_usuario_aceite,
			ds_observacao_aceite,
			qt_dieta_acomp,
			ie_lib_dieta,
			nr_prioridade)
		(select	atend_categoria_convenio_seq.nextval,
			nr_atend_rn_w,
			cd_convenio,
			cd_categoria,
			nvl(dt_nascimento_rn_w, sysdate),
			dt_final_vigencia_w,
			dt_atualizacao,
			cd_usuario_convenio,
			cd_empresa,
			nm_usuario,
			nr_doc_convenio,
			cd_tipo_acomodacao,
			cd_municipio_convenio,
			cd_convenio_glosa,
			cd_categoria_glosa,
			dt_validade_carteira,
			nr_acompanhante,
			cd_plano_convenio,
			cd_dependente,
			nr_seq_origem,
			cd_senha,
			ie_tipo_guia,
			ds_observacao,
			qt_dia_internacao,
			dt_ultimo_pagto,
			cd_complemento,
			dt_aceite_dif_acomod,
			nm_usuario_aceite,
			ds_observacao_aceite,
			qt_dieta_acomp,
			ie_lib_dieta,
			nr_prioridade
		from	atend_categoria_convenio
		where	nr_atendimento	= nr_atendimento_p
		and	nr_seq_interno = (select max(nr_seq_interno) from atend_categoria_convenio where nr_atendimento = nr_atendimento_p));

		if	(nr_seq_unid_rn_w > 0) then
			select	cd_setor_atendimento,
				cd_unidade_basica,
				cd_unidade_compl,
				cd_tipo_acomodacao,
				qt_max_acomp
			into	cd_setor_rn_w,
				cd_unidade_bas_rn_w,
				cd_unidade_comp_rn_w,
				cd_acomod_rn_w,
				qt_max_acomp_rn_w
			from	unidade_atendimento
			where	nr_seq_interno	= nr_seq_unid_rn_w;
		else
			cd_setor_rn_w		:=	cd_setor_atendimento_p;
			cd_unidade_bas_rn_w	:=	cd_unidade_basica_p;
			cd_unidade_comp_rn_w	:=	cd_unidade_compl_p;
			cd_acomod_rn_w		:=	cd_acomodacao_p;

			select 	qt_max_acomp
			into	qt_max_acomp_rn_w
			from	unidade_atendimento
			where	cd_unidade_compl = cd_unidade_compl_p
			and	cd_unidade_basica = cd_unidade_basica_p
			and	cd_setor_atendimento = cd_setor_atendimento_p;
		end if;
		
		if	(ie_gerar_passagem_setor_w = 'S') then
			insert into atend_paciente_unidade (
				nr_atendimento,
				nr_sequencia,
				nr_seq_interno,
				cd_setor_atendimento,
				cd_unidade_basica,
				cd_unidade_compl,
				dt_entrada_unidade,
				dt_atualizacao,
				nm_usuario,
				cd_tipo_acomodacao,
				nm_usuario_original,
				dt_saida_interno,
				ie_passagem_setor,
				nr_acompanhante,
				ie_calcular_dif_diaria)
			values (
				nr_atend_rn_w,
				1,
				atend_paciente_unidade_seq.nextval,
				cd_setor_rn_w,
				cd_unidade_bas_rn_w,
				cd_unidade_comp_rn_w,
				nvl(dt_nascimento_rn_w,sysdate),
				sysdate,
				nm_usuario_p,
				cd_acomod_rn_w,
				nm_usuario_p,
				to_date('30/12/2999','dd/mm/yyyy'),
				'N',
				qt_max_acomp_rn_w,
				'S');
		end if;
		
		end;
		
		commit;

		if	(nvl(ie_gera_lancto_auto_rn_w,'N') = 'S') and  
			(nvl(nr_atend_rn_w,0) > 0) then
			gerar_lancamento_automatico (nr_atend_rn_w, null, 26, nm_usuario_p, null,null,null,null,null,null);		
		end if;

		
		if	(cid_rn_w is not null) then
			Gerar_diagnostico_Atend(nr_atend_rn_w,
						cid_rn_w,
						cd_medico_resp_w,
						nm_usuario_p,
						2,
						IE_CLASSIF_DOENCA_RN_w);
		end if;

	end if;

	nr_atend_rn_p			:= nr_atend_rn_w;

	if	(nr_atend_rn_w is not null) and
		(ie_gerar_ci_w = 'S') and
		(cd_setor_usuario_w > 0) then
		begin

		select	comunic_interna_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert	into comunic_interna(	dt_comunicado,
						ds_titulo,
						ds_comunicado,
						nm_usuario,
						dt_atualizacao,
						ie_geral,
						nm_usuario_destino,
						cd_perfil,
						nr_sequencia,
						ie_gerencial,
						nr_seq_classif,
						ds_perfil_adicional,
						cd_setor_destino,
						cd_estab_destino,
						ds_setor_adicional,
						dt_liberacao,
						ds_grupo)
				values		(
						sysdate,
						Wheb_mensagem_pck.get_texto(308481),/*'Geracao de atendimento de RN',*/
						Wheb_mensagem_pck.get_texto(308482, 'NR_ATEND_RN_W='||nr_atend_rn_w||';NR_ATENDIMENTO_P='||nr_atendimento_p), /*'Atendimento de RN ( ' || nr_atend_rn_w || ' ) gerado para o atendimento ' || nr_atendimento_p,*/
						nm_usuario_p,
						sysdate,
						'N',
						null,
						null,
						nr_sequencia_w,
						'N',
						null,
						null,
						null,
						cd_estabelecimento_w,
						cd_setor_usuario_w || ',',
						sysdate,
						null);
		end;
	end if;
	
	if	(nvl(nr_atend_rn_w,0) > 0)  then
		begin
		GQA_Gerar_Protocolo_assist(nr_atend_rn_w,'3',nm_usuario_p);
		exception
		when others then
			null;
		end;
	end if;
	
	nr_seq_classif_w  := obter_classificacao_pf(cd_pessoa_fisica_w);
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_rn_w,sysdate,'A'),0);
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
	begin
	cd_setor_paciente_w	:= obter_setor_atendimento(nr_atendimento_p);
	exception
		when others then
		null;
	end;
	open C01;
		loop
		fetch C01 into
			nr_seq_evento_w;
		exit when C01%notfound;
			begin
			gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_p,cd_pessoa_rn_w,null,nm_usuario_p);
			end;
		end loop;
	close C01;
	end;

	gerar_pedido_parecer_RN(cd_medico_resp_w,nr_atendimento_p,nm_usuario_p);
else
	nr_atend_rn_p	:= nr_atend_rn_w;
end if;

if	(nvl(ie_html_p, 'N') = 'S') and (nr_atend_rn_p > 0) and (nr_seq_nascimento_p > 0) then
	update	nascimento
	set	      nr_atend_rn       = nr_atend_rn_p
	where	nr_sequencia      = nr_seq_nascimento_p
	and       nr_atendimento  = nr_atendimento_p;
   
   update   ESCALA_CAPURRO
   set      nr_atend_rn       = nr_atend_rn_p
   where    nr_atendimento    = nr_atendimento_p
   and      nr_seq_nascimento = nr_seq_nascimento_p;
   
   update   ESCALA_CAPURRO_NEURO
   set      nr_atend_rn       = nr_atend_rn_p
   where    nr_atendimento    = nr_atendimento_p
   and      nr_seq_nascimento = nr_seq_nascimento_p;
   
   update   ESCALA_SILVERMAN
   set      nr_atend_rn       = nr_atend_rn_p
   where    nr_atendimento    = nr_atendimento_p
   and      nr_seq_nascimento = nr_seq_nascimento_p;
   
   update   ESCALA_APGAR
   set      nr_atend_rn       = nr_atend_rn_p
   where    nr_atendimento    = nr_atendimento_p
   and      nr_seq_nascimento = nr_seq_nascimento_p;

	commit;
end if;

  if (nr_atendimento_p is not null and nr_seq_nascimento_p is not null) then
      INSERIR_PACIENTE_VACINA_RN (null, nr_atendimento_p, nr_seq_nascimento_p, null, null, null, nm_usuario_p, 'ST');
      commit;
  end if;

end gerar_atendimento_rn;
/
