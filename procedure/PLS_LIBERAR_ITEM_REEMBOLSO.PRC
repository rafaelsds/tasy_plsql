create or replace
procedure pls_liberar_item_reembolso
			( 	nr_seq_reembolso_proc_p		Number,
				nr_seq_reembolso_mat_p		Number,
				ie_opcao_p			Varchar2,
				nm_usuario_p			Varchar2) is 

nr_seq_conta_w			Number(10);
nr_seq_segurado_w		Number(10);
cd_estabelecimento_w		Number(4);
ie_valor_apresentado_w		Varchar2(1);
vl_unitario_imp_w		pls_conta_mat.vl_unitario%type;
qt_procedimento_imp_w		Number(12,4);
vl_procedimento_imp_w		Number(15,2);
vl_procedimento_w		Number(15,2);
vl_unitario_w			pls_conta_proc.vl_unitario%type;
vl_liberado_w			Number(15,2);
ie_status_proc_w		Varchar2(1);
nr_seq_regra_acao_reemb_w	pls_regra_reemb_acao.nr_sequencia%type;
ie_acao_w			pls_regra_reemb_acao.ie_acao%type;
qt_material_imp_w		pls_conta_mat.qt_material_imp%type;
vl_material_imp_w		pls_conta_mat.vl_material_imp%type;
vl_material_w			pls_conta_mat.vl_material%type;
nr_lote_reembolso_w		lote_contabil.nr_lote_contabil%type;
vl_glosa_w			pls_conta_proc.vl_glosa%type := 0;

begin

/* Obter o valor do par�metro 12 - Ao liberar um item pendente, atualizar o valor liberado com o valor apresentado */
ie_valor_apresentado_w	:= nvl(obter_valor_param_usuario(1229, 12, Obter_Perfil_Ativo, nm_usuario_p, 0), 'S');

if 	(nvl(nr_seq_reembolso_proc_p,0) <> 0) then
	select	a.nr_seq_conta,
		a.vl_unitario_imp,
		a.qt_procedimento_imp,
		a.vl_procedimento_imp,
		a.vl_procedimento,
		a.vl_liberado,
		c.nr_seq_segurado,
		c.cd_estabelecimento,
		a.ie_status,
		a.nr_seq_regra_acao_reemb,
		c.nr_lote_contabil
	into	nr_seq_conta_w,
		vl_unitario_imp_w,
		qt_procedimento_imp_w,
		vl_procedimento_imp_w,
		vl_procedimento_w,
		vl_liberado_w,
		nr_seq_segurado_w,
		cd_estabelecimento_w,
		ie_status_proc_w,
		nr_seq_regra_acao_reemb_w,
		nr_lote_reembolso_w
	from	pls_protocolo_conta	c,
		pls_conta		b,
		pls_conta_proc		a
	where	a.nr_sequencia		= nr_seq_reembolso_proc_p
	and	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_protocolo	= c.nr_sequencia;
	
	if	(nvl(ie_status_proc_w,'U') <> 'D') then
		/* 24/01/2011 - Felipe - OS 282033 */
				
		if	(ie_valor_apresentado_w	= 'S') then
			vl_unitario_w	:= vl_unitario_imp_w;
			vl_liberado_w	:= vl_procedimento_imp_w;
		else
			vl_unitario_w	:= dividir(vl_liberado_w,qt_procedimento_imp_w);
			
			
		end if;

		if	(ie_opcao_p = 'L') then	
		
			/* Tratamento para liberar conforme a��o do reembolso */
			if	(nr_seq_regra_acao_reemb_w is not null) then
				select	ie_acao
				into	ie_acao_w
				from	pls_regra_reemb_acao
				where	nr_sequencia	= nr_seq_regra_acao_reemb_w;
				
				/* Regra de pre�o */
				if	(ie_acao_w = 'RP') then
					vl_liberado_w	:= least(vl_procedimento_w,vl_procedimento_imp_w);
					vl_unitario_w	:= dividir(vl_liberado_w,qt_procedimento_imp_w);
				/* Valor apresentado */
				elsif	(ie_acao_w = 'VA') then
					vl_unitario_w	:= vl_unitario_imp_w;
					vl_liberado_w	:= vl_procedimento_imp_w;
				/* N�o reembolsar*/
				elsif	(ie_acao_w = 'N') then
					vl_unitario_w	:= 0;
					vl_liberado_w	:= 0;
				end if;
				
				--pls_gerenciar_reembolso_pck.pls_gerar_apropriacao(null,nr_seq_reembolso_proc_p,null,nm_usuario_p);
			end if;
			
			vl_glosa_w := vl_procedimento_imp_w - vl_liberado_w;
			
			if	(vl_glosa_w < 0) then
				vl_glosa_w := 0;
			end if;
		
			update	pls_conta_proc
			set	ie_status	= 'L',
				qt_procedimento	= qt_procedimento_imp,
				vl_unitario	= vl_unitario_w,
				vl_liberado	= vl_liberado_w,
				nm_usuario 	= nm_usuario_p,
				vl_glosa	= vl_glosa_w,
				dt_atualizacao	= sysdate
			where	nr_sequencia 	= nr_seq_reembolso_proc_p;
			
			if	(nr_seq_regra_acao_reemb_w is not null) then
			
				if	(ie_acao_w = 'RC') then
					pls_apropriacao_pck.pls_gerar_apropriacao_conta(nr_seq_conta_w,nr_seq_reembolso_proc_p,null, nm_usuario_p);
				else
					pls_gerenciar_reembolso_pck.pls_gerar_apropriacao(null,nr_seq_reembolso_proc_p,null,nm_usuario_p);
				end if;
			end if;

		elsif 	(ie_opcao_p = 'D') then
						
			if	(nvl(nr_lote_reembolso_w,0) <> 0) then
				--A a��o n�o pode ser completada pois o movimento est� vinculado ao lote cont�bil #@NR_LOTE_CONTABIL#@ do tipo #@DS_TIPO_LOTE_CONTABIL#@!
				wheb_mensagem_pck.exibir_mensagem_abort(472015, 'NR_LOTE_CONTABIL=' || nr_lote_reembolso_w ||
										';DS_TIPO_LOTE_CONTABIL=' || ctb_obter_tipo_lote_contabil(nr_lote_reembolso_w,'D'));
			end if;
		
			update	pls_conta_proc
			set	ie_status	= 'P',
				qt_procedimento	= 0,
				vl_unitario	= 0,
				vl_liberado	= 0,
				nm_usuario 	= nm_usuario_p,
				dt_atualizacao	= sysdate,
				vl_coparticipacao = 0,
				vl_glosa	  = 0
			where	nr_sequencia 	= nr_seq_reembolso_proc_p;
			
			if	(nr_seq_regra_acao_reemb_w is not null) then
				pls_gerenciar_reembolso_pck.pls_reemb_limpa_apropriacao(null,nr_seq_reembolso_proc_p,null);
			end if;
		end if;
	end if;
elsif 	(nvl(nr_seq_reembolso_mat_p,0) <> 0) then

	select	a.nr_seq_conta,
		c.nr_seq_segurado,
		c.cd_estabelecimento,
		a.nr_seq_regra_acao_reemb,
		a.qt_material_imp,
		a.vl_unitario_imp,
		a.vl_material_imp,
		a.vl_material,
		c.nr_lote_contabil
	into	nr_seq_conta_w,
		nr_seq_segurado_w,
		cd_estabelecimento_w,
		nr_seq_regra_acao_reemb_w,
		qt_material_imp_w,
		vl_unitario_imp_w,
		vl_material_imp_w,
		vl_material_w,
		nr_lote_reembolso_w
	from	pls_protocolo_conta	c,
		pls_conta		b,
		pls_conta_mat		a
	where	a.nr_sequencia		= nr_seq_reembolso_mat_p
	and	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_protocolo	= c.nr_sequencia;
	
	if	(ie_opcao_p = 'L') then
		/* Tratamento para liberar conforme a��o do reembolso */
		if	(nr_seq_regra_acao_reemb_w is not null) then
			select	ie_acao
			into	ie_acao_w
			from	pls_regra_reemb_acao
			where	nr_sequencia	= nr_seq_regra_acao_reemb_w;
			
			/* Regra de pre�o */
			if	(ie_acao_w = 'RP') then
				vl_liberado_w	:= vl_material_w;
				vl_unitario_w	:= dividir(vl_liberado_w,qt_material_imp_w);
			/* Valor apresentado */
			elsif	(ie_acao_w = 'VA') then
				vl_unitario_w	:= vl_unitario_imp_w;
				vl_liberado_w	:= vl_material_imp_w;
			/* N�o reembolsar*/
			elsif	(ie_acao_w = 'N') then
				vl_unitario_w	:= 0;
				vl_liberado_w	:= 0;
			end if;
			
			--pls_gerenciar_reembolso_pck.pls_gerar_apropriacao(null,null,nr_seq_reembolso_mat_p,nm_usuario_p);
		end if;
		
		if	(vl_liberado_w is not null) then
			vl_glosa_w := vl_material_imp_w - vl_liberado_w;
		end if;
		
		if	(vl_glosa_w < 0) then
			vl_glosa_w := 0;
		end if;
	
		update	pls_conta_mat
		set	ie_status 	= 'L',
			qt_material	= qt_material_imp,
			vl_unitario	= nvl(vl_unitario_w,vl_unitario_imp),
			vl_liberado	= nvl(vl_liberado_w,vl_material_imp),
			nm_usuario	= nm_usuario_p,
			vl_glosa	= vl_glosa_w,
			dt_atualizacao	= sysdate
		where	nr_sequencia 	= nr_seq_reembolso_mat_p;
		
		if	(nr_seq_regra_acao_reemb_w is not null) then
		
			if	(ie_acao_w = 'RC') then
				pls_apropriacao_pck.pls_gerar_apropriacao_conta(nr_seq_conta_w,null,nr_seq_reembolso_mat_p, nm_usuario_p);
			else
				pls_gerenciar_reembolso_pck.pls_gerar_apropriacao(null,null,nr_seq_reembolso_mat_p,nm_usuario_p);
			end if;
		end if;
	elsif 	(ie_opcao_p = 'D') then		
		if	(nvl(nr_lote_reembolso_w,0) <> 0) then
			--A a��o n�o pode ser completada pois o movimento est� vinculado ao lote cont�bil #@NR_LOTE_CONTABIL#@ do tipo #@DS_TIPO_LOTE_CONTABIL#@!
			wheb_mensagem_pck.exibir_mensagem_abort(472015, 'NR_LOTE_CONTABIL=' || nr_lote_reembolso_w ||
									';DS_TIPO_LOTE_CONTABIL=' || ctb_obter_tipo_lote_contabil(nr_lote_reembolso_w,'D'));
		end if;
	
		update	pls_conta_mat
		set	ie_status 	= 'P',
			qt_material	= 0,
			vl_unitario	= 0,
			vl_liberado	= 0,
			vl_material	= 0,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate,
			vl_participacao = 0,
			vl_glosa	= 0
		where	nr_sequencia 	= nr_seq_reembolso_mat_p;
		
		if	(nr_seq_regra_acao_reemb_w is not null) then
			pls_gerenciar_reembolso_pck.pls_reemb_limpa_apropriacao(null,null,nr_seq_reembolso_mat_p);
		end if;
	end if;
end if;

pls_alterar_status_reembolso(nr_seq_conta_w, nm_usuario_p);
pls_atualiza_valor_conta(nr_seq_conta_w,nm_usuario_p);

commit;

end pls_liberar_item_reembolso;
/
