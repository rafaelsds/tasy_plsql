create or replace
procedure pls_gerar_w_analise_glosa_ana
			(	nr_seq_analise_p	number,
				nr_seq_grupo_atual_p	number,
				nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a tabela tempor�ria para visualiza��o das glosas e ocorr�ncias dos itens da an�lise Nova.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_tipo_w			varchar2(255);
ds_status_w			varchar2(255);
ds_glosa_vinc_w			varchar2(255);
ds_pagamento_w			varchar2(255);
ds_faturamento_w		varchar2(255);
ds_nivel_liberacao_w		varchar2(255);
ds_grupos_analise_w		varchar2(255);
ie_anexo_w			varchar2(255);
ds_forma_inativacao_w		varchar2(80);
ie_status_w			varchar2(10);
ie_permissao_grupo_w		varchar2(1);
ie_selecionada_w		varchar2(1)	:= null;
nr_seq_motivo_glosa_w		number(10);
nr_seq_conta_glosa_w		number(10);
nr_sequencia_w			number(10)	:= 0;
nr_seq_motivo_vinc_w		number(10)	:= null;
nr_nivel_liberacao_w		number(10);
nr_seq_fluxo_grupo_w		number(10);
qt_concluida_w			number(10);
qt_grupos_w			number(10);
qt_fluxo_w			number(10);
nr_seq_glosa_vinc_w		number(10);

Cursor C01 is
	/* Glosas da conta */
	select	a.nr_sequencia,
		a.nr_seq_motivo_glosa,
		b.cd_motivo_tiss,
		b.ds_motivo_tiss,
		a.ie_situacao,
		a.ie_fechar_conta,
		a.ds_observacao,
		a.ie_forma_inativacao,
		a.nr_seq_conta nr_seq_conta,
		null nr_seq_conta_proc,
		null nr_seq_proc_partic,
		null nr_seq_conta_mat
	from	tiss_motivo_glosa	b,
		pls_conta_glosa		a,
		pls_conta		c
	where	a.nr_seq_motivo_glosa	= b.nr_sequencia
	and	a.nr_seq_conta		= c.nr_sequencia
	and	c.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_ocorrencia_benef 	is null
	and	a.nr_seq_conta_proc 		is null
	and	a.nr_seq_conta_mat 		is null
	and	a.nr_seq_proc_partic 		is null
	and	a.nr_seq_conta_pos_estab 	is null
	and	not exists	(	select	1
					from	pls_ocorrencia_benef x
					where	x.nr_seq_glosa = a.nr_sequencia)
	union all
	/* Glosas do procedimento */
	select	a.nr_sequencia,
		a.nr_seq_motivo_glosa,
		b.cd_motivo_tiss,
		b.ds_motivo_tiss,
		a.ie_situacao,
		a.ie_fechar_conta,
		a.ds_observacao,
		a.ie_forma_inativacao,
		null nr_seq_conta,
		a.nr_seq_conta_proc nr_seq_conta_proc,
		a.nr_seq_proc_partic nr_seq_proc_partic,
		null nr_seq_conta_mat
	from	tiss_motivo_glosa	b,
		pls_conta_glosa		a,
		pls_conta_proc_v	c
	where	a.nr_seq_motivo_glosa	= b.nr_sequencia
	and	a.nr_seq_conta_proc	= c.nr_sequencia
	and	c.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_ocorrencia_benef 	is null
	and	a.nr_seq_conta_pos_estab 	is null
	and	not exists	(	select	1
					from	pls_ocorrencia_benef x
					where	x.nr_seq_glosa = a.nr_sequencia)
	union all
	/* Glosas do material */
	select	a.nr_sequencia,
		a.nr_seq_motivo_glosa,
		b.cd_motivo_tiss,
		b.ds_motivo_tiss,
		a.ie_situacao,
		a.ie_fechar_conta,
		a.ds_observacao,
		a.ie_forma_inativacao,
		null nr_seq_conta,
		null nr_seq_conta_proc,
		null nr_seq_proc_partic,
		a.nr_seq_conta_mat nr_seq_conta_mat
	from	tiss_motivo_glosa	b,
		pls_conta_glosa		a,
		pls_conta_mat_v		c
	where	a.nr_seq_motivo_glosa	= b.nr_sequencia
	and	a.nr_seq_conta_mat	= c.nr_sequencia
	and	c.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_ocorrencia_benef 	is null
	and	a.nr_seq_conta_pos_estab 	is null
	and	not exists	(	select	1
					from	pls_ocorrencia_benef x
					where	x.nr_seq_glosa = a.nr_sequencia);

Cursor C02 is
	/* Ocorr�ncias da conta */
	select	a.nr_sequencia,
		a.nr_seq_ocorrencia,
		b.cd_ocorrencia,
		b.ds_ocorrencia,
		a.ie_situacao,
		a.ie_fechar_conta,
		b.ie_glosar_pagamento,
		b.ie_glosar_faturamento,
		b.nr_seq_nivel_lib,
		a.nr_seq_glosa,
		a.ie_finalizar_analise,
		a.ie_auditoria,
		a.ds_observacao,
		a.ie_forma_inativacao,
		a.nr_seq_oc_cta_comb,
		a.nr_seq_conta nr_seq_conta,
		null nr_seq_conta_proc,
		null nr_seq_proc_partic,
		null nr_seq_conta_mat
	from	pls_ocorrencia		b,
		pls_ocorrencia_benef	a,
		pls_conta		c
	where	a.nr_seq_ocorrencia	= b.nr_sequencia
	and	a.nr_seq_conta		= c.nr_sequencia
	and	c.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_conta_proc 	is null
	and	a.nr_seq_proc 		is null
	and	a.nr_seq_conta_mat 	is null
	and	a.nr_seq_mat 		is null
	and	a.nr_seq_proc_partic 	is null
	and	a.nr_seq_conta_pos_estab is null
	union all
	/* Ocorr�ncias do procedimento */
	select	a.nr_sequencia,
		a.nr_seq_ocorrencia,
		b.cd_ocorrencia,
		b.ds_ocorrencia,
		a.ie_situacao,
		a.ie_fechar_conta,
		b.ie_glosar_pagamento,
		b.ie_glosar_faturamento,
		b.nr_seq_nivel_lib,
		a.nr_seq_glosa,
		a.ie_finalizar_analise,
		a.ie_auditoria,
		a.ds_observacao,
		a.ie_forma_inativacao,
		a.nr_seq_oc_cta_comb,
		null nr_seq_conta,
		a.nr_seq_conta_proc nr_seq_conta_proc,
		a.nr_seq_proc_partic nr_seq_proc_partic,
		null nr_seq_conta_mat
	from	pls_ocorrencia 		b,
		pls_ocorrencia_benef 	a,
		pls_conta_proc_v	c
	where	a.nr_seq_ocorrencia	= b.nr_sequencia
	and	a.nr_seq_conta_proc	= c.nr_sequencia
	and	c.nr_seq_analise	= nr_seq_analise_p
	union all
	/* Ocorr�ncias do material */
	select	a.nr_sequencia,
		a.nr_seq_ocorrencia,
		b.cd_ocorrencia,
		b.ds_ocorrencia,
		a.ie_situacao,
		a.ie_fechar_conta,
		b.ie_glosar_pagamento,
		b.ie_glosar_faturamento,
		b.nr_seq_nivel_lib,
		a.nr_seq_glosa,
		a.ie_finalizar_analise,
		a.ie_auditoria,
		a.ds_observacao,
		a.ie_forma_inativacao,
		a.nr_seq_oc_cta_comb,
		null nr_seq_conta,
		null nr_seq_conta_proc,
		null nr_seq_proc_partic,
		a.nr_seq_conta_mat nr_seq_conta_mat
	from	pls_ocorrencia		b,
		pls_ocorrencia_benef	a,
		pls_conta_mat_v		c
	where	a.nr_seq_ocorrencia	= b.nr_sequencia
	and	a.nr_seq_conta_mat	= c.nr_sequencia
	and	c.nr_seq_analise	= nr_seq_analise_p;

/* Fluxo de an�lise */
Cursor C03 (nr_seq_ocorrencia_benef_pc	pls_ocorrencia_benef.nr_sequencia%type) is
	select	a.nr_seq_grupo,
		a.ie_status,
		b.nm_grupo_auditor
	from	pls_grupo_auditor b,
		pls_analise_glo_ocor_grupo a
	where	a.nr_seq_grupo		= b.nr_sequencia
	and	a.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_ocor_benef	= nr_seq_ocorrencia_benef_pc
	and	((a.ie_fluxo_adic	is null) or (a.ie_fluxo_adic	= 'N'));

begin

delete from w_pls_analise_glosa_ocor
where	nr_seq_analise	= nr_seq_analise_p;

/* Inser��o das glosas */
nr_seq_motivo_glosa_w	:= null;
nr_seq_conta_glosa_w	:= null;
ie_status_w		:= null;
ds_tipo_w		:= null;
ds_status_w		:= null;
ds_glosa_vinc_w		:= null;
ds_pagamento_w		:= null;
ds_faturamento_w	:= null;

for r_c01_w in C01() loop
	begin
	ds_forma_inativacao_w	:= null;
	nr_sequencia_w		:= nr_sequencia_w + 1;
	ds_tipo_w		:= 'Glosa';
	ds_pagamento_w		:= 'Impede';
	ds_faturamento_w	:= 'N�o impede';

	/* Verifica��o do status */
	ie_status_w	:= 'F';

	select	count(1)
	into	qt_fluxo_w
	from	pls_analise_glo_ocor_grupo a
	where	a.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_conta_glosa	= r_c01_w.nr_sequencia
	and	((a.ie_fluxo_adic	is null) or (a.ie_fluxo_adic	= 'N'));
	
	if	(qt_fluxo_w > 0) then
		if	(r_c01_w.ie_situacao = 'I') then
			ie_status_w	:= 'I';
		else
			ie_status_w	:= 'C';
		end if;
	end if;

	ds_status_w	:= null;--obter_valor_dominio(5186,ie_status_w);

	if	(ie_status_w is not null) then
		select	ds_valor_dominio
		into	ds_status_w
		from	valor_dominio_v
		where	cd_dominio	= 5186
		and	vl_dominio	= ie_status_w;
	end if;

	ie_selecionada_w	:= null;

	if	(r_c01_w.ie_situacao = 'A') then
		ie_selecionada_w	:= 'S';
	end if;

	if	(r_c01_w.ie_forma_inativacao = 'U') then
		ds_forma_inativacao_w	:= 'Usu�rio';
	elsif	(r_c01_w.ie_forma_inativacao = 'S') then
		ds_forma_inativacao_w	:= 'Sistema';
	elsif	(r_c01_w.ie_forma_inativacao = 'US') then
		ds_forma_inativacao_w	:= 'Usu�rio e Sistema';	
	end if;
	
	insert into w_pls_analise_glosa_ocor
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_analise,
		nr_seq_conta,
		nr_seq_conta_proc,
		nr_seq_conta_mat,
		nr_seq_proc_partic,
		nr_seq_ocorrencia,
		nr_seq_motivo_glosa,
		ds_glosa_ocorrencia,
		nr_seq_ocor_benef,
		nr_seq_conta_glosa,
		ie_situacao,
		ie_status,
		cd_ocorrencia,
		ds_tipo,
		ie_fechar_conta,
		ds_pagamento,
		ds_faturamento,
		ds_status,
		ds_observacao,
		ie_selecionada,
		ie_forma_inativacao,
		ds_forma_inativacao,
		nr_seq_oc_cta_comb)
	values	(nr_sequencia_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_analise_p,
		r_c01_w.nr_seq_conta,
		r_c01_w.nr_seq_conta_proc,
		r_c01_w.nr_seq_conta_mat,
		r_c01_w.nr_seq_proc_partic,
		null,
		r_c01_w.nr_seq_motivo_glosa,
		r_c01_w.ds_motivo_tiss,
		null,
		r_c01_w.nr_sequencia,
		r_c01_w.ie_situacao,
		ie_status_w,
		r_c01_w.cd_motivo_tiss,
		ds_tipo_w,
		r_c01_w.ie_fechar_conta,
		ds_pagamento_w,
		ds_faturamento_w,
		ds_status_w,
		r_c01_w.ds_observacao,
		ie_selecionada_w,
		r_c01_w.ie_forma_inativacao,
		ds_forma_inativacao_w,
		null);
	end;
end loop;

/* Inser��o das ocorr�ncias */
nr_seq_motivo_glosa_w	:= null;
nr_seq_conta_glosa_w	:= null;
ie_status_w		:= null;
ds_tipo_w		:= null;
ds_status_w		:= null;
ds_glosa_vinc_w		:= null;
ds_pagamento_w		:= null;
ds_faturamento_w	:= null;

for r_c02_w in c02() loop
	begin
	nr_sequencia_w		:= nr_sequencia_w + 1;
	ds_forma_inativacao_w	:= null;
	ds_tipo_w		:= 'Ocorr�ncia';
	ds_glosa_vinc_w		:= null;
	nr_seq_glosa_vinc_w	:= null;
	
	if	(r_c02_w.nr_seq_glosa is not null) then
		nr_seq_glosa_vinc_w	:= r_c02_w.nr_seq_glosa;
	else
		select	max(a.nr_sequencia)
		into	nr_seq_glosa_vinc_w
		from	pls_conta_glosa a
		where	a.nr_seq_ocorrencia_benef	= r_c02_w.nr_sequencia;
	end if;
	
	if	(nr_seq_glosa_vinc_w is not null) then
		select	nr_seq_motivo_glosa,
			b.ds_motivo_tiss
		into	nr_seq_motivo_vinc_w,
			ds_glosa_vinc_w
		from	tiss_motivo_glosa b,
			pls_conta_glosa a
		where	a.nr_seq_motivo_glosa = b.nr_sequencia
		and	a.nr_sequencia	= nr_seq_glosa_vinc_w;
	end if;

	if	(r_c02_w.ie_glosar_pagamento = 'S') then
		ds_pagamento_w	:= 'Impede';
	else
		ds_pagamento_w	:= 'N�o impede';
	end if;

	if	(r_c02_w.ie_glosar_faturamento = 'S') then
		ds_faturamento_w	:= 'Impede';
	else
		ds_faturamento_w	:= 'N�o impede';
	end if;

	nr_nivel_liberacao_w	:= null;
	ds_nivel_liberacao_w	:= null;

	if	(r_c02_w.nr_seq_nivel_lib is not null) then
		select	a.nr_nivel_liberacao,
			a.ds_nivel_liberacao
		into	nr_nivel_liberacao_w,
			ds_nivel_liberacao_w
		from	pls_nivel_liberacao a
		where	a.nr_sequencia	= r_c02_w.nr_seq_nivel_lib;
	end if;

	/* Verifica��o do status */
	ie_status_w		:= 'F'; /* Inicia como Sem Fluxo */

	/* Cursor do fluxo de an�lise da ocorr�ncia */
	ie_permissao_grupo_w	:= 'N';
	ds_grupos_analise_w	:= null;
	qt_concluida_w		:= 0;
	qt_grupos_w		:= 0;
	
	for r_c03_w in C03(r_c02_w.nr_sequencia) loop
		begin
		if	(r_c03_w.nr_seq_grupo = nr_seq_grupo_atual_p) then
			ie_permissao_grupo_w	:= 'S';
		end if;

		qt_grupos_w	:= qt_grupos_w + 1;
		
		/* Se tem algum grupo ainda pendente, o status fica pendente de an�lise*/
		if	(r_c03_w.ie_status = 'P') then
			ie_status_w	:= 'P';
		else
			qt_concluida_w	:= qt_concluida_w + 1;
		end if;

		/* Apenas listagem descritiva dos grupos de an�lise da ocorr�ncia */
		if	(ds_grupos_analise_w is null) then
			ds_grupos_analise_w	:= r_c03_w.nm_grupo_auditor;
		else
			ds_grupos_analise_w	:= substr(ds_grupos_analise_w || ', ' || r_c03_w.nm_grupo_auditor, 1, 255);
		end if;
		end;
	end loop;
	
	if	(qt_grupos_w > 0) and
		(qt_concluida_w = qt_grupos_w) then
		if	(r_c02_w.ie_situacao = 'I') then
			if	(r_c02_w.ie_auditoria = 'N') then
				ie_status_w	:= 'N';
			else
				ie_status_w	:= 'I';
			end if;
		else
			if	(r_c02_w.ie_auditoria = 'N') then
				ie_status_w	:= 'N';
			else
				ie_status_w	:= 'C';
			end if;
		end if;
	else
		if	(r_c02_w.ie_auditoria = 'N') then
			ie_status_w	:= 'N';
		end if;
	end if;

	ds_status_w	:= null;--obter_valor_dominio(5186,ie_status_w);

	if	(ie_status_w is not null) then
		select	ds_valor_dominio
		into	ds_status_w
		from	valor_dominio_v
		where	cd_dominio	= 5186
		and	vl_dominio	= ie_status_w;
	end if;

	ie_selecionada_w	:= null;

	if	(r_c02_w.ie_situacao = 'A') then
		ie_selecionada_w	:= 'S';
	end if;

	nr_sequencia_w	:= nr_sequencia_w + 1;

	if	(r_c02_w.ie_forma_inativacao = 'U') then
		ds_forma_inativacao_w	:= 'Usu�rio';
	elsif	(r_c02_w.ie_forma_inativacao = 'S') then
		ds_forma_inativacao_w	:= 'Sistema';
	elsif	(r_c02_w.ie_forma_inativacao = 'US') then
		ds_forma_inativacao_w	:= 'Usu�rio e Sistema';
	end if;

	select	decode(count(1),0, 'N', 'S')
	into	ie_anexo_w
	from	pls_ocorrencia_anexo	a
	where	a.nr_seq_ocorrencia	= r_c02_w.nr_seq_ocorrencia
	and	rownum			= 1;

	insert into w_pls_analise_glosa_ocor
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_analise,
		nr_seq_conta,
		nr_seq_conta_proc,
		nr_seq_conta_mat,
		nr_seq_proc_partic,
		nr_seq_ocorrencia,
		nr_seq_motivo_glosa,
		ds_glosa_ocorrencia,
		nr_seq_ocor_benef,
		nr_seq_conta_glosa,
		ie_situacao,
		ie_status,
		cd_ocorrencia,
		ds_tipo,
		ds_glosa_vinc,
		ie_fechar_conta,
		nr_nivel_liberacao,
		ds_status,
		ds_nivel_liberacao,
		ds_pagamento,
		ds_faturamento,
		ie_permissao_grupo,
		ds_grupos_analise,
		ie_finalizar_analise,
		ds_observacao,
		ie_selecionada,
		ie_forma_inativacao,
		ds_forma_inativacao,
		ie_anexo,
		nr_seq_oc_cta_comb)
	values	(nr_sequencia_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_analise_p,
		r_c02_w.nr_seq_conta,
		r_c02_w.nr_seq_conta_proc,
		r_c02_w.nr_seq_conta_mat,
		r_c02_w.nr_seq_proc_partic,
		r_c02_w.nr_seq_ocorrencia,
		nr_seq_motivo_glosa_w,
		r_c02_w.ds_ocorrencia,
		r_c02_w.nr_sequencia,
		nr_seq_conta_glosa_w,
		r_c02_w.ie_situacao,
		ie_status_w,
		r_c02_w.cd_ocorrencia,
		ds_tipo_w,
		ds_glosa_vinc_w,
		r_c02_w.ie_fechar_conta,
		nr_nivel_liberacao_w,
		ds_status_w,
		ds_nivel_liberacao_w,
		ds_pagamento_w,
		ds_faturamento_w,
		ie_permissao_grupo_w,
		ds_grupos_analise_w,
		decode(r_c02_w.ie_finalizar_analise,'S',null,r_c02_w.ie_finalizar_analise),
		r_c02_w.ds_observacao,
		ie_selecionada_w,
		r_c02_w.ie_forma_inativacao,
		ds_forma_inativacao_w,
		ie_anexo_w,
		r_c02_w.nr_seq_oc_cta_comb);
	end;
end loop;

commit;

end pls_gerar_w_analise_glosa_ana;
/