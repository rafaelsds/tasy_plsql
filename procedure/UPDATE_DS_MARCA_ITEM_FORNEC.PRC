create or replace
procedure update_ds_marca_item_fornec(ds_marca_p		varchar2,
				vl_original_p		number,
				nr_sequencia_p		number) is 
begin

update	reg_lic_item_fornec
set	ds_marca = ds_marca_p,
	vl_original = vl_original_p
where	nr_sequencia = nr_sequencia_p;

commit;

end update_ds_marca_item_fornec;
/