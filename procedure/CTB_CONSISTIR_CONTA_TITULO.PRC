Create or Replace
Procedure CTB_Consistir_Conta_Titulo(	cd_conta_contabil_p	varchar2,
				ds_campo_p		varchar2) is

ie_tipo_w				varchar2(01);
ds_erro_w			varchar2(2000);
ds_conta_w			varchar(255);
qt_conta_w			number(05,0);

BEGIN
if	(cd_conta_contabil_p is not null) then
	select	count(*),
		max(ie_tipo),
		max(ds_conta_contabil)
	into	qt_conta_w,
		ie_tipo_w,
		ds_conta_w
	from	conta_contabil
	where	cd_conta_contabil	= cd_conta_contabil_p;

	if	(ie_tipo_w = 'T') and
		(qt_conta_w > 0) then
		if	(ds_campo_p is not null) then
			/*ds_erro_w	:= substr('O campo ' || ds_campo_p,1,2000);
			ds_erro_w	:= substr(ds_erro_w || ' Conta: ' || cd_conta_contabil_p || ' ' || ds_conta_w,1,2000);*/
			ds_erro_w	:= substr(wheb_mensagem_pck.get_texto(303124,'DS_CAMPO_P='||ds_campo_p),1,2000);
			ds_erro_w	:= substr(ds_erro_w || wheb_mensagem_pck.get_texto(303128,'CD_CONTA_CONTABIL_P='||cd_conta_contabil_p||';DS_CONTA_W='||ds_conta_w),1,2000);
		end if;
		wheb_mensagem_pck.exibir_mensagem_abort(266435,'DS_ERRO_W=' || ds_erro_w);
	end if;
end if;

end CTB_Consistir_Conta_Titulo;
/