create or replace
procedure eup_verifica_orientacao_js(
					nr_atendimento_p	number,
					cd_convenio_p		number,
					ie_origem_proced_p	number,
					cd_procedimento_p	number,
					cd_material_exame_p	varchar2,
					ie_mostra_obs_tecnica_p	varchar2,
					nr_seq_exame_p		number,
					nr_prescricao_p		number,
					ie_orien_formatada_p	varchar2,
					nr_seq_proc_interno_p	number,
					nm_usuario_p		varchar2,
					ds_orientacao_p	    out	varchar2,
					ie_orientacao_eup_p out varchar2) is
nr_seq_orientacao_w			number(10);
ds_orientacao_w				long			:= '';	
ds_orientacao_usuario_w		varchar2(4000) 	:= '';		
ie_orientacao_eup_w			varchar2(1) 	:= 'S';
Cursor c01 is
	select	nr_sequencia
	from 	proc_interno_orientacao 
	where 	nr_seq_proc_interno = nr_seq_proc_interno_p
	and 	ie_tipo_orientacao = 'C';
begin
if	(ie_orien_formatada_p = 'S') then
		open c01;
		loop
		fetch c01 into nr_seq_orientacao_w;
		exit when c01%notfound;
		begin
				select	ds_orientacao
				into	ds_orientacao_w
				from 	proc_interno_orientacao 
				where 	nr_sequencia = nr_seq_orientacao_w;
				ds_orientacao_p	:= ds_orientacao_p || ' ' || ds_orientacao_w;
		end;
		end loop;
		close c01;
			goto final;
else 
	if	(nvl(nr_seq_exame_p,0) > 0) then
		ds_orientacao_w := substr(obter_orientacao_exame_id_sexo(nr_seq_exame_p, nr_prescricao_p, wheb_usuario_pck.get_cd_estabelecimento, ie_mostra_obs_tecnica_p, null, null, null),1,2000);		
		ds_orientacao_w := ds_orientacao_w || substr(obter_orientacao_exame_id_sexo(nr_seq_exame_p, nr_prescricao_p, wheb_usuario_pck.get_cd_estabelecimento, ie_mostra_obs_tecnica_p, null, null, null),2001,4000);		
		ds_orientacao_w := ds_orientacao_w ||  substr(obter_orientacao_exame_id_sexo(nr_seq_exame_p, nr_prescricao_p, wheb_usuario_pck.get_cd_estabelecimento, 'N','N','S', null),1,2000);		
		ds_orientacao_usuario_w := substr(obter_orientacao_exame_id_sexo(nr_seq_exame_p, nr_prescricao_p, wheb_usuario_pck.get_cd_estabelecimento,'N', 'S','N','N'),1,4000);		
		if	(ds_orientacao_w is null) then			
			ds_orientacao_w := substr(obter_orientacao_exame(nr_seq_exame_p, cd_material_exame_p, ie_mostra_obs_tecnica_p),1,2000);			
			ds_orientacao_w :=  ds_orientacao_w || substr(obter_orientacao_exame(nr_seq_exame_p, cd_material_exame_p, ie_mostra_obs_tecnica_p),2001,4000);
		end if;
		if	(ds_orientacao_usuario_w is not null) then			
			ds_orientacao_w := substr(ds_orientacao_w || ds_orientacao_usuario_w,1,4000);			
		end if;
	else 
		ds_orientacao_w := substr(Obter_Orientacao_Proc(cd_procedimento_p, ie_origem_proced_p, cd_convenio_p, nr_seq_proc_interno_p, nr_atendimento_p, 0),1,4000);
	end if;	
	
	select	(nvl(max(ie_orientacao_eup),'S'))
	into	ie_orientacao_eup_w
	from	exame_laboratorio
	where 	nr_seq_exame = nr_seq_exame_p;

end if;
ie_orientacao_eup_p := ie_orientacao_eup_w;
ds_orientacao_p := ds_orientacao_w;
<<final>>
commit;
end eup_verifica_orientacao_js;
/