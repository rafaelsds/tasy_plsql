create or replace
procedure atualizar_tipo_baixa_neg(
		nr_titulo_p 		number,
		cd_tipo_baixa_neg_p	number) is 
begin
if	(nr_titulo_p is not null) then
	begin
	update 	titulo_pagar
	set    	cd_tipo_baixa_neg = cd_tipo_baixa_neg_p
	where  	nr_titulo = nr_titulo_p;
	commit;
	end;
end if;
end atualizar_tipo_baixa_neg;
/