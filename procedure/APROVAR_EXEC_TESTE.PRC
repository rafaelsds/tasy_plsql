create or replace
procedure Aprovar_Exec_Teste(
			nm_usuario_p		Varchar2,
			nr_seq_p		number) is 

begin
update	teste_software_execucao          
set	dt_aprovacao = sysdate,     
	dt_atualizacao = sysdate,     
	nm_usuario_aprov  = nm_usuario_p, 
	nm_usuario = nm_usuario_p  
where    	nr_sequencia = nr_seq_p;      

commit;

end Aprovar_Exec_Teste;
/