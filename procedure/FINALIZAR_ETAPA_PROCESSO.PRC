create or replace
procedure finalizar_etapa_processo(nm_usuario_p			Varchar2,
									nr_processo_p	 	number) is
									
ie_ordens_nao_prep_area_w varchar2(1);
nr_seq_ordem_w				number(10);
nr_seq_area_prep_w			number(10);
ie_grava_log_gedipa_w			varchar2(15);

Cursor C01 is
	select	a.nr_sequencia,
			b.nr_seq_area_prep
	from	prescr_mat_hor b,
			adep_processo_frac a
	where	b.nr_seq_etiqueta = a.nr_sequencia
	and		a.nr_seq_processo = nr_processo_p
	and		obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';									

begin

select	nvl(max(ie_grava_log_gedipa),'S')
into	ie_grava_log_gedipa_w
from	parametros_farmacia
where	cd_estabelecimento = nvl(wheb_usuario_pck.get_cd_estabelecimento,1); 

open c01;
loop
fetch c01 into
	nr_seq_ordem_w,
	nr_seq_area_prep_w;
exit when c01%notfound; 
	begin
	nr_seq_ordem_w := nr_seq_ordem_w ;
	pemof_iniciar_finalizar_ordem(nr_seq_ordem_w,null,'F',nm_usuario_p);
	end;
end loop;
close c01;

select	decode(count(*),0,'N','S')
into	ie_ordens_nao_prep_area_w
from	adep_processo_frac x,
		prescr_mat_hor y
where	y.nr_seq_etiqueta	= x.nr_sequencia
and		y.nr_seq_processo	= x.nr_seq_processo
and		x.nr_seq_processo	= nr_processo_p
and		x.dt_fim_preparo	is null
and		y.dt_suspensao 		is null;				

if	(ie_ordens_nao_prep_area_w = 'N') then
	atualiza_status_processo_adep(nr_processo_p, nr_seq_area_prep_w	, 'G', 'P', SYSDATE, nm_usuario_p);
end if;

if	(ie_grava_log_gedipa_w = 'S') then
	insert into log_gedipa 
		(nr_sequencia, 
		dt_log, 
		nr_log, 
		nm_objeto_execucao, 
		nm_objeto_chamado, 
		ds_parametros, 
		ds_log,
		nr_seq_processo)
	values (obter_nextval_sequence('log_gedipa'), 
		sysdate, 
		2000, 
		'FINALIZAR_ETAPA_PROCESSO', 
		null,
		null,
		substr('NR_SEQ_PROCESSO= '||nr_processo_p|| ' - nr_seq_ordem_w= ' || nr_seq_ordem_w 
					  || ' - nr_seq_area_prep_w= ' || nr_seq_area_prep_w ||
			' - NM_USUARIO= ' || wheb_usuario_pck.get_nm_usuario || ' - FUNCAO= ' || obter_funcao_ativa,1,200),
		nr_processo_p);
end if;


commit;

end finalizar_etapa_processo;
/