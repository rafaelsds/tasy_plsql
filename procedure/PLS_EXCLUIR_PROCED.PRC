create or replace
procedure pls_excluir_proced(nm_usuario_p		Varchar2) is 

begin
	if	(nm_usuario_p is not null) then
		begin
			delete from w_pls_conta_proc where nm_usuario = nm_usuario_p;
		end;
	end if;

commit;

end pls_excluir_proced;
/