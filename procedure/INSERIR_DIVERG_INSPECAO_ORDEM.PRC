create or replace
procedure inserir_diverg_inspecao_ordem(	cd_estabelecimento_p	number,
						nr_seq_registro_p	number,
						nr_seq_inspecao_p	number,
						nr_ordem_compra_p	number,
						nr_item_oci_p		number,
						cd_tipo_divergencia_p	varchar2,
						nm_usuario_p		varchar2,
						nr_seq_regra_p number default null) is 

qt_registros_w			number(10);						
						
begin

select	count(*)
into	qt_registros_w
from	diverg_inspecao_ordem
where	cd_tipo_divergencia = cd_tipo_divergencia_p
and	nr_seq_inspecao = nr_seq_inspecao_p;

if	(qt_registros_w = 0) then

	insert into diverg_inspecao_ordem(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_registro,
		nr_seq_inspecao,
		nr_ordem_compra,
		nr_item_oci,
		cd_tipo_divergencia,
		nr_seq_regra)
	values(	diverg_inspecao_ordem_seq.nextval,
		cd_estabelecimento_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_registro_p,
		nr_seq_inspecao_p,
		nr_ordem_compra_p,
		nr_item_oci_p,
		cd_tipo_divergencia_p,
		nr_seq_regra_p);
end if;

commit;

end inserir_diverg_inspecao_ordem;
/