create or replace
PROCEDURE verifica_proced_dupl_dia(	nr_seq_derivado_p		number,
					nr_prescricao_p		number,
					nm_usuario_p		varchar2,
					ds_retorno_p	Out	Varchar2) is
cd_pessoa_fisica_w	varchar2(10);
dt_prescricao_w		date;
nr_prescricao_w		number(14);
nm_medico_w		varchar2(60);
ie_suspenso_w		Varchar(10);
ie_consitir_susp_w	Varchar(10);
cd_estabelecimento_w	number(15);
nr_atendimento_w	number(15);

cursor	C01 is
select	a.nr_prescricao,
	substr(obter_nome_medico(b.cd_medico,'N'),1,60),
	decode(a.ie_suspenso,'S','SUSPENSO','')
from	prescr_procedimento	a,
	prescr_medica		b
where	a.nr_prescricao		= b.nr_prescricao
and	b.nr_prescricao		<> nr_prescricao_p
and	a.nr_seq_derivado	= nr_seq_derivado_p
and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w
and	b.nr_atendimento	= nr_atendimento_w
and	trunc(b.dt_prescricao)	= trunc(dt_prescricao_w)
and	a.nr_seq_derivado is not null
and	((ie_consitir_susp_w	= 'S') or
	 (nvl(a.ie_suspenso,'N') = 'N'));

begin

Obter_Param_Usuario(924, 307, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, ie_consitir_susp_w);

select	dt_prescricao,
	cd_pessoa_fisica,
	cd_estabelecimento,
	nr_atendimento
into	dt_prescricao_w,
	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	nr_atendimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

open	C01;
loop
fetch	C01 into
	nr_prescricao_w,
	nm_medico_w,
	ie_suspenso_w;
exit when C01%notfound;
	ds_retorno_p:= substr(ds_retorno_p || ie_suspenso_w || ' ' || nr_prescricao_w || ' ' || nm_medico_w || chr(10) || chr(13),1,255);	
end loop;
close C01;

end verifica_proced_dupl_dia;
/
