create or replace
procedure GERAR_PRE_REP_PROC(	nr_prescricao_orig_p	number,
								nr_prescricao_p			number,
								nm_usuario_p			Varchar2) is 

nr_sequencia_w				number(6,0);
nr_seq_proc_ant_w			number(15,0);
cd_setor_atendimento_w		number(10);
nr_agrupamento_w			number(6,0);
ie_executar_leito_w			varchar2(1);
varie_verifica_setor_w		varchar2(1);
ie_amostra_w				varchar2(1);
cd_procedimento_w			number(15,0);
cd_estabelecimento_w		number(15,0);
qt_procedimento_w			number(8,3);
ds_horarios_w				varchar2(2000);
ds_observacao_w				varchar2(2000);
ds_observacao_enf_w			varchar2(2000);
cd_motivo_baixa_w			number(3,0);
dt_prescricao_w				date;
dt_validade_prescr_w		date;
nr_atendimento_w			number(15,0);
hr_setor_w					varchar2(10);
dt_baixa_w					date;
cd_procedimento_aih_w		varchar2(15);
cd_intervalo_param_w		varchar2(7);
cd_intervalo_proc_agora_w	varchar2(7);
ie_origem_proced_w			number(10,0);
cd_intervalo_w				varchar2(7);
ie_urgencia_w				varchar2(1);
ie_regra_medic_w			varchar2(15);
dt_emissao_setor_atend_w 	date;
ds_dado_clinico_w			varchar2(2000);
ie_suspenso_w				varchar2(1);
cd_material_exame_w			varchar2(20);
nr_seq_exame_w				number(10);
ds_material_especial_w		varchar2(255);
ie_status_atend_w			number(2);
ie_origem_inf_w				varchar2(1);
ie_se_necessario_w			varchar2(1);
ie_acm_w					varchar2(1);
nr_ocorrencia_w				number(15,4);
nr_seq_proc_interno_w		number(10,0);
qt_peca_ap_w				number(3);
ds_qualidade_peca_ap_w		varchar2(2000);
ds_diag_provavel_ap_w		varchar2(255);
ds_exame_anterior_ap_w		varchar2(255);
nr_seq_derivado_w			number(10);
nr_seq_exame_sangue_w		number(10);
nr_seq_solic_sangue_w		number(10);
cd_unid_med_sangue_w		varchar2(5);
cd_pessoa_coleta_w			varchar2(10);
dt_coleta_w					date;
dt_prev_execucao_ww			date;
ie_avisar_result_w			varchar2(1);
qt_vol_hemocomp_w			number(15);
nr_seq_prot_glic_w			number(10);
ie_respiracao_w				varchar2(15);
cd_mod_vent_w				varchar2(15);
ie_disp_resp_esp_w			varchar2(15);
qt_fluxo_oxigenio_w			number(15,4);
ds_rotina_w					varchar2(80);
ie_autorizacao_w			varchar2(3);
ie_lado_w					varchar2(1);
nr_seq_proc_w				number(15,0);
nr_seq_interno_w			number(10,0);
ie_copiar_w					varchar2(10);
ie_regra_geral_w			varchar2(10);
nr_seq_regra_w				number(15);
nr_seq_proc_interno_aux_w	number(15);
cd_intervalo_aux_w			varchar2(50);
ie_proced_agora_w			varchar2(50);
ie_regra_prim_hor_proc_w	varchar2(50);
dt_validade_origem_w		date;
dt_validade_nova_w			date;
hr_prim_horario_proc_w		varchar2(10);
cd_intervalo_proc_w			varchar2(50);
hr_inicio_setor_w			varchar2(50);
dt_prev_execucao_w			date;
ie_situacao_w				varchar2(50);
nr_prescricoes_w			varchar2(255);
dt_inicio_prescr_www		date;
dt_inicio_prescr_ww			date;
dt_inicio_prescr_w			date;
dt_primeiro_horario_w		date;
dt_prescricao_www			date;
nr_horas_validade_w			number(15,0);
qt_hora_intervalo_w			number(2,0);
qt_min_intervalo_w			number(5,0);
nr_seq_proc_int_w			number(15,0);
nr_intervalo_ww				number(15,0);
nr_intervalo_w				number(15,0);
ds_horarios_ww				varchar2(2000);
ds_horarios2_w				varchar2(2000);
ds_erro_w					varchar2(2000);
ie_setor_paciente_w			varchar2(07);
ie_forma_exame_w			varchar2(1);
ie_restring_interv_lib_w	varchar2(1);
ds_horarios_aux_w			varchar2(2000);
ie_regra_horarios_w			varchar2(15);
dt_horario_w				date;
qt_operacao_w				number(15,4);
ie_operacao_w				varchar2(15);
ie_reordenar_w				varchar2(1);
ds_justificativa_w			varchar2(4000);
ie_copiar_justificativa_w 	varchar2(1) := 'N';
cont_peca_w					number(10);

cursor c01 is
select	a.nr_sequencia,
		nvl(a.nr_agrupamento,1),
		a.ie_executar_leito,
		'N',
		a.cd_procedimento,
		a.qt_procedimento,
		a.ds_horarios,
		a.ds_observacao,
		a.ds_observacao_enf,
		0,
		null,
		null,
		a.ie_origem_proced,
		a.cd_intervalo,
		'N',
		decode(VarIe_verifica_setor_w, 'N', a.cd_setor_atendimento, nvl(obter_setor_atend_proc(cd_estabelecimento_w, a.cd_procedimento, a.ie_origem_proced, null, a.cd_setor_atendimento, null, a.nr_seq_proc_interno, nr_atendimento_w), a.cd_setor_atendimento)),
		null,
		a.ds_dado_clinico,
		'N',
		a.cd_material_exame,
		a.nr_seq_exame,
		a.ds_material_especial,
		5,
		a.ie_origem_inf,
		nvl(a.ie_se_necessario,'N'),
		nvl(a.ie_acm,'N'),
		a.nr_ocorrencia,
		a.nr_seq_proc_interno,
		a.qt_peca_ap,
		a.ds_qualidade_peca_ap,
		a.ds_diag_provavel_ap,
		a.ds_exame_anterior_ap,
		a.nr_seq_derivado,
		a.nr_seq_exame_sangue,
		a.nr_seq_solic_sangue,
		a.cd_unid_med_sangue,
		a.cd_pessoa_coleta,
		null,
		'N',
		a.qt_vol_hemocomp,
		a.nr_seq_prot_glic,
		a.ie_respiracao,
		a.cd_mod_vent,
		a.ie_disp_resp_esp,
		a.qt_fluxo_oxigenio,
		a.ds_rotina,
		a.qt_hora_intervalo,
		a.qt_min_intervalo,
		'L',
		a.ie_lado,
		a.ie_forma_exame,
		a.ds_justificativa
from	prescr_procedimento a
where	a.nr_prescricao	= nr_prescricao_orig_p
and		a.ie_suspenso	<> 'S'
and		nr_atendimento_w is not null
and		exists (select 1 from w_item_pre_rep b where b.nr_atendimento = nr_atendimento_w and b.cd_item = a.cd_procedimento and b.ie_tipo_item = 'PROC');

begin

select	max(cd_estabelecimento),
		max(nr_atendimento),
		max(cd_setor_atendimento),
		max(dt_inicio_prescr),
		max(dt_validade_prescr),
		max(nr_horas_validade),
		max(dt_prescricao)
into	cd_estabelecimento_w,
		nr_atendimento_w,
		cd_setor_atendimento_w,
		dt_inicio_prescr_w,
		dt_validade_prescr_w,
		nr_horas_validade_w,
		dt_prescricao_www
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	max(to_char(nvl(hr_inicio_prescricao,sysdate),'hh24:mi'))
into	hr_inicio_setor_w
from	setor_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_w;

begin
	dt_prescricao_w	:= to_date(to_char(sysdate,'dd/mm/yyyy ') || hr_inicio_setor_w || ':00','dd/mm/yyyy hh24:mi:ss');
exception when others then
	dt_prescricao_w	:= sysdate;
end;

Obter_Param_Usuario(924,35,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,cd_intervalo_param_w);
Obter_Param_Usuario(924,259,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_regra_medic_w);
Obter_Param_Usuario(924,718,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w, VarIe_verifica_setor_w);
Obter_Param_Usuario(924,1120,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w, ie_restring_interv_lib_w);

select	nvl(max(nr_sequencia),0)
into	nr_seq_proc_w
from	prescr_Procedimento
where	nr_prescricao = nr_prescricao_p;

open C01;
loop
fetch C01 into
	nr_sequencia_w,
	nr_agrupamento_w,
	ie_executar_leito_w,
	ie_amostra_w,
	cd_procedimento_w,
	qt_procedimento_w,
	ds_horarios_w,
	ds_observacao_w,
	ds_observacao_enf_w,
	cd_motivo_baixa_w,
	dt_baixa_w,
	cd_procedimento_aih_w,
	ie_origem_proced_w,
	cd_intervalo_w,
	ie_urgencia_w,
	cd_setor_atendimento_w,
	dt_emissao_setor_atend_w,
	ds_dado_clinico_w,
	ie_suspenso_w,
	cd_material_exame_w,
	nr_seq_exame_w,
	ds_material_especial_w,
	ie_status_atend_w,
	ie_origem_inf_w,
	ie_se_necessario_w,
	ie_acm_w,
	nr_ocorrencia_w,
	nr_seq_proc_interno_w,
	qt_peca_ap_w,
	ds_qualidade_peca_ap_w,
	ds_diag_provavel_ap_w,
	ds_exame_anterior_ap_w,
	nr_seq_derivado_w,
	nr_seq_exame_sangue_w,
	nr_seq_solic_sangue_w,
	cd_unid_med_sangue_w,
	cd_pessoa_coleta_w,
	dt_coleta_w,
	ie_avisar_result_w,
	qt_vol_hemocomp_w,
	nr_seq_prot_glic_w,
	ie_respiracao_w,
	cd_mod_vent_w,
	ie_disp_resp_esp_w,
	qt_fluxo_oxigenio_w,
	ds_rotina_w,
	qt_hora_intervalo_w,
	qt_min_intervalo_w,
	ie_autorizacao_w,
	ie_lado_w,
	ie_forma_exame_w,
	ds_justificativa_w;
exit when C01%notfound;
	
	nr_seq_proc_ant_w	:= nr_sequencia_w;
	nr_sequencia_w		:= nr_sequencia_w + nr_seq_proc_w;

	IF	(NVL(nr_seq_proc_interno_w,0) > 0) THEN
		SELECT	NVL(MAX(ie_setor_paciente),'N')
		INTO	ie_setor_paciente_w
		FROM	proc_interno
		WHERE	nr_sequencia	= nr_seq_proc_interno_w;

		IF	(ie_setor_paciente_w = 'S') THEN
			cd_setor_atendimento_w	:= obter_setor_atendimento(nr_atendimento_w);
		END IF;
	END IF;		
	
	if	(nr_seq_prot_glic_w is not null) then
		select	nvl(max(obter_intervalo_ccg(nr_seq_prot_glic)), cd_intervalo_w)
		into	cd_intervalo_w
		from	prescr_procedimento
		where	nvl(ie_urgencia,'N') <> 'N' -- Caso o CIG/CCG seja agora, deve buscar o intervalo do protocolo na c�pia
		and		nr_sequencia	= nr_seq_proc_ant_w
		and		nr_prescricao	= nr_prescricao_orig_p;
	end if;
	
	select	max(cd_intervalo)
	into	cd_intervalo_w
	from	intervalo_prescricao
	where	ie_agora	= 'S'
	and		ie_situacao	= 'A'
	and 	Obter_se_intervalo(cd_intervalo,'P') = 'S'
	and 	obter_se_exibe_intervalo(nr_prescricao_p, cd_intervalo, cd_setor_atendimento_w) = 'S';

	select	prescr_procedimento_seq.NextVal
	into	nr_seq_interno_w
	from	dual;

	Insert  into Prescr_Procedimento (
		nr_prescricao,
		nr_sequencia,
		nr_agrupamento,
		ie_executar_leito,
		ie_amostra,
		cd_procedimento,
		qt_procedimento,
		dt_atualizacao,
		nm_usuario,
		ds_horarios,
		ds_observacao,
		ds_observacao_enf,
		cd_motivo_baixa,
		dt_baixa,
		cd_procedimento_aih,
		ie_origem_proced,
		cd_intervalo,
		ie_urgencia,
		cd_setor_atendimento,
		dt_emissao_setor_atend,
		ds_dado_clinico,
		dt_prev_execucao,
		ie_suspenso,
		cd_material_exame,
		nr_seq_exame,
		ds_material_especial,
		ie_status_atend,
		ie_origem_inf,
		ie_se_necessario,
		ie_acm,
		nr_ocorrencia,
		nr_seq_interno,
		nr_seq_proc_interno,
		qt_peca_ap,
		ds_qualidade_peca_ap,
		ds_diag_provavel_ap,
		ds_exame_anterior_ap,
		nr_seq_derivado,
		nr_seq_exame_sangue,
		nr_seq_solic_sangue,
		cd_unid_med_sangue,
		cd_pessoa_coleta,
		dt_coleta,
		ie_avisar_result,
		qt_vol_hemocomp,
		nr_seq_prot_glic,
		ie_respiracao,
		cd_mod_vent,
		ie_disp_resp_esp,
		qt_fluxo_oxigenio,
		ds_rotina,
		qt_hora_intervalo,
		qt_min_intervalo,
		ie_autorizacao,
		ie_lado,
		nr_seq_anterior,
		nr_prescricao_original,
		ie_forma_exame,
		ds_justificativa)
	values	(nr_prescricao_p,
		nr_sequencia_w,
		nr_agrupamento_w,
		ie_executar_leito_w,
		ie_amostra_w,
		cd_procedimento_w,
		qt_procedimento_w,
		sysdate,
		nm_usuario_p,
		to_char(sysdate, 'hh24:mi'),
		ds_observacao_w,
		ds_observacao_enf_w,
		cd_motivo_baixa_w,
		dt_baixa_w,
		cd_procedimento_aih_w,
		ie_origem_proced_w,
		cd_intervalo_w,
		'S',
		cd_setor_atendimento_w,
		dt_emissao_setor_atend_w,
		ds_dado_clinico_w,
		trunc(sysdate, 'mi'),
		ie_suspenso_w,
		cd_material_exame_w,
		nr_seq_exame_w,
		ds_material_especial_w,
		ie_status_atend_w,
		ie_origem_inf_w,
		'N',
		'N',
		nr_ocorrencia_w,
		nr_seq_interno_w,
		nr_seq_proc_interno_w,
		qt_peca_ap_w,
		ds_qualidade_peca_ap_w,
		ds_diag_provavel_ap_w,
		ds_exame_anterior_ap_w,
		nr_seq_derivado_w,
		nr_seq_exame_sangue_w,
		nr_seq_solic_sangue_w,
		cd_unid_med_sangue_w,
		cd_pessoa_coleta_w,
		dt_coleta_w,
		ie_avisar_result_w,
		qt_vol_hemocomp_w,
		nr_seq_prot_glic_w,
		ie_respiracao_w,
		cd_mod_vent_w,
		ie_disp_resp_esp_w,
		qt_fluxo_oxigenio_w,
		ds_rotina_w,
		qt_hora_intervalo_w,
		qt_min_intervalo_w,
		ie_autorizacao_w,
		ie_lado_w,
		nr_seq_proc_ant_w,
		nr_prescricao_orig_p,
		ie_forma_exame_w,
		ds_justificativa_w);		

end loop;
close C01;

commit;

end GERAR_PRE_REP_PROC;
/