create or replace
procedure pls_alimenta_estab_monit is 

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
tb_nr_sequencia_w	pls_util_cta_pck.t_number_table;
i			pls_integer;

Cursor C01 is
	select	nr_sequencia
	from	pls_monitor_tiss_lote_com;
	
Cursor C02 is
	select	nr_sequencia
	from	pls_monitor_tiss_reg_gpo;
	
Cursor C03 is
	select	nr_sequencia
	from	pls_regra_tabela_tiss;
	
Cursor C04 is
	select	nr_sequencia
	from	pls_monitor_tiss_param;

begin
i := 0;
cd_estabelecimento_w := pls_obter_cd_estab_ops;

--pls_monitor_tiss_lote_com
for r_C01_w in C01 loop
	
	tb_nr_sequencia_w(i) := r_C01_w.nr_sequencia;
	
	if	(tb_nr_sequencia_w.count >= pls_util_pck.qt_registro_transacao_w) then
		forall j in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	pls_monitor_tiss_lote_com set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(j);
		commit;
		
		tb_nr_sequencia_w.delete;
		i := 0;
	else	
		i := i + 1;	
	end if;
end loop;

if	(tb_nr_sequencia_w.count > 0) then
	forall j in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
		update	pls_monitor_tiss_lote_com set
			cd_estabelecimento = cd_estabelecimento_w
		where	nr_sequencia = tb_nr_sequencia_w(j);
	commit;
	
	tb_nr_sequencia_w.delete;
	i := 0;
end if;

--pls_monitor_tiss_reg_gpo
for r_C02_w in C02 loop
	
	tb_nr_sequencia_w(i) := r_C02_w.nr_sequencia;
	
	if	(tb_nr_sequencia_w.count >= pls_util_pck.qt_registro_transacao_w) then
		forall j in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	pls_monitor_tiss_reg_gpo set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(j);
		commit;
		
		tb_nr_sequencia_w.delete;
		i := 0;
	else	
		i := i + 1;	
	end if;
end loop;

if	(tb_nr_sequencia_w.count > 0) then
	forall j in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
		update	pls_monitor_tiss_reg_gpo set
			cd_estabelecimento = cd_estabelecimento_w
		where	nr_sequencia = tb_nr_sequencia_w(j);
	commit;
	
	tb_nr_sequencia_w.delete;
	i := 0;
end if;

--pls_regra_tabela_tiss
for r_C03_w in C03 loop
	
	tb_nr_sequencia_w(i) := r_C03_w.nr_sequencia;
	
	if	(tb_nr_sequencia_w.count >= pls_util_pck.qt_registro_transacao_w) then
		forall j in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	pls_regra_tabela_tiss set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(j);
		commit;
		
		tb_nr_sequencia_w.delete;
		i := 0;
	else	
		i := i + 1;	
	end if;
end loop;

if	(tb_nr_sequencia_w.count > 0) then
	forall j in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
		update	pls_regra_tabela_tiss set
			cd_estabelecimento = cd_estabelecimento_w
		where	nr_sequencia = tb_nr_sequencia_w(j);
	commit;
	
	tb_nr_sequencia_w.delete;
	i := 0;
end if;

--pls_monitor_tiss_param
for r_C04_w in C04 loop
	
	tb_nr_sequencia_w(i) := r_C04_w.nr_sequencia;
	
	if	(tb_nr_sequencia_w.count >= pls_util_pck.qt_registro_transacao_w) then
		forall j in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
			update	pls_monitor_tiss_param set
				cd_estabelecimento = cd_estabelecimento_w
			where	nr_sequencia = tb_nr_sequencia_w(j);
		commit;
		
		tb_nr_sequencia_w.delete;
		i := 0;
	else	
		i := i + 1;	
	end if;
end loop;

if	(tb_nr_sequencia_w.count > 0) then
	forall j in tb_nr_sequencia_w.first .. tb_nr_sequencia_w.last
		update	pls_monitor_tiss_param set
			cd_estabelecimento = cd_estabelecimento_w
		where	nr_sequencia = tb_nr_sequencia_w(j);
	commit;
	
	tb_nr_sequencia_w.delete;
	i := 0;
end if;


end pls_alimenta_estab_monit;
/