CREATE OR REPLACE PROCEDURE copiar_proced_diagnotisco (
  qt_chave_p	NUMBER,
  nr_atendimento_cih_w NUMBER)
IS
nr_seq_procedimento_pac_w PROCEDIMENTO_PAC_MEDICO.nr_sequencia%type;
BEGIN

  SELECT PROCEDIMENTO_PAC_MEDICO_SEQ.nextval 
  INTO nr_seq_procedimento_pac_w 
  FROM dual;

  INSERT INTO PROCEDIMENTO_PAC_MEDICO(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_atendimento,
			ie_situacao,
			dt_liberacao,
			dt_inativacao,
			nm_usuario_inativacao,
			ds_justificativa,
			cd_procedimento,
			ie_origem_proced,
			qt_procedimento,
			dt_procedimento,
			ie_lado,
			cd_departamento,
			nr_seq_propaci,
			cd_setor_atendimento,
			nr_seq_episodio,
			ie_proc_princ,
			nr_seq_proc_interno,
			ie_proc_adicional,
			nr_seq_pflegegrad,
			nr_seq_proc_hor,
			ie_carta_referencia,
			nr_seq_atend_cons_pepa,
			nr_cirurgia,
			ie_sist_ext_origem,
			ie_relevante_drg) 
    SELECT nr_seq_procedimento_pac_w, 
           proc_pac_descricao.dt_atualizacao,
           proc_pac_descricao.nm_usuario,
           proc_pac_descricao.dt_atualizacao_nrec,
           proc_pac_descricao.nm_usuario_nrec,
           proc_pac_descricao.nr_atendimento,
           proc_pac_descricao.ie_situacao,
           sysdate,
           proc_pac_descricao.dt_inativacao,
           proc_pac_descricao.nm_usuario_inativacao,
           proc_pac_descricao.ds_justificativa,
           proc_pac_descricao.cd_procedimento,
           proc_pac_descricao.ie_origem_proced,
           proc_pac_descricao.qt_procedimento,
           NVL(proc_pac_descricao.dt_inicio,proc_pac_descricao.dt_registro),
           NULL,
           NULL,
           NULL,
           obter_setor_atendimento(nr_atendimento_cih_w),
           Decode(obter_episodio_atendimento(nr_atendimento_cih_w),'0',null,obter_episodio_atendimento(nr_atendimento_cih_w)),
           NULL,
           proc_pac_descricao.nr_seq_proc_interno,
           NULL,
           NULL,
           NULL,
           NULL,
           proc_pac_descricao.nr_seq_atend_cons_pepa,
           NULL,
           NULL,
           NULL
           FROM proc_pac_descricao
           WHERE nr_sequencia = qt_chave_p;

END copiar_proced_diagnotisco;
/
