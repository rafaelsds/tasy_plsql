create or replace
procedure pls_gravar_log_envio_email_ted
			(	nr_seq_copartic_pagador_p	pls_ted_copartic_envio.nr_seq_ted_pagador%type,
				ds_observacao_p			pls_ted_copartic_envio.ds_observacao%type,
				nm_usuario_p			varchar2 ) is

nr_seq_lote_w			pls_ted_copartic_envio.nr_seq_lote%type;
ds_destino_w			pls_ted_copartic_envio.ds_destino%type;
ds_observacao_w			pls_ted_copartic_envio.ds_observacao%type;
nr_seq_pagador_w		pls_ted_copartic_pagador.nr_seq_pagador%type;

begin

select	nr_seq_lote,
	substr(pls_obter_dados_pagador(nr_seq_pagador,'M'),1,255),
	nr_seq_pagador
into	nr_seq_lote_w,
	ds_destino_w,
	nr_seq_pagador_w
from	pls_ted_copartic_pagador
where	nr_sequencia = nr_seq_copartic_pagador_p;

insert into pls_ted_copartic_envio
	      (	nr_sequencia,		nr_seq_lote, 		nr_seq_ted_pagador, 	dt_envio,
	        ds_destino,          	nr_seq_pagador,       	dt_atualizacao, 	nm_usuario,
		dt_atualizacao_nrec, 	nm_usuario_nrec,      	ds_observacao,  	nm_usuario_envio )
        values(	pls_ted_copartic_envio_seq.Nextval,	nr_seq_lote_w,	 	nr_seq_copartic_pagador_p,	sysdate,
		ds_destino_w, 				nr_seq_pagador_w, 	sysdate, 		nm_usuario_p,
		sysdate, 				nm_usuario_p, 		ds_observacao_p, 	nm_usuario_p );

update	pls_ted_copartic_pagador
set	ie_email_enviado = 'S'
where	nr_sequencia = nr_seq_copartic_pagador_p;

commit;

end pls_gravar_log_envio_email_ted;
/