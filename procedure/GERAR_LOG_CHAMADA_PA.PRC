create or replace
procedure gerar_log_chamada_pa ( nr_atendimento_p 	number,
				 nr_seq_local_chamado_p	number,
				 nm_usuario_p		Varchar2) is 
nr_sequencia_w	number(10);
begin

if 	(nr_atendimento_p 	is not null) and
	(nr_seq_local_chamado_p is not null) and
	(nm_usuario_p 		is not null) then
	begin
	
	select	log_chamada_pa_seq.nextval
	into 	nr_sequencia_w
	from	dual;
	
	insert	into	log_chamada_pa 
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_chamado,
			nr_atendimento,
			nr_seq_local_chamado,
			nm_usuario_chamado,
			ie_chamado)
		values	(
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_atendimento_p,
			nr_seq_local_chamado_p,
			nm_usuario_p,
			'N');
	commit;	
	end;
end if;


commit;

end gerar_log_chamada_pa;
/
