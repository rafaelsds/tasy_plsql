create or replace procedure pfcs_db_tl_req_by_unit_cenus(
    nr_seq_indicator_p   number,
    cd_estabelecimento_p number,
    nm_usuario_p         varchar2
) is

nr_seq_panel_w 					pfcs_panel.nr_sequencia%type;

/* telemetry Census = Patients on Tele / Capacity By Unit */
/* Cursor to read from pfcs integration tables */
cursor c01_pfcs is
	select 	ds_unit,
          sum(qt_patients_census) qt_patients_census,
          sum(nr_unit_capacity) nr_unit_capacity
    from
    (
    	select	sa.ds_Setor_atendimento ds_unit,
              count(dev.nr_sequencia) qt_patients_census,
              0 nr_unit_capacity
    	from  pfcs_service_request sr,
    		  pfcs_encounter enc,
              pfcs_patient pat,
              pfcs_encounter_location el,
              pfcs_device dev,
              unidade_atendimento uni,
              setor_atendimento sa
      where	((sr.si_status = 'COMPLETED' and sr.cd_service = 'E0404') or
            (sr.si_status = 'ACTIVE' and sr.cd_service = 'E0403'))
      and 	sr.nr_seq_encounter = enc.nr_sequencia
      and   dev.si_status = 'ACTIVE'
	  and	dev.ds_device_type = 'Monitor'
	  and 	pat.nr_sequencia = dev.nr_seq_patient
	  and 	pat.ie_active = '1'
	  and	enc.nr_seq_patient = pat.nr_sequencia
	  and	enc.si_status in ('PLANNED', 'ARRIVED')
	  and	el.nr_seq_encounter = enc.nr_sequencia
	  and 	uni.nr_seq_location = el.nr_seq_location
      and	uni.cd_setor_atendimento = sa.cd_setor_atendimento
      and 	sa.ie_situacao = 'A'
      and 	uni.ie_situacao = 'A'
      and 	sa.cd_classif_setor in ('1','3','4','9','11','12')
      and   sa.cd_estabelecimento_base = cd_estabelecimento_p
      group by sa.ds_Setor_atendimento, 0
      union all		
      select	sa.ds_Setor_atendimento ds_unit,
              0 qt_patients_census,
              sa.nr_unit_tl_capacity nr_unit_capacity
      from	  setor_atendimento sa
      where   sa.cd_estabelecimento_base = cd_estabelecimento_p
      and 	  sa.cd_classif_setor in ('1','3','4','9','11','12')
      and 	  sa.ie_situacao = 'A'
      group by sa.ds_setor_atendimento, sa.nr_unit_tl_capacity, 0
    )
   group by ds_unit;
begin
	for c01_w in c01_pfcs loop
	    pfcs_pck_v2.pfcs_generate_results(vl_indicator_p => c01_w.qt_patients_census,
			vl_indicator_aux_p => c01_w.nr_unit_capacity,
			ds_reference_value_p => c01_w.ds_unit,
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => cd_estabelecimento_p,
			nm_usuario_p => nm_usuario_p,
			nr_seq_panel_p => nr_seq_panel_w);
	end loop;
	commit;
	pfcs_pck_v2.pfcs_activate_records(nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p);

end pfcs_db_tl_req_by_unit_cenus;
/
