create or replace
procedure ptu_ped_insistencia_v50_ws ( 	ie_tipo_cliente_p			ptu_pedido_insistencia.ie_tipo_cliente%type,
					cd_unimed_executora_p			ptu_pedido_insistencia.cd_unimed_executora%type,
					cd_unimed_beneficiario_p		ptu_pedido_insistencia.cd_unimed_beneficiario%type,
					nr_seq_execucao_p			ptu_pedido_insistencia.nr_seq_execucao%type,
					nr_seq_origem_p				ptu_pedido_insistencia.nr_seq_origem%type,
					cd_transacao_p				ptu_pedido_insistencia.cd_transacao%type,
					ds_mensagem_p				ptu_pedido_insistencia.ds_mensagem%type,
					ds_arquivo_pedido_p			ptu_pedido_insistencia.ds_arquivo_pedido%type,
					nr_versao_p				ptu_pedido_insistencia.nr_versao%type,
					nm_usuario_p				usuario.nm_usuario%type,
					nr_seq_pedido_insistencia_p	out	ptu_pedido_insistencia.nr_sequencia%type) is
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a importação do arquivo de 00302 - Pedido de Insistência
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ x] Tasy (Delphi/Java) [  x] Portal [  ]  Relatórios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

ptu_imp_scs_ws_pck.ptu_imp_pedido_insistencia(	ie_tipo_cliente_p, cd_unimed_executora_p, cd_unimed_beneficiario_p,
						nr_seq_execucao_p, nr_seq_origem_p, cd_transacao_p,
						ds_mensagem_p, ds_arquivo_pedido_p, nr_versao_p,
						nm_usuario_p, nr_seq_pedido_insistencia_p );

end ptu_ped_insistencia_v50_ws;
/