create or replace
procedure swisslog_lancamento_material(
			cd_acao_p			varchar2,
			nr_atendimento_p			number,
			cd_local_estoque_p		number,
			cd_material_p			number,
			qt_atual_p			number,
			dt_movimento_p			date,
			cd_fornecedor_p			varchar2,
			nr_prescricao_p			number,	
			nr_seq_material_p			number,
			nr_seq_lote_p			number,
			ie_atualizou_p		out	varchar2,
			ds_erro_p			out	varchar2,
			cd_barras_p			varchar2 default null,
			ie_commit_p			varchar2 default 'S') is

cd_estabelecimento_w		number(4);
qt_estoque_w			number(15,4);
qt_material_w			number(15,4);
qt_devolvida_w			number(15,4);
qt_original_w			number(15,4);
cd_acao_w			varchar2(1) := '1';
cd_fornec_consignado_w		varchar2(14);
cd_setor_atendimento_w		number(5);
ie_cobra_paciente_w		varchar2(1);
cd_unidade_medida_w		varchar2(30);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
nr_doc_convenio_w		varchar2(20);
ie_tipo_guia_w			varchar2(2);
cd_senha_w			varchar2(20);
dt_entrada_unidade_w		date;
cd_centro_custo_w		number(8);
cd_conta_contabil_w		varchar2(20);
cd_operacao_estoque_w		number(3);
nr_seq_atepacu_w		number(10);
ie_consignado_w			varchar2(1);
ie_atualizar_consig_w		varchar2(1);
ds_erro_w			varchar2(255);
nr_sequencia_w			number(10);

/* par�metros converte_codigo_barras */
cd_material_w			number(6);
cd_mat_dedobrado_w		number(6);
qt_mat_barras_w			number(13,4);
nr_seq_lote_w			number(10);
nr_seq_lote_agrup_w		number(10);
cd_kit_mat_w			number(10);
ds_validade_w			varchar2(255);
ds_material_w			varchar2(255);
cd_unid_med_w			varchar2(30);
nr_etiqueta_lp_w		varchar2(255);
/* par�metros converte_codigo_barras */

nr_seq_tipo_baixa_w		number(10);
qt_total_dispensar_item_w	number(10);
qt_total_item_conta_w		number(10);
qt_existe_w			number(10);
nr_seq_lote_ap_w		number(10);
cd_motivo_baixa_int_w		number(3);
ie_saldo_estoque_w		varchar2(1);
nr_item_desdobrado_w		number(10);
nr_seq_motivo_w			number(10);
ds_motivo_w			varchar2(255);
nr_novo_lote_w			number(10);
ie_motivo_desdobra_w		varchar2(1);
nm_usuario_w			varchar2(15);
ie_data_alta_w			varchar2(1);
dt_alta_w				atendimento_paciente.dt_alta%type;
ie_atualiza_estoque_w		tipo_baixa_prescricao.ie_atualiza_estoque%type;
ie_conta_paciente_w		tipo_baixa_prescricao.ie_conta_paciente%type;
ie_gerar_transf_lote_w		parametros_farmacia.ie_gerar_transf_lote%type;
ie_transf_atend_lote_w		setor_atendimento.ie_transf_atend_lote%type;
cd_local_estoque_ent_w		local_estoque.cd_local_estoque%type;
nr_seq_horario_w		prescr_mat_hor.nr_sequencia%type;
nr_seq_material_w		prescr_mat_hor.nr_seq_material%type;
cd_local_novo_w			local_estoque.cd_local_estoque%type;
cd_material_prescr_w		material.cd_material%type;
ie_via_aplicacao_w		via_aplicacao.ie_via_aplicacao%type;
dt_atendimento_w		material_atend_paciente.dt_atendimento%type;
ie_tipo_saldo_w			varchar2(1);
qt_estoque_disp_w  			number(10) := 0;
ie_baixou_tudo_w 				varchar2(1):= 'N';
saldo_forn_consig_w 			number(10) := 0;
cd_cgc_fornec_anterior_w	varchar2(14);
qtd_saldo_pendente_w		number(15,4);

cursor c01 is
	select	a.cd_material,
		a.nr_sequencia,
		sum(a.qt_dispensar),
		a.nr_seq_mat_hor,
		b.nr_seq_material
	from	prescr_mat_hor b,
		ap_lote_item a,
		ap_lote c
	where	a.nr_seq_lote = nr_seq_lote_p
	and	b.nr_seq_material = nvl(nr_seq_material_p,b.nr_seq_material)
	and	c.nr_sequencia = a.nr_seq_lote
	and	a.nr_seq_mat_hor = b.nr_sequencia
	and	b.dt_suspensao is null
	and	a.dt_supensao is null
	group by a.cd_material,
		a.nr_sequencia,
		a.nr_seq_mat_hor,
		b.nr_seq_material
	having sum(a.qt_dispensar) > 0;

begin
begin
nm_usuario_w := 'Swisslog';
cd_material_w := cd_material_p;
cd_fornec_consignado_w := cd_fornecedor_p;
ie_atualizou_p := 'N';

/*select	max(b.cd_setor_atendimento),
	max(c.cd_motivo_baixa_int)
into	cd_setor_atendimento_w,
	cd_motivo_baixa_int_w
from	far_local_cc_int c,
	far_setores_integracao b,
	empresa_integracao a
where	c.cd_local_estoque = cd_local_estoque_p
and	a.nm_empresa = 'SWISSLOG'
and	b.nr_sequencia = c.nr_seq_far_setores
and	a.nr_sequencia = b.nr_seq_empresa_int;*/

obter_param_usuario(7029, 57, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, ie_data_alta_w);

if	(nr_seq_lote_p is not null) then
	select	nvl(max(cd_tipo_baixa),1),
		nvl(max(cd_setor_atendimento),0)
	into	cd_motivo_baixa_int_w,
		cd_setor_atendimento_w
	from	ap_lote
	where	nr_sequencia = nr_seq_lote_p;
end if;

if	(nvl(cd_setor_atendimento_w,0) = 0) then
	-- 'N�o encontrado setor de atendimento para o local de estoque!';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(278817);
end if;

begin
select	cd_estabelecimento
into	cd_estabelecimento_w
from	local_estoque
where	cd_local_estoque = cd_local_estoque_p;
exception
when others then
	-- 'N�o cadastro do local de estoque no Tasy!';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(278818);
end;

select	nvl(max(ie_gerar_transf_lote),'N')
into	ie_gerar_transf_lote_w
from	parametros_farmacia
where	cd_estabelecimento = cd_estabelecimento_w;

select	nvl(max(ie_transf_atend_lote),'N')
into	ie_transf_atend_lote_w
from	setor_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_w;

if	(cd_acao_p = 'I') then
	cd_acao_w := '1';
elsif	(cd_acao_p = 'E') then
	cd_acao_w := '2';
end if;

if	(cd_barras_p is not null) then
	converte_codigo_barras(	cd_barras_p,		cd_estabelecimento_w,	null,
				'N',			cd_material_w,		qt_material_w,
				nr_seq_lote_w,		nr_seq_lote_agrup_w,	cd_kit_mat_w,
				ds_validade_w,		ds_material_w,		cd_unid_med_w,
				nr_etiqueta_lp_w,	ds_erro_w);
end if;

qt_material_w := qt_atual_p;

if	(qt_material_w = 0) then
	-- 'Quantidade de contagem igual ao saldo em estoque!',1,255);
	ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(278822),1,255);
end if;

begin
select	cd_unidade_medida_estoque,
	ie_cobra_paciente,
	nvl(ie_consignado,'0')
into	cd_unidade_medida_w,
	ie_cobra_paciente_w,
	ie_consignado_w
from	material
where	cd_material = cd_material_w;
exception
when others then
	-- 'N�o encontrado cadastro do material no Tasy!';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(278819);
end;

if	(ie_consignado_w = '2') and (cd_fornec_consignado_w is null) then
	obter_fornec_consig_ambos(cd_estabelecimento_w, cd_material_w, nr_seq_lote_w, cd_local_estoque_p, ie_tipo_saldo_w, cd_fornec_consignado_w);
	
	if (ie_tipo_saldo_w	= 'N') then			
		begin
			qt_estoque_disp_w := obter_saldo_disp_estoque(cd_estabelecimento_w, cd_material_w, cd_local_estoque_p, dt_movimento_p);
		end;
		elsif (ie_tipo_saldo_w	= 'C') then
		begin
			obter_saldo_estoque_consignado(cd_estabelecimento_w, cd_fornec_consignado_w, cd_material_w, cd_local_estoque_p, dt_movimento_p, qt_estoque_disp_w);
		end;
		end if;
		
		if	(ie_tipo_saldo_w = 'E') then 
			ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(342019),1,255);
		end if;
end if;

if	(ie_consignado_w <> '0') and
	(cd_fornec_consignado_w is not null) then
	begin
	ie_atualizar_consig_w := 'S';
	
	begin
	select	cd_cgc
	into	cd_fornec_consignado_w
	from	pessoa_juridica
	where	cd_cgc = cd_fornec_consignado_w;
	exception
	when others then
		-- 'N�o encontrado cadastro do fornecedor no Tasy! CGC: ' || cd_fornec_consignado_w,1,255);
		ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(278821) || cd_fornec_consignado_w,1,255);
	end;
	end;
else
	begin
	ie_atualizar_consig_w := 'N';
	cd_fornec_consignado_w := null;
	end;
end if;

while (ie_baixou_tudo_w <> 'S') loop
	begin
	if ((ie_consignado_w = '2') and (qt_estoque_disp_w < qt_material_w)) then 
		begin
		if (ie_tipo_saldo_w	= 'N' and qt_estoque_disp_w > 0) then
			begin
			if (qt_estoque_disp_w >= qt_material_w)then
				begin
				ie_baixou_tudo_w := 'S'; 
				end;
			else 
				begin
					qtd_saldo_pendente_w := qt_material_w - qt_estoque_disp_w;   
					qt_material_w := qt_estoque_disp_w;  
					qt_estoque_disp_w := 0;
				end; 
			end if;
			end;
		else 
			begin 
			obter_fornec_consig_ambos(cd_estabelecimento_w, cd_material_w, nr_seq_lote_w, cd_local_estoque_p, ie_tipo_saldo_w, cd_fornec_consignado_w);		
					
			if ((cd_fornec_consignado_w is null) or (cd_cgc_fornec_anterior_w = cd_fornec_consignado_w)) then 
				ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(1084105, 'QT_PENDENTE=' || qt_material_w),1,255);
				ie_baixou_tudo_w := 'S';
			else
				begin
				cd_cgc_fornec_anterior_w := cd_fornec_consignado_w;			
				obter_saldo_estoque_consignado(cd_estabelecimento_w, cd_fornec_consignado_w, cd_material_w, cd_local_estoque_p, dt_movimento_p, saldo_forn_consig_w); 
				
				if (saldo_forn_consig_w >= qt_material_w)then 
					begin
					ie_baixou_tudo_w := 'S'; 
					end;
				elsif (saldo_forn_consig_w > 0)then 
					begin
						qtd_saldo_pendente_w := qt_material_w - saldo_forn_consig_w;  
						qt_material_w := saldo_forn_consig_w; 
					end; 
				end if;
				end;
			end if;
			end;
		end if;
		end;
	else
		begin
		ie_baixou_tudo_w := 'S'; 
		end;
	end if;

	if	(ds_erro_w is null) then
		begin
		if	(nvl(nr_atendimento_p,0) > 0) then
			begin
			obter_convenio_execucao(nr_atendimento_p, sysdate, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);
			nr_seq_atepacu_w := obter_atepacu_paciente(nr_atendimento_p,'A');
			
			select 	max(a.dt_entrada_unidade)
			into	dt_entrada_unidade_w
			from	atend_paciente_unidade a
			where	a.nr_seq_interno = nr_seq_atepacu_w;
			
			select 	nvl(nr_sequencia,0),
				nvl(ie_atualiza_estoque,'S'),
				nvl(ie_conta_paciente,'S')
			into	nr_seq_tipo_baixa_w,
				ie_atualiza_estoque_w,
				ie_conta_paciente_w
			from 	tipo_baixa_prescricao
			where	ie_situacao = 'A'
			and 	cd_tipo_baixa = cd_motivo_baixa_int_w
			and 	ie_prescricao_devolucao = 'P';
					
			if	(cd_acao_w = 2) then
				begin
				qt_devolvida_w	:= qt_material_w;
				qt_material_w	:= (qt_material_w * -1);
				
				select 	nvl(nr_sequencia,0)
				into	nr_seq_tipo_baixa_w
				from 	tipo_baixa_prescricao
				where	ie_situacao = 'A'
				and 	cd_tipo_baixa = cd_motivo_baixa_int_w				
				and 	ie_prescricao_devolucao = 'D';
				end;
			end if;
			
			if	(ie_cobra_paciente_w = 'S') and (ie_conta_paciente_w = 'S') then
				begin
				/* Altera��o realizada para n�o executar a OBTER_DISP_ESTOQUE e n�o entrar na rotina de desdobramento do lote.
					 Essa altera��o se faz necess�ria pois a Swisslog n�o est� com os processo ajustados para trabalhar conforme solicitado pelo Hospital.
					 N�o foi retirada a rotina da procedure pois futuramente essa rotina deve entrar em produ��o. */
				--obter_disp_estoque(cd_material_w, cd_local_estoque_p, cd_estabelecimento_w, 0, qt_material_w, cd_fornec_consignado_w, ie_saldo_estoque_w);
				if	(nvl(ie_saldo_estoque_w,'S') = 'N') and (nr_seq_lote_p is not null) and (nvl(ie_saldo_estoque_w,'S') = 'X') then
					open c01;
					loop
					fetch c01 into
						cd_mat_dedobrado_w,
						nr_item_desdobrado_w,
						qt_original_w,
						nr_seq_horario_w,
						nr_seq_material_w;
					exit when c01%notfound;
						begin
						Gerar_w_ap_lote_desdob (
								nr_seq_lote_p,
								nr_item_desdobrado_w,
								nr_seq_horario_w,
								qt_original_w,
								0);
						
						obter_param_usuario(7029,69,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,ie_motivo_desdobra_w);
					
						if	(nvl(ie_motivo_desdobra_w,'N') = 'S') then
							select	max(nr_sequencia),
								max(ds_motivo)
							into	nr_seq_motivo_w,
								ds_motivo_w
							from	motivo_desdobrar_lote
							where	ie_situacao = 'A';
						end if;
						
						desdobrar_lote_prescricao (
								nr_seq_lote_p,
								nm_usuario_w,
								'D',
								nr_seq_motivo_w,
								ds_motivo_w,
								'N',
								nr_novo_lote_w);
						
						cd_local_novo_w := obter_local_disp_prescr_item(nr_prescricao_p,nr_seq_material_w,0,'N','N','N','Swisslog');
						
						if	(nvl(nr_novo_lote_w,0) > 0) then
							update	ap_lote
							set	cd_local_estoque = cd_local_novo_w
							where	nr_sequencia = nr_novo_lote_w;
							
							update	prescr_mat_hor
							set	cd_local_estoque = cd_local_novo_w
							where	nr_seq_lote = nr_novo_lote_w;
						end if;
						end;
					end loop;
					close c01;
					
					-- Coloca lote como atendido
					if	(cd_acao_w = 1) and (nvl(nr_seq_lote_p,0) > 0) then
						select	count(*)
						into	qt_existe_w
						from	ap_lote_item
						where	dt_controle is null
						and	dt_supensao is null
						and	qt_dispensar > 0
						and	obter_se_generico_igual(cd_material,cd_material_w) = 'S'
						and	nr_seq_lote = nr_seq_lote_p;
						
						if	(qt_existe_w > 0) then
							update	ap_lote_item
							set	dt_controle = sysdate,
								dt_atualizacao = sysdate,
								nm_usuario = 'SWLM'
							where	nr_seq_lote = nr_seq_lote_p
							and	qt_dispensar > 0
							and	obter_se_generico_igual(cd_material,cd_material_w) = 'S';
						end if;
						
						select	count(*)
						into	qt_existe_w
						from	ap_lote_item
						where	nr_seq_lote = nr_seq_lote_p
						and	dt_controle is null
						and	dt_supensao is null
						and	qt_dispensar > 0
						and	ie_prescrito = 'S';
						
						if	(qt_existe_w = 0) then
							update	ap_lote	
							set	dt_atend_farmacia = sysdate,
								nm_usuario_atend = nm_usuario_w,
								ie_status_lote   = 'A'
							where 	nr_sequencia = nr_seq_lote_p;
						end if;
					end if;
					
					goto Final;
				end if;
				
				dt_atendimento_w := sysdate;
				if	(ie_data_alta_w = 'S') then
					begin
					if	(obter_se_atendimento_alta(nr_atendimento_p) = 'S') then
						dt_alta_w := obter_data_alta_atendimento(nr_atendimento_p);	

						if	(dt_atendimento_w > dt_alta_w) then
							dt_atendimento_w := dt_alta_w;
						end if;
					end if;
					end;
				end if;
				
				
				begin
				select	cd_material,
					ie_via_aplicacao
				into	cd_material_prescr_w,
					ie_via_aplicacao_w
				from	prescr_material
				where	nr_prescricao = nr_prescricao_p
				and	nr_sequencia = nr_seq_material_p;
				exception
				when others then
					cd_material_prescr_w := cd_material_w;
					ie_via_aplicacao_w := null;
				end;
				
				select	material_atend_paciente_seq.nextval
				into	nr_sequencia_w
				from	dual;
				
				insert into material_atend_paciente (
					nr_sequencia,
					nr_atendimento,
					dt_entrada_unidade,
					cd_material,
					dt_atendimento,
					cd_unidade_medida,
					qt_material,
					dt_atualizacao,
					nm_usuario,
					cd_acao,
					cd_setor_atendimento,
					nr_seq_atepacu,
					cd_material_prescricao,
					cd_material_exec,
					ie_via_aplicacao,
					dt_prescricao,
					nr_prescricao,
					nr_sequencia_prescricao,
					cd_cgc_fornecedor,
					qt_executada,
					nr_cirurgia,
					cd_local_estoque,
					vl_unitario,
					qt_ajuste_conta,
					ie_valor_informado,
					ie_guia_informada,
					ie_auditoria,
					nm_usuario_original,
					cd_situacao_glosa,
					cd_convenio,
					cd_categoria,
					nr_doc_convenio,
					ie_tipo_guia,
					nr_seq_lote_fornec,
					cd_senha,
					dt_conta,
					nr_seq_kit_estoque,
					qt_devolvida,
					cd_funcao,
					nr_seq_tipo_baixa,
					nr_seq_lote_ap)
				values(	nr_sequencia_w,
					nr_atendimento_p,
					dt_entrada_unidade_w,
					cd_material_w,
					dt_atendimento_w,
					cd_unidade_medida_w,
					qt_material_w,
					sysdate,
					nm_usuario_w,
					cd_acao_w,
					cd_setor_atendimento_w,
					nr_seq_atepacu_w, 
					cd_material_prescr_w,
					cd_material_w,
					ie_via_aplicacao_w,
					null,
					decode(nvl(nr_prescricao_p,0),0,null,nr_prescricao_p),
					decode(nvl(nr_seq_material_p,0),0,null,nr_seq_material_p),
					cd_fornec_consignado_w,
					qt_material_w,
					null,
					cd_local_estoque_p,
					0,
					0,
					'N',
					'N',
					'N',
					nm_usuario_w,
					0,
					cd_convenio_w, 
					cd_categoria_w, 
					nr_doc_convenio_w, 
					ie_tipo_guia_w,
					decode(nvl(nr_seq_lote_w,0),0,null,nr_seq_lote_w),
					cd_senha_w,
					dt_atendimento_w,
					null,
					qt_devolvida_w,
					147,
					nr_seq_tipo_baixa_w,
					decode(nvl(nr_seq_lote_p,0),0,null,nr_seq_lote_p));
				
				ie_atualizou_p := 'S';
				atualiza_preco_material(nr_sequencia_w,nm_usuario_w);
				gerar_lanc_automatico_mat(nr_atendimento_p, cd_local_estoque_p, 132, nm_usuario_w, nr_sequencia_w, null, null);
				
				select	sum(qt_total_dispensar)
				into	qt_total_dispensar_item_w
				from	prescr_material
				where	nr_prescricao = nr_prescricao_p
				and	nr_sequencia = nr_seq_material_p;
				
				select 	sum(qt_material)
				into	qt_total_item_conta_w
				from 	material_atend_paciente
				where 	nr_atendimento	= nr_atendimento_p
				and 	nr_prescricao	= nr_prescricao_p
				and 	nr_sequencia_prescricao	= nr_seq_material_p;
				
				select	count(*)
				into	qt_existe_w
				from	prescr_medica
				where	nr_prescricao = nr_prescricao_p
				and	dt_liberacao is null;
				
				-- Baixa o item na prescri��o quando a quantidade da conta � maior ou igual a quantidade a ser dispensada.
				if	(qt_existe_w = 0) and (qt_total_dispensar_item_w <= qt_total_item_conta_w) then

					update 	prescr_material
					set 	cd_motivo_baixa = cd_motivo_baixa_int_w,
						dt_baixa 	= sysdate,
						nm_usuario	= nm_usuario_w
					where 	nr_prescricao 	= nr_prescricao_p
					and 	nr_sequencia	= nr_seq_material_p;
				end if;
				
				-- Coloca lote como atendido
				if	(cd_acao_w = 1) and (nvl(nr_seq_lote_p,0) > 0) then
					
					select	count(*)
					into	qt_existe_w
					from	ap_lote_item
					where	dt_controle is null
					and	dt_supensao is null
					and	qt_dispensar > 0
					and	obter_se_generico_igual(cd_material,cd_material_w) = 'S'
					and	nr_seq_lote = nr_seq_lote_p;
					
					
					if	(qt_existe_w > 0) then
						update	ap_lote_item
						set	dt_controle = sysdate,
							dt_atualizacao = sysdate,
							nm_usuario = 'SWLM'
						where	nr_seq_lote = nr_seq_lote_p
						and	qt_dispensar > 0
						and	obter_se_generico_igual(cd_material,cd_material_w) = 'S';
					end if;
					
					select	count(*)
					into	qt_existe_w
					from	ap_lote_item
					where	nr_seq_lote = nr_seq_lote_p
					and	dt_controle is null
					and	dt_supensao is null
					and	qt_dispensar > 0
					and	ie_prescrito = 'S';
					
					if	(qt_existe_w = 0) then
						update	ap_lote	
						set	dt_atend_farmacia = sysdate,
							nm_usuario_atend = nm_usuario_w,
							ie_status_lote   = 'A'
						where 	nr_sequencia = nr_seq_lote_p;
					end if;
		
				end if;
				
				end;
			elsif	(ie_atualiza_estoque_w = 'S') then
				begin
				if	(ie_gerar_transf_lote_w = 'S') and (ie_transf_atend_lote_w = 'S') then
					obter_param_usuario(7029, 108, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_w, cd_local_estoque_ent_w);
					
					gerar_transf_lote_prescricao(
								nr_seq_lote_p,nr_prescricao_p,nr_seq_material_p, nr_atendimento_p,cd_material_w,
								cd_setor_atendimento_w,sysdate,cd_fornec_consignado_w,qt_material_w,
								nvl(nr_seq_lote_w,0),cd_local_estoque_p,cd_local_estoque_ent_w,
								cd_estabelecimento_w,nm_usuario_w);
					ie_atualizou_p := 'S';
				else
					gerar_prescricao_estoque(
						cd_estabelecimento_w,
						nr_atendimento_p,
						dt_entrada_unidade_w,
						cd_material_w,
						sysdate,
						cd_acao_w,
						cd_local_estoque_p,
						qt_material_w,
						cd_setor_atendimento_w,
						cd_unidade_medida_w,
						nm_usuario_w,
						'I',
						nr_prescricao_p,
						null,
						null,
						0,
						cd_fornec_consignado_w,
						null,
						nvl(nr_seq_lote_w,0),
						null,
						null,
						nr_seq_lote_p,
						null,
						null,
						null);
					
					ie_atualizou_p := 'S';
				end if;
				-- Coloca lote como atendido
				if	(cd_acao_w = 1) and (nvl(nr_seq_lote_p,0) > 0) then
					
					select	count(*)
					into	qt_existe_w
					from	ap_lote_item
					where	dt_controle is null
					and	dt_supensao is null
					and	qt_dispensar > 0
					and	obter_se_generico_igual(cd_material,cd_material_w) = 'S'
					and	nr_seq_lote = nr_seq_lote_p;
					
					if	(qt_existe_w > 0) then
						update	ap_lote_item
						set	dt_controle = sysdate,
							dt_atualizacao = sysdate,
							nm_usuario = 'SWLM'
						where	nr_seq_lote = nr_seq_lote_p
						and	qt_dispensar > 0
						and	obter_se_generico_igual(cd_material,cd_material_w) = 'S';
					end if;
					
					select	count(*)
					into	qt_existe_w
					from	ap_lote_item
					where	nr_seq_lote = nr_seq_lote_p
					and	dt_controle is null
					and	dt_supensao is null
					and	qt_dispensar > 0
					and	ie_prescrito = 'S';
					
					if	(qt_existe_w = 0) then
						update	ap_lote	
						set	dt_atend_farmacia = sysdate,
							nm_usuario_atend = nm_usuario_w,
							ie_status_lote   = 'A'
						where 	nr_sequencia = nr_seq_lote_p;
					end if;				
				end if;
				end;
			end if;
			end;
		else
			begin
			if	(ie_atualizar_consig_w = 'N') then
				select	max(cd_operacao_estoque)
				into	cd_operacao_estoque_w
				from	operacao_estoque
				where	ie_situacao = 'A'
				and	ie_tipo_requisicao = 5
				and	nvl(ie_consignado,0) = 0
				and	ie_atualiza_estoque = 'S'
				and	ie_entrada_saida = decode(cd_acao_w,'1','S','E');
			else
				select	max(cd_operacao_estoque)
				into	cd_operacao_estoque_w
				from	operacao_estoque
				where	ie_situacao = 'A'
				and	ie_tipo_requisicao = 5
				and	nvl(ie_consignado,0) = 7
				and	ie_entrada_saida = decode(cd_acao_w,'1','S','E');
			end if;
			
			cd_acao_w := '1';	
			
			select	max(cd_centro_custo)
			into	cd_centro_custo_w
			from	setor_atendimento
			where	cd_setor_atendimento = cd_setor_atendimento_w;
			
			define_conta_material(	
				cd_estabelecimento_w,
				cd_material_w,
				'2',
				null,
				cd_setor_atendimento_w,
				null,
				null,
				null,
				null,
				null,
				cd_local_estoque_p,
				null,
				sysdate,
				cd_conta_contabil_w,
				cd_centro_custo_w,
				null);
				
			insert into movimento_estoque(
				nr_movimento_estoque,
				cd_estabelecimento,
				cd_local_estoque,
				dt_movimento_estoque,
				cd_operacao_estoque,
				cd_acao,
				cd_material,
				dt_mesano_referencia,
				qt_movimento,
				dt_atualizacao,
				nm_usuario,
				ie_origem_documento,
				nr_documento,
				nr_sequencia_item_docto,
				cd_unidade_medida_estoque,
				cd_setor_atendimento,
				qt_estoque,
				cd_centro_custo,
				cd_unidade_med_mov,
				cd_fornecedor,
				ds_observacao,
				nr_seq_tab_orig,
				nr_seq_lote_fornec,
				cd_lote_fabricacao,
				dt_validade,
				nr_atendimento,
				nr_prescricao,
				nr_receita,
				cd_conta_contabil,
				nr_ordem_compra,
				nr_item_oci,
				nr_lote_ap,
				nr_lote_producao,
				cd_funcao)
			values(	movimento_estoque_seq.nextval,
				cd_estabelecimento_w,
				cd_local_estoque_p,
				sysdate,
				cd_operacao_estoque_w,
				'1',
				cd_material_w,
				sysdate,
				qt_material_w,
				sysdate,
				nm_usuario_w,
				'3',
				null,
				null,
				cd_unidade_medida_w,
				cd_setor_atendimento_w,
				qt_material_w,
				cd_centro_custo_w,
				cd_unidade_medida_w,
				cd_fornec_consignado_w,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				cd_conta_contabil_w,
				null,
				null,
				null,
				null,
				147);
			end;
		end if;
		ie_atualizou_p := 'S';
		end;
	end if;
	qt_material_w := qtd_saldo_pendente_w; 
	ds_erro_p := ds_erro_w;

	<<Final>>

	if	(ds_erro_w is null) and (nvl(ie_commit_p,'S') = 'S') then
		commit;
	end if;
	end;
end loop;

exception
when others then
	ds_erro_p := substr(ds_erro_w||' | '||SQLERRM(sqlcode),1,2000);
	
end;
end swisslog_lancamento_material;
/
