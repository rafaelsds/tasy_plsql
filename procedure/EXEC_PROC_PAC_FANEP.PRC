CREATE OR REPLACE
PROCEDURE exec_proc_pac_fanep(	nr_seq_pepo_p		NUMBER,
				nm_usuario_p		VARCHAR2) IS

nr_seq_proc_interno_w		NUMBER(15,0);
nr_seq_proc_interno_ww		NUMBER(15,0);
qt_taxa_w			NUMBER(15,3);
nr_seq_pepo_w			NUMBER(15);
cd_procedimento_ww		NUMBER(15,0);
cd_procedimento_w		NUMBER(15,0);
ie_origem_proced_ww		NUMBER(15,0);
ie_origem_proced_w		NUMBER(15,0);
cd_procedimento_www		NUMBER(15,0);
ie_origem_proced_www		NUMBER(10,0);
nr_seq_proc_w			NUMBER(10,0);
qt_procedimento_w			NUMBER(15,0);
qt_procedimento_ww		NUMBER(15,0);
cd_convenio_w			NUMBER(15,0);
nr_atendimento_w			NUMBER(15,0);
cd_setor_atendimento_w		NUMBER(15,0);
nr_seq_interno_w			NUMBER(15,0);
cd_estabelecimento_w		NUMBER(14,0);
ie_tipo_atendimento_w		NUMBER(13,0);
nr_seq_classificacao_w		NUMBER(15);
cd_especialidade_w		NUMBER(15);
nr_seq_partic_w			NUMBER(15);
nr_seq_regra_w			NUMBER(15,0)	:= NULL;
ie_doc_executor_w			NUMBER(15);
cd_medico_anestesista_w		VARCHAR2(15);
cd_pessoa_fisica_w		VARCHAR2(15);
cd_categoria_w			VARCHAR2(15);
nr_doc_convenio_w		VARCHAR2(25);
ie_tipo_guia_w			VARCHAR2(12);
cd_senha_w			VARCHAR2(25);
cd_cbo_w			VARCHAR2(16);
ie_responsavel_credito_w		VARCHAR2(15);
cd_funcao_w			VARCHAR2(15);
cd_cgc_prestador_w		VARCHAR2(18);
cd_cgc_retorno_w			VARCHAR2(18);
ie_emite_conta_w           		VARCHAR2(11);
ie_forma_apresentacao_w		VARCHAR2(11);
dt_inicio_w			DATE;
dt_fim_w				DATE;
dt_termino_w			DATE;
dt_inicio_proced_w			DATE;
dt_entrada_unidade_w		DATE;
dt_inicio_procedimento_w		DATE;
dt_proc_equip_w			DATE;
cd_material_w			NUMBER(16);
qt_material_w 			NUMBER(20,3);
nr_sequencia_w			NUMBER(15,0);
nr_seq_atepacu_w		NUMBER(15,0);
ie_via_acesso_w			VARCHAR2(1);
tx_procedimento_w		NUMBER(15,4);


CURSOR c01 IS
	SELECT	a.cd_procedimento,
		a.ie_origem_proced,
		a.nr_seq_pepo,
		a.nr_seq_proc_interno,
		1 qt_procedimento,
		ie_via_acesso
	FROM	pepo_cirurgia_proc a
	WHERE	nr_seq_pepo = nr_seq_pepo_p
	AND NOT EXISTS (	SELECT	w.cd_procedimento
        	          	FROM	procedimento_paciente w
                	  	WHERE	a.cd_procedimento	 	= w.cd_procedimento
				AND	a.ie_origem_proced		= w.ie_origem_proced
				AND	NVL(a.nr_seq_proc_interno,0)	= NVL(w.nr_seq_proc_interno,0)
				AND 	w.nr_seq_pepo			= nr_seq_pepo_p)
	ORDER BY a.nr_sequencia;


CURSOR c02 IS /* LAN�A AS TAXAS DO EQUIPAMENTOS*/
	SELECT	nr_seq_proc_interno,
		qt_taxa,
		cd_procedimento,
		ie_origem_proced,
		dt_inicio,
		dt_fim
	FROM	equipamento_cirurgia a,
		taxa_equipamento_cirurgia b
	WHERE	a.nr_sequencia = b.nr_seq_equi_cir
	AND    ((nr_seq_proc_interno IS NOT NULL) OR (cd_procedimento IS NOT NULL))
	AND	a.nr_seq_pepo = nr_seq_pepo_p
	AND	NVL(a.ie_situacao,'A') = 'A'
	AND NOT EXISTS (	SELECT	1
        	          	FROM	procedimento_paciente w
                	  	WHERE	b.nr_seq_proc_interno 	= w.nr_seq_proc_interno
                    		AND	w.nr_seq_pepo 		= nr_seq_pepo_p
				AND	cd_motivo_exc_conta IS NULL);

CURSOR c03 IS
	SELECT	a.nr_sequencia,
		a.cd_material,
		a.qt_material
	FROM 	material_equip_cirurgia a,
		equipamento_cirurgia b
	WHERE	a.nr_seq_equi_cir	= b.nr_sequencia
	AND	b.nr_seq_pepo 		= nr_seq_pepo_p
	AND	NVL(b.ie_situacao,'A') = 'A'
	AND	a.dt_lancamento_automatico IS NULL;

BEGIN

SELECT	NVL(MAX(nr_atendimento),0),
	MAX(cd_pessoa_fisica),
	MAX(dt_inicio_proced),
	MAX(cd_medico_anestesista),
	MAX(dt_fim_cirurgia),
	MAX(nr_seq_atepacu)
INTO	nr_atendimento_w,
	cd_pessoa_fisica_w,
	dt_inicio_proced_w,
	cd_medico_anestesista_w,
	dt_termino_w,
	nr_seq_atepacu_w
FROM	pepo_cirurgia
WHERE	nr_sequencia	=	nr_seq_pepo_p;

if (nr_atendimento_w = 0) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(447119); 
	/*Este processo n�o possui atendimento! � necess�rio verificar a forma de v�nculo de atendimento no processo. Analisar a possibilidade de habilitar o par�metro [10] Solicitar atendimento/unidade de atendimento e data ao iniciar/finalizar o processo, do FANEP.*/
end if;

SELECT	NVL(MAX(cd_estabelecimento),1),
	MAX(ie_tipo_atendimento),
	MAX(nr_seq_classificacao)
INTO	cd_estabelecimento_w,
	ie_tipo_atendimento_w,
	nr_seq_classificacao_w
FROM	atendimento_paciente
WHERE	nr_atendimento	=	nr_atendimento_w;


IF (nr_seq_atepacu_w IS NOT NULL) THEN
	SELECT	MAX(cd_setor_atendimento),
		MAX(nr_seq_interno),
		MAX(dt_entrada_unidade)
	INTO	cd_setor_atendimento_w,
		nr_seq_interno_w,
		dt_entrada_unidade_w
	FROM	atend_paciente_unidade
	WHERE	nr_seq_interno		=	nr_seq_atepacu_w;
ELSE
	SELECT	MAX(cd_setor_atendimento),
		MAX(nr_seq_interno),
		MAX(dt_entrada_unidade)
	INTO	cd_setor_atendimento_w,
		nr_seq_interno_w,
		dt_entrada_unidade_w
	FROM	atend_paciente_unidade
	WHERE	nr_seq_interno		=	obter_atepacu_paciente(nr_atendimento_w, 'A');
END IF;


obter_convenio_execucao(nr_atendimento_w, dt_inicio_proced_w, cd_convenio_w, cd_categoria_w,nr_doc_convenio_w,ie_tipo_guia_w,cd_senha_w);

OPEN c01;
LOOP
FETCH c01 INTO
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_pepo_w,
	nr_seq_proc_interno_w,
	qt_procedimento_w,
	ie_via_acesso_w;
EXIT WHEN c01%NOTFOUND;
	BEGIN

	IF	(ie_via_acesso_w IS NULL) THEN
		ie_via_acesso_w := obter_regra_via_acesso(cd_procedimento_w, ie_origem_proced_w, cd_estabelecimento_w, cd_convenio_w);
	END IF;

	tx_procedimento_w:= 100;
	IF	(ie_via_acesso_w IS NOT NULL) THEN
		tx_procedimento_w	:= obter_tx_proc_via_acesso(ie_via_acesso_w);
	END IF;

	SELECT	procedimento_paciente_seq.NEXTVAL
	INTO	nr_seq_proc_w
	FROM	dual;

	INSERT INTO procedimento_paciente(
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_procedimento,
		cd_pessoa_fisica,
		ie_funcao_medico,
		cd_convenio,
		cd_categoria,
		ie_proc_princ_atend,
		ie_video,
		tx_medico,
		tx_anestesia,
		tx_procedimento,
		ie_valor_informado,
		ie_guia_informada,
		cd_situacao_glosa,
		nm_usuario_original,
		nr_atendimento,
		cd_setor_atendimento,
		dt_entrada_unidade,
		nr_seq_atepacu,
		ie_auditoria,
		ie_emite_conta,
		cd_cgc_prestador,
		nr_seq_proc_interno,
		dt_procedimento,
		dt_atualizacao,
		nm_usuario,
		nr_seq_pepo,
		dt_conta,
		nr_doc_convenio,
		ie_tipo_guia,
		cd_senha,
		ie_via_acesso)
	VALUES(
		nr_seq_proc_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		DECODE(qt_procedimento_w, 0, 1, qt_procedimento_w),
		DECODE(ie_origem_proced_w,7,NULL,obter_pessoa_fisica_usuario(nm_usuario_p,'C')),
		'1',
		cd_convenio_w,
		cd_categoria_w,
		'N',
		'N',
		100,
		100,
		NVL(tx_procedimento_w,100),
		'N',
		'N',
		0,
		nm_usuario_p,
		nr_atendimento_w,
		cd_setor_atendimento_w,
		dt_entrada_unidade_w,
		nr_seq_interno_w,
		'N',
		'N',
		obter_cgc_estabelecimento(cd_estabelecimento_w),
		nr_seq_proc_interno_w,
		dt_inicio_proced_w,
		SYSDATE,
		nm_usuario_p,
		nr_seq_pepo_p,
		dt_termino_w,
		nr_doc_convenio_w,
		ie_tipo_guia_w,
		cd_senha_w,
		ie_via_acesso_w);
	COMMIT;
	atualiza_preco_procedimento(nr_seq_proc_w,cd_convenio_w,nm_usuario_p);

--	begin
	IF	(ie_origem_proced_w = 7) AND
		(cd_medico_anestesista_w IS NOT NULL) THEN
		BEGIN

		SELECT	NVL(MAX(ie_responsavel_credito),''),
			NVL(MAX(cd_cgc_prestador),'')
		INTO	ie_responsavel_credito_w,
			cd_cgc_prestador_w
		FROM	procedimento_paciente
		WHERE	nr_sequencia	= nr_seq_proc_w;

		cd_cbo_w := sus_obter_cbo_medico(cd_medico_anestesista_w, cd_procedimento_w, dt_inicio_proced_w, sus_obter_indicador_equipe(cd_funcao_w));

		sus_atualiza_doc_exec
			(cd_procedimento_w,
			ie_origem_proced_w,
			cd_medico_anestesista_w,
			cd_medico_anestesista_w,
			cd_cgc_prestador_w,
			wheb_usuario_pck.get_cd_estabelecimento,
			cd_convenio_w,
			ie_responsavel_credito_w,
			cd_funcao_w,
			cd_cbo_w,
			ie_doc_executor_w,
			nr_seq_regra_w);

		END;
	END IF;

	SELECT 	MAX(nr_sequencia + 1)
	INTO	nr_seq_partic_w
	FROM 	procedimento_participante;

	SELECT 	MIN(cd_funcao)
	INTO	cd_funcao_w
	FROM 	funcao_medico
	WHERE 	ie_anestesista = 'S';

	INSERT INTO procedimento_participante(
		nr_sequencia,
		nr_seq_partic,
		ie_funcao,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica,
		cd_cgc,
		ie_valor_informado,
		ie_emite_conta,
		vl_participante,
		vl_conta,
		nr_lote_contabil,
		nr_conta_medico,
		cd_especialidade,
		cd_cbo,
		ie_doc_executor,
		ie_participou_sus
		)
	VALUES	(nr_seq_proc_w,
		nr_seq_partic_w,
		cd_funcao_w,
		SYSDATE,
		nm_usuario_p,
		cd_medico_anestesista_w,
		cd_cgc_retorno_w,
		'N',
		ie_emite_conta_w,
		0,
		0,
		0,
		0,
		cd_especialidade_w,
		cd_cbo_w,
		ie_doc_executor_w,
		'S');


	IF 	(cd_funcao_w IS NOT NULL) THEN
		SELECT	pepo_participante_seq.NEXTVAL
		INTO	nr_seq_partic_w
		FROM 	dual;

		INSERT INTO pepo_participante(
			nr_seq_pepo,
			nr_sequencia,
			ie_funcao,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_pessoa_fisica)
		VALUES(
			nr_seq_pepo_p,
			nr_seq_partic_w,
			cd_funcao_w,
			nm_usuario_p,
			SYSDATE,
			nm_usuario_p,
			SYSDATE,
			cd_medico_anestesista_w);

		COMMIT;
	END IF;
--	exception
--	when others then
--		nr_seq_partic_w	:= nr_seq_partic_w;
--	end;
	END;
END LOOP;
CLOSE c01;

OPEN c02;
LOOP
FETCH c02 INTO
	nr_seq_proc_interno_ww,
	qt_taxa_w,
	cd_procedimento_ww,
	ie_origem_proced_ww,
	dt_inicio_w,
	dt_fim_w;
EXIT WHEN c02%NOTFOUND;
	BEGIN
	SELECT	procedimento_paciente_seq.NEXTVAL
	INTO	nr_seq_proc_w
	FROM	dual;

	dt_inicio_proced_w	:= NULL;

	IF 	(nr_seq_proc_interno_ww IS NOT NULL) THEN
		obter_proc_tab_interno(nr_seq_proc_interno_ww,0,nr_atendimento_w,0,cd_procedimento_www,ie_origem_proced_www,NULL,NULL);
	END IF;

	IF	(cd_procedimento_www IS NOT NULL)  AND (ie_origem_proced_www IS NOT NULL) THEN
		cd_procedimento_ww	:= cd_procedimento_www;
		ie_origem_proced_ww	:= ie_origem_proced_www;
	END IF;

	SELECT 	MAX(ie_forma_apresentacao)
	INTO	ie_forma_apresentacao_w
	FROM	procedimento
	WHERE	cd_procedimento		= cd_procedimento_ww
	AND	ie_origem_proced	= ie_origem_proced_ww;

	IF	(cd_procedimento_ww > 0) AND (ie_origem_proced_ww > 0) THEN
		cd_procedimento_w	:= cd_procedimento_ww;
		ie_origem_proced_w	:= ie_origem_proced_ww;
	END IF;

	IF	((dt_inicio_w IS NOT NULL) AND (dt_fim_w IS NOT NULL)) THEN
		dt_inicio_procedimento_w	:= dt_inicio_w;
		dt_proc_equip_w			:= dt_fim_w;
	END IF;

	IF	(ie_forma_apresentacao_w IN (2,3,10,11,15)) THEN
		SELECT	NVL(obter_qte_proced_cirurgia(cd_procedimento_ww, ie_origem_proced_ww, dt_proc_equip_w, dt_inicio_procedimento_w),0)
		INTO	qt_procedimento_ww
		FROM	dual;
	END IF;

	IF	(qt_procedimento_ww = 0) THEN
		qt_procedimento_ww := NULL;
	END IF;

	INSERT INTO procedimento_paciente(
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_procedimento,
		cd_pessoa_fisica,
		cd_convenio,
		cd_categoria,
		ie_proc_princ_atend,
		ie_video,
		tx_medico,
		tx_anestesia,
		tx_procedimento,
		ie_valor_informado,
		ie_guia_informada,
		cd_situacao_glosa,
		nm_usuario_original,
		nr_atendimento,
		cd_setor_atendimento,
		dt_entrada_unidade,
		nr_seq_atepacu,
		cd_senha,
		ie_auditoria,
		ie_emite_conta,
		cd_cgc_prestador,
		nr_seq_proc_interno,
		dt_procedimento,
		dt_atualizacao,
		nm_usuario,
		nr_seq_pepo,
		nr_prescricao,
		dt_inicio_procedimento,
		nr_doc_convenio,
		ie_tipo_guia)
	VALUES(
		nr_seq_proc_w,
		cd_procedimento_ww,
		ie_origem_proced_ww,
		NVL(NVL(qt_procedimento_ww,qt_taxa_w),1),
		DECODE(ie_origem_proced_w,7,NULL,obter_pessoa_fisica_usuario(nm_usuario_p,'C')),
		cd_convenio_w,
		cd_categoria_w,
		'N',
		'N',
		100,
		100,
		100,
		'N',
		'N',
		0,
		nm_usuario_p,
		nr_atendimento_w,
		cd_setor_atendimento_w,
		dt_entrada_unidade_w,
		nr_seq_interno_w,
		cd_senha_w,
		'N',
		'N',
		obter_cgc_estabelecimento(cd_estabelecimento_w),
		nr_seq_proc_interno_ww,
		SYSDATE,
		SYSDATE,
		nm_usuario_p,
		nr_seq_pepo_p,
		NULL,
		dt_inicio_w,
		nr_doc_convenio_w,
		ie_tipo_guia_w);
	COMMIT;

	atualiza_preco_procedimento(nr_seq_proc_w,cd_convenio_w,nm_usuario_p);

	END;
END LOOP;
CLOSE c02;

OPEN c03;
LOOP
FETCH c03 INTO
	nr_sequencia_w,
	cd_material_w,
	qt_material_w;
EXIT WHEN c03%NOTFOUND;
	BEGIN

	gerar_lancto_auto_conta_fanep(	nr_atendimento_w,
					nr_seq_pepo_p,
					cd_material_w,
					qt_material_w,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					nm_usuario_p);

	UPDATE	material_equip_cirurgia
	SET	dt_lancamento_automatico	=	SYSDATE
	WHERE	nr_sequencia			=	nr_sequencia_w;
	COMMIT;
	END;
END LOOP;
CLOSE c03;

COMMIT;

END exec_proc_pac_fanep;
/
