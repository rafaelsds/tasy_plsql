create or replace
procedure hd_desfazer_inicio_dialise(nr_seq_dialise_p		number,
					nr_seq_dialisador_p	number) is 

ie_status_ant_w	varchar2(15);

begin
update 	hd_dialise
set    	dt_inicio_dialise	= null,
	cd_pf_inicio_dialise   	= null
where	nr_sequencia           	= nr_seq_dialise_p;

select	max(ie_status_ant)
into	ie_status_ant_w
from	hd_dializador
where	nr_sequencia = nr_seq_dialisador_p;

if	(ie_status_ant_w is not null) then
	update	hd_dializador
	set	ie_status = ie_status_ant
	where	nr_sequencia = nr_seq_dialisador_p;	
end if;	


commit;

end hd_desfazer_inicio_dialise;
/