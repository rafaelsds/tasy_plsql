create or replace
procedure mp_insere_objeto(	nr_seq_processo_p	in	number,
				nr_seq_objeto_p		in	number,
				qt_topo_p		in	number,
				qt_esquerda_p		in	number,
				qt_altura_p		in	number,
				qt_largura_p		in	number,
				nr_seq_obj_origem_p	in	number,
				nr_seq_obj_destino_p	in	number,
				ds_lista_pontos_p	in	varchar2,
				nm_usuario_p		in	varchar2,
				nr_sequencia_p		out	number,
				cd_estabelecimento_p	in	number,
				nr_seq_projeto_p	in	number,
				ie_tipo_mapeamento_p	in	varchar2) is

nr_sequencia_w		number(10);
ds_objeto_w		varchar2(80);
ds_cor_w		varchar2(15);
nr_seq_grupo_w		number(10);
qt_largura_w		number(10);
qt_altura_w		number(10);
ie_estilo_objeto_w	number(10);

qt_objeto_w		number(10);
ie_tipo_grupo_w		varchar2(2);
nr_seq_processo_ref_w	number(10);
ie_proc_raiz_w		varchar2(1)	:= 'N';
ie_orientacao_w		varchar2(1)	:= 'P';

begin

if	(nr_seq_processo_p is not null) and
	(nr_seq_objeto_p is not null) then

	select	ds_objeto,
		ds_cor,
		nr_seq_grupo,
		qt_largura,
		qt_altura,
		ie_estilo_objeto,
		ie_proc_raiz
	into	ds_objeto_w,
		ds_cor_w,
		nr_seq_grupo_w,
		qt_largura_w,
		qt_altura_w,
		ie_estilo_objeto_w,
		ie_proc_raiz_w
	from	mp_objeto
	where	nr_sequencia	= nr_seq_objeto_p;

	select	count(*) + 1
	into	qt_objeto_w
	from	mp_processo_objeto
	where	nr_seq_processo		= nr_seq_processo_p
	and	nr_seq_objeto		= nr_seq_objeto_p;

	select	ie_tipo_grupo
	into	ie_tipo_grupo_w
	from	mp_grupo_objeto
	where	nr_sequencia	= nr_seq_grupo_w;

	/*ds_objeto_w	:= ds_objeto_w || '_' || qt_objeto_w; */

	if	(ie_proc_raiz_w	= 'N') then

		select	mp_processo_objeto_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert	into	mp_processo_objeto
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_processo,
			nr_seq_objeto,
			qt_topo,
			qt_esquerda,
			qt_altura,
			qt_largura,
			ds_cor,
			nm_objeto,
			ds_objeto,
			nr_seq_apres,
			nr_seq_obj_origem,
			nr_seq_obj_destino,
			ds_lista_pontos,
			pr_aderencia,
			pr_aderencia_es)
		values	(nr_sequencia_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_processo_p,
			nr_seq_objeto_p,
			qt_topo_p,
			qt_esquerda_p,
			nvl(qt_altura_p,qt_altura_w),
			nvl(qt_largura_p,qt_largura_w),
			ds_cor_w,
			null,
			null,
			null,
			nr_seq_obj_origem_p,
			nr_seq_obj_destino_p,
			ds_lista_pontos_p,
			0,
			0);

		mp_atualizar_unid_org(nr_sequencia_w,nm_usuario_p);
	end if;

	if	(ie_tipo_grupo_w = 'P') then
		select	max(ie_orientacao_diagrama)
		into	ie_orientacao_w
		from	mp_configuracao
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	nm_usuario_param	= nm_usuario_p;
		
		if	(ie_orientacao_w is null) then
			select	max(ie_orientacao_diagrama)
			into	ie_orientacao_w
			from	mp_configuracao
			where	cd_estabelecimento	= cd_estabelecimento_p
			and	nm_usuario_param	is null;
		end if;

		select	mp_processo_seq.nextval
		into	nr_seq_processo_ref_w
		from	dual;

		insert	into	mp_processo
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_processo,
			cd_estabelecimento,
			nr_seq_superior,
			nr_seq_objeto,
			nr_seq_projeto,
			ie_tipo_mapeamento,
			cd_empresa,
			ie_status_versao,
			ie_orientacao_diagrama)
		values	(nr_seq_processo_ref_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			' ',
			cd_estabelecimento_p,
			nr_seq_processo_p,/*decode(ie_proc_raiz_w,'N',nr_seq_processo_p,null),*/
			nr_seq_objeto_p,
			nr_seq_projeto_p,
			ie_tipo_mapeamento_p,
			obter_empresa_estab(cd_estabelecimento_p),
			'R',
			ie_orientacao_w);

		update	mp_processo_objeto
		set	nr_seq_processo_ref	= nr_seq_processo_ref_w
		where	nr_sequencia		= nr_sequencia_w;
	end if;
end if;

nr_sequencia_p	:= nr_sequencia_w;

commit;

end mp_insere_objeto;
/