create or replace
procedure gerar_pend_conta_prot_doc(
			nr_seq_protocolo_p	number,
			nr_seq_interno_p	number, 
			nr_seq_tipo_p		number,
			ds_erro_p	out	varchar2,	
			nm_usuario_p		Varchar2) is 
cursor c01 is
	select	distinct
		nr_seq_interno
	from	protocolo_doc_item 
	where	nr_sequencia 		= nr_seq_protocolo_p	
	and	(nr_seq_interno 	= nr_seq_interno_p or nvl(nr_seq_interno_p,0) = 0);
nr_interno_conta_w 	number(15);
begin
nr_interno_conta_w := 0;
open c01;
loop
fetch c01 into nr_interno_conta_w;
exit when c01%notfound;
	begin
		if	(nr_seq_protocolo_p is not null) and
			(nr_seq_interno_p is not null) and
			(nr_seq_tipo_p is not null) and
			(nm_usuario_p is not null) then
			gerar_pendencia_conta(nr_interno_conta_w, nr_seq_tipo_p, nm_usuario_p, ds_erro_p);
		end if;
	end;	
end loop;
close c01;
commit;
end gerar_pend_conta_prot_doc;
/