create or replace procedure silab_gerar_titulo_receber 
			(	cd_pessoa_fisica_p		varchar2,
				dt_emissao_p			date,
				dt_vencimento_p			date,
				nr_titulo_p				number,
				vl_titulo_p				number, 
				ds_erro_p				out varchar2,
				vl_saldo_titulo_p		number default null,	
				nr_seq_alt_valor_p		number default null,	
				nr_seq_alt_vencimento_p	number default null,	
				nr_seq_alt_portador_p	number default null,	
				ie_origem_integracao_p	varchar2 default null,
				dt_vencimento_prorrog_p	date default null, 
				cd_portador_p			number default null,  
				cd_tipo_portador_p		number default null,         
				cd_estabelecimento_p    number default null, 
				cd_tipo_taxa_juro_p		number default null,        
				cd_tipo_taxa_multa_p	number default null,       				
				ie_origem_titulo_p		varchar2 default null,         
				ie_tipo_titulo_p		varchar2 default null,           
				nr_seq_nf_saida_p		number default null,          
				tx_juros_p				number default null,                 
				tx_multa_p				number default null,                 
				vl_juros_p				number default null,                 
				vl_multa_p				number default null
			) is

cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;		
			
begin

begin
	select 	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pessoa_fisica
	where	cd_sistema_ant = cd_pessoa_fisica_p;

	insert into silab_titulo_receber
		(
		cd_estabelecimento,      
		cd_pessoa_fisica,        
		cd_portador,             
		cd_tipo_portador,        
		cd_tipo_taxa_juro,       
		cd_tipo_taxa_multa,      
		ds_erro,                 
		dt_atualizacao,          
		dt_atualizacao_nrec,     
		dt_emissao,              
		dt_vencimento,          
		dt_vencimento_prorrog,
		ie_origem_integracao,    
		ie_origem_titulo,        
		ie_status_integracao,  -- "N" Pendente / "S" Integrado / "E" Erro
		ie_tipo_titulo,          
		nm_usuario,              
		nm_usuario_nrec,         
		nr_seq_nf_saida,         
		nr_sequencia,            
		nr_titulo,               
		tx_juros,                
		tx_multa,                
		vl_juros,                
		vl_multa,                
		vl_titulo,
		vl_saldo_titulo,
		nr_seq_alt_valor,	
		nr_seq_alt_vencimento,
		nr_seq_alt_portador,
		dt_integracao
		)
	values
		(
		nvl(cd_estabelecimento_p,wheb_usuario_pck.get_cd_estabelecimento),
		cd_pessoa_fisica_p,
		cd_portador_p,
		cd_tipo_portador_p,
		cd_tipo_taxa_juro_p,       
		cd_tipo_taxa_multa_p,
  		null,
		sysdate,
		sysdate,
		dt_emissao_p,
		dt_vencimento_p,
		nvl(dt_vencimento_prorrog_p,dt_vencimento_p),
		nvl(ie_origem_integracao_p,'Silab'),
		nvl(ie_origem_titulo_p,'9'), -- Outros
		'N',
		nvl(ie_tipo_titulo_p,'1'),
		'Silab',
		'Silab',
		nr_seq_nf_saida_p,
		silab_titulo_receber_seq.nextval,
		nr_titulo_p,
		tx_juros_p,
		tx_multa_p,
		vl_juros_p,
		vl_multa_p,
		vl_titulo_p,
		nvl(vl_saldo_titulo_p,vl_titulo_p),
		nr_seq_alt_valor_p,	
		nr_seq_alt_vencimento_p,	
		nr_seq_alt_portador_p,
		sysdate		
		);
		
	commit;
		
exception
	when others then
		ds_erro_p := substr(sqlerrm,1,255);
end;	

end silab_gerar_titulo_receber;
/
