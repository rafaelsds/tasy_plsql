create or replace
procedure gerar_dieta_acompanhantes(	cd_setor_atendimento_p	number,
					dt_dieta_p		date,
					cd_refeicao_p		varchar2,
					cd_dieta_acomp_p	number,
					nm_usuario_p		varchar2 )
					is


nr_sequencia_w		number(10,0);
nr_atendimento_w	number(10,0);
nr_seq_acomp_w		number(10,0);
ie_gerar_uti_w		varchar2(01);
cd_dieta_w		number(10,0);
ie_parametro_w		varchar2(1);
qt_dieta_acomp_w	number(3);
qt_dieta_w		number(3);
ie_gera_dieta_w		varchar2(1);
ie_lib_dieta_w		varchar2(1);
ie_regra_ref_lib_w	varchar2(1);

cursor	C01 is
	select	a.nr_sequencia,
		a.nr_atendimento
	from	setor_atendimento b,
		mapa_dieta a
	where	a.cd_setor_atendimento	= cd_setor_atendimento_p
	and	a.cd_setor_atendimento	= b.cd_setor_atendimento
	and	((ie_gerar_uti_w = 'S') or (b.cd_classif_setor <> '4'))
	and	a.dt_dieta		= dt_dieta_p
	and	a.cd_refeicao		= cd_refeicao_p
	and	a.ie_destino_dieta	= 'P';			

begin

select	nvl(obter_valor_param_usuario(1000, 24, Obter_Perfil_Ativo, nm_usuario_p, 1),'S')
into	ie_gerar_uti_w
from	dual;

OPEN C01;
LOOP
FETCH C01 into
	nr_sequencia_w,
	nr_atendimento_w;
exit when C01%notfound;
	begin
	
	ie_gera_dieta_w	:= 'S';
	
	select	max(nr_sequencia)
	into	nr_seq_acomp_w
	from	mapa_dieta
	where	nr_atendimento		=	nr_atendimento_w
	and	ie_destino_dieta	=	'A'
	and	not exists (	select	1
				from	mapa_dieta
				where	cd_setor_atendimento	=	cd_setor_atendimento_p
				and	dt_dieta		=	dt_dieta_p
				and	cd_refeicao		=	cd_refeicao_p
				and	ie_destino_dieta	=	'A'
				and	nr_atendimento		=	nr_atendimento_w);
				
	if	(ie_parametro_w = 'S') then
		
		ie_gera_dieta_w	:= 'N';

		select	max(ie_lib_dieta),
			max(qt_dieta_acomp)
		into	ie_lib_dieta_w,
			qt_dieta_acomp_w
		from	atend_categoria_convenio
		where	nr_atendimento	= nr_atendimento_w
		and	dt_inicio_vigencia = (	select	max(dt_inicio_vigencia)
						from	atend_categoria_convenio
						where	nr_atendimento = nr_atendimento_w);

		begin
		
		select	count(*)
		into	qt_dieta_w
		from	mapa_dieta
		where	nr_atendimento		= nr_atendimento_w
		and	ie_destino_dieta	= 'A'
		and	((cd_refeicao		= cd_refeicao_p) or (ie_lib_dieta_w = 'S'))
		and	trunc(dt_dieta,'dd') 	= trunc(dt_dieta_p,'dd');
		exception
			when no_data_found then
				qt_dieta_w 	:= 0;
		end;		

		if	((ie_lib_dieta_w = 'T')) or
			((ie_lib_dieta_w = 'C') and (cd_refeicao_p = 'D')) or
			((ie_lib_dieta_w = 'B') and (cd_refeicao_p = 'A')) or
			((ie_lib_dieta_w = 'E') and (cd_refeicao_p = 'J')) or
			((ie_lib_dieta_w = 'G') and (cd_refeicao_p = 'D') and (cd_refeicao_p = 'J')) or
			((ie_lib_dieta_w = 'L') and ((cd_refeicao_p = 'D') or (cd_refeicao_p = 'A') or (cd_refeicao_p = 'J'))) or
			((ie_lib_dieta_w = 'A') and ((cd_refeicao_p = 'A') or (cd_refeicao_p = 'J'))) or
			((ie_lib_dieta_w = 'D') and ((cd_refeicao_p = 'D') or (cd_refeicao_p = 'L') or (cd_refeicao_p = 'C'))) or
			((ie_lib_dieta_w = 'F') and ((cd_refeicao_p = 'D') or (cd_refeicao_p = 'A'))) or
			((ie_lib_dieta_w = 'S') and (qt_dieta_w < qt_dieta_acomp_w)) then
			ie_gera_dieta_w	:= 'S';
		end if;

	end if;	
	
	ie_regra_ref_lib_w := Obter_se_refeicao_lib_acomp(nr_atendimento_w);
	if (ie_regra_ref_lib_w = 'N') then
		ie_gera_dieta_w := 'N';
	end if;

	if	(nr_seq_acomp_w > 0) and
		(ie_gera_dieta_w = 'S') then	
	
		cd_dieta_w	:= cd_dieta_acomp_p;
		
		if	(cd_dieta_acomp_p = 0) then
	
			select 	nvl(max(cd_dieta),0)
			into	cd_dieta_w
			from	mapa_dieta
			where	nr_sequencia = nr_seq_acomp_w;
		end if;
	
		duplicar_dieta(nr_sequencia_w, 'A', nm_usuario_p, cd_dieta_w); 
	end if;

	end;
END LOOP;
CLOSE C01;

end gerar_dieta_acompanhantes;
/