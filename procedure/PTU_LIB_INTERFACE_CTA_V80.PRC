create or replace
procedure ptu_lib_interface_cta_v80 is

begin

-- A1200
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2787, 'A1200 - Pacotes e Tabelas Contratualizadas - 8.0', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', ' select substr(ds_conteudo,1,255) ds_conteudo, substr(ds_conteudo,256,255) ds_conteudo_1, substr(ds_conteudo,511,255) ds_conteudo_2 from w_ptu_envio_arq where nm_usuario = :nm_usuario_cor order by nr_seq_apres ', null, '', '', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_REG (CD_INTERFACE, CD_REG_INTERFACE, DS_REG_INTERFACE, IE_SEPARADOR_REG, IE_FORMATO_REG, DT_ATUALIZACAO, NM_USUARIO, IE_REGISTRO, IE_TIPO_REGISTRO)
values (2787, 1, 'Conteudo', 'N', 'V', sysdate, 'wcbernardino', '1', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2787, 1, 1, '', 'DS_CONTEUDO', 'VARCHAR2', 255, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2787, 1, 2, '', 'DS_CONTEUDO_1', 'VARCHAR2', 255, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2787, 1, 3, '', 'DS_CONTEUDO_2', 'VARCHAR2', 255, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

-- A400 - UTL
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2786, 'A400 - Movimenta��o Cadastral de Prestador - 8.0 - UTL_FILE', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', '', null, '', '', 'N');
exception
when others then
	null;
end;

-- A450 - UTL
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2781, 'A450 - Complemento de Dados - Guia m�dico - 8.0 - UTL_FILE', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', '', null, '', '', 'N');
exception
when others then
	null;
end;

-- A500
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2789, 'A500 - Notas de Fatura em Interc�mbio 8.0', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', ' select ds_conteudo from w_ptu_envio_arq where nm_usuario = :nm_usuario_cor order by nr_seq_apres ', null, '', '', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_REG (CD_INTERFACE, CD_REG_INTERFACE, DS_REG_INTERFACE, IE_SEPARADOR_REG, IE_FORMATO_REG, DT_ATUALIZACAO, NM_USUARIO, IE_REGISTRO, IE_TIPO_REGISTRO)
values (2789, 1, 'Conte�do', 'N', 'V', sysdate, 'wcbernardino', '1', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2789, 1, 1, '', 'DS_CONTEUDO', 'VARCHAR2', 930, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

-- A500 - UTL
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2791, 'A500 - Notas de Fatura em Interc�mbio 8.0 - UTL_FILE', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', '', null, '', '', 'N');
exception
when others then
	null;
end;

-- A550
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2790, 'A550 - Questionamento da C�mara de Contesta��o 8.0', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', ' select substr(ds_conteudo,1,255) ds_conteudo, substr(ds_conteudo,256,255) ds_conteudo_1, substr(ds_conteudo,511,255) ds_conteudo_2 from w_ptu_envio_arq where nm_usuario = :nm_usuario_cor order by nr_sequencia ', null, '', '', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_REG (CD_INTERFACE, CD_REG_INTERFACE, DS_REG_INTERFACE, IE_SEPARADOR_REG, IE_FORMATO_REG, DT_ATUALIZACAO, NM_USUARIO, IE_REGISTRO, IE_TIPO_REGISTRO)
values (2790, 1, 'Conte�do', 'N', 'V', sysdate, 'wcbernardino', '1', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2790, 1, 1, '', 'DS_CONTEUDO', 'VARCHAR2', 255, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2790, 1, 2, '', 'DS_CONTEUDO_1', 'VARCHAR2', 255, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2790, 1, 3, '', 'DS_CONTEUDO_2', 'VARCHAR2', 255, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

-- A560
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2782, 'A560 - Carga para Nota de D�bito 8.0', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', ' select ds_conteudo from w_ptu_envio_arq where nm_usuario = :nm_usuario_cor order by nr_sequencia ', null, '', '', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_REG (CD_INTERFACE, CD_REG_INTERFACE, DS_REG_INTERFACE, IE_SEPARADOR_REG, IE_FORMATO_REG, DT_ATUALIZACAO, NM_USUARIO, IE_REGISTRO, IE_TIPO_REGISTRO)
values (2782, 1, 'Conte�do geral - Procedure', 'N', 'V', sysdate, 'wcbernardino', '1', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2782, 1, 1, '', 'DS_CONTEUDO', 'VARCHAR2', 300, sysdate, 'wcbernardino', null, '', ' ', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

-- A580
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2780, 'A580 - Fatura de Uso Geral 8.0', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', ' select ds_conteudo from w_ptu_envio_arq where nm_usuario = :nm_usuario_cor order by nr_seq_apres ', null, '', '', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_REG (CD_INTERFACE, CD_REG_INTERFACE, DS_REG_INTERFACE, IE_SEPARADOR_REG, IE_FORMATO_REG, DT_ATUALIZACAO, NM_USUARIO, IE_REGISTRO, IE_TIPO_REGISTRO)
values (2780, 1, 'Conte�do', 'N', 'V', sysdate, 'wcbernardino', '1', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2780, 1, 1, '', 'DS_CONTEUDO', 'VARCHAR2', 130, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

-- A700
begin
insert into INTERFACE (CD_INTERFACE, DS_INTERFACE, NM_ARQUIVO_SAIDA, DT_ATUALIZACAO, NM_USUARIO, IE_IMPLANTAR, CD_TIPO_INTERFACE, DS_COMANDO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_TIPO_PTU, IE_IMP_EXP)
values (2788, 'A700 - Servi�os prestados em Pr�-Pagamento 8.0', 'C:', sysdate, 'wcbernardino', 'N', 'PTU8.0', ' select ds_conteudo from w_ptu_envio_arq where nm_usuario = :nm_usuario_cor order by nr_seq_apres', null, '', '', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_REG (CD_INTERFACE, CD_REG_INTERFACE, DS_REG_INTERFACE, IE_SEPARADOR_REG, IE_FORMATO_REG, DT_ATUALIZACAO, NM_USUARIO, IE_REGISTRO, IE_TIPO_REGISTRO)
values (2788, 1, 'Conte�do', 'N', 'V', sysdate, 'wcbernardino', '1', '');
exception
when others then
	null;
end;

begin
insert into INTERFACE_ATRIBUTO (CD_INTERFACE, CD_REG_INTERFACE, NR_SEQ_ATRIBUTO, NM_TABELA, NM_ATRIBUTO, IE_TIPO_ATRIBUTO, QT_TAMANHO, DT_ATUALIZACAO, NM_USUARIO, QT_DECIMAIS, DS_MASCARA_DATA, DS_VALOR, QT_POSICAO_INICIAL, IE_IMPORTA_TABELA, DS_REGRA_VALIDACAO, IE_IDENTIFICA_ERRO, IE_EXPORTA, IE_TIPO_CAMPO, IE_CONVERSAO, NM_ATRIB_USUARIO)
values (2788, 1, 1, '', 'DS_CONTEUDO', 'VARCHAR2', 800, sysdate, 'wcbernardino', null, '', '', null, 'S', '', 'N', 'S', 'N', 'S', '');
exception
when others then
	null;
end;

commit;

end ptu_lib_interface_cta_v80;
/