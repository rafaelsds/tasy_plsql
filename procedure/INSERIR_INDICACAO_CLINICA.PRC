CREATE OR REPLACE
PROCEDURE Inserir_indicacao_clinica(
			nr_seq_solic_p		Number,
			nr_seq_indicacao_p	Number,
			nm_usuario_p		Varchar2) IS 

ie_tipo_hemoc_w		varchar2(3);

BEGIN

select	max(ie_tipo_hemoc)
into		ie_tipo_hemoc_w
from		san_indicacao
where	nr_sequencia	= nr_seq_indicacao_p;

insert	into prescr_sol_bs_indicacao(
		nr_sequencia,
		nr_seq_solic_bs,
		dt_atualizacao,
		nm_usuario,
		nr_seq_indicacao,
		ds_indicacao_adic,
		ie_grupo_hemocom)
select	prescr_sol_bs_indicacao_seq.nextval,
		nr_seq_solic_p,
		sysdate,
		nm_usuario_p,
		nr_seq_indicacao_p,
		null,
		ie_tipo_hemoc_w
from		dual;

commit;

END Inserir_indicacao_clinica;
/