CREATE OR REPLACE procedure recalcular_sessoes_servico	(nr_seq_agenda_p	number) is

cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
nr_seq_proc_interno_w		number(10,0);
nr_secao_w			number(3,0);
nr_seq_agenda_w			number(10,0);
nr_controle_secao_w		agenda_consulta.NR_CONTROLE_SECAO%type;
dt_agenda_w			date;
ie_status_f_w			varchar2(1) := '';
ie_status_fj_w			varchar2(2) := '';
ie_status_i_w			varchar2(1) := '';
ie_recalcula_sessao_atual_w	varchar2(1);

cursor c01 is
	select	nr_sequencia
	from	agenda_consulta
	where	ie_status_agenda 	not in ('C', 'B', 'L', 'II', ie_status_f_w, ie_status_fj_w, ie_status_i_w)
	and 	dt_agenda between sysdate and dt_agenda_w
	AND	cd_pessoa_fisica	= cd_pessoa_fisica_w
	AND	nr_controle_secao	= nr_controle_secao_w
	AND	qt_total_secao	>= nr_secao
	order by dt_agenda;

begin
if	(NVL(nr_seq_agenda_p,0) > 0) then
	Obter_Param_Usuario(866, 244, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_recalcula_sessao_atual_w);
	if (ie_recalcula_sessao_atual_w in ('S','A','C')) then
		ie_status_f_w := 'F';
		ie_status_fj_w := 'FJ';
		ie_status_i_w := 'I';
	end if;
	select	max(cd_pessoa_fisica),
		MAX(NR_CONTROLE_SECAO)
	into	cd_pessoa_fisica_w,
		nr_controle_secao_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_p;

	select 	max(dt_agenda)+1
	into 	dt_agenda_w
	from 	agenda_consulta
	where 	nr_controle_secao	= nr_controle_secao_w;

	select 	max(nr_sequencia)
	into	nr_seq_agenda_w
	from 	agenda_consulta
	where 	nr_controle_secao	= nr_controle_secao_w
	and 	cd_pessoa_fisica = cd_pessoa_fisica_w
	and 	dt_agenda between sysdate and dt_agenda_w
	and 	ie_status_agenda not in ('C', 'B', 'L', 'II', ie_status_f_w, ie_status_fj_w, ie_status_i_w);

	select	max(nr_atendimento),
		max(cd_procedimento),
		max(ie_origem_proced),
		max(nr_seq_proc_interno),
		max(nr_secao),
		MAX(NR_CONTROLE_SECAO)
	into	nr_atendimento_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		nr_secao_w,
		nr_controle_secao_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_w;

	if	(nr_secao_w is not null) and
		(nr_secao_w = 1) then
		nr_secao_w := 0;
	else
		SELECT	nvl(MAX(nr_secao),0)
		into	nr_secao_w
		FROM	agenda_consulta
		WHERE	ie_status_agenda	not in ('C', 'B', 'L', 'II', ie_status_f_w, ie_status_fj_w, ie_status_i_w)
		AND	dt_agenda		< sysdate
		AND	cd_pessoa_fisica	= cd_pessoa_fisica_w
		AND	nr_controle_secao	= nr_controle_secao_w
		AND	qt_total_secao	> nr_secao;
	end if;

	open c01;
	loop
	fetch c01 into	nr_seq_agenda_w;
		exit when c01%notfound;
		begin
		update	agenda_consulta
		set	nr_secao	= nr_secao_w + 1
		where	nr_sequencia	= nr_seq_agenda_w;

		nr_secao_w	:= nr_secao_w + 1;
		end;
	end loop;
	close c01;
end if;

commit;

end recalcular_sessoes_servico;
/