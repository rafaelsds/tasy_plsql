create or replace PROCEDURE gerar_prescr_proc_sem_dt_lib (
    nr_prescricao_p  NUMBER,
    nr_seq_item_p    NUMBER,
    cd_perfil_p      NUMBER,
    ie_liberacao_p   VARCHAR2,
    nm_usuario_p     VARCHAR2,
    ie_tipo_item_p   VARCHAR2 DEFAULT NULL
) IS

    ds_horarios_w                 VARCHAR2(2000);
    ds_horarios_padr_w            VARCHAR2(2000);
    ds_hora_w                     VARCHAR2(2000);
    dt_liberacao_w                DATE;
    dt_horario_w                  DATE;
    k                             INTEGER;
    y                             INTEGER;
    nr_sequencia_w                NUMBER(10);
    nr_seq_prescr_w               NUMBER(6);
    qt_dose_w                     NUMBER(18, 6);
    qt_total_dispensar_w          NUMBER(18, 6);
    cd_material_w                 NUMBER(6);
    dt_primeiro_horario_w         DATE;
    ie_agrupador_w                NUMBER(2);
    cd_intervalo_w                VARCHAR2(7);
    ie_prescricao_dieta_w         VARCHAR2(1);
    nr_ocorrencia_w               NUMBER(15, 4);
    ie_esquema_alternado_w        VARCHAR2(1);
    nr_seq_solucao_w              NUMBER(6);
    ie_agrupador_dil_w            NUMBER(2);
    nr_sequencia_dil_w            NUMBER(10);
    qt_dose_dil_w                 NUMBER(18, 6);
    qt_total_disp_dil_w           NUMBER(18, 6);
    nr_ocorrencia_dil_w           NUMBER(15, 4);
    cd_material_dil_w             NUMBER(6);
    hr_prim_horario_w             VARCHAR2(5);
    ie_urgente_w                  VARCHAR2(1);
    qt_conversao_w                NUMBER(18, 6);
    qt_dose_especial_w            NUMBER(18, 6);
    hr_dose_especial_w            VARCHAR2(5);
    nr_seq_procedimento_w         NUMBER(6, 0);
    cd_procedimento_w             NUMBER(15, 0);
    ie_origem_proced_w            NUMBER(10, 0);
    nr_seq_proc_interno_w         NUMBER(10, 0);
    nr_ocor_proc_w                NUMBER(15, 4);
    ie_proc_urgente_w             VARCHAR2(1);
    dt_prev_execucao_w            DATE;
    ds_hora_proc_w                VARCHAR2(2000);
    dt_horario_proc_w             DATE;
    ds_horarios_proc_w            VARCHAR2(2000);
    ds_horarios_padrao_proc_w     VARCHAR2(2000);
    cd_material_exame_w           VARCHAR2(20);
    nr_seq_recomendacao_w         NUMBER(6, 0);
    cd_recomendacao_w             NUMBER(10, 0);
    ds_hora_rec_w                 VARCHAR(2000);
    dt_horario_rec_w              DATE;
    nr_seq_classif_rec_w          NUMBER(10, 0);
    ds_horarios_rec_w             VARCHAR2(2000);
    ds_horarios_padrao_rec_w      VARCHAR2(2000);
    ie_se_necessario_w            VARCHAR2(1);
    ie_acm_w                      VARCHAR2(1);
    ie_horario_especial_w         VARCHAR2(1) := 'N';
    qt_dieta_w                    NUMBER(10, 0);
    cd_setor_atendimento_w        NUMBER(5);
    cd_estabelecimento_w          NUMBER(4);
    dt_prescricao_w               DATE;
    dt_prescricao_ww              DATE;
    qt_dia_adic_w                 NUMBER(10) := 0;
    qt_registro_w                 NUMBER(10);
    dt_inicio_prescr_w            DATE;
    nr_seq_procedimento_novo_w    NUMBER(6);
    nr_seq_exame_w                NUMBER(10);
    ie_status_atend_w             NUMBER(2);
    ie_status_execucao_w          VARCHAR2(3);
    cd_setor_atendimento_proc_w   NUMBER(5);
    cd_setor_coleta_w             NUMBER(5);
    cd_setor_entrega_w            NUMBER(5);
    cd_setor_exec_fim_w           NUMBER(5);
    cd_setor_exec_inic_w          NUMBER(5);
    nr_seq_lab_w                  VARCHAR2(20);
    ie_gerar_proc_intervalo_w     VARCHAR2(1);
    ie_suspenso_w                 VARCHAR2(1);
    ds_observacao_w               VARCHAR2(2000);
    ds_dado_clinico_w             VARCHAR2(2000);
    ds_material_especial_w        VARCHAR2(255);
    ie_controlado_w               VARCHAR2(1);
    nr_seq_prescr_hor_w           NUMBER(6);
    dt_horario_proc_prev_w        DATE;
    ie_proc_atual_w               VARCHAR2(1);
    qt_min_agora_w                NUMBER(15);
    qt_min_especial_w             NUMBER(15);
    ie_classif_urgente_w          VARCHAR2(3);
    dt_limite_agora_w             DATE;
    dt_limite_especial_w          DATE;
    nr_seq_classif_w              NUMBER(10);
    dt_liberacao_farmacia_w       DATE;
    ie_amostra_w                  VARCHAR2(1);
    nr_seq_derivado_w             NUMBER(10);
    ie_util_hemocomponente_w      VARCHAR2(15);
    ie_tipo_proced_w              VARCHAR2(15);
    qt_vol_hemocomp_w             NUMBER(15);
    qt_procedimento_w             NUMBER(8, 3);
    ie_lib_individual_w           VARCHAR2(1);
    dt_suspensao_progr_w          DATE;
    nr_seq_etapa_w                NUMBER(10);
    dt_prev_execucao_ww           DATE;
    nr_horas_validade_w           NUMBER(5);
    dt_validade_prescr_w          DATE;
    nr_seq_horario_w              NUMBER(10);
    ie_estendido_w                VARCHAR2(1);
    ie_add_dia_w                  VARCHAR2(1) := 'S';
    dt_resultado_w                DATE;
    nr_seq_proc_cpoe_w            prescr_procedimento.nr_seq_proc_cpoe%TYPE;
    ie_considera_data_prevista_w  VARCHAR2(1);
    ie_tipo_proced_ww             prescr_procedimento.ie_tipo_proced%TYPE;
    nr_seq_prot_glic_w            prescr_procedimento.nr_seq_prot_glic%TYPE;

--Parametros obter_setor_exame_lab
    cd_setor_atual_usuario_w      NUMBER(5);
    cd_setor_atend_w              VARCHAR2(255);
    cd_setor_col_w                VARCHAR2(255);
    cd_setor_entrega_lab_w        VARCHAR2(255) := NULL;
    qt_dia_entrega_w              NUMBER(20, 0);
    ie_emite_mapa_w               VARCHAR2(255);
    ds_hora_fixa_w                VARCHAR2(255);
    ie_data_resultado_w           VARCHAR2(255);
    qt_min_entrega_w              NUMBER(10);
    ie_atualizar_recoleta_w       VARCHAR2(255);
    ie_agora_w                    VARCHAR2(1);
    ie_dia_semana_final_w         NUMBER(10);
    ie_forma_atual_dt_result_w    exame_lab_regra_setor.ie_atul_data_result%TYPE;
    qt_min_atraso_w               NUMBER(10);
    cd_funcao_origem_w            prescr_medica.cd_funcao_origem%TYPE;
    nr_atendimento_w              prescr_medica.nr_atendimento%TYPE;
    cd_pessoa_fisica_w            prescr_medica.cd_pessoa_fisica%TYPE;
    sql_w                         VARCHAR2(200);
    CURSOR c13 IS
    SELECT
        nr_sequencia,
        cd_procedimento,
        ie_origem_proced,
        nr_seq_proc_interno,
        nr_ocorrencia,
        nvl(ie_urgencia, 'N'),
        nvl(dt_prev_execucao, a.dt_inicio_prescr),
        substr(ds_horarios, 1, 2000),
        substr(padroniza_horario_prescr(b.ds_horarios, decode(to_char(b.dt_prev_execucao, 'hh24:mi'), '00:00', to_char(b.dt_prev_execucao,
        'dd/mm/yyyy hh24:mi:ss'), to_char(b.dt_prev_execucao, 'dd/mm/yyyy hh24:mi')
                                                                                                                                         ||
                                                                                                                                         ':00')),
               1,
               2000),
        nvl(ie_se_necessario, 'N'),
        nvl(ie_acm, 'N'),
        cd_material_exame,
        nr_seq_exame,
        b.ie_status_atend,
        b.ie_status_execucao,
        b.cd_setor_atendimento,
        b.cd_setor_coleta,
        b.cd_setor_entrega,
        b.cd_setor_exec_fim,
        b.cd_setor_exec_inic,
        b.nr_seq_lab,
        b.ie_suspenso,
        b.ds_observacao,
        b.ds_dado_clinico,
        b.ds_material_especial,
        b.ie_amostra,
        b.nr_seq_derivado,
        b.ie_util_hemocomponente,
        b.qt_vol_hemocomp,
        b.qt_procedimento,
        b.dt_suspensao_progr,
        obter_se_exibe_proced(b.nr_prescricao, b.nr_sequencia, b.ie_tipo_proced, 'BSST'),
        b.cd_intervalo,
        nr_seq_proc_cpoe,
        b.ie_tipo_proced,
        a.cd_funcao_origem,
        a.nr_atendimento,
        a.cd_pessoa_fisica,
        b.nr_seq_prot_glic
    FROM
        prescr_procedimento  b, 
        prescr_medica        a 
    WHERE
            a.nr_prescricao = b.nr_prescricao
        AND a.nr_prescricao = nr_prescricao_p
        AND ( ( b.nr_sequencia = nr_seq_item_p )
              OR ( nvl(nr_seq_item_p, 0) = 0 ) )
        AND b.nr_seq_origem IS NULL
        AND nvl(b.ie_suspenso, 'N') <> 'S'
        AND ( ( ( ie_tipo_item_p = 'P' )
                AND ( b.nr_seq_exame IS NULL )
                AND ( b.nr_seq_prot_glic IS NULL ) )
              OR ( ( ie_tipo_item_p = 'E' )
                   AND ( b.nr_seq_exame IS NOT NULL ) )
              OR ( ( ie_tipo_item_p = 'G' )
                   AND ( b.nr_seq_prot_glic IS NOT NULL ) )
              OR ( ie_tipo_item_p IS NULL ) )
        AND a.dt_suspensao IS NULL
        AND NOT EXISTS (
            SELECT
                1
            FROM
                prescr_proc_hor x
            WHERE
                    x.nr_prescricao = b.nr_prescricao
                AND x.nr_seq_procedimento = b.nr_sequencia
                AND x.dt_lib_horario IS NOT NULL
        );

    CURSOR c15 IS
    SELECT
        b.dt_horario,
        b.nr_sequencia,
        nvl(c.dt_resultado, '')
    FROM
        prescr_proc_hor      b,
        prescr_medica        a,
        prescr_procedimento  c
    WHERE
            a.nr_prescricao = nr_prescricao_p
        AND a.nr_prescricao = b.nr_prescricao
        AND c.nr_prescricao = a.nr_prescricao
        AND c.nr_sequencia = b.nr_seq_procedimento
        AND b.nr_seq_procedimento = nr_seq_procedimento_w
        AND ( ( ie_gerar_proc_intervalo_w = 'S' )
              OR ( ( ie_gerar_proc_intervalo_w = 'D' )
                   AND ( verifica_se_proc_regra_interv(a.nr_prescricao, b.nr_seq_procedimento) = 'S' ) ) )
        AND ie_lib_individual_w = 'S'
        AND ie_liberacao_p = 'S'
        AND nvl(c.ie_tipo_proced, 'X') NOT IN ( 'AP', 'APH', 'APC' )
    ORDER BY
        b.dt_horario ASC;

BEGIN
    DELETE FROM prescr_proc_hor
    WHERE
            nr_prescricao = nr_prescricao_p
        AND ( ( nr_seq_procedimento = nr_seq_item_p )
              OR ( nvl(nr_seq_item_p, 0) = 0 ) )
        AND dt_lib_horario IS NULL;

    qt_registro_w := 0;
/*select	count(*)
into	qt_registro_w
from	prescr_proc_hor
where	nr_prescricao = nr_prescricao_p
and	((nr_seq_procedimento = nr_seq_item_p) or
	 (nvl(nr_seq_item_p,0) = 0));*/

    obter_param_usuario(924, 530, cd_perfil_p, nm_usuario_p, 0,  ie_lib_individual_w);
    
    
    
    IF ( qt_registro_w = 0 ) THEN
    
    
    
        SELECT
            nvl(dt_liberacao, dt_liberacao_medico),
            dt_primeiro_horario,
            cd_setor_atendimento,
            nvl(cd_estabelecimento, 1),
            trunc(dt_prescricao, 'mi'),
            dt_inicio_prescr,
            dt_liberacao_farmacia,
            nr_horas_validade,
            dt_validade_prescr
        INTO
            dt_liberacao_w,
            dt_primeiro_horario_w,
            cd_setor_atendimento_w,
            cd_estabelecimento_w,
            dt_prescricao_w,
            dt_inicio_prescr_w,
            dt_liberacao_farmacia_w,
            nr_horas_validade_w,
            dt_validade_prescr_w
        FROM
            prescr_medica
        WHERE
            nr_prescricao = nr_prescricao_p;

        SELECT
            MAX(nvl(ie_cons_dt_prev_regra, 'N'))
        INTO ie_considera_data_prevista_w
        FROM
            lab_parametro
        WHERE
            cd_estabelecimento = cd_estabelecimento_w;

        cd_setor_atual_usuario_w := obter_setor_usuario(nm_usuario_p);
        obter_param_usuario(924, 192, cd_perfil_p, nm_usuario_p, cd_estabelecimento_w,
                           ie_gerar_proc_intervalo_w);
        IF ( dt_primeiro_horario_w IS NULL ) THEN
            dt_primeiro_horario_w := dt_prescricao_w;
        END IF;
        qt_dia_adic_w := 0;
        OPEN c13;
        LOOP
            FETCH c13 INTO
                nr_seq_procedimento_w,
                cd_procedimento_w,
                ie_origem_proced_w,
                nr_seq_proc_interno_w,
                nr_ocor_proc_w,
                ie_proc_urgente_w,
                dt_prev_execucao_w,
                ds_horarios_proc_w,
                ds_horarios_padrao_proc_w,
                ie_se_necessario_w,
                ie_acm_w,
                cd_material_exame_w,
                nr_seq_exame_w,
                ie_status_atend_w,
                ie_status_execucao_w,
                cd_setor_atendimento_proc_w,
                cd_setor_coleta_w,
                cd_setor_entrega_w,
                cd_setor_exec_fim_w,
                cd_setor_exec_inic_w,
                nr_seq_lab_w,
                ie_suspenso_w,
                ds_observacao_w,
                ds_dado_clinico_w,
                ds_material_especial_w,
                ie_amostra_w,
                nr_seq_derivado_w,
                ie_util_hemocomponente_w,
                qt_vol_hemocomp_w,
                qt_procedimento_w,
                dt_suspensao_progr_w,
                ie_tipo_proced_w,
                cd_intervalo_w,
                nr_seq_proc_cpoe_w,
                ie_tipo_proced_ww,
                cd_funcao_origem_w,
                nr_atendimento_w,
                cd_pessoa_fisica_w,
                nr_seq_prot_glic_w;

            EXIT WHEN c13%notfound;
            BEGIN
                qt_dia_adic_w := 0;
                IF ( ds_horarios_padrao_proc_w IS NOT NULL ) THEN
                    BEGIN
                        nr_seq_etapa_w := 0;
                        WHILE ds_horarios_padrao_proc_w IS NOT NULL LOOP
                            BEGIN
                                SELECT
                                    instr(ds_horarios_padrao_proc_w, ' ')
                                INTO k
                                FROM
                                    dual;
                --INICIO MD 

                                BEGIN
                                    sql_w := 'begin calc_hor_padr_sem_dt_lib_md (:1, :2, :3); end;';
                                    EXECUTE IMMEDIATE sql_w
                                        USING IN k, OUT ds_hora_proc_w, IN OUT ds_horarios_padrao_proc_w;
                                EXCEPTION
                                    WHEN OTHERS THEN
                                        ds_hora_proc_w := NULL;
                                        ds_horarios_padrao_proc_w := NULL;
                                END;
                
                --FIM MD
                /* Inicio MD1 */

                                BEGIN
                                    sql_w := 'begin calc_dia_hor_sem_dt_lib_md (:1, :2, :3); end;';
                                    EXECUTE IMMEDIATE sql_w
                                        USING IN ie_proc_urgente_w, IN OUT ds_hora_proc_w, IN OUT qt_dia_adic_w;
                                EXCEPTION
                                    WHEN OTHERS THEN
                                        ds_hora_proc_w := NULL;
                                        qt_dia_adic_w := NULL;
                                END;
                
                /* Fim MD1 */
		        /* Inicio MD2 */

                                BEGIN
                                    sql_w := 'begin calc_dt_prescr_sem_dt_lib_md (:1, :2, :3, :4); end;';
                                    EXECUTE IMMEDIATE sql_w
                                        USING IN ie_proc_urgente_w, IN dt_liberacao_w, IN dt_prescricao_w, IN OUT dt_prescricao_ww;
                                EXCEPTION
                                    WHEN OTHERS THEN
                                        dt_prescricao_ww := NULL;
                                END;
                
			    /* Fim MD2 */
                                SELECT
                                    nvl(MAX('S'), 'N')
                                INTO ie_estendido_w
                                FROM
                                    prescr_procedimento  a,
                                    prescr_medica        b
                                WHERE
                                        a.nr_prescricao = b.nr_prescricao
                                    AND a.nr_prescricao = nr_prescricao_p
                                    AND a.nr_sequencia = nr_seq_procedimento_w
                                    AND nvl(b.cd_funcao_origem, 924) = 950
                                    AND a.nr_seq_anterior IS NOT NULL
                                    AND a.nr_prescricao_original IS NOT NULL;
				
				/* Inicio MD3 */

                                BEGIN
                                    sql_w := 'CALL calc_dt_hor_sem_dt_lib_md(:1, :2, :3, :4, :6, :6) INTO :dt_horario_proc_w';
                                    EXECUTE IMMEDIATE sql_w
                                        USING IN dt_prev_execucao_w, IN dt_prescricao_ww, IN ie_estendido_w, IN ds_hora_proc_w, IN
                                        qt_dia_adic_w, IN ie_proc_urgente_w, OUT dt_horario_proc_w;

                                EXCEPTION
                                    WHEN OTHERS THEN
                                        dt_horario_proc_w := NULL;
                                END;
                
                /* Fim MD3 */

                                IF ( dt_suspensao_progr_w > dt_horario_proc_w ) OR ( dt_suspensao_progr_w IS NULL ) THEN
                                    SELECT
                                        prescr_proc_hor_seq.NEXTVAL
                                    INTO nr_sequencia_w
                                    FROM
                                        dual;
                    /* Inicio MD4 */

                                    BEGIN
                                        sql_w := 'CALL calc_hor_esp_sem_dt_lib_md(:1, :2, :3, :4) INTO :ie_horario_especial_w';
                                        EXECUTE IMMEDIATE sql_w
                                            USING IN ie_se_necessario_w, IN ie_acm_w, IN dt_prev_execucao_ww, IN 1, OUT ie_horario_especial_w;

                                   EXCEPTION
                                        WHEN OTHERS THEN
                                            ie_horario_especial_w := NULL;
                                    END;
                    
                    /* Fim MD4 */
					
                    --INICIO MD

                                    BEGIN
                                        sql_w := 'begin calc_nr_seq_etp_sem_dt_lib_md (:1, :2, :3); end;';
                                        EXECUTE IMMEDIATE sql_w
                                            USING IN nr_seq_derivado_w, IN ie_tipo_proced_ww, IN OUT nr_seq_etapa_w;
                                    EXCEPTION
                                        WHEN OTHERS THEN
                                            nr_seq_etapa_w := NULL;
                                    END;
                    
					--FIM MD
                                    INSERT INTO prescr_proc_hor (
                                        nr_sequencia,
                                        dt_atualizacao,
                                        nm_usuario,
                                        dt_atualizacao_nrec,
                                        nm_usuario_nrec,
                                        nr_prescricao,
                                        nr_seq_procedimento,
                                        cd_procedimento,
                                        ie_origem_proced,
                                        nr_seq_proc_interno,
                                        ds_horario,
                                        dt_horario,
                                        nr_ocorrencia,
                                        ie_urgente,
                                        ie_horario_especial,
                                        cd_material_exame,
                                        ie_aprazado,
                                        nr_etapa
                                    ) VALUES (
                                        nr_sequencia_w,
                                        sysdate,
                                        nm_usuario_p,
                                        sysdate,
                                        nm_usuario_p,
                                        nr_prescricao_p,
                                        nr_seq_procedimento_w,
                                        cd_procedimento_w,
                                        ie_origem_proced_w,
                                        nr_seq_proc_interno_w,
                                        ds_hora_proc_w,
                                        dt_horario_proc_w,
                                        nr_ocor_proc_w,
                                        ie_proc_urgente_w,
                                        nvl(ie_horario_especial_w, 'N'),
                                        cd_material_exame_w,
                                        'N',
                                        decode(nr_seq_etapa_w, 0, NULL, nr_seq_etapa_w)
                                    );

                                END IF;

                            END;
                        END LOOP;

                    END;

                ELSE	
			--INICIO MD

                    BEGIN
                        sql_w := 'begin obter_hor_proc_sem_dt_lib_md (:1, :2, :3, :4, :5); end;';
                        EXECUTE IMMEDIATE sql_w
                            USING IN hr_prim_horario_w, IN dt_primeiro_horario_w, IN dt_prev_execucao_w, OUT ds_hora_proc_w, OUT dt_horario_proc_w;

                    EXCEPTION
                        WHEN OTHERS THEN
                            nr_seq_etapa_w := NULL;
                    END;
            
                --FIM MD

                    IF ( dt_suspensao_progr_w > dt_horario_proc_w ) OR ( dt_suspensao_progr_w IS NULL ) THEN
                        SELECT
                            prescr_proc_hor_seq.NEXTVAL
                        INTO nr_sequencia_w
                        FROM
                            dual;

                        SELECT
                            MAX(dt_prev_execucao)
                        INTO dt_prev_execucao_ww
                        FROM
                            prescr_procedimento
                        WHERE
                                nr_prescricao = nr_prescricao_p
                            AND nr_sequencia = nr_seq_procedimento_w;

                        ie_horario_especial_w := 'N';
                        
                 --INICIO MD                       
                        BEGIN
                            sql_w := 'CALL calc_hor_esp_sem_dt_lib_md(:1, :2, :3, :4) INTO :ie_horario_especial_w';
                            EXECUTE IMMEDIATE sql_w
                                USING IN ie_se_necessario_w, IN ie_acm_w, IN dt_prev_execucao_ww, IN 2, OUT ie_horario_especial_w;

                        EXCEPTION
                            WHEN OTHERS THEN
                                ie_horario_especial_w := NULL;
                        END;
                --FIM MD

                        INSERT INTO prescr_proc_hor (
                            nr_sequencia,
                            dt_atualizacao,
                            nm_usuario,
                            dt_atualizacao_nrec,
                            nm_usuario_nrec,
                            nr_prescricao,
                            nr_seq_procedimento,
                            cd_procedimento,
                            ie_origem_proced,
                            nr_seq_proc_interno,
                            ds_horario,
                            dt_horario,
                            nr_ocorrencia,
                            ie_urgente,
                            ie_horario_especial,
                            cd_material_exame,
                            ie_aprazado,
                            nr_etapa
                        ) VALUES (
                            nr_sequencia_w,
                            sysdate,
                            nm_usuario_p,
                            sysdate,
                            nm_usuario_p,
                            nr_prescricao_p,
                            nr_seq_procedimento_w,
                            cd_procedimento_w,
                            ie_origem_proced_w,
                            nr_seq_proc_interno_w,
                            ds_hora_proc_w,
                            dt_horario_proc_w,
                            nr_ocor_proc_w,
                            ie_proc_urgente_w,
                            ie_horario_especial_w,
                            cd_material_exame_w,
                            'N',
                            decode(ie_horario_especial_w, 'S', NULL, 1)
                        );

                        IF (
                            ( nr_horas_validade_w > 24 )
                            AND ( (
                                ( cd_funcao_origem_w = 2314 )
                                AND ( ie_tipo_proced_ww NOT IN ( 'AP', 'APH', 'APC' ) )
                            ) OR ( cd_funcao_origem_w <> 2314 ) )
                        ) THEN
                            SELECT
                                prescr_proc_hor_seq.NEXTVAL
                            INTO nr_seq_horario_w
                            FROM
                                dual;

                            IF ( dt_horario_proc_w BETWEEN ( dt_validade_prescr_w - 1 ) AND dt_validade_prescr_w ) THEN
                                INSERT INTO prescr_proc_hor (
                                    nr_sequencia,
                                    dt_atualizacao,
                                    nm_usuario,
                                    dt_atualizacao_nrec,
                                    nm_usuario_nrec,
                                    nr_prescricao,
                                    nr_seq_procedimento,
                                    cd_procedimento,
                                    ie_origem_proced,
                                    nr_seq_proc_interno,
                                    ds_horario,
                                    dt_horario,
                                    nr_ocorrencia,
                                    ie_urgente,
                                    ie_horario_especial,
                                    cd_material_exame,
                                    ie_aprazado
                                ) VALUES (
                                    nr_seq_horario_w,
                                    sysdate,
                                    nm_usuario_p,
                                    sysdate,
                                    nm_usuario_p,
                                    nr_prescricao_p,
                                    nr_seq_procedimento_w,
                                    cd_procedimento_w,
                                    ie_origem_proced_w,
                                    nr_seq_proc_interno_w,
                                    substr(to_char(dt_inicio_prescr_w, 'dd/mm/yyyy hh24:mi:ss'), 12, 5),
                                    dt_inicio_prescr_w,
                                    nr_ocor_proc_w,
                                    ie_proc_urgente_w,
                                    'S',
                                    cd_material_exame_w,
                                    'N'
                                );

                            ELSE
                                INSERT INTO prescr_proc_hor (
                                    nr_sequencia,
                                    dt_atualizacao,
                                    nm_usuario,
                                    dt_atualizacao_nrec,
                                    nm_usuario_nrec,
                                    nr_prescricao,
                                    nr_seq_procedimento,
                                    cd_procedimento,
                                    ie_origem_proced,
                                    nr_seq_proc_interno,
                                    ds_horario,
                                    dt_horario,
                                    nr_ocorrencia,
                                    ie_urgente,
                                    ie_horario_especial,
                                    cd_material_exame,
                                    ie_aprazado
                                ) VALUES (
                                    nr_seq_horario_w,
                                    sysdate,
                                    nm_usuario_p,
                                    sysdate,
                                    nm_usuario_p,
                                    nr_prescricao_p,
                                    nr_seq_procedimento_w,
                                    cd_procedimento_w,
                                    ie_origem_proced_w,
                                    nr_seq_proc_interno_w,
                                    substr(to_char(dt_validade_prescr_w, 'dd/mm/yyyy hh24:mi:ss'), 12, 5),
                                    dt_validade_prescr_w,
                                    nr_ocor_proc_w,
                                    ie_proc_urgente_w,
                                    'S',
                                    cd_material_exame_w,
                                    'N'
                                );

                            END IF;

                        END IF;

                    END IF;

                END IF;

            END;

            ie_proc_atual_w := 'S';
            OPEN c15;
            LOOP
                FETCH c15 INTO
                    dt_horario_proc_prev_w,
                    nr_seq_horario_w,
                    dt_resultado_w;
                EXIT WHEN c15%notfound;
                IF ( ie_proc_atual_w = 'N' ) THEN
                    SELECT
                        nvl(MAX(nr_sequencia), 0) + 1
                    INTO nr_seq_procedimento_novo_w
                    FROM
                        prescr_procedimento
                    WHERE
                        nr_prescricao = nr_prescricao_p;

                    atualiza_dt_realizacao_exame(nr_seq_exame_w, nr_prescricao_p, dt_horario_proc_prev_w,
                                                dt_horario_proc_prev_w);
                    obter_setor_exame_lab(nr_prescricao_p, nr_seq_exame_w, cd_setor_atual_usuario_w, cd_material_exame_w, NULL,
                                         'S',
                                         cd_setor_atend_w,
                                         cd_setor_col_w,
                                         cd_setor_entrega_lab_w,
                                         qt_dia_entrega_w,
                                         ie_emite_mapa_w,
                                         ds_hora_fixa_w,
                                         ie_data_resultado_w,
                                         qt_min_entrega_w,
                                         ie_atualizar_recoleta_w,
                                         nvl(ie_agora_w, 'N'),
                                         ie_dia_semana_final_w,
                                         ie_forma_atual_dt_result_w,
                                         qt_min_atraso_w,
                                         dt_horario_proc_prev_w);

                    SELECT
                        decode(ie_considera_data_prevista_w, 'S', nvl(dt_horario_proc_prev_w, nvl(dt_entrada_unidade, dt_prescricao)),
                               nvl(dt_entrada_unidade, dt_prescricao))
                    INTO dt_resultado_w
                    FROM
                        prescr_medica
                    WHERE
                        nr_prescricao = nr_prescricao_p;					
                
                /* Inicio MD7 */

                  BEGIN
                        sql_w := 'begin calc_dt_resul_sem_dt_lib_md (:1, :2, :3, :4); end;';
                        EXECUTE IMMEDIATE sql_w
                            USING IN ds_hora_fixa_w, IN qt_dia_entrega_w, IN qt_min_entrega_w, IN OUT dt_resultado_w;
                           
                   EXCEPTION
                        WHEN OTHERS THEN
                            dt_resultado_w := NULL;
                    END;
                /* Fim MD7 */

                    INSERT INTO prescr_procedimento (
                        nr_prescricao,
                        nr_sequencia,
                        nr_seq_origem,
                        dt_prev_execucao,
                        qt_procedimento,
                        nm_usuario,
                        dt_atualizacao,
                        cd_procedimento,
                        ie_origem_proced,
                        nr_seq_proc_interno,
                        ie_urgencia,
                        nr_seq_exame,
                        ie_status_atend,
                        ie_status_execucao,
                        cd_setor_atendimento,
                        cd_setor_coleta,
                        cd_setor_entrega,
                        cd_setor_exec_fim,
                        cd_setor_exec_inic,
					--nr_seq_lab,
                        ie_origem_inf,
                        cd_motivo_baixa,
                        ie_suspenso,
                        ds_observacao,
                        ds_dado_clinico,
                        ds_material_especial,
                        ie_amostra,
                        cd_material_exame,
                        nr_seq_derivado,
                        ie_util_hemocomponente,
                        qt_vol_hemocomp,
                        cd_intervalo,
                        dt_resultado,
                        nr_seq_proc_cpoe,
                        nr_seq_prot_glic
                    ) VALUES (
                        nr_prescricao_p,
                        nr_seq_procedimento_novo_w,
                        nr_seq_procedimento_w,
                        dt_horario_proc_prev_w,
                        qt_procedimento_w,
                        nm_usuario_p,
                        sysdate,
                        cd_procedimento_w,
                        ie_origem_proced_w,
                        nr_seq_proc_interno_w,
                        ie_proc_urgente_w,
                        nr_seq_exame_w,
                        ie_status_atend_w,
                        ie_status_execucao_w,
                        cd_setor_atendimento_proc_w,
                        cd_setor_coleta_w,
                        cd_setor_entrega_w,
                        cd_setor_exec_fim_w,
                        cd_setor_exec_inic_w,
					--nr_seq_lab_w,
                        1,
                        0,
                        ie_suspenso_w,
                        ds_observacao_w,
                        ds_dado_clinico_w,
                        ds_material_especial_w,
                        ie_amostra_w,
                        cd_material_exame_w,
                        nr_seq_derivado_w,
                        ie_util_hemocomponente_w,
                        qt_vol_hemocomp_w,
                        cd_intervalo_w,
                        dt_resultado_w,
                        nr_seq_proc_cpoe_w,
                        nr_seq_prot_glic_w
                    );

                    UPDATE prescr_proc_hor
                    SET
                        nr_seq_proc_origem = nr_seq_procedimento_novo_w
                    WHERE
                        nr_sequencia = nr_seq_horario_w;

                    BEGIN
                        IF (
                            ( cd_funcao_origem_w = 2314 )
                            AND ( nr_seq_proc_cpoe_w IS NOT NULL )
                            AND ( nr_seq_prot_glic_w IS NOT NULL )
                        ) THEN
                            cpoe_gerar_proc_glic_reg(nr_prescricao_p, nr_atendimento_w, nr_seq_proc_cpoe_w, nr_seq_prot_glic_w,
                                                    nr_seq_procedimento_novo_w,
                                                    nm_usuario_p,
                                                    cd_pessoa_fisica_w);
                        END IF;
                    EXCEPTION
                        WHEN OTHERS THEN
                            gravar_log_cpoe(substr('GERAR_PRESCR_PROC_SEM_DT_LIB EXCEPTION CPOE_GERAR_PROC_GLIC_REG: '
                                                   || substr(to_char(sqlerrm), 1, 2000)
                                                   || ' -nr_seq_proc_cpoe_w:'
                                                   || nr_seq_proc_cpoe_w
                                                   || ' -nr_prescricao_p :'
                                                   || nr_prescricao_p
                                                   || ' -nr_sequencia_w:'
                                                   || nr_seq_procedimento_novo_w,
                                                  1,
                                                  2000),
                                           nr_atendimento_w,
                                           'P',
                                           nr_seq_proc_cpoe_w);
                    END;

                END IF;

                ie_proc_atual_w := 'N';
            END LOOP;

            CLOSE c15;
        END LOOP;

        CLOSE c13;
    END IF;

    IF ( nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S' ) THEN
        COMMIT;
    END IF;

    IF
        ( ie_lib_individual_w = 'S' )
        AND ( ie_liberacao_p = 'S' )
    THEN
        UPDATE prescr_proc_hor
        SET
            dt_lib_horario = sysdate
        WHERE
                nr_prescricao = nr_prescricao_p
            AND dt_lib_horario IS NULL
            AND ( ( nr_seq_procedimento = nr_seq_item_p )
                  OR ( nvl(nr_seq_item_p, 0) = 0 ) );

        atualizar_seq_hor_glicemia(nr_prescricao_p);
    END IF;

END gerar_prescr_proc_sem_dt_lib;
/
