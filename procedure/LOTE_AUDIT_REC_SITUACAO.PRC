create or replace
procedure lote_audit_rec_situacao(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 
begin
update	lote_audit_recurso
set	ie_situacao_lote = 'E'
where	nr_sequencia = nr_sequencia_p;
commit;
end lote_audit_rec_situacao;
/