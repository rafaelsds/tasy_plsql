create or replace
procedure man_definir_motivo_nao_cobra(	nr_seq_orcamento_p	number,
					nr_seq_motivo_p		number,
					ds_observacao_p		varchar2,
					nm_usuario_p		Varchar2) is 

ds_titulo_w			varchar2(255) := Wheb_mensagem_pck.get_texto(488904);
ds_comunic_w			varchar2(4000);
ds_email_w			varchar2(255);
ds_gerencia_w			varchar2(255);
nm_gerente_w			varchar2(40);
ds_motivo_w			varchar2(255);
nr_seq_ordem_w			number(10,0);

begin
if	(nr_seq_orcamento_p > 0) and
	(nr_seq_motivo_p > 0) then
	begin
	select	a.nr_seq_ordem,
		substr(obter_gerencia_grupo_desen(b.nr_seq_grupo_des,'D'),1,255) ds_gerencia,
		substr(obter_pf_usuario(nm_usuario_p,'N'),1,40),
		substr(obter_descricao_padrao_pk('MAN_MOTIVO_NCOBR_ORC','DS_MOTIVO','NR_SEQUENCIA', nr_seq_motivo_p),1,255)
	into	nr_seq_ordem_w,
		ds_gerencia_w,
		nm_gerente_w,
		ds_motivo_w
	from	man_ordem_servico_orc a,
		man_ordem_servico b
	where	a.nr_sequencia	= nr_seq_orcamento_p
	and	b.nr_sequencia	= a.nr_seq_ordem;
	
	update	man_ordem_servico_orc
	set	nr_seq_motivo_nao_cobra = nr_seq_motivo_p,
		nm_usuario_nao_cobra	= nm_usuario_p,
		ds_observacao		= substr(ds_observacao || chr(13) || chr(10) || ds_observacao_p,1,4000)
	where	nr_sequencia		= nr_seq_orcamento_p;	
	
	select 	max(a.ds_email)
	into 	ds_email_w
	from 	pessoa_fisica b,
		usuario a 
	where 	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and 	b.cd_cargo = 791;

	if	(nvl(ds_email_w,'x') <> 'x') then
		begin
		ds_comunic_w := Wheb_mensagem_pck.get_texto( 488902, 'NR_SEQ_ORDEM=' || nr_seq_ordem_w ||
								';DS_GERENCIA=' || ds_gerencia_w ||
								';NM_GERENTE=' || nm_gerente_w ||
								';DS_MOTIVO=' || ds_motivo_w);
	
		Enviar_Email(ds_titulo_w, ds_comunic_w, null, ds_email_w, nm_usuario_p, 'M');
		end;
	end if;
	
	commit;
	end;	
end if;

end man_definir_motivo_nao_cobra;
/