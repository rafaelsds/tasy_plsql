create or replace 
procedure atualiza_atend_aval_srpa_dtlb (
			nr_sequencia_p	number) is 

begin

if	(nr_sequencia_p is not null) then
	begin
	
	update	atend_aval_srpa
	set	dt_liberacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	
	end;
end if;

commit;

end atualiza_atend_aval_srpa_dtlb;
/