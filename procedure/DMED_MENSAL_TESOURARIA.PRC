create or replace
procedure dmed_mensal_tesouraria
			(	nr_seq_dmed_mensal_p		number,
				cd_estabelecimento_p		number,
				dt_referencia_p			date,
				ie_nota_fiscal_p			varchar2,
				ie_conta_paciente_p		varchar2,
				ie_cpf_p				varchar2,
				ie_idade_p			varchar2, 
				ie_estrangeiro_p			varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionário [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
-------------------------------------------------------------------------------------------------------------------
Referências:
	GERAR_DMED_MENSAL_PRESTADOR
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_responsavel_w		dmed_titulos_mensal.cd_pessoa_titular%type;
cd_pessoa_beneficiario_w		dmed_titulos_mensal.cd_pessoa_beneficiario%type;
nr_documento_w			dmed_titulos_mensal.nr_documento%type;
vl_documento_w			dmed_titulos_mensal.vl_pago%type;
nr_atendimento_w			dmed_titulos_mensal.nr_atendimento%type;
contador_w			number(10);
qt_classe_w			number(10);
dt_documento_w			date;
dt_referencia_w			date;

cursor c_tesouraria is	/* cursor para identificar os responsáveis */
	select 	max(r.nr_titulo) nr_documento,
		max(y.cd_pessoa_fisica) cd_pessoa_responsavel,
		max(a.cd_pessoa_fisica) cd_pessoa_beneficiario,
		sum(nvl(l.vl_recebido + l.vl_rec_maior,r.vl_titulo)) vl_documento,
		max(y.dt_recebimento) dt_documento,
		max(a.nr_atendimento)
	from 	caixa_receb		y,
		caixa_saldo_diario	o,
		caixa			t,
		titulo_receber_liq	l,
		titulo_receber		r,
		atendimento_paciente	a,
		pessoa_fisica		p,
		nacionalidade		n,
		pessoa_fisica		p2
	where	o.nr_sequencia		= y.nr_seq_saldo_caixa
	and	o.nr_seq_caixa		= t.nr_sequencia
	and	l.nr_seq_caixa_rec	= y.nr_sequencia
	and	l.nr_titulo		= r.nr_titulo
	and	r.nr_atendimento	= a.nr_atendimento
	and	p.cd_nacionalidade	= n.cd_nacionalidade(+)
	and	r.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_pessoa_fisica	= p.cd_pessoa_fisica	
	and	y.cd_pessoa_fisica	= p2.cd_pessoa_fisica	
	and	p2.nr_cpf is not null
	and	nvl(r.ie_pls, 'N')	= 'N'
	and	to_char(y.dt_recebimento,'mm/yyyy')	= to_char(dt_referencia_p, 'mm/yyyy')
	and	y.dt_cancelamento is null
	and 	l.vl_recebido > 0
	and	(r.ie_situacao not in ('3','5') or (r.ie_situacao = '5' and 
		exists	(select	1			
			from	titulo_receber_liq k
			where	k.nr_titulo = r.nr_titulo
			and 	vl_recebido > 0	
			and 	not exists (select 1 from titulo_receber_liq x where x.nr_titulo = k.nr_titulo and x.nr_seq_liq_origem = k.nr_sequencia and rownum = 1))))
	and	exists	(select	1
			from	dmed_regra_tesouraria	w
			where	w.cd_tipo_recebimento	= l.cd_tipo_recebimento)
	and	((ie_cpf_p = 'AM') or
		((ie_cpf_p = 'SC') and 	 (p.nr_cpf is null) 	and 	(pkg_date_utils.add_month(p.dt_nascimento, ie_idade_p * 12,0) <= Fim_Mes(dt_referencia_w))) or
		((ie_cpf_p = 'CC') and (((p.nr_cpf is not null) and 	(pkg_date_utils.add_month(p.dt_nascimento, ie_idade_p * 12,0) <= Fim_Mes(dt_referencia_w))) or 
									(pkg_date_utils.add_month(p.dt_nascimento, ie_idade_p * 12,0) >= Fim_Mes(dt_referencia_w)))))
	and	(((nvl(n.ie_brasileiro,'S') = 'S') and (ie_estrangeiro_p = '2'))
	or	((nvl(ie_brasileiro,'S') = 'N') and (ie_estrangeiro_p = '1'))
	or	(ie_estrangeiro_p = '3'))
	and 	y.cd_pessoa_fisica is not null
	and 	((r.nr_seq_classe is null) or
		(qt_classe_w = 0) or
		(exists	(select 1 
			from 	 dmed_regra_classe_tit t
			where	 t.nr_seq_classe = r.nr_seq_classe)))
	and 	((exists (select	1
			from	atend_paciente_unidade c,
				dmed_regra_setor_atend b
			where	c.cd_setor_atendimento	= b.cd_setor_atendimento
			and 	c.nr_atendimento	= a.nr_atendimento)) or
		(not exists(	select 1 
				from 	 dmed_regra_setor_atend
				where 	rownum  <= 1)))
	group by y.nr_recibo, y.cd_pessoa_fisica, a.cd_pessoa_fisica;

begin
dt_referencia_w	:= pkg_date_utils.start_of(dt_referencia_p, 'MONTH',0);

select	count(1)
into	qt_classe_w
from	dmed_regra_classe_tit
where	rownum	= 1;

open c_tesouraria;
loop
fetch c_tesouraria into
	nr_documento_w,
	cd_pessoa_responsavel_w,
	cd_pessoa_beneficiario_w,
	vl_documento_w,
	dt_documento_w,
	nr_atendimento_w;
exit when c_tesouraria%notfound;
	begin
	contador_w	:= contador_w + 1;
	
	if	(cd_pessoa_beneficiario_w is null) then
		cd_pessoa_beneficiario_w := cd_pessoa_responsavel_w;
	end if;	
	
	insert into dmed_titulos_mensal
		(nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		nr_seq_dmed_mensal, 
		nr_documento, 
		ie_tipo_documento, 
		cd_pessoa_titular, 
		cd_pessoa_beneficiario, 
		vl_pago, 
		dt_liquidacao,
		ie_prestadora_ops,
		nr_atendimento)
	values	(dmed_titulos_mensal_seq.NextVal,
		sysdate,
		'Tasy7',
		sysdate,
		'Tasy7',
		nr_seq_dmed_mensal_p,
		nr_documento_w,
		'TE',
		cd_pessoa_responsavel_w,
		cd_pessoa_beneficiario_w,
		vl_documento_w,
		dt_documento_w,
		'P',
		nr_atendimento_w);
	
	if	(mod(contador_w,100) = 0) then
		commit;
	end if;
	
	end;
end loop;
close c_tesouraria;

commit;

end dmed_mensal_tesouraria;
/
