create or replace
procedure cm_estorna_retirada_receb(
			nr_conjunto_cont_p		number,
			nm_usuario_p		varchar2) is 

qt_retirada_w		number(5);
qt_receb_w		number(5);
nr_sequencia_w		number(10);
nr_seq_conjunto_w		number(10);
cd_local_estoque_w	number(4);
cd_estabelecimento_w	number(4);

begin

select	count(*)
into	qt_retirada_w
from	cm_expurgo_retirada
where	nr_conjunto_cont = nr_conjunto_cont_p;

select	count(*)
into	qt_receb_w
from	cm_expurgo_receb
where	nr_conjunto_cont = nr_conjunto_cont_p;

if	(qt_retirada_w > 0) then

	select	nr_sequencia,
		nr_seq_conjunto,
		cd_local_estoque,
		cd_estabelecimento
	into	nr_sequencia_w,
		nr_seq_conjunto_w,
		cd_local_estoque_w,
		cd_estabelecimento_w
	from	cm_expurgo_retirada
	where	nr_conjunto_cont = nr_conjunto_cont_p;

	cm_gera_expurgo_saldo(nr_seq_conjunto_w, cd_local_estoque_w, 'D', cd_estabelecimento_w, nm_usuario_p);

	update	cm_expurgo_retirada
	set	dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p,
		cd_local_estoque = null,
		dt_retirada = null,
		nm_usuario_retirada = null,
		nr_seq_interno = null,
		cd_setor_retirada = null
	where	nr_sequencia = nr_sequencia_w;
	
elsif	(qt_receb_w > 0) then

	select	nr_sequencia,
		nr_seq_conjunto,
		cd_local_estoque,
		cd_estabelecimento
	into	nr_sequencia_w,
		nr_seq_conjunto_w,
		cd_local_estoque_w,
		cd_estabelecimento_w
	from	cm_expurgo_receb
	where	nr_conjunto_cont = nr_conjunto_cont_p;

	cm_gera_expurgo_saldo(nr_seq_conjunto_w, cd_local_estoque_w, 'D', cd_estabelecimento_w, nm_usuario_p);
	
	update	cm_expurgo_receb
	set	dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p,
		nm_usuario_receb = null,
		cd_setor_receb = null,
		nm_usuario_entrega = null,
		cd_local_estoque = null,
		dt_recebimneto = null,
		ds_observacao = null
	where	nr_sequencia = nr_sequencia_w;

end if;

commit;

end cm_estorna_retirada_receb;
/