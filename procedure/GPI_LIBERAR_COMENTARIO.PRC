create or replace
procedure gpi_liberar_comentario(
			nr_seq_comentario_p		number) is 

begin

update	gpi_cron_etapa_coment
set		dt_liberacao	=	sysdate
where	nr_sequencia    =   nr_seq_comentario_p;

commit;

end gpi_liberar_comentario;
/