create or replace
procedure Consistir_transf_agenda (	nr_seq_origem_p		number,
				nr_seq_destino_p	number,
				ie_acao_p		varchar2,
				cd_perfil_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				ds_consistencia_p	out varchar2,
				ds_erro_p		out varchar2,
				ie_medico_p		out varchar2) is

ie_consiste_medico_w		varchar2(1);
cd_medico_exec_origem_w		varchar2(10);
ds_medico_exec_origem_w		varchar2(90);
cd_medico_exec_destino_w	varchar2(10);
ds_medico_exec_destino_w	varchar2(90);
ds_consistencia_w		varchar2(255);
ds_erro_w			varchar2(255);
cd_agenda_w			number(10);
cd_convenio_w			number(10);
cd_medico_exec_w		varchar2(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_proc_interno_w		number(10);
IE_CONSISTENCIA_w		varchar2(255);
ie_agenda_w			varchar2(1);
ie_transf_exec_w		varchar2(1);
ie_status_agenda_w		varchar2(3);
hr_inicio_w			date;
ie_transf_data_ant_w		varchar2(1);
ie_status_agenda_destino_w	varchar2(3);
ie_manut_proced_w		varchar2(1);
hr_inicio_dest_w		date;
cd_retorno_w			number(10,0);
ie_valida_quant_proc_w		varchar2(1);
ie_consist_js_w			varchar2(255) := '';
cd_pessoa_fisica_w		varchar2(10);
qt_idade_paciente_w		number(3,0);
ie_medico_exec_copia_w		varchar2(1);
ie_medico_exec_transf_w		varchar2(1);
cd_categoria_w			varchar2(10);
ie_consiste_sobrep_hor_w	varchar2(1);
ie_manter_min_dur_transf_w	varchar2(1);
ie_se_horario_sobreposto_w	varchar2(1);
nr_min_duracao_w		number(10);
ie_permite_w			varchar2(1);
cd_empresa_ref_w		number(10,0);
ie_anestesia_w			varchar2(1);
cd_proc_adic_w		agenda_paciente_proc.cd_procedimento%type;
ie_origem_proc_adic_w agenda_paciente_proc.ie_origem_proced%type;
nr_seq_proc_int_adic_w agenda_paciente_proc.nr_seq_proc_interno%type;
cd_conv_adic_w		number(10);
cd_categoria_adic_w	varchar2(10);

cursor c01 is
	select app.cd_procedimento,
		   app.ie_origem_proced,
		   app.nr_seq_proc_interno,
		   nvl(app.cd_convenio,ap.cd_convenio),
		   nvl(app.cd_categoria,ap.cd_categoria)
	  from agenda_paciente_proc app,
		   agenda_paciente ap
	 where ap.nr_sequencia = app.nr_sequencia
	   and ap.nr_sequencia = nr_seq_origem_p;
	

begin


Obter_Param_Usuario(820,40,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_consiste_medico_w);
Obter_Param_Usuario(820,72,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_transf_exec_w);
Obter_Param_Usuario(820,73,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_transf_data_ant_w);
Obter_Param_Usuario(820,12,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_manut_proced_w);
Obter_Param_Usuario(820,103,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_valida_quant_proc_w);
ie_medico_exec_transf_w		:= obter_parametro_agenda(cd_estabelecimento_p, 'IE_MEDICO_EXEC_TRANSF', 	'S');
ie_medico_exec_copia_w		:= obter_parametro_agenda(cd_estabelecimento_p, 'IE_MEDICO_EXEC_COPIA', 	'S');
ie_consiste_sobrep_hor_w	:= obter_parametro_agenda(cd_estabelecimento_p, 'IE_CONSISTE_DURACAO', 		'N');
ie_manter_min_dur_transf_w	:= obter_parametro_agenda(cd_estabelecimento_p, 'IE_DURACAO_TRANSF', 		'N');

ds_erro_w		:= null;
ds_consistencia_w	:= null;
ie_medico_p		:= 'N';

select	max(ie_status_agenda)
into	ie_status_agenda_destino_w
from	agenda_paciente
where 	nr_sequencia = nr_seq_destino_p;

if	(ie_status_agenda_destino_w <> 'L') then
	ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279820,null);
end if;


select 	max(cd_medico_exec),
	max(substr(obter_nome_pf(cd_medico_exec),1,90)),
	max(ie_status_agenda),
	max(hr_inicio),
	max(nr_minuto_duracao)
into	cd_medico_exec_origem_w,
	ds_medico_exec_origem_w,
	ie_status_agenda_w,
	hr_inicio_w,
	nr_min_duracao_w
from	agenda_paciente
where 	nr_sequencia = nr_seq_origem_p;

select	max(cd_agenda),
	max(cd_medico_exec),
	max(hr_inicio)
into	cd_agenda_w,
	cd_medico_exec_w,
	hr_inicio_dest_w
from	agenda_paciente
where	nr_sequencia = nr_seq_destino_p;
	
if	(ie_acao_p = 'C') and
	(ie_medico_exec_copia_w = 'N') then
	cd_medico_exec_origem_w 	:= cd_medico_exec_w;
elsif	(ie_acao_p = 'T') and
	(ie_medico_exec_transf_w = 'N') then
	cd_medico_exec_origem_w 	:= cd_medico_exec_w;
end if;

select	max(cd_convenio),
	max(cd_pessoa_fisica),
	max(qt_idade_paciente),
	max(cd_categoria),
	max(cd_empresa_ref),
	max(nvl(ie_anestesia,'N'))
into	cd_convenio_w,
	cd_pessoa_fisica_w,
	qt_idade_paciente_w,
	cd_categoria_w,
	cd_empresa_ref_w,
	ie_anestesia_w
from	agenda_paciente
where	nr_sequencia = nr_seq_origem_p;

ie_permite_w	:= Consiste_Agenda_Sexo(cd_pessoa_fisica_w, cd_agenda_w);

if	(ie_permite_w = 'N') then
	Wheb_mensagem_pck.exibir_mensagem_abort(268471);
end if;

select	max(cd_procedimento),
	max(ie_origem_proced),
	max(nr_seq_proc_interno)
into	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_proc_interno_w
from	agenda_paciente
where	nr_sequencia = nr_seq_origem_p;

if	(ds_erro_w is null) and
	(ds_consistencia_w is null) then
	
	if	(ie_manut_proced_w = 'N') or
		((ie_manut_proced_w = 'C') and
		(ie_acao_p = 'T')) or
		((ie_manut_proced_w = 'T') and
		(ie_acao_p = 'C')) then
		
		cd_procedimento_w 	:= null;
		ie_origem_proced_w 	:= null;
		nr_seq_proc_interno_w 	:= null;
		
	end if;
	
	if	(ie_acao_p = 'T') and
		(ie_consiste_sobrep_hor_w <> 'N') and
		(ie_manter_min_dur_transf_w <> 'N')then
		begin
		
		select	max(obter_se_sobreposicao_horario(cd_agenda_w, hr_inicio_dest_w, nr_min_duracao_w))
		into	ie_se_horario_sobreposto_w
		from	dual;
		
		if	(ie_se_horario_sobreposto_w = 'S')then
			if	(ie_consiste_sobrep_hor_w = 'A')then
				ds_consistencia_w	:= WHEB_MENSAGEM_PCK.get_texto(279830,null);
			elsif	(ie_consiste_sobrep_hor_w = 'I')then
				ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279823,null);
			end if;		
		end if;
		
		end;
		
	end if;
	
	if	(cd_procedimento_w is not null) or
		(nr_seq_proc_interno_w is not null) then
	
		Consistir_Proc_Conv_Agenda(	cd_estabelecimento_p,
					cd_pessoa_fisica_w,
					hr_inicio_dest_w,
					cd_agenda_w,
					cd_convenio_w,
					cd_categoria_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					nr_seq_proc_interno_w,
					cd_medico_exec_origem_w,
					'E',
					null, 
					IE_CONSISTENCIA_w,
					ie_agenda_w,
					null,
					cd_retorno_w,
					ie_consist_js_w,
					null,
					null,
					cd_empresa_ref_w,
					ie_anestesia_w,
					null,
					null);		
		
		if	(ie_consistencia_w is not null) and
			(ds_erro_w is null) and
			(ds_consistencia_w is null)then

			if	(ie_valida_quant_proc_w	= 'N') and
				(ie_agenda_w	= 'Q') then
				ie_agenda_w	:= 'N';
			end if;
		
			if	(ie_agenda_w = 'N') or (ie_agenda_w = 'H')  then
				ds_erro_w		:= ie_consistencia_w;
			end if;

			if	(ie_agenda_w = 'Q') then
				ds_consistencia_w	:= ie_consistencia_w;
			end if;

		end if;
		
	end if;
	--Validacao para exames adicionais
	OPEN C01;
		LOOP
		FETCH C01 INTO
			cd_proc_adic_w,
			ie_origem_proc_adic_w,
			nr_seq_proc_int_adic_w,
			cd_conv_adic_w,
			cd_categoria_adic_w;
		EXIT WHEN C01%NOTFOUND;
		begin
			
			if (((cd_proc_adic_w is not null and ie_origem_proc_adic_w is not null) and
				nr_seq_proc_int_adic_w is not null) and
				ds_erro_w is null and
				ds_consistencia_w is null and
				ie_consistencia_w is null)	then
				
				Consistir_Proc_Conv_Agenda(cd_estabelecimento_p,
					cd_pessoa_fisica_w,
					hr_inicio_dest_w,
					cd_agenda_w,
					cd_conv_adic_w,
					cd_categoria_adic_w,
					cd_proc_adic_w,
					ie_origem_proc_adic_w,
					nr_seq_proc_int_adic_w,
					cd_medico_exec_origem_w,
					'E',
					null, 
					IE_CONSISTENCIA_w,
					ie_agenda_w,
					null,
					cd_retorno_w,
					ie_consist_js_w,
					null,
					null,
					cd_empresa_ref_w,
					ie_anestesia_w,
					null,
					null);		
				
				if	(ie_consistencia_w is not null) and
					(ds_erro_w is null) and
					(ds_consistencia_w is null)then

					if	(ie_valida_quant_proc_w	= 'N') and
						(ie_agenda_w	= 'Q') then
						ie_agenda_w	:= 'N';
					end if;
				
					if	(ie_agenda_w = 'N') or (ie_agenda_w = 'H')  then
						ds_erro_w		:= ie_consistencia_w;
					end if;

					if	(ie_agenda_w = 'Q') then
						ds_consistencia_w	:= ie_consistencia_w;
					end if;
					exit;
				end if;
			else
				exit;
			end if;
		end;
		end loop;
	close c01;
	if	(nr_seq_proc_interno_w is not null) then
		Consistir_Regra_Agenda_Grupo(cd_agenda_w, hr_inicio_dest_w, nr_seq_proc_interno_w, nr_seq_destino_p, nm_usuario_p, cd_estabelecimento_p,0);
	end if;
end if;

if	(ie_acao_p = 'T') and 
	(ds_erro_w is null) and
	(ds_consistencia_w is null) then
	
	if	(trunc(hr_inicio_w) < trunc(sysdate)) and
		(ie_transf_data_ant_w = 'N') then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279825,null);
	end if;

	if	(ie_status_agenda_w = 'E') and	
		(ie_transf_exec_w = 'N') then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279826,null);
	end if;

	select 	max(cd_medico_exec),
		max(substr(obter_nome_pf(cd_medico_exec),1,90))
	into	cd_medico_exec_destino_w,
		ds_medico_exec_destino_w
	from	agenda_paciente
	where 	nr_sequencia = nr_seq_destino_p;
	
	select 	max(cd_medico_exec),
		max(substr(obter_nome_pf(cd_medico_exec),1,90)),
		max(ie_status_agenda),
		max(hr_inicio)
	into	cd_medico_exec_origem_w,
		ds_medico_exec_origem_w,
		ie_status_agenda_w,
		hr_inicio_w
	from	agenda_paciente
	where 	nr_sequencia = nr_seq_origem_p;

	if (cd_medico_exec_origem_w is not null) and
	   (cd_medico_exec_destino_w is not null) and
	   ((cd_medico_exec_origem_w <> cd_medico_exec_destino_w)) then
		ie_medico_p	:= 'S';
		if	(ie_consiste_medico_w = 'S') then
			ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(279828,'DS_MEDICO_EXEC_DESTINO='|| ds_medico_exec_destino_w||';DS_MEDICO_EXEC_ORIGEM='||  ds_medico_exec_origem_w);
		elsif	(ie_consiste_medico_w = 'Q') then
			ds_consistencia_w	:= WHEB_MENSAGEM_PCK.get_texto(279828,'DS_MEDICO_EXEC_DESTINO='|| ds_medico_exec_destino_w||';DS_MEDICO_EXEC_ORIGEM='|| ds_medico_exec_origem_w);
		end if;
	end if;
end if;

ds_consistencia_p	:= ds_consistencia_w;
ds_erro_p		:= ds_erro_w;

end Consistir_transf_agenda;
/
