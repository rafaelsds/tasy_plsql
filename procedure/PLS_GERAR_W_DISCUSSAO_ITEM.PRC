create or replace
procedure pls_gerar_w_discussao_item
			(	nr_seq_discussao_p		number,
				nr_seq_analise_p		number,
				nm_usuario_p			varchar2,
				nr_seq_item_criado_p	out	Number	) is 
				
nr_seq_prestador_exec_w		Number(10);
nr_seq_grau_partic_w		Number(10);
nr_seq_conta_referencia_w	number(10);				
nr_seq_item_w			Number(10);
ie_tipo_item_w			Varchar2(1);
cd_item_w			Number(15);
ie_origem_proced_w		Number(10);
ie_tipo_despesa_w		Varchar2(10);
ie_via_acesso_w			Varchar2(10);
dt_item_w			Date;
tx_item_w			Number(9,3);
vl_unitario_w			pls_conta_proc.vl_unitario%type;

vl_total_w			Number(15,2);
cd_medico_executor_w		varchar2(10);
ie_tipo_guia_w			varchar2(10);
cd_guia_referencia_w		varchar2(20);
cd_guia_w			varchar2(20);
nr_seq_apres_w			number(10);
ds_grau_participacao_w		varchar2(255);
ds_item_w			varchar2(255);
ds_via_acesso_w			varchar2(255);
nm_prestador_w			varchar2(255);	
nm_participante_w		varchar2(255);
ds_tipo_guia_w			varchar2(255);
ie_status_w			varchar2(1);
qt_glosa_w			number(5);
ie_glosa_w			varchar2(1);
qt_ocorrencia_w			number(5);
ie_ocorrencia_glosa_w		varchar2(1);
vl_calculado_w			Number(15,2);
qt_ocorrencia_glosa_w		number(5);
nr_seq_protocolo_w		number(10);
ie_status_conta_w		varchar2(1);
ds_status_conta_w		varchar2(120);
cd_porte_anestesico_w		varchar2(10);
nr_auxiliares_w			number(10);
nm_prestador_pag_w		varchar2(255);
nr_seq_prestador_pgto_w		number(10);
nr_seq_guia_w			number(10);
ie_autorizado_w			varchar2(1)	:= 'N';
cd_classificacao_sip_w		varchar2(15);
cd_classif_cred_w		varchar2(40);
cd_classif_deb_w		varchar2(40);
ie_carater_internacao_w		varchar2(1);
ie_exige_nf_w			varchar2(1);
nr_nota_fiscal_w		Number(20);
nr_seq_prestador_solic_w	Number(10);
nm_prestador_solic_w		varchar2(255);
vl_coparticipacao_unit_w	Number(15,2);
vl_coparticipacao_w		Number(15,2);
ie_nota_fiscal_w		varchar2(1);
nr_seq_restricao_w		Number(10);
dt_atend_ref_w			date;
nr_seq_prest_fornec_w		Number(10);
ds_fornecedor_w			varchar2(255);
nr_seq_segurado_w		Number(10);
cd_usuario_plano_w		varchar2(30);
nm_segurado_w			varchar2(255);
nm_prestador_exec_w		varchar2(255);
nr_identificador_w		Number(10);
nr_seq_item_analise_w		Number(10);
nr_seq_partic_w			Number(10);
cd_medico_w			varchar2(10);
nr_seq_cbo_saude_w		Number(10);	
nr_seq_conselho_w		Number(10);
nr_seq_conta_proc_w		Number(10);
nr_seq_honorario_crit_w		Number(10);
nr_seq_prestador_w		Number(10);
vl_honorario_medico_w		Number(15,2);
vl_participante_w		Number(15,2);
nr_seq_proc_ref_w		Number(10);
vl_apresentado_w		Number(15,2);
nr_seq_grau_partic_ww		Number(10);
ds_grau_participacao_ww		varchar2(255);
nr_seq_cbo_saude_ww		Number(10);
nm_participante_ww		varchar2(255);
nr_seq_apres_ww			Number(10);

nr_seq_motivo_glosa_aceita_w	number(10)	:= null;
nr_seq_motivo_glosa_neg_w	number(10)	:= null;
qt_apresentado_w		number(12,4);
vl_total_apres_w		number(15,2);
qt_contestada_w			number(12,4);
vl_contestado_w			number(15,2);
qt_recurso_w			number(12,4);
vl_recurso_w			number(15,2);
qt_negada_w			number(12,4);
vl_negado_w			number(15,2);
qt_aceita_w			number(12,4);
vl_aceito_w			number(15,2);
nr_seq_conta_w			number(10);
qt_liberado_w			number(12,4);
vl_unitario_apres_w		number(15,2);
nr_seq_disc_mat_w		number(10)	:= null;
nr_seq_disc_proc_w		number(10)	:= null;
nr_seq_disc_item_w		number(10)	:= null;
nr_ordem_w			number(10);
qt_partics_w			number(10);
nr_seq_contestacao_proc_w	pls_discussao_proc.nr_seq_contestacao_proc%type;
nr_seq_contestacao_mat_w	pls_discussao_mat.nr_seq_contestacao_mat%type;

Cursor C01 is
	select	b.nr_sequencia,
		'P',
		b.cd_procedimento,
		b.ie_origem_proced,
		b.ie_tipo_despesa,
		b.ie_via_acesso,
		b.dt_procedimento,
		b.tx_item,
		b.qt_procedimento_imp,
		b.qt_procedimento,
		b.vl_unitario,
		b.vl_liberado,
		b.vl_unitario_imp,
		--nvl(b.vl_taxa_servico_imp,0) + nvl(b.vl_taxa_co_imp,0) + nvl(b.vl_taxa_material_imp,0) + nvl(b.vl_procedimento_imp,0),
		b.vl_procedimento,
		b.ie_status,
		b.cd_porte_anestesico,
		b.nr_auxiliares,
		a.qt_contestada,
		a.vl_contestado,
		a.qt_recurso,
		a.vl_recurso,
		a.qt_aceita,
		a.vl_aceito,
		a.qt_negada,
		a.vl_negado,
		a.nr_sequencia,
		a.nr_seq_contestacao_proc,
		null
	from	pls_conta_proc b,
		pls_discussao_proc a
	where	a.nr_seq_conta_proc	= b.nr_sequencia
	and	a.nr_seq_discussao	= nr_seq_discussao_p
	union
	select	b.nr_sequencia,
		'M',
		b.nr_seq_material,
		null,
		b.ie_tipo_despesa,
		'',
		b.dt_atendimento,
		b.tx_reducao_acrescimo,
		b.qt_material_imp,
		b.qt_material,
		b.vl_unitario,
		b.vl_liberado,
		b.vl_unitario_imp,
		--nvl(b.vl_taxa_material_imp,0) + nvl(b.vl_material_imp,0),
		b.vl_material,
		b.ie_status,
		'',
		null,
		a.qt_contestada,
		a.vl_contestado,
		a.qt_recurso,
		a.vl_recurso,
		a.qt_aceita,
		a.vl_aceito,
		a.qt_negada,
		a.vl_negado,
		a.nr_sequencia,
		null,
		a.nr_seq_contestacao_mat
	from	pls_conta_mat b,
		pls_discussao_mat a
	where	a.nr_seq_conta_mat	= b.nr_sequencia
	and	a.nr_seq_discussao	= nr_seq_discussao_p;

begin

select	max(b.nr_seq_conta)
into	nr_seq_conta_w
from	pls_contestacao b,
	pls_contestacao_discussao a
where	a.nr_seq_contestacao	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_discussao_p;

delete from w_pls_discussao_item
where nr_seq_discussao	= nr_seq_discussao_p;

/* Obter dados conta */
select	nr_seq_prestador_exec,
	nr_seq_prestador,
	nr_seq_grau_partic,
	nr_seq_conta_referencia,
	cd_medico_executor,
	ie_tipo_guia,
	cd_guia_referencia,
	cd_guia,
	nr_seq_protocolo,
	ie_status,	
	obter_valor_dominio(1746,ie_tipo_guia),
	obter_valor_dominio(1961,ie_status),
	nr_seq_guia,
	ie_carater_internacao,
	dt_atendimento_referencia,
	nr_seq_segurado,
	pls_obter_dados_segurado(nr_seq_segurado,'C'),
	pls_obter_dados_segurado(nr_seq_segurado,'N'),	
	nr_seq_cbo_saude
into	nr_seq_prestador_exec_w,
	nr_seq_prestador_solic_w,
	nr_seq_grau_partic_w,
	nr_seq_conta_referencia_w,
	cd_medico_executor_w,
	ie_tipo_guia_w,
	cd_guia_referencia_w,
	cd_guia_w,
	nr_seq_protocolo_w,
	ie_status_conta_w,	
	ds_tipo_guia_w,
	ds_status_conta_w,
	nr_seq_guia_w,
	ie_carater_internacao_w,
	dt_atend_ref_w,
	nr_seq_segurado_w,
	cd_usuario_plano_w,
	nm_segurado_w,
	nr_seq_cbo_saude_w
from	pls_conta
where	nr_sequencia	= nr_seq_conta_w;

begin
select	nr_seq_apres
into	nr_seq_apres_w
from	pls_grau_participacao
where	nr_sequencia = nr_seq_grau_partic_w;
exception
when others then
	nr_seq_apres_w := null;
end;

if	(cd_medico_executor_w is not null) then
	ds_grau_participacao_w 	:= pls_obter_grau_participacao(nr_seq_grau_partic_w);
	nm_participante_w	:= Obter_nome_medico(cd_medico_executor_w,'N');
end if;

open C01;
loop
fetch C01 into	
	nr_seq_item_w,
	ie_tipo_item_w,
	cd_item_w,
	ie_origem_proced_w,
	ie_tipo_despesa_w,
	ie_via_acesso_w,
	dt_item_w,
	tx_item_w,
	qt_apresentado_w,
	qt_liberado_w,
	vl_unitario_w,
	vl_total_w,
	vl_unitario_apres_w,
	--vl_total_apres_w,
	vl_calculado_w,
	ie_status_w,
	cd_porte_anestesico_w,
	nr_auxiliares_w,
	qt_contestada_w,
	vl_contestado_w,
	qt_recurso_w,
	vl_recurso_w,
	qt_aceita_w,
	vl_aceito_w,
	qt_negada_w,
	vl_negado_w,
	nr_seq_disc_item_w,
	nr_seq_contestacao_proc_w,
	nr_seq_contestacao_mat_w;
exit when C01%notfound;
	begin		
	ie_nota_fiscal_w := '';
	
	if	(ie_tipo_item_w = 'P') then
		select	nvl(max(b.vl_procedimento),0)
		into	vl_total_apres_w
		from	pls_contestacao	a,
			pls_contestacao_discussao c,
			pls_contestacao_proc b
		where	a.nr_sequencia		= b.nr_seq_contestacao
		and	a.nr_sequencia		= c.nr_seq_contestacao
		and	c.nr_sequencia		= nr_seq_discussao_p
		and	b.nr_seq_conta_proc 	= nr_seq_item_w
		and	b.nr_sequencia		= nr_seq_contestacao_proc_w;
	
	elsif	(ie_tipo_item_w = 'M') then
		select	nvl(max(b.vl_material),0)
		into	vl_total_apres_w
		from	pls_contestacao	a,
			pls_contestacao_discussao c,
			pls_contestacao_mat b
		where	a.nr_sequencia		= b.nr_seq_contestacao
		and	a.nr_sequencia		= c.nr_seq_contestacao
		and	c.nr_sequencia		= nr_seq_discussao_p
		and	b.nr_seq_conta_mat 	= nr_seq_item_w
		and	b.nr_sequencia		= nr_seq_contestacao_mat_w;
	end if;
	
	/*Diego OS - 282259 - Obter o prestador que receber� pelo pagamento*/
	begin
	select	nm_prestador_pgto,
		nr_seq_prestador_pgto
	into	nm_prestador_pag_w,
		nr_seq_prestador_pgto_w
	from	pls_conta_medica_resumo
	where	nr_seq_conta	= nr_seq_conta_w
	and	ie_tipo_item	= 'P'
	and	nvl(cd_procedimento, pls_obter_seq_codigo_material(nr_seq_material, '')) = cd_item_w
	and	nvl(nr_seq_prest_fornec_w,0) = 0
	and	((ie_situacao != 'I') or (ie_situacao is null))
	union
	select	pls_obter_dados_prestador(nr_seq_prest_fornec_w, 'N'),
		nr_seq_prest_fornec_w
	from	dual
	where	nvl(nr_seq_prest_fornec_w,0) > 0;	
	exception
	when others then				
		nr_seq_prestador_pgto_w := nr_seq_prestador_exec_w;
		nm_prestador_pag_w := pls_obter_dados_prestador(nr_seq_prestador_exec_w,'N');		
	end;	
	
	select	max(nm_prestador)
	into	nm_prestador_pag_w
	from	(select	b.nr_crm||' - '||nm_prestador_pag_w nm_prestador
		from	pls_prestador a,
			medico b
		where	a.nr_sequencia = nr_seq_prestador_pgto_w
		and	nvl(a.cd_cgc,'X') = 'X'
		and	a.cd_pessoa_fisica = b.cd_pessoa_fisica
		union
		select	a.cd_prestador||' - '||nm_prestador_pag_w nm_prestador
		from	pls_prestador a
		where	a.nr_sequencia = nr_seq_prestador_pgto_w
		and	nvl(a.cd_cgc,'X') <> 'X');
		
	
	/*Diego OS 313317 - Obter o prestador exec */
	select	max(nm_prestador)
	into	nm_prestador_w
	from	(select	b.nr_crm||' - '||pls_obter_dados_prestador(nr_seq_prestador_exec_w, 'N') nm_prestador
		from	pls_prestador a,
			medico b
		where	a.nr_sequencia = nr_seq_prestador_exec_w
		and	nvl(a.cd_cgc,'X') = 'X'
		and	a.cd_pessoa_fisica = b.cd_pessoa_fisica
		union
		select	a.cd_prestador||' - '||pls_obter_dados_prestador(nr_seq_prestador_exec_w, 'N') nm_prestador
		from	pls_prestador a
		where	a.nr_sequencia = nr_seq_prestador_exec_w
		and	nvl(a.cd_cgc,'X') <> 'X');

	/*Diego OS 324844 - Obter o prestador solic */
	select	max(nm_prestador)
	into	nm_prestador_solic_w
	from	(select	b.nr_crm||' - '||pls_obter_dados_prestador(nr_seq_prestador_solic_w, 'N') nm_prestador
		from	pls_prestador a,
			medico b
		where	a.nr_sequencia = nr_seq_prestador_solic_w
		and	nvl(a.cd_cgc,'X') = 'X'
		and	a.cd_pessoa_fisica = b.cd_pessoa_fisica
		union
		select	a.cd_prestador||' - '||pls_obter_dados_prestador(nr_seq_prestador_solic_w, 'N') nm_prestador
		from	pls_prestador a
		where	a.nr_sequencia = nr_seq_prestador_solic_w
		and	nvl(a.cd_cgc,'X') <> 'X');
	
	select	decode(ie_origem_proced_w,null,pls_obter_desc_material(cd_item_w),obter_descricao_procedimento(cd_item_w,ie_origem_proced_w)),
		obter_valor_dominio(1268,ie_via_acesso_w)
	into	ds_item_w,
		ds_via_acesso_w
	from	dual;

	select	(nvl(max(nr_identificador),0) + 1)
	into	nr_identificador_w
	from	w_pls_discussao_item
	where	nr_seq_analise = nr_seq_analise_p;
	
	nm_prestador_exec_w := pls_obter_dados_prestador(nr_seq_prestador_exec_w,'N');
	
	select	w_pls_discussao_item_seq.nextval
	into	nr_seq_item_analise_w
	from	dual;
	
	if	(ie_tipo_item_w = 'P') then
		nr_seq_disc_proc_w := nr_seq_disc_item_w;
	elsif	(ie_tipo_item_w = 'M') then
		nr_seq_disc_mat_w := nr_seq_disc_item_w;
	end if;
	
	/* William CB OS 414238 - Retirado o Cursor 2 */
	if	(ie_tipo_item_w = 'P') then
		select	count(a.nr_sequencia)
		into	qt_partics_w
		from	pls_proc_participante	a,
			pls_conta_proc		c
		where	a.nr_seq_conta_proc	= c.nr_sequencia
		and	c.nr_sequencia 		= nr_seq_item_w;

		if	(qt_partics_w = 0) then		 
			ie_status_w := 'L';
		else
			select	max(a.cd_medico),
				max(a.nr_seq_conselho),
				max(a.nr_seq_grau_partic),				
				max(a.nr_seq_prestador),
				max(a.nr_seq_prestador_pgto),		
				max('R'),				
				max(decode(a.nr_seq_prestador,null,nvl(Obter_nome_medico(a.cd_medico,'N'), a.nm_medico_executor_imp),pls_obter_dados_prestador(a.nr_Seq_prestador,'N'))),
				max(pls_obter_grau_participacao(a.nr_seq_grau_partic)),				
				max(b.nr_seq_apres)				
			into	cd_medico_w,
				nr_seq_conselho_w,			
				nr_seq_grau_partic_w,
				nr_seq_prestador_w,
				nr_seq_prestador_pgto_w,
				ie_tipo_item_w,
				nm_participante_w,
				ds_grau_participacao_w,
				nr_seq_apres_w
			from	pls_proc_participante	a,
				pls_grau_participacao	b,				
				pls_conta_proc		c		
			where	a.nr_seq_grau_partic 	= b.nr_sequencia(+)
			and	a.nr_seq_conta_proc	= c.nr_sequencia	
			and	c.nr_sequencia		= nr_seq_item_w;
			
		end if;
	end if;	
	
	select	decode(ie_tipo_item_w || ie_tipo_despesa_w,'P1','1','P2','3','P3','2','P4','4','M1','6','M2','5','M3','7','M7','8', 'R1','1','R2','3','R3','2','R4','4')
	into	nr_ordem_w
	from	dual;
	
	insert into w_pls_discussao_item
		(nr_sequencia,
		nr_ordem,
		nr_seq_conta,
		nr_seq_item,
		dt_item,
		cd_item,
		ie_origem_proced,
		ds_tipo_despesa,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_medico_executor,
		cd_guia_referencia,
		cd_guia,
		ds_item,
		nr_seq_discussao,
		nm_prestador,
		ds_tipo_guia,
		ie_tipo_item,
		nr_seq_motivo_glosa_aceita,
		nr_seq_motivo_glosa_neg,
		nr_seq_analise,
		nm_prestador_solic,
		cd_usuario_plano,
		nm_prestador_exec,
		nm_segurado,
		ie_tipo_despesa,
		ie_tipo_guia,
		ie_via_acesso,
		ds_via_acesso,
		nr_identificador,
		nr_seq_protocolo,
		qt_apresentado,
		vl_total_apres,
		qt_contestada,
		vl_contestado,
		qt_recurso,
		vl_recurso,
		qt_negada,
		vl_negado,
		qt_aceita,
		vl_aceito,
		nr_seq_disc_mat,
		nr_seq_disc_proc,
		nr_seq_partic_proc,
		nm_participante)
	values	(nr_seq_item_analise_w,
		nr_ordem_w,
		nr_seq_conta_w,
		nr_seq_item_w,
		dt_item_w,
		cd_item_w,
		ie_origem_proced_w,
		ie_tipo_item_w || ie_tipo_despesa_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_medico_executor_w,
		cd_guia_referencia_w,
		cd_guia_w,
		ds_item_w,
		nr_seq_discussao_p,
		nm_prestador_w,
		ds_tipo_guia_w,
		ie_tipo_item_w,
		nr_seq_motivo_glosa_aceita_w,
		nr_seq_motivo_glosa_neg_w,
		nr_seq_analise_p,
		nm_prestador_solic_w,
		cd_usuario_plano_w,
		nm_prestador_exec_w,
		nm_segurado_w,
		ie_tipo_despesa_w,
		ie_tipo_guia_w,
		ie_via_acesso_w,
		ds_via_acesso_w,
		nr_identificador_w,
		nr_seq_protocolo_w,
		qt_apresentado_w,
		vl_total_apres_w,
		qt_contestada_w,
		vl_contestado_w,
		qt_recurso_w,
		vl_recurso_w,
		qt_negada_w,
		vl_negado_w,
		qt_aceita_w,
		vl_aceito_w,
		nr_seq_disc_mat_w,
		nr_seq_disc_proc_w,
		nr_seq_partic_w,
		nm_participante_w);
	end;
end loop;
close C01;

end pls_gerar_w_discussao_item;
/