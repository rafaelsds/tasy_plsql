create or replace
procedure bionexo_gerar_ordem_compra(		nr_cot_compra_p		number,
					ie_forma_calc_entrega_p	varchar2,
					nm_usuario_p		varchar2) is

cd_estabelecimento_w		Number(5);
cd_material_w			number(6)	:= 0;
nr_item_cot_compra_w		number(5)	:= 0;
nr_item_oci_w			number(5)	:= 0;
nr_ordem_compra_w		number(10)	:= 0;
nr_seq_fornecedor_w		number(10)	:= 0;
nr_seq_fornecedor_w2		number(10)	:= 0;
qt_conv_unid_fornec_w		number(09,4);
qt_material_w			number(13,4)	:= 0;
qt_material_oci_w		number(13,4)	:= 0;
qt_material_solic_w		number(13,4)	:= 0;
vl_unitario_w			number(13,4)	:= 0;
pr_desconto_w			number(13,4)	:= 0;
vl_desconto_w			number(13,2)	:= 0;
cd_unidade_w			varchar2(30)	:= '0';
ds_material_direto_w		varchar2(255)	:= '0';
ds_observacao_w			varchar2(255)	:= '0';
ds_marca_w			varchar2(30)	:= '';
ds_marca2_w			varchar2(30)	:= '';
ie_situacao_w			varchar2(1)	:= '0';
dt_entrega_w			date;
dt_validade_w			date;
ie_ajusta_unid_fornec_w		varchar2(1)	:= 'S';
nr_contrato_w			number(10);
dt_entrega_ww			date;
nr_solic_compra_w			number(10);
nr_item_solic_compra_w		number(10);
nr_documento_externo_w		varchar2(100);
dt_baixa_w			date;
qt_itens_solic_w		number(10);
qt_itens_baixados_w		number(10);
qt_mat_solic_w			number(13,5);
qt_mat_ordens_w			number(13,5);
nr_ordem_liberada_w		ordem_compra.nr_ordem_compra%type;
ds_material_item_w		material.ds_material%type;
nr_item_oci_lib_w		ordem_compra_item.nr_item_oci%type;
ds_erro_w			varchar2(255);
ie_divergente_w			varchar2(1) := 'N';
nr_solic_compra_ww		solic_compra.nr_solic_compra%type;

cursor c01 is
select	a.cd_estabelecimento,
	c.nr_seq_cot_forn,
	c.nr_item_cot_compra,
	c.cd_material,
	c.cd_unidade_medida_compra,
	nvl(c.ie_situacao,'A'),
	sum(c.qt_material),
	c.ds_observacao,
	substr(nvl(c.ds_marca_fornec,decode(instr(c.ds_marca,'('),0,c.ds_marca,substr(c.ds_marca,1,instr(c.ds_marca,'(')-1))),1,30),
	substr(decode(instr(c.ds_marca,'('),0,c.ds_marca,substr(c.ds_marca,1,instr(c.ds_marca,'(')-1)),1,30),
	c.dt_limite_entrega,
	nvl(c.vl_unitario_material,0),
	nvl(c.pr_desconto,0),
	nvl(c.qt_conv_unid_fornec,0),
	c.dt_validade,
	nvl(c.vl_desconto,0)
from	cot_compra a,
	cot_compra_resumo_v c
where	c.nr_cot_compra      = nr_cot_compra_p
and	(substr(obter_se_existe_cot_resumo(c.nr_cot_compra),1,1) = 'S')
and	c.nr_cot_compra      = a.nr_cot_compra
and	obter_dados_cot_compra_item(a.nr_cot_compra, c.nr_item_cot_compra, 'DBI') is null
group by
	a.cd_estabelecimento,
	c.nr_seq_cot_forn,
	c.nr_item_cot_compra,
	c.cd_material,
	c.cd_unidade_medida_compra,
	nvl(c.ie_situacao,'A'),
	c.ds_observacao,
	nvl(c.ds_marca_fornec,decode(instr(c.ds_marca,'('),0,c.ds_marca,substr(c.ds_marca,1,instr(c.ds_marca,'(')-1))),
	substr(decode(instr(c.ds_marca,'('),0,c.ds_marca,substr(c.ds_marca,1,instr(c.ds_marca,'(')-1)),1,30),
	c.dt_limite_entrega,
	nvl(c.vl_unitario_material,0),
	nvl(c.pr_desconto,0),
	nvl(c.qt_conv_unid_fornec,0),
	c.dt_validade,
	nvl(c.vl_desconto,0)
order by c.nr_seq_cot_forn,
	nr_item_cot_compra;

cursor c02 is
select	nr_ordem_compra
from	ordem_compra_item
where	nr_cot_compra = nr_cot_compra_p
group by nr_ordem_compra;

cursor c03 is
select	nr_ordem_compra,
	nr_item_oci,
	qt_material,
	nr_item_cot_compra
from	ordem_compra_item
where	nr_cot_compra = nr_cot_compra_p
and	nr_cot_compra is not null;

cursor c04 is
select	distinct
	a.nr_solic_compra,
	a.nr_item_solic_compra,
	a.qt_material
from	cot_compra_solic_agrup a
where	nr_cot_compra = nr_cot_compra_p
and	nr_item_cot_compra = nr_item_cot_compra_w
and	nr_solic_compra is not null;

cursor c05 is
select	a.nr_solic_compra
from	cot_compra_solic_agrup a
where	nr_cot_compra = nr_cot_compra_p
and	nr_solic_compra is not null
group by a.nr_solic_compra;

cursor c06 is
select	nr_item_solic_compra,
	nr_solic_compra,
	substr(obter_desc_material(cd_material),1,255) ds_material,
	nr_item_oci
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_w;


begin
select	nvl(max(b.ie_ajusta_unid_fornec),'N'),
	nvl(max(a.nr_documento_externo),'')
into	ie_ajusta_unid_fornec_w,
	nr_documento_externo_w
from	parametro_compras b,
	cot_compra a
where	a.cd_estabelecimento = b.cd_estabelecimento
and	a.nr_cot_compra = nr_cot_compra_p;

open C01;
loop
fetch C01 into	
	cd_estabelecimento_w,
	nr_seq_fornecedor_w,
	nr_item_cot_compra_w,
	cd_material_w,
	cd_unidade_w,
	ie_situacao_w,
	qt_material_w,
	ds_observacao_w,
	ds_marca_w,
	ds_marca2_w,
	dt_entrega_w,
	vl_unitario_w,
	pr_desconto_w,
	qt_conv_unid_fornec_w,
	dt_validade_w,
	vl_desconto_w;
exit when C01%notfound;
	begin
	
	select	max(nr_contrato)
	into	nr_contrato_w
	from	cot_compra_item
	where	nr_cot_compra = nr_cot_compra_p
	and	nr_item_cot_compra = nr_item_cot_compra_w;
	
	if	(ie_forma_calc_entrega_p = 'L') then
			dt_entrega_ww	:= dt_entrega_w + nvl(obter_dados_cot_compra_forn(nr_cot_compra_p, nr_seq_fornecedor_w,'DE'), 0);
	elsif	(ie_forma_calc_entrega_p = 'F') then
		dt_entrega_ww	:= dt_entrega_w;
	else
		dt_entrega_ww	:= trunc(sysdate,'dd') + nvl(obter_dados_cot_compra_forn(nr_cot_compra_p, nr_seq_fornecedor_w,'DE'), 0);
	end if;

	if	(nr_seq_fornecedor_w2 <> nr_seq_fornecedor_w) then	
		grava_ordem_compra(
			nr_cot_compra_p,
			cd_estabelecimento_w,
			nr_seq_fornecedor_w,
			nm_usuario_p,
			nr_ordem_compra_w);		
			
		grava_item_ordem_compra(
			nr_ordem_compra_w,
			cd_material_w,
			cd_unidade_w,
			vl_unitario_w,
			qt_material_w,
			ds_observacao_w,
			ds_marca_w,
			ds_marca2_w,
			nm_usuario_p,
			ie_situacao_w,
			dt_entrega_ww,
			pr_desconto_w,
			nr_cot_compra_p,
			nr_item_cot_compra_w,
			nr_seq_fornecedor_w,
			qt_conv_unid_fornec_w,
			ie_ajusta_unid_fornec_w,
			dt_validade_w,
			'N',
			nvl(vl_desconto_w,0),
			nr_contrato_w,
			null,
			null);
			
		nr_seq_fornecedor_w2 	:= nr_seq_fornecedor_w;
	else
		grava_item_ordem_compra(
			nr_ordem_compra_w,
			cd_material_w,
			cd_unidade_w,
			vl_unitario_w,
			qt_material_w,
			ds_observacao_w,
			ds_marca_w,
			ds_marca2_w,
			nm_usuario_p,
			ie_situacao_w,
			dt_entrega_ww,
			pr_desconto_w,
			nr_cot_compra_p,
			nr_item_cot_compra_w,
			nr_seq_fornecedor_w,
			qt_conv_unid_fornec_w,
			ie_ajusta_unid_fornec_w,
			dt_validade_w,
			'N',
			nvl(vl_desconto_w,0),
			nr_contrato_w,
			null,
			null);
	end if;
	
	gerar_ordem_compra_venc( nr_ordem_compra_w, nm_usuario_p);
	calcular_liquido_ordem_compra( nr_ordem_compra_w, nm_usuario_p);
	
	update	ordem_compra
	set	ie_sistema_origem 	= 'WCF'
	where	nr_ordem_compra		= nr_ordem_compra_w;
	
	begin
	update	ordem_compra
	set	nr_documento_externo	= nr_documento_externo_w
	where	nr_ordem_compra		= nr_ordem_compra_w;
	exception
	when others then
		nr_documento_externo_w := nr_documento_externo_w;
	end;
	end;
end loop;
close C01;

commit;

open C02;
loop
fetch C02 into	
	nr_ordem_compra_w;
exit when C02%notfound;
	begin
	ie_divergente_w := 'N';
	
	open C06;
	loop
	fetch C06 into	
		nr_item_solic_compra_w,
		nr_solic_compra_ww,
		ds_material_item_w,
		nr_item_oci_lib_w;
	exit when C06%notfound;
		begin
		
		if	(nr_solic_compra_ww > 0) then
		
			select	sum(qt_material)			
			into	qt_mat_solic_w		
			from	solic_compra_item
			where	nr_item_solic_compra = nr_item_solic_compra_w
			and	nr_solic_compra = nr_solic_compra_ww;
			
			select	sum(b.qt_material)
			into	qt_mat_ordens_w
			from	ordem_compra a,
				ordem_compra_item b
			where	a.nr_ordem_compra = b.nr_ordem_compra
			and	b.nr_item_solic_compra = nr_item_solic_compra_w
			and	b.nr_solic_compra = nr_solic_compra_ww
			and	((a.dt_liberacao is not null) or (a.nr_ordem_compra = nr_ordem_compra_w));
			
			if (qt_mat_ordens_w > qt_mat_solic_w) then
				ie_divergente_w := 'S';									
			end if;		
		end if;
		end;
	end loop;
	close C06;
		
	if (ie_divergente_w = 'N') then	
	
		update	ordem_compra_item
		set	qt_original	= qt_material
		where	nr_ordem_compra	= nr_ordem_compra_w
		and	qt_original is null;
		
		update	ordem_compra_item
		set	vl_unit_mat_original = vl_unitario_material
		where	nr_ordem_compra	= nr_ordem_compra_w
		and	vl_unit_mat_original is null;
		
		update	ordem_compra_item
		set	dt_aprovacao	= sysdate
		where	nr_ordem_compra	= nr_ordem_compra_w;
		
		update	ordem_compra
		set	dt_liberacao	= sysdate,
			nm_usuario_lib	= nm_usuario_p,
			dt_aprovacao	= sysdate
		where	nr_ordem_compra	= nr_ordem_compra_w;		
	

	end if;
	
	end;
end loop;
close C02;


open C03;
loop
fetch C03 into	
	nr_ordem_compra_w,
	nr_item_oci_w,
	qt_material_oci_w,
	nr_item_cot_compra_w;
exit when C03%notfound;
	begin
	
	open C04;
	loop
	fetch C04 into	
		nr_solic_compra_w,
		nr_item_solic_compra_w,
		qt_material_solic_w;
	exit when C04%notfound;
		begin
		
		if	(qt_material_oci_w >= qt_material_solic_w) then
			baixar_item_solic_bionexo(nr_ordem_compra_w, nr_solic_compra_w, nr_item_solic_compra_w, nm_usuario_p);			
		end if;	
		
		end;
	end loop;
	close C04;
	
	end;
end loop;
close C03;

open C05;
loop
fetch C05 into	
	nr_solic_compra_w;
exit when C05%notfound;
	begin
	
	select	dt_baixa
	into	dt_baixa_w
	from	solic_compra
	where	nr_solic_compra = nr_solic_compra_w;
	
	if	(dt_baixa_w is not null) then
		atualiza_status_solic_bionexo(nr_solic_compra_w, 'RC', '', nm_usuario_p);
	else
		select	count(*)
		into	qt_itens_solic_w
		from	solic_compra_item
		where	nr_solic_compra = nr_solic_compra_w;
		
		select	count(*)
		into	qt_itens_baixados_w
		from	solic_compra_item
		where	nr_solic_compra = nr_solic_compra_w
		and	dt_baixa is not null;
		
		if	(qt_itens_solic_w > qt_itens_baixados_w) then
			atualiza_status_solic_bionexo(nr_solic_compra_w, 'RP', '', nm_usuario_p);
		end if;	
	end if;
	end;
end loop;
close C05;

commit;


end bionexo_gerar_ordem_compra;
/