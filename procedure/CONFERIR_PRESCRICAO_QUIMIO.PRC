create or replace
procedure conferir_prescricao_quimio(
		nr_prescricao_p	number,
		ie_conferido_p	varchar2,
		nm_usuario_p	varchar2) is 

begin

update	can_ordem_prod
set	nm_usuario_conf	= nm_usuario_p,
	ie_conferido	= ie_conferido_p
where	nr_prescricao	= nr_prescricao_p;

commit;

end conferir_prescricao_quimio;
/