create or replace
procedure pls_gerar_ops_congenere_benef
			(	nr_seq_segurado_p	number,
				cd_usuario_plano_p	varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2,
				ie_commit_p		varchar2) is 

cd_operadora_empresa_w		number(10);
nr_seq_ops_congenere_w		number(10);
nr_seq_congenere_w		number(10);
nr_seq_congenere_aux_w		number(10);
ie_tipo_congenere_w		varchar2(2);
cd_usuario_plano_w		varchar2(30);
nr_seq_pj_empresa_w		number(10);
qt_registros_w			number(10);
cd_cooperativa_w		varchar2(10);

Cursor C01 is
	select	b.nr_sequencia,
		a.nr_sequencia
	from	pls_pj_empresa	a,
		pls_congenere	b
	where	a.cd_cgc	= b.cd_cgc
	and	a.cd_empresa	= cd_operadora_empresa_w;

begin

select	max(nr_seq_congenere)
into	nr_seq_congenere_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

if	(cd_usuario_plano_p is not null) then
	cd_usuario_plano_w	:= cd_usuario_plano_p;
else
	select	max(cd_usuario_plano)
	into	cd_usuario_plano_w
	from	pls_segurado_carteira
	where	nr_seq_segurado	= nr_seq_segurado_p;
end if;

/*aaschlote 10/03/2014 - OS 700610*/
if	(length(cd_usuario_plano_w) = 17) then
	cd_cooperativa_w	:= substr(cd_usuario_plano_w,1,4);
	select	max(nr_sequencia)
	into	nr_seq_congenere_aux_w
	from	pls_congenere
	where	to_number(cd_cooperativa) = to_number(cd_cooperativa_w);
end if;

cd_operadora_empresa_w	:= pls_obter_cod_empresa_cart(cd_usuario_plano_w, cd_estabelecimento_p);

if	(nvl(cd_operadora_empresa_w,0) <> 0) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_ops_congenere_w,
		nr_seq_pj_empresa_w;
	exit when C01%notfound;
		begin
		
		/*aaschlote 05/04/2013 OS - 571004*/
		select	count(1)
		into	qt_registros_w
		from	PLS_PJ_EMPRESA_COOPERATIVA
		where	NR_SEQ_COD_PJ_EMPRESA	= nr_seq_pj_empresa_w
		and	rownum			<= 1;
		
		if	(qt_registros_w	> 0) then
			select	count(1)
			into	qt_registros_w
			from	PLS_PJ_EMPRESA_COOPERATIVA
			where	NR_SEQ_COD_PJ_EMPRESA	= nr_seq_pj_empresa_w
			and	nr_seq_congenere	= nr_seq_congenere_aux_w
			and	rownum			<= 1;
			
			if	(qt_registros_w	= 0) then
				nr_seq_ops_congenere_w	:= null;
			end if;
		end if;
		
		end;
	end loop;
	close C01;
	
	if	(nvl(nr_seq_ops_congenere_w,0) <> 0) then
		update	pls_segurado
		set	nr_seq_ops_congenere	= nr_seq_ops_congenere_w,
			cd_operadora_empresa	= cd_operadora_empresa_w
		where	nr_sequencia		= nr_seq_segurado_p;
	end if;
else
	select	max(ie_tipo_congenere)
	into	ie_tipo_congenere_w
	from	pls_congenere
	where	nr_sequencia	= nr_seq_congenere_w;
	
	if	(ie_tipo_congenere_w = 'OP') then
		update	pls_segurado
		set	nr_seq_ops_congenere	= nr_seq_congenere_w
		where	nr_sequencia		= nr_seq_segurado_p;
	end if;
end if;

if	(nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;

end pls_gerar_ops_congenere_benef;
/
