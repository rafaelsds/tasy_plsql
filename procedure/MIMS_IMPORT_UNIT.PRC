create or replace procedure MIMS_IMPORT_UNIT(	cd_unidade_medida_p	varchar2,

				ds_unidade_medida_p	varchar2,

				nm_usuario_p		varchar2,
        nr_mims_ver_p number) is 

qt_reg_w	number(10);

unidade_medida_w	imp_unidade_medida%rowtype;

begin







select	count(1)

into	qt_reg_w

from	imp_unidade_medida

where	cd_unidade_medida	= cd_unidade_medida_p;



if	(qt_reg_w	= 0) then

	

	unidade_medida_w.cd_unidade_medida	:= cd_unidade_medida_p;

	unidade_medida_w.ds_unidade_medida	:= substr(ds_unidade_medida_p,1,40);

	unidade_medida_w.dt_atualizacao		:= sysdate;

	unidade_medida_w.dt_atualizacao_nrec	:= sysdate;

	unidade_medida_w.nm_usuario			:= nm_usuario_p;

	unidade_medida_w.nm_usuario_nrec	:= nm_usuario_p;

	unidade_medida_w.ie_situacao		:= 'A';

	unidade_medida_w.IE_ADM_DILUICAO	:= 'N';

  unidade_medida_w.NR_SEQ_MIMS_VERSION := nr_mims_ver_p;
	 

	insert into imp_unidade_medida values unidade_medida_w;  



end if; 





end mims_import_unit;
/