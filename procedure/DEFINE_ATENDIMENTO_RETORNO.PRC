create or replace
procedure Define_Atendimento_Retorno(
							nr_atendimento_p		number,
							nr_atend_retorno_p	out 	number,
							nr_seq_regra_msg_p	out	number ) is

cd_estabelecimento_w		number(4);
cd_pessoa_fisica_w		varchar2(10);
ie_tipo_atendimento_w		number(3);
cd_convenio_w			number(5);
cd_setor_atendimento_w		number(5);
cd_medico_resp_w			varchar2(10);
qt_retorno_w			number(3);
nr_atendimento_w			number(10);
cd_medico_w			varchar2(10);
ie_regra_w			varchar2(1);
qt_dia_w				number(5);
ie_ok_w				integer;
ie_ok_atual_w			integer;
qt_reg_w				number(3);
nr_seq_classificacao_w		number(10);
nr_seq_classificacao_regra_w	number(10);
ie_dentro_mes_w			Varchar2(1)	:=  'N';
ie_dentro_dia_w			Varchar2(1)	:= 'N';
dt_entrada_w			Date;
dt_atual_w			Date;
nr_seq_queixa_w			number(10);
nr_seq_queixa_origem_w		number(10);
ie_consiste_setor_w		varchar2(10);
cd_cid_principal_w			varchar2(10);
cd_cid_principal_origem_w		varchar2(10);
cd_categoria_cid_w		varchar2(10);
cd_categoria_cid_origem_w		varchar2(10);
ie_mesmo_tipo_atend_w		varchar2(1);
qt_existe_w			number(10);
cd_espec_atend_w			number(5);
cd_espec_atend_retorno_w		number(5);
qt_hora_w			number(5);
ie_considera_atend_retorno_w	varchar2(1);
ie_tipo_atend_tiss_w		varchar2(2);
ie_clinica_w			number(5);
cd_categoria_w			varchar2(10);
nr_seq_origem_w			number(10);
cd_plano_convenio_w		varchar2(10);
nr_pos_inicial_w			number(3);
nr_pos_final_w			number(3);
cd_usuario_padrao_w		varchar2(30);
nr_seq_regra_w			number(10);
ie_atendimento_cancelado_w		varchar(1);
ie_consiste_classif_w		varchar2(1);
cd_usuario_convenio_w		varchar2(30);
ie_tipo_convenio_w		                number(2);
ds_mensagem_w			varchar2(4000);

ds_retorno_proc_w		varchar2(255);
ie_consiste_categ_w		varchar2(10);
ie_consiste_plano_w		varchar2(10);

ie_mesmo_estab_w		 regra_atend_retorno.ie_mesmo_estab%TYPE;
ie_data_alta_w		 regra_atend_retorno.ie_data_alta%TYPE;
ie_consiste_mot_alt_atend_w  regra_atend_retorno.ie_consiste_mot_alt_atend%TYPE;
cd_motivo_alta_w                    regra_atend_retorno.cd_motivo_alta%TYPE;
IE_ESPEC_MEDICO_RETORNO_w		REGRA_ATEND_RETORNO.IE_ESPEC_MEDICO_RETORNO%type;
IE_MESMO_TIPO_TISS_w			REGRA_ATEND_RETORNO.IE_MESMO_TIPO_TISS%type;
IE_IGNORA_ESPEC_MEDICA_w			REGRA_ATEND_RETORNO.IE_IGNORA_ESPEC_MEDICA%type;
IE_IGNORA_CLINICA_ATEND_w			REGRA_ATEND_RETORNO.IE_IGNORA_CLINICA_ATEND%type;

ie_classif_agenda_con_w		Agenda_consulta.ie_classif_agenda%type;
ie_classif_agenda_pac_w		Agenda_paciente.nr_seq_classif_agenda%type;

cursor C01 is
	select	ie_regra,
		qt_dia,
		nvl(qt_horas,0),
		nvl(qt_retorno,0),
		nvl(ie_dentro_mes,'N'),
		nvl(ie_dentro_dia,'N'),
		nvl(ie_consiste_setor,'S'),
		ie_mesmo_tipo_atend,
		nvl(ie_considera_atend_retorno,'N'),
		nvl(ie_atendimento_cancelado,'S'),
		nr_pos_inicial,
		nr_pos_final,
		cd_usuario_padrao,
		nr_sequencia,
		ie_consiste_classificacao,
		nr_seq_classificacao,
		ds_mensagem,
		nvl(ie_consiste_categoria,'S'),
		nvl(ie_consiste_plano,'S'),
		NVL(ie_mesmo_estab,'S'),
		NVL(ie_data_alta,'N'),
		cd_motivo_alta,
		nvl(ie_consiste_mot_alt_atend,'N'),
		nvl(IE_ESPEC_MEDICO_RETORNO, 'N'),
		nvl(IE_MESMO_TIPO_TISS, 'N'),
		nvl(IE_IGNORA_ESPEC_MEDICA,'N'),
		nvl(IE_IGNORA_CLINICA_ATEND,'N')
	from	regra_atend_retorno
	where	(nvl(cd_estabelecimento, cd_estabelecimento_w)		= cd_estabelecimento_w or (cd_estabelecimento_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(nvl(ie_tipo_atendimento, ie_tipo_atendimento_w) 	= ie_tipo_atendimento_w or (ie_tipo_atendimento_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(nvl(cd_convenio, cd_convenio_w) 			= cd_convenio_w or (cd_convenio_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(nvl(cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w or (cd_setor_atendimento_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(((nvl(nr_seq_classificacao, nr_seq_classificacao_w)	= nr_seq_classificacao_w or (nr_seq_classificacao_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))) or
		 (ie_consiste_classificacao = 'N')) -- amramos - Nao consistir a classificacao do atend. anterior - OS 304565
	and	(nvl(ie_tipo_atend_tiss, ie_tipo_atend_tiss_w)		= ie_tipo_atend_tiss_w or (ie_tipo_atend_tiss_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(nvl(cd_especialidade_medica,cd_espec_atend_w)		= cd_espec_atend_w or (cd_espec_atend_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and 	(nvl(ie_clinica,ie_clinica_w)				= ie_clinica_w	   or (ie_clinica_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(nvl(cd_categoria,cd_categoria_w)			= cd_categoria_w or (cd_categoria_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(nvl(nr_seq_origem,nr_seq_origem_w)			= nr_seq_origem_w or (nr_seq_origem_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(nvl(cd_plano_convenio,cd_plano_convenio_w)             = cd_plano_convenio_w or (cd_plano_convenio_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(nvl(ie_tipo_convenio,ie_tipo_convenio_w)               = ie_tipo_convenio_w or (ie_tipo_convenio_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and	(((cd_usuario_padrao is null) and (obter_se_uso_conv_reg_retorno(cd_convenio_w, cd_usuario_convenio_w) = 'N')) or
		 (substr(cd_usuario_convenio_w, nr_pos_inicial, (nr_pos_final - nr_pos_inicial) + 1) = cd_usuario_padrao))
	and nvl(ie_regra_agenda,'A') not in ('G','O')
	and (nvl(ie_classif_agenda_pac, ie_classif_agenda_pac_w) 	= ie_classif_agenda_pac_w or (ie_classif_agenda_pac_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and (nvl(ie_classif_agenda_con, ie_classif_agenda_con_w) 	= ie_classif_agenda_con_w or (ie_classif_agenda_con_w is null and nvl(ie_validar_campo_vazio, 'N') = 'N'))
	and sysdate between nvl(dt_inicio_vigencia, sysdate) and nvl(dt_fim_vigencia, sysdate)
	and nvl(ie_situacao,'A') = 'A'
	order by nvl(cd_convenio,0), nvl(cd_estabelecimento,0), nvl(ie_tipo_atendimento,0), nvl(cd_setor_atendimento,0),
	         nvl(nr_seq_classificacao,0), nvl(ie_tipo_atend_tiss,'0'), nvl(cd_especialidade_medica,0), nvl(ie_clinica,0), nvl(cd_categoria,'0'),
		     nvl(nr_seq_origem,0), nvl(cd_plano_convenio,'0'), nvl(ie_tipo_convenio,0), 
		     nvl(ie_classif_agenda_pac,0), nvl(IE_CLASSIF_AGENDA_CON,'0'), nvl(qt_dia,0);

cursor C02 is
	select	nr_atendimento,
		cd_medico_resp,
		nr_seq_queixa,
		cd_cid_principal
	from 	Atendimento_paciente_v3 a
	where	((ie_mesmo_estab_w = 'N' AND nvl(a.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
	and 	((nvl(a.ie_tipo_atendimento, ie_tipo_atendimento_w) 	= ie_tipo_atendimento_w or ie_tipo_atendimento_w is null) or ie_mesmo_tipo_atend_w = 'N')
	and 	(nvl(a.cd_convenio, cd_convenio_w) 			= cd_convenio_w or cd_convenio_w is null)
	and 	((nvl(a.ie_clinica, ie_clinica_w) 			= ie_clinica_w or ie_clinica_w is null)  or IE_IGNORA_CLINICA_ATEND_w = 'S')
	and	((nvl(a.cd_setor_atendimento, cd_setor_atendimento_w)	= cd_setor_atendimento_w or cd_setor_atendimento_w is null
	and 	ie_consiste_setor_w = 'S') or (ie_consiste_setor_w = 'N'))
	and	((nvl(a.nr_seq_classificacao, nr_seq_classificacao_w)	= nr_seq_classificacao_w or nr_seq_classificacao_w is null) or
		 (ie_consiste_classif_w = 'N')) -- amramos - Nao consistir a classificacao do atend. anterior - OS 304565)
	and	(nvl(a.ie_tipo_atend_tiss, ie_tipo_atend_tiss_w)	= ie_tipo_atend_tiss_w or ie_tipo_atend_tiss_w is null)
	and	(((nvl(Obter_Espec_medico_atend(a.nr_atendimento,a.cd_medico_resp,'C'),cd_espec_atend_w)		= cd_espec_atend_w)
	or	(ie_regra_w = 'Q')
	or  ((ie_regra_w = 'R') and IE_IGNORA_ESPEC_MEDICA_w = 'S')) 
	or 	cd_espec_atend_w is null)
	and 	a.cd_pessoa_fisica					= cd_pessoa_fisica_w
	and 	a.nr_atendimento					<> nr_atendimento_p
	and	((nvl(a.cd_categoria, cd_categoria_w)	= cd_categoria_w or cd_categoria_w is null
	and 	ie_consiste_categ_w = 'S') or (ie_consiste_categ_w = 'N'))
	and	(nvl(a.nr_seq_origem,nr_seq_origem_w)			= nr_seq_origem_w or nr_seq_origem_w is null)
	and	((nvl(a.cd_plano_convenio, cd_plano_convenio_w)	= cd_plano_convenio_w or cd_plano_convenio_w is null
	and 	ie_consiste_plano_w = 'S') or (ie_consiste_plano_w = 'N'))
	and 	((qt_dia_w > 0  and a.dt_entrada between sysdate - qt_dia_w and sysdate) or
		(qt_dia_w  = 0 and qt_hora_w > 0  and a.dt_entrada between sysdate - (qt_hora_w/24) and sysdate))
	and	((ie_data_alta_w = 'S' AND ((qt_dia_w > 0  AND NVL(a.dt_alta,a.DT_ENTRADA) BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
		 (qt_dia_w  = 0 AND qt_hora_w > 0  AND NVL(a.dt_alta,a.DT_ENTRADA) BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))) OR
		 (ie_data_alta_w = 'N' AND ((qt_dia_w > 0  AND a.dt_entrada BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
		 (qt_dia_w  = 0 AND qt_hora_w > 0  AND a.dt_entrada BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))))
	and	((a.nr_atend_original	is null) or (ie_considera_atend_retorno_w = 'S')) -- Dalcastagne em 06/08/2007 OS 64644
	and	(((cd_usuario_padrao_w is null) and (obter_se_uso_conv_reg_retorno(a.cd_Convenio, a.cd_usuario_convenio) = 'N')) or
		 (substr(a.cd_usuario_convenio, nr_pos_inicial_w, (nr_pos_final_w - nr_pos_inicial_w) + 1) = cd_usuario_padrao_w))
	and	(((ie_atendimento_cancelado_w = 'N') and (a.dt_cancelamento is null))  or
		 (ie_atendimento_cancelado_w = 'S'))     --  whwestphal  em  19/04/2011 OS 311972
	and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'))
	order by a.nr_atendimento;




begin

begin
select	cd_estabelecimento,
	cd_pessoa_fisica,
	ie_tipo_Atendimento,
	cd_convenio,
	cd_setor_atendimento,
	cd_medico_resp,
	nvl(nr_seq_classificacao,''),
	nr_seq_queixa,
	cd_cid_principal,
	Obter_Categoria_Cid(cd_cid_principal),
	ie_tipo_atend_tiss,
	ie_clinica,
	cd_categoria,
	nr_seq_origem,
	cd_plano_convenio,
	cd_usuario_convenio,
	ie_tipo_convenio
into	cd_estabelecimento_w,
	cd_pessoa_fisica_w,
	ie_tipo_atendimento_w,
	cd_convenio_w,
	cd_setor_atendimento_w,
	cd_medico_resp_w,
	nr_seq_classificacao_w,
	nr_seq_queixa_origem_w,
	cd_cid_principal_origem_w,
	cd_categoria_cid_origem_w,
	ie_tipo_atend_tiss_w,
	ie_clinica_w,
	cd_categoria_w,
	nr_seq_origem_w,
	cd_plano_convenio_w,
	cd_usuario_convenio_w,
	ie_tipo_convenio_w
from	Atendimento_paciente_v3
where	nr_atendimento = nr_atendimento_p;
exception
when others then
	cd_estabelecimento_w		:= null;
	cd_pessoa_fisica_w		:= null;
	ie_tipo_atendimento_w		:= null;
	cd_convenio_w			:= null;
	cd_setor_atendimento_w		:= null;
	cd_medico_resp_w		:= null;
	nr_seq_classificacao_w		:= null;
	cd_cid_principal_origem_w	:= null;
	cd_categoria_cid_origem_w	:= null;
	ie_tipo_atend_tiss_w		:= null;
	ie_clinica_w			:= null;
	cd_categoria_w			:= null;
	nr_seq_origem_w			:= null;
	cd_plano_convenio_w		:= null;
	ie_tipo_convenio_w              := null;
end;

select max(ie_classif_agenda)
into ie_classif_agenda_con_w
from Agenda_consulta
where nr_atendimento = nr_atendimento_p;

select max(nr_seq_classif_agenda)
into ie_classif_agenda_pac_w
from Agenda_paciente
where nr_atendimento = nr_atendimento_p;


cd_espec_atend_w		:= Obter_Espec_medico_atend(nr_atendimento_p,cd_medico_resp_w,'C');
open C01;
loop
	fetch C01 into	ie_regra_w,
			qt_dia_w,
			qt_hora_w,
			qt_retorno_w,
			ie_dentro_mes_w,
			ie_dentro_dia_w,
			ie_consiste_setor_w,
			ie_mesmo_tipo_atend_w,
			ie_considera_atend_retorno_w,
			ie_atendimento_cancelado_w,
			nr_pos_inicial_w,
			nr_pos_final_w,
			cd_usuario_padrao_w,
			nr_seq_regra_w,
			ie_consiste_classif_w,
			nr_seq_classificacao_regra_w,
			ds_mensagem_w,
			ie_consiste_categ_w,
			ie_consiste_plano_w,
			ie_mesmo_estab_w,
			ie_data_alta_w,
			cd_motivo_alta_w,
			ie_consiste_mot_alt_atend_w,
			IE_ESPEC_MEDICO_RETORNO_w,
			IE_MESMO_TIPO_TISS_w,
			IE_IGNORA_ESPEC_MEDICA_w,
			IE_IGNORA_CLINICA_ATEND_w;
	exit when C01%notfound;
	

end loop;
close C01;

if	(qt_dia_w > 0)  or (qt_hora_w > 0) then

	begin

	if (qt_retorno_w > 0) then

		if (IE_ESPEC_MEDICO_RETORNO_w = 'N') then
			select	1 /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
			into	qt_reg_w
			from	Atendimento_paciente_v3
			where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
			and	((ie_tipo_atendimento	= ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
			and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
			and	cd_convenio		= cd_convenio_w
			and 	(ie_clinica		= ie_clinica_w or ie_clinica is null)
			and	cd_medico_resp		= cd_medico_resp_w
			and	((ie_data_alta_w = 'S' AND NVL(dt_alta,DT_ENTRADA) >= SYSDATE - qt_dia_w) OR (ie_data_alta_w = 'N' AND DT_ENTRADA >= SYSDATE - qt_dia_w))
			and	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	nr_atendimento		<> nr_atendimento_p
			and	((nr_atend_original	is not null) or (ie_considera_atend_retorno_w = 'S'))
			and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
				 (ie_atendimento_cancelado_w = 'S'))
			and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'))
			and	rownum = 1;

			select	count(*) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
			into	qt_reg_w
			from	Atendimento_paciente_v3
			where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
			and	((ie_tipo_atendimento	= ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
			and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
			and	cd_convenio		= cd_convenio_w
			and 	(ie_clinica		= ie_clinica_w or ie_clinica is null)
			and	cd_medico_resp		= cd_medico_resp_w
			and	((ie_data_alta_w = 'S' AND NVL(dt_alta,DT_ENTRADA) >= SYSDATE - qt_dia_w) OR (ie_data_alta_w = 'N' AND DT_ENTRADA >= SYSDATE - qt_dia_w))
			and	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	nr_atendimento		<> nr_atendimento_p
			and	((nr_atend_original	is not null) or (ie_considera_atend_retorno_w = 'S'))
			and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
				 (ie_atendimento_cancelado_w = 'S'))	--  whwestphal  em  19/04/2011 OS 311972
			and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'));

		else
			select	1 /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
			into	qt_reg_w
			from	Atendimento_paciente_v3 a
			where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
			and	((ie_tipo_atendimento	= ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
			and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
			and	cd_convenio		= cd_convenio_w
			and 	(ie_clinica		= ie_clinica_w or ie_clinica is null)
			and	((ie_data_alta_w = 'S' AND NVL(dt_alta,DT_ENTRADA) >= SYSDATE - qt_dia_w) OR (ie_data_alta_w = 'N' AND DT_ENTRADA >= SYSDATE - qt_dia_w))
			and	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	a.nr_atendimento		<> nr_atendimento_p
			and	((nr_atend_original	is not null) or (ie_considera_atend_retorno_w = 'S'))
			and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
				 (ie_atendimento_cancelado_w = 'S'))
			and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'))
			and	(exists (select 1 from atendimento_troca_medico b where b.nr_atendimento = a.nr_atendimento and b.cd_medico_atual = a.cd_medico_resp and b.cd_especialidade = cd_espec_atend_w and rownum = 1) or
				IE_IGNORA_ESPEC_MEDICA_w = 'S')
			and	rownum = 1;

			select	count(*) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
			into	qt_reg_w
			from	Atendimento_paciente_v3 a
			where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
			and	((ie_tipo_atendimento	= ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
			and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
			and	cd_convenio		= cd_convenio_w
			and 	(ie_clinica		= ie_clinica_w or ie_clinica is null)
			and	((ie_data_alta_w = 'S' AND NVL(dt_alta,DT_ENTRADA) >= SYSDATE - qt_dia_w) OR (ie_data_alta_w = 'N' AND DT_ENTRADA >= SYSDATE - qt_dia_w))
			and	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	nr_atendimento		<> nr_atendimento_p
			and	((nr_atend_original	is not null) or (ie_considera_atend_retorno_w = 'S'))
			and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
				 (ie_atendimento_cancelado_w = 'S'))	--  whwestphal  em  19/04/2011 OS 311972
			and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'))
			and	(exists (select 1 from atendimento_troca_medico b where b.nr_atendimento = a.nr_atendimento and b.cd_medico_atual = a.cd_medico_resp and b.cd_especialidade = cd_espec_atend_w and rownum = 1) or
				IE_IGNORA_ESPEC_MEDICA_w = 'S');

		end if;
	else
		qt_reg_w := 0;
	end if;

	exception
	when others then
		qt_reg_w := 0;
	end;

	if	(qt_retorno_w > 0)	and
		(qt_reg_w >= qt_retorno_w)	then
		nr_atend_retorno_p	:= 0;
	elsif	(ie_regra_w = 'M') then
		select	nvl(max(nr_atendimento),0) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
		into 	nr_atend_retorno_p
		from	Atendimento_paciente_v3
		where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
		and 	((ie_tipo_atendimento = ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
		and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
		and 	cd_convenio		  = cd_convenio_w
		and 	ie_clinica		  = ie_clinica_w
		and	 cd_medico_resp	  = cd_medico_resp_w
		and	((nvl(nr_seq_classificacao, nr_seq_classificacao_regra_w)	= nr_seq_classificacao_regra_w or nr_seq_classificacao_regra_w is null) or
			 (ie_consiste_classif_w = 'N')) -- amramos - Nao consistir a classificacao do atend. anterior - OS 304565)
		and	((ie_data_alta_w = 'S' AND NVL(dt_alta,DT_ENTRADA) >= SYSDATE - qt_dia_w) OR (ie_data_alta_w = 'N' AND DT_ENTRADA >= SYSDATE - qt_dia_w))
		and 	cd_pessoa_fisica	  = cd_pessoa_fisica_w
		and 	((nr_atend_original	is null) or (ie_considera_atend_retorno_w = 'S')) -- Dalcastagne em 06/08/2007 OS 64644
		and 	nr_atendimento 	  <> nr_atendimento_p
		and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
			(ie_atendimento_cancelado_w = 'S')) --  whwestphal  em  19/04/2011 OS 311972
		and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'))
		and	((nvl(cd_categoria, cd_categoria_w)	= cd_categoria_w or cd_categoria_w is null
		and 	ie_consiste_categ_w = 'S') or (ie_consiste_categ_w = 'N'))
		and	((nvl(cd_plano_convenio, cd_plano_convenio_w)	= cd_plano_convenio_w or cd_plano_convenio_w is null
		and 	ie_consiste_plano_w = 'S') or (ie_consiste_plano_w = 'N'));

	elsif	(ie_regra_w = 'F') then
	--criado para a OS 261747

		select	nvl(max(nr_atendimento),0) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
		into 	nr_atend_retorno_p
		from	Atendimento_paciente_v3
		where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
		  and	((nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
		  and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
		--  and	((cd_convenio		  = cd_convenio_w) or (cd_convenio is null))
		-- and	((ie_clinica		  = ie_clinica_w) or (ie_clinica is null))
		--  and	((cd_medico_resp	  = cd_medico_resp_w) or (cd_medico_resp is null))
		  and 	((ie_data_alta_w = 'S' AND ((qt_dia_w > 0  AND NVL(dt_alta,DT_ENTRADA) BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
			(qt_dia_w  = 0 AND qt_hora_w > 0  AND NVL(dt_alta,DT_ENTRADA) BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))) OR
			(ie_data_alta_w = 'N' AND ((qt_dia_w > 0  AND dt_entrada BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
			(qt_dia_w  = 0 AND qt_hora_w > 0  AND dt_entrada BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))))
		  and	cd_pessoa_fisica	  = cd_pessoa_fisica_w
		  and	((nr_atend_original	is null) or (ie_considera_atend_retorno_w = 'S'))
		  and 	nr_atendimento 	  <> nr_atendimento_p
		  and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
			(ie_atendimento_cancelado_w = 'S')) --  whwestphal  em  19/04/2011 OS 311972
		  and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'));
	elsif	(ie_regra_w = 'H') then

		select	nvl(max(nr_atendimento),0) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
		into 	nr_atend_retorno_p
		from	Atendimento_paciente_v3
		where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
		and	((nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
		and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
		and	((cd_convenio		  = cd_convenio_w) or (cd_convenio is null))
		-- and	((ie_clinica		  = ie_clinica_w) or (ie_clinica is null))
		--  and	((cd_medico_resp	  = cd_medico_resp_w) or (cd_medico_resp is null))
		and 	((ie_data_alta_w = 'S' AND ((qt_dia_w > 0  AND NVL(dt_alta,DT_ENTRADA) BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
			(qt_dia_w  = 0 AND qt_hora_w > 0  AND NVL(dt_alta,DT_ENTRADA) BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))) OR
			(ie_data_alta_w = 'N' AND ((qt_dia_w > 0  AND dt_entrada BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
			(qt_dia_w  = 0 AND qt_hora_w > 0  AND dt_entrada BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))))
		and	cd_pessoa_fisica	  = cd_pessoa_fisica_w
		and	((nr_atend_original	is null) or (ie_considera_atend_retorno_w = 'S'))
		and 	nr_atendimento 	  <> nr_atendimento_p
		and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
			(ie_atendimento_cancelado_w = 'S')) --  whwestphal  em  19/04/2011 OS 311972
		and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'))
		and	((nvl(cd_plano_convenio, cd_plano_convenio_w)	= cd_plano_convenio_w or cd_plano_convenio_w is null
	and 	ie_consiste_plano_w = 'S') or (ie_consiste_plano_w = 'N'));

	elsif	(ie_regra_w = 'E') then
		open C02;
		loop
		fetch C02 into	nr_atendimento_w,
				cd_medico_w,
				nr_seq_queixa_w,
				cd_cid_principal_w;
		exit when C02%notfound;

			select count(*)
			into	ie_ok_w
			from	medico_especialidade a
			where	a.cd_pessoa_fisica = cd_medico_w
			  and	exists
				(select 1 from medico_especialidade x
				where x.cd_pessoa_fisica = cd_medico_resp_w
				  and x.cd_especialidade = a.cd_especialidade);

			if	(ie_ok_w > 0) then
				nr_atend_retorno_p := nr_atendimento_w;
			else
				select count(*)
				into	ie_ok_w
				from	medico_especialidade a
				where	a.cd_pessoa_fisica = cd_medico_w;

				select count(*)
				into	ie_ok_atual_w
				from	medico_especialidade a
				where	a.cd_pessoa_fisica = cd_medico_resp_w;

				if	((ie_ok_w + ie_ok_atual_w) = 0) then
					nr_atend_retorno_p := nr_atendimento_w;
				end if;
			end if;

		end loop;
		close C02;
	elsif	(ie_regra_w = 'Y') then
		open C02;
		loop
		fetch C02 into	nr_atendimento_w,
				cd_medico_w,
				nr_seq_queixa_w,
				cd_cid_principal_w;
		exit when C02%notfound;

			--cd_espec_atend_w		:= Obter_Espec_medico_atend(nr_atendimento_p,cd_medico_resp_w,'C');
			cd_espec_atend_retorno_w	:= Obter_Espec_medico_atend(nr_atendimento_w,cd_medico_w,'C');

/*			select 	count(*)
			into	ie_ok_w
			from	medico_especialidade a
			where	a.cd_pessoa_fisica = cd_medico_w
			  and	exists
				(select 1 from medico_especialidade x
				where x.cd_pessoa_fisica = cd_medico_resp_w
				  and x.cd_especialidade = a.cd_especialidade); */

			if	/* (ie_ok_w > 0) and */ (cd_espec_atend_w = cd_espec_atend_retorno_w) then
				nr_atend_retorno_p := nr_atendimento_w;
			end if;

		end loop;
		close C02;
	elsif	(ie_regra_w = 'U') then
		open C02;
		loop
		fetch C02 into	nr_atendimento_w,
				cd_medico_w,
				nr_seq_queixa_w,
				cd_cid_principal_w;
		exit when C02%notfound;
			if	(nr_seq_queixa_w is not null) and
				(nr_seq_queixa_origem_w is not null) and
				(nr_seq_queixa_origem_w = nr_seq_queixa_w) then
				nr_atend_retorno_p := nr_atendimento_w;
			end if;
		end loop;
		close C02;
	elsif	(ie_regra_w = 'C') then
		begin
		open C02;
		loop
		fetch C02 into	nr_atendimento_w,
				cd_medico_w,
				nr_seq_queixa_w,
				cd_cid_principal_w;
		exit when C02%notfound;
			if	(cd_cid_principal_w is not null) and
				(cd_cid_principal_origem_w is not null) and
				((cd_cid_principal_origem_w = cd_cid_principal_w) or
				 (obter_se_cid_equivalente(cd_cid_principal_w,cd_cid_principal_origem_w) = 'S')) then
				nr_atend_retorno_p := nr_atendimento_w;
			end if;
		end loop;
		close C02;
		end;
	elsif	(ie_regra_w = 'T') then
		begin
		open C02;
		loop
		fetch C02 into	nr_atendimento_w,
				cd_medico_w,
				nr_seq_queixa_w,
				cd_cid_principal_w;
		exit when C02%notfound;
			select	Obter_Categoria_Cid(cd_cid_principal_w)
			into	cd_categoria_cid_w
			from	dual;
			if	(cd_categoria_cid_w is not null) and
				(cd_categoria_cid_origem_w is not null) and
				(cd_categoria_cid_origem_w = cd_categoria_cid_w) then
				nr_atend_retorno_p := nr_atendimento_w;
			end if;
		end loop;
		close C02;
		end;
	elsif	(ie_regra_w = 'K') then
		begin

		open C02;
		loop
		fetch C02 into	nr_atendimento_w,
				cd_medico_w,
				nr_seq_queixa_w,
				cd_cid_principal_w;
		exit when C02%notfound;
			if	(cd_cid_principal_w is not null) and
				(cd_cid_principal_origem_w is not null) and
				((cd_cid_principal_origem_w = cd_cid_principal_w) or
				 (obter_se_cid_equivalente(cd_cid_principal_w,cd_cid_principal_origem_w) = 'S')) then
				begin
				select	count(*) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
				into	qt_existe_w
				from	Atendimento_paciente_v3
				where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
				  and ((ie_tipo_atendimento	= ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
				  and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
				  and cd_convenio		= cd_convenio_w
				  and ie_clinica		= ie_clinica_w
				  and cd_medico_resp		= cd_medico_resp_w
				  and ((ie_data_alta_w = 'S' AND NVL(dt_alta,DT_ENTRADA) >= SYSDATE - qt_dia_w) OR (ie_data_alta_w = 'N' AND DT_ENTRADA >= SYSDATE - qt_dia_w))
				  and cd_pessoa_fisica		= cd_pessoa_fisica_w
				  and ((nr_atend_original	is null) or (ie_considera_atend_retorno_w = 'S'))
				  and nr_atendimento		<> nr_atendimento_p
				--  and cd_categoria		= cd_categoria_w
				  and nr_atendimento		= nr_atendimento_w
				    and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
					(ie_atendimento_cancelado_w = 'S')) --  whwestphal  em  19/04/2011 OS 311972
				  and ((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'));

				if	(qt_existe_w > 0) then
					nr_atend_retorno_p := nr_atendimento_w;
				end if;
				end;
			end if;
		end loop;
		close C02;
		end;
	elsif	(ie_regra_w = 'R') then
		open C02;
		loop
		fetch C02 into	nr_atendimento_w,
				cd_medico_w,
				nr_seq_queixa_w,
				cd_cid_principal_w;
		exit when C02%notfound;

			select 	count(*)
			into 	ie_ok_w
			from	cirurgia
			where	nr_atendimento = nr_atendimento_w;

			if	(ie_ok_w > 0) then
				nr_atend_retorno_p := nr_atendimento_w;
			end if;
		end loop;
		close C02;
	elsif	(ie_regra_w = 'G') then
	/*OS 298226  tvvieira - nao considerar clinica nem convenio para mesmo medico*/
		select	nvl(max(nr_atendimento),0) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
		into 	nr_atend_retorno_p
		from	Atendimento_paciente_v3
		where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
		  and ((ie_tipo_atendimento = ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
		  and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
		  --and cd_convenio		  = cd_convenio_w
		  --and ie_clinica		  = ie_clinica_w
		  and cd_medico_resp	  = cd_medico_resp_w
		  and ((ie_data_alta_w = 'S' AND NVL(dt_alta,DT_ENTRADA) >= SYSDATE - qt_dia_w) OR (ie_data_alta_w = 'N' AND DT_ENTRADA >= SYSDATE - qt_dia_w))
		  and cd_pessoa_fisica	  = cd_pessoa_fisica_w
		  and ((nr_atend_original	is null) or (ie_considera_atend_retorno_w = 'S')) -- Dalcastagne em 06/08/2007 OS 64644
		  and nr_atendimento 	  <> nr_atendimento_p
		  and (((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
			  (ie_atendimento_cancelado_w = 'S')) --  whwestphal  em  19/04/2011 OS 311972
		  and ((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'));
	elsif	(ie_regra_w = 'D') then

			open C02;
			loop
			fetch C02 into
					nr_atendimento_w,
					cd_medico_w,
					nr_seq_queixa_w,
					cd_cid_principal_w;
			exit when C02%notfound;

				if	(cd_cid_principal_w is not null) and
					(cd_cid_principal_origem_w is not null) and
					((cd_cid_principal_origem_w = cd_cid_principal_w) or
					 (obter_se_cid_equivalente(cd_cid_principal_w,cd_cid_principal_origem_w) = 'S')) then
					begin

					select	count(*) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
					into	qt_existe_w
					from	Atendimento_paciente_v3
					where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
					  and cd_pessoa_fisica		= cd_pessoa_fisica_w
					  and ie_clinica			= ie_clinica_w
					  and ((ie_tipo_atendimento	= ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
					  and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
					  and cd_convenio			= cd_convenio_w
					  and cd_medico_resp		= cd_medico_resp_w
					  and ((ie_data_alta_w = 'S' AND ((qt_dia_w > 0  AND NVL(dt_alta,DT_ENTRADA) BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
					      (qt_dia_w  = 0 AND qt_hora_w > 0  AND NVL(dt_alta,DT_ENTRADA) BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))) OR
					      (ie_data_alta_w = 'N' AND ((qt_dia_w > 0  AND dt_entrada BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
					      (qt_dia_w  = 0 AND qt_hora_w > 0  AND dt_entrada BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))))
					  and ((nr_atend_original	is null) or (ie_considera_atend_retorno_w = 'S'))
					  and nr_atendimento		<> nr_atendimento_p
					  and nr_atendimento		= nr_atendimento_w
						and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
						(ie_atendimento_cancelado_w = 'S'))
					  and ((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'));

					if	(qt_existe_w > 0) then
						nr_atend_retorno_p := nr_atendimento_w;
					end if;

					end;
				end if;
			end loop;
			close C02;

	elsif	(ie_regra_w = 'N') then

		select	nvl(max(nr_atendimento),0) /*+ index(b atepacu_i1) index(a atepaci_pk) index (c.atecaco_pk)*/
		into 	nr_atend_retorno_p
		from	Atendimento_paciente_v3
		where	((ie_mesmo_estab_w = 'N' AND nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w) OR (ie_mesmo_estab_w = 'S'))
		and 	cd_pessoa_fisica	  = cd_pessoa_fisica_w
		and 	cd_convenio		  = cd_convenio_w
		and 	ie_clinica		  = ie_clinica_w
		and 	((ie_tipo_atendimento = ie_tipo_atendimento_w) or (ie_mesmo_tipo_atend_w = 'N'))
		and	((IE_TIPO_ATEND_TISS	= IE_TIPO_ATEND_TISS_w) or (IE_MESMO_TIPO_TISS_w = 'N'))
		and	((nvl(nr_seq_classificacao, nr_seq_classificacao_regra_w)	= nr_seq_classificacao_regra_w or nr_seq_classificacao_regra_w is null) or
			 (ie_consiste_classif_w = 'N')) -- amramos - Nao consistir a classificacao do atend. anterior - OS 304565)
		and 	((ie_data_alta_w = 'S' AND ((qt_dia_w > 0  AND NVL(dt_alta,DT_ENTRADA) BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
			(qt_dia_w  = 0 AND qt_hora_w > 0  AND NVL(dt_alta,DT_ENTRADA) BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))) OR
			(ie_data_alta_w = 'N' AND ((qt_dia_w > 0  AND dt_entrada BETWEEN SYSDATE - qt_dia_w AND SYSDATE) OR
			(qt_dia_w  = 0 AND qt_hora_w > 0  AND dt_entrada BETWEEN SYSDATE - (qt_hora_w/24) AND SYSDATE))))
		and 	((nr_atend_original	is null) or (ie_considera_atend_retorno_w = 'S')) -- Dalcastagne em 06/08/2007 OS 64644
		and 	nr_atendimento 	  <> nr_atendimento_p
		and	(((ie_atendimento_cancelado_w = 'N') and (dt_cancelamento is null))  or
			(ie_atendimento_cancelado_w = 'S')) --  whwestphal  em  19/04/2011 OS 311972
		and	((((cd_motivo_alta = cd_motivo_alta_w) or (cd_motivo_alta_w is null)) and (ie_consiste_mot_alt_atend_w = 'S')) or (ie_consiste_mot_alt_atend_w = 'N'));

	else
		open C02;
		loop
		fetch C02 into	nr_atendimento_w,
				cd_medico_w,
				nr_seq_queixa_w,
				cd_cid_principal_w;
		exit when C02%notfound;


			nr_atend_retorno_p := nr_atendimento_w;
		end loop;
		close C02;

	end if;

	if	(nr_atend_retorno_p > 0) then

		consiste_regra_retorno_proc(nr_seq_regra_w,nr_atend_retorno_p,ds_retorno_proc_w);

		if	(ds_retorno_proc_w = 'N') then		--A regra de retorno possuia regra por procedimento, porem nao encontrou o procedimento no atendimento anterior.
			nr_atend_retorno_p	:= 0;
		end if;

	end if;

	if	(nr_atend_retorno_p	> 0) and
		(ie_dentro_mes_w	= 'S') then
		select	to_date('01/'||to_char(dt_entrada,'mm/yyyy')),
			to_date('01/'||to_char(sysdate,'mm/yyyy'))
		into	dt_entrada_w,
			dt_atual_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atend_retorno_p;


		if	(dt_atual_w > dt_entrada_w) then
			nr_atend_retorno_p	:= 0;
		end if;
	end if;


	if	(nr_atend_retorno_p	> 0) and
		(ie_dentro_dia_w	= 'S') then
		select	to_date(dt_entrada,'dd/mm/yyyy'),
			to_date(sysdate,'dd/mm/yyyy')
		into	dt_entrada_w,
			dt_atual_w
		from	atendimento_paciente
		where	nr_atendimento	= nr_atend_retorno_p;


		if	(dt_atual_w > dt_entrada_w) then
			nr_atend_retorno_p	:= 0;
		end if;
	end if;

	if	(nr_atend_retorno_p > 0)
	and	(ds_mensagem_w is not null) then
		nr_seq_regra_msg_p := nr_seq_regra_w;
	end if;

	 if	(nr_atend_retorno_p	> 0) then
		insert into log_retorno_atendimento
			(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
		values	(sysdate,
				wheb_usuario_pck.get_nm_usuario,
				99999,
				Wheb_mensagem_pck.get_texto(445094 ) || nr_atendimento_p || Wheb_mensagem_pck.get_texto(445095 ) || nr_atend_retorno_p);
		commit;
	end if;

end if;

end  Define_Atendimento_Retorno;
/
