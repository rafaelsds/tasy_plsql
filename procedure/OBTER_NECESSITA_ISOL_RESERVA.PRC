create or replace
procedure obter_necessita_isol_reserva(
			            cd_pessoa_reserva_p	varchar2,
				    ds_mensagem_p	out varchar2) is 

ie_necessita_isol_reserva_w	varchar2(1);
				    
begin

ds_mensagem_p := '';

if	(cd_pessoa_reserva_p is not null) then
	
	select	nvl(max(ie_necessita_isol_reserva),'N')
	into	ie_necessita_isol_reserva_w
	from	ocupacao_unidade_v
	where	cd_paciente_reserva = cd_pessoa_reserva_p;  
	
	if	(ie_necessita_isol_reserva_w = 'S') then
		
		ds_mensagem_p := substr(obter_texto_tasy(317959, wheb_usuario_pck.get_nr_seq_idioma),1,255); 
		                 --'Leito reservado com necessidade de isolamento.';
	end if;

end if;	

commit;

end obter_necessita_isol_reserva;
/