create or replace
procedure pls_cancelar_tit_desdob_reemb
			(	nr_titulo_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

nr_titulo_w		number(10);	

begin
if	(nr_titulo_p is not null) then
	select	max(a.nr_titulo)
	into	nr_titulo_w
	from	titulo_pagar	a
	where	a.nr_titulo_original	= nr_titulo_p;

	pls_cancelar_tit_desdob_reemb(nr_titulo_w, cd_estabelecimento_p, nm_usuario_p);

	if	(nr_titulo_w is not null) then
		cancelar_titulo_pagar(nr_titulo_w, nm_usuario_p, sysdate);
	end if;
end if;

end pls_cancelar_tit_desdob_reemb;
/