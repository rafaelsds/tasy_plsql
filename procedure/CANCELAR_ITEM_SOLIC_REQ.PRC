create or replace
procedure cancelar_item_solic_req(
		nr_requisicao_p	number,
		nr_seq_item_p	number,
		nm_usuario_p	varchar2) is

nr_item_solic_compra_w	solic_compra_item.nr_item_solic_compra%type;
nr_solic_compra_w		solic_compra_item.nr_solic_compra%type;
ds_action_w		varchar2(2000);

cursor c01 is
select	nr_solic_compra,
	nr_item_solic_compra
from	solic_compra_item
where	nr_requisicao = nr_requisicao_p
and 	nr_seq_item_requisicao = nr_seq_item_p
and	dt_baixa is null;

begin

ds_action_w := sys_context('USERENV','ACTION');
dbms_application_info.set_action('CANCELAR_ITEM_SOLIC_REQ');

open c01;
loop
fetch c01 into
	nr_solic_compra_w,
	nr_item_solic_compra_w;
exit when c01%notfound;
	begin
	baixar_solic_compra_item (nr_solic_compra_w, nr_item_solic_compra_w, nm_usuario_p, 2, Wheb_mensagem_pck.get_Texto(298673));
	end;
end loop;
close c01;

dbms_application_info.set_action(ds_action_w);

commit;

end cancelar_item_solic_req;
/
