create or replace
procedure man_an_impacto_inserir_pr (	nr_seq_impacto_p			number,
					nr_seq_product_req_p			varchar2,
					nm_usuario_p				varchar2,
					nr_seq_imp_pr_p			out	number ) is

nr_seq_imp_pr_w			man_ordem_serv_imp_pr.nr_sequencia%type;

begin

	if	(nr_seq_impacto_p is not null) and
		(nr_seq_product_req_p is not null) then

		select	man_ordem_serv_imp_pr_seq.nextval
		into	nr_seq_imp_pr_w
		from	dual;

		nr_seq_imp_pr_p := nr_seq_imp_pr_w;

		insert into man_ordem_serv_imp_pr (
			nr_sequencia,
			nr_seq_impacto,
			nr_product_requirement,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec
		) values (
			nr_seq_imp_pr_w,
			nr_seq_impacto_p,
			nr_seq_product_req_p,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate
		);

	end if;

	commit;

end man_an_impacto_inserir_pr;
/
