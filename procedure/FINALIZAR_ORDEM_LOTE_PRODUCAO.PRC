create or replace
procedure finalizar_ordem_lote_producao(	nr_seq_horario_p	number,
						nr_seq_area_prep_p	number,	
						nr_seq_lp_individual_p	number,
						nm_usuario_p		varchar2) is 

nr_seq_ordem_w			number(10,0);
nr_seq_processo_w		number(10,0);
dt_inicio_preparo_w		date;
dt_preparo_w			date;
nm_usuario_manipulador_w	varchar2(15);
nm_usuario_farmaceutico_w	varchar2(15);

						
begin


select	max(nr_seq_etiqueta)
into	nr_seq_ordem_w
from	prescr_mat_hor
where	nr_sequencia	=	nr_seq_horario_p
and		nr_seq_superior is null
and		Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

if	(nr_seq_ordem_w is null) then
	select	max(nr_seq_etiqueta)
	into	nr_seq_ordem_w
	from	prescr_mat_hor
	where	nr_sequencia	=	nr_seq_horario_p	
	and		Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
end if;

select	max(dt_preparo),
	max(dt_inicio_preparo),
	max(obter_usuario_pf(cd_farmaceutico)),
	max(obter_usuario_pf(cd_manipulador))   
into	dt_preparo_w,
	dt_inicio_preparo_w,
	nm_usuario_farmaceutico_w,
	nm_usuario_manipulador_w
from	lp_individual
where	nr_sequencia	=	nr_seq_lp_individual_p;

if	(nr_seq_ordem_w > 0) then
	update	adep_processo_frac
	set	dt_preparo		= dt_inicio_preparo_w,
		nm_usuario_preparo	= nm_usuario_manipulador_w,
		dt_fim_preparo		= dt_preparo_w,
		nm_usuario_fim_preparo	= nm_usuario_farmaceutico_w,
		dt_atualizacao		= sysdate,			
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_ordem_w;	

	if	(nvl(nr_seq_area_prep_p,0) > 0) then
		select	nvl(max(nr_seq_processo),0)
		into	nr_seq_processo_w
		from	adep_processo_frac
		where	nr_sequencia = nr_seq_ordem_w;
		
		update	adep_processo_area a
		set	a.dt_preparo 		= sysdate,
			a.nm_usuario_preparo	= nm_usuario_p
		where	a.nr_seq_processo	= nr_seq_processo_w
		and	a.nr_seq_area_prep	= nr_seq_area_prep_p
		and	not exists (
				select	1
				from	adep_processo_frac x,
					prescr_mat_hor y
				where	y.nr_seq_etiqueta	= x.nr_sequencia
				and	y.nr_seq_processo	= x.nr_seq_processo
				and	x.nr_seq_processo	= nr_seq_processo_w
				and	y.nr_seq_area_prep	= nr_seq_area_prep_p
				and	x.dt_fim_preparo	is null
				and	y.dt_suspensao		is null
				and	x.nr_sequencia		<> nr_seq_ordem_w);
	end if;
end if;

commit;

end finalizar_ordem_lote_producao;
/