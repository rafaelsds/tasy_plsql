create or replace procedure nicu_get_birthday_message(	nr_seq_patient_p 		number,
							ds_title_p	out nocopy	varchar2,
							ds_subtitle_p 	out nocopy 	varchar2) is

qt_weeks_w	        number(2) := 0;
nm_patient_w		varchar2(255) := '';
nr_seq_person_name_w	person_name.nr_sequencia%type;

begin

	select 	nvl(max(ceil((sysdate - dt_birthdate) / 7)), -1),
			max(nr_seq_person_name)
	into	qt_weeks_w,
			nr_seq_person_name_w
	from	nicu_patient
	where	nr_sequencia = nr_seq_patient_p;

	if (qt_weeks_w > -1) then

		select	max(ds_given_name)
		into	nm_patient_w
		from	person_name
		where	nr_sequencia = nr_seq_person_name_w;

		ds_title_p := obter_desc_exp_idioma(1063472, wheb_usuario_pck.get_nr_seq_idioma, 'NM_PACIENTE_P=' || nm_patient_w);

		if (qt_weeks_w = 1) then
			ds_subtitle_p := obter_desc_exp_idioma(1063476, wheb_usuario_pck.get_nr_seq_idioma);
		else
			ds_subtitle_p := obter_desc_exp_idioma(1063474, wheb_usuario_pck.get_nr_seq_idioma, 'QT_SEMANAS_P=' || qt_weeks_w);
		end if;

	end if;

end nicu_get_birthday_message;
/
