CREATE OR REPLACE
PROCEDURE dis_gerar_pedido_venc(	nr_pedido_p		Number,
				nm_usuario_p		Varchar2) is

cd_condicao_pagamento_w	Number(10);
cd_estabelecimento_w	Number(4);
dt_prevista_entrega_w		Date;
dt_geracao_w			date;
dt_pedido_w				date;
qt_vencimentos_w		Number(5);
ds_vencimentos_w		Varchar2(2000);
dt_vencimento_w		Date;
nr_vencimento_w		Number(5);
vl_vencimento_w		Number(15,2)	:= 0;
vl_total_pedido_w		Number(13,2);
tx_fracao_parcela_w	Number(15,4);
qt_venc_w		Number(5);
ie_forma_pagamento_w	Number(2);
vl_mercadoria_w		number(15,2);
vl_base_venc_w		number(15,2);
vl_desconto_w		number(15,2);
vl_desconto_venc_w	number(15,2);
vl_desc_financ_w		number(15,2);
vl_desc_fin_venc_w	number(15,2);
vl_total_vencimento_w	number(13,2);
dt_ultimo_vencimento_w	date;
vl_total_pedido_ww		number(13,2);
vl_diferenca_w		number(13,2);
ie_data_base_venc_ordem_w	varchar2(1);
qt_registro_w			number(3);

cursor C01 IS
select	tx_fracao_parcela
from	parcela
where	cd_condicao_pagamento = cd_condicao_pagamento_w;

BEGIN
dis_atualizar_valores_pedido(nr_pedido_p, nm_usuario_p);

select	cd_estabelecimento,
	cd_cond_pagto,
	vl_total_pedido,
	dt_entrega,
	dt_pedido
into	cd_estabelecimento_w,
	cd_condicao_pagamento_w,
	vl_total_pedido_w,
	dt_prevista_entrega_w,
	dt_pedido_w
from	dis_pedido
where	nr_sequencia = nr_pedido_p;

select	ie_forma_pagamento
into	ie_forma_pagamento_w
from	condicao_pagamento
where	cd_condicao_pagamento	= cd_condicao_pagamento_w;

if	(ie_forma_pagamento_w = 10) then
	begin
	/*se for condi��o de pagamento com tipo (conforme vencimentos)*/
	select	count(*)
	into	qt_registro_w
	from	dis_pedido_venc
	where	nr_seq_pedido	= nr_pedido_p;
	
	if	(qt_registro_w > 0) then
		begin
		select	nvl(sum(vl_vencimento),0)
		into	vl_vencimento_w
		from	dis_pedido_venc
		where	nr_seq_pedido	= nr_pedido_p;

		select	round(nvl(sum(vl_total_pedido),0),2)
		into	vl_total_pedido_w
		from	dis_pedido
		where	nr_sequencia = nr_pedido_p;

		update	dis_pedido_venc
		set	vl_vencimento 		= vl_vencimento + dividir((vl_total_pedido_w - vl_vencimento_w), qt_registro_w)
		where	nr_seq_pedido	= nr_pedido_p;
		end;
	end if;
	end;
else
	begin
	delete	from dis_pedido_venc
	where	nr_seq_pedido = nr_pedido_p;
	
	select	nvl(max(ie_data_base_venc_ordem),'E')
	into	ie_data_base_venc_ordem_w
	from	parametro_compras
	where	cd_estabelecimento = cd_estabelecimento_w;

	dt_geracao_w	:= dt_prevista_entrega_w;
	if	(ie_data_base_venc_ordem_w = 'A') then
		 dt_geracao_w	:= sysdate;
	elsif	(ie_data_base_venc_ordem_w = 'O') then
		 dt_geracao_w	:= dt_pedido_w;
	end if;
		
	if	(ie_forma_pagamento_w = 1) then	/* Se for a vista*/
		begin
		insert into dis_pedido_venc(
					nr_seq_pedido,
					nr_sequencia,
					dt_vencimento, 
					vl_vencimento,
					dt_atualizacao,
					nm_usuario)
				values(nr_pedido_p,
					dis_pedido_venc_seq.nextval,
					dt_geracao_w,
					vl_vencimento_w,
					sysdate,
					nm_usuario_p);
		end;
	else
		begin
		Calcular_Vencimento(
			cd_estabelecimento_w,
			cd_condicao_pagamento_w,
			dt_geracao_w,
			qt_vencimentos_w,
			ds_vencimentos_w);	
		OPEN C01;
		LOOP
		FETCH C01 INTO
			tx_fracao_parcela_w;
		exit when c01%notfound;
			begin
			
			dt_vencimento_w		:= To_Date(substr(ds_vencimentos_w,1,10),'dd/mm/yyyy');
			ds_vencimentos_w	:= substr(ds_vencimentos_w,12, length(ds_vencimentos_w));		
			vl_vencimento_w		:= dividir((vl_total_pedido_w * tx_fracao_parcela_w),100);
			
			insert into dis_pedido_venc(
					nr_seq_pedido,
					nr_sequencia,
					dt_vencimento, 
					vl_vencimento,
					dt_atualizacao,
					nm_usuario)
				values(nr_pedido_p,
					dis_pedido_venc_seq.nextval,
					dt_vencimento_w,
					vl_vencimento_w,
					sysdate,
					nm_usuario_p);
			commit;
			end;
		END LOOP;
		close c01;
		end;
	end if;
	/*Tratamento abaixo para jogar a diferen�a para o �ltimo vencimento*/
	select	nvl(sum(vl_vencimento),0),
		max(dt_vencimento)
	into	vl_total_vencimento_w,
		dt_ultimo_vencimento_w
	from	dis_pedido_venc
	where	nr_seq_pedido = nr_pedido_p;
	
	select	vl_total_pedido
	into	vl_total_pedido_ww
	from	dis_pedido
	where	nr_sequencia = nr_pedido_p;
	
	if	(vl_total_vencimento_w <> vl_total_pedido_ww) then
		begin
		vl_diferenca_w := vl_total_vencimento_w - vl_total_pedido_ww;
		
		update	dis_pedido_venc
		set	vl_vencimento	= vl_vencimento - vl_diferenca_w
		where	nr_seq_pedido = nr_pedido_p
		and	dt_vencimento	= dt_ultimo_vencimento_w;
		end;
	end if;
	end;
end if;

END dis_gerar_pedido_venc;
/