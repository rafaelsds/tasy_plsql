create or replace
procedure gerar_retorno_glosa_conta(	nr_seq_ret_item_p     		number,
					nm_usuario_p         	varchar2,
					ie_opcao_p		varchar2,
					ie_agrupado_p		varchar2,
					ie_tipo_valor_p		number,
					ie_procedimento_p		varchar2,
					ie_honorario_p		varchar2,
					ie_taxa_p		varchar2,
					ie_diaria_p		varchar2,
					ie_material_p		varchar2,
					ie_medicamento_p		varchar2,
					ie_extra_p		varchar2,
					ie_complexidade_p		varchar2,
					cd_motivo_glosa_p		number,
					ds_observacao_p		varchar2,
					ie_proc_partic_p	varchar2) is

/*	ie_opcao_p

G	proporcional � glosa
P	proporcional ao valor pago

*/

nr_sequencia_w			number(10);
cd_procedimento_w		number(15);
cd_material_w			number(6);
ie_origem_proced_w		number(10);
vl_procedimento_w		number(15,2);
vl_saldo_procedimento_w		number(15,2);
vl_material_w			number(15,2);
vl_guia_w			number(15,2);
qt_glosa_w			number(9,3);
nr_interno_conta_w		number(10);
qt_procedimento_w		number(9,3);
qt_material_w			number(9,3);
vl_glosa_w			number(15,2);
vl_glosado_w			number(15,2);
vl_glosa_final_w		number(15,2) := 0;
pr_glosado_w			number(6,4);
cd_setor_atendimento_w		number(5);
ie_emite_conta_w		varchar2(3);
nr_seq_glosa_w			number(10);
cd_item_convenio_w		varchar2(20);
cd_autorizacao_w		varchar(20);
ie_atualizar_motivo_glosa_w	varchar2(255);
ie_possui_saldo_w		varchar2(255);
ds_observacao_w			varchar2(4000) := null;
vl_saldo_material_w		number(15,2);
nr_seq_partic_w			number(15);
cd_pessoa_fisica_w		varchar2(10);
cd_perfil_w			perfil.cd_perfil%type 			:= obter_perfil_ativo;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type	:= wheb_usuario_pck.get_cd_estabelecimento;
Cursor c02 is
select	a.nr_sequencia,
	a.cd_material,
	a.vl_material,
	a.qt_material,
	a.cd_material_convenio,
	a.cd_setor_atendimento,
	obter_saldo_conpaci_item(null,a.nr_sequencia) vl_saldo_material
from	material_atend_paciente a
where	a.nr_interno_conta = nr_interno_conta_w
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	((obter_classif_material_proced(a.cd_material, null, null) in (1,5,7,10)      and ie_material_p 	= 'S')
or 	(obter_classif_material_proced(a.cd_material, null, null)  in (0,2,3,4,6,8,9) and ie_medicamento_p	= 'S')
or 	(obter_classif_material_proced(a.cd_material, null, null)  in (5)             and ie_extra_p 	= 'S'))
and	a.cd_motivo_exc_conta is null
and	ie_complexidade_p is null;

Cursor c01 is
select	a.nr_sequencia,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.vl_procedimento,
	a.qt_procedimento,
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia, null) vl_saldo_proced,
	a.cd_setor_atendimento,
	null nr_seq_partic,
	null cd_medico_executor
from	procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '3' and ie_diaria_p = 'S'))
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	a.cd_motivo_exc_conta is null
and	nvl(ie_proc_partic_p,'N') = 'N'
union all
select	a.nr_sequencia,
	a.cd_procedimento,
	a.ie_origem_proced,
	decode(nvl(a.cd_medico_executor, 'X'), 'X', a.vl_procedimento, a.vl_medico + vl_custo_operacional + vl_materiais)  vl_procedimento,
	a.qt_procedimento,
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia, null, null, ie_proc_partic_p) vl_saldo_proced,
	a.cd_setor_atendimento,
	null nr_seq_partic,
	a.cd_medico_executor
from	procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '3' and ie_diaria_p = 'S'))
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	a.cd_motivo_exc_conta is null
and	nvl(ie_proc_partic_p,'N') = 'S'
union all
select	a.nr_sequencia,
	a.cd_procedimento,
	a.ie_origem_proced,
	c.vl_participante,
	a.qt_procedimento,
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia, null, c.nr_seq_partic) vl_saldo_proced,
	a.cd_setor_atendimento,
	c.nr_seq_partic nr_seq_partic,
	c.cd_pessoa_fisica
from	procedimento b,
	procedimento_paciente a,
	procedimento_participante c
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.nr_sequencia		= c.nr_sequencia
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '3' and ie_diaria_p = 'S'))
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	a.cd_motivo_exc_conta is null
and	nvl(ie_proc_partic_p,'N') = 'S'
union all
select	a.nr_sequencia,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.vl_procedimento vl_procedimento,
	a.qt_procedimento,
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia, null, null, ie_proc_partic_p) vl_saldo_proced,
	a.cd_setor_atendimento,
	null nr_seq_partic,
	a.cd_medico_executor
from	procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '3' and ie_diaria_p = 'S'))
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	a.cd_motivo_exc_conta is null
and	nvl(ie_proc_partic_p,'N') = 'T'
and	nvl(obter_participantes_proc(a.nr_sequencia),'X') = 'X' --aqui determina que n�o h� participantes para o procedimento
union all
select	a.nr_sequencia,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.vl_medico vl_procedimento,
	a.qt_procedimento,
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia, null, null, ie_proc_partic_p) vl_saldo_proced,
	a.cd_setor_atendimento,
	null nr_seq_partic,
	a.cd_medico_executor
from	procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '3' and ie_diaria_p = 'S'))
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	a.cd_motivo_exc_conta is null
and	nvl(ie_proc_partic_p,'N') = 'T'
and	nvl(obter_participantes_proc(a.nr_sequencia),'X') <> 'X' --aqui determina que h� participantes para o procedimento
union all
select	a.nr_sequencia,
	a.cd_procedimento,
	a.ie_origem_proced,
	c.vl_participante,
	a.qt_procedimento,
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia, null, c.nr_seq_partic) vl_saldo_proced,
	a.cd_setor_atendimento,
	c.nr_seq_partic nr_seq_partic,
	c.cd_pessoa_fisica
from	procedimento b,
	procedimento_paciente a,
	procedimento_participante c
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.nr_sequencia		= c.nr_sequencia
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '3' and ie_diaria_p = 'S'))
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	a.cd_motivo_exc_conta is null
and	nvl(ie_proc_partic_p,'N') = 'T' 
union all
select	a.nr_sequencia,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.vl_procedimento,
	a.qt_procedimento,
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia,null) vl_saldo_proced,
	a.cd_setor_atendimento,
	null nr_seq_partic,
	null cd_medico_executor
from	procedimento b,
	sus_estrutura_procedimento_v d,
	sus_procedimento f,
	procedimento_paciente c,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	a.nr_seq_proc_princ	= c.nr_sequencia
and	f.ie_origem_proced	= b.ie_origem_proced
and	f.cd_procedimento	= b.cd_procedimento
and	f.cd_procedimento 	= d.cd_procedimento
and	f.ie_origem_proced 	= d.ie_origem_proced
and	d.cd_forma_organizacao 	= 80201
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '3' and ie_diaria_p = 'S'))
and	(sus_obter_complexidade_proced(c.cd_procedimento, c.ie_origem_proced, 'C') 	= ie_complexidade_p)
and	((a.nr_seq_proc_princ is not null and Sus_Validar_Regra(13, a.cd_procedimento, a.ie_origem_proced,a.dt_procedimento) > 0) and (ie_complexidade_p is not null))
and	a.cd_motivo_exc_conta is null;

Cursor c03 is
select	a.cd_material,
	a.cd_setor_atendimento,
	ie_emite_conta,
	sum(nvl(a.vl_material,0)),
	sum(nvl(a.qt_material,0)),
	a.cd_material_convenio
from	material_atend_paciente a
where	a.nr_interno_conta = nr_interno_conta_w
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and	a.cd_motivo_exc_conta is null
and	ie_complexidade_p is null
and 	((obter_classif_material_proced(a.cd_material, null, null) in (1,5,7,10)      and ie_material_p	= 'S')
or 	(obter_classif_material_proced(a.cd_material, null, null)  in (0,2,3,4,6,8,9) and ie_medicamento_p	= 'S')
or 	(obter_classif_material_proced(a.cd_material, null, null)  in (5)             and ie_extra_p 	= 'S'))
having	sum(nvl(a.vl_material,0)) <> 0
group by
	a.cd_material,
	a.cd_setor_atendimento,
	ie_emite_conta,
	a.cd_material_convenio;

Cursor c04 is
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_setor_atendimento,
	a.ie_emite_conta,
	sum(nvl(a.vl_procedimento,0)),
	sum(nvl(a.qt_procedimento,0)),
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia,null) vl_saldo_proced
from	procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '3' and ie_diaria_p = 'S'))
and	a.cd_motivo_exc_conta is null
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
having	sum(nvl(a.vl_procedimento,0)) <> 0
group by
	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_setor_atendimento,
	a.ie_emite_conta,
	a.cd_procedimento_convenio,
	a.nr_interno_conta,
	a.nr_sequencia
union all
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_setor_atendimento,
	a.ie_emite_conta,
	sum(nvl(a.vl_procedimento,0)),
	sum(nvl(a.qt_procedimento,0)),
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia,null) vl_saldo_proced
from	procedimento b,
	sus_estrutura_procedimento_v d,
	sus_procedimento f,
	procedimento_paciente c,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	a.nr_seq_proc_princ	= c.nr_sequencia
and	f.ie_origem_proced	= b.ie_origem_proced
and	f.cd_procedimento	= b.cd_procedimento
and	f.cd_procedimento 	= d.cd_procedimento
and	f.ie_origem_proced 	= d.ie_origem_proced
and	d.cd_forma_organizacao 	= 80201
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '3' and ie_diaria_p = 'S'))
and	a.cd_motivo_exc_conta is null
and	(sus_obter_complexidade_proced(c.cd_procedimento, c.ie_origem_proced, 'C') 	= ie_complexidade_p)
and	((a.nr_seq_proc_princ is not null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) and (ie_complexidade_p is not null))
having	sum(nvl(a.vl_procedimento,0)) <> 0
group by
	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_setor_atendimento,
	a.ie_emite_conta,
	a.cd_procedimento_convenio,
	a.nr_interno_conta,
	a.nr_sequencia;

Cursor c05 is
select	a.cd_material,
	sum(nvl(a.vl_material,0)),
	sum(nvl(a.qt_material,0)),
	a.cd_material_convenio
from	material_atend_paciente a
where	a.nr_interno_conta = nr_interno_conta_w
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and	a.cd_motivo_exc_conta is null
and	ie_complexidade_p is null
and 	((obter_classif_material_proced(a.cd_material, null, null) in (1,5,7,10)      and ie_material_p	= 'S')
or 	(obter_classif_material_proced(a.cd_material, null, null)  in (0,2,3,4,6,8,9) and ie_medicamento_p	= 'S')
or 	(obter_classif_material_proced(a.cd_material, null, null)  in (5) 	      and ie_extra_p 	= 'S'))
having	sum(nvl(a.vl_material,0)) <> 0
group by
	a.cd_material,
	a.cd_material_convenio;
		

Cursor c06 is
select	a.cd_procedimento,
	a.ie_origem_proced,
	sum(nvl(a.vl_procedimento,0)),
	sum(nvl(a.qt_procedimento,0)),
	a.cd_procedimento_convenio,
	sum(obter_saldo_conpaci_item(a.nr_sequencia,null)) vl_saldo_proced,
	null,
	null,
	null
from	procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '3' and ie_diaria_p = 'S'))
and	a.cd_motivo_exc_conta is null
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	nvl(ie_proc_partic_p,'N') = 'N'
having	sum(nvl(a.vl_procedimento,0)) <> 0
group by
	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_procedimento_convenio,
	a.nr_interno_conta
	--a.nr_sequencia
union all
select	a.cd_procedimento,
	a.ie_origem_proced,
	decode(nvl(a.cd_medico_executor, 'X'), 'X', sum(a.vl_procedimento), sum(a.vl_medico)),	
	sum(nvl(a.qt_procedimento,0)),
	a.cd_procedimento_convenio,
	sum(obter_saldo_conpaci_item(a.nr_sequencia,null)) vl_saldo_proced,
	null nr_seq_partic,
	a.cd_medico_executor,
	a.nr_sequencia
from	procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '3' and ie_diaria_p = 'S'))
and	a.cd_motivo_exc_conta is null
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	nvl(ie_proc_partic_p,'N') = 'S'
and	exists (select 1 from procedimento_participante x where x.nr_sequencia = a.nr_sequencia)
having	decode(nvl(a.cd_medico_executor, 'X'), 'X', sum(a.vl_procedimento), sum(a.vl_medico)) <> 0
group by
	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_procedimento_convenio,
	a.nr_interno_conta,
	a.nr_sequencia,
	a.cd_medico_executor
union all
select	a.cd_procedimento,
	a.ie_origem_proced,
	sum(a.vl_procedimento),
	sum(nvl(a.qt_procedimento,0)),
	a.cd_procedimento_convenio,
	sum(obter_saldo_conpaci_item(a.nr_sequencia,null)) vl_saldo_proced,
	null nr_seq_partic,
	a.cd_medico_executor,
	a.nr_sequencia
from	procedimento b,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '3' and ie_diaria_p = 'S'))
and	a.cd_motivo_exc_conta is null
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	nvl(ie_proc_partic_p,'N') = 'S'
and	not exists (select 1 from procedimento_participante x where x.nr_sequencia = a.nr_sequencia)
having	sum(nvl(a.vl_procedimento,0)) <> 0
group by
	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_procedimento_convenio,
	a.nr_interno_conta,
	a.nr_sequencia,
	a.cd_medico_executor
union all
select	a.cd_procedimento,
	a.ie_origem_proced,
	nvl(c.vl_participante,0),
	nvl(a.qt_procedimento,0),
	a.cd_procedimento_convenio,
	c.vl_participante - NVL(obter_glosa_item_procmat(null, NULL, 2, c.nr_seq_partic),0) vl_saldo_proced,
	c.nr_seq_partic nr_seq_partic,
	c.cd_pessoa_fisica,
	a.nr_sequencia
from	procedimento b,
	procedimento_paciente a,
	procedimento_participante c
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.nr_sequencia		= c.nr_sequencia
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '3' and ie_diaria_p = 'S'))
and	((sus_obter_complexidade_proced(a.cd_procedimento, a.ie_origem_proced, 'C') 	= ie_complexidade_p) or (ie_complexidade_p is null))
and	((a.nr_seq_proc_princ is null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) or (ie_complexidade_p is null))
and	a.cd_motivo_exc_conta is null
and	nvl(ie_proc_partic_p,'N') = 'S'
union all
select	a.cd_procedimento,
	a.ie_origem_proced,
	sum(nvl(a.vl_procedimento,0)),
	sum(nvl(a.qt_procedimento,0)),
	a.cd_procedimento_convenio,
	obter_saldo_conpaci_item(a.nr_sequencia,null) vl_saldo_proced,
	null,
	null,
	null
from	procedimento b,
	sus_estrutura_procedimento_v d,
	sus_procedimento f,
	procedimento_paciente c,
	procedimento_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_w
and	a.cd_procedimento  	= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	a.nr_seq_proc_princ	= c.nr_sequencia
and	f.ie_origem_proced	= b.ie_origem_proced
and	f.cd_procedimento	= b.cd_procedimento
and	f.cd_procedimento 	= d.cd_procedimento
and	f.ie_origem_proced 	= d.ie_origem_proced
and	d.cd_forma_organizacao 	= 80201
and	nvl(a.nr_doc_convenio, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada')
and 	(((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced)	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'N') and ie_procedimento_p = 'S')
or 	((obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '1' and obter_se_pasta_honorario(a.ie_responsavel_credito) = 'S') and ie_honorario_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '2' and ie_taxa_p = 'S')
or 	(obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) 	= '3' and ie_diaria_p = 'S'))
and	a.cd_motivo_exc_conta is null
and	(sus_obter_complexidade_proced(c.cd_procedimento, c.ie_origem_proced, 'C') 	= ie_complexidade_p)
and	((a.nr_seq_proc_princ is not null and Sus_Validar_Regra(13,a.cd_procedimento,a.ie_origem_proced,a.dt_procedimento) = 0) and (ie_complexidade_p is not null))
having	sum(nvl(a.vl_procedimento,0)) <> 0
group by
	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_procedimento_convenio,
	a.nr_interno_conta,
	a.nr_sequencia;

begin

ds_observacao_w	:= substr(ds_observacao_p,1,3999);

obter_param_usuario(27,92,cd_perfil_w,nm_usuario_p,cd_estabelecimento_w,ie_atualizar_motivo_glosa_w);
obter_param_usuario(27,121,cd_perfil_w,nm_usuario_p,cd_estabelecimento_w,ie_possui_saldo_w);

/*select	nr_interno_conta,
	dividir_sem_round(vl_glosado + vl_amenor, (vl_pago + vl_glosado + vl_amenor)) pr_glosado,
	(nvl(vl_glosado,0) + nvl(vl_amenor,0))
into	nr_interno_conta_w,
	pr_glosado_w,
	vl_glosado_w
from	convenio_retorno_item
where	nr_sequencia = nr_seq_ret_item_p;	*/

select	a.nr_interno_conta,
	a.cd_autorizacao
into	nr_interno_conta_w,
	cd_autorizacao_w
from	convenio_retorno_item a
where	a.nr_sequencia		= nr_seq_ret_item_p;

/*lhalves OS 491455 em 05/09/2012 - Em alguns clientes na conta_paciente_guia fica como 'N?o' devido a configura��o do banco.
Incluido este tratamento para n�o dar problema nos cursores abaixo que fazem nvl do cd_autorizacao_w*/
if	(cd_autorizacao_w = 'N?o Informada') then
	cd_autorizacao_w := 'N�o Informada';
end if;

select	nvl(sum(vl_guia),0)
into	vl_guia_w
from	conta_paciente_guia
where	nr_interno_conta = nr_interno_conta_w
and	nvl(cd_autorizacao, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada');

if	(ie_opcao_p	= 'G') then

	select	dividir_sem_round(vl_glosado + vl_amenor, (vl_guia_w)) pr_glosado,
		(nvl(vl_glosado,0) + nvl(vl_amenor,0))
	into	pr_glosado_w,
		vl_glosado_w
	from	convenio_retorno_item
	where	nr_sequencia = nr_seq_ret_item_p
	and	nvl(cd_autorizacao, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada');

elsif	(ie_opcao_p	= 'P') then

	select	dividir_sem_round(nvl(vl_pago,0), (vl_guia_w)) pr_glosado,
		nvl(vl_pago,0)
	into	pr_glosado_w,
		vl_glosado_w
	from	convenio_retorno_item
	where	nr_sequencia				= nr_seq_ret_item_p
	and	nvl(cd_autorizacao, 'N�o Informada')	= nvl(cd_autorizacao_w, 'N�o Informada');

end if;

if	(ie_agrupado_p = 'S') then
	begin
	open c03;
	loop
	fetch c03 into
		cd_material_w,
		cd_setor_atendimento_w,
		ie_emite_conta_w,
		vl_material_w,
		qt_material_w,
		cd_item_convenio_w;
	exit when c03%notfound;

		vl_glosa_w	:= 0;
		if	(ie_opcao_p	in ('G','P')) then
			vl_glosa_w	:= nvl(vl_material_w,0) * nvl(pr_glosado_w,0);
		end if;

		vl_glosa_final_w := vl_glosa_final_w + vl_glosa_w;

		select	convenio_retorno_glosa_seq.nextval
		into	nr_seq_glosa_w
		from	dual;

		insert	into convenio_retorno_glosa
			(nr_sequencia,
			nr_seq_ret_item,
			vl_glosa,
			dt_atualizacao,
			nm_usuario,
			cd_material,
			cd_setor_atendimento,
			ie_emite_conta,
			ie_atualizacao,
			qt_glosa,
			qt_cobrada,
			vl_cobrado,
			cd_item_convenio,
			cd_motivo_glosa,
			ds_observacao)
		values	(nr_seq_glosa_w,
			nr_seq_ret_item_p,
			vl_glosa_w,
			sysdate,
			nm_usuario_p,
			cd_material_w,
			cd_setor_atendimento_w,
			ie_emite_conta_w,
			'N',
			decode(ie_tipo_valor_p, 0, 0, 1, qt_material_w, 0),
			qt_material_w,
			vl_material_w,
			cd_item_convenio_w,
			decode(ie_atualizar_motivo_glosa_w,'S',cd_motivo_glosa_p,null),
			ds_observacao_w);
	end loop;
	close c03;
	open c04;
	loop
	fetch c04 into
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_setor_atendimento_w,
		ie_emite_conta_w,
		vl_procedimento_w,
		qt_procedimento_w,
		cd_item_convenio_w,
		vl_saldo_procedimento_w;
	exit when c04%notfound;

		vl_glosa_w	:= 0;
		if	(ie_opcao_p	in ('G','P')) then
			vl_glosa_w	:= nvl(vl_procedimento_w,0) * nvl(pr_glosado_w,0);
		end if;

		vl_glosa_final_w := vl_glosa_final_w + vl_glosa_w;

		select	convenio_retorno_glosa_seq.nextval
		into	nr_seq_glosa_w
		from	dual;

		if	(nvl(ie_possui_saldo_w,'N') = 'N') or ((ie_possui_saldo_w = 'S') and (vl_saldo_procedimento_w > 0)) then
			insert	into convenio_retorno_glosa
				(nr_sequencia,
				nr_seq_ret_item,
				vl_glosa,
				dt_atualizacao,
				nm_usuario,
				cd_procedimento,
				ie_origem_proced,
				cd_setor_atendimento,
				ie_emite_conta,
				ie_atualizacao,
				qt_glosa,
				qt_cobrada,
				vl_cobrado,
				cd_item_convenio,
				cd_motivo_glosa,
				ds_observacao)
			values	(nr_seq_glosa_w,
				nr_seq_ret_item_p,
				vl_glosa_w,
				sysdate,
				nm_usuario_p,
				cd_procedimento_w,
				ie_origem_proced_W,
				cd_setor_atendimento_w,
				ie_emite_conta_w,
				'N',
				decode(ie_tipo_valor_p, 0, 0, 1, qt_procedimento_w, 0),
				qt_procedimento_w,
				vl_procedimento_w,
				cd_item_convenio_w,
				decode(ie_atualizar_motivo_glosa_w,'S',cd_motivo_glosa_p,null),
				ds_observacao_w);
		end if;


	end loop;
	close c04;
	end;
elsif	(ie_agrupado_p = 'C') then
	begin
	open c05;
	loop
	fetch c05 into
		cd_material_w,
		vl_material_w,
		qt_material_w,
		cd_item_convenio_w;
	exit when c05%notfound;

		vl_glosa_w	:= 0;
		if	(ie_opcao_p	in ('G','P')) then
			vl_glosa_w	:= nvl(vl_material_w,0) * nvl(pr_glosado_w,0);
		end if;

		vl_glosa_final_w := vl_glosa_final_w + vl_glosa_w;

		select	convenio_retorno_glosa_seq.nextval
		into	nr_seq_glosa_w
		from	dual;

		insert	into convenio_retorno_glosa
			(nr_sequencia,
			nr_seq_ret_item,
			vl_glosa,
			dt_atualizacao,
			nm_usuario,
			cd_material,
			cd_setor_atendimento,
			ie_emite_conta,
			ie_atualizacao,
			qt_glosa,
			qt_cobrada,
			vl_cobrado,
			cd_item_convenio,
			cd_motivo_glosa,
			ds_observacao)
		values	(nr_seq_glosa_w,
			nr_seq_ret_item_p,
			vl_glosa_w,
			sysdate,
			nm_usuario_p,
			cd_material_w,
			null,
			null,
			'N',
			decode(ie_tipo_valor_p, 0, 0, 1, qt_material_w, 0),
			qt_material_w,
			vl_material_w,
			cd_item_convenio_w,
			decode(ie_atualizar_motivo_glosa_w,'S',cd_motivo_glosa_p,null),
			ds_observacao_w);
	end loop;
	close c05;

	open c06;
	loop
	fetch c06 into
		cd_procedimento_w,
		ie_origem_proced_w,
		vl_procedimento_w,
		qt_procedimento_w,
		cd_item_convenio_w,
		vl_saldo_procedimento_w,
		nr_seq_partic_w,
		cd_pessoa_fisica_w,
		nr_sequencia_w;
	exit when c06%notfound;

		vl_glosa_w	:= 0;
		if	(ie_opcao_p	in ('G','P')) then
			vl_glosa_w	:= nvl(vl_procedimento_w,0) * nvl(pr_glosado_w,0);
		end if;

		vl_glosa_final_w := vl_glosa_final_w + vl_glosa_w;

		select	convenio_retorno_glosa_seq.nextval
		into	nr_seq_glosa_w
		from	dual;

		if	(nvl(ie_possui_saldo_w,'N') = 'N') or ((ie_possui_saldo_w = 'S') and (vl_saldo_procedimento_w > 0)) then
			insert	into convenio_retorno_glosa
				(nr_sequencia,
				nr_seq_ret_item,
				vl_glosa,
				dt_atualizacao,
				nm_usuario,
				cd_procedimento,
				ie_origem_proced,
				cd_setor_atendimento,
				ie_emite_conta,
				ie_atualizacao,
				qt_glosa,
				qt_cobrada,
				vl_cobrado,
				cd_item_convenio,
				cd_motivo_glosa,
				ds_observacao,
				nr_seq_propaci,
				nr_seq_partic,
				cd_pessoa_fisica)
			values	(nr_seq_glosa_w,
				nr_seq_ret_item_p,
				vl_glosa_w,
				sysdate,
				nm_usuario_p,
				cd_procedimento_w,
				ie_origem_proced_W,
				null,
				null,
				'N',
				decode(ie_tipo_valor_p, 0, 0, 1, qt_procedimento_w, 0),
				qt_procedimento_w,
				vl_procedimento_w,
				cd_item_convenio_w,
				decode(ie_atualizar_motivo_glosa_w,'S',cd_motivo_glosa_p,null),
				ds_observacao_w,
				nr_sequencia_w,				
				nr_seq_partic_w,
				cd_pessoa_fisica_w);
		end if;


	end loop;
	close c06;
	end;
else

	open c01;
	loop
	fetch c01 into
		nr_sequencia_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		vl_procedimento_w,
		qt_procedimento_w,
		cd_item_convenio_w,
		vl_saldo_procedimento_w,
		cd_setor_atendimento_w,
		nr_seq_partic_w,
		cd_pessoa_fisica_w;
	exit when c01%notfound;

		vl_glosa_w	:= 0;
		if	(ie_opcao_p	in ('G','P')) then
			vl_glosa_w	:= nvl(vl_procedimento_w,0) * nvl(pr_glosado_w,0);
		end if;

		vl_glosa_final_w := vl_glosa_final_w + vl_glosa_w;

		select	convenio_retorno_glosa_seq.nextval
		into	nr_seq_glosa_w
		from	dual;

		if	(nvl(ie_possui_saldo_w,'N') = 'N') or ((ie_possui_saldo_w = 'S') and (vl_saldo_procedimento_w > 0)) then
			insert	into convenio_retorno_glosa
				(nr_sequencia,
				nr_seq_propaci,
				nr_seq_ret_item,
				vl_glosa,
				dt_atualizacao,
				nm_usuario,
				cd_procedimento,
				ie_origem_proced,
				ie_atualizacao,
				qt_glosa,
				qt_cobrada,
				vl_cobrado,
				cd_item_convenio,
				cd_motivo_glosa,
				cd_setor_atendimento,
				ds_observacao,
				nr_seq_partic,
				cd_pessoa_fisica)
			values	(nr_seq_glosa_w,
				nr_sequencia_w,
				nr_seq_ret_item_p,
				vl_glosa_w,
				sysdate,
				nm_usuario_p,
				cd_procedimento_w,
				ie_origem_proced_W,
				'N',
				decode(ie_tipo_valor_p, 0, 0, 1, qt_procedimento_w, 0),
				qt_procedimento_w,
				vl_procedimento_w,
				cd_item_convenio_w,
				decode(ie_atualizar_motivo_glosa_w,'S',cd_motivo_glosa_p,null),
				cd_setor_atendimento_w,
				ds_observacao_w,
				nr_seq_partic_w,
				cd_pessoa_fisica_w);
		end if;

	end loop;
	close c01;

	/* ahoffelder - OS 246666 - 05/10/2010 - agrupar somente os materiais */
	if	(ie_agrupado_p = 'M') then

		open c05;
		loop
		fetch c05 into
			cd_material_w,
			vl_material_w,
			qt_material_w,
			cd_item_convenio_w;
		exit when c05%notfound;

			vl_glosa_w	:= 0;
			if	(ie_opcao_p	in ('G','P')) then
				vl_glosa_w	:= nvl(vl_material_w,0) * nvl(pr_glosado_w,0);
			end if;

			vl_glosa_final_w := vl_glosa_final_w + vl_glosa_w;

			select	convenio_retorno_glosa_seq.nextval
			into	nr_seq_glosa_w
			from	dual;

			insert	into convenio_retorno_glosa
				(nr_sequencia,
				nr_seq_ret_item,
				vl_glosa,
				dt_atualizacao,
				nm_usuario,
				cd_material,
				cd_setor_atendimento,
				ie_emite_conta,
				ie_atualizacao,
				qt_glosa,
				qt_cobrada,
				vl_cobrado,
				cd_item_convenio,
				cd_motivo_glosa,
				ds_observacao)
			values	(nr_seq_glosa_w,
				nr_seq_ret_item_p,
				vl_glosa_w,
				sysdate,
				nm_usuario_p,
				cd_material_w,
				null,
				null,
				'N',
				decode(ie_tipo_valor_p, 0, 0, 1, qt_material_w, 0),
				qt_material_w,
				vl_material_w,
				cd_item_convenio_w,
				decode(ie_atualizar_motivo_glosa_w,'S',cd_motivo_glosa_p,null),
				ds_observacao_w);

		end loop;
		close c05;

	else

		open c02;
		loop
		fetch c02 into
			nr_sequencia_w,
			cd_material_w,
			vl_material_w,
			qt_material_w,
			cd_item_convenio_w,
			cd_setor_atendimento_w,
			vl_saldo_material_w;
		exit when c02%notfound;

			vl_glosa_w	:= 0;
			if	(ie_opcao_p	in ('G','P')) then
				vl_glosa_w	:= nvl(vl_material_w,0) * nvl(pr_glosado_w,0);
			end if;

			vl_glosa_final_w := vl_glosa_final_w + vl_glosa_w;

			select	convenio_retorno_glosa_seq.nextval
			into	nr_seq_glosa_w
			from	dual;

			if	(nvl(ie_possui_saldo_w,'N') = 'N') or ((ie_possui_saldo_w = 'S') and (vl_saldo_material_w > 0)) then

				insert	into convenio_retorno_glosa
					(nr_sequencia,
					nr_seq_matpaci,
					nr_seq_ret_item,
					vl_glosa,
					dt_atualizacao,
					nm_usuario,
					cd_material,
					ie_atualizacao,
					qt_glosa,
					qt_cobrada,
					vl_cobrado,
					cd_item_convenio,
					cd_motivo_glosa,
					cd_setor_atendimento,
					ds_observacao)
				values	(nr_seq_glosa_w,
					nr_sequencia_w,
					nr_seq_ret_item_p,
					vl_glosa_w,
					sysdate,
					nm_usuario_p,
					cd_material_w,
					'N',
					decode(ie_tipo_valor_p, 0, 0, 1, qt_material_w, 0),
					qt_material_w,
					vl_material_w,
					cd_item_convenio_w,
					decode(ie_atualizar_motivo_glosa_w,'S',cd_motivo_glosa_p,null),
					cd_setor_atendimento_w,
					ds_observacao_w);

			end if;

		end loop;
		close c02;
	end if;
end if;

if	(vl_glosado_w > vl_glosa_final_w) and
	(ie_opcao_p	in ('G','P')) then

	update	convenio_retorno_glosa
	set	vl_glosa	= vl_glosa_w + (vl_glosado_w - vl_glosa_final_W)
	where	nr_sequencia	= nr_seq_glosa_w
	and	nr_seq_ret_item	= nr_seq_ret_item_p
	and	(nvl(ie_proc_partic_p,'N') = 'N' or nvl(vl_glosa,0) <> nvl(vl_cobrado,0));

elsif	(vl_glosado_w < vl_glosa_final_w) and
	(ie_opcao_p	in ('G','P')) then

	update	convenio_retorno_glosa
	set	vl_glosa	= vl_glosa_w - (vl_glosa_final_W - vl_glosado_w)
	where	nr_sequencia	= nr_seq_glosa_w
	and	nr_seq_ret_item	= nr_seq_ret_item_p;

end if;

if	(ie_opcao_p	= 'P') then

	update	convenio_retorno_item
	set	vl_glosado	= vl_glosado_w,
		vl_pago		= 0
	where	nr_sequencia	= nr_seq_ret_item_p
	and	nvl(cd_autorizacao, 'N�o Informada') = nvl(cd_autorizacao_w, 'N�o Informada');

end if;

commit;
end Gerar_Retorno_Glosa_Conta;
/
