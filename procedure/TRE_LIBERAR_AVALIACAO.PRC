create or replace
procedure tre_liberar_avaliacao(nr_seq_avaliacao_p	number,
				nm_usuario_p		Varchar2) is 

begin

update 	tre_avaliacao
set 	dt_liberacao = sysdate
where 	nr_sequencia = nr_seq_avaliacao_p;

commit;

end tre_liberar_avaliacao;
/
