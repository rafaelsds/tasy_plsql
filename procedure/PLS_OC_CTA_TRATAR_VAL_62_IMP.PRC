create or replace
procedure pls_oc_cta_tratar_val_62_imp(	nr_seq_combinada_p		in pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p		in pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p		in pls_oc_cta_selecao_imp.nr_id_transacao%type) is 

tb_seq_selecao_w	pls_util_cta_pck.t_number_table;
tb_valido_w		pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w		pls_util_cta_pck.t_varchar2_table_4000;

cursor	C01 (	nr_seq_oc_cta_comb_pc		pls_oc_cta_combinada.nr_sequencia%type) is
	select	nr_sequencia,
		ie_val_guia_tiss,
		ie_exige_ref_guia
	from	pls_oc_cta_val_guia_tiss regra
	where	regra.nr_seq_oc_cta_comb = nr_seq_oc_cta_comb_pc;
	
cursor C02 (	nr_id_transacao_pc		pls_oc_cta_selecao_imp.nr_id_transacao%type,
		ie_exige_ref_guia_pc		pls_oc_cta_val_guia_tiss.ie_exige_ref_guia%type) is
	select	sel.nr_sequencia nr_seq_selecao,
		pls_obter_guia_ref_valida_imp(	conta.cd_guia_principal, 
						conta.nr_sequencia,
						prot.ie_tipo_guia, 
						ie_exige_ref_guia_pc, 
						'S') ie_valido,
		null ds_observacao
	from	pls_oc_cta_selecao_imp	sel,
		pls_conta_imp conta,
		pls_protocolo_conta_imp	prot
	where	sel.nr_id_transacao	= nr_id_transacao_pc
	and	sel.ie_valido		= 'S'
	and	conta.nr_sequencia	= sel.nr_seq_conta
	and 	prot.nr_sequencia	= conta.nr_seq_protocolo
	--O tratamento abaixo se deve ao fato da necessidade de consistir apenas as contas que possuem guia de referencia informada
	and	conta.cd_guia_principal	is not null;
begin

-- Verificar se a regra informada � v�lida.
if	(nr_seq_combinada_p is not null) then	
	-- Percorrer as regra cadastradas para a valida��o.
	for	r_c01_w in C01(nr_seq_combinada_p) loop
	
		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_ocor_imp_pck.atualiza_campo_auxiliar (	'V', 
								'N', 
								nr_id_transacao_p, 
								null);
		
		-- Se a regra validar apenas os registros 
		if	(r_c01_w.ie_val_guia_tiss = 'S') then
			
			--limpa as vari�veis
			pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
								tb_valido_w,
								tb_observacao_w);
			
			-- tratamento para procedimentos
			open C02(nr_id_transacao_p, r_c01_w.ie_exige_ref_guia);
			loop
				fetch C02 bulk collect into	tb_seq_selecao_w, tb_valido_w, tb_observacao_w		
				limit pls_util_pck.qt_registro_transacao_w;
				exit when tb_seq_selecao_w.count = 0;
				
				--Grava as informa��es na tabela de sele��o
				pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, 
										tb_valido_w,
										tb_observacao_w, 
										nr_id_transacao_p, 
										'SEQ');
				--limpa as vari�veis
				pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
									tb_valido_w,
									tb_observacao_w);
			end loop;
			close C02;
		end if;

	end loop; -- r_c01_w
	
	--Grava as informa��es na tabela de sele��o
	pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, 
							tb_valido_w,
							tb_observacao_w, 
							nr_id_transacao_p, 
							'SEQ');
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_ocor_imp_pck.atualiza_campo_valido ('V', 
						'N', 
						ie_regra_excecao_p, 
						null,
						nr_id_transacao_p, 
						null);
end if;

end pls_oc_cta_tratar_val_62_imp;
/