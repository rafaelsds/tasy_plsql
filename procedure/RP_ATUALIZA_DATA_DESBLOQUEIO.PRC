create or replace
procedure rp_atualiza_data_desbloqueio(nr_seq_pac_reab_p	number) is 

begin
if	(nr_seq_pac_reab_p is not null) then
	update	rp_paciente_reabilitacao
	set	dt_desbloqueio = sysdate
	where	nr_sequencia = nr_seq_pac_reab_p;
	
end if;

commit;

end rp_atualiza_data_desbloqueio;
/