create or replace
procedure importar_ctb_orcamento(	nm_usuario_p 		varchar2,
				dt_referencia_p		varchar2,
				cd_conta_contabil_p	varchar2,
				ds_observacao_p		varchar2,
				cd_estabelecimento_p	number,
				cd_centro_custo_p	number,
				vl_orcado_p		number,
				vl_realizado_p		number) is

nr_seq_mes_ref_w		number(10);
qt_centro_custo_w		number(10);
nr_seq_orcamento_w		number(10);
cd_empresa_w			number(4);
qt_registro_w			number(4);
ie_imp_mes_realizado_w		varchar2(1);
vl_orcado_w			number(15,2);
vl_realizado_w			number(15,2);
ie_permite_vl_zerado_w		varchar2(1);
ie_imp_vl_original_w		varchar2(1);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
dt_referencia_w			ctb_mes_ref.dt_referencia%type;	
ie_valida_mes_w		varchar2(1);

begin
qt_centro_custo_w			:= 0;
nr_seq_mes_ref_w			:= 0;

begin
select	a.cd_estabelecimento,
	nvl(obter_empresa_estab(a.cd_estabelecimento),0)
into	cd_estabelecimento_w,
	cd_empresa_w
from	centro_custo a
where	a.cd_centro_custo = cd_centro_custo_p;
exception
when others then
	cd_empresa_w := 0;
end;

if	(cd_empresa_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(227219);
end if;

begin 
	dt_referencia_w := trunc(to_date(dt_referencia_p,'ddmmyyyy'), 'mm');
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(285538);
end;

obter_param_usuario(925,112,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_permite_vl_zerado_w);

select	nvl(max(nr_sequencia),0)
into	nr_seq_mes_ref_w
from	ctb_mes_ref
where	dt_referencia	= dt_referencia_w
and	cd_empresa	= cd_empresa_w;

select	count(*)
into	qt_centro_custo_w
from	ctb_orcamento
where	cd_estabelecimento	= cd_estabelecimento_w
and	cd_conta_contabil	= cd_conta_contabil_p
and	cd_centro_custo		= cd_centro_custo_p
and	nr_seq_mes_ref		= nr_seq_mes_ref_w;

ie_valida_mes_w := nvl(obter_valor_param_usuario(925, 120, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w), 'N');

if	(nr_seq_mes_ref_w = 0)	 then
	begin
	if	(ie_valida_mes_w = 'N')	then
		begin
		
		select	ctb_mes_ref_seq.nextval
		into 	nr_seq_mes_ref_w
		from	dual;
	
		insert into ctb_mes_ref(
			nr_sequencia,
			cd_empresa,
			dt_referencia,
			dt_atualizacao,
			nm_usuario)
		values(nr_seq_mes_ref_w,
			cd_empresa_w,
			dt_referencia_w,
			sysdate,
			nm_usuario_p);
	end;		
	elsif	(ie_valida_mes_w = 'S') then
				wheb_mensagem_pck.exibir_mensagem_abort(1060268);
	end if;
	end;
end if;

ie_imp_mes_realizado_w	:= nvl(obter_valor_param_usuario(925, 90, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w), 'S');
ie_imp_vl_original_w	:= nvl(obter_valor_param_usuario(925, 118, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w), 'S');
vl_orcado_w		:= vl_orcado_p;
vl_realizado_w		:= vl_realizado_p;

if	(qt_centro_custo_w = 0) then
	select	ctb_orcamento_seq.nextval
	into	nr_seq_orcamento_w
	from	dual;

	begin
	insert	into ctb_orcamento(
		nr_sequencia,
		nr_seq_mes_ref,
		dt_atualizacao,
		nm_usuario,
		cd_estabelecimento,
		cd_conta_contabil,
		cd_centro_custo,
		vl_original,
		vl_orcado,
		vl_realizado,
		ds_observacao,
		ie_origem_orc,
		nm_usuario_nrec,
		dt_atualizacao_nrec)
	values(nr_seq_orcamento_w,
		nr_seq_mes_ref_w,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_w,
		cd_conta_contabil_p,
		cd_centro_custo_p,
		nvl(vl_orcado_p,0),
		nvl(vl_orcado_p,0),
		nvl(vl_realizado_p,0),
		ds_observacao_p,
		'IMP',
		nm_usuario_p,
		sysdate);
	exception when others then
		wheb_mensagem_pck.exibir_mensagem_abort(227220,'CD_CONTA_CONTABIL_P=' || cd_conta_contabil_p || ';CD_CENTRO_CUSTO_P=' || cd_centro_custo_p);
	end;
elsif	(qt_centro_custo_w > 0) then
	/* matheus os 65029 16/08/07 hsl para os meses que a contabilidade ja lancou o realizado e falta importar o valor orcado e original*/
	select	nr_sequencia
	into	nr_seq_orcamento_w
	from	ctb_orcamento
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_conta_contabil	= cd_conta_contabil_p
	and	cd_centro_custo		= cd_centro_custo_p
	and	nr_seq_mes_ref		= nr_seq_mes_ref_w;

	if	((vl_orcado_w = 0) 		and
		(ie_permite_vl_zerado_w = 'N'))	then
		vl_orcado_w	:= null;
	end if;

	if	((vl_realizado_w = 0) 		and
		(ie_permite_vl_zerado_w = 'N')) then
		vl_realizado_w	:= null;
	end if;

	if	(ie_imp_mes_realizado_w	= 'S') then

		update	ctb_orcamento
		set	vl_orcado		= nvl(vl_orcado_w, vl_orcado),
			vl_original		= decode (ie_imp_vl_original_w, 'S', nvl(vl_orcado_w, vl_original),'N',vl_original),
			vl_realizado		= nvl(vl_realizado_w, vl_realizado),
			ie_origem_orc		= 'IMP',
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_seq_orcamento_w;

	elsif	(ie_imp_mes_realizado_w	= 'N') then

		update	ctb_orcamento
		set	vl_orcado		= nvl(vl_orcado_w, vl_orcado),
			vl_original		= decode (ie_imp_vl_original_w, 'S', nvl(vl_orcado_w, vl_original),'N',vl_original),
			ie_origem_orc		= 'IMP',
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_seq_orcamento_w
		and	nvl(vl_realizado, 0)	= 0;
	end if;
end if;

commit;

end importar_ctb_orcamento;
/