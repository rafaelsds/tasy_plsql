create or replace
procedure sup_gerar_unitarizacao_item(	qtd_registro_p		number,
				qtd_material_p		number,
				nr_seq_unitarizacao_p	number,
				nm_usuario_p		varchar2) is 
	
nr_sequencia_w number(10);

begin

if (nvl(qtd_registro_p,0) <> 0) and (nvl(qtd_material_p,0) <> 0) then
begin
	for i in 1..qtd_registro_p loop
	begin
		
		Select ITEM_UNITARIZACAO_seq.nextval
		into	nr_sequencia_w
		from 	dual;

		insert into ITEM_UNITARIZACAO (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_unitarizacao,
			ie_tipo_processo,
			qt_material,
			dt_atualizacao_nrec,
			nr_seq_motivo_perda,
			nm_usuario_nrec,
			nr_seq_atividade
		) values (
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			nr_seq_unitarizacao_p,
			'A',
			qtd_material_p,
			sysdate,
			null,
			nm_usuario_p,
			null);
			
		commit;
	end;
	end loop;
end;
end if;

end sup_gerar_unitarizacao_item;
/