create or replace PROCEDURE PFCS_ENABLE_FUNC_VALID_CODE (
    cd_validation_code_p varchar2,
    cd_cgc_client_p varchar2,
    NM_USUARIO_P varchar2
) IS

cd_funcao_w number;
cd_funcao_pfcs_w number;

BEGIN

cd_funcao_w := GET_IF_ACTIVATION_KEY_VALID (cd_validation_code_p, cd_cgc_client_p);

select 	MAX(cd_funcao) into cd_funcao_pfcs_w
from PFCS_OFFICIAL_USE where cd_funcao = cd_funcao_w;

if cd_funcao_pfcs_w IS NOT NULL then
 Wheb_mensagem_pck.exibir_mensagem_abort(1155537);
else

    select 	MAX(cd_funcao) into cd_funcao_pfcs_w
    from 	 funcao 
    where 	 NR_SEQ_MODULO = 1606 and cd_funcao = cd_funcao_w;
    
    if cd_funcao_pfcs_w IS NOT NULL then
        INSERT INTO PFCS_OFFICIAL_USE 
        (NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, CD_FUNCAO, DS_VALIDATION_CODE) 
        VALUES (
        pfcs_official_use_seq.nextval, 
        sysdate, 
        NM_USUARIO_P, 
        sysdate, 
        NM_USUARIO_P, 
        cd_funcao_w, 
        cd_validation_code_p
        );
    else 
        Wheb_mensagem_pck.exibir_mensagem_abort(1155455);
    end if;
 
end if;

END PFCS_ENABLE_FUNC_VALID_CODE;
/