create or replace procedure checar_acesso_plataforma(	nm_usuario_p		varchar2,
														ds_retorno_p		out varchar2,
														nr_seq_ordem_serv_p	number default null) is
																
nr_seq_ordem_serv_w			number(10)		:= null;
ds_plataforma_w				varchar2(255)	:= 'Tasy';
begin


if	(nr_seq_ordem_serv_p is null) then
	obter_valor_dinamico_bv(
						'SELECT REG_OBJECT_LOG_PCK.load_so_activity_corp(:nm_usuario_p) FROM DUAL',
						'nm_usuario_p=' || nm_usuario_p,
						nr_seq_ordem_serv_w
						);
else
	nr_seq_ordem_serv_w := nr_seq_ordem_serv_p;
end if;



obter_valor_dinamico_bv(
	'SELECT	CORP.MDR_SP_RULES_PCK.get_plataform_service_order@whebl01_dbcorp(:nr_seq_ordem_serv_p) FROM DUAL',
	'nr_seq_ordem_serv_p=' || nr_seq_ordem_serv_w,
	ds_plataforma_w
);

ds_plataforma_w := check_application_sp(ds_plataforma_w);

ds_retorno_p		:= ds_plataforma_w;

end CHECAR_ACESSO_PLATAFORMA;
/
