create or replace
procedure liberar_todos_conv_carga_preco(
			cd_convenio_p		number,
			nm_usuario_p		Varchar2) is 

begin
	if	(cd_convenio_p is not null) and 
		(nm_usuario_p is not null)then
		begin
			update 	convenio_carga_preco 
			set 	ie_status = 'L',
				nm_usuario = nm_usuario_p
			where 	cd_convenio = cd_convenio_p;		
		end;
	end if;

commit;

end liberar_todos_conv_carga_preco;
/