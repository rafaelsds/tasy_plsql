create or replace
procedure PEP_Gerar_Token_Acesso(nm_usuario_p			Varchar2,
			nr_atendimento_p		number,
			cd_pessoa_fisica_p		varchar2,
			dt_inicio_p 		date default null,
			cd_token_p		in out varchar2,
			nr_prescricao_p 		number default null,
			nr_transcricao_p 		number default null) is 
qt_reg_w	number(10);
ds_comando_w	varchar2(2000);
ds_parametro_w	varchar2(2000);
ds_sep_bv_w	varchar2(10);
nr_atendimento_w	number(10);
nr_prescricao_w     number(14);
cd_pessoa_fisica_w	varchar2(10);

begin

select	count(*)
into	qt_reg_w
from	user_tables
where	table_name = 'CUS_SIH_TOKENS';

nr_atendimento_w := nr_atendimento_p;

if	(nr_atendimento_p > 0) then

	SELECT 	max(CD_PESSOA_FISICA)
	INTO 	cd_pessoa_fisica_w
	FROM 	atendimento_paciente
	WHERE 	NR_ATENDIMENTO = NR_ATENDIMENTO_P;

	if (cd_pessoa_fisica_w <> cd_pessoa_fisica_p) then	
	
		nr_atendimento_w :=  0 ;
	
	end if;


end if;	


if	(qt_reg_w	> 0) then

	select 	decode(wheb_usuario_pck.get_cd_funcao,3112,null,nr_prescricao_p)
	into   	nr_prescricao_w
	from 	dual;
	ds_sep_bv_w	:= OBTER_SEPARADOR_BV;
	cd_token_p	:= SYS_GUID();
	ds_comando_w	:= '	insert into CUS_SIH_TOKENS (TOKEN,LOGIN,ATENDIMENTO,cd_pessoa_fisica,nr_prescricao,dt_inicio, nr_transcricao, cd_funcao) values (:TOKEN,:NM_USUARIO,:ATENDIMENTO,:cd_pessoa_fisica,:nr_prescricao,:dt_inicio, :nr_transcricao, :cd_funcao)';
	ds_parametro_w	:= 'TOKEN='||cd_token_p||ds_sep_bv_w||
			   'ATENDIMENTO='||nr_atendimento_w||ds_sep_bv_w||
			   'cd_pessoa_fisica='||cd_pessoa_fisica_p||ds_sep_bv_w||
			   'nr_prescricao='|| nr_prescricao_w ||ds_sep_bv_w||
			   'nr_transcricao='||nr_transcricao_p||ds_sep_bv_w||
			   'NM_USUARIO='||nm_usuario_p||ds_sep_bv_w||
			   'dt_inicio='||TO_CHAR(dt_inicio_p,'dd/mm/yyyy hh24:mi:ss')||ds_sep_bv_w ||
			   'cd_funcao='|| wheb_usuario_pck.get_cd_funcao;
			   
			   
	Exec_sql_Dinamico_bv('TOKEN',ds_comando_w,ds_parametro_w);
	commit;
end if;

end PEP_Gerar_Token_Acesso;
/
