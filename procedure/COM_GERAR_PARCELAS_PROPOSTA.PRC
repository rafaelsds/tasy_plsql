create or replace
procedure com_gerar_parcelas_proposta( 	dt_inicio_parcela_p	date,
					qt_parcela_p		number,
					nr_seq_proposta_item_p	number,
					nm_usuario_p		Varchar2) is 

vl_total_w		number(17,4);					
qt_parcela_atual_W	number(10);
dt_vencimento_w		date;
vl_parcela_w		number(17,4);					
begin
select	max(vl_item)
into	vl_total_w
from	COM_CLIENTE_PROP_ITEM
where 	nr_sequencia = nr_seq_proposta_item_p;

vl_parcela_w := round(vl_total_w / qt_parcela_p,4); 

qt_parcela_atual_W := 1;

dt_vencimento_w := dt_inicio_parcela_p;

while (qt_parcela_atual_w <= qt_parcela_p) loop 
	begin				
	insert into com_cliente_prop_parc (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_prop_item,
		dt_vencimento,
		vl_parcela,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	com_cliente_prop_parc_seq.nextVal,
		sysdate,
		nm_usuario_p,
		nr_seq_proposta_item_p,
		dt_vencimento_w,
		vl_parcela_w,
		sysdate,
		nm_usuario_p);
		
	dt_vencimento_w := PKG_DATE_UTILS.ADD_MONTH(dt_inicio_parcela_p,qt_parcela_atual_w,0);
	qt_parcela_atual_w := qt_parcela_atual_w + 1;
	end;
end loop;

commit;

end com_gerar_parcelas_proposta;
/