create or replace
procedure nut_gerar_cardapio_dieta(	dt_cardapio_p	        date,
					nr_seq_local_p	        varchar2,
					nr_seq_servico_p        varchar2,
					nr_seq_opcao_p	        varchar2,
					nr_seq_receita_p        varchar2,					
					dt_mes_ref_p            date) is


nr_sequencia_w		nut_cardapio_dia.nr_sequencia%type;
nr_seq_cardapio_w	nut_cardapio.nr_sequencia%type;
nr_seq_comp_w           nut_cardapio.nr_seq_comp%type;
nr_seq_grupo_producao_w	nut_dieta_rec.nr_sequencia%type;
cd_dieta_w		nut_dieta_rec.cd_dieta%type;
ie_dia_semana_w		number(3);
ie_semana_w		number(3);
dt_ini_origem_w		date;
dt_fim_origem_w		date;

-- Dietas / Grupo de dietas
cursor c01 is
	select 	cd_dieta, 
		nr_seq_grupo
	from   	nut_dieta_rec  
	where  	nr_seq_receita = nr_Seq_receita_p;

begin

if (nr_Seq_receita_p is not null) then

	--obter c�digo do dia e da semana de destino da c�pia
	ie_dia_semana_w := obter_cod_dia_semana(dt_cardapio_p);
	ie_semana_w 	:= obter_semana_cardapio(dt_cardapio_p);
	--obter datas de in�cio e fim de vig�ncia do dia selecionado para c�pia
	obter_dia_ini_fim_mes_vig( dt_mes_ref_p, dt_ini_origem_w, dt_fim_origem_w);

	open	c01;
		loop
		fetch	c01 into
			cd_dieta_w, 
			nr_seq_grupo_producao_w;
		exit	when	c01%notfound;

		-- verifica se j� existe registro pra essa receita
		select max(nr_sequencia)
		into   nr_sequencia_w
		from   nut_cardapio_dia 
		where  cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
		and    (((nr_seq_grupo_producao is null and cd_dieta is not null) and (cd_dieta = cd_dieta_w)) or   
		       ((cd_dieta is null and nr_seq_grupo_producao is not null) and (nr_seq_grupo_producao = nr_seq_grupo_producao_w)))
		and    nr_seq_opcao = nr_seq_opcao_p
		and    nr_seq_local = nr_seq_local_p
		and    nr_seq_servico = nr_seq_servico_p
		and    dt_vigencia_inicial = dt_ini_origem_w
		and    dt_vigencia_final = dt_fim_origem_w
		and	ie_dia_semana = ie_dia_semana_w
		and	ie_semana = ie_semana_w;

		if (nr_sequencia_w is null) then 

			select	nut_cardapio_dia_seq.nextval
			into	nr_sequencia_w
			from	dual;
		
			insert into nut_cardapio_dia(
				nr_sequencia,
				cd_estabelecimento,
				nr_seq_grupo_producao, 
				cd_dieta, 
				nr_seq_opcao,
				nr_seq_local,
				nr_seq_servico,
				qt_pessoa_atend,
				ie_cardapio_padrao,
				ie_dia_semana,
				ie_semana,
				dt_vigencia_inicial,
				dt_vigencia_final,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario)
			values(	nr_sequencia_w,
				wheb_usuario_pck.get_cd_estabelecimento,
				nr_seq_grupo_producao_w,
				cd_dieta_w,
				nr_seq_opcao_p,
				nr_seq_local_p,
				nr_seq_servico_p,
				null,
				'N',
				ie_dia_semana_w,
				ie_semana_w,
				dt_ini_origem_w,
				dt_fim_origem_w,
				sysdate,
				wheb_usuario_pck.get_nm_usuario,
				sysdate,
				wheb_usuario_pck.get_nm_usuario);
		end if;
		

		SELECT 	nr_seq_composicao 
		into 	nr_seq_comp_w
		FROM 	nut_receita 
		WHERE nr_sequencia = nr_Seq_receita_p;		 

		nr_seq_cardapio_w := null;
		--Verifica se j� existe
		select 	max(nr_sequencia) 
		into   	nr_seq_cardapio_w		
		from 	nut_cardapio
		where	nr_seq_card_dia = nr_sequencia_w
		and	nr_seq_comp = nr_seq_comp_w
		and	nr_seq_receita = nr_Seq_receita_p;
		
		if (nr_seq_cardapio_w is null) then 
		
			insert into nut_cardapio
				(nr_sequencia,
				nr_seq_card_dia,
				nr_seq_comp,
				nr_seq_receita,
				qt_refeicao,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario)
			values(	nut_cardapio_seq.nextval,
				nr_sequencia_w,
				nr_seq_comp_w,
				nr_Seq_receita_p,
				1,
				sysdate,
				wheb_usuario_pck.get_nm_usuario,
				sysdate,
				wheb_usuario_pck.get_nm_usuario);
		end if;
	
	end loop;
	close c01;

commit;	
	
end if;
	
end nut_gerar_cardapio_dieta;
/
