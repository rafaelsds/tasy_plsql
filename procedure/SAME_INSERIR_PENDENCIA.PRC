create or replace
procedure same_inserir_pendencia(nr_seq_prontuario_p	number,
				cd_barras_p		varchar2,
				ie_assinatura_p		varchar2,
				ie_existe_tasy_p	varchar2,
				nr_seq_tipo_documento_p	number,
				nr_documento_p		number,
				ie_documentacao_p	varchar2,
				cd_responsavel_p	varchar2,
				ie_situacao_doc_p	varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 
				
nr_sequencia_w			number(10);
qt_pendencia_existe_tasy_w	number(10);
qt_pendencia_assinatura_w	number(10);

ie_pendencia_existe_tasy_w	varchar2(1);
ie_pendencia_assinatura_w	varchar2(1);


begin

if 	(nr_seq_tipo_documento_p is not null) and
	(nr_documento_p is not null) then
	
	select	same_controle_prontuario_seq.NextVal
	into	nr_sequencia_w
	from	dual;
	
	select	max(a.ie_exige_registro),
		max(a.ie_exige_assinatura)
	into	ie_pendencia_existe_tasy_w,
		ie_pendencia_assinatura_w
	from	same_tipo_documento a
	where	a.nr_sequencia = nr_seq_tipo_documento_p;
	
	insert into same_controle_prontuario(
					nr_sequencia, 
					nr_seq_tipo_documento, 
					nr_seq_prontuario, 
					nr_documento,
					cd_barras,
					nm_usuario_nrec, 
					nm_usuario, 
					ie_situacao, 
					ie_documentacao, 
					dt_registro, 
					dt_atualizacao_nrec, 
					dt_atualizacao, 
					cd_responsavel, 
					cd_estabelecimento)
			values		(nr_sequencia_w,
					nr_seq_tipo_documento_p,
					nr_seq_prontuario_p,
					nr_documento_p,
					cd_barras_p,
					nm_usuario_p,
					nm_usuario_p,
					'A',					
					ie_documentacao_p,
					sysdate,
					sysdate,
					sysdate,
					cd_responsavel_p,
					cd_Estabelecimento_p);
					
	--Falta Assinatura
	select	count(*)
	into	qt_pendencia_assinatura_w
	from	same_pendencia
	where	ie_tipo_pendencia = 'A';
	
	if (ie_assinatura_p = 'N') and (qt_pendencia_assinatura_w > 0) and (ie_pendencia_assinatura_w = 'S') then
	
		insert into same_pendencia_prontuario(	nr_sequencia, 
							nr_seq_controle,
							nr_seq_pendencia,
							nm_usuario,
							nm_usuario_nrec, 
							dt_atualizacao,
							dt_atualizacao_nrec,
							ie_situacao_doc)
						select	same_pendencia_prontuario_seq.NextVal,
							nr_sequencia_w,
							nr_sequencia,
							nm_usuario_p,
							nm_usuario_p,
							sysdate,
							sysdate,
							ie_situacao_doc_p
						from	same_pendencia
						where	ie_tipo_pendencia = 'A'
						and	ie_situacao = 'A';							
	end if;
	
	--Falta registro no Tasy
	select	count(*)
	into	qt_pendencia_existe_tasy_w
	from	same_pendencia
	where	ie_tipo_pendencia = 'R';
	
	if (ie_existe_tasy_p = 'N') and (qt_pendencia_assinatura_w > 0) and (ie_pendencia_existe_tasy_w = 'S')  then
	
		insert into same_pendencia_prontuario(	nr_sequencia, 
							nr_seq_controle,
							nr_seq_pendencia,
							nm_usuario,
							nm_usuario_nrec, 
							dt_atualizacao,
							dt_atualizacao_nrec,
							ie_situacao_doc)
						select	same_pendencia_prontuario_seq.NextVal,
							nr_sequencia_w,
							nr_sequencia,
							nm_usuario_p,
							nm_usuario_p,
							sysdate,
							sysdate,
							ie_situacao_doc_p
						from	same_pendencia
						where	ie_tipo_pendencia = 'R'
						and	ie_situacao = 'A';
	end if;
	
end if;

commit;

end same_inserir_pendencia;
/