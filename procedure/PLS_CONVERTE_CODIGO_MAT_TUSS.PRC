create or replace
procedure pls_converte_codigo_mat_tuss
			(	nr_seq_material_p	in	Number,				
				cd_material_tuss_p	out	Number,
				nr_seq_mat_tuss_p	out	Number) is
				
qt_registro_w			Number(10);
cd_material_tuss_w		tuss_material_item.cd_material_tuss%type;
nr_seq_mat_tuss_w		tuss_material_item.nr_sequencia%type;

begin

select	count(1)
into	qt_registro_w
from	pls_material_tuss
where	nr_seq_material =  nr_seq_material_p;


if	( qt_registro_w > 0 ) then
	begin	
		select	nr_seq_tuss_mat_item,
			obter_dados_mat_tuss(nr_seq_tuss_mat_item,'C')
		into	nr_seq_mat_tuss_w,
			cd_material_tuss_w
		from	pls_material_tuss
		where	nr_seq_material = nr_seq_material_p
		and 	dt_vigencia_inicial <= sysdate
		and	(dt_vigencia_final is null or dt_vigencia_final > sysdate);	
	exception
	when others then
		nr_seq_mat_tuss_w 	:= null;
		cd_material_tuss_w	:= null;
	end;
end if;

nr_seq_mat_tuss_p	:= nr_seq_mat_tuss_w;
cd_material_tuss_p	:= cd_material_tuss_w;

end pls_converte_codigo_mat_tuss;
/