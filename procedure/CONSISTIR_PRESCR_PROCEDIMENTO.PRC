CREATE OR REPLACE
PROCEDURE Consistir_Prescr_Procedimento (
				nr_prescricao_p	Number,
				nr_seq_prescricao_p	Number,
				nm_usuario_p		Varchar2,
				cd_perfil_p		Number,
				ds_erro_p	out	Number) is

cd_estabelecimento_w		Number(4);
dt_prescricao_w			Date;
nr_atendimento_w		Number(10,0);
qt_peso_w			number(6,3);
qt_altura_cm_w			number(4,1);
nr_seq_exame_w			exame_laboratorio.nr_seq_exame%type;

/*	Erros
	0   - N�o tem erro
	124 - Tem erro mas permite liberar a prescri��o
	125 - Tem erro e n�o permite liberar a prescri��o

	As consist�ncias 125 devem ficar no final da procedure.
*/
BEGIN
begin
select	cd_estabelecimento,
	dt_prescricao,
	nvl(nr_atendimento,0),
	qt_peso,
	qt_altura_cm
into	cd_estabelecimento_w,
	dt_prescricao_w,
	nr_atendimento_w,
	qt_peso_w,
	qt_altura_cm_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;
exception
	when others then
		cd_estabelecimento_w	:= 1;
		nr_atendimento_w	:= 0;
end;
 
consistir_regra_prescr_proced(nr_prescricao_p, nr_seq_prescricao_p, cd_estabelecimento_w, cd_perfil_p, nm_usuario_p);

select	max(ie_erro)
into	ds_erro_p
from	prescr_procedimento
where	nr_prescricao	= nr_prescricao_p
and	((nr_sequencia		= nr_seq_prescricao_p) or
	 (nr_seq_prescricao_p	= 0));


if (nr_seq_prescricao_p is not null) then	 

	select	max(a.nr_seq_exame)
	into	nr_seq_exame_w
	from 	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_p
	and		a.nr_sequencia = nr_seq_prescricao_p;
	
	gerar_exame_lab_dependente(nr_prescricao_p, nr_seq_prescricao_p, 10, nr_atendimento_w, nm_usuario_p, nr_seq_exame_w, null, null);
end if;
	 
	 
END Consistir_Prescr_Procedimento;
/
