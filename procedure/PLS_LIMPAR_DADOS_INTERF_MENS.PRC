create or replace
procedure pls_limpar_dados_interf_mens(
		nm_usuario_p	varchar2) is 

begin

delete	from w_pls_interface_mens
where	nm_usuario = nm_usuario_p;

commit;

end pls_limpar_dados_interf_mens;
/