create or replace
procedure dirf_registro_respo(	nr_seq_lote_p		number,
				nm_usuario_p		Varchar2) is 

ds_arquivo_w	varchar2(2000);
separador_w	varchar2(1) := '|';

begin

select 	'RESPO' || separador_w || --nome arquivo
	lpad(nvl(substr(obter_dados_pf(e.cd_contabilista,'CPF'),1,11),'0'),11,'0') || separador_w || --cpf do responsável pelo preenchimento
	substr(obter_nome_pf_pj(e.cd_contabilista,null),1,60) || separador_w || --Nome do responsável pelo preenchimento
	substr(obter_compl_pf(e.cd_contabilista,2,'DDT'),1,8)  || separador_w || -- DDD do telefone do responsável pelo preenchimento
	substr(obter_compl_pf(e.cd_contabilista,2,'T'),1,8)  || separador_w || -- Telefone do responsável pelo preenchimento
	substr(obter_compl_pf(e.cd_contabilista,2,'RAM'),1,6) || separador_w || --Ramal do responsável pelo preenchimento
	substr(obter_compl_pf(e.cd_contabilista,2,'FAX'),1,8) || separador_w || --Fax do responsável pelo preenchimento
	substr(obter_compl_pf(e.cd_contabilista,2,'M'),1,50) || separador_w --E-mail do responsável pelo preenchimento
into	ds_arquivo_w	
from	dirf_lote a,
	estabelecimento b,
	empresa e
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	b.cd_empresa = e.cd_empresa	
and	a.nr_sequencia = nr_seq_lote_p;

insert 	into w_dirf_arquivo (nr_sequencia,
			     nm_usuario,
			     nm_usuario_nrec,
			     dt_atualizacao,
			     dt_atualizacao_nrec,
			     ds_arquivo,
			     nr_seq_apresentacao,
			     nr_seq_registro)
values			    (w_dirf_arquivo_seq.nextval,
			     nm_usuario_p,
			     nm_usuario_p,
			     sysdate,
			     sysdate,
			     ds_arquivo_w,
			     2,
			     0);
commit;

end dirf_registro_respo;
/
