create or replace
procedure hd_gerar_itens_analise_agua_js(nr_seq_protocolo_analise_p	number,
					nr_seq_ponto_acesso_p		number,
					cd_estabelecimento_p		number,
					ie_tipo_controle_p		varchar2,
					ie_acao_executada_p		number,
					nr_seq_analise_agua_p		number,
					nm_usuario_p			varchar2) is 

nr_seq_prot_exame_micro_w 	number(10);				
nr_seq_prot_exame_fisico_w	number(10);				
nr_seq_prot_exame_w		number(10);
nr_seq_protocolo_w		number(10);

begin

select	max(nr_seq_prot_exame_micro) nr_seq_prot_exame_micro,
	max(nr_seq_prot_exame_fisico) nr_seq_prot_exame_fisico
into	nr_seq_prot_exame_micro_w,
	nr_seq_prot_exame_fisico_w
from 	hd_parametro 
where 	cd_estabelecimento = cd_estabelecimento_p;

select	max(nr_seq_prot_exame) nr_seq_prot_exame
into	nr_seq_prot_exame_w
from    hd_ponto_acesso
where   nr_sequencia = nr_seq_ponto_acesso_p;


if	(nr_seq_protocolo_analise_p > 0) and
	(ie_acao_executada_p = 1) then
	
	nr_seq_protocolo_w :=  nr_seq_protocolo_analise_p;
	
elsif 	(nr_seq_prot_exame_micro_w > 0) and
	(ie_tipo_controle_p = 'M') and
	(ie_acao_executada_p = 1) then
	
	nr_seq_protocolo_w :=  nr_seq_prot_exame_micro_w;
	
elsif	(nr_seq_prot_exame_fisico_w > 0) and
	(ie_tipo_controle_p = 'F') and
	(ie_acao_executada_p = 1) then
	
	nr_seq_protocolo_w :=  nr_seq_prot_exame_fisico_w;
	
else 	nr_seq_protocolo_w :=  nr_seq_prot_exame_w;

end if;


Hd_gerar_itens_analise_agua(nr_seq_protocolo_w,nr_seq_analise_agua_p,nm_usuario_p);


end hd_gerar_itens_analise_agua_js;
/