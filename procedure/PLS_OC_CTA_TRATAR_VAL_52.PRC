create or replace
procedure pls_oc_cta_tratar_val_52(
			dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de servi�os n�o habilitados para o prestador. Esta valida��o ir� verificar 
	o cadastro de servi�os do prestador, fun��o OPS - Prestadores \ Prestadores \ Servi�os e Materiais \ Servi�os.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

QUANDO ALTERAR UM DOS CURSORES DA VALIDA��O VERIFICAR A NECESSIDADE DE ALTERAR NO OUTRO CURSOR.

Altera��es:
------------------------------------------------------------------------------------------------------------------
 jjkruk OS 701794 11/09/2014 - Cria��o da rotina.
 jjkruk OS 701794 23/09/2014 - Ajustado para usar executor da conta para tipo de prestador e prestador principal, 
				Ajustado para usar ie_carater_internacao mesmo no IMP, devido a vers�o 3.01 do TISS.
				Ajustado para usar cd_procedimento_imp
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_tb_selecao_w		pls_tipos_ocor_pck.dados_table_selecao_ocor;
bind_sql_valor_w		sql_pck.t_dado_bind;
-- Servi�os da tabela de sele��o que n�o est�o habilitados para o prestador quando a valida��o estiver sendo feita na importa��o do arquivo
cursor_w			sql_pck.t_cursor;
ds_sql_w			varchar2(4000);
ds_restricao_sub_w		varchar2(4000);		-- Restri��o do select usado no FROM
ds_restricao_w			varchar2(4000);		-- Restri��o do select do cursor com TABLE FUNCTION
ds_tabelas_w			varchar2(4000);
ds_nr_seq_prestador_w		varchar2(50);
ds_nr_seq_tipo_prestador_w	varchar2(50);
ds_cd_prestador_princ_w		varchar2(50);
ds_nr_seq_tipo_atendimento_w	varchar2(50);
ds_ie_internado_w		varchar2(50);
ds_nr_seq_prestador_exec_w	varchar2(50);
			
-- Informa��es da regra de valida��o dos procedimentos n�o habilitados.
cursor C02 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
		a.ie_valida_procedimento,
		a.ie_tipo_prestador
	from	pls_oc_cta_val_serv_prest a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

begin
-- S� ir� executar qualquer copnsulta e\ou valida��o quando existir informa��o v�lida da regra.
if	(dados_regra_p.nr_sequencia is not null) then

	-- gerencia a atualiza��o da tabela TM
	pls_gerencia_upd_obj_pck.atualizar_objetos('tasy', 'PLS_OC_CTA_TRATAR_VAL_52', 'PLS_GRUPO_SERVICO_TM');
	
	-- Informa��es da regra de valida��o da carteira.
	for	r_C02_w in C02(dados_regra_p.nr_sequencia) loop
	
		-- Se tiver informa��o na tabela e a informa��o for diferente de nenhuma ser� verificado, caso contr�rio sai da rotina e nem abre as informa��es das contas.
		if	(r_C02_w.ie_valida_procedimento = 'S') and
			(r_C02_w.ie_tipo_prestador is not null) then
			
			-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
			pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
			
			ds_restricao_sub_w:=	'';
			ds_restricao_w:=	'';
			
			
			if	(r_C02_w.ie_tipo_prestador = 'P') then
				ds_tabelas_w :=			'		pls_conta_proc_ocor_v		a ';
				if	(dados_regra_p.ie_evento = 'IMP') then
					ds_nr_seq_prestador_w :=		'a.nr_seq_prestador_imp_prot';
				else
					ds_nr_seq_prestador_w:=		'a.nr_seq_prestador_prot';
				end if;
				
			elsif	(r_C02_w.ie_tipo_prestador = 'C') then
				ds_tabelas_w:=			'		pls_conta_proc_ocor_v		a ';
				if	(dados_regra_p.ie_evento = 'IMP') then
					ds_nr_seq_prestador_w:=		'a.nr_seq_prestador_exec_imp_ref';
				else 
					ds_nr_seq_prestador_w:=		'a.nr_seq_prestador_exec';
				end if;
			elsif	(r_C02_w.ie_tipo_prestador = 'R') then
				ds_tabelas_w:=			'		pls_conta_proc_ocor_v		a, '		|| pls_tipos_ocor_pck.enter_w ||
								'		pls_proc_participante_ocor_v 	partic ';
				ds_restricao_sub_w := ds_restricao_sub_w ||
								'	and	a.nr_sequencia		= partic.nr_seq_conta_proc ';
				ds_nr_seq_prestador_w:=		'			partic.nr_seq_prestador';
			end if;
			if	(dados_regra_p.ie_evento = 'IMP') then
				ds_ie_internado_w:=		'a.ie_internado_imp';
				ds_nr_seq_prestador_exec_w:=	'a.nr_seq_prestador_exec_imp_ref';
				ds_nr_seq_tipo_atendimento_w:=	'a.nr_seq_tipo_atend_imp';
				ds_cd_prestador_princ_w:=	'a.cd_prestador_exec_imp';
				ds_nr_seq_tipo_prestador_w:=	'a.nr_seq_tipo_prest_exec_imp';
			else
				ds_ie_internado_w:=		'a.ie_internado';
				ds_nr_seq_prestador_exec_w:=	'a.nr_seq_prestador_exec';
				ds_nr_seq_tipo_atendimento_w:=	'a.nr_seq_tipo_atendimento';
				ds_cd_prestador_princ_w:=	'a.cd_prestador_exec';
				ds_nr_seq_tipo_prestador_w:=	'a.nr_seq_tipo_prest_exec';
			end if;

			ds_restricao_w := ds_restricao_w ||
					'	where	(select	count(1) ' 			|| pls_tipos_ocor_pck.enter_w ||
					'		from	table (pls_grupos_aux_pck.obter_proc_lib_prest ( ' || pls_tipos_ocor_pck.enter_w ||
					'			a.nr_seq_prestador,'				|| pls_tipos_ocor_pck.enter_w ||
					'			a.dt_procedimento,' 				|| pls_tipos_ocor_pck.enter_w ||
					'			a.ie_tipo_guia, ' 				|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_nr_seq_prestador_exec_w	|| ','		|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_nr_seq_tipo_prestador_w	|| ','		|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_cd_prestador_princ_w || ','		|| pls_tipos_ocor_pck.enter_w ||
					'			a.ie_carater_internacao,'			|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_nr_seq_tipo_atendimento_w || ','	|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_ie_internado_w || ','			|| pls_tipos_ocor_pck.enter_w ||
					'			a.nr_seq_plano, ' 				|| pls_tipos_ocor_pck.enter_w ||
					'			a.ie_origem_proced, ' 				|| pls_tipos_ocor_pck.enter_w ||
					'			a.cd_procedimento,   '				|| pls_tipos_ocor_pck.enter_w ||
					'			(	select  max(x.nr_seq_tipo_prest_prot) '	|| pls_tipos_ocor_pck.enter_w ||
					'				from    pls_conta_v x '			|| pls_tipos_ocor_pck.enter_w ||
					'				where	x.nr_sequencia = a.nr_seq_conta), '	|| pls_tipos_ocor_pck.enter_w ||
					'			a.cd_especialidade,   '				|| pls_tipos_ocor_pck.enter_w ||
					'			nvl(a.nr_seq_cbo_saude_conta, a.nr_seq_cbo_saude)   '				|| pls_tipos_ocor_pck.enter_w ||
					'				)) pm) = 0 '	|| pls_tipos_ocor_pck.enter_w;
								
			ds_sql_w := 	'	select 		a.nr_sequencia,'			|| pls_tipos_ocor_pck.enter_w ||
					'			a.ie_valido,'				|| pls_tipos_ocor_pck.enter_w ||
					'			a.ds_observacao'			|| pls_tipos_ocor_pck.enter_w ||
					'	from		('					|| pls_tipos_ocor_pck.enter_w ||
					'	select 		x.nr_sequencia, ' 			|| pls_tipos_ocor_pck.enter_w ||
					'			''S'' ie_valido, '			|| pls_tipos_ocor_pck.enter_w ||
					'			''O procedimento informado n�o est� habilitado para o prestador '' || '	|| pls_tipos_ocor_pck.enter_w ||
					'				decode('''|| r_C02_w.ie_tipo_prestador ||''',''P'',''do protocolo:'',''C'',''executor da conta:'',''participante:'') || '	|| pls_tipos_ocor_pck.enter_w ||
					'				'' '' || '			|| pls_tipos_ocor_pck.enter_w ||
					'				pls_obter_dados_prestador(' || ds_nr_seq_prestador_w || ', ''N'') || ' 		|| pls_tipos_ocor_pck.enter_w ||
					'				decode('''|| r_C02_w.ie_tipo_prestador ||''',''C'',''.'',''. Prestador executor: '' || pls_obter_dados_prestador(' || ds_nr_seq_prestador_exec_w || ', ''N'') || ''.'') ds_observacao, ' || pls_tipos_ocor_pck.enter_w ||
					'			' || ds_nr_seq_prestador_w || ' nr_seq_prestador,'		|| pls_tipos_ocor_pck.enter_w ||
					'			a.dt_procedimento,' 		|| pls_tipos_ocor_pck.enter_w ||
					'			a.ie_tipo_guia, ' 				|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_nr_seq_prestador_exec_w	|| ','		|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_nr_seq_tipo_prestador_w	|| ','		|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_cd_prestador_princ_w || ','		|| pls_tipos_ocor_pck.enter_w ||
					'			a.ie_carater_internacao,'			|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_nr_seq_tipo_atendimento_w || ','	|| pls_tipos_ocor_pck.enter_w ||
					'			' || ds_ie_internado_w || ','			|| pls_tipos_ocor_pck.enter_w ||
					'			a.nr_seq_plano, ' 				|| pls_tipos_ocor_pck.enter_w ||
					'			a.ie_origem_proced, ' 				|| pls_tipos_ocor_pck.enter_w ||
					'			a.cd_procedimento, '	 		|| pls_tipos_ocor_pck.enter_w ||
					'			a.nr_seq_conta,	'			|| pls_tipos_ocor_pck.enter_w ||	
					'			a.nr_seq_cbo_saude nr_seq_cbo_saude_conta, '|| pls_tipos_ocor_pck.enter_w ||	
					'		(select	max(nr_seq_cbo_saude) '|| pls_tipos_ocor_pck.enter_w ||
					'		from	pls_proc_participante '|| pls_tipos_ocor_pck.enter_w ||
					'		where	cd_medico 		= pls_obter_medico_executor(a.nr_sequencia, ''P'') '|| pls_tipos_ocor_pck.enter_w ||
					'		and	nr_seq_conta_proc 	= a.nr_sequencia) nr_seq_cbo_saude, '|| pls_tipos_ocor_pck.enter_w ||	
					'			substr(pls_obter_espec_prest_serv_hab(a.nr_seq_prestador_exec, a.cd_pessoa_fisica_prest),1,4000) cd_especialidade	'			|| pls_tipos_ocor_pck.enter_w ||
					'	from	pls_oc_cta_selecao_ocor_v	x, ' 		|| pls_tipos_ocor_pck.enter_w ||
					ds_tabelas_w							|| pls_tipos_ocor_pck.enter_w ||
					'	where	x.ie_valido		= ''S'' '		|| pls_tipos_ocor_pck.enter_w ||
					'	and	x.nr_id_transacao	= :nr_id_transacao_pc '	|| pls_tipos_ocor_pck.enter_w ||
					'	and	a.nr_sequencia 		= x.nr_seq_conta_proc '	|| pls_tipos_ocor_pck.enter_w ||
					'	and	a.cd_procedimento 	is not null '	|| pls_tipos_ocor_pck.enter_w ||
					'	and	a.ie_origem_proced	is not null '		|| pls_tipos_ocor_pck.enter_w ||
					'	and	' || ds_nr_seq_prestador_w || ' is not null '	|| pls_tipos_ocor_pck.enter_w ||
					ds_restricao_sub_w						|| pls_tipos_ocor_pck.enter_w ||
					'	) a  ' 							|| pls_tipos_ocor_pck.enter_w ||
					ds_restricao_w;
			
			sql_pck.bind_variable(':nr_id_transacao_pc', nr_id_transacao_p, bind_sql_valor_w);
			
			begin
			
				cursor_w := sql_pck.executa_sql_cursor(ds_sql_w, bind_sql_valor_w);
				pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);

				loop
					fetch 	cursor_w bulk collect 
					into	dados_tb_selecao_w.nr_seq_selecao, dados_tb_selecao_w.ie_valido, dados_tb_selecao_w.ds_observacao
					limit pls_util_cta_pck.qt_registro_transacao_w;
					exit when dados_tb_selecao_w.nr_seq_selecao.count = 0;
					
					pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
											'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);									

				end loop; 

				close cursor_w;			

			exception
			when others then
				if (cursor_w%isopen) then
					close cursor_w;
				end if;
				pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p, ds_sql_w, nr_id_transacao_p, nm_usuario_p);
			end;
			
			-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
			pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
		end if;
	end loop; -- C02
end if;

end pls_oc_cta_tratar_val_52;
/
