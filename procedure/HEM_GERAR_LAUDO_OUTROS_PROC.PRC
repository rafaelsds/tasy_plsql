create or replace
procedure hem_gerar_laudo_outros_proc( 	nr_sequencia_p 		Number,
				nm_usuario_p		Varchar2) is 
				
ds_sql_w			Varchar2(20000);
c001				Integer;
retorno_w			Number(5);

begin
if 	(nvl(nr_sequencia_p,0) > 0) then
	
	ds_sql_w	:= ' update hem_proc set ds_laudo = :DS_LAUDO where nr_sequencia  = '||nr_sequencia_p;
	
	C001 := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(C001, ds_sql_w, dbms_sql.Native);
	DBMS_SQL.BIND_VARIABLE(C001, 'DS_LAUDO', '{\rtf1\ansi\ansicpg1252\deff0\deflang1046{\fonttbl{\f0\fnil\fcharset0 Arial;}}
{\colortbl ;\red0\green0\blue0;}
\viewkind4\uc1\pard\cf1\b\fs24 
\par }');
	retorno_w := DBMS_SQL.EXECUTE(c001);
	DBMS_SQL.CLOSE_CURSOR(C001);
end if;
	
commit;

end hem_gerar_laudo_outros_proc;
/