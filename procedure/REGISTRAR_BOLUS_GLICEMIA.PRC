create or replace
procedure registrar_bolus_glicemia		(nr_seq_cig_p		number,
						qt_bolus_adm_p	number,
						ds_observacao_p	varchar2,
						nm_usuario_p		varchar2) is

begin
if	(nr_seq_cig_p		is not null) and
	(qt_bolus_adm_p	is not null) then
	/* atualizar bolus */
	update	atendimento_cig
	set	qt_bolus_adm	= qt_bolus_adm_p,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_cig_p;

	/* gerar evento */
	gerar_alteracao_cig(nr_seq_cig_p, 1, ds_observacao_p, nm_usuario_p);
end if;

commit;

end registrar_bolus_glicemia;
/