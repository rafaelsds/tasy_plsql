create or replace 
procedure proj_gerar_ata_projeto (	nr_seq_proj_p	number,
									cd_consultor_p	varchar2,
									ds_ata_p		varchar2,
									nm_usuario_p	varchar2) is
									
nr_seq_cliente_w	number(10);
									
begin
	
	select	max(nr_seq_cliente)
	into	nr_seq_cliente_w
	from	proj_projeto
	where	nr_sequencia = nr_seq_proj_p;
	
	insert into proj_ata(	NR_SEQUENCIA,
							CD_ESTABELECIMENTO,
							CD_CONSULTOR,
							DS_ATA,
							NR_SEQ_PROJETO,
							NM_USUARIO,
							DT_ATA,
							DT_ATUALIZACAO,
							IE_TIPO_ATA,
							NR_SEQ_CLIENTE,
							IE_SITUACAO)
				values(		proj_ata_seq.nextval,
							wheb_usuario_pck.get_cd_estabelecimento(),
							cd_consultor_p,
							ds_ata_p,
							nr_seq_proj_p,
							nm_usuario_p,
							sysdate,
							sysdate,
							'P',
							nr_seq_cliente_w,
							'A');
						
	commit;						

end proj_gerar_ata_projeto;
/

