create or replace
procedure lab_desfazer_digitacao_result (nr_seq_resultado_p 	number,
					nr_seq_prescr_p		number,
					nm_usuario_p		varchar2) is

ds_resultado_w		exame_lab_result_item.ds_resultado%type;
qt_resultado_w		number(15,4);
pr_resultado_w		number(9,4);
nr_prescricao_w		number(10);
nr_seq_result_item_w	number(10);
nr_seq_exame_w		number(10);
nr_seq_result_antib_w	number(10);
ie_segunda_dig_w		varchar2(1);
cd_prescr_proc_compl_seq_w  number;

cursor c01 is
	select 	ds_resultado,
		qt_resultado,
		pr_resultado,
		b.nr_prescricao,
		a.nr_sequencia,
		a.nr_seq_exame
	from	exame_lab_result_item a,
		exame_lab_resultado b,
		prescr_procedimento c
	where	a.nr_seq_resultado = b.nr_seq_resultado
	and	b.nr_prescricao = c.nr_prescricao
	and	a.nr_seq_prescr = c.nr_sequencia
	and	obter_data_aprov_lab(b.nr_prescricao, a.nr_seq_prescr) is null
	and	c.ie_status_atend < 35
	and	b.nr_seq_resultado = nr_seq_resultado_p
	and	a.nr_seq_prescr	= nr_seq_prescr_p;
	
cursor c02 is
	select	a.nr_sequencia
	from	exame_lab_result_antib a
	where	a.nr_seq_resultado = nr_Seq_resultado_p
	and	a.nr_seq_result_item = nr_seq_result_item_w;
	
begin

open c01;
loop
fetch c01 into
		ds_resultado_w,
		qt_resultado_w,
		pr_resultado_w,
		nr_prescricao_w,
		nr_seq_result_item_w,
		nr_seq_exame_w;
exit when c01%notfound;

	--insert into log  tasy (dt_atualizacao,nm_usuario,cd_log,ds_log)
	--values (sysdate,nm_usuario_p,21457,'Prescrição: ' || nr_prescricao_w || ' Seq. Prescr.: ' || nr_seq_prescr_p ||' Exame: ' || nr_seq_exame_w ||'Result.: Qt ' ||
--qt_resultado_w || ' Pr ' || qt_resultado_w || ' Ds ' || ds_resultado_w);
	
	select 	decode(count(*), 0, 'N', 'S')
	into 	ie_segunda_dig_w
	from	exame_lab_resultado a,
			exame_lab_result_item b
	where 	a.nr_seq_resultado = b.nr_seq_resultado
	and		a.nr_seq_resultado = nr_seq_resultado_p
	and		b.nr_seq_prescr = nr_seq_prescr_p
	and		b.nr_sequencia = nr_seq_result_item_w
	and		b.nm_usuario_seg_dig is not null
	and		exists (select 	1 
					from 	exame_lab_result_dig 
					where	nr_seq_resultado = b.nr_seq_resultado 
					and 	nr_seq_prescr = b.nr_seq_prescr);

	if(ie_segunda_dig_w = 'N') then 
		update exame_lab_result_item
		set	qt_resultado   = null,
			ds_resultado   = null,
			pr_resultado   = null,
			nm_usuario	= nm_usuario_p,
			ie_status	= null,
			nm_usuario_prim_dig = null,
			ds_observacao = null,
			ds_obs_curta = null
		where nr_sequencia = nr_seq_result_item_w
		and    nr_seq_prescr   = nr_seq_prescr_p
		and   nr_seq_resultado = nr_seq_resultado_p;
	else 
		update exame_lab_result_item
		set	qt_resultado = null,
			ds_resultado = null,
			pr_resultado = null,
			nm_usuario = nm_usuario_p,
			nm_usuario_seg_dig = null,
			ds_observacao = null,
			ds_obs_curta = null
		where nr_sequencia = nr_seq_result_item_w
		and   nr_seq_prescr = nr_seq_prescr_p
		and   nr_seq_resultado = nr_seq_resultado_p;
	end if;

  select max(nr_seq_proc_compl)
  into cd_prescr_proc_compl_seq_w
  from PRESCR_PROCEDIMENTO pp
  where pp.nr_prescricao = nr_prescricao_w
  and pp.nr_sequencia = nr_seq_prescr_p;
 
  if (cd_prescr_proc_compl_seq_w is not null) then
    update PRESCR_PROCEDIMENTO_COMPL ppc 
    set ppc.ie_comunic_crit_ant = ppc.ie_comunicacao_critico,
    ppc.ie_comunicacao_critico = null,
    ppc.ie_critico = 'N',
    ppc.nm_usuario_critico = null
    where ppc.nr_sequencia = cd_prescr_proc_compl_seq_w;
  end if;

	open c02;
	loop
	fetch c02 into
		nr_seq_result_antib_w;
	exit when c02%notfound;
		
		delete exame_lab_result_antib
		where 	nr_sequencia 		= nr_seq_result_antib_w
		and   	nr_seq_result_item	= nr_seq_result_item_w
		and   	nr_seq_resultado 	= nr_seq_resultado_p;
	
	end loop;
	close c02;

end loop;
close c01;
end;
/
