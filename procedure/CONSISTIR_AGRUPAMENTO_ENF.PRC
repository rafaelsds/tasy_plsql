create or replace
procedure Consistir_agrupamento_enf(nr_prescricao_p number) 
				 as
					
ds_horarios_w		varchar2(2000);
ds_horarios_ww		varchar2(2000);
nr_agrupamento_w	number(7,1);
					
Cursor C01 is
	select	distinct 
		nr_agrupamento
	from	prescr_material
	where	nr_prescricao =  nr_prescricao_p
	and	ie_agrupador in (1,5)
	and	nr_sequencia_solucao is null
	and	nr_sequencia_dieta is null
	and	nr_sequencia_diluicao is null
	and     dt_suspensao is null
	and     nvl(ie_administrar,'S') = 'S'
	and	ie_agrupador in (1,5)
	and	obter_tipo_material(cd_material, 'C') in (2,3);

Cursor C02 is
	select	ds_horarios
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and	nr_agrupamento = nr_agrupamento_w;
	
begin

if	(nr_prescricao_p is not null) then 
	open C01;
	loop
	fetch C01 into	
		nr_agrupamento_w;
	exit when C01%notfound;
		begin
		ds_horarios_ww := null;
		open C02;
		loop
		fetch C02 into	
			ds_horarios_w;
		exit when C02%notfound;
			begin
			if 	(ds_horarios_ww is not null) and
				(ds_horarios_ww <> ds_horarios_w)then 
				
				--N�o � permitido liberar a prescri��o que tenha itens agrupados com hor�rios diferentes!'||'#@#@'); 
				Wheb_mensagem_pck.exibir_mensagem_abort(261463);
			end if;
			ds_horarios_ww := ds_horarios_w;
			end;
		end loop;
		close C02;	
		
		end;
	end loop;
	close C01;	

end if;



end Consistir_agrupamento_enf;
/