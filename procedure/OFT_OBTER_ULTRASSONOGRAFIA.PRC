create or replace
PROCEDURE oft_obter_ultrassonografia (	nr_seq_consulta_p			number,
													nr_seq_consulta_form_p	number,
													cd_pessoa_fisica_p		varchar2,
													ie_opcao_p					varchar2,
													vListaUltrassonografia		in out strRecTypeFormOft) IS

dt_exame_w					oft_ultrassonografia.dt_registro%TYPE;
ds_observacao_od_w		oft_ultrassonografia.ds_observacao_od%TYPE;
ds_observacao_oe_w		oft_ultrassonografia.ds_observacao_oe%TYPE;
ds_observacao_w			oft_ultrassonografia.ds_observacao%TYPE;
cd_profissional_w			oft_ultrassonografia.cd_profissional%TYPE;
dt_liberacao_w				date;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.get_cd_estabelecimento;
nm_usuario_w				usuario.nm_usuario%TYPE := wheb_usuario_pck.get_nm_usuario;
ds_erro_w					varchar2(4000);

cursor	ultrassonografia_form is
	select	a.*
	from		oft_ultrassonografia a,
				oft_consulta_formulario b
	where		a.nr_seq_consulta_form 	=	b.nr_sequencia
	and		a.nr_seq_consulta_form 	=	nr_seq_consulta_form_p
	and		a.nr_seq_consulta			=	nr_seq_consulta_p
	and		((a.dt_liberacao is not null) or (a.nm_usuario = nm_usuario_w))
	and		((a.dt_inativacao is null) or (b.dt_inativacao is not null))
	order by dt_registro;
	
cursor	ultrassonografia_paciente is
	select	a.*
	from		oft_ultrassonografia a,
				oft_consulta b
	where		a.nr_seq_consulta		=	b.nr_sequencia
	and		b.cd_pessoa_fisica	=	cd_pessoa_fisica_p
	and		a.dt_liberacao 		is not null
	and		a.dt_inativacao 		is null
	and		b.nr_sequencia 		<> nr_seq_consulta_p
	order by dt_registro;


BEGIN
begin

if	(nvl(nr_seq_consulta_p,0) > 0) and (vListaUltrassonografia.count > 0) then
	if	(ie_opcao_p = 'F') then
		FOR c_ultrassonografia IN ultrassonografia_form LOOP
			begin
			dt_exame_w				:=	c_ultrassonografia.dt_registro;
			ds_observacao_od_w	:=	c_ultrassonografia.ds_observacao_od;
			ds_observacao_oe_w	:=	c_ultrassonografia.ds_observacao_oe;
			ds_observacao_w		:=	c_ultrassonografia.ds_observacao;
			dt_liberacao_w			:=	c_ultrassonografia.dt_liberacao;
			cd_profissional_w		:=	c_ultrassonografia.cd_profissional;
			end;
		end loop;	
	else
		FOR c_ultrassonografia IN ultrassonografia_paciente LOOP
			begin
			ds_observacao_od_w	:=	c_ultrassonografia.ds_observacao_od;
			ds_observacao_oe_w	:=	c_ultrassonografia.ds_observacao_oe;
			ds_observacao_w		:=	c_ultrassonografia.ds_observacao;
			cd_profissional_w		:=	obter_pf_usuario(nm_usuario_w,'C');
			dt_exame_w				:= sysdate;
			end;
		end loop;	
	end if;
	FOR i IN 1..vListaUltrassonografia.COUNT LOOP
		BEGIN
		if	(ie_opcao_p = 'F') or (vListaUltrassonografia(i).ie_obter_resultado = 'S') then
			vListaUltrassonografia(i).dt_liberacao	:= dt_liberacao_w;
			CASE UPPER(vListaUltrassonografia(i).nm_campo)
				WHEN 'DT_REGISTRO' THEN
					vListaUltrassonografia(i).dt_valor	:= dt_exame_w;
				WHEN 'CD_PROFISSIONAL' THEN
					vListaUltrassonografia(i).ds_valor	:= cd_profissional_w;
				WHEN 'DS_OBSERVACAO_OD' THEN
					vListaUltrassonografia(i).ds_valor	:= ds_observacao_od_w;
				WHEN 'DS_OBSERVACAO_OE' THEN
					vListaUltrassonografia(i).ds_valor	:= ds_observacao_oe_w;
				WHEN 'DS_OBSERVACAO' THEN
					vListaUltrassonografia(i).ds_valor	:= ds_observacao_w;
				else
					null;	
			END CASE;
		end if;	
	END;
	END LOOP;
END IF;

exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,4000);
end;

END oft_obter_ultrassonografia;
/