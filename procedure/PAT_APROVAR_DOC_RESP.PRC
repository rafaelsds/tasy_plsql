create or replace procedure pat_aprovar_doc_resp(
	nr_sequencia_p		number,
	nr_seq_doc_transf_p	number,
	cd_estabelecimento_p	number,
	ie_opcao_p		varchar2,
nm_usuario_p		Varchar2) is

qt_registro_w					number(10);
dt_aprovacao_w					date;
ds_observacao_w					pat_doc_transferencia.ds_observacao%type;
ds_assunto_w					pat_regra_envio_aviso.ds_assunto%type;
ds_mensagem_w					pat_regra_envio_aviso.ds_mensagem_padrao%type;
ds_email_origem_w					varchar2(255);
ds_email_destino_w					varchar2(255);
ds_email_adicional_w					varchar2(255);
ds_usuario_origem_w				varchar2(255);
ds_estabelecimento_w				varchar2(255);
ds_cargo_usuario_w				cargo.ds_cargo%type;
nr_ramal_w					usuario.nr_ramal%type;
ds_email_w					usuario.ds_email%type;		
dt_documento_w					pat_doc_transferencia.dt_documento%type;
nr_documento_w					pat_doc_transferencia.nr_sequencia%type;
cd_pessoa_fisica_w				usuario.cd_pessoa_fisica%type;
nm_pessoa_fisica_w				pessoa_fisica.nm_pessoa_fisica%type;
nm_pessoa_fisica_dest_w				pessoa_fisica.nm_pessoa_fisica%type;
nm_usuario_destino_w				usuario.nm_usuario%type;
cd_pessoa_fisica_destino_w				pat_doc_transf_resp.cd_pessoa_fisica%type;
ie_comunic_w					pat_regra_envio_aviso.ie_forma_comunicacao%type;
nr_ordem_atual_w					pat_doc_transf_resp.nr_ordem%type;
ie_tipo_aprovador_w				pat_doc_transf_resp.ie_tipo_aprovador%type;
qt_aprov_origem_pend_w				number(10);

Cursor C01 is
select	ds_mensagem_padrao,
ds_assunto,	
ie_forma_comunicacao,
ds_email_adicional
from 	pat_regra_envio_aviso
where	ie_tipo_aviso  = 2;

begin
	
	
	if 	(nr_sequencia_p is not null) then
		
		select	a.ie_tipo_aprovador
		into	ie_tipo_aprovador_w
		from	pat_doc_transf_resp a
		where	a.nr_sequencia	= nr_sequencia_p;
		
		if	(ie_tipo_aprovador_w = 'D') then
			
			select	count(nr_sequencia)
			into	qt_aprov_origem_pend_w
			from	pat_doc_transf_resp a
			where	a.nr_seq_doc_transf	= nr_seq_doc_transf_p
			and	a.ie_tipo_aprovador	= 'O'
			and	a.dt_aprovacao is null;
			
			if	(qt_aprov_origem_pend_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(291474);
			end if;
		end if;
		
		update	pat_doc_transf_resp
		set	dt_aprovacao	= decode(ie_opcao_p,'A',sysdate,'E',null),
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_sequencia_p;
	end if;
	
	select	count(nr_seq_doc_transf)
	into	qt_registro_w
	from	pat_doc_transf_resp
	where	dt_aprovacao is null
	and	nr_seq_doc_transf = nr_seq_doc_transf_p;
	
	if	(qt_registro_w = 0) then
		pat_aprovar_doc_transf(nr_seq_doc_transf_p, cd_estabelecimento_p, ie_opcao_p, nm_usuario_p, sysdate);
		
	end if;
	
	commit;
	if	(ie_opcao_p = 'A') then
		
		select	count(nr_sequencia)
		into	qt_registro_w
		from	pat_regra_envio_aviso
		where 	ie_tipo_aviso = 2;
		
		if	(qt_registro_w > 0) then
			begin
				select	ds_observacao,
				dt_documento,
				nr_sequencia
				into	ds_observacao_w,
				dt_documento_w,
				nr_documento_w
				from	pat_doc_transferencia
				where	nr_sequencia = nr_seq_doc_transf_p;	
				
				select	nr_ramal,
				ds_email,
				cd_pessoa_fisica
				into	nr_ramal_w,
				ds_email_w,
				cd_pessoa_fisica_w
				from	usuario
				where	nm_usuario = nm_usuario_p;
				
				begin
					select	substr(obter_desc_cargo(cd_cargo),1,100) ds_cargo,
					SUBSTR(OBTER_NOME_PF(CD_PESSOA_FISICA),0,60)
					into	ds_cargo_usuario_w,
					nm_pessoa_fisica_w
					from	pessoa_fisica
					where	cd_pessoa_fisica = cd_pessoa_fisica_w;
					exception
					when others then
					ds_cargo_usuario_w := '';
					nm_pessoa_fisica_w := '';
				end;
				
				select	min(nr_ordem)
				into	nr_ordem_atual_w
				from	pat_doc_transf_resp y
				where	nr_sequencia = nr_sequencia_p;	
				
				begin		
					select	substr(obter_dados_pf_pj(cd_pessoa_fisica,null,'M'),1,255),
					cd_pessoa_fisica
					into	ds_email_destino_w,
					cd_pessoa_fisica_destino_w
					from	pat_doc_transf_resp a
					where	a.nr_seq_doc_transf = nr_seq_doc_transf_p
					and 	a.nr_ordem = (	select min(y.nr_ordem)
						from pat_doc_transf_resp y
						where y.nr_seq_doc_transf = nr_seq_doc_transf_p
					and y.nr_ordem > nr_ordem_atual_w)
					and   	dt_aprovacao is null;
					exception
					when others then
					ds_email_destino_w := '';
					cd_pessoa_fisica_destino_w := '';
				end;
				
				if	(cd_pessoa_fisica_destino_w is not null) then		
					select	max(nm_usuario)
					into	nm_usuario_destino_w
					from	usuario
					where	cd_pessoa_fisica = cd_pessoa_fisica_destino_w;
					
					begin
						select	SUBSTR(OBTER_NOME_PF(CD_PESSOA_FISICA),0,60)
						into	nm_pessoa_fisica_dest_w
						from	pessoa_fisica
						where	cd_pessoa_fisica	= cd_pessoa_fisica_destino_w;
						exception when others then
						nm_pessoa_fisica_dest_w	:= '';
					end;
					
					open C01;
					loop
						
						fetch C01 into	
						ds_mensagem_w,
						ds_assunto_w,
						ie_comunic_w,
						ds_email_adicional_w;
						exit when C01%notfound;
						begin
							
							select  substr(obter_nome_estab(cd_estabelecimento_p),1,255)
							into	ds_estabelecimento_w
							from	dual;
							
							/* ASSUNTO */
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@data',to_char(dt_documento_w,'dd/mm/yyyy')),1,2000);
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@doc',to_char(nr_documento_w)),1,2000);
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@observacao',nvl(ds_observacao_w,'')),1,2000);
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@cargo',nvl(ds_cargo_usuario_w,'')),1,2000);
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ramal',nvl(nr_ramal_w,'')),1,2000);
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@email',nvl(ds_email_w,'')),1,2000);
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nm_usuario',nvl(nm_pessoa_fisica_w,'')),1,2000);
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@usuario_dest',nvl(nm_usuario_destino_w,'')),1,2000);	
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nm_usuario_dest',nvl(nm_pessoa_fisica_dest_w,'')),1,2000);
							ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_estabelecimento',nvl(ds_estabelecimento_w,'')),1,2000);
							/* TEXTO DA MENSAGEM */
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@data',to_char(dt_documento_w,'dd/mm/yyyy')),1,2000);
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@doc',to_char(nr_documento_w)),1,2000);
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@observacao',nvl(ds_observacao_w,'')),1,2000);
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@cargo',nvl(ds_cargo_usuario_w,'')),1,2000);
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@ramal',nvl(nr_ramal_w,'')),1,2000);
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@email',nvl(ds_email_w,'')),1,2000);
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@nm_usuario',nvl(nm_pessoa_fisica_w,'')),1,2000);
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@usuario_dest',nvl(nm_usuario_destino_w,'')),1,2000);	
							ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@nm_usuario_dest',nvl(nm_pessoa_fisica_dest_w,'')),1,2000);	
							--ds_assunto_w  := substr(replace_macro(ds_mensagem_w,'@ds_estabelecimento',nvl(ds_estabelecimento_w,'')),1,2000);
							ds_mensagem_w  := substr(replace_macro(ds_mensagem_w,'@ds_estabelecimento',nvl(ds_estabelecimento_w,'')),1,2000);
							
							if(ds_email_adicional_w is not null) then
								if(ds_email_destino_w is not null) then
									ds_email_destino_w := concat(ds_email_destino_w, concat('; ', ds_email_adicional_w));
									else
									ds_email_destino_w := ds_email_adicional_w;
								end if;
							end if;
							if	(ie_comunic_w = 'EM') then
								enviar_email(ds_assunto_w, ds_mensagem_w, ds_email_w, ds_email_destino_w, nm_usuario_p, 'M', null);
								elsif	(ie_comunic_w = 'CI') then		
								gerar_comunic_padrao (sysdate,ds_assunto_w,ds_mensagem_w,nm_usuario_p,'N',nm_usuario_destino_w,'N','','','','',sysdate,'','');
								else 		
								enviar_email(ds_assunto_w, ds_mensagem_w, ds_email_w, ds_email_destino_w, nm_usuario_p, 'M', null);			
								gerar_comunic_padrao (sysdate,ds_assunto_w,ds_mensagem_w,nm_usuario_p,'N',nm_usuario_destino_w,'N','','','','',sysdate,'','');
							end if; 
						end;
					end loop;
					close C01;
				end if;
			end;
		end if; 
	end if;
	commit;
end pat_aprovar_doc_resp;
/