create or replace
procedure aprovar_idiomas_doc( nr_sequencia_p		number,
			       dt_aprovacao_p		date,
			       nm_usuario_p		varchar2) is 

nr_sequencia_w		number(10);
nm_usuario_partic_w	varchar2(15);

cursor c01 is
	select	nr_sequencia
	from	qua_documento
	where	nr_seq_superior = nr_sequencia_p;

cursor c02 is
	select	distinct
		b.nm_usuario
	from	usuario b,
		qua_doc_participante a
	where	b.cd_pessoa_fisica = a.cd_participante
	and	a.nr_seq_doc = nr_sequencia_p;	

begin

	open c01;
	loop
	fetch c01 into	
		nr_sequencia_w;
	exit when c01%notfound;
		begin
			update	qua_documento
			set	dt_atualizacao = sysdate, 
				nm_usuario = nm_usuario_p, 
				dt_aprovacao = dt_aprovacao_p,
				ie_status = 'D',
				nm_usuario_aprov = nm_usuario_p,
				dt_reprovacao = null
			where	nr_sequencia = nr_sequencia_w
			and	dt_validacao is not null;
			
			open c02;
			loop
			fetch c02 into	
				nm_usuario_partic_w;
			exit when c02%notfound;
				insert into qua_doc_log_acesso(
					nr_sequencia,
					nr_seq_doc,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					dt_acesso,
					dt_leitura,
					ie_log,
					cd_estabelecimento,
					cd_funcao)
				values(	qua_doc_log_acesso_seq.nextval,
					nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_partic_w,
					sysdate,
					sysdate,
					'C',
					wheb_usuario_pck.get_cd_estabelecimento,
					wheb_usuario_pck.get_cd_funcao);
			end loop;
			close c02;
		end;
	end loop;
	close c01;

commit;

end aprovar_idiomas_doc;
/