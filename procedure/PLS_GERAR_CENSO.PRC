create or replace
procedure pls_gerar_censo
			(	dt_inicial_p		Date,
				dt_final_p		Date,
				ie_informacao_p		Varchar2,
				ie_dimensao_p		Varchar2,
				ie_seg_dimensao_p	Varchar2,
				ie_tipo_contrato_p	varchar2,
				ie_benef_sca_p		varchar2,
				ie_param_01_p		varchar2,
				ie_param_02_p		varchar2,
				ie_param_03_p		varchar2,
				ie_param_04_p		varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar o censo visualizado na fun��o OPS - Gest�o de Operadoras / Censo
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Lembrar que esta procedure � chamada pela proceduere PLS_GERAR_CENSO_JS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */				
				
/*	IE_DIMENSAO_P (dom 2277)
	C =  Canal de venda
	F = Faixa etaria
	N = Contrato
	O = Operadora
	P = Produto
	R = Regulamenta��o
	S = Segmenta��o assistencial
	T = Tipo de contrata��o 
	U = Subestipulante
	TB = Tipo de benefici�rio
	SX = Sexo
	FM = Fator moderador
	TI = Titularidade
	*/
	
/*ie_seg_dimensao_p -> Segunda dimens�o*/
	
/*	IE_INFORMACAO_P (dom 2278)
	A = Benefici�rios ativos
	E = Exclus�o de vidas
	I = Inclus�o de vidas*/
	
/*	IE_TIPO_CONTRATO_P (dom 3770)
	CI = Contratos de interc�mbio
	CO = Contratos do plano de sa�de
*/	
	
ds_comando_w			varchar2(32000);
ds_comando_2_w			varchar2(32000);
ds_informacao_w			varchar2(255)	:= '';
ds_descricao_w			varchar2(255)	:= '';
ds_informacao_2_w		varchar2(255)	:= '';
ds_descricao_2_w		varchar2(255)	:= '';

dt_referencia_w			date;
dt_referencia_inicio_w		date;
dt_referencia_fim_w		date;

Cursor C01 is
	select	dt_mes
	from	mes_v
	where	dt_mes between dt_inicial_p and dt_final_p
	order by	dt_mes;

begin

Exec_sql_Dinamico('Tasy', 'truncate table w_pls_censo_geral');
Exec_sql_Dinamico('Tasy', 'delete from w_pls_censo_geral');

if	(ie_dimensao_p = 'C') then
	ds_informacao_w	:= '	b.nr_seq_vendedor_canal ';
	ds_descricao_w	:= '	substr(pls_obter_nomes_contrato(b.nr_seq_vendedor_canal,' || chr(39) || 'VC' || chr(39) || '),1,255) ';
elsif	(ie_dimensao_p = 'F') then
	ds_informacao_w	:= '	substr(pls_obter_faixa_etaria_padrao(b.nr_sequencia,' || chr(39) || 'I' || chr(39) || ',' || chr(39) || 'IG' || chr(39) || ',' || cd_estabelecimento_p ||'),1,100) ';
	ds_descricao_w	:= '	substr(pls_obter_faixa_etaria_padrao(b.nr_sequencia,' || chr(39) || 'I' || chr(39) || ',' || chr(39) || 'IG' || chr(39) || ',' || cd_estabelecimento_p ||'),1,100) ';
elsif	(ie_dimensao_p = 'N') then
	if	(ie_tipo_contrato_p	= 'CO') then
		ds_informacao_w	:= '	d.nr_contrato ';
		ds_descricao_w	:= '	d.nr_contrato || ' ||' '' -  '' '|| ' ||  substr(obter_nome_pf_pj(d.cd_pf_estipulante, d.cd_cgc_estipulante),1,200)  ';
	elsif	(ie_tipo_contrato_p	= 'CI') then
		ds_informacao_w	:= '	d.nr_sequencia ';
		ds_descricao_w	:= '	d.nr_sequencia || ' ||' '' -  '' '|| ' ||  substr(obter_nome_pf_pj(d.CD_PESSOA_FISICA, d.cd_cgc),1,200)  ';
	end if;	
elsif	(ie_dimensao_p = 'O') then
	ds_informacao_w	:= '	b.cd_estabelecimento ';	
	--ds_descricao_w	:= '	substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,100)';
	ds_descricao_w	:=	'substr((select	decode(x.ie_razao_fantasia,''F'',y.nm_fantasia,''E'',x.nm_fantasia_estab,y.ds_razao_social) ' ||
				'	from	estabelecimento x, ' ||
				'	pessoa_juridica y ' ||
				'	where	x.cd_cgc = y.cd_cgc ' ||
				'	and 	x.cd_estabelecimento = b.cd_estabelecimento),1,100)';
elsif	(ie_dimensao_p = 'P') then
	ds_informacao_w	:= '	c.nr_sequencia ';
	ds_descricao_w	:= '	c.ds_plano || '||' '' - C�d. ANS: '' '|| ' ||  c.nr_protocolo_ans ';
elsif	(ie_dimensao_p = 'R') then
	ds_informacao_w	:= '	c.ie_regulamentacao ';
	ds_descricao_w	:= '	substr(obter_valor_dominio(2157,c.ie_regulamentacao),1,100) ';
elsif	(ie_dimensao_p = 'S') then
	ds_informacao_w	:= '	c.ie_segmentacao ';
	ds_descricao_w	:= '	substr(obter_valor_dominio(1665,c.ie_segmentacao),1,100) ';
elsif	(ie_dimensao_p = 'T') then
	ds_informacao_w	:= '	c.ie_tipo_contratacao ';
	ds_descricao_w	:= '	substr(obter_valor_dominio(1666,c.ie_tipo_contratacao),1,100) ';
elsif	(ie_dimensao_p = 'U') then
	ds_informacao_w	:= '	b.nr_seq_subestipulante ';
	ds_descricao_w	:= '	substr(pls_obter_desc_subestipulante(b.nr_seq_subestipulante),1,255) ';
elsif	(ie_dimensao_p = 'TB') then
	if	(ie_tipo_contrato_p	= 'CO') then
		ds_informacao_w	:= '	d.ie_tipo_beneficiario ';
		ds_descricao_w	:= '	substr(obter_valor_dominio(1923,d.ie_tipo_beneficiario),1,100) ';
	elsif	(ie_tipo_contrato_p	= 'CI') then
		ds_informacao_w	:= '	null ';
		ds_descricao_w	:= '	chr(39) || chr(39)  ';
	end if;
elsif	(ie_dimensao_p = 'SX')	then
	ds_informacao_w	:= '	e.ie_sexo ';
	ds_descricao_w	:= '	substr(obter_valor_dominio(4,e.ie_sexo),1,255) ';
elsif	(ie_dimensao_p = 'FM')  then
	ds_informacao_w	:= '	c.ie_coparticipacao ';
	ds_descricao_w	:= '	decode(c.ie_coparticipacao,''S'',''Com participa��o'',''Sem participa��o'') ';
elsif	(ie_dimensao_p = 'TI')	then
	ds_informacao_w	:= '	decode(b.nr_seq_titular,null,''T'',''D'') ds_titularidade ';
	ds_descricao_w	:= '	decode(decode(b.nr_seq_titular,null,''T'',''D''),''T'',''Titular'',''Dependente'') ';
end if;

/*Criar segunda dimens�o*/
if	(ie_seg_dimensao_p = 'C') then
	ds_informacao_2_w	:= '	b.nr_seq_vendedor_canal ';
	ds_descricao_2_w	:= '	substr(pls_obter_nomes_contrato(b.nr_seq_vendedor_canal,' || chr(39) || 'VC' || chr(39) || '),1,255) ';
elsif	(ie_seg_dimensao_p = 'F') then
	ds_informacao_2_w	:= '	substr(pls_obter_faixa_etaria_padrao(b.nr_sequencia,' || chr(39) || 'I' || chr(39) || ',' || chr(39) || 'IG' || chr(39) || ',' || cd_estabelecimento_p ||'),1,100) ';
	ds_descricao_2_w	:= '	substr(pls_obter_faixa_etaria_padrao(b.nr_sequencia,' || chr(39) || 'I' || chr(39) || ',' || chr(39) || 'IG' || chr(39) || ',' || cd_estabelecimento_p ||'),1,100) ';
elsif	(ie_seg_dimensao_p = 'N') then
	if	(ie_tipo_contrato_p	= 'CO') then
		ds_informacao_2_w	:= '	d.nr_contrato ';
		ds_descricao_2_w	:= '	d.nr_contrato || ' ||' '' -  '' '|| ' ||  substr(obter_nome_pf_pj(d.cd_pf_estipulante, d.cd_cgc_estipulante),1,200)  ';
	elsif	(ie_tipo_contrato_p	= 'CI') then
		ds_informacao_2_w	:= '	d.nr_sequencia ';
		ds_descricao_2_w	:= '	d.nr_sequencia || ' ||' '' -  '' '|| ' ||  substr(obter_nome_pf_pj(d.CD_PESSOA_FISICA, d.cd_cgc),1,200)  ';
	end if;	
elsif	(ie_seg_dimensao_p = 'O') then
	ds_informacao_2_w	:= '	b.cd_estabelecimento ';	
	ds_descricao_2_w	:=	'substr((select	decode(x.ie_razao_fantasia,''F'',y.nm_fantasia,''E'',x.nm_fantasia_estab,y.ds_razao_social) ' ||
				'	from	estabelecimento x, ' ||
				'	pessoa_juridica y ' ||
				'	where	x.cd_cgc = y.cd_cgc ' ||
				'	and 	x.cd_estabelecimento = b.cd_estabelecimento),1,100)';
elsif	(ie_seg_dimensao_p = 'P') then
	ds_informacao_2_w	:= '	c.nr_sequencia ';
	ds_descricao_2_w	:= '	c.ds_plano || '||' '' - C�d. ANS: '' '|| ' ||  c.nr_protocolo_ans ';
elsif	(ie_seg_dimensao_p = 'R') then
	ds_informacao_2_w	:= '	c.ie_regulamentacao ';
	ds_descricao_2_w	:= '	substr(obter_valor_dominio(2157,c.ie_regulamentacao),1,100) ';
elsif	(ie_seg_dimensao_p = 'S') then
	ds_informacao_2_w	:= '	c.ie_segmentacao ';
	ds_descricao_2_w	:= '	substr(obter_valor_dominio(1665,c.ie_segmentacao),1,100) ';
elsif	(ie_seg_dimensao_p = 'T') then
	ds_informacao_2_w	:= '	c.ie_tipo_contratacao ';
	ds_descricao_2_w	:= '	substr(obter_valor_dominio(1666,c.ie_tipo_contratacao),1,100) ';
elsif	(ie_seg_dimensao_p = 'U') then
	ds_informacao_2_w	:= '	b.nr_seq_subestipulante ';
	ds_descricao_2_w	:= '	substr(pls_obter_desc_subestipulante(b.nr_seq_subestipulante),1,255) ';
elsif	(ie_seg_dimensao_p = 'TB') then
	if	(ie_tipo_contrato_p	= 'CO') then
		ds_informacao_2_w	:= '	d.ie_tipo_beneficiario ';
		ds_descricao_2_w	:= '	substr(obter_valor_dominio(1923,d.ie_tipo_beneficiario),1,100) ';
	elsif	(ie_tipo_contrato_p	= 'CI') then
		ds_informacao_2_w	:= '	null ';
		ds_descricao_2_w	:= '	chr(39) || chr(39)  ';
	end if;
elsif	(ie_seg_dimensao_p = 'SX')	then
	ds_informacao_2_w	:= '	e.ie_sexo ';
	ds_descricao_2_w	:= '	substr(obter_valor_dominio(4,e.ie_sexo),1,255) ';	
elsif	(ie_seg_dimensao_p = 'FM')	then
	ds_informacao_2_w	:= '	c.ie_coparticipacao ';
	ds_descricao_2_w	:= '	decode(c.ie_coparticipacao,''S'',''Com participa��o'',''Sem participa��o'') ';
elsif	(ie_seg_dimensao_p = 'TI')	then
	ds_informacao_2_w	:= '	decode(b.nr_seq_titular,null,''T'',''D'') ds_titularidade ';
	ds_descricao_2_w	:= '	decode(decode(b.nr_seq_titular,null,''T'',''D''),''T'',''Titular'',''Dependente'') ';
elsif	(ie_seg_dimensao_p is null) then
	ds_informacao_2_w	:= '0';
	ds_descricao_2_w	:= '0';	
end if;

open C01;
loop
fetch C01 into	
	dt_referencia_w;
exit when C01%notfound;
	begin
	
	dt_referencia_inicio_w	:= trunc(dt_referencia_w,'month');
	dt_referencia_fim_w	:= fim_mes(dt_referencia_w);
	if	(ie_tipo_contrato_p = 'CO') and (ie_informacao_p = 'A') then
	
		pls_gerar_w_benef_movto_mensal(dt_referencia_inicio_w,nm_usuario_p,cd_estabelecimento_p);
		if	(ie_benef_sca_p	= 'S') then		
			ds_comando_w	:=	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO, ' ||
						'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
						'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
						'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
						'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
												cd_estabelecimento_p ||', ' ||
						'						nvl(b.ie_tipo_segurado,'||'''x'''||' ), ' ||
						'						sysdate, ' ||
												chr(39) || nm_usuario_p || chr(39) || ', ' ||
												ds_informacao_w || ', ' ||
												ds_descricao_w || ', ' ||
												ds_informacao_2_w || ', ' ||
												ds_descricao_2_w || ', ' ||
												ds_informacao_w ||
						'					from	w_pls_benef_movto_mensal a, ' ||
						'						pls_segurado		b, ' ||
						'						pls_contrato		d, ' ||
						'						pls_plano		c, ' ||
						'						pessoa_fisica		e  ' ||
						'					where	a.nr_seq_segurado	= b.nr_sequencia ' ||
						'					and	b.nr_seq_contrato	= d.nr_sequencia ' ||
						'					and	b.nr_Seq_plano		= c.nr_sequencia ' ||
						'					and	e.cd_pessoa_fisica	= b.cd_pessoa_fisica ' ||
						'					and	a.dt_referencia		= to_date(' || chr(39) || dt_referencia_inicio_w || chr(39) || ') ' ||
						'					and	b.cd_estabelecimento	= ' || cd_estabelecimento_p || 
						'					and	exists (select	1 from pls_sca_vinculo h where h.nr_seq_segurado = b.nr_sequencia))' ;
		else
			ds_comando_w	:=	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO, ' ||
						'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
						'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
						'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
						'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
												cd_estabelecimento_p ||', ' ||
						'						nvl(b.ie_tipo_segurado,'||'''x'''||' ), ' ||
						'						sysdate, ' ||
												chr(39) || nm_usuario_p || chr(39) || ', ' ||
												ds_informacao_w || ', ' ||
												ds_descricao_w || ', ' ||
												ds_informacao_2_w || ', ' ||
												ds_descricao_2_w || ', ' ||
												ds_informacao_w ||
						'					from	w_pls_benef_movto_mensal a, ' ||
						'						pls_segurado		b,  ' ||
						'						pls_contrato		d,  ' ||
						'						pls_plano		c,  ' ||
						'						pessoa_fisica		e   ' ||
						'					where	a.nr_seq_segurado	= b.nr_sequencia '     ||
						'					and	b.nr_seq_contrato	= d.nr_sequencia '     ||
						'					and	b.nr_Seq_plano		= c.nr_sequencia '     ||
						'					and 	e.cd_pessoa_fisica  	= b.cd_pessoa_fisica'  ||
						'					and	a.dt_referencia		= to_date(' || chr(39) || dt_referencia_inicio_w || chr(39) || ') ' ||
						'					and	b.cd_estabelecimento	= ' || cd_estabelecimento_p || ' )' ;
		end if;		
	elsif	(ie_tipo_contrato_p = 'CI') and (ie_informacao_p = 'A') then
		pls_gerar_w_benef_movto_mensal(dt_referencia_inicio_w,nm_usuario_p,cd_estabelecimento_p);
		
		if	(ie_benef_sca_p = 'S') then
			ds_comando_w	:=	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO, ' ||
						'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
						'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
						'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
						'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
												cd_estabelecimento_p ||', ' ||
						'						nvl(b.ie_tipo_segurado,'||'''x'''||' ), ' ||
						'						sysdate, ' ||
												chr(39) || nm_usuario_p || chr(39) || ', ' ||
												ds_informacao_w || ', ' ||
												ds_descricao_w || ', ' ||
												ds_informacao_2_w || ', ' ||
												ds_descricao_2_w || ', ' ||
												ds_informacao_w ||
						'					from	w_pls_benef_movto_mensal a, ' ||
						'						pls_segurado		b, ' ||
						'						pls_intercambio		d, ' ||
						'						pls_plano		c, ' ||
						'						pessoa_fisica		e  ' ||
						'					where	a.nr_seq_segurado	= b.nr_sequencia ' ||
						'					and	b.nr_seq_intercambio	= d.nr_sequencia ' ||
						'					and	b.nr_Seq_plano		= c.nr_sequencia ' ||
						'					and 	e.cd_pessoa_fisica	= b.cd_pessoa_fisica '||
						'					and	a.dt_referencia		= to_date(' || chr(39) || dt_referencia_inicio_w || chr(39) || ') ' ||
						'					and	b.cd_estabelecimento	= ' || cd_estabelecimento_p ||
						'					and	exists (select	1 from pls_sca_vinculo h where h.nr_seq_segurado = b.nr_sequencia))' ;
		else
			ds_comando_w	:=	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO, ' ||
						'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
						'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
						'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
						'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
												cd_estabelecimento_p ||', ' ||
						'						nvl(b.ie_tipo_segurado,'||'''x'''||' ), ' ||
						'						sysdate, ' ||
												chr(39) || nm_usuario_p || chr(39) || ', ' ||
												ds_informacao_w || ', ' ||
												ds_descricao_w || ', ' ||
												ds_informacao_2_w || ', ' ||
												ds_descricao_2_w || ', ' ||
												ds_informacao_w ||
						'					from	w_pls_benef_movto_mensal a, ' ||
						'						pls_segurado		b, ' ||
						'						pls_intercambio		d, ' ||
						'						pls_plano		c, ' ||
						'						pessoa_fisica		e  ' || 
						'					where	a.nr_seq_segurado	= b.nr_sequencia ' ||
						'					and	b.nr_seq_intercambio	= d.nr_sequencia ' ||
						'					and	b.nr_Seq_plano		= c.nr_sequencia ' ||
						'					and	e.cd_pessoa_fisica  	= b.cd_pessoa_fisica ' ||
						'					and	a.dt_referencia		= to_date(' || chr(39) || dt_referencia_inicio_w || chr(39) || ') ' ||
						'					and	b.cd_estabelecimento	= ' || cd_estabelecimento_p || ' )' ;
		end if;		
	else
		if	(ie_informacao_p = 'E') then
			if	(ie_tipo_contrato_p	= 'CO') then
				if	(ie_benef_sca_p = 'S') then
					ds_comando_w	:= 	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO, NR_SEQ_SEGURADO, ' ||
								'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
								'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
								'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
								'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
														cd_estabelecimento_p ||', ' ||
								'						nvl(b.ie_tipo_segurado,'||'''x'''||' ), ' ||
								'						b.nr_sequencia, ' ||
								'						sysdate, ' ||
														chr(39) || nm_usuario_p || chr(39) || ', ' ||
														ds_informacao_w || ', ' ||
														ds_descricao_w || ', ' ||
														ds_informacao_2_w || ', ' ||
														ds_descricao_2_w || ', ' ||
														ds_informacao_w ||
								'					from   	pls_segurado_historico a, ' ||
								'						pls_segurado b, ' 	||
								'						pls_plano c, ' 		||
								'						pls_contrato d, ' 	|| 
								'						pessoa_fisica e	'	||
								'					where	b.nr_sequencia	= a.nr_seq_segurado ' ||
								'					and	c.nr_sequencia	= b.nr_seq_plano ' ||
								'					and	d.nr_sequencia	= b.nr_seq_contrato ' ||
								'					and 	e.cd_pessoa_fisica = b.cd_pessoa_fisica ' ||
								'					and	d.ie_tipo_beneficiario <>  '||chr(39)||'ENB'||chr(39)||
								'					and 	b.dt_liberacao is not null	' ||	
								'					and	b.dt_cancelamento is null	' ||
								'					and	b.cd_estabelecimento = ' || cd_estabelecimento_p ||
								'					and	exists (select	1 from pls_sca_vinculo h where h.nr_seq_segurado = b.nr_sequencia))' ;
				else
					ds_comando_w	:= 	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO, NR_SEQ_SEGURADO, ' ||
								'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
								'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
								'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
								'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
														cd_estabelecimento_p ||', ' ||
								'						nvl(b.ie_tipo_segurado,'||'''x'''||' ), ' ||
								'						b.nr_sequencia, ' ||
								'						sysdate, ' ||
														chr(39) || nm_usuario_p || chr(39) || ', ' ||
														ds_informacao_w || ', ' ||
														ds_descricao_w || ', ' ||
														ds_informacao_2_w || ', ' ||
														ds_descricao_2_w || ', ' ||
														ds_informacao_w ||
								'					from   	pls_segurado_historico a, ' ||
								'						pls_segurado b, ' ||
								'						pls_plano c, ' 	  ||
								'						pls_contrato d, ' || 
								'						pessoa_fisica e ' ||
								'					where	b.nr_sequencia	= a.nr_seq_segurado ' ||
								'					and	c.nr_sequencia	= b.nr_seq_plano ' ||
								'					and	d.nr_sequencia	= b.nr_seq_contrato ' ||
								'					and 	e.cd_pessoa_fisica = b.cd_pessoa_fisica ' ||
								'					and	d.ie_tipo_beneficiario <>  '||chr(39)||'ENB'||chr(39)||
								'					and 	b.dt_liberacao is not null	' ||	
								'					and	b.dt_cancelamento is null	' ||
								'					and	b.cd_estabelecimento = ' || cd_estabelecimento_p ;
				end if;
				
			elsif	(ie_tipo_contrato_p	= 'CI') then
				if	(ie_benef_sca_p = 'S') then
					ds_comando_w	:= 	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO,NR_SEQ_SEGURADO, ' ||
								'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
								'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
								'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
								'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
														cd_estabelecimento_p ||', ' ||
								'						nvl(b.ie_tipo_segurado,'||'''x'''||' ), ' ||
								'						b.nr_sequencia, ' ||
								'						sysdate, ' ||
														chr(39) || nm_usuario_p || chr(39) || ', ' ||
														ds_informacao_w || ', ' ||
														ds_descricao_w || ', ' ||
														ds_informacao_2_w || ', ' ||
														ds_descricao_2_w || ', ' ||
														ds_informacao_w ||
								'					from   	pls_segurado_historico a, ' ||
								'						pls_segurado b, ' ||
								'						pls_plano c, ' ||
								'						pls_intercambio d, ' || 
								'						pessoa_fisica e ' ||
								'					where	b.nr_sequencia	= a.nr_seq_segurado ' ||
								'					and	c.nr_sequencia	= b.nr_seq_plano ' ||
								'					and	d.nr_sequencia	= b.nr_seq_intercambio ' ||
								'					and	e.cd_pessoa_fisica = b.cd_pessoa_fisica ' ||
								'					and 	b.dt_liberacao is not null	' ||	
								'					and	b.dt_cancelamento is null	' ||
								'					and	b.cd_estabelecimento = ' || cd_estabelecimento_p ||
								'					and	exists (select	1 from pls_sca_vinculo h where h.nr_seq_segurado = b.nr_sequencia))' ;
				else
					ds_comando_w	:= 	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO,NR_SEQ_SEGURADO, ' ||
								'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
								'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
								'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
								'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
														cd_estabelecimento_p ||', ' ||
								'						nvl(b.ie_tipo_segurado,'||'''x'''||' ), ' ||
								'						b.nr_sequencia, ' ||
								'						sysdate, ' ||
														chr(39) || nm_usuario_p || chr(39) || ', ' ||
														ds_informacao_w || ', ' ||
														ds_descricao_w || ', ' ||
														ds_informacao_2_w || ', ' ||
														ds_descricao_2_w || ', ' ||
														ds_informacao_w ||
								'					from   	pls_segurado_historico a, ' ||
								'						pls_segurado b, ' ||
								'						pls_plano c, ' ||
								'						pls_intercambio d, ' || 
								'						pessoa_fisica e ' ||
								'					where	b.nr_sequencia	= a.nr_seq_segurado ' ||
								'					and	c.nr_sequencia	= b.nr_seq_plano ' ||
								'					and	d.nr_sequencia	= b.nr_seq_intercambio ' ||
								'					and 	e.cd_pessoa_fisica = b.cd_pessoa_fisica ' ||
								'					and 	b.dt_liberacao is not null	' ||	
								'					and	b.dt_cancelamento is null	' ||
								'					and	b.cd_estabelecimento = ' || cd_estabelecimento_p ;
				end if;
				
			end if;
			
			if	(ie_benef_sca_p = 'S') then
				ds_comando_w	:=	ds_comando_w || 
							'	and	(trunc(b.dt_contratacao,'||chr(39)||'Month'||chr(39)||') <> trunc(b.dt_rescisao,'||chr(39)||'Month'||chr(39)||') and b.dt_rescisao is not null) ' ||
							'	and	a.ie_tipo_historico in (' || chr(39) || '1' || chr(39) || ',' || chr(39) || '5' || chr(39) || ') ' ||
							'	and 	a.nr_sequencia = (	select	decode(z.ie_tipo_historico,''2'',0,b.nr_seq_historico) ' ||
							'					from(	select	max(x.nr_sequencia) nr_seq_historico, ' ||
							'							trunc(nvl(x.dt_ocorrencia_sib,x.dt_historico),''month'') dt_ocorrencia_sib, ' ||
							'							x.nr_seq_segurado ' ||
							'						from  	pls_segurado_historico	x ' ||
							'						where 	x.ie_tipo_historico in (''1'',''2'',''5'') '||
							'						group by	x.nr_seq_segurado, trunc(nvl(x.dt_ocorrencia_sib,x.dt_historico),''month'')) b, ' || 
							'						pls_segurado_historico z ' ||
							'					where	z.nr_sequencia	= b.nr_seq_historico ' ||
							'					and	a.nr_seq_segurado = b.nr_seq_segurado ' ||
							'					and	exists (select	1 from pls_sca_vinculo h where h.nr_seq_segurado = b.nr_sequencia)' ||
							'					and	trunc(nvl(z.dt_ocorrencia_sib,z.dt_historico),''month'') = trunc(to_date(' || chr(39)|| dt_referencia_inicio_w ||chr(39)|| '),'||chr(39)||'Month'||chr(39)||')))';
			
			else
				ds_comando_w	:=	ds_comando_w || 
							'	and	(trunc(b.dt_contratacao,'||chr(39)||'Month'||chr(39)||') <> trunc(b.dt_rescisao,'||chr(39)||'Month'||chr(39)||') and b.dt_rescisao is not null) ' ||
							'	and	a.ie_tipo_historico in (' || chr(39) || '1' || chr(39) || ',' || chr(39) || '5' || chr(39) || ') ' ||
							'	and 	a.nr_sequencia = (	select	decode(z.ie_tipo_historico,''2'',0,b.nr_seq_historico) ' ||
							'					from(	select	max(x.nr_sequencia) nr_seq_historico, ' ||
							'							trunc(nvl(x.dt_ocorrencia_sib,x.dt_historico),''month'') dt_ocorrencia_sib, ' ||
							'							x.nr_seq_segurado ' ||
							'						from  	pls_segurado_historico	x ' ||
							'						where 	x.ie_tipo_historico in (''1'',''2'',''5'') '||
							'						group by	x.nr_seq_segurado, trunc(nvl(x.dt_ocorrencia_sib,x.dt_historico),''month'')) b, ' || 
							'						pls_segurado_historico z ' ||
							'					where	z.nr_sequencia	= b.nr_seq_historico ' ||
							'					and	a.nr_seq_segurado = b.nr_seq_segurado ' ||
							'					and	trunc(nvl(z.dt_ocorrencia_sib,z.dt_historico),''month'') = trunc(to_date(' || chr(39)|| dt_referencia_inicio_w ||chr(39)|| '),'||chr(39)||'Month'||chr(39)||')))';
			end if;	
			
			
		elsif	(ie_informacao_p = 'I') then
			if	(ie_tipo_contrato_p	= 'CO') then
				ds_comando_w	:= 	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO, NR_SEQ_SEGURADO, ' ||
							'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
							'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
							'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
							'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
													cd_estabelecimento_p ||', ' ||
							'						b.ie_tipo_segurado, ' ||
							'						b.nr_sequencia, ' ||
							'						sysdate, ' ||
													chr(39) || nm_usuario_p || chr(39) || ', ' ||
													ds_informacao_w || ', ' ||
													ds_descricao_w || ', ' ||
													ds_informacao_2_w || ', ' ||
													ds_descricao_2_w || ', ' ||
													ds_informacao_w ||
							'					from   	pls_segurado b, ' ||
							'						pls_plano c, ' ||
							'						pls_contrato d, ' || 
								'					pessoa_fisica e ' ||							
							'					where	c.nr_sequencia	= b.nr_seq_plano ' ||
							'					and	d.nr_sequencia	= b.nr_seq_contrato ' ||
							'					and 	e.cd_pessoa_fisica = b.cd_pessoa_fisica ' ||							
							'					and	d.ie_tipo_beneficiario <>  '||chr(39)||'ENB'||chr(39)||
							'					and 	b.dt_liberacao is not null	' ||	
							'					and	b.dt_cancelamento is null	' ||
							'					and	b.cd_estabelecimento = ' || cd_estabelecimento_p ;

			elsif	(ie_tipo_contrato_p	= 'CI') then
				ds_comando_w	:= 	'	insert into w_pls_censo_geral (	NR_SEQUENCIA, DT_MES_REFERENCIA, CD_ESTABELECIMENTO, IE_TIPO_SEGURADO,NR_SEQ_SEGURADO, ' ||
							'					DT_ATUALIZACAO, NM_USUARIO, IE_DIMENSAO, DS_DIMENSAO,IE_DIMENSAO_SECUNDARIA,'||
							'					DS_DIMENSAO_SECUNDARIA,NR_SEQ_REFERENCIA) ' ||
							'				(	select	w_pls_censo_geral_seq.NextVal, ' ||
							'						to_date(' || chr(39) || dt_referencia_w || chr(39) || '), ' ||
													cd_estabelecimento_p ||', ' ||
							'						b.ie_tipo_segurado, ' ||
							'						b.nr_sequencia, ' ||
							'						sysdate, ' ||
													chr(39) || nm_usuario_p || chr(39) || ', ' ||
													ds_informacao_w || ', ' ||
													ds_descricao_w || ', ' ||
													ds_informacao_2_w || ', ' ||
													ds_descricao_2_w || ', ' ||
													ds_informacao_w ||
							'					from   	pls_segurado b, ' ||
							'						pls_plano c, ' ||
							'						pls_intercambio d, ' || 
							'						pessoa_fisica e ' ||
							'					where	c.nr_sequencia	= b.nr_seq_plano ' ||
							'					and	d.nr_sequencia	= b.nr_seq_intercambio ' ||
							'					and 	e.cd_pessoa_fisica = b.cd_pessoa_fisica ' ||
							'					and 	b.dt_liberacao is not null	' ||	
							'					and	b.dt_cancelamento is null	' ||
							'					and	b.cd_estabelecimento = ' || cd_estabelecimento_p ;
			end if;
			
			ds_comando_w	:= 	ds_comando_w || '	and	((trunc(b.dt_contratacao,'||chr(39)||'Month'||chr(39)||') <> trunc(b.dt_rescisao,'||chr(39)||'Month'||chr(39)||') and b.dt_rescisao is not null) or b.dt_rescisao is null) ' ||
						'	and	trunc(b.dt_contratacao,'||chr(39)||'Month'||chr(39)||') = trunc(to_date(' || chr(39)|| dt_referencia_inicio_w ||chr(39)|| '),'||chr(39)||'Month'||chr(39)||'))';
		end if;		
	end if;
	Exec_sql_Dinamico('Tasy', ds_comando_w);
	
	end;
end loop;
close C01;

commit;

end pls_gerar_censo;
/
