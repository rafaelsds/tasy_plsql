create or replace
procedure san_liberar_bolsa_direcionada(	sql_item_p		Varchar2,
						nm_usuario_p		Varchar2) is 

lista_producao_w		dbms_sql.varchar2_table;
nr_seq_producao_w		number(10);

begin
lista_producao_w := obter_lista_string(sql_item_p, ',');

for	i in lista_producao_w.first..lista_producao_w.last loop
	nr_seq_producao_w := to_number(lista_producao_w(i));
	
	if	(nvl(nr_seq_producao_w, 0) > 0) then
		
		update	san_producao
		set	dt_liberacao_direc	= sysdate,
			nm_usuario_direc	= nm_usuario_p
		where	nr_sequencia		= nr_seq_producao_w;

	end if;

end loop;
commit;

end san_liberar_bolsa_direcionada;
/