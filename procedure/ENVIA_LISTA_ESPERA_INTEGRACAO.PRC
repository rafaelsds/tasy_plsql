CREATE OR REPLACE PROCEDURE ENVIA_LISTA_ESPERA_INTEGRACAO(NR_SEQ_LISTA_ESPERA_P NUMBER,
                                                                                                                 IE_ALTERA_STATUS_P VARCHAR2 DEFAULT NULL,
                                                                                                                 DS_EVENTO_INTEGRACAO_P VARCHAR2 DEFAULT NULL,
                                                                                                                 IE_NOVO_STATUS_P VARCHAR2 DEFAULT NULL ) IS

  NR_SEQ_REGULACAO_ATEND_W   REGULACAO_ATEND.NR_SEQUENCIA%TYPE;
  IE_STATUS_W                REGULACAO_ATEND.IE_STATUS%TYPE;
  IE_INTEGRACAO_W            VARCHAR2(2);
  DS_INTEGRACAO_W            CLOB;
  NM_USUARIO_W               REGULACAO_ATEND.NM_USUARIO%TYPE;
  IE_EVENTO_BIFROST_W        VARCHAR2(40);
  CD_EXPRESSAO_W					NUMBER(10);

BEGIN

  SELECT MAX(A.NR_SEQ_REGULACAO)
    INTO NR_SEQ_REGULACAO_ATEND_W
    FROM AGENDA_LISTA_ESPERA A

   WHERE NR_SEQUENCIA = NR_SEQ_LISTA_ESPERA_P;

  IF (NR_SEQ_REGULACAO_ATEND_W IS NOT NULL) THEN

    SELECT MAX(IE_STATUS)
      INTO IE_STATUS_W
      FROM REGULACAO_ATEND
     WHERE NR_SEQUENCIA = NR_SEQ_REGULACAO_ATEND_W;

    NM_USUARIO_W := WHEB_USUARIO_PCK.GET_NM_USUARIO;
    
    OBTER_PARAM_USUARIO(366,
                        4,
                        OBTER_PERFIL_ATIVO,
                        NM_USUARIO_W,
                        NULL,
                        IE_INTEGRACAO_W);



    IF (IE_INTEGRACAO_W = 'I') THEN
    
      IF(DS_EVENTO_INTEGRACAO_P IS NOT NULL) THEN
        IE_EVENTO_BIFROST_W := DS_EVENTO_INTEGRACAO_P;
      ELSIF (NVL(IE_ALTERA_STATUS_P,'N') = 'N')THEN
        IE_EVENTO_BIFROST_W := 'regulation.waitingList';
      ELSE 
        IE_EVENTO_BIFROST_W := 'regulation.waitingList.update';
      END IF; 
      
      IF(IE_NOVO_STATUS_P = 'FI') THEN
         CD_EXPRESSAO_W := 1108547;
      ELSIF (IE_NOVO_STATUS_P = 'CA') THEN
        CD_EXPRESSAO_W := 1105223;
      END IF;

      SELECT BIFROST.SEND_INTEGRATION(IE_EVENTO_BIFROST_W,
                                      'com.philips.tasy.integration.atepac.waitinglist.WaitingListRequest',
                                      '{"waitingListOrigin" : "' ||NR_SEQ_LISTA_ESPERA_P || '"'  
                                      || nvl2(IE_NOVO_STATUS_P, ', "newStatus" : "' || IE_NOVO_STATUS_P ||'"' , '')
                                      || nvl2(CD_EXPRESSAO_W, ', "message" : "' ||obter_expressao_dic_objeto( CD_EXPRESSAO_W) ||'"' , '')
                                      || '}',
                                      NM_USUARIO_W)
        INTO DS_INTEGRACAO_W
        FROM DUAL;

    END IF;

  END IF;

END;
/