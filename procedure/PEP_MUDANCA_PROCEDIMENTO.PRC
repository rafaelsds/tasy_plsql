create or replace
procedure pep_mudanca_procedimento(	ds_justificativa_p	varchar2,
					nr_seq_interno_p	number) is

begin
if	(nr_seq_interno_p is not null) then
	update 	sus_laudo_paciente 
	set 	ds_justificativa = ds_justificativa_p 
	where 	nr_seq_interno = nr_seq_interno_p;
end if;

commit;

end pep_mudanca_procedimento;
/