create or replace
procedure extorna_liberacao_req_conj(
		nr_sequencia_p	number,
		nm_usuario_p	varchar2) is

begin

update	cm_requisicao 
set	dt_liberacao	= null,
	nm_usuario	= nm_usuario_p
where 	nr_sequencia	= nr_sequencia_p;

commit;
end extorna_liberacao_req_conj;
/
