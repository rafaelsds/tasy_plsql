create or replace
procedure	pep_ver_imagens_js(
				nm_usuario_tasy_p			varchar2,
				nr_prescricao_p			number,
				nr_sequencia_p			number,				
				ds_caminho_result_p	out	varchar2,
				nm_usuario_integr_p	out	varchar2,
				ds_senha_usuario_integr_p	out	varchar2,
				ds_servico_p		out	varchar2,				
				cd_tipo_procedimento_p	out	number,
				nm_usuario_pixeon_p	out 	varchar2,
				ds_senha_pixeon_p	out	varchar2) is

ds_caminho_result_w		varchar2(255);
nm_usuario_integr_w		varchar2(30);
ds_senha_usuario_integr_w		varchar2(30);
ds_servico_w			varchar2(80);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_tipo_procedimento_w		number(3);

nm_usuario_pixeon_w		varchar2(15);
ds_senha_pixeon_w		varchar2(80);

				
begin
				
select	max(ds_caminho_result)
into	ds_caminho_result_w
from	empresa_integr_dados
where	nr_seq_empresa_integr = 4;

if	(nm_usuario_tasy_p is not null) then
	begin
	select	max(nm_usuario_integr), 
		max(ds_senha_usuario_integr),
		max(ds_servico)
	into	nm_usuario_integr_w,
		ds_senha_usuario_integr_w,
		ds_servico_w
	from	lab_regra_usuario_integr
	where	nm_instituto = 'isite'
	and	nm_usuario_tasy = nm_usuario_tasy_p;
	end;
end if;

if	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) then
	begin
	select	cd_procedimento, 
		ie_origem_proced
	into	cd_procedimento_w,
		ie_origem_proced_w
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_sequencia_p;
	
	
	select	cd_tipo_procedimento
	into	cd_tipo_procedimento_w
	from	procedimento
	where	cd_procedimento = cd_procedimento_w
	and	ie_origem_proced = ie_origem_proced_w;
	end;
	
	
end if;

select  max(nm_usuario_pixeon),
	max(ds_senha_pixeon)
into	nm_usuario_pixeon_w,
	ds_senha_pixeon_w
from    parametro_integracao_pacs 
where   cd_estabelecimento  = wheb_usuario_pck.get_cd_estabelecimento;

ds_caminho_result_p	:= ds_caminho_result_w;
nm_usuario_integr_p	:= nm_usuario_integr_w;
ds_senha_usuario_integr_p	:= ds_senha_usuario_integr_w;
ds_servico_p		:= ds_servico_w;
cd_tipo_procedimento_p	:= cd_tipo_procedimento_w; 

nm_usuario_pixeon_p := nm_usuario_pixeon_w;
ds_senha_pixeon_p := ds_senha_pixeon_w;

end pep_ver_imagens_js;
/
