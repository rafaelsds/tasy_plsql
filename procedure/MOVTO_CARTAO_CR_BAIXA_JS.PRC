create or replace
procedure movto_cartao_cr_baixa_js (
		nr_sequencia_p		number,
		nr_trans_financ_p		number,
		nm_usuario_p		varchar2,
		cd_estabelecimento_p	number,
		ie_cred_nao_ident_p out 	varchar2,
		ds_erro_p out		varchar2) is 

nr_seq_baixa_w 		number(5,0)	 := 0;
ie_cred_nao_ident_w 	varchar2(1);
ds_erro_w		varchar2(100);
		
begin

if	(nr_sequencia_p is not null) then
	begin
	select 	max(nr_sequencia) 
	into	nr_seq_baixa_w
	from 	movto_cartao_cr_baixa 
	where 	nr_sequencia = nr_sequencia_p;
	
	if	( nr_seq_baixa_w <> 0) then
		begin
		select 	max(ie_cred_nao_ident) 
		into	ie_cred_nao_ident_w
		from 	transacao_financeira 
		where 	nr_sequencia = nr_trans_financ_p;
		
		atualiza_baixa_cartao(nr_sequencia_p, nm_usuario_p);
		
		if	(ie_cred_nao_ident_w = 'S') then
			begin
			ds_erro_w := obter_texto_tasy(95988, 1);
			atualizar_cni_baixa_cartao(cd_estabelecimento_p, nr_sequencia_p, null, 'I');
			end;
		end if;
		end;
	end if;	
	end;
end if;
ie_cred_nao_ident_p 	:= ie_cred_nao_ident_w;
ds_erro_p 		:= ds_erro_w;

commit;

end movto_cartao_cr_baixa_js;
/