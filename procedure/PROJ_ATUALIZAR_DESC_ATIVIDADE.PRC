create or replace
procedure proj_atualizar_desc_atividade(
			nr_sequencia_p		Number) as


cd_classificacao_w	varchar2(255);
ds_atividade_w	varchar2(255);
ds_atividade_ww	varchar2(255);
i			Number(10);
ds_espaco_w		varchar2(255);


begin

ds_espaco_w		:= '';

select	nvl(max(cd_classificacao),'X'),
	nvl(max(ds_atividade),'X')
into	cd_classificacao_w,
	ds_atividade_w
from	proj_cron_etapa
where	nr_sequencia = nr_sequencia_p;

ds_atividade_ww := ltrim(ds_atividade_ww);
ds_atividade_w  := ltrim(ds_atividade_w);

ds_atividade_ww	:= ds_atividade_w;

if	(cd_classificacao_w <> 'X') then
	FOR i IN 1..Length(cd_classificacao_w) LOOP
		ds_espaco_w	:= ds_espaco_w || '  ';
	end loop;

	ds_atividade_ww	:= substr(ds_espaco_w || ds_atividade_w,1,255);
end if;

update	proj_cron_etapa
set	ds_atividade = ds_atividade_ww
where	nr_sequencia = nr_sequencia_p;
commit;

end proj_atualizar_desc_atividade;
/
