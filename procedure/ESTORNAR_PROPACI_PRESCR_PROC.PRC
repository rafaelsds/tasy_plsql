create or replace
procedure Estornar_ProPaci_Prescr_Proc(	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					nm_usuario_p		varchar2,
					nr_seq_proc_hor_p	number default null) is

ie_canc_proc_prescr_w		varchar2(1);

nr_sequencia_w			number(10,0);
cd_convenio_w			number(5,0);
nr_seq_gerada_w			number(10,0);
nr_interno_conta_w		number(10,0);
ie_recalcular_conta_w		varchar2(1):= 'N';
ie_status_acerto_w		varchar2(1);
dt_procedimento_w		procedimento_paciente.dt_procedimento%type;
nr_seq_proc_pac_w		procedimento_paciente.nr_sequencia%type;

cursor	c01 is

	select	a.nr_sequencia,
		a.cd_convenio,
		a.nr_interno_conta,
		a.dt_procedimento
	from	procedimento_paciente a
	where 	a.nr_prescricao		= nr_prescricao_p
	and	((a.nr_sequencia_prescricao	= nr_seq_prescr_p) 
		or (a.nr_sequencia_prescricao is null and a.nr_seq_proc_princ =  (select	nr_sequencia 
								        from		procedimento_paciente 
								        where	nr_sequencia_prescricao	= nr_seq_prescr_p 
								        and		nr_prescricao	= nr_prescricao_p 
								        and		qt_procedimento > 0 
 								        and		nr_seq_proc_est is null 
 				  				        and		rownum = 1)))
	and	a.qt_procedimento > 0
	and	a.nr_seq_proc_est is null
	and	(nr_seq_proc_hor_p is null or a.nr_sequencia = nr_seq_proc_pac_w);

begin

if	(nr_seq_proc_hor_p is not null) then
	nr_seq_proc_pac_w := obter_nr_seq_proc_pac(nr_seq_proc_hor_p);
end if;

select 	nvl(max(ie_canc_proc_prescr),'N'),
	nvl(max(ie_recalcular_conta),'N')
into	ie_canc_proc_prescr_w,
	ie_recalcular_conta_w
from 	parametro_faturamento c,
	Atendimento_paciente b,
	prescr_medica a
where	a.nr_prescricao		= nr_prescricao_p
  and	a.nr_atendimento	= b.nr_atendimento
  and	b.cd_estabelecimento	= c.cd_estabelecimento;

if	(ie_canc_proc_prescr_w = 'S') then
	open c01;
	loop
	fetch c01 into	nr_sequencia_w,
			cd_convenio_w,
			nr_interno_conta_w,
			dt_procedimento_w;
		exit when c01%notfound;

		duplicar_proc_paciente(nr_sequencia_w, nm_usuario_p, nr_seq_gerada_w);

		if	(nr_seq_gerada_w is not null) then
			update 	procedimento_paciente
			set	nr_interno_conta 	= null,
				qt_procedimento	= qt_procedimento * -1,
				vl_procedimento = vl_procedimento * -1,
				vl_medico = vl_medico * -1,
				vl_anestesista = vl_anestesista * -1,
				vl_materiais = vl_materiais * -1,
				vl_auxiliares = vl_auxiliares * -1,
				vl_custo_operacional = vl_custo_operacional * -1,
				vl_adic_plant = vl_adic_plant * -1,
				ds_observacao	= Wheb_mensagem_pck.get_Texto(305579, 'DS_OBSERVACAO_W='|| ds_observacao), /*ds_observacao || ' Estorno gerado a partir do cancelamento da prescrição'*/				
				dt_procedimento	= dt_procedimento_w
			where nr_sequencia = nr_seq_gerada_w;
			
			update 	procedimento_paciente
			set	nr_seq_proc_est	= nr_seq_gerada_w
			where nr_sequencia	= nr_sequencia_w;

			atualiza_preco_procedimento(nr_seq_gerada_w, cd_convenio_w, nm_usuario_p);
		end if;
	end loop;
	close c01;
else
	open c01;
	loop
	fetch c01 into	nr_sequencia_w,
			cd_convenio_w,
			nr_interno_conta_w,
			dt_procedimento_w;
		exit when c01%notfound;
		
		delete procedimento_paciente
		where  nr_seq_proc_princ = nr_sequencia_w; 
		
	end loop;
	close c01;
	
	if	(nr_seq_proc_hor_p is null) then
		delete from material_atend_Paciente
		where	NR_SEQ_PROC_PRINC in (	select	NR_SEQUENCIA
										from	procedimento_paciente
										where nr_prescricao = nr_prescricao_p
										and nr_sequencia_prescricao = nr_seq_prescr_p);
	else
		delete	material_atend_paciente
		where	nr_seq_proc_princ = nr_seq_proc_pac_w;
	end if;
	
	delete	procedimento_paciente
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia_prescricao = nr_seq_prescr_p
	and		(nr_seq_proc_hor_p is null or nr_sequencia = nr_seq_proc_pac_w);
	
	if	(nvl(ie_recalcular_conta_w,'N') = 'S') then
		
		select 	nvl(max(ie_status_acerto),2)
		into	ie_status_acerto_w
		from 	conta_paciente
		where 	nr_interno_conta = nr_interno_conta_w;
		
		if	(ie_status_acerto_w = 1) then
			Recalcular_itens_conta(nr_interno_conta_w, nm_usuario_p);
		end if;
	end if;
end if;

end Estornar_ProPaci_Prescr_Proc;
/
