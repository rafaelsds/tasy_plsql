create or replace
procedure pls_html5_reaj_cobranca_retro(	nr_seq_reajuste_p		pls_reajuste.nr_sequencia%type,
											nr_seq_lote_retro_p		pls_reajuste_cobr_retro.nr_sequencia%type,
											cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
											nm_usuario_p			usuario.nm_usuario%type) is 

begin

pls_reaj_cobranca_retro_pck.gerar_lancamentos(	nr_seq_reajuste_p,	nr_seq_lote_retro_p, cd_estabelecimento_p, nm_usuario_p);

end pls_html5_reaj_cobranca_retro;
/