create or replace procedure cp_activity_hist_save (
   NR_ATENDIMENTO_P             CP_ACTIVITY_HIST.NR_ATENDIMENTO%type,
   NM_USUARIO_P                 CP_ACTIVITY_HIST.NM_USUARIO%type,
   NR_SEQ_ITEM_INTERV_ASSOC_P   CP_ACTIVITY_HIST.NR_SEQ_ITEM_INTERV_ASSOC%type,
   NR_SEQ_PRESCR_P              PE_PRESCR_PROC.NR_SEQ_PRESCR%type,
   NR_SEQ_PROC_P                PE_PRESCR_PROC.NR_SEQUENCIA%type,
   SI_SELECTED_P                CP_ACTIVITY_HIST.SI_SELECTED%type,
   NR_SEQ_PROC_HORA_P           PE_PRESCR_PROC_HOR.NR_SEQUENCIA%type
) is

nr_sequencia_w			        CP_ACTIVITY_HIST.NR_SEQUENCIA%type;
nr_seq_proc_hora_w			    PE_PRESCR_PROC_HOR.NR_SEQUENCIA%type;

begin

    if (NR_ATENDIMENTO_P is not null) and
       (NM_USUARIO_P is not null) and
       (NR_SEQ_ITEM_INTERV_ASSOC_P is not null) and
       (NR_SEQ_PRESCR_P is not null) and
       (NR_SEQ_PROC_P is not null) and
       (SI_SELECTED_P is not null) and
       (NR_SEQ_PROC_HORA_P is not null) then
       
       --NR_SEQ_PROC_HORA_P � uma referencia apenas para pegar a data e hora
        select NR_SEQUENCIA 
            into nr_seq_proc_hora_w 
            from PE_PRESCR_PROC_HOR 
        where NR_SEQ_PE_PRESCR = NR_SEQ_PRESCR_P 
          and NR_SEQ_PE_PROC = NR_SEQ_PROC_P 
          and DT_HORARIO = (
            select DT_HORARIO from PE_PRESCR_PROC_HOR where NR_SEQUENCIA = NR_SEQ_PROC_HORA_P
        );

        select  CP_ACTIVITY_HIST_SEQ.nextval
        into    nr_sequencia_w
        from    dual;

        insert into CP_ACTIVITY_HIST (
            DT_ATUALIZACAO,
            DT_ATUALIZACAO_NREC,
            DT_EXECUCAO,
            NM_USUARIO,
            NM_USUARIO_NREC,
            NR_ATENDIMENTO,
            NR_SEQ_ITEM_INTERV_ASSOC,
            NR_SEQ_PE_PRESCR_PROC,
            SI_SELECTED,
            NR_SEQ_PRESCR,
            NR_SEQ_PE_PRESCR_PROC_HOR,
            NR_SEQUENCIA
        ) values (
            sysdate,
            sysdate,
            sysdate,
            NM_USUARIO_P,
            NM_USUARIO_P,
            NR_ATENDIMENTO_P,
            NR_SEQ_ITEM_INTERV_ASSOC_P,
            NR_SEQ_PROC_P,
            SI_SELECTED_P,
            NR_SEQ_PRESCR_P,
            nr_seq_proc_hora_w,
            nr_sequencia_w
        );
    end if;
end cp_activity_hist_save;
/
