create or replace procedure sus_gerar_longa_permanencia
		(	nr_atendimento_p		number,
			nr_interno_conta_p		number,
			nm_usuario_p		varchar2) is


cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
qt_perm_sus_w			number(10);
qt_perm_real_w			number(10);
qt_longa_perm_w			number(10);
dt_entrada_unidade_w		date;
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
cd_setor_atendimento_w		number(5);
cd_estabelecimento_w		number(5);
nr_seq_atepacu_w			number(10);
nr_seq_interno_w			number(10);
cd_cgc_prestador_w		varchar2(14);
nr_sequencia_w			number(10);
nr_aih_w				number(13);
vl_procedimento_w			number(15,2)	:= 0;
vl_medico_w			number(15,2)	:= 0;
vl_anestesista_w			number(15,2)	:= 0;
vl_materiais_w			number(15,2)	:= 0;
cd_medico_solic_w			varchar2(10);
cd_medico_resp_w			varchar2(10);
cd_medico_req_laudo_w		varchar2(10);
cd_medico_resp_laudo_w		varchar2(10);
ie_medico_w			varchar2(10)	:= 'N';
cd_medico_executor_w		varchar2(10);
cd_procedimento_real_w		number(15);
ie_origem_proc_real_w		number(10);
dt_procedimento_w			date;
dt_periodo_inicial_w		date;
ie_data_execucao_w		varchar2(10) 	:= 'N';
ie_corrige_loga_permanecia_w	varchar2(10) 	:= 'N';
ie_ultrapassa_qtd_max_w		varchar2(10) 	:= 'S';
cd_estab_usuario_w		Number(5) 	:= 0;
ie_qtd_perm_informada_w		varchar2(10) 	:= 'N';
qt_longa_permanencia_w		Number(10) 	:= null;
ie_ajuste_perm_comp_w		varchar2(10) 	:= 'N';

BEGIN

begin
cd_estab_usuario_w := nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
exception
when others then
	cd_estab_usuario_w := 0;
end;

ie_medico_w		:= nvl(Obter_Valor_Param_Usuario(1123,54,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w),'N');
ie_data_execucao_w	:= nvl(Obter_Valor_Param_Usuario(1123,65,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w),'N');
ie_ultrapassa_qtd_max_w := nvl(Obter_Valor_Param_Usuario(1123,117,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w),'S');
ie_qtd_perm_informada_w	:= nvl(Obter_Valor_Param_Usuario(1123,183,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w),'N');
ie_ajuste_perm_comp_w	:= nvl(Obter_Valor_Param_Usuario(1123,187,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w),'N');

begin
select	qt_longa_permanencia
into	qt_longa_permanencia_w
from	sus_aih_unif
where	nr_atendimento		= nr_atendimento_p
and	nr_interno_conta	= nr_interno_conta_p;
exception
when others then
	qt_longa_permanencia_w := null;
end;

if	(qt_longa_permanencia_w is null) then
	begin

	begin
	select	qt_longa_permanencia
	into	qt_longa_permanencia_w
	from	sus_dados_aih_conta
	where	nr_interno_conta = nr_interno_conta_p;
	exception
	when others then
		qt_longa_permanencia_w := null;
	end;

	end;
end if;
/* Selecao das informacoes para geracao do procedimento Longa permanencia */
Sus_Calcular_Longa_Perm_Unif(nr_atendimento_p, nr_interno_conta_p, nm_usuario_p, cd_procedimento_w, ie_origem_proced_w,	qt_perm_sus_w, qt_perm_real_w, qt_longa_perm_w);

if	(nvl(ie_qtd_perm_informada_w,'N') = 'S') and
	(qt_longa_permanencia_w is not null) then
	qt_longa_perm_w := qt_longa_permanencia_w;
end if;

if	(ie_ultrapassa_qtd_max_w = 'N') and
	(qt_longa_perm_w > 99) then
	qt_longa_perm_w := 99;
end if;

begin
update	sus_aih_unif
set	qt_permanencia_sus	= qt_perm_sus_w,
	qt_permanencia_real 	= qt_perm_real_w,
	qt_longa_permanencia	= qt_longa_perm_w
where	nr_atendimento		= nr_atendimento_p
and	nr_interno_conta	= nr_interno_conta_p;
exception
	when others then
	qt_perm_sus_w	:= qt_perm_sus_w;
end;

begin
select	nr_aih,
	cd_procedimento_real,
	ie_origem_proc_real
into	nr_aih_w,
	cd_procedimento_real_w,
	ie_origem_proc_real_w
from	sus_aih_unif
where	nr_atendimento		= nr_atendimento_p
and	nr_interno_conta	= nr_interno_conta_p;
exception
	when others then
	nr_aih_w	:= 0;

	select	max(cd_procedimento_solic),
		max(ie_origem_proced),
		max(cd_medico_requisitante),
		max(cd_medico_responsavel)
	into	cd_procedimento_real_w,
		ie_origem_proc_real_w,
		cd_medico_req_laudo_w,
		cd_medico_resp_laudo_w
	from	sus_laudo_paciente
	where	nr_interno_conta	= nr_interno_conta_p
	and	nr_atendimento		= nr_atendimento_p
	and	ie_classificacao 	= 1
	and	ie_tipo_laudo_sus 	= 0;
end;

select	max(cd_estabelecimento),
	max(dt_periodo_inicial)
into	cd_estabelecimento_w,
	dt_periodo_inicial_w
from	conta_paciente
where 	nr_interno_conta	= nr_interno_conta_p;

ie_corrige_loga_permanecia_w := obter_valor_param_usuario(1123,89,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w);

if	(qt_longa_perm_w = 0) and
	(obter_se_regra_permanencia(cd_estabelecimento_w,cd_procedimento_real_w,ie_origem_proc_real_w) = 'S') and
	(ie_corrige_loga_permanecia_w = 'S') then
	begin
	delete	procedimento_paciente
	where	nr_atendimento		= nr_atendimento_p
	and	nr_interno_conta	= nr_interno_conta_p
	and	ie_origem_Proced	= 7
	and	cd_procedimento		= 0802010199
	and	cd_motivo_exc_conta is null;
	end;
end if;

if	(qt_longa_perm_w	> 0)  and
	(obter_se_regra_permanencia(cd_estabelecimento_w,cd_procedimento_real_w,ie_origem_proc_real_w) = 'S') then
	begin

	cd_procedimento_w	:= 0802010199;

	begin
	select 	dt_entrada_unidade,
		cd_convenio,
		cd_categoria,
		cd_setor_atendimento,
		cd_estabelecimento,
		nr_sequencia
	into	dt_entrada_unidade_w,
		cd_convenio_w,
		cd_categoria_w,
		cd_setor_atendimento_w,
		cd_estabelecimento_w,
		nr_seq_atepacu_w
	from 	atendimento_paciente c,
		atend_categoria_convenio b,
		atend_paciente_unidade a
	where	c.nr_atendimento	= nr_atendimento_p
	and	a.nr_atendimento 	= c.nr_atendimento
	and	a.nr_atendimento	= b.nr_atendimento
	and	b.nr_seq_interno	= obter_atecaco_atendimento(c.nr_atendimento)
	and	obter_classif_setor(a.cd_setor_atendimento) in (3,4)
	and	a.dt_entrada_unidade	= (	select 	max(d.dt_entrada_unidade)
						from 	atend_paciente_unidade d
						where 	obter_classif_setor(d.cd_setor_atendimento) in (3,4)
						and	d.nr_Atendimento = c.nr_atendimento);
	exception
		when no_data_found then
			wheb_mensagem_pck.exibir_mensagem_abort(191752);
			/* Paciente n�o teve nenhuma passagem por um setor de Interna��o. */
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(191753);
			/* Problema na leitura dos dados da unidade. */
	end;
	/* selecionar ultimo setor de interna��o */
	nr_seq_interno_w	:= nvl(obter_atepacu_paciente(nr_atendimento_p,'IA'),0);

	if	(nr_seq_interno_w > 0) and
		(nr_seq_interno_w <> nr_seq_atepacu_w) then
		select	dt_entrada_unidade,
			cd_setor_atendimento,
			nr_seq_interno
		into	dt_entrada_unidade_w,
			cd_setor_atendimento_w,
			nr_seq_atepacu_w
		from	atend_paciente_unidade
		where	nr_atendimento	= nr_atendimento_p
		and	nr_seq_interno	= nr_seq_interno_w;
	end if;

	select	cd_cgc
	into	cd_cgc_prestador_w
	from	estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_w;
	/* Delecao de longa permanecia caso exista alguma */
	delete	procedimento_paciente
	where	nr_atendimento		= nr_atendimento_p
	and	nr_interno_conta	= nr_interno_conta_p
	and	ie_origem_Proced	= 7
	and	cd_procedimento		= cd_procedimento_w
	and	cd_motivo_exc_conta is null;

	select	procedimento_paciente_seq.nextval
	into	nr_sequencia_w
	from	dual;

	if	(ie_medico_w	<> 'N') then

		begin
		select	cd_medico_solic,
			cd_medico_responsavel
		into	cd_medico_solic_w,
			cd_medico_resp_w
		from	sus_aih_unif
		where	nr_interno_conta	= nr_interno_conta_p;
		exception
			when others then
			if	(nvl(cd_medico_resp_laudo_w,'X') <> 'X') then
				cd_medico_resp_w := cd_medico_resp_laudo_w;
			else
				cd_medico_resp_w	:= null;
			end if;
			if	(nvl(cd_medico_req_laudo_w,'X') <> 'X') then
				cd_medico_solic_w := cd_medico_req_laudo_w;
			else
				cd_medico_solic_w	:= null;
			end if;
		end;

		if	(ie_medico_w	= 'R') then
			cd_medico_executor_w	:= cd_medico_resp_w;
		elsif	(ie_medico_w	= 'S') then
			cd_medico_executor_w	:= cd_medico_solic_w;
		end if;
	end if;

	dt_procedimento_w	:= dt_entrada_unidade_w;

	if	(ie_data_execucao_w		= 'S') then
		dt_procedimento_w		:= dt_periodo_inicial_w;
	end if;

	insert into procedimento_paciente
	(	nr_atendimento,
		dt_entrada_unidade,
		cd_procedimento,
		dt_procedimento,
		qt_procedimento,
		dt_atualizacao,
		nm_usuario,
		cd_convenio,
		cd_categoria,
		dt_prescricao,
		cd_setor_atendimento,
		ie_origem_proced,
		vl_procedimento,
		vl_medico,
		vl_anestesista,
		vl_materiais,
		nr_aih,
		tx_procedimento,
		ie_valor_informado,
		nr_sequencia,
		dt_inicio_procedimento,
		ie_classif_sus,
		cd_cgc_prestador,
		nr_interno_conta,
		nr_seq_atepacu,
		nr_seq_aih,
		ds_observacao,
		cd_medico_executor)
	values(	nr_atendimento_p,
		dt_entrada_unidade_w,
		cd_procedimento_w,
		nvl(dt_procedimento_w,dt_entrada_unidade_w),
		qt_longa_perm_w,
		sysdate,
		'Tasy',
		cd_convenio_w,
		cd_categoria_w,
		dt_entrada_unidade_w,
		cd_setor_atendimento_w,
		7,
		vl_procedimento_w,
		vl_medico_w,
		vl_anestesista_w,
		vl_materiais_w,
		nr_aih_w,
		100,
		'N',
		nr_sequencia_w,
		dt_entrada_unidade_w,
		4,
		cd_cgc_prestador_w,
		nr_interno_conta_p,
		nr_seq_atepacu_w,
		0,
		Wheb_mensagem_pck.get_Texto(301533), /*'Param. AIH',*/
		cd_medico_executor_w);

	begin
	Atualiza_Preco_Procedimento(nr_sequencia_w, cd_convenio_w, nm_usuario_p);
	exception
		when others then
		dt_entrada_unidade_w := sysdate;
	end;

	if	(ie_ajuste_perm_comp_w = 'S') then
		sus_ajustar_perm_competencia(nr_atendimento_p,nr_interno_conta_p,nr_sequencia_w,nm_usuario_p);
	end if;

	end;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end sus_gerar_longa_permanencia;
/