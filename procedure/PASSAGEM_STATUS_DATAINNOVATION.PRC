create or replace
procedure passagem_status_datainnovation(
                        CD_BARRAS_P  varchar2,
                        NM_USUARIO_P  varchar2,
                        IE_ERRO_P   out varchar2,
                        DS_MENSAGEM_P out varchar2) is 

ie_status_amostra_w             prescr_proc_material.ie_status%type;
nr_prescricao_w                 prescr_proc_material.nr_prescricao%type;
nr_seq_prescr_proc_mat_w        prescr_proc_material.nr_sequencia%type;
nr_seq_prescr_proc_mat_item_w   prescr_proc_mat_item.nr_sequencia%type;

nr_seq_prescr_w                 prescr_procedimento.nr_sequencia%type;
ie_status_exame_w               prescr_procedimento.ie_status_atend%type;

ie_status_esperado_w            prescr_procedimento.ie_status_atend%type;
ie_status_destino_w             prescr_procedimento.ie_status_atend%type;

ie_atualiza_exame               varchar2(1);
ie_realizou_atual               varchar2(1);

CURSOR  C_EXAMES_POR_AMOSTRA IS
SELECT  pp.nr_sequencia,
        pp.ie_status_atend,
        ppmi.ie_status,
        ppmi.nr_sequencia
from    prescr_proc_mat_item ppmi
        inner join prescr_procedimento pp on (ppmi.nr_seq_prescr = pp.nr_sequencia)
where   pp.nr_prescricao = nr_prescricao_w and
        ppmi.nr_seq_prescr_proc_mat = nr_seq_prescr_proc_mat_w;

begin

    ie_status_esperado_w        := 25;
    ie_status_destino_w         := 26;
    ie_atualiza_exame           := 'N';
    ie_realizou_atual           := 'N';
    nr_prescricao_w             := 0;
    nr_seq_prescr_proc_mat_w    := 0;
    ie_status_amostra_w         := 0;
    nr_seq_prescr_w             := 0;
    ie_status_exame_w           := 0;

    begin --Seleciona as informa��es da amostra com base no c�digo de barras recebido
        select  ppm.nr_prescricao,
                ppm.nr_sequencia
        into    nr_prescricao_w,
                nr_seq_prescr_proc_mat_w
        from    prescr_proc_material ppm
                inner join prescr_proc_mat_item ppmi on (ppm.nr_prescricao = ppmi.nr_prescricao and ppm.nr_sequencia = ppmi.nr_seq_prescr_proc_mat)
        where   ppm.cd_barras = to_char(to_number(cd_barras_p))
        group by ppm.nr_prescricao,
                 ppm.nr_sequencia;
    exception
    when TOO_MANY_ROWS then
        DS_MENSAGEM_P := 'Algum erro de cadastro, h� mais de uma amostra para o c�digo de barras ' ||
            cd_barras_p;
        IE_ERRO_P := 'S';
        return;
    when NO_DATA_FOUND then
        DS_MENSAGEM_P := 'Algum erro de cadastro ocorreu, n�o h� amostras para o c�digo de barras ' ||
            cd_barras_p;
        IE_ERRO_P := 'S';
        return;
    end;

    begin
        DS_MENSAGEM_P := '';
        OPEN C_EXAMES_POR_AMOSTRA; --Verificar todos os exames contidos na amostra selecionada.
        LOOP FETCH C_EXAMES_POR_AMOSTRA into    nr_seq_prescr_w,
                                                ie_status_exame_w,
                                                ie_status_amostra_w,
                                                nr_seq_prescr_proc_mat_item_w;
            Exit When C_EXAMES_POR_AMOSTRA%NotFound;

            if (ie_status_amostra_w = ie_status_esperado_w) then

                ie_realizou_atual := 'S';

                update  prescr_proc_mat_item
                set     ie_status = ie_status_destino_w,
                        nm_usuario = nm_usuario_p,
                        dt_atualizacao = sysdate
                where   nr_sequencia = nr_seq_prescr_proc_mat_item_w;

                DS_MENSAGEM_P := DS_MENSAGEM_P || 'Prescri��o ' ||
                    to_char(nr_prescricao_w) || ' sequencia ' ||
                    to_char(nr_seq_prescr_w) || ' item de amostra ' ||
                    to_char(nr_seq_prescr_proc_mat_item_w) || ' atualizada para o status ' ||
                    ie_status_destino_w || '; ';

                if (ie_status_exame_w = ie_status_esperado_w) then

                    select  decode(count(1), 0, 'S', 'N') --verifica se o exame n�o possui nenhuma outra amostra abaixo do status de destino
                    into    ie_atualiza_exame
                    from    prescr_proc_mat_item ppmi
                            inner join prescr_proc_material ppm on (ppm.nr_sequencia = ppmi.nr_seq_prescr_proc_mat)
                    where   ppmi.nr_seq_prescr = nr_seq_prescr_w and
                            ppm.nr_prescricao = nr_prescricao_w and
                            ppm.nr_sequencia != nr_seq_prescr_proc_mat_w and
                            ppm.ie_status < ie_status_destino_w;

                    if (ie_atualiza_exame = 'S') then
                        update  prescr_procedimento
                        set     ie_status_atend = ie_status_destino_w,
                                nm_usuario = nm_usuario_p,
                                dt_atualizacao = sysdate
                        where   nr_prescricao = nr_prescricao_w and
                                nr_sequencia = nr_seq_prescr_w;
                        DS_MENSAGEM_P := DS_MENSAGEM_P || 'Prescri��o ' ||
                            to_char(nr_prescricao_w) || ' sequencia ' ||
                            to_char(nr_seq_prescr_w) || ' atualizada para o status ' ||
                            ie_status_destino_w || '; ';
                    else
                        DS_MENSAGEM_P := DS_MENSAGEM_P || 'Prescri��o ' ||
                            to_char(nr_prescricao_w) || ' sequencia ' ||
                            to_char(nr_seq_prescr_w) || ' possui outras amostras em status inferiores � ' ||
                            ie_status_destino_w || '; ';
                    end if;

                else
                    DS_MENSAGEM_P := DS_MENSAGEM_P || 'Prescri��o ' ||
                        to_char(nr_prescricao_w) || ' sequencia ' ||
                        to_char(nr_seq_prescr_w) || ' n�o est� no status ' ||
                        ie_status_destino_w || '; ';
                end if;
            else
                DS_MENSAGEM_P := DS_MENSAGEM_P || 'Prescri��o ' ||
                    to_char(nr_prescricao_w) || ' sequencia ' ||
                    to_char(nr_seq_prescr_w) || ' item de amostra ' ||
                    to_char(nr_seq_prescr_proc_mat_item_w) || ' n�o est� no status ' ||
                    ie_status_destino_w || '; ';
            end if;
        END LOOP;
        CLOSE C_EXAMES_POR_AMOSTRA;

        if ie_realizou_atual = 'N' then
            IE_ERRO_P := 'S';
        else
            IE_ERRO_P := 'N';
        end if;

    exception
    when others then
        return;
        DS_MENSAGEM_P :=    DS_MENSAGEM_P || 'Erro inesperado: ' || SQLERRM || '; ';
        DS_MENSAGEM_P :=    'nr_prescricao_w: ' || to_char(nr_prescricao_w) || '; ' ||
                            'nr_seq_prescr_proc_mat_w: ' || to_char(nr_seq_prescr_proc_mat_w) || '; ' ||
                            'ie_status_amostra_w: ' || to_char(ie_status_amostra_w) || '; ' ||
                            'nr_seq_prescr_w: ' || to_char(nr_seq_prescr_w) || '; ' ||
                            'ie_status_exame_w: ' || to_char(ie_status_exame_w) || '; ' ||
                            'ie_status_esperado_w: ' || to_char(ie_status_esperado_w) || '; ' ||
                            'ie_status_destino_w: ' || to_char(ie_status_destino_w) || '; ' ||
                            'ie_atualiza_exame: ' || to_char(ie_atualiza_exame) || ';' ||
                            'ie_realizou_atual: ' || to_char(ie_realizou_atual) || ';';
        IE_ERRO_P := 'S';
        return;
    end;
end passagem_status_datainnovation;
/
