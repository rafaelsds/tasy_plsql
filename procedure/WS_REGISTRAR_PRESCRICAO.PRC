create or replace
procedure ws_registrar_prescricao(	nr_prescricao_ext_w	varchar2,
									nr_atendimento_p	number,
									dt_entrega_p		date,
									cd_medico_resp_p	varchar2,
									qt_peso_p			number,
									qt_altura_cm_p		number,
									dt_prescricao_p		date,
									dt_mestruacao_p		date,
									cd_setor_entrega_p	number,
									nm_usuario_p		varchar2,
									nr_prescricao_p		out number,
									cd_erro_p			out varchar2,
									ds_erro_p			out varchar2) is 

nr_prescricao_w		number(14);

ie_existe_medico_w	varchar2(1);
ie_existe_pf_w		varchar2(1);
ie_existe_atendimento_w	varchar2(1);
ie_existe_setor_entr_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
cd_estabelecimento_w	number(4);


/* Lista de erros
0 - SUCESSO
1 - Campo n�o informado
	1.1 - Atendimento n�o informado
	1.2 - Pessoa f�sica n�o informada
	1.3 - M�dico n�o informado
	1.4 - Data da prescri��o n�o informado.
2 - Registro n�o existe
	2.1 - M�dico n�o existe no sistema.
	2.2 - Pessoa f�sica n�o encontrada
	2.3 - Atendimento n�o encontrado
	2.4 - Setor de entrega n�o cadastrado
3 - Erro ao criar a prescri��o.
	
*/
					
begin

cd_erro_p	:= 0;

if	(nr_atendimento_p is null) then
	cd_erro_p	:= 1.1;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277340,null);
elsif	(cd_medico_resp_p is null) then
	cd_erro_p	:= 1.3;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277341,null);
elsif	(dt_prescricao_p is null) then
	cd_erro_p	:= 1.4;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277342,null);
end if;
	
if (cd_erro_p = 0) then

	select	decode(count(*),0,'N','S')
	into	ie_existe_medico_w
	from	medico a
	where	a.cd_pessoa_fisica = cd_medico_resp_p;
		
	select	decode(count(*),0,'N','S')
	into	ie_existe_atendimento_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_p;
	
	select	max(cd_pessoa_fisica),
			max(cd_estabelecimento)
	into	cd_pessoa_fisica_w,
			cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;	
		
	select	decode(count(*),0,'N','S')
	into	ie_existe_pf_w
	from	pessoa_fisica a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_setor_entr_w
	from	setor_atendimento a
	where	a.cd_setor_atendimento = cd_setor_entrega_p;
	
	if	(ie_existe_medico_w = 'N') then
		cd_erro_p	:= 2.1;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277343,null)||cd_medico_resp_p;
	elsif	(ie_existe_pf_w = 'N') then
		cd_erro_p	:= 2.2;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277344,null)||cd_pessoa_fisica_w;
	elsif	(ie_existe_atendimento_w = 'N') then
		cd_erro_p	:= 2.3;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277345,null)||to_char(nr_atendimento_p);
	elsif	(cd_setor_entrega_p is not null) and
		(ie_existe_setor_entr_w = 'N') then
		cd_erro_p	:= 2.4;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277346,null)||to_char(cd_setor_entrega_p);		
	end if;
	
	
	if (cd_erro_p = 0) then
	
	
		select	prescr_medica_seq.NextVal
		into	nr_prescricao_w
		from	dual;
		
		begin			
		insert into prescr_medica(nr_atendimento,
					nr_prescricao, 
					cd_pessoa_fisica, 
					cd_medico, 
					dt_prescricao,
					dt_entrega,
					cd_setor_entrega,
					qt_peso,
					qt_altura_cm,
					dt_mestruacao,
					cd_estabelecimento,
					dt_atualizacao, 
					nm_usuario)
		values		(	nr_atendimento_p,
					nr_prescricao_w,
					cd_pessoa_fisica_w,
					cd_medico_resp_p,
					dt_prescricao_p,
					dt_entrega_p,
					cd_setor_entrega_p,
					qt_peso_p,
					qt_altura_cm_p,
					dt_mestruacao_p,
					cd_estabelecimento_w,
					sysdate,
					nm_usuario_p);
					
		nr_prescricao_p		:= nr_prescricao_w;
		
		exception
		when others then
			cd_erro_p	:= 3;
			ds_erro_p	:= substr(sqlerrm,1,2000);
		end;
	
	end if;
	
end if;

commit;

end ws_registrar_prescricao;
/