create or replace 
procedure atualizar_vig_npt (
					qt_elem_kg_dia_p  number,
					nr_seq_nut_pac_p  number,
					nm_usuario_p      varchar2) is

qt_vel_inf_glicose_w  	nut_pac.qt_vel_inf_glicose%type;
qt_hora_inf_w         	nut_pac.qt_hora_inf%type;
qt_min_inf_w          	nut_pac.qt_min_inf%type;
qt_mult_glic_w        	number(15, 2);
ds_sql_w                varchar2(4000);
ds_erro_w             	varchar2(4000);
ds_parametros_w       	varchar2(4000);
begin

select	max(nvl(qt_hora_inf, 24)),
		max(nvl(qt_min_inf, 0))
into	qt_hora_inf_w,
		qt_min_inf_w
from	nut_pac
where	nr_sequencia = nr_seq_nut_pac_p;

begin
	ds_sql_w := 'call calcular_vel_inf_glicose_md(:1, :2, :3) into :qt_vel_inf_glicose_w';
	execute immediate ds_sql_w using in qt_hora_inf_w, 
									 in qt_min_inf_w, 
									 in qt_elem_kg_dia_p, 
									 out qt_vel_inf_glicose_w;
exception
	when others then
		ds_erro_w := substr(sqlerrm, 1, 4000);
		ds_parametros_w := substr('nr_seq_nut_pac_p: '||nr_seq_nut_pac_p||'-'||'qt_hora_inf_w: '||qt_hora_inf_w||'-'||
								  'qt_min_inf_w: '||qt_min_inf_w||'-'||'qt_elem_kg_dia_p: '||qt_elem_kg_dia_p||'-'||
								  'qt_vel_inf_glicose_w: '||qt_vel_inf_glicose_w, 1, 4000);

		gravar_log_medical_device('ATUALIZAR_VIG_NPT', 'CALCULAR_VEL_INF_GLICOSE_MD', ds_parametros_w, ds_erro_w, nm_usuario_p,'S');
		qt_vel_inf_glicose_w := null;
end;

update	nut_pac
set		qt_vel_inf_glicose = qt_vel_inf_glicose_w
where	nr_sequencia = nr_seq_nut_pac_p;

commit;
	
end atualizar_vig_npt;
/
