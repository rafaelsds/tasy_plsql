create or replace
procedure med_evolucao_newRecord(	nr_seq_cliente_p	number,
					qt_altura_cm_p      out number) is

nr_seq_evolucao_w		number(10,0)	:= 0;
qt_altura_cm_w			number(5,2)	:= 0;
qt_med_consulta_w		number(10,0)	:= 0;
		
begin

if	(nr_seq_cliente_p is not null) then
	begin
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_evolucao_w
	from	med_evolucao
	where	qt_altura_cm is not null
	and	nr_seq_cliente      = nr_seq_cliente_p;
	
	if	(nr_seq_evolucao_w <> 0) then	
		select	qt_altura_cm
		into	qt_altura_cm_w
		from	med_evolucao
		where	nr_sequencia = nr_seq_evolucao_w;
	else			
		begin		
		select	count(*)
		into	qt_med_consulta_w
		from	med_consulta
		where	ie_tipo_consulta = 3 
		and	nr_seq_cliente = nr_seq_cliente_p;		
		
		if	(qt_med_consulta_w <> 0) then		
			select	nvl(qt_altura_cm,0)
			into	qt_altura_cm_w
			from	med_consulta
			where	ie_tipo_consulta = 3 
			and	nr_seq_cliente = nr_seq_cliente_p;		
		end if;
		end;
	end if;
	
	end;	
end if;

qt_altura_cm_p	:= qt_altura_cm_w;

end med_evolucao_newRecord;
/