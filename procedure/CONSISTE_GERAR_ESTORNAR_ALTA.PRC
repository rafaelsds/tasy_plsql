create or replace
procedure  consiste_gerar_estornar_alta(
		ds_observacao_p			varchar2,
		ie_gerar_alta_uti_p		varchar2,
		ie_gerar_alta_uti_pac_p		varchar2,
		ie_alta_condicionada_p		varchar2,
		ie_alta_prescr_prev_p		varchar2,
		ie_permite_alta_lanc_post_p	varchar2,
		ie_mat_pend_adep_alta_p		varchar2,
		ie_consiste_transferencia_p	varchar2,
		cd_hospital_dest_p		varchar2,
		cd_empresa_transp_p		varchar2,
		ie_consiste_empresa_transp_p	varchar2,
		cd_motivo_alta_p		number,
		nr_atendimento_p		number,
		cd_setor_atendimento_p		number,
		dt_alta_p			date,
		cd_motivo_permanencia_p		number,
		cd_estabelecimento_p		number,
		ie_inserir_motivo_perm_p out	varchar2,
		ds_avaliacao_alta_p out	varchar2,
		ie_prescr_dev_pend_p	 out	varchar2,
		ds_erro_p 		 out	varchar2,
		ie_itens_nao_adm_p	 out	varchar2,
		nm_usuario_p			varchar2) is

ie_permite_gerar_alta_uti_w	varchar2(1);
ie_atend_paciente_unidade_w	varchar2(1);
ie_obito_motivo_alta_w		varchar2(1);
ie_gasto_pendente_w		varchar2(1);
ie_gasto_pend_proced_eup_w	varchar2(1);
ie_data_gasto_superior_w	varchar2(1);
ie_prescr_prev_w		varchar2(1);
ie_mat_pend_adep_w		varchar2(1);
ie_prescr_dev_pend_w		varchar2(1) := 'N';
ds_setores_w			varchar2(4000);
ds_erro_w			varchar2(2000);
ie_transferencia_w		varchar2(1);
cd_setor_atend_w		number(5,0);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);
qt_minuto_max_w			number(10,0);
qt_min_perm_setor_w		number(20,0);
ie_inserir_motivo_perm_w	varchar2(1) := 'N';
ie_bloq_alta_inf_w		varchar2(1);
ie_impedir_alta_ps_w		varchar2(1);
ie_consiste_ps_w		varchar2(1);
ds_valor_parametro_w		varchar2(255);
ie_exige_just_mot_alta_w	varchar2(1);
ie_exibir_grid_w		varchar2(1);
ie_Consist_Prescr_pend_w	varchar2(1);
ie_prescr_pende_w		varchar2(1);
ie_Consist_CNS_infor_w		varchar2(1);
ie_CNS_ifor_w			varchar2(1);
nr_consiste_obito_w		med_avaliacao_paciente.nr_seq_tipo_avaliacao%type;
ie_bloquear_alta_w		varchar2(1);
ie_consiste_proc_rec_w		varchar2(10);
begin
if	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	ie_exibir_grid_w := obter_valor_param_usuario(3111, 246, wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);	
	ie_Consist_Prescr_pend_w := nvl(obter_valor_param_usuario(3111, 253, obter_perfil_ativo,obter_usuario_ativo, obter_estabelecimento_ativo),'S');	
	ie_Consist_CNS_infor_w := nvl(obter_valor_param_usuario(3111, 257, obter_perfil_ativo,obter_usuario_ativo, obter_estabelecimento_ativo),'N');
	nr_consiste_obito_w := obter_valor_param_usuario(3111, 330, wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	ie_consiste_proc_rec_w := obter_valor_param_usuario(3111, 107, wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	
	if (ie_Consist_Prescr_pend_w = 'N') then
		select 	max(obter_prescr_pendente_atend(nr_atendimento_p))
		into	ie_prescr_pende_w
		from	dual;
		
		if (ie_prescr_pende_w = 'S') then
			Wheb_mensagem_pck.exibir_mensagem_abort(231117);		
		end if;
	end if;
	
	if (ie_Consist_CNS_infor_w = 'S') then
		
		select	nvl(obter_se_cns_informado(nr_atendimento_p),'N')
		into	ie_CNS_ifor_w
		from	dual;
		
		if (ie_CNS_ifor_w = 'N') then
			Wheb_mensagem_pck.exibir_mensagem_abort(231928);		
		end if;		
	end if;
	
	if	(ie_gerar_alta_uti_p = 'S') then
		begin
		/* corrigido = Delphi, utiliza setor do usuario
		ie_permite_gerar_alta_uti_w := permite_gerar_alta_uti(cd_setor_atendimento_p,cd_motivo_alta_p);
		*/
				
		ie_permite_gerar_alta_uti_w := permite_gerar_alta_uti(wheb_usuario_pck.get_cd_setor_atendimento, cd_motivo_alta_p);		
		
		if	(ie_permite_gerar_alta_uti_w = 'N') then
			begin
			Wheb_mensagem_pck.exibir_mensagem_abort(198468);		
			end;
		end if;
		end;
	end if;
		
		select	nvl(max(ie_obito),'N')
		into	ie_obito_motivo_alta_w
		from	motivo_alta
		where	cd_motivo_alta	= cd_motivo_alta_p;
		
		if (ie_obito_motivo_alta_w = 'S') and (nr_consiste_obito_w is not null) then
			begin
				select	decode(count(*), 0, 'S', 'N')
				into	ie_bloquear_alta_w
				from	med_avaliacao_paciente
				where	nr_atendimento = nr_atendimento_p
				and		dt_liberacao is not null
				and 	ie_situacao = 'A'
				and		nr_seq_tipo_avaliacao = nr_consiste_obito_w;
				
				if (ie_bloquear_alta_w = 'S') then
					ds_avaliacao_alta_p := SUBSTR(obter_descricao_padrao('MED_TIPO_AVALIACAO','DS_TIPO', nr_consiste_obito_w),1,100);
				end if;
			end;
		end if;
		
	if	(ie_gerar_alta_uti_pac_p = 'S') then
		begin
		
		select	nvl(max('S'),'N')
		into	ie_atend_paciente_unidade_w
		from	atend_paciente_unidade
		where	nr_atendimento = nr_atendimento_p
		and	dt_saida_unidade is null
		and	obter_classif_setor(cd_setor_atendimento) = 4;
		
		select	nvl(max(ie_obito),'N')
		into	ie_obito_motivo_alta_w
		from	motivo_alta
		where	cd_motivo_alta	= cd_motivo_alta_p;
		
		if	(ie_atend_paciente_unidade_w = 'S') and
			(ie_obito_motivo_alta_w = 'N') then
			begin
			Wheb_mensagem_pck.exibir_mensagem_abort(198469);		
			end;
		end if;
		
		
	
		end;
	end if;
	
	if	(ie_alta_condicionada_p	= 'S') or
		(ie_alta_condicionada_p	= 'E')	then
		begin
		
		ie_gasto_pendente_w		:= obter_gasto_pendente(nr_atendimento_p);
		ie_gasto_pend_proced_eup_w	:= obter_gasto_pend_proced_eup(nr_atendimento_p,ie_alta_prescr_prev_p,dt_alta_p);
		
		if	(ie_gasto_pendente_w = 'S') or
			(ie_gasto_pend_proced_eup_w = 'S') then
			begin
			
			ds_erro_w		:= wheb_mensagem_pck.get_texto(279773);
			ds_erro_w		:= ds_erro_w || wheb_mensagem_pck.get_texto(279774);
			ie_prescr_dev_pend_w	:= 'S';
			end;
		end if;
		end;
	end if;
		
	if	(ie_alta_condicionada_p = 'P') then
		ie_gasto_pend_proced_eup_w	:= obter_gasto_pend_proced_eup(nr_atendimento_p,ie_alta_prescr_prev_p,dt_alta_p);
		if 	(ie_gasto_pend_proced_eup_w = 'S') then
				begin
				ds_erro_w		:= wheb_mensagem_pck.get_texto(279775);
				ds_erro_w		:= ds_erro_w || wheb_mensagem_pck.get_texto(279774);
				ie_prescr_dev_pend_w	:= 'S';
				end;
		end if;
	end if;
	
	if	(ie_alta_condicionada_p = 'D') then
		ie_gasto_pendente_w		:= obter_gasto_pendente(nr_atendimento_p);
		if 	(ie_gasto_pendente_w = 'S') then
				begin
				ds_erro_w		:= wheb_mensagem_pck.get_texto(279776);
				ds_erro_w		:= ds_erro_w || wheb_mensagem_pck.get_texto(279774);
				ie_prescr_dev_pend_w	:= 'S';
				end;
		end if;
	end if;
	
	if	(ds_erro_w is null) then
		begin
		
		ie_data_gasto_superior_w := obter_gasto_superior_nao_canc(nr_atendimento_p,dt_alta_p);
			
			
		if	(ie_exibir_grid_w = 'N') and
			(ie_permite_alta_lanc_post_p <> 'S') and
			(ie_data_gasto_superior_w = 'S') then
			begin
			
			ds_setores_w	:= obter_setor_gasto_sup_nao_canc(nr_atendimento_p,dt_alta_p);
			
			if	(ie_permite_alta_lanc_post_p = 'D')then
				begin
				
				select 	nvl(ie_bloq_alta_inf,'N') 
				into	ie_bloq_alta_inf_w
				from 	motivo_alta 
				where 	cd_motivo_alta = cd_motivo_alta_p;
				
				if	(ie_bloq_alta_inf_w = 'S')then
					begin
					Wheb_mensagem_pck.exibir_mensagem_abort(198470,'DS_SETORES='||ds_setores_w);										
					end;
				end if;
				
				end;
			end if;
			
			if	(ie_permite_alta_lanc_post_p = 'N')then
				begin
				Wheb_mensagem_pck.exibir_mensagem_abort(198470,'DS_SETORES='||ds_setores_w);				
				
				end;
			end if;
			
			end;
		end if;
		
		if	(ie_alta_prescr_prev_p <> 'S') then
			begin
		
			ie_prescr_prev_w	:= obter_se_prescr_prev(nr_atendimento_p,dt_alta_p);
		
			if	(ie_prescr_prev_w = 'S') then
				begin
				Wheb_mensagem_pck.exibir_mensagem_abort(198471);				
				end;
			end if;
			end;
		end if;
	
		ie_mat_pend_adep_w	:= obter_se_atend_pendente_adm(nr_atendimento_p,'N');
	
		if	(ie_mat_pend_adep_alta_p <> 'N') and
			(ie_mat_pend_adep_alta_p <> 'D') and
			(ie_mat_pend_adep_w = 'N') then							
			
			if	(ie_mat_pend_adep_alta_p = 'S') then
				Wheb_mensagem_pck.exibir_mensagem_abort(198472);				
			else	
				ds_erro_w	:= wheb_mensagem_pck.get_texto(279777);
			end if;
								
		elsif	(ie_mat_pend_adep_alta_p = 'D') and
			(ie_mat_pend_adep_w = 'N') then
			
			select	decode(count(1),0,'N','S')
			into	ie_itens_nao_adm_p
			from	itens_nao_administrados_v
			where	((ie_consiste_proc_rec_w = 'S') or (ie_tipo = 1))
			and	nr_atendimento = nr_atendimento_p;
		end if;
		
		if	(ie_consiste_transferencia_p = 'S') or
			(ie_consiste_empresa_transp_p = 'S') then
			begin
			
			select	nvl(ie_transferencia,'N')
			into	ie_transferencia_w
			from	motivo_alta
			where	cd_motivo_alta	= cd_motivo_alta_p;
			
			if	(ie_transferencia_w = 'S') then
				begin
				if	(ie_consiste_transferencia_p = 'S') and
					(cd_motivo_alta_p is not null) and
					(cd_hospital_dest_p is null) then
					begin
					Wheb_mensagem_pck.exibir_mensagem_abort(198473);									
					end;
				end if;
				
				if	(ie_consiste_empresa_transp_p = 'S') and
					(cd_empresa_transp_p is null) then
					begin
					Wheb_mensagem_pck.exibir_mensagem_abort(198474);					
					end;
				end if;
				
				end;
			end if;
			end;
		end if;
		
		ds_valor_parametro_w := obter_valor_param_usuario(
			3111, 195, wheb_usuario_pck.get_cd_perfil,
			wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
		if	(ds_valor_parametro_w = 'S') and
			(ds_observacao_p is null) then
			begin
			ie_exige_just_mot_alta_w := exige_justificativa_mot_alta(cd_motivo_alta_p);
			if	(ie_exige_just_mot_alta_w = 'S') then
				begin
				Wheb_mensagem_pck.exibir_mensagem_abort(105055);				
				end;
			end if;
			end;
		end if;
	
		
		cd_setor_atend_w	:= obter_unidade_atendimento(nr_atendimento_p,'A','CS');
		cd_unidade_basica_w	:= obter_unidade_atendimento(nr_atendimento_p,'A','UB');
		cd_unidade_compl_w	:= obter_unidade_atendimento(nr_atendimento_p,'A','UC');
		
		if	(cd_setor_atend_w <> null) then
			begin
			select	qt_minuto_max
			into	qt_minuto_max_w
			from	setor_atendimento
			where	cd_setor_atendimento	= cd_setor_atend_w;
			end;
		else
			qt_minuto_max_w	:= 0;
		end if;
		
		if	(qt_minuto_max_w <> 0) then
			begin
			qt_min_perm_setor_w	:= obter_min_permanencia_setor(nr_atendimento_p,cd_setor_atend_w,cd_unidade_basica_w,cd_unidade_compl_w);
			
			if	(qt_minuto_max_w > qt_min_perm_setor_w) and
				(cd_motivo_permanencia_p = null) then
				begin
				Wheb_mensagem_pck.exibir_mensagem_abort(198475);				
				end;
			end if;
			
			ie_inserir_motivo_perm_w	:= 'S';
			end;
		end if;

		/* Movimenta��o de Pacientes - Par�metro [170] - Bloquear a gera��o de alta ap�s 24 horas do atendimento de Pronto Socorro */
		obter_param_usuario(3111, 170, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_impedir_alta_ps_w);

		if	(ie_impedir_alta_ps_w = 'S') then
			begin
			select	verifica_tempo_internacao_ps(nr_atendimento_p,dt_alta_p)
			into	ie_consiste_ps_w
			from	dual;

			if	(ie_consiste_ps_w = 'S') then
				begin
				ds_erro_w	:= substr(obter_texto_tasy(60403, wheb_usuario_pck.get_nr_seq_idioma),1,255);
				end;
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;
ds_erro_p			:= ds_erro_w;
ie_prescr_dev_pend_p		:= ie_prescr_dev_pend_w;
ie_inserir_motivo_perm_p	:= ie_inserir_motivo_perm_w;
commit;
end consiste_gerar_estornar_alta;
/