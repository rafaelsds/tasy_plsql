CREATE OR REPLACE PROCEDURE PRC_VAP_REPORT_CALLER_INCID(
  start_date_p DATE,
  end_date_p DATE,
  age_p NUMBER,
  cd_setor_p VARCHAR2) IS
BEGIN
  INSERT INTO W_VAP_REPORT_LOG(
    nr_sequencia,
    dt_inicio, 
    dt_fim, 
    nr_idade, 
    cd_setor, 
    ie_protocolo,
    ie_incidencia,
    nm_usuario) 
  VALUES(
    w_vap_report_log_seq.nextval, start_date_p, end_date_p, NVL(age_p, 0), NVL(cd_setor_p, '0'), 'N', 'S', wheb_usuario_pck.get_nm_usuario);
  --
  COMMIT;
  --
  PKG_VAP_REPORT.RUN(start_date_p, end_date_p, NVL(age_p, 0), NVL(cd_setor_p, '0'), 'N', 'S');
  --
END;
/
