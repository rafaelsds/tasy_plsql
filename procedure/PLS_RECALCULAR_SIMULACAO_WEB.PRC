create or replace
procedure pls_recalcular_simulacao_web(	nr_seq_simulacao_p	number,
					nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Recalcular a simula��o de pre�o gerada pela Solicita��o de Proposta on-line
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ x] Outros: HTML5
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

vl_simulacao_w		pls_simulacao_preco.vl_simulacao%type;	

Cursor C01 is
	select	nr_sequencia
	from	pls_simulpreco_individual
	where	nr_seq_simulacao	= nr_seq_simulacao_p
	and	ie_tipo_benef <> 'R';

begin

pls_lanc_bonific_simul_online(nr_seq_simulacao_p, null, '1', nm_usuario_p);

for C01_w in C01 loop
	begin
	vl_simulacao_w	:= 0;
	
	select	vl_item
	into	vl_simulacao_w
	from	table(pls_simulacao_preco_pck.obter_resumo_simulacao(nr_seq_simulacao_p,null,null,null,'S'))
	where	nr_seq_simul_individual	= C01_w.nr_sequencia
	and	ie_totalizador = 'S';
	
	update	pls_simulpreco_individual
	set	vl_mensalidade		= vl_simulacao_w, 
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= C01_w.nr_sequencia;
	end;
end loop;

vl_simulacao_w	:= pls_simulacao_preco_pck.obter_valor_simulacao(nr_seq_simulacao_p,'I',null,null,null);
	
update	pls_simulacao_preco
set	vl_simulacao		= vl_simulacao_w,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_simulacao_p;
	
commit;

end pls_recalcular_simulacao_web;
/