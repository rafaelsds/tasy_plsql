create or replace
procedure ptu_gestao_env_resp_ordem_serv
			(	nr_seq_ordem_p		Number,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 

nr_versao_ptu_w			Varchar2(255);

begin

--dbms_application_info.SET_ACTION('TASY_SCS');

nr_versao_ptu_w	:= pls_obter_versao_scs;

if	(nr_versao_ptu_w	= '035') then
	ptu_gerar_resp_ordem_serv(nr_seq_ordem_p, cd_estabelecimento_p, nm_usuario_p);
elsif	(nr_versao_ptu_w	= '040') then
	ptu_gerar_resp_ordem_serv_v40(nr_seq_ordem_p, cd_estabelecimento_p, nm_usuario_p);
elsif	(nr_versao_ptu_w	= '050') then
	ptu_gerar_resp_ordem_serv_v50(nr_seq_ordem_p, nr_versao_ptu_w, cd_estabelecimento_p, nm_usuario_p);
end if;

--dbms_application_info.SET_ACTION('');

end ptu_gestao_env_resp_ordem_serv;
/