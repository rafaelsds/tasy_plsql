create or replace 
procedure ageint_gerar_wpanel_angular(	/*PARaMETROS LIBERAcaO DE AGENDAS*/
										nr_seq_ageint_p			number,
										cd_estabelecimento_p	number,
										cd_estab_agenda_p		number,
										nr_seq_regiao_p			number,
										cd_estab_multimed_p		number,
										cd_empresa_multimed_p	number,
										nm_usuario_p			varchar2,
										ie_transferencia_p		varchar2,
										ds_itens_selec_p		varchar2,
										cd_medico_exec_p		varchar2,
										dt_agenda_p				date,
										/*PARaMETROS GERAcaO DE HORaRIOS*/
										ds_agendas_montadas_p	varchar2 default null,
										ds_erro_p			    out varchar2,
										cd_regiao_p  		number default null,
										ie_prazo_maximo_p       varchar2 default 'N',
										ie_genero_prof_p        varchar2 default 'A',
                                        ie_limitrofes_p         varchar2 default 'N') is
qt_agenda_lib_w		number(10) := 0; 
ds_erro_w			varchar2(1);
ds_hor_min_w		number(10);
ds_hor_max_w		number(10); 
ds_hor_min_livre_w	number(10);  
ds_hor_max_livre_w	number(10); 
dt_agenda_w			date;
qt_agendamento_quimio_w	number(10) := 0;
 
begin
dt_agenda_w := to_date(to_char(nvl(dt_agenda_p,sysdate), 'dd/mm/yyyy'), 'dd/mm/yyyy');

if	(nr_seq_ageint_p is not null) then

	Gerar_AgeInt_lib_Usuario(
							nr_seq_ageint_p,
							cd_estabelecimento_p,
							cd_estab_agenda_p,
							nr_seq_regiao_p,
							cd_estab_multimed_p,
							cd_empresa_multimed_p,
							nm_usuario_p,
							ie_transferencia_p,
							ds_itens_selec_p,
							cd_medico_exec_p,
							dt_agenda_p,
              cd_regiao_p,
			  ie_genero_prof_p,
              ie_limitrofes_p);
end if;

select	count(1)
into	qt_agenda_lib_w
from 	ageint_lib_usuario
where 	nr_seq_ageint = nr_seq_ageint_p
and 	nm_usuario = nm_usuario_p;

if	(qt_agenda_lib_w > 0) then

	gerar_horarios_ageint(
							dt_agenda_w,
							nm_usuario_p,
							nr_seq_ageint_p,
							cd_estabelecimento_p,
							ds_agendas_montadas_p,
							ds_hor_min_w,
							ds_hor_max_w,
							ds_hor_min_livre_w,
							ds_hor_max_livre_w,
							ie_prazo_maximo_p);
end if;

end ageint_gerar_wpanel_angular;
/
