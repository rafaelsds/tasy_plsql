create or replace
procedure fis_gerar_reg_F100_efd
			(	nr_seq_controle_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				dt_inicio_p			date,
				dt_fim_p			date,
				cd_empresa_p			number,
				ds_separador_p			varchar2,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	IE_LOCAL_GERAR_SPED_W
		Par�metro 5500 - [25] - SPED PIS/COFINS - Ao gerar os dados,  de onde buscar as informa��es
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

aliquota_pis_w			number(7,4);
aliquota_cofins_w		number(7,4);
cd_tributo_pis_w		number(3);
cd_tributo_cofins_w		number(3);					
nr_seq_regra_efd_w		number(10);
nr_versao_efd_w			varchar2(5);
tp_registro_w			varchar2(4);
cd_municipio_ibge_w		varchar2(7);
nr_linha_w			number(10) 	:= qt_linha_p;
nr_seq_registro_w		number(10) 	:= nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1)	:= ds_separador_p;
qt_registros_w			number(10);
cd_empresa_w			number(4);
nr_seq_data_w			number(10);
ie_local_gerar_sped_w		varchar2(1);
vl_pis_w			number(15,2);
vl_cofins_w			number(15,2);

cursor c01 is
	select	'F100'					tp_registro,
		1					ind_operacao,
		to_char(dt_inicio_p,'ddmmyyyy')		dt_operacao,
		sum(vl_movimento)			vl_operacao,
		nvl(ie_tributacao_cst,'01') 		cst_pis,
		sum(vl_base_pis)			base_calculo_pis,
		nvl(ie_tributacao_cst_cofins,'01')	cst_cofins,
		sum(vl_base_cofins)			base_calculo_cofins,
		''					base_calculo_Creditos,
		pr_aliq_pis				pr_aliquota_pis,
		pr_aliq_cofins				pr_aliquota_cofins,
	        cd_conta_contabil
	from	(select	decode(nvl(c.ie_tipo_valor,'VM'),'VM',nvl(a.vl_movimento,0),'VC',nvl(a.vl_credito,0),'VD',nvl(a.vl_debito,0)) vl_movimento,
			decode(nvl(c.ie_tipo_valor,'VM'),'VM',nvl(a.vl_movimento,0),'VC',nvl(a.vl_credito,0),'VD',nvl(a.vl_debito,0),'Z',0) vl_base_pis,
			decode(nvl(c.ie_tipo_valor_cofins,'VM'),'VM',nvl(a.vl_movimento,0),'VC',nvl(a.vl_credito,0),'VD',nvl(a.vl_debito,0),'Z',0) vl_base_cofins,
			c.ie_tributacao_cst,
			c.ie_tributacao_cst_cofins,
			fis_obter_aliq_pis_cofins(c.ie_regime_receita,'PIS')pr_aliq_pis,
			fis_obter_aliq_pis_cofins(c.ie_regime_receita,'COFINS')pr_aliq_cofins,
			a.cd_conta_contabil
		from 	ctb_saldo		a,
			fis_efd_conta_contabil	c,
			fis_efd_regra_tipo_ct	r
		where 	a.cd_conta_contabil	= c.cd_conta_contabil
		and	c.nr_seq_tipo_ct	= r.nr_sequencia
		and	a.nr_seq_mes_ref	= nr_seq_data_w
		and	r.ie_tipo_ct		= 'F'
		and	nvl(c.cd_estabelecimento, cd_estabelecimento_p) = a.cd_estabelecimento
		and 	a.cd_estabelecimento = cd_estabelecimento_p		
		and	c.cd_empresa = cd_empresa_p)
	group by dt_inicio_p,ie_tributacao_cst, ie_tributacao_cst_cofins, '01', 'F100', pr_aliq_pis, pr_aliq_cofins, cd_conta_contabil;

/*cursor c02 is
	select	'F100'					tp_registro,
		2					ind_operacao,
		to_char(dt_inicio_p,'ddmmyyyy')		dt_operacao,
		a.vl_movimento 				vl_operacao,
		nvl(c.ie_tributacao_cst,'06')		cst_pis,
		a.vl_movimento 				base_calculo_pis,
		nvl(c.ie_tributacao_cst,'06')		cst_cofins,
		a.vl_movimento 				base_calculo_cofins,
		''					base_calculo_Creditos,
		0					pr_aliquota_pis,
		0					pr_aliquota_cofins,
		a.cd_conta_contabil
	from 	ctb_balancete_v		a,
		fis_efd_conta_contabil	c
	where 	a.nr_seq_mes_ref		= nr_seq_data_w
	and	a.cd_conta_contabil		= '2436'
	and	a.cd_estabelecimento		= cd_estabelecimento_p
	and	a.ie_normal_encerramento	= 'N'
	and	a.cd_estabelecimento		= nvl(c.cd_estabelecimento, cd_estabelecimento_p)
	and	obter_empresa_estab(a.cd_estabelecimento) = nvl(c.cd_empresa, cd_empresa_p)	
	union
	select	'F100'					tp_registro,
		1					ind_operacao,
		to_char(dt_inicio_p,'ddmmyyyy')		dt_operacao,
		a.vl_movimento 				vl_operacao,
		nvl(c.ie_tributacao_cst,'01') 		cst_pis,
		a.vl_movimento 				base_calculo_pis,
		nvl(c.ie_tributacao_cst,'01') 		cst_cofins,
		a.vl_movimento 				base_calculo_cofins,
		''					base_calculo_Creditos,
		1.65					pr_aliquota_pis,
		7.6					pr_aliquota_cofins,
		a.cd_conta_contabil
	from 	ctb_balancete_v		a,
		fis_efd_conta_contabil	c
	where 	a.nr_seq_mes_ref		= nr_seq_data_w
	and	a.cd_conta_contabil		= '2441'
	and	a.cd_estabelecimento		= cd_estabelecimento_p
	and	a.ie_normal_encerramento	= 'N'
	and	a.cd_estabelecimento		= nvl(c.cd_estabelecimento, cd_estabelecimento_p)
	and	obter_empresa_estab(a.cd_estabelecimento) = nvl(c.cd_empresa, cd_empresa_p)	;*/
	
vet01	C01%RowType;
--vet02	C02%RowType;

begin	

select	cd_empresa
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

select	count(1)
into	qt_registros_w
from	ctb_mes_ref
where	trunc(dt_referencia,'mm')	= trunc(dt_inicio_p,'mm')
and	cd_empresa			= cd_empresa_w
and	rownum				= 1;

if	(qt_registros_w = 1) then
	select	nr_sequencia
	into	nr_seq_data_w
	from 	ctb_mes_ref 
	where 	trunc(dt_referencia,'mm')	= trunc(dt_inicio_p,'mm')
	and	cd_empresa	= cd_empresa_w;
end if;

select	nr_seq_regra_efd
into	nr_seq_regra_efd_w
from	fis_efd_controle
where	nr_sequencia	= nr_seq_controle_p;

select	cd_tributo_pis,
	cd_tributo_cofins
into	cd_tributo_pis_w,
	cd_tributo_cofins_w
from	fis_regra_efd
where	nr_sequencia	= nr_seq_regra_efd_w;

--Alterado para verificar se o imposto � cumulativo ou n�o cumulativo para definir a aliquota
--aliquota_pis_w 		:= obter_pr_imposto(cd_tributo_pis_w);
--aliquota_cofins_w 	:= obter_pr_imposto(cd_tributo_cofins_w);

obter_param_usuario(5500,25,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_local_gerar_sped_w);

--if	(ie_local_gerar_sped_w = 'N')  then
	open C01;
	loop
	fetch C01 into	
		vet01;
	exit when C01%notfound;
		begin
		vl_pis_w	:= abs(vet01.base_calculo_pis * (vet01.pr_aliquota_pis/100));
		vl_cofins_w	:= abs(vet01.base_calculo_cofins * (vet01.pr_aliquota_cofins/100));
		
		ds_linha_w	:= substr(	sep_w || vet01.tp_registro 							|| --texto fixo
						sep_w || vet01.ind_operacao							|| -- receita sujeita ao pagamento de PIS e COFINS
						sep_w || ''									|| -- participante
						sep_w || ''									|| -- item
						sep_w || vet01.dt_operacao							|| -- data da opera��o
						sep_w || replace(campo_mascara(abs(vet01.vl_operacao),2),'.',',')		|| -- valor da opera��o
						sep_w || vet01.cst_pis								|| -- cst pis
						sep_w || replace(campo_mascara(abs(vet01.base_calculo_pis),2),'.',',')		|| -- 8valor da base de c�lculo do pis
						sep_w || replace(campo_mascara(vet01.pr_aliquota_pis,2),'.',',')		|| -- 9valor da aliquota pis
						sep_w || replace(campo_mascara(vl_pis_w ,2),'.',',') 				|| -- 10valor do pis
						sep_w || vet01.cst_cofins	 						|| -- 11cst cofins
						sep_w || replace(campo_mascara(abs(vet01.base_calculo_cofins),2),'.',',')	|| -- 12valor da base de c�lculo do cofins
						sep_w || replace(campo_mascara(vet01.pr_aliquota_cofins,2),'.',',')		|| -- 13valor da aliquota cofins
						sep_w || replace(campo_mascara(vl_cofins_w ,2),'.',',') 			|| -- 14valor do cofins
						sep_w || '' 									|| -- base de c�lculo de cr�ditos
						sep_w || '' 									|| -- origem de cr�dito
						sep_w || vet01.cd_conta_contabil 						|| -- c�digo da conta
						sep_w || ''									|| -- c�digo do centro de custos
						sep_w || ''									|| -- descricao do documento/operacao
						sep_w,1,8000);
		
		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		nr_linha_w		:= nr_linha_w + 1;
		
		insert into fis_efd_0900 (	nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_controle,
									ds_registro,
									cd_cst,
									cd_ind_oper,
									vl_registro )
						values ( 	fis_efd_0900_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_controle_p,
									'F100',
									vet01.cst_cofins,
									vet01.ind_operacao,
									vet01.vl_operacao );
		
		insert into fis_efd_nota_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			nr_seq_nota_fiscal,
			cd_cgc,
			vl_total_receita,
			vl_base_calculo,
			vl_pis,
			vl_cofins,
			vl_pis_retido,
			vl_cofins_retido,
			nr_seq_controle)
		values	(fis_efd_nota_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			vet01.vl_operacao,
			vet01.base_calculo_pis,
			vl_pis_w,
			vl_cofins_w,
			0,
			0,
			nr_seq_controle_p);

		insert into fis_efd_arquivo
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_controle_efd,
			nr_linha,
			cd_registro,
			ds_arquivo,
			ds_arquivo_compl)
		values	(nr_seq_registro_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_controle_p,
			nr_linha_w,
			vet01.tp_registro,
			ds_arquivo_w,
			ds_arquivo_compl_w);	
		end;
	end loop;
	close C01;
/*elsif	(ie_local_gerar_sped_w = 'C') then
	open C02;
	loop
	fetch C02 into	
		vet02;
	exit when C02%notfound;
		begin
		vl_pis_w	:= abs(vet02.base_calculo_pis * (vet02.pr_aliquota_pis/100));
		vl_cofins_w	:= abs(vet02.base_calculo_pis * (vet02.pr_aliquota_cofins/100));
		
		ds_linha_w	:= substr(	sep_w || vet02.tp_registro 							|| --texto fixo
						sep_w || vet02.ind_operacao							|| -- receita sujeita ao pagamento de PIS e COFINS
						sep_w || ''									|| -- participante
						sep_w || ''									|| -- item
						sep_w || vet02.dt_operacao							|| -- data da opera��o
						sep_w || replace(campo_mascara(abs(vet02.vl_operacao),2),'.',',')		|| -- valor da opera��o
						sep_w || vet02.cst_pis								|| -- cst pis
						sep_w || replace(campo_mascara(abs(vet02.base_calculo_pis),2),'.',',')		|| -- valor da base de c�lculo do pis
						sep_w || replace(campo_mascara(vet02.pr_aliquota_pis,2),'.',',')		|| -- valor da aliquota pis
						sep_w || replace(campo_mascara(vl_pis_w,2),'.',',') 				|| -- valor do pis
						sep_w || vet02.cst_cofins	 						|| -- cst cofins
						sep_w || replace(campo_mascara(abs(vet02.base_calculo_cofins),2),'.',',')	|| -- valor da base de c�lculo do cofins
						sep_w || replace(campo_mascara(vet02.pr_aliquota_cofins,2),'.',',')		|| -- valor da aliquota cofins
						sep_w || replace(campo_mascara(vl_cofins_w,2),'.',',')				|| -- valor do cofins
						sep_w || '' 									|| -- base de c�lculo de cr�ditos
						sep_w || '' 									|| -- origem de cr�dito
						sep_w || vet02.cd_conta_contabil 						|| -- c�digo da conta
						sep_w || ''									|| -- c�digo do centro de custos
						sep_w || ''									|| -- descricao do documento/operacao
						sep_w,1,8000);
		
		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		nr_linha_w		:= nr_linha_w + 1;
		
		insert into fis_efd_nota_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			nr_seq_nota_fiscal,
			cd_cgc,
			vl_total_receita,
			vl_base_calculo,
			vl_pis,
			vl_cofins,
			vl_pis_retido,
			vl_cofins_retido,
			nr_seq_controle)
		values	(fis_efd_nota_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			vet02.vl_operacao,
			vet02.base_calculo_pis,
			vl_pis_w,
			vl_cofins_w,
			0,
			0,
			nr_seq_controle_p);

		insert into fis_efd_arquivo
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_controle_efd,
			nr_linha,
			cd_registro,
			ds_arquivo,
			ds_arquivo_compl)
		values	(nr_seq_registro_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_controle_p,
			nr_linha_w,
			vet02.tp_registro,
			ds_arquivo_w,
			ds_arquivo_compl_w);
		end;
	end loop;
	close C02;
end if;*/
	
commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;	

end fis_gerar_reg_F100_efd;
/