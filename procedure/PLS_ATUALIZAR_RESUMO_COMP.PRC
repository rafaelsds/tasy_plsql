create or replace
procedure pls_atualizar_resumo_comp
			(	nr_seq_competencia_p	Number,
				nm_usuario_p		Varchar2) is 
				
nr_seq_conta_w			Number(10);
cd_estabelecimento_w		Number(4);
				
Cursor C01 is
	select	a.nr_sequencia,
		a.cd_estabelecimento
	from	pls_conta	a
	where	a.nr_seq_competencia	= nr_seq_competencia_p
	and	a.ie_status		= 'F'
	union
	select	a.nr_sequencia,
		a.cd_estabelecimento
	from	pls_protocolo_conta	b,
		pls_conta		a
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	a.nr_seq_competencia is null
	and	a.ie_status		= 'F'
	and	exists (	select	1
				from	pls_competencia	x
				where	trunc(x.dt_mes_competencia,'month')	= trunc(b.dt_mes_competencia,'month')
				and	x.cd_estabelecimento			= b.cd_estabelecimento
				and	x.nr_sequencia				= nr_seq_competencia_p);

begin

open C01;
loop
fetch C01 into	
	nr_seq_conta_w,
	cd_estabelecimento_w;
exit when C01%notfound;
	begin
	pls_atualizar_conta_resumo(nr_seq_conta_w, cd_estabelecimento_w, nm_usuario_p);
	end;
end loop;
close C01;

commit;

end pls_atualizar_resumo_comp;
/