create or replace
procedure alterar_vl_proc_repasse_js(
			nr_sequencia_p		number,
			nm_usuario_p		varchar2,
			vl_novo_repasse_p		number) is 

begin

update	procedimento_repasse
set 	vl_repasse	= vl_novo_repasse_p,
	vl_liberado	= decode(vl_liberado,0,0,vl_novo_repasse_p),
	nm_usuario	= nm_usuario_p
where 	nr_sequencia	= nr_sequencia_p;

commit;

end alterar_vl_proc_repasse_js;
/