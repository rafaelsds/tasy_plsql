create or replace
procedure gen_care_plan_import_log(nm_usuario_p		in	Varchar2,
								nr_seq_superior_p	in	number,
								ds_filename_p		in	varchar2,
								nr_seq_out_p		out	number) is 

nr_sequencia_w		care_plan_import_log.nr_sequencia%type;	

begin

select	care_plan_import_log_seq.nextval
into	nr_sequencia_w
from	dual;

insert into care_plan_import_log
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	si_main_process,
	ds_filename,
	nr_seq_superior,
	si_status)
values
	(nr_sequencia_w,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	decode(nr_seq_superior_p,null,'Y','N'),
	ds_filename_p,
	nr_seq_superior_p,
	0);
	
commit;

nr_seq_out_p	:= nr_sequencia_w;

end gen_care_plan_import_log;
/