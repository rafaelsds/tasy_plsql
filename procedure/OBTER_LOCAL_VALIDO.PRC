CREATE OR REPLACE
PROCEDURE Obter_Local_Valido( 	cd_estabelecimento_p		Number,
					cd_local_estoque_p		Number,
				   	cd_material_p			number,
					ie_origem_documento_p	varchar2,
                 			ie_local_valido_p	out	Varchar2) IS 

cd_grupo_material_w	number(5);
cd_subgrupo_material_w	number(5);
cd_classe_material_w	number(5);
ie_local_valido_w	varchar2(1) := 'N';
ie_origem_doc_w		varchar2(15);
nr_seq_familia_w	number(10);

/*	Tipos de documento
	1	Notas Fiscais
	2	Requisi��es
	3	Prescri��o
	5	Invent�rio
	7	Produ��o
	8	Empr�stimo
	9	Digita��o Movimento
	10	Ordem de produ��o*/

cursor c01 is
	select	nvl(ie_liberar,'S')
	from	loc_estoque_estrut_materiais
	where 	cd_local_estoque					= cd_local_estoque_p
	and	nvl(cd_material,cd_material_p)			= cd_material_p
	and 	nvl(cd_classe_material,cd_classe_material_w)	= cd_classe_material_w
	and 	nvl(cd_subgrupo_material,cd_subgrupo_material_w)= cd_subgrupo_material_w
	and 	nvl(cd_grupo_material,cd_grupo_material_w)	= cd_grupo_material_w
	and 	nvl(nr_seq_familia,nr_seq_familia_w)		= nr_seq_familia_w
	and	nvl(ie_origem_documento, ie_origem_doc_w)		= ie_origem_doc_w
	order by nvl(cd_material,0),
		nvl(nr_seq_familia,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0);

BEGIN
select	nvl(max(a.cd_classe_material),0),
	nvl(max(b.cd_subgrupo_material),0),
	nvl(max(c.cd_grupo_material),0),
	nvl(max(a.nr_seq_familia),0)
into	cd_classe_material_w,
	cd_subgrupo_material_w,
	cd_grupo_material_w,
	nr_seq_familia_w
from	material a,
	classe_material b,
	subgrupo_material c
where	a.cd_material 		= cd_material_p
and	a.cd_classe_material 	= b.cd_classe_material
and	b.cd_subgrupo_material 	= c.cd_subgrupo_material;

ie_origem_doc_w			:= nvl(ie_origem_documento_p,'All');
ie_local_valido_w			:= 'N';

OPEN C01;
LOOP
FETCH C01 into
	ie_local_valido_w;
EXIT WHEN C01%NOTFOUND;
	ie_local_valido_w	:= ie_local_valido_w;
END LOOP;
CLOSE C01;

ie_local_valido_p := ie_local_valido_w;

END Obter_Local_Valido;
/