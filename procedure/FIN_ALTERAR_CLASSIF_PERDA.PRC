create or replace
procedure fin_alterar_classif_perda(
			nr_seq_perda_p		number,
			ie_tipo_perda_p		varchar2,
			nm_usuario_p		varchar2) is 

cd_estabelecimento_w	number(4);
vl_saldo_w		number(15,2);
ie_tipo_perda_w		varchar2(1);
nr_seq_tipo_baixa_w	number(10);
nr_sequencia_w		number(5);
nr_sequencia_ww		number(5);

nr_seq_cobranca_w	number(10);
nr_seq_cheque_w		number(10);
nr_titulo_w		number(10);
nr_seq_hist_cob_w		number(10);

begin

begin
select	nvl(vl_saldo,0),
	nvl(ie_tipo_perda,'R'),
	nr_seq_cheque,
	nr_titulo,
	cd_estabelecimento
into	vl_saldo_w,
	ie_tipo_perda_w,
	nr_seq_cheque_w,
	nr_titulo_w,
	cd_estabelecimento_w
from	perda_contas_receber
where	nr_sequencia = nr_seq_perda_p;
exception
when others then
	ie_tipo_perda_w := 'X';
end;

select	max(nr_sequencia)
into	nr_seq_tipo_baixa_w
from	fin_tipo_baixa_perda
where	ie_tipo_consistencia = '5'
and	ie_situacao = 'A';

if	(nr_seq_tipo_baixa_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(214619); 
end if;

if	((ie_tipo_perda_w = 'R') and (vl_saldo_w > 0) and (ie_tipo_perda_p = 'I')) or
	((ie_tipo_perda_w = 'I') and (ie_tipo_perda_p = 'R')) then
	begin
	if	(ie_tipo_perda_p = 'I')	 then
		begin		
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_w
		from	perda_contas_receb_baixa
		where	nr_seq_perda = nr_seq_perda_p;


		insert into perda_contas_receb_baixa(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_perda,
			dt_baixa,
			vl_baixa,
			nr_seq_tipo_baixa,
			ie_acao)
		values (	nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_perda_p,
			sysdate,
			vl_saldo_w,
			nr_seq_tipo_baixa_w,
			1);
			
		fin_atualizar_saldo_perda(nr_seq_perda_p, nm_usuario_p);
		
		update	perda_contas_receber
		set	ie_tipo_perda = ie_tipo_perda_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			dt_liquidacao = sysdate
		where	nr_sequencia = nr_seq_perda_p;
		end;
	else
		begin	
		
		select	nvl(max(a.vl_baixa),0)
		into	vl_saldo_w
		from	perda_contas_receb_baixa a,
				fin_tipo_baixa_perda b
		where	a.nr_seq_tipo_baixa = b.nr_sequencia
		and		b.ie_tipo_consistencia in (5,2)
		and		a.vl_baixa > 0
		and		nvl(ie_acao,1) = 1
		and		a.nr_seq_perda = nr_seq_perda_p
		and		a.nr_sequencia = (select max(x.nr_sequencia)
								  from	 perda_contas_receb_baixa x
								  where	 x.nr_seq_perda = a.nr_seq_perda);
		
		
		if	(vl_saldo_w > 0) then
			begin
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	perda_contas_receb_baixa
			where	nr_seq_perda = nr_seq_perda_p;
			
			insert into perda_contas_receb_baixa(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_perda,
				dt_baixa,
				vl_baixa,
				nr_seq_tipo_baixa,
				ie_acao)
			values (	nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_perda_p,
				sysdate,
				vl_saldo_w * -1,
				nr_seq_tipo_baixa_w,
				1);
				
			fin_atualizar_saldo_perda(nr_seq_perda_p, nm_usuario_p);
			
			update	perda_contas_receber
			set	ie_tipo_perda = ie_tipo_perda_p,
				nm_usuario = nm_usuario_p,
				dt_atualizacao = sysdate,
				dt_liquidacao = null
			where	nr_sequencia = nr_seq_perda_p;
			end;
		end if;
		end;
	end if;
	
	fin_atualiza_cobranca_perda(nr_seq_perda_p, 'AS', 'N', nm_usuario_p);

	commit;
	end;
end if;

end fin_alterar_classif_perda; 
/