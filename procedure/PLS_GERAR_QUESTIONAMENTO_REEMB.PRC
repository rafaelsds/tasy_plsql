create or replace
procedure pls_gerar_questionamento_reemb
			(	nr_seq_conta_proc_p		pls_conta_proc.nr_sequencia%type,
				nr_seq_conta_mat_p		pls_conta_mat.nr_sequencia%type,
				nr_seq_conta_p			pls_conta.nr_sequencia%type,
				nr_seq_contestacao_p		ptu_camara_contestacao.nr_sequencia%type,
				ie_tipo_tabela_p		varchar2,
				cd_interface_p			interface.cd_interface%type,
				nm_usuario_p			usuario.nm_usuario%type,
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type) is

--Ie_tipo_tabela
--'P' Procedimento
--'M' Material 

-- Versao PTU
ie_versao_w			number(10);

ie_tipo_acordo_w		varchar2(10);
ie_tipo_w			varchar2(1);
nr_seq_analise_w		pls_analise_conta.nr_sequencia%type;
nr_seq_mot_questionamento_w	ptu_motivo_questionamento.nr_sequencia%type;
cd_motivo_w			ptu_motivo_questionamento.cd_motivo%type;
ie_parecer_analise_w		ptu_motivo_questionamento.ie_parecer_analise%type;
ds_parecer_aux_w		pls_analise_fluxo_item.ds_parecer%type;
ds_parecer_w			varchar2(4000);

-- 507
nr_seq_registro_rrs_w		ptu_questionamento_rrs.nr_sequencia%type;
nr_seq_nota_cobr_rrs_w		ptu_nota_cobranca_rrs.nr_sequencia%type;
nr_seq_fatura_w			ptu_nota_cobranca_rrs.nr_seq_fatura%type;
nr_lote_w			ptu_nota_cobranca_rrs.nr_lote%type;
nr_nota_w			ptu_nota_cobranca_rrs.nr_nota%type;
cd_unimed_w			ptu_nota_cobranca_rrs.cd_unimed%type;
id_benef_w			ptu_nota_cobranca_rrs.id_benef%type;
nm_beneficiario_w		ptu_nota_cobranca_rrs.nm_beneficiario%type;
dt_nasc_w			ptu_nota_cobranca_rrs.dt_nasc%type;
tp_sexo_w			ptu_nota_cobranca_rrs.tp_sexo%type;
id_rn_w				ptu_nota_cobranca_rrs.id_rn%type;
dt_servico_w			ptu_nota_servico_rrs.dt_servico%type;
dt_reembolso_w			ptu_nota_cobranca_rrs.dt_reembolso%type;
tp_carater_atend_w		ptu_nota_cobranca_rrs.tp_carater_atend%type;
tp_pessoa_w			ptu_nota_cobranca_rrs.tp_pessoa%type;
nr_cnpj_cpf_w			ptu_nota_cobranca_rrs.nr_cnpj_cpf%type;
nm_prestador_w			ptu_nota_cobranca_rrs.nm_prestador%type;
nm_profissional_w		ptu_nota_servico_rrs.nm_profissional%type;
sg_cons_prof_w			ptu_nota_servico_rrs.sg_cons_prof%type;
nr_cons_prof_w			ptu_nota_servico_rrs.nr_cons_prof%type;
sg_uf_cons_prof_w		ptu_nota_servico_rrs.sg_uf_cons_prof%type;
tp_particip_w			ptu_nota_servico_rrs.tp_particip%type;
tp_tabela_w			ptu_nota_servico_rrs.tp_tabela%type;
cd_servico_w			ptu_nota_servico_rrs.cd_servico%type;
qt_cobrada_w			ptu_nota_servico_rrs.qt_cobrada%type;
vl_dif_vl_inter_w		ptu_nota_servico_rrs.vl_dif_vl_inter%type;
vl_serv_cob_w			ptu_nota_servico_rrs.vl_serv_cob%type;
nr_autoriz_w			ptu_nota_servico_rrs.nr_autoriz%type;
nr_seq_a500_w			ptu_nota_servico_rrs.nr_seq_a500%type;
nr_seq_conta_proc_w		ptu_nota_servico_rrs.nr_seq_conta_proc%type;
nr_seq_conta_mat_w		ptu_nota_servico_rrs.nr_seq_conta_mat%type;
nr_seq_conta_w			ptu_nota_cobranca_rrs.nr_seq_conta%type;
nr_nota_numerico_w		ptu_nota_cobranca_rrs.nr_nota_numerico%type;
cd_procedimento_w		ptu_nota_servico_rrs.cd_procedimento%type;
ie_origem_proced_w		ptu_nota_servico_rrs.ie_origem_proced%type;
nr_seq_proc_partic_w		ptu_nota_servico_rrs.nr_seq_proc_partic%type;
nr_seq_nota_serv_rrs_w		ptu_nota_servico_rrs.nr_sequencia%type;
vl_reconh_serv_w		ptu_quest_serv_rrs.vl_reconh_serv%type;
vl_glosa_w			pls_conta_proc.vl_glosa%type;
nr_seq_quest_serv_rrs_w		ptu_quest_serv_rrs.nr_sequencia%type;
nr_versao_transacao_w		varchar2(255);
ie_versao_xml_w      		number(10);
ie_tipo_tabela_tiss_w		ptu_nota_servico.ie_tipo_tabela_tiss%type;
ie_tipo_tabela_w		number(10);

Cursor C01 is 
	select	i.nr_seq_motivo
	from	ptu_questionamento_item i
	where	i.nr_seq_conta_proc	= nr_seq_conta_proc_p
	and	ie_tipo_w	= 'P'
	and	i.nr_seq_motivo is not null
	and	ie_tipo_acordo_w <> '11' -- Quando o item for nao contestado, so deve colocar motivo 99
	and	not exists (	select	1
				from	ptu_questionamento_codigo q
				where	q.nr_seq_mot_questionamento 	= i.nr_seq_motivo
				and	q.nr_seq_registro_rrs		= nr_seq_registro_rrs_w)
	union
	select	i.nr_seq_motivo
	from	ptu_questionamento_item i
	where	i.nr_seq_conta_mat	= nr_seq_conta_mat_p
	and	ie_tipo_w	= 'M'
	and	i.nr_seq_motivo is not null
	and	ie_tipo_acordo_w <> '11' -- Quando o item for nao contestado, so deve colocar motivo 99
	and	not exists (	select	1
				from	ptu_questionamento_codigo q
				where	q.nr_seq_mot_questionamento 	= i.nr_seq_motivo
				and	q.nr_seq_registro_rrs		= nr_seq_registro_rrs_w)
	union
	select	i.nr_sequencia
	from	ptu_motivo_questionamento i
	where	i.cd_motivo		= '99'
	and	ie_tipo_acordo_w	= '11' -- Quando o item for nao contestado, so deve colocar motivo 99
	and	not exists (	select	1
				from	ptu_questionamento_codigo q
				where	q.nr_seq_mot_questionamento 	= i.nr_sequencia
				and	q.nr_seq_registro_rrs		= nr_seq_registro_rrs_w);
	
Cursor C03 (	nr_seq_analise_pc		pls_conta.nr_seq_analise%type,
		nr_seq_conta_proc_pc		pls_conta_proc.nr_sequencia%type,
		nr_seq_conta_mat_pc		pls_conta_mat.nr_sequencia%type)is
	select	a.ds_parecer
	from	pls_grupo_auditor	b,
		pls_analise_fluxo_item	a
	where	b.nr_sequencia(+)	= a.nr_seq_grupo
	and	a.nr_seq_analise	= nr_seq_analise_pc
	and	a.nr_seq_conta_proc	= nr_seq_conta_proc_pc
	and	a.ds_parecer 		is not null
	and	a.ie_acao_item		= 'G'
	union
	select	a.ds_parecer
	from	pls_grupo_auditor	b,
		pls_analise_fluxo_item	a
	where	b.nr_sequencia(+)	= a.nr_seq_grupo
	and	a.nr_seq_analise	= nr_seq_analise_pc
	and	a.nr_seq_conta_mat	= nr_seq_conta_mat_pc
	and	a.ds_parecer 		is not null
	and	a.ie_acao_item		= 'G'
	group by a.ds_parecer;

begin 
ie_tipo_acordo_w	:= '00'; -- O processo se inicia com o tipo de acordo como Questionamento em Negociacao 
ie_tipo_w 		:= ie_tipo_tabela_p;
vl_reconh_serv_w	:= 0;
ie_versao_w 		:= somente_numero(ptu_obter_versao_dominio('A550', cd_interface_p));

if	(ie_tipo_w in ('P','Q')) then
	select	c.nr_sequencia,	
		c.nr_seq_fatura,
		c.nr_lote,
		c.nr_nota,
		c.cd_unimed,
		c.id_benef,
		c.nm_beneficiario,
		c.dt_nasc,
		c.tp_sexo,
		c.id_rn,
		s.dt_servico,
		c.dt_reembolso,
		c.tp_carater_atend,
		c.tp_pessoa,
		c.nr_cnpj_cpf,
		c.nm_prestador,
		s.nm_profissional,
		s.sg_cons_prof,
		s.nr_cons_prof,
		s.sg_uf_cons_prof,
		s.tp_particip,
		s.tp_tabela,
		s.cd_servico,
		s.qt_cobrada,
		s.vl_dif_vl_inter,
		s.vl_serv_cob,
		s.nr_autoriz,
		nvl(s.nr_seq_a500,s.nr_seq_item),
		s.nr_seq_conta_proc,
		s.nr_seq_conta_mat,
		c.nr_seq_conta,
		c.nr_nota_numerico,
		s.cd_procedimento,
		s.ie_origem_proced,
		s.nr_seq_proc_partic,
		s.nr_sequencia,
		p.vl_glosa,
		s.tp_tabela
	into	nr_seq_nota_cobr_rrs_w,
		nr_seq_fatura_w,
		nr_lote_w,
		nr_nota_w,
		cd_unimed_w,
		id_benef_w,
		nm_beneficiario_w,
		dt_nasc_w,
		tp_sexo_w,
		id_rn_w,
		dt_servico_w,
		dt_reembolso_w,
		tp_carater_atend_w,
		tp_pessoa_w,
		nr_cnpj_cpf_w,
		nm_prestador_w,
		nm_profissional_w,
		sg_cons_prof_w,
		nr_cons_prof_w,
		sg_uf_cons_prof_w,
		tp_particip_w,
		tp_tabela_w,
		cd_servico_w,
		qt_cobrada_w,
		vl_dif_vl_inter_w,
		vl_serv_cob_w,
		nr_autoriz_w,
		nr_seq_a500_w,
		nr_seq_conta_proc_w,
		nr_seq_conta_mat_w,
		nr_seq_conta_w,
		nr_nota_numerico_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_partic_w,
		nr_seq_nota_serv_rrs_w,
		vl_glosa_w,
		ie_tipo_tabela_w
	from	ptu_nota_servico_rrs	s,
		ptu_nota_cobranca_rrs	c,
		pls_conta_proc		p
	where	c.nr_sequencia		= s.nr_seq_nota_cobr_rrs
	and	p.nr_sequencia		= s.nr_seq_conta_proc
	and	s.nr_seq_conta_proc	= nr_seq_conta_proc_p;
	
	if	(sg_cons_prof_w is not null) then
		select nvl(max(a.cd_ptu),sg_cons_prof_w)
		into sg_cons_prof_w
		from conselho_profissional 	a
		where a.sg_conselho = sg_cons_prof_w;
	end if;
	
	pls_gerar_motivo_quest(	nr_seq_conta_proc_p, nr_seq_conta_p, 'P', cd_estabelecimento_p, nm_usuario_p);
	
elsif	(ie_tipo_w in ('M')) then
	select	c.nr_sequencia,	
		c.nr_seq_fatura,
		c.nr_lote,
		c.nr_nota,
		c.cd_unimed,
		c.id_benef,
		c.nm_beneficiario,
		c.dt_nasc,
		c.tp_sexo,
		c.id_rn,
		s.dt_servico,
		c.dt_reembolso,
		c.tp_carater_atend,
		c.tp_pessoa,
		c.nr_cnpj_cpf,
		c.nm_prestador,
		s.nm_profissional,
		s.sg_cons_prof,
		s.nr_cons_prof,
		s.sg_uf_cons_prof,
		s.tp_particip,
		s.tp_tabela,
		s.cd_servico,
		s.qt_cobrada,
		s.vl_dif_vl_inter,
		s.vl_serv_cob,
		s.nr_autoriz,
		nvl(s.nr_seq_a500,s.nr_seq_item),
		s.nr_seq_conta_proc,
		s.nr_seq_conta_mat,
		c.nr_seq_conta,
		c.nr_nota_numerico,
		s.cd_procedimento,
		s.ie_origem_proced,
		s.nr_seq_proc_partic,
		s.nr_sequencia,
		p.vl_glosa,
		s.tp_tabela
	into	nr_seq_nota_cobr_rrs_w,
		nr_seq_fatura_w,
		nr_lote_w,
		nr_nota_w,
		cd_unimed_w,
		id_benef_w,
		nm_beneficiario_w,
		dt_nasc_w,
		tp_sexo_w,
		id_rn_w,
		dt_servico_w,
		dt_reembolso_w,
		tp_carater_atend_w,
		tp_pessoa_w,
		nr_cnpj_cpf_w,
		nm_prestador_w,
		nm_profissional_w,
		sg_cons_prof_w,
		nr_cons_prof_w,
		sg_uf_cons_prof_w,
		tp_particip_w,
		tp_tabela_w,
		cd_servico_w,
		qt_cobrada_w,
		vl_dif_vl_inter_w,
		vl_serv_cob_w,
		nr_autoriz_w,
		nr_seq_a500_w,
		nr_seq_conta_proc_w,
		nr_seq_conta_mat_w,
		nr_seq_conta_w,
		nr_nota_numerico_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_partic_w,
		nr_seq_nota_serv_rrs_w,
		vl_glosa_w,
		ie_tipo_tabela_w
	from	ptu_nota_servico_rrs	s,
		ptu_nota_cobranca_rrs	c,
		pls_conta_mat		p
	where	c.nr_sequencia		= s.nr_seq_nota_cobr_rrs
	and	p.nr_sequencia		= s.nr_seq_conta_mat
	and	s.nr_seq_conta_mat	= nr_seq_conta_mat_p;
	
	if	(sg_cons_prof_w is not null) then
		select nvl(max(a.cd_ptu),sg_cons_prof_w)
		into sg_cons_prof_w
		from conselho_profissional	a
		where a.sg_conselho = sg_cons_prof_w;
	end if;

	pls_gerar_motivo_quest(	nr_seq_conta_mat_p, nr_seq_conta_p, 'M', cd_estabelecimento_p, nm_usuario_p);
end if;

select	max(nr_sequencia)
into	nr_seq_registro_rrs_w
from	ptu_questionamento_rrs
where	nr_seq_contestacao	= nr_seq_contestacao_p
and	nr_seq_nota_cobr_rrs	= nr_seq_nota_cobr_rrs_w;

-- 557

-- Se for maior ou igual a versao do PTU 11.0 
if	(ie_versao_w >= 110) then
	nm_prestador_w := substr(nm_prestador_w,1,60);
end if;

if	(nr_seq_registro_rrs_w is null) then
	insert into ptu_questionamento_rrs
		(nr_sequencia,				dt_atualizacao,		nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,	nr_seq_contestacao,
		cd_unimed,				id_benef,		nm_beneficiario,
		dt_reembolso,				nr_cnpj_cpf,		nm_prestador,
		nr_seq_conta,				nr_seq_nota_cobr_rrs,	nr_lote,
		nr_nota)
	values	(ptu_questionamento_rrs_seq.nextval,	sysdate,		nm_usuario_p,
		sysdate,				nm_usuario_p,		nr_seq_contestacao_p,
		cd_unimed_w,				id_benef_w,		nm_beneficiario_w,
		dt_reembolso_w,				nr_cnpj_cpf_w,		nm_prestador_w,
		nr_seq_conta_p,				nr_seq_nota_cobr_rrs_w,	nr_lote_w,
		nr_nota_w)  returning nr_sequencia into nr_seq_registro_rrs_w;
end if;

-- 558
if	(nr_seq_registro_rrs_w is not null) then
	if	(vl_dif_vl_inter_w > vl_glosa_w) then
		vl_reconh_serv_w := vl_dif_vl_inter_w - vl_glosa_w;
	end if;

	-- Se for maior ou igual a versao do PTU 11.0 
	if	(ie_versao_w >= 110) then
		nm_profissional_w := substr(nm_profissional_w,1,60);
	end if;
	
	ie_versao_xml_w	:= somente_numero(ptu_batch_xml_pck.obter_versao_dominio( cd_estabelecimento_p, null, null, sysdate, 'A550'))*100;	
	
	-- Se for XML
	if	(ie_versao_xml_w > 0) then
		ie_tipo_tabela_tiss_w	:= ptu_conversao_tipo_tabela(ie_tipo_tabela_w, cd_servico_w, 'E', ie_origem_proced_w);
		cd_servico_w	:= ptu_conversao_item_tabela(ie_tipo_tabela_w, cd_servico_w, ie_origem_proced_w);

	end if;	

	insert into ptu_quest_serv_rrs
		(nr_sequencia,				dt_atualizacao,		nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,	nr_seq_quest_rrs,
		nr_lote,				nr_nota,		nr_seq_a500,
		tp_particip,				tp_tabela,		cd_servico,
		dt_servico,				vl_dif_vl_inter,	vl_serv,
		vl_reconh_serv,				vl_acordo_serv,		tp_acordo,	
		qt_cobrada,				qt_reconh,		qt_acordada,
		nm_profissional,			dt_acordo,		sg_cons_prof,
		nr_cons_prof,				sg_uf_cons_prof,	nr_autoriz,
		nr_seq_nota_serv_rrs,			nr_seq_conta_mat,	nr_seq_conta_proc,
		nr_nota_numerico,			ie_tipo_tabela_tiss)
	values	(ptu_quest_serv_rrs_seq.nextval,	sysdate,		nm_usuario_p,
		sysdate,				nm_usuario_p,		nr_seq_registro_rrs_w,
		nr_lote_w,				nr_nota_w,		nr_seq_a500_w,
		tp_particip_w,				tp_tabela_w,		cd_servico_w,
		dt_servico_w,				vl_dif_vl_inter_w,	vl_serv_cob_w,
		vl_reconh_serv_w,			0,			'00',
		qt_cobrada_w,				0,			0,
		nm_profissional_w,			null,			sg_cons_prof_w,
		nr_cons_prof_w,				sg_uf_cons_prof_w,	nr_autoriz_w,
		nr_seq_nota_serv_rrs_w,			nr_seq_conta_mat_p,	nr_seq_conta_proc_p,
		null,					ie_tipo_tabela_tiss_w) returning nr_sequencia into nr_seq_quest_serv_rrs_w;
end if;
	
begin
nr_seq_analise_w	:= to_number(pls_obter_dados_conta(nr_seq_conta_w,'AN'));
exception
when others then
	nr_seq_analise_w	:= null;
end;

begin
open C01; 
loop
fetch C01 into
	nr_seq_mot_questionamento_w;
exit when C01%notfound; 
	begin
	ds_parecer_w 		:= null;
	ds_parecer_aux_w	:= null;
	
	select	max(cd_motivo),
		max(ie_parecer_analise)
	into	cd_motivo_w,
		ie_parecer_analise_w
	from	ptu_motivo_questionamento
	where	nr_sequencia	= nr_seq_mot_questionamento_w;
	
	if	(cd_motivo_w = '98') or
		(nvl(ie_parecer_analise_w,'N') = 'S') then
		if	(ie_tipo_w = 'P') then
			select	max(b.ds_parecer_glosa)
			into	ds_parecer_w
			from	pls_contest_item_glosa	b,
				pls_contestacao_proc	a
			where	a.nr_sequencia 		= nr_seq_contest_proc
			and	a.nr_seq_conta_proc	= nr_seq_conta_proc_p
			and	b.nr_seq_mot_quest	= nr_seq_mot_questionamento_w;
		
			/*aaschlote 15/04/2014 OS - 660251*/
			if	(ds_parecer_w is null) and
				(nr_seq_analise_w is not null) then
				
				for r_c03_w in C03(nr_seq_analise_w, nr_seq_conta_proc_w, null) loop
					ds_parecer_w	:= replace(ds_parecer_w,r_c03_w.ds_parecer,' ');
					ds_parecer_w	:= substr(ds_parecer_w||' - '||r_c03_w.ds_parecer,1,4000);
				end loop;
				
			end if;
			
		elsif	(ie_tipo_w = 'M') then
			select	max(b.ds_parecer_glosa)
			into	ds_parecer_w
			from	pls_contest_item_glosa	b,
				pls_contestacao_mat	a
			where	a.nr_sequencia 		= nr_seq_contest_proc
			and	a.nr_seq_conta_mat	= nr_seq_conta_mat_p
			and	b.nr_seq_mot_quest	= nr_seq_mot_questionamento_w;
		
			/*aaschlote 15/04/2014 OS - 660251*/
			if	(ds_parecer_w is null) and
				(nr_seq_analise_w is not null) then
				
				for r_c03_w in C03(nr_seq_analise_w, null, nr_seq_conta_mat_w) loop
					ds_parecer_w	:= replace(ds_parecer_w,r_c03_w.ds_parecer,' ');
					ds_parecer_w	:= substr(ds_parecer_w||' - '||r_c03_w.ds_parecer,1,4000);
				end loop;
				
			end if;
		end if;
	end if;
	
	insert into ptu_questionamento_codigo
		(nr_sequencia,				nm_usuario,		nm_usuario_nrec,
		dt_atualizacao,				dt_atualizacao_nrec,	nr_seq_mot_questionamento,
		ds_parecer_glosa,			nr_seq_registro_rrs)
	values	(ptu_questionamento_codigo_seq.nextval,	nm_usuario_p, 		nm_usuario_p,
		sysdate,				sysdate,		nr_seq_mot_questionamento_w,
		ds_parecer_w,				nr_seq_registro_rrs_w);	
	end; 	
end loop; 
close C01;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(185636,'NR_SEQ_MOTIVO=' || nvl(nr_seq_mot_questionamento_w, 999999));	
end;

end pls_gerar_questionamento_reemb;
/