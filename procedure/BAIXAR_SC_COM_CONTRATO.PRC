create or replace
procedure baixar_sc_com_contrato(	nr_cot_compra_p			number,
				nm_usuario_p			varchar2) is

nr_solic_compra_w			Number(10,0);
nr_item_solic_compra_w		Number(10,0);
nr_seq_contrato_w			Number(10,0);

cursor	c01 is
select	distinct 
	a.nr_solic_compra,
	a.nr_item_solic_compra,
	b.nr_seq_contrato
from	solic_compra_item_agrup_v a,
	contrato_regra_nf b
where	a.nr_cot_compra = b.nr_cot_compra
and	a.nr_item_cot_compra = b.nr_item_cot_compra
and	a.nr_cot_compra = nr_cot_compra_p;

cursor c02 is
select	nr_cot_compra,
	nr_item_cot_compra
from	cot_compra_solic_agrup
where	nr_solic_compra = nr_solic_compra_w;

begin

open c01;
loop
fetch c01 into 	
	nr_solic_compra_w,
	nr_item_solic_compra_w,
	nr_seq_contrato_w;
exit when c01%notfound;
	begin
	
	update	solic_compra_item
	set 	dt_baixa			= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_solic_compra 		= nr_solic_compra_w
	and	nr_item_solic_compra	= nr_item_solic_compra_w;

	gerar_hist_solic_sem_commit(
			nr_solic_compra_w,
			WHEB_MENSAGEM_PCK.get_texto(297896),
			WHEB_MENSAGEM_PCK.get_texto(297897,'NR_ITEM_SOLIC_COMPRA_W=' || nr_item_solic_compra_w || ';' ||
							'NR_SOLIC_COMPRA_W=' || nr_solic_compra_w || ';' ||
							'NR_COT_COMPRA_P=' || nr_cot_compra_p),
			'B',
			nm_usuario_p);	

	update	solic_compra
	set	dt_baixa			= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_solic_compra		= nr_solic_compra_w
	and	not exists(
		select 1
		from	solic_compra_item
		where	nr_solic_compra = nr_solic_compra_w
		and	dt_baixa is null);

	update	processo_aprov_compra a
	set	a.ie_aprov_reprov	= 'B',
		a.ds_observacao		= substr(WHEB_MENSAGEM_PCK.get_texto(297898,'DS_OBSERVACAO_W=' || a.ds_observacao || ';' || 'NR_SEQ_CONTRATO_W=' || nr_seq_contrato_w),1,2000),
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	a.nr_sequencia in(
		select	distinct(nr_seq_aprovacao)
		from	solic_compra_item
		where	nr_solic_compra = nr_solic_compra_w)
	and	ie_aprov_reprov = 'P'
	and	not exists(
		select	1
		from	solic_compra_item x
		where	x.nr_seq_aprovacao = a.nr_sequencia
		and	dt_baixa is null);
		
	end;	
end loop;
close c01;

end baixar_sc_com_contrato;
/
