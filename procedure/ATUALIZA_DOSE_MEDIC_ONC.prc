create or replace procedure ATUALIZA_DOSE_MEDIC_ONC (
    NR_SEQ_PACIENTE_P	number,
    QT_AUC_P number default null,
    QT_MG_CARBOPLATINA_P number default null,
    NR_SEQ_ATENDIMENTO_P number default null
) is 

  qt_auc_w                   paciente_setor.qt_auc%type;
  qt_mg_carboplatina_w       paciente_setor.qt_mg_carboplatina%type;
  qt_creatinina_w            paciente_setor.qt_creatinina%type;
  qt_clearance_creatinina_w  paciente_setor.qt_clearance_creatinina%type;
begin  
    if (qt_auc_p is null and qt_mg_carboplatina_p is null) then             
      select paciente_setor.qt_auc,
             paciente_setor.qt_mg_carboplatina,
             paciente_setor.qt_creatinina,
             paciente_setor.qt_clearance_creatinina
        into qt_auc_w,
             qt_mg_carboplatina_w,
             qt_creatinina_w,
             qt_clearance_creatinina_w
        from paciente_setor 
        where nr_seq_paciente = nr_seq_paciente_p;
      
        update paciente_protocolo_medic ppm
        set 
            ppm.qt_dose = qt_auc_w,
            ppm.qt_dose_prescr = qt_mg_carboplatina_w
        where 
            ppm.nr_seq_paciente = nr_seq_paciente_p
            and ppm.nr_seq_diluicao is null
            and ppm.nr_seq_solucao is null
            and ppm.nr_seq_procedimento is null
            and ppm.nr_seq_medic_material is null
            and 'mgcar' = (select max(lower(cd_unidade_med_sec)) 
                           from unidade_medida um 
                           where um.cd_unidade_medida = ppm.cd_unidade_medida);
                           
      for i in (select paciente_atendimento.nr_seq_atendimento
                  from paciente_atendimento
                 where paciente_atendimento.nr_seq_paciente = nr_seq_paciente_p
                   and paciente_atendimento.dt_cancelamento is null
                   and paciente_atendimento.nr_prescricao is null
                   and substr(Obter_status_Paciente_qt(paciente_atendimento.NR_SEQ_ATENDIMENTO,
                                                       paciente_atendimento.dt_inicio_adm,
                                                       paciente_atendimento.dt_fim_adm,
                                                       paciente_atendimento.nr_seq_local,
                                                       paciente_atendimento.ie_exige_liberacao,
                                                       paciente_atendimento.dt_chegada,'C'),1,60) <> 83) loop
        update PACIENTE_ATENDIMENTO
           set PACIENTE_ATENDIMENTO.Qt_Auc = qt_auc_w,
               PACIENTE_ATENDIMENTO.Qt_Mg_Carboplatina = qt_mg_carboplatina_w,
               PACIENTE_ATENDIMENTO.qt_creatinina = qt_creatinina_w,
               PACIENTE_ATENDIMENTO.qt_clearance_creatinina = qt_clearance_creatinina_w
         where paciente_atendimento.nr_seq_atendimento = i.nr_seq_atendimento
           and paciente_atendimento.nr_seq_paciente = nr_seq_paciente_p;
      end loop;
    else
      qt_auc_w := qt_auc_p;
      qt_mg_carboplatina_w := qt_mg_carboplatina_p;
    end if;



    update paciente_atend_medic pam
    set 
        qt_dose = qt_auc_w,
        qt_dose_prescricao = qt_mg_carboplatina_w
    where
        pam.nr_seq_atendimento in (select um.nr_seq_atendimento 
                                from paciente_atendimento um
                                where um.nr_seq_paciente = nr_seq_paciente_p
                                and (um.dt_cancelamento is null))
        and (select max(pa.nr_prescricao) 
             from paciente_atendimento pa 
             where pa.NR_SEQ_ATENDIMENTO = pam.nr_seq_atendimento
               and pa.nr_seq_paciente = nr_seq_paciente_p) is null
        and (select max(substr(Obter_status_Paciente_qt(pa.NR_SEQ_ATENDIMENTO,pa.dt_inicio_adm,pa.dt_fim_adm,pa.nr_seq_local,pa.ie_exige_liberacao,pa.dt_chegada,'C'),1,60)) 
             from paciente_atendimento pa 
             where pa.NR_SEQ_ATENDIMENTO = pam.nr_seq_atendimento
               and nr_seq_paciente = nr_seq_paciente_p) <> 83
        and pam.dt_cancelamento is null
        and 'mgcar' = (select max(lower(cd_unidade_med_sec)) 
                       from unidade_medida um 
                       where um.cd_unidade_medida = pam.cd_unid_med_dose)
        and nr_seq_diluicao is null
        and nr_seq_solucao is Null
        and nr_seq_procedimento is null
        and pam.nr_seq_atendimento = nvl(NR_SEQ_ATENDIMENTO_P, pam.nr_seq_atendimento);

end atualiza_dose_medic_onc;
/
