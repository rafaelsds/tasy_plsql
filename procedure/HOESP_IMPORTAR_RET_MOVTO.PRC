create or replace
procedure HOESP_IMPORTAR_RET_MOVTO
			(nr_seq_retorno_p	in number) is


nr_atendimento_w	number(10);
nr_conta_paciente_w	number(10);
nm_paciente_w		varchar2(255);		
nm_medico_w		varchar2(255);
cd_crm_medico_w		varchar2(20);
cd_crm_medico_tasy_w	varchar2(20);
cd_autorizacao_w	varchar2(255);
vl_pago_w		number(15,2);
cd_procedimento_w	number(15);
ds_item_retorno_w	varchar2(255);
nr_seq_item_conta_w	number(10);
ds_complemento_w	varchar2(255);
vl_cobrado_w		number(15,2);

		
cursor 	c01 is
select	obter_valor_campo_separador(ds_conteudo,1,';') nr_atendimento,
	obter_valor_campo_separador(ds_conteudo,2,';') nr_conta_paciente,
	substr(obter_valor_campo_separador(ds_conteudo,3,';'),1,255) nm_paciente,
	substr(obter_valor_campo_separador(ds_conteudo,5,';'),1,255) nm_medico,
	somente_numero(obter_valor_campo_separador(ds_conteudo,4,';')) cd_crm_medico,
	obter_valor_campo_separador(ds_conteudo,6,';') cd_autorizacao,
	somente_numero(obter_valor_campo_separador(ds_conteudo,8,';')) vl_pago,
	substr(obter_valor_campo_separador(ds_conteudo,11,';'),1,255) ds_item_retorno,
	somente_numero(obter_valor_campo_separador(ds_conteudo,10,';')) cd_procedimento	
from	w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p;
--and	somente_numero(obter_valor_campo_separador(ds_conteudo,1,';')) <> 0;

begin

open C01;
loop
fetch C01 into	
	nr_atendimento_w,
	nr_conta_paciente_w,
	nm_paciente_w,
	nm_medico_w,
	cd_crm_medico_w,
	cd_autorizacao_w,
	vl_pago_w,
	ds_item_retorno_w,
	cd_procedimento_w;
exit when C01%notfound;
	begin
	
	ds_complemento_w := null;
	
	if	(nr_conta_paciente_w is not null) then
	
		select	max(nr_sequencia)
		into	nr_seq_item_conta_w
		from	procedimento_paciente
		where	nr_interno_conta	= nr_conta_paciente_w
		and	cd_motivo_exc_conta	is null
		and	(cd_procedimento				= cd_procedimento_w or
			cd_procedimento_tuss				= cd_procedimento_w or
			somente_numero(CD_PROCEDIMENTO_CONVENIO)	= cd_procedimento_w);
			
		if	(nr_seq_item_conta_w is not null) then		
			select	max(nr_crm)
			into	cd_crm_medico_tasy_w
			from	procedimento_paciente a,
				medico b
			where	a.cd_medico_executor	= b.cd_pessoa_fisica
			and	a.nr_sequencia		= nr_seq_item_conta_w;

			select	vl_procedimento
			into	vl_cobrado_w
			from	procedimento_paciente
			where	nr_sequencia		= nr_seq_item_conta_w;
			
			if	(somente_numero(cd_crm_medico_tasy_w) <> cd_crm_medico_w) then			
				ds_complemento_w :=	'M�dico corrigido pelo conv�nio! - M�dico Tasy (CRM): '|| cd_crm_medico_tasy_w ||
							' - M�dico Arquivo (CRM): '|| cd_crm_medico_w||' - '||nm_medico_w ;			
			end if;		
		end if;	
	end if;
	
	insert into convenio_retorno_movto
		(nr_seq_retorno,
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		nr_conta,
		nr_doc_convenio,
		vl_total_pago,
		cd_item,
		ds_item_retorno,
		ds_complemento,
		nr_seq_item_conta,
		vl_cobrado,
		vl_glosa)
	values	(nr_seq_retorno_p,
		convenio_retorno_movto_seq.nextval,
		sysdate,
		'TasyImp',
		nr_atendimento_w,
		nr_conta_paciente_w,
		cd_autorizacao_w,
		dividir(nvl(vl_pago_w,0),100),
		cd_procedimento_w,
		ds_item_retorno_w,
		ds_complemento_w,
		nr_seq_item_conta_w,
		vl_cobrado_w,
		vl_cobrado_w - dividir(nvl(vl_pago_w,0),100));	
	
	end;
end loop;
close C01;

delete	from w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end HOESP_IMPORTAR_RET_MOVTO;
/