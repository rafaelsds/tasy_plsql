create or replace
procedure transf_prontuario_setores_html(nr_sequencia_p		number,
					nm_usuario_p		varchar2,
					ie_opcao_p		varchar2,
					cd_estabelecimento_p	number
					) is

nr_sequencia_w	number(10);					
	
Cursor C01 is
	select 	nr_sequencia
	from 	transf_prontuario
	where	nr_seq_lote_transf = nr_sequencia_p
	and	dt_envio is null;
	
begin


if (nr_sequencia_p is not null) then
	
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w;
	exit when C01%notfound;
		begin
		
		transferir_prontuario_setores(nr_sequencia_w,nm_usuario_p,ie_opcao_p, cd_estabelecimento_p);
		
		end;
	end loop;
	close C01;

end if;
					
end	transf_prontuario_setores_html;
/