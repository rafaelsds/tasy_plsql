create or replace
procedure gerar_w_movto_estoque_dia(
			dt_mesano_referencia_p		date,
			cd_estabelecimento_p		number,
			dt_inicio_movto_p			date,
			dt_fim_movto_p			date,
			cd_local_estoque_p		number,
			cd_material_p			number,
			cd_grupo_material_p		number,
			cd_subgrupo_material_p		number,
			cd_classe_material_p		number,	
			ie_movimento_p			varchar2,
			ie_consignado_p			varchar2,
			nm_usuario_p			Varchar2) is

qt_movimento_w			number(13,4);
dt_processo_w			Date;
dt_atualzacao_w			date;
cd_local_estoque_w		Number(4);
cd_operacao_estoque_w		Number(3);
cd_material_estoque_w		Number(6);
nr_movimento_estoque_w		Number(10);
nr_documento_w			Number(15);
dt_mes_ant_w			Date;
qt_saldo_ant_w			number(15,4);
qt_estoque_w			number(15,4)	:= 0;
ie_entrada_saida_w			varchar2(1);
cd_acao_w			varchar2(1);
dt_mesano_referencia_w		date;
vl_estoque_ant_w			number(13,2);
vl_movimento_w			number(13,2);
vl_estoque_w			number(13,2)	:= 0;
ie_tipo_informacao_w		number(10);
cd_material_w			number(6);
dt_fim_movto_w			date;
ie_consignado_w			varchar2(1);

cursor c01 is
select	a.cd_material,
	'N' ie_consignado
from	saldo_estoque a,
	estrutura_material_v b
where	a.cd_material = b.cd_material
and	a.cd_material = nvl(cd_material_p,a.cd_material)
and	b.cd_grupo_material = nvl(cd_grupo_material_p, b.cd_grupo_material)
and	b.cd_subgrupo_material = nvl(cd_subgrupo_material_p, b.cd_subgrupo_material)
and	b.cd_classe_material = nvl(cd_classe_material_p, b.cd_classe_material)
and	((ie_movimento_p = 'N') or 
		exists(	select	1 
			from	resumo_movto_estoque_dia c
			where	c.cd_material = a.cd_material
			and	c.cd_local_estoque 	= nvl(cd_local_estoque_p, c.cd_local_estoque)
			and	ie_consignado = 'N'
			and	c.dt_dia between nvl(dt_inicio_movto_p, c.dt_dia) and PKG_DATE_UTILS.end_of(nvl(nvl(dt_fim_movto_p,c.dt_dia),sysdate),'DAY',0)
			and	c.cd_estabelecimento	= cd_estabelecimento_p
			and	c.dt_mesano_referencia = a.dt_mesano_referencia))
and	ie_consignado_p = 'N'
and	a.dt_mesano_referencia = dt_mesano_referencia_p
and	a.cd_estabelecimento = cd_estabelecimento_p
group by a.cd_material
union all
select	a.cd_material,
	'S' ie_consignado
from	fornecedor_mat_consignado a,
	estrutura_material_v b
where	a.cd_material = b.cd_material
and	a.cd_material = nvl(cd_material_p,a.cd_material)
and	b.cd_grupo_material = nvl(cd_grupo_material_p, b.cd_grupo_material)
and	b.cd_subgrupo_material = nvl(cd_subgrupo_material_p, b.cd_subgrupo_material)
and	b.cd_classe_material = nvl(cd_classe_material_p, b.cd_classe_material)
and	((ie_movimento_p = 'N') or 
	exists(	select	1 
		from	resumo_movto_estoque_dia c
		where	c.cd_material = a.cd_material
		and	c.cd_local_estoque 	= nvl(cd_local_estoque_p, c.cd_local_estoque)
		and	ie_consignado = 'N'
		and	c.dt_dia between nvl(dt_inicio_movto_p, c.dt_dia) and PKG_DATE_UTILS.end_of(nvl(nvl(dt_fim_movto_p,c.dt_dia),sysdate),'DAY',0)
		and	c.cd_estabelecimento	= cd_estabelecimento_p
		and	c.dt_mesano_referencia = a.dt_mesano_referencia))
and	ie_consignado_p = 'S'
and	a.dt_mesano_referencia = dt_mesano_referencia_p
and	a.cd_estabelecimento = cd_estabelecimento_p
group by a.cd_material;

cursor c02 is
select	a.dt_dia,
	a.cd_local_estoque,
	a.cd_operacao_estoque,
	a.cd_material,
	decode(b.ie_entrada_saida,'E',a.qt_estoque,a.qt_estoque*-1),
	decode(b.ie_entrada_saida,'E',a.vl_estoque,a.vl_estoque*-1),
	b.ie_entrada_saida
from	resumo_movto_estoque_dia a,
	operacao_estoque b
where	a.cd_operacao_estoque = b.cd_operacao_estoque
and	a.cd_local_estoque 	= nvl(cd_local_estoque_p, a.cd_local_estoque)
and	a.ie_consignado 	= ie_consignado_w
and	a.cd_material		= cd_material_w
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.dt_mesano_referencia	= dt_mesano_referencia_p
order by a.dt_dia;

BEGIN

delete	w_razao_estoque
where	nm_usuario = nm_usuario_p;

commit;

dt_mesano_referencia_w		:= dt_mesano_referencia_p;
dt_mes_ant_w			:= PKG_DATE_UTILS.ADD_MONTH(dt_mesano_referencia_p,-1,0);

if	(dt_fim_movto_p is not null) then
	dt_fim_movto_w := PKG_DATE_UTILS.end_of(nvl(dt_fim_movto_p,sysdate),'DAY',0);
end if;

open c01;
loop
fetch c01 into
	cd_material_w,
	ie_consignado_w;
exit when c01%notfound;
	begin	
	if	(ie_consignado_w = 'S') then
		select	nvl(sum(qt_estoque),0),
			nvl(sum(vl_estoque),0)
		into	qt_saldo_ant_w,
			vl_estoque_ant_w
		from	fornecedor_mat_consignado
		where	cd_local_estoque 	= nvl(cd_local_estoque_p, cd_local_estoque)
		and	cd_material		= cd_material_w
		and	cd_estabelecimento	= cd_estabelecimento_p
		and	dt_mesano_referencia	= dt_mes_ant_w;
	else
		select	nvl(sum(qt_estoque),0),
			nvl(sum(vl_estoque),0)
		into	qt_saldo_ant_w,
			vl_estoque_ant_w
		from	saldo_estoque
		where	cd_local_estoque 	= nvl(cd_local_estoque_p, cd_local_estoque)
		and	cd_material		= cd_material_w
		and	cd_estabelecimento	= cd_estabelecimento_p
		and	dt_mesano_referencia	= dt_mes_ant_w;
	end if;

	qt_estoque_w	:= qt_saldo_ant_w;
	vl_estoque_w	:= vl_estoque_ant_w;

	insert into w_razao_estoque(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ie_tipo_informacao,
		cd_estabelecimento,
		dt_mesano_referencia,
		cd_material,
		qt_estoque,
		vl_estoque)
	values(	w_razao_estoque_seq.nextval,
		sysdate,
		nm_usuario_p,
		0,
		cd_estabelecimento_p,
		dt_mesano_referencia_w,
		cd_material_w,
		qt_saldo_ant_w,
		vl_estoque_ant_w);

	open c02;
	loop
	fetch c02 into
		dt_processo_w,
		cd_local_estoque_w,
		cd_operacao_estoque_w,
		cd_material_w,
		qt_movimento_w,
		vl_movimento_w,
		ie_entrada_saida_w;
	exit when c02%notfound;
		begin		
		if	(dt_fim_movto_w is null) or (dt_processo_w <= dt_fim_movto_w) then
			begin		
			if	(dt_inicio_movto_p is not null) and (dt_processo_w < dt_inicio_movto_p) then
				qt_estoque_w	:= qt_estoque_w + qt_movimento_w;
				vl_estoque_w	:= vl_estoque_w + vl_movimento_w;
				
				update	w_razao_estoque
				set	qt_estoque = qt_estoque_w,
					vl_estoque = vl_estoque_w
				where	ie_tipo_informacao = 0
				and	cd_material = cd_material_w
				and	nm_usuario = nm_usuario_p;
			else
				begin
				qt_estoque_w	:= qt_estoque_w + qt_movimento_w;
				vl_estoque_w	:= vl_estoque_w + vl_movimento_w;
				
				if	(ie_entrada_saida_w = 'E') then
					ie_tipo_informacao_w := '2';
				else
					ie_tipo_informacao_w := '6';
				end if;
				
				insert into w_razao_estoque(
					nr_sequencia,
					ie_tipo_informacao,
					cd_estabelecimento,
					dt_mesano_referencia,
					dt_processo,
					cd_operacao_estoque,
					cd_material,
					qt_movimento,
					vl_movimento,
					qt_estoque,
					vl_estoque,
					dt_atualizacao,
					nm_usuario)
				values( w_razao_estoque_seq.nextval,
					ie_tipo_informacao_w,
					cd_estabelecimento_p,
					dt_mesano_referencia_w,
					dt_processo_w,
					cd_operacao_estoque_w,
					cd_material_w,
					qt_movimento_w,
					vl_movimento_w,
					qt_movimento_w,
					vl_movimento_w,
					sysdate,
					nm_usuario_p);
				end;
			end if;
			end;
		end if;
		end;
	end loop;
	close c02;
	
	select	nvl(sum(qt_movimento),0),
		nvl(sum(vl_movimento),0)
	into	qt_movimento_w,
		vl_movimento_w
	from	w_razao_estoque
	where	ie_tipo_informacao = 2
	and	cd_material = cd_material_w
	and	nm_usuario = nm_usuario_p;
	
	insert into w_razao_estoque(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ie_tipo_informacao,
		cd_estabelecimento,
		dt_mesano_referencia,
		cd_material,
		qt_estoque,
		vl_estoque)
	values(	w_razao_estoque_seq.nextval,
		sysdate,
		nm_usuario_p,
		4,
		cd_estabelecimento_p,
		dt_mesano_referencia_w,
		cd_material_w,
		qt_movimento_w,
		vl_movimento_w);
		
	select	nvl(sum(qt_movimento),0),
		nvl(sum(vl_movimento),0)
	into	qt_movimento_w,
		vl_movimento_w
	from	w_razao_estoque
	where	ie_tipo_informacao = 6
	and	cd_material = cd_material_w
	and	nm_usuario = nm_usuario_p;
	
	insert into w_razao_estoque(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ie_tipo_informacao,
		cd_estabelecimento,
		dt_mesano_referencia,
		cd_material,
		qt_estoque,
		vl_estoque)
	values(	w_razao_estoque_seq.nextval,
		sysdate,
		nm_usuario_p,
		8,
		cd_estabelecimento_p,
		dt_mesano_referencia_w,
		cd_material_w,
		qt_movimento_w,
		vl_movimento_w);

	if	(dt_fim_movto_w is null) then
		begin
		if	(ie_consignado_w = 'S') then
			select	nvl(sum(qt_estoque),0),
				nvl(sum(vl_estoque),0)
			into	qt_estoque_w,
				vl_estoque_w
			from	fornecedor_mat_consignado
			where	cd_local_estoque 		= nvl(cd_local_estoque_p, cd_local_estoque)
			and	cd_material		= cd_material_w
			and	cd_estabelecimento		= cd_estabelecimento_p
			and	dt_mesano_referencia	= dt_mesano_referencia_w;
		else
			select	nvl(sum(qt_estoque),0),
				nvl(sum(vl_estoque),0)
			into	qt_estoque_w,
				vl_estoque_w
			from	saldo_estoque
			where	cd_local_estoque 		= nvl(cd_local_estoque_p, cd_local_estoque)
			and	cd_material		= cd_material_w
			and	cd_estabelecimento		= cd_estabelecimento_p
			and	dt_mesano_referencia	= dt_mesano_referencia_w;
		end if;
		end;
	end if;
		
	insert into w_razao_estoque(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ie_tipo_informacao,
		cd_estabelecimento,
		dt_mesano_referencia,
		cd_material,
		qt_estoque,
		vl_estoque)
	values(	w_razao_estoque_seq.nextval,
		sysdate,
		nm_usuario_p,
		10,
		cd_estabelecimento_p,
		dt_mesano_referencia_w,
		cd_material_w,
		qt_estoque_w,
		vl_estoque_w);
	end;
end loop;
close c01;

commit;

END gerar_w_movto_estoque_dia;
/