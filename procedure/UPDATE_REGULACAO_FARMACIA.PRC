create or replace procedure update_regulacao_farmacia (
                                                         nr_seq_regulacao_p number,
                                                         nr_seq_origem_p number default null,
                                                         cd_cnpj_origem_p varchar2 default null,
                                                         ds_estabelecimento_origem_p varchar2 default null,
                                                         cd_medico_p varchar2 default null,
                                                         cd_pessoa_fisica_p varchar2 default null,
                                                         dt_inicio_receita_p date default null,
                                                         dt_receita_p date default null,
                                                         ie_nivel_atencao_p varchar2 default null,
                                                         ie_tipo_receita_p varchar2 default null
                                                       )
is
nr_atendimento_w number(10);
begin  
  
  if (nr_seq_regulacao_p is not null) then
  
   select nr_atendimento
    into nr_atendimento_w
   from regulacao_atend
    where regulacao_atend.nr_sequencia = nr_seq_regulacao_p;
    
    insert into fa_receita_farmacia
      ( 
        nr_sequencia,
        nr_seq_regulacao,
        nr_seq_origem,
        cd_cnpj_origem,
        ds_estabelecimento_origem,
        cd_medico,
        dt_inicio_receita,
        dt_receita,
        ie_nivel_atencao,
        ie_tipo_receita,
        cd_pessoa_fisica,
        nm_usuario,
        nm_usuario_nrec,
        dt_atualizacao,
        dt_atualizacao_nrec,
        nr_atendimento
      )
    values
      (
        fa_receita_farmacia_seq.nextval,
        nr_seq_regulacao_p,
        nr_seq_origem_p,
        cd_cnpj_origem_p,
        ds_estabelecimento_origem_p,
        cd_medico_p,
        dt_inicio_receita_p,
        dt_receita_p,
        ie_nivel_atencao_p,
        ie_tipo_receita_p,
        cd_pessoa_fisica_p,
        'integracao',
        'integracao',
        sysdate,
        sysdate,
        nr_atendimento_w
      );
    commit;
    
  end if;
end;
/