create or replace 
procedure gerar_evento_acesso_PEP(
				nr_atendimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_evento_w			number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w		varchar2(10);
qt_idade_w				number(10);
nr_seq_classif_w		number(10);
cd_setor_w				number(10);
cd_procedencia_w		number(10);
ie_sexo_w				varchar2(1);
cd_medico_resp_w		varchar2(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_w
	and		ie_evento_disp		= 'AP'
	and		qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and		nvl(cd_setor_atendimento, cd_setor_w) = cd_setor_w
	and		nvl(nr_seq_classif,nr_seq_classif_w) = nr_seq_classif_w
	and		nvl(cd_procedencia, cd_procedencia_w) = cd_procedencia_w
	and		nvl(ie_sexo,ie_sexo_w) = ie_sexo_w
	and		nvl(cd_medico,cd_medico_resp_w) = cd_medico_resp_w
	and		nvl(ie_situacao,'A') = 'A';

begin
if	(nr_atendimento_p > 0) then
	select	max(cd_pessoa_fisica),
			max(cd_estabelecimento),
			max(cd_procedencia),
			max(cd_medico_resp)
	into	cd_pessoa_fisica_w,
			cd_estabelecimento_w,
			cd_procedencia_w,
			cd_medico_resp_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_p;

	qt_idade_w			:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
	cd_setor_w  		:= obter_setor_atendimento(nr_atendimento_p);
	nr_seq_classif_w  	:= nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0);
	ie_sexo_w			:= obter_dados_pf(cd_pessoa_fisica_w,'SE');

	open C01;
	loop
	fetch C01 into	
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		
		gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_p,cd_pessoa_fisica_w,null,nm_usuario_p,null);

		end;
	end loop;
	close C01;
	
end if;

end gerar_evento_acesso_PEP;
/