create or replace
procedure fechar_conta_consulta(
			nr_interno_conta_p number,
			nm_usuario_p varchar2) is 

begin

if	(nr_interno_conta_p is not null) then
	begin
	update	conta_paciente 
	set     ie_status_acerto  	= 1,
		dt_atualizacao    	= sysdate,
		nm_usuario        	= nm_usuario_p
	where   nr_interno_conta	= nr_interno_conta_p;
	end;	
end if;
commit;

end fechar_conta_consulta;
/