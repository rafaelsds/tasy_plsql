create or replace procedure alterar_horario_ent_oci (	nr_ordem_compra_p	number,
														nr_item_oci_p		number,
														nr_sequencia_p		number,
														hora_p				date) is
														
begin
	update	ordem_compra_item_entrega
	set		hr_prevista_entrega = hora_p
	where	nr_sequencia = nr_sequencia_p
	and		nr_item_oci = nr_item_oci_p
	and		nr_ordem_compra = nr_ordem_compra_p;
	
	commit;
end;
/