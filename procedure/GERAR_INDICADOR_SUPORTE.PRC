create or replace
procedure gerar_indicador_suporte ( dt_referencia_p		date ) 
			is

/*
		IE_INDICADOR

OSA - OS antiga
SAT - Satisfa��o
DUV - OSs de d�vida
PRO - Produtividade

		IE_ABRANGENCIA

GER - Ger�ncia
GRU - Grupo
IND - Individual

		IE_INFORMACAO

QTOSA - Qtde de OSs antigas
PROSA - % de OSs antiga
QTDOS - Qtde de OSs gerais
PRSAT - % de satisfa��o
QTINS - Qtde de OSs com insatisfa��o
QTSAI - Qtde de OSs com satisfa��o informada
QTDUV - Qtde de OSs de d�vida
PRDUV - % de OSs de d�vida
QTOSRG - Qtde de OSs gerais recebidas

*/

qt_os_insatisfacao_w	number(10);
pr_satisf_w		number(15,4);
qt_duvida_w		number(10);
pr_duvida_w		number(15,4);
qt_total_w		number(10);

cursor c01 is
select	b.nr_seq_gerencia_sup,
	c.dt_posicao,
	sum(b.qt_os_antiga) qt_os_antiga,
	round(dividir(sum(b.qt_os_antiga) * 100, sum(b.qt_total)),1) pr_antiga,
	sum(b.qt_total) qt_total
from	gerencia_wheb a,
	man_posicao_diaria_res_sup b,
	man_posicao_diaria c
where	a.nr_sequencia = b.nr_seq_gerencia_sup
and	b.nr_seq_posicao = c.nr_sequencia
and	a.cd_setor_atendimento in (4,17)
and	c.dt_posicao between trunc(dt_referencia_p) and dt_referencia_p + 86399/86400
and	c.ie_tipo is null
and	c.ie_tipo_registro = 1
group by b.nr_seq_gerencia_sup, c.dt_posicao;

cursor c02 is
select	b.qt_os_antiga,
	round(pr_os_antiga, 1) pr_antiga,
	c.dt_posicao,
	a.nr_sequencia,
	a.nr_seq_gerencia_sup,
	b.qt_total
from	grupo_suporte a,
	man_posicao_diaria_res_sup b,
	man_posicao_diaria c
where	a.nr_sequencia = b.nr_seq_grupo_sup
and	b.nr_seq_posicao = c.nr_sequencia
and	c.dt_posicao between trunc(dt_referencia_p) and dt_referencia_p + 86399/86400
and	c.ie_tipo is null
and	c.ie_tipo_registro = 1;

cursor c03 is
select	nr_sequencia
from	gerencia_wheb
where	cd_setor_atendimento in (4,17)
and	ie_situacao = 'A';

cursor c04 is
select	a.nr_sequencia,
	a.nr_seq_gerencia_sup,
	b.nm_usuario_grupo
from	grupo_suporte a,
	usuario_grupo_sup b
where	a.nr_sequencia = b.nr_seq_grupo
--and	b.ie_funcao_usuario = 'S'
and	a.ie_situacao = 'A'
and	a.nr_seq_gerencia_sup is not null;

cursor c05 is
select	b.nm_usuario_grupo,
	c.cd_pessoa_fisica,
	a.nr_seq_gerencia_sup,
	a.nr_sequencia
from	grupo_suporte a,
	usuario_grupo_sup b,
	usuario c
where	a.nr_sequencia = b.nr_seq_grupo
and	b.nm_usuario_grupo = c.nm_usuario
and	a.ie_situacao = 'A'
and	a.nr_seq_gerencia_sup is not null;

c01_w c01%rowtype;
c02_w c02%rowtype;
c03_w c03%rowtype;
c04_w c04%rowtype;
c05_w c05%rowtype;

begin

delete from	w_indicador_suporte
where		dt_referencia = trunc(dt_referencia_p);
commit;

--Indicador de OSs antigas
open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
begin

	--Qtde de OSs antigas por ger�ncia
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'OSA',
		'GER',
		'QTOSA',
		c01_w.dt_posicao,
		c01_w.qt_os_antiga,
		c01_w.nr_seq_gerencia_sup);

	--% de OSs antigas por ger�ncia
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'OSA',
		'GER',
		'PROSA',
		c01_w.dt_posicao,
		c01_w.pr_antiga,
		c01_w.nr_seq_gerencia_sup);

	--Qtde de OSs por ger�ncia
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'OSA',
		'GER',
		'QTDOS',
		c01_w.dt_posicao,
		c01_w.qt_total,
		c01_w.nr_seq_gerencia_sup);

end;
end loop;
close c01;

open c02;
loop
fetch c02 into
	c02_w;
exit when c02%notfound;
begin

	--Qtde de OSs por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'OSA',
		'GRU',
		'QTDOS',
		C02_w.dt_posicao,
		C02_w.qt_total,
		C02_w.nr_seq_gerencia_sup,
		c02_w.nr_sequencia);

	--Qtde de OSs antigas por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'OSA',
		'GRU',
		'QTOSA',
		c02_w.dt_posicao,
		c02_w.qt_os_antiga,
		c02_w.nr_seq_gerencia_sup,
		c02_w.nr_sequencia);

	--% de OSs antigas por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'OSA',
		'GRU',
		'PROSA',
		c02_w.dt_posicao,
		c02_w.pr_antiga,
		c02_w.nr_seq_gerencia_sup,
		c02_w.nr_sequencia);

end;
end loop;
close c02;

commit;

open c03;
loop
fetch c03 into
	c03_w;
exit when c03%notfound;
begin

	--% de satisfa��o por ger�ncia
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'GER',
		'PRSAT',
		trunc(dt_referencia_p),
		100 - obter_info_os_gerencia_sup(dt_referencia_p,c03_w.nr_sequencia,'PRI',null),
		c03_w.nr_sequencia);

	--Qtde de OSs com insatisfa��o por ger�ncia
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'GER',
		'QTINS',
		trunc(dt_referencia_p),
		obter_info_os_gerencia_sup(dt_referencia_p,C03_w.nr_sequencia,'QTI',null),
		c03_w.nr_sequencia);

	--Qtde de OSs com satisfa��o informada por ger�ncia
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'GER',
		'QTSAI',
		trunc(dt_referencia_p),
		obter_info_os_gerencia_sup(dt_referencia_p,C03_w.nr_sequencia,'ENS',null),
		c03_w.nr_sequencia);

	--Qtde de OSs de d�vida por ger�ncia
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'DUV',
		'GER',
		'QTDUV',
		trunc(dt_referencia_p),
		obter_info_os_gerencia_sup(dt_referencia_p,C03_w.nr_sequencia,'QTDU',null),
		c03_w.nr_sequencia);

	--% de OSs de d�vida por ger�ncia
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'DUV',
		'GER',
		'PRDUV',
		trunc(dt_referencia_p),
		obter_info_os_gerencia_sup(dt_referencia_p,C03_w.nr_sequencia,'PRDU',null),
		c03_w.nr_sequencia);

	--Qtde de OSs recebidas em geral
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'PRO',
		'GER',
		'QTOSRG',
		trunc(dt_referencia_p),
		obter_info_os_gerencia_sup(dt_referencia_p,C03_w.nr_sequencia,'QT',null),
		c03_w.nr_sequencia);
	
end;
end loop;
close c03;

commit;

open c04;
loop
fetch c04 into
	c04_w;
exit when c04%notfound;
begin

	--% de satisfacao por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'GRU',
		'PRSAT',
		trunc(dt_referencia_p),
		100 - obter_info_os_grupo_sup(dt_referencia_p,C04_w.nr_sequencia,'PRI',null),
		c04_w.nr_seq_gerencia_sup,
		c04_w.nr_sequencia);

	--Qtde de OSs com insatisfa��o por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'GRU',
		'QTINS',
		trunc(dt_referencia_p),
		obter_info_os_grupo_sup(dt_referencia_p, C04_w.nr_sequencia,'QTI',null),
		c04_w.nr_seq_gerencia_sup,
		c04_w.nr_sequencia);

	--Qtde de OSs com satisfa��o informada por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'GRU',
		'QTSAI',
		trunc(dt_referencia_p),
		obter_info_os_grupo_sup(dt_referencia_p,C04_w.nr_sequencia,'ENS',null),
		c04_w.nr_seq_gerencia_sup,
		c04_w.nr_sequencia);

	--Qtde de OSs de d�vida por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'DUV',
		'GRU',
		'QTDUV',
		trunc(dt_referencia_p),
		obter_info_os_grupo_sup(dt_referencia_p,C04_w.nr_sequencia,'QTDU',null),
		c04_w.nr_seq_gerencia_sup,
		c04_w.nr_sequencia);

	--% de OSs de d�vida por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'DUV',
		'GRU',
		'PRDUV',
		trunc(dt_referencia_p),
		obter_info_os_grupo_sup(dt_referencia_p,C04_w.nr_sequencia,'PRDU',null),
		c04_w.nr_seq_gerencia_sup,
		c04_w.nr_sequencia);

	--Qtde de OSs recebidas por grupo
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'PRO',
		'GRU',
		'QTOSRG',
		trunc(dt_referencia_p),
		obter_info_os_grupo_sup(dt_referencia_p,C04_w.nr_sequencia,'QT',null),
		c04_w.nr_seq_gerencia_sup,
		c04_w.nr_sequencia);

end;
end loop;
close c04;

commit;

open c05;
loop
fetch c05 into
	c05_w;
exit when c05%notfound;
begin

	select	nvl(max(a.qt_excelente + a.qt_regular + a.qt_ruim + a.qt_otimo + a.qt_bom),0),
		nvl(max(a.qt_regular + a.qt_ruim),0) qt_os_insatisfacao,
		nvl(max(a.pr_satisf),100)
	into	qt_total_w,
		qt_os_insatisfacao_w,
		pr_satisf_w
	from	(	select	b.nm_pessoa_fisica nm_usuario,
				b.nm_usuario_exec,
				sum(decode(b.ie_grau_satisfacao,'R',1,0)) qt_regular,
				sum(decode(b.ie_grau_satisfacao,'P',1,0)) qt_ruim,
				sum(decode(b.ie_grau_satisfacao,'E',1,0)) qt_excelente,
				sum(decode(b.ie_grau_satisfacao,'O',1,0)) qt_otimo,
				sum(decode(b.ie_grau_satisfacao,'B',1,0)) qt_bom,
				sum(1) qt_total,
				(dividir(sum(decode(b.ie_grau_satisfacao,'O',1,'B',1,0)),
				      sum(decode(b.ie_grau_satisfacao,'O',1,'B',1,'R',1,'P',1,0))) * 100) pr_satisf
			from	(	select	c.nr_sequencia,
						e.nm_usuario_exec,
						c.ie_grau_satisfacao,
						g.nm_pessoa_fisica
					from	man_ordem_servico c,
						grupo_suporte d,
						man_ordem_servico_exec e,
						usuario f,
						pessoa_fisica g
					where	c.nr_seq_grupo_sup = d.nr_sequencia
					and	c.nr_sequencia = e.nr_seq_ordem
					and	e.nm_usuario_exec = f.nm_usuario
					and	f.cd_pessoa_fisica = g.cd_pessoa_fisica
					and	trunc(c.dt_fim_real, 'month') = trunc(dt_referencia_p, 'month')
					and	e.nm_usuario_exec = c05_w.nm_usuario_grupo
					and	d.nr_sequencia = c05_w.nr_sequencia
					and	e.dt_fim_execucao is null
					and	nvl(c.nr_seq_gerencia_insatisf, d.nr_seq_gerencia_sup) = c05_w.nr_seq_gerencia_sup
					and	e.nm_usuario_exec = (	select	max(h.nm_usuario_exec) nm_usuario_exec
									from	man_ordem_servico_exec h
									where	h.nr_seq_ordem = c.nr_sequencia
									and	exists (	select	1
												from	usuario_grupo_sup i
												where	h.nm_usuario_exec = i.nm_usuario_grupo
											)
								)
					and	c.ie_grau_satisfacao is not null
					and	exists (	select	1
								from	man_ordem_serv_estagio j,
									man_estagio_processo k
								where	c.nr_sequencia = j.nr_seq_ordem
								and	j.nr_seq_estagio = k.nr_sequencia
								and	k.ie_suporte = 'S'
							)
								
				) b
			group by b.nm_pessoa_fisica,
				b.nm_usuario_exec
		) a;

	--Qtde de OSs com satisfa��o informada por usu�rio
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo,
		cd_pessoa_fisica)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'IND',
		'QTSAI',
		trunc(dt_referencia_p),
		qt_total_w,
		c05_w.nr_seq_gerencia_sup,
		c05_w.nr_sequencia,
		c05_w.cd_pessoa_fisica);

	--Qtde de OSs com insatisfa��o por usu�rio
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo,
		cd_pessoa_fisica)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'IND',
		'QTINS',
		trunc(dt_referencia_p),
		qt_os_insatisfacao_w,
		c05_w.nr_seq_gerencia_sup,
		c05_w.nr_sequencia,
		c05_w.cd_pessoa_fisica);

	--% de satisfa��o por usu�rio
	insert into w_indicador_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_indicador,
		ie_abrangencia,
		ie_informacao,
		dt_referencia,
		qt_informacao,
		nr_seq_gerencia,
		nr_seq_grupo,
		cd_pessoa_fisica)
	values(	w_indicador_suporte_seq.nextval,
		sysdate,
		'Tasy_sup',
		sysdate,
		'Tasy_sup',
		'SAT',
		'IND',
		'PRSAT',
		trunc(dt_referencia_p),
		pr_satisf_w,
		c05_w.nr_seq_gerencia_sup,
		c05_w.nr_sequencia,
		c05_w.cd_pessoa_fisica);

end;
end loop;
close c05;

--GRAVAR INDICADORES EM FORMA DE LINHA PARA APRESENTAR EM TELA
gerar_w_indicador_suporte_apre(trunc(dt_referencia_p));

end gerar_indicador_suporte;
/