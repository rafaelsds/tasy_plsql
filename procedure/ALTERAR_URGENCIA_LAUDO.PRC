CREATE OR REPLACE
PROCEDURE ALTERAR_URGENCIA_LAUDO(
    status_p       VARCHAR2,
    nm_usuario_p   VARCHAR2,
    nr_seq_laudo_p NUMBER )
IS
BEGIN
  UPDATE laudo_paciente
  SET ie_urgente     = status_p,
    dt_atualizacao   = sysdate,
    nm_usuario       = nm_usuario_p
  WHERE nr_sequencia = nr_seq_laudo_p;
  COMMIT;
END ALTERAR_URGENCIA_LAUDO;
/