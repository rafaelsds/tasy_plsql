create or replace
procedure atualizar_lote_dctf(nr_sequencia_p	number) is 

begin

if	(nr_sequencia_p	is not null) then
	begin
	update	dctf_lote
	set	dt_envio		= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	commit;
	end;
end if;

end atualizar_lote_dctf;
/