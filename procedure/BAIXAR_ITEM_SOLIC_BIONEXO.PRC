create or replace
procedure baixar_item_solic_bionexo(	nr_ordem_compra_p		number,
				nr_solic_compra_p			number,
				nr_item_solic_compra_p		number,
				nm_usuario_p			varchar2) is

nr_solic_compra_w				Number(10,0);
nr_cot_compra_w				number(10,0);
nr_item_cot_compra_w			number(05,0);
dt_baixa_w				date;

begin

update	solic_compra_item
set 	dt_baixa 	= sysdate,		
	ds_observacao		= substr(WHEB_MENSAGEM_PCK.get_texto(297887,'DS_OBSERVACAO_W=' || ds_observacao || ';' || 'NR_ORDEM_COMPRA_P=' || nr_ordem_compra_p),1,2000)
where	nr_solic_compra 		= nr_solic_compra_p
and	nr_item_solic_compra	= nr_item_solic_compra_p
and	dt_baixa is null;

gerar_hist_solic_sem_commit(
			nr_solic_compra_p,
			WHEB_MENSAGEM_PCK.get_texto(297888),
			WHEB_MENSAGEM_PCK.get_texto(297890,'NR_ITEM_SOLIC_COMPRA_P=' || nr_item_solic_compra_p || ';' || 'NR_ORDEM_COMPRA_P=' || nr_ordem_compra_p),
			'B',
			nm_usuario_p);

update	solic_compra
set	dt_baixa		= sysdate
where	nr_solic_compra	= nr_solic_compra_p
and	not exists(
	select 1
	from	solic_compra_item
	where	nr_solic_compra = nr_solic_compra_p
	and	dt_baixa is null);

select	dt_baixa
into	dt_baixa_w
from	solic_compra
where	nr_solic_compra	= nr_solic_compra_p;

if	(dt_baixa_w is not null) then
	gerar_hist_solic_sem_commit(
			nr_solic_compra_p,
			WHEB_MENSAGEM_PCK.get_texto(297891),
			WHEB_MENSAGEM_PCK.get_texto(297892,'NR_ORDEM_COMPRA_P=' || nr_ordem_compra_p),
			'B',
			nm_usuario_p);	
end if;
	
update	processo_aprov_compra a
set	a.ie_aprov_reprov = 'B',
	a.ds_observacao = substr(WHEB_MENSAGEM_PCK.get_texto(297893,'DS_OBSERVACAO_W=' || a.ds_observacao || ';' || 'NR_ORDEM_COMPRA_P=' || nr_ordem_compra_p),1,2000)
where	a.nr_sequencia in(
	select	distinct(nr_seq_aprovacao)
	from	solic_compra_item
	where	nr_solic_compra = nr_solic_compra_p)
and	ie_aprov_reprov = 'P'
and	not exists(
	select	1
	from	solic_compra_item x
	where	x.nr_seq_aprovacao = a.nr_sequencia
	and	dt_baixa is null);

end baixar_item_solic_bionexo;
/
