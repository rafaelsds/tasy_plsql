create or replace procedure pls_vincular_prestador_wsuite (
         nr_seq_prestador_p in pls_cad_prestador_auxiliar.nr_seq_prestador%TYPE,
		 ie_acao_p	        in	varchar2
         ) is

/*
ie_acao_p
	'V' - Vincular
	'D' - Desvincular
*/

begin

if	(ie_acao_p = 'V') then
	insert  into pls_cad_prestador_auxiliar (
		nr_sequencia,
        nr_seq_prestador,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec
        )
	values (
        pls_cad_prestador_auxiliar_seq.nextval,
		nr_seq_prestador_p,
        sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		wheb_usuario_pck.get_nm_usuario
        );
elsif	(ie_acao_p = 'D') then
	delete from pls_cad_prestador_auxiliar
	where	nr_seq_prestador	= nr_seq_prestador_p;
end if;
commit;

end pls_vincular_prestador_wsuite;
/
