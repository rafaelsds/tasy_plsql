create or replace
procedure gqa_limpar_inicio_etapa (nr_seq_etapa_p		number) is

begin

  update gqa_protocolo_etapa_pac
    set 
        dt_inicio = null,
        nm_usuario_executor = null,
        qt_tempo_exec_resposta = null,
        dt_atualizacao = sysdate
  where nr_sequencia = nr_seq_etapa_p;

end gqa_limpar_inicio_etapa;
/
