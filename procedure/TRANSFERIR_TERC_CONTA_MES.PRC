create or replace
procedure TRANSFERIR_TERC_CONTA_MES
		(cd_estabelecimento_p	in	number,
		 vl_maximo_p		in	number,
		 dt_mes_ref_origem_p	in	date,
		 dt_mes_ref_destino_p	in	date,
		 nm_usuario_p		in	varchar2) is

begin

update	terceiro_conta a
set	a.dt_mesano_referencia	= trunc(dt_mes_ref_destino_p, 'month'),
	a.dt_atualizacao		= sysdate,
	a.nm_usuario		= nm_usuario_p
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	trunc(a.dt_mesano_referencia, 'month') = trunc(dt_mes_ref_origem_p, 'month')
and	a.ie_status_conta		= 'P'
and	(select	nvl(sum(y.vl_operacao), 0)
	from	terceiro_operacao y
	where	y.nr_seq_conta	= a.nr_sequencia) <= vl_maximo_p;

commit;

end TRANSFERIR_TERC_CONTA_MES;
/
