create or replace
procedure Fa_consistir_dose_limite(	nr_seq_receita_p	Number,
					nr_seq_item_receita_p	Number default null,
					ds_mensagem_p	out	Varchar2,
					ie_html5_p varchar default 'N') is

ds_mensagem_w		Varchar2(4000);
cd_material_w		Number(10);
ds_material_w		Varchar2(255);
nr_dias_receita_w	Number(5);
qt_dose_w		Number(18,6);
cd_unidade_medida_w	Varchar2(30);
cd_intervalo_w		Varchar2(7);
qt_idade_w		Number(10);
cd_pessoa_fisica_w	Varchar2(10);
qt_dose_minima_w	Number(18,6);
qt_dose_maxima_w	Number(18,6);
ie_dose_limite_w	Varchar2(15);
cd_unid_med_regra_w	Varchar2(30);
qt_operacao_w		Number(10);
nr_ocorrencia_w		Number(15,4);
nr_ciclo_w		Number(5,0);
ds_justificativa_w	Varchar2(255);
ie_justificativa_w	Varchar2(1);
ie_just_retorno_w	Varchar2(1) := 'N';

	
Cursor C01 is
	select	cd_material,
		substr(obter_desc_material(cd_material),1,255),
		nr_dias_receita,
		qt_dose,
		cd_unidade_medida,
		cd_intervalo,
		nvl(nr_ciclo,1),
		ds_justificativa
	from	fa_receita_farmacia_item
	where	nr_seq_receita = nr_seq_receita_p
	and	(nr_sequencia = nr_seq_item_receita_p or nr_seq_item_receita_p is null)
	order by 1;
	
Cursor C02 is
	select	a.qt_dose_minima,
		a.qt_dose_maxima,
		nvl(a.ie_dose_limite,'DOSE'),
		nvl(a.cd_unidade_medida,cd_unidade_medida_w),
		a.ie_justificativa
	from	fa_regra_limite_terap a,
		fa_medic_farmacia_amb b
	where	a.nr_seq_medic = b.nr_sequencia
	and	b.cd_material = cd_material_w
	and	nvl(a.cd_intervalo,cd_intervalo_w) = cd_intervalo_w
	and	nvl(a.nr_dias_utilizacao,nr_dias_receita_w) = nr_dias_receita_w
	and	nvl(qt_idade_w,1) between nvl(obter_idade_conversao(a.qt_idade_min,a.qt_idade_min_mes,a.qt_idade_min_dia,0,0,0,'MIN'),0) and 
					nvl(obter_idade_conversao(0,0,0,a.qt_idade_max,a.qt_idade_max_mes,a.qt_idade_max_dia,'MAX'),9999999)
	and	((a.ie_justificativa = 'N') or (ds_justificativa_w = '') or (ds_justificativa_w is null))
	order by 1;	

begin

if (nr_seq_receita_p is not null) then

	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	fa_receita_farmacia
	where	nr_sequencia = nr_seq_receita_p;

	select	max(obter_idade(dt_nascimento,nvl(dt_obito,sysdate),'DIA'))
	into	qt_idade_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;

	open C01;
	loop
	fetch C01 into	
		cd_material_w,
		ds_material_w,
		nr_dias_receita_w,
		qt_dose_w,
		cd_unidade_medida_w,
		cd_intervalo_w,
		nr_ciclo_w,
		ds_justificativa_w;
	exit when C01%notfound;
		begin
		
		open C02;
		loop
		fetch C02 into	
			qt_dose_minima_w,
			qt_dose_maxima_w,
			ie_dose_limite_w,
			cd_unid_med_regra_w,
			ie_justificativa_w;
		exit when C02%notfound;
			begin
				select	decode(ie_just_retorno_w,'N',ie_justificativa_w,ie_just_retorno_w)
				into	ie_just_retorno_w
				from	dual;
				if (cd_unidade_medida_w <> cd_unid_med_regra_w) then
					qt_dose_w := Obter_dose_convertida(cd_material_w, qt_dose_w, cd_unidade_medida_w, cd_unid_med_regra_w);
				end if;
				
				if (ie_dose_limite_w = 'DIA') then
					-- Intervalo em horas
					select	nvl(MAX(nvl(qt_operacao_fa,qt_operacao)),0)
					into	qt_operacao_w
					from	intervalo_prescricao
					where	cd_intervalo = cd_intervalo_w
					and	ie_operacao = 'H';
					
					nr_ocorrencia_w := Obter_ocorrencia_intervalo(cd_intervalo_w,24,'O');
					
					select	nr_ocorrencia_w * qt_dose_w * decode(nvl(nr_ciclo_w,1),0,1,nvl(nr_ciclo_w,1))
					into	qt_dose_w
					from	dual;
				end if;
				
				if (ie_html5_p = 'S') then
					if (qt_dose_w < qt_dose_minima_w) then
						ds_mensagem_w := ds_mensagem_w || wheb_mensagem_pck.get_texto(343290, 'DS_MATERIAL_W='||ds_material_w||';'||'QT_DOSE_MINIMA_W='||qt_dose_minima_w||';'||
													'CD_UNID_MED_REGRA_W='||cd_unid_med_regra_w||';'||'IE_DOSE_LIMITE_W='||lower(ie_dose_limite_w)||';')||Chr(13)||Chr(10);
					elsif (qt_dose_w > qt_dose_maxima_w) then
						ds_mensagem_w := ds_mensagem_w || wheb_mensagem_pck.get_texto(343295, 'DS_MATERIAL_W='||ds_material_w||';'||'QT_DOSE_MAXIMA_W='||qt_dose_maxima_w||';'||
													'CD_UNID_MED_REGRA_W='||cd_unid_med_regra_w||';'||'IE_DOSE_LIMITE_W='||lower(ie_dose_limite_w)||';')||Chr(13)||Chr(10);
					end if;
				else
					if (qt_dose_w < qt_dose_minima_w) then
						ds_mensagem_w := ds_mensagem_w || '343290;'||ds_material_w||';'||qt_dose_minima_w||';'||cd_unid_med_regra_w||';'||lower(ie_dose_limite_w)||'|';
					elsif (qt_dose_w > qt_dose_maxima_w) then
						ds_mensagem_w := ds_mensagem_w || '343295;'||ds_material_w||';'||qt_dose_maxima_w||';'||cd_unid_med_regra_w||';'||lower(ie_dose_limite_w)||'|';
					end if;
				end if;
			end;
		end loop;
		close C02;
		
		end;
	end loop;
	close C01;
		
	if	((ds_mensagem_w is not null) and (ie_just_retorno_w = 'S')) then
		ds_mensagem_w := ds_mensagem_w|| wheb_mensagem_pck.get_texto(343297); -- Favor informar a justificativa!
	end if;
end if;	

ds_mensagem_p := ds_mensagem_w;

end Fa_consistir_dose_limite;
/
