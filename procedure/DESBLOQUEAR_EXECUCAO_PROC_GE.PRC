create or replace
procedure desbloquear_execucao_proc_ge(		nr_prescricao_p		Number,
						nr_sequencia_p		Number,
						nm_usuario_p		Varchar2) is 

begin

update prescr_procedimento 
set    ie_proced_bloqueado = 'N'
where  nr_prescricao = nr_prescricao_p 
and    nr_sequencia = nr_sequencia_p;

commit;

end desbloquear_execucao_proc_ge;
/