create or replace
procedure san_armazernar_bolsa
			(	nr_seq_producao_p		number,
				nr_seq_local_armaz_p		number,
				nr_seq_armazenamento_p		number,
				nm_usuario_p			varchar2) is
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_congelacao_w			number(10);

begin
if	(nr_seq_producao_p is not null) then
	update	san_producao x
	set	x.nr_seq_local_armaz		= nr_seq_local_armaz_p,
		nr_seq_armazenamento		= nr_seq_armazenamento_p,
		x.nm_usuario_local_armaz	= nm_usuario_p,
		x.nm_usuario			= nm_usuario_p,
		x.dt_local_armaz		= sysdate,
		x.dt_utilizacao			= sysdate
	where	x.nr_sequencia			= nr_seq_producao_p;

	select	count(1)
	into	qt_congelacao_w
	from	san_dados_congelacao	a
	where	a.nr_seq_producao	= nr_seq_producao_p;
	
	if	(qt_congelacao_w > 0) then
		for reg_congelacao in (	select	a.nr_sequencia
					from	san_dados_congelacao	a
					where	a.nr_seq_producao	= nr_seq_producao_p) loop
			begin
			san_liberar_crioprotetores(	reg_congelacao.nr_sequencia,
							obter_estabelecimento_ativo,
							nm_usuario_p);
			end;
		end loop;
	end if;
end if;

commit;

end san_armazernar_bolsa;
/