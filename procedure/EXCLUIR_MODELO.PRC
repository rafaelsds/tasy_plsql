create or replace
procedure excluir_modelo
		(	nr_seq_modelo_p		number,
			nm_usuario_p		Varchar2) is

begin

delete from	modelo_desenho
where		nr_seq_modelo = nr_seq_modelo_p;

delete from	modelo_conteudo
where		nr_seq_modelo = nr_seq_modelo_p;

delete from	modelo
where		nr_sequencia = nr_seq_modelo_p;

commit;

end excluir_modelo;
/ 