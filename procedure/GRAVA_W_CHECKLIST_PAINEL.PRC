create or replace 
PROCEDURE grava_w_checklist_painel (	ds_lista_checklist_p	VARCHAR2,
					ie_avaliacao_p		NUMBER,
					ie_filtro_p		VARCHAR2,
					nm_usuario_p		VARCHAR2) IS


ds_checklist_w			VARCHAR2(255);
nm_checklist_w			VARCHAR2(255);
nr_seq_checklist_w		VARCHAR2(255);
ds_atendimento_w		VARCHAR2(255);
nm_paciente_w			VARCHAR2(255);
nr_sequencia_w			NUMBER(10);
ds_comando_w			VARCHAR2(4000);
ds_sql_check_cria_w		VARCHAR2(2000);
ds_sql_check_w			VARCHAR2(2000);
nr_atributo_w			NUMBER(2);
ds_completude_w			VARCHAR2(255);
nr_seq_atend_w			NUMBER(10);
ds_sql_cursor_w   		VARCHAR2(2000);
ds_sql_cursor03_w   		VARCHAR2(2000);
type EmpCurTyp			IS REF CURSOR;
C01	  			EmpCurTyp;
C02	  			EmpCurTyp;	
C03	  			EmpCurTyp;	
C04	  			EmpCurTyp;
C05	  			EmpCurTyp;
ds_lista_checklist_w		VARCHAR2(255);
ie_pos_and_w			NUMBER(10);
ds_lista_aux_w			VARCHAR2(255);
tam_lista_w			NUMBER(10);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(295156, null, wheb_usuario_pck.get_nr_seq_idioma);--Paciente
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(283863, null, wheb_usuario_pck.get_nr_seq_idioma);--Atendimento

BEGIN

DELETE FROM w_checklist_painel WHERE nm_usuario = nm_usuario_p; 

IF	(ds_lista_checklist_p IS NOT NULL) THEN
	BEGIN
	nr_atributo_w	:= 1;
	
	/*if (instr(ds_lista_checklist_p,'and cd_estabelecimento',1,1) > 0) then
		select substr(ds_lista_checklist_p, 
				decode(sign(instr(ds_lista_checklist_p,'and nr_seq_checklist',1,1)), --Se retornar um significa que � maior que zero
					1, 
					instr(ds_lista_checklist_p,' and nr_seq_checklist',1,1),								
					1),
				decode(instr(ds_lista_checklist_p,' and cd_estabelecimento',1,1),
					0,
					length(ds_lista_checklist_p),								
					(instr(ds_lista_checklist_p,' and cd_estabelecimento',1,1)-1)))
		into ds_lista_checklist_w
		from dual;
	else
		ds_lista_checklist_w := ds_lista_checklist_p;
	end if;*/
	
	if	(instr(ds_lista_checklist_p,'and cd_estabelecimento') > 0) and
		(length(ds_lista_checklist_p) > 0 ) then
		begin
		
		tam_lista_w	:= length(ds_lista_checklist_p);
		ie_pos_and_w	:= instr(ds_lista_checklist_p,'and cd_estabelecimento');
		ds_lista_aux_w	:= ds_lista_checklist_p;
		ds_lista_checklist_w 	:= ' ';
		
		while	(ie_pos_and_w > 0) and 
			(instr(ds_lista_aux_w,'and nr_seq_checklist') > 0) loop			
			begin
			ds_lista_checklist_w	:= ds_lista_checklist_w || ' ' || substr(ds_lista_aux_w,1,(ie_pos_and_w - 1));
			ds_lista_aux_w		:= substr(ds_lista_aux_w,(ie_pos_and_w + 22),tam_lista_w);
			ds_lista_aux_w		:= substr(ds_lista_aux_w,(instr(ds_lista_aux_w,'and nr_seq_checklist')),tam_lista_w);
			ie_pos_and_w		:= instr(ds_lista_aux_w,'and cd_estabelecimento');
			end;
		end loop;
		
		if	(instr(ds_lista_aux_w,'and nr_seq_checklist') > 0) then
			ds_lista_checklist_w	:= ds_lista_checklist_w || ' ' || substr(ds_lista_aux_w,(instr(ds_lista_aux_w,'and nr_seq_checklist')),tam_lista_w);
		end if;
		
		end;
	else
		ds_lista_checklist_w := ds_lista_checklist_p;
	end if;
	
	--Montar o cabe�alho
	IF 	(ie_avaliacao_p = 1) THEN  	--por avalia��o
		BEGIN
		ds_sql_cursor_w := ' select	distinct
						a.nr_sequencia,
						substr(a.nm_item_checklist,1,255)
					from	checklist_processo_item a
					where	1 = 1 '
					|| nvl(ds_lista_checklist_w, '')
					||' order by 1';

		OPEN C02 for ds_sql_cursor_w;
		LOOP
		FETCH 	C02 INTO
			nr_sequencia_w,
			nm_checklist_w;
		EXIT WHEN C02%NOTFOUND;
			BEGIN
			ds_sql_check_w			:= ds_sql_check_w || CHR(39) || nm_checklist_w || CHR(39) || ',';
			ds_sql_check_cria_w		:= ds_sql_check_cria_w || ' DS_RESULTADO' || nr_atributo_w || ',';

			nr_atributo_w			:= nr_atributo_w + 1;

			END;
		END LOOP;
		CLOSE C02;		
		END;
	ELSE					--por checklist
		BEGIN
		ds_sql_cursor_w := ' select	distinct
						a.nr_sequencia,
						substr(a.nm_checklist,1,255)
					from	checklist_processo a
					where	1 = 1'
					|| nvl(replace(ds_lista_checklist_w,'nr_seq_checklist','nr_sequencia'),'')
					||' order by 1';
				
		OPEN C01 for ds_sql_cursor_w;
		LOOP
		FETCH 	C01 INTO
			nr_sequencia_w,
			nm_checklist_w;
		EXIT WHEN C01%NOTFOUND;
			BEGIN
			ds_sql_check_w			:= ds_sql_check_w || CHR(39) || nm_checklist_w || CHR(39) || ',';
			ds_sql_check_cria_w		:= ds_sql_check_cria_w || ' DS_RESULTADO' || nr_atributo_w || ',';

			nr_atributo_w			:= nr_atributo_w + 1;

			END;
		END LOOP;
		CLOSE C01;
		END;
	END IF;

	ds_comando_w	:= ' insert into w_checklist_painel (	ds_atendimento,
							nm_paciente,
							ie_opcao, '||
							ds_sql_check_cria_w ||
							'nm_usuario)
						values( '||CHR(39)||expressao2_w||CHR(39)||', ' ||
							CHR(39)||expressao1_w||CHR(39) || ', ' ||
							CHR(39)||'C'||CHR(39)||', ' ||
							ds_sql_check_w ||
							CHR(39)||nm_usuario_p||CHR(39)||') ';

	exec_sql_dinamico('TASY', ds_comando_w);

	--Montar as informa��es

	ds_sql_cursor03_w := ' select	to_char(b.nr_atendimento),
					substr(obter_nome_pf(b.cd_pessoa_fisica),1,255),
					b.nr_sequencia
				from	atendimento_checklist b,
					atendimento_paciente a
				where	a.nr_atendimento = b.nr_atendimento
				and	((( :ie_filtro_p  = ' || CHR(39) || 'PT' || CHR(39) || ') and (nvl(obter_complitude_clepa(b.nr_sequencia, 0),0) = 0)) or			
					(( :ie_filtro_p  = ' || CHR(39) || 'PP'|| CHR(39) || ') and (nvl(obter_complitude_clepa(b.nr_sequencia, 0),0) between 1 and 99)) or	
					(( :ie_filtro_p  = ' || CHR(39) || 'TO'|| CHR(39) || '))) '	
				|| nvl(ds_lista_checklist_p, '');

	OPEN C03 for ds_sql_cursor03_w using ie_filtro_p,ie_filtro_p,ie_filtro_p;
	LOOP
	FETCH C03 INTO
		ds_atendimento_w,
		nm_paciente_w,
		nr_seq_atend_w;
	EXIT WHEN C03%NOTFOUND;
		BEGIN

		nr_atributo_w		:= 1;
		ds_sql_check_w		:= '';
		ds_sql_check_cria_w	:= '';

		IF 	(ie_avaliacao_p = 1) THEN  	--por avalia��o		
			BEGIN
			ds_sql_cursor_w := ' select 	distinct
							a.nr_sequencia,
							substr(a.nm_item_checklist,1,255),
							(select	max(nvl(obter_complitude_clepa(b.nr_seq_atend_checklist, a.nr_sequencia),0))
							from	med_avaliacao_paciente b
							where	b.nr_seq_checklist_item = a.nr_sequencia
							and	b.nr_seq_atend_checklist = ' || nr_seq_atend_w || ')
						from	checklist_processo_item a
						where	1 = 1'
						|| nvl(ds_lista_checklist_w, '') 
						||' order by 1';
			
			OPEN C05 for ds_sql_cursor_w;
			LOOP
			FETCH C05 INTO
				nr_sequencia_w,
				nm_checklist_w,
				ds_completude_w;
			EXIT WHEN C05%NOTFOUND;
				BEGIN
				ds_sql_check_w		:= ds_sql_check_w || CHR(39) || ds_completude_w ||CHR(39) || ',';
				ds_sql_check_cria_w	:= ds_sql_check_cria_w || ' DS_RESULTADO' || nr_atributo_w || ',';
	
				nr_atributo_w			:= nr_atributo_w + 1;
				END;
			END LOOP;	
			CLOSE C05;
			END;
		ELSE					--por checklist
			BEGIN
			ds_sql_cursor_w := ' select 	distinct
							a.nr_sequencia,
							substr(a.nm_checklist,1,255),
							(select	max(nvl(obter_complitude_clepa(b.nr_sequencia, 0),0))
							 from	atendimento_checklist b
							where	a.nr_sequencia	= b.nr_seq_checklist
							and	b.nr_sequencia	= ' || nr_seq_atend_w || ')
						from	checklist_processo a
						where	1 = 1'
						|| nvl(replace(ds_lista_checklist_w,'nr_seq_checklist','nr_sequencia'),'')
						||' order by 1';
			
			OPEN C04 for ds_sql_cursor_w;
			LOOP
			FETCH C04 INTO
				nr_sequencia_w,
				nm_checklist_w,
				ds_completude_w;
			EXIT WHEN C04%NOTFOUND;
				BEGIN
				ds_sql_check_w		:= ds_sql_check_w || CHR(39) || ds_completude_w ||CHR(39) || ',';
				ds_sql_check_cria_w	:= ds_sql_check_cria_w || ' DS_RESULTADO' || nr_atributo_w || ',';
	
				nr_atributo_w			:= nr_atributo_w + 1;
				END;
			END LOOP;	
			CLOSE C04;
			END;
		END IF;
		
		
		ds_comando_w	:= ' insert into w_checklist_painel (	ds_atendimento,
							nm_paciente,
							ie_opcao, '||
							ds_sql_check_cria_w ||
							'nm_usuario)
						values( '||CHR(39)||ds_atendimento_w||CHR(39)||', ' ||
							CHR(39)||nm_paciente_w||CHR(39) || ', ' ||
							CHR(39)||'I'||CHR(39)||', ' ||
							ds_sql_check_w ||
							CHR(39)||nm_usuario_p||CHR(39)||') ';

		exec_sql_dinamico('TASY', ds_comando_w);
		END;
	END LOOP;
	CLOSE C03;
	END;
END IF;

COMMIT;

END grava_w_checklist_painel;
/
