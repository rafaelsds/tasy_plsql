create or replace
procedure gpi_aprovar_orcamento(	nr_seq_orcamento_p	number,
				ie_operacao_p		varchar2,
				nm_usuario_p		Varchar2) is 

BEGIN

if	(ie_operacao_p = 'A') then

	update	gpi_orcamento
	set	dt_aprovacao	= sysdate,
		nm_usuario_aprov	= nm_usuario_p
	where	nr_sequencia	= nr_seq_orcamento_p;

elsif	(ie_operacao_p = 'D') then

	update	gpi_orcamento
	set	dt_aprovacao	= null,
		nm_usuario_aprov	= null
	where	nr_sequencia	= nr_seq_orcamento_p;

end if;
commit;

END gpi_aprovar_orcamento;
/