create or replace
procedure man_os_especif_liberar_esp	(	nr_seq_especif_p		number,
						nm_usuario_p			varchar2	) is 

begin

	if	(nr_seq_especif_p is not null) then

		update	man_os_especificacao
		set	dt_liberacao = sysdate,
			nm_usuario_liberacao = nm_usuario_p
		where	nr_sequencia = nr_seq_especif_p;

	end if;

commit;

end man_os_especif_liberar_esp;
/