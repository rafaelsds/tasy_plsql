create or replace
procedure gpt_atualiza_dt_fim_recon(	nr_sequencia_p		number,
					dt_fim_p		date
					) is

begin

	begin
	
	update   hist_saude_reconciliacao       
	set      dt_fim         = dt_fim_p
        where    nr_sequencia   = nr_sequencia_p;
	
	end;

commit;

end gpt_atualiza_dt_fim_recon;
/
