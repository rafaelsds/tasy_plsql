create or replace
procedure pls_consistir_proposta_pagador
		(	nr_seq_proposta_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2,
			ds_erro_p	out	varchar2) is 
			
ds_erro_w		varchar2(255);
ie_pagador_ativo_w	varchar2(1);
qt_pagador_inativo_w	number(10);
qt_pagador_w		number(10);		

begin

/* Obter o valor do par�metro 17 */
ie_pagador_ativo_w	:= nvl(obter_valor_param_usuario(1232, 57, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');

if	(ie_pagador_ativo_w = 'S') then
	select	count(*)
	into	qt_pagador_inativo_w
	from	pls_proposta_pagador
	where	nr_seq_proposta	= nr_seq_proposta_p
	and	dt_fim_vigencia is not null;
	
	select	count(*)
	into	qt_pagador_w
	from	pls_proposta_pagador
	where	nr_seq_proposta	= nr_seq_proposta_p;
	
	if	(qt_pagador_w	> 1) and
		((qt_pagador_w - qt_pagador_inativo_w) > 1)  then
		ds_erro_w := wheb_mensagem_pck.get_texto(280936);
	end if;	
end if;

ds_erro_p	:= ds_erro_w;

end pls_consistir_proposta_pagador;
/