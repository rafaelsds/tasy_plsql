CREATE OR REPLACE
PROCEDURE Atualizar_Dt_Impressao_Laudo	(	nr_sequencia_p	number,
						nm_usuario_p	varchar2) is
BEGIN

update	laudo_paciente    
set	dt_impressao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

commit;

END	Atualizar_Dt_Impressao_Laudo;
/
