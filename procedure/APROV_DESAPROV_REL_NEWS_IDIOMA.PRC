create or replace
procedure aprov_desaprov_rel_news_idioma(	ie_aprovar_desaprovar_p	varchar2,
					nm_usuario_p		Varchar2,
					nr_sequencia_p		number) is 

begin

	if	(ie_aprovar_desaprovar_p	= 'A') then
		update	release_news_idioma
		set	dt_aprovacao 	= sysdate,
			nm_usuario_aprov = nm_usuario_p,
			nm_usuario	 = nm_usuario_p
		where	nr_sequencia	= nr_sequencia_p;
	else
		update	release_news_idioma
		set	dt_aprovacao 	= null,
			nm_usuario_aprov = null,
			nm_usuario	 = nm_usuario_p
		where	nr_sequencia	= nr_sequencia_p;
	end if;

commit;

end aprov_desaprov_rel_news_idioma;
/