create or replace 
procedure intpd_ret_encargos_muguerza(
			nr_sequencia_p in number,
			ds_xml_p     in long) is
xml_base_w				xmltype;
nr_seq_documento_w		intpd_fila_transmissao.nr_seq_documento%type;
nr_seq_item_documento_w	intpd_fila_transmissao.nr_seq_item_documento%type;
nr_seq_material_w		material_atend_paciente.nr_sequencia%type;
nr_seq_procedimento_w	procedimento_paciente.nr_sequencia%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ie_status_w				varchar2(3);
ie_status_atual_w		varchar2(3);
nm_usuario_w			usuario.nm_usuario%type;
 
  
cursor c01 is
select *
from	xmltable(
	xmlnamespaces('http://www.w3.org/2003/05/soap-envelope' AS "soap",
        'http://www.christusmuguerza.com.mx/EnterpriseObjects/GCM/Common' AS "com",
        'http://www.christusmuguerza.com.mx/EnterpriseObjects/EpisodeCharge' AS "epis"),
	'/soap:Envelope/soap:Body/epis:ProcessEpisodeChargeResponseAcknowledgeEBM/epis:Acknowledge' passing xml_base_w COLUMNS 
        code varchar2(10) path 'com:Code',
        message varchar2(1000) path 'com:Message',
        detail varchar2(4000) path 'com:Detail',
	instanceId number(10,0) path 'com:InstanceId');

c01_w	c01%rowtype;

begin
xml_base_w := xmltype.createxml(ds_xml_p);

open c01;
	fetch c01 into c01_w;
close c01;

update	intpd_fila_transmissao
set	ds_xml_retorno = ds_xml_p,
	ds_log = nvl(c01_w.detail, ds_log),
	ie_response_procedure = 'S'
where	nr_sequencia = nr_sequencia_p;

select	nr_seq_documento,
	nr_seq_item_documento
into	nr_seq_documento_w,
	nr_seq_item_documento_w
from	intpd_fila_transmissao
where	nr_sequencia = nr_sequencia_p;

if	(nr_seq_item_documento_w = 1) then -- material
	select	nr_sequencia,
		nr_atendimento
	into	nr_seq_material_w,
		nr_atendimento_w
	from	material_atend_paciente
	where	nr_sequencia = nr_seq_documento_w;	      
elsif	(nr_seq_item_documento_w = 2) then -- procedimento	
	select 	nr_sequencia,
		nr_atendimento
	into   	nr_seq_procedimento_w, nr_atendimento_w
	from   	procedimento_paciente
	where  	nr_sequencia = nr_seq_documento_w;
elsif	(nr_seq_item_documento_w = 3) then -- devolu��o de material
	select	a.nr_sequencia,
		a.nr_atendimento
	into	nr_seq_material_w,
		nr_atendimento_w
	from	material_atend_paciente a,
		intpd_devol_mat_proc b
	where	a.nr_sequencia = b.nr_seq_matpaci
	and	b.nr_sequencia = nr_seq_documento_w;	
elsif	(nr_seq_item_documento_w = 4) then -- devolu��o de procedimento
	select 	a.nr_sequencia,
		a.nr_atendimento
	into   	nr_seq_procedimento_w,
		nr_atendimento_w
	from   	procedimento_paciente a,
		intpd_devol_mat_proc b
	where  	a.nr_sequencia = b.nr_seq_propaci
	and	b.nr_sequencia = nr_seq_documento_w;
end if;

nm_usuario_w := 'WEBSERVICE';

select case
     when ies.ie_tipo_utilizacao = 'S'
	  and to_number(c01_w.code) = 0 then 'S'
     when ies.ie_tipo_utilizacao = 'S'
	  and to_number(c01_w.code) <> 0 then 'E'
     when ies.ie_tipo_utilizacao = 'A'
	  and to_number(c01_w.code) = 0 then 'AEX'
     when ies.ie_tipo_utilizacao = 'A'
	  and to_number(c01_w.code) <> 0 then 'E'
     else 'E'
   end
into	ie_status_w
from	intpd_eventos_sistema ies
join	intpd_fila_transmissao ift
on	ies.nr_sequencia = ift.nr_seq_evento_sistema
where	ift.nr_sequencia = nr_sequencia_p;

select 	ie_status
into	ie_status_atual_w
from	intpd_fila_transmissao
where	nr_sequencia = nr_sequencia_p;

if	(ie_status_w <> 'AEX') or
	(ie_status_atual_w <> 'E' and
	(ie_status_atual_w <> 'P')) then
	begin
	update	intpd_fila_transmissao
	set		ie_status = ie_status_w
	where	nr_sequencia = nr_sequencia_p;

	insert into status_integr_conta_pac(
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_atendimento,
		nr_seq_matpaci,
		nr_seq_propaci,
		ie_status,
		nr_seq_fila_transm)
	values(	status_integr_conta_pac_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_w,
		nm_usuario_w,
		nr_atendimento_w,
		nr_seq_material_w,
		nr_seq_procedimento_w,
		ie_status_w,
		nr_sequencia_p);
	end;
end if;
commit;
end intpd_ret_encargos_muguerza;
/