create or replace
procedure ptu_importar_decurso_prazo_v50
			(	ds_arquivo_p		ptu_decurso_prazo.ds_arquivo_pedido%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type,
				nr_seq_origem_p	out	ptu_resposta_autorizacao.nr_seq_origem%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Importar a transa��o 00700 - Decurso de Prazo do PTU online

CASO FOR REALIZAR ALGUMA ALTERA��O NESTA PROCEDURE FAVOR VERIFICAR COM OS AN�LISTAS DO GRUPO OPS - ATENDIMENTO
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
cd_transacao_w			ptu_decurso_prazo.cd_transacao%type;
ie_tipo_cliente_w		ptu_decurso_prazo.ie_tipo_cliente%type;
ds_tipo_cliente_w		varchar2(15);
cd_unimed_executora_w		ptu_decurso_prazo.cd_unimed_executora%type;
cd_unimed_beneficiario_w	ptu_decurso_prazo.cd_unimed_beneficiario%type;
nr_seq_execucao_w		ptu_decurso_prazo.nr_seq_execucao%type;
nr_versao_w			ptu_decurso_prazo.nr_versao%type;
nr_seq_pedido_compl_w		ptu_pedido_compl_aut.nr_sequencia%type;
nr_seq_pedido_aut_w		ptu_pedido_autorizacao.nr_sequencia%type;
nr_seq_guia_w			pls_guia_plano.nr_sequencia%type;
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;

cursor c01 is
	select	nr_seq_importacao,
		ds_valores
	from	w_scs_importacao
	where	nm_usuario	= nm_usuario_p
	order by nr_seq_importacao;
	
begin

for	r_c01_w	in c01	loop
	if	(r_c01_w.ds_valores	<> '$FIM') then
		if	(r_c01_w.nr_seq_importacao	= 1) then
			cd_transacao_w			:= substr(r_c01_w.ds_valores,1,5);
			ds_tipo_cliente_w		:= trim(substr(r_c01_w.ds_valores,6,15));
			cd_unimed_executora_w		:= to_number(nvl(trim(substr(r_c01_w.ds_valores,21,4)),0));
			cd_unimed_beneficiario_w	:= to_number(nvl(trim(substr(r_c01_w.ds_valores,25,4)),0));
			nr_seq_execucao_w		:= to_number(nvl(trim(substr(r_c01_w.ds_valores,29,10)),0));
			nr_versao_w			:= trim(substr(r_c01_w.ds_valores,39,3));
			
			begin
				select	nr_seq_origem
				into	nr_seq_origem_p
				from	ptu_resposta_autorizacao
				where	nr_seq_execucao		= nr_seq_execucao_w
				and	cd_unimed_executora	= cd_unimed_executora_w;
			exception
			when others then
				nr_seq_origem_p	:= null;
			end;
			
			if	(nr_seq_origem_p	is not null) then
				begin
					select	nr_seq_pedido_compl,
						nr_seq_pedido_aut
					into	nr_seq_pedido_compl_w,
						nr_seq_pedido_aut_w
					from	ptu_controle_execucao
					where	nr_sequencia	= nr_seq_origem_p;
				exception
				when others then
					nr_seq_pedido_compl_w	:= null;
					nr_seq_pedido_aut_w	:= null;
				end;

				if	(nr_seq_pedido_compl_w	is not null) then
					select	nr_seq_guia,
						nr_seq_requisicao
					into	nr_seq_guia_w,
						nr_seq_requisicao_w
					from	ptu_pedido_compl_aut
					where	nr_sequencia	= nr_seq_pedido_compl_w;
				elsif	(nr_seq_pedido_aut_w	is not null) then
					select	nr_seq_guia,
						nr_seq_requisicao
					into	nr_seq_guia_w,
						nr_seq_requisicao_w
					from	ptu_pedido_autorizacao
					where	nr_sequencia	= nr_seq_pedido_aut_w;
				end if;
			end if;
			
			if	(ds_tipo_cliente_w	= 'UNIMED') then
				ie_tipo_cliente_w	:= 'U';
			elsif	(ds_tipo_cliente_w	= 'PORTAL') then
				ie_tipo_cliente_w	:= 'P';
			elsif	(ds_tipo_cliente_w	= 'PRESTADOR') then
				ie_tipo_cliente_w	:= 'R';
			end if;

			if	(cd_transacao_w	= '00700') then
				insert	into ptu_decurso_prazo
					(nr_sequencia, cd_transacao, ie_tipo_cliente,
					 cd_unimed_executora, cd_unimed_beneficiario, nr_seq_execucao,
					 dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					 nm_usuario_nrec, nr_versao, nr_seq_requisicao,
					 nr_seq_guia, nr_seq_origem, ds_arquivo_pedido)
				values	(ptu_decurso_prazo_seq.NextVal, cd_transacao_w,ie_tipo_cliente_w,
					 cd_unimed_executora_w, cd_unimed_beneficiario_w, nr_seq_execucao_w,
					 sysdate, nm_usuario_p, sysdate,
					 nm_usuario_p, nr_versao_w, nr_seq_requisicao_w,
					 nr_seq_guia_w, nr_seq_origem_p, ds_arquivo_p);
					 
			if	(nr_seq_requisicao_w	is not null) then
				pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L','Recebido o Decurso de Prazo da Unimed '|| cd_unimed_executora_w, '', nm_usuario_p );
			elsif	(nr_seq_guia_w	is not null) then
				pls_guia_gravar_historico(nr_seq_guia_w,2,'Recebido o Decurso de Prazo da Unimed '|| cd_unimed_executora_w, '', nm_usuario_p );
			end if;		 
			
			end if;
		end if;
	end if;
end loop;

commit;

ptu_gestao_envio_confirmacao(nr_seq_origem_p, cd_estabelecimento_p, 'DP', nm_usuario_p);

end ptu_importar_decurso_prazo_v50;
/
