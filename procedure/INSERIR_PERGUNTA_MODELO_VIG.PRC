create or replace
procedure	inserir_pergunta_modelo_vig(
			nr_seq_pergunta_p			number,
			nr_seq_servico_p			number,
			nm_usuario_p			varchar2,
			nr_seq_modelo_p		out	number) is

nr_seq_modelo_w			number(10);
nr_seq_apres_w			number(10);
qt_existe_w			number(10);

begin

nr_seq_modelo_p	:= 0;

select	nvl(max(nr_seq_modelo),0)
into	nr_seq_modelo_w
from	servico_questionario
where	nr_seq_servico = nr_seq_servico_p
and	trunc(sysdate) between trunc(nvl(dt_inicio_vigencia,sysdate)) 
			and trunc(nvl(dt_fim_vigencia,sysdate));
select	count(*)
into	qt_existe_w
from	modelo_conteudo
where	nr_seq_modelo = nr_seq_modelo_w
and	nr_seq_pergunta = nr_seq_pergunta_p;

if	(nr_seq_modelo_w <> 0) and
	(qt_existe_w = 0) then
	begin

	select	nvl(max(nr_seq_apres),0) + 5
	into	nr_seq_apres_w
	from	modelo_conteudo
	where	nr_seq_modelo = nr_seq_modelo_w;

	insert into modelo_conteudo(
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nr_seq_modelo,
		ie_readonly,
		ie_obrigatorio,
		ie_tabstop,
		nr_seq_pergunta,
		ie_negrito,
		ie_italico,
		qt_altura_perg_visual,
		ie_apres_cad_pre,
		ie_sublinhado,
		qt_tamanho,
		nr_seq_apres) values(
			modelo_conteudo_seq.nextval,
			sysdate,
			sysdate,
			nm_usuario_p,
			nr_seq_modelo_w,
			'N',
			'N',
			'S',
			nr_seq_pergunta_p,
			'N',
			'N',
			15,
			'N',
			'N',
			200,
			nr_seq_apres_w);

	commit;

	nr_seq_modelo_p	:= nr_seq_modelo_w;

	end;
end if;

end inserir_pergunta_modelo_vig;
/