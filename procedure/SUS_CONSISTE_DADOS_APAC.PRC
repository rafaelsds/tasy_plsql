CREATE OR REPLACE
PROCEDURE Sus_Consiste_Dados_Apac
	(	nm_campo_p		varchar2,
		nr_apac_p		number,
		nr_seq_apac_p		number,
		cd_procedimento_p	number,
		ie_origem_proced_p	number,
		cd_cid_princ_p		varchar2,
		nm_usuario_p		varchar2,
		ie_tipo_consiste_p	number,
		ds_erro_p	out	varchar2) is


/* ie_tipo_consiste_p
	- 1 fechamento da conta
	- 2 saida do campo
*/

ie_sexo_cid_w		varchar2(15)	:= 'A';
ds_detalhe_w		varchar2(255)	:= '';
ds_erro_w		varchar2(255)	:= '';
nr_seq_geral_w		number(10);
nr_interno_conta_w	number(10);
cd_estabelecimento_w	number(4);
cd_cid_topografia_w	varchar2(4);
cd_estadio_w		number(4);
ie_estadio_w		varchar2(10);
ie_sexo_pac_w		varchar2(15);
nr_atendimento_w	Sus_Apac_Unif.nr_atendimento%type;
ie_tipo_laudo_apac_w    sus_procedimento.ie_tipo_laudo_apac%type;

BEGIN
if	(ie_tipo_consiste_p = 1) then
	begin
	select	nvl(nr_interno_conta,0),
		cd_estabelecimento,
		cd_cid_topografia,
		cd_estadio,
		nr_atendimento
	into	nr_interno_conta_w,
		cd_estabelecimento_w,
		cd_cid_topografia_w,
		cd_estadio_w,
		nr_atendimento_w
	from	sus_apac_unif
	where	nr_apac		= nr_apac_p
	and	nr_sequencia	= nr_seq_apac_p;

	select	nvl(max(ie_tipo_laudo_apac),'00')
	into	ie_tipo_laudo_apac_w
	from	sus_procedimento
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;    
	end;
end if;

if	(Sus_Obter_Inco_Ativa(54)) and
	(cd_cid_princ_p is not null) then
	begin
	nr_seq_geral_w	:= 0;
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_geral_w
	from	sus_procedimento_cid
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= 7
	and	cd_doenca_cid		= cd_cid_princ_p;
	
	if	(nr_seq_geral_w		= 0) then
		begin
		if	(ie_tipo_consiste_p = 1) then
			ds_detalhe_w	:= WHEB_MENSAGEM_PCK.get_texto(279347) || nr_apac_p || WHEB_MENSAGEM_PCK.get_texto(279348) || cd_procedimento_p || WHEB_MENSAGEM_PCK.get_texto(279349) || cd_cid_princ_p;
			sus_gravar_inconsistencia(nr_interno_conta_w, 54, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
		elsif	(ie_tipo_consiste_p 	= 2) and
			(nm_campo_p		= 'CD_CID_PRINCIPAL') then
			ds_erro_w	:= ds_erro_w || '54, ';
		end if;
		end;
	end if;
	end;
end if;	

if	(Sus_Obter_Inco_Ativa(86)) and
	(cd_cid_topografia_w is not null) and
    (ie_tipo_laudo_apac_w in ('03','04')) then
	begin
	select	nvl(max(ie_estadio), 'N')
	into	ie_estadio_w
	from	cid_doenca
	where	cd_doenca_cid	= cd_cid_topografia_w;
	
	if	((ie_estadio_w	= 'N') and 
		not (cd_estadio_w is null)) or
		((ie_estadio_w	= 'S') and
		(cd_estadio_w is null)) then
		begin
		if	(ie_tipo_consiste_p = 1) then
			ds_detalhe_w	:= WHEB_MENSAGEM_PCK.get_texto(279347) || nr_apac_p || WHEB_MENSAGEM_PCK.get_texto(279348) || cd_procedimento_p || WHEB_MENSAGEM_PCK.get_texto(279351) || cd_cid_topografia_w;
			sus_gravar_inconsistencia(nr_interno_conta_w, 86, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
		elsif	(ie_tipo_consiste_p 	= 2) and
			(nm_campo_p		= 'CD_ESTADIO') then
			ds_erro_w	:= ds_erro_w || '86, ';
		end if;
		end; 
	end if;
	end;
end if;

/*cid principal da apac incompativel com o sexo do paciente */
if (Sus_Obter_Inco_Ativa(289)) then 
	begin
	select	nvl(max(a.ie_sexo),'A')
	into	ie_sexo_cid_w
	from	cid_doenca a
	where	a.cd_doenca_cid = cd_cid_princ_p;
	exception
		when others then
		ie_sexo_cid_w := 'A';
	end;
	begin 
	select	nvl(obter_sexo_pf(cd_pessoa_fisica,'C'),'X')
	into	ie_sexo_pac_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;
	exception
		when others then
		ie_sexo_pac_w := 'X';
	end;
	
	if	(ie_sexo_cid_w <> 'A') and
		(ie_sexo_pac_w <> 'X') and
		(ie_sexo_cid_w <> ie_sexo_pac_w) then		
		if	(ie_tipo_consiste_p = 1) then
			ds_detalhe_w	:= substr(wheb_mensagem_pck.get_texto(278139) || cd_cid_princ_p || wheb_mensagem_pck.get_texto(278141) || ie_sexo_cid_w || wheb_mensagem_pck.get_texto(278142) || ie_sexo_pac_w,1,255);
			sus_gravar_inconsistencia(nr_interno_conta_w, 289, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
		end if;
		
	end if;
	 
end if;

ds_erro_p	:= ds_erro_w;

end sus_consiste_dados_apac;
/
