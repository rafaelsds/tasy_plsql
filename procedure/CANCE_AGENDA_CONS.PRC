create or replace
procedure cance_agenda_cons	(	nr_seq_atendimento_p	number,
					nm_usuario_p		Varchar2) is 
nr_seq_agend_cons_w	number(10);
begin
if (nvl(nr_seq_atendimento_p,0) > 0) then
	
	
		update	agenda_consulta
		set	dt_cancelamento = sysdate, 
			nm_usuario_cancelamento = nm_usuario_p
		where	nr_seq_atendimento = nr_seq_atendimento_p;
end if;
commit;

end cance_agenda_cons;
/
