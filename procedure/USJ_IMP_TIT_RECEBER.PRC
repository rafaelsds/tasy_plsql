create or replace
procedure USJ_IMP_TIT_RECEBER(
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2,
			dt_emissao_p			date,
			dt_vencimento_p			date,
			dt_pagamento_previsto_p		date,
			vl_titulo_p			number,
			vl_saldo_titulo_p		number,
			vl_saldo_juros_p		number,
			vl_saldo_multa_p		number,
			cd_moeda_p			number,
			cd_portador_p			number,
			cd_tipo_portador_p		number,
			tx_juros_p			number,
			tx_multa_p			number,
			cd_tipo_taxa_juro_p		number,
			cd_tipo_taxa_multa_p		number,
			tx_desc_antecipacao_p		number,
			ie_situacao_p			varchar2,
			ie_tipo_emissao_titulo_p	number,
			ie_origem_titulo_p		varchar2,
			ie_tipo_titulo_p		varchar2,
			ie_tipo_inclusao_p		varchar2,
			cd_pessoa_fisica_p		varchar2,
			nr_interno_conta_p		number,
			cd_cgc_p			varchar2,
			cd_serie_p			varchar2,
			nr_documento_p			number,
			nr_sequencia_doc_p		number,
			cd_banco_p			number,
			cd_agencia_bancaria_p		varchar2,
			nr_bloqueto_p			varchar2,
			dt_liquidacao_p			date,
			nr_lote_contabil_p		number,
			ds_observacao_titulo_p		varchar2,
			nr_atendimento_p		number,
			nr_seq_protocolo_p		number,
			dt_contabil_p			date,
			nr_seq_conta_banco_p		number,
			dt_emissao_bloqueto_p		date,
			nr_seq_classe_p			number,
			nr_guia_p			varchar2,
			nr_nota_fiscal_p		number,
			nr_nosso_numero_p		varchar2,
			dt_ref_conta_p			date,
			cd_convenio_conta_p		number,
			nr_seq_terc_conta_p		number,
			nr_seq_nf_saida_p		number,
			nr_processo_aih_p		number,
			cd_tipo_recebimento_p		number,
			nr_seq_controle_pessoa_p	number,
			nr_seq_trans_fin_contab_p	number,
			nr_seq_contrato_p		number,
			vl_desc_previsto_p		number,
			nr_seq_carteira_cobr_p		number,
			dt_integracao_externa_p		date,
			cd_estab_financeiro_p		number,
			nr_titulo_externo_p		varchar2,
			nr_seq_mensalidade_p		number,
			nr_seq_mens_segurado_p		number,
			ie_pls_p			varchar2,
			ie_gerar_classif_p		varchar2) is 
/*

IE_TIPO_EMISSAO_TITULO			1 - Sem emiss�o Bloqueto      
					2 - Emiss�o Bloqueto  Origem
					3 - Emiss�o Bloqueto Banco    
					4 - Nota Promiss�ria          
					5 - Duplicata                 

CD_TIPO_PORTADOR			0 - Carteira                  
					1 - Banco                     
					2 - Empresa de Cobran�a
					3 - Advogado/Jur�dico         
					4 - Cart�rio                  
					5 - Cart�o de Cr�dito         
					6 - Adm financeira            

IE_ORIGEM_TITULO			1 - Nota Fiscal
					2 - Conta Paciente
					3 - Plano de Sa�de
					9 - Outros

ie_situacao_p
					1 - Aberto
					2 - Liquidado
					3 - Cancelado
					4 - Liquidado com Cheque
					5 - Titulo Transferido
					6 - Liquidado como perda

ie_tipo_titulo_p
					1 - Bloqueto
					2 - Duplicata
					3 - Nota Promiss�ria
					4 - Cheque
					5 - Guia
					6 - Nota Fiscal
					7 - Dep�sito banc�rio
					8 - Contas de terceiros

ie_tipo_inclusao_p
					1 - Manual
					2 - Autom�tica


*/

qt_tipo_emissao_titulo_w	number(10);
qt_cd_tipo_portador_w		number(10);
qt_ie_origem_titulo_w		number(10);
qt_ie_situacao_w		number(10);
qt_ie_tipo_titulo_w		number(10);
qt_ie_tipo_inclusao_w		number(10);
nr_titulo_w			number(10);
nr_seq_classif_w		number(10);
nr_seq_conta_financ_w		number(15);
ds_erro_w			varchar2(255);
ie_origem_titulo_w		varchar2(10);
nr_seq_classe_tit_rec_w		number(10);
ie_tipo_titulo_rec_w		titulo_receber.ie_tipo_titulo%type;

begin


select	count(b.vl_dominio)
into	qt_tipo_emissao_titulo_w
from	dominio a,
	valor_dominio b
where	a.cd_dominio	= b.cd_dominio
and	a.cd_dominio	= 702
and	b.vl_dominio	= ie_tipo_emissao_titulo_p;

select	count(b.vl_dominio)
into	qt_cd_tipo_portador_w
from	dominio a,
	valor_dominio b
where	a.cd_dominio	= b.cd_dominio
and	a.cd_dominio	= 703
and	b.vl_dominio	= cd_tipo_portador_p;

select	count(b.vl_dominio)
into	qt_ie_origem_titulo_w
from	dominio a,
	valor_dominio b
where	a.cd_dominio	= b.cd_dominio
and	a.cd_dominio	= 709
and	b.vl_dominio	= ie_origem_titulo_p;

select	count(b.vl_dominio)
into	qt_ie_situacao_w
from	dominio a,
	valor_dominio b
where	a.cd_dominio	= b.cd_dominio
and	a.cd_dominio	= 710
and	b.vl_dominio	= ie_situacao_p;

select	count(b.vl_dominio)
into	qt_ie_tipo_titulo_w
from	dominio a,
	valor_dominio b
where	a.cd_dominio	= b.cd_dominio
and	a.cd_dominio	= 712
and	b.vl_dominio	= ie_tipo_titulo_p;

select	count(b.vl_dominio)
into	qt_ie_tipo_inclusao_w
from	dominio a,
	valor_dominio b
where	a.cd_dominio	= b.cd_dominio
and	a.cd_dominio	= 713
and	b.vl_dominio	= ie_tipo_inclusao_p;

select	max(nr_titulo),
	max(ie_origem_titulo),
	max(nr_seq_classe),
	max(ie_tipo_titulo)
into	nr_titulo_w,
	ie_origem_titulo_w,
	nr_seq_classe_tit_rec_w,
	ie_tipo_titulo_rec_w
from	titulo_receber
where	nr_titulo_externo	= nr_titulo_externo_p;


if	(nr_titulo_w	is null)	then
	if	(cd_estabelecimento_p 		is not null) and
		(nm_usuario_p			is not null) and
		(dt_emissao_p			is not null) and
		(dt_vencimento_p		is not null) and
		(dt_pagamento_previsto_p	is not null) and
		(vl_titulo_p			is not null) and
		(vl_saldo_titulo_p		is not null) and
		(vl_saldo_juros_p		is not null) and
		(vl_saldo_multa_p		is not null) and
		(cd_moeda_p			is not null) and
		(cd_portador_p			is not null) and
		(tx_juros_p			is not null) and
		(tx_multa_p			is not null) and
		(cd_tipo_taxa_juro_p		is not null) and
		(cd_tipo_taxa_multa_p		is not null) and
		(tx_desc_antecipacao_p		is not null) then
		if	(qt_tipo_emissao_titulo_w	is not null) and
			(qt_cd_tipo_portador_w		is not null) and
			(qt_ie_origem_titulo_w		is not null) and
			(qt_ie_situacao_w		is not null) and
			(qt_ie_tipo_titulo_w		is not null) and
			(qt_ie_tipo_inclusao_w		is not null) then
			if	((cd_cgc_p is not null) and (cd_pessoa_fisica_p is null))
			or ((cd_pessoa_fisica_p is not null) and (cd_cgc_p is null))	then			
			
				select	titulo_seq.nextval
				into	nr_titulo_w
				from	dual;

				begin
				insert into titulo_receber(
					nr_titulo,
					cd_estabelecimento,
					dt_atualizacao,
					nm_usuario,
					dt_emissao,
					dt_vencimento,
					dt_pagamento_previsto,
					vl_titulo,
					vl_saldo_titulo,
					vl_saldo_juros,
					vl_saldo_multa,
					cd_moeda,
					cd_portador,
					cd_tipo_portador,
					tx_juros,
					tx_multa,
					cd_tipo_taxa_juro,
					cd_tipo_taxa_multa,
					tx_desc_antecipacao,
					ie_situacao,
					ie_tipo_emissao_titulo,
					ie_origem_titulo,
					ie_tipo_titulo,
					ie_tipo_inclusao,
					cd_pessoa_fisica,	
					nr_interno_conta,
					cd_cgc,
					cd_serie,
					nr_documento,
					nr_sequencia_doc,
					cd_banco,
					cd_agencia_bancaria,
					nr_bloqueto,
					dt_liquidacao,
					nr_lote_contabil,
					ds_observacao_titulo,
					nr_atendimento,
					nr_seq_protocolo,
					dt_contabil,
					nr_seq_conta_banco,
					dt_emissao_bloqueto,
					nr_seq_classe,
					nr_guia,
					nr_nota_fiscal,
					nr_nosso_numero,
					dt_ref_conta,
					cd_convenio_conta,
					nr_seq_terc_conta,
					nr_seq_nf_saida,
					nr_processo_aih,
					cd_tipo_recebimento,
					nr_seq_controle_pessoa,
					nr_seq_trans_fin_contab,
					nr_seq_contrato,
					vl_desc_previsto,
					nr_seq_carteira_cobr,
					dt_integracao_externa,
					cd_estab_financeiro,
					nr_titulo_externo,
					nr_seq_mensalidade,
					nr_seq_mens_segurado,
					ie_pls,
					nm_usuario_orig,
					dt_inclusao)
				values(	nr_titulo_w,
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p,
					trunc(dt_emissao_p,'dd'),
					trunc(dt_vencimento_p,'dd'),	
					trunc(dt_pagamento_previsto_p,'dd'),
					vl_titulo_p,
					vl_saldo_titulo_p,
					vl_saldo_juros_p,
					vl_saldo_multa_p,
					cd_moeda_p,
					cd_portador_p,
					cd_tipo_portador_p,
					tx_juros_p,
					tx_multa_p,
					cd_tipo_taxa_juro_p,
					cd_tipo_taxa_multa_p,
					tx_desc_antecipacao_p,
					ie_situacao_p,
					ie_tipo_emissao_titulo_p,
					ie_origem_titulo_p,
					ie_tipo_titulo_p,	
					ie_tipo_inclusao_p,
					cd_pessoa_fisica_p,	
					nr_interno_conta_p,
					cd_cgc_p,	
					cd_serie_p,
					nr_documento_p,
					nr_sequencia_doc_p,
					cd_banco_p,	
					cd_agencia_bancaria_p,
					nr_bloqueto_p,
					trunc(dt_liquidacao_p,'dd'),	
					nr_lote_contabil_p,
					ds_observacao_titulo_p,
					nr_atendimento_p,
					nr_seq_protocolo_p,
					trunc(dt_contabil_p,'dd'),
					nr_seq_conta_banco_p,
					trunc(dt_emissao_bloqueto_p,'dd'),
					nr_seq_classe_p,
					nr_guia_p,
					nr_nota_fiscal_p,
					nr_nosso_numero_p,
					trunc(dt_ref_conta_p,'dd'),
					cd_convenio_conta_p,
					nr_seq_terc_conta_p,
					nr_seq_nf_saida_p,
					nr_processo_aih_p,
					cd_tipo_recebimento_p,
					nr_seq_controle_pessoa_p,
					nr_seq_trans_fin_contab_p,
					nr_seq_contrato_p,	
					vl_desc_previsto_p,
					nr_seq_carteira_cobr_p,
					trunc(dt_integracao_externa_p,'dd'),
					cd_estab_financeiro_p,
					nr_titulo_externo_p,
					nr_seq_mensalidade_p,
					nr_seq_mens_segurado_p,
					ie_pls_p,
					nm_usuario_p,
					sysdate);

				if	(nvl(ie_gerar_classif_p,'N') = 'S') then
					if	(cd_pessoa_fisica_p is not null) then

						select	nvl(max(nr_sequencia),0) + 1
						into	nr_seq_classif_w
						from	titulo_receber_classif
						where	nr_titulo = nr_titulo_w;

						insert into titulo_receber_classif
							(nr_titulo,
							nr_sequencia,
							vl_classificacao,
							dt_atualizacao,
							nm_usuario,
							cd_conta_financ)
						values	(nr_titulo_w,
							nr_seq_classif_w,
							vl_titulo_p,
							sysdate,
							nm_usuario_p,
							243);
					end if;

					if	(nvl(cd_cgc_p,'X') <> 'X') then

						select	nvl(max(nr_sequencia),0) + 1
						into	nr_seq_classif_w
						from	titulo_receber_classif
						where	nr_titulo = nr_titulo_w;

						obter_conta_financeira( 'E',
									cd_estabelecimento_p,
									null,
									null,
									null,
									null,
									null,
									cd_cgc_p,
									null,
									nr_seq_conta_financ_w,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									null,
									ie_origem_titulo_w,
									null,
									nr_seq_classe_tit_rec_w,
									null,
									null,
									null,
									null,
									null,
									null,
									ie_tipo_titulo_rec_w,
									null);

						if	(nr_seq_conta_financ_w is not null) then
							insert into titulo_receber_classif
								(nr_titulo,
								nr_sequencia,
								vl_classificacao,
								dt_atualizacao,
								nm_usuario,
								cd_conta_financ)
							values	(nr_titulo_w,
								nr_seq_classif_w,
								vl_titulo_p,
								sysdate,
								nm_usuario_p,
								nr_seq_conta_financ_w);
						end if;
					end if;
				end if;

				exception
				when others then
					ds_erro_w := SQLERRM(sqlcode);

					insert into log_tasy(
						dt_atualizacao,
						nm_usuario,
						cd_log,
						ds_log)
					values(
						sysdate,
						nm_usuario_p,
						55708,
						'nr_titulo = ' || nr_titulo_w ||
						'DS_ERRO = ' || ds_erro_w);
				end;
			end if;
		end if;
	end if;
end if;

end USJ_IMP_TIT_RECEBER;
/
