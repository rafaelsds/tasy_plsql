create or replace
procedure verifica_historico_obrigatorio
			(	nr_seq_ordem_p	in	number,
				nr_seq_tipo_p	in	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_registro_w		number(10);
ds_tipo_w		varchar2(255);
begin

select 	count(1)
into	qt_registro_w
from	man_ordem_serv_tecnico
where	nr_seq_ordem_serv = nr_seq_ordem_p
and		nr_seq_tipo	= nr_seq_tipo_p;

if	(qt_registro_w = 0 ) then
	select	max(substr(obter_desc_expressao(cd_exp_tipo, ds_tipo),1,80))
	into	ds_tipo_w
	from	man_tipo_hist
	where	nr_sequencia	= nr_seq_tipo_p;

	wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(173239,'DS_TIPO='||ds_tipo_w||';NR_ORDEM='||nr_seq_ordem_p||';');
end if;

end verifica_historico_obrigatorio;
/