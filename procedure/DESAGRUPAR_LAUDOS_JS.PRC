create or replace
procedure desagrupar_laudos_js(	nr_sequencia_p		Varchar2,
				nm_usuario_p		Varchar2) is 
			
begin

update 	laudo_paciente 
set 	nr_agrupamento = null 
where 	nr_sequencia = nr_sequencia_p;

commit;

end desagrupar_laudos_js;
/