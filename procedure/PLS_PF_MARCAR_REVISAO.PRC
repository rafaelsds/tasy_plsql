create or replace
procedure pls_pf_marcar_revisao
			(	nm_usuario_p		varchar2,
				cd_pessoa_fisica_p	varchar2,
				ie_revisar_p		number) is 

ie_revisar_w		varchar(10) := 'R';
				
begin

if	(ie_revisar_p	= 1) then
	ie_revisar_w	:= 'R';
elsif	(ie_revisar_p	= 2) then
	ie_revisar_w	:= 'N';
end if;

update	pessoa_fisica
set	ie_revisar		= ie_revisar_w,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

commit;	

end pls_pf_marcar_revisao;
/