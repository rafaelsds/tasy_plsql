create or replace procedure pls_gravar_historico_pagador
			(	ie_opcao_p			number, 
				nr_sequencia_p			number, 
				cd_pessoa_fisica_p		varchar2, 
				cd_cgc_p			varchar2, 
				ie_endereco_boleto_p		varchar2, 
				ie_tipo_pagador_p		varchar2, 
				ie_envia_cobranca_p		varchar2, 
				cd_sistema_anterior_p		varchar2, 
				dt_primeira_mensalidade_p	date, 
				nr_seq_destino_corresp_p	number, 
				ie_calc_primeira_mens_p		varchar2, 
				ie_calculo_proporcional_p	varchar2, 
				ie_pessoa_comprovante_p		varchar2, 
				ie_notificacao_p		varchar2, 
				ie_taxa_emissao_boleto_p	varchar2, 
				ie_inadimplencia_via_adic_p	varchar2, 
				nr_seq_classif_itens_p		number, 
				ds_email_p			varchar2, 
				dt_inicio_vigencia_p		date, 
				dt_fim_vigencia_p		date, 
				dt_dia_vencimento_p		number, 
				nr_seq_forma_cobranca_p		varchar2, 
				ie_mes_vencimento_p		varchar2, 
				cd_condicao_pagamento_p		number, 
				cd_tipo_portador_p		number, 
				cd_portador_p			number, 
				nr_seq_conta_banco_p		number, 
				nr_seq_carteira_cobr_p		number, 
				cd_autenticidade_p		varchar2, 
				ie_portador_exclusivo_p		varchar2, 
				cd_banco_p			number, 
				cd_agencia_bancaria_p		varchar2, 
				ie_digito_agencia_p		varchar2, 
				cd_conta_p			varchar2, 
				ie_digito_conta_p		varchar2, 
				nr_seq_empresa_p		number, 
				cd_profissao_p			number, 
				nr_seq_vinculo_empresa_p	number, 
				cd_matricula_p			varchar2, 
				ie_geracao_nota_titulo_p	varchar2, 
				ie_destacar_reajuste_p		varchar2, 
				ie_gerar_cobr_escrit_p		varchar2, 
				ds_observacao_p			varchar2, 
				cd_estabelecimento_p		varchar2, 
				nm_usuario_p			varchar2) is 
/* 
ie_opcao_p 
1 = Pagador 
2 = Informa��o financeira pagador 
*/ 
 
ds_alteracao_w			varchar2(4000); 
ds_retorno_antigo_w		varchar2(4000); 
ds_retorno_novo_w		varchar2(4000); 
ds_titulo_w			varchar2(255); 
nr_seq_pagador_w		number(10); 
 
cd_pessoa_fisica_w		varchar2(10); 
cd_cgc_w			varchar2(14); 
ie_endereco_boleto_w		varchar2(4); 
ie_tipo_pagador_w		varchar2(1); 
ie_envia_cobranca_w		varchar2(1); 
dt_primeira_mensalidade_w	date; 
ie_calc_primeira_mens_w		varchar2(1); 
ie_calculo_proporcional_w	varchar2(1); 
ie_pessoa_comprovante_w		varchar2(2); 
ie_notificacao_w		varchar2(2); 
ie_taxa_emissao_boleto_w	varchar2(2); 
ie_inadimplencia_via_adic_w	varchar2(1); 
nr_seq_classif_itens_w		number(10); 
nm_pessoa_nova_w		varchar2(255); 
nm_pessoa_antiga_w		varchar2(255); 
ds_endereco_boleto_antigo_w	varchar2(255); 
ds_endereco_boleto_novo_w	varchar2(255); 
 
cd_sistema_anterior_w		varchar2(30); 
dt_inicio_vigencia_w		date; 
nr_seq_destino_corresp_w	number(10); 
dt_fim_vigencia_w		date; 
dt_dia_vencimento_w		number(2); 
nr_seq_forma_cobranca_w		varchar2(2); 
ie_mes_vencimento_w		varchar2(1); 
cd_condicao_pagamento_w		number(10); 
cd_tipo_portador_w		number(5); 
cd_portador_w			number(10); 
nr_seq_conta_banco_w		number(10); 
nr_seq_carteira_cobr_w		number(10); 
cd_autenticidade_w		varchar2(10); 
ie_portador_exclusivo_w		varchar2(1); 
cd_banco_w			number(5); 
cd_agencia_bancaria_w		varchar2(8); 
ie_digito_agencia_w		varchar2(1); 
cd_conta_w			varchar2(20); 
ie_digito_conta_w		varchar2(1); 
nr_seq_empresa_w		number(10); 
cd_profissao_w			number(10); 
nr_seq_vinculo_empresa_w	number(10); 
cd_matricula_w			varchar2(20); 
ie_geracao_nota_titulo_w	varchar2(2); 
ie_destacar_reajuste_w		varchar2(1); 
ie_gerar_cobr_escrit_w		varchar2(2); 
ds_observacao_w			varchar2(4000); 
qt_registro_w			number(10); 
ds_destino_anterior_w		varchar2(255); 
ds_destino_novo_w		varchar2(255); 
ds_classif_item_anterior_w	varchar2(255); 
ds_classif_item_novo_w		varchar2(255); 
ds_cart_bancaria_anterior_w	varchar2(255); 
ds_cart_bancaria_novo_w		varchar2(255); 
ds_email_w			pls_contrato_pagador.ds_email%type; 
 
begin 
 
if	(ie_opcao_p = 1) then 
	select	count(1) 
	into	qt_registro_w 
	from	pls_contrato_pagador 
	where	nr_sequencia	= nr_sequencia_p; 
	 
	if	(qt_registro_w > 0) then 
		select	cd_pessoa_fisica, 
			cd_cgc, 
			ie_endereco_boleto, 
			ie_tipo_pagador, 
			ie_envia_cobranca, 
			cd_sistema_anterior, 
			dt_primeira_mensalidade, 
			nr_seq_destino_corresp, 
			ie_calc_primeira_mens, 
			ie_calculo_proporcional, 
			ie_pessoa_comprovante, 
			ie_notificacao, 
			ie_taxa_emissao_boleto, 
			ie_inadimplencia_via_adic, 
			nr_seq_classif_itens, 
			obter_nome_pf_pj(cd_pessoa_fisica_p, cd_cgc_p), 
			obter_nome_pf_pj(cd_pessoa_fisica, cd_cgc), 
			nr_sequencia, 
			ds_email 
		into	cd_pessoa_fisica_w, 
			cd_cgc_w, 
			ie_endereco_boleto_w, 
			ie_tipo_pagador_w, 
			ie_envia_cobranca_w, 
			cd_sistema_anterior_w, 
			dt_primeira_mensalidade_w, 
			nr_seq_destino_corresp_w, 
			ie_calc_primeira_mens_w, 
			ie_calculo_proporcional_w, 
			ie_pessoa_comprovante_w, 
			ie_notificacao_w, 
			ie_taxa_emissao_boleto_w, 
			ie_inadimplencia_via_adic_w, 
			nr_seq_classif_itens_w, 
			nm_pessoa_nova_w, 
			nm_pessoa_antiga_w, 
			nr_seq_pagador_w, 
			ds_email_w 
		from	pls_contrato_pagador 
		where	nr_sequencia	= nr_sequencia_p; 
		 
		ds_titulo_w	:= 'Altera��o no cadastro do pagador'; 
		 
		if	(cd_pessoa_fisica_p <> cd_pessoa_fisica_w) then 
			ds_alteracao_w	:= substr('Alterado a pessoa f�sica do pagador, de "'||cd_pessoa_fisica_w||' - '||nm_pessoa_antiga_w||'" para "'|| 
						cd_pessoa_fisica_p||' - '||nm_pessoa_nova_w||'".'||chr(13)||chr(10),1,4000); 
		elsif	(cd_cgc_p <> cd_cgc_w) then 
			ds_alteracao_w	:= substr('Alterado a pessoa jur�dica do pagador, de "'||cd_cgc_w||' - '||nm_pessoa_antiga_w||'" para "'||cd_cgc_p|| 
						' - '||nm_pessoa_nova_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(ds_email_p <> ds_email_w) then 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o e-mail do pagador, de "'||ds_email_w||'" para "'|| ds_email_p||'".'||chr(13)||chr(10),1,4000); 
		elsif	(ds_email_p is not null and ds_email_w is null) then 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Informado o e-mail do pagador "' || ds_email_p||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(ie_endereco_boleto_p <> ie_endereco_boleto_w) then 
			select	obter_valor_dominio(1941,ie_endereco_boleto_w), 
				obter_valor_dominio(1941,ie_endereco_boleto_p) 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o endere�o de correspond�ncia do pagador, de "'||ds_retorno_antigo_w||'" para "'|| 
						ds_retorno_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(ie_tipo_pagador_p <> ie_tipo_pagador_w) then 
			select	decode(ie_tipo_pagador_w,'P','Principal','Secund�rio'), 
				decode(ie_tipo_pagador_p,'P','Principal','Secund�rio') 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o pagador, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w||'".'||chr(13) 
						||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(ie_envia_cobranca_p,' ') <> nvl(ie_envia_cobranca_w,' ')) then 
			select	obter_valor_dominio(2817,ie_envia_cobranca_w), 
				obter_valor_dominio(2817,ie_envia_cobranca_p) 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o tipo de envio da cobran�a do pagador, de "'||ds_retorno_antigo_w||'" para "'|| 
						ds_retorno_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(cd_sistema_anterior_p,' ') <> nvl(cd_sistema_anterior_w,' ')) then 
			ds_alteracao_w	:= substr(ds_alteracao_w || 'Alterado pagador anterior, de "' || cd_sistema_anterior_w || '" para "' || 
						cd_sistema_anterior_p || '".' || chr(13) || chr(10),1,4000); 
		end if; 
		 
		if	(dt_primeira_mensalidade_p <> dt_primeira_mensalidade_w) then 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a data de gera��o da primeira mensalidade, de "'||dt_primeira_mensalidade_w|| 
						'" para "'||dt_primeira_mensalidade_p||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(nr_seq_destino_corresp_p,0) <> nvl(nr_seq_destino_corresp_w,0)) then 
			begin 
			select	ds_destino 
			into	ds_destino_anterior_w 
			from	pls_destino_corresp 
			where	cd_estabelecimento	= cd_estabelecimento_p 
			and	nr_sequencia		= nr_seq_destino_corresp_w; 
			exception 
				when others then 
					ds_destino_anterior_w	:= ''; 
			end; 
			 
			begin 
			select	ds_destino 
			into	ds_destino_novo_w 
			from	pls_destino_corresp 
			where  cd_estabelecimento	= cd_estabelecimento_p 
			and		nr_sequencia		= nr_seq_destino_corresp_p; 
			exception 
				when others then 
					ds_destino_novo_w	:= ''; 
			end; 
			ds_alteracao_w	:= substr(ds_alteracao_w || 'Alterado o destino de correspond�ncia, de "'||ds_destino_anterior_w||'" para "'|| 
						ds_destino_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(ie_calc_primeira_mens_p <> ie_calc_primeira_mens_w) then 
			select	decode(ie_calc_primeira_mens_w,'I','Integral','Proporcional'), 
				decode(ie_calc_primeira_mens_p,'I','Integral','Proporcional') 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a forma de c�lculo da 1� mensalidade, de "'||ds_retorno_antigo_w||'" para "'|| 
						ds_retorno_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(ie_calculo_proporcional_p <> ie_calculo_proporcional_w) then 
			select	decode(ie_calculo_proporcional_w,'A','M�s calend�rio ades�o','Data vencimento'), 
				decode(ie_calculo_proporcional_p,'A','M�s calend�rio ades�o','Data vencimento') 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a data de refer�ncia para o c�lculo proporcional, de "'||ds_retorno_antigo_w|| 
						'" para "'||ds_retorno_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(ie_pessoa_comprovante_p,' ') <> nvl(ie_pessoa_comprovante_w,' ')) then 
			select	obter_valor_dominio(2648,ie_pessoa_comprovante_w), 
				obter_valor_dominio(2648,ie_pessoa_comprovante_p) 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a pessoa do comprovante de pagamento, de "'||ds_retorno_antigo_w||'" para "' 
						||ds_retorno_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(ie_notificacao_p,' ') <> nvl(ie_notificacao_w,' ')) then 
			select	obter_valor_dominio(2883,ie_notificacao_w), 
				obter_valor_dominio(2883,ie_notificacao_p) 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a forma de notifica��o do pagador, de "'||ds_retorno_antigo_w||'" para "'|| 
						ds_retorno_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(ie_taxa_emissao_boleto_p,' ') <> nvl(ie_taxa_emissao_boleto_w,' ')) then 
			select	obter_valor_dominio(3312,ie_taxa_emissao_boleto_w), 
				obter_valor_dominio(3312,ie_taxa_emissao_boleto_p) 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a taxa de emiss�o do boleto, de "'||ds_retorno_antigo_w||'" para "'|| 
						ds_retorno_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(ie_inadimplencia_via_adic_p <> ie_inadimplencia_via_adic_w) then 
			select	decode(ie_inadimplencia_via_adic_w,'S','Sim','N�o'), 
				decode(ie_inadimplencia_via_adic_p,'S','Sim','N�o') 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o campo consistir inadimpl�ncia ao gerar a via adicional do cart�o, de "'|| 
						ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if (nvl(nr_seq_classif_itens_p,0) <> nvl(nr_seq_classif_itens_w,0)) then 
			begin 
			select	ds_classificacao 
			into	ds_classif_item_anterior_w 
			from	pls_classif_itens_pagador 
			where	nr_sequencia	= nr_seq_classif_itens_w; 
			exception 
			when others then 
				ds_classif_item_anterior_w	:= ''; 
			end; 
			 
			begin 
			select	ds_classificacao 
			into	ds_classif_item_novo_w 
			from	pls_classif_itens_pagador 
			where	nr_sequencia	= nr_seq_classif_itens_p; 
			exception 
			when others then 
				ds_classif_item_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a situa��o trabalhista, de "'||ds_classif_item_anterior_w||'" para "'|| 
						ds_classif_item_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
	end if; 
elsif	(ie_opcao_p = 2) then 
	select	count(1) 
	into	qt_registro_w 
	from	pls_contrato_pagador_fin 
	where	nr_sequencia	= nr_sequencia_p; 
	 
	if	(qt_registro_w > 0) then 
		select	dt_inicio_vigencia, 
			dt_fim_vigencia, 
			dt_dia_vencimento, 
			nr_seq_forma_cobranca, 
			ie_mes_vencimento, 
			cd_condicao_pagamento, 
			cd_tipo_portador, 
			cd_portador, 
			nr_seq_conta_banco, 
			nr_seq_carteira_cobr, 
			cd_autenticidade, 
			ie_portador_exclusivo, 
			cd_banco, 
			cd_agencia_bancaria, 
			ie_digito_agencia, 
			cd_conta, 
			ie_digito_conta, 
			nr_seq_empresa, 
			cd_profissao, 
			nr_seq_vinculo_empresa, 
			cd_matricula, 
			nr_seq_pagador, 
			ie_geracao_nota_titulo, 
			ie_destacar_reajuste, 
			ie_gerar_cobr_escrit, 
			ds_observacao 
		into	dt_inicio_vigencia_w, 
			dt_fim_vigencia_w, 
			dt_dia_vencimento_w, 
			nr_seq_forma_cobranca_w, 
			ie_mes_vencimento_w, 
			cd_condicao_pagamento_w, 
			cd_tipo_portador_w, 
			cd_portador_w, 
			nr_seq_conta_banco_w, 
			nr_seq_carteira_cobr_w, 
			cd_autenticidade_w, 
			ie_portador_exclusivo_w, 
			cd_banco_w, 
			cd_agencia_bancaria_w, 
			ie_digito_agencia_w, 
			cd_conta_w, 
			ie_digito_conta_w, 
			nr_seq_empresa_w, 
			cd_profissao_w, 
			nr_seq_vinculo_empresa_w, 
			cd_matricula_w, 
			nr_seq_pagador_w, 
			ie_geracao_nota_titulo_w, 
			ie_destacar_reajuste_w, 
			ie_gerar_cobr_escrit_w, 
			ds_observacao_w 
		from	pls_contrato_pagador_fin 
		where	nr_sequencia	= nr_sequencia_p; 
		 
		ds_titulo_w	:= 'Altera��o no cadastro de informa��o financeira do pagador'; 
		 
		if	((dt_inicio_vigencia_p <> dt_inicio_vigencia_w) or (dt_fim_vigencia_p <> dt_fim_vigencia_w)) then 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a data de vig�ncia da informa��o financeira, de ('||dt_inicio_vigencia_w||' � '|| 
						dt_fim_vigencia_w||') para ('||dt_inicio_vigencia_p||' � '||dt_fim_vigencia_p||'), na informa��o financeira '|| 
						nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(dt_dia_vencimento_p <> dt_dia_vencimento_w) then 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o dia de vencimento, de "'||dt_dia_vencimento_w||'" para "'||dt_dia_vencimento_p|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nr_seq_forma_cobranca_p <> nr_seq_forma_cobranca_w) then 
			select	obter_valor_dominio(1720,nr_seq_forma_cobranca_w), 
				obter_valor_dominio(1720,nr_seq_forma_cobranca_p) 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a forma de cobran�a, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(ie_mes_vencimento_p,' ') <> nvl(ie_mes_vencimento_w,' ')) then 
			select	decode(ie_mes_vencimento_w,'P','M�s posterior','N','M�s anterior','M�s atual'), 
				decode(ie_mes_vencimento_p,'P','M�s posterior','N','M�s anterior','M�s atual') 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o m�s de vencimento, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(cd_condicao_pagamento_p,0) <> nvl(cd_condicao_pagamento_w,0)) then 
			begin 
			select	ds_condicao_pagamento 
			into	ds_retorno_antigo_w 
			from	condicao_pagamento 
			where	cd_condicao_pagamento = cd_condicao_pagamento_w; 
			exception 
			when others then 
				ds_retorno_antigo_w	:= ''; 
			end; 
			 
			begin 
			select	ds_condicao_pagamento 
			into	ds_retorno_novo_w 
			from	condicao_pagamento 
			where	cd_condicao_pagamento = cd_condicao_pagamento_p; 
			exception 
			when others then 
				ds_retorno_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a condi��o de pagamento, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(cd_tipo_portador_p,0) <> nvl(cd_tipo_portador_w,0)) then 
			select	obter_valor_dominio(703,cd_tipo_portador_w), 
				obter_valor_dominio(703,cd_tipo_portador_p) 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o tipo de portador, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(cd_portador_p,0) <> nvl(cd_portador_w,0)) then 
			begin 
			select	ds_portador 
			into	ds_retorno_antigo_w 
			from	portador 
			where	cd_portador = cd_portador_w; 
			exception 
			when others then 
				ds_retorno_antigo_w	:= ''; 
			end; 
			 
			begin 
			select	ds_portador 
			into	ds_retorno_novo_w 
			from	portador 
			where	cd_portador = cd_portador_p; 
			exception 
			when others then 
				ds_retorno_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o portador, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(nr_seq_conta_banco_p,0) <> nvl(nr_seq_conta_banco_w,0)) then 
			begin 
			select	substr(ds_conta,1,255) 
			into	ds_retorno_antigo_w 
			from	banco_estabelecimento_v 
			where	nr_sequencia	= nr_seq_conta_banco_w; 
			exception 
			when others then 
				ds_retorno_antigo_w	:= ''; 
			end; 
			 
			begin 
			select	substr(ds_conta,1,255) 
			into	ds_retorno_novo_w 
			from	banco_estabelecimento_v 
			where	nr_sequencia	= nr_seq_conta_banco_w; 
			exception 
			when others then 
				ds_retorno_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a Ag�ncia/Conta Estab. de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	nvl(nr_seq_carteira_cobr_p,0) <> nvl(nr_seq_carteira_cobr_w,0) then 
			begin 
			select	substr(cd_carteira || ' - ' || ds_carteira,1,255) ds 
			into	ds_cart_bancaria_anterior_w 
			from	banco_carteira 
			where	nr_sequencia = nr_seq_carteira_cobr_w; 
			exception 
				when others then 
					ds_cart_bancaria_anterior_w	:= ''; 
			end; 
			 
			begin 
			select	substr(cd_carteira || ' - ' || ds_carteira,1,255) ds 
			into	ds_cart_bancaria_novo_w 
			from	banco_carteira 
			where	nr_sequencia = nr_seq_carteira_cobr_p; 
			exception 
			when others then 
				ds_cart_bancaria_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterada a carteira banc�ria, de "'||ds_cart_bancaria_anterior_w||'" para "'|| 
						ds_cart_bancaria_novo_w||'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(cd_autenticidade_p,' ') <> nvl(cd_autenticidade_w,' ')) then 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o c�digo de autenticidade, de "'||cd_autenticidade_w||'" para "'||cd_autenticidade_p|| 
						'".'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(ie_portador_exclusivo_p,' ') <> nvl(ie_portador_exclusivo_w,' ')) then 
			select	decode(ie_portador_exclusivo_w,'S','Sim','N�o') 
			into	ds_retorno_antigo_w 
			from	dual; 
			 
			select	decode(ie_portador_exclusivo_p,'S','Sim','N�o') 
			into	ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o valor do campo Permite altera��o do portador (via lote), de "'||ds_retorno_antigo_w|| 
						'" para "'||ds_retorno_novo_w||'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(cd_banco_p,0) <> nvl(cd_banco_w,0)) then 
			begin 
			select	substr(obter_nome_banco(cd_banco_w),1,200) 
			into	ds_retorno_antigo_w 
			from	dual; 
			exception 
			when others then 
				ds_retorno_antigo_w	:= ''; 
			end; 
			 
			begin 
			select	substr(obter_nome_banco(cd_banco_p),1,200) 
			into	ds_retorno_novo_w 
			from	dual; 
			exception 
			when others then 
				ds_retorno_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o banco, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	((nvl(cd_agencia_bancaria_w,' ') <> nvl(cd_agencia_bancaria_p,' ')) or (nvl(ie_digito_agencia_w,' ') <> nvl(ie_digito_agencia_p,' '))) then 
			select	cd_agencia_bancaria_w||'-'||ie_digito_agencia_w 
			into	ds_retorno_antigo_w 
			from	dual; 
			 
			select	cd_agencia_bancaria_p||'-'||ie_digito_agencia_p 
			into	ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a Ag�ncia/Dig, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	((nvl(cd_conta_w,' ') <> nvl(cd_conta_p,' ')) or (nvl(ie_digito_conta_w,' ') <> nvl(ie_digito_conta_p,' '))) then 
			select	cd_conta_w||'-'||ie_digito_conta_w 
			into	ds_retorno_antigo_w 
			from	dual; 
			 
			select	cd_conta_p||'-'||ie_digito_conta_p 
			into	ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a Ag�ncia/Dig, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(nr_seq_empresa_w,0) <> nvl(nr_seq_empresa_p,0)) then 
			begin 
			select	substr(pls_obter_dados_empresa(nr_seq_empresa_w,'D'),1,250) 
			into	ds_retorno_antigo_w 
			from	dual; 
			exception 
			when others then 
				ds_retorno_antigo_w	:= ''; 
			end; 
			 
			begin 
			select	substr(pls_obter_dados_empresa(nr_seq_empresa_p,'D'),1,250) 
			into	ds_retorno_novo_w 
			from	dual; 
			exception 
			when others then 
				ds_retorno_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a empresa, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(cd_profissao_w,0) <> nvl(cd_profissao_p,0)) then 
			begin 
			select	substr(Obter_Desc_profissao(cd_profissao_w),1,255) 
			into	ds_retorno_antigo_w 
			from	dual; 
			exception 
			when others then 
				ds_retorno_antigo_w	:= ''; 
			end; 
			 
			begin 
			select	substr(Obter_Desc_profissao(cd_profissao_p),1,255) 
			into	ds_retorno_novo_w 
			from	dual; 
			exception 
			when others then 
				ds_retorno_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a empresa, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(nr_seq_vinculo_empresa_w,0) <> nvl(nr_seq_vinculo_empresa_p,0)) then 
			begin 
			select	ds_vinculo 
			into	ds_retorno_antigo_w 
			from	pls_empresa_vinculo 
			where	nr_sequencia = nr_seq_vinculo_empresa_w; 
			exception 
			when others then 
				ds_retorno_antigo_w	:= ''; 
			end; 
			 
			begin 
			select	ds_vinculo 
			into	ds_retorno_novo_w 
			from	pls_empresa_vinculo 
			where	nr_sequencia = nr_seq_vinculo_empresa_p; 
			exception 
			when others then 
				ds_retorno_novo_w	:= ''; 
			end; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o v�nculo com a empresa, de "'||ds_retorno_antigo_w||'" para "'||ds_retorno_novo_w|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(cd_matricula_w,' ') <> nvl(cd_matricula_p,' ')) then 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado a matr�cula, de "'||cd_matricula_w||'" para "'||cd_matricula_p|| 
						'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(nvl(ie_geracao_nota_titulo_w,' ') <> nvl(ie_geracao_nota_titulo_p,' ')) then 
			select	decode(ie_geracao_nota_titulo_w,'NT','Nota fiscal e t�tulo','Somente t�tulo'), 
				decode(ie_geracao_nota_titulo_p,'NT','Nota fiscal e t�tulo','Somente t�tulo') 
			into	ds_retorno_antigo_w, 
				ds_retorno_novo_w 
			from	dual; 
			 
			ds_alteracao_w	:= substr(ds_alteracao_w ||'Alterado o valor do campo Gera��o de Nota fiscal/T�tulo, de "'||ds_retorno_antigo_w||'" para "'|| 
						ds_retorno_novo_w||'", na informa��o financeira '||nr_sequencia_p||'.'||chr(13)||chr(10),1,4000); 
		end if; 
		 
		if	(ie_destacar_reajuste_p <> ie_destacar_reajuste_w) then 
			select	substr(ds_alteracao_w ||'Alterado o destaque de taxa de reajuste, de "'||decode(ie_destacar_reajuste_w,'S','Ativo','N','Inativo','')|| 
					'" para "'||decode(ie_destacar_reajuste_p,'S','Ativo','N','Inativo','')||'".'||chr(13)||chr(10),1,4000) 
			into	ds_alteracao_w 
			from	dual; 
		end if; 
		 
		if	(ie_gerar_cobr_escrit_p <> ie_gerar_cobr_escrit_w) then 
			select	substr(ds_alteracao_w ||'Alterado a gera��o de cobran�a escritural para o t�tulo, de "'|| 
				decode(ie_gerar_cobr_escrit_w,'S','Ativo','N','Inativo','')||'" para "'||decode(ie_gerar_cobr_escrit_p,'S','Ativo','N','Inativo','')|| 
				'".'||chr(13)||chr(10),1,4000) 
			into	ds_alteracao_w 
			from	dual; 
		end if; 
		 
		if	(nvl(ds_observacao_p,' ') <> nvl(ds_observacao_w,' ')) then 
			ds_alteracao_w	:= substr(ds_alteracao_w || 'Alterado a observa��o, de "' || ds_observacao_w || '" para "' || ds_observacao_p || 
						'".' || chr(13) || chr(10),1,4000); 
		end if; 
	end if; 
end if; 
 
if	(ds_alteracao_w is not null) then 
	insert into pls_pagador_historico 
		(nr_sequencia, 
		dt_historico, 
		nm_usuario_historico, 
		ds_titulo, 
		ds_historico, 
		nr_seq_pagador, 
		cd_estabelecimento, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		ie_tipo_historico) 
	values	(pls_pagador_historico_seq.nextval, 
		sysdate, 
		nm_usuario_p, 
		ds_titulo_w, 
		ds_alteracao_w, 
		nr_seq_pagador_w, 
		cd_estabelecimento_p, 
		sysdate, 
		nm_usuario_p, 
		sysdate, 
		nm_usuario_p, 
		'S'); 
end if; 
 
commit; 
 
end pls_gravar_historico_pagador;
/
 