create or replace
PROCEDURE Consistir_Aprov_Exame_Lote(	  nr_seq_lote_p		number,
					  nm_usuario_p		varchar2,
					  cd_perfil_ativo_p	number,
					  ds_consist_p		out varchar2) is

nr_seq_prescr_w			number(10);
nr_prescricao_w 		number(10);

cursor C01 is
	SELECT 	c.nr_sequencia,
		c.nr_prescricao
	FROM  	lab_lote_exame_item a,
		lab_lote_exame b,
		prescr_procedimento c	  
	WHERE 	b.nr_sequencia =  a.nr_seq_lote
	AND	a.nr_prescricao = c.nr_prescricao 
	AND	a.nr_seq_prescr = c.nr_sequencia
	AND	b.nr_sequencia =  nr_seq_lote_p;	
begin
open C01;
loop
fetch C01 into	
	nr_seq_prescr_w,
	nr_prescricao_w;
exit when C01%notfound;
	begin
		Consistir_Aprov_Exame_Regra(nr_prescricao_w,nr_seq_prescr_w,nm_usuario_p,'A',cd_perfil_ativo_p,ds_consist_p, 725);
	end;
end loop;
close C01;
		
		
end Consistir_Aprov_Exame_Lote;
/