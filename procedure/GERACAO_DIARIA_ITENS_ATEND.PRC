create or replace
procedure Geracao_Diaria_Itens_Atend(	dt_parametro_p	Date,
					nm_usuario_p	Varchar2) is 

nr_atendimento_w	number(10,0);
qt_regra_evento_w	number(10,0);
					
CURSOR C00 IS
      select 	e.nr_atendimento
	from 	atendimento_paciente e
       where 	e.dt_entrada    <= dt_parametro_p
         and 	e.dt_fim_conta 		is null
	 and 	e.dt_cancelamento 	is null
	 and 	qt_regra_evento_w > 0
         and 	nvl(e.dt_alta,to_date('2999','yyyy')) > dt_parametro_p;
			
begin

-- Evento Gera��o di�ria de itens por atendimento
select	count(*)
into	qt_regra_evento_w
from 	regra_lanc_automatico
where 	nr_seq_evento = 544
and 	ie_situacao = 'A';

OPEN C00;
LOOP
FETCH C00 into 
	nr_atendimento_w;
	exit when c00%notfound;	
	BEGIN	
	
	Gerar_lancamento_automatico(nr_atendimento_w, null, 544, nm_usuario_p, null, null,null,null,null,null);
	
	END;
END LOOP;
CLOSE C00;
	
commit;

end Geracao_Diaria_Itens_Atend;
/