CREATE OR REPLACE PROCEDURE CONSISTE_CLASSIF_RELATORIO(	cd_relatorio_p		number,
							ds_consistencia_p	out varchar2)
IS

qt_relatorios_w		number(10,0);
ie_base_wheb_w		number(1,0);
ds_sep_bv_w		varchar2(10);

BEGIN
select	obter_separador_bv
into	ds_sep_bv_w
from	dual;

obter_valor_dinamico_bv('select 1 from estabelecimento where cd_cgc = :cd_cgc and cd_estabelecimento = :cd_estabelecimento and ie_situacao = :ie_situacao',
	'cd_cgc=01950338000177'||ds_sep_bv_w||'cd_estabelecimento=1'||ds_sep_bv_w||'ie_situacao=A',ie_base_wheb_w);

Select	count(*)
into	qt_relatorios_w
from	relatorio
where	cd_relatorio = cd_relatorio_p
and	(((ie_base_wheb_w = 1) and cd_classif_relat like 'W%')
or	(ie_base_wheb_w = 0));

if	(qt_relatorios_w > 1) then
	if	(ie_base_wheb_w = 1) then
		ds_consistencia_p	:= Wheb_mensagem_pck.get_texto(306282); -- 'J� existe um relat�rio de classifica��o W cadastrado';
	else                    		-- 'J� existe um relat�rio com este c�digo cadastrado.'   
		ds_consistencia_p	:= Wheb_mensagem_pck.get_texto(306284)|| chr(10) || Wheb_mensagem_pck.get_texto(306285); -- 'A libera��o de relat�rios duplicados para mesma fun��o pode dificultar a impress�o!'; 
	end if;
end if;

END CONSISTE_CLASSIF_RELATORIO;
/