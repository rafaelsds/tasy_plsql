create or replace
procedure obter_prox_medicao_prot_glic	(cd_intervalo_p		varchar2,
					dt_prev_ant_p			date,
					dt_prev_controle_p		date,
					dt_controle_atual_p		date,
					dt_proximo_controle_p	out	date,
					qt_min_proximo_p 	out	number) is

ie_operacao_w			varchar2(1);
qt_operacao_w			intervalo_prescricao.qt_operacao%type;
ds_horarios_w			varchar2(255);
ds_horarios_ww		varchar2(255);
ds_horarios_www		varchar2(255);
ds_horario_w			varchar2(7);
i				integer;
dt_horario_w			date;
dt_proximo_controle_w	date;
qt_min_proximo_w		number(4,0);

begin
/* obter dados intervalo */
select	max(ie_operacao),
	max(qt_operacao),
	max(ds_horarios)
into	ie_operacao_w,
	qt_operacao_w,
	ds_horarios_w
from	intervalo_prescricao
where	cd_intervalo = cd_intervalo_p;

/* calcular proximo controle */
if	(ie_operacao_w = 'H') and
	(qt_operacao_w is not null) then
	dt_proximo_controle_w	:= dt_controle_atual_p + qt_operacao_w / 24;
	qt_min_proximo_w		:= (qt_operacao_w / 24) * 1440;

elsif	(ie_operacao_w = 'X') and
	(qt_operacao_w is not null) then
	dt_proximo_controle_w	:= dt_controle_atual_p + (24 / qt_operacao_w) / 24;
	qt_min_proximo_w		:= ((24 / qt_operacao_w) / 24) * 1440;

elsif	(ie_operacao_w in ('F','V')) and
	(ds_horarios_w is not null) then
	if	(dt_prev_controle_p is not null) and
		(dt_controle_atual_p >= dt_prev_controle_p - 30/1440) and
		(instr(ds_horarios_w, to_char(dt_prev_controle_p,'hh24')) > 0) then
		ds_horarios_ww := replace(substr(ds_horarios_w, instr(ds_horarios_w, to_char(dt_prev_controle_p,'hh24'))+3, length(ds_horarios_w)), to_char(dt_prev_controle_p,'hh24'), '');
	elsif	(dt_prev_controle_p is not null) and
		(dt_prev_ant_p is not null) and
		(dt_controle_atual_p >= dt_prev_ant_p - 30/1440) and
		(instr(ds_horarios_w, to_char(dt_prev_ant_p,'hh24')) > 0) then
		ds_horarios_ww := replace(substr(ds_horarios_w, instr(ds_horarios_w, to_char(dt_prev_ant_p,'hh24'))+3, length(ds_horarios_w)), to_char(dt_prev_ant_p,'hh24'), '');
	end if;

	if	(ds_horarios_ww is not null) then
		ds_horarios_w := ds_horarios_ww;
	end if;

	/*
	select	padroniza_horario_prescr(ds_horarios_w, null) || ' '
	into	ds_horarios_www
	from	dual;
	*/

	ds_horarios_www := ds_horarios_w || ' ';

	while (ds_horarios_www is not null) loop
		begin
		select	instr(ds_horarios_www, ' ') 
		into	i
		from 	dual;

		if	(i > 1) and
			(substr(ds_horarios_www, 1, i-1) is not null) then
			ds_horario_w		:= substr(ds_horarios_www, 1, i-1);
			ds_horario_w		:= replace(ds_horario_w, ' ', '');
			ds_horarios_www	:= substr(ds_horarios_www, i+1, length(ds_horarios_www));

			/*
			if	(instr(ds_horario_w, 'A') > 0) then
				ds_horario_w := replace(ds_horario_w, 'A', '');
				dt_horario_w := to_date(to_char(dt_controle_atual_p+1,'dd/mm/yyyy') || ' ' || ds_horario_w || ':00', 'dd/mm/yyyy hh24:mi:ss');
			else
				dt_horario_w := to_date(to_char(dt_controle_atual_p,'dd/mm/yyyy') || ' ' || ds_horario_w || ':00', 'dd/mm/yyyy hh24:mi:ss');
			end if;
			*/

			if	(ds_horario_w < to_char(dt_controle_atual_p,'hh24')) then
				dt_horario_w := to_date(to_char(dt_controle_atual_p+1,'dd/mm/yyyy') || ' ' || ds_horario_w || ':00', 'dd/mm/yyyy hh24:mi:ss');
			else
				begin
					dt_horario_w := to_date(to_char(dt_controle_atual_p,'dd/mm/yyyy') || ' ' || ds_horario_w || ':00', 'dd/mm/yyyy hh24:mi:ss');
				exception when others then
					dt_horario_w := to_date(to_char(dt_controle_atual_p,'dd/mm/yyyy') || ' ' || to_char(sysdate,'hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss');
				end;
			end if;

			if	(dt_horario_w is not null) and
				(dt_horario_w > dt_controle_atual_p) and
				((dt_horario_w >= dt_prev_controle_p) or
				(dt_prev_controle_p is null)) and
				(dt_proximo_controle_w is null) then
				dt_proximo_controle_w	:= dt_horario_w;
				ds_horarios_www		:= null;
			end if;
		else
			ds_horarios_www := null;
		end if;
		end;
	end loop;
end if;

dt_proximo_controle_p	:= dt_proximo_controle_w;
qt_min_proximo_p		:= qt_min_proximo_w;

end obter_prox_medicao_prot_glic;
/
