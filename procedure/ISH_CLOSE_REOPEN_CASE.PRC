create or replace
procedure ish_close_reopen_case(		
		ish_caseno_p		varchar2,
		ie_close_reopen_p		varchar2) is 

episodio_paciente_w	episodio_paciente%rowtype;
begin

begin

begin
select	*
into	episodio_paciente_w
from	episodio_paciente
where	nr_episodio = ish_caseno_p;
exception
when others then
	episodio_paciente_w.nr_sequencia := null;
end;

if	(episodio_paciente_w.nr_sequencia is not null) then
	if	(ie_close_reopen_p = 'C') and (episodio_paciente_w.dt_fim_episodio is null) then
		encerrar_episodio(episodio_paciente_w.nr_sequencia);
	elsif	(ie_close_reopen_p = 'R') and (episodio_paciente_w.dt_fim_episodio is not null) then
		reopen_case(episodio_paciente_w.nr_sequencia);
	end if;
end if;
exception
when others then
	null;
end;

end ish_close_reopen_case;
/