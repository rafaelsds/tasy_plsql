CREATE OR REPLACE 
PROCEDURE TIE_HL7_INSERE_LAUDO_PDF(NR_SEQ_LAUDO_P	    IN      laudo_paciente_pdf_serial.nr_seq_laudo%TYPE,
								  NR_ACESSO_DICOM_P	    IN      laudo_paciente_pdf_serial.nr_acesso_dicom%TYPE,
								  DS_LAUDO_PDF_P	    IN      laudo_paciente_pdf_serial.ds_pdf_serial%TYPE,
								  NM_USUARIO_P	        IN      laudo_paciente_pdf_serial.nm_usuario%TYPE,
								  DS_ERRO_P				OUT     VARCHAR2,
								  NR_SEQ_LAUDO_PDF_P	Out     NUMBER) IS

nr_seq_laudo_pdf_w		laudo_paciente_pdf_serial.nr_sequencia%TYPE;
nr_prescricao_w         prescr_procedimento.nr_prescricao%TYPE;
nr_seq_prescr_w         prescr_procedimento.nr_sequencia%TYPE;
								  
BEGIN
								  
IF (nr_seq_laudo_p IS NOT NULL)	THEN
	
	SELECT 	laudo_paciente_pdf_serial_seq.NEXTVAL
	INTO	nr_seq_laudo_pdf_w
	FROM 	dual;
	
	BEGIN
	
	INSERT INTO laudo_paciente_pdf_serial (nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_acesso_dicom,
									nr_seq_laudo,
									ds_pdf_serial)
							VALUES (nr_seq_laudo_pdf_w,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_acesso_dicom_p,
									nr_seq_laudo_p,
									ds_laudo_pdf_p);
									

	--Atualizando o formato do resultado do laudo para buscar o laudo  em pdf serializado.									
	UPDATE	laudo_paciente
	SET		ie_formato = 4
	WHERE	nr_sequencia = nr_seq_laudo_p;
    
    SELECT nr_prescricao, nr_sequencia
    INTO nr_prescricao_w, nr_seq_prescr_w
    FROM prescr_procedimento
    WHERE nr_acesso_dicom = nr_acesso_dicom_p;
    
    gravar_auditoria_mmed(nr_prescricao_w,nr_seq_prescr_w,'VUEPACS',30,ds_laudo_pdf_p);
		
	EXCEPTION
		WHEN OTHERS THEN
			ds_erro_p := substr(sqlerrm,1,4000);
		
	END;

END IF;
		
COMMIT;								  

nr_seq_laudo_pdf_p := nr_seq_laudo_pdf_w;

END TIE_HL7_INSERE_LAUDO_PDF;
/