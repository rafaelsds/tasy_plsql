create or replace
procedure pcs_gerar_nao_conformidade(
				nr_seq_registro_p		number,				
				cd_material_p			number,
				nr_seq_tipo_p			number,
				cd_estabelecimento_p	number,
				cd_setor_atendimento_p	number,
				cd_pf_abertura_p		varchar2,
				dt_nao_conformidade_p	date default sysdate,
				nm_usuario_p			varchar2,
				ie_dano_paciente_p	varchar2 default 'N') as
				

ds_nao_conformidade_w			varchar2(4000);
ds_historico_w			varchar2(4000);
nr_sequencia_w			number(10);
nm_pessoa_abertura_w		varchar2(255);
dt_abertura_w			date;
nr_seq_indicador_w			number(10) := null;
cd_setor_resp_w			number(15) := null;
cd_setor_eficacia_w		number(15) := null;
cd_pf_eficacia_w			varchar2(10) := null;
cd_pf_resp_ocorrencia_w		varchar2(10) := null;
qr_dia_analise_eficacia_w		number(15) := null;
ie_carrega_pf_eficacia_w		varchar2(2);
ds_disposicao_imediata_w		varchar2(4000);
begin
dt_abertura_w := nvl(dt_nao_conformidade_p, sysdate);
--PCS - N�o conformidade gerada a partir do registro de an�lise n�mero #@NR_REGISTRO#@.
ds_nao_conformidade_w :=  substr(wheb_mensagem_pck.get_texto(316401,'NR_REGISTRO='|| nr_seq_registro_p||';CD_MATERIAL='||cd_material_p),1,255);

select	qua_nao_conformidade_seq.nextval
into	nr_sequencia_w
from	dual;


insert	into qua_nao_conformidade(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_abertura,
		cd_pf_abertura,
		ds_nao_conformidade,
		cd_setor_atendimento,
		nm_usuario_origem,
		ie_status,
		nr_seq_tipo,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pf_resp_eficacia,
		cd_pf_resp_ocorrencia,
		ie_dano_paciente,
		ds_disposicao_imediata,
		nr_seq_registro_pcs,
		cd_material)
values(		nr_sequencia_w,
		cd_estabelecimento_p,
		dt_abertura_w,
		nm_usuario_p,
		dt_abertura_w,
		cd_pf_abertura_p,
		ds_nao_conformidade_w,
		cd_setor_atendimento_p,
		nm_usuario_p,
		WHEB_MENSAGEM_PCK.get_texto(799493),
		nr_seq_tipo_p,
		dt_abertura_w,
		nm_usuario_p,
		cd_pf_eficacia_w,
		cd_pf_resp_ocorrencia_w,
		ie_dano_paciente_p,
		ds_disposicao_imediata_w,
		nr_seq_registro_p,
		cd_material_p);
		
select	substr(obter_nome_pf(cd_pf_abertura_p),1,255)
into	nm_pessoa_abertura_w
from	dual;

ds_historico_w	:= substr(wheb_mensagem_pck.get_texto(316401,'NR_SEQUENCIA='|| nr_sequencia_w ||';NM_PESSOA_ABERTURA_W' || NM_PESSOA_ABERTURA_W || 'DT_ABERTURA_W' || to_char(dt_abertura_w,'dd/mm/yyyy hh24:mm:ss')),1,4000);

insert into qua_evento_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
values	(	qua_evento_hist_seq.nextval,
		sysdate,
		nm_usuario_p,
		ds_historico_w,
		sysdate,
		nm_usuario_p);

commit;

end  pcs_gerar_nao_conformidade;
/