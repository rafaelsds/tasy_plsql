create or replace
procedure cpoe_obter_recon_redil_prot(
						nr_seq_diluicao_p					number,
						cd_protocolo_p						number,
						nr_sequencia_p						number,
						cd_estabelecimento_p				number,
						cd_material_p						number,
						cd_unidade_medida					varchar2,
						qt_dose_p							number,
						qt_unitaria_p						number,
						ie_se_necessario_p					varchar2,
						ie_acm_p							varchar2,
						qt_solucao_dil_p				out number,
						cd_mat_dil_p					out number,
						qt_dose_dil_p					out number,
						cd_unid_med_dose_dil_p			out varchar2,
						qt_solucao_red_p				out number,
						qt_dose_red_p					out number,
						cd_mat_red_p					out number,
						cd_unid_med_dose_red_p			out varchar2,
						cd_mat_recons_p					out number,
						cd_unid_med_dose_recons_p		out varchar2,
						qt_dose_recons_p				out number,
						qt_minuto_aplicacao_p			out number,
						ds_dose_diferenciada_dil_p      out varchar2,
						ds_dose_diferenciada_rec_p      out varchar2,
						ds_dose_diferenciada_red_p      out varchar2) is
						
cd_material_w						protocolo_medic_material.cd_material%type;
nr_seq_material_w					protocolo_medic_material.nr_seq_material%type;
cd_unidade_medida_w					protocolo_medic_material.cd_unidade_medida%type;
qt_dose_w							protocolo_medic_material.qt_dose%type;
ie_agrupador_w						protocolo_medic_material.ie_agrupador%type;
qt_minuto_aplicacao_w				protocolo_medic_material.qt_minuto_aplicacao%type;
qt_solucao_dil_w					cpoe_material.qt_solucao_dil%type;
cd_mat_dil_w						cpoe_material.cd_mat_dil%type;
qt_dose_dil_w						cpoe_material.qt_dose_dil%type;
cd_unid_med_dose_dil_w				cpoe_material.cd_unid_med_dose_dil%type;
qt_solucao_red_w					cpoe_material.qt_solucao_red%type;
qt_dose_red_w						cpoe_material.qt_dose_red%type;
cd_mat_red_w						cpoe_material.cd_mat_red%type;
cd_unid_med_dose_red_w				cpoe_material.cd_unid_med_dose_red%type;
cd_mat_recons_w						cpoe_material.cd_mat_recons%type;
cd_unid_med_dose_recons_w			cpoe_material.cd_unid_med_dose_recons%type;
qt_dose_recons_w					cpoe_material.qt_dose_recons%type;
qt_solucao_w						cpoe_material.qt_solucao_dil%type;
ds_dose_diferenciada_w				protocolo_medic_material.ds_dose_diferenciada%type;
ds_dose_diferenciada_dil_w			protocolo_medic_material.ds_dose_diferenciada%type;
ds_dose_diferenciada_red_w			protocolo_medic_material.ds_dose_diferenciada%type;
ds_dose_diferenciada_rec_w			protocolo_medic_material.ds_dose_diferenciada%type;																			 
cursor c01 is
select	a.cd_material,
		a.cd_unidade_medida,
		a.qt_dose,
		decode(b.ie_tipo_material, 1, 2, a.ie_agrupador) ie_agrupador,
		a.qt_solucao,
		qt_minuto_aplicacao,
		a.ds_dose_diferenciada						  
from 	material b,
		protocolo_medic_material a
where	a.cd_protocolo = cd_protocolo_p
and		a.nr_sequencia = nr_sequencia_p
and		a.cd_material = b.cd_material
and		decode(b.ie_tipo_material, 1, 2, a.ie_agrupador) in (3,7,9)
and		a.nr_seq_diluicao	= nr_seq_diluicao_p		
order by 
		nr_seq_material;
		
begin



open c01;
loop
fetch c01 into	cd_material_w,
				cd_unidade_medida_w,
				qt_dose_w,
				ie_agrupador_w,
				qt_solucao_w,
				qt_minuto_aplicacao_w,
				ds_dose_diferenciada_w;						   
exit when c01%notfound;
	begin
	
	if (ie_agrupador_w = 3) then
		qt_solucao_dil_w := qt_solucao_w;
		cd_mat_dil_w := cd_material_w;
		qt_dose_dil_w := qt_dose_w;
		cd_unid_med_dose_dil_w := cd_unidade_medida_w;
		qt_minuto_aplicacao_p := qt_minuto_aplicacao_w;		
		ds_dose_diferenciada_dil_w := ds_dose_diferenciada_w;											   
	elsif (ie_agrupador_w = 7) then
		qt_solucao_red_w := qt_solucao_w;
		qt_dose_red_w := qt_dose_w;
		cd_mat_red_w := cd_material_w;
		cd_unid_med_dose_red_w := cd_unidade_medida_w;
		ds_dose_diferenciada_red_w := ds_dose_diferenciada_w;											   
	elsif (ie_agrupador_w = 9) then
		cd_mat_recons_w := cd_material_w;
		cd_unid_med_dose_recons_w := cd_unidade_medida_w;
		qt_dose_recons_w := qt_dose_w;
		ds_dose_diferenciada_rec_w := ds_dose_diferenciada_w;											   
	end if;
	end;
end loop;
close c01;

qt_solucao_dil_p := qt_solucao_dil_w;
cd_mat_dil_p := cd_mat_dil_w;
qt_dose_dil_p := qt_dose_dil_w;
cd_unid_med_dose_dil_p := cd_unid_med_dose_dil_w;
qt_solucao_red_p := qt_solucao_red_w;
qt_dose_red_p := qt_dose_red_w;
cd_mat_red_p := cd_mat_red_w;
cd_unid_med_dose_red_p := cd_unid_med_dose_red_w;
cd_mat_recons_p := cd_mat_recons_w;
cd_unid_med_dose_recons_p := cd_unid_med_dose_recons_w;
qt_dose_recons_p := qt_dose_recons_w;
ds_dose_diferenciada_dil_p := ds_dose_diferenciada_dil_w;
ds_dose_diferenciada_red_p := ds_dose_diferenciada_red_w;
ds_dose_diferenciada_rec_p := ds_dose_diferenciada_rec_w;													 

end cpoe_obter_recon_redil_prot;
/
