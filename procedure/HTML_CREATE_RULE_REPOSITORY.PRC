create or replace
procedure	HTML_CREATE_RULE_REPOSITORY(
				nr_seq_lote_p				number,
				nr_seq_regra_acao_p			number,
				nm_usuario_p				varchar2) is

nr_seq_parametro_w			funcao_parametro.nr_sequencia%type;
cd_funcao_w					funcao.cd_funcao%type;

nm_repositorio_w			file_repository.nm_repository%type;
nm_atributo_w				tabela_atributo.nm_atributo%type;
nm_tabela_w					tabela_sistema.nm_tabela%type;

nr_seq_rule_w				html_param_to_rule.nr_sequencia%type;	
nr_seq_obj_schematic_w		objeto_schematic.nr_sequencia%type;
	
vl_parametro_w				funcao_parametro.vl_parametro%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_perfil_w					perfil.cd_perfil%type;
nm_usuario_param_w			usuario.nm_usuario%type;

cursor	c10 is
select	cd_estabelecimento,
		vl_parametro
from	funcao_param_estab
where	cd_funcao = cd_funcao_w
and		nr_seq_param = nr_seq_parametro_w
and		vl_parametro is not null;

cursor	c11 is
select	cd_estabelecimento,
		cd_perfil,
		vl_parametro
from	funcao_param_perfil
where	cd_funcao = cd_funcao_w
and		nr_sequencia = nr_seq_parametro_w
and		vl_parametro is not null;

cursor	c12 is
select	cd_estabelecimento,
		nm_usuario_param,
		vl_parametro
from	funcao_param_usuario
where	cd_funcao = cd_funcao_w
and		nr_sequencia = nr_seq_parametro_w
and		vl_parametro is not null;

begin

select	nr_seq_obj_schematic,
		coalesce(nm_atributo,'X'),
		nr_seq_rule
into	nr_seq_obj_schematic_w,
		nm_atributo_w,
		nr_seq_rule_w
from	html_param_to_rule_action
where	nr_sequencia = nr_seq_regra_acao_p;

select	cd_funcao,
		nr_seq_parametro,
		substr(obter_desc_funcao(cd_funcao) || ' (' ||obter_desc_expressao(295274,'Par�metro') || ' ' || to_char(nr_seq_parametro) || ')',1,255)
into	cd_funcao_w,
		nr_seq_parametro_w,
		nm_repositorio_w
from	html_param_to_rule
where	nr_sequencia = nr_seq_rule_w;

select	coalesce(nm_tabela,'X')
into	nm_tabela_w
from	objeto_schematic
where	nr_sequencia = nr_seq_obj_schematic_w;

select	coalesce(vl_parametro,'X')
into	vl_parametro_w
from	funcao_parametro
where	cd_funcao = cd_funcao_w
and		nr_sequencia = nr_seq_parametro_w;

/* INSERIR REGRA GERAL */
if	(vl_parametro_w <> 'X') and
	(nm_tabela_w <> 'X') and
	(nm_atributo_w <> 'X') then
	INSERT_RULE_REPOSITORY(
				nr_seq_lote_p,
				cd_funcao_w,
				nr_seq_parametro_w,
				vl_parametro_w,
				null,
				null,
				null,
				nm_repositorio_w,
				nm_atributo_w,
				nm_tabela_w,
				nm_usuario_p);
end if;

/* INSERIR REGRA POR ESTABELECIMENTO*/
open c10;
loop
fetch c10 into
		cd_estabelecimento_w,
		vl_parametro_w;
exit when c10%notfound;
		begin
		
		INSERT_RULE_REPOSITORY(
				nr_seq_lote_p,
				cd_funcao_w,
				nr_seq_parametro_w,
				vl_parametro_w,
				cd_estabelecimento_w,
				null,
				null,
				nm_repositorio_w,
				nm_atributo_w,
				nm_tabela_w,
				nm_usuario_p);
				
		end;
end loop;
close c10;

/* INSERIR REGRA POR PERFIL*/
open c11;
loop
fetch c11 into
		cd_estabelecimento_w,
		cd_perfil_w,
		vl_parametro_w;
exit when c11%notfound;
		begin
		
		INSERT_RULE_REPOSITORY(
				nr_seq_lote_p,
				cd_funcao_w,
				nr_seq_parametro_w,
				vl_parametro_w,
				cd_estabelecimento_w,
				cd_perfil_w,
				null,
				nm_repositorio_w,
				nm_atributo_w,
				nm_tabela_w,
				nm_usuario_p);
				
		end;
end loop;
close c11;

/* INSERIR REGRA POR USUARIO*/
open c12;
loop
fetch c12 into
		cd_estabelecimento_w,
		nm_usuario_param_w,
		vl_parametro_w;
exit when c12%notfound;
		begin
		
		INSERT_RULE_REPOSITORY(
				nr_seq_lote_p,
				cd_funcao_w,
				nr_seq_parametro_w,
				vl_parametro_w,
				cd_estabelecimento_w,
				null,
				nm_usuario_param_w,
				nm_repositorio_w,
				nm_atributo_w,
				nm_tabela_w,
				nm_usuario_p);
				
		end;
end loop;
close c12;

commit;

end HTML_CREATE_RULE_REPOSITORY;
/