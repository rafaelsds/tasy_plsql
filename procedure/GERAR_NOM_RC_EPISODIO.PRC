create or replace
procedure gerar_nom_rc_episodio(nr_seq_cabecalho_p		number,
								nm_usuario_p			varchar2) is
								

nr_atendimento_w					atendimento_paciente.nr_atendimento%type;
nr_seq_episodio_w					episodio_paciente.nr_sequencia%type;
cd_estabelecimento_w				estabelecimento.cd_estabelecimento%type;
cd_pessoa_fisica_w					pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_pessoa_endereco_w			compl_pessoa_fisica.nr_seq_pessoa_endereco%type;
cd_cgc_w							pessoa_juridica.cd_cgc%type;
nm_oid_sistema_w				    varchar2(255);
ds_tipo_w		          	        varchar2(255);						
ds_oid_episodio_w					varchar2(30);	/*id_182*/			
nr_sequencia_episodio_w				episodio_paciente.nr_sequencia%type;	/*id_183*/
ie_tipo_episodio_w					nom_rc_episodio.cd_tipo_episodio%type;	/*id_184*/
dt_inicio_episodio_w				episodio_paciente.dt_episodio%type;			/*id_186*/								
dt_fim_episodio_w					episodio_paciente.dt_fim_episodio%type;			/*id_187*/		
ds_tipo_episodio_w					tipo_episodio.ds_tipo%type;		
nr_cod_prof_resp_legal_w			varchar2(30);	/*id_188*/
nm_prim_nome_medico_w				varchar2(255);	/*id_189*/
nm_sobren_medico_2_pai_w			varchar2(255);	/*id_190*/
nm_sobren_medico_2_mae_w			varchar2(255);	/*id_191*/
cd_clues_estab_w					varchar2(30);	/*id_192*/
nr_licenca_sanitaria_w				varchar2(30);	/*id_193*/
nm_estab_salud_w					pessoa_juridica.ds_razao_social%type;	/*id_194*/	
nr_telefone_w						varchar2(30);	/*id_195*/
ds_email_w							varchar2(255);	/*id_196*/
ds_domicilio_w						varchar2(2000);	/*id_197*/
cd_tipo_vialidade_w					varchar2(10);	/*id_198*/								
nm_vialidade_w						varchar2(255);	/*id_199*/
nr_domicilio_ext_w					varchar2(255);	/*id_200*/
nr_domicilio_ext_alfa_w				varchar2(255);	/*id_201*/
nr_domicilio_int_w					varchar2(255);	/*id_202*/
nr_domicilio_int_alfa_w				varchar2(255);	/*id_203*/								
cd_tipo_assentamento_w				varchar2(10);	/*id_204*/	
nm_assentamento_w					varchar2(255);	/*id_205*/
cd_localidade_w						varchar2(10);	/*id_206*/
cd_municipio_w						varchar2(10);	/*id_207*/
cd_entidade_fed_w					varchar2(10);	/*id_208*/
cd_postal_w							varchar2(10);	/*id_209*/
cd_pais_w							varchar2(10);	/*id_210*/	

nr_seq_catalogo_w		end_catalogo.nr_sequencia%type;

begin

delete nom_rc_episodio where nr_seq_cabecalho = nr_seq_cabecalho_p;

select	a.nr_atendimento,
		a.nr_seq_episodio,
		a.cd_estabelecimento
into	nr_atendimento_w,
		nr_seq_episodio_w,
		cd_estabelecimento_w
from	nom_rc_cabecalho a
where	a.nr_sequencia	= nr_seq_cabecalho_p;

if	((nr_seq_episodio_w is null) and (nr_atendimento_w is not null)) then
	select	max(nr_seq_episodio)
	into	nr_seq_episodio_w
	from	atendimento_paciente a
	where	a.nr_atendimento = nr_atendimento_w;
else 
	select	min(nr_atendimento)
	into	nr_atendimento_w
	from	atendimento_paciente
	where	nr_seq_episodio = nr_seq_episodio_w;
end if;

if (nr_atendimento_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1071390);
end if;

if	(nr_seq_episodio_w is not null) then

	select	a.nr_sequencia, /*id_183*/
			decode(b.ie_tipo, 	1,'IMP',
								3,'EMER',
								7,'FLD',
								8,'AMB',
								10,'ACUTE',
								11,'HH',
								12,'SS',
								14,'VR'), /*id_184*/
			b.ds_tipo,
			a.dt_episodio, /*id_186*/
			a.dt_fim_episodio	/*id_191*/
	into	nr_sequencia_episodio_w, /*183*/
			ie_tipo_episodio_w, /*id_184*/
			ds_tipo_episodio_w,
			dt_inicio_episodio_w,		/*186*/
			dt_fim_episodio_w			/*187*/
	from	episodio_paciente a,
			tipo_episodio b
	where 	a.nr_sequencia =	nr_seq_episodio_w
	and		a.nr_seq_tipo_episodio (+) = b.nr_sequencia;

	select	substr(obter_crm_medico(m.cd_pessoa_fisica), 1, 255) ,
			z.ds_given_name nm_primeiro_nome, 
			z.ds_family_name nm_sobrenome_pai,
			nvl(z.ds_component_name_1, 'SIN INFORMACION') nm_sobrenome_mae	
	into	nr_cod_prof_resp_legal_w,
			nm_prim_nome_medico_w,		/*189*/
			nm_sobren_medico_2_pai_w,	/*190*/
			nm_sobren_medico_2_mae_w	/*191*/
	from	medico m,
			pessoa_fisica p,
			person_name z,
			atendimento_paciente a
	where	m.cd_pessoa_fisica = p.cd_pessoa_fisica
	and 	p.nr_seq_person_name = z.nr_sequencia(+)
	and		z.ds_type(+) = 'main'
	and		a.cd_medico_resp = p.cd_pessoa_fisica
	and		a.nr_atendimento = nr_atendimento_w;


	select 	pj.cd_cgc cd_cgc,
			pj.ds_razao_social ds_razao_social,				/*id_194*/
			pj.cd_internacional cd_internacional,			/*id_192*/
			pj.nr_alvara_sanitario nr_alvara_sanitario,		/*id_193*/
			obter_nom_rc_telefone(null, pj.cd_cgc, null) nr_telefone,		/*id_195*/
			nvl(pe.ds_email,pj.ds_email) ds_email	/*id_196*/
	into	cd_cgc_w,
			nm_estab_salud_w,
			cd_clues_estab_w,/*id_192*/
			nr_licenca_sanitaria_w,	/*id_193*/
			nr_telefone_w,
			ds_email_w
	from 	pessoa_juridica_estab pe,
			pessoa_juridica pj,
			estabelecimento e,
			atendimento_paciente ap
	where 	e.cd_cgc = pj.cd_cgc 
	and 	ap.cd_estabelecimento = e.cd_estabelecimento
	and		e.cd_estabelecimento = pe.cd_estabelecimento
	and		pe.cd_cgc	= pj.cd_cgc
	and		ap.nr_atendimento = nr_atendimento_w;

	if (cd_cgc_w is not null) then

		/* Endereco */
		select	nr_seq_pessoa_endereco,	
				get_complete_address_desc(a.nr_seq_pessoa_endereco,null,null,null,null,'Y') ds_endereco_completo, /* id_197 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'TIPO_LOGRAD','C') cd_tipo_vialidade, /* id_198 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'RUA_VIALIDADE','D') ds_rua_vialidade, /* id_199 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'NUMERO','D') nr_numero_externo,	/* id_200 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'NUM_EXT_ALFA','D') nr_numero_externo_alfa,
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'NUMERO_INT','D') nr_numero_interno, /* id_203 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'COMPLEMENTO','D') nr_numero_interno_alfa,
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'TIPO_BAIRRO','C') cd_tipo_assentamento, /* id_204 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'BAIRRO_VILA','CD') nm_assentamento, /* id_205 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'LOCALIDADE_AREA','C') cd_localidade, /* 206 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'MUNICIPIO','C') cd_municipio, /* id_207 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'ESTADO_PROVINCI','C') cd_entidade, /* id_208 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'CODIGO_POSTAL','D') cd_postal,  /* id_209 */
				get_info_end_endereco(a.nr_seq_pessoa_endereco,'PAIS','C') cd_pais /* id_210 */
		into	nr_seq_pessoa_endereco_w,
				ds_domicilio_w,	
				cd_tipo_vialidade_w,
				nm_vialidade_w,
				nr_domicilio_ext_w,
				nr_domicilio_ext_alfa_w,
				nr_domicilio_int_w,
				nr_domicilio_int_alfa_w,
				cd_tipo_assentamento_w, 
				nm_assentamento_w,
				cd_localidade_w,
				cd_municipio_w,
				cd_entidade_fed_w,
				cd_postal_w,
				cd_pais_w
		from	pessoa_juridica a
		where	a.cd_cgc = cd_cgc_w;

		select	max(b.nr_seq_catalogo)
		into	nr_seq_catalogo_w
		from	end_endereco b,
				pessoa_endereco_item a
		where	b.nr_sequencia = a.nr_seq_end_endereco
		and		a.nr_seq_pessoa_endereco = nr_seq_pessoa_endereco_w;

	else	
		nr_telefone_w				:=	null;
		ds_email_w					:=	null;
		ds_domicilio_w				:=	null;
		cd_tipo_vialidade_w			:=	null;
		nm_vialidade_w				:=	null;
		nr_domicilio_ext_w			:=	null;
		nr_domicilio_ext_alfa_w		:=	null;
		nr_domicilio_int_w			:=	null;
		nr_domicilio_int_alfa_w		:=	null;
		cd_tipo_assentamento_w		:=	null;
		nm_assentamento_w			:=	null;
		cd_localidade_w				:=	null;
		cd_municipio_w				:=	null;
		cd_entidade_fed_w			:=	null;
		cd_postal_w					:=	null;
		cd_pais_w					:=	null;

	end if;

	if (ie_tipo_episodio_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(1071091, 'DS_TIPO=' || ds_tipo_episodio_w);
		--Nao foi definido o tipo de atendimento para o tipo de episodio "#@DS_TIPO#@". Verifique em Cadastros Gerais/Aplicacao Principal/Episodio (Gestao de casos)/Tipo de episodio.
	end if;

	insert into nom_rc_episodio(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_cabecalho,
			ds_oid_episodio,			
			nr_ident_episodio,		
			cd_tipo_episodio,			
			dt_inicio_episodio,		
			dt_fim_episodio,			
			nr_cod_prof_resp_legal,	
			nm_prim_nome_medico,		
			nm_sobren_medico_2_pai,	
			nm_sobren_medico_2_mae,	
			cd_clues_estab,			
			nr_licenca_sanitaria,		
			nm_estab_salud,			
			nr_telefone,
			ds_email,
			ds_domicilio,
			cd_tipo_vialidade,
			nm_vialidade,
			nr_domicilio_ext,
			nr_domicilio_ext_alfa,
			nr_domicilio_int,
			nr_domicilio_int_alfa,
			cd_tipo_assentamento,
			nm_assentamento,
			cd_localidade,
			cd_municipio,
			cd_entidade_fed,
			cd_postal,
			cd_pais,
			nr_seq_catalogo) 
	values (
			nom_rc_episodio_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_cabecalho_p,
			get_oid_details(11,'OID_NUMBER', 'NOM',cd_estabelecimento_w),						
			nr_sequencia_episodio_w,				
			ie_tipo_episodio_w,					
			dt_inicio_episodio_w,										
			dt_fim_episodio_w,											
			nr_cod_prof_resp_legal_w,			
			nm_prim_nome_medico_w,				
			nm_sobren_medico_2_pai_w,			
			nm_sobren_medico_2_mae_w,			
			cd_clues_estab_w,					
			nr_licenca_sanitaria_w,			
			nm_estab_salud_w,					
			nr_telefone_w,	
			ds_email_w,		
			ds_domicilio_w,
			cd_tipo_vialidade_w,
			nm_vialidade_w,
			nr_domicilio_ext_w,
			nr_domicilio_ext_alfa_w,
			nr_domicilio_int_w,
			nr_domicilio_int_alfa_w,
			cd_tipo_assentamento_w,
			nm_assentamento_w,
			cd_localidade_w,
			cd_municipio_w,
			cd_entidade_fed_w,
			cd_postal_w,
			cd_pais_w,
			nr_seq_catalogo_w);
end if;

end gerar_nom_rc_episodio;
/
