create or replace
procedure gerar_ajuste_disp_consig(	nr_cirurgia_p		number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is

cd_material_w		number(6,0);
cd_material_estoque_w	number(6,0);
cd_unidade_medida_w	varchar2(30);
qt_dispensacao_w	number(15,3);
nr_prescricao_w		number(14,0);
nr_prescricao_esp_w	number(14,0);
cd_estabelecimento_w	number(4,0);
ie_gerar_espec_w	varchar2(15);
cd_cgc_fornec_w		varchar2(14);
cd_fornec_consignado_w	varchar2(14);
cd_pessoa_fisica_w	varchar2(10);
nr_atendimento_w	number(10,0);
cd_medico_cirurgiao_w	varchar2(10);
nr_seq_lote_fornec_w	number(10,0);
nr_seq_lote_fornec_ww	number(10,0);
cd_local_estoque_w	varchar2(4) := null;
dt_inicio_real_w	date;
ie_atualiza_dt_inicio_real_w varchar2(1);
dt_prescricao_w		date;
ie_ajusta_disp_w	varchar2(15);
cd_setor_atendimento_w	number(5);

cursor C01 is
	select	a.cd_material,
		a.cd_unidade_medida,
		sum(qt_dispensacao),
		c.cd_material_estoque,
		obter_dados_lote_fornec(nr_seq_lote_fornec,'CF'),
		nr_seq_lote_fornec
	from    material c,
		cirurgia_agente_disp a,
		cirurgia b
	where	a.nr_cirurgia   = b.nr_cirurgia
	and     a.cd_material   = c.cd_material
	and	a.nr_cirurgia	= nr_cirurgia_p
	and	a.ie_operacao	= 'D'
	and	(ie_ajusta_disp_w <> 'R')
	and	c.ie_consignado in (1)
	and	((ie_gerar_espec_w <> 'SM') or (ie_tipo_material not in (2,3,6)))
	group by a.cd_material, 
		a.cd_unidade_medida, 
		c.cd_material_estoque, 
		obter_dados_lote_fornec(nr_seq_lote_fornec,'CF'), 
		nr_seq_lote_fornec
	union
	select	a.cd_material,
		a.cd_unidade_medida,
		sum(qt_dispensacao),
		c.cd_material_estoque,
		obter_dados_lote_fornec(nr_seq_lote_fornec,'CF'),
		nr_seq_lote_fornec
	from    material c,
		cirurgia_agente_disp a,
		cirurgia b
	where	a.nr_cirurgia   = b.nr_cirurgia
	and     a.cd_material   = c.cd_material
	and	a.nr_cirurgia	= nr_cirurgia_p
	and	a.ie_operacao	= 'D'
	and	(ie_ajusta_disp_w = 'R')
	and	(obter_regra_disp_pepo(2,a.cd_material) = 'S')
	group by a.cd_material, 
		a.cd_unidade_medida, 
		c.cd_material_estoque, 
		obter_dados_lote_fornec(nr_seq_lote_fornec,'CF'), 
		nr_seq_lote_fornec;


cursor C02 is
	select	x.cd_material,
		x.cd_unidade_medida,
		sum(x.qt_material),
		y.cd_material_estoque,
		x.cd_fornec_consignado,
		x.nr_seq_lote_fornec
	from    material y,
		prescr_material x
	where	y.cd_material           = x.cd_material
	and	x.nr_prescricao		= nr_prescricao_esp_w
	group by x.cd_material, x.cd_unidade_medida, y.cd_material_estoque, cd_fornec_consignado, x.nr_seq_lote_fornec;


begin

dt_prescricao_w	:= Sysdate;
obter_param_usuario(872,24,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_ajusta_disp_w);
obter_param_usuario(872,142,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_gerar_espec_w);
obter_param_usuario(872,150,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,cd_local_estoque_w);
obter_param_usuario(900,91,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_atualiza_dt_inicio_real_w);
obter_param_usuario(900,548,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,cd_setor_atendimento_w);


select	nr_prescricao,
	cd_estabelecimento,
	cd_pessoa_fisica,
	nr_atendimento,
	cd_medico_cirurgiao,
	dt_inicio_real
into	nr_prescricao_w,
	cd_estabelecimento_w,
	cd_pessoa_fisica_w,
	nr_atendimento_w,
	cd_medico_cirurgiao_w,
	dt_inicio_real_w
from	cirurgia
where	nr_cirurgia = nr_cirurgia_p;

if	(ie_atualiza_dt_inicio_real_w = 'S') and
	(dt_inicio_real_w is not null) then
	dt_prescricao_w := dt_inicio_real_w;
end if;


delete	from ajuste_dispensacao_pepo
where	nr_cirurgia = nr_cirurgia_p;

select	nvl(max(nr_prescricao),0)
into	nr_prescricao_esp_w
from	prescr_medica
where	nr_cirurgia = nr_cirurgia_p
and	nr_prescricao <> nr_prescricao_w
and	nvl(ie_tipo_prescr_cirur,0) <> 2;


if	(nr_prescricao_esp_w = 0) then
	select	prescr_medica_seq.nextval
	into	nr_prescricao_esp_w
	from	dual;

	insert into prescr_medica(
		nr_prescricao,
		cd_pessoa_fisica,
		nr_atendimento,
		cd_medico,
		dt_prescricao,
		dt_atualizacao,
		nm_usuario,
		nr_horas_validade,
		ie_origem_inf,
		nm_usuario_original,
		nr_cirurgia,
		cd_estabelecimento,
		cd_prescritor,
		ie_emergencia,
		ie_prescricao_alta,
		ie_prescr_emergencia,
		ie_adep,
		cd_local_estoque,
		cd_setor_atendimento)
	values(
		nr_prescricao_esp_w,
		cd_pessoa_fisica_w,
		nr_atendimento_w,
		cd_medico_cirurgiao_w,
		dt_prescricao_w,
		sysdate,
		nm_usuario_p,
		12,
		obter_tipo_pessoa(Obter_Dados_Usuario_Opcao(nm_usuario_p,'C')),
		nm_usuario_p,
		nr_cirurgia_p,
		cd_estabelecimento_w,
		cd_pessoa_fisica_w,
		'N',
		'N',
		'N',
		'N',
		decode(campo_numerico(cd_local_estoque_w),0,null,campo_numerico(cd_local_estoque_w)),
		cd_setor_atendimento_w);
end if;

open C01;
loop
fetch C01 into
	cd_material_w,
	cd_unidade_medida_w,
	qt_dispensacao_w,
	cd_material_estoque_w,
	cd_cgc_fornec_w,
	nr_seq_lote_fornec_w;
exit when C01%notfound;
	begin

	insert into ajuste_dispensacao_pepo(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_cirurgia,
		cd_material,
		cd_material_estoque,
		qt_dispensacao,
		cd_unidade_medida,
		cd_fornecedor,
		nr_seq_lote_fornec)
	values(
		ajuste_dispensacao_pepo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_cirurgia_p,
		cd_material_w,
		cd_material_estoque_w,
		qt_dispensacao_w,
		cd_unidade_medida_w,
		cd_cgc_fornec_w,
		nr_seq_lote_fornec_w);
	commit;

	end;
end loop;
close C01;

open C02;
loop
fetch C02 into
	cd_material_w,
	cd_unidade_medida_w,
	qt_dispensacao_w,
	cd_material_estoque_w,
	cd_fornec_consignado_w,
	nr_seq_lote_fornec_ww;
exit when C02%notfound;
	begin

	update	ajuste_dispensacao_pepo
	set	qt_dispensacao						=	qt_dispensacao - qt_dispensacao_w
	where	nr_cirurgia						=	nr_cirurgia_p
	and	cd_material_estoque					=	cd_material_estoque_w
	and	cd_unidade_medida					=	cd_unidade_medida_w
	and	nvl(cd_fornecedor,nvl(cd_fornec_consignado_w,0))	=	nvl(cd_fornec_consignado_w,0)
	and	nvl(nr_seq_lote_fornec,nvl(nr_seq_lote_fornec_ww,0))	=	nvl(nr_seq_lote_fornec_ww,0);
	commit;

	end;
end loop;
close C02;

commit;

end gerar_ajuste_disp_consig;
/