create or replace
procedure Libera_Passagem_turno_js(	nr_ocorrencia_p	Number,
					nm_usuario_p		Varchar2) is 

begin

update 	ocorrencia 
set 	dt_liberacao = sysdate,
	nm_usuario = nm_usuario_p
where 	nr_ocorrencia = nr_ocorrencia_p;

commit;

end Libera_Passagem_turno_js;
/