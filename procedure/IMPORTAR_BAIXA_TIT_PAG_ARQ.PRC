create or replace
procedure importar_baixa_tit_pag_arq	(nr_titulo_p			number,
					dt_baixa_p			date,
					cd_moeda_p			number,
					cd_tipo_baixa_p			number,
					vl_baixa_p			number,
					vl_descontos_p			number,
					vl_outras_deducoes_p		number,
					vl_juros_p			number,
					vl_multa_p			number,
					vl_outros_acrescimos_p		number,
					vl_pago_p			number,
					nr_seq_trans_fin_p		number,
					nr_seq_conta_banco_p		number,
					cd_conta_contabil_p		varchar,
					cd_centro_custo_p		number,
					ds_observacao_p			varchar,
					nm_usuario_p			varchar,
					nr_externo_p			varchar) is


nr_sequencia_w			number(5);
qt_externo_w			number(10);



/* 
Procedure utilizada para importa��o padr�o WHEB das baixas de t�tulos a pagar
Usar interface 1619 - "Integra��o WHEB - Importar Baixas t�tulos a pagar 1.0" 
*/

begin

if	(nr_titulo_p is null) then
	--r.aise_application_error(-20011,'T�tulo n�o encontrado');
	wheb_mensagem_pck.exibir_mensagem_abort(267406);
else
	select 	nvl(max(nr_sequencia),0) + 1 
	into	nr_sequencia_w
	from	titulo_pagar_baixa
	where	nr_titulo = nr_titulo_p;
end if;

select	count(*)
into	qt_externo_w
from	titulo_pagar_baixa
where	nr_titulo	= nr_titulo_p
and	nr_externo	= nr_externo_p;

if	(qt_externo_w = 0) then
	begin
	insert	into titulo_pagar_baixa (nr_titulo,
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_baixa,
					cd_moeda,
					cd_tipo_baixa,
					ie_acao,
					vl_baixa,
					vl_descontos,
					vl_outras_deducoes,
					vl_juros,
					vl_multa,
					vl_outros_acrescimos,
					vl_pago,
					vl_devolucao,
					nr_seq_trans_fin,
					nr_seq_conta_banco,
					cd_conta_contabil,
					cd_centro_custo,
					ds_observacao,
					nr_externo)
				values	(nr_titulo_p,
					nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					dt_baixa_p,
					1,
					cd_tipo_baixa_p,
					'I',
					vl_baixa_p,
					vl_descontos_p,
					vl_outras_deducoes_p,
					vl_juros_p,
					vl_multa_p,
					vl_outros_acrescimos_p,
					vl_pago_p,
					0,
					null,
					null,
					null,
					null,
					ds_observacao_p,
					nr_externo_p);

	atualizar_saldo_tit_pagar(nr_titulo_p,nm_usuario_p);
	Gerar_W_Tit_Pag_imposto(nr_titulo_p, nm_usuario_p);

	commit;
	end;

/*else

	insert into logxxx_tasy
		(cd_log,
		nm_usuario,
		dt_atualizacao,
		ds_log)
	values	(55784,
		'IMP_WHEB',
		sysdate,
		'Erro ao inserir o t�tulo ' || nr_titulo_p || chr(13) || chr(10) ||
		'Erro: Esta baixa j� foi importada!');*/

end if;

end importar_baixa_tit_pag_arq;
/