create or replace 
procedure fsfx_gerar_ret_femco_assist
			(	ds_linha_p		varchar2,
				nr_seq_cobranca_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_lote_rem_valor_w		number(15);
vl_registro_w			number(15,2);
cd_matricula_estip_w		pls_segurado.cd_matricula_estipulante%type;
ds_linha_w			varchar2(255);
dt_remessa_retorno_w		date;
nr_seq_mensalidade_w		number(10);
nr_titulo_w			number(10);
titulo_receber_w		titulo_receber%rowtype;
nr_seq_empresa_w		number(10);
vl_liquidacao_w			titulo_receber_cobr.vl_liquidacao%type;

begin

ds_linha_w		:= replace(ds_linha_p,chr(13),'');
cd_matricula_estip_w	:= substr(ds_linha_w,1,11);
vl_registro_w		:= to_number(substr(ds_linha_w,23,13));

select	trunc(dt_remessa_retorno,'Month'),
	nr_seq_empresa
into	dt_remessa_retorno_w,
	nr_seq_empresa_w
from	cobranca_escritural
where	nr_sequencia	= nr_seq_cobranca_p;

select	max(a.nr_titulo)
into	nr_titulo_w
from	titulo_receber_cobr	a,
	titulo_receber		b,
	pls_mensalidade		c,
	pls_contrato_pagador_fin d,
	cobranca_escritural	e
where	a.nr_titulo		= b.nr_titulo
and	b.nr_seq_mensalidade	= c.nr_sequencia
and	c.nr_seq_pagador_fin	= d.nr_sequencia
and	a.nr_seq_cobranca	= e.nr_sequencia
and	e.nr_sequencia		<> nr_seq_cobranca_p
and	e.ie_remessa_retorno	= 'R'
and	trunc(e.dt_remessa_retorno,'Month')	= dt_remessa_retorno_w
and	e.nr_seq_empresa	= nr_seq_empresa_w
and	d.cd_matricula		= cd_matricula_estip_w
and	vl_titulo		= vl_registro_w;

if	(nr_titulo_w is null) then
	insert	into	pls_desc_inconsistencia
	(	nr_sequencia,dt_atualizacao, nm_usuario, dt_atualizacao_nrec,nm_usuario_nrec,
		nr_seq_cobranca, cd_estabelecimento,nr_matricula, vl_titulo,
		ds_inconsistencia)
	values
	(	pls_desc_inconsistencia_seq.nextval, sysdate, nm_usuario_p, sysdate,nm_usuario_p,
		nr_seq_cobranca_p, cd_estabelecimento_p,cd_matricula_estip_w, vl_registro_w,
		'N�o foi poss�vel encontrar o t�tulo do pagador com a matr�cula ' ||cd_matricula_estip_w);
	goto final;
else
	select	max(nr_seq_mensalidade)
	into	nr_seq_mensalidade_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
end if;

select	*
into	titulo_receber_w
from	titulo_receber
where	nr_titulo	= nr_titulo_w;

vl_liquidacao_w	:= titulo_receber_w.vl_titulo - vl_registro_w;

insert	into	titulo_receber_cobr
	(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nr_seq_cobranca, nr_titulo, vl_cobranca,
		cd_banco, cd_agencia_bancaria,
		cd_moeda, dt_liquidacao, vl_liquidacao,
		vl_despesa_bancaria, vl_juros, vl_multa,
		vl_desc_previsto, nr_seq_mensalidade)
values	(	titulo_receber_cobr_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
		nr_seq_cobranca_p, nr_titulo_w, titulo_receber_w.vl_titulo,
		titulo_receber_w.cd_banco, titulo_receber_w.cd_agencia_bancaria,
		titulo_receber_w.cd_moeda, titulo_receber_w.dt_liquidacao, vl_liquidacao_w,
		0, 0, 0,
		0, nr_seq_mensalidade_w);

<<final>>
nr_seq_mensalidade_w	:= nr_seq_mensalidade_w;

end fsfx_gerar_ret_femco_assist;
/
