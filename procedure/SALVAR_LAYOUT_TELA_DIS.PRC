create or replace procedure salvar_layout_tela_dis(nm_componente_p in conf_splitter_dis.nm_componente%type,
												   nr_posicao_p in conf_splitter_dis.nr_posicao%type,
												   nm_usuario_p in conf_splitter_dis.nm_usuario%type,
												   cd_tab_p in conf_splitter_dis.cd_tab%type) is 
				
nm_componente_w componente_java.ds_componente%type;

begin

	select count(nr_sequencia) 
	  into nm_componente_w
	  from CONF_SPLITTER_DIS 
     where nm_componente = nm_componente_p
	   and cd_tab = cd_tab_p
	   and nm_usuario = nm_usuario_p;

	if (nm_componente_w = 0) then
		insert into CONF_SPLITTER_DIS(nr_sequencia, nm_componente, nr_posicao, nm_usuario, dt_atualizacao, cd_tab) 
		     values (CONF_SPLITTER_DIS_SEQ.nextval, nm_componente_p, nr_posicao_p, nm_usuario_p, sysdate, cd_tab_p);
	else
		update CONF_SPLITTER_DIS 
		   set nr_posicao = nr_posicao_p 
		 where nm_componente = nm_componente_p
		   and cd_tab = cd_tab_p
		   and nm_usuario = nm_usuario_p;
	end if;

	commit;

end salvar_layout_tela_dis;
/