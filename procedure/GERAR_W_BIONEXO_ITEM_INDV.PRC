create or replace
procedure gerar_w_bionexo_item_indv(
				    cd_cnpj_filial_p	varchar2,                      
				    nr_requisicao_p	varchar2,                        
				    nr_seq_item_p	varchar2,  
				    qt_quantidade_p 	varchar2,
				    nm_usuario_p        varchar2) is
				

nr_sequencia_w	number(10);

			
begin
--apapandini OS623800
select	w_bionexo_item_individual_seq.nextval
into	nr_sequencia_w
from	dual;


insert into w_bionexo_item_individual(	
			cd_cnpj_filial,                      
			nr_requisicao,                      
			nr_seq_item,
			qt_quantidade,
			nr_sequencia,
			nm_usuario,              
			nm_usuario_nrec, 
			dt_atualizacao,          
			dt_atualizacao_nrec)
values(	cd_cnpj_filial_p,                      
	nr_requisicao_p,                      
	nr_seq_item_p,
	qt_quantidade_p,
	nr_sequencia_w,        
	nm_usuario_p,              
	nm_usuario_p, 
	sysdate,          
	sysdate);
	


commit;

end gerar_w_bionexo_item_indv;
/