CREATE OR REPLACE
PROCEDURE Gerar_EIS_Qualidade_Mes(
				dt_referencia_p	Date,
				nm_usuario_p		Varchar2) IS

BEGIN

begin
Gerar_EIS_Eficacia_OS(dt_referencia_p, nm_usuario_p);
exception
	when others then
		null;
end;

begin
Gerar_EIS_Elab_Doc(dt_referencia_p, nm_usuario_p);
exception
	when others then
		null;
end;


END Gerar_EIS_Qualidade_Mes;
/