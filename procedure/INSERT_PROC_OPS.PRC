create or replace 
procedure INSERT_PROC_OPS (	cd_procedimento_p       varchar2,
				nr_atendimento_p        number,
				nr_seq_proc_interno_p   number,
				ie_lado_p               varchar2,
				ie_proc_princ_atend_p   varchar2,
				nr_cirurgia_p			number default null,
				nm_usuario_p			varchar2 default null,
				ie_origem_proced_p		number default 11,
				ie_liberar_informacao_p	varchar2 default 'N') is

cd_setor_atendimento_w   	number(5);
cd_departamento_w	 	number(6);
nr_seq_episodio_w	 	atendimento_paciente.nr_seq_episodio%type;
nr_sequencia_w			number(10);
cd_convenio_w			number(10);
cd_categoria_w			varchar2(10);
cd_plano_w				varchar2(10);
cd_pessoa_fisica_w		number(10);
cd_procedimento_w		procedimento.cd_procedimento%type;
ie_origem_proced_w		procedimento.ie_origem_proced%type;
ie_proc_princ_w			procedimento_pac_medico.ie_proc_princ%type;
ie_proc_adicional_w		procedimento_pac_medico.ie_proc_adicional%type;
qt_ie_proc_princ_w			number(2);

    
begin
-- cd_setor_atendimento_w e cd_departamento_w
begin
select	x.cd_departamento,
	x.cd_setor_atendimento
into	cd_departamento_w,
	cd_setor_atendimento_w
from	atend_paciente_unidade x
where	x.nr_atendimento 	= nr_atendimento_p
and	x.nr_seq_interno	= Obter_AtePacu_Data(x.nr_atendimento,'A',sysdate);
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(1025219); --O paciente nao tem passagem por setor sem alta!
end;

-- nr_seq_episodio_w
select 	nr_seq_episodio
into 	nr_seq_episodio_w
from 	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

select	procedimento_pac_medico_seq.nextval
into	nr_sequencia_w
from	dual;

select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select 	max(cd_convenio),
		max(cd_categoria),
		max(cd_plano_convenio)
into	cd_convenio_w,
		cd_categoria_w,
		cd_plano_w
from	atend_categoria_convenio
where 	nr_atendimento		= nr_atendimento_p
and		dt_inicio_vigencia	= 
			(select max(dt_inicio_vigencia)
			from Atend_categoria_convenio 
			where nr_atendimento	= nr_atendimento_p);
	
cd_procedimento_w 	:= cd_procedimento_p;
ie_origem_proced_w 	:= ie_origem_proced_p;

if	(nvl(nr_seq_proc_interno_p,0) > 0) then
	OBTER_PROC_TAB_INTERNO(nr_seq_proc_interno_p, null, nr_atendimento_p, null, cd_procedimento_w, ie_origem_proced_w, null, null, null);
	if	(cd_procedimento_w is null) then
		cd_procedimento_w 	:= cd_procedimento_p;
		ie_origem_proced_w 	:= ie_origem_proced_p;
	end if;
end if;

select 	count(ie_proc_princ)
into  	qt_ie_proc_princ_w
from  	procedimento_pac_medico 
where 	nr_seq_episodio = nr_seq_episodio_w
and   	ie_proc_princ   = 'S'
and	dt_liberacao is not null
and	ie_situacao = 'A';

if	(nvl(ie_proc_princ_atend_p,'NULL') = '$HL') then
	if (qt_ie_proc_princ_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1074870);
	end if;
	ie_proc_princ_w		:= 'S';
	ie_proc_adicional_w	:= 'N';
elsif	(nvl(ie_proc_princ_atend_p,'NULL') = '$NL') then
	ie_proc_princ_w		:= 'N';
	ie_proc_adicional_w	:= 'S';
end if;

-- inserir o procedimento
insert into procedimento_pac_medico 
	(nr_sequencia, 
	dt_atualizacao, 
	nm_usuario, 
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_atendimento, 
	ie_situacao, 
	cd_procedimento, 
	ie_origem_proced, 
	qt_procedimento, 
	nr_seq_episodio,
	ie_lado,
	ie_proc_princ,
	ie_proc_adicional,
	cd_departamento,
	cd_setor_atendimento,
	nr_seq_proc_interno,
	dt_procedimento,
	nr_cirurgia,
	IE_SIST_EXT_ORIGEM)
values  (nr_sequencia_w, 
	sysdate, 
	nvl(nm_usuario_p,wheb_usuario_pck.get_nm_usuario()), 
	sysdate, 
	nvl(nm_usuario_p,wheb_usuario_pck.get_nm_usuario()), 
	nr_atendimento_p, 
	'A', 			 
	cd_procedimento_w, 
	ie_origem_proced_w,   
	1, 
	nr_seq_episodio_w,
	ie_lado_p,
	ie_proc_princ_w,
	ie_proc_adicional_w,
	cd_departamento_w,
	cd_setor_atendimento_w,
	nr_seq_proc_interno_p,
	sysdate,
	nr_cirurgia_p,
	'TASY');

if	(nvl(ie_liberar_informacao_p,'N') = 'S') then	
	LIBERAR_PROC_PAC_MEDIC(nr_sequencia_w, nm_usuario_p);	
end if;
	
commit;
insert_proc_adic_ops( nr_seq_proc_interno_p,
					  cd_convenio_w,
					  cd_categoria_w,		
					  cd_plano_w,	
					  obter_pf_usuario(nm_usuario_p, 'C'),
					  obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A'),
					  nm_usuario_p,		
					  nr_atendimento_p,
					  cd_departamento_w,		
					  cd_setor_atendimento_w,
					  nr_seq_episodio_w,
					  ie_liberar_informacao_p,
					  'N');
					  
end INSERT_PROC_OPS;
/