CREATE OR REPLACE
PROCEDURE Gerar_protocolo_procedimento(cd_protocolo_dest_p     NUMBER
                                        , nr_seq_destino_p      NUMBER
                                        , nr_seq_procedimento_p VARCHAR2)
IS
  cd_material_w       protocolo_medic_material.cd_material%TYPE;
  qt_dose_w           protocolo_medic_material.qt_dose%TYPE;
  ie_via_aplicacao_w  protocolo_medic_material.ie_via_aplicacao%TYPE;
  cd_unidade_medida_w protocolo_medic_material.cd_unidade_medida%TYPE;
  ds_horarios_w       protocolo_medic_material.ds_horarios%TYPE;
  ie_checar_adep_w    protocolo_medic_material.ie_checar_adep%TYPE;
  cd_intervalo_w      protocolo_medic_material.cd_intervalo%TYPE;
  dt_inicio_w         cpoe_procedimento.dt_inicio%TYPE;
  hr_prim_horario_w   protocolo_medic_material.hr_prim_horario%TYPE;
  ie_urgencia_w       protocolo_medic_material.ie_urgencia%TYPE;
  nr_agrupamento_w    protocolo_medic_material.nr_agrupamento%TYPE;
  nr_seq_material_w   protocolo_medic_material.nr_seq_material%TYPE;

  cd_procedimento_w   protocolo_medic_proc.cd_procedimento%TYPE;
  ie_origem_proced_w  protocolo_medic_proc.ie_origem_proced%TYPE;

  nm_usuario_w        VARCHAR2(255);

  CURSOR c01 IS
    SELECT cd_material
           , qt_dose
           , ie_via_aplicacao
           , cd_unidade_medida
           , ds_horarios
           , ie_checar_adep
           , cd_intervalo
           , dt_inicio
           , hr_prim_horario
           , ie_urgencia
           , nr_agrupamento
    FROM   cpoe_material_proced_v
    WHERE  nr_seq_proced = nr_seq_procedimento_p
    UNION
    SELECT cd_material
           , qt_dose
           , ie_via_aplicacao
           , cd_unidade_medida
           , ds_horarios
           , ''   ie_checar_adep
           , cd_intervalo
           , dt_inicio
           , hr_prim_horario
           , ie_urgencia
           , NULL nr_agrupamento
    FROM   cpoe_material
    WHERE  nr_seq_procedimento = nr_seq_procedimento_p;
BEGIN
    nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

    IF ( nr_seq_procedimento_p IS NOT NULL ) THEN

      FOR t IN (
        SELECT nr_sequencia
             , qt_procedimento
             , cd_intervalo
             , Substr(ds_observacao, 1, 255)       AS ds_observacao
             , Substr(ds_dado_clinico, 1, 255)     AS ds_dado_clinico
             , cd_material_exame
             , cd_setor_atendimento
             , Nvl(ie_se_necessario, 'N')          AS ie_se_necessario
             , Nvl(ie_acm, 'N')                    AS ie_acm
             , Substr(ds_material_especial, 1, 45) AS ds_material_especial
             , Nvl(ie_urgencia, 'N')               AS ie_urgencia
             , nr_seq_proc_interno
             , ie_lado
             , dt_prev_execucao
             , ds_justificativa
             , nr_seq_prot_glic
             , nr_atendimento
             , nr_seq_topografia
	     , nr_seq_contraste
        FROM   cpoe_procedimento
        WHERE  nr_sequencia = nr_seq_procedimento_p
      )
      LOOP

        Obter_Proc_Tab_Interno(t.nr_seq_proc_interno, 0, t.nr_atendimento, 0, cd_procedimento_w, ie_origem_proced_w); 

        INSERT INTO protocolo_medic_proc
                    (cd_protocolo
                     , nr_sequencia
                     , nr_seq_proc
                     , qt_procedimento
                     , cd_procedimento
                     , ie_origem_proced
                     , dt_atualizacao
                     , nm_usuario
                     , cd_intervalo
                     , ds_observacao
                     , ds_dado_clinico
                     , cd_material_exame
                     , cd_setor_atendimento
                     , ie_se_necessario
                     , ie_acm
                     , ds_material_especial
                     , ie_urgencia
                     , nr_seq_proc_interno
                     , ie_lado
                     , ie_multiplicar_quant
                     , dt_prev_execucao
                     , ds_justificativa
                     , nr_seq_prot_glic
                     , cd_procedimento_lcb
                     , ie_anatomia
                     , nr_seq_topografia
		     , nr_seq_contraste)
        VALUES(  cd_protocolo_dest_p
               , nr_seq_destino_p
               , t.nr_sequencia
               , t.qt_procedimento
               , cd_procedimento_w
               , ie_origem_proced_w
               , SYSDATE
               , nm_usuario_w
               , t.cd_intervalo
               , t.ds_observacao
               , t.ds_dado_clinico
               , t.cd_material_exame
               , t.cd_setor_atendimento
               , t.ie_se_necessario
               , t.ie_acm
               , t.ds_material_especial
               , t.ie_urgencia
               , t.nr_seq_proc_interno
               , t.ie_lado
               , 'S'
               , t.dt_prev_execucao
               , t.ds_justificativa
               , t.nr_seq_prot_glic
               , t.nr_seq_proc_interno
               , 'N'
               , t.nr_seq_topografia
	       , t.nr_seq_contraste
        );
      END LOOP;

      OPEN c01;

      LOOP
          FETCH c01 INTO
            cd_material_w
            , qt_dose_w
            , ie_via_aplicacao_w
            , cd_unidade_medida_w
            , ds_horarios_w
            , ie_checar_adep_w
            , cd_intervalo_w
            , dt_inicio_w
            , hr_prim_horario_w
            , ie_urgencia_w
            , nr_agrupamento_w;

          exit WHEN c01%NOTFOUND;

          BEGIN
              Incluir_prot_derivado_material(
                cd_protocolo_dest_p
                , nr_seq_destino_p
                , cd_material_w
                , nr_agrupamento_w
                , 5 -- Procedimento
                , qt_dose_w
                , NULL
                , ie_via_aplicacao_w
                , cd_unidade_medida_w
                , ds_horarios_w
                , ie_checar_adep_w
                , cd_intervalo_w
                , hr_prim_horario_w
                , NULL
                , NULL
                , SYSDATE
                , ie_urgencia_w
                , 'N'
                , 'N'
                , 'N'
                , nr_seq_procedimento_p);
          EXCEPTION
              WHEN OTHERS THEN
                NULL;
          END;
      END LOOP;

      CLOSE c01;
    END IF;

    COMMIT;
END gerar_protocolo_procedimento;

/
