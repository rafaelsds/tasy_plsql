create or replace
procedure copiar_exame_med_protocolo(
			nr_atendimento_p    	number,
			cd_protocolo_p		number,
			nr_sequencia_p		number,
			nr_seq_proc_p		number,
			nr_horas_validade_p	number,
			nr_prescricao_p		number,
			dt_primeiro_horario_p varchar2,
			dt_prescricao_p		date,
			dt_procedimento_p	date,
			nm_usuario_p		varchar2,	
			ie_lado_p			varchar2,
			ds_dano_clinico_p	varchar2,
			cd_cgc_externo_p	varchar2 default null,
			ds_material_especial_p	varchar2 default null,
			cd_medico_exec_p	varchar2
			) is 
 
nr_prescricao_w    			number(10);
nr_seq_proc_w				number(10,0);
cd_intervalo_w          	varchar2(7);
nr_sequencia_w				number(10);
nr_agrupamento_w			number(07,1);
cd_proced_w					number(15);
qt_proced_w					number(15,4);
ds_obs_w					varchar2(2000);
ie_origem_w					number(10);
cd_setor_exclusivo_w		number(5);
cd_material_exame_w			varchar2(20);
nr_seq_exame_w				number(10);
cd_proced_aux_w				number(15);
ie_origem_aux_w				number(10);
cd_convenio_w				number(5);
ds_erro_w					varchar2(255);
ds_material_especial_w		varchar2(45);	
nr_seq_proc_novo_w			number(6);
nr_seq_proc_interno_w		number(10);
nr_intervalo_w				number(10);
nr_seq_prot_glic_w			number(10);
nr_intervalo_ww				number(10) := 0;
ds_horarios_w				varchar2(2000);
ds_dano_clinico_w			varchar2(2000);
ds_horarios_ww				varchar2(2000) :='';
ie_lado_w					varchar2(1);
dt_prescricao_w				date;
ie_urgencia_w				varchar2(1);
ie_prescr_proc_sem_lib_w 	varchar2(30);
cd_estabelecimento_w		number(4);
ie_tipo_atendimento_w		number(3);
ie_tipo_convenio_w			number(2);
cd_categoria_w				varchar2(10);
cd_setor_w					number(5,0);
ie_avisar_result_w			varchar2(1);
cd_setor_atendimento_w		number(10,0);
ie_setor_classif_w			varchar2(1);
ie_gravar_indic_w			varchar2(2);
ds_indicacao_w				varchar2(255);
ie_previsao_proced_w		varchar2(15);
nr_seq_proc_interno_aux_w 	number(10);
dt_agenda_w					date;
cd_pessoa_fis_agenda_w		varchar2(10);
ie_atualizar_nr_seq_ag_w	varchar2(1);
nr_seq_agenda_serv_w		number(10);
ie_vinc_seq_agend_w			varchar2(1);
ds_diag_provavel_ap_w		varchar2(255);
ds_exame_anterior_ap_w		varchar2(255);
ds_localizacao_lesao_w		varchar2(2000);
ds_tempo_doenca_w			varchar2(255);
cd_cgc_laboratorio_w		varchar2(14);
qt_frasco_env_w				number(5);
cd_medico_exec_w			varchar2(20);
nr_seq_proc_int_cirur_w		number(10);
qt_peca_ap_w				number(3);
nr_seq_amostra_princ_w		number(10);
ds_qualidade_peca_ap_w		varchar2(2000);
ie_forma_exame_w			varchar2(15);
cd_pessoa_coleta_w			varchar2(10);
cd_plano_convenio_w			varchar2(10);
ie_verifica_setores_adic_w	varchar2(10);
qt_existe_setores_w			number(15);
qt_existe_setores_int_w		number(15);
cd_setor_prescricao_w		prescr_medica.cd_setor_atendimento%type;
ie_Gera_Setor_PrescrProc_w	varchar2(1); 
nr_seq_topografia_w			prescr_procedimento.nr_seq_topografia%type;
ie_acm_w					prescr_procedimento.ie_acm%type;

CURSOR C02 IS
select	a.nr_seq_proc,
		a.nr_seq_proc + nr_sequencia_w,
		a.cd_procedimento,        
		a.qt_procedimento,        
		a.ds_observacao, 
		a.ie_origem_proced,
		a.cd_intervalo,
		a.cd_setor_atendimento,
		a.cd_material_exame,
		a.nr_seq_exame,
		a.ds_material_especial,
		a.nr_seq_proc_interno,
		decode(nvl(ie_questiona_lado, 'N'), 'S', decode(ie_lado_p, null, a.ie_lado, ie_lado_p), a.ie_lado),
		nvl(a.ie_urgencia, 'N'),
		a.nr_seq_prot_glic,
		a.ds_dado_clinico,
		a.ie_horario,
		a.cd_medico_exec,
		substr(a.ds_diag_provavel_ap,1,255),
		substr(a.ds_exame_anterior_ap,1,255),
		substr(a.ds_localizacao_lesao,1,2000),
		substr(a.ds_tempo_doenca,1,255),
		substr(a.cd_cgc_laboratorio,1,14),
		a.qt_frasco_env,
		a.nr_seq_proc_int_cirur,
		a.qt_peca_ap,
		a.nr_seq_amostra_princ,
		a.ds_qualidade_peca_ap,
		a.ie_forma_exame,
		a.cd_pessoa_coleta,
		nr_seq_topografia,
		a.ie_acm
from	protocolo_medic_proc a
where	a.cd_protocolo	= cd_protocolo_p
and		a.nr_sequencia	= nr_sequencia_p
and		((a.nr_seq_proc	= nr_seq_proc_p) or (nr_seq_proc_p = 0));

BEGIN


nr_seq_agenda_serv_w	:= null;
select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	usuario
where	nm_usuario = nm_usuario_p;

Obter_Param_Usuario(924,530,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_prescr_proc_sem_lib_w);
Obter_Param_Usuario(924,835,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_setor_classif_w);
Obter_Param_Usuario(924,987, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gravar_indic_w);
Obter_Param_Usuario(924,1020, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_vinc_seq_agend_w);
Obter_Param_Usuario(924,493, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_Gera_Setor_PrescrProc_w);
	
obter_param_usuario(916,562, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_verifica_setores_adic_w);

nr_prescricao_w := nr_prescricao_p;

if	(dt_prescricao_p	is null) or
	(cd_protocolo_p		is null) or
	(nr_sequencia_p		is null) then
	--Os Campos M�dico, Data, Paciente e Protocolo/Sequencia devem ser informados
	Wheb_mensagem_pck.exibir_mensagem_abort(178295);
end if;

-- Obter_Conv�nio do Atendimento

if 	(nr_atendimento_p is not null) then
	begin
	select 	cd_convenio,
			cd_plano_convenio
	into 	cd_convenio_w,
			cd_plano_convenio_w
	from 	atend_categoria_convenio
	where 	nr_atendimento = nr_atendimento_p
	  and 	dt_inicio_vigencia = (	select 	max(dt_inicio_vigencia)
									from 	atend_categoria_convenio
									where 	nr_atendimento = nr_atendimento_p);
	exception
		when others then
			cd_convenio_w := null;
	end;
end if;


-- Procedimentos
if 	(nr_prescricao_w > 0) then
	select 	nvl(max(nr_sequencia),0),
	   	nvl(max(nr_agrupamento),0)
	into	nr_sequencia_w,
		nr_agrupamento_w
	from prescr_procedimento
	where nr_prescricao = nr_prescricao_w;
end if;

select	obter_classif_setor(max(cd_setor_atendimento)),
		max(cd_setor_atendimento)
into	cd_setor_atendimento_w,
		cd_setor_prescricao_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_w;

if	(ie_setor_classif_w = 'S') and
	(cd_setor_atendimento_w = 1) then
	ie_avisar_result_w := 'S';
end if;	

open C02;
loop
fetch C02 into
	nr_seq_proc_w,		
	nr_seq_proc_novo_w,
	cd_proced_w,
	qt_proced_w,
	ds_obs_w,
	ie_origem_w,
	cd_intervalo_w,
	cd_setor_exclusivo_w,
	cd_material_exame_w,
	nr_seq_exame_w,
	ds_material_especial_w,
	nr_seq_proc_interno_w,
	ie_lado_w,
	ie_urgencia_w,
	nr_seq_prot_glic_w,
	ds_dano_clinico_w,
	ie_previsao_proced_w,
	cd_medico_exec_w,
	ds_diag_provavel_ap_w,
	ds_exame_anterior_ap_w,
	ds_localizacao_lesao_w,
	ds_tempo_doenca_w,
	cd_cgc_laboratorio_w,
	qt_frasco_env_w,
	nr_seq_proc_int_cirur_w,
	qt_peca_ap_w,
	nr_seq_amostra_princ_w,
	ds_qualidade_peca_ap_w,
	ie_forma_exame_w,
	cd_pessoa_coleta_w,
	nr_seq_topografia_w,
	ie_acm_w;
exit when C02%notfound;

	if	(nr_seq_proc_interno_w is not null) then
		Obter_Proc_Tab_Interno(nr_seq_proc_interno_w,nr_prescricao_w,0,0,cd_proced_w,ie_origem_w,null,null);
	end if;
	
	begin
	select	nvl(cd_setor_exclusivo,cd_setor_exclusivo_w)
	into	cd_setor_exclusivo_w
	from	procedimento
	where	cd_procedimento		= cd_proced_w
	and	ie_origem_proced	= ie_origem_w;
	exception
	when others then
		cd_setor_exclusivo_w := cd_setor_exclusivo_w;
	end;
	
	if	(cd_setor_exclusivo_w is null) and
		(nr_seq_exame_w is not null) then

		select	obter_setor_atend_proc_lab(cd_estabelecimento_w,cd_proced_w,ie_origem_w, null,cd_setor_prescricao_w,null,nr_seq_exame_w)
		into	cd_setor_exclusivo_w
		from	dual;

	end if;
	
	if	(cd_setor_exclusivo_w is null) and 
		(ie_Gera_Setor_PrescrProc_w = 'S') and 
		(obter_se_setor_exec(cd_setor_prescricao_w, cd_estabelecimento_w, cd_proced_w,ie_origem_w, nr_seq_proc_interno_w) = 'S') then 
		cd_setor_exclusivo_w	:= cd_setor_prescricao_w; 
	end if; 
	
	if	(nr_seq_exame_w is not null) then
		/*begin
		select	distinct
			cd_procedimento,
			ie_origem_proced
		into	cd_proced_aux_w,
			ie_origem_aux_w
		from	exame_lab_convenio
		where	nr_seq_exame = nr_seq_exame_w
		  and	cd_convenio = cd_convenio_w;
		exception
			when others then
				cd_proced_aux_w	:= null;
				ie_origem_aux_w	:= null;
		end;*/
		
		obter_exame_lab_convenio(nr_seq_exame_w, cd_convenio_w, cd_categoria_w, ie_tipo_atendimento_w,
					cd_estabelecimento_w, ie_tipo_convenio_w, nr_seq_proc_interno_w, cd_material_exame_w, cd_plano_convenio_w,
					cd_setor_w, cd_proced_aux_w, ie_origem_aux_w, ds_erro_w, nr_seq_proc_interno_aux_w);

		if 	(nr_seq_proc_interno_aux_w > 0) then
			nr_seq_proc_interno_w := nr_seq_proc_interno_aux_w;
		end if;
		if	(nvl(cd_proced_aux_w,0) > 0) then
			cd_proced_w	:= cd_proced_aux_w;
			ie_origem_w	:= nvl(ie_origem_aux_w,ie_origem_w);
		end if;
		
	end if;
	
	/* Verificar se procedimento cadastrado com especifica��o para a data prevista */
	if	(ie_previsao_proced_w is not null) then
		if	(ie_previsao_proced_w = 'DP') then		---- Data da prescri��o (DP) 
			dt_prescricao_w := dt_prescricao_p;
		elsif (ie_previsao_proced_w = '3h') then	---- 3 horas ap�s a prescri��o (3h)
			dt_prescricao_w := dt_prescricao_p + 3/24;
		elsif (ie_previsao_proced_w = '6h') then	---- 6 horas ap�s a prescri��o (6h)
			dt_prescricao_w := dt_prescricao_p + 6/24;
		elsif (ie_previsao_proced_w = '9h') then	---- 9 horas ap�s a prescri��o (9h)
			dt_prescricao_w := dt_prescricao_p + 9/24;			
		end if;
	else
		select	Obter_data_prev_exec(dt_prescricao_p,nvl(dt_procedimento_p,dt_prescricao_p),cd_setor_exclusivo_w, nr_prescricao_w, 'A')
		into	dt_prescricao_w
		from	dual;
	end if;
	
	if	(nr_seq_prot_glic_w is not null) then
		select	max(ds_indicacao)
		into	ds_indicacao_w
		from	pep_protocolo_glicemia
		where	nr_sequencia = nr_seq_prot_glic_w;
		
		if	(ie_gravar_indic_w = 'S') and 
			(ds_indicacao_w is not null) then
			ds_obs_w := substr(ds_obs_w || ' ' || ds_indicacao_w,1,2000);
		end if;
	end if;
	
	if	(nr_atendimento_p is not null) and
		(ie_vinc_seq_agend_w = 'S')then
		select	max(a.cd_pessoa_fisica)
		into	cd_pessoa_fis_agenda_w
		from	atendimento_paciente a
		where	a.nr_atendimento	= nr_atendimento_p;
		
		Age_obt_regra_vinc_proc_rep	(dt_prescricao_p,
						cd_pessoa_fis_agenda_w,
						cd_estabelecimento_w,
						nr_seq_proc_interno_w,						
						nm_usuario_p,
						nr_seq_agenda_serv_w
						);			
	end if;
	
	if	(ie_verifica_setores_adic_w = 'S')	then
		begin
		if	(nvl(nr_seq_proc_interno_w,0) > 0) then
			begin
			select	count(*)
			into	qt_existe_setores_int_w
			from	proc_interno_setor
			where	nr_seq_proc_interno	= nr_seq_proc_interno_w;
			end;

			if	(qt_existe_setores_int_w > 1) then
				cd_setor_exclusivo_w	:=	null;
			end if;
		else
			begin
			select	nvl(max(b.cd_setor_exclusivo),0)
			into	cd_setor_w
			from	procedimento b
			where	b.cd_procedimento	= cd_proced_w
			and	b.ie_origem_proced	= ie_origem_w;

			if	(cd_setor_w = 0) then
				begin
				select	count(*)
				into	qt_existe_setores_w
				from	procedimento_setor_atend
				where	cd_procedimento 	= cd_proced_w
				and	ie_origem_proced 	= ie_origem_w
				and	cd_estabelecimento = cd_estabelecimento_w;
				if	(qt_existe_setores_w > 1) then
					cd_setor_exclusivo_w	:=	null;
				elsif (qt_existe_setores_w = 1) then
					select	max(cd_setor_atendimento)
					into	cd_setor_exclusivo_w
					from	procedimento_setor_atend
					where	cd_procedimento 	= cd_proced_w
					and	ie_origem_proced 	= ie_origem_w
					and	cd_estabelecimento = cd_estabelecimento_w;
				else
					cd_setor_exclusivo_w	:=	null;
				end if;
				end;
			else
				cd_setor_exclusivo_w	:=	cd_setor_w;
			end if;
			end;
		end if;
		end;
	end if;

	if	(cd_cgc_externo_p is not null) then
		cd_setor_exclusivo_w	:= obter_setor_laboratorio_ext(cd_cgc_externo_p);
	end if;
	
	nr_agrupamento_w := nr_agrupamento_w + 1;
	insert into prescr_procedimento 
		(nr_prescricao, 
		nr_sequencia,
		nr_agrupamento, 
		cd_procedimento, 
		qt_procedimento, 
		dt_atualizacao,
		nm_usuario, 
		ds_observacao, 
		cd_motivo_baixa, 
		ie_origem_proced, 
		cd_intervalo,
		ie_urgencia, 
		ie_suspenso, 
		cd_setor_atendimento, 
		dt_prev_execucao,
		cd_material_exame, 
		nr_seq_exame, 
		ie_status_atend, 
		ie_amostra, 
		ie_origem_inf,
		ie_executar_leito,
		ie_se_necessario,
		ds_material_especial,
		nr_seq_interno,
		nr_seq_proc_interno,
		ie_avisar_result,
		cd_protocolo,
		nr_seq_protocolo,
		nr_seq_proc_protocolo,
		ie_lado,
		ds_dado_clinico,
		nr_seq_prot_glic,
		cd_cgc_laboratorio,
		nr_seq_agenda_cons,
		cd_medico_exec,
		ds_diag_provavel_ap,
		ds_exame_anterior_ap,
		ds_localizacao_lesao,
		ds_tempo_doenca,
		qt_frasco_env,
		nr_seq_proc_int_cirur,
		qt_peca_ap,
		nr_seq_amostra_princ,
		ds_qualidade_peca_ap,
		ie_forma_exame,
		cd_pessoa_coleta,
		nr_seq_topografia,
		ie_acm)
	values (nr_prescricao_w, 
		nr_sequencia_w + nr_seq_proc_w,
		nr_agrupamento_w, 
		cd_proced_w, 
		qt_proced_w, 
		dt_prescricao_p,
		nm_usuario_p, 
		ds_obs_w, 
		0, 
		ie_origem_w, 
		cd_intervalo_w, 
		ie_urgencia_w, 
		'N', 
		cd_setor_exclusivo_w,
		dt_prescricao_w, 
		cd_material_exame_w, 
		nr_seq_exame_w, 
		5, 
		'N', 
		'1',
		'N',
		'N',
		nvl(ds_material_especial_p,ds_material_especial_w),
		prescr_procedimento_seq.NextVal,
		nr_seq_proc_interno_w,
		nvl(ie_avisar_result_w,'N'),
		cd_protocolo_p,
		nr_sequencia_p,
		nr_seq_proc_w,
		ie_lado_w,
		nvl(ds_dano_clinico_p,ds_dano_clinico_w),
		nr_seq_prot_glic_w,
		nvl(cd_cgc_externo_p,cd_cgc_laboratorio_w),
		nr_seq_agenda_serv_w,
		nvl(cd_medico_exec_p,cd_medico_exec_w),
		ds_diag_provavel_ap_w,
		ds_exame_anterior_ap_w,
		ds_localizacao_lesao_w,
		ds_tempo_doenca_w,
		qt_frasco_env_w,
		nr_seq_proc_int_cirur_w,
		qt_peca_ap_w,
		nr_seq_amostra_princ_w,
		ds_qualidade_peca_ap_w,
		ie_forma_exame_w,
		cd_pessoa_coleta_w,
		nr_seq_topografia_w,
		ie_acm_w);

	if	(nr_seq_prot_glic_w is not null) then
		Gerar_Prescr_Proc_Glic(nr_prescricao_w, nr_sequencia_w + nr_seq_proc_w,nr_seq_prot_glic_w,nm_usuario_p);	
	end if;
		
	if	(ie_prescr_proc_sem_lib_w = 'S') then
		Gerar_prescr_proc_sem_dt_lib(nr_prescricao_w,nr_sequencia_w + nr_seq_proc_w,obter_perfil_ativo,'N',nm_usuario_p);
	end if;
		
	Inserir_mat_prescr_prot(nr_prescricao_p, cd_protocolo_p, nr_sequencia_p, 0,
				5, 0, nr_seq_proc_w, 0, null,
				nr_seq_proc_novo_w, null, cd_intervalo_w, 1, nm_usuario_p, 'N',0,null,0,0, null,null,null,null,null,null,null,null,null,null,null,null,null,'S',null,null,null,null,null,null,null,null,null,null,null,null); 
	
	nr_intervalo_w	:= nr_intervalo_ww;
	ds_horarios_w	:= ds_horarios_ww;
	

	Recalcular_Horario_Prescricao(cd_intervalo_w,dt_prescricao_w,NR_HORAS_VALIDADE_P,cd_proced_w,nr_intervalo_w,ds_horarios_w,'N',cd_proced_w,nr_prescricao_w, nr_sequencia_w + nr_seq_proc_w);

	consistir_prescr_procedimento(nr_prescricao_w, nr_sequencia_w + nr_seq_proc_w, nm_usuario_p, 0, ds_erro_w);

end loop; 
close C02;

COMMIT;
END Copiar_Exame_Med_Protocolo;
/