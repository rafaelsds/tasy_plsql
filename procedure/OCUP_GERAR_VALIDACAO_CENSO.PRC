create or replace
PROCEDURE ocup_gerar_validacao_censo	(	dt_referencia_p	DATE ) IS

cd_setor_atendimento_w	NUMBER(10);
ds_setor_atendimento_w	VARCHAR2(255);
cd_unidade_w		VARCHAR2(50);
nr_seq_interno_w	NUMBER(10);
dt_referencia_w		DATE;
dt_referencia_dia_w	VARCHAR2(2);
nr_ordenacao_w		NUMBER(30);
vl_dia01_w 		NUMBER(15,2) := 0;
vl_dia02_w 		NUMBER(15,2) := 0;
vl_dia03_w 		NUMBER(15,2) := 0;
vl_dia04_w 		NUMBER(15,2) := 0;
vl_dia05_w 		NUMBER(15,2) := 0;
vl_dia06_w 		NUMBER(15,2) := 0;
vl_dia07_w 		NUMBER(15,2) := 0;
vl_dia08_w 		NUMBER(15,2) := 0;
vl_dia09_w 		NUMBER(15,2) := 0;
vl_dia10_w 		NUMBER(15,2) := 0;
vl_dia11_w 		NUMBER(15,2) := 0;
vl_dia12_w 		NUMBER(15,2) := 0;
vl_dia13_w 		NUMBER(15,2) := 0;
vl_dia14_w 		NUMBER(15,2) := 0;
vl_dia15_w 		NUMBER(15,2) := 0;
vl_dia16_w 		NUMBER(15,2) := 0;
vl_dia17_w 		NUMBER(15,2) := 0;
vl_dia18_w 		NUMBER(15,2) := 0;
vl_dia19_w 		NUMBER(15,2) := 0;
vl_dia20_w 		NUMBER(15,2) := 0;
vl_dia21_w 		NUMBER(15,2) := 0;
vl_dia22_w 		NUMBER(15,2) := 0;
vl_dia23_w 		NUMBER(15,2) := 0;
vl_dia24_w 		NUMBER(15,2) := 0;
vl_dia25_w 		NUMBER(15,2) := 0;
vl_dia26_w 		NUMBER(15,2) := 0;
vl_dia27_w 		NUMBER(15,2) := 0;
vl_dia28_w 		NUMBER(15,2) := 0;
vl_dia29_w 		NUMBER(15,2) := 0;
vl_dia30_w 		NUMBER(15,2) := 0;
vl_dia31_w 		NUMBER(15,2) := 0;
nr_atendimento_w	NUMBER(10);

CURSOR C01 IS
	SELECT	DISTINCT
		a.cd_setor_atendimento,
		obter_nome_setor(a.cd_setor_atendimento) ds_setor_atendimeto
	FROM	unidade_atendimento a,
		setor_Atendimento b
	WHERE 	b.cd_classif_setor 		IN (3,4)
	AND	a.cd_Setor_Atendimento	= b.cd_Setor_atendimento
	ORDER BY obter_nome_setor(a.cd_setor_atendimento);

CURSOR C02 IS
	SELECT	eis_obter_unid_bc(nr_seq_unidade),
		nr_seq_unidade,
		dt_referencia,
		SUBSTR(TO_CHAR(dt_referencia, 'dd'),1,2),
		NVL(nr_atendimento,0)
	FROM	log_Geracao_censo
	WHERE	cd_setor_atendimento = cd_setor_atendimento_w
	AND	TRUNC(dt_referencia) BETWEEN PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0) AND TRUNC(PKG_DATE_UTILS.END_OF(dt_referencia_p, 'MONTH', 0))
	ORDER BY 3;
BEGIN
nr_ordenacao_w := 0;

DELETE FROM w_censo_validacao
WHERE PKG_DATE_UTILS.start_of(dt_referencia,'month',0) = PKG_DATE_UTILS.start_of(dt_referencia_p,'month',0);

COMMIT;

OPEN C01;
LOOP
FETCH C01 INTO
	cd_setor_atendimento_w,
	ds_setor_atendimento_w;
EXIT WHEN C01%NOTFOUND;
	BEGIN
	-- cabe�alho
	INSERT INTO w_censo_validacao (cd_setor_atendimento,ds_setor_atendimento,nr_ordenacao, dt_referencia)
				VALUES	(cd_setor_atendimento_w,ds_setor_atendimento_w,0,PKG_DATE_UTILS.start_of(dt_referencia_p, 'month', 0));
		COMMIT;
	nr_ordenacao_w := nr_ordenacao_w + 1;
	-- dados
	OPEN C02;
	LOOP
	FETCH C02 INTO
		cd_unidade_w,
		nr_seq_interno_w,
		dt_referencia_w,
		dt_referencia_dia_w,
		nr_atendimento_w;
	EXIT WHEN C02%NOTFOUND;
		BEGIN
		IF	(nr_atendimento_w	> 0) THEN
			IF	(dt_referencia_dia_w = '01') THEN
				vl_dia01_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '02') THEN
				vl_dia02_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '03') THEN
				vl_dia03_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '04') THEN
				vl_dia04_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '05') THEN
				vl_dia05_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '06') THEN
				vl_dia06_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '07') THEN
				vl_dia07_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '08') THEN
				vl_dia08_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '09') THEN
				vl_dia09_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '10') THEN
				vl_dia10_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '11') THEN
				vl_dia11_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '12') THEN
				vl_dia12_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '13') THEN
				vl_dia13_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '14') THEN
				vl_dia14_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '15') THEN
				vl_dia15_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '16') THEN
				vl_dia16_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '17') THEN
				vl_dia17_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '18') THEN
				vl_dia18_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '19') THEN
				vl_dia19_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '20') THEN
				vl_dia20_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '21') THEN
				vl_dia21_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '22') THEN
				vl_dia22_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '23') THEN
				vl_dia23_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '24') THEN
				vl_dia24_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '25') THEN
				vl_dia25_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '26') THEN
				vl_dia26_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '27') THEN
				vl_dia27_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '28') THEN
				vl_dia28_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '29') THEN
				vl_dia29_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '30') THEN
				vl_dia30_w	:= 1;
			ELSIF	(dt_referencia_dia_w = '31') THEN
				vl_dia31_w	:= 1;
			END IF;
		END IF;






			INSERT INTO w_censo_validacao(  cd_Setor_Atendimento,
							ds_unidade,
							nr_ordenacao,
							ie_dia01,
							ie_dia02,
							ie_dia03,
							ie_dia04,
							ie_dia05,
							ie_dia06,
							ie_dia07,
							ie_dia08,
							ie_dia09,
							ie_dia10,
							ie_dia11,
							ie_dia12,
							ie_dia13,
							ie_dia14,
							ie_dia15,
							ie_dia16,
							ie_dia17,
							ie_dia18,
							ie_dia19,
							ie_dia20,
							ie_dia21,
							ie_dia22,
							ie_dia23,
							ie_dia24,
							ie_dia25,
							ie_dia26,
							ie_dia27,
							ie_dia28,
							ie_dia29,
							ie_dia30,
							ie_dia31,
							dt_referencia,
							nr_seq_interno)
						VALUES	(
							cd_setor_Atendimento_w,
							cd_unidade_w,
							1,
							vl_dia01_w,
							vl_dia02_w,
							vl_dia03_w,
							vl_dia04_w,
							vl_dia05_w,
							vl_dia06_w,
							vl_dia07_w,
							vl_dia08_w,
							vl_dia09_w,
							vl_dia10_w,
							vl_dia11_w,
							vl_dia12_w,
							vl_dia13_w,
							vl_dia14_w,
							vl_dia15_w,
							vl_dia16_w,
							vl_dia17_w,
							vl_dia18_w,
							vl_dia19_w,
							vl_dia20_w,
							vl_dia21_w,
							vl_dia22_w,
							vl_dia23_w,
							vl_dia24_w,
							vl_dia25_w,
							vl_dia26_w,
							vl_dia27_w,
							vl_dia28_w,
							vl_dia29_w,
							vl_dia30_w,
							vl_dia31_w,
							dt_referencia_w,
							nr_seq_interno_w);
				COMMIT;
			nr_ordenacao_w := nr_ordenacao_w + 1;
			vl_dia01_w 		:= 0;
			vl_dia02_w 		:= 0;
			vl_dia03_w		:= 0;
			vl_dia04_w 		:= 0;
			vl_dia05_w 		:= 0;
			vl_dia06_w 		:= 0;
			vl_dia07_w 		:= 0;
			vl_dia08_w 		:= 0;
			vl_dia09_w 		:= 0;
			vl_dia10_w 		:= 0;
			vl_dia11_w 		:= 0;
			vl_dia12_w 		:= 0;
			vl_dia13_w 		:= 0;
			vl_dia14_w 		:= 0;
			vl_dia15_w 		:= 0;
			vl_dia16_w 		:= 0;
			vl_dia17_w 		:= 0;
			vl_dia18_w 		:= 0;
			vl_dia19_w 		:= 0;
			vl_dia20_w 		:= 0;
			vl_dia21_w 		:= 0;
			vl_dia22_w 		:= 0;
			vl_dia23_w 		:= 0;
			vl_dia24_w 		:= 0;
			vl_dia25_w 		:= 0;
			vl_dia26_w 		:= 0;
			vl_dia27_w 		:= 0;
			vl_dia28_w 		:= 0;
			vl_dia29_w 		:= 0;
			vl_dia30_w 		:= 0;
			vl_dia31_w 		:= 0;
			END;
	END LOOP;
	CLOSE C02;
	END;
	nr_ordenacao_w := nr_ordenacao_w + 1;
END LOOP;
CLOSE C01;

COMMIT;

END ocup_gerar_validacao_censo;
/