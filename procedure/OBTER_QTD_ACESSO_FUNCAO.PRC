create or replace
procedure obter_qtd_acesso_funcao(
		nr_sequencia_p		number) is 

cd_funcao_w	number(10);
vl_inf_w	varchar2(100);
id_inf_w	varchar2(100);
cd_cnpj_w	varchar2(14);
			
Cursor C01 is
	select	cd_funcao 
	from	funcao 
	where	upper(nvl(ie_situacao,'I')) = 'A';
			
begin
if	(nr_sequencia_p is not null) then
	begin
	--funcaoo
	if	(nr_sequencia_p = 9) then
		begin
		open c01;
		loop
		fetch c01 into cd_funcao_w;
		exit when c01%notfound;
			begin
			
			select	count(*),
				null,
				obter_cgc_estabelecimento(obter_estabelecimento_ativo)
			into	vl_inf_w,
				id_inf_w,
				cd_cnpj_w
			from	log_acesso_funcao
			where	cd_funcao = cd_funcao_w;

			insert_inf_base_cliente(
				nr_sequencia_p,
				vl_inf_w,
				id_inf_w,
				cd_cnpj_w,
				null,
				'Tasy');
			
			end;
		end loop;
		close c01;
		end;
	end if;
	end;
end if;

commit;

end obter_qtd_acesso_funcao;
/