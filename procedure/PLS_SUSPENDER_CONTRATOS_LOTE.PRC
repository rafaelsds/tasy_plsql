create or replace
procedure pls_suspender_contratos_lote
			(	nr_seq_lote_p			number,
				nr_seq_motivo_p			number,
				ds_observacao_p			varchar2,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				ds_mensagem_p		out	varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Suspender o atendimento dos contratos do lote
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	OPS - Controle de notifica�oes de atraso
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_notific_pagador_w	number(10);
qt_nao_recebidas_w		number(10);
qt_liquidados_w			number(10);
nr_seq_regra_w			number(10);
qt_titulo_regra_w		number(10);
ie_titulos_suspensao_w		varchar2(2);
qt_titulo_pagador_w		number(10);
ds_pagador_w			varchar2(255);
ds_lista_pagador_w		varchar2(4000)	:= null;
ie_regulamentacao_plano_w	varchar2(2);
ie_suspensao_atend_w		varchar2(1);
nr_seq_pagador_w		pls_notificacao_pagador.nr_seq_pagador%type;
ds_nr_processo_w		varchar2(255);
ie_resc_sem_confirm_receb_w	pls_notificacao_regra.ie_exige_confirmacao_receb%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_notificacao_pagador x
	where	nr_seq_lote	= nr_seq_lote_p
	and	dt_suspensao is null
	and	(dt_recebimento_notif is not null or ie_resc_sem_confirm_receb_w = 'S')
	and	nvl(x.ie_suspensao,'S') = 'S'
	and	not exists	(select	1
				from	pls_notificacao_pagador a,
					pls_notificacao_item b,
					pls_mensalidade c,
					titulo_receber d	
				where	b.nr_seq_mensalidade	= c.nr_sequencia
				and	d.nr_seq_mensalidade	= c.nr_sequencia
				and	a.nr_sequencia		= b.nr_seq_notific_pagador
				and	a.nr_sequencia		= x.nr_sequencia
				and	d.dt_liquidacao	is not null
				and	d.ie_situacao	<> '6');
	
Cursor C02 is
	select	distinct
		pls_obter_dados_pagador(a.nr_seq_pagador,'N')	
	from	pls_notificacao_pagador a,
		pls_notificacao_item b,
		pls_mensalidade c,
		titulo_receber d	
	where	b.nr_seq_mensalidade	= c.nr_sequencia
	and	d.nr_seq_mensalidade	= c.nr_sequencia
	and	a.nr_sequencia		= b.nr_seq_notific_pagador
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	d.dt_liquidacao	is not null
	and	d.ie_situacao	<> '6';

begin

select	nvl(max(a.ie_exige_confirmacao_receb),'N')
into	ie_resc_sem_confirm_receb_w
from	pls_notificacao_regra a,
	pls_notificacao_lote b
where	a.nr_sequencia	= b.nr_seq_regra
and	b.nr_sequencia	= nr_seq_lote_p;

--Realiza a verifica��o de recebimentos do lote
select	count(*)
into	qt_nao_recebidas_w
from	pls_notificacao_pagador
where	nr_seq_lote	= nr_seq_lote_p
and	dt_suspensao is null;

if	(qt_nao_recebidas_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(180812);
end if;

select	count(*)
into	qt_nao_recebidas_w
from	pls_notificacao_pagador
where	nr_seq_lote	= nr_seq_lote_p
and	dt_recebimento_notif is not null
and	dt_suspensao is null;

if	(qt_nao_recebidas_w = 0) and (ie_resc_sem_confirm_receb_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(180813);
end if;

/* Realiza a verifica��o de t�tulos liquidados no lote que a situa��o N�O seja liquidado por perdas */
select	nvl(max(a.nr_sequencia), 0)
into	nr_seq_regra_w
from	pls_regra_suspensao a,
	pls_notificacao_regra b,
	pls_notificacao_lote c
where	a.nr_seq_regra_geracao	= b.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_regra
and	c.nr_sequencia		= nr_seq_lote_p
and	trunc(sysdate,'dd') between trunc(nvl(a.dt_inicio_vigencia,sysdate),'dd') and trunc(nvl(a.dt_fim_vigencia,sysdate),'dd');

if	(nr_seq_regra_w	> 0) then
	select	qt_titulo_aberto,
		ie_titulos_suspensao,
		ie_regulamentacao_plano
	into	qt_titulo_regra_w,
		ie_titulos_suspensao_w,
		ie_regulamentacao_plano_w
	from	pls_regra_suspensao
	where	nr_sequencia	= nr_seq_regra_w;
end if;

open C01;
loop
fetch C01 into	
	nr_seq_notific_pagador_w;
exit when C01%notfound;
	begin
	
	select  max(nr_seq_pagador)
	into	nr_seq_pagador_w
	from  	pls_notificacao_pagador 
	where  	nr_sequencia = nr_seq_notific_pagador_w;
	
	begin
	ie_suspensao_atend_w 	:= pls_obter_processo_jud_notif(nr_seq_lote_p, nr_seq_pagador_w, null, 'PSA');
	exception
	when others then
		ie_suspensao_atend_w := 'N';
	end;
	
	if 	(ie_suspensao_atend_w  = 'N') then
		ds_nr_processo_w := pls_obter_processo_jud_notif( nr_seq_lote_p, nr_seq_pagador_w, null, 'NRP');
		wheb_mensagem_pck.exibir_mensagem_abort(337228,'NR_PROCESSO_P=' ||ds_nr_processo_w);
	end if;
	
	if	(qt_titulo_regra_w > 0) then
		if	(ie_titulos_suspensao_w	= 'T') then	/* Considera todos os t�tulos do pagador, independente se est� ou n�o na notifica��o */
			select	count(*)
			into	qt_titulo_pagador_w
			from	pls_notificacao_pagador a,
				pls_contrato_pagador b,
				pls_mensalidade c,
				titulo_receber d	
			where	a.nr_seq_pagador	= b.nr_sequencia
			and	b.nr_sequencia		= c.nr_seq_pagador
			and	d.nr_seq_mensalidade	= c.nr_sequencia
			and	a.nr_sequencia		= nr_seq_notific_pagador_w
			and	d.ie_situacao		in ('1', '6');
		elsif	(ie_titulos_suspensao_w	= 'N') then	/* Considera apenas os t�tulos que est�o na notifica��o */
			select	count(*)
			into	qt_titulo_pagador_w
			from	pls_notificacao_pagador a,
				pls_notificacao_item b,
				pls_mensalidade c,
				titulo_receber d	
			where	b.nr_seq_mensalidade	= c.nr_sequencia
			and	d.nr_seq_mensalidade	= c.nr_sequencia
			and	a.nr_sequencia		= b.nr_seq_notific_pagador
			and	a.nr_sequencia		= nr_seq_notific_pagador_w
			and	d.ie_situacao		in ('1', '6');
		end if;
		
		if	((qt_titulo_pagador_w >= qt_titulo_regra_w)  and  (ie_suspensao_atend_w = 'S'))then
			pls_suspender_contrato_notific(	nr_seq_notific_pagador_w,
							nr_seq_motivo_p,
							ds_observacao_p,
							nr_seq_lote_p,
							nm_usuario_p,
							cd_estabelecimento_p);
		end if;
	elsif 	(ie_suspensao_atend_w = 'S') then
		pls_suspender_contrato_notific(	nr_seq_notific_pagador_w,
						nr_seq_motivo_p,
						ds_observacao_p,
						nr_seq_lote_p,
						nm_usuario_p,
						cd_estabelecimento_p);
	end if;
	
	if	(nr_seq_regra_w is not null) then
		update	pls_notificacao_pagador
		set	nr_seq_regra_susp	= nr_seq_regra_w
		where	nr_sequencia		= nr_seq_notific_pagador_w;
	end if;
	
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	ds_pagador_w;
exit when C02%notfound;
	begin
	if	(ds_lista_pagador_w is null) then
		ds_lista_pagador_w 	:= ds_pagador_w;
	else
		if	(length(ds_lista_pagador_w) > 100) then
			ds_lista_pagador_w	:= ds_lista_pagador_w || '...';
			exit;
		else
			ds_lista_pagador_w	:= ds_lista_pagador_w || ds_pagador_w;
		end if;
	end if;
	end;
end loop;
close C02;

if	(ds_lista_pagador_w is not null) then
	ds_mensagem_p	:= 'O seguintes pagadores n�o foram suspensos: ' || chr(13) || chr(10) || ds_lista_pagador_w || chr(13) || chr(10) ||
				'Motivo: H� t�tulos notificados que j� foram liquidados. Verifique.';
end if;

update	pls_notificacao_lote
set	dt_suspensao	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_lote_p;

pls_atualizar_valor_notific(nr_seq_lote_p);

commit;

end pls_suspender_contratos_lote;
/