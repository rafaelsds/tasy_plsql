create or replace
procedure consistir_apac_proc_eup_js(	nr_atendimento_p	number,
					nr_apac_p		number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nm_usuario_p		varchar2,
					cd_motivo_cobranca_p	number,
					cd_doenca_cid_p		varchar2,
					ds_msg_erro_p		out varchar2,
					ds_msg_confirma_p	out varchar2) is 

nr_atendimento_apac_w		number(10,0);
ds_msg_erro_w			varchar2(255);
ds_msg_confirma_w		varchar2(255);
ie_continua_exec_w		varchar2(1)	:= 'S';
ie_situacao_w			varchar2(1);
cd_estabelecimento_w		number(4);
ie_confirma_dt_ocorrencia_w	varchar2(1);
ie_permite_nova_apac_w		varchar2(1);
ie_nova_apac_w			varchar2(1);
			
begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

Obter_param_Usuario(1124, 23, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_confirma_dt_ocorrencia_w);
Obter_param_Usuario(1124, 26, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_permite_nova_apac_w);

if	(nvl(nr_atendimento_p,0) > 0)then
	begin
	
	select 	nvl(max(nr_atendimento),0)
	into	nr_atendimento_apac_w
	from    sus_apac_unif
	where   nr_apac  = nr_apac_p
	and     nr_atendimento <> nr_atendimento_p
	and     obter_pessoa_atendimento(nr_atendimento, 'C') <> obter_pessoa_atendimento(nr_atendimento_p, 'C');
	
	if	(nr_atendimento_apac_w <> 0)then
		begin
		
		ds_msg_erro_w		:= substr(obter_texto_dic_objeto(290095, wheb_usuario_pck.get_nr_seq_idioma, 'NR_ATENDIMENTO_APAC_W=' || nr_atendimento_apac_w),1,255);
		ie_continua_exec_w	:= 'N';
		end;
	end if;
	
	if	(ie_continua_exec_w = 'S')then
		begin
		
		select 	ie_situacao
		into	ie_situacao_w
		from 	procedimento 
		where 	cd_procedimento = cd_procedimento_p
		and 	ie_origem_proced = ie_origem_proced_p;
		
		
		if	(ie_situacao_w = 'I')then
			begin
			ie_continua_exec_w := 'N';
			ds_msg_erro_w	:= substr(obter_texto_tasy(290093,  wheb_usuario_pck.get_nr_seq_idioma),1,255);
			
			end;
		end if;
		
		end;
	end if;
	
	if	(ie_permite_nova_apac_w = 'C') and
		(ie_continua_exec_w = 'S') then
	
		ie_nova_apac_w := Sus_Permite_Nova_Apac(nr_atendimento_p,cd_doenca_cid_p);
		if	(ie_nova_apac_w = 'N') then
			ie_continua_exec_w := 'N';
			ds_msg_erro_w	:= substr(obter_texto_tasy(290104,  wheb_usuario_pck.get_nr_seq_idioma),1,255);
		end if;
	end if;
	
	if	(ie_continua_exec_w = 'S') and
		(ie_confirma_dt_ocorrencia_w = 'S') and
		(cd_motivo_cobranca_p in (11, 12 ,14 ,15 ,16 ,18, 26 ,31 ,41 ,42, 43, 51)) then
		ds_msg_confirma_w := substr(obter_texto_tasy(72847,  wheb_usuario_pck.get_nr_seq_idioma),1,255);
		ie_continua_exec_w := 'S';
	end if;
	
	end;
end if;

ds_msg_confirma_p := ds_msg_confirma_w;
ds_msg_erro_p := ds_msg_erro_w;

end consistir_apac_proc_eup_js;
/