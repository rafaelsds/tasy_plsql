create or replace 
procedure copy_adds_model_settings(	nr_seq_model_p		number,
					nr_atendimento_p	number,
					nm_usuario_p varchar) is

nr_sequencia_adds_sv_w  	NUMBER(10);   


cursor c01 is
	select	distinct nr_sequencia 
	from 	adds_sv 
	where 	nr_seq_modelo = nr_seq_model_p;

cursor C02 (	nr_sequencia_adds_sv_p number) is
	SELECT	*
	FROM 	adds_sv_item 
	WHERE 	nr_seq_adds_sv = nr_sequencia_adds_sv_p 
	AND 	ie_situacao='A' 
	and 	nr_atendimento is null;

begin


for r_c01 in c01 loop
	begin
	nr_sequencia_adds_sv_w := r_c01.nr_sequencia;
	
	delete 	from adds_sv_item
	where 	nr_atendimento = nr_atendimento_p
	and	nr_seq_adds_sv = nr_sequencia_adds_sv_w;
	commit;
	
	
	for r_c02 in c02(nr_sequencia_adds_sv_w) loop
		begin
		insert into adds_sv_item (nr_sequencia,
				nm_range,         
				nr_seq_adds_sv,
				ie_score,      
				nr_seq_apres,
				vl_minimo,         
				vl_maximo,        
				ie_valor,
				dt_atualizacao ,         
				nm_usuario,          
				dt_atualizacao_nrec,      
				nm_usuario_nrec ,          
				ie_situacao,
				nr_atendimento)       
		values 	(	adds_sv_item_seq.nextval,
				r_c02.nm_range,         
				nr_sequencia_adds_sv_w,
				r_c02.ie_score,      
				r_c02.nr_seq_apres,
				r_c02.vl_minimo,         
				r_c02.vl_maximo,        
				r_c02.ie_valor,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				r_c02.ie_situacao,
				nr_atendimento_p);
		end;
	end loop;
end;
end loop;

commit;

end copy_adds_model_settings;
/