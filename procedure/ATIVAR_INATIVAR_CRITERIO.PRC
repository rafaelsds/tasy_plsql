create or replace
procedure ativar_inativar_criterio(	ie_tabela_p		Varchar2,
				ie_situacao_p		Varchar2,
				nm_usuario_p		Varchar2,
				nr_sequencia_p		Number) is 

begin

if	(ie_tabela_p = 'P') then
	begin
	
	update	proc_criterio_repasse
	set	ie_situacao	= ie_situacao_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
	
	end;
elsif	(ie_tabela_p = 'M') then
	begin
	
	update	mat_criterio_repasse
	set	ie_situacao	= ie_situacao_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_sequencia_p;
		
	end;
end if;                

commit;

end ativar_inativar_criterio;
/
