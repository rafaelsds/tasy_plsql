create or replace
procedure pls_oc_cta_tratar_val_97_imp(	nr_seq_combinada_p	pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p	pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p	pls_oc_cta_selecao_imp.nr_id_transacao%type) is

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a compara��o entre prestadores. A combina��o pode ser entre os prestadores do protocolo, 
conta ou procedimento, Para cada  combina��o, pode-se selecionar o tipo do prestador a ser comparado.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
nr_cont_w		pls_integer;
tb_seq_selecao_w	pls_util_cta_pck.t_number_table;
tb_valido_w		pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w		pls_util_cta_pck.t_varchar2_table_4000;
ie_gera_ocorrencia_w	varchar2(1);
qt_registro_w		pls_integer;
nr_seq_prest_w		pls_prestador.nr_sequencia%type;
nr_seq_prest_comp_w	pls_prestador.nr_sequencia%type;
cd_prestador_w		pls_prestador.cd_prestador%type;
cd_prestador_comp_w	pls_prestador.cd_prestador%type;
ie_prestador_solic_util_w	pls_parametros.ie_prestador_solic_util%type;

-- Informa��es sobre a Regra
cursor c01 (	nr_seq_oc_cta_comb_pc	pls_oc_cta_combinada.nr_sequencia%type) is
	select	ie_valida_prest_info,
		ie_valida_prest_info_comp,
		ie_tipo_prestador,
		ie_tipo_prestador_comp,
		ie_comparacao,
		ie_campo
	from	pls_oc_cta_val_prest_dif
	where	nr_seq_oc_cta_comb = nr_seq_oc_cta_comb_pc;

-- Carrega os dados do prestador
cursor	c02 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type) is
	select	a.nr_sequencia nr_seq_selecao,
		c.nr_sequencia nr_seq_protocolo,
		b.nr_seq_prest_solic_conv nr_seq_prestador,
		b.cd_prest_solic_conv cd_prestador,
		b.nr_seq_prest_exec_conv nr_seq_prestador_exec,
		b.cd_prest_exec_conv cd_prestador_exec,
		c.nr_seq_prestador_conv nr_seq_prest_prot,
		c.cd_prestador_conv cd_prestador_prot,
		b.nr_seq_guia_conv nr_seq_guia
	from	pls_oc_cta_selecao_imp a,
		pls_conta_imp b,
		pls_protocolo_conta_imp c
	where	a.nr_id_transacao = nr_id_transacao_pc
	and	a.ie_valido = 'S'
	and	b.nr_sequencia = a.nr_seq_conta
	and	c.nr_sequencia = b.nr_seq_protocolo;

begin

if	(nr_seq_combinada_p is not null) then
	nr_cont_w := 0;
	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', ie_regra_excecao_p, nr_id_transacao_p, null);	
	-- Incializar as listas para cada regra.
	pls_ocor_imp_pck.limpar_nested_tables(tb_seq_selecao_w, tb_valido_w, tb_observacao_w);

	-- Carrega as regras
	for r_c01_w in c01 (nr_seq_combinada_p) loop

		-- S� executa a regra se ela n�o for de tipos de prestadores iguais
		if	(r_c01_w.ie_tipo_prestador <> r_c01_w.ie_tipo_prestador_comp) then
		
			--Somente se a regra est� para comparar com o prestador requisitante, ent�o nesse caso, faz as buscas 
			--baseado no que ocorre na popula��o dessa informa��o na tabela quente(l� ocorre via pls_conta_atual, por�m
			--aqui apenas buscar� na exeu��o da regra). 
			if (r_c01_w.ie_tipo_prestador_comp = 'R') then
				select	max(ie_prestador_solic_util)
				into	ie_prestador_solic_util_w
				from	table(pls_parametros_pck.f_retorna_param(wheb_usuario_pck.get_cd_estabelecimento));
			end if;
			
			-- abre o cursor com os prestadores a serem comparados
			for r_c02_w in c02 (nr_id_transacao_p) loop

				-- inicia como n�o gera ocorr�ncia
				ie_gera_ocorrencia_w := 'N';
				tb_observacao_w(nr_cont_w) := null;

				-- define qual � o prestador que precisa verificar
				case	(r_c01_w.ie_tipo_prestador)
					-- atendimento
					when 'A' then 					
						nr_seq_prest_w := r_c02_w.nr_seq_prest_prot;
						cd_prestador_w := r_c02_w.cd_prestador_prot;
					-- solicitante
					when 'S' then
						nr_seq_prest_w := r_c02_w.nr_seq_prestador;
						cd_prestador_w := r_c02_w.cd_prestador;					
					-- executor
					when 'E' then
						nr_seq_prest_w := r_c02_w.nr_seq_prestador_exec;
						cd_prestador_w := r_c02_w.cd_prestador_exec;					
					else
						nr_seq_prest_w := null;
						cd_prestador_w := null;
				end case;

				-- define qual prestador de compara��o
				case	(r_c01_w.ie_tipo_prestador_comp)
					-- atendimento
					when 'A' then 		
						nr_seq_prest_comp_w := r_c02_w.nr_seq_prest_prot;
						cd_prestador_comp_w := r_c02_w.cd_prestador_prot;
				
					-- solicitante
					when 'S' then
						nr_seq_prest_comp_w := r_c02_w.nr_seq_prestador;
						cd_prestador_comp_w := r_c02_w.cd_prestador;						
					-- executor
					when 'E' then
						nr_seq_prest_comp_w := r_c02_w.nr_seq_prestador_exec;
						cd_prestador_comp_w := r_c02_w.cd_prestador_exec;					
					-- Requisitante
					when 'R' then
											
						if	( ie_prestador_solic_util_w	= 'R') then
							select	max(c.nr_seq_prestador)
							into	nr_seq_prest_comp_w
							from	pls_requisicao c,
								pls_execucao_req_item a
							where	a.nr_seq_requisicao	= c.nr_sequencia
							and	a.nr_seq_guia		= r_c02_w.nr_seq_guia;
						end if;						
						if	(ie_prestador_solic_util_w	= 'G') or
							((ie_prestador_solic_util_w	= 'R') and
							(nr_seq_prest_comp_w	is null))then
							select	max(nr_seq_prestador)
							into	nr_seq_prest_comp_w
							from	pls_guia_plano
							where	nr_sequencia	= r_c02_w.nr_seq_guia;
						end if;					
						if	(ie_prestador_solic_util_w		= 'C') or
							((ie_prestador_solic_util_w		in ('G','R')) and
							(nr_seq_prest_comp_w	is null)) then
							nr_seq_prest_comp_w:= r_c02_w.nr_seq_prestador;
						end if;				
						cd_prestador_comp_w := pls_obter_cod_prestador(nr_seq_prest_comp_w, null);
					when 'G' then --executor da guia 
						select	max(nr_seq_prestador)
						into	nr_seq_prest_comp_w
						from	pls_guia_plano
						where	nr_sequencia	= r_c02_w.nr_seq_guia;
						
						cd_prestador_comp_w := pls_obter_cod_prestador(nr_seq_prest_comp_w, null);
					else
						nr_seq_prest_comp_w := null;
						cd_prestador_comp_w := null;
				end case;

				-- se obriga o prestador, e o mesmo n�o est� informado...
				if	(r_c01_w.ie_valida_prest_info = 'S') and
					(nr_seq_prest_w is null) then

					ie_gera_ocorrencia_w := 'S';
				end if;

				-- caso seja para validar o participante a valida��o � feita de uma outra forma
				if	(r_c01_w.ie_tipo_prestador_comp = 'P') and
					(ie_gera_ocorrencia_w = 'N') then

					-- se for para verificar a igualdade
					if	(r_c01_w.ie_comparacao = 'I') then

						-- verifica se existe algum participante diferente do prestador que precisa ser verificado						
						select	sum(qt)
						into	qt_registro_w
						from	(
							-- aqui retorna os que devem verificar a sequ�ncia
							select	count(1) qt
							from	pls_oc_cta_selecao_imp a,
								pls_conta_imp b,
								pls_conta_proc_imp c,
								pls_conta_item_equipe_imp d,
								pls_protocolo_conta_imp e
							where	r_c01_w.ie_campo = 'S'
							and	a.nr_id_transacao = nr_id_transacao_p
							and	a.ie_valido = 'S'
							and	b.nr_sequencia = a.nr_seq_conta
							and	c.nr_seq_conta = b.nr_sequencia
							and	d.nr_seq_conta_proc(+) = c.nr_sequencia
							and	e.nr_sequencia = b.nr_seq_protocolo
							and	e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and	d.nr_seq_prestador_conv = nr_seq_prest_w
							union all
							-- aqui retorna caso tenha que validar nulos
							select	count(1) qt
							from	pls_oc_cta_selecao_imp a,
								pls_conta_imp b,
								pls_conta_proc_imp c,
								pls_conta_item_equipe_imp d,
								pls_protocolo_conta_imp e
							where	a.nr_id_transacao = nr_id_transacao_p
							and	a.ie_valido = 'S'
							and	b.nr_sequencia = a.nr_seq_conta
							and	c.nr_seq_conta = b.nr_sequencia
							and	d.nr_seq_conta_proc(+) = c.nr_sequencia
							and	e.nr_sequencia = b.nr_seq_protocolo
							and	e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and	d.nr_seq_prestador_conv is null
							and	r_c01_w.ie_valida_prest_info_comp = 'S'
							union all
							-- aqui retorna quando deve validar o c�digo do prestador
							select	count(1) qt
							from	pls_oc_cta_selecao_imp a,
								pls_conta_imp b,
								pls_conta_proc_imp c,
								pls_conta_item_equipe_imp d,
								pls_protocolo_conta_imp e,
								pls_prestador f
							where	r_c01_w.ie_campo = 'C'
							and	a.nr_id_transacao = nr_id_transacao_p
							and	a.ie_valido = 'S'
							and	b.nr_sequencia = a.nr_seq_conta
							and	c.nr_seq_conta = b.nr_sequencia
							and	d.nr_seq_conta_proc(+) = c.nr_sequencia
							and	e.nr_sequencia = b.nr_seq_protocolo
							and	e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and	f.nr_sequencia = d.nr_seq_prestador_conv
							and	f.cd_prestador = cd_prestador_w
						);					
					else
						-- verifica se existe algum participante diferente do prestador que precisa ser verificado
						select	sum(qt)
						into	qt_registro_w
						from	(
							-- aqui retorna os que devem verificar a sequ�ncia
							select	count(1) qt
							from	pls_oc_cta_selecao_imp a,
								pls_conta_imp b,
								pls_conta_proc_imp c,
								pls_conta_item_equipe_imp d,
								pls_protocolo_conta_imp e
							where	r_c01_w.ie_campo = 'S'
							and	a.nr_id_transacao = nr_id_transacao_p
							and	a.ie_valido = 'S'
							and	b.nr_sequencia = a.nr_seq_conta
							and	c.nr_seq_conta = b.nr_sequencia
							and	d.nr_seq_conta_proc(+) = c.nr_sequencia
							and	e.nr_sequencia = b.nr_seq_protocolo
							and	e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and	d.nr_seq_prestador_conv != nr_seq_prest_w
							union all
							-- aqui retorna caso tenha que validar nulos
							select	count(1) qt
							from	pls_oc_cta_selecao_imp a,
								pls_conta_imp b,
								pls_conta_proc_imp c,
								pls_conta_item_equipe_imp d,
								pls_protocolo_conta_imp e
							where	a.nr_id_transacao = nr_id_transacao_p
							and	a.ie_valido = 'S'
							and	b.nr_sequencia = a.nr_seq_conta
							and	c.nr_seq_conta = b.nr_sequencia
							and	d.nr_seq_conta_proc(+) = c.nr_sequencia
							and	e.nr_sequencia = b.nr_seq_protocolo
							and	e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and	d.nr_seq_prestador_conv is null
							and	r_c01_w.ie_valida_prest_info_comp = 'S'
							union all
							-- aqui retorna quando deve validar o c�digo do prestador
							select	count(1) qt
							from	pls_oc_cta_selecao_imp a,
								pls_conta_imp b,
								pls_conta_proc_imp c,
								pls_conta_item_equipe_imp d,
								pls_protocolo_conta_imp e,
								pls_prestador f
							where	r_c01_w.ie_campo = 'C'
							and	a.nr_id_transacao = nr_id_transacao_p
							and	a.ie_valido = 'S'
							and	b.nr_sequencia = a.nr_seq_conta
							and	c.nr_seq_conta = b.nr_sequencia
							and	d.nr_seq_conta_proc(+) = c.nr_sequencia
							and	e.nr_sequencia = b.nr_seq_protocolo
							and	e.nr_sequencia = r_c02_w.nr_seq_protocolo
							and	f.nr_sequencia = d.nr_seq_prestador_conv
							and	f.cd_prestador != cd_prestador_w
						);
					end if;

					 -- se encontrou, registra a ocorrencia
					if	(qt_registro_w > 0) then
						ie_gera_ocorrencia_w := 'S';
					end if;
				-- Valida��o por prestador fornecedor
				elsif	(r_c01_w.ie_tipo_prestador_comp = 'F') and
					(ie_gera_ocorrencia_w = 'N') then
										
					-- se for para verificar a igualdade
					if	(r_c01_w.ie_comparacao = 'I') then
						select	sum(qt)
						into	qt_registro_w
						from	(
								-- aqui retorna os que devem verificar a sequ�ncia
								select	count(1) qt
								from	pls_oc_cta_selecao_imp a,
										pls_conta_imp b,
										pls_conta_mat_imp c,			
										pls_protocolo_conta_imp d
								where	r_c01_w.ie_campo = 'S'
								and	a.nr_id_transacao = nr_id_transacao_p
								and	a.ie_valido = 'S'
								and	b.nr_sequencia = a.nr_seq_conta
								and	c.nr_seq_conta = b.nr_sequencia		
								and	d.nr_sequencia = b.nr_seq_protocolo
								and	d.nr_sequencia = r_c02_w.nr_seq_protocolo
								and	c.nr_seq_fornec_mat_conv = nr_seq_prest_w
								union all
								-- aqui retorna caso tenha que validar nulos
								select	count(1) qt
								from	pls_oc_cta_selecao_imp a,
										pls_conta_imp b,
										pls_conta_mat_imp c,			
										pls_protocolo_conta_imp d
								where	a.nr_id_transacao = nr_id_transacao_p
								and	a.ie_valido = 'S'
								and	b.nr_sequencia = a.nr_seq_conta
								and	c.nr_seq_conta = b.nr_sequencia
								and	d.nr_sequencia = b.nr_seq_protocolo
								and	d.nr_sequencia = r_c02_w.nr_seq_protocolo
								and	c.nr_seq_fornec_mat_conv is null
								and	r_c01_w.ie_valida_prest_info_comp = 'S'
								union all
								-- aqui retorna quando deve validar o c�digo do prestador
								select	count(1) qt
								from	pls_oc_cta_selecao_imp a,
										pls_conta_imp b,
										pls_conta_mat_imp c,			
										pls_protocolo_conta_imp d,
										pls_prestador e
								where	r_c01_w.ie_campo = 'C'
								and	a.nr_id_transacao = nr_id_transacao_p
								and	a.ie_valido = 'S'
								and	b.nr_sequencia = a.nr_seq_conta
								and	c.nr_seq_conta = b.nr_sequencia
								and	d.nr_sequencia = b.nr_seq_protocolo
								and	d.nr_sequencia = r_c02_w.nr_seq_protocolo
								and	e.nr_sequencia = c.nr_seq_fornec_mat_conv
								and	e.cd_prestador = cd_prestador_w
						);
					else
						select	sum(qt)
							into	qt_registro_w
							from	(
									-- aqui retorna os que devem verificar a sequ�ncia
									select	count(1) qt
									from	pls_oc_cta_selecao_imp a,
											pls_conta_imp b,
											pls_conta_mat_imp c,			
											pls_protocolo_conta_imp d
									where	r_c01_w.ie_campo = 'S'
									and		a.nr_id_transacao = nr_id_transacao_p
									and		a.ie_valido = 'S'
									and		b.nr_sequencia = a.nr_seq_conta
									and		c.nr_seq_conta = b.nr_sequencia		
									and		d.nr_sequencia = b.nr_seq_protocolo
									and		d.nr_sequencia = r_c02_w.nr_seq_protocolo
									and		c.nr_seq_fornec_mat_conv != nr_seq_prest_w
									union all
									-- aqui retorna caso tenha que validar nulos
									select	count(1) qt
									from	pls_oc_cta_selecao_imp a,
											pls_conta_imp b,
											pls_conta_mat_imp c,		
											pls_protocolo_conta_imp d
									where	a.nr_id_transacao = nr_id_transacao_p
									and		a.ie_valido = 'S'
									and		b.nr_sequencia = a.nr_seq_conta
									and		c.nr_seq_conta = b.nr_sequencia		
									and		d.nr_sequencia = b.nr_seq_protocolo
									and		d.nr_sequencia = r_c02_w.nr_seq_protocolo
									and		c.nr_seq_fornec_mat_conv is null
									and		r_c01_w.ie_valida_prest_info_comp = 'S'
									union all
									-- aqui retorna quando deve validar o c�digo do prestador
									select	count(1) qt
									from	pls_oc_cta_selecao_imp a,
											pls_conta_imp b,
											pls_conta_mat_imp c,			
											pls_protocolo_conta_imp d,
											pls_prestador e
									where	r_c01_w.ie_campo = 'C'
									and		a.nr_id_transacao = nr_id_transacao_p
									and		a.ie_valido = 'S'
									and		b.nr_sequencia = a.nr_seq_conta
									and		c.nr_seq_conta = b.nr_sequencia		
									and		d.nr_sequencia = b.nr_seq_protocolo
									and		d.nr_sequencia = r_c02_w.nr_seq_protocolo
									and		e.nr_sequencia = c.nr_seq_fornec_mat_conv
									and		e.cd_prestador != cd_prestador_w
							);
					end if;
					
					 -- se encontrou, registra a ocorrencia
					if	(qt_registro_w > 0) then
						ie_gera_ocorrencia_w := 'S';
					end if;

				elsif	(ie_gera_ocorrencia_w = 'N') then
					-- se obriga o prestador, e o mesmo n�o est� informado... Prestador da compara��o
					if	(r_c01_w.ie_valida_prest_info_comp = 'S') and
						(nr_seq_prest_comp_w is null) then

						ie_gera_ocorrencia_w := 'S';
					end if;

					-- se for para validar a sequ�ncia
					if	(r_c01_w.ie_campo = 'C') then

						-- se for para validar igualdade
						if	(r_c01_w.ie_comparacao = 'I') then

							-- se os prestadores forem iguais gera a ocorr�ncia
							-- caso n�o seja para verificar se o prestador est� informado corretamente e algum deles for nulo, 
							-- n�o deve gerar a ocorr�ncia, para tratar os prestadores nulos deve ser marcado algum ou ambos os checkbox
							if	(cd_prestador_comp_w = cd_prestador_w) then

								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							-- se os prestadores forem diferentes gera a ocorr�ncia
							if	(cd_prestador_comp_w != cd_prestador_w) then

								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					else
						-- se for para validar igualdade
						if	(r_c01_w.ie_comparacao = 'I') then

							-- se os prestadores forem iguais gera a ocorr�ncia
							-- caso n�o seja para verificar se o prestador est� informado corretamente e algum deles for nulo, 
							-- n�o deve gerar a ocorr�ncia, para tratar os prestadores nulos deve ser marcado algum ou ambos os checkbox
							if	(nr_seq_prest_comp_w = nr_seq_prest_w) then

								ie_gera_ocorrencia_w := 'S';
							end if;
						else
							-- se os prestadores forem diferentes gera a ocorr�ncia
							if	(nr_seq_prest_comp_w != nr_seq_prest_w) then

								ie_gera_ocorrencia_w := 'S';
							end if;
						end if;
					end if;
				end if;
				
				if	(ie_gera_ocorrencia_w = 'S') then

					tb_valido_w(nr_cont_w) := 'S';
					tb_seq_selecao_w(nr_cont_w) := r_C02_w.nr_seq_selecao;

					tb_observacao_w	(nr_cont_w) := 'O prestador ' ||					
											case 	r_c01_w.ie_tipo_prestador
												when 'A' then 'do protocolo '
												when 'S' then 'solicitante '
												when 'E' then 'executor '
												when 'P' then 'participante ' 
											end 
											|| '(' ||
											case r_c01_w.ie_campo 
												when 'S' then 'seq. ' || nr_seq_prest_w
												when 'C' then 'c�d. ' || cd_prestador_w
											end
											|| ') ' ||
											case r_c01_w.ie_comparacao
												when 'I' then '� igual ao prestador '
												when 'D' then '� diferente do prestador '
											end ||
											case 	r_c01_w.ie_tipo_prestador_comp
												when 'A' then 'do protocolo '
												when 'S' then 'solicitante '
												when 'E' then 'executor '
												when 'P' then 'participante '
												when 'R' then 'requisitante '
												when 'F' then 'Fornecedor '
												when 'G' then 'da guia'
											end 
											|| '(' || 
											case r_c01_w.ie_campo 
												when 'S' then 'seq. ' || nr_seq_prest_comp_w
												when 'C' then 'c�d. ' || cd_prestador_comp_w
											end || ')';

					if	(nr_cont_w >= pls_util_pck.qt_registro_transacao_w) then

						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w, nr_id_transacao_p,'SEQ');
							
							nr_cont_w := 0;
							-- Incializar as listas para cada regra.
							pls_ocor_imp_pck.limpar_nested_tables(tb_seq_selecao_w, tb_valido_w, tb_observacao_w);
					else
						nr_cont_w := nr_cont_w + 1;
					end if;	
				end if;
			end loop; -- Prestadores a serem comparados por regra

			/*Lan�a as glosas caso existir registros que n�o foram gerados*/
			if	(nr_cont_w > 0)	then
				pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w, tb_observacao_w, nr_id_transacao_p,'SEQ');
			end if;
		end if; -- fim tipo prestador diferente
	end loop; -- regras

	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', ie_regra_excecao_p, null, nr_id_transacao_p, null);
end if;

end pls_oc_cta_tratar_val_97_imp;
/
