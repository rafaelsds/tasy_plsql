create or replace procedure desfazer_item_controle(	nr_cirurgia_p		number,
													cd_material_p		number,
													nr_seq_item_cme_p	number,
													nm_usuario_p		varchar2 ) is

begin

if	(nr_cirurgia_p > 0) and 
	((cd_material_p > 0) or (nr_seq_item_cme_p > 0)) then
	update	cirurgia_item_controle
	set	ie_acao		= 5,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= (	select	max(nr_sequencia)
					from	cirurgia_item_controle
					where	nr_cirurgia	=	nr_cirurgia_p
					and		((cd_material	=	cd_material_p) or (nr_seq_item_cme = nr_seq_item_cme_p))
					and		ie_acao <> 5);
end if;

commit;

end desfazer_item_controle;
/
