create or replace
procedure pls_gerar_tit_trib_lote_pag(	nr_seq_pag_prest_p	pls_pagamento_prestador.nr_sequencia%type,
					nr_seq_venc_trib_p	pls_pag_prest_venc_trib.nr_sequencia%type,
					vl_tributo_p		number,
					cd_moeda_p		number,	
					vl_base_calculo_p	number,
					cd_cgc_prestador_p	varchar2,
					cd_pf_prestador_p	varchar2,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					cd_tributo_p		tributo.cd_tributo%type,
					dt_vencimento_p		date,
					dt_emissao_p		date,
					nm_usuario_p		usuario.nm_usuario%type) is 
	
ds_irrelevante_w		varchar2(4000);
cd_benef_regra_w		varchar2(255);
ie_conta_contab_tit_trib_w	varchar2(255);
ie_conta_financ_tit_trib_w	varchar2(255);
cd_darf_w			varchar2(255);
cd_conta_cred_w			varchar2(20);
cd_conta_contab_trib_w		varchar2(20);
ie_tipo_contratacao_w		varchar2(2);
nr_titulo_w			titulo_pagar.nr_titulo%type;
nr_seq_classe_w			number(15);
cd_tipo_baixa_neg_w		number(15);
cd_conta_financ_regra_w		number(15);
cd_conta_financ_w		number(15);
nr_seq_trans_reg_w		number(15);
nr_seq_trans_baixa_w		number(15);
cd_empresa_w			number(15);
nr_seq_regra_w			number(15);
nr_seq_tipo_prestador_w		pls_tipo_prestador.nr_sequencia%type;
nr_seq_classificacao_w		pls_prestador.nr_seq_classificacao%type;
qt_registros_w			pls_integer;
	
begin
-- busca o tipo de contrata��o
if	(nr_seq_venc_trib_p is not null) then

	select	max(a.ie_tipo_contratacao)
	into	ie_tipo_contratacao_w
	from	pls_pag_prest_venc_trib a
	where	a.nr_sequencia = nr_seq_venc_trib_p;
end if;

-- busca o tipo de prestador
if	(nr_seq_pag_prest_p is not null) then

	select	max(b.nr_seq_tipo_prestador),
		max(b.nr_seq_classificacao)
	into	nr_seq_tipo_prestador_w,
		nr_seq_classificacao_w
	from	pls_pagamento_prestador a,
		pls_prestador		b
	where	b.nr_sequencia	= a.nr_seq_prestador
	and	a.nr_sequencia	= nr_seq_pag_prest_p;
end if;	

-- obtem os dados do titulo
obter_dados_trib_tit_pagar(	cd_tributo_p,
				cd_estabelecimento_p,
				cd_cgc_prestador_p,
				cd_pf_prestador_p,
				cd_benef_regra_w,
				ds_irrelevante_w,
				ds_irrelevante_w,
				cd_conta_financ_regra_w,
				nr_seq_trans_reg_w,
				nr_seq_trans_baixa_w,
				ds_irrelevante_w,
				ds_irrelevante_w,
				ds_irrelevante_w,
				ds_irrelevante_w,
				ds_irrelevante_w,
				cd_darf_w,
				sysdate,
				ds_irrelevante_w,
				ds_irrelevante_w,
				'N',
				null,
				null,
				null,
				ie_tipo_contratacao_w,
				null,
				nr_seq_regra_w,
				null,
				0,
				nr_seq_classe_w,
				cd_tipo_baixa_neg_w,
				vl_base_calculo_p,
				'S',
				null,
				null,
				nr_seq_tipo_prestador_w,
				nr_seq_classificacao_w);

-- se encontrou algum benefici�rio na regra ent�o da continuidade, sen�o encontrou n�o gera do t�tulo
if	(cd_benef_regra_w is not null) then

	select	count(1)
	into	qt_registros_w
	from	titulo_pagar
	where	nr_seq_pls_pag_prest	= nr_seq_pag_prest_p
	and	nr_seq_pls_venc_trib	= nr_seq_venc_trib_p
	and	cd_tributo		= cd_tributo_p
	and	ie_situacao		= 'A';
	
	-- N�o deixar gerar o t�tulo do tributo duplicado
	if	(qt_registros_w = 0) then

		select	nvl(max(ie_conta_contab_tit_trib),'T'),
			nvl(max(ie_conta_financ_tit_trib),'T')
		into	ie_conta_contab_tit_trib_w,
			ie_conta_financ_tit_trib_w
		from	parametros_contas_pagar
		where 	cd_estabelecimento = cd_estabelecimento_p;	
		
		if	(ie_conta_financ_tit_trib_w = 'R') then
		
			cd_conta_financ_w := cd_conta_financ_regra_w;
		end if;
		
		if	(ie_conta_contab_tit_trib_w = 'R') then
		
			select	max(cd_empresa)
			into	cd_empresa_w
			from	estabelecimento	
			where	cd_estabelecimento = cd_estabelecimento_p;
			
			cd_conta_contab_trib_w := substr(obter_conta_contabil_trib(cd_empresa_w, cd_tributo_p, cd_cgc_prestador_p, sysdate),1,20);
		end if;	

		insert into titulo_pagar(
			nr_titulo, cd_estabelecimento, dt_atualizacao,
			nm_usuario, dt_emissao, dt_contabil,
			dt_vencimento_original, dt_vencimento_atual, vl_titulo, 	
			vl_saldo_titulo, vl_saldo_juros, vl_saldo_multa, 
			cd_moeda, tx_juros, tx_multa,
			cd_tipo_taxa_juro, cd_tipo_taxa_multa, tx_desc_antecipacao,
			ie_situacao, ie_origem_titulo, ie_tipo_titulo,
			cd_cgc, ie_desconto_dia, nr_lote_contabil,
			nr_seq_trans_fin_contab, nr_seq_trans_fin_baixa, ie_status_tributo,
			nr_lote_transf_trib, nr_seq_classe, cd_tipo_baixa_neg,
			ie_status, cd_tributo, nr_seq_pls_pag_prest,
			nr_seq_pls_venc_trib, ds_observacao_titulo, cd_darf
		) values (
			titulo_pagar_seq.nextval, cd_estabelecimento_p, sysdate,
			nm_usuario_p, nvl(dt_emissao_p,trunc(sysdate,'dd')), nvl(dt_emissao_p,trunc(sysdate,'dd')),
			dt_vencimento_p, dt_vencimento_p, vl_tributo_p,
			vl_tributo_p, 0, 0, 
			cd_moeda_p, 0, 0, 
			1, 1, 0,
			'A', 4, 4, -- Situa��o Aberto / 4- Imposto, 3- Repasse (quando origem for repasse) / Imposto 
			cd_benef_regra_w, 'N', 0,
			nr_seq_trans_reg_w, nr_seq_trans_baixa_w, 'NT',
			0, nr_seq_classe_w, cd_tipo_baixa_neg_w,
			'D', cd_tributo_p, nr_seq_pag_prest_p,
			nr_seq_venc_trib_p, substr(obter_nome_pf_pj(cd_pf_prestador_p, cd_cgc_prestador_p), 1,80), cd_darf_w
		) returning nr_titulo into nr_titulo_w;
			
		atualizar_inclusao_tit_pagar(nr_titulo_w, nm_usuario_p);
		
		select	max(c.cd_conta_cred)
		into	cd_conta_cred_w
		from	pls_conta_proc c,
			pls_conta_medica_resumo b,
			pls_pagamento_item a
		where	a.nr_seq_pagamento	= nr_seq_pag_prest_p
		and	a.nr_sequencia		= b.nr_seq_pag_item
		and	b.nr_seq_item		= c.nr_sequencia
		and	b.ie_proc_mat		= 'P'
		and	((b.ie_situacao is null) or (b.ie_situacao != 'I'));
		
		insert into titulo_pagar_classif(
			nr_titulo, nr_sequencia, vl_titulo,
			dt_atualizacao, nm_usuario, cd_conta_contabil, 
			cd_centro_custo, nr_seq_conta_financ
		) values(
			nr_titulo_w, 1, vl_tributo_p,
			sysdate, nm_usuario_p, nvl(cd_conta_contab_trib_w,cd_conta_cred_w), 
			null, cd_conta_financ_w);
	end if;
end if;
/* Sem commit */

end pls_gerar_tit_trib_lote_pag;
/