create or replace
procedure pls_consistir_mens_reajuste
			(	ie_tipo_lote_p		in	varchar2,
				dt_referencia_p		in	date,
				cd_estabelecimento_p	in	number,
				ds_erro_p		out	varchar2,
				ds_erro_2p		out	varchar2) is 

ds_lotes_w			varchar2(255);
qt_lote_individual_w		number(10);
qt_lote_coletivo_w		number(10);
qt_nao_liberado_individual_w	number(10);
ds_nao_liberado_individual_w	varchar2(255);
ds_nao_liberado_coletivo_w	varchar2(255);
qt_nao_liberado_coletivo_w	number(10);
ds_erro_w			varchar2(4000);

begin
ds_lotes_w	:= null;

if	(pls_obter_se_item_lista(ie_tipo_lote_p,'I') = 'S') then
	select	count(*)
	into	qt_lote_individual_w
	from	pls_reajuste
	where	trunc(dt_reajuste,'month')	= trunc(dt_referencia_p,'month')
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_status		= '2'
	and	ie_tipo_reajuste	= 'I';

	if	(qt_lote_individual_w = 0) then
		if	(ds_lotes_w is null) then
			ds_lotes_w	:= wheb_mensagem_pck.get_texto(280797); -- Individual/familiar
		else
			ds_lotes_w	:= ds_lotes_w || ', ' || wheb_mensagem_pck.get_texto(280797);
		end if;
	end if;
	
	select	count(*)
	into	qt_nao_liberado_individual_w
	from	pls_reajuste		a,
		pls_segurado_preco	b
	where	b.nr_seq_lote_reajuste	= a.nr_sequencia
	and	trunc(a.dt_reajuste,'month')	= trunc(dt_referencia_p,'month')
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.dt_liberacao is null
	and	a.ie_tipo_reajuste	= 'I';
	
	if	(qt_nao_liberado_individual_w > 0) then
		ds_nao_liberado_individual_w	:= wheb_mensagem_pck.get_texto(280798, 'QT_NAO_LIBERADO_P=' || qt_nao_liberado_individual_w);
	end if;
end if;

if	(pls_obter_se_item_lista(ie_tipo_lote_p,'C') = 'S') then
	select	count(*)
	into	qt_lote_coletivo_w
	from	pls_reajuste
	where	trunc(dt_reajuste,'month')	= trunc(dt_referencia_p,'month')
	and	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_status		= '2'
	and	ie_tipo_reajuste	= 'C';

	if	(qt_lote_coletivo_w = 0) then
		if	(ds_lotes_w is null) then
			ds_lotes_w	:= wheb_mensagem_pck.get_texto(280799); -- Coletivo
		else
			ds_lotes_w	:= ds_lotes_w || ', ' || wheb_mensagem_pck.get_texto(280799);
		end if;
	end if;
	
	select	count(*)
	into	qt_nao_liberado_coletivo_w
	from	pls_reajuste		a,
		pls_lote_reaj_segurado	b,
		pls_segurado_preco	c
	where	b.nr_seq_reajuste	= a.nr_sequencia
	and	c.nr_seq_lote		= b.nr_sequencia
	and	trunc(a.dt_reajuste,'month')	= trunc(dt_referencia_p,'month')
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	c.dt_liberacao is null
	and	a.ie_tipo_reajuste	= 'C';
	
	if	(qt_nao_liberado_coletivo_w > 0) then
		ds_nao_liberado_coletivo_w	:= wheb_mensagem_pck.get_texto(280800, 'QT_NAO_LIBERADO_P=' || qt_nao_liberado_coletivo_w);
	end if;
end if;

if	(ds_lotes_w	is not null) then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(280803, 'DS_LOTES_P=' || ds_lotes_w ||
								';DT_REFERENCIA_P=' || to_char(dt_referencia_p,'mm/yyyy'));
else
	ds_erro_p	:= null;
end if;

ds_erro_w	:= ds_erro_p||ds_nao_liberado_individual_w||ds_nao_liberado_coletivo_w;

ds_erro_p	:= substr(ds_erro_w,1,255);
ds_erro_2p	:= substr(ds_erro_w,256,255);

commit;

end pls_consistir_mens_reajuste;
/
