create or replace procedure alterar_nr_seq_mod_licenc (
		nr_sequencia_p	number,
		nr_seq_mod_licenc_p		number) is

begin
if	(nr_seq_mod_licenc_p is not null) then
	begin
	update	com_cli_neg_lic_item
	set	nr_seq_mod_licenc = nr_seq_mod_licenc_p
	where	nr_sequencia = nr_sequencia_p;
	end;
end if;
commit;
end alterar_nr_seq_mod_licenc;
/