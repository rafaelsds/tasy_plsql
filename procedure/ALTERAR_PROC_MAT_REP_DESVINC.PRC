create or replace
procedure alterar_proc_mat_rep_desvinc(
			ie_proc_mat_p		number,
			nr_seq_p		number,
			nr_repasse_terceiro_p	number,
			nr_sequencia_p		number,
			nm_usuario_p		varchar2) is 
begin

if	(ie_proc_mat_p = 0) then

	update	procedimento_repasse
	set	nr_repasse_terceiro	= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_seq_procedimento     = nr_seq_p
	and	nr_repasse_terceiro	= nr_repasse_terceiro_p
	and	nr_sequencia		= nr_sequencia_p;

elsif	(ie_proc_mat_p = 1) then

	update	material_repasse
	set	nr_repasse_terceiro	= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_seq_material		= nr_seq_p
	and	nr_repasse_terceiro	= nr_repasse_terceiro_p
	and	nr_sequencia		= nr_sequencia_p;

end if;

commit;

end alterar_proc_mat_rep_desvinc;
/
