create or replace
procedure ctb_pls_contab_rec_desc
			(	nr_lote_contabil_p	in	number,
				nm_usuario_p		in	varchar2,
				ie_exclusao_p		in	varchar2,
				ds_retorno_p  		in out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dt_referencia_w			date;
dt_referencia_titulo_w		date;
dt_referencia_fm_w		date;
cd_estabelecimento_w		number(4);
nr_seq_lote_comissao_w		number(10);
nr_seq_regra_cc_w		number(10);
cd_centro_custo_w		number(8);
nr_lote_contabil_w		number(10);
nr_titulo_w			titulo_receber.nr_titulo%type;
nr_seq_w_movto_cont_w		number(10);
ie_centro_custo_w		varchar2(1);
ie_debito_credito_w		varchar2(2);
vl_retorno_w			varchar2(2000);
ds_conteudo_w			varchar2(4000);
vl_diferenca_w			number(15,2);
cd_conta_contabil_w		varchar2(20);
cd_historico_w			number(10);
nr_seq_info_ctb_w		informacao_contabil.nr_sequencia%type;
nm_tabela_w			w_movimento_contabil.nm_tabela%type;
nm_atributo_w			w_movimento_contabil.nm_atributo%type;
nr_seq_item_w			w_movimento_contabil.nr_seq_tab_orig%type;
nm_agrupador_w			varchar2(255);	
nr_seq_agrupamento_w		number(10,0);

Cursor c_lote_disc is
	select	nr_sequencia
	from	pls_desc_lote_comissao
	where	nr_lote_contabil = nr_lote_contabil_p
	and	nr_seq_empresa is not null;

Cursor c_cobranca is
	select	'C',
		nvl(b.vl_cobranca,0) - nvl(a.vl_cobranca,0),
		cd_conta_cred,
		cd_historico_cred,
		'TITULO_RECEBER_COBR' nm_tabela,
		'VL_COBRANCA' nm_atributo,
		b.nr_sequencia,
		43 nr_seq_info_ctb,
		b.nr_titulo
	from	pls_desc_comissao	a,
		titulo_receber_cobr	b
	where	a.nr_seq_titulo_cobr	= b.nr_sequencia
	and	nr_seq_lote = nr_seq_lote_comissao_w
	union all
	select	'D',
		nvl(b.vl_cobranca,0) - nvl(a.vl_cobranca,0),
		cd_conta_deb,
		cd_historico_deb,
		'TITULO_RECEBER_COBR' nm_tabela,
		'VL_COBRANCA' nm_atributo,
		b.nr_sequencia,
		43 nr_seq_info_ctb,
		b.nr_titulo
	from	pls_desc_comissao	a,
		titulo_receber_cobr	b
	where	a.nr_seq_titulo_cobr	= b.nr_sequencia
	and	nr_seq_lote = nr_seq_lote_comissao_w;

cursor c_movimento is
	select	rowid
	from	w_movimento_contabil
	where	nr_lote_contabil = nr_lote_contabil_p;
	
cursor c_movimento_ctb is
	select	rowid
	from	movimento_contabil
	where	nr_lote_contabil = nr_lote_contabil_p;
	
begin
select	dt_referencia,
	cd_estabelecimento,
	nr_lote_contabil
into 	dt_referencia_w,
	cd_estabelecimento_w,
	nr_lote_contabil_w
from 	lote_contabil
where 	nr_lote_contabil = nr_lote_contabil_p;

dt_referencia_w := trunc(dt_referencia_w,'month');
dt_referencia_fm_w := fim_dia(fim_mes(dt_referencia_w));

if	(ie_exclusao_p = 'S') then
	begin
	wheb_usuario_pck.set_ie_lote_contabil('S');
	
	for reg in c_movimento
		loop    
		delete	from w_movimento_contabil
		where   rowid	= reg.rowid;
		end loop;

	for reg_ctb in c_movimento_ctb
		loop    
		delete	from movimento_contabil
		where   rowid	= reg_ctb.rowid;
		end loop;
		
	update	lote_contabil
	set	vl_credito = 0,
		vl_debito  = 0
	where	nr_lote_contabil = nr_lote_contabil_p;
	
	commit;
	
	update	pls_desc_lote_comissao
	set	nr_lote_contabil = 0
	where	nr_lote_contabil = nr_lote_contabil_p;
	
	commit;
	
	wheb_usuario_pck.set_ie_lote_contabil('N');
	end;
else
	begin
	wheb_usuario_pck.set_ie_lote_contabil('S');
	
	for reg in c_movimento
		loop    
		delete	from w_movimento_contabil
		where   rowid		= reg.rowid;
		end loop;
	
	update	pls_desc_lote_comissao
	set		nr_lote_contabil = nr_lote_contabil_p
	where	nvl(nr_lote_contabil,0)	= 0
  	and		cd_estabelecimento = cd_estabelecimento_w
	and		nr_seq_empresa is not null
	and		dt_referencia between dt_referencia_w and dt_referencia_fm_w;
	
	commit;
	
	wheb_usuario_pck.set_ie_lote_contabil('N');
	
	nr_seq_w_movto_cont_w := 0;
	
	nm_agrupador_w := nvl(trim(obter_agrupador_contabil(28)),'NR_SEQ_COBRANCA');
	
	open c_lote_disc;
	loop
	fetch c_lote_disc into
		nr_seq_lote_comissao_w;
	exit when c_lote_disc%notfound;
		begin
		open c_cobranca;
		loop
		fetch c_cobranca into
			ie_debito_credito_w,
			vl_diferenca_w,
			cd_conta_contabil_w,
			cd_historico_w,
			nm_tabela_w,
			nm_atributo_w,
			nr_seq_item_w,
			nr_seq_info_ctb_w,
			nr_titulo_w;
		exit when c_cobranca%notfound;
			begin
			cd_centro_custo_w := null;
			
			if (nm_agrupador_w = 'NR_SEQ_COBRANCA') then
				nr_seq_agrupamento_w	:=	nr_seq_item_w;
			elsif (nm_agrupador_w = 'NR_TITULO_RECEBER')then
				nr_seq_agrupamento_w	:=	nr_titulo_w;
			end if;
			
			if (nvl(nr_seq_agrupamento_w,0) = 0)then
				nr_seq_agrupamento_w	:=	nr_seq_item_w;
			end if;
			
			if (nvl(cd_conta_contabil_w,'X') <> 'X') then
				select	ie_centro_custo
				into	ie_centro_custo_w
				from	conta_contabil
				where	cd_conta_contabil = cd_conta_contabil_w;
			end if;
			
			if 	(ie_centro_custo_w = 'S') then
				pls_obter_centro_custo(	'R',
							null,
							cd_estabelecimento_w,
							'',
							'',
							'', 
							'',
							'',
							cd_centro_custo_w, 
							nr_seq_regra_cc_w);
			end if;
			
			nr_seq_w_movto_cont_w := nr_seq_w_movto_cont_w + 1;
			
			insert into w_movimento_contabil 
				(nr_lote_contabil,
				nr_sequencia,
				cd_conta_contabil,
				ie_debito_credito,
				cd_historico,
				dt_movimento,
				vl_movimento,
				cd_estabelecimento,
				cd_centro_custo,
				ds_compl_historico,
				nr_seq_agrupamento,
				nr_seq_tab_orig,
				nr_seq_info,
				nm_tabela,
				nm_atributo)
			values (nr_lote_contabil_p,
				nr_seq_w_movto_cont_w,
				cd_conta_contabil_w,
				ie_debito_credito_w,
				cd_historico_w,
				dt_referencia_w,
				vl_diferenca_w,
				cd_estabelecimento_w,
				cd_centro_custo_w,
				null,
				nr_seq_agrupamento_w,
				nr_seq_item_w,
				nr_seq_info_ctb_w,
				nm_tabela_w,
				nm_atributo_w);
			end;
		end loop;
		close c_cobranca;
		end;
	end loop;
	close c_lote_disc;
	
	agrupa_movimento_contabil(	nr_lote_contabil_p,
					nm_usuario_p); 
	end;
end if;

if   	(ds_retorno_p is null) then
	begin
	update	lote_contabil
	set	ie_situacao		= 'A',
		dt_geracao_lote		= decode(ie_exclusao_p,'N',sysdate,'S',null)
	where	nr_lote_contabil 		= nr_lote_contabil_p;

	if	(ie_exclusao_p = 'S') then
		ds_retorno_p	:= wheb_mensagem_pck.get_texto(298780,null);  /*'Exclus�o do Lote Ok';*/
	else
		ds_retorno_p	:= wheb_mensagem_pck.get_texto(298781,null);  /*'Gera��o do Lote Ok';*/
	end if;

	commit;
	end;
else
	rollback;
end if;

end ctb_pls_contab_rec_desc;
/