create or replace 
procedure gerar_movto_inv_direto_consig(
		nm_usuario_p			varchar2,
		cd_estabelecimento_p 		number,
		nr_documento_p			number,
		cd_fornecedor_p			varchar2,
		dt_mesano_referencia_p	date default sysdate,
		dt_movimento_estoque_p  date default sysdate) is

cd_estabelecimento_w		movimento_estoque.cd_estabelecimento%type;
cd_local_estoque_w		movimento_estoque.cd_local_estoque%type;
cd_material_estoque_w		movimento_estoque.cd_material_estoque%type;
cd_fornecedor_w			movimento_estoque.cd_fornecedor%type;
qt_inventario_w			movimento_estoque.qt_inventario%type;
nr_seq_lote_fornec_w		movimento_estoque.nr_seq_lote_fornec%type;		
dt_validade_w			movimento_estoque.dt_validade%type;
cd_lote_fabricacao_w		movimento_estoque.cd_lote_fabricacao%type;
cd_unidade_medida_estoque_w	movimento_estoque.cd_unidade_medida_estoque%type;
cd_unidade_med_mov_w		movimento_estoque.cd_unidade_med_mov%type;
qt_saldo_consig_w		fornecedor_mat_consignado.qt_estoque%type;
ie_material_estoque_lote_w	varchar2(1); 
ie_controle_lote_w		parametro_estoque.ie_estoque_lote%type;
qt_diferenca_w			movimento_estoque.qt_estoque%type;
cd_operacao_estoque_w		movimento_estoque.cd_operacao_estoque%type;
ie_tipo_entrada_w		varchar2(1); 
nr_sequencia_item_docto_w	movimento_estoque.nr_sequencia_item_docto%type;
					
cursor c01 is
select	cd_estabelecimento,
	cd_local_estoque,
	cd_material_estoque cd_material,
	cd_fornecedor,
	sum(qt_inventario) qt_inventario,
	nr_seq_lote_fornec,
	dt_validade,
	cd_lote_fabricacao
from	w_inventario_barra
where	nm_usuario = nm_usuario_p
group by 	
	cd_estabelecimento,
	cd_local_estoque,
	cd_material_estoque,
	cd_fornecedor,
	nr_seq_lote_fornec,
	dt_validade,
	cd_lote_fabricacao;
	
begin	
open C01;
	loop
	fetch C01 into	
		cd_estabelecimento_w,	
		cd_local_estoque_w,	
		cd_material_estoque_w,
		cd_fornecedor_w,	
		qt_inventario_w,		
		nr_seq_lote_fornec_w,
		dt_validade_w,	
		cd_lote_fabricacao_w;		
	exit when C01%notfound;
		begin
		
		select	cd_material_estoque 
		into	cd_material_estoque_w
		from 	material 
		where	cd_material = cd_material_estoque_w;
		
		ie_material_estoque_lote_w := substr(obter_se_material_estoque_lote(cd_estabelecimento_p, cd_material_estoque_w),1,1);
		
		cd_unidade_medida_estoque_w := obter_dados_material_estab(cd_material_estoque_w, cd_estabelecimento_p, 'UME');
		cd_unidade_med_mov_w := cd_unidade_medida_estoque_w;
		
		cd_fornecedor_w := somente_numero(cd_fornecedor_w);
		
		qt_saldo_consig_w := obter_saldo_estoque_consig(cd_estabelecimento_p, cd_fornecedor_w, cd_material_estoque_w, cd_local_estoque_w);	
		
		if	(nvl(nr_seq_lote_fornec_w, 0) <> 0 and
			('S' = ie_material_estoque_lote_w))  then
			qt_saldo_consig_w := obter_saldo_consig_lote(cd_estabelecimento_p,cd_material_estoque_w,cd_local_estoque_w,cd_fornecedor_w,trunc(dt_mesano_referencia_p,'month'),nr_seq_lote_fornec_w);
		end if;
		
		qt_diferenca_w := qt_inventario_w - qt_saldo_consig_w;
		ie_tipo_entrada_w := 'E';
		
		if	(qt_diferenca_w < 0) then
			qt_diferenca_w := qt_diferenca_w * -1;	
			ie_tipo_entrada_w := 'S';
		end if;		
		
		select 	min(cd_operacao_estoque) cd_operacao_estoque
		into	cd_operacao_estoque_w
		from 	operacao_estoque
		where 	ie_tipo_requisicao  = 5                                                                                       
		and 	ie_entrada_saida = ie_tipo_entrada_w
		and 	ie_consignado = 7;	

		if	(nvl(cd_operacao_estoque_w, 0) = 0) then
			--Nao identificada a opera�ao de estoque para #@DS_ENTRADA_SAIDA#@ de Invent�rio de Consignado.
			wheb_mensagem_pck.exibir_mensagem_abort(172275, ie_tipo_entrada_w);
		end if;	
		
		select	(nvl(max(nr_sequencia_item_docto), 0) + 1)		
		into	nr_sequencia_item_docto_w
		from	movimento_estoque
		where	nr_documento = nr_documento_p;
		
		if	(nvl(qt_inventario_w, 0) <> 0) then
			insert into movimento_estoque(
				nr_movimento_estoque,
				nr_seq_lote_fornec,
				dt_validade,
				cd_lote_fabricacao,
				cd_material_estoque,
				qt_inventario,
				cd_unidade_medida_estoque,
				cd_unidade_med_mov,
				qt_movimento,
				qt_estoque,
				cd_operacao_estoque,
				cd_estabelecimento,
				cd_local_estoque,
				dt_movimento_estoque,
				nm_usuario,
				cd_acao,
				cd_material,
				dt_atualizacao,
				ie_origem_documento,
				nr_documento,
				nr_sequencia_item_docto,
				cd_fornecedor,
				dt_mesano_referencia)
			values(	
				movimento_estoque_seq.nextval,
				decode(nr_seq_lote_fornec_w, 0, null, nr_seq_lote_fornec_w),
				dt_validade_w,
				cd_lote_fabricacao_w,
				cd_material_estoque_w,
				qt_inventario_w,
				cd_unidade_medida_estoque_w,
				cd_unidade_med_mov_w,
				qt_diferenca_w,
				qt_diferenca_w,
				cd_operacao_estoque_w,
				cd_estabelecimento_w,
				cd_local_estoque_w,
				dt_movimento_estoque_p,
				nm_usuario_p,
				1,
				cd_material_estoque_w,
				sysdate,
				5,
				nr_documento_p,
				nr_sequencia_item_docto_w,
				cd_fornecedor_p,
				dt_mesano_referencia_p);	
		else 
			insert into movimento_estoque(
				nr_movimento_estoque,
				nr_seq_lote_fornec,				
				cd_material_estoque,
				qt_inventario,
				cd_unidade_medida_estoque,
				cd_unidade_med_mov,
				qt_movimento,
				qt_estoque,
				cd_operacao_estoque,
				cd_estabelecimento,
				cd_local_estoque,
				dt_movimento_estoque,
				nm_usuario,
				cd_acao,
				cd_material,
				dt_atualizacao,
				ie_origem_documento,
				nr_documento,
				nr_sequencia_item_docto,
				cd_fornecedor,
				dt_mesano_referencia)
			values(	
				movimento_estoque_seq.nextval,	
				decode(nr_seq_lote_fornec_w, 0, null, nr_seq_lote_fornec_w),				
				cd_material_estoque_w,
				qt_inventario_w,
				cd_unidade_medida_estoque_w,
				cd_unidade_med_mov_w,
				qt_diferenca_w,
				qt_diferenca_w,
				cd_operacao_estoque_w,
				cd_estabelecimento_w,
				cd_local_estoque_w,
				dt_movimento_estoque_p,
				nm_usuario_p,
				1,
				cd_material_estoque_w,
				sysdate,
				5,
				nr_documento_p,
				nr_sequencia_item_docto_w,
				cd_fornecedor_p,
				dt_mesano_referencia_p);
		end if;
		
		end;
	end loop;
close C01;


end gerar_movto_inv_direto_consig;
/
