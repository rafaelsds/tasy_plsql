create or replace
procedure desvincular_titulo_pagar(
									nr_titulo_p			number,
									nr_contrato_p		number,
									ds_justificativa_p	varchar2,
									nm_usuario_p		varchar2) is 

begin

update	titulo_pagar
set		nr_seq_contrato = null
where	nr_titulo =	nr_titulo_p;

update	titulo_pagar_classif
set		nr_contrato = null
where	nr_titulo =	nr_titulo_p;

insert into	titulo_pagar_hist
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_titulo,
			dt_historico,
			ds_historico)
values		(titulo_pagar_hist_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_titulo_p,
			sysdate,
			wheb_mensagem_pck.get_texto(374337, 'NR_CONTRATO=' || nr_contrato_p || ';DS_JUSTIFICATIVA=' || ds_justificativa_p));
commit;

end desvincular_titulo_pagar;
/
