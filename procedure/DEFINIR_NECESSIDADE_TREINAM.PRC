create or replace
procedure definir_necessidade_treinam	(nr_sequencia_p	number,
				ie_necessario_p	varchar2,
				nr_seq_motivo_p number default null) is

begin
	
if	(nvl(nr_sequencia_p,0) > 0) then
	update	qms_treinamento
	set	ie_necessario = ie_necessario_p,
		nr_seq_mot_nao_aplic = nr_seq_motivo_p
	where	nr_sequencia = nr_sequencia_p
	and	coalesce(ie_situacao, 'A') = 'A';
	
	commit;
end if;

end definir_necessidade_treinam;
/
