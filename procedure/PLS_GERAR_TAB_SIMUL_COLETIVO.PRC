create or replace
procedure pls_gerar_tab_simul_coletivo
			(	nr_seq_simul_perfil_p		pls_simulacao_perfil.nr_sequencia%type,
				nr_seq_proposta_p		pls_proposta_adesao.nr_sequencia%type,
				nr_seq_tabela_p			pls_tabela_preco.nr_sequencia%type,
				nr_seq_tabela_nova_p	out	pls_tabela_preco.nr_sequencia%type,
				nm_usuario_p			usuario.nm_usuario%type) is

nr_seq_plano_w			pls_plano.nr_sequencia%type;
nr_seq_tabela_w			pls_tabela_preco.nr_sequencia%type;
ds_perfil_w			pls_simul_regra_perfil.ds_perfil_simulacao%type;
nr_seq_faixa_etaria_w		pls_tabela_preco.nr_seq_faixa_etaria%type;
ie_simulacao_antiga_w		varchar2(1)	:= 'N';
nr_seq_plano_preco_w		pls_plano_preco.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

Cursor C01 is --Simulacao antiga Delphi
	select	nr_sequencia,
		qt_idade_inicial,
		qt_idade_final,
		vl_preco_faixa
	from	pls_simulacao_plano
	where	nr_seq_simul_perfil	= nr_seq_simul_perfil_p
	and	nr_seq_tabela		= nr_seq_tabela_p
	and	qt_idade_final	<> 0
	order by qt_idade_inicial;

Cursor C02 is --Simulacao nova HTML5
	select	a.qt_idade_inicial,
		a.qt_idade_final,
		a.vl_preco_atual
	from	pls_plano_preco		a,
		pls_tabela_preco	b,
		pls_simul_perfil_tabela	c
	where	b.nr_sequencia		= a.nr_seq_tabela
	and	b.nr_sequencia		= c.nr_seq_tabela
	and	c.nr_seq_simul_perfil	= nr_seq_simul_perfil_p
	and	c.nr_seq_tabela		= nr_seq_tabela_p
	order by	a.qt_idade_inicial;

begin

select	max(a.nr_seq_plano),
	max(a.nr_seq_faixa_etaria),
	max(b.cd_estabelecimento)
into	nr_seq_plano_w,
	nr_seq_faixa_etaria_w,
	cd_estabelecimento_w
from	pls_tabela_preco	a,
	pls_plano		b
where	b.nr_sequencia	= a.nr_seq_plano
and	a.nr_sequencia	= nr_seq_tabela_p;

select	b.ds_perfil_simulacao
into	ds_perfil_w
from	pls_simulacao_perfil	a,
	pls_simul_regra_perfil	b
where	b.nr_sequencia	= a.nr_seq_regra_perfil
and	a.nr_sequencia	= nr_seq_simul_perfil_p;

insert into pls_tabela_preco
	(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nm_tabela, dt_inicio_vigencia, nr_seq_plano, dt_liberacao, ie_tabela_base,
		ie_preco_vidas_contrato, ie_calculo_vidas, ie_proposta_adesao, nr_seq_simul_perfil,
		nr_seq_faixa_etaria, nr_seq_tabela_origem)
values	(	pls_tabela_preco_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
		substr('Tabela gerada para a proposta ' || to_char(nr_seq_proposta_p) || ' - ' || ds_perfil_w,1,255), sysdate, nr_seq_plano_w, sysdate, 'N',
		'N', 'N', 'S', nr_seq_simul_perfil_p,
		nr_seq_faixa_etaria_w, nr_seq_tabela_p)
	returning nr_sequencia into nr_seq_tabela_w;

ie_simulacao_antiga_w	:= 'N';
for c01_w in C01 loop
	begin
	insert into pls_plano_preco
		(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_tabela, qt_idade_inicial, qt_idade_final, vl_preco_inicial, vl_preco_atual,
			tx_acrescimo, vl_preco_nao_subsidiado, vl_preco_nao_subsid_atual)
	values	(	pls_plano_preco_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			nr_seq_tabela_w, c01_w.qt_idade_inicial, c01_w.qt_idade_final, c01_w.vl_preco_faixa, c01_w.vl_preco_faixa,
			0, 0, 0)
		returning nr_sequencia into nr_seq_plano_preco_w;
	
	pls_calcular_acresc_tab_preco(nr_seq_tabela_w, nr_seq_plano_preco_w, cd_estabelecimento_w, nm_usuario_p);
	
	ie_simulacao_antiga_w	:= 'S';
	end;
end loop; --C01

if	(ie_simulacao_antiga_w = 'N') then
	for c02_w in C02 loop
		begin
		insert into pls_plano_preco
			(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_tabela, qt_idade_inicial, qt_idade_final, vl_preco_inicial, vl_preco_atual,
				tx_acrescimo, vl_preco_nao_subsidiado, vl_preco_nao_subsid_atual)
		values	(	pls_plano_preco_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_tabela_w, c02_w.qt_idade_inicial, c02_w.qt_idade_final, c02_w.vl_preco_atual, c02_w.vl_preco_atual,
				0, 0, 0)
			returning nr_sequencia into nr_seq_plano_preco_w;
		
		pls_calcular_acresc_tab_preco(nr_seq_tabela_w, nr_seq_plano_preco_w, cd_estabelecimento_w, nm_usuario_p);
		end;
	end loop;
end if;

nr_seq_tabela_nova_p	:= nr_seq_tabela_w;

end pls_gerar_tab_simul_coletivo;
/