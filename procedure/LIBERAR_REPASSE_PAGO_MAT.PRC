create or replace
procedure LIBERAR_REPASSE_PAGO_MAT(	nr_seq_retorno_p		number,
					nr_seq_grg_p		number,
					nr_seq_ret_glosa_p	number,
					nm_usuario_p		varchar2) is

nr_seq_proc_repasse_w		number(15);
nr_seq_mat_repasse_w		number(15);
nr_seq_ret_item_w		number(15);
dt_retorno_w			date;
nr_interno_conta_w		number(15);
ie_status_w			varchar2(255);
vl_liberado_w			number(15,4);
vl_repasse_w			number(15,4);
nr_seq_lote_audit_hist_w	number(10);
cd_estabelecimento_w		number(4);
ie_repasse_grg_w		varchar2(1);
ds_observacao_w			varchar2(3980);
cd_autorizacao_w		varchar2(20);
ie_repasse_retorno_w		parametro_repasse.ie_repasse_retorno%type;
vl_desconto_item_w		material_repasse.vl_desconto%type;
vl_desconto_w			convenio_retorno_item.vl_desconto%type;
vl_guia_w			number(15,2);
vl_original_repasse_w		material_repasse.vl_original_repasse%type;

cursor c01 is
select	a.nr_interno_conta,
	a.nr_sequencia,
	null nr_seq_lote_hist,
	a.cd_autorizacao,
	nvl((select	max(x.vl_desconto)
	from	convenio_retorno_item x
	where	x.nr_sequencia = a.nr_sequencia
	and	not	exists(	select	1
				from	convenio_retorno_glosa y
				where	y.nr_seq_ret_item = x.nr_sequencia
				and	y.vl_desconto_item <> 0)),0)
from	convenio_retorno_item a
where	a.nr_seq_retorno	= nr_seq_retorno_p
and	a.vl_pago	<> 0
and	((ie_repasse_retorno_w = 'S') or
	(ie_repasse_retorno_w = 'P' and a.vl_amenor = 0))
union all
select	b.nr_interno_conta,
	null,
	b.nr_seq_lote_hist,
	b.cd_autorizacao,
	0
from	lote_audit_hist_guia b,
	lote_audit_hist_item a
where	a.nr_seq_guia			= b.nr_sequencia
and	a.vl_pago			<> 0
and	b.nr_seq_lote_hist		= nr_seq_grg_p
and	ie_repasse_grg_w		= 'P'
order by	1;

cursor c02 is
select	a.nr_sequencia,
	a.ie_status,
	a.vl_liberado,
	a.vl_repasse,
	a.vl_original_repasse
from	material_repasse a,
	material_atend_paciente b
where	a.nr_seq_material	= b.nr_sequencia
and	b.nr_interno_conta	= nr_interno_conta_w
and	not exists	(select	1
			from	convenio_retorno_item y,
				convenio_retorno_glosa x
			where	x.nr_seq_matpaci	= b.nr_sequencia
			and	x.nr_seq_ret_item	= y.nr_sequencia
			and	y.nr_seq_retorno	= nr_seq_retorno_p
			and	x.vl_cobrado		= x.vl_glosa
			and	nvl(x.vl_cobrado,0) <> 0
			and	nvl(x.vl_glosa,0) <> 0)
and	a.ie_status	in ('A','U')
and	nvl(nr_seq_ret_glosa_p, b.nr_sequencia)	= b.nr_sequencia
and	nvl(a.nr_seq_item_retorno, -1)	<> nvl(nr_seq_ret_item_w,-1)
and	nr_seq_lote_audit_hist_w is null
and	nvl(b.nr_doc_convenio,nvl(cd_autorizacao_w,'N�o Informada')) = nvl(cd_autorizacao_w,'N�o Informada')
union
select	a.nr_sequencia,
	a.ie_status,
	a.vl_liberado,
	a.vl_repasse,
	a.vl_original_repasse
from	material_repasse a,
	material_atend_paciente b
where	a.nr_seq_material		= b.nr_sequencia
and	b.nr_interno_conta		= nr_interno_conta_w
and	a.ie_status			in ('A','U')
and	nvl(nr_seq_ret_glosa_p, b.nr_sequencia)	= b.nr_sequencia
and	(nr_seq_lote_audit_hist_w is null or nvl(a.nr_seq_lote_audit_hist, -1) <> nr_seq_lote_audit_hist_w)
and	nr_seq_ret_item_w is null
and	ie_repasse_grg_w		= 'P'
and	nvl(b.nr_doc_convenio,nvl(cd_autorizacao_w,'N�o Informada')) = nvl(cd_autorizacao_w,'N�o Informada');

begin

if	(nr_seq_retorno_p is not null) then

	select	decode(b.ie_data_lib_repasse_ret, 'F', nvl(a.dt_baixa_cr, a.dt_fechamento), 'R', a.dt_retorno, 'E', a.dt_fechamento),
		a.cd_estabelecimento
	into	dt_retorno_w,
		cd_estabelecimento_w
	from	parametro_faturamento b,
		convenio_retorno a
	where	a.nr_sequencia		= nr_seq_retorno_p
	and	a.cd_estabelecimento	= b.cd_estabelecimento;

end if;

if	(nr_seq_grg_p is not null) then

	select	c.dt_fechamento,
		a.cd_estabelecimento
	into	dt_retorno_w,
		cd_estabelecimento_w
	from	lote_auditoria a,
		lote_audit_hist c
	where	c.nr_sequencia		= nr_seq_grg_p
	and	a.nr_sequencia		= c.nr_seq_lote_audit;

end if;

select	nvl(max(a.ie_repasse_grg),'N'),
	nvl(max(ie_repasse_retorno),'S')
into	ie_repasse_grg_w,
	ie_repasse_retorno_w
from	parametro_repasse a
where	a.cd_estabelecimento	= cd_estabelecimento_w;

open c01;
loop
fetch c01 into
	nr_interno_conta_w,
	nr_seq_ret_item_w,
	nr_seq_lote_audit_hist_w,
	cd_autorizacao_w,
	vl_desconto_w;
exit when c01%notfound;
	
	vl_guia_w	:= obter_valor_conpaci_guia(nr_interno_conta_w, cd_autorizacao_w, 1);
	
	open c02;
	loop
	fetch c02 into
		nr_seq_mat_repasse_w,
		ie_status_w,
		vl_liberado_w,
		vl_repasse_w,
		vl_original_repasse_w;
	exit when c02%notfound;
		
		vl_desconto_item_w	:= (vl_desconto_w/vl_guia_w)*nvl(vl_original_repasse_w,0);
		
		update	material_repasse
		set	nr_seq_item_retorno	= nr_seq_ret_item_w,
			vl_repasse		= vl_repasse - vl_desconto_item_w,
			vl_liberado		= vl_repasse - vl_desconto_item_w,
			ie_status		= 'R',
			dt_liberacao		= dt_retorno_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			nm_usuario_lib		= nm_usuario_p,
			nr_seq_lote_audit_hist	= nr_seq_lote_audit_hist_w,
			vl_desconto		= decode(nvl(vl_desconto,0),0,vl_desconto_item_w,vl_desconto)
		where	nr_sequencia		= nr_seq_mat_repasse_w;
		
		GRAVAR_MAT_REPASSE_VALOR(	WHEB_MENSAGEM_PCK.get_texto(303754),
						nm_usuario_p,
						ie_status_w,
						'R',
						nvl(vl_liberado_w,0),
						nvl(vl_repasse_w,0),
						nr_seq_retorno_p,
						nr_seq_grg_p,
						nr_seq_mat_repasse_w,
						null);

	end loop;
	close c02;

end loop;
close c01;

end	LIBERAR_REPASSE_PAGO_MAT;
/