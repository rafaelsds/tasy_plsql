create or replace
procedure consiste_carteira_conv_eup_js(
					cd_pessoa_fisica_p		varchar2,
					nr_atendimento_p		number,
					cd_usuario_convenio_p		varchar2,
					cd_convenio_p			number,
					ie_consistir_dupli_cart_p	varchar2,	
					cd_estabelecimento_p		number,
					nm_usuario_p			Varchar2,
					ds_msg_abort_p		out	varchar2,
					ds_msg_alerta_cart_p	out	varchar2) is 

ie_consiste_cart_conv_w	varchar2(1);
qt_convenio_w		number(10);
nr_atendimento_w	number(10);
nm_pessoa_fisica_w	varchar2(80);
ie_cart_rn_lib_w	varchar2(1);					
					
begin



if	(ie_consistir_dupli_cart_p <> 'N') then
	
	select	max(ie_cons_duplic_cod_usu) 
	into	ie_consiste_cart_conv_w
	from	convenio_estabelecimento
        where	cd_convenio  = cd_convenio_p 
        and 	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(ie_consiste_cart_conv_w = 'S') then
	
		select	count(*)
		into	qt_convenio_w
                from	atend_categoria_convenio a, 
                        atendimento_paciente b    
                where	a.nr_atendimento = b.nr_atendimento
                and   	a.cd_convenio = cd_convenio_p
                and   	a.cd_usuario_convenio = cd_usuario_convenio_p
                and   	a.nr_atendimento     <> nr_atendimento_p
                and   	b.cd_pessoa_fisica   <> cd_pessoa_fisica_p;
	
		if	(nvl(qt_convenio_w,0) > 0) then
			
			select	max(a.nr_atendimento), 
				max(substr(obter_nome_pf(b.cd_pessoa_fisica),1,80))
			into	nr_atendimento_w,
				nm_pessoa_fisica_w
                        from  	atend_categoria_convenio a, 
                                atendimento_paciente b    
                        where	a.nr_atendimento = b.nr_atendimento
                        and  	a.cd_convenio = cd_convenio_p
                        and   	a.cd_usuario_convenio = cd_usuario_convenio_p
                        and   	a.nr_atendimento <> nr_atendimento_p
                        and   	b.cd_pessoa_fisica <> cd_pessoa_fisica_p;
			
			if	(ie_consistir_dupli_cart_p = 'S') then
				ds_msg_abort_p := substr(obter_texto_dic_objeto(296412, wheb_usuario_pck.get_nr_seq_idioma, 'NR_ATEND='||nr_atendimento_w || ';NM_PESSOA='||nm_pessoa_fisica_w),1,255);
			elsif	(ie_consistir_dupli_cart_p = 'A') then
				ds_msg_alerta_cart_p := substr(obter_texto_dic_objeto(296412, wheb_usuario_pck.get_nr_seq_idioma, 'NR_ATEND='||nr_atendimento_w ||';NM_PESSOA='||nm_pessoa_fisica_w),1,255);
			elsif	(ie_consistir_dupli_cart_p = 'RN') then
				ie_cart_rn_lib_w := Obter_nr_carteira_liberado_RN(nr_atendimento_p, cd_usuario_convenio_p, nr_atendimento_w, cd_convenio_p);
				if	(ie_cart_rn_lib_w = 'N') then
					ds_msg_alerta_cart_p := substr(obter_texto_dic_objeto(296412, wheb_usuario_pck.get_nr_seq_idioma, 'NR_ATEND='||nr_atendimento_w || ';NM_PESSOA='||nm_pessoa_fisica_w),1,255);	
				end if;
			else
				ds_msg_abort_p := substr(obter_texto_dic_objeto(324186, wheb_usuario_pck.get_nr_seq_idioma, 'NR_ATEND='||nr_atendimento_w || ';NM_PESSOA='||nm_pessoa_fisica_w),1,255);
			end if;
		end if;
	end if;
end if;


commit;

end consiste_carteira_conv_eup_js;
/
