CREATE OR REPLACE
PROCEDURE	gravar_hist_pf_equipe_partic(	nr_seq_equipe_p   	NUMBER,
						ie_alteracao_p		VARCHAR2,
						ds_historico_p		VARCHAR2,
						nm_usuario_p      	VARCHAR2)
						IS


nr_seq_agenda_w	NUMBER(10);
ds_alteracao_w VARCHAR2(255);

--begin
BEGIN

IF (ie_alteracao_p = 'I') THEN
ds_alteracao_w := obter_desc_expressao(291817);
ELSIF (ie_alteracao_p = 'U') THEN
ds_alteracao_w :=  obter_desc_expressao(283380);
ELSE
ds_alteracao_w := obter_desc_expressao(289757);
END IF;

INSERT	INTO	pf_equipe_partic_hist
		(nr_sequencia,
		nr_seq_equipe_partic,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_historico,
		ds_alteracao)
VALUES	(pf_equipe_partic_hist_seq.NEXTVAL,
		nr_seq_equipe_p,
		SYSDATE,
		nm_usuario_p,
		SYSDATE,
		nm_usuario_p,
		ds_historico_p,
		ds_alteracao_w);
/*exception
when others then
	nr_seq_agenda_w	:= 0;
end;*/


END gravar_hist_pf_equipe_partic;
/