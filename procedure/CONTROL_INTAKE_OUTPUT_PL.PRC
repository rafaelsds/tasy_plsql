create or replace procedure control_intake_output_pl (nr_seq_tipo_p varchar2 default null,
													  nr_seq_modelo_secao_p number,
													  nm_usuario_p varchar2
													  ) as

cursor inOutList is
select to_number(regexp_substr(nr_seq_tipo_p,'[^,]+', 1, level)) as nr_seq_tipo, rownum as seq from dual
  connect by regexp_substr(nr_seq_tipo_p, '[^,]+', 1, level) is not null
union
select nr_seq_tipo, 0 as seq from pepo_modelo_secao_perd_ga
where nr_seq_modelo_secao = nr_seq_modelo_secao_p and nr_seq_tipo not in (
select regexp_substr(nr_seq_tipo_p,'[^,]+', 1, level)  from dual
  connect by regexp_substr(nr_seq_tipo_p, '[^,]+', 1, level) is not null
)
order by seq;

nr_sequencia_reorder_p number(10);
nr_order_p number(5);

begin

if (nr_seq_tipo_p is null) then
  delete
		  from pepo_modelo_secao_perd_ga
		 where nr_seq_modelo_secao = nr_seq_modelo_secao_p;
else


for r1 in inOutList loop

	if (r1.seq = 0) then
		delete
		  from pepo_modelo_secao_perd_ga
		 where nr_seq_modelo_secao = nr_seq_modelo_secao_p
		   and nr_seq_tipo = r1.nr_seq_tipo;
	else
		begin
			update pepo_modelo_secao_perd_ga
			   set nr_seq_apresentacao = r1.seq
		     where nr_seq_modelo_secao = nr_seq_modelo_secao_p
		   and nr_seq_tipo = r1.nr_seq_tipo;
			if sql%rowcount = 0 then
				insert into
				pepo_modelo_secao_perd_ga(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_apresentacao,
						nr_seq_tipo,
						nr_seq_modelo_secao
						) values (
						pepo_modelo_secao_perd_ga_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						r1.seq,
						r1.nr_seq_tipo,
						nr_seq_modelo_secao_p
						);
			end if;
    end;
	end if;
  
end loop;
end if;
commit;
end control_intake_output_pl;
/