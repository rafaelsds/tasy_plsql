create or replace
procedure update_resp_physician(
		CD_MEDICO_ATENDIMENTO_p		Varchar2,
		NR_SEQ_ATEND_PAC_UNIDADE_p	number) is 

pragma autonomous_transaction;

begin
update 	ATENDIMENTO_PACIENTE ap
set	ap.CD_MEDICO_RESP = CD_MEDICO_ATENDIMENTO_p
where	ap.NR_ATENDIMENTO = (select   	apu.NR_ATENDIMENTO
				from      	ATEND_PACIENTE_UNIDADE apu
				where     	apu.NR_SEQ_INTERNO = NR_SEQ_ATEND_PAC_UNIDADE_p);

commit;

end update_resp_physician;
/
