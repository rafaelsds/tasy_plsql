create or replace
procedure excluir_relatorio_perfil
			(cd_perfil_p			number,
			nr_seq_relatorio_p		varchar2,
			nm_usuario_p			varchar2) as

nr_seq_perfil_relat_w   number(10,0);

begin

select nr_sequencia
into nr_seq_perfil_relat_w
from relatorio_perfil 
where nr_seq_relatorio = nr_seq_relatorio_p 
and cd_perfil = cd_perfil_p;

delete from relatorio_perfil_param where nr_seq_perfil_relat = nr_seq_perfil_relat_w;
commit;
delete from relatorio_perfil where nr_sequencia = nr_seq_perfil_relat_w;
commit;

end excluir_relatorio_perfil;
/