create or replace FUNCTION Obter_Desc_Indicacao_Medico (nr_seq_indicacao_p number)
     Return VARCHAR2 is

ds_indicacao_w Varchar2(40);

begin
if (nr_seq_indicacao_p is not null) then
 select max(ds_indicacao)
 into ds_indicacao_w
 from tipo_indicacao_medico
 where nr_sequencia = nr_seq_indicacao_p;
end if;

RETURN ds_indicacao_w;

END Obter_Desc_Indicacao_Medico;
/