create or replace
procedure ctb_pls_atualizar_rec_glosa
			(	nr_seq_conta_p			number,
				nr_seq_atualizacao_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				qt_movimento_p		in out	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_classificacao_credito_w	varchar2(255);
cd_classificacao_debito_w	varchar2(255);
cd_conta_credito_w		varchar2(20);
cd_conta_debito_w		varchar2(20);
nr_seq_esquema_w		number(10);
cd_historico_padrao_w		number(10);
nr_seq_grupo_ans_w		number(15);
nr_seq_grupo_superior_w		number(15);
ie_classif_grupo_w		varchar2(5);
ie_classif_grupo_ww		varchar2(5);
ie_tipo_relacao_w		varchar2(2);
ie_tipo_contrato_w		varchar2(2);
cd_classificacao_item_w		varchar2(30);
qt_movimento_w			number(10)	:= 0;
nr_seq_tipo_prestador_w		pls_prestador.nr_seq_tipo_prestador%type;
ie_tipo_grupo_ans_w		ans_grupo_despesa.ie_tipo_grupo_ans%type;
cd_cgc_prestador_w		pls_prestador.cd_cgc%type;
cd_pf_prestador_w		pls_prestador.cd_pessoa_fisica%type;
ie_tipo_prestador_w		pls_tipo_prestador.ie_tipo_ptu%type;
cd_conta_contabil_w		conta_contabil.cd_conta_contabil%type;
cd_empresa_w			estabelecimento.cd_empresa%type;
ie_tipo_repasse_w		pls_segurado_repasse.ie_tipo_repasse%type;
ie_tipo_compartilhamento_w	pls_segurado_repasse.ie_tipo_compartilhamento%type;
dt_referencia_w                 pls_rec_glosa_protocolo.dt_apresentacao_lote%type;
dt_competencia_w		pls_atualizacao_contabil.dt_mes_competencia%type;
dt_pagamento_w			pls_lote_pagamento.dt_mes_competencia%type;
dt_repasse_w			date;
dt_fim_repasse_w		date;

Cursor c_rec_glosa is
	select	a.nr_sequencia nr_seq_resumo_item,
		e.ie_tipo_contratacao,
		e.ie_preco,
		e.ie_segmentacao,
		e.ie_regulamentacao,
		nvl(c.ie_tipo_segurado,d.ie_tipo_segurado) ie_tipo_segurado,
		d.nr_seq_contrato,
		d.nr_seq_intercambio,
		c.nr_sequencia nr_seq_conta,
		e.ie_tipo_operacao,
		e.nr_sequencia nr_seq_plano,
		b.cd_procedimento,
		b.ie_origem_proced,
		a.ie_ato_cooperado,
		c.ie_tipo_guia,
		c.nr_seq_tipo_atendimento,
		c.cd_medico_executor,
		c.ie_regime_internacao,
		null ie_estorno_custo, --a.ie_estorno_custo,
		a.nr_seq_grupo_ans,
		b.ie_tipo_despesa,
		'P' ie_proc_mat,
		a.nr_seq_prestador_pgto nr_seq_prestador,
		a.rowid nr_id,
		a.nr_lote_contabil,
		a.nr_lote_contabil_apres,
		a.nr_lote_contabil_pag,
		e.nr_seq_classificacao nr_seq_classificacao_sca,
		d.nr_sequencia nr_seq_segurado,
                h.dt_apresentacao_lote dt_apresentacao,
                h.dt_liberacao_pag dt_liberacao_pag,
		nvl(b.dt_procedimento, h.dt_apresentacao_lote) dt_ref_repasse,
		a.nr_seq_lote_pgto,
		a.nr_seq_pp_lote,
		f.nr_seq_congenere,
		obter_estabelecimento_base(d.cd_pessoa_fisica) cd_estabelecimento_setor
	from	pls_conta_rec_resumo_item	a,
		pls_conta_proc			b,
		pls_conta			c,
		pls_segurado			d,
		pls_plano			e,
		pls_protocolo_conta		f,
		pls_rec_glosa_conta 		g,
		pls_rec_glosa_protocolo 	h,
		pls_rec_glosa_proc		i
	where	h.nr_sequencia	= g.nr_seq_protocolo
	and	g.nr_sequencia	= a.nr_seq_conta_rec
	and	b.nr_sequencia	= i.nr_seq_conta_proc
	and	i.nr_sequencia	= a.nr_seq_proc_rec
	and	c.nr_sequencia	= b.nr_seq_conta
	and	c.nr_seq_segurado = d.nr_sequencia(+)
	and	c.nr_seq_plano	= e.nr_sequencia(+)
	and	f.nr_sequencia	= c.nr_seq_protocolo
	and	g.nr_sequencia	= nr_seq_conta_p
	union all
	select	a.nr_sequencia,
		e.ie_tipo_contratacao,
		e.ie_preco,
		e.ie_segmentacao,
		e.ie_regulamentacao,
		nvl(c.ie_tipo_segurado,d.ie_tipo_segurado) ie_tipo_segurado,
		d.nr_seq_contrato,
		d.nr_seq_intercambio,
		c.nr_sequencia nr_seq_conta,
		e.ie_tipo_operacao,
		e.nr_sequencia nr_seq_plano_seg,
		b.cd_material,
		null,--b.ie_origem_proced,
		a.ie_ato_cooperado,
		c.ie_tipo_guia,
		c.nr_seq_tipo_atendimento,
		c.cd_medico_executor,
		c.ie_regime_internacao,
		null,--a.ie_estorno_custo,
		a.nr_seq_grupo_ans,
		b.ie_tipo_despesa,
		'M' ie_proc_mat,
		a.nr_seq_prestador_pgto nr_seq_prestador,
		a.rowid nr_id,
		a.nr_lote_contabil,
		a.nr_lote_contabil_apres,
		a.nr_lote_contabil_pag,
		e.nr_seq_classificacao nr_seq_classificacao_sca,
		d.nr_sequencia nr_seq_segurado,
                h.dt_apresentacao_lote dt_apresentacao,
                h.dt_liberacao_pag dt_liberacao_pag,
		nvl(b.dt_atendimento, h.dt_apresentacao_lote) dt_ref_repasse,
		a.nr_seq_lote_pgto,
		a.nr_seq_pp_lote,
		f.nr_seq_congenere,
		obter_estabelecimento_base(d.cd_pessoa_fisica) cd_estabelecimento_setor
	from	pls_conta_rec_resumo_item	a,
		pls_conta_mat			b,
		pls_conta			c,
		pls_segurado			d,
		pls_plano			e,
		pls_protocolo_conta		f,
		pls_rec_glosa_conta 		g,
		pls_rec_glosa_protocolo 	h,
		pls_rec_glosa_mat		i
	where	h.nr_sequencia	= g.nr_seq_protocolo
	and	g.nr_sequencia	= a.nr_seq_conta_rec
	and	b.nr_sequencia	= i.nr_seq_conta_mat
	and	i.nr_sequencia	= a.nr_seq_mat_rec
	and	c.nr_sequencia	= b.nr_seq_conta
	and	c.nr_seq_segurado = d.nr_sequencia(+)
	and	c.nr_seq_plano	= e.nr_sequencia(+)
	and	f.nr_sequencia	= c.nr_seq_protocolo
	and	g.nr_sequencia	= nr_seq_conta_p;
	
c_rec_glosa_w c_rec_glosa%rowtype;

cursor c_tipo_movimento is
	select	'20' ie_tipo_movto -- Apresentacao
	from	dual
	where	nvl(c_rec_glosa_w.nr_lote_contabil_apres,0) = 0
	and	((pkg_date_utils.start_of(c_rec_glosa_w.dt_apresentacao, 'MONTH', 0) = dt_competencia_w) or 
		nr_seq_atualizacao_p is null)
	union
	select	'21' ie_tipo_movto -- Fechamento
	from	dual
	where	nvl(c_rec_glosa_w.nr_lote_contabil,0) = 0
	and	((pkg_date_utils.start_of(c_rec_glosa_w.dt_liberacao_pag, 'MONTH', 0) = dt_competencia_w) or
		nr_seq_atualizacao_p is null)
	union
	select	'22' ie_tipo_movto -- Pagamento
	from	dual
	where	nvl(c_rec_glosa_w.nr_lote_contabil_pag,0) = 0
	and	((pkg_date_utils.start_of(dt_pagamento_w, 'MONTH', 0) = dt_competencia_w) or 
		nr_seq_atualizacao_p is null);
	
c_tipo_movimento_w c_tipo_movimento%rowtype;
	
Cursor c_esquema is
	select	a.nr_sequencia nr_seq_esquema,
		a.cd_historico_padrao,
		a.ie_prestador_codificacao,
		a.nr_seq_tipo_prestador,
		a.ie_tipo_relacao,
		a.ie_tipo_movimentacao,
		a.nr_seq_prestador,
		a.ie_tipo_ptu ie_tipo_prestador
	from	pls_esquema_contabil	a
	where	a.ie_tipo_regra		= 'RG'
	and	nvl(a.ie_tipo_movimentacao,c_tipo_movimento_w.ie_tipo_movto)	= c_tipo_movimento_w.ie_tipo_movto
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	dt_referencia_w between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_referencia_w)
	and	((a.nr_seq_plano = c_rec_glosa_w.nr_seq_plano) or (a.nr_seq_plano is null))
	and	((a.ie_tipo_segurado = c_rec_glosa_w.ie_tipo_segurado) or (a.ie_tipo_segurado is null))
	and	((a.nr_seq_contrato = c_rec_glosa_w.nr_seq_contrato) or (a.nr_seq_contrato is null))
	and	((a.ie_tipo_guia = c_rec_glosa_w.ie_tipo_guia) or (a.ie_tipo_guia is null))
	and	((a.nr_seq_intercambio = c_rec_glosa_w.nr_seq_intercambio) or (a.nr_seq_intercambio is null))
	and	((a.nr_seq_classif_sca = c_rec_glosa_w.nr_seq_classificacao_sca) or (a.nr_seq_classif_sca is null))
	and	((a.ie_tipo_repasse = ie_tipo_repasse_w) or (a.ie_tipo_repasse is null))
	and	((a.ie_tipo_compartilhamento = ie_tipo_compartilhamento_w) or (a.ie_tipo_compartilhamento is null))
	and	((a.cd_estab_setor_pessoa = c_rec_glosa_w.cd_estabelecimento_setor) or (a.cd_estab_setor_pessoa is null))
	order by
                nvl(a.ie_tipo_guia,' '),
		nvl(a.nr_seq_intercambio,0),
		nvl(a.ie_tipo_segurado,' '),
		nvl(a.nr_seq_contrato,0),
		nvl(a.nr_seq_plano,0),
                nvl(a.nr_seq_classif_sca,0),
		nvl(a.ie_tipo_compartilhamento,0),
		nvl(a.ie_tipo_repasse,' '),
		nvl(a.cd_estab_setor_pessoa,0),
		nvl(a.ie_tipo_relacao,' '),
		nvl(a.ie_tipo_ptu,' '),
		nvl(a.nr_seq_tipo_prestador,0),
		nvl(a.dt_inicio_vigencia,sysdate);

c_esquema_w c_esquema%rowtype;
		
Cursor c_segmentacao is
	select	ie_codificacao,
		vl_fixo,
		cd_conta_contabil,
		ie_debito_credito,
		ds_mascara
	from	pls_esquema_contabil_seg
	where	nr_seq_regra_esquema	= nr_seq_esquema_w
	order by
		ie_debito_credito,
		nr_seq_apresentacao;
c_segmentacao_w c_segmentacao%rowtype;
		
begin

qt_movimento_w	:= qt_movimento_p;

select	max(cd_empresa)
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

select 	max(pkg_date_utils.start_of(dt_mes_competencia, 'MONTH', 0))
into	dt_competencia_w
from	pls_atualizacao_contabil
where	nr_sequencia = nr_seq_atualizacao_p;

open c_rec_glosa;
loop
fetch c_rec_glosa into	
	c_rec_glosa_w;
exit when c_rec_glosa%notfound;
	begin
	ie_tipo_contrato_w		:= '';	
	ie_tipo_relacao_w		:= null;
	nr_seq_tipo_prestador_w		:= null;
	
	pls_obter_dados_repasse(	c_rec_glosa_w.dt_ref_repasse,
					c_rec_glosa_w.nr_seq_segurado,
					c_rec_glosa_w.nr_seq_congenere,
					ie_tipo_repasse_w,
					ie_tipo_compartilhamento_w,
					dt_repasse_w,
					dt_fim_repasse_w);

	if	(c_rec_glosa_w.nr_seq_prestador is not null) then
		select	ie_tipo_relacao,
			nr_seq_tipo_prestador,
			cd_cgc,
			cd_pessoa_fisica
		into	ie_tipo_relacao_w,
			nr_seq_tipo_prestador_w,
			cd_cgc_prestador_w,
			cd_pf_prestador_w
		from	pls_prestador
		where	nr_sequencia	= c_rec_glosa_w.nr_seq_prestador
		and	rownum		<= 1;
		
		if	(nr_seq_tipo_prestador_w is not null) then
			select	ie_tipo_ptu
			into	ie_tipo_prestador_w
			from	pls_tipo_prestador
			where	nr_sequencia	= nr_seq_tipo_prestador_w
			and	rownum		<= 1;
		end if;
	end if;
	
	if	(c_rec_glosa_w.nr_seq_contrato is not null) then
		select	decode(cd_cgc_estipulante,null,'PF','PJ')
		into	ie_tipo_contrato_w
		from	pls_contrato
		where	nr_sequencia	= c_rec_glosa_w.nr_seq_contrato
		and	rownum		<= 1;
		
	elsif	(nvl(c_rec_glosa_w.nr_seq_intercambio,0) <> 0) then
		select	decode(cd_pessoa_fisica, null, 'PJ', 'PF')
		into	ie_tipo_contrato_w
		from	pls_intercambio
		where	nr_sequencia	= c_rec_glosa_w.nr_seq_intercambio
		and	rownum		<= 1;
	end if;
	
	select	max(x.dt_mes_competencia)
	into	dt_pagamento_w
	from	(select	p.dt_mes_competencia
		from	pls_pp_lote p
		where	p.nr_sequencia = c_rec_glosa_w.nr_seq_pp_lote
		union all
		select	p.dt_mes_competencia
		from	pls_lote_pagamento p
		where	p.nr_sequencia = c_rec_glosa_w.nr_seq_lote_pgto ) x;
	
	open c_tipo_movimento;
	loop
	fetch c_tipo_movimento into	
		c_tipo_movimento_w;
	exit when c_tipo_movimento%notfound;
		begin
		nr_seq_esquema_w		:= null;
		cd_conta_credito_w		:= null;
		cd_conta_debito_w		:= null;
		cd_conta_contabil_w		:= null;
		cd_classificacao_credito_w	:= null;
		cd_classificacao_debito_w	:= null;
		cd_historico_padrao_w		:= null;
                
                if      (c_tipo_movimento_w.ie_tipo_movto = '20') then
                        dt_referencia_w := c_rec_glosa_w.dt_apresentacao;
                elsif   (c_tipo_movimento_w.ie_tipo_movto = '21') then
                        dt_referencia_w := c_rec_glosa_w.dt_liberacao_pag;
                elsif   (c_tipo_movimento_w.ie_tipo_movto = '22') then
                        dt_referencia_w := dt_pagamento_w;
                end if;
		open c_esquema;
		loop
		fetch c_esquema into
			c_esquema_w;
		exit when c_esquema%notfound;
			begin
			if	((nvl(c_esquema_w.ie_tipo_relacao,nvl(ie_tipo_relacao_w,'X')) = nvl(ie_tipo_relacao_w,'X')) and
				(nvl(c_esquema_w.ie_tipo_prestador,nvl(ie_tipo_prestador_w,'X')) = nvl(ie_tipo_prestador_w,'X')) and
				(nvl(c_esquema_w.nr_seq_tipo_prestador,nvl(nr_seq_tipo_prestador_w,0)) = nvl(nr_seq_tipo_prestador_w,0))) then
					
				nr_seq_esquema_w := c_esquema_w.nr_seq_esquema;
				cd_historico_padrao_w := c_esquema_w.cd_historico_padrao;
			end if;
			end;
		end loop;
		close c_esquema;
		
		nr_seq_grupo_ans_w := c_rec_glosa_w.nr_seq_grupo_ans;
		
		/* GRUPO ANS */
		/* 5 - Grupo ANS com base nos valores do ITAMED */
		if	(nvl(nr_seq_grupo_ans_w,0) > 0) then
			begin
			
			select	nr_seq_grupo_superior
			into	nr_seq_grupo_superior_w
			from	ans_grupo_despesa
			where	nr_sequencia	= nr_seq_grupo_ans_w;
			
			exception
			when others then
				nr_seq_grupo_superior_w	:= null;
			end;
		end if;
		
		if	(nvl(nr_seq_grupo_superior_w, 0) > 0) then
			nr_seq_grupo_ans_w	:= nr_seq_grupo_superior_w;
		end if;
		
		select	max(ie_tipo_grupo_ans)
		into	ie_tipo_grupo_ans_w
		from	ans_grupo_despesa
		where	nr_sequencia	= nr_seq_grupo_ans_w;
		
		if	(ie_tipo_grupo_ans_w = 10) then /* 1 - Consultas */
			ie_classif_grupo_w	:= '1';
			ie_classif_grupo_ww	:= '0';
		elsif	(ie_tipo_grupo_ans_w = 20) then /* 49 - Exames */
			ie_classif_grupo_w	:= '2';
			ie_classif_grupo_ww	:= '0';
		elsif	(ie_tipo_grupo_ans_w = 30) then /* 51 - Terapias */
			ie_classif_grupo_w	:= '3';
			ie_classif_grupo_ww	:= '0';
		elsif	(ie_tipo_grupo_ans_w = 41) then /* 7 - Internacao - Honorario medico */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '1';
		elsif	(ie_tipo_grupo_ans_w = 42) then /* 8 - Internacao - Exames */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '2';
		elsif	(ie_tipo_grupo_ans_w = 43) then /* 9 - Internacao - Terapias*/
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '3';
		elsif	(ie_tipo_grupo_ans_w = 44) then /* 10 - Internacao - Materiais medicos */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '4';
		elsif	(ie_tipo_grupo_ans_w = 45) then /* 11 - Internacao - Medicamentos */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '5';
		elsif	(ie_tipo_grupo_ans_w = 46) then /* 11 - Internacao - Procedimentos com preco fixo */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '6';
		elsif	(ie_tipo_grupo_ans_w = 49) then /* 12 - Internacao - Outras despesas */
			ie_classif_grupo_w	:= '4';
			ie_classif_grupo_ww	:= '9';
		elsif	(ie_tipo_grupo_ans_w = 50) then /* 6 - Outros atendimentos - Ambulatoriais */
			ie_classif_grupo_w	:= '5';
			ie_classif_grupo_ww	:= '0';
		elsif	(ie_tipo_grupo_ans_w = 60) then /* 16 - Demais despesas assistenciais */
			ie_classif_grupo_w	:= '6';
			ie_classif_grupo_ww	:= '0';
		end if;
		
		open c_segmentacao;
		loop
		fetch c_segmentacao into	
			c_segmentacao_w;
		exit when c_segmentacao%notfound;
			begin
			cd_classificacao_item_w	:= null;
			
			if	(c_rec_glosa_w.ie_estorno_custo = 'S') then
				if	(c_segmentacao_w.ie_debito_credito = 'C') then
					c_segmentacao_w.ie_debito_credito	:= 'D';
				else
					c_segmentacao_w.ie_debito_credito	:= 'C';
				end if;
			end if;
			
			if	(c_segmentacao_w.ie_debito_credito = 'C') then /* Classificacao CREDITO */
				if	(c_segmentacao_w.ie_codificacao = 'CR') then /* Codigo reduzido */
					select	cd_classificacao_atual
					into	cd_classificacao_credito_w
					from	conta_contabil
					where	cd_conta_contabil	= c_segmentacao_w.cd_conta_contabil;
					
					cd_conta_credito_w	:= c_segmentacao_w.cd_conta_contabil;
				elsif	(c_segmentacao_w.ie_codificacao = 'FX') then /* Fixo */
					cd_classificacao_item_w	:= c_segmentacao_w.vl_fixo;
				elsif	(c_segmentacao_w.ie_codificacao = 'GA') then /* Grupo ANS */
					if	(ie_classif_grupo_w is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_w;
					else
						cd_classificacao_item_w	:= 'GA';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'FP') then /* Formacao de preco */
					if	(c_rec_glosa_w.ie_tipo_segurado = 'A') then
						cd_classificacao_item_w	:= '9';
					else
						cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_formacao_preco(c_rec_glosa_w.ie_preco);
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'R') then /* Regumelantacao */
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_regulamentacao(c_rec_glosa_w.ie_regulamentacao);
				elsif	(c_segmentacao_w.ie_codificacao = 'TC') then /* Tipo de contratacao */
					if	(c_rec_glosa_w.ie_tipo_contratacao in ('I','CE','CA')) then
						cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_contratacao(c_rec_glosa_w.ie_tipo_contratacao);
					else
						cd_classificacao_item_w	:= cd_classificacao_credito_w || 'TC';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'TP') then /* Tipo de Contrato (Pessoa fisica ou Juridica) */
					if	(ie_tipo_contrato_w in ('PF','PJ')) then
						cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_pessoa_contrato(ie_tipo_contrato_w);
					else
						cd_classificacao_item_w	:= cd_classificacao_credito_w || 'TP';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'S') then /* Segmentacao */
					cd_classificacao_item_w	:= lpad(c_rec_glosa_w.ie_segmentacao,2,'0');
				elsif	(c_segmentacao_w.ie_codificacao = 'TR') then /* Tipo de relacao com a OPS */
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_relacao(ie_tipo_relacao_w);
				elsif	(c_segmentacao_w.ie_codificacao = 'TA') then /* Tipo de ato cooperado */
					if	(c_rec_glosa_w.ie_ato_cooperado in ('1','2','3')) then
						cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_ato_cooperado(c_rec_glosa_w.ie_ato_cooperado);
					else
						cd_classificacao_item_w	:= 'A';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'CG') then /* Complemento grupo ANS */
					if	(ie_classif_grupo_ww is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_ww;
					else
						cd_classificacao_item_w	:= 'CG';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'RC') then /* Tipo de contratacao / Regumelantacao */
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(c_rec_glosa_w.ie_tipo_contratacao,c_rec_glosa_w.ie_regulamentacao);
				elsif	(c_segmentacao_w.ie_codificacao = 'JP') then
					if	(nvl(cd_cgc_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_jur_conta_cont a
						where	a.cd_cgc	= cd_cgc_prestador_w
						and	a.ie_tipo_conta	= 'P'
						and	nvl(a.cd_estabelecimento,cd_estabelecimento_p) 	= cd_estabelecimento_p
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					elsif	(nvl(cd_pf_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_fisica_conta_ctb	a
						where	a.cd_pessoa_fisica 	= cd_pf_prestador_w
						and	a.cd_empresa		= cd_empresa_w
						and	nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
						and	a.ie_tipo_conta	= 'P'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					end if;

					select	max(cd_classificacao_atual)
					into	cd_classificacao_credito_w
					from	conta_contabil
					where	cd_conta_contabil = cd_conta_contabil_w;

					cd_conta_credito_w	:= cd_conta_contabil_w;
				elsif	(c_segmentacao_w.ie_codificacao = 'RP') then
					if	(nvl(cd_cgc_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_jur_conta_cont a
						where	a.cd_cgc	= cd_cgc_prestador_w
						and	a.ie_tipo_conta	= 'R'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					elsif	(nvl(cd_pf_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_fisica_conta_ctb	a
						where	a.cd_pessoa_fisica 	= cd_pf_prestador_w
						and	a.cd_empresa		= cd_empresa_w
						and	nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
						and	a.ie_tipo_conta	= 'R'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					end if;

					select	max(cd_classificacao_atual)
					into	cd_classificacao_credito_w
					from	conta_contabil
					where	cd_conta_contabil = cd_conta_contabil_w;

					cd_conta_credito_w	:= cd_conta_contabil_w;
				elsif	(c_segmentacao_w.ie_codificacao = 'TD') then /* Tipo de despesa*/
					if (c_rec_glosa_w.ie_proc_mat = 'P') then
						if (c_rec_glosa_w.ie_tipo_despesa is not null) then
							cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_despesa(c_rec_glosa_w.ie_tipo_despesa);
						else 
							cd_classificacao_item_w := 'TD';
						end if;
					else
						cd_classificacao_item_w := 1;
					end if;
				end if;
				
				if	(cd_classificacao_item_w is not null) then
					if	(c_segmentacao_w.ds_mascara = '00') then
						cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
					elsif	(c_segmentacao_w.ds_mascara = '0.0') then
						cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
					elsif	(c_segmentacao_w.ds_mascara = '0_') then
						cd_classificacao_item_w	:= cd_classificacao_item_w;
					else
						cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
					end if;
					
					cd_classificacao_credito_w	:= cd_classificacao_credito_w || cd_classificacao_item_w;
				end if;
			elsif	(c_segmentacao_w.ie_debito_credito = 'D') then /* Classificacao DEBITO */
				if	(c_segmentacao_w.ie_codificacao = 'CR') then /* Codigo reduzido */
					select	cd_classificacao_atual
					into	cd_classificacao_debito_w
					from	conta_contabil
					where	cd_conta_contabil	= c_segmentacao_w.cd_conta_contabil;
					
					cd_conta_debito_w	:= c_segmentacao_w.cd_conta_contabil;
				elsif	(c_segmentacao_w.ie_codificacao = 'FX') then /* Fixo */
					cd_classificacao_item_w	:= c_segmentacao_w.vl_fixo;
				elsif	(c_segmentacao_w.ie_codificacao = 'GA') then /* Grupo ANS */
					if	(ie_classif_grupo_w is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_w;
					else
						cd_classificacao_item_w	:= 'GA';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'FP') then /* Formacao de preco */
					if	(c_rec_glosa_w.ie_tipo_segurado = 'A') then
						cd_classificacao_item_w	:= '9';
					else
						cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_formacao_preco(c_rec_glosa_w.ie_preco);
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'R') then /* Regumelantacao */
					cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_regulamentacao(c_rec_glosa_w.ie_regulamentacao);
				elsif	(c_segmentacao_w.ie_codificacao = 'TC') then /* Tipo de contratacao */
					if	(c_rec_glosa_w.ie_tipo_contratacao in ('I','CE','CA')) then
						cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_contratacao(c_rec_glosa_w.ie_tipo_contratacao);
					else
						cd_classificacao_item_w	:= 'TC';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'S') then /* Segmentacao */
					cd_classificacao_item_w	:= lpad(c_rec_glosa_w.ie_segmentacao,2,'0');
				elsif	(c_segmentacao_w.ie_codificacao = 'TP') then /* Tipo de Contrato (Pessoa fisica ou Juridica) */
					if	(ie_tipo_contrato_w in ('PF','PJ')) then
						cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_tipo_pessoa_contrato(ie_tipo_contrato_w);
					else
						cd_classificacao_item_w	:= 'TP';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'TR') then /* Tipo de relacao com a OPS */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_relacao(ie_tipo_relacao_w);
				elsif	(c_segmentacao_w.ie_codificacao = 'TA') then /* Tipo de ato cooperado */
					if	(c_rec_glosa_w.ie_ato_cooperado in ('1','2','3')) then
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_ato_cooperado(c_rec_glosa_w.ie_ato_cooperado);
					else
						cd_classificacao_item_w	:= 'A';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'CG') then /* Complemento grupo ANS */
					if	(ie_classif_grupo_ww is not null) then
						cd_classificacao_item_w	:= ie_classif_grupo_ww;
					else
						cd_classificacao_item_w	:= 'CG';
					end if;
				elsif	(c_segmentacao_w.ie_codificacao = 'RC') then /* Tipo de contratacao / Regumelantacao */
					cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(c_rec_glosa_w.ie_tipo_contratacao,c_rec_glosa_w.ie_regulamentacao);
				elsif	(c_segmentacao_w.ie_codificacao = 'JP') then
					if	(nvl(cd_cgc_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_jur_conta_cont a
						where	a.cd_cgc	= cd_cgc_prestador_w
						and	a.ie_tipo_conta	= 'P'
						and	nvl(a.cd_estabelecimento,cd_estabelecimento_p) 	= cd_estabelecimento_p
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					elsif	(nvl(cd_pf_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_fisica_conta_ctb	a
						where	a.cd_pessoa_fisica 	= cd_pf_prestador_w
						and	a.cd_empresa		= cd_empresa_w
						and	nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
						and	a.ie_tipo_conta	= 'P'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					end if;

					select	max(cd_classificacao_atual)
					into	cd_classificacao_credito_w
					from	conta_contabil
					where	cd_conta_contabil = cd_conta_contabil_w;

					cd_conta_debito_w	:= cd_conta_contabil_w;
				elsif	(c_segmentacao_w.ie_codificacao = 'RP') then
					if	(nvl(cd_cgc_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_jur_conta_cont a
						where	a.cd_cgc	= cd_cgc_prestador_w
						and	a.ie_tipo_conta	= 'R'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					elsif	(nvl(cd_pf_prestador_w,0) <> 0) then
						select	max(cd_conta_contabil)
						into	cd_conta_contabil_w
						from	pessoa_fisica_conta_ctb	a
						where	a.cd_pessoa_fisica 	= cd_pf_prestador_w
						and	a.cd_empresa		= cd_empresa_w
						and	nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
						and	a.ie_tipo_conta	= 'R'
						and	dt_referencia_w between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w);
					end if;

					select	max(cd_classificacao_atual)
					into	cd_classificacao_credito_w
					from	conta_contabil
					where	cd_conta_contabil = cd_conta_contabil_w;

					cd_conta_debito_w	:= cd_conta_contabil_w;
				elsif	(c_segmentacao_w.ie_codificacao = 'TD') then /* Tipo de despesa*/
					if (c_rec_glosa_w.ie_proc_mat = 'P') then
						if (c_rec_glosa_w.ie_tipo_despesa is not null) then
							cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_despesa(c_rec_glosa_w.ie_tipo_despesa);
						else 
							cd_classificacao_item_w := 'TD';
						end if;
					else
						cd_classificacao_item_w := 1;
					end if;
				end if;
				
				if	(cd_classificacao_item_w is not null) then
					if	(c_segmentacao_w.ds_mascara = '00') then
						cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
					elsif	(c_segmentacao_w.ds_mascara = '0.0') then
						cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
					elsif	(c_segmentacao_w.ds_mascara = '0_') then
						cd_classificacao_item_w	:= cd_classificacao_item_w;
					else
						cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
					end if;
					
					cd_classificacao_debito_w	:= cd_classificacao_debito_w || cd_classificacao_item_w;
				end if;
			end if;
			end;
		end loop;
		close c_segmentacao;
		/* Remover o ultimo ponto da Classificacao */
		if	(substr(cd_classificacao_credito_w,length(cd_classificacao_credito_w),length(cd_classificacao_credito_w)) = '.') then
			cd_classificacao_credito_w	:= substr(cd_classificacao_credito_w,1,length(cd_classificacao_credito_w)-1);
		end if;
		
		if	(substr(cd_classificacao_debito_w,length(cd_classificacao_debito_w),length(cd_classificacao_debito_w)) = '.') then
			cd_classificacao_debito_w	:= substr(cd_classificacao_debito_w,1,length(cd_classificacao_debito_w)-1);
		end if;

		if	(cd_conta_credito_w is null) then
			cd_conta_credito_w	:= ctb_obter_conta_classif(cd_classificacao_credito_w,dt_referencia_w,cd_estabelecimento_p);
		end if;

		if	(cd_conta_debito_w is null) then
			cd_conta_debito_w	:= ctb_obter_conta_classif(cd_classificacao_debito_w,dt_referencia_w,cd_estabelecimento_p);
		end if;
		
		if	(nr_seq_atualizacao_p is not null) then
			if	(nr_seq_esquema_w is null) then
				pls_gravar_mov_contabil(nr_seq_atualizacao_p,
							1,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							nm_usuario_p,
							nr_seq_esquema_w,
							null,
							null,
							c_rec_glosa_w.nr_seq_resumo_item,
							c_tipo_movimento_w.ie_tipo_movto);
			elsif	((cd_conta_credito_w is null) or
				(cd_conta_debito_w is null)) then
				pls_gravar_mov_contabil(nr_seq_atualizacao_p,
							2,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							nm_usuario_p,
							nr_seq_esquema_w,
							null,
							null,
							c_rec_glosa_w.nr_seq_resumo_item,
							c_tipo_movimento_w.ie_tipo_movto);
			end if;
		end if;

		if	(c_tipo_movimento_w.ie_tipo_movto = '21') then
			begin
			
			update	pls_conta_rec_resumo_item
			set	cd_conta_cred		= cd_conta_credito_w,
				cd_conta_deb		= cd_conta_debito_w,
				cd_historico		= cd_historico_padrao_w,
				cd_classif_cred		= cd_classificacao_credito_w,
				cd_classif_deb		= cd_classificacao_debito_w,
				nr_seq_esquema		= nr_seq_esquema_w,
				nr_seq_grupo_ans	= nr_seq_grupo_ans_w
			where	rowid			= c_rec_glosa_w.nr_id;
			end;
		elsif	(c_tipo_movimento_w.ie_tipo_movto = '20') then
			begin
			update	pls_conta_rec_resumo_item
			set	cd_conta_cred_apres	= cd_conta_credito_w,
				cd_conta_deb_apres	= cd_conta_debito_w,
				cd_historico_apres	= cd_historico_padrao_w,
				cd_classif_cred_apres	= cd_classificacao_credito_w,
				cd_classif_deb_apres	= cd_classificacao_debito_w,
				nr_seq_esquema_apres	= nr_seq_esquema_w,
				nr_seq_grupo_ans	= nr_seq_grupo_ans_w
			where	rowid			= c_rec_glosa_w.nr_id;
			end;
		elsif	(c_tipo_movimento_w.ie_tipo_movto = '22') then
			begin
			update	pls_conta_rec_resumo_item
			set	cd_conta_cred_pag	= cd_conta_credito_w,
				cd_conta_deb_pag	= cd_conta_debito_w,
				cd_historico_pag	= cd_historico_padrao_w,
				cd_classif_cred_pag	= cd_classificacao_credito_w,
				cd_classif_deb_pag	= cd_classificacao_debito_w,
				nr_seq_esquema_pag	= nr_seq_esquema_w,
				nr_seq_grupo_ans	= nr_seq_grupo_ans_w
			where	rowid			= c_rec_glosa_w.nr_id;
			end;			
		end if;
		
		qt_movimento_w	:= qt_movimento_w + 1;
		
		if	(mod(qt_movimento_w,300) = 0) then
			commit;
		end if;
		end;
	end loop;
	close c_tipo_movimento;
	end;
end loop;
close c_rec_glosa;

qt_movimento_p	:= qt_movimento_w;
--commit;

end ctb_pls_atualizar_rec_glosa;
/