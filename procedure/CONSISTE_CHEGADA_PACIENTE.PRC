create or replace
procedure consiste_chegada_paciente(
		nr_prescricao_p			number,
		nr_seq_paciente_p			number,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
		cd_pessoa_fisica_p		out	varchar,
		ie_regra_setor_p		out	varchar2) is

permite_chegada_sem_prescr_w	varchar2(1);

begin

if	(nr_prescricao_p is not null) then
	begin
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_p
	from	prescr_medica
	where	nr_atendimento	is null
	and	nr_prescricao	= nr_prescricao_p;
	end;
end if;

if	(cd_pessoa_fisica_p is null) then
	begin
	/* Quimioterapia - Par�metro [106] - Permite gerar chegada do paciente sem prescri��o */
	obter_param_usuario(3130, 106, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, permite_chegada_sem_prescr_w);

	if	(permite_chegada_sem_prescr_w = 'S') then
		begin
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_p
		from	paciente_setor
		where	nr_seq_paciente = nr_seq_paciente_p;
		end;
	end if;
	end;
end if;

if	(cd_pessoa_fisica_p is not null) then
	begin
	select	decode(count(*), 0, 'N', 'S')
	into	ie_regra_setor_p
	from	regra_setor_atend_quimio
	where	ie_situacao = 'A';
	end;
end if;

end consiste_chegada_paciente;
/
