create or replace
procedure inserir_dados_dacon(nr_seq_dacon_p	number,
			      nm_usuario_p	varchar2) is 

dt_referencia_w	date;
nr_seq_nota_fiscal_w	number(10);

cursor C01 is
	select 	n.nr_sequencia
	from	nota_fiscal n,
		operacao_nota o
	where	n.cd_operacao_nf = o.cd_operacao_nf
	and	o.ie_servico = 'S' -- nota de servi�o
	and	o.ie_operacao_fiscal = 'S' --saida
	and	n.ie_situacao = 1 -- ativa
	and	n.dt_atualizacao_estoque is not null  -- nota calculada
	and	to_char(n.dt_emissao,'mm/yyyy') = to_char(dt_referencia_w,'mm/yyyy');


begin

select 	dt_referencia
into	dt_referencia_w
from	dacon
where	nr_sequencia = nr_seq_dacon_p;	

open c01;
	loop
	fetch c01 into	
		nr_seq_nota_fiscal_w;
	exit when c01%notfound;
		insert 
		into dacon_nota_fiscal	(nr_sequencia,
					 dt_atualizacao,
					 nm_usuario,
					 dt_atualizacao_nrec,
					 nm_usuario_nrec,
					 nr_seq_nota_fiscal,
					 nr_seq_dacon)
		values			(dacon_nota_fiscal_seq.nextval,
					 sysdate,
					 nm_usuario_p,
					 sysdate,
					 nm_usuario_p,
					 nr_seq_nota_fiscal_w,
					 nr_seq_dacon_p);
				
		
		commit;
	end loop;
	close C01;
commit;

update dacon set dt_geracao = sysdate
where nr_sequencia = nr_seq_dacon_p;			

commit;

end inserir_dados_dacon;
/