create or replace
procedure apae_vincular_aval_externa(nr_aval_clinica_externa_p	number,
						 nr_sequencia_p 	    	number) is

begin

if	(nr_sequencia_p is not null) and
	(nr_aval_clinica_externa_p is not null) then

	update	aval_pre_anestesica
	set	nr_aval_clinica_externa	= nr_aval_clinica_externa_p
	where	nr_sequencia		= nr_sequencia_p;
	
	commit;

end if;

end apae_vincular_aval_externa;
/