create or replace
procedure pls_gerar_sip
			(	nr_seq_lote_sip_p	Number,
				ie_despesa_assist_p	Varchar2,
				ie_anexo_ii_p		Varchar2,
				ie_anexo_iii_p		Varchar2,
				ie_anexo_iv_p		Varchar2,
				nm_usuario_p		Varchar2) is 
			
dt_alteracao_proc_w		Date;
dt_geracao_sip_w		Date;
ie_sip_regra_despesa_w		Varchar2(1);
			
begin

select	nvl(max(dt_geracao_sip),sysdate)
into	dt_geracao_sip_w
from	pls_lote_sip
where	nr_sequencia	= nr_seq_lote_sip_p;

/* Leitura do par�metro 2 da OPS - Sistema de informa��es de produtos (SIP) */
begin
select	nvl(max(obter_valor_param_usuario(1231, 2, Obter_Perfil_Ativo, nm_usuario_p, 0)), 'N')
into	ie_sip_regra_despesa_w
from	dual;
exception
when others then
	ie_sip_regra_despesa_w	:= 'N';
end;

/* Despesas n�o assistenciais */
if	(ie_despesa_assist_p	= 'S') then
	sip_calcular_despesa_assist(nr_seq_lote_sip_p, nm_usuario_p);
end if;
/* Anexo II */
if	(ie_anexo_ii_p	= 'S') then
	delete	from w_sip_item_despesa
	where	nr_seq_lote_sip	= nr_seq_lote_sip_p;

	select	nvl(max(dt_atualizacao),sysdate -1)
	into	dt_alteracao_proc_w
	from	sip_procedimento;
	if	(dt_alteracao_proc_w > dt_geracao_sip_w) then
		delete	from sip_anexo_ii_procedimento
		where	nr_seq_lote_sip	= nr_seq_lote_sip_p;
	end if;

	delete	from pls_desp_sip_diops_conta
	where	nr_seq_lote_sip	= nr_seq_lote_sip_p;

	delete	from w_sip_grupo_beneficiario
	where	nr_seq_lote_sip	= nr_seq_lote_sip_p;
	
	if	(ie_sip_regra_despesa_w	= 'N') then
		sip_gerar_item_despesa(nr_seq_lote_sip_p, nm_usuario_p);
		sip_calcular_coparticipacao(nr_seq_lote_sip_p, nm_usuario_p);
	elsif	(ie_sip_regra_despesa_w	= 'S') then
		sip_gerar_anexo_ii(nr_seq_lote_sip_p, nm_usuario_p);
	end if;
	/* Anexo II-A */	
	sip_gerar_grupo_beneficiario(nr_seq_lote_sip_p, nm_usuario_p);	
end if;
/* Anexo III */
if	(ie_anexo_iii_p	= 'S') then
	delete	from w_sip_anexo
	where	nr_seq_lote_sip	= nr_seq_lote_sip_p;

	select	nvl(max(dt_atualizacao),sysdate -1)
	into	dt_alteracao_proc_w
	from	sip_estrut_anexo_regra;
	if	(dt_alteracao_proc_w >= dt_geracao_sip_w) then
		delete	from sip_anexo_iii_procedimento
		where	nr_seq_lote_sip	= nr_seq_lote_sip_p;
	end if;
	
	sip_gerar_anexo_iii(nr_seq_lote_sip_p, nm_usuario_p);
end if;
/* Anexo IV */
if	(ie_anexo_iv_p	= 'S') then
	delete	from w_sip_atencao_saude
	where	nr_seq_lote_sip	= nr_seq_lote_sip_p;
	
	delete	from sip_anexo_iv_procedimento
	where	nr_seq_lote_sip	= nr_seq_lote_sip_p;
	
	sip_gerar_atencao_saude(nr_seq_lote_sip_p, nm_usuario_p);
end if;

update	pls_lote_sip
set	dt_geracao_sip	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_lote_sip_p;

commit;

end pls_gerar_sip;
/
