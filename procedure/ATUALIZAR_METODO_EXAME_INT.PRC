create or replace
procedure atualizar_metodo_exame_int(	nr_prescricao_p		number,
					nr_seq_exame_p		number,
					ds_sigla_equip_p	varchar2,					
					cd_metodo_integracao_p  varchar2) is 

nr_seq_metodo_result_w		number(10);
					
begin

nr_seq_metodo_result_w :=  obter_metodo_regra_int(nr_prescricao_p,nr_seq_exame_p, ds_sigla_equip_p, cd_metodo_integracao_p);

update 	exame_lab_result_item
set 	nr_seq_metodo = nr_seq_metodo_result_w
where 	nr_seq_resultado = (	select	a.nr_seq_resultado 
				from 	exame_lab_resultado a
                                		where 	a.nr_prescricao = nr_prescricao_p)
and	nr_seq_exame = nr_seq_exame_p;

commit;

end atualizar_metodo_exame_int;
/
