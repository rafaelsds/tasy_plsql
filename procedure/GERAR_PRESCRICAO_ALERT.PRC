create or replace
procedure Gerar_prescricao_alert(dt_prescricao_p date, nm_usuario_p varchar2,ie_medico_p varchar2) is 

cd_estabelecimento_w		prescr_medica_alert.cd_estabelecimento%type;
cd_amostra_cih_w       		prescr_material_alert.cd_amostra_cih%type;
cd_intervalo_w       		prescr_material_alert.cd_intervalo%type;
cd_kit_material_w       	prescr_material_alert.cd_kit_material%type;
cd_local_estoque_w      	prescr_material_alert.cd_local_estoque%type;
cd_material_w       			prescr_material_alert.cd_material%type;
cd_material_baixa_w     	prescr_material_alert.cd_material_baixa%type;
cd_material_kit_w       	prescr_material_alert.cd_material_kit%type;
cd_mat_sem_apresent_w   	prescr_material_alert.cd_mat_sem_apresent%type;
cd_medico_w       			prescr_material_alert.cd_medico%type;
cd_microorganismo_cih_w 	prescr_material_alert.cd_microorganismo_cih%type;
cd_motivo_baixa_w       	prescr_material_alert.cd_motivo_baixa%type;
cd_perfil_ativo_w       	prescr_material_alert.cd_perfil_ativo%type;
cd_topografia_cih_w     	prescr_material_alert.cd_topografia_cih%type;
cd_unidade_medida_w     	prescr_material_alert.cd_unidade_medida%type;
cd_unidade_medida_dose_w   prescr_material_alert.cd_unidade_medida_dose%type;
cd_unidade_medida_sol_w    prescr_material_alert.cd_unidade_medida_sol%type;
ds_diagnostico_w       		prescr_material_alert.ds_diagnostico%type;
ds_dose_diferenciada_w     prescr_material_alert.ds_dose_diferenciada%type;
ds_horarios_w       			prescr_material_alert.ds_horarios%type;
ds_justificativa_w       	prescr_material_alert.ds_justificativa%type;
ds_motivo_susp_w       		prescr_material_alert.ds_motivo_susp%type;
ds_obs_alert_w       		prescr_material_alert.ds_observacao%type;
ds_observacao_w		 		prescr_material.ds_observacao%type;
dt_atualizacao_w       		prescr_material_alert.dt_atualizacao%type;
dt_atualizacao_nrec_w      prescr_material_alert.dt_atualizacao_nrec%type;
dt_proxima_dose_w       	prescr_material_alert.dt_proxima_dose%type;
dt_suspensao_w       		prescr_material_alert.dt_suspensao%type;
dt_suspensao_progr_w       prescr_material_alert.dt_suspensao_progr%type;
hr_dose_especial_w       	prescr_material_alert.hr_dose_especial%type;
hr_prim_horario_w       	prescr_material_alert.hr_prim_horario%type;
ie_acm_w       				prescr_material_alert.ie_acm%type;
ie_administrar_w       		prescr_material_alert.ie_administrar%type;
ie_agrupador_w       		prescr_material_alert.ie_agrupador%type;
ie_aplicar_w       			prescr_material_alert.ie_aplicar%type;
ie_aplic_bolus_w       		prescr_material_alert.ie_aplic_bolus%type;
ie_aplic_lenta_w       		prescr_material_alert.ie_aplic_lenta%type;
ie_bomba_infusao_w       	prescr_material_alert.ie_bomba_infusao%type;
ie_calc_automatico_w       prescr_material_alert.ie_calc_automatico%type;
ie_dose_espec_agora_w      prescr_material_alert.ie_dose_espec_agora%type;
ie_gerar_lote_w       		prescr_material_alert.ie_gerar_lote%type;
ie_lado_w       				prescr_material_alert.ie_lado%type;
ie_medicacao_paciente_w    prescr_material_alert.ie_medicacao_paciente%type;
ie_objetivo_w       			prescr_material_alert.ie_objetivo%type;
ie_origem_disp_w       		prescr_material_alert.ie_origem_disp%type;
ie_origem_inf_w       		prescr_material_alert.ie_origem_inf%type;
ie_origem_infeccao_w       prescr_material_alert.ie_origem_infeccao%type;
ie_regra_disp_w       		prescr_material_alert.ie_regra_disp%type;
ie_se_necessario_w       	prescr_material_alert.ie_se_necessario%type;
ie_suspenso_w       			prescr_material_alert.ie_suspenso%type;
ie_urgencia_w       			prescr_material_alert.ie_urgencia%type;
ie_via_aplicacao_w       	prescr_material_alert.ie_via_aplicacao%type;
nm_usuario_w       			prescr_material_alert.nm_usuario%type;
nm_usuario_nrec_w       	prescr_material_alert.nm_usuario_nrec%type;
nm_usuario_susp_w       	prescr_material_alert.nm_usuario_susp%type;
nr_agrupamento_w       		prescr_material_alert.nr_agrupamento%type;
nr_dia_util_w       			prescr_material_alert.nr_dia_util%type;
nr_ocorrencia_w       		prescr_material_alert.nr_ocorrencia%type;
nr_prescricao_w       		prescr_material_alert.nr_prescricao%type;
nr_prescr_alert_w		 		prescr_material_alert.nr_prescricao%type;	
nr_seq_alert_w					prescr_material_alert.nr_sequencia%type;
qt_conversao_dose_w       	prescr_material_alert.qt_conversao_dose%type;
qt_dias_liberado_w       	prescr_material_alert.qt_dias_liberado%type;
qt_dias_solicitado_w      	prescr_material_alert.qt_dias_solicitado%type;
qt_dias_util_w       		prescr_material_alert.qt_dias_util%type;
qt_dose_w       				prescr_material_alert.qt_dose%type;
qt_dose_especial_w      	prescr_material_alert.qt_dose_especial%type;
qt_hora_aplicacao_w       	prescr_material_alert.qt_hora_aplicacao%type;
qt_hora_intervalo_w       	prescr_material_alert.qt_hora_intervalo%type;
qt_material_w       			prescr_material_alert.qt_material%type;
qt_min_aplicacao_w       	prescr_material_alert.qt_min_aplicacao%type;
qt_min_aplic_dose_esp_w    prescr_material_alert.qt_min_aplic_dose_esp%type;
qt_min_intervalo_w       	prescr_material_alert.qt_min_intervalo%type;
qt_solucao_w       			prescr_material_alert.qt_solucao%type;
qt_vel_infusao_w       		prescr_material_alert.qt_vel_infusao%type;
qt_total_dispensar_w		 	prescr_material_alert.qt_total_dispensar%type;
nr_atendimento_w	 			prescr_medica_alert.nr_atendimento%type;
ds_obs_med_alert_w 			prescr_medica_alert.ds_observacao%type;
nr_sequencia_ww				prescr_material.nr_sequencia%type;
qt_unitaria_w				prescr_material.nr_sequencia%type;
ds_erro_w						varchar2(2000);

Cursor C01 is
	select	a.nr_prescricao,
				substr(a.ds_observacao || ' Prescrição gerada via Integração Alert ( Prescrição Alert: '|| a.nr_prescricao || ')',1,4000),
				a.nr_atendimento,
				a.cd_perfil_ativo,
				a.cd_estabelecimento
	from		prescr_medica_alert a
	where		trunc(a.dt_prescricao,'dd') = trunc(dt_prescricao_p,'dd')
	and	 		not exists(select 1
							from prescr_Medica b
							where b.ds_observacao like '%'||' Prescrição gerada via Integração Alert ( Prescrição Alert: '|| a.nr_prescricao||'%'
							and	b.nr_atendimento = a.nr_atendimento
							and	trunc(b.dt_prescricao,'dd') = trunc(dt_prescricao_p,'dd'));		

Cursor C02 is
	select	cd_amostra_cih,
				cd_intervalo,
				cd_kit_material,
				cd_local_estoque,
				cd_material,
				cd_material_baixa,
				cd_material_kit,
				cd_mat_sem_apresent,
				cd_medico,
				cd_microorganismo_cih,
				cd_motivo_baixa,
				cd_perfil_ativo,
				cd_topografia_cih,
				cd_unidade_medida,
				cd_unidade_medida_dose,
				cd_unidade_medida_sol,
				ds_diagnostico,
				ds_dose_diferenciada,
				ds_horarios,
				ds_justificativa,
				ds_motivo_susp,
				ds_observacao,
				dt_atualizacao,
				dt_atualizacao_nrec,
				dt_proxima_dose,
				dt_suspensao,
				dt_suspensao_progr,
				hr_dose_especial,
				hr_prim_horario,
				ie_acm,
				ie_administrar,
				ie_agrupador,
				ie_aplicar,
				ie_aplic_bolus,
				ie_aplic_lenta,
				ie_bomba_infusao,
				ie_calc_automatico,
				ie_dose_espec_agora,
				ie_gerar_lote,
				ie_lado,
				ie_medicacao_paciente,
				ie_objetivo,
				ie_origem_disp,
				ie_origem_inf,
				ie_origem_infeccao,
				ie_regra_disp,
				ie_se_necessario,
				ie_suspenso,
				ie_urgencia,
				ie_via_aplicacao,
				nm_usuario,
				nm_usuario_nrec,
				nm_usuario_susp,
				nr_agrupamento,
				nr_dia_util,
				nr_ocorrencia,
				qt_conversao_dose,
				qt_dias_liberado,
				qt_dias_solicitado,
				qt_dias_util,
				qt_dose,
				qt_dose_especial,
				qt_hora_aplicacao,
				qt_hora_intervalo,
				qt_material,
				qt_min_aplicacao,
				qt_min_aplic_dose_esp,
				qt_min_intervalo,
				qt_solucao,
				qt_vel_infusao,
				qt_total_dispensar,
				qt_unitaria
	from 		prescr_material_alert
	where 	nr_Prescricao = nr_prescr_alert_w;
	

begin

open C01;
loop
fetch C01 into	
	nr_prescr_alert_w,
	ds_obs_med_alert_w,
	nr_atendimento_w,
	cd_perfil_ativo_w,
	cd_estabelecimento_w;
exit when C01%notfound;
	begin
	select prescr_medica_seq.nextval
	into 	 nr_prescricao_w
	from   dual;
	
	insert into prescr_medica(CD_CGC_SOLIC,
					CD_ESPECIALIDADE,
					CD_ESTABELECIMENTO,
					CD_LOCAL_ESTOQUE,
					CD_MEDICO,
					CD_MOTIVO_BAIXA,
					CD_PERFIL_ATIVO,
					CD_PESSOA_FISICA,
					CD_PRESCRITOR,
					CD_PROTOCOLO,
					CD_RECEM_NATO,
					CD_SENHA,
					CD_SETOR_ATENDIMENTO,
					CD_SETOR_ENTREGA,
					CD_SETOR_ORIG,
					CD_UNID_BASICA,
					CD_UNID_COMPL,
					DS_DADO_CLINICO,
					DS_ENDERECO_ENTREGA,
					DS_EXAME_ANTERIOR,
					DS_JUSTIFICATIVA,
					DS_MEDICACAO_USO,
					DS_MOTIVO_SUSP,
					DS_OBSERVACAO,
					DS_PEDIDO_EXT,
					DT_ATUALIZACAO,
					DT_EMISSAO_REQ_LAB,
					DT_ENDOSSO,
					DT_ENTREGA,
					DT_INICIO_PRESCR,
					DT_PRESCRICAO,
					DT_PRIMEIRO_HORARIO,
					DT_REVISAO,
					DT_SUSPENSAO,
					DT_VALIDADE_PRESCR,
					IE_ADEP,
					IE_AJUSTA_VALIDADE_ATEND,
					IE_CALCULO_CLEARENCE,
					IE_CARTAO_EMERGENCIA,
					IE_EMERGENCIA,
					IE_EXAME_ANTERIOR,
					IE_FUNCAO_PRESCRITOR,
					IE_HEMODIALISE,
					IE_MOTIVO_PRESCRICAO,
					IE_ORIGEM_INF,
					IE_PRESCRICAO_ALTA,
					IE_PRESCRITOR_AUX,
					IE_PRESCR_EMERGENCIA,
					IE_RECEM_NATO,
					IE_TIPO_PRESCR_CIRUR,
					NM_MAQUINA,
					NM_MEDICO_EXTERNO,
					NM_USUARIO,
					NM_USUARIO_IGNORA_NUT,
					NM_USUARIO_ORIGINAL,
					NM_USUARIO_REVISAO,
					NM_USUARIO_SUSP,
					NR_ATENDIMENTO,
					NR_DOC_CONV,
					NR_HORAS_VALIDADE,
					NR_PRESCRICAO,
					NR_PRIORIDADE,
					NR_RECEM_NATO,
					NR_SEQ_EXTERNO,
					NR_SEQ_MOTIVO_SUSP,
					NR_SEQ_PEND_PAC_ACAO,
					QT_ALTURA_CM,
					QT_CREATININA,
					QT_PESO)
select	CD_CGC_SOLIC,
					CD_ESPECIALIDADE,
					cd_estabelecimento_w,
					CD_LOCAL_ESTOQUE,
					CD_MEDICO,
					CD_MOTIVO_BAIXA,
					CD_PERFIL_ATIVO_w,
					CD_PESSOA_FISICA,
					CD_PRESCRITOR,
					CD_PROTOCOLO,
					CD_RECEM_NATO,
					CD_SENHA,
					CD_SETOR_ATENDIMENTO,
					CD_SETOR_ENTREGA,
					CD_SETOR_ORIG,
					CD_UNID_BASICA,
					CD_UNID_COMPL,
					DS_DADO_CLINICO,
					DS_ENDERECO_ENTREGA,
					DS_EXAME_ANTERIOR,
					DS_JUSTIFICATIVA,
					DS_MEDICACAO_USO,
					DS_MOTIVO_SUSP,
					ds_obs_med_alert_w,
					DS_PEDIDO_EXT,
					DT_ATUALIZACAO,
					DT_EMISSAO_REQ_LAB,
					DT_ENDOSSO,
					DT_ENTREGA,
					DT_INICIO_PRESCR,
					DT_PRESCRICAO,
					DT_PRIMEIRO_HORARIO,
					DT_REVISAO,
					DT_SUSPENSAO,
					DT_VALIDADE_PRESCR,
					IE_ADEP,
					IE_AJUSTA_VALIDADE_ATEND,
					IE_CALCULO_CLEARENCE,
					IE_CARTAO_EMERGENCIA,
					IE_EMERGENCIA,
					IE_EXAME_ANTERIOR,
					IE_FUNCAO_PRESCRITOR,
					IE_HEMODIALISE,
					IE_MOTIVO_PRESCRICAO,
					IE_ORIGEM_INF,
					IE_PRESCRICAO_ALTA,
					IE_PRESCRITOR_AUX,
					IE_PRESCR_EMERGENCIA,
					IE_RECEM_NATO,
					IE_TIPO_PRESCR_CIRUR,
					NM_MAQUINA,
					NM_MEDICO_EXTERNO,
					NM_USUARIO,
					NM_USUARIO_IGNORA_NUT,
					NM_USUARIO_ORIGINAL,
					NM_USUARIO_REVISAO,
					NM_USUARIO_SUSP,
					NR_ATENDIMENTO_w,
					NR_DOC_CONV,
					NR_HORAS_VALIDADE,
					nr_prescricao_w,
					NR_PRIORIDADE,
					NR_RECEM_NATO,
					NR_SEQ_EXTERNO,
					NR_SEQ_MOTIVO_SUSP,
					NR_SEQ_PEND_PAC_ACAO,
					QT_ALTURA_CM,
					QT_CREATININA,
					QT_PESO
		from  	prescr_medica_alert
		where 	nr_prescricao = nr_prescr_alert_w;
		commit;
		
		ds_observacao_w := '';
		open C02;
		loop
		fetch C02 into	
			cd_amostra_cih_w,
			cd_intervalo_w,
			cd_kit_material_w,
			cd_local_estoque_w,
			cd_material_w,
			cd_material_baixa_w,
			cd_material_kit_w,
			cd_mat_sem_apresent_w,
			cd_medico_w,
			cd_microorganismo_cih_w,
			cd_motivo_baixa_w,
			cd_perfil_ativo_w,
			cd_topografia_cih_w,
			cd_unidade_medida_w,
			cd_unidade_medida_dose_w,
			cd_unidade_medida_sol_w,
			ds_diagnostico_w,
			ds_dose_diferenciada_w,
			ds_horarios_w,
			ds_justificativa_w,
			ds_motivo_susp_w,
			ds_obs_alert_w,
			dt_atualizacao_w,
			dt_atualizacao_nrec_w,
			dt_proxima_dose_w,
			dt_suspensao_w,
			dt_suspensao_progr_w,
			hr_dose_especial_w,
			hr_prim_horario_w,
			ie_acm_w,
			ie_administrar_w,
			ie_agrupador_w,
			ie_aplicar_w,
			ie_aplic_bolus_w,
			ie_aplic_lenta_w,
			ie_bomba_infusao_w,
			ie_calc_automatico_w,
			ie_dose_espec_agora_w,
			ie_gerar_lote_w,
			ie_lado_w,
			ie_medicacao_paciente_w,
			ie_objetivo_w,
			ie_origem_disp_w,
			ie_origem_inf_w,
			ie_origem_infeccao_w,
			ie_regra_disp_w,
			ie_se_necessario_w,
			ie_suspenso_w,
			ie_urgencia_w,
			ie_via_aplicacao_w,
			nm_usuario_w,
			nm_usuario_nrec_w,
			nm_usuario_susp_w,
			nr_agrupamento_w,
			nr_dia_util_w,
			nr_ocorrencia_w,
			qt_conversao_dose_w,
			qt_dias_liberado_w,
			qt_dias_solicitado_w,
			qt_dias_util_w,
			qt_dose_w,
			qt_dose_especial_w,
			qt_hora_aplicacao_w,
			qt_hora_intervalo_w,
			qt_material_w,
			qt_min_aplicacao_w,
			qt_min_aplic_dose_esp_w,
			qt_min_intervalo_w,
			qt_solucao_w,
			qt_vel_infusao_w,
			qt_total_dispensar_w,
			qt_unitaria_w;
		exit when C02%notfound;
			begin
			
			ds_observacao_w := substr(ds_obs_alert_w || ' Item gerado via Integração Alert ( Prescrição Alert: '|| nr_prescr_alert_w||' Sequência item alert: '||nr_seq_alert_w || ')',1,4000);
			
			/*select 	prescr_material_seq.nextval
			into 	nr_sequencia_ww
			from	dual;*/
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_ww
			from	prescr_material
			where	nr_prescricao	= nr_prescricao_w;
			
			insert into prescr_Material(cd_amostra_cih,
						cd_intervalo,
						cd_kit_material,
						cd_local_estoque,
						cd_material,
						cd_material_baixa,
						cd_material_kit,
						cd_mat_sem_apresent,
						cd_medico,
						cd_microorganismo_cih,
						cd_motivo_baixa,
						cd_perfil_ativo,
						cd_topografia_cih,
						cd_unidade_medida,
						cd_unidade_medida_dose,
						cd_unidade_medida_sol,
						ds_diagnostico,
						ds_dose_diferenciada,
						ds_horarios,
						ds_justificativa,
						ds_motivo_susp,
						ds_observacao,
						dt_atualizacao,
						dt_atualizacao_nrec,
						dt_proxima_dose,
						dt_suspensao,
						dt_suspensao_progr,
						hr_dose_especial,
						hr_prim_horario,
						ie_acm,
						ie_administrar,
						ie_agrupador,
						ie_aplicar,
						ie_aplic_bolus,
						ie_aplic_lenta,
						ie_bomba_infusao,
						ie_calc_automatico,
						ie_dose_espec_agora,
						ie_gerar_lote,
						ie_lado,
						ie_medicacao_paciente,
						ie_objetivo,
						ie_origem_disp,
						ie_origem_inf,
						ie_origem_infeccao,
						ie_regra_disp,
						ie_se_necessario,
						ie_suspenso,
						ie_urgencia,
						ie_via_aplicacao,
						nm_usuario,
						nm_usuario_nrec,
						nm_usuario_susp,
						nr_agrupamento,
						nr_dia_util,
						nr_ocorrencia,
						nr_prescricao,
						nr_sequencia,
						qt_conversao_dose,
						qt_dias_liberado,
						qt_dias_solicitado,
						qt_dias_util,
						qt_dose,
						qt_dose_especial,
						qt_hora_aplicacao,
						qt_hora_intervalo,
						qt_material,
						qt_min_aplicacao,
						qt_min_aplic_dose_esp,
						qt_min_intervalo,
						qt_solucao,
						qt_vel_infusao,
						qt_total_dispensar,
						qt_unitaria)
			values (cd_amostra_cih_w,
						cd_intervalo_w,
						cd_kit_material_w,
						cd_local_estoque_w,
						cd_material_w,
						cd_material_baixa_w,
						cd_material_kit_w,
						cd_mat_sem_apresent_w,
						cd_medico_w,
						cd_microorganismo_cih_w,
						cd_motivo_baixa_w,
						cd_perfil_ativo_w,
						cd_topografia_cih_w,
						cd_unidade_medida_w,
						cd_unidade_medida_dose_w,
						cd_unidade_medida_sol_w,
						ds_diagnostico_w,
						ds_dose_diferenciada_w,
						ds_horarios_w,
						ds_justificativa_w,
						ds_motivo_susp_w,
						ds_observacao_w,
						dt_atualizacao_w,
						dt_atualizacao_nrec_w,
						dt_proxima_dose_w,
						dt_suspensao_w,
						dt_suspensao_progr_w,
						hr_dose_especial_w,
						hr_prim_horario_w,
						ie_acm_w,
						ie_administrar_w,
						ie_agrupador_w,
						ie_aplicar_w,
						ie_aplic_bolus_w,
						ie_aplic_lenta_w,
						ie_bomba_infusao_w,
						ie_calc_automatico_w,
						ie_dose_espec_agora_w,
						ie_gerar_lote_w,
						ie_lado_w,
						ie_medicacao_paciente_w,
						ie_objetivo_w,
						ie_origem_disp_w,
						nvl(ie_origem_inf_w,1),
						ie_origem_infeccao_w,
						ie_regra_disp_w,
						ie_se_necessario_w,
						ie_suspenso_w,
						ie_urgencia_w,
						ie_via_aplicacao_w,
						nm_usuario_w,
						nm_usuario_nrec_w,
						nm_usuario_susp_w,
						nr_agrupamento_w,
						nr_dia_util_w,
						nr_ocorrencia_w,
						nr_prescricao_w,
						nr_sequencia_ww,
						qt_conversao_dose_w,
						qt_dias_liberado_w,
						qt_dias_solicitado_w,
						qt_dias_util_w,
						qt_dose_w,
						qt_dose_especial_w,
						qt_hora_aplicacao_w,
						qt_hora_intervalo_w,
						qt_material_w,
						qt_min_aplicacao_w,
						qt_min_aplic_dose_esp_w,
						qt_min_intervalo_w,
						qt_solucao_w,
						qt_vel_infusao_w,
						qt_total_dispensar_w,
						qt_unitaria_w);
						
				commit;
						
				gerar_reconst_diluicao(nr_prescricao_w,nr_sequencia_ww,'A');	
				commit;
				
				Ajustar_Prescr_Material(nr_prescricao_w,nr_sequencia_ww);
			end;
		end loop;
		close C02;
		
	Gerar_kit_Prescricao(cd_estabelecimento_w,nr_prescricao_w,0,nm_usuario_p);
	liberar_prescricao(nr_prescricao_w, nr_atendimento_w, ie_medico_p, cd_perfil_ativo_w, nm_usuario_p, 'N', ds_erro_w);	
	end;
end loop;
close C01;
	
commit;

end Gerar_prescricao_alert;
/