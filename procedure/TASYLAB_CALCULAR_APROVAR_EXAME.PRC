create or replace 
procedure tasylab_Calcular_Aprovar_Exame(	nr_seq_result_p	number,
					nr_prescricao_p	number,
					nr_seq_prescr_p	number
					) is

nr_seq_material_w		number(10);
nr_seq_exame_w			number(10);
qt_resultado_w			number(15,4);
pr_resultado_w			number(9,4);
ds_resultado_w			varchar2(2000);
ie_status_receb_w		number(2);
ie_status_novo_receb_w		number(2);
ds_consistencia_w		varchar2(4000);
ie_aprovar_todos_interf_w	varchar2(1);
qt_exames_consist_w		number(5);
ie_status_receb_lote_w		number(2);

cursor c02 is
	select	nr_seq_exame,
		qt_resultado,
		pr_resultado,
		ds_resultado
	from exame_lab_result_item
	where nr_seq_resultado	= nr_seq_result_p
	  and nr_seq_prescr	= nr_seq_prescr_p
	  and ((qt_resultado <> 0) or (pr_resultado <> 0) or (ds_resultado is not null))
	order by nr_sequencia;


begin

select	max(nr_seq_material)
into	nr_seq_material_w
from exame_lab_result_item
where nr_seq_resultado	= nr_seq_result_p
  and nr_seq_prescr	= nr_seq_prescr_p
  and nr_seq_material is not null;

open c02;
loop
fetch c02 into	nr_seq_exame_w,
		qt_resultado_w,
		pr_resultado_w,
		ds_resultado_w;
	exit when c02%notfound;

	calcular_valores_exame(nr_seq_result_p,
				nr_prescricao_p,
				nr_seq_prescr_p,
				nr_seq_material_w,
				nr_seq_exame_w,
				qt_resultado_w,
				pr_resultado_w,
				ds_resultado_w);

end loop;
close c02;

commit;

end tasylab_Calcular_Aprovar_Exame;
/