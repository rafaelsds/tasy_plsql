create or replace
procedure Ageint_gerar_anexos_proc(nr_seq_ageint_p		number,
				   nr_seq_proc_interno_p	number,
				   cd_estabelecimento_p		number,
				   nm_usuario_p			varchar2) is 

				   
ds_arquivo_w	varchar2(255);
nr_seq_anexo_w	number(10,0);
qt_existe_w	number(10,0);
nr_sequencia_w	number(10,0);
								   
Cursor C01 is
	select	ds_arquivo,
		nr_sequencia
	from	ageint_proc_int_arquivo
	where	nr_seq_proc_interno				= nr_seq_proc_interno_p
	and	nvl(cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
	order by ds_arquivo;				   
				   
begin

open C01;
loop
fetch C01 into	
	ds_arquivo_w,
	nr_sequencia_w;
exit when C01%notfound;
	begin
	
	select	ageint_arq_anexo_email_seq.nextval
	into	nr_seq_anexo_w
	from	dual;
	
	select	count(*)
	into	qt_existe_w
	from	ageint_arq_anexo_email
	where	nr_seq_agenda_int	= nr_seq_ageint_p
	and	ds_arquivo		= ds_arquivo_w;
	
	if	(qt_existe_w = 0) then
	
		insert into ageint_arq_anexo_email( nr_sequencia,           
						    dt_atualizacao,
						    nm_usuario,             
						    dt_atualizacao_nrec,    
						    nm_usuario_nrec,        
						    nr_seq_agenda_int,    
						    ds_arquivo,
						    nr_seq_anexo_regra
						    )
					values	  ( nr_seq_anexo_w,
						    sysdate,
						    nm_usuario_p,
						    sysdate,
						    nm_usuario_p,
						    nr_seq_ageint_p,
						    ds_arquivo_w,
						    nr_sequencia_w);	
	end if;
	
	end;
end loop;
close C01;


commit;

end Ageint_gerar_anexos_proc;
/