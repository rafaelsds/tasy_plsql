create or replace
procedure obter_regra_ajuste_proc
                       	(		cd_estabelecimento_p 			number,
								cd_convenio_p					number,
								cd_categoria_p					varchar2,
								cd_procedimento_p				number,
								ie_origem_proced_p				number,
								ie_video_p						varchar2,
								dt_vigencia_p          			date,
								cd_tipo_acomodacao_p    		number,
								ie_tipo_atendimento_p   	 	number,
								cd_setor_atendimento_p        	number,
								cd_medico_p						varchar2,
								cd_funcao_medico_p				number,
								qt_idade_p						number,
								nr_seq_exame_lab_p				number,
								nr_seq_proc_interno_p			number,
								cd_usuario_convenio_p			varchar2,
								cd_plano_p						varchar2,
								ie_clinica_p					number,
								cd_empresa_ref_p				number,
								ie_sexo_p						varchar2,
								tx_ajuste_p				out	number,
								tx_ajuste_custo_oper_p	out	number,
								tx_ajuste_medico_p		out	number,
								tx_ajuste_partic_p		out	number,
								tx_ajuste_filme_p		out	number,
								vl_negociado_p			out	number,
								ie_preco_informado_p	out	varchar2,
								ie_glosa_p				out	varchar2,
								cd_procedimento_esp_p	out	number,
								nr_seq_regra_preco_p	out	number,
								cd_edicao_ajuste_p		out	number,
								vl_medico_p				out	number,
								vl_custo_operacional_p	out	number,
								qt_filme_p				out	number,
								nr_auxiliares_p			out	number,
								qt_porte_anestesico_p	out	number,
								pr_glosa_p				out	number,
								vl_glosa_p				out	number,
								cd_motivo_exc_conta_p	out	number,
								ie_atend_retorno_p		in	varchar2,
								qt_dias_internacao_p	in	number,
								ie_autor_particular_p	out	varchar2,
								cd_convenio_glosa_p		out	number,
								cd_categoria_glosa_p	out	varchar2,
								nr_sequencia_p			out	number,
								ie_tipo_atend_bpa_p		in	number,
								cd_procedencia_p		in	number,
								ie_beira_leito_p		varchar2,
								ie_spect_p				varchar2,
								cd_cgc_prestador_p		in	varchar2,
								cd_equipamento_p		in	number,
								nr_seq_tipo_acidente_p  	in 	number,
								cd_especialidade_medica_p 	in	number,
								vl_filme_p				out	number,
								nr_seq_cobertura_p		in	number,
								cd_setor_atend_prescr_p	in	number,
								nr_seq_classif_atend_p	in	number,
								cd_medico_resp_p		in	varchar2,
								ie_carater_inter_sus_p	in	varchar2,
								cd_dependente_p			in	number,
								nr_seq_grupo_rec_p		in	number,
								nr_seq_origem_p			in 	number,
								ie_paciente_deficiente_p	in	varchar2,
								nr_seq_classif_medico_p		in	number,
								ie_estrangeiro_p			varchar2 default null ) is

qt_pontos_w			preco_amb.qt_pontuacao%type;
cd_procedimento_esp_w		number(15)		:= 0;
nr_seq_regra_preco_w		number(10)		:= 0;
ie_preco_informado_w   		varchar2(1)		:= null;
ie_glosa_w   			varchar2(1)		:= null;
dt_inicio_vigencia_w		date;
tx_ajuste_w			number(15,4)		:= 1;
vl_negociado_w			number(15,4)		:= null;
cd_edicao_amb_w			number(6)		:= 0;
cd_edicao_ajuste_w		number(6)		:= 0;
nr_sequencia_w         		number(10,0)		:= 0;
cd_grupo_w			number(15)		:= 0;
cd_especialidade_w		number(15)		:= 0;
cd_area_w			number(15)		:= 0;
tx_ajuste_geral_w			number(15,4)		:= 1;

vl_ch_honorarios_w		number(15,4) := 1;
vl_ch_custo_oper_w	number(15,4) := 1;
vl_m2_filme_w		number(15,4) := 0;
tx_ajuste_proc_w		number(15,4) := 0;


vl_medico_w		number(15,2)		:= null;
vl_custo_operacional_w	number(15,2)		:= null;
qt_filme_w		number(15,4)		:= null;
nr_auxiliares_w		number(2)		:= null;
qt_porte_anestesico_w	number(2)		:= null;

tx_ajuste_custo_oper_w	number(15,4)		:= 1;
tx_ajuste_medico_w	number(15,4)		:= 1;
tx_ajuste_partic_w		number(15,4)	:= 1;
tx_ajuste_filme_w		number(15,4)		:= 1;
ie_credenciado_w		varchar2(01);
pr_glosa_w		number(7,4)		:= 0;
vl_glosa_w		number(15,4)		:= 0;
qt_idade_w		number(10,0);
qt_reg_w			number(10,0);
ie_origem_proced_w	number(10);
cd_motivo_exc_conta_w	number(15,0);
ie_prioridade_ajuste_proc_w	varchar2(01);
qt_dias_internacao_w	number(10,0);
dt_vigencia_w		date;

ie_classificacao_w		varchar2(01);
ie_autor_particular_w	varchar2(1)	:= 'N';
cd_convenio_glosa_w	number(5,0);
cd_categoria_glosa_w	varchar2(10);
ie_edicao_convenio_w	varchar2(1);
tx_ajuste_tabela_serv_w	number(15,4);
nr_seq_grupo_w		number(10)	:= 0;
nr_seq_subgrupo_w	number(10)	:= 0;
nr_seq_forma_org_w	number(10)	:= 0;
nr_seq_cbhpm_edicao_w	number(10,0);
ie_vinculo_medico_w	number(2);
vl_filme_neg_w		number(15,2);
ie_complexidade_sus_w	varchar2(2);
nr_seq_grupo_lab_w	number(10,0);
nr_seq_classif_w	number(10,0);
nr_seq_area_int_w	number(10);
nr_seq_espec_int_w	number(10);
nr_seq_grupo_int_w	number(10);
tx_regra_w		number(15,4);
ie_tx_edicao_amb_regra_w varchar2(1);	
qt_regra_edicao_conv_w	number(10,0);
ie_order_estrangeiro_w	number(2,0); 
nr_seq_estrutura_w	regra_ajuste_proc.nr_seq_estrutura%type;

cursor c01 is
select 	nvl(ie_preco_informado,'N'),
	nvl(ie_glosa,'L'),
	cd_procedimento_esp,
	decode(nvl(IE_CONSISTE_EDICAO_PRIOR,'N'),'S', decode(obter_se_proced_edicao(cd_procedimento_p, ie_origem_proced_w, cd_edicao_amb, ie_prior_edicao_ajuste), 'S', cd_edicao_amb, cd_edicao_amb_w), nvl(cd_edicao_amb,cd_edicao_amb_w)),
	nr_seq_regra_preco,
	nvl(tx_ajuste, tx_ajuste_geral_w),
	nvl(vl_proc_ajustado,0),
	vl_medico,
	vl_custo_operacional,
	qt_filme,
	nr_auxiliares,
	qt_porte_anestesico,
	nr_sequencia,
	tx_ajuste_custo_oper,
	tx_ajuste_medico,
	nvl(tx_ajuste_partic, tx_ajuste_medico),
	tx_ajuste_filme,
	decode(ie_glosa,'P',pr_glosa,'D',pr_glosa, '1', pr_glosa,0),
	decode(ie_glosa,'R',vl_glosa,'4',vl_glosa, 0),
	cd_motivo_exc_conta,
	ie_autor_particular,
	nvl(cd_convenio_glosa,0),
	cd_categoria_glosa,
	vl_filme,
	tx_ajuste,
	nvl(ie_tx_edicao_amb_regra,'N'),	
	decode(nvl(ie_estrangeiro,'N'),'R',4,'E',3,'A',2,'N',1) ie_order_estrangeiro
from 	regra_ajuste_proc
where	cd_estabelecimento				= cd_estabelecimento_p
and	cd_convenio				= cd_convenio_p
and	((cd_categoria is null) or (cd_categoria = cd_categoria_p))
and	((cd_procedimento is null) or (cd_procedimento = cd_procedimento_p))
and	((ie_origem_proced is null) or (ie_origem_proced = ie_origem_proced_w))
and	((cd_grupo_proc is null) or (cd_grupo_proc = cd_grupo_w))
and	((cd_especialidade is null) or (cd_especialidade = cd_especialidade_w))
and	((cd_area_procedimento is null) or (cd_area_procedimento = cd_area_w))
and	((cd_tipo_acomodacao is null) or (cd_tipo_acomodacao = cd_tipo_acomodacao_p))
and	((ie_tipo_atendimento is null) or (ie_tipo_atendimento = ie_tipo_atendimento_p))
and	((cd_setor_atendimento is null) or (cd_setor_atendimento = cd_setor_atendimento_p))
and	((cd_setor_atend_prescr is null) or (cd_setor_atend_prescr = cd_setor_atend_prescr_p))
and	ie_situacao         					= 'A'
and	((ie_credenciado = 'N') or (ie_credenciado_w = 'S'))
and 	dt_vigencia_p between ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_inicio_vigencia) and
				ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(nvl(dt_final_vigencia,dt_vigencia_w))
and	qt_idade_w between nvl(qt_idade_min, qt_idade_w) and nvl(qt_idade_max, qt_idade_w)
and	((nr_seq_proc_interno is null) or (nr_seq_proc_interno = nr_seq_proc_interno_p))
and	((nr_seq_exame is null) or (nr_seq_exame = nr_seq_exame_lab_p))
and	((cd_plano is null) or (cd_plano = nvl(cd_plano_p, 0)))
and	((nr_seq_estrutura is null) or (nr_seq_estrutura = nr_seq_estrutura_w))
and	((ie_clinica is null) or (ie_clinica = ie_clinica_p))
and	((cd_empresa_ref is null) or (cd_empresa_ref = cd_empresa_ref_p))
and	((ie_sexo is null) or (ie_sexo = ie_sexo_p))
and	((cd_medico is null) or (cd_medico = cd_medico_p))
and	((ie_prioridade_ajuste_proc_w = 'N') or ((ie_prior_edicao_ajuste  = 'N') or (nvl(cd_edicao_amb,cd_edicao_amb_w)  = cd_edicao_amb_w)))
--and 	((cd_edicao_amb is null) or (obter_se_proc_edicao2(cd_procedimento_p, ie_origem_proced_p, cd_edicao_amb) = 'S'))
and	((ie_atend_retorno_p is null) or (nvl(ie_atend_retorno,'A') = 'A') or (ie_atend_retorno = ie_atend_retorno_p))
and	qt_dias_internacao_w between nvl(qt_dias_inter_inicio, qt_dias_internacao_w) and nvl(qt_dias_inter_final, qt_dias_internacao_w)
--and	((ie_video_p is null) or (ie_utiliza_video is null) or (nvl(ie_utiliza_video, 'N')	= ie_video_p))
and	((nvl(ie_utiliza_video,'N') = 'N') or ((ie_utiliza_video = 'S')	and (nvl(ie_video_p,'N') = 'S')))
and 	((cd_edicao_amb is null) or (obter_se_edicao_ativa(cd_edicao_amb) = 'A'))
and 	((nvl(ie_edicao_convenio,'N') = 'N') or ((ie_edicao_convenio = 'S') and (ie_edicao_convenio_w = 'N')))
and	nvl(nr_seq_grupo,nr_seq_grupo_w)		= nr_seq_grupo_w
and	nvl(nr_seq_subgrupo,nr_seq_subgrupo_w)		= nr_seq_subgrupo_w
and	nvl(nr_seq_forma_org,nr_seq_forma_org_w)	= nr_seq_forma_org_w
and	((nvl(ie_beira_leito,'A') = 'A') or (nvl(ie_beira_leito,'A') = nvl(ie_beira_leito_p,'A')))
and	((ie_tipo_atend_bpa is null) or (ie_tipo_atend_bpa = nvl(ie_tipo_atend_bpa_p,0))) --Foi alterado NVL para zero pois estava encontrando a regra para atendimentos sem tipo BPA informado OS 898512 - Geliard e Heckmann
and	((cd_procedencia is null) or (cd_procedencia = nvl(cd_procedencia_p,cd_procedencia)))
and	((nvl(ie_spect,'N') = 'N') or ((ie_spect = 'S')	and (nvl(ie_spect_p,'N') = 'S')))
and	((cd_cgc_prestador is null) or (cd_cgc_prestador = nvl(cd_cgc_prestador_p, cd_cgc_prestador)))
and	((ie_vinculo_medico is null) or (ie_vinculo_medico = nvl(ie_vinculo_medico_w, ie_vinculo_medico)))
and	((cd_equipamento is null) or (cd_equipamento = nvl(cd_equipamento_p, 0)))
and	((nr_seq_tipo_acidente is null) or (nr_seq_tipo_acidente = nvl(nr_seq_tipo_acidente_p, 0)))
and	((cd_especialidade_medica is null) or (cd_especialidade_medica = nvl(cd_especialidade_medica_p, 0)))
and 	((nr_seq_equipe is null) or (obter_se_medico_equipe(nr_seq_equipe, cd_medico_p) = 'S'))
and	((nvl(ie_complexidade_sus,nvl(ie_complexidade_sus_w,'X')) = nvl(ie_complexidade_sus_w,'X')))
and	((nr_seq_grupo_lab is null) or (nr_seq_grupo_lab = nvl(nr_seq_grupo_lab_w,0)))
and	((nr_seq_cobertura is null) or (nr_seq_cobertura = nvl(nr_seq_cobertura_p,0)))
and	((nr_seq_classif is null) or (nr_seq_classif = nvl(nr_seq_classif_w,0)))
and	((nr_seq_classificacao is null) or (nr_seq_classificacao = nvl(nr_seq_classif_atend_p,0)))
and	((cd_usuario_convenio is null) or (cd_usuario_convenio = substr(cd_usuario_convenio_p, qt_pos_inicial, qt_pos_final)))
and	((nr_seq_area_int is null) or (nr_seq_area_int = nr_seq_area_int_w))
and	((nr_seq_espec_int is null) or (nr_seq_espec_int = nr_seq_espec_int_w))
and	((nr_seq_grupo_int is null) or (nr_seq_grupo_int = nr_seq_grupo_int_w))
and	(nvl(cd_medico_resp, nvl(cd_medico_resp_p,'0')) = nvl(cd_medico_resp_p,'0'))
and	nvl(cd_dependente, nvl(cd_dependente_p, 0)) = nvl(cd_dependente_p, 0)
and	nvl(nr_seq_grupo_rec, nvl(nr_seq_grupo_rec_p,0)) = nvl(nr_seq_grupo_rec_p,0)
and	nvl(nr_seq_origem, nvl(nr_seq_origem_p,0)) = nvl(nr_seq_origem_p,0)
and	((nvl(ie_paciente_deficiente,'S') = 'S') or ((nvl(ie_paciente_deficiente,'S') = 'N') and (nvl(ie_paciente_deficiente_p,'N') = 'N')))
and ((nvl(ie_estrangeiro,'N') = 'N')
	or ((nvl(ie_estrangeiro,'N') = 'A') and (nvl(ie_estrangeiro_p,'N') in ('R','E')))
	or (nvl(ie_estrangeiro,'N') = nvl(ie_estrangeiro_p,'N') ))
and	nvl(nr_seq_classif_medico, nvl(nr_seq_classif_medico_p,0)) = nvl(nr_seq_classif_medico_p,0)
and	nvl(ie_carater_inter_sus,nvl(ie_carater_inter_sus_p,0)) = nvl(ie_carater_inter_sus_p,0)
order by
	nvl(cd_medico,'0'),
	nvl(nr_seq_proc_interno,0),
	nvl(nr_seq_exame,0),
	nvl(cd_procedimento, 0),
	nvl(nr_seq_estrutura, 0),	
	nvl(cd_grupo_proc, 0),
	nvl(cd_especialidade, 0),
	nvl(cd_area_procedimento, 0),
	nvl(nr_seq_grupo_lab,0),
	nvl(nr_seq_classif,0),
	nvl(cd_tipo_acomodacao, 0),
	nvl(ie_tipo_atendimento, 0),
	nvl(cd_setor_atendimento, 0),
	nvl(cd_empresa_ref,0),
	nvl(ie_clinica,'0'),
	nvl(cd_plano,' '),
	nvl(cd_usuario_convenio, 0),
	ie_credenciado,
	dt_inicio_vigencia,
	nvl(cd_proc_referencia,0),
	nvl(qt_idade_min,0),
	nvl(nr_seq_proc_interno,0),
	nvl(cd_categoria,'0'),
	nvl(cd_procedencia,0),
	nvl(cd_cgc_prestador,'0'),
	nvl(ie_vinculo_medico, 0),
	nvl(ie_utiliza_video,'N'),
	nvl(ie_spect,'N'),
	nvl(cd_equipamento,0),
	nvl(nr_seq_equipe,0),
	nvl(nr_seq_cobertura,0),
	nvl(cd_especialidade_medica,0),
	nvl(nr_seq_classificacao,0),
	nvl(nr_seq_grupo_int, 0),
	nvl(nr_seq_espec_int, 0),
	nvl(nr_seq_area_int, 0),
	nvl(cd_medico_resp, '0'),
	nvl(cd_dependente, 0),
	nvl(nr_seq_grupo_rec,0),
	nvl(nr_seq_origem,0),
	nvl(nr_seq_classif_medico,0),
	ie_order_estrangeiro,
	nvl(ie_carater_inter_sus,0);

cursor c02 is
	select	nvl(tx_ajuste_geral,1)
	from	preco_servico a,
		convenio_servico b
	where	a.cd_tabela_servico	= b.cd_tabela_servico
	and	b.cd_convenio		= cd_convenio_p
	and	b.cd_estabelecimento = cd_estabelecimento_p
	and	b.cd_categoria		= cd_categoria_p
	--and	b.dt_liberacao_tabela	<= dt_vigencia_p
	and	PKG_DATE_UTILS.start_of(dt_vigencia_p,'dd',0)	between b.dt_liberacao_tabela and nvl(b.dt_termino, dt_vigencia_p)
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.cd_procedimento		= cd_procedimento_p
	and	dt_vigencia_p between a.dt_inicio_vigencia and nvl(a.dt_vigencia_final, dt_vigencia_p)
	and 	nvl(b.ie_situacao,'A')	= 'A'
	and 	(a.dt_inativacao is null or a.dt_inativacao > dt_vigencia_p)
	order by nvl(b.nr_prioridade,1),
			a.dt_inicio_vigencia desc,
			a.vl_servico desc;
begin

dt_vigencia_w	:= sysdate;

if	(dt_vigencia_p > sysdate) then
	dt_vigencia_w	:= dt_vigencia_p;
end if;

select	nvl(max(ie_prioridade_ajuste_proc), 'N')
into	ie_prioridade_ajuste_proc_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

ie_origem_proced_w	:= ie_origem_proced_p;
qt_idade_w		:= nvl(qt_idade_p,0);
qt_dias_internacao_w	:= nvl(qt_dias_internacao_p, 0);

/* Trocado pela function abaixo. OS 334552 - aaheckler.
select	max(ie_vinculo_medico)
into	ie_vinculo_medico_w
from	medico
where	cd_pessoa_fisica = cd_medico_p;*/

ie_vinculo_medico_w	:= nvl(obter_vinculo_medico(cd_medico_p, cd_estabelecimento_p),0);

/* obter estrutura do procedimento */
select 		nvl(max(cd_grupo_proc),0),
		nvl(max(cd_especialidade),0),
		nvl(max(cd_area_procedimento),0),
		max(ie_classificacao)
into		cd_grupo_w,
		cd_especialidade_w,
		cd_area_w,
		ie_classificacao_w
from		estrutura_procedimento_v
where		cd_procedimento 	= cd_procedimento_p
and		ie_origem_proced	= ie_origem_proced_w;

nr_seq_area_int_w	:= 0;
nr_seq_espec_int_w	:= 0;
nr_seq_grupo_int_w	:= 0;

nr_seq_grupo_lab_w 	:= 0;
if	(nr_seq_exame_lab_p is not null) then
	begin
	select	nr_seq_grupo,
		nr_seq_grupo_int
	into	nr_seq_grupo_lab_w,
		nr_seq_grupo_int_w
	from	exame_laboratorio
	where	nr_seq_exame = nr_seq_exame_lab_p;
	exception
		when others then
			nr_seq_grupo_lab_w := 0;
			nr_seq_grupo_int_w := 0;
	end;
	
	--Estrutura Interna (Exame Lab)
	if	(nvl(nr_seq_grupo_int_w,0) > 0) then
		begin
		select 	nr_seq_especialidade,
			nr_seq_area
		into	nr_seq_espec_int_w,
			nr_seq_area_int_w
		from 	estrutura_interna_v
		where	nr_seq_grupo = nr_seq_grupo_int_w;
		exception
		when others then
			nr_seq_area_int_w	:= 0;
			nr_seq_espec_int_w	:= 0;
			nr_seq_grupo_int_w	:= 0;
		end;
	end if;
	
end if;	

nr_seq_classif_w := 0;
if	(nvl(nr_seq_proc_interno_p,0) > 0) then
	select	nvl(max(nr_seq_classif),0),
		nvl(max(nr_seq_grupo_int),0)
	into	nr_seq_classif_w,
		nr_seq_grupo_int_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_p;
	
	--Estrutura Interna (Proc Interno)
	if	(nvl(nr_seq_grupo_int_w,0) > 0) then
		begin
		select 	nr_seq_especialidade,
			nr_seq_area
		into	nr_seq_espec_int_w,
			nr_seq_area_int_w
		from 	estrutura_interna_v
		where	nr_seq_grupo = nr_seq_grupo_int_w;
		exception
		when others then
			nr_seq_area_int_w	:= 0;
			nr_seq_espec_int_w	:= 0;
			nr_seq_grupo_int_w	:= 0;
		end;
	end if;
end if;


if	(ie_origem_proced_p	= 7) then

	begin
	select	nr_seq_grupo,
		nr_seq_subgrupo,
		nr_seq_forma_org
	into	nr_seq_grupo_w,
		nr_seq_subgrupo_w,
		nr_seq_forma_org_w
	from	sus_estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;
	exception
		when others then
		nr_seq_grupo_w		:= 0;		/* felipe martini e almir os135256 */
		nr_seq_subgrupo_w	:= 0;
		nr_seq_forma_org_w	:= 0;
	end;

end if;
/* trocar origem proced qdo cbhpm com ajuste amb ou invertido */
/* precisa incluir origem na chamada da define preco procedimento para eliminar */
if	(cd_grupo_w		= 0)	and
	(cd_especialidade_w	= 0)	and
	(cd_area_w		= 0)	then
	begin
	select count(*)
	into	 qt_reg_w
	from	 procedimento
	where	rownum = 1
	and		cd_procedimento	= cd_procedimento_p
	and		ie_origem_proced	= ie_origem_proced_w;
	
	if	 (qt_reg_w	= 0) then
		 begin
		 if	(ie_origem_proced_w = 1) then
			ie_origem_proced_w := 5;
		 elsif (ie_origem_proced_w = 5) then
			ie_origem_proced_w := 1;
		 end if;
		 /* obter estrutura do procedimento */
		 select nvl(max(cd_grupo_proc),0),
			  nvl(max(cd_especialidade),0),
			  nvl(max(cd_area_procedimento),0)
		 into	  cd_grupo_w,
			  cd_especialidade_w,
			  cd_area_w
		 from	  estrutura_procedimento_v
		 where    cd_procedimento 	= cd_procedimento_p
		 and	  ie_origem_proced	= ie_origem_proced_w;
		 end;
	end if;
	end;
end if;

/*      	obter edicao da amb  */
if	(ie_prioridade_ajuste_proc_w		= 'N') then
	select	nvl(max(cd_edicao_amb),0),
		nvl(max(tx_ajuste_geral),1)
	into	cd_edicao_amb_w,
		tx_ajuste_geral_w
	from	convenio_amb
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio		= cd_convenio_p
	and	nvl(ie_situacao,'A')	= 'A'
	and	cd_categoria		= cd_categoria_p
	and	dt_inicio_vigencia	=
		(select max(dt_inicio_vigencia)
		from		convenio_amb a
		where		a.cd_estabelecimento	= cd_estabelecimento_p
		and		nvl(a.ie_situacao,'A')	= 'A'
		and		a.cd_convenio		= cd_convenio_p
		and		a.cd_categoria		= cd_categoria_p
		and		a.dt_inicio_vigencia 	<= dt_vigencia_p);
else
	obter_edicao_proc_conv
		(cd_estabelecimento_p,
		cd_convenio_p,
		cd_categoria_p,
		dt_vigencia_p,
		cd_procedimento_p,
		cd_edicao_amb_w,
		vl_ch_honorarios_w,
		vl_ch_custo_oper_w,
		vl_m2_filme_w,
		dt_inicio_vigencia_w,
		tx_ajuste_geral_w,
		nr_seq_cbhpm_edicao_w,
		ie_origem_proced_p);
end if;


if	(ie_classificacao_w <> '1') then

	/*felipe martini os109691 em 23/09/2008  inicio*/
	open c02;
	loop
	fetch c02 into
		tx_ajuste_tabela_serv_w;
	exit when c02%notfound;
		begin
		exit;
		end;
	end loop;
	close c02;
	if	(tx_ajuste_tabela_serv_w is null) then
		tx_ajuste_tabela_serv_w	:= 1;
	end if;

	tx_ajuste_w			:= tx_ajuste_tabela_serv_w;
	tx_ajuste_custo_oper_w		:= tx_ajuste_tabela_serv_w;
	tx_ajuste_medico_w		:= tx_ajuste_tabela_serv_w;
	tx_ajuste_partic_w		:= tx_ajuste_tabela_serv_w;
	tx_ajuste_filme_w		:= tx_ajuste_tabela_serv_w;
	tx_ajuste_geral_w		:= tx_ajuste_tabela_serv_w;
	/*felipe martini os109691 em 23/09/2008  fim*/
else
	tx_ajuste_w			:= tx_ajuste_geral_w;
	tx_ajuste_custo_oper_w		:= tx_ajuste_geral_w;
	tx_ajuste_medico_w		:= tx_ajuste_geral_w;
	tx_ajuste_partic_w		:= tx_ajuste_medico_w;
	tx_ajuste_filme_w		:= tx_ajuste_geral_w;
end if;

ie_credenciado_w	:= obter_se_medico_credenciado(cd_estabelecimento_p, cd_medico_p, cd_convenio_p, null, null, cd_categoria_p,cd_setor_atendimento_p, cd_plano_p, dt_vigencia_p, null, to_char(cd_funcao_medico_p), null);
ie_complexidade_sus_w	:= sus_obter_complexidade_proced(cd_procedimento_p,ie_origem_proced_w, 'C');

ie_edicao_convenio_w:= 'N';

select 	count(1)
into	qt_regra_edicao_conv_w
from 	regra_ajuste_proc
where 	rownum = 1
and	ie_situacao = 'A'
and 	cd_convenio = cd_convenio_p
and 	cd_estabelecimento = cd_estabelecimento_p
and 	ie_edicao_convenio = 'S';

if	(qt_regra_edicao_conv_w > 0) then
	ie_edicao_convenio_w	:= nvl(verifica_se_proc_conv(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, dt_vigencia_p, cd_procedimento_p, ie_origem_proced_p, nr_seq_proc_interno_p, ie_tipo_atendimento_p),'N');	
end if;

if	(cd_edicao_amb_w is null) then
	cd_edicao_amb_w	:= 0;
end if;

select	nvl(max(nr_seq_estrutura),0)
into	nr_seq_estrutura_w
from	pi_estrutura_cad
where	nr_seq_proc_int = nr_seq_proc_interno_p;

open c01;
loop
fetch c01 into
		ie_preco_informado_w,
		ie_glosa_w,
		cd_procedimento_esp_w,
		cd_edicao_ajuste_w,
		nr_seq_regra_preco_w,
		tx_ajuste_w,
		vl_negociado_w,
		vl_medico_w,
		vl_custo_operacional_w,
		qt_filme_w,
		nr_auxiliares_w,
		qt_porte_anestesico_w,
		nr_sequencia_w,
		tx_ajuste_custo_oper_w,
		tx_ajuste_medico_w,
		tx_ajuste_partic_w,
		tx_ajuste_filme_w,
		pr_glosa_w,
		vl_glosa_w,
		cd_motivo_exc_conta_w,
		ie_autor_particular_w,
		cd_convenio_glosa_w,
		cd_categoria_glosa_w,
		vl_filme_neg_w,
		tx_regra_w,
		ie_tx_edicao_amb_regra_w,
		ie_order_estrangeiro_w;
	exit when c01%notfound;
		begin
		tx_ajuste_w			:= tx_ajuste_w;		
		end;
end loop;
close c01;

if	(nvl(ie_tx_edicao_amb_regra_w,'N') = 'S') and (nvl(tx_regra_w,0) <> 0) then
	tx_ajuste_w:= tx_ajuste_w * tx_ajuste_geral_w;
end if;

tx_ajuste_p		:= tx_ajuste_w;
tx_ajuste_custo_oper_p	:= nvl(tx_ajuste_custo_oper_w, tx_ajuste_w);
tx_ajuste_medico_p	:= nvl(tx_ajuste_medico_w, tx_ajuste_w);
tx_ajuste_partic_p	:= nvl(tx_ajuste_partic_w, tx_ajuste_w);
tx_ajuste_filme_p	:= nvl(tx_ajuste_filme_w, tx_ajuste_w);
ie_preco_informado_p	:= ie_preco_informado_w;
ie_glosa_p		:= ie_glosa_w;
cd_procedimento_esp_p	:= cd_procedimento_esp_w;
nr_seq_regra_preco_p	:= nr_seq_regra_preco_w;
cd_edicao_ajuste_p	:= cd_edicao_ajuste_w;
vl_negociado_p		:= vl_negociado_w;
vl_custo_operacional_p	:= vl_custo_operacional_w;
vl_medico_p		:= vl_medico_w;
qt_filme_p		:= qt_filme_w;
nr_auxiliares_p		:= nr_auxiliares_w;
qt_porte_anestesico_p	:= qt_porte_anestesico_w;
pr_glosa_p		:= pr_glosa_w;
vl_glosa_p		:= vl_glosa_w;
ie_autor_particular_p	:= ie_autor_particular_w;
cd_convenio_glosa_p	:= cd_convenio_glosa_w;
cd_categoria_glosa_p	:= cd_categoria_glosa_w;
nr_sequencia_p		:= nr_sequencia_w;
vl_filme_p   		:= vl_filme_neg_w;

cd_motivo_exc_conta_p	:= cd_motivo_exc_conta_w;

end obter_regra_ajuste_proc;
/
