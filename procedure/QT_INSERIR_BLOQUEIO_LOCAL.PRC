CREATE OR REPLACE PROCEDURE qt_inserir_bloqueio_local (
    nr_seq_local_p  NUMBER,
    dt_agenda_p     DATE,
    hr_inicio_p     DATE,
    hr_fim_p        DATE,
    ds_observacao_p VARCHAR2,
    nm_usuario_p    VARCHAR2
) IS 
BEGIN

IF  (nr_seq_local_p IS NOT NULL) THEN

    INSERT INTO qt_bloqueio (
        nr_sequencia,
        nr_seq_local,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        dt_inicial,
        dt_final,
        hr_inicio_bloqueio,
        hr_final_bloqueio,
        ds_observacao,
        dt_dia_semana
    ) VALUES (
        qt_bloqueio_seq.nextval,
        nr_seq_local_p,
        SYSDATE,
        nm_usuario_p,
        SYSDATE,
        nm_usuario_p,
        TRUNC(dt_agenda_p),
        fim_dia(dt_agenda_p),
        hr_inicio_p,
        hr_fim_p,
        ds_observacao_p,
        NULL
    );
END IF;

COMMIT;

END qt_inserir_bloqueio_local;
/