create or replace PROCEDURE Generate_hpc_snap_content(nr_seq_dataset_p  NUMBER, 
                                                      nm_usuario_p      VARCHAR2 
, 
cd_convenio_p     VARCHAR2, 
returned_value_p  OUT nocopy NUMBER, 
other_exception_p OUT nocopy VARCHAR2) 
IS 
  ds_segment_w                 VARCHAR2(32767); 
  ds_segment_clob_w            CLOB := empty_clob; 
  is_clob_insertion_successful NUMBER := 0; 
  excp_caught_dataset_clob_ins VARCHAR2( 500 ); 
  ds_insurer_iden_w            VARCHAR2(15); 
BEGIN 
    BEGIN 
        ds_insurer_iden_w := 
        generate_data_hcp_pkg.Get_insurer_identifier(cd_convenio_p); 

        SELECT '    IMI' -- insurer membership identifier 
               ||Decode(cd_membership_id, NULL, '               ', 
                                          Rpad(cd_membership_id, 15, ' ')) 
               ||Decode(cd_insurer_id, NULL, '   ', 
                                       Rpad(cd_insurer_id, 3, ' ')) 
               ||Decode(cd_snap, NULL, '               ', 
                                 Rpad(cd_snap, 15, ' ')) 
               ||Decode(nm_last_name, NULL, '                            ', 
                                      Rpad(nm_last_name, 28, ' ')) 
               ||Decode(nm_first_name, NULL, '                    ', 
                                       Rpad(nm_first_name, 20, ' ')) 
               ||Decode(dt_dob, NULL, '        ', 
                                To_char(dt_dob, 'yyyymmdd')) 
               ||Decode(cd_zip_code, NULL, '0000', 
                                     Lpad(cd_zip_code, 4, '0')) 
               ||Decode(ie_gender, NULL, ' ', 
                                   Trim(ie_gender)) 
               ||Decode(dt_admission, NULL, '        ', 
                                      To_char(dt_admission, 'yyyymmdd')) 
               ||Decode(dt_separation, NULL, '        ', 
                                       To_char(dt_separation, 'yyyymmdd')) 
               ||Decode(ie_episode_type, NULL, ' ', 
                                         Trim(ie_episode_type)) 
               ||Decode(nr_dis_fim_score, NULL, ' ', 
                                          Trim(nr_dis_fim_score)) 
               ||Decode(cd_primary_impairment, NULL, '       ', 
                                               Rpad(cd_primary_impairment, 7, 
                                               '0')) 
               ||Decode(cd_assesmt_indic, NULL, ' ', 
                                          Trim(cd_assesmt_indic)) 
               ||Decode(cd_snap_class, NULL, '    ', 
                                       Trim(cd_snap_class)) 
               ||Decode(cd_snap_version, NULL, '  ', 
                                         Trim(cd_snap_version)) 
               ||Decode(dt_rehab_plan, NULL, '        ', 
                                       To_char(dt_rehab_plan, 'yyyymmdd')) 
               ||Decode(dt_discharge_plan, NULL, '        ', 
                                           To_char(dt_discharge_plan, 'yyyymmdd' 
                                           )) 
               ||Decode(nr_seq_dataset, NULL, '        ', 
                                        To_char(nr_seq_dataset, 5, '0'))
	       ||Decode(cd_episode_start, NULL, ' ', 
                                         Lpad(Trim(cd_episode_start),1 ,'0' ))
               ||Decode(cd_episode_end, NULL, ' ', 
                                         Lpad(Trim(cd_episode_end),1 ,'0' ))
        INTO   ds_segment_w 
        FROM   hcp_snap_seg_data 
        WHERE  nr_seq_dataset = nr_seq_dataset_p;
     EXCEPTION 	
        WHEN OTHERS THEN 	
          ds_segment_w := NULL; 
    END; 

    SELECT Concat(ds_segment_clob_w, ds_segment_w) 
    INTO   ds_segment_clob_w 
    FROM   dual; 

    Insert_hcp_dataset_content(nm_usuario_p, nr_seq_dataset_p, ds_segment_clob_w 
    , 
    is_clob_insertion_successful, excp_caught_dataset_clob_ins); 

    IF( is_clob_insertion_successful = 1 ) THEN 
      returned_value_p := 1; 

      other_exception_p := NULL; 
    ELSE 
      IF( excp_caught_dataset_clob_ins IS NOT NULL ) THEN 
        returned_value_p := 0; 

        other_exception_p := excp_caught_dataset_clob_ins; 
      END IF; 
    END IF; 
END Generate_hpc_snap_content;
/
