create or replace procedure pls_gerar_ocor_aut_medico
			(	nr_seq_ocor_filtro_p		Number,
				nr_seq_guia_p			Number,
				nr_seq_requisicao_p		Number,
				nr_seq_execucao_p		Number,
				nr_seq_exec_item_p		Number,
				nr_seq_param2_p			Number,
				nr_seq_param3_p			Number,
				nr_seq_param4_p			Number,
				nr_seq_param5_p			Number,
				nm_usuario_p			Varchar2,
				ie_gerar_ocorrencia_p	out	Varchar2,
				ie_tipo_ocorrencia_p	out	Varchar2) is


/*
IE_TIPO_OCORRENCIA_W	= C - Gera a ocorrência para o cabeçalho
			= I - Gera ocorrência para os itens
*/

ie_gerar_ocorrencia_w		Varchar2(2)	:= 'N';
ie_regra_com_itens_w		Varchar2(1);
nr_seq_requisicao_w		Number(10);
cd_medico_solicitante_w		Varchar2(10);
ie_grupo_medico_w		Varchar2(2);
cd_especialidade_w		Varchar2(10);
cd_especialidade_med_w		Varchar2(10);
nr_seq_prestador_w		Number(10);
ie_medico_vinculo_prest_w	Varchar2(1);
qt_medico_prest_w		Number(10);
cd_medico_executor_w		Varchar2(10);
ie_valida_medico_coop_w		pls_ocor_aut_filtro_medic.ie_valida_medico_coop%type;
ie_tipo_relacao_w		pls_ocor_aut_filtro_medic.ie_valida_medico_coop%type;
ie_medico_cooperado_w		Varchar2(1);
nr_seq_conselho_w		pessoa_fisica.nr_seq_conselho%type;
nr_crm_solicitante_w		medico.nr_crm%type;
nr_seq_cbo_saude_w		cbo_saude.nr_sequencia%type;

Cursor C01 is
	select	nr_seq_grupo_medico
	from	pls_ocor_aut_filtro_medic
	where	nr_seq_ocor_aut_filtro	= nr_seq_ocor_filtro_p
	and	nvl(ie_situacao,'A')	= 'A'
	and	(cd_especialidade is null or cd_especialidade = cd_especialidade_med_w)
	and	((nvl(ie_valida_medico_coop, 'A') = 'A')
	or 	((ie_medico_cooperado_w is not null) and (ie_valida_medico_coop = ie_medico_cooperado_w)))
	and 	((nvl(ie_medico_vinculo_prest, 'N') = 'N') or (ie_medico_vinculo_prest = 'S' and qt_medico_prest_w > 0))
	and	((nr_seq_conselho_solic is null) 	 or (nr_seq_conselho_solic = nr_seq_conselho_w))
	and	((cd_medico_executor_solic is null) 	 or (cd_medico_executor_solic = nvl(cd_medico_solicitante_w,cd_medico_executor_w)))
	and	((nr_crm_solicitante is null) 		 or (nr_crm_solicitante = nr_crm_solicitante_w))
	and	((nr_seq_cbo_saude is null)		 or (nr_seq_cbo_saude = nr_seq_cbo_saude_w));

begin



if	(nr_seq_guia_p	is not null) then
	begin
		select	nr_seq_prestador,
			cd_medico_solicitante,
			cd_especialidade,
			nr_seq_cbo_saude
		into	nr_seq_prestador_w,
			cd_medico_solicitante_w,
			cd_especialidade_med_w,
			nr_seq_cbo_saude_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;
	exception
	when others then
		cd_medico_solicitante_w		:= null;
		nr_seq_prestador_w		:= null;
		cd_especialidade_med_w		:= null;
	end;

	if (cd_medico_solicitante_w is not null) then
		select	decode(pls_obter_se_cooperado_ativo(cd_medico_solicitante_w, sysdate, null), 'S', 'C', 'N')
		into	ie_medico_cooperado_w
		from 	dual;

		if (nr_seq_prestador_w is not null) then
			select	count(1)
			into	qt_medico_prest_w
			from	pls_prestador_medico
			where	nr_seq_prestador	= nr_seq_prestador_w
			and	cd_medico		= cd_medico_solicitante_w;
		end if;
	end if;
elsif	(nr_seq_requisicao_p	is not null) then
	begin
		select	nr_seq_prestador,
			cd_medico_solicitante,
			cd_especialidade,
			nr_seq_cbo_saude
		into	nr_seq_prestador_w,
			cd_medico_solicitante_w,
			cd_especialidade_med_w,
			nr_seq_cbo_saude_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;
	exception
	when others then
		cd_medico_solicitante_w		:= null;
		nr_seq_prestador_w		:= null;
		cd_especialidade_med_w		:= null;
	end;

	if (cd_medico_solicitante_w is not null) then
		select 	decode(pls_obter_se_cooperado_ativo(cd_medico_solicitante_w, sysdate, null), 'S', 'C', 'N')
		into	ie_medico_cooperado_w
		from 	dual;

		if (nr_seq_prestador_w is not null) then
			select	count(1)
			into	qt_medico_prest_w
			from	pls_prestador_medico
			where	nr_seq_prestador	= nr_seq_prestador_w
			and	cd_medico		= cd_medico_solicitante_w;
		end if;
	end if;
elsif	(nr_seq_execucao_p	is not null) then
	begin
		select	nr_seq_requisicao,
			nr_seq_prestador
		into	nr_seq_requisicao_w,
			nr_seq_prestador_w
		from	pls_execucao_requisicao
		where	nr_sequencia	= nr_seq_execucao_p;
	exception
	when others then
		nr_seq_requisicao_w	:= null;
	end;

	if	(nr_seq_requisicao_w	is not null) then
		begin
			select	cd_especialidade,
				nr_seq_cbo_saude
			into	cd_especialidade_med_w,
				nr_seq_cbo_saude_w
			from	pls_requisicao
			where	nr_sequencia	= nr_seq_requisicao_w;
		exception
		when others then
			cd_especialidade_med_w	:= null;
		end;
	end if;

	if (nr_seq_exec_item_p is not null) then
		begin
			select	cd_medico_executor
			into	cd_medico_executor_w
			from	pls_execucao_req_item
			where	nr_sequencia	= nr_seq_exec_item_p
			and	nr_seq_execucao	= nr_seq_execucao_p;
		exception
		when others then
			cd_medico_executor_w	:= null;
		end;
	end if;
	
	if (cd_medico_executor_w is not null) then
		select	decode(pls_obter_se_cooperado_ativo(cd_medico_executor_w, sysdate, null), 'S', 'C', 'N')
		into	ie_medico_cooperado_w
		from	dual;

		if (nr_seq_prestador_w is not null) then
			select	count(1)
			into	qt_medico_prest_w
			from	pls_prestador_medico
			where	nr_seq_prestador	= nr_seq_prestador_w
			and	cd_medico		= cd_medico_executor_w;
		end if;
	end if;
end if;

begin
	select	nr_seq_conselho
	into	nr_seq_conselho_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = nvl(cd_medico_solicitante_w,cd_medico_executor_w);
exception
when others then
	nr_seq_conselho_w := null;
end;

begin
	select	nr_crm
	into	nr_crm_solicitante_w
	from	medico
	where	cd_pessoa_fisica	= nvl(cd_medico_solicitante_w,cd_medico_executor_w);
exception
when others then
	nr_crm_solicitante_w := null;
end;

for	r_C01_w in C01 loop
	ie_gerar_ocorrencia_w 	:= 'S';
		
	if (r_C01_w.nr_seq_grupo_medico > 0) and (nvl(cd_medico_executor_w, cd_medico_solicitante_w) is not null) then
		ie_grupo_medico_w 	:= pls_obter_se_grupo_medico(r_C01_w.nr_seq_grupo_medico, nvl(cd_medico_executor_w, cd_medico_solicitante_w));
		
		if 	(ie_grupo_medico_w = 'N') then
			ie_gerar_ocorrencia_w 	:= 'N';
		end if;
	end if;
	
	if	(ie_gerar_ocorrencia_w 	= 'S') then
		exit;
	end if;
end loop;

if	(ie_gerar_ocorrencia_w = 'S') then
	ie_regra_com_itens_w  :=  pls_obter_se_oco_aut_fil_itens(nr_seq_ocor_filtro_p);

	if	(ie_regra_com_itens_w = 'S') then
		ie_tipo_ocorrencia_p := 'I';
	else
		ie_tipo_ocorrencia_p := 'C';
	end if;
end if;

ie_gerar_ocorrencia_p	:= ie_gerar_ocorrencia_w;

end pls_gerar_ocor_aut_medico;
/