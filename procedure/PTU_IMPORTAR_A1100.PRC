create or replace
procedure ptu_importar_a1100
			(	nm_usuario_p		Varchar2) is 

ds_conteudo_w			Varchar2(4000);
nr_seq_arquivo_w		Number(8);
ie_tipo_registro_w		Varchar2(3);
cd_unimed_w			Varchar2(4);
dt_mov_inicial_ww		Varchar2(8);
dt_mov_final_ww			Varchar2(8);
dt_mov_inicial_w		Date;
dt_mov_final_w			Date;
nr_versao_trans_w		Varchar2(2);
qt_total_r102_w			Number(5);
qt_total_r103_w			Number(5);
cd_unimed_executora_w		Varchar2(4);
cd_unimed_origem_w		Varchar2(4);
nr_seq_execucao_w		Number(10);
nr_seq_origem_w			Number(10);
nr_seq_trans_refe_w		Number(10);
nr_seq_cabecalho_w		Number(10);
nr_seq_dados_pedido_w		Number(10);
cd_usuario_plano_w		Varchar2(13);
dt_solicitacao_ww		Varchar2(21);
dt_resposta_wsd_ww		Varchar2(21);
dt_solicitacao_w		Date;
dt_resposta_wsd_w		Date;
ie_tipo_tabela_w		Number(1);
cd_servico_w			Number(8);
ds_servico_w			Varchar2(80);
qt_autorizada_w			Number(8);
ie_resposta_wsd_w		Number(1);
ie_situacao_opera_w		ptu_dados_pedido_a1100.ie_situacao_operadora%type;
cd_mensagem_erro_w		ptu_itens_pedido_a1100.cd_mensagem_erro%type;

Cursor C01 is
	select	ds_valores
	from	w_scs_importacao
	where	nm_usuario	= nm_usuario_p
	order by nr_sequencia;
begin

open C01;
loop
fetch C01 into	
	ds_conteudo_w;
exit when C01%notfound;
	begin
	--Header
	if	(length(ds_conteudo_w)	= 33) then
		nr_seq_arquivo_w	:= to_number(substr(ds_conteudo_w,1,8));
		ie_tipo_registro_w	:= trim(substr(ds_conteudo_w,9,3));
		cd_unimed_w		:= trim(substr(ds_conteudo_w,12,4));
		dt_mov_inicial_ww	:= trim(substr(ds_conteudo_w,22,2))||trim(substr(ds_conteudo_w,20,2))||trim(substr(ds_conteudo_w,16,4));
		dt_mov_final_ww		:= trim(substr(ds_conteudo_w,30,2))||trim(substr(ds_conteudo_w,28,2))||trim(substr(ds_conteudo_w,24,4));
		nr_versao_trans_w	:= trim(substr(ds_conteudo_w,32,2));

		if	(nvl(dt_mov_inicial_ww,'X')	<> 'X') then
			dt_mov_inicial_w	:= to_date(dt_mov_inicial_ww,'dd/mm/yyyy');
		end if;

		if	(nvl(dt_mov_final_ww,'X')	<> 'X') then
			dt_mov_final_w	:= to_date(dt_mov_final_ww,'dd/mm/yyyy');
		end if;

		select	ptu_cabecalho_a1100_seq.NextVal
		into	nr_seq_cabecalho_w
		from	dual;

		insert	into ptu_cabecalho_a1100
			(nr_sequencia, nm_usuario, nm_usuario_nrec,
			 dt_atualizacao, dt_atualizacao_nrec, nr_seq_arquivo,
			 ie_tipo_registro, cd_unimed, dt_movimentacao_inicial,
			 dt_movimentacao_final, nr_versao_transacao)
		values	(nr_seq_cabecalho_w, nm_usuario_p, nm_usuario_p,
			 sysdate, sysdate, nr_seq_arquivo_w,
			 ie_tipo_registro_w, cd_unimed_w, dt_mov_inicial_w,
			 dt_mov_final_w, nr_versao_trans_w);
	--Trailer
	elsif	(length(ds_conteudo_w)	= 21) then
		nr_seq_arquivo_w	:= to_number(substr(ds_conteudo_w,1,8));
		ie_tipo_registro_w	:= trim(substr(ds_conteudo_w,9,3));
		qt_total_r102_w		:= to_number(substr(ds_conteudo_w,12,5));
		qt_total_r103_w		:= to_number(substr(ds_conteudo_w,17,5));
		
		insert	into ptu_trailer_a1100
			(nr_sequencia, nr_seq_cabecalho, nm_usuario,
			 nm_usuario_nrec, dt_atualizacao, dt_atualizacao_nrec,
			 nr_seq_arquivo, ie_tipo_registro, qt_total_r102,
			 qt_total_r103)
		values	(ptu_trailer_a1100_seq.NextVal, nr_seq_cabecalho_w, nm_usuario_p,
			 nm_usuario_p, sysdate, sysdate,
			 nr_seq_arquivo_w, ie_tipo_registro_w, qt_total_r102_w,
			 qt_total_r103_w);
	--Dados do pedido
	elsif	(length(ds_conteudo_w)	= 109) then
		nr_seq_arquivo_w	:= to_number(substr(ds_conteudo_w,1,8));
		ie_tipo_registro_w	:= trim(substr(ds_conteudo_w,9,3));
		cd_unimed_executora_w	:= trim(substr(ds_conteudo_w,12,4));
		cd_unimed_origem_w	:= trim(substr(ds_conteudo_w,16,4));
		nr_seq_execucao_w	:= to_number(substr(ds_conteudo_w,20,10));
		nr_seq_origem_w		:= to_number(substr(ds_conteudo_w,30,10));
		nr_seq_trans_refe_w	:= to_number(substr(ds_conteudo_w,40,10));
		cd_unimed_w		:= trim(substr(ds_conteudo_w,50,4));
		cd_usuario_plano_w	:= trim(substr(ds_conteudo_w,54,13));
		dt_solicitacao_ww	:= trim(substr(ds_conteudo_w,75,2))||trim(substr(ds_conteudo_w,72,2))||trim(substr(ds_conteudo_w,67,4))||' '||trim(substr(ds_conteudo_w,77,8));
		dt_resposta_wsd_ww	:= trim(substr(ds_conteudo_w,96,2))||trim(substr(ds_conteudo_w,93,2))||trim(substr(ds_conteudo_w,88,4))||' '||trim(substr(ds_conteudo_w,98,8));
		ie_situacao_opera_w	:= to_number(substr(ds_conteudo_w,109,1));
		
		
		
		if	(nvl(dt_solicitacao_ww,'X')	<> 'X') then
			dt_solicitacao_w	:= to_date(dt_mov_inicial_ww,'dd/mm/yyyy hh24:mi:ss');
		end if;

		if	(nvl(dt_resposta_wsd_ww,'X')	<> 'X') then
			dt_resposta_wsd_w	:= to_date(dt_mov_final_ww,'dd/mm/yyyy hh24:mi:ss');
		end if;
		
		select	ptu_dados_pedido_a1100_seq.NextVal
		into	nr_seq_dados_pedido_w
		from	dual;
		
		insert	into ptu_dados_pedido_a1100
			(nr_sequencia, nr_seq_cabecalho, nm_usuario,
			 nm_usuario_nrec, dt_atualizacao, dt_atualizacao_nrec,
			 nr_seq_arquivo, ie_tipo_registro, cd_unimed_executora,
			 cd_unimed_origem, nr_seq_execucao, nr_seq_origem,
			 nr_seq_trans_refe, cd_unimed, cd_usuario_plano,
			 dt_solicitacao, dt_resposta_wsd, ie_situacao_operadora)
		values	(nr_seq_dados_pedido_w, nr_seq_cabecalho_w, nm_usuario_p,
			 nm_usuario_p, sysdate, sysdate,
			 nr_seq_arquivo_w, ie_tipo_registro_w, cd_unimed_executora_w,
			 cd_unimed_origem_w, nr_seq_execucao_w, nr_seq_origem_w,
			 nr_seq_trans_refe_w, cd_unimed_w, cd_usuario_plano_w,
			 dt_solicitacao_w, dt_resposta_wsd_w, ie_situacao_opera_w);
	--Dados dos itens
	else
		nr_seq_arquivo_w	:= to_number(substr(ds_conteudo_w,1,8));
		ie_tipo_registro_w	:= trim(substr(ds_conteudo_w,9,3));
		ie_tipo_tabela_w	:= to_number(substr(ds_conteudo_w,12,1));
		cd_servico_w		:= to_number(substr(ds_conteudo_w,13,8));
		ds_servico_w		:= trim(substr(ds_conteudo_w,21,80));
		qt_autorizada_w		:= to_number(substr(ds_conteudo_w,101,8));
		ie_resposta_wsd_w	:= to_number(substr(ds_conteudo_w,109,1));
		cd_mensagem_erro_w	:= trim(substr(ds_conteudo_w,110,4));
		
		insert	into ptu_itens_pedido_a1100
			(nr_sequencia, nr_seq_dados_pedido, nm_usuario,
			 nm_usuario_nrec, dt_atualizacao, dt_atualizacao_nrec,
			 nr_seq_arquivo, ie_tipo_registro, ie_tipo_tabela,
			 cd_servico, ds_servico, qt_autorizada,
			 ie_resposta_wsd, cd_mensagem_erro)
		values	(ptu_itens_pedido_a1100_seq.NextVal, nr_seq_dados_pedido_w, nm_usuario_p,
			 nm_usuario_p, sysdate, sysdate,
			 nr_seq_arquivo_w, ie_tipo_registro_w, ie_tipo_tabela_w,
			 cd_servico_w, ds_servico_w, qt_autorizada_w,
			 ie_resposta_wsd_w, cd_mensagem_erro_w);
	end if;
	end;
end loop;
close C01;

commit;

end ptu_importar_a1100;
/