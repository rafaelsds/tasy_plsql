create or replace
procedure HSL_IMPORTAR_RET_MOVTO_CABESP(nr_seq_retorno_p	number) is

vl_total_pago_w		number(17,4);
vl_glosa_w		number(17,4);
cd_convenio_w		number(10);
nr_interno_conta_w	number(10);
nr_seq_min_w		number(20);
nm_paciente_w		varchar2(255);
vl_glosa_char_w		varchar2(255);
vl_total_char_w		varchar2(255);
Cursor C01 is	
select	obter_valor_campo_separador(ds_conteudo,1,';') nr_conta,
	substr(obter_valor_campo_separador(ds_conteudo,2,';'),1,60) nm_paciente,
	replace(obter_valor_campo_separador(ds_conteudo,4,';'),'.','')vl_glosa,
	replace(obter_valor_campo_separador(ds_conteudo,5,';'),'.','') vl_baixa
from	w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p
--and	nr_sequencia	> nr_seq_min_w + 3
and	upper(obter_valor_campo_separador(ds_conteudo,1,';')) not like  '%CONTA%';

begin

select	a.cd_convenio
into	cd_convenio_w
from	convenio_retorno a
where	a.nr_Sequencia = nr_Seq_retorno_p;

select	min(nr_sequencia)
into	nr_seq_min_w
from	w_conv_ret_movto
where	nr_seq_retorno = nr_Seq_retorno_p;

open C01;
loop
fetch C01 into
	nr_interno_conta_w,
	nm_paciente_w,
	vl_glosa_char_w,
	vl_total_char_w;
exit when C01%notfound;
	begin
		
	begin
		VL_TOTAL_PAGO_W	:= to_number(vl_total_char_w);
	exception
	when others then
		VL_TOTAL_PAGO_W	:= 0;
	end;
	begin
		VL_GLOSA_W 	:= to_number(vl_glosa_char_w);
	exception
	when others then
		VL_GLOSA_W	:= 0;
	end;


	begin
	insert 	into convenio_retorno_movto
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_Seq_retorno,
		vl_total_pago,
		nr_conta,
		ds_complemento,
		vl_glosa)
	values	(convenio_retorno_movto_seq.nextval,
		Sysdate,
		'Tasy',
		nr_Seq_retorno_p,
		vl_total_pago_w,
		nr_interno_conta_w,
		nm_paciente_w,
		vl_glosa_w);
	exception
	when others then
		Raise_application_error(-20011,	' CONTA:'||nr_interno_conta_w||chr(13)||
						' PAGO:'||vl_total_pago_w||chr(13)||
						' GLOSA:'||vl_glosa_w||chr(13));
	end;

	end;
end loop;
close C01; 

delete 	from w_conv_ret_movto
where	nr_Seq_retorno	= nr_seq_retorno_p;
commit;


end HSL_IMPORTAR_RET_MOVTO_CABESP;
/
