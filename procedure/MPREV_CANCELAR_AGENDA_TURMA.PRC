create or replace
procedure mprev_cancelar_agenda_turma(	nr_sequencia_p		number,	
					nm_usuario_p		varchar2,
					dt_referencia_p		date,
					qt_cancelamentos_p	out	number) is 

begin

if (nr_sequencia_p is not null and nm_usuario_p is not null and dt_referencia_p is not null) then

	select	count(*)
	into	qt_cancelamentos_p
	from	mprev_agendamento
	where	dt_agenda > dt_referencia_p	
	and	nr_seq_horario_turma = nr_sequencia_p;
	
	update	mprev_agendamento
	set	ie_status_agenda = 'C',
		dt_cancelamento = sysdate,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p,
		ds_observacao = wheb_mensagem_pck.get_texto(802575)||'.'
	where	dt_agenda > dt_referencia_p
	and	nr_seq_horario_turma = nr_sequencia_p;

	update	mprev_grupo_col_turma_hor
	set	dt_cancelamento = sysdate
	where	nr_sequencia = nr_sequencia_p;
	
end if;

commit;

end mprev_cancelar_agenda_turma;
/
