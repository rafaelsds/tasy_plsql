create or replace
procedure pls_gerar_lancamento_import
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

ie_tipo_item_w			varchar2(5);
ie_tipo_lanc_w			varchar2(1);
vl_lancamento_w			number(15,2);
nr_seq_segurado_w		number(10);
nr_seq_pagador_w		number(10);
dt_referencia_w			date;
dt_competencia_w		date;
ie_coparticipacao_w		varchar2(1);
nr_seq_tipo_lanc_item_w		pls_tipo_import_lancamento.nr_seq_tipo_lanc%type;
nr_seq_tipo_lanc_copartic_w	pls_tipo_import_lancamento.nr_seq_tipo_lanc_copartic%type;
nr_seq_tipo_lanc_w		number(10);
qt_registros_w			number(10);
nr_seq_lanc_prog_imp_w		pls_lanc_prog_importacao.nr_sequencia%type;
nr_seq_segurado_mens_w		pls_segurado_mensalidade.nr_sequencia%type;
qt_segurado_w			pls_integer;
qt_segurado_mens_w		pls_integer;
vl_item_w			number(15,2);
vl_total_lancamentos_w		number(15,2);

Cursor C01 is
	select	nr_seq_segurado,
		ie_tipo_item,
		vl_lancamento,
		nr_seq_pagador,
		nr_sequencia,
		'N' ie_coparticipacao,
		dt_competencia --*
	from	pls_lanc_prog_importacao
	where	nr_seq_lote		= nr_seq_lote_p
	and	ie_situacao_lanc	= '3'
	and	vl_lancamento	> 0
	union all
	select	nr_seq_segurado,
		ie_tipo_item,
		vl_coparticipacao,
		nr_seq_pagador,
		nr_sequencia,
		'S' ie_coparticipacao,
		dt_competencia --*
	from	pls_lanc_prog_importacao
	where	nr_seq_lote		= nr_seq_lote_p
	and	ie_situacao_lanc	= '3'
	and	vl_coparticipacao	> 0;

Cursor C02(nr_seq_pagador_pc	pls_contrato_pagador.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_segurado
	from	pls_segurado a
	where	a.nr_seq_pagador = nr_seq_pagador_pc
	and	a.dt_liberacao is not null
	and	a.dt_rescisao is null;	
	
begin

select	max(b.nr_seq_tipo_lanc)
into	nr_seq_tipo_lanc_item_w
from	pls_lanc_prog_import_lote a,
	pls_tipo_import_lancamento b
where	a.nr_seq_tipo_imp_lanc	= b.nr_sequencia
and	a.nr_sequencia	= nr_seq_lote_p;

select	max(b.nr_seq_tipo_lanc_copartic)
into	nr_seq_tipo_lanc_copartic_w
from	pls_lanc_prog_import_lote a,
	pls_tipo_import_lancamento b
where	a.nr_seq_tipo_imp_lanc	= b.nr_sequencia
and	a.nr_sequencia	= nr_seq_lote_p;

select	count(1)
into	qt_registros_w
from	pls_lanc_prog_importacao
where	nr_seq_lote	= nr_seq_lote_p
and	ie_situacao_lanc	= '1';

if	(qt_registros_w > 0) then
	/*Deve ser consistido as informações antes de ser gerado os lançamentos programados!*/
	wheb_mensagem_pck.exibir_mensagem_abort(277565);
end if;

open C01;
loop
fetch C01 into
	nr_seq_segurado_w,
	ie_tipo_item_w,
	vl_lancamento_w,
	nr_seq_pagador_w,
	nr_seq_lanc_prog_imp_w,
	ie_coparticipacao_w,
	dt_competencia_w;--*
exit when C01%notfound;
	begin
	if	(nr_seq_segurado_w is not null) then
		select	max(a.dt_mesano_referencia)
		into	dt_referencia_w
		from	pls_mensalidade_segurado a,
			pls_mensalidade b
		where	a.nr_seq_mensalidade	= b.nr_sequencia
		and	a.nr_seq_segurado	= nr_seq_segurado_w
		and	b.ie_cancelamento is null;
		
		ie_tipo_lanc_w	:= 'B';
	elsif	(nr_seq_pagador_w is not null) then
		select	max(a.dt_mesano_referencia)
		into	dt_referencia_w
		from	pls_mensalidade_segurado a,
			pls_mensalidade b
		where	a.nr_seq_mensalidade	= b.nr_sequencia
		and	b.nr_seq_pagador	= nr_seq_pagador_w
		and	b.ie_cancelamento is null;
		
		ie_tipo_lanc_w	:= 'P';
	end if;
	
	if (dt_competencia_w is not null) then
		dt_referencia_w	:= dt_competencia_w;
	else
		dt_referencia_w	:= trunc(add_months(nvl(dt_referencia_w,sysdate),1),'month');
	end if;
	
	
	if	(ie_tipo_item_w = '20') then
		if	(ie_coparticipacao_w = 'S') then
			nr_seq_tipo_lanc_w	:= nr_seq_tipo_lanc_copartic_w;
		else
			nr_seq_tipo_lanc_w	:= nr_seq_tipo_lanc_item_w;
		end if;
		
		if	(nr_seq_tipo_lanc_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(266730);
		end if;
		
		insert	into	pls_lancamento_mensalidade
			(	nr_sequencia, ie_tipo_item, dt_ocorrencia,
				dt_mes_competencia, nr_seq_motivo, cd_estabelecimento,
				dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_segurado, nr_seq_pagador, vl_lancamento,
				ds_observacao, ie_situacao)
			values
			(	pls_lancamento_mensalidade_seq.nextval, ie_tipo_item_w, sysdate,
				dt_referencia_w, nr_seq_tipo_lanc_w, cd_estabelecimento_p,
				sysdate, nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_segurado_w, nr_seq_pagador_w, vl_lancamento_w,
				'Lançamento gerado por importação', 'A');
	elsif	(nr_seq_segurado_w is not null) then
		insert into pls_segurado_mensalidade(	
			nr_sequencia, 		nr_seq_segurado, 	cd_estabelecimento,
			dt_atualizacao,		nm_usuario, 		dt_atualizacao_nrec,
			nm_usuario_nrec, 	ie_situacao, 		dt_referencia,
			vl_item, 		ie_tipo_item, 		ie_acao_desfazer,
			ie_tipo_lanc)
		values(	nr_seq_segurado_mens_w, nr_seq_segurado_w,	cd_estabelecimento_p,
			sysdate, 		nm_usuario_p,		sysdate,
			nm_usuario_p, 		'A', 			dt_referencia_w,
			vl_lancamento_w,	ie_tipo_item_w, 	'A',
			ie_tipo_lanc_w);
	elsif	(nr_seq_pagador_w is not null) then
		select	count(1)
		into	qt_segurado_mens_w
		from	pls_segurado a
		where	a.nr_seq_pagador = nr_seq_pagador_w
		and	a.dt_liberacao is not null
		and	(a.dt_rescisao is null or a.dt_rescisao > dt_referencia_w);
							
		if 	(qt_segurado_mens_w > 0) then
			qt_segurado_w 		:= 0;
			vl_total_lancamentos_w 	:= 0;
			
			for r_c02_w in C02(nr_seq_pagador_w) loop
				qt_segurado_w := qt_segurado_w + 1;
				
				vl_item_w	:= dividir_sem_round(vl_lancamento_w,qt_segurado_mens_w);
					
				select	pls_segurado_mensalidade_seq.nextval
				into	nr_seq_segurado_mens_w
				from	dual;
		
				insert into pls_segurado_mensalidade(	
					nr_sequencia, 		nr_seq_segurado, 	cd_estabelecimento,
					dt_atualizacao,		nm_usuario, 		dt_atualizacao_nrec,
					nm_usuario_nrec, 	ie_situacao, 		dt_referencia,
					vl_item, 		ie_tipo_item, 		ie_acao_desfazer,
					ie_tipo_lanc)
				values(	nr_seq_segurado_mens_w, r_c02_w.nr_seq_segurado,cd_estabelecimento_p,
					sysdate, 		nm_usuario_p,		sysdate,
					nm_usuario_p, 		'A', 			dt_referencia_w,
					vl_item_w,		ie_tipo_item_w, 	'A',
					ie_tipo_lanc_w);
				
				/* Se for o último fazer ajuste de arredondamento */
				if	(qt_segurado_w = qt_segurado_mens_w) then
					update	pls_segurado_mensalidade
					set	vl_item		= vl_item + (vl_lancamento_w - vl_total_lancamentos_w)
					where	nr_sequencia	= nr_seq_segurado_mens_w;
				end if;
				
				vl_total_lancamentos_w := vl_total_lancamentos_w + vl_item_w;
			end loop;
		end if;
	end if;
	
	update	pls_lanc_prog_importacao
	set	ie_situacao_lanc	= '4',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_lanc_prog_imp_w;
	
	dt_referencia_w		:= sysdate;
	nr_seq_pagador_w	:= null;
	nr_seq_segurado_w	:= null;
	ie_tipo_lanc_w		:= null;
	end;
end loop;
close C01;

commit;

end pls_gerar_lancamento_import;
/
