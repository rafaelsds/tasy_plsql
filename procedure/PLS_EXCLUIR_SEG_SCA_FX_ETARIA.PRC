create or replace
procedure pls_excluir_seg_sca_fx_etaria
		(	nr_seq_segurado_sca_p	number	) is 

begin

delete	
from	pls_segurado_preco_origem
where	nr_sequencia = nr_seq_segurado_sca_p;

commit;

end pls_excluir_seg_sca_fx_etaria;
/