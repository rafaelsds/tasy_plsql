create or replace
function obter_se_possui_pac_risco(cd_pessoa_fisica_p	varchar2)
 		    	return varchar2 is

ie_quant_w      number(10):=0;
ds_expressao_w  varchar2(255):='';
			
begin

if(cd_pessoa_fisica_p is not null)then
	select	count(a.nr_sequencia)
		into  ie_quant_w
	from    asa_apae            a,
		aval_pre_anestesica b
	where   b.nr_sequencia      = a.nr_seq_aval_pre
	and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p
	and     b.dt_liberacao is not null
	and     b.dt_inativacao is null;
	
	if(ie_quant_w = 0)then
		select	count(a.nr_sequencia)
		into	ie_quant_w
		from    escala_possum       a,
			aval_pre_anestesica b 
		where   a.nr_seq_apae       = b.nr_sequencia 
		and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p
		and     a.dt_liberacao is not null
		and     a.dt_inativacao is null
		and     b.dt_liberacao is not null
		and     b.dt_inativacao is null;
	end if;
	
	if(ie_quant_w = 0)then
		select	count(a.nr_sequencia)
		into	ie_quant_w
		from    escala_lee a,
			aval_pre_anestesica b 
		where   a.nr_seq_aval_pre = a.nr_sequencia
		and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p
		and     a.dt_liberacao is not null
		and     a.dt_inativacao is null
		and     b.dt_liberacao is not null
		and     b.dt_inativacao is null;
	end if;
	if(ie_quant_w = 0)then
		select	count(a.nr_sequencia)
		into	ie_quant_w
		from    escala_risco_pulmonar a,
			aval_pre_anestesica b 
		where   b.nr_sequencia = a.nr_seq_aval_pre
		and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p
		and     a.dt_liberacao is not null
		and     a.dt_inativacao is null
		and     b.dt_liberacao is not null
		and     b.dt_inativacao is null;
	end if;
	if(ie_quant_w = 0)then
		select	count(a.nr_sequencia)
		into	ie_quant_w
		from    escala_risco_insuf_renal a,
			aval_pre_anestesica b 
		where   a.nr_seq_aval_pre = b.nr_sequencia
		and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p
		and     a.dt_liberacao is not null
		and     a.dt_inativacao is null
		and     b.dt_liberacao is not null
		and     b.dt_inativacao is null;
	end if;
	if(ie_quant_w = 0)then
		select	count(a.nr_sequencia)
		into	ie_quant_w
		from    escala_child_pugh a,
			aval_pre_anestesica b 
		where   a.nr_seq_aval_pre = b.nr_sequencia
		and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p
		and     a.dt_liberacao is not null
		and     a.dt_inativacao is null
		and     b.dt_liberacao is not null
		and     b.dt_inativacao is null;
	end if;
	if(ie_quant_w = 0)then
		select	count(a.nr_sequencia)
		into	ie_quant_w
		from    escala_risco_delirium a,
			aval_pre_anestesica b 
		where   a.nr_seq_aval_pre = b.nr_sequencia
		and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p
		and     a.dt_liberacao is not null
		and     a.dt_inativacao is null
		and     b.dt_liberacao is not null
		and     b.dt_inativacao is null;
	end if;
	if(ie_quant_w = 0)then
		select	count(a.nr_sequencia)
		into	ie_quant_w
		from    resultado_risco_apae a,  
			aval_pre_anestesica b 
		where   a.nr_seq_apae = b.nr_sequencia
		and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p		
		and     b.dt_liberacao is not null
		and     b.dt_inativacao is null;
	end if;
	if(ie_quant_w = 0)then
		select	count(a.nr_sequencia)
		into	ie_quant_w
		from    pac_clereance_creatinina a,
			aval_pre_anestesica b
		where   a.nr_seq_apae = b.nr_sequencia
		and     b.cd_pessoa_fisica  = cd_pessoa_fisica_p
		and     a.dt_liberacao is not null
		and     a.dt_inativacao is null
		and     b.dt_liberacao is not null
		and     b.dt_inativacao is null;
	end if;	
end if;

if(ie_quant_w > 0)then
 	ds_expressao_w := obter_desc_expressao(883834);	
end if;	

return	ds_expressao_w;

end obter_se_possui_pac_risco;
/
