create or replace
procedure Excluir_Itens_Exec_Auditoria(
			nr_seq_auditoria_p		number,
			cd_motivo_exc_conta_p	number,
			nm_usuario_p			varchar2) is 
			
ie_proc_mat_w		varchar2(1);
nr_seq_item_w		number(10);			
nr_interno_conta_w	number(10);
			
Cursor C01 is
	select	'P',
			a.nr_seq_propaci,
			b.nr_interno_conta
	from	auditoria_propaci a,
			procedimento_paciente b
	where	a.nr_seq_propaci		= b.nr_sequencia
	and		nr_seq_auditoria		= nr_seq_auditoria_p
	and		ie_tipo_auditoria		= 'E'
	union all
	select	'M',
			nr_seq_matpaci,
			b.nr_interno_conta
	from	auditoria_matpaci a,
			material_atend_paciente b
	where	a.nr_seq_matpaci 	= b.nr_sequencia
	and		nr_seq_auditoria		= nr_seq_auditoria_p
	and		ie_tipo_auditoria		= 'E'
	order by 1;
begin

if	(cd_motivo_exc_conta_p is not null) then
	open C01;
	loop
	fetch C01 into	
		ie_proc_mat_w,
		nr_seq_item_w,
		nr_interno_conta_w;
	exit when C01%notfound;
		begin
		excluir_matproc_conta(nr_seq_item_w,nr_interno_conta_w,cd_motivo_exc_conta_p,null,ie_proc_mat_w,nm_usuario_p);
		end;
	end loop;
	close C01;
end if;
commit;

end Excluir_Itens_Exec_Auditoria;
/