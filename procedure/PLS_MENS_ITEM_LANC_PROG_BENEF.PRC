create or replace
procedure pls_mens_item_lanc_prog_benef
			(	nr_seq_mensalidade_p		pls_mensalidade.nr_sequencia%type,
				nr_seq_mensalidade_seg_p	pls_mensalidade_segurado.nr_sequencia%type,
				dt_referencia_p			pls_mensalidade_segurado.dt_mesano_referencia%type,
				nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
				nr_seq_pagador_p		pls_contrato_pagador.nr_sequencia%type,
				nr_seq_solic_resc_fin_p		pls_solic_rescisao_fin.nr_sequencia%type,
				dt_referencia_lote_p		pls_lote_mensalidade.dt_mesano_referencia%type,
				ie_desconsiderar_vl_limite_p	pls_causa_rescisao.ie_desconsiderar_vl_limite%type,
				nm_usuario_p			usuario.nm_usuario%type) is

dt_referencia_inicio_w		date;
dt_referencia_fim_w		date;
ie_inseriu_item_w		varchar2(1);
nr_indice_aprop_w		number;
nr_operacao_w			number;
vl_lancamento_w			pls_lancamento_mensalidade.vl_lancamento%type;
vl_saldo_lancamento_w		pls_lancamento_mensalidade.vl_lancamento%type;
vl_mensalidade_w		pls_mensalidade_seg_item.vl_item%type;
vl_item_nao_inserido_w		pls_mensalidade_seg_item.vl_item%type;
ds_observacao_w			pls_segurado_mensalidade.ds_observacao%type;
ds_msg_reajuste_w		pls_segurado_mensalidade.ds_observacao%type;
regra_limite_lanc_w		pls_mens_itens_limite_pck.regra_limite_item;
indice_regra_limite_w		number;
apropriacao_item_w		pls_mens_itens_pck.tb_apropriacao_item;
ie_apropriacao_w		varchar2(1);
ie_pagador_item_w		varchar2(1);
ie_item_apropriacao_w		varchar2(1);
tb_nr_seq_centro_aprop_pag_w	pls_util_cta_pck.t_number_table;
nr_indice_centro_aprop_pag_w	pls_integer;
ie_apropriacao_sem_saldo_w	varchar2(1);

--Programacao Manual
Cursor C01 is
	select	a.vl_lancamento,
		a.ie_tipo_item,
		a.nr_seq_motivo,
		a.cd_centro_custo,
		a.nr_sequencia,
		a.ds_observacao,
		b.ie_operacao_motivo,
		a.nr_seq_pagador,
		(select	nvl(sum(x.vl_item),0)
		from	pls_mensalidade_seg_item x
		where	a.nr_sequencia	= x.nr_seq_lancamento_mens) vl_baixa,
		a.dt_mes_competencia,
		(select	max(x.nr_seq_titular)
		from	pls_segurado x
		where	x.nr_sequencia = nr_seq_segurado_p) nr_seq_titular,
		a.dt_inicio_cobertura,
		a.dt_fim_cobertura
	from	pls_lancamento_mensalidade	a,
		pls_tipo_lanc_adic		b
	where	b.nr_sequencia		= a.nr_seq_motivo
	and	a.nr_seq_segurado	= nr_seq_segurado_p
	and	a.dt_liberacao is not null
	and	a.nm_usuario_liberacao is not null
	and	a.vl_saldo > 0
	and	a.ie_situacao	= 'A'
	order by b.ie_operacao_motivo, --Necessario lancar primeiramente os descontos
		a.dt_mes_competencia; --Priorizar os lancamentos mais antigos

--Programacao automatica
Cursor C02 is
	select	nr_sequencia,
		ie_tipo_item,
		nvl(vl_item,0) vl_lancamento,
		nvl(tx_desconto,0) tx_desconto,
		decode(ie_tipo_lanc,'P','P','N') ie_tipo_mensalidade,
		nr_seq_motivo,
		ds_observacao,
		nr_seq_pagador,
		dt_inicio_cobertura,
		dt_fim_cobertura,
		nr_seq_vinculo_sca,
		nr_parcela_sca,
		dt_referencia,
		ie_recomposicao
	from	pls_segurado_mensalidade
	where	nr_seq_segurado	= nr_seq_segurado_p
	and	nr_seq_bonificacao is null
	and	ie_situacao	= 'A';

Cursor C03 (	nr_seq_lanc_auto_pc	pls_segurado_mensalidade.nr_sequencia%type) is
	select	vl_apropriacao,
		nr_seq_centro_apropriacao
	from	pls_lancamento_mens_aprop
	where	nr_seq_lanc_auto = nr_seq_lanc_auto_pc;
	
Cursor C04 (	nr_seq_lancamento_pc		pls_lancamento_mensalidade.nr_sequencia%type,
		ie_apropriacao_pc		varchar2,
		nr_seq_regra_pc			number,
		ie_operacao_motivo_pc		varchar2,
		nr_seq_centro_apropriacao_pc	pls_centro_apropriacao.nr_sequencia%type) is
	select	vl_apropriacao,
		nr_seq_centro_apropriacao,
		vl_apropriado,
		qt_registros_regra_apropriacao
	from	(select	decode(ie_operacao_motivo_pc, 'D', a.vl_apropriacao * -1, a.vl_apropriacao) vl_apropriacao,
			a.nr_seq_centro_apropriacao,
			(select	nvl(sum(x.vl_apropriacao),0)
			from	pls_mens_seg_item_aprop		x,
				pls_mensalidade_seg_item	i
			where	i.nr_sequencia			= x.nr_seq_item
			and	i.nr_seq_lancamento_mens	= a.nr_seq_lancamento
			and	x.nr_seq_centro_apropriacao	= a.nr_seq_centro_apropriacao) vl_apropriado,
			0	qt_registros_regra_apropriacao
		from	pls_lancamento_mens_aprop	a,
			pls_lancamento_mensalidade	b
		where	b.nr_sequencia	= a.nr_seq_lancamento
		and	a.nr_seq_lancamento = nr_seq_lancamento_pc
		and	a.nr_seq_centro_apropriacao = nr_seq_centro_apropriacao_pc
		and	ie_apropriacao_pc = 'N'
		union
		select	decode(ie_operacao_motivo_pc, 'D', a.vl_apropriacao * -1, a.vl_apropriacao) vl_apropriacao,
			a.nr_seq_centro_apropriacao,
			(select	nvl(sum(x.vl_apropriacao),0)
			from	pls_mens_seg_item_aprop		x,
				pls_mensalidade_seg_item	i
			where	i.nr_sequencia			= x.nr_seq_item
			and	i.nr_seq_lancamento_mens	= a.nr_seq_lancamento
			and	x.nr_seq_centro_apropriacao	= a.nr_seq_centro_apropriacao) vl_apropriado,
			(select	count(1)
			from	pls_regra_limite_mens_apro x
			where	x.nr_seq_regra = nr_seq_regra_pc
			and	x.nr_seq_centro_apropriacao = a.nr_seq_centro_apropriacao) qt_registros_regra_apropriacao
		from	pls_lancamento_mens_aprop	a,
			pls_lancamento_mensalidade	b
		where	b.nr_sequencia	= a.nr_seq_lancamento
		and	a.nr_seq_lancamento = nr_seq_lancamento_pc
		and	a.nr_seq_centro_apropriacao = nr_seq_centro_apropriacao_pc
		and	ie_apropriacao_pc = 'S')
	order by qt_registros_regra_apropriacao; --Ordenacao necessaria para tratamento de arredondamento

Cursor C05 (	nr_seq_lancamento_pc	pls_lancamento_mensalidade.nr_sequencia%type) is
	select	nr_seq_centro_apropriacao
	from	pls_lancamento_mens_aprop
	where	nr_seq_lancamento = nr_seq_lancamento_pc;
	
begin

dt_referencia_inicio_w	:= trunc(dt_referencia_p,'month');
dt_referencia_fim_w	:= fim_dia(last_day(dt_referencia_p));

--Lancamento Manual
for r_c01_w in C01 loop
	begin
	ie_pagador_item_w	:= 'N';
	ie_item_apropriacao_w	:= 'N';
	tb_nr_seq_centro_aprop_pag_w.delete;
	nr_indice_centro_aprop_pag_w	:= 0;
	
	if	(pls_mens_itens_pck.obter_se_item_grupo(r_c01_w.ie_tipo_item,r_c01_w.nr_seq_motivo) = 'S') and
		((r_c01_w.dt_mes_competencia <= dt_referencia_fim_w) or (nr_seq_solic_resc_fin_p is not null)) then
		if	((r_c01_w.nr_seq_pagador is not null) and (r_c01_w.nr_seq_pagador = nr_seq_pagador_p)) then
			for r_c05_w in c05 (r_c01_w.nr_sequencia) loop
				begin
				tb_nr_seq_centro_aprop_pag_w(nr_indice_centro_aprop_pag_w) := r_c05_w.nr_seq_centro_apropriacao;
				nr_indice_centro_aprop_pag_w	:= nr_indice_centro_aprop_pag_w + 1;
				ie_item_apropriacao_w	:= 'S';
				end;
			end loop;
			ie_pagador_item_w	:= 'S';
		else
			for r_c05_w in c05 (r_c01_w.nr_sequencia) loop
				begin
				if	((r_c01_w.nr_seq_pagador is null) and 
					(pls_mens_itens_pck.obter_se_pagador_item(nr_seq_segurado_p, nr_seq_pagador_p, r_c01_w.ie_tipo_item, r_c01_w.nr_seq_motivo, null, r_c05_w.nr_seq_centro_apropriacao) = 'S')) then
					ie_pagador_item_w	:= 'S';
					tb_nr_seq_centro_aprop_pag_w(nr_indice_centro_aprop_pag_w) := r_c05_w.nr_seq_centro_apropriacao;
					nr_indice_centro_aprop_pag_w	:= nr_indice_centro_aprop_pag_w + 1;
				end if;
				ie_item_apropriacao_w	:= 'S';
				end;
			end loop;
			
			if	((ie_item_apropriacao_w = 'N') and
				(r_c01_w.nr_seq_pagador is null) and 
				(pls_mens_itens_pck.obter_se_pagador_item(nr_seq_segurado_p, nr_seq_pagador_p, r_c01_w.ie_tipo_item, r_c01_w.nr_seq_motivo, null, null) = 'S')) then
				ie_pagador_item_w	:= 'S';
			end if;
		end if;

		if	(ie_pagador_item_w = 'S') then
			apropriacao_item_w.delete;
			nr_indice_aprop_w		:= 0;
			vl_saldo_lancamento_w		:= 0;
			ie_apropriacao_w		:= 'N';
			ie_apropriacao_sem_saldo_w	:= 'N';
			
			if	(ie_desconsiderar_vl_limite_p = 'N') then
				indice_regra_limite_w	:= pls_mens_itens_limite_pck.obter_indice_regra_item('20',r_c01_w.nr_seq_motivo);
			end if;

			if	(indice_regra_limite_w is not null) then
				regra_limite_lanc_w	:= pls_mens_itens_limite_pck.obter_regra_limite(indice_regra_limite_w);
			else
				regra_limite_lanc_w	:= null;
			end if;
			
			if	(tb_nr_seq_centro_aprop_pag_w.count > 0) then
				for i in tb_nr_seq_centro_aprop_pag_w.first..tb_nr_seq_centro_aprop_pag_w.last loop
					begin
					for r_c04_w in C04(r_c01_w.nr_sequencia, nvl(regra_limite_lanc_w.ie_apropriacao,'N'), regra_limite_lanc_w.nr_seq_regra, r_c01_w.ie_operacao_motivo, tb_nr_seq_centro_aprop_pag_w(i)) loop
						begin
						if	((abs(r_c04_w.vl_apropriacao) - abs(r_c04_w.vl_apropriado)) > 0) then
							nr_indice_aprop_w	:= nr_indice_aprop_w + 1;
	
							apropriacao_item_w(nr_indice_aprop_w).nr_seq_centro_apropriacao	:= r_c04_w.nr_seq_centro_apropriacao;
							apropriacao_item_w(nr_indice_aprop_w).vl_apropriacao		:= r_c04_w.vl_apropriacao - (r_c04_w.vl_apropriado + pls_mens_itens_pck.obter_vl_lanc_aprop_ger_vetor(r_c01_w.nr_sequencia, tb_nr_seq_centro_aprop_pag_w(i)));

							if	((r_c04_w.qt_registros_regra_apropriacao > 0) or (nvl(regra_limite_lanc_w.ie_apropriacao,'N') = 'N')) then
								apropriacao_item_w(nr_indice_aprop_w).vl_apropriacao := pls_mens_itens_limite_pck.obter_valor_regra_limite(indice_regra_limite_w, apropriacao_item_w(nr_indice_aprop_w).vl_apropriacao, 'S');
							end if;
							
							vl_saldo_lancamento_w	:= vl_saldo_lancamento_w + apropriacao_item_w(nr_indice_aprop_w).vl_apropriacao;

							ie_apropriacao_w	:= 'S';
						else
							ie_apropriacao_sem_saldo_w	:= 'S';
						end if;
						end;
					end loop;
					end;
				end loop;
			end if;

			if	((ie_apropriacao_w = 'N') and (ie_apropriacao_sem_saldo_w = 'S')) then
				null; -- Ja apropriou o saldo total de todos os item do pagador
			else
				if	(ie_apropriacao_w = 'N') then
					vl_saldo_lancamento_w	:= r_c01_w.vl_lancamento - (abs(r_c01_w.vl_baixa) + abs(pls_mens_itens_pck.obter_vl_lanc_aprop_ger_vetor(r_c01_w.nr_sequencia, null)));

					if	(r_c01_w.ie_operacao_motivo = 'D') then
						vl_saldo_lancamento_w := vl_saldo_lancamento_w * -1;
					elsif	(r_c01_w.ie_operacao_motivo = 'S') then
						vl_saldo_lancamento_w := vl_saldo_lancamento_w;
					end if;
					
					if	(indice_regra_limite_w is not null and nvl(regra_limite_lanc_w.ie_apropriacao,'N') = 'N') then
						vl_saldo_lancamento_w	:= pls_mens_itens_limite_pck.obter_valor_regra_limite(indice_regra_limite_w, vl_saldo_lancamento_w, 'S');
					end if;
				end if;
				
				if	(vl_saldo_lancamento_w <> 0) then
					pls_mens_itens_pck.add_item_lanc(nr_seq_mensalidade_p,nr_seq_mensalidade_seg_p, r_c01_w.ie_tipo_item, 
									r_c01_w.nr_seq_motivo, vl_saldo_lancamento_w, 'N',
									r_c01_w.nr_sequencia, null, r_c01_w.cd_centro_custo, 
									r_c01_w.dt_inicio_cobertura, r_c01_w.dt_fim_cobertura, null, 
									null, null, r_c01_w.ds_observacao, 
									apropriacao_item_w, nr_seq_segurado_p, r_c01_w.nr_seq_titular,
									null, ie_inseriu_item_w);
				end if;
			
			apropriacao_item_w.delete;
			end if;
		end if;
	end if;
	end;
end loop;--C01
	
--Lancamento automatico
for r_c02_w in C02 loop
	begin

	if	(pls_mens_itens_pck.obter_se_item_grupo(r_c02_w.ie_tipo_item,r_c02_w.nr_seq_motivo) = 'S') and
		((r_c02_w.dt_referencia between dt_referencia_inicio_w and dt_referencia_fim_w) or 
		 ((nr_seq_solic_resc_fin_p is not null and dt_referencia_lote_p = dt_referencia_inicio_w) and ((r_c02_w.ie_tipo_item <> '4') or (pls_mensalidade_util_pck.get_ie_adiantar_cobranca_retro = 'S' and r_c02_w.ie_tipo_item = '4')))) and
		(pls_mens_itens_pck.obter_se_item_vetor(nr_seq_segurado_p, r_c02_w.ie_tipo_item, nr_seq_pagador_p, null, r_c02_w.nr_sequencia) = 'N') then
		if	(((r_c02_w.nr_seq_pagador is not null) and (r_c02_w.nr_seq_pagador = nr_seq_pagador_p)) or
			 ((r_c02_w.nr_seq_pagador is null) and (pls_mens_itens_pck.obter_se_pagador_item(nr_seq_segurado_p, nr_seq_pagador_p, r_c02_w.ie_tipo_item, r_c02_w.nr_seq_motivo, null, null) = 'S'))) then

			apropriacao_item_w.delete;
			nr_indice_aprop_w		:= 0;
			 
			if	(r_c02_w.tx_desconto > 0) then
				select	sum(vl_item)
				into	vl_mensalidade_w
				from	pls_mensalidade_seg_item
				where	nr_seq_mensalidade_seg = nr_seq_mensalidade_seg_p;
				
				vl_item_nao_inserido_w	:= pls_mens_itens_pck.obter_valor_mens(nr_seq_mensalidade_seg_p,null,null);
				vl_mensalidade_w	:= nvl(vl_mensalidade_w,0) + nvl(vl_item_nao_inserido_w,0);
				
				vl_lancamento_w	:= ((r_c02_w.tx_desconto/100) * vl_mensalidade_w) + r_c02_w.vl_lancamento;
			else
				vl_lancamento_w	:= r_c02_w.vl_lancamento;
			end if;
			
			if	(vl_lancamento_w <> 0) then
				if	(r_c02_w.ie_tipo_item = '14') and
					(vl_lancamento_w > 0) then
					vl_lancamento_w	:= vl_lancamento_w * -1;
				end if;
				
				if	(r_c02_w.ie_tipo_item in ('4','45')) then
					ds_observacao_w		:= '';
					ds_msg_reajuste_w	:= r_c02_w.ds_observacao;
				else
					ds_observacao_w		:= r_c02_w.ds_observacao;
					ds_msg_reajuste_w	:= '';
				end if;
				
				if	(r_c02_w.ie_tipo_item = '4') then
					for r_c03_w in c03 (r_c02_w.nr_sequencia) loop
						begin
						nr_indice_aprop_w	:= nr_indice_aprop_w + 1;
						
						apropriacao_item_w(nr_indice_aprop_w).nr_seq_centro_apropriacao	:= r_c03_w.nr_seq_centro_apropriacao;
						apropriacao_item_w(nr_indice_aprop_w).vl_apropriacao		:= r_c03_w.vl_apropriacao;
						end;
					end loop;
				end if;
				
				pls_mens_itens_pck.add_item_lanc(nr_seq_mensalidade_p,nr_seq_mensalidade_seg_p, r_c02_w.ie_tipo_item, 
								r_c02_w.nr_seq_motivo, vl_lancamento_w, r_c02_w.ie_tipo_mensalidade,
								null, r_c02_w.nr_sequencia, null, 
								r_c02_w.dt_inicio_cobertura, r_c02_w.dt_fim_cobertura, r_c02_w.nr_seq_vinculo_sca, 
								r_c02_w.nr_parcela_sca, ds_msg_reajuste_w, ds_observacao_w, 
								apropriacao_item_w, nr_seq_segurado_p, null,
								r_c02_w.ie_recomposicao, ie_inseriu_item_w);
				--Na rotina pls_mens_itens_pck.atualizar_pls_mens_seg_item e feito update na tabela pls_segurado_mensalidade gravando a sequencia do item de mensalidade
			end if;
			
			apropriacao_item_w.delete;
		end if;
	end if;
	end;
end loop;--C02

commit;

end pls_mens_item_lanc_prog_benef;
/