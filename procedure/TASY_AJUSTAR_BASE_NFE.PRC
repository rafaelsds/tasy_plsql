create or replace
procedure tasy_ajustar_base_nfe(	nm_usuario_p	varchar2) is 

dt_versao_atual_cliente_w 	date;
qt_registro_w			number;

begin

dt_versao_atual_cliente_w := nvl(to_date(to_char(obter_data_geracao_versao-1,'dd/mm/yyyy') ||' 23:59:59','dd/mm/yyyy hh24:mi:ss'),sysdate-90);

if	(dt_versao_atual_cliente_w < to_date('18/09/2012','dd/mm/yyyy')) then
	select 	count(*)
	into	qt_registro_w
	from	regra_consistencia_nfe;
	
	if (qt_registro_w > 0) then

	update	regra_consistencia_nfe
	set	ie_tipo_nota = 2;

	insert into regra_consistencia_nfe (nr_sequencia,
					dt_atualizacao,      
					nm_usuario,        
					dt_atualizacao_nrec,
					nm_usuario_nrec,   
					ie_campo_consistir,
					ie_tipo_nota)
	select 		regra_consistencia_nfe_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ie_campo_consistir,
			1
	from	regra_consistencia_nfe;

	end if;	
	
	commit;
	
end if;

if	(dt_versao_atual_cliente_w < to_date('06/12/2012','dd/mm/yyyy')) then

	Update xml_elemento 
	set ds_sql = 
	'select	0 orig,' || ' '||
	'	' || chr(39) || '00' || chr(39) || ' CST,' || ' '||
	'	0 modBC,' || ' '||
	'	max(vBC) vBC,' || ' '||
	'	max(pICMS) pICMS,' || ' '||
	'	max(vICMS) vICMS' || ' '||
	'from	(' || ' '||
	'	select	to_char(a.vl_base_calculo ,' || chr(39) || 'FM99999990.0099' || chr(39) || ') vBC,' || ' '||
	'		to_char(a.tx_tributo,' || chr(39) || 'FM99999990.0099' || chr(39) || ') pICMS,' || ' '||
	'		to_char(a.vl_tributo,' || chr(39) || 'FM99990.0099' || chr(39) || ') vICMS,' || ' '||
	'		b.ie_tributacao_icms' || ' '||
	'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
	'		material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
	'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
	'	and	a.cd_tributo = d.cd_tributo' || ' '||
	'	and	c.cd_material = b.cd_material' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '00' || chr(39) || '' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	' || chr(39) || '0' || chr(39) || ' vBC,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' pICMS,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' vICMS,' || ' '||
	'		b.ie_tributacao_icms' || ' '||
	'	from	material_fiscal b,' || ' '||
	'		nota_fiscal_item c' || ' '||
	'	where	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '00' || chr(39) || ')' || ' '||
	'where ie_tributacao_icms = ' || chr(39) || '00' || chr(39) || '' || ' '||
	'group by ie_tributacao_icms' 
	where nr_sequencia in (18727,18407);

	Update xml_elemento 
	set ds_sql = 
	'select	0 orig,' || ' '||
	'	' || chr(39) || '10' || chr(39) || ' CST,' || ' '||
	'	0 modBC,' || ' '||
	'	sum(vBC) vBC,' || ' '||
	'	sum(pICMS) pICMS,' || ' '||
	'	sum(vICMS) vICMS,' || ' '||
	'	null pMVAST,' || ' '||
	'	null pRedBCST,	' || ' '||
	'	sum(vBCST) vBCST,' || ' '||
	'	sum(pICMSST) pICMSST,' || ' '||
	'	sum(vICMSST) vICMSST,' || ' '||
	'	0 modBCST' || ' '||
	'from	(' || ' '||
	'	select	a.vl_base_calculo vBC,' || ' '||
	'		a.tx_tributo      pICMS,' || ' '||
	'		to_char(a.vl_tributo,' || chr(39) || 'FM9999.0099' || chr(39) || ')  vICMS,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'BC' || chr(39) || ') vBCST,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'PIMP' || chr(39) || ') pICMSST,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'VIMP' || chr(39) || ') vICMSST,' || ' '||
	'		b.ie_tributacao_icms' || ' '||
	'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
	'		material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
	'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
	'	and	a.cd_tributo = d.cd_tributo' || ' '||
	'	and	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '10' || chr(39) || '' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	0 vBC,' || ' '||
	'		0 pICMS,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' vICMS,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' vBCST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' pICMSST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' vICMSST,' || ' '||
	'		b.ie_tributacao_icms' || ' '||
	'	from	material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '10' || chr(39) || ')' || ' '||
	'where	ie_tributacao_icms = ' || chr(39) || '10' || chr(39) || '' || ' '||
	'group by ie_tributacao_icms' 
	where nr_sequencia in (18728,18408);

	Update xml_elemento 
	set ds_sql = 
	'select	0 orig,' || ' '||
	'	' || chr(39) || '20' || chr(39) || ' CST,' || ' '||
	'	0 modBC,' || ' '||
	'	max(vBC) vBC,' || ' '||
	'	max(pICMS) pICMS,' || ' '||
	'	max(vICMS) vICMS' || ' '||
	'from	(' || ' '||
	'	select	to_char(a.vl_base_calculo ,' || chr(39) || 'FM99999990.0099' || chr(39) || ') vBC,' || ' '||
	'		to_char(a.tx_tributo,' || chr(39) || 'FM99999990.0099' || chr(39) || ') pICMS,' || ' '||
	'		to_char(a.vl_tributo,' || chr(39) || 'FM99990.0099' || chr(39) || ') vICMS,' || ' '||
	'		b.ie_tributacao_icms' || ' '||
	'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
	'		material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
	'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
	'	and	a.cd_tributo = d.cd_tributo' || ' '||
	'	and	c.cd_material = b.cd_material' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '20' || chr(39) || '' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	' || chr(39) || '0' || chr(39) || ' vBC,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' pICMS,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' vICMS,' || ' '||
	'		b.ie_tributacao_icms' || ' '||
	'	from	material_fiscal b,' || ' '||
	'		nota_fiscal_item c' || ' '||
	'	where	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '20' || chr(39) || ')' || ' '||
	'where ie_tributacao_icms = ' || chr(39) || '20' || chr(39) || '' || ' '||
	'group by ie_tributacao_icms' 
	where nr_sequencia in (18729,18409);

	Update xml_elemento 
	set ds_sql = 
	'select	0 orig,' || ' '||
	'	' || chr(39) || '30' || chr(39) || ' CST,' || ' '||
	'	0 modBCST,' || ' '||
	'	null pMVAST,' || ' '||
	'	null pRedBCST,' || ' '||
	'	max(vBCST) vBCST,' || ' '||
	'	max(pICMSST) pICMSST,' || ' '||
	'	max(vICMSST) vICMSST' || ' '||
	'from	(' || ' '||
	'	select	nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'STI' || chr(39) || ',' || chr(39) || 'BC' || chr(39) || ') vBCST,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'STI' || chr(39) || ',' || chr(39) || 'PIMP' || chr(39) || ') pICMSST,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'STI' || chr(39) || ',' || chr(39) || 'VIMP' || chr(39) || ') vICMSST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
	'		material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
	'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
	'	and	c.cd_material = b.cd_material' || ' '||
	'	and	a.cd_tributo = d.cd_tributo' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '30' || chr(39) || '' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	' || chr(39) || '0' || chr(39) || ' vBCST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' pICMSST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || ' vICMSST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	material_fiscal b,' || ' '||
	'		nota_fiscal_item c' || ' '||
	'	where	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '30' || chr(39) || ')' || ' '||
	'where ie_tributacao_icms = ' || chr(39) || '30' || chr(39) || '' || ' '||
	'group by ie_tributacao_icms' 
	where nr_sequencia in (18730,18410);

	Update xml_elemento 
set ds_sql = 
'select	0 orig,' || ' '||
'	max(CST) CST,' || ' '||
'	max(vICMS) vICMS,' || ' '||
'	max(motDesICMS) motDesICMS' || ' '||
'from	(' || ' '||
'	select	b.ie_tributacao_icms CST,' || ' '||
'		decode(a.vl_tributo, 0, null, decode(b.ie_tributacao_icms, 41, null,to_char(a.vl_tributo,' || chr(39) || 'FM9999999990.0099' || chr(39) || ')))  vICMS,' || ' '||
'		decode(a.vl_tributo, 0, null, decode(b.ie_tributacao_icms, 41, null,' || chr(39) || '9' || chr(39) || ')) motDesICMS,' || ' '||
'		ie_tributacao_icms' || ' '||
'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
'		material_fiscal b,' || ' '||
'		nota_fiscal_item c,' || ' '||
'		tributo d' || ' '||
'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
'	and	c.cd_material = b.cd_material' || ' '||
'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
'	and	a.cd_tributo = d.cd_tributo' || ' '||
'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
'	and	b.ie_tributacao_icms in (40,41,50)' || ' '||
'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
'	union all' || ' '||
'	select	b.ie_tributacao_icms CST,' || ' '||
'		null  vICMS,' || ' '||
'		null motDesICMS,' || ' '||
'		ie_tributacao_icms' || ' '||
'	from	material_fiscal b,' || ' '||
'		nota_fiscal_item c' || ' '||
'	where	c.cd_material = b.cd_material' || ' '||
'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
'	and	b.ie_tributacao_icms in (40,41,50))' || ' '||
'where ie_tributacao_icms in (40,41,50)' || ' '||
'group by ie_tributacao_icms 	' 
where nr_sequencia in (18731,18411);

	Update xml_elemento 
	set ds_sql = 
	'select	0 orig,' || ' '||
	'	' || chr(39) || '51' || chr(39) || ' CST,' || ' '||
	'	0 modBC,' || ' '||
	'	max(vBC) vBC,' || ' '||
	'	max(pICMS) pICMS,' || ' '||
	'	max(vICMS) vICMS,' || ' '||
	'	to_char(0,' || chr(39) || '0.00' || chr(39) || ') pRedBC	' || ' '||
	'from	(' || ' '||
	'	select	to_char(a.vl_base_calculo ,' || chr(39) || 'FM9999.0099' || chr(39) || ') vBC,' || ' '||
	'		to_char(a.tx_tributo,' || chr(39) || '00.00' || chr(39) || ') pICMS,' || ' '||
	'		to_char(a.vl_tributo,' || chr(39) || 'FM9999.0099' || chr(39) || ') vICMS,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
	'		material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
	'	and	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
	'	and	a.cd_tributo = d.cd_tributo' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = 51' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	to_char(0 ,' || chr(39) || 'FM9999.0099' || chr(39) || ') vBC,' || ' '||
	'		to_char(0,' || chr(39) || '00.00' || chr(39) || ') pICMS,' || ' '||
	'		to_char(0,' || chr(39) || 'FM9999.0099' || chr(39) || ') vICMS,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	material_fiscal b,' || ' '||
	'		nota_fiscal_item c' || ' '||
	'	where	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = 51)' || ' '||
	'where ie_tributacao_icms = ' || chr(39) || '51' || chr(39) || '' || ' '||
	'group by ie_tributacao_icms		' 
	where nr_sequencia in (18732,18412);

	Update xml_elemento 
	set ds_sql = 
	'select	0 orig,' || ' '||
	'	max(CST) CST,' || ' '||
	'	' || chr(39) || '0' || chr(39) || ' vICMS,' || ' '||
	'	null motDesICMS	' || ' '||
	'from	(' || ' '||
	'	select	b.ie_tributacao_icms CST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
	'		material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
	'	and	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
	'	and	a.cd_tributo = d.cd_tributo' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '60' || chr(39) || '' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	null CST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	material_fiscal b,' || ' '||
	'		nota_fiscal_item c' || ' '||
	'	where	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '60' || chr(39) || ')' || ' '||
	'where ie_tributacao_icms = ' || chr(39) || '60' || chr(39) || '' || ' '||
	'group by ie_tributacao_icms' 
	where nr_sequencia in (18733,18413);

	Update xml_elemento 
	set ds_sql = 
	'select	0 orig,' || ' '||
	'	' || chr(39) || '70' || chr(39) || ' CST,' || ' '||
	'	0 modBC,' || ' '||
	'	null pRedBC,' || ' '||
	'	0 modBCST,' || ' '||
	'	null pMVAST,' || ' '||
	'	null pRedBCST,	' || ' '||
	'	max(vBC) vBC,' || ' '||
	'	max(pICMS) pICMS,' || ' '||
	'	max(vICMS) vICMS,' || ' '||
	'	max(vBCST) vBCST,' || ' '||
	'	max(pICMSST) pICMSST,' || ' '||
	'	max(vICMSST) vICMSST	' || ' '||
	'from	(' || ' '||
	'	select	' || ' '||
	'		a.vl_base_calculo vBC,' || ' '||
	'		a.tx_tributo      pICMS,' || ' '||
	'		to_char(a.vl_tributo,' || chr(39) || 'FM9999.0099' || chr(39) || ')  vICMS,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'BC' || chr(39) || ') vBCST,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'PIMP' || chr(39) || ') pICMSST,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'VIMP' || chr(39) || ') vICMSST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
	'		material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
	'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
	'	and	a.cd_tributo = d.cd_tributo' || ' '||
	'	and	c.cd_material = b.cd_material' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '70' || chr(39) || '' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	0	vBC,' || ' '||
	'		0	pICMS,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	vICMS,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	vBCST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	pICMSST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	vICMSST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	material_fiscal b,' || ' '||
	'		nota_fiscal_item c' || ' '||
	'	where	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '70' || chr(39) || ')' || ' '||
	'where ie_tributacao_icms = ' || chr(39) || '70' || chr(39) || '' || ' '||
	'group by ie_tributacao_icms		' 
	where nr_sequencia in (18734,18414);

	Update xml_elemento 
	set ds_sql = 
	'select	0 orig,' || ' '||
	'	' || chr(39) || '90' || chr(39) || ' CST,' || ' '||
	'	0 modBC,' || ' '||
	'	null pRedBC,	' || ' '||
	'	0 modBCST,' || ' '||
	'	null pMVAST,' || ' '||
	'	null pRedBCST,	' || ' '||
	'	max(vBC) vBC,' || ' '||
	'	max(pICMS) pICMS,' || ' '||
	'	max(vICMS) vICMS,' || ' '||
	'	max(vBCST) vBCST,' || ' '||
	'	max(pICMSST) pICMSST,' || ' '||
	'	max(vICMSST) vICMSST		' || ' '||
	'from	(' || ' '||
	'	select	a.vl_base_calculo vBC,' || ' '||
	'		a.tx_tributo      pICMS,' || ' '||
	'		to_char(a.vl_tributo,' || chr(39) || 'FM9999.0099' || chr(39) || ')  vICMS,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'BC' || chr(39) || ') vBCST,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'PIMP' || chr(39) || ') pICMSST,' || ' '||
	'		nfe_obter_valores_impostos(a.nr_item_nf,a.nr_sequencia,' || chr(39) || 'ICMSST' || chr(39) || ',' || chr(39) || 'VIMP' || chr(39) || ') vICMSST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	NOTA_FISCAL_ITEM_TRIB a,' || ' '||
	'		material_fiscal b,' || ' '||
	'		nota_fiscal_item c,' || ' '||
	'		tributo d' || ' '||
	'	where	c.nr_item_nf = a.nr_item_nf' || ' '||
	'	and	c.nr_sequencia = a.nr_sequencia ' || ' '||
	'	and	a.cd_tributo = d.cd_tributo' || ' '||
	'	and	c.cd_material = b.cd_material' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '90' || chr(39) || '' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	0	vBC,' || ' '||
	'		0	pICMS,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	vICMS,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	vBCST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	pICMSST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	vICMSST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from	material_fiscal b,' || ' '||
	'		nota_fiscal_item c' || ' '||
	'	where	c.cd_material = b.cd_material' || ' '||
	'	and	c.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	c.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	b.ie_tributacao_icms = ' || chr(39) || '90' || chr(39) || ')' || ' '||
	'where ie_tributacao_icms = ' || chr(39) || '90' || chr(39) || '' || ' '||
	'group by ie_tributacao_icms		' 
	where nr_sequencia in (18735,18415);

	Update xml_elemento 
	set ds_sql = 
	'select	' || chr(39) || '203' || chr(39) || ' CSOSN,' || ' '||
	'	null pMVAST,' || ' '||
	'	null pRedBCST,' || ' '||
	'	max(orig) orig,' || ' '||
	'	max(modBCST) modBCST,' || ' '||
	'	max(vBCST) vBCST,' || ' '||
	'	max(pICMSST) pICMSST,' || ' '||
	'	max(vICMSST) vICMSST' || ' '||
	'from	(' || ' '||
	'	select  f.ie_origem_mercadoria orig,		' || ' '||
	'		decode(ie_lista,0,2,1,1,2,3,2) modBCST,' || ' '||
	'		to_char(nvl(c.vl_base_calculo,0),' || chr(39) || 'FM99990.0099' || chr(39) || ') vBCST,' || ' '||
	'		to_char(nvl(c.tx_tributo,0),' || chr(39) || 'FM99990.0099' || chr(39) || ') pICMSST,' || ' '||
	'		to_char(c.vl_tributo,' || chr(39) || 'FM99990.0099' || chr(39) || ') vICMSST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from 	NOTA_FISCAL_ITEM a,' || ' '||
	'		nota_fiscal_item_trib c,' || ' '||
	'		material b,' || ' '||
	'		tributo d,' || ' '||
	'		material_fiscal f' || ' '||
	'	where	a.cd_material = b.cd_material' || ' '||
	'	and	b.cd_material = f.cd_material' || ' '||
	'	and	c.cd_tributo = d.cd_tributo' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_sequencia = c.nr_sequencia' || ' '||
	'	and	a.nr_item_nf = c.nr_item_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	f.ie_tributacao_icms not in(' || chr(39) || '00' || chr(39) || ',' || chr(39) || '10' || chr(39) || ',' || chr(39) || '20' || chr(39) || ',' || chr(39) || '30' || chr(39) || ',' || chr(39) || '40' || chr(39) || ',' || chr(39) || '41' || chr(39) || ',' || chr(39) || '50' || chr(39) || ',' || chr(39) || '51' || chr(39) || ',' || chr(39) || '60' || chr(39) || ',' || chr(39) || '70' || chr(39) || ',' || chr(39) || '90' || chr(39) || ')' || ' '||
	'	and	d.ie_tipo_tributo = ' || chr(39) || 'ICMS' || chr(39) || '' || ' '||
	'	union all' || ' '||
	'	select	null	orig,' || ' '||
	'		0	modBCST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	vBCST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	pICMSST,' || ' '||
	'		' || chr(39) || '0' || chr(39) || '	vICMSST,' || ' '||
	'		ie_tributacao_icms' || ' '||
	'	from 	nota_fiscal_item a,' || ' '||
	'		material_fiscal f' || ' '||
	'	where	a.cd_material = f.cd_material' || ' '||
	'	and	a.nr_sequencia = :nr_seq_nf' || ' '||
	'	and	a.nr_item_nf  = :nr_item_nf' || ' '||
	'	and	f.ie_tributacao_icms not in(' || chr(39) || '00' || chr(39) || ',' || chr(39) || '10' || chr(39) || ',' || chr(39) || '20' || chr(39) || ',' || chr(39) || '30' || chr(39) || ',' || chr(39) || '40' || chr(39) || ',' || chr(39) || '41' || chr(39) || ',' || chr(39) || '50' || chr(39) || ',' || chr(39) || '51' || chr(39) || ',' || chr(39) || '60' || chr(39) || ',' || chr(39) || '70' || chr(39) || ',' || chr(39) || '90' || chr(39) || '))' || ' '||
	'where ie_tributacao_icms not in(' || chr(39) || '00' || chr(39) || ',' || chr(39) || '10' || chr(39) || ',' || chr(39) || '20' || chr(39) || ',' || chr(39) || '30' || chr(39) || ',' || chr(39) || '40' || chr(39) || ',' || chr(39) || '41' || chr(39) || ',' || chr(39) || '50' || chr(39) || ',' || chr(39) || '51' || chr(39) || ',' || chr(39) || '60' || chr(39) || ',' || chr(39) || '70' || chr(39) || ',' || chr(39) || '90' || chr(39) || ')' || ' '||
	'group by ie_tributacao_icms' 
	where nr_sequencia in (18682,18362);


	commit;

end if;

if	(dt_versao_atual_cliente_w < to_date('19/02/2013','dd/mm/yyyy')) then

	update 	XML_ATRIBUTO 
	set 	ie_criar_nulo = 'N', 
		ie_obrigatorio = 'N',  
		IE_TIPO_ATRIBUTO  = 'VARCHAR2',
		NM_ATRIBUTO  = 'infAdProd',
		IE_REMOVER_ESPACO_BRANCO  = 'S',
		NR_SEQ_ATRIB_ELEM  = ''
	where nr_sequencia = 71582;

	delete  from XML_ATRIBUTO where NR_SEQUENCIA = 71428;

	delete  from XML_ELEMENTO where NR_SEQUENCIA = 18373;

	commit;

end if;

end tasy_ajustar_base_nfe;
/
