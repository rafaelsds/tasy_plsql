create or replace
procedure ptu_gerar_fatura
			(	nr_sequencia_p		Number,
				nm_usuario_p		Varchar2) is 

nr_seq_fatura_w			number(10);
Cursor C01 is
	select	nr_sequencia
	from	ptu_fatura
	where	nr_seq_lote = nr_sequencia_p;

begin

open C01;
loop
fetch C01 into	
	nr_seq_fatura_w;
exit when C01%notfound;
	begin
	ptu_gerar_nota_cobranca(nr_seq_fatura_w, nm_usuario_p);
	ptu_gerar_fatura_cedente(nr_seq_fatura_w, nm_usuario_p);
	/*Robson OS - 291031 ultimo item do anexo (21)
	ptu_gerar_fatura_corpo(nr_seq_fatura_w, nm_usuario_p);*/
	ptu_gerar_fatura_boleto(nr_seq_fatura_w, nm_usuario_p);
	end;
end loop;
close C01;	
commit;

end ptu_gerar_fatura;
/
