create or replace
procedure fanep_libera_hemoderivados( nr_agente_anest_ocor_p	integer,
									 nr_agente_anest_hemo_p	integer,
									 nm_usuario_p		Varchar2) is 
									 
nr_seq_derivado_w	number(10);

begin

if (nr_agente_anest_hemo_p > 0) then
	select	NR_SEQ_DERIVADO
	into	nr_seq_derivado_w
	from	CIRURGIA_AGENTE_ANESTESICO
	where	nr_sequencia = nr_agente_anest_hemo_p;

	if (nr_seq_derivado_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(81942);
	end if;

	update	CIRURGIA_AGENTE_ANESTESICO
	set		dt_liberacao = sysdate
	where	nr_sequencia = nr_agente_anest_hemo_p
	and 	dt_liberacao is null ;
end if;

if (nr_agente_anest_ocor_p > 0) then
	update	CIRURGIA_AGENTE_ANEST_OCOR
	set		dt_liberacao = sysdate
	where	nr_sequencia = nr_agente_anest_ocor_p
	and 	dt_liberacao is null ;
end if;

commit;

end fanep_libera_hemoderivados;
/