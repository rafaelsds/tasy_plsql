create or replace
procedure ptu_gerar_historico_fatura(	nr_seq_fatura_p		Number,
					nm_usuario_p		Varchar2) is 

ds_call_stack_w		varchar2(4000);

begin
ds_call_stack_w := substr(sqlerrm || ' ' || DBMS_UTILITY.FORMAT_CALL_STACK,1,4000);

insert into ptu_fatura_historico
	(nr_sequencia, nr_seq_fatura, dt_historico,
	nm_usuario_historico, ie_tipo_historico, dt_atualizacao,
	nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
	ds_call_stack)
values(	ptu_fatura_historico_seq.nextval, nr_seq_fatura_p, sysdate,
	nm_usuario_p, 1, sysdate,
	nm_usuario_p, sysdate, nm_usuario_p,
	ds_call_stack_w);

insert into ptu_fatura_historico
	(nr_sequencia, nr_seq_fatura, dt_historico,
	nm_usuario_historico, ie_tipo_historico, dt_atualizacao,
	nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
	ds_call_stack)
values(	ptu_fatura_historico_seq.nextval, nr_seq_fatura_p, sysdate,
	nm_usuario_p, 2, sysdate,
	nm_usuario_p, sysdate, nm_usuario_p,
	ds_call_stack_w);
	
insert into ptu_fatura_historico
	(nr_sequencia, nr_seq_fatura, dt_historico,
	nm_usuario_historico, ie_tipo_historico, dt_atualizacao,
	nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
	ds_call_stack)
values(	ptu_fatura_historico_seq.nextval, nr_seq_fatura_p, sysdate,
	nm_usuario_p, 3, sysdate,
	nm_usuario_p, sysdate, nm_usuario_p,
	ds_call_stack_w);
	
insert into ptu_fatura_historico
	(nr_sequencia, nr_seq_fatura, dt_historico,
	nm_usuario_historico, ie_tipo_historico, dt_atualizacao,
	nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
	ds_call_stack)
values(	ptu_fatura_historico_seq.nextval, nr_seq_fatura_p, sysdate,
	nm_usuario_p, 4, sysdate,
	nm_usuario_p, sysdate, nm_usuario_p,
	ds_call_stack_w);
	
insert into ptu_fatura_historico
	(nr_sequencia, nr_seq_fatura, dt_historico,
	nm_usuario_historico, ie_tipo_historico, dt_atualizacao,
	nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
	ds_call_stack)
values(	ptu_fatura_historico_seq.nextval, nr_seq_fatura_p, sysdate,
	nm_usuario_p, 5, sysdate,
	nm_usuario_p, sysdate, nm_usuario_p,
	ds_call_stack_w);	
	
commit;

end ptu_gerar_historico_fatura;
/