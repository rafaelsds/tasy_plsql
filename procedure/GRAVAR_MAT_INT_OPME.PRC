create or replace
procedure gravar_mat_int_opme(	cd_material_p		number,
				qt_unitaria_p		number,
				cd_fornecedor_p		varchar2,
				nr_seq_agenda_p		number,	
				nr_seq_prescricao_p	number,
				cd_motivo_baixa_p		number,
				ie_acao_p		number,
				nm_usuario_p		Varchar2) is 

nr_sequencia_w		number(10);
nr_seq_agenda_w		number(10);
qt_existe_w		varchar2(1);	
nr_seq_opme_w		number(10);					
nr_atendimento_w	number(14);
nr_seq_tipo_baixa_w	number(10);	
qt_material_total_w	number(18,6);
qt_material_atual_W 	number(18,6);
qt_itens_novo_w		number(10);		

begin

select	count(*)
into	qt_existe_w
from	agenda_pac_opme
where	cd_material = cd_material_p
and	nr_seq_agenda = nr_seq_agenda_p;

if	(ie_acao_p = 1) and (nvl(qt_existe_w,0) = 0) then
	begin
	select 	agenda_pac_opme_seq.nextval
	into	nr_sequencia_w
	from 	dual;
	
	insert into agenda_pac_opme(
		cd_cgc, 
		cd_cond_pagto, 
		cd_material, 
		ds_observacao, 
		dt_atualizacao, 
		dt_atualizacao_nrec, 
		dt_exclusao, 
		ie_autorizado, 
		ie_gerar_autor, 
		ie_integracao, 
		ie_integracao_util, 
		ie_origem_inf, 
		ie_padrao, 
		nm_usuario, 
		nm_usuario_exclusao, 
		nm_usuario_nrec, 
		nr_seq_agenda, 
		nr_seq_apres, 
		nr_seq_motivo_exclusao, 
		nr_seq_proc_interno, 
		nr_sequencia, 
		qt_material, 
		vl_desconto, 
		vl_unitario_item,
		nr_seq_prescricao)
	values	(cd_fornecedor_p,
		'',
		cd_material_p,
		'',
		sysdate,
		sysdate,
		'',
		'P',
		'N',
		'S',
		'AC',
		'I',
		'S',
		nm_usuario_p,
		'',
		nm_usuario_P,
		nr_seq_agenda_p,
		500,
		null,
		null,
		nr_sequencia_w,
		qt_unitaria_p,
		'',
		'',
		nr_seq_prescricao_p);
	end;
	
	select 	nr_atendimento
	into	nr_atendimento_w
	from 	cirurgia
	where 	nr_seq_agenda = nr_seq_agenda_p;

	select	nr_sequencia
	into	nr_seq_tipo_baixa_w
	from	tipo_baixa_prescricao
	where	cd_tipo_baixa = cd_motivo_baixa_p
	and 	ie_prescricao_devolucao = 'P';
	
	update 	material_atend_paciente
	set 	nr_seq_agenda_pac_opme = nr_sequencia_w,
		nr_seq_tipo_baixa = nr_seq_tipo_baixa_w
	where	nr_atendimento = nr_atendimento_w
	and 	cd_material = cd_material_p;
	

elsif	 (nvl(qt_existe_w,0) > 0) then 

	select	qt_material,
		nr_sequencia
	into	qt_material_atual_W,
		nr_sequencia_w
	from	agenda_pac_opme
	where 	cd_material = cd_material_p
	and	nr_seq_agenda = nr_seq_agenda_p;
	
	qt_material_total_w := qt_material_atual_W + qt_unitaria_p;
				
	update	agenda_pac_opme
	set	qt_material = qt_material_total_w
	where 	cd_material = cd_material_p
	and	nr_seq_agenda = nr_seq_agenda_p;
	
	select 	nr_atendimento
	into	nr_atendimento_w
	from 	cirurgia
	where 	nr_seq_agenda = nr_seq_agenda_p;

	select	nr_sequencia
	into	nr_seq_tipo_baixa_w
	from	tipo_baixa_prescricao
	where	cd_tipo_baixa = cd_motivo_baixa_p
	and 	ie_prescricao_devolucao = 'P';
	
	update 	material_atend_paciente
	set 	nr_seq_agenda_pac_opme = nr_sequencia_w,
		nr_seq_tipo_baixa = nr_seq_tipo_baixa_w
	where	nr_atendimento = nr_atendimento_w
	and 	cd_material = cd_material_p;
	
	
end if;

commit;

end gravar_mat_int_opme;
/