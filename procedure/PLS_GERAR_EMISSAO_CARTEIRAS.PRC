create or replace
procedure pls_gerar_emissao_carteiras
		(	nr_seq_lote_p	number,	
			nm_usuario_p	varchar2	) is 

begin

update	pls_lote_carteira
set	dt_emissao_arquivo = sysdate,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_sequencia = nr_seq_lote_p;

commit;

end pls_gerar_emissao_carteiras;
/