create or replace procedure lote_ent_ver_se_baixa_aut_BA(
					nr_seq_ficha_p		number,
					nr_seq_resultado_p	number,
					nm_usuario_p		Varchar2) is 
totalItens_w		number(10) := 0;
totalItensNormal_w	number(10) := 0;
nrSeqReconvocado_w	number(10) := 0;
pessoaClassif_w		number(10) := 0;
nrSeqMotPadrao_w	number(10) := 0;
nrSeqFichaAnterior_w	number(10) := 0;
cdPessoaFisica_w	varchar2(10);

begin   
    if 	(nr_seq_ficha_p is not null) and (nr_seq_resultado_p is not null) then
        select 	nr_seq_ficha_ant,
                cd_pessoa_fisica
        into	nrSeqFichaAnterior_w,
                cdPessoaFisica_w
        from	lote_ent_sec_ficha
        where	nr_sequencia = nr_seq_ficha_p;
        
        if (cdPessoaFisica_w is not null) then
            begin        
                select count(1)
                into pessoaClassif_w
                from pessoa_classif
                where cd_pessoa_fisica = cdPessoaFisica_w;
            exception
                when others then
                pessoaClassif_w := 0;
            end;
        end if;
        
        begin
            select nr_sequencia
            into nrSeqReconvocado_w
            from lote_ent_reconvocado 
            where nr_seq_ficha_lote = nrSeqFichaAnterior_w;
        exception
            when others then
                nrSeqReconvocado_w := 0;
        end;            

        if (nrSeqReconvocado_w > 0) and (pessoaClassif_w = 0) then 
            begin
                select count(eri.nr_seq_exame)
                into totalItens_w
                from prescr_procedimento pp
                inner join exame_lab_resultado er on er.nr_prescricao = pp.nr_prescricao
                inner join exame_lab_result_item eri on eri.nr_seq_resultado = er.nr_seq_resultado 
                                                        and eri.nr_seq_prescr = pp.nr_sequencia
                where eri.nr_seq_resultado = nr_seq_resultado_p
                and pp.dt_baixa is null
                and eri.nr_seq_material is not null
                and ((eri.ds_resultado is not null) 
                    or (eri.pr_resultado is not null) 
                    or (eri.qt_resultado is not null));
            exception
                when others then
                    totalItens_w := 0;  
            end;
            
            if (totalItens_w > 0) then
                begin    
                    select count(eri.nr_seq_exame)
                    into totalItensNormal_w
                    from prescr_procedimento pp
                    inner join exame_lab_resultado er on er.nr_prescricao = pp.nr_prescricao
                    inner join exame_lab_result_item eri on eri.nr_seq_resultado = er.nr_seq_resultado 
                                                            and eri.nr_seq_prescr = pp.nr_sequencia
                    where eri.nr_seq_resultado = nr_seq_resultado_p
                    and pp.dt_baixa is null
                    and	eri.nr_seq_material is not null
                    and ((eri.ds_resultado is not null) 
                        or (eri.pr_resultado is not null) 
                        or (eri.qt_resultado is not null)) 
                    and (nvl(eri.ie_acao_criterio_lote,'S') = 'S')
                    and (exists(select ebat.nr_seq_exame 
                                from lote_ent_exame_buscaat ebat 
                                where ebat.nr_seq_exame = eri.nr_seq_exame 
                                and (nvl(ebat.ie_situacao,'A') = 'A')
                                and (nvl(ebat.ie_baixa_res_normal,'N') = 'S'))            
                        OR exists (select  ebap.nr_seq_exame
                                    from lote_ent_exame_buscap ebap 
                                    where ebap.nr_seq_exame = eri.nr_seq_exame 
                                    and (nvl(ebap.ie_situacao,'A') = 'A')
                                    and (nvl(ebap.ie_baixa_res_normal,'N') = 'S')));  
                exception
                    when others then
                        totalItensNormal_w := 0;  
                end;       
                
                if (totalItens_w = totalItensNormal_w) then 
                    begin
                        select nr_sequencia
                        into nrSeqMotPadrao_w
                        from lote_ent_baixa_mot
                        where (nvl(ie_motivo_padrao,'N') = 'S')
                        and ie_situacao = 'A';
                    exception
                    when others then
                        nrSeqMotPadrao_w := null;  
                    end; 
                
                    lote_ent_baixa_reg_ba(nrSeqReconvocado_w, nrSeqMotPadrao_w);
                end if;
            end if;
        end if;
    end if;
    
    commit;
    
end lote_ent_ver_se_baixa_aut_BA;
/
