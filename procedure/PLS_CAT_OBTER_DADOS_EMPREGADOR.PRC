create or replace
procedure pls_cat_obter_dados_empregador
			(	nr_seq_estipulante_p	in	Number,
				nr_seq_contrato_p	in	Number,
				ie_tipo_documento_p	out	Number,
				nm_empregador_p		out	Varchar2,
				cd_documento_p		out	Varchar2, 
				ds_endereco_p		out	Varchar2,
				ds_bairro_p		out	Varchar2,
				cd_cep_p		out	Varchar2,
				nm_municipio_p		out	Varchar2,
				sg_estado_p		out	Varchar2,
				nr_telefone_p		out	Varchar2,
				ds_complemento_p	out	Varchar2) is

nr_seq_contrato_w			Number(10);
nr_seq_subestipulante_w		Number(10);
cd_cgc_w			Varchar2(14);
cd_pessoa_fisica_w		Varchar2(10);
ie_tipo_documento_w		Number(2)	:= 0;
cd_documento_w			Varchar2(14)	:= '';
nm_empregador_w			pessoa_juridica.ds_razao_social%type;
ds_endereco_w			Varchar2(100);
ds_bairro_w			Varchar2(40);
cd_cep_w			Varchar2(15);
nm_municipio_w			Varchar2(100);
sg_estado_w			pessoa_juridica.sg_estado%type;
nr_telefone_w			Varchar2(15);
ds_complemento_w			Varchar2(40);

begin

/* Obter o estipulante */
if(nr_seq_contrato_p is not null) then
	nr_seq_contrato_w := nr_seq_contrato_p;
	nr_seq_subestipulante_w := 0;
elsif(nr_seq_estipulante_p is not null) then
	select	nr_seq_contrato,
		nvl(nr_seq_subestipulante,0)
	into	nr_seq_contrato_w,
		nr_seq_subestipulante_w
	from	pls_estipulante_web
	where	nr_sequencia	= nr_seq_estipulante_p;
end if;

if	(nr_seq_subestipulante_w > 0) then
	begin
	select	nvl(cd_cgc,''),
		nvl(cd_pessoa_fisica, '')
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_sub_estipulante
	where	nr_sequencia	= nr_seq_subestipulante_w;
	exception
		when others then
		cd_cgc_w		:= '';
		cd_pessoa_fisica_w	:= '';
	end;
else
	begin
	select	nvl(cd_cgc_estipulante,''),
		nvl(cd_pf_estipulante, '')
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
	exception
		when others then
		cd_cgc_w		:= '';
		cd_pessoa_fisica_w	:= '';
	end;
end if;

if	(cd_cgc_w is not null) then
	ie_tipo_documento_w	:= 1;
	cd_documento_w		:= cd_cgc_w;

	select	ds_razao_social,
		ds_endereco,
		ds_bairro,
		cd_cep,
		ds_municipio,
		sg_estado,
		nr_telefone,
		ds_complemento
	into	nm_empregador_w,
		ds_endereco_w,
		ds_bairro_w,
		cd_cep_w,
		nm_municipio_w,
		sg_estado_w,
		nr_telefone_w,
		ds_complemento_w
	from	pessoa_juridica
	where	cd_cgc	= cd_cgc_w;
elsif	(cd_pessoa_fisica_w is not null) then
	ie_tipo_documento_w	:= 3;
	
	select	a.nm_pessoa_fisica,
		a.nr_cpf,
		b.ds_endereco,
		substr(b.ds_bairro,1,40),
		b.cd_cep,
		b.ds_municipio,
		b.sg_estado,
		b.nr_telefone,
		b.ds_complemento
	into	nm_empregador_w,
		cd_documento_w,
		ds_endereco_w,
		ds_bairro_w,
		cd_cep_w,
		nm_municipio_w,
		sg_estado_w,
		nr_telefone_w,
		ds_complemento_w
	from	compl_pessoa_fisica	b,
		pessoa_fisica		a
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	b.ie_tipo_complemento	= 1
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
end if;

ie_tipo_documento_p	:= ie_tipo_documento_w;
nm_empregador_p		:= nm_empregador_w;
cd_documento_p		:= cd_documento_w;
ds_endereco_p		:= ds_endereco_w;
ds_complemento_p	:= ds_complemento_w;
ds_bairro_p		:= ds_bairro_w;
cd_cep_p		:= cd_cep_w;
nm_municipio_p		:= nm_municipio_w;
sg_estado_p		:= sg_estado_w;
nr_telefone_p		:= nr_telefone_w;

end pls_cat_obter_dados_empregador;
/
