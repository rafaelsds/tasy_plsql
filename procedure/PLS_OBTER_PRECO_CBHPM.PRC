CREATE OR REPLACE
PROCEDURE pls_obter_preco_cbhpm(
				cd_estabelecimento_p		number,
				dt_referencia_p			date,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				dt_vigencia_edicao_p		date,
				nr_seq_prestador_p		number,
				ie_video_p			varchar2,
				vl_medico_p		out	number,
				cd_porte_p		out	varchar2,
				tx_porte_p		out	number,
				qt_uco_p		out	number,
				nr_porte_anest_p	out	number,
				nr_auxiliar_p		out	number,
				qt_filme_p		out	number,
				qt_incidencia_p		out	number,
				ie_unid_ra_p		out	varchar2,
				vl_porte_p		out	number,
				dt_vigencia_porte_p	out	date,
				dt_vigencia_preco_p	out	date,
				vl_porte_anestesista_p	out	number,
				cd_categoria_p			varchar2,
				ie_credenciado_p		varchar2,
				nr_seq_congenere_p		Number,
				nr_seq_plano_p			pls_segurado.nr_seq_plano%type) is

vl_custo_operacional_w	number(15,2);
vl_medico_w		number(15,2);
vl_anestesista_w	number(15,2);
cd_porte_w		varchar2(10);
tx_porte_w		number(15,4);
tx_porte_orig_w		number(15,4);
qt_uco_w		number(15,4);
nr_porte_anest_w	number(3);
nr_auxiliar_w		number(3);
qt_filme_w		number(15,4);
qt_incidencia_w		number(15,4);
ie_unid_ra_w		varchar2(1);
vl_porte_w		number(15,2);
dt_vigencia_porte_w	date;
dt_vigencia_preco_w	date;
cd_porte_anestesista_w	varchar2(8);
vl_porte_anestesista_w	number(15,2);
cd_grupo_w		Number(15)	:= 0;
cd_especialidade_w	Number(15)	:= 0;
cd_area_w		Number(15)	:= 0;
IE_DATA_VIG_CBHPM_w	varchar2(01);
tx_porte_anest_w	number(15,4);
IE_DESPREZA_CASA_CBHPM_w 	varchar2(1);
vl_medico_arred_w	number(15,4);
vl_anest_arred_w	number(15,4);
VL_PORTE_NEGOCIADO_w	number(15,2):= 0;
ie_preco_cbhpm_data_w	Varchar2(1)	:= 'N';
ie_preco_cbhpm_data_ww	Varchar2(1)	:= 'N';
qt_ajuste_uco_cbhpm_w	number(10,0);
qt_uco_ajuste_w		number(15,4);
nr_porte_anest_cobranca_w	number(3,0);
ie_grupo_produto_w	varchar2(1);
ie_grupo_prestador_w	varchar2(1);

Cursor C03 is
	select	nvl(a.tx_ajuste,1) tx_ajuste,
		a.vl_porte_negociado,
		a.nr_seq_grupo_produto,
		a.nr_seq_grupo_prest,
		a.nr_seq_prestador
	from	pls_ajuste_porte_cbhpm	a
	where	((a.nr_seq_prestador is null) or (a.nr_seq_prestador = nr_seq_prestador_p))
	and	a.cd_porte		= substr(cd_porte_w,1,8)
	and	(((cd_procedimento 	= cd_procedimento_p) and (ie_origem_proced 	= ie_origem_proced_p)) 	or cd_procedimento 	is null)
	and	((cd_grupo_proc 	= cd_grupo_w) 		or (cd_grupo_proc 	is null))
	and	((cd_especialidade 	= cd_especialidade_w) 	or (cd_especialidade 	is null))
	and	((cd_area_procedimento 	= cd_area_w )		or (cd_area_procedimento	is null))
	and	((nr_seq_plano 		= nr_seq_plano_p)	or (nr_seq_plano_p	is null))
	and 	(nvl(a.ie_tipo_ajuste,'M') = 'M')
	and	a.ie_situacao	= 'A'
	and	a.dt_vigencia	=
			(select	max(nvl(x.dt_vigencia,sysdate - 3650))
			from	pls_ajuste_porte_cbhpm	x
			where	x.cd_porte 	= substr(cd_porte_w,1,8)
			and	x.dt_vigencia	<= dt_referencia_p
			and	x.ie_situacao	= 'A'
			and	((x.nr_seq_prestador is null) or (x.nr_seq_prestador = nr_seq_prestador_p)))
	order by
		nvl(cd_procedimento,0),
		nvl(ie_origem_proced,0),
		nvl(cd_area_procedimento,0),
		nvl(cd_especialidade,0),
		nvl(cd_grupo_proc,0),
		nvl(nr_seq_prestador,0),
		nvl(nr_seq_plano,0);

Cursor C04 is
	select	nvl(a.tx_ajuste,1) tx_ajuste,
		a.vl_porte_negociado,
		a.nr_seq_grupo_produto,
		a.nr_seq_grupo_prest,
		a.nr_seq_prestador
	from	pls_ajuste_porte_cbhpm	a
	where	((a.nr_seq_prestador is null) or (a.nr_seq_prestador = nr_seq_prestador_p))
	and	a.cd_porte		= substr(cd_porte_anestesista_w,1,8)
	and	(((cd_procedimento 	= cd_procedimento_p) and (ie_origem_proced 	= ie_origem_proced_p)) 	or cd_procedimento 	is null)
	and	((cd_grupo_proc 	= cd_grupo_w) 		or (cd_grupo_proc 	is null))
	and	((cd_especialidade 	= cd_especialidade_w) 	or (cd_especialidade 	is null))
	and	((cd_area_procedimento 	= cd_area_w) 		or (cd_area_procedimento	is null))
	and	((nr_seq_plano 		= nr_seq_plano_p)	or (nr_seq_plano_p	is null))
	and 	(nvl(a.ie_tipo_ajuste,'M') = 'A')
	and	a.ie_situacao	= 'A'
	and	a.dt_vigencia	=
			(select	max(nvl(x.dt_vigencia,sysdate - 3650))
			from	pls_ajuste_porte_cbhpm	x
			where	x.cd_porte 	= substr(cd_porte_anestesista_w,1,8)
			and	x.dt_vigencia	<= dt_referencia_p
			and	x.ie_situacao	= 'A'
			and	((x.nr_seq_prestador is null) or (x.nr_seq_prestador = nr_seq_prestador_p)))
	order by
		nvl(cd_procedimento,0),
		nvl(ie_origem_proced,0),
		nvl(cd_area_procedimento,0),
		nvl(cd_especialidade,0),
		nvl(cd_grupo_proc,0),
		nvl(nr_seq_prestador,0),
		nvl(nr_seq_plano,0);
				
Cursor C06 is
	select	nr_porte_anest_cobranca
	from	regra_porte_anest_cbhpm
	where	nr_porte_anest = nr_porte_anest_w
	and	nvl(cd_procedimento, nvl(cd_procedimento_p,0))	= nvl(cd_procedimento_p,0)
	and	((cd_procedimento is null) or (nvl(ie_origem_proced, nvl(ie_origem_proced_p,0)) = nvl(ie_origem_proced_p,0)))
	and	nvl(cd_area_procedimento, cd_area_w) = cd_area_w
	and	nvl(cd_especialidade, cd_especialidade_w) = cd_especialidade_w
	and	nvl(cd_grupo_proc, cd_grupo_w) = cd_grupo_w
	order by nr_porte_anest;		
			

BEGIN

select	nvl(max(IE_DATA_VIG_CBHPM), 'N'),
	nvl(max(IE_DESPREZA_CASA_CBHPM), 'N'),
	nvl(max(IE_PRECO_CBHPM_DATA), 'N')
into	IE_DATA_VIG_CBHPM_w,
	IE_DESPREZA_CASA_CBHPM_w,
	ie_preco_cbhpm_data_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

/* Obter Estrutura do procedimento */
select	nvl(max(cd_grupo_proc),0),
	nvl(max(cd_especialidade),0),
	nvl(max(cd_area_procedimento),0)
into	cd_grupo_w,
	cd_especialidade_w,
	cd_area_w
from	Estrutura_Procedimento_V
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;

/* Felipe - 16/09/2011 - OS 301610 - Se o par�metro estiver como Ambos ou Prestador e o c�lculo for para o Tasy/Hospital ou 
	como Ambos ou Operadora e o c�lculo for para Tasy/OPS */
if	(((ie_preco_cbhpm_data_w in ('A','P'))) or
	((ie_preco_cbhpm_data_w in ('A','O')) and
	((nr_seq_prestador_p is not null) or
	( nr_seq_congenere_p is not null)))) then
	ie_preco_cbhpm_data_ww	:= 'S';
end if;

begin

/* Felipe - 16/09/2011 - OS 301610 */
if	(ie_preco_cbhpm_data_ww = 'S') then
	select	a.dt_vigencia,
		nvl(a.cd_porte,'X'),
	 	nvl(a.tx_porte,1),
		nvl(a.qt_uco,0),
		nvl(a.nr_porte_anest,0),
		nvl(a.nr_auxiliar,0),
		nvl(a.qt_filme,0),
		nvl(a.qt_incidencia,0),
		a.ie_unid_ra
	into	dt_vigencia_preco_w,
		cd_porte_w,
		tx_porte_w,
		qt_uco_w,
		nr_porte_anest_w,
		nr_auxiliar_w,
		qt_filme_w,
		qt_incidencia_w,
		ie_unid_ra_w
	from	cbhpm_preco a
	where	a.cd_procedimento	= cd_procedimento_p
	and	a.ie_origem_proced	= ie_origem_proced_p
	and	a.dt_vigencia		=
		(select max(nvl(x.dt_vigencia,sysdate - 3650))
			from 	cbhpm_preco x
			where x.cd_procedimento		= cd_procedimento_p
			and	x.ie_origem_proced	= ie_origem_proced_p
			and	x.dt_vigencia		<= dt_referencia_p);
elsif	(IE_DATA_VIG_CBHPM_w	= 'N') then
	select	a.dt_vigencia,
		nvl(a.cd_porte,'X'),
	 	nvl(a.tx_porte,1),
		 nvl(a.qt_uco,0),
		 nvl(a.nr_porte_anest,0),
		 nvl(a.nr_auxiliar,0),
		 nvl(a.qt_filme,0),
		 nvl(a.qt_incidencia,0),
		 a.ie_unid_ra
	into	 dt_vigencia_preco_w,
		 cd_porte_w,
		 tx_porte_w,
		 qt_uco_w,
		 nr_porte_anest_w,
		 nr_auxiliar_w,
		 qt_filme_w,
		 qt_incidencia_w,
		 ie_unid_ra_w
	from	 cbhpm_preco a
	where	 a.cd_procedimento	= cd_procedimento_p
	and	 a.ie_origem_proced	= ie_origem_proced_p
	and	 a.dt_vigencia		=
		(select max(nvl(x.dt_vigencia,sysdate - 3650))
			from 	cbhpm_preco x
			where x.cd_procedimento		= cd_procedimento_p
			and	x.ie_origem_proced	= ie_origem_proced_p
			and	x.dt_vigencia		<= dt_referencia_p
			and	x.dt_vigencia		<= nvl(dt_vigencia_edicao_p, dt_referencia_p));
else
	select	a.dt_vigencia,
		nvl(a.cd_porte,'X'),
	 	nvl(a.tx_porte,1),
		 nvl(a.qt_uco,0),
		 nvl(a.nr_porte_anest,0),
		 nvl(a.nr_auxiliar,0),
		 nvl(a.qt_filme,0),
		 nvl(a.qt_incidencia,0),
		 a.ie_unid_ra
	into	 dt_vigencia_preco_w,
		 cd_porte_w,
		 tx_porte_w,
		 qt_uco_w,
		 nr_porte_anest_w,
		 nr_auxiliar_w,
		 qt_filme_w,
		 qt_incidencia_w,
		 ie_unid_ra_w
	from	 cbhpm_preco a
	where	 a.cd_procedimento	= cd_procedimento_p
	and	 a.ie_origem_proced	= ie_origem_proced_p
	and	 a.dt_vigencia		=
		(select min(nvl(x.dt_vigencia,sysdate - 3650))
			from 	cbhpm_preco x
			where x.cd_procedimento		= cd_procedimento_p
			and	x.ie_origem_proced	= ie_origem_proced_p
			and	x.dt_vigencia		<= dt_referencia_p
			and	x.dt_vigencia		<= nvl(dt_vigencia_edicao_p, dt_referencia_p)
			and	x.dt_vigencia		= dt_vigencia_edicao_p);
end if;
exception
     	when others then
	begin
	dt_vigencia_preco_w	:= null;
	cd_porte_w		:= 'X';
	tx_porte_w		:= 1;
	qt_uco_w		:= 0;
	nr_porte_anest_w	:= 0;
	nr_auxiliar_w		:= 0;
	qt_filme_w		:= 0;
	qt_incidencia_w		:= null;
	end;
end;		

if	(tx_porte_w	= 0) then
	tx_porte_w	:= 1;
end if;

tx_porte_orig_w		:= tx_porte_w;

if	(cd_porte_w	<> 'X') then
	begin
		
	for r_c03_w in C03() loop
		begin
		ie_grupo_produto_w	:= 'S';
		ie_grupo_prestador_w	:= 'S';
		
		if	(nvl(r_c03_w.nr_seq_grupo_produto,0) > 0) then 
			ie_grupo_produto_w	:= pls_se_grupo_preco_produto(r_c03_w.nr_seq_grupo_produto, nr_seq_plano_p);                                                                                                             
		end if;
		
		if	(r_c03_w.nr_seq_grupo_prest is not null) then
		
			ie_grupo_prestador_w := pls_obter_se_prestador_grupo(r_c03_w.nr_seq_grupo_prest, nr_seq_prestador_p);
		end if;
		
		if	(ie_grupo_produto_w	= 'S') and
			(ie_grupo_prestador_w	= 'S') then
			if	(r_c03_w.tx_ajuste <> 1) then
				tx_porte_w	:= r_c03_w.tx_ajuste;
			else	
				tx_porte_w	:= tx_porte_orig_w;
			end if;
			vl_porte_negociado_w	:= r_c03_w.vl_porte_negociado;
		end if;
		end;
	end loop;

	/* Felipe e Fabricio - OS 194603 - Colocamos a �ltima linha do comando abaixo*/
	if	(IE_DATA_VIG_CBHPM_w	= 'N') then
		select	a.vl_porte,
			a.dt_vigencia
		into	vl_porte_w,
			dt_vigencia_porte_w
		from	cbhpm_porte	a
		where	a.cd_porte	= substr(cd_porte_w,1,8)
		and	a.dt_vigencia	=
			(select	max(nvl(x.dt_vigencia,sysdate - 3650))
				from cbhpm_porte x
				where x.cd_porte = substr(cd_porte_w,1,8)
				and	x.dt_vigencia <= dt_referencia_p
				and	x.dt_vigencia <= nvl(dt_vigencia_edicao_p, dt_referencia_p));
	else
		select	a.vl_porte,
			a.dt_vigencia
		into	vl_porte_w,
			dt_vigencia_porte_w
		from	cbhpm_porte	a
		where	a.cd_porte	= substr(cd_porte_w,1,8)
		and	a.dt_vigencia	=
			(select	min(nvl(x.dt_vigencia,sysdate - 3650))
				from cbhpm_porte x
				where x.cd_porte = substr(cd_porte_w,1,8)
				and	x.dt_vigencia 	<= dt_referencia_p
				and	x.dt_vigencia	= dt_vigencia_edicao_p);
	end if;
	exception
     		when others then
		begin
		vl_porte_w		:= 0;
		dt_vigencia_porte_w	:= null;
		end;
	end;
end if;

/*Define valor do m�dico */
if	(IE_DESPREZA_CASA_CBHPM_w = 'S') then	

	/*Fabr�cio OS 189016 e 187019*/
	if	(nvl(VL_PORTE_NEGOCIADO_w,0) <> 0) then
		vl_medico_arred_w:= VL_PORTE_NEGOCIADO_w * tx_porte_orig_w;
		arredondamento(vl_medico_arred_w, 2, 'D');
		vl_medico_w:= vl_medico_arred_w;
	else
		vl_medico_arred_w:= vl_porte_w * tx_porte_w;
		arredondamento(vl_medico_arred_w, 2, 'D');
		vl_medico_w:= vl_medico_arred_w;
	end if;
	
else
	/*Fabr�cio OS 189016 e 187019*/
	if	(nvl(VL_PORTE_NEGOCIADO_w,0) <> 0) then
		vl_medico_w	:= (VL_PORTE_NEGOCIADO_w * tx_porte_orig_w);
	else
		vl_medico_w	:= (vl_porte_w * tx_porte_w);
	end if;		
	
end if;

nr_porte_anest_cobranca_w:= null;
open C06;
loop
fetch C06 into	
	nr_porte_anest_cobranca_w;
exit when C06%notfound;
	begin
	nr_porte_anest_cobranca_w:= nr_porte_anest_cobranca_w;
	end;
end loop;
close C06;

if	(nr_porte_anest_cobranca_w is not null) then
	nr_porte_anest_w:= nr_porte_anest_cobranca_w;
end if;

/*	Define porte do anestesista */
if	(nr_porte_anest_w	= 1) then
	cd_porte_anestesista_w	:= '3A';
elsif (nr_porte_anest_w	= 2) then
	cd_porte_anestesista_w	:= '3C';
elsif (nr_porte_anest_w	= 3) then
	cd_porte_anestesista_w	:= '4C';
elsif (nr_porte_anest_w	= 4) then
	cd_porte_anestesista_w	:= '6B';
elsif (nr_porte_anest_w	= 5) then
	cd_porte_anestesista_w	:= '7C';
elsif (nr_porte_anest_w	= 6) then
	cd_porte_anestesista_w	:= '9B';
elsif (nr_porte_anest_w	= 7) then
	cd_porte_anestesista_w	:= '10C';
elsif (nr_porte_anest_w	= 8) then
	cd_porte_anestesista_w	:= '12A';
else
	cd_porte_anestesista_w	:= 'X';
end if;

vl_porte_anestesista_w	:= 0;
if	(cd_porte_anestesista_w <> 'X') then
	begin
	
	if	(IE_DATA_VIG_CBHPM_w	= 'N') then
		select 	nvl(max(a.vl_porte),0)
		into	vl_porte_anestesista_w
		from	cbhpm_porte a
		where	a.cd_porte		= cd_porte_anestesista_w
		and	a.dt_vigencia	=
		(select max(nvl(x.dt_vigencia,sysdate - 3650))
			from cbhpm_porte x
			where x.cd_porte = cd_porte_anestesista_w
			and	x.dt_vigencia <= dt_referencia_p
			and	x.dt_vigencia <= nvl(dt_vigencia_edicao_p, dt_referencia_p));
	else
		select 	nvl(max(a.vl_porte),0)
		into	vl_porte_anestesista_w
		from	cbhpm_porte a
		where	a.cd_porte	= cd_porte_anestesista_w
		and	a.dt_vigencia	=
		(select	min(nvl(x.dt_vigencia,sysdate - 3650))
				from cbhpm_porte x
				where x.cd_porte = cd_porte_anestesista_w
				and	x.dt_vigencia 	<= dt_referencia_p
				and	x.dt_vigencia	= dt_vigencia_edicao_p);
	end if;
	
	tx_porte_anest_w:= 1;
	

	for r_c04_w in C04() loop
		begin
		ie_grupo_produto_w	:= 'S';
		ie_grupo_prestador_w	:= 'S';
		
		if	(nvl(r_c04_w.nr_seq_grupo_produto,0) > 0) then 
			ie_grupo_produto_w	:= pls_se_grupo_preco_produto(r_c04_w.nr_seq_grupo_produto, nr_seq_plano_p);                                                                                                             
		end if;
		
		if	(r_c04_w.nr_seq_grupo_prest is not null) then
		
			ie_grupo_prestador_w := pls_obter_se_prestador_grupo(r_c04_w.nr_seq_grupo_prest, nr_seq_prestador_p);
		end if;
		
		if	(ie_grupo_produto_w	= 'S') and
			(ie_grupo_prestador_w	= 'S') then
			
			tx_porte_anest_w	:= r_c04_w.tx_ajuste;

			vl_porte_negociado_w	:= r_c04_w.vl_porte_negociado;
		end if;
		end;
	end loop;
	

	/*	Define valor do Anestesista */
	if	(IE_DESPREZA_CASA_CBHPM_w = 'S') then	

		/*Fabr�cio OS 189016 e 187019*/
		if	(nvl(VL_PORTE_NEGOCIADO_w,0) <> 0) then
			vl_anest_arred_w:= VL_PORTE_NEGOCIADO_w * 1;
			arredondamento(vl_anest_arred_w, 2, 'D');
			vl_porte_anestesista_w:= vl_anest_arred_w;
		else
			vl_anest_arred_w:= vl_porte_anestesista_w * tx_porte_anest_w;
			arredondamento(vl_anest_arred_w, 2, 'D');
			vl_porte_anestesista_w:= vl_anest_arred_w;
		end if;
	
	else

		/*Fabr�cio OS 189016 e 187019*/
		if	(nvl(VL_PORTE_NEGOCIADO_w,0) <> 0) then
			vl_porte_anestesista_w	:= (VL_PORTE_NEGOCIADO_w * 1);
		else
			vl_porte_anestesista_w	:= (vl_porte_anestesista_w * tx_porte_anest_w);
		end if;
		
	end if;

		
	end;
end if;

/*	Define valor custo operacional */
vl_custo_operacional_w	:= 0;

/*	Retorno dos valores */

vl_medico_p		:= vl_medico_w;
cd_porte_p		:= cd_porte_w;
tx_porte_p		:= tx_porte_w;
qt_uco_p		:= qt_uco_w;
nr_porte_anest_p	:= nr_porte_anest_w;
nr_auxiliar_p		:= nr_auxiliar_w;
qt_filme_p		:= qt_filme_w;
qt_incidencia_p		:= qt_incidencia_w;
ie_unid_ra_p		:= ie_unid_ra_w;
vl_porte_p		:= vl_porte_w;
dt_vigencia_porte_p	:= dt_vigencia_porte_w;
dt_vigencia_preco_p	:= dt_vigencia_preco_w;
vl_porte_anestesista_p	:= vl_porte_anestesista_w;

END pls_obter_preco_cbhpm;
/
