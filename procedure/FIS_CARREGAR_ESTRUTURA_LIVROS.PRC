create or replace
procedure fis_carregar_estrutura_livros (ie_lalur_lacs_p varchar2, ie_tipo_estrutura_p varchar2, nm_usuario_p varchar2) is 

nr_sequencia_w		number(10);
ds_estrutura_w		varchar2(255);
nr_seq_ordem_w		number(10);
ie_lalur_lacs_w		varchar2(2);
ie_tipo_estrutura_w	varchar2(2);
ie_forma_apuracao_w	varchar2(2);
ie_gerar_anual_w	varchar2(2);

cursor c01 is
select	nr_sequencia,
	ds_estrutura,
	nr_seq_ordem,
	ie_lalur_lacs,
	ie_tipo_estrutura,
	ie_forma_apuracao,
	decode(ie_tipo_estrutura, 'DE', decode(ie_forma_apuracao, 'R', 'S', 'N'), 'N')
from	fis_livros_estr_padrao p
where	not exists (select 1
		from fis_estrutura_livros
		where nr_seq_estrutura = p.nr_sequencia
		and cd_empresa = obter_empresa_estab(wheb_usuario_pck.get_cd_estabelecimento)
		and rownum = 1)
and	ie_lalur_lacs = ie_lalur_lacs_p
and	ie_tipo_estrutura = ie_tipo_estrutura_p 
and	ie_situacao = 'A'
order by nr_seq_ordem;

begin

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	ds_estrutura_w,
	nr_seq_ordem_w,
	ie_lalur_lacs_w,
	ie_tipo_estrutura_w,
	ie_forma_apuracao_w,
	ie_gerar_anual_w;
exit when c01%notfound;
	begin		
	insert into fis_estrutura_livros(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_conta_contabil,
		vl_fixo,
		ie_gerar_estrutura,
		nr_seq_estrutura,
		ds_estrutura,
		nr_seq_ordem,
		ie_lalur_lacs,
		ie_tipo_estrutura,
		ie_forma_apuracao,
		ie_gerar_anual,
		cd_empresa)
	values(
		fis_estrutura_livros_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		null,
		null,
		'S',
		nr_sequencia_w,
		ds_estrutura_w,
		nr_seq_ordem_w,
		ie_lalur_lacs_w,
		ie_tipo_estrutura_w,
		ie_forma_apuracao_w,
		ie_gerar_anual_w,
		obter_empresa_estab(wheb_usuario_pck.get_cd_estabelecimento));	
	end;
end loop;
close c01;

commit;

end fis_carregar_estrutura_livros;
/	
