CREATE OR REPLACE PROCEDURE gercon_cad_agenda_consulta IS

    CURSOR c IS
        SELECT ROWID AS id,
               g.*
          FROM gercon_solic_consulta g
         WHERE g.ie_status = 'I'
            OR g.ie_status IS NULL; -- I = IMPORTADO

    c_01 c%ROWTYPE;

    ie_erro_w          GERCON_SOLIC_CONSULTA.IE_STATUS%TYPE;
    ds_erro_w          GERCON_SOLIC_CONSULTA.DS_ERRO_AGENDAMENTO%TYPE;
    cd_pessoa_fisica_w AGENDA_CONSULTA.CD_PESSOA_FISICA%TYPE;
    nm_pessoa_fisica_w GERCON_SOLIC_CONSULTA.NM_PESSOA_FISICA%TYPE;
    ie_novo_paciente_w VARCHAR2(1);
    qt_agendamentos_w  NUMBER;
    nr_seq_agenda_w    GERCON_SOLIC_CONSULTA.CD_AGENDA_EXTERNA%TYPE;
    qt_idade_w         AGENDA_CONSULTA.QT_IDADE_PAC%TYPE;

BEGIN

    OPEN c;
    LOOP
        FETCH c
            INTO c_01;
        EXIT WHEN c%NOTFOUND;
        BEGIN

            ie_erro_w := 'N';
            ds_erro_w := '';

            -- verifica se a data esta disponivel
            SELECT MAX(nr_sequencia)
              INTO nr_seq_agenda_w
              FROM agenda_consulta c
             WHERE dt_agenda = c_01.dt_agenda
               AND ie_status_agenda IN ('L', 'LF')
               AND cd_agenda = c_01.cd_agenda_externa;
            IF nr_seq_agenda_w IS NULL THEN
                ie_erro_w := 'S';
                ds_erro_w := obter_expressao_dic_objeto(1112604);
            END IF;

            IF ie_erro_w = 'N' THEN
                gercon_cad_pf(c_01.nm_pessoa_fisica,
                              c_01.nr_cartao_nac_sus,
                              c_01.nr_telefone_paciente,
                              c_01.nr_cpf,
                              c_01.dt_nascimento,
                              c_01.ie_sexo,
                              c_01.ie_estado_civil,
                              c_01.nr_seq_cor_pele,
                              c_01.cd_nacionalidade,
                              c_01.ds_bairro,
                              c_01.cd_cep,
                              c_01.ds_endereco,
                              c_01.nr_endereco,
                              c_01.ds_complemento,
                              c_01.ds_municipio,
                              c_01.nm_contato,
                              cd_pessoa_fisica_w,
                              nm_pessoa_fisica_w,
                              ie_novo_paciente_w,
                              ie_erro_w,
                              ds_erro_w);
            END IF;

            -- verificar se existe outro agendamento na mesma data para aquele paciente
            IF ie_erro_w = 'N' AND
               ie_novo_paciente_w = 'N' THEN
                SELECT COUNT(*)
                  INTO qt_agendamentos_w
                  FROM agenda_consulta c,
                       agenda          a
                 WHERE c.cd_agenda = a.cd_agenda
                   AND c.dt_agenda = c_01.dt_agenda
                   AND c.cd_pessoa_fisica = cd_pessoa_fisica_w
                   AND c.ie_status_agenda NOT IN ('C', 'F', 'I')
                   AND a.cd_tipo_agenda IN ('3', '4');
                IF qt_agendamentos_w > 0 THEN
                    ie_erro_w := 'S';
                    ds_erro_w := obter_expressao_dic_objeto(1112605);
                END IF;
            END IF;

            -- efetiva o agendamento da consulta
            IF ie_erro_w = 'N' THEN

                IF c_01.dt_nascimento IS NOT NULL THEN
                    SELECT trunc(months_between(trunc(SYSDATE), c_01.dt_nascimento) / 12) INTO qt_idade_w FROM dual;
                END IF;

                BEGIN
                    UPDATE agenda_consulta a
                       SET a.dt_agendamento    = nvl(a.dt_agendamento, SYSDATE),
                           a.nm_usuario        = 'GERCON',
                           a.nm_usuario_origem = 'GERCON',
                           a.ie_status_agenda  = 'CN',
                           a.cd_pessoa_fisica  = cd_pessoa_fisica_w,
                           a.nm_paciente       = nm_pessoa_fisica_w,
                           a.qt_idade_pac      = qt_idade_w,
                           a.dt_nascimento_pac = c_01.dt_nascimento,
                           a.ie_classif_agenda = nvl(a.ie_classif_agenda, c_01.ie_classif_agenda),
                           a.cd_cid            = c_01.cd_cid,
                           --a.cd_especialidade  = c_01.cd_especialidade,
                           a.cd_medico_req = c_01.cd_medico
                     WHERE a.nr_sequencia = nr_seq_agenda_w;
                EXCEPTION
                    WHEN OTHERS THEN
                        ie_erro_w := 'S';
                        ds_erro_w := obter_expressao_dic_objeto(1112606) || ' - '  || SQLERRM;
                END;

            END IF;

            IF ie_erro_w = 'S' THEN
                UPDATE gercon_solic_consulta
                   SET ie_status           = 'E', -- E = ERRO NO AGENDAMENTO
                       ds_erro_agendamento = ds_erro_w
                 WHERE ROWID = c_01.id;
            ELSE
                UPDATE gercon_solic_consulta
                   SET ie_status           = 'A', -- A = AGENDADO
                       ds_erro_agendamento = ''
                 WHERE ROWID = c_01.id;
                 
                 INSERT INTO AGENDA_CONSULTA_ADIC
                   (nr_seq_agenda, nr_seq_sistema_integracao)
                 VALUES
                    (nr_seq_agenda_w, C_01.nr_seq_sistema_integracao);
                 
            END IF;

            COMMIT;

        END;
    END LOOP;

END;
/
