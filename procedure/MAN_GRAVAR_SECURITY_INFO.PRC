create or replace procedure man_gravar_security_info(
		nr_seq_ordem_serv_p 		number,
		ie_potencial_privacy_p 		varchar2,
		ds_potencial_privacy_p 		varchar2,
		ie_potencial_security_p 	varchar2,
		ds_potencial_security_p		varchar2,
		nr_customer_requirement_p 	number,
		ie_potential_harmed_p 		varchar2,
		ds_potential_harmed_p 		varchar2,
		ie_potencial_safety_p 		varchar2,
		ds_potencial_safety_p 		varchar2,
		nr_seq_reg_tipo_risco_p		varchar2,
		ie_malfunction_p 		varchar2,
		ds_malfunction_p 		varchar2,
		ie_severity_harm_p 		varchar2,
		ie_probability_harm_p 		varchar2,
		ie_market_anomaly_p		varchar2,
		nm_user_safety_p 		varchar2,
		nm_usuario_p 			varchar2) is
	
	ie_security_event_w		varchar2(1);
	ie_harmed_event_w 		varchar2(1);
	ie_unacceptable_w 		varchar2(1);
	ie_determination_risk_w		varchar2(1);
	ie_medical_device_w		reg_customer_requirement.ie_clinico%type;	
	
	alterou_info_w 			varchar2(1);
	ie_plataforma_w 		man_ordem_servico.ie_plataforma%type;
	nr_versao_cliente_w 		man_ordem_servico.nr_versao_cliente_abertura%type;
	ie_assessment_w			man_ordem_servico.ie_complaint%type;
	ie_complaint_w			man_ordem_servico.ie_complaint%type;
	ie_classificacao_w		man_ordem_servico.ie_classificacao%type;
	ie_classificacao_cliente_w	man_ordem_servico.ie_classificacao_cliente%type;
	ie_market_anomaly_w		man_ordem_servico.ie_market_anomaly%type;

	os_duplicadas_w 		varchar2(1000);

	ds_mensagem_segu_w 	varchar2(4000);
	ds_mensagem_priv_w 	varchar2(4000);
	ds_potential_duplic_w 	varchar2(2000);
	
	ds_email_destino_w 	varchar2(255) 	:= 'security.emr@philips.com';
	new_line_w 		varchar2(2) 	:= chr(13) || chr(10);

begin

if ( nr_seq_ordem_serv_p is not null ) then

	select	mos.ie_classificacao,
		mos.ie_classificacao_cliente
	into	ie_classificacao_w,
		ie_classificacao_cliente_w
	from	man_ordem_servico mos
	where 	mos.nr_sequencia 	= nr_seq_ordem_serv_p;

	ie_medical_device_w	:= man_obter_se_md(nr_customer_requirement_p);
	ie_security_event_w	:= case when nvl(ie_potencial_privacy_p, 'N') = 'S' or nvl(ie_potencial_security_p, 'N') = 'S' then 'S' else 'N' end;
	ie_harmed_event_w 	:= case when (nvl(ie_potential_harmed_p, 'N') = 'S' or nvl(ie_potencial_safety_p, 'N') = 'S' or nvl(ie_malfunction_p, 'N') = 'S') and ie_medical_device_w = 'S' then 'S' else 'N' end;
	
	ie_determination_risk_w := man_obter_regra_determ_risco(ie_probability_harm_p, ie_severity_harm_p);
	ie_unacceptable_w 	:= case when ie_determination_risk_w = 'I' then 'S' else 'N' end;
	ie_market_anomaly_w	:= case when ie_classificacao_cliente_w = 'A' and ie_classificacao_w = 'E' and ie_market_anomaly_p = 'S' then 'S' else 'N' end;

	ie_assessment_w		:= case	when		ie_security_event_w = 'S'
						or	ie_harmed_event_w = 'S'
						or	ie_unacceptable_w = 'S'
						or	ie_medical_device_w = 'S'
						or	ie_market_anomaly_w = 'S'
						then 'S'
					else 'N'
				end;
	
	select 	nvl(max('S'), 'N') alterou_info,
		max(ie_plataforma),
		max(nr_versao_cliente_abertura),
		decode(max(ie_plataforma), 'H', ie_assessment_w, 'N') ie_complaint /* Only HTML5 can have IE_COMPLAINT = 'S' */
	into 	alterou_info_w,
		ie_plataforma_w,
		nr_versao_cliente_w,
		ie_complaint_w
	from 	man_ordem_servico
	where 	nr_sequencia 	= nr_seq_ordem_serv_p
	and 	(	'N' 	= decode(ie_potencial_privacy, ie_potencial_privacy_p, 'S', 'N')
		or 	'N' 	= decode(ds_potencial_privacy, ds_potencial_privacy_p, 'S', 'N')
		or 	'N' 	= decode(ie_potencial_security, ie_potencial_security_p, 'S', 'N')
		or 	'N' 	= decode(ds_potencial_security, ds_potencial_security_p, 'S', 'N')
		or 	'N' 	= decode(nr_customer_requirement, nr_customer_requirement_p, 'S', 'N')
		or 	'N' 	= decode(ie_potential_harmed, ie_potential_harmed_p, 'S', 'N')
		or 	'N' 	= decode(ds_potential_harmed, ds_potential_harmed_p, 'S', 'N')
		or 	'N' 	= decode(ie_potencial_safety, ie_potencial_safety_p, 'S', 'N')
		or 	'N' 	= decode(ds_potencial_safety, ds_potencial_safety_p, 'S', 'N')
		or 	'N' 	= decode(ie_malfunction, ie_malfunction_p, 'S', 'N')
		or 	'N' 	= decode(ds_malfunction, ds_malfunction_p, 'S', 'N')
		or 	'N' 	= decode(ie_severity_harm, ie_severity_harm_p, 'S', 'N')
		or 	'N' 	= decode(ie_probability_harm, ie_probability_harm_p, 'S', 'N')
		or 	'N' 	= decode(ie_market_anomaly, ie_market_anomaly_p, 'S', 'N')
	);
	
	if ( alterou_info_w = 'S' ) then
		os_duplicadas_w 	:= man_obter_os_complaint_duplic(nr_seq_ordem_serv_p, nr_versao_cliente_w, nr_customer_requirement_p);
		
		if (ie_market_anomaly_p = 'N' and ie_security_event_w = 'N' and ie_classificacao_cliente_w = 'A') then
			ie_complaint_w := 'N';
		end if;

		update 	man_ordem_servico
		set 	ie_potencial_privacy 	= ie_potencial_privacy_p,
			ds_potencial_privacy 	= ds_potencial_privacy_p,
			ie_potencial_security 	= ie_potencial_security_p,
			ds_potencial_security 	= ds_potencial_security_p,
			nr_customer_requirement = nr_customer_requirement_p,
			ie_potential_harmed 	= decode(ie_medical_device_w, 'S', ie_potential_harmed_p, null),
			ds_potential_harmed 	= decode(ie_medical_device_w, 'S', ds_potential_harmed_p, null),
			ie_potencial_safety 	= decode(ie_medical_device_w, 'S', ie_potencial_safety_p, null),
			ds_potencial_safety 	= decode(ie_medical_device_w, 'S', ds_potencial_safety_p, null),
			nr_seq_reg_tipo_risco	= nr_seq_reg_tipo_risco_p,
			ie_malfunction 		= decode(ie_medical_device_w, 'S', ie_malfunction_p, null),
			ds_malfunction 		= decode(ie_medical_device_w, 'S', ds_malfunction_p, null),
			ie_severity_harm 		= decode(ie_medical_device_w, 'S', ie_severity_harm_p, null),
			ie_probability_harm 	= decode(ie_medical_device_w, 'S', ie_probability_harm_p, null),
			nm_user_safety 			= nm_user_safety_p,
			dt_release_safety 		= case when nm_user_safety_p is not null then sysdate else null end,
			ie_complaint 			= ie_complaint_w,
			ie_potential_complaint_duplic 	= case when ie_complaint_w = 'S' and os_duplicadas_w is not null then 'S' else 'N' end,
			ie_market_anomaly		= ie_market_anomaly_p
		where 	nr_sequencia = nr_seq_ordem_serv_p;
		commit;

		if (ie_potencial_privacy_p is not null or ie_potencial_security_p is not null) then
			generate_history_regulatory(nr_seq_ordem_serv_p, nm_usuario_p, 'S', 'N', 'N', 'N'); -- CH: Potential Security or Privacy Event=
		end if;
		
		if (ie_medical_device_w = 'S' and (ie_potential_harmed_p is not null or ie_potencial_safety_p is not null or ie_malfunction_p is not null)) then
			generate_history_regulatory(nr_seq_ordem_serv_p, nm_usuario_p, 'N', 'S', 'N', 'N'); -- CH: Potential Patient Harmed Event
		end if;
		
		
		
		/* REGISTRO DE HISTORICOS */
		if (ie_plataforma_w = 'H') then
			generate_history_regulatory(nr_seq_ordem_serv_p, nm_usuario_p, 'N', 'N', 'S', 'N'); -- CH: Complaint identified or CH: Not complaint identified
		end if;
		
		if (ie_security_event_w = 'S') then
			generate_history_regulatory(nr_seq_ordem_serv_p, nm_usuario_p, 'N', 'N', 'N', 'S');
		end if;
	end if;
end if;

end man_gravar_security_info;
/
