create or replace
procedure pls_verificar_baixa_titulo(	nr_seq_mensalidade_p 	number,
					ie_varios_titulos_p	varchar2) is 

qt_baixas_mens_w	number(10);
ie_varios_titulos_w	varchar2(1) := 'N';
nr_titulo_w		titulo_receber.nr_titulo%type;
ie_situacao_w		varchar2(1);

Cursor C01 is
	select	nr_titulo,
		ie_situacao
	from	titulo_receber
	where	nr_seq_mensalidade = nr_seq_mensalidade_p;

begin

if	(ie_varios_titulos_p = 'S') then
	open C01;
	loop
	fetch C01 into	
		nr_titulo_w,
		ie_situacao_w;
	exit when C01%notfound;
		begin
		if	(ie_situacao_w <> 3) then
			select	count(1)
			into	qt_baixas_mens_w
			from	titulo_receber_liq a
			where	a.nr_titulo = nr_titulo_w
			and	a.nr_seq_liq_origem is null
			and	not exists (	select	1
						from	titulo_receber_liq x
						where	x.nr_titulo = a.nr_titulo
						and	x.nr_seq_liq_origem = a.nr_sequencia);
			
			if	(qt_baixas_mens_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort( 475552, 'NR_SEQ_MENSALIDADE=' || nr_seq_mensalidade_p || ';NR_TITULO=' || nr_titulo_w);
			end if;
		end if;
		end;
	end loop;
	close C01;
elsif	(ie_varios_titulos_p = 'N') then
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_seq_mensalidade = nr_seq_mensalidade_p;

	if	(nr_titulo_w is not null) then
		select	count(1)
		into	qt_baixas_mens_w
		from	titulo_receber_liq b,
			titulo_receber	a
		where	a.nr_titulo	= b.nr_titulo
		and	a.nr_titulo = nr_titulo_w
		and	a.ie_situacao <> 3
		and     b.nr_seq_liq_origem is null
		and     not exists (  select  1
				      from    titulo_receber_liq x
				      where   x.nr_titulo = a.nr_titulo
				      and     x.nr_seq_liq_origem = b.nr_sequencia);
		
		if	(qt_baixas_mens_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort( 475552, 'NR_SEQ_MENSALIDADE=' || nr_seq_mensalidade_p || ';NR_TITULO=' || nr_titulo_w);
		end if;
	end if;
end if;

end pls_verificar_baixa_titulo;
/
