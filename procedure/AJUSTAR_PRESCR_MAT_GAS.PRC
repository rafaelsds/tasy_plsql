create or replace
procedure Ajustar_prescr_mat_gas(	nr_prescricao_p		number,
									nr_sequencia_p		number,
									cd_perfil_p			number,
									nm_usuario_p		varchar2) is

nr_ocorrencia_w			number(19);
nr_sequencia_w			number(6);
cd_material_w			number(6);
qt_unitaria_w			number(18,6);
qt_material_w			number(15,3);
qt_dose_especial_w		number(18,6);
ds_dose_diferenciada_w	varchar2(50);
ie_origem_inf_w			varchar2(1);
qt_dispensar_w			number(18,6);
qt_dose_w				number(18,6);
ds_erro_w				varchar2(255);
cd_intervalo_w			varchar2(7);
ie_suspenso_w			varchar2(1);
ie_via_aplicacao_w		varchar2(5);
cd_unidade_medida_dose_w varchar2(30);
dt_primeiro_horario_w	date;
nr_horas_validade_w		number(5);
ds_horarios_w			varchar2(2000);
ds_horarios2_w			varchar2(255);
ds_horarios3_w			varchar2(255);
dt_prev_execucao_w		date;
ie_regra_disp_w			varchar2(1);
qt_hora_intervalo_w		number(2);
qt_min_intervalo_w		number(5);
ie_prescr_mat_sem_lib_w	varchar2(30);
cd_estabelecimento_w	number(4);

cursor C01 is
Select	a.cd_intervalo,
		nvl(b.nr_sequencia,0),
		a.dt_prev_execucao,
		c.dt_primeiro_horario,
		c.nr_horas_validade,
		nvl(b.cd_material,0),
		nvl(b.qt_unitaria,0),
		nvl(b.qt_material,0),
		nvl(b.qt_dose_especial,0),
		nvl(b.ds_dose_diferenciada,''),
		nvl(b.ie_origem_inf,'1'),
		b.ie_via_aplicacao,
		b.cd_unidade_medida_dose,
		decode(nvl(a.ie_suspenso,'N'),'N',nvl(b.ie_suspenso,'N'),nvl(a.ie_suspenso,'N')),
		substr(a.ds_horarios,1,255),
		b.qt_dose,
		b.qt_hora_intervalo,
		b.qt_min_intervalo
from	prescr_material b,
		prescr_gasoterapia a,
		prescr_medica c
where	a.nr_sequencia	= b.nr_seq_gasoterapia
and		a.nr_prescricao = b.nr_prescricao
and		b.nr_prescricao = c.nr_prescricao
and		b.nr_prescricao = nr_prescricao_p
and		a.nr_sequencia  = nr_sequencia_p
and		a.nr_prescricao = nr_prescricao_p
and		c.nr_prescricao	= nr_prescricao_p;

begin

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

Obter_Param_Usuario(924, 530, cd_perfil_p, nm_usuario_p, cd_estabelecimento_w, ie_prescr_mat_sem_lib_w);

open C01;
loop
fetch C01 into
	cd_intervalo_w,
	nr_sequencia_w,
	dt_prev_execucao_w,
	dt_primeiro_horario_w,
	nr_horas_validade_w,
	cd_material_w,
	qt_unitaria_w,
	qt_material_w,
	qt_dose_especial_w,
	ds_dose_diferenciada_w,
	ie_origem_inf_w,
	ie_via_aplicacao_w,
	cd_unidade_medida_dose_w,
	ie_suspenso_w,
	ds_horarios3_w,
	qt_dose_w,
	qt_hora_intervalo_w,
	qt_min_intervalo_w;
exit when C01%notfound;	

	qt_material_w	:= 0;
	nr_ocorrencia_w	:= 0;
	Calcular_Horario_Prescricao(nr_prescricao_p,cd_intervalo_w,dt_primeiro_horario_w,to_date(to_char(dt_prev_execucao_w,'hh24:mi'),'hh24:mi'),
				nr_horas_validade_w,cd_material_w,qt_hora_intervalo_w,qt_min_intervalo_w,nr_ocorrencia_w,ds_horarios_w,ds_horarios2_w,'N', null);

	ds_horarios_w	:= ds_horarios_w || ds_horarios2_w;

	Obter_Quant_Dispensar(cd_estabelecimento_w,cd_material_w, nr_prescricao_p, nr_sequencia_w, cd_intervalo_w, ie_via_aplicacao_w,
				qt_unitaria_w, nvl(qt_dose_especial_w,0), nr_ocorrencia_w, ds_dose_diferenciada_w,
				ie_origem_inf_w, cd_unidade_medida_dose_w, 1, qt_material_w, qt_dispensar_w, ie_regra_disp_w, ds_erro_w,'N','N');
	
	Update	prescr_material
	set	cd_intervalo		= cd_intervalo_w,
		ie_suspenso			= ie_suspenso_w,
		ie_utiliza_kit		= 'N',
		ie_medicacao_paciente	= 'N',
		hr_prim_horario		= to_char(dt_prev_execucao_w,'hh24:mi'),
		qt_hora_intervalo	= qt_hora_intervalo_w,
		qt_min_intervalo	= qt_min_intervalo_w,
		ds_horarios		= nvl(ds_horarios3_w,ds_horarios_w),
		nr_ocorrencia		= nr_ocorrencia_w,
		qt_total_dispensar	= qt_dispensar_w,
		qt_material		= qt_material_w,
		ie_regra_disp		= decode(nvl(ie_regra_disp,'X'), 'D', ie_regra_disp, ie_regra_disp_w),
		qt_dose			= qt_dose_w
	where	nr_sequencia		= nr_sequencia_w
	and		nr_prescricao		= nr_prescricao_p;

	if (ie_prescr_mat_sem_lib_w = 'S') then
		Gerar_prescr_mat_sem_dt_lib(nr_prescricao_p,nr_sequencia_w,cd_perfil_p,'N',nm_usuario_p,null);
	end if;

end loop;
close C01;

commit;

end Ajustar_prescr_mat_gas;
/