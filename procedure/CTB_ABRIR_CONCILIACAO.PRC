create or replace
procedure ctb_abrir_conciliacao(
				nr_seq_concil_p		number,
				nm_usuario_p		varchar2,
				cd_conta_contabil_p	varchar2,
				ie_operacao_p		varchar2) is 

cd_estab_exclusivo_w		ctb_registro_concil.cd_estab_exclusivo%type;
cd_tipo_lote_contabil_w		ctb_registro_concil.cd_tipo_lote_contabil%type;
dt_inicial_w			date;
dt_final_w			date;
ie_tem_lote_w			varchar2(1);
nr_seq_mes_ref_w		ctb_registro_concil.nr_seq_mes_ref%type;
nr_seq_movimento_w		ctb_movimento.nr_sequencia%type;
qt_lote_concil_w		number(10);
qt_registro_w			number(10)	:= 0;
dt_fim_mes_w			date;

cursor	c_movimentos is
select 	a.nr_sequencia
from 	ctb_mes_ref c,
	lote_contabil b,
	ctb_movimento_v a
where	b.nr_lote_contabil	= a.nr_lote_contabil
and	c.nr_sequencia		= b.nr_seq_mes_ref
/*and	c.nr_sequencia		= nr_seq_mes_ref_w Retirado na OS 1435053 
Altera��o para trazer todos os movimentos n�o conciliados, independente do m�s
*/
and	b.cd_estabelecimento	= nvl(cd_estab_exclusivo_w, b.cd_estabelecimento)
and	b.cd_tipo_lote_contabil	= nvl(cd_tipo_lote_contabil_w, b.cd_tipo_lote_contabil)
and	b.dt_atualizacao_saldo is not null
and	(a.nr_seq_reg_concil is null or nvl(a.ie_status_concil,'NC') != 'C')
and	exists(	select	1
		from	ctb_registro_concil_conta y
		where	y.nr_seq_reg_concil	= nr_seq_concil_p
		and	y.cd_conta_contabil	= a.cd_conta_contabil)
and	a.dt_movimento <= dt_fim_mes_w;

begin

select	a.nr_seq_mes_ref,
	a.dt_inicial,
	a.dt_final,
	a.cd_estab_exclusivo,
	a.cd_tipo_lote_contabil
into	nr_seq_mes_ref_w,
	dt_inicial_w,
	dt_final_w,
	cd_estab_exclusivo_w,
	cd_tipo_lote_contabil_w
from	ctb_registro_concil a
where	a.nr_sequencia	= nr_seq_concil_p;

select	fim_mes(dt_referencia)
into	dt_fim_mes_w
from 	ctb_mes_ref
where	nr_sequencia = nr_seq_mes_ref_w;

dt_inicial_w	:= trunc(dt_inicial_w);
dt_final_w	:= fim_dia(dt_final_w);

ie_tem_lote_w	:= 'N';
select	count(nr_lote_contabil)
into	qt_lote_concil_w
from	ctb_registro_concil_lote
where	nr_seq_reg_concil	= nr_seq_concil_p;

if	(qt_lote_concil_w > 0) then
	ie_tem_lote_w	:= 'S';
end if;

if	(ie_operacao_p = 'A') then

	open c_movimentos;
	loop
	fetch c_movimentos into
		nr_seq_movimento_w;
	exit when c_movimentos%notfound;
		begin
		
		update 	ctb_movimento a
		set 	a.ie_status_concil 	= 'NC',
			a.nr_seq_reg_concil 	= nr_seq_concil_p
		where 	a.nr_sequencia		= nr_seq_movimento_w;
		qt_registro_w	:= qt_registro_w + 1;
		if	(qt_registro_w >= 500) then
			qt_registro_w	:= 0;
			commit;
		end if;
		end;
	end loop;
	close c_movimentos;	

elsif	(ie_operacao_p = 'D') then

	update 	ctb_movimento a
	set 	a.ie_status_concil 	= 'NC',
		a.nr_seq_reg_concil 	= null
	where 	a.nr_seq_reg_concil	= nr_seq_concil_p;	

end if;

update	ctb_registro_concil
set	dt_abertura		= decode(ie_operacao_p, 'A', sysdate, null),
	nm_usuario_abertura	= decode(ie_operacao_p, 'A', nm_usuario_p, null)
where	nr_sequencia		= nr_seq_concil_p;

ctb_atualizar_concil(nr_seq_concil_p);

commit;

end ctb_abrir_conciliacao;
/