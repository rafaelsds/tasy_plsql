create or replace
procedure atualiza_caixa_saldo_diario(
			nm_usuario_p		Varchar2,
			nr_sequencia_p		number) is 

begin

if (nr_sequencia_p is not null) then
	begin
	update  caixa_saldo_diario 
	set     dt_fechamento = null, 
        	nm_usuario_fechamento = null, 
        	nm_usuario = nm_usuario_p,
        	dt_atualizacao = sysdate 
	where   nr_sequencia = nr_sequencia_p;
	end;
end if;

commit;

end atualiza_caixa_saldo_diario;
/