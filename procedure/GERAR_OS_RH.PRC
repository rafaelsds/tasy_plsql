create or replace
procedure gerar_os_rh(	nr_seq_inscrito_p	in number,
			nm_usuario_p		in varchar2,
			cd_estabelecimento_p	in varchar2,
			dt_fim_previsto_p	in date,
			nr_sequencia_p		out number,
			nm_solicitante_p	out varchar2) is

cd_pessoa_fisica_w	varchar2(50);
nr_seq_tipo_aval_w	number(10);
nm_usuario_exec_w	varchar2(50);
cd_superior_w		varchar2(50);
ds_dano_w		varchar2(8000);
ds_usuario_w		varchar2(100);
ds_gerente_w		varchar2(100);
ds_evento_w		varchar2(100);
dt_fim_tre_w		date;

begin

-- Cria uma OS para treinamento
select	max(a.cd_pessoa_fisica),
	max(a.ds_usuario)
into	cd_pessoa_fisica_w,
	nm_solicitante_p
from	usuario	a
where	a.nm_usuario = nm_usuario_p;

select	man_ordem_servico_seq.nextval
into	nr_sequencia_p
from	dual;


select	  max(a.nm_usuario),
	  max(a.cd_pessoa_fisica),
	  max(a.ds_usuario)
into	  nm_usuario_exec_w,
	  cd_pessoa_fisica_w,
	  ds_usuario_w
from	  usuario a
where	  a.cd_pessoa_fisica = (select b.cd_pessoa_fisica
			   	from   tre_inscrito b
				where  b.nr_sequencia = nr_seq_inscrito_p);								

select	max(ds_evento),
	max(dt_fim)
into	ds_evento_w,
	dt_fim_tre_w
from	tre_evento
where	nr_sequencia = (select	b.nr_seq_evento
			from	tre_inscrito b
			where	b.nr_sequencia = nr_seq_inscrito_p);

select	max(ds_usuario)
into	ds_gerente_w
from	usuario
where	cd_pessoa_fisica = sis_obter_gerente(nm_usuario_exec_w, 'C');

ds_dano_w := 'Funcion�rio (a): ' || ds_usuario_w || chr(13) || chr(10);
ds_dano_w := ds_dano_w || 'Cargo: ' || obter_cargo_usuario(nm_usuario_exec_w) || chr(13) || chr(10);
ds_dano_w := ds_dano_w || 'Departamento: ' || obter_setor_pf_usuario(cd_pessoa_fisica_w) || chr(13) || chr(10);
ds_dano_w := ds_dano_w || 'Gerente: ' || ds_gerente_w || chr(13) || chr(10);
ds_dano_w := ds_dano_w || 'Treinamento: ' || ds_evento_w || chr(13) || chr(10);
ds_dano_w := ds_dano_w || 'Data final do treinamento: ' || dt_fim_tre_w || chr(13) || chr(10);


insert	into man_ordem_servico(
	nr_sequencia,
	cd_pessoa_solicitante,
	nr_seq_localizacao,
	nr_seq_equipamento,
	dt_ordem_servico,
	ds_dano_breve,
	nr_seq_estagio,
	nr_grupo_planej,
	nr_grupo_trabalho,
	ds_dano,
	ie_tipo_ordem,
	ie_prioridade,
	ie_parado,
	dt_atualizacao,
	nm_usuario,
	dt_fim_previsto,
	ie_aval_efic,
	ie_status_ordem,
	ie_origem_os)
values (nr_sequencia_p,
	'29285', -- Vanessa Louren�o deve ser a solicitante
	1272,	--corp	1272
	5267,	--corp	5267
	sysdate,
	'Avalia��o de efic�cia do treinamento',
	1891,
	2,
	2,
	ds_dano_w,
	15,
	'M',
	'N',
	sysdate,
	nm_usuario_p,
	dt_fim_previsto_p,
	'S',
	1,
	7);


-- Vanessa Lourenco solicitou que fosse colocada como executora prevista
insert	into man_ordem_servico_exec(
	nr_sequencia,
	nr_seq_ordem,
	dt_atualizacao,
	nm_usuario,
	nm_usuario_exec,
	nr_seq_funcao)
values (man_ordem_servico_exec_seq.nextval,
	nr_sequencia_p,
	sysdate,
	'vlourenco',
	'vlourenco',
	521); -- Analise		--521	

	
if	(nm_usuario_exec_w is not null) then
	insert	into man_ordem_servico_exec(
		nr_sequencia,
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		nm_usuario_exec,
		nr_seq_funcao)
	values (man_ordem_servico_exec_seq.nextval,
		nr_sequencia_p,
		sysdate,
		nm_usuario_p,
		nm_usuario_exec_w,
		521); -- Analise		--521
end if;

cd_superior_w := sis_obter_gerente(nm_usuario_exec_w, 'C');
if	(cd_superior_w = cd_pessoa_fisica_w) then
	cd_superior_w := sis_obter_diretor(nm_usuario_exec_w, 'C');
end if;

if	(nvl(cd_superior_w, 0) > 0) then
begin
	select	max(a.nm_usuario)
	into	nm_usuario_exec_w
	from	usuario a
	where	a.cd_pessoa_fisica = cd_superior_w;
	
	insert	into man_ordem_servico_exec(
		nr_sequencia,
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		nm_usuario_exec,
		nr_seq_funcao)
	values (man_ordem_servico_exec_seq.nextval,
		nr_sequencia_p,
		sysdate,
		nm_usuario_p,
		nm_usuario_exec_w,
		521); -- Analise        521
end;
end if;
	
-- Atualiza informa��es do treinamento	
update	tre_inscrito
set	nr_seq_ordem = nr_sequencia_p
where	nr_sequencia = nr_seq_inscrito_p;


commit;

end gerar_os_rh;
/