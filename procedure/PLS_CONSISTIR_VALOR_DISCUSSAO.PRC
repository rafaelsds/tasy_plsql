create or replace
procedure pls_consistir_valor_discussao(nr_seq_disc_proc_p	number,
					nr_seq_disc_mat_p	number,
					vl_aceito_p		number,
					vl_negado_p		number,
					vl_discussao_p		number) is 

vl_total_w		number(15,2);
					
begin
if	(nr_seq_disc_proc_p is not null) then
	vl_total_w := (nvl(vl_aceito_p,0) + nvl(vl_negado_p,0));
	
	if	(nvl(vl_discussao_p,0) <> vl_total_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(148398);
	end if;

elsif	(nr_seq_disc_mat_p is not null) then
	vl_total_w := (nvl(vl_aceito_p,0) + nvl(vl_negado_p,0));
	
	if	(nvl(vl_discussao_p,0) <> vl_total_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(148398);
	end if;

end if;

commit;

end pls_consistir_valor_discussao;
/
