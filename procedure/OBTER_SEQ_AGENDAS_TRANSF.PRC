CREATE OR REPLACE
PROCEDURE obter_seq_agendas_transf (	ds_lista_seq_p		VARCHAR2,
					hr_transf_p		VARCHAR2,
					cd_estabelecimento_p	NUMBER,
					cd_agenda_p		NUMBER,
					nm_usuario_p		VARCHAR2,
					ds_erro_p	OUT	VARCHAR2	) IS

tam_lista_w		NUMBER(10,0);
ie_pos_virgula_w	NUMBER(3,0);
ds_lista_seq_w		VARCHAR2(2000);
ds_retorno_w		varchar2(255);
nr_seq_destino_w	number(10,0);
nr_seq_origem_w		number(10,0);
dt_agenda_w		date;
ds_erro_transf_w	varchar2(255);
ie_feriado_w		varchar2(1);
ie_sobra_horario_w	varchar2(1);
ds_erro_w			varchar2(255);

begin

ds_erro_w	:= wheb_mensagem_pck.get_texto(280736);
ds_lista_seq_w := ds_lista_seq_p;

while	(ds_lista_seq_w is not null)  loop
	begin
	tam_lista_w		:= length(ds_lista_seq_w);
	ie_pos_virgula_w	:= instr(ds_lista_seq_w,',');

	if	(ie_pos_virgula_w <> 0) then
		nr_seq_origem_w		:= to_number(substr(ds_lista_seq_w,1,(ie_pos_virgula_w - 1)));
		ds_lista_seq_w		:= substr(ds_lista_seq_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;

	select	max(dt_agenda)
	into	dt_agenda_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_origem_w;
	
	select 	nvl(a.ie_feriado,'N'),
		nvl(a.ie_gerar_sobra_horario,'N')
	into	ie_feriado_w,
		ie_sobra_horario_w
	from	agenda a
	where	a.cd_agenda	= cd_agenda_p;
	
	Horario_Livre_Consulta	(	cd_estabelecimento_p,
					cd_agenda_p,
					ie_feriado_w,
					dt_agenda_w,
					nm_usuario_p,
					'S',
					ie_sobra_horario_w,
					'N',
					0,
					ds_retorno_w
				);				
					
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_destino_w
	from	agenda_consulta	
	where	dt_agenda = pkg_date_utils.get_time(dt_agenda_w, hr_transf_p, 0)
	and	cd_agenda = cd_agenda_p
	and	ie_status_agenda = 'L';
		
	if	(nr_seq_destino_w > 0) then
		copiar_transferir_agenda_cons(	
						cd_estabelecimento_p,
						nr_seq_origem_w,
						nr_seq_destino_w,
						'T',
						null,
						null,
						nm_usuario_p,
						null,
						null,
						ds_erro_transf_w
						);
	else
		ds_erro_p := ds_erro_p ||ds_erro_w ||to_char(dt_agenda_w,'hh24:mi') ||',';
		ds_erro_w	:= '';
	end if;
		
	end;
end loop;

commit;
end obter_seq_agendas_transf;
/
