create or replace
procedure pls_consultar_cns(	nr_seq_solicitacao_p		number,
				nm_usuario_p			varchar2,
				nr_seq_transmissao_p	out	number) is 

nr_sequencia_w			pls_transmissao_solic_cns.nr_sequencia%type;	

begin

select	pls_transmissao_solic_cns_seq.nextval
into	nr_sequencia_w
from	dual;

insert into pls_transmissao_solic_cns (	nr_sequencia,
					nr_seq_solicitacao,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					ie_status_transmissao	)
				values(	nr_sequencia_w,
					nr_seq_solicitacao_p,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					'0');

commit;

nr_seq_transmissao_p  := nr_sequencia_w;

end pls_consultar_cns;
/
