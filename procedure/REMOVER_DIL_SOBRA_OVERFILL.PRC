create or replace
procedure remover_dil_sobra_overfill(nr_sequencia_p number) is 

nr_seq_diluentes_w 		number(10);
nr_seq_medic_sup_w 		number(10);
qt_inicial_w 			number(15,5);
cd_um_superior_w 		varchar(10);
cd_material_w 			varchar(10);
ie_tipo_w 				varchar2(2);
dt_atualizacao_nrec_w	date;	
nr_seq_lote_fornec_w 	number(10);
nr_seq_cabine_w			number(10);

begin
--nr_sequencia_p do medicamento dilu�do
--nr_seq_medic_sup_w  - nr seq do medicamento da sobra ou overfill

--pega o nr_seq_superior do medicamento dilu�do que ser� exclu�do
select 	nr_seq_superior, 
		qt_inicial,
		cd_material,
		nr_seq_cabine
into 	nr_seq_medic_sup_w,
		qt_inicial_w,
		cd_material_w,
		nr_seq_cabine_w
from 	quimio_sobra_overfill
where 	nr_sequencia = nr_sequencia_p;

select 	cd_unidade_medida,
		ie_tipo,
		trunc(dt_atualizacao_nrec),
		nr_seq_lote_fornec
into 	cd_um_superior_w,
		ie_tipo_w,
		dt_atualizacao_nrec_w,
		nr_seq_lote_fornec_w
from 	quimio_sobra_overfill
where 	nr_sequencia = nr_seq_medic_sup_w;

--Pega os diluentes do medicamento diluido
select 	nr_sequencia
into   	nr_seq_diluentes_w
from 	quimio_sobra_overfill
where 	nr_seq_superior = nr_sequencia_p;

--Atualiza o medicamento de sobra ou overfill com o qt_saldo do Dilu�do que ser� exclu�do
--Sobras e overfill sempre ser�o apenas um registro no dia

delete from quimio_sobra_overfill
where 	nr_sequencia = nr_seq_diluentes_w;

update 	quimio_sobra_overfill
set		ie_tipo				= substr(ie_tipo,1,1)
where	nr_sequencia 		= nr_sequencia_p;

commit;

end remover_dil_sobra_overfill;
/