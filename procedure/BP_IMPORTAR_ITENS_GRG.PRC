create or replace procedure BP_IMPORTAR_ITENS_GRG(nr_seq_lote_hist_p	in number) is

nr_interno_conta_w	lote_audit_hist_item.nr_interno_conta%type;
nr_interno_conta_aqr_w	conta_paciente_guia.nr_interno_conta%type;
cd_item_w		procedimento_paciente.cd_procedimento%type;
vl_glosa_w		lote_audit_hist_item.vl_glosa%type := 0;
vl_pago_w		lote_audit_hist_item.vl_pago%type;
vl_pago_char_w		varchar2(255);
vl_aceite_char_w	varchar2(255);
vl_glosa_aceita_w	lote_audit_hist_item.vl_glosa%type;
vl_glosa_char_w		varchar2(255);
vl_recebido_char_w	varchar2(255);
vl_recursado_char_w	varchar2(255);
vl_glosa_inf_char_w	varchar2(255);
nr_seq_item_conta_w	number(10);
cd_autorizacao_w	varchar2(255);
nr_doc_conv_number_w	conta_paciente_guia.cd_autorizacao%type;
ds_observacao_w		varchar2(4000);
nr_sequencia_w  lote_audit_hist_item.nr_sequencia%type;
nr_seq_propaci_w	number(10);
nr_seq_matpaci_w	lote_audit_hist_item.nr_seq_matpaci%type;
ds_item_retorno_w	varchar2(255);
qt_item_w		lote_audit_hist_item.qt_item%type;
cd_motivo_glosa_w	varchar2(20);
ie_acao_w		varchar2(5);
vl_saldo_w		lote_audit_hist_item.vl_saldo%type;
qt_retorno_w		number(5);
qt_guia_w		number(5);
qt_material_w		material_atend_paciente.qt_material%type;
qt_saldo_item_w		number(10);
vl_pago_item_w		lote_audit_hist_imp.vl_pago%type;
ds_inconsistencia_w	lote_audit_hist_imp.ds_inconsistencia%type;
ie_material_w		varchar2(1) := 'N';
ie_proced_w		varchar2(1) := 'N';
vl_procedimento_w	procedimento_paciente.vl_procedimento%type;
nr_sequencia_proc_w	procedimento_paciente.nr_sequencia%type;
vl_material_w		material_atend_paciente.vl_material%type;
dt_atendimento_w	date;
vl_descontar_w		number(15,2) := 0;
qt_descontar_w		number(10) := 0;
vl_glosa_infor_item_w	number(15,2) := 0;
vl_amenor_item_w	number(15,2) := 0;
vl_glosa_acum_w		number(15,2) := 0;
ds_erro_w		varchar2(4000) := '';
qt_reg_conpagu_w	number(10);

nr_doc_convenio_origem_w    conta_paciente_guia.cd_autorizacao%type;

type lista_importacao is record
		(nr_conta		conta_paciente.nr_interno_conta%type,
		nr_guia			conta_paciente_guia.cd_autorizacao%type,
		cd_item			number(20),
		ds_item_imp		varchar2(255),
		ds_observacao		varchar2(4000),
		vl_glosa_aceita		number(15,2),
		vl_pago			lote_audit_hist_item.vl_pago%type,
		vl_glosa_informada	number(15,2),
		vl_amenor		number(15,2),
		cd_motivo_glosa		varchar2(20),
		ie_acao_glosa		varchar2(5),
		qt_item			number(9,3),
		nr_seq_material		number(10),
		nr_seq_proced		number(10),
		vl_cobrado		number(15,2),
		nr_item			number(5));

type lista_importacao_vt is table of lista_importacao index by binary_integer;
lista_importacao_w			lista_importacao_vt;
i				integer;
j				integer;
x				integer;
type lista_mat is record
		(seq_mat		material_atend_paciente.nr_sequencia%type,
		 qt_mat			material_atend_paciente.qt_material%type,
		 dt_atend		material_atend_paciente.dt_atendimento%type,
		 vl_mat			material_atend_paciente.vl_material%type,
		cd_mat_convenio		material_atend_paciente.cd_material_convenio%type,
		cd_material_tiss	material_atend_paciente.cd_material_tiss%type,
		cd_material		material_atend_paciente.cd_material%type,
		nr_conta		conta_paciente.nr_interno_conta%type,
		nr_item_vinculado	number(5));
type lista_mat_vt is table of lista_mat index by binary_integer;
lista_mat_w			lista_mat_vt;
type lista_proc is record
		(seq_proc		procedimento_paciente.nr_Sequencia%type,
		vl_proc			procedimento_paciente.vl_procedimento%type,
		qt_proc			procedimento_paciente.qt_procedimento%type,
		cd_proc			procedimento_paciente.cd_procedimento%type,
		ie_origem		procedimento_paciente.ie_origem_proced%type,
		cd_tuss			procedimento_paciente.cd_procedimento_tuss%type,
		cd_proc_convenio	procedimento_paciente.cd_procedimento_convenio%type,
		nr_conta		conta_paciente.nr_interno_conta%type,
		nr_item_vinculado	number(5));
type lista_proc_vt is table of lista_proc index by binary_integer;
lista_proc_w			lista_proc_vt;

qt_item_acumulado_w		number(10) := 0;
vl_item_acumulado_w		number(15,2) := 0;
nr_maior_seq_positiva_w		number(10) := 0;
count_seq_positiva_w 		number(5) := 0;

cd_convenio_w		number(10);
nr_seq_guia_w		number(10);

cd_motivo_glosa_insert_w	convenio_retorno_movto.cd_motivo%type;
cd_motivo_tiss_insert_w		convenio_retorno_movto.cd_motivo_glosa_tiss%type;
ie_acao_glosa_w			varchar2(10);
ds_complemento_w		lote_audit_hist_imp.ds_complemento%type;
cd_resposta_glosa_w		lote_audit_hist_item.cd_resposta%type;
count_w				integer;
ie_motivo_glosa_convenio_w	varchar2(1) := 'N';
nr_seq_w			lote_audit_hist_item.nr_sequencia%type;
vl_amenor_w			lote_audit_hist_item.vl_amenor%type;
vl_saldo_item_w			number(15,2);
nm_usuario_w			varchar2(15) := Nvl(wheb_usuario_pck.get_nm_usuario,'Tasy_imp');
vl_glosa_informada_w		number(15,2);
vl_recursado_w			lote_audit_hist_imp.vl_amenor%type;
qt_procedimento_w		procedimento_paciente.qt_procedimento%type;
nr_seq_partic_w			lote_audit_hist_item.nr_seq_partic%type;
cursor	c01 is
	select	obter_valor_campo_separador(ds_conteudo,1,';') nr_interno_conta, -- NRO DA CONTA
		substr(obter_valor_campo_separador(ds_conteudo,2,';'),1,20) cd_autorizacao, --NRO DA GUIA
        somente_numero(obter_valor_campo_separador(ds_conteudo,6,';')) cd_item_1, --CODIGO ITEM NO TASY
		somente_numero(obter_valor_campo_separador(ds_conteudo,7,';')) cd_item_2, --CODIGO ITEM EQUIVALENTE
        obter_valor_campo_separador(ds_conteudo,19,';') ds_complemento, --JUSTIFICATIVA TEXTO LIVRE
		obter_valor_campo_separador(ds_conteudo,8,';') vl_glosa, --VALOR APRESENTADO ITEM
		substr(obter_valor_campo_separador(ds_conteudo,15,';'),1,15) vl_glosa_aceita, --VALOR ACEITE
		substr(obter_valor_campo_separador(ds_conteudo,9,';'),1,15) vl_recebido, --VL_RECEBIDO
		substr(obter_valor_campo_separador(ds_conteudo,11,';'),1,20) cd_motivo_glosa, -- CODIGO MOTIVO GLOSA CONV
        substr(obter_valor_campo_separador(ds_conteudo,21,';'),1,5) ie_acao, --ACAO DA GLOSA
		substr(obter_valor_campo_separador(ds_conteudo,23,';'),1,15) qt_cobrada, --QUANTIDADE COBRADA
		substr(obter_valor_campo_separador(ds_conteudo,20,';'),1,15) vl_recursado, --VALOR RECURSADO
		obter_valor_campo_separador(ds_conteudo,10,';') vl_glosa_informada
	from	w_conv_ret_movto
	where 	nr_seq_lote_hist	= nr_seq_lote_hist_p
	and	somente_numero(obter_valor_campo_separador(ds_conteudo,1,';')) > 0;

cursor c02 is
	select	a.nr_sequencia seq_mat,
		a.qt_material qt_mat,
		a.dt_atendimento dt_atend,
		a.vl_material vl_mat,
		a.cd_material_convenio cd_mat_convenio,
		a.cd_material_tiss cd_material_tiss,
		a.cd_material cd_material,
		a.nr_interno_conta nr_conta
	from	material_atend_paciente a
	where	a.nr_interno_conta	= nr_interno_conta_w
	and	a.cd_motivo_exc_conta	is null --nao pode considerar os materiais excluidos
	--and	a.qt_material		> 0 	--ignorar os materiais com quantidade negativa
	and	a.nr_doc_convenio = cd_autorizacao_w
	and	(a.cd_material		= somente_numero(cd_item_w) or
		somente_numero(a.cd_material_convenio)	= somente_numero(cd_item_w) or
		somente_numero(a.cd_material_tiss)	= somente_numero(cd_item_w))
	order by seq_mat;
c02_w c02%rowtype;
cursor c03 is
	select	a.nr_sequencia seq_proc,
		a.vl_procedimento vl_proc,
		a.qt_procedimento qt_proc,
		a.cd_procedimento cd_proc,
		a.ie_origem_proced ie_origem,
		a.cd_procedimento_tuss cd_tuss,
		a.cd_procedimento_convenio cd_proc_convenio,
		a.nr_interno_conta nr_conta
	from	procedimento_paciente a,
		conta_paciente b
	where	a.nr_interno_conta			= b.nr_interno_conta
	and	a.nr_interno_conta			= nr_interno_conta_w
	AND	A.NR_DOC_CONVENIO			= CD_AUTORIZACAO_W
	and	a.cd_motivo_exc_conta			is null
	and	(somente_numero(a.cd_procedimento)	= somente_numero(cd_item_w) or
		somente_numero(a.cd_procedimento_tuss)	= somente_numero(cd_item_w) or
		somente_numero(a.cd_procedimento_convenio)	= somente_numero(cd_item_w))
	order by seq_proc;
c03_w c03%rowtype;	

cursor	c04 is
select	a.nr_interno_conta,
	substr(a.nr_doc_convenio,1,20)
from	lote_audit_hist_imp a,
	conta_paciente b
where	a.nr_interno_conta	= b.nr_interno_conta
and	b.cd_convenio_parametro	= cd_convenio_w
and	a.nr_seq_lote_audit	= nr_seq_lote_hist_p
and	a.nr_interno_conta is not null
and	a.nr_doc_convenio is not null
--and	1 = 2
group by a.nr_interno_conta,
	a.nr_doc_convenio;

cursor	c05 is	
select	i.nr_sequencia,
	null nr_seq_propaci,
	i.nr_seq_matpaci,
	nvl(i.vl_pago,0),
	nvl(i.vl_glosa,0),
	nvl(i.qt_item,0),
	nvl(i.vl_glosa_informada,0),
	nvl(i.vl_amenor,0),
	i.cd_motivo_glosa_imp,
	i.ie_acao_glosa_imp,
	i.ds_complemento,
	null nr_seq_partic
from	lote_audit_hist_imp i,
	material_atend_paciente m
where	i.nr_seq_guia		= nr_seq_guia_w
and	i.nr_seq_matpaci	= m.nr_sequencia
and	i.nr_seq_lote_audit	= nr_seq_lote_hist_p
union
select	i.nr_sequencia,
	i.nr_seq_propaci,
	null nr_seq_matpaci,
	nvl(i.vl_pago,0),
	nvl(i.vl_glosa,0),
	nvl(i.qt_item,0),
	nvl(i.vl_glosa_informada,0),
	nvl(i.vl_amenor,0),
	i.cd_motivo_glosa_imp,
	i.ie_acao_glosa_imp,
	i.ds_complemento,
	p.nr_seq_partic
from	lote_audit_hist_imp i,
	procedimento_paciente m,
  procedimento_participante p
where	i.nr_seq_guia		= nr_seq_guia_w
and	i.nr_Seq_propaci	= m.nr_sequencia
and	i.nr_seq_lote_audit	= nr_seq_lote_hist_p
and p.nr_sequencia(+)  = m.nr_sequencia
and p.vl_participante(+) = i.vl_cobrado;	

cursor	c06 is
	select	distinct obter_valor_campo_separador(ds_conteudo,1,';') nr_interno_conta, 
            substr(obter_valor_campo_separador(ds_conteudo,2,';'),1,20) cd_autorizacao,
            somente_numero(obter_valor_campo_separador(ds_conteudo,6,';')) cd_item_1
	from	w_conv_ret_movto
	where 	nr_seq_lote_hist	= nr_seq_lote_hist_p
	and	somente_numero(obter_valor_campo_separador(ds_conteudo,1,';')) > 0;

begin

i := 0;
x := 0;
j := 0;
lista_importacao_w.delete;	
lista_proc_w.delete;
lista_mat_w.delete;

begin
open C06;
loop
fetch C06 into
	nr_interno_conta_aqr_w,
	nr_doc_conv_number_w, 
	cd_item_w; 
exit when C06%notfound;

	begin
		cd_autorizacao_w := nr_doc_conv_number_w;
	exception
	when others then
		Raise_application_error(-20011,'Numero da guia fora do padrao.');
	end;

	begin
	select 	a.cd_autorizacao
	into	nr_doc_convenio_origem_w
	from	conta_paciente_guia a
	where	a.nr_interno_conta = nr_interno_conta_aqr_w
	and	trim(a.cd_autorizacao) = trim(cd_autorizacao_w);
	exception	
		when others then
		nr_doc_convenio_origem_w := null;
	end;

	if	(nr_doc_convenio_origem_w is not null) then
		cd_autorizacao_w := nr_doc_convenio_origem_w;
	end if;


	nr_interno_Conta_w := nr_interno_conta_aqr_w;


	open C02;
	loop
	fetch C02 into	
		c02_w;
	exit when C02%notfound;

		lista_mat_w(x).seq_mat			:= c02_w.seq_mat;
		lista_mat_w(x).qt_mat			:= c02_w.qt_mat;
		lista_mat_w(x).dt_atend			:= c02_w.dt_atend;
		lista_mat_w(x).vl_mat			:= c02_w.vl_mat;
		lista_mat_w(x).cd_mat_convenio		:= somente_numero(c02_w.cd_mat_convenio);
		lista_mat_w(x).cd_material_tiss		:= somente_numero(c02_w.cd_material_tiss);
		lista_mat_w(x).cd_material		:= c02_w.cd_material;
		lista_mat_w(x).nr_conta			:= c02_w.nr_conta;

		x := x + 1;

	end loop;
	close C02;


	open C03;
	loop
	fetch C03 into	
		c03_w;
	exit when C03%notfound;

		lista_proc_w(j).seq_proc		:= c03_w.seq_proc;	
		lista_proc_w(j).vl_proc			:= c03_w.vl_proc;
		lista_proc_w(j).qt_proc			:= c03_w.qt_proc;
		lista_proc_w(j).cd_proc			:= c03_w.cd_proc;
		lista_proc_w(j).ie_origem		:= c03_w.ie_origem;
		lista_proc_w(j).cd_tuss			:= c03_w.cd_tuss;
		lista_proc_w(j).cd_proc_convenio	:= somente_numero(c03_w.cd_proc_convenio);
		lista_proc_w(j).nr_conta		:= c03_w.nr_conta;

		j := j + 1;
	end loop;
	close C03;

	end loop;
close C06;

open C01;
loop
fetch C01 into
	nr_interno_conta_aqr_w, -- NRO DA CONTA
	nr_doc_conv_number_w,  --NRO DA GUIA
	cd_item_w,  --CODIGO ITEM NO TASY
	ds_item_retorno_w, --CODIGO ITEM EQUIVALENTE
	ds_observacao_w,  --JUSTIFICATIVA TEXTO LIVRE
	vl_glosa_char_w,  --VALOR APRESENTADO ITEM
	vl_aceite_char_w, ----VALOR ACEITE (hospital esta aceitando a glosa)
	vl_recebido_char_w,  --VL_RECEBIDO
	cd_motivo_glosa_w,-- CODIGO MOTIVO GLOSA CONV
	ie_acao_w,--ACAO DA GLOSA
	qt_item_w,
	vl_recursado_char_w,
	vl_glosa_inf_char_w; --QUANTIDADE COBRADA
exit when C01%notfound;
	ds_inconsistencia_w := null;
	ie_material_w := 'N';
	ie_proced_w   := 'N';
	begin
		cd_autorizacao_w := nr_doc_conv_number_w;
	exception
	when others then
		Raise_application_error(-20011,'Numero da guia fora do padrao.');
	end;

	begin
	select 	a.cd_autorizacao
	into	nr_doc_convenio_origem_w
	from	conta_paciente_guia a
	where	a.nr_interno_conta = nr_interno_conta_aqr_w
	and	trim(a.cd_autorizacao) = trim(cd_autorizacao_w);
	exception	
		when others then
		nr_doc_convenio_origem_w := null;
	end;

	if	(nr_doc_convenio_origem_w is not null) then
		cd_autorizacao_w := nr_doc_convenio_origem_w;
	end if;


	begin
		vl_pago_w	:= nvl(to_number(vl_pago_char_w),0);
		if(vl_pago_w = 0) then
			vl_pago_w := nvl(to_number(vl_recebido_char_w),0);
		end if;

		vl_glosa_aceita_w 	:= nvl(to_number(vl_aceite_char_w),0);

		vl_glosa_w		:= nvl(to_number(vl_glosa_char_w),0); --cobrado

		vl_recursado_w		:= nvl(to_number(vl_recursado_char_w),0);

		vl_glosa_informada_w	:= nvl(to_number(vl_glosa_inf_char_w),0);

	exception
	when others then
		vl_pago_w		:= 0;
		vl_glosa_w		:= 0;
		vl_glosa_aceita_w 	:= 0;
		vl_recursado_w	 	:= 0;
	end;

	nr_interno_Conta_w := nr_interno_conta_aqr_w;

	lista_importacao_w(i).nr_conta 		:= nr_interno_conta_w;
	lista_importacao_w(i).nr_guia		:= cd_autorizacao_w;
	lista_importacao_w(i).cd_item		:= somente_numero(cd_item_w);
	lista_importacao_w(i).ds_item_imp	:= ds_item_retorno_w;
	lista_importacao_w(i).ds_observacao	:= ds_observacao_w;
	lista_importacao_w(i).vl_glosa_aceita	:= vl_glosa_aceita_w;
	lista_importacao_w(i).vl_pago		:= vl_pago_w;
	lista_importacao_w(i).vl_glosa_informada:= vl_glosa_informada_w;
	lista_importacao_w(i).vl_amenor		:= vl_recursado_w;
	lista_importacao_w(i).cd_motivo_glosa	:= cd_motivo_glosa_w;
	lista_importacao_w(i).ie_acao_glosa	:= ie_acao_w;
	lista_importacao_w(i).qt_item		:= qt_item_w;
	lista_importacao_w(i).nr_seq_material	:= null;
	lista_importacao_w(i).nr_seq_proced	:= null;
	lista_importacao_w(i).vl_cobrado	:= vl_glosa_w; --cobrado
	lista_importacao_w(i).nr_item		:= i;

	i := i + 1;



	end loop;
close C01;


ds_inconsistencia_w := null;


i	:= 0;

nr_interno_conta_w := 0;
for i in 0..lista_importacao_w.count - 1 loop
	ie_proced_w 	:= 'N';
	ie_material_w	:= 'N';
	ds_inconsistencia_w := null;
	j 	:= 0;
	x	:= 0;
	select	Nvl(count(*),0)
	into	qt_guia_w
	from	conta_paciente_Guia
	where	nr_interno_conta = lista_importacao_w(i).nr_conta
	and	cd_autorizacao = lista_importacao_w(i).nr_guia;

	if	(qt_guia_w = 0) then
		begin
		ds_inconsistencia_w	:= 'A guia: ' || cd_autorizacao_w || ' nao foi encontrada na conta: ' || lista_importacao_w(i).nr_conta;
		end;end if;


	qt_item_acumulado_w := 0;
	vl_item_acumulado_w := 0;
	nr_maior_seq_positiva_w := 0;
	count_seq_positiva_w := 0;

	for j in 0..lista_proc_w.count - 1 loop

		if 	(lista_importacao_w(i).nr_conta = lista_proc_w(j).nr_conta) and	
			((lista_importacao_w(i).cd_item  = lista_proc_w(j).cd_proc) or
            (lista_importacao_w(i).cd_item  = lista_proc_w(j).cd_tuss) or
            (lista_importacao_w(i).cd_item  = lista_proc_w(j).cd_proc_convenio)) and
			(lista_importacao_w(i).nr_seq_proced is null) and
			(lista_proc_w(j).nr_item_vinculado is null) then

			if (lista_proc_w(j).qt_proc > 0) then
				qt_item_acumulado_w 	:= qt_item_acumulado_w + lista_proc_w(j).qt_proc;
				vl_item_acumulado_w 	:= vl_item_acumulado_w + lista_proc_w(j).vl_proc;

				if (lista_proc_w(j).seq_proc > nr_maior_seq_positiva_w) then
					nr_maior_seq_positiva_w := lista_proc_w(j).seq_proc;
					count_seq_positiva_w := j;
				end if;
			end if;

			if (lista_importacao_w(i).vl_cobrado = lista_proc_w(j).vl_proc) then
				lista_importacao_w(i).nr_seq_proced := lista_proc_w(j).seq_proc;
				lista_proc_w(j).nr_item_vinculado  := lista_importacao_w(i).nr_item;
			end if;

		end if;

	end loop;

	if 	(lista_importacao_w(i).nr_seq_proced is null)and	
		(nr_maior_seq_positiva_w > 0) then
		lista_importacao_w(i).nr_seq_proced := nr_maior_seq_positiva_w;
		lista_proc_w(count_seq_positiva_w).nr_item_vinculado  := lista_importacao_w(i).nr_item;
	end if;

	qt_item_acumulado_w := 0;
	vl_item_acumulado_w := 0;
	nr_maior_seq_positiva_w := 0;
	count_seq_positiva_w := 0;

	for x in 0..lista_mat_w.count - 1 loop

		if 	(lista_importacao_w(i).nr_conta = lista_mat_w(x).nr_conta) and	
			((lista_importacao_w(i).cd_item  = lista_mat_w(x).cd_material) or
            ((lista_importacao_w(i).cd_item  = lista_mat_w(x).cd_mat_convenio) or
            (lista_importacao_w(i).cd_item  = lista_mat_w(x).cd_material_tiss) ) ) and
			(lista_importacao_w(i).nr_seq_material is null) and
			(lista_mat_w(x).nr_item_vinculado is null) then

			if (lista_mat_w(x).qt_mat > 0) then
				qt_item_acumulado_w 	:= qt_item_acumulado_w + lista_mat_w(x).qt_mat;
				vl_item_acumulado_w 	:= vl_item_acumulado_w + lista_mat_w(x).vl_mat;

				if (lista_mat_w(x).seq_mat > nr_maior_seq_positiva_w) then
					nr_maior_seq_positiva_w := lista_mat_w(x).seq_mat;
					count_seq_positiva_w := x;
				end if;
			end if;

			if (lista_importacao_w(i).vl_cobrado = lista_mat_w(x).vl_mat) then
				lista_importacao_w(i).nr_seq_material := lista_mat_w(x).seq_mat;
				lista_mat_w(x).nr_item_vinculado  := lista_importacao_w(i).nr_item;
			end if;

		end if;

	end loop;

	if 	(lista_importacao_w(i).nr_seq_material is null)and	
		(nr_maior_seq_positiva_w > 0) then
		lista_importacao_w(i).nr_seq_material := nr_maior_seq_positiva_w;
		lista_mat_w(count_seq_positiva_w).nr_item_vinculado  := lista_importacao_w(i).nr_item; 
	end if;

	if (lista_importacao_w(i).nr_seq_proced is null and lista_importacao_w(i).nr_seq_material is null) then 
		for j in 0..lista_proc_w.count - 1 loop
			if 	(lista_importacao_w(i).nr_conta = lista_proc_w(j).nr_conta) and	
				((lista_importacao_w(i).cd_item  = lista_proc_w(j).cd_proc) or
                 (lista_importacao_w(i).cd_item  = lista_proc_w(j).cd_tuss) or
                 (lista_importacao_w(i).cd_item  = lista_proc_w(j).cd_proc_convenio)) then

				if (lista_proc_w(j).qt_proc > 0) then
					if (lista_proc_w(j).seq_proc > nr_maior_seq_positiva_w) then
						nr_maior_seq_positiva_w := lista_proc_w(j).seq_proc;
					end if;
				end if;
			end if;
		end loop;
		if (nr_maior_seq_positiva_w > 0) then
			lista_importacao_w(i).nr_seq_proced := nr_maior_seq_positiva_w;
        end if;
	end if;

	if (lista_importacao_w(i).nr_seq_proced is null and lista_importacao_w(i).nr_seq_material is null) then 
		for x in 0..lista_mat_w.count - 1 loop
			if 	(lista_importacao_w(i).nr_conta = lista_mat_w(x).nr_conta) and	
				((lista_importacao_w(i).cd_item  = lista_mat_w(x).cd_material) or
                (lista_importacao_w(i).cd_item  = lista_mat_w(x).cd_mat_convenio) or 
                (lista_importacao_w(i).cd_item  = lista_mat_w(x).cd_material_tiss)) then

				if (lista_mat_w(x).qt_mat > 0) then
					if (lista_mat_w(x).seq_mat > nr_maior_seq_positiva_w) then
						nr_maior_seq_positiva_w := lista_mat_w(x).seq_mat;
					end if;
				end if;
			end if;
		end loop;
		if (nr_maior_seq_positiva_w > 0) then
			lista_importacao_w(i).nr_seq_material := nr_maior_seq_positiva_w;
        end if;    
	end if;

	if	(qt_guia_w > 0) then

	begin
		vl_saldo_w := obter_saldo_conpaci(lista_importacao_w(i).nr_conta,lista_importacao_w(i).nr_guia);
	exception
	when others then
		vl_saldo_w := 0;
	end;
	
	select	Nvl(count(*),0)
	into	qt_retorno_w
	from	convenio_retorno_item a
	where	a.nr_interno_conta = lista_importacao_w(i).nr_conta
	and	a.cd_autorizacao	= lista_importacao_w(i).nr_guia;

	if	 (vl_saldo_w > 0) then

		dbms_output.put_line(i || ' - Conta: '||lista_importacao_w(i).nr_conta);

		if	(qt_retorno_w > 0) then

			if	(Nvl(lista_importacao_w(i).nr_seq_material,0) > 0) then
				ie_material_w := 'S';
			end if;

			if	(Nvl(lista_importacao_w(i).nr_seq_proced,0) > 0) then
				ie_proced_w := 'S';
			end if;

			insert into lote_audit_hist_imp
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_lote_audit,
				nr_interno_conta,
				cd_item,
				ds_complemento,
				vl_pago,
				vl_glosa,
				vl_glosa_informada,
				nr_doc_convenio,
				nr_seq_propaci,
				nr_seq_matpaci,
				ds_item_retorno,
				qt_item,
				ie_acao_glosa_imp,
				cd_motivo_glosa_imp,
				ds_inconsistencia,
				vl_amenor,
				vl_cobrado)
			values	(lote_audit_hist_imp_seq.nextval,
				sysdate,
				'Tasy_Imp',
				sysdate,
				'Tasy_Imp',
				nr_seq_lote_hist_p,
				lista_importacao_w(i).nr_conta,
				lista_importacao_w(i).cd_item,
				lista_importacao_w(i).ds_observacao,
				lista_importacao_w(i).vl_pago,
				lista_importacao_w(i).vl_glosa_aceita,
				lista_importacao_w(i).vl_glosa_informada,
				lista_importacao_w(i).nr_guia,
				lista_importacao_w(i).nr_seq_proced,
				lista_importacao_w(i).nr_seq_material,
				lista_importacao_w(i).ds_item_imp,
				lista_importacao_w(i).qt_item,
				lista_importacao_w(i).ie_acao_glosa,
				lista_importacao_w(i).cd_motivo_glosa,
				ds_inconsistencia_w,
				lista_importacao_w(i).vl_amenor,
				lista_importacao_w(i).vl_cobrado);
		else	
			ds_inconsistencia_w	:= 'Conta ainda nao passou pelo processo da funcao Retorno convenio: '||nr_interno_conta_aqr_w;
		end if;
	else
		ds_inconsistencia_w	:= 'Guia sem saldo: '|| nr_interno_conta_aqr_w ||' - Guia: '|| cd_autorizacao_w;
	end if;



	end if;

	if	((ds_inconsistencia_w is null) and
		((ie_proced_w = 'N') and	
		(ie_material_w = 'N'))) then
		ds_inconsistencia_w := 'Nao foi possivel encontrar o item na Conta paciente: '||nr_interno_conta_aqr_w||' - Guia: '||cd_autorizacao_w || ' Item: ' || cd_item_w;
	end if;	

	if	(ds_inconsistencia_w is not null) then
		insert into lote_audit_hist_imp
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_lote_audit,
			nr_interno_conta,
			cd_item,
			ds_complemento,
			vl_pago,
			vl_glosa,
			vl_glosa_informada,
			nr_doc_convenio,
			nr_seq_propaci,
			nr_seq_matpaci,
			ds_item_retorno,
			qt_item,
			ie_acao_glosa_imp,
			cd_motivo_glosa_imp,
			ds_inconsistencia,
			vl_amenor,
			vl_cobrado)
		values	(lote_audit_hist_imp_seq.nextval,
			sysdate,
			'Tasy_Imp',
			sysdate,
			'Tasy_Imp',
			nr_seq_lote_hist_p,
			lista_importacao_w(i).nr_conta,
			lista_importacao_w(i).cd_item,
			lista_importacao_w(i).ds_observacao,
			lista_importacao_w(i).vl_pago,
			lista_importacao_w(i).vl_glosa_aceita,
			lista_importacao_w(i).vl_glosa_informada,
			lista_importacao_w(i).nr_guia,
			null,
			null,
			lista_importacao_w(i).ds_item_imp,
			lista_importacao_w(i).qt_item,
			lista_importacao_w(i).ie_acao_glosa,
			lista_importacao_w(i).cd_motivo_glosa,
			ds_inconsistencia_w,
			lista_importacao_w(i).vl_amenor,
			lista_importacao_w(i).vl_cobrado);
	end if;



end loop;



delete 	from w_conv_ret_movto
where	nr_seq_lote_hist = nr_seq_lote_hist_p; 

/******************* PROCESSO DE GERAcAO DAS GUIAS E ITENS **************************************/


select	max(a.cd_convenio)	
into	cd_convenio_w	
from	lote_auditoria a,
	lote_audit_hist b
where	a.nr_sequencia	= b.nr_seq_lote_audit
and	b.nr_sequencia	= nr_seq_lote_hist_p;

select	count(1)
into	count_w
from	convenio_motivo_glosa
where	cd_convenio = cd_convenio_w;

if (count_w > 0)  then
	ie_motivo_glosa_convenio_w := 'S';
end if;

open C04;
loop
fetch C04 into	
	nr_interno_conta_w,
	cd_autorizacao_w;
exit when C04%notfound;
	begin

	vl_saldo_w	:= to_number(obter_saldo_conpaci(nr_interno_conta_w,cd_autorizacao_w));

	--if	(vl_saldo_w > 0) then

	select	max(nr_sequencia)
	into  	nr_seq_guia_w
	from	lote_audit_hist_guia
	where	nr_interno_conta	= nr_interno_conta_w
	and	cd_autorizacao		= cd_autorizacao_w
	and	nr_seq_lote_hist	= nr_seq_lote_hist_p;
	
	select	nvl(count(*), 0)
	into	qt_reg_conpagu_w
	from	conta_paciente_guia
	where	nr_interno_Conta 	= nr_interno_conta_w
	and	cd_autorizacao 		= cd_autorizacao_w;

	if	(nr_seq_guia_w is null) and (qt_reg_conpagu_w > 0) then

		select	lote_audit_hist_guia_seq.nextval
		into	nr_seq_guia_w
		from	dual;
		begin
			insert into lote_audit_hist_guia
				(cd_autorizacao,
				dt_atualizacao,
				nm_usuario,
				nr_interno_conta,
				nr_seq_lote_hist,
				nr_sequencia,
				vl_saldo_guia)
			values	(cd_autorizacao_w,
				sysdate,
				nm_usuario_w,
				nr_interno_conta_w,
				nr_seq_lote_hist_p,
				nr_seq_guia_w,
				vl_saldo_w);
		exception
		when others then
			ds_erro_w := ds_erro_w + 'Conta: '||nr_interno_conta_w||' Guia: '|| cd_autorizacao_w  || sqlerrm ;
		end;

		update	lote_audit_hist_imp
		set	nr_seq_guia		= nr_seq_guia_w
		where	nr_interno_conta	= nr_interno_conta_w
		and	nr_doc_convenio		= cd_autorizacao_w;

	else
		if (qt_reg_conpagu_w = 0) then
			update  lote_audit_hist_imp
			set	    nr_seq_guia		= nr_seq_guia_w,
                    ds_inconsistencia	= 'A guia: ' || cd_autorizacao_w || ' nao foi encontrada na conta: ' || nr_interno_Conta_w || '.'
			where	nr_interno_conta	= nr_interno_conta_w
			and	    nr_doc_convenio		= cd_autorizacao_w;
		else
			update	lote_audit_hist_imp
			set	    nr_seq_guia		= nr_seq_guia_w
			where	nr_interno_conta	= nr_interno_conta_w
			and	    nr_doc_convenio		= cd_autorizacao_w;
		end if;		
		
	end if;



	open C05;
	loop
	fetch C05 into	
		nr_sequencia_w,
		nr_seq_propaci_w,
		nr_seq_matpaci_w,
		vl_pago_w,
		vl_glosa_w,
		qt_item_w,
		vl_glosa_informada_w,
		vl_amenor_w,
		cd_motivo_glosa_w,
		ie_acao_glosa_w,
		ds_complemento_w,
		nr_seq_partic_w;
	exit when C05%notfound;
		begin

		begin
			if (ie_motivo_glosa_convenio_w = 'S') then
				select	x.cd_motivo_glosa,
					y.cd_motivo_tiss,
					z.cd_resposta
				into	cd_motivo_glosa_insert_w,
					cd_motivo_tiss_insert_w,
					cd_resposta_glosa_w
				from	convenio_motivo_glosa x,
					tiss_motivo_glosa y,
					motivo_glosa z
				where	x.cd_motivo_glosa 	= y.cd_motivo_glosa (+)
				and	x.cd_motivo_glosa 	= z.cd_motivo_glosa (+)
				and	x.cd_glosa_convenio 	= cd_motivo_glosa_w
				and	x.cd_convenio 		= cd_convenio_w;
			else
				select	z.cd_motivo_glosa,
					y.cd_motivo_tiss,
					z.cd_resposta
				into	cd_motivo_glosa_insert_w,
					cd_motivo_tiss_insert_w,
					cd_resposta_glosa_w
				from	tiss_motivo_glosa y,
					motivo_glosa z
				where	z.cd_motivo_glosa 	= y.cd_motivo_glosa (+)
				and	z.cd_motivo_glosa 	= cd_motivo_glosa_w;			
			end if;
		exception
		when others then
			cd_motivo_glosa_insert_w := null;
			cd_motivo_tiss_insert_w	:= null;
			cd_resposta_glosa_w := null;
		end;		
		begin
		insert into lote_audit_hist_item
			(dt_atualizacao,
			dt_atualizacao_nrec,
			dt_historico,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_guia,				
			nr_seq_lote,
			nr_seq_matpaci,
			nr_seq_propaci,
			nr_sequencia,
			qt_item,
			vl_adicional,
			vl_amenor,
			vl_glosa,
			vl_glosa_informada,
			vl_pago,
			vl_saldo,
			vl_saldo_amenor,
			cd_motivo_glosa,
			cd_motivo_glosa_tiss,
			cd_resposta,
			ie_acao_glosa,
			ds_observacao,
			nr_seq_partic)
		values	(sysdate,
			sysdate,
			sysdate,
			nm_usuario_w,
			nm_usuario_w,
			nr_seq_guia_w,
			nr_seq_lote_hist_p,
			nr_seq_matpaci_w,
			nr_seq_propaci_w,
			lote_audit_hist_item_seq.nextval,
			qt_item_w,
			0, -- vl_adicional
			vl_amenor_w, --vl_amenor
			vl_glosa_w, --vl_glosa aceita
			vl_glosa_informada_w,
			vl_pago_w,
			vl_glosa_informada_w, --vl_saldo
			null, --vl_saldo_amenor
			cd_motivo_glosa_insert_w,
			cd_motivo_tiss_insert_w,
			cd_resposta_glosa_w,
			ie_acao_glosa_w,
			ds_complemento_w,
			nr_seq_partic_w)
		returning nr_sequencia into nr_seq_w;
		exception
			when others then
			ds_erro_w := ds_erro_w + 'Conta: '||nr_interno_conta_w||' Guia: '|| cd_autorizacao_w || ' Proc : '|| nr_seq_propaci_w ||' Mat: ' || nr_seq_matpaci_w || sqlerrm ;
		end;


		end;
	end loop;
	close C05;		

	end;
end loop;
close C04;


exception
when others then
	delete 	from w_conv_ret_movto
	where	nr_seq_lote_hist = nr_seq_lote_hist_p;
	commit;
end;
commit;


if	(ds_erro_w is not null) then
	raise_application_error(-20011, substr(ds_erro_w,1,2000));
end if;
end BP_IMPORTAR_ITENS_GRG;
/
