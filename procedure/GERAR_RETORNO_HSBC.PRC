create or replace
procedure GERAR_RETORNO_HSBC(	nr_seq_banco_escrit_p	number,
				nm_usuario_p		varchar2) is

/* CNAB 240 posi��es */

ds_dt_liquidacao_w	varchar2(255);
ds_vl_liquidacao_w	varchar2(255);
cd_ocorrencia_w		varchar2(255);
cd_arquivo_w		varchar2(255);

cd_retorno_liq_w	varchar2(50);

nr_titulo_w		number(16,0);
cd_tipo_baixa_w		number(10,0);
nr_seq_trans_escrit_w	number(10,0);
nr_seq_conta_banco_w	number(10,0);
nr_sequencia_w		number(10,0);
dt_liquidacao_w		date;
vl_liquidacao_w		number(15,2);
qt_reg_w		number(10);
cd_estabelecimento_w	number(10,0);
vl_escritural_w		number(15,2);
cd_tipo_baixa_padrao_w	number(5);
ds_transacao_w		varchar2(255);

cursor c01 is
select	somente_numero(substr(ds_string,74,16))	nr_titulo,
	substr(ds_string,94,8)			ds_dt_liquidacao,
	somente_numero(substr(ds_string,122,13))	ds_vl_liquidacao,
	substr(ds_string,231,2)			cd_ocorrencia
from	w_retorno_banco
where	substr(ds_string,8,1)		= '3'
and	substr(ds_string,14,1)	= 'A'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p;

cursor c02 is
select	somente_numero(substr(ds_string,183,20))	nr_titulo,
	substr(ds_string,145,8)			ds_dt_liquidacao,
	somente_numero(substr(ds_string,155,13))	ds_vl_liquidacao,
	substr(ds_string,231,2)			cd_ocorrencia
from	w_retorno_banco
where	substr(ds_string,8,1)		= '3'
and	substr(ds_string,14,1)	= 'J'
and	nr_seq_banco_escrit		= nr_seq_banco_escrit_p;

begin

begin
select	b.cd_retorno_liq,
	a.cd_estabelecimento,
	a.nr_seq_conta_banco
into	cd_retorno_liq_w,
	cd_estabelecimento_w,
	nr_seq_conta_banco_w
from	banco_retorno_cp b,
	banco_escritural a
where	a.cd_banco		= b.cd_banco
and	a.nr_sequencia		= nr_seq_banco_escrit_p;
exception
	when no_data_found then
	/* N�o foi encontrado o c�digo de retorno da liquida��o!
	Verifique o cadastro de "Retorno CP" no cadastro de bancos */
	wheb_mensagem_pck.exibir_mensagem_abort(200005);
end;

select	max(a.nr_seq_trans_escrit),
	max(b.ds_transacao)
into	nr_seq_trans_escrit_w,
	ds_transacao_w
from	transacao_financeira b,
	parametro_tesouraria a
where	a.nr_seq_trans_escrit	= b.nr_sequencia
and	a.cd_estabelecimento	= cd_estabelecimento_w;

if	(nr_Seq_trans_escrit_w is not null) then

	select	max(cd_tipo_baixa)
	into	cd_tipo_baixa_w
	from	transacao_financeira
	where	nr_sequencia = nr_seq_trans_escrit_w;

	if	(cd_tipo_baixa_w	is null) then

		/* N�o foi localizado tipo de baixa para a transa��o ds_transacao_w */
		wheb_mensagem_pck.exibir_mensagem_abort(200002,'DS_TRANSACAO=' || ds_transacao_w);

	end if;

end if;

if	(cd_tipo_baixa_w	is null) then

	select	nvl(max(a.cd_tipo_baixa_padrao), 1)
	into	cd_tipo_baixa_w
	from	parametros_contas_pagar a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;

end if;

select	substr(max(ds_string),143,1)
into	cd_arquivo_w
from 	w_retorno_banco
where	substr(ds_string,8,1) = '0'
and	substr(ds_string,4,4) = '0000'
and	nr_seq_banco_escrit	= nr_seq_banco_escrit_p;

if	(cd_arquivo_w	= '2')	then
	open c01;
	loop
	fetch c01 into
		nr_titulo_w,
		ds_dt_liquidacao_w,
		ds_vl_liquidacao_w,
		cd_ocorrencia_w;
	exit when c01%notfound;

		dt_liquidacao_w		:= to_date(ds_dt_liquidacao_w);
		vl_liquidacao_w		:= to_number(ds_vl_liquidacao_w);
		vl_liquidacao_w		:= dividir_sem_round(ds_vl_liquidacao_w,100);

		select	count(*)
		into	qt_reg_w
		from	titulo_pagar_escrit
		where	nr_seq_escrit	= nr_seq_banco_escrit_p
		and	nr_titulo			= nr_titulo_w;

		if	(qt_reg_w = 0) then
			gerar_titulo_escritural(nr_titulo_w,nr_seq_banco_escrit_p,nm_usuario_p);

			qt_reg_w	:= 1;
		end if;


		if	(cd_retorno_liq_w = cd_ocorrencia_w) and	
			(qt_reg_w	> 0)	then
			
			select	vl_escritural
			into	vl_escritural_w
			from	titulo_pagar_escrit
			where	nr_seq_escrit	= nr_seq_banco_escrit_p
			and	nr_titulo	= nr_titulo_w;
			/* Colocado este tratamento para n�o efetuar baixa com valor maior que o t�tulo */
			if	(vl_liquidacao_w	> vl_escritural_w) then
				vl_liquidacao_w	:= vl_escritural_w;
			end if;
			baixa_titulo_pagar
					(cd_estabelecimento_w,
					cd_tipo_baixa_w,
					nr_titulo_w,
					vl_liquidacao_w,
					nm_usuario_p,
					nr_seq_trans_escrit_w,
					null,
					nr_seq_banco_escrit_p,
					dt_liquidacao_w,
					nr_seq_conta_banco_w);

			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	titulo_pagar_baixa
			where	nr_titulo	= nr_titulo_w;

			gerar_movto_tit_baixa
					(nr_titulo_w,
					nr_sequencia_w,
					'P',
					nm_usuario_p,
					'N');

			atualizar_saldo_tit_pagar(nr_titulo_w, nm_usuario_p);
			Gerar_W_Tit_Pag_imposto(nr_titulo_w, nm_usuario_p);
		else
			update	titulo_pagar_escrit
			set	ds_erro			= cd_ocorrencia_w
			where	nr_seq_escrit		= nr_seq_banco_escrit_p
			and	nr_titulo		= nr_titulo_w;
		end if;
	
	
	end loop;
	close c01;

	Open c02;
	loop
	fetch c02 into
		nr_titulo_w,
		ds_dt_liquidacao_w,
		ds_vl_liquidacao_w,
		cd_ocorrencia_w;
	exit when c02%notfound;
		
		dt_liquidacao_w		:= to_date(ds_dt_liquidacao_w);
		vl_liquidacao_w		:= to_number(ds_vl_liquidacao_w);
		vl_liquidacao_w		:= dividir_sem_round(ds_vl_liquidacao_w,100);
		
		select	count(*)
		into	qt_reg_w
		from	titulo_pagar_escrit
		where	nr_seq_escrit	= nr_seq_banco_escrit_p
		and		nr_titulo			= nr_titulo_w;

		if	(qt_reg_w = 0) then
			gerar_titulo_escritural(nr_titulo_w,nr_seq_banco_escrit_p,nm_usuario_p);

			qt_reg_w	:= 1;
		end if;
		
		if	(cd_retorno_liq_w = cd_ocorrencia_w) and	
			(qt_reg_w	> 0)	then

			select	vl_escritural
			into	vl_escritural_w
			from	titulo_pagar_escrit
			where	nr_seq_escrit	= nr_seq_banco_escrit_p
			and		nr_titulo			= nr_titulo_w;
		
			/*Colocado este tratamento para n�o efetuar baixa com valor maior que o t�tulo - Feltrin OS88049*/
			if	(vl_liquidacao_w	> vl_escritural_w) then
				vl_liquidacao_w	:= vl_escritural_w;
			end if;

			baixa_titulo_pagar
					(cd_estabelecimento_w,
					cd_tipo_baixa_w,
					nr_titulo_w,
					vl_liquidacao_w,
					nm_usuario_p,
					nr_seq_trans_escrit_w,
					null,
					nr_seq_banco_escrit_p,
					dt_liquidacao_w,
					nr_seq_conta_banco_w);

			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	titulo_pagar_baixa
			where	nr_titulo	= nr_titulo_w;

			gerar_movto_tit_baixa
					(nr_titulo_w,
					nr_sequencia_w,
					'P',
					nm_usuario_p,
					'N');

			atualizar_saldo_tit_pagar(nr_titulo_w, nm_usuario_p);
			Gerar_W_Tit_Pag_imposto(nr_titulo_w,nm_usuario_p);
		else
			update	titulo_pagar_escrit
			set	ds_erro			= cd_ocorrencia_w
			where	nr_seq_escrit		= nr_seq_banco_escrit_p
			and	nr_titulo		= nr_titulo_w;
		end if;
	
	end loop;
	close c02;
else
	/* O arquivo que est� sendo importado n�o � de retorno, favor verifique! */
	wheb_mensagem_pck.exibir_mensagem_abort(200003);
end if;

commit;

end GERAR_RETORNO_HSBC;
/
