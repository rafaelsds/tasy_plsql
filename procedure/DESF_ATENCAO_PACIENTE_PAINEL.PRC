Create or replace
PROCEDURE desf_atencao_paciente_painel (  nr_seq_agenda_p     NUMBER)
					IS


BEGIN


if	(nr_seq_agenda_p >  0) THEN
	UPDATE	agenda_paciente
	SET 	ie_atencao = null
	WHERE 	nr_sequencia = nr_seq_agenda_p;
	
END IF;

COMMIT;

END desf_atencao_paciente_painel;
/