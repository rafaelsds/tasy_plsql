create or replace
procedure import_sus_aih_proc_cbo(cd_procedimento_p		number,
			cd_cbo_p	varchar2,
			ie_opcao_p	varchar2,
			nr_sequencia_p	number,
			nm_usuario_p		varchar2) is
 		 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Importar dados AIH
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
ie_opcao_p
DE - atualizacao_padrao_parametros(delete) 
IN - inserir dado tabela sus_procedimento_cbo
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
nr_sequencia_w	sus_procedimento_cbo.nr_sequencia%type;

begin
		
if	(upper(ie_opcao_p) = 'DE')then

	delete	from sus_procedimento_cbo 
	where	Obter_Situacao_Procedimento(cd_procedimento,ie_origem_proced) = 'A' 
	and	((ie_aih = 'S') or (ie_aih is null and ie_bpa is null));

elsif	(upper(ie_opcao_p) = 'IN') and
	(cd_cbo_p is not null) and
	(cd_procedimento_p is not null) then 	
		
		select	sus_procedimento_cbo_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		insert	into sus_procedimento_cbo 
				(cd_procedimento, 
				ie_origem_proced, 
				cd_cbo, 
				dt_atualizacao, 
				nm_usuario, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				nr_sequencia, 
				ie_aih) 
		values	(cd_procedimento_p, 
				7, 
				cd_cbo_p, 
				sysdate, 
				nm_usuario_p, 
				sysdate, 
				nm_usuario_p, 
				nr_sequencia_w,
				'S');
	
end if;
commit;
end import_sus_aih_proc_cbo;
/
