create or replace
procedure pepo_atualizar_status_doc(    nr_cirurgia_p pepo_doc_status.nr_cirurgia%type,
                                        cd_item_p pepo_doc_status.cd_item%type,
                                        ie_status_p pepo_doc_status.ie_status%type,
                                        nm_usuario_p pepo_doc_status.nm_usuario%type,
					                    nr_seq_pepo_p pepo_doc_status.nr_seq_pepo%type) as
begin
    if(nr_cirurgia_p is not null) then
        update  pepo_doc_status
        set     ie_status = ie_status_p,
                nm_usuario = nm_usuario_p,
                dt_atualizacao = sysdate
        where   nr_cirurgia = nr_cirurgia_p
        and     cd_item = cd_item_p;
        commit;
    end if;
    
    if(nr_seq_pepo_p is not null and nr_cirurgia_p is null) then 
        update  pepo_doc_status
        set     ie_status = ie_status_p,
                nm_usuario = nm_usuario_p,
                dt_atualizacao = sysdate
        where   nr_seq_pepo = nr_seq_pepo_p
        and     cd_item = cd_item_p
		and     nr_cirurgia is null;
        commit;        
    end if;

end pepo_atualizar_status_doc;
/