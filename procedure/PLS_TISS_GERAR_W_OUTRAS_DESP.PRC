create or replace 
procedure pls_tiss_gerar_w_outras_desp( nr_seq_conta_p        pls_conta.nr_sequencia%type,
					ds_dir_padrao_p       varchar2,
					cd_estabelecimento_p  estabelecimento.cd_estabelecimento%type,
					nm_usuario_p          usuario.nm_usuario%type) is

cd_guia_w                 	varchar2(20);
cd_guia_principal_w       	Varchar2(20);
cd_ans_w                  	varchar2(20);
cd_senha_w                	varchar2(20);
dt_autorizacao_w          	date;
dt_validade_senha_w       	date;
dt_atend_ref_w   	  	date;
nr_seq_plano_w            	number(10);
nr_seq_segurado_w         	number(10);
cd_medico_executor_w      	varchar2(10);
nr_seq_prestador_w        	number(10);
cd_usuario_plano_w        	varchar2(30);
dt_validade_carteira_w    	date;
cd_pessoa_fisica_w        	varchar2(10);
ds_plano_w                	varchar2(255);
ie_carater_internacao_w   	varchar2(1);
nr_seq_conta_w            	number(10);
nr_cartao_nac_sus_w       	varchar(60);
nm_pessoa_fisica_w        	varchar2(255);
sg_conselho_w             	varchar2(20);
cd_doenca_cid_w           	varchar2(10);
ds_diagnostico_w          	varchar2(2000);
cd_tabela_relat_w         	varchar2(2);
cd_porte_anestesico_w     	Varchar2(10);
ds_retorno_w              	Varchar2(255);
ds_indicacao_w            	Varchar2(255);
cgc_exec_w              	Varchar2(14);
cpf_exec_w              	Varchar2(50);
nm_contratado_exec_w    	Varchar2(255);
ds_logradouro_w         	Varchar2(255);
nm_municipio_w          	Varchar2(255);
sg_estado_w             	compl_pessoa_fisica.sg_estado%type;
cd_municipio_ibge_w     	Varchar2(15);
cd_cep_w                	Varchar2(50);
cd_cnes_w               	Varchar2(20);
qt_proc_conta_w         	number(10);
nr_seq_apresentacao_w		number(10);
nr_seq_conta_proc_w     	number(10);
cd_procedimento_w       	number(15);
ds_procedimento_w       	varchar2(255);
ie_origem_proced_w      	number(10);
qt_solicitada_w         	number(7,3);
qt_autorizada_w         	number(7,3);
nr_cpf_w                	varchar2(11);
nr_crm_w                	varchar2(20);
uf_crm_w                	medico.uf_crm%type;
cd_cbo_w                	varchar2(10);
nm_medico_solicitante_w 	varchar(255);
ds_observacao_w         	varchar2(4000);
vl_procedimento_w       	number(15,2);
vl_custo_oper_w         	number(15,2);
vl_anestesista_w        	number(15,2);
vl_medico_w             	number(15,2);
vl_filme_w              	number(15,2);
vl_auxiliares_w         	number(15,2);
nr_seq_regra_w          	number(10);
cd_edicao_amb_w         	number(6);
ie_valor_informado_w    	Varchar2(1)    := 'N';
nr_aux_regra_w            	Number(10);
vl_unitario_w                  	pls_conta_proc.vl_unitario%type;
dt_procedimento_w             	date    := null;
vl_liberado_w                  	number(15,2);
nr_seq_regra_autogerado_w       Number(10);
ie_tipo_acomodacao_ptu_w        varchar2(1);
nr_seq_tipo_atendimento_w       varchar2(5);
nr_seq_saida_spsadt_w           varchar2(5);
nr_seq_rp_combinada_w     	pls_rp_cta_combinada.nr_sequencia%type;
ie_indicacao_acidente_w         varchar2(10);
cd_moeda_autogerado_w    	Number(3);
vl_procedimentos_w    		pls_conta.vl_procedimentos%type;
vl_taxas_w      		pls_conta.vl_taxas%type;
vl_materiais_w      		pls_conta.vl_materiais%type;
vl_opm_w      			pls_conta.vl_opm%type;
vl_medicamentos_w    		pls_conta.vl_medicamentos%type;
vl_gases_w      		pls_conta.vl_gases%type;
vl_total_w      		pls_conta.vl_total%type;
vl_ch_honorarios_w    		cotacao_moeda.vl_cotacao%type;
vl_ch_custo_oper_w    		cotacao_moeda.vl_cotacao%type;
vl_ch_custo_filme_w    		cotacao_moeda.vl_cotacao%type;
vl_ch_anestesista_w    		cotacao_moeda.vl_cotacao%type;
dados_regra_preco_proc_w  	pls_cta_valorizacao_pck.dados_regra_preco_proc;
dados_conta_w      		pls_cta_valorizacao_pck.dados_conta;
cd_guia_referencia_w    	pls_conta.cd_guia_referencia%type;
hr_inicio_w      		pls_conta_proc.hr_inicio_proc%type;
hr_fim_w      			pls_conta_proc.hr_fim_proc%type;
ie_tipo_despesa_w    		pls_conta_mat.ie_tipo_despesa%type;
nr_seq_guia_w      		number(10,0);
nr_registro_anvisa_w		pls_conta_mat.nr_registro_anvisa%type;
cd_ref_fabricante_w		pls_conta_mat.cd_ref_fabricante%type;
ds_aut_funcionamento_w 		pls_conta_mat.ds_aut_funcionamento%type;
cd_unidade_medida_w		pls_conta_mat.cd_unidade_medida%type;
tx_reducao_acrescimo_w		pls_conta_mat.tx_reducao_acrescimo%type;
	
cursor c01 is
	select  	a.nr_sequencia nr_seq_item, 	to_char(a.cd_procedimento) cd_item, 	substr(obter_descricao_procedimento(a.cd_procedimento,a.ie_origem_proced),1,255) ds_item, 
			a.ie_origem_proced, 		a.qt_procedimento_imp qt_item, 		a.qt_procedimento_imp qt_item,
			a.vl_unitario_imp vl_unitario, 	a.dt_procedimento dt_item, 		(a.qt_procedimento_imp * a.vl_unitario_imp) vl_item,--vl_liberado,
			a.nr_seq_rp_combinada, 		null ie_tipo_despesa, 			a.hr_inicio_proc dt_hora_inicial,
			a.hr_fim_proc dt_hora_final, 	null nr_registro_anvisa, 		null cd_material_fabricante,
			null nr_autor_func_opme, 	null cd_unidade_medida, 		null tx_reducao_acrescimo
	from    	pls_conta_proc a
	where   	nr_seq_conta    = nr_seq_conta_p
	and		ie_tipo_despesa != '1'
	union all
	select  	b.nr_sequencia nr_seq_item, 	substr(pls_obter_seq_codigo_material(b.nr_seq_material, ''),1,255) cd_item, substr(obter_descricao_padrao('PLS_MATERIAL','DS_MATERIAL',nr_seq_material),1,255) ds_item,
			null ie_origem_proced,  	b.qt_material_imp qt_item, 		b.qt_material_imp qt_item,
			b.vl_unitario_imp vl_unitario,	b.dt_item_imp dt_item,			(b.qt_material_imp * b.vl_unitario_imp) vl_item,
			null nr_seq_rp_combinada,	b.ie_tipo_despesa,			b.hr_inicio_mat dt_hora_inicial, 
			b.hr_fim_mat dt_hora_final,  	b.nr_registro_anvisa, 			b.cd_ref_fabricante cd_material_fabricante,
			b.ds_aut_funcionamento nr_autor_func_opme, b.cd_unidade_medida, 	b.tx_reducao_acrescimo
	from 		pls_conta_mat b
	where  		nr_seq_conta    = nr_seq_conta_p;
    
/* Relat?rio utilizado no complemento de contas m?dicas, n?o pode restrindir por status.*/ 

begin

--insert into lrp values('pls_tiss_gerar_w_outras_desp - nr_seq_conta_p = '||nr_seq_conta_p);
--commit;

delete   from w_tiss_guia
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_dados_atendimento
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_beneficiario
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_proc_paciente
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_contratado_exec
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_totais
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_proc_solic
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_solicitacao
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_contratado_solic
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_relatorio
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_opm
where    nm_usuario        = nm_usuario_p;

delete   from w_tiss_opm_exec
where    nm_usuario        = nm_usuario_p;

delete  from w_tiss_outras_despesas
where  nm_usuario     = nm_usuario_p;

commit;


if    (nvl(nr_seq_conta_p,0) > 0) then

	select		a.cd_guia,		a.cd_senha, 			a.dt_autorizacao,
			a.dt_validade_senha,	a.dt_atendimento_referencia, 	pls_obter_produto_benef(b.nr_sequencia,a.dt_atendimento_referencia),
			a.nr_seq_segurado,	a.cd_medico_executor, 		a.nr_seq_prestador_exec,
			substr(pls_obter_dados_cart_segurado(a.nr_seq_segurado,'C'),1,30), to_date(pls_obter_dados_cart_segurado(a.nr_seq_segurado,'V'),'dd/mm/yyyy'), b.cd_pessoa_fisica,
			substr(pls_obter_dados_produto(b.nr_seq_plano,'N'),1,255) ds_plano, a.ie_carater_internacao, a.cd_guia,
			a.ds_observacao, 	substr(a.ds_indicacao_clinica,1,255), a.ie_tipo_acomodacao_ptu,
			substr(pls_obter_cd_tipo_atend_tiss(a.nr_seq_tipo_atendimento, a.cd_estabelecimento),1,5), a.nr_seq_saida_spsadt, a.ie_indicacao_acidente,
			a.vl_procedimentos_imp, a.vl_taxas, 			a.vl_materiais_imp,
			a.vl_opm,		a.vl_medicamentos,		a.vl_gases,
			a.vl_total_imp,		a.ie_tipo_consulta,		a.cd_guia_referencia,
			a.nr_seq_tipo_conta
		
	into    	cd_guia_w, 		cd_senha_w, 			dt_autorizacao_w,
			dt_validade_senha_w, 	dt_atend_ref_w, 		nr_seq_plano_w,
			nr_seq_segurado_w, 	cd_medico_executor_w, 		nr_seq_prestador_w,
			cd_usuario_plano_w, 	dt_validade_carteira_w, 	cd_pessoa_fisica_w,
			ds_plano_w, 		ie_carater_internacao_w, 	cd_guia_principal_w,
			ds_observacao_w, 	ds_indicacao_w, 		ie_tipo_acomodacao_ptu_w,
			nr_seq_tipo_atendimento_w, nr_seq_saida_spsadt_w, 	ie_indicacao_acidente_w,
			vl_procedimentos_w, 	vl_taxas_w, 			vl_materiais_w,
			vl_opm_w, 		vl_medicamentos_w, 		vl_gases_w,
			vl_total_w, 		dados_conta_w.ie_tipo_consulta, cd_guia_referencia_w,
			dados_conta_w.nr_seq_tipo_conta
	from    	pls_segurado b,
			pls_conta a
	where       	a.nr_sequencia       = nr_seq_conta_p
	and          	a.nr_seq_segurado    = b.nr_sequencia;

begin
  
select  	a.cd_ans
into    	cd_ans_w
from    	pls_plano b,
		pls_outorgante a
where       	a.nr_sequencia    = b.nr_seq_outorgante
and         	b.nr_sequencia    = nr_seq_plano_w;
exception
	when others then
		cd_ans_w    := null;
	end;

insert    into w_tiss_relatorio	(	nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, ds_arquivo_logo)
					
values 				(	w_tiss_relatorio_seq.nextval, sysdate, nm_usuario_p,
					sysdate,	nm_usuario_p, 	ds_dir_padrao_p || '\pls_logo.jpg');
              
    qt_proc_conta_w		:= 0;
    nr_seq_apresentacao_w       := 0;
    
select  w_tiss_guia_seq.nextval
into	nr_seq_guia_w
from  	dual;
    
insert  into 	w_tiss_guia		(	nr_sequencia, 	dt_atualizacao, nm_usuario,
						cd_ans,		cd_autorizacao, dt_autorizacao,
						cd_senha,	dt_validade_senha, dt_emissao_guia,
						nr_sequencia_autor, cd_autorizacao_princ, ds_observacao,
						ie_tiss_tipo_guia,  dt_entrada,	nr_guia_operadora)
						
	values    			( 	nr_seq_guia_w, 	sysdate, 	nm_usuario_p,
						cd_ans_w,	cd_guia_w,	dt_autorizacao_w,
						cd_senha_w,	dt_validade_senha_w, nvl(dt_atend_ref_w,sysdate),
						nr_seq_conta_p, cd_guia_referencia_w, ds_observacao_w,
						'2',		null,		nr_seq_prestador_w);  						
                                     
insert 	into 	w_tiss_dados_atendimento(	nr_sequencia,	dt_atualizacao,	nm_usuario,
							nr_seq_guia, 	nr_seq_conta,	ie_tipo_atendimento,
							ie_tipo_saida, 	ie_tipo_acidente)
							
	values    			(	w_tiss_dados_atendimento_seq.nextval, 	sysdate, nm_usuario_p,
							1,	nr_seq_conta_w,	nr_seq_tipo_atendimento_w,
							nr_seq_saida_spsadt_w,	ie_indicacao_acidente_w);
    
insert 	into 	w_tiss_totais 		(	nr_sequencia,	dt_atualizacao,	nm_usuario,
							nr_seq_conta,	nr_seq_guia,	vl_procedimentos,
							vl_taxas,	vl_materiais,	vl_total_geral_opm, 
							vl_medicamentos,vl_gases, 	vl_total_geral)
        values    			(	w_tiss_totais_seq.nextval, sysdate, nm_usuario_p,
							nr_seq_conta_w,		nr_seq_guia_w,	vl_procedimentos_w,
							vl_taxas_w, 	vl_materiais_w, vl_opm_w,
							vl_medicamentos_w, vl_gases_w,	vl_total_w);
        
insert   into 	w_tiss_beneficiario	(	nr_sequencia, 	  dt_atualizacao, 	nm_usuario,
						nr_seq_conta, 	  nr_seq_guia, 		cd_pessoa_fisica,
						nm_pessoa_fisica, nr_cartao_nac_sus, 	ds_plano,
						dt_validade_carteira, cd_usuario_convenio)
						
	values  			(	w_tiss_beneficiario_seq.nextval, sysdate, nm_usuario_p,
						nr_seq_conta_w,		1,	cd_pessoa_fisica_w,
						nm_pessoa_fisica_w,	nr_cartao_nac_sus_w,	ds_plano_w,
						dt_validade_carteira_w, cd_usuario_plano_w	);
      
    

    
  
open c01;
	loop
		fetch c01 into    
		nr_seq_conta_proc_w, 	cd_procedimento_w, 	ds_procedimento_w,
		ie_origem_proced_w, 	qt_solicitada_w, 	qt_autorizada_w,
		vl_unitario_w, 		dt_procedimento_w, 	vl_liberado_w,
		nr_seq_rp_combinada_w, 	ie_tipo_despesa_w, 	hr_inicio_w,
		hr_fim_w, 		nr_registro_anvisa_w, 	cd_ref_fabricante_w,
		ds_aut_funcionamento_w, cd_unidade_medida_w, 	tx_reducao_acrescimo_w;
  
	exit when c01%notfound;

        qt_proc_conta_w        := qt_proc_conta_w + 1;
        
	if    (qt_proc_conta_w = 1) then

      
		begin
			select  nr_cartao_nac_sus,
				substr(obter_nome_pf(cd_pessoa_fisica),1,255)
			into    nr_cartao_nac_sus_w,
				nm_pessoa_fisica_w
			from    pessoa_fisica
			where   cd_pessoa_fisica    = cd_pessoa_fisica_w;
			exception
			when others then
				nr_cartao_nac_sus_w    := null;
				nm_pessoa_fisica_w    := null;
		end;
		
		insert	into	w_tiss_contratado_exec	(	nr_sequencia,	dt_atualizacao,	nm_usuario,
								nr_seq_conta, 	cd_cgc, 	cd_interno,
								nr_cpf,		nm_contratado, 	ds_tipo_logradouro,
								ds_logradouro, 	nm_municipio, 	sg_estado,
								cd_municipio_ibge, cd_cep, 	cd_cnes,
								nr_seq_guia)
								
			values  			(	w_tiss_contratado_exec_seq.nextval, sysdate, nm_usuario_p,
								nr_seq_conta_w,	cgc_exec_w,	nr_seq_prestador_w,
								cpf_exec_w,	nm_contratado_exec_w,	'',
								ds_logradouro_w,nm_municipio_w,	sg_estado_w,
								cd_municipio_ibge_w, cd_cep_w, cd_cnes_w,
								1);
		
			
		begin
			select  b.nr_cpf, 	substr(obter_nome_pf(a.cd_pessoa_fisica),1,255), substr(obter_conselho_profissional(b.nr_seq_conselho,'S'),1,10),
				nr_crm,		uf_crm,						 substr(obter_descricao_padrao('CBO_SAUDE','CD_CBO',b.nr_seq_cbo_saude),1,10)
			into    nr_cpf_w,	nm_medico_solicitante_w, 			 sg_conselho_w,
				nr_crm_w,	uf_crm_w, 					cd_cbo_w 
			from    pessoa_fisica b,
				medico a
			where   a.cd_pessoa_fisica    = b.cd_pessoa_fisica
			and     a.cd_pessoa_fisica    = cd_medico_executor_w;
			exception
			when others then
				nr_cpf_w          	:= null;
				nm_medico_solicitante_w := null;
				sg_conselho_w          	:= null;
				nr_crm_w          	:= null;
				uf_crm_w          	:= null;
				cd_cbo_w          	:= null;
		end;
		
			insert into w_tiss_contratado_solic	(	nr_sequencia,	dt_atualizacao,	nm_usuario,
									nr_seq_conta,	nr_seq_guia, 	cd_cgc,
									cd_interno,	nr_cpf, 	nm_contratado,
									nm_solicitante, cd_cnes,	sg_conselho,
									nr_crm,		uf_crm,		cd_cbo_saude)
									
				values    			(	w_tiss_contratado_solic_seq.nextval, 	sysdate,					nm_usuario_p,
									nr_seq_conta_w, 			1, 						'',
									'',					obter_compl_pf(cd_medico_executor_w,1,'CPF'), 	'',
									nm_medico_solicitante_w, 		'', 						sg_conselho_w,
									nr_crm_w, 				uf_crm_w, 					cd_cbo_w);
				
			select  max(cd_doenca),
				max(ds_diagnostico)
			into    cd_doenca_cid_w,
				ds_diagnostico_w
			from    pls_diagnostico_conta
			where   nr_seq_conta        = nr_seq_conta_p
			and    	ie_classificacao    = 'P';
            
			insert 	into 	w_tiss_solicitacao	(	nr_sequencia,		dt_atualizacao,	nm_usuario,
									nr_seq_conta,		nr_seq_guia,	dt_solicitacao,
									ie_carater_solic, 	cd_cid,		ds_indicacao)
									
			values    				(	w_tiss_solicitacao_seq.nextval, sysdate, 	nm_usuario_p,
									nr_seq_conta_w, 		1,		dt_autorizacao_w, 
									ie_carater_internacao_w, 	cd_doenca_cid_w, ds_indicacao_w);
        end if;

        nr_seq_apresentacao_w    := nr_seq_apresentacao_w + 1;

        pls_define_preco_proc
            (cd_estabelecimento_p, nr_seq_prestador_w, null,
            dt_autorizacao_w, null, cd_procedimento_w,
            ie_origem_proced_w, null, null,
            null, nr_seq_plano_w, 'P',
            0, 0, null,
            'N', null, '',
            '', '', '',
            '', '', null,
            '', '', '',
            '','A','X',
            null, ie_carater_internacao_w, dt_procedimento_w, 
            ie_tipo_acomodacao_ptu_w,nr_seq_rp_combinada_w, null,
            null, dados_conta_w, dados_regra_preco_proc_w);
      
	vl_procedimento_w		:= dados_regra_preco_proc_w.vl_procedimento;
	vl_anestesista_w    		:= dados_regra_preco_proc_w.vl_anestesista;
	vl_medico_w      		:= dados_regra_preco_proc_w.vl_medico;
	vl_filme_w      		:= dados_regra_preco_proc_w.vl_filme; 
	vl_auxiliares_w      		:= dados_regra_preco_proc_w.vl_auxiliares;
	nr_seq_regra_w      		:= dados_regra_preco_proc_w.nr_sequencia;
	cd_edicao_amb_w      		:= dados_regra_preco_proc_w.cd_edicao_amb; 
	cd_porte_anestesico_w   	:= dados_regra_preco_proc_w.cd_porte_anestesico;
	nr_aux_regra_w      		:= dados_regra_preco_proc_w.nr_auxiliares;
	nr_seq_regra_autogerado_w  	:= dados_regra_preco_proc_w.nr_seq_regra_autogerado;
	cd_moeda_autogerado_w   	:= dados_regra_preco_proc_w.cd_moeda_autogerado; 
	vl_ch_honorarios_w    		:= dados_regra_preco_proc_w.vl_ch_honorarios; 
	vl_ch_custo_oper_w    		:= dados_regra_preco_proc_w.vl_ch_custo_oper; 
	vl_ch_custo_filme_w   		:= dados_regra_preco_proc_w.vl_ch_custo_filme; 
	vl_ch_anestesista_w    		:= dados_regra_preco_proc_w.vl_ch_anestesista;
  
        if    (nvl(cd_edicao_amb_w,0) > 0) then
            select    substr(obter_descricao_padrao('TISS_TIPO_TABELA','CD_TABELA_RELAT',nr_seq_tiss_tabela),1,2)
            into    cd_tabela_relat_w
            from    edicao_amb
            where    cd_edicao_amb    = cd_edicao_amb_w;
        else
            cd_tabela_relat_w    := '94';
        end if;
            
        insert 	into 	w_tiss_proc_paciente	(	nr_sequencia,		dt_atualizacao,		nm_usuario,
							nr_seq_conta, 		nr_seq_guia,		cd_procedimento,
							ds_procedimento, 	dt_procedimento, 	cd_edicao_amb,
							qt_procedimento, 	vl_procedimento, 	vl_unitario,
							nr_seq_apresentacao)
							
        values    				(	w_tiss_proc_paciente_seq.nextval, sysdate,		nm_usuario_p,
							nr_seq_conta_w,	   		  1,			cd_procedimento_w,
							ds_procedimento_w, 		  dt_procedimento_w, 	cd_tabela_relat_w,
							qt_autorizada_w,  		  vl_liberado_w, 	vl_unitario_w,
							nr_seq_apresentacao_w);

        if    (qt_proc_conta_w = 5) then
      
		qt_proc_conta_w    := 0;

            /* pls_tiss_completar_conta(nr_seq_conta_w, nm_usuario_p); */
        end if;

  
	insert	into 	w_tiss_outras_despesas	(	nr_seq_apresentacao,	ie_tipo_despesa,	dt_item,
							dt_periodo_inicial, 	dt_periodo_final, 	cd_procedimento,
							qt_item, 		vl_unitario, 		vl_item,
							nr_sequencia, 		dt_atualizacao, 	nr_seq_guia,
							nm_usuario, 		cd_edicao_amb, 		nr_registro_anvisa,
							cd_material_fabricante, nr_autor_func_opme, 	cd_unidade_medida_tiss,
							vl_reducao_acrescimo,	ds_procedimento)
							
		values				(	nr_seq_apresentacao_w, 	ie_tipo_despesa_w,	dt_procedimento_w,
							hr_inicio_w,		hr_fim_w, 		cd_procedimento_w,
							qt_autorizada_w, 	vl_unitario_w, 		vl_liberado_w,
							w_tiss_outras_despesas_seq.nextval, sysdate, 	nr_seq_guia_w,
							nm_usuario_p,		cd_tabela_relat_w, 	nr_registro_anvisa_w,
							cd_ref_fabricante_w, 	ds_aut_funcionamento_w, cd_unidade_medida_w,
							tx_reducao_acrescimo_w, ds_procedimento_w);



	end loop;
close c01;


    if    (qt_proc_conta_w > 0) and (qt_proc_conta_w < 5) then   
	if    (	nr_seq_prestador_w    is not null) 	then
		cgc_exec_w        	:= pls_obter_dados_prestador(nr_seq_prestador_w,'CGC');
		cpf_exec_w        	:= pls_obter_dados_prestador(nr_seq_prestador_w,'CPF');
		nm_contratado_exec_w    := pls_obter_dados_prestador(nr_seq_prestador_w,'NF');
		ds_logradouro_w        	:= obter_dados_pf_pj(cpf_exec_w,cgc_exec_w,'ES');
		nm_municipio_w        	:= obter_dados_pf_pj(cpf_exec_w,cgc_exec_w,'CI');
		sg_estado_w        	:= obter_dados_pf_pj(cpf_exec_w,cgc_exec_w,'UF');
		cd_municipio_ibge_w    := obter_dados_pf_pj(cpf_exec_w,cgc_exec_w,'CDM');
		cd_cep_w        	:= obter_dados_pf_pj(cpf_exec_w,cgc_exec_w,'CEP');
		cd_cnes_w        	:= obter_dados_pf_pj(cpf_exec_w,cgc_exec_w,'CNES');
	end if;
    end if;

	pls_tiss_completar_conta(nr_seq_conta_w, nm_usuario_p);
end if;

commit;

end pls_tiss_gerar_w_outras_desp;
/