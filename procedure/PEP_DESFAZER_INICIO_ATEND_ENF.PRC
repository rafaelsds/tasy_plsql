create or replace
procedure pep_desfazer_inicio_atend_enf(nr_atendimento_p	number) is

begin

if	(nr_atendimento_p is not null) then

	update  atendimento_paciente 
	set     dt_inicio_atendimento = null, 
		ie_status_pa = 'NA'
	where   nr_atendimento = nr_atendimento_p;
	
end if;

commit;

end  pep_desfazer_inicio_atend_enf;
/