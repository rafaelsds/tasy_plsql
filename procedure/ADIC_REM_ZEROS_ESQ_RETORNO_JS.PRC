create or replace
procedure adic_rem_zeros_esq_retorno_js(	nr_sequencia_p		number,
					qt_digitos_p		number,
					ie_opcao_p		number,
					nm_usuario_p		Varchar2) is 		

begin

if	(nvl(ie_opcao_p, -1) = 0) then

	update	convenio_retorno_movto
	set	nr_doc_convenio	= ltrim(nr_doc_convenio, '0'),
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p
	and	nr_doc_convenio is not null;
	
elsif (nvl(ie_opcao_p, -1) = 1) then

	update	convenio_retorno_movto
	set	nr_doc_convenio   = lpad(nr_doc_convenio, qt_digitos_p, '0'),
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia      = nr_sequencia_p
	and	nr_doc_convenio is not null
	and	length(nr_doc_convenio) <= qt_digitos_p;
	
end if;

commit;

end adic_rem_zeros_esq_retorno_js;
/