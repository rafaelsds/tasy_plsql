create or replace procedure bft_insert_lab_result_item(	nr_prescricao_p     	number,
														nr_seq_prescr_p     	number,
														cd_exame_p          	varchar2,
														ds_resultado_p      	varchar2,
														nm_usuario_p        	varchar2,
														dt_coleta_p         	date,
														ds_referencia_p     	varchar2,
														ds_unidade_medida_p 	varchar2,
														dt_resultado_p      	date,
														ds_erro_p           	out varchar2,
														ds_observacao_p     	varchar2 default null,
														nr_atendimento_p    	number default null,
														cd_codigo_externo_p 	varchar2 default null,
														cd_cod_system_p     	varchar2 default null,
														ds_flag_p 		    	varchar2 default null) is

  ds_resultado_w         exame_lab_result_item.ds_resultado%type;
  qt_resultado_w         exame_lab_result_item.qt_resultado%type;
  pr_resultado_w         exame_lab_result_item.pr_resultado%type;
  ie_formato_resultado_w varchar2(20);
  pr_minimo_w            exame_lab_result_item.pr_minimo%type;
  pr_maximo_w            exame_lab_result_item.pr_maximo%type;
  qt_minima_w            exame_lab_result_item.qt_minima%type;
  qt_maxima_w            exame_lab_result_item.qt_maxima%type;
  lista_w                lista_varchar_pck.tabela_varchar;
  cd_material_exame_w    prescr_procedimento.cd_material_exame%type;
  qt_prescr_w            number(10);
  ds_valor_w		varchar2(20);

  cursor c01 is
    select el.nr_seq_exame, el.nr_seq_superior
      from exame_laboratorio el
     where nvl(el.cd_exame_integracao, el.cd_exame) = cd_exame_p
    union
    select e.nr_seq_exame, e.nr_seq_superior
      from exame_laboratorio e
     where ((cd_cod_system_p is null
       and (e.nr_seq_exame = obter_equip_exame_integracao(cd_exame_p, 'fleury', 1)
        or e.nr_seq_exame = obter_equip_exame_integracao(cd_exame_p, 'labext', 1)))
        or (cd_cod_system_p is not null
       and e.nr_seq_exame = obter_equip_exame_integracao(cd_exame_p, cd_cod_system_p, 1)))
     order by 1;

	function is_exam_prescr return boolean is
	nr_seq_prescr_nova_w	number(6);
	nr_seq_exame_w		number(10);
	nr_seq_exame_atual_w	number(10);
	nr_seq_superior_w	number(10);
	begin
	--Check if the exam was prescribed
	open C01;
	loop
	fetch C01 into
		nr_seq_exame_atual_w,
		nr_seq_superior_w;
	exit when C01%notfound;
		begin

		select max(nvl(nr_sequencia, null))
		into	nr_seq_prescr_nova_w
		from	prescr_procedimento
		where	nr_prescricao		= nr_prescricao_p
		  and	nr_seq_exame		= nr_seq_exame_atual_w
		  and	nr_sequencia = nvl(nr_seq_prescr_p,nr_sequencia);

		if	(nr_seq_prescr_nova_w is null) then

			select	max(nvl(nr_sequencia, null))
			into	nr_seq_prescr_nova_w
			from	prescr_procedimento
			where	nr_prescricao = nr_prescricao_p
			  and	ie_status_atend >= 30
			  and	nr_seq_exame = nr_seq_superior_w
			  and	nr_sequencia = nvl(nr_seq_prescr_p,nr_sequencia);

			  if	(nr_seq_prescr_nova_w is null) then

				select	max(nvl(nr_sequencia, null))
				into	nr_seq_prescr_nova_w
				from	prescr_procedimento
				where	nr_prescricao = nr_prescricao_p
				and		nr_seq_exame = nr_seq_superior_w
				and		nr_sequencia = nvl(nr_seq_prescr_p,nr_sequencia);

			end if;
		end if;

		if	(nr_seq_prescr_nova_w is not null) then
			exit;
		end if;

		end;
	end loop;
	close C01;
	
	return (nr_seq_prescr_nova_w is not null);
	
	end is_exam_prescr;


begin


select	max(ie_formato_resultado)
into	ie_formato_resultado_w
from	exame_laboratorio el
where	nvl(el.cd_exame_integracao, el.cd_exame) = cd_exame_p;


select	max(a.cd_material_exame)
into	cd_material_exame_w
from	prescr_procedimento a
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia 	= nr_seq_prescr_p;


begin

	if	(ie_formato_resultado_w	= 'V') then
		qt_resultado_w	:= to_number(ds_resultado_p);
	elsif	(ie_formato_resultado_w	= 'P') then
		pr_resultado_w	:= to_number(ds_resultado_p);
	elsif	(ie_formato_resultado_w	= 'VP') then
		qt_resultado_w	:= to_number(ds_resultado_p);
		pr_resultado_w	:= to_number(ds_resultado_p);
	elsif	(ie_formato_resultado_w	= 'DV') then
		ds_resultado_w	:= ds_resultado_p;
		begin
			qt_resultado_w	:= to_number(ds_resultado_p);
		exception
		when others then
			null;
		end;
		
	else
		ds_resultado_w	:= ds_resultado_p;
	end if;

exception
when others then
	ds_resultado_w	:= ds_resultado_p;
end;


begin

	if	(ds_referencia_p is not null) and
		(instr(ds_referencia_p,'-') > 0) then
		 lista_w	:= obter_lista_string2(ds_referencia_p,'-');
		 
		 if	(ie_formato_resultado_w	= 'V') then
			qt_minima_w	:= to_number(lista_w(1));
			qt_maxima_w	:= to_number(lista_w(2));
		elsif	(ie_formato_resultado_w	= 'P') then
			pr_minimo_w	:= to_number(lista_w(1));
			pr_maximo_w	:= to_number(lista_w(2));
		elsif	(ie_formato_resultado_w	= 'VP') then
			pr_minimo_w	:= to_number(lista_w(1));
			pr_maximo_w	:= to_number(lista_w(2));
			qt_minima_w	:= to_number(lista_w(1));
			qt_maxima_w	:= to_number(lista_w(2));
		end if;	
	elsif	(instr(ds_referencia_p,'<') > 0) then
		 ds_valor_w	:= trim(replace(ds_referencia_p,'<',''));
		  if	(ie_formato_resultado_w	= 'V') then
			qt_maxima_w	:= to_number(ds_valor_w);
		elsif	(ie_formato_resultado_w	= 'P') then
			pr_maximo_w	:= to_number(ds_valor_w);
		elsif	(ie_formato_resultado_w	= 'VP') then
			qt_maxima_w	:= to_number(ds_valor_w);
			pr_maximo_w	:= to_number(ds_valor_w);
		end if;	
	elsif	(instr(ds_referencia_p,'>') > 0) then
		 ds_valor_w	:= trim(replace(ds_referencia_p,'>',''));
		  if	(ie_formato_resultado_w	= 'V') then
			qt_minima_w	:= to_number(ds_valor_w);
		elsif	(ie_formato_resultado_w	= 'P') then
			pr_minimo_w	:= to_number(ds_valor_w);
		elsif	(ie_formato_resultado_w	= 'VP') then
			qt_minima_w	:= to_number(ds_valor_w);
			pr_minimo_w	:= to_number(ds_valor_w);
		end if;	
	end if;
	
exception
when others then
	null;
end;


select	count(1)
into	qt_prescr_w
from	prescr_procedimento
where	nr_prescricao 	= nr_prescricao_p
and	nr_sequencia	= nr_seq_prescr_p;

if	(qt_prescr_w	> 0) and (is_exam_prescr()) then

	atualizar_lab_result_item(	nr_prescricao_p		=> nr_prescricao_p, 
					nr_seq_prescr_p 	=> nr_seq_prescr_p, 
					cd_exame_p 		=> cd_exame_p , 
					qt_resultado_p 		=> qt_resultado_w,
					pr_resultado_p 		=> pr_resultado_w, 
					ds_resultado_p 		=> ds_resultado_w, 
					ds_observacao_p 	=> ds_observacao_p, 
					cd_material_exame_p 	=> cd_material_exame_w, 
					ie_reenvio_p 		=> null, 
					nm_usuario_p 		=> nm_usuario_p , 
					dt_coleta_p 		=> dt_coleta_p, 
					ds_referencia_p 	=> ds_referencia_p, 
					ds_unidade_medida_p 	=> ds_unidade_medida_p, 
					nr_doc_lab_p 		=> null, 
					dt_resultado_p 		=> dt_resultado_p, 
					ds_erro_p 		=> ds_erro_p, 
					nr_seq_exame_atualiz_p 	=> null, 
					cd_metodo_p 		=> null, 
					qt_minima_p 		=> qt_minima_w, 
					qt_maxima_p 		=> qt_maxima_w, 
					pr_minimo_p 		=> pr_minimo_w, 
					pr_maximo_p 		=> pr_maximo_w, 
					ds_flag_p 			=> DS_FLAG_P,
					cd_comment_ext_p 	=> CD_CODIGO_EXTERNO_P);
					
					
	if	(ds_erro_p	is  null) then
		ws_gerar_prescr_proc_compl(	nr_prescricao_p,
						nr_seq_prescr_p,
						nm_usuario_p,
						'L');
	end if;
elsif	(nr_atendimento_p is not null) and
	(nr_atendimento_p > 0) then
	atualizar_lab_result_item_ext(	nr_atendimento_p		=> nr_atendimento_p,
					cd_exame_p			=> cd_exame_p,
					dt_resultado_p			=> dt_resultado_p,
					dt_coleta_p			=> dt_coleta_p,
					ds_resultado_p			=> ds_resultado_w,
					qt_resultado_p			=> qt_resultado_w,
					pr_resultado_p			=> pr_resultado_w,
					ds_referencia_p			=> ds_referencia_p,
					ds_unidade_medida_p		=> ds_unidade_medida_p,
					ds_observacao_p			=> ds_observacao_p,
					nm_usuario_p			=> nm_usuario_p,
					qt_minima_p			=> qt_minima_w,
					qt_maxima_p			=> qt_maxima_w,
					pr_minimo_p			=> pr_minimo_w,
					pr_maximo_p			=> pr_maximo_w, 
					ds_flag_p 			=> DS_FLAG_P,
					cd_comment_ext_p 	=> cd_codigo_externo_p);
end if;
commit;

end bft_insert_lab_result_item;
/
