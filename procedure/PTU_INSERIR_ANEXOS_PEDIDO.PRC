create or replace
procedure ptu_inserir_anexos_pedido
			(	nr_seq_guia_p		Number,
				nr_seq_requisicao_p	Number,
				nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir os registros referentes aos anexos dos pedidos de autorização e complemento  
contidos nas tableas temporárias, em tabelas quentes do sistema.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
ROTINA UTILIZADA NAS TRANSAÇÕES PTU VIA SCS HOMOLOGADAS COM A UNIMED BRASIL.
QUANDO FOR ALTERAR, FAVOR VERIFICAR COM O ANÁLISTA RESPONSÁVEL PARA A REALIZAÇÃO DE TESTES.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_altura_benef_w		w_ptu_dados_anexo_quimio.nr_altura_benef%type;
nr_peso_benef_w			w_ptu_dados_anexo_quimio.nr_peso_benef%type;
nr_superfice_corp_w		w_ptu_dados_anexo_quimio.nr_superfice_corp%type;
dt_diagnostico_w		w_ptu_dados_anexo_quimio.dt_diagnostico%type;
cd_cid_1_w			w_ptu_dados_anexo_quimio.cd_cid_1%type;
cd_cid_2_w			w_ptu_dados_anexo_quimio.cd_cid_2%type;
cd_cid_3_w			w_ptu_dados_anexo_quimio.cd_cid_3%type;
cd_cid_4_w			w_ptu_dados_anexo_quimio.cd_cid_4%type;
cd_estadiamento_w		w_ptu_dados_anexo_quimio.cd_estadiamento%type;
cd_finalidade_w			w_ptu_dados_anexo_quimio.cd_finalidade%type;
cd_ecog_w			w_ptu_dados_anexo_quimio.cd_ecog%type;
ie_tipo_quimioterapia_w		w_ptu_dados_anexo_quimio.ie_tipo_quimioterapia%type;
nr_ciclos_w			w_ptu_dados_anexo_quimio.nr_ciclos%type;
nr_ciclo_atual_w		w_ptu_dados_anexo_quimio.nr_ciclo_atual%type;
nr_intervalo_ciclo_w		w_ptu_dados_anexo_quimio.nr_intervalo_ciclo%type;
ds_cirurgia_w			w_ptu_dados_anexo_quimio.ds_cirurgia%type;
dt_cirurgia_w			w_ptu_dados_anexo_quimio.dt_cirurgia%type;
ds_area_irradiada_w		w_ptu_dados_anexo_quimio.ds_area_irradiada%type;
dt_irradiacao_w			w_ptu_dados_anexo_quimio.dt_irradiacao%type;
cd_diagnostico_imagem_w		w_ptu_dados_anexo_radio.cd_diagnostico_imagem%type;
qt_campos_w			w_ptu_dados_anexo_radio.qt_campos%type;
qt_dose_dia_w			w_ptu_dados_anexo_radio.qt_dose_dia%type;
qt_dose_total_w			w_ptu_dados_anexo_radio.qt_dose_total%type;
qt_dias_tratamento_w		w_ptu_dados_anexo_radio.qt_dias_tratamento%type;
dt_prev_administracao_w		w_ptu_dados_anexo_radio.dt_prev_administracao%type;
ds_quimioterapia_w		w_ptu_dados_anexo_radio.ds_quimioterapia%type;
dt_quimioterapia_w		w_ptu_dados_anexo_radio.dt_quimioterapia%type;
ds_diag_cit_hist_quimio_w	w_ptu_dados_anexo_pedido.ds_diag_cit_hist_quimio%type;
ds_diag_cit_hist_radio_w	w_ptu_dados_anexo_pedido.ds_diag_cit_hist_radio%type;
ds_inf_relevante_quimio_w	w_ptu_dados_anexo_pedido.ds_inf_relevante_quimio%type;
ds_inf_relevante_radio_w	w_ptu_dados_anexo_pedido.ds_inf_relevante_radio%type;
ds_justific_tecnica_w		w_ptu_dados_anexo_pedido.ds_justific_tecnica%type;
ds_material_solic_w		w_ptu_dados_anexo_pedido.ds_material_solic%type;
ds_plano_terapeutico_w		w_ptu_dados_anexo_pedido.ds_plano_terapeutico%type;
ie_existe_anexo_w		Varchar2(1)	:= 'S';

begin

begin
	select	ds_diag_cit_hist_quimio,
		ds_diag_cit_hist_radio,
		ds_inf_relevante_quimio,
		ds_inf_relevante_radio,
		ds_justific_tecnica,
		ds_material_solic,
		ds_plano_terapeutico
	into	ds_diag_cit_hist_quimio_w,
		ds_diag_cit_hist_radio_w,
		ds_inf_relevante_quimio_w,
		ds_inf_relevante_radio_w,
		ds_justific_tecnica_w,
		ds_material_solic_w,
		ds_plano_terapeutico_w
	from	w_ptu_dados_anexo_pedido
	where	(nr_seq_guia		= nr_seq_guia_p
	or	nr_seq_requisicao	= nr_seq_requisicao_p);
exception
when others then
	ie_existe_anexo_w	:= 'N';
end;

if	(ie_existe_anexo_w	= 'S') then
	ie_existe_anexo_w	:= 'N';
end if;

begin
	select	nr_altura_benef,
		nr_peso_benef,
		nr_superfice_corp,
		dt_diagnostico,
		cd_cid_1,
		cd_cid_2,
		cd_cid_3,
		cd_cid_4,
		cd_estadiamento,
		cd_finalidade,
		cd_ecog,
		ie_tipo_quimioterapia,
		nr_ciclos,
		nr_ciclo_atual,
		nr_intervalo_ciclo,
		ds_cirurgia,
		dt_cirurgia,
		ds_area_irradiada,
		dt_irradiacao
	into	nr_altura_benef_w,
		nr_peso_benef_w,
		nr_superfice_corp_w,
		dt_diagnostico_w,
		cd_cid_1_w,
		cd_cid_2_w,
		cd_cid_3_w,
		cd_cid_4_w,
		cd_estadiamento_w,
		cd_finalidade_w,
		cd_ecog_w,
		ie_tipo_quimioterapia_w,
		nr_ciclos_w,
		nr_ciclo_atual_w,
		nr_intervalo_ciclo_w,
		ds_cirurgia_w,
		dt_cirurgia_w,
		ds_area_irradiada_w,
		dt_irradiacao_w
	from	w_ptu_dados_anexo_quimio
	where	(nr_seq_guia		= nr_seq_guia_p
	or	nr_seq_requisicao	= nr_seq_requisicao_p);
exception
when others then
	ie_existe_anexo_w	:= 'N';
end;

if	(ie_existe_anexo_w	= 'S') then
	ie_existe_anexo_w	:= 'N';
end if;

begin
	select	dt_diagnostico,
		cd_cid_1,
		cd_cid_2,
		cd_cid_3,
		cd_cid_4,
		cd_estadiamento,
		cd_finalidade,
		cd_ecog,
		cd_diagnostico_imagem,
		qt_campos,
		qt_dose_dia,
		qt_dose_total,
		qt_dias_tratamento,
		dt_prev_administracao,
		ds_cirurgia,
		dt_cirurgia,
		ds_quimioterapia,
		dt_quimioterapia
	into	dt_diagnostico_w,
		cd_cid_1_w,
		cd_cid_2_w,
		cd_cid_3_w,
		cd_cid_4_w,
		cd_estadiamento_w,
		cd_finalidade_w,
		cd_ecog_w,
		cd_diagnostico_imagem_w,
		qt_campos_w,
		qt_dose_dia_w,
		qt_dose_total_w,
		qt_dias_tratamento_w,
		dt_prev_administracao_w,
		ds_cirurgia_w,
		dt_cirurgia_w,
		ds_quimioterapia_w,
		dt_quimioterapia_w
	from	w_ptu_dados_anexo_radio
	where	(nr_seq_guia		= nr_seq_guia_p
	or	nr_seq_requisicao	= nr_seq_requisicao_p);
exception
when others then
	ie_existe_anexo_w	:= 'N';
end;

if	(ie_existe_anexo_w	= 'S') then
	ie_existe_anexo_w	:= 'N';
end if;

if	(nr_seq_guia_p	is not null) then
	delete
	from	w_ptu_dados_anexo_pedido
	where	nr_seq_guia	= nr_seq_guia_p;

	delete
	from	w_ptu_dados_anexo_quimio
	where	nr_seq_guia	= nr_seq_guia_p;
	
	delete
	from	w_ptu_dados_anexo_radio
	where	nr_seq_guia	= nr_seq_guia_p;
elsif	(nr_seq_requisicao_p	is not null) then
	delete
	from	w_ptu_dados_anexo_pedido
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

	delete
	from	w_ptu_dados_anexo_quimio
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	
	delete
	from	w_ptu_dados_anexo_radio
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
end if;

commit;

end ptu_inserir_anexos_pedido;
/