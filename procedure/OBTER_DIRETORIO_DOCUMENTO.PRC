create or replace
function obter_diretorio_documento(nr_seq_documento_p number,
				  nm_usuario_p varchar2)
 		    	return varchar2 is

ds_arquivo_w	varchar2(255);			
begin

select ds_arquivo
into ds_arquivo_w 
from qua_documento
where nr_sequencia = nr_seq_documento_p;

return	ds_arquivo_w;

end obter_diretorio_documento;
/