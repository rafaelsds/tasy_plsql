create or replace
procedure pls_ajustar_coop_segurado is

nr_seq_congenere_w	number(10);
nr_seq_repasse_w	number(10);
cd_cooperativa_w	varchar2(10);

Cursor C01 is
	select	nr_seq_congenere,
		nr_sequencia
	from	pls_segurado_repasse
	where	cd_cooperativa is null;

begin

open C01;
loop
fetch C01 into	
	nr_seq_congenere_w,
	nr_seq_repasse_w;
exit when C01%notfound;
	begin
	
	begin
	select	max(cd_cooperativa)
	into	cd_cooperativa_w
	from	pls_congenere
	where	nr_sequencia	= nr_seq_congenere_w;
	exception
	when others then
		cd_cooperativa_w := '';
	end;
	
	if	(cd_cooperativa_w is not null) then
		update	pls_segurado_repasse
		set	cd_cooperativa	= cd_cooperativa_w
		where	nr_sequencia	= nr_seq_repasse_w;
	end if;
	end;
end loop;
close C01;

end pls_ajustar_coop_segurado;
/
