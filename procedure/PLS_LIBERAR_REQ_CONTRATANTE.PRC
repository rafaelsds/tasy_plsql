create or replace
procedure pls_liberar_req_contratante
			(	ie_status_p		number,
				nr_seq_requisicao_p	number,
				nm_usuario_p		Varchar2) is 

ie_estagio_w			number(2);

begin

if	(ie_status_p = 0) then
	ie_estagio_w := 9;
elsif	(ie_status_p = 1) then
	ie_estagio_w := 7;
end if;

update	pls_requisicao
set	ie_estagio	= ie_estagio_w,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_requisicao_p;

commit;

end pls_liberar_req_contratante;
/
