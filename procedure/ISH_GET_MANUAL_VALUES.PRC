create or replace procedure ish_get_manual_values(
		nr_seq_fila_p	in	number,
		patientno_p	out	varchar2,
		caseno_p		out	varchar2,		
		movenumber_p	out	varchar2,
		sendclient_p	out	varchar2,
		sendinstit_p	out	varchar2) is 
		
		


patientno_w			hcm_patienten.patientno%type;
caseno_w			hcm_fall.caseno%type;
movenumber_w			hcm_fall.movenumber%type;
sendclient_w			hcm_kopf.sendclient%type;
sendinstit_w			hcm_kopf.sendinstit%type;

nr_seq_documento_w		intpd_fila_transmissao.nr_seq_documento%type;
nr_seq_item_documento_w		intpd_fila_transmissao.nr_seq_item_documento%type;
ie_evento_w			intpd_fila_transmissao.ie_evento%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_empresa_w			estabelecimento.cd_empresa%type;
ie_conversao_w			intpd_eventos_sistema.ie_conversao%type;
nr_seq_regra_conv_w		intpd_eventos_sistema.nr_seq_regra_conv%type;

begin
begin
select	a.nr_seq_documento,
	a.nr_seq_item_documento,
	a.ie_evento,
	nvl(a.cd_estab_documento, c.cd_estabelecimento) cd_estabelecimento,
	b.ie_conversao,
	b.nr_seq_regra_conv
into	nr_seq_documento_w,
	nr_seq_item_documento_w,
	ie_evento_w,
	cd_estabelecimento_w,
	ie_conversao_w,
	nr_seq_regra_conv_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b,
	intpd_eventos_inf_receb c
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	b.nr_sequencia = c.nr_seq_evento_sistema(+)
and	a.nr_sequencia = nr_seq_fila_p
and	rownum = 1;

if	(cd_estabelecimento_w is null) then
	begin
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	estabelecimento
	where	ie_situacao = 'A';
	exception
	when others then
		cd_estabelecimento_w	:=	null;
	end;
end if;

if	(cd_estabelecimento_w is not null) then
	begin
	select	cd_empresa
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_w;	
	exception
	when others then
		cd_empresa_w	:=	null;
	end;
end if;
	
exception
when others then
	nr_seq_documento_w	:=	null;
end;

if	(somente_numero(nr_seq_documento_w) > 0) then
	begin
	if	(ie_evento_w = '81') then
		patientno_p	:=	nr_seq_documento_w;
	else
		begin
		caseno_p	:=	nr_seq_documento_w;
		movenumber_p	:=	nr_seq_item_documento_w;
		end;
	end if;
	
	sendclient_p	:= intpd_conv('EMPRESA','CD_EMPRESA', cd_empresa_w, nr_seq_regra_conv_w, ie_conversao_w, 'E');
	sendinstit_p	:= intpd_conv('ESTABELECIMENTO','CD_ESTABELECIMENTO', cd_estabelecimento_w, nr_seq_regra_conv_w, ie_conversao_w, 'E');
	end;
end if;
end ish_get_manual_values;
/
