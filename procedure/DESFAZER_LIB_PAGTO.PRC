create or replace
procedure desfazer_lib_pagto(	nr_sequencia_p 		number) is 
begin

	if	(nr_sequencia_p is not null) then
		update conta_pagar_lib  
                set    dt_liberacao         = null 
                where  nr_sequencia         = nr_sequencia_p;
	end if;
commit;

end desfazer_lib_pagto;
/