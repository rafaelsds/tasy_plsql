create or replace
function emar_obter_nr_seq_item(	nr_seq_cpoe_p		number,
									nr_prescricao_p		number,
									ie_tipo_item_p		varchar2)
								return number is

nr_sequencia_w	number(15)	:= null;

begin

if	(nvl(nr_prescricao_p,0) > 0) and
	(nvl(nr_seq_cpoe_p,0) > 0 ) and
	(ie_tipo_item_p is not null)then

	if	(ie_tipo_item_p in ('M','SOL','MAT')) then
		
		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from 	prescr_material
		where 	nr_prescricao 	= nr_prescricao_p
		and 	nr_seq_mat_cpoe	= nr_seq_cpoe_p;

	elsif (ie_tipo_item_p in ('AP','P')) then --'Procedimento, Anatomia Patológica';
			
		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from 	prescr_procedimento
		where 	nr_prescricao 		= nr_prescricao_p
		and 	nr_seq_proc_cpoe	= nr_seq_cpoe_p;

	elsif (ie_tipo_item_p = 'D') then --Dieta Oral	
	
		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from 	prescr_dieta
		where 	nr_prescricao 		= nr_prescricao_p
		and 	nr_seq_dieta_cpoe	= nr_seq_cpoe_p;
		
	elsif (ie_tipo_item_p in ('SNE','S')) then --Dienta Enteral/'Suplemento
	
		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from 	prescr_material
		where 	nr_prescricao	= nr_prescricao_p
		and 	nr_seq_mat_cpoe = nr_seq_cpoe_p;
		
	elsif (ie_tipo_item_p = 'LD') then --'Leites e derivados';
	
		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from	prescr_leite_deriv
		where 	nr_prescricao 		= nr_prescricao_p
		and 	nr_seq_dieta_cpoe	= nr_seq_cpoe_p;

	elsif (ie_tipo_item_p = 'DI') then --Diálise
	
		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from 	hd_prescricao
		where 	nr_prescricao 		= nr_prescricao_p
		and 	nr_seq_dialise_cpoe	= nr_seq_cpoe_p;

	elsif (ie_tipo_item_p = 'R') then --'Recomendação';
	
		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from 	prescr_recomendacao
		where 	nr_prescricao	= nr_prescricao_p
		and 	nr_seq_rec_cpoe = nr_seq_cpoe_p;

	elsif (ie_tipo_item_p = 'G') then --'Gasoterapia';
	
		select	max(nr_sequencia)
		into 	nr_sequencia_w
		from 	prescr_gasoterapia
		where 	nr_prescricao	= nr_prescricao_p
		and 	nr_seq_gas_cpoe	= nr_seq_cpoe_p;

	end if;	

end if;

return	nr_sequencia_w;

end emar_obter_nr_seq_item;
/