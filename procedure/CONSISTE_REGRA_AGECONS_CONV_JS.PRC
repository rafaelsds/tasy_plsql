create or replace
procedure consiste_regra_agecons_conv_js (
		dt_agenda_p		date,
		cd_convenio_p		number,
		cd_agenda_p		number,
		cd_setor_atendimento_p	number,
		cd_plano_convenio_p	varchar2,
		cd_pessoa_fisica_p 	varchar2,
		cd_categoria_p 		varchar2,
		ie_consiste_regra_conv_p	varchar2,
		ie_consiste_turno_p		varchar2,
		nm_usuario_p		varchar2,
		ds_msg_p	out	varchar2,
		cd_estabelecimento_p	number) is

ie_existe_turno_w		varchar2(1) := 'N';
begin
if	(nm_usuario_p is not null) then
	begin
	
	if	(ie_consiste_regra_conv_p = 'S') then
		begin
		consiste_regra_agecons_conv(cd_convenio_p, cd_categoria_p, cd_agenda_p, cd_setor_atendimento_p, cd_plano_convenio_p, cd_pessoa_fisica_p, dt_agenda_p, cd_estabelecimento_p, null, null);
		end;
	end if;
	
	if	(ie_consiste_turno_p = 'S') and
		(cd_agenda_p is not null) and
		(dt_agenda_p is not null) then
		begin
		ie_existe_turno_w := substr(obter_existe_agend_turno_cons(cd_agenda_p, dt_agenda_p, null),1);

		if	(ie_existe_turno_w = 'N') then
			begin
			ds_msg_p := obter_texto_dic_objeto(182449, wheb_usuario_pck.get_nr_seq_idioma, null);
			end;
		end if;
		end;
	end if;
	end;
end if;
commit;
end consiste_regra_agecons_conv_js;
/

