create or replace
procedure des_criar_diagrama(nm_diagrama_p		Varchar2,
							 ie_tipo_p			Varchar2,
							 nr_seq_projeto_p	Number,
							 nm_usuario_p		Varchar2,
							 nr_seq_objeto_p	Number,
							 nr_seq_diag_p	out	Number) is 

begin

select	des_diagrama_seq.nextval
into	nr_seq_diag_p
from	dual;

insert	into	des_diagrama(nr_sequencia, ie_tipo_diagrama, dt_atualizacao,
							 nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
							 nr_seq_projeto, nm_diagrama)
					  values(nr_seq_diag_p, ie_tipo_p, sysdate,
							 nm_usuario_p, sysdate, nm_usuario_p,
							 nr_seq_projeto_p, nm_diagrama_p);

if	(nr_seq_objeto_p is not null) then
	update	des_diagrama_objeto
	set	nr_des_diagrama = nr_seq_diag_p
	where	nr_sequencia = nr_seq_objeto_p;
end if;
					 
commit;		

			
end des_criar_diagrama;
/		