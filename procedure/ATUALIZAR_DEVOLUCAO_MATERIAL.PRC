create or replace
procedure atualizar_devolucao_material(	nr_sequencia_p		number,
										nr_item_nf_p		number,
										qt_devolucao_p		number,
										nr_seq_nf_origem_p	number,
										ie_estorna_ordem_p	varchar2,					
										nm_usuario_p		varchar2) is 

vl_total_item_nf_w		number(13,2);
vl_unitario_item_nf_w	number(13,4);
vl_desconto_w			number(13,2);
vl_liquido_w			number(15,2);
nr_seq_unidade_adic_w	number(10);
qt_conversao_w			number(13,4);
qt_item_estoque_w		number(13,4);
qt_registros_w			number(5);
nr_ordem_compra_w		number(10);
nr_item_oci_w			number(10);
cd_material_w			number(6);
cd_estabelecimento_w	number(4);
dt_entrega_ordem_w		date;
ds_historico_w			varchar2(2000);
nr_nota_fiscal_w		varchar2(255);

begin

select	count(1)
into	qt_registros_w
from	nota_fiscal_item a
where	a.nr_sequencia 	= nr_sequencia_p
and		a.nr_item_nf 	= nr_item_nf_p
and		a.qt_item_nf >= qt_devolucao_p;

if	(qt_registros_w > 0) and (qt_devolucao_p > 0) then
	select	a.vl_total_item_nf,
			a.vl_unitario_item_nf,
			nvl(a.vl_desconto,0),
			nvl(a.nr_seq_unidade_adic,0),
			obter_qt_convertida_compra_est(a.cd_material,qt_devolucao_p,a.cd_unidade_medida_compra,a.nr_seq_marca,b.cd_cgc,b.cd_estabelecimento),
			b.cd_estabelecimento,
			b.nr_nota_fiscal
	into	vl_total_item_nf_w,
			vl_unitario_item_nf_w,
			vl_desconto_w,
			nr_seq_unidade_adic_w,
			qt_item_estoque_w,
			cd_estabelecimento_w,
			nr_nota_fiscal_w
	from	nota_fiscal_item a,
			nota_fiscal b
	where	a.nr_sequencia 	= nr_sequencia_p
	and		a.nr_item_nf 	= nr_item_nf_p
	and		a.nr_sequencia 	= b.nr_sequencia
	and		a.qt_item_nf >= qt_devolucao_p;

	vl_total_item_nf_w	:= (qt_devolucao_p * vl_unitario_item_nf_w);
	vl_liquido_w		:= (vl_total_item_nf_w - vl_desconto_w);

	update	nota_fiscal_item
	set	vl_liquido = vl_liquido_w,
		vl_total_item_nf = vl_total_item_nf_w,
		qt_item_estoque = qt_item_estoque_w, 
		qt_item_nf = qt_devolucao_p
	where	nr_sequencia = nr_sequencia_p
	and	nr_item_nf = nr_item_nf_p;

	ds_historico_w	:= wheb_mensagem_pck.get_texto(311000) || ': ' || nr_nota_fiscal_w || ' seq.: ' || nr_sequencia_p;
			 

	if	(ie_estorna_ordem_p = 'S') then
		begin
		ds_historico_w	:= wheb_mensagem_pck.get_texto(311003) ||': '||nr_seq_nf_origem_p || ' seq.: ' || nr_sequencia_p;
				
		begin
		select	a.nr_ordem_compra,
			a.nr_item_oci,
			a.cd_material,
			a.dt_entrega_ordem
		into	nr_ordem_compra_w,
			nr_item_oci_w,
			cd_material_w,
			dt_entrega_ordem_w
		from	nota_fiscal_item a
		where	a.nr_sequencia = nr_seq_nf_origem_p
		and	a.nr_ordem_compra is not null
		and	a.nr_item_nf = nr_item_nf_p;		
		exception
		when others then
			nr_ordem_compra_w	:= 0;
		end;

		if	(nr_ordem_compra_w > 0) then
			estornar_baixa_OC_nota_dev(nr_ordem_compra_w, nr_item_oci_w, dt_entrega_ordem_w, nm_usuario_p,cd_estabelecimento_w,qt_devolucao_p);
		end if;
		end;
	end if;

	gerar_historico_nota_fiscal(nr_seq_nf_origem_p, nm_usuario_p, '3', ds_historico_w);
	commit;
end if;

end atualizar_devolucao_material;
/