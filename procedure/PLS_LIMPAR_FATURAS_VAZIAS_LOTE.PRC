create or replace
procedure pls_limpar_faturas_vazias_lote(	nr_seq_lote_p		pls_lote_faturamento.nr_sequencia%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
						nm_usuario_p		usuario.nm_usuario%type) is
						
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Limpar faturas e eventos que n�o possuem mais registros, devido �s rotinas de divis�o de fatura
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cursor c01 (	nr_seq_lote_pc		pls_lote_faturamento.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_fatura_evento
	from	pls_fatura_evento	b,
		pls_fatura		a
	where	a.nr_sequencia		= b.nr_seq_fatura
	and	not exists  (	select	1
				from	pls_fatura_conta	x
				where	x.nr_seq_fatura_evento	= b.nr_sequencia)
	and	a.nr_seq_lote = nr_seq_lote_pc;
	
cursor c02 (	nr_seq_lote_pc		pls_lote_faturamento.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_fatura
	from	pls_fatura 	a
	where not exists (	select	1
				from	pls_fatura_evento	b
				where	b.nr_seq_fatura		= a.nr_sequencia)
	and not exists (	select	1
				from	nota_fiscal		b
				where	b.nr_seq_fatura_ndc	= a.nr_sequencia)
	and not exists (	select	1
				from	nota_fiscal		b
				where	b.nr_seq_fatura		= a.nr_sequencia)
	and	a.nr_seq_lote	= nr_seq_lote_pc;
	
cursor c03 (	nr_seq_lote_pc		pls_lote_faturamento.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_conta_pos
	from	pls_conta_pos_estabelecido	a
	where	a.ie_status_faturamento		= 'L'
	and	a.nr_seq_lote_fat		= nr_seq_lote_pc
	and not exists(	select	1
			from	pls_fatura_conta	d,
				pls_fatura_evento	c,
				pls_fatura		b
			where	b.nr_sequencia		= c.nr_seq_fatura
			and	c.nr_sequencia		= d.nr_seq_fatura_evento
			and	b.nr_seq_lote		= a.nr_seq_lote_fat
			and	d.nr_seq_conta		= a.nr_seq_conta);
			
cursor c04 (	nr_seq_lote_pc		pls_lote_faturamento.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_conta_pos
	from	pls_conta_pos_proc		a
	where	a.ie_status_faturamento in ('L', 'P')
	and	a.nr_seq_lote_fat		= nr_seq_lote_pc
	and not exists(	select	1
			from	pls_fatura_conta	d,
				pls_fatura_evento	c,
				pls_fatura		b
			where	b.nr_sequencia		= c.nr_seq_fatura
			and	c.nr_sequencia		= d.nr_seq_fatura_evento
			and	b.nr_seq_lote		= a.nr_seq_lote_fat
			and	d.nr_seq_conta		= a.nr_seq_conta);
	
cursor c05 (	nr_seq_lote_pc		pls_lote_faturamento.nr_sequencia%type) is
	select	a.nr_sequencia nr_seq_conta_pos
	from	pls_conta_pos_mat		a
	where	a.ie_status_faturamento in ('L', 'P')
	and	a.nr_seq_lote_fat		= nr_seq_lote_pc
	and not exists(	select	1
			from	pls_fatura_conta	d,
				pls_fatura_evento	c,
				pls_fatura		b
			where	b.nr_sequencia		= c.nr_seq_fatura
			and	c.nr_sequencia		= d.nr_seq_fatura_evento
			and	b.nr_seq_lote		= a.nr_seq_lote_fat
			and	d.nr_seq_conta		= a.nr_seq_conta);
begin

-- Deleta eventos que n�o possuem contas
for r_c01_w in c01 ( nr_seq_lote_p ) loop

	delete	pls_fatura_item_trib
	where	nr_seq_fatura_evento	= r_c01_w.nr_seq_fatura_evento;

	delete 	pls_fatura_evento
	where	nr_sequencia 		= r_c01_w.nr_seq_fatura_evento;
	
end loop;

-- Deleta faturas que n�o possuem eventos
for r_c02_w in c02 ( nr_seq_lote_p ) loop
	delete	pls_fatura_item_trib
	where	nr_seq_fatura_trib in (	select	nr_sequencia
					from	pls_fatura_trib
					where	nr_seq_fatura	= r_c02_w.nr_seq_fatura);
	
	delete	pls_fatura_trib
	where	nr_seq_fatura	= r_c02_w.nr_seq_fatura;

	delete	pls_fatura_motivo_imp_cob
	where	nr_seq_fatura 		= r_c02_w.nr_seq_fatura;
	
	update	pls_fatura
	set	nr_seq_fat_divisao 	= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_seq_fat_divisao 	= r_c02_w.nr_seq_fatura;
	
	delete	pls_fatura_atend
	where	nr_seq_fatura_orig 	= r_c02_w.nr_seq_fatura;

        update  titulo_receber
        set     nr_seq_pls_fatura       = null,
                nm_usuario              = nm_usuario_p,
                dt_atualizacao          = sysdate
        where   nr_seq_pls_fatura       = r_c02_w.nr_seq_fatura;
	
	delete	pls_fatura
	where	nr_sequencia 		= r_c02_w.nr_seq_fatura;
end loop;

-- Caso tenha p�s vinculado ao lote de faturamento mas n�o esta em fatura
for r_c03_w in c03 ( nr_seq_lote_p ) loop
	update	pls_conta_pos_estabelecido
	set	nr_seq_lote_fat		= null,
		nr_seq_evento_fat	= null
	where	nr_sequencia		= r_c03_w.nr_seq_conta_pos
	and	nr_seq_lote_fat		= nr_seq_lote_p;
	
	update	pls_conta_pos_estab_contab
	set	nr_seq_lote_fat		= null,
		nr_seq_evento_fat	= null
	where	nr_seq_conta_pos	= r_c03_w.nr_seq_conta_pos
	and	nr_seq_lote_fat		= nr_seq_lote_p;
end loop;

-- Caso tenha p�s vinculado ao lote de faturamento mas n�o esta em fatura
for r_c04_w in c04 ( nr_seq_lote_p ) loop
	update	pls_conta_pos_proc
	set	nr_seq_lote_fat		= null,
		nr_seq_evento_fat	= null
	where	nr_sequencia		= r_c04_w.nr_seq_conta_pos;
end loop;

-- Caso tenha p�s vinculado ao lote de faturamento mas n�o esta em fatura
for r_c05_w in c05 ( nr_seq_lote_p ) loop
	update	pls_conta_pos_mat
	set	nr_seq_lote_fat		= null,
		nr_seq_evento_fat	= null
	where	nr_sequencia		= r_c05_w.nr_seq_conta_pos;
end loop;

-- Limpar tabela tempor�ria
delete 	w_pls_lote_fat_item
where	nr_seq_lote	= nr_seq_lote_p
and	nm_usuario	= nm_usuario_p;

commit;

end pls_limpar_faturas_vazias_lote;
/