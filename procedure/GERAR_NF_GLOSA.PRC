create or replace procedure GERAR_NF_GLOSA(
    nr_seq_lote_p number default null,
    nm_usuario_p varchar2,
    vl_glosa_p number,
    nr_seq_ret_conv_p number,
    nr_sequencia_p  out number,
    ie_opcao_p varchar2
) is

cd_convenio_w               convenio.cd_convenio%type;

cd_operacao_nf_w            nota_fiscal.cd_operacao_nf%type;
cd_natureza_operacao_w      nota_fiscal.cd_natureza_operacao%type;
nr_nota_fiscal_w            nota_fiscal.nr_nota_fiscal%type;
cd_serie_nf_w               nota_fiscal.cd_serie_nf%type;
cd_estabelecimento_w        nota_fiscal.cd_estabelecimento%type;
cd_cgc_w                    nota_fiscal.cd_cgc%type;
cd_condicao_pagamento_w     nota_fiscal.cd_condicao_pagamento%type;
ie_tipo_nota_w              nota_fiscal.ie_tipo_nota%type;
cd_cgc_emitente_w           nota_fiscal.cd_cgc_emitente%type;

ds_observacao_w             nota_fiscal_item.ds_observacao%type;
cd_procedimento_w           nota_fiscal_item.cd_procedimento%type;
ie_origem_proced_w          nota_fiscal_item.ie_origem_proced%type;
nr_seq_conta_financeira_w   nota_fiscal_item.nr_seq_conta_financ%type   := null;
ie_status_retorno_w	    convenio_retorno.ie_status_retorno%type;
qt_nota_fiscal_deb_w         number(4);
qt_nota_fiscal_cred_w        number(4);
max_nr_numero_nf_deb_w       nota_fiscal.nr_nota_fiscal%type;
max_nr_seq_nf_deb_w          nota_fiscal.nr_sequencia%type;
max_nr_numero_nf_cred_w      nota_fiscal.nr_nota_fiscal%type;
max_nr_seq_nf_cred_w         nota_fiscal.nr_sequencia%type;
ie_tipo_nota_ww              number(1);
nr_sequencia_w               nota_fiscal.nr_sequencia%type;
nr_sequencia_nf_w            nota_fiscal.nr_sequencia%type;

BEGIN

    if(nr_seq_lote_p > 0) then
        begin
            select  l.cd_convenio,
                    l.cd_estabelecimento
            into    cd_convenio_w,
                    cd_estabelecimento_w
            from    lote_audit_hist h,
                    lote_auditoria l
            where   h.nr_sequencia      = nr_seq_lote_p
            and     h.nr_seq_lote_audit = l.nr_sequencia;
        exception WHEN OTHERS THEN
            wheb_mensagem_pck.exibir_mensagem_abort(1130867);
        end;
    end if;
    
    if (nr_seq_ret_conv_p > 0) then
        begin
            select  max(nvl(cd_convenio,0)),
                    max(a.cd_estabelecimento)
            into    cd_convenio_w,
                    cd_estabelecimento_w
            from    convenio_retorno a,
                    convenio_retorno_item b
            where   a.nr_sequencia = nr_seq_ret_conv_p
            and     a.nr_sequencia = b.nr_seq_retorno;
        exception WHEN OTHERS THEN
            wheb_mensagem_pck.exibir_mensagem_abort(1130867);
        end;
    end if;

    if (nr_seq_ret_conv_p > 0 OR nr_seq_lote_p > 0) then
      if (nr_seq_ret_conv_p > 0) then

        select ie_status_retorno
        into ie_status_retorno_w
        from convenio_retorno
        where nr_sequencia = nr_seq_ret_conv_p; 

        if (ie_status_retorno_w <> 'V') then
          wheb_mensagem_pck.exibir_mensagem_abort(1168537);
        end if;
      end if;

      select count(*), max(nr_sequencia)
      into qt_nota_fiscal_cred_w, max_nr_seq_nf_cred_w
      from nota_fiscal
      where (
        (nr_seq_ret_conv_p > 0 AND nr_seq_retorno = nr_seq_ret_conv_p)
        or
        (nr_seq_lote_p > 0 AND nr_seq_lote_audit = nr_seq_lote_p)
      )
      and ie_situacao = '1'
      and ie_tipo_fatura = 'C';

      select count(*), max(nr_sequencia)
      into qt_nota_fiscal_deb_w, max_nr_seq_nf_deb_w
      from nota_fiscal
      where (
        (nr_seq_ret_conv_p > 0 AND nr_seq_retorno = nr_seq_ret_conv_p)
        or
        (nr_seq_lote_p > 0 AND nr_seq_lote_audit = nr_seq_lote_p)
      )
      and ie_situacao = '1'
      and ie_tipo_fatura = 'D';

      if (ie_opcao_p = 'D' AND qt_nota_fiscal_deb_w > 0) then

        select nr_nota_fiscal
        into max_nr_numero_nf_deb_w
        from nota_fiscal
        where nr_sequencia = max_nr_seq_nf_deb_w;

        wheb_mensagem_pck.exibir_mensagem_abort(1169984, 'NR_NUMERO='||max_nr_numero_nf_deb_w);
      end if;

      if (ie_opcao_p = 'C' AND qt_nota_fiscal_cred_w > 0) then

        select nr_nota_fiscal
        into max_nr_numero_nf_cred_w
        from nota_fiscal
        where nr_sequencia = max_nr_seq_nf_cred_w;

        wheb_mensagem_pck.exibir_mensagem_abort(1168627, 'NR_NUMERO='||max_nr_numero_nf_cred_w);
      end if;
    end if;

    if (vl_glosa_p <= 0) then
      wheb_mensagem_pck.exibir_mensagem_abort(1168538);
    end if;

    if  (cd_convenio_w = 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(1130867);
    else

        if (ie_opcao_p = 'C') then

          select  cd_cgc
          into    cd_cgc_w
          from    convenio
          where   cd_convenio = cd_convenio_w;

          select  cd_cgc
          into    cd_cgc_emitente_w
          from    estabelecimento
          where   cd_estabelecimento = cd_estabelecimento_w;

        elsif (ie_opcao_p = 'D') then

          select  cd_cgc
          into    cd_cgc_w
          from    estabelecimento
          where   cd_estabelecimento = cd_estabelecimento_w;

          cd_cgc_emitente_w := obter_cgc_convenio(cd_convenio_w);

        end if;
        
        if (cd_cgc_w is NULL) then
            wheb_mensagem_pck.exibir_mensagem_abort(1130864);
        else
            begin
                select  nvl(n.cd_operacao_nf,0),
                        nvl(n.cd_natureza_operacao,0),
                        n.cd_serie_nf,
                        nvl(n.cd_condicao_pagamento,0),
                        nvl(p.cd_procedimento,0),
                        nvl(n.ie_origem_proced,0)
                into    cd_operacao_nf_w,
                        cd_natureza_operacao_w,
                        cd_serie_nf_w,
                        cd_condicao_pagamento_w,
                        cd_procedimento_w,
                        ie_origem_proced_w
                from    parametro_nfs_credito n,
                        procedimento p
                where   n.cd_convenio           = cd_convenio_w
                and     n.cd_estabelecimento    = cd_estabelecimento_w
                and     p.cd_procedimento       = n.cd_procedimento
                and     p.ie_origem_proced      = n.ie_origem_proced
                and     n.ie_tipo_fatura        = ie_opcao_p
                and     rownum                  = 1;
            exception WHEN OTHERS THEN
                wheb_mensagem_pck.exibir_mensagem_abort(1130853);
            end;
            
            if (cd_natureza_operacao_w  = 0 or 
                cd_operacao_nf_w        = 0 or
                cd_serie_nf_w       IS NULL or 
                cd_condicao_pagamento_w = 0 or 
                cd_procedimento_w       = 0 or 
                ie_origem_proced_w      = 0) 
            then                
                wheb_mensagem_pck.exibir_mensagem_abort(1130853);                
            else
                if (ie_opcao_p = 'C') then
                  ie_tipo_nota_w      := 'SD';
                  ie_tipo_nota_ww     := 1;
                elsif (ie_opcao_p = 'D') then
                  ie_tipo_nota_w      := 'EN';
                  ie_tipo_nota_ww     := 0;
                end if;

                select  nota_fiscal_seq.nextval
                into    nr_sequencia_w
                from    dual;

                if (ie_opcao_p = 'D') then
                  select  (to_number(t.nr_ultima_nf) + 1)
                  into    nr_nota_fiscal_w
                  from    serie_nota_fiscal t
                  where   t.cd_serie_nf           = cd_serie_nf_w
                  and     t.cd_estabelecimento    = cd_estabelecimento_w;

                  update  serie_nota_fiscal
                  set     nr_ultima_nf        = nr_nota_fiscal_w,
                          dt_atualizacao      = sysdate,
                          nm_usuario          = nm_usuario_p
                  where   cd_serie_nf         = cd_serie_nf_w
                  and     cd_estabelecimento  = cd_estabelecimento_w;

                elsif (ie_opcao_p = 'C') then
                  nr_nota_fiscal_w        := nr_sequencia_w + 800000;
                end if;

                SELECT    NVL(MAX(nr_sequencia_nf),0) +1
                INTO    nr_sequencia_nf_w
                FROM    nota_fiscal
                WHERE    cd_estabelecimento    = cd_estabelecimento_w
                AND    NVL(cd_cgc_emitente,'0')= NVL(cd_cgc_emitente_w,'0')
                AND    cd_serie_nf        = cd_serie_nf_w
                AND    nr_nota_fiscal        = nr_nota_fiscal_w;

                ds_observacao_w     := substr(wheb_mensagem_pck.get_Texto(1130875, 'NR_SEQ_LOTE_P='|| nr_seq_lote_p || ';VL_GLOSA_P=' || vl_glosa_p ), 1, 255);
                
                insert into nota_fiscal (
                    nr_sequencia,       
                    cd_estabelecimento, 
                    cd_cgc_emitente,    
                    cd_serie_nf,        
                    nr_nota_fiscal,     
                    nr_sequencia_nf,    
                    cd_operacao_nf,     
                    dt_emissao,     
                    dt_entrada_saida,   
                    ie_acao_nf,     
                    ie_emissao_nf,      
                    ie_tipo_frete,      
                    vl_mercadoria,      
                    vl_total_nota,      
                    qt_peso_bruto,      
                    qt_peso_liquido,    
                    dt_atualizacao,     
                    nm_usuario,
                    cd_condicao_pagamento,
                    cd_cgc, 
                    vl_ipi,
                    vl_descontos,
                    vl_frete,
                    vl_seguro,
                    vl_despesa_acessoria,
                    cd_natureza_operacao,
                    vl_desconto_rateio,
                    ie_situacao,
                    nr_lote_contabil,
                    ie_tipo_nota,
                    ds_observacao,
                    nr_seq_retorno,
                    nr_seq_lote_audit,
                    ie_tipo_fatura
                ) values( 
                    nr_sequencia_w,
                    cd_estabelecimento_w,
                    cd_cgc_emitente_w,
                    cd_serie_nf_w,
                    nr_nota_fiscal_w,
                    nr_sequencia_nf_w,
                    cd_operacao_nf_w,
                    trunc(sysdate),
                    sysdate,
                    '1',
                    '0',
                    0,
                    0,
                    vl_glosa_p,
                    0,
                    0,
                    sysdate,
                    nm_usuario_p,
                    cd_condicao_pagamento_w,
                    cd_cgc_w,
                    0,
                    0,
                    0,
                    0,
                    0,
                    cd_natureza_operacao_w,
                    0,
                    '1',
                    0,
                    ie_tipo_nota_w,
                    ds_observacao_w,
                    nr_seq_ret_conv_p,
                    nr_seq_lote_p,
                    ie_opcao_p
                );

                gerar_historico_nota_fiscal(
                    nr_seq_nota_p   => nr_sequencia_w,
                    nm_usuario_p    => nm_usuario_p,
                    cd_evento_p     => '3', 
                    ds_historico_p  => ds_observacao_w);

                obter_conta_financeira(
                    IE_ENTRADA_SAIDA_P          => 'S',
                    CD_ESTABELECIMENTO_P        => cd_estabelecimento_w,
                    CD_MATERIAL_P               => null,
                    CD_PROCEDIMENTO_P           => cd_procedimento_w,
                    IE_ORIGEM_PROCED_P          => ie_origem_proced_w,
                    CD_SETOR_ATEND_P            => null,
                    CD_CONVENIO_P               => cd_convenio_w,
                    CD_CGC_p                    => cd_cgc_emitente_w,
                    CD_CENTRO_CUSTO_P           => null,
                    NR_SEQ_CONTA_FINANC_P       => nr_seq_conta_financeira_w,
                    ie_clinica_p                => null,
                    cd_operacao_nf_p            => cd_operacao_nf_w,
                    ie_tipo_pessoa_p            => null,
                    ie_tipo_atendimento_p       => null,
                    cd_categoria_convenio_p     => null,
                    nr_seq_proj_recurso_p       => null,
                    nr_seq_produto_p            => null,
                    cd_pessoa_fisica_p          => null,
                    ie_origem_tit_rec_p         => null,
                    ie_origem_tit_pag_p         => null,
                    nr_seq_classe_tit_rec_p     => null,
                    nr_seq_classe_tit_pag_p     => null,
                    cd_local_estoque_p          => null,
                    nr_seq_trans_fin_p          => null,
                    ie_vago2_p                  => null,
                    ie_vago3_p                  => null,
                    ie_tipo_titulo_pagar_p      => null,
                    ie_tipo_titulo_receber_p    => null,
                    cd_moeda_p                  => null);

                if  (nr_seq_conta_financeira_w  = 0) then
                    nr_seq_conta_financeira_w  := null;
                end if;

                insert into nota_fiscal_item(
                    cd_estabelecimento, 
                    cd_cgc_emitente,    
                    cd_serie_nf,        
                    nr_nota_fiscal,
                    nr_sequencia_nf,    
                    nr_item_nf,     
                    cd_natureza_operacao,   
                    qt_item_nf,     
                    vl_unitario_item_nf,    
                    vl_total_item_nf,   
                    dt_atualizacao,     
                    nm_usuario,     
                    vl_frete,       
                    vl_desconto,        
                    vl_despesa_acessoria,   
                    cd_procedimento,    
                    ie_origem_proced,   
                    ds_observacao,      
                    vl_desconto_rateio, 
                    vl_seguro,      
                    nr_sequencia,       
                    vl_liquido,     
                    nr_seq_conta_financ
                ) values (
                    cd_estabelecimento_w,
                    cd_cgc_emitente_w,
                    cd_serie_nf_w,
                    nr_nota_fiscal_w,
                    nr_sequencia_nf_w,
                    1,
                    cd_natureza_operacao_w,
                    1,
                    vl_glosa_p,
                    vl_glosa_p,
                    sysdate,
                    nm_usuario_p,
                    0, 
                    0, 
                    0, 
                    cd_procedimento_w,
                    ie_origem_proced_w,
                    ds_observacao_w, 
                    0, 
                    0, 
                    nr_sequencia_w,
                    vl_glosa_p,
                    nr_seq_conta_financeira_w
                );
                atualiza_total_nota_fiscal(nr_sequencia_w, nm_usuario_p);

                atualizar_nota_fiscal(
                  nr_sequencia_p => nr_sequencia_w,
                  ie_acao_p => 'I',
                  nm_usuario_p => nm_usuario_p,
                  ie_tipo_nota_p => ie_tipo_nota_ww
                );

                nr_sequencia_p := nr_sequencia_w;

                commit;

            end if;
        end if;
    end if;

END GERAR_NF_GLOSA;
/
