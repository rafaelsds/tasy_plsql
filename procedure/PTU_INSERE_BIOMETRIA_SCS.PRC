create or replace
procedure ptu_insere_biometria_scs
			(	ds_biometria_p		Varchar2,
				nr_seq_segurado_p	Number,
				nr_seq_guia_p		Number,
				nr_seq_requisicao_p	Number,
				nr_seq_transacao_p	Number,
				ie_tipo_pedido_p	Varchar2,
				nm_usuario_p		Varchar2) is 

begin

if	(ie_tipo_pedido_p	= 'PA') then
	update	ptu_pedido_autorizacao
	set	ds_biometria	= ds_biometria_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_transacao_p;
	
	if	(nr_seq_guia_p	is not null)  then
		if	(nr_seq_segurado_p	is not null) then
			update	pls_guia_plano
			set	ds_biometria	= ds_biometria_p,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_guia_p;
		end if;
	elsif	(nr_seq_requisicao_p	is not null) then
		if	(nr_seq_segurado_p	is not null) then
			update	pls_requisicao
			set	ds_biometria	= ds_biometria_p,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_requisicao_p;
		end if;
	end if;
elsif	(ie_tipo_pedido_p	= 'PC') then
	update	ptu_pedido_compl_aut
	set	ds_biometria	= ds_biometria_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_transacao_p;
	
	if	(nr_seq_guia_p	is not null)  then
		if	(nr_seq_segurado_p	is not null) then
			update	pls_guia_plano
			set	ds_biometria	= ds_biometria_p,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_guia_p;
		end if;
	elsif	(nr_seq_requisicao_p	is not null) then
		if	(nr_seq_segurado_p	is not null) then
			update	pls_requisicao
			set	ds_biometria	= ds_biometria_p,
				dt_atualizacao	= sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_sequencia	= nr_seq_requisicao_p;
		end if;
	end if;
end if;

commit;

end ptu_insere_biometria_scs;
/