create or replace
procedure deletar_grupo_medic(	nr_sequencia_p	number) is 

begin

if	(nr_sequencia_p is not null) then

	delete from med_grupo_medic where nr_sequencia = nr_sequencia_p;

	commit;
end if;


end deletar_grupo_medic;
/