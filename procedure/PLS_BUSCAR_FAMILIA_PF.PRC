create or replace
procedure pls_buscar_familia_pf
			(ie_tipo_incremento_p 	varchar2,
			cd_matricula_familia_p 	number,
			cd_matricula_familia_pp number,
			nr_seq_regra_p		number,
			nr_seq_contrato_p	number,
			nr_seq_segurado_p	number,
			nr_seq_intercambio_p	number,
			nm_usuario_p		varchar2,
			cd_operadora_empresa_p	number,
			cd_matricula_p	out	number) is 
			
cd_atual_w			number(10);
cd_inicial_w			number(10);
cd_final_w			number(10);
cd_regra_w			number(10);
cd_operadora_empresa_w		number(10);
cd_matricula_w			number(10);
cd_matricula_familia_ww		number(10);

begin
/*Sequencial */
if	(ie_tipo_incremento_p	= 'SQ') then

	pls_buscar_familia_pf_seq ( ie_tipo_incremento_p, cd_matricula_familia_p, cd_matricula_familia_pp,
					nr_seq_regra_p, nr_seq_contrato_p, nr_seq_segurado_p,
					nr_seq_intercambio_p, nm_usuario_p,cd_operadora_empresa_p, cd_matricula_w);
/*Sequencia por empresa*/
elsif	(ie_tipo_incremento_p	= 'SE') then
	
	cd_operadora_empresa_w := cd_operadora_empresa_p;
	
	if	(cd_operadora_empresa_w is not null) then
		if	(nvl(cd_matricula_familia_p,0) = 0) then
			begin
			select	max(cd_inicial),
				max(cd_atual),
				nvl(max(cd_final),0)
			into	cd_inicial_w,
				cd_atual_w,
				cd_final_w
			from	pls_carteira_controle_iden
			where	nr_sequencia	= nr_seq_regra_p;
			exception
			when others then
				cd_inicial_w := 1;
			end;
			
			cd_regra_w :=	cd_inicial_w;	
			
			--Alterado para verificar se existe c�digo limite da regra, caso a mesma tenha, dever� dar um max apenas dentro da faixa de c�digos da regra 
			if	(cd_final_w > 0) then
				begin	
				select	max(cd_matricula_familia)
				into	cd_matricula_familia_ww
				from	pls_segurado
				where	cd_operadora_empresa	= cd_operadora_empresa_w
				and	cd_matricula_familia	<= cd_final_w;
				exception
				when others then
					cd_matricula_familia_ww	:= 0;
				end;
			else
				begin	
				select	max(cd_matricula_familia)
				into	cd_matricula_familia_ww
				from	pls_segurado
				where	cd_operadora_empresa	= cd_operadora_empresa_w;
				exception
				when others then
					cd_matricula_familia_ww	:= 0;
				end;
			end if;
			
			if	(nvl(cd_matricula_familia_ww,0) = 0) then
				
				cd_matricula_w	:=  cd_regra_w;
				
			elsif	(nvl(cd_matricula_familia_ww,0) > 0) then
			
				cd_matricula_w	:= cd_matricula_familia_ww +1;
			end if;

		else
			if	(nvl(cd_matricula_familia_ww,0) <> nvl(cd_matricula_familia_p,0)) then
				
				cd_matricula_w	:=  cd_matricula_familia_p;
				
			end if;
		end if;
	end if;	
/* Sequencia por contrato*/
elsif	(ie_tipo_incremento_p	= 'SC') then

	if	(nvl(cd_matricula_familia_p,0) = 0) then
		begin
		select	max(cd_inicial),
			max(cd_atual),
			nvl(max(cd_final),0)
		into	cd_inicial_w,
			cd_atual_w,
			cd_final_w
		from	pls_carteira_controle_iden
		where	nr_sequencia	= nr_seq_regra_p;
		exception
		when others then
			cd_inicial_w := 1;
		end;
		
		cd_regra_w :=	cd_inicial_w;
		
		--Alterado para verificar se existe c�digo limite da regra, caso a mesma tenha, dever� dar um max apenas dentro da faixa de c�digos da regra 
		if	(cd_final_w > 0) then
			if	(nr_seq_contrato_p is not null) then
				begin
				select	max(cd_matricula_familia)
				into	cd_matricula_familia_ww
				from	pls_segurado
				where	nr_seq_contrato	= nr_seq_contrato_p
				and	cd_matricula_familia	<= cd_final_w;
				exception
				when others then
					cd_matricula_familia_ww	:= 0;
				end;
			elsif	(nr_seq_intercambio_p is not null) then
				begin
				select	max(cd_matricula_familia)
				into	cd_matricula_familia_ww
				from	pls_segurado
				where	nr_seq_intercambio	= nr_seq_intercambio_p
				and	cd_matricula_familia	<= cd_final_w;
				exception
				when others then
					cd_matricula_familia_ww	:= 0;
				end;
			end if;
		else
			if	(nr_seq_contrato_p is not null) then
				begin
				select	max(cd_matricula_familia)
				into	cd_matricula_familia_ww
				from	pls_segurado
				where	nr_seq_contrato	= nr_seq_contrato_p;
				exception
				when others then
					cd_matricula_familia_ww	:= 0;
				end;
			elsif	(nr_seq_intercambio_p is not null) then
				begin
				select	max(cd_matricula_familia)
				into	cd_matricula_familia_ww
				from	pls_segurado
				where	nr_seq_intercambio	= nr_seq_intercambio_p;
				exception
				when others then
					cd_matricula_familia_ww	:= 0;
				end;
			end if;
		end if;
		
		if	(nvl(cd_matricula_familia_ww,0) = 0) then
		
			cd_matricula_w	:=  cd_regra_w;
	
		elsif	(nvl(cd_matricula_familia_ww,0) > 0) then
			cd_matricula_w	:= cd_matricula_familia_ww +1;
		end if;
	else
		if	(nvl(cd_matricula_familia_ww,0) <> nvl(cd_matricula_familia_p,0)) then
		
			cd_matricula_w	:=  cd_matricula_familia_p;
		end if;
	end if;
end if;

cd_matricula_p := cd_matricula_w;

end pls_buscar_familia_pf;
/
