CREATE OR REPLACE
PROCEDURE Gerar_cobranca_brasil_150	(	nr_seq_cobr_escrit_p	NUMBER,
						nm_usuario_p		VARCHAR2) IS


ie_tipo_registro_w		number(3);
cd_convenio_banco_w		varchar(100);
cd_pessoa_fisica_w		varchar(14);
cd_agencia_bancaria_w		varchar(8);
cd_banco_w			varchar(40);
cd_movimento_w			varchar(5);
cd_conta_w			varchar(15);
dt_geracao_w			date;
dt_vencimento_w			date;
ds_banco_w			varchar(40);
ds_mensagem_2_w			varchar(255);
ds_mensagem_3_w			varchar(255);
nm_empresa_w			varchar(80);
nr_seq_envio_w			number(10);
nr_titulo_w			number(10);
vl_escritural_w			number(15,2);
vl_tot_registros_w		number(15,2);
qt_registros_w			number(10);

cursor c01 is
select	ie_tipo_registro,
	cd_convenio_banco,
	cd_pessoa_fisica,
	cd_agencia_bancaria,
	cd_banco,
	cd_movimento,
	cd_conta,
	dt_geracao,
	dt_vencimento,
	ds_banco,
	ds_mensagem_2,
	ds_mensagem_3,
	nm_empresa,
	nr_seq_envio,
	nr_titulo,
	vl_escritural,
	vl_tot_registros
from	(
select	'01' ie_tipo_registro,
	c.cd_convenio_banco cd_convenio_banco,
	'' cd_pessoa_fisica,
	c.cd_agencia_bancaria cd_agencia_bancaria,
	c.cd_banco,
	'' cd_movimento,
	'' cd_conta,
	sysdate dt_geracao,
	null dt_vencimento,
	substr(obter_nome_banco(a.cd_banco),1,100) ds_banco,
	'' ds_mensagem_2,
	'' ds_mensagem_3,
	substr(obter_razao_social(b.cd_cgc),1,100) nm_empresa,
	a.nr_sequencia nr_seq_envio,
	0 nr_titulo,
	0 vl_escritural,
	0 vl_tot_registros
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
union
select	'10' ie_tipo_registro,
	'' cd_convenio_banco,
	 b.cd_pessoa_fisica,
	c.cd_agencia_bancaria cd_agencia_bancaria,
	0 cd_banco,
	'' cd_movimento,
	c.nr_conta,
	null dt_geracao,
	null dt_vencimento,
	'' ds_banco,
	'' ds_mensagem_2,
	'' ds_mensagem_3,
	'' nm_empresa,
	a.nr_sequencia nr_seq_envio,
	0 nr_titulo,
	0 vl_escritural,
	0 vl_tot_registros
from	cobranca_escrit_pf b,
		pessoa_fisica_conta c,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	c.cd_pessoa_fisica 	= b.cd_pessoa_fisica
and	c.dt_atualizacao 	= (	select	max(x.dt_atualizacao)
					from	pessoa_fisica_conta x
					where	x.cd_pessoa_fisica 	= c.cd_pessoa_fisica)
union
select	'15' ie_tipo_registro,
	'' cd_convenio_banco,
	 b.cd_pessoa_fisica,
	c.cd_agencia_bancaria cd_agencia_bancaria,
	0 cd_banco,
	decode(b.ie_acao,'A','0','E','1') cd_movimento,
	c.nr_conta,
	null dt_geracao,
	null dt_vencimento,
	'' ds_banco,
	'' ds_mensagem_2,
	'' ds_mensagem_3,
	'' nm_empresa,
	a.nr_sequencia nr_seq_envio,
	0 nr_titulo,
	0 vl_escritural,
	0 vl_tot_registros
from	cobranca_escrit_pf b,
	pessoa_fisica_conta c,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	c.cd_pessoa_fisica 	= b.cd_pessoa_fisica
and	b.ie_acao 		<> 'I'
and	c.dt_atualizacao 	= (	select 	max(x.dt_atualizacao)
					from	pessoa_fisica_conta x
					where	x.cd_pessoa_fisica 	= c.cd_pessoa_fisica)
union
select	'20' ie_tipo_registro,
	'' cd_convenio_banco,
	 b.cd_pessoa_fisica,
	c.cd_agencia_bancaria cd_agencia_bancaria,
	0 cd_banco,
	'' cd_movimento,
	c.nr_conta,
	null dt_geracao,
	b.dt_vencimento,
	'' ds_banco,
	'' ds_mensagem_2,
	'' ds_mensagem_3,
	'' nm_empresa,
	a.nr_sequencia nr_seq_envio,
	c.nr_titulo,
	c.vl_cobranca vl_escritural,
	0 vl_tot_registros
from	titulo_receber_v b,
	titulo_receber_cobr c,
	cobranca_escritural a
where	a.nr_sequencia		= c.nr_seq_cobranca
and	c.nr_titulo		= b.nr_titulo
union
select	'25' ie_tipo_registro,
	'' cd_convenio_banco,
	'' cd_pessoa_fisica,
	'' cd_agencia_bancaria,
	0 cd_banco,
	'' cd_movimento,
	'' cd_conta,
	null dt_geracao,
	null dt_vencimento,
	'' ds_banco,
	'' ds_mensagem_2,
	'' ds_mensagem_3,
	'' nm_empresa,
	a.nr_sequencia nr_seq_envio,
	0 nr_titulo,
	0 vl_escritural,
	sum(b.vl_cobranca) vl_tot_registros
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
group by a.nr_sequencia
)
where	nr_seq_envio = nr_seq_cobr_escrit_p;

begin

delete	from w_cobranca_banco;
commit;

open c01;
loop
fetch c01 into
	ie_tipo_registro_w,
	cd_convenio_banco_w,
	cd_pessoa_fisica_w,
	cd_agencia_bancaria_w,
	cd_banco_w,
	cd_movimento_w,
	cd_conta_w,
	dt_geracao_w,
	dt_vencimento_w,
	ds_banco_w,
	ds_mensagem_2_w,
	ds_mensagem_3_w,
	nm_empresa_w,
	nr_seq_envio_w,
	nr_titulo_w,
	vl_escritural_w,
	vl_tot_registros_w;
exit when c01%notfound;

qt_registros_w := qt_registros_w + 1;

insert	into	w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_registro,
		cd_convenio_banco,
		ds_cgc_cpf,
		cd_agencia_bancaria,
		cd_banco,
		cd_instrucao,
		cd_conta,
		dt_geracao,
		dt_vencimento,
		ds_banco,
		ds_mensagem_2,
		ds_mensagem_3,
		nm_empresa,
		nr_seq_envio,
		nr_titulo,
		vl_titulo,
		vl_tot_registros,
		qt_registros)
	values	(w_interf_itau_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		ie_tipo_registro_w,
		cd_convenio_banco_w,
		cd_pessoa_fisica_w,
		cd_agencia_bancaria_w,
		cd_banco_w,
		cd_movimento_w,
		cd_conta_w,
		dt_geracao_w,
		dt_vencimento_w,
		ds_banco_w,
		ds_mensagem_2_w,
		ds_mensagem_3_w,
		nm_empresa_w,
		nr_seq_envio_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_tot_registros_w,
		qt_registros_w);

end loop;
close c01;

commit;

end Gerar_cobranca_brasil_150;
/
