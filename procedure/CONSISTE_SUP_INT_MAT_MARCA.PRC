create or replace
procedure consiste_sup_int_mat_marca(
			nr_sequencia_p		number,
			cd_material_p		number,
			cd_unidade_medida_p 	varchar2,
			ie_padronizado_p	varchar2,
			nr_seq_status_aval_p	number,
			nm_usuario_p		varchar2) is
		
qt_existe_material_w		number(10);		
qt_existe_marca_w		number(10);
qt_existe_unidade_medida_w	number(10);
qt_existe_status_aval_w		number(10);
nr_inconsistencia_w		number(5);

begin

delete 	from sup_int_marca_consist
where	nr_sequencia = nr_sequencia_p
and	ie_integracao = '1';

select 	count(*)
into	qt_existe_material_w
from 	material
where 	cd_sistema_ant = to_char(cd_material_p);

select 	count(*)
into	qt_existe_marca_w
from 	marca
where	cd_sistema_ant = to_char(nr_sequencia_p);

if 	(qt_existe_material_w = 0) then

	select	nvl(max(nr_inconsistencia),0) + 1
	into	nr_inconsistencia_w
	from	sup_int_marca_consist
	where	nr_sequencia = nr_sequencia_p
	and	ie_integracao = '1';
	
	insert into sup_int_marca_consist(
		nr_sequencia,       
		nr_inconsistencia,
		ds_mensagem,        
		ie_integracao,
		dt_atualizacao,     
		nm_usuario,         
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_sequencia_p,
		nr_inconsistencia_w,
		wheb_mensagem_pck.get_texto(1043741),
		1,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p);

end if;

if 	(qt_existe_marca_w = 0) then

	select	nvl(max(nr_inconsistencia),0) + 1
	into	nr_inconsistencia_w
	from	sup_int_marca_consist
	where	nr_sequencia = nr_sequencia_p
	and	ie_integracao = '1';

	insert into sup_int_marca_consist(
		nr_sequencia,       
		nr_inconsistencia,
		ds_mensagem,        
		ie_integracao,
		dt_atualizacao,     
		nm_usuario,         
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_sequencia_p,
		nr_inconsistencia_w,
		wheb_mensagem_pck.get_texto(1043742),
		1,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p);
	
end if;

if	(cd_unidade_medida_p is not null) then

	select	count(*)
	into	qt_existe_unidade_medida_w
	from	unidade_medida
	where	upper(cd_unidade_medida) = upper(cd_unidade_medida_p);

	if	(qt_existe_unidade_medida_w = 0) then
	
		select	nvl(max(nr_inconsistencia),0) + 1
		into	nr_inconsistencia_w
		from	sup_int_marca_consist
		where	nr_sequencia = nr_sequencia_p
		and	ie_integracao = '1';
		
		insert into sup_int_marca_consist(
			nr_sequencia,       
			nr_inconsistencia,
			ds_mensagem,        
			ie_integracao,
			dt_atualizacao,     
			nm_usuario,         
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	nr_sequencia_p,
			nr_inconsistencia_w,
			wheb_mensagem_pck.get_texto(1043743),
			1,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p);
		
	end if;

end if;

if	(ie_padronizado_p is not null and ie_padronizado_p <> 'S' and ie_padronizado_p <> 'N') then

	select	nvl(max(nr_inconsistencia),0) + 1
	into	nr_inconsistencia_w
	from	sup_int_marca_consist
	where	nr_sequencia = nr_sequencia_p
	and	ie_integracao = '1';
	
	insert into sup_int_marca_consist(
		nr_sequencia,       
		nr_inconsistencia,
		ds_mensagem,        
		ie_integracao,
		dt_atualizacao,     
		nm_usuario,         
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_sequencia_p,
		nr_inconsistencia_w,
		wheb_mensagem_pck.get_texto(1043744),
		1,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p);

end if;

if	(nr_seq_status_aval_p is not null) then

	select	count(*)
	into	qt_existe_status_aval_w
	from	status_avaliacao_marca
	where	nr_sequencia = nr_seq_status_aval_p;

	if	(qt_existe_status_aval_w = 0) then
	
		select	nvl(max(nr_inconsistencia),0) + 1
		into	nr_inconsistencia_w
		from	sup_int_marca_consist
		where	nr_sequencia = nr_sequencia_p
		and	ie_integracao = '1';
		
		insert into sup_int_marca_consist(
			nr_sequencia,       
			nr_inconsistencia,
			ds_mensagem,        
			ie_integracao,
			dt_atualizacao,     
			nm_usuario,         
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	nr_sequencia_p,
			nr_inconsistencia_w,
			wheb_mensagem_pck.get_texto(1043745),
			1,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p);
		
	end if;

end if;

end consiste_sup_int_mat_marca;
/