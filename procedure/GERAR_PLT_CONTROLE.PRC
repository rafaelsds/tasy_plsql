create or replace
procedure Gerar_plt_controle(	cd_pessoa_fisica_p	varchar2,
				nr_atendimento_p	number,
				nm_usuario_p		Varchar2) is 

cont_w		number(5);

begin

if	(nvl(nr_atendimento_p,0) > 0) or
	(cd_pessoa_fisica_p	<> '') then
	
	select	count(*)
	into	cont_w
	from	plt_controle
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	nvl(nr_atendimento,nvl(nr_atendimento_p,0))	= nvl(nr_atendimento_p,0)
	and	nm_usuario		= nm_usuario_p;

	if	(cont_w = 0) then
		insert	into plt_controle (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_atendimento,
			cd_pessoa_fisica,
			ie_atualizar_dieta,
			ie_atualizar_jejum,
			ie_atualizar_supl,
			ie_atualizar_sne,
			ie_atualizar_npt_adulta,
			ie_atualizar_npt_neo,
			ie_atualizar_npt_ped,
			ie_atualizar_leite,
			ie_atualizar_solucao,
			ie_atualizar_medic,
			ie_atualizar_material,
			ie_atualizar_proced,
			ie_atualizar_lab,
			ie_atualizar_ccg,
			ie_atualizar_cig,
			ie_atualizar_ivc,
			ie_atualizar_gas,
			ie_atualizar_hemoterap,
			ie_atualizar_rec,
			ie_atualizar_dialise)
		values	(Plt_controle_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_atendimento_p,
				cd_pessoa_fisica_p,
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S',
				'S');
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	else
		Atualizar_plt_controle(nm_usuario_p, nr_atendimento_p, cd_pessoa_fisica_p, 'TODOS', 'S', null);
	end if;
end if;

end Gerar_plt_controle;
/
