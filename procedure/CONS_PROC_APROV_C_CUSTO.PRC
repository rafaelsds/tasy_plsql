create or replace
procedure cons_proc_aprov_c_custo( nr_ordem_compra_p	number,
									ds_erro_p			out	varchar2 ) is

--consiste ao liberar OC, se o processo aprova��o � por centro de custo, e a ordem possui essa informa��o preenchida.

cd_material_w			ordem_compra_item.cd_material%type;
cd_centro_custo_w		ordem_compra_item.cd_centro_custo%type;
cd_setor_atendimento_w	ordem_compra.cd_setor_atendimento%type;
cd_local_estoque_w		ordem_compra_item.cd_local_estoque%type;
cd_conta_contabil_w		ordem_compra_item.cd_conta_contabil%type;	
cd_cgc_fornecedor_w		ordem_compra.cd_cgc_fornecedor%type;
cd_pessoa_fisica_w		ordem_compra.cd_pessoa_fisica%type;
ie_urgente_w			ordem_compra.ie_urgente%type;
cd_estabelecimento_w	ordem_compra.cd_estabelecimento%type;
cd_perfil_w				ordem_compra.cd_perfil%type;
nr_seq_proj_rec_w		ordem_compra_item.nr_seq_proj_rec%type;
nr_ordem_compra_w		ordem_compra_item.nr_ordem_compra%type;
ie_responsavel_w		processo_aprov_resp.ie_responsavel%type;
cd_processo_aprov_w		processo_aprov_resp.cd_processo_aprov%type;
ie_tipo_processo_w		varchar2(01);
ie_tipo_ordem_w			ordem_compra.ie_tipo_ordem%type;
ds_erro_w				varchar2(255) := '';


cursor c01 is
select	oci.cd_material,
	oci.cd_centro_custo,
	oc.cd_setor_atendimento,
	oci.cd_local_estoque,
	oci.cd_conta_contabil,
	oc.cd_cgc_fornecedor,
	oc.cd_pessoa_fisica,
	oc.ie_urgente,
	oc.cd_estabelecimento,
	oc.cd_perfil,
	oci.nr_seq_proj_rec,
	oci.nr_ordem_compra
from	ordem_compra oc,
	ordem_compra_item oci
where	oc.nr_ordem_compra = oci.nr_ordem_compra
and	oc.nr_ordem_compra = nr_ordem_compra_p;

cursor c02 is
select	ie_responsavel
from	processo_aprov_resp
where	cd_processo_Aprov		= cd_processo_aprov_w
and	ie_ordem_compra		= 'S'
and	ie_tipo_ordem_w		<> 'T'
union
select	ie_responsavel
from	processo_aprov_resp
where	cd_processo_Aprov		= cd_processo_aprov_w
and	ie_ordem_compra_transf	= 'S'
and	ie_tipo_ordem_w		= 'T'
union
select	ie_responsavel
from	processo_aprov_resp
where	cd_processo_Aprov		= cd_processo_aprov_w
and	ie_transf_pcs	= 'S'
and	ie_tipo_ordem_w		= 'Z'
order by 1;

begin

open C01;
loop
fetch C01 into
	cd_material_w,			
	cd_centro_custo_w,		
	cd_setor_atendimento_w,	
	cd_local_estoque_w,		
	cd_conta_contabil_w,		
	cd_cgc_fornecedor_w,		
	cd_pessoa_fisica_w,		
	ie_urgente_w,			
	cd_estabelecimento_w,	
	cd_perfil_w,				
	nr_seq_proj_rec_w,		
	nr_ordem_compra_w;		
exit when C01%notfound;
	begin
	
	select 	nvl(max(ie_tipo_ordem),'N')
	into	ie_tipo_ordem_w
	from	ordem_compra
	where	nr_ordem_compra	= nr_ordem_compra_w;
	
	ie_tipo_processo_w		:= 'O';	
	
	if	(ie_tipo_ordem_w = 'T') then
		ie_tipo_processo_w	:= 'T';
	elsif	(ie_tipo_ordem_w = 'Z') then
		ie_tipo_processo_w	:= 'Z';		
	end if;
	
	obter_processo_aprovacao(
		cd_material_w,
		cd_centro_custo_w,
		cd_setor_atendimento_w,
		cd_local_estoque_w,
		null, 
		null, 
		cd_conta_contabil_w,
		cd_cgc_fornecedor_w,
		cd_pessoa_fisica_w,
		ie_tipo_processo_w,
		ie_urgente_w,
		cd_estabelecimento_w,
		cd_perfil_w,
		nr_seq_proj_rec_w,
		nr_ordem_compra_w,
		cd_processo_aprov_w);
	
	open c02;
	loop
	fetch C02 into
		ie_responsavel_w;
	exit when c02%notfound;
		begin
		
		if (nvl(ie_responsavel_w,'') = 'R') and
			(cd_centro_custo_w is null) then
			ds_erro_w := substr(wheb_mensagem_pck.get_texto(763952),1,255);
		end if;
		
		end;
	end loop;
	close c02;	

	end;
end loop;
close C01;	

ds_erro_p := ds_erro_w;
			
end cons_proc_aprov_c_custo;
/