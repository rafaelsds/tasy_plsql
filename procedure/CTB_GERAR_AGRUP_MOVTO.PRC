create or replace
procedure ctb_gerar_agrup_movto(
		nr_lote_contabil_p		number,
		nm_usuario_p			varchar2) is

cursor C01 is
	select	nr_seq_agrupamento,
		dt_movto,
		nr_seq_partida,
		rownum nr_agrup_sequencial
	from	(
		select	nr_seq_agrupamento,
			dt_movto,
			min(nr_seq_partida) keep (dense_rank first order by qt_conta,nr_seq_partida) nr_seq_partida
		from	(
			select	count(cd_conta_debito) qt_conta, /* DEBITO */
				nr_seq_agrupamento,
				ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_movimento) dt_movto,
				min(nr_sequencia) nr_seq_partida
			from	ctb_movimento
			where	nr_lote_contabil = nr_lote_contabil_p
			and	cd_conta_debito is not null
			group by
				nr_seq_agrupamento,
				ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_movimento)
			union all
			select	count(cd_conta_credito) qt_conta, /* CREDITO */
				nr_seq_agrupamento,
				ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_movimento) dt_movto,
				min(nr_sequencia) nr_seq_partida
			from	ctb_movimento
			where	nr_lote_contabil = nr_lote_contabil_p
			and	cd_conta_credito is not null
			group by
				nr_seq_agrupamento,
				ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_movimento)
			)
		group by
			nr_seq_agrupamento,
			dt_movto
		);

type C01_type is table of C01%rowtype;
C01_regs_w C01_type;

begin
if	(nvl(philips_contabil_pck.get_ie_reat_saldo(),'N') = 'N') then
	begin
	update 	ctb_movimento
	set	nr_agrup_sequencial 	= 0
	where	nr_lote_contabil	= nr_lote_contabil_p;
	end;
end if;

open C01;
loop
fetch C01 bulk collect into C01_regs_w limit 1000;
	begin
	for i in 1..C01_regs_w.count loop
		begin
		update	ctb_movimento a
		set	a.nr_seq_movto_partida = decode(C01_regs_w(i).nr_seq_partida,a.nr_sequencia,null,C01_regs_w(i).nr_seq_partida),
			nr_agrup_sequencial = C01_regs_w(i).nr_agrup_sequencial
		where	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(a.dt_movimento) = C01_regs_w(i).dt_movto
		and	a.nr_lote_contabil = nr_lote_contabil_p
		and	a.nr_seq_agrupamento = C01_regs_w(i).nr_seq_agrupamento;
		end;
	end loop;
	commit;
	end;
exit when C01%notfound;
end loop;
close C01;

commit;

end ctb_gerar_agrup_movto;
/
