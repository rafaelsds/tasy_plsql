create or replace procedure qua_verifica_pf_cargo_trein(nm_usuario_p	varchar2) is 
			
nr_sequencia_w				number(10);
nm_usuario_exec_w			varchar2(15);
qt_existe_w					number(10,0);	
nr_seq_doc_w				number(10,0);
nm_documento_w				varchar2(255);
nm_pessoa_treinada_w		varchar2(60);
cd_pessoa_fisica_w			varchar2(10);
ds_lista_treinados_w		varchar2(3000);
ds_dano_w					varchar2(4000);
dt_ordem_servico_w			date;
nr_seq_hist_w				number(10,0);
cd_estabelecimento_w		number(04,0) := wheb_usuario_pck.get_cd_estabelecimento;

nr_seq_localizacao_w		man_localizacao.nr_sequencia%type;
cd_cnpj_w					man_localizacao.cd_cnpj%type;
nr_seq_idioma_w				pessoa_juridica.nr_seq_idioma%type;
ds_relat_tecnico_w			man_ordem_serv_tecnico.ds_relat_tecnico%type;

Cursor C01 is
select	w.nr_sequencia,
	w.nm_documento
from	qua_documento w
where	w.nr_seq_tipo = 2
and	w.ie_situacao = 'A'
and	nr_sequencia = 574
and	exists (select	1
		from	qua_doc_grupo_cargo x
		where	x.nr_seq_doc = w.nr_sequencia)
order by 2;


Cursor C02 is
select	d.cd_pessoa_fisica,
	SUBSTR(OBTER_NOME_PF(d.CD_PESSOA_FISICA),0,60) nm_pessoa_treinada
from	pessoa_fisica d,
	qua_grupo_cargo b,
	qua_cargo_agrup c,
	qua_doc_grupo_cargo a
where	a.nr_seq_grupo		= b.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_agrup
and	c.cd_cargo 		= d.cd_cargo
and	a.nr_seq_doc	 	= nr_seq_doc_w
and	d.ie_funcionario 	= 'S'
and	d.dt_cadastro_original  >= to_date('13/09/2011','dd/mm/yyyy')
and	not exists (	select	1
			from	qua_doc_treinamento j,
				qua_doc_trein_pessoa y
			where	j.nr_sequencia = y.nr_seq_treinamento
			and	j.nr_seq_documento = a.nr_seq_doc
			and	d.cd_pessoa_fisica = y.cd_pessoa_fisica)
union
select	d.cd_pessoa_fisica,
	SUBSTR(OBTER_NOME_PF(d.CD_PESSOA_FISICA),0,60) nm_pessoa_treinada
from	pessoa_fisica d,
	qua_grupo_cargo b,
	qua_cargo_agrup c,
	qua_doc_grupo_cargo a
where	a.nr_seq_grupo		= b.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_agrup
and	c.cd_cargo 		= d.cd_cargo
and	a.nr_seq_doc	 	= nr_seq_doc_w
and	d.ie_funcionario 	= 'S'
and	d.dt_cadastro_original  >= to_date('13/09/2011','dd/mm/yyyy')
and	exists (	select	1
			from	qua_doc_treinamento j,
				qua_doc_trein_pessoa y
			where	j.nr_sequencia = y.nr_seq_treinamento
			and	j.nr_seq_documento = a.nr_seq_doc
			and	d.cd_pessoa_fisica = y.cd_pessoa_fisica
			and	y.ie_faltou = 'S')
order by 2;


cursor c03 is
select	d.cd_pessoa_fisica,
	SUBSTR(OBTER_NOME_PF(d.CD_PESSOA_FISICA),0,60) nm_pessoa_treinada
from	pessoa_fisica d,
	qua_grupo_cargo b,
	qua_cargo_agrup c,
	qua_doc_grupo_cargo a
where	a.nr_seq_grupo		= b.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_agrup
and	c.cd_cargo 		= d.cd_cargo
and	a.nr_seq_doc	 	= nr_seq_doc_w
and	d.ie_funcionario 	= 'S'
and	not exists (	select	1
			from	qua_doc_treinamento j,
				qua_doc_trein_pessoa y
			where	j.nr_sequencia = y.nr_seq_treinamento
			and	j.nr_seq_documento = a.nr_seq_doc
			and	d.cd_pessoa_fisica = y.cd_pessoa_fisica)
and	d.dt_atualizacao_nrec > dt_ordem_servico_w
union
select	d.cd_pessoa_fisica,
	SUBSTR(OBTER_NOME_PF(d.CD_PESSOA_FISICA),0,60) nm_pessoa_treinada
from	pessoa_fisica d,
	qua_grupo_cargo b,
	qua_cargo_agrup c,
	qua_doc_grupo_cargo a
where	a.nr_seq_grupo		= b.nr_sequencia
and	b.nr_sequencia		= c.nr_seq_agrup
and	c.cd_cargo 		= d.cd_cargo
and	a.nr_seq_doc	 	= nr_seq_doc_w
and	d.ie_funcionario 	= 'S'
and	exists (	select	1
			from	qua_doc_treinamento j,
				qua_doc_trein_pessoa y
			where	j.nr_sequencia = y.nr_seq_treinamento
			and	j.nr_seq_documento = a.nr_seq_doc
			and	d.cd_pessoa_fisica = y.cd_pessoa_fisica
			and	y.ie_faltou = 'S')
and	d.dt_atualizacao_nrec > dt_ordem_servico_w
order by 2;

begin

open C01;
loop
fetch C01 into	
	nr_seq_doc_w,
	nm_documento_w;
exit when C01%notfound;
	begin	
	ds_lista_treinados_w:= null;
	
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	man_ordem_servico
	where	nr_seq_documento = nr_seq_doc_w
	and	ie_status_ordem <> '3'
	and	ie_tipo_ordem = '13';
	
	if	(nvl(nr_sequencia_w,0) = 0) then
		open C02;
		loop
		fetch C02 into	
			cd_pessoa_fisica_w,
			nm_pessoa_treinada_w;
		exit when C02%notfound;
			begin	
			ds_lista_treinados_w	:= Substr(ds_lista_treinados_w || nm_pessoa_treinada_w || chr(13) || chr(10),1,3000);
			end;
		end loop;
		close C02;
	
		ds_dano_w := Substr('As pessoas abaixo n�o possuem treinamento no documento ' || nm_documento_w || chr(13) || chr(10) || Substr(ds_lista_treinados_w,1,3000),1,4000);

		--Gera��o da OS por documento
		if	(nvl(ds_lista_treinados_w,'X') <> 'X') then
			begin
			select	man_ordem_servico_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert	into man_ordem_servico(
				nr_sequencia,
				cd_pessoa_solicitante,
				dt_ordem_servico,
				ie_prioridade,
				ie_parado,
				ds_dano_breve,
				dt_atualizacao,
				nm_usuario,
				dt_inicio_desejado,
				dt_conclusao_desejada,
				ds_dano,
				dt_inicio_previsto,
				dt_fim_previsto,
				dt_inicio_real,
				ie_tipo_ordem,
				ie_status_ordem,
				nr_grupo_planej,
				nr_grupo_trabalho,
				nm_usuario_exec,
				qt_contador,
				nr_seq_estagio,
				ie_classificacao,
				ie_forma_receb,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_origem_os,
				nr_seq_documento)
			values(	nr_sequencia_w,				-- nr_sequencia,
				'7123',					-- cd_pessoa_solicitante,		--Juliana
				sysdate,				-- dt_ordem_servico,
				'M',					-- ie_prioridade,
				'N',					-- ie_parado,
				substr('Necessidade treinamento. Documento ' || nm_documento_w,1,80),		-- ds_dano_breve,
				sysdate,				-- dt_atualizacao,
				nm_usuario_p,				-- nm_usuario,
				sysdate,				-- dt_inicio_desejado,
				(sysdate + 30),				-- dt_conclusao_desejada,
				ds_dano_w, 				-- ds_dano,
				sysdate,				-- dt_inicio_previsto,
				(sysdate + 30),				-- dt_fim_previsto,
				sysdate,				-- dt_inicio_real,
				'13',					-- ie_tipo_ordem,		--A��es da qualidade
				'1',					-- ie_status_ordem,
				21,					-- nr_grupo_planej,		-- Qualidade
				12,					-- nr_grupo_trabalho,		-- Qualidade
				'ntbusarello', 				-- nm_usuario_exec,
				null,					-- qt_contador,
				1871,					-- nr_seq_estagio,		--Qua - treinamento de documentos
				'S',					-- ie_classificacao,
				'U',					-- ie_forma_receb,
				sysdate,				-- dt_atualizacao_nrec,
				nm_usuario_p,				-- nm_usuario_nrec,
				'6',					-- ie_origem_os		--Qualidade
				nr_seq_doc_w);
				
			insert into  man_ordem_servico_exec(
				nr_sequencia,
				nr_seq_ordem,
				dt_atualizacao,
				nm_usuario,
				nm_usuario_exec,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	man_ordem_servico_exec_seq.nextval,
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				'ntbusarello',
				sysdate,
				nm_usuario_p);
			end;
		end if;
	else
		/*Se j� tem uma OS aberta*/
		begin
		select	dt_ordem_servico,
				nr_seq_localizacao
		into	dt_ordem_servico_w,
				nr_seq_localizacao_w
		from	man_ordem_servico
		where	nr_sequencia = nr_sequencia_w;
	
		ds_lista_treinados_w:= null;
		open C03;
		loop
		fetch C03 into	
			cd_pessoa_fisica_w,
			nm_pessoa_treinada_w;
		exit when C03%notfound;
			begin	
			ds_lista_treinados_w	:= Substr(ds_lista_treinados_w || nm_pessoa_treinada_w || chr(13) || chr(10),1,3000);
			
			if	(nvl(ds_lista_treinados_w,'X') <> 'X') then
				select	man_ordem_serv_tecnico_seq.nextval
				into	nr_seq_hist_w
				from	dual;
				
				ds_relat_tecnico_w := 'Novos funcion�rios a serem treinados: ' || chr(13) || chr(10) || ds_lista_treinados_w;
				if	(nr_seq_localizacao_w is not null) then
					select	max(a.cd_cnpj)
					into	cd_cnpj_w
					from	man_localizacao	a
					where	a.nr_sequencia	= nr_seq_localizacao_w;
					
					if	(cd_cnpj_w is not null) then
						select	max(a.nr_seq_idioma)
						into	nr_seq_idioma_w
						from	pessoa_juridica	a
						where	a.cd_cgc	= cd_cnpj_w;						
						
						if	(nr_seq_idioma_w is not null) then
							ds_relat_tecnico_w	:= substr(obter_texto_dic_objeto(338132, nr_seq_idioma_w, 'FUNCIONARIO='||ds_lista_treinados_w), 1, 255);
						end if;
					end if;
				end if;
				
				insert into man_ordem_serv_tecnico (
						nr_sequencia,
						nr_seq_ordem_serv,
						dt_atualizacao,
						nm_usuario,
						ds_relat_tecnico,
						dt_historico,
						ie_origem,
						nr_seq_tipo,
						dt_liberacao,
						nm_usuario_lib)
					values(	nr_seq_hist_w,
						nr_sequencia_w,
						sysdate,
						nm_usuario_p,
						ds_relat_tecnico_w,
						sysdate,
						'I',
						1,
						sysdate,
						nm_usuario_p);

				man_comunicar_historico(nr_seq_hist_w, 'E', nm_usuario_p, 'N', cd_estabelecimento_w);
			end if;
			end;
		end loop;
		close C03;
		
		end;
	end if;
	end;
end loop;
close C01;

commit;

end qua_verifica_pf_cargo_trein;
/