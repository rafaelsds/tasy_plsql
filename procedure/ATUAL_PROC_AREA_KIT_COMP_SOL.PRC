create or replace
procedure atual_proc_area_kit_comp_sol (
				nr_seq_processo_p	number,
				nr_prescricao_p	number,
				nr_seq_solucao_p	number,
				dt_horario_p	date,
				nr_seq_material_p	number,
				ie_sup_oral_p		varchar2) is
				
nr_seq_material_w		number(6,0);
nr_seq_area_prep_w	number(10,0);
varsql_row_count_w	number(10);
ds_erro_w		varchar2(1800);
ie_grava_log_gedipa_w	varchar2(1);

cursor c01 is
select	a.nr_seq_material,
		a.nr_seq_area_prep
from   	prescr_mat_hor a,
		prescr_material b,
		prescr_solucao c
where  	a.nr_prescricao		= b.nr_prescricao
and		a.nr_seq_material		= b.nr_sequencia
and		a.ie_agrupador			= 4
and		a.nr_seq_processo		= nr_seq_processo_p
and		a.dt_horario			= dt_horario_p
and		b.ie_agrupador			= 4
and		b.nr_prescricao			= c.nr_prescricao
and		b.nr_sequencia_solucao	= c.nr_seq_solucao
and		c.nr_prescricao			= nr_prescricao_p
and		c.nr_seq_solucao		= nr_seq_solucao_p
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
group by
		a.nr_seq_material,
		a.nr_seq_area_prep
order by
		a.nr_seq_material,
		a.nr_seq_area_prep;
	
cursor c02 is
select	a.nr_seq_material,
		a.nr_seq_area_prep
from   	prescr_mat_hor a,
		prescr_material b	
where  	a.nr_prescricao		= b.nr_prescricao
and		a.nr_seq_material	= b.nr_sequencia
and		a.ie_agrupador		= 8
and		b.ie_agrupador		= 8
and		a.nr_seq_processo	= nr_seq_processo_p
and		b.nr_prescricao 	= nr_prescricao_p
and		b.nr_sequencia 		= nr_seq_material_p
and		a.dt_horario		= dt_horario_p
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
group by
		a.nr_seq_material,
		a.nr_seq_area_prep
order by
		a.nr_seq_material,
		a.nr_seq_area_prep;
	
cursor c03 is
select	a.nr_seq_material,
		a.nr_seq_area_prep
from   	prescr_mat_hor a,
		prescr_material b	
where  	a.nr_prescricao		= b.nr_prescricao
and		a.nr_seq_material	= b.nr_sequencia
and		a.ie_agrupador		= 12
and		b.ie_agrupador		= 12
and		a.nr_seq_processo	= nr_seq_processo_p
and		b.nr_prescricao 	= nr_prescricao_p
and		b.nr_sequencia 		= nr_seq_material_p
and		a.dt_horario		= dt_horario_p
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
group by
		a.nr_seq_material,
		a.nr_seq_area_prep
order by
		a.nr_seq_material,
		a.nr_seq_area_prep;
					
begin

select	nvl(max(ie_grava_log_gedipa),'S')
into	ie_grava_log_gedipa_w
from	parametros_farmacia
where	cd_estabelecimento = nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

if (ie_grava_log_gedipa_w = 'S') then
	insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
	values (obter_nextval_sequence('log_gedipa'), sysdate, 905, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, 'nr_seq_processo_p='||to_char(nr_seq_processo_p)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_solucao_p='||to_char(nr_seq_solucao_p)||'; dt_horario_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone), obter_desc_expressao(292013) || ' da procedure ATUAL_PROC_AREA_KIT_COMP_SOL');
end if;

if	(nr_seq_processo_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is not null) and
	(dt_horario_p is not null) then
	begin
	
	if (ie_grava_log_gedipa_w = 'S') then
		insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
		values (obter_nextval_sequence('log_gedipa'), sysdate, 910, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'Open do cursor c01');
	end if;
	
	open c01;
	loop
	fetch c01 into	
			nr_seq_material_w,
			nr_seq_area_prep_w;
	exit when c01%notfound;
		begin
		
		if (ie_grava_log_gedipa_w = 'S') then
			insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
			values (obter_nextval_sequence('log_gedipa'), sysdate, 915, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null,'nr_seq_processo_p='||to_char(nr_seq_processo_p)||'; nr_seq_area_prep_w='||to_char(nr_seq_area_prep_w)||'; dt_horario_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_material_w='||to_char(nr_seq_material_w), 'Update 01 na tabela PRESCR_MAT_HOR');
		end if;
		
		update	prescr_mat_hor a
		set		a.nr_seq_processo	= nr_seq_processo_p,
				a.nr_seq_area_prep	= nr_seq_area_prep_w
		where	a.nr_sequencia in (
					select	x.nr_sequencia
					from	prescr_mat_hor x,
							prescr_material y
					where	x.nr_prescricao		= y.nr_prescricao
					and		x.nr_seq_material	= y.nr_sequencia
					and		y.ie_agrupador 		in (4,2)
					and 	x.ie_agrupador 		in (4,2)
					and		x.dt_horario		= dt_horario_p
					and		y.nr_prescricao		= nr_prescricao_p
					and		y.nr_seq_kit		= nr_seq_material_w
					and		Obter_se_horario_liberado(x.dt_lib_horario, x.dt_horario) = 'S');
					
		if (ie_grava_log_gedipa_w = 'S') then			
			insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
			values (obter_nextval_sequence('log_gedipa'), sysdate, 920, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'End fetch do cursor c01');							
		end if;
		end;
	end loop;
	close c01;
	
	if (ie_grava_log_gedipa_w = 'S') then
		insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
		values (obter_nextval_sequence('log_gedipa'), sysdate, 925, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'Close do cursor c01');								
	end if;
	end;	
	
elsif	(nr_seq_processo_p is not null) and
		(nr_prescricao_p is not null) and
		(nr_seq_solucao_p is null) and
		(nr_seq_material_p is not null) and
		(nvl(ie_sup_oral_p,'N') = 'N') and
		(dt_horario_p is not null) then
	begin
	
	if (ie_grava_log_gedipa_w = 'S') then
		insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
		values (obter_nextval_sequence('log_gedipa'), sysdate, 910, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'Open do cursor c02');
	end if;
	
	open c02;
	loop
	fetch c02 into	nr_seq_material_w,
			nr_seq_area_prep_w;
	exit when c02%notfound;
		begin

		if (ie_grava_log_gedipa_w = 'S') then
			insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
			values (obter_nextval_sequence('log_gedipa'), sysdate, 915, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null,'nr_seq_processo_p='||to_char(nr_seq_processo_p)||'; nr_seq_area_prep_w='||to_char(nr_seq_area_prep_w)||'; dt_horario_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_material_w='||to_char(nr_seq_material_w), 'Update 01 na tabela PRESCR_MAT_HOR-2');
		end if;
		
		update	prescr_mat_hor a
		set		a.nr_seq_processo	= nr_seq_processo_p,
					a.nr_seq_area_prep	= nr_seq_area_prep_w
		where	a.nr_sequencia in (
					select	x.nr_sequencia
					from	prescr_mat_hor x,
							prescr_material y
					where	x.nr_prescricao		= y.nr_prescricao
					and		x.nr_seq_material	= y.nr_sequencia
					and		y.ie_agrupador 		= 8
					and 	x.ie_agrupador 		= 8
					and		x.dt_horario		= dt_horario_p
					and		y.nr_prescricao		= nr_prescricao_p
					and		y.nr_seq_kit		= nr_seq_material_w
					and		Obter_se_horario_liberado(x.dt_lib_horario, x.dt_horario) = 'S');
		
		if (ie_grava_log_gedipa_w = 'S') then
			insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
			values (obter_nextval_sequence('log_gedipa'), sysdate, 920, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'End fetch do cursor c02');							
		end if;
		end;
	end loop;
	close c02;
	
	if (ie_grava_log_gedipa_w = 'S') then
		insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
		values (obter_nextval_sequence('log_gedipa'), sysdate, 925, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'Close do cursor c02');								
	end if;
	
	end;
elsif	(nr_seq_processo_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_solucao_p is null) and
	(nr_seq_material_p is not null) and
	(nvl(ie_sup_oral_p,'N') = 'S') and
	(dt_horario_p is not null) then
	begin
	
	if (ie_grava_log_gedipa_w = 'S') then
		insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
		values (obter_nextval_sequence('log_gedipa'), sysdate, 910, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'Open do cursor c03');
	end if;
	
	open c03;
	loop
	fetch c03 into	nr_seq_material_w,
			nr_seq_area_prep_w;
	exit when c03%notfound;
		begin
		
		if (ie_grava_log_gedipa_w = 'S') then
			insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
			values (obter_nextval_sequence('log_gedipa'), sysdate, 915, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null,'nr_seq_processo_p='||to_char(nr_seq_processo_p)||'; nr_seq_area_prep_w='||to_char(nr_seq_area_prep_w)||'; dt_horario_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_material_w='||to_char(nr_seq_material_w), 'Update 01 na tabela PRESCR_MAT_HOR-3');
		end if;
		
		update	prescr_mat_hor a
		set	a.nr_seq_processo	= nr_seq_processo_p,
			a.nr_seq_area_prep	= nr_seq_area_prep_w
		where	a.nr_sequencia in (
					select	x.nr_sequencia
					from	prescr_mat_hor x,
						prescr_material y
					where	x.nr_prescricao		= y.nr_prescricao
					and	x.nr_seq_material	= y.nr_sequencia
					and	y.ie_agrupador 		= 12
					and 	x.ie_agrupador 		= 12
					and	x.dt_horario		= dt_horario_p
					and	y.nr_prescricao		= nr_prescricao_p
					and	y.nr_seq_kit		= nr_seq_material_w
					and	Obter_se_horario_liberado(x.dt_lib_horario, x.dt_horario) = 'S');
					
		if (ie_grava_log_gedipa_w = 'S') then
			insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
			values (obter_nextval_sequence('log_gedipa'), sysdate, 920, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'End fetch do cursor c02');
		end if;
		end;
	end loop;
	close c03;
	
	if (ie_grava_log_gedipa_w = 'S') then		
		insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
		values (obter_nextval_sequence('log_gedipa'), sysdate, 925, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, null, 'Close do cursor c03');
	end if;	
	
	end;
end if;
if (ie_grava_log_gedipa_w = 'S') then
	insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
	values (obter_nextval_sequence('log_gedipa'), sysdate, 930, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, 'nr_seq_processo_p='||to_char(nr_seq_processo_p)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_solucao_p='||to_char(nr_seq_solucao_p)||'; dt_horario_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone), obter_desc_expressao(290116) || ' da procedure ATUAL_PROC_AREA_KIT_COMP_SOL');
end if;
exception
when others then
	ds_erro_w	:= substr(SQLERRM(sqlcode),1,1800);
	if (ie_grava_log_gedipa_w = 'S') then
		insert into log_gedipa (nr_sequencia, dt_log, nr_log, nm_objeto_execucao, nm_objeto_chamado, ds_parametros, ds_log)
		values (obter_nextval_sequence('log_gedipa'), sysdate, 905, 'ATUAL_PROC_AREA_KIT_COMP_SOL', null, 'nr_seq_processo_p='||to_char(nr_seq_processo_p)||'; nr_prescricao_p='||to_char(nr_prescricao_p)||'; nr_seq_solucao_p='||to_char(nr_seq_solucao_p)||'; dt_horario_p='||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(dt_horario_p, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone), obter_desc_expressao(289744) || ' na ' || lower(obter_desc_expressao(308293)) || ' da procedure ATUAL_PROC_AREA_KIT_COMP_SOL');
	end if;
	
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end atual_proc_area_kit_comp_sol;
/
