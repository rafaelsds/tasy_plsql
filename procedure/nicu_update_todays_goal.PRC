create or replace procedure
NICU_UPDATE_TODAYS_GOAL (	nr_encounter_p number,
							nm_usuario_p varchar,
							ds_goal_p varchar) is
							
vl_count number(10);

begin

	select 	count(*)
	into	vl_count
	from	NICU_PATIENT_GOAL
	where	nr_seq_encounter = nr_encounter_p;
	
	if (vl_count > 0) then
		UPDATE 	NICU_PATIENT_GOAL
		set		dt_atualizacao = sysdate,
				dt_atualizacao_nrec = sysdate,
				nm_usuario = nm_usuario_p,
				ds_goal = ds_goal_p
		where 	nr_seq_encounter = nr_encounter_p;
	else
		insert into NICU_PATIENT_GOAL (NR_SEQUENCIA,
		NR_SEQ_ENCOUNTER,
		DT_GOAL_DATE,
		DT_ATUALIZACAO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO,
		NM_USUARIO_NREC,
		DS_GOAL)
		values( nicu_patient_goal_seq.nextval,
		nr_encounter_p,
		sysdate,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		ds_goal_p
		);	
	end if;
	commit;
end NICU_UPDATE_TODAYS_GOAL;
/

