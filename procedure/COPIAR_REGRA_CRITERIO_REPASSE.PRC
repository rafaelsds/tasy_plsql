create or replace procedure copiar_regra_criterio_repasse(cd_regra_p		number,
										cd_estab_dest_p	number) is 

cd_regra_new_w							regra_repasse_terceiro.cd_regra%type;
nr_seq_proc_crit_repasse_new_w			proc_criterio_repasse.nr_sequencia%type;
nr_seq_proc_crit_repasse_old_w			proc_criterio_repasse.nr_sequencia%type;
nr_seq_mat_crit_repasse_old_w			mat_criterio_repasse.nr_sequencia%type;
nr_seq_mat_crit_repasse_new_w			mat_criterio_repasse.nr_sequencia%type;
nr_sequencia_old_w						number(10);

cursor c01 is
	select	nr_seq_item
	from	regra_repasse_terc_item
	where	cd_regra = cd_regra_p;
	
cursor c02 is
	select	nr_sequencia
	from	proc_criterio_repasse
	where	cd_regra = cd_regra_p;
	
cursor c03 is
	select	nr_sequencia
	from	proc_criterio_rep_mat
	where	nr_seq_criterio = nr_seq_proc_crit_repasse_old_w;
	
cursor c04 is
	select	nr_sequencia
	from	proc_crit_repasse_adic
	where	nr_seq_criterio = nr_seq_proc_crit_repasse_old_w;
	
cursor c05 is
	select	nr_sequencia
	from	proc_crit_repasse_dia
	where	nr_seq_criterio = nr_seq_proc_crit_repasse_old_w;
	
cursor c06 is
	select	nr_sequencia
	from	proc_crit_repasse_equipe
	where	nr_seq_proc_criterio = nr_seq_proc_crit_repasse_old_w;
	
cursor c07 is
	select	nr_sequencia
	from	proc_crit_rep_regra_ponto
	where	nr_seq_proc_criterio = nr_seq_proc_crit_repasse_old_w;
	
cursor c08 is
	select	nr_sequencia
	from	mat_criterio_repasse
	where	cd_regra = cd_regra_p;
	
cursor c09 is
	select	nr_sequencia
	from	mat_criterio_rep_proc
	where	nr_seq_criterio = nr_seq_mat_crit_repasse_old_w;
	
cursor c10 is
	select	nr_sequencia
	from	mat_crit_repasse_adic
	where	nr_seq_criterio = nr_seq_mat_crit_repasse_old_w;
	
cursor c11 is
	select	nr_sequencia
	from	mat_crit_rep_regra_ponto
	where	nr_seq_mat_criterio = nr_seq_mat_crit_repasse_old_w;
	
cursor c12 is
	select	nr_sequencia
	from	regra_repasse_terc_arq
	where	cd_regra = cd_regra_p;

begin
  
update regra_repasse_terceiro r
   set r.dt_limite_regra = sysdate
 where (r.dt_limite_regra is null or dt_limite_regra > sysdate)
   and r.cd_estabelecimento = cd_estab_dest_p
   and r.cd_regra_origem = cd_regra_p;

select	max(nvl(cd_regra, 0)) + 1
into	cd_regra_new_w
from	regra_repasse_terceiro;

insert into	regra_repasse_terceiro (
		cd_regra,
		ds_regra,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		ie_pagto,
		ds_repasse,
		nr_seq_trans_fin_prod,
		nr_seq_trans_fin_tit,
		dt_limite_regra,
		ie_acao_glosa,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_inicio_vigencia,
		nr_seq_trans_fin_rep_maior,
		ds_repasse_longa,
    cd_regra_origem)
select	cd_regra_new_w,
		ds_regra,
		cd_estab_dest_p,
		sysdate,
		obter_usuario_ativo,
		ie_pagto,
		ds_repasse,
		nr_seq_trans_fin_prod,
		nr_seq_trans_fin_tit,
		dt_limite_regra,
		ie_acao_glosa,
		sysdate,
		obter_usuario_ativo,
		dt_inicio_vigencia,
		nr_seq_trans_fin_rep_maior,
		'',
    cd_regra_p
from	regra_repasse_terceiro
where	cd_regra = cd_regra_p
and		cd_estabelecimento = obter_estabelecimento_ativo;

copia_campo_long(
	'regra_repasse_terceiro',
	'ds_repasse_longa',
	'where cd_regra = :cd_regra_p',
	'cd_regra_p=' || cd_regra_p,
	'cd_regra_p=' || cd_regra_new_w);

open c01;
loop
	fetch c01 into
		nr_sequencia_old_w;
	exit when c01%notfound;

	insert into regra_repasse_terc_item (
			cd_regra,
			nr_seq_item,
			tx_repasse,
			dt_atualizacao,
			nm_usuario,
			ie_beneficiario,
			nr_seq_terceiro,
			cd_conta_contabil,
			vl_repasse,
			ie_perc_saldo,
			ie_restricao_medico,
			ie_gravacao_medico,
			ie_regra_saldo,
			cd_pessoa_fisica,
			ds_observacao,
			tx_anestesista,
			tx_auxiliares,
			tx_custo_operacional,
			tx_materiais,
			tx_medico,
			ie_por_unidade,
			ie_perc_executor,
			ie_funcao_medico,
			dt_inicio_vigencia,
			dt_fim_vigencia,
			nr_seq_categoria,
			ie_tipo_terceiro,
			ie_gera_item_zerado,
			ie_recebe_regra_pontuacao,
			ie_valida_terceiro_forma)
	select	cd_regra_new_w,
			nr_seq_item,
			tx_repasse,
			sysdate,
			obter_usuario_ativo,
			ie_beneficiario,
			nr_seq_terceiro,
			cd_conta_contabil,
			vl_repasse,
			ie_perc_saldo,
			ie_restricao_medico,
			ie_gravacao_medico,
			ie_regra_saldo,
			cd_pessoa_fisica,
			ds_observacao,
			tx_anestesista,
			tx_auxiliares,
			tx_custo_operacional,
			tx_materiais,
			tx_medico,
			ie_por_unidade,
			ie_perc_executor,
			ie_funcao_medico,
			dt_inicio_vigencia,
			dt_fim_vigencia,
			nr_seq_categoria,
			ie_tipo_terceiro,
			ie_gera_item_zerado,
			ie_recebe_regra_pontuacao,
			ie_valida_terceiro_forma
	from	regra_repasse_terc_item
	where	cd_regra = cd_regra_p
	and		nr_seq_item = nr_sequencia_old_w;
end loop;
close c01;

open c02;
loop
	fetch c02 into
		nr_seq_proc_crit_repasse_old_w;
	exit when c02%notfound;

	select	proc_criterio_repasse_seq.nextval
	into	nr_seq_proc_crit_repasse_new_w
	from	dual;

	insert into proc_criterio_repasse (
			nr_sequencia,					ie_funcao,					cd_regra,
			dt_atualizacao,					nm_usuario,					cd_edicao_amb,
			cd_area_proced,					cd_especial_proced,			cd_grupo_proced,
			cd_procedimento,				ie_origem_proced,			cd_convenio,
			cd_setor_atendimento,			cd_medico,					ie_forma_calculo,					
			ie_pacote,						tx_medico,					tx_anestesista,
			tx_materiais,					tx_auxiliares,				tx_custo_operacional,				
			vl_repasse,						cd_tabela_preco,			cd_convenio_calc,
			cd_categoria_calc,				ie_tipo_atendimento,		cd_edicao_amb_calc,					
			ie_honorario,					vl_limite,					cd_prestador,
			ie_tipo_convenio,				cd_categoria,				ie_tipo_servico_sus,				
			ie_tipo_ato_sus,				dt_vigencia_inicial,		dt_vigencia_final,
			ie_regra_dia,					dt_atualizacao_nrec,		nm_usuario_nrec,					
			cd_grupo_proc_aih,				hr_inicial,					hr_final,		
			ie_honorario_restricao,			ie_plantao,					tx_procedimento,					
			ie_perc_pacote,					ie_carater_inter_sus,		cd_municipio_ibge,
			nr_seq_grupo,					nr_seq_subgrupo,			nr_seq_forma_org,					
			cd_registro,					cd_especialidade,			cd_medico_laudo,
			cd_situacao_glosa,				ie_med_exec_socio,			nr_seq_proc_interno,
			ie_cobra_pf_pj,					qt_dia_inicial,				qt_dia_final,
			ie_atend_retorno,				ie_participou_sus,			ie_clinica,
			ie_regra_medico,				nr_seq_exame,				ie_conveniado,
			cd_equipamento,					ie_situacao,				ie_repasse_calc,
			cd_med_exec_proc_princ,			qt_porte_anestesico,		cd_procedencia,
			cd_pessoa_func,					ie_tipo_atend_calc,			cd_medico_prescr,
			cd_medico_req,					cd_plano,					ie_med_plantonista,
			cd_tipo_acomodacao,				nr_seq_classif_medico,		ds_function,
			cd_tipo_pessoa,					cd_tipo_anestesia,			nr_seq_etapa_checkup,
			ie_rep_auto,					ie_sexo,					cd_tipo_pessoa_prest,
			cd_medico_aval,					ie_via_acesso,				ds_observacao,
			ie_carater_cirurgia,			nr_seq_regra_prior_repasse,	ie_prioridade,
			vl_minimo,						nr_seq_classificacao,		tx_proc_minima,
			tx_proc_maxima,					cd_tipo_procedimento,		nr_seq_detalhe_sus,
			nr_seq_categoria,				cd_setor_proc_prescr,		nr_seq_estagio_autor,
			qt_idade_min,					qt_idade_max,				cd_cbo_sus,
			ie_limite_qtdade,				cd_setor_int_anterior,		cd_tipo_acomod_atend,
			nr_seq_terceiro,				nr_seq_estagio_conta,		cd_medico_cirurgia,
			cd_medico_atend,				ie_com_material,			cd_medico_referido,
			nr_seq_indicacao,				ie_complexidade,			ie_tipo_financiamento,
			cd_convenio_atend,				ie_tipo_terceiro,			ie_considerar_custo_item,
			ie_campo_base_vl_repasse,		ie_vinculo_medico,			ie_medico_pertence_equipe,
			ie_lib_laudo_proc)
	select	nr_seq_proc_crit_repasse_new_w,	ie_funcao,					cd_regra_new_w,
			sysdate,						obter_usuario_ativo,		cd_edicao_amb,
			cd_area_proced,					cd_especial_proced,			cd_grupo_proced,
			cd_procedimento,				ie_origem_proced,			cd_convenio,
			cd_setor_atendimento,			cd_medico,					ie_forma_calculo,					
			ie_pacote,						tx_medico,					tx_anestesista,
			tx_materiais,					tx_auxiliares,				tx_custo_operacional,				
			vl_repasse,						cd_tabela_preco,			cd_convenio_calc,
			cd_categoria_calc,				ie_tipo_atendimento,		cd_edicao_amb_calc,					
			ie_honorario,					vl_limite,					cd_prestador,
			ie_tipo_convenio,				cd_categoria,				ie_tipo_servico_sus,				
			ie_tipo_ato_sus,				dt_vigencia_inicial,		dt_vigencia_final,
			ie_regra_dia,					sysdate,					obter_usuario_ativo,					
			cd_grupo_proc_aih,				hr_inicial,					hr_final,		
			ie_honorario_restricao,			ie_plantao,					tx_procedimento,					
			ie_perc_pacote,					ie_carater_inter_sus,		cd_municipio_ibge,
			nr_seq_grupo,					nr_seq_subgrupo,			nr_seq_forma_org,					
			cd_registro,					cd_especialidade,			cd_medico_laudo,
			cd_situacao_glosa,				ie_med_exec_socio,			nr_seq_proc_interno,
			ie_cobra_pf_pj,					qt_dia_inicial,				qt_dia_final,
			ie_atend_retorno,				ie_participou_sus,			ie_clinica,
			ie_regra_medico,				nr_seq_exame,				ie_conveniado,
			cd_equipamento,					ie_situacao,				ie_repasse_calc,
			cd_med_exec_proc_princ,			qt_porte_anestesico,		cd_procedencia,
			cd_pessoa_func,					ie_tipo_atend_calc,			cd_medico_prescr,
			cd_medico_req,					cd_plano,					ie_med_plantonista,
			cd_tipo_acomodacao,				nr_seq_classif_medico,		ds_function,
			cd_tipo_pessoa,					cd_tipo_anestesia,			nr_seq_etapa_checkup,
			ie_rep_auto,					ie_sexo,					cd_tipo_pessoa_prest,
			cd_medico_aval,					ie_via_acesso,				ds_observacao,
			ie_carater_cirurgia,			nr_seq_regra_prior_repasse,	ie_prioridade,
			vl_minimo,						nr_seq_classificacao,		tx_proc_minima,
			tx_proc_maxima,					cd_tipo_procedimento,		nr_seq_detalhe_sus,
			nr_seq_categoria,				cd_setor_proc_prescr,		nr_seq_estagio_autor,
			qt_idade_min,					qt_idade_max,				cd_cbo_sus,
			ie_limite_qtdade,				cd_setor_int_anterior,		cd_tipo_acomod_atend,
			nr_seq_terceiro,				nr_seq_estagio_conta,		cd_medico_cirurgia,
			cd_medico_atend,				ie_com_material,			cd_medico_referido,
			nr_seq_indicacao,				ie_complexidade,			ie_tipo_financiamento,
			cd_convenio_atend,				ie_tipo_terceiro,			ie_considerar_custo_item,
			ie_campo_base_vl_repasse,		ie_vinculo_medico,			ie_medico_pertence_equipe,
			ie_lib_laudo_proc
	from	proc_criterio_repasse
	where	nr_sequencia = nr_seq_proc_crit_repasse_old_w;
	
	open c03;
	loop
		fetch c03 into
			nr_sequencia_old_w;
		exit when c03%notfound;
		
		insert into proc_criterio_rep_mat (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_criterio,
				cd_material)
		select	proc_criterio_rep_mat_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				sysdate,
				obter_usuario_ativo,
				nr_seq_proc_crit_repasse_new_w,
				cd_material
		from	proc_criterio_rep_mat
		where	nr_sequencia = nr_sequencia_old_w;
	end loop;
	close c03;
	
	open c04;
	loop
		fetch c04 into
			nr_sequencia_old_w;
		exit when c04%notfound;
	
		insert into proc_crit_repasse_adic (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_criterio,
				ds_function)
		select	proc_crit_repasse_adic_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				sysdate,
				obter_usuario_ativo,
				nr_seq_proc_crit_repasse_new_w,
				ds_function
		from	proc_crit_repasse_adic
		where	nr_sequencia = nr_sequencia_old_w;
	end loop;
	close c04;
	
	open c05;
	loop
		fetch c05 into
			nr_sequencia_old_w;
		exit when c05%notfound;
	
		insert into proc_crit_repasse_dia (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_criterio,
				ie_dia_semana)
		select	proc_crit_repasse_dia_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				sysdate,
				obter_usuario_ativo,
				nr_seq_proc_crit_repasse_new_w,
				ie_dia_semana
		from	proc_crit_repasse_dia
		where	nr_sequencia = nr_sequencia_old_w;
	end loop;
	close c05;
	
	open c06;
	loop
		fetch c06 into
			nr_sequencia_old_w;
		exit when c06%notfound;
	
		insert into proc_crit_repasse_equipe (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_proc_criterio,
				ie_funcao_medico,
				ie_situacao)
		select	proc_crit_repasse_equipe_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				sysdate,
				obter_usuario_ativo,
				nr_seq_proc_crit_repasse_new_w,
				ie_funcao_medico,
				ie_situacao
		from	proc_crit_repasse_equipe
		where	nr_sequencia = nr_sequencia_old_w;
	end loop;
	close c06;

	open c07;
	loop
		fetch c07 into
			nr_sequencia_old_w;
		exit when c07%notfound;

		insert into proc_crit_rep_regra_ponto (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_proc_criterio,
				nr_seq_regra_rep_ponto)
		select	proc_crit_rep_regra_ponto_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				sysdate,
				obter_usuario_ativo,
				nr_seq_proc_crit_repasse_new_w,
				nr_seq_regra_rep_ponto
		from	proc_crit_rep_regra_ponto
		where	nr_sequencia = nr_sequencia_old_w;
	end loop;
	close c07;
end loop;
close c02;

open c08;
loop
	fetch c08 into
		nr_seq_mat_crit_repasse_old_w;
	exit when c08%notfound;

	select	mat_criterio_repasse_seq.nextval
	into	nr_seq_mat_crit_repasse_new_w
	from	dual;

	insert into mat_criterio_repasse (
			nr_sequencia,				cd_regra,
			dt_atualizacao,				nm_usuario,
			cd_edicao_amb,				cd_grupo_material,
			cd_subgrupo_material,		cd_classe_material,
			cd_material,				cd_convenio,
			cd_setor_atendimento,		cd_medico,
			ie_forma_calculo,			ie_pacote,
			tx_repasse,					vl_repasse,
			cd_tabela_preco,			cd_convenio_calc,
			cd_categoria_calc,			ie_tipo_atendimento,
			ie_tipo_convenio,			dt_atualizacao_nrec,
			nm_usuario_nrec,			ie_regra_medico,
			cd_cgc_prestador,			ie_carater_inter_sus,
			cd_municipio_ibge,			ie_atend_retorno,
			dt_vigencia_inicial,		dt_vigencia_final,
			cd_tab_preco_calc,			ie_situacao,
			cd_med_exec_proc_princ,		ie_repasse_calc,
			cd_cgc_fornecedor,			cd_plano,
			cd_categoria,				ie_perc_pacote,
			ie_medico_ind_corpo_cli,	ie_medico_ind_proc_princ,
			ie_med_exec_pp_corpo_cli,	ds_function,
			cd_tipo_pessoa,				ie_rep_auto,
			ie_sexo,					nr_seq_grupo_rec,
			cd_medico_aval,				nr_seq_lote_fornec,
			ds_observacao,				ie_prioridade,
			nr_seq_classificacao,		ie_honorario,
			cd_especialidade,			cd_setor_mat_prescr,
			nr_seq_terceiro,			nr_seq_estagio_conta,
			cd_medico_referido,			nr_seq_categoria,
			nr_seq_indicacao,			cd_convenio_atend,
			ie_restrito,				ie_tipo_terceiro,
			pr_desconto_val_fatur,		ie_vinculo_medico,
			ie_clinica,					cd_tipo_protocolo,
			cd_protocolo,				nr_seq_subtipo_protocolo,
			ie_tipo_criterio,nr_seq_classif_subtipo_prot)
	select	nr_seq_mat_crit_repasse_new_w,cd_regra_new_w,
			sysdate,					obter_usuario_ativo,
			cd_edicao_amb,				cd_grupo_material,
			cd_subgrupo_material,		cd_classe_material,
			cd_material,				cd_convenio,
			cd_setor_atendimento,		cd_medico,
			ie_forma_calculo,			ie_pacote,
			tx_repasse,					vl_repasse,
			cd_tabela_preco,			cd_convenio_calc,
			cd_categoria_calc,			ie_tipo_atendimento,
			ie_tipo_convenio,			sysdate,
			obter_usuario_ativo,		ie_regra_medico,
			cd_cgc_prestador,			ie_carater_inter_sus,
			cd_municipio_ibge,			ie_atend_retorno,
			dt_vigencia_inicial,		dt_vigencia_final,
			cd_tab_preco_calc,			ie_situacao,
			cd_med_exec_proc_princ,		ie_repasse_calc,
			cd_cgc_fornecedor,			cd_plano,
			cd_categoria,				ie_perc_pacote,
			ie_medico_ind_corpo_cli,	ie_medico_ind_proc_princ,
			ie_med_exec_pp_corpo_cli,	ds_function,
			cd_tipo_pessoa,				ie_rep_auto,
			ie_sexo,					nr_seq_grupo_rec,
			cd_medico_aval,				nr_seq_lote_fornec,
			ds_observacao,				ie_prioridade,
			nr_seq_classificacao,		ie_honorario,
			cd_especialidade,			cd_setor_mat_prescr,
			nr_seq_terceiro,			nr_seq_estagio_conta,
			cd_medico_referido,			nr_seq_categoria,
			nr_seq_indicacao,			cd_convenio_atend,
			ie_restrito,				ie_tipo_terceiro,
			pr_desconto_val_fatur,		ie_vinculo_medico,
			ie_clinica,					cd_tipo_protocolo,
			cd_protocolo,				nr_seq_subtipo_protocolo,
			ie_tipo_criterio, nr_seq_classif_subtipo_prot
	from	mat_criterio_repasse
	where	nr_sequencia = nr_seq_mat_crit_repasse_old_w;
	
	open c09;
	loop
		fetch c09 into
			nr_sequencia_old_w;
		exit when c09%notfound;
	
		insert into mat_criterio_rep_proc (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_criterio,
				cd_procedimento,
				ie_origem_proced,
				cd_area_procedimento,
				cd_especialidade_proc,
				cd_grupo_proc,
				ie_proc_princ,
				cd_medico_executor)
		select	mat_criterio_rep_proc_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				sysdate,
				obter_usuario_ativo,
				nr_seq_mat_crit_repasse_new_w,
				cd_procedimento,
				ie_origem_proced,
				cd_area_procedimento,
				cd_especialidade_proc,
				cd_grupo_proc,
				ie_proc_princ,
				cd_medico_executor
		from	mat_criterio_rep_proc
		where	nr_sequencia = nr_sequencia_old_w;
	end loop;
	close c09;
	
	open c10;
	loop
		fetch c10 into
			nr_sequencia_old_w;
		exit when c10%notfound;
	
		insert into mat_crit_repasse_adic (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_criterio,
				ds_function)
		select	mat_crit_repasse_adic_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				sysdate,
				obter_usuario_ativo,
				nr_seq_mat_crit_repasse_new_w,
				ds_function
		from	mat_crit_repasse_adic
		where	nr_sequencia = nr_sequencia_old_w;
	end loop;
	close c10;
	
	open c11;
	loop
		fetch c11 into
			nr_sequencia_old_w;
		exit when c11%notfound;
	
		insert into mat_crit_rep_regra_ponto (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_mat_criterio,
				nr_seq_regra_rep_ponto)
		select	mat_crit_rep_regra_ponto_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				sysdate,
				obter_usuario_ativo,
				nr_seq_mat_crit_repasse_new_w,
				nr_seq_regra_rep_ponto
		from	mat_crit_rep_regra_ponto
		where	nr_sequencia = nr_sequencia_old_w;
	end loop;
	close c11;
end loop;
close c08;

open c12;
loop
	fetch c12 into
		nr_sequencia_old_w;
	exit when c12%notfound;

	insert into regra_repasse_terc_arq (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_regra,
			ds_arquivo)
	select	regra_repasse_terc_arq_seq.nextval,
			sysdate,
			obter_usuario_ativo,
			sysdate,
			obter_usuario_ativo,
			cd_regra_new_w,
			ds_arquivo
	from	regra_repasse_terc_arq
	where	nr_sequencia = nr_sequencia_old_w;
end loop;
close c12;

commit;

end copiar_regra_criterio_repasse;
/