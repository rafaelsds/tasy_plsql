create or replace procedure gerar_pacote_exigido(nr_seq_pacote_exigido_p number,
						nr_seq_pacote_p  number,
						nm_usuario_p varchar2) is
						
nr_sequencia_w	convenio_pacote_autor_exig.nr_sequencia%type;
	
begin

select	convenio_pacote_autor_exig_seq.nextval
into	nr_sequencia_w
from	dual;

insert into convenio_pacote_autor_exig (
		nr_sequencia,
		dt_atualizacao_nrec,
		dt_atualizacao,
		dt_fim_vigencia,
		dt_inicio_vigencia,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_pacote,
		nr_seq_pacote_exigido)
	values (nr_sequencia_w,
		sysdate,
		sysdate,
		null,
		null,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_pacote_p,
		nr_seq_pacote_exigido_p);

commit;

end gerar_pacote_exigido;
/