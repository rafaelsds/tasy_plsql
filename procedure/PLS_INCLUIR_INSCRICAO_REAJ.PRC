create or replace
procedure pls_incluir_inscricao_reaj
			(	nr_seq_lote_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

ie_origem_inscricao_w		varchar2(1);
nr_seq_contrato_w		number(10);
nr_seq_inscricao_w		number(10);
nr_seq_intercambio_w		number(10);
nr_seq_reajuste_inscricao_w	number(10);
tx_reajuste_w			number(7,4);
tx_reajuste_vl_maximo_w		number(7,4);
dt_referencia_w			date;


Cursor C01 is
	select	a.nr_sequencia,
		decode(a.nr_seq_plano, '', 'C', 'P')
	from	pls_regra_inscricao a
	where	a.nr_seq_contrato	= nr_seq_contrato_w
	and	dt_referencia_w	between nvl(a.dt_inicio_vigencia,dt_referencia_w) and nvl(a.dt_fim_vigencia,dt_referencia_w)
	union
	select	q.nr_sequencia,
		decode(q.nr_seq_plano, '', 'C', 'P')
	from	pls_contrato		y,
		pls_contrato_plano	w,
		pls_plano		z,
		pls_regra_inscricao	q
	where	y.nr_sequencia	  = nr_seq_contrato_w
	and	w.nr_seq_contrato = y.nr_sequencia
	and	w.nr_seq_plano    = z.nr_sequencia
	and	q.nr_seq_plano	  = z.nr_sequencia
	and	dt_referencia_w	between nvl(q.dt_inicio_vigencia, dt_referencia_w) and nvl(q.dt_fim_vigencia, dt_referencia_w)
	and	not exists	(select	1
				from	pls_regra_inscricao	x
				where	x.nr_seq_contrato	= nr_seq_contrato_w)
	union all
	select	a.nr_sequencia,
		decode(a.nr_seq_plano, '', 'C', 'P')
	from	pls_regra_inscricao	a
	where	a.nr_seq_intercambio	= nr_seq_intercambio_w
	and	dt_referencia_w	between nvl(a.dt_inicio_vigencia, dt_referencia_w) and nvl(a.dt_fim_vigencia, dt_referencia_w)
	union
	select	q.nr_sequencia,
		decode(q.nr_seq_plano, '', 'C', 'P')
	from	pls_intercambio		y,
		pls_intercambio_plano	w,
		pls_plano		z,
		pls_regra_inscricao	q
	where	y.nr_sequencia	  = nr_seq_intercambio_w
	and	w.nr_seq_intercambio = y.nr_sequencia
	and	w.nr_seq_plano    = z.nr_sequencia
	and	q.nr_seq_plano	  = z.nr_sequencia
	and	dt_referencia_w	between nvl(q.dt_inicio_vigencia,dt_referencia_w) and nvl(q.dt_fim_vigencia,dt_referencia_w)
	and	not exists	(select	1
				from	pls_regra_inscricao x
				where	x.nr_seq_intercambio	= nr_seq_intercambio_w);

begin
if	(nr_seq_lote_p is not null) then
	select	nr_seq_contrato,
		nvl(tx_reajuste,0),
		dt_referencia,
		nr_seq_intercambio
	into	nr_seq_contrato_w,
		tx_reajuste_w,
		dt_referencia_w,
		nr_seq_intercambio_w
	from	pls_lote_reaj_inscricao
	where	nr_sequencia	= nr_seq_lote_p;

	open C01;
	loop
	fetch C01 into	
		nr_seq_inscricao_w,
		ie_origem_inscricao_w;
	exit when C01%notfound;
		begin
		select	pls_reajuste_inscricao_seq.nextval
		into	nr_seq_reajuste_inscricao_w
		from	dual;
		
		insert into pls_reajuste_inscricao
			(nr_sequencia,
			nr_seq_lote,
			cd_estabelecimento,
			nr_seq_regra_inscricao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			tx_reajuste,
			ie_origem_inscricao)
		values	(nr_seq_reajuste_inscricao_w,
			nr_seq_lote_p,
			cd_estabelecimento_p,
			nr_seq_inscricao_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			tx_reajuste_w,
			ie_origem_inscricao_w);
		end;
	end loop;
	close C01;
end if;
	
commit;

end pls_incluir_inscricao_reaj;
/