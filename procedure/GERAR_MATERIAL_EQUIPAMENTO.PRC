create or replace
procedure gerar_material_equipamento(	cd_estabelecimento_p	number,
				nr_seq_equipamento_p	number,
				nm_usuario_p		varchar2 ) is 

cd_equipamento_w		number(10,0);
nr_seq_classif_equip_w		number(10,0);
nr_seq_proc_w			number(10,0);
nr_cirurgia_w			number(10,0);
cd_material_w			number(6,0);
qt_material_w			number(15,3);
cd_unidade_medida_w		varchar2(30);
nr_seq_equi_cir_w		number(10,0);
ie_gera_quantidade_w		varchar2(1);
ds_lista_nao_atualiza_qtd_w 	varchar2(255);
qt_equipamento_w		number(10,0);


Cursor C01 is
	select	cd_material,
		qt_material,
		cd_unidade_medida
	from	material_equipamento
	where	nvl(cd_equipamento,cd_equipamento_w)		= cd_equipamento_w
	and	nvl(nr_seq_classif_equip, nvl(nr_seq_classif_equip_w,0))	= nvl(nr_seq_classif_equip_w,0)
	and	nvl(nr_seq_proc, nvl(nr_seq_proc_w,0))			= nvl(nr_seq_proc_w,0)
	and	cd_estabelecimento 				= cd_estabelecimento_p;
				
begin

Obter_Param_Usuario(872, 296, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_gera_quantidade_w);
Obter_Param_Usuario(872, 331, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ds_lista_nao_atualiza_qtd_w);


select	nr_cirurgia,
	cd_equipamento,
	campo_numerico(obter_dados_equipamento(cd_equipamento,'C')),
	qt_equipamento
into	nr_cirurgia_w,
	cd_equipamento_w,
	nr_seq_classif_equip_w,
	qt_equipamento_w
from	equipamento_cirurgia
where	nr_sequencia	=	nr_seq_equipamento_p
and	nvl(ie_situacao,'A') = 'A';

select	max(nr_seq_proc_interno)
into	nr_seq_proc_w
from	cirurgia
where	nr_cirurgia	=	nr_cirurgia_w;	

if	(ie_gera_quantidade_w = 'S') and
	(ds_lista_nao_atualiza_qtd_w is not null) and
	(obter_se_contido_char(cd_equipamento_w,ds_lista_nao_atualiza_qtd_w) = 'S') then
	ie_gera_quantidade_w := 'N';
end if;

open C01;
loop
fetch C01 into	
	cd_material_w,
	qt_material_w,
	cd_unidade_medida_w;
exit when C01%notfound;
	begin
	select	material_equip_cirurgia_seq.nextval 
	into	nr_seq_equi_cir_w
	from 	dual;

	insert into material_equip_cirurgia(
		nr_sequencia,
		dt_atualizacao,         
		nm_usuario,             
		dt_atualizacao_nrec,    
		nm_usuario_nrec,        
		cd_material,   
		qt_material,
		cd_unidade_medida,
		nr_seq_equi_cir)
	values	(nr_seq_equi_cir_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_material_w,   
		decode(ie_gera_quantidade_w,'S',qt_equipamento_w,qt_material_w),
		cd_unidade_medida_w,
		nr_seq_equipamento_p);		
	end;
end loop;
close C01;
commit;

end gerar_material_equipamento;
/