CREATE OR REPLACE
PROCEDURE Duplicar_Kit_Material
		(cd_kit_material_p     	number,
		 nm_usuario_p			varchar2) IS

cd_kit_material_w	number(5);
ds_kit_material_w	varchar2(80);
ie_situacao_w		varchar2(1);
ie_tipo_w		varchar2(1);
ie_forma_baixa_w	varchar2(1);
ie_material_pai_w	varchar2(1);
cd_estab_exclusivo_w	kit_material.cd_estab_exclusivo%type;

BEGIN

select	max(cd_kit_material) + 1
into	cd_kit_material_w
from 	kit_material;

select	ds_kit_material,
	ie_situacao,
	ie_tipo,
	ie_forma_baixa,
	ie_material_pai,
	cd_estab_exclusivo
into	ds_kit_material_w,
	ie_situacao_w,
	ie_tipo_w,
	ie_forma_baixa_w,
	ie_material_pai_w,
	cd_estab_exclusivo_w
from	kit_material
where 	cd_kit_material = cd_kit_material_p; 

insert	into kit_material(
	cd_kit_material,
	ds_kit_material,
	ie_situacao,
	dt_atualizacao,
	nm_usuario,
	ie_tipo,
	ie_forma_baixa,
	ie_material_pai,
	cd_estab_exclusivo)
values(cd_kit_material_w,
	wheb_mensagem_pck.get_Texto(311905, 'DS_KIT_MATERIAL_W='|| DS_KIT_MATERIAL_W),
	ie_situacao_w,
	sysdate,
	nm_usuario_p,
	ie_tipo_w,
	ie_forma_baixa_w,
	ie_material_pai_w,
	cd_estab_exclusivo_w);


insert	into componente_kit(
	cd_kit_material,
	nr_sequencia,
	cd_material,
	ie_via_aplicacao,
	ie_tipo_paciente,
	qt_material,
	ie_situacao,
	dt_atualizacao,
	nm_usuario,
	qt_volume_min,
	qt_volume_max,
	ie_baixa_estoque,
	cd_medico,
	cd_convenio,
	ie_tipo_convenio,
	ie_dispensavel,
	ie_video,
	CD_FORNECEDOR,
	QT_IDADE_MINIMA,
	QT_IDADE_MAXIMA,
	IE_CALCULA_PRECO,
	IE_ENTRA_CONTA,
	IE_DUPLICA_PRESCR,
	IE_MULTIPLICA_DOSE)
select	cd_kit_material_w,
	nr_sequencia,
	cd_material,
	ie_via_aplicacao,
	ie_tipo_paciente,
	qt_material,
	ie_situacao,
	sysdate,
	nm_usuario_p,
	qt_volume_min,
	qt_volume_max,
	ie_baixa_estoque,
	cd_medico,
	cd_convenio,
	ie_tipo_convenio,
	ie_dispensavel,
	ie_video,
	CD_FORNECEDOR,
	QT_IDADE_MINIMA,
	QT_IDADE_MAXIMA,
	IE_CALCULA_PRECO,
	IE_ENTRA_CONTA,
	nvl(IE_DUPLICA_PRESCR,'S'),
	nvl(IE_MULTIPLICA_DOSE,'N')	
from	componente_kit
where	cd_kit_material = cd_kit_material_p;

insert	into componente_kit_opcao(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	cd_kit_material,
	nr_seq_componente,
	cd_material)
(select componente_kit_opcao_seq.nextval,
	sysdate,
	nm_usuario_p,
	cd_kit_material_w,
	nr_seq_componente,
	cd_material
from	componente_kit_opcao
where	cd_kit_material = cd_kit_material_p);

COMMIT;

END Duplicar_Kit_material;
/