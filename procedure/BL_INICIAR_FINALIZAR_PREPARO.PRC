create or replace
procedure BL_iniciar_finalizar_preparo(	nr_seq_item_p		Number,
					ie_opcao_p		Varchar2,
					nm_usuario_p		Varchar2) is 

nr_prescricao_w		Number(14);
nr_seq_prod_lac_w	Number(10);
dt_horario_w		Date;
nr_seq_item_alterar_w	Number(10);
qt_dose_w		Number(18,6);
qt_atendida_w		Number(15);
					
Cursor C01 is
	select	a.nr_sequencia,
		a.qt_dose - qt_atendida_w
	from	nut_producao_lactario_item a
	where	a.nr_prescricao	= nr_prescricao_w
	and	a.nr_seq_prod_lac = nr_seq_prod_lac_w
	and	a.dt_horario	= dt_horario_w
	and not exists (select	1
			from	nutricao_leite_deriv b,
				classif_leite_deriv c
			where	b.cd_material = a.cd_material
			and	b.nr_seq_classif = c.nr_sequencia
			and nvl(b.ie_situacao,'A') = 'A'
			and	nvl(c.ie_leite_materno,'N') = 'S')
	order by 1;
					
begin
if	(nr_seq_item_p is not null) then

	if	(ie_opcao_p = 'I') then
	
		update	nut_producao_lactario_item
		set	dt_inicio_preparo 	= sysdate,
			nm_usuario_ini_preparo	= nm_usuario_p
		where	nr_sequencia 		= nr_seq_item_p;
		
	elsif	(ie_opcao_p = 'F') then
	
		update	nut_producao_lactario_item
		set	dt_fim_preparo 		= sysdate,
			nm_usuario_fim_preparo	= nm_usuario_p
		where	nr_sequencia 		= nr_seq_item_p;
	
		select	nr_prescricao,
			nr_seq_prod_lac,
			dt_horario,
			BL_obter_qt_atendida(nr_sequencia)
		into	nr_prescricao_w,
			nr_seq_prod_lac_w,
			dt_horario_w,
			qt_atendida_w
		from	nut_producao_lactario_item
		where	nr_sequencia = nr_seq_item_p;
	
		open C01;
		loop
		fetch C01 into	
			nr_seq_item_alterar_w,
			qt_dose_w;
		exit when C01%notfound;
			begin
			
			if	(qt_dose_w < 0) then
				qt_dose_w := 0;
			end if;
			
			Nut_alterar_qtd_producao(nr_seq_item_alterar_w, qt_dose_w, nm_usuario_p);
			
			end;
		end loop;
		close C01;
		
		update	bl_frasco
		set	ie_situacao_processa = 'N',
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_seq_nut_prod	= nr_seq_item_p;
		
	end if;
end if;

commit;

end BL_iniciar_finalizar_preparo;
/
