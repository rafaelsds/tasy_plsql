create or replace
procedure pls_altera_status_contestacao(	nr_seq_contestacao_p	in pls_lote_contestacao.nr_sequencia%type,
						ie_status_p		in pls_lote_contestacao.ie_status%type,
						nm_usuario_p		in usuario.nm_usuario%type) is
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Altera o status da contesta��o

	Foi criado uam rotina separada, pois alguns status podem precisar realizar algumas opera��es
	
	Por exemplo, quando a contesta��o for concluida, todas as contas em discuss�o dever�o 
	ter o seu status alterado para "[E] - Discussao encerrada"
	
	Aten��o, N�O DEVER� TER COMMIT, essa rotina atualmente � chamada por trigger,
	portanto ela n�o poder� ter commit.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

	N�O DEVER� TER COMMIT, essa rotina atualmente � chamada por trigger,
	portanto ela n�o poder� ter commit.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

-- Carrega os lotes de discuss�o da contesta��o
cursor	c01(	nr_seq_contestacao_pc	pls_lote_contestacao.nr_sequencia%type) is
	select	a.nr_sequencia
	from	pls_lote_discussao	a
	where	a.nr_seq_lote_contest	= nr_seq_contestacao_pc
	and	a.ie_status		= 'F';

begin

-- se for concluido
if	(ie_status_p = 'C') then

	-- busca todos os lotes de discuss�o que estiverem fechados ou cancelados, e atualiza o status das contas da discuss�o (n�o a conta m�dica) para "[E] - Discussao encerrada"
	for r_c01_w in c01(nr_seq_contestacao_p) loop
	
		update	pls_contestacao_discussao
		set	ie_status	= 'E',
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_seq_lote	= r_c01_w.nr_sequencia;
	end loop;
	
	-- atualiza o status
	update	pls_lote_contestacao
	set	ie_status	= ie_status_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_contestacao_p;
	
else

	-- apenas atualiza o status passado
	update	pls_lote_contestacao
	set	ie_status	= ie_status_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_contestacao_p;
end if;

end pls_altera_status_contestacao;
/
