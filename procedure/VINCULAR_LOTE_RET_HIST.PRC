create or replace
procedure vincular_lote_Ret_hist (	nr_seq_lote_recurso_p	number,
					nr_sequencia_p	number) is
begin
	if(nr_sequencia_p > 0)then
		update 	conta_paciente_ret_hist 
		set 	nr_seq_lote_recurso = nr_seq_lote_recurso_p
		where  	nr_sequencia = nr_sequencia_p;
		commit;
	end if;
end vincular_lote_Ret_hist;
/