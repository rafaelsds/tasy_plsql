create or replace
procedure Consistir_Horario_Agenda_Pac	
			(cd_agenda_p		in	number,
			ie_dia_semana_p		in	number,
			hr_inicio_p		in	date,
			hr_final_p		in	date,
			ds_erro_p		out	varchar2) is


ds_erro_w		varchar2(255)	:= '';
cd_medico_w		varchar2(10);
cd_agenda_w		varchar2(10);
qt_agenda_w		number(10,0)	:= 0;   
qt_agenda_total_w	number(10,0)	:= 0;   

cursor	c01 is
	select	a.cd_pessoa_fisica,
		a.cd_agenda
	from	agenda a,
		agenda_medico b
	where	a.cd_pessoa_fisica	= b.cd_medico
	and	b.cd_agenda		= cd_agenda_p
	and	a.ie_situacao		= 'A';

cursor	c02 is
	select	count(*)
	from	agenda b,
		agenda_turno a
	where	a.cd_agenda	= cd_agenda_w
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	ie_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') > 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') < 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	union
	select	count(*)
	from	agenda b,
		agenda_turno a
	where	a.cd_agenda	= cd_agenda_w
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	ie_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') < 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') > 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	union
	select	count(*)
	from	agenda b,
		agenda_turno a
	where	a.cd_agenda	= cd_agenda_w
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	ie_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	union
	select	count(*)
	from	agenda b,
		agenda_turno a
	where	a.cd_agenda	= cd_agenda_w
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	ie_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	union
	select	count(*)
	from	agenda b,
		agenda_turno a
	where	a.cd_agenda	= cd_agenda_w
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	ie_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	union
	select	count(*)
	from	agenda b,
		agenda_turno a
	where	a.cd_agenda	= cd_agenda_w
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	ie_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <= 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') > 
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');

begin

open	c01;
loop
fetch	c01 into
	cd_medico_w,
	cd_agenda_w;
exit	when c01%notfound;
	begin

	open	c02;
	loop
	fetch	c02 into
		qt_agenda_w;
	exit	when c02%notfound;
		begin

		qt_agenda_total_w	:= qt_agenda_total_w + qt_agenda_w;
			
		end;
	end loop;
	close c02;

	end;
end loop;
close c01;

if	(qt_agenda_total_w > 0) then
	ds_erro_w	:= wheb_mensagem_pck.get_texto(279111);
end if;

ds_erro_p	:= ds_erro_w;

end Consistir_Horario_Agenda_Pac;
/