CREATE OR REPLACE
PROCEDURE GERAR_CONTAS_PENDENTES_W (dt_inicial_p			date,
					dt_final_p				date,
					nr_titulo_p			number,
					cd_convenio_p			varchar2,
					ie_tipo_convenio_p			varchar2,
					cd_setor_atendimento_p		varchar2,
					ie_situacao_titulo_p			varchar2,
					ie_opcao_p			varchar2,
					ie_ordem_p			varchar2,
					cd_estabelecimento_p		number) IS

nr_titulo_w			number(10);
nr_interno_conta_w			number(10);
nr_nota_protocolo_w		varchar2(255);
nm_paciente_w			varchar2(40);
ds_procedimento_w			varchar2(255);
cd_autorizacao_w			varchar2(30);
nr_atendimento_w			number(10);
cd_matricula_w			varchar2(30);
vl_conta_w			number(15,4);
vl_pago_w			number(15,4);
dt_pagamento_previsto_w		date;
nr_protocolo_w			varchar2(40);
dt_emissao_w			date;
vl_glosa_w			number(15,4);
vl_amaior_w			number(15,4);
vl_amenor_w			number(15,4);
vl_saldo_w			number(15,4);
dt_pagamento_previsto_ww		date;
ie_opcao_data_w			number(1);
cd_estabelecimento_w		number(10);
nr_nota_fiscal_w			varchar2(255);

/*Obt�m somente a data de pagamento previsto, Parte com QUEBRA*/
cursor C01 is
select	r.dt_pagamento_previsto dt_pagamento_previsto
from	atendimento_paciente p,
	conta_paciente_guia b,
	conta_paciente a,
	titulo_receber r
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_atendimento 	= p.nr_atendimento
and	r.nr_interno_conta	= a.nr_interno_conta
and	p.cd_estabelecimento	= cd_estabelecimento_p
and	r.dt_liquidacao	is null
and	r.nr_seq_protocolo is null
and	OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) > 0
and	((r.ie_situacao = ie_situacao_titulo_p) or (ie_situacao_titulo_p is null))
and	((ie_opcao_p = 'P' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) < nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'T' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) = nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'O' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) <= nvl(b.vl_guia,0)))
and	r.dt_pagamento_previsto between dt_inicial_p and dt_final_p
and	ie_ordem_p not in ('P','A','T')
and	((obter_se_contido(p.ie_tipo_convenio,ie_tipo_convenio_p) = 'S') or (ie_tipo_convenio_p is null))
group by	r.dt_pagamento_previsto
union
select	r.dt_pagamento_previsto dt_pagamento_previsto
from	atendimento_paciente p,
	conta_paciente_guia b,
	conta_paciente a,
	titulo_receber r
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_atendimento 	= p.nr_atendimento
and	r.nr_seq_protocolo 	= a.nr_seq_protocolo
and	p.cd_estabelecimento	= cd_estabelecimento_p
and	r.dt_liquidacao	is null
and	r.nr_interno_conta is null
and	OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) > 0
and	((r.ie_situacao = ie_situacao_titulo_p) or (ie_situacao_titulo_p is null))
and	((ie_opcao_p = 'P' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) < nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'T' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) = nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'O' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) <= nvl(b.vl_guia,0)))
and	r.dt_pagamento_previsto between dt_inicial_p and dt_final_p
and	ie_ordem_p not in ('P','A','T')
and	((obter_se_contido(p.ie_tipo_convenio,ie_tipo_convenio_p) = 'S') or (ie_tipo_convenio_p is null))
and	((obter_se_contido(a.cd_convenio_parametro,cd_convenio_p) = 'S') or (cd_convenio_p is null))
and	((obter_se_contido(obter_setor_atendimento(a.nr_atendimento),cd_setor_atendimento_p) = 'S') or 

(cd_setor_atendimento_p is null))
group by	r.dt_pagamento_previsto;

/*Onde o NR_SEQ_PROTOCOLO � nulo, na tabela titulo_receber*/
cursor C02 is
select	a.nr_atendimento,
	substr(obter_pessoa_atendimento(a.nr_atendimento,'N'),1,40) nm_paciente,
	substr(obter_matricula_usuario(a.cd_convenio_parametro, a.nr_atendimento),1,254) cd_matricula,
	b.cd_autorizacao,
	a.nr_protocolo,
	nvl(b.vl_guia,0) vl_conta,
	r.nr_titulo,
	a.nr_interno_conta nr_interno_conta,
	obter_proc_principal(a.nr_atendimento,a.cd_convenio_parametro,p.ie_tipo_atendimento,a.nr_interno_conta,'D') ds_procedimento,
	a.dt_mesano_referencia dt_emissao,
	obter_dados_titulo_receber(r.nr_titulo,'NF') nr_nota_protocolo,
	r.dt_pagamento_previsto,
	r.nr_nota_fiscal
from	atendimento_paciente p,
	conta_paciente_guia b,
	conta_paciente a,
	titulo_receber r
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_atendimento 	= p.nr_atendimento
and	r.nr_interno_conta	= a.nr_interno_conta
and	p.cd_estabelecimento	= cd_estabelecimento_p
and	r.dt_liquidacao	is null
and	r.nr_seq_protocolo is null
and	((ie_opcao_p = 'P' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) < nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'T' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) = nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'O' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) <= nvl(b.vl_guia,0)))
and	OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) > 0
and	r.dt_pagamento_previsto = dt_pagamento_previsto_w
and	((r.ie_situacao = ie_situacao_titulo_p) or (ie_situacao_titulo_p is null))
and	((r.nr_titulo = nr_titulo_p) or (nr_titulo_p is null))
and	ie_ordem_p not in ('P','A','T')
and	ie_opcao_data_w = 1
and	((obter_se_contido(p.ie_tipo_convenio,ie_tipo_convenio_p) = 'S') or (ie_tipo_convenio_p is null))
and	((obter_se_contido(a.cd_convenio_parametro,cd_convenio_p) = 'S') or (cd_convenio_p is null))
and	((obter_se_contido(obter_setor_atendimento(a.nr_atendimento),cd_setor_atendimento_p) = 'S') or 
(cd_setor_atendimento_p is null))
group by 	a.nr_atendimento,
		a.cd_convenio_parametro,
		b.cd_autorizacao,
		a.nr_protocolo,
		nvl(b.vl_guia,0),
		r.nr_titulo,
		a.nr_interno_conta,
		obter_proc_principal(a.nr_atendimento,a.cd_convenio_parametro,p.ie_tipo_atendimento,a.nr_interno_conta,'D'),
		a.dt_mesano_referencia,
		r.dt_pagamento_previsto,
		r.nr_nota_fiscal
union
select	a.nr_atendimento,
	substr(obter_pessoa_atendimento(a.nr_atendimento,'N'),1,40) nm_paciente,
	substr(obter_matricula_usuario(a.cd_convenio_parametro, a.nr_atendimento),1,254) cd_matricula,
	b.cd_autorizacao,
	a.nr_protocolo,
	nvl(b.vl_guia,0) vl_conta,
	r.nr_titulo,
	a.nr_interno_conta nr_interno_conta,
	obter_proc_principal(a.nr_atendimento,a.cd_convenio_parametro,p.ie_tipo_atendimento,a.nr_interno_conta,'D') ds_procedimento,
	a.dt_mesano_referencia dt_emissao,
	obter_dados_titulo_receber(r.nr_titulo,'NF') nr_nota_protocolo,
	r.dt_pagamento_previsto,
	r.nr_nota_fiscal
from	atendimento_paciente p,
	conta_paciente_guia b,
	conta_paciente a,
	titulo_receber r
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_atendimento 	= p.nr_atendimento
and	r.nr_interno_conta	= a.nr_interno_conta
and	p.cd_estabelecimento	= cd_estabelecimento_p
and	r.dt_liquidacao	is null
and	r.nr_seq_protocolo is null
and	((ie_opcao_p = 'P' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) < nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'T' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) = nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'O' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) <= nvl(b.vl_guia,0)))
and	OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) > 0
and	r.dt_pagamento_previsto between dt_inicial_p and dt_final_p
and	((r.ie_situacao = ie_situacao_titulo_p) or (ie_situacao_titulo_p is null))
and	((r.nr_titulo = nr_titulo_p) or (nr_titulo_p is null))
and	((ie_ordem_p  = 'P') or (ie_ordem_p  = 'A') or (ie_ordem_p = 'T'))
and	ie_opcao_data_w = 2
and	((obter_se_contido(p.ie_tipo_convenio,ie_tipo_convenio_p) = 'S') or (ie_tipo_convenio_p is null))
and	((obter_se_contido(a.cd_convenio_parametro,cd_convenio_p) = 'S') or (cd_convenio_p is null))
and	((obter_se_contido(obter_setor_atendimento(a.nr_atendimento),cd_setor_atendimento_p) = 'S') or 
(cd_setor_atendimento_p is null))
group by	a.nr_atendimento,
		a.cd_convenio_parametro,
		b.cd_autorizacao,
		a.nr_protocolo,
		nvl(b.vl_guia,0),
		r.nr_titulo,
		a.nr_interno_conta,
		obter_proc_principal(a.nr_atendimento,a.cd_convenio_parametro,p.ie_tipo_atendimento,a.nr_interno_conta,'D'),
		a.dt_mesano_referencia,
		r.dt_pagamento_previsto,
		r.nr_nota_fiscal;
/*Onde o NR_INTERNO_CONTA � nulo, na tabela titulo_receber*/
cursor C03 is
select	a.nr_atendimento,
	substr(obter_pessoa_atendimento(a.nr_atendimento,'N'),1,40) nm_paciente,
	substr(obter_matricula_usuario(a.cd_convenio_parametro, a.nr_atendimento),1,254) cd_matricula,
	b.cd_autorizacao,
	a.nr_protocolo,
	nvl(b.vl_guia,0) vl_conta,
	r.nr_titulo,
	a.nr_interno_conta,
	obter_proc_principal(a.nr_atendimento,a.cd_convenio_parametro,p.ie_tipo_atendimento,a.nr_interno_conta,'D') ds_procedimento,
	a.dt_mesano_referencia dt_emissao,
	obter_dados_titulo_receber(r.nr_titulo,'NF') nr_nota_protocolo,
	r.dt_pagamento_previsto,
	r.nr_nota_fiscal
from	atendimento_paciente p,
	conta_paciente_guia b,
	conta_paciente a,
	titulo_receber r
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_atendimento 	= p.nr_atendimento
and	r.nr_seq_protocolo 	= a.nr_seq_protocolo
and	p.cd_estabelecimento	= cd_estabelecimento_p
and	r.dt_liquidacao	is null
and	r.nr_interno_conta is null
and	OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) > 0
and	((ie_opcao_p = 'P' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) < nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'T' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) = nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'O' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) <= nvl(b.vl_guia,0)))
and	r.dt_pagamento_previsto = dt_pagamento_previsto_w
and	((r.ie_situacao = ie_situacao_titulo_p) or (ie_situacao_titulo_p is null))
and	((r.nr_titulo = nr_titulo_p) or (nr_titulo_p is null))
and	ie_ordem_p not in ('P','A','T')
and	ie_opcao_data_w = 1
and	((obter_se_contido(p.ie_tipo_convenio,ie_tipo_convenio_p) = 'S') or (ie_tipo_convenio_p is null))
and	((obter_se_contido(a.cd_convenio_parametro,cd_convenio_p) = 'S') or (cd_convenio_p is null))
and	((obter_se_contido(obter_setor_atendimento(a.nr_atendimento),cd_setor_atendimento_p) = 'S') or 
(cd_setor_atendimento_p is null))
group	by a.nr_atendimento,
	a.cd_convenio_parametro,
	b.cd_autorizacao,
	a.nr_protocolo,
	nvl(b.vl_guia,0),
	r.nr_titulo,
	a.nr_interno_conta,
	obter_proc_principal(a.nr_atendimento,a.cd_convenio_parametro,p.ie_tipo_atendimento,a.nr_interno_conta,'D'),
	a.dt_mesano_referencia,
	r.dt_pagamento_previsto,
	r.nr_nota_fiscal
union
select	a.nr_atendimento,
	substr(obter_pessoa_atendimento(a.nr_atendimento,'N'),1,40) nm_paciente,
	substr(obter_matricula_usuario(a.cd_convenio_parametro, a.nr_atendimento),1,254) cd_matricula,
	b.cd_autorizacao,
	a.nr_protocolo,
	nvl(b.vl_guia,0) vl_conta,
	r.nr_titulo,
	a.nr_interno_conta,
	obter_proc_principal(a.nr_atendimento,a.cd_convenio_parametro,p.ie_tipo_atendimento,a.nr_interno_conta,'D') ds_procedimento,
	a.dt_mesano_referencia dt_emissao,
	obter_dados_titulo_receber(r.nr_titulo,'NF') nr_nota_protocolo,
	r.dt_pagamento_previsto,
	r.nr_nota_fiscal
from	atendimento_paciente p,
	conta_paciente_guia b,
	conta_paciente a,
	titulo_receber r
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.nr_atendimento 	= p.nr_atendimento
and	r.nr_seq_protocolo 	= a.nr_seq_protocolo
and	p.cd_estabelecimento	= cd_estabelecimento_p
and	r.dt_liquidacao	is null
and	r.nr_interno_conta is null
and	OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) > 0
and	((ie_opcao_p = 'P' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) < nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'T' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) = nvl(b.vl_guia,0))
	or
	(ie_opcao_p = 'O' and OBTER_VALOR_PENDENTE_CONTA(a.nr_interno_conta, b.CD_AUTORIZACAO) <= nvl(b.vl_guia,0)))
and	r.dt_pagamento_previsto between dt_inicial_p and dt_final_p
and	((r.ie_situacao = ie_situacao_titulo_p) or (ie_situacao_titulo_p is null))
and	((r.nr_titulo = nr_titulo_p) or (nr_titulo_p is null))
and	((ie_ordem_p  = 'P') or (ie_ordem_p  = 'A') or (ie_ordem_p = 'T'))
and	ie_opcao_data_w = 2
and	((obter_se_contido(p.ie_tipo_convenio,ie_tipo_convenio_p) = 'S') or (ie_tipo_convenio_p is null))
and	((obter_se_contido(a.cd_convenio_parametro,cd_convenio_p) = 'S') or (cd_convenio_p is null))
and	((obter_se_contido(obter_setor_atendimento(a.nr_atendimento),cd_setor_atendimento_p) = 'S') or 
(cd_setor_atendimento_p is null))
group by 	a.nr_atendimento,
		a.cd_convenio_parametro,
		b.cd_autorizacao,
		a.nr_protocolo,
		nvl(b.vl_guia,0),
		r.nr_titulo,
		a.nr_interno_conta,
		obter_proc_principal(a.nr_atendimento,a.cd_convenio_parametro,p.ie_tipo_atendimento,a.nr_interno_conta,'D'),
		a.dt_mesano_referencia,
		r.dt_pagamento_previsto,
		r.nr_nota_fiscal;

BEGIN

delete from w_contas_pendentes;
commit;

if	(ie_ordem_p = 'V') then

	ie_opcao_data_w	:= 1;

	open C01;
	loop
		fetch C01 into
			dt_pagamento_previsto_w;
		exit when C01%notfound;

		open C02;
		loop
			fetch C02 into
				nr_atendimento_w,
				nm_paciente_w,
				cd_matricula_w,
				cd_autorizacao_w,
				nr_protocolo_w,
				vl_conta_w,
				nr_titulo_w,
				nr_interno_conta_w,
				ds_procedimento_w,
				dt_emissao_w,
				nr_nota_protocolo_w,
				dt_pagamento_previsto_ww,
				nr_nota_fiscal_w;
			exit when C02%notfound;

			/*Obtendo o Valor Da Glosa*/
			Select	nvl(sum(vl_glosado),0) vl_glosa
			into	vl_glosa_w
			from	convenio_retorno_item
			where	nr_titulo = nr_titulo_w
			and	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor Amaior*/
			Select	nvl(sum(vl_adicional),0) vl_amaior
			into	vl_amaior_w
			from	convenio_retorno_item
			where	nr_titulo = nr_titulo_w
			and	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor Amenor*/
			select	obter_valor_amenor_conta(nr_interno_conta_w,cd_autorizacao_w) vl_amenor
			into	vl_amenor_w
			from	dual;

			/*Obtendo o valor pago*/
			select	nvl(sum(vl_pago),0) vl_pago
			into	vl_pago_w
			from	convenio_retorno_item
			where	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor do saldo*/
			Select	nvl(vl_conta_w - sum(vl_pago + vl_glosado),vl_conta_w) vl_saldo
			into	vl_saldo_w
			from	convenio_retorno_item
			where	nr_interno_conta = nr_interno_conta_w;

			insert into w_contas_pendentes(
					nr_atendimento,
					nm_paciente,
					cd_matricula,
					cd_autorizacao,
					nr_protocolo,
					vl_conta,
					dt_pagamento_previsto,
					vl_pendente,
					nr_titulo,
					nr_interno_conta,
					ds_procedimento,
					dt_emissao,
					nr_nota_protocolo,
					vl_glosa,
					vl_amaior,
					vl_amenor,
					cd_estabelecimento,
					vl_pago,
					nr_nota_fiscal
					)
			values		(nr_atendimento_w,
					nm_paciente_w,
					cd_matricula_w,
					cd_autorizacao_w,
					nr_protocolo_w,
					vl_conta_w,
					dt_pagamento_previsto_w,
					vl_saldo_w,
					nr_titulo_w,
					nr_interno_conta_w,
					ds_procedimento_w,
					dt_emissao_w,
					nr_nota_protocolo_w,
					vl_glosa_w,
					vl_amaior_w,
					vl_amenor_w,
					cd_estabelecimento_p,
					vl_pago_w,
					nr_nota_fiscal_w
					);
					
		end loop; /*Cursor 02*/
		close C02;

		open C03;
		loop
			fetch C03 into
				nr_atendimento_w,
				nm_paciente_w,
				cd_matricula_w,
				cd_autorizacao_w,
				nr_protocolo_w,
				vl_conta_w,
				nr_titulo_w,
				nr_interno_conta_w,
				ds_procedimento_w,
				dt_emissao_w,
				nr_nota_protocolo_w,
				dt_pagamento_previsto_ww,
				nr_nota_fiscal_w;
			exit when C03%notfound;

			/*Obtendo o Valor Da Glosa*/
			Select	nvl(sum(vl_glosado),0) vl_glosa
			into	vl_glosa_w
			from	convenio_retorno_item
			where	nr_titulo = nr_titulo_w
			and	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor Amaior*/
			Select	nvl(sum(vl_adicional),0) vl_amaior
			into	vl_amaior_w
			from	convenio_retorno_item
			where	nr_titulo = nr_titulo_w
			and	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor Amenor*/
			select	obter_valor_amenor_conta(nr_interno_conta_w,cd_autorizacao_w) vl_amenor
			into	vl_amenor_w
			from	dual;

			/*Obtendo o valor pago*/
			select	nvl(sum(vl_pago),0) vl_pago
			into	vl_pago_w
			from	convenio_retorno_item
			where	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor do saldo*/
			Select	nvl(vl_conta_w - sum(vl_pago + vl_glosado),vl_conta_w) vl_saldo
			into	vl_saldo_w
			from	convenio_retorno_item
			where	nr_interno_conta = nr_interno_conta_w;
	
		insert into w_contas_pendentes(
				nr_atendimento,
				nm_paciente,
				cd_matricula,
				cd_autorizacao,
				nr_protocolo,
				vl_conta,
				dt_pagamento_previsto,
				vl_pendente,
				nr_titulo,
				nr_interno_conta,
				ds_procedimento,
				dt_emissao,
				nr_nota_protocolo,
				vl_glosa,
				vl_amaior,
				vl_amenor,
				cd_estabelecimento,
				vl_pago,
				nr_nota_fiscal
				)
		values		(nr_atendimento_w,
				nm_paciente_w,
				cd_matricula_w,
				cd_autorizacao_w,
				nr_protocolo_w,
				vl_conta_w,
				dt_pagamento_previsto_w,
				vl_saldo_w,
				nr_titulo_w,
				nr_interno_conta_w,
				ds_procedimento_w,
				dt_emissao_w,
				nr_nota_protocolo_w,
				vl_glosa_w,
				vl_amaior_w,
				vl_amenor_w,
				cd_estabelecimento_p,
				vl_pago_w,
				nr_nota_fiscal_w
			);

		end loop; 
		close C03;

	end loop; /*Cursor 01*/
	close C01;

else

	ie_opcao_data_w	:= 2;

		open C02;
		loop
			fetch C02 into
				nr_atendimento_w,
				nm_paciente_w,
				cd_matricula_w,
				cd_autorizacao_w,
				nr_protocolo_w,
				vl_conta_w,
				nr_titulo_w,
				nr_interno_conta_w,
				ds_procedimento_w,
				dt_emissao_w,
				nr_nota_protocolo_w,
				dt_pagamento_previsto_w,
				nr_nota_fiscal_w;
			exit when C02%notfound;

			/*Obtendo o Valor Da Glosa*/
			Select	nvl(sum(vl_glosado),0) vl_glosa
			into	vl_glosa_w
			from	convenio_retorno_item
			where	nr_titulo = nr_titulo_w
			and	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor Amaior*/
			Select	nvl(sum(vl_adicional),0) vl_amaior
			into	vl_amaior_w
			from	convenio_retorno_item
			where	nr_titulo = nr_titulo_w
			and	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor Amenor*/
			select	obter_valor_amenor_conta(nr_interno_conta_w,cd_autorizacao_w) vl_amenor
			into	vl_amenor_w
			from	dual;

			/*Obtendo o valor pago*/
			select	nvl(sum(vl_pago),0) vl_pago
			into	vl_pago_w
			from	convenio_retorno_item
			where	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor do saldo*/
			Select	nvl(vl_conta_w - sum(vl_pago + vl_glosado),vl_conta_w) vl_saldo
			into	vl_saldo_w
			from	convenio_retorno_item
			where	nr_interno_conta = nr_interno_conta_w;

			insert into w_contas_pendentes(
					nr_atendimento,
					nm_paciente,
					cd_matricula,
					cd_autorizacao,
					nr_protocolo,
					vl_conta,
					dt_pagamento_previsto,
					vl_pendente,
					nr_titulo,
					nr_interno_conta,
					ds_procedimento,
					dt_emissao,
					nr_nota_protocolo,
					vl_glosa,
					vl_amaior,
					vl_amenor,
					cd_estabelecimento,
					vl_pago,
					nr_nota_fiscal
					)
			values		(nr_atendimento_w,
					nm_paciente_w,
					cd_matricula_w,
					cd_autorizacao_w,
					nr_protocolo_w,
					vl_conta_w,
					dt_pagamento_previsto_w,
					vl_saldo_w,
					nr_titulo_w,
					nr_interno_conta_w,
					ds_procedimento_w,
					dt_emissao_w,
					nr_nota_protocolo_w,
					vl_glosa_w,
					vl_amaior_w,
					vl_amenor_w,
					cd_estabelecimento_p,
					vl_pago_w,
					nr_nota_fiscal_w
					);
					
		end loop;
		close C02;

		open C03;
		loop
			fetch C03 into
				nr_atendimento_w,
				nm_paciente_w,
				cd_matricula_w,
				cd_autorizacao_w,
				nr_protocolo_w,
				vl_conta_w,
				nr_titulo_w,
				nr_interno_conta_w,
				ds_procedimento_w,
				dt_emissao_w,
				nr_nota_protocolo_w,
				dt_pagamento_previsto_w,
				nr_nota_fiscal_w;
			exit when C03%notfound;

			/*Obtendo o Valor Da Glosa*/
			Select	nvl(sum(vl_glosado),0) vl_glosa
			into	vl_glosa_w
			from	convenio_retorno_item
			where	nr_titulo = nr_titulo_w
			and	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor Amaior*/
			Select	nvl(sum(vl_adicional),0) vl_amaior
			into	vl_amaior_w
			from	convenio_retorno_item
			where	nr_titulo = nr_titulo_w
			and	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor Amenor*/
			select	obter_valor_amenor_conta(nr_interno_conta_w,cd_autorizacao_w) vl_amenor
			into	vl_amenor_w
			from	dual;

			/*Obtendo o valor pago*/
			select	nvl(sum(vl_pago),0) vl_pago
			into	vl_pago_w
			from	convenio_retorno_item
			where	nr_interno_conta = nr_interno_conta_w;

			/*Obtendo o valor do saldo*/
			Select	nvl(vl_conta_w - sum(vl_pago + vl_glosado),vl_conta_w) vl_saldo
			into	vl_saldo_w
			from	convenio_retorno_item
			where	nr_interno_conta = nr_interno_conta_w;
	
		insert into w_contas_pendentes(
				nr_atendimento,
				nm_paciente,
				cd_matricula,
				cd_autorizacao,
				nr_protocolo,
				vl_conta,
				dt_pagamento_previsto,
				vl_pendente,
				nr_titulo,
				nr_interno_conta,
				ds_procedimento,
				dt_emissao,
				nr_nota_protocolo,
				vl_glosa,
				vl_amaior,
				vl_amenor,
				cd_estabelecimento,
				vl_pago,
				nr_nota_fiscal
				)
		values		(nr_atendimento_w,
				nm_paciente_w,
				cd_matricula_w,
				cd_autorizacao_w,
				nr_protocolo_w,
				vl_conta_w,
				dt_pagamento_previsto_w,
				vl_saldo_w,
				nr_titulo_w,
				nr_interno_conta_w,
				ds_procedimento_w,
				dt_emissao_w,
				nr_nota_protocolo_w,
				vl_glosa_w,
				vl_amaior_w,
				vl_amenor_w,
				cd_estabelecimento_p,
				vl_pago_w,
				nr_nota_fiscal_w
				);

		end loop; 
		close C03;
end if;

commit;

END GERAR_CONTAS_PENDENTES_W;
/