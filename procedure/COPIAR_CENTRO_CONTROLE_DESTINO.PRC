create or replace
procedure copiar_centro_controle_destino(	cd_estabelecimento_p	number,
					cd_seq_criterio_origem_p	number,
					cd_seq_criterio_destino_p	number,
					nm_usuario_p		varchar2) is 


cd_centro_controle_dest_w	number(8);
qt_distribuicao_w		number(15,4);
pr_distribuicao_w		number(7,4);
cd_natureza_gasto_dest_w	number(20);
qt_peso_w		number(15,4);
nr_seq_ng_dest_w		number(10);
cd_estabelecimento_w		number(10);

cursor c01 is
select	cd_estabelecimento,
	cd_centro_controle_dest,
	qt_distribuicao,
	pr_distribuicao,
	cd_natureza_gasto_dest,
	nr_seq_ng_dest,
	qt_peso
from 	criterio_distr_orc_dest
where 	cd_sequencia_criterio	= cd_seq_criterio_origem_p;

begin

delete	from criterio_distr_orc_dest
where	cd_sequencia_criterio	= cd_seq_criterio_destino_p;

open c01;
loop
fetch c01 into
	cd_estabelecimento_w,
	cd_centro_controle_dest_w,
	qt_distribuicao_w,
	pr_distribuicao_w,
	cd_natureza_gasto_dest_w,
	nr_seq_ng_dest_w,
	qt_peso_w;
exit when c01%notfound;
	begin
	
	insert into criterio_distr_orc_dest(
		cd_sequencia_criterio,
		cd_estabelecimento,
		cd_centro_controle_dest,
		qt_distribuicao,
		pr_distribuicao,
		cd_natureza_gasto_dest,
		nr_seq_ng_dest,
		dt_atualizacao,
		nm_usuario,
		qt_peso,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(cd_seq_criterio_destino_p,
		cd_estabelecimento_w,
		cd_centro_controle_dest_w,
		qt_distribuicao_w,
		pr_distribuicao_w,
		cd_natureza_gasto_dest_w,
		nr_seq_ng_dest_w,
		sysdate,
		nm_usuario_p,
		qt_peso_w,
		sysdate,
		nm_usuario_p);
		
	end;
end loop;
close c01;


commit;
end copiar_centro_controle_destino;
/