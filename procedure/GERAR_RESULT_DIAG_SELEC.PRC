create or replace
procedure gerar_result_diag_selec(
			nr_seq_diag_p		number,
			nr_seq_result_esp_p	number,
			nm_usuario_p		varchar2) is 

begin

if	(nr_seq_diag_p	is not null) and
	(nr_seq_result_esp_p is not null) then

	insert into pe_prescr_diag_res_esp 
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_diag_result,
					nr_seq_diag)
					values
					(pe_prescr_diag_res_esp_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_result_esp_p,
					nr_seq_diag_p);
end if;


commit;

end gerar_result_diag_selec;
/
