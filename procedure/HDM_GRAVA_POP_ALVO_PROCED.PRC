create or replace
procedure hdm_grava_pop_alvo_proced(	nr_seq_populacao_alvo_p number, 
				nr_seq_proced_p number,
				cd_pessoa_fisica_p varchar2,
				dt_procedimento_p date,
				cd_procedimento_p number,
				ie_origem_proced_p number
				) is  
begin	        
	if ((nr_seq_populacao_alvo_p is not null) ) then
		mprev_gravar_dados_pessoa_pck.grava_pop_alvo_procedimentos(nr_seq_populacao_alvo_p, 
					nr_seq_proced_p,
					cd_pessoa_fisica_p,
					dt_procedimento_p,
					cd_procedimento_p,
					ie_origem_proced_p
					);
					
		commit;
	end if;       
										
end hdm_grava_pop_alvo_proced;
/
