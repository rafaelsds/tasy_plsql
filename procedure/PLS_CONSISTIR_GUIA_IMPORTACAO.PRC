create or replace
procedure pls_consistir_guia_importacao
			(	nr_seq_guia_p		Number,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 
				
cd_cnes_imp_w			Varchar2(20);
cd_usuario_plano_imp_w		Varchar2(30);
cd_usuario_plano_imp_ww		Varchar2(30);
nr_seq_prestador_w		Number(10);
cd_usuario_plano_w		Varchar2(30);
qt_benef_valido_w		Number(5)	:= 0;
qt_prestador_valido_w		Number(5)	:= 0;
cd_cnes_w			Varchar2(20);
cd_medico_w			Varchar2(10);
nm_medico_solicitante_imp_w	Varchar2(255);
nr_seq_guia_proc_w		Number(10);
cd_procedimento_imp_w		Number(15);
dados_tipo_conv_tiss_w		pls_cta_valorizacao_pck.dados_tipo_conv_tiss;
ie_origem_proced_w		Number(10);
qt_registro_w			Number(5)	:= 0;
nr_seq_guia_mat_w		Number(10);
cd_material_imp_w		pls_guia_plano_mat.cd_material_imp%type;
nr_seq_diagnostico_w		Number(10);
ie_tipo_doenca_imp_w		Varchar2(1);
ie_indicacao_acidente_imp_w	Varchar2(1);
cd_doenca_imp_w			Varchar2(10);
ds_diagnostico_imp_w		Varchar2(2000);
ie_classificacao_imp_w		Varchar2(1);
nr_seq_segurado_w		Number(10);
nr_seq_plano_w			Number(10);
ie_valida_zero_w		Varchar(1) := 'N';
nr_seq_cooperativa_w		number(10);
cd_cgc_ortogante_w		varchar2(14);
cd_guia_manual_imp_w		varchar2(20);
dt_solicitacao_imp_w		date;
nr_guia_auditoria_w		number(10);
qt_medico_w			number(10);
nr_crm_imp_w			varchar2(20);
ds_conselho_profissional_imp_w	varchar2(255);
ie_regra_integracao_aut_w	varchar2(1) := 'N';
cd_versao_w			pls_guia_plano.cd_versao%type;
nr_seq_clinica_imp_w		pls_guia_plano.nr_seq_clinica_imp%type;
ie_carater_internacao_imp_w	pls_guia_plano.ie_carater_internacao_imp%type;
nr_seq_material_w		pls_guia_plano_mat.nr_seq_material%type;
versao_tiss_operadora		varchar2(100);
ie_origem_solic_w		pls_guia_plano.ie_origem_solic%type;
ie_tipo_guia_w			pls_guia_plano.ie_tipo_guia%type;
nr_seq_cbo_saude_w		cbo_saude.nr_sequencia%type;
cd_cbo_saude_imp_w		cbo_saude.cd_cbo%type;
uf_crm_imp_w			pls_guia_plano.uf_crm_imp%type;

Cursor c01 is
	select	nr_sequencia, 
		cd_procedimento_imp,
		cd_tipo_tabela_imp
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_p;

Cursor c02 is
	select	nr_sequencia,
		cd_material_imp,
		nr_seq_material
	from	pls_guia_plano_mat
	where	nr_seq_guia	= nr_seq_guia_p;

Cursor c03 is
	select	nr_sequencia,
		ie_tipo_doenca_imp,
		ie_indicacao_acidente_imp,
		cd_doenca_imp,
		ds_diagnostico_imp,
		ie_classificacao_imp
	from	pls_diagnostico
	where	nr_seq_guia	= nr_seq_guia_p;

begin

/* ST_TABELA
01 - Tabela AMB-90 (INATIVA)
02 - Tabela AMB 92 (INATIVA)
03 - Tabela AMB 96 (INATIVA)
04 - Tabela AMB 99 (INATIVA)
05 - Tabela Brasindice
06 - Classifica��o Brasileira Hierarquizada de Procedimentos M�dicos
07 - Tabela CIEFAS 93
08 - Tabela CIEFAS 2000
09 - Rol de procedimentos ANS
10 - Tabela de procedimentos ambulatoriais SUS
11 - Tabela de procedimentos hospitalares SUS
12 - Tabela SIMPRO
13 - Tabela TUNEP
14 - Tabela VRPO
15 - Tabela de Interc�mbio Sistema Uniodonto
16 - TUSS - Procedimentos M�dicos
17 - TUSS - Procedimentos Odontol�gicos
18 - TUSS - Taxas Hospitalares
19 - TUSS - Materiais
20 - TUSS - Medicamentos
21 - TUSS - Outras especialidades
91 - Tabela provis�ria TUSS 
92 - Tabela provis�ria TUSS - Procedimentos Odontol�gicos 
93 - Tabela provis�ria TUSS - Procedimentos M�dicos 
94 - Tabela pr�pria procedimento (INATIVA)
95 - Tabela pr�pria materiais
96 - Tabela pr�pria medicamentos
97 - Tabela pr�pria taxas hospitalares
98 - Tabela pr�pria pacotes
99 - Tabela pr�pria gases medicinais
00 - Outras tabelas (INATIVA) */

ie_valida_zero_w := pls_obter_param_padrao_funcao(3,1249);

/* Obter dados da guia importada */
select	cd_cnes_imp,
	cd_usuario_plano_imp,
	cd_cbo_saude_imp,
	pls_obter_prestador_imp(cd_cgc_prestador_imp, cd_cpf_prestador_imp, nr_seq_prestador_imp, '', '', '', 'G'),
	upper(elimina_acentuacao(nm_medico_solicitante_imp)),
	cd_guia_manual_imp,
	dt_solicitacao_imp,
	nr_crm_imp,
	ds_conselho_profissional_imp,
	nvl(cd_versao,'3.02.00'),
	nr_seq_clinica_imp,
	ie_carater_internacao_imp,
	ie_origem_solic,
	ie_tipo_guia,
	pls_obter_sg_uf_tiss(uf_crm_imp)
into	cd_cnes_imp_w,
	cd_usuario_plano_imp_w,
	cd_cbo_saude_imp_w,
	nr_seq_prestador_w,
	nm_medico_solicitante_imp_w,
	cd_guia_manual_imp_w,
	dt_solicitacao_imp_w,
	nr_crm_imp_w,
	ds_conselho_profissional_imp_w,
	cd_versao_w,
	nr_seq_clinica_imp_w,
	ie_carater_internacao_imp_w,
	ie_origem_solic_w,
	ie_tipo_guia_w,
	uf_crm_imp_w
from	pls_guia_plano
where	nr_sequencia	= nr_seq_guia_p;

if	(cd_cbo_saude_imp_w is null or cd_cbo_saude_imp_w = '') then	
	pls_gravar_motivo_glosa('1213', nr_seq_guia_p, null, null, '', nm_usuario_p, '', 'IG', nr_seq_prestador_w, null, null); -- CBOS(especialidade)inv�lido	
else
	select	max(nr_sequencia)
	into	nr_seq_cbo_saude_w
	from	cbo_saude a
	where	a.cd_cbo	=  cd_cbo_saude_imp_w
	and	((a.ie_situacao = 'A') or (a.ie_situacao is null));	

	if	(nr_seq_cbo_saude_w is null) then	
	pls_gravar_motivo_glosa('1213', nr_seq_guia_p, null, null, '', nm_usuario_p, '', 'IG', nr_seq_prestador_w, null, null); -- CBOS(especialidade)inv�lido

	end if;		
end if;

select	pls_obter_versao_tiss
into	versao_tiss_operadora
from	dual;

if	( cd_versao_w <> versao_tiss_operadora ) then
	pls_gravar_motivo_glosa('5028', nr_seq_guia_p, null, null, '', nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
end if;

select	max(cd_cgc_outorgante)
into	cd_cgc_ortogante_w
from	pls_outorgante
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(cd_cgc_ortogante_w is not null) then
	select	max(nr_sequencia)
	into	nr_seq_cooperativa_w
	from	pls_congenere
	where	cd_cgc = cd_cgc_ortogante_w;
end if;
	
if	(ie_valida_zero_w = 'S') then
	if	(length(cd_usuario_plano_imp_w) = 16) then
		cd_usuario_plano_imp_w := '0' || cd_usuario_plano_imp_w;
	end if;
end if;

select	max(cd_usuario_plano)
into	cd_usuario_plano_w
from	pls_segurado_carteira
where	cd_usuario_plano = cd_usuario_plano_imp_w;

/* Obter se o prestador est� cadastrado */
select	count(1)
into	qt_prestador_valido_w
from	pls_prestador
where	nr_sequencia	= nr_seq_prestador_w;

if	((cd_usuario_plano_w is null) or (qt_prestador_valido_w = 0)) then
	if	(cd_usuario_plano_w is null) then
		pls_gravar_motivo_glosa('1001', nr_seq_guia_p, null, null, 'Carteira importada: ' || cd_usuario_plano_imp_w, 
					nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
	end if;
	
	if	(qt_prestador_valido_w = 0) then
				pls_gravar_motivo_glosa('1203', nr_seq_guia_p, null, null, 'Prestador: ' || nr_seq_prestador_w, 
					nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
	end if;
		
	update  pls_guia_plano_mat
	set	ie_status = 'N'
	where	nr_seq_guia = nr_seq_guia_p;
	
	update  pls_guia_plano_proc
	set	ie_status = 'N'
	where	nr_seq_guia = nr_seq_guia_p;
	
	update	pls_guia_plano
	set	ie_status = '3',
		ie_estagio = '7'
	where	nr_sequencia = nr_seq_guia_p;
	
	commit;
else
	select	count(1)
	into	qt_benef_valido_w
	from	pls_segurado		b,
		pls_segurado_carteira	a
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	a.cd_usuario_plano	= cd_usuario_plano_w
	and	b.ie_tipo_segurado	<> 'P'
	and	nvl(a.dt_validade_carteira,sysdate) >= sysdate
	and	a.dt_inicio_vigencia <= sysdate;
	
	if	(qt_benef_valido_w = 0) then
		pls_gravar_motivo_glosa('1017', nr_seq_guia_p, null, null, 'Carteira: ' ||cd_usuario_plano_w, 
					nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
	end if;
	
	select	b.nr_sequencia,
		pls_obter_produto_benef(b.nr_sequencia, sysdate)
	into	nr_seq_segurado_w,
		nr_seq_plano_w
	from	pls_segurado 		b,
		pls_segurado_carteira	a
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	a.cd_usuario_plano	= cd_usuario_plano_w
	and	b.cd_estabelecimento	= cd_estabelecimento_p;
	
	if	(nvl(nr_seq_segurado_w,0) > 0) then
		pls_consistir_elegibilidade(nr_seq_segurado_w, 'IG', nr_seq_guia_p,
			'A', nr_seq_prestador_w, null,
			'', nm_usuario_p, cd_estabelecimento_p);
	end if;	
	
	
	select	substr(pls_obter_cnes_prestador(nr_sequencia),1,255)
	into	cd_cnes_w
	from	pls_prestador
	where	nr_sequencia	= nr_seq_prestador_w;

	if	(cd_cnes_w <> cd_cnes_imp_w) then
		pls_gravar_motivo_glosa('1202', nr_seq_guia_p, null, null, 
					'CNES prestador: ' || cd_cnes_w || ' / CNES imp.: ' || cd_cnes_imp_w || ' / Prestador: ' || nr_seq_prestador_w,
					nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
	end if;
	
	if	(nm_medico_solicitante_imp_w = '') then
		/* OS 284008 - O tratamento desta glosa ser� feito na pls_tiss_consistir_autorizacao*/
		--pls_gravar_motivo_glosa('1411', nr_seq_guia_p, null, null, '', nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
		nm_medico_solicitante_imp_w := '';
	else			
		begin
		if	(nvl(nr_crm_imp_w,'X') <> 'X' and nvl(ds_conselho_profissional_imp_w,'X') <> 'X') then
			if	( cd_versao_w >= '3.02.00') then
				select	nvl(max(somente_numero(b.cd_pessoa_fisica)),0)
				into	cd_medico_w
				from	medico         a,
					pessoa_fisica  b 
				where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
				and	a.nr_crm		= nr_crm_imp_w
				and	((a.uf_crm		= uf_crm_imp_w) or (a.uf_crm is null) or (uf_crm_imp_w is null))
				and	b.nr_seq_conselho	= pls_obter_seq_conselho_tiss(ds_conselho_profissional_imp_w);						
			else
				select	nvl(max(somente_numero(b.cd_pessoa_fisica)),0)
				into	cd_medico_w
				from	medico         a,
					pessoa_fisica  b 
				where	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
				and	a.nr_crm		= nr_crm_imp_w
				and	((a.uf_crm		= uf_crm_imp_w) or (a.uf_crm is null) or (uf_crm_imp_w is null))
				and	b.nr_seq_conselho	= pls_obter_seq_conselho(ds_conselho_profissional_imp_w);		
			end if;
		else		
			select	nvl(max(somente_numero(cd_pessoa_fisica)),0)
			into	cd_medico_w
			from	medico
			where	upper(elimina_acentuacao(substr(obter_nome_medico(cd_pessoa_fisica,'N'),1,255))) = nm_medico_solicitante_imp_w;
		end if;
		exception
		when others then
			cd_medico_w := 0;
		end;
		
		select	count(1)
		into	qt_medico_w
		from	pls_prestador_medico
		where	nr_seq_prestador = nr_seq_prestador_w
		and	cd_medico	= cd_medico_w
		and	ie_situacao	= 'A'
		and	trunc(nvl(dt_solicitacao_imp_w,sysdate),'dd') between trunc(nvl(dt_inclusao,nvl(dt_solicitacao_imp_w,sysdate)),'dd') and  fim_dia(nvl(dt_exclusao,nvl(dt_solicitacao_imp_w,sysdate)));
		
		/*select	nvl(max(somente_numero(cd_medico)),0)
		into	cd_medico_w
		from	pls_prestador_medico
		where	nr_seq_prestador	= nr_seq_prestador_w
		and	upper(elimina_acentuacao(substr(obter_nome_medico(cd_medico,'N'),1,255))) = nm_medico_solicitante_imp_w
		and	ie_situacao	= 'A'
		and	trunc(nvl(dt_solicitacao_imp_w,sysdate),'dd') between trunc(nvl(dt_inclusao,nvl(dt_solicitacao_imp_w,sysdate)),'dd') and  fim_dia(nvl(dt_exclusao,nvl(dt_solicitacao_imp_w,sysdate)));*/

		if	(qt_medico_w = 0) then
			ie_regra_integracao_aut_w := pls_obter_se_regra_integ_aut(nr_seq_prestador_w,'1210',nm_usuario_p);
			if	(ie_regra_integracao_aut_w = 'N') then
				pls_gravar_motivo_glosa('1210', nr_seq_guia_p, null, null, 
							'Prestador: ' || nr_seq_prestador_w || ' / Solic.: ' || nm_medico_solicitante_imp_w, 
							nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
			end if;
		end if;
	end if;		
	
	
	open c01;
	loop
	fetch c01 into	
		nr_seq_guia_proc_w,
		cd_procedimento_imp_w,
		dados_tipo_conv_tiss_w.ie_tipo_tabela;
	exit when c01%notfound;
		begin
		dados_tipo_conv_tiss_w	:= pls_obter_conversao_tab_tiss(dados_tipo_conv_tiss_w.ie_tipo_tabela, cd_estabelecimento_p, cd_procedimento_imp_w, '', 'A', nr_seq_prestador_w, null);
		
		--Processo utilizado para vers�o 3.01.00 do TISS
		if	( cd_versao_w >= '3.02.00'  ) then	
			if	(dados_tipo_conv_tiss_w.ie_tipo_tabela = '06') then
				ie_origem_proced_w	:= 5; /* CBHPM */
			elsif	(dados_tipo_conv_tiss_w.ie_tipo_tabela in ('18','22')) then
				ie_origem_proced_w	:= 8; --TUSS 		
			elsif	(dados_tipo_conv_tiss_w.ie_tipo_tabela in('90','98','00')) then
				ie_origem_proced_w	:= 4; --PROPRIO 				
			end if;
		else
			if	(dados_tipo_conv_tiss_w.ie_tipo_tabela in ('01','02','03','04','07','08')) then
				ie_origem_proced_w	:= 1; /* AMB */
			elsif	(dados_tipo_conv_tiss_w.ie_tipo_tabela = '06') then
				ie_origem_proced_w	:= 5; /* CBHPM */		
			elsif	(dados_tipo_conv_tiss_w.ie_tipo_tabela in ('10', '11')) then
				ie_origem_proced_w	:= 7; /* SUS_2008 */	
			elsif	(dados_tipo_conv_tiss_w.ie_tipo_tabela in ('16', '18')) then
				ie_origem_proced_w	:= 8; /* TUSS */		
			elsif	(dados_tipo_conv_tiss_w.ie_tipo_tabela in('94','95','96','97','98','99','00')) then
				ie_origem_proced_w	:= 4; /* PROPRIO */
			end if;			
		end if;		
		
		/* Tratar a convers�o de procedimentos TUSS - OPS - Cadastro de Regras / Procedimentos TUSS */
		if	(ie_origem_proced_w	= 8) then
			pls_converte_codigo_tuss(cd_procedimento_imp_w, ie_origem_proced_w, cd_procedimento_imp_w, ie_origem_proced_w);
		end if;

		select	count(*)
		into	qt_registro_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_imp_w
		and	ie_origem_proced	= ie_origem_proced_w;
		
		if	(qt_registro_w	= 0) then
			update	pls_guia_plano_proc
			set	ie_status		= 'N'
			where	nr_sequencia		= nr_seq_guia_proc_w;

			pls_gravar_motivo_glosa('1801', null, nr_seq_guia_proc_w, null, 
						'Procedimento: ' || cd_procedimento_imp_w || ' / Origem: ' || ie_origem_proced_w, 
						nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
		else
			update	pls_guia_plano_proc
			set	dt_atualizacao		= sysdate,
				ie_status		= 'U',
				nm_usuario		= nm_usuario_p,
				cd_procedimento		= cd_procedimento_imp_w,
				ie_origem_proced	= ie_origem_proced_w,
				qt_solicitada		= qt_solicitada_imp
			where	nr_sequencia		= nr_seq_guia_proc_w;
			
			pls_tiss_consistir_guia(nr_seq_guia_proc_w, 'AP', 'IG', 
					null, null, '', 
					nm_usuario_p, cd_estabelecimento_p);
		end if;
		end;
	end loop;
	close c01;
	
	open c02;
	loop
	fetch c02 into	
		nr_seq_guia_mat_w,
		cd_material_imp_w,
		nr_seq_material_w;
	exit when c02%notfound;
		begin
		
		if	(nr_seq_material_w is null and cd_material_imp_w is null) then
			update	pls_guia_plano_mat
			set	ie_status		= 'N'
			where	nr_sequencia		= nr_seq_guia_mat_w;
			
			pls_gravar_motivo_glosa('2003', nr_seq_guia_p, null, null, '', nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null); /* Material n�o especificado */
		else	
			select	max(nr_sequencia)
			into	nr_seq_material_w
			from	pls_material
			where	cd_material_ops = cd_material_imp_w;	

			if	(nr_seq_material_w is not null) then
				update	pls_guia_plano_mat
				set	nr_seq_material = nr_seq_material_w,
					ie_status	= 'U',
					nm_usuario	= nm_usuario_p,
					qt_solicitada	= qt_solicitada_imp,
					dt_atualizacao	= sysdate
				where	nr_sequencia = nr_seq_guia_mat_w;
			else
				update	pls_guia_plano_mat
				set	ie_status		= 'N'
				where	nr_sequencia		= nr_seq_guia_mat_w;
				
				pls_gravar_motivo_glosa('2001', null, null, nr_seq_guia_mat_w, '', nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null); /* Material inv�lido */
			end if;
		end if;		
		end;
	end loop;
	close c02;
	
	open c03;
	loop
	fetch c03 into
		nr_seq_diagnostico_w,
		ie_tipo_doenca_imp_w,
		ie_indicacao_acidente_imp_w,
		cd_doenca_imp_w,
		ds_diagnostico_imp_w,
		ie_classificacao_imp_w;
	exit when c03%notfound;
		begin
		if	(ie_tipo_doenca_imp_w <> 'A') and
			(ie_tipo_doenca_imp_w <> 'C') then
			pls_gravar_motivo_glosa('1502', nr_seq_guia_p, null, null, '', nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
			ie_tipo_doenca_imp_w	:= '';
		end if;

		if	(ie_indicacao_acidente_imp_w <> '0') and
			(ie_indicacao_acidente_imp_w <> '1') and
			(ie_indicacao_acidente_imp_w <> '2') and
			(ie_indicacao_acidente_imp_w <> '9') and
			---Para as guias geradas pelo WebService ( ie_origem_solic = 'E') , s� dever� consistir a Glosa se o tipo de guia for de interna��o.
			((ie_origem_solic_w <> 'E') or (ie_origem_solic_w = 'E' and ie_tipo_guia_w = 1)) then
			pls_gravar_motivo_glosa('1503', nr_seq_guia_p, null, null, '', nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
			ie_indicacao_acidente_imp_w	:= '';
		end if;

		if	(cd_doenca_imp_w = '') then
			pls_gravar_motivo_glosa('1508', nr_seq_guia_p, null, null, '', nm_usuario_p, '',' IG', nr_seq_prestador_w, null,null);
		else
		
			select	count(*)
			into	qt_registro_w
			from	cid_doenca
			where	cd_doenca_cid	= cd_doenca_imp_w;
			
			if	(qt_registro_w	= 0) then
				pls_gravar_motivo_glosa('1509', nr_seq_guia_p, null, null, 'CID: ' || cd_doenca_imp_w, 
							nm_usuario_p, '', 'IG', nr_seq_prestador_w, null,null);
			else
				update	pls_diagnostico
				set	dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p,
					ie_tipo_doenca		= ie_tipo_doenca_imp_w,
					ie_indicacao_acidente	= ie_indicacao_acidente_imp_w,
					ie_classificacao	= ie_classificacao_imp_w,
					cd_doenca		= cd_doenca_imp_w,
					ds_diagnostico		= ds_diagnostico_imp_w
				where	nr_sequencia		= nr_seq_diagnostico_w;	
			end if;
		end if;
		end;
	end loop;
	close c03;
	
	begin
		select	nr_sequencia
		into	nr_seq_clinica_imp_w
		from	pls_clinica
		where	nvl(cd_tiss, cd_clinica) = nr_seq_clinica_imp_w;
	exception
	when others then
		nr_seq_clinica_imp_w := null;
	end;
	
	/*Rotina utilizada para validar se os procedimentos enviados exigem anexo conforme regra criada na fun��o
	OPS - Cadastro de Regras / OPS - Atendimento / Regra lan�amento anexo guia tiss */
	pls_gerar_regra_anexo_ws(nr_seq_guia_p, null, nm_usuario_p);
	
	pls_tiss_consistir_autorizacao(nr_seq_guia_p, 'A', 'IG', 
		nr_seq_prestador_w, null, '', 
		nm_usuario_p, cd_estabelecimento_p,null);
		
	if	(ie_carater_internacao_imp_w is not null) then
		if	(ie_carater_internacao_imp_w = '1') then
			ie_carater_internacao_imp_w := 'E';
		elsif	(ie_carater_internacao_imp_w = '2') then
			ie_carater_internacao_imp_w := 'U';
		end if;
	end if;
	
	/*if	(cd_cbo_saude_imp_w is not null) then
		select	max(nr_sequencia)
		into	nr_seq_cbo_saude_w
		from	cbo_saude a
		where	a.cd_cbo	=  cd_cbo_saude_imp_w
		and	((a.ie_situacao = 'A') or (a.ie_situacao is null));
	end if; */
	
	update	pls_guia_plano
	set	nr_seq_prestador	= decode(qt_prestador_valido_w,0, null, nr_seq_prestador_w),
		nr_seq_segurado		= nr_seq_segurado_w,
		nr_seq_plano		= nr_seq_plano_w,
		cd_medico_solicitante	= decode(cd_medico_w,0, null, cd_medico_w),
		nr_seq_clinica		= nr_seq_clinica_imp_w,
		ie_carater_internacao	= ie_carater_internacao_imp_w,
		ie_regime_internacao 	= ie_regime_internacao_imp,
		cd_guia_prestador	= cd_guia_imp,
		dt_emissao		= dt_emissao_imp,
		dt_solicitacao		= dt_solicitacao_imp,
		qt_dia_solicitado	= qt_dia_autorizado_imp,
		cd_guia_principal	= cd_guia_principal_imp,
		ds_observacao		= ds_observacao_imp,
		nr_seq_uni_exec		= nr_seq_cooperativa_w,
		cd_guia_manual		= cd_guia_manual_imp_w,
		ds_indicacao_clinica    = ds_indicacao_clinica_imp,
		nr_seq_cbo_saude	= nr_seq_cbo_saude_w
	where	nr_sequencia		= nr_seq_guia_p;
	
	commit;
	
	/* Rotina utilizada para consistir a autoriza��o ap�s confirmar os dados dos campos imp */
	pls_consistir_guia(nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p);	
	
	/* Rotina utlizada para deixar a Guia em an�lise com o status 'Aguardando anexo guia TISS'
	deve ser chamada depois de consistir a guia, assim ir� atualizar o status da an�lise e da autoriza��o */
	pls_gerar_analise_anexo_ws(nr_seq_guia_p, null, nm_usuario_p);	
	
	nr_guia_auditoria_w	:= pls_obter_se_guia_auditoria(nr_seq_guia_p);
	
	if	(nr_guia_auditoria_w = 0) then		
		pls_liberar_guia(nr_seq_guia_p, null, null, 'S', cd_estabelecimento_p, nm_usuario_p, null, null);	
	end if;
end if;

end pls_consistir_guia_importacao;
/