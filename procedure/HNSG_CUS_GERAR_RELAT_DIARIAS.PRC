create or replace
procedure hnsg_cus_gerar_relat_diarias(	cd_empresa_p		number,
					cd_estabelecimento_p	number,
					dt_inicial_p		date,
					dt_final_p			date,
					cd_centro_controle_p	number,
					nm_usuario_p		Varchar2) is 

cd_natureza_gasto_w			varchar2(255);
nr_sequencia_w				number(10);
ie_gerar_w				varchar2(1);

/*O CURSOR ABAIXO N�O ESTA ORDENADO POR CLASSIFICACAO DEVIDO SOLICITACAO DO CLIENTE*/
Cursor C01 is
select	10 					cd_classificacao,
	'Receita com Di�rias'			ds_classificacao
from	dual
union all
select	5 					cd_classificacao,
	'Receita com Di�rias'			
from	dual
union all
select	20 					cd_classificacao,
	'Receita Materiais Hospitalares' 
from	dual
union all
select	15					cd_classificacao,
	'Receita Materiais Hospitalares'
from	dual
union all
select	25 					cd_classificacao,
	'Receita Materiais Hospitalares'
from	dual
union all
select	35 					cd_classificacao,
	'Receita com Medicamentos'
from	dual
union all
select	30 					cd_classificacao,
	'Receita com Medicamentos'
from	dual
union all
select	40 					cd_classificacao,
	'Receita com Medicamentos'
from	dual;

vet01 c01%rowtype;

cursor c02 is
select	a.cd_centro_controle,
	substr(obter_desc_centro_controle(a.cd_centro_controle,cd_estabelecimento_p),1,255) ds_centro_controle,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),00),nvl(sum(a.vl_mes),0),0) vl_mes_1,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),01),nvl(sum(a.vl_mes),0),0) vl_mes_2,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),02),nvl(sum(a.vl_mes),0),0) vl_mes_3,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),03),nvl(sum(a.vl_mes),0),0) vl_mes_4,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),04),nvl(sum(a.vl_mes),0),0) vl_mes_5,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),05),nvl(sum(a.vl_mes),0),0) vl_mes_6,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),06),nvl(sum(a.vl_mes),0),0) vl_mes_7,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),07),nvl(sum(a.vl_mes),0),0) vl_mes_8,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),08),nvl(sum(a.vl_mes),0),0) vl_mes_9,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),09),nvl(sum(a.vl_mes),0),0) vl_mes_10,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),10),nvl(sum(a.vl_mes),0),0) vl_mes_11,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),11),nvl(sum(a.vl_mes),0),0) vl_mes_12,
	a.cd_natureza_gasto
from	natureza_gasto d,
	classif_result c,
	tabela_custo b,
	resultado_centro_controle a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.cd_tabela_custo	= b.cd_tabela_custo
and	a.ie_classif_conta	= c.cd_classificacao
and	a.nr_seq_ng		= d.nr_sequencia
and	d.cd_grupo_natureza_gasto	= 100
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	c.cd_empresa		= cd_empresa_p
and	a.cd_centro_controle	in(02,87,88,90,91,92,93,94,95)
and	a.cd_centro_controle	= nvl(cd_centro_controle_p,a.cd_centro_controle)
and	vet01.cd_classificacao	in(5,15,30)
and	(trunc(b.dt_mes_referencia,'mm') between trunc(dt_inicial_p,'mm') and trunc(dt_final_p,'mm'))
group by	a.cd_centro_controle, b.dt_mes_referencia,a.cd_natureza_gasto
union all
select	a.cd_centro_controle,
	substr(obter_desc_centro_controle(a.cd_centro_controle,cd_estabelecimento_p),1,255) ds_centro_controle,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),00),nvl(sum(a.vl_mes),0),0) vl_mes_1,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),01),nvl(sum(a.vl_mes),0),0) vl_mes_2,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),02),nvl(sum(a.vl_mes),0),0) vl_mes_3,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),03),nvl(sum(a.vl_mes),0),0) vl_mes_4,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),04),nvl(sum(a.vl_mes),0),0) vl_mes_5,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),05),nvl(sum(a.vl_mes),0),0) vl_mes_6,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),06),nvl(sum(a.vl_mes),0),0) vl_mes_7,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),07),nvl(sum(a.vl_mes),0),0) vl_mes_8,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),08),nvl(sum(a.vl_mes),0),0) vl_mes_9,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),09),nvl(sum(a.vl_mes),0),0) vl_mes_10,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),10),nvl(sum(a.vl_mes),0),0) vl_mes_11,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),11),nvl(sum(a.vl_mes),0),0) vl_mes_12,
	a.cd_natureza_gasto
from	natureza_gasto d,
	classif_result c,
	tabela_custo b,
	resultado_centro_controle a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.cd_tabela_custo	= b.cd_tabela_custo
and	a.ie_classif_conta	= c.cd_classificacao
and	a.nr_seq_ng		= d.nr_sequencia
and	d.cd_grupo_natureza_gasto	= 100
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	c.cd_empresa		= cd_empresa_p
and	a.cd_centro_controle	in(97,98,99)
and	a.cd_centro_controle	= nvl(cd_centro_controle_p,a.cd_centro_controle)
and	vet01.cd_classificacao	in(10,20,35)
and	(trunc(b.dt_mes_referencia,'mm') between trunc(dt_inicial_p,'mm') and trunc(dt_final_p,'mm'))
group by	a.cd_centro_controle,b.dt_mes_referencia,a.cd_natureza_gasto
union all
select	a.cd_centro_controle,
	substr(obter_desc_centro_controle(a.cd_centro_controle,cd_estabelecimento_p),1,255) ds_centro_controle,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),00),nvl(sum(a.vl_mes),0),0) vl_mes_1,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),01),nvl(sum(a.vl_mes),0),0) vl_mes_2,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),02),nvl(sum(a.vl_mes),0),0) vl_mes_3,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),03),nvl(sum(a.vl_mes),0),0) vl_mes_4,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),04),nvl(sum(a.vl_mes),0),0) vl_mes_5,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),05),nvl(sum(a.vl_mes),0),0) vl_mes_6,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),06),nvl(sum(a.vl_mes),0),0) vl_mes_7,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),07),nvl(sum(a.vl_mes),0),0) vl_mes_8,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),08),nvl(sum(a.vl_mes),0),0) vl_mes_9,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),09),nvl(sum(a.vl_mes),0),0) vl_mes_10,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),10),nvl(sum(a.vl_mes),0),0) vl_mes_11,
	decode(trunc(b.dt_mes_referencia,'mm'), add_months(trunc(dt_inicial_p,'yy'),11),nvl(sum(a.vl_mes),0),0) vl_mes_12,
	a.cd_natureza_gasto
from	natureza_gasto d,
	classif_result c,
	tabela_custo b,
	resultado_centro_controle a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.cd_tabela_custo	= b.cd_tabela_custo
and	a.ie_classif_conta	= c.cd_classificacao
and	a.nr_seq_ng		= d.nr_sequencia
and	d.cd_grupo_natureza_gasto	= 100
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	c.cd_empresa		= cd_empresa_p
and	a.cd_centro_controle	in(23,24,81,84,202,205)
and	a.cd_centro_controle	= nvl(cd_centro_controle_p,a.cd_centro_controle)
and	vet01.cd_classificacao	in(25,40)
and	(trunc(b.dt_mes_referencia,'mm') between trunc(dt_inicial_p,'mm') and trunc(dt_final_p,'mm'))
group by	a.cd_centro_controle,b.dt_mes_referencia,a.cd_natureza_gasto;

vet02	c02%rowtype;

begin

delete	from w_hncg_custo_diarias
where	nm_usuario	= nm_usuario_p;
commit;

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	
	if	(vet01.cd_classificacao = 5) then
		
		cd_natureza_gasto_w	:= '234,235,317,318';
		
	elsif	(vet01.cd_classificacao = 10) then
	
		cd_natureza_gasto_w	:= '236,319';
		
	elsif	(vet01.cd_classificacao in(15,20,25)) then
	
		cd_natureza_gasto_w	:= '244,245,327,328';
		
	elsif	(vet01.cd_classificacao in(30,35,40)) then
		
		cd_natureza_gasto_w	:= '246,247,329,330';
	end if;
	
	open C02;
	loop
	fetch C02 into	
		vet02;
	exit when C02%notfound;
		begin
		
		ie_gerar_w	:= 'N';		
		ie_gerar_w	:= substr(ctb_obter_se_elemento_contido(vet02.cd_natureza_gasto, cd_natureza_gasto_w),1,1);
		
		if	(ie_gerar_w	= 'S') then
		
			insert into w_hncg_custo_diarias (	
				nr_sequencia,
				cd_classificacao,
				ds_classificacao,
				cd_centro_controle,
				ds_centro_controle,
				vl_mes_01,
				vl_mes_02,
				vl_mes_03,
				vl_mes_04,
				vl_mes_05,
				vl_mes_06,
				vl_mes_07,
				vl_mes_08,
				vl_mes_09,
				vl_mes_10,
				vl_mes_11,
				vl_mes_12,
				nm_usuario)
			values(	w_hncg_custo_diarias_seq.nextval,
				vet01.cd_classificacao,
				vet01.ds_classificacao,
				vet02.cd_centro_controle,
				vet02.ds_centro_controle,
				vet02.vl_mes_1,
				vet02.vl_mes_2,
				vet02.vl_mes_3,
				vet02.vl_mes_4,
				vet02.vl_mes_5,
				vet02.vl_mes_6,
				vet02.vl_mes_7,
				vet02.vl_mes_8,
				vet02.vl_mes_9,
				vet02.vl_mes_10,
				vet02.vl_mes_11,
				vet02.vl_mes_12,
				nm_usuario_p);
				
		end if;
		end;
	end loop;
	close C02;
	cd_natureza_gasto_w	:= '';
	end;
end loop;
close C01;

commit;

end hnsg_cus_gerar_relat_diarias;
/