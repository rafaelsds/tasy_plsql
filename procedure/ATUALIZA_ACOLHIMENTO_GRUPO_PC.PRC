create or replace
procedure atualiza_acolhimento_grupo_pc(
	nr_seq_agenda_int_p	number,
	ie_opcao_p	varchar2,
	cd_profissional_p	varchar2,
	nm_usuario_p	varchar2) is 

nr_atendimento_w	number(10);

Cursor C01 is
	select	a.nr_atendimento
	from	agenda_consulta a
	where	a.nr_seq_agend_coletiva in (	select	b.nr_sequencia
					from	agendamento_coletivo b
					where	b.nr_seq_agenda_int = nr_seq_agenda_int_p);

begin

open C01;
loop
fetch C01 into	
	nr_atendimento_w;
exit when C01%notfound;
	begin
	atualiza_acolhimento_pc(nr_atendimento_w,ie_opcao_p,cd_profissional_p,nm_usuario_p);
	end;
end loop;
close C01;

commit;

end atualiza_acolhimento_grupo_pc;
/