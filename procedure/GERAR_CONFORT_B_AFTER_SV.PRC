create or replace PROCEDURE GERAR_CONFORT_B_AFTER_SV( nr_seq_sv_p number,
													  nr_atendimento_p number,
													  cd_profissional_p number,
													  qt_pontuacao_p number,  
													  ie_conciencia_p number,
													  ie_agitacao_p number,
													  ie_ventilacao_p number,
													  ie_choro_p number,
													  ie_fisico_p number,
													  ie_tonus_p number,
													  ie_tensao_p number,
													  ie_liberacao_p varchar2  default 'N',
													  nr_seq_analgesia_p number default null ) is

 nr_sequencia_w 	 number(10) := null;
 dt_liberacao_sv_w 	 date := null;
 count_confort_b_w 	 number(1);
 qt_cftb_analgesia_w number(1);
 dt_liberacao_w 	 date := null;
 
begin  
  select count(*)
  into count_confort_b_w
  from escala_comfort_b
  where nr_seq_sv = nr_seq_sv_p;
  
  select count(1)
  into   qt_cftb_analgesia_w
  from escala_comfort_b
  where nr_seq_analgesia = nr_seq_analgesia_p;
  
  if (ie_liberacao_p = 'S') then
	dt_liberacao_w := sysdate;
  end if;
  
  if count_confort_b_w > 0 then
	begin
    select dt_liberacao
    into dt_liberacao_sv_w
    from atendimento_sinal_vital
    where nr_sequencia = nr_seq_sv_p;
    
    if (dt_liberacao_sv_w is null) then
      update ESCALA_COMFORT_B
      set 
        dt_atualizacao = sysdate,
        ie_conciencia = ie_conciencia_p,  
        ie_ventilacao = ie_ventilacao_p,     
        ie_choro = ie_choro_p,     
        ie_tonus = ie_tonus_p,   
        ie_tensao = ie_tensao_p,   
        qt_pontuacao = qt_pontuacao_p, 
        ie_agitacao = ie_agitacao_p, 
        ie_fisico = ie_fisico  
      where nr_seq_sv = nr_seq_sv_p;      
    end if;
	end;
  elsif (qt_cftb_analgesia_w > 0) then
	begin
	select dt_liberacao
    into dt_liberacao_sv_w
    from atend_aval_analgesia
    where nr_sequencia = nr_seq_analgesia_p;
    
    if (dt_liberacao_sv_w is null) then
      update escala_comfort_b
      set 
        dt_atualizacao = sysdate,
        ie_conciencia = ie_conciencia_p,  
        ie_ventilacao = ie_ventilacao_p,     
        ie_choro = ie_choro_p,     
        ie_tonus = ie_tonus_p,   
        ie_tensao = ie_tensao_p,   
        qt_pontuacao = qt_pontuacao_p, 
        ie_agitacao = ie_agitacao_p, 
        ie_fisico = ie_fisico  
      where nr_seq_analgesia = nr_seq_analgesia_p;      
    end if;
	end;
  else    
	begin
    select nvl(max(nr_sequencia),0)+1
    into nr_sequencia_w
    from escala_comfort_b;
    
    insert into escala_comfort_b(
        nr_sequencia,    
        dt_atualizacao,   
        dt_atualizacao_nrec,               
        nm_usuario, 
        nm_usuario_nrec, 
        nr_atendimento,    
        ie_situacao,   
        dt_avaliacao,
		dt_liberacao,
        cd_profissional,  
        ie_conciencia,   
        ie_ventilacao,     
        ie_choro,     
        ie_tonus,   
        ie_tensao,   
        qt_pontuacao, 
        ie_agitacao, 
        ie_fisico,
        nr_seq_sv,
		nr_seq_analgesia
      ) values (
        nr_sequencia_w,   
        sysdate,        
        sysdate,   
        wheb_usuario_pck.get_nm_usuario, 
        wheb_usuario_pck.get_nm_usuario, 
        nr_atendimento_p,
        'A',  
        sysdate,
		dt_liberacao_w,
        cd_profissional_p,
        ie_conciencia_p,      
        ie_ventilacao_p,      
        ie_choro_p,     
        ie_tonus_p, 
        ie_tensao_p,     
        qt_pontuacao_p,
        ie_agitacao_p,    
        ie_fisico_p,
        nr_seq_sv_p,
		nr_seq_analgesia_p
      );  
  end;
  end if;
  
  commit;

end gerar_confort_b_after_sv;
/
