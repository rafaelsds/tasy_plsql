create or replace
procedure desfazer_hist_audit_retorno(
			nr_seq_retorno_p	number,
			nm_usuario_p		varchar2) is

nr_seq_ret_item_w		number(10);
nr_seq_conpaci_ret_hist_w	number(10);

Cursor c01 is
	select	distinct a.nr_seq_ret_item
	from	motivo_glosa c,
		convenio_retorno_item b,
		convenio_retorno_glosa a
	where	a.nr_seq_ret_item	= b.nr_sequencia
	and	a.cd_motivo_glosa	= c.cd_motivo_glosa
	and	nvl(a.ie_acao_glosa,c.ie_acao_glosa) = 'R'
	and	b.nr_seq_retorno	= nr_seq_retorno_p;

Cursor c02 is
	select	a.nr_seq_conpaci_ret_hist
	from	motivo_glosa b,
		convenio_retorno_glosa a
	where	a.cd_motivo_glosa	= b.cd_motivo_glosa
	and	nvl(a.ie_acao_glosa,b.ie_acao_glosa) = 'R'
	and	a.nr_seq_conpaci_ret_hist is not null
	and	a.nr_seq_ret_item	= nr_seq_ret_item_w; 

begin

open c01;
loop
fetch c01 into
	nr_seq_ret_item_w;
exit when c01%notfound;
	
	open c02;
	loop
	fetch c02 into
		nr_seq_conpaci_ret_hist_w;
	exit when c02%notfound;
		desfazer_historico_auditoria(nr_seq_conpaci_ret_hist_w, nm_usuario_p);
	end loop;
	close c02;
	
end loop;
close c01;

end desfazer_hist_audit_retorno;
/