create or replace
procedure hd_gerar_profissional_rotina (	nm_usuario_p		varchar2,
														cd_pessoa_fisica_p		varchar2,
														ie_tipo_profissional_p		varchar2,
														cd_medico_resp_p				varchar2,
														control_p					varchar2,
														cd_medico_update_p			varchar2 default null,
														cd_estabelecimento_p		number,
														ie_novo_registro_p			varchar2	default null) is
				

/*
control_p:
I - Insert new record
U - Update record
*/

ie_possui_prof_w	varchar2(1);
ie_possui_cad_pac_w	varchar2(1);

begin

if	(ie_novo_registro_p = 'S') and
		(control_p = 'I') then
		
			insert into hd_profissional_resp (nr_sequencia,
												cd_estabelecimento,
												dt_atualizacao,
												nm_usuario,
												dt_atualizacao_nrec,
												nm_usuario_nrec,
												cd_profissional,
												dt_inicio,
												cd_pessoa_fisica,
												ie_tipo_profissional
												)values(hd_profissional_resp_seq.nextval,
												cd_estabelecimento_p,
												sysdate,
												nm_usuario_p,
												sysdate,
												nm_usuario_p,
												cd_medico_resp_p,
												sysdate,
												cd_pessoa_fisica_p,
												ie_tipo_profissional_p);

end if;

if	(ie_novo_registro_p is null) then
	
	select	decode(count(*),0,'N','S')
	into	ie_possui_cad_pac_w
	from	hd_pac_renal_cronico
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	select	decode(count(*),0,'N','S')
	into	ie_possui_prof_w
	from	hd_profissional_resp
	where	cd_profissional = cd_medico_update_p
	and		cd_pessoa_fisica = cd_pessoa_fisica_p
	and		dt_fim is null;

	if	(control_p = 'U') and
		(ie_possui_cad_pac_w = 'S') then

			if(ie_possui_prof_w = 'S') then
				update hd_profissional_resp 
				set dt_fim = sysdate
				where (cd_profissional = cd_medico_update_p or cd_medico_update_p is null)
				and cd_pessoa_fisica = cd_pessoa_fisica_p
				and	dt_fim is null
				and	ie_tipo_profissional = ie_tipo_profissional_p;
			
			end if;
		
			insert into hd_profissional_resp (nr_sequencia,
												cd_estabelecimento,
												dt_atualizacao,
												nm_usuario,
												dt_atualizacao_nrec,
												nm_usuario_nrec,
												cd_profissional,
												dt_inicio,
												cd_pessoa_fisica,
												ie_tipo_profissional
												)values(hd_profissional_resp_seq.nextval,
												cd_estabelecimento_p,
												sysdate,
												nm_usuario_p,
												sysdate,
												nm_usuario_p,
												cd_medico_resp_p,
												sysdate,
												cd_pessoa_fisica_p,
												ie_tipo_profissional_p);
	end if;
end if;
end hd_gerar_profissional_rotina;
/
