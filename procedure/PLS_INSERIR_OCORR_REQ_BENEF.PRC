create or replace
procedure pls_inserir_ocorr_req_benef
			(	nr_seq_ocorrencia_p	Number,
				nr_seq_regra_p		Number,
				nr_seq_requisicao_p	Number,
				nr_seq_req_proc_p	Number,
				nr_seq_req_mat_p	Number,
				nr_seq_segurado_p	Number,
				nr_seq_auditoria_p	Number,
				nm_usuario_p		Varchar2, 
				cd_estabelecimento_p	Number) is 

ds_documentacao_w		Varchar2(4000);

ie_finalizar_analise_w		varchar2(1);

nr_seq_grupo_w			Number(10);
nr_seq_fluxo_w			Number(10);
nr_seq_ordem_atual_w		Number(10);
nr_seq_auditoria_w		Number(10)	:= 0;

qt_ocorr_proc_w			Number(10)	:= 0;
qt_ocorr_mat_w			Number(10)	:= 0;
qt_ocorr_w			Number(10)	:= 0;
qt_grupo_w			Number(10);
qt_fluxo_w			Number(10);

nr_seq_ocorr_benef_w		number(10);
nr_nivel_liberacao_w		number(2);
ie_utiliza_nivel_w		varchar2(1);
cd_ocorrencia_w			pls_ocorrencia.cd_ocorrencia%type;


Cursor C01 is
	select	nr_seq_grupo,
		nr_seq_fluxo
	from	pls_ocorrencia_grupo
	where	nr_seq_ocorrencia	= nr_seq_ocorrencia_p
	and	ie_requisicao		= 'S'
	and	ie_situacao		= 'A'
	group by nr_seq_fluxo, nr_seq_grupo
	order by nr_seq_fluxo;

begin

select	nvl(max(a.ie_finalizar_analise),'S')
into	ie_finalizar_analise_w
from	pls_ocorrencia	a
where	a.nr_sequencia	= nr_seq_ocorrencia_p;

if	(nr_seq_req_proc_p is not null) then
	select	count(*)
	into	qt_ocorr_proc_w
	from	pls_ocorrencia_benef
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	nr_seq_proc		= nr_seq_req_proc_p
	and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;
elsif	(nr_seq_req_mat_p	is not null) then
	select	count(*)
	into	qt_ocorr_mat_w
	from	pls_ocorrencia_benef
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	nr_seq_mat		= nr_seq_req_mat_p
	and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p;
elsif	(nr_seq_req_proc_p is null) and
	(nr_seq_req_mat_p is null) then
	select	count(*)
	into	qt_ocorr_w
	from	pls_ocorrencia_benef
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	nr_seq_ocorrencia	= nr_seq_ocorrencia_p
	and	nr_seq_proc		is null
	and	nr_seq_mat		is null;
end if;

if	(qt_ocorr_proc_w = 0) and
	(qt_ocorr_mat_w = 0) and
	(qt_ocorr_w = 0) then	
	
	select	pls_ocorrencia_benef_seq.nextval
	into	nr_seq_ocorr_benef_w
	from	dual;
	
	insert	into pls_ocorrencia_benef
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ds_observacao,
		ie_auditoria,
		ie_tipo_glosa, 
		nr_seq_ocorrencia,
		nr_seq_regra,
		nr_seq_requisicao, 
		nr_seq_segurado,
		nr_seq_proc,
		nr_seq_mat,
		ie_finalizar_analise)
	values	(nr_seq_ocorr_benef_w,
		sysdate,
		nm_usuario_p,
		ds_documentacao_w,
		'S',
		'A', 
		nr_seq_ocorrencia_p,
		nr_seq_regra_p,
		nr_seq_requisicao_p, 
		nr_seq_segurado_p,
		nr_seq_req_proc_p,
		nr_seq_req_mat_p,
		ie_finalizar_analise_w);
	
	begin
		select	ie_utiliza_nivel
		into	ie_utiliza_nivel_w
		from	pls_param_analise_aut
		where	cd_estabelecimento	= cd_estabelecimento_p;
	exception
	when others then
		ie_utiliza_nivel_w	:= 'N';
	end;	
	
	if (ie_utiliza_nivel_w = 'S') then

		begin
			select	a.nr_nivel_liberacao
			into	nr_nivel_liberacao_w
			from	pls_nivel_liberacao a,
				pls_ocorrencia b
			where	b.nr_sequencia = nr_seq_ocorrencia_p
			and	a.nr_sequencia = b.nr_seq_nivel_lib;
		exception
		when others then
			nr_nivel_liberacao_w := null;
		end;
		
		if (nr_nivel_liberacao_w is null) then
			select	cd_ocorrencia
			into	cd_ocorrencia_w
			from	pls_ocorrencia
			where nr_sequencia = nr_seq_ocorrencia_p;
			wheb_mensagem_pck.exibir_mensagem_abort('Nivel de libera��o n�o informado no cadastro da ocorr�ncia '||cd_ocorrencia_w||'.');
		end if;
		insert into pls_analise_ocor_glosa_aut
			(nr_sequencia, ie_status, dt_atualizacao,
			nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_auditoria, nr_seq_aud_item, nr_seq_glosa,
			nr_seq_ocorrencia_benef, ie_tipo, nr_nivel_liberacao,
			nr_seq_ocorrencia)
		values	(pls_analise_ocor_glosa_aut_seq.nextval, 'P', sysdate,
			nm_usuario_p, sysdate, nm_usuario_p,
			nr_seq_auditoria_p, null, null,
			nr_seq_ocorr_benef_w, 'O', nr_nivel_liberacao_w,
			nr_seq_ocorrencia_p);
	end if;
	
		
	
	if	(nr_seq_req_proc_p is null) and
		(nr_seq_req_mat_p is null) then
		update	pls_requisicao
		set	ie_estagio	= 4,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_requisicao_p
		and	ie_estagio	<> 4;
		
		update	pls_requisicao_proc
		set	ie_status		= 'A',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	ie_status		= 'N';
		
		update	pls_requisicao_mat
		set	ie_status		= 'A',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_requisicao	= nr_seq_requisicao_p
		and	ie_status		= 'N';
	elsif	(nr_seq_req_proc_p is not null) then
		update	pls_requisicao
		set	ie_estagio	= 4,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_requisicao_p
		and	ie_estagio	<> 4;
		
		update	pls_requisicao_proc
		set	ie_status		= 'A',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_req_proc_p
		and	ie_status		not in ('A','S');
	elsif	(nr_seq_req_mat_p is not null) then
		update	pls_requisicao
		set	ie_estagio	= 4,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_requisicao_p
		and	ie_estagio	<> 4;

		update	pls_requisicao_mat
		set	ie_status		= 'A',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_req_mat_p
		and	ie_status		not in ('A','S');
	end if;
	
	if	(nr_seq_auditoria_p is not null) then
		nr_seq_auditoria_w	:= nr_seq_auditoria_p;
	else
		begin
		select	nr_sequencia
		into	nr_seq_auditoria_w
		from	pls_auditoria
		where	nr_seq_requisicao 	= nr_seq_requisicao_p
		and	dt_liberacao		is null;
		exception
		when others then
			nr_seq_auditoria_w	:= 0;	
		end;
	end if;
	
	if	(nr_seq_auditoria_w <> 0) then	
		open C01;
		loop
		fetch C01 into	
			nr_seq_grupo_w,
			nr_seq_fluxo_w;
		exit when C01%notfound;
						
			begin
			select	count(*)
			into	qt_grupo_w
			from	pls_auditoria_grupo
			where	nr_seq_auditoria 	= nr_seq_auditoria_w
			and	nr_seq_grupo 		= nr_seq_grupo_w;
			
			if	(qt_grupo_w = 0) then
				
				select	count(*)
				into	qt_fluxo_w
				from	pls_auditoria_grupo
				where	nr_seq_auditoria 	= nr_seq_auditoria_w
				and	nr_seq_ordem 		= nr_seq_fluxo_w;
				
				if	(qt_fluxo_w > 0) then
					select	max(nr_seq_ordem) + 1
					into	nr_seq_ordem_atual_w
					from	pls_auditoria_grupo
					where	nr_seq_auditoria 	= nr_seq_auditoria_w;
					
					nr_seq_fluxo_w	:= nr_seq_ordem_atual_w;
				end if;
				
				insert into pls_auditoria_grupo
					(nr_sequencia, nr_seq_auditoria, nr_seq_grupo,
					dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, nr_seq_ordem, ie_status,
					ie_manual)
				values	(pls_auditoria_grupo_seq.nextval, nr_seq_auditoria_w, nr_seq_grupo_w,
					sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, nr_seq_fluxo_w, 'U',
					'N');
			end if;
			end;
		end loop;
		close C01;
	elsif	(nr_seq_auditoria_w = 0) then		
		pls_gerar_auditoria_requisicao(nr_seq_requisicao_p, nm_usuario_p, 'A');
	end if;
	
	pls_requisicao_gravar_hist(nr_seq_requisicao_p, 'L', 'A requisi��o foi enviada para an�lise pois existe liminar para o benefici�rio.', null, nm_usuario_p);
end if;

commit;

end pls_inserir_ocorr_req_benef;
/