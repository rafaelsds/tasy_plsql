create or replace
procedure gerar_braden_sae(	nr_sequencia_p		number,
						nm_usuario_p		Varchar2) is 

cd_Pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);
ie_item_braden_w		varchar2(3) := null;
ie_braden_w			varchar2(3);
nr_seq_result_w			number(10);
ie_percepcao_sensorial_w	varchar2(10);
ie_umidade_w			varchar2(10);
ie_atividade_fisica_w		varchar2(10);
ie_mobilidade_w			varchar2(10);
ie_nutricao_w			varchar2(10);
ie_friccao_cisalhamento_w	varchar2(10);
qt_reg_w			number(10);
cd_profissional_w		varchar2(10);


	function obterValorSAEBraden(ie_braden_p	varchar2) return varchar2 is
	
	ie_retorno_w	varchar2(10);
	
	begin
	select	nvl(max(f.ie_item_braden),'4')
	into	ie_retorno_w
	from	pe_prescricao a,
		pe_prescr_item_result b,
		pe_item_examinar c,
		pe_item_tipo_item d,
		pe_tipo_item e,
		pe_item_resultado f
	where	a.nr_sequencia	= b.nr_seq_prescr
	and	a.nr_sequencia	= nr_sequencia_p
	and	b.nr_seq_item	= c.nr_sequencia
	and	d.nr_seq_item	= c.nr_sequencia
	and	e.nr_sequencia	= d.nr_seq_tipo_item
	and	c.ie_braden	= ie_braden_p
	and	f.nr_sequencia	= b.nr_seq_result;
	
	return ie_retorno_w;
	end;


begin
select	max(a.cd_prescritor),
	max(a.nr_atendimento)
into	cd_profissional_w,
	nr_atendimento_w
from	pe_prescricao a,
	pe_prescr_item_result b
where	a.nr_sequencia = b.nr_seq_prescr
and	a.nr_sequencia = nr_sequencia_p;


select	count(*)
into	qt_reg_w
from	pe_prescricao a,
	pe_prescr_item_result b,
	pe_item_examinar c,
	pe_item_tipo_item d,
	pe_tipo_item e,
	pe_item_resultado f
where	a.nr_sequencia	= b.nr_seq_prescr
and	a.nr_sequencia	= nr_sequencia_p
and	b.nr_seq_item	= c.nr_sequencia
and	d.nr_seq_item	= c.nr_sequencia
and	e.nr_sequencia	= d.nr_seq_tipo_item
and	c.ie_braden	is not null
and	f.nr_sequencia	= b.nr_seq_result;

if 	(qt_reg_w	> 0) then

	ie_percepcao_sensorial_w	:= obterValorSAEBraden('PS');
	ie_umidade_w			:= obterValorSAEBraden('UM');
	ie_atividade_fisica_w		:= obterValorSAEBraden('AF');
	ie_mobilidade_w			:= obterValorSAEBraden('MO');
	ie_nutricao_w			:= obterValorSAEBraden('NU');
	ie_friccao_cisalhamento_w	:= obterValorSAEBraden('FC');
	if	(ie_friccao_cisalhamento_w	= '4') then
		ie_friccao_cisalhamento_w	:= '3';
	end if;

	insert into atend_escala_braden (	
					nr_sequencia,
					cd_pessoa_fisica,
					dt_atualizacao,
					dt_avaliacao,
					ie_atividade_fisica,
					ie_friccao_cisalhamento,
					ie_mobilidade,
					ie_nutricao,
					ie_percepcao_sensorial,
					ie_umidade,
					nm_usuario,
					nr_atendimento,
					IE_SITUACAO,
					dt_liberacao,
					cd_setor_atendimento)
		values	(		atend_escala_braden_SEQ.NEXTVAL,
					cd_profissional_w,
					sysdate,
					sysdate,
					ie_atividade_fisica_w,
					ie_friccao_cisalhamento_w,
					ie_mobilidade_w,
					ie_nutricao_w,
					ie_percepcao_sensorial_w,
					ie_umidade_w,
					nm_usuario_p,
					nr_atendimento_w,
					'A',
					sysdate,
					obter_setor_atendimento(nr_atendimento_w));
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_braden_sae;
/
