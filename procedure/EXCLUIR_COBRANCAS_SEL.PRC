create or replace
procedure excluir_cobrancas_sel(ds_lista_p	varchar2) is

begin

if	(ds_lista_p is not null) then

	delete
	from	cobranca
	where	ds_lista_p like '% ' || nr_sequencia || ' %';

	commit;

end if;

end excluir_cobrancas_sel;
/