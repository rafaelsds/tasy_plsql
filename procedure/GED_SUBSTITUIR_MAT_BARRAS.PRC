create or replace
PROCEDURE ged_substituir_mat_barras(
    CD_MATERIAL_ATUAL_P NUMBER,
    CD_MATERIAL_SUBST_P NUMBER,
    QT_MATERIAL_SUBST_P NUMBER,
    DS_MATERIAL_SUBST_P VARCHAR2,
    QT_MATERIAL_P       NUMBER DEFAULT 0,
    SL_MATERIAL_P       NUMBER DEFAULT 0,
    NR_ID_P             NUMBER )
IS
BEGIN
  IF ((NVL(nr_id_p,0) > 0) AND (NVL(cd_material_atual_p,0) > 0)) THEN
    IF(qt_material_p  > 0) THEN
      BEGIN
        UPDATE material_wbarras
        SET cd_material = cd_material_subst_p,
          qt_material   = qt_material_subst_p * qt_material_p ,
          ds_material   = ds_material_subst_p,
          sl_material   = qt_material_subst_p * sl_material_p
        WHERE nr_id     = nr_id_p
        AND cd_material = cd_material_atual_p;
      END;
    ELSE
      BEGIN
        UPDATE material_wbarras
        SET cd_material = cd_material_subst_p,
          qt_material   = qt_material_subst_p,
          ds_material   = ds_material_subst_p,
          sl_material   = qt_material_subst_p
        WHERE nr_id     = nr_id_p
        AND cd_material = cd_material_atual_p;
      END;
    END IF;
    COMMIT;
  END IF;
END ged_substituir_mat_barras;
/