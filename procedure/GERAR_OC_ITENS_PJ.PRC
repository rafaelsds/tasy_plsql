create or replace
procedure gerar_oc_itens_pj(
				cd_cnpj_p			varchar2,
				cd_comprador_p			varchar2,
				dt_entrega_p			date,
				cd_setor_atendimento_p		number,
				ie_frete_p				varchar2,
				cd_moeda_p			number,
				cd_local_estoque_p		number,
				cd_centro_custo_p			number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				nr_ordem_compra_p	out 	number) is

cd_centro_custo_w			number(08,0);
cd_local_estoque_w		number(4);
ie_tipo_conta_w			number(5,0)	:= 2;
nr_item_oci_w			number(05,0);
cd_unidade_medida_compra_w	varchar2(30);
cd_cond_pagto_w			number(10);
cd_material_w			number(6);
vl_item_w				number(17,4);
nr_ordem_compra_w		number(10,0);
cd_conta_contabil_w		varchar2(20);

cursor c01 is
select	cd_material,
	nvl(vl_item,0)
from	preco_pj
where	cd_cgc = cd_cnpj_p
and	cd_material is not null
and	((dt_vigencia is null) or (trunc(dt_vigencia,'dd') <= trunc(sysdate,'dd')))
and	((dt_vigencia_fim is null) or (trunc(dt_vigencia_fim,'dd') >= trunc(sysdate,'dd')));

begin

cd_centro_custo_w	:= cd_centro_custo_p;
if	(cd_centro_custo_w = 0) then
	cd_centro_custo_w := null;
end if;

cd_local_estoque_w	:= cd_local_estoque_p;
if	(cd_local_estoque_w = 0) then
	cd_local_estoque_w := null;
end if;

select	nvl(max(cd_cond_pagto),0)
into	cd_cond_pagto_w
from	pessoa_juridica_estab
where	cd_cgc = cd_cnpj_p
and	cd_estabelecimento = cd_estabelecimento_p;

if	(cd_cond_pagto_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(266012);
	--'Favor informar a condi��o de pagamento do cadastro da pessoa jur�dica.'
end if;

select	ordem_compra_seq.nextval
into	nr_ordem_compra_w
from	dual;

insert into ordem_compra (
	nr_ordem_compra,
	cd_estabelecimento,
	cd_cgc_fornecedor,
	cd_condicao_pagamento,
	cd_comprador,
	dt_ordem_compra,
	dt_atualizacao,
	nm_usuario,
	cd_moeda,
	ie_situacao,
	dt_inclusao,
	cd_pessoa_solicitante,
	ie_frete,
	vl_frete,
	pr_desconto,
	pr_desc_pgto_antec,
	pr_juros_negociado,
	dt_entrega,
	ie_aviso_chegada,
	ie_emite_obs,
	ie_urgente,
	cd_setor_atendimento,
	pr_desc_financeiro,
	vl_desconto,
	ie_somente_pagto,
	ie_tipo_ordem,
	cd_local_entrega)
values(	nr_ordem_compra_w,
	cd_estabelecimento_p,
	cd_cnpj_p,
	cd_cond_pagto_w,
	cd_comprador_p,
	sysdate,
	sysdate,
	nm_usuario_p,
	cd_moeda_p,
	'A',
	sysdate,
	obter_pessoa_fisica_usuario(nm_usuario_p,'C'),
	ie_frete_p,
	0,
	0,
	0,
	0,
	dt_entrega_p,
	'N',
	'N',
	'N',
	cd_setor_atendimento_p,
	0,
	0,
	'N',
	'C',
	cd_local_estoque_w);
	
open C01;
loop
fetch C01 into	
	cd_material_w,
	vl_item_w;
exit when C01%notfound;
	begin	
	
	select	cd_unidade_medida_compra
	into	cd_unidade_medida_compra_w
	from	material
	where	cd_material = cd_material_w;
	
	select	nvl(max(nr_item_oci),0) + 1
	into	nr_item_oci_w
	from	ordem_compra_item
	where	nr_ordem_compra = nr_ordem_compra_w;

	if	(cd_conta_contabil_w is null) then
		if	(cd_centro_custo_w is null) then
			ie_tipo_conta_w	:= 2;
		else
			ie_tipo_conta_w	:= 3;
		end if;

		define_conta_material(	cd_estabelecimento_p,
					cd_material_w,
					ie_tipo_conta_w,
					null, null, null, null, null, null, null,
					cd_local_estoque_w,
					Null, sysdate,
					cd_conta_contabil_w,
					cd_centro_custo_w,null);
	end if;	
	
	insert into ordem_compra_item(
		nr_ordem_compra,
		nr_item_oci,
		cd_material,
		cd_unidade_medida_compra,
		vl_unitario_material,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		cd_pessoa_solicitante,
		pr_descontos,
		cd_local_estoque,
		vl_item_liquido,
		cd_centro_custo,
		cd_conta_contabil,
		pr_desc_financ,
		vl_desconto,
		ie_geracao_solic,
		vl_total_item)
	values(	nr_ordem_compra_w,
		nr_item_oci_w,
		cd_material_w,
		cd_unidade_medida_compra_w,
		vl_item_w,
		1,
		sysdate,
		nm_usuario_p,
		'A',
		obter_pessoa_fisica_usuario(nm_usuario_p,'C'),
		0,
		cd_local_estoque_w,
		vl_item_w,
		cd_centro_custo_w,
		cd_conta_contabil_w,
		0,
		0,
		'U',
		round((1 * vl_item_w),4));

	insert into ordem_compra_item_entrega(
		nr_ordem_compra,
		nr_item_oci,
		dt_prevista_entrega,
		qt_prevista_entrega,
		dt_atualizacao,
		nm_usuario,
		nr_sequencia)
	values(	nr_ordem_compra_w,
		nr_item_oci_w,
		dt_entrega_p,
		1,
		sysdate,
		nm_usuario_p,
		ordem_compra_item_entrega_seq.nextval);
	end;
end loop;
close C01;

Gerar_Ordem_Compra_Venc(nr_ordem_compra_w, nm_usuario_p);

commit;

nr_ordem_compra_p	:= nr_ordem_compra_w;

end gerar_oc_itens_pj;
/
