create or replace
procedure rxt_gerar_prot_pep(	nr_seq_tratamento_p	number,
				nm_usuario_p		varchar2) is			

/* protocolo */
qt_dose_total_w			number(15);				
ie_necessita_bloco_w rxt_protocolo.ie_necessita_bloco%TYPE;
ie_necessita_qa_w rxt_protocolo.ie_necessita_qa%TYPE;
				
/* tratamento */
qt_duracao_trat_w		number(5,0);
QT_CHECK_FILM_W			number(5,0);
				
/* fases */
nr_seq_fase_protocolo_w		number(10,0);
nm_fase_w			varchar2(40);
nr_seq_decubito_w		number(10,0);
qt_dia_trat_w			number(5,0);
qt_dose_fase_w			number(15,4);
qt_dose_dia_w			number(15,4);
ie_final_semana_w		varchar2(1);

/* campos */
qt_dose_total_fase_w		number(15,0);
qt_dose_fracao_w		number(15,0);
nr_seq_campo_w			rxt_campo.nr_sequencia%type;
nr_seq_campo_protoc_w		rxt_campo_protocolo.nr_sequencia%type;
nr_seq_campo_braq_w		rxt_braq_campo.nr_sequencia%type;


/* acessórios do protocolo */
nr_seq_acessorio_prot_w		number(10,0);

/* acessórios do campo */
nr_seq_acess_prot_campo_w	number(10,0);

/* sequences */
nr_seq_fase_tratamento_w	number(10,0);
nr_seq_campo_fase_w		number(10,0);
nr_seq_acessorio_w		number(10,0);
nr_seq_acessorio_campo_w	number(10,0);
nr_seq_volume_protocolo_w	number(10,0);
nr_seq_volume_w			number(10,0);
ds_volume_w			varchar2(90);
nr_seq_volume_tratamento_w	number(10,0);
ie_tratamento_volume_w		varchar2(1);
qt_intervalo_w			number(5);
qt_duracao_trat_ww		number(5);
qt_dose_total_ww		number(15,4);
qt_dose_fase_ww			number(15,4);
qt_dose_campo_ww		number(15,4);

nr_seq_campo_trat_roentgen_w	number(10);
nm_campo_w			varchar2(40);
ie_tipo_trat_w			varchar2(5);
nr_seq_kvp_w			number(10);
nr_seq_ma_w			number(10);
nr_seq_aplicador_w		number(10);
qt_tamanho_x_w			number(5);
qt_tamanho_y_w			number(5);
nr_minuto_duracao_w		number(10);
nr_seq_campo_prot_w		number(10);
nr_seq_campo_prot_roentgen_w	number(10);
nr_seq_aplic_trat_w		number(10);
qt_dose_campo_w			number(15,4);
nr_segundo_duracao_w		number(10);
ie_braquiterapia_prot_w		varchar2(1);
nr_seq_aplic_prot_w		number(10);
nr_insercao_w			number(5);
qt_dose_total_aplic_prot_w	number(15,4);
qt_dose_insercao_w		number(15,4);
qt_insercao_semana_w		number(15,4);
nr_insercao_campo_w		number(5);
ds_resultado_w			varchar2(80);
qt_resultado_w			number(15,4);
nr_seq_braq_aplic_trat_w	number(10);

/*Dados do tratamento */
nr_seq_tumor_w			number(10,0);
nr_seq_protocolo_w		number(10,0);
dt_inicio_trat_w		date;

/* obter fases */
cursor c01 is
select	nr_sequencia,
	nm_fase,
	nr_seq_decubito,
	qt_dia_trat,
	qt_dose_fase,
	qt_dose_dia,
	ie_final_semana,
	nr_seq_volume_protocolo
from	rxt_fase_protocolo
where	nr_seq_protocolo = nr_seq_protocolo_w
and	nr_seq_volume_protocolo is null
and	ie_tratamento_volume_w = 'N'
order by
	nr_sequencia;
	
/* obter campos fases */
cursor c02 is
select	qt_dose_total,
	qt_dose_fracao,
	nr_seq_campo
from	rxt_campo_fase_prot
where	nr_seq_fase = nr_seq_fase_protocolo_w
order by
	nr_seq_apres;

/* obter acessórios do protocolo */
cursor c03 is
select	nr_seq_acessorio
from	rxt_protocolo_acessorio
where	nr_seq_protocolo = nr_seq_protocolo_w
and	(substr(rxt_obter_dados_acessorio(nr_seq_acessorio, 'T'),1,1) = 'I')
order by
	nr_sequencia;

/* obter acessórios do campo */
cursor c04 is
select	nr_seq_acessorio
from	rxt_protocolo_acessorio
where	nr_seq_protocolo	= nr_seq_protocolo_w
and	nr_seq_campo	= nr_seq_campo_protoc_w
and	(substr(rxt_obter_dados_acessorio(nr_seq_acessorio, 'T'),1,1) = 'C')
order by
	nr_sequencia;

CURSOR c05 IS
SELECT	nr_sequencia,
	nr_seq_volume,
	ds_volume
FROM	rxt_volume_protocolo
WHERE	nr_seq_protocolo	= nr_seq_protocolo_w
AND	ie_tratamento_volume_w = 'S'
ORDER BY
	nr_sequencia;

/* obter fases  do volume*/
CURSOR c06 IS
SELECT	nr_sequencia,
	nm_fase,
	nr_seq_decubito,
	qt_dia_trat,
	qt_dose_fase,
	qt_dose_dia,
	ie_final_semana,
	qt_intervalo
FROM	rxt_fase_protocolo
WHERE	nr_seq_protocolo 	= nr_seq_protocolo_w
AND	nr_seq_volume_protocolo = nr_seq_volume_protocolo_w
ORDER BY
	nr_sequencia;
	
	
cursor c07 is
select	nr_sequencia,
	nm_campo,
	qt_dia_trat,
	qt_dose_campo,
	qt_dose_dia,
	nr_seq_volume_protocolo
from	rxt_campo_prot_roentgen
where	nr_seq_protocolo = nr_seq_protocolo_w
and	nr_seq_volume_protocolo is null
and	ie_tratamento_volume_w = 'N'
order by
	nr_sequencia;
	
cursor c08 is
select	nr_seq_kvp,
	nr_seq_ma,
	nr_seq_aplicador,
	qt_tamanho_x,
	qt_tamanho_y,
	nr_minuto_duracao,
	nr_seq_campo_prot,
	nr_segundo_duracao
from	rxt_aplic_campo_prot_roent
where	nr_seq_campo_prot = nr_seq_campo_prot_roentgen_w;
	
cursor c09 is
select	nr_sequencia,
	nr_seq_volume,
	ds_volume
from	rxt_volume_protocolo
where	nr_seq_protocolo	= nr_seq_protocolo_w
and	ie_tratamento_volume_w = 'S'
order by
	nr_sequencia;
	
cursor c10 is
select	nr_sequencia,
	nm_campo,
	qt_dia_trat,
	qt_dose_campo,
	qt_dose_dia
from	rxt_campo_prot_roentgen
where	nr_seq_protocolo 	= nr_seq_protocolo_w
and	nr_seq_volume_protocolo = nr_seq_volume_protocolo_w
order by
	nr_sequencia;
	
cursor c11 is
select	nr_sequencia,
	nr_seq_aplicador,
	nr_insercao,
	qt_dose_total,
	qt_dose_insercao,
	qt_insercao_semana
from	rxt_braq_aplic_prot
where	nr_seq_protocolo = nr_seq_protocolo_w
order by
	nr_sequencia;
	
cursor c12 is
select	nr_insercao,
	ds_resultado,
	qt_resultado,
	nr_seq_campo
from	rxt_braq_campo_aplic_prot
where	nr_seq_aplic_prot = nr_seq_aplic_prot_w;
	
begin

ie_braquiterapia_prot_w := obter_valor_param_usuario(3030, 31, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);

SELECT 	NVL(max(ie_considera_tratamento_volume),'N')
INTO	ie_tratamento_volume_w
FROM 	rxt_parametro;

if	(nr_seq_tratamento_p is not null) then
	begin
	/* obter valores do tratamento */
	select	a.nr_seq_tumor,
		nr_seq_protocolo,
		a.dt_inicio_trat
	into	nr_seq_tumor_w,
		nr_seq_protocolo_w,
		dt_inicio_trat_w
	from	rxt_tratamento a
	where	a.nr_sequencia = nr_seq_tratamento_p;
	
	select	nvl(max(rxt_obter_tipo_trat_prot(nr_seq_protocolo_w)),'X')
	into	ie_tipo_trat_w
	from	dual;
	
	if	(ie_tipo_trat_w <> 'R') and ((ie_tipo_trat_w <> 'B') or (ie_braquiterapia_prot_w = 'N')) then
		/* obter valores protocolo */
		select	max(qt_dose_total),
			MAX(QT_CHECK_FILM),
			MAX(ie_necessita_bloco),
			MAX(ie_necessita_qa)
		into	qt_dose_total_w,
			QT_CHECK_FILM_W,
			ie_necessita_bloco_w,
			ie_necessita_qa_w
		from	rxt_protocolo
		where	nr_sequencia = nr_seq_protocolo_w;
		
		/* ajustar a dose total do tratamento conforme o protocolo */
		update	rxt_tratamento
		set	qt_dose_total	= qt_dose_total_w,
			QT_CHECK_FILM	= QT_CHECK_FILM_W,
			ie_necessita_bloco = ie_necessita_bloco_w,
			ie_necessita_qa = ie_necessita_qa_w
		where	nr_sequencia	= nr_seq_tratamento_p;	
		
		OPEN c05;
		LOOP
		FETCH c05 INTO
			nr_seq_volume_protocolo_w,
			nr_seq_volume_w,
			ds_volume_w;
		EXIT WHEN c05%NOTFOUND;
			BEGIN

			SELECT	SUM(qt_dia_trat),
				SUM(qt_dose_fase),
				SUM(qt_dose_dia)
			INTO	qt_duracao_trat_ww,
				qt_dose_total_ww,
				qt_dose_fase_ww
			FROM 	rxt_fase_protocolo
			WHERE	nr_seq_volume_protocolo = nr_seq_volume_protocolo_w;

			SELECT	rxt_volume_tratamento_seq.NEXTVAL
			INTO	nr_seq_volume_tratamento_w
			FROM	dual;


			INSERT	INTO rxt_volume_tratamento(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_tratamento,
				nr_seq_volume_protocolo,
				nr_seq_volume,
				ds_volume,
				qt_duracao_trat,
				qt_dose_total,
				qt_dose_fase)
			VALUES	(nr_seq_volume_tratamento_w,
				SYSDATE,
				nm_usuario_p,
				SYSDATE,
				nm_usuario_p,
				nr_seq_tratamento_p,
				nr_seq_volume_protocolo_w,
				nr_seq_volume_w,
				ds_volume_w,
				qt_duracao_trat_ww,
				qt_dose_total_ww,
				qt_dose_fase_ww);

			/* obter fases  do volume*/
			OPEN c06;
			LOOP
			FETCH c06 INTO	nr_seq_fase_protocolo_w,
					nm_fase_w,
					nr_seq_decubito_w,
					qt_dia_trat_w,
					qt_dose_fase_w,
					qt_dose_dia_w,
					ie_final_semana_w,
					qt_intervalo_w;
			EXIT WHEN c06%NOTFOUND;
				BEGIN

				/* obter sequence */
				SELECT	rxt_fase_tratamento_seq.NEXTVAL
				INTO	nr_seq_fase_tratamento_w
				FROM	dual;

				/* gerar fases */
				INSERT INTO rxt_fase_tratamento (
					nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_seq_tratamento,
					nm_fase,
					qt_duracao_trat,
					qt_dose_total,
					qt_dose_fase,
					ie_final_semana,
					nr_seq_decubito,
					nr_seq_fase_prot,
					nr_seq_volume_tratamento,
					qt_intervalo
					)
				VALUES (
					nr_seq_fase_tratamento_w,
					SYSDATE,
					nm_usuario_p,
					SYSDATE,
					nm_usuario_p,
					nr_seq_tratamento_p,
					nm_fase_w,
					qt_dia_trat_w,
					qt_dose_fase_w,
					qt_dose_dia_w,
					ie_final_semana_w,
					nr_seq_decubito_w,
					nr_seq_fase_protocolo_w,
					nr_seq_volume_tratamento_w,
					qt_intervalo_w);


				/* obter campos fase */
				OPEN c02;
				LOOP
				FETCH c02 INTO	qt_dose_total_fase_w,
						qt_dose_fracao_w,
						nr_seq_campo_protoc_w;
				EXIT WHEN c02%NOTFOUND;
					BEGIN

					/* obter sequence */
					SELECT	rxt_campo_seq.NEXTVAL
					INTO	nr_seq_campo_w
					FROM	dual;

					/* gerar campos fase */
					INSERT INTO rxt_campo (
						nr_sequencia,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_atualizacao,
						nm_usuario,
						nr_seq_fase,
						nr_seq_campo,
						nr_aplicacoes,
						qt_dose_total,
						qt_dose_diaria,
						ie_situacao)
					VALUES (
						nr_seq_campo_w,
						SYSDATE,
						nm_usuario_p,
						SYSDATE,
						nm_usuario_p,
						nr_seq_fase_tratamento_w,
						nr_seq_campo_protoc_w,
						qt_dia_trat_w,
						qt_dose_total_fase_w,
						qt_dose_fracao_w,
						'A');

					/* obter acessorios dos campos */
					OPEN c04;
					LOOP
					FETCH c04 INTO nr_seq_acess_prot_campo_w;
					EXIT WHEN c04%NOTFOUND;
						BEGIN

						/* obter sequence */
						SELECT	rxt_acessorio_pac_seq.NEXTVAL
						INTO	nr_seq_acessorio_campo_w
						FROM	dual;

						/* gerar acessórios do campo */
						INSERT INTO rxt_acessorio_pac (
							nr_sequencia,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							dt_atualizacao,
							nm_usuario,
							nr_seq_tratamento,
							nr_seq_campo,
							nr_seq_acessorio)
						VALUES (
							nr_seq_acessorio_campo_w,
							SYSDATE,
							nm_usuario_p,
							SYSDATE,
							nm_usuario_p,
							nr_seq_tratamento_p,
							nr_seq_campo_w,
							nr_seq_acess_prot_campo_w);
						END;
					END LOOP;
					CLOSE c04;
					END;
				END LOOP;
				CLOSE c02;
				END;
			END LOOP;
			CLOSE c06;
		END;
		END LOOP;
		CLOSE c05;
		
		/* obter fases */
		open c01;
		loop
		fetch c01 into	nr_seq_fase_protocolo_w,
				nm_fase_w,
				nr_seq_decubito_w,
				qt_dia_trat_w,
				qt_dose_fase_w,
				qt_dose_dia_w,
				ie_final_semana_w,
				nr_seq_volume_protocolo_w;
		exit when c01%notfound;
			begin
			
			/* obter sequence */
			select	rxt_fase_tratamento_seq.nextval
			into	nr_seq_fase_tratamento_w
			from	dual;
			
			/* gerar fases */
			insert into rxt_fase_tratamento (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tratamento,
				nm_fase,
				qt_duracao_trat,
				qt_dose_total,
				qt_dose_fase,
				ie_final_semana,
				nr_seq_decubito,
				nr_seq_fase_prot)
			values (
				nr_seq_fase_tratamento_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_tratamento_p,
				nm_fase_w,
				qt_dia_trat_w,
				qt_dose_fase_w,
				qt_dose_dia_w,
				ie_final_semana_w,
				nr_seq_decubito_w,
				nr_seq_fase_protocolo_w);
				
			/* obter campos fase */
			open c02;
			loop
			fetch c02 into	qt_dose_total_fase_w,
					qt_dose_fracao_w,
					nr_seq_campo_protoc_w;
			exit when c02%notfound;
				begin
				
				/* obter sequence */
				select	rxt_campo_seq.nextval
				into	nr_seq_campo_w
				from	dual;
				
				/* gerar campos fase */
				insert into rxt_campo (
					nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_seq_fase,
					nr_seq_campo,
					nr_aplicacoes,
					qt_dose_total,
					qt_dose_diaria,
					ie_situacao)
				values (
					nr_seq_campo_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_fase_tratamento_w,
					nr_seq_campo_protoc_w,
					qt_dia_trat_w,
					qt_dose_total_fase_w,
					qt_dose_fracao_w,
					'A');

				/* obter acessorios dos campos */
				open c04;
				loop
				fetch c04 into nr_seq_acess_prot_campo_w;
				exit when c04%notfound;
					begin

					/* obter sequence */
					select	rxt_acessorio_pac_seq.nextval
					into	nr_seq_acessorio_campo_w
					from	dual;
			
					/* gerar acessórios do campo */
					insert into rxt_acessorio_pac (
						nr_sequencia,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_atualizacao,
						nm_usuario,
						nr_seq_tratamento,
						nr_seq_campo,
						nr_seq_acessorio)
					values (
						nr_seq_acessorio_campo_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_tratamento_p,
						nr_seq_campo_w,
						nr_seq_acess_prot_campo_w);
					end;
				end loop;
				close c04;	
				end;
			end loop;
			close c02;
			
			end;
		end loop;
		close c01;
		
		open c03;
		loop
		fetch c03 into nr_seq_acessorio_prot_w;
		exit when c03%notfound;
			begin
			
			/* obter sequence */
			select	rxt_acessorio_pac_seq.nextval
			into	nr_seq_acessorio_w
			from	dual;
			
			/* gerar acessórios */
			insert into rxt_acessorio_pac (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tratamento,
				nr_seq_acessorio)
			values (
				nr_seq_acessorio_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_tratamento_p,
				nr_seq_acessorio_prot_w);
			
			end;
		end loop;
		close c03;
	elsif (ie_tipo_trat_w = 'R') then
		/* obter valores protocolo */
		select	max(qt_dose_total),
			MAX(QT_CHECK_FILM),
			MAX(ie_necessita_bloco),
			MAX(ie_necessita_qa)
		into	qt_dose_total_w,
			QT_CHECK_FILM_W,
			ie_necessita_bloco_w,
			ie_necessita_qa_w
		from	rxt_protocolo
		where	nr_sequencia = nr_seq_protocolo_w;
		
		/* ajustar a dose total do tratamento conforme o protocolo */
		update	rxt_tratamento
		set	qt_dose_total	= qt_dose_total_w,
			QT_CHECK_FILM	= QT_CHECK_FILM_W,
			ie_necessita_bloco = ie_necessita_bloco_w,
			ie_necessita_qa = ie_necessita_qa_w
		where	nr_sequencia	= nr_seq_tratamento_p;
		
		open c09;
		loop
		fetch c09 into	
			nr_seq_volume_protocolo_w,
			nr_seq_volume_w,
			ds_volume_w;
		exit when c09%notfound;
			begin
			
			select	sum(qt_dia_trat),
				sum(qt_dose_campo),
				sum(qt_dose_dia)
			into	qt_duracao_trat_ww,
				qt_dose_total_ww,
				qt_dose_campo_ww		
			from 	rxt_campo_prot_roentgen
			where	nr_seq_volume_protocolo = nr_seq_volume_protocolo_w;
			
			select	rxt_volume_tratamento_seq.nextval
			into	nr_seq_volume_tratamento_w
			from	dual;			
			
			insert	into rxt_volume_tratamento(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_tratamento,
				nr_seq_volume_protocolo,
				nr_seq_volume,
				ds_volume,
				qt_duracao_trat,
				qt_dose_total,
				qt_dose_fase)
			values	(nr_seq_volume_tratamento_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_tratamento_p,
				nr_seq_volume_protocolo_w,
				nr_seq_volume_w,
				ds_volume_w,
				qt_duracao_trat_ww,
				qt_dose_total_ww,
				qt_dose_campo_ww);
		
			/* obter fases  do volume*/
			open c10;
			loop
			fetch c10 into	nr_seq_campo_prot_roentgen_w,
					nm_campo_w,
					qt_dia_trat_w,
					qt_dose_campo_w,
					qt_dose_dia_w;
			exit when c10%notfound;
				begin

				/* obter sequence */
				select	rxt_campo_trat_roentgen_seq.nextval
				into	nr_seq_campo_trat_roentgen_w
				from	dual;

				/* gerar fases */
				insert into rxt_campo_trat_roentgen (
					nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_seq_tratamento,
					nm_campo,
					qt_dia_trat,
					qt_dose_campo,
					qt_dose_dia,
					nr_seq_campo_prot,
					nr_seq_volume_tratamento
					)
				values (
					nr_seq_campo_trat_roentgen_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_tratamento_p,
					nm_campo_w,
					qt_dia_trat_w,
					qt_dose_campo_w,
					qt_dose_dia_w,
					nr_seq_campo_prot_roentgen_w,
					nr_seq_volume_tratamento_w);
					

				/* obter campos fase */
				open c08;
				loop
				fetch c08 into	nr_seq_kvp_w,
						nr_seq_ma_w,
						nr_seq_aplicador_w,
						qt_tamanho_x_w,
						qt_tamanho_y_w,
						nr_minuto_duracao_w,
						nr_seq_campo_prot_w,
						nr_segundo_duracao_w;
				exit when c08%notfound;
					begin

					/* obter sequence */
					select	rxt_aplic_trat_roentgen_seq.nextval
					into	nr_seq_aplic_trat_w
					from	dual;

					/* gerar campos fase */
					insert into rxt_aplic_trat_roentgen (
						nr_sequencia,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_atualizacao,
						nm_usuario,
						nr_seq_campo_trat,
						nr_seq_kvp,
						nr_seq_ma,
						nr_seq_aplicador,
						qt_tamanho_x,
						qt_tamanho_y,
						nr_minuto_duracao,
						nr_segundo_duracao)
					values (
						nr_seq_aplic_trat_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_campo_trat_roentgen_w,
						nr_seq_kvp_w,
						nr_seq_ma_w,
						nr_seq_aplicador_w,
						qt_tamanho_x_w,
						qt_tamanho_y_w,
						nr_minuto_duracao_w,
						nr_segundo_duracao_w);
					end;
				end loop;
				close c08;
				end;
			end loop;
			close c10;
		end;
		end loop;
		close c09;

		/* obter fases */
		open c07;
		loop
		fetch c07 into	nr_seq_campo_prot_roentgen_w,
				nm_campo_w,
				qt_dia_trat_w,
				qt_dose_campo_w,
				qt_dose_dia_w,
				nr_seq_volume_protocolo_w;
		exit when c07%notfound;
			begin

			/* obter sequence */
			select	rxt_campo_trat_roentgen_seq.nextval
			into	nr_seq_campo_trat_roentgen_w
			from	dual;

			/* gerar fases */
			insert into rxt_campo_trat_roentgen (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao, 
				nm_usuario,
				nr_seq_tratamento,
				nm_campo,
				qt_dia_trat,
				qt_dose_campo,
				qt_dose_dia,
				nr_seq_campo_prot
				)
			values (
				nr_seq_campo_trat_roentgen_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_tratamento_p,
				nm_campo_w,
				qt_dia_trat_w,
				qt_dose_campo_w,
				qt_dose_dia_w,
				nr_seq_campo_prot_roentgen_w);
				

				/* obter campos fase */
				open c08;
				loop
				fetch c08 into	nr_seq_kvp_w,
						nr_seq_ma_w,
						nr_seq_aplicador_w,
						qt_tamanho_x_w,
						qt_tamanho_y_w,
						nr_minuto_duracao_w,
						nr_seq_campo_prot_w,
						nr_segundo_duracao_w;
				exit when c08%notfound;
					begin

					/* obter sequence */
					select	rxt_aplic_trat_roentgen_seq.nextval
					into	nr_seq_aplic_trat_w
					from	dual;

					/* gerar campos fase */
					insert into rxt_aplic_trat_roentgen (
						nr_sequencia,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_atualizacao,
						nm_usuario,
						nr_seq_campo_trat,
						nr_seq_kvp,
						nr_seq_ma,
						nr_seq_aplicador,
						qt_tamanho_x,
						qt_tamanho_y,
						nr_minuto_duracao,
						nr_segundo_duracao)
					values (
						nr_seq_aplic_trat_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_campo_trat_roentgen_w,
						nr_seq_kvp_w,
						nr_seq_ma_w,
						nr_seq_aplicador_w,
						qt_tamanho_x_w,
						qt_tamanho_y_w,
						nr_minuto_duracao_w,
						nr_segundo_duracao_w);

					end;
				end loop;
				close c08;
			end;
		end loop;
		close c07;	
	elsif (ie_tipo_trat_w = 'B') or (ie_braquiterapia_prot_w = 'S') then
		/* obter valores protocolo */
		select	max(qt_dose_total),
			MAX(QT_CHECK_FILM),
			MAX(ie_necessita_bloco),
			MAX(ie_necessita_qa)
		into	qt_dose_total_w,
			QT_CHECK_FILM_W,
			ie_necessita_bloco_w,
			ie_necessita_qa_w
		from	rxt_protocolo
		where	nr_sequencia = nr_seq_protocolo_w;
		
		/* ajustar a dose total do tratamento conforme o protocolo */
		update	rxt_tratamento
		set	qt_dose_total	= qt_dose_total_w,
			QT_CHECK_FILM	= QT_CHECK_FILM_W,
			ie_necessita_bloco = ie_necessita_bloco_w,
			ie_necessita_qa = ie_necessita_qa_w
		where	nr_sequencia	= nr_seq_tratamento_p;
	
		/* Obter aplicadores */
		open C11;
		loop
		fetch C11 into	
			nr_seq_aplic_prot_w,
			nr_seq_aplicador_w,
			nr_insercao_w,
			qt_dose_total_aplic_prot_w,
			qt_dose_insercao_w,
			qt_insercao_semana_w;
		exit when C11%notfound;
			begin
			
			/* obter sequence */
			select	rxt_braq_aplic_trat_seq.nextval
			into	nr_seq_braq_aplic_trat_w
			from	dual;

			/* gerar aplicadores */
			insert into rxt_braq_aplic_trat (
				nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_tratamento,
				nr_insercao,
				qt_dose_total,
				qt_dose_insercao,
				qt_insercao_semana,
				ie_situacao,
				nr_seq_aplicador
				)
			values (
				nr_seq_braq_aplic_trat_w,
				nvl(wheb_usuario_pck.get_cd_estabelecimento,1),
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_tratamento_p,
				nr_insercao_w,
				qt_dose_total_aplic_prot_w,
				qt_dose_insercao_w,
				qt_insercao_semana_w,
				'A',
				nr_seq_aplicador_w);			
			
			/* Ober campos do aplicador */
			open C12;
			loop
			fetch C12 into	
				nr_insercao_campo_w,
				ds_resultado_w,
				qt_resultado_w,
				nr_seq_campo_braq_w;
			exit when C12%notfound;
				begin
				
				/* gerar aplicadores */
				insert into rxt_braq_campo_aplic_trat (
					nr_sequencia,
					cd_estabelecimento,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_aplic_trat,
					qt_resultado,
					ds_resultado,
					nr_insercao,
					nr_seq_campo,
					ie_situacao
					)
				values (
					rxt_braq_campo_aplic_trat_seq.nextval,
					nvl(wheb_usuario_pck.get_cd_estabelecimento,1),
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_braq_aplic_trat_w,
					qt_resultado_w,
					ds_resultado_w,
					nr_insercao_campo_w,
					nr_seq_campo_braq_w,
					'A');
				
				end;
			end loop;
			close C12;
			
			end;
		end loop;
		close C11;
	end if;
	end;
	
end if;

commit;

end rxt_gerar_prot_pep;
/