create or replace
procedure deletar_diag_proc_relevante(
				nr_seq_diag_proc_p		number,
				ie_commit_p				varchar2 default 'S') is 
				
begin
		
	delete	cirurgia_descr_relevante
	where	nr_sequencia = nr_seq_diag_proc_p;

if	(nvl(ie_commit_p,'N') = 'S') and (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then
	commit;
end if;

end deletar_diag_proc_relevante;
/