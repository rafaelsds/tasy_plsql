create or replace
procedure atendimento_visita_saida_auto is 
nr_sequencia_w	number(10);

Cursor C01 is	/*   visitantes sem data saida e com data entrada < 24horas */
	select	nr_sequencia
	from	atendimento_visita
	where	dt_saida is null 
	and 	(dt_entrada +1) <= sysdate;
			
begin

open C01;
loop	
fetch C01 into
	nr_sequencia_w;
exit when C01%notfound;	
	begin

	update 	atendimento_visita
	set	dt_saida = sysdate
	where	nr_sequencia = nr_sequencia_w;

	end;
end loop;
close C01;


commit;

end atendimento_visita_saida_auto;
/