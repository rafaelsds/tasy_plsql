create or replace 
procedure Calcular_Nut_Pac_Ped(	nr_sequencia_p			number,
								nm_usuario_p			varchar2,
								cd_estabelecimento_p	number,
								ie_alterou_volume_p		varchar2 default 'N') is

qt_peso_w				number(15,3);	
qt_idade_dia_w			number(15,0);
qt_idade_mes_w			number(15,0);
qt_idade_ano_w			number(15,0);
qt_altura_cm_w			number(15,0);
qt_dia_npt_w			number(15,0);
qt_kcal_total_w			number(30,16);
qt_kcal_kg_w			number(30,16);
qt_fase_npt_w			number(15,0);
qt_nec_hidrica_diaria_w	number(16,1);
qt_aporte_nec_kcal_dia_w number(15,1);
qt_sodio_calcio_mag_w	number(30,16);
qt_osmolaridade_total_w	number(30,16);
qt_potassio_w			number(30,16);
qt_diaria_sodio_w		number(30,16);
qt_fosforo_w			number(30,16);
qt_vol_glicose_w		number(30,16);
qt_vol_lip_ml_w			number(30,16);
qt_diaria_calcio_w		number(30,16);
qt_conc_calcio_w		number(30,16);
qt_aporte_hidrico_diario_w number(30,16);
qt_vel_inf_glicose_w	number(30,16);
qt_nec_kcal_kg_dia_w	number(30,16);
pr_conc_glic_solucao_w	number(30,16);
qt_volume_final_w		number(30,16);
qt_vol_final_ele_w		number(30,16);
nr_casas_w				number(2,0);
qt_hora_inf_w			number(3,0);
nr_fator_lipideo_w		number(10,2);
nr_fator_glicose_w		number(10,2);
qt_rel_cal_nit_w		number(15,0);
nr_seq_zinco_w			number(15,0);
nr_seq_calcio_w			number(10,0);
nr_seq_oligoelemento_w	number(15,0);
qt_equipo_w				number(10,2);
qt_rel_cal_fosforo_w	number(30,16);
qt_elem_kg_dia_calcio_w	number(30,16);
qt_calcio_w				number(30,16);
qt_g_prot_kg_dia_w		number(15,3);
qt_g_lip_kg_dia_w		number(15,3);
qt_g_glic_kg_dia_w		number(15,3);
qt_g_prot_dia_w			number(15,3);
qt_g_lip_dia_w			number(15,3);
qt_g_glic_dia_w			number(15,3);
qt_kcal_prot_w			number(15,3);
qt_kcal_lip_w			number(15,3);
qt_kcal_glic_w			number(15,3);
qt_vol_prot_w			number(15,3);
qt_vol_lip_w			number(15,3);
qt_vol_glic_w			number(15,3);
qt_kcal_elem_w			number(15,3);
nr_seq_glicose_w		number(10,0);
nr_seq_elemento_w		number(10,0);
qt_vol_elemento_w		number(30,16);
qt_vol_total_w			number(30,16);
pr_total_w				number(30,16);
qt_conversao_ml_w		number(30,16);
qt_conversao_ml_ww		number(15,4);			
ie_equilibrio_w			varchar2(1)		:= 'S';
qt_produto_w			number(05,0)	:= 0;
qt_g_menor_w			number(30,16);
qt_g_maior_w			number(30,16);
nr_seq_pac_elem_w		number(10,0);
nr_seq_pac_elem2_w		number(10,0);
nr_seq_elem_w			number(10,0);
ie_prim_fase_w			varchar2(1);
ie_seg_fase_w			varchar2(1);
ie_terc_fase_w			varchar2(1);
ie_quar_fase_w			varchar2(1);
ie_zinco_w				varchar2(1);
qt_fase_w				number(30,16);
qt_fase1_w				number(30,16) := null;
qt_fase2_w				number(30,16) := null;
qt_fase3_w				number(30,16) := null;
qt_fase4_w				number(30,16) := null;
qt_volume_w				number(30,16);
qt_vol_cor_w			number(30,16);
nr_seq_elem_mat_w		number(10,0);
nr_seq_potassio_w		number(10,0);
nr_seq_sodio_w			number(10,0);
nr_seq_fosforo_w		number(10,0);
cd_material_w			number(06,0);
qt_pot_clor_fosforo_w	number(30,16);
nr_seq_material_w		number(10,0);
qt_diaria_w				number(30,16);
ie_correcao_w			varchar2(01);
ie_ajustar_potassio_w	varchar2(01);
ie_ajustar_sodio_w		varchar2(01);
nr_seq_heparina_w		number(10,0);
qt_troca_glicose_w		number(10,0);
nr_seq_glic_Troca_w		number(10,0);
qt_volume_total_w		number(30,16);
nr_prescricao_w			number(14,0);
nr_horas_validade_w		number(5,0);
qt_gotejo_w				number(30,16);
qt_vol_sodio_w			number(30,16);
qt_vol_magnesio_w		number(30,16);
nr_casas_gotejo_w		number(10,0);
qt_vol_calcio_w			number(30,16);
qt_rel_cal_fos_w		number(30,16);
qt_kcal_carboidrato_w	number(15,4);
qt_kcal_proteina_w		number(15,4);
qt_kcal_lipidio_w		number(15,4);
qt_kcal_totais_w		number(15,4);
pr_proteina_w			number(4,1);
pr_lipidio_w			number(4,1);
pr_carboidrato_w		number(4,1);
ie_geracao_fos_cal_w	varchar2(1);
ie_calculo_auto_w		varchar2(1);
qt_descontar_hidrico_w	number(15,4);
ie_editado_w			nut_pac.ie_editado%type;
qt_conversao_kcal_w		nut_elem_material.qt_conversao_kcal%type;
nr_fator_lipideo_ww		number(10,2);
qt_g_lip_kg_dia_ww		number(15,3);
qt_vol_lip_ww			number(15,3);
qt_g_glu_kg_dia_w		number(15,3);
qt_vol_glutamina_w		number(30,16);
qt_kcal_glutamina_w		number(15,3);
qt_diaria_zinco_w		number(30,16);
ie_arredonda_npt_w		parametro_medico.ie_arredondar_npt%type := 'N';
qt_fator_correcao_w		number(30,16);
nr_seq_elem_agua_w		number(10) := 0;

cursor c01 is
select	a.nr_sequencia,
		nvl(qt_kcal,0)
from	nut_pac_elemento a
where	nr_seq_nut_pac	= nr_sequencia_p; 

cursor c02 is
select	a.nr_sequencia,
		qt_conversao_ml,
		a.nr_seq_pac_elem
from	nut_elem_material b,
		nut_pac_elem_mat a,
		nut_pac_elemento e
where	a.nr_seq_pac_elem	= e.nr_sequencia
and		a.nr_seq_elem_mat	= b.nr_sequencia
and		e.nr_seq_elemento	= nr_seq_glicose_w
and		e.nr_seq_nut_pac	= nr_sequencia_p
and		qt_g_menor_w		> 0
and		nvl(b.ie_tipo,'NPT')	= 'NPT'
order by qt_conversao_ml desc; 

cursor c03 is
select	a.nr_sequencia,
		a.ie_prim_fase,
		a.ie_seg_fase,
		a.ie_terc_fase,
		a.ie_quar_fase
from	nut_pac_elem_mat b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_sequencia	= b.nr_seq_pac_elem;

cursor c04 is
select	nvl(qt_vol_cor, qt_volume),
		nr_sequencia
from	nut_pac_elem_mat
where	nr_seq_pac_elem	= nr_seq_elem_w;

cursor c05 is
select	a.nr_sequencia,
		b.nr_sequencia,
		b.qt_volume
from	nut_pac_elem_mat b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_sequencia	= b.nr_seq_pac_elem
and		nvl(b.qt_volume,0)	> 0;

cursor c06 is
select	b.nr_sequencia
from	nut_elem_material b
where	b.nr_seq_elemento       = nr_seq_glicose_w
and		b.nr_sequencia  not in (select	nr_seq_elem_mat
								from	nut_pac_elem_mat
								where	nr_seq_pac_elem = nr_seq_pac_elem_w)
and		qt_conversao_ml < qt_conversao_ml_w
and		nvl(b.ie_tipo,'NPT')	= 'NPT'
order by qt_conversao_ml;

cursor c07 is
select c.qt_elem_kg_dia,
	   obter_vol_elem_nut_pac(c.nr_sequencia),
	   b.qt_conversao_kcal
from   nut_elemento a,
	   nut_elem_material b,
	   nut_pac_elemento c,
	   nut_pac_elem_mat d
where  a.nr_sequencia		= c.nr_seq_elemento
and	   c.nr_sequencia		= d.nr_seq_pac_elem
and	   b.nr_sequencia		= d.nr_seq_elem_mat
and	   c.nr_seq_nut_pac	  	= nr_sequencia_p
and    a.ie_tipo_elemento	= 'L';

begin
Gerar_Nut_Elemento_Pac(nr_sequencia_p, nm_usuario_p);

Obter_Param_Usuario(924,748,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_casas_w);
Obter_Param_Usuario(924,763,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_fator_lipideo_w);
Obter_Param_Usuario(924,884,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_casas_gotejo_w);
Obter_Param_Usuario(924,1009,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_fator_glicose_w);

select	qt_peso,
		qt_idade_dia,
		qt_idade_mes,
		qt_idade_ano,
		qt_altura_cm,
		qt_dia_npt,
		qt_kcal_total,
		qt_kcal_kg,
		qt_fase_npt,
		qt_nec_hidrica_diaria,
		qt_aporte_hidrico_diario,
		qt_vel_inf_glicose,
		qt_nec_kcal_kg_dia,
		pr_conc_glic_solucao,
		qt_rel_cal_nit,
		qt_equipo,
		nvl(ie_correcao,'N'),
		nvl(ie_ajustar_potassio,'S'),
		nr_prescricao,
		nvl(ie_ajustar_sodio,'N'),
		nvl(ie_zinco,'N'),
		qt_rel_cal_fos,
		nvl(ie_geracao_fos_cal,'N'),
		nvl(ie_calculo_auto,'S'),
		qt_gotejo_npt,
		qt_hora_inf,
		nvl(qt_descontar_hidrico,0),
		nvl(ie_editado,'N')
into	qt_peso_w,
		qt_idade_dia_w,
		qt_idade_mes_w,
		qt_idade_ano_w,
		qt_altura_cm_w,
		qt_dia_npt_w,
		qt_kcal_total_w,
		qt_kcal_kg_w,
		qt_fase_npt_w,
		qt_nec_hidrica_diaria_w,
		qt_aporte_hidrico_diario_w,
		qt_vel_inf_glicose_w,
		qt_nec_kcal_kg_dia_w,
		pr_conc_glic_solucao_w,
		qt_rel_cal_nit_w,
		qt_equipo_w,
		ie_correcao_w,
		ie_ajustar_potassio_w,
		nr_prescricao_w,
		ie_ajustar_sodio_w,
		ie_zinco_w,
		qt_rel_cal_fos_w,
		ie_geracao_fos_cal_w,
		ie_calculo_auto_w,
		qt_gotejo_w,
		qt_hora_inf_w,
		qt_descontar_hidrico_w,
		ie_editado_w
from	Nut_pac
where	nr_sequencia = nr_sequencia_p;

select	nvl(max(nr_sequencia),0)
into	nr_seq_glicose_w
from	nut_elemento
where	ie_tipo_elemento	= 'C'
and		nvl(ie_gerar_ped,'S') = 'S'
and		ie_situacao = 'A';

select 	nvl(max(ie_arredondar_npt),'N')
into	ie_arredonda_npt_w
from	parametro_medico
where 	cd_estabelecimento = cd_estabelecimento_p;

qt_aporte_hidrico_diario_w	:= (qt_nec_hidrica_diaria_w * qt_peso_w) - qt_descontar_hidrico_w;
qt_aporte_nec_kcal_dia_W	:= qt_nec_kcal_kg_dia_w * qt_peso_w;

if (nvl(ie_alterou_volume_p,'N') = 'S') then

	select	max(nr_sequencia),
			nvl(max(qt_volume_final),0)
	into	nr_seq_pac_elem2_w,
			qt_vol_final_ele_w
	from	nut_pac_elemento
	where	nr_seq_nut_pac = nr_sequencia_p
	and		nr_seq_elemento = nr_seq_glicose_w;

	select	round(nvl(max(nvl(qt_vol_final_ele_w,0) * dividir_sem_round(nvl(b.qt_conv_unid_cons,1),nvl(b.qt_conversao_ml,1))),0),2)
	into	qt_g_glic_dia_w
	from	nut_elem_material b,
			nut_pac_elem_mat a
	where	a.nr_seq_pac_elem	= nr_seq_pac_elem2_w
	and		a.nr_seq_elem_mat	= b.nr_sequencia
	and		nvl(b.ie_padrao,'N') = 'S'
	and		nvl(b.ie_tipo,'NPT')	= 'NPT';
	
	qt_g_Glic_kg_dia_w 	 := round(dividir_Sem_round(qt_g_glic_dia_w,qt_peso_w),2);
	qt_vel_inf_glicose_w := round(qt_g_Glic_kg_dia_w/1.44,2);
	qt_kcal_glic_w		 := qt_g_glic_dia_w * nr_fator_glicose_w;
	
	if (ie_arredonda_npt_w = 'S') then
		qt_g_glic_dia_w	:= round(qt_g_glic_dia_w,2);
		qt_g_Glic_kg_dia_w	:= round(qt_g_Glic_kg_dia_w,2);
	end if;
	
	update	nut_pac_elemento
	set		qt_diaria 		= qt_g_glic_dia_w,
			qt_elem_kg_dia	= qt_g_Glic_kg_dia_w,
			pr_concentracao = dividir((qt_g_glic_dia_w * 100), qt_aporte_hidrico_diario_w),
			nm_usuario		= nm_usuario_p,
			dt_atualizacao 	= sysdate,
			qt_kcal			= qt_kcal_glic_w
	where	nr_sequencia 	= nr_seq_pac_elem2_w;
	
	update	nut_pac
	set		qt_vel_inf_glicose 	= qt_vel_inf_glicose_w,
			nm_usuario 			= nm_usuario_p,
			dt_atualizacao 		= sysdate
	where	nr_sequencia 		= nr_sequencia_p;
elsif (nvl(ie_alterou_volume_p,'N') = 'Q') then

	select	max(nr_sequencia),
			nvl(max(qt_elem_kg_dia),0)
	into	nr_seq_pac_elem2_w,
			qt_g_Glic_kg_dia_w
	from	nut_pac_elemento
	where	nr_seq_nut_pac = nr_sequencia_p
	and		nr_seq_elemento = nr_seq_glicose_w;

	select	max(nvl(b.qt_conversao_ml,1))
	into	qt_conversao_ml_ww
	from	nut_elem_material b,
			nut_pac_elem_mat a
	where	a.nr_seq_pac_elem	= nr_seq_pac_elem2_w
	and		a.nr_seq_elem_mat	= b.nr_sequencia
	and		nvl(b.ie_padrao,'N') = 'S'
	and		nvl(b.ie_tipo,'NPT')	= 'NPT';
	
	qt_g_glic_dia_w		 := round(qt_g_Glic_kg_dia_w*qt_peso_w,2);
	qt_vel_inf_glicose_w := round(qt_g_Glic_kg_dia_w/1.44,2);
	qt_vol_final_ele_w	 := round(qt_g_glic_dia_w * qt_conversao_ml_ww,2);
	qt_kcal_glic_w		 := qt_g_glic_dia_w * nr_fator_glicose_w;
	
	if (ie_arredonda_npt_w = 'S') then
		--qt_vol_final_ele_w	:= ceil(qt_vol_final_ele_w * 100) / 100;
		qt_vol_final_ele_w	:= round(qt_vol_final_ele_w, 2);
	end if;
	
	update	nut_pac_elemento
	set		qt_diaria 		= qt_g_glic_dia_w,
			pr_concentracao = dividir((qt_g_glic_dia_w * 100), qt_aporte_hidrico_diario_w),
			nm_usuario		= nm_usuario_p,
			dt_atualizacao 	= sysdate,
			qt_kcal			= qt_kcal_glic_w,
			qt_volume_final = qt_vol_final_ele_w
	where	nr_sequencia 	= nr_seq_pac_elem2_w;
	
	update	nut_pac
	set		qt_vel_inf_glicose 	= qt_vel_inf_glicose_w,
			nm_usuario 			= nm_usuario_p,
			dt_atualizacao 		= sysdate
	where	nr_sequencia 		= nr_sequencia_p;
end if;

Calcular_Nut_Elem_Ped(nr_sequencia_p, nm_usuario_p,ie_alterou_volume_p);

if (ie_arredonda_npt_w = 'S') then

	select	nvl(sum(qt_elem_kg_dia),0),		
			nvl(sum(Obter_vol_elem_nut_pac(a.nr_sequencia)),0)
	into	qt_g_prot_kg_dia_w,
			qt_vol_prot_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'P'
	and		b.ie_glutamina 		= 'N';

	select	nvl(sum(qt_elem_kg_dia),0),		
			nvl(sum(Obter_vol_elem_nut_pac(a.nr_sequencia)),0)
	into	qt_g_glu_kg_dia_w,
			qt_vol_glutamina_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'P'
	and		b.ie_glutamina 		= 'S';
	
else

	select	nvl(sum(qt_elem_kg_dia),0),
			nvl(sum(Obter_vol_elem_nut_pac(a.nr_sequencia)),0)
	into	qt_g_prot_kg_dia_w,
			qt_vol_prot_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'P';
	
end if;

/*
	C�lculo dos lip�dios separados devido ao fator de convers�o para calorias poderem ser diferentes
*/
qt_kcal_lip_w	:= 0;
qt_vol_lip_ww	:= 0;
open c07;
loop
fetch c07 into	
	qt_g_lip_kg_dia_w,
	qt_vol_lip_w,
	qt_conversao_kcal_w;
exit when c07%notfound;
	begin		
		if (qt_conversao_kcal_w > 0) then
			nr_fator_lipideo_ww	:= qt_conversao_kcal_w;
		else
			nr_fator_lipideo_ww	:= nr_fator_lipideo_w;		
		end if;
		
		if (ie_arredonda_npt_w = 'S') then
			qt_g_Lip_kg_dia_ww	:= qt_vol_lip_w;
		else
			qt_g_Lip_kg_dia_ww	:= qt_g_Lip_kg_dia_w * qt_peso_w;
		end if;
		
		qt_kcal_lip_w	:= qt_kcal_lip_w + (qt_g_Lip_kg_dia_ww * nr_fator_lipideo_ww);
		qt_vol_lip_ww	:= qt_vol_lip_ww + qt_vol_lip_w;
	end;
end loop;
close c07;

qt_vol_Glic_w	:= trunc(qt_aporte_hidrico_diario_w - qt_vol_prot_w - qt_vol_lip_w);

if (nvl(ie_alterou_volume_p,'N') = 'N') then
	if (ie_editado_w = 'S') or
	   (nvl(qt_kcal_total_w,0) = 0) then
		qt_g_Glic_kg_dia_w	:= round(qt_vel_inf_glicose_w * 1.44,2);
	else
			select	nvl(max(qt_elem_kg_dia),0)
			into	qt_g_Glic_kg_dia_w
			from	nut_pac_elemento
			where	nr_seq_nut_pac = nr_sequencia_p
			and		nr_seq_elemento = nr_seq_glicose_w;
	end if;
	qt_g_glic_dia_w		:= qt_g_glic_kg_dia_w * qt_peso_w;
	qt_kcal_glic_w		:= qt_g_glic_dia_w * nr_fator_glicose_w;
		
	begin
	update	nut_pac_elemento
	set		qt_elem_kg_dia	= qt_g_Glic_kg_dia_w,
			qt_diaria	= qt_g_glic_dia_w,
			qt_kcal		= qt_kcal_glic_w
	where	nr_seq_nut_pac	= nr_sequencia_p
	and		nr_seq_elemento	= nr_seq_glicose_w;
	exception when others then
		--'Ocorreu uma excess�o ao calcular os valores da NPT. Verifique se os valores informados est�o corretos! #@#@');
		Wheb_mensagem_pck.exibir_mensagem_abort(190072);
	end;
end if;

qt_g_prot_dia_w		:= qt_g_Prot_kg_dia_w * qt_peso_w;
qt_g_lip_dia_w		:= qt_g_Lip_kg_dia_w * qt_peso_w;
qt_kcal_prot_w		:= qt_g_Prot_dia_w * 4;

if (ie_arredonda_npt_w = 'S') then
	qt_kcal_glutamina_w	:= qt_vol_glutamina_w * 0.8;
	qt_kcal_total_w		:= qt_kcal_prot_w + qt_kcal_lip_w + qt_kcal_glic_w + qt_kcal_glutamina_w;
else
	qt_kcal_total_w		:= qt_kcal_prot_w + qt_kcal_lip_w + qt_kcal_glic_w;
end if;
	
qt_kcal_kg_w		:= Dividir(qt_kcal_total_w, qt_peso_w);
if (ie_arredonda_npt_w = 'S') then
	qt_rel_cal_nit_w	:= dividir_sem_round((nvl(qt_kcal_lip_w,0) + (nvl(qt_g_glic_dia_w,0) * 3.4)), (dividir_sem_round(nvl(qt_g_prot_dia_w,0), 6.25) + dividir_sem_round((qt_vol_glutamina_w * 3.87),100)));
else
	qt_rel_cal_nit_w	:= Dividir((nvl(qt_kcal_lip_w,0) + nvl(qt_kcal_glic_w,0)), (nvl(qt_g_prot_dia_w,0) * 0.15));
end if;
pr_conc_glic_solucao_w	:= Dividir((qt_g_glic_dia_w * 100), qt_aporte_hidrico_diario_w);

select	nvl(sum(qt_osmolaridade),0) 
into	qt_osmolaridade_total_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia;

select	nvl(sum(obter_vol_elem_nut_pac(nr_sequencia)),0)
into	qt_vol_total_w	
from	nut_pac_elemento
where	nr_seq_nut_pac	= nr_sequencia_p;

update	Nut_pac
set		qt_dia_npt		= qt_dia_npt_w,
		qt_kcal_total		= qt_kcal_total_w,
		qt_kcal_kg		= qt_kcal_kg_w,
		qt_aporte_hidrico_diario = qt_aporte_hidrico_diario_w,
		qt_nec_kcal_dia		= qt_aporte_nec_kcal_dia_W,
		pr_conc_glic_solucao	= pr_conc_glic_solucao_w,
		qt_rel_cal_nit		= qt_rel_cal_nit_w,
		qt_osmolaridade_total	= qt_osmolaridade_total_w
where	nr_sequencia		= nr_sequencia_p;

open c01;
loop
fetch c01	into
		nr_seq_elemento_w,
      	qt_kcal_elem_w;	
exit when C01%notfound;
	begin
	update	nut_pac_elem_mat
	set		qt_vol_1_fase	= 0,
			qt_vol_2_fase	= 0,
			qt_vol_3_fase	= 0,
			qt_vol_4_fase	= 0,
			qt_vol_cor	= 0
	where	nr_seq_pac_elem	= nr_seq_elemento_w;
	
	pr_total_w		:= dividir(qt_kcal_elem_w * 100, qt_kcal_total_w);
	update	nut_pac_elemento
	set		pr_total	= nvl(pr_total_w,0)
	where	nr_sequencia	= nr_seq_elemento_w;
	end;
end loop;
close c01;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
/* Equilibrar Glicose x Balan�o Hidrico */
qt_produto_w		:= 0;
qt_vol_total_w	:= 0;
ie_equilibrio_w	:= 'S';
qt_troca_glicose_w	:= 0;
qt_g_menor_w		:= 999;
open c02;
loop
fetch c02	into
	nr_seq_elemento_w,
	qt_conversao_ml_w,
	nr_seq_pac_elem_w;	
exit when C02%notfound;
	qt_produto_w		:= qt_produto_w + 1;
	if	(qt_produto_w = 1) then
		qt_vol_elemento_w	:= qt_g_glic_dia_w * qt_conversao_ml_w;
		qt_vol_total_w		:= qt_vol_elemento_w;
		qt_g_menor_w		:= qt_g_glic_dia_w;
		qt_g_maior_w		:= 0;
		nr_seq_glic_Troca_w	:= nr_seq_elemento_w;
	else
		qt_vol_elemento_w	:= 0;
	end if;
	
	update	nut_pac_elem_mat
	set		qt_volume			= qt_vol_elemento_w,
			qt_vol_1_fase		= 0,
			qt_vol_2_fase		= 0,
			qt_vol_3_fase		= 0,
			qt_vol_4_fase		= 0,
			qt_vol_cor			= 0
	where	nr_sequencia		= nr_seq_elemento_w;
end loop;
close c02;

if	(qt_vol_total_w <= qt_vol_glic_w) then
	ie_equilibrio_w		:= 'S';
elsif	(qt_produto_w	= 2) then
	ie_equilibrio_w		:= 'N';
end if;

WHILE (ie_equilibrio_w = 'N') LOOP
	if	(qt_vol_total_w > 0) and
		(qt_vol_total_w <= qt_vol_glic_w) then
		ie_equilibrio_w	:= 'S';
	elsif	(qt_vol_total_w = 0) and
		(qt_troca_glicose_w > 7) then
		ie_equilibrio_w	:= 'S';
	elsif	(qt_vol_total_w = 0) or
		(qt_g_menor_w < 0) then
		nr_seq_elemento_w	:= 0;
		OPEN c06;
		LOOP
		FETCH C06	into
			nr_seq_elemento_w;	
		exit when C06%notfound;
			nr_seq_elemento_w	:= nr_seq_elemento_w;
		END LOOP;
		CLOSE C06;
		if	(nr_seq_elemento_w > 0) then
			qt_troca_glicose_w	:= qt_troca_glicose_w + 1;
			update	nut_pac_elem_mat
			set		nr_seq_elem_mat = nr_seq_elemento_w
			where	nr_sequencia	= nr_seq_glic_Troca_w;
		else
			ie_equilibrio_w	:= 'S';
		end if;
	else
		qt_g_menor_w		:= qt_g_menor_w - 1;
		qt_g_maior_w		:= qt_g_maior_w + 1;
		qt_vol_total_w		:= 0;
		OPEN c02;
		LOOP
		FETCH C02	into
			nr_seq_elemento_w,
			qt_conversao_ml_w,
			nr_seq_pac_elem_w;	
		exit when C02%notfound;
			if	(qt_vol_total_w = 0) then
				qt_vol_elemento_w	:= qt_g_menor_w * qt_conversao_ml_w;
			else
				qt_vol_elemento_w	:= qt_g_maior_w * qt_conversao_ml_w;
			end if;
			qt_vol_total_w		:= qt_vol_total_w + qt_vol_elemento_w;
			
			update	nut_pac_elem_mat
			set		qt_volume	= qt_vol_elemento_w
			where	nr_sequencia	= nr_seq_elemento_w;
		END LOOP;
		CLOSE C02;
	end if;
END LOOP;

if	(ie_geracao_fos_cal_w = 'S') then
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_fosforo_w
	from	nut_elemento
	where	ie_tipo_elemento	= 'F';
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_calcio_w
	from	nut_elemento
	where	ie_tipo_elemento	= 'I';
	
	select	nvl(sum(qt_elem_kg_dia),0)
	into	qt_elem_kg_dia_calcio_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'I';
	
	qt_fosforo_w	:= dividir(qt_elem_kg_dia_calcio_w,qt_rel_cal_fos_w);
	
	update	nut_pac_elemento
	set		qt_elem_kg_dia	= qt_fosforo_w,
			qt_diaria		= (qt_fosforo_w * qt_peso_w)
	where	nr_seq_nut_pac	= nr_sequencia_p
	and		nr_seq_elemento	= nr_seq_fosforo_w;	
	
end if;

if	(ie_ajustar_sodio_w = 'S') then
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_sodio_w
	from	nut_elemento
	where	ie_tipo_elemento	= 'N'
	and	nvl(ie_gerar_ped,'N')	= 'S';
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_fosforo_w
	from	nut_elemento
	where	ie_tipo_elemento	= 'F';

	select	nvl(max(c.cd_material),0),
			nvl(max(b.qt_volume),0),
			nvl(max(a.qt_diaria),0)
	into	cd_material_w,
			qt_volume_w,
			qt_diaria_w
	from	nut_elem_material c,
			nut_pac_elem_mat b,
			nut_pac_elemento a
	where	b.nr_seq_elem_mat	= c.nr_sequencia
	and		a.nr_sequencia		= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_fosforo_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p
	and		nvl(c.ie_tipo,'NPT')	= 'NPT';
	
/* Obter dados do f�sforo 
Pegar os valores do F�sforo org�nico, por�m a convers�o deve ser de acordo com o cadastro do s�dio, uma vez que o PRODUTO � o mesmo em ambos */
	
	select	max(qt_diaria)
	into	qt_diaria_sodio_w
	from	nut_pac_elemento a
	where	nr_seq_elemento		= nr_seq_sodio_w
	and	a.nr_seq_nut_pac	= nr_sequencia_p;
	
	begin
		if (ie_arredonda_npt_w = 'S') then
			select	dividir_sem_round(qt_volume_w, qt_conversao_ml)
			into	qt_fosforo_w
			from	nut_elem_material
			where	nr_seq_elemento		= nr_seq_sodio_w
			and		cd_material		= cd_material_w
			and		nvl(ie_tipo,'NPT')	= 'NPT';
		else
			select	dividir(qt_volume_w, qt_conversao_ml)
			into	qt_fosforo_w
			from	nut_elem_material
			where	nr_seq_elemento		= nr_seq_sodio_w
			and		cd_material		= cd_material_w
			and		nvl(ie_tipo,'NPT')	= 'NPT';
		end if;
	exception
	when others then	
		qt_fosforo_w	:= 0;
	end;	
	
	select	nvl(max(a.qt_diaria),0),
			nvl(max(b.nr_sequencia),0)
	into	qt_diaria_w,
			nr_seq_material_w
	from	nut_pac_elem_mat b,
			nut_pac_elemento a
	where	a.nr_sequencia	= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_sodio_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p;

	select	nvl(max(c.qt_conversao_ml),0)
	into	qt_conversao_ml_w
	from	nut_elem_material c,
			nut_pac_elem_mat b,
			nut_pac_elemento a
	where	b.nr_seq_elem_mat	= c.nr_sequencia
	and		a.nr_sequencia		= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_sodio_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p
	and		nvl(c.ie_tipo,'NPT')	= 'NPT';

/* Atualizar dados do pot�ssio */
	/*Tirar do total de mEq do S�dio, os Meqs de S�dio que vieram do F�SFORO org�nico e do Bicarbonato.
	S� ent�o Gerar os mls do produto do S�dio, geralmente Cloreto de S�dio.*/
	
	if (ie_arredonda_npt_w = 'S') then
		qt_diaria_w	:= qt_diaria_sodio_w;
	else
		qt_diaria_w	:= qt_diaria_sodio_w - qt_fosforo_w;
	end if;
	
	--qt_diaria_w	:= qt_diaria_w - qt_pot_clor_fosforo_w;
	if	(qt_diaria_w < 0) then
		qt_diaria_w		:= 0;
	end if;
	
	update	nut_pac_elemento
	set		qt_diaria	= qt_diaria_w
	where	nr_seq_nut_pac	= nr_sequencia_p
	and		nr_seq_elemento	= nr_seq_sodio_w;

	if (ie_arredonda_npt_w = 'S') then
		if (qt_diaria_sodio_w < qt_fosforo_w) then
			qt_volume_w	:= 0;
		else
			qt_volume_w	:= (qt_diaria_sodio_w - qt_fosforo_w) * qt_conversao_ml_w;
		end if;

		qt_volume_w	:= ceil(qt_volume_w * 100) / 100;
	else
		qt_volume_w	:= qt_diaria_w * qt_conversao_ml_w;
	end if;

	update	nut_pac_elem_mat
	set		qt_volume	= qt_volume_w
	where	nr_sequencia	= nr_seq_material_w;
end if;


/* Ajustar MEQ de pot�ssio em fun��o de que parte do mesmo j� vem com o Fosfato de pot�ssio do elemento F�sforo */
/* Obter dados do F�sforo e do fosfato de pot�ssio */
if	(ie_ajustar_potassio_w = 'S') then
	select nvl(max(nr_sequencia),0)
	into	nr_seq_potassio_w
	from	nut_elemento
	where	ie_tipo_elemento	= 'K';
	
	select nvl(max(nr_sequencia),0)
	into	nr_seq_fosforo_w
	from	nut_elemento
	where	ie_tipo_elemento	= 'F';

	select	nvl(max(c.cd_material),0),
			nvl(max(b.qt_volume),0),
			nvl(max(a.qt_diaria),0)
	into	cd_material_w,
			qt_volume_w,
			qt_diaria_w
	from	nut_elem_material c,
			nut_pac_elem_mat b,
			nut_pac_elemento a
	where	b.nr_seq_elem_mat	= c.nr_sequencia
	and		a.nr_sequencia	= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_fosforo_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p
	and		nvl(c.ie_tipo,'NPT')	= 'NPT';
	
/* Obter dados do pot�ssio */
	begin
	select	dividir(qt_volume_w, qt_conversao_ml)
	into	qt_pot_clor_fosforo_w
	from	nut_elem_material
	where	nr_seq_elemento	= nr_seq_potassio_w
	and		cd_material		= cd_material_w
	and		nvl(ie_tipo,'NPT')	= 'NPT';
	exception
	when others then	
		qt_pot_clor_fosforo_w:= 0;
	end;
	select	nvl(max(a.qt_diaria),0),
			nvl(max(b.nr_sequencia),0)
	into	qt_diaria_w,
			nr_seq_material_w
	from	nut_pac_elem_mat b,
			nut_pac_elemento a
	where	a.nr_sequencia	= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_potassio_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p;

	select	nvl(max(c.qt_conversao_ml),0)
	into	qt_conversao_ml_w
	from	nut_elem_material c,
			nut_pac_elem_mat b,
			nut_pac_elemento a
	where	b.nr_seq_elem_mat	= c.nr_sequencia
	and		a.nr_sequencia	= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_potassio_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p
	and		nvl(c.ie_tipo,'NPT')	= 'NPT';

/* Atualizar dados do pot�ssio */
	qt_diaria_w	:= qt_diaria_w - qt_pot_clor_fosforo_w;
	if	(qt_diaria_w < 0) then
		qt_diaria_w		:= 0;
	end if;
		
	update	nut_pac_elemento
	set		qt_diaria		= qt_diaria_w
	where	nr_seq_nut_pac	= nr_sequencia_p
	and		nr_seq_elemento	= nr_seq_potassio_w;

	qt_volume_w	:= qt_diaria_w * qt_conversao_ml_w;
	
	if (ie_arredonda_npt_w = 'S') then
		--qt_volume_w	:= ceil(qt_volume_w * 100) / 100;
		qt_volume_w	:= round(qt_volume_w, 2);
	end if;
	
	update	nut_pac_elem_mat
	set		qt_volume	= qt_volume_w
	where	nr_sequencia	= nr_seq_material_w;
end if;

if	(ie_zinco_w = 'S') then
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_zinco_w
	from	nut_elemento
	where	ie_tipo_elemento	= 'Z'
	and		nvl(ie_gerar_ped,'N')	= 'S'
	and		nvl(ie_situacao,'I')	= 'A';
	
	select	nvl(min(nr_sequencia),0)
	into	nr_seq_oligoelemento_w
	from	nut_elemento
	where	ie_tipo_elemento	= 'G'
	and		nvl(ie_gerar_ped,'N')	= 'S'
	and		nvl(ie_situacao,'I')	= 'A';

	select	nvl(max(c.cd_material),0),
			nvl(max(b.qt_volume),0),
			nvl(max(a.qt_diaria),0)
	into	cd_material_w,
			qt_volume_w,
			qt_diaria_w
	from	nut_elem_material c,
			nut_pac_elem_mat b,
			nut_pac_elemento a
	where	b.nr_seq_elem_mat	= c.nr_sequencia
	and		a.nr_sequencia		= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_oligoelemento_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p
	and		nvl(c.ie_tipo,'NPT')	= 'NPT';
	
/* Obter dados do Zinco 
Pegar os valores do Zinco org�nico, por�m a convers�o deve ser de acordo com o cadastro do s�dio, uma vez que o PRODUTO � o mesmo em ambos */

	if (ie_arredonda_npt_w = 'S') then
		select	max(qt_diaria)
		into	qt_diaria_zinco_w
		from	nut_pac_elemento a
		where	nr_seq_elemento		= nr_seq_zinco_w
		and		a.nr_seq_nut_pac	= nr_sequencia_p;
	else
		select	max(qt_diaria)
		into	qt_diaria_sodio_w
		from	nut_pac_elemento a
		where	nr_seq_elemento		= nr_seq_zinco_w
		and		a.nr_seq_nut_pac	= nr_sequencia_p;
	end if;
	
	begin
	select	dividir(qt_volume_w, qt_conversao_ml)
	into	qt_fosforo_w
	from	nut_elem_material
	where	nr_seq_elemento		= nr_seq_zinco_w
	and		cd_material		= cd_material_w
	and		nvl(ie_tipo,'NPT')	= 'NPT'
	and		ie_situacao = 'A';
	exception
	when others then	
		qt_fosforo_w	:= 0;
	end;	
	
	select	nvl(max(a.qt_diaria),0),
			nvl(max(b.nr_sequencia),0)
	into	qt_diaria_w,
			nr_seq_material_w
	from	nut_pac_elem_mat b,
			nut_pac_elemento a
	where	a.nr_sequencia	= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_zinco_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p;

	select	nvl(max(c.qt_conversao_ml),0)
	into	qt_conversao_ml_w
	from	nut_elem_material c,
			nut_pac_elem_mat b,
			nut_pac_elemento a
	where	b.nr_seq_elem_mat	= c.nr_sequencia
	and		a.nr_sequencia		= b.nr_seq_pac_elem
	and		a.nr_seq_elemento	= nr_seq_zinco_w
	and		a.nr_seq_nut_pac	= nr_sequencia_p
	and		nvl(c.ie_tipo,'NPT')	= 'NPT';

/* Atualizar dados do pot�ssio */
	/*Tirar do total de mEq do S�dio, os Meqs de S�dio que vieram do F�SFORO org�nico e do Bicarbonato.
	S� ent�o Gerar os mls do produto do S�dio, geralmente Cloreto de S�dio.*/
		
	if (ie_arredonda_npt_w = 'S') then
		qt_diaria_w	:= qt_diaria_zinco_w;
	else
		qt_diaria_w	:= qt_diaria_sodio_w - qt_fosforo_w;
	end if;
	
	--qt_diaria_w	:= qt_diaria_w - qt_pot_clor_fosforo_w;
	if	(qt_diaria_w < 0) then
		qt_diaria_w		:= 0;
	end if;
	
	update	nut_pac_elemento
	set		qt_diaria	= qt_diaria_w
	where	nr_seq_nut_pac	= nr_sequencia_p
	and		nr_seq_elemento	= nr_seq_zinco_w;	

	if (ie_arredonda_npt_w = 'S') then
		qt_volume_w	:= (qt_diaria_w - qt_fosforo_w) * qt_conversao_ml_w;
		--qt_volume_w := ceil(qt_volume_w * 100) / 100;
		qt_volume_w := round(qt_volume_w, 2);
		
		if (qt_volume_w < 0) then
			qt_volume_w	:= 0;
		end if;
	else
		qt_volume_w	:= qt_diaria_w * qt_conversao_ml_w;
	end if;
		
	update	nut_pac_elem_mat
	set		qt_volume	= qt_volume_w
	where	nr_sequencia	= nr_seq_material_w;
end if;

/* Calcular a necessidade de Heparina da NPT */
/* Obtem se com base em 1 MEQ por ml da mesma */
select 	nvl(max(a.nr_sequencia),0)
into	nr_seq_heparina_w
from	nut_elemento b,
		nut_pac_elemento a
where	b.ie_tipo_elemento	= 'H'
and		a.nr_seq_elemento	= b.nr_sequencia
and		a.nr_seq_nut_pac	= nr_sequencia_p;

if	(nr_seq_heparina_w > 0) then
	select	nvl(sum(obter_vol_elem_nut_pac(nr_sequencia)),0)
	into	qt_vol_total_w	
	from	nut_pac_elemento
	where	nr_seq_nut_pac		= nr_sequencia_p;
	
	update	nut_pac_elemento
	set		qt_diaria		= qt_vol_total_w,
			qt_elem_kg_dia		= 0
	where	nr_sequencia		= nr_seq_heparina_w;

	select	nvl(max(c.qt_conversao_ml),0)
	into	qt_conversao_ml_w
	from	nut_elem_material c,
			nut_pac_elem_mat b,
			nut_pac_elemento a
	where	b.nr_seq_elem_mat	= c.nr_sequencia
	and		b.nr_seq_pac_elem	= a.nr_sequencia
	and		a.nr_sequencia	= nr_seq_heparina_w
	and		nvl(c.ie_tipo,'NPT')	= 'NPT';

	qt_volume_w	:= qt_vol_total_w * qt_conversao_ml_w;
	
	if (ie_arredonda_npt_w = 'S') then
		--qt_volume_w	:= ceil(qt_volume_w * 100) / 100;
		qt_volume_w	:= round(qt_volume_w, 2);
	end if;
	
	update	nut_pac_elem_mat
	set		qt_volume		= qt_volume_w
	where	nr_seq_pac_elem	= nr_seq_heparina_w;
end if;

/* Aplicar o fator de corre��o em fun��o do volume do equipo */
if	(ie_correcao_w = 'S') and
	(qt_equipo_w > 0) then
	select	nvl(sum(obter_vol_elem_nut_pac(nr_sequencia)),0)
	into	qt_vol_total_w	
	from	nut_pac_elemento
	where	nr_seq_nut_pac	= nr_sequencia_p;
			
	if (ie_arredonda_npt_w = 'S') then
		qt_fator_correcao_w := ceil(dividir_sem_round((qt_aporte_hidrico_diario_w + qt_equipo_w), qt_aporte_hidrico_diario_w) * 10000) / 10000;		
	else
		qt_fator_correcao_w := dividir_sem_round((qt_aporte_hidrico_diario_w + qt_equipo_w), qt_aporte_hidrico_diario_w);
	end if;
	
	open C05;
	loop
	fetch C05	into
		nr_seq_elemento_w,
		nr_seq_elem_mat_w,
		qt_volume_w;	
	exit when C05%notfound;
		
		if (ie_arredonda_npt_w = 'S') then						
			--qt_volume_w		:= ceil(qt_volume_w * 100) / 100;
			qt_volume_w		:= round(qt_volume_w, 2);
			qt_vol_cor_w	:= qt_volume_w * qt_fator_correcao_w;
			--qt_vol_cor_w	:= ceil(qt_vol_cor_w * 100) / 100;
			qt_vol_cor_w	:= round(qt_vol_cor_w, 2);
		else
			qt_vol_cor_w	:= (qt_volume_w * qt_fator_correcao_w);
		end if; 
		
		update	nut_pac_elem_mat
		set		qt_vol_cor	= qt_vol_cor_w
		where	nr_sequencia	= nr_seq_elem_mat_w;
							
	end loop;
	close C05;
	
	if (ie_arredonda_npt_w = 'S') then	
		select	max(a.nr_sequencia)
		into	nr_seq_elem_agua_w
		from	nut_elemento b,
				nut_pac_elemento a
		where	a.nr_seq_nut_pac	= nr_sequencia_p
		and		a.nr_seq_elemento	= b.nr_sequencia
		and		b.ie_tipo_elemento	= 'A';
	
		select	sum(b.qt_vol_cor)
		into	qt_volume_total_w
		from	nut_pac_elem_mat b,
				nut_pac_elemento a
		where	a.nr_seq_nut_pac	= nr_sequencia_p
		and		a.nr_sequencia		= b.nr_seq_pac_elem
		and 	a.nr_sequencia		<> nr_seq_elem_agua_w
		and		nvl(b.qt_volume,0)	> 0;
						
		qt_vol_cor_w	:= trunc((qt_aporte_hidrico_diario_w + qt_equipo_w) - qt_volume_total_w,2);
				
		update	nut_pac_elem_mat
		set		qt_vol_cor	= qt_vol_cor_w
		where	nr_seq_pac_elem	= nr_seq_elem_agua_w;		
	end if;	
else
	update	nut_pac_elem_mat
	set		qt_vol_cor	= qt_volume
	where	nr_seq_pac_elem in (
		select nr_sequencia
		from	nut_pac_elemento
		where	nr_seq_nut_pac	= nr_sequencia_p);
end if;

/* Dividir o volume do elemento para cada uma das fases que o elemento define */
open c03;
loop
fetch c03	into
	nr_seq_elem_w,
	ie_prim_fase_w,
	ie_seg_fase_w,
	ie_terc_fase_w,
	ie_quar_fase_w;	
exit when C03%notfound;
	qt_fase_w	:= 0;
	if	(ie_prim_fase_w = 'S') then
		qt_fase_w	:= 1;
	end if;
	if	(ie_seg_fase_w = 'S') then
		qt_fase_w	:= qt_fase_w + 1;
	end if;
	if	(ie_terc_fase_w = 'S') then
		qt_fase_w	:= qt_fase_w + 1;
	end if;
	if	(ie_quar_fase_w = 'S') then
		qt_fase_w	:= qt_fase_w + 1;
	end if;	
	
	open c04;
	loop
	fetch c04	into
		qt_volume_w,
		nr_seq_elem_mat_w;	
	exit when c04%notfound;
		qt_fase1_w	:= null;
		qt_fase2_w	:= null;
		qt_fase3_w	:= null;
		qt_fase4_w	:= null;		
		if	(qt_volume_w > 0) then
			qt_volume_w	:= dividir(qt_volume_w, qt_fase_w);
		end if;
		if	(ie_prim_fase_w = 'S') then
			qt_fase1_w	:= qt_volume_w;
		end if;
		if	(ie_seg_fase_w = 'S') then
			qt_fase2_w	:= qt_volume_w;
		end if;
		if	(ie_terc_fase_w = 'S') then
			qt_fase3_w	:= qt_volume_w;
		end if;	
		if	(ie_quar_fase_w = 'S') then
			qt_fase4_w	:= qt_volume_w;
		end if;	
		
		update	nut_pac_elem_mat
		set		qt_vol_1_fase	= qt_fase1_w,
				qt_vol_2_fase	= qt_fase2_w,
				qt_vol_3_fase	= qt_fase3_w,
				qt_vol_4_fase	= qt_fase4_w
		where	nr_sequencia	= nr_seq_elem_mat_w;

	end loop;
	close c04;

end loop;
close c03;

select	nvl(sum(c.qt_volume),0)
into	qt_volume_total_w
from	nut_pac_elem_mat c,
		nut_pac_elemento b,
		nut_pac a
where	a.nr_sequencia	= nr_sequencia_p
and		a.nr_sequencia	= b.nr_seq_nut_pac
and		b.nr_sequencia	= c.nr_seq_pac_elem;

select	nvl(max(nr_horas_validade),0)
into	nr_horas_validade_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_w; 

select	nvl(sum(obter_vol_elem_nut_pac(nr_sequencia)),0)
into	qt_vol_total_w
from	nut_pac_elemento
where	nr_seq_nut_pac	= nr_sequencia_p;

if	(ie_calculo_auto_w	<> 'N') then
	qt_gotejo_w		:= round(dividir(qt_volume_total_w,nvl(qt_hora_inf_w,nr_horas_validade_w)),nr_casas_gotejo_w);
end if;	

-- fator c�lcio = 0,25
-- fator f�sforo = 0,323 
/* F�sforo */
select	nvl(sum(a.qt_elem_kg_dia),0)
into	qt_fosforo_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'F';

/* c�lcio */
select	nvl(sum(a.qt_elem_kg_dia),0)
into	qt_calcio_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'I';

if	(ie_geracao_fos_cal_w = 'S') then
	qt_rel_cal_fosforo_w	:= qt_rel_cal_fos_w;
elsif	(qt_fosforo_w > 0) and
		(qt_calcio_w > 0) then
	qt_rel_cal_fosforo_w	:= Dividir(qt_calcio_w, qt_fosforo_w);
end if;

if (nvl(ie_alterou_volume_p,'N') = 'N') then
	if (ie_arredonda_npt_w = 'S') then
		update	nut_pac_elemento 
		set		pr_concentracao	= dividir((qt_diaria * 100), qt_aporte_hidrico_diario_w),
				--qt_volume_final = ceil(obter_vol_elem_nut_pac(nr_sequencia) * 100) / 100
				qt_volume_final = round(obter_vol_elem_nut_pac(nr_sequencia), 2)
		where	nr_seq_nut_Pac	= nr_sequencia_p
		and		cd_unidade_medida like '%g%';
	else
		update	nut_pac_elemento 
		set		pr_concentracao	= dividir((qt_diaria * 100), qt_aporte_hidrico_diario_w),
				qt_volume_final = obter_vol_elem_nut_pac(nr_sequencia)
		where	nr_seq_nut_Pac	= nr_sequencia_p
		and		cd_unidade_medida like '%g%';
	end if;
else
	if (ie_arredonda_npt_w = 'S') then
		update	nut_pac_elemento 
		set		pr_concentracao	= dividir((qt_diaria * 100), qt_aporte_hidrico_diario_w),
				--qt_volume_final = ceil(obter_vol_elem_nut_pac(nr_sequencia) * 100) / 100
				qt_volume_final = round(obter_vol_elem_nut_pac(nr_sequencia), 2)
		where	nr_seq_nut_Pac	= nr_sequencia_p
		and		cd_unidade_medida like '%g%'
		and		obter_tipo_elemento(nr_seq_elemento) <> 'C';
	else
		update	nut_pac_elemento 
		set		pr_concentracao	= dividir((qt_diaria * 100), qt_aporte_hidrico_diario_w),
				qt_volume_final = obter_vol_elem_nut_pac(nr_sequencia)
		where	nr_seq_nut_Pac	= nr_sequencia_p
		and		cd_unidade_medida like '%g%'
		and		obter_tipo_elemento(nr_seq_elemento) <> 'C';		
	end if;
end if;

if (ie_arredonda_npt_w = 'S') then
	update	nut_pac_elemento
	set		pr_concentracao	= 0,
			--qt_volume_final = ceil(obter_vol_elem_nut_pac(nr_sequencia) * 100) / 100
			qt_volume_final = round(obter_vol_elem_nut_pac(nr_sequencia), 2)
	where	nr_seq_nut_pac	= nr_sequencia_p
	and		cd_unidade_medida not like '%g%';
else
	update	nut_pac_elemento
	set		pr_concentracao	= 0,
			qt_volume_final = Obter_vol_elem_nut_pac(NR_SEQUENCIA)
	where	nr_seq_nut_pac	= nr_sequencia_p
	and		cd_unidade_medida not like '%g%';
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

select	sum(qt_volume_final)
into	qt_volume_final_w
from	nut_pac_elemento
where	nr_seq_nut_Pac	= nr_sequencia_p;

if (ie_arredonda_npt_w = 'S') then
	--qt_volume_final_w	:= ceil(qt_volume_final_w * 100) / 100;
	qt_volume_final_w	:= round(qt_volume_final_w, 2);
end if;

--- Quantidade de Carboidratos
select	nvl(sum(a.qt_kcal),0)
into	qt_kcal_carboidrato_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'C'
and 	nvl(a.ie_prod_adicional,'N') = 'N';

--- Quantidade de Prote�na
select	nvl(sum(qt_kcal),0)
into	qt_kcal_proteina_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'P'
and 	nvl(a.ie_prod_adicional,'N') = 'N';

--- Quantidade de L�pidio
select	nvl(sum(qt_kcal),0)
into	qt_kcal_lipidio_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'L'
and 	nvl(a.ie_prod_adicional,'N') = 'N';

--- Quantidade total dos elementos
select	nvl(sum(qt_kcal),0)
into	qt_kcal_totais_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento in ('C','P','L')
and		nvl(a.ie_prod_adicional,'N') = 'N';

pr_proteina_w					:= dividir((qt_kcal_proteina_w * 100),qt_kcal_totais_w);
pr_lipidio_w					:= dividir((qt_kcal_lipidio_w * 100),qt_kcal_totais_w);
pr_carboidrato_w				:= dividir((qt_kcal_carboidrato_w * 100),qt_kcal_totais_w);

select	nvl(sum(qt_diaria),0)
into	qt_diaria_calcio_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'I';
	
if	(qt_diaria_calcio_w > 0) then
	select	nvl(sum(qt_volume_final),0)
	into	qt_vol_total_w	
	from	nut_pac_elemento
	where	nr_seq_nut_pac		= nr_sequencia_p;
	qt_conc_calcio_w	:= dividir((qt_diaria_calcio_w * 1000), qt_vol_total_w);
end if;

update	nut_pac
set		qt_gotejo_npt		= qt_gotejo_w,
		qt_conc_calcio		= qt_conc_calcio_w,
		qt_rel_cal_fos		= qt_rel_cal_fosforo_w,
		pr_carboidrato		= nvl(pr_carboidrato_w,0),
		pr_lipidio			= nvl(pr_lipidio_w,0),
		pr_proteina			= nvl(pr_proteina_w,0),
		qt_volume_diario	= qt_volume_final_w
where	nr_sequencia		= nr_sequencia_p; 


if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

calcula_osmolaridade_npt_ped(nr_sequencia_p);

end Calcular_Nut_Pac_Ped;
/