create or replace procedure gerar_solic_compra_repasse(   nm_usuario_p		    varchar2,
                                        nr_repasse_terceiro_p   number) is

nr_solic_compra_w       repasse_terceiro_venc.nr_solic_compra%type;
cd_pessoa_fisica_w      varchar2(10);
cd_setor_atendimento_w  varchar2(10);
cd_centro_custo_w       setor_atendimento.cd_centro_custo%type;
cd_cgc_w                terceiro.cd_cgc%type;
cd_pf_terc_w            terceiro.cd_pessoa_fisica%type;

begin

select cd_pessoa_fisica,
    cd_setor_atendimento
into cd_pessoa_fisica_w,
    cd_setor_atendimento_w
from usuario_v
where nm_usuario = nm_usuario_p;

if  (cd_setor_atendimento_w is not null) then
    select cd_centro_custo
    into cd_centro_custo_w
    from setor_atendimento
    where cd_setor_atendimento = cd_setor_atendimento_w;
end if;

select solic_compra_seq.nextval
into nr_solic_compra_w
from dual;

select b.cd_cgc,
    b.cd_pessoa_fisica
into cd_cgc_w,
    cd_pf_terc_w
from repasse_terceiro a,
    terceiro b
where a.nr_seq_terceiro = b.nr_sequencia
and nr_repasse_terceiro = nr_repasse_terceiro_p;

insert into solic_compra (
    cd_estabelecimento,
    cd_pessoa_solicitante,
    dt_atualizacao,
    dt_solicitacao_compra,
    ie_aviso_aprov_oc,
    ie_situacao,
    ie_urgente,
    nm_usuario,
    nr_repasse_terceiro,
    nr_solic_compra,
    ie_tipo_servico,
    cd_centro_custo,
    cd_fornec_sugerido,
    cd_pessoa_fisica
) values (
    wheb_usuario_pck.get_cd_estabelecimento,
    cd_pessoa_fisica_w,
    sysdate,
    sysdate,
    'N',
    'A',
    'N',
    nm_usuario_p,
    nr_repasse_terceiro_p,
    nr_solic_compra_w,
    'SP',
    cd_centro_custo_w,
    cd_cgc_w,
    cd_pf_terc_w
);

update repasse_terceiro_venc
set nr_solic_compra = nr_solic_compra_w
where nr_repasse_terceiro = nr_repasse_terceiro_p;

commit;

end gerar_solic_compra_repasse;
/