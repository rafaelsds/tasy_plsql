create or replace
procedure liberar_proposta_aquisicao(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 

nm_usuario_destino_w			varchar2(15);
nm_usuario_origem_w			varchar2(15);
nm_solicitante_proposta_w		varchar2(255);
nm_aprovador_w				varchar2(255);
ds_comunicado_w				varchar2(4000);
nr_seq_classif_w			number(10);
nr_solic_compra_w			number(10);
nr_seq_comunic_w			number(10);
					
begin

select	obter_usuario_pessoa(cd_solicitante_proposta) nm_usuario_origem,
	obter_usuario_pessoa(cd_aprovador) nm_usuario_destino,	
	substr(obter_nome_pf(cd_solicitante_proposta),1,255),
	substr(obter_nome_pf(cd_aprovador),1,255),
	nr_solic_compra
into	nm_usuario_origem_w,
	nm_usuario_destino_w,
	nm_solicitante_proposta_w,
	nm_aprovador_w,
	nr_solic_compra_w
from	solic_compra_proposta
where	nr_sequencia = nr_sequencia_p;

update	solic_compra_proposta
set	dt_liberacao	= sysdate,
	nm_usuario_lib	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

select	obter_classif_comunic('F')
into	nr_seq_classif_w
from	dual;

ds_comunicado_w	:=	substr(WHEB_MENSAGEM_PCK.get_texto(303076,'NM_APROVADOR_W='|| NM_APROVADOR_W ||';NR_SOLIC_COMPRA_W='|| NR_SOLIC_COMPRA_W),1,4000);
			/*substr(	'Prezado ' || nm_aprovador_w || ',' || chr(13) || chr(10) || 
				'Existe uma nova proposta de aquisi��o da solicita��o de compras n�mero ' || nr_solic_compra_w || ' para a sua an�lise.' || chr(13) || chr(10) || 
				'Essa proposta se encontra na pasta Proposta aquisi��o da fun��o Solicita��o de compras.' || chr(13) || chr(10) || chr(13) || chr(10) || 
				'Atenciosamente, ' || chr(13) || chr(10) || 
				nm_solicitante_proposta_w,1,4000);*/

select	comunic_interna_seq.nextval
into	nr_seq_comunic_w
from	dual;

insert into comunic_interna(
	dt_comunicado,
	ds_titulo,
	ds_comunicado,
	nm_usuario,
	dt_atualizacao,
	ie_geral,
	nm_usuario_destino,
	nr_sequencia,
	ie_gerencial,
	nr_seq_classif,
	dt_liberacao,
	ds_perfil_adicional,
	ds_setor_adicional)
values(	sysdate,
	Wheb_mensagem_pck.get_Texto(303077), /*'Nova proposta aquisi��o para an�lise',*/
	ds_comunicado_w,
	nm_usuario_origem_w,
	sysdate,
	'N',
	nm_usuario_destino_w,
	nr_seq_comunic_w,
	'N',
	nr_seq_classif_w,
	sysdate,
	'',
	'');
				
insert into comunic_interna_lida(
	nr_sequencia,
	nm_usuario,
	dt_atualizacao)
values(	nr_seq_comunic_w,
	nm_usuario_origem_w,
	sysdate);

commit;

end liberar_proposta_aquisicao;
/
