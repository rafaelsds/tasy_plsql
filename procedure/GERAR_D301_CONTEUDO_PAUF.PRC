create or replace
procedure gerar_d301_conteudo_PAUF(nr_seq_dataset_p	number,
				   nm_usuario_p		Varchar2) is


/*Gerar e armazenar o conte�do (com separadores) deste dataset no campo DS_CONTEUDO da tabela D301_DATASET_CONTEUDO*/

ds_segmento_w		varchar2(32767);
ds_segmento_clob_w	clob := empty_clob;

cursor c01 is
select 	('EAD+'||
	CD_CID_ADMISSAO_1||decode(NR_SEQ_301_LOCAL_ADM_1,null,null,':')||
	obter_descricao_padrao('C301_16_LOCALIZACAO','IE_LOCALIZACAO',NR_SEQ_301_LOCAL_ADM_1)||'+'||
	CD_CID_ADMISSAO_2||decode(NR_SEQ_301_LOCAL_ADM_2,null,null,':')||
	obter_descricao_padrao('C301_16_LOCALIZACAO','IE_LOCALIZACAO',NR_SEQ_301_LOCAL_ADM_2)||'+'||
	CD_CID_REFERENCIA_1||decode(NR_SEQ_301_LOCAL_REF_1,null,null,':')||
	obter_descricao_padrao('C301_16_LOCALIZACAO','IE_LOCALIZACAO',NR_SEQ_301_LOCAL_REF_1)||'+'||
	CD_CID_REFERENCIA_2||decode(NR_SEQ_301_LOCAL_REF_2,null,null,':')||
	obter_descricao_padrao('C301_16_LOCALIZACAO','IE_LOCALIZACAO',NR_SEQ_301_LOCAL_REF_2)||'+'||chr(39)) ds_segmento
into	ds_segmento_w
FROM 	D301_SEGMENTO_EAD
where	nr_seq_dataset	= nr_seq_dataset_p;

c01_w	c01%rowtype;

begin

begin
--cabecalho
select  'UNH+'||
	lpad(nr_ref_segmento,5,'0')||'+'||
	ie_dataset||':'||
	ie_versao_dataset||':'||
	nr_release_dataset||':'||
	nr_versao_organiz||chr(39)
into  	ds_segmento_w
from  	d301_segmento_unh
where 	nr_seq_dataset  = nr_seq_dataset_p;
exception
when others then
	ds_segmento_w := null;
end;

select  concat(ds_segmento_clob_w,ds_segmento_w)
into  	ds_segmento_clob_w
from  	dual;

/*
begin
select  'FKT+'||
	nr_seq_301_ident_processo||'+'||
	nr_transacao||'+'||
	nr_ik_origem||'+'||
	nr_ik_destino||chr(39)
into  	ds_segmento_w
from  	d301_segmento_fkt
where  	nr_seq_dataset  = nr_seq_dataset_p;
exception
when others then
	ds_segmento_w := null;
end;*/

OBTER_CONTEUDO_SEG_FKT_301( nr_seq_dataset_p, ds_segmento_w );

select  concat(ds_segmento_clob_w, ds_segmento_w)
into  	ds_segmento_clob_w
from  	dual;

begin
select  'PNV+'||
	cd_usuario_convenio||'+'||
	cd_identificacao_pessoa||'+'||
	ds_dt_validade||'+'||
	nr_ik_hospital||'+'||
	nr_episodio_convenio||'+'||
	nr_arquivo_convenio||'+'||
	ds_dt_inicio_cobert_conv||'+'||
	nr_contrato_conv||'+'||
	ds_nome||'+'||
	ds_primeiro_nome||chr(39)	
into  	ds_segmento_w
from  	d301_segmento_pnv
where  	nr_seq_dataset  = nr_seq_dataset_p;
exception
when others then
	ds_segmento_w := null;
end;

select  concat(ds_segmento_clob_w, ds_segmento_w)
into  	ds_segmento_clob_w
from  	dual;

begin
select  'NAD+'||
	nm_sobrenome||'+'||
	ds_nome||'+'||
	obter_descricao_padrao('C301_21_SEXO','IE_SEXO',nr_seq_301_sexo)||'+'||
	ds_dt_nascimento||'+'||
	ds_rua_numero||'+'||
	cd_postal||'+'||
	ds_cidade||'+'||
	ds_titulacao||'+'||
	obter_descricao_padrao('C301_7_PAIS','IE_PAIS',nr_seq_301_pais)||'+'||
	ds_sufixo_nome||'+'||
	ds_prefixo_sobrenome||'+'||
	ds_compl_endereco||chr(39)
into	ds_segmento_w
from	d301_segmento_nad
where	nr_seq_dataset	= nr_seq_dataset_p;
exception
when others then
	ds_segmento_w := null;
end;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

begin
select 'DPV+'||
	CD_VERSAO_CID||'+'||
	CD_VERSAO_OPS||'+'||chr(39)
into	ds_segmento_w
FROM 	D301_SEGMENTO_DPV
where	nr_seq_dataset	= nr_seq_dataset_p;
exception
when others then
	ds_segmento_w := null;
end;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

begin
SELECT 'AUF+'||
	DS_DT_ADMISSAO||'+'||
	DS_HORA_ADMISSAO||'+'||
	obter_descricao_padrao('C301_1_MOTIVO_ADM_MOTIVO','IE_MOTIVO',NR_SEQ_301_MOT_ADM_MOT)|| obter_descricao_padrao('C301_1_MOTIVO_ADM_TIPO','IE_TIPO',NR_SEQ_301_MOT_ADM_MOT)||'+'||
	obter_descricao_padrao('C301_6_DEPARTAMENTO','CD_DEPARTAMENTO',NR_SEQ_301_DEPARTMENT)||'+'||
	DS_DT_PREVISTA_SAIDA||'+'||
	NR_MEDICO_REFERENCIA||'+'||
	NR_ESTAB_MEDICO_REF||'+'||
	NR_IK_HOSP_ORIGEM||'+'||
	DS_SETOR_ADM_EMERG||'+'||
	NR_DENTISTA_REFERENCIA||'+'||
	QT_PESO_KG_ADMISSAO||'+'||chr(39)
into	ds_segmento_w
FROM 	D301_SEGMENTO_AUF
where	nr_seq_dataset	= nr_seq_dataset_p;
exception
when others then
	ds_segmento_w := null;
end;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

--EAD
open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	select	concat(ds_segmento_clob_w, c01_w.ds_segmento)
	into	ds_segmento_clob_w
	from	dual;

end loop;
close C01;

begin
select  'PVA+'||
	'0+'||
	IE_MEDICO_ELETIVO||'+'||
	obter_descricao_padrao('C301_TIPO_ACOMODACAO','IE_ACOMODACAO',NR_SEQ_301_TIPO_ACOMOD)||'+'||
	IE_MEDICO_FAMILIA||'+'||
	IE_ACOMPANHANTE||'+'||
	NR_IK_PAGADOR||'+'||
	NM_PAGADOR||'+'||
	NR_REGISTRO||chr(39)
into	ds_segmento_w
from	d301_segmento_pva
where	nr_seq_dataset	= nr_seq_dataset_p;
exception
when others then
	ds_segmento_w := null;
end;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

--rodape
begin
select  'UNT+'||
	qt_segmento||'+'||
	lpad(nr_ref_segmento,5,'0')||chr(39)
	into  ds_segmento_w
from  	d301_segmento_unt
where  	nr_seq_dataset  = nr_seq_dataset_p;
exception
when others then
	ds_segmento_w := null;
end;

select  concat(ds_segmento_clob_w, ds_segmento_w)
into  ds_segmento_clob_w
from  dual;


inserir_d301_dataset_conteudo(nm_usuario_p, nr_seq_dataset_p, ds_segmento_clob_w);


end gerar_d301_conteudo_PAUF;
/