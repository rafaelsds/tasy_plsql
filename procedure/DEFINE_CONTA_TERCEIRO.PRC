create or replace
procedure Define_Conta_Terceiro(nr_seq_pep_med_fatur_p	number,
				nm_usuario_p		Varchar2) is 

dt_liberacao_w		date;	
qt_conta_aberta_w		number(10,0);	
nr_seq_conta_w		number(10,0);	
dt_faturamento_w	date;
ie_permite_valor_w	varchar2(1);
nr_atendimento_w	number(10,0);
nr_seq_conta_terceiro_w	number(10,0);
ie_trat_conta_rn_w	varchar2(15);
nr_atendimento_mae_w	number(10,0);
				 
begin

ie_permite_valor_w:= 'N';
begin
select 	dt_faturamento,
	nvl(Obter_se_permite_valor(nr_seq_regra_cobranca),'N'),
	nr_atendimento,
	dt_liberacao,
	nvl(nr_seq_conta_terceiro,0)
into	dt_faturamento_w,
	ie_permite_valor_w,
	nr_atendimento_w,
	dt_liberacao_w,
	nr_seq_conta_terceiro_w
from 	pep_med_fatur
where	nr_sequencia = nr_seq_pep_med_fatur_p;
exception
	when others then
	dt_faturamento_w:= null;
	ie_permite_valor_w:= 'N';
	nr_atendimento_w:= 0;
	dt_liberacao_w:= null;
	nr_seq_conta_terceiro_w:= 0;
end;	

if	(ie_permite_valor_w = 'S') and
	(nr_seq_conta_terceiro_w = 0) then
	
	select 	max(ie_trat_conta_rn),
		max(nr_atendimento_mae)
	into	ie_trat_conta_rn_w,
		nr_atendimento_mae_w
	from 	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;
	
	if	(ie_trat_conta_rn_w = 'M�e') and (nvl(nr_atendimento_mae_w,0) > 0) then
		nr_atendimento_w := nr_atendimento_mae_w;
	end if;
	
	-- Ver se tem alguma conta aberta para o m�s
	select	count(*)
	into	qt_conta_aberta_w
	from	conta_terceiro
	where 	trunc(dt_mesano_referencia,'month') = trunc(dt_faturamento_w,'month')
	and 	nr_atendimento = nr_atendimento_w
	and 	ie_status = 1;

	if	(qt_conta_aberta_w > 0) then		
	
		select	max(nr_sequencia)		
		into	nr_seq_conta_w
		from	conta_terceiro
		where 	trunc(dt_mesano_referencia,'month') = trunc(dt_faturamento_w,'month')
		and 	nvl(nr_atendimento,nr_atendimento_w) = nr_atendimento_w
		and 	ie_status = 1;
		
		update	pep_med_fatur
		set	nr_seq_conta_terceiro = nr_seq_conta_w
		where 	nr_sequencia = nr_seq_pep_med_fatur_p;
		
	else
	

		select 	conta_terceiro_seq.nextVal
		into	nr_seq_conta_w
		from 	dual;
				
		insert into conta_terceiro
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec, 
			nm_usuario_nrec,
			dt_mesano_referencia,
			nr_atendimento,
			ie_status)
		values 	(nr_seq_conta_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			trunc(dt_faturamento_w,'month'),
			nr_atendimento_w,
			1);

		update	pep_med_fatur
		set	nr_seq_conta_terceiro = nr_seq_conta_w
		where 	nr_sequencia = nr_seq_pep_med_fatur_p;
	
	end if;
	
end if;

commit;

end Define_Conta_Terceiro;
/