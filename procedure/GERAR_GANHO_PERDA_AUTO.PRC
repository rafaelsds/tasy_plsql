create or replace
procedure Gerar_Ganho_perda_auto(
			nm_usuario_p		Varchar2) is 


nr_atendimento_w	number(10);
qt_sc_w			number(15,2);	
cd_setor_atendimento_w	number(10);	
nr_seq_tipo_w		number(10);
qt_volume_w		number(15,4);	
nr_sequencia_w		number(10);
			
Cursor C01 is
	select	a.nr_atendimento,
		a.cd_setor_atendimento
	from	unidade_atendimento a
	where	a.nr_atendimento is not null
	and	exists	(	select	1
				from	GANHO_PERDA_AUTO x
				where	x.cd_setor_atendimento	= a.cd_setor_atendimento);
		
Cursor C02 is
	select	nr_seq_tipo,
		qt_volume
	from	GANHO_PERDA_AUTO
	where	cd_setor_atendimento	= cd_setor_atendimento_w
	and	qt_sc_w between QT_SUPERF_CORPORIA_MIN and QT_SUPERF_CORPORIA_MAX;
begin


open C01;
loop
fetch C01 into	
	nr_atendimento_w,
	cd_setor_atendimento_w;
exit when C01%notfound;
	begin
	qt_sc_w		:= obter_sinal_vital(nr_atendimento_w,'SC');
	
	if	(nvl(qt_sc_w,0)	> 0) then
		nr_seq_tipo_w	:= null;
		open C02;
		loop
		fetch C02 into	
			nr_seq_tipo_w,
			qt_volume_w;
		exit when C02%notfound;
		end loop;
		close C02;
		
		if	(nr_seq_tipo_w	is not null)then
			Inserir_ganho_perda(	nr_atendimento_w,
						null,
						nr_seq_tipo_w,
						null,
						sysdate,
						qt_volume_w,
						'S',
						null,
						null,
						nm_usuario_p,
						null,
						1,
						null,
						nr_sequencia_w);
		end if;
		
	end if;
	
	end;
end loop;
close C01;


commit;

end Gerar_Ganho_perda_auto;
/