create or replace
procedure Gerar_w_upc_prescricao(	cd_setor_atendimento_p	number,
					dt_referencia_p		date,
					dt_referencia_fim_p	date,
					nm_usuario_p		varchar2) is 

dt_referencia_w			date;
qt_unidade_acomp_w		number(10,5);
nr_unidades_higienizacao_w	number(10,5);
nr_unidades_interditadas_w	number(10,5);
nr_admissoes_w			number(10,5);
qt_paciente_dia_w		number(10,5);
nr_transf_entrada_w		number(10,5);
nr_tot_entradas_w		number(10,5);
nr_altas_w			number(10,5);
nr_obitos_w			number(10,5);
nr_transf_saida_w		number(10,5);
nr_tot_saidas_w			number(10,5);
qt_ocupacao_w			number(10,5);
nr_leitos_w			number(10,5);
qt_leitos_oper_w		number(10,5);
qt_clinica_medica_w		number(10,5);
qt_clinica_cirurgica_w		number(10,5);
qt_clinica_obstetrica_w		number(10,5);
qt_medica_w			number(10,5);
qt_cirurgica_w			number(10,5);
qt_obstetrica_w			number(10,5);
qt_pediatrica_w			number(10,5);
qt_homecare_w			number(10,5);
qt_exames_cc_w			number(10,5);
qt_ciramb_w			number(10,5);
qt_sdi_w			number(10,5);
qt_admitido_alta_dia_w		number(10,5);
nr_sequencia_w			number(10);
				
Cursor C01 is
	select  a.dt_referencia,
		sum(a.qt_unidade_acomp) qt_unidade_acomp,
		sum(a.nr_unidades_higienizacao) nr_unidades_higienizacao,
		sum(a.nr_unidades_interditadas) nr_unidades_interditadas,
		sum(a.nr_admissoes) nr_admissoes,
		sum(a.qt_paciente_dia) qt_paciente_dia,
		sum(a.nr_transf_entrada) nr_transf_entrada,
		sum(a.nr_tot_entradas) nr_tot_entradas,
		sum(a.nr_altas) nr_altas,
		sum(a.nr_obitos) nr_obitos,
		sum(a.nr_transf_saida) nr_transf_saida,
		sum(a.nr_tot_saidas) nr_tot_saidas,
		sum(b.nr_leitos) nr_leitos,
		sum(((qt_paciente_dia+f.qt_admitido_alta_dia)*100)/(b.nr_leitos-nr_unidades_interditadas-qt_unidade_acomp)) qt_ocupacao,
		sum((b.nr_leitos-nr_unidades_interditadas-qt_unidade_acomp)) qt_leitos_oper,
		sum(c.medica + c.pediatrica) qt_clinica_medica,
		sum(c.cirurgica + c.exames_cc) qt_clinica_cirurgica,
		sum(c.obstetrica) qt_clinica_obstetrica,
		sum(c.medica) qt_medica,
		sum(c.cirurgica) qt_cirurgica,
		sum(c.obstetrica) qt_obstetrica,
		sum(c.pediatrica) qt_pediatrica,
		sum(c.homecare) qt_homecare,
		sum(c.exames_cc) qt_exames_cc,
		sum(c.ciramb) qt_ciramb,
		sum(c.sdi) qt_sdi,
		sum(f.qt_admitido_alta_dia) qt_admitido_alta_dia
from
(
select	trunc(b.dt_referencia,'month') dt_referencia, 
	b.cd_setor_atendimento,
	b.ds_setor_atendimento,
	sum(qt_unidade_acomp) qt_unidade_acomp,
	sum(nr_unidades_higienizacao) nr_unidades_higienizacao,
	sum(nr_unidades_interditadas+qt_unidades_isolamento) nr_unidades_interditadas,
	sum(b.nr_admissoes) nr_admissoes,
	sum(b.nr_leitos_ocupados) qt_paciente_dia,
	sum(b.nr_transf_entrada) nr_transf_entrada,
	(sum(b.nr_admissoes) + sum(b.nr_transf_entrada)) nr_tot_entradas,
	sum(b.nr_altas+nr_transf_saida) nr_altas,
	sum(b.nr_obitos) nr_obitos,
	sum(b.nr_transf_saida) nr_transf_saida,
	(sum(b.nr_altas) + sum(b.nr_obitos) + sum(b.nr_transf_saida)) nr_tot_saidas
from   eis_ocupacao_setor_v b
where   b.ie_periodo = 'D'
and	((b.cd_setor_atendimento = cd_setor_atendimento_p) or (cd_setor_atendimento_p = 0))
and	trunc(b.dt_referencia,'month') between trunc(to_date(dt_referencia_p),'month') and last_day(trunc(to_date(dt_referencia_fim_p),'month'))
group by trunc(b.dt_referencia,'month'),
	b.cd_setor_atendimento,
	b.ds_setor_atendimento
) a,
(
select trunc(b.dt_referencia,'month') dt_referencia,
	b.cd_setor_atendimento,
	sum(qt_admitido_alta_dia) qt_admitido_alta_dia
from eis_censo_diario_v2 b
where   b.ie_periodo = 'D'
and	((b.cd_setor_atendimento = cd_setor_atendimento_p) or (cd_setor_atendimento_p = 0))
and	trunc(b.dt_referencia,'month') between trunc(to_date(dt_referencia_p),'month') and last_day(trunc(to_date(dt_referencia_fim_p),'month'))
group by trunc(b.dt_referencia,'month'),
	b.cd_setor_atendimento
) f,
(
select trunc(dt_referencia,'month') dt_referencia, cd_setor_atendimento, OBTER_UNIDADES_SETOR_DT(CD_SETOR_ATENDIMENTO, DT_REFERENCIA) nr_leitos, count(*) nr_leitos_a
from
(
select distinct trunc(dt_referencia,'month') dt_referencia, cd_setor_atendimento, trim(cd_unidade_basica) cd_unidade_basica, nr_unidades_interditadas
from eis_ocupacao_setor_v b
where	b.ie_periodo = 'D'
and	b.cd_estabelecimento = 1
and	((b.cd_setor_atendimento = cd_setor_atendimento_p) or (cd_setor_atendimento_p = 0))
and	trunc(b.dt_referencia,'month') between trunc(to_date(dt_referencia_p),'month') and last_day(trunc(to_date(dt_referencia_fim_p),'month'))
)
group by dt_referencia, trunc(dt_referencia,'month'), cd_setor_atendimento
) b,
(
select distinct trunc(to_date(ds_referencia),'month') dt_referencia,
       sum(decode(ie_clinica,1,nr_pac_dia,0)) medica, 
       sum(decode(ie_clinica,2,nr_pac_dia,0)) cirurgica, 
       sum(decode(ie_clinica,3,nr_pac_dia,0)) obstetrica, 
       sum(decode(ie_clinica,4,nr_pac_dia,0)) pediatrica, 
       sum(decode(ie_clinica,5,nr_pac_dia,0)) homecare, 
       sum(decode(ie_clinica,6,nr_pac_dia,0)) exames_cc, 
       sum(decode(ie_clinica,7,nr_pac_dia,0)) ciramb, 
       sum(decode(ie_clinica,8,nr_pac_dia,0)) sdi
from eis_censo_diario_v2 b
where   b.ie_periodo = 'D'
and	((b.cd_setor_atendimento = cd_setor_atendimento_p) or (cd_setor_atendimento_p = 0))
and	trunc(b.dt_referencia,'month') between trunc(to_date(dt_referencia_p),'month') and last_day(trunc(to_date(dt_referencia_fim_p),'month'))
group by ds_referencia
) c
where	trunc(a.dt_referencia) = trunc(b.dt_referencia)
and	trunc(a.dt_referencia) = trunc(c.dt_referencia)
and     a.cd_setor_atendimento = b.cd_setor_atendimento
and	trunc(a.dt_referencia) = trunc(f.dt_referencia)
and     a.cd_setor_atendimento = f.cd_setor_atendimento
and     cd_setor_atendimento_p > 0
group by a.dt_referencia,
	 a.cd_setor_atendimento,
	 a.ds_setor_atendimento
order by	1,2;

begin

Delete
from	w_upc_prescricao
where	nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into	
	dt_referencia_w,
	qt_unidade_acomp_w,
	nr_unidades_higienizacao_w,
	nr_unidades_interditadas_w,
	nr_admissoes_w,
	qt_paciente_dia_w,
	nr_transf_entrada_w,
	nr_tot_entradas_w,
	nr_altas_w,
	nr_obitos_w,
	nr_transf_saida_w,
	nr_tot_saidas_w,
	nr_leitos_w,
	qt_ocupacao_w,
	qt_leitos_oper_w,
	qt_clinica_medica_w,
	qt_clinica_cirurgica_w,
	qt_clinica_obstetrica_w,
	qt_medica_w,
	qt_cirurgica_w,
	qt_obstetrica_w,
	qt_pediatrica_w,
	qt_homecare_w,
	qt_exames_cc_w,
	qt_ciramb_w,
	qt_sdi_w,
	qt_admitido_alta_dia_w;
exit when C01%notfound;
	begin
	
	select	w_upc_prescricao_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into w_upc_prescricao(	dt_atualizacao,
					dt_atualizacao_nrec,
					dt_referencia,
					nm_usuario,
					nm_usuario_nrec,
					nr_admissoes,
					nr_altas,
					nr_leitos,
					nr_obitos,
					nr_sequencia,
					nr_tot_entradas,
					nr_tot_saidas,
					nr_transf_entrada,
					nr_transf_saida,
					nr_unidades_higienizacao,
					nr_unidades_interditadas,
					qt_admitido_alta_dia,
					qt_ciramb,
					qt_cirurgica,
					qt_clinica_cirurgica,
					qt_clinica_medica,
					qt_clinica_obstetrica,
					qt_exames_cc,
					qt_homecare,
					qt_leitos_oper,
					qt_medica,
					qt_obstetrica,
					qt_ocupacao,
					qt_paciente_dia,
					qt_pediatrica,
					qt_sdi,
					qt_unidade_acomp)
				values( sysdate,
					sysdate,
					dt_referencia_w,
					nm_usuario_p,
					nm_usuario_p,
					nr_admissoes_w,
					nr_altas_w,
					nr_leitos_w,
					nr_obitos_w,
					nr_sequencia_w,
					nr_tot_entradas_w,
					nr_tot_saidas_w,
					nr_transf_entrada_w,
					nr_transf_saida_w,
					nr_unidades_higienizacao_w,
					nr_unidades_interditadas_w,
					qt_admitido_alta_dia_w,
					qt_ciramb_w,
					qt_cirurgica_w,
					qt_clinica_cirurgica_w,
					qt_clinica_medica_w,
					qt_clinica_obstetrica_w,
					qt_exames_cc_w,
					qt_homecare_w,
					qt_leitos_oper_w,
					qt_medica_w,
					qt_obstetrica_w,
					qt_ocupacao_w,
					qt_paciente_dia_w,
					qt_pediatrica_w,
					qt_sdi_w,
					qt_unidade_acomp_w);
	end;
end loop;
close C01;

commit;

end Gerar_w_upc_prescricao;
/