create or replace
procedure gravar_log_status_protocolo (	nr_seq_medicacao_p	number,
					ds_alteracao_p		varchar2,
					ie_status_p		varchar2,
					nm_usuario_p		Varchar2) is 

begin

insert into protocolo_medic_hist (
	nr_sequencia, 
	dt_atualizacao, 
	nm_usuario, 
	dt_atualizacao_nrec, 
	nm_usuario_nrec, 
	nr_seq_medicacao, 
	ds_alteracao, 
	ie_status)
values (protocolo_medic_hist_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_medicacao_p,
	ds_alteracao_p,
	ie_status_p);

commit;

end gravar_log_status_protocolo;
/