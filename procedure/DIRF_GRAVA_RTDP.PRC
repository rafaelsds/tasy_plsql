create or replace
procedure DIRF_GRAVA_RTDP
			(	nr_seq_lote_dirf_p		number,
				nm_usuario_p		varchar2,
				nr_seq_apres_p		in out	number,
				cd_darf_p		varchar2,
				cd_estabelecimento_p 	number,
				cd_pessoa_fisica_p		varchar2) is 
				
vl_imposto_gps_jan_w		varchar2(20);
vl_imposto_gps_fev_w		varchar2(20);
vl_imposto_gps_mar_w		varchar2(20);
vl_imposto_gps_abr_w		varchar2(20);
vl_imposto_gps_mai_w		varchar2(20);
vl_imposto_gps_jun_w		varchar2(20);
vl_imposto_gps_jul_w		varchar2(20);
vl_imposto_gps_ago_w		varchar2(20);
vl_imposto_gps_set_w		varchar2(20);
vl_imposto_gps_out_w		varchar2(20);	
vl_imposto_gps_nov_w		varchar2(20);
vl_imposto_gps_dez_w		varchar2(20);
vl_imposto_gps_13_w		varchar2(20);
separador_w			varchar2(1)	:= '|';
ds_arquivo_w			varchar2(2000);
qt_registro_w			number(10);
nr_cpf_w				varchar2(11);
nm_pessoa_fisica_w		varchar2(255);
cd_pessoa_fisica_w		varchar2(10);
qt_dependentes_w			number(10);
vl_desc_dependente_w		tributo_conta_pagar.vl_desc_dependente%TYPE;
cd_tributo_w			tributo.cd_tributo%TYPE;
vl_desc_rtdp_w                  		number(15,2);
dt_lote_w			dirf_lote.dt_lote%type;


function obter_valor_dependente	(	dt_mes_ano_p	date,
					qt_dependente_p	number)
				return number is

ds_retorno_w			number(15,2);

begin

begin
select	(vl_deducao * qt_dependente_p) vl_retorno
into	ds_retorno_w
from	fis_ded_dependente_irpf
where	trunc(dt_mes_ano_p, 'mm') between trunc(dt_inicio_vigencia, 'mm') and fim_mes(trunc(nvl(dt_fim_vigencia, sysdate), 'mm')); 

exception
when others then
	ds_retorno_w:= 0;
end;

return	ds_retorno_w;

end obter_valor_dependente;

begin
-- Inserir o registro RTPO - valores da previd�ncia oficial
-- Verificar se h� pessoas f�sicas / t�tulos que s�o do tipo previd�ncia oficial
select	count(*)
into	qt_registro_w
from	titulo_pagar p,
	dirf_titulo_pagar t,
	dirf_lote_mensal d,
	dirf_agrupar_lote a,
	dirf_lote l,
	dirf_regra_tributo r,
	pessoa_fisica_dependente w
where	t.nr_titulo = p.nr_titulo
and	t.nr_seq_lote_dirf = d.nr_sequencia
and	d.nr_sequencia = a.nr_seq_lote_mensal
and	l.nr_sequencia = a.nr_seq_lote_anual
and	t.cd_tributo = r.cd_tributo
and	nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica) = w.cd_pessoa_fisica
and	((t.cd_darf = cd_darf_p)
or	(r.cd_darf_vinculado = cd_darf_p))
and	nvl(r.cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
and 	nvl(r.ie_indica_rendimento,'N') = 'N'
and	nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica) = cd_pessoa_fisica_p
and	l.nr_sequencia = nr_seq_lote_dirf_p
and 	rownum = 1;

if	(qt_registro_w > 0) then -- Caso exista, buscar os dados das pessoas f�sicas dos t�tulos de previd�ncias oficial
	begin
	select	distinct
		substr(obter_dados_pf(nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica),'CPF'),1,11) cpf_pessoa_fisica,
		substr(elimina_caractere_especial(obter_nome_pf_pj(nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica), null)),1,255) nm_pessoa_fisica,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '01', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_jan,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '02', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_fev,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '03', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_mar,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '04', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_abr,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '05', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_mai,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '06', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_jun,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '07', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_jul,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '08', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_ago,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '09', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_set,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '10', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_out,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '11', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_nov,
		replace(substr(campo_mascara(sum(decode(nvl(to_char(nvl(t.dt_base_titulo, d.dt_mes_referencia), 'mm'),'00'), '12', t.vl_imposto, 0)),2),1,20),'.','') vl_imposto_gps_dez,
		replace(substr(campo_mascara(0,2),1,20),'.','') vl_imposto_gps_13,
		trunc(max(l.dt_lote), 'yyyy') dt_lote
	into	nr_cpf_w,
		nm_pessoa_fisica_w,
		vl_imposto_gps_jan_w,
		vl_imposto_gps_fev_w,
		vl_imposto_gps_mar_w,
		vl_imposto_gps_abr_w,
		vl_imposto_gps_mai_w,
		vl_imposto_gps_jun_w,
		vl_imposto_gps_jul_w,
		vl_imposto_gps_ago_w,
		vl_imposto_gps_set_w,
		vl_imposto_gps_out_w,
		vl_imposto_gps_nov_w,
		vl_imposto_gps_dez_w,
		vl_imposto_gps_13_w,
		dt_lote_w
	from	titulo_pagar p,
		dirf_titulo_pagar t,
		dirf_lote_mensal d,
		dirf_agrupar_lote a,
		dirf_lote l,
		dirf_regra_tributo r
	where	t.nr_titulo = p.nr_titulo
	and	t.nr_seq_lote_dirf = d.nr_sequencia
	and	d.nr_sequencia = a.nr_seq_lote_mensal
	and	l.nr_sequencia = a.nr_seq_lote_anual
	and	t.cd_tributo = r.cd_tributo
	--and	r.ie_tipo_imposto = 2 -- Previd�ncia oficial
	and 	nvl(r.ie_indica_rendimento,'N') = 'N'
	and	((t.cd_darf = cd_darf_p)
	or	(r.cd_darf_vinculado = cd_darf_p))
	and	nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica) = cd_pessoa_fisica_p
	and nvl(r.cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
	and	l.nr_sequencia = nr_seq_lote_dirf_p
	group by substr(obter_dados_pf(nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica),'CPF'),1,11),
			substr(elimina_caractere_especial(obter_nome_pf_pj(nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica), null)),1,255)
	order by substr(obter_dados_pf(nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica),'CPF'),1,11);
	exception
	when no_data_found then
		nr_cpf_w		:= null;
		nm_pessoa_fisica_w	:= null;
		vl_imposto_gps_jan_w	:= 0;
		vl_imposto_gps_fev_w	:= 0;
		vl_imposto_gps_mar_w	:= 0;
		vl_imposto_gps_abr_w	:= 0;
		vl_imposto_gps_mai_w	:= 0;
		vl_imposto_gps_jun_w	:= 0;
		vl_imposto_gps_jul_w	:= 0;
		vl_imposto_gps_ago_w	:= 0;
		vl_imposto_gps_set_w	:= 0;
		vl_imposto_gps_out_w	:= 0;
		vl_imposto_gps_nov_w	:= 0;
		vl_imposto_gps_dez_w	:= 0;
		vl_imposto_gps_13_w	:= 0;
	end;
	
	select	max(t.cd_tributo)
	into	cd_tributo_w
	from	tributo	k,
		titulo_pagar p,
		dirf_titulo_pagar t,
		dirf_lote_mensal d,
		dirf_agrupar_lote a,
		dirf_lote l,
		dirf_regra_tributo r,
		pessoa_fisica_dependente w
	where	t.nr_titulo = p.nr_titulo
	and	t.nr_seq_lote_dirf = d.nr_sequencia
	and	d.nr_sequencia = a.nr_seq_lote_mensal
	and	l.nr_sequencia = a.nr_seq_lote_anual
	and	t.cd_tributo = r.cd_tributo
	and	k.cd_tributo = t.cd_tributo
	and	k.ie_tipo_tributo = 'IR'
	and	nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica) = w.cd_pessoa_fisica
	and	((t.cd_darf = cd_darf_p)
	or	(r.cd_darf_vinculado = cd_darf_p))
	and	nvl(r.cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
	and 	nvl(r.ie_indica_rendimento,'N') = 'N'
	and	nvl(t.cd_pessoa_fisica, p.cd_pessoa_fisica) = cd_pessoa_fisica_p
	and	l.nr_sequencia = nr_seq_lote_dirf_p;
	
	select	count(*)
	into	qt_dependentes_w
	from	pessoa_fisica_dependente d
	where	d.cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
        -- Caso n�o tenha dependentes, o registro n�o � gerado.
	if	(qt_dependentes_w > 0) then
	
	        -- Na DIRF  de 2015, nos meses de janeiro a mar�o, � colocado fixo o valor de R$ 179,91, de abril a dezembro o valor � de R$ 189,59, todos multiplicados pela quantidade de dependentes.
		
			/*vl_desc_rtdp_w 	     :=  qt_dependentes_w * 179.91; 			
			vl_desc_dependente_w :=  qt_dependentes_w * 189.59; */
			
			
		
			if	(vl_imposto_gps_jan_w > 0) then
				vl_imposto_gps_jan_w	:= obter_valor_dependente(dt_lote_w, qt_dependentes_w);      
			end if;

			if	(vl_imposto_gps_fev_w > 0) then
				vl_imposto_gps_fev_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 1), qt_dependentes_w);       
			end if;

			if	(vl_imposto_gps_mar_w > 0) then
				vl_imposto_gps_mar_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 2), qt_dependentes_w);   
			end if;

			if	(vl_imposto_gps_abr_w > 0) then
				vl_imposto_gps_abr_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 3), qt_dependentes_w);
			end if;	
			
			if	(vl_imposto_gps_mai_w > 0) then
				vl_imposto_gps_mai_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 4), qt_dependentes_w);
			end if;	
			
			if	(vl_imposto_gps_jun_w > 0) then
				vl_imposto_gps_jun_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 5), qt_dependentes_w);
			end if;	
			
			if	(vl_imposto_gps_jul_w > 0) then
				vl_imposto_gps_jul_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 6), qt_dependentes_w);
			end if;	
			
			if	(vl_imposto_gps_ago_w > 0) then
				vl_imposto_gps_ago_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 7), qt_dependentes_w);
			end if;	

			if	(vl_imposto_gps_set_w > 0) then
				vl_imposto_gps_set_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 8), qt_dependentes_w);
			end if;	

			if	(vl_imposto_gps_out_w > 0) then
				vl_imposto_gps_out_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 9), qt_dependentes_w);
			end if;

			if	(vl_imposto_gps_nov_w > 0) then
				vl_imposto_gps_nov_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 10), qt_dependentes_w);
			end if;	
			
			if	(vl_imposto_gps_dez_w > 0) then
				vl_imposto_gps_dez_w	:= obter_valor_dependente(pkg_date_utils.add_month(dt_lote_w, 11), qt_dependentes_w);
			end if;	
			
			
			vl_imposto_gps_jan_w	:= elimina_caracteres_especiais(vl_imposto_gps_jan_w);
			vl_imposto_gps_fev_w	:= elimina_caracteres_especiais(vl_imposto_gps_fev_w);
			vl_imposto_gps_mar_w	:= elimina_caracteres_especiais(vl_imposto_gps_mar_w);
			vl_imposto_gps_abr_w	:= elimina_caracteres_especiais(vl_imposto_gps_abr_w);
			vl_imposto_gps_mai_w	:= elimina_caracteres_especiais(vl_imposto_gps_mai_w);
			vl_imposto_gps_jun_w	:= elimina_caracteres_especiais(vl_imposto_gps_jun_w);
			vl_imposto_gps_jul_w	:= elimina_caracteres_especiais(vl_imposto_gps_jul_w);
			vl_imposto_gps_ago_w	:= elimina_caracteres_especiais(vl_imposto_gps_ago_w);
			vl_imposto_gps_set_w	:= elimina_caracteres_especiais(vl_imposto_gps_set_w);
			vl_imposto_gps_out_w	:= elimina_caracteres_especiais(vl_imposto_gps_out_w);
			vl_imposto_gps_nov_w	:= elimina_caracteres_especiais(vl_imposto_gps_nov_w);
			vl_imposto_gps_dez_w	:= elimina_caracteres_especiais(vl_imposto_gps_dez_w);
			vl_imposto_gps_13_w	:= elimina_caracteres_especiais(vl_imposto_gps_13_w);
			
			ds_arquivo_w := 'RTDP' 	|| separador_w || vl_imposto_gps_jan_w 	|| separador_w || vl_imposto_gps_fev_w 	|| separador_w || vl_imposto_gps_mar_w
						|| separador_w || vl_imposto_gps_abr_w 	|| separador_w || vl_imposto_gps_mai_w	|| separador_w || vl_imposto_gps_jun_w
						|| separador_w || vl_imposto_gps_jul_w	|| separador_w || vl_imposto_gps_ago_w 	|| separador_w || vl_imposto_gps_set_w
						|| separador_w || vl_imposto_gps_out_w 	|| separador_w || vl_imposto_gps_nov_w 	|| separador_w || vl_imposto_gps_dez_w
						|| separador_w || vl_imposto_gps_13_w   || separador_w;
			
			nr_seq_apres_p := nr_seq_apres_p + 1;
			
			insert into w_dirf_arquivo
				(nr_sequencia,
				nm_usuario,
				nm_usuario_nrec,
				dt_atualizacao,
				dt_atualizacao_nrec,
				ds_arquivo,
				nr_seq_apresentacao,
				nr_seq_registro,
				cd_darf)
			values	(w_dirf_arquivo_seq.nextval,
				nm_usuario_p,
				nm_usuario_p,
				sysdate,
				sysdate,
				ds_arquivo_w,
				to_char(nr_seq_apres_p),
				0,
				cd_darf_p);
				
			commit;
			
			nr_seq_apres_p	:= nr_seq_apres_p + 1;
		
	end if;
end if;

end DIRF_GRAVA_RTDP;
/