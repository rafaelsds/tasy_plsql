create or replace
procedure alterar_obs_item_req_mat(
					cd_material_p		number,
					ds_observacao_p		varchar2,
					nr_requisicao_p		number) is

nr_sequencia_v			number(10,0);
					
begin

select	max(nr_sequencia)
into	nr_sequencia_v
from	item_requisicao_material
where	nr_requisicao	= nr_requisicao_p
and	cd_material	= cd_material_p;

update	item_requisicao_material
set	ds_observacao = ds_observacao_p
where	nr_requisicao  = nr_requisicao_p
and	nr_sequencia   = nr_sequencia_v;

end alterar_obs_item_req_mat;
/