create or replace
procedure med_definir_cid_pedido_exame	(ds_lista_cid_p	varchar2,
						nr_seq_pedido_p	number) is

ds_lista_cid_w	varchar2(4000);
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);
cd_doenca_cid_w	varchar2(10);
ds_doenca_cid_w	varchar2(240);
ds_lista_cd_cid_w	varchar2(4000);
ds_lista_ds_cid_w	varchar2(4000);

begin
if	(ds_lista_cid_p is not null) then
	ds_lista_cid_w := ds_lista_cid_p;

	/* obter c�digo cid */
	while	(ds_lista_cid_w is not null) loop
		begin
		tam_lista_w		:= length(ds_lista_cid_w);
		ie_pos_virgula_w	:= instr(ds_lista_cid_w,',');

		if	(ie_pos_virgula_w <> 0) then
			cd_doenca_cid_w	:= substr(ds_lista_cid_w,1,(ie_pos_virgula_w - 1));
			ds_lista_cid_w	:= substr(ds_lista_cid_w,(ie_pos_virgula_w + 1),tam_lista_w);
		end if;

		/* obter descri��o cid */
		if	(nvl(cd_doenca_cid_w,'0') <> '0') then
			select	ds_doenca_cid
			into	ds_doenca_cid_w
			from	cid_doenca
			where	cd_doenca_cid = cd_doenca_cid_w;

			ds_lista_cd_cid_w := ds_lista_cd_cid_w || cd_doenca_cid_w || ' - ';
			ds_lista_ds_cid_w := ds_lista_ds_cid_w || ds_doenca_cid_w || ' - ';
		end if;
		end;
	end loop;

	/* gerar cid */
	update	med_pedido_exame
	set	ds_cid		   	= substr(ds_lista_cd_cid_w,1,4000),
		ds_diagnostico_cid 	= substr(ds_lista_ds_cid_w,1,4000)
	where	nr_sequencia		= nr_seq_pedido_p;
end if;

commit;

end med_definir_cid_pedido_exame;
/