create or replace procedure HP_CONCILIAR_EXT_CARTAO_TKS (	nr_seq_extrato_arq_p		number,
					ds_lista_extrato_movto_fin_p	varchar2,
					ds_lista_extrato_movto_cred_p	varchar2,
					ie_opcao_p			varchar2,
					nm_usuario_p			varchar2) is

/*	ie_opcao_p
C -- Conciliar
D -- Desconciliar
*/

nr_seq_parcela_w	number(10);
qt_nao_concil_w		number(10);
vl_parcela_w		number(15,2);

ds_comprovante_w	varchar2(100);
ds_comprovante_tasy_w		varchar2(100);
qt_comprovante_w		number(10);

nr_autorizacao_w	varchar2(40);
nr_cartao_w		number(20);
nr_parcela_w		number(10);
nr_resumo_w		varchar2(20);
nr_seq_bandeira_w	number(10);
nr_seq_movto_w		number(10);
nr_seq_parcela_concil_w	number(10);
nr_documento_w		varchar2(100);
cd_bandeira_w		varchar2(3);
vl_saldo_concil_fin_w	number(15,2);
nr_seq_grupo_w		number(10);
vl_min_indevido_w	number(15,2);
vl_max_indevido_w	number(15,2);
nr_seq_extrato_parcela_w	number(10);
nr_seq_extrato_w number(10);
vl_liquido_ext_w number(15,2);
DT_PREV_PAGTO_w date;

cursor	c01 is
select	b.ds_comprovante,
        b.nr_autorizacao,
        somente_numero(b.nr_cartao),
        b.nr_parcela,
        a.nr_resumo,
        a.nr_seq_bandeira,
        b.nr_seq_parcela,
        b.nr_sequencia,
        b.nr_documento,
        c.cd_bandeira,
        b.vl_saldo_concil_fin,
        b.VL_LIQUIDO,
        a.DT_PREV_PAGTO
from bandeira_cartao_cr c,
	 extrato_cartao_cr_movto b,
	 extrato_cartao_cr_res a
where a.nr_seq_bandeira	= c.nr_sequencia(+)
      and (ds_lista_extrato_movto_fin_p is null or ' ' || ds_lista_extrato_movto_fin_p || ' ' like '% ' || b.nr_sequencia || ' %')
      and nvl(b.ie_pagto_indevido,'N')	= 'N'
      and a.nr_sequencia		= b.nr_seq_extrato_res
      and a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p;

begin

select	max(a.nr_seq_grupo),
        max(a.nr_sequencia)
into	nr_seq_grupo_w,
        nr_seq_extrato_w
from extrato_cartao_cr a,
	 extrato_cartao_cr_arq b
where b.nr_seq_extrato	= a.nr_sequencia
      and b.nr_sequencia = nr_seq_extrato_arq_p;

select max(nvl(a.vl_min_indevido,0)),
       max(nvl(a.vl_max_indevido,0))
into vl_min_indevido_w,
	 vl_max_indevido_w
from grupo_band_cr_regra a
where a.nr_seq_grupo = nr_seq_grupo_w;

open	c01;
loop
fetch	c01 into
        ds_comprovante_w,
        nr_autorizacao_w,
        nr_cartao_w,
        nr_parcela_w,
        nr_resumo_w,
        nr_seq_bandeira_w,
        nr_seq_parcela_concil_w,
        nr_seq_movto_w,
        nr_documento_w,
        cd_bandeira_w,
        vl_saldo_concil_fin_w,
        vl_liquido_ext_w,
        DT_PREV_PAGTO_w;
exit	when c01%notfound;
/*-----------------------------------------------------------------------------
            C - Conciliar o extrato e movto_cartao_cr_parcela
-----------------------------------------------------------------------------*/
	if	(ie_opcao_p = 'C') and (nr_seq_parcela_concil_w is null) then

		select	max(b.nr_sequencia)
		into	nr_seq_parcela_w
		from bandeira_cartao_cr c,
			 movto_cartao_cr_parcela b,
			 movto_cartao_cr a
        where  a.nr_seq_bandeira = nr_seq_bandeira_w
--        where  c.cd_bandeira		= nvl(cd_bandeira_w,c.cd_bandeira)
               and	a.nr_seq_bandeira	= c.nr_sequencia
               and	a.nr_sequencia		= b.nr_seq_movto       
               and	a.dt_cancelamento	is null
		and	(ds_lista_extrato_movto_cred_p is null or ' ' || ds_lista_extrato_movto_cred_p || ' ' like '% ' || b.nr_sequencia || ' %')
		and	not exists(select
                             1
                       from	extrato_cartao_cr_movto x
                       where x.nr_seq_parcela	= b.nr_sequencia)
--
        and b.NR_PARCELA = nr_parcela_w
        and b.vl_liquido = vl_liquido_ext_w
        and somente_numero(nvl(a.NR_AUTORIZACAO,0)) = somente_numero(nvl(nr_autorizacao_w,0)) --alterado para Autorizacao.
        and trunc(b.dt_parcela) = trunc(DT_PREV_PAGTO_w); -- data da parcela = data do pagamento

 --and  somente_numero(nvl(a.ds_comprovante,0)) = somente_numero(nvl(ds_comprovante_w,0)); --alterado para Autorizacao.

--and	(nvl(nr_parcela_w,0) = 0 or obter_numero_parcela_cartao(a.nr_sequencia,b.nr_sequencia) = nr_parcela_w)
--and	 obter_numero_parcela_cartao(a.nr_sequencia,b.nr_sequencia) = nr_parcela_w        
--and b.vl_liquido BETWEEN vl_liquido_ext_w - vl_min_indevido_w and vl_liquido_ext_w+vl_max_indevido_w   // causou problema?:)
 --and (b.vl_liquido >= (vl_liquido_ext_w - 0.05) and  b.vl_liquido  <= (vl_liquido_ext_w+0.05))
--and (b.vl_liquido >= (vl_liquido_ext_w - vl_min_indevido_w) and  b.vl_liquido  <= (vl_liquido_ext_w+vl_max_indevido_w))
--CV e valor liquido
		--and	(b.nr_resumo is null or b.nr_resumo = nvl(nr_resumo_w,b.nr_resumo))
        --and	(a.nr_cartao is null or nvl(somente_numero(a.nr_cartao),0) = nvl(nr_cartao_w,nvl(somente_numero(a.nr_cartao),0)))
/*		
        and( somente_numero(ltrim(trim(a.nr_autorizacao),'0'))	= somente_numero(ltrim(trim(nvl(nr_autorizacao_w,a.nr_autorizacao)),'0'))
              or (ds_comprovante_w is null and nr_documento_w is null) 
              or ( somente_numero(nvl(a.ds_comprovante,0)) = somente_numero(nvl(ds_comprovante_w,0)) ) 
              --or ( nvl(a.ds_comprovante,'X') = nvl(ds_comprovante_w,'X')) 
              --or (nvl(a.ds_comprovante,'X') = nr_documento_w) );
              --or( somente_numero(nvl(a.ds_comprovante,0)) = somente_numero(nr_documento_w))
              );
*/              


		if	(nr_seq_parcela_w	is not null) then

			select	max(a.vl_parcela)
                into vl_parcela_w
			from movto_cartao_cr_parcela a
			where a.nr_sequencia	= nr_seq_parcela_w;

			vl_saldo_concil_fin_w	:= nvl(vl_saldo_concil_fin_w,0) - nvl(vl_parcela_w,0);

			if	(nvl(vl_saldo_concil_fin_w,0)	>= nvl(vl_min_indevido_w,0)) or
				(nvl(vl_saldo_concil_fin_w,0)	<= nvl(vl_max_indevido_w,0)) then

				vl_saldo_concil_fin_w	:= 0;

			end if;


             /*Ricardo*/
            select	max(b.ds_comprovante)
            into	ds_comprovante_tasy_w
            from movto_cartao_cr b,
                movto_cartao_cr_parcela a
            where	a.nr_seq_movto		= b.nr_sequencia
            and	a.nr_sequencia		= nr_seq_parcela_w;

                   if	(ds_comprovante_tasy_w	is not null) then

                                    select	count(*)
                                    into	qt_comprovante_w
                                    from	movto_cartao_cr a
                                    where	obter_saldo_cartao_cr(a.nr_sequencia,null)	<> 0
                                    and	a.nr_seq_bandeira	= nr_seq_bandeira_w
                                    and	a.dt_cancelamento	is null
                                    and	a.ds_comprovante	= ds_comprovante_tasy_w;


                                if	(nvl(qt_comprovante_w,0)	<= 1) then

                                        select	extrato_cartao_cr_parcela_seq.nextval
                                            into	nr_seq_extrato_parcela_w
                                        from	dual;

                                        insert	into extrato_cartao_cr_parcela
                                                    (nr_sequencia,dt_atualizacao_nrec,nm_usuario_nrec,
                                                     dt_atualizacao,nm_usuario,nr_seq_extrato,NR_SEQ_EXTRATO_ARQ)
                                        values	(nr_seq_extrato_parcela_w,sysdate,nm_usuario_p,
                                                 sysdate,nm_usuario_p,nr_seq_extrato_w,nr_seq_extrato_arq_p); 

                                        update	movto_cartao_cr_parcela
                                                set	nr_seq_extrato_parcela	= nr_seq_extrato_parcela_w                                                    
                                        where	nr_sequencia		= nr_seq_parcela_w;   

                                         --- Update EXTRATO_CARTAO_CR_MOVTO
                                end if;
                   end if;
            ----
			update extrato_cartao_cr_movto
            set	nr_seq_parcela		= nr_seq_parcela_w,
                vl_saldo_concil_fin	= vl_saldo_concil_fin_w,
                dt_atualizacao		= sysdate,
                nm_usuario		    = nm_usuario_p,
                NR_SEQ_EXTRATO_PARCELA = nr_seq_extrato_parcela_w -- incluido HP/TI
			where	nr_sequencia	= nr_seq_movto_w;           
		end if;
/*-----------------------------------------------------------------------------
            D - Desconciliar o extrato e movto_cartao_cr_parcela
-----------------------------------------------------------------------------*/
	elsif	(ie_opcao_p = 'D') and (nr_seq_parcela_concil_w is not null) then

            select	max(a.vl_parcela)
                into	vl_parcela_w
            from movto_cartao_cr_parcela a 
            where a.nr_sequencia	= nr_seq_parcela_concil_w;            
            /*Ricardo*/
             --if	(nr_seq_extrato_parcela_w is not null) then

            select	max(nr_seq_extrato_parcela)
                    into	nr_seq_extrato_parcela_w
            from extrato_cartao_cr_movto
            where NR_SEQ_EXTRATO =  nr_seq_extrato_w
                  and nr_seq_extrato_arq = nr_seq_extrato_arq_p
                  and nr_seq_parcela = nr_seq_parcela_concil_w;
            --and nr_sequencia	= nr_seq_parcela_concil_w;

                    update	movto_cartao_cr_parcela
                        set	nr_seq_extrato_parcela	= null
                    where nr_sequencia = nr_seq_parcela_concil_w; 
                    --where nr_seq_extrato_parcela = nr_seq_extrato_parcela_w; commit;                          
            --end if;
            ------------ok---            
            update	extrato_cartao_cr_movto
            set	nr_seq_parcela		= null,
                nr_seq_extrato_parcela = null,
                vl_saldo_concil_fin	= nvl(vl_saldo_concil_fin,0) + nvl(vl_parcela_w,0),
                dt_atualizacao		= sysdate,
                nm_usuario		    = nm_usuario_p
            where	nr_sequencia	= nr_seq_movto_w;

            delete	from extrato_cartao_cr_parcela where nr_sequencia = nr_seq_extrato_parcela_w; commit;
	end if;

end	loop;
close c01;

/*-----------------------------------------------------------------------------
Atualizando Adm. Cartoes>>Importacao Extrato>>Grupo de Bandeiras> Grid Arquivos
-----------------------------------------------------------------------------*/
select	count(*)
into	qt_nao_concil_w
from extrato_cartao_cr_movto b,
	 extrato_cartao_cr_res a
where nvl(b.vl_saldo_concil_fin,0) <> 0
      and	a.nr_sequencia		= b.nr_seq_extrato_res
      and	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p;

update	extrato_cartao_cr_arq
set	dt_conciliacao		= decode(qt_nao_concil_w,0,sysdate,null),
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_extrato_arq_p;
commit;
-----------------------------------------------------------------------------

end HP_CONCILIAR_EXT_CARTAO_TKS;
/
