create or replace
procedure OBTER_REGRA_REPASSE_RETORNO	(ie_status_repasse_p	in	varchar2,
					cd_motivo_glosa_p		in	number,
					ie_forma_calculo_crit_p	in  out	varchar2,
					nr_seq_regra_p		out	number,
					ie_calculo_p		out	varchar2,
					ie_status_rep_novo_p	out	varchar2,
					ie_proc_partic_p		out	varchar2,
					ie_origem_valor_p		out	varchar2,
					ie_adicional_p		in	varchar2,
					nr_seq_procedimento_p	in	number,
					tx_adicional_p		out	number,
					qt_partic_rep_p		in	number) is

ie_calculo_w		varchar2(10);
nr_seq_regra_w		number(15);
ie_status_rep_novo_w	varchar2(10);
ie_forma_calculo_crit_w	varchar2(10);
ie_proc_partic_w		varchar2(10);
ie_origem_valor_w		varchar2(50);
ie_adicional_w		varchar2(50);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(15);
cd_area_procedimento_w	number(15);
cd_especialidade_w	number(15);
cd_grupo_proc_w		number(15);
tx_adicional_w		number(15,4);
ie_partic_rep_w		varchar2(255);

cursor c01 is
select	a.nr_sequencia,
	a.ie_calculo,
	a.ie_status_rep_novo,
	a.ie_forma_calculo_crit,
	nvl(a.ie_proc_partic,'A'),
	a.ie_origem_valor,
	nvl(a.ie_adicional,'N'),
	a.tx_adicional
from	regra_repasse_retorno a
where	a.ie_status_repasse 					= ie_status_repasse_p
and	nvl(a.cd_motivo_glosa, nvl(cd_motivo_glosa_p, -1))		= nvl(cd_motivo_glosa_p, -1)
and	nvl(a.ie_forma_calculo_crit, nvl(ie_forma_calculo_crit_p, '-1'))	= nvl(ie_forma_calculo_crit_p, '-1')
and	((nvl(a.ie_adicional, 'N')					= 'N')
	 or (nvl(ie_adicional_p, 'N')					= nvl(a.ie_adicional, 'N')))
and	nvl(cd_procedimento, nvl(cd_procedimento_w, 0))		= nvl(cd_procedimento_w, 0)
and	nvl(cd_area_procedimento, nvl(cd_area_procedimento_w,0))	= nvl(cd_area_procedimento_w,0)
and	nvl(cd_especialidade, nvl(cd_especialidade_w,0))		= nvl(cd_especialidade_w,0)
and	nvl(cd_grupo_proc, nvl(cd_grupo_proc_w,0))			= nvl(cd_grupo_proc_w,0)
and	nvl(ie_origem_proced, nvl(ie_origem_proced_w,0))		= nvl(ie_origem_proced_w,0)
and	((nvl(ie_partic_rep, 'N') = 'N')
	 or (nvl(qt_partic_rep_p, 0) > 0))
order by	decode(nvl(ie_adicional, 'N'), 'S', 1, 0),
	nvl(ie_forma_calculo_crit, 0),
	nvl(cd_procedimento, 0),
	nvl(cd_grupo_proc, 0),
	nvl(cd_especialidade, 0),
	nvl(cd_area_procedimento, 0),
	decode(nvl(ie_partic_rep, 'N'), 'S', 1, 0);

begin

select	max(a.cd_procedimento),
	max(a.ie_origem_proced)
into	cd_procedimento_w,
	ie_origem_proced_w
from	procedimento_paciente a
where	a.nr_sequencia		= nr_seq_procedimento_p;

select	max(a.cd_area_procedimento),
	max(a.cd_especialidade),
	max(a.cd_grupo_proc)
into	cd_area_procedimento_w,
	cd_especialidade_w,
	cd_grupo_proc_w
from	estrutura_procedimento_v a
where	a.cd_procedimento		= cd_procedimento_w
and	a.ie_origem_proced	= ie_origem_proced_w;

open c01;
loop
fetch c01 into
	nr_seq_regra_w,
	ie_calculo_w,
	ie_status_rep_novo_w,
	ie_forma_calculo_crit_w,
	ie_proc_partic_w,
	ie_origem_valor_w,
	ie_adicional_w,
	tx_adicional_w;
exit when c01%notfound;

end loop;
close c01;

nr_seq_regra_p		:= nr_seq_regra_w;
ie_calculo_p		:= ie_calculo_w;
ie_status_rep_novo_p	:= ie_status_rep_novo_w;
ie_forma_calculo_crit_p	:= ie_forma_calculo_crit_w;
ie_proc_partic_p		:= ie_proc_partic_w;
ie_origem_valor_p		:= ie_origem_valor_w;
tx_adicional_p		:= tx_adicional_w;

end	OBTER_REGRA_REPASSE_RETORNO;
/