create or replace
procedure dmed_anual_arquivo_brppss
			(	nr_sequencia_p			number,
				cd_pessoa_titular_p		number,
				nr_cpf_titular_p		varchar2,
				nm_usuario_p			varchar2,
				nr_linha_p		in out	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_beneficiario_w	pessoa_fisica.cd_pessoa_fisica%type;
nr_cpf_beneficiario_w		pessoa_fisica.nr_cpf%type;
dt_nascimento_w			pessoa_fisica.dt_nascimento%Type;
dt_nascimento_arquivo_w		varchar2(8);
nm_beneficiario_w		pessoa_fisica.nm_pessoa_fisica%type;
vl_pago_w			dmed_titulos_mensal.vl_pago%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
ds_arquivo_w			varchar2(2000);
ie_dependencia_w		varchar2(2);
nr_linha_w			number(10)	:= nr_linha_p;
contador_w			number(10)	:= 0;
nr_sequencia_w  		number(10);						
qt_registros_w			number(10);

--pega todos os dependentes que tiveram algum documento para pagar, inclusive reembolso
--pois pode haver reembolso e n�o ter tido mensalidade, mesmo assim
cursor c_dependente is
	select	cd_pessoa_beneficiario, 
		nr_cpf_beneficiario,
		dt_nascimento_benef,
		nm_beneficiario
	from	(select	p.cd_pessoa_fisica cd_pessoa_beneficiario, 
			p.nr_cpf nr_cpf_beneficiario,
			p.dt_nascimento dt_nascimento_benef,
			p.nm_pessoa_fisica nm_beneficiario,
			to_char(case when ((pkg_date_utils.add_month(p.dt_nascimento, 18 * 12, 0) > sysdate) and (p.nr_cpf = nr_cpf_titular_p)) then
				'0'
			else
				p.nr_cpf
			end) cpf_order
		from	pessoa_fisica		p
		where	exists	(select	1
				from	dmed_titulos_mensal	t,
					dmed_agrupar_lote	l,
					pessoa_fisica		f
				where	p.cd_pessoa_fisica		= t.cd_pessoa_beneficiario
				and	t.nr_seq_dmed_mensal		= l.nr_seq_dmed_mensal
				and	t.cd_pessoa_titular		= f.cd_pessoa_fisica
				and	nvl(t.ie_prestadora_ops,'P')	= 'P'
				and	t.cd_pessoa_titular <> t.cd_pessoa_beneficiario
				and	l.nr_seq_dmed_anual		= nr_sequencia_p
				and	f.nr_cpf			= nr_cpf_titular_p)
		order by
			nvl(cpf_order,'0') asc,
			to_char(p.dt_nascimento,'yyyymmdd') asc);
	
type 		fetch_array is table of c_dependente%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_depenedente_w	vetor;

begin
open c_dependente;
loop
fetch c_dependente bulk collect into s_array limit 1000;
	vetor_depenedente_w(i)	:= s_array;
	i			:= i + 1;
exit when c_dependente%notfound;
end loop;
close c_dependente;

for i in 1..vetor_depenedente_w.count loop
	begin
	s_array := vetor_depenedente_w(i);
	
	for z in 1..s_array.count loop
		cd_pessoa_beneficiario_w	:= s_array(z).cd_pessoa_beneficiario;
		nr_cpf_beneficiario_w		:= s_array(z).nr_cpf_beneficiario;
		dt_nascimento_w			:= s_array(z).dt_nascimento_benef;
		nm_beneficiario_w		:= s_array(z).nm_beneficiario;
		begin
		if	(nr_cpf_titular_p = nr_cpf_beneficiario_w) and
			(pkg_date_utils.add_month(dt_nascimento_w, 18 * 12,0) < sysdate) then
			null;
		else
		
			nm_beneficiario_w	:= fis_remove_special_characters(nm_beneficiario_w);
			dt_nascimento_arquivo_w := to_char(dt_nascimento_w,'yyyymmdd');
			
			if	(nr_cpf_titular_p = nr_cpf_beneficiario_w) and
				(pkg_date_utils.add_month(dt_nascimento_w, 18 * 12,0) > sysdate) then
				nr_cpf_beneficiario_w	:= null;
			end if;
		
			select	sum(t.vl_pago) vl_pago_beneficiario
			into 	vl_pago_w
			from	dmed_titulos_mensal	t,
				dmed_agrupar_lote	l,
				pessoa_fisica		p
			where	t.nr_seq_dmed_mensal		= l.nr_seq_dmed_mensal
			and	l.nr_seq_dmed_anual		= nr_sequencia_p
			and	nvl(t.ie_prestadora_ops,'P')	= 'P'
			and	t.cd_pessoa_titular		= p.cd_pessoa_fisica
			--and	t.ie_tipo_documento <> 'RE'
			and	t.cd_pessoa_titular 		<> t.cd_pessoa_beneficiario
			and	p.nr_cpf			= nr_cpf_titular_p
			and	t.cd_pessoa_beneficiario	= cd_pessoa_beneficiario_w;

			if	(vl_pago_w > 0) then
				vl_pago_w	:= replace(replace(to_char(vl_pago_w,'999,999,990.00'),'.',''),',','');
				
				ds_arquivo_w 	:= 'BRPPSS' || '|' || nr_cpf_beneficiario_w || '|' || dt_nascimento_arquivo_w || '|' || nm_beneficiario_w || '|' ||
							vl_pago_w || '|';
			else
				ds_arquivo_w 	:= 'BRPPSS' || '|' || nr_cpf_beneficiario_w || '|' || dt_nascimento_arquivo_w || '|' || nm_beneficiario_w || '|' ||
							'' || '|';
			end if;	
			
			contador_w	:= contador_w + 1;
			nr_linha_w	:= nr_linha_w + 1;
			
			insert into w_dacon
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_arquivo,
				nr_linha,
				ie_tipo_registro,
				nr_origem)
			values	(w_dacon_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_w,
				nr_linha_w,
				9,
				null);
			
			if	(mod(contador_w,100) = 0) then
				commit;
			end if;
			
		end if;
		end;
		
	end loop;
	end;
end loop;

nr_linha_p	:= nr_linha_w;

commit;

end dmed_anual_arquivo_brppss;
/