create or replace 
procedure Desfazer_Aprovacao_Repasse
		(nr_repasse_terceiro_p		number,
		nm_usuario_p			varchar2,
		ie_commit_p			varchar2) is

qt_titulos_w	number(5);
ie_aprova_repasse_w	varchar(1);
count_w			number(10);
ie_nota_w		number(10);

begin

Obter_Param_Usuario(89, 160, obter_perfil_ativo, nm_usuario_p,obter_estabelecimento_ativo, ie_aprova_repasse_w);

select count(*)
into	qt_titulos_w
from titulo_pagar
where nr_repasse_terceiro = nr_repasse_terceiro_p;

select	count(*)
into	ie_nota_w
from	REPASSE_NOTA_FISCAL
where	nr_repasse_terceiro = nr_repasse_terceiro_p;

if	(ie_nota_w > 0) then
	--'A aprova��o n�o pode ser desfeita. Existe nota fiscal gerada para este repasse!'
	wheb_mensagem_pck.exibir_mensagem_abort(339911);
end if;


if (nvl(ie_aprova_repasse_w,'N') = 'S') then

	select	count(*)
	into	count_w
	from	repasse_terceiro_aprovacao
	where   nr_repasse_terceiro = nr_repasse_terceiro_p
	and     dt_aprovacao is not null;
	
	if (count_w > 0) then
	
		update	repasse_terceiro_aprovacao
		set	dt_aprovacao = null
		where   nr_repasse_terceiro = nr_repasse_terceiro_p;
		
	end if;
	
end if;

if	(qt_titulos_w > 0) then
	/* Opera��o cancelada. Existem t�tulos para este repasse! */
	wheb_mensagem_pck.exibir_mensagem_abort(261374);
end if;

update	repasse_terceiro
set	dt_aprovacao_terceiro   = null,
	nm_usuario_aprov	= null,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

if	(ie_commit_p	<> 'N') then
	commit;
end if;

end Desfazer_Aprovacao_Repasse;
/
