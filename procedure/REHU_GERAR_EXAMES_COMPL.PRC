create or replace
procedure REHU_GERAR_EXAMES_COMPL (	nr_seq_ciclo_p		number,
					nm_usuario_p		Varchar2) is 


nr_seq_tipo_termo_w	number(10);
nr_sequencia_w		number(10);
ds_tipo_termo_w		varchar2(255);
qt_exames_w		number(10);
					
Cursor C01 is
	select	nr_sequencia,
		ds_tipo_termo
	from	rehu_tipo_termo
	where	ie_situacao = 'A'
	order by 2;
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_tipo_termo_w,
	ds_tipo_termo_w;
exit when C01%notfound;
	begin
	
	select 	count(*)
	into	qt_exames_w
	from	REHU_CICLO_TERMO
	where	nr_seq_tipo_termo = nr_seq_tipo_termo_w
	and	nr_seq_ciclo = nr_seq_ciclo_p;	
	
	if 	(qt_exames_w = 0) then
		begin
	
		select	REHU_CICLO_TERMO_seq.nextval
		into	nr_sequencia_w	
		from	dual;
		
		insert into REHU_CICLO_TERMO (  nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_ciclo,
						nr_seq_tipo_termo,
						ie_entrege,
						ie_devolvido
						) 
					values (nr_sequencia_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_ciclo_p,
						nr_seq_tipo_termo_w,
						'N',
						'N'
						);
		end;
	end if;
	end;
end loop;
close C01;


commit;

end REHU_gerar_exames_compl;
/