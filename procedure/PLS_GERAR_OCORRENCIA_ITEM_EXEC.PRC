create or replace
procedure pls_gerar_ocorrencia_item_exec
			(	ie_tipo_item_p			varchar2,
				cd_procedimento_p 		number,
				ie_origem_proced_p		number,
				nr_seq_material_p			number,				
				nr_seq_segurado_p			number,
				nr_seq_exec_item_p		number,
				nr_seq_proc_p			number,
				nr_seq_mat_p			number,
				ie_exige_medico_p			varchar2,
				nr_seq_grupo_contrato_p		number,
				dt_referencia_p			date,
				qt_item_p				number,
				cd_estabelecimento_p		number,
				nm_usuario_p			Varchar2,
				ie_ocorrencia_p		out	varchar2) is 

nr_seq_regra_item_w		number(10);
ie_nao_informado_w		varchar2(10);
nr_seq_ocorrencia_w		number(10);
nr_seq_motivo_glosa_w		number(10);
ie_tipo_item_w			varchar2(10);
ie_tipo_conta_w			varchar2(10);
vl_item_w				number(15,4);
ie_tipo_despesa_w			varchar2(1)	:= null;
ie_nota_fiscal_w			varchar2(1)	:= null;
ie_fornecedor_w			varchar2(1)	:= null;
nr_seq_estrutura_w			number(10);
ie_estrutura_w			Varchar2(1)	:= 'S';
cd_procedimento_w		number(10);
nr_seq_restricao_w			number(15);
qt_minima_w			number(15);
qt_maxima_w			number(15);
ie_consiste_qtd_w			varchar2(1)	:= 'N';
ie_consiste_sexo_w			varchar2(1)	:= 'N';
ie_consiste_idade_w		varchar2(1)	:= 'N';
ie_sexo_seg_w			varchar2(1);
ie_idade_seg_w			number(5);	
ie_sexo_mat_w			varchar(2);
qt_idade_min_mat_w		number(5);
qt_idade_max_mat_w		number(5);
ie_bloqueia_custo_op_w		varchar2(5)	:= 'N';
ie_bloqueia_pre_pag_w		varchar2(5)	:= 'N';
ie_bloqueia_intercambio_w	varchar2(5)	:= 'N';
ie_bloqueia_prod_nao_reg_w	varchar2(5)	:= 'N';
ie_bloqueia_prod_reg_w		varchar2(5)	:= 'N';
ie_classif_sip_w		varchar2(5)	:= 'N';
ie_pacote_w			varchar2(1);
nr_seq_conta_princ_w		number(10);
ie_gerar_oc_pacote_w		varchar2(1)	:= 'N';
ie_nao_utilizado_w		varchar2(1);
qt_item_autorizado_w		number(10);
qt_item_solic_w			number(10);
ds_observacao_w			varchar2(255)	:= '';
ie_autorizacao_especial_w	varchar2(1)	:= 'N';
cd_guia_ref_w			varchar2(20);
nr_seq_guia_w			number(10);
ie_mat_espec_aut_w		varchar2(1);
cd_especialidade_w		number(15);
cd_area_procedimento_w		number(15);
cd_grupo_proc_w			number(15);
ie_origem_proced_w		number(10);
nr_seq_estrut_mat_w		number(10);
nr_seq_grupo_rec_w		number(10);
ie_via_acesso_w			varchar2(1);
nr_nota_fiscal_mat_w		number(20);
ie_calculo_coparticipacao_w	varchar2(10);
ie_coparticipacao_w		Varchar2(10);
ie_co_preco_operadora_w		Varchar2(10);

vl_calculado_w			number(15,2);	
vl_apresentado_w		number(15,2);
vl_max_item_w			number(15,2);
vl_minimo_item_w		number(15,2);
ie_consistencia_valor_w		varchar2(1);
ie_tipo_segurado_w		varchar2(3);
ie_sexo_w			varchar2(2);
qt_idade_min_w			number(5);
qt_idade_max_w			number(5);
ie_unid_tempo_idade_w		varchar2(2);		
qt_idade_w			Varchar2(5);
qt_idade_meses_w		number(5);	
ie_sem_evento_w			Varchar2(2);
qt_eventos_pagto_w		number(10);
qt_ocorrencia_w			number(10);
qt_idade_minima_w		number(5);
qt_idade_maxima_w		number(5);
ie_sexo_exclusivo_w		varchar(1);
ie_auditoria_w			varchar2(1);
ie_ocorrencia_w			varchar2(1) := 'N';
nr_seq_grupo_produto_w		number(5);	
nr_seq_plano_w			number(5);	
nr_seq_regra_duplic_w		number(10);
ie_duplicidade_w		Varchar2(2);
nr_seq_grupo_material_w		Number(10);
ie_grupo_material_w		Varchar2(1)	:= 'S';
nr_seq_execucao_w		number(10);
nr_seq_tipo_acomod_conta_w	number(10);
nr_seq_tipo_acomodacao_w	number(10);
ie_exige_hora_item_w		Varchar2(1);
dt_inicio_proc_w		date;
dt_fim_proc_w			date;
ie_alterado_analise_w 		varchar2(1) := 'N';
nr_seq_ocorrencia_benef_w	number(10);
nr_seq_estrut_regra_w		number(10);
ie_estrut_mat_w			varchar2(1);
ie_restringe_estab_w		varchar2(2);
ie_ocor_estab_w			pls_controle_estab.ie_ocorrencia%type := pls_obter_se_controle_estab('GO');

Cursor C01 is
	select	a.nr_sequencia,
		a.ie_nao_informado,
		a.nr_seq_ocorrencia,
		b.nr_seq_motivo_glosa,
		nr_seq_estrutura,
		a.vl_max_item,
		nvl(a.vl_minimo_item,0),
		nvl(a.ie_nao_utilizado,'N'),
		a.qt_idade_min,
		a.qt_idade_max,
		a.ie_unid_tempo_idade,
		nvl(b.ie_auditoria,'S'),
		ie_sem_evento,
		nr_seq_regra_duplic,
		a.nr_seq_grupo_material,
		a.ie_exige_hora_item,
		a.nr_seq_estrutura_mat
	from	pls_ocorrencia_regra_item	a,
		pls_ocorrencia			b
	where	a.nr_seq_ocorrencia	= b.nr_sequencia
	and	trunc(dt_referencia_p) between trunc(nvl(a.dt_inicio_vigencia, sysdate)) and trunc(fim_dia(nvl(a.dt_fim_vigencia,dt_referencia_p)))
	and	a.ie_situacao = 'A'
	and	b.ie_situacao = 'A'
	and	((a.ie_tipo_item = ie_tipo_item_w) or (ie_tipo_item = 'A'))
	and	(a.ie_execucao_requisicao = 'S')
	and	((a.cd_procedimento is null) or (a.cd_procedimento = nvl(cd_procedimento_p,0)
	and	(a.ie_origem_proced = nvl(ie_origem_proced_p,0))))
	and	((a.cd_area_procedimento is null) or (a.cd_area_procedimento = nvl(cd_area_procedimento_w,0)))
	and	((a.cd_especialidade is null) or (a.cd_especialidade = nvl(cd_especialidade_w,0)))
	and	((a.cd_grupo_proc is null) or (a.cd_grupo_proc = nvl(cd_grupo_proc_w,0)))
	and	((a.nr_seq_material is null) or (a.nr_seq_material = nvl(nr_seq_material_p,0)))
	and	((nvl(ie_exige_medico,'N') = 'N')	or (nvl(ie_exige_medico,'N') <> ie_exige_medico_p))
	and	((nvl(nr_seq_grupo_contrato,0) = nvl(nr_seq_grupo_contrato_p,0))or (nr_seq_grupo_contrato is null))	
	and	((ie_consiste_qtd = 'N')   or (ie_consiste_qtd = ie_consiste_qtd_w))
	and	((ie_consiste_sexo = 'N')  or (ie_consiste_sexo = ie_consiste_sexo_w))
	and	((ie_consiste_idade = 'N') or (ie_consiste_idade = ie_consiste_idade_w))	
	and	((a.nr_seq_grupo_rec is null) or (a.nr_seq_grupo_rec = nr_seq_grupo_rec_w))
	and	((a.ie_tipo_segurado is null) or (a.ie_tipo_segurado = ie_tipo_segurado_w ))	
	and	((a.ie_sexo is null) or	( a.ie_sexo = ie_sexo_w))
	and	((a.ie_bloqueio_mat is null) or (a.ie_bloqueio_mat in (	ie_bloqueia_custo_op_w,
									ie_bloqueia_pre_pag_w,
									ie_bloqueia_intercambio_w,
									ie_bloqueia_prod_nao_reg_w,
									ie_bloqueia_prod_reg_w )))
	
	and	((nvl(ie_regra_valor_co,'N') = 'N') or nvl(ie_regra_valor_co,'N') = 'S' and nvl(ie_co_preco_operadora_w,'N') = 'N')	
	and	((a.nr_seq_plano is null) or ( a.nr_seq_plano = nr_seq_plano_w ))
	and	((a.nr_seq_grupo_produto is null) or (a.nr_seq_grupo_produto = nr_seq_grupo_produto_w ))
	and	((ie_ocor_estab_w = 'N') or (ie_ocor_estab_w = 'S' and b.cd_estabelecimento = cd_estabelecimento_p))
	order by		
		nvl(ie_tipo_item,'A'),
		nvl(nr_seq_grupo_contrato_p,0);

begin

/*askono - produto e grupo de produto*/
if	(nvl(nr_seq_segurado_p,0) > 0  ) then		
	/*obter dados do segurado*/
	
	if	(nr_seq_exec_item_p is not null) then
		begin
		select	a.nr_seq_plano
		into	nr_seq_plano_w
		from	pls_execucao_req_item b,
			pls_requisicao a
		where	a.nr_sequencia = b.nr_seq_requisicao
		and	b.nr_sequencia = nr_seq_exec_item_p;
		exception
		when others then
			nr_seq_plano_w	:= null;		
		end;
	else
		begin
		--select	nr_seq_plano 
		select	pls_obter_produto_benef(nr_sequencia,dt_referencia_p)
		into	nr_seq_plano_w
		from  	pls_segurado
		where	nr_sequencia = nr_seq_segurado_p;	
		exception
		when others then
			nr_seq_plano_w	:= null;		
		end;
	end if;

	/*dados do produto -- OS362870 - 17/10/2011*/	
	begin
	select	nr_seq_tipo_acomodacao
	into	nr_seq_tipo_acomodacao_w
	from 	pls_plano_acomodacao
	where	nr_seq_plano = nr_seq_plano_w;
	exception
	when others then
		nr_seq_tipo_acomodacao_w	:= null;	
	end;
	
	/*grupo do produto*/
	begin
	select 	a.nr_seq_grupo
	into	nr_seq_grupo_produto_w
	from	pls_preco_produto 	a	
	where	a.nr_seq_plano 	= nr_seq_plano_w;
	exception
	when others then
		nr_seq_grupo_produto_w	:= null;
	end;
end if;

/* Obter par�metros da conta m�dica */
ie_restringe_estab_w	:= pls_obter_se_controle_estab('CO');

select	nvl(max(ie_calculo_coparticipacao),'P')
into	ie_calculo_coparticipacao_w
from	pls_parametros
where	(((ie_restringe_estab_w	= 'S') and	(cd_estabelecimento	= cd_estabelecimento_p))
or	(ie_restringe_estab_w	= 'N'));

if 	(nvl(nr_seq_proc_p,0) <> 0) then
	ie_tipo_item_w	:= 'P';	
	
elsif	(nvl(nr_seq_mat_p,0) <> 0) then
	ie_tipo_item_w	:= 'M';
	
	pls_obter_mat_restricao_data(nr_seq_material_p,dt_referencia_p,nr_seq_restricao_w);

	if	(nvl(nr_seq_restricao_w,0) > 0) then
		
		select	qt_minima,
			qt_maxima,
			decode(ie_bloqueia_custo_op,'S','BCO'),
			decode(ie_bloqueia_pre_pag,'S','BPP'),
			decode(ie_bloqueia_intercambio,'S','BI'),
			decode(ie_bloqueia_prod_nao_reg,'S','BNR'),
			decode(ie_bloqueia_prod_reg,'S','BR'),
			nvl(ie_autorizacao,'N'),
			nvl(ie_nota_fiscal,'N'),
			qt_idade_minima,
			qt_idade_maxima,
			ie_sexo_exclusivo
		into	qt_minima_w,
			qt_maxima_w,
			ie_bloqueia_custo_op_w,
			ie_bloqueia_pre_pag_w,
			ie_bloqueia_intercambio_w,
			ie_bloqueia_prod_nao_reg_w,
			ie_bloqueia_prod_reg_w,
			ie_autorizacao_especial_w,
			ie_nota_fiscal_w,
			qt_idade_minima_w,
			qt_idade_maxima_w,
			ie_sexo_exclusivo_w
		from	pls_material_restricao
		where	nr_sequencia	= nr_seq_restricao_w;
		
		if	((qt_item_p <qt_minima_w) or (qt_item_p > qt_maxima_w)) then
			ie_consiste_qtd_w	:= 'S'; 
		end if;		
	end if;
	
	if	(nr_seq_segurado_p is not null) then			
	
		
		select	a.ie_sexo,
			obter_idade(a.dt_nascimento, sysdate, 'A')			
		into	ie_sexo_seg_w,
			ie_idade_seg_w			
		from	pessoa_fisica a,
			pls_segurado b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	b.nr_sequencia		= nr_seq_segurado_p;		

		if	(ie_sexo_seg_w	<> ie_sexo_exclusivo_w /*ie_sexo_mat_w*/) then
			ie_consiste_sexo_w	:= 'S';
		end if;
		
		if	(ie_idade_seg_w > qt_idade_maxima_w /*qt_idade_max_mat_w*/ or ie_idade_seg_w < qt_idade_minima_w /*qt_idade_min_mat_w*/) then
			ie_consiste_idade_w	:= 'S';
		end if;	
		
	end if;
	
end if;	

if	(nvl(cd_procedimento_p,0) <> 0) then
	pls_obter_estrut_proc	(cd_procedimento_p, ie_origem_proced_p, cd_area_procedimento_w,
				cd_especialidade_w, cd_grupo_proc_w, ie_origem_proced_w);	
	
	begin
		select	nr_seq_grupo_rec
		into	nr_seq_grupo_rec_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p;
	exception
	when others then
		nr_seq_grupo_rec_w := null;
	end;
end if;
if	(nvl(nr_seq_material_p,0) <> 0) then
	select	max(nr_seq_estrut_mat)
	into	nr_seq_estrut_mat_w
	from	pls_material
	where	nr_sequencia	= nr_seq_material_p;
end if;


/*dados do segurado*/
begin 
select	b.ie_tipo_segurado,
	a.ie_sexo,
	substr(obter_idade_pf(a.cd_pessoa_fisica, sysdate, 'A'),1,10),
	substr(obter_idade_pf(a.cd_pessoa_fisica, sysdate, 'M'),1,10)
into	ie_tipo_segurado_w,
	ie_sexo_w,
	qt_idade_w,
	qt_idade_meses_w
from	pls_segurado	b,
	pessoa_fisica	a
where	b.nr_sequencia		= nr_seq_segurado_p 
and	b.cd_pessoa_fisica	= a.cd_pessoa_fisica;
exception 
when others then	
	ie_tipo_segurado_w	:= '0'; 
end;

select	nr_seq_execucao
into	nr_seq_execucao_w
from	pls_execucao_req_item
where	nr_sequencia = nr_seq_exec_item_p;

open C01;
loop
fetch C01 into	
	nr_seq_regra_item_w,
	ie_nao_informado_w,
	nr_seq_ocorrencia_w,
	nr_seq_motivo_glosa_w,
	nr_seq_estrutura_w,
	vl_max_item_w,
	vl_minimo_item_w,
	ie_nao_utilizado_w,
	qt_idade_min_w,
	qt_idade_max_w,
	ie_unid_tempo_idade_w,
	ie_auditoria_w,
	ie_sem_evento_w,
	nr_seq_regra_duplic_w,
	nr_seq_grupo_material_w,
	ie_exige_hora_item_w,
	nr_seq_estrut_regra_w;	
exit when C01%notfound;
	begin
	qt_ocorrencia_w := 0;
	ds_observacao_w := '';	
	ie_estrut_mat_w	:= 'S';
	
	if	(nr_seq_estrut_regra_w is not null) then
		if	(pls_obter_se_mat_estrutura(nr_seq_material_p, nr_seq_estrut_regra_w) = 'N') then
			ie_estrut_mat_w	:= 'N';
		end if;
	end if;
	
	if	(ie_estrut_mat_w = 'S') then
		/*Diego 02/05/2011
		    Caso j� tenha sido gerado a ocorr�ncia da regra que se esta verificando n�o � necess�rio continuar as verifica��es das demais regras.*/
		if	(ie_tipo_item_p = 10) then
			select	count(*)
			into	qt_ocorrencia_w 
			from	pls_ocorrencia_benef
			where	nr_seq_ocorrencia	= nr_seq_ocorrencia_w 
			and	nr_seq_execucao		= nr_seq_execucao_w 
			and	nr_seq_proc 		= nr_seq_proc_p;
		elsif	(ie_tipo_item_p = 11) then 
			select	count(*)
			into	qt_ocorrencia_w 
			from	pls_ocorrencia_benef
			where	nr_seq_ocorrencia	= nr_seq_ocorrencia_w 
			and	nr_seq_execucao		= nr_seq_execucao_w 
			and	nr_seq_mat		= nr_seq_mat_p;		
		end if;
		
		if	(qt_ocorrencia_w > 0) then
			goto final;
		end if;		
			
		if	(nvl(nr_seq_estrutura_w,0)	<> 0) then
			ie_estrutura_w := pls_obter_se_estrutura(cd_procedimento_p, ie_origem_proced_p, nr_seq_mat_p, nr_seq_estrutura_w); 
		end if;
		
		/* Verificar se for regra de quantidade de idade minima ou maxima da regra, sendo "A" por ano e "M" por meses*/
		if	(ie_unid_tempo_idade_w = 'A') then
			if	((qt_idade_min_w is not null) and (qt_idade_min_w > qt_idade_w)) or
				((qt_idade_max_w is not null) and (qt_idade_max_w < qt_idade_w)) then
				goto final;
			end if;
		elsif	(ie_unid_tempo_idade_w = 'M') then
			if	((qt_idade_min_w is not null) and (qt_idade_min_w > qt_idade_meses_w)) or
				((qt_idade_max_w is not null) and (qt_idade_max_w < qt_idade_meses_w)) then
				goto final;
			end if;		
		end if;		

		/* Grupo de material */
		if	(nvl(nr_seq_grupo_material_w,0) > 0) and
			(nvl(nr_seq_material_p,0) > 0) then
			ie_grupo_material_w	:= pls_se_grupo_preco_material(nr_seq_grupo_material_w, nr_seq_material_p);
			
			if	(ie_grupo_material_w	= 'N') then
				goto final;
			end if;	
		end if;
		
		if	(ie_estrutura_w <> 'N') then
			if	(ie_auditoria_w = 'S') then
				ie_ocorrencia_w := 'S';
			end if;
			
			pls_inserir_ocorrencia (nr_seq_segurado_p, nr_seq_ocorrencia_w, null,
						null, null, nr_seq_proc_p, 
						nr_seq_mat_p, nr_seq_regra_item_w, nm_usuario_p, 
						null, nr_seq_motivo_glosa_w, ie_tipo_item_p, 
						cd_estabelecimento_p, 'N' ,nr_seq_execucao_w,
						nr_seq_ocorrencia_benef_w, null,
						null, null, null);

		end if;
		
		<<final>> 
		ie_estrutura_w	:= ie_estrutura_w;-- esta linha n�o faz nada, apenas para n�o gerar erro de sql.
	end if;
	end;
end loop;
close C01;

--Diego 11/05/2011 - Chamado dentro da consistir conta portanto n�o pode existir commit
--commit;

ie_ocorrencia_p := ie_ocorrencia_w;

end pls_gerar_ocorrencia_item_exec;
/
