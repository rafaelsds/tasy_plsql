create or replace
procedure pls_reativar_intercambio
			(	nr_seq_intercambio_p	number,
				dt_reativacao_p		date,
				ie_dependente_p		varchar2,
				ds_observacao_p		varchar2,
				cd_estabelecimento_p	number,
				ie_commit_p		varchar2,
				nm_usuario_p		Varchar2) is 
				
nr_seq_segurado_w		number(10);
dt_rescisao_w			date;
dt_exclusao_w			date;
				
cursor c01 is
	select	nr_sequencia
	from	pls_segurado
	where	nr_seq_intercambio	= nr_seq_intercambio_p
	and	dt_rescisao is not null
	and	nr_seq_titular is null;

begin


select	dt_exclusao
into	dt_exclusao_w
from	pls_intercambio
where	nr_sequencia	= nr_seq_intercambio_p;

/*Reativa os beneficiários que foram rescindidos no mesmo dia do contrato*/	
open C01;
loop
fetch C01 into	
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	
	select	trunc(dt_rescisao,'dd')
	into	dt_rescisao_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	
	if	(trunc(dt_exclusao_w,'dd') = dt_rescisao_w) then	
		pls_reativar_seg_intercambio(nr_seq_segurado_w,dt_reativacao_p,ds_observacao_p,'C','N',ie_dependente_p,null,cd_estabelecimento_p,nm_usuario_p);
	end if;
	
	end;
end loop;
close C01;

update	pls_intercambio
set	dt_exclusao		= null,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate,
	dt_reativacao		= dt_reativacao_p
where	nr_sequencia		= nr_seq_intercambio_p;

insert into pls_contrato_historico (nr_sequencia,
			cd_estabelecimento,
			nr_seq_intercambio,
			dt_historico,
			ie_tipo_historico,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_historico)
		values(	pls_contrato_historico_seq.NextVal,
			cd_estabelecimento_p,
			nr_seq_intercambio_p,
			dt_reativacao_p,
			'2',
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'Reativação do contrato');

if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_reativar_intercambio;
/
