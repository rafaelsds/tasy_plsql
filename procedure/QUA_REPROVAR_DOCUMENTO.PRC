create or replace
procedure qua_reprovar_documento(
	nr_sequencia_p		number,
	ds_motivo_p		varchar2, 
	nm_usuario_p		varchar2) is
	
dt_validacao_w			qua_documento.dt_validacao%type;
dt_elaboracao_w			qua_documento.dt_elaboracao%type;
nr_seq_superior_w		number(10,0);
cd_pessoa_validacao_w		qua_documento.cd_pessoa_validacao%type;
cd_pessoa_aprov_w               qua_documento.cd_pessoa_aprov%type;

cursor C01 is
select	nr_sequencia
from	qua_documento
where	nr_seq_superior = nr_sequencia_p;
	
begin

begin
select	dt_validacao,
	dt_elaboracao,
	nr_seq_superior,
	cd_pessoa_validacao,
	cd_pessoa_aprov	
into	dt_validacao_w,
	dt_elaboracao_w,
	nr_seq_superior_w,
	cd_pessoa_validacao_w,
	cd_pessoa_aprov_w	
from	qua_documento
where	nr_sequencia = nr_sequencia_p;
exception
when others then
	nr_seq_superior_w := 0;
end;

open C01;
loop
fetch C01 into
	nr_seq_superior_w;
exit when C01%notfound;
begin
	if(nr_seq_superior_w > 0) then
	begin
	
		update	qua_documento
		set	dt_validacao	= null,
			dt_elaboracao	= null,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate,
			dt_reprovacao	= sysdate,
			ie_status	= 'O'	
		where	nr_sequencia	= nr_seq_superior_w;
	end;
	end if;
end;	
end loop;
close C01;

update	qua_documento
set	dt_validacao	= null,
	dt_elaboracao	= null,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	dt_reprovacao	= sysdate,
	ie_status	= 'O'	
where	nr_sequencia	= nr_sequencia_p;


insert into qua_doc_historico(
	nr_sequencia,	
	nr_seq_documento,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_historico)
values (qua_doc_historico_seq.nextval,
	nr_sequencia_p,	
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	ds_motivo_p);
commit;
end qua_reprovar_documento;
/
