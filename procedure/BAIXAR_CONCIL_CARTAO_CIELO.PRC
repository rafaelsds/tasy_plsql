create or replace
procedure BAIXAR_CONCIL_CARTAO_CIELO	(nr_seq_extrato_arq_p		number,
					nm_usuario_p			varchar2) is

ie_gerar_movto_banco_w		varchar2(1);
cd_estabelecimento_w		number(4);
nr_seq_parcela_w		number(10);
dt_prev_pagto_w			date;
nr_seq_extrato_w		number(10);
vl_saldo_concil_fin_w		number(15,2);
nr_seq_conta_banco_res_w	number(10);
nr_seq_grupo_w			number(10);
nr_seq_conta_banco_w		number(10);
nr_seq_trans_indevido_w		number(10);
nr_seq_movto_trans_w		number(10);
vl_ajuste_w			number(15,2);
nr_seq_movto_w			number(10);
ds_mensagem_w			varchar2(255);
/*Despesas com equipamento*/
nr_seq_trans_desp_w		grupo_bandeira_cr.nr_seq_trans_desp_equip%type;
vl_despesa_w			extrato_cartao_cr_desp.vl_liquido%type;
dt_pagto_desp_w			extrato_cartao_cr_desp.dt_prev_pagto%type;

cursor	c01 is
select	b.nr_seq_parcela,
	a.dt_prev_pagto,
	a.nr_seq_conta_banco,
	b.vl_ajuste,
	b.nr_sequencia
from	extrato_cartao_cr_movto b,
	extrato_cartao_cr_res a
where	b.nr_seq_parcela	is not null
and	a.nr_sequencia		= b.nr_seq_extrato_res
and	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p
union
select	d.nr_sequencia,
	a.dt_prev_pagto,
	a.nr_seq_conta_banco,
	b.vl_ajuste,
	b.nr_sequencia
from	movto_cartao_cr_parcela d,
	extrato_cartao_cr_parcela c,
	extrato_cartao_cr_movto b,
	extrato_cartao_cr_res a
where	c.nr_sequencia			= d.nr_seq_extrato_parcela
and	b.nr_seq_extrato_parcela	= c.nr_sequencia
and	a.nr_sequencia			= b.nr_seq_extrato_res
and	a.nr_seq_extrato_arq		= nr_seq_extrato_arq_p;

cursor	c02 is
select	b.vl_saldo_concil_fin,
	a.dt_prev_pagto,
	a.nr_seq_conta_banco
from	extrato_cartao_cr_movto b,
	extrato_cartao_cr_res a
where	b.ie_pagto_indevido	= 'S'
and	a.nr_sequencia		= b.nr_seq_extrato_res
and	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p;

Cursor c03 is
select	a.vl_liquido,
	a.dt_prev_pagto
from	extrato_cartao_cr_desp a
where	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p;

begin

select	max(b.cd_estabelecimento),
	max(b.nr_sequencia),
	max(b.nr_seq_grupo)
into	cd_estabelecimento_w,
	nr_seq_extrato_w,
	nr_seq_grupo_w
from	extrato_cartao_cr b,
	extrato_cartao_cr_arq a
where	a.nr_seq_extrato	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_extrato_arq_p;

select	decode(nvl(max(a.ie_forma_lanc_lote_cartao),'P'),'S','N')
into	ie_gerar_movto_banco_w
from	parametro_contas_receber a
where	a.cd_estabelecimento	= cd_estabelecimento_w;

/* gerar movimentação para os pagamentos indevidos */
select	max(a.nr_seq_conta_banco),
	max(a.nr_seq_trans_indevido_cred),
	max(a.nr_seq_trans_desp_equip)
into	nr_seq_conta_banco_w,
	nr_seq_trans_indevido_w,
	nr_seq_trans_desp_w
from	grupo_bandeira_cr a
where	a.nr_sequencia	= nr_seq_grupo_w;

/* baixar as parcelas */
open	c01;
loop
fetch	c01 into
	nr_seq_parcela_w,
	dt_prev_pagto_w,
	nr_seq_conta_banco_res_w,
	vl_ajuste_w,
	nr_seq_movto_w;
exit	when c01%notfound;

	ajustar_parcela_cartao_cr(nr_seq_parcela_w,nr_seq_grupo_w,vl_ajuste_w,nm_usuario_p,nr_seq_movto_w);
	
	ds_mensagem_w := substr(wheb_mensagem_pck.get_texto(303016, 'nr_seq_extrato_w=' || nr_seq_extrato_w),1,255); 
	
	baixar_parcela_cartao_cr(	nr_seq_parcela_w,
					nvl(dt_prev_pagto_w,sysdate),
					nm_usuario_p,
					'S',
					'S',
					ds_mensagem_w,
					ie_gerar_movto_banco_w,
					'N',
					0,
					null,
					nvl(nr_seq_conta_banco_res_w,nr_seq_conta_banco_w));

end	loop;
close	c01;

open	c02;
loop
fetch	c02 into
	vl_saldo_concil_fin_w,
	dt_prev_pagto_w,
	nr_seq_conta_banco_res_w;
exit	when c02%notfound;

	if	(nr_seq_trans_indevido_w	is null) then
		/* Falta informar a transação de pagamento indevido no cadastro do grupo de bandeiras. */
		wheb_mensagem_pck.exibir_mensagem_abort(186815);
	end if;

	select	movto_trans_financ_seq.nextval
	into	nr_seq_movto_trans_w
	from	dual;

	insert	into movto_trans_financ
		(dt_atualizacao,
		dt_transacao,
		ie_conciliacao,
		nm_usuario,
		nr_lote_contabil,
		nr_seq_banco,
		nr_seq_trans_financ,
		nr_sequencia,
		vl_transacao)
	values	(sysdate,
		nvl(dt_prev_pagto_w,sysdate),
		'N',
		nm_usuario_p,
		0,
		nvl(nr_seq_conta_banco_res_w, nr_seq_conta_banco_w),
		nr_seq_trans_indevido_w,
		nr_seq_movto_trans_w,
		vl_saldo_concil_fin_w);
			
	atualizar_transacao_financeira(cd_estabelecimento_w,nr_seq_movto_trans_w,nm_usuario_p,'I');

end	loop;
close	c02;

	if	( nr_seq_trans_desp_w is not null) and
		( nr_seq_conta_banco_w is not null) then
		open c03;
		loop
		fetch c03 into
			vl_despesa_w,
			dt_pagto_desp_w;
		exit when c03%notfound;
			gerar_movto_despesa_equip(vl_despesa_w,nr_seq_trans_desp_w,nr_seq_conta_banco_w,nvl(dt_pagto_desp_w,sysdate),nm_usuario_p,'N');
		end loop;
		close c03;
	end if;

update	extrato_cartao_cr_arq
set	dt_baixa	= sysdate,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_extrato_arq_p;

commit;

end BAIXAR_CONCIL_CARTAO_CIELO;
/