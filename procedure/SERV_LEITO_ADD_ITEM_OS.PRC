create or replace
procedure SERV_LEITO_ADD_ITEM_OS (	nr_sequencia_p		Number,-- regra da tela
					nr_seq_regra_p		Number,-- regra do shift+f11
					ie_regra_p		Varchar2,
					nm_usuario_p		Varchar2) is 

begin

if (nvl(nr_sequencia_p,0) > 0) and
   (nvl(nr_seq_regra_p,0) > 0) and
   (nvl(ie_regra_p,'XXX') <> 'XXX') then

	if (ie_regra_p = 'G') then
	   
		insert into sl_geracao_ordem_servico
			(	nr_sequencia, 
				dt_atualizacao, 
				nm_usuario, 
				ie_situacao, 
				nr_seq_regra, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				nr_grupo_check_list, 
				nr_item_check_list)
		values	(	sl_geracao_ordem_servico_seq.nextval,
				sysdate,
				nm_usuario_p,
				'A',
				nr_sequencia_p,
				sysdate,
				nm_usuario_p,
				nr_seq_regra_p,
				null);
			
	elsif (ie_regra_p = 'I') then
		
		insert into sl_geracao_ordem_servico
			(	nr_sequencia, 
				dt_atualizacao, 
				nm_usuario, 
				ie_situacao, 
				nr_seq_regra, 
				dt_atualizacao_nrec, 
				nm_usuario_nrec, 
				nr_grupo_check_list, 
				nr_item_check_list)
		values	(	sl_geracao_ordem_servico_seq.nextval,
				sysdate,
				nm_usuario_p,
				'A',
				nr_sequencia_p,
				sysdate,
				nm_usuario_p,
				null,
				nr_seq_regra_p);
				
	end if;
end if;

commit;

end serv_leito_add_item_OS;
/