create or replace procedure PRC_MANTER_TABELAS_MODULO (nr_seq_modulo_p in number,
                                                          nm_tabela_p in varchar2,
                                                          nm_usuario_p in varchar2, 
                                                          action_p in varchar2) is

begin

  IF action_p = 'I' THEN

    insert into MKT_MODULO_TABELA (
      nr_sequencia,
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      nr_seq_modulo,
      nm_tabela
    ) values(
      MKT_MODULO_TABELA_SEQ.nextval,
      sysdate,
      nm_usuario_p,
      sysdate,
      nm_usuario_p,
      nr_seq_modulo_p,
      nm_tabela_p
      );
  
  END IF;

  IF action_p = 'D' THEN

    delete from MKT_MODULO_TABELA
    where nr_seq_modulo = nr_seq_modulo_p
    and nm_tabela = nm_tabela_p;

  END IF;
end PRC_MANTER_TABELAS_MODULO;
/