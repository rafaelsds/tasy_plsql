create or replace
procedure gerar_w_sus_fpo_regra_relat(	dt_competencia_p	date,
					nm_usuario_p		Varchar2) is 

cd_carater_internacao_w 	varchar2(2);
cd_cbo_w 			varchar2(6);
cd_forma_organizacao_w 		number(6);
cd_grupo_w 			number(2);
cd_procedimento_w 		number(15);
cd_subgrupo_w 			number(4);
ds_cbo_w 			varchar2(600);
ds_forma_organizacao_w 		varchar2(100);
ds_grupo_w 			varchar2(100);
ds_procedimento_w 		varchar2(255);
ds_subgrupo_w 			varchar2(100);
dt_competencia_w 		date;
ie_complexidade_w 		varchar2(2);
ie_origem_proced_w 		number(10);
ie_tipo_atendimento_w 		varchar2(20);
ie_tipo_financiamento_w 	varchar2(4);
nm_usuario_resp_w 		varchar2(15);
nr_seq_estrutura_regra_w	number(10);
nr_seq_forma_org_w 		number(10);
nr_seq_fpo_regra_w 		number(10);
nr_seq_grupo_w 			number(10);
nr_seq_subgrupo_w 		number(10);
qt_executado_w 			number(15,4);
qt_fisico_w 			number(15,4);
qt_protocolo_w 			number(15,4);
vl_executado_w 			number(15,4);
vl_orcamento_w 			number(15,4);
vl_protocolo_w 			number(15,4);
qt_saldo_fisico_w		number(15,4);
vl_saldo_orcamentario_w		number(15,4);
ds_tipo_w			varchar2(2000);

Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_grupo,
		b.cd_grupo,
		b.ds_grupo,
		a.nr_seq_subgrupo,
		c.cd_subgrupo,
		c.ds_subgrupo,
		a.nr_seq_forma_org,
		d.cd_forma_organizacao,
		d.ds_forma_organizacao,
		a.cd_procedimento,
		a.ie_origem_proced,
		substr(obter_descricao_procedimento(a.cd_procedimento,a.ie_origem_proced),1,255) ds_procedimento,
		a.cd_cbo,
		substr(sus_obter_desc_cbo(a.cd_cbo),1,255) ds_cbo,
		a.qt_fisico,
		a.vl_orcamento,
		sus_obter_resultado_fpo(a.nr_sequencia,1,'E') qt_executado,
		sus_obter_resultado_fpo(a.nr_sequencia,2,'E') vl_executado,
		sus_obter_resultado_fpo(a.nr_sequencia,1,'P') qt_protocolo,
		sus_obter_resultado_fpo(a.nr_sequencia,2,'P') vl_protocolo,
		a.cd_carater_internacao,
		a.ie_tipo_atendimento,
		a.dt_competencia,
		a.ie_tipo_financiamento,
		a.ie_complexidade,
		a.nr_seq_estrutura_regra,
		a.nm_usuario_resp
	from	sus_fpo_regra a,
		sus_grupo b,
		sus_subgrupo c,
		sus_forma_organizacao d
	where	a.dt_competencia = dt_competencia_p
	and	a.nr_seq_grupo = b.nr_sequencia(+)
	and	a.nr_seq_subgrupo = c.nr_sequencia(+)
	and	a.nr_seq_forma_org = d.nr_sequencia(+)
	group by	a.nr_sequencia,
		a.nr_seq_grupo,
		b.cd_grupo,
		b.ds_grupo,
		a.nr_seq_subgrupo,
		c.cd_subgrupo,
		c.ds_subgrupo,
		a.nr_seq_forma_org,
		d.cd_forma_organizacao,
		d.ds_forma_organizacao,
		a.cd_procedimento,
		a.ie_origem_proced,
		substr(obter_descricao_procedimento(a.cd_procedimento,a.ie_origem_proced),1,255),
		a.cd_cbo,
		substr(sus_obter_desc_cbo(a.cd_cbo),1,255),
		a.qt_fisico,
		a.vl_orcamento,
		sus_obter_resultado_fpo(a.nr_sequencia,1,'E'),
		sus_obter_resultado_fpo(a.nr_sequencia,2,'E'),
		sus_obter_resultado_fpo(a.nr_sequencia,1,'P'),
		sus_obter_resultado_fpo(a.nr_sequencia,2,'P'),
		a.cd_carater_internacao,
		a.ie_tipo_atendimento,
		a.dt_competencia,
		a.ie_tipo_financiamento,
		a.ie_complexidade,
		a.nr_seq_estrutura_regra,
		a.nm_usuario_resp
	order by  nr_sequencia;

begin

delete from w_sus_fpo_regra_relat
where nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into	
	nr_seq_fpo_regra_w,
	nr_seq_grupo_w,
	cd_grupo_w,
	ds_grupo_w,
	nr_seq_subgrupo_w,
	cd_subgrupo_w,
	ds_subgrupo_w,
	nr_seq_forma_org_w,
	cd_forma_organizacao_w,
	ds_forma_organizacao_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	ds_procedimento_w,
	cd_cbo_w,
	ds_cbo_w,
	qt_fisico_w,
	vl_orcamento_w,
	qt_executado_w,
	vl_executado_w,
	qt_protocolo_w,
	vl_protocolo_w,
	cd_carater_internacao_w,
	ie_tipo_atendimento_w,
	dt_competencia_w,
	ie_tipo_financiamento_w,
	ie_complexidade_w,
	nr_seq_estrutura_regra_w,
	nm_usuario_resp_w;
exit when C01%notfound;
	begin
	
	qt_saldo_fisico_w	:= qt_fisico_w - qt_executado_w;
	vl_saldo_orcamentario_w	:= vl_orcamento_w - vl_executado_w;
	
	if	(nvl(cd_grupo_w,0) <> 0) then
		ds_tipo_w := substr( cd_grupo_w ||' - '|| ds_grupo_w ||' . ',1,2000);
	end if;
	if	(nvl(cd_subgrupo_w,0) <> 0) then
		ds_tipo_w := substr( cd_subgrupo_w ||' - '|| ds_subgrupo_w ||' . ',1,2000);
	end if;
	if	(nvl(cd_forma_organizacao_w,0) <> 0) then
		ds_tipo_w := substr( cd_forma_organizacao_w ||' - '|| ds_forma_organizacao_w ||' . ',1,2000);
	end if;
	if	(nvl(cd_procedimento_w,0) <> 0) then
		ds_tipo_w := substr( cd_procedimento_w ||' - '|| ds_procedimento_w ||' . ',1,2000);
	end if;
	if	(nvl(cd_cbo_w,0) <> 0) then
		ds_tipo_w := substr( ds_cbo_w ,1,2000);
	end if;
	
	insert into w_sus_fpo_regra_relat (	
		nr_sequencia,
		nr_seq_fpo_regra,
		nm_usuario,  
		dt_atualizacao,  
		dt_competencia,  
		cd_carater_internacao,
		cd_cbo,
		cd_forma_organizacao, 
		cd_grupo,
		cd_procedimento, 
		cd_subgrupo, 
		ds_cbo,
		ds_forma_organizacao, 
		ds_grupo,
		ds_procedimento, 
		ds_subgrupo, 
		ie_complexidade, 
		ie_origem_proced,
		ie_tipo_atendimento,  
		ie_tipo_financiamento,
		nm_usuario_resp, 
		nr_seq_estrutura_regra,
		nr_seq_forma_org,
		nr_seq_grupo,
		nr_seq_subgrupo,
		qt_executado,
		qt_fisico,
		qt_protocolo,
		vl_executado,
		vl_orcamento,
		vl_protocolo,
		qt_saldo_fisico,
		vl_saldo_orcamentario,
		ds_tipo)
	values(	w_sus_fpo_regra_relat_seq.nextval,
		nr_seq_fpo_regra_w,
		nm_usuario_p,
		sysdate,
		dt_competencia_w,
		cd_carater_internacao_w,
		cd_cbo_w,
		cd_forma_organizacao_w,
		cd_grupo_w,
		cd_procedimento_w,
		cd_subgrupo_w,
		ds_cbo_w,
		ds_forma_organizacao_w,
		ds_grupo_w,
		ds_procedimento_w,
		ds_subgrupo_w,
		ie_complexidade_w,
		ie_origem_proced_w,
		ie_tipo_atendimento_w,
		ie_tipo_financiamento_w,
		nm_usuario_resp_w,
		nr_seq_estrutura_regra_w,
		nr_seq_forma_org_w,
		nr_seq_grupo_w,
		nr_seq_subgrupo_w,
		nvl(qt_executado_w,0),
		nvl(qt_fisico_w,0),
		nvl(qt_protocolo_w,0),
		nvl(vl_orcamento_w,0),
		nvl(vl_executado_w,0),
		nvl(vl_protocolo_w,0),
		nvl(qt_saldo_fisico_w,0),
		nvl(vl_saldo_orcamentario_w,0),
		substr(ds_tipo_w,1,2000));
	
	end;
end loop;
close C01;

commit;

end gerar_w_sus_fpo_regra_relat;
/
