create or replace
PROCEDURE Import_pessoa_document(nm_usuario_p IN VARCHAR2) IS 

  nr_sequencia_w       w_pessoa_documentacao.nr_sequencia%TYPE; 
  nr_line_w            w_pessoa_documentacao.nr_line%TYPE; 
  dt_atualizacao_w     w_pessoa_documentacao.dt_atualizacao%TYPE; 
  nm_usuario_w         w_pessoa_documentacao.nm_usuario%TYPE; 
  nr_seq_documento_w   w_pessoa_documentacao.nr_seq_documento%TYPE; 
  nr_crm_w             w_pessoa_documentacao.nr_crm%TYPE; 
  cd_medico_w          w_pessoa_documentacao.cd_medico%TYPE; 
  si_special_license_w w_pessoa_documentacao.si_special_license%TYPE; 
  nr_document_w        w_pessoa_documentacao.nr_document%TYPE; 
  dt_entrega_w         w_pessoa_documentacao.dt_entrega%TYPE; 
  dt_start_w           w_pessoa_documentacao.dt_start%TYPE; 
  dt_validade_w        w_pessoa_documentacao.dt_validade%TYPE; 
  qt_type_doc_w        NUMBER(10); 
  qt_medico_w          NUMBER(10); 
  cd_pessoa_fisica_w   VARCHAR2(200); 
  qt_valdomino_w       NUMBER(10); 
  
  CURSOR c01 IS 
    SELECT nr_sequencia, 
           nr_line, 
           dt_atualizacao, 
           nm_usuario, 
           nr_seq_documento, 
           nr_crm, 
           cd_medico, 
           si_special_license, 
           nr_document, 
           dt_entrega, 
           dt_start, 
           dt_validade 
    FROM   w_pessoa_documentacao 
    WHERE  nm_usuario = nm_usuario_p; 
    
BEGIN
    FOR c01_w IN c01 LOOP 
 
        SELECT Count(1) 
        INTO   qt_type_doc_w 
        FROM   tipo_documentacao
        WHERE  nr_sequencia = c01_w.nr_seq_documento; 

        SELECT Count(1) 
        INTO   qt_medico_w 
        FROM   medico 
        WHERE  cd_pessoa_fisica = c01_w.cd_medico; 

        SELECT Count(1) 
        INTO   qt_valdomino_w 
        FROM   valor_dominio 
        WHERE  cd_dominio = 9616 
        AND 	vl_dominio = c01_w.si_special_license; 

        IF (qt_type_doc_w <> 0) and
           (qt_medico_w <> 0) and
           (qt_valdomino_w <> 0) THEN
		   
          INSERT INTO pessoa_documentacao 
                      (nr_sequencia, 
                       dt_atualizacao, 
                       nm_usuario, 
                       cd_pessoa_fisica, 
                       nr_seq_documento, 
                       ie_entregue, 
                       dt_atualizacao_nrec, 
                       nm_usuario_nrec, 
                       ds_arquivo, 
                       dt_entrega, 
                       dt_validade, 
                       ds_observacao, 
                       nr_seq_ageint, 
                       ds_titulo, 
                       ie_situacao, 
                       nr_prescricao, 
                       ie_doc_externo, 
                       cd_convenio, 
                       nm_usuario_exclusao, 
                       dt_exclusao, 
                       ds_motivo_exclusao, 
                       si_special_license, 
                       nr_document, 
                       dt_start) 
          VALUES      ( c01_w.nr_sequencia, 
                       c01_w.dt_atualizacao, 
                       c01_w.nm_usuario, 
                       c01_w.cd_medico, 
                       c01_w.nr_seq_documento, 
                       'S', 
                       c01_w.dt_atualizacao, 
                       c01_w.nm_usuario, 
                       NULL, 
                       c01_w.dt_entrega, 
                       c01_w.dt_validade, 
                       NULL, 
                       NULL, 
                       NULL, 
                       'A', 
                       NULL, 
                       NULL, 
                       NULL, 
                       NULL, 
                       NULL, 
                       NULL, 
                       c01_w.si_special_license, 
                       c01_w.nr_document, 
                       c01_w.dt_start ); 
                        
			DELETE FROM w_pessoa_documentacao 
             WHERE  nr_sequencia = c01_w.nr_sequencia;
			 
        ELSIF (qt_type_doc_w = 0) THEN 
			wheb_mensagem_pck.Exibir_mensagem_abort(nr_seq_mensagem_p => 1142384, 
			vl_macros_p => 'LINE_NUMBER='     
                         || c01_w.nr_line 
                         || ';DS_COLUMN_NAME='   
                         || obter_desc_expressao(299757));
		ELSIF (qt_medico_w = 0) THEN 
			wheb_mensagem_pck.Exibir_mensagem_abort(nr_seq_mensagem_p => 1142384, 
			vl_macros_p => 'LINE_NUMBER='     
                         || c01_w.nr_line 
                         || ';DS_COLUMN_NAME='   
                         || obter_desc_expressao(555908));
		ELSIF (qt_valdomino_w = 0) THEN 
			wheb_mensagem_pck.Exibir_mensagem_abort(nr_seq_mensagem_p => 1142384, 
			vl_macros_p => 'LINE_NUMBER='     
                         || c01_w.nr_line 
                         || ';DS_COLUMN_NAME='   
                         || obter_desc_expressao(969180));
        END IF; 
    END LOOP; 
 COMMIT; 
END;
/