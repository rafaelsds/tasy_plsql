create or replace
procedure HSC_Protocolo_Exames(	nr_prescricao_p		number,
				nm_usuario_p		Varchar2) is 
begin
delete from HSCProtocoloExames
where	nm_usuario = nm_usuario_p;

insert into HSCProtocoloExames(
		ds_observacao,
		nr_prescricao,
		nm_paciente,
		ds_senha,
		ds_idade,
		dt_nascimento,
		nr_atendimento,
		nm_medico,
		ds_sexo,
		ds_convenio,
		dt_entrada,
		dt_entrega,
		ds_setor_coleta,
		ds_setor_entrega,
		ds_exame,
		nm_usuario)
		SELECT distinct
		a.ds_observacao,
		c.nr_prescricao nr_prescricao_paciente,
		a.cd_pessoa_fisica||' - '||DECODE(A.IE_RECEM_NATO, 'S', 'R.N. ', '') || obter_nome_pf(g.cd_pessoa_fisica) nm_paciente,
		d.ds_senha ds_senha, 
		obter_idade_pf(d.cd_pessoa_fisica, SYSDATE, 'C') ds_idade, 
		d.dt_nascimento,
		a.nr_atendimento, 
		obter_nome_medico (a.cd_medico, 'p') nm_medico, 
		g.ds_sexo,
		g.cd_convenio || ' - ' || g.ds_convenio ds_convenio, 
		g.dt_entrada,
		(SELECT x.dt_entrega 
		 FROM prescr_medica x 
		 WHERE a.nr_prescricao = x.nr_prescricao 
		 AND NOT EXISTS (SELECT 1 
				 FROM prescr_procedimento w 
				 WHERE w.nr_prescricao = a.nr_prescricao 
				 AND ie_amostra = 'N')) dt_entrega,
		obter_nome_setor(a.cd_setor_entrega) ds_setor_coleta,
		(SELECT ds_forma 
		 FROM forma_entrega_laudo x 
		 WHERE x.nr_sequencia = a.nr_seq_forma_laudo) ds_setor_entrega,
		 obter_select_concatenado_bv('SELECT INITCAP(SUBSTR(rownum ||''-''||SUBSTR(Obter_Desc_Exame(a.nr_seq_exame),1,240),1,40)) FROM material_exame_lab c,
									   prescr_procedimento a WHERE a.cd_material_exame = c.cd_material_exame AND a.nr_prescricao = :nr_prescr ORDER BY a.nr_sequencia',
								       'nr_prescr='||a.nr_prescricao, ', ') ds_exame,
		nm_usuario_p
		FROM  	pessoa_fisica d, 
		atend_categoria_convenio e, 
		prescr_procedimento c, 
		prescr_medica a, 
		atendimento_paciente_v g
		WHERE   a.nr_prescricao 	= c.nr_prescricao
		AND  	c.nr_sequencia    	= (SELECT x.nr_sequencia 
						   FROM prescr_procedimento x 
						   WHERE x.nr_prescricao = c.nr_prescricao 
						   AND ROWNUM <=1)
		AND    	a.nr_atendimento 	= g.nr_atendimento 
		AND 	a.cd_pessoa_fisica 	= d.cd_pessoa_fisica 
		AND 	a.nr_atendimento 	=  e.nr_atendimento
		AND	a.nr_prescricao		= nr_prescricao_p;


commit;

end HSC_Protocolo_Exames;
/
