create or replace
procedure pro_comunic_tcp_unitariza(nr_seq_lote_p	number) is 

c 	utl_tcp.connection;  -- TCP/IP connection to the Web server  
x	pls_integer;

ie_existe_w	varchar2(1);
ds_material_w	varchar2(255);
ds_barras_w	varchar2(11);
dt_validade_w	varchar2(10);
ds_lote_fornec_w	varchar2(20);

ds_address_w	varchar2(15)	:= '192.168.43.217';
nr_porta_w	number(10)	:= 21000;

ds_mensagem_w	varchar2(4000);

begin
begin
select	a.ds_lote_fornec,
	to_char(a.dt_validade,'dd/mm/yyyy'),
	adiciona_zeros_esquerda(a.nr_sequencia||nr_digito_verif,11),
	b.ds_material
into	ds_lote_fornec_w,
	dt_validade_w,
	ds_barras_w,
	ds_material_w
from	material_lote_fornec a,
	material b
where	a.cd_material = b.cd_material
and	a.nr_sequencia = nr_seq_lote_p;	
exception
when others then
	Raise_application_error(-20011,'Falha ao buscar dados do lote: ' || nr_seq_lote_p || ' - ' || sqlerrm);
end;

ds_mensagem_w	:= substr(chr(2) || '~JS0|POA|0|NOME|' || ds_material_w || '|VALIDADE|' || dt_validade_w || '|LOTE|' || ds_lote_fornec_w || '|CODIGO|' || ds_barras_w || '|' || chr(3),1,4000);
	
begin
c := utl_tcp.open_connection(ds_address_w, nr_porta_w);	
exception
when others then
	Raise_application_error(-20011,'Falha na conex�o: ' || sqlerrm);
end;
begin
x := utl_tcp.write_line(c, ds_mensagem_w);
exception
when others then
	Raise_application_error(-20011,'Falha no envio de mensagem 1: ' || sqlerrm);
end;
begin
x := utl_tcp.write_line(c);
exception
when others then
	Raise_application_error(-20011,'Falha no envio de mensagem 2: ' || sqlerrm);
end;

begin
utl_tcp.close_connection(c);
exception
when others then
	Raise_application_error(-20011,'Falha ao desconectar: ' || sqlerrm);
end;			
end pro_comunic_tcp_unitariza;
/
