create or replace
procedure INTPD_REST_RECEIVE_EXECUTE(	NR_SEQUENCIA_P			in number,
										ds_message_p			in clob,
										ie_evento_p				in	varchar2,
										nr_seq_sistema_p		in	number,
										ie_method_p				in varchar2) is
										
--nr_sequencia_w			number(10);
nr_seq_evento_sistema_w	intpd_eventos_sistema.nr_sequencia%type;
ds_procedure_w			intpd_eventos_sistema.ds_procedure%type;

c001	  	Integer;		
retorno_w	Number(5);

begin
/*Necess�rio para um to_char n�o retirar a hora da data nas rotinas que recebem valores desse tipo de dado*/
EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS'''; 


begin
	select	b.nr_sequencia
	into	nr_seq_evento_sistema_w
	from	intpd_eventos a,
			intpd_eventos_sistema b
	where	b.nr_seq_evento = a.nr_sequencia
	and		exists	(select	1
					from	intpd_config_eventos x
					where	x.ie_evento = a.ie_evento
					and		x.ie_envia_recebe = 'R')
	and		a.ie_situacao = 'A'
	and		b.ie_situacao = 'A'
	and		a.ie_evento = ie_evento_p
	and		b.nr_seq_sistema = nr_seq_sistema_p
	and		b.ie_protocolo_integracao = 'REST'
	and		rownum = 1;
exception
when others then
	nr_seq_evento_sistema_w	:=	0;
end;


wheb_usuario_pck.set_ie_executar_trigger('N');

if	(nr_seq_evento_sistema_w > 0) then
	
	begin

		select	ds_procedure
		into	ds_procedure_w
		from	intpd_eventos_sistema
		where	nr_sequencia = nr_seq_evento_sistema_w;

		if	(nvl(ds_procedure_w,'NULL') <> 'NULL') then
	
			exec_sql_dinamico('INTPDTASY', 'declare begin ' || ds_procedure_w || '(' || nr_sequencia_p || '); end;');

		else
			update	intpd_fila_transmissao
			set		ie_status_http = '200'
			where	nr_sequencia = nr_sequencia_p;
		end if;

	exception
	when others then
		wheb_usuario_pck.set_ie_executar_trigger('S');
		wheb_mensagem_pck.exibir_mensagem_abort(sqlerrm);		
	end;
end if;

wheb_usuario_pck.set_ie_executar_trigger('S');

commit;

end INTPD_REST_RECEIVE_EXECUTE;
/