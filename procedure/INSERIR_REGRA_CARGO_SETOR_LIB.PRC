create or replace
procedure inserir_regra_cargo_setor_lib( nr_seq_regra_cargo_p	number,
				cd_setor_atendimento_p	number,
				 nm_usuario_p		Varchar2) is 

begin

insert into regra_cargo_setor_lib (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,        
		dt_atualizacao_nrec,
		nm_usuario_nrec,   
		nr_seq_regra_cargo,
		cd_setor_atendimento)
values(	regra_cargo_setor_lib_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_regra_cargo_p,
		cd_setor_atendimento_p);

commit;

end inserir_regra_cargo_setor_lib;
/