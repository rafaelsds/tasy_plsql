create or replace
procedure gpw_gerar_pendencia_ordem_serv(
				nr_sequencia_p	number,
				nm_usuario_p	varchar2) is
				
ds_titulo_w			varchar2(80);
ds_pendencia_w			varchar2(4000);
qt_existe_w			number(10,0);
nr_seq_pendencia_w		number(10);
nr_seq_classif_w			number(10);
nr_seq_mod_impl_w		number(10);
cd_funcao_w			number(5);
nr_seq_indicador_w			number(10);
cd_relatorio_w			number(5);
cd_classif_relat_w			varchar2(4);
nm_tabela_w			varchar2(50);

/*	Esta procedure � utilizada na base Corp para a gera��o das pend�ncias - fun��o Gest�o de Produtos Philips	*/

Cursor C01 is
	select	a.nr_seq_classif,
		a.nr_seq_mod_impl,
		a.cd_funcao,
		a.nr_seq_indicador,
		a.cd_relatorio,
		a.cd_classif_relat,
		a.nm_tabela
	from	(select	gpw_obter_seq_classif_pend('F') nr_seq_classif,
			a.nr_seq_mod_impl,
			a.cd_funcao,
			null nr_seq_indicador,
			null cd_relatorio,
			null cd_classif_relat,
			null nm_tabela
		from	funcao@whebl02_orcl a
		where	a.ie_situacao <> 'I'
		and	a.nr_seq_ordem_serv = nr_sequencia_p
		union all
		select	gpw_obter_seq_classif_pend('I') nr_seq_classif,
			null nr_seq_mod_impl,
			null cd_funcao,
			a.nr_sequencia nr_seq_indicador,
			null cd_relatorio,
			null cd_classif_relat,
			null nm_tabela
		from	indicador_gestao@whebl02_orcl a
		where	a.ie_situacao = 'A'
		and	a.nr_seq_ordem_serv = nr_sequencia_p
		union all
		select	gpw_obter_seq_classif_pend('P') nr_seq_classif,
			null nr_seq_mod_impl,
			null cd_funcao,
			null nr_seq_indicador,
			null cd_relatorio,
			null cd_classif_relat,
			null nm_tabela
		from	prontuario_item@whebl02_orcl a
		where	a.ie_situacao = 'A'
		and	a.nr_seq_ordem_serv = nr_sequencia_p
		union all
		select	gpw_obter_seq_classif_pend('P') nr_seq_classif,
			null nr_seq_mod_impl,
			null cd_funcao,
			null nr_seq_indicador,
			null cd_relatorio,
			null cd_classif_relat,
			null nm_tabela
		from	pepo_item@whebl02_orcl a
		where	a.nr_seq_ordem_serv = nr_sequencia_p
		union all
		select	gpw_obter_seq_classif_pend('P') nr_seq_classif,
			null nr_seq_mod_impl,
			null cd_funcao,
			null nr_seq_indicador,
			null cd_relatorio,
			null cd_classif_relat,
			null nm_tabela
		from	oftalmologia_item@whebl02_orcl a
		where	a.nr_seq_ordem_serv = nr_sequencia_p
		union all
		select	gpw_obter_seq_classif_pend('R') nr_seq_classif,
			null nr_seq_mod_impl,
			null cd_funcao,
			null nr_seq_indicador,
			a.cd_relatorio,
			a.cd_classif_relat,
			null nm_tabela
		from	relatorio@whebl02_orcl a
		where	substr(a.cd_classif_relat,1,1) = 'W'
		and	a.nr_seq_ordem_serv = nr_sequencia_p
		union all
		select	gpw_obter_seq_classif_pend('T') nr_seq_classif,
			a.nr_seq_mod_impl,
			null cd_funcao,
			null nr_seq_indicador,
			null cd_relatorio,
			null cd_classif_relat,
			a.nm_tabela
		from	tabela_sistema@whebl02_orcl a
		where	a.ie_situacao <> 'I'
		and	a.nr_seq_ordem_serv = nr_sequencia_p
		union all
		select	gpw_obter_seq_classif_pend('A') nr_seq_classif,
			null nr_seq_mod_impl,
			null cd_funcao,
			null nr_seq_indicador,
			null cd_relatorio,
			null cd_classif_relat,
			null nm_tabela
		from	man_ordem_ativ_prev a
		where	a.nr_seq_ordem_serv = nr_sequencia_p
		and	a.nr_seq_ativ_exec in (2,5)
		and	rownum < 2) a
	where	a.nr_seq_classif is not null
	order by 1;

begin

if	(nvl(nr_sequencia_p,0) <> 0) and
	(nvl(nm_usuario_p,'X') <> 'X') then
	begin
	select	gpw_pendencia_seq.nextval
	into	nr_seq_pendencia_w
	from	dual;
	
	ds_titulo_w	:= substr('Pend�ncia gerada a partir da OS ' || nr_sequencia_p,1,80);
	ds_pendencia_w	:= substr('Esta pend�ncia foi gerada a partir da ordem de servi�o n� ' || nr_sequencia_p || '.',1,4000);
	
	insert into gpw_pendencia(
			nr_sequencia,
			ds_titulo,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_pendencia,
			ie_status,
			nr_seq_ordem_serv)
		values(	nr_seq_pendencia_w,
			ds_titulo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ds_pendencia_w,
			'P',
			nr_sequencia_p);
			
	open C01;
	loop
	fetch C01 into	
		nr_seq_classif_w,
		nr_seq_mod_impl_w,
		cd_funcao_w,
		nr_seq_indicador_w,
		cd_relatorio_w,
		cd_classif_relat_w,
		nm_tabela_w;
	exit when C01%notfound;
		begin
		insert into gpw_pendencia_classif( 
					nr_sequencia,
					nr_seq_pendencia,
					nr_seq_classif,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_indicador,
					cd_funcao,
					nm_tabela,
					cd_relatorio,
					cd_classif_relat,
					nr_seq_mod_impl)
				values(	gpw_pendencia_classif_seq.nextval,
					nr_seq_pendencia_w,
					nr_seq_classif_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_indicador_w,
					cd_funcao_w,
					nm_tabela_w,
					cd_relatorio_w,
					cd_classif_relat_w,
					nr_seq_mod_impl_w);
		end;
	end loop;
	close C01;
	
	--commit;
	end;
end if;

end gpw_gerar_pendencia_ordem_serv;
/