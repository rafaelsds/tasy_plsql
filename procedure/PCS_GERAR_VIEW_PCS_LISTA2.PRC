create or replace 
procedure pcs_gerar_view_pcs_lista2(nm_usuario_p varchar2) is 

qt_existe_objeto_w	number(5);
ds_comando_w		varchar2(4000);
begin

select	count(*)
into	qt_existe_objeto_w
from	user_objects
where	object_name = 'PCS_ANALISE_COLUNAS_V';

if (qt_existe_objeto_w = 0) then
	ds_comando_w	:= 'create or replace view pcs_analise_colunas_v as';
	ds_comando_w	:= substr(ds_comando_w || ' ' || 'select ''0'' cd_material from dual',1,4000);	
end if;

commit;

exec_sql_dinamico('TASY',ds_comando_w);

end pcs_gerar_view_pcs_lista2;
/