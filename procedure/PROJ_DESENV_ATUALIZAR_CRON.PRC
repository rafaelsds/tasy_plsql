create or replace
procedure proj_desenv_atualizar_cron (
		nr_seq_cronograma_p	number,
		nm_usuario_p		varchar2) is
		
begin
if	(nr_seq_cronograma_p is not null) and
	(nm_usuario_p is not null) then
	begin
	proj_desenv_atual_perc_cron(nr_seq_cronograma_p, nm_usuario_p);
	end;
end if;
commit;
end proj_desenv_atualizar_cron;
/