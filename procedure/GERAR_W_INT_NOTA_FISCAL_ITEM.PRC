create or replace
procedure gerar_w_int_nota_fiscal_item(
				cd_centro_custo_p		varchar2,                 
				cd_conta_contabil_p		varchar2,               
				cd_local_estoque_p		varchar2,                
				cd_marca_p			varchar2,                        
				cd_material_p			varchar2,                     
				cd_setor_atendimento_p		varchar2,            
				cd_unidade_medida_compra_p	varchar2,        
				ds_complemento_p		varchar2,                  
				ds_observacao_p			varchar2,                                         
				nm_usuario_p			varchar2,                            
				nr_seq_int_nf_p			varchar2,                                
				pr_desconto_p			varchar2,                     
				qt_item_nf_p			varchar2,                      
				vl_desconto_p			varchar2,                     
				vl_despesa_acessoria_p		varchar2,            
				vl_frete_p			varchar2,                        
				vl_seguro_p			varchar2,                       
				vl_unitario_item_nf_p		varchar2,
				cd_barras_material_p		varchar2,
				cd_lote_fabricacao_p		varchar2,
				dt_validade_p			varchar2) is  

ds_erro_w	varchar2(4000);				
				
begin

begin
insert into w_int_nota_fiscal_item (	
			nr_sequencia,           
			cd_centro_custo,                 
			cd_conta_contabil,               
			cd_local_estoque,                
			cd_marca,                        
			cd_material,                     
			cd_setor_atendimento,            
			cd_unidade_medida_compra,        
			ds_complemento,                  
			ds_observacao,                   
			dt_atualizacao,           
			dt_atualizacao_nrec,             
			nm_usuario,               
			nm_usuario_nrec,                 
			nr_seq_int_nf,                                
			pr_desconto,                     
			qt_item_nf,                      
			vl_desconto,                     
			vl_despesa_acessoria,            
			vl_frete,                        
			vl_seguro,                       
			vl_unitario_item_nf,
			cd_barras_material,
			cd_lote_fabricacao,
			dt_validade)
values(	w_int_nota_fiscal_item_seq.nextval,                           
			cd_centro_custo_p,                 
			cd_conta_contabil_p,               
			cd_local_estoque_p,                
			cd_marca_p,                        
			cd_material_p,                     
			cd_setor_atendimento_p,            
			cd_unidade_medida_compra_p,        
			ds_complemento_p,                  
			ds_observacao_p,                   
			sysdate,           
			sysdate,             
			nm_usuario_p,               
			nm_usuario_p,                 
			nr_seq_int_nf_p,                                
			pr_desconto_p,                     
			qt_item_nf_p,                      
			vl_desconto_p,                     
			vl_despesa_acessoria_p,            
			vl_frete_p,                        
			vl_seguro_p,                       
			vl_unitario_item_nf_p,
			cd_barras_material_p,
			cd_lote_fabricacao_p,
			dt_validade_p);
	
exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,4000);
end;


commit;

end gerar_w_int_nota_fiscal_item;
/
