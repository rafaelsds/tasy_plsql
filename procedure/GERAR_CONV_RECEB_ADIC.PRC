create or replace
procedure gerar_conv_receb_adic
		(nr_seq_receb_p			number,
		vl_adicional_p			number,
		nr_seq_trans_financ_p		number,
		nm_usuario_p			varchar2) is

begin

if	(nvl(vl_adicional_p,0) <> 0) and (nr_seq_trans_financ_p is not null) then

	insert	into convenio_receb_adic
		(dt_atualizacao,
		nm_usuario,
		nr_lote_contabil,
		nr_seq_receb,
		nr_seq_trans_financ,
		nr_sequencia,
		vl_adicional)
	values	(sysdate,
		nm_usuario_p,
		0,
		nr_seq_receb_p,
		nr_seq_trans_financ_p,
		convenio_receb_adic_seq.nextval,
		vl_adicional_p);
end if;

end gerar_conv_receb_adic;
/