create or replace
procedure	excluir_w_importa_cliente(
			nr_sequencia_p		number) is

begin

delete
from	w_importa_cliente_incons
where	nr_seq_cliente = nr_sequencia_p;

delete
from	w_importa_conta_cliente
where	nr_seq_cliente = nr_sequencia_p;

delete
from	w_importa_clientes
where	nr_sequencia = nr_sequencia_p;

commit;

end excluir_w_importa_cliente;
/