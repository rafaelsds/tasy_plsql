create or replace
procedure adps_gerar_cobr_santander_400	(nr_seq_cobr_escrit_p	number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p		varchar2) is 

ds_conteudo_w			varchar2(400);
ds_zeros_16_w			varchar2(16);
ds_mensagem_275_w		varchar2(275);
nr_seq_reg_arq_w		number(6) := 0;
nm_empresa_w			varchar2(30);
nm_banco_w			varchar2(15);
dt_geracao_w			varchar2(6);
cd_banco_san_w			varchar2(3);
cd_transmissao_w		varchar2(20);
ds_zeros_374_w			varchar2(374);
ds_brancos_6_w			varchar2(6);
ds_brancos_4_w			varchar2(4);
cd_cgc_w			varchar2(14);
cd_agencia_ced_w		varchar2(4);
cd_conta_movto_w		varchar2(8);
cd_conta_cobr_w			varchar2(8);
nr_controle_w			varchar2(25);
nr_nosso_numero_w		varchar2(8);
dt_seg_desconto_w		varchar2(6);
ie_multa_w			varchar2(1);
pr_multa_w			varchar2(4);
vl_moeda_w			varchar2(2);
vl_tit_unidade_w		varchar2(13);
dt_cobr_multa_w			varchar2(6);
cd_carteira_w			varchar2(1);
cd_ocorrencia_w			varchar2(2);
nr_seu_numero_w			varchar2(10);
dt_vencimento_w			varchar2(6);
ds_vl_titulo_w			varchar2(13);
vl_titulo_w			number(15,2);
cd_banco_w			varchar2(3);
cd_agencia_w			varchar2(5);
ie_documento_w			varchar2(2);
ie_aceite_w			varchar2(1);
dt_emissao_w			varchar2(6);
ie_primeira_instr_w		varchar2(2);
ie_segunda_intr_w		varchar2(2);
vl_juros_diario_w		varchar2(13);
dt_desconto_w			varchar2(6);
vl_desconto_dia_w		varchar2(13);
vl_iof_w				varchar2(13);
vl_abatimento_w			varchar2(13);
ie_tipo_inscricao_w		varchar2(2);
nr_inscricao_w			varchar2(14);
nm_sacado_w				varchar2(40);
ds_endereco_sacado_w	varchar2(40);
ds_bairro_sacado_w		varchar2(12);
cd_cep_sacado_w			varchar2(8);
ds_municipio_sacado_w	varchar2(15);
ds_estado_sacado_w		varchar2(2);
nm_sacador_w			varchar2(30);
ie_complemento_w		varchar2(1);
ds_complemento_w		varchar2(2);
nr_dias_baixa_w			varchar2(2);
vl_total_w				number(13,2) := 0;
nr_sequencia_w			number(15);
qt_registro_w			number(6) :=0;


/*UTL File*/
arq_texto_w			utl_file.file_type;
ds_erro_w			varchar2(255);
ds_local_w			varchar2(255);
nm_arquivo_w		varchar2(255);
ds_mensagem_w		varchar2(255);


Cursor C01 is
	select	lpad(nvl(to_char(c.cd_agencia_bancaria),'0'),4,'0') cd_agencia,
		lpad(substr(nvl(d.cd_conta,nvl(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'C'),'0')),1,8),8,'0') cd_conta_movto,
		lpad(substr(nvl(x.cd_conta,'0'),1,8),8,'0') cd_conta_cobr,
		lpad('0',25,'0')  nr_controle,
		lpad(nvl(substr(obter_nosso_numero_interf(x.cd_banco,b.nr_titulo),1,8),'0'),8,'0') nr_nosso_numero,
		'000000' dt_seg_desconto,
		'0' ie_multa,
		'0000' pr_multa,
		'00' vl_moeda,
		replace(to_char(b.vl_titulo, 'fm00000000000.00'),'.','') vl_tit_unidade,
		'000000' dt_cobr_multa,
		'1' cd_carteira,
		'01' cd_ocorrencia,
		rpad(nvl(substr(b.nr_titulo,1,10),'0'),10,'0') nr_seu_numero,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyy') dt_vencimento,
		nvl(b.vl_titulo,0) vl_titulo,
		lpad(nvl(substr(x.cd_banco,1,3),'0'),3,'0') cd_banco,
		lpad(nvl(to_char(c.cd_agencia_bancaria),'0'),5,'0') cd_agencia,
		'01' ie_documento,
		'N' ie_aceite,
		lpad(nvl(to_char(b.dt_emissao,'ddmmyy'),' '),6,' ') dt_emissao,
		'00' ie_primeira_instr,
		'00' ie_segunda_intr,
		lpad(replace(to_char(nvl(obter_vl_juros_diario_tit(null,b.nr_titulo),0),'fm00000000000.00'),'.',''),13,'0') vl_juros_diario,
		to_char(sysdate,'ddmmyy') dt_desconto,
		lpad('0',13,'0') vl_desconto_dia,
		lpad('0',13,'0') vl_iof,
		lpad('0',13,'0') vl_abatimento,
		decode(b.cd_cgc,null,'01','02') ie_tipo_inscricao, 
		lpad(nvl(b.cd_cgc_cpf,'0'),14,'0') nr_inscricao,
		rpad(upper(elimina_acentuacao(substr(nvl(b.nm_pessoa,' '),1,40))),40,' ') nm_sacado,
		rpad(nvl(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'E')),1,40),
			substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),' '),1,40)),40,' ') ds_endereco_sacado,
		rpad(nvl(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,12),
			substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),' '),1,12)),12,' ') ds_bairro_sacado,
		lpad(nvl(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8),
			substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),'0'),1,8)),8,'0') cd_cep_sacado,
		rpad(nvl(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,15),
			substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),1,15)),15,' ') ds_municipio_sacado,
		rpad(nvl(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2),
			substr(nvl(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),' '),1,2)),2,' ') ds_estado_sacado,
		lpad(' ',30,' ') nm_sacador,
		'I' ie_complemento,
		'38' ds_complemento,
		'00' nr_dias_baixa
	from	banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v	b,
		titulo_receber_cobr	c,
		cobranca_escritural	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador	= f.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;			
	

begin
nm_arquivo_w	:= to_char(sysdate,'ddmmyyyy') || to_char(sysdate,'hh24') || to_char(sysdate,'mi') || to_char(sysdate,'ss') || nm_usuario_p || '.rem';

obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);

begin
arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W'); --arq_texto_w := utl_file.fopen('/srvfs03/FINANCEIRO/TASY/',nm_arquivo_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;	
	wheb_mensagem_pck.exibir_mensagem_abort(186768,'DS_ERRO_W=' || ds_erro_w);
end;
--arq_texto_w := utl_file.fopen('/oraprd03/utlfile/',nm_arquivo_w,'W');
--\\192.168.0.230\UTLFILE

update	cobranca_escritural
set	ds_arquivo	= ds_local_w || nm_arquivo_w
where	nr_sequencia	= nr_seq_cobr_escrit_p;
	

select	rpad('0',16,'0'),
	rpad(' ',275,' '),
	rpad('0',374,'0'),
	rpad(' ',6,' '),
	rpad(' ',4,' ')
into	ds_zeros_16_w,
	ds_mensagem_275_w,
	ds_zeros_374_w,
	ds_brancos_6_w,
	ds_brancos_4_w
from	dual;


/* Header */
select	rpad(substr(obter_nome_pf_pj(null,b.cd_cgc),1,30),30,' ') nm_empresa,
	rpad(substr(upper(obter_nome_banco(c.cd_banco)),1,15),15,' ') nm_banco,
	to_char(sysdate, 'ddmmyy') dt_geracao,
	lpad(substr(c.cd_banco,1,3),3,'0') cd_banco,
	lpad(nvl(substr(c.cd_transmissao,1,20),'0'),20,'0') cd_transmissao,
	lpad(substr(b.cd_cgc,1,14),14,'0') cd_cgc
into	nm_empresa_w,
	nm_banco_w,
	dt_geracao_w,
	cd_banco_san_w,
	cd_transmissao_w,
	cd_cgc_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

nr_seq_reg_arq_w := nr_seq_reg_arq_w + 1;
qt_registro_w := qt_registro_w + 1;
	

ds_conteudo_w	:=	'01REMESSA01COBRAN�A       '	|| 
			cd_transmissao_w		|| 
			nm_empresa_w 			||
			cd_banco_san_w 			|| 
			nm_banco_w			|| 
			dt_geracao_w			|| 
			ds_zeros_16_w			|| 
			ds_mensagem_275_w		||
			'000'				||
			lpad(nr_seq_reg_arq_w,6,'0');

utl_file.put_line(arq_texto_w,ds_conteudo_w);
utl_file.fflush(arq_texto_w);			
			

/* Fim Header  */

/* Registro */
open C01;
loop
fetch C01 into	
	cd_agencia_ced_w,
	cd_conta_movto_w,
	cd_conta_cobr_w,
	nr_controle_w,
	nr_nosso_numero_w,
	dt_seg_desconto_w,
	ie_multa_w,
	pr_multa_w,
	vl_moeda_w,
	vl_tit_unidade_w,
	dt_cobr_multa_w,
	cd_carteira_w,
	cd_ocorrencia_w,
	nr_seu_numero_w,
	dt_vencimento_w,
	vl_titulo_w,	
	cd_banco_w,
	cd_agencia_w,
	ie_documento_w,
	ie_aceite_w,
	dt_emissao_w,
	ie_primeira_instr_w,
	ie_segunda_intr_w,
	vl_juros_diario_w,
	dt_desconto_w,
	vl_desconto_dia_w,
	vl_iof_w,
	vl_abatimento_w,
	ie_tipo_inscricao_w, 
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	ds_bairro_sacado_w,
	cd_cep_sacado_w,
	ds_municipio_sacado_w,
	ds_estado_sacado_w,
	nm_sacador_w,
	ie_complemento_w,
	ds_complemento_w,
	nr_dias_baixa_w;
exit when C01%notfound;
	begin
	nr_seq_reg_arq_w := nr_seq_reg_arq_w + 1;

	ds_vl_titulo_w	:= lpad(replace(to_char(vl_titulo_w,'fm00000000000.00'),'.',''),13,'0');
	
	ds_conteudo_w	:=	'102' 			||
				cd_cgc_w		||
				cd_transmissao_w	||
				nr_controle_w		||
				nr_nosso_numero_w	||
				dt_seg_desconto_w	||
				' '			||
				ie_multa_w		||
				pr_multa_w		||
				vl_moeda_w		||
				vl_tit_unidade_w	||
				ds_brancos_4_w		|| --98
				dt_cobr_multa_w		|| --102
				cd_carteira_w		||
				cd_ocorrencia_w		||
				nr_seu_numero_w		||
				dt_vencimento_w		||
				ds_vl_titulo_w		||
				cd_banco_w		||
				cd_agencia_w		||
				ie_documento_w		||
				ie_aceite_w		||
				dt_emissao_w		||
				ie_primeira_instr_w	||
				ie_segunda_intr_w	||
				vl_juros_diario_w	||
				dt_desconto_w		||
				vl_desconto_dia_w	||
				vl_iof_w		||
				vl_abatimento_w		||
				ie_tipo_inscricao_w	||
				nr_inscricao_w		||
				nm_sacado_w		||
				ds_endereco_sacado_w	||
				ds_bairro_sacado_w	||
				cd_cep_sacado_w		||
				ds_municipio_sacado_w	||
				ds_estado_sacado_w	||
				nm_sacador_w		||
				' '			||
				ie_complemento_w	||
				ds_complemento_w	||
				ds_brancos_6_w		||
				nr_dias_baixa_w		||
				' '					||
				lpad(nr_seq_reg_arq_w,6,'0');
	
	utl_file.put_line(arq_texto_w, ds_conteudo_w);
	utl_file.fflush(arq_texto_w);	
	
	vl_total_w := vl_total_w + vl_titulo_w;
	qt_registro_w := qt_registro_w + 1;
	end;
	
end loop;
close C01;
/* Fim Registro */

/* Trailler */

nr_seq_reg_arq_w := nr_seq_reg_arq_w + 1;
qt_registro_w := qt_registro_w + 1;

ds_conteudo_w	:=	'9' 				|| 
			lpad(qt_registro_w,6,'0')	||
			lpad(replace(to_char(vl_total_w,'fm000000000.99'),'.',''),13,'0')		||
			ds_zeros_374_w			||
			lpad(nr_seq_reg_arq_w,6,'0');
				
			

utl_file.put_line(arq_texto_w, ds_conteudo_w);
utl_file.fflush(arq_texto_w);

/* Fim Trailler */

commit;

end adps_gerar_cobr_santander_400;
/