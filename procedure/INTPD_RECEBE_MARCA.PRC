create or replace
procedure intpd_recebe_marca(
	nr_sequencia_p	in	number,
	xml_p		in	xmltype) is 

marca_w			marca%rowtype;
ie_conversao_w		intpd_eventos_sistema.ie_conversao%type;
nr_seq_projeto_xml_w	intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_sistema_w	intpd_eventos_sistema.nr_seq_sistema%type;
ie_sistema_externo_w	varchar2(15);
reg_integracao_w	gerar_int_padrao.reg_integracao_conv;
nr_seq_regra_w		conversao_meio_externo.nr_seq_regra%type;
ds_erro_w		varchar2(4000);
i			integer;
ie_atualizou_w		varchar2(1) := 'N';

/*'Efetua a consulta transformando o elemento XML num tipo de tabela'*/
cursor c01 is
select	*
from	xmltable('/STRUCTURE/BRAND' passing xml_p columns
	ie_action		varchar2(15)	path	'IE_ACTION',
	ie_oparation		varchar2(15)	path	'IE_OPARATION',
	nr_sequencia		varchar2(40)	path	'NR_SEQUENCE',
	ds_marca		varchar2(80)	path	'DS_BRAND',
	ie_situacao		varchar2(15)	path	'IE_STATUS',
	dt_atualizacao		varchar2(40)	path	'DT_UPDATE',
	nm_usuario		varchar2(40)	path	'NM_USER',
	nr_seq_tipo		varchar2(40)	path	'NR_SEQ_TYPE',
	qt_dia_ressup		number(3)	path	'QT_DIA_RESSUP',
	dt_atualizacao_nrec	varchar2(40)	path	'DT_NREC_UPDATE',
	nm_usuario_nrec		varchar2(40)	path	'NM_NREC_USER',
	dt_reprovacao		varchar2(40)	path	'DT_DISAPPROVAL',
	nm_usuario_reprovacao	varchar2(40)	path	'NM_DISAPPROVAL_USER',
	ds_observacao		varchar2(255)	path	'DS_NOTE',
	cd_sistema_ant		varchar2(40)	path	'CD_BRANDS_ID',
	cd_cnpj_fabricante	varchar2(40)	path	'CD_MANUFACTURER');
	
c01_w	c01%rowtype;
	
begin
/*'Atualiza o status da fila para Em processamento'*/
update	intpd_fila_transmissao
set	ie_status = 'R'
where	nr_sequencia = nr_sequencia_p;

/*'Realiza o commit para n�o permite o status de processamento em casa de rollback por existir consist�ncia. Existe tratamento de exce��o abaixo para colocar o status de erro em caso de falha'*/
commit;

/*'In�cio de controle de falha'*/
begin
/*'Busca os dados da regra do registro da fila que est� em processamento'*/
select	nvl(b.ie_conversao,'I'),
	nr_seq_sistema,
	nr_seq_projeto_xml,
	nr_seq_regra_conv
into	ie_conversao_w,
	nr_seq_sistema_w,
	nr_seq_projeto_xml_w,
	nr_seq_regra_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;

ie_sistema_externo_w	:=	nr_seq_sistema_w;

/*'Alimenta as informa��es iniciais de controle e consist�ncia de cada atributo do XML'*/
reg_integracao_w.nr_seq_fila_transmissao	:=	nr_sequencia_p;
reg_integracao_w.ie_envio_recebe		:=	'R';
reg_integracao_w.ie_sistema_externo		:=	ie_sistema_externo_w;
reg_integracao_w.ie_conversao			:=	ie_conversao_w;
reg_integracao_w.nr_seq_projeto_xml		:=	nr_seq_projeto_xml_w;
reg_integracao_w.nr_seq_regra_conversao		:=	nr_seq_regra_w;
reg_integracao_w.intpd_log_receb.delete;
reg_integracao_w.qt_reg_log			:=	0;

open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin
	/*'Alimenta as informa��es de controle e consist�ncia referente ao Elemento a ser processado no momento. � importante manter dentro do cursor e n�o fora.'*/
	reg_integracao_w.nm_tabela		:=	'MARCA';
	reg_integracao_w.nm_elemento	:=	'BRAND';
	reg_integracao_w.nr_seq_visao	:=	36147;
	
	if	(ie_conversao_w = 'I') then
		marca_w.nr_sequencia	:=	bkf_obter_conv_interna(null, 'MARCA', 'NR_SEQUENCIA', c01_w.nr_sequencia, null, nr_seq_regra_w);
	else
		marca_w.nr_sequencia	:=	c01_w.nr_sequencia;
	end if;
	
	if	(marca_w.nr_sequencia is not null) then
		begin
		/*'Busca o registro atual do Tasy pela PK'*/
		select	*
		into	marca_w
		from	marca
		where	nr_sequencia = marca_w.nr_sequencia;
		exception
		when others then
			marca_w.nr_sequencia	:=	null;
		end;
	end if;
	
	if	(nvl(c01_w.ie_action,'INSERT') <> 'DELETE') then
		begin
		/*'Consiste cada atributo do XML'*/		
		intpd_processar_atributo(reg_integracao_w, 'DS_MARCA', c01_w.ds_marca, 'N', marca_w.ds_marca);
		intpd_processar_atributo(reg_integracao_w, 'IE_SITUACAO', c01_w.ie_situacao, 'N', marca_w.ie_situacao);
		intpd_processar_atributo(reg_integracao_w, 'DT_ATUALIZACAO', c01_w.dt_atualizacao, 'N', marca_w.dt_atualizacao);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO', c01_w.nm_usuario, 'N', marca_w.nm_usuario);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_TIPO', c01_w.nr_seq_tipo, 'S', marca_w.nr_seq_tipo);
		intpd_processar_atributo(reg_integracao_w, 'QT_DIA_RESSUP', c01_w.qt_dia_ressup, 'N', marca_w.qt_dia_ressup);
		intpd_processar_atributo(reg_integracao_w, 'DT_ATUALIZACAO_NREC', c01_w.dt_atualizacao_nrec, 'N', marca_w.dt_atualizacao_nrec);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO_NREC', c01_w.nm_usuario_nrec, 'N', marca_w.nm_usuario_nrec);
		intpd_processar_atributo(reg_integracao_w, 'DT_REPROVACAO', c01_w.dt_reprovacao, 'N', marca_w.dt_reprovacao);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO_REPROVACAO', c01_w.nm_usuario_reprovacao, 'N', marca_w.nm_usuario_reprovacao);
		intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c01_w.ds_observacao, 'N', marca_w.ds_observacao);
		intpd_processar_atributo(reg_integracao_w, 'CD_SISTEMA_ANT', c01_w.cd_sistema_ant, 'N', marca_w.cd_sistema_ant);
		intpd_processar_atributo(reg_integracao_w, 'CD_CNPJ_FABRICANTE', c01_w.cd_cnpj_fabricante, 'N', marca_w.cd_cnpj_fabricante);
	
		/*'Efetua a atualiza��o caso n�o possua consist�ncia '*/
		
		if	(reg_integracao_w.qt_reg_log = 0) then
			begin
			
			select	max(nr_sequencia)
			into	marca_w.nr_sequencia
			from	marca
			where	cd_sistema_ant = c01_w.cd_sistema_ant;
			
			
			
			if	(marca_w.nr_sequencia is not null) then
				begin
				update	marca
				set	row = marca_w
				where	nr_sequencia = marca_w.nr_sequencia;
				end;
			else	
				begin		
				select	marca_seq.nextval
				into	marca_w.nr_sequencia
				from	dual;
				
				insert into marca values marca_w;
				
				/*'Registra a convers�o que poder� ser utilizada em outros pontos de integra��o e para somente atualizar (update) num recebimento futuro do mesmo registro.'*/
				gerar_conv_meio_externo(null, 'MARCA', 'NR_SEQUENCIA', marca_w.nr_sequencia, c01_w.nr_sequencia, null, nr_seq_regra_w, 'A', marca_w.nm_usuario);
				end;
			end if;
			
			ie_atualizou_w	:= 'S';
			end;
		end if;
		end;
	else
		delete	marca
		where	nr_sequencia = marca_w.nr_sequencia;
		
		ie_atualizou_w	:= 'S';
	end if;
	
	if	(ie_atualizou_w = 'S')	then
		update	intpd_fila_transmissao
		set	ie_status = 'S',
			nr_seq_documento = marca_w.nr_sequencia
		where	nr_sequencia = nr_sequencia_p;
	end if;
	
	end;
	
end loop;
close c01;
exception
when others then
	begin
	/*'Em caso de qualquer falha o sistema captura a mensagem de erro, efetua o rollback, atualiza o status para Erro e registra a falha ocorrida'*/
	ds_erro_w	:=	substr(sqlerrm,1,4000);
	rollback;
	update	intpd_fila_transmissao
	set	ie_status = 'E',
		ds_log = ds_erro_w
	where	nr_sequencia = nr_sequencia_p;
	end;
end;

if	(reg_integracao_w.qt_reg_log > 0) then
	begin
	/*'Em caso de qualquer consist�ncia o sistema efetua rollback, atualiza o status para Erro e registra todos os logs de consist�ncia'*/
	rollback;
	
	update	intpd_fila_transmissao
	set	ie_status = 'E',
		ds_log = ds_erro_w
	where	nr_sequencia = nr_sequencia_p;
	
	for i in 0..reg_integracao_w.qt_reg_log-1 loop
		intpd_gravar_log_recebimento(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');
	end loop;
	end;
end if;
	
commit;
end intpd_recebe_marca;
/