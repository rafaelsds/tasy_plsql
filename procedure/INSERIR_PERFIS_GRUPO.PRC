create or replace
procedure inserir_perfis_grupo(
			nm_usuario_p		Varchar2,
			nr_seq_grupo_perfil_p	number) is 

cd_perfil_w	number(5);
				
Cursor C01 is
select	a.cd_perfil
from	grupo_perfil_item a,
	grupo_perfil b
where	a.nr_seq_grupo_perfil =	b.nr_sequencia
and	b.nr_sequencia = nr_seq_grupo_perfil_p
and	not exists(	select	1
			from	grupo_perfil_item c,
				grupo_perfil d,
				usuario_perfil e
			where	c.nr_seq_grupo_perfil =	d.nr_sequencia
			and	e.cd_perfil = c.cd_perfil
			and	d.nr_sequencia	= nr_seq_grupo_perfil_p
			and	e.cd_perfil = a.cd_perfil
			and 	e.nm_usuario	= nm_usuario_p);

begin
open c01;
	loop
	fetch c01 into
		cd_perfil_w;
	exit when c01%notfound;


	insert into usuario_perfil 
			(cd_perfil, 
			ds_observacao, 
			dt_atualizacao, 
			dt_validade, 
			nm_usuario, 
			nm_usuario_atual, 
			nr_seq_apres)	
	values	(	cd_perfil_w,
			'',
			sysdate,
			null,
			nm_usuario_p,
			wheb_usuario_pck.get_nm_usuario,
			null
		);
	end loop;
	close c01;

commit;
end inserir_perfis_grupo;
/
