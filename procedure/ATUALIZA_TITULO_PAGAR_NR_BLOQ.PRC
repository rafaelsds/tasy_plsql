create or replace
Procedure atualiza_titulo_pagar_nr_bloq(nm_usuario_p Varchar2,
										nr_bloqueto_p  Varchar2,
										nr_titulo_p  Number) is 

begin

if	(nm_usuario_p is not null) then
	begin
		 update titulo_pagar set nr_bloqueto = nr_bloqueto_p 
		 where nr_titulo = nr_titulo_p;	
	end;
end if;

commit;

end atualiza_titulo_pagar_nr_bloq;
/
