create or replace
procedure SCHEMATIC_LIB_INATIVAR_REGRA
			(ie_acao_p		varchar2,
			nr_seq_regra_p		number,
			nm_usuario_p		varchar2,
			ds_justificativa_p	varchar2)
			is
			
begin

if	(ie_acao_p = 'I') then

	update	regra_schematic
	set	dt_inativacao 		= sysdate,
		nm_usuario_inativacao 	= nm_usuario_p,
		ie_situacao 		= 'I',
		ds_justificativa	= substr(ds_justificativa_p,1,255)
	where	nr_sequencia		= nr_seq_regra_p;
	
elsif	(ie_acao_p = 'L') then

	update	regra_schematic
	set	dt_liberacao 		= sysdate,
		NM_USUARIO_LIB 		= nm_usuario_p
	where	nr_sequencia		= nr_seq_regra_p;
	
end if;

commit;

end SCHEMATIC_LIB_INATIVAR_REGRA;
/