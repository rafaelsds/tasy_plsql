create or replace
procedure pls_calcular_manut_tab_preco
			(	nr_seq_tabela_p		number,
				nr_seq_plano_preco_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

tx_acrescimo_w			number(15,4);
tx_acrescimo_erro_w		number(17,4);
vl_preco_novo_w			number(15,2);
vl_preco_inicial_w		number(15,2);
vl_preco_ant_w			number(15,2);
qt_idade_inicio_w		number(5);
qt_idade_inicial_w		number(5);
nr_seq_preco_ant_w		number(10);
nr_seq_preco_w			number(10);
ie_tabela_base_w		varchar2(1);
tx_acrescimo_preco_w		number(15,4);
ie_grau_parentesco_w		varchar2(10);

cursor c01 is
	select	nr_sequencia,
		qt_idade_inicial,
		vl_preco_inicial
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_p
	and	nr_sequencia	<> nr_seq_plano_preco_p
	order by qt_idade_inicial;

Cursor C02 is
	select	nr_sequencia
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_p
	and	qt_idade_inicial	< qt_idade_inicial_w
	and	qt_idade_final	= qt_idade_inicial_w -1
	and	nvl(ie_grau_titularidade,ie_grau_parentesco_w)	= ie_grau_parentesco_w
	order	by	nvl(ie_grau_titularidade,' ');

begin

if	(nr_seq_tabela_p is not null) and
	(nvl(nr_seq_tabela_p,0) <> 0) then
	
	select	nvl(min(qt_idade_inicial),0)
	into	qt_idade_inicio_w
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_p;
	
	select	nvl(ie_tabela_base,'N')
	into	ie_tabela_base_w
	from	pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_p;
	
	select	vl_preco_atual,
		tx_acrescimo,
		qt_idade_inicial,
		nvl(ie_grau_titularidade,'X')
	into	vl_preco_inicial_w,
		tx_acrescimo_preco_w,
		qt_idade_inicial_w,
		ie_grau_parentesco_w
	from	pls_plano_preco
	where	nr_sequencia	= nr_seq_plano_preco_p;
	
	open C02;
	loop
	fetch C02 into
		nr_seq_preco_ant_w;
	exit when C02%notfound;
	end loop;
	close C02;
	--Calcula o valor ou o acrescimo da faixa et�ria alterada
	if	(nr_seq_preco_ant_w is not null) then
		select	nvl(vl_preco_atual,0)
		into	vl_preco_ant_w
		from	pls_plano_preco
		where	nr_sequencia	= nr_seq_preco_ant_w;
		if	(vl_preco_inicial_w <> 0) then			
			begin
			tx_acrescimo_w	:= (dividir_sem_round(vl_preco_inicial_w,vl_preco_ant_w) * 100) - 100;
			exception
			when others then
				tx_acrescimo_erro_w	:= (dividir_sem_round(vl_preco_inicial_w,vl_preco_ant_w) * 100) - 100;
				commit;
				
				wheb_mensagem_pck.exibir_mensagem_abort(200503,'QT_IDADE='||qt_idade_inicial_w||';'||'TX_ACRESCIMO='||tx_acrescimo_erro_w);
				--Menssagem: O valor do acr�scimo da faixa et�ria  ||qt_idade_inicial_w||  chegou no limite: ||tx_acrescimo_erro_w||
			end;
			update	pls_plano_preco
			set	tx_acrescimo	= tx_acrescimo_w,
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where	nr_sequencia	= nr_seq_plano_preco_p;
			
		elsif (tx_acrescimo_preco_w <> 0) then
			vl_preco_novo_w	:= vl_preco_ant_w + (vl_preco_ant_w * dividir_sem_round(tx_acrescimo_preco_w,100));
			if	(vl_preco_novo_w is null) then
				vl_preco_novo_w	:= 0;
			end if;
			
			update	pls_plano_preco
			set	vl_preco_inicial	= vl_preco_novo_w,
				vl_preco_atual		= vl_preco_novo_w,
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_sequencia		= nr_seq_plano_preco_p;
		end if;
	end if;
	
	--Calcula os acrescimos que n�o foram alterados
	if	(ie_tabela_base_w = 'N') then
		open c01;
		loop
		fetch c01 into
			nr_seq_preco_w,
			qt_idade_inicial_w,
			vl_preco_inicial_w;
		exit when c01%notfound;
			
			select	vl_preco_atual,
				tx_acrescimo,
				qt_idade_inicial,
				nvl(ie_grau_titularidade,'X')
			into	vl_preco_inicial_w,
				tx_acrescimo_preco_w,
				qt_idade_inicial_w,
				ie_grau_parentesco_w
			from	pls_plano_preco
			where	nr_sequencia	= nr_seq_preco_w;
			
			if	(qt_idade_inicial_w <> qt_idade_inicio_w) then
				/*select	max(nr_sequencia)
				into	nr_seq_preco_ant_w
				from	pls_plano_preco
				where	nr_seq_tabela		= nr_seq_tabela_p
				and	qt_idade_inicial	< qt_idade_inicial_w;*/
				open C02;
				loop
				fetch C02 into	
					nr_seq_preco_ant_w;
				exit when C02%notfound;
				end loop;
				close C02;
				
				if	(nr_seq_preco_ant_w is not null) then
					select	nvl(vl_preco_atual,0)
					into	vl_preco_ant_w
					from	pls_plano_preco
					where	nr_sequencia	= nr_seq_preco_ant_w;
					
					begin
					tx_acrescimo_w	:= (dividir_sem_round(vl_preco_inicial_w,vl_preco_ant_w) * 100) - 100;
					exception
					when others then
						tx_acrescimo_erro_w	:= (dividir_sem_round(vl_preco_inicial_w,vl_preco_ant_w) * 100) - 100;
						commit;
						
						wheb_mensagem_pck.exibir_mensagem_abort(200503,'QT_IDADE='||qt_idade_inicial_w||';'||'TX_ACRESCIMO='||tx_acrescimo_erro_w);
						--Menssagem: O valor do acr�scimo da faixa et�ria  ||qt_idade_inicial_w||  chegou no limite: ||tx_acrescimo_erro_w||
					end;
					
					update	pls_plano_preco
					set	tx_acrescimo	= tx_acrescimo_w,
						nm_usuario	= nm_usuario_p,
						dt_atualizacao	= sysdate
					where	nr_sequencia	= nr_seq_preco_w;
				end if;
			end if;
		end loop;
		close c01;
	end if;
end if;

commit;

end pls_calcular_manut_tab_preco;
/