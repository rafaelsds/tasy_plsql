create or replace
procedure pls_encerrar_proc_conta
			(	nr_seq_proc_conta_p	pls_processo_conta.nr_sequencia%type,
				nm_usuario_p		usuario.nm_usuario%type,
				ie_impug_nova_p		varchar2, 
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is 

cont_w				pls_integer;
ie_status_conta_w		pls_processo_conta.ie_status_conta%type;
vl_ressarcir_w			pls_processo_conta.vl_ressarcir%type;
vl_deferido_w			pls_processo_conta.vl_deferido%type;
ie_status_pagamento_w		pls_processo_conta.ie_status_pagamento%type;
ie_gerar_pos_estab_ressar_w	pls_parametros.ie_gerar_pos_estab_ressar%type;
ie_preco_w			pls_plano.ie_preco%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
dt_atual_w			date := trunc(sysdate);
count_impugnacao_w		pls_integer;
ie_formulario_w			varchar2(5);
qt_movimento_w			number(10) := 0; 
nr_seq_atualizacao_w		pls_atualizacao_contabil.nr_sequencia%type := 0;
dt_referencia_w			date;
nr_seq_processo_w		pls_processo.nr_sequencia%type;
ie_concil_contab_w		pls_visible_false.ie_concil_contab%type;
dt_internacao_w			pls_processo_conta.dt_internacao%type;

begin

select	ie_status_conta
into	ie_status_conta_w
from	pls_processo_conta
where 	nr_sequencia		= nr_seq_proc_conta_p;

select	decode(count(*),0,'N','S')
into	ie_formulario_w
from	pls_formulario
where	nr_seq_conta 		= nr_seq_proc_conta_p
and	ie_status_impugnacao 	in ('F','S','P');

select 	count(*) 
into	count_impugnacao_w
from 	pls_impugnacao 
where 	nr_seq_conta 		= nr_seq_proc_conta_p;

select 	trunc(a.dt_processo,'month'),
	a.nr_sequencia
into	dt_referencia_w,
	nr_seq_processo_w
from	pls_processo a,
	pls_processo_conta b
where	b.nr_seq_processo = a.nr_sequencia
and	b.nr_sequencia = nr_seq_proc_conta_p;

if	(ie_status_conta_w in ('A','J','R') and ie_status_conta_w <> 'C' and (count_impugnacao_w <> 0 or ie_formulario_w = 'S')) then

	ie_status_conta_w := null;

	pls_atualizar_proc_conta(nr_seq_proc_conta_p, nm_usuario_p, 'N', ie_impug_nova_p);

	if	(ie_impug_nova_p = 'N') then
		select	count(1)
		into	cont_w
		from	pls_impugnacao_defesa b,
			pls_impugnacao a
		where	a.nr_seq_conta	 	= nr_seq_proc_conta_p
		and	a.nr_sequencia		= b.nr_seq_impugnacao
		and	b.ie_status		in ('A','E')
		and	b.dt_cancelamento	is null;
	else
		select	count(1)
		into	cont_w
		from	pls_formulario_defesa b,
			pls_formulario a
		where	a.nr_seq_conta	 	= nr_seq_proc_conta_p
		and	a.nr_sequencia		= b.nr_seq_formulario
		and	b.ie_status		in ('A','E');
	end if;

	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(310204);	--Existem defesas de impugnacao pendentes!
									--Nao e possivel encerrar esta conta.
	end if;

	if	(ie_impug_nova_p = 'N') then
		select	count(1)
		into	cont_w
		from	pls_impugnacao a
		where	a.nr_seq_conta		= nr_seq_proc_conta_p
		and	nvl(a.ie_status_impugnacao, '-1')	in ('A','C','D','E','J','R','-1') 
		and not exists(	select 	1
				from	pls_impugnacao x
				where	x.nr_seq_conta = nr_seq_proc_conta_p
				and	x.ie_status_impugnacao in('F', 'S', 'P')
				and     x.ie_tipo_peticao = 2);
	else
		select	count(1)
		into	cont_w
		from	pls_formulario a
		where	a.nr_seq_conta		= nr_seq_proc_conta_p
		and	nvl(a.ie_status_impugnacao, '-1')	in ('A','C','D','E','J','R','-1') 
		and not exists(	select 	1
				from	pls_formulario x
				where	x.nr_seq_conta = nr_seq_proc_conta_p
				and	x.ie_status_impugnacao in('F', 'S', 'P')
				and     x.ie_tipo_peticao = 2);
	end if;

	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(310205);  -- Existem formularios de impugnacao pendentes!
								  -- Nao e possivel encerrar esta conta.
	end if;

	select	nvl(vl_ressarcir,0),
		nvl(vl_deferido,0)
	into	vl_ressarcir_w,
		vl_deferido_w
	from	pls_processo_conta
	where	nr_sequencia	= nr_seq_proc_conta_p;

	if	(vl_deferido_w = 0) then
		select	nvl(sum(nvl(vl_deferido,0)),0)
		into	vl_deferido_w
		from	pls_impugnacao
		where	nr_seq_conta	= nr_seq_proc_conta_p;
	end if;

	if	(vl_deferido_w > 0) then
		ie_status_conta_w	:= 'D';	-- Encerrada com deferimento
	else	
		ie_status_conta_w	:= 'I';	-- Encerrada com indeferimento
	end if;

	if	(vl_ressarcir_w > 0) then
		ie_status_pagamento_w	:= 'P'; -- Pagamento previsto
	else
		ie_status_pagamento_w	:= 'N'; -- Nao havera pagamento	
	end if;

	update	pls_processo_conta
	set	ie_status_conta			= ie_status_conta_w,
		ie_status_pagamento 		= ie_status_pagamento_w,
		vl_deferido			= nvl(vl_deferido,vl_deferido_w) -- Atualiza caso nulo
	where	nr_sequencia			= nr_seq_proc_conta_p;



	select	nvl(max(ie_concil_contab), 'N')
	into	ie_concil_contab_w
	from 	pls_visible_false;

	if	(ie_concil_contab_w = 'S') then
		begin

		pls_gerar_atualizacao_contabil(	dt_referencia_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						27,
						'G',
						qt_movimento_w,
						nr_seq_atualizacao_w);

		ctb_pls_atualizar_ressa		(nr_seq_processo_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						nr_seq_proc_conta_p,
						null,
						null,
						qt_movimento_w,
						nr_seq_atualizacao_w);			
		
		pls_gerar_atualizacao_contabil(	dt_referencia_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						27,
						'A',
						qt_movimento_w,
						nr_seq_atualizacao_w);
		end;

		if 	(vl_ressarcir_w <> 0) then
			ctb_concil_financeira_pck.ctb_gravar_documento( cd_estabelecimento_p,
									pkg_date_utils.start_of(sysdate, 'DAY'),
									27, 
									null, 
									27,
									nr_seq_proc_conta_p,
									null,
									null,
									vl_ressarcir_w,
									'PLS_PROCESSO_CONTA',
									'VL_RESSARCIR',
									nm_usuario_p);
		end if;

		if 	(vl_deferido_w <> 0) then
			ctb_concil_financeira_pck.ctb_gravar_documento( cd_estabelecimento_p,
									pkg_date_utils.start_of(sysdate, 'DAY'),
									27, 
									null, 
									28, 
									nr_seq_proc_conta_p,
									null,
									null,
									vl_deferido_w,
									'PLS_PROCESSO_CONTA',
									'VL_DEFERIDO',
									nm_usuario_p);
		end if;

	end if;

	----Verifica o parametro abaixo, para saber se sera ou nao gerado o pos estabelecido do ressarcimento.
	select	max(ie_gerar_pos_estab_ressar)
	into	ie_gerar_pos_estab_ressar_w
	from	pls_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;

	----Deste ponto para baixo trata a geracao do pos para o ressarcimento.
	if	(nvl(ie_gerar_pos_estab_ressar_w , 'N') = 'S') then
		-- Pegar o tipo de preco do plano
		begin
		select	a.ie_preco,
			b.nr_sequencia,
			c.dt_internacao
		into	ie_preco_w,
			nr_seq_segurado_w,
			dt_internacao_w
		from	pls_plano a,
			pls_segurado b,
			pls_processo_conta c
		where	a.nr_sequencia = b.nr_seq_plano
		and	b.nr_sequencia = c.nr_seq_segurado
		and	c.nr_sequencia = nr_seq_proc_conta_p;
		exception
		when others then
			ie_preco_w := 0;
			nr_seq_segurado_w := null;
		end;

		if	(ie_preco_w in(2,3)) then
			-- Verificar se e de repasse
			select	count(1)
			into	cont_w
			from	pls_segurado_repasse
			where	nr_seq_segurado	= nr_seq_segurado_w
			and	dt_atual_w between nvl(dt_repasse,sysdate) and nvl(dt_fim_repasse,sysdate);
		
			-- Gerar o item de pos
			if	(cont_w > 0) then
				pls_gera_pos_ressarcimento(	nr_seq_proc_conta_p, nm_usuario_p, cd_estabelecimento_p);
			end if;
		-- Ressarcimento ao SUS
		elsif	((ie_preco_w = '1') and (pls_obter_se_benef_remido(nr_seq_segurado_w, dt_internacao_w) = 'S')) then
			pls_gera_pos_ressarcimento(nr_seq_proc_conta_p, nm_usuario_p, cd_estabelecimento_p);
		end if;
	end if;
end if;

end pls_encerrar_proc_conta;
/