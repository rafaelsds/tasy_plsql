create or replace
procedure Alterar_seq_lab(	nr_prescricao_p		number,
				nm_usuario_p		varchar2,
				nr_seq_material_p	number,
				nr_seq_prescr_p		number) is

cd_material_exame_w	varchar2(20);
ie_gerar_sequencia_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_grupo_imp_w	number(10);
ie_regra_w		varchar2(5);
ie_regra_ww		varchar2(5);
nr_seq_grupo_w		number(10);

nr_prescricao_w		number(10) := 0;
dt_prescricao_w		date;
nr_seq_lab_w		number(10);
nr_sequencia_w		number(10);
nr_seq_imp_w		Number(15,0);
dt_prev_execucao_w	date;

dt_prev_exec_exame_w		date;
dt_prescr_exame_w 		date;
nr_seq_grupo_imp_exame_w	number(10);
ie_regra_seq_exame_w		varchar2(5);
ie_regra_seq_exame_ww		varchar2(5);
ie_gerado_w			varchar2(1);

nr_prescricao_pos_w	number(10);
nr_sequencia_pos_w	number(6);
nr_seq_max_mat_w	number(10);
nr_seq_prescr_alt_w	number(6);
nr_seq_prescr_ant_w	number(6);
nr_prescricao_ant_w	number(10);
nr_seq_origem_w		number(6);
nr_Seq_prescr_hor_w	number(6);

Cursor C01 is
select 	nr_prescricao,
	nr_sequencia
from 	exame_laboratorio a,
	prescr_procedimento b	
where	a.nr_seq_exame = b.nr_seq_exame
and	cd_material_exame = cd_material_exame_w
and	a.nr_seq_grupo_imp = nr_seq_grupo_imp_exame_w
and	b.nr_prescricao = nr_prescricao_p
and (	((nr_seq_origem	is null) and (nr_seq_origem_w = 0)) or 
	((nr_seq_origem	= nr_seq_origem_w) and (nr_Sequencia = nr_Seq_prescr_hor_w)))
and 	not exists (select 1 from prescr_proc_mat_alteracao e
			 where 	e.nr_prescricao = b.nr_prescricao
			 and 	e.nr_seq_prescr = b.nr_sequencia);

Cursor C02 is
select 	nr_prescricao,
	nr_sequencia
from 	exame_laboratorio a,
	prescr_procedimento b	
where	a.nr_seq_exame = b.nr_seq_exame
and	b.nr_prescricao = nr_prescricao_p
and 	nvl(nr_seq_origem,0)	= nr_seq_origem_w
and (	((nr_seq_origem	is null) and (nr_seq_origem_w = 0)) or 
	((nr_seq_origem	= nr_seq_origem_w) and (nr_Sequencia = nr_Seq_prescr_hor_w)))
and	exists (select 1 from prescr_proc_mat_alteracao e
			 where 	e.nr_prescricao = b.nr_prescricao
			 and 	e.nr_seq_prescr = b.nr_sequencia);



begin

select  cd_material_exame
into	cd_material_exame_w
from	material_exame_lab
where	nr_sequencia = nr_seq_material_p;

select	nvl(max(ie_gerar_sequencia),'N')
into	ie_gerar_sequencia_w
from	lab_parametro;

select	max(b.nr_seq_grupo_imp),
	max(d.dt_prescricao),
	max(c.ie_regra_sequencia),
	max(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(a.dt_prev_execucao,sysdate))),
	nvl(max(nr_seq_origem),0),
	max(decode(nr_seq_origem, null,null,a.nr_sequencia))
into	nr_seq_grupo_imp_exame_w,
	dt_prescr_exame_w,
	ie_regra_seq_exame_w,
	dt_prev_exec_exame_w,
	nr_seq_origem_w,
	nr_Seq_prescr_hor_w
from 	prescr_procedimento a,
	exame_laboratorio b,
	lab_grupo_impressao c,
	prescr_medica d
where	a.nr_seq_exame 	= b.nr_seq_exame
and	c.nr_sequencia	= b.nr_seq_grupo_imp
and	d.nr_prescricao	= a.nr_prescricao
and 	a.nr_prescricao = nr_prescricao_p
and	a.nr_sequencia 	= nr_seq_prescr_p;

ie_regra_seq_exame_ww := substr(ie_regra_seq_exame_w,1,1);

lock table lab_seq_grupo_imp in exclusive mode;
select	nvl(max(nr_valor_seq),0) + 1,
	max(nr_sequencia)
into	nr_seq_lab_w,
	nr_sequencia_w
from lab_seq_grupo_imp
where nr_sequencia = 	(select max(nr_sequencia)
			from lab_seq_grupo_imp
 			where nr_seq_grupo_imp = nr_seq_grupo_imp_exame_w
			and (((ie_regra_seq_exame_ww = 'D') and
			 ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_sequencia) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_prescr_exame_w)) or
			((ie_regra_seq_exame_ww = 'S') and
			 trunc(dt_sequencia,'day') = trunc(dt_prescr_exame_w,'day')) or
			((ie_regra_seq_exame_ww = 'M') and
			 ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_sequencia) = ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_prescr_exame_w)) or
			((ie_regra_seq_exame_w = 'A') and
			 ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_sequencia) = ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_prescr_exame_w)) or (ie_regra_seq_exame_ww = 'G')));

update	lab_seq_grupo_imp
set	nr_valor_seq = nr_seq_lab_w
where nr_sequencia = nr_sequencia_w;



if	(substr(ie_regra_seq_exame_w,2,1)= 'M') then

	OPEN C01;
	LOOP
	FETCH C01 into
		nr_prescricao_pos_w,
		nr_sequencia_pos_w;
	exit when c01%notfound;
		begin

		update	prescr_procedimento b
		set	nr_seq_lab = nr_seq_lab_w
		where 	nr_prescricao = nr_prescricao_pos_w
		and	nr_sequencia = nr_sequencia_pos_w;	

		end;
	END LOOP;
	CLOSE C01;


	OPEN C02;
	LOOP
	FETCH C02 into
		nr_prescricao_ant_w,
		nr_seq_prescr_ant_w;
	exit when c02%notfound;
		begin

		select  max(f.nr_sequencia)
		into	nr_seq_max_mat_w
		from 	prescr_proc_mat_alteracao f
		where	f.nr_seq_prescr = nr_seq_prescr_ant_w
		and	f.nr_prescricao =  nr_prescricao_ant_w;

		if	(nr_seq_max_mat_w > 0) then



			select 	nvl(max(e.nr_seq_prescr),0)
			 into	nr_seq_prescr_alt_w
			 from 	prescr_proc_mat_alteracao e
			where 	e.nr_seq_prescr  =  nr_seq_prescr_ant_w
			  and 	e.nr_prescricao	 =  nr_prescricao_ant_w
			  and 	e.nr_seq_mat_atual = nr_seq_material_p
			  and 	e.nr_sequencia	 =  nr_seq_max_mat_w;

			if (nr_seq_prescr_alt_w > 0) then

				update	prescr_procedimento
				set	nr_seq_lab = nr_seq_lab_w
				where	nr_sequencia = nr_seq_prescr_alt_w
				and	nr_prescricao = nr_prescricao_ant_w;

			end if;
		end if;

		end;
	END LOOP;
	CLOSE C02;

elsif ((substr(ie_regra_seq_exame_w,2,1)= 'P')) then

	update	prescr_procedimento b
	set	nr_seq_lab = nr_seq_lab_w
	where 	cd_material_exame = cd_material_exame_w
	and	nr_prescricao = nr_prescricao_p
	and 	nr_sequencia = nr_seq_prescr_p
	  and	Obter_Grupo_imp_Exame_lab(b.nr_seq_exame) = nr_seq_grupo_imp_exame_w
	  and 	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(b.dt_prev_execucao,sysdate)) 	= dt_prev_exec_exame_w
	  and 	not exists (select 1 from prescr_proc_mat_alteracao e
			 where 	e.nr_prescricao = b.nr_prescricao
			 and 	e.nr_seq_prescr = b.nr_sequencia);


	update	prescr_procedimento b
	set	b.nr_seq_lab = nr_seq_lab_w
	where	b.nr_sequencia in (select e.nr_seq_prescr
				  from prescr_proc_mat_alteracao e
				  where b.nr_sequencia = e.nr_seq_prescr
				    and b.nr_prescricao = e.nr_prescricao	
				    and e.nr_seq_mat_atual = nr_seq_material_p
				    and e.nr_sequencia = (select max(f.nr_sequencia)
							from 	prescr_proc_mat_alteracao f
							where	f.nr_seq_prescr = e.nr_seq_prescr
							and	f.nr_prescricao = e.nr_prescricao ))
	and	nr_prescricao = nr_prescricao_p
	  and	Obter_Grupo_imp_Exame_lab(b.nr_seq_exame) = nr_seq_grupo_imp_exame_w
	  and 	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(b.dt_prev_execucao,sysdate)) 	= dt_prev_exec_exame_w;

end if;


END Alterar_seq_lab;
/
