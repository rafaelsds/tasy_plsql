Create or Replace
Procedure EXCLUIR_PRECO_PROC_SUS	(dt_competencia_p	date,
					ie_opcao_p		number) is

/*
1 - SUS_PRECO_PROCAIH
2 - SUS_PRECO_PROCBPA
3 - SUS_PROCED_DOENCA
*/

begin
if (ie_opcao_p = 1) then
	Delete from 	SUS_PRECO_PROCAIH
	where 		dt_competencia = dt_competencia_p;
elsif (ie_opcao_p = 2) then
	Delete from 	SUS_PRECO_PROCBPA
	where 		dt_competencia = dt_competencia_p;
elsif (ie_opcao_p = 3) then
	Delete from sus_proced_doenca 
	where	ie_origem_proced = 3;
end if;

commit;

end EXCLUIR_PRECO_PROC_SUS;
/