create or replace
procedure gerar_w_atend_mensal( ds_turno_p	number,
				dt_referencia_p	date,
				nm_usuario_p	Varchar2) is 
				
cd_turno_w		number(10);
ds_turno_w		varchar2(15);

qt_medico_w		number(10);	
cd_dia_semana_w		number(1);

nr_sequencia_max_w	number(10);

qt_medico_seg_w		number(10);
qt_medico_ter_w		number(10);
qt_medico_qua_w		number(10);
qt_medico_qui_w		number(10);
qt_medico_sex_w		number(10);
qt_medico_sab_w		number(10);
qt_medico_dom_w		number(10);

qt_atend_seg_w		number(10);
qt_atend_ter_w		number(10);
qt_atend_qua_w		number(10);
qt_atend_qui_w		number(10);
qt_atend_sex_w		number(10);
qt_atend_sab_w		number(10);
qt_atend_dom_w		number(10);

qt_dia_semana_seg_w	number(10);
qt_dia_semana_ter_w     number(10);
qt_dia_semana_qua_w     number(10);
qt_dia_semana_qui_w     number(10);
qt_dia_semana_sex_w     number(10);
qt_dia_semana_sab_w     number(10);
qt_dia_semana_dom_w     number(10);
				
Cursor C01 is
	select 	a.nr_sequencia,
		a.ds_turno
	from   	turno_atendimento a
	where 	a.nr_sequencia = decode(ds_turno_p, null, a.nr_sequencia, ds_turno_p)
	order by a.nr_sequencia;
				
begin

delete	w_atend_mensal_med where nm_usuario = nm_usuario_p;
delete	w_atend_mensal_atend where nm_usuario = nm_usuario_p;
delete 	w_atend_mensal_semana where nm_usuario = nm_usuario_p;
commit;

open C01;
loop
fetch C01 into	
	cd_turno_w, 
	ds_turno_w;
exit when C01%notfound;
	
	/* Quantidade de m�dico*/
	select	count(cd_medico)
	into	qt_medico_seg_w
	from    (select	distinct a.cd_medico_resp cd_medico
		from	Eis_Paciente_v a
		where	a.ds_dia_semana = 'Segunda_Feira'
		and	a.cd_turno = cd_turno_w
		and	a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p));

	select	count(cd_medico)
	into	qt_medico_ter_w
	from    (select	distinct a.cd_medico_resp cd_medico
		from	Eis_Paciente_v a
		where	a.ds_dia_semana = 'Ter�a-Feira'
		and	a.cd_turno = cd_turno_w
		and	a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p));

	select	count(cd_medico)
	into	qt_medico_qua_w
	from    (select	distinct a.cd_medico_resp cd_medico
		from	Eis_Paciente_v a
		where	a.ds_dia_semana= 'Quarta-Feira'
		and	a.cd_turno = cd_turno_w
		and	a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p));

	select	count(cd_medico)
	into	qt_medico_qui_w
	from    (select	distinct a.cd_medico_resp cd_medico
		from	Eis_Paciente_v a
		where	a.ds_dia_semana = 'Quinta-Feira'
		and	a.cd_turno = cd_turno_w
		and	a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p));

	select	count(cd_medico)
	into	qt_medico_sex_w
	from    (select	distinct a.cd_medico_resp cd_medico
		from	Eis_Paciente_v a
		where	a.ds_dia_semana = 'Sexta-Feira'
		and	a.cd_turno = cd_turno_w
		and	a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p));

	select	count(cd_medico)
	into	qt_medico_sab_w
	from    (select	distinct a.cd_medico_resp cd_medico
		from	Eis_Paciente_v a
		where	a.ds_dia_semana = 'S�bado'
		and	a.cd_turno = cd_turno_w
		and	a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p));	
	
	select	count(cd_medico)
	into	qt_medico_dom_w
	from    (select	distinct a.cd_medico_resp cd_medico
		from	Eis_Paciente_v a
		where	a.ds_dia_semana = 'Domingo'
		and	a.cd_turno = cd_turno_w
		and	a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p));


	insert into w_atend_mensal_med (
		nr_sequencia,
		dt_referencia,
		cd_turno,
		nm_usuario,
		qt_med_seg,
		qt_med_ter,
		qt_med_qua,
		qt_med_qui,
		qt_med_sex,
		qt_med_sab,
		qt_med_dom)
	values(	
		w_atend_mensal_med_seq.nextval,
		dt_referencia_p,
		cd_turno_w,
		nm_usuario_p,
		qt_medico_seg_w,
		qt_medico_ter_w,
		qt_medico_qua_w,
		qt_medico_qui_w,
		qt_medico_sex_w,
		qt_medico_sab_w,
		qt_medico_dom_w);

	/* Quantidade de atendimentos */
	select	count(*) atendimento
	into	qt_atend_seg_w
	from    Eis_Paciente_v a
	where   a.ds_dia_semana = 'Segunda-Feira'
	and     a.cd_turno = cd_turno_w
	and     a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p);

	select	count(*) atendimento
	into	qt_atend_ter_w
	from    Eis_Paciente_v a
	where   a.ds_dia_semana = 'Ter�a-Feira'
	and     a.cd_turno = cd_turno_w
	and     a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p);

	select	count(*) atendimento
	into	qt_atend_qua_w
	from    Eis_Paciente_v a
	where   a.ds_dia_semana = 'Quarta-Feira'
	and     a.cd_turno = cd_turno_w
	and     a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p);

	select	count(*) atendimento
	into	qt_atend_qui_w
	from    eis_Paciente_v a
	where   a.ds_dia_semana = 'Quinta-Feira'
	and     a.cd_turno = cd_turno_w
	and     a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p);

	select	count(*) atendimento
	into	qt_atend_sex_w
	from    Eis_Paciente_v a
	where   a.ds_dia_semana = 'Sexta-Feira'
	and     a.cd_turno = cd_turno_w
	and     a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p);

	select	count(*) atendimento
	into	qt_atend_sab_w
	from    Eis_Paciente_v a
	where   a.ds_dia_semana = 'S�bado'
	and     a.cd_turno = cd_turno_w
	and     a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p);

	select	count(*) atendimento
	into	qt_atend_dom_w
	from   	Eis_Paciente_v a
	where   a.ds_dia_semana = 'Domingo'
	and     a.cd_turno = cd_turno_w
	and     a.dt_entrada between dt_referencia_p and last_day(dt_referencia_p);	

	insert into w_atend_mensal_atend (
		nr_sequencia,
		dt_referencia,
		cd_turno,
		nm_usuario,
		qt_atend_seg,
		qt_atend_ter,
		qt_atend_qua,
		qt_atend_qui,
		qt_atend_sex,
		qt_atend_sab,
		qt_atend_dom
		)
	values(	
		w_atend_mensal_atend_seq.nextval,
		dt_referencia_p,
		cd_turno_w,
		nm_usuario_p,
		qt_atend_seg_w,
		qt_atend_ter_w,
		qt_atend_qua_w,
		qt_atend_qui_w,
		qt_atend_sex_w,
		qt_atend_sab_w,
		qt_atend_dom_w);


	/* Quantidade dias da semana */
	qt_dia_semana_seg_w := 0;
	select count(obter_dia_semana(a.dt_dia)) qt_segunda
	into	qt_dia_semana_seg_w
	from    dia_v a
	where   obter_dia_semana(a.dt_dia) = 'Segunda-Feira'
	and      a.dt_dia between dt_referencia_p and last_day(dt_referencia_p);
	
	select count(obter_dia_semana(a.dt_dia)) qt_terca
	into	qt_dia_semana_ter_w
	from    dia_v a
	where   obter_dia_semana(a.dt_dia) = 'Ter�a-Feira'
	and      a.dt_dia between dt_referencia_p and last_day(dt_referencia_p);
	
	select count(obter_dia_semana(a.dt_dia)) qt_quarta
	into	qt_dia_semana_qua_w
	from    dia_v a
	where   obter_dia_semana(a.dt_dia) = 'Quarta-Feira'
	and      a.dt_dia between dt_referencia_p and last_day(dt_referencia_p);
	
	select count(obter_dia_semana(a.dt_dia)) qt_quinta
	into	qt_dia_semana_qui_w
	from    dia_v a
	where   obter_dia_semana(a.dt_dia) = 'Quinta-Feira'
	and      a.dt_dia between dt_referencia_p and last_day(dt_referencia_p);
	
	select count(obter_dia_semana(a.dt_dia)) qt_sexta
	into	qt_dia_semana_sex_w
	from    dia_v a
	where   obter_dia_semana(a.dt_dia) = 'Sexta-Feira'
	and      a.dt_dia between dt_referencia_p and last_day(dt_referencia_p);
	
	select count(obter_dia_semana(a.dt_dia)) qt_sabado
	into	qt_dia_semana_sab_w
	from    dia_v a
	where   obter_dia_semana(a.dt_dia) = 'S�bado'
	and      a.dt_dia between dt_referencia_p and last_day(dt_referencia_p);
	
	select count(obter_dia_semana(a.dt_dia)) qt_domingo
	into	qt_dia_semana_dom_w
	from    dia_v a
	where   obter_dia_semana(a.dt_dia) = 'Domingo'
	and      a.dt_dia between dt_referencia_p and last_day(dt_referencia_p);
	
	insert into w_atend_mensal_semana (
		nr_sequencia,
		dt_referencia,
		cd_turno,
		nm_usuario,
		qt_dia_seg,
		qt_dia_ter,
		qt_dia_qua,
		qt_dia_qui,
		qt_dia_sex,
		qt_dia_sab,
		qt_dia_dom
		)
	values(	
		w_atend_mensal_semana_seq.nextval,
		dt_referencia_p,
		cd_turno_w,
		nm_usuario_p,
		qt_dia_semana_seg_w,
		qt_dia_semana_ter_w,
		qt_dia_semana_qua_w,
		qt_dia_semana_qui_w,
		qt_dia_semana_sex_w,
		qt_dia_semana_sab_w,
		qt_dia_semana_dom_w);	
end loop;
close C01;

commit;

end gerar_w_atend_mensal;
/