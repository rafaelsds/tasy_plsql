create or replace
procedure validar_traducao_funcao(	cd_funcao_p	number,
					nm_usuario_p	varchar2,
					nr_seq_idioma_p	number) is

/*

7	Alem�o (DE)	de_DE
6	Ingl�s (UK)	en_GB
3	Espanhol (CO)	es_CO
1	Portugu�s (BR)	pt_BR
2	Espanhol (MX)	es_MX
5	Ingl�s (US)	en_US
4	Espanhol (AR)	es_AR
8	�rabe (SA)	ar_SA

*/

ds_expressao_de_w		varchar2(4000);
nr_inconsistencia_traducao_w	inconsistencia_traducao.nr_sequencia%type;
nr_seq_funcao_schematic_w	funcao_schematic.nr_sequencia%type;
ds_texto_fixo_w			varchar2(4000);

nr_seq_inconsistencia_igual_w	inconsistencia_traducao.nr_sequencia%type;
cd_expressao_w			inconsistencia_traducao.cd_expressao%type;
nm_atributo_w			inconsistencia_traducao.nm_atributo%type;
ds_label_w			inconsistencia_traducao.ds_label%type;
ds_idioma_w			varchar2(80) := null;

nr_seq_idioma_w			tasy_idioma.nr_sequencia%type;

Cursor	C01 is			
	select	a.nr_sequencia,
		a.nr_seq_dic_objeto,
		a.nm_tabela,
		a.nr_seq_visao,
		a.ie_tipo_componente,
		ie_tipo_objeto
	from	objeto_schematic a
	where	a.nr_seq_funcao_schematic = nr_seq_funcao_schematic_w;
	
c01_w	C01%rowtype;
	
Cursor C02 is
	select	cd_exp_titulo cd_exp_label, /* WDBPanel title */
		'View' nm_atributo,
		ds_titulo ds_label
	from	tabela_visao
	where	nr_sequencia = c01_w.nr_seq_visao
	and	c01_w.ie_tipo_componente = 'WDBP'
	and	cd_exp_titulo not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_label, /* WDBPanel Detalhe  */
		nm_atributo,
		ds_label
	from	tabela_visao_atributo
	where	nr_sequencia = c01_w.nr_seq_visao
	and	cd_exp_label is not null
	and	nr_seq_apresent is not null
	and	c01_w.ie_tipo_componente = 'WDBP'
	and	cd_exp_label not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_label_longo, /* WDBPanel Detalhe label longo */
		nm_atributo,
		ds_label
	from	tabela_visao_atributo
	where	nr_sequencia = c01_w.nr_seq_visao
	and	cd_exp_label_longo is not null
	and	nr_seq_apresent is not null
	and	c01_w.ie_tipo_componente = 'WDBP'
	and	cd_exp_label_longo not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_label_grid, /* WDBPanel Grid */
		nm_atributo,
		ds_label_grid
	from	tabela_visao_atributo
	where	nr_sequencia = c01_w.nr_seq_visao
	and	cd_exp_label_grid is not null
	and	nr_seq_apresent is not null
	and	c01_w.ie_tipo_componente = 'WDBP'
	and	cd_exp_label_grid not in (286382, 282905, 613641, 288924)	
	union
	select	b.cd_exp_desc, -- WDBPanel Additional info (Shift + F1) 
		a.nm_atributo || ' (Add. info)',
		b.ds_atributo
	from	tabela_visao_atributo a,
		tabela_visao c,
		tabela_atributo b
	where	a.nr_sequencia = c01_w.nr_seq_visao
	and	c.nr_sequencia = a.nr_sequencia
	and	c.nm_tabela = b.nm_tabela
	and	a.nm_atributo = b.nm_atributo
	and	b.cd_exp_desc is not null
	and	a.nr_seq_apresent is not null
	and	c01_w.ie_tipo_componente = 'WDBP'
	and	b.cd_exp_desc not in (286382, 282905, 613641, 288924)
	union
	select	b.cd_exp_titulo, /* WDBPanel fields group */
		'Field group',
		null
	from	tabela_visao_atributo a,
		tabela_visao_grupo b
	where	a.nr_sequencia = c01_w.nr_seq_visao
	and	a.nr_seq_visao_grupo = b.nr_sequencia
	and	a.cd_exp_label is not null
	and	b.cd_exp_titulo is not null
	and	a.nr_seq_apresent is not null
	and	c01_w.ie_tipo_componente = 'WDBP'
	and	b.cd_exp_titulo not in (286382, 282905, 613641, 288924)
	union
	select	b.cd_exp_dominio, /* WDBPanel field domain */
		a.nm_atributo || ' domain (' || b.cd_dominio || ')',
		b.ds_dominio
	from	tabela_visao_atributo a,
		dominio b
	where	a.nr_sequencia = c01_w.nr_seq_visao
	and	a.cd_dominio = b.cd_dominio
	and	a.cd_exp_label is not null
	and	((b.ds_dominio is not null) or (b.cd_exp_dominio is not null))
	and	a.nr_seq_apresent is not null
	and	c01_w.ie_tipo_componente = 'WDBP'
	and	b.cd_exp_dominio not in (286382, 282905, 613641, 288924)
	union
	select	b.cd_exp_valor_dominio, /* WDBPanel field domain */
		a.nm_atributo || ' domain (' || b.cd_dominio || ') value',
		b.ds_valor_dominio
	from	tabela_visao_atributo a,
		valor_dominio_v b
	where	a.nr_sequencia = c01_w.nr_seq_visao
	and	a.cd_dominio = b.cd_dominio
	and	a.cd_exp_label is not null
	and	((b.ds_valor_dominio is not null) or (b.cd_exp_valor_dominio is not null))
	and	a.nr_seq_apresent is not null
	and	c01_w.ie_tipo_componente = 'WDBP'
	and	b.cd_exp_valor_dominio not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_texto, /* WCPanel title */
		'Lista (WCPanel)',
		ds_texto
	from	dic_objeto
	where	nr_sequencia = c01_w.nr_seq_dic_objeto
	and	c01_w.ie_tipo_componente = 'WCP'
	and	CD_EXP_TEXTO is not null
	and	cd_exp_texto not in (286382, 282905, 613641, 288924)
	union	
	select	cd_exp_campo_tela, /* WCPanel fields */
		nm_campo_base,
		nm_campo_tela
	from	dic_objeto
	where	nr_seq_obj_sup = c01_w.nr_seq_dic_objeto
	and	c01_w.ie_tipo_componente = 'WCP'
	and	cd_exp_campo_tela not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_label, /* WFiltro / WDinamicForm */
		nm_atributo,
		ds_label
	from	dic_objeto_filtro
	where	nr_seq_objeto = c01_w.nr_seq_dic_objeto
	and	c01_w.ie_tipo_componente in ('WF', 'WDF')
	and	cd_exp_label not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_desc, /* WFiltro / WDinamicForm - description (Shift + F1) */
		nm_atributo,
		null
	from	dic_objeto_filtro
	where	nr_seq_objeto = c01_w.nr_seq_dic_objeto
	and	c01_w.ie_tipo_componente in ('WF', 'WDF')
	and	cd_exp_desc is not null
	and	cd_exp_desc not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_texto, /* WFiltro / WDinamicForm - title */
		'Filter title',
		ds_texto
	from	dic_objeto
	where	nr_sequencia = c01_w.nr_seq_dic_objeto
	and	c01_w.ie_tipo_componente in ('WF', 'WDF')
	and	cd_exp_texto is not null
	and	cd_exp_texto not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_opcoes, /* wfiltro / WDinamicForm - radiogroup */
		nm_atributo,
		ds_opcoes
	from	dic_objeto_filtro
	where	nr_seq_objeto = c01_w.nr_seq_dic_objeto
	and	c01_w.ie_tipo_componente in ('WF', 'WDF')
	and	cd_exp_opcoes not in (286382, 282905, 613641, 288924)
	union
	select	b.cd_exp_titulo, /* wfiltro / WDinamicForm - group */
		'Field group',
		null
	from	dic_objeto_filtro a,
		dic_objeto_filtro_grupo b
	where	a.nr_seq_objeto = c01_w.nr_seq_dic_objeto
	and	a.nr_seq_grupo = b.nr_sequencia
	and	a.nr_seq_grupo is not null
	and	c01_w.ie_tipo_componente in ('WF', 'WDF')
	and	b.cd_exp_titulo not in (286382, 282905, 613641, 288924)
	union
	select	b.cd_exp_dominio, /*  wfiltro / WDinamicForm - field domain */
		a.nm_atributo || ' domain (' || b.cd_dominio || ')',
		b.ds_dominio
	from	dic_objeto_filtro a,
		dominio b
	where	a.nr_sequencia = c01_w.nr_seq_dic_objeto
	and	a.cd_dominio = b.cd_dominio
	and	((b.ds_dominio is not null) or (b.cd_exp_dominio is not null))
	and	c01_w.ie_tipo_componente  in ('WF', 'WDF')
	and	b.cd_exp_dominio not in (286382, 282905, 613641, 288924)
	union
	select	b.cd_exp_valor_dominio, /*  wfiltro / WDinamicForm - field domain */
		a.nm_atributo || ' domain (' || b.cd_dominio || ') value',
		b.ds_valor_dominio
	from	dic_objeto_filtro a,
		valor_dominio_v b
	where	a.nr_sequencia = c01_w.nr_seq_dic_objeto
	and	a.cd_dominio = b.cd_dominio
	and	((b.ds_valor_dominio is not null) or (b.cd_exp_valor_dominio is not null))
	and	c01_w.ie_tipo_componente  in ('WF', 'WDF')
	and	b.cd_exp_valor_dominio not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_texto, /* WPopUp */
		nm_objeto,
		ds_texto
	from	dic_objeto
	where	nr_seq_obj_sup = c01_w.nr_seq_dic_objeto
	and	c01_w.ie_tipo_componente = 'WPOPUP'
	and	cd_exp_texto not in (286382, 282905, 613641, 288924)
	union
	select	cd_exp_desc_obj, -- Tabs e dropdown menus
		ds_objeto,
		ds_objeto
	from	objeto_schematic
	where	c01_w.ie_tipo_objeto in ('T', 'IT', 'DDM')
	and	nr_sequencia = c01_w.nr_seq_dic_objeto
	and	cd_exp_desc_obj not in (286382, 282905, 613641, 288924);
	
c02_w	C02%rowtype;

Cursor C03 is
	select	a.cd_expressao cd_exp_label, -- function name
		'Function name' nm_atributo,
		x.ds_funcao ds_label
	from	funcao x,
		dic_expressao a
	where	a.cd_expressao = x.cd_exp_funcao
	and	x.cd_funcao = cd_funcao_p
	and	a.cd_expressao not in (286382, 282905, 613641, 288924)
	union
	select	a.cd_expressao, -- function item name
		'Function item name',
		x.ds_item
	from	funcao_item x,
		dic_expressao a
	where	a.cd_expressao = x.cd_exp_desc_item
	and	x.cd_funcao = cd_funcao_p
	and	a.cd_expressao not in (286382, 282905, 613641, 288924)
	union
	select	a.cd_expressao, -- Schematic name
		'Schematic title (INTERNAL USE ONLY)',
		x.ds_schematic
	from	funcao_schematic x,
		dic_expressao a
	where	a.cd_expressao = x.cd_exp_schematic
	and	x.nr_sequencia = nr_seq_funcao_schematic_w
	and	a.cd_expressao not in (286382, 282905, 613641, 288924)
	union
	select	a.cd_expressao, -- Parameter description
		'Parameter ' || x.nr_sequencia || ' description',
		x.ds_parametro
	from	funcao_parametro x,
		dic_expressao a
	where	a.cd_expressao = x.cd_exp_parametro
	and	x.cd_funcao = cd_funcao_p
	and	a.cd_expressao not in (286382, 282905, 613641, 288924)
	union
	select	a.cd_expressao, -- Parameter additional info
		'Parameter ' || x.nr_sequencia || ' additional info',
		x.ds_parametro
	from	funcao_parametro x,
		dic_expressao a
	where	a.cd_expressao = x.cd_exp_observacao
	and	x.cd_funcao = cd_funcao_p
	and	a.cd_expressao not in (286382, 282905, 613641, 288924)
	union
	select	a.cd_expressao, -- Parameter dependencies
		'Parameter ' || x.nr_sequencia || ' dependencies',
		x.ds_parametro
	from	funcao_parametro x,
		dic_expressao a
	where	a.cd_expressao = x.cd_exp_dependencia
	and	x.cd_funcao = cd_funcao_p
	and	a.cd_expressao not in (286382, 282905, 613641, 288924)
	union
	select	b.cd_exp_dominio, -- parameter domain title
		'Parameter ' || x.nr_sequencia || ' domain (' || b.cd_dominio || ')',
		b.ds_dominio
	from	funcao_parametro x,
		dic_expressao a,
		dominio b
	where	a.cd_expressao = b.cd_exp_dominio
	and	x.cd_funcao = cd_funcao_p
	and	b.cd_dominio = x.cd_dominio
	and	a.cd_expressao not in (286382, 282905, 613641, 288924)
	union
	select	b.cd_exp_valor_dominio, -- parameter domain value
		'Parameter ' || x.nr_sequencia || ' domain (' || b.cd_dominio || ') value',
		b.ds_valor_dominio
	from	funcao_parametro x,
		dic_expressao a,
		valor_dominio_v b
	where	a.cd_expressao = x.cd_exp_dependencia
	and	x.cd_funcao = cd_funcao_p
	and	b.cd_dominio = x.cd_dominio
	and	a.cd_expressao not in (286382, 282905, 613641, 288924);
	
c03_w	C03%rowtype;

	function obterExpressaoIdioma(	cd_expressao_p	number,
					nr_seq_idioma_p	number) return varchar2 is
	ds_lang_w	varchar2(20);
	
	ds_retorno_w	varchar2(4000);
	begin
	
		if	(nr_seq_idioma_w	= 2) then
			ds_lang_w	:=	'es';
		elsif	(nr_seq_idioma_w	= 5) then
			ds_lang_w	:=	'en';
		elsif	(nr_seq_idioma_w	= 6) then
			ds_lang_w	:=	'ar';
		elsif	(nr_seq_idioma_w	= 7) then
			ds_lang_w	:=	'de';
		end if;
		
		select	max(ds_expressao)
		into	ds_retorno_w
		from	dic_expressao_idioma
		where	cd_expressao = cd_expressao_p
		and	DS_IDIOMA	= ds_lang_w;
		
		return ds_retorno_w;
	
	end obterExpressaoIdioma;
	

begin
nr_seq_idioma_w := nvl(nr_seq_idioma_p, -1);

if	(nr_seq_idioma_w not in (2, 5, 6, 7)) then

	select	SUBSTR(obter_desc_expressao(CD_EXP_IDIOMA,DS_IDIOMA),1,80)
	into	ds_idioma_w
	from	tasy_idioma
	where	nr_sequencia = nr_seq_idioma_w;
	
	--Ra ise_application_error(-20011, 'Validation of the translation to the ' || ds_idioma_w || ' language is not possible yet!');

end if;

delete	inconsistencia_traducao
where	nm_usuario = nm_usuario_p;

-- procura schematic ativo
select	max(nr_sequencia)
into	nr_seq_funcao_schematic_w
from	funcao_schematic
where	cd_funcao = cd_funcao_p
and	nvl(ie_situacao_funcao, 'I') = 'A';

if	(nr_seq_funcao_schematic_w is null) then
	-- se n�o houver schematic ativo, procura um que esteja em desenvolvimento
	select	max(nr_sequencia)
	into	nr_seq_funcao_schematic_w
	from	funcao_schematic
	where	cd_funcao = cd_funcao_p
	and	nvl(ie_situacao_funcao, 'I') = 'D';
end if;

-- itens da fun��o n�o vinculados ao schematic
open C03;
loop
fetch C03 into	
	c03_w;
exit when C03%notfound;
	
	ds_expressao_de_w := obterExpressaoIdioma(c03_w.cd_exp_label,nr_seq_idioma_w);
	
	
	if	(ds_expressao_de_w is null) and
		((ds_expressao_de_w is null) or
		 (c03_w.cd_exp_label is null)) and 
		((c03_w.ds_label is not null) or 
		 (c03_w.cd_exp_label is not null)) then
	
		if	(c03_w.cd_exp_label is not null) then
			ds_texto_fixo_w := null;
		else
			ds_texto_fixo_w := c03_w.ds_label;
		end if;
	
		select	max(nr_sequencia)
		into	nr_seq_inconsistencia_igual_w
		from	inconsistencia_traducao
		where	cd_expressao = c03_w.cd_exp_label;
		
		if	(nr_seq_inconsistencia_igual_w is null) then
	
			select	inconsistencia_traducao_seq.nextval
			into	nr_inconsistencia_traducao_w
			from	dual;
			
			insert into inconsistencia_traducao (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_funcao_schematic,
				nr_seq_objeto_schematic,
				nr_seq_dic_objeto,
				nm_tabela,
				nm_atributo,
				nr_seq_visao,
				ie_tipo_componente,
				cd_expressao,
				ds_label,
				cd_funcao,
				nr_seq_idioma)
			values (nr_inconsistencia_traducao_w,
				sysdate,
				nm_usuario_p,
				nr_seq_funcao_schematic_w,
				null,
				null,
				null,
				c03_w.nm_atributo,
				null,
				'FUNCTION',
				c03_w.cd_exp_label,
				ds_texto_fixo_w,
				cd_funcao_p,
				nr_seq_idioma_w);
		end if;
			
	end if;	

end loop;
close C03;


ds_expressao_de_w := null;
open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	
	open C02;
	loop
	fetch C02 into	
		c02_w;
	exit when C02%notfound;
	
		ds_expressao_de_w := obterExpressaoIdioma(c02_w.cd_exp_label,nr_seq_idioma_w);
				
		if	(ds_expressao_de_w is null) and
			((ds_expressao_de_w is null) or
			 (c02_w.cd_exp_label is null)) and 
			((c02_w.ds_label is not null) or 
			 (c02_w.cd_exp_label is not null)) then
		
			if	(c02_w.cd_exp_label is not null) then
				ds_texto_fixo_w := null;
			else
				ds_texto_fixo_w := c02_w.ds_label;
			end if;
		
			select	max(nr_sequencia)
			into	nr_seq_inconsistencia_igual_w
			from	inconsistencia_traducao
			where	cd_expressao = c02_w.cd_exp_label;
			
			if	(nr_seq_inconsistencia_igual_w is null) then
		
				select	inconsistencia_traducao_seq.nextval
				into	nr_inconsistencia_traducao_w
				from	dual;
				
				insert into inconsistencia_traducao (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_funcao_schematic,
					nr_seq_objeto_schematic,
					nr_seq_dic_objeto,
					nm_tabela,
					nm_atributo,
					nr_seq_visao,
					ie_tipo_componente,
					cd_expressao,
					ds_label,
					cd_funcao,
					nr_seq_idioma)
				values (nr_inconsistencia_traducao_w,
					sysdate,
					nm_usuario_p,
					nr_seq_funcao_schematic_w,
					c01_w.nr_sequencia,
					c01_w.nr_seq_dic_objeto,
					c01_w.nm_tabela,
					c02_w.nm_atributo,
					c01_w.nr_seq_visao,
					c01_w.ie_tipo_componente,
					c02_w.cd_exp_label,
					ds_texto_fixo_w,
					cd_funcao_p,
					nr_seq_idioma_w);
			end if;
		end if;	
	end loop;
	close C02;	
	
end loop;
close C01;


commit;

end validar_traducao_funcao;
/