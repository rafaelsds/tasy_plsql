create or replace
procedure antecipar_resultado_prescr	(nr_prescricao_p		number,
					nr_seq_prescr_p			number,
					ds_observacao_antecipacao_p	varchar2,
					nr_seq_motivo_antecipacao_p 	number,
					dt_antecipacao_exame_p		date,
					nm_usuario_p			varchar2) is
begin

update 	prescr_procedimento
set	dt_antecipa_result = dt_antecipacao_exame_p,
	ds_observacao_antecipacao = ds_observacao_antecipacao_p,
	nm_usuario_antecipacao = nm_usuario_p,
	nr_seq_motivo_antecipacao = nr_seq_motivo_antecipacao_p
where nr_prescricao 	= nr_prescricao_p
and nr_sequencia 	= nr_seq_prescr_p;

update	prescr_procedimento
set	dt_neg_atencip_result = null,
	ds_obs_antecip_result = null,
	nr_seq_mot_anteci_result = null
where nr_prescricao 	= nr_prescricao_p
and nr_sequencia 	= nr_seq_prescr_p;
	

commit;

end;
/
