create or replace
procedure hpd_gerar_tributo_titulo_prev	(	nr_titulo_p		number,
						nm_usuario_p		varchar2,
						ie_baixa_titulo_p		varchar2,
						nr_bordero_p		number,
						dt_baixa_bordero_p		date,
						nr_seq_escrit_p		number,
						dt_baixa_escrit_p		date,
						dt_baixa_p		date,
						cd_estabelecimento_p	number,
						vl_pago_tit_p		number) is

cd_estabelecimento_w		number(5,0);
vl_tributo_w			number(15,2);
vl_titulo_w			number(15,2);
vl_diferenca_tit_nf_w		number(15,2);
vl_outros_vencimentos_w		number(15,2);
cd_tributo_w			number(10,0);
nr_seq_imposto_w		number(10,0);
dt_vencimento_w			date;
dt_venc_titulo_w		date;
dt_venc_tributo_w		date;
dt_contabil_w			date;
pr_aliquota_w			number(07,4);
cd_beneficiario_w		varchar2(14);
cd_conta_contabil_w		varchar2(20);
ie_tipo_tributacao_w		varchar2(255);
cd_cond_pagto_w			number(10,0);
vl_inss_ww			number(15,2);
ds_erro_w			varchar2(255);
ie_gerar_titulo_w		varchar2(01);
cd_cgc_titulo_w			varchar2(14);
cd_pf_titulo_w			varchar2(10);
qt_venc_w			number(05,0);
ds_venc_w			varchar2(2000);
ie_acumulativo_w		varchar2(1);
vl_trib_acum_w			number(15,2);
nr_seq_trans_reg_w		number(10,0);
nr_seq_trans_baixa_w		number(10,0);
cd_conta_financ_w		number(10,0);
ie_vencimento_w			varchar2(01);
ie_tipo_tributo_w		varchar2(15);
vl_minimo_base_w		number(15,2);
vl_minimo_tributo_w		number(15,2);
vl_base_nao_retido_w		number(15,2);
vl_trib_adic_w			number(15,2);
vl_base_adic_w			number(15,2);
vl_tributo_a_reter_w		number(15,2);
vl_base_a_reter_w		number(15,2);
vl_soma_trib_nao_retido_w	number(15,2);
vl_trib_nao_retido_w		number(15,2);
vl_soma_trib_adic_w		number(15,2);
vl_soma_base_nao_retido_w	number(15,2);
vl_soma_base_adic_w		number(15,2);
vl_inss_w			number(15,2);
vl_titulo_original_w		number(15,2);
vl_base_tributo_w		number(15,2);
vl_teto_base_w			number(15,2);
vl_trib_anterior_w		number(15,2);
vl_pago_w			number(15,2);
vl_total_base_w			number(15,2);
ie_irpf_w			varchar2(3);
ie_apuracao_piso_w		varchar2(3);
vl_reducao_w			number(15,2);
vl_base_calculo_paga_w		number(15,2);
vl_desc_dependente_w		number(15,2);
qt_dependente_w			number(15,2);
cd_darf_w			varchar2(20);
dt_tributo_w			date;
cd_variacao_w			varchar2(2);
ie_periodicidade_w		varchar2(1);
ie_baixa_titulo_w		varchar2(10);
cd_cnpj_raiz_w			varchar2(50);
ie_cnpj_w			varchar2(50);
vl_saldo_titulo_w		number(15,2);
vl_mercadoria_w			number(15,2);
vl_base_trib_calculado_w	number(15,2);
nr_seq_nota_fiscal_w		number(10,0);
nr_titulo_original_w		number(10,0);
nr_parcelas_w			number(10,0);
nr_total_parcelas_w		number(10,0);
vl_soma_trib_nao_retido_temp_w	number(15,2);
vl_soma_base_nao_retido_temp_w	number(15,2);
vl_soma_trib_adic_temp_w	number(15,2);
vl_soma_base_adic_temp_w	number(15,2);
vl_trib_anterior_temp_w		number(15,2);
vl_total_base_temp_w		number(15,2);
vl_base_retido_outro_w		number(15,2);
vl_reducao_temp_w		number(15,2);
vl_desdobrado_w			number(15,2);
vl_base_pago_adic_base_w	number(15,2);
vl_adiantamento_w		number(15,2);
vl_pago_outros_w		number(15,2);
vl_base_calc_paga_outros_w	number(15,2);
ie_tipo_titulo_w		varchar2(100);
dt_emissao_w			date;
cd_estab_titulo_w		number(4);
dt_inicio_vigencia_w		date;
dt_fim_vigencia_w		date;
cd_tributo_pf_w			varchar2(10);
vl_base_tributo_pf_w		number(15,2);
vl_tributo_pf_w			number(15,2);
cd_pessoa_fisica_pf_w		varchar2(10);
qt_registro_w			number(5);
ie_vencimento_pf_w		varchar2(01);
ds_emp_retencao_w		varchar2(255);
ie_pago_prev_tit_pagar_w	varchar2(255);
ie_tipo_data_w			varchar2(5);
ie_acumular_trib_liq_w		varchar2(1)	:= 'S';
vl_saldo_tit_w			number(15,2);
nr_ccm_w			number(10)	:= null;
cd_tipo_servico_w		varchar2(100);
ds_irrelevante_w		varchar2(4000);
cd_natureza_operacao_w		number(10);
cd_operacao_nf_w		number(4);
ie_restringe_estab_w		varchar2(1);
qt_pago_outros_w		number(15);
ie_trib_atualiza_saldo_w	varchar2(3);
nr_seq_classe_w			number(10);
cd_empresa_w			number(4);
dt_venc_inicio_w		date;
dt_venc_fim_w			date;
nr_titulo_w			number(10);
ie_valor_base_titulo_nf_w	varchar2(1);
vl_base_trib_nf_w		number(15,2);
ie_bordero_baixado_w		number(10);
ie_origem_titulo_w		titulo_pagar.ie_origem_titulo%type;
nr_seq_regra_irpf_w		regra_calculo_irpf.nr_sequencia%type;

cursor c01 is
	select	a.cd_tributo,
		a.ie_gerar_titulo_pagar,
		a.ie_vencimento,
		a.ie_tipo_tributo,
		a.ie_apuracao_piso,
		nvl(a.ie_baixa_titulo, 'N'),
		a.ie_cnpj,
		nvl(a.ie_acumular_trib_liq,'S'),
		a.ie_restringe_estab,
		nvl(a.ie_valor_base_titulo_nf, 'N')
	from	tributo a
	where	a.ie_conta_pagar	= 'S'
	and	a.ie_situacao		= 'A'
	and	(nr_ccm_w is null or nvl(a.ie_ccm,'S') = 'S')
	and	(not exists
			(select	1
			from	titulo_pagar_imposto x
			where	x.nr_titulo	= nr_titulo_p
			and	x.cd_tributo	= a.cd_tributo
			and	x.ie_pago_prev	= 'V') or
		nvl(a.ie_baixa_titulo, 'N')	= 'S')
	and	((nvl(a.ie_pf_pj,'A') 	= 'A') or
		 ((a.ie_pf_pj = 'PF') and (cd_pf_titulo_w is not null)) or
		 ((a.ie_pf_pj = 'PJ') and (cd_cgc_titulo_w is not null)))
	and	nvl(a.ie_baixa_titulo, 'N')	= nvl(ie_baixa_titulo_p, 'N')
	and	ie_tipo_titulo_w		not in ('4','5','27')
	and	(nvl(nr_seq_nota_fiscal_w,0) > 0 or nvl(a.ie_somente_nf,'N') = 'N')
	and	((nvl(ie_tipo_tributacao_w, 'X') <> '0') or (nvl(ie_super_simples, 'S') = 'S'))
	and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
	order	by decode(a.ie_tipo_tributo, 'INSS',1,2);

cursor c02 is
	select 	b.cd_tributo,
		b.vl_base_calculo,
		b.vl_tributo,
		b.dt_inicio_vigencia,
		b.dt_fim_vigencia,
		b.cd_pessoa_fisica,
		a.ie_vencimento,
		b.ds_emp_retencao,
		b.ie_tipo_data,
		nvl(b.ie_pago_prev_tit_pagar, 'R')
	from	pessoa_fisica_trib	b,
		tributo			a
	where	b.cd_tributo 	= a.cd_tributo
	and	cd_pessoa_fisica 	= cd_pf_titulo_w
	and	((b.cd_estabelecimento = cd_estabelecimento_p) or (b.cd_estabelecimento is null))
	and	decode(b.ie_tipo_data,'E',dt_emissao_w,'V',dt_venc_titulo_w) between b.dt_inicio_vigencia and dt_fim_vigencia
	and	(nr_ccm_w is null or nvl(a.ie_ccm,'S') = 'S');

begin
vl_adiantamento_w		:= 0;

open c02;
loop
fetch c02 into
	cd_tributo_pf_w,
	vl_base_tributo_pf_w,
	vl_tributo_pf_w,
	dt_inicio_vigencia_w,
	dt_fim_vigencia_w,
	cd_pessoa_fisica_pf_w,
	ie_vencimento_pf_w,
	ds_emp_retencao_w,
	ie_tipo_data_w,
	ie_pago_prev_tit_pagar_w;
exit when c02%notfound;
	begin
	select	count(*)
	into	qt_registro_w
	from	titulo_pagar_imposto	a,
		titulo_pagar		b
	where	a.nr_titulo			= b.nr_titulo
	and	a.cd_tributo			= cd_tributo_pf_w
	and	b.cd_pessoa_fisica		= cd_pessoa_fisica_pf_w
	and	trunc(a.dt_imposto,'month')	= trunc(dt_emissao_w,'month');

	if	(qt_registro_w = 0) then
		
		insert into w_hpd_tit_pag_trib_prev(
			cd_tributo,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			nr_titulo,
			nr_bordero,
			vl_base_calculo,
			vl_imposto )
		values(	cd_tributo_pf_w,
			sysdate,
			null,
			nm_usuario_p,
			null,
			nr_titulo_p,
			nr_bordero_p,
			vl_base_tributo_pf_w,
			trunc(vl_tributo_pf_w, 2) );
	end if;
	end;
end loop;
close c02;

if	(nvl(nr_bordero_p, 0) > 0) and
	(nvl(ie_baixa_titulo_p,'N') = 'S') then
	begin
	select	cd_estabelecimento,
		dt_baixa_bordero_p,
		cd_cgc,
		cd_pessoa_fisica,
		vl_bordero,
		obter_cnpj_raiz(cd_cgc)
	into	cd_estab_titulo_w,
		dt_venc_titulo_w,
		cd_cgc_titulo_w,
		cd_pf_titulo_w,
		vl_base_tributo_w,
		cd_cnpj_raiz_w
	from	titulo_pagar_bordero_v
	where	nr_titulo		= nr_titulo_p
	and	nr_bordero	= nr_bordero_p;
	exception
	when no_data_found then
		wheb_mensagem_pck.exibir_mensagem_abort(175622,'DS_ERRO=' || sqlerrm || ';' ||'NR_TIT=' || nr_titulo_p || ';' || 'NR_BORD=' || nr_bordero_p);
	end;

end if;

if	(cd_pf_titulo_w is not null) then
	select	max(nr_ccm)
	into	nr_ccm_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pf_titulo_w;
end if;

select	max(ie_tipo_tributacao)
into	ie_tipo_tributacao_w
from	pessoa_juridica
where	cd_cgc	= cd_cgc_titulo_w;

cd_estabelecimento_w	:= nvl(cd_estabelecimento_p,cd_estab_titulo_w);

select	a.vl_saldo_titulo,
	a.nr_seq_nota_fiscal,
	a.vl_titulo,
	a.ie_tipo_titulo,
	b.cd_tipo_servico,
	b.cd_operacao_nf,
	a.nr_seq_classe,
	ie_origem_titulo
into	vl_saldo_titulo_w,
	nr_seq_nota_fiscal_w,
	vl_titulo_w,
	ie_tipo_titulo_w,
	cd_tipo_servico_w,
	cd_operacao_nf_w,
	nr_seq_classe_w,
	ie_origem_titulo_w
from	nota_fiscal b,
	titulo_pagar a
where	a.nr_seq_nota_fiscal	= b.nr_sequencia(+)
and	a.nr_titulo		= nr_titulo_p;

select	a.cd_empresa
into	cd_empresa_w
from	estabelecimento a
where	a.cd_estabelecimento	= cd_estabelecimento_w;

vl_trib_acum_w	:= 0;
vl_titulo_original_w	:= vl_base_tributo_w;
dt_tributo_w	:= dt_venc_titulo_w;

open c01;
loop
fetch c01 into
	cd_tributo_w,
	ie_gerar_titulo_w,
	ie_vencimento_w,
	ie_tipo_tributo_w,
	ie_apuracao_piso_w,
	ie_baixa_titulo_w,
	ie_cnpj_w,
	ie_acumular_trib_liq_w,
	ie_restringe_estab_w,
	ie_valor_base_titulo_nf_w;
exit when c01%notfound;

	pr_aliquota_w			:= 0;

	select	max(a.nr_seq_nota_fiscal)
	into		nr_seq_nota_fiscal_w
	from	titulo_pagar a
	where	a.nr_titulo		= nr_titulo_p;

	if		(nr_seq_nota_fiscal_w is not null) then

			select	max(cd_natureza_operacao)
			into	cd_natureza_operacao_w
			from	nota_fiscal
			where	nr_sequencia	= nr_seq_nota_fiscal_w;

	end if;

	select	count(*)
	into	qt_pago_outros_w
	from	titulo_pagar_imposto
	where	nr_titulo		= nr_titulo_p
	and	cd_tributo	= cd_tributo_w
	and	ie_pago_prev	= 'P';

	obter_dados_trib_tit_pagar(	
		cd_tributo_w,
		cd_estabelecimento_w,
		cd_cgc_titulo_w,
		cd_pf_titulo_w,
		cd_beneficiario_w,
		pr_aliquota_w,
		cd_cond_pagto_w,
		cd_conta_financ_w,
		nr_seq_trans_reg_w,
		nr_seq_trans_baixa_w,
		vl_minimo_base_w,
		vl_minimo_tributo_w,
		ie_acumulativo_w,
		vl_teto_base_w,
		vl_desc_dependente_w,
		cd_darf_w,
		dt_tributo_w,
		cd_variacao_w,
		ie_periodicidade_w,
		'M',
		cd_natureza_operacao_w,
		cd_tipo_servico_w,
		null,
		null,
		null,
		ds_irrelevante_w,
		cd_operacao_nf_w,
		qt_pago_outros_w,
		ds_irrelevante_w,
		ds_irrelevante_w,
		vl_base_tributo_w,
		'N',
		nr_seq_classe_w,
		ie_origem_titulo_w,
		null,
		null);

	vl_base_tributo_w	:= vl_titulo_original_w;

	ie_irpf_w		:= 'N';

	if	(nvl(pr_aliquota_w,0) > 0) then
		-- calcular redu��o base irpf e saldo menos inss
		if	(ie_tipo_tributo_w = 'IR') and
			(cd_pf_titulo_w is not null) then
			select	nvl(max(ie_trib_atualiza_saldo),'S')
			into	ie_trib_atualiza_saldo_w
			from	parametros_contas_pagar a
			where	a.cd_estabelecimento = cd_estabelecimento_p;

			select	nvl(sum(a.vl_imposto),0)
			into	vl_inss_w
			from	tributo b,
				titulo_pagar_imposto a
			where	a.cd_tributo		= b.cd_tributo
			and	b.ie_tipo_tributo	= 'INSS'
			and	a.nr_titulo		= nr_titulo_p
			and	a.ie_pago_prev		= 'V'
			and	(ie_baixa_titulo_w	= 'N' or
				(nvl(b.ie_saldo_tit_pagar,'S') = 'N' or ie_trib_atualiza_saldo_w = 'N'));

			select	nvl(sum(a.vl_imposto),0)
			into	vl_inss_ww
			from	tributo b,
				w_titulo_pagar_imposto a
			where	a.cd_tributo		= b.cd_tributo
			and	b.ie_tipo_tributo	= 'INSS'
			and	a.nr_titulo		= nr_titulo_p
			and	ie_baixa_titulo_w	<> 'N';

			vl_inss_w	:= vl_inss_w + vl_inss_ww;

			select	nvl(qt_dependente,0)
			into	qt_dependente_w
			from	pessoa_fisica
			where	cd_pessoa_fisica	= cd_pf_titulo_w;

			ie_irpf_w		:= 'S';
			vl_base_tributo_w	:= vl_base_tributo_w - vl_inss_w;
		else
			vl_base_tributo_w	:= vl_titulo_original_w;
		end if;

		/* francisco - os 86089 - 13/03/2008 */
		if	(ie_vencimento_w = 'R') and
			(dt_emissao_w is not null) then
			dt_vencimento_w		:= dt_emissao_w;
			dt_venc_tributo_w	:= dt_emissao_w;
		elsif	(ie_vencimento_w = 'C') then
			dt_vencimento_w		:= nvl(nvl(dt_contabil_w,dt_emissao_w),sysdate);
			dt_venc_tributo_w	:= nvl(nvl(dt_contabil_w,dt_emissao_w),sysdate);
		else
			dt_vencimento_w		:= nvl(dt_venc_titulo_w,sysdate);
			dt_venc_tributo_w	:= nvl(dt_venc_titulo_w,sysdate);
		end if;
		/* fim francisco - os 86089 - 13/03/2008 */

		if	(cd_cond_pagto_w is not null) then
			calcular_vencimento(cd_estabelecimento_w, cd_cond_pagto_w,dt_vencimento_w, qt_venc_w, ds_venc_w);

			if	(qt_venc_w = 1) then
				dt_vencimento_w	:= to_date(substr(ds_venc_w,1,10),'dd/mm/yyyy');
			end if;
		end if;
		
		dt_venc_inicio_w	:= trunc(dt_venc_tributo_w,'mm');
		dt_venc_fim_w	:= fim_mes(dt_venc_tributo_w);

		select	nvl(sum(vl_soma_trib_nao_retido),0),
			nvl(sum(vl_soma_base_nao_retido),0),
			nvl(sum(vl_soma_trib_adic),0),
			nvl(sum(vl_soma_base_adic),0),
			nvl(sum(vl_tributo),0),
			nvl(sum(vl_total_base),0),
			nvl(sum(vl_reducao),0)
		into	vl_soma_trib_nao_retido_w,
			vl_soma_base_nao_retido_w,
			vl_soma_trib_adic_w,
			vl_soma_base_adic_w,
			vl_trib_anterior_w,
			vl_total_base_w,
			vl_reducao_w
		from	valores_tributo_v
		where	nvl(cd_empresa, cd_empresa_w)	= cd_empresa_w
		and	cd_tributo			= cd_tributo_w
		and	(
			  (
			    (cd_cgc		= cd_cgc_titulo_w) or
			    ((cd_cnpj_raiz		= cd_cnpj_raiz_w) and (ie_cnpj_w = 'Empresa'))
			  )
			  or
			  (cd_pessoa_fisica		= cd_pf_titulo_w)
			)
		and	dt_tributo between dt_venc_inicio_w and dt_venc_fim_w
		and	(
			  (ie_origem_valores 	= 'TP') or
			  (ie_apuracao_piso_w 	= 'S') or
			  (
			    (ie_baixa_titulo_w	= 'S') and
			    (ie_baixa_titulo = 'S')
			  )
			)
		and	((ie_restringe_estab_w	= 'N') or
			 (cd_estabelecimento	= cd_estabelecimento_w) or
			 (cd_estab_financeiro	= cd_estabelecimento_w))
		and	ie_baixa_titulo		= ie_baixa_titulo_w
		and	(
			  (
			    (ie_apuracao_piso_w = 'N') or
			    (ie_apuracao_piso_w = ie_base_calculo)
			  ) or
			  (
			    (ie_baixa_titulo_w	= 'S') and
			    (ie_baixa_titulo = 'S')
			  )
			);
		
		
		select	count(*)
		into	ie_bordero_baixado_w
		from	bordero_pagamento
		where	nr_bordero 	    = nr_bordero_p
		and	dt_real_pagamento  is not null;

		if (ie_bordero_baixado_w > 0) then
		
			vl_saldo_titulo_w := vl_titulo_w;
		
		end if;

		if	(ie_baixa_titulo_w = 'S') then
			-- edgar 03/03/2008, os 80633, qdo a baixa for total dever� adicionar o valor da baixa calculada dever� adicionar o valor ref � nf
			
			

			if	(vl_saldo_titulo_w + vl_adiantamento_w = vl_base_tributo_w) then
				-- edgar 31/03/2008, os 87455, tratar t�tulo desdobrado
				select	nvl(nr_titulo_original, 0),
					nvl(nr_parcelas,1),
					nvl(nr_total_parcelas,1)
				into	nr_titulo_original_w,
					nr_parcelas_w,
					nr_total_parcelas_w
				from	titulo_pagar
				where	nr_titulo	= nr_titulo_p;

				vl_desdobrado_w		:= 0;
				if	(nr_titulo_original_w > 0) and
					(ie_acumular_trib_liq_w = 'N') then	-- edgar 06/05/2009, os 138996, s� somar o valor desdobrado caso esteja parametrizado para n�o acumular somente na �ltima baixa
					select	nvl(sum(vl_titulo),0)
					into	vl_desdobrado_w
					from	titulo_pagar
					where	nr_titulo_original	= nr_titulo_original_w
					and	nr_titulo		<> nr_titulo_p
					and	ie_tipo_titulo		<> '4';	-- imposto
				end if;

				if	(nvl(nr_seq_nota_fiscal_w,0) > 0) and
					(nr_parcelas_w = nr_total_parcelas_w) and
					(nr_total_parcelas_w > 1) then /*francisco - 10/06/2009 - so fazer esse trat quando mais de uma parcela */
					/* edgar 31/03/2008, os 87455, s� gerar trib no �ltimo desdobramento */
					select	nvl(sum(vl_titulo),0)
					into	vl_outros_vencimentos_w
					from	titulo_pagar
					where	nr_seq_nota_fiscal	= nr_seq_nota_fiscal_w
					and	nr_titulo		<> nr_titulo_p
					and	ie_tipo_titulo		<> '4'
					and	ie_situacao		not in ('D','C','T');

					select	a.vl_mercadoria
					into	vl_mercadoria_w
					from	nota_fiscal a
					where	a.nr_sequencia		= nr_seq_nota_fiscal_w;

					if	(ie_acumular_trib_liq_w = 'N') then
						select	nvl(sum(vl_base_calculo),0)
						into	vl_base_trib_calculado_w
						from	titulo_pagar_imposto
						where	nr_titulo	= nr_titulo_p
						and	cd_tributo	= cd_tributo_w;

						vl_base_tributo_w		:= vl_mercadoria_w - vl_titulo_w + vl_base_tributo_w - vl_desdobrado_w - vl_outros_vencimentos_w +
									   (vl_titulo_w - vl_saldo_titulo_w) - vl_base_trib_calculado_w;
					else
						vl_base_tributo_w		:= vl_mercadoria_w - vl_titulo_w + vl_base_tributo_w - vl_desdobrado_w - vl_outros_vencimentos_w +
									   (vl_titulo_w - vl_saldo_titulo_w); -- edgar 23/01/2009, os 124531, inclu�do este calculo para adicionar o que j� foi baixado anteriormente
					end if;
				else
					select	nvl(sum(vl_base_calculo),0)
					into	vl_base_trib_calculado_w
					from	titulo_pagar_imposto
					where	nr_titulo	= nr_titulo_p
					and	cd_tributo	= cd_tributo_w;

					vl_diferenca_tit_nf_w		:= 0;

					if	(nvl(nr_seq_nota_fiscal_w,0) > 0) and
						(nr_total_parcelas_w = 1) then
						select	a.vl_mercadoria - vl_titulo_w
						into	vl_diferenca_tit_nf_w
						from	nota_fiscal a
						where	a.nr_sequencia		= nr_seq_nota_fiscal_w;
					end if;
					
					-- edgar / diether 30/03/2012, os 386764, obter novamente o valor do inss, pois a base do tributo est� sendo recalculada
					select	nvl(sum(a.vl_imposto),0)
					into	vl_inss_w
					from	tributo b,
						titulo_pagar_imposto a
					where	a.cd_tributo		= b.cd_tributo
					and	b.ie_tipo_tributo	= 'INSS'
					and	a.nr_titulo		= nr_titulo_p
					and	a.ie_pago_prev		= 'V'
					and	(ie_tipo_tributo_w = 'IR') and (cd_pf_titulo_w is not null);

					/* edgar 14/05/2008, os troquei o vl_titulo_original_w pelo vl_titulo_w */
					vl_base_tributo_w		:= vl_diferenca_tit_nf_w + to_number(obter_dados_tit_pagar(nr_titulo_p, 'V')) -
									(vl_saldo_titulo_w + vl_adiantamento_w) +
									   vl_base_tributo_w - 	vl_base_trib_calculado_w - vl_inss_w;
				end if;
			
				/*lhalves OS594282 em 21/06/2013 - Se parametrizado busca o valo base c�lculo da nota fiscal de origem */
			if	(nvl(ie_valor_base_titulo_nf_w,'N') = 'S') and
				(nvl(nr_seq_nota_fiscal_w,0) > 0) then
			
				select	max(vl_base_calculo)
				into	vl_base_trib_nf_w
				from	nota_fiscal_trib
				where	nr_sequencia	= nr_seq_nota_fiscal_w;
				
				if	(nvl(vl_base_trib_nf_w,0) <> 0) then
					vl_base_tributo_w	:= vl_base_trib_nf_w;
				end if;
			elsif	(nvl(ie_valor_base_titulo_nf_w,'N') = 'M') and
				(nvl(nr_seq_nota_fiscal_w,0) > 0) then
				begin
				select	max(vl_mercadoria)
				into	vl_mercadoria_w
				from	nota_fiscal
				where	nr_sequencia = nr_seq_nota_fiscal_w;
				if	(nvl(vl_mercadoria_w,0) <> 0) then
					vl_base_tributo_w	:= vl_mercadoria_w;
				end if;
				end;
			end if;
	
			
			end if;
			

			select	nvl(sum(a.vl_nao_retido),0) vl_soma_trib_nao_retido,
				nvl(sum(a.vl_base_nao_retido),0) vl_soma_base_nao_retido,
				nvl(sum(a.vl_trib_adic),0) vl_soma_trib_adic,
				nvl(sum(a.vl_base_adic),0) vl_soma_base_adic,
				nvl(sum(vl_imposto),0),
				nvl(sum(a.vl_base_calculo),0),
				nvl(sum(nvl(a.vl_reducao,0)),0)
			into	vl_soma_trib_nao_retido_temp_w,
				vl_soma_base_nao_retido_temp_w,
				vl_soma_trib_adic_temp_w,
				vl_soma_base_adic_temp_w,
				vl_trib_anterior_temp_w,
				vl_total_base_temp_w,
				vl_reducao_temp_w
			from	estabelecimento d,
				tributo c,
				titulo_pagar b,
				w_titulo_pagar_imposto a
			where	b.ie_situacao			<> 'C'
			and	nvl(cd_empresa,nvl(cd_empresa_w,0)) = nvl(cd_empresa_w,0)
			and	b.cd_estabelecimento	= d.cd_estabelecimento
			and	a.nr_titulo			= b.nr_titulo
			and	a.cd_tributo		= c.cd_tributo
			and	c.cd_tributo		= cd_tributo_w
			and	(
				  (
				    (b.cd_cgc			= cd_cgc_titulo_w) or
				    ((substr(obter_cnpj_raiz(b.cd_cgc),1,20)		= cd_cnpj_raiz_w) and (ie_cnpj_w = 'Empresa'))
				  )
				  or
				  (b.cd_pessoa_fisica		= cd_pf_titulo_w)
				)
			and	(a.nr_seq_escrit = nr_seq_escrit_p or
				trunc(decode(ie_vencimento_w,	'V', b.dt_vencimento_atual,
								'C', nvl(b.dt_contabil, b.dt_emissao),
								'B', dt_venc_titulo_w,
								b.dt_emissao
				), 'month') 	= trunc(dt_venc_tributo_w, 'month'))
			and	(ie_restringe_estab_w		= 'N' or
				b.cd_estabelecimento		= cd_estabelecimento_w or
				b.cd_estab_financeiro		= cd_estabelecimento_w)
			and	c.ie_baixa_titulo		= 'S'
			and	a.nr_seq_baixa	 is null
			and	(nr_seq_escrit_p is null or a.nr_seq_escrit is not null)
			and	(nr_bordero_p is null or a.nr_bordero = nr_bordero_p)
			and	(a.nr_titulo <> nr_titulo_p or vl_pago_tit_p <> 0);

			vl_soma_trib_nao_retido_w	:= vl_soma_trib_nao_retido_w + vl_soma_trib_nao_retido_temp_w;
			vl_soma_base_nao_retido_w	:= vl_soma_base_nao_retido_w + vl_soma_base_nao_retido_temp_w;
			vl_soma_trib_adic_w	:= vl_soma_trib_adic_w + vl_soma_trib_adic_temp_w;
			vl_soma_base_adic_w	:= vl_soma_base_adic_w + vl_soma_base_adic_temp_w;
			vl_trib_anterior_w		:= vl_trib_anterior_w + vl_trib_anterior_temp_w;
			vl_total_base_w		:= vl_total_base_w + vl_total_base_temp_w;
			vl_reducao_w		:= vl_reducao_w + vl_reducao_temp_w;
			
		end if;

		select	nvl(sum(b.vl_imposto),0),
			nvl(sum(b.vl_base_calculo),0)
		into	vl_pago_outros_w,
			vl_base_calc_paga_outros_w
		from	titulo_pagar_imposto b,
			titulo_pagar a
		where	a.nr_titulo			= b.nr_titulo
		and	a.nr_titulo			<> nr_titulo_p
		and	((a.cd_cgc		= cd_cgc_titulo_w) or
			 (a.cd_pessoa_fisica 	= cd_pf_titulo_w))
		and	b.cd_tributo		= cd_tributo_w
		and	trunc(b.dt_imposto, 'month')	= trunc(dt_venc_tributo_w, 'month')
		and	b.ie_pago_prev		= 'P'
		and	a.ie_situacao		in ('A','L');

		select	nvl(sum(vl_imposto),0) + vl_pago_outros_w,
			nvl(sum(vl_base_calculo),0) + vl_base_calc_paga_outros_w
		into	vl_pago_w,
			vl_base_calculo_paga_w
		from	titulo_pagar_imposto
		where	nr_titulo	= nr_titulo_p
		and	cd_tributo	= cd_tributo_w
		and	ie_pago_prev	= 'P';

		select	nvl(sum(vl_base_calculo),0)
		into	vl_base_pago_adic_base_w
		from	titulo_pagar_imposto
		where	nr_titulo	= nr_titulo_p
		and	cd_tributo	= cd_tributo_w
		and	ie_pago_prev	= 'S';

		select	nvl(sum(vl_base_calculo),0)
		into	vl_base_retido_outro_w
		from	titulo_pagar_imposto
		where	nr_titulo		= nr_titulo_p
		and	cd_tributo		= cd_tributo_w
		and	ie_pago_prev	= 'R';

		obter_valores_tributo(	ie_acumulativo_w,
					pr_aliquota_w,
					vl_minimo_base_w,
					vl_minimo_tributo_w,
					vl_soma_trib_nao_retido_w,
					vl_soma_trib_adic_w,
					vl_soma_base_nao_retido_w,
					vl_soma_base_adic_w,
					vl_base_tributo_w,
					vl_tributo_w,
					vl_trib_nao_retido_w,
					vl_trib_adic_w,
					vl_base_nao_retido_w,
					vl_base_adic_w,
					vl_teto_base_w,
					vl_trib_anterior_w,
					ie_irpf_w,
					vl_total_base_w,
					vl_reducao_w,
					vl_desc_dependente_w,
					qt_dependente_w,
					vl_base_calculo_paga_w,
					vl_base_pago_adic_base_w,
					vl_base_retido_outro_w,
					obter_outras_reducoes_irpf(cd_pf_titulo_w, cd_estabelecimento_w, dt_venc_tributo_w),
					sysdate,
					nr_seq_regra_irpf_w);				

		if	(vl_pago_w < vl_tributo_w) then
			vl_tributo_w	:= vl_tributo_w - vl_pago_w;
		elsif	(vl_pago_w > vl_tributo_w) then
			goto proximo;
		end if;

		-- edgar 02/02/2010, os 192586, n�o gerar tributo n�o retido negativo
		if	(vl_trib_nao_retido_w < 0) then
			vl_trib_nao_retido_w	:= 0;
		end if;

		if	(ie_baixa_titulo_w = 'N') then
			
			insert into w_hpd_tit_pag_trib_prev(
					cd_tributo,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					nr_titulo,
					nr_bordero,
					vl_base_calculo,
					vl_imposto )
			values(		cd_tributo_w,
					sysdate,
					null,
					nm_usuario_p,
					null,
					nr_titulo_p,
					nr_bordero_p,
					vl_base_tributo_w,
					trunc(vl_tributo_w, 2) );


			vl_trib_acum_w	:= vl_trib_acum_w + trunc(vl_tributo_w, 2);
		else
			insert into w_hpd_tit_pag_trib_prev(
					cd_tributo,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					nr_titulo,
					nr_bordero,
					vl_base_calculo,
					vl_imposto )
			values(		cd_tributo_w,
					sysdate,
					null,
					nm_usuario_p,
					null,
					nr_titulo_p,
					nr_bordero_p,
					vl_base_tributo_w,
					trunc(vl_tributo_w, 2) );
		end if;

	end if;
	<<proximo>>
	null;
end loop;
close c01;

commit;

end hpd_gerar_tributo_titulo_prev;
/