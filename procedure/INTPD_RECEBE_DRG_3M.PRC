create or replace 
procedure intpd_recebe_drg_3m(nr_sequencia_p number) is
ds_log_w      intpd_fila_transmissao.ds_message_response%type;
pac_drg_w     philips_json;

begin
select	fltr.ds_message_response
into	ds_log_w
from	intpd_fila_transmissao fltr
where	fltr.nr_sequencia = nr_sequencia_p;

pac_drg_w	:= philips_json(ds_log_w);
ds_log_w	:= pac_drg_w.get('dsXml').get_string();
kodip_pck.recebe_drg_3m(nr_sequencia_p, ds_log_w);

end intpd_recebe_drg_3m;
/
