create or replace
procedure reopen_case_reason( nr_sequencia_p	number,
                              nr_seq_motivo_p number, 
                              ds_motivo_reabertura_p varchar2, 
                              nm_usuario_p varchar2) is 


begin

if ((nr_sequencia_p is not null)and (nr_seq_motivo_p is not null)) then

	update	episodio_paciente
	set	ds_motivo_reabertura	  = ds_motivo_reabertura_p,
		nm_usuario_reabertura		  = nm_usuario_p,
    nr_seq_motivo_reabertura  = nr_seq_motivo_p,
    dt_reabertura             = sysdate
	where 	nr_sequencia		= nr_sequencia_p;
  
end if;

commit;

end reopen_case_reason;
/
