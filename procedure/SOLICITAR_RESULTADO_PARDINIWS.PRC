create or replace
PROCEDURE solicitar_resultado_pardiniws (
    NR_SEQ_LOTE_EXTERNO_P number,
    CD_ESTABELECIMENTO_P number,
    DT_INICIAL date default null,
    DT_FINAL date default null) is
BEGIN

    gravar_agend_integracao(678,
            'nr_seq_lote=' || NR_SEQ_LOTE_EXTERNO_P || ';' ||
            'cd_estabelecimento=' || CD_ESTABELECIMENTO_P || ';' ||
            'dt_inicial=' || to_char(DT_INICIAL, 'yyyy-mm-dd') || ';' ||
            'dt_final=' || to_char(DT_FINAL, 'yyyy-mm-dd') || ';' ||
            'hr_inicial=' || to_char(DT_INICIAL, 'hh24:mi:ss') || ';' ||
            'hr_final=' || to_char(DT_FINAL, 'hh24:mi:ss') || ';');

    lab_set_lote_integrando(
        nr_sequencia_p => nr_seq_lote_externo_p,
        ie_lote_integrando_p => 'R'
    );

END solicitar_resultado_pardiniws;
/
