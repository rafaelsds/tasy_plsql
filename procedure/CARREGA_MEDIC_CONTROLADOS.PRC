create or replace
procedure carrega_medic_controlados(	nm_usuario_p		varchar2) is 


cd_dcb_w		varchar2(20);
ds_dcb_w		varchar2(255);
nr_cas_w		varchar2(20);
nr_sequencia_w		number(10);
qt_existe_w		number(10);

cursor c01 is
select	cd_dcb,
	ds_dcb,
	nr_cas
from	w_carga_dcb_medic_control;

begin

select	count(*)
into	qt_existe_w
from	medic_controlado;

if	(qt_existe_w > 0) then

	delete from medic_controlado where nr_seq_dcb is not null;

	gravar_carga_dcb_historico(
		'Limpeza na tabela MEDIC_CONTROLADO',
		'A procedure CARREGA_MEDIC_CONTROLADOS limpou a tabela MEDIC_CONTROLADO.',
		nm_usuario_p);
end if;

select	count(*)
into	qt_existe_w
from	medic_ficha_tecnica
where	nr_seq_dcb is not null;

if	(qt_existe_w > 0) then

	update medic_ficha_tecnica 
	set nr_seq_dcb = null
	where nr_seq_dcb is not null;

	gravar_carga_dcb_historico(
		'Limpeza na tabela MEDIC_FICHA_TECNICA',
		'A procedure CARREGA_MEDIC_CONTROLADOS limpou o v�nculo da tabela MEDIC_FICHA_TECNICA com a tabela DCB_MEDIC_CONTROLADO(NR_SEQ_DCB).',
		nm_usuario_p);
end if;

select	count(*)
into	qt_existe_w
from	paciente_alergia
where	nr_seq_dcb is not null;

if	(qt_existe_w > 0) then

	update paciente_alergia 
	set nr_seq_dcb = null
	where nr_seq_dcb is not null;
	
	gravar_carga_dcb_historico(
		'Limpeza na tabela PACIENTE_ALERGIA',
		'A procedure CARREGA_MEDIC_CONTROLADOS limpou o v�nculo da tabela PACIENTE_ALERGIA com a tabela DCB_MEDIC_CONTROLADO(NR_SEQ_DCB).',
		nm_usuario_p);
end if;

select	count(*)
into	qt_existe_w
from	material_dcb;

if	(qt_existe_w > 0) then

	delete from material_dcb where nr_seq_dcb is not null;
	
	gravar_carga_dcb_historico(
		'Limpeza na tabela MATERIAL_DCB',
		'A procedure CARREGA_MEDIC_CONTROLADOS limpou a tabela MATERIAL_DCB.',
		nm_usuario_p);
end if;

select	count(*)
into	qt_existe_w
from	historico_saude
where	nr_seq_dcb is not null;

if	(qt_existe_w > 0) then
	
	update historico_saude 
	set nr_seq_dcb = null
	where nr_seq_dcb is not null;
	
	gravar_carga_dcb_historico(
		'Limpeza na tabela HISTORICO_SAUDE',
		'A procedure CARREGA_MEDIC_CONTROLADOS limpou o v�nculo da tabela HISTORICO_SAUDE com a tabela DCB_MEDIC_CONTROLADO(NR_SEQ_DCB).',
		nm_usuario_p);
end if;

select	count(*)
into	qt_existe_w
from	dcb_medic_controlado;

if	(qt_existe_w > 0) then	
	
	delete from dcb_medic_controlado;
	
	gravar_carga_dcb_historico(
		'Limpeza na tabela DCB_MEDIC_CONTROLADO',
		'A procedure CARREGA_MEDIC_CONTROLADOS limpou a tabela DCB_MEDIC_CONTROLADO.',
		nm_usuario_p);
end if;

open C01;
loop
fetch C01 into	
	cd_dcb_w,
	ds_dcb_w,
	nr_cas_w;
exit when C01%notfound;
	begin
	
	select	nvl(max(nr_sequencia),0) +1
	into	nr_Sequencia_w
	from	dcb_medic_controlado;
	
	insert into dcb_medic_controlado(
		nr_sequencia,
		cd_dcb,
		ds_dcb,
		dt_atualizacao,
		nm_usuario,
		cd_cas,
		ie_situacao,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_Sequencia_w,
		cd_dcb_w,
		ds_dcb_w,
		sysdate,
		nm_usuario_p,
		nr_cas_w,
		'A',
		sysdate,
		nm_usuario_p);
	end;
end loop;
close C01;

gravar_carga_dcb_historico(
		'Grava��o na tabela DCB_MEDIC_CONTROLADO',
		'A procedure CARREGA_MEDIC_CONTROLADOS carregou as informa��es na tabela DCB_MEDIC_CONTROLADO.', 
		nm_usuario_p);

delete from w_carga_dcb_medic_control;

commit;

end carrega_medic_controlados;
/