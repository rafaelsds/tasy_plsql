create or replace
procedure pls_desvincular_pag_lote_notif
			(	nr_seq_lote_p		number,
				nr_seq_notific_pag_p	number,
				nm_usuario_p		varchar2) is 

ds_titulos_w			varchar2(500)	:= null;
ds_titulo_aux_w			varchar2(10)	:= null;
nr_seq_pagador_w		number(10);

cursor C01 is
	select	to_char(nr_titulo)
	from	pls_notificacao_item
	where	nr_seq_notific_pagador	= nr_seq_notific_pag_p;

begin
open C01;
loop
fetch C01 into	
	ds_titulo_aux_w;
exit when C01%notfound;
	begin
	if	(length(ds_titulos_w || ', ' || ds_titulo_aux_w) < 500) then
		ds_titulos_w	:= ds_titulos_w	|| ds_titulo_aux_w || ', ';
	end if;
	end;
end loop;
close C01;

select	max(nr_seq_pagador)
into	nr_seq_pagador_w
from	pls_notificacao_pagador
where	nr_sequencia	= nr_seq_notific_pag_p;

delete	from pls_notificacao_item
where	nr_seq_notific_pagador	= nr_seq_notific_pag_p;

delete	from pls_notificacao_pagador
where	nr_seq_lote	= nr_seq_lote_p
and	nr_sequencia	= nr_seq_notific_pag_p;

insert into pls_notificacao_lote_log
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_lote,
	nr_seq_pagador_exc,
	ds_log)
values	(pls_notificacao_lote_log_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_lote_p,
	nr_seq_pagador_w,
	'Excluido o pagador ' || nr_seq_pagador_w || ' - ' || pls_obter_dados_pagador(nr_seq_pagador_w,'N') || ' e os t�tulos: ' || substr(ds_titulos_w,1,length(ds_titulos_w)-2));

pls_atualizar_valor_notific(nr_seq_lote_p);

commit;

end pls_desvincular_pag_lote_notif;
/