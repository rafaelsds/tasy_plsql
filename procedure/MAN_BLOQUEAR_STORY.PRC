create or replace
procedure man_bloquear_story(
			nr_story_p		number,
			ds_motivo_p		varchar2,
			nm_usuario_p		varchar2) is 

begin

update	desenv_story
set	ds_bloqueio = ds_motivo_p
where	nr_story = nr_story_p;

commit;

end man_bloquear_story;
/