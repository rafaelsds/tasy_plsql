create or replace
procedure convenio_ret_glosa(
			nr_seq_conpaci_ret_hist_p	number,
			nr_sequencia_p			number,
			nm_usuario_p			Varchar2) is 
begin
update	convenio_retorno_glosa
set	nr_seq_conpaci_ret_hist = nr_seq_conpaci_ret_hist_p
where	nr_sequencia = nr_sequencia_p;
commit;
end convenio_ret_glosa;
/