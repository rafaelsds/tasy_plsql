CREATE OR REPLACE
PROCEDURE GERAR_W_INTEGR_REFERENCIAL(	nm_tabela_origem_p	varchar2,
					nm_tabela_destino_p	varchar2) IS

nm_tabela_referencia_w	varchar2(50);
nm_tabela_w		varchar2(50);
qt_sem_conferencia_w	number(10);
ie_achou_tabela_w	boolean		:= false;
nr_caminho_w		number(10);
qt_sem_caminho_w	number(10)	:= 1;
ie_ordem_w		number(10);
qt_contador_w		number(10);
nr_sequencia_w		number(10)	:= 0;

cursor	c01 is
select	a.nm_tabela
from	w_tabela_integridade a
where	a.dt_conferencia	is null;

cursor	c02 is
select	a.nm_tabela_referencia
from	integridade_referencial a
where	not exists
	(select	1
	from	w_tabela_integridade x
	where	x.nm_tabela	= a.nm_tabela_referencia)
and	a.nm_tabela_referencia	<> nm_tabela_origem_p
and	a.nm_tabela		= nm_tabela_w;

cursor	c03 is
select	distinct
	a.nm_tabela
from	integridade_referencial a
where	exists
	(select	1
	from	w_tabela_integridade x
	where	x.nm_tabela	= a.nm_tabela)
and	a.nm_tabela		<> nm_tabela_destino_p
and	a.nm_tabela_referencia	= nm_tabela_destino_p;

cursor	c04 is
select	nm_tabela_destino_p,
	1 ie_ordem
from	dual
union all
select	nm_tabela_w,
	2 ie_ordem
from	dual
union all
select	distinct
	a.nm_tabela,
	decode(a.nm_tabela,nm_tabela_origem_p,3,4) ie_ordem
from	integridade_referencial a
where	not exists
	(select	1
	from	w_tabela_integr_caminho x
	where	x.nm_tabela		= a.nm_tabela
	and	x.nm_tabela_origem	= nm_tabela_w)
and	exists
	(select	1
	from	w_tabela_integridade x
	where	x.nm_tabela	= a.nm_tabela)
and	a.nm_tabela		<> nm_tabela_destino_p
and	a.nm_tabela		<> nm_tabela_w
and	a.nm_tabela_referencia	= nm_tabela_w
order by	2;

cursor	c05 is
select	distinct
	a.nr_caminho
from	w_tabela_integr_caminho a
order by	a.nr_caminho;

begin

delete	from w_tabela_integridade;
delete	from w_tabela_integr_caminho;

/* diferentes integridades */
qt_sem_conferencia_w	:= 1;

insert	into w_tabela_integridade
	(nm_tabela,
	nm_usuario,
	dt_atualizacao)
values	(nm_tabela_origem_p,
	'Tasy',
	sysdate);

while	(qt_sem_conferencia_w	> 0) loop

	open	c01;
	loop
	fetch	c01 into
		nm_tabela_w;
	exit	when c01%notfound;

		ie_achou_tabela_w	:= false;

		/* marcar as tabelas que j� foram percorridas para n�o percorr�-las novamente */
		update	w_tabela_integridade
		set	dt_conferencia	= sysdate
		where	nm_tabela	= nm_tabela_w;

		/* n�o � necess�rio conferir as integridades da tabela destino */
		if	(nm_tabela_w	<> nm_tabela_destino_p) then

			open	c02;
			loop
			fetch	c02 into
				nm_tabela_referencia_w;
			exit	when (c02%notfound or ie_achou_tabela_w);


				insert	into w_tabela_integridade
					(nm_tabela,
					nm_tabela_pai,
					nm_usuario,
					dt_atualizacao)
				values	(nm_tabela_referencia_w,
					nm_tabela_w,
					'Tasy',
					sysdate);

				if	(nm_tabela_referencia_w	= nm_tabela_destino_p) then

					delete	from w_tabela_integridade
					where	nm_tabela_pai	= nm_tabela_w
					and	nm_tabela	<> nm_tabela_referencia_w;

					update	w_tabela_integridade
					set	dt_conferencia	= sysdate
					where	nm_tabela	= nm_tabela_referencia_w;

					/* quando chegar na tabela destino, n�o precisa verificar as demais integridades da tabela pai */
					ie_achou_tabela_w	:= true;

				end if;

			end	loop;
			close	c02;

		end if;

	end	loop;
	close	c01;

	select	count(*)
	into	qt_sem_conferencia_w
	from	w_tabela_integridade a
	where	a.dt_conferencia	is null;

end	loop;
/* fim diferentes integridades */

/* caminho mais curto */
open	c03;
loop
fetch	c03 into
	nm_tabela_w;
exit	when c03%notfound;

	nr_caminho_w		:= 0;
	ie_achou_tabela_w	:= false;

	open	c04;
	loop
	fetch	c04 into
		nm_tabela_referencia_w,
		ie_ordem_w;
	exit	when (c04%notfound or ie_achou_tabela_w);

		nr_caminho_w	:= nvl(nr_caminho_w,0) + 1;
		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;

		insert	into w_tabela_integr_caminho
			(nm_tabela,
			nm_tabela_origem,
			nr_caminho,
			nr_sequencia,
			nm_usuario,
			dt_atualizacao)
		values	(nm_tabela_referencia_w,
			nm_tabela_w,
			nr_caminho_w,
			nr_sequencia_w,
			'Tasy',
			sysdate);

		if	(nm_tabela_referencia_w	= nm_tabela_origem_p) then

			ie_achou_tabela_w	:= true;

		end if;

	end	loop;
	close	c04;

end	loop;
close	c03;

delete	from w_tabela_integr_caminho a
where	not exists
	(select	1
	from	w_tabela_integr_caminho x
	where	x.nm_tabela		= nm_tabela_origem_p
	and	x.nm_tabela_origem	= a.nm_tabela_origem);

/* manter apenas um caminho (para o caso de haver mais de um caminho de mesma dist�ncia) */
open	c05;
loop
fetch	c05 into
	nr_caminho_w;
exit	when c05%notfound;

	delete	from w_tabela_integr_caminho
	where	nr_sequencia	<>
		(select	max(x.nr_sequencia)
		from	w_tabela_integr_caminho x
		where	x.nr_caminho	= nr_caminho_w)
	and	nr_caminho	= nr_caminho_w;

end	loop;
close	c05;

/* fim caminho mais curto */

commit;

end GERAR_W_INTEGR_REFERENCIAL;
/