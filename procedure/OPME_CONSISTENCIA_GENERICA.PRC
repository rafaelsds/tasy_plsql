create or replace
procedure opme_consistencia_generica(
			nr_seq_agenda_p		number,
			ds_mensagem_p	out	varchar2) as


qt_existe_w	number(10);
ds_mensagem_w	varchar2(255);

begin

ds_mensagem_p	:= null;

select	count(*)
into	qt_existe_w
from	agenda_paciente
where	nr_sequencia	= nr_seq_agenda_p
and	ie_opme_integracao <> 'S';
if	(qt_existe_w = 1) then
	ds_mensagem_w	:= wheb_mensagem_pck.get_texto(802939) || nr_seq_agenda_p;
end if;

select	count(*)
into	qt_existe_w
from	agenda_paciente
where	nr_sequencia	= nr_seq_agenda_p;
if	(qt_existe_w = 0) then
	ds_mensagem_w	:= wheb_mensagem_pck.get_texto(802944) || nr_seq_agenda_p;
end if;


if	(nvl(ds_mensagem_w, 'X') <> 'X') then
	ds_mensagem_p	:= substr(ds_mensagem_w,1,255);
end if;

end opme_consistencia_generica;
/
