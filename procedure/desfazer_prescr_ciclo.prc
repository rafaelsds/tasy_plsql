create or replace procedure desfazer_prescr_ciclo( nr_seq_paciente_p    paciente_atendimento.nr_seq_paciente%type,
                                                   nr_ciclo_p           paciente_atendimento.nr_ciclo%type,
                                                   nm_usuario_p			varchar2) is
                                                  
nr_prescricao_w   paciente_atendimento.nr_prescricao%type;

cursor c01 is
    select  nr_prescricao
    from    paciente_atendimento
    where   nr_seq_paciente = nr_seq_paciente_p
    and     nr_ciclo        = nr_ciclo_p;


begin

open c01;
loop
    fetch c01 into
        nr_prescricao_w;
    exit when c01%notfound;
    desfazer_prescr_oncologia(nr_prescricao_w, nr_seq_paciente_p, nm_usuario_p);
end loop;
close c01;

end desfazer_prescr_ciclo;
/
