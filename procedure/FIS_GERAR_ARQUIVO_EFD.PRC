create or replace
procedure fis_gerar_arquivo_efd
			(	nr_seq_controle_p	number,
				nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Chamar as rotinas que geram os registros EFD
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
-------------------------------------------------------------------------------------------------------------------
Referências:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dt_ref_final_w			date;
dt_ref_inicial_w		date;	
dt_ano_w				date;
ie_mes_inventario_w		number(1);
qt_contador_w			number(10);
nr_seq_regra_sped_w		number(10);
cd_estabelecimento_w	number(5);
cd_empresa_w			number(4);
qt_linha_w				number(10) := 0;
linha_0900_w			number(10) := 0;
nr_sequencia_w			number(10);
qt_linhas_w				number(10);
qt_total_w				number(10);
ds_arquivo_w			varchar2(4000);
ds_arquivo_ww			varchar2(4000);	
ie_forma_escrituracao_w	varchar2(15);
cd_registro_w			varchar2(15);
ds_sep_w				varchar2(01);
ie_tipo_sped_w			varchar2(2);
ie_local_gerar_w		varchar2(1);
ds_proc_longo_w			varchar2(40);
gerar_0900_w			varchar(1) := 'N';

cd_estabelecimento_scp_w	fis_efd_controle.cd_estabelecimento_scp%type;

cursor c_registro is
	select	cd_registro
	from	fis_regra_efd_reg
	where	nr_seq_regra_efd	= nr_seq_regra_sped_w
	and	ie_gerar		= 'S'
	order by nr_sequencia;
	
begin
delete	from w_sped_efd_m400;

delete	from w_efd_dados_arq;

delete	from fis_efd_nota_arq
where	nr_seq_controle	= nr_seq_controle_p;

commit;

select	max(nr_seq_regra_efd),
	max(cd_estabelecimento),
	max(cd_empresa),
	max(dt_ref_inicial),
	max(dt_ref_final),
	nvl(max(ie_mes_inventario), 2),
	max(cd_estabelecimento_scp)
into	nr_seq_regra_sped_w,
	cd_estabelecimento_w,
	cd_empresa_w,
	dt_ref_inicial_w,
	dt_ref_final_w,
	ie_mes_inventario_w,
	cd_estabelecimento_scp_w
from	fis_efd_controle
where	nr_sequencia	= nr_seq_controle_p;

--obter_param_usuario(5500,25,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_local_gerar_w);

select	max(dt_ano),
	max(ds_separador),
	max(ie_tipo_sped)
into	dt_ano_w,
	ds_sep_w,
	ie_tipo_sped_w
from	fis_regra_efd
where	nr_sequencia	= nr_seq_regra_sped_w;

select	count(*)
into	qt_total_w
from	fis_regra_efd_reg
where	nr_seq_regra_efd	= nr_seq_regra_sped_w
and	ie_gerar		= 'S';

delete	fis_efd_arquivo
where	nr_seq_controle_efd	= nr_seq_controle_p;

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	fis_efd_arquivo;

open c_registro;
loop
fetch c_registro into	
	cd_registro_w;
exit when c_registro%notfound;
	begin
	nr_sequencia_w	:= nr_sequencia_w;
	qt_linha_w	:= qt_linha_w;
	qt_contador_w 	:= qt_contador_w + 1;

	ds_proc_longo_w	:= substr(wheb_mensagem_pck.get_texto(300326),1,40);
	
	gravar_processo_longo(ds_proc_longo_w || ': ' || cd_registro_w || ' ('|| qt_contador_w ||'/' ||qt_total_w ||')','CTB_GERAR_ARQUIVO_ECD', qt_contador_w);
	
	if 	(ie_tipo_sped_w = 1) then --ICMS
		begin
		if	(cd_registro_w = '0000') then
			fis_gerar_reg_0000_efd(	nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w in ('0001','A001','C001','D001','E001','G001','H001','1001','9001')) then
			fis_gerar_reg_abertura_efd(	nr_seq_controle_p,
							nm_usuario_p,
							ds_sep_w,
							cd_registro_w,
							qt_linha_w,
							nr_sequencia_w);
		elsif	(cd_registro_w = 'E100') then
			fis_gerar_reg_E100_efd(	nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);		
		elsif	(cd_registro_w = 'E110') then
			fis_gerar_reg_E110_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);		
		elsif	(cd_registro_w = 'E111') then
			fis_gerar_reg_E111_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);		
		elsif	(cd_registro_w in ('0990','A990','C990','D990','E990','G990','H990','1990','9990','9999')) then
			fis_gerar_reg_fim_bloco_efd(	nr_seq_controle_p,
							nm_usuario_p,
							ds_sep_w,
							cd_registro_w,
							qt_linha_w,
							nr_sequencia_w);
		elsif	(cd_registro_w = '0005') then
			fis_gerar_reg_0005_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif 	(cd_registro_w = '0035') then 
			fis_gerar_reg_0035_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_scp_w,
						ds_sep_w,
						cd_empresa_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '0100') then
			fis_gerar_reg_0100_efd(	nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '0110') then
			fis_gerar_reg_0110_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '1010') then
			fis_gerar_reg_1010_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '0190') then
			fis_gerar_reg_0190_efd_icms(	nr_seq_controle_p,
							nm_usuario_p,
							cd_estabelecimento_w,
							dt_ref_inicial_w,
							dt_ref_final_w,
							cd_empresa_w,
							ds_sep_w,
							qt_linha_w,
							nr_sequencia_w);		
		elsif	(cd_registro_w = '0200') then
			fis_gerar_reg_0200_efd_icms(	nr_seq_controle_p,
							nm_usuario_p,
							cd_estabelecimento_w,
							dt_ref_inicial_w,
							dt_ref_final_w,
							cd_empresa_w,
							ds_sep_w,
							qt_linha_w,
							nr_sequencia_w,
							ie_mes_inventario_w);		
		elsif	(cd_registro_w = '0400') then
			fis_gerar_reg_0400_efd_icms(	nr_seq_controle_p,
							nm_usuario_p,
							cd_estabelecimento_w,
							dt_ref_inicial_w,
							dt_ref_final_w,
							cd_empresa_w,
							ds_sep_w,
							qt_linha_w,
							nr_sequencia_w);
		elsif	(cd_registro_w = '9900') then
			fis_gerar_reg_9900_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'A010') then
			fis_gerar_reg_A010_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'C100') then
			fis_gerar_reg_C100_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'C500') then
			fis_gerar_reg_C500_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'D100') then
			fis_gerar_reg_D100_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'D500') then
			fis_gerar_reg_D500_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '0150') then
			fis_gerar_0150_efd_icms(nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'H005') then
			fis_gerar_reg_H005_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w,
						ie_mes_inventario_w);
		end if;
		end;
	else
		begin --Pis/COFINS (EFD Contribuições)
		if	(cd_registro_w = '0000') then
			fis_reg_0000_efd_piscofins(	nr_seq_controle_p,
							nm_usuario_p,
							cd_estabelecimento_w,
							dt_ref_inicial_w,
							dt_ref_final_w,
							cd_empresa_w,
							ds_sep_w,
							qt_linha_w,
							nr_sequencia_w);
		elsif	(cd_registro_w in ('0001','A001','C001','D001','E001','F001','G001','H001','M001','1001','9001','I001')) then
			fis_gerar_reg_abertura_efd(	nr_seq_controle_p,
							nm_usuario_p,
							ds_sep_w,
							cd_registro_w,
							qt_linha_w,
							nr_sequencia_w);
		elsif	(cd_registro_w = '0900') then
			
			delete 	from fis_efd_0900 
			where 	nr_seq_controle = nr_seq_controle_p
			and		nm_usuario = nm_usuario_p;
			commit;
			
			linha_0900_w 	:= qt_linha_w;
			qt_linha_w 		:= qt_linha_w + 1;
			gerar_0900_w 	:= 'S';
			
		elsif	(cd_registro_w in ('0990','A990','C990','D990','E990','F990','G990','H990','M990','1990','9990','9999','I990')) then
			fis_gerar_reg_fim_bloco_efd(	nr_seq_controle_p,
							nm_usuario_p,
							ds_sep_w,
							cd_registro_w,
							qt_linha_w,
							nr_sequencia_w);
		elsif	(cd_registro_w = '0005') then
			fis_gerar_reg_0005_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif 	(cd_registro_w = '0035') then 
			fis_gerar_reg_0035_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_scp_w,
						ds_sep_w,
						cd_empresa_w,
						qt_linha_w,
						nr_sequencia_w);						
		elsif	(cd_registro_w = '0100') then
			fis_gerar_reg_0100_efd(	nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '0110') then
			fis_gerar_reg_0110_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '0500') then
			fis_gerar_reg_0500_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
						
		elsif	(cd_registro_w = '9900') then
			
			if 	(gerar_0900_w = 'S') then
				fis_gerar_reg_0900_efd(	nr_seq_controle_p,
										nm_usuario_p,
										ds_sep_w,
										linha_0900_w,
										nr_sequencia_w);
			end if;
			
			fis_gerar_reg_9900_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'A010') then
			fis_gerar_reg_A010_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'C010') then
			fis_gerar_reg_C010_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '0140') then
			fis_reg_0140_efd_piscofins(	nr_seq_controle_p,
							nm_usuario_p,
							cd_estabelecimento_w,
							dt_ref_inicial_w,
							dt_ref_final_w,
							cd_empresa_w,
							ds_sep_w,
							qt_linha_w,
							nr_sequencia_w);
		elsif	(cd_registro_W = 'M200') then
			fis_gerar_reg_m200_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_W = 'M600') then
			fis_gerar_reg_m600_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_W = 'M350') then
			fis_gerar_reg_m350_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);	
		elsif	(cd_registro_W = 'M400') then
			fis_gerar_reg_m400_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);	
		elsif	(cd_registro_W = 'M800') then
			fis_gerar_reg_m800_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);	
		elsif	(cd_registro_w = 'F010') then
			fis_gerar_reg_F010_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_W = 'M220') then
			fis_gerar_reg_m220_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'M620') then
			fis_gerar_reg_m620_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '1300') then
			fis_gerar_reg_1300_efd(	nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = '1700') then
			fis_gerar_reg_1700_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);	
		elsif	(cd_registro_w = '1900') then
			fis_gerar_reg_1900_efd( nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		elsif	(cd_registro_w = 'I010') then
			fis_gerar_reg_I010_efd(	nr_seq_controle_p,
						nm_usuario_p,
						cd_estabelecimento_w,
						dt_ref_inicial_w,
						dt_ref_final_w,
						cd_empresa_w,
						ds_sep_w,
						qt_linha_w,
						nr_sequencia_w);
		end if;
		end;
	end if;
	end;
	
	if (cd_registro_w = 'H990') then
		fis_gerar_reg_abertura_efd(	nr_seq_controle_p,
						nm_usuario_p,
						ds_sep_w,
						'K001',
						qt_linha_w,
						nr_sequencia_w);
						
		fis_gerar_reg_fim_bloco_efd(	nr_seq_controle_p,
						nm_usuario_p,
						ds_sep_w,
						'K990',
						qt_linha_w,
						nr_sequencia_w);
	end if;
	
end loop;
close c_registro;

/*AAMFIRMO OS 583192 10/05/2013*/
update   fis_efd_controle a  
set      a.dt_geracao	=  sysdate  
where    a.nr_sequencia	= nr_seq_controle_p;

commit;

end fis_gerar_arquivo_efd;
/