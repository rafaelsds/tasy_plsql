create or replace
procedure alterar_grau_satisf_man_os(
			nr_seq_p		number,
			nr_sequencia_p		number) is 

begin

update	man_ordem_servico 
set	nr_seq_justif_grau_satisf = nr_seq_p 
where	nr_sequencia = nr_sequencia_p; 

commit;

end alterar_grau_satisf_man_os;
/