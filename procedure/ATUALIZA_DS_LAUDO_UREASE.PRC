create or replace procedure atualiza_ds_laudo_urease(
	ds_laudo_p varchar2,
	nm_usuario_p varchar2,
	ie_urease_p varchar2,
	nr_seq_laudo_p number) is
	
begin
update 	laudo_paciente 
set 		ds_laudo = ds_laudo_p,
			dt_atualizacao = sysdate,
			dt_urease = sysdate,
			nm_usuario = nm_usuario_p,
			ie_urease = ie_urease_p
where 	nr_sequencia = nr_seq_laudo_p;

commit;

end atualiza_ds_laudo_urease;
/