create or replace
procedure GERAR_PAGTO_SICOOB_240
		(	nr_seq_envio_p		number,
			nm_usuario_p		varchar2) is

/* PADRAO CNAB 240 VERSAO 8.6 DE 05/01/2012 */

ds_conteudo_w		varchar2(240);
cd_estabelecimento_w	number(4);
nr_seq_apres_w		number(10)	:= 0;

/* header de arquivo */
cd_agencia_estab_w	varchar2(6);
cd_cgc_estab_w		varchar2(14);
cd_convenio_banco_w	varchar2(20);
dt_geracao_w		varchar2(14);
nm_empresa_w		varchar2(30);
nr_conta_estab_w	varchar2(12);
nr_remessa_w		number(10);
ie_digito_estab_w	varchar2(1);
ds_banco_w		varchar2(30);
cd_banco_cobr_w		number(3);

/* header de lote - DOC */
nr_lote_servico_w	number(10);
ie_forma_lanc_w		varchar2(2);
ie_tipo_pagamento_w	varchar2(3);
ie_finalidade_w		varchar2(2);
ds_endereco_w		varchar2(30);
nr_endereco_w		varchar2(5);
ds_complemento_w	varchar2(15);
ds_municipio_w		varchar2(20);
nr_cep_w		varchar2(8);
sg_estado_w		varchar2(15);

nr_tipo_pagamento_w     varchar2(2);
ie_tipo_titulo_w        varchar2(2);

cursor	c01 is
select	distinct
	decode(b.ie_tipo_pagamento,'CC','01','DOC','03','CCP','05','TED','41','OP','30','03'),
	b.ie_tipo_pagamento,
	decode(b.ie_tipo_pagamento,'CC','01','99')
from	titulo_pagar_escrit b,
	banco_escritural a
where	b.ie_tipo_pagamento	in ('DOC','CC','CCP','OP','TED')
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* detalhe - DOC */
nr_sequencia_w		number(10);
cd_camara_compensacao_w	varchar2(3);
cd_banco_w		number(3);
nr_conta_w		varchar2(20);
nm_pessoa_w		varchar2(30);
nr_titulo_w		number(10);
dt_remessa_retorno_w	date;
vl_escritural_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_despesa_w		number(15,2);
ie_tipo_inscricao_w	varchar2(1);
nr_inscricao_w		varchar2(14);
dt_vencimento_w		date;
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
cd_agencia_conta_w	varchar2(20);
ie_digito_conta_w	varchar2(1);
ds_endereco_det_w	varchar2(30);
nr_endereco_det_w	varchar2(5);
ds_complemento_det_w	varchar2(15);
ds_municipio_det_w	varchar2(20);
nr_cep_det_w		varchar2(8);
sg_estado_det_w		varchar2(15);
ds_bairro_det_w		varchar2(15);
cd_camara_w		varchar2(3);
qt_favorecido_w		number(10);
cd_conv_banco_interf_w 	banco_estab_interf.cd_convenio_banco%type;
nr_seq_conta_banco_w	banco_escritural.nr_seq_conta_banco%type;

cd_agencia_bancaria_w	titulo_pagar_escrit.cd_agencia_bancaria%type;
cd_digito_agencia_w 	titulo_pagar_escrit.ie_digito_agencia%type;

cursor	c02 is
select	b.cd_banco,
	lpad(substr(b.cd_agencia_bancaria,1,5),5,'0'),
	nvl(substr(b.ie_digito_agencia,1,1),' '),
	rpad(elimina_caractere_especial(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30)),30,' '),
	c.nr_titulo,
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	lpad(nvl(d.nr_cpf,c.cd_cgc),14,'0'),
	replace(b.nr_conta,'X','0'),
	replace(substr(nvl(b.ie_digito_conta,'0'),1,1),'X','0'),
	c.dt_vencimento_atual,
	b.vl_juros,
	b.vl_multa,	
	decode(c.cd_pessoa_fisica,null,'2','1') ie_tipo_inscricao,
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'R'),1,30),' '),30,' '),
	lpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'NR'),1,5),'0'),5,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CO'),1,15),' '),15,' '),
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CI'),1,20),' '),20,' '),
	lpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CEP'),1,8),'0'),8,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'UF'),1,2),' '),2,' '),
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'B'),1,15),' '),15,' '),
	decode(b.ie_tipo_pagamento,'DOC','700','TED','018','000') cd_camara,
	c.ie_tipo_titulo
from	pessoa_fisica d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	b.nr_titulo		= c.nr_titulo
and	b.ie_tipo_pagamento	= ie_tipo_pagamento_w
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer lote - DOC */
vl_total_w		number(15,2);

/* header de lote - BLQ */
cursor	c03 is
select	distinct
	decode(	(select	count(*)
		from	gps_titulo x
		where	x.nr_titulo	= b.nr_titulo),0,
		decode(	(select	count(*)
			from	darf_titulo_pagar x
			where	x.nr_titulo	= b.nr_titulo),0,
			decode(	b.ie_tipo_pagamento,'PC','11',
				decode(	decode(d.cd_banco_externo,null,b.cd_banco,somente_numero(d.cd_banco_externo)),cd_banco_cobr_w,'30',
					decode(	(select	count(*)
						from	banco x
						where	decode(x.cd_banco_externo,null,x.cd_banco,somente_numero(x.cd_banco_externo))	= somente_numero(substr(c.nr_bloqueto,1,3))),0,'11','31'))),
		decode(	c.nr_bloqueto,null,'16','11')),
	decode(c.nr_bloqueto,null,'17','11')) ie_forma_lanc,
	b.ie_tipo_pagamento
from	banco d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	b.cd_banco		= d.cd_banco(+)
and	b.nr_titulo		= c.nr_titulo
and	(b.ie_tipo_pagamento in ('BLQ','PC') or
	exists
	(select	1
	from	darf_titulo_pagar x
	where	x.nr_titulo	= b.nr_titulo) or
	exists
	(select	1
	from	gps_titulo x
	where	x.nr_titulo	= b.nr_titulo))
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p
order by	1;

/* detalhe - BLQ */
nr_bloqueto_w		varchar2(44);
nr_nosso_numero_w	varchar2(20);
ie_tipo_servico_w	varchar2(2);
ie_converte_bloq_w	varchar2(1);
ie_tipo_identificacao_w	varchar2(2);
qt_banco_w		number(10);
dt_apuracao_w		date;
cd_darf_w		varchar2(6);
nr_referencia_w		varchar2(17);
cd_pagamento_w		varchar2(6);
dt_referencia_w		varchar2(6);
vl_inss_w		number(15,2);
vl_outras_entidades_w	number(15,2);
nr_versao_w		varchar2(3);

cursor	c04 is
select	rpad(elimina_caractere_especial(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30)),30,' '),
	nvl(c.nr_titulo,0),
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	c.dt_vencimento_atual,
	b.vl_juros,
	b.vl_multa,
	rpad(substr(decode(nvl(ie_converte_bloq_w,'N'),'S',converte_codigo_bloqueto('Lido_Barra',c.nr_bloqueto),c.nr_bloqueto),1,44),44,' '),
	rpad(substr(nvl(c.nr_nosso_numero,' '),1,20),20,' '),
	decode(c.cd_cgc,null,'01','02') ie_tipo_identificacao,
	lpad(nvl(c.cd_cgc,d.nr_cpf),14,'0') nr_inscricao
from	banco e,
	pessoa_fisica d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	b.nr_titulo		= c.nr_titulo
and	decode(	(select	count(*)
		from	gps_titulo x
		where	x.nr_titulo	= b.nr_titulo),0,
		decode(	(select	count(*)
			from	darf_titulo_pagar x
			where	x.nr_titulo	= b.nr_titulo),0,
			decode(	b.ie_tipo_pagamento,'PC','11',
				decode(	decode(e.cd_banco_externo,null,b.cd_banco,somente_numero(e.cd_banco_externo)),cd_banco_cobr_w,'30',
					decode(	(select	count(*)
						from	banco x
						where	decode(x.cd_banco_externo,null,x.cd_banco,somente_numero(x.cd_banco_externo))	= somente_numero(substr(c.nr_bloqueto,1,3))),0,'11','31'))),
		decode(	c.nr_bloqueto,null,'16','11')),
	decode(c.nr_bloqueto,null,'17','11'))	= ie_forma_lanc_w
and	b.cd_banco		= e.cd_banco(+)
and	(b.ie_tipo_pagamento in ('BLQ','PC') or
	exists
	(select	1
	from	darf_titulo_pagar x
	where	x.nr_titulo	= b.nr_titulo) or
	exists
	(select	1
	from	gps_titulo x
	where	x.nr_titulo	= b.nr_titulo))
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer do arquivo */
qt_registro_w		number(10);

begin

delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

/* header de arquivo */
nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;

select	lpad(b.cd_cgc,14,'0'),
	rpad(substr(nvl(c.cd_convenio_banco,' '),1,20),20,' '),
	lpad(somente_numero(substr(d.cd_agencia_bancaria,1,5)) || somente_numero(replace(substr(nvl(d.ie_digito,'0'),1,1),'X','0')),6,'0'),
	lpad(somente_numero(replace(substr(c.cd_conta,1,12),'X','0')),12,'0'),
	rpad(elimina_caractere_especial(substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,30)),30,' '),
	to_char(sysdate,'ddmmyyyyhh24miss'),
	nvl(a.nr_remessa,a.nr_sequencia),
	b.cd_estabelecimento,
	a.dt_remessa_retorno,
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'R'),1,30),' '),30,' '),
	lpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'NR'),1,5),'0'),5,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'CO'),1,15),' '),15,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'CI'),1,20),' '),20,' '),
	lpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'CEP'),1,8),'0'),8,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'UF'),1,2),' '),2,' '),
	replace(nvl(substr(c.ie_digito_conta,1,1),'0'),'X','0'),
	c.cd_banco,
	substr(e.ds_banco,1,30) ds_banco,
	a.nr_seq_conta_banco
into	cd_cgc_estab_w,
	cd_convenio_banco_w,
	cd_agencia_estab_w,
	nr_conta_estab_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_remessa_w,
	cd_estabelecimento_w,
	dt_remessa_retorno_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_municipio_w,
	nr_cep_w,
	sg_estado_w,
	ie_digito_estab_w,
	cd_banco_cobr_w,
	ds_banco_w,
	nr_seq_conta_banco_w
from	banco e,
	agencia_bancaria d,
	banco_estabelecimento c,
	estabelecimento b,
	banco_escritural a
where	c.cd_banco		= e.cd_banco
and	c.cd_banco		= d.cd_banco
and	c.cd_agencia_bancaria	= d.cd_agencia_bancaria
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_sequencia		= nr_seq_envio_p;

if	(nr_seq_conta_banco_w is not null) then

	select	max(a.cd_convenio_banco)
	into	cd_conv_banco_interf_w
	from	banco_estab_interf a
	where	a.ie_tipo_interf_fin 		= 'PE' /*Somente se a interface estiver cadastrada como Pagamento escritural envio*/
	and	upper(a.ds_objeto)		= upper('GERAR_PAGTO_SICOOB_240')
	and	a.nr_seq_conta_banco		= nr_seq_conta_banco_w
	and	upper(a.nm_tabela)		= upper('W_ENVIO_BANCO');
	
	if	(cd_conv_banco_interf_w	is not null) then
		cd_convenio_banco_w	:= rpad(substr(nvl(cd_conv_banco_interf_w,' '),1,20),20,' ');
	end if;
end if;

obter_param_usuario(857,36,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_converte_bloq_w);

ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
			'0000' ||
			'0' ||
			rpad(' ',9,' ') ||
			'2' ||
			cd_cgc_estab_w ||
			cd_convenio_banco_w ||
			cd_agencia_estab_w ||
			nr_conta_estab_w ||
			ie_digito_estab_w ||
			' ' ||
			nm_empresa_w ||
			rpad(upper(ds_banco_w),30,' ') ||
			rpad(' ',10,' ') ||
			'1' ||
			dt_geracao_w ||
			lpad(nr_remessa_w,6,'0') ||
			'087' ||
			'00000' ||
			rpad(' ',69,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

open	c01;
loop
fetch	c01 into
	ie_forma_lanc_w,
	ie_tipo_pagamento_w,
	ie_finalidade_w;
exit	when c01%notfound;

	/* header de lote */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w		:= 0;

	ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'1' ||
				'C' ||
				'20' ||
				ie_forma_lanc_w ||
				'045' || --versao do layout
				' ' ||
				'2' ||
				cd_cgc_estab_w ||
				cd_convenio_banco_w ||
				cd_agencia_estab_w ||
				nr_conta_estab_w ||
				ie_digito_estab_w ||
				' ' ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_w ||
				nr_endereco_w ||
				ds_complemento_w ||
				ds_municipio_w ||
				nr_cep_w ||
				substr(sg_estado_w,1,2) ||
				rpad(' ',18,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c02;
	loop
	fetch	c02 into
		cd_banco_w,
		cd_agencia_bancaria_w,
		cd_digito_agencia_w,
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		nr_inscricao_w,
		nr_conta_w,
		ie_digito_conta_w,
		dt_vencimento_w,
		vl_juros_w,
		vl_multa_w,
		ie_tipo_inscricao_w,
		ds_endereco_det_w,
		nr_endereco_det_w,
		ds_complemento_det_w,
		ds_municipio_det_w,
		nr_cep_det_w,
		sg_estado_det_w,
		ds_bairro_det_w,
		cd_camara_w,
		ie_tipo_titulo_w;
	exit	when c02%notfound;

		select	count(*)
		into	qt_favorecido_w
		from	titulo_pagar_favorecido a
		where	a.nr_titulo	= nr_titulo_w;

		if	(nvl(qt_favorecido_w,0)	> 0) then

			select	somente_numero(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'B'),1,3)) cd_banco,
				lpad(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'A'),1,5),5,'0') cd_agencia_bancaria,
				nvl(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DA'),1,1),' ') cd_digito_agencia,
				replace(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'C'),1,255),'X','0') nr_conta,
				replace(nvl(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DC'),1,1),'0'),'X','0') ie_digito_conta
			into	cd_banco_w,
				cd_agencia_bancaria_w,
				cd_digito_agencia_w,
				nr_conta_w,
				ie_digito_conta_w
			from	titulo_pagar_favorecido a
			where	a.nr_titulo	= nr_titulo_w;

		end if;

		/* segmento A */
		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;
		vl_total_w	:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0));
		
		if      (ie_tipo_titulo_w =    '7') or 
		        (ie_tipo_titulo_w =    '8') then
		 
		        nr_tipo_pagamento_w := '11';
			 
		else
		
		        nr_tipo_pagamento_w := '07';
		
		end if;

		ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
					lpad(nr_lote_servico_w,4,'0') ||
					'3' ||
					lpad(nr_sequencia_w,5,'0') ||
					'A' ||
					'000' ||
					cd_camara_w ||
					lpad(cd_banco_w,3,'0') ||
					cd_agencia_bancaria_w ||
					cd_digito_agencia_w ||
					lpad(substr(nr_conta_w,1,12),12,'0') ||
					ie_digito_conta_w ||
					' ' ||
					nm_pessoa_w ||
					rpad(nr_titulo_w,20,' ') ||
					to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
					'BRL' ||
					'000000000000000' ||
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
					rpad(' ',20,' ') ||
					'00000000' ||
					'000000000000000' ||
					rpad(' ',40,' ') ||
					nr_tipo_pagamento_w ||
					'00005' ||
					rpad(' ',5,' ') ||
					'0' ||
					rpad(' ',10,' ');

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

		/* segmento B */
		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;

		ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
					lpad(nr_lote_servico_w,4,'0') ||
					'3' ||
					lpad(nr_sequencia_w,5,'0') ||
					'B' ||
					rpad(' ',3,' ') ||
					ie_tipo_inscricao_w ||
					nr_inscricao_w ||
					ds_endereco_det_w ||
					nr_endereco_det_w ||
					ds_complemento_det_w ||
					ds_bairro_det_w ||
					ds_municipio_det_w ||
					nr_cep_det_w ||
					substr(sg_estado_det_w,1,2) ||
					to_char(dt_vencimento_w,'ddmmyyyy') ||
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0) - nvl(vl_desconto_w,0),'9999999999990.00')),15,'0') ||
					'000000000000000' ||
					lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'9999999999990.00')),15,'0') ||
					lpad(somente_numero(to_char(nvl(vl_juros_w,0),'9999999999990.00')),15,'0') ||
					lpad(somente_numero(to_char(nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
					rpad(' ',15,' ') ||
					'0' ||
					rpad(' ',14,' ');

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

	end	loop;
	close	c02;

	/* trailer de lote */
	nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'5' ||
				rpad(' ',9,' ') ||
				lpad(nr_sequencia_w + 2,6,'0') ||
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') ||
				'000000000000000000' ||
				'000000' ||
				rpad(' ',175,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c01;

open	c03;
loop
fetch	c03 into
	ie_forma_lanc_w,
	ie_tipo_pagamento_w;
exit	when c03%notfound;

	/* header de lote - BLQ */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w		:= 0;

	if	(ie_forma_lanc_w	= '16') or
		(ie_forma_lanc_w	= '17') or
		(ie_forma_lanc_w	= '11') then

		ie_tipo_servico_w	:= '22';
		nr_versao_w		:= '012';

	else

		if	(cd_banco_cobr_w	= 1) then

			ie_tipo_servico_w	:= '20';

		else

			ie_tipo_servico_w	:= '03';

		end if;

		nr_versao_w	:= '040';

	end if;


	ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'1' ||
				'C' ||
				ie_tipo_servico_w ||
				ie_forma_lanc_w ||
				nr_versao_w ||
				' ' ||
				'2' ||
				cd_cgc_estab_w ||
				cd_convenio_banco_w ||
				cd_agencia_estab_w ||
				nr_conta_estab_w ||
				ie_digito_estab_w ||
				' ' ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_w ||
				nr_endereco_w ||
				ds_complemento_w ||
				ds_municipio_w ||
				nr_cep_w ||
				substr(sg_estado_w,1,2) ||
				rpad(' ',18,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c04;
	loop
	fetch	c04 into
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		dt_vencimento_w,
		vl_juros_w,
		vl_multa_w,
		nr_bloqueto_w,
		nr_nosso_numero_w,
		ie_tipo_identificacao_w,
		nr_inscricao_w;
	exit	when c04%notfound;


		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;
		vl_total_w	:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0));

		select	count(*)
		into	qt_banco_w
		from	banco a
		where	decode(a.cd_banco_externo,null,a.cd_banco,somente_numero(a.cd_banco_externo))	= somente_numero(substr(nr_bloqueto_w,1,3));

		/* segmento N - DARF normal */
		if	(ie_forma_lanc_w	= '16') then

			select	max(b.dt_apuracao) dt_apuracao,
				lpad(substr(nvl(max(b.cd_darf),'0'),1,6),6,'0') cd_darf,
				lpad(substr(nvl(max(b.nr_referencia),'0'),1,17),17,'0') nr_referencia
			into	dt_apuracao_w,
				cd_darf_w,
				nr_referencia_w
			from	darf b,
				darf_titulo_pagar a
			where	a.nr_seq_darf	= b.nr_sequencia
			and	a.nr_titulo	= nr_titulo_w;

			ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'N' ||
						'0' ||
						'00' ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(' ',20,' ') ||
						nm_empresa_w ||
						to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						cd_darf_w ||
						ie_tipo_identificacao_w ||
						cd_cgc_estab_w ||
						'16' ||
						to_char(dt_apuracao_w,'ddmmyyyy') ||
						nr_referencia_w ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') ||
						lpad(somente_numero(to_char(nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						lpad(somente_numero(to_char(nvl(vl_juros_w,0),'9999999999990.00')),15,'0') ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						rpad(' ',18,' ') ||
						rpad(' ',10,' ');

		/* segmento N - GPS */
		elsif	(ie_forma_lanc_w	= '17') then

			select	to_char(max(b.dt_referencia),'mmyyyy') dt_referencia,
				lpad(substr(nvl(max(b.cd_pagamento),'0'),1,6),6,'0') cd_pagamento,
				max(b.vl_inss) vl_inss,
				max(b.vl_outras_entidades) vl_outras_entidades
			into	dt_referencia_w,
				cd_pagamento_w,
				vl_inss_w,
				vl_outras_entidades_w
			from	gps b,
				gps_titulo a
			where	a.nr_seq_gps	= b.nr_sequencia
			and	a.nr_titulo	= nr_titulo_w;

			ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'N' ||
						'0' ||
						'00' ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(' ',20,' ') ||
						nm_empresa_w ||
						to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						cd_pagamento_w ||
						ie_tipo_identificacao_w ||
						cd_cgc_estab_w ||
						'17' ||
						dt_referencia_w ||
						lpad(somente_numero(to_char(nvl(vl_inss_w,0),'9999999999990.00')),15,'0') ||
						lpad(somente_numero(to_char(nvl(vl_outras_entidades_w,0),'9999999999990.00')),15,'0') ||
						lpad('0',15,'0') ||
						rpad(' ',45,' ') ||
						rpad(' ',10,' ');

		/* segmento O */
		elsif	(ie_forma_lanc_w	= '11') or
			(qt_banco_w		= 0) then

			ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'O' ||
						'0' ||
						'00' ||
						lpad(nvl(nr_bloqueto_w,' '),44,' ') ||
						nm_pessoa_w ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(' ',20,' ') ||
						rpad(' ',78,' ');


		/* segmento J */
		else

			ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'J' ||
						'0' ||
						'00' ||
						lpad(nvl(nr_bloqueto_w,' '),44,' ') ||
						lpad(nm_pessoa_w,30,' ') ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') ||
						lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'9999999999990.00')),15,'0') ||
						lpad(somente_numero(to_char(nvl(vl_multa_w,0) + nvl(vl_juros_w,0),'9999999999990.00')),15,'0') ||
						to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char((nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0)),'9999999999999990.00')),15,'0') ||
						'000000000000000' ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(nr_nosso_numero_w,20,' ') ||
						'09' ||
						rpad(' ',16,' ');
						
			insert	into	w_envio_banco
					(cd_estabelecimento,
					ds_conteudo,
					dt_atualizacao,
					nm_usuario,
					nr_seq_apres,
					nr_seq_apres_2,
					nr_sequencia)
			values	(	cd_estabelecimento_w,
					ds_conteudo_w,
					sysdate,
					nm_usuario_p,
					nr_seq_apres_w,
					nr_seq_apres_w,
					w_envio_banco_seq.nextval);

			nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
			nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
			qt_registro_w	:= nvl(qt_registro_w,0) + 1;
			
			ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||		--1/3
						lpad(nr_lote_servico_w,4,'0') ||	--4/7
						'3' ||					--8/8
						lpad(nr_sequencia_w,5,'0') ||		--9/13
						'J' ||					--14/14
						' ' ||					--15/15
						'01' ||					--16/17
						'52' ||					--18/19
						'2' ||					--20/20
						lpad(cd_cgc_estab_w,15,'0') ||		--21/35
						rpad(nm_empresa_w,40,' ') ||		--36/75
						substr(ie_tipo_identificacao_w,2,1) ||	--76/76
						lpad(nr_inscricao_w,15,'0') ||		--77/91
						rpad(nm_pessoa_w,40,' ') ||		--92/131
						'0' ||					--132/132
						rpad('0',15,'0') ||			--133/147
						rpad(' ',40,' ') ||			--148/187
						rpad(' ',52,' ');			--188/240
									

		end if;

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);
			
             /*segmento W*/
		if	(ie_forma_lanc_w	= '11') then
		
			nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
			nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
			qt_registro_w	:= nvl(qt_registro_w,0) + 1;
			
			ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'W' ||
						'0' ||
						'9' ||
						lpad(' ',160,' ') ||
						'01' ||
						lpad(' ',48,' ') ||
						lpad(' ',12,' ');
						
			insert	into w_envio_banco
					(cd_estabelecimento,
					ds_conteudo,
					dt_atualizacao,
					nm_usuario,
					nr_seq_apres,
					nr_seq_apres_2,
					nr_sequencia)
				values	(cd_estabelecimento_w,
					ds_conteudo_w,
					sysdate,
					nm_usuario_p,
					nr_seq_apres_w,
					nr_seq_apres_w,
					w_envio_banco_seq.nextval);
		end if;	


	end	loop;
	close	c04;

	/* trailer de lote */
	nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'5' ||
				rpad(' ',9,' ') ||
				lpad(nr_sequencia_w + 2,6,'0') ||
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') ||
				'000000000000000000' ||
				'000000' ||
				rpad(' ',175,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c03;

/* trailer de arquivo */
nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			lpad(nr_lote_servico_w,6,'0') ||
			lpad(nvl(qt_registro_w,0) + (nvl(nr_lote_servico_w,0) * 2) + 2,6,'0') ||	/* registros detalhe + header e trailer dos lotes + header e trailer do arquivo */
			'000000' ||
			rpad(' ',205,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

commit;

end GERAR_PAGTO_SICOOB_240;
/