create or replace
function fis_obter_aliq_pis_cofins( ie_regime_receita_p		varchar2,
				ie_pis_cofins_p			varchar2)
 		    	return varchar2 is

			
			
ds_retorno_w		varchar2(10);			
			
begin
if 	(ie_regime_receita_p in ('C','D'))and
	(ie_pis_cofins_p = 'PIS')then
	ds_retorno_w	:=	'0,65';
else	
	ds_retorno_w	:=	'1,65';
end if;

if 	(ie_regime_receita_p = 'C')and
	(ie_pis_cofins_p = 'COFINS')then
	ds_retorno_w	:=	'3,00';
elsif (ie_regime_receita_p  = 'N') and
	(ie_pis_cofins_p   = 'COFINS')then	
	ds_retorno_w	:=	'7,60';
elsif (ie_regime_receita_p  = 'D') and
	(ie_pis_cofins_p   = 'COFINS')then	
	ds_retorno_w	:=	'4,00';	
end if;

return	ds_retorno_w;

end fis_obter_aliq_pis_cofins;
/