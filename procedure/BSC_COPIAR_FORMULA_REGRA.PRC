create or replace
procedure bsc_copiar_formula_regra(	nm_usuario_p		varchar2,
				nr_seq_formula_p		number,
				nr_seq_regra_dest_p	number) is 

nr_seq_indicador_w		number(10);
ds_comentario_w		varchar2(255);
ie_origem_real_w		varchar2(15);
ds_sql_w			varchar2(4000);
ds_procedure_w		varchar2(60);
ie_regra_apuracao_w	varchar2(15);
nr_seq_formula_new_w	number(10);
nr_seq_formula_w		number(10);
ie_part_calc_w		varchar2(15);
vl_fixo_w			number(15,4);
nr_seq_calc_w		number(15);
nr_seq_result_w		number(10);
vl_maximo_w		number(15,4);
vl_minimo_w		number(15,4);
vl_resultado_w		number(15,4);
nr_seq_regra_form_w	number(10);
ds_origem_w		varchar2(2000);
ds_origem_ww		varchar2(2000);
ds_titulo_w		varchar2(80);
ie_estab_inf_w		varchar2(1);
ie_origem_ctb_w		varchar2(15);
nr_seq_mes_ref_w		number(10);
qt_mes_w		number(3);
ie_participacao_calc_w	varchar2(15);
nr_seq_ind_ref_w		number(10);

cursor	c01 is
select	a.nr_sequencia,
	a.ie_participacao_calc,
	a.vl_fixo,
	a.nr_seq_ind_ref
from	bsc_regra_formula a
where	 a.nr_seq_regra_calc = nr_seq_formula_p;

cursor	c02 is
select	a.nr_seq_calc,
	a.nr_seq_result,
	a.vl_maximo,
	a.vl_minimo,
	a.vl_resultado
from	bsc_regra_form_item a
where	a.nr_seq_formula = nr_seq_formula_w;

cursor	c03 is
select	a.ds_origem,
	a.ds_titulo,
	a.ie_estab_inf,
	a.ie_origem_ctb,
	a.nr_seq_mes_ref,
	a.qt_mes
from	bsc_regra_ctb_esp a
where	a.nr_seq_regra_calc = nr_seq_formula_p;

begin
select	a.nr_seq_indicador,
	a.ds_comentario,
	a.ie_origem_real,
	a.ds_sql,
	a.ds_procedure,
	a.nr_seq_calc,
	a.ie_regra_apuracao,
	a.ie_origem_ctb,
	a.ds_origem
into	nr_seq_indicador_w,
	ds_comentario_w,
	ie_origem_real_w,
	ds_sql_w,
	ds_procedure_w,
	nr_seq_calc_w,
	ie_regra_apuracao_w,
	ie_origem_ctb_w,
	ds_origem_w
from	bsc_regra_calc_ind a
where	a.nr_sequencia = nr_seq_formula_p;

select	bsc_regra_calc_ind_seq.nextval
into	nr_seq_formula_new_w
from	dual;

insert into	bsc_regra_calc_ind (nr_sequencia,
			nr_seq_regra,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_indicador,
			ds_comentario,
			ie_origem_real,
			ds_sql,
			ds_procedure,
			nr_seq_calc,
			ie_regra_apuracao,
			ie_origem_ctb,
			ds_origem)
		values (	nr_seq_formula_new_w,
			nr_seq_regra_dest_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_indicador_w,
			ds_comentario_w,
			ie_origem_real_w,
			ds_sql_w,
			ds_procedure_w,
			nr_seq_calc_w,
			ie_regra_apuracao_w,
			ie_origem_ctb_w,
			ds_origem_w);
open C01;
loop
fetch C01 into	
	nr_seq_formula_w,
	ie_participacao_calc_w,
	vl_fixo_w,
	nr_seq_ind_ref_w;
exit when C01%notfound;
	begin
	select	bsc_regra_formula_seq.nextval
	into	nr_seq_regra_form_w
	from	dual;

	insert into bsc_regra_formula (	nr_sequencia,
				nr_seq_regra_calc,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_participacao_calc,
				nr_seq_ind_ref,
				vl_fixo)
			values (	nr_seq_regra_form_w,
				nr_seq_formula_new_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ie_participacao_calc_w,
				nr_seq_ind_ref_w,
				vl_fixo_w);
	open C02;
	loop
	fetch C02 into
		nr_seq_calc_w,
		nr_seq_result_w,
		vl_maximo_w,
		vl_minimo_w,
		vl_resultado_w;
	exit when C02%notfound;
		begin
		insert into bsc_regra_form_item(	nr_sequencia,
						nr_seq_formula,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_calc,
						nr_seq_result,
						vl_minimo,
						vl_maximo,
						vl_resultado)
					values (	bsc_regra_form_item_seq.nextval,
						nr_seq_regra_form_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_calc_w,
						nr_seq_result_w,
						vl_minimo_w,
						vl_maximo_w,
						vl_resultado_w);
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

open C03;
loop
fetch C03 into	
	ds_origem_ww,
	ds_titulo_w,
	ie_estab_inf_w,
	ie_origem_ctb_w,
	nr_seq_mes_ref_w,
	qt_mes_w;
exit when C03%notfound;
	begin
	insert into bsc_regra_ctb_esp (nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_origem_ctb,
				ds_origem,
				nr_seq_regra_calc,
				qt_mes,
				ds_titulo,
				nr_seq_mes_ref,
				ie_estab_inf)
			values(	bsc_regra_ctb_esp_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ie_origem_ctb_w,
				ds_origem_ww,
				nr_seq_formula_new_w,
				qt_mes_w,
				ds_titulo_w,
				nr_seq_mes_ref_w,
				ie_estab_inf_w);
	end;
end loop;
close C03;

commit;

end bsc_copiar_formula_regra;
/