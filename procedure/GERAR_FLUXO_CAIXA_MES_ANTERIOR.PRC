create or replace procedure GERAR_FLUXO_CAIXA_MES_ANTERIOR
		(cd_estabelecimento_p	in	number,
		cd_conta_financ_p	in	number,
		ie_regra_data_p		in	varchar2,
		nr_dia_fixo_p		in	number,
		qt_mes_anterior_p	in	number,
		dt_inicial_p		in	date,
		dt_final_p		in	date,
		IE_ACUMULAR_FLUXO_p	in	varchar2,
		nm_usuario_p		in	varchar2,
		cd_empresa_p		in	number,
		nr_seq_regra_p		in	number) is

/*--------------------------------------------------------------- ATEN��O ----------------------------------------------------------------*/
/* Cuidado ao realizar altera��es no fluxo de caixa. Toda e qualquer altera��o realizada em qualquer uma das       */
/* procedures do fluxo de caixa deve ser cuidadosamente verificada e realizada no fluxo de caixa em lote.           */
/* Devemos garantir que os dois fluxos de caixa tragam os mesmos valores no resultado, evitando assim que           */
/* existam diferen�as entre os fluxos de caixa.                                                                                                                */
/*--------------- AO ALTERAR O FLUXO DE CAIXA ALTERAR TAMB�M O FLUXO DE CAIXA EM LOTE ---------------*/

vl_fluxo_w		number(15,2);
vl_deducao_w		number(15,2);
dt_referencia_w		date;
dt_mes_w		date;
ie_tratar_fim_semana_w	varchar2(2);
cd_moeda_empresa_w	number(5);
cd_moeda_w		number(5);

cursor c02 is
select	sum(vl_fluxo),
	PKG_DATE_UTILS.ADD_MONTH(dt_referencia,QT_MES_ANTERIOR_p,0),
	cd_moeda
from	fluxo_caixa
where	cd_estabelecimento	= cd_estabelecimento_p
and	cd_conta_financ		= cd_conta_financ_p
and	PKG_DATE_UTILS.start_of(dt_referencia, 'DD',0) between 	PKG_DATE_UTILS.ADD_MONTH(dt_inicial_p, -1 * QT_MES_ANTERIOR_p,0) and 
						PKG_DATE_UTILS.ADD_MONTH(dt_final_p, -1 * QT_MES_ANTERIOR_p,0)
and	ie_periodo		= 'D'
and	ie_classif_fluxo	= 'P'
group	by PKG_DATE_UTILS.ADD_MONTH(dt_referencia,QT_MES_ANTERIOR_p,0), cd_moeda;


begin

select	ie_tratar_fim_semana
into	ie_tratar_fim_semana_w
from	parametro_fluxo_caixa
where	cd_estabelecimento	= cd_estabelecimento_p;

/* Projeto Multimoeda - Busca a moeda padr�o da empresa para gravar no fluxo. */
select	obter_moeda_padrao_empresa(cd_estabelecimento_p,'E')
into	cd_moeda_empresa_w
from 	dual;

if	(ie_regra_data_p = 'F') then

	dt_mes_w	:= PKG_DATE_UTILS.start_of(dt_inicial_p,'MONTH',0);

	select	nvl(sum(vl_fluxo),0),
		max(cd_moeda)
	into	vl_fluxo_w,
		cd_moeda_w
	from	fluxo_caixa
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_periodo		= 'D'
	and	ie_classif_fluxo	= 'P'
	and	cd_conta_financ		= cd_conta_financ_p
	and	PKG_DATE_UTILS.start_of(dt_referencia, 'MONTH',0) = PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_inicial_p,-1 * QT_MES_ANTERIOR_p,0), 'MONTH', 0);

	while	(dt_mes_w <= PKG_DATE_UTILS.start_of(dt_final_p, 'MONTH',0)) loop
		
		if	(nr_dia_fixo_p > to_number(PKG_DATE_UTILS.extract_field('DAY',PKG_DATE_UTILS.end_of(dt_mes_w,'MONTH',0),0))) then
			dt_referencia_w		:= pkg_date_utils.get_datetime(PKG_DATE_UTILS.end_of(dt_mes_w,'MONTH',0), NVL(dt_mes_w, PKG_DATE_UTILS.GET_TIME('00:00:00')));
		elsif	(nr_dia_fixo_p = 0) then
			dt_referencia_w		:= pkg_date_utils.get_date(1,dt_mes_w,0);
		else
			dt_referencia_w		:= pkg_date_utils.get_date(nr_dia_fixo_p, dt_mes_w, 0);
		end if;

		if	(dt_referencia_w between dt_inicial_p and dt_final_p) then

			if	(ie_tratar_fim_semana_w = 'S') then
				dt_referencia_w			:= obter_proximo_dia_util(cd_estabelecimento_p, dt_referencia_w);
			end if;

			if	(OBTER_SE_FLUXO_ESP(cd_estabelecimento_p, cd_conta_financ_p, dt_referencia_w) = 'S') and
				(obter_se_fluxo_caixa_regra(nr_seq_regra_p, dt_referencia_w, cd_estabelecimento_p, 'E') = 'S') then
				begin
				vl_deducao_w		:= nvl(obter_se_fluxo_caixa_regra(nr_seq_regra_p, dt_referencia_w, cd_estabelecimento_p, 'VL'), 0);
				insert into fluxo_caixa
					(cd_estabelecimento,
					dt_referencia, 
					cd_conta_financ, 
					ie_classif_fluxo, 
					dt_atualizacao, 
					nm_usuario, 
					vl_fluxo, 
					ie_origem, 
					ie_periodo, 
					ie_integracao,
					cd_empresa,
					cd_moeda)
				values	(cd_estabelecimento_p, 
					dt_referencia_w,
					cd_conta_financ_p,
					'R',
					sysdate,
					nm_usuario_p,
					vl_fluxo_w - vl_deducao_w,
					'I',
					'D', 
					'RE',
					cd_empresa_p,
					nvl(cd_moeda_w,cd_moeda_empresa_w));	
				exception
					when dup_val_on_index then
						update	fluxo_caixa 
						set	vl_fluxo		= decode(IE_ACUMULAR_FLUXO_p, 'S', vl_fluxo_w + vl_fluxo, vl_fluxo_w) - vl_deducao_w
						where	cd_estabelecimento	= cd_estabelecimento_p
						and	cd_conta_financ	= cd_conta_financ_p
						and	dt_referencia		= dt_referencia_w
						and	ie_periodo		= 'D'
						and	ie_classif_fluxo	= 'R'
						and	ie_integracao		= 'RE'
						and	cd_empresa		= cd_empresa_p;
				end;
			end if;
		end if;

		dt_mes_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mes_w, 1,0);

		end loop;

elsif	(ie_regra_data_p = 'MA') then

	open c02;
	loop
	fetch c02 into
		vl_fluxo_w,
		dt_referencia_w,
		cd_moeda_w;
	exit when c02%notfound;

		if	(ie_tratar_fim_semana_w = 'S') then
			dt_referencia_w			:= obter_proximo_dia_util(cd_estabelecimento_p, dt_referencia_w);
		end if;

		if	(OBTER_SE_FLUXO_ESP(cd_estabelecimento_p, cd_conta_financ_p, dt_referencia_w) = 'S') then
			begin
			vl_deducao_w		:= nvl(obter_se_fluxo_caixa_regra(nr_seq_regra_p, dt_referencia_w, cd_estabelecimento_p, 'VL'), 0);
			insert into fluxo_caixa
				(cd_estabelecimento,
				dt_referencia, 
				cd_conta_financ, 
				ie_classif_fluxo, 
				dt_atualizacao, 
				nm_usuario, 
				vl_fluxo, 
				ie_origem, 
				ie_periodo, 
				ie_integracao,
				cd_empresa,
				cd_moeda)
			values	(cd_estabelecimento_p, 
				dt_referencia_w,
				cd_conta_financ_p,
				'R',
				sysdate,
				nm_usuario_p,
				vl_fluxo_w - vl_deducao_w,
				'I',
				'D', 
				'RE',
				cd_empresa_p,
				nvl(cd_moeda_w,cd_moeda_empresa_w));		
			exception
				when dup_val_on_index then
					update	fluxo_caixa 
					set	vl_fluxo		= decode(IE_ACUMULAR_FLUXO_p, 'S', vl_fluxo_w + vl_fluxo, vl_fluxo_w) - vl_deducao_w
					where	cd_estabelecimento	= cd_estabelecimento_p
					and	cd_conta_financ	= cd_conta_financ_p
					and	dt_referencia		= dt_referencia_w
					and	ie_periodo		= 'D'
					and	ie_classif_fluxo	= 'R'
					and	ie_integracao		= 'RE'
					and	cd_empresa		= cd_empresa_p;
			end;
		end if;
	end loop;
	close c02;

end if;

end GERAR_FLUXO_CAIXA_MES_ANTERIOR;
/