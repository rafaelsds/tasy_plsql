create or replace
procedure man_marcar_os_escala_email_sup
			(	nr_seq_ordem_p		number,
				nm_usuario_p		Varchar2) is 

begin
if	(nr_seq_ordem_p is not null) then
	update	man_ordem_servico
	set	ie_escala_email_sup	= 'S'
	where	nr_sequencia		= nr_seq_ordem_p;
end if;

commit;

end man_marcar_os_escala_email_sup;
/