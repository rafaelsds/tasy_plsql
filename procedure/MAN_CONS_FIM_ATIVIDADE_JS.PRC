create or replace
procedure man_cons_fim_atividade_js(	nr_seq_ordem_p			number,
					qt_projeto_p		number,
					ds_atividade_p		varchar2,
					ds_observacao_p		varchar2,
					dt_fim_atividade_p		date,
					nm_usuario_p		varchar2,
					pr_realizacao_os_p		number,
					ie_finalizar_ativ_prev_p	varchar2,
					ds_retorno_p		out	varchar2,
					ds_aviso_p		out	varchar2
					) is

nr_seq_proj_cron_etapa_p	number(10);
dt_inicio_atividade_w		date;
nr_seq_atividade_w		number(10);
nr_seq_proj_cron_etapa_w	number(10);

begin

select	max(nr_sequencia),
	max(nr_seq_proj_cron_etapa)
into	nr_seq_atividade_w,
	nr_seq_proj_cron_etapa_w
from	man_ordem_serv_ativ
where	nr_seq_ordem_serv	= nr_seq_ordem_p
and	nm_usuario_exec		= nm_usuario_p;

if	(nr_seq_atividade_w is not null)
	and (nr_seq_proj_cron_etapa_w is not null)
	and (qt_projeto_p < 0 or qt_projeto_p > 100) then
		ds_retorno_p := obter_texto_tasy(117217, wheb_usuario_pck.get_nr_seq_idioma);
		goto final;
end if;

man_finalizar_ativ_iniciada(nr_seq_ordem_p, ds_atividade_p, ds_observacao_p, dt_fim_atividade_p, nm_usuario_p, '', pr_realizacao_os_p, ie_finalizar_ativ_prev_p, ds_aviso_p, nr_seq_atividade_w, nr_seq_proj_cron_etapa_w);

atualizar_realizacao_ativ_proj (nr_seq_proj_cron_etapa_w,qt_projeto_p,nm_usuario_p);

man_ajustar_estagio_OS(nr_seq_ordem_p, null);

<<final>>
commit;

end man_cons_fim_atividade_js;
/