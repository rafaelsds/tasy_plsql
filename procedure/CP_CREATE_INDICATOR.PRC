create or replace procedure cp_create_indicator (
	nr_seq_goal_p		cp_goal_indicator.nr_seq_goal%type,
	ds_unique_value_p	cp_indicator.ds_unique_value%type,
	ds_display_name_p	cp_indicator.ds_display_name%type,
	ds_version_p		cp_indicator.ds_version%type,
	ie_data_type_p		cp_indicator.ie_data_type%type,
	ie_language_p		cp_indicator.ie_language%type,
	ie_active_p		cp_indicator.ie_active%type,
	ie_progressive_p	cp_indicator.ie_progressive%type,
	nm_usuario_p		cp_indicator.nm_usuario%type,
	nr_sequencia_p		out cp_indicator.nr_sequencia%type,
	nr_seq_goal_indic_p	out cp_goal_indicator.nr_sequencia%type
) is

ds_version_w			cp_indicator.ds_version%type;
ds_display_name_w		cp_indicator.ds_display_name%type;
nr_sequencia_w			cp_indicator.nr_sequencia%type;
si_record_changed_w		cp_indicator.si_record_changed%type;

begin

begin
	select	nr_sequencia,
			ds_version,
			ds_display_name
	into	nr_sequencia_w,
			ds_version_w,
			ds_display_name_w
	from	cp_indicator
	where	nr_sequencia = (
		select	max(nr_sequencia)
		from	cp_indicator
		where	ds_unique_value = ds_unique_value_p
	);
exception
	when no_data_found then
	begin
		nr_sequencia_w		:= null;
		ds_version_w		:= null;
		ds_display_name_w	:= null;
	end;
end;

select	decode(count(1), 0, 'N', 'Y')
into	si_record_changed_w
from	cp_indicator
where	ds_version is not null
and		ds_version <> ds_version_p;

if (ds_version_w is not null) and (si_record_changed_w = 'Y') then

	select	decode(count(1), 0, 'Y', 'N')
	into	si_record_changed_w
	from	cp_indicator
	where	ds_unique_value = ds_unique_value_p;
	
	if (si_record_changed_w = 'N') then
		select	decode(ds_display_name_p, nvl(ds_display_name_w, ds_display_name_p), 'N', 'Y')
		into	si_record_changed_w
		from 	dual;
	end if;

end if;

if (nr_sequencia_p is null or ds_version_w <> ds_version_p) then
	select	cp_indicator_seq.nextval
	into	nr_sequencia_p
	from	dual;

	insert into cp_indicator (
		nr_sequencia,
		ds_unique_value,
		ds_display_name,
		ds_version,
		ie_data_type,
		ie_language,
		ie_active,
		ie_origin,
		ie_progressive,
		si_import_status,
		ie_situacao,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		si_record_changed
	) values (
		nr_sequencia_p,
		ds_unique_value_p,
		ds_display_name_p,
		ds_version_p,
		ie_data_type_p,
		ie_language_p,
		ie_active_p,
		'E', -- From Elsevier
		ie_progressive_p,
		'SP',
		'I',
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nvl(si_record_changed_w, 'N')
	);
	
	update	cp_indicator
	set		si_record_changed = 'N'
	where 	ds_version = ds_version_w;
	
end if;

if (nr_seq_goal_p is not null) then
	begin
		select	nr_sequencia
		into	nr_seq_goal_indic_p
		from	cp_goal_indicator
		where	nr_seq_goal = nr_seq_goal_p
			and nr_seq_indicator = nr_sequencia_p;
	exception
		when no_data_found then
			nr_seq_goal_indic_p	:= null;
	end;
	
	if (si_record_changed_w = 'N') then
		
		select 	decode(count(1), 0, 'Y', 'N')
		into	si_record_changed_w
		from 	cp_goal_indicator a,
				cp_indicator b
		where	b.nr_sequencia = a.nr_seq_indicator
		and		b.nr_sequencia = (	select 	max(nr_sequencia) 
									from 	cp_indicator 
									where 	ds_unique_value = ds_unique_value_p 
									and 	nr_sequencia < nr_sequencia_p)
		and		a.nr_seq_goal = nr_seq_goal_p;
		
	end if;
	
	if (nr_seq_goal_indic_p is null) then
		select	cp_goal_indicator_seq.nextval
		into	nr_seq_goal_indic_p
		from	dual;

		insert into cp_goal_indicator (
			nr_sequencia,
			nr_seq_goal,
			nr_seq_indicator,
			ie_origin,
			si_import_status,
			ie_situacao,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			si_record_changed
		) values (
			nr_seq_goal_indic_p,
			nr_seq_goal_p,
			nr_sequencia_p,
			'E', -- From Elsevier
			'SP',
			'I',
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nvl(si_record_changed_w, 'N')
		);
		
		update	cp_goal_indicator
		set		si_record_changed = 'N'
		where 	nr_seq_indicator = nr_sequencia_w;
		
	end if;
end if;

end cp_create_indicator;
/