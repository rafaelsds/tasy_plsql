create or replace
procedure Atualizar_paciente_nao_apto( nr_seq_atendimento_p		number,
					ie_opcao_p		varchar2,
					nm_usuario_p		Varchar2) is 

begin

if 	( nr_seq_atendimento_p is not null) and
	(ie_opcao_p = 'NA')	then
	update	paciente_atendimento
	set	nm_usuario	=	nm_usuario_p,
		dt_nao_apto	=	sysdate
	where	nr_seq_atendimento	=	nr_seq_atendimento_p ;
elsif 	( nr_seq_atendimento_p is not null) and
	(ie_opcao_p = 'DNA')	then
	
	update	paciente_atendimento
	set	nm_usuario	=	nm_usuario_p,
		dt_nao_apto	=	null
	where	nr_seq_atendimento	=	nr_seq_atendimento_p ;	
end if;

commit;

end Atualizar_paciente_nao_apto;
/