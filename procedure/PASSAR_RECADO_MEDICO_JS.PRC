create or replace
procedure passar_recado_medico_js (
		nr_sequencia_p	number,
		nm_usuario_p	varchar2) is

begin
if 	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	recado_medico
	set	nm_usuario_recado = nm_usuario_p,
		dt_recado_passado = sysdate
	where	nr_sequencia = nr_sequencia_p;
	end;
end if;
commit;
end passar_recado_medico_js;
/