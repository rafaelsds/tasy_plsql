create or replace
procedure HTML5_Atual_Unid_Atend_Sit_EUP	(nr_seq_interno_p	number,
						ie_situacao_p		varchar2,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number,
						ie_verifica_clique_p	varchar2 default 'N') is

ie_atualiza_agrupamento_w	varchar2(1);
nr_agrupamento_w		number(5);
ds_erro_w			varchar2(255);
cd_setor_atendimento_w		number(5);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);

ie_situacao_w	unidade_atendimento.ie_situacao%type;

begin

ds_erro_w:='';

Obter_Param_Usuario(1,6,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_atualiza_agrupamento_w);

select 	nvl(max(nr_agrupamento),0),
	max(cd_setor_atendimento),
	max(cd_unidade_basica),
	max(cd_unidade_compl)
into	nr_agrupamento_w,
	cd_setor_atendimento_w,
	cd_unidade_basica_w,
	cd_unidade_compl_w
from	unidade_atendimento 
where	nr_seq_interno 	= nr_seq_interno_p;

if	(ie_situacao_p = 'I') then
	ie_situacao_w := 'A';
else
	ie_situacao_w := 'I';
end if;

if	(nr_agrupamento_w > 0) and
	(ie_atualiza_agrupamento_w = 'S') then

	 atualizar_situacao_leito(cd_setor_atendimento_w,cd_unidade_basica_w,cd_unidade_compl_w,ie_situacao_p,nm_usuario_p,ds_erro_w,ie_verifica_clique_p);

else
	update	unidade_atendimento 
	set	ie_situacao	= ie_situacao_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate 
	where	nr_seq_interno 	= nr_seq_interno_p;
end if;

if	(ds_erro_w is not null) then	
	Wheb_mensagem_pck.exibir_mensagem_abort(395321,'DS_MENSAGEM=' || ds_erro_w);	
end if; 

if 	(nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then 
	commit; 
end if;

end HTML5_Atual_Unid_Atend_Sit_EUP;
/
