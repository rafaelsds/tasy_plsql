create or replace
procedure lab_desaprovar_resultado_bco (nr_prescricao_p		NUMBER,
				nr_seq_prescr_p		NUMBER,
				nm_usuario_p		VARCHAR2,
				ds_erro_p 	   out	varchar2) is 

										
nr_seq_resultado_w		exame_lab_resultado.nr_seq_resultado%type;
nr_seq_result_item_w		exame_lab_result_item.nr_sequencia%type;
nr_seq_result_w			exame_lab_result_item.nr_sequencia%type;
nr_sequencia_result_w		result_laboratorio.nr_sequencia%type;
nr_prescricao_result_w		result_laboratorio.nr_prescricao%type;
nr_seq_prescricao_result_w	result_laboratorio.nr_seq_prescricao%type;
nm_usuario_result_w		result_laboratorio.nm_usuario%type;
dt_atualizacao_result_w		result_laboratorio.dt_atualizacao%type;
ie_cobranca_result_w		result_laboratorio.ie_cobranca%type;
ie_origem_proced_result_w	result_laboratorio.ie_origem_proced%type;
cd_procedimento_result_w	result_laboratorio.cd_procedimento%type;
ds_resultado_result_w		result_laboratorio.ds_resultado%type;
dt_coleta_result_w		result_laboratorio.dt_coleta%type;
ie_status_conversao_result_w	result_laboratorio.ie_status_conversao%type;
ds_result_codigo_result_w	result_laboratorio.ds_result_codigo%type;
ie_formato_texto_result_w	result_laboratorio.ie_formato_texto%type;
nr_seq_exame_result_w		result_laboratorio.nr_seq_exame%type;
nr_seq_pagina_result_w		result_laboratorio.nr_seq_pagina%type;
ds_erro_w			varchar2(255) := null;
ie_existe_prescr_w		number(10);


cursor c01 is
	select 	nr_sequencia
	from 	exame_lab_result_item
	where 	nr_seq_resultado = nr_seq_resultado_w
	and 	nr_seq_prescr	 = nr_seq_prescr_p;

cursor c02 is
 	select	nr_sequencia,
		nr_prescricao,
		nr_seq_prescricao,
		nm_usuario,
		dt_atualizacao,
		ie_cobranca,
		ie_origem_proced,
		cd_procedimento,
		ds_resultado,
		dt_coleta,
		ie_status_conversao,
		ds_result_codigo,
		ie_formato_texto,
		nr_seq_exame,
		nr_seq_pagina
 	from	result_laboratorio
 	where	nr_prescricao 		= nr_prescricao_p
	and	nr_seq_prescricao 	= nr_seq_prescr_p;										
										
begin

	begin
	select	MAX(nr_seq_resultado)
	into	nr_seq_resultado_w
	from	exame_lab_resultado
	where	nr_prescricao = nr_prescricao_p;

	select	count(*)
	into	ie_existe_prescr_w
	from	prescr_procedimento
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_seq_prescr_p;
	
	if (ie_existe_prescr_w = 0) then
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279797,null);
	else
		open c02;
		loop
		fetch c02 into
			nr_sequencia_result_w,
			nr_prescricao_result_w,
			nr_seq_prescricao_result_w,
			nm_usuario_result_w,
			dt_atualizacao_result_w,
			ie_cobranca_result_w,
			ie_origem_proced_result_w,
			cd_procedimento_result_w,
			ds_resultado_result_w,
			dt_coleta_result_w,
			ie_status_conversao_result_w,
			ds_result_codigo_result_w,
			ie_formato_texto_result_w,
			nr_seq_exame_result_w,
			nr_seq_pagina_result_w;
		exit when c02%notfound;
			begin


			insert into result_laboratorio_copia (	nr_sequencia,
								nr_prescricao,
								nr_seq_prescricao,
								nm_usuario,
								dt_atualizacao,
								ie_cobranca,
								ie_origem_proced,
								cd_procedimento,
								ds_resultado,
								ie_formato_texto,
								dt_coleta,
								nr_seq_result_ant,
								ie_status_conversao,
								ds_result_codigo,
								nr_seq_pagina,
								nr_seq_exame,
								dt_desaprovacao)
			values(					result_laboratorio_copia_seq.nextval,
								nr_prescricao_result_w,
								nr_seq_prescricao_result_w,
								nm_usuario_p,
								sysdate,
								ie_cobranca_result_w,
								ie_origem_proced_result_w,
								cd_procedimento_result_w,
								ds_resultado_result_w,
								ie_formato_texto_result_w,
								dt_coleta_result_w,
								nr_sequencia_result_w,
								ie_status_conversao_result_w,
								ds_result_codigo_result_w,
								nr_seq_pagina_result_w,
								nr_seq_exame_result_w,
								sysdate);

			end;
		end loop;
		close c02;

		update	prescr_procedimento
		set	ie_status_atend = 30,
			cd_motivo_baixa = 0,
			nm_usuario  = nm_usuario_p
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia  = nr_seq_prescr_p;

		update 	exame_lab_result_item
		set 	dt_aprovacao 	= null,
			cd_medico_resp 	= null
		where   nr_seq_resultado= nr_seq_resultado_w
		and 	nr_seq_prescr	= nr_seq_prescr_p;

		delete 	result_laboratorio
		where 	nr_prescricao	  = nr_prescricao_p
		and 	nr_seq_prescricao = nr_seq_prescr_p;

		delete 	w_exame_lab_result_item
		where 	nr_seq_resultado= nr_seq_resultado_w
		and 	nr_seq_prescr	= nr_seq_prescr_p;

		open c01;
		loop
		fetch c01 into
			nr_seq_result_item_w;
		exit when c01%notfound;
			begin
			select 	nvl(max(nr_sequencia),0)+1
			into	nr_seq_result_w
			from	w_exame_lab_result_item
			where 	nr_seq_resultado	= nr_seq_resultado_w;

			insert into w_exame_lab_result_item (
					nr_seq_resultado,
					nr_sequencia,
					nr_seq_exame,
					dt_atualizacao,
					nm_usuario,
					qt_resultado,
					ds_resultado,
					nr_seq_metodo,
					nr_seq_material,
					pr_resultado,
					ie_status,
					dt_aprovacao,
					nm_usuario_aprovacao,
					nr_seq_prescr,
					pr_minimo,
					pr_maximo,
					qt_minima,
					qt_maxima,
					ds_observacao,
					ds_referencia,
					ds_unidade_medida,
					qt_decimais,
					ds_regra,
					dt_coleta,
					nr_seq_reagente,
					nr_seq_formato,
					cd_medico_resp,
					dt_impressao,
					nr_seq_unid_med,
					cd_equipamento)
			select 	nr_seq_resultado,
					nr_seq_result_w,
					nr_seq_exame,
					sysdate,
					nm_usuario,
					qt_resultado,
					ds_resultado,
					nr_seq_metodo,
					nr_seq_material,
					pr_resultado,
					ie_status,
					dt_aprovacao,
					nm_usuario_aprovacao,
					nr_seq_prescr,
					pr_minimo,
					pr_maximo,
					qt_minima,
					qt_maxima,
					ds_observacao,
					ds_referencia,
					ds_unidade_medida,
					qt_decimais,
					ds_regra,
					dt_coleta,
					nr_seq_reagente,
					nr_seq_formato,
					cd_medico_resp,
					dt_impressao,
					nr_seq_unid_med,
					cd_equipamento
			from 	exame_lab_result_item
			where 	nr_seq_resultado= nr_seq_resultado_w
			and 	nr_sequencia	= nr_seq_result_item_w
			and 	nr_seq_prescr	= nr_seq_prescr_p;

			delete 	exame_lab_result_item
			where 	nr_seq_resultado = nr_seq_resultado_w
			and	nr_sequencia 	= nr_seq_result_item_w
			and 	nr_seq_prescr	= nr_seq_prescr_p;
		  
			end;
		end loop;
		close c01;
		
	
	end if;
	
	exception
	when others then
		ds_erro_w := ds_erro_w||CHR(10)||CHR(13)||WHEB_MENSAGEM_PCK.get_texto(279713,null)|| substr(sqlerrm,1,240);
	end;
ds_erro_p := ds_erro_w;
commit;

end lab_desaprovar_resultado_bco;
/