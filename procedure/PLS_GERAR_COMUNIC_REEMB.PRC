create or replace 
procedure  pls_gerar_comunic_reemb
			(	ie_tipo_regra_p			pls_reemb_comunic_interna.ie_tipo_regra%type,
				ds_conteudo_p			varchar2,
				nr_seq_protocolo_p		pls_protocolo_conta.nr_sequencia%type,
				nm_usuario_p			usuario.nm_usuario%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is 


nm_usuario_comunic_w	pls_reemb_comunic_interna.nm_usuario_comunic%type;	
cd_perfil_w				pls_reemb_comunic_interna.cd_perfil%type;
ds_comunicado_w			pls_reemb_comunic_interna.ds_comunicado%type;
ds_titulo_w				pls_reemb_comunic_interna.ds_titulo%type;
ds_perfil_w				varchar2(20) := null;
ie_status_protocolo_w	pls_protocolo_conta.ie_status%type;
ie_status_prot_regra_w	pls_protocolo_conta.ie_status%type;
qtde_regras_status_w	pls_integer;
	
Cursor C01 is
	select	nm_usuario_comunic,
			cd_perfil,
			ds_comunicado,
			ds_titulo,
			ie_status_protocolo
	from	pls_reemb_comunic_interna
	where	(ie_tipo_regra = ie_tipo_regra_p)
	and		ie_status_protocolo is null
	and	ie_situacao	= 'A'
	order by 1;

--Caso a regra possuir restri��o por status do protocolo
--que coincida com o status do protocolo em quest�o, 
--ent�o executar� as regras obtidas nesse cursor.	
Cursor C02 is
	select	nm_usuario_comunic,
			cd_perfil,
			ds_comunicado,
			ds_titulo,
			ie_status_protocolo
	from	pls_reemb_comunic_interna
	where	(ie_tipo_regra = ie_tipo_regra_p)
	and		ie_status_protocolo = ie_status_protocolo_w
	and		ie_situacao	= 'A'
	order by 1;

begin

select	ie_status
into	ie_status_protocolo_w
from	pls_protocolo_conta
where 	nr_sequencia = nr_seq_protocolo_p;

select	count(1)
into	qtde_regras_status_w
from	pls_reemb_comunic_interna a
where 	a.ie_status_protocolo = ie_status_protocolo_w
and		(ie_tipo_regra = ie_tipo_regra_p)
and		a.ie_situacao = 'A';


--Se tiver regras com restri��o por status do protocolo que coincidam
--com o status do protocolo em quest�o, ent�o somente enviar� comunica��o para esses

if	(qtde_regras_status_w > 0 ) then	
	open C02;
	loop
	fetch C02 into	
		nm_usuario_comunic_w,
		cd_perfil_w,
		ds_comunicado_w,
		ds_titulo_w,
		ie_status_prot_regra_w;
	exit when C02%notfound;
		begin	
		if	(cd_perfil_w is not null) then
			ds_perfil_w := To_char(cd_perfil_w) || ',';
		end if;
		
			gerar_comunic_padrao
				(sysdate, ds_titulo_w, ds_comunicado_w || chr(13) || chr(10) || ' ' || ds_conteudo_p,
				nm_usuario_p, 'N', nm_usuario_comunic_w,
				'N', null, ds_perfil_w,	cd_estabelecimento_p,
				null, sysdate, null,
				null);
		end;
	end loop;
	close C02;

--Se n�o houver nenhuma regra cuja restri��o por status do protocolo
--for igual ao status do protocolo em quest�o, ent�o envia comunicado 
--quando houver regras sem essa restri��o
else

	open C01;
	loop
	fetch C01 into	
		nm_usuario_comunic_w,
		cd_perfil_w,
		ds_comunicado_w,
		ds_titulo_w,
		ie_status_prot_regra_w;
	exit when C01%notfound;
		begin	
		if	(cd_perfil_w is not null) then
			ds_perfil_w := To_char(cd_perfil_w) || ',';
		end if;
				
			gerar_comunic_padrao
				(sysdate, ds_titulo_w, ds_comunicado_w || chr(13) || chr(10) || ' ' || ds_conteudo_p,
				nm_usuario_p, 'N', nm_usuario_comunic_w,
				'N', null, ds_perfil_w,	cd_estabelecimento_p,
				null, sysdate, null,
				null);
		end;
	end loop;
	close C01;
	
end if;
	
--commit;
 
end pls_gerar_comunic_reemb;
/
