create or replace
procedure reverter_dispositivo(	nr_seq_disp_pac_p	number,
				nr_seq_reversao_p	number,
				nm_usuario_p		varchar2) is 

nr_sequencia_w		number(10,0);
nr_seq_dispositivo_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
ie_acao_w		varchar2(15);
				
begin

if	(nr_seq_disp_pac_p > 0) then
	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	atend_pac_disp_proc
	where	nr_seq_disp_pac		=	nr_seq_disp_pac_p
	and	nr_seq_reversao is null;
end if;

if	(nr_sequencia_w > 0) then
	select	a.nr_seq_dispositivo,
		b.nr_seq_proc_interno
	into	nr_seq_dispositivo_w,
		nr_seq_proc_interno_w
	from	atend_pac_dispositivo a,
		atend_pac_disp_proc b
	where	a.nr_sequencia	=	b.nr_seq_disp_pac
	and	b.nr_sequencia	=	nr_sequencia_w;
	
	select	max(ie_acao)
	into	ie_acao_w
	from	dispositivo_proc_interno
	where	nr_seq_proc_interno	=	nr_seq_proc_interno_w
	and	nr_seq_dispositivo	=	nr_seq_dispositivo_w;
	
	if	(ie_acao_w = 'R') then
		update	atend_pac_dispositivo
		set	dt_retirada	=	null,
				dt_atualizacao		=	sysdate,
		nm_usuario			=	nm_usuario_p
		where	nr_sequencia	=	nr_seq_disp_pac_p;
	end if;
	
	update	atend_pac_disp_proc
	set	nr_seq_reversao		=	nr_seq_reversao_p,
			dt_atualizacao		=	sysdate,
		nm_usuario			=	nm_usuario_p
	where	nr_sequencia		=	nr_sequencia_w;
end if;

commit;

end reverter_dispositivo;
/