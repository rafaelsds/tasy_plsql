create or replace
procedure pls_excluir_reaj_copartic
			( nr_seq_reajuste_p	number) is 

Cursor C01 is
	select	nr_sequencia
	from	pls_lote_reaj_copartic
	where	nr_seq_reajuste = nr_seq_reajuste_p
	and	dt_liberacao is null;

nr_seq_lote_w	number(10);

begin

open C01;
loop
fetch C01 into	
	nr_seq_lote_w;
exit when C01%notfound;
	begin
	delete	from	pls_reajuste_copartic
	where	nr_seq_lote = nr_seq_lote_w;
	
	delete	from	pls_lote_reaj_copartic
	where	nr_sequencia = nr_seq_lote_w;
	end;
end loop;
close C01;

commit;

end pls_excluir_reaj_copartic;
/