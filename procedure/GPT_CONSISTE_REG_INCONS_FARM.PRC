create or replace
procedure gpt_consiste_reg_incons_farm(nr_seq_cpoe_p	number,
											nm_tabela_p		varchar2,
											nm_usuario_p		varchar2,
											cd_estabelecimento_p	number,
											ds_erro_p 		out 	varchar2) is 
						
ds_erro_w		varchar2(255):=null;

Cursor c01(nr_seq_cpoe_pc	number,
			nm_tabela_pc	varchar2) is
	select	a.nr_prescricao,
			a.nr_sequencia
	from	prescr_material a
	where	a.nr_seq_mat_cpoe = nr_seq_cpoe_p
	and		nm_tabela_p	 = 'CPOE_MATERIAL';

begin

for r_c01 in c01(nr_seq_cpoe_p,nm_tabela_p) loop
		consiste_registro_incons_farm(r_c01.nr_prescricao,
									r_c01.nr_sequencia,
									nm_usuario_p,
									cd_estabelecimento_p,
									ds_erro_w);
									
		if	(ds_erro_w is not null) then
			exit;
		end if;
end loop;

ds_erro_p := ds_erro_w;

end gpt_consiste_reg_incons_farm;
/