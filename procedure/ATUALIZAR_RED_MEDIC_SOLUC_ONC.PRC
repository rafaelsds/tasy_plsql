create or replace
procedure atualizar_red_medic_soluc_onc (	nr_seq_paciente_p		number,
											nr_seq_material_p		number,			
											pr_reducao_p			number,
											ie_opcao_p				varchar2,
											ie_acao_p				varchar2,
											ds_retorno_p			out Varchar2) is
											
/*
			ie_opcao_p
Alterar Medicamento     	'M'
Alterar Solu��o		'S'

			ie_acao_p			
Consultar		'C'
Executar		'E'
*/

ds_retorno_w	varchar2(255) := '';
qt_reg_w		Number(10);
						
begin

if 	(nr_seq_paciente_p is not null) and
	(nr_seq_material_p is not null) then
   
	If	( ie_acao_p = 'C' ) then
	
		if	( ie_opcao_p = 'M' ) then
		
			Select 	count(*)
			into	qt_reg_w
			from   	paciente_protocolo_medic  
			where  	nr_seq_paciente = nr_seq_paciente_p
			and    	cd_material = nr_seq_material_p
			and    	nvl(ie_aplica_reducao,'S') = 'S'  
			and    	nr_seq_diluicao is Null
			and    	nr_seq_solucao is null
			and    	nr_seq_medic_material is null
			and    	nr_seq_procedimento is null
			and		pr_reducao <> pr_reducao_p;

			if	(qt_reg_w > 0) then			
				ds_retorno_w := obter_desc_expressao(782555)/*'Existe o mesmo medicamento na pasta medicamentos do protocolo com percentual de redu��o diferente.'*/ || obter_desc_expressao(782563); --'Deseja realizar a altera��o do percentual nessa pasta?';				
			end if;			
		
		else
		
			Select 	count(*)
			into	qt_reg_w
			from   	paciente_protocolo_medic  
			where  	nr_seq_paciente = nr_seq_paciente_p
			and    	cd_material = nr_seq_material_p
			and    	nvl(ie_aplica_reducao,'S') = 'S'  
			and		nvl(ie_zerado,'N') = 'N'
			and    	nr_seq_solucao is not null
			and		pr_reducao <> pr_reducao_p;
			
			if	(qt_reg_w > 0) then			
				ds_retorno_w := obter_desc_expressao(782555)/*'Existe o mesmo medicamento na pasta solu��es do protocolo com percentual de redu��o diferente.'*/ ||obter_desc_expressao(782557); --'Deseja realizar a altera��o do percentual na solu��o?';				
			end if;				
		
		end if;
		
	
	end if;
	
	If	( ie_acao_p = 'E' ) and
		( pr_reducao_p is not null) and
		( pr_reducao_p > 0)	then
		
		if	( ie_opcao_p = 'M' ) then
		
			update	paciente_protocolo_medic
			set		qt_dose_prescr	= (qt_dose_prescr - ((pr_reducao_p * nvl(qt_dose_prescr,0)) / 100)),
					pr_reducao = pr_reducao_p
			where  	nr_seq_paciente = nr_seq_paciente_p
			and    	cd_material = nr_seq_material_p
			and    	nvl(ie_aplica_reducao,'S') = 'S'  
			and    	nr_seq_diluicao is Null
			and    	nr_seq_solucao is null
			and    	nr_seq_medic_material is null
			and    	nr_seq_procedimento is null;			
		
		else
		
			update	paciente_protocolo_medic
			set		qt_dose_prescr	= (qt_dose_prescr - ((pr_reducao_p * nvl(qt_dose_prescr,0)) / 100)),
					pr_reducao = pr_reducao_p  
			where  	nr_seq_paciente = nr_seq_paciente_p
			and    	cd_material = nr_seq_material_p
			and    	nvl(ie_aplica_reducao,'S') = 'S'  
			and		nvl(ie_zerado,'N') = 'N'
			and    	nr_seq_solucao is not null;						
		
		end if;		
	
		commit;
	
	end if;
end if;

ds_retorno_p := ds_retorno_w;

end atualizar_red_medic_soluc_onc;
/