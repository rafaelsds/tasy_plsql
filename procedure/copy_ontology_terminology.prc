create or replace procedure copy_ontology_terminology(
        NM_TABELA_P varchar2, NM_ATRIBUTO_P varchar2, COPY_NM_TABELA_P varchar2, 
        COPY_NM_ATRIBUTO_P varchar2, CD_VALOR_TASY_P varchar2, IE_ONTOLOGIA_P varchar2) is

nm_usuario_w                                      RES_CADASTRO_ONTOLOGIA_PHI.nm_usuario%type;
cd_valor_ontologia_w                              RES_CADASTRO_ONTOLOGIA_PHI.cd_valor_ontologia%type;
ds_documentacao_w                                 RES_CADASTRO_ONTOLOGIA_PHI.ds_documentacao%type;

cursor c01 is
select CD_VALOR_TASY,
       IE_ONTOLOGIA,
       CD_VALOR_ONTOLOGIA,
       DS_DOCUMENTACAO       
  from RES_CADASTRO_ONTOLOGIA_PHI
 where nm_tabela = COPY_NM_TABELA_P 
   and nm_atributo = COPY_NM_ATRIBUTO_P
   and ((CD_VALOR_TASY_P is null and cd_valor_tasy is null)
        or (CD_VALOR_TASY_P is not null and cd_valor_tasy = CD_VALOR_TASY_P))
   and ie_ontologia = IE_ONTOLOGIA_P;

begin
  if (NM_TABELA_P = COPY_NM_TABELA_P and NM_ATRIBUTO_P = COPY_NM_ATRIBUTO_P) then
    /* No copy if source and destination is same table-atribute */
    return;
  end if;

   nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
   
   for c01_w in c01 loop
   
     cd_valor_ontologia_w  := c01_w.cd_valor_ontologia;
     ds_documentacao_w     := c01_w.ds_documentacao;
   
    insert into RES_CADASTRO_ONTOLOGIA_PHI
      (nr_sequencia,
       dt_atualizacao,
       nm_usuario,
       dt_atualizacao_nrec,
       nm_usuario_nrec,
       cd_Valor_tasy,
       nm_tabela,
       nm_atributo,
       ie_ontologia,
       cd_valor_ontologia,
       ds_documentacao,
       ie_situacao
       )
    values
      (
       (select min(a.nr_sequencia + 1) from res_cadastro_ontologia_phi a 
            where not exists (select 1 from res_cadastro_ontologia_phi b where b.nr_sequencia = a.nr_sequencia + 1)),
       sysdate,
       nm_usuario_w,
       sysdate,
       nm_usuario_w,
       CD_VALOR_TASY_P,
       NM_TABELA_P,
       NM_ATRIBUTO_P,
       IE_ONTOLOGIA_P,
       cd_valor_ontologia_w,
       ds_documentacao_w,
       'A'
       );
     
     end loop;

end copy_ontology_terminology;
/
