CREATE OR REPLACE
PROCEDURE Duplicar_ganhos_perdas(
			nr_sequencia_p		Number,
			nm_usuario_p		Varchar2) IS 

nr_sequencia_w		number(10);
nr_seq_atend_pg_inf_w	number(15);
nr_seq_inf_adic_w	number(15);
nr_seq_inf_adic_item_w	number(15);
cd_pessoa_duplica_w usuario.cd_pessoa_fisica%type;

cursor c01 is
	select 	ATEND_PERDA_GANHO_INF_ADIC_seq.nextval,
		NR_SEQ_INF_ADIC,
		NR_SEQ_INF_ADIC_ITEM
	from ATEND_PERDA_GANHO_INF_ADIC
	where nr_seq_atend_pg = nr_sequencia_p;

BEGIN

select	ATENDIMENTO_PERDA_GANHO_seq.nextval
into	nr_sequencia_w
from	dual;

select max(cd_pessoa_fisica) 
into cd_pessoa_duplica_w
from usuario where upper(nm_usuario) = upper(nm_usuario_p);

insert into ATENDIMENTO_PERDA_GANHO(
	NR_SEQUENCIA,
	NR_ATENDIMENTO,
	DT_ATUALIZACAO,
	NM_USUARIO,
	NR_SEQ_TIPO,
	QT_VOLUME,
	DS_OBSERVACAO,
	DT_MEDIDA,
	CD_TURNO,
	CD_SETOR_ATENDIMENTO,
	NR_CIRURGIA,
	IE_ORIGEM,
	DT_REFERENCIA,
	CD_PROFISSIONAL,
	NR_HORA,
	IE_SITUACAO,
	DT_APAP,
	QT_OCORRENCIA,
	NR_SEQ_EVENTO_ADEP,
	NR_SEQ_PEPO,
	CD_PERFIL_ATIVO,
	IE_RECEM_NATO,
	QT_PESO,
	NR_SEQ_EVENTO_IVC,
	NR_SEQ_TOPOGRAFIA,
	IE_LADO,
	NR_SEQ_ASPECTO,
	IE_INTENSIDADE,
	NR_SEQ_HORARIO,
	DS_UTC,
	IE_HORARIO_VERAO,
	DS_UTC_ATUALIZACAO)
select  nr_sequencia_w,
	NR_ATENDIMENTO,
	sysdate,
	nm_usuario_p,
	NR_SEQ_TIPO,
	QT_VOLUME,
	DS_OBSERVACAO,
	sysdate,
	CD_TURNO,
	CD_SETOR_ATENDIMENTO,
	NR_CIRURGIA,
	IE_ORIGEM,
	DT_REFERENCIA,
	cd_pessoa_duplica_w,
	to_number(to_char(round(sysdate,'hh24'),'hh24')),
	'A',
	DT_APAP,
	QT_OCORRENCIA,
	NR_SEQ_EVENTO_ADEP,
	NR_SEQ_PEPO,
	CD_PERFIL_ATIVO,
	IE_RECEM_NATO,
	QT_PESO,
	NR_SEQ_EVENTO_IVC,
	NR_SEQ_TOPOGRAFIA,
	IE_LADO,
	NR_SEQ_ASPECTO,
	IE_INTENSIDADE,
	NR_SEQ_HORARIO,
	DS_UTC,
	IE_HORARIO_VERAO,
	DS_UTC_ATUALIZACAO 
	from ATENDIMENTO_PERDA_GANHO 
	where nr_sequencia = nr_sequencia_p;
	commit;
	
OPEN c01;
LOOP
FETCH c01 INTO
	nr_seq_atend_pg_inf_w,
	nr_seq_inf_adic_w,
	nr_seq_inf_adic_item_w;
EXIT WHEN c01%NOTFOUND;
BEGIN
	insert into ATEND_PERDA_GANHO_INF_ADIC(
		NR_SEQUENCIA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		NR_SEQ_ATEND_PG,
		NR_SEQ_INF_ADIC,
		NR_SEQ_INF_ADIC_ITEM
	) values (
		nr_seq_atend_pg_inf_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		nr_seq_inf_adic_w,
		nr_seq_inf_adic_item_w
	);
END;
END LOOP;
CLOSE c01;

commit;			

END Duplicar_ganhos_perdas;
/