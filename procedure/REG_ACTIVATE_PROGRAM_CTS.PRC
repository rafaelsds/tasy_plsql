create or replace procedure reg_activate_program_cts(
                                                     
                                                     nr_seq_reg_program_p number,
                                                     
                                                     nr_seq_caso_teste_p number,
                                                     
                                                     ds_reason_p varchar2,
                                                     
                                                     ie_opcao_p varchar2,
                                                     
                                                     nm_usuario_p varchar2) is

begin

  if (nr_seq_reg_program_p is not null and nr_seq_caso_teste_p is not null) then
  
    if (ie_opcao_p = 'URS') then
    
      update reg_program_urs_cts
      
         set ie_situacao = 'A',
             
             ds_justificativa_inativacao = null,
             
             ds_justificativa_ativacao = ds_reason_p,
             
             nm_usuario = nm_usuario_p,
             
             dt_atualizacao = sysdate
      
       where nr_seq_reg_program = nr_seq_reg_program_p
            
         and nr_seq_caso_teste = nr_seq_caso_teste_p;
    
    elsif (ie_opcao_p = 'PRS') then
    
      update reg_program_prs_cts
      
         set ie_situacao = 'I',
             
             ds_justificativa_inativacao = null,
             
             ds_justificativa_ativacao = ds_reason_p,
             
             nm_usuario = nm_usuario_p,
             
             dt_atualizacao = sysdate
      
       where nr_seq_reg_program = nr_seq_reg_program_p
            
         and nr_seq_caso_teste = nr_seq_caso_teste_p;
    
    end if;
  
    commit;
  
  end if;

end reg_activate_program_cts;
/
