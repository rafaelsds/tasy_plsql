create or replace
procedure VALIDAR_DADOS_PF
			(cd_estabelecimento_p	in number,
			cd_convenio_p		in number,
			cd_pessoa_fisica_p	in varchar2,
			ie_evento_p		in varchar2,
			ds_mensagem_p		out varchar2,
			ie_acao_mensagem_p	out varchar2,
			nr_seq_regra_p		out number) is


ie_tipo_convenio_w	convenio.ie_tipo_convenio%type;
nr_cpf_w		pessoa_fisica.nr_cpf%type;
nr_rg_w			pessoa_fisica.nr_identidade%type;
nr_passaporte_w		pessoa_fisica.nr_identidade%type;
nr_rne_w		pessoa_fisica.nr_reg_geral_estrang%type;
nr_ric_w		pessoa_fisica.nr_ric%type;
cd_nacionalidade_w	pessoa_fisica.cd_nacionalidade%type;
sg_estado_w		compl_pessoa_fisica.sg_estado%type;
ds_municipio_w		compl_pessoa_fisica.ds_municipio%type;
ds_bairro_w		compl_pessoa_fisica.ds_bairro%type;
ds_endereco_w		compl_pessoa_fisica.ds_endereco%type;
ds_complemento_w	compl_pessoa_fisica.ds_complemento%type;
ds_email_w		compl_pessoa_fisica.ds_email%type;

nr_sequencia_w		regra_valida_dados_pf.nr_sequencia%type;
ds_mensagem_w		regra_valida_dados_pf.ds_mensagem%type;
ie_acao_mensagem_w	regra_valida_dados_pf.ie_acao_mensagem%type;
nr_sequencia_ww		regra_valida_dados_pf.nr_sequencia%type;
ds_mensagem_ww		regra_valida_dados_pf.ds_mensagem%type;
ie_acao_mensagem_ww	regra_valida_dados_pf.ie_acao_mensagem%type;
ie_cpf_w		regra_valida_dados_pf.ie_cpf%type;
ie_rg_w			regra_valida_dados_pf.ie_rg%type;
ie_passaporte_w		regra_valida_dados_pf.ie_passaporte%type;
ie_rne_w		regra_valida_dados_pf.ie_rne%type;
ie_ric_w		regra_valida_dados_pf.ie_ric%type;
ie_estado_w		regra_valida_dados_pf.ie_estado%type;
ie_municipio_w		regra_valida_dados_pf.ie_municipio%type;
ie_bairro_w		regra_valida_dados_pf.ie_bairro%type;
ie_rua_w		regra_valida_dados_pf.ie_rua%type;
ie_complemento_w	regra_valida_dados_pf.ie_complemento%type;
ie_email_w		regra_valida_dados_pf.ie_email%type;
ie_condicao_w		regra_valida_dados_pf.ie_condicao%type;

ie_brasileiro_w		varchar2(1);
ie_ok_w			varchar2(1);
qt_idade_pac_w		number(10);
qt_consistir_w		number(10);
qt_consistido_w		number(10);
ie_pis_w			regra_valida_dados_pf.ie_pis%type;
ie_cns_w			regra_valida_dados_pf.ie_cns%type;
ie_dnv_w			regra_valida_dados_pf.ie_dnv%type;
nr_pis_pasep_w			pessoa_fisica.nr_pis_pasep%type;
cd_declaracao_nasc_vivo_w	pessoa_fisica.cd_declaracao_nasc_vivo%type;
nr_cartao_nac_sus_w		pessoa_fisica.nr_cartao_nac_sus%type;

cursor c01 is
	select	nr_sequencia,
		ds_mensagem,
		ie_acao_mensagem,
		ie_cpf,
		ie_rg,
		ie_passaporte,
		ie_rne,
		ie_ric,
		ie_estado,
		ie_municipio,
		ie_bairro,
		ie_rua,
		ie_complemento,
		ie_email,
		ie_condicao,
		ie_pis,
		ie_cns,
		ie_dnv
	from	regra_valida_dados_pf
	where	cd_estabelecimento 				= cd_estabelecimento_p
	and	ie_evento					= ie_evento_p
	and	nvl(ie_situacao,'A')				= 'A'
	and	nvl(cd_convenio,nvl(cd_convenio_p,0))		= nvl(cd_convenio_p,0)
	and	nvl(ie_tipo_convenio,nvl(ie_tipo_convenio_w,0))	= nvl(ie_tipo_convenio_w,0)
	and	qt_idade_pac_w between nvl(qt_idade_min,-1) 	and nvl(qt_idade_max,999)
	and	((ie_brasileiro = 'S' and ie_brasileiro_w = 'S') or
		(ie_estrangeiro = 'S' and ie_brasileiro_w = 'N') or
		(ie_estrangeiro = 'R' and sg_estado_w = 'IN'))
	order by nvl(qt_prioridade,0) desc,
		nvl(cd_convenio,0),
		nvl(ie_tipo_convenio,0);

begin
ds_mensagem_p		:= null;
ie_acao_mensagem_p	:= null;
nr_seq_regra_p		:= null;

if (cd_pessoa_fisica_p is not null) then

	select	max(ie_tipo_convenio)
	into	ie_tipo_convenio_w
	from	convenio
	where	cd_convenio	= cd_convenio_p;

	begin
		select	a.nr_cpf,
			a.nr_identidade,
			a.nr_passaporte,
			a.nr_reg_geral_estrang,
			a.nr_ric,
			a.cd_nacionalidade,
			to_number(obter_idade(a.dt_nascimento,sysdate,'A')),
			nr_pis_pasep,
			nr_cartao_nac_sus,
			cd_declaracao_nasc_vivo
		into	nr_cpf_w,
			nr_rg_w,
			nr_passaporte_w,
			nr_rne_w,
			nr_ric_w,
			cd_nacionalidade_w,
			qt_idade_pac_w,
			nr_pis_pasep_w,
			nr_cartao_nac_sus_w,
			cd_declaracao_nasc_vivo_w
		from	pessoa_fisica a
		where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	rownum 			= 1; --Residencial.
	exception
	when others then
		nr_cpf_w		:= null;
		nr_rg_w			:= null;
		nr_passaporte_w		:= null;
		nr_rne_w		:= null;
		nr_ric_w		:= null;
		cd_nacionalidade_w	:= null;
		qt_idade_pac_w		:= 0;
	end;

	begin
		select	b.sg_estado,
			b.ds_municipio,
			b.ds_bairro,
			b.ds_endereco,
			b.ds_complemento,
			b.ds_email
		into	sg_estado_w,
			ds_municipio_w,
			ds_bairro_w,
			ds_endereco_w,
			ds_complemento_w,
			ds_email_w
		from	pessoa_fisica a,
			compl_pessoa_fisica b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	b.ie_tipo_complemento	= 1
		and	rownum 			= 1;
	exception
	when others then
		sg_estado_w		:= null;
		ds_municipio_w		:= null;
		ds_bairro_w		:= null;
		ds_endereco_w		:= null;
		ds_complemento_w	:= null;
		ds_email_w		:= null;
	end;

	select	nvl(max(ie_brasileiro),'N')
	into	ie_brasileiro_w
	from	nacionalidade
	where	cd_nacionalidade	= cd_nacionalidade_w;

	ie_ok_w	:= 'N';

	open C01;
	loop
	fetch C01 into
		nr_sequencia_w,
		ds_mensagem_w,
		ie_acao_mensagem_w,
		ie_cpf_w,
		ie_rg_w,
		ie_passaporte_w,
		ie_rne_w,
		ie_ric_w,
		ie_estado_w,
		ie_municipio_w,
		ie_bairro_w,
		ie_rua_w,
		ie_complemento_w,
		ie_email_w,
		ie_condicao_w,
		ie_pis_w,
		ie_cns_w,
		ie_dnv_w;
	exit when C01%notfound or ds_mensagem_ww is not null;
		begin
		qt_consistir_w	:= 0;
		qt_consistido_w	:= 0;
		
		if	(ie_cpf_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(nr_cpf_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;		
		end if;
		if	(ie_rg_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(nr_rg_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;		
		end if;
		if	(ie_passaporte_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(nr_passaporte_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		if	(ie_rne_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(nr_rne_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		if	(ie_ric_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(nr_ric_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		if	(ie_estado_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(sg_estado_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		if	(ie_municipio_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(ds_municipio_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		if	(ie_bairro_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(ds_bairro_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		if	(ie_rua_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(ds_endereco_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		if	(ie_complemento_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(ds_complemento_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		if	(ie_email_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			if	(ds_email_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		
		if	(ie_pis_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			
			if	(nr_pis_pasep_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		
		if	(ie_cns_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			
			if	(nr_cartao_nac_sus_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;
		
		if	(ie_dnv_w = 'S') then
			qt_consistir_w	:= qt_consistir_w + 1;
			
			if	(cd_declaracao_nasc_vivo_w is null) then
				qt_consistido_w	:= qt_consistido_w + 1; 
			end if;
		end if;

		if	(ie_condicao_w = 'E' and qt_consistir_w = qt_consistido_w) or
			(ie_condicao_w = 'OU' and qt_consistido_w > 0) then
			nr_sequencia_ww		:= nr_sequencia_w;
			ds_mensagem_ww		:= ds_mensagem_w;
			if	(ds_mensagem_w is not null) then
				ie_acao_mensagem_ww	:= ie_acao_mensagem_w;
			end if;	

		end if;	

		end;
	end loop;
	close C01;

	ds_mensagem_p		:= ds_mensagem_ww;
	ie_acao_mensagem_p	:= ie_acao_mensagem_ww;
	nr_seq_regra_p		:= nr_sequencia_ww;

end if;

end VALIDAR_DADOS_PF;
/