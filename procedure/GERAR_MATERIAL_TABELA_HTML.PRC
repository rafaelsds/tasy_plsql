create or replace
procedure gerar_material_tabela_html(cd_estabelecimento_p		number,
					nr_seq_tabela_p			number,					
					nm_usuario_p			varchar2) is 

cd_tabela_custo_w	number;					
					
begin

cd_tabela_custo_w := obter_tab_custo_html(nr_seq_tabela_p);

gerar_material_tabela(	cd_estabelecimento_p,
			cd_tabela_custo_w,
			nr_seq_tabela_p,
			nm_usuario_p);

commit;

end gerar_material_tabela_html;
/