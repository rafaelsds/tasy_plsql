create or replace
procedure gp_atualiza_obito_pront(cd_pessoa_fisica_p	varchar2) is 

begin
if	(cd_pessoa_fisica_p is not null) then
	update	same_prontuario
	set	ie_tipo_prontuario	= 'O'
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_p;	
end if;
commit;

end gp_atualiza_obito_pront;
/