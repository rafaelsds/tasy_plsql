create or replace
procedure pls_gerar_benef_selec(
				nr_seq_inclusao_benef_p		pls_inclusao_beneficiario.nr_sequencia%type,
				nr_seq_lote_p			pls_lote_inclusao_benef.nr_sequencia%type) is 

begin

--Tratamento realizado para desfazer a seleção de todos os beneficiários.
if	(nr_seq_lote_p is not null) then
	update	pls_inclusao_beneficiario
	set	ie_selecionado		= 'N'
	where	nr_seq_lote_inclusao	= nr_seq_lote_p;
end if;

if	(nr_seq_inclusao_benef_p is not null) then
	update	pls_inclusao_beneficiario
	set	ie_selecionado	= 'S'
	where	nr_sequencia	= nr_seq_inclusao_benef_p;

end if;

commit;
	
end pls_gerar_benef_selec;
/