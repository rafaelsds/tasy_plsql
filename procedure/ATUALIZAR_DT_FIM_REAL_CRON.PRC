create or replace
procedure Atualizar_Dt_Fim_Real_Cron(
			nm_usuario_p		Varchar2,
			nr_seq_cronograma_p	number) is 

qt_etapa_nao_con_w	number(10);

begin

select	count(nr_sequencia) -- pega a quantidade de etapas com porcentagem de conclus�o menor que 100%
into 	qt_etapa_nao_con_w
from   	proj_cron_etapa 
where  	nr_seq_cronograma = nr_seq_cronograma_p
and    	pr_etapa < 100;	
	
	if(qt_etapa_nao_con_w <= 0 )then -- Se estiver 0 � porque todas as etapas do cronograma est�o 100% conclu�das
		begin
			update proj_cronograma a
			set	dt_fim_real = sysdate,
				nm_usuario = nm_usuario_p
			where a.nr_sequencia = nr_seq_cronograma_p;
		end;
	else
		begin
			update proj_cronograma a
			set dt_fim_real = null,
				nm_usuario = nm_usuario_p
			where a.nr_sequencia = nr_seq_cronograma_p;
		end;
	end if;



commit;

end Atualizar_Dt_Fim_Real_Cron;
/