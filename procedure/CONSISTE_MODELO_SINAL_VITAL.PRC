create or replace procedure consiste_modelo_sinal_vital(nm_atributo_p 	varchar2,
														nr_seq_modelo_p number,
														nr_cirurgia_p number,
														vl_item_p		varchar2,
														ds_retorno_p	out	varchar2,
														ie_retorno_p	out	varchar2) is
cursor regrasv is
select psv.ie_sinal_vital, sv.nm_atributo, pmsvr.qt_minimo,pmsvr.qt_maximo,pmsvr.qt_min_aviso
      ,pmsvr.qt_max_aviso,pmsvr.ds_mensagem_alerta,pmsvr.ds_mensagem_bloqueio, obter_valor_dominio(1768,psv.ie_sinal_vital) ds_sinal
  from pepo_modelo_secao pms
  inner join pepo_modelo_secao_sv pmsv on pmsv.nr_seq_modelo_secao = pms.nr_sequencia
  inner join pepo_sinal_vital psv on psv.nr_sequencia = pmsv.nr_seq_pepo_sv
  inner join pepo_sv sv on sv.ie_sinal_vital = psv.ie_sinal_vital
  inner join pepo_modelo_sec_sv_regra pmsvr on pmsvr.nr_seq_modelo_secao_sv = pmsv.nr_sequencia
  where pms.nr_seq_modelo = nr_seq_modelo_p
  and sv.nm_atributo = nm_atributo_p;

w_ie_retorno varchar2(1) := 'N';
w_ds_retorno varchar2(1000) := null;
w_vl_min number := null;
w_vl_max number := null;
w_ds_sinal varchar2(50);

begin

for r1 in regraSV loop

	if (r1.qt_minimo is not null and vl_item_p < r1.qt_minimo and w_ie_retorno <> 'E') then
		w_ie_retorno := 'E';
		w_ds_retorno := r1.ds_mensagem_bloqueio;
		w_vl_min 	 := r1.qt_minimo;
		w_vl_max 	 := r1.qt_maximo;
		w_ds_sinal   := r1.ds_sinal;
	elsif (r1.qt_maximo is not null and vl_item_p > r1.qt_maximo  and w_ie_retorno <> 'E') then
		w_ie_retorno := 'E';
		w_vl_min 	 := r1.qt_minimo;
		w_vl_max     := r1.qt_maximo;
		w_ds_retorno := r1.ds_mensagem_bloqueio;
		w_ds_sinal   := r1.ds_sinal;
	elsif (r1.qt_min_aviso is not null and vl_item_p < r1.qt_min_aviso  and w_ie_retorno not in ('E','A')) then
		w_ie_retorno := 'A';
		w_ds_retorno := r1.ds_mensagem_alerta;
		w_vl_min     := r1.qt_min_aviso;
		w_vl_max     := r1.qt_max_aviso;
		w_ds_sinal   := r1.ds_sinal;
	elsif (r1.qt_max_aviso is not null and vl_item_p > r1.qt_max_aviso and w_ie_retorno not in ('E','A')) then
		w_ie_retorno := 'A';
		w_ds_retorno := r1.ds_mensagem_alerta;
		w_vl_min     := r1.qt_min_aviso;
		w_vl_max     := r1.qt_max_aviso;
		w_ds_sinal   := r1.ds_sinal;
	end if;
end loop;

if (w_ds_retorno is null and w_ie_retorno <> 'N') then
  if (w_ie_retorno = 'E') then
  w_ds_retorno	:= wheb_mensagem_pck.get_texto(1161772, 'QT_MINIMO_PER_W=' || to_char(w_vl_min) ||
  ';VL_MAXIMO_PER_W=' || to_char(w_vl_max) || ';DS_SINAL_VITAL_W=' || w_ds_sinal);
  else
    w_ds_retorno	:= wheb_mensagem_pck.get_texto(1161774, 'QT_MIN_AVISO_W=' || to_char(w_vl_min) ||
    ';QT_MAX_AVISO_W=' || to_char(w_vl_max) || ';DS_SINAL_VITAL_W=' || w_ds_sinal);
  end if;
end if;
ds_retorno_p := w_ds_retorno;
ie_retorno_p := w_ie_retorno;

end consiste_modelo_sinal_vital;
/
