create or replace
procedure iso_ctb_movto_export_INSERT(	nr_sequencia_p 		ISO_CTB_MOVIMENTO_EXPORT.NR_SEQUENCIA%TYPE,
					nr_lote_contabil_p 		ISO_CTB_MOVIMENTO_EXPORT.NR_LOTE_CONTABIL%TYPE,
					nm_usuario_p		ISO_CTB_MOVIMENTO_EXPORT.NM_USUARIO%TYPE,
					dt_movimento_p		ISO_CTB_MOVIMENTO_EXPORT.DT_MOVIMENTO%TYPE,
					vl_movimento_p		ISO_CTB_MOVIMENTO_EXPORT.VL_MOVIMENTO%TYPE,
					cd_historico_p		ISO_CTB_MOVIMENTO_EXPORT.CD_HISTORICO%TYPE,
					cd_conta_debito_p		ISO_CTB_MOVIMENTO_EXPORT.CD_CONTA_DEBITO%TYPE,
					cd_conta_credito_p		ISO_CTB_MOVIMENTO_EXPORT.CD_CONTA_CREDITO%TYPE,
					ds_complemento_p		ISO_CTB_MOVIMENTO_EXPORT.DS_COMPL_HISTORICO%TYPE,
					nr_seq_agrupamento_p	ISO_CTB_MOVIMENTO_EXPORT.NR_SEQ_AGRUPAMENTO%TYPE,
					nr_lancamento_p		ISO_CTB_MOVIMENTO_EXPORT.NR_LANCAMENTO%TYPE,
					cd_conta_centro_debito_p	ISO_CTB_MOVIMENTO_EXPORT.CD_CENTRO_CUSTO_DEB%TYPE,
					cd_conta_centro_credito_p	ISO_CTB_MOVIMENTO_EXPORT.CD_CENTRO_CUSTO_DEB%TYPE) is 

begin

insert	into	iso_ctb_movimento_export
	(	nr_sequencia,
		nr_lote_contabil,
		dt_atualizacao,
		nm_usuario,
		dt_movimento,
		vl_movimento,
		cd_historico,
		cd_conta_debito,
		cd_conta_credito,
		ds_compl_historico,
		nr_seq_agrupamento,
		nr_lancamento,
		cd_centro_custo_deb,
		cd_centro_custo_cred)
	values(	nr_sequencia_p,
		nr_lote_contabil_p,
		sysdate,
		nm_usuario_p,
		dt_movimento_p,
		vl_movimento_p,
		cd_historico_p,
		cd_conta_debito_p,
		cd_conta_credito_p,
		ds_complemento_p,
		nr_seq_agrupamento_p,
		nr_lancamento_p,
		cd_conta_centro_debito_p,
		cd_conta_centro_credito_p);

end iso_ctb_movto_export_INSERT;
/