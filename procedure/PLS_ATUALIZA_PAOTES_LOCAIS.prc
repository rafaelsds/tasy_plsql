create or replace procedure pls_atualiza_paotes_locais(
  nr_sequencia_p number,
  nm_usuario_p varchar2,
  cd_estabelecimento_p number
) is


cursor cPacotes is
select cd_procedimento, ie_situacao_pacote 
from pls_sis_pac_retorno_pacote
where nr_seq_requisicao = nr_sequencia_p;

begin	
	for c1 in cPacotes loop
	update pls_pacote 
	   set ie_status_sispac = c1.ie_situacao_pacote,
		   nm_usuario		= 'WEBSERVICE', 
		   dt_atualizacao   = sysdate
     where cd_procedimento  = c1.cd_procedimento
	   and ie_status_sispac <> c1.ie_situacao_pacote;
	end loop;
commit;

end pls_atualiza_paotes_locais;
/
