create or replace
procedure PLS_GERAR_REQ_DIAGNOSTICO_WEB
			(	NM_USUARIO_P		varchar2,
				NR_SEQ_REQUISICAO_P	varchar2,
				IE_UNIDADE_TEMPO_P	varchar2,
				QT_TEMPO_P		varchar2,
				IE_TIPO_DOENCA_P	varchar2,
				ie_indicacao_acidente_p varchar2,
				CD_DOENCA_P		varchar2,
				IE_CLASSIFICACAO_P	varchar2 ) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Gerar diagnósticos da requisição para autorização no portal web
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relatórios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de atenção:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/				
				
begin

if	(cd_doenca_p is not null or ie_tipo_doenca_p is not null or ie_indicacao_acidente_p is not null or qt_tempo_p is not null) then
	insert into pls_requisicao_diagnostico
		(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
		nm_usuario, nm_usuario_nrec, nr_seq_requisicao,
		ie_unidade_tempo, qt_tempo, ie_tipo_doenca,
		ie_indicacao_acidente, cd_doenca, ie_classificacao)
	values	(pls_requisicao_diagnostico_seq.nextval, sysdate, sysdate,
		nm_usuario_p, nm_usuario_p, nr_seq_requisicao_p,
		ie_unidade_tempo_p, qt_tempo_p, ie_tipo_doenca_p,
		ie_indicacao_acidente_p, upper(cd_doenca_p),ie_classificacao_p);
end if;

commit;

end PLS_GERAR_REQ_DIAGNOSTICO_WEB;
/