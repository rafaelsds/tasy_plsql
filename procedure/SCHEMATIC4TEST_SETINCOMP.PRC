CREATE OR REPLACE PROCEDURE SCHEMATIC4TEST_SETINCOMP(NR_SEQUENCIA_P NUMBER, DS_OBS_P VARCHAR2, USUARIO_P VARCHAR2) IS      
BEGIN
    UPDATE SCHEM_TEST_PACKAGE_SCHED SET DS_INFO = DS_OBS_P, DT_ATUALIZACAO = SYSDATE, NM_USUARIO = USUARIO_P, IE_STATUS = '5', IE_ESTAG_EXEC = '4' WHERE NR_SEQUENCIA = NR_SEQUENCIA_P;
    UPDATE SCHEM_TEST_SCHEDULE SET DS_INFO = DS_OBS_P, DT_ATUALIZACAO = SYSDATE, NM_USUARIO = USUARIO_P, IE_STATUS = '5' WHERE NR_SEQ_PACKAGE = NR_SEQUENCIA_P;
    COMMIT;    
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('Erro: Data not found');
END SCHEMATIC4TEST_SETINCOMP;
/