create or replace 
procedure sup_comunicar_nova_pf
			(	cd_cnpj_p		varchar2,
				nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar comunica��o interna da de cadastro de pessoa jur�dica.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_tipo_pessoa_w		number(10);
ds_comunic_w			varchar2(2000);
ds_titulo_w			varchar2(255);
nr_seq_classif_w		number;
cd_perfil_dest_w		number;
cd_setor_dest_w			number;
nm_usuario_regra_w		varchar2(255);
qt_regras_w			number;
ds_perfil_w			varchar2(255);
ds_setor_w			varchar2(255);
nm_usuario_dest_w		varchar2(4000);

cursor	c01 is
	select	cd_perfil,
		cd_setor_destino,
		ds_usuarios_destino
	from 	regra_aviso_cadastro_pj
	where	nvl(cd_tipo_pessoa, cd_tipo_pessoa_w)	= cd_tipo_pessoa_w
	and	nvl(cd_cnpj, cd_cnpj_p)			= cd_cnpj_p
	and	nvl(ie_novo_cadastro,'N')		= 'S'
	order by 
		nvl(cd_tipo_pessoa, 0),
		nvl(cd_cnpj,'');

begin
select	max(cd_tipo_pessoa)
into	cd_tipo_pessoa_w
from	pessoa_juridica
where 	cd_cgc	= cd_cnpj_p;

select	max(obter_classif_comunic('F'))
into	nr_seq_classif_w
from	dual;

select	count(*)
into	qt_regras_w
from 	regra_aviso_cadastro_pj
where	nvl(ie_novo_cadastro,'N')	= 'S';

ds_titulo_w	:= wheb_mensagem_pck.get_texto(351550);
ds_comunic_w	:= substr(	wheb_mensagem_pck.get_texto(351550) || chr(13) || chr(10) ||
				wheb_mensagem_pck.get_texto(351552, 'CD_CNPJ=' || cd_cnpj_p || ';NM_PESSOA=' || substr(obter_nome_pf_pj(null, cd_cnpj_p),1,120)),1,2000);
				
if	(qt_regras_w > 0) then
	open c01;
	loop
	fetch c01 into
		cd_perfil_dest_w,
		cd_setor_dest_w,
		nm_usuario_regra_w;
	exit when c01%notfound;
		begin
		if	(cd_perfil_dest_w is not null) then
			ds_perfil_w	:= ds_perfil_w || substr(to_char(cd_perfil_dest_w) || ',',1,255);
		end if;
		
		if	(cd_setor_dest_w is not null) then
			ds_setor_w	:= ds_setor_w || substr(to_char(cd_setor_dest_w) || ',',1,255);
		end if;	

		if	(nm_usuario_regra_w is not null) then
			nm_usuario_dest_w	:= substr(nm_usuario_dest_w || nm_usuario_regra_w || ',',1,4000);
		end if;
		end;
	end loop;
	close 	c01;
end if;

if	(qt_regras_w > 0) and
	((nm_usuario_dest_w is not null) or (ds_setor_w is not null)) then
	gerar_comunic_padrao(	sysdate, 
				ds_titulo_w,
				ds_comunic_w,
				nm_usuario_p,
				'N',
				nm_usuario_dest_w,
				'N',
				nr_seq_classif_w,
				ds_perfil_w,
				'',
				ds_setor_w,
				sysdate,
				'',
				'');
end if;

commit;

end sup_comunicar_nova_pf;
/