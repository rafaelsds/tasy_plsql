create or replace
procedure cih_liberar_higienizacao_maos(nr_sequencia_p 		number,
					nm_usuario_p		Varchar2) is 

begin
if	(nr_sequencia_p is not null) then
	update cih_higienizacao_maos
	set 	nm_usuario_lib = nm_usuario_p,
		dt_liberacao = SYSDATE
	WHERE 	nr_sequencia 	= nr_sequencia_p;

end if;

commit;

end cih_liberar_Higienizacao_maos;
/
