create or replace 
procedure fis_reinf_4010(	nr_sequencia_p 	number, 
				nm_usuario_p 	varchar2) is

cd_estabelecimento_w	fis_reinf_r4010_regra.cd_estabelecimento%type;


	cursor c1 is
		select /*ideEvento*/
			a.ie_ind_retif indretif,
			a.nr_recibo nrrecibo,
			a.dt_competencia perapur,
			a.cd_ident_ambiente tpamb,
			'1' procemi,
			a.cd_ident_ambiente verproc,
			/*ideContri*/
			'1' tpInsc,
			b.cd_cgc nrInsc,
			/*ideEstab*/
			'1' tpInscEstab,
			b.cd_cgc nrinscestab,
			b.cd_estabelecimento cd_estabelecimento
		from	fis_reinf_r4010 a, estabelecimento b
		where	a.cd_estabelecimento = b.cd_estabelecimento
		and	a.nr_sequencia = nr_sequencia_p;

	r1 c1%rowtype;

	cursor c2 is
		select	cd_tributo,
				ie_tipo_data
		from	fis_reinf_r4010_regra
		where	cd_estabelecimento = cd_estabelecimento_w
		or		cd_estabelecimento is null;
		
	r2 c2%rowtype;
	



	/*@@@@@@@@@@@@@@@@@@@@@@@@   PROCEDURES INTERNAS  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	
	/*BLOCO - > benefPen  -   Bloco filho de  detDed
	procedure p_r4010_benef_pen(	nr_seq_superior_p in number,
					cd_pessoa_fisica_p in number) is
	
		nr_sequencia_w number(10);
		
		cursor c_benef_pen is
			select	a.cd_pessoa_dependente cpf,
				a.dt_nascimento dtnascto,
				a.nm_dependente nome,
				substr(obter_nome_pf(a.cd_pessoa_dependente),1,255) reldep,
				a.ds_observacao descrdep
			from	pessoa_fisica_dependente a
			where	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
			-- Falta filtar por esta regra. 
			-- Dever� buscar do cadastro de Pessoa fisica, pasta dependentes(PESSOA_FISICA_DEPENDENTE), dever� ser criado um campo que define se este benefici�rio � dependente de pens�o alimenticia. 
			-- Estes campos devem somente sair no REINF caso o campo  indTpDeducao seja = 5. PESSOA_FISICA_DEPENDENTE.CD_PESSOA_DEPENDENTE
		r_benef_pen c_benef_pen%rowtype;	
	begin
		
		open c_benef_pen;
		loop
		fetch c_benef_pen into r_benef_pen;
		exit when c_benef_pen%notfound;
				
			begin
				select	fis_reinf_r4010_benef_pen_seq.nextval
				into	nr_sequencia_w
				from	dual;
			end;	
		
			insert into fis_reinf_r4010_benef_pen
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				cd_cpf,
				dt_nascto,
				ds_nome,
				cd_rel_dep,
				ds_dep,
				vl_depen,
				vl_depen_susp,
				nr_seq_superior)
			values (nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				r_benef_pen.cpf,
				r_benef_pen.dtnascto,
				r_benef_pen.nome,
				r_benef_pen.reldep,
				r_benef_pen.descrdep,
				null,
				null,
				nr_seq_superior_p);
				
		end loop;
		close c_benef_pen;	
	end p_r4010_benef_pen;
	*/

	/*BLOCO - > detDed  -   Bloco filho de  idePgto 
	procedure p_r4010_det_ded(	nr_seq_dados_p in number,
					cd_pessoa_fisica_p in number) is
	
		nr_sequencia_w number(10);
		
		cursor c_det_deb is
			select	null	indTpDeducao,
				null	vlrDeducao,
				null	vlrDedSusp,
				null	nrInscPrevComp
			from dual;
			
		r_det_deb c_det_deb%rowtype;  
				
	begin
	open c_det_deb;
	loop
	fetch c_det_deb into r_det_deb;
	exit when c_det_deb%notfound;
	
		begin
			select	fis_reinf_r4010_det_ded_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;
	
		insert into fis_reinf_r4010_det_ded
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ie_tp_deducao,
			vl_deducao,
			vl_ded_susp,
			nr_insc_prev_comp,
			nr_seq_dados)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			r_det_deb.indTpDeducao,
			r_det_deb.vlrDeducao,
			r_det_deb.vlrDedSusp,
			r_det_deb.nrInscPrevComp,
			nr_seq_dados_p);
		
		-- chama registro filho
		p_r4010_benef_pen(	nr_sequencia_w,
					cd_pessoa_fisica_p);
	end loop;
	close c_det_deb;
	end p_r4010_det_ded;
	*/
	
	/*BLOCO - > rendIsento  -   Bloco filho de  idePgto 
	procedure p_r4010_rend_isen(	nr_seq_dados_p in number )  is
	
		nr_sequencia_w number(10);
	begin

		begin
			select	fis_reinf_r4010_rend_isen_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;
	
		insert into fis_reinf_r4010_rend_isen
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_tp_isencao,
			vl_isento,
			ds_rendimento,
			dt_laudo,
			nr_seq_dados)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			0,
			null,
			null,
			nr_seq_dados_p);
			
	end p_r4010_rend_isen;
	*/

	/*BLOCO - > infoProcRet  -   Bloco filho de  idePgto 
	procedure p_r4010_proc_ret(	nr_seq_dados_p in number ) is
	
		nr_sequencia_w number(10);
	begin

		begin
			select	fis_reinf_r4010_proc_ret_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;
	
		insert into fis_reinf_r4010_proc_ret
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_tp_proc_ret,
			nr_proc_ret,
			cd_susp,
			vl_nretido,
			vl_dep,
			nr_seq_dados)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			0,
			0,
			nr_seq_dados_p);
			
	end p_r4010_proc_ret; 
	*/

	/*BLOCO - > ideAdv 1  -   Bloco filho de  despProcJud 1 
	procedure p_r4010_ideadv_rra (	nr_seq_superior_p in number ) is

		nr_sequencia_w number(10);

	begin
	
		begin
			select	fis_reinf_r4010_ideadv_rra_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;
	
		insert into fis_reinf_r4010_ideadv_rra
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_tp_insc_adv,
			nr_insc_adv,
			vl_adv,
			nr_seq_superior)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			nr_seq_superior_p);
			
	end p_r4010_ideadv_rra;
	*/

	/*BLOCO - > despProcJud 1  -   Bloco filho de  infoRRA 1 
	procedure p_r4010_dpj_or_rra(	nr_seq_superior_p in number ) is

		nr_sequencia_w number(10);

	begin
		begin
			select	fis_reinf_r4010_dpj_or_rra_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;
	
		insert into fis_reinf_r4010_dpj_or_rra
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			vl_desp_custas,
			vl_desp_advogados,
			cd_cnpj_orig_recurso,
			nr_seq_superior)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			nr_seq_superior_p);
			
		p_r4010_ideadv_rra(nr_sequencia_w);
		
	end p_r4010_dpj_or_rra;
	*/

	/*BLOCO - > infoRRA   -   Bloco filho de   idePgto 
	procedure p_r4010_info_rra(	nr_seq_dados_p in number ) is
	
		nr_sequencia_w number(10);
	begin

		begin
			select	fis_reinf_r4010_info_rra_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;
	
		insert into fis_reinf_r4010_info_rra
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_tp_proc_rra,
			nr_proc_rra,
			cd_ind_orig_rec,
			ds_rra,
			nr_qt_meses_rra,
			nr_seq_dados)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			null,
			null,
			nr_seq_dados_p);
		p_r4010_dpj_or_rra(nr_sequencia_w);
	end p_r4010_info_rra;
	*/

	/*BLOCO - > ideAdv 2  -   Bloco filho de  despProcJud 2 
	procedure p_r4010_ideadv_pj(nr_seq_superior_p	in number) is 

		nr_sequencia_w number(10);

	begin
		begin
			select	fis_reinf_r4010_ideadv_pj_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;

		insert into fis_reinf_r4010_ideadv_pj
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_tp_insc_adv,
			nr_insc_adv,
			vl_adv,
			nr_seq_superior)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			nr_seq_superior_p);
			
	end p_r4010_ideadv_pj;
	*/

	/*BLOCO - > despProcJud 2  -   Bloco filho de  infoRRA 2 
	procedure p_r4010_dpj_orec(nr_seq_superior_p	in number) is

		nr_sequencia_w number(10);

	begin
		begin
			select	fis_reinf_r4010_dpj_orec_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;

		insert into fis_reinf_r4010_dpj_orec
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			vl_desp_custas,
			vl_desp_advogados,
			cd_cnpj_orig_recurso,
			nr_seq_superior)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			nr_seq_superior_p);
			
		p_r4010_ideadv_pj(nr_sequencia_w);
	end p_r4010_dpj_orec;
	*/

	/*BLOCO - > infoProcJud   -   Bloco filho de   idePgto 
	procedure p_r4010_proc_jud(nr_seq_dados_p	in number) is
	
		nr_sequencia_w number(10);

	begin
		begin
			select	fis_reinf_r4010_proc_jud_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;

		insert into fis_reinf_r4010_proc_jud
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_proc,
			cd_ind_origem_recursos,
			ds_proc_jud,
			nr_seq_dados)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			nr_seq_dados_p);
	
		p_r4010_dpj_orec(nr_sequencia_w);
	end p_r4010_proc_jud;
	*/

	/*BLOCO - > endExt   -   Bloco filho de   ideEvento */
	procedure p_r4010_inf_pg_ext(	nr_seq_evento_p		in	number,
					cd_pessoa_fisica_p	in	varchar2) is	

		nr_sequencia_w number(10);

	begin

		if OBTER_SE_PF_PJ_ESTRANGEIRO(cd_pessoa_fisica_p,null) = 'S' then
	
			begin
				select	fis_reinf_r4010_inf_pg_ext_seq.nextval
				into	nr_sequencia_w
				from	dual;
			end;

			insert into fis_reinf_r4010_inf_pg_ext
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_lograd,
				nr_lograd,
				ds_complem,
				ds_bairro,
				ds_cidade,
				ds_estado,
				cd_postal,
				nr_telefone,
				cd_ind_nif,
				nr_if_benef,
				ds_frm_tribut,
				nr_seq_evento)
			values (nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				obter_dados_pf_pj(cd_pessoa_fisica_p,null,'R'),
				obter_dados_pf_pj(cd_pessoa_fisica_p,null,'NR'),
				obter_dados_pf_pj(cd_pessoa_fisica_p,null,'CO'),
				obter_dados_pf_pj(cd_pessoa_fisica_p,null,'B'),
				obter_dados_pf_pj(cd_pessoa_fisica_p,null,'CI'),
				obter_dados_pf_pj(cd_pessoa_fisica_p,null,'UF'),
				null,
				obter_dados_pf_pj(cd_pessoa_fisica_p,null,'T'),
				null,
				null,
				null,
				nr_seq_evento_p);
			
		end if;			
			
	end p_r4010_inf_pg_ext;

	/*BLOCO - > infoReembDep   -   Bloco filho de   infoReemb 
	procedure p_r4010_reemb_dep(nr_seq_superior_p	in	number) is
	
		nr_sequencia_w number(10);

	begin
		begin
			select	fis_reinf_r4010_reemb_dep_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;

		insert into fis_reinf_r4010_reemb_dep
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_tp_insc,
			nr_insc,
			vl_reemb,
			vl_reemb_ant,
			nr_seq_superior)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			null,
			nr_seq_superior_p);
			
	end p_r4010_reemb_dep;
	*/

	/*BLOCO - > infoReemb   -   Bloco filho de   ideOpSaude 
	procedure p_r4010_reemb_tit(nr_seq_superior_p	in	number) is
	
		nr_sequencia_w number(10);

	begin
	
		begin
			select	fis_reinf_r4010_reemb_tit_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;

		insert into fis_reinf_r4010_reemb_tit
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_tp_insc,
			nr_insc,
			vl_reemb,
			vl_reemb_ant,
			nr_seq_superior)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			null,
			nr_seq_superior_p);
			
		begin
			select	fis_reinf_r4010_depend_pl_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;

		insert into fis_reinf_r4010_depend_pl
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_cpf,
			dt_nascto,
			ds_nome,
			cd_rel_dep,
			vl_saude,
			nr_seq_superior)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			null,
			null,
			nr_seq_superior_p);
			
		p_r4010_reemb_dep(nr_sequencia_w);
	end p_r4010_reemb_tit;
	*/

	/*BLOCO - > ideOpSaude   -   Bloco filho de   ideOpSaude 
	procedure p_r4010_op_saude(nr_seq_evento_p	in	number) is
	
		nr_sequencia_w number(10);

	begin
		begin
			select	fis_reinf_r4010_op_saude_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;

		insert into fis_reinf_r4010_op_saude
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_nr_insc,
			nr_reg_ans,
			vl_saude,
			nr_seq_evento)
		values (nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			null,
			null,
			nr_seq_evento_p);
	
		p_r4010_reemb_tit(nr_sequencia_w);
	end p_r4010_op_saude;
	*/

	/*BLOCO - > idePgto  -   Bloco filho de  GERAL */
	procedure p_r4010_dados(nr_sequencia_p		in number,
				cd_pessoa_fisica_p	in varchar2,
				cd_estabelecimento_p	in number,
				dt_apuracao_p		in date,
				cd_tributo_p	in number,
				ie_tipo_data_p	in number) IS					
	
		cursor c_idePgto is
			select /*idePgto*/
				d.nr_titulo nr_titulo,
				obter_cod_pais_reinf(obter_compl_pf(a.cd_pessoa_fisica, 1, 'NP')) paisResid,
				null observ,
				/*infoPgto*/
				decode(ie_tipo_data_p, 1, trunc(d.dt_emissao), 2, trunc(c.dt_baixa)) dtFG,
				'N' indDecTerc,
				sum(d.vl_titulo) vlrRendBruto,
				sum(e.vl_base_calculo) vlrRendTrib,
				sum(e.vl_imposto) vlrIR,
				sum(decode(nvl(f.ie_exigibilidade_suspensa,'N'), 'N', 0, e.vl_base_calculo)) vlrRendSusp,
				sum(decode(nvl(f.ie_exigibilidade_suspensa,'N'), 'S', decode(f.ie_exig_susp_forma, 1, e.vl_imposto, 2, 0),0)) vlrNIR,
				sum(decode(nvl(f.ie_exigibilidade_suspensa,'N'), 'S', decode(f.ie_exig_susp_forma, 1, 0, 2, e.vl_imposto),0)) vlrDeposito,
				0 vlrCompAnoCalend,
				0 vlrCompAnoAnt,
				a.cd_pessoa_fisica
			from	pessoa_fisica  a,				
				titulo_pagar_baixa   c,
				titulo_pagar         d,
				titulo_pagar_imposto e,
				tributo_conta_pagar  f,
				tributo              h
			where	a.cd_pessoa_fisica = d.cd_pessoa_fisica
			and	d.nr_titulo = c.nr_titulo(+)	
			and	e.nr_titulo = d.nr_titulo(+)
			and	h.cd_tributo = e.cd_tributo(+)
			and e.nr_seq_trib_cp = f.nr_sequencia(+)
			and	h.ie_tipo_tributo = 'IR'
			and	e.cd_tributo = cd_tributo_p
			and	decode(ie_tipo_data_p, 1, to_char(d.dt_emissao,'MMRRRR'), 2, to_char(c.dt_baixa,'MMRRRR')) = to_char(dt_apuracao_p,'MMRRRR')
			and	d.cd_estabelecimento = cd_estabelecimento_p
			and	a.cd_pessoa_fisica = cd_pessoa_fisica_p
			group by	d.nr_titulo,
						'N',
						d.dt_emissao,
						c.dt_baixa,
						a.cd_pessoa_fisica
			order by	dtFG, nr_titulo;
					
		r_idePgto c_idePgto%rowtype;
		nr_sequencia_w number(10);		
				
	BEGIN
	open c_idePgto;
	loop
	fetch c_idePgto into r_idePgto;
	exit when c_idePgto%notfound;
	
		begin
			select	fis_reinf_r4010_dados_seq.nextval
			into	nr_sequencia_w
			from	dual;
		end;
	
		insert into fis_reinf_r4010_dados
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_evento,
			cd_nat_rend,
			cd_pais_resid,
			ds_observ,
			dt_fg,
			ie_ind_dec_terc,
			vl_rend_bruto,
			vl_rend_trib,
			vl_ir,
			vl_rend_susp,
			vl_nir,
			vl_deposito,
			vl_comp_ano_calend,
			vl_comp_ano_ant,
			nr_insc_fci,
			nr_insc_scp,
			tx_perc_scp,
			nr_titulo)
		values
			(nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			nr_sequencia_p,
			null,
			r_idePgto.paisResid,
			r_idePgto.observ,
			r_idePgto.dtFG,
			r_idePgto.indDecTerc,
			r_idePgto.vlrRendBruto,
			r_idePgto.vlrRendTrib,
			r_idePgto.vlrIR,
			r_idePgto.vlrRendSusp,
			r_idePgto.vlrNIR,
			r_idePgto.vlrDeposito,
			r_idePgto.vlrCompAnoCalend,
			r_idePgto.vlrCompAnoAnt,
			null,
			null,
			null,
			r_idePgto.nr_titulo);
			
		/* Proc de Tags n�o obrigatorias
		p_r4010_det_ded(nr_sequencia_w, r_idePgto.cd_pessoa_fisica);
		p_r4010_rend_isen(nr_sequencia_w);
		p_r4010_proc_ret(nr_sequencia_w);
		p_r4010_info_rra(nr_sequencia_w);
		p_r4010_proc_jud(nr_sequencia_w);
		*/
		
	end loop;
	close c_idePgto;

	END p_r4010_dados;

	procedure p_r4010_evento(	nrInsc_p 		in varchar2,
								nrinscestab_p	in varchar2,
								cd_tributo_p	in number,
								ie_tipo_data_p	in number) IS
	
		/*ideBenef*/
		cursor c_ideBenef is
			select  distinct
				a.cd_pessoa_fisica cd_pessoa_fisica,
				a.nr_cpf cpfBenef,
				substr(obter_nome_pf(a.cd_pessoa_fisica),1,255) nmBenef,
				e.cd_estabelecimento,
				e.dt_competencia,
				'1' tpInsc,
				'1' tpInscEstab
			from	pessoa_fisica a,
				titulo_pagar b,
				titulo_pagar_baixa c,
				titulo_pagar_imposto d,
				fis_reinf_r4010 e
			where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
			and	decode(ie_tipo_data_p, 1, to_char(b.dt_emissao,'MMRRRR'), 2, to_char(c.dt_baixa,'MMRRRR')) = to_char(e.dt_competencia,'MMRRRR')
			and	b.cd_estabelecimento = e.cd_estabelecimento
			and	b.nr_titulo = d.nr_titulo(+)
			and	b.nr_titulo = c.nr_titulo(+)
			and	d.cd_tributo = cd_tributo_p
			and	e.nr_sequencia = nr_sequencia_p;
			
			
		r_ideBenef c_ideBenef%rowtype; 
		nr_sequencia_w number(10);

	BEGIN
	
	open c_ideBenef;
	loop
	fetch c_ideBenef into r_ideBenef;
	exit when c_ideBenef%notfound;
	
		select	fis_reinf_r4010_evento_seq.nextval
		into	nr_sequencia_w
		from	dual;
	
		insert into fis_reinf_r4010_evento
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_lote,
			cd_tp_insc,
			nr_insc,
			cd_tp_insc_estab,
			cd_id,
			nr_insc_estab,
			cd_cpf_benef,
			ds_nm_benef,
			cd_pessoa_fisica)
		values
			(nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			nr_sequencia_p,
			r_idebenef.tpInsc,
			nrInsc_p,
			r_idebenef.tpInscEstab,  
			null,
			nrinscestab_p,
			r_idebenef.cpfbenef,
			r_idebenef.nmbenef,
			r_idebenef.cd_pessoa_fisica);
			
			/* Chamada dos registros filhos */
			p_r4010_dados(	nr_sequencia_w,
					r_idebenef.cd_pessoa_fisica,
					r_idebenef.cd_estabelecimento,
					r_idebenef.dt_competencia,
					cd_tributo_p,
					ie_tipo_data_p);
					
			p_r4010_inf_pg_ext(	nr_sequencia_w,
						r_idebenef.cd_pessoa_fisica);
						
			--p_r4010_op_saude(nr_sequencia_w);
			
	end loop;
	close c_ideBenef;
			
	END p_r4010_evento;
	

begin

open c1;
loop fetch c1 into r1;
exit when c1%notfound;

	cd_estabelecimento_w := r1.cd_estabelecimento;

	open c2;
	loop fetch c2 into r2;
	exit when c2%notfound;
		p_r4010_evento(r1.nrInsc, r1.nrinscestab, r2.cd_tributo, r2.ie_tipo_data);
	end loop;
	close c2;

end loop;
close c1;

update fis_reinf_r4010 set dt_geracao = sysdate where nr_sequencia = nr_sequencia_p;

commit;

end fis_reinf_4010;
/