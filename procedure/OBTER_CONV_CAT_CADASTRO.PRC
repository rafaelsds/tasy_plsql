create or replace
procedure obter_conv_cat_cadastro(
			cd_pessoa_fisica_p	varchar2,
			cd_categoria_p	    out varchar2,
			cd_convenio_p	    out number,
			cd_carteirinha_P    out varchar2) is 

nr_sequencia_w	number(10);			
begin

if	(cd_pessoa_fisica_p is not null) then

	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	pessoa_titular_convenio
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	if	(nr_sequencia_w > 0) then
		
		select	max(cd_convenio),
			max(cd_categoria),
			max(substr(cd_usuario_convenio,1,30))
		into	cd_convenio_p,
			cd_categoria_p,
			cd_carteirinha_p
		from	pessoa_titular_convenio
		where 	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	nr_sequencia = nr_sequencia_w;
	end if;
end if;

commit;

end obter_conv_cat_cadastro;
/