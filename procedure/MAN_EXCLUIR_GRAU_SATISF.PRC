create or replace
procedure man_excluir_grau_satisf(	nr_seq_ordem_serv_p		number) is 

begin
	if	(nr_seq_ordem_serv_p is not null) then
		begin
		delete
		from    man_ordem_serv_satisf
		where	nr_seq_ordem_serv = nr_seq_ordem_serv_p;
		commit;
		end;
	end if;

end man_excluir_grau_satisf;
/