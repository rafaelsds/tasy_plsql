create or replace procedure GERAR_SCORE_FLEX_II_SAE	(	nr_sequencia_p		number,
						nm_usuario_p		varchar2) is
cursor c01 is
  select z.* from (
    select a.nr_atendimento as a_nr_atendimento, a.nr_sequencia as a_nr_sequencia, a.cd_prescritor as a_cd_prescritor, a.cd_pessoa_fisica as a_cd_pessoa_fisica,
    b.nr_sequencia as b_nr_sequencia, b.nr_seq_item as b_nr_seq_item, b.nr_seq_result as b_nr_seq_result, b.nr_seq_prescr as b_nr_seq_prescr, b.ds_observacao as b_ds_observacao,
    c.nr_sequencia as c_nr_sequencia, c.ds_item as c_ds_item,
    d.nr_seq_tipo_item as d_nr_seq_tipo_item, d.nr_seq_item as d_nr_seq_item,
    e.nr_sequencia as e_nr_sequencia, e.ds_tipo_item as e_ds_tipo_item,
    f.nr_sequencia as f_nr_sequencia, f.nr_seq_item as f_nr_seq_item, f.ds_resultado as f_resultado_sae, f.nr_seq_apresent as f_nr_seq_apresent,
    g.nr_sequencia as g_nr_sequencia, g.nr_seq_result_escala as g_nr_seq_result_escala, g.nr_seq_regra as g_nr_seq_regra, g.nr_seq_result_sae as g_nr_seq_result_sae,
    h.nr_sequencia as h_nr_sequencia, h.nr_seq_item as h_nr_seq_item, h.ds_resultado as h_resultado_scoreflex, h.qt_pontuacao as h_qt_pontuacao,
    i.ie_score_flex_ii as i_ie_score_flex_ii
    from 
        pe_prescricao a,
        pe_prescr_item_result b,
        pe_item_examinar c,
        pe_item_tipo_item d,
        pe_tipo_item e,
        pe_item_resultado f,
        pe_regra_result_escala g,
        eif_escala_ii_item_result h,
        pe_regra_item_escala i
    where	a.nr_sequencia	= nr_sequencia_p
      and a.nr_sequencia	= b.nr_seq_prescr
      and	b.nr_seq_item	= c.nr_sequencia
      and	d.nr_seq_item	= c.nr_sequencia
      and	e.nr_sequencia	= d.nr_seq_tipo_item
      and	f.nr_sequencia	= b.nr_seq_result
      and g.nr_seq_result_sae = b.nr_seq_result
      and h.nr_sequencia = g.nr_seq_result_escala
      and f.nr_seq_item = i.nr_seq_item_sae
    ) z;
    
cursor c02 is
  select i.ie_score_flex_ii as escala_score_flex, count(*) as qtd
    from 
        pe_prescricao a,
        pe_prescr_item_result b,
        pe_regra_item_escala i
    where	a.nr_sequencia	= nr_sequencia_p
      and a.nr_sequencia	= b.nr_seq_prescr
      and	b.nr_seq_item	= i.nr_seq_item_sae
      group by i.ie_score_flex_ii;
    
c01_w			            c01%rowtype;
c02_w			            c02%rowtype;
prescritor_w          pe_prescricao.cd_prescritor%type;
nr_seq_w	            escala_eif_ii.nr_sequencia%type;
nr_seq_item_w	        escala_eif_ii_item.nr_sequencia%type;
nr_seq_atendimento_w  pe_prescricao.nr_atendimento%type;
nr_seq_escala_w	      pe_regra_item_escala.ie_score_flex_ii%type;
nr_seq_avaliacao_w	  eif_escala_ii.nr_seq_avaliacao%type;
pontos_w	            eif_escala_ii_item_result.qt_pontuacao%type := 0;
dummy                 number := 0;
sql_w                 varchar2(250);
pontos_out_w          eif_escala_ii_item_result.qt_pontuacao%type := 0;
    
begin
      select a.cd_prescritor, a.nr_atendimento
        into prescritor_w, nr_seq_atendimento_w
        from pe_prescricao a
      where	a.nr_sequencia	= nr_sequencia_p
        and rownum = 1;
  open c02;
  loop
  fetch c02 into	
    c02_w;
  exit when c02%notfound;
    begin
      if (c02_w.escala_score_flex is null) then
        goto continue_label_outter;
      end if;
      
      select escala_eif_ii_seq.nextval into nr_seq_w from dual;
      
      insert into escala_eif_ii (
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec, 
                nm_usuario_nrec,
                nr_atendimento, 
                ie_situacao,
                dt_liberacao,
                dt_inativacao,
                nm_usuario_inativacao,
                ds_justificativa,
                nr_seq_escala,
                cd_profissional,
                qt_pontos,
                dt_avaliacao,
                ie_nivel_atencao,
                nr_seq_escuta,
                nr_seq_assinatura, 
                nr_seq_assinat_inativacao
      ) values (
                nr_seq_w,
                sysdate, 
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                nr_seq_atendimento_w,
                'A',
                sysdate,
                null,
                null,
                null,
                c02_w.escala_score_flex,
                prescritor_w,
                0,
                sysdate,
                'T',
                null,
                null,
                null 
      );

      open c01;
      loop
      fetch c01 into	
        c01_w;
      exit when c01%notfound;
        begin
        if (c01_w.i_ie_score_flex_ii is null or c01_w.i_ie_score_flex_ii <> c02_w.escala_score_flex) then
          goto continue_label_inner;
        end if;
        
        begin
          sql_w := 'CALL OBTER_SCORE_FLEXII_SAE_MD(:1, :2) INTO :pontos_out_w';
          EXECUTE IMMEDIATE sql_w USING IN pontos_w, 
                                        IN c01_w.h_qt_pontuacao,
                                        OUT pontos_out_w;
        exception
          when others then 
            pontos_w := null;
          end;

        pontos_w := pontos_out_w;
        
          select escala_eif_ii_item_seq.nextval into nr_seq_item_w from dual;
          
          insert into escala_eif_ii_item (
                    nr_sequencia,
                    nr_seq_escala,
                    nr_seq_item,
                    nr_seq_result,
                    nm_usuario,
                    nm_usuario_nrec,
                    dt_atualizacao,
                    dt_atualizacao_nrec
          )
          values( 
                    nr_seq_item_w,
                    nr_seq_w,
                    c01_w.h_nr_seq_item,
                    c01_w.h_nr_sequencia,
                    nm_usuario_p,
                    nm_usuario_p,
                    sysdate,
                    sysdate
          );
          <<continue_label_inner>>
          
          dummy := 0;
        end;
      end loop;
      close c01;
    
      update escala_eif_ii set qt_pontos = pontos_w where nr_sequencia = nr_seq_w;
      
    <<continue_label_outter>>
      pontos_w := 0;
    end;
  end loop;
  close c02;

	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
  
end GERAR_SCORE_FLEX_II_SAE;
/
