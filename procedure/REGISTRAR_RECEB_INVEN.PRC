create or replace
procedure registrar_receb_inven(
			nr_seq_inventario_p		number,
			nm_usuario_p			varchar) is

qt_pendente_w		number(10);

begin

select	count (*)
into	qt_pendente_w
from	rop_inv_reposicao_item
where	nr_seq_inventario = nr_seq_inventario_p
and	dt_recebimento is null;


update  rop_inv_reposicao
set	dt_recebimento = sysdate,
	nm_usuario_receb = nm_usuario_p
where	nr_sequencia = nr_seq_inventario_p;

if	(qt_pendente_w > 0) then
	update	rop_inv_reposicao_item
	set	dt_recebimento = sysdate,
		nm_usuario_receb = nm_usuario_p
	where	nr_seq_inventario = nr_seq_inventario_p;
end if;

commit;

end registrar_receb_inven;
/