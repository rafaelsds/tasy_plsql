create or replace
procedure pls_liberar_itens_interc_guia
			(	nr_seq_guia_p		Number,
				nr_seq_proc_mat_guia_p	Number,
				ie_status_p		Varchar2,
				ie_tipo_item_p		varchar2,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 

begin

if	(ie_tipo_item_p = 'P') then
	pls_liberar_guia_intercambio(	null, nr_seq_proc_mat_guia_p, null, 
					ie_status_p, null, null,
					null, cd_estabelecimento_p, nm_usuario_p);
elsif	(ie_tipo_item_p = 'M') then
	pls_liberar_guia_intercambio(	null, null, nr_seq_proc_mat_guia_p, 
					ie_status_p, null, null,
					null, cd_estabelecimento_p, nm_usuario_p);
end if;

commit;

end pls_liberar_itens_interc_guia;
/