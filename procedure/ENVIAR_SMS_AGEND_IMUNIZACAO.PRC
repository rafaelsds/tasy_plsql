create or replace
procedure Enviar_sms_agend_imunizacao is 

id_sms_w		number(10);
ie_envia_w		varchar2(1);
ds_remetente_w		varchar2(255);
ds_destinatario_w	varchar2(255);
ds_mensagem_w		varchar2(255);
nr_seq_vacina_w		number(10);
nm_usuario_w		varchar2(15);
nm_pf_w			varchar2(255);
ds_erro_w		varchar2(2000);
ds_vacina_w		varchar2(255);
ds_dose_w		varchar2(255);
dt_prevista_execucao_w	date;
ds_estab_w		varchar2(255);
nr_telefone_w		varchar2(15);

Cursor C01 is
select	a.nr_sequencia nr_seq_viagem,
	Nvl(Decode(substr(somente_numero(obter_dados_pf(obter_dados_atendimento(nr_atendimento,'PFR'),'TCD')),1,255),0,'',substr(somente_numero(obter_dados_pf(obter_dados_atendimento(nr_atendimento,'PFR'),'TCD')),1,255)),decode(substr(somente_numero(obter_dados_pf(a.cd_pessoa_fisica,'TCD')),1,2), 
	substr(somente_numero(obter_dados_pf(a.cd_pessoa_fisica,'TC')),1,2),
	somente_numero(obter_dados_pf(a.cd_pessoa_fisica,'TC')),
	somente_numero(obter_dados_pf(a.cd_pessoa_fisica,'TCD')))) ds_destinatario,
	dt_prevista_execucao,
	substr(obter_descricao_padrao('VACINA','DS_VACINA',nr_seq_vacina),1,255) ds_vacina,
	substr(obter_valor_dominio(1018,ie_dose),1,100) ds_vacina
from	paciente_vacina a
where	trunc(dt_prevista_execucao-2,'dd') = trunc(sysdate,'dd')
and	exists (select 1 
		from vacina x 
		where x.nr_sequencia = a.nr_seq_vacina
		and	ie_envia_sms = 'S'
		)
and	not exists (	select	1 
			from	log_envio_sms x 
			where	x.nr_seq_vacina = a.nr_sequencia)
order by 1;
	
begin

select	substr(obter_nome_estabelecimento(1),1,255)
into	ds_estab_w
from	dual;

select	OBTER_DADOS_PF_PJ(null,obter_cgc_estabelecimento(1),'T')
into	nr_telefone_w
from	dual;

open C01;
loop
fetch C01 into	
	nr_seq_vacina_w,
	ds_destinatario_w,
	dt_prevista_execucao_w,
	ds_vacina_w,
	ds_dose_w;
exit when C01%notfound;
	begin
	nm_usuario_w := 'Job';
	ds_remetente_w := obter_valor_param_usuario(0, 63, obter_perfil_ativo, nm_usuario_w, 1);
	ds_mensagem_w := substr(wheb_mensagem_pck.get_texto(307463, 'DT_PREVISTA=' || to_char(dt_prevista_execucao_w,'dd/mm/yyyy') || ';' ||
																'NR_TELEFONE_W=' || nr_telefone_w),1,120);
					--  Vacinas agendadas para #@DT_PREVISTA#@. Traga a carteira de vacina��o. Fone: #@NR_TELEFONE_W#@
	if	(ds_remetente_w is not null) and
		(ds_destinatario_w is not null) and
		(ds_mensagem_w is not null) and
		(nr_seq_vacina_w is not null) and
		(nm_usuario_w is not null) then
		begin
		ds_erro_w := '';
		
		wheb_sms.enviar_sms(ds_remetente_w, ds_destinatario_w, ds_mensagem_w, nm_usuario_w,id_sms_w);
		
		insert into log_envio_sms (	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_envio,
						nr_telefone,
						ds_mensagem,
						nr_seq_vacina,
						id_sms,
						ie_tipo_viagem)
					values(	log_envio_sms_seq.nextval,
						sysdate,
						nm_usuario_w,
						sysdate,
						ds_destinatario_w,
						ds_mensagem_w,
						nr_seq_vacina_w,
						id_sms_w,
						'T');
		commit;
		exception
		when others then
		
		ds_erro_w	:= SQLERRM(sqlcode);
		
		insert into log_envio_sms (	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_envio,
				nr_telefone,
				ds_mensagem,
				nr_seq_vacina,
				id_sms,
				ie_tipo_viagem)
			values(	log_envio_sms_seq.nextval,
				sysdate,
				nm_usuario_w,
				sysdate,
				ds_destinatario_w,
				ds_erro_w,
				nr_seq_vacina_w,
				id_sms_w,
				'T');
		commit;
			

		end;
	end if;
	end;
end loop;
close C01;

commit;

end Enviar_sms_agend_imunizacao;
/
