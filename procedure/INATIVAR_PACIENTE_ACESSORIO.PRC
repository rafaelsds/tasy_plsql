create or replace
procedure inativar_paciente_acessorio(	nr_sequencia_p			number,
				nm_usuario_p			varchar2,
				ds_justificativa_p 			varchar2) is

begin

update		paciente_acessorio
set		dt_inativacao 		= sysdate,
		nm_usuario_inativacao 	= nm_usuario_p,
		ds_justificativa 		= substr(ds_justificativa_p,1,250),
		ie_situacao		= 'I'
where		nr_sequencia 		= nr_sequencia_p; 

commit;

end	inativar_paciente_acessorio;
/
