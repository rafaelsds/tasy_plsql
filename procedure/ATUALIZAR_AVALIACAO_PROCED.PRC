create or replace
procedure Atualizar_avaliacao_proced(
			nr_seq_avaliacao_p	number,
			nr_prescricao_p		number,
			nr_sequencia_p		Varchar2,
			nm_usuario_p		Varchar2) is 

begin

if	(nr_seq_avaliacao_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	prescr_procedimento
	set	nr_seq_avaliacao	= nr_seq_avaliacao_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_prescricao		= nr_prescricao_p
	and	obter_se_contido(nr_sequencia,	nr_sequencia_p) = 'S';
	end;
end if;

commit;

end Atualizar_avaliacao_proced;
/
