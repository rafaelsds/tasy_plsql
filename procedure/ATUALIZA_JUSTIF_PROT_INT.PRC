create or replace
procedure atualiza_justif_prot_int	( nr_sequencia_p	number,
									  ds_justificativa_p  varchar2,
									  dt_fim_p date default null,
									  ds_acao varchar2 default 'R') is

begin

	if (ds_acao = 'R') then
		update	protocolo_integrado
		set	ds_justificativa	=	ds_justificativa_p
		where	nr_sequencia	=	nr_sequencia_p;
	else
		update protocolo_integrado
		set ds_justificativa	=	ds_justificativa_p,
			dt_fim 				=	dt_fim_p
		where nr_sequencia		=	nr_sequencia_p;
	end if;
	commit;

end atualiza_justif_prot_int;
/
