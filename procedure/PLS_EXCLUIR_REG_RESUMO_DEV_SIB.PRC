/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Excluir os registros de resumo no momento da importa��o do arquivo do SIB
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_excluir_reg_resumo_dev_sib
		(	nr_seq_lote_sib_p	number,
			nm_usuario_p		Varchar2) is 

begin

delete	sib_devolucao_resumo
where	nr_seq_lote 	= nr_seq_lote_sib_p
and	ie_xml		= 'S';

commit;

end pls_excluir_reg_resumo_dev_sib;
/
