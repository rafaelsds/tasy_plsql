create or replace
procedure consistir_credenciamento_pf	(cd_estabelecimento_p	number,
						cd_especialidade_p		varchar2,
						cd_paciente_p			varchar2,
						dt_referencia_p		date,
						cd_pf_credenciada_p		varchar2,
						nr_atendimento_p	number,
						nr_atendimento_mae_p	number default null,
						ie_pf_credenciada_p out	varchar2) is
					
ie_pf_credenciada_w	varchar2(2);
qt_credenciamento_w	number(10,0);
qt_recem_nascido_w	number(10);					
					
begin
if	(cd_pf_credenciada_p is not null) then

	/* obter se regras pf */
	select	count(*)
	into	qt_credenciamento_w
	from	pf_credenciamento
	where	cd_pf_credenciamento	= cd_pf_credenciada_p
	and	((cd_estabelecimento	= cd_estabelecimento_p) or (cd_estabelecimento is null));
	
	/* verificar regras pf */
	if	(qt_credenciamento_w > 0) then
	
		select 	count(*)		
		into 	qt_recem_nascido_w
		from	nascimento
		where 	nr_atendimento = nr_atendimento_mae_p;
	
	
		select	decode(count(*), 0, 'N', 'S')
		into	ie_pf_credenciada_w
		from	pf_credenciamento
		where	cd_pf_credenciamento				= cd_pf_credenciada_p
		and	nvl(cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
		--and	((cd_especialidade_p = 0) or (nvl(cd_especialidade,cd_especialidade_p)	= cd_especialidade_p))
		and	((nvl(cd_paciente,nvl(cd_paciente_p,'0'))		= nvl(cd_paciente_p,'0')) or ((Nvl(ie_credenciar_rn,'N') = 'S') and (qt_recem_nascido_w > 0)))
		and	((nvl(nr_atendimento, nvl(nr_atendimento_p,0))	= nvl(nr_atendimento_p,0)) or ((Nvl(ie_credenciar_rn,'N') = 'S') and (qt_recem_nascido_w > 0)))
		and	consistir_vigencia_credenc(dt_credenciamento,dt_fim_credenciamento,dt_referencia_p)	= 'S';

			
	else
	
		ie_pf_credenciada_w	:= 'NC';
		
	end if;
	
end if;

ie_pf_credenciada_p := ie_pf_credenciada_w;

end consistir_credenciamento_pf;
/
