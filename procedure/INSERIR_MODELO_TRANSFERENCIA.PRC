create or replace
procedure inserir_modelo_transferencia(nr_seq_modelo_dest_p	number,
				       nr_seq_modelo_origem_p	number,
				     nr_seq_pac_reab_p		VARCHAR2,
				     nm_usuario_p		VARCHAR2) is

cursor C01 is
	select	nr_sequencia,
		hr_horario,
		cd_agenda,
		cd_profissional_padrao,
		ie_dia_semana
	from	rp_item_modelo_agenda
	where	nr_seq_modelo = nr_seq_modelo_dest_p;

nr_sequencia_pac_modelo_w  	number(10,0);
nr_sequencia_w			number(10,0);
nr_seq_item_modelo_w		number(10,0);
cd_agenda_w			number(10,0);
hr_horario_w			date;
qt_vagas_w			number(10,0);
qt_modelo_w			number(10,0);
cd_medico_exec_w		varchar2(10);
ie_dia_semana_w			number(1,0);
nr_seq_motivo_fim_tratamento_w	number(10,0);
dt_fim_tratamento_w		date;


begin

select	nvl(qt_vagas,0)
into	qt_vagas_w
from	rp_modelo_agendamento
where	nr_sequencia = nr_seq_modelo_dest_p;


select	count(*)
into	qt_modelo_w
from	rp_pac_modelo_agendamento
where	nr_seq_modelo_agendamento = nr_seq_modelo_dest_p
and	nvl(ie_situacao,'A') = 'A'
and	dt_fim_tratamento is null;

if	(qt_modelo_w >= qt_vagas_w) then
	--Rase_application_error(-20011,'N�o � poss�vel gerar este modelo. Limite de vagas foi excedido!');
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(233146);
end if;

select	rp_pac_modelo_agendamento_seq.nextval
into	nr_sequencia_pac_modelo_w
from	dual;

insert into rp_pac_modelo_agendamento( nr_sequencia,
				       dt_inicio_modelo,
				       nr_seq_pac_reab,
				       nr_seq_modelo_agendamento,
				       dt_atualizacao,
				       nm_usuario,
				       dt_atualizacao_nrec,
				       nm_usuario_nrec,
				       ie_situacao)
				values (nr_sequencia_pac_modelo_w,
					sysdate,
					nr_seq_pac_reab_p,
					nr_seq_modelo_dest_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					'A');
	
	commit;

open C01;
loop
fetch C01 into
	nr_seq_item_modelo_w,
	hr_horario_w,
	cd_agenda_w,
	cd_medico_exec_w,
	ie_dia_semana_w;
exit when C01%notfound;
	begin
	
	select	max(nr_seq_motivo_fim_tratamento),
		max(dt_fim_tratamento)
	into	nr_seq_motivo_fim_tratamento_w,
		dt_fim_tratamento_w
	from	rp_pac_modelo_agend_item
	where	nr_seq_modelo_pac 	= nr_seq_modelo_origem_p
	and	cd_agenda		= cd_agenda_w
	and	dt_horario		= hr_horario_w
	and	ie_dia_semana		= ie_dia_semana_w
	and	((cd_medico_exec		= cd_medico_exec_w) or (cd_medico_exec_w is null))
	and	dt_fim_tratamento is not null;

	select	rp_pac_modelo_agend_item_seq.nextval
	into	nr_sequencia_w
	from	dual;

	if	(dt_fim_tratamento_w is not null) then
		
		insert into rp_pac_modelo_agend_item( nr_sequencia,
						nr_seq_modelo_pac,
						nr_seq_item_modelo,
						dt_horario,
						cd_agenda,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_medico_exec,
						ie_dia_semana,
						dt_fim_tratamento,
						nr_seq_motivo_fim_tratamento)
					values (nr_sequencia_w,
						nr_sequencia_pac_modelo_w,
						nr_seq_item_modelo_w,
						hr_horario_w,
						cd_agenda_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						cd_medico_exec_w,
						ie_dia_semana_w,
						dt_fim_tratamento_w,
						nr_seq_motivo_fim_tratamento_w);
	
	elsif	(dt_fim_tratamento_w is null) then
	
		insert into rp_pac_modelo_agend_item( nr_sequencia,
						nr_seq_modelo_pac,
						nr_seq_item_modelo,
						dt_horario,
						cd_agenda,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_medico_exec,
						ie_dia_semana)
					values (nr_sequencia_w,
						nr_sequencia_pac_modelo_w,
						nr_seq_item_modelo_w,
						hr_horario_w,
						cd_agenda_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						cd_medico_exec_w,
						ie_dia_semana_w);
	end if;
	end;
end loop;
close C01;



commit;

end inserir_modelo_transferencia;
/
