create or replace
procedure pls_gerar_lanc_prog_reaj_fx_et
			(	nr_seq_item_origem_p	number,
				vl_item_p		number,
				nr_seq_segurado_p	number,
				dt_lancamento_p		date,
				qt_parcela_p		number,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

qt_lancamentos_w			number(10);
nr_seq_pagador_w			pls_mensalidade.nr_seq_pagador%type;
dt_mesano_referencia_w		pls_mensalidade_segurado.dt_mesano_referencia%type;
qt_parcela_w				number(10);
vl_parcela_w				pls_segurado_mensalidade.vl_item%type;
dt_parcela_w				pls_segurado_mensalidade.dt_referencia%type;
dt_inicio_cobertura_w		pls_segurado_mensalidade.dt_inicio_cobertura%type;
dt_fim_cobertura_w			pls_segurado_mensalidade.dt_fim_cobertura%type;
ds_mensagem_parcela_w		varchar2(255);
nr_seq_lanc_auto_w			pls_segurado_mensalidade.nr_sequencia%type;
vl_apropriacao_w			pls_mens_seg_item_aprop.vl_apropriacao%type;
nr_seq_centro_apropriacao_w	pls_mens_seg_item_aprop.nr_seq_centro_apropriacao%type;
vl_apropriacao_soma_w		pls_mens_seg_item_aprop.vl_apropriacao%type := 0;
vl_apropriacao_final_w		pls_mens_seg_item_aprop.vl_apropriacao%type;
qt_apropriacoes_w			number(10);

Cursor C01 is
	select
		a.nr_seq_centro_apropriacao,
		a.vl_apropriacao
	from	pls_mens_seg_item_aprop a
	where a.nr_seq_item = nr_seq_item_origem_p;

begin

if	(nvl(qt_parcela_p,0) < 2) then
	qt_parcela_w	:= 1;
else
	qt_parcela_w	:= qt_parcela_p;
end if;

select	count(1)
into	qt_lancamentos_w
from	pls_segurado_mensalidade
where	nr_seq_item_origem = nr_seq_item_origem_p;

select 	count(1)
into 	qt_apropriacoes_w
from	pls_mens_seg_item_aprop
where 	nr_seq_item = nr_seq_item_origem_p;

if	(qt_lancamentos_w = 0) then
	select	c.nr_seq_pagador,
		b.dt_mesano_referencia,
		a.dt_inicio_cobertura,
		a.dt_fim_cobertura
	into	nr_seq_pagador_w,
		dt_mesano_referencia_w,
		dt_inicio_cobertura_w,
		dt_fim_cobertura_w
	from	pls_mensalidade_seg_item	a,
		pls_mensalidade_segurado	b,
		pls_mensalidade			c
	where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
	and	c.nr_sequencia	= b.nr_seq_mensalidade
	and	a.nr_sequencia	= nr_seq_item_origem_p;
	
	for i in 1..qt_parcela_w loop
		begin
		dt_parcela_w	:= add_months(dt_lancamento_p,i-1);
		if	(i = qt_parcela_w) then
			vl_parcela_w	:= vl_item_p / qt_parcela_w;
			vl_parcela_w	:= vl_parcela_w + (vl_item_p - (vl_parcela_w*qt_parcela_w));
		else
			vl_parcela_w	:= vl_item_p / qt_parcela_w;
		end if;
		
		if	(qt_parcela_w >1) then
			ds_mensagem_parcela_w	:= ' Parcela ' ||i||'/'||qt_parcela_w||' - Valor total: R$'||campo_mascara_virgula(vl_item_p);
		else
			ds_mensagem_parcela_w	:= null;
		end if;
		
		insert into pls_segurado_mensalidade
			(	nr_sequencia, cd_estabelecimento, dt_atualizacao,
				dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
				ie_situacao, vl_item, nr_seq_segurado,
				dt_referencia, nr_seq_item_origem, ie_tipo_lanc,
				ie_tipo_item, nr_seq_pagador, ds_observacao,
				dt_inicio_cobertura, dt_fim_cobertura)
			values(	pls_segurado_mensalidade_seq.nextval, cd_estabelecimento_p, sysdate,
				sysdate, nm_usuario_p, nm_usuario_p,
				'A', vl_parcela_w, nr_seq_segurado_p,
				dt_parcela_w, nr_seq_item_origem_p, 'B',
				'45', nr_seq_pagador_w, wheb_mensagem_pck.get_texto(1130479,'DT_REFERENCIA='||to_char(dt_mesano_referencia_w,'mm/yyyy')) || ds_mensagem_parcela_w,
				dt_inicio_cobertura_w, dt_fim_cobertura_w ) returning nr_sequencia into nr_seq_lanc_auto_w;
				
		for j in 1..qt_apropriacoes_w loop
		
			select
				a.nr_seq_centro_apropriacao,
				a.vl_apropriacao
			into
				nr_seq_centro_apropriacao_w,
				vl_apropriacao_w
			from (
				select
					nr_seq_centro_apropriacao,
					vl_apropriacao,
					rownum as rn
				from	pls_mens_seg_item_aprop
				where nr_seq_item = nr_seq_item_origem_p
				order by nr_sequencia) a
			where a.rn = j;
			
			if	(i = qt_parcela_w) then
				select	sum(b.vl_apropriacao)
				into	vl_apropriacao_final_w
				from	pls_segurado_mensalidade a,
						pls_lancamento_mens_aprop b
				where a.nr_sequencia = b.nr_seq_lanc_auto
				and a.nr_seq_item_origem = nr_seq_item_origem_p
				and b.nr_seq_centro_apropriacao = nr_seq_centro_apropriacao_w;
				
				vl_apropriacao_final_w := vl_apropriacao_w - vl_apropriacao_final_w;
			else
				if	(j = qt_apropriacoes_w) then
					vl_apropriacao_final_w	:= vl_parcela_w - vl_apropriacao_soma_w;
					vl_apropriacao_soma_w	:= 0;
				else
					vl_apropriacao_final_w	:= vl_parcela_w * (vl_apropriacao_w / vl_item_p);
					vl_apropriacao_soma_w	:= vl_apropriacao_soma_w + vl_apropriacao_final_w;
				end if;
			end if;
			
			insert into pls_lancamento_mens_aprop
				(	nr_sequencia, nr_seq_centro_apropriacao, nr_seq_lanc_auto,
					dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
					vl_apropriacao)
			values
				(	pls_lancamento_mens_aprop_seq.nextval, nr_seq_centro_apropriacao_w, nr_seq_lanc_auto_w,
					sysdate, sysdate, nm_usuario_p, nm_usuario_p,
					vl_apropriacao_final_w);
		
		end loop;
		end;
	end loop;
end if;

commit;

end pls_gerar_lanc_prog_reaj_fx_et;
/