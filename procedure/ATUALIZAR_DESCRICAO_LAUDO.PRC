create or replace
procedure atualizar_descricao_laudo(	nr_sequencia_p	number,
					ds_laudo_p	varchar2,
					nm_usuario_p	Varchar2) is 

begin

update	laudo_paciente
set	ds_laudo = ds_laudo_p
where	nr_sequencia = nr_sequencia_p;

commit;

end atualizar_descricao_laudo;
/