create or replace
PROCEDURE atend_pac_befpost_conf_eup_js(
					dt_entrada_p			DATE,
					ie_data_reinternacao_p		VARCHAR2,
					qt_dias_internacao_p		NUMBER,
					ie_completo_p			VARCHAR2,
					ie_tipo_atend_old_p		NUMBER,
					ie_novo_alta_anterior_p		VARCHAR2,
					ie_novo_p			VARCHAR2,
					cd_medico_resp_p		VARCHAR2,
					ie_alerta_menor_sem_resp_w	VARCHAR2,
					ie_consiste_regra_atend_conv_w	VARCHAR2,
					ie_tipo_atendimento_p		NUMBER,
					cd_estabelecimento_p		NUMBER,
					nr_atendimento_p		NUMBER,
					ie_modo_edicao_p 		VARCHAR2,
					cd_pessoa_responsavel_p		VARCHAR2,
					cd_pessoa_fisica_p		VARCHAR2,
					ie_exige_cpf_resp_w		VARCHAR2,
					nm_usuario_p			VARCHAR2,
					ie_forma_consist_credenc_med_p	VARCHAR2,
					ie_clinica_p			VARCHAR2,
					dt_alta_p			DATE,
					ds_msg_erro_p		 OUT 	VARCHAR2,
					ds_msg_alerta_cpf_resp_p OUT VARCHAR2,
					ds_msg_alerta_cons_conv_p OUT	VARCHAR2,
					ds_msg_confir_inf_resp_p  OUT VARCHAR2,
					ds_msg_conf_atend_alta_p OUT VARCHAR2,
					ds_msg_confirm_reinternacao_p OUT VARCHAR2,
					ds_msg_alerta_crend_mdic_p OUT VARCHAR2,
					nr_seq_classificacao_p	NUMBER,
					ie_tipo_convenio_p	varchar2 default null) IS

ds_msg_erro_w			VARCHAR2(255);
ds_msg_alerta_cpf_resp_w	VARCHAR2(255);
ie_possui_cpf_w			VARCHAR2(1);
qt_convenio_atend_w		NUMBER(10);
ds_msg_cons_convenio_w		VARCHAR2(255);
ds_msg_alerta_cons_conv_w	VARCHAR2(255);
ie_emancipado_w			VARCHAR2(1);
qt_anos_w			NUMBER(10);
ds_msg_confir_inf_resp_w	VARCHAR2(255);
ds_msg_atend_sem_alta_w		VARCHAR2(255);
ds_msg_conf_atend_sem_alta_w	VARCHAR2(255);
dt_cons_reinternacao_w		DATE;
ds_msg_confirm_reinternacao_w	VARCHAR2(255);
ds_msg_abrt_avs_crend_mdic_w	VARCHAR2(255);
ie_corpo_clinico_w		VARCHAR2(1);
cd_especialidade_w		NUMBER(10);
ie_credencial_venc_w		VARCHAR2(10);
nm_medico_w			VARCHAR2(255);
dt_referencia_w			DATE;
BEGIN



IF	(ie_novo_p = 'S') THEN
	
	consiste_gerar_novo_atend(cd_pessoa_fisica_p, nm_usuario_p, ie_tipo_atendimento_p, cd_estabelecimento_p, NVL(cd_medico_resp_p,'0'), ie_clinica_p, ds_msg_atend_sem_alta_w, nr_seq_classificacao_p, ie_tipo_convenio_p);
	
	IF	(ds_msg_atend_sem_alta_w IS NOT NULL) THEN

		IF	(ie_novo_alta_anterior_p = 'N') OR
			(ie_novo_alta_anterior_p = 'T') OR
			(ie_novo_alta_anterior_p = 'C') OR
			(ie_novo_alta_anterior_p = 'E') THEN
			ds_msg_erro_w := SUBSTR(ds_msg_atend_sem_alta_w,1,255);
			GOTO final;
		ELSIF 	(ie_novo_alta_anterior_p = 'A') THEN
			ds_msg_conf_atend_sem_alta_w:=  SUBSTR(ds_msg_atend_sem_alta_w,1,255);			
		END IF;
	END IF;
END IF;

IF	(cd_pessoa_responsavel_p IS NULL) AND
	(cd_pessoa_fisica_p IS NOT NULL) THEN

	IF	(ie_exige_cpf_resp_w <> 'N') THEN

		SELECT	DECODE(COUNT(*),0,'N','S')
		INTO	ie_possui_cpf_w
		FROM	pessoa_fisica
		WHERE 	cd_pessoa_fisica = cd_pessoa_fisica_p
		AND	nr_cpf IS NOT NULL;

		IF	(ie_possui_cpf_w = 'N') THEN
			IF	(ie_exige_cpf_resp_w = 'S') THEN
				ds_msg_erro_w := SUBSTR(obter_texto_tasy(196695, wheb_usuario_pck.get_nr_seq_idioma),1,255);
				GOTO final;
			ELSE
				ds_msg_alerta_cpf_resp_w := SUBSTR(obter_texto_tasy(196695, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			END IF;
		END IF;
	END IF;
	IF	(ie_alerta_menor_sem_resp_w = 'S') THEN

		SELECT	SUBSTR(NVL(ie_emancipado,'N'),1,1),
			obter_idade(dt_nascimento, SYSDATE, 'A')
		INTO	ie_emancipado_w,
			qt_anos_w
		FROM	pessoa_fisica
		WHERE	cd_pessoa_fisica = cd_pessoa_fisica_p;

		IF	(qt_anos_w < 18) AND
			(ie_emancipado_w = 'N') THEN
			ds_msg_confir_inf_resp_w := SUBSTR(obter_texto_tasy(293675, wheb_usuario_pck.get_nr_seq_idioma) || CHR(10) ||
			                                   obter_texto_tasy(293677, wheb_usuario_pck.get_nr_seq_idioma) || CHR(10) ||
							   obter_texto_tasy(293679, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		END IF;

	END IF;
END IF;

IF	(ie_modo_edicao_p = 'S') THEN

	SELECT	COUNT(*)
	INTO	qt_convenio_atend_w
	FROM	atend_categoria_convenio
	WHERE	nr_atendimento = nr_atendimento_p;

	IF	(qt_convenio_atend_w > 0) THEN
		Verifica_Conv_Regra_Atend(nr_atendimento_p, cd_estabelecimento_p, ie_tipo_atendimento_p, ds_msg_cons_convenio_w);
		IF	(ds_msg_cons_convenio_w IS NOT NULL) THEN
			IF	(ie_consiste_regra_atend_conv_w = 'S') THEN
				ds_msg_erro_w := ds_msg_cons_convenio_w;
				GOTO final;
			ELSE
				ds_msg_alerta_cons_conv_w := ds_msg_cons_convenio_w;
			END IF;
		END IF;
	END IF;

	IF	(ie_completo_p = 'S') AND
		(NVL(ie_tipo_atendimento_p,0) = 1) AND
		(NVL(ie_tipo_atendimento_p,0) <> NVL(ie_tipo_atend_old_p,0)) AND
		(NVL(qt_dias_internacao_p,0) <> 0) THEN

		IF	(ie_data_reinternacao_p = 'DE') THEN
			dt_cons_reinternacao_w := dt_entrada_p;
		ELSE
			dt_cons_reinternacao_w := SYSDATE;
		END IF;
		consistir_reinternacao(cd_pessoa_fisica_p, qt_dias_internacao_p, nm_usuario_p, cd_estabelecimento_p, dt_cons_reinternacao_w, NULL, NULL, NULL, NULL, ds_msg_confirm_reinternacao_w);

		IF	(ds_msg_confirm_reinternacao_w IS NOT NULL) THEN
			ds_msg_confirm_reinternacao_p := ds_msg_confirm_reinternacao_w;
		END IF;
	END IF;
END IF;

IF (ie_forma_consist_credenc_med_p = 'A') THEN
	SELECT	NVL(dt_alta_p,SYSDATE),
		NVL(obter_especialidade_medico(cd_medico_resp_p,'C'),0)
	INTO	dt_referencia_w,
		cd_especialidade_w
	FROM 	dual;

	SELECT 	NVL(obter_se_corpo_clinico(cd_medico_resp_p),'X'),
		NVL(obter_se_credenciamento_venc(cd_estabelecimento_p,
						cd_especialidade_w,
						cd_pessoa_fisica_p,
						dt_referencia_w,
						nr_atendimento_p,
						cd_medico_resp_p),'N'),
		SUBSTR(obter_nome_pf(cd_medico_resp_p),1,255)
	INTO	ie_corpo_clinico_w,
		ie_credencial_venc_w,
		nm_medico_w
	FROM	dual;


	IF (NVL(ie_corpo_clinico_w,'X') = 'S') AND
	   (NVL(ie_credencial_venc_w,'X') = 'S') THEN

		ds_msg_alerta_crend_mdic_p := SUBSTR(obter_texto_dic_objeto(93660, wheb_usuario_pck.get_nr_seq_idioma, 'ITEM='||nm_medico_w),1,255);
	END IF;
ELSIF (ie_forma_consist_credenc_med_p = 'B') THEN

	SELECT	NVL(dt_alta_p,SYSDATE),
		NVL(obter_especialidade_medico(cd_medico_resp_p,'C'),0)
	INTO	dt_referencia_w,
		cd_especialidade_w
	FROM 	dual;

	SELECT 	NVL(obter_se_credenciamento_venc(cd_estabelecimento_p,
						cd_especialidade_w,
						cd_pessoa_fisica_p,
						dt_referencia_w,
						nr_atendimento_p,
						cd_medico_resp_p),'N'),
		SUBSTR(obter_nome_pf(cd_medico_resp_p),1,255)
	INTO	ie_credencial_venc_w,
		nm_medico_w
	FROM	dual;


	IF (NVL(ie_credencial_venc_w,'X') = 'S') THEN
		ds_msg_erro_w := SUBSTR(obter_texto_dic_objeto(93660, wheb_usuario_pck.get_nr_seq_idioma, 'ITEM='||nm_medico_w),1,255);
		GOTO final;
	END IF;
END IF;


<<final>>

ds_msg_conf_atend_alta_p 	:= ds_msg_conf_atend_sem_alta_w;
ds_msg_confir_inf_resp_p 	:= ds_msg_confir_inf_resp_w;
ds_msg_alerta_cons_conv_p 	:= ds_msg_alerta_cons_conv_w;
ds_msg_alerta_cpf_resp_p	:= ds_msg_alerta_cpf_resp_w;
ds_msg_erro_p 			:= ds_msg_erro_w;
COMMIT;

END atend_pac_befpost_conf_eup_js;
/