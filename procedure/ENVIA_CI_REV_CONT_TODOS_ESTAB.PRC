create or replace
procedure	envia_ci_rev_cont_todos_estab
			(cd_empresa_p		number) is

cd_estabelecimento_w		number(5);

cursor	c01 is
select	cd_estabelecimento
from	estabelecimento_v
where	cd_empresa = cd_empresa_p
and	ie_situacao = 'A';

begin

open c01;
loop
fetch c01 into
	cd_estabelecimento_w;
exit when c01%notfound;
	begin

	envia_ci_revisao_contrato(cd_estabelecimento_w);

	end;
end loop;
close c01;

end envia_ci_rev_cont_todos_estab;
/