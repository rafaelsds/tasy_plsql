create or replace
procedure Med_Inserir_Item_Protocolo
		(nr_seq_fatur_p		number,
		nr_seq_protocolo_p	number,
		nm_usuario_p		varchar2) is


begin

update	med_faturamento
set	nr_seq_protocolo	= nr_seq_protocolo_p,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_fatur_p;

commit;

end Med_Inserir_Item_Protocolo;
/