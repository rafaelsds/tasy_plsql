create or replace
procedure consistir_leitura_processo	(nr_seq_processo_p	number,
						vl_retorno_p out	varchar2) is

vl_retorno_w	varchar2(1);

begin
if	(nr_seq_processo_p is not null) then

	select	decode(count(*),0,'S','N')
	into	vl_retorno_w
	from	adep_processo_item
	where	nr_seq_processo 	= nr_seq_processo_p
	and	ie_suspenso		= 'N'
	and	ie_barras		= 'N';

end if;

vl_retorno_p := vl_retorno_w;

end consistir_leitura_processo;
/