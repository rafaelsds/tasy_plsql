create or replace
procedure selecionar_resultado_pe(
			nr_prescricao_p			number,		
			nr_seq_item_p			number,
			nr_seq_topografia_p		number,      
			ie_lado_p              	varchar2,
			nr_seq_result_p			number,
			ie_topografia_p		out	varchar2,
			nr_seq_sesult_out_p	out number,
			ie_observacao_p		out	varchar2,
			ie_reg_regra_p		out	varchar2,
			ie_sinal_vital_p	out	varchar2,
			nm_usuario_p			varchar2) is 
		
nr_seq_result_w			number(10);
ie_topografia_w			varchar2(1) := 'N';
ie_observacao_w			varchar2(1) := 'N';
qt_reg_w				number(10);
cd_perfil_w				number(10);
cd_estabelecimento_w	number(10);
ie_regra_w				varchar2(1) := 'N';
ie_sinal_vital_w		pe_item_resultado.ie_sinal_vital%type;

begin
if	(nr_prescricao_p is not null) and
	(nr_seq_result_p is not null) and
	(nr_seq_item_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	selecionar_resultado(
		nr_prescricao_p,
		nr_seq_result_p,
		nr_seq_item_p,
		nr_seq_topografia_p,
		ie_lado_p,
		nm_usuario_p);

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_result_w
	from	pe_prescr_item_result
	where	nr_seq_prescr = nr_prescricao_p;
	
	select	nvl(ie_topografia,'N')
	into	ie_topografia_w
	from	pe_item_examinar
	where	nr_sequencia = nr_seq_item_p;
	
	select 	nvl(ie_observacao,'N')
	into	ie_observacao_w
	from 	pe_item_resultado 
	where 	nr_sequencia = nr_seq_result_p;
	
	if	(ie_topografia_w = 'N') then
		begin
		
		select	nvl(ie_topografia,'N')
		into	ie_topografia_w
		from	pe_item_resultado
		where	nr_sequencia = nr_seq_result_p;
		end;
	end if;

	select	max(ie_sinal_vital)
	into	ie_sinal_vital_w
	from	pe_item_resultado
	where	nr_sequencia = nr_seq_result_p;
	end;
	
	cd_perfil_w 	:= obter_perfil_ativo;
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
	
	select  decode(count(*),0,'N','S') 
	into	ie_regra_w
	from   	regra_eventos_result_sae 
	where  	nr_seq_resultado = nr_seq_result_p 
	and    	nr_seq_item_examinar = nr_seq_item_p
	and    	nvl(cd_estabelecimento,nvl(cd_estabelecimento_w,0)) = nvl(cd_estabelecimento_w,0)
	and    	nvl(cd_perfil,nvl(cd_perfil_w,0)) = nvl(cd_perfil_w,0);	
	
end if;
nr_seq_sesult_out_p 	:= nr_seq_result_w;
ie_topografia_p 	:= ie_topografia_w;
ie_sinal_vital_p	:= ie_sinal_vital_w;
ie_observacao_p		:= ie_observacao_w;
ie_reg_regra_p		:= ie_regra_w;
commit;

end selecionar_resultado_pe;
/