create or replace
procedure pls_vincular_tabela_contrato
			(	nr_seq_tabela_p		number,
				nr_seq_contrato_p	number,
				nm_usuario_p		varchar2) is

nr_seq_contrato_w	number(10);
nm_segurado_w		varchar2(60);
ie_tipo_estipulante_w	varchar2(2);
nr_seq_plano_w		number(10);
ie_tipo_contratacao_w	varchar2(2);
cont_w			pls_integer;

begin

if	(nr_seq_tabela_p is not null) and
	(nr_seq_contrato_p is not null) then
	select	count(1)
	into	cont_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_p;
	
	if	(cont_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort( 296111, null );
	end if;
	
	select	max(nr_contrato),
		max(nr_seq_plano)
	into	nr_seq_contrato_w,
		nr_seq_plano_w
	from	pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_p;
	
	if	(nr_seq_contrato_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort( 244797, null );
	end if;
	
	select	max(nr_seq_contrato)
	into	nr_seq_contrato_w
	from	pls_contrato_plano
	where	nr_seq_tabela	= nr_seq_tabela_p;
	
	if	(nr_seq_contrato_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort( 244798, 'NR_SEQ_CONTRATO='||nr_seq_contrato_w );
	end if;
	
	select	max(nr_seq_contrato),
		max(substr(obter_nome_pf(cd_pessoa_fisica),1,60))
	into	nr_seq_contrato_w,
		nm_segurado_w
	from	pls_segurado
	where	nr_seq_tabela	= nr_seq_tabela_p
	and	nr_seq_contrato	<> nr_seq_contrato_p;
	
	if	(nr_seq_contrato_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort( 244799, 'NM_SEGURADO='|| nm_segurado_w ||';'||'NR_SEQ_CONTRATO='|| nr_seq_contrato_w ); 
	end if;
	
	select	max(decode(cd_pf_estipulante,null,'J','F'))
	into	ie_tipo_estipulante_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_p;
	
	/* Ana Oberziner (14/06/2013) - Pode ocorrer a situa��o de um benefici�rio possuir uma liminar judicial para possuir outra tabela de pre�o, sendo assim, � necess�rio a vincula��o de mais tabelas num contrato.
	if	(ie_tipo_estipulante_w = 'F') then
		'O contrato selecionado � individual/familiar n�o sendo permitido este tipo de vincula��o!'
	else*/
	
	if	(ie_tipo_estipulante_w = 'J') then
		select	ie_tipo_contratacao
		into	ie_tipo_contratacao_w
		from	pls_plano
		where	nr_sequencia	= nr_seq_plano_w;
		
		if	(ie_tipo_contratacao_w = 'I') then
			wheb_mensagem_pck.exibir_mensagem_abort( 244800, null );
		end if;
	end if;
	
	select	count(1)
	into	cont_w
	from	pls_contrato_plano
	where	nr_seq_contrato	= nr_seq_contrato_p
	and	nr_seq_plano	= nr_seq_plano_w;
	
	if	(cont_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort( 244801, null );
	end if;
	
	update	pls_tabela_preco
	set	nr_contrato	= nr_seq_contrato_p
	where	nr_sequencia	= nr_seq_tabela_p;
end if;

commit;

end pls_vincular_tabela_contrato;
/
