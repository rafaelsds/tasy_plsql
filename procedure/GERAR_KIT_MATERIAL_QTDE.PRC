create or replace
procedure gerar_kit_material_qtde (	cd_kit_material_p		in 	number,
			      	dt_atendimento_p		in	date,
			      	nr_prescricao_p		in	number,
			      	ie_auditoria_p		in	varchar2,
			      	cd_local_estoque_p	in	varchar2,
			      	nr_seq_proc_princ_p	in	varchar2,
					nr_seq_atepacu_p		in	number,
					ie_baixa_sem_estoque_p	in	varchar2,
					nm_usuario_p		in	varchar2,
					qt_kit_p			in	number,	
					ds_erro_p			out	varchar2,
					cd_material_p		in number)	is


dt_entrada_unidade_w		date;
nr_seq_w				number(10);
cd_convenio_w			number(10);
nr_atendimento_w		number(10);
cd_setor_atendimento_w	number(5);
cd_material_w			number(6); 
qt_material_w			number(9,3);
cd_unidade_medida_consumo_w	varchar2(30);
ie_via_aplicacao_w		varchar2(10);
cd_unidade_medida_consumo_ww	varchar2(30);
ie_via_aplicacao_ww		varchar2(10);
cd_categoria_w			varchar2(10);
nr_doc_convenio_w		varchar2(20);
ie_tipo_guia_w			varchar2(2);
ie_estoque_disp_w		varchar2(1);
cd_estabelecimento_w	number(5);
cd_senha_w				varchar2(20);
ie_gerar_item_dis_kit_w	varchar2(1);
ie_dispara_regra_lanc_kit_w	varchar2(1):= 'N';

cursor	c001	is
	select 	a.cd_material,
		a.qt_material,
		substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
		b.ie_via_aplicacao
	from	material b, componente_kit a 
	where 	(a.cd_material = b.cd_material)
	and 	a.cd_kit_material = cd_kit_material_p
	and 	a.ie_situacao = 'A'
	and 	b.ie_situacao = 'A'
	and	((a.cd_estab_regra is null) or (a.cd_estab_regra = cd_estabelecimento_w));

begin

select	a.dt_entrada_unidade,
	a.cd_setor_atendimento,
	a.nr_atendimento,
	b.cd_estabelecimento
into	dt_entrada_unidade_w,
	cd_setor_atendimento_w,
	nr_atendimento_w,
	cd_estabelecimento_w
from	atend_paciente_unidade a,
	atendimento_paciente b	
where	a.nr_atendimento = b.nr_atendimento
and	a.nr_seq_interno = nr_seq_atepacu_p;

obter_convenio_execucao(nr_atendimento_w, dt_atendimento_p, 
			cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);
			
select	nvl(obter_valor_param_usuario(67, 407, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),'N')
into	ie_gerar_item_dis_kit_w
from	dual;

select	nvl(max(ie_dispara_regra_lanc_kit), 'N')
into	ie_dispara_regra_lanc_kit_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_w;

open c001;
loop
	fetch c001 into
		cd_material_w,
		qt_material_w,
		cd_unidade_medida_consumo_w,
		ie_via_aplicacao_w;
	exit when c001%notfound;
	ie_estoque_disp_w := 'N';
	if (cd_local_estoque_p is not null) then 
		obter_disp_estoque(cd_material_w,
				   to_number(cd_local_estoque_p),
				   cd_estabelecimento_w,
				   0,	
				   qt_material_w, '',
				   ie_estoque_disp_w);
	end if;
	if (ie_baixa_sem_estoque_p = 'S') or (cd_local_estoque_p is null) or (ie_estoque_disp_w = 'S') then
	
		select 	material_atend_paciente_seq.nextval
		into	nr_seq_w
		from 	dual;

		insert into material_atend_paciente	(nr_sequencia,
					nr_atendimento,
					dt_atendimento,
					cd_material,
					cd_material_prescricao,
					ie_via_aplicacao,
					qt_material,
					cd_unidade_medida,
					nr_prescricao,
					nr_seq_proc_princ,
					cd_setor_atendimento,
					cd_local_estoque,
					qt_executada,
					cd_acao,
					vl_unitario,
					qt_ajuste_conta,
					ie_valor_informado,
					ie_guia_informada,
					ie_auditoria,
					nm_usuario_original,
					cd_situacao_glosa,
					dt_entrada_unidade,
					nr_seq_atepacu,
					dt_atualizacao,
					nm_usuario,
					cd_convenio,
					cd_categoria,
					nr_doc_convenio,
					ie_tipo_guia,
					cd_senha,
					cd_kit_material)
		values			(nr_seq_w,
					nr_atendimento_w,
					dt_atendimento_p,
					cd_material_w,
					cd_material_w,
					ie_via_aplicacao_w,
					qt_material_w * qt_kit_p,
					cd_unidade_medida_consumo_w,
					nr_prescricao_p,
					to_number(nr_seq_proc_princ_p),
					cd_setor_atendimento_w,
					cd_local_estoque_p,
					qt_material_w * qt_kit_p,
					1,
					0,
					0,
					'N',
					'N',
					ie_auditoria_p,
					nm_usuario_p,
					0,
					dt_entrada_unidade_w,
					nr_seq_atepacu_p,
					sysdate,
					nm_usuario_p,
					cd_convenio_w,
					cd_categoria_w,
					nr_doc_convenio_w,
					ie_tipo_guia_w,
					cd_senha_w,
					cd_kit_material_p);
		
		if	(nvl(ie_dispara_regra_lanc_kit_w,'N') = 'S') then
			Gerar_Lanc_Automatico_Mat(nr_atendimento_w, cd_local_estoque_p, 132, nm_usuario_p, nr_seq_w, null, null);
		end if;
		
		commit;
		atualiza_preco_material(nr_seq_w, nm_usuario_p);
	else
		ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(279199) || cd_material_w || WHEB_MENSAGEM_PCK.get_texto(279201) || chr(13);
	end if;
end loop;
close c001;

if	(ie_gerar_item_dis_kit_w = 'S')	then

	select 	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
		ie_via_aplicacao
	into	cd_unidade_medida_consumo_ww,
		ie_via_aplicacao_ww
	from	material
	where 	cd_material = cd_material_p
	and 	ie_situacao = 'A';

	ie_estoque_disp_w := 'N';
	if (cd_local_estoque_p is not null) then 
		obter_disp_estoque(cd_material_p,
				   to_number(cd_local_estoque_p),
				   cd_estabelecimento_w,
				   0,	
				   qt_kit_p, '',
				   ie_estoque_disp_w);
	end if;
	if (ie_baixa_sem_estoque_p = 'S') or (cd_local_estoque_p is null) or (ie_estoque_disp_w = 'S') then
	
		select 	material_atend_paciente_seq.nextval
		into	nr_seq_w
		from 	dual;

		insert into material_atend_paciente	(nr_sequencia,
					nr_atendimento,
					dt_atendimento,
					cd_material,
					cd_material_prescricao,
					ie_via_aplicacao,
					qt_material,
					cd_unidade_medida,
					nr_prescricao,
					nr_seq_proc_princ,
					cd_setor_atendimento,
					cd_local_estoque,
					qt_executada,
					cd_acao,
					vl_unitario,
					qt_ajuste_conta,
					ie_valor_informado,
					ie_guia_informada,
					ie_auditoria,
					nm_usuario_original,
					cd_situacao_glosa,
					dt_entrada_unidade,
					nr_seq_atepacu,
					dt_atualizacao,
					nm_usuario,
					cd_convenio,
					cd_categoria,
					nr_doc_convenio,
					ie_tipo_guia,
					cd_senha,
					cd_kit_material)
		values			(nr_seq_w,
					nr_atendimento_w,
					dt_atendimento_p,
					cd_material_p,
					cd_material_p,
					ie_via_aplicacao_ww,
					qt_kit_p,
					cd_unidade_medida_consumo_ww,
					nr_prescricao_p,
					to_number(nr_seq_proc_princ_p),
					cd_setor_atendimento_w,
					cd_local_estoque_p,
					qt_kit_p,
					1,
					0,
					0,
					'N',
					'N',
					ie_auditoria_p,
					nm_usuario_p,
					0,
					dt_entrada_unidade_w,
					nr_seq_atepacu_p,
					sysdate,
					nm_usuario_p,
					cd_convenio_w,
					cd_categoria_w,
					nr_doc_convenio_w,
					ie_tipo_guia_w,
					cd_senha_w,
					cd_kit_material_p);
		
		if	(nvl(ie_dispara_regra_lanc_kit_w,'N') = 'S') then
			Gerar_Lanc_Automatico_Mat(nr_atendimento_w, cd_local_estoque_p, 132, nm_usuario_p, nr_seq_w, null, null);
		end if;		
		
		commit;
		
		atualiza_preco_material(nr_seq_w, nm_usuario_p);
	else
		ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(279199) || cd_material_w || WHEB_MENSAGEM_PCK.get_texto(279201) || chr(13);
	end if;
	
end if;

end gerar_kit_material_qtde;
/
