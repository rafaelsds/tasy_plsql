CREATE OR REPLACE
PROCEDURE Tasy_Limpar_Paciente(
			ds_senha_p	Number,
			nr_pessoa_fisica_p	Number) IS 

nm_tabela_w		Varchar2(50);
nm_atributo_w		Varchar2(50);
ds_comando_w		Varchar2(2000);
ie_acao_w		Varchar2(1);
ds_set_update_w		Varchar2(255);
ds_senha_w		Number(15,0);
nr_pessoa_fisica_w		Number(15,0);
cd_pessoa_fisica_w	Varchar2(10);
ie_tipo_pessoa_w		Number(1);
w_erro_rotina_w		Number(5);
ie_limpar_w		Varchar2(1);
qt_reg_w			Number(15,0);
nr_seq_paciente_w		number(10);
nr_seq_pac_hc_w		number(10);

Cursor C01 is
select a.cd_pessoa_fisica,
	 a.ie_tipo_pessoa
from	 pessoa_fisica a
where	 not exists(select 1 from usuario b
		where b.cd_pessoa_fisica = a.cd_pessoa_fisica)
and	 not exists(select 1 from medico c
		where c.cd_pessoa_fisica = a.cd_pessoa_fisica)
and	 not exists(select 1 from comprador d
		where d.cd_pessoa_fisica = a.cd_pessoa_fisica);

Cursor C02 is
select nr_seq_paciente
from	 paciente_setor
where	 cd_pessoa_fisica = cd_pessoa_fisica_w;

Cursor C03 is
select nr_sequencia
from	 PACIENTE_HOME_CARE
where	 cd_pessoa_fisica = cd_pessoa_fisica_w;



BEGIN

select	max(28301 +
   campo_numerico(to_char(sysdate,'yyyy')) +
   campo_numerico(to_char(sysdate,'mm')) +
   campo_numerico(to_char(sysdate,'dd')))
into	ds_senha_w
from dual;

select	count(*)
into	nr_pessoa_fisica_w
from	pessoa_fisica;


if	(ds_senha_p = ds_senha_w) and
	(nr_pessoa_fisica_w = nr_pessoa_fisica_p) then
	BEGIN
	open c01;
	loop
	fetch c01 into 	
		cd_pessoa_fisica_w,
		ie_tipo_pessoa_w;
	exit when c01%notfound;
		BEGIN
		ie_limpar_w	:= 'S';

		/* Pessoa juridica */
		select 	count(*)
		into	qt_reg_w
		from	pessoa_juridica
		where	cd_pf_resp_tecnico = cd_pessoa_fisica_w;
		if	(qt_reg_w	 > 0) then
			ie_limpar_w	:= 'N';
		end if;

		/* Terceiros */
		select 	count(*)
		into	qt_reg_w
		from	terceiro
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		if	(qt_reg_w	 > 0) then
			ie_limpar_w	:= 'N';
		end if;

		/* Portador */
		select 	count(*)
		into	qt_reg_w
		from	portador
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		if	(qt_reg_w	 > 0) then
			ie_limpar_w	:= 'N';
		end if;

		if	(ie_limpar_w = 'S') then
			BEGIN

			open c02;
			loop
			fetch c02 	into 	
					nr_seq_paciente_w;
			exit when c02%notfound;
			begin
				begin
				delete from PACIENTE_ATEND_HEMODIALISE
				where	 nr_seq_paciente = nr_seq_paciente_w;
				exception when others then
					null;
				end;

				begin
				delete from PACIENTE_PROTOCOLO_MEDIC
				where	 nr_seq_paciente = nr_seq_paciente_w;
				exception when others then
					null;
				end;

				begin
				delete from PACIENTE_ATEND_INTERC
				where	 nr_seq_atendimento in
					 (select nr_seq_atendimento from paciente_atendimento
						where nr_seq_paciente = nr_seq_paciente_w);
				exception when others then
					null;
				end;

				begin
				delete from PACIENTE_ATEND_MEDIC
				where	 nr_seq_atendimento in
					 (select nr_seq_atendimento from paciente_atendimento
						where nr_seq_paciente = nr_seq_paciente_w);
				exception when others then
					null;
				end;

				begin
				delete from PACIENTE_ATEND_RESTRICAO
				where	 nr_seq_atendimento in
					 (select nr_seq_atendimento from paciente_atendimento
						where nr_seq_paciente = nr_seq_paciente_w);
				exception when others then
					null;
				end;

				begin
				delete from PACIENTE_ATENDIMENTO
				where	 nr_seq_paciente = nr_seq_paciente_w;
				exception when others then
					null;
				end;
			end;
			end loop;
			close c02;


			open c03;
			loop
			fetch c03 	into 	
					nr_seq_pac_hc_w;
			exit when c03%notfound;
			begin
				begin
				delete from PACIENTE_HC_ATEND
				where	 nr_seq_paciente = nr_seq_pac_hc_w;
				exception when others then
					null;
				end;

				begin
				delete from PACIENTE_HC_DOENCA
				where	 nr_seq_paciente = nr_seq_pac_hc_w;
				exception when others then
					null;
				end;

				begin
				delete from PACIENTE_HC_EQUIPE
				where	 nr_seq_paciente = nr_seq_pac_hc_w;
				exception when others then
					null;
				end;

				begin
				delete from PACIENTE_HOME_CARE
				where	 nr_sequencia = nr_seq_pac_hc_w;
				exception when others then
					null;
				end;

			end;
			end loop;
			close c03;


			/* limpeza dados do paciente */
			begin
			delete from PACIENTE_ALERGIA
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PACIENTE_ANTEC_CLINICO
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PACIENTE_ESPERA
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PACIENTE_HABITO_VICIO
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;


			begin
			delete from PACIENTE_MEDIC_USO
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PACIENTE_SETOR
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PACIENTE_VACINA
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;


			/* limpeza dados da pessoa fisica */
			begin
			delete from PESSOA_FISICA_CONTA
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PESSOA_FISICA_CONTA_CTB
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PESSOA_FISICA_DELEGACAO
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PESSOA_FISICA_DUPLIC
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PESSOA_FISICA_ENT_ASSOC
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PESSOA_FISICA_HOBBY
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PESSOA_FISICA_IDIOMA
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PESSOA_FISICA_NOMEACAO
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			begin
			delete from PESSOA_FISICA_TITULO
			where	 cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;



			/* Limpar avalia��es do paciente */
			begin
			delete from med_avaliacao_paciente
			where cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;


			/* Limpar endere�os do paciente */
			begin
			delete from compl_pessoa_fisica
			where cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;

			/* Limpar paciente */
			begin
			delete from pessoa_fisica
			where cd_pessoa_fisica = cd_pessoa_fisica_w;
			exception when others then
				null;
			end;
			commit;
			END;
		end if;
		END;
	end loop;
	close c01;
	commit;
	END;
end if;

END Tasy_Limpar_Paciente;
/