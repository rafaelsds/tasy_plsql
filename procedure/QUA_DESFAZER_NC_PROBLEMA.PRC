create or replace
procedure qua_desfazer_nc_problema(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 

nr_seq_nao_conform_w	number(10,0);

begin
select	nr_seq_rnc
into	nr_seq_nao_conform_w
from	qua_analise_problema
where   nr_sequencia    = nr_sequencia_p;

if	(nvl(nr_seq_nao_conform_w,0) > 0) then
	begin
	update	qua_analise_problema
	set	nr_seq_rnc	= null
	where	nr_sequencia	= nr_sequencia_p;

	delete from qua_nao_conformidade
	where nr_sequencia = nr_seq_nao_conform_w;

	exception when others then
		/*(-20011,'Ocorreu um problema ao desvincular a n�o conformidade' || chr(10) || 	Sqlerrm);*/
		wheb_mensagem_pck.exibir_mensagem_abort(263171,'DS_ERRO=' || Sqlerrm);
	end;
	
	commit;
end if;
	
end qua_desfazer_nc_problema;
/
