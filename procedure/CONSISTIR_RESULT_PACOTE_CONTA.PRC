create or replace 
procedure Consistir_Result_Pacote_Conta(	nr_interno_conta_p 	in 	number,
							ds_erro_p		in out	varchar2) is


pr_prejuizo_w		number(6,2);
nr_seq_proc_pacote_w	number(10);

pr_pacote_w		number(15,2);

cursor c01 is
select nr_seq_procedimento
from	Atendimento_Pacote a,
	procedimento_paciente b
where a.nr_seq_procedimento	= b.nr_sequencia
  and a.nr_seq_procedimento	is not null
  and b.nr_interno_conta	= nr_interno_conta_p;

begin

select nvl(max(b.pr_perm_prej_pacote),0)
into pr_prejuizo_w
from parametro_faturamento b,
	conta_paciente a
where a.cd_estabelecimento = b.cd_estabelecimento
  and a.nr_interno_conta = nr_interno_conta_p;

open c01;
loop
	fetch c01 into nr_seq_proc_pacote_w;
	exit when c01%notfound;

	select	((dividir(sum(nvl(vl_pacote,0)), sum(nvl(vl_original,0))) * 100)-100)
	into	pr_pacote_w
	from (	select	a.vl_procedimento vl_pacote, 
			nvl(b.vl_procedimento,0) vl_original
		from	proc_paciente_valor b,
			procedimento_paciente a
		where a.nr_sequencia	= b.nr_seq_procedimento(+)
		  and 1			= b.ie_tipo_valor(+)
		  and a.nr_seq_proc_pacote <> a.nr_sequencia
		  and nvl(a.nr_seq_proc_pacote,0) = nr_seq_proc_pacote_w
		  and a.nr_interno_conta = nr_interno_conta_p 
		union all
		select	a.vl_material,
			a.vl_tabela_original vl_original
		from	material_atend_paciente a
		where nvl(a.nr_seq_proc_pacote,0) = nr_seq_proc_pacote_w
		  and a.nr_interno_conta = nr_interno_conta_p);

	dbms_output.put_line(nr_interno_conta_p);

	dbms_output.put_line(pr_pacote_w);

	
	if	(pr_pacote_w < 0) and
		(abs(pr_pacote_w) > pr_prejuizo_w) then
		ds_erro_p := ds_erro_p || '2092 ';
		exit;
	end if;	
end loop;
close c01;

end;
/
