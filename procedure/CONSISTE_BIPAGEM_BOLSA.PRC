create or replace
procedure Consiste_Bipagem_Bolsa
		(ds_identificacao_bolsa_p	in number,
		 nr_cirurgia_p 				in number,
		 nr_seq_derivado_p          in number,
		 ds_erro_p		out	varchar2) is
		 
ds_erro_w			varchar2(255) := '';
ie_existe_w         varchar2(1) := 'N';
ie_bolsa_bipada_w   varchar2(1) := 'N';
ie_pertence_cirurgia_w varchar2(1) := 'N';
nr_seq_inutil_w     number(10);
ds_identificacao_bolsa_w	number(20) := ds_identificacao_bolsa_p;

begin

if	(obtain_user_locale(wheb_usuario_pck.get_nm_usuario) = 'de_DE') then
	
	select	nvl(max(nr_sequencia),to_char(ds_identificacao_bolsa_p))
	into	ds_identificacao_bolsa_w
	from	san_producao
	where	nr_sangue = to_char(ds_identificacao_bolsa_p);

end if;
	
if	(ds_identificacao_bolsa_w is not null) then	
	/*Consistencia que impede de bipar uma bolsa inexistente*/
	SELECT 	NVL(MAX('S'),'N')
	INTO   	ie_existe_w
	FROM   	san_producao
	WHERE   nr_sequencia = ds_identificacao_bolsa_w;
	
	if (ie_existe_w = 'N') then	
		/*Mensagem: Identifica��o da bolsa n�o encontrada!*/
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(354397,null);
		Wheb_mensagem_pck.exibir_mensagem_abort(354397);
	end if;
	
	/*Consistencia de que impede de bipar bolsa inutilizada.*/
	SELECT 	MAX(NR_SEQ_INUTIL)
	INTO   	nr_seq_inutil_w
	FROM   	san_producao
	WHERE   nr_sequencia = ds_identificacao_bolsa_w;
	
	if (nr_seq_inutil_w is not null) then
		/*Mensagem: Esta bolsa est� inutilizada!*/
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(354405,null);		
		Wheb_mensagem_pck.exibir_mensagem_abort(354405);
	end if;
	
	/*Consistencia que impede de bipar duas vezes a mesma bolsa*/
	SELECT 	NVL(MAX('S'),'N')
	INTO   	ie_bolsa_bipada_w
	FROM   	CIRURGIA_AGENTE_ANEST_OCOR
	WHERE  TRIM(DS_IDENTIFICACAO_BOLSA) = TO_CHAR(ds_identificacao_bolsa_w)
	AND 	NVL(ie_situacao,'A')  = 'A';
		
	if (ie_bolsa_bipada_w = 'S') then
		/*Mensagem: N�o � poss�vel informar esta bolsa de sangue! A mesma j� foi registrada.*/
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(354407,null);		
		Wheb_mensagem_pck.exibir_mensagem_abort(354407);
	end if;
			
	/*Consistencia que impede bipar a bolsa de outra cirurgia*/
	SELECT    NVL(MAX('S'),'N')
	INTO   	  ie_pertence_cirurgia_w
	FROM      san_transfusao b,
			  san_producao c
	WHERE     b.nr_sequencia     = c.nr_seq_transfusao
	AND       NVL(b.ie_status,'A')  = 'A'
	AND       b.nr_cirurgia    = nr_cirurgia_p
	AND 	  c.nr_sequencia   = ds_identificacao_bolsa_w;
	
	if (ie_pertence_cirurgia_w = 'N') then
		/*Mensagem: N�o � poss�vel informar esta bolsa de sangue! A mesma n�o pertence a esta cirurgia.*/
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(354408,null);		
		Wheb_mensagem_pck.exibir_mensagem_abort(354408);
	end if;
	
	/*Consistencia que impede bipar uma bolsa em um derivado diferente*/
	SELECT   NVL(MAX('S'),'N')
	INTO   	 ie_existe_w
	FROM     san_transfusao b,
			 san_producao c
	WHERE    b.nr_sequencia  = c.nr_seq_transfusao
	AND      b.nr_cirurgia   = nr_cirurgia_p
	AND      c.nr_sequencia  = ds_identificacao_bolsa_w
	AND      nr_seq_derivado = nr_seq_derivado_p;
	
	if (ie_existe_w = 'N') then
		/*Mensagem: N�o � poss�vel informar esta bolsa de sangue! A mesma n�o pertence a esta cirurgia.*/
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(354811,null);		
		Wheb_mensagem_pck.exibir_mensagem_abort(354811);
	end if;

end if;

ds_erro_p	:= ds_erro_w;

end Consiste_Bipagem_Bolsa;
/