create or replace
procedure gerar_resumo_consulta_pep (
		cd_pessoa_fisica_p	varchar2,
		nm_procedure_p		varchar2,
		nm_usuario_p		varchar2) is

nr_seq_laudo_w	number(10,0);
ds_resumo_w	long;
ds_laudo_w	long;
ds_consulta_w	long;

/*cursor c01 is
select	nr_seq_laudo,
	ds_resumo
from 	w_oft_resumo_consulta
where 	cd_pessoa_fisica = cd_pessoa_fisica_p
and 	nm_usuario = nm_usuario_p
order by
	nr_sequencia;*/

begin
if	(cd_pessoa_fisica_p is not null) and
	(nm_procedure_p is not null) then
	begin
	exec_sql_dinamico('','begin gerar_resumo_oftalmo_pkg.' || nm_procedure_p || '(' || chr(39) || cd_pessoa_fisica_p || chr(39) || ','
											|| chr(39) || nm_usuario_p || chr(39) || ','
											|| 'null' || '); end;');

	/*open c01;
	loop
	fetch c01 into	nr_seq_laudo_w,
			ds_resumo_w;
	exit when c01%notfound;
		begin
		if	(nr_seq_laudo_w is not null) then
			begin
			select	ds_laudo
			into	ds_laudo_w
			from	laudo_paciente
			where	nr_sequencia = nr_seq_laudo_w;

			ds_consulta_w := ds_consulta_w || ds_laudo_w;
			end;
		else
			begin
			ds_consulta_w := ds_consulta_w || ds_resumo_w;
			end;
		end if;
		end;
	end loop;
	close c01;

	exec_sql_dinamico('Tasy','truncate table w_valor_long');
	insert into w_valor_long values (ds_consulta_w);
	*/
	end;
end if;
commit;
end gerar_resumo_consulta_pep;
/