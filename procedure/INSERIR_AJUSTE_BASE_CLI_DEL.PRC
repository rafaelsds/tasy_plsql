create or replace
procedure inserir_ajuste_base_cli_del(nm_usuario_p		Varchar2) is

qt_registros_w		number(10);
nr_seq_ajuste_base_w	Varchar2(10);
ds_consulta_w   	varchar2(512);
nm_responsavel_w	varchar2(60);
ie_fase_w		varchar2(1);
ds_motivo_w             varchar2(2000);
ds_comando_w            clob;
c02              Integer;
retorno_w        Number(5);

begin

begin
	execute immediate 'drop table ajuste_versao_base_w';
exception
when others then
	qt_registros_w:= 0;
end;

execute immediate	'create table ajuste_versao_base_w ' ||
			' as ' ||
			'	(select	a.ie_fase, ' ||
			' 		a.nr_seq_ajuste_base, ' ||
			' 		a.nm_responsavel, ' ||
			' 		a.ds_motivo, ' ||
			'		to_lob(a.ds_comando) ds_comando ' ||
			' 	from	ajuste_versao_base_longo a ) ';

delete from ajuste_versao_base_longo;
commit;

ds_consulta_w := ' select a.ie_fase, '    ||
			' a.nr_seq_ajuste_base, ' ||
			' a.nm_responsavel, ' ||
			' a.ds_motivo, ' ||
			' a.ds_comando ' ||
		' from ajuste_versao_base_w a ';
		
C02 := DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(C02, ds_consulta_w, dbms_sql.Native);
DBMS_SQL.DEFINE_COLUMN(C02,1,ie_fase_w,1);
DBMS_SQL.DEFINE_COLUMN(C02,2,nr_seq_ajuste_base_w,10);
DBMS_SQL.DEFINE_COLUMN(C02,3,nm_responsavel_w,60);
DBMS_SQL.DEFINE_COLUMN(C02,4,ds_motivo_w,2000);
DBMS_SQL.DEFINE_COLUMN(C02,5,ds_comando_w);
retorno_w := DBMS_SQL.execute(C02);	

while   ( DBMS_SQL.FETCH_ROWS(C02) > 0 ) loop
        DBMS_SQL.COLUMN_VALUE(C02, 1, ie_fase_w);
	DBMS_SQL.COLUMN_VALUE(C02, 2, nr_seq_ajuste_base_w);
	DBMS_SQL.COLUMN_VALUE(C02, 3, nm_responsavel_w);
        DBMS_SQL.COLUMN_VALUE(C02, 4, ds_motivo_w);
	DBMS_SQL.COLUMN_VALUE(C02, 5, ds_comando_w);
	begin
	
	select	count(*)
	into	qt_registros_w
	from	ajuste_versao_base_cliente
	where	nr_sequencia = nr_seq_ajuste_base_w;
	
	if (qt_registros_w = 0) then
	
		insert into ajuste_versao_base_cliente (nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ds_comando,
						nm_responsavel,
						ds_motivo,
						ie_fase,
						ie_compila)
					values	(nr_seq_ajuste_base_w,
						nm_usuario_p,
						sysdate,
						sysdate,
						nm_usuario_p,
						ds_comando_w,
						nm_responsavel_w,
						ds_motivo_w,
						ie_fase_w,
						'N');
		commit;
		
	end if;
	
	end;
END LOOP;
DBMS_SQL.CLOSE_CURSOR(C02);

end inserir_ajuste_base_cli_del;
/
