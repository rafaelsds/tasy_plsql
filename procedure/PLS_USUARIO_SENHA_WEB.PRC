create or replace
procedure pls_usuario_senha_web(
		ds_senha_p	varchar2,
		nr_sequencia_p	varchar2) is 

begin

update	pls_usuario_web
set	ds_senha = ds_senha_p
where	nr_sequencia = nr_sequencia_p;

commit;

end pls_usuario_senha_web;
/