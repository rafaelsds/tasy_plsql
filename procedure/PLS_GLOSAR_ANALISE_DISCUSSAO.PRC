create or replace procedure pls_glosar_analise_discussao
			(nr_sequencia_p		    number,
			qt_aceita_p		        number,
			vl_aceito_p		        number,
			qt_negada_p		        number,
			vl_negado_p		        number,
			nr_seq_grupo_p		    number,
			nr_seq_mot_negada_p	  number,
			ds_parecer_glosas_p	  varchar2,
			nm_usuario_p		      varchar2,
			cd_estabelecimento_p	varchar2,
			ds_observacao_p       varchar2 default null) is
            

qt_aceita_w		      number(12,4);
vl_aceito_w		      number(15,2);
qt_negada_w		      number(12,4);
vl_negado_w		      number(15,2);
qt_recurso_w		    number(12,4);
vl_recurso_w		    number(15,2);
ds_observacao_w		  varchar2(4000);
nr_seq_conta_w		  number(10);
nr_seq_analise_w	  number(10);
ie_tipo_item_w		  varchar2(1);
ds_item_w		        varchar2(255);
nr_identeificador_w	number(10);
qt_negada_alt_w		  number(12,4);
vl_negado_alt_w	  	number(15,2);
vl_calculo_w		    number(15,2);
vl_contestado_w		  number(15,2);
qt_contestada_w		  number(12,4);
nr_seq_item_w		    number(10);

begin
select	nvl(qt_aceita,0),
	nvl(vl_aceito,0),
	nvl(qt_negada,0),
	nvl(vl_negado,0),
	nvl(qt_recurso,0),
	nvl(vl_recurso,0),
	nvl(vl_contestado,0),
	nvl(qt_contestada,0),
	nr_seq_item
into	qt_aceita_w,
	vl_aceito_w,
	qt_negada_w,
	vl_negado_w,
	qt_recurso_w,
	vl_recurso_w,
	vl_contestado_w,
	qt_contestada_w,
	nr_seq_item_w
from	w_pls_discussao_item
where	nr_sequencia = nr_sequencia_p;

/* Quando alterado apenas a quantidade aceita */
if	(qt_aceita_p > 0) then
	vl_calculo_w := (vl_contestado_w * qt_aceita_p);
	vl_aceito_w := dividir_sem_round(vl_calculo_w,qt_contestada_w);
else
/* Quando alterado apenas o valor aceito */
	qt_negada_alt_w := qt_contestada_w - qt_aceita_p;
	vl_negado_alt_w := vl_contestado_w - vl_aceito_p;
	vl_aceito_w := vl_aceito_p;
end if;

if	(qt_aceita_p + qt_negada_alt_w > qt_contestada_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(114566);
elsif	(vl_aceito_p + vl_negado_alt_w > vl_contestado_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(114567);
end if;            

if	(qt_aceita_w <> qt_aceita_p) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Quantidade aceita:'||chr(13)||chr(10)||
				chr(9)||'Anterior: '||qt_aceita_w||' - Modificada: '||qt_aceita_p||chr(13)||chr(10);
end if;

if	(vl_aceito_w <> vl_aceito_p) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Valor aceito:'||chr(13)||chr(10)||
				chr(9)||'Anterior: '||vl_aceito_w||' - Modificada: '||vl_aceito_p||chr(13)||chr(10);
end if;

if	(qt_negada_w <> qt_negada_alt_w) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Quantidade negada:'||chr(13)||chr(10)||
				chr(9)||'Anterior: '||qt_negada_w||' - Modificada: '||qt_negada_p||chr(13)||chr(10);
end if;

if	(vl_negado_w <> vl_negado_alt_w) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Valor negado:'||chr(13)||chr(10)||
				chr(9)||'Anterior: '||vl_negado_w||' - Modificada: '||vl_negado_p||chr(13)||chr(10);
end if;

select	max(nr_seq_conta),
	max(nr_seq_analise),
	max(ie_tipo_item),
	max(ds_item),
	max(nr_identificador)
into	nr_seq_conta_w,
	nr_seq_analise_w,
	ie_tipo_item_w,
	ds_item_w,
	nr_identeificador_w
from	w_pls_discussao_item
where	nr_sequencia = nr_sequencia_p;

         if(ds_observacao_p is not null) then 

            insert into pls_conta_observacao_a500	/* Observacao*/
                (nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                ds_observacao,
                nr_seq_conta,
                nr_seq_conta_proc,
                nr_seq_conta_mat)
                
                
            values	(pls_conta_observacao_a500_seq.nextval,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                ds_observacao_p,
                nr_seq_conta_w,
                decode(ie_tipo_item_w, 'M', null, nr_seq_item_w),
                decode(ie_tipo_item_w, 'M', nr_seq_item_w, null));
            
            end if;
            
if	(nvl(ds_observacao_w,'X') <> 'X') then
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_w,27,nr_sequencia_p,ie_tipo_item_w, null,null,
				'Modificado pelo auditor '||obter_nome_usuario(nm_usuario_p)||'.'||chr(13)||chr(10)||
				'Item '||nr_identeificador_w||' - '||ds_item_w||'.'||chr(13)||chr(10)||ds_observacao_w,
				nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);
else
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_w,27,nr_sequencia_p,ie_tipo_item_w, null,null,
				'Aceito parcialmente.',nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);
end if;

update	w_pls_discussao_item
set	qt_aceita		= nvl(qt_aceita_p,qt_aceita_w),
	vl_aceito		= nvl(vl_aceito_w,0),
	qt_negada		= nvl(qt_negada_alt_w,0),
	vl_negado		= nvl(vl_negado_alt_w,0),
	ie_situacao		= 'P'
where	nr_sequencia		= nr_sequencia_p;

select	nvl(vl_aceito,0)
into	vl_aceito_w
from	w_pls_discussao_item
where	nr_sequencia		= nr_sequencia_p;

update	w_pls_discussao_item
set	ie_situacao		= 'P'
where	nr_seq_item		= nr_seq_item_w
and	nr_seq_partic_proc is not null;

if	(vl_aceito_w = 0) then
	update	w_pls_discussao_item
	set	ie_situacao	= 'N'
	where	nr_sequencia	= nr_sequencia_p;

	update	w_pls_discussao_item
	set	ie_situacao	= 'N'
	where	nr_seq_item	= nr_seq_item_w
	and	nr_seq_partic_proc is not null;
end if;

/* Gerar o parecer dos itens glosados */
pls_parecer_glosar_discussao(nr_sequencia_p,nr_seq_mot_negada_p,ds_parecer_glosas_p,nr_seq_grupo_p,nm_usuario_p,cd_estabelecimento_p);

commit;

end pls_glosar_analise_discussao;
/