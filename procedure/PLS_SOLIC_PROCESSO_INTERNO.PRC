create or replace
procedure pls_solic_processo_interno
			(	nr_seq_analise_p	Number,
				nr_seq_proc_interno_p	Number,
				nm_usuario_p		Varchar2) is 

ds_processo_interno_w		Varchar2(255);
nr_seq_requisicao_w		Number(10);
nr_seq_guia_w			Number(10);
nr_seq_execucao_w		Number(10);
nr_seq_grupo_atual_w		Number(10);
nr_seq_grupo_auditor_w		number(10);

begin

update	pls_auditoria
set	ie_status		= 'AP',
	nr_seq_proc_interno	= nr_seq_proc_interno_p,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_analise_p;

begin
	select	nr_seq_requisicao,
		nr_seq_guia,
		nr_seq_execucao
	into	nr_seq_requisicao_w,
		nr_seq_guia_w,
		nr_seq_execucao_w
	from	pls_auditoria
	where	nr_sequencia	= nr_seq_analise_p;
exception
when others then
	nr_seq_requisicao_w	:= 0;
	nr_seq_guia_w		:= 0;
	nr_seq_execucao_w	:= 0;
end;

begin
	select	substr(ds_processo_interno,1,255)
	into	ds_processo_interno_w
	from	pls_analise_proc_interno
	where	nr_sequencia	= nr_seq_proc_interno_p;
exception
when others then
	ds_processo_interno_w	:= 'X';
end;

select	pls_obter_grupo_analise_atual(nr_seq_analise_p)
into	nr_seq_grupo_atual_w
from	dual;

select	max(nr_seq_grupo)
into	nr_seq_grupo_auditor_w
from	pls_auditoria_grupo
where	nr_sequencia = nr_seq_grupo_atual_w;

if	(nvl(ds_processo_interno_w,'X')	<> 'X') then
	if	(nvl(nr_seq_requisicao_w,0)	<> 0) then
		pls_req_gravar_hist_analise(	nr_seq_requisicao_w, nr_seq_grupo_auditor_w, 'L', 'O usu�rio '||nm_usuario_p||' solicitou o processo interno '||ds_processo_interno_w||
						' em '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'), null, nm_usuario_p);
	elsif	(nvl(nr_seq_guia_w,0)	<> 0) then
		pls_guia_gravar_hist_analise(	nr_seq_guia_w, nr_seq_grupo_auditor_w, 2, 'O usu�rio '||nm_usuario_p||' solicitou o processo interno '||ds_processo_interno_w||
						' em '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'), null, nm_usuario_p);
	elsif	(nvl(nr_seq_execucao_w,0)	<> 0) then
		select	max(nr_seq_requisicao)
		into	nr_seq_requisicao_w
		from	pls_execucao_requisicao
		where	nr_sequencia	= nr_seq_execucao_w;
		
		if	(nvl(nr_seq_requisicao_w,0)	<> 0) then
			pls_req_gravar_hist_analise(	nr_seq_requisicao_w, nr_seq_grupo_auditor_w, 'L', 'O usu�rio '||nm_usuario_p||' solicitou o processo interno '||ds_processo_interno_w||
							' em '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'), nr_seq_execucao_w, nm_usuario_p);
		end if;
	end if;
end if;

commit;

end pls_solic_processo_interno;
/