CREATE OR REPLACE PROCEDURE inserir_aval_paciente( 
            cd_medico_p IN VARCHAR2,
            cd_pessoa_fisica_p IN varchar2,
            nr_seq_tipo_avaliacao_p IN NUMBER,
            nm_usuario_p IN VARCHAR2,
            nr_seq_proc_p IN NUMBER,
            nr_seq_aval_out OUT number) IS

nr_seq_aval_w NUMBER;

begin

SELECT med_avaliacao_paciente_seq.NEXTVAL INTO nr_seq_aval_w FROM dual;

insert into med_avaliacao_paciente( 
            nr_sequencia,
            cd_medico,
            cd_pessoa_fisica, 
            nr_seq_tipo_avaliacao, 
            dt_atualizacao, 
            dt_avaliacao,
            nm_usuario
            ) values ( 
            nr_seq_aval_w,
            cd_medico_p,
            cd_pessoa_fisica_p,
            nr_seq_tipo_avaliacao_p,
            sysdate,
            sysdate,
            nm_usuario_p);

update proc_pac_descricao
set nr_seq_avaliacao = nr_seq_aval_w
where nr_sequencia = nr_seq_proc_p;

nr_seq_aval_out := nr_seq_aval_w;

end inserir_aval_paciente;
/