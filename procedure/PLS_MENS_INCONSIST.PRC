create or replace
procedure pls_mens_inconsist
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2) is 
			
nr_seq_segurado_w		pls_mensalidade_segurado.nr_seq_segurado%type;
dt_referencia_w			pls_mensalidade.dt_referencia%type;
nr_seq_contrato_w		pls_mensalidade_segurado.nr_seq_contrato%type;
qt_idade_w			pls_mensalidade_segurado.qt_idade%type;
ie_preco_vidas_contrato_w	pls_tabela_preco.ie_preco_vidas_contrato%type;
ie_calculo_vidas_w		pls_tabela_preco.ie_calculo_vidas%type;
nr_seq_tabela_w			pls_segurado.nr_seq_tabela%type;
nr_seq_titular_w		pls_segurado.nr_seq_titular%type;
nr_seq_preco_w			pls_plano_preco.nr_sequencia%type;
vl_preco_atual_w		pls_plano_preco.vl_preco_atual%type;
nm_beneficiario_w		varchar2(200);
ie_grau_parentesco_w		varchar2(2);
qt_vidas_w			number(10)	:= 0;
qt_idade_ww			pls_mensalidade_segurado.qt_idade%type;
qt_idade_limite_w		pls_parametros.qt_idade_limite%type; 
qt_tempo_limite_w		pls_parametros.qt_tempo_limite%type;
cd_estabelecimento_w		number(10)	:= 1;

Cursor C01 is
	select	b.nr_seq_segurado,
		a.dt_referencia,
		b.nr_seq_contrato, 
		b.qt_idade,
		nvl(substr(pls_obter_garu_dependencia_seg(b.nr_seq_segurado,'C'),1,2),'X'),
		c.nr_seq_preco		
	from 	pls_mensalidade a,
		pls_mensalidade_segurado b,
		pls_segurado_preco c
	where 	a.nr_seq_lote = nr_seq_lote_p
	and   	a.nr_sequencia = b.nr_seq_mensalidade 
	and   	b.nr_seq_segurado_preco = c.nr_sequencia
	and 	(((nvl(qt_tempo_limite_w,0) = 0) or (trunc(months_between(trunc(sysdate,'month'),pls_obter_dados_segurado(b.nr_seq_segurado,'D'))/12) < qt_tempo_limite_w))
	and 	((nvl(qt_idade_limite_w,0) = 0) or (b.qt_idade < qt_idade_limite_w)));
	
cursor c02 is	
	select	nr_sequencia,
		nvl(vl_preco_atual,0)
	from	pls_plano_preco
	where	qt_idade_w	>= qt_idade_inicial
	and	qt_idade_w	<= qt_idade_final
	and	nr_seq_tabela	= nr_seq_tabela_w
	and	nvl(ie_grau_titularidade,ie_grau_parentesco_w)	= ie_grau_parentesco_w
	and	qt_vidas_w between nvl(qt_vidas_inicial,qt_vidas_w) and nvl(qt_vidas_final,qt_vidas_w)
	and 	nr_sequencia <> nr_seq_preco_w	
	order	by	nvl(ie_grau_titularidade,' ');	
begin

delete	w_pls_incons_mensalidade 
where	nm_usuario = nm_usuario_p;

cd_estabelecimento_w	:= obter_estabelecimento_ativo;

select	qt_idade_limite, 
	qt_tempo_limite 
into	qt_idade_limite_w, 
	qt_tempo_limite_w
from  	pls_parametros 
where 	cd_estabelecimento = cd_estabelecimento_w;

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w,
	dt_referencia_w,
	nr_seq_contrato_w, 
	qt_idade_w,
	ie_grau_parentesco_w,
	nr_seq_preco_w;
exit when C01%notfound;
	begin
	select	a.nr_seq_tabela,
		a.nr_seq_titular
	into	nr_seq_tabela_w,
		nr_seq_titular_w
	from 	pls_segurado a 
	where 	a.nr_sequencia = nr_seq_segurado_w;
	
	select	nvl(ie_preco_vidas_contrato,'N'),
		nvl(ie_calculo_vidas,'A')
	into	ie_preco_vidas_contrato_w,
		ie_calculo_vidas_w
	from	pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_w;
	exception
	when others then
		ie_calculo_vidas_w	:= 'A';
	end;

	if	(ie_preco_vidas_contrato_w = 'S') then
		if	(ie_calculo_vidas_w = 'A') then
			select	count(*)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (trunc(dt_rescisao,'mm') > dt_referencia_w));
		elsif	(ie_calculo_vidas_w = 'T') then
			select	count(*)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	nr_seq_titular is null
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (trunc(dt_rescisao,'mm') > dt_referencia_w));
		elsif	(ie_calculo_vidas_w = 'D') then
			select	count(*)
			into	qt_vidas_w
			from	pls_segurado
			where	nr_seq_contrato = nr_seq_contrato_w
			and	nr_seq_titular is not null
			and	dt_liberacao is not null
			and	((dt_rescisao is null) or (trunc(dt_rescisao,'mm') > dt_referencia_w));
		elsif	(ie_calculo_vidas_w = 'TD') then
			select	count(*)
			into	qt_vidas_w
			from	pls_segurado a
			where	a.nr_seq_contrato = nr_seq_contrato_w
			and	a.dt_liberacao is not null
			and	((a.dt_rescisao is null) or (trunc(a.dt_rescisao,'mm') > dt_referencia_w))
			and	((nr_seq_titular is null) or ((nr_seq_titular is not null) and ((select	count(*)
												from	grau_parentesco x
												where	x.nr_sequencia = a.nr_seq_parentesco
												and	x.ie_tipo_parentesco = '1') > 0)));
		elsif	(ie_calculo_vidas_w = 'F') then
			qt_vidas_w	:= nvl(pls_obter_qt_vida_benef(nr_seq_segurado_w,nr_seq_titular_w,dt_referencia_w),0);
		end if;
	else
		qt_vidas_w	:= 1;
	end if;
	
	qt_idade_ww	 	:= pls_obter_idade_segurado (nr_seq_segurado_w, dt_referencia_w, 'A');
	nm_beneficiario_w	:= pls_obter_dados_segurado (nr_seq_segurado_w, 'N');

	open c02;
	loop
	fetch c02 into
		nr_seq_preco_w,
		vl_preco_atual_w;
	exit when c02%notfound;
		begin
		insert 	into w_pls_incons_mensalidade
			(nr_sequencia, nr_seq_segurado, nm_beneficiario, qt_idade_mens, 
			qt_idade_atual, vl_tabela, nm_usuario, dt_atualizacao)
		values	(w_pls_incons_mensalidade_seq.nextval, nr_seq_segurado_w, nm_beneficiario_w, qt_idade_w, 
			qt_idade_ww, vl_preco_atual_w, nm_usuario_p, sysdate);		
		end;		
	end loop;
	close c02;		
end loop;
close C01;

commit;

end pls_mens_inconsist;
/

