create or replace
procedure atualizar_status_reg_barras(	nr_prescricao_p		number,
			nr_seq_prescr_p		number,
			ie_status_p		number,
			ie_status_atual_p		number,
			ds_sql_motivo_alt_p	varchar2,
			ds_sql_item_prescr_p	varchar2,
			ie_status_amostra_p	varchar2,
			ie_cosiste_liberacao_p	varchar2,
			ie_exame_amostra_p	varchar2,
			nm_usuario_p		varchar2) is 

					
nr_amostra_w		number(10);	
qt_exames_sem_result_w	number(10);
ie_exame_etapa_amostra_w	varchar2(1);	
ds_comando_update_w	varchar2(4000);
ds_sep_bv_w		varchar2(50);
					
begin

ds_sep_bv_w	:= obter_separador_bv;

select 	nr_seq_prescr_proc_mat
into	nr_amostra_w
from 	prescr_proc_mat_item
where 	nr_prescricao = nr_prescricao_p
and   	nr_seq_prescr = nr_seq_prescr_p;

select 	nvl(max('S'),'N')
into	ie_exame_etapa_amostra_w
from 	dual
where 	obter_se_contido(ie_status_p,ie_status_amostra_p) = 'S';

if	(ie_status_p = 40) then
  
	if  	(ie_cosiste_liberacao_p = 'S') then

		select 	count(*)
		into	qt_exames_sem_result_w
		from 	prescr_procedimento c
		where 	nr_prescricao 	= nr_prescricao_p
		and 	ie_status_atend = ie_status_atual_p
		and 	nr_sequencia 	= nr_seq_prescr_p
		and not exists (select 	1 
				from 	result_laboratorio x 
				where 	c.nr_prescricao = x.nr_prescricao 
				and 	c.nr_sequencia 	= x.nr_seq_prescricao);
										   
		if 	(qt_exames_sem_result_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(282249);
		end if;  
			  
	end if;
	       
	ds_comando_update_w :=	' update	prescr_procedimento c ' ||
				' set 		ie_status_atend = :ie_status_p, ' ||
				'		nm_usuario 	= :nm_usuario_p, ' ||
				'		dt_atualizacao 	= sysdate, ' ||
				'		nr_seq_motivo_alt_status = null ' ||
				' where  	nr_prescricao 	= :nr_prescricao_p ' ||
				' and    	ie_status_atend = :ie_status_atual_p ' ||
				' and    	nr_sequencia 	= :nr_seq_prescr_p ';
	
	
	if  	(ie_cosiste_liberacao_p = 'S') then
	
		ds_comando_update_w := ds_comando_update_w || 	'  and 	exists (select 	1 ' ||
								'		from 	result_laboratorio x '|| 
								' 		where 	c.nr_prescricao = x.nr_prescricao '||
								' 		and 	c.nr_sequencia = x.nr_seq_prescricao)';
	end if;
	
	exec_sql_dinamico_bv('', ds_comando_update_w, 	'ie_status_p=' || to_char(ie_status_p) || ds_sep_bv_w || 
							'nm_usuario_p=' || nm_usuario_p || ds_sep_bv_w || 
							'nr_prescricao_p=' || to_char(nr_prescricao_p) || ds_sep_bv_w || 
							'ie_status_atual=' || to_char(ie_status_atual_p) || ds_sep_bv_w ||
							'nr_seq_prescr_p=' || to_char(nr_seq_prescr_p) || ds_sep_bv_w);				
		
elsif (ie_status_p = 45) then
        
	if 	(ie_exame_amostra_p = 'S') and 
		(ie_exame_etapa_amostra_w  = 'S') then
		
                lab_atualizar_etapa_amostra(nr_prescricao_p, ie_status_p, nr_amostra_w, ds_sql_item_prescr_p, nm_usuario_p);
	else 
	
		ds_comando_update_w :=	' update 	prescr_procedimento c '||
					' set 		ie_status_atend = 45, '||
					'		nm_usuario 	= :nm_usuario_p, '||
					'		dt_atualizacao 	= sysdate' || ds_sql_motivo_alt_p ||
					' where 	nr_prescricao 	= :nr_prescricao_p '||
					' and 		ie_status_atend = :ie_status_atual_p '||
					' and 		nr_sequencia 	= :nr_seq_prescr_p';
					
		exec_sql_dinamico_bv('', ds_comando_update_w, 	'nm_usuario_p=' || nm_usuario_p || ds_sep_bv_w || 
								'nr_prescricao_p=' || to_char(nr_prescricao_p) || ds_sep_bv_w || 
								'ie_status_atual_p=' || to_char(ie_status_atual_p) || ds_sep_bv_w ||
								'nr_seq_prescr_p=' || to_char(nr_seq_prescr_p) || ds_sep_bv_w);	
					
	end if;
end if;

end atualizar_status_reg_barras;
/