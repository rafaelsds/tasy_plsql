create or replace
procedure via_gerar_inf_adicionais(	nr_sequencia_p		number,
				nr_localizador_p		varchar2,
				nr_reserva_p		varchar2,
				ds_assento_p		varchar2,
				ds_eticket_p		varchar2,
				ds_observacao_p		varchar2,
				nm_usuario_p		varchar2) is 

begin
update	via_reserva
set	nr_localizador	= nr_localizador_p,
	nr_reserva	= nr_reserva_p,
	ds_assento	= ds_assento_p,
	ds_eticket	= ds_eticket_p,
	ds_observacao	= ds_observacao_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

end via_gerar_inf_adicionais;
/