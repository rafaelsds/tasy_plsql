create or replace
procedure pls_oc_cta_aplic_val_mat_prest(
			dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de materiais n�o habilitados para o prestador. Esta valida��o ir� verificar 
	o cadastro de materiais do prestador, fun��o OPS - Prestador \ pasta Prestadores \ pasta 
	Servi�os e Materiais \ pasta Materiais.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

QUANDO ALTERAR UM DOS CURSORES DA VALIDA��O VERIFICAR A NECESSIDADE DE ALTERAR NO OUTRO CURSOR.

Altera��es:
 ------------------------------------------------------------------------------------------------------------------
 jjung OS 601977 10/06/2013 - Cria��o da rotina.
 ------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
null;
end pls_oc_cta_aplic_val_mat_prest;
/
