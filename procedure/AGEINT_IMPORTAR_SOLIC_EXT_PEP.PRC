create or replace procedure ageint_importar_solic_ext_pep(  nr_seq_ageint_p      Number,
            nr_seq_solicitacao_p  Number,
            nm_usuario_p    Varchar2) is

cd_convenio_w			agenda_integrada. cd_convenio %type;
cd_categoria_w			agenda_integrada. cd_categoria %type;
cd_plano_w			agenda_integrada. cd_plano %type;
cd_pessoa_fisica_w		agenda_integrada. cd_pessoa_fisica %type;
nr_minuto_duracao_w		agenda_integrada_item. nr_minuto_duracao %type;
ie_exame_lab_w      		varchar2(1 CHAR);
ie_tempo_padrao_ageint_w	varchar2(3 CHAR);

Cursor C01 is
    select pee.nr_sequencia,
    	peei.cd_procedimento,
        peei.ie_origem_proced,
        peei.nr_proc_interno,
        peei.nr_seq_exame_lab nr_seq_exame,
        pee.cd_doenca cd_doenca_cid,
        nvl(pee.nr_atendimento,0) nr_atendimento,
	peei.ie_lado
    from  pedido_exame_externo_item peei,
          pedido_exame_externo pee
    where  peei.nr_seq_pedido  = nr_seq_solicitacao_p
    and  pee.nr_sequencia = peei.nr_seq_pedido
    and  peei.nr_proc_interno is not null
    and  ((peei.nr_seq_exame_lab is not null and ie_exame_lab_w = 'S') or
         (peei.nr_seq_exame_lab is null and ie_exame_lab_w = 'N') or
         (ie_exame_lab_w = 'A'));

begin
obter_param_usuario(869, 178, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_tempo_padrao_ageint_w);
obter_param_usuario(869, 413, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_exame_lab_w);

if  ((nvl(nr_seq_ageint_p,0) > 0) and (nvl(nr_seq_solicitacao_p,0) > 0)) then
  select max(cd_convenio),
  	max(cd_categoria),
	max(cd_plano),
	max(cd_pessoa_fisica)
  into  cd_convenio_w,
  	cd_categoria_w,
	cd_plano_w,
	cd_pessoa_fisica_w
  from 	agenda_integrada
  where nr_sequencia = nr_seq_ageint_p;

  for c01_w in c01 loop
    IF (c01_w.cd_doenca_cid IS NULL) and
       (c01_w.nr_atendimento IS NOT NULL) THEN
        c01_w.cd_doenca_cid  := nvl(Obter_Ultimo_Cid_Atend_Pac(c01_w.nr_atendimento,'P','C',0), Obter_Ultimo_Cid_Atend_Pac(c01_w.nr_atendimento,'S','C',0));
    END IF;
    
    if (ie_tempo_padrao_ageint_w = 'S') then
    	Obter_Tempo_Padrao_Ageint
		(c01_w.nr_proc_interno,
		c01_w.cd_procedimento,
		c01_w.ie_origem_proced,
		null,
		null,
		cd_pessoa_fisica_w,
		nr_minuto_duracao_w,
		c01_w.ie_lado,
		cd_convenio_w,
		cd_categoria_w,
		cd_plano_w);
	if (nr_minuto_duracao_w = 0) then
		nr_minuto_duracao_w := null;
	end if;
    end if;

    insert into agenda_integrada_item(
      nr_sequencia,
      nr_seq_agenda_int,
      dt_atualizacao,
      nm_usuario,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      ie_tipo_agendamento,
      cd_procedimento,
      ie_origem_proced,
      nr_seq_proc_interno,
      cd_doenca_cid,
      ie_lado,
      nr_minuto_duracao,
      nr_seq_pedido_externo
    ) values (
      agenda_integrada_item_seq.nextval,
      nr_seq_ageint_p,
      sysdate,
      nm_usuario_p,
      sysdate,
      nm_usuario_p,
      'E',
      c01_w.cd_procedimento,
      c01_w.ie_origem_proced,
      c01_w.nr_proc_interno,
      c01_w.cd_doenca_cid,
      c01_w.ie_lado,
      nr_minuto_duracao_w,
      c01_w.nr_sequencia
    );
  end loop;
end if;

commit;

end ageint_importar_solic_ext_pep;
/
