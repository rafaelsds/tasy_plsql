create or replace
procedure atualiza_man_dt_ult_acesso(	nr_sequencia_p	number) is		
begin
if 	(nr_sequencia_p is not null) then
	begin
	update	man_ordem_servico 
	set	dt_ult_acesso_lider = sysdate
	where	nr_sequencia = nr_sequencia_p;
	commit;
	end;
end if;
end atualiza_man_dt_ult_acesso;
/