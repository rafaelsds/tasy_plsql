create or replace
procedure atualizar_motivo_falta_preco(cd_motivo_falta_preco_p	Varchar2,
			nr_item_cot_compra_p		number,
			nr_sequencia_p			number,
			nr_opcao_p			number)	is 

begin

if ( nr_opcao_p = 0) then

	update	cot_compra_forn_item 
	set	cd_motivo_falta_preco = cd_motivo_falta_preco_p
	where	nr_item_cot_compra = nr_item_cot_compra_p;

else

	update	cot_compra_forn_item 
	set	cd_motivo_falta_preco = cd_motivo_falta_preco_p 
	where	nr_sequencia = nr_sequencia_p;
	
end if;
commit;

end atualizar_motivo_falta_preco;
/



