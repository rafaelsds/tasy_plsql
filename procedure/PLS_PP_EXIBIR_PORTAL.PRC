create or replace 
procedure pls_pp_exibir_portal(	nr_seq_lote_p		pls_pp_lote.nr_sequencia%type,
				nr_seq_prestador_p	pls_pp_prestador.nr_sequencia%type,
				ie_exibe_portal_p	pls_pp_lote.ie_exibe_portal%type,
				nm_usuario_p		usuario.nm_usuario%type) is

tb_nr_sequencia_w	pls_util_cta_pck.t_number_table;
tp_ie_exibe_portal_w	pls_util_cta_pck.t_varchar2_table_5;
ie_atualiza_lote_w	boolean;
qt_registro_w		pls_integer;

cursor c01(	nr_seq_lote_pc	pls_pp_lote.nr_sequencia%type) is
        select	p.nr_sequencia,
		case 	when p.vl_liquido > 0 then ie_exibe_portal_p
			when decode((	select	count(1) 
					from	pls_pp_prest_evento_valor v
					where	v.nr_seq_prestador = p.nr_seq_prestador
					and	v.nr_seq_lote    = p.nr_seq_lote
                                        and     v.vl_liquido    >= 0
                                ),0,'N','S') = 'S' then ie_exibe_portal_p
			else 'N'
		end 
	from	pls_pp_prestador p
	where	p.nr_seq_lote = nr_seq_lote_pc;

begin
-- s� faz algo se foi passado o lote
if	(nr_seq_lote_p is not null) then

	-- por padr�o n�o ir� atualizar o lote
	ie_atualiza_lote_w := false;

	-- se n�o foi passado o prestador ent�o atualiza de todos os prestadores do lote
	if	(nr_seq_prestador_p is null) then

		open c01(nr_seq_lote_p);
		loop
			fetch c01 bulk collect into	tb_nr_sequencia_w, tp_ie_exibe_portal_w
			limit pls_util_pck.qt_registro_transacao_w;
			exit when tb_nr_sequencia_w.count = 0;

			forall i in tb_nr_sequencia_w.first..tb_nr_sequencia_w.last
				update	pls_pp_prestador
				set	ie_exibe_portal = tp_ie_exibe_portal_w(i),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
				where	nr_sequencia = tb_nr_sequencia_w(i);
			commit;
		end loop;
		close c01;
	else
		update	pls_pp_prestador
		set	ie_exibe_portal = ie_exibe_portal_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_seq_prestador_p;
	end if;

	-- quando � para disponibilizar no portal ent�o sempre ir� atualizar o lote
	if	(ie_exibe_portal_p = 'S') then
		ie_atualiza_lote_w := true;
	-- se n�o for para disponibilizar, s� atualiza se n�o existir prestador para exibir no portal
	else
		select	count(1)
		into	qt_registro_w
		from	pls_pp_prestador
		where	nr_seq_lote = nr_seq_lote_p
		and	ie_exibe_portal = 'S';

		ie_atualiza_lote_w := (qt_registro_w = 0);
	end if;

	if	(ie_atualiza_lote_w) then
		update	pls_pp_lote
		set	ie_exibe_portal = ie_exibe_portal_p,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_seq_lote_p;
	end if;
	commit;
end if;
end pls_pp_exibir_portal;
/