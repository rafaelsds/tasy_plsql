create or replace
procedure TUSS_Carga_Servico_Erro_Item(
			nr_sequencia_p	number,
			ds_erro_p		Varchar2) is 
begin
update	tuss_servico_item
set	ds_log = substr(ds_erro_p,1,255)
where	nr_sequencia = nr_sequencia_p;
commit;

end TUSS_Carga_Servico_Erro_Item;
/