create or replace
procedure TASY_CRIPTOGRAFAR_OBJETOS is

qt_objeto_w     Number(10);

begin

select  count(*)
into    qt_objeto_w
from    user_objects
where   upper(object_name) = 'TASY_WRAPPED_OBJETOS'
and             upper(STATUS) = 'VALID';
begin

        if (qt_objeto_w > 0) then
                exec_sql_dinamico('','begin tasy_wrapped_objetos; end;');
        else
                Exec_Sql_Dinamico('Tasy', 'drop procedure tasy_wrapped_objetos');
				Exec_Sql_Dinamico('Tasy', ' delete from objeto_sistema_param where nr_seq_objeto = 60296');
				Exec_Sql_Dinamico('Tasy', ' delete from objeto_sistema where nr_sequencia = 60296');
				Exec_Sql_Dinamico('Tasy', ' delete from tasy_versao.objeto_sistema_param where nr_seq_objeto = 60296');
				Exec_Sql_Dinamico('Tasy', ' delete from tasy_versao.objeto_sistema where nr_sequencia = 60296');
        end if;
exception
        when others then
                Exec_Sql_Dinamico('Tasy', 'drop procedure tasy_wrapped_objetos');
				Exec_Sql_Dinamico('Tasy', ' delete from objeto_sistema_param where nr_seq_objeto = 60296');
				Exec_Sql_Dinamico('Tasy', ' delete from objeto_sistema where nr_sequencia = 60296');
				Exec_Sql_Dinamico('Tasy', ' delete from tasy_versao.objeto_sistema_param where nr_seq_objeto = 60296');
				Exec_Sql_Dinamico('Tasy', ' delete from tasy_versao.objeto_sistema where nr_sequencia = 60296');				
end;

end tasy_criptografar_objetos;
/
