create or replace 
procedure mob_gerar_comunic_interna(dt_comunicado_p			date,
									ds_titulo_p				varchar2,
									ds_comunicado_p			varchar2,
									nm_usuario_destino_p	varchar2,
									ds_perfil_adicional_p	varchar2, 
									nm_usuario_p			varchar2,
									ds_setor_adicional_p	varchar2,
									ds_grupo_p				varchar2,
									cd_perfil_origem_p		number,
									cd_setor_origem_p		number,
									nr_sequencia_p		out	number) is

begin

if ((nm_usuario_p is not null) and (ds_titulo_p is not null)) then
	
	select	comunic_interna_seq.nextval 
	into	nr_sequencia_p
	from	dual;
	
	insert	into comunic_interna (	nr_sequencia, 
									nm_usuario, 
									dt_atualizacao, 
									dt_comunicado, 
									ds_titulo, 
									ds_comunicado, 
									ie_gerencial,
									nm_usuario_destino,
									ds_perfil_adicional,
									ds_setor_adicional,
									ds_grupo,
									cd_perfil_origem,
									cd_setor_origem
									) values (
									nr_sequencia_p,
									nm_usuario_p,
									dt_comunicado_p,
									dt_comunicado_p,
									ds_titulo_p,
									ds_comunicado_p,
									'N',
									nm_usuario_destino_p,
									ds_perfil_adicional_p,
									ds_setor_adicional_p,
									ds_grupo_p,
									cd_perfil_origem_p,
									cd_setor_origem_p);

	commit;
end if;

end mob_gerar_comunic_interna;
/

