create or replace
procedure sus_consiste_laudo_sismama_mam(	nr_seq_laudo_p	number,
					nm_usuario_p	varchar2) is

ds_detalhe_w			varchar2(255)	:= '';
cd_estabelecimento_w  		number(4);
cd_profissional_w     		varchar2(10);
dt_emissao_w          		date;
dt_liberacao_w        		date;
nr_atendimento_w      		number(10);
nr_sequencia_w			number(10);
nr_exame_w			number(10);
qt_exame_w			number(10);
ie_nodulo_caroco_w		varchar2(1);
ie_risco_elevado_w			varchar2(1);
dt_recebimento_w			date;
ie_mamografia_diag_w		varchar2(1);
ie_mam_rastreamento_w		varchar2(1);
dt_exame_w			date;
cd_responsavel_w			varchar2(10);
dt_liber_conclusao_w		date;
ie_categoria_dir_w			varchar2(1);
ie_categoria_esq_w			varchar2(1);
ie_recomendacao_dir_w		varchar2(1);
ie_recomendacao_esq_w		varchar2(1);
qt_idade_w			number(10);
c_anm_mamograf_w		varchar2(1);
c_anm_mamo_ano_w		varchar2(4);
ie_achado_exame_clin_w		varchar2(1);
ie_controle_rad_w			varchar2(1);
ie_lesao_diag_cancer_w		varchar2(1);
ie_aval_qt_neo_adjuvante_w		varchar2(1);
ie_area_densa_loc1_w		varchar2(2);
ie_area_densa_loc2_w		varchar2(2);
ie_area_densa1_w			varchar2(1);
ie_area_densa2_w			varchar2(1);
ie_assim_difusa_loc1_w		varchar2(2);
ie_assim_difusa_loc2_w		varchar2(2);
ie_assim_difusa1_w			varchar2(1);
ie_assim_difusa2_w			varchar2(1);
ie_assim_focal_loc1_w          	 	varchar2(2);
ie_assim_focal_loc2_w           		varchar2(2);
ie_assim_focal1_w               		varchar2(1);
ie_assim_focal2_w               		varchar2(1);
ie_calcificacao_vasc_w          	varchar2(1);
ie_composicao_mama_w            	varchar2(2);
ie_dilatacao_ductal_w           		varchar2(1);
ie_dist_arq_cirurg_w            		varchar2(1);
ie_distor_focal1_w              		varchar2(1);
ie_distor_focal2_w              		varchar2(1);
ie_distor_foc_loc1_w            		varchar2(2);
ie_distor_foc_loc2_w            		varchar2(2);
ie_implante_integro_w           		varchar2(1);
ie_implante_ruptura_w           		varchar2(1);
ie_inf_aumentados_w             		varchar2(1);
ie_lado_w                       		varchar2(1);
ie_linf_confluentes_w           		varchar2(1);
ie_linf_densos_w                		varchar2(1);
ie_linf_intramamario_w          		varchar2(1);
ie_linf_visibilizado_w          		varchar2(1);
ie_micr_distrib1_w              		varchar2(3);
ie_micr_distrib2_w              		varchar2(3);
ie_micr_distrib3_w              		varchar2(3);
ie_micr_forma1_w                		varchar2(3);
ie_micr_forma2_w                		varchar2(3);
ie_micr_forma3_w                		varchar2(3);
ie_micr_localizacao1_w          		varchar2(2);
ie_micr_localizacao2_w          		varchar2(2);
ie_micr_localizacao3_w         		varchar2(2);
ie_microcalcificacao1_w         		varchar2(1);
ie_microcalcificacao2_w         		varchar2(1);
ie_microcalcificacao3_w         		varchar2(1);
ie_nod_calcificado_w            		varchar2(1);
ie_nod_contorno1_w              		varchar2(1);
ie_nod_contorno2_w              		varchar2(1);
ie_nod_contorno3_w              		varchar2(1);
ie_nod_dens_gordura_w         	varchar2(1);
ie_nod_dens_het_w               		varchar2(1);
ie_nod_densidade_w              		varchar2(1);
ie_nod_limite1_w                		varchar2(1);
ie_nod_limite2_w                		varchar2(1);
ie_nod_limite3_w                		varchar2(1);
ie_nod_localizacao1_w           	varchar2(2);
ie_nod_localizacao2_w           	varchar2(2);
ie_nod_localizacao3_w           	varchar2(2);
ie_nod_tamanho1_w               	varchar2(1);
ie_nod_tamanho2_w               	varchar2(1);
ie_nod_tamanho3_w               	varchar2(1);
ie_nodulo1_w                    		varchar2(1);
ie_nodulo2_w                    		varchar2(1);
ie_nodulo3_w                    		varchar2(1);
ie_achad_rad_w			varchar2(10);
dt_ult_mestruacao_w		date;
dt_nascimento_w			date;
dt_plastic_implante_dir_w	sismama_anamnese_rad.dt_plastic_implante_dir%type;--Number(4);
dt_plastic_implante_esq_w	sismama_anamnese_rad.dt_plastic_implante_esq%type;--Number(4);
qt_existe_w			number(10);
ie_lesao_pap_dir_w               	varchar2(1);
ie_descarga_pap_dir_w               	varchar2(1);
ie_nod_qsl_dir_w               	varchar2(1);
ie_nod_qil_dir_w               	varchar2(1);
ie_nod_qsm_dir_w               	varchar2(1);
ie_nod_qim_dir_w               	varchar2(1);
ie_nod_uqlat_dir_w               	varchar2(1);
ie_nod_uqsup_dir_w               	varchar2(1);
ie_nod_uqmed_dir_w               	varchar2(1);
ie_nod_uqinf_dir_w               	varchar2(1);
ie_nod_rra_dir_w               	varchar2(1);
ie_nod_pa_dir_w               	varchar2(1);
ie_esp_qsm_dir_w               	varchar2(1);
ie_esp_qim_dir_w               	varchar2(1);
ie_esp_qsl_dir_w               	varchar2(1);
ie_esp_qil_dir_w               	varchar2(1); 
ie_esp_uqlat_dir_w               	varchar2(1);
ie_esp_uqsup_dir_w               	varchar2(1);
ie_esp_uqmed_dir_w               	varchar2(1);
ie_esp_uqinf_dir_w               	varchar2(1);
ie_esp_rra_dir_w               	varchar2(1);
ie_esp_pa_dir_w               	varchar2(1); 
ie_linf_axilar_dir_w               	varchar2(1);
ie_linf_supraclavic_dir_w               	varchar2(1);
ie_lesao_pap_esq_w               	varchar2(1);
ie_descarga_pap_esq_w               	varchar2(1);
ie_nod_qsl_esq_w               	varchar2(1);
ie_nod_qim_esq_w               	varchar2(1);
ie_nod_qsm_esq_w               	varchar2(1);
ie_nod_qil_esq_w               	varchar2(1);
ie_nod_uqlat_esq_w               	varchar2(1);
ie_nod_uqsup_esq_w               	varchar2(1);
ie_nod_uqmed_esq_w               	varchar2(1);
ie_nod_uqinf_esq_w               	varchar2(1);
ie_nod_rra_esq_w               	varchar2(1);
ie_nod_pa_esq_w               	varchar2(1);
ie_esp_qsm_esq_w               	varchar2(1);
ie_esp_qim_esq_w               	varchar2(1);
ie_esp_qsl_esq_w               	varchar2(1);
ie_esp_qil_esq_w               	varchar2(1);
ie_esp_uqlat_esq_w               	varchar2(1);
ie_esp_uqsup_esq_w               	varchar2(1);
ie_esp_uqmed_esq_w               	varchar2(1);
ie_esp_uqinf_esq_w               	varchar2(1);
ie_esp_rra_esq_w               	varchar2(1);

Cursor C01 is
	select	ie_area_densa_loc1,
		ie_area_densa_loc2,
		ie_area_densa1,
		ie_area_densa2,
		ie_assim_difusa_loc1,
		ie_assim_difusa_loc2,
		ie_assim_difusa1,
		ie_assim_difusa2,
		ie_assim_focal_loc1,
		ie_assim_focal_loc2,
		ie_assim_focal1,
		ie_assim_focal2,
		ie_calcificacao_vasc,
		ie_composicao_mama ,
		ie_dilatacao_ductal,
		ie_dist_arq_cirurg ,
		ie_distor_focal1,
		ie_distor_focal2,
		ie_distor_foc_loc1 ,
		ie_distor_foc_loc2 ,
		ie_implante_integro,
		ie_implante_ruptura,
		ie_inf_aumentados  ,
		ie_lado ,
		ie_linf_confluentes,
		ie_linf_densos,
		ie_linf_intramamario,
		ie_linf_visibilizado,
		ie_micr_distrib1,
		ie_micr_distrib2,
		ie_micr_distrib3,
		ie_micr_forma1,
		ie_micr_forma2,
		ie_micr_forma3,
		ie_micr_localizacao1,
		ie_micr_localizacao2,
		ie_micr_localizacao3,
		ie_microcalcificacao1,
		ie_microcalcificacao2,
		ie_microcalcificacao3,
		ie_nod_calcificado ,
		ie_nod_contorno1,
		ie_nod_contorno2,
		ie_nod_contorno3,
		ie_nod_dens_gordura,
		ie_nod_dens_het,
		ie_nod_limite1,
		ie_nod_limite2,
		ie_nod_limite3,
		ie_nod_localizacao1,
		ie_nod_localizacao2,
		ie_nod_localizacao3,
		ie_nod_tamanho1,
		ie_nod_tamanho2,
		ie_nod_tamanho3,
		ie_nodulo1,
		ie_nodulo2,
		ie_nodulo3
	from	sismama_achado_rad
	where	nr_seq_sismama = nr_sequencia_w;

begin

/*Obter os dados do Laudo*/
begin
select	a.nr_sequencia,
	a.cd_estabelecimento,
	a.cd_profissional,
	a.dt_emissao,
	a.nr_atendimento,
	Obter_Idade_PF(c.cd_pessoa_fisica,sysdate,'A'),
	c.dt_nascimento
into	nr_sequencia_w,
	cd_estabelecimento_w,
	cd_profissional_w,
	dt_emissao_w,
	nr_atendimento_w,
	qt_idade_w,
	dt_nascimento_w
from	pessoa_fisica 		c,
	atendimento_paciente 	b,
	sismama_atendimento	a
where	a.nr_sequencia 	 	= nr_seq_laudo_p
and	a.nr_atendimento 	= b.nr_atendimento
and	b.cd_pessoa_fisica	= c.cd_pessoa_fisica;
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort(174941);
	/*'Problemas para obter as informa��es do atendimento ao consistir o laudo.'*/
	end;

begin
select	nvl(max(b.ie_nodulo_caroco),''),
	nvl(max(b.ie_risco_elevado),''),
	nvl(max(b.ie_ultima_mamografia),'N'),
	nvl(max(substr(b.dt_ultima_mamografia,1,4)),'X')
into	ie_nodulo_caroco_w,
	ie_risco_elevado_w,
	c_anm_mamograf_w,
	c_anm_mamo_ano_w
from	sismama_mam_anamnese b
where	b.nr_seq_sismama = nr_sequencia_w;
exception
	when others then
	ie_nodulo_caroco_w 	:= '';
	ie_risco_elevado_w 	:= '';
	c_anm_mamograf_w	:= 'N';
	c_anm_mamo_ano_w	:= 'X';
	end;

begin
select	nvl(c.ie_mamografia_diag,'N'),
	nvl(c.ie_mam_rastreamento,'N'),
	nvl(c.ie_achado_exame_clin,'N'),
	nvl(c.ie_controle_rad,'N'),
	nvl(c.ie_lesao_diag_cancer,'N'),
	nvl(c.ie_aval_qt_neo_adjuvante,'N'),
	dt_exame,
	nvl(ie_lesao_pap_dir,'N'),
	nvl(ie_descarga_pap_dir,'N'),
	nvl(ie_nod_qsl_dir,'N'),
	nvl(ie_nod_qil_dir,'N'),
	nvl(ie_nod_qsm_dir,'N'),
	nvl(ie_nod_qim_dir,'N'),
	nvl(ie_nod_uqlat_dir,'N'),
	nvl(ie_nod_uqsup_dir,'N'),
	nvl(ie_nod_uqmed_dir,'N'),
	nvl(ie_nod_uqinf_dir,'N'),
	nvl(ie_nod_rra_dir,'N'),
	nvl(ie_nod_pa_dir,'N'),
	nvl(ie_esp_qsm_dir,'N'),
	nvl(ie_esp_qim_dir,'N'),
	nvl(ie_esp_qsl_dir,'N'),
	nvl(ie_esp_qil_dir,'N'), 
	nvl(ie_esp_uqlat_dir,'N'),
	nvl(ie_esp_uqsup_dir,'N'),
	nvl(ie_esp_uqmed_dir,'N'),
	nvl(ie_esp_uqinf_dir,'N'),
	nvl(ie_esp_rra_dir,'N'),
	nvl(ie_esp_pa_dir,'N'), 
	nvl(ie_linf_axilar_dir,'N'),
	nvl(ie_linf_supraclavic_dir,'N'),
	nvl(ie_lesao_pap_esq,'N'),
	nvl(ie_descarga_pap_esq,'N'),
	nvl(ie_nod_qsl_esq,'N'),
	nvl(ie_nod_qim_esq,'N'),
	nvl(ie_nod_qsm_esq,'N'),
	nvl(ie_nod_qil_esq,'N'),
	nvl(ie_nod_uqlat_esq,'N'),
	nvl(ie_nod_uqsup_esq,'N'),
	nvl(ie_nod_uqmed_esq,'N'),
	nvl(ie_nod_uqinf_esq,'N'),
	nvl(ie_nod_rra_esq,'N'),
	nvl(ie_nod_pa_esq,'N'),
	nvl(ie_esp_qsm_esq,'N'),
	nvl(ie_esp_qim_esq,'N'),
	nvl(ie_esp_qsl_esq,'N'),
	nvl(ie_esp_qil_esq,'N'),
	nvl(ie_esp_uqlat_esq,'N'),
	nvl(ie_esp_uqsup_esq,'N'),
	nvl(ie_esp_uqmed_esq,'N'),
	nvl(ie_esp_uqinf_esq,'N'),
	nvl(ie_esp_rra_esq,'N')
into	ie_mamografia_diag_w,
	ie_mam_rastreamento_w,
	ie_achado_exame_clin_w,
	ie_controle_rad_w,
	ie_lesao_diag_cancer_w,
	ie_aval_qt_neo_adjuvante_w,
	dt_exame_w,
	ie_lesao_pap_dir_w,
	ie_descarga_pap_dir_w,
	ie_nod_qsl_dir_w,
	ie_nod_qil_dir_w,
	ie_nod_qsm_dir_w,
	ie_nod_qim_dir_w,
	ie_nod_uqlat_dir_w,
	ie_nod_uqsup_dir_w,
	ie_nod_uqmed_dir_w,
	ie_nod_uqinf_dir_w,
	ie_nod_rra_dir_w,
	ie_nod_pa_dir_w,
	ie_esp_qsm_dir_w,
	ie_esp_qim_dir_w,
	ie_esp_qsl_dir_w,
	ie_esp_qil_dir_w, 
	ie_esp_uqlat_dir_w,
	ie_esp_uqsup_dir_w,
	ie_esp_uqmed_dir_w,
	ie_esp_uqinf_dir_w,
	ie_esp_rra_dir_w,
	ie_esp_pa_dir_w, 
	ie_linf_axilar_dir_w,
	ie_linf_supraclavic_dir_w,
	ie_lesao_pap_esq_w,
	ie_descarga_pap_esq_w,
	ie_nod_qsl_esq_w,
	ie_nod_qim_esq_w,
	ie_nod_qsm_esq_w,
	ie_nod_qil_esq_w,
	ie_nod_uqlat_esq_w,
	ie_nod_uqsup_esq_w,
	ie_nod_uqmed_esq_w,
	ie_nod_uqinf_esq_w,
	ie_nod_rra_esq_w,
	ie_nod_pa_esq_w,
	ie_esp_qsm_esq_w,
	ie_esp_qim_esq_w,
	ie_esp_qsl_esq_w,
	ie_esp_qil_esq_w,
	ie_esp_uqlat_esq_w,
	ie_esp_uqsup_esq_w,
	ie_esp_uqmed_esq_w,
	ie_esp_uqinf_esq_w,
	ie_esp_rra_esq_w
from	sismama_mam_ind_clinica c
where	c.nr_seq_sismama = nr_sequencia_w;
exception
	when others then
	ie_mamografia_diag_w 		:= 'N';
	ie_mam_rastreamento_w 		:= 'N';
	ie_achado_exame_clin_w		:= 'N';
	ie_controle_rad_w		:= 'N';
	ie_lesao_diag_cancer_w		:= 'N';
	ie_aval_qt_neo_adjuvante_w	:= 'N';
	ie_lesao_pap_dir_w		:= 'N';
	ie_descarga_pap_dir_w		:= 'N';
	ie_nod_qsl_dir_w		:= 'N';
	ie_nod_qil_dir_w		:= 'N';
	ie_nod_qsm_dir_w		:= 'N';
	ie_nod_qim_dir_w		:= 'N';
	ie_nod_uqlat_dir_w		:= 'N';
	ie_nod_uqsup_dir_w		:= 'N';
	ie_nod_uqmed_dir_w		:= 'N';
	ie_nod_uqinf_dir_w		:= 'N';
	ie_nod_rra_dir_w		:= 'N';
	ie_nod_pa_dir_w			:= 'N';	
	ie_esp_qsm_dir_w		:= 'N';
	ie_esp_qim_dir_w		:= 'N';
	ie_esp_qsl_dir_w		:= 'N';
	ie_esp_qil_dir_w		:= 'N'; 
	ie_esp_uqlat_dir_w		:= 'N';
	ie_esp_uqsup_dir_w		:= 'N';
	ie_esp_uqmed_dir_w		:= 'N';
	ie_esp_uqinf_dir_w		:= 'N';
	ie_esp_rra_dir_w		:= 'N';
	ie_esp_pa_dir_w			:= 'N'; 
	ie_linf_axilar_dir_w		:= 'N';
	ie_linf_supraclavic_dir_w	:= 'N';
	ie_lesao_pap_esq_w		:= 'N';
	ie_descarga_pap_esq_w		:= 'N';
	ie_nod_qsl_esq_w		:= 'N';
	ie_nod_qim_esq_w		:= 'N';
	ie_nod_qsm_esq_w		:= 'N';
	ie_nod_qil_esq_w		:= 'N';
	ie_nod_uqlat_esq_w		:= 'N';
	ie_nod_uqsup_esq_w		:= 'N';
	ie_nod_uqmed_esq_w		:= 'N';
	ie_nod_uqinf_esq_w		:= 'N';
	ie_nod_rra_esq_w		:= 'N';
	ie_nod_pa_esq_w			:= 'N';
	ie_esp_qsm_esq_w		:= 'N';
	ie_esp_qim_esq_w		:= 'N';
	ie_esp_qsl_esq_w		:= 'N';
	ie_esp_qil_esq_w		:= 'N';
	ie_esp_uqlat_esq_w		:= 'N';
	ie_esp_uqsup_esq_w		:= 'N';
	ie_esp_uqmed_esq_w		:= 'N';
	ie_esp_uqinf_esq_w		:= 'N';
	ie_esp_rra_esq_w 		:= 'N';
	dt_exame_w			:= null;
	end;

begin
select	d.nr_exame,
	d.dt_recebimento,
	dt_ult_mestruacao,
	dt_plastic_implante_dir,
	dt_plastic_implante_esq
into	nr_exame_w,
	dt_recebimento_w,
	dt_ult_mestruacao_w,
	dt_plastic_implante_dir_w,
	dt_plastic_implante_esq_w
from	sismama_anamnese_rad d
where	d.nr_seq_sismama = nr_sequencia_w;

exception
	when others then
	nr_exame_w		:= null;
	dt_recebimento_w	:= null;
	end;

begin
select	cd_responsavel,
	dt_liberacao,
	ie_categoria_dir,
	ie_categoria_esq,
	ie_recomendacao_dir,
	ie_recomendacao_esq
into	cd_responsavel_w,
	dt_liber_conclusao_w,
	ie_categoria_dir_w,
	ie_categoria_esq_w,
	ie_recomendacao_dir_w,
	ie_recomendacao_esq_w
from	sismama_mam_conclusao
where	nr_seq_sismama = nr_sequencia_w;
exception
	when others then
	cd_responsavel_w	:= null;
	dt_liber_conclusao_w	:= null;
	ie_categoria_dir_w	:= null;
	ie_categoria_esq_w	:= null;
	ie_recomendacao_dir_w	:= null;
	ie_recomendacao_esq_w	:= null;
	end;

if	(sus_obter_incolaudo_ativa(30)) and
	(ie_nodulo_caroco_w is null) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 30, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(31)) and
	(ie_risco_elevado_w is null) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 31, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(32)) and
	(ie_mamografia_diag_w = 'N') and
	(ie_mam_rastreamento_w  = 'N') then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 32, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(33)) and
	(dt_exame_w is null) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 33, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(34)) and
	(nr_exame_w is null) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 34, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(35)) and
	not (nr_exame_w is null) then
	begin

	begin
	select 	count(*)
	into	qt_exame_w
	from	sismama_anamnese_rad
	where	nr_exame = nr_exame_w;
	exception
		when others then
		qt_exame_w := 0;
		end;

	if	(qt_exame_w > 1) then
		ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
		Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 35, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
	end if;
	end;
end if;

if	(sus_obter_incolaudo_ativa(36)) and
	(dt_recebimento_w is null) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 36, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(37)) and
	(nvl(cd_responsavel_w,'0') = '0') then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 37, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(38)) and
	(dt_liber_conclusao_w is null) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 38, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(39)) and
	(nvl(ie_categoria_dir_w,'Z') = 'Z') and
	(nvl(ie_categoria_esq_w,'Z') = 'Z') then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 39, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(40)) and
	(nvl(ie_recomendacao_dir_w,'Z') = 'Z') and
	(nvl(ie_recomendacao_esq_w,'Z') = 'Z') then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 40, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(42)) and
	(ie_mam_rastreamento_w = 'S') and
	(qt_idade_w < 35) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 42, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(45)) and
	(c_anm_mamograf_w = 'S') and
	(nvl(c_anm_mamo_ano_w,'X') = 'X') then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 45, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(46)) and
	(nvl(ie_mamografia_diag_w,'X') in ('D','E','A')) and
	(ie_achado_exame_clin_w = 'N') and
	(ie_controle_rad_w = 'N') and
	(ie_lesao_diag_cancer_w = 'N') and
	(ie_aval_qt_neo_adjuvante_w = 'N') then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 46, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

/* 63 - SISMAMA - Mamografia - Profissional respons�vel n�o possui CBO de radiologista */
if	(sus_obter_incolaudo_ativa(63)) then
	begin
	select	count(*)
	into	qt_existe_w
	from	sus_cbo_pessoa_fisica
	where	cd_cbo in ('766420','223260','223124','225320','324115','324120')
	and	cd_pessoa_fisica = nvl(cd_responsavel_w,0);

	if	(qt_existe_w = 0) then
		begin
		ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
		Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 63, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
		end;
	end if;
	end;
end if;

open C01;
loop
fetch C01 into
	ie_area_densa_loc1_w,
	ie_area_densa_loc2_w,
	ie_area_densa1_w,
	ie_area_densa2_w,
	ie_assim_difusa_loc1_w,
	ie_assim_difusa_loc2_w,
	ie_assim_difusa1_w,
	ie_assim_difusa2_w,
	ie_assim_focal_loc1_w,
	ie_assim_focal_loc2_w,
	ie_assim_focal1_w,
	ie_assim_focal2_w,
	ie_calcificacao_vasc_w,
	ie_composicao_mama_w,
	ie_dilatacao_ductal_w,
	ie_dist_arq_cirurg_w,
	ie_distor_focal1_w,
	ie_distor_focal2_w,
	ie_distor_foc_loc1_w,
	ie_distor_foc_loc2_w,
	ie_implante_integro_w,
	ie_implante_ruptura_w,
	ie_inf_aumentados_w,
	ie_lado_w,
	ie_linf_confluentes_w,
	ie_linf_densos_w,
	ie_linf_intramamario_w,
	ie_linf_visibilizado_w,
	ie_micr_distrib1_w,
	ie_micr_distrib2_w,
	ie_micr_distrib3_w,
	ie_micr_forma1_w,
	ie_micr_forma2_w,
	ie_micr_forma3_w,
	ie_micr_localizacao1_w,
	ie_micr_localizacao2_w,
	ie_micr_localizacao3_w,
	ie_microcalcificacao1_w,
	ie_microcalcificacao2_w,
	ie_microcalcificacao3_w,
	ie_nod_calcificado_w,
	ie_nod_contorno1_w,
	ie_nod_contorno2_w,
	ie_nod_contorno3_w,
	ie_nod_dens_gordura_w,
	ie_nod_dens_het_w,
	ie_nod_limite1_w,
	ie_nod_limite2_w,
	ie_nod_limite3_w,
	ie_nod_localizacao1_w,
	ie_nod_localizacao2_w,
	ie_nod_localizacao3_w,
	ie_nod_tamanho1_w,
	ie_nod_tamanho2_w,
	ie_nod_tamanho3_w,
	ie_nodulo1_w,
	ie_nodulo2_w,
	ie_nodulo3_w ;
exit when C01%notfound;
	begin
	if	(sus_obter_incolaudo_ativa(47)) then
		begin
		if	(nvl(ie_nodulo1_w,'N') = 'S') and
			((nvl(ie_nod_tamanho1_w,'Z') = 'Z') or
			(nvl(ie_nod_localizacao1_w,'Z') = 'Z') or
			(nvl(ie_nod_limite1_w,'Z') = 'Z') or
			(nvl(ie_nod_contorno1_w,'Z') = 'Z')) then
			begin			
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 47, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		if	(nvl(ie_nodulo2_w,'N') = 'S') and
			((nvl(ie_nod_tamanho2_w,'Z') = 'Z') or
			(nvl(ie_nod_localizacao2_w,'Z') = 'Z') or
			(nvl(ie_nod_limite2_w,'Z') = 'Z') or
			(nvl(ie_nod_contorno2_w,'Z') = 'Z')) then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 47, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		if	(nvl(ie_nodulo3_w,'N') = 'S') and
			((nvl(ie_nod_tamanho3_w,'Z') = 'Z') or
			(nvl(ie_nod_localizacao3_w,'Z') = 'Z') or
			(nvl(ie_nod_limite3_w,'Z') = 'Z') or
			(nvl(ie_nod_contorno3_w,'Z') = 'Z')) then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 47, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		end;
	end if;

	if	(sus_obter_incolaudo_ativa(48)) then
		begin
		if	(nvl(ie_microcalcificacao1_w,'N') = 'S') and
			(nvl(ie_micr_localizacao1_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 48, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		if	(nvl(ie_microcalcificacao2_w,'N') = 'S') and
			(nvl(ie_micr_localizacao2_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 48, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		if	(nvl(ie_microcalcificacao3_w,'N') = 'S') and
			(nvl(ie_micr_localizacao3_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 48, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		end;
	end if;

	if	(sus_obter_incolaudo_ativa(49)) then
		begin
		if	(nvl(ie_assim_focal1_w,'N') = 'S') and
			(nvl(ie_assim_focal_loc1_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 49, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		if	(nvl(ie_assim_focal2_w,'N') = 'S') and
			(nvl(ie_assim_focal_loc2_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 49, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		end;
	end if;

	if	(sus_obter_incolaudo_ativa(50)) then
		begin
		if	(nvl(ie_assim_difusa1_w,'N') = 'S') and
			(ie_assim_difusa_loc1_w is null) then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 50, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		if	(nvl(ie_assim_difusa2_w,'N') = 'S') and
			(ie_assim_difusa_loc2_w is null) then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 50, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		end;
	end if;

	if	(sus_obter_incolaudo_ativa(51)) then
		begin
		if	(nvl(ie_distor_focal1_w,'N') = 'S') and
			(nvl(ie_distor_foc_loc1_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 51, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		if	(nvl(ie_distor_focal2_w,'N') = 'S') and
			(nvl(ie_distor_foc_loc2_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 51, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		end;
	end if;

	if	(sus_obter_incolaudo_ativa(52)) then
		begin
		if	(nvl(ie_area_densa1_w,'N') = 'S') and
			(nvl(ie_area_densa_loc1_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 52, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		if	(nvl(ie_area_densa2_w,'N') = 'S') and
			(nvl(ie_area_densa_loc2_w,'Z') = 'Z') then
			begin
			ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311224,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_sequencia_w='||nr_sequencia_w||';'||'ie_lado_w='||ie_lado_w);
			Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 52, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
			end;
		end if;
		end;
	end if;

	end;
end loop;
close C01;

if	(sus_obter_incolaudo_ativa(55)) then
	begin

	select	nvl(max('S'),'N')
	into	ie_achad_rad_w
	from	sismama_mam_ind_clinica a,
		sismama_achado_rad b
	where	a.nr_seq_sismama 	= b.nr_seq_sismama(+)
	and	a.nr_seq_sismama 	= nr_sequencia_w
	and	(((a.ie_mamografia_diag = 'N') and
		(a.ie_mam_rastreamento = 'S')) or
		((a.ie_mamografia_diag	= b.ie_lado) or
		((a.ie_mamografia_diag = 'A') and
		(((select 	count(*)
		from	sismama_achado_rad x
		where 	x.nr_seq_sismama = b.nr_seq_sismama
		and	x.ie_lado = 'D') > 0) and
		((select 	count(*)
		from	sismama_achado_rad y
		where 	y.nr_seq_sismama = b.nr_seq_sismama
		and	y.ie_lado = 'E') > 0)))));

	if	(ie_achad_rad_w = 'N') then
		ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
		Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 55, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
	end if;

	end;
end if;

if	(sus_obter_incolaudo_ativa(56)) and
	(trunc(dt_liber_conclusao_w) < trunc(dt_recebimento_w)) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 56, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(57)) and
	(trunc(dt_exame_w) > trunc(dt_recebimento_w)) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 57, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(58)) and
	((trunc(dt_ult_mestruacao_w) > trunc(dt_recebimento_w)) or
	(trunc(dt_ult_mestruacao_w) < trunc(dt_nascimento_w))) then
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 58, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
end if;

if	(sus_obter_incolaudo_ativa(62)) then
	begin

	select	nvl(max('S'),'N')
	into	ie_achad_rad_w
	from	sismama_mam_ind_clinica a,
		sismama_achado_rad b
	where	a.nr_seq_sismama 	= b.nr_seq_sismama(+)
	and	a.nr_seq_sismama 	= nr_sequencia_w
	and	(((a.ie_mam_rastreamento = 'S')
	and	((select 	count(*)
		from	sismama_achado_rad x
		where 	x.nr_seq_sismama = b.nr_seq_sismama
		and	x.ie_lado = 'D') > 0)
	and	((select 	count(*)
		from	sismama_achado_rad x
		where 	x.nr_seq_sismama = b.nr_seq_sismama
		and	x.ie_lado = 'E') > 0))
	or	(a.ie_mam_rastreamento = 'N'));

	if	(ie_achad_rad_w = 'N') then
		ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
		Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 62, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
	end if;

	end;
end if;

if	(sus_obter_incolaudo_ativa(65)) and
	((ie_implante_integro_w = 'S') or
	(ie_implante_ruptura_w = 'S')) and
	((dt_plastic_implante_esq_w is null) and
	(dt_plastic_implante_dir_w is null)) then
	begin
	ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
	Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 65, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
	end;
end if;

if	(sus_obter_incolaudo_ativa(66)) and
	(ie_mamografia_diag_w <> 'N') and
	(ie_achado_exame_clin_w = 'S') then
	begin
	
	if	(nvl(ie_lesao_pap_dir_w,'N') = 'N')	and (nvl(ie_descarga_pap_dir_w,'N') = 'N') and
		(nvl(ie_nod_qsl_dir_w,'N') = 'N') 	and (nvl(ie_nod_qil_dir_w,'N') = 'N') 	and
		(nvl(ie_nod_qsm_dir_w,'N') = 'N') 	and (nvl(ie_nod_qim_dir_w,'N') = 'N') 	and
		(nvl(ie_nod_uqlat_dir_w,'N') = 'N')	and (nvl(ie_nod_uqsup_dir_w,'N') = 'N')	and
		(nvl(ie_nod_uqmed_dir_w,'N') = 'N')	and (nvl(ie_nod_uqinf_dir_w,'N') = 'N')	and
		(nvl(ie_nod_rra_dir_w,'N') = 'N') 	and (nvl(ie_nod_pa_dir_w,'N') = 'N') 	and
		(nvl(ie_esp_qsm_dir_w,'N') = 'N') 	and (nvl(ie_esp_qim_dir_w,'N') = 'N')	and
		(nvl(ie_esp_qsl_dir_w,'N') = 'N') 	and (nvl(ie_esp_qil_dir_w,'N') = 'N')	and 
		(nvl(ie_esp_uqlat_dir_w,'N') = 'N') 	and (nvl(ie_esp_uqsup_dir_w,'N') = 'N')	and
		(nvl(ie_esp_uqmed_dir_w,'N') = 'N') 	and (nvl(ie_esp_uqinf_dir_w,'N') = 'N')	and
		(nvl(ie_esp_rra_dir_w,'N') = 'N') 	and (nvl(ie_esp_pa_dir_w,'N') = 'N') 	and 
		(nvl(ie_linf_axilar_dir_w,'N') = 'N') 	and (nvl(ie_linf_supraclavic_dir_w,'N') = 'N') and
		(nvl(ie_lesao_pap_esq_w,'N') = 'N') 	and (nvl(ie_descarga_pap_esq_w,'N') = 'N') and
		(nvl(ie_nod_qsl_esq_w,'N') = 'N') 	and (nvl(ie_nod_qim_esq_w,'N') = 'N')	and
		(nvl(ie_nod_qsm_esq_w,'N') = 'N') 	and (nvl(ie_nod_qil_esq_w,'N') = 'N')	and
		(nvl(ie_nod_uqlat_esq_w,'N') = 'N') 	and (nvl(ie_nod_uqsup_esq_w,'N') = 'N')	and
		(nvl(ie_nod_uqmed_esq_w,'N') = 'N') 	and (nvl(ie_nod_uqinf_esq_w,'N') = 'N')	and
		(nvl(ie_nod_rra_esq_w,'N') = 'N') 	and (nvl(ie_nod_pa_esq_w,'N') = 'N') 	and
		(nvl(ie_esp_qsm_esq_w,'N') = 'N') 	and (nvl(ie_esp_qim_esq_w,'N') = 'N')	and
		(nvl(ie_esp_qsl_esq_w,'N') = 'N') 	and (nvl(ie_esp_qil_esq_w,'N') = 'N')	and
		(nvl(ie_esp_uqlat_esq_w,'N') = 'N') 	and (nvl(ie_esp_uqsup_esq_w,'N') = 'N')	and
		(nvl(ie_esp_uqmed_esq_w,'N') = 'N') 	and (nvl(ie_esp_uqinf_esq_w,'N') = 'N')	and
		(nvl(ie_esp_rra_esq_w,'N') = 'N') then
		begin
		ds_detalhe_w	:= wheb_mensagem_pck.get_texto(311158,'nr_atendimento_w='||nr_atendimento_w||';'||'nr_seq_sisco_w='||nr_sequencia_w);
		Sus_Laudo_Gravar_Inco(nr_seq_laudo_p, 66, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
		end;
	end if;	
	end;
end if;

commit;

end sus_consiste_laudo_sismama_mam;
/