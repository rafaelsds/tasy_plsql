create or replace
procedure DIRF_REGISTRO_VAL_MENSAIS_PJ(	cd_cgc_p		varchar2,
					cd_dirf_p		varchar2,
					dt_lote_p		date,
					nm_usuario_p		varchar2,
					nr_seq_posicao_p	number,
					nr_seq_apres_p		out number,
					ie_tipo_data_p		number,
					cd_estabelecimento_p	varchar2) is 

/* ie_tipo_data
1 - data contabil
2 - data liquidacao
3 - data emissao

*/
					
					
ds_arquivo_ret_w varchar2(2000);
ds_arquivo_ir_w  varchar2(2000);
nr_seq_apres_w	number(10);
dt_lote_w	date;
ie_mes_w	varchar2(2);
ie_entou_w	boolean;
vl_rendimento_w	number(15,2);
vl_irrf_w	number(15,2);
separador_w	varchar2(1) := '|';
nr_titulo_w	varchar2(20);

CURSOR C01 is
	select 	sum(vl_rendimento),
		sum(vl_imposto)
	from
	(select distinct
		p.nr_titulo,
		nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'RET','IR'),0) vl_rendimento,
		nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'IMP','IR'),0) vl_imposto
	from	titulo_pagar p,
		titulo_pagar_imposto i,
		tributo t
	where	p.nr_titulo = i.nr_titulo(+)
	and	i.cd_tributo = t.cd_tributo(+)
	and	nvl(obter_se_nota_servico(p.nr_titulo),'S') = 'S'
	and	p.nr_titulo_original is null
	and 	trunc(decode(ie_tipo_data_p,1,p.dt_contabil,2,p.dt_liquidacao,p.dt_emissao),'YYYY') = trunc(dt_lote_p,'YYYY')
	and	p.cd_cgc = cd_cgc_p
	and	( (p.cd_estabelecimento =  nvl(cd_estabelecimento_p,p.cd_estabelecimento))
	or        ((p.cd_estabelecimento is null) and (cd_estabelecimento_p is null)))
	and	nvl(nvl(i.cd_darf,
	        substr(obter_codigo_darf(i.cd_tributo,
		                         to_number(obter_descricao_padrao('TITULO_PAGAR','CD_ESTABELECIMENTO',i.nr_titulo)), 
				         obter_descricao_padrao('TITULO_PAGAR','CD_CGC',i.nr_titulo), 
				     null),1,10)
         	       ),'1708') = decode(cd_dirf_p,'5952','9999',cd_dirf_p)
	and	p.ie_situacao <> 'C'
	and	p.ie_origem_titulo in (select y.ie_origem_titulo from dirf_regra_origem_tit y)
	and	nvl(to_char(decode(ie_tipo_data_p,1,p.dt_contabil,2,p.dt_liquidacao,p.dt_emissao),'mm'),'00') = lpad(ie_mes_w,2,'0')
	and	exists (select 	1 
		from 	titulo_pagar x,
			titulo_pagar_imposto z,
			tributo t
		where	x.nr_titulo = z.nr_titulo
		and	t.cd_tributo = z.cd_tributo
		and	x.cd_cgc = P.cd_cgc
		and	z.vl_imposto >= 0
		and 	t.ie_tipo_tributo = 'IR')
	union all
	select 	distinct
		p.nr_titulo,
		nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'RET','COFINS'),0) vl_rendimento,
		nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'IMP','COFINS'),0) vl_imposto
	from	titulo_pagar p
	where	nvl(obter_se_nota_servico(p.nr_titulo),'S') = 'S'
	and	trunc(decode(ie_tipo_data_p,1,p.dt_contabil,2,p.dt_liquidacao,p.dt_emissao),'YYYY') = trunc(dt_lote_p,'YYYY')
	and	p.cd_cgc = cd_cgc_p
	and	p.ie_situacao <> 'C'
	and	p.nr_titulo_original is null
	and	p.ie_origem_titulo in (select y.ie_origem_titulo from dirf_regra_origem_tit y)
	and	( (p.cd_estabelecimento =  nvl(cd_estabelecimento_p,p.cd_estabelecimento))
	or        ((p.cd_estabelecimento is null) and (cd_estabelecimento_p is null)))
	and	((cd_dirf_p = '5952') or (cd_dirf_p = '5960'))
	and	nvl(to_char(decode(ie_tipo_data_p,1,p.dt_contabil,2,p.dt_liquidacao,p.dt_emissao),'mm'),'00') = lpad(ie_mes_w,2,'0')
	and	exists (select 	1 
		from 	titulo_pagar x,
			titulo_pagar_imposto z,
			tributo t
		where	x.nr_titulo = z.nr_titulo
		and	t.cd_tributo = z.cd_tributo
		and	x.cd_cgc = P.cd_cgc
		and	z.vl_imposto >= 0
		and 	t.ie_tipo_tributo = 'COFINS')
	union all
	select 	distinct
		p.nr_titulo,
		nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'RET','CSLL'),0) vl_rendimento,
		nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'IMP','CSLL'),0) vl_imposto
	from	titulo_pagar p
	where	nvl(obter_se_nota_servico(p.nr_titulo),'S') = 'S'
	and	trunc(decode(ie_tipo_data_p,1,p.dt_contabil,2,p.dt_liquidacao,p.dt_emissao),'YYYY') = trunc(dt_lote_p,'YYYY')
	and	p.cd_cgc = cd_cgc_p
	and	p.ie_situacao <> 'C'
	and	p.nr_titulo_original is null
	and	p.ie_origem_titulo in (select y.ie_origem_titulo from dirf_regra_origem_tit y)
	and	( (p.cd_estabelecimento =  nvl(cd_estabelecimento_p,p.cd_estabelecimento))
	or        ((p.cd_estabelecimento is null) and (cd_estabelecimento_p is null)))
	and	cd_dirf_p = '5987'
	and	nvl(to_char(decode(ie_tipo_data_p,1,p.dt_contabil,2,p.dt_liquidacao,p.dt_emissao),'mm'),'00') = lpad(ie_mes_w,2,'0')
	and	exists (select 	1 
		from 	titulo_pagar x,
			titulo_pagar_imposto z,
			tributo t
		where	x.nr_titulo = z.nr_titulo
		and	t.cd_tributo = z.cd_tributo
		and	x.cd_cgc = P.cd_cgc
		and	z.vl_imposto >= 0
		and 	t.ie_tipo_tributo = 'CSLL'));

begin
nr_seq_apres_w := nr_seq_posicao_p;
ie_mes_w := '01';
ie_entou_w := false;
ds_arquivo_ret_w := 'RTRT'  || separador_w;
ds_arquivo_ir_w  := 'RTIRF' || separador_w;

while to_number(ie_mes_w) <= 13 loop
	OPEN c01;
	LOOP
	FETCH c01 INTO
		vl_rendimento_w,
		vl_irrf_w;	
	EXIT WHEN c01%NOTFOUND;
		if (to_number(ie_mes_w) = 13) and (nvl(vl_rendimento_w,0) = 0) then
		ds_arquivo_ret_w := ds_arquivo_ret_w  || separador_w;		
		elsif (nvl(vl_rendimento_w,0) = 0) then
			ds_arquivo_ret_w := ds_arquivo_ret_w  || lpad(replace(campo_mascara(nvl('0','0'),2),'.',''),15,'0') || separador_w;
		else
			ds_arquivo_ret_w := ds_arquivo_ret_w || lpad(replace(campo_mascara(nvl(vl_rendimento_w,'0'),2),'.',''),15,'0') || separador_w;
		end if;
	
		if (to_number(ie_mes_w) = 13) and (nvl(vl_irrf_w,0) = 0) then
			ds_arquivo_ir_w := ds_arquivo_ir_w    || separador_w;
		elsif (nvl(vl_irrf_w,0) = 0) then
			ds_arquivo_ir_w := ds_arquivo_ir_w  || lpad(replace(campo_mascara(nvl('0','0'),2),'.',''),15,'0') || separador_w;
		else
			ds_arquivo_ir_w  := ds_arquivo_ir_w || lpad(replace(campo_mascara(nvl(vl_irrf_w,'0'),2),'.',''),15,'0') || separador_w;
		end if;
	
	
		/*if (nvl(vl_rendimento_w,0) = 0) then
			ds_arquivo_ret_w := ds_arquivo_ret_w || separador_w;	
		else
			ds_arquivo_ret_w := ds_arquivo_ret_w || lpad(replace(campo_mascara(nvl(vl_rendimento_w,'0'),2),'.',''),15,'0') || separador_w;
		end if;
		
		if (nvl(vl_irrf_w,0) = 0) then
			ds_arquivo_ir_w := ds_arquivo_ir_w   || separador_w;			
		else
			ds_arquivo_ir_w	 := ds_arquivo_ir_w  || lpad(replace(campo_mascara(nvl(vl_irrf_w,'0'),2),'.',''),15,'0') || separador_w;
		end if; */
		ie_entou_w := true;
	END LOOP;
	CLOSE c01;	
	
	/*if (not ie_entou_w) then
		ds_arquivo_ret_w := ds_arquivo_ret_w || separador_w;
		ds_arquivo_ir_w := ds_arquivo_ir_w   || separador_w;			
	end if;*/
		
	ie_entou_w := false;	
	ie_mes_w := to_char(to_number(ie_mes_w + 1));		
end loop;

--registro de rentdimentos
insert 	into w_dirf_arquivo (	     nr_sequencia,
				     nm_usuario,
				     nm_usuario_nrec,
				     dt_atualizacao,
				     dt_atualizacao_nrec,
				     ds_arquivo,
				     nr_seq_apresentacao,
				     nr_seq_registro,
				     cd_pessoa_fisica)
		values		    (w_dirf_arquivo_seq.nextval,
				     nm_usuario_p,
				     nm_usuario_p,
				     sysdate,
				     sysdate,
				     DS_ARQUIVO_RET_W,
				     nr_seq_apres_w,
				     0,
				     null);

nr_seq_apres_w := nr_seq_apres_w + 1; 				      

--registro de IR
insert 	into w_dirf_arquivo (nr_sequencia,
				     nm_usuario,
				     nm_usuario_nrec,
				     dt_atualizacao,
				     dt_atualizacao_nrec,
				     ds_arquivo,
				     nr_seq_apresentacao,
				     nr_seq_registro,
				     cd_pessoa_fisica)
		values		    (w_dirf_arquivo_seq.nextval,
				     nm_usuario_p,
				     nm_usuario_p,
				     sysdate,
				     sysdate,
				     DS_ARQUIVO_IR_W,
				     nr_seq_apres_w,
				     0,
				     null);				     
commit;
nr_seq_apres_p := nr_seq_apres_w;

end DIRF_REGISTRO_VAL_MENSAIS_PJ;
/
