create or replace
procedure liberar_pertence_pac(
			nr_seq_registro_p			number,
			nr_sequencia_p			number,
			nm_usuario_p			varchar2) is 

begin

update	PERTENCE_PACIENTE_ITEM
set	dt_liberacao	=	sysdate
where	nr_sequencia	=	nr_sequencia_p
and	NR_SEQ_PERTENCE_PACIENTE	=	nr_seq_registro_p;
commit;

end liberar_pertence_pac;
/