create or replace
procedure agenda_paciente_GridDblClick(	nr_seq_agenda_p		number,
					cd_pessoa_fisica_p	varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2,
					ds_reservado_p      out	varchar2,
					ie_se_pac_agenda_p  out	varchar2,
					ds_telefone_p	    out varchar2) is

ie_reservado_w		varchar2(1);
ie_se_pac_agenda_w	varchar2(1);
ds_telefone_w		varchar2(255);
						
begin

if	(nr_seq_agenda_p > 0) then
	begin
	if	(cd_pessoa_fisica_p is not null) then
		begin
		ie_se_pac_agenda_w	:= substr(Obter_Se_Pac_Agenda(cd_pessoa_fisica_p, nm_usuario_p, cd_estabelecimento_p),1,1);
		ds_telefone_w		:= substr(obter_fone_pac_agenda(cd_pessoa_fisica_p),1,255);
		end;
	end if;	
	end;
end if;

ie_se_pac_agenda_p	:= ie_se_pac_agenda_w;
ds_telefone_p		:= ds_telefone_w;

end agenda_paciente_GridDblClick;
/
