create or replace
procedure CONSISTIR_REGRA_PROT_PROCED(	nr_seq_paciente_p	number,
					nm_usuario_p		Varchar2) is 

cd_perfil_w		number(10);
nr_seq_erro_w		number(10);
qt_procedimentos_w	number(10) := 0;

begin
cd_perfil_w	:= obter_perfil_ativo;

delete from paciente_atendimento_erro
where	nr_seq_paciente	= nr_seq_paciente_p
and		nr_seq_material is null;

if	(obter_se_consistir_regra_onc(5, cd_perfil_w)	= 'S') then
begin

	select	count(nr_seq_procedimento)
	into	qt_procedimentos_w
	from	paciente_protocolo_proc
	where	nr_seq_paciente	= nr_seq_paciente_p;
	
	if	(qt_procedimentos_w = 0) then
		gerar_erro_pac_prot(nr_seq_paciente_p, null, 5, null, cd_perfil_w, nm_usuario_p, nr_seq_erro_w);
	end if;
end;
end if;

commit;

end CONSISTIR_REGRA_PROT_PROCED;
/