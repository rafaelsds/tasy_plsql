create or replace
procedure pls_copiar_regras_vendedor(
		ds_lista_check_regra_p	varchar2,
		nr_sequencia_p		number,
		ie_tipo_guia_p		varchar2,
		nm_usuario_p		varchar2) is 

ds_lista_check_regra_w	varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_seq_sequencia_w	number(10);
nr_controle_loop_w	number(3,0) := 0;
			
begin
if	(ds_lista_check_regra_p is not null) and
	(nr_sequencia_p is not null) and
	(ie_tipo_guia_p is not null) and
	(nm_usuario_p is not null) then
	begin		
	ds_lista_check_regra_w := ds_lista_check_regra_p;
		
	while 	(ds_lista_check_regra_w is not null) and
		(nr_controle_loop_w < 100) loop
		begin
		nr_pos_virgula_w := instr(ds_lista_check_regra_w,',');
	
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_seq_sequencia_w	:= to_number(substr(ds_lista_check_regra_w,0,nr_pos_virgula_w-1));
			ds_lista_check_regra_w	:= substr(ds_lista_check_regra_w,nr_pos_virgula_w+1,length(ds_lista_check_regra_w));			
			end;
		else
			begin
			nr_seq_sequencia_w	:= to_number(ds_lista_check_regra_w);
			ds_lista_check_regra_w	:= null;
			end;
		end if;
		
		if	(nr_seq_sequencia_w > 0) then
			begin
			pls_copiar_regra_comissao(
					nr_seq_sequencia_w,
					nr_sequencia_p,
					ie_tipo_guia_p,
					nm_usuario_p);
			end;
		end if;
		nr_controle_loop_w := nr_controle_loop_w + 1;
		end;
	end loop;		
	end;
end if;
commit;
end pls_copiar_regras_vendedor;
/