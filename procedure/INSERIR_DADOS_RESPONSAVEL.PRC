create or replace
procedure Inserir_dados_Responsavel( cd_responsavel_p	varchar2,
				     cd_pessoa_fisica_p	varchar2,
				     nm_usuario_p	varchar2) is 


nr_seq_responsavel_w	number(10) := 0;
ie_gerar_res_w		varchar2(1);
cd_cep_w 		varchar2(15);
cd_cep_paciente_w       varchar2(15);
cd_cpf_w		varchar2(11);
nr_telefone_w		varchar2(40);
nr_identidade_w		varchar2(15);
nm_responsavel_w	varchar(60);
nr_sequencia_w		number(10);

begin

if	(cd_responsavel_p is not null) and
	(cd_pessoa_fisica_p is not null) then	
	delete 
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and 	ie_tipo_complemento = 3;
end if;

begin
	select	nr_sequencia
	into	nr_sequencia_w	
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_responsavel_p 
	and     ie_tipo_complemento = 1;
exception
	when 	others then
		nr_sequencia_w := 0;
end;	

if	(nr_sequencia_w > 0) then
	insert into compl_pessoa_fisica (nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_contato,
					ds_endereco,
					cd_cep,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					sg_estado,
					nr_telefone,
					nr_identidade,
					nr_cpf,
					cd_municipio_ibge,
					cd_tipo_logradouro,
					nr_ddd_telefone,
					nr_ddi_telefone,
					dt_atualizacao,
					nm_usuario,
					cd_empresa_refer,
					cd_pessoa_fisica_ref,
					nr_seq_pessoa_endereco)
					(Select 
						(select	nvl(max(x.nr_sequencia),0) + 1
						from	compl_pessoa_fisica x
						where	x.cd_pessoa_fisica = cd_pessoa_fisica_p),
						cd_pessoa_fisica_p,
						3,
						a.nm_pessoa_fisica,
						b.ds_endereco,
						b.cd_cep,
						b.ds_complemento,
						b.ds_bairro,
						b.ds_municipio,
						b.sg_estado,
						b.nr_telefone,
						a.nr_identidade,
						a.nr_cpf,
						b.cd_municipio_ibge,
						b.cd_tipo_logradouro,
						b.nr_ddd_telefone,
						b.nr_ddi_telefone,
						sysdate,
						nm_usuario_p,
						cd_empresa_refer,
						cd_responsavel_p,
						b.nr_seq_pessoa_endereco
					from	compl_pessoa_fisica b,
						pessoa_fisica a						
					where	b.cd_pessoa_fisica = a.cd_pessoa_fisica
					and	ie_tipo_complemento = 1
					and	b.cd_pessoa_fisica	= cd_responsavel_p
					and	a.cd_pessoa_fisica	= cd_responsavel_p					
					and	b.nr_sequencia = nr_sequencia_w);
					
else

	begin
		select	max(nr_cpf),
			max(nr_identidade),
			max(nr_telefone_celular),
			max(substr(nm_pessoa_fisica,1,60))
		into	cd_cpf_w,
			nr_identidade_w,
			nr_telefone_w,
			nm_responsavel_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_responsavel_p;
	exception
		when 	others then
			cd_cpf_w := null;
			nr_identidade_w := null;
			nr_telefone_w := null;
			nm_responsavel_w := null;
	end;
	
	
	if	(cd_responsavel_p is not null) and
		(cd_pessoa_fisica_p is not null) then

		begin
		
		if	(cd_responsavel_p is not null) and
			(cd_pessoa_fisica_p is not null) then	
			
			delete 
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica = cd_responsavel_p
			and 	ie_tipo_complemento = 1;
			
		end if;		
		
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica = cd_responsavel_p;
		
			insert into compl_pessoa_fisica 
				(	nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nr_identidade,
					nr_cpf,
					nm_contato,
					nr_telefone,
					dt_atualizacao,
					nm_usuario)
			values	(	nr_sequencia_w,
					cd_responsavel_p,
					1,
					nr_identidade_w,
					cd_cpf_w,
					nm_responsavel_w,
					nr_telefone_w,
					sysdate,
					nm_usuario_p);		
		exception
		when others then
			null;
		end;
				
	end if;
end if;

obter_param_usuario(916, 473, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gerar_res_w);					

if	(ie_gerar_res_w = 'R') then

	select	max(cd_cep)
	into	cd_cep_w	
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_responsavel_p 
	and     ie_tipo_complemento = 1;
	
	select	max(cd_cep)
	into	cd_cep_paciente_w	
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p 
	and     ie_tipo_complemento = 1;
	
	if	(cd_cep_w is not null) and 
		(cd_cep_paciente_w is null) then
	
		if	(cd_responsavel_p is not null) and
			(cd_pessoa_fisica_p is not null) then
			delete 
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica = cd_pessoa_fisica_p
			and 	ie_tipo_complemento = 1;
		end if;	
		
		insert into compl_pessoa_fisica (nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					ds_endereco,
					cd_cep,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					sg_estado,
					cd_municipio_ibge,
					cd_tipo_logradouro,
					dt_atualizacao,
					nm_usuario)
					(Select 
						(select	nvl(max(x.nr_sequencia),0) + 1
						from	compl_pessoa_fisica x
						where	x.cd_pessoa_fisica = cd_pessoa_fisica_p),
						cd_pessoa_fisica_p,
						1,
						b.ds_endereco,
						b.cd_cep,
						b.ds_complemento,
						b.ds_bairro,
						b.ds_municipio,
						b.sg_estado,
						b.cd_municipio_ibge,
						b.cd_tipo_logradouro,
						sysdate,
						nm_usuario_p
					from	compl_pessoa_fisica b,
						pessoa_fisica a						
					where	b.cd_pessoa_fisica = a.cd_pessoa_fisica
					and	b.ie_tipo_complemento = 1
					and	b.cd_pessoa_fisica = cd_responsavel_p	
					and	a.cd_pessoa_fisica = cd_responsavel_p	
                 			and	b.nr_sequencia = nr_sequencia_w);
	else	
	
		begin
			select	max(nr_cpf),
				max(nr_identidade),
				max(nr_telefone_celular),
				max(substr(nm_pessoa_fisica,1,60))
			into	cd_cpf_w,
				nr_identidade_w,
				nr_telefone_w,
				nm_responsavel_w
			from	pessoa_fisica
			where	cd_pessoa_fisica = cd_responsavel_p;
		exception
			when 	others then
				cd_cpf_w := null;
				nr_identidade_w := null;
				nr_telefone_w := null;
				nm_responsavel_w := null;
		end;
		
		if	(cd_responsavel_p is not null) and
			(cd_pessoa_fisica_p is not null) then
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica = cd_pessoa_fisica_p;
			
			begin
				insert into compl_pessoa_fisica 
					(	nr_sequencia,
						cd_pessoa_fisica,
						ie_tipo_complemento,
						nr_identidade,
						nr_cpf,
						nm_contato,
						nr_telefone,
						dt_atualizacao,
						nm_usuario)
				values	(	nr_sequencia_w,
						cd_responsavel_p,
						3,
						nr_identidade_w,
						cd_cpf_w,
						nm_responsavel_w,
						nr_telefone_w,
						sysdate,
						nm_usuario_p);
			exception
				when others then
					null;
			end;
		end if;
	end if;
end if;

commit;

end Inserir_dados_Responsavel;
/