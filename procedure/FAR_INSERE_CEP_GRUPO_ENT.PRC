create or replace
procedure	far_insere_cep_grupo_ent(
			nr_seq_grupo_p		number,
			cd_cep_p		varchar2,
			ds_observacao_p		varchar2,
			nm_usuario_p		varchar2) is

nr_sequencia_w		number(10);

begin

select	far_grupo_entrega_cep_seq.nextval
into	nr_sequencia_w
from	dual;

insert into far_grupo_entrega_cep(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_grupo,
	cd_cep,
	ds_observacao) values (
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_grupo_p,
		cd_cep_p,
		ds_observacao_p);

commit;

end far_insere_cep_grupo_ent;
/