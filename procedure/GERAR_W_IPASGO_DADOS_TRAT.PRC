create or replace
procedure gerar_w_ipasgo_dados_trat(
			nr_interno_conta_p		number,
			dt_mesano_referencia_p	date,
			nm_usuario_p		Varchar2,
			nr_seq_tipo_fatura_p	number,
			qt_linha_arq_p in out	number,
			qt_linha_atend_p in out	number) is

nr_atendimento_w			atendimento_paciente.nr_atendimento%type;
dt_alta_w				date;
dt_periodo_final_w			date;
cd_motivo_alta_w			atendimento_paciente.cd_motivo_alta%type;
ds_motivo_alta_w			number(03,0);
cd_tipo_acomodacao_w		varchar2(10);
ds_tipo_acomodacao_w		number(01,0);
cd_tipo_tratamento_w		number(01,0);
ds_tipo_tratamento_w		number(01,0);
cd_convenio_parametro_w		number(05,0);
cd_cnpj_w			varchar2(14);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_mot_alta_conta_ipasgo_w	parametro_faturamento.ie_mot_alta_conta_ipasgo%type := 'N';
ie_exportar_dt_final_w		funcao_param_usuario.vl_parametro%type;

begin

select	a.nr_atendimento,
	nvl(a.dt_alta,b.dt_periodo_final),
	b.dt_periodo_final,
	a.cd_motivo_alta,
	substr(obter_tipo_acomod_atend(a.nr_atendimento, 'C'),1,1) cd_tipo_acomodacao,
	cd_convenio_parametro,
	b.cd_estabelecimento
into	nr_atendimento_w,
	dt_alta_w,
	dt_periodo_final_w,
	cd_motivo_alta_w,
	cd_tipo_acomodacao_w,
	cd_convenio_parametro_w,
	cd_estabelecimento_w
from	atendimento_paciente a,
	conta_paciente b
where	a.nr_atendimento	= b.nr_atendimento
and	b.nr_interno_conta	= nr_interno_conta_p;

obter_param_usuario(999, 92, obter_perfil_Ativo, nm_usuario_p, cd_estabelecimento_w, ie_exportar_dt_final_w);

select	cd_cgc
into	cd_cnpj_w
from	convenio
where	cd_convenio = cd_convenio_parametro_w;

select	cd_tipo_tratamento
into	cd_tipo_tratamento_w
from	fatur_tipo_fatura
where	nr_sequencia = nr_seq_tipo_fatura_p;

select	nvl(max(ie_mot_alta_conta_ipasgo),'N')
into	ie_mot_alta_conta_ipasgo_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_w;


if	(nvl(ie_mot_alta_conta_ipasgo_w,'N') = 'S') then
	
	cd_motivo_alta_w := nvl(obter_alta_tiss_conta(nr_interno_conta_p,'I'),cd_motivo_alta_w);

end if;



/*Buscar as conversoes meio externo*/
begin
select	coalesce(max(cd_externo), to_char(cd_motivo_alta_w))
into	ds_motivo_alta_w
from	conversao_meio_externo
where	cd_cgc = cd_cnpj_w
and	upper(nm_tabela) 	= 'W_IPASGO_DADOS_TRAT'
and	upper(nm_atributo)	= 'CD_MOTIVO_ALTA'
and	cd_interno	= to_char(cd_motivo_alta_w);
exception
when others then
	ds_motivo_alta_w := 0;
end;

begin
select	coalesce(max(cd_externo), cd_tipo_acomodacao_w)
into	ds_tipo_acomodacao_w
from	conversao_meio_externo
where	cd_cgc = cd_cnpj_w
and	upper(nm_tabela) 	= 'W_IPASGO_DADOS_TRAT'
and	upper(nm_atributo)	= 'CD_TIPO_ACOMODACAO'
and	cd_interno	= cd_tipo_acomodacao_w;
exception
when others then
	ds_tipo_acomodacao_w := 0;
end;

begin
select	coalesce(max(cd_externo), to_char(cd_tipo_tratamento_w))
into	ds_tipo_tratamento_w
from	conversao_meio_externo
where	cd_cgc = cd_cnpj_w
and	upper(nm_tabela) 	= 'W_IPASGO_DADOS_TRAT'
and	upper(nm_atributo)	= 'CD_TIPO_TRATAMENTO'
and	cd_interno		= to_char(cd_tipo_tratamento_w);
exception
when others then
	ds_tipo_tratamento_w := 0;
end;

qt_linha_arq_p 	:= qt_linha_arq_p + 1;

if	(nvl(ie_exportar_dt_final_w,'N') = 'S') then
	dt_alta_w := dt_periodo_final_w;
end if;

insert into w_ipasgo_dados_trat(
	nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	nr_linha,
	tp_registro,
	nr_linha_atend,
	dt_alta,
	cd_motivo_alta,
	cd_tipo_acomodacao,
	cd_tipo_tratamento,
	dt_mesano_referencia,
	nr_interno_conta,
	ds_linha,
	nr_seq_tipo_fatura,
	ds_motivo_alta,
	ds_tipo_acomodacao,
	ds_tipo_tratamento) 
values(	w_ipasgo_dados_trat_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	qt_linha_arq_p,
	4,
	qt_linha_atend_p,
	dt_alta_w,
	cd_motivo_alta_w,
	cd_tipo_acomodacao_w,
	cd_tipo_tratamento_w,
	dt_mesano_referencia_p,
	nr_interno_conta_p,
	qt_linha_arq_p|| '|' ||
	'4' || '|' ||
	qt_linha_atend_p || '|' ||
	to_char(dt_alta_w,'YYYY-MM-DD') || '|' || 
	ds_motivo_alta_w || '|' || 
	ds_tipo_acomodacao_w || '|' ||
	ds_tipo_tratamento_w ||
	'|||||||',
	nr_seq_tipo_fatura_p,
	ds_motivo_alta_w,
	ds_tipo_acomodacao_w,
	ds_tipo_tratamento_w);
commit;

end gerar_w_ipasgo_dados_trat;
/
