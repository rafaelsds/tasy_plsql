CREATE OR REPLACE
procedure grava_material_log_alteracao(
			cd_estabelecimento_p	number,
			cd_material_p		number,
			nm_tabela_p		varchar2,
			nm_atributo_p		varchar2,
			ds_valor_ant_p		varchar2,
			ds_valor_novo_p		varchar2,
			nm_usuario_p		varchar2,
			cd_material_princ_p	number default null,
			ie_agrupador_p		varchar2 default null,
			cd_funcao_p 		number default null,
			cd_perfil_p 		number default null) as

nr_sequencia_w			number(10);

begin

select	material_log_alteracao_seq.nextval
into	nr_sequencia_w
from	dual;

insert into material_log_alteracao(
	nr_sequencia,
	cd_estabelecimento,
	cd_material,
	dt_atualizacao,
	nm_usuario,
	nm_tabela,
	nm_atributo,
	ds_valor_ant,
	ds_valor_novo,
	cd_funcao,
	cd_perfil,
	cd_material_princ,
	ie_agrupador)
values(	nr_sequencia_w,
	cd_estabelecimento_p,
	cd_material_p,
	sysdate,
	nm_usuario_p,
	nm_tabela_p,
	nm_atributo_p,
	substr(ds_valor_ant_p,1,2000),
	substr(ds_valor_novo_p,1,2000),
	nvl(cd_funcao_p, obter_funcao_ativa),
	nvl(cd_perfil_p, obter_perfil_ativo),
	cd_material_princ_p,
	ie_agrupador_p);


commit;

end grava_material_log_alteracao;
/