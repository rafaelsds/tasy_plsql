create or replace
procedure wsuite_update_view_exam(nr_seq_prescr_p		number,
								  nr_seq_prescr_item_p	number,
								  ds_login_p			varchar2,
								  ie_tipo_item_p		varchar2) is 
								  
nr_seq_view_w	wsuite_result_view.nr_sequencia%type;

begin

select 	nvl(max(nr_sequencia),0)
into	nr_seq_view_w
from	wsuite_result_view
where 	nr_seq_prescr = nr_seq_prescr_p
and		nr_seq_prescr_item = nr_seq_prescr_item_p
and		ds_login = ds_login_p
and		ie_tipo_item = ie_tipo_item_p;

if (nr_seq_view_w = 0) then
	insert into wsuite_result_view (nr_sequencia, 
									dt_visualizacao, 
									nr_seq_prescr, 
									nr_seq_prescr_item, 
									ie_tipo_item, 
									ds_login, 
									nm_usuario, 
									dt_atualizacao, 
									nm_usuario_nrec, 
									dt_atualizacao_nrec
									) values (	
									wsuite_result_view_seq.nextval, 
									sysdate, 
									nr_seq_prescr_p, 
									nr_seq_prescr_item_p, 
									ie_tipo_item_p, 
									ds_login_p,
									ds_login_p,
									sysdate,
									ds_login_p,
									sysdate);
else
	update 	wsuite_result_view 
	set 	dt_visualizacao = sysdate,
			nm_usuario = ds_login_p,
			dt_atualizacao = sysdate
	where 	nr_sequencia = nr_seq_view_w;
end if;

commit;

end wsuite_update_view_exam;
/
