create or replace
procedure GERAR_W_HVC_REPASSE_PROD (	nr_repasse_terceiro_p	number,
					nm_usuario_p		varchar2) is

cd_convenio_w		number(15);
ie_funcao_medico_w	number(15);
cd_medico_w		varchar2(15);
cd_pessoa_fisica_w	varchar2(15);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(15);
nr_atendimento_w		number(15);
nr_interno_conta_w		number(15);
nr_seq_procedimento_w	number(15);
ds_observacao_w		varchar2(255);
nr_seq_terceiro_w		number(15);
nr_sequencia_w		number(15);
qt_procedimento_w		number(15, 4);
vl_glosa_w		number(15, 4);
vl_recurso_w		number(15, 4);
vl_amaior_w		number(15, 4);
vl_faturado_w		number(15, 4);
vl_original_repasse_w	number(15, 4);
nr_seq_lote_auditoria_w	number(15);
nr_seq_lote_audit_hist_w	number(15);
nr_seq_retorno_w		number(15);
nr_seq_ret_item_w		number(15);
vl_amenor_w		number(15, 4);
vl_pago_w		number(15, 4);
vl_adicional_w		number(15, 4);
vl_glosa_grg_w		number(15, 4);
vl_glosa_retorno_w		number(15, 4);
vl_recurso_grg_w		number(15, 4);
vl_recurso_retorno_w	number(15, 4);
vl_adicional_grg_w		number(15, 4);
vl_adicional_retorno_w	number(15, 4);
nr_seq_partic_w		number(15);
vl_pago_ant_w		number(15, 4);
dt_periodo_inicial_w	date;
dt_periodo_final_w		date;
vl_aguardando_w		number(15, 4);
vl_liberado_w		number(15, 4);
vl_liberado_mes_w		number(15, 4);
ie_glosa_per_w		number(15);
ie_grg_per_w		number(15);
vl_lib_per_w		number(15, 4);
nr_seq_material_w		number(15);
cd_material_w		number(10);
qt_material_w		number(15);
qt_funcao_w		number(15);
 
cursor c01 is 
select	distinct 
	b.nr_sequencia,
	a.cd_medico,
	b.cd_procedimento,
	b.ie_origem_proced,
	b.qt_procedimento,
	b.vl_procedimento,
	a.vl_original_repasse,
	a.nr_seq_partic,
	a.vl_liberado
from	procedimento_repasse a,
	procedimento_paciente b 
where	a.nr_seq_procedimento	= b.nr_sequencia 
and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;
 
cursor c02 is 
select	distinct 
	b.nr_sequencia,
	a.cd_medico,
	b.cd_material,
	b.qt_material,
	b.vl_material,
	a.vl_original_repasse,
	a.vl_liberado
from	material_repasse a,
	material_atend_paciente b 
where	a.nr_seq_material		= b.nr_sequencia 
and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;
 
begin 
 
delete from	W_HVC_REPASSE_TERCEIRO;
 
open c01;
loop 
fetch c01 into 
	nr_seq_procedimento_w,
	cd_medico_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_procedimento_w,
	vl_faturado_w,
	vl_original_repasse_w,
	nr_seq_partic_w,
	vl_liberado_w;
exit when c01%notfound;
 
	ds_observacao_w	:= '';
 
	select	max(a.nr_interno_conta),
		max(a.ie_funcao_medico) 
	into	nr_interno_conta_w,
		ie_funcao_medico_w 
	from	procedimento_paciente a 
	where	a.nr_sequencia	= nr_seq_procedimento_w;
 
	if	(nvl(nr_interno_conta_w, 0) > 0) then 
 
		select	max(a.cd_convenio_parametro),
			max(a.nr_atendimento) 
		into	cd_convenio_w,
			nr_atendimento_w 
		from	conta_paciente a 
		where	a.nr_interno_conta	= nr_interno_conta_w;
 
		if	(nvl(nr_atendimento_w, 0) > 0) then 
 
			select	max(a.cd_pessoa_fisica) 
			into	cd_pessoa_fisica_w 
			from	atendimento_paciente a 
			where	a.nr_atendimento	= nr_atendimento_w;
 
		end if;
 
	end if;
 
	select	max(a.nr_seq_terceiro),
		max(dt_periodo_inicial),
		max(dt_periodo_final) 
	into	nr_seq_terceiro_w,
		dt_periodo_inicial_w,
		dt_periodo_final_w 
	from	repasse_terceiro a 
	where	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;
 
	select	nvl(max(nr_sequencia), 0) + 1 
	into	nr_sequencia_w 
	from	w_hvc_repasse_terceiro;
 
	select	count(*) 
	into	qt_funcao_w 
	from	funcao_medico 
	where	cd_funcao	= ie_funcao_medico_w;
 
	if	(qt_funcao_w=0) then 
 
		ie_funcao_medico_w	:= null;
 
	end if;
 
	insert into w_hvc_repasse_terceiro 
		(cd_convenio,
		cd_funcao,
		cd_medico,
		cd_pessoa_fisica,
		cd_procedimento,
		dt_atualizacao,
		dt_atualizacao_nrec,
		ds_observacao,
		ie_origem_proced,
		nm_usuario,
		nm_usuario_nrec,
		nr_atendimento,
		nr_interno_conta,
		nr_repasse_terceiro,
		nr_seq_procedimento,
		nr_seq_retorno,
		nr_seq_terceiro,
		nr_sequencia,
		qt_procedimento,
		vl_glosa,
		vl_recurso,
		vl_amaior,
		vl_faturado,
		vl_repasse,
		vl_liberado,
		vl_pago_ant) 
	values	(cd_convenio_w,
		ie_funcao_medico_w,
		cd_medico_w,
		cd_pessoa_fisica_w,
		cd_procedimento_w,
		sysdate,
		sysdate,
		ds_observacao_w,
		ie_origem_proced_w,
		nm_usuario_p,
		nm_usuario_p,
		nr_atendimento_w,
		nr_interno_conta_w,
		nr_repasse_terceiro_p,
		nr_seq_procedimento_w,
		nr_seq_retorno_w,
		nr_seq_terceiro_w,
		nr_sequencia_w,
		qt_procedimento_w,
		0,
		0,
		0,
		nvl(vl_faturado_w, 0),
		nvl(vl_faturado_w, 0),
		vl_liberado_w,
		0);
 
 
end loop;
close c01;
 
open c02;
loop 
fetch c02 into 
	nr_seq_material_w,
	cd_medico_w,
	cd_material_w,
	qt_material_w,
	vl_faturado_w,
	vl_original_repasse_w,
	vl_liberado_w;
exit when c02%notfound;
 
	nr_atendimento_w	:= null;
	nr_interno_conta_w	:= null;

	select	max(a.nr_interno_conta) 
	into	nr_interno_conta_w 
	from	material_atend_paciente a 
	where	a.nr_sequencia	= nr_seq_material_w;
 
	if	(nvl(nr_interno_conta_w, 0) > 0) then 
 
		select	max(a.cd_convenio_parametro),
			max(a.nr_atendimento) 
		into	cd_convenio_w,
			nr_atendimento_w 
		from	conta_paciente a 
		where	a.nr_interno_conta	= nr_interno_conta_w;
 
		if	(nvl(nr_atendimento_w, 0) > 0) then 
 
			select	max(a.cd_pessoa_fisica) 
			into	cd_pessoa_fisica_w 
			from	atendimento_paciente a 
			where	a.nr_atendimento	= nr_atendimento_w;
 
		end if;
 
	end if;
 
	select	max(a.nr_seq_terceiro),
		max(dt_periodo_inicial),
		max(dt_periodo_final) 
	into	nr_seq_terceiro_w,
		dt_periodo_inicial_w,
		dt_periodo_final_w 
	from	repasse_terceiro a 
	where	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;
 
	select	nvl(max(nr_sequencia), 0) + 1 
	into	nr_sequencia_w 
	from	w_hvc_repasse_terceiro;
 
	insert into w_hvc_repasse_terceiro 
		(cd_convenio,
		cd_funcao,
		cd_medico,
		cd_pessoa_fisica,
		cd_material,
		dt_atualizacao,
		dt_atualizacao_nrec,
		ds_observacao,
		nm_usuario,
		nm_usuario_nrec,
		nr_atendimento,
		nr_interno_conta,
		nr_repasse_terceiro,
		nr_seq_retorno,
		nr_seq_terceiro,
		nr_sequencia,
		qt_procedimento,
		vl_glosa,
		vl_recurso,
		vl_amaior,
		vl_faturado,
		vl_repasse,
		vl_liberado,
		vl_pago_ant) 
	values	(cd_convenio_w,
		ie_funcao_medico_w,
		cd_medico_w,
		cd_pessoa_fisica_w,
		cd_material_w,
		sysdate,
		sysdate,
		ds_observacao_w,
		nm_usuario_p,
		nm_usuario_p,
		nr_atendimento_w,
		nr_interno_conta_w,
		nr_repasse_terceiro_p,
		nr_seq_retorno_w,
		nr_seq_terceiro_w,
		nr_sequencia_w,
		qt_material_w,
		0,
		0,
		0,
		nvl(vl_faturado_w, 0),
		nvl(vl_faturado_w, 0),
		vl_liberado_w,
		0);
 
end loop;
close c02;
 
commit;
 
end	GERAR_W_HVC_REPASSE_PROD;
/