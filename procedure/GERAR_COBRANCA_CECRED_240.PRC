create or replace
procedure GERAR_COBRANCA_CECRED_240(	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2)  is

--PADR�O CNAB 240 POSI��ES
--Vers�o 03
--Novembro/2014 

ds_conteudo_w			varchar2(240);

/* Header do arquivo */
nm_empresa_w			varchar2(30);
nm_banco_w			varchar2(30);
cd_cgc_w			varchar2(14);
cd_agencia_bancaria_w		varchar2(5);
dt_remessa_retorno_w		date;

/* Detalhe */
nm_pessoa_w			varchar2(40);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(15);
ds_cidade_w			varchar2(15);
nr_inscricao_w			varchar2(15);
ds_nosso_numero_w		varchar2(15);
vl_titulo_w			varchar2(15);
vl_desconto_w			varchar2(15);
vl_juros_mora_w			varchar2(15);
nr_titulo_w			varchar2(15);
cd_cep_w			varchar2(8);
dt_emissao_w			varchar2(8);
dt_vencimento_w			varchar2(8);
ds_uf_w				varchar2(2);
ie_tipo_inscricao_w			varchar2(1);
qt_linhas_w			number(5);
ds_mensagem_w			varchar2(140);
ds_mensagem_ant_w		varchar2(140);

/* Trailer de Lote */
vl_titulos_cobr_w			varchar2(15);
qt_titulos_cobr_w			number(5);
qt_registro_lote_w			number(4)	:= 0;

/* Trailer do Arquivo */
qt_registro_w			number(5)	:= 0;
nr_seq_reg_lote_w			number(6)	:= 0;


Cursor	C01 is
select	lpad(nvl(c.cd_agencia_bancaria,'0'),5,'0'),
	lpad(82||somente_numero(lpad(b.nr_titulo,8,'0')||'-'|| calcula_digito('Modulo11',(82||lpad(b.nr_titulo,8,'0')))),15,'0') ds_nosso_numero,
	lpad(b.nr_titulo,15,0) nr_titulo,
	to_char(nvl(b.dt_pagamento_previsto,sysdate),'DDMMYYYY') dt_vencimento,
	lpad(replace(to_char(b.vl_titulo, 'fm0000000000000.00'),'.',''),15,'0') vl_titulo,
	to_char(nvl(b.dt_emissao,sysdate),'DDMMYYYY') dt_emissao,
	lpad(replace(to_char(b.vl_titulo * b.tx_juros / 100 / 30, 'fm0000000000000.00'),'.',''),15,'0') vl_juros_mora,
	lpad(replace(to_char(b.TX_DESC_ANTECIPACAO, 'fm0000000000000.00'),'.',''),15,'0') vl_desconto,
	nvl(decode(b.cd_pessoa_fisica, null, '2', '1'),'0') ie_tipo_inscricao,
	lpad(decode(decode(b.cd_pessoa_fisica, null, 2, 1),2,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,(substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),1,9) || substr(obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'C'),10,2)),'000000000000000'),15,'0') nr_inscricao,
	rpad(substr(nvl(elimina_caractere_especial(obter_nome_pf_pj(b.cd_pessoa_fisica, b.cd_cgc)),' '),1,40),40,' ') nm_pessoa,
	rpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'R')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,2,'EDN')),' ')),1,40),40,' ') ds_endereco,
	rpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'B')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,2,'B')),' ')),1,15),15,' ') ds_bairro,
	lpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'CEP')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,2,'CEP')),' ')),1,8),8,'0') cd_cep,
	rpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'CI')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,2,'CI')),' ')),1,15),15,' ') ds_cidade,
	rpad(substr(decode(b.cd_pessoa_fisica,null,nvl(elimina_caractere_especial(obter_dados_pf_pj('', b.cd_cgc,'UF')),' '),nvl(elimina_caractere_especial(obter_compl_pf(b.cd_pessoa_fisica,2,'UF')),' ')),1,2),2,' ') ds_uf
from	pls_mensalidade d,
	titulo_receber b,
	titulo_receber_cobr c,
	cobranca_escritural a
where	a.nr_sequencia		= c.nr_seq_cobranca
and	c.nr_titulo			= b.nr_titulo
and	d.nr_sequencia(+)		= b.nr_seq_mensalidade
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
begin

delete	from w_envio_banco
where	nm_usuario		= nm_usuario_p;

/* Header do Arquivo */

select	lpad(b.cd_cgc,14,'0'),
	lpad(nvl(c.cd_agencia_bancaria,'0'),5,'0') cd_agencia_bancaria,
	rpad(substr(nvl(elimina_caractere_especial(obter_razao_social(b.cd_cgc)),' '),1,30),30,' ') nm_empresa,
	rpad('SICREDI', 30, ' ') nm_banco,
	nvl(a.dt_remessa_retorno,sysdate)
into	cd_cgc_w,
	cd_agencia_bancaria_w,
	nm_empresa_w,
	nm_banco_w,
	dt_remessa_retorno_w
from	banco_estabelecimento c,
	estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

qt_registro_w	:= qt_registro_w + 1;

ds_conteudo_w	:= 	'085' ||
			'0000' || 
			'0' || 
			rpad(' ',9,' ') ||
			'2' ||
			cd_cgc_w ||
			rpad(' ',20,' ') ||
			cd_agencia_bancaria_w ||
			' ' ||
			'000000000000' ||
			' ' ||
			' ' ||
			nm_empresa_w ||
			nm_banco_w ||
			rpad(' ',10,' ') ||
			'1' ||
			lpad(to_char(dt_remessa_retorno_w,'DDMMYYYY'),8,'0') ||
			lpad(to_char(dt_remessa_retorno_w,'hh24miss'),6,'0') ||
			'000001' ||
			'087' ||
			'01600' ||
			rpad(' ',20,' ') ||
			rpad(' ',20,' ') ||
			rpad(' ',29,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia,
	nr_seq_apres,
	nr_seq_apres_2)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval,
	1,
	1);

/* Header do Lote	*/

qt_registro_lote_w		:= qt_registro_lote_w + 1;
qt_registro_w		:= qt_registro_w + 1;

ds_conteudo_w		:= 	'085' ||
				lpad(qt_registro_lote_w,4,'0') ||
				'1' ||
				'R' ||
				'01' ||
				'  ' ||
				'045' || 
				' ' ||
				'2' ||
				lpad(cd_cgc_w,15,'0') ||
				rpad(' ',20,' ') ||
				cd_agencia_bancaria_w ||
				' ' ||
				'000000000000' ||
				' ' ||
				' ' ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				rpad(' ',40,' ') ||
				'00000001' ||
				to_char(dt_remessa_retorno_w,'DDMMYYYY') ||
				'00000000' ||
				rpad(' ',33,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia,
	nr_seq_apres,
	nr_seq_apres_2)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval,
	2,
	2);
				
/* Segmento P, Segmento Q*/

open	C01;
loop
fetch	C01 into
	cd_agencia_bancaria_w,
	ds_nosso_numero_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_mora_w,
	vl_desconto_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	ds_uf_w;
exit	when C01%notfound;
	begin
	/* Segmento P */
	qt_registro_w		:= qt_registro_w + 1;
	nr_seq_reg_lote_w		:= nr_seq_reg_lote_w + 1;

	ds_conteudo_w		:=	'085' ||
					lpad(qt_registro_lote_w,4,'0') ||
					'3' ||
					lpad(nr_seq_reg_lote_w,5,'0') ||
					'P' ||
					' ' ||
					'01' ||	
					cd_agencia_bancaria_w ||
					' ' ||
					'000000000000' ||
					' ' ||
					' ' ||					
					rpad(' ',20,' ') || 
					'1' || 
					'0' || -- Forma de cad. do t�tulo no banco
					'2' || -- Tipo de documento
					'1' || -- Ident. emiss�o do bloqueto
					'1' || -- Ident. da distribui��o					
					nr_titulo_w ||
					dt_vencimento_w ||
					vl_titulo_w ||
					'00000' ||
					' ' ||
					'99' || -- Esp�cie do t�tulo					
					'A' ||
					dt_emissao_w ||
					'1' || -- C�digo juros mora
					'00000000' ||
					vl_juros_mora_w ||
					'0' || -- C�digo desconto
					'00000000' ||
					vl_desconto_w ||
					'000000000000000' ||
					'000000000000000' ||
					lpad(nr_titulo_w, 25, ' ') ||
					'3' || -- C�digo para protesto
					'00' ||
					'1' ||
					'   ' ||
					'09' ||
					'0000000000' ||
					' ';

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval,
		3,
		nr_seq_reg_lote_w);

	/* Segmento Q */

	qt_registro_w		:= qt_registro_w + 1;
	nr_seq_reg_lote_w		:= nr_seq_reg_lote_w + 1;
	
	ds_conteudo_w		:=	'085' ||
					lpad(qt_registro_lote_w, 4, '0') ||
					'3' ||
					lpad(nr_seq_reg_lote_w, 5, '0') ||
					'Q' ||
					' ' ||
					'01' ||
					ie_tipo_inscricao_w ||
					nr_inscricao_w ||
					nm_pessoa_w ||
					ds_endereco_w ||
					ds_bairro_w ||
					cd_cep_w ||
					ds_cidade_w ||
					ds_uf_w ||
					'0' ||
					'000000000000000' ||
					rpad(' ',40,' ') ||
					'000' ||
					rpad(' ',20,' ') ||
					rpad(' ',8,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(cd_estabelecimento_p,
		ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_envio_banco_seq.nextval,
		3,
		nr_seq_reg_lote_w);

	/* Segmento S 
	
	qt_linhas_w		:= 1;
	ds_mensagem_w		:= '';
	ds_mensagem_ant_w	:= '';
	
	while qt_linhas_w > 0 loop		
		begin
		
		select	substr(replace(replace(elimina_acentuacao(nvl(obter_texto_sem_quebrar_linha(obter_valor_campo_separador(max(ds_mensagem),qt_linhas_w,chr(13) || chr(10))),max(ds_mensagem))),'�',null),'�',null),1,140)
		into	ds_mensagem_w
		from	titulo_receber_mensagem
		where	nr_titulo = nr_titulo_w;
		
		if	(ds_mensagem_w is not null) then
			if 	(ds_mensagem_w = ds_mensagem_ant_w) then				
				qt_linhas_w		:= 0;
			else
				ds_conteudo_w		:= null;
				qt_registro_w		:= qt_registro_w + 1;
				nr_seq_reg_lote_w	:= nr_seq_reg_lote_w + 1;

				ds_conteudo_w	:=	'085' ||
							lpad(qt_registro_lote_w, 4, '0') ||
							'3' ||
							lpad(nr_seq_reg_lote_w, 5, '0') ||
							'S' ||
							' ' ||
							'01' ||
							'2' ||
							'01' ||
							rpad(ds_mensagem_w,140,' ') ||
							'01' ||
							rpad(' ',78,' ');
				
				insert	into w_envio_banco
					(cd_estabelecimento,
					ds_conteudo,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					nr_sequencia,
					nr_seq_apres,
					nr_seq_apres_2)
				values	(cd_estabelecimento_p,
					ds_conteudo_w,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					w_envio_banco_seq.nextval,
					3,
					nr_seq_reg_lote_w);
					
				qt_linhas_w		:= qt_linhas_w + 1;
				ds_mensagem_ant_w	:= ds_mensagem_w;
			end if;
		else
			qt_linhas_w		:= 0;
		end if;	
		
		end;	
	end loop;

	 Fim segmento S */	
	
	end;
end	loop;
close	C01;

/* Trailer de Lote	*/

select	lpad(count(1),6,'0'),
	replace(to_char(sum(c.vl_titulo), 'fm0000000000000.00'),'.','')
into	qt_titulos_cobr_w,
	vl_titulos_cobr_w
from	titulo_receber c,
	titulo_receber_cobr b,
	cobranca_escritural a
where	b.nr_titulo	= c.nr_titulo
and	a.nr_sequencia	= b.nr_seq_cobranca
and	a.nr_sequencia	= nr_seq_cobr_escrit_p;

ds_conteudo_w		:=	'085' ||
				'0001' || 
				'5' ||
				rpad(' ',9,' ') ||
				lpad(qt_registro_w,6,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				lpad(qt_titulos_cobr_w,6,'0') ||
				lpad(vl_titulos_cobr_w,17,'0') ||
				rpad(' ',8,' ') ||
				rpad(' ',117,' ');
qt_registro_w		:= qt_registro_w + 1;

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia,
	nr_seq_apres,
	nr_seq_apres_2)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval,
	5,
	5);

/* Trailer do Arquivo */

ds_conteudo_w	:=	'085' ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			'000001' ||
			lpad(qt_registro_w + 1,6,'0') ||
			'000000' ||
			rpad(' ',205,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia,
	nr_seq_apres,
	nr_seq_apres_2)
values	(cd_estabelecimento_p,
	ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_envio_banco_seq.nextval,
	6,
	6);

end GERAR_COBRANCA_CECRED_240;
/
