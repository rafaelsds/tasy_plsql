create or replace
procedure fa_lib_receita_total(nr_sequencia_p	number,
                               nm_usuario_p		Varchar2) is 

begin
if	(nr_sequencia_p is not null) then
    UPDATE	FA_RECEITA_FARMACIA
	SET	DT_LIBERACAO    = sysdate,	
		NM_USUARIO_LIB	= nm_usuario_p,
		NM_USUARIO	= nm_usuario_p,
		DT_ATUALIZACAO	= sysdate
	WHERE 	NR_SEQUENCIA 	= nr_sequencia_p;
end if;

commit;

end fa_lib_receita_total;
/