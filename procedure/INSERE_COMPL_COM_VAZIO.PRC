create or replace
procedure insere_compl_com_vazio(cd_pessoa_fisica_p  in varchar2,
								nm_usuario_p       in  varchar2,
								nr_seq_compl_p out number) is
	
qt_compl_pf_w number(10);
nr_seq_compl_w number(10);
	
begin

select count(*) 
into	qt_compl_pf_w
from compl_pessoa_fisica
where cd_pessoa_fisica = cd_pessoa_fisica_p
and ie_tipo_complemento = 2;

if (qt_compl_pf_w > 0) then
	select max(nr_sequencia)
	into	nr_seq_compl_w
	from compl_pessoa_fisica
	where cd_pessoa_fisica = cd_pessoa_fisica_p
	and ie_tipo_complemento = 2;
else
	select nvl(max(nr_sequencia),0) + 1
	into	nr_seq_compl_w
	from compl_pessoa_fisica
	where cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	insert into compl_pessoa_fisica (CD_PESSOA_FISICA,
					NR_SEQUENCIA,
					IE_TIPO_COMPLEMENTO,
					DT_ATUALIZACAO,
					NM_USUARIO)
				values (cd_pessoa_fisica_p,
					nr_seq_compl_w,
					2,
					sysdate,
					nm_usuario_p);
	commit;
end if;

nr_seq_compl_p := nr_seq_compl_w;

end insere_compl_com_vazio;
/
