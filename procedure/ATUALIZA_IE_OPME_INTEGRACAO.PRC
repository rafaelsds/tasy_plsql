create or replace
procedure atualiza_ie_opme_integracao(
		ie_opme_integracao_p	varchar2,
		nr_seq_agenda_p		number) is
begin
if	(nr_seq_agenda_p is not null) then
	begin
	update	agenda_paciente
	set	ie_opme_integracao = ie_opme_integracao_p,
		ie_opme_int_status = '0'
	where	nr_sequencia	= nr_seq_agenda_p;
	end;
end if;
commit;
end atualiza_ie_opme_integracao;
/