create or replace procedure gerar_dependente_proc_adic ( cd_tipo_agenda_p		number,
					nm_usuario_p		varchar2,
					nr_seq_agenda_p		number) is

cd_convenio_w		convenio.cd_convenio%type;
cd_setor_atendimento_w	setor_atendimento.cd_setor_atendimento%type;
ie_origem_proced_w	procedimento.ie_origem_proced%type;
cd_estabelecimento_w	agenda.cd_estabelecimento%type;
cd_categoria_w		agenda_consulta.cd_categoria%type;
cd_categoria_convenio_w	agenda_consulta.cd_categoria%type;
ie_tipo_atendimento_w	agenda_consulta.ie_tipo_atendimento%type;
cd_plano_convenio_w	convenio_plano.cd_plano%type;
dt_vigencia_w		date;
nr_seq_exame_w		exame_lab_dependente.nr_seq_exame%type;
ie_gerar_dependente_w	varchar2(10);
qt_procedimento_w   agenda_consulta_proc.qt_procedimento%type;
nr_seq_agenda_proc_w agenda_consulta_proc.nr_sequencia%type;

Cursor C01 is
	select
		a.nr_seq_exame_dep,
		(select min(a.cd_material_exame)
		from material_exame_lab a,
			 exame_lab_material b
		where a.nr_sequencia = b.nr_seq_material
		and b.nr_seq_exame = a.nr_seq_exame_dep
		and b.ie_situacao = 'A'
		and b.ie_prioridade = (select min(c.ie_prioridade)
							   from exame_lab_material c
							   where c.nr_seq_exame = b.nr_seq_exame
							   and c.ie_situacao = b.ie_situacao)) cd_material_exame
	from  exame_laboratorio c,
		  exame_lab_dependente a
	where 	c.nr_seq_exame = nr_seq_exame_w
	and 	nvl(a.cd_convenio,cd_convenio_w) = cd_convenio_w
	and 	coalesce(a.cd_setor_atendimento,cd_setor_atendimento_w,0) = coalesce(cd_setor_atendimento_w,0)
	and 	a.nr_seq_exame = c.nr_seq_exame
	and 	nvl(a.cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and 	NVL(c.ie_situacao,'A') = 'A';

Cursor C02 is
	select nr_seq_exame,
           qt_procedimento
	from agenda_consulta_proc
	where nr_seq_agenda = nr_seq_agenda_p
	and nvl(ie_dependente,'N') = 'N';

procedure insert_proc_adic_dep (nr_seq_exame_pp number, cd_material_exame_pp varchar2, qt_procedimento_pp number) is
	cd_procedimento_pp_w     number(10);
	ie_origem_proced_pp_w    number(10);
	nr_seq_proc_interno_pp_w number(10);
	cd_setor_out        	 number(10);
	ds_procedimento_out	     varchar2(240);
	ds_erro_out		    	 varchar2(255);
begin
	obter_exame_lab_convenio_js(
		null,
		nr_seq_exame_pp,
		cd_convenio_w,
		cd_categoria_convenio_w,
		null,
		null,
		wheb_usuario_pck.get_cd_estabelecimento,
		null,
		cd_setor_out,
		cd_procedimento_pp_w,
		ds_procedimento_out,
		ie_origem_proced_pp_w,
		ds_erro_out
	);

	select max(nr_seq_proc_interno) nr_seq_proc_interno
	into nr_seq_proc_interno_pp_w
	from exame_laboratorio
	where cd_procedimento = cd_procedimento_pp_w
	and ie_origem_proced = ie_origem_proced_pp_w;    
    
	if ((nvl(cd_procedimento_pp_w, 0) > 0) and (nvl(ie_origem_proced_pp_w, 0) > 0)) then
        
      	select agenda_consulta_proc_seq.nextval into nr_seq_agenda_proc_w from dual;
        
		insert into agenda_consulta_proc
		(cd_material_exame,
		cd_procedimento,
		ds_observacao,
		dt_agenda_externa,
		dt_atualizacao,
		dt_atualizacao_nrec,
		ie_executar_proc,
		ie_origem_proced,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia,
		nr_seq_agenda,
		nr_seq_exame,
		nr_seq_proc_interno,
		ie_dependente,
		qt_procedimento
		)
		values
		(
		cd_material_exame_pp, --cd_material_exame,
		cd_procedimento_pp_w, --cd_procedimento,
		null, --ds_observacao,
		null, --dt_agenda_externa,
		sysdate, --dt_atualizacao,
		sysdate, --dt_atualizacao_nrec,
		'S', --ie_executar_proc,
		ie_origem_proced_pp_w, --ie_origem_proced,
		nm_usuario_p, --nm_usuario,
		nm_usuario_p, --nm_usuario_nrec,
		nr_seq_agenda_proc_w, --nr_sequencia,
		nr_seq_agenda_p, --nr_seq_agenda,
		nr_seq_exame_pp, --nr_seq_exame,
		nr_seq_proc_interno_pp_w, --nr_seq_proc_interno,
		'S', --ie_dependente
		qt_procedimento_pp --qt_procedimento
		);
        
        gerar_autor_regra(null,null,null,null,null,null,'AS',nm_usuario_p,null,nr_seq_proc_interno_pp_w,null,nr_seq_agenda_p,null,nr_seq_agenda_proc_w,'','','');
    
	end if;

end insert_proc_adic_dep;

begin

Obter_param_Usuario(866, 313, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_gerar_dependente_w);

if (nvl(ie_gerar_dependente_w,'N') = 'N') then
	return;
end if;

delete agenda_consulta_proc
where nr_seq_agenda = nr_seq_agenda_p
and ie_dependente = 'S';

select 	max(a.cd_estabelecimento),
	max(b.cd_categoria),
	max(b.cd_convenio),
	max(nvl(b.cd_setor_atendimento, a.cd_setor_agenda)),
	max(b.ie_origem_proced),
	max(b.cd_categoria),
	max(b.ie_tipo_atendimento),
	max(b.cd_plano),
	max(b.dt_agenda),
	max(b.nr_seq_exame)
into	cd_estabelecimento_w,
	cd_categoria_convenio_w,
	cd_convenio_w,
	cd_setor_atendimento_w,
	ie_origem_proced_w,
	cd_categoria_w,
	ie_tipo_atendimento_w,
	cd_plano_convenio_w,
	dt_vigencia_w,
	nr_seq_exame_w
from 	agenda a,
	agenda_consulta b
where 	b.nr_sequencia = nr_seq_agenda_p
and 	a.cd_agenda = b.cd_agenda;

if (nvl(nr_seq_exame_w,0) > 0) then
	for c01_w in c01 loop
		insert_proc_adic_dep (c01_w.nr_seq_exame_dep, c01_w.cd_material_exame, 1);
	end loop;
end if;

for c02_w in c02 loop
	nr_seq_exame_w    := c02_w.nr_seq_exame;
	qt_procedimento_w := c02_w.qt_procedimento;
	if (nvl(nr_seq_exame_w,0) > 0) then
		for c01_w in c01 loop
			insert_proc_adic_dep (c01_w.nr_seq_exame_dep, c01_w.cd_material_exame, qt_procedimento_w);
		end loop;
	end if;
end loop;

commit;

end gerar_dependente_proc_adic;
/
