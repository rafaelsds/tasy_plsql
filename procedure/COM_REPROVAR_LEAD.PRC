create or replace
procedure com_reprovar_lead(	nr_sequencia_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

cd_cnpj_w			varchar2(14);
ds_razao_social_w		varchar2(80);
nr_seq_canal_w			number(10,0);
ds_assunto_w			varchar2(100);
ds_mensagem_w			varchar2(2000);
dt_solicitacao_w		date;
ds_email_w			varchar2(255) := '';
ds_email_gestor_w		varchar2(255);
ds_motivo_reprov_w		varchar2(2000);
cd_empresa_w			number(04,0);
ie_status_lead_w		varchar2(03);
nm_pessoa_fisica_w		varchar2(100);
ds_email_contato_w		varchar2(255);
ds_produto_w			varchar2(255);
cd_pessoa_fisica_w		varchar2(10);

begin
select	nr_seq_canal,
	dt_solicitacao,
	cd_cnpj,
	ds_razao_social,
	ds_motivo_reprov,
	ie_status_lead,
	obter_valor_dominio(2668,ie_produto),
	cd_pessoa_fisica
into	nr_seq_canal_w,
	dt_solicitacao_w,
	cd_cnpj_w,
	ds_razao_social_w,
	ds_motivo_reprov_w,
	ie_status_lead_w,
	ds_produto_w,
	cd_pessoa_fisica_w
from	com_solic_lead
where	nr_sequencia = nr_sequencia_p;

if	(ie_status_lead_w = 'P') then
	begin
	if	(nvl(nr_seq_canal_w,0) <> 0) then
		begin
		select	ds_email_contato,
			substr(obter_dados_pf_pj_estab(cd_estabelecimento_p, cd_pessoa_fisica, cd_cnpj,'MCOM'),1,255)
		into	ds_email_contato_w,
			ds_email_w
		from	com_canal
		where	nr_sequencia = nr_seq_canal_w;
		end;
	end if;

	if ds_email_contato_w is not null then
		ds_email_w := ds_email_contato_w;
	end if;

	if	(ds_email_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(251048);
	end if;	

	select	substr(Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'N'),1,100)
	into	nm_pessoa_fisica_w
	from	dual;

	select	obter_empresa_estab(cd_estabelecimento_p)
	into	cd_empresa_w
	from	dual;
	
	select	obter_dados_pf_pj(cd_pessoa_fisica_w,null,'M')
	into	ds_email_gestor_w
	from	dual;
	
	if	(ds_email_gestor_w is not null) then
		ds_email_w := ds_email_w || ';' || ds_email_gestor_w;
	end if;

	ds_assunto_w	:= substr(	'Solicita��o Lead',1,70);	
	ds_mensagem_w	:= substr(	'CNPJ: ' || cd_cnpj_w || chr(13) || chr(10) || 
				'Raz�o Social: ' || ds_razao_social_w || chr(13) || chr(10) ||
				'Produto: ' || ds_produto_w || chr(13) || chr(10) ||
				'Dt. solicita��o: ' || dt_solicitacao_w || chr(13) || chr(10) || 
				'Reprovado em ' || sysdate || ' por ' || nm_pessoa_fisica_w || chr(13) || chr(10) || 
				'Motivo: ' || ds_motivo_reprov_w,1,2000);
	enviar_email(ds_assunto_w, ds_mensagem_w, 'comercial@wheb.com.br', 'comercial@wheb.com.br;' || ds_email_w, 'Tasy', 'M');	

	update	com_solic_lead
	set	dt_aprov_reprov = sysdate,
		nm_usuario	= nm_usuario_p,
		ie_status_lead	= 'R',
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	end;
end if;
commit;
end com_reprovar_lead;
/