create or replace
procedure Importar_prescr_rec is 

nr_prescr_w		number(15,0);
cd_estab_w		number(15,0);
cd_pessoa_w		varchar2(50);
cd_prescricao_w		number(15,0);
dt_prescricao_w		date;
cd_medico_w		varchar2(50);
cd_prescritor_w		varchar2(50);
cd_medico_2w		varchar2(50);
cd_paciente_w		varchar2(50);
cd_recomendacao_w	number(15);
nr_seq_rec_w		number(15);
ds_recomendacao_w	varchar2(4000);
ds_erro_w		varchar2(4000);
	
cursor c01 is
select	cd_estab,
	cd_pessoa,
	cd_prescricao,
	dt_prescricao,
	cd_medico,
	cd_prescritor,
	cd_recomendacao,
	substr(ds_recomendacao,1,4000)
from	w_import_rec
where	rownum < 200;

begin

open C01;
loop
fetch C01 into	
	cd_estab_w,
	cd_pessoa_w,
	cd_prescricao_w,
	dt_prescricao_w,
	cd_medico_w,
	cd_prescritor_w,
	cd_recomendacao_w,
	ds_recomendacao_w;
exit when C01%notfound;
	begin
	select	prescr_medica_seq.nextval
	into	nr_prescr_w
	from	dual;
	
	select	max(cd_pessoa_fisica)
	into	cd_paciente_w
	from	pessoa_fisica
	where	cd_sistema_ant = cd_pessoa_w;
	
	select	max(cd_pessoa_fisica)
	into	cd_medico_2w
	from	pessoa_fisica
	where	cd_sistema_ant = cd_medico_w;
	
	insert into prescr_medica
		(nr_prescricao,
		cd_estabelecimento,
		cd_pessoa_fisica,
		cd_medico,
		dt_prescricao,
		dt_atualizacao,
		nm_usuario,
		nr_horas_validade,
		dt_primeiro_horario,
		ds_observacao)
	values	(nr_prescr_w,
		cd_estab_w,
		cd_paciente_w,
		cd_medico_2w,
		dt_prescricao_w,
		sysdate,
		'OS315694',
		24,
		dt_prescricao_w,
		to_char(cd_prescricao_w));
		
		
	select	prescr_recomendacao_seq.nextval
	into	nr_seq_rec_w
	from	dual;
	
	insert into prescr_recomendacao
		(nr_prescricao,
		nr_sequencia,
		ie_destino_rec,
		dt_atualizacao,
		nm_usuario,
		cd_recomendacao,
		ds_recomendacao)
	values	(nr_prescr_w,
		nr_seq_rec_w,
		'E',
		sysdate,
		'OS315694',
		cd_recomendacao_w,
		ds_recomendacao_w);
	exception when others then
		ds_erro_w	:= sqlerrm;
	end;
end loop;
close C01;

commit;

end Importar_prescr_rec;
/