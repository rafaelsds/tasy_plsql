create or replace
procedure import_apac_tipo_laudo(ie_tipo_laudo_apac_p	varchar2,
			ie_tpcc_p	varchar2,
			ie_exige_cns_p	varchar2,
			cd_procedimento_p	number,
			nm_usuario_p		varchar2) is
 		 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: atualizacao_padrao_parametros tabela sus_procedimento
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
if	(cd_procedimento_p is not null) then
		update	sus_procedimento 
		set	ie_tipo_laudo_apac = ie_tipo_laudo_apac_p, 
			ie_tpcc = ie_tpcc_p, 
			ie_exige_cns = ie_exige_cns_p 
		where	cd_procedimento = cd_procedimento_p 
		and	ie_origem_proced = 7;
		commit;
end if;
end import_apac_tipo_laudo;
/