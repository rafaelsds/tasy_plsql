create or replace PROCEDURE cp_select_interv_plan(
    nr_seq_pat_care_plan_p  NUMBER,
    nr_seq_interv_plan_p    NUMBER,
    nr_seq_pat_cp_problem_p NUMBER,
    nm_usuario_p            VARCHAR2,
    action                  VARCHAR2,
    nr_seq_prescr_diag_p    NUMBER DEFAULT NULL,
    NR_SEQ_INT_PLA_DIAG_P   NUMBER DEFAULT NULL,
    nr_seq_pat_cp_interv_plan_p OUT NUMBER)
IS
  nr_seq_pat_cp_interv_plan_w patient_cp_interv_plan.nr_sequencia%type;
  si_selected_w VARCHAR2(1) := 'N';
BEGIN
  BEGIN
    SELECT X.NR_SEQUENCIA INTO nr_seq_pat_cp_interv_plan_w FROM (
    SELECT nr_sequencia
    FROM patient_cp_interv_plan pcpip
    WHERE pcpip.nr_seq_pat_care_plan = nr_seq_pat_care_plan_p
    AND pcpip.nr_seq_interv_plan     = nr_seq_interv_plan_p
    union
    SELECT nr_sequencia
    FROM patient_cp_interv_plan pcpip
    WHERE nvl(pcpip.nr_seq_pat_care_plan,0) = nvl(nr_seq_pat_care_plan_p,0)
    AND nvl(pcpip.nr_seq_interv_plan,0) = nvl(nr_seq_interv_plan_p,0)
    AND pcpip.NR_SEQ_PRESCR_DIAG = nr_seq_prescr_diag_p
    AND pcpip.NR_SEQ_INT_PLA_DIAG = NR_SEQ_INT_PLA_DIAG_p) X;
  EXCEPTION
  WHEN no_data_found THEN
    nr_seq_pat_cp_interv_plan_w := NULL;
  END;
  IF (nr_seq_pat_cp_interv_plan_w IS NULL) THEN

    SELECT patient_cp_interv_plan_seq.nextval
    INTO nr_seq_pat_cp_interv_plan_w
    FROM dual;
    INSERT
    INTO patient_cp_interv_plan
      (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_seq_pat_care_plan,
        nr_seq_interv_plan,
        dt_start,
        dt_end,
        dt_release,
        si_selected,
        nr_seq_pat_cp_problem,
        nr_seq_prescr_diag,
        NR_SEQ_INT_PLA_DIAG
      )
      VALUES
      (
        nr_seq_pat_cp_interv_plan_w,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        nr_seq_pat_care_plan_p,
        nr_seq_interv_plan_p,
        NULL,
        NULL,
        NULL,
        'Y',
        nr_seq_pat_cp_problem_p,
        nr_seq_prescr_diag_p,
        NR_SEQ_INT_PLA_DIAG_P
      );
  ELSE
    SELECT DECODE(COUNT(1),0,'N','Y')
    INTO si_selected_w
    FROM patient_cp_intervention a
    WHERE a.nr_seq_pat_cp_interv_plan = nr_seq_pat_cp_interv_plan_w
    AND a.si_selected                 = 'Y';

    UPDATE patient_cp_interv_plan
    SET si_selected    = si_selected_w,
      nm_usuario       = nm_usuario_p,
      dt_atualizacao   = sysdate
    WHERE nr_sequencia = nr_seq_pat_cp_interv_plan_w;

  END IF;
  nr_seq_pat_cp_interv_plan_p := nr_seq_pat_cp_interv_plan_w;
  COMMIT;
END cp_select_interv_plan;
/