create or replace
procedure sus_vincular_proc_princ_aih(	nr_interno_conta_p		number,
					nr_seq_proc_princ_p		number,
					nm_usuario_p		Varchar2) is 

nr_sequencia_w		number(10);
					
Cursor C01 is
	select	nr_sequencia
	from	procedimento_paciente
	where	nr_interno_conta = nr_interno_conta_p
	and	nr_seq_proc_princ is null
	and	Sus_Obter_TipoReg_Proc(cd_procedimento,ie_origem_proced,'C',0) <> 3	
	order by nr_sequencia;
begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
	
	update	procedimento_paciente
	set	nr_seq_proc_princ 	= nr_seq_proc_princ_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia = nr_sequencia_w;
	
	end;
end loop;
close C01;

commit;

end sus_vincular_proc_princ_aih;
/