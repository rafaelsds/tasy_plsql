create or replace
procedure man_aprovar_documento(
			nr_documento_p		number,
			cd_pessoa_fisica_p	varchar2,
			nm_usuario_p		varchar2) is 

begin

update	desenv_art_doc_aprovacao
set	dt_aprovacao = sysdate,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_documento = nr_documento_p
and	cd_pessoa_fisica = cd_pessoa_fisica_p;

commit;

end man_aprovar_documento;
/