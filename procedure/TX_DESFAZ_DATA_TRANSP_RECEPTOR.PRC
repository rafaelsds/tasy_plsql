create or replace
procedure tx_desfaz_data_transp_receptor( nr_seq_receptor_p	number) is 
begin
if	(nr_seq_receptor_p is not null) then
	update	tx_receptor
	set	dt_transplante = null
	where	nr_sequencia = nr_seq_receptor_p;
end if;
commit;

end tx_desfaz_data_transp_receptor;
/