create or replace
procedure acc_copiar_genero_alim(cd_estab_origem_p	number,
				cd_estab_destino_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_genero_origem_w	nut_genero_alim.nr_sequencia%type;
nr_seq_genero_novo_w	nut_genero_alim.nr_sequencia%type;
ds_genero_w		nut_genero_alim.ds_genero%type;
cd_material_w		nut_genero_alim.cd_material%type;
qt_genero_w		number(10);
				
Cursor C01 is
	select	nr_sequencia
	from	nut_genero_alim
	where	cd_estabelecimento = cd_estab_origem_p
	order by 1;      
        
       
begin

open C01;
loop
fetch C01 into	
	nr_seq_genero_origem_w;
exit when C01%notfound;
	begin
	
	select	max(ds_genero),
		max(cd_material)
	into	ds_genero_w,
		cd_material_w
	from	nut_genero_alim
	where	nr_sequencia = nr_seq_genero_origem_w;
	
	select	count(*)
	into	qt_genero_w
	from	nut_genero_alim
	where	ds_genero = ds_genero_w
	and	nvl(cd_material,0) = nvl(cd_material_w,0)
	and	cd_estabelecimento = cd_estab_destino_p;
	
	if	(qt_genero_w = 0) then
		select	nut_genero_alim_seq.nextval
		into	nr_seq_genero_novo_w
		from	dual;
		
		insert into nut_genero_alim (	
			nr_sequencia,
			ds_genero,
			dt_atualizacao,
			nm_usuario,
			cd_material,
			ie_situacao,
			qt_conversao,
			cd_estabelecimento,
			nr_seq_tabela,
			qt_fator_correcao,
			qt_fator_coccao,
			ie_arred_req,
			ie_mostra_item_extra,
			nr_seq_tipo_alimento,
			ie_gerar_solic_compra,
			cd_unidade_medida)
		(select	nr_seq_genero_novo_w,
			ds_genero,
			sysdate,
			nm_usuario_p,
			cd_material,
			ie_situacao,
			qt_conversao,
			cd_estab_destino_p,
			nr_seq_tabela,
			qt_fator_correcao,
			qt_fator_coccao,
			ie_arred_req,
			ie_mostra_item_extra,
			nr_seq_tipo_alimento,
			ie_gerar_solic_compra,
			cd_unidade_medida
		from	nut_genero_alim
		where	nr_sequencia = nr_seq_genero_origem_w);
		
		insert into NUT_COMP_NUTRICIONAL (	
			nr_sequencia,
			nr_seq_nutriente,
			nr_seq_gen_alim,
			dt_atualizacao,
			nm_usuario,
			qt_composicao,
			ie_informacao,
			nr_seq_apresent)
		(select	nut_comp_nutricional_seq.nextval,
			nr_seq_nutriente,
			nr_seq_genero_novo_w,
			sysdate,
			nm_usuario_p,
			qt_composicao,
			ie_informacao,
			nr_seq_apresent
		from	nut_comp_nutricional
		where	nr_seq_gen_alim = nr_seq_genero_origem_w);
		
		insert into NUT_GRUPO_GENERO (	
			nr_sequencia,
			nr_seq_genero,
			nr_seq_grupo,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		(select	nut_grupo_genero_seq.nextval,
			nr_seq_genero_novo_w,
			nr_seq_grupo,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p
		from	nut_grupo_genero
		where	nr_seq_genero = nr_seq_genero_origem_w);	
	end if;
	
	end;
end loop;
close C01;

commit;

end acc_copiar_genero_alim;
/