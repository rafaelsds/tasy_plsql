create or replace
procedure  tasy_log_alteracao_update  (ds_justificativa_p varchar2,
				nm_usuario_p varchar2) is

begin

update	tasy_log_alteracao
set	ds_justificativa = substr(ds_justificativa_p,1,255) 
where	nm_usuario = nm_usuario_p
and	nm_tabela   = 'TISS_PARAMETROS_CONVENIO'
and	dt_atualizacao  =  (	select	max(x.dt_atualizacao)
			from	tasy_log_alteracao x
			where	x.nm_usuario  = nm_usuario_p
			and	x.nm_tabela    = 'TISS_PARAMETROS_CONVENIO');
              
commit;
              
end tasy_log_alteracao_update;
/