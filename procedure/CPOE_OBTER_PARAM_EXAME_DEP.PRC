create or replace 
procedure CPOE_Obter_Param_Exame_Dep(	nr_atendimento_p			in	atendimento_paciente.nr_atendimento%type,
										cd_estabelecimento_p		in 	estabelecimento.cd_estabelecimento%type,
										nr_seq_proc_interno_p		in	exame_laboratorio.nr_seq_proc_interno%type,
										nr_seq_rotina_p				in	exame_lab_rotina.nr_seq_exame%type,
										cd_material_exame_p			in	material_exame_lab.cd_material_exame%type,
										cd_convenio_p				out	atend_categoria_convenio.cd_convenio%type,
										cd_plano_convenio_p			out	atend_categoria_convenio.cd_plano_convenio%type,
										cd_categoria_p				out	atend_categoria_convenio.cd_categoria%type,
										nr_seq_material_prescr_p	out	material_exame_lab.nr_sequencia%type,
										ie_origem_proced_cursor_p	out	exame_laboratorio.ie_origem_proced%type,
										ie_origem_proced_sus_p		out	exame_laboratorio.ie_origem_proced%type,
										ie_origem_proced_p			out	exame_laboratorio.ie_origem_proced%type,
										nr_seq_exame_p				out	exame_lab_dependente.nr_seq_exame_dep%type,
										ie_tipo_atendimento_p 		out	atendimento_Paciente.ie_tipo_atendimento%type,
										ie_tipo_convenio_p			out	convenio.ie_tipo_convenio%type,
										cd_edicao_amb_p				out	convenio_amb.cd_edicao_amb%type
									) is

	ie_prioridade_edicao_w		parametro_faturamento.ie_prioridade_edicao_amb%type;
	cd_procedimento_w			number(15);
	vl_ch_honorarios_w			number(15,4) := 1;
    vl_ch_custo_oper_w			number(15,4) := 1;
    vl_m2_filme_w				number(15,4) := 0;
    tx_ajuste_geral_w			number(15,4) := 1;
    nr_seq_cbhpm_edicao_w		number(10);
    dt_inicio_vigencia_w		date;
	dt_vigencia_w				date;

begin

	cd_convenio_p := obter_convenio_atendimento(nr_atendimento_p);
	cd_plano_convenio_p := obter_plano_conv_atend(nr_atendimento_p);
	cd_categoria_p := obter_categoria_atendimento(nr_atendimento_p);
	ie_tipo_atendimento_p := obter_tipo_atendimento(nr_atendimento_p);
	
	dt_vigencia_w := sysdate;
	
	select	max(ie_tipo_convenio)
	  into	ie_tipo_convenio_p
	  from	convenio
	 where	cd_convenio = cd_convenio_p;
	
	select	coalesce(max(ie_prioridade_edicao_amb), 'N')
	  into	ie_prioridade_edicao_w
	  from	parametro_faturamento
	 where	cd_estabelecimento = cd_estabelecimento_p;
	
	ie_origem_proced_sus_p := obter_origem_proced_cat(cd_estabelecimento_p, ie_tipo_atendimento_p, ie_tipo_convenio_p, cd_convenio_p, cd_categoria_p);
	
	if (ie_prioridade_edicao_w = 'N') then
		select	coalesce(max(cd_edicao_amb), 0)
		  into	cd_edicao_amb_p
		  from	convenio_amb
		 where	cd_estabelecimento = cd_estabelecimento_p
		   and	cd_convenio 	   = cd_convenio_p
		   and	cd_categoria	   = cd_categoria_p
		   and  coalesce(ie_situacao,'A') = 'A'
		   and	(dt_inicio_vigencia = (select max(dt_inicio_vigencia)
										 from convenio_amb a
										where a.cd_estabelecimento  = cd_estabelecimento_p
										  and a.cd_convenio         = cd_convenio_p
										  and a.cd_categoria        = cd_categoria_p
										  and coalesce(a.ie_situacao,'A') = 'A'
										  and a.dt_inicio_vigencia <= dt_vigencia_w));
	else
		obter_edicao_proc_conv(	cd_estabelecimento_p,
								cd_convenio_p,
								cd_categoria_p,
								dt_vigencia_w,
								cd_procedimento_w,
								cd_edicao_amb_p,
								vl_ch_honorarios_w,
								vl_ch_custo_oper_w,
								vl_m2_filme_w,
								dt_inicio_vigencia_w,
								tx_ajuste_geral_w,
								nr_seq_cbhpm_edicao_w );
	end if;
	
	select	max(ie_origem_proced)
	  into	ie_origem_proced_cursor_p
	  from	edicao_amb
	 where	cd_edicao_amb = cd_edicao_amb_p;	
		
	if (ie_origem_proced_cursor_p is null) then
		ie_origem_proced_cursor_p := ie_origem_proced_sus_p;
	end if;
	
	select 	max(nr_seq_exame_lab),
			max(ie_origem_proced)
	  into	nr_seq_exame_p,
			ie_origem_proced_p
	  from	proc_interno
	 where	nr_sequencia = nr_seq_proc_interno_p;

	if (nr_seq_rotina_p is not null) then
		select max(a.nr_seq_exame)
		  into nr_seq_exame_p
		  from exame_lab_rotina a
		 where a.nr_sequencia =  nr_seq_rotina_p;
	end if;	

	if (nr_seq_exame_p is null) then
		select max(a.nr_seq_exame)
		  into nr_seq_exame_p
		  from exame_laboratorio a
		 where a.cd_estabelecimento = cd_estabelecimento_p
		   and a.nr_seq_proc_interno = nr_seq_proc_interno_p;
	end if;		

	select	nvl(max(b.nr_sequencia), 0)
	  into	nr_seq_material_prescr_p
	  from	material_exame_lab b
	 where	b.cd_material_exame = cd_material_exame_p;


end CPOE_Obter_Param_Exame_Dep;
/