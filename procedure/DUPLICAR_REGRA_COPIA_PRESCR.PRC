create or replace
PROCEDURE Duplicar_Regra_Copia_prescr(	nr_seq_regra_p		NUMBER,
					nm_usuario_p		VARCHAR2) IS

cd_intervalo_w		VARCHAR2(7);
cd_material_w		NUMBER(10);
ie_agora_w		VARCHAR2(1);
ie_copia_proced_w	VARCHAR2(15);
ie_copiar_w		VARCHAR2(1);
ie_regra_geral_w	VARCHAR2(15);
ie_tipo_item_w		VARCHAR2(15);
nr_seq_proc_interno_w	NUMBER(10);
nr_seq_regra_crit_w	NUMBER(10);
ds_regra_w		VARCHAR2(255);
ie_considera_adep_w	VARCHAR(15);


ie_regra_prim_hor_w	VARCHAR2(15);
ie_regra_horarios_w	VARCHAR2(15);
ie_elimina_hor_w	VARCHAR2(15);
ie_regra_prim_horarios_w VARCHAR2(15);
ie_reordenar_w		VARCHAR2(15);
nr_seq_apres_w		NUMBER(15);
nr_seq_crit_w		NUMBER(15);
nr_seq_regra_w		NUMBER(15);
nr_seq_horario_w	NUMBER(15);
nr_seq_exame_w		NUMBER(10);
ie_regra_prim_hor_proc_w VARCHAR2(15);

ie_acm_w  		    	VARCHAR2(15);
ie_proced_agora_w		VARCHAR2(15);
ie_se_necessario_w		VARCHAR2(15);
ie_dose_especial_w  	VARCHAR2(15);
ie_pca_w 				VARCHAR2(15);
ie_esquema_alternado_w 	VARCHAR2(15);
ie_horario_rep_ant_w 	VARCHAR2(15);
ie_manter_intervalo_w 	VARCHAR2(15);
ie_copia_dialise_w 		VARCHAR2(15);
ie_copiar_reprov_cih_w 	VARCHAR2(15);
ie_copiar_justificativa_w VARCHAR2(15);
cd_grupo_material_w		NUMBER(10);
cd_subgrupo_material_w	NUMBER(10);
cd_classe_material_w	NUMBER(10);



CURSOR c01 IS
SELECT	cd_intervalo,
	cd_material,
	ie_agora,
	ie_copia_proced,
	ie_copiar,
	ie_regra_geral,
	ie_tipo_item,
	nr_seq_proc_interno,
	nr_sequencia,
	nr_seq_exame,
	ie_acm,
	ie_proced_agora,
	ie_se_necessario,
	ie_dose_especial,
	ie_pca,
	ie_esquema_alternado,
	ie_horario_rep_ant,
	ie_manter_intervalo,
	ie_copia_dialise,
	ie_copiar_reprov_cih,
	ie_copiar_justificativa,
	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material,
	nr_seq_apres
FROM	rep_regra_copia_crit
WHERE	nr_seq_regra	= nr_seq_regra_p;

CURSOR c02 IS
SELECT	ie_regra_prim_hor,
	ie_regra_horarios,
	ie_elimina_hor,
	ie_regra_prim_horarios,
	ie_reordenar,
	nr_seq_apres,
	ie_regra_prim_hor_proc
FROM	rep_regra_copia_hor
WHERE	nr_seq_regra_copia	= nr_seq_regra_crit_w;

BEGIN

SELECT	regra_copia_rep_seq.NEXTVAL
INTO	nr_seq_regra_w
FROM	dual;

SELECT	ds_regra || ' - ' || OBTER_DESC_EXPRESSAO(303214)
INTO	ds_regra_w
FROM	regra_copia_rep
WHERE	nr_sequencia	= nr_seq_regra_p;

INSERT INTO regra_copia_rep
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_regra,
	ie_considera_adep)
VALUES	(nr_seq_regra_w,
	SYSDATE,
	nm_usuario_p,
	SYSDATE,
	nm_usuario_p,
	ds_regra_w,
	ie_considera_adep_w);

COMMIT;

OPEN C01;
LOOP
FETCH C01 INTO
	cd_intervalo_w,
	cd_material_w,
	ie_agora_w,
	ie_copia_proced_w,
	ie_copiar_w,
	ie_regra_geral_w,
	ie_tipo_item_w,
	nr_seq_proc_interno_w,
	nr_seq_regra_crit_w,
	nr_seq_exame_w,
	ie_acm_w,
	ie_proced_agora_w,
	ie_se_necessario_w,
	ie_dose_especial_w,
	ie_pca_w,
	ie_esquema_alternado_w,
	ie_horario_rep_ant_w,
	ie_manter_intervalo_w,
	ie_copia_dialise_w,
	ie_copiar_reprov_cih_w,
	ie_copiar_justificativa_w,
	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	nr_seq_apres_w;
EXIT WHEN C01%NOTFOUND;

	SELECT	rep_regra_copia_crit_seq.NEXTVAL
	INTO	nr_seq_crit_w
	FROM	dual;

	INSERT INTO rep_regra_copia_crit
		(nr_sequencia,
		cd_intervalo,
		cd_material,
		dt_atualizacao,
		dt_atualizacao_nrec,
		ie_agora,
		ie_copia_proced,
		ie_copiar,
		ie_regra_geral,
		ie_tipo_item,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_proc_interno,
		nr_seq_regra,
		nr_seq_exame,
		ie_acm,
		ie_proced_agora,
		ie_se_necessario,
		ie_dose_especial,
		ie_pca,
		ie_esquema_alternado,
		ie_horario_rep_ant,
		ie_manter_intervalo,
		ie_copia_dialise,
		ie_copiar_reprov_cih,
		ie_copiar_justificativa,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		nr_seq_apres)

	VALUES	(nr_seq_crit_w,
		cd_intervalo_w,
		cd_material_w,
		SYSDATE,
		SYSDATE,
		ie_agora_w,
		ie_copia_proced_w,
		ie_copiar_w,
		ie_regra_geral_w,
		ie_tipo_item_w,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_proc_interno_w,
		nr_seq_regra_w,
		nr_seq_exame_w,
		ie_acm_w,
		ie_proced_agora_w,
		ie_se_necessario_w,
		ie_dose_especial_w,
		ie_pca_w,
		ie_esquema_alternado_w,
		ie_horario_rep_ant_w,
		ie_manter_intervalo_w,
		ie_copia_dialise_w,
		ie_copiar_reprov_cih_w,
		ie_copiar_justificativa_w,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		nr_seq_apres_w);

	OPEN C02;
	LOOP
	FETCH C02 INTO
		ie_regra_prim_hor_w,
		ie_regra_horarios_w,
		ie_elimina_hor_w,
		ie_regra_prim_horarios_w,
		ie_reordenar_w,
		nr_seq_apres_w,
		ie_regra_prim_hor_proc_w;
	EXIT WHEN C02%NOTFOUND;

		SELECT	rep_regra_copia_hor_seq.NEXTVAL
		INTO	nr_seq_horario_w
		FROM	dual;

		INSERT INTO rep_regra_copia_hor
			(nr_sequencia,
			nr_seq_regra_copia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_regra_prim_hor,
			ie_regra_horarios,
			ie_elimina_hor,
			ie_regra_prim_horarios,
			ie_reordenar,
			nr_seq_apres,
			ie_regra_prim_hor_proc)
		VALUES	(nr_seq_horario_w,
			nr_seq_crit_w,
			SYSDATE,
			nm_usuario_p,
			SYSDATE,
			nm_usuario_p,
			ie_regra_prim_hor_w,
			ie_regra_horarios_w,
			ie_elimina_hor_w,
			ie_regra_prim_horarios_w,
			ie_reordenar_w,
			nr_seq_apres_w,
			ie_regra_prim_hor_proc_w);

	END LOOP;
	CLOSE C02;

END LOOP;
CLOSE C01;

COMMIT;

END Duplicar_Regra_Copia_prescr;
/
