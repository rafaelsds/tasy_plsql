create or replace
procedure pls_atualiza_evento_prox_pgto	(nr_seq_lote_p		pls_lote_pagamento.nr_sequencia%type) is 

--	ROTINA PARA ATUALIZAR CORRETAMENTE OS VENCIMENTOS QUE 'N�O'DEVEM SER GERADOS NO PR�XIMO PAGAMENTO PORQUE J� FOI GERADO O TITULO

nr_seq_pag_prest_venc_w		pls_pag_prest_vencimento.nr_sequencia%type;
nr_seq_evento_prox_pgto_w	pls_parametro_pagamento.nr_seq_evento_prox_pgto%type;
nr_seq_lote_w			pls_lote_pagamento.nr_sequencia%type;
qt_evento_venc_w		pls_integer;

Cursor C00 ( nr_seq_evento_pc pls_pag_prest_venc_valor.nr_seq_evento%type , nr_seq_vencimento_pc pls_pag_prest_venc_valor.nr_seq_vencimento%type ) is
	select	nr_sequencia,
		nr_seq_evento_movto
	from	pls_pag_prest_venc_valor
	where	nr_seq_evento		= nr_seq_evento_pc
	and	nr_seq_vencimento	= nr_seq_vencimento_pc;

Cursor C01 is
	select	a.nr_seq_prestador
	from	pls_parametro_pagamento		p,
		pls_pag_prest_venc_valor	x,
		pls_pag_prest_vencimento	b,
		pls_pagamento_prestador		a
	where	b.nr_sequencia		= x.nr_seq_vencimento
	and	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	x.nr_seq_evento		= p.nr_seq_evento_prox_pgto
	and	b.ie_proximo_pgto	= 'S'
	and	x.ie_tipo_valor		= 'PP'
	group by a.nr_seq_prestador
	order by a.nr_seq_prestador;
	
Cursor C02	(nr_seq_lote_ant_pc	pls_lote_pagamento.nr_sequencia%type) is
	select	a.nr_seq_lote,
		a.vl_pagamento,
		a.nr_seq_prestador,
		x.vl_item,
		p.cd_prestador,
		substr(pls_obter_dados_prestador(a.nr_seq_prestador, 'N'),1,255) nm_prestador
	from	pls_pagamento_item		x,
		pls_pag_prest_vencimento	b,
		pls_pagamento_prestador		a,
		pls_prestador			p
	where	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	x.nr_seq_pagamento	= a.nr_sequencia
	and	p.nr_sequencia		= a.nr_seq_prestador
	and	x.nr_seq_evento		= nr_seq_evento_prox_pgto_w
	and	a.nr_seq_lote		> nr_seq_lote_ant_pc
	union
	select	nr_seq_lote_p nr_seq_lote,
		0 vl_pagamento,
		a.nr_sequencia nr_seq_prestador,
		0 vl_item,
		a.cd_prestador,
		substr(pls_obter_dados_prestador(a.nr_sequencia, 'N'),1,255) nm_prestador
	from	pls_prestador	a
	where	exists	(select	1
			from	pls_pagamento_prestador	b,
				pls_pagamento_item	x
			where	b.nr_seq_prestador	= a.nr_sequencia
			and	x.nr_seq_pagamento	= b.nr_sequencia
			and	x.nr_seq_evento		= nr_seq_evento_prox_pgto_w
			and	b.nr_seq_lote		> nr_seq_lote_ant_pc)
	order by nr_seq_lote, vl_item;
	
cursor c03	(nr_seq_prestador_pc	pls_prestador.nr_sequencia%type,
		nr_seq_lote_pc		pls_lote_pagamento.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_vencimento
	from	pls_parametro_pagamento		p,
		pls_pag_prest_venc_valor	x,
		pls_pag_prest_vencimento	b,
		pls_pagamento_prestador		a
	where	b.nr_sequencia		= x.nr_seq_vencimento
	and	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	x.nr_seq_evento		= p.nr_seq_evento_prox_pgto 
	and	b.ie_proximo_pgto	= 'S'
	and	x.ie_tipo_valor		= 'PP'
	and	a.nr_seq_prestador	= nr_seq_prestador_pc
	and	a.nr_seq_lote		>= nr_seq_lote_pc;

begin

-- BUSCA NA GEST�O DE OPERADORAS QUAL EVENTO QUE EST� CONFIGURADO PARA SER VINCULADO AS APROPRIA��ES
select	max(nr_seq_evento_prox_pgto)
into	nr_seq_evento_prox_pgto_w
from	pls_parametro_pagamento;

-- ESTE CURSOR TEM O OBJETIVO DE ATUALIZAR OS 'VENCIMENTO X APROPRIA��O' QUE GERARAM TITULO A RECEBER MAS N�O MUDOU O STATUS DA APROPRIA��O
for r_C01_w in C01 loop

	-- PEGA O �LTIMO VENCIMENTO GERADO, O QUAL EST� VINCULADO A UMA APROPRIA��O QUE TENHA T�TULO A RECEBER GERADO (POR PRESTADOR)
	select	max(b.nr_sequencia)
	into	nr_seq_pag_prest_venc_w
	from	pls_parametro_pagamento		p,
		pls_pag_prest_venc_valor	x,
		pls_pag_prest_vencimento	b,
		pls_pagamento_prestador		a
	where	b.nr_sequencia		= x.nr_seq_vencimento
	and	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	x.nr_seq_evento		= p.nr_seq_evento_prox_pgto
	and	a.nr_seq_prestador	= r_c01_w.nr_seq_prestador
	and	x.ie_tipo_valor		= 'TR'
	and	b.nr_titulo_receber is not null;
	
	-- AQUI O SISTEMA IR� ATUALIZAR O CAMPO 'IE_PROXIMO_PGTO' PARA 'N' DE TODOS OS VENCIMENTOS QUE FORAM GERADOS ANTERIORMENTE AO VENCIMENTO QUE J� TEM T�TULO A RECEBER GERADO, ESTE OBTIDO NO SELECT ACIMA
	if	(nr_seq_pag_prest_venc_w is not null) then
		update	pls_pag_prest_vencimento b
		set	b.ie_proximo_pgto	= 'N'
		where	b.nr_sequencia	in (	select	b.nr_sequencia
						from	pls_parametro_pagamento		p,
							pls_pag_prest_venc_valor	x,
							pls_pag_prest_vencimento	b,
							pls_pagamento_prestador		a
						where	b.nr_sequencia		= x.nr_seq_vencimento
						and	a.nr_sequencia		= b.nr_seq_pag_prestador
						and	x.nr_seq_evento		= p.nr_seq_evento_prox_pgto
						and	a.nr_seq_prestador	= r_c01_w.nr_seq_prestador
						and	b.ie_proximo_pgto	= 'S'
						and	x.ie_tipo_valor		= 'PP'
						and	b.nr_sequencia < nr_seq_pag_prest_venc_w
						and	b.nr_seq_evento_movto is null
						and	a.ie_cancelamento is null);
	end if;
end loop;

-- SE H� UM EVENTO PARA APROPRIA��O CONFIGURA��O NA GEST�O DE OPERADORAS ENT�O
if	(nr_seq_evento_prox_pgto_w is not null) then

	-- O SISTEMA BUSCA TODOS OS ITENS DOS �LTIMOS 10 LOTES DE PAGAMENTOS GERADOS
	for r_C02_w in C02(nr_seq_lote_p - 10) loop
		
		-- BUSCA O �LTIMO LOTE DE PAGAMENTO QUE GEROU UM ITEM PARA UM DETERMINADO PRESTADOR O QUAL ESTE ITEM SEJA DO EVENTO DE APROPRIA��O CONFIGURA��O NA GEST�O DE OPERADORAS E QUE ESTEJA EM UM LOTE ANTERIOR AOS �LTIMOS 10 LOTES
		select	max(a.nr_seq_lote)
		into	nr_seq_lote_w
		from	pls_pagamento_item		x,
			pls_pag_prest_vencimento	b,
			pls_pagamento_prestador		a
		where	a.nr_sequencia		= b.nr_seq_pag_prestador
		and	x.nr_seq_pagamento	= a.nr_sequencia
		and	a.nr_seq_prestador 	= r_C02_w.nr_seq_prestador
		and	x.nr_seq_evento		= nr_seq_evento_prox_pgto_w
		and	a.vl_pagamento		> 0
		and	b.ie_proximo_pgto	= 'N'
		and	a.nr_seq_lote		<= r_C02_w.nr_seq_lote
		and	a.nr_Seq_lote 		>= nr_seq_lote_p - 10;
		
		-- SE ENCONTROU UM LOTE DE PAGAMENTO QUE TENHA UM ITEM PARA UM DETERMINADO PRESTADOR O QUAL ESTE ITEM SEJA DO EVENTO DE APROPRIA��O CONFIGURA��O NA GEST�O DE OPERADORAS E QUE ESTEJA EM UM LOTE ANTERIOR AOS �LTIMOS 10 LOTES ENT�O
		if	(nr_seq_lote_w is not null) then
		
			-- O SISTEMA BUSCA TODOS OS 'VENCIMENTO X APROPRIA��O' O QUAL ESTES EST�O COM O 'IE_PROXIMO_PGTO' = 'S' E COM O 'IE_TIPO_VALOR' = 'PP' E QUE ESTE VENCIMENTO FOI GERADOS DEPOIS DE UM LOTE QUE J� N�O DEVERIA MAIS APROPRIAR
			for r_C03_w in C03(r_C02_w.nr_seq_prestador,nr_seq_lote_w) loop
			
				-- VERIFICA QUANTAS APROPRIA��ES H� NO VENCIMENTO EM QUEST�O
				select	count(1)
				into	qt_evento_venc_w
				from	pls_pag_prest_venc_valor
				where	nr_seq_vencimento	= r_C03_w.nr_seq_vencimento;
				
				-- SE O VENCIMENTO EM QUEST�O TEM MAIS DE UMA APROPRIA��ES ENT�O
				if	(qt_evento_venc_w > 1) then
					-- CANCELA AS APROPRIA��ES DESTE VENCIMENTO QUE ESTEJAM VINCULADAS AO EVENTO DE APROPRIA��O CONFIGURA��O NA GEST�O DE OPERADORAS, TAMB�M DESVINCULA ESTA APROPRIA��O DO EVENTO MOVIMENTO E DELETE ESTE EVENTO MOVIMENTO
					for r_C00_w in C00 ( nr_seq_evento_prox_pgto_w ,  r_C03_w.nr_seq_vencimento ) loop
					
						update	pls_pag_prest_venc_valor
						set	ie_tipo_valor		= 'IC',
							nr_seq_evento_movto	= null
						where	nr_seq_evento		= nr_seq_evento_prox_pgto_w
						and	nr_seq_vencimento	= r_C03_w.nr_seq_vencimento
						and	nr_sequencia		= r_c00_w.nr_sequencia;
						
						update	pls_pag_prest_vencimento
						set	nr_seq_evento_movto	= null
						where	nr_sequencia		= r_C03_w.nr_seq_vencimento
						and	nr_seq_evento_movto	= r_c00_w.nr_seq_evento_movto;
						
						delete	pls_evento_movimento
						where	nr_sequencia		= r_c00_w.nr_seq_evento_movto;
					end loop;
				else	-- SEN�O, SE O VENCIMENTO TEM APENAS UMA APROPRIA��O, SETA O VENCIMENTO PARA N�O MAIS APROPRIAR
					update	pls_pag_prest_vencimento
					set	ie_proximo_pgto		= 'N'
					where	nr_sequencia		= r_C03_w.nr_seq_vencimento;
				end if;
			end loop;
			
		end if;
		
	end loop;
end if;

end pls_atualiza_evento_prox_pgto;
/