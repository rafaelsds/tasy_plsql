create or replace 
procedure Excluir_Glosa_Valor_Mat(nr_sequencia_p	number,
						nm_usuario_p	varchar) is

ie_status_acerto_w	number(1);
nr_sequencia_w		number(10);

begin

select nvl(min(nr_sequencia),0)
into nr_sequencia_w
from material_atend_paciente
where (nr_atendimento, dt_entrada_unidade, cd_material, dt_atendimento, cd_setor_atendimento) =
	(select nr_atendimento, dt_entrada_unidade, cd_material, dt_atendimento, cd_setor_atendimento
	 from material_atend_paciente
	 where nr_sequencia = nr_sequencia_p)
  and nr_sequencia <> nr_sequencia_p
  and qt_material = 0;

if (nr_sequencia_w <> 0) then
	select nvl(max(b.ie_status_acerto),0)
	into ie_status_acerto_w
	from conta_paciente b,
		material_atend_paciente a
	where a.nr_interno_conta = b.nr_interno_conta
	  and a.nr_sequencia = nr_sequencia_w;

	dbms_output.put_line(wheb_mensagem_pck.get_texto(793128) || ' ' || ie_status_acerto_w);
	if (ie_status_acerto_w = 1) then
	dbms_output.put_line(wheb_mensagem_pck.get_texto(803395));
		delete material_atend_paciente
		where nr_sequencia = nr_sequencia_w;
		commit;
	end if;
end if;

end Excluir_Glosa_Valor_Mat;
/
