create or replace
procedure gerar_seq_lab_externo(nr_seq_lote_p number,
				cd_estabelecimento_p number,
				nm_usuario_p		Varchar2) is 

nr_seq_lab_ext_w	number(10);	
nr_prescricao_w		number(10);
nr_seq_prescr_w		number(6);
cursor c01 is
select	distinct
	nr_prescricao
from	prescr_procedimento
where	nr_seq_lote_externo = nr_seq_lote_p;

cursor c02 is
select	distinct
	nr_sequencia
from 	prescr_procedimento
where 	nr_prescricao = nr_prescricao_w
and	nr_seq_lote_externo = nr_seq_lote_p;

begin

open c01;
loop
fetch c01 into
	nr_prescricao_w;
exit when c01%notfound;

	select	nvl(max(somente_numero(nr_seq_lab_ext)),0)+1
	into	nr_seq_lab_ext_w
	from 	prescr_procedimento;
	
	if	(nr_seq_lab_ext_w = 1) then
	
		select	nvl(nr_Seq_inicial_externo,0)
		into	nr_seq_lab_ext_w
		from	lab_parametro
		where	cd_estabelecimento = cd_estabelecimento_p;
		
	end if;

	open c02;
	loop
	fetch c02 into
		nr_seq_prescr_w;
	exit when c02%notfound;
	
		update	prescr_procedimento
		set	nr_seq_lab_ext	= nr_seq_lab_ext_w,
			nm_usuario	= nm_usuario_p
		where	nr_prescricao 	= nr_prescricao_w
		and	nr_sequencia	= nr_seq_prescr_w;
	
	END LOOP;
	CLOSE C02;

END LOOP;
CLOSE C01;

commit;

end gerar_seq_lab_externo;
/
