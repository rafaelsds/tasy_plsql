create or replace
procedure INSERIR_ITEM_TEMPLATE( nr_sequencia_p number,
			nr_seq_conteudo_p	number,
			nm_atributo_p	varchar2,
			nr_seq_apres_p	number,
			nm_usuario_p		Varchar2) is 

begin

insert into ehr_temp_sql_item(
			NR_SEQUENCIA, 
			NR_SEQ_SQL, 
			NR_SEQ_CONTEUDO, 
			NM_ATRIBUTO, 
			DT_ATUALIZACAO, 
			NM_USUARIO, 
			NR_SEQ_APRES)
values (	ehr_temp_sql_item_seq.nextval, 
			nr_sequencia_p, 
			nr_seq_conteudo_p, 
			SUBSTR(nm_atributo_p, 1, 60), 
			sysdate, 
			nm_usuario_p, 
			nr_seq_apres_p);

commit;

end INSERIR_ITEM_TEMPLATE;
/