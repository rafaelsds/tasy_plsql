create or replace procedure Enviar_CI_prescr_procedimento
				(nr_prescricao_p	number,
				cd_estabelecimento_p	number,
				cd_perfil_p		number,
				nm_usuario_p		varchar2) is

ds_titulo_w		varchar2(255);
nm_paciente_w		varchar2(255);
nm_medico_w		varchar2(255);
nm_medico_exec_w	varchar2(255);
ds_comunicado_w		varchar2(4000);
ds_comunic_w		varchar2(4000);
ds_procedimento_w	varchar2(4000);
nm_usuario_destino_w	varchar2(255);
cd_medico_atend_w	varchar2(255);
ds_lista_usuario_destino_w	varchar2(1000);
ds_observacao_w		varchar2(255);
ds_dado_clinico_w	varchar2(255);
cd_prescritor_w		varchar2(255);
ds_unidade_w		varchar2(255);
ie_enviar_email_w	varchar2(5);
cd_procedimento_w	number(15,0);
nr_atendimento_w	number(15,0);
ds_email_w		varchar2(4000);
cd_perfil_w		varchar2(4000);
cd_setor_envio_w    varchar2(4000);
ds_email_usuario_w	varchar2(4000);
ds_final_w		varchar2(4000);
nr_sequencia_w		number(15,0);
nr_sequencia_ww		number(15,0);
cd_area_procedimento_w	number(15,0);
cd_perfil_destino_w	number(15,0);
cd_setor_destino_w	number(15,0);
cd_especialidade_w	number(15,0);
cd_grupo_proc_w		number(15,0);
cd_setor_w		number(15,0);
ie_origem_proced_w	number(15,0);
ie_tipo_atendimento_w	number(15,0);
nr_seq_regra_w		number(15,0);
ie_enviar_prescritor_w	varchar2(1);
cd_medico_w		varchar2(50);
NM_USUARIO_MEDICO_W	varchar2(50);
cd_pessoa_fisica_w	varchar2(50);
ie_enviar_executor_w	varchar2(1);
nr_seq_comunic_w	number(15,0);
nr_seq_exame_w		number(15,0);
cd_convenio_w		number(5);
nr_seq_proc_interno_w	number(15,0);
ds_quebra_w		varchar2(2) := chr(13) || chr(10);

cd_intervalo_w		varchar2(30);
ds_horarios_w		varchar2(255);
ds_intervalo_w		varchar2(255);
ds_setor_exec_w		varchar2(255);
ds_setor_prescr_w	varchar2(255);	
dt_liberacao_w		date;
dt_prescricao_w		date;
dt_prev_exec_w		date;
qt_procedimento_w	number(8,3);
ds_convenio_w		varchar2(255);
cd_protocolo_w		number(10);
nr_seq_protocolo_w	number(10);
cd_medico_exec_w	number(15,0);
qt_exames_w		number(15);
qt_prescr_w		number(15);
ie_medico_responsavel_w	varchar2(1);
ie_aplica_prescr_eup_w	varchar2(1);

cursor c01 is
select	a.nr_sequencia,
	a.ds_titulo,
	a.ds_comunicado,
	a.cd_area_procedimento,
	a.cd_especialidade,
	a.cd_grupo_proc,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.ie_tipo_atendimento,
	nvl(a.ie_enviar_prescritor,'S'),
	a.nr_seq_proc_interno,
	a.cd_protocolo,
	a.nr_seq_protocolo,
	nvl(a.ie_enviar_executor,'S'),
	a.cd_pessoa_fisica,
	a.nr_seq_exame,
	a.ie_enviar_email,
	a.qt_exames,
	nvl(a.ie_medico_responsavel,'S'),
	nvl(a.ie_aplica_prescr_eup,'S')
from	rep_regra_envio_ci_proced a
where	obter_se_envia_ci_setor(a.nr_sequencia, cd_setor_w) = 'S'
and		((cd_convenio is null) or (cd_convenio = cd_convenio_w))
and		((cd_perfil is null) or (cd_perfil = obter_Perfil_ativo))
and		nvl(cd_estabelecimento,nvl(cd_estabelecimento_p,0))	= nvl(cd_estabelecimento_p,0)
and	nvl(ie_job,'N') = 'N';

cursor c02 is
select	b.cd_perfil
from	rep_perfil_ci_proced b
where	b.nr_seq_regra	= nr_sequencia_ww;

cursor c03 is
select	b.cd_setor_recebimento 
from	rep_regra_ci_proced_setor b
where	b.nr_seq_regra	= nr_sequencia_ww
and    b.cd_setor_recebimento is not null;

cursor c14 is
select	substr(a.DS_PROCEDIMENTO,1,80),
	substr(c.ds_observacao,1,255),
	substr(c.ds_dado_clinico,1,255),
	c.cd_procedimento,
	nvl(obter_desc_intervalo_prescr(c.cd_intervalo),c.cd_intervalo),
	obter_desc_intervalo(c.cd_intervalo),
	substr(c.ds_horarios,1,255),
	c.dt_prev_execucao,
	c.qt_procedimento,
	substr(obter_nome_setor(c.cd_setor_atendimento),1,80),
	c.cd_medico_exec
from	prescr_medica p,
	estrutura_procedimento_v a,
	prescr_procedimento c
where	a.cd_procedimento	= c.cd_procedimento
and	a.ie_origem_proced	= c.ie_origem_proced
and	c.nr_prescricao		= p.nr_prescricao
and	c.nr_prescricao		= nr_prescricao_p
and	((cd_procedimento_w	is null) or
	 (c.cd_procedimento	= cd_procedimento_w))
and	((ie_origem_proced_w	is null) or
	 (c.ie_origem_proced	= ie_origem_proced_w) or (cd_procedimento_w is null))
and	((cd_grupo_proc_w is null) or
	 (a.cd_grupo_proc = cd_grupo_proc_w))
and	((cd_area_procedimento_w is null) or
	 (a.cd_area_procedimento = cd_area_procedimento_w))
and	((p.cd_prescritor = cd_pessoa_fisica_w) or (cd_pessoa_fisica_w is null))
and	((cd_especialidade_w is null) or
	 (a.cd_especialidade = cd_especialidade_w))
and	((nr_seq_proc_interno_w is null) or
	 (c.nr_seq_proc_interno	= nr_seq_proc_interno_w))
and	((c.nr_seq_exame	= nr_seq_exame_w) or
	 (nr_seq_exame_w is null))
and	((ie_tipo_atendimento_w is null) or
	 (obter_tipo_atendimento(obter_atendimento_prescr(nr_prescricao_p)) = ie_tipo_atendimento_w))
and	((cd_protocolo_w is null) or
	 (p.cd_protocolo = cd_protocolo_w))
and	((ie_aplica_prescr_eup_w = 'S') or
	(nvl(p.cd_funcao_origem,924) <> 916))
and	((nr_seq_protocolo_w is null) or
	 (p.nr_seq_protocolo = nr_seq_protocolo_w));
	 
cursor	c15 is
select	b.nm_usuario_regra
from	rep_regra_envio_ci_proced a,
	rep_usuario_ci_proced b
where	a.nr_sequencia	= b.nr_seq_regra
and	b.nr_seq_regra	= nr_sequencia_ww
--and	    nvl(a.ie_medico_responsavel,'S') = 'S'
and	obter_se_envia_ci_setor(a.nr_sequencia, cd_setor_w) = 'S';

begin

select	min(nr_sequencia) 
into	nr_sequencia_w
from	comunic_interna_classif 
where	ie_tipo	= 'F';

select	max(substr(obter_nome_pf(b.cd_pessoa_fisica),0,255)),
	max(substr(obter_nome_pf(c.cd_pessoa_fisica),0,255)),
	max(a.cd_setor_atendimento),
	max(a.nr_atendimento),
	max(obter_unidade_atendimento(a.nr_atendimento,'A','U')),
	max(a.dt_prescricao),
	max(nvl(a.dt_liberacao_medico,a.dt_liberacao)),
	max(obter_nome_setor(a.cd_setor_atendimento)),
	max(a.cd_medico),
	max(obter_convenio_Atendimento(a.nr_atendimento))
into	nm_paciente_w,
	nm_medico_w,
	cd_setor_w,
	nr_atendimento_w,
	ds_unidade_w,
	dt_prescricao_w,
	dt_liberacao_w,
	ds_setor_prescr_w,
	cd_medico_w,
	cd_convenio_w
from	pessoa_fisica c,
		pessoa_fisica b,
		prescr_medica a
where	a.cd_prescritor		= c.cd_pessoa_fisica
and		a.cd_pessoa_fisica = b.cd_pessoa_fisica
and		a.nr_prescricao	= nr_prescricao_p;

if	(cd_convenio_w > 0) then
	ds_convenio_w	:= substr(obter_desc_convenio(cd_convenio_w),1,100);
end if;


open c01;
loop
fetch c01 into
	nr_sequencia_ww,
	ds_titulo_w,
	ds_comunic_w,
	cd_area_procedimento_w,
	cd_especialidade_w,
	cd_grupo_proc_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	ie_tipo_atendimento_w,
	ie_enviar_prescritor_w,
	nr_seq_proc_interno_w,
	cd_protocolo_w,
	nr_seq_protocolo_w,
	ie_enviar_executor_w,
	cd_pessoa_fisica_w,
	nr_seq_exame_w,
	ie_enviar_email_w,
	qt_exames_w,
	ie_medico_responsavel_w,
	ie_aplica_prescr_eup_w;
exit when c01%notfound;
	ds_comunicado_w := null;
	
	if	(nvl(qt_exames_w,0) > 0) then
		select	count(*)
		into	qt_prescr_w
		from	prescr_medica p,
			estrutura_procedimento_v a,
			prescr_procedimento c
		where	a.cd_procedimento	= c.cd_procedimento
		and	a.ie_origem_proced	= c.ie_origem_proced
		and	c.nr_prescricao		= p.nr_prescricao
		and 	PKG_DATE_UTILS.start_of(c.dt_prev_execucao,'month',0) = PKG_DATE_UTILS.start_of(sysdate,'month',0)
		and	((cd_procedimento_w	is null) or
			 (c.cd_procedimento	= cd_procedimento_w))
		and	((ie_origem_proced_w	is null) or
			 (c.ie_origem_proced	= ie_origem_proced_w) or (cd_procedimento_w is null))
		and	((cd_grupo_proc_w is null) or
			 (a.cd_grupo_proc = cd_grupo_proc_w))
		and	((cd_area_procedimento_w is null) or
			 (a.cd_area_procedimento = cd_area_procedimento_w))
		and	((p.cd_prescritor = cd_pessoa_fisica_w) or (cd_pessoa_fisica_w is null))
		and	((cd_especialidade_w is null) or
			 (a.cd_especialidade = cd_especialidade_w))
		and	((nr_seq_proc_interno_w is null) or
			 (c.nr_seq_proc_interno	= nr_seq_proc_interno_w))
		and	((c.nr_seq_exame	= nr_seq_exame_w) or
			 (nr_seq_exame_w is null))
		and	((ie_tipo_atendimento_w is null) or
			 (obter_tipo_atendimento(obter_atendimento_prescr(nr_prescricao_p)) = ie_tipo_atendimento_w))
		and	((cd_protocolo_w is null) or
			 (p.cd_protocolo = cd_protocolo_w))
		and	((ie_aplica_prescr_eup_w = 'S') or
			 (nvl(p.cd_funcao_origem,924) <> 916))
		and	((nr_seq_protocolo_w is null) or
			 (p.nr_seq_protocolo = nr_seq_protocolo_w));
	end if;
	
	if	(nvl(qt_exames_w,0) = 0) or
		(nvl(qt_exames_w,0) < nvl(qt_prescr_w,0)) then
	
		open c14;
		loop
		fetch c14 into	
			ds_procedimento_w,
			ds_observacao_w,
			ds_dado_clinico_w,
			cd_procedimento_w,
			cd_intervalo_w,
			ds_intervalo_w,
			ds_horarios_w,
			dt_prev_exec_w,
			qt_procedimento_w,
			ds_setor_exec_w,
			cd_medico_exec_w;
		exit when c14%notfound;
			if	(ds_comunicado_w is not null) then
				ds_comunicado_w := substr(ds_comunicado_w || ds_quebra_w,1,4000);
			end if;
			--Procedimento
			ds_comunicado_w := substr(ds_comunicado_w || obter_desc_expressao(720319)||': ' || cd_procedimento_w ||  ' - ' || ds_procedimento_w || ds_quebra_w,1,4000);
      --Quantidade
			ds_comunicado_w := substr(ds_comunicado_w || obter_desc_expressao(297049)||':   ' || qt_procedimento_w || ds_quebra_w,1,4000); 
			
			if	(dt_prev_exec_w is not null) then
      --Data Prevista execu��o
				ds_comunicado_w := substr(ds_comunicado_w || obter_desc_expressao(287132)||':   ' || PKG_DATE_FORMATERS.TO_VARCHAR(dt_prev_exec_w,'timestamp', cd_estabelecimento_p, nm_usuario_p) || ds_quebra_w,1,4000); 
			end if;
			
			if	(cd_medico_exec_w is not null) then
      --M�dico executor
				ds_comunicado_w	:= substr(ds_comunicado_w || obter_desc_expressao(722393)||': ' || substr(obter_nome_pf(cd_medico_exec_w),1,70) || ds_quebra_w,1,4000);
			end if;
			
			if	(nvl(cd_intervalo_w, ds_intervalo_w) is not null) then
      --Intervalo
				ds_comunicado_w := substr(ds_comunicado_w || obter_desc_expressao(292190)||':   ' || nvl(cd_intervalo_w, ds_intervalo_w) || ds_quebra_w,1,4000);
			end if;
			
			if	(ds_horarios_w is not null) then
      --Hor�rios
				ds_comunicado_w := substr(ds_comunicado_w || obter_desc_expressao(291449)||':   ' || ds_horarios_w || ds_quebra_w,1,4000); 
			end if;
			
			if	(ds_setor_exec_w is not null) then
      --Setor de execu��o
				ds_comunicado_w := substr(ds_comunicado_w || obter_desc_expressao(298393)||':   ' || ds_setor_exec_w || ds_quebra_w,1,4000);
			end if;
			
			if	(ds_observacao_w is not null) then
      --Observa��o
				ds_comunicado_w	:= substr(ds_comunicado_w || obter_desc_expressao(667998)||': ' || ds_observacao_w|| ds_quebra_w,1,4000);		
			end if;
			
			if	(ds_dado_clinico_w is not null) then
      --Indica��o cl�nica
				ds_comunicado_w	:= substr(ds_comunicado_w || obter_desc_expressao(291841)||': ' || ds_dado_clinico_w|| ds_quebra_w,1,4000);
			end if;
			
			if	(ie_enviar_executor_w = 'S') and
				(cd_medico_exec_w is not null) then
				select	max(nm_usuario)
				into	nm_usuario_destino_w
				from	usuario
				where	cd_pessoa_fisica = cd_medico_exec_w
				and		nvl(ie_situacao,'A') = 'A';
				
				if	(ds_lista_usuario_destino_w is not null) then
					ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || ',',1,1000);
				end if;
				
				ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || nm_usuario_destino_w,1,1000);			
				
			end if;
		end loop;
		close C14;
	end if;
	
	cd_perfil_w := null;
	open c02;
	loop
	fetch c02 into
		cd_perfil_destino_w;
	exit when c02%notfound;
		if	(cd_perfil_w is not null) or
			(cd_perfil_w <> '') then
			cd_perfil_w := cd_perfil_w || ',';
		end if;
		
		cd_perfil_w := substr(cd_perfil_w || cd_perfil_destino_w,1,1000);	
		
	end loop;
	close c02;
	
	cd_setor_envio_w := null;
	open c03;
	loop
	fetch c03 into
		cd_setor_destino_w;
	exit when c03%notfound;
		if	(cd_setor_envio_w is not null) or
			(cd_setor_envio_w <> '') then
			cd_setor_envio_w := cd_setor_envio_w || ',';
		end if;
		
		cd_setor_envio_w := substr(cd_setor_envio_w || cd_setor_destino_w,1,1000);	
		
	end loop;
	close c03;
	
	
	if	(ds_comunicado_w is not null) then
	
		open C15;
		loop
		fetch C15 into
			nm_usuario_destino_w;
		exit when C15%notfound;
			if	(ie_enviar_prescritor_w = 'S') or
				(nm_usuario_destino_w <> nm_usuario_P) then
				if	(ds_lista_usuario_destino_w is not null) then
					ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || ',',1,1000);
				end if;
				
				ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || nm_usuario_destino_w,1,1000);
				
				select	max(ds_email)
				into	ds_email_usuario_w
				from	usuario
				where	substr(obter_se_email_valido(ds_email),1,1) = 'S'
				and	upper(nm_usuario) = upper(nm_usuario_destino_w);
				
				if	(ds_email_usuario_w is not null) then
					if	(ds_email_w is not null) then
						ds_email_w	:= ds_email_usuario_w || ';' || ds_email_w;
					else
						ds_email_w	:= ds_email_usuario_w;
					end if;
				end if;
			end if;
		end loop;
		close C15;
		
		select	max(cd_pessoa_fisica)
		into	cd_prescritor_w
		from	usuario
		where	nm_usuario	= nm_usuario_p;
		
		if	(cd_medico_w <> cd_prescritor_w) and
			(ie_enviar_prescritor_w = 'S') then
			select	max(nm_usuario)
			into	nm_usuario_medico_w
			from	usuario
			where	cd_pessoa_fisica	= cd_medico_w;
						
			if	(nm_usuario_medico_w is not null) then
				if	(ds_lista_usuario_destino_w is not null) then
					ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || ',',1,1000);
				end if;
				
				ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || nm_usuario_medico_w,1,1000);
			end if;
		end if;
		
		if	(ie_medico_responsavel_w = 'S') then
			select	max(substr(Obter_medico_resp_atend(nr_atendimento_w,'C'),1,80))
			into	cd_medico_atend_w
			from	dual;
			
			if	(cd_medico_atend_w <> cd_medico_w) then
				select	max(nm_usuario)
				into	nm_usuario_medico_w
				from	usuario
				where	cd_pessoa_fisica	= cd_medico_atend_w;
				
				if	(ie_enviar_prescritor_w = 'S') or
					(nm_usuario_medico_w <> nm_usuario_P) then
					if	(nm_usuario_medico_w is not null) then
						if	(ds_lista_usuario_destino_w is not null) then
							ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || ',',1,1000);
						end if;
						
						ds_lista_usuario_destino_w := substr(ds_lista_usuario_destino_w || nm_usuario_medico_w,1,1000);
					end if;
				end if;
			end if;
		end if;	
		
		ds_comunic_w	:=	
          --Prescri��o
          obter_desc_expressao(296208)||': ' || nr_prescricao_p || ds_quebra_W ||
          --Atendimento
					obter_desc_expressao(283863)||': ' || nr_atendimento_w || ds_quebra_W ||
          --Paciente
					obter_desc_expressao(295156)||': ' || nm_paciente_w || ds_quebra_W ||
          --Setor da prescri��o
					obter_desc_expressao(298386)||': ' || ds_setor_prescr_w || ds_quebra_W ||
          --Leito do paciente
					obter_desc_expressao(345228)||': ' || ds_unidade_w || ds_quebra_W ||
          --Conv�nio
					obter_desc_expressao(286193)||': '|| ds_convenio_w || ds_quebra_w ||
          --Dt Prescri��o
					obter_desc_expressao(335718)||':' ||PKG_DATE_FORMATERS.TO_VARCHAR(dt_prescricao_W,'timestamp', cd_estabelecimento_p, nm_usuario_p) || ds_quebra_W ||
          --Dt Libera��o
					obter_desc_expressao(287026)||':' ||PKG_DATE_FORMATERS.TO_VARCHAR(dt_liberacao_w,'timestamp', cd_estabelecimento_p, nm_usuario_p) || ds_quebra_W ||
          --Prescritor
					obter_desc_expressao(296217)||': ' || nm_medico_w || ds_quebra_W|| ds_quebra_W ||
					ds_comunic_w;
		
		if	(ds_titulo_w is not null) then
		
			if	(ie_enviar_email_w = 'S') and
				(nvl(ds_email_w,'X') <> 'X') then
				enviar_email(	ds_titulo_w,
						ds_comunic_w,
						null,
						ds_email_w,
						nm_usuario_p,
						'M');				
			end if;
		
			select	comunic_interna_seq.nextval
			into	nr_seq_comunic_w
			from	dual;
			
			ds_final_w	:= substr(ds_comunic_w || ds_quebra_w || ds_quebra_w|| ds_comunicado_w,1,3000);
			
			insert	into comunic_interna(	
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				nm_usuario_destino,
				dt_atualizacao,
				ie_geral,
				ie_gerencial,
				ds_perfil_adicional,
				ds_setor_adicional,
				nr_sequencia,
				nr_seq_classif,
				dt_liberacao)
			values(
				sysdate,
				ds_titulo_w,
				ds_final_w,
				nm_usuario_p,
				ds_lista_usuario_destino_w,
				sysdate,
				'N',
				'N',
				cd_perfil_w,
				cd_setor_envio_w,
				nr_seq_comunic_w,
				nr_sequencia_w,
				sysdate);
				
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
				
			if	(ie_enviar_prescritor_w = 'N') then
				insert into comunic_interna_lida 
				values(	nr_seq_comunic_w,
					nm_usuario_p,
					sysdate);
			end if;
			
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	
		end if;
		ds_lista_usuario_destino_w := null;
	end if;
	
end loop;
close C01; 

END Enviar_CI_prescr_procedimento;
/