create or replace PROCEDURE iud_work_status(
    cd_pessoa_fisica_p pessoa_fisica.cd_pessoa_fisica%type,
    nm_usuario_p     VARCHAR2,
    operation_type_p VARCHAR2,
    cd_work_status_p pessoa_fisica_aux.cd_work_status%type )
IS
  nr_seq_pessoa_fisica_aux_w pessoa_fisica_aux.nr_sequencia%type;
  nr_seq_pessoa_aux_w pessoa_fisica_aux.nr_sequencia%type;
BEGIN
  SELECT MAX(nr_sequencia)
  INTO nr_seq_pessoa_aux_w
  FROM pessoa_fisica_aux
  WHERE cd_pessoa_fisica = cd_pessoa_fisica_p;
  IF(operation_type_p NOT IN('DELETE')) THEN
    IF(nr_seq_pessoa_aux_w IS NULL) THEN
      SELECT pessoa_fisica_aux_seq.nextval
      INTO nr_seq_pessoa_fisica_aux_w
      FROM dual
      WHERE rownum < 2;
      INSERT
      INTO pessoa_fisica_aux
        (
          nr_sequencia,
          dt_atualizacao,
          nm_usuario,
          dt_atualizacao_nrec,
          nm_usuario_nrec,
          cd_pessoa_fisica,
          cd_work_status
        )
        VALUES
        (
          nr_seq_pessoa_fisica_aux_w,
          sysdate,
          nm_usuario_p,
          sysdate,
          nm_usuario_p,
          cd_pessoa_fisica_p,
          cd_work_status_p
        );
    ELSIF (nr_seq_pessoa_aux_w IS NOT NULL) THEN
      UPDATE pessoa_fisica_aux p
      SET p.cd_work_status = cd_work_status_p,
        p.dt_atualizacao_nrec    = sysdate,
        p.nm_usuario_nrec        = nm_usuario_p
      WHERE p.cd_pessoa_fisica   = cd_pessoa_fisica_p
      AND p.nr_sequencia         = nr_seq_pessoa_aux_w;
    END IF;
  ELSIF (operation_type_p = 'DELETE') THEN
    DELETE
    FROM pessoa_fisica_aux p
    WHERE p.cd_pessoa_fisica = cd_pessoa_fisica_p
    AND p.nr_sequencia       = nr_seq_pessoa_aux_w;
  END IF;
  COMMIT;
END iud_work_status;
/
