/*
Finalidade:
Retorna se deve gerar a coparticipação vendo a regra de exceção de dias do prestador
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_consist_cobr_prest_copart
		(	nr_seq_coparticipacao_p		number,
			nr_seq_conta_p			number,
			cd_procedimento_p		number,
			ie_origem_proced_p		number,
			nr_seq_prestador_exec_p		number,
			dt_conta_p			date,
			ie_cobrar_copart_p	out	varchar2) is 
			
qt_dias_retorno_w	number(10);
dt_inicial_w		date;
dt_final_w		date;
qt_registros_w		number(10);
nr_seq_segurado_w	number(10);

begin

select	max(qt_dias_retorno)
into	qt_dias_retorno_w
from	PLS_REGRA_COPARTIC_RETORNO
where	NR_SEQ_REGRA_COPARTIC	= nr_seq_coparticipacao_p
and	rownum			<= 1;

if	(qt_dias_retorno_w is null) then
	ie_cobrar_copart_p	:= 'S';
else
	dt_inicial_w	:= trunc(dt_conta_p,'dd') - qt_dias_retorno_w;
	dt_final_w	:= trunc(dt_conta_p,'dd');
	
	select	nr_seq_segurado
	into	nr_seq_segurado_w
	from	pls_conta
	where	nr_sequencia	= nr_seq_conta_p;
	
	select	count(1)
	into	qt_registros_w
	from	pls_conta_proc			c,
		PLS_CONTA_COPARTICIPACAO	b,
		pls_conta			a
	where	b.nr_seq_conta			= a.nr_sequencia
	and	c.nr_seq_conta			= a.nr_sequencia
	and	a.nr_sequencia			<> nr_seq_conta_p
	and	c.cd_procedimento		= cd_procedimento_p
	and	a.nr_seq_segurado		= nr_seq_segurado_w
	and	c.ie_origem_proced		= ie_origem_proced_p
	and	trunc(a.dt_atendimento_referencia,'dd') between dt_inicial_w and dt_final_w
	and	a.NR_SEQ_PRESTADOR_EXEC		= nr_seq_prestador_exec_p
	and	rownum				<= 1;
	
	if	(qt_registros_w > 0) then
		ie_cobrar_copart_p	:= 'N';
	else
		ie_cobrar_copart_p	:= 'S';
	end if;
end if;

end pls_consist_cobr_prest_copart;
/