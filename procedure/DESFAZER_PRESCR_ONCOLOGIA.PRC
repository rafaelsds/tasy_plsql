create or replace
procedure desfazer_prescr_oncologia(	nr_prescricao_p		number,
										nr_seq_paciente_p	number,
										nm_usuario_p		varchar2) is

cursor c01 is
select	'P' ie_tipo_item,			   --Procedimento
		a.nr_seq_proc_cpoe nr_seq_cpoe
from	prescr_procedimento a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_proc_cpoe is not null
union all
select	'M' ie_tipo_item,				--Medicamento
		a.nr_seq_mat_cpoe nr_seq_cpoe
from	prescr_material a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_mat_cpoe is not null
union all
select	'N' ie_tipo_item,				--Enteral/Suplemento
		a.nr_seq_dieta_cpoe nr_seq_cpoe
from	prescr_material a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_dieta_cpoe is not null
union all
select	'N' ie_tipo_item,				--Dieta Oral
		a.nr_seq_dieta_cpoe nr_seq_cpoe
from	prescr_dieta a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_dieta_cpoe is not null
union all
select	'R' ie_tipo_item,				--Recomendac�o
		a.nr_seq_rec_cpoe nr_seq_cpoe
from	prescr_recomendacao a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_rec_cpoe is not null
union all
select	'G' ie_tipo_item,				--Gasoterapia
		a.nr_seq_gas_cpoe nr_seq_cpoe
from	prescr_gasoterapia a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_gas_cpoe is not null
union all
select	'N' ie_tipo_item,				--Jejum
		a.nr_seq_dieta_cpoe nr_seq_cpoe
from	rep_jejum a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_dieta_cpoe is not null
union all
select	'D' ie_tipo_item,				--Dialise
		a.nr_seq_dialise_cpoe nr_seq_cpoe
from	hd_prescricao a
where	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_dialise_cpoe is not null
union all
select	'N' ie_tipo_item,				--NPT Adulta
		a.nr_seq_npt_cpoe nr_seq_cpoe
from	nut_pac a,
		prescr_material b
where	a.nr_prescricao = nr_prescricao_p
and   	a.nr_sequencia  = b.nr_seq_nut_pac(+)
and		a.nr_seq_npt_cpoe is not null
order by nr_seq_cpoe;										
	

c01_w c01%rowtype;
	
begin

if	(nr_prescricao_p is not null) and
	(nm_usuario_p	is not null) then
	
	update	paciente_atendimento
	set		nr_prescricao = null,
			dt_geracao_prescricao = null
	where	nr_seq_paciente = nr_seq_paciente_p
	and		nr_prescricao = nr_prescricao_p;
	commit;
	
	if (nvl(obter_se_prescr_CPOE(nr_prescricao_p),'N') = 'S') then
		begin 
			open c01;
			loop
			fetch c01 into
				c01_w;
			exit when c01%notfound;	
				cpoe_remover_item(c01_w.ie_tipo_item, c01_w.nr_seq_cpoe, nm_usuario_p);
			end loop;
			close c01;				
		exception when others then
			gravar_log_tasy(1616, 'erro desfazer_prescr_oncologia: ' || substr(to_char(sqlerrm),1,2000), nm_usuario_p);
		end; 
	end if;
	
	delete 	from prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	commit;
		
end if;

end desfazer_prescr_oncologia;
/