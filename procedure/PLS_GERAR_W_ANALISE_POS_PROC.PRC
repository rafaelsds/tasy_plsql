create or replace 
procedure pls_gerar_w_analise_pos_proc
			(	nr_seq_analise_p	number,
				nr_seq_conta_p		number,
				nr_seq_grupo_p		number,
				ie_minha_analise_p	varchar2,
				ie_pendentes_p		varchar2,
				nm_usuario_p		varchar2,
				nr_id_transacao_p	w_pls_analise_item.nr_id_transacao%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir na tabela tempor�ria dos itens da an�lise os procedimentos que n�o s�o
agrupadores (procedimentos sem participantes) - An�lise de P�s
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: N�o colocar nos selects desta procedure participantes, os mesmos ser�o
gerados em outra procedure
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_item_w			varchar2(255);
nm_prestador_w			varchar2(255);
ds_via_acesso_w			varchar2(255);
ds_tipo_guia_w			varchar2(255);
nm_prestador_solic_w		varchar2(255);
nm_prestador_exec_w		varchar2(255);
nm_prestador_pag_w		varchar2(255);
ds_fornecedor_w			varchar2(255);
ds_setor_atend_w		varchar2(255);
ds_item_importacao_w		varchar2(255);
ds_espec_cbo_w			varchar2(255);
ds_medico_executor_w		varchar2(255);
ds_status_item_w		varchar2(255);
ie_glosa_w			varchar2(255);
ds_procedimento_w		procedimento.ds_procedimento%TYPE;
ds_identacao_w			varchar2(30);
cd_guia_w			varchar2(30);
ie_autorizado_w			varchar2(20);
ie_tipo_linha_w			varchar2(10);
ie_tipo_despesa_w		varchar2(10);
ie_tipo_item_w			varchar2(10);
cd_medico_w			varchar2(10);
ie_via_acesso_w			varchar2(10);
cd_porte_anestesico_w		varchar2(10);
ie_pagamento_w			varchar2(10);
qt_selecao_w			varchar2(10);
ie_tipo_guia_w			varchar2(2);
ie_status_item_w		varchar2(2);
ie_status_faturamento_w		varchar2(2);
ie_faturamento_w		varchar2(2);
ie_status_analise_w		varchar2(1);
ie_valor_base_w			varchar2(1);
ie_pend_grupo_w			varchar2(1)	:= null;
ie_minha_analise_w		varchar2(1)	:= 'N';
ie_pendentes_w			varchar2(1)	:= 'N';
ie_nao_finalizar_w		varchar2(1)	:= null;
ie_item_nao_encontrado_w	varchar2(1);
ie_selecionado_w		varchar2(1)	:= 'N';
ie_sem_fluxo_w			varchar2(1)	:= null;
vl_taxa_servico_imp_w		number(15,4);
vl_taxa_co_imp_w		number(15,4);
vl_taxa_material_imp_w		number(15,4);
vl_taxa_servico_w		number(15,4);
vl_taxa_co_w			number(15,4);
vl_taxa_material_w		number(15,4);
vl_procedimento_imp_w		number(15,2);
vl_proc_unitario_w		pls_conta_proc.vl_unitario%type;
vl_procedimento_w		number(15,2);
vl_unitario_imp_w		number(15,2);
vl_glosa_w			number(15,2);
vl_liberado_w			number(15,2);
vl_glosa_material_w		number(15,2);
vl_glosa_hi_w			number(15,2);
vl_glosa_co_w			number(15,2);
vl_custo_operacional_w		number(15,2);
vl_liberado_material_w		number(15,2);
vl_liberado_co_w		number(15,2);
vl_liberado_hi_w		number(15,2);
vl_calculado_material_w		number(15,2);
vl_calculado_co_w		number(15,2);
vl_calculado_hi_w		number(15,2);
vl_taxa_intercambio_w		number(15,2);
vl_taxa_intercambio_imp_w	number(15,2);
vl_unitario_apres_w		number(15,2);
vl_base_w			number(15,2);
vl_lib_taxa_co_w		number(15,2);
vl_lib_taxa_material_w		number(15,2);
vl_lib_taxa_servico_w		number(15,2);
vl_glosa_taxa_co_w		number(15,2);
vl_glosa_taxa_material_w	number(15,2);
vl_glosa_taxa_servico_w		number(15,2);
vl_calculado_fat_w		number(15,2);
vl_beneficiario_w		number(15,2);
vl_unitario_w			number(15,2);
vl_proc_pag_calc_w		number(15,2);
vl_proc_pag_apres_w		number(15,2);
vl_proc_pag_lib_w		number(15,2);
vl_proc_pag_glosa_w		number(15,2);
cd_procedimento_w		number(15);
qt_liberado_w			number(12,4);
qt_procedimento_imp_w		number(12,4);
qt_liberar_w			number(12,4);
qt_original_w			number(12,4);
qt_item_w			number(12,4);
qt_proc_pag_w			number(12,4);
qt_proc_pag_lib_w		number(12,4);
nr_seq_conta_proc_w		number(10);
nr_seq_proc_partic_w		number(10);
nr_seq_prestador_pgto_w		number(10);
nr_seq_proc_pai_w		number(10);
ie_origem_proced_w		number(10);
nr_seq_guia_w			number(10);
nr_seq_prestador_exec_w		number(10);
nr_seq_protocolo_w		number(10);
nr_seq_prest_fornec_w		number(10);
nr_seq_setor_atend_w		number(10);
nr_auxiliares_w			number(10);
nr_sequencia_w			number(10)	:= 0;
nr_seq_conta_pos_w		number(10);
nr_id_analise_w			number(10);
tx_item_w			number(9,3);
tx_reducao_acrescimo_w		number(7,4);
tx_intercambio_w		number(7,4);
tx_intercambio_imp_w		number(7,4);
nr_nivel_w			number(3)	:= 1;
dt_procedimento_w		date;
dt_emissao_conta_w		date;
vl_medico_calc_w		Number(15,2);
vl_materiais_calc_w		Number(15,2);
vl_custo_operacional_calc_w	Number(15,2);
vl_co_ptu_imp_w			Number(15,2);
vl_material_ptu_imp_w		Number(15,2);
vl_procedimento_ptu_imp_w	Number(15,2);
vl_liberado_material_fat_w	Number(15,2);
vl_liberado_co_fat_w		Number(15,2);
vl_liberado_hi_fat_w		Number(15,2);
vl_glosa_material_fat_w		Number(15,2);
vl_glosa_hi_fat_w		Number(15,2);
vl_glosa_co_fat_w		Number(15,2);
ie_pacote_ptu_w			Varchar2(1);
qt_liberado_pag_w		number(12,4);
vl_glosa_taxa_co_fat_w 		pls_conta_pos_estabelecido.vl_glosa_taxa_co%type;
vl_glosa_taxa_material_fat_w	pls_conta_pos_estabelecido.vl_glosa_taxa_material%type;
vl_glosa_taxa_servico_fat_w 	pls_conta_pos_estabelecido.vl_glosa_taxa_servico%type;
vl_lib_taxa_co_fat_w   		pls_conta_pos_estabelecido.vl_lib_taxa_co%type;
vl_lib_taxa_material_fat_w	pls_conta_pos_estabelecido.vl_lib_taxa_material%type;
vl_lib_taxa_servico_fat_w     	pls_conta_pos_estabelecido.vl_lib_taxa_servico%type;
vl_taxa_co_fat_w 		pls_conta_pos_estabelecido.vl_taxa_co%type;
vl_taxa_material_fat_w		pls_conta_pos_estabelecido.vl_taxa_material%type;
vl_taxa_servico_fat_w 		pls_conta_pos_estabelecido.vl_taxa_servico%type;
nr_seq_lote_fat_w		pls_conta_pos_estabelecido.nr_seq_lote_fat%type;
nr_seq_evento_fat_w		pls_conta_pos_estabelecido.nr_seq_evento_fat%type;
nr_seq_disc_proc_w		pls_conta_pos_estabelecido.nr_seq_disc_proc%type;
qt_negada_w			pls_discussao_proc.qt_negada%type;
vl_negado_w			pls_discussao_proc.vl_negado%type;
tx_administracao_w		pls_conta_pos_estabelecido.tx_administracao%type;
ds_item_ptu_w			pls_conta_pos_estabelecido.ds_item_ptu%type;
cd_procedimento_orig_w		procedimento.cd_procedimento%type;
cd_procedimento_conv_w		procedimento.cd_procedimento%type;
ds_procedimento_orig_w		procedimento.ds_procedimento%type;
ds_procedimento_conv_w		procedimento.ds_procedimento%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_item_convertido_w		pls_parametros.ie_item_convertido%type;
nr_seq_proc_rec_w		pls_rec_glosa_proc.nr_sequencia%type;
vl_recursado_w			pls_rec_glosa_proc.vl_recursado%type;
vl_acatado_w			pls_rec_glosa_proc.vl_acatado%type;
nr_seq_prest_pgto_resumo_w	pls_conta_medica_resumo.nr_seq_prestador_pgto%type;
ie_aviso_a520_proc_w		pls_conta_proc_regra.ie_a520%type;
tx_item_proc_w			pls_conta_pos_estabelecido.tx_item%type;

Cursor C01 is
	select	a.nr_sequencia,
		'IC', /* Item da conta */
		'P', /* Procedimento */
		a.cd_procedimento,
		c.cd_item_convertido,
		a.dt_procedimento,
		b.ds_procedimento,
		c.ds_item_convertido,
		a.ie_tipo_despesa,
		nvl(c.qt_original, a.qt_procedimento_imp),
		a.vl_procedimento_imp,
		a.vl_unitario,
		nvl(c.vl_calculado, a.vl_procedimento),
		a.ie_origem_proced,
		a.ie_via_acesso,
		a.nr_auxiliares,
		a.cd_porte_anestesico,
		a.nr_seq_setor_atend,
		substr(a.cd_procedimento_imp || ' - ' || a.ds_procedimento_imp,1,255),
		a.vl_glosa_material,
		a.vl_glosa_hi,
		a.vl_glosa_co,
		a.vl_custo_operacional,
		a.vl_liberado_material,
		a.vl_liberado_co,
		a.vl_liberado_hi,
		a.vl_materiais,
		a.vl_custo_operacional,
		a.vl_total_partic,
		a.tx_intercambio,
		a.tx_intercambio_imp,
		c.qt_item,
		a.tx_item,
		c.vl_calculado - c.vl_beneficiario ,
		nvl(c.vl_beneficiario, a.vl_liberado),
		a.vl_unitario_imp,
		a.vl_taxa_servico_imp,
		a.vl_taxa_co_imp,
		a.vl_taxa_material_imp,
		a.vl_taxa_servico,
		a.vl_taxa_co,
		a.vl_taxa_material,
		a.ie_status,
		a.ie_valor_base,
		decode(b.cd_procedimento,null,'S',null),
		a.nr_seq_prestador_pgto,
		a.vl_lib_taxa_co,
		a.vl_lib_taxa_material,
		a.vl_lib_taxa_servico,
		a.vl_glosa_taxa_co,
		a.vl_glosa_taxa_material,
		a.vl_glosa_taxa_servico,
		c.nr_sequencia,
		c.ie_status_faturamento,
		c.qt_original,
		c.qt_item,
		c.vl_calculado,
		c.vl_beneficiario,
		dividir_sem_round(c.vl_calculado,c.qt_original),
		a.qt_procedimento_imp,
		a.qt_procedimento,
		a.vl_procedimento,
		a.vl_procedimento_imp,
		a.vl_liberado,
		a.vl_glosa,
		a.ie_glosa,
		a.nr_id_analise,
		a.ie_status_pagamento,
		c.vl_medico_calc,
		c.vl_materiais_calc,
		c.vl_custo_operacional_calc,
		a.vl_co_ptu_imp,
		a.vl_material_ptu_imp,
		a.vl_procedimento_ptu_imp,
		c.vl_liberado_material_fat,
		c.vl_liberado_co_fat,
		c.vl_liberado_hi_fat,
		c.vl_glosa_material_fat,
		c.vl_glosa_hi_fat,
		c.vl_glosa_co_fat,
		a.ie_pacote_ptu,
		a.qt_procedimento,
		c.vl_glosa_taxa_co,
		c.vl_glosa_taxa_material,
		c.vl_glosa_taxa_servico,
		c.vl_lib_taxa_co,
		c.vl_lib_taxa_material,
		c.vl_lib_taxa_servico,
		c.vl_taxa_co,
		c.vl_taxa_material,
		c.vl_taxa_servico,
		c.nr_seq_lote_fat,
		c.nr_seq_evento_fat,
		c.nr_seq_disc_proc,
		c.tx_administracao,
		c.ds_item_ptu,
		c.nr_seq_proc_rec,
		( 	select 	max(nr_seq_prestador_pgto) 
			from	pls_conta_medica_resumo r
			where 	r.nr_seq_conta = a.nr_seq_conta
			and 	r.nr_seq_conta_proc = a.nr_sequencia) nr_seq_prest_pgto_resumo,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		nvl(c.tx_item, 0)  tx_item_proc
	from	procedimento	b,
		pls_conta_proc	a,
		pls_conta_pos_estabelecido c
	where	c.nr_seq_conta_proc	= a.nr_sequencia
	and	c.cd_procedimento	= b.cd_procedimento(+)
	and	c.ie_origem_proced	= b.ie_origem_proced(+)
	and	c.nr_seq_conta		= nr_seq_conta_p
	and	c.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_agrup_analise is null
	--and	a.nr_seq_participante_hi is null /* N�o gerar procedimentos de participantes */
	and	not exists	(select	1
				from	pls_proc_participante x
				where	x.nr_seq_conta_proc	= a.nr_sequencia)
	and	(c.ie_situacao = 'A' or c.ie_situacao is null)
	and	c.ie_status_faturamento	!= 'A'
	and	c.ie_cobrar_mensalidade	!= 'A'
	union all
	select	a.nr_sequencia,
		'IC', /* Item da conta */
		'P', /* Procedimento */
		a.cd_procedimento,
		c.cd_item_convertido,
		a.dt_procedimento,
		b.ds_procedimento,
		c.ds_item_convertido,
		a.ie_tipo_despesa,
		nvl(c.qt_original, a.qt_procedimento_imp),
		a.vl_procedimento_imp,
		a.vl_unitario,
		nvl(c.vl_calculado, a.vl_procedimento),
		a.ie_origem_proced,
		a.ie_via_acesso,
		a.nr_auxiliares,
		a.cd_porte_anestesico,
		a.nr_seq_setor_atend,
		substr(a.cd_procedimento_imp || ' - ' || a.ds_procedimento_imp,1,255),
		a.vl_glosa_material,
		a.vl_glosa_hi,
		a.vl_glosa_co,
		a.vl_custo_operacional,
		a.vl_liberado_material,
		a.vl_liberado_co,
		a.vl_liberado_hi,
		a.vl_materiais,
		a.vl_custo_operacional,
		a.vl_total_partic,
		a.tx_intercambio,
		a.tx_intercambio_imp,
		c.qt_item,
		a.tx_item,
		c.vl_calculado - c.vl_beneficiario,
		nvl(c.vl_beneficiario, a.vl_liberado) ,
		a.vl_unitario_imp,
		a.vl_taxa_servico_imp,
		a.vl_taxa_co_imp,
		a.vl_taxa_material_imp,
		a.vl_taxa_servico,
		a.vl_taxa_co,
		a.vl_taxa_material,
		a.ie_status,
		a.ie_valor_base,
		decode(b.cd_procedimento,null,'S',null),
		a.nr_seq_prestador_pgto,
		a.vl_lib_taxa_co,
		a.vl_lib_taxa_material,
		a.vl_lib_taxa_servico,
		a.vl_glosa_taxa_co,
		a.vl_glosa_taxa_material,
		a.vl_glosa_taxa_servico,
		c.nr_sequencia,
		c.ie_status_faturamento,
		c.qt_original,
		c.qt_item,
		c.vl_calculado,
		c.vl_beneficiario,
		dividir_sem_round(c.vl_calculado,c.qt_original),
		a.qt_procedimento_imp,
		a.qt_procedimento,
		a.vl_procedimento,
		a.vl_procedimento_imp,
		a.vl_liberado,
		a.vl_glosa,
		a.ie_glosa,
		a.nr_id_analise,
		a.ie_status_pagamento,
		c.vl_medico_calc,
		c.vl_materiais_calc,
		c.vl_custo_operacional_calc,
		a.vl_co_ptu_imp,
		a.vl_material_ptu_imp,
		a.vl_procedimento_ptu_imp,
		c.vl_liberado_material_fat,
		c.vl_liberado_co_fat,
		c.vl_liberado_hi_fat,
		c.vl_glosa_material_fat,
		c.vl_glosa_hi_fat,
		c.vl_glosa_co_fat,
		a.ie_pacote_ptu,
		a.qt_procedimento,
		c.vl_glosa_taxa_co,
		c.vl_glosa_taxa_material,
		c.vl_glosa_taxa_servico,
		c.vl_lib_taxa_co,
		c.vl_lib_taxa_material,
		c.vl_lib_taxa_servico,
		c.vl_taxa_co,
		c.vl_taxa_material,
		c.vl_taxa_servico,
		c.nr_seq_lote_fat,
		c.nr_seq_evento_fat,
		c.nr_seq_disc_proc,
		c.tx_administracao,
		c.ds_item_ptu,
		c.nr_seq_proc_rec,
		( 	select 	max(nr_seq_prestador_pgto)
			from	pls_conta_medica_resumo r
			where 	r.nr_seq_conta = a.nr_seq_conta
			and 	r.nr_seq_conta_proc = a.nr_sequencia) nr_seq_prest_pgto_resumo,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		nvl(c.tx_item, 0)  tx_item_proc
	from	procedimento	b,
		pls_conta_proc	a,
		pls_conta_pos_estabelecido c
	where	c.nr_seq_conta_proc	= a.nr_sequencia
	and	a.cd_procedimento	= b.cd_procedimento(+)
	and	a.ie_origem_proced	= b.ie_origem_proced(+)
	and	c.nr_seq_conta		= nr_seq_conta_p
	and	c.nr_seq_analise	= nr_seq_analise_p
	and	c.cd_procedimento	is null
	and	a.nr_seq_agrup_analise is null
	--and	a.nr_seq_participante_hi is null /* N�o gerar procedimentos de participantes */
	and	not exists	(select	1
				from	pls_proc_participante x
				where	x.nr_seq_conta_proc	= a.nr_sequencia)
	and	(c.ie_situacao = 'A' or c.ie_situacao is null)
	and	c.ie_status_faturamento	!= 'A'
	and	c.ie_cobrar_mensalidade	!= 'A'
	union all
	select	a.nr_sequencia,
		'IC', /* Item da conta */
		'P', /* Procedimento */
		a.cd_procedimento,
		c.cd_item_convertido,
		a.dt_procedimento,
		b.ds_procedimento,
		c.ds_item_convertido,
		a.ie_tipo_despesa,
		nvl(c.qt_original, a.qt_procedimento_imp),
		a.vl_procedimento_imp,
		a.vl_unitario,
		nvl(c.vl_calculado, a.vl_procedimento),
		a.ie_origem_proced,
		a.ie_via_acesso,
		a.nr_auxiliares,
		a.cd_porte_anestesico,
		a.nr_seq_setor_atend,
		substr(a.cd_procedimento_imp || ' - ' || a.ds_procedimento_imp,1,255),
		a.vl_glosa_material,
		a.vl_glosa_hi,
		a.vl_glosa_co,
		a.vl_custo_operacional,
		a.vl_liberado_material,
		a.vl_liberado_co,
		a.vl_liberado_hi,
		a.vl_materiais,
		a.vl_custo_operacional,
		a.vl_total_partic,
		a.tx_intercambio,
		a.tx_intercambio_imp,
		c.qt_item,
		a.tx_item,
		c.vl_calculado - c.vl_beneficiario,
		nvl(c.vl_beneficiario, a.vl_liberado) ,
		a.vl_unitario_imp,
		a.vl_taxa_servico_imp,
		a.vl_taxa_co_imp,
		a.vl_taxa_material_imp,
		a.vl_taxa_servico,
		a.vl_taxa_co,
		a.vl_taxa_material,
		a.ie_status,
		a.ie_valor_base,
		decode(b.cd_procedimento,null,'S',null),
		a.nr_seq_prestador_pgto,
		a.vl_lib_taxa_co,
		a.vl_lib_taxa_material,
		a.vl_lib_taxa_servico,
		a.vl_glosa_taxa_co,
		a.vl_glosa_taxa_material,
		a.vl_glosa_taxa_servico,
		c.nr_sequencia,
		c.ie_status_faturamento,
		c.qt_original,
		c.qt_item,
		c.vl_calculado,
		c.vl_beneficiario,
		dividir_sem_round(c.vl_calculado,c.qt_original),
		a.qt_procedimento_imp,
		a.qt_procedimento,
		a.vl_procedimento,
		a.vl_procedimento_imp,
		a.vl_liberado,
		a.vl_glosa,
		a.ie_glosa,
		a.nr_id_analise,
		a.ie_status_pagamento,
		c.vl_medico_calc,
		c.vl_materiais_calc,
		c.vl_custo_operacional_calc,
		a.vl_co_ptu_imp,
		a.vl_material_ptu_imp,
		a.vl_procedimento_ptu_imp,
		c.vl_liberado_material_fat,
		c.vl_liberado_co_fat,
		c.vl_liberado_hi_fat,
		c.vl_glosa_material_fat,
		c.vl_glosa_hi_fat,
		c.vl_glosa_co_fat,
		a.ie_pacote_ptu,
		a.qt_procedimento,
		c.vl_glosa_taxa_co,
		c.vl_glosa_taxa_material,
		c.vl_glosa_taxa_servico,
		c.vl_lib_taxa_co,
		c.vl_lib_taxa_material,
		c.vl_lib_taxa_servico,
		c.vl_taxa_co,
		c.vl_taxa_material,
		c.vl_taxa_servico,
		c.nr_seq_lote_fat,
		c.nr_seq_evento_fat,
		c.nr_seq_disc_proc,
		c.tx_administracao,
		c.ds_item_ptu,
		c.nr_seq_proc_rec,
		( 	select 	max(nr_seq_prestador_pgto)
			from	pls_conta_medica_resumo r
			where 	r.nr_seq_conta = a.nr_seq_conta
			and 	r.nr_seq_conta_proc = a.nr_sequencia) nr_seq_prest_pgto_resumo,
		(	select	nvl(max(x.ie_a520), 'N')
			from	pls_conta_proc_regra	x
			where	x.nr_sequencia		= a.nr_sequencia) ie_a520,
		nvl(c.tx_item, 0)  tx_item_proc
	from	procedimento	b,
			pls_conta_proc	a,
			pls_conta_pos_estabelecido c
	where	c.nr_seq_conta_proc	= a.nr_sequencia
	and	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced	= b.ie_origem_proced
	and	c.nr_seq_conta		= nr_seq_conta_p
	and	c.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_agrup_analise is not null
	and	a.nr_seq_proc_pct is null
	and	not exists (select 1	
					from 	pls_conta_pos_estabelecido pos, 
							pls_conta_proc proc 
					where 	proc.nr_sequencia = a.nr_seq_agrup_analise 
					and 	proc.nr_sequencia = pos.nr_seq_conta_proc
					and	pos.ie_status_faturamento	!= 'A'
					and	pos.ie_cobrar_mensalidade	!= 'A')
	and	exists (select 1	
					from 	pls_conta_proc proc 
					where 	proc.nr_sequencia = a.nr_seq_agrup_analise 
					and 	proc.ie_status	!= 'D')
	--and	a.nr_seq_participante_hi is null /* N�o gerar procedimentos de participantes */
	and	not exists	(select	1
				from	pls_proc_participante x
				where	x.nr_seq_conta_proc	= a.nr_sequencia)
	and	(c.ie_situacao = 'A' or c.ie_situacao is null)
	and	c.ie_status_faturamento	!= 'A'
	and	c.ie_cobrar_mensalidade	!= 'A';

begin
ie_minha_analise_w	:= nvl(ie_minha_analise_p,'N');
ie_pendentes_w		:= nvl(ie_pendentes_p,'N');
ds_identacao_w		:= '        ';

/* Obter dados da conta/guia */
nr_seq_guia_w		:= pls_consulta_analise_pos_pck.get_nr_seq_guia;
ie_autorizado_w		:= pls_consulta_analise_pos_pck.get_ie_autorizado;
cd_guia_w		:= pls_consulta_analise_pos_pck.get_cd_guia;
ds_tipo_guia_w		:= pls_consulta_analise_pos_pck.get_ds_tipo_guia;
nm_prestador_solic_w	:= pls_consulta_analise_pos_pck.get_nm_prestador_solic;
nm_prestador_exec_w	:= pls_consulta_analise_pos_pck.get_nm_prestador_exec;
nr_seq_prestador_exec_w	:= pls_consulta_analise_pos_pck.get_nr_seq_prestador_exec;
dt_emissao_conta_w	:= pls_consulta_analise_pos_pck.get_dt_emissao_conta;
nr_seq_protocolo_w	:= pls_consulta_analise_pos_pck.get_nr_seq_protocolo;
ds_espec_cbo_w		:= pls_consulta_analise_pos_pck.get_ds_espec_cbo;
ds_medico_executor_w	:= pls_consulta_analise_pos_pck.get_ds_medico_executor;
nr_nivel_w		:= pls_consulta_analise_pos_pck.get_nr_nivel;
ie_tipo_guia_w		:= pls_consulta_analise_pos_pck.get_ie_tipo_guia;

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

select	nvl(max(ie_item_convertido),'S')
into	ie_item_convertido_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_w;

if	(nr_nivel_w > 0) then
	open C01;
	loop
	fetch C01 into
		nr_seq_conta_proc_w,
		ie_tipo_linha_w,
		ie_tipo_item_w,
		cd_procedimento_orig_w,
		cd_procedimento_conv_w,
		dt_procedimento_w,
		ds_procedimento_orig_w,
		ds_procedimento_conv_w,
		ie_tipo_despesa_w,
		qt_procedimento_imp_w,
		vl_procedimento_imp_w,
		vl_proc_unitario_w,
		vl_procedimento_w,
		ie_origem_proced_w,
		ie_via_acesso_w,
		nr_auxiliares_w,
		cd_porte_anestesico_w,
		nr_seq_setor_atend_w,
		ds_item_importacao_w,
		vl_glosa_material_w,
		vl_glosa_hi_w,
		vl_glosa_co_w,
		vl_custo_operacional_w,
		vl_liberado_material_w,
		vl_liberado_co_w,
		vl_liberado_hi_w,
		vl_calculado_material_w,
		vl_calculado_co_w,
		vl_calculado_hi_w,
		tx_intercambio_w,
		tx_intercambio_imp_w,
		qt_liberado_w,
		tx_item_w,
		vl_glosa_w,
		vl_liberado_w,
		vl_unitario_apres_w,
		vl_taxa_servico_imp_w,
		vl_taxa_co_imp_w,
		vl_taxa_material_imp_w,
		vl_taxa_servico_w,
		vl_taxa_co_w,
		vl_taxa_material_w,
		ie_status_item_w,
		ie_valor_base_w,
		ie_item_nao_encontrado_w,
		nr_seq_prestador_pgto_w,
		vl_lib_taxa_co_w,
		vl_lib_taxa_material_w,
		vl_lib_taxa_servico_w,
		vl_glosa_taxa_co_w,
		vl_glosa_taxa_material_w,
		vl_glosa_taxa_servico_w,
		nr_seq_conta_pos_w,
		ie_status_faturamento_w,
		qt_original_w,
		qt_item_w,
		vl_calculado_fat_w,
		vl_beneficiario_w,
		vl_unitario_w,
		qt_proc_pag_w,
		qt_proc_pag_lib_w,
		vl_proc_pag_calc_w,
		vl_proc_pag_apres_w,
		vl_proc_pag_lib_w,
		vl_proc_pag_glosa_w,
		ie_glosa_w,
		nr_id_analise_w,
		ie_pagamento_w,
		vl_medico_calc_w,
		vl_materiais_calc_w,
		vl_custo_operacional_calc_w,
		vl_co_ptu_imp_w,
		vl_material_ptu_imp_w,
		vl_procedimento_ptu_imp_w,
		vl_liberado_material_fat_w,
		vl_liberado_co_fat_w,
		vl_liberado_hi_fat_w,
		vl_glosa_material_fat_w,
		vl_glosa_hi_fat_w,
		vl_glosa_co_fat_w,
		ie_pacote_ptu_w,
		qt_liberado_pag_w,
		vl_glosa_taxa_co_fat_w,
		vl_glosa_taxa_material_fat_w,
		vl_glosa_taxa_servico_fat_w,
		vl_lib_taxa_co_fat_w,
		vl_lib_taxa_material_fat_w,
		vl_lib_taxa_servico_fat_w,
		vl_taxa_co_fat_w,
		vl_taxa_material_fat_w,
		vl_taxa_servico_fat_w,
		nr_seq_lote_fat_w,
		nr_seq_evento_fat_w,
		nr_seq_disc_proc_w,
		tx_administracao_w,
		ds_item_ptu_w,
		nr_seq_proc_rec_w,
		nr_seq_prest_pgto_resumo_w,
		ie_aviso_a520_proc_w,
		tx_item_proc_w;
	exit when C01%notfound;
		begin
		ds_via_acesso_w			:= null;
		ie_pend_grupo_w			:= null;
		ie_pend_grupo_w			:= pls_obter_pend_grupo_ana_pos(nr_seq_analise_p,nr_seq_conta_pos_w,nr_seq_grupo_p,'N');
		
		
		if (nr_seq_prestador_pgto_w is null) then
			nr_seq_prestador_pgto_w := nr_seq_prest_pgto_resumo_w;
		end if;
		
		if	(ie_item_convertido_w = 'N') then
			cd_procedimento_w := cd_procedimento_orig_w;
			ds_procedimento_w := ds_procedimento_orig_w;
		else
			cd_procedimento_w := nvl(cd_procedimento_conv_w, cd_procedimento_orig_w);
			ds_procedimento_w := nvl(ds_procedimento_conv_w, ds_procedimento_orig_w);
		end if;
	
		if	(ds_procedimento_w is null) then
			ds_procedimento_w := 'N�o encontrado';
		end if;
	
		if	(((ie_minha_analise_w = 'S') and
			(ie_pend_grupo_w is not null)) or
			(ie_minha_analise_w = 'N')) and
			((ie_pendentes_w = ie_pend_grupo_w) or
			(ie_pendentes_w = 'N')) then
			ds_item_w			:= ds_identacao_w || nvl(ds_item_ptu_w, ds_procedimento_w);
			ds_status_item_w		:= obter_valor_dominio(1870,ie_status_item_w);
			vl_taxa_intercambio_imp_w	:= nvl(vl_taxa_servico_imp_w,0) + nvl(vl_taxa_co_imp_w,0) + nvl(vl_taxa_material_imp_w,0);
			vl_taxa_intercambio_w		:= nvl(vl_taxa_servico_w,0) + nvl(vl_taxa_co_w,0) + nvl(vl_taxa_material_w,0);
			nm_prestador_pag_w		:= substr(pls_obter_dados_prestador(nr_seq_prestador_pgto_w,'N'),1,255);

			if	(ie_via_acesso_w is not null) then
				select	ds_valor_dominio
				into	ds_via_acesso_w
				from	valor_dominio_v
				where	cd_dominio	= 1268
				and	vl_dominio	= ie_via_acesso_w;
			end if;

			ie_faturamento_w	:= pls_analise_obter_status_fat(ie_status_faturamento_w,qt_original_w,qt_item_w,vl_calculado_fat_w,vl_beneficiario_w);

			if	(ie_pagamento_w is null) and
				(nr_seq_disc_proc_w is null) and
				(nr_seq_proc_rec_w is null) then
				ie_pagamento_w	:= pls_analise_obter_status_pag(ie_status_item_w,qt_proc_pag_w,qt_proc_pag_lib_w,
							vl_proc_pag_apres_w,vl_proc_pag_calc_w,vl_proc_pag_lib_w,vl_proc_pag_glosa_w, ie_glosa_w);
			elsif	(nr_seq_disc_proc_w	is not null) then
				select	max(qt_negada),
					max(vl_negado),
					max(qt_aceita),
					max(vl_aceito)
				into	qt_negada_w,
					vl_negado_w,
					qt_proc_pag_lib_w,
					vl_proc_pag_lib_w
				from	pls_discussao_proc
				where	nr_sequencia	= nr_seq_disc_proc_w;

				if	(qt_negada_w	> 0) or
					(vl_negado_w 	> 0) then
					ie_pagamento_w	:= 'P';
				else
					ie_pagamento_w	:= 'L';
				end if;
			elsif	(nr_seq_proc_rec_w is not null) then
				
				select	nvl(max(vl_recursado),0),
					nvl(max(vl_acatado),0)
				into	vl_recursado_w,
					vl_acatado_w
				from	pls_rec_glosa_proc
				where	nr_sequencia = nr_seq_proc_rec_w;
				
				if	(vl_acatado_w = 0) then
					ie_pagamento_w := 'G';
				elsif	(vl_recursado_w <> vl_acatado_w) then
					ie_pagamento_w := 'P';
				else
					ie_pagamento_w := 'L';
				end if;			
			end if;
			/* Obter o status geral da an�lise do item */
			ie_status_analise_w	:= pls_obter_status_analise_pos(nr_seq_analise_p,nr_seq_conta_pos_w, ie_item_nao_encontrado_w);

			/* Obter se possui fluxo de an�lise */
			ie_sem_fluxo_w		:= pls_obter_se_item_sem_fluxo(nr_seq_analise_p,null, nr_seq_conta_proc_w, null,null);

			nr_sequencia_w	:= pls_consulta_analise_pos_pck.get_nr_seq_item;

			qt_liberar_w	:= null;

			select	count(1),
				max(qt_liberar)
			into	qt_selecao_w,
				qt_liberar_w
			from	w_pls_analise_selecao_item	a
			where	a.nr_seq_analise	= nr_seq_analise_p
			and	a.nr_seq_w_item		= nr_sequencia_w;

			if	(qt_selecao_w > 0) then
				ie_selecionado_w	:= 'S';
			else
				ie_selecionado_w	:= 'N';
			end if;
			
			--Quando utilizada a op��o ajustar valor sobre o item e o mesmo ajustado para um valor maior
			--nesse caso n�o informando uma glosa.
			if	(vl_glosa_w < 0) then
				vl_glosa_w := 0;
			end if;
			
			if (tx_item_proc_w > 0 ) then
				tx_item_w := tx_item_proc_w;
			end if;
			
			insert into w_pls_analise_item
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_analise,
				nr_seq_conta,
				nr_seq_conta_proc,
				cd_item,
				dt_item,
				ds_item,
				ie_tipo_despesa,
				ie_tipo_linha,
				ie_tipo_item,
				qt_apresentada,
				vl_apresentado,
				vl_calculado_unitario,
				vl_calculado,
				cd_procedimento,
				ie_origem_proced,
				ie_via_acesso,
				ds_via_acesso,
				nm_prestador_solic,
				nr_auxiliares,
				cd_porte_anestesico,
				ds_tipo_guia,
				nm_prestador_exec,
				nm_prestador_pag,
				ds_espec_cbo,
				nr_seq_protocolo,
				ie_autoriz_previa,
				cd_guia,
				ds_setor_atend,
				ds_item_importacao,
				ds_medico_executor,
				vl_glosa_material,
				vl_glosa_hi,
				vl_glosa_co,
				vl_custo_operacional,
				vl_liberado_material,
				vl_liberado_co,
				vl_liberado_hi,
				vl_calculado_material,
				vl_calculado_co,
				vl_calculado_hi,
				tx_intercambio,
				tx_intercambio_imp,
				vl_taxa_intercambio,
				vl_taxa_intercambio_imp,
				qt_liberado,
				tx_item,
				vl_glosa,
				vl_liberado,
				vl_unitario_apres,
				nr_seq_prestador_exec,
				nr_seq_guia,
				ie_status_item,
				ds_status_item,
				ie_status_analise,
				ie_pend_grupo,
				ie_pagamento,
				ie_item_nao_encontrado,
				ie_selecionado,
				qt_liberar,
				ie_sem_fluxo,
				ie_tipo_guia,
				vl_lib_taxa_co,
				vl_lib_taxa_material,
				vl_lib_taxa_servico,
				vl_glosa_taxa_co,
				vl_glosa_taxa_material,
				vl_glosa_taxa_servico,
				vl_taxa_servico_imp,
				vl_taxa_co_imp,
				vl_taxa_material_imp,
				vl_taxa_servico,
				vl_taxa_co,
				vl_taxa_material,
				nr_seq_conta_pos_estab,
				ie_faturamento,
				ie_status_faturamento,
				qt_faturada_orig,
				vl_faturado_orig,
				vl_unitario_faturado,
				nr_identificador,
				vl_co_ptu_imp,
				vl_material_ptu_imp,
				vl_procedimento_ptu_imp,
				vl_medico_calc_fat,
				vl_materiais_calc_fat,
				vl_custo_operacional_calc_fat,
				vl_liberado_material_fat,
				vl_liberado_co_fat,
				vl_liberado_hi_fat,
				vl_glosa_material_fat,
				vl_glosa_hi_fat,
				vl_glosa_co_fat,
				ie_pacote_ptu,
				vl_liberado_pag,
				vl_glosa_pag,
				qt_cobranca,
				vl_lib_taxa_co_fat,
				vl_lib_taxa_hm_fat,
				vl_lib_taxa_material_fat,
				vl_taxa_co_fat,
				vl_taxa_hm_fat,
				vl_taxa_material_fat,
				vl_glosa_taxa_co_fat,
				vl_glosa_taxa_hm_fat,
				vl_glosa_taxa_material_fat,
				nr_seq_lote_fat,
				nr_seq_evento_fat,
				tx_administracao_pos,
				nr_id_transacao,
				ie_a520)
			values	(nr_sequencia_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_analise_p,
				nr_seq_conta_p,
				nr_seq_conta_proc_w,
				cd_procedimento_w,
				dt_procedimento_w,
				ds_item_w,
				ie_tipo_despesa_w,
				ie_tipo_linha_w,
				ie_tipo_item_w,
				qt_procedimento_imp_w,
				vl_procedimento_imp_w,
				vl_proc_unitario_w,
				vl_procedimento_w,
				cd_procedimento_w,
				ie_origem_proced_w,
				ie_via_acesso_w,
				ds_via_acesso_w,
				nm_prestador_solic_w,
				nr_auxiliares_w,
				cd_porte_anestesico_w,
				ds_tipo_guia_w,
				nm_prestador_exec_w,
				nm_prestador_pag_w,
				ds_espec_cbo_w,
				nr_seq_protocolo_w,
				ie_autorizado_w,
				cd_guia_w,
				ds_setor_atend_w,
				ds_item_importacao_w,
				ds_medico_executor_w,
				vl_glosa_material_w,
				vl_glosa_hi_w,
				vl_glosa_co_w,
				vl_custo_operacional_w,
				vl_liberado_material_w,
				vl_liberado_co_w,
				vl_liberado_hi_w,
				vl_calculado_material_w,
				vl_calculado_co_w,
				vl_calculado_hi_w,
				tx_intercambio_w,
				tx_intercambio_imp_w,
				vl_taxa_intercambio_w,
				vl_taxa_intercambio_imp_w,
				qt_liberado_pag_w,
				tx_item_w,
				vl_glosa_w,
				vl_beneficiario_w,
				vl_unitario_apres_w,
				nr_seq_prestador_exec_w,
				nr_seq_guia_w,
				ie_status_item_w,
				ds_status_item_w,
				ie_status_analise_w,
				ie_pend_grupo_w,
				ie_pagamento_w,
				ie_item_nao_encontrado_w,
				ie_selecionado_w,
				qt_liberar_w,
				ie_sem_fluxo_w,
				ie_tipo_guia_w,
				vl_lib_taxa_co_w,
				vl_lib_taxa_material_w,
				vl_lib_taxa_servico_w,
				vl_glosa_taxa_co_w,
				vl_glosa_taxa_material_w,
				vl_glosa_taxa_servico_w,
				vl_taxa_servico_imp_w,
				vl_taxa_co_imp_w,
				vl_taxa_material_imp_w,
				vl_taxa_servico_w,
				vl_taxa_co_w,
				vl_taxa_material_w,
				nr_seq_conta_pos_w,
				ie_faturamento_w,
				ie_status_faturamento_w,
				qt_original_w,
				vl_calculado_fat_w,
				vl_unitario_w,
				nr_id_analise_w,
				vl_co_ptu_imp_w,
				vl_material_ptu_imp_w,
				vl_procedimento_ptu_imp_w,
				vl_medico_calc_w,
				vl_materiais_calc_w,
				vl_custo_operacional_calc_w,
				vl_liberado_material_fat_w,
				vl_liberado_co_fat_w,
				vl_liberado_hi_fat_w,
				vl_glosa_material_fat_w,
				vl_glosa_hi_fat_w,
				vl_glosa_co_fat_w,
				ie_pacote_ptu_w,
				vl_proc_pag_lib_w,
				vl_proc_pag_glosa_w,
				qt_item_w,
				vl_lib_taxa_co_fat_w,
				vl_lib_taxa_servico_fat_w,
				vl_lib_taxa_material_fat_w,
				vl_taxa_co_fat_w,
				vl_taxa_servico_fat_w,
				vl_taxa_material_fat_w,
				vl_glosa_taxa_co_fat_w,
				vl_glosa_taxa_servico_fat_w,
				vl_glosa_taxa_material_fat_w,
				nr_seq_lote_fat_w,
				nr_seq_evento_fat_w,
				tx_administracao_w,
				nr_id_transacao_p,
				ie_aviso_a520_proc_w);

			pls_consulta_analise_pos_pck.set_vl_apresentado(vl_procedimento_imp_w);
			pls_consulta_analise_pos_pck.set_vl_liberado(nvl(vl_proc_pag_lib_w,0));
			pls_consulta_analise_pos_pck.set_vl_glosado(vl_glosa_w);
			pls_consulta_analise_pos_pck.set_nr_seq_item(nr_sequencia_w + 1);
		end if;
		end;
	end loop;
	close C01;
end if;

end pls_gerar_w_analise_pos_proc;
/
