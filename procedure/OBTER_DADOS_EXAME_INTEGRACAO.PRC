create or replace
procedure Obter_Dados_Exame_Integracao(  nr_prescricao_p	number,
					 nr_seq_prescr_p	number,
					 nr_seq_resultado_p	number,
					 nr_seq_material_p	number,
					 ds_sigla_equip_p	varchar2,
					 cd_parametro_p		varchar2,
					 nm_usuario_p		varchar2,
					 nr_seq_item_p		out number,
					 ie_formato_resultado_p	out varchar2,
					 ie_campo_calculo_p	out varchar2) is
nr_seq_item_w		number(10);
ie_formato_resultado_w	varchar2(3);
ie_campo_calculo_w	varchar2(1);
					 
begin

select  nvl(max(a.nr_seq_exame),0)
into	nr_seq_item_w
from 	exame_laboratorio b, 
	exame_lab_result_item a
where   b.nr_seq_exame = a.nr_seq_exame
and 	Obter_Equip_Exame_integracao(cd_parametro_p, ds_sigla_equip_p, 1) = b.nr_seq_exame 
and 	a.nr_seq_resultado = nr_seq_resultado_p
and 	a.nr_seq_prescr    = nr_seq_prescr_p;

select	nvl(max(obter_formato_result_exame(a.nr_seq_exame, nr_seq_material_p )),'') ie_formato_resultado
into	ie_formato_resultado_w
from 	exame_laboratorio b, 
	exame_lab_result_item a 
where 	b.nr_seq_exame = a.nr_seq_exame
and 	Obter_Equip_Exame_integracao(cd_parametro_p, ds_sigla_equip_p, 1) = b.nr_seq_exame
and 	a.nr_seq_resultado = nr_seq_resultado_p
and 	a.nr_seq_prescr    = nr_seq_prescr_p;



gerar_lab_log_interf_imp( nr_prescricao_p,
			  nr_seq_prescr_p,
			  null,
			  null,
			  null,
			  '1.01 log:  sigla_equip: ' || ds_sigla_equip_p ||
			  ' Material do TasyLAB: ' || nr_seq_material_p || 
			  ' cd_parametro: ' || cd_parametro_p ||
			  ' length(cd_parametro): ' || length(cd_parametro_p) || 
			  ' nr_Seq_resultado: ' || nr_seq_resultado_p || 
			  ' nr_seq_prescr: ' || nr_seq_prescr_p || 
			  ' ie_formato_result: ' ||ie_formato_resultado_w || 
			  ' nr_seq_item: ' || nr_seq_item_w,
			  null,
			  nm_usuario_p,
			  'N');			  

			  
if ( nvl(ie_formato_resultado_w,'0') = '0') then

	select	nvl(max(substr(obter_formato_result_exame(Obter_Equip_Exame_integracao(cd_parametro_p, ds_sigla_equip_p, 1), nr_seq_material_p),1,3)),'') ie_formato_resultado 
	into	ie_formato_resultado_w
	from 	dual;
	
	gerar_lab_log_interf_imp( nr_prescricao_p,
				  nr_seq_prescr_p,
				  null,
				  null,
				  null,
				  '1.11 log::  sigla_equip: ' || ds_sigla_equip_p ||
				  ' Material do TasyLAB: ' || nr_seq_material_p || 
				  ' cd_parametro: ' || cd_parametro_p ||
				  ' length(cd_parametro): ' || length(cd_parametro_p) || 
				  ' nr_Seq_resultado: ' || nr_seq_resultado_p || 
				  ' nr_seq_prescr: ' || nr_seq_prescr_p || 
				  ' ie_formato_result: ' ||ie_formato_resultado_w || 
				  ' nr_seq_item: ' || nr_seq_item_w,				  
				  null,
				  nm_usuario_p,
				  'N');

	
end if;

select  nvl(max(b.ie_campo_calculo),'')
into	ie_campo_calculo_w
from 	exame_laboratorio b, 
 	exame_lab_result_item a 
where 	b.nr_seq_exame = a.nr_seq_exame
and 	Obter_Equip_Exame_integracao(cd_parametro_p, ds_sigla_equip_p, 1) = b.nr_seq_exame 
and 	a.nr_seq_resultado = nr_seq_resultado_p
and 	a.nr_seq_prescr    = nr_seq_prescr_p;

if ( nvl(nr_seq_item_w,0) = 0) then

	select  nvl(max(a.nr_seq_exame),0)
	into	nr_seq_item_w
	from 	exame_laboratorio b, 
		exame_lab_result_item a 
	where 	b.nr_seq_exame = a.nr_seq_exame
	and 	nvl(b.cd_exame_integracao, b.cd_exame) = cd_parametro_p
	and 	a.nr_seq_resultado = nr_seq_resultado_p
	and 	a.nr_seq_prescr    = nr_seq_prescr_p;
	
	select  nvl(max(obter_formato_result_exame(a.nr_seq_exame, nr_seq_material_p)),'') ie_formato_resultado 
	into	ie_formato_resultado_w
	from 	exame_laboratorio b, 
		exame_lab_result_item a
	where 	b.nr_seq_exame = a.nr_seq_exame
	and 	nvl(b.cd_exame_integracao, b.cd_exame) = cd_parametro_p
	and 	a.nr_seq_resultado = nr_seq_resultado_p
	and 	a.nr_seq_prescr    = nr_seq_prescr_p;
	
	gerar_lab_log_interf_imp( nr_prescricao_p,
				  nr_seq_prescr_p,
				  null,
				  null,
				  null,				  
				  '2 log: Material do TasyLAB: ' || nr_seq_material_P ||
				  ' cd_parametro: ' || cd_parametro_p || 
				  ' nr_Seq_resultado: ' || nr_seq_resultado_p ||
				  ' nr_seq_prescr: ' || nr_seq_prescr_p ||
				  ' ie_formato_result: ' || ie_formato_resultado_w,				  
				  null,
				  nm_usuario_p,
				  'N');

				  
				  
	select 	nvl(max(b.ie_campo_calculo),'')
	into	ie_campo_calculo_w
	from 	exame_laboratorio b, 
		exame_lab_result_item a 
	where 	b.nr_seq_exame = a.nr_seq_exame
	and 	nvl(b.cd_exame_integracao, b.cd_exame) = cd_parametro_p
	and 	a.nr_seq_resultado = nr_seq_resultado_p
	and 	a.nr_seq_prescr    = nr_seq_prescr_p;
	
end if;

nr_seq_item_p 	        := nr_seq_item_w;
ie_formato_resultado_P  := ie_formato_resultado_w;
ie_campo_calculo_p  	:= ie_campo_calculo_w;

end Obter_Dados_Exame_Integracao;
/