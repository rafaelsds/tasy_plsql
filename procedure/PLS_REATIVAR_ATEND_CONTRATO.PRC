create or replace
procedure pls_reativar_atend_contrato
		(	nr_seq_suspensao_p	number,
			nm_usuario_p		Varchar2) is 
			
nr_seq_segurado_w	number(10);
			
Cursor C01 is
	select	nr_sequencia
	from	pls_segurado
	where	NR_SEQ_SUSPENSAO	= nr_seq_suspensao_p;

begin


update	pls_segurado_suspensao
set	dt_fim_suspensao 	= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_suspensao_p;

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	
	update	pls_segurado
	set	ie_situacao_atend	= 'A',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		NR_SEQ_SUSPENSAO	= null
	where	nr_sequencia		= nr_seq_segurado_w;

	pls_gerar_segurado_historico(	nr_seq_segurado_w, '59', sysdate, 'Reativa��o de atendimento atrav�s do contrat',
					'pls_reativar_atend_contrato', null, null, null,
					null, null, null, null,
					null, null, null, null,
					nm_usuario_p, 'N');	
	end;
end loop;
close C01;

commit;

end pls_reativar_atend_contrato;
/
