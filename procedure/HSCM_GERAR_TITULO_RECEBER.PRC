create or replace
procedure HSCM_GERAR_TITULO_RECEBER
			(cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

dt_emissao_w			date;
cd_moeda_w			number(10);
nr_titulo_externo_w		varchar2(255);
vl_titulo_w			number(15,2);
dt_vencimento_w			date;
ds_cpf_cgc_w			varchar2(16);
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
cd_portador_w			number(10);
cd_tipo_portador_w		number(5);
cd_tipo_taxa_juro_w		number(10);
cd_tipo_taxa_multa_w		number(10);
pr_juro_padrao_w		number(7,4);
pr_multa_padrao_w		number(7,4);
nr_titulo_w			number(10);

cd_pessoa_w			varchar2(16);
ie_pf_pj_w			varchar2(1);
nm_pessoa_w			varchar2(35);
ds_observacao_w			varchar2(10);
ds_endereco_w			varchar2(34);
ds_complemento_w		varchar2(20);
ds_bairro_w			varchar2(35);
cd_cep_w			varchar2(8);
ds_municipio_w			varchar2(35);
sg_estado_w			varchar2(15);
sg_pais_w			varchar2(3);
sg_idioma_w			varchar2(3);
nr_telefone_w			varchar2(16);
nr_telefone_celular_w		varchar2(16);
ds_email_w			varchar2(132);

qt_pessoa_w			number(10);
cd_nacionalidade_w		varchar2(8);
nr_seq_idioma_w			number(10);
nr_seq_pais_w			number(10);
qt_titulo_w			number(10);
nr_seq_trans_fin_contab_w	number(10);
nr_seq_tf_curto_prazo_w		number(10);

ie_origem_titulo_imp_w		varchar2(10);	
ie_tipo_titulo_imp_w		varchar2(2);
nr_seq_conta_banco_imp_w	number(10);
cd_conta_financ_imp_w		number(10);
cd_centro_custo_imp_w		number(9);
cd_conta_contabil_imp_w		varchar2(20);

/* cursor de pessoas */
cursor	c01 is
select	to_char(somente_numero(substr(a.ds_conteudo,1,16))) cd_pessoa,
	substr(a.ds_conteudo,17,1) ie_pf_pj,
	initcap(trim(substr(a.ds_conteudo,18,35))) nm_pessoa,
	initcap(trim(substr(a.ds_conteudo,53,10))) ds_observacao,
	initcap(trim(substr(a.ds_conteudo,63,34))) ds_endereco,
	initcap(trim(substr(a.ds_conteudo,97,20))) ds_complemento,
	initcap(trim(substr(a.ds_conteudo,117,35))) ds_bairro,
	to_char(somente_numero(substr(a.ds_conteudo,152,8))) cd_cep,
	initcap(trim(substr(a.ds_conteudo,160,35))) ds_municipio,
	trim(substr(a.ds_conteudo,195,3)) sg_estado,
	trim(substr(a.ds_conteudo,198,3)) sg_pais,
	trim(substr(a.ds_conteudo,216,3)) sg_idioma,
	trim(substr(a.ds_conteudo,219,16)) nr_telefone,
	trim(substr(a.ds_conteudo,235,16)) nr_telefone_celular,
	lower(trim(substr(a.ds_conteudo,251,132))) ds_email
from	w_interf_sefip a
where	substr(a.ds_conteudo,1,2)	= '00';

/* cursor de t�tulos */
cursor	c02 is
select	to_date(substr(a.ds_conteudo,11,8),'dd/mm/yyyy') dt_emissao,
	decode(trim(substr(a.ds_conteudo,19,5)),'BRL',1,'USD',3) cd_moeda,
	trim(substr(a.ds_conteudo,24,16)) || '-' || trim(substr(a.ds_conteudo,77,2)) nr_titulo_externo,
	to_number(replace(substr(a.ds_conteudo,56,13),'.',',')) vl_titulo,
	to_date(substr(a.ds_conteudo,69,8),'dd/mm/yyyy') dt_vencimento,
	to_char(somente_numero(substr(a.ds_conteudo,79,16))) ds_cpf_cgc
from	w_interf_sefip a
where	substr(a.ds_conteudo,1,2)	= '01';

begin

obter_param_usuario(801,79,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_seq_trans_fin_contab_w);
obter_param_usuario(801,147,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_seq_tf_curto_prazo_w);

open	c01;
loop
fetch	c01 into
	cd_pessoa_w,
	ie_pf_pj_w,
	nm_pessoa_w,
	ds_observacao_w,
	ds_endereco_w,
	ds_complemento_w,
	ds_bairro_w,
	cd_cep_w,
	ds_municipio_w,
	sg_estado_w,
	sg_pais_w,
	sg_idioma_w,
	nr_telefone_w,
	nr_telefone_celular_w,
	ds_email_w;
exit	when c01%notfound;

	if	(ie_pf_pj_w	= 'X') then	/* pessoa f�sica */

		select	count(*)
		into	qt_pessoa_w
		from	pessoa_fisica a
		where	a.nr_cpf	= cd_pessoa_w;

		if	(nvl(qt_pessoa_w,0)	= 0) then

			if	(sg_idioma_w = 'PT') then
				nr_seq_idioma_w	:= 1;
			elsif	(sg_idioma_w = 'ES') then
				nr_seq_idioma_w	:= 3;
			elsif	(sg_idioma_w = 'EN') then
				nr_seq_idioma_w	:= 7;
			end if;

			select	min(a.cd_nacionalidade)
			into	cd_nacionalidade_w
			from	nacionalidade a
			where	a.nr_seq_idioma	= nr_seq_idioma_w;

			select	max(a.nr_sequencia)
			into	nr_seq_pais_w
			from	pais a
			where	a.sg_pais	= sg_pais_w;

			select	to_char(pessoa_fisica_seq.nextval)
			into	cd_pessoa_fisica_w
			from	dual;

			insert	into pessoa_fisica
				(cd_nacionalidade,
				cd_pessoa_fisica,
				ds_observacao,
				dt_atualizacao,
				ie_tipo_pessoa,
				nm_pessoa_fisica,
				nm_usuario,
				nr_cpf,
				nr_seq_pais,
				nr_telefone_celular)
			values	(cd_nacionalidade_w,
				cd_pessoa_fisica_w,
				ds_observacao_w,
				sysdate,
				2,
				nm_pessoa_w,
				nm_usuario_p,
				cd_pessoa_w,
				nr_seq_pais_w,
				nr_telefone_celular_w);

			if	(nr_seq_idioma_w	is not null) then

				insert	into pessoa_fisica_idioma
					(cd_pessoa_fisica,
					dt_atualizacao,
					ie_fluencia,
					nm_usuario,
					nr_seq_idioma,
					nr_sequencia)
				values	(cd_pessoa_fisica_w,
					sysdate,
					'F',
					nm_usuario_p,
					nr_seq_idioma_w,
					pessoa_fisica_idioma_seq.nextval);

			end if;

			if	(ds_endereco_w is not null) or (ds_complemento_w is not null) or
				(ds_bairro_w is not null) or (cd_cep_w is not null) or
				(ds_municipio_w is not null) or (sg_estado_w is not null) or
				(nr_telefone_w is not null) or (ds_email_w is not null) then


				insert	into compl_pessoa_fisica
					(cd_cep,
					cd_pessoa_fisica,
					ds_bairro,
					ds_complemento,
					ds_email,
					ds_endereco,
					ds_municipio,
					dt_atualizacao,
					ie_tipo_complemento,
					nm_usuario,
					nr_sequencia,
					nr_telefone,
					sg_estado)
				values	(cd_cep_w,
					cd_pessoa_fisica_w,
					ds_bairro_w,
					ds_complemento_w,
					ds_email_w,
					ds_endereco_w,
					ds_municipio_w,
					sysdate,
					1,
					nm_usuario_p,
					1,
					nr_telefone_w,
					sg_estado_w);

			end if;

		end if;

	else	/* pessoa jur�dica */

		select	count(*)
		into	qt_pessoa_w
		from	pessoa_juridica a
		where	a.cd_cgc	= cd_pessoa_w;

		if	(nvl(qt_pessoa_w,0)	= 0) then

			select	max(a.nr_sequencia)
			into	nr_seq_pais_w
			from	pais a
			where	a.sg_pais	= sg_pais_w;

			insert	into pessoa_juridica
				(cd_cep,
				cd_cgc,
				cd_tipo_pessoa,
				ds_bairro,
				ds_complemento,
				ds_email,
				ds_endereco,
				ds_municipio,
				ds_observacao,
				ds_razao_social,
				dt_atualizacao,
				ie_prod_fabric,
				ie_situacao,
				nm_fantasia,
				nm_usuario,
				nr_seq_pais,
				nr_telefone,
				sg_estado)
			values	(cd_cep_w,
				cd_cgc_w,
				16,
				ds_bairro_w,
				ds_complemento_w,
				ds_email_w,
				ds_endereco_w,
				ds_municipio_w,
				ds_observacao_w,
				nm_pessoa_w,
				sysdate,
				'N',
				'A',
				nm_pessoa_w,
				nm_usuario_p,
				nr_seq_pais_w,
				nr_telefone_w,
				sg_estado_w);

		end if;

	end if;

end	loop;
close	c01;

select	max(a.cd_portador),
	max(a.cd_tipo_portador),
	max(a.cd_tipo_taxa_juro),
	max(a.cd_tipo_taxa_multa),
	max(a.pr_juro_padrao),
	max(a.pr_multa_padrao),
	max(ie_origem_titulo_imp),
	max(ie_tipo_titulo_imp),
	max(nr_seq_conta_banco_imp),
	max(cd_conta_financ_imp),
	max(cd_centro_custo_imp),
	max(cd_conta_contabil_imp)
into	cd_portador_w,
	cd_tipo_portador_w,
	cd_tipo_taxa_juro_w,
	cd_tipo_taxa_multa_w,
	pr_juro_padrao_w,
	pr_multa_padrao_w,
	ie_origem_titulo_imp_w,
	ie_tipo_titulo_imp_w,
	nr_seq_conta_banco_imp_w,
	cd_conta_financ_imp_w,
	cd_centro_custo_imp_w,
	cd_conta_contabil_imp_w
from	parametro_contas_receber a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

open	c02;
loop
fetch	c02 into
	dt_emissao_w,
	cd_moeda_w,
	nr_titulo_externo_w,
	vl_titulo_w,
	dt_vencimento_w,
	ds_cpf_cgc_w;
exit	when c02%notfound;

	select	count(*)
	into	qt_titulo_w
	from	titulo_receber a
	where	a.dt_vencimento		= dt_vencimento_w
	and	a.nr_titulo_externo	= nr_titulo_externo_w;

	if	(nvl(qt_titulo_w,0) > 0) then
		raise_application_error(-20011,'J� existe um t�tulo com n�mero externo ' || nr_titulo_externo_w || ' e data de vencimento ' || dt_vencimento_w);
	end if;


	select	max(a.cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pessoa_fisica a
	where	a.nr_cpf	= ds_cpf_cgc_w;

	if	(cd_pessoa_fisica_w	is null) then

		select	max(a.cd_cgc)
		into	cd_cgc_w
		from	pessoa_juridica a
		where	a.cd_cgc	= ds_cpf_cgc_w;

		if	(cd_cgc_w	is null) then

			raise_application_error(-20011,	'Pessoa n�o localizada no Tasy!' || chr(13) || chr(10) ||
							'C�digo (CPF/CNPJ) obtido do arquivo: ' || ds_cpf_cgc_w);

		end if;

	else

		cd_cgc_w	:= null;

	end if;

	select	titulo_seq.nextval
	into	nr_titulo_w
	from	dual;

	insert	into titulo_receber
		(cd_cgc,
		cd_estabelecimento,
		cd_moeda,
		cd_pessoa_fisica,
		cd_portador,
		cd_tipo_portador,
		cd_tipo_taxa_juro,
		cd_tipo_taxa_multa,
		dt_atualizacao,
		dt_emissao,
		dt_pagamento_previsto,
		dt_vencimento,
		ie_origem_titulo,
		ie_situacao,
		ie_tipo_emissao_titulo,
		ie_tipo_inclusao,
		ie_tipo_titulo,
		nm_usuario,
		nr_seq_trans_fin_contab,
		nr_titulo,
		nr_titulo_externo,
		nr_seq_conta_banco,
		tx_desc_antecipacao,
		tx_juros,
		tx_multa,
		vl_saldo_juros,
		vl_saldo_multa,
		vl_saldo_titulo,
		vl_titulo,
		NR_SEQ_TF_CURTO_PRAZO)
	values	(cd_cgc_w,
		cd_estabelecimento_p,
		cd_moeda_w,
		cd_pessoa_fisica_w,
		cd_portador_w,
		cd_tipo_portador_w,
		cd_tipo_taxa_juro_w,
		cd_tipo_taxa_multa_w,
		sysdate,
		dt_emissao_w,
		dt_vencimento_w,
		dt_vencimento_w,
		nvl(ie_origem_titulo_imp_w,'1'),
		'1',
		1,
		'1',
		nvl(ie_tipo_titulo_imp_w,'1'),
		nm_usuario_p,
		nr_seq_trans_fin_contab_w,
		nr_titulo_w,
		nr_titulo_externo_w,
		nr_seq_conta_banco_imp_w,
		0,
		pr_juro_padrao_w,
		pr_multa_padrao_w,
		0,
		0,
		vl_titulo_w,
		vl_titulo_w,
		nr_seq_tf_curto_prazo_w);
		
	if	(cd_conta_financ_imp_w is not null) or
		(cd_centro_custo_imp_w is not null) or
		(cd_conta_contabil_imp_w is not null) then
		
		
		insert into titulo_receber_classif
			(cd_centro_custo,
			cd_conta_contabil,
			cd_conta_financ,
			dt_atualizacao,
			nm_usuario,
			nr_sequencia,
			nr_titulo,
			vl_classificacao,
			vl_desconto,
			vl_original)
		values	(cd_centro_custo_imp_w,
			cd_conta_contabil_imp_w,
			cd_conta_financ_imp_w,
			sysdate,
			nm_usuario_p,
			1,
			nr_titulo_w,
			vl_titulo_w,
			0,
			0);		
		
	end if;

end	loop;
close	c02;

commit;

end HSCM_GERAR_TITULO_RECEBER;
/