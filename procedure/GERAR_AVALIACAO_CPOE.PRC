create or replace
procedure gerar_avaliacao_cpoe(nr_seq_tipo_avaliacao_p	number,
				nr_atendimento_p	number,
				cd_medico_p	varchar2,
				cd_pessoa_fisica_p	varchar2,
				cd_perfil_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				nr_seq_cpoe_p	number,
				ie_tipo_item_p	varchar2) is


nr_seq_avaliacao_w	med_avaliacao_paciente.nr_sequencia%type;
				
begin

if	(nr_atendimento_p is not null and cd_medico_p is not null and cd_perfil_p is not null and nr_seq_tipo_avaliacao_p is not null) then
	begin
	
	select	med_avaliacao_paciente_seq.nextval
	into	nr_seq_avaliacao_w
	from	dual;	
	
	insert	into med_avaliacao_paciente (
					cd_estabelecimento,
					cd_medico,
					cd_perfil_ativo,
					cd_pessoa_fisica,
					dt_atualizacao,
					dt_atualizacao_nrec,
					dt_avaliacao,
					ie_apresentar_web,
					ie_avaliacao_parcial,
					ie_avaliador_aux,
					ie_restricao_visualizacao,
					ie_situacao,
					nm_usuario,
					nm_usuario_nrec,
					nr_atendimento,
					nr_seq_mat_cpoe, 
					nr_seq_proc_cpoe, 
					nr_seq_hemo_cpoe,
					nr_seq_tipo_avaliacao,
					nr_sequencia)
		values (
					cd_estabelecimento_p,
					cd_medico_p,
					cd_perfil_p,
					cd_pessoa_fisica_p,
					sysdate,
					sysdate,
					sysdate,
					'N',
					'N',
					'N',
					'T',
					'A',
					nm_usuario_p,
					nm_usuario_p,
					nr_atendimento_p,
					decode(ie_tipo_item_p, 'M', nr_seq_cpoe_p, null),
					decode(ie_tipo_item_p, 'P', nr_seq_cpoe_p, null),
					decode(ie_tipo_item_p, 'H', nr_seq_cpoe_p, null),
					nr_seq_tipo_avaliacao_p,
					nr_seq_avaliacao_w);
	
	commit;

	exception
	when others then
		nr_seq_avaliacao_w	:= null;
	end;
	
end if;

end gerar_avaliacao_cpoe;
/
