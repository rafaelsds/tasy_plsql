create or replace
procedure executar_taxa_equipamento(	nr_cirurgia_p		number,
					nm_usuario_p		varchar2 ) as

cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
dt_prescricao_w		date;
qt_procedimento_w	number(8,3)	:= 0;
nr_prescricao_w		number(14,0);
nr_sequencia_w		number(6,0);
cd_convenio_w		number(5,0);
cd_categoria_w		varchar2(10);
cd_estabelecimento_w	number(4,0);
ie_proced_hor_atual_w	varchar2(1);
dt_inicio_real_w	date;
dt_procedimento_w	date;
dt_entrada_unidade_w	date;
cd_setor_atendimento_w	number(5,0);
nr_seq_interno_w	number(10,0);
nr_doc_convenio_w	varchar2(20);
ie_tipo_guia_w		varchar2(2);
cd_senha_w		varchar2(20);
ie_classificao_w	varchar2(1);
cd_tipo_procedimento_w	number(3,0);
nr_seq_proc_w		number(10,0);
varie_tipo_lancto_w	varchar2(10) := null;
nr_seq_proc_interno_w	number(10,0);
nr_atendimento_w	number(10,0);
ie_classificacao_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
ds_erro_w		varchar2(255);
cd_medico_cirurgiao_w	varchar2(10);
ie_forma_apresentacao_w		number(2);
dt_termino_w			date;
dt_inicio_procedimento_w	date;
ie_Atual_data_proc_w		Varchar2(1)	:= 'N';
ie_gerar_partic_w		Varchar2(1)	:= 'N';
dt_proc_equip_w			Date;
qt_taxa_w			number(15,3);
ie_gestao_cirurgia_w		varchar2(1);
cd_procedimento_ww		number(15,0);
ie_origem_proced_ww		number(10,0);
dt_inicio_w			date;
dt_fim_w			date;
dt_procedimento_ww		date;
ie_gerar_convenio_w		varchar2(1);
cd_convenio_ww			number(5,0) := null;
cd_categoria_ww			varchar2(10);
ie_doc_convenio_w		varchar2(3);
ie_executou_taxas_w		varchar2(1);
cd_perfil_w             	number(5);
ie_gerar_taxa_partic_w  	varchar2(1);   
cd_equipamento_w		number(10,0);

ie_regra_uso_w			VARCHAR2(1);
ie_tipo_entrada_w		VARCHAR2(255);
nr_atendimento_ww		NUMBER(10,0);
qt_lancamento_w			NUMBER(15,0);
dt_execucao_w			DATE;
cd_medico_executor_w		VARCHAR2(10);
cd_categoria_www		VARCHAR2(10);
nr_cirurgia_propaci_w		NUMBER(15,0);
nr_interno_conta_ww		NUMBER(15,0);
cd_setor_proc_w			NUMBER(5,0);
ie_acao_excesso_w		VARCHAR2(5);
qt_excedida_w			NUMBER(15,0);
cd_convenio_excesso_w		NUMBER(5,0);
cd_categoria_excesso_w		VARCHAR2(10);
cd_convenio_glosa_w		NUMBER(05,0);
cd_categoria_glosa_w		VARCHAR2(10);
cd_medico_executor_ww		varchar(10) 	:= null;
ds_texto_aux_w			varchar2(255);
cd_motivo_exc_conta_w		parametro_faturamento.cd_motivo_exc_conta%type;
ie_grava_medico_exec_w  varchar2(1);
cd_medico_retorno_w		varchar2(10);
ie_tipo_atendimento_w	number(3,0);
ie_medico_executor_w   	varchar2(1);
cd_cgc_retorno_w		varchar2(14);
cd_pessoa_fisica_ww		varchar2(10);
nr_seq_classificacao_w	number(10);
cd_setor_prescricao_w	number(5,0);

cursor c01 is
	select	nr_seq_proc_interno,
		qt_taxa,
		cd_procedimento,
		ie_origem_proced,
		dt_inicio,
		dt_fim,
		cd_convenio,
		cd_categoria,
		cd_equipamento
	from	equipamento_cirurgia a,
		taxa_equipamento_cirurgia b
	where	a.nr_sequencia = b.nr_seq_equi_cir 
	and	a.nr_cirurgia = nr_cirurgia_p
	and	nvl(a.ie_situacao,'A') = 'A'
	and	nvl(qt_taxa,0) >= 0
	and	a.dt_conta is null;   
	
	-- Definido altera��o com o Daniel, pois as taxas iguais n�o estavam indo para a conta quando geradas apos lan�amentos anteriores 
	/*and not exists (	select	1
        	          	from	procedimento_paciente w
                	  	where	nvl(b.nr_seq_proc_interno,0) 	= nvl(w.nr_seq_proc_interno,0)
                    	and		w.nr_cirurgia 					= nr_cirurgia_p
						and		cd_motivo_exc_conta is null
						union
						select	1
        	          	from	procedimento_paciente w
                	  	where	nvl(b.cd_procedimento,0) 	= nvl(w.cd_procedimento,0)
						and		nvl(b.ie_origem_proced,0)	= nvl(w.ie_origem_proced,0)
                    	and		w.nr_cirurgia 		= nr_cirurgia_p
						and		cd_motivo_exc_conta is null);*/

begin
gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p,WHEB_MENSAGEM_PCK.get_texto(299947,null), 'S', null, null); 
cd_perfil_w := obter_perfil_ativo;

select	nr_atendimento,
	cd_pessoa_fisica,
	nr_prescricao,
	dt_inicio_real,
	dt_entrada_unidade,
	cd_medico_cirurgiao,
	dt_termino
into	nr_atendimento_w,
	cd_pessoa_fisica_w,
	nr_prescricao_w,
	dt_inicio_real_w,
	dt_entrada_unidade_w,
	cd_medico_cirurgiao_w,
	dt_termino_w
from	cirurgia
where	nr_cirurgia	=	nr_cirurgia_p;	

select	nvl(max(cd_estabelecimento),1)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	=	nr_atendimento_w;	

obter_param_usuario(872, 313, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_gerar_convenio_w);
obter_param_usuario(900, 558, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_grava_medico_exec_w);

dt_procedimento_w :=	dt_inicio_real_w;	
if	(dt_procedimento_w < dt_entrada_unidade_w) then
	dt_procedimento_w	:= dt_entrada_unidade_w;
end if; 

begin
select	cd_setor_atendimento,
	nr_seq_interno
into	cd_setor_atendimento_w,
	nr_seq_interno_w
from	atend_paciente_unidade
where	dt_entrada_unidade	=	dt_entrada_unidade_w
and	nr_atendimento		=	nr_atendimento_w;
exception
when others then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(185246);
	--r a  i s e _application_error(-20011,'N�o foi gerado passagem de setor para esta cirurgia!');
end;
gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p,WHEB_MENSAGEM_PCK.get_texto(299948,null), 'S', null, null); 

OPEN C01;
LOOP
FETCH C01 into
	nr_seq_proc_interno_w,
	qt_taxa_w,
	cd_procedimento_ww,
	ie_origem_proced_ww,
	dt_inicio_w,
	dt_fim_w,
	cd_convenio_ww,
	cd_categoria_ww,
	cd_equipamento_w;
exit when c01%notfound;
	begin
	if	(ie_gerar_convenio_w = 'S') then
		select	max(cd_convenio),
			max(cd_categoria)
		into	cd_convenio_ww,
			cd_categoria_ww
		from	cirurgia
		where	nr_cirurgia	=	nr_cirurgia_p;
	end if;
	gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p, '01 ' || WHEB_MENSAGEM_PCK.get_texto(299950,null) || 'nr_seq_proc_interno_w=' || nr_seq_proc_interno_w ||  ' cd_procedimento_ww=' || cd_procedimento_ww || ' ie_origem_proced_ww=' || ie_origem_proced_ww || ' qt_taxa_w=' || qt_taxa_w, 'S', null, null); 
	dt_inicio_procedimento_w	:= null;
		
	qt_procedimento_w	:= 0;

	Obter_Proc_Tab_Interno(nr_seq_proc_interno_w,nr_prescricao_w,nr_atendimento_w,0,cd_procedimento_w,ie_origem_proced_w,null,null);

	if	(cd_procedimento_ww > 0) and (ie_origem_proced_ww > 0) then
		cd_procedimento_w	:= cd_procedimento_ww;
		ie_origem_proced_w	:= ie_origem_proced_ww;
	end if;
	
	Consiste_Paciente_Proc(nr_seq_proc_interno_w, nr_atendimento_w,cd_pessoa_fisica_w,cd_procedimento_w,ie_origem_proced_w,ds_erro_w);

	gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p, '02 ' || WHEB_MENSAGEM_PCK.get_texto(299950,null) || 'nr_seq_proc_interno_w=' || nr_seq_proc_interno_w ||  ' cd_procedimento_ww=' || cd_procedimento_ww || ' ie_origem_proced_ww=' || ie_origem_proced_ww || ' qt_taxa_w=' || qt_taxa_w, 'S', null, null); 
	
	if	(ds_erro_w is not null) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(185247,	'DS_ERRO_W=' || ds_erro_w);
		--r a i s e _application_error(-20011,ds_erro_w);
	end if;

	--Obter_Convenio_Execucao(nr_atendimento_w,dt_procedimento_w,cd_convenio_w,cd_categoria_w,nr_doc_convenio_w,ie_tipo_guia_w,cd_senha_w);
	
	if	(nvl(cd_convenio_ww,0) = 0) then
		Obter_Convenio_Execucao(nr_atendimento_w, dt_procedimento_w, cd_convenio_w, cd_categoria_w,nr_doc_convenio_w,ie_tipo_guia_w,cd_senha_w);
	else
		cd_convenio_w	:= cd_convenio_ww;
		cd_categoria_w	:= cd_categoria_ww;
		
		select	max(nr_doc_convenio),
			max(ie_tipo_guia),
			max(cd_senha)
		into	nr_doc_convenio_w,
			ie_tipo_guia_w,
			cd_senha_w
		from 	Atend_Categoria_convenio
		where	nr_atendimento	= nr_atendimento_w
		and	nr_seq_interno	=
				(	select	max(nr_seq_interno)
					from	atend_categoria_convenio
					where	nr_atendimento		= nr_atendimento_w
					and	dt_inicio_vigencia	<= dt_procedimento_w
					and	cd_convenio		= nvl(cd_convenio_w,cd_categoria)
					and	cd_categoria		= nvl(cd_categoria_w,cd_categoria));
					
		select	Obter_Valor_Conv_Estab(cd_convenio, cd_estabelecimento_w, 'IE_DOC_CONVENIO')
		into 	ie_doc_convenio_w
		from 	convenio
		where	cd_convenio	= cd_convenio_w;

		if 	(ie_doc_convenio_w = 'S') then
			nr_doc_convenio_w := nvl(cd_senha_w, nr_doc_convenio_w);
		elsif	(ie_doc_convenio_w in ('N','T')) then
			nr_doc_convenio_w := null;
		end if;

		if	(ie_doc_convenio_w	<> 'A') then
			cd_senha_w		:= null;
		end if;
	end if;	
	gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p, '03 ' || WHEB_MENSAGEM_PCK.get_texto(299950,null) || 'nr_seq_proc_interno_w=' || nr_seq_proc_interno_w ||  ' cd_procedimento_ww=' || cd_procedimento_ww || ' ie_origem_proced_ww=' || ie_origem_proced_ww || ' qt_taxa_w=' || qt_taxa_w, 'S', null, null); 
	
	select	max(ie_classificacao),
		max(cd_tipo_procedimento),
		max(ie_forma_apresentacao)
	into	ie_classificacao_w,
		cd_tipo_procedimento_w,
		ie_forma_apresentacao_w
	from	procedimento
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w;

	if	(ie_forma_apresentacao_w in (2,3,10,11,12,14,15,17)) then
		dt_inicio_procedimento_w	:= null;
		if (dt_inicio_real_w is not null) then
			dt_inicio_procedimento_w := dt_inicio_real_w;
		end if;
	end if;
	gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p, '04 ' || WHEB_MENSAGEM_PCK.get_texto(299950,null) || 'nr_seq_proc_interno_w=' || nr_seq_proc_interno_w ||  ' cd_procedimento_ww=' || cd_procedimento_ww || ' ie_origem_proced_ww=' || ie_origem_proced_ww || ' qt_taxa_w=' || qt_taxa_w, 'S', null, null); 
	
	-- Substitu�do pelo 180 da fun��o Gest�o da Cirurgia OS 178873
	Obter_Param_Usuario(0, 88, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_gestao_cirurgia_w);
	if	(ie_gestao_cirurgia_w = 'N') then
		Obter_Param_Usuario(901, 122, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_atual_data_proc_w);
	else
		Obter_Param_Usuario(900, 180, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_atual_data_proc_w);
	end if;
	obter_param_usuario(900, 109, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_gerar_partic_w);
	
	dt_procedimento_ww	:= dt_procedimento_w;
	if	((dt_inicio_w is not null) and (dt_fim_w is not null)) then
		dt_inicio_procedimento_w	:= dt_inicio_w;
		dt_proc_equip_w			:= dt_fim_w;
		dt_procedimento_ww		:= dt_fim_w;
		dt_termino_w			:= dt_fim_w;
	end if;
	gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p, '05 ' || WHEB_MENSAGEM_PCK.get_texto(299950,null) || 'nr_seq_proc_interno_w=' || nr_seq_proc_interno_w ||  ' cd_procedimento_ww=' || cd_procedimento_ww || ' ie_origem_proced_ww=' || ie_origem_proced_ww || ' qt_taxa_w=' || qt_taxa_w, 'S', null, null); 
	
	if	(dt_termino_w is not null) then
		dt_proc_equip_w	:= dt_termino_w;
	
		if	(ie_forma_apresentacao_w in (2,3,10,11,12,14,15,17)) then		
			select	nvl(OBTER_QTE_PROCED_CIRURGIA(cd_procedimento_w, ie_origem_proced_w, dt_proc_equip_w, dt_inicio_procedimento_w),0)
			into	qt_procedimento_w
			from	dual;
		end if; 
		
	end if;

	if	(qt_taxa_w is not null) then
		qt_procedimento_w	:= qt_taxa_w;
	end if;

	gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p, '06 ' || WHEB_MENSAGEM_PCK.get_texto(299950,null) || 'nr_seq_proc_interno_w=' || nr_seq_proc_interno_w ||  ' cd_procedimento_ww=' || cd_procedimento_ww || ' ie_origem_proced_ww=' || ie_origem_proced_ww || ' qt_taxa_w=' || qt_taxa_w, 'S', null, null); 
	
	if	(nvl(qt_procedimento_w,0) < 0) then
		qt_procedimento_w	:= 0;
	end if;
	
	if	(ie_forma_apresentacao_w not in (17)) or (qt_procedimento_w > 0) then
		
		select	procedimento_paciente_seq.nextval
		into	nr_seq_proc_w
		from	dual;
		gerar_cirurgia_hist(nr_cirurgia_p,'ETE',nm_usuario_p, '07 ' || WHEB_MENSAGEM_PCK.get_texto(299950,null) || 'nr_seq_proc_interno_w=' || nr_seq_proc_interno_w ||  ' cd_procedimento_ww=' || cd_procedimento_ww || ' ie_origem_proced_ww=' || ie_origem_proced_ww || ' qt_taxa_w=' || qt_taxa_w, 'S', null, null); 

		
		
		if (nvl(ie_grava_medico_exec_w,'N') = 'S' ) then
		
			select	max(ie_tipo_atendimento),
					max(nr_seq_classificacao)
			into	ie_tipo_atendimento_w,
					nr_seq_classificacao_w
			from	atendimento_paciente
			where	nr_atendimento	=	nr_atendimento_w;	
		
			select	max(cd_setor_atendimento)
			into	cd_setor_prescricao_w
			from	prescr_medica
			where	nr_prescricao = nr_prescricao_w;
				
		
			consiste_medico_executor(cd_estabelecimento_w, cd_convenio_w, cd_setor_atendimento_w, cd_procedimento_w, 
									ie_origem_proced_w,	ie_tipo_atendimento_w, null, nr_seq_proc_interno_w, 
									ie_medico_executor_w, cd_cgc_retorno_w, cd_medico_retorno_w, cd_pessoa_fisica_ww, 
									cd_medico_cirurgiao_w, dt_procedimento_w, nr_seq_classificacao_w, null, 
									null, cd_setor_prescricao_w);

			if (ie_medico_executor_w = 'N') then
            cd_medico_executor_ww := null;
         elsif (nvl(cd_medico_retorno_w,0) > 0) then
				cd_medico_executor_ww := cd_medico_retorno_w;
			else	
				select	max(cd_medico_executor)
				into	cd_medico_executor_ww
				from	procedimento_paciente
				where	nr_cirurgia = nr_cirurgia_p
				and 	nr_prescricao = nr_prescricao_w
				and		cd_procedimento = (select	max(cd_procedimento_princ) 
										   from		cirurgia
										   where	nr_cirurgia = nr_cirurgia_p);
			end if;
		else
			cd_medico_executor_ww := null;
		end if;
	
		insert into procedimento_paciente(
			nr_sequencia,
			cd_procedimento,
			ie_origem_proced,
			dt_prescricao,
			qt_procedimento,
			cd_pessoa_fisica,
			cd_convenio,
			cd_categoria,
			ie_proc_princ_atend,
			ie_video,
			tx_medico,
			tx_anestesia,
			tx_procedimento,
			ie_valor_informado,
			ie_guia_informada,
			cd_situacao_glosa,
			nm_usuario_original,
			nr_atendimento,
			cd_setor_atendimento,
			dt_entrada_unidade,
			nr_seq_atepacu,
			cd_senha,
			ie_auditoria,
			ie_emite_conta,
			cd_cgc_prestador,
			nr_seq_proc_interno,
			dt_procedimento,
			dt_atualizacao,
			nm_usuario,
			nr_cirurgia,
			nr_prescricao,
			nr_sequencia_prescricao,
			cd_medico_executor,
			cd_medico_req,
			dt_inicio_procedimento,
			nr_doc_convenio,
			ie_tipo_guia,
			cd_equipamento)
		values(
			nr_seq_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			dt_prescricao_w,
			decode(qt_procedimento_w, 0, 1, qt_procedimento_w),
			decode(ie_origem_proced_w,7,null,obter_pessoa_fisica_usuario(nm_usuario_p,'C')),
			cd_convenio_w,
			cd_categoria_w,	
			'N',
			'N',
			100,
			100,
			100,
			'N',
			'N',
			0,
			nm_usuario_p,
			nr_atendimento_w,
			cd_setor_atendimento_w,
			dt_entrada_unidade_w,
			nr_seq_interno_w,
			cd_senha_w,
			'N',
			'N',
			obter_cgc_estabelecimento(cd_estabelecimento_w),
			nr_seq_proc_interno_w,
			dt_procedimento_ww,
			sysdate,
			nm_usuario_p,
			nr_cirurgia_p,
			nr_prescricao_w,
			decode(nr_sequencia_w,0,null,nr_sequencia_w),
			cd_medico_executor_ww,
			null,

			dt_inicio_procedimento_w,
			nr_doc_convenio_w,
			ie_tipo_guia_w,
			cd_equipamento_w);
		commit;
		
		ie_executou_taxas_w := 'S';

		/*Inicio*/
		obter_param_usuario(900, 400, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_regra_uso_w);

		IF	(ie_regra_uso_w = 'S') THEN

			--ie_tipo_entrada_w := ie_tipo_entrada_w || ',' ||'18';
			SELECT	MAX(nr_atendimento),
				MAX(qt_procedimento),
				MAX(dt_procedimento),
				MAX(cd_medico_executor),
				MAX(cd_categoria),
				MAX(nr_cirurgia),
				MAX(nr_interno_conta),
				MAX(cd_setor_atendimento)
			INTO	nr_atendimento_ww,
				qt_lancamento_w,
				dt_execucao_w,
				cd_medico_executor_w,
				cd_categoria_www,
				nr_cirurgia_propaci_w,
				nr_interno_conta_ww,
				cd_setor_proc_w
			FROM	procedimento_paciente
			WHERE	nr_sequencia = nr_seq_proc_w;

			obter_regra_qtde_proc_exec(nr_atendimento_ww, cd_procedimento_w, ie_origem_proced_w, qt_lancamento_w, dt_execucao_w, cd_medico_executor_w,
							ie_acao_excesso_w, qt_excedida_w, ds_erro_w, cd_convenio_excesso_w, cd_categoria_excesso_w, nr_seq_proc_interno_w, cd_categoria_www, NULL, 0,
							nr_cirurgia_propaci_w, NULL, cd_setor_proc_w,NULL);
			
			IF 	(ie_acao_excesso_w = 'E') THEN
				IF 	(qt_excedida_w   > 0) THEN

					--Exclu�do pela regra de uso da fun��o Cadastro de Conv�nios
					ds_texto_aux_w := substr(wheb_mensagem_pck.get_texto(300556),1,255);
					
					select	max(cd_motivo_exc_conta)
					into	cd_motivo_exc_conta_w
					from	parametro_faturamento
					where	cd_estabelecimento = cd_estabelecimento_w;
				
					excluir_matproc_conta(nr_seq_proc_w, nr_interno_conta_ww, nvl(cd_motivo_exc_conta_w, 12), ds_texto_aux_w, 'P', nm_usuario_p);

				END IF;
				--ie_tipo_entrada_w := ie_tipo_entrada_w || ',' ||'19';
			ELSIF	(ie_acao_excesso_w = 'P') THEN
				IF 	(qt_excedida_w   > 0) THEN

					 obter_convenio_particular_pf(cd_estabelecimento_w, cd_convenio_w, '', dt_execucao_w, cd_convenio_glosa_w,
						cd_categoria_glosa_w);

					UPDATE	procedimento_paciente
					SET	nr_interno_conta	= NULL,
						cd_convenio		= cd_convenio_glosa_w,
						cd_categoria		= cd_categoria_glosa_w
					WHERE	nr_sequencia 		= nr_seq_proc_w;

					COMMIT;

					atualiza_preco_procedimento(nr_seq_proc_w, cd_convenio_w, nm_usuario_p);
					Ajustar_Conta_Vazia(nr_atendimento_ww, nm_usuario_p);

				END IF;
				--ie_tipo_entrada_w := ie_tipo_entrada_w || ',' ||'20';
			ELSIF	(ie_acao_excesso_w = 'Z') THEN
				IF 	(qt_excedida_w   > 0) THEN

					UPDATE	procedimento_paciente
					SET	vl_anestesista		= 0,
						vl_auxiliares		= 0,
						vl_custo_operacional	= 0,
						vl_materiais		= 0,
						vl_medico		= 0,
						vl_procedimento		= 0,
						ie_valor_informado	= 'S'
					WHERE	nr_sequencia 		= nr_seq_proc_w;

					COMMIT;

				END IF;
				--ie_tipo_entrada_w := ie_tipo_entrada_w || ',' ||'21';
			ELSIF	(ie_acao_excesso_w = 'C') THEN

				IF 	(qt_excedida_w   > 0) AND
					((cd_convenio_excesso_w IS NOT NULL) AND (cd_categoria_excesso_w IS NOT NULL)) THEN
					UPDATE	procedimento_paciente
					SET	nr_interno_conta	= NULL,
						cd_convenio		= cd_convenio_excesso_w,
						cd_categoria		= cd_categoria_excesso_w
					WHERE	nr_sequencia 		= nr_sequencia_w;


					atualiza_preco_procedimento(nr_sequencia_w, cd_convenio_excesso_w, nm_usuario_p);

					Ajustar_Conta_Vazia(nr_atendimento_w, nm_usuario_p);
				END IF;
				--ie_tipo_entrada_w := ie_tipo_entrada_w || ',' ||'22';
			END IF;

		END IF;
		/*Fim*/
		
		if	(ie_gerar_partic_w = 'S') then
		obter_param_usuario(872, 479, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_gerar_taxa_partic_w);
		if (ie_gerar_taxa_partic_w = 'S') or
		   ((ie_gerar_taxa_partic_w = 'N') and
		   (ie_classificacao_w not in (2,3))) then
			Gravar_partic_proced(nr_seq_proc_w, nm_usuario_p);
		end if;
		end if;

		if	(ie_classificacao_w in (1,8)) then 		
			atualiza_preco_procedimento(nr_seq_proc_w,cd_convenio_w,nm_usuario_p);
		else
			atualiza_preco_servico(nr_seq_proc_w,nm_usuario_p);
		end if;


		if	(nvl(varie_tipo_lancto_w,'0') = '0') then
			Gerar_Lanc_Automatico_mat(nr_atendimento_w,null,132,nm_usuario_p,null,null,null);
		end if;

		gerar_autor_regra(nr_atendimento_w,null,nr_seq_proc_w,null,null,nr_seq_proc_interno_w,'EP',nm_usuario_p,null,null,null,null,null,null,'','','');
	end if;
	end;
	END LOOP;
	CLOSE C01;

if (ie_executou_taxas_w = 'S') then
	update 	equipamento_cirurgia
	set		dt_conta	=	sysdate
	where 	nr_cirurgia =	nr_cirurgia_p
	and	dt_conta is null;
end if;

commit;

end executar_taxa_equipamento;
/