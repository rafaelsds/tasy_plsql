create or replace
procedure pls_valida_cmo_pag_web ( ds_arquivo_p in out varchar2) is
				
/*Utilizado para apresentar o caminho do arquivo da folha de pagamento corretamente no portal */
/*  ds_tipo_so_w 
 'L' - Linux    'caminho com /'  
 'W' - Windows  ' caminho com \ '*/
					
ds_arquivo_caminho_w 	pls_ame_lote_rem_arquivo.nm_arquivo%type;
ds_arquivo_caminho_aux_w pls_ame_lote_rem_arquivo.nm_arquivo%type;
ds_saida_arq	UTL_File.File_Type;
nr_posicao_w	number;
ds_arquivo_w 	varchar2(100);
ds_caminho_w	varchar2(150);
ds_tipo_so_w	varchar2(1) := 'L';
	
begin

	ds_arquivo_caminho_w := ds_arquivo_p;
	if(ds_arquivo_caminho_w is not null)then
	
		nr_posicao_w := instr(ds_arquivo_caminho_w ,'/', -1, 1);
		if(nr_posicao_w = 0)  then
			nr_posicao_w := instr(ds_arquivo_caminho_w ,'\',-1,1);
			ds_tipo_so_w := 'W';
		end if;
		ds_arquivo_w := substr(ds_arquivo_caminho_w,nr_posicao_w+1,length(ds_arquivo_caminho_w));
		ds_caminho_w := substr(ds_arquivo_caminho_w,1,nr_posicao_w-1);	
		
		begin
		--tenta abrir o arquivo 
		ds_saida_arq := UTL_File.Fopen(ds_caminho_w,ds_arquivo_w, 'r');
		exception
		when others then
		-- caso n�o consiga tenta tratar o caminho invertendo as barras
			begin
			if(ds_tipo_so_w = 'W')then
				ds_arquivo_caminho_aux_w := replace(ds_caminho_w,'\\','/');
				ds_arquivo_caminho_aux_w := replace(ds_arquivo_caminho_aux_w,'\','/')||'/';			
			end if;
			ds_arquivo_p := ds_arquivo_caminho_aux_w||ds_arquivo_w;			
				begin
				ds_saida_arq := UTL_File.Fopen(ds_arquivo_caminho_aux_w,ds_arquivo_w, 'r');
				exception
				when others then
				ds_arquivo_p := ds_arquivo_caminho_w;
				end;				
			end;
		end;	
	end if;

end pls_valida_cmo_pag_web;
/