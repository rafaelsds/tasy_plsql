create or replace
procedure duplica_nota_fiscal(nr_sequencia_p	in out	number,
			nm_usuario_p		varchar2,
			ie_data_p			varchar2) is

nr_sequencia_w			number(10);
nr_sequencia_nf_w		number(10);
cd_estabelecimento_w		number(4);
cd_cgc_emitente_w		varchar2(14);
cd_cgc_w			varchar2(14);
cd_serie_nf_w			nota_fiscal.cd_serie_nf%type;
nr_nota_fiscal_w		varchar2(255);
dt_emissao_w			date;
nr_seq_rat_w			number(10);
nr_seq_lote_ww			number(10);
nr_item_nf_w			number(10);
nr_sequencia_rat_w		number(10);
nr_seq_lote_w			number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_perfil_w			number(10);
ie_copiar_nr_danfe_w		varchar2(1);
ie_copiar_vencimentos_w		varchar2(1);
nr_nota_fiscal_ww               varchar2(255);
ie_numero_nota_w	        serie_nota_fiscal.ie_numero_nota%type;
qt_serie_w                      number(10);
ie_tipo_nota_w			nota_fiscal.ie_tipo_nota%type;
ie_entrada_w			varchar2(1);
parametro_w				varchar2(1);

-- ie_data_p     parmetro 161 da nota fiscal, se data atual ou data original

Cursor C01 is
	select 	nr_item_nf
	from	nota_fiscal_item
	where 	nr_sequencia = nr_sequencia_p
	order by 1;

Cursor C02 is
	select	nr_sequencia
	from	nota_fiscal_item_rateio
	where	nr_seq_nota = 	nr_sequencia_p
	and	nr_item_nf  = 	nr_item_nf_w;

Cursor c03 is
	select	nr_sequencia
	from	nota_fiscal_item_lote
	where	nr_seq_nota = 	nr_sequencia_p
	and	nr_item_nf  = 	nr_item_nf_w;

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

cd_perfil_w	:= Obter_perfil_ativo;

parametro_w := nvl(obter_valor_param_usuario(40, 487, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w),'N');

select	nota_fiscal_seq.nextval
into	nr_sequencia_w
from	dual;

select	cd_estabelecimento,
	cd_cgc_emitente,
	cd_serie_nf,
	nr_nota_fiscal,
	dt_emissao,
	cd_pessoa_fisica,
	cd_cgc
into	cd_estabelecimento_w,
	cd_cgc_emitente_w,
	cd_serie_nf_w,
	nr_nota_fiscal_w,
	dt_emissao_w,
	cd_pessoa_fisica_w,
	cd_cgc_w
from	nota_fiscal
where	nr_sequencia = nr_sequencia_p;

cd_perfil_w	:= obter_perfil_ativo;

select	substr(nvl(obter_valor_param_usuario(40, 406, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w),'N'),1,1),
	substr(nvl(obter_valor_param_usuario(40, 407, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w),'N'),1,1)
into	ie_copiar_nr_danfe_w,
	ie_copiar_vencimentos_w
from	dual;

if (ie_data_p = 'N') then
	dt_emissao_w := dt_emissao_w;
else
	dt_emissao_w := sysdate;
end if;


if (philips_param_pck.get_cd_pais = 2) then
	if (cd_serie_nf_w is not null) then
	begin
	
	select       count(*)
	into          qt_serie_w
	from         serie_nota_fiscal
	where	cd_serie_nf		= cd_serie_nf_w
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
	if (nvl(qt_serie_w,0) > 0) then
	
	
	select  nvl(ie_numero_nota,'M')
	into       ie_numero_nota_w
	from	serie_nota_fiscal
	where	cd_serie_nf		= cd_serie_nf_w
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
		if (ie_numero_nota_w  = 'S') then
	
		select	nr_ultima_nf + 1,
		        ie_numero_nota
		into	nr_nota_fiscal_ww,
		        ie_numero_nota_w
		from	serie_nota_fiscal
		where	cd_serie_nf		= cd_serie_nf_w
		and	cd_estabelecimento	= cd_estabelecimento_w;
	       
	        end if;
		
        end if;

	end;
	end if;
end if;


select	nvl(max(nr_sequencia_nf),0) +1,
	max(ie_tipo_nota)
into	nr_sequencia_nf_w,
	ie_tipo_nota_w
from	nota_fiscal
where	cd_estabelecimento	= cd_estabelecimento_w
and	nvl(cd_cgc_emitente,'0')= nvl(cd_cgc_emitente_w,'0')
and	cd_serie_nf		= cd_serie_nf_w
and	nr_nota_fiscal		= nr_nota_fiscal_w;

if ie_tipo_nota_w in ('EN','EF','EP') then
	ie_entrada_w := 'S';
end if;

insert into nota_fiscal(
	nr_sequencia,
	cd_estabelecimento,
	cd_cgc_emitente,
	cd_serie_nf,
	nr_nota_fiscal,
	nr_sequencia_nf,
	cd_operacao_nf,
	dt_emissao,
	dt_entrada_saida,
	ie_acao_nf,
	ie_emissao_nf,
	ie_tipo_frete,
	vl_mercadoria,
	vl_total_nota,
	qt_peso_bruto,
	qt_peso_liquido,
	dt_atualizacao,
	nm_usuario,
	cd_condicao_pagamento,
	cd_cgc,
	cd_pessoa_fisica,
	vl_ipi,
	vl_descontos,
	vl_frete,
	vl_seguro,
	vl_despesa_acessoria,
	ds_observacao,
	nr_nota_referencia,
	cd_serie_referencia,
	cd_natureza_operacao,
	dt_atualizacao_estoque,
	vl_desconto_rateio,
	ie_situacao,
	nr_ordem_compra,
	nr_sequencia_ref,
	nr_interno_conta,
	dt_atualizacao_cp_cr,
	nr_seq_protocolo,
	cd_moeda,
	vl_conv_moeda,
	ds_inconsistencia,
	dt_impressao,
	ds_obs_desconto_nf,
	nr_seq_classif_fiscal,
	ie_entregue_bloqueto,
	nr_seq_exportada,
	nr_nfe_imp,
	ie_tipo_nota,
	cd_setor_impressao,
	nr_seq_motivo_cancel,
	nr_seq_motivo_devol,
	nr_recibo,
	nr_seq_mensalidade,
	cd_setor_digitacao,
	nr_seq_prot_res_pls,
	nr_seq_lote_res_pls,
	nr_danfe,
	cd_tipo_servico,
	ie_nf_eletronica,
	NR_SEQ_FORMA_PAGTO, 
	CD_CONV_ENTRADA,
	NR_SEQ_MODELO, 
	CD_PACIENTE_INF,
	DS_DADOS_ADIC_ITENS,
	IE_NAT_OPER_RPS, 
	CD_FAB_ESTRANGEIRO,
	UF_EMBARQUE, 
	DS_LOCAL_EMBARQUE,
	CD_BANCO, 
	CD_AGENCIA_BANCARIA,
	IE_DIGITO_AGENCIA,
	NR_CONTA,
	NR_DIGITO_CONTA,
	NR_SEQ_TRANS_FINANC, 
	NR_SEQ_CLASSIFICACAO )
	(select	nr_sequencia_w,
		cd_estabelecimento,
		cd_cgc_emitente,
		cd_serie_nf,
		nvl(nr_nota_fiscal_ww,nr_nota_fiscal),
		nr_sequencia_nf_w,
		cd_operacao_nf,
		dt_emissao_w,
		sysdate,
		1,
		ie_emissao_nf,
		ie_tipo_frete,
		vl_mercadoria,
		vl_total_nota,
		qt_peso_bruto,
		qt_peso_liquido,
		sysdate,
		nm_usuario_p,
		cd_condicao_pagamento,
		cd_cgc,
		cd_pessoa_fisica,
		vl_ipi,
		vl_descontos,
		vl_frete,
		vl_seguro,
		vl_despesa_acessoria,
		substr(ds_observacao,1,3950) || wheb_mensagem_pck.get_Texto(312089, 'NR_SEQUENCIA_P='|| NR_SEQUENCIA_P), /*Duplicada atravs da nota #@NR_SEQUENCIA_P#@*/
		nr_nota_referencia,
		cd_serie_referencia,
		cd_natureza_operacao,
		'',
		vl_desconto_rateio,
		'1',
		nr_ordem_compra,
		nr_sequencia_ref,
		nr_interno_conta,
		dt_atualizacao_cp_cr,
		nr_seq_protocolo,
		cd_moeda,
		vl_conv_moeda,
		ds_inconsistencia,
		'',
		ds_obs_desconto_nf,
		nr_seq_classif_fiscal,
		ie_entregue_bloqueto,
		nr_seq_exportada,
		null,
		ie_tipo_nota,
		cd_setor_impressao,
		nr_seq_motivo_cancel,
		nr_seq_motivo_devol,
		nr_recibo,
		nr_seq_mensalidade,
		cd_setor_digitacao,
		nr_seq_prot_res_pls,
		nr_seq_lote_res_pls,
		decode(ie_copiar_nr_danfe_w,'S',decode(ie_entrada_w,'S',nr_danfe,null),null),
		cd_tipo_servico,
		ie_nf_eletronica,
		NR_SEQ_FORMA_PAGTO, 
		CD_CONV_ENTRADA, 
		NR_SEQ_MODELO, 
		CD_PACIENTE_INF,
		DS_DADOS_ADIC_ITENS,
		IE_NAT_OPER_RPS, 
		CD_FAB_ESTRANGEIRO,
		UF_EMBARQUE, 
		DS_LOCAL_EMBARQUE,
		CD_BANCO, 
		CD_AGENCIA_BANCARIA,
		IE_DIGITO_AGENCIA,
		NR_CONTA,
		NR_DIGITO_CONTA,
		NR_SEQ_TRANS_FINANC,
		NR_SEQ_CLASSIFICACAO
	from	nota_fiscal
	where	nr_sequencia = nr_sequencia_p);

if (parametro_w = 'S') then	

	insert into nota_fiscal_item(
		nr_sequencia,
		cd_estabelecimento,
		cd_cgc_emitente,
		cd_serie_nf,
		nr_nota_fiscal,
		nr_sequencia_nf,
		nr_item_nf,
		cd_natureza_operacao,
		qt_item_nf,
		vl_unitario_item_nf,
		vl_total_item_nf,
		dt_atualizacao,
		nm_usuario,
		vl_frete,
		vl_desconto,
		vl_despesa_acessoria,
		cd_material,
		cd_procedimento,
		cd_setor_atendimento,
		cd_conta,
		cd_local_estoque,
		ds_observacao,
		qt_peso_bruto,
		qt_peso_liquido,
		cd_unidade_medida_compra,
		qt_item_estoque,
		cd_unidade_medida_estoque,
		cd_lote_fabricacao,
		dt_validade,
		dt_atualizacao_estoque,
		cd_conta_contabil,
		vl_desconto_rateio,
		vl_seguro,
		cd_centro_custo,
		cd_material_estoque,
		ie_origem_proced,
		nr_ordem_compra,
		vl_liquido,
		pr_desconto,
		nr_item_oci,
		dt_entrega_ordem,
		nr_seq_conta_financ,
		nr_contrato,
		ds_complemento,
		nr_seq_proj_rec,
		vl_projeto,
		nr_atendimento,
		pr_desc_financ,
		vl_desc_financ,
		nr_seq_lic_item,
		nr_seq_ordem_serv,
		ds_inconsistencia,
		ds_justificativa,
		vl_liq_moeda_ref,
		nr_seq_unidade_adic,
		nr_emprestimo,
		nr_seq_item_emprestimo,
		ie_atualizar_barras,
		cd_barra_material,
		nr_sequencia_vinc_consig,
		nr_seq_proj_gpi,
		nr_seq_etapa_gpi,
		nr_seq_conta_gpi,
		dt_inicio_garantia,
		dt_fim_garantia,
		nr_seq_marca,
		ie_indeterminado,
	  nr_doc_importacao,
		dt_reg_importacao,
		ds_local_desemb,
		sg_uf_desemb,
		dt_desemb,
		nr_laudo,
		cd_exportador,
		nr_adicao,
		nr_seq_adicao,
		cd_fab_estrangeiro,
		vl_desc_adicao,
		nr_seq_cnae,
		nr_seq_opm,
		nr_seq_lote_fornec)
		(select	nr_sequencia_w,
			cd_estabelecimento,
			cd_cgc_emitente,
			cd_serie_nf,
			nvl(nr_nota_fiscal_ww,nr_nota_fiscal),
			nr_sequencia_nf_w,
			nr_item_nf,
			cd_natureza_operacao,
			qt_item_nf,
			vl_unitario_item_nf,
			vl_total_item_nf,
			sysdate,
			nm_usuario_p,
			vl_frete,
			vl_desconto,
			vl_despesa_acessoria,
			cd_material,
			cd_procedimento,
			cd_setor_atendimento,
			cd_conta,
			cd_local_estoque,
			ds_observacao,
			qt_peso_bruto,
			qt_peso_liquido,
			cd_unidade_medida_compra,
			qt_item_estoque,
			cd_unidade_medida_estoque,
			cd_lote_fabricacao,
			dt_validade,
			'',
			cd_conta_contabil,
			vl_desconto_rateio,
			vl_seguro,
			cd_centro_custo,
			cd_material_estoque,
			ie_origem_proced,
			nr_ordem_compra,
			(vl_total_item_nf - vl_desconto),
			pr_desconto,
			nr_item_oci,
			dt_entrega_ordem,
			nr_seq_conta_financ,
			nr_contrato,
			ds_complemento,
			nr_seq_proj_rec,
			vl_projeto,
			nr_atendimento,
			pr_desc_financ,
			vl_desc_financ,
			nr_seq_lic_item,
			nr_seq_ordem_serv,
			ds_inconsistencia,
			ds_justificativa,
			vl_liq_moeda_ref,
			nr_seq_unidade_adic,
			nr_emprestimo,
			nr_seq_item_emprestimo,
			ie_atualizar_barras,
			cd_barra_material,
			nr_sequencia_vinc_consig,
			nr_seq_proj_gpi,
			nr_seq_etapa_gpi,
			nr_seq_conta_gpi,
			dt_inicio_garantia,
			dt_fim_garantia,
			nr_seq_marca,
			ie_indeterminado,
	    nr_doc_importacao,
	    dt_reg_importacao,
	    ds_local_desemb,
	    sg_uf_desemb,
	    dt_desemb,
	    nr_laudo,
	    cd_exportador,
	    nr_adicao,
	    nr_seq_adicao,
	    cd_fab_estrangeiro,
	    vl_desc_adicao,
	    nr_seq_cnae,
	    nr_seq_opm,
	    nr_seq_lote_fornec
		from	nota_fiscal_item
		where	nr_sequencia = nr_sequencia_p);
else
	
	insert into nota_fiscal_item(
	nr_sequencia,
	cd_estabelecimento,
	cd_cgc_emitente,
	cd_serie_nf,
	nr_nota_fiscal,
	nr_sequencia_nf,
	nr_item_nf,
	cd_natureza_operacao,
	qt_item_nf,
	vl_unitario_item_nf,
	vl_total_item_nf,
	dt_atualizacao,
	nm_usuario,
	vl_frete,
	vl_desconto,
	vl_despesa_acessoria,
	cd_material,
	cd_procedimento,
	cd_setor_atendimento,
	cd_conta,
	cd_local_estoque,
	ds_observacao,
	qt_peso_bruto,
	qt_peso_liquido,
	cd_unidade_medida_compra,
	qt_item_estoque,
	cd_unidade_medida_estoque,
	cd_lote_fabricacao,
	dt_validade,
	dt_atualizacao_estoque,
	cd_conta_contabil,
	vl_desconto_rateio,
	vl_seguro,
	cd_centro_custo,
	cd_material_estoque,
	ie_origem_proced,
	nr_ordem_compra,
	vl_liquido,
	pr_desconto,
	nr_item_oci,
	dt_entrega_ordem,
	nr_seq_conta_financ,
	nr_contrato,
	ds_complemento,
	nr_seq_proj_rec,
	vl_projeto,
	nr_atendimento,
	pr_desc_financ,
	vl_desc_financ,
	nr_seq_lic_item,
	nr_seq_ordem_serv,
	ds_inconsistencia,
	ds_justificativa,
	vl_liq_moeda_ref,
	nr_seq_unidade_adic,
	nr_emprestimo,
	nr_seq_item_emprestimo,
	ie_atualizar_barras,
	cd_barra_material,
	nr_sequencia_vinc_consig,
	nr_seq_proj_gpi,
	nr_seq_etapa_gpi,
	nr_seq_conta_gpi,
	dt_inicio_garantia,
	dt_fim_garantia,
	nr_seq_marca,
	ie_indeterminado,
  nr_doc_importacao,
	dt_reg_importacao,
	ds_local_desemb,
	sg_uf_desemb,
	dt_desemb,
	nr_laudo,
	cd_exportador,
	nr_adicao,
	nr_seq_adicao,
	cd_fab_estrangeiro,
	vl_desc_adicao,
	nr_seq_cnae,
	nr_seq_opm)
	(select	nr_sequencia_w,
		cd_estabelecimento,
		cd_cgc_emitente,
		cd_serie_nf,
		nvl(nr_nota_fiscal_ww,nr_nota_fiscal),
		nr_sequencia_nf_w,
		nr_item_nf,
		cd_natureza_operacao,
		qt_item_nf,
		vl_unitario_item_nf,
		vl_total_item_nf,
		sysdate,
		nm_usuario_p,
		vl_frete,
		vl_desconto,
		vl_despesa_acessoria,
		cd_material,
		cd_procedimento,
		cd_setor_atendimento,
		cd_conta,
		cd_local_estoque,
		ds_observacao,
		qt_peso_bruto,
		qt_peso_liquido,
		cd_unidade_medida_compra,
		qt_item_estoque,
		cd_unidade_medida_estoque,
		cd_lote_fabricacao,
		dt_validade,
		'',
		cd_conta_contabil,
		vl_desconto_rateio,
		vl_seguro,
		cd_centro_custo,
		cd_material_estoque,
		ie_origem_proced,
		nr_ordem_compra,
		(vl_total_item_nf - vl_desconto),
		pr_desconto,
		nr_item_oci,
		dt_entrega_ordem,
		nr_seq_conta_financ,
		nr_contrato,
		ds_complemento,
		nr_seq_proj_rec,
		vl_projeto,
		nr_atendimento,
		pr_desc_financ,
		vl_desc_financ,
		nr_seq_lic_item,
		nr_seq_ordem_serv,
		ds_inconsistencia,
		ds_justificativa,
		vl_liq_moeda_ref,
		nr_seq_unidade_adic,
		nr_emprestimo,
		nr_seq_item_emprestimo,
		ie_atualizar_barras,
		cd_barra_material,
		nr_sequencia_vinc_consig,
		nr_seq_proj_gpi,
		nr_seq_etapa_gpi,
		nr_seq_conta_gpi,
		dt_inicio_garantia,
		dt_fim_garantia,
		nr_seq_marca,
		ie_indeterminado,
    nr_doc_importacao,
    dt_reg_importacao,
    ds_local_desemb,
    sg_uf_desemb,
    dt_desemb,
    nr_laudo,
    cd_exportador,
    nr_adicao,
    nr_seq_adicao,
    cd_fab_estrangeiro,
    vl_desc_adicao,
    nr_seq_cnae,
    nr_seq_opm
	from	nota_fiscal_item
	where	nr_sequencia = nr_sequencia_p);

end if;
	
insert into nota_fiscal_item_trib(
	cd_cgc_emitente,
	cd_estabelecimento,
	cd_serie_nf,
	cd_tributo,
	dt_atualizacao,
	ie_rateio,
	nm_usuario,
	nr_item_nf,
	nr_nota_fiscal,
	nr_seq_sit_trib,
	nr_sequencia,
	nr_sequencia_nf,
	tx_tributo,
	vl_base_adic,
	vl_basE_calculo,
	vl_base_nao_retido,
	vl_reducao_base,
	vl_trib_adic,
	vl_trib_nao_retido,
	vl_tributo)
	(select	cd_cgc_emitente,
		cd_estabelecimento,
		cd_serie_nf,
		cd_tributo,
		sysdate,
		ie_rateio,
		nm_usuario,
		nr_item_nf,
		nvl(nr_nota_fiscal_ww,nr_nota_fiscal),
		nr_seq_sit_trib,
		nr_sequencia_w,
		nr_sequencia_nf_w,
		tx_tributo,
		vl_base_adic,
		vl_base_calculo,
		vl_base_nao_retido,
		vl_reducao_base,
		vl_trib_adic,
		vl_trib_nao_retido,
		vl_tributo
	from	nota_fiscal_item_trib
	where	nr_sequencia = nr_sequencia_p);

insert into nota_fiscal_trib(
	nr_sequencia,
	cd_tributo,
	vl_tributo,
	dt_atualizacao,
	nm_usuario,
	vl_base_calculo,
	tx_tributo,
	vl_reducao_base,
	vl_trib_nao_retido,
	vl_base_nao_retido,
	vl_trib_adic,
	vl_base_adic,
	vl_reducao,
	cd_darf,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	cd_conta_financ,
	cd_variacao,
	ie_periodicidade,
	ie_origem_trib,
	nr_seq_interno)
		(select	nr_sequencia_w,
			cd_tributo,
			vl_tributo,
			sysdate,
			nm_usuario,
			vl_base_calculo,
			tx_tributo,
			vl_reducao_base,
			vl_trib_nao_retido,
			vl_base_nao_retido,
			vl_trib_adic,
			vl_base_adic,
			vl_reducao,
			cd_darf,
			nm_usuario_nrec,
			sysdate,
			cd_conta_financ,
			cd_variacao,
			ie_periodicidade,
			ie_origem_trib,
			nota_fiscal_trib_seq.nextval
		from	nota_fiscal_trib
		where	nr_sequencia = nr_sequencia_p);

insert into nota_fiscal_transportadora(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_nota,
	cd_cnpj,
	ie_tipo_frete,
	nr_placa_veiculo,
	uf_placa_veiculo,
	qt_transporte,
	ds_especie,
	qt_volume,
	qt_peso_bruto,
	qt_peso_liquido,
	nr_lacre)
(select	nota_fiscal_transportadora_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_sequencia_w,
	cd_cnpj,
	ie_tipo_frete,
	nr_placa_veiculo,
	uf_placa_veiculo,
	qt_transporte,
	ds_especie,
	qt_volume,
	qt_peso_bruto,
	qt_peso_liquido,
	nr_lacre
from	nota_fiscal_transportadora
where	nr_seq_nota = nr_sequencia_p);

if	(ie_copiar_vencimentos_w = 'S') then
	insert into nota_fiscal_venc(
		nr_sequencia,
		cd_estabelecimento,
		cd_cgc_emitente,
		cd_serie_nf,
		nr_sequencia_nf,
		dt_vencimento,
		vl_vencimento,
		dt_atualizacao,
		nm_usuario,
		vl_base_venc,
		vl_desconto,
		vl_desc_financ,
		nr_titulo_pagar,
		ds_observacao,
		nr_nota_fiscal,
		ie_venc_alt_sistema,
		ie_origem)
	(select	nr_sequencia_w,
		cd_estabelecimento_w,
		cd_cgc_emitente,
		cd_serie_nf,
		nr_sequencia_nf_w,
		dt_vencimento,
		vl_vencimento,
		sysdate,
		nm_usuario_p,
		vl_base_venc,
		vl_desconto,
		vl_desc_financ,
		null,
		ds_observacao,
		nvl(nr_nota_fiscal_ww,nr_nota_fiscal),
		ie_venc_alt_sistema,
		nvl(ie_origem,'N')
	from	nota_fiscal_venc
	where	nr_sequencia = nr_sequencia_p);
end if;

insert into nota_fiscal_hist(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	cd_evento,
	ds_historico,
	nr_seq_nota)
values(	nota_fiscal_hist_seq.nextval,
	sysdate,
	nm_usuario_p,
	'58',		
	wheb_mensagem_pck.get_Texto(312090, 'NR_SEQUENCIA_P='|| NR_SEQUENCIA_P), /*'Nota fiscal duplicada a partir da nota ' || nr_sequencia_p,*/
	nr_sequencia_w);


open C01;
loop
fetch C01 into	
	nr_item_nf_w;
exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into	
		nr_sequencia_rat_w;
	exit when C02%notfound;
		begin
		
		select 	nota_fiscal_item_rateio_seq.nextval
		into	nr_seq_rat_w
		from 	dual;
		
		insert into nota_fiscal_item_rateio(
			dt_atualizacao,
			ie_situacao,
			nm_usuario,
			nr_item_nf,
			nr_seq_nota,
			nr_sequencia,
			vl_rateio,
			cd_centro_custo,
			cd_conta_contabil,
			cd_conta_financ,
			nr_seq_criterio,
			vl_desconto,
			vl_despesa_acessoria,
			vl_frete,
			vl_seguro,
			vl_tributo,
			qt_rateio)
		(select DT_ATUALIZACAO,
			IE_SITUACAO,
			NM_USUARIO,
			NR_ITEM_NF,
			nr_sequencia_w,
			nr_seq_rat_w,
			VL_RATEIO,
			CD_CENTRO_CUSTO,
			CD_CONTA_CONTABIL,
			CD_CONTA_FINANC,
			NR_SEQ_CRITERIO,
			VL_DESCONTO,
			VL_DESPESA_ACESSORIA,
			VL_FRETE,
			VL_SEGURO,
			VL_TRIBUTO,
			qt_rateio
		from	nota_fiscal_item_rateio
		where	nr_seq_nota 	= 	nr_sequencia_p
		and	nr_item_nf  	= 	nr_item_nf_w
		and	nr_sequencia 	= 	nr_sequencia_rat_w);
		end;
	end loop;
	close C02;

	open C03;
	loop
	fetch C03 into	
		nr_seq_lote_w;
	exit when C03%notfound;
		begin
		
		select 	nota_fiscal_item_lote_seq.nextval
		into	nr_seq_lote_ww
		from 	dual;
		
		insert into nota_fiscal_item_lote(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_nota,
			nr_item_nf,
			dt_validade,
			qt_material,
			cd_lote_fabricacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_marca,
			nr_seq_lote_fornec,
			cd_barra_material,
			ds_barras,
			ie_indeterminado,
			qt_compra,
			dt_fabricacao,
			nr_seq_lote_fornec_orig)
		(select	nr_seq_lote_ww,
			dt_atualizacao,
			nm_usuario,
			nr_sequencia_w,
			nr_item_nf,
			dt_validade,
			qt_material,
			cd_lote_fabricacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_marca,
			nr_seq_lote_fornec,
			cd_barra_material,
			ds_barras,
			ie_indeterminado,
			qt_compra,
			dt_fabricacao,
			nr_seq_lote_fornec_orig
		from	nota_fiscal_item_lote
		where	nr_seq_nota = nr_sequencia_p
		and	nr_item_nf = nr_item_nf_w
		and	nr_sequencia = nr_seq_lote_w);
		end;
	end loop;
	close C03;
	end;
end loop;
close C01;

nr_sequencia_p := nr_sequencia_w;
	
commit;

end duplica_nota_fiscal;
/
