create or replace
procedure pls_gerar_mens_coparticipacao
			(	nr_seq_mensalidade_seg_p	number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number) is

nr_seq_conta_w			pls_conta.nr_sequencia%type;
nr_seq_conta_copartic_w		pls_conta_coparticipacao.nr_sequencia%type;
vl_coparticipacao_w		pls_conta_coparticipacao.vl_coparticipacao%type;
vl_copartic_mens_w		pls_conta_coparticipacao.vl_copartic_mens%type;
vl_copartic_item_w		pls_conta_coparticipacao.vl_coparticipacao%type;
dt_competencia_mens_w		pls_conta_coparticipacao.dt_competencia_mens%type;
nr_seq_protocolo_w		pls_protocolo_conta.nr_sequencia%type;
dt_mes_competencia_w		pls_protocolo_conta.dt_mes_competencia%type;
vl_ato_auxiliar_w		pls_mensalidade_seg_item.vl_ato_auxiliar%type;
vl_ato_cooperado_w		pls_mensalidade_seg_item.vl_ato_cooperado%type;
vl_ato_nao_cooperado_w		pls_mensalidade_seg_item.vl_ato_nao_cooperado%type;
ie_gerar_copartic_w		varchar2(1);
dt_mesano_referencia_fim_w	date;
ie_status_liberado_w		varchar2(1);
nr_seq_pagador_w		pls_segurado_pagador.nr_seq_pagador%type;
nr_seq_mensalidade_canc_w	pls_mensalidade.nr_sequencia%type;
qt_conta_w			number(10);
qt_registro_w			pls_integer;
nr_seq_item_mensalidade_w	pls_mensalidade_seg_item.nr_sequencia%type;
ie_tipo_protocolo_w		pls_conta_coparticipacao.ie_tipo_protocolo%type;
ie_gerar_a700_w			pls_parametros.ie_gerar_coparticipacao_a700%type;
qt_seg_pag_w			number(2) := 0;
nr_seq_pagador_mens_w		pls_contrato_pagador.nr_sequencia%type;
ie_status_w			varchar2(1);

Cursor C01 is
	select	a.nr_seq_conta,
		a.nr_seq_protocolo,
		a.dt_mes_competencia,
		a.dt_competencia_mens,
		sum(a.vl_coparticipacao) vl_coparticipacao,
		sum(a.vl_ato_cooperado) vl_ato_cooperado,
		sum(a.vl_ato_auxiliar) vl_ato_auxiliar,
		sum(a.vl_ato_nao_cooperado) vl_ato_nao_cooperado,
		a.ie_tipo_protocolo
	from	pls_conta_coparticipacao a
	where	a.nr_seq_segurado			= pls_store_data_mens_pck.get_nr_seq_segurado
	and	nvl(a.ie_status_mensalidade,'L')	= 'L' -- Liberado para a mensalidade
	and	nvl(a.ie_status_coparticipacao,'S')	= 'S' -- Liberado para a mensalidade
	and	nvl(a.ie_gerar_mensalidade,'S')		= 'S' -- O campo somente estar� como "N" quando a coparticipa��o foi migrada pelo cliente
	and	a.dt_estorno is null
	and	a.ie_tipo_segurado in ('B','A','R')
	and	a.nr_seq_mensalidade_seg is null
	and	((a.nr_seq_pagador = nr_seq_pagador_w) or (a.nr_seq_pagador is null) 
	or exists	(select	1
			from	pls_pagador_item_mens x
			where	x.nr_seq_pagador = a.nr_seq_pagador
			and	x.nr_seq_pagador_item = nr_seq_pagador_w
			and	x.ie_tipo_item = '3'))
	and	pls_store_data_mens_pck.get_nr_seq_regra_grupo_lote is null
	group by a.nr_seq_conta, a.nr_seq_protocolo, a.dt_mes_competencia, a.dt_competencia_mens, a.ie_tipo_protocolo
	union all
	select	a.nr_seq_conta,
		a.nr_seq_protocolo,
		a.dt_mes_competencia,
		a.dt_competencia_mens,
		sum(a.vl_coparticipacao) vl_coparticipacao,
		sum(a.vl_ato_cooperado) vl_ato_cooperado,
		sum(a.vl_ato_auxiliar) vl_ato_auxiliar,
		sum(a.vl_ato_nao_cooperado) vl_ato_nao_cooperado,
		a.ie_tipo_protocolo
	from	pls_conta_coparticipacao a
	where	a.nr_seq_segurado			= pls_store_data_mens_pck.get_nr_seq_segurado
	and	nvl(a.ie_status_mensalidade,'L')	= 'L' -- Liberado para a mensalidade
	and	nvl(a.ie_status_coparticipacao,'S')	= 'S' -- Liberado para a mensalidade
	and	nvl(a.ie_gerar_mensalidade,'S')		= 'S' -- O campo somente estar� como "N" quando a coparticipa��o foi migrada pelo cliente
	and	a.dt_estorno is null
	and	a.ie_tipo_segurado in ('B','A','R')
	and	a.nr_seq_mensalidade_seg is null
	and	(a.nr_seq_pagador = nr_seq_pagador_w or a.nr_seq_pagador is null)
	and	pls_store_data_mens_pck.get_nr_seq_regra_grupo_lote is not null
	and	((a.nr_seq_prestador_exec = pls_store_data_mens_pck.get_seq_prestador_regra_exec) or (pls_store_data_mens_pck.get_seq_prestador_regra_exec is null))
	and	((a.nr_seq_prestador_atend = pls_store_data_mens_pck.get_seq_prestador_regra_atend) or (pls_store_data_mens_pck.get_seq_prestador_regra_atend is null))
	and	((a.ie_tipo_prestador_exec = pls_store_data_mens_pck.get_tipo_prestador_exec_regra) or (pls_store_data_mens_pck.get_tipo_prestador_exec_regra is null))
	and	((a.ie_tipo_prestador_atend = pls_store_data_mens_pck.get_tipo_prestador_atend_regra) or (pls_store_data_mens_pck.get_tipo_prestador_atend_regra is null))
	and	((a.ie_tipo_guia = pls_store_data_mens_pck.get_ie_tipo_guia_regra) or (pls_store_data_mens_pck.get_ie_tipo_guia_regra is null))
	and	((a.ie_tipo_segurado = pls_store_data_mens_pck.get_ie_tipo_segurado_regra) or (pls_store_data_mens_pck.get_ie_tipo_segurado_regra is null))
	and	((a.ie_preco = pls_store_data_mens_pck.get_ie_preco_regra) or (pls_store_data_mens_pck.get_ie_preco_regra is null))
	group by a.nr_seq_conta, a.nr_seq_protocolo, a.dt_mes_competencia, a.dt_competencia_mens, a.ie_tipo_protocolo;

Cursor C02 is
	select	nr_sequencia,
		vl_coparticipacao
	from	pls_conta_coparticipacao
	where	nr_seq_conta	= nr_seq_conta_w
	and	nvl(ie_status_mensalidade,'L') 	= 'L' -- Liberado para a mensalidade
	and	nvl(ie_status_coparticipacao,'S')	= 'S' -- Liberado para a mensalidade
	and	nvl(ie_gerar_mensalidade,'S')		= 'S' -- O campo somente estar� como "N" quando a coparticipa��o foi migrada pelo cliente
	and	nr_seq_mensalidade_seg is null
	and	nr_seq_pagador = nr_seq_pagador_w;

begin
dt_mesano_referencia_fim_w	:= fim_dia(last_day(pls_store_data_mens_pck.get_dt_mesano_referencia));
ie_gerar_a700_w			:= pls_store_data_mens_pck.get_ie_gerar_copartic_a700;

select	count(1) --N�o alterar esse select, pois possui �ndice espec�fico  PLSCOCOP_I4
into	qt_registro_w
from	dual
where	exists(	select	1
		from	pls_conta_coparticipacao a
		where	a.nr_seq_segurado	= pls_store_data_mens_pck.get_nr_seq_segurado
		and	a.nr_seq_mensalidade_seg is null);

select	max(nr_seq_pagador)
into	nr_seq_pagador_mens_w
from	pls_pagador_item_mens
where	nr_seq_pagador_item = pls_store_data_mens_pck.get_nr_seq_pagador
and	ie_tipo_item = '3';

if	(nr_seq_pagador_mens_w is null) then
	begin
	select	count(1)
	into	qt_seg_pag_w
	from	pls_segurado_pagador
	where	nr_seq_segurado	= pls_store_data_mens_pck.get_nr_seq_segurado
	and	nr_seq_pagador	= pls_store_data_mens_pck.get_nr_seq_pagador
	and	dt_fim_vigencia is not null;
	exception
	when others then
		qt_seg_pag_w := 0;
	end;
else
	qt_seg_pag_w := 1;
end if;

if	(qt_seg_pag_w = 0) then
	nr_seq_pagador_w := nvl(pls_store_data_mens_pck.get_seq_pagador_copartic,pls_store_data_mens_pck.get_nr_seq_pagador_benef);
else
	nr_seq_pagador_w := pls_store_data_mens_pck.get_nr_seq_pagador;
end if;

pls_store_data_mens_pck.set_vl_coparticipacao_gerada(0);

if	(qt_registro_w > 0) then
	open C01;
	loop
	fetch C01 into
		nr_seq_conta_w,
		nr_seq_protocolo_w,
		dt_mes_competencia_w,
		dt_competencia_mens_w,
		vl_coparticipacao_w,
		vl_ato_cooperado_w,
		vl_ato_auxiliar_w,
		vl_ato_nao_cooperado_w,
		ie_tipo_protocolo_w;
	exit when C01%notfound;
		begin
		ie_gerar_copartic_w	:= 'N';
		qt_conta_w		:= 1;
		if	(pls_store_data_mens_pck.get_ie_ref_copartic_mens = 'C') then
			nr_seq_mensalidade_canc_w := pls_store_data_mens_pck.get_nr_seq_mens_canc;
			
			if	(nr_seq_mensalidade_canc_w is not null) then
				select	count(1)
				into	qt_conta_w
				from	pls_mensalidade a,
					pls_mensalidade_segurado b,
					pls_mensalidade_seg_item c
				where	a.nr_sequencia = b.nr_seq_mensalidade
				and	b.nr_sequencia = c.nr_seq_mensalidade_seg
				and	b.dt_mesano_referencia = pls_store_data_mens_pck.get_dt_mesano_referencia
				and	c.nr_seq_conta = nr_seq_conta_w;
			end if;
		end if;
		
		if	(qt_conta_w > 0) then
			if	(trunc(dt_mes_competencia_w,'month') <= dt_mesano_referencia_fim_w) and
				((dt_competencia_mens_w is null) or (dt_competencia_mens_w = pls_store_data_mens_pck.get_dt_mesano_referencia)) and
				((ie_tipo_protocolo_w = 'C') or (ie_tipo_protocolo_w = 'I') or ((ie_tipo_protocolo_w = 'F') and (ie_gerar_a700_w = 'S'))) then
				if	(pls_store_data_mens_pck.get_ie_forma_pagamento = 'P') then
					select	max(ie_status)
					into	ie_status_w
					from	pls_protocolo_conta
					where	nr_sequencia	= nr_seq_protocolo_w;
					
					if	((ie_status_w in ('3','6','7')) or
						((ie_status_w = '4') and (pls_store_data_mens_pck.get_ie_copartic_prot_sem_pag = 'S'))) then
						ie_status_liberado_w	:= 'S';
					else
						ie_status_liberado_w	:= 'N';
					end if;
				else
					begin
					select	'S'
					into	ie_status_liberado_w
					from	pls_conta
					where	nr_sequencia	= nr_seq_conta_w
					and	ie_status	in ('F','S');
					exception
					when others then
						ie_status_liberado_w	:= 'N';
					end;
				end if;
				
				if	(ie_status_liberado_w = 'S') then
					if	(nvl(pls_store_data_mens_pck.get_vl_max_copartic,0) > 0) then
						vl_coparticipacao_w	:= 0;
						open C02;
						loop
						fetch C02 into
							nr_seq_conta_copartic_w,
							vl_copartic_item_w;
						exit when C02%notfound;
							begin
							--Caso a coparticipa��o j� gerada seja maior, ent�o 0 a coparticipa��o
							if	(nvl(pls_store_data_mens_pck.get_vl_coparticipacao_gerada,0) >= pls_store_data_mens_pck.get_vl_max_copartic) then
								vl_copartic_item_w	:= 0;
								update	pls_conta_coparticipacao
								set	vl_copartic_mens		= 0,
									nr_seq_regra_limite_copartic	= pls_store_data_mens_pck.get_nr_seq_regra_limite_copar
								where	nr_sequencia			= nr_seq_conta_copartic_w;
								ie_gerar_copartic_w	:= 'S';
							--Caso a copartici��o j� gerada e mais a que vai ser gerada seja maior
							elsif	((nvl(pls_store_data_mens_pck.get_vl_coparticipacao_gerada,0) + vl_copartic_item_w) > pls_store_data_mens_pck.get_vl_max_copartic) then
								--Se foi o primeiro item
								if	(nvl(pls_store_data_mens_pck.get_vl_coparticipacao_gerada,0) = 0) and
									(vl_copartic_item_w > pls_store_data_mens_pck.get_vl_max_copartic) then
									vl_copartic_mens_w	:= pls_store_data_mens_pck.get_vl_max_copartic;
								else
									vl_copartic_mens_w	:= pls_store_data_mens_pck.get_vl_max_copartic - pls_store_data_mens_pck.get_vl_coparticipacao_gerada;
								end if;
								
								vl_copartic_item_w	:= vl_copartic_mens_w;
								
								update	pls_conta_coparticipacao
								set	vl_copartic_mens		= vl_copartic_mens_w,
									nr_seq_regra_limite_copartic	= pls_store_data_mens_pck.get_nr_seq_regra_limite_copar
								where	nr_sequencia			= nr_seq_conta_copartic_w;
							end if;
							
							vl_coparticipacao_w	:= vl_coparticipacao_w + nvl(vl_copartic_item_w,0);
							--Atualiza a regra gerada
							pls_store_data_mens_pck.set_vl_coparticipacao_gerada(pls_store_data_mens_pck.get_vl_coparticipacao_gerada + vl_copartic_item_w);
							end;
						end loop;
						close C02;
					end if;
					
					if	((nvl(vl_coparticipacao_w,0) > 0) or (ie_gerar_copartic_w = 'S')) then
						nr_seq_item_mensalidade_w := null;
						
						pls_insert_mens_seg_item('3',	nm_usuario_p,	null,
									null,	null,	null,
									null,	null,	null,
									'N',	null,	null,
									null,	null,	nr_seq_conta_w,
									null,	null,	nr_seq_mensalidade_seg_p,
									null,	null,	null,
									nr_seq_protocolo_w,	null,	null,
									null,	null,	null,
									null,	vl_ato_auxiliar_w,	vl_ato_cooperado_w,
									vl_ato_nao_cooperado_w,	vl_coparticipacao_w,	nr_seq_item_mensalidade_w);
						
						if	(nr_seq_item_mensalidade_w is not null) then
							update	pls_conta_coparticipacao
							set	nr_seq_mensalidade_seg	= nr_seq_mensalidade_seg_p,
								dt_atualizacao		= sysdate,
								nm_usuario		= nm_usuario_p,
								ie_status_mensalidade   = 'G'
							where	nr_seq_conta		= nr_seq_conta_w
							and	nvl(ie_status_coparticipacao,'S')  = 'S'
							and	nr_seq_mensalidade_seg is null;
							
							pls_atualizar_item_faturado(nr_seq_conta_w, 1, nm_usuario_p);
						end if;
					end if;
				end if;
			end if;
		end if;
		end;
	end loop;
	close C01;
end if;

/* N�o pode dar commit nesta procedure */
--commit;

end pls_gerar_mens_coparticipacao;
/