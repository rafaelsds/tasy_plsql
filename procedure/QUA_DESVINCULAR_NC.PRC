create or replace
procedure qua_desvincular_nc(nr_sequencia_p	number,
			     nm_usuario_p	Varchar2) is 

begin

if	(nvl(nr_sequencia_p,0) > 0) then
	
	begin
	
	delete 
	from qua_nao_conform_vinc
	where nr_sequencia = nr_sequencia_p;
	
	end;
	
end if;

commit;

end qua_desvincular_nc;
/