create or replace
procedure Gerar_EIS_Man_Executor(	cd_estabelecimento_p	number,
     				dt_referencia_p		date,
     				nm_usuario_p		varchar2) is

dt_referencia_w  		date;
nm_usuario_exec_w  	varchar2(15);
ds_usuario_exec_w  	varchar2(255);
qt_ordens_w   		number(10,0);
qt_total_os_w  		number(10,0);
nr_seq_planej_w	  	number(10,0);
nr_seq_estagio_w	  	number(10,0);
nr_seq_localizador_w  	number(10,0);
qt_encerrada_w 	 	number(10,0);
qt_pendente_w   		number(10,0);
nr_sequencia_w		number(10,0);
nr_seq_log_w		number(10,0);
ie_informacao_w		varchar2(02);
nr_grupo_trabalho_w	number(10,0);
cd_estabelecimento_w	number(04,0);
cd_setor_local_w	setor_atendimento.cd_setor_atendimento%type;
nr_seq_motivo_cancel_w  man_ordem_servico.nr_seq_motivo_cancel%type;

cursor c01 is
/*Pendentes*/
select	decode(a.dt_fim_real,'','0',decode(trunc(a.dt_fim_real,'month'),dt_referencia_w,'1','0')) ie_informacao,
	count(distinct b.nm_usuario_exec),
	b.nm_usuario_exec,
	a.nr_grupo_planej,
	nvl(a.nr_seq_estagio,0),
	a.nr_seq_localizacao,
	a.nr_grupo_trabalho,
	a.cd_estabelecimento,
	a.nr_seq_motivo_cancel
from	man_ordem_servico_exec b,
	man_ordem_servico_v a
where	a.nr_sequencia   = b.nr_seq_ordem
and	trunc(a.dt_ordem_servico, 'month') = dt_referencia_w
group by
	nvl(a.nr_seq_estagio,0),
	b.nm_usuario_exec,
	a.nr_grupo_planej,
	a.dt_fim_real,
	a.nr_grupo_trabalho,
	a.nr_seq_localizacao,
	a.cd_estabelecimento,
	a.nr_seq_motivo_cancel
union all
/*Encerradas*/
select	'1' ie_informacao,
	count(distinct b.nm_usuario_exec),
	b.nm_usuario_exec,
	a.nr_grupo_planej,
	nvl(a.nr_seq_estagio,0),
	a.nr_seq_localizacao,
	a.nr_grupo_trabalho,
	a.cd_estabelecimento,
	a.nr_seq_motivo_cancel
from	man_ordem_servico_exec b,
	man_ordem_servico_v a
where	a.nr_sequencia   = b.nr_seq_ordem
and	trunc(a.dt_ordem_servico, 'month') < dt_referencia_w
and	trunc(a.dt_fim_real, 'month') = dt_referencia_w
and	a.dt_fim_real is not null
group by
	b.nm_usuario_exec,
	a.nr_grupo_planej,
	nvl(a.nr_seq_estagio,0),
	a.nr_grupo_trabalho,
	a.nr_seq_localizacao,
	a.nr_sequencia,
	a.cd_estabelecimento,
	a.nr_seq_motivo_cancel
union all
/*Total de ordens*/
select	'2' ie_informacao,
	count(*),
	b.nm_usuario_exec,
	a.nr_grupo_planej,
	nvl(a.nr_seq_estagio,0),
	a.nr_seq_localizacao,
	a.nr_grupo_trabalho,
	a.cd_estabelecimento,
	a.nr_seq_motivo_cancel
from	man_ordem_servico_exec b,
	man_ordem_servico_v a
where	a.nr_sequencia   = b.nr_seq_ordem
and	trunc(a.dt_ordem_servico, 'month') = dt_referencia_w
group by
	nvl(a.nr_seq_estagio,0),
	b.nm_usuario_exec,
	a.nr_grupo_planej,
	a.nr_grupo_trabalho,
	a.nr_seq_localizacao,
	a.cd_estabelecimento,
	a.nr_seq_motivo_cancel;	
	
begin
dt_referencia_w  := trunc(dt_referencia_p, 'month');

Gravar_Log_Indicador(405, wheb_mensagem_pck.get_texto(305888), sysdate, dt_referencia_w, nm_usuario_p, nr_seq_log_w);

delete	from eis_man_executor
where	dt_referencia = dt_referencia_w;

open c01;
loop
fetch c01 into
	ie_informacao_w,
	qt_ordens_w,
	nm_usuario_exec_w,
	nr_seq_planej_w,
	nr_seq_estagio_w,
	nr_seq_localizador_w,
	nr_grupo_trabalho_w,
	cd_estabelecimento_w,
	nr_seq_motivo_cancel_w;
exit when c01%notfound;
begin

select	count(*)
into	qt_pendente_w
from(	select	distinct
		a.nr_sequencia,
		b.nm_usuario_exec
	from	man_ordem_servico_exec b,
		man_ordem_servico_v a
	where	a.nr_sequencia  = b.nr_seq_ordem
	and	a.dt_fim_real  is null
	and	trunc(a.dt_ordem_servico, 'month') 	= dt_referencia_w
	and	b.nm_usuario_exec 			= nm_usuario_exec_w
	and	nvl(a.nr_grupo_planej,0) 		= nvl(nr_seq_planej_w,0)
	and	nvl(a.nr_seq_estagio,0) 		= nvl(nr_seq_estagio_w,0)
	and	nvl(nr_grupo_trabalho,0)		= nvl(nr_grupo_trabalho_w,0)
	and	ie_informacao_w			= '0'
	and	nvl(a.nr_seq_localizacao,0) 		= nvl(nr_seq_localizador_w,0));

select	count(*)
into	qt_total_os_w
from(	select	distinct
		a.nr_sequencia,
		b.nm_usuario_exec
	from	man_ordem_servico_exec b,
		man_ordem_servico_v a
	where	a.nr_sequencia  = b.nr_seq_ordem
	and	trunc(a.dt_ordem_servico, 'month') 	= dt_referencia_w
	and	b.nm_usuario_exec 			= nm_usuario_exec_w
	and	nvl(a.nr_grupo_planej,0) 		= nvl(nr_seq_planej_w,0)
	and	nvl(a.nr_seq_estagio,0) 		= nvl(nr_seq_estagio_w,0)
	and	nvl(nr_grupo_trabalho,0)		= nvl(nr_grupo_trabalho_w,0)
	and	ie_informacao_w			= '2'
	and	nvl(a.nr_seq_localizacao,0) 		= nvl(nr_seq_localizador_w,0));

select	max(ds_usuario)
into	ds_usuario_exec_w
from	usuario
where	nm_usuario = nm_usuario_exec_w;

select	nvl(max(cd_setor),0)
into	cd_setor_local_w
from	man_localizacao
where	nr_sequencia = nr_seq_localizador_w;

insert	into eis_man_executor(
	nr_sequencia,
	cd_estabelecimento,
	dt_referencia,
	nm_usuario_executor,
	nr_seq_localizador,
	dt_atualizacao,
	nm_usuario,
	nr_seq_estagio,
	qt_ordem_servico,
	qt_pendente,
	qt_encerrada,
	nr_seq_planej,
	ie_informacao,
	nr_grupo_trabalho,
	ds_usuario_exec,
	cd_setor_atendimento,
	nr_seq_motivo_cancel)
values (	eis_man_executor_seq.nextval,
	cd_estabelecimento_w,
	dt_referencia_w,
	nm_usuario_exec_w,
	nr_seq_localizador_w,
	sysdate,
	nm_usuario_p,
	nr_seq_estagio_w,
	qt_total_os_w,
	qt_pendente_w,
	decode(ie_informacao_w,'1',qt_ordens_w, 0),
	nr_seq_planej_w,
	ie_informacao_w,
	nr_grupo_trabalho_w,
	ds_usuario_exec_w,
	cd_setor_local_w,
	nr_seq_motivo_cancel_w);
end;
end loop;
close c01;

Atualizar_Log_Indicador(sysdate, nr_seq_log_w);

commit;
end Gerar_EIS_Man_Executor;
/
