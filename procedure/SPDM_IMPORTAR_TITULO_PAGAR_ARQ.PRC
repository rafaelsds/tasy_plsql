create or replace
procedure SPDM_IMPORTAR_TITULO_PAGAR_ARQ
				(cd_estabelecimento_p		number,
				dt_emissao_p			date,
				dt_vencimento_original_p	date,
				dt_vencimento_atual_p		date,
				vl_titulo_p			number,
				vl_saldo_titulo_p		number,
				cd_tipo_taxa_juro_p		number,
				cd_tipo_taxa_multa_p		number,
				tx_desc_antecipacao_p		number,
				dt_limite_antecipacao_p		date,
				vl_dia_antecipacao_P		number,
				cd_tipo_taxa_antecipacao_p	number,
				ie_origem_titulo_p		varchar,
				ie_tipo_titulo_p		varchar,
				cd_pessoa_fisica_p		varchar,
				cd_cgc_p			varchar,
				nr_titulo_externo_p		varchar,
				tx_juros_p			number,
				tx_multa_p			number,
				cd_moeda_p			number,
				vl_saldo_juros_P		number,
				vl_saldo_multa_p     		number,
				nm_usuario_p			varchar,
				nr_seq_trans_fin_baixa_p	number,
				nr_seq_trans_fin_contab_p	number,
				nr_seq_conta_financ_p		number,
				cd_centro_custo_p		number,
				cd_conta_contabil_p		varchar2,
				nr_seq_trans_fin_p		number,
				ds_observacao_p			varchar2,
				ie_tipo_registro_p		number) is


nr_titulo_w		number(10,0);
sqlerrm_w		varchar2(4000);
nr_titulo_tasy_w	number(15);	
cd_conta_financ_w	number(10);
cd_centro_custo_w	number(10);
nr_seq_classif_w	number(10);

begin
if	(cd_centro_custo_p = 0) then
	cd_centro_custo_w	:= null;
else
	cd_centro_custo_w	:= cd_centro_custo_p;
end if;

if	(cd_cgc_p is not null) and (cd_pessoa_fisica_p is not null) then
	raise_application_error(-20011,'O t�tulo n�o pode ser de uma pessoa f�sica e jur�dica ao mesmo tempo');
end if;

select	max(cd_conta_financ)
into	cd_conta_financ_w
from	conta_financeira
where	cd_sistema_externo = nr_seq_conta_financ_p;

if	(ie_tipo_registro_p = 1) then --Importa��o do T�tulo a Pagar

	select	nvl(max(nr_titulo),0)
	into	nr_titulo_tasy_w
	from	titulo_pagar
	where	somente_numero(nr_titulo_externo) = somente_numero(nr_titulo_externo_p);
	
	select	titulo_pagar_seq.nextval
	into 	nr_titulo_w
	from	dual;

	if	(nr_titulo_tasy_w = 0) then
		begin

		insert	into titulo_pagar
			(nr_titulo,
			cd_estabelecimento,
			dt_emissao,
			dt_vencimento_original,
			dt_vencimento_atual,
			vl_titulo,
			vl_saldo_titulo,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_multa,
			tx_desc_antecipacao,
			dt_limite_antecipacao,
			vl_dia_antecipacao,
			cd_tipo_taxa_antecipacao,
			ie_origem_titulo,
			ie_tipo_titulo,
			cd_pessoa_fisica,
			cd_cgc,
			nr_titulo_externo,
			tx_juros,
			tx_multa,
			cd_moeda,
			vl_saldo_juros,
			vl_saldo_multa,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			dt_contabil,
			ds_observacao_titulo)
		values	(nr_titulo_w,
			cd_estabelecimento_p,
			dt_emissao_p,
			dt_vencimento_original_p,
			dt_vencimento_atual_p,
			vl_titulo_p,
			vl_saldo_titulo_p,
			cd_tipo_taxa_juro_p,
			cd_tipo_taxa_multa_p,
			tx_desc_antecipacao_p,
			dt_limite_antecipacao_p,
			vl_dia_antecipacao_P,
			decode(cd_tipo_taxa_antecipacao_p, 0, null, cd_tipo_taxa_antecipacao_p),
			ie_origem_titulo_p,
			ie_tipo_titulo_p,
			cd_pessoa_fisica_p,
			cd_cgc_p,
			nr_titulo_externo_p,
			tx_juros_p,
			tx_multa_p,
			cd_moeda_p,
			vl_saldo_juros_P,
			vl_saldo_multa_p,
			sysdate,
			nm_usuario_p,
			'A',
			dt_emissao_p,
			ds_observacao_p);
		ATUALIZAR_INCLUSAO_TIT_PAGAR(nr_titulo_w, nm_usuario_p);

		/*
		insert	into titulo_pagar_classif
			(nr_titulo,
			nr_sequencia,
			nr_seq_conta_financ,
			cd_centro_custo,
			vl_titulo,
			dt_atualizacao,
			nm_usuario,
			cd_conta_contabil,
			nr_seq_trans_fin)
		values	(nr_titulo_w,
			1,
			cd_conta_financ_w,
			cd_centro_custo_w,
			vl_titulo_p,
			sysdate,
			nm_usuario_p,
			cd_conta_contabil_p,
			nr_seq_trans_fin_p);*/

		--gerar_tributo_titulo(nr_titulo_w, nm_usuario_p, 'N', null, null, null, null, null, cd_estabelecimento_p, null);

		exception
		when others then
			sqlerrm_w	:= sqlerrm;
			gravar_log_tasy(55888, 'Erro ao importar t�tulo!'||chr(13)||
						'Dados Importados: '||chr(13)||
						'  cd_estabelecimento_p: '||cd_estabelecimento_p||chr(13)||
						'  dt_emissao_p: '||dt_emissao_p||chr(13)||
						'  dt_vencimento_original_p: '||dt_vencimento_original_p||chr(13)||
						'  dt_vencimento_atual_p: '||dt_vencimento_atual_p||chr(13)||
						'  vl_titulo_p: '||vl_titulo_p||chr(13)||
						'  vl_saldo_titulo_p: '||vl_saldo_titulo_p||chr(13)||
						'  cd_tipo_taxa_juro_p: '||cd_tipo_taxa_juro_p||chr(13)||
						'  cd_tipo_taxa_multa_p: '||cd_tipo_taxa_multa_p||chr(13)||
						'  tx_desc_antecipacao_p: '||tx_desc_antecipacao_p||chr(13)||
						'  dt_limite_antecipacao_p: '||dt_limite_antecipacao_p||chr(13)||
						'  vl_dia_antecipacao_P: '||vl_dia_antecipacao_P||chr(13)||
						'  cd_tipo_taxa_antecipacao_p: '||cd_tipo_taxa_antecipacao_p||chr(13)||
						'  ie_origem_titulo_p: '||ie_origem_titulo_p||chr(13)||
						'  ie_tipo_titulo_p: '||ie_tipo_titulo_p||chr(13)||
						'  cd_pessoa_fisica_p: '||cd_pessoa_fisica_p||chr(13)||
						'  cd_cgc_p: '||cd_cgc_p||chr(13)||
						'  nr_titulo_externo_p: '||nr_titulo_externo_p||chr(13)||
						'  tx_juros_p: '||tx_juros_p||chr(13)||
						'  tx_multa_p: '||tx_multa_p||chr(13)||
						'  cd_moeda_p: '||cd_moeda_p||chr(13)||
						'  vl_saldo_juros_P: '||vl_saldo_juros_P||chr(13)||
						'  vl_saldo_multa_p: '||vl_saldo_multa_p||chr(13)||
						'  nm_usuario_p: '||nm_usuario_p||chr(13)||
						'  nr_seq_trans_fin_baixa_p: '||nr_seq_trans_fin_baixa_p||chr(13)||
						'  nr_seq_trans_fin_contab_p: '||nr_seq_trans_fin_contab_p||chr(13)||
						'  nr_seq_conta_financ_p: '||nr_seq_conta_financ_p||chr(13)||
						'  nr_seq_conta_financ_tasy_p: '||cd_conta_financ_w||chr(13)||
						'  cd_centro_custo_w: '||cd_centro_custo_w||chr(13)||
						'  cd_conta_contabil_p: '||cd_conta_contabil_p||chr(13)||
						'  nr_seq_trans_fin_p: '||nr_seq_trans_fin_p||chr(13)||
						'  ds_observacao_p: '||ds_observacao_p||chr(13)||
						'  ie_tipo_registro_p: '||ie_tipo_registro_p||chr(13)||
						sqlerrm_w, nm_usuario_p);
		end;
	else
		gravar_log_tasy(55888,
				'T�tulo j� importato!'||chr(13)||
				' nr_titulo_externo_p: '||nr_titulo_externo_p||chr(13)||
				' nr_titulo_tasy_w: '||nr_titulo_tasy_w,
				nm_usuario_p);	

	end if;
	
elsif (ie_tipo_registro_p = 2) then --Classifica��o do t�tulo a pagar

	select	nvl(max(nr_titulo),0)
	into	nr_titulo_tasy_w
	from	titulo_pagar
	where	somente_numero(nr_titulo_externo) = somente_numero(nr_titulo_externo_p);
	
	if	(nr_titulo_tasy_w > 0) then
		
		select	count(*) + 1
		into	nr_seq_classif_w
		from	titulo_pagar_classif
		where	nr_titulo	= nr_titulo_tasy_w;
		
		begin
		
		insert	into titulo_pagar_classif
			(nr_titulo,
			nr_sequencia,
			nr_seq_conta_financ,
			cd_centro_custo,
			vl_titulo,
			dt_atualizacao,
			nm_usuario,
			cd_conta_contabil,
			nr_seq_trans_fin)
		values	(nr_titulo_tasy_w,
			nr_seq_classif_w,
			cd_conta_financ_w,
			cd_centro_custo_w,
			vl_titulo_p,
			sysdate,
			nm_usuario_p,
			cd_conta_contabil_p,
			nr_seq_trans_fin_p);
		exception
		when others then
			sqlerrm_w	:= sqlerrm;
			gravar_log_tasy(55888, 'Erro ao importar classifica��o do t�tulo!'||chr(13)||
						'  nr_titulo_tasy_w: '||nr_titulo_tasy_w||chr(13)||
						'  ie_tipo_registro_p:'||ie_tipo_registro_p||chr(13)||
						'  nr_titulo_externo_p: '||nr_titulo_externo_p||chr(13)||
						'  nr_seq_conta_financ_p: '||nr_seq_conta_financ_p||chr(13)||
						'  nr_seq_conta_financ_tasy_p: '||cd_conta_financ_w||chr(13)||
						'  cd_centro_custo_w: '||cd_centro_custo_w||chr(13)||
						'  cd_conta_contabil_p: '||cd_conta_contabil_p||chr(13)||
						'  nr_seq_trans_fin_p: '||nr_seq_trans_fin_p||chr(13)||
						sqlerrm_w,
						nm_usuario_p);
		end;
	
	else
		gravar_log_tasy(55888,
				'T�tulo n�o localizado!'||chr(13)||
				'  ie_tipo_registro_p:'||ie_tipo_registro_p||chr(13)||
				'  nr_titulo_externo_p: '||nr_titulo_externo_p,
				nm_usuario_p);
	end if;

end if;

commit;

end SPDM_IMPORTAR_TITULO_PAGAR_ARQ;
/