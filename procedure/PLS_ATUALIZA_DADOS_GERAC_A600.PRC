create or replace
procedure pls_atualiza_dados_gerac_A600(nr_seq_fatura_p		ptu_camara_compensacao.nr_sequencia%type,
					cd_unimed_origem_p	ptu_camara_compensacao.cd_unimed_origem%type,
					nr_seq_arquivo_p	ptu_camara_compensacao.nr_seq_arquivo%type,
					nm_usuario_p		varchar2) is
					
nr_seq_arquivo_w	ptu_camara_compensacao.nr_seq_arquivo%type;

begin

-- Se n�o veio nr_seq_arquivo_p � necess�rio buscar o ponta a realizar a incrementa��o, sen�o, se vir valor no campo nr_seq_arquivo_p, � porque se trata de uma re-gera��o de arquivo, portanto utiliza-r� a mesma nr_seq_arquivo_p
if	(nr_seq_arquivo_p is null) then
	select	nvl(max(nr_seq_arquivo),0) + 1
	into	nr_seq_arquivo_w
	from	ptu_camara_compensacao
	where	trunc(dt_geracao_arquivo) = trunc(sysdate)
	and	cd_unimed_origem = cd_unimed_origem_p
	and	nr_sequencia <> nr_seq_fatura_p;
end if;

-- Se o campo nr_seq_arquivo_w est� nulo utiliza o nr_seq_arquivo_p vindo por par�metro visto que se trata de uma re-gera��o de arquivo
nr_seq_arquivo_w := nvl(nr_seq_arquivo_w, nr_seq_arquivo_p);

update	ptu_camara_compensacao
set	dt_geracao_arquivo = sysdate,
	nr_seq_arquivo = nr_seq_arquivo_w,
	nm_usuario_arq = nm_usuario_p,
	ds_arquivo = 'C' || to_char(sysdate,'ddmmyy') || nr_seq_arquivo_w || '.' || substr(lpad(somente_numero(cd_unimed_origem),3,'0'),1,3)
where	nr_sequencia = nr_seq_fatura_p;

commit;

end pls_atualiza_dados_gerac_A600;
/