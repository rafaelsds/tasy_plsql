CREATE OR REPLACE
PROCEDURE HSR_GERAR_W_CONV_RET_MOVTO(	nr_seq_retorno_p	number) is

/* Conv�nio */
cd_convenio_w		number(5);
cd_estabelecimento_w	number(4);
ie_importar_w		varchar2(2);

/* Conta */
nr_seq_movto_conta_w	number(10);
nr_seq_prox_conta_w	number(10);
nr_doc_convenio_w	number(15);
cd_prestador_w		number(14);
ie_tipo_guia_w		varchar2(2);

/* Item Cobrado */
dt_execucao_w		date;
hr_execucao_w		number(4);
tp_item_w		number(1);
cd_item_w		number(15);
qt_cobrada_w		number(9,3);
qt_paga_w		number(9,3);
vl_cobrado_w		number(17,4);
vl_pago_w		number(17,4);
qt_filme_w		number(7,5);
vl_filme_w		number(17,4);
dt_mesano_pagamento_w	date;

/* Conta */
cursor	c01 is
select	a.nr_sequencia,
	somente_numero(substr(a.ds_conteudo,124,10)) nr_doc_convenio,
	-- somente_numero(substr(a.ds_conteudo,31,11)) cd_prestador,
	substr(a.ds_conteudo,117,1) ie_tipo_guia
from	w_conv_ret_movto a
where	substr(a.ds_conteudo,1,1)	= '1'
and	a.nr_seq_retorno	= nr_seq_retorno_p;

/* Item Cobrado */
cursor	c02 is
select	somente_numero(substr(a.ds_conteudo,19,11)) cd_prestador,
	to_date(substr(a.ds_conteudo,30,8),'dd/mm/yyyy') dt_execucao,
	somente_numero(substr(a.ds_conteudo,38,4)) hr_execucao,
	somente_numero(substr(a.ds_conteudo,44,1)) tp_item,
	somente_numero(substr(a.ds_conteudo,45,8)) cd_item,
	somente_numero(substr(a.ds_conteudo,53,8)) / 1000 qt_cobrada,
	somente_numero(substr(a.ds_conteudo,61,8)) / 1000 qt_paga,
	somente_numero(substr(a.ds_conteudo,69,14)) / 100 vl_cobrado,
	somente_numero(substr(a.ds_conteudo,83,14)) / 100 vl_pago,
	somente_numero(substr(a.ds_conteudo,97,7)) / 100000 qt_filme,
	somente_numero(substr(a.ds_conteudo,104,14)) / 100 vl_filme,
	to_date(substr(a.ds_conteudo,129,6),'mm/yyyy') dt_mesano_pagamento
from	w_conv_ret_movto a
where	(nvl(nr_seq_prox_conta_w,0) = 0 or a.nr_sequencia <
 nr_seq_prox_conta_w)
and	a.nr_sequencia		> nr_seq_movto_conta_w
and	substr(a.ds_conteudo,1,1)	= '3'
and	a.nr_seq_retorno	= nr_seq_retorno_p;

begin

select	max(a.cd_convenio),
	max(a.cd_estabelecimento)
into	cd_convenio_w,
	cd_estabelecimento_w
from	convenio_retorno a
where	a.nr_sequencia	= nr_seq_retorno_p;

open	c01;
loop
fetch	c01 into
	nr_seq_movto_conta_w,
	nr_doc_convenio_w,
	-- cd_prestador_w,
	ie_tipo_guia_w;
exit	when c01%notfound;

	select	nvl(min(a.nr_sequencia),0)
	into	nr_seq_prox_conta_w
	from	w_conv_ret_movto a
	where	substr(a.ds_conteudo,1,1)	= '1'
	and	a.nr_sequencia		> nr_seq_movto_conta_w
	and	a.nr_seq_retorno	= nr_seq_retorno_p;

	if	(ie_tipo_guia_w	= 'E') then
		ie_tipo_guia_w	:= 'A';
	end if;

	open	c02;
	loop
	fetch	c02 into
		cd_prestador_w,
		dt_execucao_w,
		hr_execucao_w,
		tp_item_w,
		cd_item_w,
		qt_cobrada_w,
		qt_paga_w,
		vl_cobrado_w,
		vl_pago_w,
		qt_filme_w,
		vl_filme_w,
		dt_mesano_pagamento_w;
	exit	when c02%notfound;

		obter_regras_imp_guia(cd_convenio_w,cd_estabelecimento_w,cd_prestador_w,ie_importar_w);

		insert	into convenio_retorno_movto
			(cd_item,
			cd_prestador,
			dt_atualizacao,
			dt_execucao,
			dt_mesano_pagamento,
			hr_execucao,
			nm_usuario,
			nr_doc_convenio,
			nr_seq_retorno,
			nr_sequencia,
			qt_cobrada,
			qt_filme,
			qt_paga,
			tp_item,
			ie_gera_resumo,
			ie_tipo_guia,
			vl_cobrado,
			vl_filme,
			vl_pago,
			vl_total_pago)
		values	(cd_item_w,
			cd_prestador_w,
			sysdate,
			dt_execucao_w,
			dt_mesano_pagamento_w,
			hr_execucao_w,
			'Tasy',
			nr_doc_convenio_w,
			nr_seq_retorno_p,
			convenio_retorno_movto_seq.nextval,
			qt_cobrada_w,
			qt_filme_w,
			qt_paga_w,
			tp_item_w,
			ie_importar_w,
			ie_tipo_guia_w,
			vl_cobrado_w,
			vl_filme_w,
			vl_pago_w,
			vl_pago_w);

	end	loop;
	close	c02;

end	loop;
close	c01;

delete	from w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end HSR_GERAR_W_CONV_RET_MOVTO;
/