create or replace
procedure GRAVA_AJUSTE_PRECO_SERV_estrut
		(CD_AREA_PROCEDIMENTO_p		number,
		CD_ESPECIALIDADE_p		number,
		CD_GRUPO_PROC_p			number,
		CD_ESTABELECIMENTO_P		number,   
		CD_TAB_ORIGEM_P        		number,
		CD_TAB_DESTINO_P       		number,
		DT_VIGENCIA_ORIGEM_P   		date,
		DT_VIGENCIA_DESTINO_P		date,
		IE_INDICE_P            		number,
		IE_ULTIMA_VIGENCIA_P		VARCHAR2,
		NM_USUARIO_P           		varchar2) is

cd_procedimento_w	number(15,0);

Cursor C01 is
	select	cd_procedimento
	from	estrutura_procedimento_v
	where	(cd_grupo_proc		= nvl(cd_grupo_proc_p, cd_grupo_proc))
	and	(cd_especialidade	= nvl(cd_especialidade_p, cd_especialidade))
	and	(CD_AREA_PROCEDIMENTO	= nvl(CD_AREA_PROCEDIMENTO_p, CD_AREA_PROCEDIMENTO));

begin

open	c01;
loop
fetch	c01 into cd_procedimento_w;
exit when c01%notfound;
	begin

	GRAVA_AJUSTE_PRECO_SERVICO
		(CD_ESTABELECIMENTO_P,
		CD_TAB_ORIGEM_P,
		CD_TAB_DESTINO_P,
		CD_PROCEDIMENTO_w,
		DT_VIGENCIA_ORIGEM_P,
		DT_VIGENCIA_DESTINO_P,
		IE_INDICE_P,
		NM_USUARIO_P,
		nvl(IE_ULTIMA_VIGENCIA_P,'N'),'N',0);


	end;
end loop;
close c01;

end GRAVA_AJUSTE_PRECO_SERV_estrut;
/