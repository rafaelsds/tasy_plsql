create or replace
procedure ALTERAR_LIB_TITULO_PAGAR(
		nr_titulo_p		number,
		nm_usuario_p		varchar2) is 

begin
if	(nr_titulo_p is not null) and
	(nm_usuario_p is not null) then

	update	titulo_pagar
	set	dt_liberacao   = sysdate,
		dt_atualizacao = sysdate,
		nm_usuario     = nm_usuario_p,
		nm_usuario_lib = nm_usuario_p
	where	nr_titulo      = nr_titulo_p;
	
end if;
commit;
end ALTERAR_LIB_TITULO_PAGAR;
/
