Create or replace
procedure pls_aplicar_reajuste
			(	nr_seq_reajuste_p	number,
				ie_reajustar_regra_p	varchar2,
				nr_seq_contrato_p	number,
				ie_commit_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

nr_seq_tabela_reaj_w		number(10);
nr_sequencia_w			number(10);
nr_seq_preco_w			number(10);
vl_reajustado_w			number(15,2);
nr_seq_tabela_w			number(10);
vl_preco_nao_sibsid_reaj_w	number(15,2);
vl_minimo_w			number(15,2);
nr_seq_regra_inscricao_w	number(10);
tx_reajuste_w			number(15,5);
ie_reajustar_inscricao_w	Varchar2(1);
vl_inscricao_w			number(15,2);
tx_inscricao_w			number(7,4);
qt_parcela_inicial_w		number(5);
qt_parcela_final_w		number(5);
ie_acao_contrato_w		varchar2(2);
ie_reajustar_vl_manutencao_w	varchar2(2);
vl_manutencao_w			number(15,2);
ie_cobranca_w			varchar2(2);
nr_seq_regra_pos_estab_w	number(10);
qt_regra_inscricao_w		number(10);
tx_administracao_w		number(7,4);
ie_autorizacao_previa_w		varchar2(1);
qt_regra_pos_estab_w		number(10);
ie_coparticipacao_w		varchar2(1);
ie_grau_dependencia_w		varchar2(2);
tx_reajuste_inscricao_w		number(7,4);
ie_reajustar_via_adic_w		varchar2(1);
nr_seq_lote_reaj_inscricao_w	number(10);
nr_seq_lote_reaj_copartic_w	number(10);
dt_reajuste_w			date;
nr_seq_intercambio_w		number(10);
vl_adaptacao_w			number(15,2);
ie_existe_tabela_w		varchar2(1);
ie_existe_copartic_w		varchar2(1);
ie_existe_inscricao_w		varchar2(1);
nr_seq_contrato_w		number(10);
nr_contrato_w			number(10);
nr_seq_reajuste_desfazer_w	pls_reajuste.nr_seq_reajuste_desfazer%type;
nr_contrato_ww			pls_contrato.nr_contrato%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
qt_cobr_retro_mens_w		number(10);
nr_seq_contrato_ww		pls_contrato.nr_sequencia%type;
ie_tipo_lote_w			pls_reajuste.ie_tipo_lote%type;
nr_seq_lote_deflator_w		pls_reajuste.nr_seq_lote_deflator%type;

Cursor c01 is
	select	nr_sequencia,
		nr_seq_tabela
	from	pls_reajuste_tabela
	where	nr_seq_reajuste	= nr_seq_reajuste_p
	and	dt_liberacao is null;

Cursor c02 is
	select	nr_sequencia,
		nr_seq_preco,
		vl_reajustado,
		vl_preco_nao_subsidiado,
		vl_minimo,
		vl_adaptacao
	from	pls_reajuste_preco
	where	nr_seq_tabela	= nr_seq_tabela_reaj_w
	and	dt_liberacao is null;

Cursor C03 is
	select	a.nr_sequencia
	from	pls_lote_reaj_copartic	a,
		pls_reajuste		b
	where	a.nr_seq_reajuste	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_reajuste_p;

Cursor C04 is
	select	a.nr_sequencia
	from	pls_lote_reaj_inscricao	a,
		pls_reajuste		b
	where	a.nr_seq_reajuste	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_reajuste_p;
	
Cursor C05 is
	select	nr_sequencia,
		((nvl(tx_reajuste_w,0)/100) * nvl(vl_informado,0)) + nvl(vl_informado,0) vl_manutencao,
		ie_cobranca,
		tx_administracao,
		ie_autorizacao_previa,
		nr_seq_grupo_servico,
		nr_seq_grupo_material,
		ie_repassa_medico
	from	pls_regra_pos_estabelecido
	where	nr_seq_contrato	= nr_seq_contrato_p
	and	dt_reajuste_w	between nvl(dt_vigencia_inicio,dt_reajuste_w) and nvl(dt_vigencia_fim,dt_reajuste_w)
	union
	select	nr_sequencia,
		((nvl(tx_reajuste_w,0)/100) * nvl(vl_informado,0)) + nvl(vl_informado,0) vl_manutencao,
		ie_cobranca,
		tx_administracao,
		ie_autorizacao_previa,
		nr_seq_grupo_servico,
		nr_seq_grupo_material,
		ie_repassa_medico
	from	pls_regra_pos_estabelecido
	where	nr_seq_intercambio	= nr_seq_intercambio_w
	and	dt_reajuste_w	between nvl(dt_vigencia_inicio,dt_reajuste_w) and nvl(dt_vigencia_fim,dt_reajuste_w);	

TYPE 		fetch_array IS TABLE OF C01%ROWTYPE;
s_array 	fetch_array;
i		Integer := 1;
type Vetor is table of fetch_array index by binary_integer;
Vetor_C01_w			Vetor;

begin
ie_existe_tabela_w		:= 'N';
ie_existe_copartic_w		:= 'N';
ie_existe_inscricao_w		:= 'N';

select	max(nr_seq_contrato)
into	nr_seq_contrato_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_p;

select	max(nr_contrato)
into	nr_contrato_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_w;

open C01;
loop
fetch C01 bulk collect into s_array limit 1000;
	Vetor_C01_w(i) := s_array;
	i := i + 1;
exit when C01%NOTFOUND;
end loop;
close C01;

for i in 1..Vetor_C01_w.count loop
	s_array := Vetor_C01_w(i);
	for z in 1..s_array.count loop
		begin
		nr_seq_tabela_reaj_w		:= s_array(z).nr_sequencia;
		nr_seq_tabela_w			:= s_array(z).nr_seq_tabela;
		
		ie_existe_tabela_w	:= 'S';
		
		open c02;
		loop
		fetch c02 into
			nr_sequencia_w,
			nr_seq_preco_w,
			vl_reajustado_w,
			vl_preco_nao_sibsid_reaj_w,
			vl_minimo_w,
			vl_adaptacao_w;
		exit when c02%notfound;
			update	pls_reajuste_preco
			set	dt_liberacao	= sysdate,
				nm_usuario_lib	= nm_usuario_p
			where	nr_sequencia	= nr_sequencia_w;
			
			update	pls_plano_preco
			set	vl_preco_atual			= vl_reajustado_w,
				vl_preco_nao_subsid_atual	= vl_preco_nao_sibsid_reaj_w,
				vl_minimo			= vl_minimo_w,
				vl_adaptacao			= vl_adaptacao_w,
				dt_atualizacao			= sysdate,
				nm_usuario			= nm_usuario_p
			where	nr_sequencia			= nr_seq_preco_w;
		end loop;
		close c02;
		
		update	pls_reajuste_tabela
		set	dt_liberacao	= sysdate,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_tabela_reaj_w;
		
		update	pls_segurado
		set	ie_tipo_valor	= 'I'
		where	nr_seq_tabela	= nr_seq_tabela_w;
		end;
	end loop;
end loop;

select	tx_reajuste,
	ie_reajustar_inscricao,
	ie_reajustar_vl_manutencao,
	nvl(tx_reajuste_inscricao,tx_reajuste),
	ie_reajustar_copartic,
	ie_reajustar_via_adic,
	dt_reajuste,
	nr_seq_intercambio,
	nr_seq_reajuste_desfazer,
	nr_contrato,
	nr_seq_segurado,
	ie_tipo_lote,
	nr_seq_lote_deflator
into	tx_reajuste_w,
	ie_reajustar_inscricao_w,
	ie_reajustar_vl_manutencao_w,
	tx_reajuste_inscricao_w,
	ie_coparticipacao_w,
	ie_reajustar_via_adic_w,
	dt_reajuste_w,
	nr_seq_intercambio_w,
	nr_seq_reajuste_desfazer_w,
	nr_contrato_ww,
	nr_seq_segurado_w,
	ie_tipo_lote_w,
	nr_seq_lote_deflator_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_p;

if	(ie_coparticipacao_w = 'S') then	

	open C03;
	loop
	fetch C03 into
		nr_seq_lote_reaj_copartic_w;
	exit when C03%notfound;
		begin		
		ie_existe_copartic_w	:= 'S';
		pls_aplicar_reaj_lote_copartic(nr_seq_lote_reaj_copartic_w, dt_reajuste_w, 'L', 'N', cd_estabelecimento_p, nm_usuario_p);
		pls_aplicar_reaj_lote_copartic(nr_seq_lote_reaj_copartic_w, dt_reajuste_w, 'A', 'N', cd_estabelecimento_p, nm_usuario_p);
		end;
	end loop;
	close C03;
end if;

if	(ie_reajustar_inscricao_w = 'S') then
	open C04;
	loop
	fetch C04 into
		nr_seq_lote_reaj_inscricao_w;
	exit when C04%notfound;
		begin
		ie_existe_inscricao_w	:= 'S';
		pls_aplicar_reaj_inscricao(nr_seq_lote_reaj_inscricao_w, 'L', dt_reajuste_w, cd_estabelecimento_p, nm_usuario_p);
		pls_aplicar_reaj_inscricao(nr_seq_lote_reaj_inscricao_w, 'A', dt_reajuste_w, cd_estabelecimento_p, nm_usuario_p);
		end;
	end loop;
	close C04;
end if;

if	(ie_existe_tabela_w ='N') and
	(ie_existe_copartic_w = 'N') and
	(ie_existe_inscricao_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort( 238968, 'NR_CONTRATO='||nr_contrato_w );
end if;

if	(ie_reajustar_vl_manutencao_w = 'S') then
	for r_c05_w in c05 loop

	update	pls_regra_pos_estabelecido
	set	dt_vigencia_fim	= trunc(dt_reajuste_w - 1,'dd')
	where	nr_sequencia	= r_c05_w.nr_sequencia;
	
	insert	into	pls_regra_pos_estabelecido
		(	nr_sequencia, tx_administracao, vl_informado,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
			nm_usuario_nrec, dt_vigencia_inicio, nr_seq_contrato,
			ie_cobranca, ie_autorizacao_previa, nr_seq_intercambio,
			nr_seq_grupo_servico, nr_seq_grupo_material, ie_repassa_medico)
		values
		(	pls_regra_pos_estabelecido_seq.nextval, r_c05_w.tx_administracao, r_c05_w.vl_manutencao,
			sysdate, nm_usuario_p, sysdate,
			nm_usuario_p, dt_reajuste_w, nr_seq_contrato_p,
			r_c05_w.ie_cobranca, r_c05_w.ie_autorizacao_previa, nr_seq_intercambio_w,
			r_c05_w.nr_seq_grupo_servico, r_c05_w.nr_seq_grupo_material, r_c05_w.ie_repassa_medico);
	end loop;
end if;

if	(ie_reajustar_regra_p = 'S') then
	pls_reajustar_regra_mens(nr_seq_reajuste_p, nm_usuario_p, cd_estabelecimento_p);
end if;

if	(ie_reajustar_via_adic_w	= 'S') then
	pls_reajustar_via_carteira(nr_seq_reajuste_p,nr_seq_contrato_p,cd_estabelecimento_p,nm_usuario_p);
end if;	

pls_reaj_regra_apropriacao(nr_seq_reajuste_p,nm_usuario_p,'N');

if	(ie_tipo_lote_w = 'A') then
	--Gerar lanšamento de cobranša retroativa.
	pls_reaj_cobranca_retro_pck.gerar_lancamentos(nr_seq_reajuste_p, null, cd_estabelecimento_p, nm_usuario_p);
else
	--Inativar o lanšamento de cobranša retroativa
	if	(nr_seq_segurado_w is not null) then
		select	count(1)
		into	qt_cobr_retro_mens_w
		from	pls_segurado_mensalidade a,
			pls_reajuste_cobr_retro b,
			pls_reajuste c
		where	b.nr_sequencia = a.nr_seq_reaj_retro
		and	c.nr_sequencia = b.nr_seq_reajuste
		and	c.dt_reajuste = dt_reajuste_w
		and	((((c.nr_sequencia = nr_seq_reajuste_desfazer_w) or (nr_seq_reajuste_desfazer_w is null)) and (c.nr_seq_lote_deflator is null))
		 or	((c.nr_seq_lote_deflator = nr_seq_reajuste_p) and (c.nr_seq_lote_deflator is not null)))
		and	a.nr_seq_segurado = nr_seq_segurado_w
		and	a.nr_seq_item_mensalidade is not null;
		
		if	(qt_cobr_retro_mens_w = 0) then
			update	pls_segurado_mensalidade
			set	ie_situacao = 'I',
				ds_observacao = ds_observacao || ' - Registro inativado.',
				nm_usuario_nrec = nm_usuario_p,
				dt_atualizacao_nrec = sysdate
			where	nr_sequencia in (select	a.nr_sequencia
						from	pls_segurado_mensalidade a,
							pls_reajuste_cobr_retro b,
							pls_reajuste c
						where	b.nr_sequencia = a.nr_seq_reaj_retro
						and	c.nr_sequencia = b.nr_seq_reajuste
						and	c.dt_reajuste = dt_reajuste_w
						and	((((c.nr_sequencia = nr_seq_reajuste_desfazer_w) or (nr_seq_reajuste_desfazer_w is null)) and (c.nr_seq_lote_deflator is null))
						 or	((c.nr_seq_lote_deflator = nr_seq_reajuste_p) and (c.nr_seq_lote_deflator is not null)))
						and	a.nr_seq_segurado = nr_seq_segurado_w);
		end if;
	elsif	(nr_contrato_ww is not null) then
		select	max(nr_sequencia)
		into	nr_seq_contrato_ww
		from	pls_contrato
		where	nr_contrato = nr_contrato_ww;

		select	count(1)
		into	qt_cobr_retro_mens_w
		from	pls_segurado_mensalidade a,
			pls_reajuste_cobr_retro b,
			pls_reajuste c
		where	b.nr_sequencia = a.nr_seq_reaj_retro
		and	c.nr_sequencia = b.nr_seq_reajuste
		and	c.dt_reajuste = dt_reajuste_w
		and	((((c.nr_sequencia = nr_seq_reajuste_desfazer_w) or (nr_seq_reajuste_desfazer_w is null)) and (c.nr_seq_lote_deflator is null))
		 or	((c.nr_seq_lote_deflator = nr_seq_reajuste_p) and (c.nr_seq_lote_deflator is not null)))
		and	a.nr_seq_item_mensalidade is not null
		and	exists 	(select	1
				from	pls_segurado x
				where	x.nr_sequencia = a.nr_seq_segurado
				and	x.nr_seq_contrato = nr_seq_contrato_ww);
			
		if	(qt_cobr_retro_mens_w = 0) then
			update	pls_segurado_mensalidade
			set	ie_situacao = 'I',
				ds_observacao = ds_observacao || ' - Registro inativado.',
				nm_usuario_nrec = nm_usuario_p,
				dt_atualizacao_nrec = sysdate
			where	nr_sequencia in (select	a.nr_sequencia
						from	pls_segurado_mensalidade a,
							pls_reajuste_cobr_retro b,
							pls_reajuste c
						where	b.nr_sequencia = a.nr_seq_reaj_retro
						and	c.nr_sequencia = b.nr_seq_reajuste
						and	c.dt_reajuste = dt_reajuste_w
						and	((((c.nr_sequencia = nr_seq_reajuste_desfazer_w) or (nr_seq_reajuste_desfazer_w is null)) and (c.nr_seq_lote_deflator is null))
						 or	((c.nr_seq_lote_deflator = nr_seq_reajuste_p) and (c.nr_seq_lote_deflator is not null)))
						and	exists 	(select	1
								from	pls_segurado x
								where	x.nr_sequencia = a.nr_seq_segurado
								and	x.nr_seq_contrato = nr_seq_contrato_ww));
		end if;
	elsif	(nr_seq_intercambio_w is not null) then
		select	count(1)
		into	qt_cobr_retro_mens_w
		from	pls_segurado_mensalidade a,
			pls_reajuste_cobr_retro b,
			pls_reajuste c
		where	b.nr_sequencia = a.nr_seq_reaj_retro
		and	c.nr_sequencia = b.nr_seq_reajuste
		and	c.dt_reajuste = dt_reajuste_w
		and	((((c.nr_sequencia = nr_seq_reajuste_desfazer_w) or (nr_seq_reajuste_desfazer_w is null)) and (c.nr_seq_lote_deflator is null))
		 or	((c.nr_seq_lote_deflator = nr_seq_reajuste_p) and (c.nr_seq_lote_deflator is not null)))
		and	a.nr_seq_item_mensalidade is not null
		and	exists 	(select	1
				from	pls_segurado x
				where	x.nr_sequencia = a.nr_seq_segurado
				and	x.nr_seq_intercambio = nr_seq_intercambio_w);
				
		if	(qt_cobr_retro_mens_w = 0) then
			update	pls_segurado_mensalidade
			set	ie_situacao = 'I',
				ds_observacao = ds_observacao || ' - Registro inativado.',
				nm_usuario_nrec = nm_usuario_p,
				dt_atualizacao_nrec = sysdate
			where	nr_sequencia in (select	a.nr_sequencia
						from	pls_segurado_mensalidade a,
							pls_reajuste_cobr_retro b,
							pls_reajuste c
						where	b.nr_sequencia = a.nr_seq_reaj_retro
						and	c.nr_sequencia = b.nr_seq_reajuste
						and	c.dt_reajuste = dt_reajuste_w
						and	((((c.nr_sequencia = nr_seq_reajuste_desfazer_w) or (nr_seq_reajuste_desfazer_w is null)) and (c.nr_seq_lote_deflator is null))
						 or	((c.nr_seq_lote_deflator = nr_seq_reajuste_p) and (c.nr_seq_lote_deflator is not null)))
						and	exists 	(select	1
								from	pls_segurado x
								where	x.nr_sequencia = a.nr_seq_segurado
								and	x.nr_seq_intercambio = nr_seq_intercambio_w));
		end if;
	else
		select	count(1)
		into	qt_cobr_retro_mens_w
		from	pls_segurado_mensalidade a,
			pls_reajuste_cobr_retro b,
			pls_reajuste c
		where	b.nr_sequencia = a.nr_seq_reaj_retro
		and	c.nr_sequencia = b.nr_seq_reajuste
		and	c.dt_reajuste = dt_reajuste_w
		and	((((c.nr_sequencia = nr_seq_reajuste_desfazer_w) or (nr_seq_reajuste_desfazer_w is null)) and (c.nr_seq_lote_deflator is null))
		 or	((c.nr_seq_lote_deflator = nr_seq_reajuste_p) and (c.nr_seq_lote_deflator is not null)))
		and	a.nr_seq_item_mensalidade is not null;

		if	(qt_cobr_retro_mens_w = 0) then
			update	pls_segurado_mensalidade
			set	ie_situacao = 'I',
				ds_observacao = ds_observacao || ' - Registro inativado.',
				nm_usuario_nrec = nm_usuario_p,
				dt_atualizacao_nrec = sysdate
			where	nr_sequencia in (select	a.nr_sequencia
						from	pls_segurado_mensalidade a,
							pls_reajuste_cobr_retro b,
							pls_reajuste c
						where	b.nr_sequencia = a.nr_seq_reaj_retro
						and	c.nr_sequencia = b.nr_seq_reajuste
						and	c.dt_reajuste = dt_reajuste_w
						and	((((c.nr_sequencia = nr_seq_reajuste_desfazer_w) or (nr_seq_reajuste_desfazer_w is null)) and (c.nr_seq_lote_deflator is null))
						 or	((c.nr_seq_lote_deflator = nr_seq_reajuste_p) and (c.nr_seq_lote_deflator is not null))));
		end if;
	end if;

	if	(qt_cobr_retro_mens_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(812078);
	end if;
end if;

update	pls_reajuste
set	ie_status	= '2',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_reajuste_p;

if	(nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;

end pls_aplicar_reajuste;
/
