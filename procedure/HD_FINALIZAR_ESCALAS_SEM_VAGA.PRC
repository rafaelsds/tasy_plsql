create or replace
procedure HD_finalizar_escalas_sem_vaga (	cd_pessoa_fisica_p	varchar2,
						cd_estabelecimento_p	number,
						nm_usuario_p		varchar2) is 
					
ds_escalas_w		varchar2(2000);
ds_codigo_temp_w	varchar2(2000);
ds_codigo_aux_w		varchar2(2);

k			number(4);
i			number(4);

begin
if (cd_pessoa_fisica_p is not null) then

	ds_escalas_w := HD_obter_se_existe_vaga(cd_pessoa_fisica_p, cd_estabelecimento_p);
	
	if (ds_escalas_w is not null) then
	
		k := 1;
		for i in k..length(ds_escalas_w) loop
			begin

			ds_codigo_aux_w	:= substr(ds_escalas_w,i,1);
			
			if (ds_codigo_aux_w = ',') then
				
				update	hd_escala_dialise_dia a
				set	a.dt_fim_escala_dia	= sysdate,
					a.dt_atualizacao	= sysdate,
					a.nm_usuario		= nm_usuario_p
				where	a.nr_sequencia		= ds_codigo_temp_w
				and	a.dt_fim_escala_dia is null;
				
				ds_codigo_temp_w := '';
			else
				ds_codigo_temp_w := ds_codigo_temp_w||ds_codigo_aux_w;
			end if;
			end;
		end loop;
	end if;
end if;

commit;

end HD_finalizar_escalas_sem_vaga;
/