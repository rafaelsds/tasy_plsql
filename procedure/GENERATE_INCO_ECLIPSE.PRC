create or replace 
procedure	GENERATE_INCO_ECLIPSE(	entity_id		number,
					nr_seq_inco_p 		eclipse_inco_account.nr_seq_inconsistency%type,
					ds_detail_p 		eclipse_inco_account.ds_action_user%type,
					nm_usuario_p 		eclipse_inco_account.nm_usuario%type,
					entity_name		varchar2 default('ACCOUNT')) is
		
nr_sequencia_w 		eclipse_inco_account.nr_sequencia%type;
ds_action_user_w	eclipse_inconsistency.ds_inconsistency%type;
cd_pessoa_fisica_w	varchar2(10);
nr_atendiment_w		number(10);
nr_seq_account_w	number(10);
begin

select  max(ds_action_user)
into    ds_action_user_w
from    eclipse_inconsistency
where   nr_sequencia = nr_seq_inco_p;

select	eclipse_inco_account_seq.nextval
into	nr_sequencia_w
from	dual;

if(entity_name = 'ACCOUNT')then

	nr_seq_account_w:= entity_id;     
	
elsif(entity_name = 'PERSON') then

	cd_pessoa_fisica_w:= TO_CHAR(entity_id);

elsif(entity_name = 'ENCOUNTER') then  
  
	nr_atendiment_w := entity_id;
	
end if;
  
insert into eclipse_inco_account(nr_sequencia,
				nr_interno_conta,
				nr_seq_inconsistency,
				dt_atualizacao,
				nm_usuario,
				ds_action_user,
				nr_atendiment,               
				cd_pessoa_fisica)
values 				(nr_sequencia_w,
				nr_seq_account_w,
				nr_seq_inco_p,
				sysdate,
				nm_usuario_p,
				ds_detail_p || ' ' || ds_action_user_w,
				nr_atendiment_w,
				cd_pessoa_fisica_w);
commit;
end generate_inco_eclipse;
/
