create or replace
procedure atualizar_wheb_assist(
			cd_funcao_p		number,
			cd_perfil_p 		number,
			cd_estabelecimento_p	number,
			cd_setor_atendimento_p	number,
			nm_usuario_p		varchar2,
			nr_seq_idioma_p		number,
			ds_form_p		varchar2,
			nr_seq_item_pep_p	number) is
begin

wheb_assist_pck.set_informacoes_usuario(cd_funcao_p,cd_perfil_p,cd_estabelecimento_p,cd_setor_atendimento_p,nm_usuario_p,nr_seq_idioma_p,ds_form_p);
wheb_assist_pck.set_seq_item_pep(nr_seq_item_pep_p);

commit;

end atualizar_wheb_assist;
/