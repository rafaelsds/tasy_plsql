create or replace
procedure GERAR_MOVTO_TRANS_CHEQUE (	nr_seq_cheque_p		in	number,
					nr_seq_conta_p		in	number,
					nr_seq_trans_financ_p	in	number,
					nm_usuario_p		in	varchar2,
					nr_seq_movto_p		out	number) is

ds_conta_w		varchar2(255);
nr_seq_movto_w		number(10);
ie_data_dev_w		varchar2(255);
dt_devolucao_w		date;
dt_transacao_w		date;
ie_status_cheque_w	number(2);
nr_seq_motivo_dev_w	cheque_cr.nr_seq_motivo_dev%type;

begin

select	movto_trans_financ_seq.nextval
into	nr_seq_movto_w
from	dual;

obter_param_usuario(810, 52, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_data_dev_w);

dt_transacao_w	:= sysdate;

/*OS 1617751 AAMFIRMO - Coloquei isso fora do IF abaixo pois ser� necess�rio o status do cheque independente do param 52, para verificar qual nr_seq_motivo_dev ser� gerado no movimento bancario.
Da forma que estava, sempre gerava o motivo da primeira devolucao ( CHEQUE_CR.NR_SEQ_MOTIVO_DEV)  no movimento banc�rio, inclusive quando se tratava de segunda dev ou terceira dev.*/
ie_status_cheque_w	:= obter_status_cheque(nr_seq_cheque_p);
if (nr_seq_cheque_p is not null) then

	if (ie_status_cheque_w = 3) then  -- Primeira devolu��o
	
		select	max(a.nr_seq_motivo_dev)
		into	nr_seq_motivo_dev_w 
		from	cheque_cr a
		where	a.nr_seq_cheque = nr_seq_cheque_p;
		
	elsif (ie_status_cheque_w = 5) then -- Segunda devolu��o

		select	max(a.nr_seq_motivo_seg_dev)
		into	nr_seq_motivo_dev_w 
		from	cheque_cr a
		where	a.nr_seq_cheque = nr_seq_cheque_p;
	
	elsif (ie_status_cheque_w = 10) then -- Terceira devolu��o.

		select	max(a.nr_seq_motivo_terc_dev)
		into	nr_seq_motivo_dev_w 
		from	cheque_cr a
		where	a.nr_seq_cheque = nr_seq_cheque_p;	
	
	end if;
	
end if;	

if	(nvl(ie_data_dev_w, 'S') = 'S') then

	if	(ie_status_cheque_w = 5) then	--Caio OS: 461237, 19/06/2012 - cosiderar data da segunda devolu��o
		select	max(dt_seg_devolucao)
		into	dt_devolucao_w
		from	cheque_cr
		where	nr_seq_cheque	= nr_seq_cheque_p;
	
	elsif	(ie_status_cheque_w = 10) then	
		select	max(dt_terc_devolucao)
		into	dt_devolucao_w
		from	cheque_cr
		where	nr_seq_cheque	= nr_seq_cheque_p;
	else
		select	max(dt_devolucao_banco)
		into	dt_devolucao_w
		from	cheque_cr
		where	nr_seq_cheque	= nr_seq_cheque_p;
	end if;

	dt_transacao_w	:= nvl(dt_devolucao_w, sysdate);

end if;

insert	into movto_trans_financ
	(cd_cgc,
	cd_pessoa_fisica,
	cd_portador,
	cd_tipo_portador,
	ds_historico,
	dt_atualizacao,
	dt_transacao,
	ie_conciliacao,
	nm_usuario,
	nr_documento,
	nr_lote_contabil,
	nr_seq_banco,
	nr_seq_cheque,
	nr_seq_motivo_dev,
	nr_seq_saldo_banco,
	nr_seq_trans_financ,
	nr_sequencia,
	vl_transacao,
	vl_transacao_estrang,
	vl_complemento,
	vl_cotacao,
	cd_moeda)
select	a.cd_cgc,
	a.cd_pessoa_fisica,
	a.cd_portador,
	a.cd_tipo_portador,
	wheb_mensagem_pck.get_texto(303569,null) || ' ' || a.nr_cheque, --Movimenta��o gerada a partir da devolu��o do cheque
	sysdate,
	dt_transacao_w,
	'N',
	nm_usuario_p,
	a.nr_cheque,
	0,
	nr_seq_conta_p,
	a.nr_seq_cheque,
	nvl(nr_seq_motivo_dev_w,a.nr_seq_motivo_dev), --Deixei ainda no nvl a a.nr_seq_motivo_dev, caso nao encontrar o motivo na tratativa acima, faz o que fazia antes,
	null,
	nr_seq_trans_financ_p,
	nr_seq_movto_w,
	a.vl_cheque,
	decode(nvl(a.vl_cheque_estrang,0),0,null,a.vl_cheque_estrang), -- Projeto Multimoeda - Grava os valores em moeda estrangeira
	decode(nvl(a.vl_cheque_estrang,0),0,null,(a.vl_cheque - a.vl_cheque_estrang)),
	decode(nvl(a.vl_cheque_estrang,0),0,null,a.vl_cotacao),
	decode(nvl(a.vl_cheque_estrang,0),0,null,a.cd_moeda)
from	cheque_cr a
where	a.nr_seq_cheque	= nr_seq_cheque_p;

nr_seq_movto_p	:= nr_seq_movto_w;

commit;

end GERAR_MOVTO_TRANS_CHEQUE;
/