create or replace
procedure atualiza_resp_conv_ret_glosa(cd_resposta_p 	varchar2,
				    nm_usuario_p	varchar2,
			            nr_sequencia_p 	number) is 

begin

if (nr_sequencia_p is not null) then
	begin
	update 	convenio_retorno_glosa 
	set 	cd_resposta 	= cd_resposta_p,
		nm_usuario 	= nm_usuario_p,
		dt_atualizacao 	= sysdate
	where 	nr_sequencia 	= nr_sequencia_p;
	commit;
	end;	
end if;
end atualiza_resp_conv_ret_glosa;
/
