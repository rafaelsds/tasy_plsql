create or replace procedure SET_TOKEN_INTEGRATION_LAB(ds_Token_Integration_p varchar2,
                                                   ds_Access_Token_p      varchar2,
                                                   ds_Scope_Token_p       varchar2,
                                                   ds_Expiration_Time_p   varchar2,
                                                   ds_Token_Type_p	    varchar2,
                                                   ds_User_p	            varchar2,
                                                   ds_Establishment_p	varchar2) is           
            
	id_Token LAB_EXAME_TOKEN.NR_SEQUENCIA%type;
	dt_token_xpiration LAB_EXAME_TOKEN.dt_expiration%type;
begin	   
	begin
		select NR_SEQUENCIA
		into id_Token
		from LAB_EXAME_TOKEN 
		where DS_INTEGRATION = ds_Token_Integration_p;
	exception
	when others then
		id_Token := 0;
	end;
    
	--add dsExpirationTime_p miliseconds to sysdate
	dt_token_xpiration := (sysdate + NUMTODSINTERVAL( ds_Expiration_Time_p / 1000, 'SECOND' ));      

	if (id_Token > 0)  then
		update LAB_EXAME_TOKEN set 
			NM_USUARIO = ds_User_p,
			DT_ATUALIZACAO = sysdate,
			DS_ACCESS_TOKEN = ds_Access_Token_p, 
			DS_SCOPE = ds_Scope_Token_p,
			DS_TOKEN_TYPE = ds_Token_Type_p,
			DS_EXPIRATION_TIME = ds_Expiration_Time_p,
			dt_expiration = dt_token_xpiration
		where NR_SEQUENCIA = id_Token;    
	else
		insert into LAB_EXAME_TOKEN
		(
			NR_SEQUENCIA,
			DS_INTEGRATION,
			DS_ACCESS_TOKEN,
			DS_SCOPE,
			DS_TOKEN_TYPE,
			DS_EXPIRATION_TIME,
			DT_EXPIRATION,
			NM_USUARIO,
			DT_ATUALIZACAO,
			NM_USUARIO_NREC,
			DT_ATUALIZACAO_NREC,
			CD_ESTABELECIMENTO         
		)
		values
		(
			lab_exame_token_seq.nextval,--NR_SEQUENCIA
			ds_Token_Integration_p,--DS_INTEGRATION
			ds_Access_Token_p,--DS_ACCESS_TOKEN
			ds_Scope_Token_p,--DS_SCOPE
			ds_Token_Type_p,--DS_TOKEN_TYPE
			ds_Expiration_Time_p,--DS_EXPIRATION_TIME
			dt_token_xpiration,--DS_EXPIRATION_TIME
			ds_User_p,--NM_USUARIO
			SYSDATE,--DT_ATUALIZACAO
			ds_User_p, --NM_USUARIO_NREC
			SYSDATE,--DT_ATUALIZACAO_NREC
			ds_Establishment_p --CD_ESTABELECIMENTO
		);
	end if;	

	commit;

end SET_TOKEN_INTEGRATION_LAB;
/