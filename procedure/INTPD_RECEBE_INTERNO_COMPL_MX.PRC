create or replace 
procedure intpd_recebe_interno_compl_mx( nm_usuario_p		 	varchar2,
					  cd_estabelecimento_p	 	number,
					  nr_seq_nf_imp_arquivo_p 	number,
					  xml_p			 in 	clob) is
										
parser_w			DBMS_XMLPARSER.Parser;
document_w			DBMS_XMLDOM.DOMDocument;
root_element_w			DBMS_XMLDOM.DOMElement;
emisor_node_w			DBMS_XMLDOM.DOMNode;
conceptos_node_w		DBMS_XMLDOM.DOMNode;
concepto_node_w			DBMS_XMLDOM.DOMNode;
receptor_node_w			DBMS_XMLDOM.DOMNode;
relacionados_node_w		DBMS_XMLDOM.DOMNode;
child_list_w			DBMS_XMLDOM.DOMNodeList;
list_w				DBMS_XMLDOM.DOMNodeList;
attribute_list_w		DBMS_XMLDOM.DOMNamedNodeMap;
list_attribute_w                DBMS_XMLDOM.DOMNamedNodeMap;
list_attribute_docto_w		DBMS_XMLDOM.DOMNamedNodeMap;
nr_ident_emissor_w		nf_imp_arquivo.nr_ident_emissor%type;
ds_nome_emisor_w		nf_imp_arquivo.ds_nome_emissor%type;
imposto_attrib_list_w  		DBMS_XMLDOM.DOMNamedNodeMap;
pagos_list_w			DBMS_XMLDOM.DOMNodeList;
pago_list_w			DBMS_XMLDOM.DOMNodeList;
docto_rel_list_w		DBMS_XMLDOM.DOMNodeList;
docto_rel_w			DBMS_XMLDOM.DOMNode;
docto_node_w			DBMS_XMLDOM.DOMNode;
pago_node_w			DBMS_XMLDOM.DOMNode;
pagos_node_w			DBMS_XMLDOM.DOMNode;
qt_tamanho_pagos_w		number(5);
qt_tamanho_docto_w		number(5);
qt_tamanho_pagos_ww		number(5);
qt_tamanho_w                 	number(5);
nr_uuid_w			varchar2(255);
vl_monto_w                      varchar2(15);
cd_cgc_w			pessoa_juridica.cd_cgc%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
dt_baixa_w                      date;
dt_baixa_ww                     varchar2(255);
qtd_pf_w                      	number(5);
qtd_pj_w                      	number(5);
nr_titulo_w                     titulo_pagar.nr_titulo%type;
nr_sequencia_w                  titulo_pagar_baixa.nr_sequencia%type;
nr_sequencia_ww               	fis_dados_compl_import.nr_sequencia%type;
nr_titulo_compl_w		fis_dados_compl_import.nr_titulo%type;
ie_existe_compl_w		number(10);
nr_nfe_imp_w			titulo_pagar_baixa.nr_nfe_imp%type;

vl_monto_docto_w	varchar2(15);
nr_uuid_docto_w		varchar2(255);
type docto_relac_t	is record( vl_monto varchar2(15),
			   nr_uuid_docto varchar2(255) );

type vetor_docto_relac_t is table of docto_relac_t index by binary_integer;
vetor_docto_relac_t_w	vetor_docto_relac_t;

begin

parser_w := DBMS_XMLPARSER.newparser();
DBMS_XMLPARSER.parseclob(parser_w, xml_p);
document_w := DBMS_XMLPARSER.getDocument(parser_w);
root_element_w := DBMS_XMLDOM.GetDocumentElement(document_w);

select	count(*)
into	ie_existe_compl_w
from 	fis_dados_compl_import
where	nr_seq_nf_imp = nr_seq_nf_imp_arquivo_p;
					
if not (DBMS_XMLDOM.isNull(root_element_w)) then

	/*Dados do emissor - /cfdi:Emisor */	
	child_list_w := DBMS_XMLDOM.GetChildrenByTagName(root_element_w, 'Emisor');
	emisor_node_w := DBMS_XMLDOM.Item(child_list_w, 0);

	attribute_list_w := DBMS_XMLDOM.GetAttributes(emisor_node_w);
	nr_ident_emissor_w := nvl(DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(attribute_list_w, 'rfc')), DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(attribute_list_w, 'Rfc')));
	ds_nome_emisor_w := nvl(DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(attribute_list_w, 'nombre')), DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(attribute_list_w, 'Nombre')));
	/*fim dados do emissor*/
	

	child_list_w := DBMS_XMLDOM.GetChildrenByTagName(root_element_w, 'Complemento');
	pagos_node_w := DBMS_XMLDOM.Item(child_list_w, 0);	
	
	if not (DBMS_XMLDOM.isNull(pagos_node_w)) then
	
	pagos_list_w := DBMS_XMLDOM.GetChildNodes(pagos_node_w);
	qt_tamanho_pagos_w := DBMS_XMLDOM.GetLength(pagos_list_w);
		
	
	for m in 0 .. qt_tamanho_pagos_w - 1 loop
	
		pago_node_w := DBMS_XMLDOM.Item(pagos_list_w, m);
	
		if not (DBMS_XMLDOM.isNull(pago_node_w)) then
			
			if (DBMS_XMLDOM.GetNodeName(pago_node_w) = 'pago10:Pagos') then
			
				pago_list_w := DBMS_XMLDOM.GetChildNodes(pago_node_w);
				qt_tamanho_pagos_ww := DBMS_XMLDOM.GetLength(pago_list_w);
				
				for q in 0 .. qt_tamanho_pagos_ww - 1 loop
				
					docto_node_w := DBMS_XMLDOM.Item(pago_list_w, q);  
					list_attribute_w := DBMS_XMLDOM.GetAttributes(docto_node_w);
					
					dt_baixa_ww :=  nvl(DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_w, 'fechaPago')),
											DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_w, 'FechaPago')));

					dt_baixa_w  := to_date(replace(dt_baixa_ww, 'T', ' '), 'yyyy-mm-dd hh24:mi:ss');
					
					begin
						/*No San Javier a rotina apresenta problemas quando executada em um  servidor windows por isso este tratamento especifico para o formato de data mexicano no caso de erro*/
						vl_monto_w  := nvl(DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_w, 'monto')),
											DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_w, 'Monto')));
					exception when value_error then
						vl_monto_w  := replace(nvl(DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_w, 'monto')),
											DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_w, 'Monto'))) ,'.',',');
					end;
					
					docto_rel_list_w := DBMS_XMLDOM.GetChildNodes(docto_node_w);
					qt_tamanho_docto_w := DBMS_XMLDOM.GetLength(docto_rel_list_w);
					
					if	(qt_tamanho_docto_w > 0) then
						for d in 0..qt_tamanho_docto_w - 1 loop
							begin
							
							docto_rel_w := DBMS_XMLDOM.Item(docto_rel_list_w, d);
							list_attribute_docto_w := DBMS_XMLDOM.GetAttributes(docto_rel_w);

							nr_uuid_docto_w := DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_docto_w, 'IdDocumento'));
							
							begin
								/*No San Javier a rotina apresenta problemas quando executada em um  servidor windows por isso este tratamento especifico para o formato de data mexicano no caso de erro*/
								vl_monto_docto_w  := nvl(DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_docto_w, 'impPagado')),
													DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_docto_w, 'ImpPagado')));
							exception when value_error then
								vl_monto_docto_w  := replace(nvl(DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_docto_w, 'impPagado')),
													DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(list_attribute_docto_w, 'ImpPagado'))) ,'.',',');
							end;
							
							vetor_docto_relac_t_w(d).vl_monto := vl_monto_docto_w;
							vetor_docto_relac_t_w(d).nr_uuid_docto := nr_uuid_docto_w;

							end;
						end loop;
					end if;
				
				end loop;
				
			elsif (DBMS_XMLDOM.GetNodeName(pago_node_w) = 'tfd:TimbreFiscalDigital') then
			
				attribute_list_w := DBMS_XMLDOM.GetAttributes(pago_node_w);
				nr_uuid_w := DBMS_XMLDOM.GetNodeValue(DBMS_XMLDOM.GetNamedItem(attribute_list_w, 'UUID'));	
	
			end if;
			
			if (nvl(nr_uuid_w, 'XPTO') <> 'XPTO') 
				and (nvl(vl_monto_w, 'XPTO') <> 'XPTO') then

				for i in 0..qt_tamanho_docto_w - 1 loop
					begin
					
					vl_monto_docto_w := vetor_docto_relac_t_w(i).vl_monto;
					nr_uuid_docto_w := vetor_docto_relac_t_w(i).nr_uuid_docto;

					select 	count (*)
					into    qtd_pf_w
					from   	titulo_pagar a,
						titulo_pagar_baixa b,
						pessoa_fisica c,
						nota_fiscal n
					where   a.nr_titulo = b.nr_titulo
					and     c.cd_pessoa_fisica = a.cd_pessoa_fisica
					and	n.nr_sequencia = a.nr_seq_nota_fiscal
					and	n.nr_nfe_imp = nr_uuid_docto_w
					and     c.cd_rfc = nr_ident_emissor_w
					and     ((to_char(b.dt_baixa,'dd/mm/yyyy') = to_char(dt_baixa_w,'dd/mm/yyyy')))
					and     b.vl_baixa = to_number(vl_monto_docto_w);
				
					select 	count (*)
					into    qtd_pj_w
					from   	titulo_pagar a,
						titulo_pagar_baixa b,
						pessoa_juridica c,
						nota_fiscal n
					where   a.nr_titulo = b.nr_titulo
					and     c.cd_cgc = a.cd_cgc
					and	n.nr_sequencia = a.nr_seq_nota_fiscal
					and	n.nr_nfe_imp = nr_uuid_docto_w
					and     c.cd_rfc = nr_ident_emissor_w
					and     ((to_char(b.dt_baixa,'dd/mm/yyyy') = to_char(dt_baixa_w,'dd/mm/yyyy')))
					and     b.vl_baixa = to_number(vl_monto_docto_w);
									
					if (qtd_pf_w > 1) then
					
						select 	a.nr_titulo,
							max(b.nr_sequencia),
							b.nr_nfe_imp
						into    nr_titulo_w,
							nr_sequencia_w,
							nr_nfe_imp_w
						from   	titulo_pagar a,
							titulo_pagar_baixa b,
							pessoa_fisica c,
							nota_fiscal n
						where   a.nr_titulo = b.nr_titulo
						and     c.cd_pessoa_fisica = a.cd_pessoa_fisica
						and	n.nr_sequencia = a.nr_seq_nota_fiscal
						and	n.nr_nfe_imp = nr_uuid_docto_w
						and     c.cd_rfc = nr_ident_emissor_w
						and     ((to_char(b.dt_baixa,'dd/mm/yyyy') = to_char(dt_baixa_w,'dd/mm/yyyy')))
						and     b.vl_baixa = to_number(vl_monto_docto_w)
						group by a.nr_titulo, b.nr_nfe_imp;
									
					elsif (qtd_pj_w > 1) then
					
						select 	a.nr_titulo,
							max(b.nr_sequencia),
							b.nr_nfe_imp
						into    nr_titulo_w,
							nr_sequencia_w,
							nr_nfe_imp_w
						from   	titulo_pagar a,
							titulo_pagar_baixa b,
							pessoa_juridica c,
							nota_fiscal n
						where   a.nr_titulo = b.nr_titulo
						and     c.cd_cgc = a.cd_cgc
						and	n.nr_sequencia = a.nr_seq_nota_fiscal
						and	n.nr_nfe_imp = nr_uuid_docto_w
						and     c.cd_rfc = nr_ident_emissor_w
						and     ((to_char(b.dt_baixa,'dd/mm/yyyy') = to_char(dt_baixa_w,'dd/mm/yyyy')))
						and     b.vl_baixa = to_number(vl_monto_docto_w)
						group by a.nr_titulo, b.nr_nfe_imp;
						
					end if;
					
					if	(ie_existe_compl_w > 0) then
						select 	max(nr_titulo)
						into	nr_titulo_compl_w
						from	fis_dados_compl_import
						where 	nr_seq_nf_imp = nr_seq_nf_imp_arquivo_p
						and	nr_nfe_imp = nr_uuid_docto_w
						and	cd_rfc = nr_ident_emissor_w
						and	vl_baixa = to_number(vl_monto_docto_w);
						
						if	(qtd_pj_w > 1 or qtd_pf_w > 1) and 
							(nr_nfe_imp_w is null or nr_nfe_imp_w <> nr_uuid_w ) then
							update  titulo_pagar_baixa
							set     nr_nfe_imp = nr_uuid_w,
								dt_atualizacao = sysdate
							where   nr_titulo = nr_titulo_w
							and     nr_sequencia = nr_sequencia_w;
						end if;
						
						if	((qtd_pj_w > 1 or qtd_pf_w > 1) and nvl(nr_titulo_compl_w, 0) <> nr_titulo_w) then
							update 	fis_dados_compl_import
							set	nr_titulo = nr_titulo_w,
								nr_seq_tit_pagar_baixa = nr_sequencia_w,
								dt_atualizacao = sysdate
							where 	nr_seq_nf_imp = nr_seq_nf_imp_arquivo_p
							and	nr_nfe_imp = nr_uuid_docto_w
							and	cd_rfc = nr_ident_emissor_w;
						end if;
					else 
						if	(qtd_pj_w = 1 or qtd_pf_w = 1) and 
							(nr_nfe_imp_w is null or nr_nfe_imp_w <> nr_uuid_w ) then
							update  titulo_pagar_baixa
							set     nr_nfe_imp = nr_uuid_w,
								dt_atualizacao = sysdate
							where   nr_titulo = nr_titulo_w
							and     nr_sequencia = nr_sequencia_w;
						end if;

						select	fis_dados_compl_import_seq.nextval
						into    nr_sequencia_ww
						from	dual;							
						
						insert  into fis_dados_compl_import(
							cd_rfc,
							dt_atualizacao,                                                                                                               
							dt_atualizacao_nrec,                                                                                                                  
							nm_usuario,
							nm_usuario_nrec,
							nr_nfe_imp,
							nr_seq_nf_imp,
							nr_seq_tit_pagar_baixa, 
							nr_sequencia,                                                                                                                    
							nr_titulo,                                                                                                           
							vl_baixa) values (
							nr_ident_emissor_w,
							sysdate,
							sysdate,
							'Tasy',
							'Tasy',
							nr_uuid_docto_w,
							nr_seq_nf_imp_arquivo_p,
							nr_sequencia_w,
							nr_sequencia_ww,
							nr_titulo_w,
							to_number(vl_monto_docto_w));
					end if;
					
					end;
				end loop;	
			end if;		
		end if;	
	
	end loop;
	end if;

end if;

update	nf_imp_arquivo
set	nr_ident_emissor = nr_ident_emissor_w,
	ds_nome_emissor = ds_nome_emisor_w
where	nr_sequencia = nr_seq_nf_imp_arquivo_p;

commit;

end intpd_recebe_interno_compl_mx;
/
