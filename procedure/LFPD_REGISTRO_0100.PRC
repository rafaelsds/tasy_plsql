create or replace
procedure lfpd_registro_0100(	nr_seq_controle_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				ds_separador_p		varchar2,
				qt_linha_p		in out number,
				nr_sequencia_p		in out number) is

contador_w		number(10) := 0;
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10) := qt_linha_p;
nr_seq_registro_w	number(10) := nr_sequencia_p;
sep_w			varchar2(1) := ds_separador_p;

cd_cgc_w		varchar2(14);
cd_empresa_w		number(4);
cd_municipio_w		varchar2(10);

cursor c01 is
	select	distinct
		'0100' 										cd_registro,
		decode(d.cd_cgc,'',p.nm_pessoa_fisica,d.ds_razao_social) 			nm_contabilista,
		d.cd_cgc									cd_cgc,
		decode(d.cd_cgc,'',p.nr_cpf,'')							nr_cpf,
		e.nr_crc									nr_crc,	
		b.sg_estado									sg_estado,
		b.cd_cep									cd_cep,
		b.ds_endereco									ds_endereco,
		b.nr_endereco									nr_endereco,
		b.ds_complemento								ds_complemento,
		b.ds_bairro									ds_bairro,
		substr(elimina_caracteres_telefone(b.nr_ddd_telefone || b.nr_telefone),1,10) 	nr_telefone,
		substr(elimina_caracteres_telefone(b.nr_ddd_fax || b.nr_fax),1,10) 		nr_fax,
		nvl(c.ds_email, b.ds_email)							ds_email,
		obter_dados_pf_pj(e.cd_contabilista, b.cd_cgc,'CDMDV') 			cd_localidade
	from	pessoa_juridica b,
		pessoa_juridica d,
		pessoa_fisica p,
		compl_pessoa_fisica c,
		estabelecimento a,
		empresa e
	where	a.cd_cgc	= b.cd_cgc(+)
	and	e.cd_empresa	= a.cd_empresa
	and	e.cd_cnpj_escritorio_ctb = d.cd_cgc(+)
	and	e.cd_contabilista = p.cd_pessoa_fisica
	and	p.cd_pessoa_fisica = c.cd_pessoa_fisica
	and	b.ie_situacao	= 'A'
	and	e.cd_empresa = cd_empresa_w
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	c.ie_tipo_complemento = 1;

vet01	c01%RowType;

begin

select	cd_empresa
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

open c01;
loop
fetch c01 into	
	vet01;
exit when c01%notfound;
	begin
	cd_municipio_w := vet01.cd_localidade;
	
	if(vet01.sg_estado = 'DF') then
		cd_municipio_w := '5300108';
	end if;
	
	contador_w := contador_w + 1;
	
	ds_linha_w	:= substr(	sep_w || vet01.cd_registro 		||
					sep_w || vet01.nm_contabilista 		||
					sep_w || vet01.cd_cgc 			||
					sep_w || vet01.nr_cpf 			||
					sep_w || vet01.nr_crc 			||
					sep_w || vet01.sg_estado 		||
					sep_w || vet01.cd_cep 			||
					sep_w || vet01.ds_endereco 		||
					sep_w || vet01.nr_endereco 		||
					sep_w || vet01.ds_complemento 		||
					sep_w || vet01.ds_bairro 		||
					sep_w || ''		 		||
					sep_w || ''		 		||
					sep_w || vet01.nr_telefone 		||
					sep_w || vet01.nr_fax			||
					sep_w || vet01.ds_email  		|| sep_w, 1, 8000);
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;
	
	insert into fis_lfpd_arquivo	(	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_linha,
						ds_arquivo,
						ds_arquivo_compl,
						cd_registro,
						nr_seq_controle_lfpd)
				values	(	fis_lfpd_arquivo_seq.NextVal,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_linha_w,
						ds_arquivo_w,
						ds_arquivo_compl_w,
						vet01.cd_registro,
						nr_seq_controle_p);

	if (mod(contador_w,100) = 0) then
		commit;
	end if;
	
	end;
end loop;
close c01;
	
commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end lfpd_registro_0100;
/