create or replace procedure grav_hist_funcoes_html5_resumo
is
begin

insert into funcoes_html5_resumo_hist(
		nr_sequencia					,
		dt_referencia         ,
		dt_atualizacao				,
		nm_usuario						,
		dt_atualizacao_nrec		,
		nm_usuario_nrec				,
		nr_seq_agrupamento		,
		ie_tipo_informacao		,
		qt_hora_previsto			,
		qt_hora_realizado			,
		pr_previsto						,
		pr_realizado          ,
		pr_previsto_baseline
	)
select	
		funcoes_html5_resumo_hist_seq.nextval	,
		trunc(sysdate)				 			          ,
		dt_atualizacao												,
		nm_usuario                   			    ,
		dt_atualizacao_nrec          			    ,
		nm_usuario_nrec              			    ,
		nr_seq_agrupamento						        ,
		ie_tipo_informacao						        ,
		qt_hora_previsto						          ,
		qt_hora_realizado						          ,
		pr_previsto								            ,
		pr_realizado                          ,
		pr_previsto_baseline
from	funcoes_html5_resumo;

commit;

end grav_hist_funcoes_html5_resumo;
/