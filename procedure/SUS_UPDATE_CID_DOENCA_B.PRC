create or replace
procedure sus_update_cid_doenca_b(ds_doenca_cid_p		varchar2,
			nm_usuario_p		varchar2,
			cd_doenca_cid_p		varchar2,
			ie_restricao_sexo_p		varchar2,
			ie_subcat_p		varchar2) is 

begin

update cid_doenca set
	ds_doenca_cid        	= ds_doenca_cid_p,
	dt_atualizacao       	= sysdate,
	nm_usuario           	= nm_usuario_p,
	cd_categoria_cid     	= substr(cd_doenca_cid_p,1,3),
	ie_sexo              	= ie_restricao_sexo_p,
	ie_cad_interno       	= 'N',
	ie_situacao          	= 'A',
	ie_subcategoria      	= nvl(ie_subcat_p,'S')
where 	cd_doenca_cid 		= cd_doenca_cid_p;

commit;

end sus_update_cid_doenca_b;
/
