create or replace
procedure atualizar_impressao_agrup_lote(
			nm_usuario_p		Varchar2,
			nr_sequencia_p		Number,
			ie_impressao_p		Varchar2) is 

begin

if (ie_impressao_p = 'I') then
	update  material_lote_fornec_agrup
        set	dt_impressao = sysdate,
		nm_usuario_impressao = nm_usuario_p
	where   nr_sequencia = nr_sequencia_p;
elsif (ie_impressao_p = 'R') then
	update  material_lote_fornec_agrup
        set	dt_reimpressao = sysdate,
		nm_usuario_reimpressao = nm_usuario_p
	where   nr_sequencia = nr_sequencia_p;
end if;

commit;

end atualizar_impressao_agrup_lote;
/