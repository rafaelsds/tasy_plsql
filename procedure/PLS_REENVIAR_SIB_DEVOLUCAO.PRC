create or replace
procedure pls_reenviar_sib_devolucao
			(	nr_seq_devolucao_p	number,
				ie_tipo_devolucao_p	varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 

/* ie_tipo_devolucao_p
	E = Erro
	R - Encerramento
	C - CCO
*/

begin

if	(ie_tipo_devolucao_p	= 'E') then
	update	sib_devolucao_erro
	set	ie_reenvio	= decode(ie_reenvio,'S','N','N','S','S')
	where	nr_sequencia	= nr_seq_devolucao_p;
end if;

commit;

end pls_reenviar_sib_devolucao;
/