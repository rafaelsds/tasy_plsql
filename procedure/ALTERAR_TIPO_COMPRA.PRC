create or replace
procedure alterar_tipo_compra(ie_tipo_compra_p		Varchar2,
				nr_solic_compra_p		number) is 

begin
UPDATE solic_compra
SET ie_tipo_compra = ie_tipo_compra_p
WHERE nr_solic_compra = nr_solic_compra_p;
commit;

end alterar_tipo_compra;
/