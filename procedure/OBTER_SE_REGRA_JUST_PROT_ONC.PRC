create or replace
procedure obter_se_regra_just_prot_onc	(nr_seq_regra_p	number,
										 nr_seq_paciente_p	number,
										 nr_seq_material_p	number,
										 ie_existe_p out varchar2) is

ie_existe_w						varchar2(1) := 'N';
qt_dose_prescricao_w			Number(15,4);
qt_regra_justificada_w  		number(10);
cd_perfil_w						number(10);
ie_justificar_consistencia_w	varchar2(255);
ie_forma_consistencia_w			varchar2(255);
qt_regra_justif_regra_w  		number(10);
											
begin
if	(nr_seq_regra_p is not null) and
	(nr_seq_paciente_p is not null) and
	(nr_seq_material_p is not null)then
	
	cd_perfil_w	:= obter_perfil_ativo;
	
	Select  max(IE_JUSTIFICAR_CONSISTENCIA),
			max(ie_forma_consistencia)
	into	ie_justificar_consistencia_w,
			ie_forma_consistencia_w
	from	regra_consiste_onc_par
	where	nr_seq_regra = nr_seq_regra_p
	and		nvl(cd_perfil,cd_perfil_w) = cd_perfil_w;
	
	if	(nvl(ie_justificar_consistencia_w,'N') = 'S') and
		(nvl(ie_forma_consistencia_w,'N') = 'S') then
	
		Select 	max(qt_dose_prescr)
		into	qt_dose_prescricao_w
		from	paciente_protocolo_medic
		where	nr_seq_paciente = nr_seq_paciente_p
		and		nr_seq_material = nr_seq_material_p;
		
		
		Select 	count(*)
		into	qt_regra_justificada_w
		from	paciente_atend_erro_just
		where	nr_seq_paciente = nr_seq_paciente_p
		and		nr_seq_material = nr_seq_material_p
		and		qt_dose_prescricao = qt_dose_prescricao_w
		and		ds_justif_incons_quimio is not null
		and		nr_seq_regra = nr_seq_regra_p;

		if	(qt_regra_justificada_w	= 0) then
			
			
			Select 	count(*)
			into	qt_regra_justif_regra_w
			from	paciente_atend_erro_just
			where	nr_seq_paciente = nr_seq_paciente_p
			and		nr_seq_material = nr_seq_material_p
			and		nr_seq_regra = nr_seq_regra_p;
			
			if	(qt_regra_justif_regra_w > 0) then
			
				update 	paciente_atend_erro_just
				set		ds_justif_incons_quimio = '',
						qt_dose_prescricao = 0
				where	nr_seq_paciente = nr_seq_paciente_p
				and		nr_seq_material = nr_seq_material_p
				and		nr_seq_regra = nr_seq_regra_p;
				
				commit;
				
			end if;
			
			ie_existe_w := 'N';
		
		else
			
			ie_existe_w := 'S';
			
		end if;
		
	end if;
	
end if;

ie_existe_p :=  ie_existe_w;

end obter_se_regra_just_prot_onc;
/