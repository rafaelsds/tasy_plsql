create or replace
procedure man_gerar_custo_terceiro(	cd_estabelecimento_p	number,
					nr_seq_ordem_serv_p	number,
					nm_usuario_p		Varchar2,
					dt_referencia_p		date) is

cd_centro_custo_w			number(8);
cd_estabelecimento_w			number(4);
cd_setor_local_w			number(5);
ds_centro_custo_w			varchar2(80);
ds_setor_atendimento_w			varchar2(100);
dt_fim_real_w				Date;
ie_status_ordem_w			Varchar2(1);
ie_tipo_w				varchar2(15);
nr_grupo_trabalho_w			number(10);
nr_seq_localizacao_w			number(10);
nr_seq_nota_fiscal_w			number(10);
nr_seq_operacao_w			Number(10,0);
nr_seq_terceiro_w			number(10);
nr_sequencia_w				number(10);
qt_registro_w				number(10);
vl_custo_w				number(17,4);

/* Custos da Ordem Servi�o*/
cursor c01 is
select	a.ie_tipo,
	nvl(sum(a.vl_custo),0)
from	man_ordem_serv_custo a
where	a.nr_seq_ordem_serv = nr_seq_ordem_serv_p
and	upper(a.ie_tipo) <> 'REQ'
group by a.ie_tipo;

BEGIN

/* Dados da ordem de servi�o*/
select	dt_fim_real,
	ie_status_ordem,
	nr_seq_localizacao,
	nr_grupo_trabalho
into	dt_fim_real_w,
	ie_status_ordem_w,
	nr_seq_localizacao_w,
	nr_grupo_trabalho_w
from	man_ordem_servico
where	nr_sequencia = nr_seq_ordem_serv_p;

/* Buscar os dados da localizacao */
select	cd_estabelecimento,
	cd_setor
into	cd_estabelecimento_w,
	cd_setor_local_w
from	man_localizacao
where	nr_sequencia = nr_seq_localizacao_w;

/* Centro de Custo do setor da localiza��o (Centro de Custo da Ordem de servi�o) */

select	Nvl(max(cd_centro_custo),0),
	Max(ds_setor_atendimento)
into	cd_centro_custo_w,
	ds_setor_atendimento_w
from	setor_atendimento
where	cd_setor_atendimento = cd_setor_local_w;

if	(cd_centro_custo_w = 0)  then
	/*(-20011,'O setor da localiza��o, ' || ds_setor_atendimento_w || chr(13) || ' n�o possui centro de custo informado!')*/
	wheb_mensagem_pck.exibir_mensagem_abort(263074,'DS_SETOR=' || ds_setor_atendimento_w);
end if;

/* Pessoa Terceiro do centro custo - conta que receber� os valores da OS */
select	nvl(max(nr_seq_terceiro),0),
	max(ds_centro_custo)
into	nr_seq_terceiro_w,
	ds_centro_custo_w
from	centro_custo
where	cd_centro_custo = cd_centro_custo_w;

if	(nr_seq_terceiro_w = 0) then
	/*(-20011,'O centro de custo da localiza��o, ' || ds_centro_custo_w || chr(13) || ' n�o possui terceiro informado!')*/
	wheb_mensagem_pck.exibir_mensagem_abort(263075,'DS_CENTRO=' || ds_centro_custo_w);
end if;
/* Opera��o terceiro do grupo de trabalho*/
select	Nvl(max(nr_seq_operacao),0)
into	nr_seq_operacao_w
from	man_grupo_trabalho
where	nr_sequencia = nr_grupo_trabalho_w
and	cd_estabelecimento = cd_estabelecimento_p;

/* Buscar Valores de Custo da Ordem de Servi�o - calcular*/

man_calcular_custo_os(nr_seq_ordem_serv_p, nm_usuario_p);

/* Integrar na conta do terceiro somente se tiver Custo na OS */
select	count(*)
into	qt_registro_w
from	man_ordem_serv_custo
where	nr_Seq_ordem_serv = nr_seq_ordem_serv_p;

if	(dt_fim_real_w is not null) and
	(ie_status_ordem_w = 3)	and
	(nr_seq_terceiro_w > 0) and
	(qt_registro_w > 0) 	then

	open c01;
	loop
	fetch c01 into
		ie_tipo_w,
		vl_custo_w;
	exit when c01%notfound;
	begin	
		select	terceiro_operacao_seq.nextval
		into	nr_sequencia_w
		from	dual;
		insert into terceiro_operacao(
			nr_sequencia,
			cd_estabelecimento,
			nr_seq_terceiro,
			nr_seq_operacao,
			tx_operacao,
			dt_atualizacao,
			nm_usuario,
			nr_doc,
			nr_seq_doc,
			nr_seq_ordem_servico,
			dt_operacao,
			nr_seq_conta,
			cd_material,
			ie_origem_proced,
			cd_procedimento,
			qt_operacao,
			vl_operacao)
		values(nr_sequencia_w,
			cd_estabelecimento_w,
			nr_seq_terceiro_w,
			nr_seq_operacao_w,
			100,
			sysdate,
			nm_usuario_p,
			nr_seq_ordem_serv_p,
			1,
			nr_seq_ordem_serv_p,
			dt_referencia_p,
			null,
 			null,
			null,
			null, 
			null,
			vl_custo_w);
		atualizar_operacao_terceiro(nr_sequencia_w, nm_usuario_p);
	end;
	end loop;
	close c01;
end if;

commit;
END man_gerar_custo_terceiro;
/