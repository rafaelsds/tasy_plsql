create or replace
procedure permite_encer_atend_enf_evol(nr_atendimento_p	number) is

ie_evolucao_w varchar2(1);

begin

select	nvl(max('S'),'N')
into	ie_evolucao_w
from	evolucao_paciente
where	nr_atendimento = nr_atendimento_p
and	dt_liberacao is not null;


if	(ie_evolucao_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(281999);
end if;

end permite_encer_atend_enf_evol;
/
