create or replace
procedure lab_atualiza_tipo_sangue_pac(	nr_seq_resultado_p exame_lab_result_item.nr_sequencia%type,
                                        nr_seq_prescr_p exame_lab_result_item.nr_seq_prescr%type) is 
					
ds_resultado_w	 exame_lab_result_item.ds_resultado%type;
cd_pessoa_fisica_w pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_exame_w	exame_lab_result_item.nr_seq_exame%type;

cursor c01 is
SELECT  a.ds_resultado,
        substr(OBTER_PESSOA_ATENDIMENTO(b.nr_atendimento, 'C'),1,10) cd_pessoa_fisica,
        a.nr_seq_exame 
FROM    exame_lab_result_item a, 
        exame_lab_resultado b 
WHERE   a.nr_seq_resultado = b.nr_seq_resultado
AND     b.nr_seq_resultado = nr_seq_resultado_p
AND     a.nr_seq_prescr = nr_seq_prescr_p
AND     LAB_OBTER_SE_ATUALIZ_TIPO_SAN(b.nr_seq_resultado, a.nr_seq_prescr, a.nr_seq_exame) = 'S';

begin
--Cautela, procedure chamada na trigger prescr_procedimento_update
open C01;
loop
fetch C01 into 
	ds_resultado_w,
	cd_pessoa_fisica_w,
	nr_seq_exame_w;
exit when C01%notfound;
begin
		update  pessoa_fisica
		set     ie_tipo_sangue = ds_resultado_w,
			dt_atualizacao = sysdate,
			nm_usuario = nvl(wheb_usuario_pck.get_nm_usuario,'Tasy')
		where   cd_pessoa_fisica = cd_pessoa_fisica_w;
end;
end loop;
close C01;

end lab_atualiza_tipo_sangue_pac;
/