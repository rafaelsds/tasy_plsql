create or replace
procedure atualiza_adiantamento_pago 
		(nr_titulo_p		number,
		nr_adiantamento_p	number,
		vl_vinculacao_p	number,
		nr_sequencia_p	number,
		nm_usuario_p		varchar2,
		ie_inclui_exclui_p	varchar2) is


vl_saldo_titulo_w		number(15,2) := 0;
vl_saldo_adiant_w		number(15,2) := 0;
nr_lote_contabil_w		number(10,0) := 0;
nr_sequencia_w			number(10,0) := 0;
vl_vinculacao_ant_w		number(15,2) := 0;
nr_seq_trans_fin_w		number(10);
dt_contabil_w			date;

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_empresa_w				estabelecimento.cd_empresa%type;
ie_tipo_titulo_cpa_w		titulo_pagar.ie_tipo_titulo%type;
ie_baixa_lote_contab_w		parametros_contas_pagar.ie_baixa_lote_contab%type;
nr_lote_contabil_adiant_w	lote_contabil.nr_lote_contabil%type;
ds_tipo_lote_contabil_w		tipo_lote_contabil.ds_tipo_lote_contabil%type;
qt_registro_w				number(10);

/* Projeto Multimoeda - Variaveis */
vl_adto_estrang_w	number(15,2);
vl_cotacao_w		cotacao_moeda.vl_cotacao%type;
vl_complemento_w	number(15,2);
cd_moeda_w			number(5);
vl_cambial_ativo_w		titulo_pagar_adiant.vl_cambial_ativo%type;
vl_cambial_passivo_w	titulo_pagar_adiant.vl_cambial_passivo%type;

cursor c01 is
select	nvl(vl_adiantamento,0)
from	titulo_pagar_adiant
where	nr_titulo		= nr_titulo_p
and	nr_adiantamento	= nr_adiantamento_p
and	vl_adiantamento	= vl_vinculacao_p * -1;

begin

if	(nvl(nr_adiantamento_p,0) > 0) then
/* obter dados do adiantamento  */ 
select	nvl(nr_lote_contabil,0),
	vl_saldo
into 	nr_lote_contabil_w,
	vl_saldo_adiant_w
from	adiantamento_pago
where	nr_adiantamento	= nr_adiantamento_p;

if	(nvl(nr_titulo_p, 0) > 0) then

	/* obter dados do titulo  */ 
	select	vl_saldo_titulo
	into 	vl_saldo_titulo_w
	from	titulo_pagar
	where	nr_titulo	= nr_titulo_p;
	
	/* Projeto Multimoeda - Busca dados  do adiantamento do titulo a pagar*/
	select max(vl_adto_estrang),
		max(vl_cotacao),
		max(cd_moeda),
		max(vl_cambial_ativo),
		max(vl_cambial_passivo)
	into vl_adto_estrang_w,
		vl_cotacao_w,
		cd_moeda_w,
		vl_cambial_ativo_w,
		vl_cambial_passivo_w
	from titulo_pagar_adiant
	where nr_titulo = nr_titulo_p
	and nr_sequencia = nr_sequencia_p;
				
	select	max(nr_seq_trans_fin),
		max(dt_contabil)
	into nr_seq_trans_fin_w,
		dt_contabil_w
	from titulo_pagar_adiant
	where nr_titulo = nr_titulo_p
	and nr_sequencia = nr_sequencia_p;

	if	(ie_inclui_exclui_p = 'I') then

		select max(ie_tipo_titulo),
			max(cd_estabelecimento)
		into ie_tipo_titulo_cpa_w,
			cd_estabelecimento_w
		from titulo_pagar
		where nr_titulo = nr_titulo_p;

		if (cd_estabelecimento_w is not null) then

			if (nvl(fin_obter_se_mes_aberto(cd_estabelecimento_w,dt_contabil_w,'CP',ie_tipo_titulo_cpa_w,null,null,null),'S') = 'N') then
				wheb_mensagem_pck.exibir_mensagem_abort(183663);
			end if;

			philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),dt_contabil_w);

			select  max(ie_baixa_lote_contab)
			into  ie_baixa_lote_contab_w
			from  parametros_contas_pagar
			where   cd_estabelecimento  = cd_estabelecimento_w;

			select  max(cd_empresa)
			into  cd_empresa_w
			from  estabelecimento
			where  cd_estabelecimento = cd_estabelecimento_w;

			if  (cd_empresa_w is not null) then

				if  (ie_baixa_lote_contab_w = 'N') then
					select  count(*)
					into  qt_registro_w
					from   ctb_mes_ref
					where  cd_empresa = cd_empresa_w
					and  substr(ctb_obter_se_mes_fechado(nr_sequencia,cd_estabelecimento_w),1,1) = 'F'
					and  trunc(dt_referencia,'month') = trunc(dt_contabil_w,'month');

					if  (qt_registro_w > 0) then
						/*O mes de referencia na contabilidade para esta data de baixa (#@DT_BAIXA#@) ja foi fechado!*/
						Wheb_mensagem_pck.exibir_mensagem_abort(204540,'DT_BAIXA='||PKG_DATE_FORMATERS.to_varchar(dt_contabil_w, 'shortDate', cd_estabelecimento_w, nm_usuario_p));
					end if;

				elsif  (ie_baixa_lote_contab_w = 'L') then

					select  count(*),
						max(a.nr_lote_contabil),
						max(b.ds_tipo_lote_contabil)
					into  qt_registro_w,
						nr_lote_contabil_adiant_w,
						ds_tipo_lote_contabil_w
					from   tipo_lote_contabil b,
						lote_contabil a
					where  a.cd_tipo_lote_contabil  = b.cd_tipo_lote_contabil
					and  a.cd_estabelecimento = cd_estabelecimento_w
					and  trunc(a.dt_referencia,'dd')  = trunc(dt_contabil_w,'dd')
					and  a.cd_tipo_lote_contabil  = 7
					and  exists  (  select  1
						from  movimento_contabil z
						where  z.nr_lote_contabil  = a.nr_lote_contabil);

					if  (qt_registro_w > 0) then
						/*Ja foi gerado lote contabil para esta data de baixa.
						Lote: #@NR_LOTE_CONTABIL#@
						Tipo de lote contabil: #@DS_TIPO_LOTE_CONTABIL#@
						Data: #@DT_BAIXA#@*/
						Wheb_mensagem_pck.exibir_mensagem_abort(204541, 'NR_LOTE_CONTABIL='||nr_lote_contabil_adiant_w ||
							';DS_TIPO_LOTE_CONTABIL='||ds_tipo_lote_contabil_w ||
							';DT_BAIXA='||PKG_DATE_FORMATERS.to_varchar(dt_contabil_w, 'shortDate', cd_estabelecimento_w, nm_usuario_p));
					end if;

				elsif  (ie_baixa_lote_contab_w = 'M') then

					select  count(*),
						max(a.nr_lote_contabil),
						max(b.ds_tipo_lote_contabil)
					into  qt_registro_w,
						nr_lote_contabil_adiant_w,
						ds_tipo_lote_contabil_w
					from  tipo_lote_contabil b,
						lote_contabil a
					where  a.cd_tipo_lote_contabil  = b.cd_tipo_lote_contabil
					and  a.cd_estabelecimento = cd_estabelecimento_w
					and  trunc(a.dt_referencia,'dd')  >= trunc(dt_contabil_w,'dd')
					and  trunc(a.dt_referencia,'month')  = trunc(dt_contabil_w,'month')
					and  a.cd_tipo_lote_contabil  = 7
					and  exists  (  select  1
						from  movimento_contabil z
						where  z.nr_lote_contabil  = a.nr_lote_contabil);

					if  (qt_registro_w > 0) then
						/*Ja foi gerado lote contabil para esta data de baixa.
						Lote: #@NR_LOTE_CONTABIL#@
						Tipo de lote contabil: #@DS_TIPO_LOTE_CONTABIL#@
						Data: #@DT_BAIXA#@*/
						Wheb_mensagem_pck.exibir_mensagem_abort(204541, 'NR_LOTE_CONTABIL='||nr_lote_contabil_adiant_w ||
							';DS_TIPO_LOTE_CONTABIL='||ds_tipo_lote_contabil_w ||
							';DT_BAIXA='||PKG_DATE_FORMATERS.to_varchar(dt_contabil_w, 'shortDate', cd_estabelecimento_w, nm_usuario_p));
					end if;

				elsif  (ie_baixa_lote_contab_w = 'F') then

					select  count(*)
					into  qt_registro_w
					from   ctb_mes_ref
					where  cd_empresa = cd_empresa_w
					and  trunc(dt_referencia,'month') = trunc(dt_contabil_w,'month');

					if  (qt_registro_w > 0) then
						/*Ja existe mes de referencia na contabilidade para esta data de baixa (#@DT_BAIXA#@).*/
						Wheb_mensagem_pck.exibir_mensagem_abort(204542,'DT_BAIXA='||PKG_DATE_FORMATERS.to_varchar(dt_contabil_w, 'shortDate', cd_estabelecimento_w, nm_usuario_p));
					end if;

				end if;
			end if;
		end if;

		vl_saldo_titulo_w	:= vl_saldo_titulo_w - vl_vinculacao_p;
		vl_saldo_adiant_w	:= vl_saldo_adiant_w - vl_vinculacao_p;

		/*		Edgar 05/05/2005 Atualizar_Saldo_Tit_Pagar ja atualiza saldo titulo
		if	(vl_saldo_titulo_w = 0) then

			update	titulo_pagar
			set	vl_saldo_titulo	= vl_saldo_titulo_w,
       		 		ie_situacao	= 'L',
				dt_liquidacao	= trunc(sysdate)
			where	nr_titulo	= nr_titulo_p;

		elsif	(vl_saldo_titulo_w <> 0) then

			update	titulo_pagar
			set	vl_saldo_titulo = vl_saldo_titulo_w,
				ie_situacao     = 'A'	
			where	nr_titulo	= nr_titulo_p;

		end if;
		*/

	elsif	(ie_inclui_exclui_p = 'E') then

		vl_saldo_titulo_w	:= vl_saldo_titulo_w + vl_vinculacao_p;
		vl_saldo_adiant_w	:= vl_saldo_adiant_w + vl_vinculacao_p;

		open c01;
		loop
		fetch c01 into
			vl_vinculacao_ant_w;
		exit when c01%notfound;
		end loop;

		if	(vl_vinculacao_ant_w = 0) then

			/*	Edgar 05/05/2005 Atualizar_Saldo_Tit_Pagar ja atualiza saldo titulo
			update	titulo_pagar
			set	vl_saldo_titulo	= vl_saldo_titulo_w,
				ie_situacao	= 'A',
				dt_liquidacao	= null
			where	nr_titulo	= nr_titulo_p;
			*/

			if	(nr_lote_contabil_w = 0) then

				delete	from titulo_pagar_adiant
				where	nr_titulo	= nr_titulo_p
				and	nr_sequencia	= nr_sequencia_p;

			elsif	(nr_lote_contabil_w <> 0) then

				select	max(nr_sequencia) + 1
				into	nr_sequencia_w 
				from	titulo_pagar_adiant
				where	nr_titulo	= nr_titulo_p;
				
				/* Projeto Multimoeda - Verifica se o adiantamento e em moeda estrangeira, caso negativo limpa os campos antes de gravar*/
				if (nvl(vl_adto_estrang_w,0) <> 0) then
					vl_complemento_w := (vl_vinculacao_p - vl_adto_estrang_w) * -1;
					vl_adto_estrang_w := vl_adto_estrang_w * -1;
					vl_cambial_ativo_w := vl_cambial_ativo_w * -1;
					vl_cambial_passivo_w := vl_cambial_passivo_w * -1;
				else
					vl_adto_estrang_w := null;
					vl_complemento_w := null;
					vl_cotacao_w := null;
					cd_moeda_w := null;
					vl_cambial_ativo_w := null;
					vl_cambial_passivo_w := null;
				end if;
				
				insert	into titulo_pagar_adiant 
					(nr_titulo, 
					nr_sequencia, 
					nr_adiantamento,
					vl_adiantamento,
					nm_usuario,
					dt_atualizacao,
					nr_seq_trans_fin,
					dt_contabil,
					vl_adto_estrang,
					vl_complemento,
					vl_cotacao,
					cd_moeda,
					vl_cambial_ativo,
					vl_cambial_passivo)
				values	(nr_titulo_p,
					nr_sequencia_w,
					nr_adiantamento_p,
					vl_vinculacao_p * -1,
					nm_usuario_p,
					sysdate,
					nr_seq_trans_fin_w,
					dt_contabil_w,
					vl_adto_estrang_w,
					vl_complemento_w,
					vl_cotacao_w,
					cd_moeda_w,
					vl_cambial_ativo_w,
					vl_cambial_passivo_w);
			end if;
		end if;
	end if;

	Atualizar_Saldo_Tit_Pagar(nr_titulo_p, nm_usuario_p);
	Gerar_W_Tit_Pag_imposto(nr_titulo_p, nm_usuario_p);

end if;
/* Projeto Multimoeda - Verifica o valor antes de passar o parametro.*/
if (nvl(vl_cotacao_w,0) = 0) then
	vl_cotacao_w := null;
end if;

ATUALIZAR_SALDO_ADIANT_PAGO(nr_adiantamento_p, nm_usuario_p, vl_cotacao_w);

commit;

end if;

end atualiza_adiantamento_pago;
/
