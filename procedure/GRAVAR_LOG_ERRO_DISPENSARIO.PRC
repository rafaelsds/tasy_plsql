create or replace
procedure gravar_log_erro_dispensario(
		cd_estabelecimento_p	number,
		nr_atendimento_p		number,
		ds_erro_p			varchar2,
		ds_arquivo_p		varchar2,
		nm_usuario_p		varchar2) as


begin

/*Log de erro para o dispensário relacionado ao programa EtqDisEL*/

insert into dispensario_log_erro(
	nr_sequencia,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	ds_erro,
	nr_atendimento,
	ds_arquivo)
values(	dispensario_log_erro_seq.nextval,
	cd_estabelecimento_p,
	sysdate,
	nm_usuario_p,
	ds_erro_p,
	nr_atendimento_p,
	ds_arquivo_p);

commit;

end gravar_log_erro_dispensario;
/