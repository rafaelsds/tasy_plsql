create or replace
procedure sus_consiste_numero_aih
		(	nr_interno_conta_p	number,
			nr_aih_p		number,
			nr_seq_aih_p		number,
			nm_usuario_p		varchar2,
			ie_tipo_consiste_p	number,
			ds_erro_p	out	varchar2) is


cd_estabelecimento_w	number(4);
ds_detalhe_w		varchar2(255)	:= '';
nr_aih_consiste13_w	number(13);
ds_digito_aih13_w	varchar2(1);
ds_erro_w               varchar2(255)   := '';
qt_aih_elet_w		number(10) := 0;


BEGIN

/* Obter dados da conta e do procedimento */
select	nvl(max(cd_estabelecimento),1)
into	cd_estabelecimento_w
from	conta_Paciente
where	nr_interno_conta	= nr_interno_conta_p;

ds_detalhe_w	:= WHEB_MENSAGEM_PCK.get_texto(281067) || nr_aih_p || ' - ' || nr_seq_aih_p;

/* 4 - N�mero da AIH inv�lido */
if	(Sus_Obter_Inco_Ativa(4)) and
	(nr_aih_p is not null) and
	(length(nr_aih_p) <> 13) then
	if      (ie_tipo_consiste_p = 1) then
		sus_gravar_inconsistencia(nr_interno_conta_p, 4, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);
	elsif   (ie_tipo_consiste_p = 2) then
        	ds_erro_w	:= ds_erro_w || '4, ';
	end if;
end if;

/* 5 - D�gito verificador da AIH inv�lido */
if	(Sus_Obter_Inco_Ativa(5)) and
	(nr_aih_p is not null) and
	(length(nr_aih_p) = 13) then
	nr_aih_consiste13_w	:= trunc(nr_aih_p);
	select	consistir_digito('AIH',to_char(nr_aih_consiste13_w))
	into	ds_digito_aih13_w
	from	dual;
	if	(ds_digito_aih13_w = 'N') then
		if	(ie_tipo_consiste_p = 1) then
			sus_gravar_inconsistencia(nr_interno_conta_p, 5, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);		
		elsif   (ie_tipo_consiste_p = 2) then
        		ds_erro_w	:= ds_erro_w || '5, ';
		end if;
	end if;
end if;

if	(Sus_Obter_Inco_Ativa(250)) and
	(nr_aih_p is not null) then
	begin
	
	select	count(*)
	into	qt_aih_elet_w
	from	sus_aih_unif
	where	nr_aih = nr_aih_p
	and	nr_sequencia = nr_seq_aih_p
	and	cd_carater_internacao <> '01'
	and	substr(nr_aih,5,9) > '500000001'
	and	substr(nr_aih,5,9) < '500099999';
	
	if	(qt_aih_elet_w > 0) then
		if	(ie_tipo_consiste_p = 1) then
			sus_gravar_inconsistencia(nr_interno_conta_p, 250, ds_detalhe_w, cd_estabelecimento_w, nm_usuario_p);		
		elsif   (ie_tipo_consiste_p = 2) then
        		ds_erro_w	:= ds_erro_w || '250, ';
		end if;
	end if;
	
	end;
end if;

ds_erro_p	:= ds_erro_w;

end sus_consiste_numero_aih;
/
