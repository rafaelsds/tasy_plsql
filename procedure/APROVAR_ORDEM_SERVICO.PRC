create or replace
procedure Aprovar_Ordem_Servico(
			nm_usuario_p		Varchar2,
			nr_seq_p		number,
			cd_pf_p			number) is 

begin
update   man_ordem_servico_aprov          
set      dt_aprovacao      = sysdate,     
dt_atualizacao    = sysdate,     
nm_usuario        = nm_usuario_p  
where    nr_seq_ordem_serv = nr_seq_p      
and      cd_pessoa_aprov   = cd_pf_p;      

commit;

end Aprovar_Ordem_Servico;
/