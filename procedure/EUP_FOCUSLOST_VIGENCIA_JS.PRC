create or replace
procedure eup_focuslost_vigencia_js(
					dt_final_vigencia_p	date,
					dt_inicio_vigencia_p	date,
					qt_dia_internacao_p	number,
					ie_completo_p		varchar2,
					cd_convenio_p		number,
					ie_atributo_p		varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2,
					dt_final_vig_ret_p out date) is 

ie_calcula_vigencia_w	varchar2(1);
ie_dias_vigencia_w		varchar2(1);
qt_adicional_w		number(5) := 0;		
qt_dias_validade_guia_w	number(5);			
			
begin

Obter_param_Usuario(916, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_calcula_vigencia_w);

if	(ie_completo_p = 'S') then
	
	if	((ie_calcula_vigencia_w = 'S') or
		(ie_calcula_vigencia_w = 'R')) then
		
		select	nvl(max(ie_cons_dia_int_vig),'N')
		into	ie_dias_vigencia_w
		from 	convenio_estabelecimento
       where	cd_convenio = cd_convenio_p
       and    	cd_estabelecimento = cd_estabelecimento_p;
		
		if	(nvl(ie_dias_vigencia_w,'N') = 'S') then
			qt_adicional_w := -1;
		end if;

		if 	((ie_atributo_p = 'DT_INICIO_VIGENCIA') or
			 (ie_atributo_p = 'QT_DIA_INTERNACAO')) and
			(dt_inicio_vigencia_p is not null) and
			(qt_dia_internacao_p is not null) then
			dt_final_vig_ret_p := dt_inicio_vigencia_p +  qt_dia_internacao_p + (nvl(qt_adicional_w,0));
		end if;
	end if;
	
	if	(ie_atributo_p = 'DT_INICIO_VIGENCIA') and
		(qt_dia_internacao_p is null) and
		(dt_inicio_vigencia_p is not null) and
		(dt_final_vig_ret_p is null) and
		(dt_final_vigencia_p is null) then
	
		select	nvl(max(qt_dias_validade_guia),0) 
		into	qt_dias_validade_guia_w
                	from	tiss_parametros_convenio 
                	where	cd_convenio          = cd_convenio_p
                	and	cd_estabelecimento   = cd_estabelecimento_p;
		
		if	(qt_dias_validade_guia_w > 0) then
		
			dt_final_vig_ret_p := dt_inicio_vigencia_p + qt_dias_validade_guia_w;
		end if;
	end if;
end if;

commit;

end eup_focuslost_vigencia_js;
/
