create or replace
procedure fa_gravar_agendamento_pmc(nr_seq_agenda_p		number,
				nr_seq_paciente_pmc_p	number) is 

begin

if (nr_seq_agenda_p is not null) then
	update	agenda_consulta
	set	nr_seq_paciente_pmc = nr_seq_paciente_pmc_p
	where	nr_sequencia = nr_seq_agenda_p;
end if;

commit;

end fa_gravar_agendamento_pmc;
/
