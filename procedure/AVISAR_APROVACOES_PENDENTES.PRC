create or replace 
procedure Avisar_Aprovacoes_Pendentes is

nr_ordem_compra_w		number(10);
ds_comunicado_w			varchar2(4000);
ds_titulo_w			varchar2(255);
nm_usuario_destino_w		varchar2(255);
nm_usuario_comprador_w		varchar2(15);
nr_sequencia_w			number(10);
nr_seq_classif_w			number(10);
ds_cargo_w			varchar2(100);
ds_aprovador_w			varchar2(2000);

cursor c01 is
select	distinct
	c.nr_ordem_compra
from	ordem_compra a,
	processo_aprov_compra b,
	ordem_compra_item c
where	trunc(a.dt_liberacao,'dd') < trunc(sysdate,'dd') 
and	a.dt_aprovacao is null
and	a.dt_baixa is null
and	a.nr_seq_motivo_cancel is null
and	a.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_sequencia = c.nr_seq_aprovacao
and	b.ie_aprov_reprov = 'P'
order by c.nr_ordem_compra;

cursor c02 is
select	distinct
	substr(obter_desc_cargo(b.cd_cargo),1,100) ds_cargo
from	ordem_compra_item c,
	processo_aprov_compra b,
	ordem_compra a
where	trunc(a.dt_liberacao,'dd') < trunc(sysdate,'dd') 
and	a.dt_aprovacao is null
and	a.dt_baixa is null
and	a.nr_ordem_compra = c.nr_ordem_compra
and	b.nr_sequencia = c.nr_seq_aprovacao
and	b.ie_aprov_reprov = 'P'
and	a.nr_ordem_compra = nr_ordem_compra_w
order by ds_cargo;
begin

nm_usuario_destino_w	:= null;
nm_usuario_comprador_w	:= null;
ds_titulo_w		:= wheb_mensagem_pck.get_texto(297613)/*Aviso de Aprova��o Pendente*/;

select	obter_classif_comunic('F')
into	nr_seq_classif_w
from	dual;

open	c01;	
loop
fetch	C01 into
	nr_ordem_compra_w;
exit when c01%notfound;
	begin
	ds_aprovador_w := null;

	open	c02;
	loop	
	fetch	c02 into
		ds_cargo_w;
	exit when c02%notfound;	
		ds_aprovador_w := ds_aprovador_w || ds_cargo_w || chr(10) || chr(13);
	end loop;
	close	c02;
	
	select	nvl(max(a.nm_usuario),'X')
	into  	nm_usuario_comprador_w
	from	usuario a,
		ordem_compra b
	where	a.cd_pessoa_fisica = b.cd_comprador
	and	b.nr_ordem_compra = nr_ordem_compra_w;

	if	nm_usuario_comprador_w <> ('X') then

		nm_usuario_destino_w	:= nm_usuario_comprador_w || ', ';
		
		/*A seguinte ordem de compra encontra-se pendente para aprova��o: #@NR_ORDEM_COMPRA#@. Pelo(s) cargo(s) abaixo:
		#@DS_APROVADORES#@*/
		ds_comunicado_w		:= wheb_mensagem_pck.get_texto(297614, 
						'NR_ORDEM_COMPRA=' || nr_ordem_compra_w || ';DS_APROVADORES=' || ds_aprovador_w);	

		select	comunic_interna_seq.nextval
		into	nr_sequencia_w
		from	dual; 

		insert into comunic_interna(
			dt_comunicado, ds_titulo, ds_comunicado, nm_usuario,
			dt_atualizacao, ie_geral, nm_usuario_destino, nr_sequencia,
			ie_gerencial, nr_seq_classif, dt_liberacao)
		values(
			sysdate, ds_titulo_w, ds_comunicado_w, substr(elimina_acentuacao(wheb_mensagem_pck.get_texto(297615)),1,15) /*Aprovacoes*/,
			sysdate, 'N', nm_usuario_destino_w, nr_sequencia_w, 'N',
			nr_seq_classif_w, sysdate); 
	end if;
	end;
end loop;
close	c01;

commit;

end Avisar_Aprovacoes_Pendentes;
/