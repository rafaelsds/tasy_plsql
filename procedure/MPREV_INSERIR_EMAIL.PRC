create or replace 
procedure mprev_inserir_email (	
				cd_pessoa_fisica_p  pessoa_fisica.cd_pessoa_fisica%type,
				nm_usuario_p		    varchar2,
				ie_email_mprev_p	  pessoa_fisica_aux.ie_email_mprev%type ) is

nr_seq_pessoa_fisica_aux_w	pessoa_fisica_aux.nr_sequencia%type;
ie_possui_aux_w 	          varchar2(1);

begin

if (ie_email_mprev_p is not null) then

  select decode(count(*), 0, 'N', 'S')
  into ie_possui_aux_w
  from pessoa_fisica_aux
  where cd_pessoa_fisica = cd_pessoa_fisica_p;

  if (ie_possui_aux_w = 'N') then

		select	pessoa_fisica_aux_seq.nextval
    into	nr_seq_pessoa_fisica_aux_w
    from	dual;

		insert into pessoa_fisica_aux (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			ie_email_mprev
		) values (
			nr_seq_pessoa_fisica_aux_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_pessoa_fisica_p,
			ie_email_mprev_p
		);

	else 

		update pessoa_fisica_aux p 
		set p.ie_email_mprev = ie_email_mprev_p,
			  p.dt_atualizacao_nrec = sysdate,
			  p.nm_usuario_nrec = nm_usuario_p
		where p.cd_pessoa_fisica = cd_pessoa_fisica_p;

	end if;
  commit;
end if;

end;
/