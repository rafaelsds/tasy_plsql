create or replace
procedure vinc_atendimento_cirurgia(	nr_seq_agenda_p		number,
					nr_atendimento_p	number,
					nm_usuario_p		varchar2)
					is

cd_pessoa_fisica_w	varchar2(10);
nr_cirurgia_w		number(10,0);
cd_estabelecimento_w	number(4,0);
ie_atendimento_w	varchar2(15);
nr_atendimento_w	number(10) := null;
begin

if	(nvl(nr_seq_agenda_p,0) > 0) then

	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;

	if	(nvl(nr_atendimento_w,0) > 0) then

		select	nvl(max(cd_estabelecimento),0)
		into	cd_estabelecimento_w
		from	atendimento_paciente	
		where	nr_atendimento	=	nr_atendimento_w;

		select	nvl(nr_cirurgia,0)
		into	nr_cirurgia_w
		from	agenda_paciente
		where	nr_sequencia	=	nr_seq_agenda_p;

		obter_param_usuario(871,84,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_atendimento_w);

		if	(nr_cirurgia_w > 0) then

			select	cd_pessoa_fisica
			into	cd_pessoa_fisica_w
			from	atendimento_paciente
			where	nr_atendimento	=	nr_atendimento_w;

			update	cirurgia
			set	cd_pessoa_fisica	=	cd_pessoa_fisica_w,
				nr_atendimento		=	decode(ie_atendimento_w,'S',nvl(nr_atendimento,nr_atendimento_w),nr_atendimento),
				nm_usuario		=	nm_usuario_p,
				dt_atualizacao		=	sysdate
			where	nr_cirurgia		=	nr_cirurgia_w
			AND	nr_atendimento is null;
			commit;
			
			if	(ie_atendimento_w = 'S') then
				update	prescr_medica
				set	nr_atendimento	= nvl(nr_atendimento,nr_atendimento_w)
				where	nr_cirurgia	= nr_cirurgia_w
				and 	nvl(ie_tipo_prescr_cirur,0) <> 2;
			end if;	
		end if;
	end if;	
end if;	

end vinc_atendimento_cirurgia;
/