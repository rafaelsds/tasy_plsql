create or replace
procedure Consistir_caixa_local(	nr_seq_caixa_p		number,
					nr_seq_local_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

					
ie_permite_util_caixa_w		varchar2(1);
qt_pront_w			number(10);

begin

select 	max(ie_permite_util_caixa)
into	ie_permite_util_caixa_w
from	same_local
where	nr_sequencia = nr_seq_local_p;


if	(ie_permite_util_caixa_w = 'S') then

	select 	count(*)
	into	qt_pront_w
	from	same_caixa
	where	nr_seq_local = nr_seq_local_p
	and	nr_sequencia <> nr_seq_caixa_p;

	if	(qt_pront_w > 0) then
	
		Wheb_mensagem_pck.exibir_mensagem_abort(264203);
	end if;
end if;
commit;

end Consistir_caixa_local;
/
