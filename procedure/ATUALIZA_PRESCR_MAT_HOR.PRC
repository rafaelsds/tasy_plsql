Create or Replace
procedure Atualiza_prescr_mat_hor(	nr_prescricao_p		number,
					nr_seq_item_p		number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2,
					ie_lib_parc_p		varchar2 default null,
					ie_tipo_item_p		varchar2 default null,
					nr_seq_horario_p	number default null) is

ajustar_disp_hor_farm_w	varchar2(1);
sqlerrm_w 				varchar2(2000);
ie_gera_nutricao_w		varchar2(1);
ie_adep_param_w			varchar2(2);
ie_adep_w				varchar2(2)	:= 'X';
cd_setor_atendimento_w  number(10);
cd_estabelecimento_w    number(4);
VarIdentPrescrADEP_w	varchar2(5);
ie_atualiza_adep_w		varchar2(1);
nr_sequencia_mat_kit_w	number(10);
ie_lote_pos_lib_w		varchar2(1);
dt_lib_horario_w		date;
ie_param768_w			varchar2(1);
nr_sequencia_w			prescr_material.nr_sequencia%type;
nr_seq_kit_w			prescr_material.nr_sequencia%type;
/* Somente para liberar os Kits do medicamento principal */ 

ie_info_rastre_prescr_w	varchar2(1 char);
ds_alteracao_rastre_w	varchar2(1800);
 
cursor c01 is
	select	nr_sequencia
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_kit = nr_seq_item_p
	and	ie_agrupador = 2;
	
Cursor C02 is
	select	a.nr_sequencia
	from	prescr_material a
	where	a.nr_prescricao = nr_prescricao_p
	and		(((nr_seq_item_p is null) and
	          (a.nr_sequencia_solucao is not null)) or 
			 (a.nr_sequencia_solucao = nr_seq_item_p))
	and		a.dt_suspensao is null
	and		nvl(a.ie_suspenso,'N') = 'N'
	and		nvl(a.ie_acm,'N') = 'N'
	and		nvl(a.ie_se_necessario,'N') = 'N'
	and		nvl(a.ie_gerar_horario,'S') <> 'N'
	and	exists (	select	1
					from	prescr_material b
					where	b.nr_seq_inconsistencia is null
					and	b.nr_sequencia		=	a.nr_sequencia 
					and	b.nr_prescricao		=	a.nr_prescricao )
	and	not exists(	select 	1
					from	prescr_material_incon_farm c
					where	nvl(c.ie_situacao,'A')  = 'A'
					and		c.nr_seq_material 	= a.nr_sequencia
					and		c.nr_prescricao 	= a.nr_prescricao );
					
Cursor C03 is
	select	nr_sequencia
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and		nr_seq_kit = nr_sequencia_w;

begin	

ie_info_rastre_prescr_w := 'N';
obter_param_usuario(924,768,cd_perfil_p,nm_usuario_p,cd_estabelecimento_w,ie_param768_w);

begin

	Select	max(ie_adep),
		max(cd_estabelecimento),
		max(cd_setor_atendimento)
	into	ie_adep_param_w,
		cd_estabelecimento_w,
		cd_setor_atendimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;

	select	nvl(max(ie_lote_pos_lib),'N')
	into	ie_lote_pos_lib_w
	from	parametro_medico
	where	cd_estabelecimento	= cd_estabelecimento_w;

	select	decode(count(nr_sequencia),0,'N','S')
	into	ie_atualiza_adep_w
	from	prescr_mat_hor
	where	dt_lib_horario is null
	and	dt_suspensao is not null
	and	((nr_seq_material = nr_seq_item_p) or
		 (nvl(nr_seq_item_p,0) = 0) or (nr_seq_superior = nr_seq_item_p) )
	and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
	and	nr_prescricao = nr_prescricao_p;
	
exception when others then
	sqlerrm_w	:= substr(sqlerrm,1,1500);
	gerar_log_prescricao(nr_prescricao_p, null, null, null, null, sqlerrm_w, nm_usuario_p, 4404, 'S');
end;

if	(ie_atualiza_adep_w	= 'S') then

	obter_param_usuario(924,246,cd_perfil_p,nm_usuario_p,cd_estabelecimento_w,VarIdentPrescrADEP_w);

	if	(VarIdentPrescrADEP_w = 'NV') then
		ie_adep_w := 'N';
	elsif	(VarIdentPrescrADEP_w = 'SV') then
		ie_adep_w := 'S';
	elsif	(VarIdentPrescrADEP_w = 'PV') or 
		(VarIdentPrescrADEP_w = 'PNV') then
		ie_adep_w := ie_adep_param_w;
	elsif (VarIdentPrescrADEP_w = 'DS') then
		ie_adep_w := nvl(ie_adep_param_w,obter_se_setor_adep(cd_setor_atendimento_w));
	end if;

	if	(ie_adep_w <> 'X') then

		begin
	
			update	prescr_mat_hor
			set	ie_adep = ie_adep_w
			where	nr_prescricao = nr_prescricao_p
			and	ie_adep <> ie_adep_w
			and	dt_lib_horario is null
			and	((nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0) or (nr_seq_superior = nr_seq_item_p) )
			and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
			and	dt_suspensao is not null;
			
		exception when others then
			sqlerrm_w	:= substr(sqlerrm,1,2000);
			gerar_log_prescricao(nr_prescricao_p, null, null, null, null, sqlerrm_w, nm_usuario_p, 4404, 'S');
		end;

	end if;

end if;

adep_gerar_area_prep(nr_prescricao_p, nr_seq_item_p, nm_usuario_p);

begin

	if	(ie_tipo_item_p is not null) then
		
		if	(ie_tipo_item_p = 'M') then
			
			dt_lib_horario_w := sysdate;
			
			update	prescr_mat_hor
			set	dt_lib_horario	= dt_lib_horario_w
			where	dt_lib_horario is null
			and	((nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0) or (nr_seq_superior = nr_seq_item_p) )
			and 	ie_agrupador in (1,3,7,9)
			and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
			and	nr_prescricao = nr_prescricao_p;
			
			if	(nvl(nr_seq_item_p,0) > 0) and 
				(nr_seq_horario_p is null) then
				open c01;
				loop
				fetch c01 into nr_sequencia_mat_kit_w;
				exit when c01%notfound;
					update	prescr_mat_hor
					set	dt_lib_horario	= dt_lib_horario_w
					where	dt_lib_horario is null
					and	nr_seq_material = nr_sequencia_mat_kit_w
					and	ie_agrupador	= 2
					and	nr_prescricao = nr_prescricao_p;
				end loop;
				close c01;
			end if;
		elsif	(ie_tipo_item_p = 'MAT') then
			update	prescr_mat_hor
			set	dt_lib_horario	= sysdate
			where	dt_lib_horario is null
			and	((nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0) or
				 (nr_seq_superior = nr_seq_item_p))
			and	ie_agrupador	= 2
			and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
			and	nr_prescricao = nr_prescricao_p;

		elsif	(ie_tipo_item_p IN ('P', 'IA')) then
			update	prescr_mat_hor
			set	dt_lib_horario	= sysdate
			where	dt_lib_horario is null
			and	((nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0))
			and	ie_agrupador	= 5
			and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
			and	nr_prescricao = nr_prescricao_p;
			
		elsif	(ie_tipo_item_p = 'S') then
		
			update	prescr_mat_hor a
			set	a.dt_lib_horario	= sysdate
			where	a.dt_lib_horario is null
			and	((a.nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0))	
			and	a.ie_agrupador in (4,13)
			and	a.nr_sequencia	= nvl(nr_seq_horario_p,a.nr_sequencia)
			and	exists (	select	1
							from	prescr_material b
							where	b.nr_seq_inconsistencia is null
							and	b.nr_sequencia		=	a.nr_seq_material 
							and	b.nr_prescricao		=	a.nr_prescricao )
			and	not exists(	select 	1
							from	prescr_material_incon_farm c
							where	nvl(c.ie_situacao,'A')  = 'A'
							and		c.nr_seq_material 	= a.nr_seq_material
							and		c.nr_prescricao 	= a.nr_prescricao )
			and	a.nr_prescricao = nr_prescricao_p;
			
			if (nvl(ie_param768_w,'N') = 'S' or ie_lib_parc_p = 'S') then
			
				open C02;
				loop
				fetch C02 into	
					nr_sequencia_w;
				exit when C02%notfound;
					begin
					
						update	prescr_mat_hor
						set		dt_lib_horario	= sysdate
						where	dt_lib_horario is null
						and 	nr_prescricao = nr_prescricao_p
						and 	nr_seq_material = nr_sequencia_w;
						
						open C03;
						loop
						fetch C03 into	
							nr_seq_kit_w;
						exit when C03%notfound;
							begin

								update	prescr_mat_hor
								set		dt_lib_horario	= sysdate
								where	dt_lib_horario is null
								and 	nr_prescricao = nr_prescricao_p
								and 	nr_seq_material = nr_seq_kit_w;
							end;
						end loop;
						close C03;
					end;
				end loop;
				close C02;
			
			end if;
			
		elsif	(ie_tipo_item_p = 'SNE') then
			update	prescr_mat_hor
			set	dt_lib_horario	= sysdate
			where	dt_lib_horario is null
			and	((nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0))
			and	ie_agrupador	= 8
			and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
			and	nr_prescricao = nr_prescricao_p;
			
		elsif	(ie_tipo_item_p = 'SUP') then
			update	prescr_mat_hor
			set	dt_lib_horario	= sysdate
			where	dt_lib_horario is null
			and	((nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0))
			and	ie_agrupador	= 12
			and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
			and	nr_prescricao = nr_prescricao_p;	
			
		elsif	(ie_tipo_item_p = 'LD') then
			update	prescr_mat_hor
			set	dt_lib_horario	= sysdate
			where	dt_lib_horario is null
			and	((nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0))
			and	ie_agrupador	in (16,17)
			and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
			and	nr_prescricao = nr_prescricao_p;
		
		elsif	(ie_tipo_item_p = 'GAS') then
			update	prescr_mat_hor
			set	dt_lib_horario	= sysdate
			where	dt_lib_horario is null
			and	((nr_seq_material = nr_seq_item_p) or
				 (nvl(nr_seq_item_p,0) = 0))
			and	ie_agrupador	= 15
			and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
			and	nr_prescricao = nr_prescricao_p;
		
		end if;
		
	elsif	(nvl(ie_lib_parc_p,'N') = 'N') then
		update	prescr_mat_hor
		set	dt_lib_horario	= sysdate
		where	dt_lib_horario is null
		and	((nr_seq_material = nr_seq_item_p) or (nvl(nr_seq_item_p,0) = 0))
		and	nr_sequencia	= nvl(nr_seq_horario_p,nr_sequencia)
		and	nr_prescricao 	= nr_prescricao_p;
	else
		
		update	prescr_mat_hor a
		set		a.dt_lib_horario	= sysdate
		where	a.dt_lib_horario is null
		and		exists (	select	1
							from	prescr_material b
							where	b.nr_seq_inconsistencia is null
							and	b.nr_sequencia				=	a.nr_seq_material 
							and	b.nr_prescricao				=	a.nr_prescricao )
		and		not exists(	select 	1
							from	prescr_material_incon_farm c
							where	nvl(c.ie_situacao,'A')  = 'A'
							--and		c.nr_seq_material 	= a.nr_seq_material
							and		((c.nr_seq_material 	= a.nr_seq_material) or
									(c.nr_seq_material = a.nr_seq_superior))
							and		c.nr_prescricao 	= a.nr_prescricao )
		and		((a.nr_seq_solucao is null) or
				 (not exists( 	select 	1
								from	prescr_material_incon_farm y
								where	y.nr_prescricao 	= a.nr_prescricao
								and		y.nr_seq_solucao 	= a.nr_seq_solucao
								and		nvl(y.ie_situacao,'A') 	= 'A' ) ) )
		and		((nr_seq_material = nr_seq_item_p) or (nvl(nr_seq_item_p,0) = 0))
		and		nr_sequencia		= nvl(nr_seq_horario_p,nr_sequencia)
		and		a.nr_prescricao 	= nr_prescricao_p;
		
	end if;

	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;	
	
exception when others then
	sqlerrm_w	:= substr(sqlerrm,1,2000);
	gerar_log_prescricao(nr_prescricao_p, null, null, null, null, sqlerrm_w, nm_usuario_p, 4404, 'S');
end;

begin

obter_param_usuario(924,179,cd_perfil_p,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ajustar_disp_hor_farm_w);
ajustar_disp_hor_farm_w	:= nvl(ajustar_disp_hor_farm_w, 'N');

begin
if	(ajustar_disp_hor_farm_w <> 'N') then
	calcular_dispensar_horario(nr_prescricao_p,0);
end if;

exception when others then
	ajustar_disp_hor_farm_w := 'N';
end;

begin
	obter_param_usuario(924, 545, cd_perfil_p, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gera_nutricao_w);
	if (ie_gera_nutricao_w = 'S') then
		gerar_servico_nut_pep(nr_prescricao_p,nm_usuario_p,cd_perfil_p);
	end if;
exception when others then
	ie_gera_nutricao_w	:= 'N';
end;

ie_info_rastre_prescr_w := obter_se_info_rastre_prescr( 'L', nm_usuario_p, cd_perfil_p, cd_estabelecimento_w);

if 	(ie_info_rastre_prescr_w = 'S') then
	ds_alteracao_rastre_w := substr('Gerar log Rastreabilidade Alteracoes / Atualiza_prescr_mat_hor = ' || 'NR_PRESCRICAO_P: ' || nr_prescricao_p || ' IE_LOTE_POS_LIB_W: ' ||  ie_lote_pos_lib_w || ' NR_SEQ_ITEM_P: ' || nvl(nr_seq_item_p,0),1,1800);
	Gerar_log_prescr_mat(nr_prescricao_p, nr_seq_item_p, null, null, null, ds_alteracao_rastre_w, nm_usuario_p, 'N');
end if;

if	(ie_lote_pos_lib_w	= 'N') then
	if	(nvl(nr_seq_item_p,0) = 0) then
		Gerar_Lote_Atend_Prescricao(nr_prescricao_p, 0, 0, 'N', nm_usuario_p, 'GPMH');
	else
		Gerar_Lote_Atend_Prescricao(nr_prescricao_p, nr_seq_item_p, 0, 'N', nm_usuario_p, 'GPMH');
	end if;
end if;

exception
	when others then
	sqlerrm_w	:= substr(sqlerrm,1,2000);
	gerar_log_prescricao(nr_prescricao_p, null, null, null, null, sqlerrm_w, nm_usuario_p, 4404, 'S');
end;

end Atualiza_prescr_mat_hor;
/
