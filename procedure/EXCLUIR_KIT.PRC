create or replace
procedure excluir_kit(	nr_prescricao_p		number,
			cd_kit_material_p		number,
			nr_seq_kit_estoque_p	number ) as

begin

if	(cd_kit_material_p > 0) then
	delete	from prescr_material
	where	nr_prescricao	= nr_prescricao_p
	and	cd_kit_material	= cd_kit_material_p
	and	cd_motivo_baixa	= 0;
	commit;
elsif	(nr_seq_kit_estoque_p > 0) then
	if (obter_se_exclui_kit(nr_prescricao_p, nr_seq_kit_estoque_p) = 'S') then
		update	kit_estoque 
		set	nm_usuario_util	= null,
					dt_utilizacao	= null,
			nr_atendimento	= null,
			nr_prescricao	= null,
			nr_cirurgia	= null
		where 	nr_sequencia	= nr_seq_kit_estoque_p;

		delete	from prescr_material
		where	nr_prescricao 	= nr_prescricao_p 
		and	nr_seq_kit_estoque	= nr_seq_kit_estoque_p
		and	cd_motivo_baixa	= 0;
		commit;
	else
		wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(1015578);
	end if;
	
end if;

end excluir_kit;
/