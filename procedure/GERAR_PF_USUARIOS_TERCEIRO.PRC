create or replace
procedure gerar_pf_usuarios_terceiro
			(nr_seq_terceiro_p	in number,
			cd_perfil_p		in number,
			nm_usuario_p		in varchar2) is

cd_pessoa_fisica_w	varchar2(255);

Cursor C01 is
	select	a.cd_pessoa_fisica
	from	usuario a
	where	a.cd_pessoa_fisica is not null
	and	a.ie_situacao = 'A'
	and	not exists (
			select	1 
			from	terceiro_pessoa_consulta x
			where	x.nr_seq_terceiro	= nr_seq_terceiro_p
			and	x.cd_perfil = cd_perfil_p
			and	x.cd_pessoa_fisica = a.cd_pessoa_fisica)
	order by a.cd_pessoa_fisica;

begin

open C01;
loop
fetch C01 into	
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	insert into terceiro_pessoa_consulta (	
		nr_sequencia,           
		dt_atualizacao,         
		nm_usuario,             
		dt_atualizacao_nrec,
		nm_usuario_nrec,        
		nr_seq_terceiro,        
		cd_pessoa_fisica,       
		cd_perfil)
	values(	terceiro_pessoa_consulta_seq.nextval,           
		sysdate,         
		nm_usuario_p,             
		sysdate,
		nm_usuario_p,        
		nr_seq_terceiro_p,        
		cd_pessoa_fisica_w,       
		cd_perfil_p);	
	end;
end loop;
close C01;

commit;

end gerar_pf_usuarios_terceiro;
/
