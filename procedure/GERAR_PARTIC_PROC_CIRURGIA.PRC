create or replace
procedure gerar_partic_proc_cirurgia(	nr_cirurgia_p		number,
				cd_estabelecimento_p	number,
				nr_sequencia_p		number,
				cd_convenio_p		number,
				cd_categoria_p		varchar2,
				cd_procedimento_p		number,
				ie_origem_proced_p	number,
				dt_conta_p			date,
				nm_usuario_p		varchar2) is

dt_atualizacao_w			date 			:= sysdate;
cd_funcao_w			varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_sequencia_w			number(10);
nr_seq_partic_w			number(10);
ie_emite_conta_w           		varchar2(01);
cd_especialidade_w		number(05,0);
ie_partic_cirurgia_w			number(2);
qt_porte_atual_w			number(2) := 0;
qt_maior_porte_w			number(2) := 0;

cd_estrutura_w			number(5) 		:= 0;
cd_estrutura_honor_w		number(5) 		:= 0;

nr_atendimento_w			number(10,0);
dt_entrada_unidade_w		date;
cd_setor_atendimento_w		number(5,0);
nr_seq_interno_w			number(10,0);
ie_tipo_atendimento_w		number(3,0);
ie_medico_executor_w   		varchar2(1);
cd_cgc_retorno_w			varchar2(14);
cd_medico_retorno_w		varchar2(10);
cd_pessoa_fisica_ww		varchar2(10);
cd_cbo_w			varchar2(6);
ie_doc_executor_w			number(1);
nr_seq_regra_w			number(10,0)	:= null;
ie_responsavel_credito_w		varchar2(5);
cd_cgc_prestador_w		varchar2(14);

nr_seq_classificacao_w		number(10);
ie_participou_sus_w		sus_regra_func_partic_cir.ie_participou_sus%type := 'S';

cursor c01 is
select		a.ie_funcao,
		a.cd_pessoa_fisica,
		a.nr_sequencia + 1000
from		cirurgia_participante a
where		a.nr_cirurgia 		= nr_cirurgia_p
and		(ie_partic_cirurgia_w	= 0 or qt_maior_porte_w < qt_porte_atual_w)
order by ie_funcao;

begin

if	(nr_cirurgia_p > 0) then
	select	max(nr_atendimento),
		max(dt_entrada_unidade),
		max(obter_tipo_atendimento(nr_atendimento))
	into	nr_atendimento_w,
		dt_entrada_unidade_w,
		ie_tipo_atendimento_w
	from	cirurgia
	where	nr_cirurgia = nr_cirurgia_p;

	select	max(cd_setor_atendimento),
		max(nr_seq_interno)
	into	cd_setor_atendimento_w,
		nr_seq_interno_w
	from	atend_paciente_unidade
	where	dt_entrada_unidade	=	dt_entrada_unidade_w
	and	nr_atendimento		=	nr_atendimento_w;
	
	nr_seq_classificacao_w := obter_classif_atendimento(nr_atendimento_w);
	
end if;

select nvl(max(vl_parametro_padrao),'N')
into   ie_emite_conta_w
from   funcao_parametro
where  cd_funcao = 81
  and  nr_sequencia = 18;

select	nvl(max(obter_valor_conv_estab(cd_convenio, cd_estabelecimento_p, 'IE_PARTIC_CIRURGIA')), 0)
into	ie_partic_cirurgia_w
from	convenio
where	cd_convenio = cd_convenio_p;

if	(ie_partic_cirurgia_w > 0) then
	select	nvl(max(qt_porte_anestesico),0)
	into	qt_porte_atual_w
	from	procedimento_paciente
	where	nr_sequencia = nr_sequencia_p;

	select	nvl(max(qt_porte_anestesico),0)
	into	qt_maior_porte_w
	from	procedimento_paciente
	where	nr_cirurgia	= nr_cirurgia_p
	  and	nr_sequencia	<> nr_sequencia_p;
end if;

open c01;
loop
fetch c01 into
	cd_funcao_w,
	cd_pessoa_fisica_w,
	nr_seq_partic_w;
exit when c01%notfound;
	begin
	if	(ie_partic_cirurgia_w = 1) and
		(qt_maior_porte_w < qt_porte_atual_w) then
		delete procedimento_participante
		where ie_funcao		= cd_funcao_w
		  and nr_sequencia	in (	select nr_sequencia
						from procedimento_paciente
						where nr_cirurgia = nr_cirurgia_p
						  and qt_porte_anestesico = qt_maior_porte_w);
	end if;
	if	(obter_regra_funcao_partic(
			cd_estabelecimento_p,
			cd_convenio_p,
			cd_categoria_p,
			cd_procedimento_p,
			ie_origem_proced_p,
			dt_conta_p,
			nr_sequencia_p,
			cd_funcao_w) = 'S') then
		begin
		select 	nr_seq_partic
		into	nr_seq_partic_w
		from 	procedimento_participante
		where	nr_sequencia 		= nr_sequencia_p
		and	ie_funcao		= cd_funcao_w
		and	cd_pessoa_fisica 	= cd_pessoa_fisica_w;
		exception
		when no_data_found then
			begin
			select obter_especialidade_medico(cd_pessoa_fisica_w,'C')
			into cd_especialidade_w
			from dual;


			begin
			if	(ie_origem_proced_p not in(2,3))	then
				begin
				cd_estrutura_w	:= 0;
				obter_estrut_conta_proc(
						cd_convenio_p,
						cd_procedimento_p,
						ie_origem_proced_p,
						null,
						cd_pessoa_fisica_w,
						null,
						nr_sequencia_p,
						0,
						0,
						cd_estabelecimento_p,
						cd_especialidade_w,
						cd_categoria_p,
						dt_conta_p,
						null,
						cd_estrutura_w,
						cd_estrutura_honor_w);

				if	(cd_estrutura_honor_w	> 0) then
					ie_emite_conta_w	:= cd_estrutura_honor_w;
				end if;
				end;
			end if;
			exception
				when others then
					ie_emite_conta_w	:= ie_emite_conta_w;
			end;
			
			
			consiste_medico_executor(	cd_estabelecimento_p,cd_convenio_p,cd_setor_atendimento_w,cd_procedimento_p,ie_origem_proced_p,ie_tipo_atendimento_w,null,
							null,ie_medico_executor_w,cd_cgc_retorno_w,cd_medico_retorno_w,cd_pessoa_fisica_ww,
							cd_pessoa_fisica_w,nvl(dt_conta_p,sysdate),nr_seq_classificacao_w,'N',null,
							null);
							
			if	(ie_origem_proced_p = 7) and
				(cd_pessoa_fisica_w is not null) then
				begin
				
				
				
				select	nvl(max(ie_responsavel_credito),''),
					nvl(max(cd_cgc_prestador),'')
				into	ie_responsavel_credito_w,
					cd_cgc_prestador_w
				from	procedimento_paciente 
				where	nr_sequencia	= nr_sequencia_p;				
				
				cd_cbo_w := sus_obter_cbo_medico(cd_pessoa_fisica_w, cd_procedimento_p, dt_conta_p, sus_obter_indicador_equipe(cd_funcao_w));
				
				sus_atualiza_doc_exec
					(cd_procedimento_p,
					ie_origem_proced_p,
					cd_pessoa_fisica_w,
					cd_pessoa_fisica_w,
					cd_cgc_prestador_w,
					cd_estabelecimento_p,
					cd_convenio_p,
					ie_responsavel_credito_w,
					cd_funcao_w,
					cd_cbo_w,
					ie_doc_executor_w,
					nr_seq_regra_w);					
				
				end;
			end if;
			
			if	(ie_origem_proced_p = 7) then
				ie_participou_sus_w := nvl(SUS_OBTER_SE_FUNC_PART_CIR(cd_procedimento_p,ie_origem_proced_p,cd_funcao_w),'S');
			end if;
			
			insert into procedimento_participante
				(nr_sequencia, nr_seq_partic, ie_funcao, dt_atualizacao, 
				nm_usuario,cd_pessoa_fisica, cd_cgc, ie_valor_informado,
				ie_emite_conta, vl_participante, vl_conta, nr_lote_contabil,
				nr_conta_medico, cd_especialidade,cd_cbo,ie_doc_executor,ie_participou_sus)
			values
				(nr_sequencia_p, nr_seq_partic_w, cd_funcao_w, dt_atualizacao_w,
				nm_usuario_p, cd_pessoa_fisica_w, cd_cgc_retorno_w, 'N', 
				ie_emite_conta_w, 0, 0, 0, 0, cd_especialidade_w,cd_cbo_w,ie_doc_executor_w,ie_participou_sus_w);
			exception
			when others then
				nr_seq_partic_w	:= nr_seq_partic_w;
			end;
		end;
	end if;
	end;
end loop;
close c01;

end gerar_partic_proc_cirurgia;
/
