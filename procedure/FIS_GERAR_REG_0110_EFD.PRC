create or replace
procedure fis_gerar_reg_0110_efd(	nr_seq_controle_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				ds_separador_p		varchar2,
				qt_linha_p	in out	number,
				nr_sequencia_p	in out	number) is 

nr_seq_regra_efd_w		number(10);
nr_versao_efd_w			varchar2(5);
tp_registro_w			varchar2(4);
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w			number(10) := nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1)	:= ds_separador_p;
nr_versao_w			number(5);
ie_crit_apur_w			varchar2(1);

cursor c01 is
select	'0110' tp_registro,
	a.ie_ind_incidencia_trib,
	a.ie_ind_metodo_apropriacao,
	a.ie_tipo_contribuicao,
	a.ie_atividade_pj,
	a.ie_ind_reg_cum
from	fis_efd_controle b,
	fis_regra_efd a
where	a.cd_empresa	= cd_empresa_p
and	a.nr_sequencia	= b.nr_seq_regra_efd
and	b.nr_sequencia	= nr_seq_controle_p;

vet01	C01%RowType;

begin

select	nr_versao_efd
into	nr_versao_w
from	fis_regra_efd a,
	fis_efd_controle b
where	a.nr_sequencia = b.NR_SEQ_REGRA_EFD
and	b.nr_sequencia = nr_seq_controle_p;

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin	
	
	ds_linha_w := 	sep_w || vet01.tp_registro					|| 
					sep_w || vet01.ie_ind_incidencia_trib		||
					sep_w || vet01.ie_ind_metodo_apropriacao	||
					sep_w || vet01.ie_tipo_contribuicao			|| 
					sep_w;
	
	if (nr_versao_w > 2) then
		ds_linha_w := 	ds_linha_w || 
						vet01.ie_ind_reg_cum || 
						sep_w;
	end if;
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_efd_arquivo(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_efd,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values(	nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		vet01.tp_registro,
		ds_arquivo_w,
		ds_arquivo_compl_w);
	
	if (((vet01.ie_ind_incidencia_trib = '1') or (vet01.ie_ind_incidencia_trib = '3')) and (vet01.ie_ind_metodo_apropriacao = '2')) then
	
		fis_gerar_reg_0111_efd(	nr_seq_controle_p,
					nm_usuario_p,
					cd_estabelecimento_p,
					dt_inicio_p,
					dt_fim_p,
					cd_empresa_p,
					ds_separador_p,
					nr_linha_w,
					nr_seq_registro_w);	
	end if;
	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;	

end fis_gerar_reg_0110_efd;
/