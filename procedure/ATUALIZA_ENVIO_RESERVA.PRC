create or replace 
PROCEDURE Atualiza_envio_reserva(
			nr_seq_viagem_p		number,
			ds_email_destino_p	varchar2,
			ds_observacao_p		varchar2,
			ds_local_p		varchar2,
			nm_usuario_p		varchar2) is

nr_seq_reserva_w	 Number(10);

Cursor C01 is
	select	nr_sequencia
	from	via_reserva
	where	nr_seq_viagem = nr_seq_viagem_p;

BEGIN


Insert into via_envio(
		NR_SEQUENCIA, 
		NR_SEQ_VIAGEM,       
		DT_ATUALIZACAO,
		NM_USUARIO,          
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DT_ENVIO,
		IE_TIPO_ENVIO,
		DS_DESTINO,
		DS_OBSERVACAO)
values  	(via_envio_seq.nextval,
		nr_seq_viagem_p,
		sysdate,
		nm_usuario_p,
		sysdate, 
		nm_usuario_p,
		sysdate, 
		'E',
		ds_email_destino_p,
		ds_observacao_p);

OPEN C01;
LOOP
FETCH C01 into
	nr_seq_reserva_w;
exit when c01%notfound;
	begin
	insert into  via_reserva_anexo 
				(NR_SEQUENCIA,
				NR_SEQ_RESERVA,
				DT_ATUALIZACAO,
				NM_USUARIO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO_NREC,
				DS_ARQUIVO)
		values		(via_reserva_anexo_seq.NextVal,
				nr_seq_reserva_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_local_p);
	end;
END LOOP;
CLOSE C01;

commit;

END Atualiza_envio_reserva;
/