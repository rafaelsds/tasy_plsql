create or replace
procedure atualizar_liberacao_darf(	nr_seq_darf_p	number,
				ie_liberar_p	varchar2) is 

begin
if	(nr_seq_darf_p	is not null) then
	begin
	if	(ie_liberar_p = 'S') then
		update	darf
		set	dt_liberacao	= sysdate
		where	nr_sequencia	= nr_seq_darf_p;
	else
		update	darf
		set	dt_liberacao	= null
		where	nr_sequencia	= nr_seq_darf_p;
	end if;
	commit;
	end;
end if;
end atualizar_liberacao_darf;
/