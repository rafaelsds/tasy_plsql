create or replace
procedure pls_obter_regra_fluxo_analise
			(	nr_seq_grupo_seg_p	in	number,
				ie_situacao_itens_p	in	varchar2,
				ie_tipo_conta_p		in	varchar2,
				nr_seq_regra_p		out	number) is 
	
nr_seq_regra_w		number(10);

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

Cursor C01 is
	select	a.nr_sequencia
	from	pls_regra_fluxo_analise	a
	where	a.nr_seq_grupo_seguinte	= nr_seq_grupo_seg_p
	and	a.ie_situacao_itens	= ie_situacao_itens_p
	and	((nvl(a.ie_tipo_conta,'T') = ie_tipo_conta_p) or (nvl(a.ie_tipo_conta,'T') = 'T'))
	and	sysdate between dt_inicio_vigencia and nvl(a.dt_fim_vigencia, sysdate)
	order by
		a.nr_sequencia;

begin
open C01;
loop
fetch C01 into	
	nr_seq_regra_w;
exit when C01%notfound;
	begin
	null;
	end;
end loop;
close C01;

nr_seq_regra_p	:= nr_seq_regra_w;

end pls_obter_regra_fluxo_analise;
/