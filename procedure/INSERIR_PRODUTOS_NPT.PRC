create or replace
procedure Inserir_Produtos_Npt(
				nr_sequencia_p	number,
				nr_seq_mat_p	number,
				nm_usuario_p	varchar2) is 

cd_material_w			protocolo_npt_prod.cd_material%type;
qt_volume_w				protocolo_npt_prod.qt_volume%type;
qt_dose_w				protocolo_npt_prod.qt_dose%type;
cd_unidade_medida_w		protocolo_npt_prod.cd_unidade_medida%type;
ie_prod_adicional_w		protocolo_npt_prod.ie_prod_adicional%type;
ie_somar_volume_w		protocolo_npt_prod.ie_somar_volume%type;
nr_seq_protocolo_w		protocolo_npt_prod.nr_seq_protocolo%type;
nr_seq_elemento_w		protocolo_npt_item.nr_seq_elemento%type;
qt_elemento_w			protocolo_npt_item.qt_elemento%type;
ie_prim_fase_w			protocolo_npt_item.ie_prim_fase%type;
ie_seg_fase_w			protocolo_npt_item.ie_seg_fase%type;
ie_terc_fase_w			protocolo_npt_item.ie_terc_fase%type;
ie_quar_fase_w			protocolo_npt_item.ie_quar_fase%type;
qt_osmolaridade_w		protocolo_npt_item.qt_osmolaridade%type;

begin

select	max(cd_material),
		max(cd_unidade_medida),
		max(ie_prod_adicional),
		nvl(max(ie_somar_volume),'S'),
		max(qt_dose),
		max(qt_volume),
		max(nr_seq_protocolo)
into	cd_material_w,
		cd_unidade_medida_w,
		ie_prod_adicional_w,
		ie_somar_volume_w,
		qt_dose_w,
		qt_volume_w,
		nr_seq_protocolo_w
from	protocolo_npt_prod
where	nr_sequencia = nr_seq_mat_p;


insert into nut_pac_elem_mat(
			nr_sequencia,
			nr_seq_nut_pac,
			cd_material,
			qt_volume,
			qt_protocolo,
			qt_dose,
			cd_unidade_medida,
			ie_prod_adicional,
			ie_somar_volume,
			dt_atualizacao,
			nm_usuario)
		values(	
			nut_pac_elem_mat_seq.nextval,
			nr_sequencia_p,
			cd_material_w,
			qt_volume_w,
			qt_volume_w,
			qt_dose_w,
			cd_unidade_medida_w,
			ie_prod_adicional_w,
			ie_somar_volume_w,
			sysdate,
			nm_usuario_p);
			
select	nvl(max(a.nr_seq_elemento),0),
		max(a.cd_unidade_medida),
		max(a.qt_elemento),
		nvl(max(a.ie_prim_fase),'N'),
		nvl(max(a.ie_seg_fase),'N'),
		nvl(max(a.ie_terc_fase),'N'),
		nvl(max(a.ie_quar_fase),'N'),
		max(a.qt_osmolaridade),
		max(a.ie_prod_adicional)
into	nr_seq_elemento_w,
		cd_unidade_medida_w,
		qt_elemento_w,
		ie_prim_fase_w,
		ie_seg_fase_w,
		ie_terc_fase_w,
		ie_quar_fase_w,
		qt_osmolaridade_w,
		ie_prod_adicional_w
from	protocolo_npt_item a,
		nut_elem_material b
where	a.nr_seq_elemento = b.nr_seq_elemento
and		b.cd_material = cd_material_w
and		a.nr_seq_protocolo = nr_seq_protocolo_w
and		nvl(a.ie_prod_adicional,'N') = 'S';

if	(nr_seq_elemento_w > 0) then
	insert into nut_pac_elemento(
				nr_sequencia,           
				nr_seq_nut_pac,
				nr_seq_elemento,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida,
				qt_elem_kg_dia,
				qt_diaria,
				pr_total,
				qt_kcal,
				ie_prim_fase,
				ie_seg_fase,
				ie_terc_fase,
				ie_quar_fase,
				ie_npt,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				qt_osmolaridade,
				qt_protocolo,
				ie_prod_adicional)
			values(	
				nut_pac_elemento_seq.nextval,
				nr_sequencia_p,
				nr_seq_elemento_w,
				sysdate,
				nm_usuario_p,
				cd_unidade_medida_w,
				0,
				qt_elemento_w,
				0,
				0,
				ie_prim_fase_w,
				ie_seg_fase_w,
				ie_terc_fase_w,
				ie_quar_fase_w,
				'S',
				sysdate,
				nm_usuario_p,
				qt_osmolaridade_w,
				qt_elemento_w,
				ie_prod_adicional_w);
end if;

commit;

end Inserir_Produtos_Npt;
/