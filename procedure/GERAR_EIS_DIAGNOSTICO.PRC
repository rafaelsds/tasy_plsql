CREATE OR REPLACE
PROCEDURE Gerar_EIS_Diagnostico
		(dt_parametro_P   	DATE,
  		NM_USUARIO_P      	VARCHAR2) IS 

dt_atualizacao_w		date          := SYSDATE;
dt_parametro_w			date;
nr_atendimento_w		NUMBER(10);
cd_medico_w			VARCHAR2(10);
cd_procedencia_w		NUMBER(5);
ie_clinica_w			NUMBER(5);
cd_convenio_w			NUMBER(5);
qt_Obito_w			NUMBER(5)	:= 0;
cd_estabelecimento_w		NUMBER(4)   := 0;
ds_retorno_w			Varchar2(40); 
ie_diagnostico_w		Varchar2(01);
cd_doenca_w			VARCHAR2(10);
cd_categoria_doenca_w		VARCHAR2(10);
ie_tipo_atendimento_w		Number(03,0);
dt_diagnostico_w		Date;
dt_diag_w		Date;
cd_setor_atendimento_w		number(5,0);
ie_tipo_diagnostico_w		number(10,0);
nm_usuario_w				varchar2(30);
ie_profissional_w			varchar2(255);
nr_sequencia_w				number(10);
qt_idade_w			varchar2(10);	
ie_sexo_w			varchar2(1);
ie_faixa_etaria_w		varchar2(10);

CURSOR C01 IS
	select 	a.nr_atendimento,
		a.cd_estabelecimento,
		a.cd_procedencia,
       		a.ie_clinica,
		a.ie_tipo_atendimento,
              	a.cd_medico_resp,
               	decode(b.ie_obito,'S',1,0),
		obter_convenio_atendimento(nr_atendimento),
		obter_setor_atendimento(a.nr_atendimento)
	from	motivo_alta b, 
		atendimento_paciente a   
	where  	a.dt_alta_interno between	PKG_DATE_UTILS.start_of(dt_parametro_w,'month', 0) 
	and 	(PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_parametro_w, 'MONTH', 0),1,0) - 1/86400)
	and	a.cd_motivo_alta	= b.cd_motivo_alta;

cursor	c02 is
      select 	a.cd_doenca, 
		b.cd_categoria_cid,
		nvl(a.ie_classificacao_doenca,'N'),
		PKG_DATE_UTILS.start_of(dt_diagnostico,'dd',0),
		a.nm_usuario
      from 	cid_doenca b, 
		diagnostico_doenca a  
      where 	a.nr_atendimento = nr_atendimento_w
        and 	a.cd_doenca	 = b.cd_doenca_cid
        and 	a.dt_diagnostico = dt_diagnostico_w
		and		a.ie_situacao = 'A'; 


BEGIN



commit;
dt_parametro_w		:= PKG_DATE_UTILS.start_of(dt_parametro_p,'month', 0);

Gravar_Log_Indicador(33, wheb_mensagem_pck.get_texto(794195), sysdate, dt_parametro_w, nm_usuario_p, nr_sequencia_w);

delete from eis_diagnostico
where PKG_DATE_UTILS.start_of(dt_referencia,'month', 0) 	= dt_parametro_w;

OPEN C01;
LOOP
FETCH C01 into 	nr_atendimento_w,
		cd_estabelecimento_w,
		cd_procedencia_w,
               	ie_clinica_w,
		ie_tipo_atendimento_w,
		cd_medico_w,
		qt_obito_w,
		cd_convenio_w,
		cd_setor_atendimento_w;
EXIT 	WHEN C01%NOTFOUND;
	begin
	select	nvl(max(dt_diagnostico),sysdate)
	into	dt_diagnostico_w
	from 	diagnostico_doenca
	where	nr_atendimento	= nr_atendimento_w
	and	PKG_DATE_UTILS.start_of(dt_diagnostico,'month', 0) = PKG_DATE_UTILS.start_of(dt_parametro_w,'month', 0);

      	select 	nvl(max(d.cd_medico),'0')
      	into 	cd_medico_w
      	from 	diagnostico_medico d  
      	where 	d.nr_atendimento = nr_atendimento_w
        and 	d.dt_diagnostico = dt_diagnostico_w; 

	select 	max(ie_tipo_diagnostico)
      	into 	ie_tipo_diagnostico_w
      	from 	diagnostico_medico d  
      	where 	d.nr_atendimento = nr_atendimento_w; 
	
	select 	max(campo_numerico(obter_idade_pf(p.cd_pessoa_fisica,SYSDATE,'A'))),
		max(p.ie_sexo)
	into	qt_idade_w,
		ie_sexo_w
	from	pessoa_fisica p,
		atendimento_paciente a
	where	a.cd_pessoa_fisica = p.cd_pessoa_fisica
	and	a.nr_atendimento = nr_atendimento_w;	

	open	c02;
	loop
	fetch	c02 
		into	cd_doenca_w, 
			cd_categoria_doenca_w,
			ie_diagnostico_w,
			dt_diag_w,
			nm_usuario_w;
	
	exit	when c02%notfound;
		begin	
		
		select	max(ie_profissional)
		into	ie_profissional_w
		from	usuario
		where	nm_usuario	= nm_usuario_w;

		insert into EIS_Diagnostico 
			(cd_estabelecimento	,
                        dt_referencia    	,
			ie_diagnostico		,   
			cd_procedencia		,
                        cd_medico		,
			cd_doenca		,
			cd_categoria_doenca	,
			ie_clinica		,
			cd_convenio		,
      			qt_ocorrencia		,
			qt_obito		,
			dt_atualizacao		,
			nm_usuario		,
			ie_tipo_atendimento,
			cd_setor_atendimento,
			ie_tipo_diagnostico,
			nr_atendimento,
			ie_profissional,
			ie_sexo,
			qt_idade)
		Values	(cd_estabelecimento_w   ,
                        dt_diag_w    	,   
			ie_diagnostico_w	,
			cd_procedencia_w	,
                        cd_medico_w		,
			cd_doenca_w		,
			cd_categoria_doenca_w	,
			ie_clinica_w		,
			cd_convenio_w		,
      			1			,
			qt_obito_w		,
			dt_atualizacao_w	,
			nm_usuario_p		,
			ie_tipo_atendimento_w	,
			cd_setor_atendimento_w,
			ie_tipo_diagnostico_w,
			nr_atendimento_w,
			ie_profissional_w,
			ie_sexo_w,
			qt_idade_w);
		end;          
	end loop;
	close c02;
	end;
END LOOP;
CLOSE C01;
Atualizar_Log_Indicador(sysdate, nr_sequencia_w);
COMMIT;
END Gerar_EIS_Diagnostico;
/
