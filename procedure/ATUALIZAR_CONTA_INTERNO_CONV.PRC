create or replace
procedure Atualizar_Conta_Interno_Conv
		(nr_seq_protocolo_p	number,
		nm_usuario_p		varchar2) is

cd_estabelecimento_w		number(04,0);
cd_convenio_w			number(05,0);
nr_seq_conta_w			number(15,5);
nr_interno_conta_w		number(10,0);
nr_conta_convenio_w		number(15,0);

cursor	c01 is
	select	nr_interno_conta
	from	conta_paciente
	where	nr_seq_protocolo	= nr_seq_protocolo_p
	and	nr_conta_convenio	is null;

begin

select	cd_estabelecimento,
	cd_convenio
into	cd_estabelecimento_w,
	cd_convenio_w
from	protocolo_convenio
where	nr_seq_protocolo	= nr_seq_protocolo_p;

select	nvl(max(nr_seq_conta),0)
into	nr_seq_conta_w
from	convenio_estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_w
and	cd_convenio		= cd_convenio_w;


nr_conta_convenio_w	:= nr_seq_conta_w;


if	(nr_seq_conta_w > 0) then
	begin
	
	open	c01;
	loop
	fetch	c01 into nr_interno_conta_w;
	exit	when c01%notfound;	
		begin	

/*		select	nvl(max(a.nr_conta_convenio),0)
		into	nr_conta_convenio_w
		from	protocolo_convenio b,
			conta_paciente a
		where	a.nr_seq_protocolo	= b.nr_seq_protocolo
		and	b.cd_convenio		=
			(select	x.cd_convenio
			from	protocolo_convenio x
			where	x.nr_seq_protocolo	= nr_seq_protocolo_p);


		if	(nr_conta_convenio_w = 0) then
			nr_conta_convenio_w	:= nr_seq_conta_w;
		else
			nr_conta_convenio_w	:= nr_conta_convenio_w + 1;
		end if; 
*/


		nr_conta_convenio_w		:= nr_conta_convenio_w + 1;

		update	conta_paciente
		set	nr_conta_convenio	= nr_conta_convenio_w
		where	nr_interno_conta	= nr_interno_conta_w;

		end;
	end loop;
	close c01;

	commit;

	update	convenio_estabelecimento
	set	nr_seq_conta = nr_conta_convenio_w
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_convenio		= cd_convenio_w;

	end;
end if;

end Atualizar_Conta_Interno_Conv;
/