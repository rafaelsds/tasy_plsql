create or replace
procedure proj_desvincular_rat_clt(nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

begin

update	proj_rat 
set	nr_seq_fech_mes	= null,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

end proj_desvincular_rat_clt;
/