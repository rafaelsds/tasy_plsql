Create Or Replace
Procedure Hdm_Gravar_Logs (Ds_Log_Hdm_P Varchar2,
                           Ie_Tipo_Log_P Varchar2,
                           Ds_Titulo_P Varchar2,
                           NR_SEQ_POPULACAO_ALVO_P number) as

Nm_Usuario_W	Varchar2(15);

BEGIN

Nm_Usuario_W := Nvl(Obter_Usuario_Ativo,'Tasy');

Insert Into Hdm_Log_Integracao(Nr_Sequencia,
                              Dt_Atualizacao,
                              Nm_Usuario,
                              Dt_Atualizacao_Nrec,      
                              Nm_Usuario_Nrec,
                              Ds_Log_Hdm,
                              Ie_Tipo_Log,
                              Ds_Titulo,
                              Nr_Seq_Populacao_Alvo)

                      Values (Hdm_Log_Integracao_Seq.Nextval,
                              Sysdate,
                              Nm_Usuario_W,
                              sysdate,      
                              Nm_Usuario_W,
                              SUBSTR(Ds_Log_Hdm_P,1,4000),
                              Ie_Tipo_Log_P,
                              Ds_Titulo_P,
                              NR_SEQ_POPULACAO_ALVO_P);


End hdm_gravar_logs;
/