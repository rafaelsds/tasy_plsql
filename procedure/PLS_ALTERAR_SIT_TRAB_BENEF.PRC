create or replace
procedure pls_alterar_sit_trab_benef
			( 	nr_seq_segurado_p		number,
				ie_situacao_trabalhista_p	varchar2,
				nm_usuario_p			varchar2) is

ie_situacao_trabalhista_ant_w	pls_segurado.ie_situacao_trabalhista%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_segurado
	where	((nr_sequencia = nr_seq_segurado_p) or (nr_seq_titular = nr_seq_segurado_p))
	order by nvl(nr_seq_titular,0);

begin

open C01;
loop
fetch C01 into
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	select	max(ie_situacao_trabalhista)
	into	ie_situacao_trabalhista_ant_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	
	if	(nvl(ie_situacao_trabalhista_ant_w,0) <> nvl(ie_situacao_trabalhista_p,0)) then
		update	pls_segurado
		set	ie_situacao_trabalhista = ie_situacao_trabalhista_p,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_segurado_w;
		
		pls_gerar_segurado_historico(
			nr_seq_segurado_w, '80', sysdate,
			wheb_mensagem_pck.get_texto(322869, 'IE_SITUACAO_TRABALHISTA_P=' ||substr(obter_valor_dominio(3840,ie_situacao_trabalhista_ant_w),1,255)),'pls_alterar_sit_trab_benef', null,
			null, null, null,
			sysdate, null, null,
			null, null, null,
			null, nm_usuario_p, 'S');
	end if;
	end;
end loop;
close C01;

commit;

end pls_alterar_sit_trab_benef;
/