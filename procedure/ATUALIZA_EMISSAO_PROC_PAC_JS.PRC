create or replace
procedure atualiza_emissao_proc_pac_js(	nr_prescricao_p	number,
					nm_usuario_p	varchar2) is 

begin

if	(nr_prescricao_p is not null)then
	begin
	
	update prescr_procedimento b
	set b.dt_emissao_setor_atend = sysdate
	where b.nr_prescricao = nr_prescricao_p
	and exists (	select 1
			from Usuario_Setor_v a
			where a.cd_setor_atendimento = b.cd_setor_atendimento
			and a.nm_usuario = nm_usuario_p );
	
	end;
end if;

commit;

end atualiza_emissao_proc_pac_js;
/