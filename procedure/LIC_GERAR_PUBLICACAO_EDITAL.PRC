create or replace
procedure lic_gerar_publicacao_edital(	nr_seq_lic_edital_p		number,
					nr_seq_licitacao_p			number,
					nr_seq_local_public_p		number,
					dt_publicacao_p			date,
					nr_publicacao_p			varchar2,
					ds_observacao_p			varchar2,
					ds_arquivo_p			varchar2,
					nm_usuario_p			varchar2,
					nr_seq_public_edital_p	out	number) is 

					
nr_seq_modelo_w			varchar2(15);
qt_existe_w			number(10);
nr_seq_mod_compra_w		reg_licitacao.nr_seq_mod_compra%type;
ie_exige_disp_contabil_w	reg_lic_mod_compra.ie_exige_disp_contabil%type;
ie_exige_disp_financeira_w	reg_lic_mod_compra.ie_exige_disp_financeira%type;
nr_sequencia_w			reg_lic_public_edital.nr_sequencia%type;

begin

select	nvl(max(nr_seq_mod_compra),0)
into	nr_seq_mod_compra_w
from	reg_licitacao
where	nr_sequencia = nr_seq_licitacao_p;

if	(nr_seq_mod_compra_w > 0) then

	select	nvl(ie_exige_disp_contabil,'N'),
		nvl(ie_exige_disp_financeira,'N')
	into	ie_exige_disp_contabil_w,
		ie_exige_disp_financeira_w
	from	reg_lic_mod_compra
	where	nr_sequencia = nr_seq_mod_compra_w;
	

	select	count(*)
	into	qt_existe_w
	from	reg_lic_documento_edital
	where	nr_seq_lic_edital	= nr_seq_lic_edital_p
	and	ie_tipo_documento	= 'F';
	
	if	(ie_exige_disp_financeira_w = 'S') and
		(qt_existe_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(455419);
		/*As licitações dessa modalidade exigem uma documentação do tipo Disponibilidade Financeira.*/
	end if;

	select	count(*)
	into	qt_existe_w
	from	reg_lic_documento_edital
	where	nr_seq_lic_edital	= nr_seq_lic_edital_p
	and	ie_tipo_documento	= 'C';
	
	if	(ie_exige_disp_contabil_w = 'S') and
		(qt_existe_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(455420);
		/*As licitações dessa modalidade exigem uma documentação do tipo Disponibilidade Contábil.*/
	end if;
end if;

select	reg_lic_public_edital_seq.nextval
into	nr_sequencia_w
from	dual;

insert into reg_lic_public_edital(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_licitacao,
	nr_seq_lic_edital,
	nr_seq_local_public,
	dt_publicacao,
	nr_publicacao,
	ds_observacao,
	ds_arquivo)
values(	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_licitacao_p,
	nr_seq_lic_edital_p,
	nr_seq_local_public_p,
	dt_publicacao_p,
	nr_publicacao_p,
	ds_observacao_p,
	ds_arquivo_p);
	
update	reg_licitacao
set	dt_publicacao_edital = sysdate,
	nm_usuario_public = nm_usuario_p
where	nr_sequencia = nr_seq_licitacao_p;

insert into reg_lic_historico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_historico,
	ds_observacao,
	nr_seq_licitacao)
values(	reg_lic_historico_seq.nextval,
	sysdate,
	nm_usuario_p,
	'PE',
	WHEB_MENSAGEM_PCK.get_texto(310618) || nr_publicacao_p,
	nr_seq_licitacao_p);

commit;

nr_seq_public_edital_p	:= nr_sequencia_w;

end lic_gerar_publicacao_edital;
/