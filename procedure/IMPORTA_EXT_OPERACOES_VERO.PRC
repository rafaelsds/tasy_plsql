create or replace procedure importa_ext_operacoes_vero (	nr_seq_extrato_p  in number,
								nm_usuario_p	 in	Varchar2,
								ds_arquivo_p	 in	varchar2) is

/*Desenvolvido cfme layout enviado na OS 2454012 -
	CONCILIACAO  DE CARTOES
	BJRVCM8 - ARQUIVO GERAL DE OPERACOES ACQUIRER
	VERO
*/

nr_seq_extrato_arq_w		extrato_cartao_cr_arq.nr_sequencia%type;
nr_seq_extrato_res_w		extrato_cartao_cr_res.nr_sequencia%type;
nr_ultimo_nsu_w				extrato_cartao_cr_res.nr_resumo%type;
nr_seq_bandeira_w			bandeira_cartao_cr.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_seq_conta_banco_w		bandeira_cartao_cr.nr_seq_conta_banco%type;

Cursor C01 is
	select	substr(ds_conteudo,59,8) nr_nsu_host_movto,
		to_date(substr(ds_conteudo,141,8),'yyyymmdd') dt_transacao,
		substr(ds_conteudo,294,6)||rpad('*',6,'*')||substr(ds_conteudo,300,4) nr_cartao,
		substr(ds_conteudo,67,2) nr_parcela,
		substr(ds_conteudo,69,2) qtd_parcelas,
		substr(ds_conteudo,126,15)/100 vl_bruto_parcela,
		substr(ds_conteudo,177,15)/100 vl_liquido_parcela,
		substr(ds_conteudo,291,3) cd_bandeira,
		to_date(substr(ds_conteudo,198,8)||substr(ds_conteudo,206,6),'yyyymmddhh24miss') dt_pagto
	from	w_extrato_cartao_cr
	where	nr_seq_extrato = nr_seq_extrato_p
	and		substr(ds_conteudo,1,2) = '10'
	and		substr(ds_conteudo,109,2) in ('00','01','02','03','04','05') --situacao
	order by nr_nsu_host_movto, nr_parcela;

begin

cd_estabelecimento_w := obter_estabelecimento_ativo;

/*Criar  Registro do arquivo que esta sendo importado - Financeiro pois no extrato podem ter Debito e Credito*/
select	extrato_cartao_cr_arq_seq.nextval
into	nr_seq_extrato_arq_w
from	dual;

insert into EXTRATO_CARTAO_CR_ARQ (	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_extrato,
	ie_tipo_arquivo,
	ds_arquivo)
values 	(	nr_seq_extrato_arq_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_extrato_p,
	'F',
	ds_arquivo_p);

if (nr_seq_extrato_p is not null) then

	<<reg_parcelas_w>>
	for reg_parcela_w in C01
	loop

		if nvl(reg_parcela_w.nr_nsu_host_movto,'X') != nvl(nr_ultimo_nsu_w,'-9999') then

			select	max(a.nr_sequencia),
				obter_valor_bandeira_estab(cd_estabelecimento_w, max(a.nr_sequencia), 'NR_SEQ_CONTA_BANCO')
			into	nr_seq_bandeira_w,
				nr_seq_conta_banco_w
			from	bandeira_cartao_cr a
			where	a.cd_bandeira	= reg_parcela_w.cd_bandeira
			and	a.cd_bandeira is not null;

			select	extrato_cartao_cr_res_seq.nextval
			into	nr_seq_extrato_res_w
			from	dual;

			insert into extrato_cartao_cr_res ( nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nr_seq_extrato,
				nr_resumo,
				nr_seq_conta_banco,
				qt_cv_aceito,
				qt_cv_rejeitado,
				vl_bruto,
				vl_comissao,
				vl_rejeitado,
				vl_liquido,
				dt_prev_pagto,
				nr_seq_extrato_arq,
				nr_seq_bandeira)
			values	( nr_seq_extrato_res_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_extrato_p,
				reg_parcela_w.nr_nsu_host_movto,
				nr_seq_conta_banco_w,
				1,--t_cv_aceito_w, Botei fixo um CV pq nao vem no layout essa informacao, esse layout eh bem diferente dos outros que ja atuamos, pois n tem reumos dos movimentos.
				0,--qt_cv_rejeitado_w,
				reg_parcela_w.vl_bruto_parcela,
				0,--vl_comissao_w,
				0,--vl_rejeitado_w,
				reg_parcela_w.vl_liquido_parcela,
				reg_parcela_w.dt_transacao,
				nr_seq_extrato_arq_w,
				nr_seq_bandeira_w);

			nr_ultimo_nsu_w := 	reg_parcela_w.nr_nsu_host_movto;

		else

			update	extrato_cartao_cr_res
			set	vl_bruto		= nvl(vl_bruto,0) + nvl(reg_parcela_w.vl_bruto_parcela,0),
				qt_cv_aceito	= nvl(qt_cv_aceito,0) + 1,
				vl_liquido		= vl_liquido + nvl(reg_parcela_w.vl_liquido_parcela,0)
			where	nr_sequencia		= nr_seq_extrato_res_w
			and		nr_seq_extrato_arq	= nr_seq_extrato_arq_w;

		end if;

		insert into extrato_cartao_cr_movto (	 nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_extrato,
			nr_seq_extrato_res,
			vl_parcela,
			dt_parcela,
			qt_parcelas,
			nr_cartao,
			nr_autorizacao,
			ds_comprovante,
			ds_rejeicao,
			dt_compra,
			nr_parcela,
			nr_seq_extrato_parcela,
			ie_pagto_indevido,
			vl_liquido,
			nr_seq_extrato_arq,
			nr_resumo,
			nr_seq_extrato_parc_cred,
			vl_saldo_concil_fin,
			vl_saldo_concil_cred,
			vl_aconciliar,
			nr_seq_origem,
			nr_seq_parcela,
			nr_documento,
			vl_ajuste,
			vl_ajuste_desp)
		values	(extrato_cartao_cr_movto_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_extrato_p,
			nr_seq_extrato_res_w, --nr_seq_resumo,
			reg_parcela_w.vl_bruto_parcela,
			reg_parcela_w.dt_transacao,
			reg_parcela_w.qtd_parcelas,
			reg_parcela_w.nr_cartao,
			null,--nr_autorizacao_w,
			null,
			null, -- ds_rejeicao
			reg_parcela_w.dt_pagto, --dt_compra
			to_number(reg_parcela_w.nr_parcela),
			null, --nr_seq_extrato_parcela,
			'N', --ie_pagto_indevido
			reg_parcela_w.vl_liquido_parcela,
			nr_seq_extrato_arq_w,
			reg_parcela_w.nr_nsu_host_movto,
			null, --nr_seq_extrato_parc_cred
			reg_parcela_w.vl_liquido_parcela,
			null, --vl_saldo_concil_cred
			null, --vl_aconciliar
			null, --nr_seq_origem
			null, --nr_seq_parcela,
			null, --nr_documento
			null, --vl_ajuste
			null); --vl_ajuste_desp    

	end loop reg_parcelas;

end if;

update	extrato_cartao_cr
set	 	dt_importacao		= sysdate
where	nr_sequencia		= nr_seq_extrato_p;

commit;

end importa_ext_operacoes_vero;
/
