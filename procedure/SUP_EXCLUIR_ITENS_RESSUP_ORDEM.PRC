create or replace
procedure sup_excluir_itens_ressup_ordem(	nr_requisicao_p		number,
					nr_seq_ordem_p		number,
					nm_usuario_p		Varchar2) is 
begin

delete	requisicao_ordem_prod
where	nr_requisicao = nr_requisicao_p
and	nr_seq_ordem_prod = nr_seq_ordem_p;

commit;

end sup_excluir_itens_ressup_ordem;
/