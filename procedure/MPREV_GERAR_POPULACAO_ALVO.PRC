create or replace
procedure mprev_gerar_populacao_alvo(nr_seq_populacao_p	number,
			             nm_usuario_p	varchar2,
				     ie_opcao_p		varchar2) is
				
nr_seq_regra_cubo_w	mprev_regra_cubo.nr_sequencia%type;
dt_inicio_geracao_w	date;
dt_fim_geracao_w	date;
ie_origem_dados_w	mprev_config_geral.ie_origem_dados%type;

begin

dt_inicio_geracao_w 	:= null;
dt_fim_geracao_w	:= null;

if	(nr_seq_populacao_p is not null) then
	
	select	a.nr_seq_regra_cubo
	into	nr_seq_regra_cubo_w
	from	mprev_populacao_alvo a
	where	a.nr_sequencia	= nr_seq_populacao_p;

	 select nvl(max(ie_origem_dados),'O')
	 into	ie_origem_dados_w
	 from 	mprev_config_geral
	 where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
	
	if	(ie_opcao_p = 'G') then
		dt_inicio_geracao_w := sysdate;
	end if;
	
	/* Atualizar a data de inicio de geracao da populacao */
	update	mprev_populacao_alvo
	set	dt_inicio_geracao 	= dt_inicio_geracao_w,
		nm_usuario 			= nm_usuario_p,
		dt_atualizacao 		= sysdate
	where	nr_sequencia	= nr_seq_populacao_p;
	
	if 	(ie_origem_dados_w = 'P') then
		mprev_pop_alvo_prest_pck.desfazer_geracao_pop_alvo(nr_seq_populacao_p);
	else
		mprev_pop_alvo_pck.desfazer_geracao_pop_alvo(nr_seq_populacao_p);
	end if;
	
	if	(ie_opcao_p = 'G') then
		if 	(ie_origem_dados_w = 'P') then
			mprev_pop_alvo_prest_pck.gerar_populacao_alvo(nr_seq_regra_cubo_w,nr_seq_populacao_p,nm_usuario_p);
		else
			mprev_pop_alvo_pck.gerar_populacao_alvo(nr_seq_regra_cubo_w,nr_seq_populacao_p,nm_usuario_p);
		end if;
		dt_fim_geracao_w := sysdate;
	end if;
	
	/* Atualizar a data de termino de geracao da populacao */
	update	mprev_populacao_alvo
	set	dt_fim_geracao 		= dt_fim_geracao_w,
		nm_usuario 			= nm_usuario_p,
		dt_atualizacao 		= sysdate
	where	nr_sequencia	= nr_seq_populacao_p;
	
end if;

end mprev_gerar_populacao_alvo;
/
