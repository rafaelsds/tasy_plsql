create or replace
procedure html_atualiza_quant_disp(
				nr_prescricao_p			number,
				nr_seq_cpoe_p			number,
				ie_agrupador_P			number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p	number) is
				
cd_material_w				prescr_material.cd_material%type;
nr_seq_material_w			prescr_material.nr_sequencia%type;
cd_intervalo_w				prescr_material.cd_intervalo%type;
ie_via_aplicacao_w			prescr_material.ie_via_aplicacao%type;
qt_unitaria_w				prescr_material.qt_unitaria%type;
qt_dose_especial_w			prescr_material.qt_dose_especial%type;
nr_ocorrencia_w				prescr_material.nr_ocorrencia%type;
ds_dose_diferenciada_w		prescr_material.ds_dose_diferenciada%type;
ie_origem_inf_w				prescr_material.ie_origem_inf%type;
cd_unidade_medida_dose_w	prescr_material.cd_unidade_medida_dose%type;
qt_dias_util_w				prescr_material.qt_dias_util%type;
qt_material_w				prescr_material.qt_material%type;
ie_se_necessario_w			prescr_material.ie_se_necessario%type;
ie_acm_w					prescr_material.ie_acm%type;
qt_dispensar_w				prescr_material.qt_total_dispensar%type;
ie_regra_disp_w				prescr_material.ie_regra_disp%type;

ds_erro_w					varchar2(4000);

cursor c01 is
select	cd_material,
		nr_sequencia,
		cd_intervalo,
		ie_via_aplicacao,
		qt_unitaria,
		qt_dose_especial,
		nr_ocorrencia,
		ds_dose_diferenciada,
		ie_origem_inf,
		cd_unidade_medida_dose,
		nvl(qt_dias_util,0),
		qt_material,
		nvl(ie_se_necessario,'N'),
		nvl(ie_acm,'N')
from	prescr_material
where	nr_prescricao = nr_prescricao_p
and		nr_seq_mat_cpoe = nr_seq_cpoe_p
and		nvl(ie_suspenso,'N') = 'N'
and		ie_agrupador = ie_agrupador_p;

begin

open c01;
loop
fetch c01 into	cd_material_w,
				nr_seq_material_w,
				cd_intervalo_w,
				ie_via_aplicacao_w,
				qt_unitaria_w,
				qt_dose_especial_w,
				nr_ocorrencia_w,
				ds_dose_diferenciada_w,
				ie_origem_inf_w,
				cd_unidade_medida_dose_w,
				qt_dias_util_w,
				qt_material_w,
				ie_se_necessario_w,
				ie_acm_w;
exit when c01%notfound;
begin

 Obter_quant_dispensar(	cd_estabelecimento_p, cd_material_w, nr_prescricao_p, nr_seq_material_w, cd_intervalo_w, ie_via_aplicacao_w,
						qt_unitaria_w, qt_dose_especial_w, nr_ocorrencia_w, ds_dose_diferenciada_w, ie_origem_inf_w, cd_unidade_medida_dose_w,
						qt_dias_util_w, qt_material_w, qt_dispensar_w, ie_regra_disp_w, ds_erro_w, ie_se_necessario_w, ie_acm_w);
						
update	prescr_material
set		qt_material			= qt_material_w,
		qt_total_dispensar	= qt_dispensar_w
where	nr_prescricao 		= nr_prescricao_p
and		nvl(ie_suspenso,'N') = 'N'
and		nr_sequencia 		= nr_seq_material_w;

commit;

end;
end loop;

end html_atualiza_quant_disp;
/