create or replace
procedure bsc_justificar_result_ind(	nr_seq_inf_p	number,
				ds_justificativa_p	varchar2,
				nm_usuario_p	varchar2) is 

begin

update	bsc_ind_inf
set	ds_justificativa	= substr(ds_justificativa_p,1,2000),
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_inf_p;

commit;

end bsc_justificar_result_ind;
/
