create or replace
procedure lab_gravar_prescr_etiq_long(	nr_prescricao_p	number,
					ds_etiqueta1_p	varchar2,
					ds_etiqueta2_p	varchar2,
					nm_usuario_p	varchar2,
					nr_controle_p	varchar2 default null) as

qt_prescricao_w		number;

c001			INTEGER;

retorno_w		NUMBER(5);
ds_sql_w		varchar2(500);
				
begin



if (nr_controle_p is null) then

	select	count(*)
	into	qt_prescricao_w
	from	prescr_etiqueta
	where	nr_prescricao = nr_prescricao_p;
else
	select 	count(*)
	into	qt_prescricao_w
	from	prescr_etiqueta a
	where	a.nr_prescricao = nr_prescricao_p
	and	nr_controle = nr_controle_p;

	
end if;

if 	(qt_prescricao_w = 0) then
	
	C001 := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(C001, ' insert into prescr_etiqueta (nr_prescricao, ds_etiqueta, dt_atualizacao, nm_usuario, nr_controle) values (:nr_prescricao, :ds_etiqueta, sysdate, :nm_usuario, :nr_controle) ', dbms_sql.Native);
	
	DBMS_SQL.BIND_VARIABLE(C001, 'nr_prescricao', nr_prescricao_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'ds_etiqueta', (ds_etiqueta1_p||ds_etiqueta2_p) );
	DBMS_SQL.BIND_VARIABLE(C001, 'nm_usuario', nm_usuario_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'nr_controle', nr_controle_p);
	
	retorno_w := DBMS_SQL.EXECUTE(c001);
	DBMS_SQL.CLOSE_CURSOR(C001);
		
else	
	
	C001 := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(C001, ' update prescr_etiqueta set ds_etiqueta = :ds_etiqueta, dt_atualizacao = sysdate, nm_usuario = :nm_usuario where nr_prescricao = :nr_prescricao and (nr_controle = :nr_controle or nvl(:nr_controle,0) = 0) ', dbms_sql.Native);
	
	DBMS_SQL.BIND_VARIABLE(C001, 'nr_prescricao', nr_prescricao_p);
	DBMS_SQL.BIND_VARIABLE(C001, 'ds_etiqueta', (ds_etiqueta1_p||ds_etiqueta2_p));
	DBMS_SQL.BIND_VARIABLE(C001, 'nm_usuario', nm_usuario_p);
	
	if	(nr_controle_p is not null) then
		DBMS_SQL.BIND_VARIABLE(C001, 'nr_controle', nr_controle_p);
	else
		DBMS_SQL.BIND_VARIABLE(C001, 'nr_controle', 0);
	end if;
	
	retorno_w := DBMS_SQL.EXECUTE(c001);
	DBMS_SQL.CLOSE_CURSOR(C001);
			
end if;	
				
commit;	

end lab_gravar_prescr_etiq_long;
/