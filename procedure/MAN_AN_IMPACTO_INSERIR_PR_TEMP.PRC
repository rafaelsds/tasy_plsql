create or replace
procedure man_an_impacto_inserir_pr_temp (	nr_seq_ordem_serv_p	number,
						nr_seq_product_req_p	varchar2,
						cd_funcao_p		number,
						ds_new_title_p 		varchar2 default null,
						nm_usuario_p		varchar2,
						ie_impacto_requisito_p	varchar2 default null) is

ie_impacto_requisito_w 	w_man_ordem_serv_imp_pr.ie_impacto_requisito%type := null;

begin

	if	(nr_seq_ordem_serv_p is not null) and
		(nr_seq_product_req_p is not null) and
		(cd_funcao_p is not null) then

		if (ds_new_title_p is not null) then
			delete from w_ordem_serv_imp_quest_res 	where nr_seq_ordem_serv = nr_seq_ordem_serv_p;
			delete from w_man_ordem_serv_imp_pr 		where nr_seq_ordem_serv = nr_seq_ordem_serv_p;
			ie_impacto_requisito_w := 'D';
		end if;

		insert into w_man_ordem_serv_imp_pr (
			nr_sequencia,
			nr_seq_ordem_serv,
			nr_product_requirement,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_funcao,
			ds_new_title,
			ie_impacto_requisito
		) values (
			man_ordem_serv_imp_pr_seq.nextval,
			nr_seq_ordem_serv_p,
			nr_seq_product_req_p,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_funcao_p,
			ds_new_title_p,
			ie_impacto_requisito_p
		);

	end if;

	commit;

end man_an_impacto_inserir_pr_temp;
/
