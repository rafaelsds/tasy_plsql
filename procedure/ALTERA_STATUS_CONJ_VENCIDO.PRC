create or replace
procedure altera_status_conj_vencido is

nr_sequencia_w		number(10);

/* Busca conjuntos com validade vencida, que nao foram atendidos */

cursor c01 is

	select	nr_sequencia
	from	cm_conjunto_cont
	where	nvl(ie_situacao,'A') = 'A'
	and	ie_status_conjunto = 3
	and	dt_validade < sysdate
	and cm_obter_dados_conjunto(nr_seq_conjunto,'S') = 'A'
	order by nr_sequencia;

begin

open c01;
loop
fetch c01 into	
	nr_sequencia_w;
exit when c01%notfound;
	begin
	/* ie_status_conjunto = 10: Vencido */

	update	cm_conjunto_cont
	set	ie_status_conjunto = 10,
		dt_atualizacao = sysdate,
		nm_usuario = 'Tasy'
	where	nr_sequencia = nr_sequencia_w;
	
	/* Gera novo conjunto_cont com status Aguardando esterilizacao */
	cm_gerar_conj_retorno_atend(nr_sequencia_w,'Tasy');
	end;
end loop;
close c01;

commit;

end altera_status_conj_vencido;
/
