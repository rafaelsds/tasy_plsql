create or replace
procedure consiste_temp_item_setor_d
			(	nr_seq_item_p				number,
				qt_temp_inicial_p			number,
				qt_temp_min_p				number,
				qt_temp_max_p				number,
				qt_umidade_p				number,
				ds_erro_p			out	varchar2,
				nm_usuario_p				varchar2,
				cd_estabelecimento_p			number,
				cd_setor_p				number,
				ie_gera_comunic_p			varchar2,
				cd_perfil_comunic_p		out	number,
				ds_observacao_p				varchar2,
				nm_usuario_comunic_p		out	varchar2,
				nr_seq_grupo_usuario_p		out	number) as

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar comunicacao interna para controle de temperatura e umidade do setor.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_temp_min_w			number(15,2);
qt_temp_max_w			number(15,2);
qt_umidade_min_w		number(15,2);
qt_umidade_max_w		number(15,2);
ds_mensagem_w			varchar2(4000);
cd_perfil_comunic_w		number(15);
ds_perfil_mensagem_w		varchar2(4000);
ds_setor_w			varchar2(4000);
ds_setor_mensagem_w		varchar2(4000);
ds_item_w			varchar2(255);
nr_seq_grupo_usuario_w		number(10,0);
ds_erro_w			varchar2(4000);

cursor c01 is
	select	a.qt_temp_min,
		a.qt_temp_max,
		a.qt_umidade_min,
		a.qt_umidade_max,
		substr(a.ds_mensagem,1,4000) ds_mensagem,
		a.cd_perfil_comunic,
		substr(obter_nome_setor(nvl(b.cd_setor_atendimento, cd_setor_p)),1,254) ds_setor,
		substr(b.ds_item,1,255) ds_item,
		a.nm_usuario_comunic,
		a.nr_seq_grupo_usuario
	from	item_temp_regra		a,
		item_temperatura	b
	where	b.nr_sequencia	= a.nr_seq_item
	and	a.nr_seq_item	= nr_seq_item_p
	and	nvl(b.cd_setor_atendimento, nvl(cd_setor_p, 0))	= nvl(cd_setor_p, 0);

begin
open c01;
loop
fetch c01 into
	qt_temp_min_w,
	qt_temp_max_w,
	qt_umidade_min_w,
	qt_umidade_max_w,
	ds_mensagem_w,
	cd_perfil_comunic_w,
	ds_setor_w,
	ds_item_w,
	nm_usuario_comunic_p,
	nr_seq_grupo_usuario_w;
exit when c01%notfound;
	ds_setor_mensagem_w	:= ds_setor_w;
	
	if	(qt_temp_inicial_p is not null) then
		if	(qt_temp_inicial_p < qt_temp_min_w) or
			(qt_temp_inicial_p > qt_temp_max_w) then
			if	(nvl(instr(upper(ds_erro_w), upper(ds_item_w)),0) = 0) then
				ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280907) || ds_item_w || ': ',1,4000);
			end if;

			ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280908) || campo_mascara(qt_temp_inicial_p,2),1,4000);
		end if;
	end if;

	if	(qt_temp_min_p is not null) then
		if	(qt_temp_min_p < qt_temp_min_w) or
			(qt_temp_min_p > qt_temp_max_w) then
			if	(nvl(instr(upper(ds_erro_w), upper(ds_item_w)),0) = 0) then
				ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280907) || ds_item_w || ': ',1,4000);
			end if;
			
			ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280909) || campo_mascara(qt_temp_min_p,2),1, 4000);
		end if;
	end if;

	if	(qt_temp_max_p is not null) then
		if	(qt_temp_max_p < qt_temp_min_w) or
			(qt_temp_max_p > qt_temp_max_w) then
			if	(nvl(instr(upper(ds_erro_w), upper(ds_item_w)),0) = 0) then
				ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280907) || ds_item_w || ': ',1,4000);
			end if;
			
			ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280910) || campo_mascara(qt_temp_max_p,2),1,4000);
		end if;
	end if;

	if	((qt_umidade_min_w is not null) or
		(qt_umidade_max_w is not null)) and
		(qt_umidade_p is not null) then
		if	(qt_umidade_p < qt_umidade_min_w) or
			(qt_umidade_p > qt_umidade_max_w) then
			if	(nvl(instr(upper(ds_erro_w), upper(ds_item_w)),0) = 0) then
				ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280907) || ds_item_w || ': ',1,4000);
			end if;
			
			ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280911) || campo_mascara(qt_umidade_p,2),1,4000);
		end if;
	end if;

	if	(nvl(ds_erro_w,'X') <> 'X') and
		(nvl(ds_item_w,'X') <> 'X') then
		ds_erro_w	:= substr(ds_erro_w || chr(13) || chr(10) || ds_mensagem_w,1,4000);

		if	(ds_observacao_p is not null) then
			ds_erro_w := substr(ds_erro_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(280912) || ds_observacao_p,1,4000);
		end if;
		
		ds_perfil_mensagem_w	:= cd_perfil_comunic_w;
	end if;
end loop;
close c01;

cd_perfil_comunic_p	:= cd_perfil_comunic_w;
nr_seq_grupo_usuario_p	:= nr_seq_grupo_usuario_w;

if	((ds_perfil_mensagem_w is not null) or 
	(nr_seq_grupo_usuario_w is not null)) and
	(ds_erro_w is not null) and
	(nvl(ie_gera_comunic_p,'N') = 'S') then

	ds_perfil_mensagem_w	:= ds_perfil_mensagem_w || ',';
	
	gerar_comunic_padrao(	sysdate,
				wheb_mensagem_pck.get_texto(280913, 'DS_SETOR_ATENDIMENTO=' || ds_setor_mensagem_w),
				ds_erro_w,
				nm_usuario_p,
				'N',
				nm_usuario_comunic_p,
				'N',
				null,
				ds_perfil_mensagem_w,
				cd_estabelecimento_p,
				null,
				sysdate,
				nr_seq_grupo_usuario_w || ',',
				null);
end if;

ds_erro_p	:= substr(ds_erro_w,1,255);

end consiste_temp_item_setor_d;
/