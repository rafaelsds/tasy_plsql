create or replace
procedure hsa_gerar_w_repasse3 
		(nr_seq_proc_repasse_p	in number, 
		nr_seq_mat_repasse_p	in number,
		ie_status_p		in varchar2,
		vl_repasse_p		in number,
		nm_usuario_p		in varchar2, 
		nr_seq_proc_rep_novo_p	out number,
		nr_seq_mat_rep_novo_p	out number,
		ie_atualizar_p		in varchar2) is 

/*	Tipo Liberacao
	A - Aguardando a��o do usu�rio 
	G - Glosado	*/

ie_status_w			varchar2(01); 
nr_seq_proc_rep_novo_w		number(10,0) := null; 
nr_seq_mat_rep_novo_w		number(10,0) := null; 
nr_interno_conta_w		number(15); 
nr_seq_retorno_w			number(15);
nr_seq_retorno_nova_w		number(15); 
dt_retorno_w			date; 
cont_w				number(15);
nr_seq_guia_w			number(15); 
dt_atualizacao_retorno_w		date; 
dt_atualizacao_tit_w		date; 

begin

if	(nr_seq_proc_repasse_p is not null) and 
	(nr_seq_proc_repasse_p > 0) then
	begin
	if	(ie_atualizar_p = 'S') then
		-- o select abaixo � para o caso de estar sendo desdobrado atrav�s da nova rotina . dsantos em 29/08/2011 
		select	max(c.nr_seq_retorno) 
		into	nr_seq_retorno_nova_w 
		from	convenio_retorno_item c,
			convenio_retorno_glosa b, 
			procedimento_repasse a
		where	b.nr_seq_propaci	= a.nr_seq_procedimento
		and	b.nr_seq_ret_item	= c.nr_sequencia
		and	a.nr_sequencia	= nr_seq_proc_repasse_p; 
	else
		select	max(c.nr_seq_retorno) 
		into	nr_seq_retorno_nova_w 
		from	convenio_retorno_item c,
			convenio_retorno_glosa b, 
			procedimento_repasse a
		where	b.nr_seq_propaci	= a.nr_seq_procedimento
		and	b.nr_seq_ret_item	= c.nr_sequencia
		and	a.nr_sequencia	= nr_seq_proc_repasse_p; 
	end if;

	if	(ie_atualizar_p = 'S') then
		select	max(c.nr_seq_retorno) 
		into	nr_seq_retorno_w 
		from	convenio_retorno_item c,
			convenio_retorno_glosa b, 
			procedimento_repasse a
		where	c.nr_sequencia	= b.nr_seq_ret_item
		and	b.nr_sequencia	= a.nr_seq_ret_glosa 
		and	a.nr_sequencia	= nr_seq_proc_repasse_p; 
	else
		select	max(c.nr_seq_retorno) 
		into	nr_seq_retorno_w 
		from	convenio_retorno_item c,
			convenio_retorno_glosa b, 
			procedimento_repasse a
		where	c.nr_sequencia	= b.nr_seq_ret_item
		and	b.nr_sequencia	= a.nr_seq_ret_glosa 
		and	a.nr_sequencia	= nr_seq_proc_repasse_p; 
	end if;


	if	(nr_seq_retorno_w is not null or nr_seq_retorno_nova_w is not null) then -- nr_seq_retorno_nova_w trata-se da libera�ao na nova rotina. 

		select	max(dt_retorno),
			max(dt_atualizacao)
		into	dt_retorno_w,
			dt_atualizacao_retorno_w
		from	(
			select	decode(b.ie_data_lib_repasse_ret,'F',nvl(a.dt_baixa_cr, a.dt_fechamento),'R',a.dt_retorno, 'E', a.dt_fechamento) dt_retorno,
				a.dt_atualizacao
			from	parametro_faturamento b, 
				convenio_retorno a
			where	a.nr_sequencia		= nr_seq_retorno_w 
			and	a.cd_estabelecimento	= b.cd_estabelecimento 
			union 
			select	decode(b.ie_data_lib_repasse_ret,'F',nvl(a.dt_baixa_cr, a.dt_fechamento),'R',a.dt_retorno, 'E', a.dt_fechamento),
				a.dt_atualizacao
			from	parametro_faturamento b, 
				convenio_retorno a
			where	a.nr_sequencia		= nr_seq_retorno_nova_w
			and	a.cd_estabelecimento	= b.cd_estabelecimento
			);

	end if;

	if	(ie_atualizar_p = 'S') then
		select	count(*),
			max(a.nr_Seq_guia) 
		into	cont_w,
			nr_seq_guia_w
		from	lote_audit_hist_item a, 
			procedimento_repasse b
		where	b.nr_seq_procedimento	= a.nr_seq_propaci
		and	b.nr_sequencia		= nr_seq_proc_repasse_p;
	else
		select	count(*),
		max(a.nr_Seq_guia) 
		into	cont_w,
			nr_seq_guia_w
		from	lote_audit_hist_item a, 
			procedimento_repasse b
		where	b.nr_seq_procedimento	= a.nr_seq_propaci
		and	b.nr_sequencia		= nr_seq_proc_repasse_p;
	end if;

	if	(cont_w > 0) and (nr_seq_guia_w is not null) then 

		select	max(dt_atualizacao)
		into	dt_atualizacao_tit_w 
		from	titulo_receber_liq 
		where	NR_SEQ_LOTE_HIST_GUIA	= nr_seq_guia_w; 


		if	(dt_atualizacao_retorno_w is null) or (dt_atualizacao_tit_w > dt_atualizacao_retorno_w) then 

			select	max(dt_recebimento) 
			into	dt_retorno_w 
			from	titulo_receber_liq
			where	NR_SEQ_LOTE_HIST_GUIA	= nr_seq_guia_w;
		end if;

	end if;


	select	procedimento_repasse_seq.NextVal 
	into	nr_seq_proc_rep_novo_w
	from	dual;

	if	(ie_atualizar_p = 'S') then
		begin
		insert into procedimento_repasse (	
			NR_SEQUENCIA, 
			NR_SEQ_PROCEDIMENTO, 
			VL_REPASSE, 
			DT_ATUALIZACAO, 
			NM_USUARIO, 
			NR_SEQ_TERCEIRO,
			NR_LOTE_CONTABIL, 
			NR_REPASSE_TERCEIRO, 
			CD_CONTA_CONTABIL,
			NR_SEQ_TRANS_FIN, 
			VL_LIBERADO,
			NR_SEQ_ITEM_RETORNO, 
			IE_STATUS,
			NR_SEQ_ORIGEM,
			CD_REGRA, 
			DT_LIBERACAO, 
			CD_MEDICO,
			dt_contabil_titulo,
			dt_contabil,
			nr_seq_partic)
		select	nr_seq_proc_rep_novo_w, 
			NR_SEQ_PROCEDIMENTO, 
			vl_repasse_p,
			sysdate, 
			nm_usuario_p,
			NR_SEQ_TERCEIRO, 
			nr_lote_contabil, 

			null, 
			null, 
			NR_SEQ_TRANS_FIN,
			0,
			null, 
			nvl(ie_status_p,'A'),
			NR_SEQUENCIA,
			CD_REGRA,
			decode(ie_status_p,'G', nvl(dt_retorno_w, sysdate), null), 
			CD_MEDICO, 
			dt_contabil_titulo,
			dt_contabil, 
			nr_seq_partic
		from	procedimento_repasse
		where	nr_sequencia = nr_seq_proc_repasse_p; 
		exception 
			when others then 
				---20011, 'Erro Desdobrar_ProcMat_Repasse! ' || SQLERRM;
				wheb_mensagem_pck.exibir_mensagem_abort(194264, 'SQLERRM_P='||SQLERRM);
		end;
	else
		begin
		insert into procedimento_repasse (	
			NR_SEQUENCIA, 
			NR_SEQ_PROCEDIMENTO, 
			VL_REPASSE, 
			DT_ATUALIZACAO, 
			NM_USUARIO, 
			NR_SEQ_TERCEIRO,
			NR_LOTE_CONTABIL, 
			NR_REPASSE_TERCEIRO, 
			CD_CONTA_CONTABIL,
			NR_SEQ_TRANS_FIN, 
			VL_LIBERADO,
			NR_SEQ_ITEM_RETORNO, 
			IE_STATUS,
			NR_SEQ_ORIGEM,
			CD_REGRA, 
			DT_LIBERACAO, 
			CD_MEDICO,
			dt_contabil_titulo,
			dt_contabil,
			nr_seq_partic)
		select	nr_seq_proc_rep_novo_w, 
			NR_SEQ_PROCEDIMENTO, 
			vl_repasse_p,
			sysdate, 
			nm_usuario_p,
			NR_SEQ_TERCEIRO, 
			nr_lote_contabil, 

			null, 
			null, 
			NR_SEQ_TRANS_FIN,
			0,
			null, 
			nvl(ie_status_p,'A'),
			NR_SEQUENCIA,
			CD_REGRA,
			decode(ie_status_p,'G', nvl(dt_retorno_w, sysdate), null), 
			CD_MEDICO, 
			dt_contabil_titulo,
			dt_contabil, 
			nr_seq_partic
		from	procedimento_repasse
		where	nr_sequencia = nr_seq_proc_repasse_p; 
		exception 
			when others then 
				---20011, 'Erro Desdobrar_ProcMat_Repasse! ' || SQLERRM;
				wheb_mensagem_pck.exibir_mensagem_abort(194264, 'SQLERRM_P='||SQLERRM);
		end;
	end if;

	if	(ie_atualizar_p = 'S') then
		select	max(a.nr_interno_conta) 
		into	nr_interno_conta_w
		from	procedimento_paciente a,
			procedimento_repasse b
		where	a.nr_sequencia	= b.nr_seq_procedimento
		and 	b.nr_sequencia	= nr_seq_proc_repasse_p;
	else
		select	max(a.nr_interno_conta) 
		into	nr_interno_conta_w
		from	procedimento_paciente a,
			procedimento_repasse b
		where	a.nr_sequencia	= b.nr_seq_procedimento
		and 	b.nr_sequencia	= nr_seq_proc_repasse_p;
	end if;
	
	if	(ie_atualizar_p = 'S') and
		(nr_interno_conta_w is not null) then
		gerar_procmat_repasse_nf(nr_interno_conta_w, nm_usuario_p, 'N');
	end if;
	end; 

elsif	(nr_seq_mat_repasse_p is not null) and 
	(nr_seq_mat_repasse_p > 0) then

	if	(ie_atualizar_p = 'S') then
		select	max(c.nr_seq_retorno) 
		into	nr_seq_retorno_w 
		from	convenio_retorno_item c,
			convenio_retorno_glosa b, 
			material_repasse a 
		where	c.nr_sequencia	= b.nr_seq_ret_item
		and	b.nr_sequencia	= a.nr_seq_ret_glosa 
		and	a.nr_sequencia	= nr_seq_mat_repasse_p;
	else
		select	max(c.nr_seq_retorno) 
		into	nr_seq_retorno_w 
		from	convenio_retorno_item c,
			convenio_retorno_glosa b, 
			material_repasse a 
		where	c.nr_sequencia	= b.nr_seq_ret_item
		and	b.nr_sequencia	= a.nr_seq_ret_glosa 
		and	a.nr_sequencia	= nr_seq_mat_repasse_p;
	end if;

	if	(nr_seq_retorno_w is not null) then
		select	decode(b.ie_data_lib_repasse_ret,'F',nvl(a.dt_baixa_cr, a.dt_fechamento),'R',a.dt_retorno, 'E', a.dt_fechamento) 
		into	dt_retorno_w
		from	parametro_faturamento b, 
			convenio_retorno a
		where	a.nr_sequencia		= nr_seq_retorno_w 
		and	a.cd_estabelecimento	= b.cd_estabelecimento;
	end if;

	select	material_repasse_seq.NextVal 
	into	nr_seq_mat_rep_novo_w 
	from	dual;

	if	(ie_atualizar_p = 'S') then
		insert into material_repasse (	
			NR_SEQUENCIA,
			NR_SEQ_MATERIAL, 
			VL_REPASSE,
			DT_ATUALIZACAO,
			NM_USUARIO,
			NR_SEQ_TERCEIRO, 
			NR_LOTE_CONTABIL,
			NR_REPASSE_TERCEIRO,
			CD_CONTA_CONTABIL,
			NR_SEQ_TRANS_FIN,
			VL_LIBERADO, 
			NR_SEQ_ITEM_RETORNO,
			IE_STATUS, 
			NR_SEQ_ORIGEM, 
			CD_REGRA,
			DT_LIBERACAO,
			CD_MEDICO, 
			dt_contabil_titulo, 
			dt_contabil) 
		select	nr_seq_mat_rep_novo_w,
			NR_SEQ_MATERIAL, 
			vl_repasse_p,
			sysdate, 
			nm_usuario_p,
			NR_SEQ_TERCEIRO, 
			nr_lote_contabil, 

			null, 
			null, 
			NR_SEQ_TRANS_FIN,
			0,
			null, 
			nvl(ie_status_p,'A'),
			NR_SEQUENCIA,
			CD_REGRA,
			decode(ie_status_p,'G', nvl(dt_retorno_w, sysdate), null), 
			CD_MEDICO, 
			dt_contabil_titulo,
			dt_contabil
		from	material_repasse 
		where	nr_sequencia = nr_seq_mat_repasse_p;
	else
		insert into material_repasse (	
			NR_SEQUENCIA,
			NR_SEQ_MATERIAL, 
			VL_REPASSE,
			DT_ATUALIZACAO,
			NM_USUARIO,
			NR_SEQ_TERCEIRO, 
			NR_LOTE_CONTABIL,
			NR_REPASSE_TERCEIRO,
			CD_CONTA_CONTABIL,
			NR_SEQ_TRANS_FIN,
			VL_LIBERADO, 
			NR_SEQ_ITEM_RETORNO,
			IE_STATUS, 
			NR_SEQ_ORIGEM, 
			CD_REGRA,
			DT_LIBERACAO,
			CD_MEDICO, 
			dt_contabil_titulo, 
			dt_contabil) 
		select	nr_seq_mat_rep_novo_w,
			NR_SEQ_MATERIAL, 
			vl_repasse_p,
			sysdate, 
			nm_usuario_p,
			NR_SEQ_TERCEIRO, 
			nr_lote_contabil, 

			null, 
			null, 
			NR_SEQ_TRANS_FIN,
			0,
			null, 
			nvl(ie_status_p,'A'),
			NR_SEQUENCIA,
			CD_REGRA,
			decode(ie_status_p,'G', nvl(dt_retorno_w, sysdate), null), 
			CD_MEDICO, 
			dt_contabil_titulo,
			dt_contabil
		from	material_repasse 
		where	nr_sequencia = nr_seq_mat_repasse_p;
	end if;

	if	(ie_atualizar_p = 'S') then
		select	max(a.nr_interno_conta) 
		into	nr_interno_conta_w
		from	material_atend_paciente a,
			material_repasse b 
		where	a.nr_sequencia	= b.nr_seq_material
		and	b.nr_sequencia	= nr_seq_mat_repasse_p;
	else
		select	max(a.nr_interno_conta) 
		into	nr_interno_conta_w
		from	material_atend_paciente a,
			material_repasse b 
		where	a.nr_sequencia	= b.nr_seq_material
		and	b.nr_sequencia	= nr_seq_mat_repasse_p;
	end if;
	
	if	(ie_atualizar_p = 'S') and
		(nr_interno_conta_w is not null) then
		gerar_procmat_repasse_nf(nr_interno_conta_w, nm_usuario_p, 'N');
	end if;
end if; 

nr_seq_proc_rep_novo_p		:= nr_seq_proc_rep_novo_w;
nr_seq_mat_rep_novo_p		:= nr_seq_mat_rep_novo_w;

--commit; Bruna OS79739 comentado o commit 

end hsa_gerar_w_repasse3;
/