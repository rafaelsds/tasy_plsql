create or replace
PROCEDURE Grava_Amostra_Rack(nm_usuario_p	         varchar2,                             
                             cd_barras_p           varchar2,
                             cd_estab_p            number,
                             nr_seq_armazem_rack_p number,
                             nr_seq_lab_am_rack_p  number,
                             amostra_gravada_p out varchar2) IS 
                             
ie_padrao_amostra_w       lab_parametro.ie_padrao_amostra%type;
nr_prescricao_w           prescr_procedimento.nr_prescricao%type;
nr_seq_prescr_w           prescr_procedimento.nr_sequencia%type;
dt_prior_codigo_barras_w  varchar2(2);
gravado_w                 varchar2(1);
nr_linha_w                number(2,0);
nr_coluna_w               number(6,0);
ie_forma_insercao_w       varchar(5);
qt_barras_w               number(10,0);
prescricao_valida_w       char(1);
nr_seq_lab_soro_am_rack_w lab_soro_amostra_rack.nr_sequencia%type;
nr_sequencia_info_w       lab_soro_processo_info.nr_sequencia%type;
cd_barras_formatado_w     prescr_proc_mat_item.cd_barras%type;
cd_material_exame_w       material_exame_lab.cd_material_exame%type;
nr_Seq_material_w         material_exame_lab.nr_sequencia%type;

nr_seq_exame_w exame_laboratorio.nr_seq_exame%type;
barras_padrao_amostra_w  varchar(15);
qt_prescr_barras_material_w number(10,0);
separador_barras_material_w number(10,0);
separador_barras_exame_w number(10,0);

cursor c01 is
  select  a.nr_prescricao,
          a.nr_sequencia          
    FROM  prescr_procedimento a,
          prescr_proc_mat_item g
    WHERE a.nr_prescricao = g.nr_prescricao              
      AND a.nr_sequencia = g.nr_seq_prescr 
      and g.cd_barras = cd_barras_formatado_w;

cursor c02 is
  select  a.nr_prescricao,
          a.nr_sequencia
    FROM  prescr_procedimento a
    where  nr_prescricao = nr_prescricao_w
    and CD_MATERIAL_EXAME = CD_MATERIAL_EXAME_w;
   
cursor c03 is
  select  a.nr_prescricao,
          a.nr_sequencia
    FROM  prescr_procedimento a
    where  nr_prescricao = nr_prescricao_w
    and  nr_seq_exame = nr_seq_exame_w;
   
BEGIN


  select max(ie_padrao_amostra)
    into ie_padrao_amostra_w
    from lab_parametro 
   where cd_estabelecimento = cd_estab_p;
   
  dt_prior_codigo_barras_w := lab_obter_valor_parametro(722,373);   
  
  gravado_w := 'N';
  separador_barras_material_w := INSTR(cd_barras_p, 'M');
  separador_barras_exame_w := INSTR(cd_barras_p, 'E');
  
  if (ie_padrao_amostra_w in ('AMO9','AMO10','AMO11','AM11F','AM10F','AMO13','WEB')) then
    cd_barras_formatado_w := to_char(to_number(cd_barras_p));
  else
   cd_barras_formatado_w := cd_barras_p;
  end if;
  
  select count(*)
  into qt_barras_w
  from prescr_proc_material
  where cd_barras = cd_barras_formatado_w;
  
  if(qt_barras_w = 0) then

	if(separador_barras_material_w > 0) then
	
		nr_Seq_material_w := substr(cd_barras_p,separador_barras_material_w+1,length(cd_barras_p));
		nr_prescricao_w := substr(cd_barras_p,1,separador_barras_material_w-1);
		
		begin
			select cd_material_exame
			into cd_material_exame_w
			from material_exame_lab
			where nr_sequencia = nr_Seq_material_w;
		exception 
			when no_data_found then
			cd_material_exame_w := '';
		end;
		
		select decode(count(*),0,'N','S')
		into prescricao_valida_w
		from prescr_procedimento
		where nr_prescricao = nr_prescricao_w
		and cd_material_exame = cd_material_exame_w;
	
	elsif (separador_barras_exame_w > 0) then
	
		nr_seq_exame_w := substr(cd_barras_p,separador_barras_exame_w+1,length(cd_barras_p));

		nr_prescricao_w := substr(cd_barras_p,1,separador_barras_exame_w-1);
		
		select decode(count(*),0,'N','S')
		into prescricao_valida_w
		from prescr_procedimento
		where nr_prescricao = nr_prescricao_w
		and nr_seq_exame = nr_seq_exame_w;

	end if;
	
  else
	prescricao_valida_w := 'S';
  end if;
  
  
  if (prescricao_valida_w = 'S') then
    if (nr_seq_lab_am_rack_p is null) then
      begin
        select nvl(lsr.ie_forma_insercao, lab_obter_valor_parametro(728,1)) ie_forma_insercao
          into ie_forma_insercao_w
          from LAB_SORO_ARMAZENA_RACK LSAR
          left outer join lab_soro_rack lsr on lsr.nr_sequencia = lsar.nr_seq_rack 
         where LSAR.nr_sequencia = nr_seq_armazem_rack_p;
    
        if ((ie_forma_insercao_w = 'UBLRL') or
            (ie_forma_insercao_w = 'UBRLL')) then
          select min(nr_pos_linha)
            into nr_linha_w
            from lab_soro_amostra_rack
           where ie_status = 'L' and
                 nr_seq_armazem_rack = nr_seq_armazem_rack_p;
                 
          if (ie_forma_insercao_w = 'UBLRL') then /*De cima para baixo, da esquerda para direita (linha)*/          
            select min(nr_pos_coluna)
              into nr_coluna_w
              from lab_soro_amostra_rack
             where ie_status = 'L' and
                   nr_pos_linha = nr_linha_w and
                   nr_seq_armazem_rack = nr_seq_armazem_rack_p;
                   
          elsif (ie_forma_insercao_w = 'UBRLL') then /*De cima para baixo, da direita para esquerda (linha)*/          
            select max(nr_pos_coluna)
              into nr_coluna_w
              from lab_soro_amostra_rack
             where ie_status = 'L' and
                   nr_pos_linha = nr_linha_w and           
                   nr_seq_armazem_rack = nr_seq_armazem_rack_p;
          end if;         
                 
        elsif ((ie_forma_insercao_w = 'BULRL') or 
               (ie_forma_insercao_w = 'BURLL')) then
          select max(nr_pos_linha)
            into nr_linha_w
            from lab_soro_amostra_rack
           where ie_status = 'L' and
                 nr_seq_armazem_rack = nr_seq_armazem_rack_p;
                 
          if (ie_forma_insercao_w = 'BULRL') then /*De baixo para cima, da esquerda para direita (linha)*/
            select min(nr_pos_coluna)
              into nr_coluna_w
              from lab_soro_amostra_rack
             where ie_status = 'L' and
                   nr_pos_linha = nr_linha_w and
                   nr_seq_armazem_rack = nr_seq_armazem_rack_p;                 
                 
          elsif (ie_forma_insercao_w = 'BURLL') then /*De baixo para cima, da direita para esquerda (linha)*/
            select max(nr_pos_coluna)
              into nr_coluna_w
              from lab_soro_amostra_rack
             where ie_status = 'L' and
                   nr_pos_linha = nr_linha_w and           
                   nr_seq_armazem_rack = nr_seq_armazem_rack_p;                   
          end if;         
                 
        elsif ((ie_forma_insercao_w = 'BULRC') or 
               (ie_forma_insercao_w = 'UBLRC')) then
          select min(nr_pos_coluna)
            into nr_coluna_w
            from lab_soro_amostra_rack
           where ie_status = 'L' and
                 nr_seq_armazem_rack = nr_seq_armazem_rack_p;
                 
          if (ie_forma_insercao_w = 'BULRC') then /*De baixo para cima, da esquerda para direita (coluna)*/                 
            select max(nr_pos_linha) 
              into nr_linha_w
              from lab_soro_amostra_rack
             where ie_status = 'L' and
                   nr_pos_coluna = nr_coluna_w and
                   nr_seq_armazem_rack = nr_seq_armazem_rack_p;                 

          elsif (ie_forma_insercao_w = 'UBLRC') then /*De cima para baixo, da esquerda para direita (coluna)*/
            select min(nr_pos_linha)
              into nr_linha_w
              from lab_soro_amostra_rack
             where ie_status = 'L' and
                   nr_pos_coluna = nr_coluna_w and           
                   nr_seq_armazem_rack = nr_seq_armazem_rack_p;                 
          end if;         
                 
        elsif ((ie_forma_insercao_w = 'BURLC') or 
               (ie_forma_insercao_w = 'UBRLC')) then
          select max(nr_pos_coluna)
            into nr_coluna_w
            from lab_soro_amostra_rack
           where ie_status = 'L' and
                 nr_seq_armazem_rack = nr_seq_armazem_rack_p;
                 
          if (ie_forma_insercao_w = 'BURLC') then /*De baixo para cima, da direita para esquerda (coluna)*/
            select max(nr_pos_linha) 
              into nr_linha_w
              from lab_soro_amostra_rack
             where ie_status = 'L' and
                   nr_pos_coluna = nr_coluna_w and
                   nr_seq_armazem_rack = nr_seq_armazem_rack_p;                 
                 
          elsif (ie_forma_insercao_w = 'UBRLC') then /*De cima para baixo, da direita para esquerda (coluna)*/
            select min(nr_pos_linha)
              into nr_linha_w
              from lab_soro_amostra_rack
             where ie_status = 'L' and
                   nr_pos_coluna = nr_coluna_w and           
                   nr_seq_armazem_rack = nr_seq_armazem_rack_p;                                  
          end if;
        end if;            
        
        select max(nr_sequencia)
          into nr_seq_lab_soro_am_rack_w
          from lab_soro_amostra_rack
         where nr_pos_linha = nr_linha_w and
               nr_pos_coluna = nr_coluna_w;               

      end;         
    else 
      nr_seq_lab_soro_am_rack_w := nr_seq_lab_am_rack_p;
    end if;
           
    update lab_soro_amostra_rack set ds_amostra = cd_barras_p, ie_status = 'O'
     where nr_sequencia = nr_seq_lab_soro_am_rack_w;
   
    select max(nr_sequencia)
      into nr_sequencia_info_w
      from lab_soro_processo_info
     where IE_ACAO = 'AR' and
           nr_seq_armazena_rack = nr_seq_armazem_rack_p and
           nr_seq_amostra_rack is null;
   
    if (nr_sequencia_info_w is null) then
      insert into lab_soro_processo_info (nr_sequencia,
                                          ie_acao,
                                          dt_acao,
                                          nr_seq_armazena_rack,
                                          nr_seq_amostra_rack,
                                          dt_atualizacao,
                                          nm_usuario)
                                  values (lab_soro_processo_info_seq.nextval,
                                          'AR',
                                          sysdate, 
                                          nr_seq_armazem_rack_p,
                                          nr_seq_lab_soro_am_rack_w,
                                          sysdate,
                                          nm_usuario_p);   
    else
      update lab_soro_processo_info set dt_acao = sysdate, 
                                        dt_atualizacao = sysdate,
                                        nm_usuario = nm_usuario_p,
                                        nr_seq_amostra_rack = nr_seq_lab_soro_am_rack_w
      where nr_sequencia = nr_sequencia_info_w;                                  
    end if;
	
    if(qt_barras_w > 0) then /*Executa quando o c�digo de barras est� inserido na tabela prescr_proc_mat_item*/
	    open C01;
	    loop
	      fetch c01 into	
		nr_prescricao_w,
		nr_seq_prescr_w;
	      exit when c01%notfound;
		inserir_amostra_rack( nm_usuario_p, nr_prescricao_w, nr_seq_prescr_w, nr_seq_lab_soro_am_rack_w);
	    end loop;
	    close c01; 
	    gravado_w := 'S';
    
    elsif (separador_barras_material_w > 0) then /*Executa quando o c�digo de barras � Prescri��o + Material*/
                    open C02;
	    loop					  
	      fetch c02 into	nr_prescricao_w,
    	                	nr_seq_prescr_w;
	      exit when c02%notfound;
    		Inserir_Amostra_Rack( nm_usuario_p, nr_prescricao_w, nr_seq_prescr_w, nr_seq_lab_soro_am_rack_w);
	    end loop;
	    close c02; 
	    gravado_w := 'S';
    
    elsif(separador_barras_exame_w > 0) then /*Executa quando o c�digo de barras � Prescri��o + Exame*/
 	    open C03;
	    loop			  
	      fetch c03 into nr_prescricao_w,
                    	 nr_seq_prescr_w;
	      exit when c03%notfound;
     		Inserir_Amostra_Rack( nm_usuario_p, nr_prescricao_w, nr_seq_prescr_w, nr_seq_lab_soro_am_rack_w);
	    end loop;
	    close c03;
	    gravado_w := 'S';
    
    end if;
  end if;
  
  commit;  
  amostra_gravada_p := gravado_w;
end grava_amostra_rack;
/
