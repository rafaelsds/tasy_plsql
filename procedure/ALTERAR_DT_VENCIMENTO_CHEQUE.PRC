create or replace
procedure alterar_dt_vencimento_cheque(	nr_seq_cheque_p		number,
					dt_vencimento_atual_p	date,
					nm_usuario_p		varchar2) is 

begin
if 	(nr_seq_cheque_p is not null) and
	(dt_vencimento_atual_p is not null) and
	(nm_usuario_p is not null) then

	update	cheque_cr
	set    	dt_vencimento_atual = trunc(dt_vencimento_atual_p,'dd'),
		dt_atualizacao = sysdate,
		nm_usuario     = nm_usuario_p
	where  	nr_seq_cheque = nr_seq_cheque_p;

	commit;

end if;

end alterar_dt_vencimento_cheque;
/