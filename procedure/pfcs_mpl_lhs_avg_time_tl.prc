create or replace procedure pfcs_mpl_lhs_avg_time_tl (
	nr_seq_indicator_p		  number,
	cd_estabelecimento_p	  number,
	nm_usuario_p			  varchar2
) is

nr_seq_panel_w				pfcs_panel_detail.nr_seq_panel%type;
nr_average_time_on_tele		pfcs_panel.vl_indicator%type;
nr_total_sequence_w			number(15) := 0;
nr_total_time_on_tele		number(15) 	:= 0;

--Cursor to fetch location
cursor cur_get_unit_for_pfcs is
select 	sec.ds_Setor_atendimento ds_department
from    pfcs_encounter enc,
		pfcs_patient pat,
		pfcs_encounter_location el,
		unidade_atendimento uni,
		setor_atendimento   sec
where 	enc.si_status in ('PLANNED', 'ARRIVED')
		and enc.nr_seq_patient = pat.nr_sequencia
		and pat.ie_active = '1'
		and el.nr_seq_encounter = enc.nr_sequencia
		and uni.nr_seq_location = el.nr_seq_location
		and uni.cd_setor_atendimento = sec.cd_setor_atendimento
		and sec.cd_estabelecimento_base = cd_estabelecimento_p
		and sec.ie_situacao = 'A'
		and uni.ie_situacao = 'A'
		and sec.cd_classif_setor = '3'
group by sec.ds_Setor_atendimento;

/* Cursor to read from pfcs integration tables */
cursor cur_get_recom_time_pfcs(nr_seq_location_p varchar2) is
select 	sr.nr_sequencia,
		pfcs_get_tele_time(dev.nr_sequencia, 'S') nr_req_time_on_tele
from 	pfcs_service_request sr,
		pfcs_encounter enc,
		pfcs_patient pat,
		pfcs_device dev,
		pfcs_encounter_location el,
		unidade_atendimento uni,
		setor_atendimento   sec
where	((sr.si_status = 'COMPLETED' and sr.cd_service = 'E0404')
		or (sr.si_status = 'ACTIVE' and sr.cd_service = 'E0403' ))
		and sr.nr_seq_encounter = enc.nr_sequencia
		and enc.si_status in ('PLANNED', 'ARRIVED')
		and enc.nr_seq_patient = pat.nr_sequencia
		and pat.ie_active = '1'
		and el.nr_seq_encounter = enc.nr_sequencia
		and uni.nr_seq_location = el.nr_seq_location
		and uni.cd_setor_atendimento = sec.cd_setor_atendimento
		and sec.cd_estabelecimento_base = cd_estabelecimento_p
		and sec.ie_situacao = 'A'
		and uni.ie_situacao = 'A'
		and sec.cd_classif_setor = '3'
		and dev.nr_seq_patient = pat.nr_sequencia
		and dev.si_status = 'ACTIVE'
		and dev.ds_device_type = 'Monitor'
		and sec.ds_setor_atendimento = nr_seq_location_p;
begin

/* Read from PFCS Integration cursor*/
for cur_get_unit in cur_get_unit_for_pfcs loop
	nr_total_sequence_w  := 0;
	nr_total_time_on_tele := 0;

	for rec_get_recom_time in cur_get_recom_time_pfcs(cur_get_unit.ds_department) loop
		nr_total_sequence_w  := nr_total_sequence_w + 1;
		nr_total_time_on_tele  := nr_total_time_on_tele + rec_get_recom_time.nr_req_time_on_tele;
	end loop;

	if(nr_total_sequence_w = 0) then
		nr_average_time_on_tele := 0;
	else
		nr_average_time_on_tele := round(nr_total_time_on_tele / nr_total_sequence_w);
	end if;

	pfcs_pck_v2.pfcs_generate_results(
        vl_indicator_p => nr_average_time_on_tele,
		ds_reference_value_p => cur_get_unit.ds_department,
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);
end loop;

pfcs_pck_v2.pfcs_activate_records(
    nr_seq_indicator_p => nr_seq_indicator_p,
    nr_seq_operational_level_p => cd_estabelecimento_p,
    nm_usuario_p => nm_usuario_p);

end pfcs_mpl_lhs_avg_time_tl;
/
