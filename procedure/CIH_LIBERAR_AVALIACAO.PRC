CREATE OR REPLACE 
PROCEDURE cih_liberar_avaliacao(nr_sequencia_p NUMBER ,
		  	                          nm_usuario_lib_aval_p VARCHAR2) IS				
BEGIN

IF  (nr_sequencia_p IS NOT NULL) THEN
	UPDATE cih_contato
	SET	nm_usuario_lib_aval =  nm_usuario_lib_aval_p,
	       	dt_lib_avaliacao 	 =  SYSDATE
	WHERE  nr_sequencia  	 =  nr_sequencia_p;
END IF;

COMMIT;
END cih_liberar_avaliacao;
/