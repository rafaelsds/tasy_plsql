CREATE OR REPLACE PROCEDURE gerar_solicitacao_leito (
                                                    dt_prevista_p                gestao_vaga.dt_prevista%TYPE,
													cd_convenio_p                gestao_vaga.cd_convenio%TYPE,
                                                    cd_pessoa_fisica_p           gestao_vaga.cd_pessoa_fisica%TYPE DEFAULT NULL,
                                                    nm_paciente_p                gestao_vaga.nm_paciente%TYPE DEFAULT NULL,
                                                    cd_departamento_desejado_p   gestao_vaga.cd_departamento_desejado%TYPE DEFAULT NULL,
                                                    cd_setor_desejado_p          gestao_vaga.cd_setor_desejado%TYPE DEFAULT NULL,
                                                    cd_unidade_basica_p          gestao_vaga.cd_unidade_basica%TYPE DEFAULT NULL,
                                                    cd_unidade_compl_p           gestao_vaga.cd_unidade_compl%TYPE DEFAULT NULL,
                                                    cd_tipo_acomod_desej_p       gestao_vaga.cd_tipo_acomod_desej%TYPE DEFAULT NULL
) IS
    nr_sequencia_gv_w NUMBER;
BEGIN

    IF ( (cd_pessoa_fisica_p IS NOT NULL OR nm_paciente_p IS NOT NULL) AND cd_convenio_p IS NOT NULL) THEN
        SELECT
            gestao_vaga_seq.NEXTVAL
        INTO nr_sequencia_gv_w
        FROM
            dual;

        INSERT INTO
		gestao_vaga
		(nr_sequencia,
		cd_estabelecimento,
		dt_solicitacao,
		dt_atualizacao,
		nm_usuario,
		ie_status,
		ie_tipo_vaga,
		ie_solicitacao,
		cd_convenio,
		dt_prevista,
		cd_pessoa_fisica,
		nm_paciente,
		cd_departamento_desejado,
		cd_setor_desejado,
		cd_unidade_basica,
		cd_unidade_compl,
		cd_tipo_acomod_desej) 
		VALUES
		(nr_sequencia_gv_w,
		obter_estabelecimento_ativo,
		sysdate,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		'A',
		'CG',
		'I',
		cd_convenio_p,
		dt_prevista_p,
		cd_pessoa_fisica_p,
		nm_paciente_p,
		cd_departamento_desejado_p,
		cd_setor_desejado_p,
		cd_unidade_basica_p,
		cd_unidade_compl_p,
		cd_tipo_acomod_desej_p);

    END IF;

    COMMIT;
END gerar_solicitacao_leito;
/