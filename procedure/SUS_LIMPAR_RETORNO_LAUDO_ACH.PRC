create or replace
procedure sus_limpar_retorno_laudo_ach(	nr_seq_lote_p	number,
					nm_usuario_p	Varchar2) is 

begin

delete from sus_retorno_laudo_ach
where nr_lote = nr_seq_lote_p;

commit;

end sus_limpar_retorno_laudo_ach;
/