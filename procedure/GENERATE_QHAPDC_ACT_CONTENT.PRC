create or replace PROCEDURE Generate_qhapdc_act_content(

    nr_seq_dataset_p             NUMBER,

    nm_usuario_p                 VARCHAR2,

    returned_value_p OUT nocopy  NUMBER,

    other_exception_p OUT nocopy VARCHAR2)

IS

  ds_segment_w VARCHAR2(32767);

  ds_segment_clob_w CLOB              := empty_clob;

  is_clob_insertion_successful NUMBER := 0;

  excp_caught_dataset_clob_ins VARCHAR2( 500 );

BEGIN

  BEGIN

    SELECT 'N' -- Record Identifier

      || Lpad(nr_atendimento, 12, '0') -- Unique Number 

      || Lpad(Obter_pessoa_atendimento(nr_atendimento, 'C'), 8, '0') --Patient Identifier 

      || Lpad(nr_atendimento, 12, '0') -- Admission Number

      || 'W'                           -- Activity Code

      || DECODE(cd_department_encounter, NULL, '000000' ,Lpad(cd_department_encounter, 6, '0'))     -- Ward

      || DECODE(cd_basic_care_unit, NULL, '    ' ,Lpad(trim(cd_basic_care_unit),4,'0'))     

      || DECODE(cd_standard_unit_code, NULL, '0000' ,Lpad(trim(cd_standard_unit_code),4,'0'))  -- Standard Unit Code

      || DECODE(dt_dept_entry, NULL, '        ' ,  TO_CHAR(dt_dept_entry, 'yyyymmdd'))   

      || DECODE(dt_dept_entry, NULL, '    ' , TO_CHAR(dt_dept_entry, 'hhmm'))

      || Lpad(' ', 4, ' ') --Standard Ward Code
      
      || chr(10)

    INTO ds_segment_w

    FROM qhapdc_segment_act

    WHERE nr_dataset = nr_seq_dataset_p;

  EXCEPTION

  WHEN OTHERS THEN

    ds_segment_w := NULL;

  END;

  SELECT Concat(ds_segment_clob_w, ds_segment_w)

  INTO ds_segment_clob_w

  FROM dual;

  Insert_dataset_content(nm_usuario_p, nr_seq_dataset_p, ds_segment_clob_w, is_clob_insertion_successful, excp_caught_dataset_clob_ins);

  IF( is_clob_insertion_successful = 1 ) THEN

    returned_value_p              :=1;

    other_exception_p             :=NULL;

  ELSE

    IF( excp_caught_dataset_clob_ins IS NOT NULL ) THEN

      returned_value_p               :=0;

      other_exception_p              := excp_caught_dataset_clob_ins;

    END IF;

  END IF;

END generate_qhapdc_act_content;
/