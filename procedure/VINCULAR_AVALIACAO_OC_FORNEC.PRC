create or replace
procedure vincular_avaliacao_oc_fornec(
					nr_ordem_compra_p	in	number,
					nr_avaliacao_p		in	varchar2) is
					
begin

update 	ordem_compra
set	nr_avaliacao_fornec = nr_avaliacao_p
where 	nr_ordem_compra = nr_ordem_compra_p;

commit;

end vincular_avaliacao_oc_fornec;
/
