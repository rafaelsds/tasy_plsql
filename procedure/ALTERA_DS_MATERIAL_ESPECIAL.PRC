create or replace
procedure altera_ds_material_especial(	nr_sequencia_p			number,
										nr_prescricao_p			number,
										ds_material_especial_p 	varchar2,
										nm_usuario_p			varchar2) is 

begin
update 	prescr_procedimento
set 	ds_material_especial	=	ds_material_especial_p,
	nm_usuario		=   	nm_usuario_p
where 	nr_sequencia		=	nr_sequencia_p
and	nr_prescricao		=	nr_prescricao_p;

commit;

end altera_ds_material_especial;
/