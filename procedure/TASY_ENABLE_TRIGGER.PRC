create or replace PROCEDURE Tasy_Enable_Trigger IS

ds_comando_w			Varchar2(2000);
nm_trigger_w			Varchar2(50);

sofar_w number(10) := 0;
n_cleanupdb_pck_w number(10) := 0;
nm_fase_atualizacao_w varchar2(20) := 'DATABASE_CLEANUP';
b_cleanupdb number(1) := 0;
l_sql_chk_cleanupdb_table varchar2(1000) := 'select count(1) from user_tables where table_name = ''CLEANUPDB_TABLE'' and status = ''VALID''';

CURSOR C010 IS
	select trigger_name
	from user_triggers
	where status = 'DISABLED';

BEGIN

SELECT COUNT(1)
INTO n_cleanupdb_pck_w
FROM USER_OBJECTS
WHERE OBJECT_NAME = 'CLEANUPDB_PCK'
AND STATUS = 'VALID';

OPEN C010;
LOOP
	FETCH C010 INTO 	nm_trigger_w;
	EXIT WHEN C010%NOTFOUND;

	ds_comando_w	:= 'Alter trigger ' ||  nm_trigger_w  || ' enable';
	Exec_Sql_Dinamico(nm_trigger_w || ' Enable trigger', ds_comando_w);
    if (n_cleanupdb_pck_w > 1) then
      execute immediate l_sql_chk_cleanupdb_table into b_cleanupdb;
      if (b_cleanupdb > 0) then
        execute immediate 'begin cleanupdb_pck.add_progress(:nm_fase_atualizacao_w, 1); end;' using nm_fase_atualizacao_w;
      end if;
    end if;
    
END LOOP;
CLOSE C010;

END Tasy_enable_trigger;
/