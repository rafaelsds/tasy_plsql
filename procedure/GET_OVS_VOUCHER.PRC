create or replace PROCEDURE Get_ovs_voucher(nr_seq_claim_p   IN 
											ovs_claim.nr_sequencia%TYPE, 
                                            nm_usuario_p     IN 
											ovs_claim.nm_usuario%TYPE, 
                                            ie_validate_p    IN VARCHAR2, 
                                            nr_seq_account_p IN 
											ovs_claim.nr_account%TYPE, 
                                            ie_imc_type_p    IN VARCHAR2) 
IS 
  nr_sequencia_voucher_w  ovs_voucher.nr_sequencia%TYPE; 
  new_voucher_ind_w       NUMBER(1) := 1; 
  ie_selfdeemed_w         VARCHAR(2); 
  nr_sequencia_w          NUMBER(10); 
  dt_service_w            DATE; 
  nr_records_w            PLS_INTEGER; 
  service_count_number    VARCHAR2(4); 
  cd_service_code_w       NUMBER := 0; 
  cd_provider_w           dva_claim.cd_provider%TYPE; 
  has_proc_w              PLS_INTEGER := 0; 
  contador_w              PLS_INTEGER := 0; 
  voucher_date_w          DATE; 
  nr_voucher_w            VARCHAR(2); 
  nr_voucher_count_w      NUMBER(3); 
  curr_cd_medico_resp_w   VARCHAR(10); 
  dt_admission_w          atendimento_paciente.dt_entrada%TYPE; 
  dt_discharge_w          atendimento_paciente.dt_alta%TYPE; 
  dt_birth_w              pessoa_fisica.dt_nascimento%TYPE; 
  nm_first_name_w         ovs_voucher.nm_first%TYPE; 
  nm_family_name_w        ovs_voucher.nm_family%TYPE; 
  cd_fund_card_w          ovs_voucher.cd_fund_card%TYPE; 
  cd_upi_w                ovs_voucher.cd_upi%TYPE; 
  ie_gender_w             ovs_voucher.ie_gender%TYPE; 
  nm_second_name_w        ovs_voucher.nm_second%TYPE; 
  nr_atendimento_w        atendimento_paciente.nr_atendimento%TYPE; 
  dt_atualizacao_nrec_w   parecer_medico_req.dt_atualizacao_nrec%TYPE; 
  nr_seq_account_w        ovs_claim.nr_account%TYPE := nr_seq_account_p; 
  nr_seq_person_name_w    pessoa_fisica.nr_seq_person_name%TYPE; 
  cd_estabelecimento_w    atendimento_paciente.cd_estabelecimento%TYPE; 
  nr_seq_classificacao_w  atendimento_paciente.nr_seq_classificacao%TYPE; 
  nm_alias_family_w       ovs_voucher.nm_alias_family%TYPE; 
  nm_alias_first_w        ovs_voucher.nm_alias_first%TYPE; 
  medico_resp_w           atendimento_paciente.cd_medico_resp%TYPE; 
  nr_provider_w           medical_provider_number.nr_provider%TYPE; 
  ie_accident_w           ovs_claim.ie_accident%TYPE; 
  voucher_id_w            PLS_INTEGER := 0; 
  ie_financial_interest_w ovs_voucher.ie_financial_interest%TYPE := 'false'; 
  ie_claim_type_w         eclipse_parameters.ie_claim_type%TYPE; 
  ie_ifc_w                ovs_voucher.ie_ifc%TYPE := NULL; 
  ie_tipo_atendimento_w   atendimento_paciente.ie_tipo_atendimento%TYPE; 
  ie_hospital_ind_w       VARCHAR2(1); 
  general_procedures_w    VARCHAR2(400); 
  specialist_procedures_w VARCHAR2(400); 
  pathology_procedures_w  VARCHAR2(400); 
  --claimant_ie_authorised_w        ovs_claim.claimant_ie_authorised%type := 'N'; 
  ie_specialist_w         VARCHAR2(10) DEFAULT 'GP'; 
  ie_duplicate_w          ovs_service.ie_duplicate%TYPE; 
  qt_patients_w           ovs_service.qt_patients%TYPE; 
  dt_referral_w           DATE; 
  ie_referral_override_w  VARCHAR2(10); 
  ie_referral_period_w    VARCHAR2(10); 
  dt_request_w            DATE; 
  cd_requesting_w         VARCHAR2(10); 
  ie_request_type_w       VARCHAR2(10); 
  cd_referring_w          VARCHAR2(10); 
  dt_ref_req_w            DATE; 
  req_ref_provider_num_w  medical_provider_number.nr_provider%TYPE; 
  dt_validate_w           DATE; 
  ie_service_type_w       VARCHAR2(10) := 'O'; 
  ie_hospital_w           VARCHAR2(10) := 'true'; 
  cd_equipamento_w        VARCHAR2(10); 
  ie_multiple_procedure_w VARCHAR2(10); 
  cd_referring_tw         VARCHAR2(15); 
  dt_referral_tw          DATE; 
  cd_requesting_tw        VARCHAR2(15); 
  dt_request_tw           DATE; 
  ie_referral_period_tw   VARCHAR2(5); 
  error_flag_w            VARCHAR2(1); 
  cd_medic_w              parecer_medico_req.cd_medico%TYPE; 
  nr_seq_ext_doc_w        NUMBER; 
  CURSOR c01 IS 
    SELECT Nvl(cd_medico_executor, medico_resp_w) AS cd_medico_executor, 
           cd_medico_req                          AS cd_medico_req 
    FROM   procedimento_paciente 
    WHERE  nr_interno_conta = nr_seq_account_w 
    GROUP  BY Nvl(cd_medico_executor, medico_resp_w), 
              cd_medico_req; 
  CURSOR c02 IS 
    SELECT a.nr_sequencia, 
           ( Nvl(a.vl_procedimento, 0) * 100 )         vl_procedimento, 
           a.dt_procedimento, 
           a.cd_equipamento, 
           Nvl(a.cd_procedimento, 0)                   cd_procedimento, 
           Nvl(c.cd_procedimento_loc, 0)               cd_procedimento_loc, 
           a.ie_origem_proced, 
           a.ds_observacao, 
           (SELECT Max(Lpad(Substr(k.nr_provider, 1, 8), 8, '0')) 
            FROM   medical_provider_number k 
            WHERE  k.cd_medico = a.cd_medico_executor) AS cd_medico_resp, 
           Trunc(( a.dt_final_procedimento - a.dt_inicio_procedimento ) * 
                 ( 24 * 60 )) 
                                                       qt_duracao, 
           Substr(b.cd_setor_externo, 0, 5)            cd_setor_externo, 
           Count(a.nr_sequencia) 
             over()                                    qt_item_w 
    FROM   procedimento_paciente a, 
           setor_atendimento b, 
           procedimento c 
    WHERE  a.nr_interno_conta = nr_seq_account_w 
           AND a.cd_setor_atendimento = b.cd_setor_atendimento 
           AND a.cd_procedimento = c.cd_procedimento 
    ORDER  BY a.dt_procedimento, 
              a.cd_medico, 
              a.cd_procedimento; 
BEGIN 
    SELECT Max(a.dt_entrada), 
           Max(a.dt_alta), 
           Max(c.nr_seq_person_name), 
           Max(c.dt_nascimento), 
           Max(c.ie_sexo), 
           Max(a.nr_atendimento), 
           Max(a.cd_estabelecimento), 
           Max(a.cd_medico_resp), 
           Max(a.nr_seq_classificacao), 
           Max(a.ie_tipo_atendimento) 
    INTO   dt_admission_w, dt_discharge_w, nr_seq_person_name_w, dt_birth_w, 
    ie_gender_w, nr_atendimento_w, cd_estabelecimento_w, medico_resp_w, 
    nr_seq_classificacao_w, ie_tipo_atendimento_w 
    FROM   atendimento_paciente a, 
           conta_paciente b, 
           pessoa_fisica c 
    WHERE  a.nr_atendimento = b.nr_atendimento 
           AND a.cd_pessoa_fisica = c.cd_pessoa_fisica 
           AND b.nr_interno_conta = nr_seq_account_w; 

    SELECT Max(a.cd_usuario_convenio), 
           Nvl(Max(a.cd_complemento), 0) 
    INTO   cd_fund_card_w, cd_upi_w 
    FROM   atend_categoria_convenio a 
    WHERE  a.nr_atendimento = nr_atendimento_w; 

    BEGIN 
        SELECT dt_atualizacao_nrec 
        INTO   dt_atualizacao_nrec_w 
        FROM   parecer_medico_req 
        WHERE  nr_atendimento = nr_atendimento_w; 
    EXCEPTION 
        WHEN no_data_found THEN 
          dt_atualizacao_nrec_w := NULL; 
    END; 

    Get_eclipse_service_types(nr_seq_account_p, general_procedures_w, 
    specialist_procedures_w, pathology_procedures_w); 

    IF( specialist_procedures_w IS NOT NULL  AND pathology_procedures_w IS NOT NULL ) THEN 
      wheb_mensagem_pck.Exibir_mensagem_abort(1118140, ' Specialist items = ' || specialist_procedures_w || ' Pathology items = '|| pathology_procedures_w ); 
    END IF; 

    IF ( specialist_procedures_w IS NOT NULL ) THEN 
      ie_service_type_w := 'S'; 
    ELSIF ( pathology_procedures_w IS NOT NULL ) THEN 
      ie_service_type_w := 'P'; 
    ELSE 
      ie_service_type_w := 'O'; 
    END IF; 

    ------------------------------------------------------ 
    IF( ie_service_type_w = 'P' ) THEN 
      ie_request_type_w := 'P'; 
    END IF; 

    SELECT Decode(Max(ie_service_type), 3, 'D',6, 'P',ie_request_type_w) 
    INTO   ie_request_type_w 
    FROM   eclipse_parameters; 

    SELECT Max(a.dt_referencia), 
           Max((SELECT Max(Lpad(Substr(k.nr_provider, 1, 8), 8, '0')) 
           FROM   medical_provider_number k 
           WHERE  k.cd_medico = a.cd_medico)) AS ref_provider_num, 
           Max(a.dt_validade), 
           Max(a.ie_type_code) 
			--decode(ie_service_type_w , 'S' , null) 
    INTO  	dt_ref_req_w, ---- ReferralIssueDate 
			req_ref_provider_num_w, ---- RequestingProviderNum 
			dt_validate_w, ---- Validate date 
			ie_referral_override_w -- referral override indicator, 
			--ie_specialist_w  ---- specialist type 
    FROM   atendimento_paciente_inf a, 
           conta_paciente b 
    WHERE  b.nr_interno_conta = nr_seq_account_p 
           AND a.nr_atendimento = b.nr_atendimento; 

    SELECT Max(c.nr_seq_tipo_medico) 
    INTO   nr_seq_ext_doc_w 
    FROM   conta_paciente a, 
           atendimento_paciente b, 
           pf_medico_externo c 
    WHERE  	a.nr_interno_conta = nr_seq_account_p 
    AND		a.nr_atendimento = b.nr_atendimento 
    AND		b.cd_medico_resp = c.cd_medico; 

    IF( nr_seq_ext_doc_w IS NULL ) THEN 
      --  ie_specialist_w := 'SP'; -- if type is not set then consider as general physician 
      SELECT Max(cd_especialidade) 
      INTO   ie_specialist_w 
      FROM   conta_paciente a, 
             atendimento_paciente b, 
             medico_especialidade c 
      WHERE  a.nr_interno_conta = nr_seq_account_p 
             AND a.nr_atendimento = b.nr_atendimento 
             AND c.cd_pessoa_fisica = b.cd_medico_resp; 
    ELSE 
      SELECT Nvl(Max(m.ie_medico_familia), 'SP') 
      INTO   ie_specialist_w 
      FROM   tipo_medico_externo m 
      WHERE  m.nr_sequencia = nr_seq_ext_doc_w; 
    END IF; 

    SELECT Decode(ie_specialist_w, NULL, 'GP', 'SP') 
    INTO   ie_specialist_w 
    FROM   dual; 

    IF( ie_service_type_w = 'S' ) THEN 
		cd_referring_w := req_ref_provider_num_w; 

		dt_referral_w := dt_ref_req_w; 
    END IF; 

    IF( ie_service_type_w = 'P' or ie_request_type_w ='D') THEN 
		cd_requesting_w := req_ref_provider_num_w; 

		dt_request_w := dt_ref_req_w; 
    END IF; 

    SELECT Decode(ie_specialist_w, NULL, 'GP','S', 'GP','N', 'SP','SP') 
    INTO   ie_specialist_w 
    FROM   dual; 


    IF( ie_service_type_w = 'S' 
        AND dt_referral_w IS NOT NULL ) THEN 

      /* 
      if we set the value of ReferralOverrideTypeCde , value of ReferralPeriodTypeCde should be null;
      */ 
      IF( dt_validate_w IS NULL ) THEN 
        ie_referral_period_w := 'I'; -- Indifinate 
      ELSIF( pkg_date_utils.Get_diffdate(dt_referral_w, dt_validate_w, 'MONTH')  <= 3   AND ie_specialist_w = 'SP' ) THEN 
        ie_referral_period_w := 'S'; 
      ELSIF( pkg_date_utils.Get_diffdate(dt_referral_w, dt_validate_w, 'MONTH')  <= 12  AND ie_specialist_w = 'GP' ) THEN 
        ie_referral_period_w := 'S'; 
      ELSIF( pkg_date_utils.Get_diffdate(dt_referral_w, dt_validate_w, 'MONTH')  >= 3   AND ie_specialist_w = 'SP' ) THEN 
        ie_referral_period_w := 'N'; 
      ELSIF( pkg_date_utils.Get_diffdate(dt_referral_w, dt_validate_w, 'MONTH')  >= 12  AND ie_specialist_w = 'GP' ) THEN 
        ie_referral_period_w := 'N'; 
      END IF; 

    END IF; 

    IF( ie_referral_override_w IS NOT NULL ) THEN 
      cd_referring_w := NULL; 

      dt_referral_w := NULL; 

      cd_requesting_w := NULL; 

      dt_request_w := NULL; 

      ie_referral_period_w := NULL; 
    END IF; 

    IF( ie_service_type_w = 'P' ) THEN 
      ie_duplicate_w := NULL; -- Hard coded to N; 
    END IF; 

    --    cd_referring_tw := cd_referring_w; 
    --    dt_referral_tw := dt_referral_w;  
    --    cd_requesting_tw := cd_requesting_w;  
    --    dt_request_tw :=dt_request_w; 
    --    ie_referral_period_tw :=ie_referral_period_w; 

    -------------------------------------------------------- 
    nm_first_name_w := pkg_name_utils.Get_person_name(nr_seq_person_name_w, cd_estabelecimento_w, 'givenName'); 

	nm_family_name_w := pkg_name_utils.Get_person_name(nr_seq_person_name_w, cd_estabelecimento_w, 'familyName'); 

	nm_second_name_w := Substr( pkg_name_utils.Get_person_name(nr_seq_person_name_w, cd_estabelecimento_w, 'middleName'), 1, 1);

	nm_alias_family_w := pkg_name_utils.Get_person_name(nr_seq_person_name_w, cd_estabelecimento_w, 'familyName', 'social');

	nm_alias_first_w := pkg_name_utils.Get_person_name(nr_seq_person_name_w,cd_estabelecimento_w,  'givenName', 'social'); 

	--  if (ie_hospital_ind_w = 'Y') then 
	--    dt_discharge_w := NULL; 
	--    dt_admission_w := NULL; 
	--  end if; 
	IF ( nm_first_name_w IS NULL 
    OR nm_family_name_w IS NULL ) THEN 
		Generate_inco_eclipse(nr_seq_account_w, 1, Obter_desc_expressao(960562),nm_usuario_p); 
	END IF; 

	IF ( dt_discharge_w > SYSDATE ) THEN 
		Generate_inco_eclipse(nr_seq_account_w, 3, Obter_desc_expressao(960562),nm_usuario_p); 
	END IF; 

	IF ( dt_discharge_w < dt_admission_w ) THEN 
		Generate_inco_eclipse(nr_seq_account_w, 3, Obter_desc_expressao(960562),nm_usuario_p); 
	END IF; 

	IF ( dt_discharge_w < dt_birth_w ) THEN 
		Generate_inco_eclipse(nr_seq_account_w, 3, Obter_desc_expressao(960562),nm_usuario_p); 
	END IF; 

	if(ie_request_type_w ='D') then
		cd_referring_w := null;
		dt_referral_w := null;   
		ie_referral_period_w := null;
		ie_referral_override_w := null;
	end if;

	IF( dt_referral_w IS NOT NULL 
	AND ( cd_requesting_w IS NOT NULL 
           OR dt_request_w IS NOT NULL 
           OR ie_referral_override_w IS NOT NULL ) ) THEN 

			Generate_inco_eclipse(nr_seq_account_p, 1,wheb_mensagem_pck.Get_texto(1112334), nm_usuario_p); --referralissuedate 
			error_flag_w := 'T'; 
	END IF; 

	IF( ie_referral_period_w IS NULL 
    AND ( cd_referring_w IS NOT NULL 
    OR dt_referral_w IS NOT NULL ) ) THEN 

			Generate_inco_eclipse(nr_seq_account_p, 1,wheb_mensagem_pck.Get_texto(1112335),nm_usuario_p); --ReferralPeriodTypeCde 
			error_flag_w := 'T'; 
	END IF; 


	IF( ie_service_type_w <> 'O'  AND cd_requesting_w IS NOT NULL   AND ( cd_referring_w IS NOT NULL OR dt_referral_w IS NOT NULL OR ie_referral_period_w IS NOT NULL OR ie_referral_override_w IS NOT NULL ) ) THEN 
		Generate_inco_eclipse(nr_seq_account_p, 1, wheb_mensagem_pck.Get_texto(1112337),nm_usuario_p); --requestingprovidernum 
		error_flag_w := 'T'; 
	END IF; 

	IF( ie_service_type_w <> 'O' AND dt_request_w IS NOT NULL AND ( cd_referring_w IS NOT NULL OR ie_referral_period_w IS NOT NULL OR dt_referral_w IS NOT NULL OR ( dt_request_w > dt_service_w ) ) ) THEN 
		Generate_inco_eclipse(nr_seq_account_p, 1, wheb_mensagem_pck.Get_texto(1112338), nm_usuario_p); --requestissuedate 
		error_flag_w := 'T'; 
	END IF; 

--IF( ie_request_type_w IS NOT NULL 
--    AND ( cd_referring_w IS NOT NULL 
--           OR ie_referral_period_w IS NOT NULL 
--           OR dt_referral_w IS NOT NULL 
--           OR ( dt_request_w > dt_service_w ) ) ) THEN 
--  dbms_output.Put_line('test362'); 
--
--  Generate_inco_eclipse(nr_seq_account_p, 1, 
--  wheb_mensagem_pck.Get_texto(1112339), 
--  nm_usuario_p); --RequestTypeCde 
--
--  error_flag_w := 'T'; 
--END IF; 

	IF( ie_referral_period_w IS NOT NULL 
    AND (( ie_referral_override_w IN( 'L', 'E', 'H' ) 
    OR ( cd_requesting_w IS NOT NULL 
     --  OR ie_request_type_w IS NOT NULL 
     OR dt_request_w IS NOT NULL ) )) ) THEN 
		Generate_inco_eclipse(nr_seq_account_p, 1, wheb_mensagem_pck.Get_texto(1112335),nm_usuario_p); --ReferralPeriodTypeCde 
		error_flag_w := 'T'; 
	END IF; 

	IF( cd_referring_w IS NOT NULL 
    AND ( ( cd_referring_w = cd_provider_w ) 
           OR cd_requesting_w IS NOT NULL 
          -- OR ie_request_type_w IS NOT NULL 
           OR dt_request_w IS NOT NULL 
           OR ( ie_referral_override_w IN( 'H', 'L', 'E' ) ) ) ) THEN 

		Generate_inco_eclipse(nr_seq_account_p, 1,wheb_mensagem_pck.Get_texto(1112336), nm_usuario_p); --referringprovidernum 
		error_flag_w := 'T'; 
	END IF; 

	IF( ie_service_type_w <> 'O' AND cd_requesting_w IS NULL   AND ( dt_request_w IS NOT NULL ) ) THEN 
			Generate_inco_eclipse(nr_seq_account_p, 1, wheb_mensagem_pck.Get_texto(1112337),nm_usuario_p); --requestingprovidernum 
			error_flag_w := 'T'; 

	END IF; 

	IF( dt_request_w IS NULL 
    AND ( cd_requesting_w IS NOT NULL  ) 
    AND ie_service_type_w <> 'O' ) THEN 

	Generate_inco_eclipse(nr_seq_account_p, 1, wheb_mensagem_pck.Get_texto(1112338), nm_usuario_p); --requestissuedate 
	error_flag_w := 'T'; 
	END IF; 

	SELECT ie_claim_type 
	INTO   ie_claim_type_w 
	FROM   eclipse_parameters; 

	IF ( ie_claim_type_w = 'SC' ) THEN 
		ie_financial_interest_w := 'true'; 
		ie_ifc_w := 'X'; 
	END IF; 
	nr_voucher_count_w := 0; 

FOR r_c02 IN c02 LOOP 
    cd_referring_tw := cd_referring_w; 

    dt_referral_tw := dt_referral_w; 

    cd_requesting_tw := cd_requesting_w; 

    dt_request_tw := dt_request_w; 

    ie_referral_period_tw := ie_referral_period_w; 

    IF ( ie_referral_override_w IS NOT NULL 
          OR ie_selfdeemed_w = 'SD' ) THEN 
      cd_referring_w := NULL; 

      dt_referral_w := NULL; 

      cd_requesting_w := NULL; 

      ie_referral_period_w := NULL; 

      dt_request_w := NULL; 
    ELSE 
      cd_referring_w := cd_referring_tw; 

      dt_referral_w := dt_referral_tw; 

      cd_requesting_w := cd_requesting_tw; 

      dt_request_w := dt_request_tw; 

      ie_referral_period_w := ie_referral_period_tw; 
    END IF; 

    SELECT Max(Nvl(To_number(Substr(r_c02.ds_observacao, 24, 2)), 1)) 
    INTO   qt_patients_w 
    FROM   dual 
    WHERE  Upper(Substr(r_c02.ds_observacao, 1, 23)) = Upper( 
           'Number of patient seen '); 

    IF ( r_c02.qt_item_w <= 14 
          OR ( r_c02.qt_item_w > 14 
               AND has_proc_w = 0 
               AND contador_w < 14 ) ) THEN 
      IF( voucher_date_w IS NULL 
           OR ( To_char(voucher_date_w, 'YYYY-MM-DD') <> 
                    To_char(r_c02.dt_procedimento, 'YYYY-MM-DD') ) ) 
                    THEN 
        new_voucher_ind_w := 1; 

        voucher_date_w := r_c02.dt_procedimento; 

        IF( r_c02.dt_procedimento IS NULL ) THEN 
          voucher_date_w := SYSDATE; --dt_service_w; 
        END IF; 
      END IF;  

      IF( new_voucher_ind_w <> 1 ) THEN 
        IF(( r_c02.cd_medico_resp IS NOT NULL 
             AND ( r_c02.cd_medico_resp <> cd_provider_w ) )) THEN 
          curr_cd_medico_resp_w := r_c02.cd_medico_resp; 

          new_voucher_ind_w := 1; 
        ELSIF ( r_c02.cd_medico_resp IS NULL ) THEN 
          IF( curr_cd_medico_resp_w <> cd_provider_w ) THEN 
            --    new_voucher_ind_w := 1; 
            curr_cd_medico_resp_w := cd_provider_w; 
          END IF; 
        END IF; 
      END IF; 
      
       SELECT Max(Decode(a.cd_setor_entrega, 50, 'SD',37, 'SS',44, 'SN')),--selfdeemed 
						--max(decode(a.ie_aprovacao_execucao,'S','Y')) ,   -- Rule 3 excemption 
						--max(decode(a.ie_urgencia, 'S', 'Y')),       -- rule s4b3, 
                 Max(Decode(a.nr_seq_topografia, NULL, 'false', 'true')), 
                 Max(Decode(a.ie_executar_leito, 'S', 'true','false')), 
						--max(a.qt_procedimento), 
                 Max(a.ds_dado_clinico), 
                 Max(Decode(a.nr_controle_ext, NULL, 'true', 1, 'false','true')) 
          INTO   ie_selfdeemed_w, 
				--ie_rule_w, 
				--ie_s4b3_w, 
				ie_multiple_procedure_w, ie_duplicate_w, 
				--qt_procedure_count_w, 
				cd_equipamento_w, ie_hospital_w 
          FROM   prescr_procedimento a, 
                 prescr_medica b, 
                 atendimento_paciente c, 
                 conta_paciente d 
          WHERE  d.nr_interno_conta = nr_seq_account_p -- :conta_pat 
                 AND a.cd_procedimento = r_c02.cd_procedimento 
                 AND d.nr_atendimento = c.nr_atendimento 
                 AND b.nr_atendimento = c.nr_atendimento 
                 AND b.nr_prescricao = a.nr_prescricao; 

      IF( ( MOD(cd_service_code_w, 14) = 0 
            AND cd_service_code_w <> 0 ) 
           OR new_voucher_ind_w = 1 
           OR ie_selfdeemed_w = 'SD' ) THEN 
        IF( error_flag_w IS NULL  
            AND ie_validate_p = 'F' ) THEN 
          nr_voucher_count_w := nr_voucher_count_w + 1; 

          new_voucher_ind_w := 0; 

          SELECT ovs_voucher_seq.NEXTVAL, 
                 Lpad(nr_voucher_count_w, 2, '0') 
          INTO   nr_sequencia_voucher_w, nr_voucher_w 
          FROM   dual; 
          
          if(nr_voucher_count_w > 16 ) then 
            wheb_mensagem_pck.Exibir_mensagem_abort('Please split the claim into different claims, the maximum number of voucher in a claim can be 16'); 
          end if;

          INSERT INTO ovs_voucher 
                      (nr_sequencia, 
                       dt_atualizacao, 
                       nm_usuario, 
                       dt_atualizacao_nrec, 
                       nm_usuario_nrec, 
                       nr_seq_claim, 
                       dt_admission, 
                       ie_compensation, 
                       dt_discharge, 
                       ie_financial_interest, 
                       ie_ifc, 
                       nm_alias_family, 
                       nm_alias_first, 
                       dt_birth, 
                       nm_family, 
                       nm_first, 
                       cd_fund_card, 
                       cd_upi, 
                       ie_gender, 
                       nm_second, 
                       dt_referral, 
                       qt_period, 
                       ie_referral_period, 
                       ie_referral_override, 
                       cd_referring, 
                       cd_requesting, 
                       dt_request, 
                       ie_request_type, 
                       ie_service_type, 
                       cd_provider, 
                       cd_voucher_id) 
          VALUES     ( nr_sequencia_voucher_w, 
                      SYSDATE, 
                      nm_usuario_p, 
                      dt_atualizacao_nrec_w, 
                      nm_usuario_p, 
                      nr_seq_claim_p, 
                      dt_admission_w, 
                      NULL, 
                      dt_discharge_w, 
                      ie_financial_interest_w, 
                      ie_ifc_w, 
                      nm_alias_family_w, 
                      nm_alias_first_w, 
                      dt_birth_w, 
                      nm_family_name_w, 
                      nm_first_name_w, 
                      cd_fund_card_w, 
                      Substr(cd_upi_w, 1, 2), 
                      ie_gender_w, 
                      nm_second_name_w, 
                      Decode(ie_selfdeemed_w,'SD',  NULL, dt_referral_w), 
                      Lpad(pkg_date_utils.Get_diffdate(dt_referral_w, 
                           dt_validate_w, 
                           'MONTH'), 2, '0'), 
                      Decode(ie_selfdeemed_w,'SD', NULL, ie_referral_period_w), 
                      ie_referral_override_w, 
                      Decode(ie_selfdeemed_w,'SD', NULL, cd_referring_w), 
                      Decode(ie_selfdeemed_w,'SD', NULL, cd_requesting_w), 
                      Decode(ie_selfdeemed_w,'SD', NULL, dt_request_w ), 
                      Decode(ie_selfdeemed_w,'SD', NULL, ie_request_type_w ), 
                      ie_service_type_w, 
                      r_c02.cd_medico_resp, 
                      nr_voucher_w); 
        END IF; 
      END IF; 

      cd_service_code_w := cd_service_code_w + 1; 

      IF ( r_c02.qt_item_w > 14 ) THEN 
        SELECT Count(nr_sequencia) 
        INTO   has_proc_w 
        FROM   ovs_service a 
        WHERE  a.nr_seq_proc = r_c02.nr_sequencia; 
      END IF; 

      contador_w := contador_w + 1; 

      dt_service_w := r_c02.dt_procedimento; 

      IF ( r_c02.vl_procedimento < 100 ) THEN 
        Generate_inco_eclipse(nr_seq_account_w, 3, Obter_desc_expressao(587168), 
        nm_usuario_p); 
        --o valor do procedimento nao poder ser inferior a 100-criar uma expressao 

        error_flag_w := 'T'; 
      END IF; 

      IF ( r_c02.cd_procedimento = 0 ) THEN 
        Generate_inco_eclipse(nr_seq_account_w, 3, Obter_desc_expressao(568639), 
        nm_usuario_p); 

        error_flag_w := 'T'; 
      END IF; 

      IF ( dt_service_w > SYSDATE ) THEN 
        Generate_inco_eclipse(nr_seq_account_w, 3, Obter_desc_expressao(583872), 
        nm_usuario_p); 
        -- a data do servico nao pode ser futura -criar uma expressao 

        error_flag_w := 'T'; 
      END IF; 

      IF( ie_selfdeemed_w IS NOT NULL 
          AND ( ie_selfdeemed_w NOT IN ( 'SD', 'SS', 'NN' ) ) ) THEN 
        Generate_inco_eclipse(nr_seq_account_p, 1, 'invalid selfdeemed code', 
        nm_usuario_p); 

        error_flag_w := 'T'; 
      END IF; 

      IF( ie_selfdeemed_w = 'SS' 
          AND ( ie_referral_period_w IS NOT NULL 
                 OR ie_referral_period_w IS NOT NULL 
                 OR cd_medic_w IS NOT NULL 
                 OR dt_referral_w IS NOT NULL ) ) THEN 
				Generate_inco_eclipse(nr_seq_account_p, 1, 'invalid selfDeemed value', nm_usuario_p); 

        error_flag_w := 'T'; 
      END IF; 

      --------------------------------------------- 
      SELECT Count(nr_sequencia) 
      INTO   nr_records_w 
      FROM   eclipse_inco_account 
      WHERE  nr_interno_conta = nr_seq_account_w; 

      IF ( nr_records_w = 0 
           AND error_flag_w IS NULL 
           AND ie_validate_p = 'F' ) THEN 
        SELECT ovs_service_seq.NEXTVAL, 
               Lpad(cd_service_code_w, 4, '0') 
        INTO   nr_sequencia_w, service_count_number 
        FROM   dual; 

        INSERT INTO ovs_service 
                    (nr_sequencia, 
                     nr_seq_voucher, 
                     nr_seq_proc, 
                     dt_atualizacao, 
                     nm_usuario, 
                     dt_atualizacao_nrec, 
                     nm_usuario_nrec, 
                     vl_charge, 
                     dt_service, 
                     ie_duplicate, 
                     ie_hospital, 
                     cd_item, 
                     ie_multiple, 
                     qt_patients, 
                     ie_selfdeemed, 
                     ds_service, 
                     nr_time_service, 
                     qt_duration, 
                     cd_service_id) 
        VALUES     ( nr_sequencia_w, 
                    nr_sequencia_voucher_w, 
                    has_proc_w, 
                    SYSDATE, 
                    nm_usuario_p, 
                    SYSDATE, 
                    nm_usuario_p,  
                    r_c02.vl_procedimento, 
                    dt_service_w, 
                    ie_duplicate_w, 
                    Decode(ie_hospital_w, NULL, 'true', 
                                          ie_hospital_w), 
                    --decode(ie_hospital_w, null, 'Y', ie_hospital_w), 
                    Substr(r_c02.cd_procedimento_loc, 1, 5), 
                    --decode(r_c02.ds_observacao, 'S', 'true', 'false'), 
                    ie_multiple_procedure_w, 
                    qt_patients_w, 
                    ie_selfdeemed_w, 
                    Substr(r_c02.ds_observacao, 1, 100), 
                    Lpad(To_number(To_char(To_date(dt_service_w), 'hh24mm')), 4, 
                    0), 
                    r_c02.qt_duracao, 
                    service_count_number); 
      END IF; 
    END IF; 
END LOOP; 
END;
/