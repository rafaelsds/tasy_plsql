create or replace
procedure liberar_pac_coleta(	nr_seq_doacao_p		number,
				nm_usuario_p		varchar2) is 
				
begin

if	(nr_seq_doacao_p is not null) then	
		
	update	san_doacao
	set	ie_status 		= 1,
		ie_avaliacao_final 	= 'A',
		dt_triagem_fisica 	= sysdate,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where 	nr_sequencia 	= nr_seq_doacao_p;
	
	--SAN EVENTO- LIBERAR PARA A COLETA
	executa_evento_hemoterapia( nm_usuario_p , nr_seq_doacao_p , 2);
		
end if;
commit;

end liberar_pac_coleta;
/