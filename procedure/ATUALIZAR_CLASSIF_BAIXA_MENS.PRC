create or replace
procedure atualizar_classif_baixa_mens
			(	nr_seq_mensalidade_p	number,
				ie_commit_p		varchar2,
				nm_usuario_p		varchar2) is

nr_titulo_w			number(10);
nr_seq_baixa_w			number(10);
cd_estabelecimento_w		number(4);
ds_titulos_contab_w		varchar2(4000);
nr_titulo_contab_w		number(10);

Cursor c01 is
	select	a.nr_sequencia,
		b.cd_estabelecimento
	from	titulo_receber b,
		titulo_receber_liq a
	where	a.nr_titulo	= b.nr_titulo
	and	b.nr_titulo	= nr_titulo_w;

begin

if	(nr_seq_mensalidade_p is not null) then
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_seq_mensalidade	= nr_seq_mensalidade_p;
	
	if	(nr_titulo_w is not null) then
		ds_titulos_contab_w	:= null;
		
		open c01;
		loop
		fetch c01 into
			nr_seq_baixa_w,
			cd_estabelecimento_w;
		exit when c01%notfound;
			
			delete	from titulo_rec_liq_cc
			where	nr_titulo	= nr_titulo_w
			and	nr_seq_baixa	= nr_seq_baixa_w;
			
			gerar_titulo_rec_liq_cc(cd_estabelecimento_w,
						null,
						nm_usuario_p,
						nr_titulo_w,
						nr_seq_baixa_w);
						
			nr_titulo_contab_w	:= null;
			
			pls_gerar_tit_rec_liq_mens(	nr_titulo_w,
							nr_seq_baixa_w,
							nm_usuario_p,
							nr_titulo_contab_w);
		
			if	(nr_titulo_contab_w is not null) then
				ds_titulos_contab_w	:= substr(ds_titulos_contab_w || nr_titulo_contab_w || ', ',1,4000); 
			end if;	 
			
		end loop;
		close c01;
	end if;
	
	if	(ds_titulos_contab_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(236517, 'NR_TITULO=' || substr(ds_titulos_contab_w,1,length(ds_titulos_contab_w) -2)  );
	end if;
end if;

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end atualizar_classif_baixa_mens;
/
