create or replace
procedure fin_estornar_baixa_perda(
			nr_seq_perda_p		number,
			nr_seq_baixa_p		number,
			nr_seq_movto_trans_p	number,
			dt_baixa_p		date,
			nm_usuario_p		varchar2) is 

nr_sequencia_w		number(5);
nr_titulo_w		number(10);
nr_Seq_cheque_w		number(10);
cd_estabelecimento_w	number(4);

begin
select	max(nr_titulo),
	max(nr_seq_cheque),
	max(cd_estabelecimento)
into	nr_titulo_w,
	nr_Seq_cheque_w,
	cd_estabelecimento_w
from	perda_contas_receber
where	nr_sequencia = 	nr_seq_perda_p;

select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	perda_contas_receb_baixa
where	nr_seq_perda = nr_seq_perda_p;

insert into perda_contas_receb_baixa(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_perda,
	dt_baixa,
	vl_baixa,
	nr_seq_tipo_baixa,
	ie_acao,
	nr_seq_baixa_orig,
	nr_seq_movto_trans_fin)
select	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_perda,
	nvl(dt_baixa_p, dt_baixa),
	(a.vl_baixa * - 1),
	nr_seq_tipo_baixa,
	2,
	nr_sequencia,
	nr_seq_movto_trans_p
from	perda_contas_receb_baixa a
where	nr_seq_perda = nr_seq_perda_p
and	nr_sequencia = nr_seq_baixa_p;

update	perda_contas_receb_baixa
set	ie_acao = 3
where	nr_seq_perda = nr_seq_perda_p
and	nr_sequencia = nr_seq_baixa_p;

fin_atualizar_saldo_perda(nr_seq_perda_p, nm_usuario_p);

atualizar_cobranca(nr_titulo_w, nr_seq_cheque_w, 'N', cd_estabelecimento_w, nm_usuario_p);

commit;

end fin_estornar_baixa_perda;
/