create or replace
procedure atualiza_emissao_req(nr_requisicao_p		number) is 

begin

UPDATE REQUISICAO_MATERIAL
SET DT_EMISSAO_LOC_ESTOQUE = sysdate
WHERE NR_REQUISICAO = nr_requisicao_p;

commit;

end atualiza_emissao_req;
/


