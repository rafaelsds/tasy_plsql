create or replace
procedure gerar_fluxo_caixa_pas_real_j is

begin
GERAR_FLUXO_CAIXA_PASSADO_REAL
		(1,
		trunc(sysdate, 'month'),
		last_day(sysdate),
		null,
		'Tasy',
		1,
		'N',
		'R',
		'S',
		'D',
		0,
		'A',
		'N',
		'N',
		null);
end;
/
