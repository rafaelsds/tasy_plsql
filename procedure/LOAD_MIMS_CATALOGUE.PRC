create or replace procedure load_mims_catalogue(nr_mims_version_p	number,
												nm_user_p 			varchar2,
												nr_estab_p 			number)  as

ds_error_w varchar2(250):=null;
begin

execute immediate 'alter session set nls_numeric_characters = ''.,''';

process_mims_catalog(nr_mims_version_p,nm_user_p,nr_estab_p);   
  
update	mbs_import_log
set  	dt_end = sysdate
where   ie_status = 'IMIMS'
and     dt_end is null;

commit; 
exception when others then
	
	rollback;
	ds_error_w:=SUBSTR(SQLERRM, 1 , 250) ;
	update	mbs_import_log
	set  	dt_end = sysdate,
			ds_error = ds_error_w 
	where   ie_status = 'IMIMS'
	and     dt_end is null;
	
	commit;
end LOAD_MIMS_CATALOGUE; 
/