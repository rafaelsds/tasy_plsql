create or replace
procedure excluir_item_Prescr_emerg_palm(nr_prescricao_p	number,
										nr_sequencia_p number) is 

begin

if	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) then
	
	delete 	from prescr_material
	where	nr_prescricao	=	nr_prescricao_p
	and		nr_sequencia	=	nr_sequencia_p;
end if;

commit;

end excluir_item_Prescr_emerg_palm;
/