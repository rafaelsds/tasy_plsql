create or replace
procedure far_inserir_item_pedido(
				nm_usuario_p		Varchar2,
				nr_seq_vendedor_p		number,
				nr_seq_pedido_p		number,
				cd_material_p		number,
				qt_material_p		number,
				vl_unitario_p	number,
				vl_desconto_p	number,
				vl_acrescimo_p	number,
				cd_estabelecimento_p	number,
				cd_pessoa_fisica_p		Varchar2,
				nr_seq_motivo_desc_p	Number) is

qt_pedido_w number(13);
nr_sequencia_w	number(13);
begin

select count(*)
into	qt_pedido_w
from	far_pedido
where	 nr_sequencia = nr_seq_pedido_p;

if 	(qt_pedido_w > 0) then
	
	select 	far_pedido_item_seq.nextval
	into	nr_sequencia_w
	from 	far_pedido_item
	where	nr_seq_pedido = nr_seq_pedido_p;
	
	insert into far_pedido_item (
				nr_seq_item,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_pedido,
				qt_material,
				cd_material,
				vl_unitario_item,
				vl_total_item,
				vl_liquido_item,
				cd_unidade_medida,
				nr_seq_promocao,
				vl_desconto,
				pr_desconto) 
			values (nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_pedido_p,
				qt_material_p,
				cd_material_p,
				vl_unitario_p,
				(vl_unitario_p*qt_material_p),
				((vl_unitario_p*qt_material_p) - (vl_desconto_p*qt_material_p) + (vl_acrescimo_p*qt_material_p)),
				0,
				null,
				vl_desconto_p,
				0);
				
	update	far_pedido
	set		vl_mercadoria = ((vl_unitario_p*qt_material_p) - (vl_desconto_p*qt_material_p) + (vl_acrescimo_p*qt_material_p))
	where	nr_sequencia = nr_seq_pedido_p;
	
end if;	


commit;

end far_inserir_item_pedido;
/