CREATE OR REPLACE
PROCEDURE Gerar_cobranca_brasil_hrf	(	nr_seq_cobr_escrit_p	NUMBER,
						nm_usuario_p		VARCHAR2) IS


nr_seq_arquivo_w			number(2);
tp_registro_w			number(5);
nr_seq_envio_w			number(10);
dt_remessa_retorno_w		date;
DT_GERACAO_ARQUIVO_w		date;
DS_BANCO_w			varchar2(255);
cd_agencia_bancaria_w		varchar2(8);
nr_digito_agencia_w		varchar2(2);
nr_conta_corrente_w		varchar2(15);
nm_empresa_w			varchar2(80);
nr_titulo_w			number(10);
dt_vencimento_w			date;
dt_emissao_w			date;
vl_cobranca_w			number(15,2);
vl_desconto_w			number(15,2);
ie_tipo_inscricao_w			number(5);
nr_inscricao_w			varchar2(14);
nm_pessoa_w			varchar2(80);
ds_endereco_w			varchar2(255);
ds_bairro_w			varchar2(80);
cd_cep_w			varchar2(10);
ds_cidade_w			varchar2(40);
ds_uf_w				varchar2(2);
vl_carteira_simples_w		number(15,2);
ds_nosso_numero_w		varchar2(20);

qt_registros_w			number(6) := 0;
nr_seq_reg_lote_w			number(5) := 1;
qt_reg_lote_w			number(6) := 1;

cursor c01 is
SELECT	nr_seq_arquivo,
	tp_registro,
	nr_seq_envio,
	dt_remessa_retorno,
  	DT_GERACAO_ARQUIVO,
  	DS_BANCO,
	cd_agencia_bancaria,
	nr_digito_agencia,
	nr_conta_corrente,
	nm_empresa,
	nr_titulo,
	dt_vencimento,
	dt_emissao,
	vl_cobranca,
	vl_desconto,
	ie_tipo_inscricao,
	nr_inscricao,
	nm_pessoa,
	ds_endereco,
	ds_bairro,
	cd_cep,
	ds_cidade,
	ds_uf,
	vl_carteira_simples,
	ds_nosso_numero
FROM	(SELECT	1 nr_seq_arquivo,
		1 tp_registro,
		a.nr_sequencia nr_seq_envio,
		a.dt_remessa_retorno,
		SYSDATE DT_GERACAO_ARQUIVO,
		'Banco DO brasil' DS_BANCO,
		c.cd_agencia_bancaria,
		calcula_digito('Modulo11',c.cd_agencia_bancaria) nr_digito_agencia,
		c.cd_conta nr_conta_corrente,
		SUBSTR(obter_razao_social(b.cd_cgc),1,100) nm_empresa,
		'' nr_titulo,
		SYSDATE dt_vencimento,
		SYSDATE dt_emissao,
		0 vl_cobranca,
		0 vl_desconto,
		0 ie_tipo_inscricao,
		b.cd_cgc nr_inscricao,
		' ' nm_pessoa,
		' ' ds_endereco,
		' ' ds_bairro,
		' ' cd_cep,
		' ' ds_cidade,
		' ' ds_uf,
		0 vl_carteira_simples,
		'' ds_nosso_numero
	FROM	estabelecimento b,
		banco_estabelecimento c,
		cobranca_escritural a
	WHERE	a.cd_estabelecimento	= b.cd_estabelecimento
	AND	a.nr_seq_conta_banco	= c.nr_sequencia
	AND	a.nr_sequencia		= nr_seq_cobr_escrit_p
	UNION
	SELECT	2 nr_seq_arquivo,
		2 tp_registro,
		a.nr_sequencia nr_seq_envio,
		a.dt_remessa_retorno,
	  	SYSDATE DT_GERACAO_ARQUIVO,
		'Banco DO brasil' DS_BANCO,
		c.cd_agencia_bancaria,
		calcula_digito('Modulo11',c.cd_agencia_bancaria) nr_digito_agencia,
		c.cd_conta nr_conta_corrente,
		SUBSTR(obter_razao_social(b.cd_cgc),1,100) nm_empresa,
		'' nr_titulo,
		SYSDATE dt_vencimento,
		SYSDATE dt_emissao,
		0 vl_cobranca,
		0 vl_desconto,
		0 ie_tipo_inscricao,
		' ' nr_inscricao,
		' ' nm_pessoa,
		' ' ds_endereco,
		' ' ds_bairro,
		' ' cd_cep,
		' ' ds_cidade,
		' ' ds_uf,
		0 vl_carteira_simples,
		'' ds_nosso_numero
	FROM	estabelecimento b,
		banco_estabelecimento c,
		cobranca_escritural a
	WHERE	a.cd_estabelecimento	= b.cd_estabelecimento
	AND	a.nr_seq_conta_banco	= c.nr_sequencia
	AND	a.nr_sequencia		= nr_seq_cobr_escrit_p
	UNION
	SELECT	3 nr_seq_arquivo,
		3 tp_registro,
		a.nr_sequencia nr_seq_envio,
		SYSDATE dt_remessa_retorno,
		SYSDATE DT_GERACAO_ARQUIVO,
		'Banco DO brasil' DS_BANCO,
		c.cd_agencia_bancaria,
		calcula_digito('Modulo11',c.cd_agencia_bancaria) nr_digito_agencia,
		c.nr_conta nr_conta_corrente,
		' ' nm_empresa,
		nvl(b.nr_titulo_externo,c.nr_titulo),
		b.dt_pagamento_previsto dt_vencimento,
		b.dt_emissao,
		c.vl_cobranca,
		b.TX_DESC_ANTECIPACAO vl_desconto,
		b.ie_tipo_pessoa ie_tipo_inscricao,
		b.cd_cgc_cpf nr_inscricao,
		b.nm_pessoa,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'EC') ds_endereco,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'B') ds_bairro,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'CEP') cd_cep,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'CI') ds_cidade,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'UF') ds_uf,
		0 vl_carteira_simples,
		'1577180'|| LPAD(c.nr_sequencia,13,0) ds_nosso_numero
	FROM	titulo_receber_v b,
		titulo_receber_cobr c,
		cobranca_escritural a
	WHERE	a.nr_sequencia		= c.nr_seq_cobranca
	AND	c.nr_titulo		= b.nr_titulo
	AND	a.nr_sequencia		= nr_seq_cobr_escrit_p
	UNION
	SELECT	3 nr_seq_arquivo,
		4 tp_registro,
		a.nr_sequencia nr_seq_envio,
		SYSDATE dt_remessa_retorno,
	  	SYSDATE DT_GERACAO_ARQUIVO,
	  	'Banco DO brasil' DS_BANCO,
		c.cd_agencia_bancaria,
		calcula_digito('Modulo11',c.cd_agencia_bancaria) nr_digito_agencia,
		c.nr_conta nr_conta_corrente,
		' ' nm_empresa,
		nvl(b.nr_titulo_externo,c.nr_titulo),
		b.dt_pagamento_previsto dt_vencimento,
		b.dt_emissao,
		c.vl_cobranca,
		b.TX_DESC_ANTECIPACAO vl_desconto,
		b.ie_tipo_pessoa ie_tipo_inscricao,
		b.cd_cgc_cpf nr_inscricao,
		b.nm_pessoa,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'EC') ds_endereco,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'B') ds_bairro,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'CEP') cd_cep,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'CI') ds_cidade,
		obter_dados_pf_pj5(b.cd_pessoa_fisica, b.cd_cgc, 'UF') ds_uf,
		0 vl_carteira_simples,
		'1577180'|| LPAD(c.nr_sequencia,13,0) ds_nosso_numero
	FROM	titulo_receber_v b,
		titulo_receber_cobr c,
		cobranca_escritural a
	WHERE	a.nr_sequencia		= c.nr_seq_cobranca
	AND	c.nr_titulo		= b.nr_titulo
	AND	a.nr_sequencia		= nr_seq_cobr_escrit_p
	UNION
	SELECT	5 nr_seq_arquivo,
		5 tp_registro,
		a.nr_sequencia nr_seq_envio,
		a.dt_remessa_retorno,
	  	SYSDATE DT_GERACAO_ARQUIVO,
	  	'Banco DO brasil' DS_BANCO,
		' ' cd_agencia_bancaria,
		0 nr_digito_agencia,
		' ' nr_conta_corrente,
		' ' nm_empresa,
		'' nr_titulo,
		SYSDATE dt_vencimento,
		SYSDATE dt_emissao,
		0 vl_cobranca,
		0 vl_desconto,
		0 ie_tipo_inscricao,
		' ' nr_inscricao,
		' ' nm_pessoa,
		' ' ds_endereco,
		' ' ds_bairro,
		' ' cd_cep,
		' ' ds_cidade,
		' ' ds_uf,
		SUM(b.vl_cobranca) vl_carteira_simples,
		'' ds_nosso_numero
	FROM	titulo_receber_cobr b,
		cobranca_escritural a
	WHERE	a.nr_sequencia		= b.nr_seq_cobranca
	AND	a.nr_sequencia		= nr_seq_cobr_escrit_p
	GROUP BY a.nr_sequencia,
	 	 a.dt_remessa_retorno,
		 a.dt_remessa_retorno,
		 a.cd_banco
	UNION
	SELECT	9 nr_seq_arquivo,
		9 tp_registro,
		a.nr_sequencia nr_seq_envio,
		a.dt_remessa_retorno,
	  	SYSDATE DT_GERACAO_ARQUIVO,
	  	'Banco DO brasil' DS_BANCO,
		' ' cd_agencia_bancaria,
		0 nr_digito_agencia,
		' ' nr_conta_corrente,
		' ' nm_empresa,
		'' nr_titulo,
		SYSDATE dt_vencimento,
		SYSDATE dt_emissao,
		0 vl_cobranca,
		0 vl_desconto,
		0 ie_tipo_inscricao,
		' ' nr_inscricao,
		' ' nm_pessoa,
		' ' ds_endereco,
		' ' ds_bairro,
		' ' cd_cep,
		' ' ds_cidade,
		' ' ds_uf,
		SUM(b.vl_cobranca) vl_carteira_simples,
		'' ds_nosso_numero
	FROM	titulo_receber_cobr b,
		cobranca_escritural a
	WHERE	a.nr_sequencia		= b.nr_seq_cobranca
	AND	a.nr_sequencia		= nr_seq_cobr_escrit_p
	GROUP BY a.nr_sequencia,
		 a.dt_remessa_retorno,
		 a.dt_remessa_retorno,
		 a.cd_banco
	)
ORDER BY	nr_seq_arquivo,
		nr_titulo,
		tp_registro;

begin

delete	from w_cobranca_banco;
commit;

open c01;
loop
fetch c01 into
	nr_seq_arquivo_w,
	tp_registro_w,
	nr_seq_envio_w,
	dt_remessa_retorno_w,
	DT_GERACAO_ARQUIVO_w,
	DS_BANCO_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nm_empresa_w,
	nr_titulo_w,
	dt_vencimento_w,
	dt_emissao_w,
	vl_cobranca_w,
	vl_desconto_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	ds_uf_w,
	vl_carteira_simples_w,
	ds_nosso_numero_w;
exit when c01%notfound;

qt_registros_w := qt_registros_w + 1;

insert	into	w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_registro,
		nr_seq_envio,
		dt_recebimento,
		DT_GERACAO,
		DS_BANCO,
		cd_agencia_bancaria,
		nr_digito_agencia,
		cd_conta,
		nm_empresa,
		nr_titulo,
		dt_vencimento,
		dt_emissao,
		vl_titulo,
		vl_desconto,
		ie_tipo_pessoa,
		ds_cgc_cpf,
		nm_pagador,
		ds_endereco,
		ds_bairro,
		cd_cep,
		ds_cidade,
		sg_estado,
		vl_tot_registros,
		nr_nosso_numero,
		nr_seq_reg_lote,
		qt_reg_lote,
		qt_registros)
	values	(w_interf_itau_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		tp_registro_w,
		nr_seq_envio_w,
		dt_remessa_retorno_w,
		DT_GERACAO_ARQUIVO_w,
		DS_BANCO_w,
		cd_agencia_bancaria_w,
		nr_digito_agencia_w,
		nr_conta_corrente_w,
		nm_empresa_w,
		nr_titulo_w,
		dt_vencimento_w,
		dt_emissao_w,
		vl_cobranca_w,
		vl_desconto_w,
		ie_tipo_inscricao_w,
		nr_inscricao_w,
		nm_pessoa_w,
		ds_endereco_w,
		substr(ds_bairro_w,1,40),
		cd_cep_w,
		ds_cidade_w,
		ds_uf_w,
		vl_carteira_simples_w,
		ds_nosso_numero_w,
		nr_seq_reg_lote_w,
		qt_reg_lote_w,
		qt_registros_w);

if	(tp_registro_w in (3,4)) then
	nr_seq_reg_lote_w := nr_seq_reg_lote_w + 1;
end if;

if	(tp_registro_w in (2,3,4,5)) then
	qt_reg_lote_w := qt_reg_lote_w + 1;
end if;


end loop;
close c01;

commit;

end Gerar_cobranca_brasil_hrf;
/