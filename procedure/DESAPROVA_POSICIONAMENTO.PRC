CREATE OR REPLACE
PROCEDURE desaprova_posicionamento(nr_posic_p NUMBER) IS

BEGIN

 UPDATE proj_posicao_coordenacao

 SET dt_aprovacao  = ''

 WHERE nr_sequencia = nr_posic_p;

 COMMIT;


END desaprova_posicionamento;
/
