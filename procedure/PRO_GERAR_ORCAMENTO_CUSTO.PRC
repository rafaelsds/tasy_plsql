create or replace
procedure PRO_GERAR_ORCAMENTO_CUSTO(	cd_estabelecimento_p	number,
					cd_tabela_custo_p		number,
					nm_usuario_p		Varchar2) is 

dt_mes_referencia_w			date;

begin

delete	from orcamento_custo
where	cd_estabelecimento	= cd_estabelecimento_p
and	cd_tabela_custo	= cd_tabela_custo_p;

PRO_IMPORTAR_ORC_RECEITA(cd_estabelecimento_p, cd_tabela_custo_p, nm_usuario_p);
PRO_IMPORTAR_ORC_CUSTO(cd_estabelecimento_p, cd_tabela_custo_p, nm_usuario_p);

commit;

end PRO_GERAR_ORCAMENTO_CUSTO;
/