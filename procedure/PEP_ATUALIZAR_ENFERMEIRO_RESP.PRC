create or replace
procedure pep_atualizar_enfermeiro_resp	(	lista_nr_atendimento_p	varchar2,
						cd_pessoa_fisica_p	varchar2,
						nm_usuario_p		varchar2) is

qt_enfermeiro_w		number(10,0) := 0;
cd_pessoa_fisica_w	varchar2(10);
nr_atendimento_w	number(10);
ds_lista_aux_w		varchar2(4000) := '';
ds_nr_atendimento_w	varchar2(15);
k			integer;

begin

ds_lista_aux_w := lista_nr_atendimento_p;

while ds_lista_aux_w is not null loop
	begin
	select	instr(ds_lista_aux_w, ',') 
	into	k
	from	dual; 

	if	(k > 1) and
		(substr(ds_lista_aux_w, 1, k -1) is not null) then
		ds_nr_atendimento_w	:= substr(ds_lista_aux_w, 1, k-1);
		nr_atendimento_w	:= replace(ds_nr_atendimento_w, ',','');
		ds_lista_aux_w		:= substr(ds_lista_aux_w, k + 1, 2000);
	elsif	(ds_lista_aux_w is not null) then
		nr_atendimento_w	:= replace(ds_lista_aux_w,',','');
		ds_lista_aux_w	:= '';
	end if;	

	select	nvl(count(*),0)
	into	qt_enfermeiro_w
	from	atend_enfermagem_resp
	where	nr_atendimento = nr_atendimento_w;

	if	(nvl(qt_enfermeiro_w,0) > 0) then
		select	nvl(max(cd_pessoa_fisica),'0')
		into	cd_pessoa_fisica_w
		from	atend_enfermagem_resp
		where	nr_atendimento = nr_atendimento_w
		and	nr_sequencia = (select	max(nr_sequencia)
					  from		atend_enfermagem_resp
					  where	nr_atendimento = nr_atendimento_w);

		update	atend_enfermagem_resp
		set	dt_final_resp = sysdate
		where	nr_atendimento = nr_atendimento_w
		and	nr_sequencia = (select	max(nr_sequencia)
					  from		atend_enfermagem_resp
					  where	nr_atendimento = nr_atendimento_w);
	end if;

	if (cd_pessoa_fisica_p is null) then
		delete from atend_enfermagem_resp
		where nr_sequencia = (	select	max(nr_sequencia)
					from	atend_enfermagem_resp
					where	nr_atendimento = nr_atendimento_w)
		and nr_atendimento = nr_atendimento_w;
	else
		insert into atend_enfermagem_resp	(
							nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							cd_pessoa_fisica,
							nr_atendimento,
							cd_pessoa_resp_ant,
							dt_inicio_resp)
						select	atend_enfermagem_resp_seq.nextval,
							sysdate,
							nm_usuario_p,
							cd_pessoa_fisica_p,
							nr_atendimento_w,
							nvl(cd_pessoa_fisica_w, null),
							sysdate
						from	dual;
	end if;
	end;					
end loop;
commit;

end pep_atualizar_enfermeiro_resp;
/