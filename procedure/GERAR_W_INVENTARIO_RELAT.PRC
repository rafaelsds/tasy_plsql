create or replace
procedure gerar_w_inventario_relat(
			nr_sequencia_p		Number,
			qt_relatorio_p		Number) as

/*
qt_relatorio_p = � a quantidade de relatorios a serem impressos para conferencia.
Dividindo os itens pela quantidade de relatorios*/


nr_seq_item_w	Number(10);
ie_agrupa_w	Number(10);


cursor c01 is
select	nr_sequencia
from	inventario_material
where	nr_seq_inventario = nr_sequencia_p;


begin


OPEN C01;
LOOP
FETCH C01 into 
	nr_seq_item_w;
EXIT when c01%notfound;
	begin

	ie_agrupa_w	:= 1;

	insert into w_inventario_relat(
		nr_seq_inventario,
		nr_seq_item,
		ie_agrupa)
	values( nr_sequencia_p,
		nr_seq_item_w,
		ie_agrupa_w);
	end;
end loop;
close c01;

commit;

end gerar_w_inventario_relat;
/