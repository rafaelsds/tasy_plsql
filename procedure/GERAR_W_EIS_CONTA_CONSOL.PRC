create or replace
procedure Gerar_W_EIS_CONTA_CONSOL
		(cd_procedimento_p	number,
		ie_origem_proced_p	number,
		dt_inicial_p	varchar2,
		dt_final_p	varchar2,
		ie_tipo_atendimento_p	number,
		ie_pacote_p		varchar2,
		ie_definitiva_p		varchar2,
		ie_cancelada_p		varchar2,
		cd_conv_p		number,
		cd_convenio_p		varchar2) is

nr_interno_conta_w	number(10,0);
ds_convenio_w		varchar2(255);
ds_tipo_item_w		varchar2(40);
ds_item_w		varchar2(80);
cd_item_w		number(10,0);
qt_resumo_w		number(15,4);
vl_resumo_w		number(15,2);
qt_atendimento_w	number(10,0);
qt_proced_w		number(10,0);
qt_atend_w		number(10,0);

cursor	c01 is
	select	x.nr_interno_conta
	from	eis_conta_paciente x
	where	x.cd_procedimento	= cd_procedimento_p
	and	x.ie_origem_proced	= ie_origem_proced_p
	and	x.ie_tipo_atendimento	= ie_tipo_atendimento_p
	and	((cd_conv_p = 0) or (x.cd_convenio		in (cd_convenio_p)))
	and	x.dt_referencia 	between to_date(dt_inicial_p, 'dd-mm-yy') and to_date(dt_final_p, 'dd-mm-yy');

cursor	c02 is
	select	a.ds_convenio,
		a.ds_tipo_item,
		a.ds_item,
		a.cd_item,
		dividir(qt_atend_w * 100, qt_proced_w) qt_atendimento,
		sum(a.qt_resumo) qt_resumo,
		sum(a.vl_item) vl_item
	from	conta_paciente_custo_v a
	where	1 = 1
/*	and	((ie_pacote_p   		= 'N')	or (a.nr_seq_proc_pacote is not null))
	and	((ie_definitiva_p	 	= 'N') 	or (b.ie_status_Acerto 	= 2))
	and	((ie_cancelada_p	 	= 'S') 	or (b.ie_cancelamento 	is null))
	and	((cd_conv_p	 		= 0) 	or (b.cd_convenio_parametro		in (cd_convenio_p)))*/
	and	a.nr_interno_conta = nr_interno_conta_w
	group by	a.ds_convenio,
			a.ds_tipo_item,
			a.ds_item,
			a.cd_item;


begin

delete from W_EIS_CONTA_CONSOLidada;


select	count(distinct(x.nr_interno_conta))
into 	qt_atend_w
from	eis_conta_paciente x
where	x.cd_procedimento	= cd_procedimento_p
and	x.ie_origem_proced	= ie_origem_proced_p
and	x.ie_tipo_atendimento	= ie_tipo_atendimento_p
and	((cd_conv_p = 0) or (x.cd_convenio		in (cd_convenio_p)))
and	x.dt_referencia 	between to_date(dt_inicial_p, 'dd-mm-yy') and to_date(dt_final_p, 'dd-mm-yy');



select	max(EIS_Obter_Atend_Periodo(cd_procedimento_p, ie_origem_proced_p, to_date(dt_inicial_p, 'dd/mm/yy'),
							to_date(dt_final_p, 'dd/mm/yy'), ie_tipo_atendimento_p))
into	qt_proced_w
from	dual;

open	c01;
loop
fetch	c01 into nr_interno_conta_w;
exit	when c01%notfound;
	begin

	open	c02;
	loop
	fetch	c02 into
		ds_convenio_w,
		ds_tipo_item_w,
		ds_item_w,
		cd_item_w,
		qt_atendimento_w,
		qt_resumo_w,
		vl_resumo_w;
	exit	when c02%notfound;
		begin

		insert into W_EIS_CONTA_CONSOLIDADA
			(DS_CONVENIO,
			DS_TIPO_ITEM,
			DS_ITEM,
			CD_ITEM,
			QT_RESUMO,
			VL_ITEM,
			QT_ATENDIMENTO)
		values	(ds_convenio_w,	
			ds_tipo_item_w,
			ds_item_w,
			cd_item_w,
			qt_resumo_w,
			vl_resumo_w,
			qt_atendimento_w);

		end;
	end loop;
	close	c02;

	end;
end loop;
close	c01;

commit;

end Gerar_W_EIS_CONTA_CONSOL;
/