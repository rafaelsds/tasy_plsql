CREATE OR REPLACE 
PROCEDURE importar_ops_no_case (	nr_sequencia_p      	procedimento_pac_medico.nr_sequencia%TYPE,
					ie_tipo_p           	varchar2, -- E - episodio; A - atendimento
					nm_usuario_p		varchar2,
					nr_seq_episodio_p   	procedimento_pac_medico.nr_seq_episodio%type,
					nr_atendimento_p	atendimento_paciente.nr_atendimento%type)  -- nr_seq_episodio ou nr_atendimento
				IS


procedimento_pac_medico_w	procedimento_pac_medico%rowtype;

BEGIN

begin
select	*
into	procedimento_pac_medico_w
FROM    procedimento_pac_medico a
WHERE   a.nr_sequencia  = nr_sequencia_p;
exception
when others then
	procedimento_pac_medico_w.nr_sequencia := null;
end;

if	(procedimento_pac_medico_w.nr_sequencia is not null) then


	select  procedimento_pac_medico_seq.nextval
	into    procedimento_pac_medico_w.nr_sequencia
	from    dual;
	
	procedimento_pac_medico_w.dt_atualizacao	:= sysdate;
	procedimento_pac_medico_w.dt_atualizacao_nrec	:= sysdate;
	procedimento_pac_medico_w.dt_procedimento	:= sysdate;
	procedimento_pac_medico_w.nm_usuario		:= nm_usuario_p;
	procedimento_pac_medico_w.nm_usuario_nrec	:= nm_usuario_p;
	procedimento_pac_medico_w.dt_liberacao		:= null;	
	
	if	(ie_tipo_p = 'E') then	
		procedimento_pac_medico_w.nr_seq_episodio	:= nr_seq_episodio_p;
		procedimento_pac_medico_w.nr_atendimento	:= nr_atendimento_p;
	elsif	(ie_tipo_p = 'A') then
		procedimento_pac_medico_w.nr_seq_episodio	:= null;
		procedimento_pac_medico_w.nr_atendimento	:= nr_atendimento_p;
	end if;
	
	insert into procedimento_pac_medico values procedimento_pac_medico_w;
	
	commit;
	
end if;

end importar_ops_no_case;
/