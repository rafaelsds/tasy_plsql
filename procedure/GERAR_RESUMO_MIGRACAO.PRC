create or replace
procedure gerar_resumo_migracao (
		dt_resumo_p	date,
		nm_usuario_p	varchar2) is
		
nr_seq_gerencia_w	number(10,0) := 9;
nr_seq_estagio_w	number(10,0) := 12;

nr_seq_projeto_w	number(10,0);
qt_horas_prev_proj_w	number(15,4);

qt_hor_possiv_res_w	number(15,4);
qt_hor_real_res_w	number(15,4);
qt_hor_sent_ant_res_w	number(15,4);
qt_hor_real_sent_w	number(15,4);

pr_inic_res_migr_w	number(5,2);
pr_possiv_res_migr_w	number(5,2);
pr_real_res_migr_w	number(5,2);

dt_provavel_termino_w	date;

cursor c01 is
select	p.nr_sequencia,
	nvl(proj_obter_qt_total_horas_cron(p.nr_sequencia,0,'T','E'),0) qt_horas_prev_proj
from	proj_projeto p
where	p.nr_seq_gerencia = nr_seq_gerencia_w
and	p.nr_seq_estagio = nr_seq_estagio_w;
		
begin
if	(dt_resumo_p is not null) and
	(nm_usuario_p is not null) then
	begin
	open c01;
	loop
	fetch c01 into	nr_seq_projeto_w,
			qt_horas_prev_proj_w;
	exit when c01%notfound;
		begin
		delete
		from	w_resumo_migracao
		where	nr_seq_projeto = nr_seq_projeto_w
		and	dt_resumo >= dt_resumo_p;
		
		qt_hor_possiv_res_w 	:= calc_hor_possiv_per_res_migr(nr_seq_projeto_w, dt_resumo_p);
		
		qt_hor_real_res_w	:= calc_hor_real_per_res_migr(nr_seq_projeto_w, dt_resumo_p);
		
		qt_hor_sent_ant_res_w	:= calc_hor_sent_per_res_migr(nr_seq_projeto_w, dt_resumo_p);
		
		qt_hor_real_sent_w	:= nvl(obter_horas_sent_proj_migr(nr_seq_projeto_w, nr_seq_estagio_w), 0);
		
		pr_inic_res_migr_w	:= obter_perc_real_inic_res_migr(nr_seq_projeto_w, dt_resumo_p);
		
		pr_possiv_res_migr_w	:= obter_perc_valor(qt_hor_sent_ant_res_w + qt_hor_possiv_res_w, qt_horas_prev_proj_w);
		
		pr_real_res_migr_w	:= proj_obter_perc_cron(nr_seq_projeto_w, 'E');	
		
		dt_provavel_termino_w	:= adic_dias_uteis_horas_data(sysdate, 8, qt_horas_prev_proj_w - qt_hor_real_sent_w, 1);
		
		insert into w_resumo_migracao (
			nr_sequencia,
			dt_resumo,
			nm_usuario,
			nr_seq_projeto,
			qt_horas_prev_resumo,
			qt_horas_real_resumo,
			qt_horas_sent_resumo,
			qt_horas_sent_geral,
			pr_inicial,
			pr_previsto,
			pr_final,
			dt_provavel_termino)
		values (
			w_resumo_migracao_seq.nextval,
			dt_resumo_p,
			nm_usuario_p,
			nr_seq_projeto_w,
			qt_hor_possiv_res_w,
			qt_hor_real_res_w,
			qt_hor_sent_ant_res_w,
			qt_hor_real_sent_w,
			pr_inic_res_migr_w,
			pr_possiv_res_migr_w,
			pr_real_res_migr_w,
			dt_provavel_termino_w);
		end;
	end loop;
	close c01;	
	end;
end if;
commit;
end gerar_resumo_migracao;
/