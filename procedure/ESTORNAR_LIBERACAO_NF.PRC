create or replace
procedure estornar_liberacao_nf(	nr_sequencia_p	number,
				nm_usuario_p	Varchar2) is


cd_estabelecimento_w	number(4);
ie_estorna_aprov_solic_w	varchar2(1);
nr_seq_aprovacao_w	number(10);

cursor c01 is
Select	distinct nr_seq_aprovacao
from	nota_fiscal_item
where	nr_sequencia	= nr_sequencia_p;

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	nota_fiscal
where	nr_sequencia	= nr_sequencia_p;

update	nota_fiscal
set	dt_liberacao	= null,
	nm_usuario_lib	= '',
	dt_aprovacao	= null,
	nm_usuario_aprov	= ''	
where	nr_sequencia	= nr_sequencia_p;

open C01;
loop
fetch C01 into	
	nr_seq_aprovacao_w;
exit when C01%notfound;
	begin
	
	delete
	from	processo_aprov_compra
	where	nr_sequencia 	= nr_seq_aprovacao_w;
	
	end;
end loop;
close C01;

update	nota_fiscal_item
set	nr_seq_aprovacao	= null,
	dt_aprovacao	= null,
	nm_usuario_aprov	= '',
    dt_reprovacao = null
where	nr_sequencia	= nr_sequencia_p;

gerar_historico_nota_fiscal(
			nr_Sequencia_p,
			nm_usuario_p,
			'44',
			wheb_mensagem_pck.get_texto(313406,'DT_REF_W='|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') ||';NM_USUARIO_P='|| NM_USUARIO_P));
			/*'Foi estornado a liberacao da nota fiscal no dia ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || ' pelo usuario ' || nm_usuario_p || '.');*/



commit;

end estornar_liberacao_nf;
/
