create or replace
procedure pls_obter_dados_padrao_benef
			(	nr_seq_contrato_p	in	number,
				nr_seq_canal_venda_p	out	varchar2,
				nr_seq_vendedor_pf_p	out	varchar2,
				nm_usuario_p		in	varchar2,
				cd_estabelecimento_p	in	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: Obter os dados padr�o para novo benefici�rio
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

begin
		
select	nr_seq_vendedor_canal,
	nr_seq_vendedor_pf
into	nr_seq_canal_venda_p,
	nr_seq_vendedor_pf_p
from	pls_contrato_regra_lanc
where	nr_seq_contrato	= nr_seq_contrato_p
and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate);

exception
when others then
	nr_seq_canal_venda_p	:= null;
	nr_seq_vendedor_pf_p	:= null;
end;

commit;

end pls_obter_dados_padrao_benef;
/
