create or replace
procedure gerar_cot_compra_cen_com( 
			nr_seq_central_compra_p		number,
			nr_cot_compra_p		in out	number,
			nm_usuario_p			varchar2) is

nr_cot_compra_w		number(10);
cd_comprador_w		varchar2(10);
cd_estabelecimento_w	number(4);
cd_estab_item_w		number(4);
qt_existe_w		number(3);
nr_item_w		number(5);

cd_material_w		number(6);
cd_unidade_medida_w	varchar2(30);
dt_solic_w		date;
ds_observacao_w		varchar2(4000);
ds_obs_cotacao_w		varchar2(4000);
ds_material_direto_w	varchar2(2000);
qt_material_w		number(13,4);
dt_entrega_solicitada_w	date;
cd_pessoa_solicitante_w	varchar2(10);
ie_regra_preco_w		varchar2(15);
vardias_retorno_w		number(10);
ds_arquivo_w		varchar2(255);
nr_seq_proj_rec_w		number(10);
ie_grava_obs_item_w	varchar2(1);
ds_titulo_w		varchar2(255);
ds_detalhe_w		varchar2(4000);
ie_inseriu_item_w 		varchar2(1) := 'N';
dt_entrega_cotacao_w	date;
dt_retorno_prev_w		date;

/* shift  + f11 = 'Regra para consist�ncia do item disp. mercado' */
qt_existe_regra_disp_mer_w		number(10);
ie_acao_disp_mercado_w		varchar2(1) := 'N';
nr_seq_central_compra_item_w	number(10);

cursor c01 is
select	a.cd_material,
	a.cd_unidade_medida,
	a.nr_sequencia,
	a.qt_material qt_entrega_solicitada,
	trunc(sysdate,'dd') dt_entrega_solicitada,
	b.nr_seq_proj_rec,
	c.cd_estabelecimento
from	sup_central_compra_item a,
	ordem_compra_item b,
	ordem_compra c
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_item_oci = b.nr_item_oci
and	b.nr_ordem_compra = c.nr_ordem_compra
and	a.dt_cancelamento is null
and	nr_seq_sup_central_compra = nr_seq_central_compra_p
and	not exists (	select	1
			from	cot_compra_item x
			where	x.nr_seq_central_compra_item = a.nr_sequencia)
and	not exists (	select	1
			from	cot_compra_solic_agrup x
			where	x.nr_seq_central_compra_item = a.nr_sequencia);

cursor c02 is
select	min(nr_item_cot_compra)
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_w
having	count(*) > 1
group by	cd_material;

begin
select	count(*)
into	qt_existe_w
from	sup_central_compra_item a
where	a.dt_cancelamento is null
and	not exists (	select	1
			from	cot_compra_item x
			where	x.nr_seq_central_compra_item = a.nr_sequencia)
and	not exists (	select	1
			from	cot_compra_solic_agrup x
			where	x.nr_seq_central_compra_item = a.nr_sequencia);

if	(qt_existe_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(189494);
	/*r.aise_application_error(-20011,'Todos os itens deste processo da Central de Compras est�o gerados em cota��es e n�o podem ser gerados novamente!');*/
end if;			

nr_cot_compra_w	:= nvl(nr_cot_compra_p, 0);

if	(nr_cot_compra_w > 0) then
	select	count(*)
	into	qt_existe_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_w;
	if	(qt_existe_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(189496,'NR_COT_COMPRA='||nr_cot_compra_w);
		/*r.aise_application_error(-20011,'N�o existe a cota��o de n�mero ' || nr_cot_compra_w || ' no Sistema');*/
	end if;
end if;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	sup_central_compra
where	nr_sequencia = nr_seq_central_compra_p;

cd_comprador_w	:= obter_pessoa_fisica_usuario(nm_usuario_p, 'C');

if	(nvl(obter_se_usuario_comprador(nm_usuario_p,cd_estabelecimento_w),'N') = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(189498);
	/*r.aise_application_error(-20011,'Voc� n�o est� cadastrado como comprador.');*/
end if;

/* shift  + f11 = 'Regra para consist�ncia do item disp. mercado' */
delete	w_consist_disp_mercado
where	nm_usuario = nm_usuario_p
and	cd_funcao = 915;
commit;

dt_entrega_cotacao_w := trunc(sysdate,'dd');

vardias_retorno_w := nvl(obter_valor_param_usuario(915, 2, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w), 3);
dt_retorno_prev_w := (sysdate + vardias_retorno_w);

if	(nr_cot_compra_w = 0) then
	begin
	select	cot_compra_seq.nextval
	into	nr_cot_compra_w
	from	dual;

	insert into cot_compra(
		nr_cot_compra,
		dt_cot_compra,
		dt_atualizacao,
		cd_comprador,
		nm_usuario,
		cd_pessoa_solicitante,
		cd_estabelecimento,
		dt_retorno_prev,
		ie_finalidade_cotacao,
		dt_entrega)
	values(	nr_cot_compra_w,
		sysdate,
		sysdate,
		cd_comprador_w,
		nm_usuario_p,
		cd_comprador_w,
		cd_estabelecimento_w,
		dt_retorno_prev_w,
		'C',
		dt_entrega_cotacao_w);
	end;
end if;

/* verifica se possui evento na 'Regra para consist�ncia do item disp. mercado' */
select	count(*)
into	qt_existe_regra_disp_mer_w
from	consiste_disp_mercado
where	cd_evento = 'CA';

open c01;
loop
fetch c01 into
	cd_material_w,
	cd_unidade_medida_w,
	nr_seq_central_compra_item_w,
	qt_material_w,
	dt_entrega_solicitada_w,
	nr_seq_proj_rec_w,
	cd_estab_item_w;
exit when c01%notfound;
	begin
	if	(qt_existe_regra_disp_mer_w > 0) then
		begin
		/* verifica se possui evento na 'Regra para consist�ncia do item disp. mercado', e se o material est� dispon�vel no mercado */
		select  substr(obter_acao_regra_disp_mercado('CA', cd_material_w, null, cd_estabelecimento_w),1,1) ie_acao_regra
		into	ie_acao_disp_mercado_w
		from    dual;

		if	(ie_acao_disp_mercado_w <> 'N') then
			begin
			grava_consiste_disp_mercado( 
				915,
				cd_material_w,
				nm_usuario_p,
				ie_acao_disp_mercado_w);
			end;
		end if;

		end;
	end if;

	if	(ie_acao_disp_mercado_w in ('M', 'N')) then
		begin
		select	max(nr_item_cot_compra)
		into	nr_item_w
		from	cot_compra_item
		where	nr_cot_compra = nr_cot_compra_w;
		if	(nr_item_w is null) then
			nr_item_w := 1;
		else
			nr_item_w := nr_item_w + 1;
		end if;
		
		select	nvl(max(ie_regra_preco),'')
		into	ie_regra_preco_w
		from	regra_material_cotacao
		where	cd_material = cd_material_w
		and	ie_situacao = 'A'
		and	cd_estabelecimento = cd_estabelecimento_w;
		
		begin
		insert into cot_compra_item(
			nr_cot_compra,
			nr_item_cot_compra,
			cd_material,
			qt_material,
			cd_unidade_medida_compra,
			dt_atualizacao,
			dt_limite_entrega,
			nm_usuario,
			ie_situacao,
			cd_estab_item,
			ie_regra_preco,
			nr_seq_proj_rec,
			nr_seq_central_compra_item)
		values(	nr_cot_compra_w,
			nr_item_w,
			cd_material_w,
			qt_material_w,
			cd_unidade_medida_w,
			sysdate,
			dt_entrega_solicitada_w,
			nm_usuario_p,
			'A',
			cd_estab_item_w,
			ie_regra_preco_w,
			nr_seq_proj_rec_w,
			nr_seq_central_compra_item_w);

		insert into cot_compra_item_entrega(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_cot_compra,
			nr_item_cot_compra,
			dt_entrega,
			qt_entrega)
		values(	cot_compra_item_entrega_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_cot_compra_w,
			nr_item_w,
			dt_entrega_solicitada_w,
			qt_material_w);
			
		ie_inseriu_item_w := 'S';
		
		gerar_itens_aceitaveis_cotacao(nr_cot_compra_w,nr_item_w,nm_usuario_p);	
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(189499,'DS_ERRO='||sqlerrm);
			/*r.aise_application_error(-20011,'Erro ao gerar cot_compra_item.' || chr(13) || chr(10) || sqlerrm);*/
		end;
		end;
	end if;
	end;
end loop;
close c01;

open c02;
loop
fetch c02 into	
	nr_item_w;
exit when c02%notfound;
	agrupar_cotacao_item(nr_cot_compra_w, nr_item_w, 0, nm_usuario_p);
end loop;
close c02;	

commit;

nr_cot_compra_p := nr_cot_compra_w;

end gerar_cot_compra_cen_com;
/