create or replace
procedure pls_atualiza_vl_regra_pacote(
			nr_seq_composicao_p	pls_pacote_composicao.nr_sequencia%type,
			nr_seq_pacote_p		pls_pacote.nr_sequencia%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar o valor do pacote na regra de pacote.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [x] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:
-------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
vl_pacote_w	number(15,2);

begin

vl_pacote_w := obter_valor_composicao(nr_seq_composicao_p,'T');

update	pls_pacote_tipo_acomodacao
set	vl_pacote = vl_pacote_w
where	nr_seq_composicao 	= nr_seq_composicao_p
and	nr_seq_pacote		= nr_seq_pacote_p;
commit;

end pls_atualiza_vl_regra_pacote;
/