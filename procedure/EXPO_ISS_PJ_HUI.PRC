create or replace
procedure expo_iss_pj_hui(dt_inicio_p			date,
			 dt_fim_p			date,
			 cd_estabelecimento_p		varchar2,
			 nm_usuario_p			varchar2) is

nr_sequencia_nota_w		number(10);
tp_registro_w			number(1);
ds_registro_w			varchar2(1);
nr_nota_fiscal_w		varchar2(255);
cd_serie_nf_w			varchar2(10);
nr_nota_final_w			varchar2(255);
dt_emisao_w			varchar2(10);
ie_situcao_w			varchar2(1);
vl_total_nota_w			varchar2(12);
ds_servicos_prestados_w		varchar2(10);
nr_tomador_w			number(1);
ie_estabelecido_w		varchar2(1);
nm_tomador_w			varchar2(100);
nr_insc_muni_w			varchar2(10);
cd_dig_insc_muni_w		varchar2(2);
cd_cgc_cpf_tomador_w		varchar2(14);
ie_isento_insc_est_w		varchar2(1);
nr_insc_est_w			varchar2(15);
ds_cep_w			varchar2(8);
ds_tipo_lo_w			varchar2(10);
ds_titulo_lo_w			varchar2(5);
ds_lo_w				varchar2(50);
ds_compl_w			varchar2(40);
nr_endereco_w			varchar2(10);
ds_bairro_w			varchar2(50);
sg_uf_w				varchar2(2);
ds_cidade_w			varchar2(50);
ds_local_prestacao_w		varchar2(1);
ds_texto_w			varchar(2000);

Cursor C01 is
select	substr(n.nr_sequencia,1,10),
	1 	tp_registro,
	'C'	ds_registro,
	lpad(n.nr_nota_fiscal, 10, 0) nr_nota_inicial,
	rpad(n.cd_serie_nf, 10, ' ') cd_serie_nf,
	lpad(n.nr_nota_fiscal, 10, 0) nr_nota_final,
	to_char(n.dt_emissao,'dd/mm/yyyy'),
	decode(n.ie_situacao,1,1,2),
	lpad(substr(elimina_caracteres_especiais(campo_mascara_virgula(vl_total_nota)),1,12),12,'0') vl_total_nota,
	lpad(substr(nvl(obter_dados_pf_pj_estab(n.cd_estabelecimento,null,cd_cgc_emitente,'ATIV'),'0'),1,10),10,'0') ds_servicos_prestados, -- verificar este com o cliente o que dever� ser preenchido.
	2 nr_tomador,
	decode(upper(elimina_caracteres_especiais(obter_dados_pf_pj(null,n.cd_cgc,'CI'))),
	upper(elimina_caracteres_especiais(obter_dados_pf_pj(null,n.cd_cgc_emitente,'CI'))),'S','N'),
	rpad(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc, 'N'),1,100),' '),100,' '),
	lpad(nvl(obter_dados_pf_pj(null,n.cd_cgc,'IM'),'0'),10,'0'),
	lpad(' ',2,' '),
	lpad(nvl(n.cd_cgc,' '),14,' '),
	decode(obter_dados_pf_pj(null,n.cd_cgc,'IE'),'','S','N'),
	lpad(nvl(obter_dados_pf_pj(null,n.cd_cgc,'IE'),' '),15,' '),
	lpad(nvl(substr(elimina_caracteres_especiais(obter_dados_pf_pj(null,n.cd_cgc,'CEP')),1,8),' '),8,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'LO'),1,5),' '),5,' '),
	lpad(' ',5,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'R'),1,50),' '),50,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'CO'),1,40),' '),40,' '),
	lpad(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'NR'),1,10),' '),10,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'B'),1,50),' '),50,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'UF'),1,2),' '),2,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null,n.cd_cgc,'CI'),1,50),' '),50,' '),
	decode(o.ie_tipo_natureza, 'D', 'D', 'F' ) ie_natureza_operacao
from	nota_fiscal n,
	natureza_operacao o,
	operacao_nota p
where	n.cd_natureza_operacao = o.cd_natureza_operacao
and	p.cd_operacao_nf = n.cd_operacao_nf
and	obter_se_nota_entrada_saida(n.nr_sequencia) = 'S'
and	p.ie_servico = 'S'
and	n.cd_estabelecimento = cd_estabelecimento_p
and	n.cd_cgc is not null
and	n.dt_emissao between to_date(to_char(dt_inicio_p,'dd/mm/yyyy'),'dd/mm/yyyy') and fim_dia(to_date(to_char(dt_fim_p,'dd/mm/yyyy'),'dd/mm/yyyy'));


begin
open C01;
loop
fetch C01 into
	nr_sequencia_nota_w,
	tp_registro_w,
	ds_registro_w,
	nr_nota_fiscal_w,
	cd_serie_nf_w,
	nr_nota_final_w,
	dt_emisao_w,
	ie_situcao_w,
	vl_total_nota_w,
	ds_servicos_prestados_w,
	nr_tomador_w,
	ie_estabelecido_w,
	nm_tomador_w,
	nr_insc_muni_w,
	cd_dig_insc_muni_w,
	cd_cgc_cpf_tomador_w,
	ie_isento_insc_est_w,
	nr_insc_est_w,
	ds_cep_w,
	ds_tipo_lo_w,
	ds_titulo_lo_w,
	ds_lo_w,
	ds_compl_w,
	nr_endereco_w,
	ds_bairro_w,
	sg_uf_w,
	ds_cidade_w,
	ds_local_prestacao_w;
exit when C01%notfound;
	begin
	ds_texto_w := 	ds_registro_w			||nr_nota_fiscal_w		||
			cd_serie_nf_w			||nr_nota_final_w		||
			dt_emisao_w			||ie_situcao_w			||
			vl_total_nota_w			||ds_servicos_prestados_w	||
			nr_tomador_w			||ie_estabelecido_w		||
			nm_tomador_w			||nr_insc_muni_w		||
			cd_dig_insc_muni_w		||cd_cgc_cpf_tomador_w		||
			ie_isento_insc_est_w		||nr_insc_est_w			||
			ds_cep_w			||ds_tipo_lo_w			||
			ds_titulo_lo_w			||ds_lo_w			||
			ds_compl_w			||nr_endereco_w			||
			ds_bairro_w			||sg_uf_w			||
			ds_cidade_w			||ds_local_prestacao_w;


	insert 	into 	w_deiss_arquivo(nr_sequencia,
					nm_usuario,
					nr_linha,
					cd_registro,
					ds_arquivo)
				values	(w_deiss_arquivo_seq.nextval,
					nm_usuario_p,
					nr_sequencia_nota_w,
					2,
					ds_texto_w);

	end;
end loop;
close C01;

commit;

end expo_iss_pj_hui;
/