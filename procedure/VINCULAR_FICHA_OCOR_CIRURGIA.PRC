create or replace
procedure Vincular_ficha_ocor_cirurgia (
			nr_cirurgia_p		number,
			nr_seq_ficha_ocor_p	number,
			nm_usuario_p		Varchar2) is 

dt_infeccao_w		date;
nr_seq_sitio_princ_w	number(10);
nr_seq_sitio_espec_w	number(10);
	
begin

if 	((nvl(nr_cirurgia_p,0) > 0) and (nvl(nr_seq_ficha_ocor_p,0) > 0)) then
	begin
	select	dt_infeccao,
		nr_seq_sitio_princ,
		nr_seq_sitio_espec
	into	dt_infeccao_w,
		nr_seq_sitio_princ_w,
		nr_seq_sitio_espec_w
	from	cih_local_infeccao
	where	nr_sequencia = nr_seq_ficha_ocor_p;

	update	cirurgia
	set	ie_infeccao 		= 'S',
		dt_infeccao 		= dt_infeccao_w,
		nr_seq_sitio_princ 	= nr_seq_sitio_princ_w,
		nr_seq_sitio_espec 	= nr_seq_sitio_espec_w
	where	nr_cirurgia 		= nr_cirurgia_p;

	commit;
	end;
end if;

end Vincular_ficha_ocor_cirurgia;
/