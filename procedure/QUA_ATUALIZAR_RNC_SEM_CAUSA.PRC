create or replace
procedure qua_atualizar_rnc_sem_causa(	qt_horas_p		number,
					cd_estabelecimento_p	number) is

nr_sequencia_w		number(10,0);
ie_causa_w		varchar2(03);
ie_eficacia_w		varchar2(03);
qt_causa_w		number(10,0);
qt_eficacia_w		number(10,0);

Cursor C01 is
select	a.nr_sequencia
from	qua_nao_conformidade a
where	not exists (	select	1
			from	man_ordem_servico b
			where	a.nr_sequencia = b.nr_seq_nao_conform)
and	not exists (	select	1
			from	qua_nao_conform_causa c
			where	a.nr_sequencia = c.nr_seq_nao_conform)
and	substr(qua_obter_tipo_nao_conform(nr_seq_tipo,'C'),1,3) <> 'NI'
and	dividir(obter_min_entre_datas(a.dt_abertura, sysdate, 1),60) > qt_horas_p
and	cd_estabelecimento	= cd_estabelecimento_p
and	nr_seq_motivo_cancel is null
and	ie_status <> 'Encerrada'
and	ie_status <> 'Cancelada'
and	ie_status <> 'Sem acao'
and	ie_status <> 'Sem causa';

begin

if	(nvl(qt_horas_p,0) > 0) then
	begin
	open C01;
	loop
	fetch C01 into
		nr_sequencia_w;
	exit when C01%notfound;
		begin
		update	qua_nao_conformidade
		set	ie_status 	= 'Sem causa'
		where	nr_sequencia 	= nr_sequencia_w;
		end;
	end loop;
	close C01;
	commit;
	end;
end if;

end qua_atualizar_rnc_sem_causa;
/