create or replace
procedure rop_gerar_itens_regra_roupa(		dt_inventario_p		date,
					cd_estabelecimento_p		number,
					ie_somente_setor_usuario_p		varchar2,
					nm_usuario_p			Varchar2) is

cd_setor_atendimento_w		number(5);
cd_pessoa_resp_w			varchar2(10);
nr_seq_inventario_w		number(10);
nr_seq_roupa_w			number(10);
qt_minimo_w			number(5);
qt_maximo_w			number(5);
nr_seq_kit_w			number(10);
ie_libera_w			varchar2(1);
ie_somente_setor_usuario_w		varchar2(1);
cd_setor_usuario_w		number(5);

cursor c01 is
select	distinct
	a.cd_setor_atendimento
from	rop_regra_roupa_setor a
where	a.cd_estabelecimento = cd_estabelecimento_p
and	nvl(ie_somente_setor_usuario_p,'N') = 'N'
union all
select	distinct
	b.cd_setor_atendimento
from	rop_regra_roupa_setor a,
	rop_regra_roupa_setores b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_estabelecimento = cd_estabelecimento_p
and	a.cd_setor_atendimento <> b.cd_setor_atendimento
and	nvl(ie_somente_setor_usuario_p,'N') = 'N'
union all
select	distinct
	a.cd_setor_atendimento
from	rop_regra_roupa_setor a
where	a.cd_estabelecimento = cd_estabelecimento_p
and	nvl(ie_somente_setor_usuario_p,'N') = 'S'
and	a.cd_setor_atendimento = cd_setor_usuario_w
union all
select	distinct
	b.cd_setor_atendimento
from	rop_regra_roupa_setor a,
	rop_regra_roupa_setores b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_estabelecimento = cd_estabelecimento_p
and	a.cd_setor_atendimento <> b.cd_setor_atendimento
and	nvl(ie_somente_setor_usuario_p,'N') = 'S'
and	b.cd_setor_atendimento = cd_setor_usuario_w;

cursor c02 is
select	a.nr_seq_roupa,
	a.qt_minimo,
	a.qt_maximo
from	rop_regra_roupa_setor a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_setor_atendimento	= cd_setor_atendimento_w
and	a.nr_seq_roupa is not null
union
select	a.nr_seq_roupa,
	b.qt_minimo,
	b.qt_maximo
from	rop_regra_roupa_setor a,
	rop_regra_roupa_setores b
where	a.nr_sequencia 		= b.nr_seq_regra
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_setor_atendimento	<> b.cd_setor_atendimento
and	b.cd_setor_atendimento	= cd_setor_atendimento_w
and	a.nr_seq_roupa is not null;

cursor c03 is
select	a.nr_seq_kit,
	a.qt_minimo,
	a.qt_maximo
from	rop_regra_roupa_setor a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_setor_atendimento	= cd_setor_atendimento_w
and	a.nr_seq_kit is not null
union
select	a.nr_seq_kit,
	b.qt_minimo,
	b.qt_maximo
from	rop_regra_roupa_setor a,
	rop_regra_roupa_setores b
where	a.nr_sequencia 		= b.nr_seq_regra
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_setor_atendimento	<> b.cd_setor_atendimento
and	b.cd_setor_atendimento	= cd_setor_atendimento_w
and	a.nr_seq_kit is not null;

begin

select	(max(obter_valor_param_usuario(1301, 71, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)))
into	ie_libera_w
from	dual;

select	nvl(max(obter_setor_usuario(nm_usuario_p)),0)
into	cd_setor_usuario_w
from	dual;

open C01;
loop
fetch C01 into
	cd_setor_atendimento_w;
exit when C01%notfound;
	begin
	select	nvl(substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,100),'X'),
		rop_inv_reposicao_seq.nextval
	into	cd_pessoa_resp_w,
		nr_seq_inventario_w
	from	dual;

	if	(cd_pessoa_resp_w = 'X') then
		wheb_mensagem_pck.exibir_mensagem_abort(266269);
		--'Usu�rio sem cadastro de pessoa fis�ca. Favor cadastrar o mesmo.');
	end if;

	insert into rop_inv_reposicao(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_inventario,
		nm_usuario_lib,
		dt_liberacao,
		cd_setor_atendimento,
		cd_estabelecimento,
		cd_pessoa_resp,
		pr_reposicao)
	values(	nr_seq_inventario_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		dt_inventario_p,
		null,
		null,
		cd_setor_atendimento_w,
		cd_estabelecimento_p,
		cd_pessoa_resp_w,
		100);
	
	open C02;
	loop
	fetch C02 into
		nr_seq_roupa_w,
		qt_minimo_w,
		qt_maximo_w;
	exit when C02%notfound;
		begin

		insert into rop_inv_reposicao_item(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_inventario,
			nr_seq_roupa,
			qt_contagem,
			qt_minimo,
			qt_maximo,
			qt_repor)
		values(	rop_inv_reposicao_item_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_inventario_w,
			nr_seq_roupa_w,
			0,
			qt_minimo_w,
			qt_maximo_w,
			0);
		end;
	end loop;
	close C02;

	open C03;
	loop
	fetch C03 into
		nr_seq_kit_w,
		qt_minimo_w,
		qt_maximo_w;
	exit when C03%notfound;
		begin

		insert into rop_inv_reposicao_item(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_inventario,
			nr_seq_kit,
			qt_contagem,
			qt_minimo,
			qt_maximo,
			qt_repor)
		values(	rop_inv_reposicao_item_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_inventario_w,
			nr_seq_kit_w,
			0,
			qt_minimo_w,
			qt_maximo_w,
			0);
		end;
	end loop;
	close C03;
	
	if	(ie_libera_w = 'S') then
		update	rop_inv_reposicao
		set	dt_liberacao	= sysdate,
			nm_usuario_lib	= nm_usuario_p
		where	nr_sequencia	= nr_seq_inventario_w;
	end if;	
	
	end;
end loop;
close C01;

commit;

end rop_gerar_itens_regra_roupa;
/