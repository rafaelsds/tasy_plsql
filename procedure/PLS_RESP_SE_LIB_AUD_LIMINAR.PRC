create or replace
procedure pls_resp_se_lib_aud_liminar
			(	nr_seq_auditoria_p	pls_auditoria.nr_sequencia%type,
				ie_lib_liminar_p	number,
				nm_usuario_p		Varchar2) is 
			
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;
nr_seq_guia_w			pls_guia_plano.nr_sequencia%type;
ie_lib_texto_w			varchar2(10);

begin

begin
	select	nr_seq_requisicao,
		nr_seq_guia
	into	nr_seq_requisicao_w,
		nr_seq_guia_w
	from	pls_auditoria
	where	nr_sequencia = nr_seq_auditoria_p;
exception
when others then
	nr_seq_requisicao_w	:= null;
	nr_seq_guia_w		:= null;
end;

select	decode(ie_lib_liminar_p,0,'foi','n�o foi') 
into	ie_lib_texto_w
from	dual;

if	(nr_seq_requisicao_w	is not null) then
	pls_requisicao_gravar_hist(	nr_seq_requisicao_w, 'L', 'O auditor '||obter_nome_usuario(nm_usuario_p)|| ' informou que esta an�lise '||
						ie_lib_texto_w || ' liberada devido a liminar do benefici�rio.',
						null, nm_usuario_p);
elsif	(nr_seq_guia_w	is not null) then
	pls_guia_gravar_historico(nr_seq_guia_w,2,'O auditor '||obter_nome_usuario(nm_usuario_p)|| ' informou que esta an�lise '||
						ie_lib_texto_w || ' liberada devido a liminar do benefici�rio.','',nm_usuario_p);
end if;

end pls_resp_se_lib_aud_liminar;
/
