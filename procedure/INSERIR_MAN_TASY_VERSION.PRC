create or replace procedure inserir_man_tasy_version(
	cd_version_p varchar2,
	nr_service_pack_p number
) is
	is_registro_existente_w varchar2(1);
begin

	select 	nvl(max('S'), 'N')
	into 	is_registro_existente_w
	from 	man_tasy_version
	where 	cd_version 	= cd_version_p
	and 	nr_service_pack 	= nr_service_pack_p
	;
	
	if (is_registro_existente_w = 'N') then
		insert into man_tasy_version(
			cd_version,
			nr_service_pack
		) values (
			cd_version_p,
			nr_service_pack_p
		);
		commit;
	end if;

end inserir_man_tasy_version;
/