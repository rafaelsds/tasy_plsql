create or replace
procedure pls_gerar_utilizacao_benef (	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
					dt_inicio_p		date,
					dt_fim_p		date,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nr_id_transacao_p	out w_pls_utilizacao_benef.nr_id_transacao%type) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Realizar a chamada das rotinas que geram a utiliza��o do benefici�rio

-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
		Essa rotina apenas faz a camada intermediaria entre a package
		e as rotinas em java e relat�rios, que n�o acessam diretamente 
		a package.
		
		Essa rotina deve apenas chamar a package e devolver os outputs dela,
		nenhuma regra de negocio deve colocado aqui.

Altera��es:	
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

pls_utilizacao_benef_pck.gerar_utilizacao_benef (	nr_seq_segurado_p,
							dt_inicio_p,
							dt_fim_p,
							nm_usuario_p,
							cd_estabelecimento_p,
							nr_id_transacao_p,
							null);

end pls_gerar_utilizacao_benef;
/