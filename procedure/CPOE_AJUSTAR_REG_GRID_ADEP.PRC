create or replace
procedure cpoe_ajustar_reg_grid_adep(
			nr_prescricao_p		number,
         nr_atendimento_p		number,
         nr_seq_registro_p    number,
         ie_item_reg_p        varchar2,
         ie_situacao_adep_p   varchar2,/*Se o item est� suspenso, aprazado, ect....*/
         nm_usuario_p         varchar2,
         ie_tipo_dieta_p      varchar2 ) is

         
begin

   /*Dietas*/
if (ie_item_reg_p = 'D') then
   begin
      if (ie_tipo_dieta_p = 'E') or
         (ie_tipo_dieta_p = 'S') then
         cpoe_gerar_grid_enteral(nr_atendimento_p, nr_seq_registro_p, nm_usuario_p);
      elsif(ie_tipo_dieta_p = 'L') then
         cpoe_gerar_grid_leite_deriv(nr_atendimento_p, nr_seq_registro_p, nm_usuario_p);     
      elsif(ie_tipo_dieta_p = 'O') then
         cpoe_gerar_grid_oral(nr_atendimento_p, nr_seq_registro_p, nm_usuario_p);      
      end if;
   end;
   /*Gasoterapia*/
elsif (ie_item_reg_p = 'G') then
   cpoe_gerar_grid_gas(nr_atendimento_p, nr_seq_registro_p, nm_usuario_p);   
   /*Medicamentos*/
elsif (ie_item_reg_p = 'M') then
   cpoe_gerar_grid_material(nr_atendimento_p, nr_seq_registro_p, nm_usuario_p);
   /*Procedimentos*/
elsif (ie_item_reg_p = 'P') then
   cpoe_gerar_grid_proc(nr_atendimento_p, nr_seq_registro_p, nm_usuario_p);
   /*Recomendacao*/
elsif (ie_item_reg_p = 'R') then
   cpoe_gerar_grid_rec(nr_atendimento_p, nr_seq_registro_p, nm_usuario_p);
   /*Solu��es*/
elsif (ie_item_reg_p = 'S') then
   cpoe_gerar_grid_solucao(nr_atendimento_p, nr_seq_registro_p, nm_usuario_p);
end if;

commit;

end cpoe_ajustar_reg_grid_adep;
/
