create or replace
procedure consiste_pac_alerg_med_rotina (
		cd_pessoa_fisica_p	varchar2,
		cd_protocolo_p		number,
		nr_sequencia_p		number,
		nr_seq_material_p	number,
		ds_item_p out		varchar2,
		ie_retorno_p out	varchar2) is

cd_material_w	number(6,0);
ie_retorno_w	varchar2(1);
ds_item_w	varchar2(255);
		
begin
if	(nr_sequencia_p is not null) and
	(nr_seq_material_p is not null) and
	(cd_protocolo_p is not null) then
	begin
	
	select	max(nvl(cd_material,0))
	into	cd_material_w
	from	protocolo_medic_material
	where	cd_protocolo	= cd_protocolo_p
	and	nr_sequencia	= nr_sequencia_p
	and	nr_seq_material = nr_seq_material_p;
	
	verifica_paciente_alergico(
		cd_pessoa_fisica_p,
		cd_material_w,
		ds_item_w,
		ie_retorno_w);
	end;	
end if;
ie_retorno_p	:= ie_retorno_w;
ds_item_p	:= ds_item_w;
commit;
end consiste_pac_alerg_med_rotina;
/