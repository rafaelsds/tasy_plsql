create or replace
procedure dmed_anual_arquivo_rtop
			(	nr_sequencia_p			number,
				cd_pessoa_titular_p		varchar,
				nm_usuario_p			varchar2,
				nr_cpf_titular_p		varchar2,
				nr_linha_p		in out	number) is

nr_linha_w		number(10)	:= nr_linha_p;
ds_arquivo_w		varchar2(2000);
cpf_cnpj_benef_w 	varchar(15);
nm_benef_w		varchar2(150);
nm_titular_w		varchar2(255);
vl_pago_w		number(15,2);
contador_w		number(10)	:= 0;
qt_registros_w		number(10);	
cd_cgc_pfprestador_w	varchar(15);	
order_union_w		number(1);						

cursor c_reemb_titular is
	--onde o beneficirio  pessoa fsica
	select	1 order_union,
		p.nr_cpf cpf_cnpj_benef,
		p.cd_pessoa_fisica cd_cgc_pfprestador, --t.cd_pf_prestador cd_cgc_pfprestador,
		p.nm_pessoa_fisica nm_benef
	from	pessoa_fisica		p
	where	exists	(select	1
			from	dmed_titulos_mensal	t,
				dmed_agrupar_lote	l,
				pessoa_fisica		f
			where	p.cd_pessoa_fisica	= t.cd_pf_prestador
			and	t.nr_seq_dmed_mensal	= l.nr_seq_dmed_mensal
			and	t.cd_pessoa_titular	= t.cd_pessoa_beneficiario
			and	t.cd_pessoa_titular	= f.cd_pessoa_fisica
			and	t.ie_prestadora_ops	= 'O'
			and	t.ie_tipo_documento	= 'RE'
			and	t.cd_cgc is null
			and	f.nr_cpf		= nr_cpf_titular_p
			and	l.nr_seq_dmed_anual	= nr_sequencia_p)
	union
	--onde o beneficirio  pessoa juridica
	select	2 order_union,
		p.cd_cgc cpf_cnpj_benef,
		p.cd_cgc cd_cgc_pfprestador,--t.cd_cgc ,
		p.ds_razao_social nm_benef
	from	pessoa_juridica		p
	where	exists	(select	1
			from	dmed_titulos_mensal	t,
				dmed_agrupar_lote	l,
				pessoa_fisica		f
			where	p.cd_cgc		= t.cd_cgc
			and	t.nr_seq_dmed_mensal	= l.nr_seq_dmed_mensal
			and	t.cd_pessoa_titular	= t.cd_pessoa_beneficiario
			and	t.cd_pessoa_titular	= f.cd_pessoa_fisica
			and	t.ie_prestadora_ops	= 'O'
			and	t.ie_tipo_documento	= 'RE'
			and 	t.cd_cgc is not null
			and	f.nr_cpf		= nr_cpf_titular_p
			and	l.nr_seq_dmed_anual	= nr_sequencia_p)
	order by
		order_union,
		cpf_cnpj_benef;
		
type 		fetch_array is table of c_reemb_titular%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_reemb_titular_w	vetor;

begin
open c_reemb_titular;
loop
fetch c_reemb_titular bulk collect into s_array limit 1000;
	vetor_reemb_titular_w(i)	:= s_array;
	i				:= i + 1;
exit when c_reemb_titular%notfound;
end loop;
close c_reemb_titular;

for i in 1..vetor_reemb_titular_w.count loop
	begin
	s_array := vetor_reemb_titular_w(i);
	for z in 1..s_array.count loop
		order_union_w			    := s_array(z).order_union;
		cpf_cnpj_benef_w		  := s_array(z).cpf_cnpj_benef;
		cd_cgc_pfprestador_w	:= s_array(z).cd_cgc_pfprestador;
		nm_benef_w				    := substr(fis_remove_special_characters(s_array(z).nm_benef), 1, 150);
		
		begin
		--verifica se o pagador/titular tem algum reembolso no DMED mensal
		select	sum(t.vl_pago)
		into	vl_pago_w
		from	dmed_titulos_mensal	t,
			dmed_agrupar_lote	l,
			pessoa_fisica		p
		where	t.nr_seq_dmed_mensal	= l.nr_seq_dmed_mensal
		and	t.cd_pessoa_titular	= t.cd_pessoa_beneficiario
		and	t.ie_prestadora_ops	= 'O'
		and	t.ie_tipo_documento	= 'RE'
		and	((t.cd_cgc = cd_cgc_pfprestador_w) or (t.cd_pf_prestador = cd_cgc_pfprestador_w))
		and	t.cd_pessoa_titular	= p.cd_pessoa_fisica
		and	p.nr_cpf		= nr_cpf_titular_p
		and	l.nr_seq_dmed_anual	= nr_sequencia_p;

		if	(vl_pago_w > 0) then
			vl_pago_w	:= replace(replace(to_char(vl_pago_w,'999,999,990.00'),'.',''),',','');
			
			ds_arquivo_w 	:= 'RTOP' || '|' || cpf_cnpj_benef_w || '|' || nm_benef_w || '|' || vl_pago_w || '|' || '|';
		else
			--quando s o dependente tem reembolso o valor do titular fica vazio no arquivo
			ds_arquivo_w 	:= 'RTOP' || '|' || cpf_cnpj_benef_w || '|' || nm_benef_w || '|' || '' || '|' || '|';
		end if;

		contador_w	:= contador_w + 1;

		nr_linha_w	:= nr_linha_w + 1;
			
		insert into w_dacon
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_arquivo,
			nr_linha,
			ie_tipo_registro)
		values	(w_dacon_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			ds_arquivo_w,
			nr_linha_w,
			6);

		if	(mod(contador_w,100) = 0) then
			commit;
		end if;
		end;
	end loop;
	end;
end loop;

nr_linha_p	:= nr_linha_w;

commit;

end dmed_anual_arquivo_rtop;
/