create or replace
procedure gerar_w_ranking_fornec(	ie_analitico_sintetico_p			varchar2,
					ie_considera_cancel_p		varchar2,
					dt_inicial_p			date,
					dt_final_p			date,
					ds_lista_pessoa_p			varchar2,
					ds_lista_cgc_p			varchar2,
					vl_inicial_p			number,
					vl_final_p				number,
					qtd_registros_p			number,
					nm_usuario_p			varchar2,
					ds_lista_estab_p		varchar2) is



cd_cgc_w		varchar2(14);
cd_classif_w            number(5);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w      varchar2(10);
ds_fornecedor_w         varchar2(80);
dt_baixa_w              date;
dt_contabil_w           date;
dt_emissao_w            date;
nr_sequencia_w          number(10);
nr_titulo_w             number(10);
vl_desconto_w           number(15,2);
vl_juros_w              number(15,2);
vl_multa_w              number(15,2);
vl_original_w           number(15,2);
vl_pago_w               number(15,2);
vl_tot_pago_w           number(15,2);
ie_continua_w		varchar2(1);
nr_tit_anterior_w	number(10);
ds_lista_pessoa_w	varchar2(4000);
ds_lista_cgc_w		varchar2(4000);
ie_todos_menos_cgc_w	varchar2(1)	:= 'N';
ie_todos_menos_pf_w	varchar2(1)	:= 'N';
ds_lista_estab_w	varchar2(4000);

/* maiores fornecedores */
cursor c01 is
select	cd_pessoa_fisica,
	cd_cgc,
	sum(vl) vl_pago
from		
	(select	a.cd_pessoa_fisica,
		a.cd_cgc,
		sum(nvl(b.vl_pago,0)) vl	
	from	titulo_pagar_baixa b,
		titulo_pagar a	
	where	b.nr_titulo		= a.nr_titulo
	/*  ahoffelder - OS 262677 - 09/11/2010 - tirei todas as restri��es por estabelecimento, pq foi inclu�do filtro no relat�rio
	and	a.cd_estabelecimento	= cd_estabelecimento_p */
	and	b.dt_baixa between dt_inicial_p and dt_final_p
	and	ds_lista_cgc_p is null
	and	ds_lista_pessoa_p is null
	and	(('N' = ie_considera_cancel_p and a.ie_situacao <> 'C') or ('S' = ie_considera_cancel_p and a.ie_situacao is not null))
	and	(ds_lista_estab_p is null or ds_lista_estab_w like '% ' || to_char(a.cd_estabelecimento) || ' %')
	group by
		a.cd_pessoa_fisica,
		a.cd_cgc
	union all
	select	a.cd_pessoa_fisica,
		a.cd_cgc,
		sum(nvl(b.vl_pago,0)) vl	
	from	titulo_pagar_baixa b,
		titulo_pagar a	
	where	b.nr_titulo		= a.nr_titulo
	-- and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.dt_baixa between dt_inicial_p and dt_final_p
	and	a.cd_cgc is not null
	and	ds_lista_cgc_p is not null
	and	(('N' = ie_considera_cancel_p and a.ie_situacao <> 'C') or ('S' = ie_considera_cancel_p and a.ie_situacao is not null))	
	and	ds_lista_cgc_w like '% ' || a.cd_cgc || ' %'
	and	ie_todos_menos_cgc_w = 'N'
	and	(ds_lista_estab_p is null or ds_lista_estab_w like '% ' || to_char(a.cd_estabelecimento) || ' %')
	group by
		a.cd_pessoa_fisica,
		a.cd_cgc
	union all
	select	a.cd_pessoa_fisica,
		a.cd_cgc,
		sum(nvl(b.vl_pago,0)) vl	
	from	titulo_pagar_baixa b,
		titulo_pagar a	
	where	b.nr_titulo		= a.nr_titulo
	-- and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.dt_baixa between dt_inicial_p and dt_final_p
	and	a.cd_cgc is not null
	and	ds_lista_cgc_p is not null
	and	(('N' = ie_considera_cancel_p and a.ie_situacao <> 'C') or ('S' = ie_considera_cancel_p and a.ie_situacao is not null))
	and	ds_lista_cgc_w not like '% ' || a.cd_cgc || ' %'
	and	ie_todos_menos_cgc_w = 'S'
	and	(ds_lista_estab_p is null or ds_lista_estab_w like '% ' || to_char(a.cd_estabelecimento) || ' %')
	group by
		a.cd_pessoa_fisica,
		a.cd_cgc
	union all
	select	a.cd_pessoa_fisica,
		a.cd_cgc,
		sum(nvl(b.vl_pago,0)) vl	
	from	titulo_pagar_baixa b,
		titulo_pagar a	
	where	b.nr_titulo		= a.nr_titulo
	-- and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.dt_baixa between dt_inicial_p and dt_final_p
	and	a.cd_pessoa_fisica is not null
	and	ds_lista_pessoa_p is not null
	and	(('N' = ie_considera_cancel_p and a.ie_situacao <> 'C') or ('S' = ie_considera_cancel_p and a.ie_situacao is not null))
	and	ds_lista_pessoa_w like '% ' || a.cd_pessoa_fisica || ' %'
	and	ie_todos_menos_pf_w = 'N'
	and	(ds_lista_estab_p is null or ds_lista_estab_w like '% ' || to_char(a.cd_estabelecimento) || ' %')
	group by
		a.cd_pessoa_fisica,
		a.cd_cgc
	union all
	select	a.cd_pessoa_fisica,
		a.cd_cgc,
		sum(nvl(b.vl_pago,0)) vl	
	from	titulo_pagar_baixa b,
		titulo_pagar a	
	where	b.nr_titulo		= a.nr_titulo
	-- and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.dt_baixa between dt_inicial_p and dt_final_p
	and	a.cd_pessoa_fisica is not null
	and	ds_lista_pessoa_p is not null
	and	(('N' = ie_considera_cancel_p and a.ie_situacao <> 'C') or ('S' = ie_considera_cancel_p and a.ie_situacao is not null))
	and	ds_lista_pessoa_w not like '% ' || a.cd_pessoa_fisica || ' %'
	and	ie_todos_menos_pf_w = 'S'
	and	(ds_lista_estab_p is null or ds_lista_estab_w like '% ' || to_char(a.cd_estabelecimento) || ' %')
	group by
		a.cd_pessoa_fisica,
		a.cd_cgc
	)
having  sum(vl) between nvl(vl_inicial_p,0) and nvl(vl_final_p,9999999999)
group by
	cd_pessoa_fisica,
	cd_cgc
order by
	vl_pago desc;


/* titulos por fornecedores */
cursor c02 is
select	a.dt_contabil,             
	a.dt_emissao,              
	a.nr_titulo,               
	b.dt_baixa,	
	nvl(b.vL_descontos,0),             
	nvl(b.vl_juros,0),
	nvl(b.vl_multa,0),             
	nvl(a.vl_titulo,0),             
	nvl(b.vl_pago,0),
	a.cd_estabelecimento	
from	titulo_pagar_baixa b,
	titulo_pagar a	
where	b.dt_baixa between dt_inicial_p and dt_final_p		
and	b.nr_titulo 		= a.nr_titulo
-- and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w        
and	(('N' = ie_considera_cancel_p and a.ie_situacao <> 'C') or ('S' = ie_considera_cancel_p and a.ie_situacao is not null))
and	(ds_lista_estab_p is null or ds_lista_estab_w like '% ' || to_char(a.cd_estabelecimento) || ' %')
union all
select	a.dt_contabil,             
	a.dt_emissao,              
	a.nr_titulo,               
	b.dt_baixa,
	nvl(b.vL_descontos,0),             
	nvl(b.vl_juros,0),
	nvl(b.vl_multa,0),             
	nvl(a.vl_titulo,0),             
	nvl(b.vl_pago,0),
	a.cd_estabelecimento
from	titulo_pagar_baixa b,
	titulo_pagar a	
where	b.dt_baixa between dt_inicial_p and dt_final_p		
and	b.nr_titulo 		= a.nr_titulo
-- and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_cgc		= cd_cgc_w  
and	(('N' = ie_considera_cancel_p and a.ie_situacao <> 'C') or ('S' = ie_considera_cancel_p and a.ie_situacao is not null))
and	(ds_lista_estab_p is null or ds_lista_estab_w like '% ' || to_char(a.cd_estabelecimento) || ' %')
order by
	3;
	
begin

delete from w_ranking_fornec where nm_usuario = nm_usuario_p;

if	(substr(ds_lista_cgc_p,1,1) = '-') then
	ie_todos_menos_cgc_w	:= 'S';
end if;

if	(substr(ds_lista_pessoa_p,1,1) = '-') then
	ie_todos_menos_pf_w	:= 'S';
end if;

ds_lista_cgc_w		:= ' ' || replace(replace(replace(replace(ds_lista_cgc_p,'(',' '),')',' '),',',' '),'-','') || ' ';
ds_lista_pessoa_w	:= ' ' || replace(replace(replace(replace(ds_lista_pessoa_p,'(',' '),')',' '),',',' '),'-','') || ' ';
ds_lista_estab_w	:= ' ' || replace(replace(replace(replace(ds_lista_estab_p,'(',' '),')',' '),',',' '),'-','') || ' ';

cd_classif_w		:= 1;
ie_continua_w		:= 'S';

open c01;
loop
fetch c01 into
	cd_pessoa_fisica_w, 
	cd_cgc_w, 
	vl_tot_pago_w;
exit when c01%notfound or ie_continua_w	= 'N';

	ds_fornecedor_w	:= obter_nome_pf_pj(cd_pessoa_fisica_w,cd_cgc_w);
	
	nr_tit_anterior_w	:= '';

	open c02;	
	loop
	fetch c02 into
		dt_contabil_w,           
		dt_emissao_w,            
		nr_titulo_w,             
		dt_baixa_w,              	
		vl_desconto_w,           
		vl_juros_w,              
		vl_multa_w,
		vl_original_w,
		vl_pago_w,
		cd_estabelecimento_w;
	exit when c02%notfound;

		if	(nr_tit_anterior_w = nr_titulo_w) then
			vl_original_w := 0;
		end if;
		nr_tit_anterior_w := nr_titulo_w;
	
		insert into w_ranking_fornec
			(cd_cgc,                  
			cd_classif,              
			cd_estabelecimento,      
			cd_pessoa_fisica,        
			ds_fornecedor,           
			dt_atualizacao,          
			dt_atualizacao_nrec,
			dt_baixa,                
			dt_contabil,             
			dt_emissao,              
			nm_usuario,              
			nm_usuario_nrec,         
			nr_sequencia,            
			nr_titulo,               
			vl_desconto,             
			vl_juros,                
			vl_multa,                
			vl_original,             
			vl_pago)
		values	
			(cd_cgc_w,                  
			cd_classif_w,
			cd_estabelecimento_w,
			cd_pessoa_fisica_w,        
			ds_fornecedor_w,
			sysdate,
			sysdate,           
			dt_baixa_w,                
			dt_contabil_w,             
			dt_emissao_w, 
			nm_usuario_p,             
			nm_usuario_p,
			w_ranking_fornec_seq.nextval,	
			nr_titulo_w,               
			vl_desconto_w,             
			vl_juros_w,                
			vl_multa_w,                
			vl_original_w,             
			vl_pago_w);

	end loop;
	close c02;

	cd_classif_w	:= cd_classif_w + 1;

	if	(cd_classif_w > qtd_registros_p) then
		ie_continua_w	:= 'N';	
	end if;	

end loop;
close c01;

commit;

end gerar_w_ranking_fornec;
/