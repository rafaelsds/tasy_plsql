create or replace
procedure pls_inserir_obito_pf
		(	cd_pessoa_fisica_p	varchar2,
			dt_obito_p		date,
			nr_certidao_obito_p	varchar2,
			nm_usuario_p		Varchar2,
			ie_certidao_obito_p	varchar2) is 

begin

if	(cd_pessoa_fisica_p is not null) and
	((nr_certidao_obito_p is not null and ie_certidao_obito_p = 'S') or
	 (ie_certidao_obito_p = 'N')) then
	 
	update	pessoa_fisica
	set	dt_obito		= dt_obito_p,
		nr_certidao_obito	= nr_certidao_obito_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
end if;

end pls_inserir_obito_pf;
/