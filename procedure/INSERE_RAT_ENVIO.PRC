create or replace
procedure Insere_Rat_Envio(
			nm_usuario_p		Varchar2,
			nr_sequencia_p		number,
			ds_email_destino_cc_p Varchar2,
			ds_observacao_p	  Varchar2) is 

begin
insert into proj_rat_envio( nr_sequencia, 
                                                                        nr_seq_rat,
                                                                        dt_atualizacao, 
                                                                        nm_usuario,
                                                                        dt_atualizacao_nrec, 
                                                                        nm_usuario_nrec, 
                                                                        dt_envio,
                                                                        ds_destino, 
																		ds_observacao) 
                                      values                           (proj_rat_envio_seq.nextval, 
                                                                        nr_sequencia_p,
                                                                        sysdate, 
                                                                        nm_usuario_p,
                                                                        sysdate,
                                                                        nm_usuario_p,
                                                                        sysdate, 
																		ds_email_destino_cc_p,
																		ds_observacao_p);

commit;

end Insere_Rat_Envio;
/	
