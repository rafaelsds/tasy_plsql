create or replace
procedure verifica_anestesia_agenda( nr_seq_proc_interno_p		number,
				     qt_minuto_duracao_p 	   out number) is

nr_minuto_duracao_w number(10);				     

begin

if	(nvl(nr_seq_proc_interno_p,0) > 0) then
		select	nvl(max(nr_minuto_duracao),0)
		into	nr_minuto_duracao_w
		from	ageint_tempo_exame_anest
		where	nr_seq_proc_interno	= nr_seq_proc_interno_p
		and 	nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento;
	
	if	( nr_minuto_duracao_w	> 0 ) then
		qt_minuto_duracao_p	:= nr_minuto_duracao_w;
	end if;
end if;

commit;

end verifica_anestesia_agenda;
/
