create or replace
procedure Cancelar_negociacao_cheque(	nr_seq_caixa_rec_p	in	number,
			     		nm_usuario_p		in	varchar2) is

dt_fechamento_w		date;
nr_seq_cheque_w		number(10);
nr_seq_caixa_w		number(10);
nr_seq_lote_w		number(10);
dt_fechamento_saldo_w	date;
dt_cancelamento_w	date;
nr_sequencia_w		number(10);
nr_seq_cheque_neg_w	number(10);

Cursor c01 is
select	nr_sequencia,
	nr_seq_cheque
from	cheque_cr_negociado
where	nr_seq_caixa_rec = nr_seq_caixa_rec_p;

cursor	c02 is
select	a.nr_seq_cheque
from	cheque_cr a
where	a.nr_seq_caixa_rec	= nr_seq_caixa_rec_p;

begin

-- Edgar 29/01/2008
lock table movto_trans_financ in exclusive mode;
lock table caixa_receb in exclusive mode;

select	dt_fechamento,
	dt_cancelamento
into	dt_fechamento_w,
	dt_cancelamento_w
from	caixa_receb
where	nr_sequencia	= nr_seq_caixa_rec_p;

if	(dt_cancelamento_w is not null) then
	/* Este recebimento j� foi cancelado! */
	wheb_mensagem_pck.exibir_mensagem_abort(242056);
end if;

if	(dt_fechamento_w is not null) then

	select	b.nr_seq_caixa,
		b.dt_fechamento
	into	nr_seq_caixa_w,
		dt_fechamento_saldo_w
	from	caixa_saldo_diario b,
		caixa_receb a
	where	a.nr_seq_saldo_caixa	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_caixa_rec_p;

	if	(dt_fechamento_saldo_w is not null) then
		/* O saldo di�rio deste recebimento j� est� fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(242057);
	end if;

	select	max(nr_seq_lote)
	into	nr_seq_lote_w
	from	movto_trans_financ
	where	nr_seq_caixa_rec	= nr_seq_caixa_rec_p;

	Estornar_lote_tesouraria(nr_seq_caixa_w,
				nr_seq_lote_w,
				nm_usuario_p,
				'N',
				null);

	open C01;
	loop
	fetch C01 into
		nr_sequencia_w,
		nr_seq_cheque_neg_w;
	exit when c01%notfound;
		insert into cheque_cr_negociado
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_caixa_rec,
			nr_seq_cheque,
			vl_negociado,
			vl_desconto,
			vl_acrescimo,
			dt_negociacao,
			nr_seq_trans_financ,
			vl_terceiro,
			vl_juros,
			vl_multa)
		select	cheque_cr_negociado_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_caixa_rec_p,
			nr_seq_cheque,
			vl_negociado * -1,
			vl_desconto * -1,
			vl_acrescimo * -1,
			dt_negociacao,
			nr_seq_trans_financ,
			vl_terceiro * -1,
			nvl(vl_juros,0) * - 1,
			nvl(vl_multa,0) * - 1
		from	cheque_cr_negociado
		where	nr_sequencia	= nr_sequencia_w;

		update	cheque_cr
		set	dt_devolucao	= null,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			vl_saldo_negociado= null
		where	nr_seq_cheque	= nr_seq_cheque_neg_w;
	end loop;
	close c01;

	/* Devolver cheques vinculados */
	update	cheque_cr
	set	dt_devolucao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		vl_saldo_negociado= null
	where	nr_seq_caixa_rec	= nr_seq_caixa_rec_p;
	-- dsantos em 11/02/2010 atualizou o vl_saldo_negociado para nulo quando cancelar negociacao

	/* Cancelar cart�es vinculados */
	update	movto_cartao_cr
	set	dt_cancelamento	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_seq_caixa_rec	= nr_seq_caixa_rec_p;

	/* Cancelar recebimento de caixa */
	update	caixa_receb
	set	dt_cancelamento	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_caixa_rec_p;
else
	delete	from	cheque_cr_negociado
	where	nr_seq_caixa_rec	= nr_seq_caixa_rec_p;

	delete	from	movto_trans_financ
	where	nr_seq_lote	is null
	and	nr_seq_caixa_rec	= nr_seq_caixa_rec_p;

	delete	from	cheque_cr
	where	nr_seq_caixa_rec	= nr_seq_caixa_rec_p;

	delete	from	movto_cartao_cr_parcela
	where	nr_seq_movto	in	(select	nr_sequencia
					 from	movto_cartao_cr
					 where	nr_seq_caixa_rec = nr_seq_caixa_rec_p);

	delete	from	movto_cartao_cr
	where	nr_seq_caixa_rec	= nr_seq_caixa_rec_p;

	delete	from	caixa_receb
	where	nr_sequencia		= nr_seq_caixa_rec_p;
end if;

open	c02;
loop
fetch	c02 into
	nr_seq_cheque_w;
exit	when c02%notfound;
	/*A negocia��o de cheques #@NR_SEQ_CAIXA_REC_P#@, onde o cheque encontra-se, foi cancelada.*/
	gerar_cheque_cr_hist(nr_seq_cheque_w,wheb_mensagem_pck.get_texto(305681, 'NR_SEQ_CAIXA_REC_P=' || nr_seq_caixa_rec_p),'N',nm_usuario_p);

end	loop;
close	c02;

commit;

end Cancelar_negociacao_cheque;
/
