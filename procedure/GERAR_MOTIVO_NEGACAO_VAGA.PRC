create or replace
procedure gerar_motivo_negacao_vaga(nr_sequencia_p			number,
				nr_seq_motivo_negacao_vaga_p		number,
						ds_justificativa_negacao_p	varchar2 default null ) is 

nr_seq_motivo_w 	solic_vaga_motivo_negacao.nr_seq_motivo_historico%type;
ds_motivo_w 		solic_vaga_motivo_negacao.ds_motivo%type;
ds_historico_w		gestao_vaga_hist.ds_historico%type;

begin

if	(nr_sequencia_p is not null) and (nr_seq_motivo_negacao_vaga_p is not null) then
	begin
		update	gestao_vaga
		set	nr_seq_motivo_negacao_vaga = nr_seq_motivo_negacao_vaga_p,
			ds_justificativa_negacao_vaga = ds_justificativa_negacao_p
		where	nr_sequencia = nr_sequencia_p;

		select	nr_seq_motivo_historico,
				ds_motivo
		into	nr_seq_motivo_w,
				ds_motivo_w
		from	solic_vaga_motivo_negacao
		where	nr_sequencia = nr_seq_motivo_negacao_vaga_p;

		if (nr_seq_motivo_w is not null) then


			ds_historico_w := obter_texto_tasy(1026370, null) ||': '|| ds_motivo_w || chr(13) || chr(10) ||
				obter_texto_tasy(1026373, null)||': '|| ds_justificativa_negacao_p;
		
			gerar_hist_alteracao_status(nr_sequencia_p, nr_seq_motivo_w, 0, ds_historico_w, wheb_usuario_pck.get_nm_usuario);
		end if;
	end;
end if;

commit;

end gerar_motivo_negacao_vaga;
/