create or replace procedure DESVINCULAR_TITULO_PAGAR_NC	(nr_seq_nota_credito_p	number,
					nr_titulo_p		number,
					ie_todos_p		varchar2,
					nm_usuario_p		varchar2) is

nr_titulo_w	number(10);

cursor c01 is
select	a.nr_titulo
from	titulo_pagar a
where	a.nr_seq_nota_credito	= nr_seq_nota_credito_p
and	a.ie_situacao		<> 'C'
and	('S' = ie_todos_p or a.nr_titulo = nr_titulo_p);

BEGIN

open c01;
loop
fetch c01 into
	nr_titulo_w;
exit when c01%notfound;

	update	titulo_pagar
	set	nr_seq_nota_credito	= null,
		ds_observacao_titulo	= WHEB_MENSAGEM_PCK.get_texto(457571, 'nr_seq_nota_credito='||nr_seq_nota_credito_p)
	where	nr_titulo		= nr_titulo_w;

	cancelar_titulo_pagar(nr_titulo_w, nm_usuario_p, sysdate);

end loop;
close c01;

ATUALIZAR_SALDO_NOTA_CREDITO(nr_seq_nota_credito_p, nm_usuario_p);

commit;

end DESVINCULAR_TITULO_PAGAR_NC;
/