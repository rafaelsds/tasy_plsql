create or replace procedure generate_history_regulatory(
		nr_seq_ordem_serv_p		man_ordem_servico.nr_sequencia%type,
		nm_usuario_p			man_ordem_servico.nm_usuario%type,
		ie_hist_sec_assessment_p	varchar2,
		ie_hist_saf_assessment_p	varchar2,
		ie_hist_complaint_p		varchar2,
		ie_send_email_p			varchar2,
		ie_dmb_analysis_p	varchar2 default 'N') is

	ie_potencial_privacy_w		man_ordem_servico.ie_potencial_privacy%type;--
	ds_potencial_privacy_w		man_ordem_servico.ds_potencial_privacy%type;
	ie_potencial_security_w		man_ordem_servico.ie_potencial_security%type;--
	ds_potencial_security_w		man_ordem_servico.ds_potencial_security%type;
	ie_potential_harmed_w		man_ordem_servico.ie_potential_harmed%type;
	ds_potential_harmed_w		man_ordem_servico.ds_potential_harmed%type;
	ie_potencial_safety_w		man_ordem_servico.ie_potencial_safety%type;--
	ds_potencial_safety_w		man_ordem_servico.ds_potencial_safety%type;
	ie_malfunction_w		man_ordem_servico.ie_malfunction%type;
	ds_malfunction_w		man_ordem_servico.ds_malfunction%type;
	ie_severity_harm_w		man_ordem_servico.ie_severity_harm%type;
	ie_probability_harm_w		man_ordem_servico.ie_probability_harm%type;
	nr_versao_cliente_w		man_ordem_servico.nr_versao_cliente_abertura%type;
	nr_customer_requirement_w	man_ordem_servico.nr_customer_requirement%type;
	ie_complaint_w			man_ordem_servico.ie_complaint%type;
	ie_classificacao_w		man_ordem_servico.ie_classificacao%type;
	nr_seq_reg_tipo_risco_w		man_ordem_servico.nr_seq_reg_tipo_risco%type;

	os_duplicadas_w			varchar2(1000);
	ds_potential_duplic_w		varchar2(2000);
	ds_mensagem_segu_w		varchar2(4000);
	ds_mensagem_priv_w		varchar2(4000);

	new_line_w			varchar2(2)	:= chr(13) || chr(10);
	nr_seq_idioma_padrao_w 		number 		:= 5;
	ds_email_destino_w 		varchar2(255)	:= 'security.emr@philips.com';

	function get_text_answer_assessment(
			ie_resposta_pp 		varchar2,
			ds_justificativa_pp 	varchar2) return varchar2 is
	begin
		return 	case ie_resposta_pp
			when 'S' then obter_desc_expressao_idioma(327113, 'Yes', 5) || '. ' || ds_justificativa_pp
			when 'N' then obter_desc_expressao_idioma(327114, 'No', 5) || '.'
			else obter_desc_expressao_idioma(344145, 'N/A', 5) || '.'
			end;
	end get_text_answer_assessment;
	
	function get_answer_malfunction(
		ie_resposta_pp 		varchar2,
		ds_justificativa_pp 	varchar2) return varchar2 is
	begin
		return 	case ie_resposta_pp
			when 'S' then obter_desc_expressao_idioma(327113, 'Yes', 5) || '. ' || ds_justificativa_pp
			when 'N' then obter_desc_expressao_idioma(327114, 'No', 5) || '. ' || ds_justificativa_pp
			else obter_desc_expressao_idioma(344145, 'N/A', 5) || '.'
			end;
	end get_answer_malfunction;

	procedure insert_history_assesssment(
			nr_seq_tipo_pp 		number,
			ds_relat_tecnico_pp 	varchar2) is
		ds_tipo_w		man_tipo_hist.ds_tipo%type;
		ds_mensagem_w		varchar2(4000);
		ie_atualiza_os_w	varchar2(1 char);
	begin
		select	ds_tipo
		into	ds_tipo_w
		from	man_tipo_hist
		where	nr_sequencia = nr_seq_tipo_pp;

		if 	(ds_tipo_w is not null) then
			ds_mensagem_w	:= ds_tipo_w || CHR(10) || CHR(13) || CHR(10) || CHR(13);
		end if;
		ds_mensagem_w := ds_mensagem_w || ds_relat_tecnico_pp;

		man_gravar_historico_ordem(
				nr_seq_ordem_serv_p 	=> nr_seq_ordem_serv_p,
				dt_liberacao_p 		=> sysdate,
				ds_relat_tecnico_p 	=> ds_mensagem_w,
				ie_origem_p 		=> 'I',
				nr_seq_tipo_p 		=> nr_seq_tipo_pp,
				nm_usuario_p 		=> nm_usuario_p);

		man_update_os_stage(
				nr_seq_tipo_hist_p 	=> nr_seq_tipo_pp,
				nr_seq_ordem_p 		=> nr_seq_ordem_serv_p,
				ie_atualizar_os_p 	=> ie_atualiza_os_w);
	end insert_history_assesssment;

	function get_text_security_assessment(
			ie_potencial_privacy_p		man_ordem_servico.ie_potencial_privacy%type,
			ds_potencial_privacy_p		man_ordem_servico.ds_potencial_privacy%type,
			ie_potencial_security_p		man_ordem_servico.ie_potencial_security%type,
			ds_potencial_security_p		man_ordem_servico.ds_potencial_security%type) return varchar2 is
	begin
		return 	obter_desc_expressao_idioma(949608, 'Does the customer allege any privacy-related issue?', 5) || new_line_w
			|| get_text_answer_assessment(ie_potencial_privacy_p, ds_potencial_privacy_p) || new_line_w || new_line_w
			|| obter_desc_expressao_idioma(949610, 'Does the customer allege any product security-related issue?', 5) || new_line_w
			|| get_text_answer_assessment(ie_potencial_security_p, ds_potencial_security_p) || new_line_w || new_line_w;
	end get_text_security_assessment;

	function get_text_no_complaint return varchar2 is
	begin
		return obter_desc_expressao_idioma(1060012, 'The feedback received by the customer or by Philips VV team in this service order has been assessed. The conclusion is that it is not related to a medical device feature since no issue related to patient harm, privacy or security was reported in case of customer feedback or it is related to a defect not present in a product version already available to external clients, in case of anomalies found in VV process. Therefore, the feedback does not meet the definition of a complaint, according to the requirements in Manage Complaints Procedure (DOC ID: EMR-P-3312115105), and it is not required to be opened in  Trackwise as a Complaint Record.', 5) || new_line_w || new_line_w;
	end get_text_no_complaint;

	function get_text_safety_assessment(
			ie_potential_harmed_p	man_ordem_servico.ie_potential_harmed%type,
			ds_potential_harmed_p	man_ordem_servico.ds_potential_harmed%type,
			ie_potencial_safety_p	man_ordem_servico.ie_potencial_safety%type,
			ds_potencial_safety_p	man_ordem_servico.ds_potencial_safety%type,
			ie_malfunction_p 	man_ordem_servico.ie_malfunction%type,
			ds_malfunction_p 	man_ordem_servico.ds_malfunction%type,
			ie_severity_harm_p	man_ordem_servico.ie_severity_harm%type,
			ie_probability_harm_p	man_ordem_servico.ie_probability_harm%type,
			nr_seq_reg_tipo_risco_p	man_ordem_servico.nr_seq_reg_tipo_risco%type) return varchar2 is
	begin
		return 	obter_desc_expressao_idioma(951149, 'Does the customer allege any patient was harmed?', 5) || new_line_w
			|| get_text_answer_assessment(ie_potential_harmed_p, ds_potential_harmed_p) || new_line_w || new_line_w
			|| obter_desc_expressao_idioma(949613, 'Does the customer allege any potential impact to patient safety?', 5) || new_line_w
			|| get_text_answer_assessment(ie_potencial_safety_p, ds_potencial_safety_p) || new_line_w || new_line_w
			|| obter_desc_expressao_idioma(959041, 'Did a malfunction occurred?', 5) || new_line_w
			|| get_answer_malfunction(ie_malfunction_p, ds_malfunction_p) || new_line_w || new_line_w;
	end get_text_safety_assessment;

	function get_text_duplicity_assessment(os_duplicadas_p varchar2) return varchar2 is
	begin
		return 	new_line_w || new_line_w
			|| obter_desc_expressao_idioma(953349, 'Potential Complaint duplicity', 5) || new_line_w
			|| os_duplicadas_p;
	end get_text_duplicity_assessment;

	function get_text_dmb_analysis(
			ie_potencial_safety_p		man_ordem_servico.ie_potencial_safety%type,
			ie_potencial_security_p		man_ordem_servico.ie_potencial_security%type,
			ie_potencial_privacy_p		man_ordem_servico.ie_potencial_privacy%type,
			ie_classificacao_p		man_ordem_servico.ie_classificacao%type) return varchar2 is
	begin
	return obter_desc_expressao_idioma(9511491, 'DMB classifies this anomaly as:', 5) || new_line_w || new_line_w || new_line_w
	|| obter_desc_expressao_idioma(9511491, 'Classification: ', 5)
	|| nvl(obter_valor_dominio_idioma(1149, ie_classificacao_p, 5), obter_desc_expressao_idioma(344145, 'N/A', 5)) || new_line_w || new_line_w
	|| obter_desc_expressao_idioma(9511491, 'Potential risk to patient safety?', 5) || ' '
	|| get_text_answer_assessment(ie_potencial_safety_p, '') || new_line_w || new_line_w
	|| obter_desc_expressao_idioma(9511491, 'Potential risk to product security?', 5) || ' '
	|| get_text_answer_assessment(ie_potencial_security_p, '') || new_line_w || new_line_w
	|| obter_desc_expressao_idioma(9511491, 'Potential risk to data privacy?', 5) || ' '
	|| get_text_answer_assessment(ie_potencial_privacy_p, '');
	end get_text_dmb_analysis;

begin
if 	(	nr_seq_ordem_serv_p is not null
	and	(	ie_hist_sec_assessment_p 	= 'S'
		or 	ie_hist_saf_assessment_p 	= 'S'
		or 	ie_hist_complaint_p 		= 'S'
		or 	ie_send_email_p 		= 'S'
		or  ie_dmb_analysis_p = 'S'
		)
	) then

	select	ie_potencial_privacy,
		ds_potencial_privacy,
		ie_potencial_security,
		ds_potencial_security,
		ie_potential_harmed,
		ds_potential_harmed,
		ie_potencial_safety,
		ds_potencial_safety,
		ie_malfunction,
		ds_malfunction,
		ie_severity_harm,
		ie_probability_harm,
		nr_versao_cliente_abertura,
		nr_customer_requirement,
		nvl(ie_complaint, 'N'),
		ie_classificacao,
		nr_seq_reg_tipo_risco
	into	ie_potencial_privacy_w,
		ds_potencial_privacy_w,
		ie_potencial_security_w,
		ds_potencial_security_w,
		ie_potential_harmed_w,
		ds_potential_harmed_w,
		ie_potencial_safety_w,
		ds_potencial_safety_w,
		ie_malfunction_w,
		ds_malfunction_w,
		ie_severity_harm_w,
		ie_probability_harm_w,
		nr_versao_cliente_w,
		nr_customer_requirement_w,
		ie_complaint_w,
		ie_classificacao_w,
		nr_seq_reg_tipo_risco_w
	from	man_ordem_servico
	where	nr_sequencia = nr_seq_ordem_serv_p;

	if 	(ie_hist_sec_assessment_p = 'S') then
		ds_mensagem_segu_w := get_text_security_assessment(ie_potencial_privacy_w, ds_potencial_privacy_w, ie_potencial_security_w, ds_potencial_security_w);
		insert_history_assesssment(233, ds_mensagem_segu_w); -- CH: Potential Security or Privacy Event
	end if;

	if 	(ie_hist_saf_assessment_p = 'S') then
		ds_mensagem_priv_w := get_text_safety_assessment(
				ie_potential_harmed_p 	=> ie_potential_harmed_w,
				ds_potential_harmed_p 	=> ds_potential_harmed_w,
				ie_potencial_safety_p 	=> ie_potencial_safety_w,
				ds_potencial_safety_p 	=> ds_potencial_safety_w,
				ie_malfunction_p 	=> ie_malfunction_w,
				ds_malfunction_p 	=> ds_malfunction_w,
				ie_severity_harm_p 	=> ie_severity_harm_w,
				ie_probability_harm_p 	=> ie_probability_harm_w,
				nr_seq_reg_tipo_risco_p	=> nr_seq_reg_tipo_risco_w);

		insert_history_assesssment(236, ds_mensagem_priv_w); -- CH: Potential Patient Harmed Event
	end if;

	if 	(ie_hist_complaint_p = 'S') then
		os_duplicadas_w		:= man_obter_os_complaint_duplic(nr_seq_ordem_serv_p, nr_versao_cliente_w, nr_customer_requirement_w);
		ds_mensagem_segu_w	:= get_text_security_assessment(ie_potencial_privacy_w, ds_potencial_privacy_w, ie_potencial_security_w, ds_potencial_security_w);
		ds_mensagem_priv_w	:= get_text_safety_assessment(
				ie_potential_harmed_p 	=> ie_potential_harmed_w,
				ds_potential_harmed_p 	=> ds_potential_harmed_w,
				ie_potencial_safety_p 	=> ie_potencial_safety_w,
				ds_potencial_safety_p 	=> ds_potencial_safety_w,
				ie_malfunction_p 	=> ie_malfunction_w,
				ds_malfunction_p 	=> ds_malfunction_w,
				ie_severity_harm_p 	=> ie_severity_harm_w,
				ie_probability_harm_p 	=> ie_probability_harm_w,
				nr_seq_reg_tipo_risco_p	=> nr_seq_reg_tipo_risco_w);

		if (os_duplicadas_w is not null) then
			ds_potential_duplic_w	:= get_text_duplicity_assessment(os_duplicadas_w);
		end if;

		if (ie_complaint_w = 'S') then
			insert_history_assesssment(215, ds_mensagem_segu_w || ds_mensagem_priv_w || ds_potential_duplic_w); -- CH: Complaint identified
		else
			insert_history_assesssment(234, get_text_no_complaint || ds_mensagem_segu_w || ds_mensagem_priv_w); -- CH: Not complaint identified
		end if;
	end if;

	if 	(ie_send_email_p = 'S') then
		ds_mensagem_segu_w	:= get_text_security_assessment(ie_potencial_privacy_w, ds_potencial_privacy_w, ie_potencial_security_w, ds_potencial_security_w);

		/* Enviar o e-mail para o solicitante*/
		enviar_email(	ds_assunto_p 		=> 'SO' || nr_seq_ordem_serv_p || ' - Potential Security or Privacy Event',
				ds_mensagem_p 		=> ds_mensagem_segu_w,
				ds_email_origem_p 	=> 'support.informatics@philips.com',
				ds_email_destino_p 	=> ds_email_destino_w,
				nm_usuario_p 		=> 'Tasy',
				ie_prioridade_p 	=> '' );
		/* Grava o envio do e-mail na pasta Envios*/
		man_atualizar_envio_ordem(
				nr_seq_ordem_p 		=> nr_seq_ordem_serv_p,
				ds_destino_p 		=> ds_email_destino_w,
				ds_observacao_p 	=> substr(ds_mensagem_segu_w, 1, 255),
				ie_tipo_envio_p 	=> 'E',
				nm_usuario_p 		=> 'Tasy');
	end if;

	if 	(ie_dmb_analysis_p = 'S') then
		insert_history_assesssment(245, get_text_dmb_analysis(ie_potencial_safety_w, ie_potencial_security_w, ie_potencial_privacy_w, ie_classificacao_w)); -- DMB Analysis
	end if;

	commit;
end if;
end generate_history_regulatory;
/