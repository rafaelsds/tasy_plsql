create or replace
procedure man_log_equip_hist(
	nr_seq_equip_p		number,
	ds_historico_p		varchar2,
	nm_usuario_p		Varchar2) is 

begin

insert into man_equipamento_hist(         
         nr_sequencia,                    
         nr_seq_equipamento,              
         dt_atualizacao,                  
         nm_usuario,                      
         dt_historico,                    
         ds_historico,                    
         dt_atualizacao_nrec,             
         nm_usuario_nrec,
	 ie_origem)                 
values(	man_equipamento_hist_seq.nextval, 
         nr_seq_equip_p,                   
         sysdate,                         
         nm_usuario_p,                     
         sysdate,                         
         ds_historico_p,                   
         sysdate,                         
         nm_usuario_p,
	 'S');

commit;

end man_log_equip_hist;
/