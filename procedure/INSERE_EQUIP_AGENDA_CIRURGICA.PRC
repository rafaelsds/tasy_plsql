create or replace
procedure insere_equip_agenda_cirurgica(	nr_seq_agenda_p		in number,
						cd_equipamento_p	in number,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number)
						is

cd_setor_exclusivo_w   number(5);	
cd_setor_atendimento_w   number(5);					

begin

select 		NVL(MAX(a.cd_setor_exclusivo),0)
into		cd_setor_exclusivo_w
from		agenda a,
		agenda_paciente b
where		a.cd_agenda 	= 	b.cd_agenda
and		b.nr_sequencia	= 	nr_seq_agenda_p;

select 		NVL(MAX(cd_setor_atendimento),0)
into		cd_setor_atendimento_w
from		equipamento
where		cd_equipamento = cd_equipamento_p;


if	(nvl(nr_seq_agenda_p,0) > 0) and
	(nvl(cd_equipamento_p,0) > 0) and
	(nvl(cd_setor_atendimento_w,cd_setor_exclusivo_w) = cd_setor_exclusivo_w)	then
	insert	into agenda_pac_equip	(nr_sequencia,
					nr_seq_agenda,
					dt_atualizacao,
					nm_usuario,
					cd_equipamento,
					dt_confirmacao,
					ie_obrigatorio,
					ie_origem_inf,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
					(select	agenda_pac_equip_seq.nextval,
						nr_seq_agenda_p,
						sysdate,
						nm_usuario_p,
						cd_equipamento_p,
						sysdate,
						'S',
						'I',
						sysdate,
						nm_usuario_p
					from 	dual);
	commit;
end if;

end insere_equip_agenda_cirurgica;
/	
