CREATE OR REPLACE PROCEDURE GRAVAR_ENVIO_RES (ds_xml_p clob, nr_seq_log_p number) is

ds_observacao_w varchar(255);
nr_sequencia_w number(38);

begin

begin
select LOG_INTEGRACAO_EVENTO_seq.nextval
into nr_sequencia_w
from dual;

ds_observacao_w := 'Informacao gerada no campo ds_log ' || chr(13) ||'Script para consulta: '|| chr(13) ||'select a.ds_log, a.* '|| chr(13) ||'from LOG_INTEGRACAO_EVENTO a'|| chr(13) ||'where NR_SEQUENCIA = ' || nr_sequencia_w;

insert into LOG_INTEGRACAO_EVENTO(NM_USUARIO,
                                  IE_ENVIO_RETORNO,
                                  IE_TIPO_EVENTO,
                                  DT_ATUALIZACAO,
                                  NR_SEQ_LOG,
                                  NR_SEQUENCIA,
                                  DS_OBSERVACAO,
                                  DS_LOG)
                          values ('Gerenciador',
                                  'E',
                                  'I',
                                  sysdate,
                                  nr_seq_log_p,
                                  nr_sequencia_w,
                                  ds_observacao_w,
                                  ds_xml_p
                                  );
commit;

EXCEPTION
when others then

ds_observacao_w := null;

end;

end GRAVAR_ENVIO_RES;
/
