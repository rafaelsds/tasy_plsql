create or replace
procedure pls_desfazer_ajuste_taxas_fat(	nr_seq_lote_p	pls_lote_faturamento.nr_sequencia%type,
						tb_sequ_pos_p	pls_util_cta_pck.t_number_table,
						tb_seq_conta_p	pls_util_cta_pck.t_number_table,
						nm_usuario_p	usuario.nm_usuario%type) is 

nr_seq_analise_w	pls_conta_pos_estabelecido.nr_seq_analise%type;
cd_estabelecimento_w	pls_conta.cd_estabelecimento%type;

begin

	if	(tb_sequ_pos_p.count > 0) then
	
		
		for i in tb_sequ_pos_p.first..tb_sequ_pos_p.last loop
			select 	max(nr_seq_analise)
			into	nr_seq_analise_w
			from	pls_conta_pos_estabelecido
			where 	nr_seq_conta	= tb_seq_conta_p(i);
			
			select max(cd_estabelecimento)
			into	cd_estabelecimento_w
			from	pls_conta
			where 	nr_sequencia 	= tb_seq_conta_p(i);
			
			pls_gerar_valor_pos_estab( tb_seq_conta_p(i), nm_usuario_p, 'F' ,null,null,'A');
			
			if	(nr_seq_analise_w is not null) then
				pls_liberar_automatic_pos(	nr_seq_analise_w,cd_estabelecimento_w,nm_usuario_p, null);
			end if;
			update	pls_conta_pos_estabelecido
			set	vl_ajuste = 0
			where	nr_sequencia = tb_seq_conta_p(i);
			
			commit;
		
		end loop;
		
		
	end if;
	
end pls_desfazer_ajuste_taxas_fat;
/
