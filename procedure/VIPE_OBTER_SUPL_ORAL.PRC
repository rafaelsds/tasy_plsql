create or replace
procedure vipe_obter_supl_oral (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p		number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p		date,					
		dt_validade_limite_p		date,					
		ie_exibir_hor_realizados_p	varchar2,
		ie_exibir_hor_suspensos_p	varchar2,
		ie_regra_inclusao_p		varchar2,
		ie_data_lib_prescr_p		varchar2,
		ie_lib_pend_rep_p		varchar2,
		ie_exibir_suspensos_p		varchar2,
		ie_agrupar_acm_sn_p		varchar2,
		ie_exibe_sem_lib_farm_p		varchar2,
		ie_prescr_setor_p		varchar2,
		cd_setor_paciente_p		number) is

nr_seq_wadep_w		number(10,0);
nr_prescricao_w		number(14,0);
nr_seq_material_w	number(6,0);
cd_material_w		number(6,0);
ds_material_w		varchar2(255);
ie_acm_sn_w		varchar2(1);
cd_intervalo_w		varchar2(7);
qt_dose_w		number(15,3);
ds_prescricao_w		varchar2(100);
ie_status_w		varchar2(1);
ie_lib_pend_rep_w	varchar2(1);
cd_unid_med_qtde_w	varchar2(30);
ie_via_administracao_w	varchar2(5);
ds_interv_prescr_w	varchar2(15);
					
cursor c01 is
select	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), null),
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_seq_material, null), null),	
	cd_material,
	ds_material,
	ie_acm_sn,	
	cd_intervalo,
	qt_dose,
	ds_prescricao,
	--decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', decode(ie_suspenso, 'S', ie_suspenso, null), null), null) ie_status,
	decode(ie_suspenso, 'S', ie_suspenso, null) ie_status,
	cd_unidade_medida_dose,
	ie_via_aplicacao,
	ds_intervalo
from	(
	select	a.nr_prescricao,
		c.nr_seq_material,
		c.cd_material,
		y.ds_material,
		obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) ie_acm_sn,		
		x.cd_intervalo,
		x.qt_dose,
		substr(adep_obter_um_dosagem_prescr(a.nr_prescricao,c.nr_seq_material,x.ie_acm,x.ie_se_necessario),1,100) ds_prescricao,
		nvl(x.ie_suspenso,'N') ie_suspenso,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao,
		substr(obter_desc_intervalo_prescr(x.cd_intervalo),1,15) ds_intervalo
	from	material y,
		prescr_material x,
		prescr_mat_hor c,
		prescr_medica a
	where	y.cd_material = x.cd_material
	and	y.cd_material = c.cd_material
	and	x.nr_prescricao = c.nr_prescricao
	and	x.nr_sequencia = c.nr_seq_material	
	and	x.nr_prescricao = a.nr_prescricao
	and	c.nr_prescricao = a.nr_prescricao
	and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	and	((obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_p) = 'S') or
		((ie_exibe_sem_lib_farm_p = 'S') and
		(nvl(a.IE_PRESCR_NUTRICAO, 'N') = 'S')))	
	and	x.ie_agrupador = 12
	and	((ie_exibir_suspensos_p = 'S') or (x.dt_suspensao is null))
	and	(((obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) = 'N') and (c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
		 ((obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) = 'S') and (obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))	
	and	nvl(c.ie_situacao,'A') = 'A'
	and	c.ie_agrupador = 12	
	and	((ie_exibir_hor_realizados_p = 'S') or (c.dt_fim_horario is null))
	and	((ie_exibir_hor_suspensos_p = 'S') or (c.dt_suspensao is null))	
	and	((ie_regra_inclusao_p = 'S') or
		 ((ie_regra_inclusao_p = 'R') and (adep_obter_regra_inclusao(	'SO', 
																		cd_estabelecimento_p, 
																		cd_setor_usuario_p, 
																		cd_perfil_p, 
																		c.cd_material, 
																		null, 
																		null, 
																		null,
																		a.cd_setor_Atendimento,
																		null,
																		null, -- nr_prescricao_p. Passei nulo porque criaram o param na adep_obter_regra_inclusao como default null, e n�o haviam passado nada
																		null) = 'S'))) -- nr_seq_exame_p
	and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = cd_setor_paciente_p)))
	and	((ie_data_lib_prescr_p = 'M') or (Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'))
	group by
		a.nr_prescricao,
		c.nr_seq_material,
		c.cd_material,
		y.ds_material,
		x.ie_acm,
		x.ie_se_necessario,		
		x.cd_intervalo,
		x.qt_dose,
		x.ie_suspenso,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao		
	)
group by
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), null),
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_seq_material, null), null),	
	cd_material,
	ds_material,
	ie_acm_sn,	
	cd_intervalo,
	qt_dose,
	ds_prescricao,
	--decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', decode(ie_suspenso, 'S', ie_suspenso, null), null), null),
	decode(ie_suspenso, 'S', ie_suspenso, null),
	cd_unidade_medida_dose,
	ie_via_aplicacao,
	ds_intervalo;
	
cursor c02 is
select	nr_prescricao,
	nr_seq_material,
	cd_material,
	ds_material,
	ie_acm_sn,	
	cd_intervalo,
	qt_dose,
	ds_prescricao,
	decode(ie_suspenso,'N',null,ie_suspenso),
	ie_lib_pend_rep,
	cd_unidade_medida_dose,
	ie_via_aplicacao,
	ds_intervalo	
from	(
	select	a.nr_prescricao,
		x.nr_sequencia nr_seq_material,
		x.cd_material,
		y.ds_material,
		obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) ie_acm_sn,		
		x.cd_intervalo,
		x.qt_dose,
		substr(adep_obter_um_dosagem_prescr(a.nr_prescricao,x.nr_sequencia,x.ie_acm,x.ie_se_necessario),1,100) ds_prescricao,
		x.ie_suspenso,
		substr(adep_obter_lib_pend_rep_gestao(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1) ie_lib_pend_rep,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao,
		substr(obter_desc_intervalo_prescr(x.cd_intervalo),1,15) ds_intervalo		
	from	material y,
		prescr_material x,
		prescr_medica a
	where	y.cd_material = x.cd_material
	and	x.nr_prescricao = a.nr_prescricao
	and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	--and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_p) = 'S'	
	and	x.ie_agrupador = 12
	and	((ie_exibir_suspensos_p = 'S') or (x.dt_suspensao is null))
	and	obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S'
	and	((ie_regra_inclusao_p = 'S') or
		 ((ie_regra_inclusao_p = 'R') and (adep_obter_regra_inclusao(	'SO', 
																		cd_estabelecimento_p, 
																		cd_setor_usuario_p, 
																		cd_perfil_p, 
																		x.cd_material, 
																		null, 
																		null, 
																		null,
																		a.cd_setor_Atendimento,
																		null,
																		null, -- nr_prescricao_p. Passei nulo porque criaram o param na adep_obter_regra_inclusao como default null, e n�o haviam passado nada
																		null) = 'S'))) -- nr_seq_exame_p
	and	not exists (
			select	1
			from	prescr_mat_hor c
			where	c.cd_material = y.cd_material
			and	c.nr_prescricao = x.nr_prescricao
			and	c.nr_seq_material = x.nr_sequencia
			and	c.ie_agrupador = 12
			and	x.ie_agrupador = 12
			and	c.nr_prescricao = a.nr_prescricao
			and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S')
	and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = cd_setor_paciente_p)))
	group by
		a.nr_prescricao,
		x.nr_sequencia,
		x.cd_material,
		y.ds_material,
		x.ie_acm,
		x.ie_se_necessario,		
		x.cd_intervalo,
		x.qt_dose,
		x.ie_suspenso,
		a.dt_liberacao_medico,
		a.dt_liberacao,
		a.dt_liberacao_farmacia,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao		
	)
group by
	nr_prescricao,
	nr_seq_material,
	cd_material,
	ds_material,
	ie_acm_sn,	
	cd_intervalo,
	qt_dose,
	ds_prescricao,
	decode(ie_suspenso,'N',null,ie_suspenso),
	ie_lib_pend_rep,
	cd_unidade_medida_dose,
	ie_via_aplicacao,
	ds_intervalo;	

begin
open c01;
loop
fetch c01 into	nr_prescricao_w,
		nr_seq_material_w,
		cd_material_w,
		ds_material_w,
		ie_acm_sn_w,		
		cd_intervalo_w,
		qt_dose_w,
		ds_prescricao_w,
		ie_status_w,
		cd_unid_med_qtde_w,
		ie_via_administracao_w,
		ds_interv_prescr_w;
exit when c01%notfound;
	begin
	select	w_vipe_t_seq.nextval
	into	nr_seq_wadep_w
	from	dual;
	
	insert into w_vipe_t (
		nr_sequencia,
		nm_usuario,
		ie_tipo_item,
		nr_prescricao,
		nr_seq_item,		
		cd_item,
		ds_item,
		ie_acm_sn,		
		cd_intervalo,
		qt_item,
		ds_prescricao,
		ie_status_item,
		ie_diferenciado,
		nr_seq_proc_interno,
		nr_agrupamento,
		cd_unid_med_qtde,
		ie_via_aplicacao,
		ds_interv_prescr)
	values (
		nr_seq_wadep_w,
		nm_usuario_p,
		'S',
		nr_prescricao_w,
		nr_seq_material_w,
		cd_material_w,
		ds_material_w,
		ie_acm_sn_w,
		cd_intervalo_w,
		qt_dose_w,
		ds_prescricao_w,
		ie_status_w,
		'N',
		0,
		0,
		cd_unid_med_qtde_w,
		ie_via_administracao_w,
		ds_interv_prescr_w);
	end;
end loop;
close c01;

if	(ie_lib_pend_rep_p = 'S') then
	begin
	open c02;
	loop
	fetch c02 into	nr_prescricao_w,
			nr_seq_material_w,
			cd_material_w,
			ds_material_w,
			ie_acm_sn_w,		
			cd_intervalo_w,
			qt_dose_w,
			ds_prescricao_w,
			ie_status_w,
			ie_lib_pend_rep_w,
			cd_unid_med_qtde_w,
			ie_via_administracao_w,
			ds_interv_prescr_w;
	exit when c02%notfound;
		begin
		select	w_vipe_t_seq.nextval
		into	nr_seq_wadep_w
		from	dual;
		
		insert into w_vipe_t (
			nr_sequencia,
			nm_usuario,
			ie_tipo_item,
			nr_prescricao,
			nr_seq_item,		
			cd_item,
			ds_item,
			ie_acm_sn,		
			cd_intervalo,
			qt_item,
			ds_prescricao,
			ie_status_item,
			ie_diferenciado,
			nr_seq_proc_interno,
			nr_agrupamento,
			ie_pendente_liberacao,
			cd_unid_med_qtde,
			ie_via_aplicacao,
			ds_interv_prescr)
		values (
			nr_seq_wadep_w,
			nm_usuario_p,
			'S',
			nr_prescricao_w,
			nr_seq_material_w,
			cd_material_w,
			ds_material_w,
			ie_acm_sn_w,
			cd_intervalo_w,
			qt_dose_w,
			ds_prescricao_w,
			ie_status_w,
			'N',
			0,
			0,
			ie_lib_pend_rep_w,
			cd_unid_med_qtde_w,
			ie_via_administracao_w,
			ds_interv_prescr_w);
		end;
	end loop;
	close c02;
	end;
end if;
end vipe_obter_supl_oral;
/
