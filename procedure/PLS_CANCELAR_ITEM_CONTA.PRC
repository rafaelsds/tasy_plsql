create or replace
procedure pls_cancelar_item_conta(	ie_tipo_p			Varchar2,
					nr_seq_conta_p			Number,
					nr_seq_item_subs_p		Number,
					nm_usuario_p			Varchar2,
					nr_seq_ocorrencia_p		number,
					nr_seq_motivo_glosa_p		number,
					cd_estabelecimento_p		Number) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
 
nr_seq_conta_proc_w		Number(10);
nr_seq_conta_mat_w		Number(10);
nr_seq_proc_participante_w	Number(10);
ie_analise_cm_nova_w		varchar2(3);
nr_seq_analise_w		number(10);
vl_unitario_apres_w		number(15,2)	:= null;
vl_unitario_calc_w		number(15,2)	:= null;
vl_unitario_w			pls_conta_proc.vl_liberado%type	:= null;
vl_glosa_w			number(15,2)	:= null;
vl_liberado_w			number(15,2)	:= null;
vl_pag_medico_conta_w		number(15,2)	:= null;
vl_saldo_w			number(15,2)	:= null;
vl_calculado_w			number(15,2)	:= null;
vl_apresentado_w		number(15,2)	:= null;
vl_base_w			number(15,2)	:= null;
ie_valor_base_w			varchar2(10);
ie_tipo_liberacao_w		varchar2(10);
qt_procedimento_imp_w		Number(12,4);
qt_material_imp_w		number(12,4);
ie_novo_pos_estab_w		pls_visible_false.ie_novo_pos_estab%type;
ie_status_prov_pagto_w		pls_parametro_contabil.ie_status_prov_pagto%type;
nr_lote_contabilizado_w		pls_conta_medica_resumo.nr_lote_contabil_prov%type;
ds_log_call_w			varchar2(1500);

Cursor C01 is
	select	nr_sequencia
	from	pls_proc_participante
	where	nr_seq_conta_proc = nr_seq_item_subs_p
	and	nvl(ie_status,'U') <> 'C'
	order by 1;
begin

select	nvl(max(ie_novo_pos_estab),'N')
into	ie_novo_pos_estab_w
from	pls_visible_false;

-- movido as verifica��es para dentro do IF pois se n�o foi passada a conta n�o tem necessidade de validar nada
select	nvl(max(ie_status_prov_pagto),'NC')
into	ie_status_prov_pagto_w
from	pls_parametro_contabil
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_status_prov_pagto_w = 'F') then

	select	max(nvl(nr_lote_contabil_prov,0)) nr_lote_contabil
	into	nr_lote_contabilizado_w
	from	pls_conta_medica_resumo
	where	nr_seq_conta = nr_seq_conta_p
	and	nvl(nr_lote_contabil_prov,0) <> 0;
	
	if	(nvl(nr_lote_contabilizado_w, 0) = 0) then

		select	max(nvl(nr_lote_contabil_prov,0)) nr_lote_contabil
		into	nr_lote_contabilizado_w
		from	pls_conta_coparticipacao
		where	nr_seq_conta = nr_seq_conta_p
		and	nvl(nr_lote_contabil_prov,0) <> 0;
	end if;
	
	if	(nvl(nr_lote_contabilizado_w,0) = 0) then
		
		if	(ie_novo_pos_estab_w = 'S') then
			select	nvl(a.nr_lote_contabil_prov,0)
			into	nr_lote_contabilizado_w
			from	pls_pos_estab_dados_contab a,
				pls_conta_pos_proc_contab b,
				pls_conta_medica_resumo c
			where	c.nr_sequencia = b.nr_seq_conta_resumo
			and	b.nr_sequencia = a.nr_seq_pos_proc_contab
			and	c.nr_seq_conta = nr_seq_conta_p;
			
			if	(nr_lote_contabilizado_w = 0) then
				select	nvl(a.nr_lote_contabil_prov,0)
				into	nr_lote_contabilizado_w
				from	pls_pos_estab_dados_contab a,
					pls_conta_pos_mat_contab b,
					pls_conta_medica_resumo c
				where	c.nr_sequencia = b.nr_seq_conta_resumo
				and	b.nr_sequencia = a.nr_seq_pos_mat_contab
				and	c.nr_seq_conta = nr_seq_conta_p;
			end if;
		else
			select	max(nvl(a.nr_lote_contabil_prov,0)) nr_lote_contabil
			into	nr_lote_contabilizado_w
			from	pls_conta_pos_estab_contab a,
				pls_conta_medica_resumo b
			where	b.nr_sequencia = a.nr_seq_conta_resumo
			and	b.nr_seq_conta = a.nr_seq_conta
			and	b.nr_seq_conta = nr_seq_conta_p;
		end if;
	end if;
	
	if	(nvl(nr_lote_contabilizado_w, 0) > 0) then
		/* A conta j� foi contabilizada no lote #@NR_LOTE_CONTABILIZADO#@. */
		wheb_mensagem_pck.exibir_mensagem_abort(324839, 'NR_LOTE_CONTABILIZADO=' || nr_lote_contabilizado_w);
	end if;
end if;

ds_log_call_w := substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
				' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);

if	(ie_tipo_p	= 'P') then
	
	insert into	pls_conta_log	(	nr_sequencia, nm_usuario, dt_atualizacao,
						nm_usuario_nrec, dt_atualizacao_nrec, nm_usuario_alteracao,
						dt_alteracao, nr_seq_conta, nr_seq_conta_proc,
						ds_alteracao)
			values		(	pls_conta_log_seq.nextval, nm_usuario_p, sysdate,
						nm_usuario_p, sysdate, nm_usuario_p,
						sysdate, nr_seq_conta_p, nr_seq_item_subs_p,
						substr('Conta proc: '||nr_seq_item_subs_p||' cancelado - Call: ' || ds_log_call_w,1,1999));
	open C01;
	loop
	fetch C01 into	
		nr_seq_proc_participante_w;
	exit when C01%notfound;
		begin
		
		update	pls_proc_participante
		set	ie_status	= 'C'
		where	nr_sequencia = nr_seq_proc_participante_w;
		
		end;
	end loop;
	close C01;

	select	nvl(max(ie_analise_cm_nova),'N')
	into	ie_analise_cm_nova_w
	from	pls_parametros a
	where	a.cd_estabelecimento = cd_estabelecimento_p;

	select	a.nr_seq_analise
	into	nr_seq_analise_w
	from	pls_conta a
	where	a.nr_sequencia	= nr_seq_conta_p;

	if	(ie_analise_cm_nova_w = 'S') and
		(nr_seq_analise_w is not null) then
		select	a.ie_valor_base,
			a.vl_procedimento_imp,
			a.vl_procedimento,
			a.vl_pag_medico_conta,
			a.vl_unitario_imp,
			a.qt_procedimento_imp
		into	ie_valor_base_w,
			vl_apresentado_w,
			vl_calculado_w,
			vl_pag_medico_conta_w,
			vl_unitario_apres_w,
			qt_procedimento_imp_w
		from	pls_conta_proc a
		where	a.nr_sequencia	= nr_seq_item_subs_p;
		
		ie_tipo_liberacao_w	:= 'A';
		vl_base_w		:= vl_apresentado_w;
		vl_unitario_w		:= vl_unitario_apres_w;

		
		vl_liberado_w	:= 0 * vl_unitario_w;
		
		vl_glosa_w	:= vl_base_w - vl_liberado_w;
		
		if	(vl_calculado_w > vl_liberado_w) then
			vl_saldo_w	:= abs(vl_glosa_w);
		else
			vl_saldo_w	:= 0;
		end if;
		
		if	(vl_glosa_w > 0) and
			(vl_liberado_w = 0) then
			ie_tipo_liberacao_w := 'G';
		end if;

		pls_atualiza_conta_item
					(	nr_seq_item_subs_p,
						'P',
						null,
						vl_unitario_w,
						vl_liberado_w,
						vl_glosa_w,
						vl_saldo_w,
						null /* vl prestador */,	
						0,
						null, /* Observa��o da glosa */
						'A', /* Origem - Analise */
						cd_estabelecimento_p,
						nm_usuario_p,
						'N' /* commit */,
						vl_pag_medico_conta_w,
						'N' /* conta inteira */,
						ie_tipo_liberacao_w,
						'S' /* Conta auditoria */,
						null,
						null,
						null,
						null,
						null,
						null);
	end if;
	
	update	pls_conta_proc
	set	ie_status 	= 'D',
		ie_glosa	= 'S',
		vl_liberado	= 0,
		vl_unitario	= 0,
		qt_procedimento	= 0
	where	nr_sequencia	= nr_seq_item_subs_p;

	select	nvl(max(ie_analise_cm_nova),'N')
	into	ie_analise_cm_nova_w
	from	pls_parametros a
	where	a.cd_estabelecimento = cd_estabelecimento_p;

	select	a.nr_seq_analise
	into	nr_seq_analise_w
	from	pls_conta a
	where	a.nr_sequencia	= nr_seq_conta_p;
	
	pls_delete_conta_medica_resumo(nr_seq_conta_p,nr_seq_item_subs_p,null,nm_usuario_p);

else
							
	insert into	pls_conta_log	(	nr_sequencia, nm_usuario, dt_atualizacao,
						nm_usuario_nrec, dt_atualizacao_nrec, nm_usuario_alteracao,
						dt_alteracao, nr_seq_conta, nr_seq_conta_mat,
						ds_alteracao)
			values		(	pls_conta_log_seq.nextval, nm_usuario_p, sysdate,
						nm_usuario_p, sysdate, nm_usuario_p,
						sysdate, nr_seq_conta_p, nr_seq_item_subs_p,
						'Conta mat: '||nr_seq_item_subs_p||' cancelada ' || ds_log_call_w);
						
	
	select	nvl(max(ie_analise_cm_nova),'N')
	into	ie_analise_cm_nova_w
	from	pls_parametros a
	where	a.cd_estabelecimento = cd_estabelecimento_p;
	
	begin	
	select	a.nr_seq_analise
	into	nr_seq_analise_w
	from	pls_conta a
	where	a.nr_sequencia	= nr_seq_conta_p;
	exception
	when others then
		nr_seq_analise_w	:= null;
	end;
	
	if	(ie_analise_cm_nova_w = 'S') and
		(nr_seq_analise_w is not null) then
		
		select	a.ie_valor_base,
			a.vl_material_imp,
			a.vl_material,
			a.vl_unitario_imp,
			a.qt_material_imp
		into	ie_valor_base_w,
			vl_apresentado_w,
			vl_calculado_w,
			vl_unitario_apres_w,
			qt_material_imp_w
		from	pls_conta_mat a
		where	a.nr_sequencia	= nr_seq_item_subs_p;
		
		ie_tipo_liberacao_w	:= 'A';
		vl_base_w		:= vl_apresentado_w;
		vl_unitario_w		:= vl_unitario_apres_w;

		vl_liberado_w	:= 0 * vl_unitario_w;
		
		vl_glosa_w	:= vl_base_w - vl_liberado_w;
		
		if	(vl_calculado_w > vl_liberado_w) then
			vl_saldo_w	:= abs(vl_glosa_w);
		else
			vl_saldo_w	:= 0;
		end if;
		
		if	(vl_glosa_w > 0) and
			(vl_liberado_w = 0) then
			ie_tipo_liberacao_w := 'G';
		end if;

		pls_atualiza_conta_item
					(	nr_seq_item_subs_p,
						'M',
						null,
						vl_unitario_w,
						vl_liberado_w,
						vl_glosa_w,
						vl_saldo_w,
						null /* vl prestador */,	
						0,
						null, /* Observa��o da glosa */
						'A', /* Origem - Analise */
						cd_estabelecimento_p,
						nm_usuario_p,
						'N' /* commit */,
						vl_pag_medico_conta_w,
						'N' /* conta inteira */,
						ie_tipo_liberacao_w,
						'S' /* Conta auditoria */,
						null,
						null,
						null,
						null,
						null,
						null);
		
		vl_unitario_calc_w := dividir_sem_round(vl_calculado_w,qt_material_imp_w);
		
	end if;
	
	update	pls_conta_mat
	set	ie_status 	= 'D',
		ie_glosa	= 'S',
		vl_liberado	= 0,
		vl_unitario	= 0,
		qt_material	= 0
	where	nr_sequencia	= nr_seq_item_subs_p;
		
	pls_delete_conta_medica_resumo(nr_seq_conta_p,null,nr_seq_item_subs_p,nm_usuario_P);
end if;
if	(nr_seq_conta_p is not null) then
	pls_atualizar_utilizacao_guia(nr_seq_conta_p, cd_estabelecimento_p, nm_usuario_p);
	pls_atualiza_valor_conta(nr_seq_conta_p, nm_usuario_p);
end if;

commit;

end pls_cancelar_item_conta;
/