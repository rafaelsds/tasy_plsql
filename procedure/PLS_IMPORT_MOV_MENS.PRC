create or replace
procedure pls_import_mov_mens
			( 	ds_conteudo_p			varchar2,
				ie_operacao_p			varchar2,
				nm_usuario_p			varchar2,
				nr_seq_lote_p			number) is

begin

if	(ie_operacao_p = 'I') then
	insert	into	w_pls_imp_mov_mens
		(	dt_atualizacao, nm_usuario, ds_conteudo, nr_seq_lote)
		values 
		(	sysdate, nm_usuario_p, ds_conteudo_p, nr_seq_lote_p);
elsif	(ie_operacao_p = 'D') then
	delete from w_pls_imp_mov_mens
	where nm_usuario = nm_usuario_p;
	
	delete from w_pls_imp_mov_mens
	where dt_atualizacao < sysdate-2;
end if;

commit;

end pls_import_mov_mens;
/

