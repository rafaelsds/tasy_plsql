create or replace
procedure pls_analise_inativar_glo_ocor
			(	nr_seq_analise_p		number,
				nr_seq_conta_p			number,
				nr_seq_conta_proc_p		number,
				nr_seq_conta_mat_p		number,
				nr_seq_proc_partic_p		number,
				nm_usuario_p			varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inativar as glosas e ocorr�ncias de um ou v�rios itens da an�lise , para que o sistema
reative na consist�ncia
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_conta_w			varchar2(1)	:= 'N';
nr_seq_conta_glosa_w		number(10);
nr_seq_ocorrencia_benef_w	number(10);
nr_seq_ocorrencia_fat_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_glosa_w			number(10);
qt_ocorrencia_w			pls_integer;

Cursor C01 (nr_seq_analise_pc	pls_conta.nr_seq_analise%type)is
	select	a.nr_sequencia
	from	pls_conta a
	where	a.nr_seq_analise	= nr_seq_analise_pc;

/* Glosas */
Cursor C02 (	nr_seq_conta_pc		pls_conta.nr_sequencia%type) is
	select	a.nr_sequencia
	from	pls_conta_glosa	a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	a.ie_situacao	= 'A' 
	and	nr_seq_ocorrencia_benef is null
	and	a.nr_seq_conta_proc 	is null
	and	a.nr_seq_conta_mat 	is null
	and	a.nr_seq_proc_partic 	is null
	and	ie_conta_w	= 'S'
	and	(ie_lib_manual 	= 'N' 	or ie_lib_manual is null)
	union all
	select	a.nr_sequencia
	from	pls_conta_glosa	a,
		pls_conta_proc	b
	where	a.ie_situacao		= 'A'
	and	a.nr_seq_conta_proc	= b.nr_sequencia
	and	nr_seq_ocorrencia_benef is null
	and	b.nr_seq_conta		= nr_seq_conta_pc
	and	(ie_lib_manual = 'N' 	or ie_lib_manual is null)
	union all
	select	a.nr_sequencia
	from	pls_conta_glosa	a,
		pls_conta_mat	b
	where	a.ie_situacao		= 'A'
	and	a.nr_seq_conta_mat	= b.nr_sequencia
	and	nr_seq_ocorrencia_benef is null
	and	b.nr_seq_conta		= nr_seq_conta_pc
	and	(ie_lib_manual = 'N' 	or ie_lib_manual is null)
	union all
	select	a.nr_sequencia
	from	pls_conta_proc		c,
		pls_proc_participante	b,
		pls_conta_glosa		a
	where	a.ie_situacao		= 'A'
	and	a.nr_seq_proc_partic	= b.nr_sequencia
	and	a.nr_seq_ocorrencia_benef is null
	and	(a.ie_lib_manual 	= 'N' 	or a.ie_lib_manual is null)
	and	c.nr_sequencia		= b.nr_seq_conta_proc
	and	c.nr_seq_conta		= nr_seq_conta_pc;
	
Cursor C03 (	nr_seq_conta_pc	pls_conta.nr_sequencia%type) is
	select	/* +USE_CONCAT */
		a.nr_sequencia,
		a.nr_seq_glosa
	from	pls_ocorrencia_benef	a
	where	((a.ie_situacao		= 'A') or (exists	(select 1
								 from 	pls_analise_glo_ocor_grupo	c
								 where	a.nr_sequencia		= c.nr_seq_ocor_benef
								 and	c.ie_status		= 'P')))
	and	a.nr_seq_conta		= nr_seq_conta_pc
	and	a.nr_seq_conta_proc 	is null
	and	a.nr_seq_conta_mat 	is null
	and	a.nr_seq_proc_partic 	is null
	and	ie_conta_w	= 'S'
	and	(ie_lib_manual 	= 'N' 	or ie_lib_manual is null)
	and	(exists	(select 1
			from	pls_acao_analise	acao,
				pls_acao_analise_ocor	ocor
			 where	ocor.nr_seq_ocorrencia	= a.nr_seq_ocorrencia
			 and	ocor.nr_seq_acao_analise = acao.nr_sequencia
			 and	acao.cd_acao		= '10') or
		not exists	(select 1
			from	pls_acao_analise	acao,
				pls_acao_analise_ocor	ocor
			 where	ocor.nr_seq_acao_analise = acao.nr_sequencia
			 and	acao.cd_acao		= '10'))
	union all
	select	a.nr_sequencia,
		a.nr_seq_glosa
	from	pls_conta_proc		b,
		pls_ocorrencia_benef	a
	where	((a.ie_situacao		= 'A') or (exists	(select 1
								 from 	pls_analise_glo_ocor_grupo	c
								 where	a.nr_sequencia		= c.nr_seq_ocor_benef
								 and	c.ie_status		= 'P')))
	and	a.nr_seq_conta_proc	= b.nr_sequencia
	and	a.nr_seq_conta		= nr_seq_conta_pc
	and	(ie_lib_manual 	= 'N' 	or ie_lib_manual is null)
	and	(exists	(select 1
			from	pls_acao_analise	acao,
				pls_acao_analise_ocor	ocor
			 where	ocor.nr_seq_ocorrencia	= a.nr_seq_ocorrencia
			 and	ocor.nr_seq_acao_analise = acao.nr_sequencia
			 and	acao.cd_acao		= '10') or
		not exists	(select 1
			from	pls_acao_analise	acao,
				pls_acao_analise_ocor	ocor
			 where	ocor.nr_seq_acao_analise = acao.nr_sequencia
			 and	acao.cd_acao		= '10'))
	union all
	select	a.nr_sequencia,
		a.nr_seq_glosa
	from	pls_conta_mat		b,
		pls_ocorrencia_benef	a
	where	((a.ie_situacao		= 'A') or (exists	(select 1
								 from 	pls_analise_glo_ocor_grupo	c
								 where	a.nr_sequencia		= c.nr_seq_ocor_benef
								 and	c.ie_status		= 'P')))
	and	a.nr_seq_conta_mat	= b.nr_sequencia
	and	a.nr_seq_conta		= nr_seq_conta_pc
	and	(ie_lib_manual 	= 'N' 	or ie_lib_manual is null)
	and	(exists	(select 1
			from	pls_acao_analise	acao,
				pls_acao_analise_ocor	ocor
			 where	ocor.nr_seq_ocorrencia	= a.nr_seq_ocorrencia
			 and	ocor.nr_seq_acao_analise = acao.nr_sequencia
			 and	acao.cd_acao		= '10') or
		not exists	(select 1
			from	pls_acao_analise	acao,
				pls_acao_analise_ocor	ocor
			 where	ocor.nr_seq_acao_analise = acao.nr_sequencia
			 and	acao.cd_acao		= '10'))
	union all
	select	a.nr_sequencia,
		a.nr_seq_glosa
	from	pls_proc_participante	b,
		pls_ocorrencia_benef	a
	where	((a.ie_situacao		= 'A') or (exists	(select 1
								 from 	pls_analise_glo_ocor_grupo	c
								 where	a.nr_sequencia		= c.nr_seq_ocor_benef
								 and	c.ie_status		= 'P')))
	and	a.nr_seq_proc_partic	= b.nr_sequencia
	and	a.nr_seq_conta		= nr_seq_conta_pc
	and	(ie_lib_manual 	= 'N' 	or ie_lib_manual is null)
	and	(exists	(select 1
			from	pls_acao_analise	acao,
				pls_acao_analise_ocor	ocor
			 where	ocor.nr_seq_ocorrencia	= a.nr_seq_ocorrencia
			 and	ocor.nr_seq_acao_analise = acao.nr_sequencia
			 and	acao.cd_acao		= '10') or
		not exists	(select 1
			from	pls_acao_analise	acao,
				pls_acao_analise_ocor	ocor
			 where	ocor.nr_seq_acao_analise = acao.nr_sequencia
			 and	acao.cd_acao		= '10'));

Cursor C04 (nr_seq_ocorrencia_benef_pc	pls_ocorrencia_benef.nr_sequencia%type) is
	select	b.nr_sequencia
	from	pls_ocorrencia_benef	b
	where	b.nr_seq_ocor_pag	= nr_seq_ocorrencia_benef_pc
	and	(b.ie_lib_manual 	= 'N' or b.ie_lib_manual is null);

begin
ie_conta_w	:= 'N';

if	(nr_seq_conta_p 	is not null) and
	(nr_seq_conta_proc_p 	is null) and
	(nr_seq_conta_mat_p 	is null) and
	(nr_seq_proc_partic_p 	is null) then
	ie_conta_w	:= 'S';
else
	if	(nr_seq_analise_p 	is not null) and
		(nr_seq_conta_p 	is null) and
		(nr_seq_conta_proc_p 	is null) and
		(nr_seq_conta_mat_p 	is null) and
		(nr_seq_proc_partic_p 	is null) then
		ie_conta_w	:= 'S';
	end if;
end if;

for r_c01_w in C01(nr_seq_analise_p) loop
	begin
	for r_c02_w in C02(	r_c01_w.nr_sequencia) loop
		begin
		select	count(1)
		into	qt_ocorrencia_w
		from	pls_ocorrencia_benef x
		where	x.nr_seq_glosa = r_c02_w.nr_sequencia;
		
		if	(qt_ocorrencia_w = 0) then
			update	pls_conta_glosa
			set	ie_situacao		= 'I',
				ie_forma_inativacao	= decode(ie_forma_inativacao,'U','US','US','US','S')
			where	nr_sequencia		= r_c02_w.nr_sequencia;
			
			update	pls_ocorrencia_benef
			set	ie_situacao		= 'I',
				ie_forma_inativacao	= decode(ie_forma_inativacao,'U','US','US','US','S')
			where	nr_seq_glosa_fat	= r_c02_w.nr_sequencia;
		end if;
		end;
	end loop;
	
	for r_c03_w in C03(	r_c01_w.nr_sequencia) loop
		begin
		nr_seq_glosa_w	:= r_c03_w.nr_seq_glosa;
		
		update	pls_ocorrencia_benef
		set	ie_situacao		= 'I',
			ie_forma_inativacao	= decode(ie_forma_inativacao,'U','US','US','US','S')
		where	nr_sequencia		= r_c03_w.nr_sequencia;
		
		if	(nr_seq_glosa_w is null) then
			select	max(a.nr_sequencia)
			into	nr_seq_glosa_w
			from	pls_conta_glosa a
			where	a.nr_seq_ocorrencia_benef	= r_c03_w.nr_sequencia;

		end if;
		
		if	(nr_seq_glosa_w is not null) then
			update	pls_conta_glosa
			set	ie_situacao		= 'I',
				ie_forma_inativacao	= decode(ie_forma_inativacao,'U','US','US','US','S')
			where	nr_sequencia		= nr_seq_glosa_w;
		end if;
		
		for r_c04_w in C04(r_c03_w.nr_sequencia) loop
			begin
			update	pls_ocorrencia_benef
			set	ie_situacao		= 'I',
				ie_forma_inativacao	= decode(ie_forma_inativacao,'U','US','US','US','S')
			where	nr_sequencia		= r_c04_w.nr_sequencia;
			end;
		end loop;

		end;
	end loop;
	
	end;
end loop;

/* Procedure intermedi�ria, sem commit */

end pls_analise_inativar_glo_ocor;
/