create or replace
procedure enviar_comunic_ageint_mat(cd_estabelecimento_p	Number,
					nr_seq_ageint_item_p	Number,
					cd_evento_p		Number,
					nm_usuario_p		Varchar2,
					ie_itens_nao_enviados_p	Varchar2) is


nm_paciente_w			varchar2(100);
nm_medico_w			varchar2(100);
hr_inicio_w			varchar2(100);
ds_convenio_w			varchar2(100);
ds_categoria_w			varchar2(100);
ds_procedimento_w			varchar2(255);
qt_existe_w			number(10);
nr_seq_regra_w			number(10);
cd_perfil_w			varchar2(10);
nr_seq_classif_w		varchar2(10);
nm_usuarios_adic_w		varchar2(255);
ds_titulo_w			varchar2(80);
ds_comunicacao_w		varchar2(4000);
ie_ci_lida_w			varchar2(1);
nr_seq_comunic_w		number(10);
nr_reserva_w			varchar2(20);
ie_carater_cirurgia_w		varchar2(80);	
ds_estabelecimento_w		varchar2(255);
nr_sequencia_w			number(10);
ie_envio_comunic_w		varchar2(1);
nr_seq_agendamento_w		number(10,0);

/*Cursor C02*/
cd_material_w			number(10);
qt_material_w			number(15,3);
cd_cgc_w			varchar2(14);
ds_materiais_agenda_w		varchar2(2000);

/*Cursor C03*/
ds_material_w			varchar2(255);
qt_quantidade_w			number(15,3);
ds_fornecedor_w			varchar2(255);
ds_materiais_sem_codigo_w		varchar2(2000);

/* Se tiver setor na regra, envia CI para os setores */
ds_setor_adicional_w                    	varchar2(2000) := '';
/* Campos da regra Usu�rio da Regra */
cd_setor_regra_usuario_w		number(5);

qt_pac_desc_mat_w		number(10,0);

cursor c01 is
	select	b.nr_sequencia,
		b.cd_perfil,
		b.nm_usuarios_adic
	from	regra_envio_comunic_compra a,
		regra_envio_comunic_evento b
	where	a.nr_sequencia = b.nr_seq_regra
	and	a.cd_funcao = 869
	and	b.cd_evento = cd_evento_p
	and	b.ie_situacao = 'A'
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,0,'X',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S';

cursor c02 is
	select	cd_material,
		qt_material,
		cd_cgc
	from 	agenda_pac_opme
	where	nr_seq_item = nr_seq_ageint_item_p;

cursor c03 is
	select	ds_material,
		qt_quantidade,
		ds_fornecedor
	from 	agenda_pac_desc_material
	where	nr_seq_item = nr_seq_ageint_item_p;

cursor c04 is
	select	ds_material,
		qt_quantidade,
		ds_fornecedor,
		nr_sequencia,
		decode(ie_envio_comunic,null,'N','S') ie_envio_comunic
	from 	agenda_pac_desc_material
	where	nr_seq_item = nr_seq_ageint_item_p;

Cursor c05 is
	select	nvl(a.cd_setor_atendimento,0) cd_setor_atendimento
	from	regra_envio_comunic_usu a
	where	a.nr_seq_evento = nr_seq_regra_w;

begin

select	count(*)
into	qt_existe_w
from	regra_envio_comunic_compra a,
	regra_envio_comunic_evento b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_funcao = 869
and	b.cd_evento = cd_evento_p
and	b.ie_situacao = 'A'
and	a.cd_estabelecimento = cd_estabelecimento_p
and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,0,'X',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S';

select	count(*)
into	qt_pac_desc_mat_w
from 	agenda_pac_desc_material
where	nr_seq_item = nr_seq_ageint_item_p;

if	(qt_existe_w > 0) and
	(qt_pac_desc_mat_w > 0) then
	select	substr(Ageint_Obter_Reserva(null, nr_seq_agenda_cons),1,20),
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,100) nm_paciente,
		substr(obter_nome_medico(a.cd_medico_solicitante,'NC'),1,100) nm_medico,
		substr(to_char(nvl(Obter_Horario_item_Ageint(b.nr_seq_agenda_cons, null, b.nr_sequencia),qt_obter_horario_agendado(b.nr_sequencia)),'dd/mm/yyyy hh24:mi'),1,20),
		substr(obter_nome_convenio(a.cd_convenio),1,100) ds_convenio,
		substr(obter_categoria_convenio(a.cd_convenio, a.cd_categoria),1,100) ds_categoria,
		substr(obter_nome_estabelecimento(a.cd_estabelecimento),1,255),
		a.nr_sequencia
	into	nr_reserva_w,
		nm_paciente_w,
		nm_medico_w,
		hr_inicio_w,
		ds_convenio_w,
		ds_categoria_w,
		ds_estabelecimento_w,
		nr_seq_agendamento_w
	from	agenda_integrada a,
		agenda_integrada_item b
	where	a.nr_sequencia = b.nr_seq_agenda_int
	and	b.nr_sequencia = nr_seq_ageint_item_p;
	
	open C02;
	loop
	fetch C02 into	
		cd_material_w,
		qt_material_w,
		cd_cgc_w;
	exit when C02%notfound;
		begin					 
		ds_materiais_agenda_w := ds_materiais_agenda_w || cd_material_w || ' - ' || substr(obter_desc_material(cd_material_w),1,255)
					 ||' ' || wheb_mensagem_pck.get_texto(794783) ||' ' || qt_material_w || ' ' || wheb_mensagem_pck.get_texto(794784) || ' ' || substr(obter_nome_pf_pj(null,cd_cgc_w),1,80) || chr(13) || chr(10);					 
		end;
	end loop;
	close C02;

	if	(nvl(ie_itens_nao_enviados_p,'N') = 'S') then
		open C04;
		loop
		fetch C04 into	
			ds_material_w,
			qt_quantidade_w,
			ds_fornecedor_w,
			nr_sequencia_w,
			ie_envio_comunic_w;
		exit when C04%notfound;
			begin
			ds_materiais_sem_codigo_w := ds_materiais_sem_codigo_w || substr(ds_material_w,1,255) || ' ' || wheb_mensagem_pck.get_texto(794783) || ' ' || qt_quantidade_w || ' ' || wheb_mensagem_pck.get_texto(794784) || ' ' || substr(ds_fornecedor_w,1,100) || ' ' || wheb_mensagem_pck.get_texto(794785) || ': ' || substr(ie_envio_comunic_w,1,10) || chr(13) || chr(10);
			end;
		end loop;
		close C04;
	else
		open C03;
		loop
		fetch C03 into	
			ds_material_w,
			qt_quantidade_w,
			ds_fornecedor_w;
		exit when C03%notfound;
			begin
			ds_materiais_sem_codigo_w := ds_materiais_sem_codigo_w || substr(ds_material_w,1,255) ||' ' || wheb_mensagem_pck.get_texto(794783) ||' ' || qt_quantidade_w || ' ' || wheb_mensagem_pck.get_texto(794784) || ' ' || substr(ds_fornecedor_w,1,100) || chr(13) || chr(10);
			end;
		end loop;
		close C03;
	end if;

	open C01;
	loop
	fetch C01 into	
		nr_seq_regra_w,
		cd_perfil_w,
		nm_usuarios_adic_w;
	exit when C01%notfound;
		begin
		
		open C05;
		loop
		fetch C05 into	
			cd_setor_regra_usuario_w;
		exit when C05%notfound;
			begin
			if	(cd_setor_regra_usuario_w <> 0) and
				(obter_se_contido_char(cd_setor_regra_usuario_w, ds_setor_adicional_w) = 'N') then
				ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_regra_usuario_w || ',',1,2000);
			end if;
			end;
		end loop;
		close C05;

		if	(cd_perfil_w is not null) then
			cd_perfil_w := cd_perfil_w ||',';
		end if;
		
		if	(nm_usuarios_adic_w is not null) or (cd_perfil_w is not null) then
		
			select	
				max(b.ds_titulo) ds_titulo,
				max(b.ds_comunicacao) ds_comunicacao
			into
				ds_titulo_w,
				ds_comunicacao_w
			from
				regra_envio_comunic_compra a,
				regra_envio_comunic_evento b
			where
				a.nr_sequencia = b.nr_seq_regra and
				b.nr_sequencia = nr_seq_regra_w;
		
			
			
			ds_titulo_w := 				replace_macro(ds_titulo_w, '@agendamento', nr_seq_agendamento_w);
			ds_titulo_w := 				replace_macro(ds_titulo_w, '@paciente', nm_paciente_w);
			ds_titulo_w := 				replace_macro(ds_titulo_w, '@medico', nm_medico_w);
			ds_titulo_w := 				replace_macro(ds_titulo_w, '@hr_inicio', hr_inicio_w);
			ds_titulo_w := 				replace_macro(ds_titulo_w, '@convenio', ds_convenio_w);
			ds_titulo_w := 				replace_macro(ds_titulo_w, '@categoria', ds_categoria_w);
			ds_titulo_w := 				replace_macro(ds_titulo_w, '@estabelecimento', ds_estabelecimento_w);
			ds_titulo_w := 				replace_macro(ds_titulo_w, '@reserva', nr_reserva_w);
			ds_titulo_w := substr(		replace_macro(ds_titulo_w, '@materiais_Sem_Codigo', ds_materiais_sem_codigo_w), 1, 80);
			
			
			ds_comunicacao_w := 		replace_macro(ds_comunicacao_w, '@agendamento', nr_seq_agendamento_w);
			ds_comunicacao_w := 		replace_macro(ds_comunicacao_w, '@paciente', nm_paciente_w);
			ds_comunicacao_w := 		replace_macro(ds_comunicacao_w, '@medico', nm_medico_w);
			ds_comunicacao_w := 		replace_macro(ds_comunicacao_w, '@hr_inicio', hr_inicio_w);
			ds_comunicacao_w := 		replace_macro(ds_comunicacao_w, '@convenio', ds_convenio_w);
			ds_comunicacao_w := 		replace_macro(ds_comunicacao_w, '@categoria', ds_categoria_w);
			ds_comunicacao_w := 		replace_macro(ds_comunicacao_w, '@estabelecimento', ds_estabelecimento_w);
			ds_comunicacao_w := 		replace_macro(ds_comunicacao_w, '@reserva', nr_reserva_w);
			ds_comunicacao_w := substr(	replace_macro(ds_comunicacao_w, '@materiais_Sem_Codigo', ds_materiais_sem_codigo_w), 1, 4000);
			

			select	nvl(ie_ci_lida,'N')
			into	ie_ci_lida_w
			from 	regra_envio_comunic_evento
			where 	nr_sequencia = nr_seq_regra_w;

			select	obter_classif_comunic('F')
			into	nr_seq_classif_w
			from	dual;
			
			select	comunic_interna_seq.nextval
			into	nr_seq_comunic_w
			from	dual;
			
			insert	into comunic_interna(
				nr_sequencia,
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ie_gerencial,
				nr_seq_classif,
				dt_liberacao,
				ds_perfil_adicional,
				ds_setor_adicional)
			values(	nr_seq_comunic_w,
				sysdate,
				ds_titulo_w,
				ds_comunicacao_w,
				nm_usuario_p,
				sysdate,
				'N',
				nm_usuarios_adic_w,
				'N',
				nr_seq_classif_w,
				sysdate,
				cd_perfil_w,
				ds_setor_adicional_w);

			/*Para que a comunica��o seja gerada como lida ao pr�prio usu�rio */
			if	(ie_ci_lida_w = 'S') then
				insert into comunic_interna_lida(nr_sequencia,nm_usuario,dt_atualizacao)values(nr_seq_comunic_w,nm_usuario_p,sysdate);
			end if;

		end if;
		end;
	end loop;
	close C01;
end if;

end enviar_comunic_ageint_mat;
/
