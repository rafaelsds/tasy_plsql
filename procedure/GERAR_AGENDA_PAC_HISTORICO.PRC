create or replace
procedure GERAR_AGENDA_PAC_HISTORICO(	 nr_seq_origem_p		number,
					 nr_seq_destino_p		number,
					 cd_setor_atendimento_p		number,
					 cd_pessoa_fisica_p		varchar,
					 nm_usuario_p			varchar,
					 ie_tipo_p			varchar,
					 nr_seq_motivo_p		number  default null,
					 ds_motivo_p			varchar default null) as
nr_sequencia_w		number(10);
ds_historico_w		varchar2(4000);
ds_observacao_origem_w	varchar2(4000);
ds_procedimento_w	varchar2(255);
hr_inicio_w		date;
hr_inicio_ww		date;
nr_seq_agenda_w		number(10);
ds_motivo_tranf_w	varchar2(255):= null;
ds_motivo_w		varchar2(255);
ds_agenda_w		varchar2(50);
ds_agenda_ww		varchar2(50);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(293478, null, wheb_usuario_pck.get_nr_seq_idioma);--Motivo
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(344235, null, wheb_usuario_pck.get_nr_seq_idioma);--Obs.:
expressao3_w	varchar2(255) := obter_desc_expressao_idioma(774033, null, wheb_usuario_pck.get_nr_seq_idioma);--Agenda Duplicada. Procedimento
expressao4_w	varchar2(255) := obter_desc_expressao_idioma(619522, null, wheb_usuario_pck.get_nr_seq_idioma);--Dia:
expressao5_w	varchar2(255) := obter_desc_expressao_idioma(774040, null, wheb_usuario_pck.get_nr_seq_idioma);--Agenda Transferida. Procedimento
expressao6_w	varchar2(255) := obter_desc_expressao_idioma(618483, null, wheb_usuario_pck.get_nr_seq_idioma);--as
expressao7_w	varchar2(255) := obter_desc_expressao_idioma(727816, null, wheb_usuario_pck.get_nr_seq_idioma);--para

begin

select	ds_observacao,
	substr(obter_descricao_procedimento(cd_procedimento, ie_origem_proced),1,200),
	hr_inicio,
	substr(obter_nome_agenda(cd_agenda),1,50)
into	ds_observacao_origem_w,
	ds_procedimento_w,
	hr_inicio_w,
	ds_agenda_w
from	agenda_paciente
where	nr_sequencia = nr_seq_origem_p;

select	hr_inicio,
	substr(obter_nome_agenda(cd_agenda),1,50)
into	hr_inicio_ww,
	ds_agenda_ww
from	agenda_paciente
where	nr_sequencia = nr_seq_destino_p;


if	(nr_seq_motivo_p is not null) then
	select	max(ds_motivo)
	into	ds_motivo_w
	from    agenda_motivo a
	where   nr_sequencia = nr_seq_motivo_p;
	ds_motivo_tranf_w :=  substr(' ' || expressao1_w || ' = ' || ds_motivo_w ||' - '|| ds_motivo_p,1,255);
end if;


if (ie_tipo_p = 'D') then
	ds_historico_w := substr(expressao2_w || ' ' || ds_observacao_origem_w || ' - ' || expressao3_w || ' : ' || ds_procedimento_w || ' '||expressao4_w||' ' || 
	to_char(hr_inicio_w, 'dd/mm/yyyy hh24:mi:ss'),1,4000);
	nr_seq_agenda_w := nr_seq_destino_p;
else
	ds_historico_w := 	substr(	expressao2_w || ' ' || ds_observacao_origem_w || ' - '||expressao5_w||' : ' || ds_procedimento_w || 
					' '||expressao4_w||' ' || ds_agenda_w || ' '||expressao6_w||' ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(hr_inicio_w,'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.gettimezone) || 
					' '||expressao7_w||' ' || ds_agenda_ww || ' '||expressao6_w||' ' || PKG_DATE_FORMATERS_TZ.TO_VARCHAR(hr_inicio_ww,'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.gettimezone) || ds_motivo_tranf_w,1,4000);

--	ds_historico_w := 'Obs.: ' || ds_observacao_origem_w || ' - Agenda Transferida. Procedimento : ' || ds_procedimento_w || ' Dia: ' || 
--	to_char(hr_inicio_w, 'dd/mm/yyyy hh24:mi:ss') || ds_motivo_tranf_w;
	nr_seq_agenda_w := nr_seq_origem_p;
end if;

select	agenda_pac_hist_seq.nextval
into	nr_sequencia_w
from 	dual;


insert into agenda_pac_hist
	(nr_sequencia,
	nr_seq_agenda,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_setor_atend_usuario,
	dt_historico,
	cd_pessoa_fisica,
	nr_seq_tipo,
	ds_historico)
values	(nr_sequencia_w,
	nr_seq_agenda_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_setor_atendimento_p,
	sysdate,
	cd_pessoa_fisica_p,
	null,
	ds_historico_w);
commit;							

end gerar_agenda_pac_historico;
/
