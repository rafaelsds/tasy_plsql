create or replace
procedure pls_gerar_aihs_deferidas (	nr_seq_competencia_p	Number,
					cd_estabelecimento_p	Number,
					nm_usuario_p		Varchar2) is

ie_cursor_informacao_w	 Varchar2(1);

/* Gets all the AIHs that are identified, have the GRU number and are not already on the charged tab */
cursor c_aih is
	select 	a.nr_seq_conta,
		a.vl_deferido,
		a.vl_ressarcir
	from	pls_processo_contas_comp a,
		pls_processo_conta b
	where	a.nr_seq_conta = b.nr_sequencia	
	and	not exists (	select	1
				from 	pls_processo_contas_comp c
				where	c.nr_seq_conta 		= a.nr_seq_conta
				and	c.nr_seq_competencia 	= nr_seq_competencia_p
				and	(c.ie_tipo_movimentacao in ('D')))
	and	a.ie_tipo_movimentacao = 'I'
	and	b.ie_status_conta = 'D'
	and	a.nr_seq_competencia = nr_seq_competencia_p;

vet_aih c_aih%rowtype;
begin

ie_cursor_informacao_w := 'N';

open c_aih;
loop
fetch c_aih into 
	vet_aih;
exit when c_aih%notfound;
	begin
	ie_cursor_informacao_w := 'S';
	insert into pls_processo_contas_comp (
		nr_sequencia,
		nm_usuario,
		nm_usuario_nrec,
		dt_atualizacao,
		dt_atualizacao_nrec,
		cd_estabelecimento,
		vl_ajuste,
		vl_deferido,
		vl_provisao,
		vl_ressarcir,
		ie_tipo_movimentacao,
		cd_conta_cred_ajuste, cd_classif_cred_ajuste,
		cd_conta_deb_ajuste, cd_classif_deb_ajuste,
		nr_seq_esquema_ajuste, cd_historico_ajuste,
		cd_conta_cred_prov, cd_classif_cred_prov,
		cd_conta_deb_prov, cd_classif_deb_prov,
		nr_seq_esquema_prov, cd_historico_prov,
		nr_seq_competencia, 
		nr_seq_conta) 
	values (pls_processo_contas_comp_seq.nextval,
		nm_usuario_p,
		nm_usuario_p,
		sysdate,
		sysdate,
		cd_estabelecimento_p,
		0,
		vet_aih.vl_deferido,
		0,
		vet_aih.vl_ressarcir,
		'D',
		null, null,
		null, null,
		null, null,
		null, null,
		null, null,
		null, null,
		nr_seq_competencia_p,
		vet_aih.nr_seq_conta);
	end;
end loop;
close c_aih;

if	(nvl(ie_cursor_informacao_w, 'N') = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(1099432);
end if;


commit;

end pls_gerar_aihs_deferidas;
/
