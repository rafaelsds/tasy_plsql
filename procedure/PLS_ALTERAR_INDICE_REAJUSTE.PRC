create or replace
procedure pls_alterar_indice_reajuste
		(	nr_seq_contrato_p		number,
			nr_seq_indice_reajuste_p	number,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number) is

nr_seq_indice_reajuste_w	number(10);
ds_indice_reajuste_antigo_w	varchar2(30);
ds_indice_reajuste_novo_w	varchar2(30);

begin

select	nr_seq_indice_reajuste
into	nr_seq_indice_reajuste_w
from	pls_contrato
where	nr_sequencia = nr_seq_contrato_p;

select  max(ds_moeda)
into	ds_indice_reajuste_antigo_w
from    moeda
where 	cd_moeda = nr_seq_indice_reajuste_w;

select  max(ds_moeda)
into	ds_indice_reajuste_novo_w
from    moeda
where 	cd_moeda = nr_seq_indice_reajuste_p;

update	pls_contrato
set	nr_seq_indice_reajuste = decode(nr_seq_indice_reajuste_p,0,null,nr_seq_indice_reajuste_p),
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_seq_contrato_p;

insert into pls_contrato_historico
	      (	nr_sequencia, cd_estabelecimento, nr_seq_contrato, dt_historico, ie_tipo_historico,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ds_historico,
		ds_observacao)
	values(	pls_contrato_historico_seq.NextVal, cd_estabelecimento_p, nr_seq_contrato_p, sysdate, '51',
		sysdate, nm_usuario_p, sysdate, nm_usuario_p, 'De: ' || ds_indice_reajuste_antigo_w || '. Para: ' || ds_indice_reajuste_novo_w,
		'pls_alterar_indice_reajuste');

commit;

end pls_alterar_indice_reajuste;
/