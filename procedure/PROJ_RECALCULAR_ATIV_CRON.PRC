create or replace 
procedure proj_recalcular_ativ_cron(	nr_seq_cronograma_p	number,
					nm_usuario_p		varchar2) is

cursor c01 is
	select	nr_sequencia 
	from	proj_cron_etapa 
	where	nr_seq_cronograma = nr_seq_cronograma_p
	and	nvl(ie_situacao,'A') = 'A';
	/* OS 147594
	select	distinct
		nr_seq_etapa_cron
	from	proj_rat_ativ a,
		proj_rat b
	where	a.nr_seq_etapa_cron in (
				select	nr_sequencia 
				from	proj_cron_etapa 
				where	nr_seq_cronograma = nr_seq_cronograma_p)
	and	a.nr_seq_rat = b.nr_sequencia;*/

nr_seq_etapa_w number(10,0);

begin
 
open c01;
loop
	fetch c01 into
		nr_seq_etapa_w;
	exit when c01%notfound;
 
	begin
 
 	Atualizar_horas_cronograma(nr_seq_etapa_w);
	Atualizar_Horas_Etapa_Cron(nr_seq_etapa_w);
	proj_atualizar_pr_realizada(nr_seq_cronograma_p, nr_seq_etapa_w, 0);
	Gerar_Horario_Inicial_Etapa(nr_seq_etapa_w, 'I');
 
 	end;
end loop;
close c01;

Proj_Recalcula_Cronograma(nr_seq_cronograma_p);
atualizar_total_horas_cron(nr_seq_cronograma_p);

commit;

end proj_recalcular_ativ_cron;
/