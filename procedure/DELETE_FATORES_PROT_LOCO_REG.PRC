CREATE OR REPLACE procedure 	delete_fatores_prot_loco_reg( nr_seq_loco_reg_p in number ) is

begin
	if (nr_seq_loco_reg_p > 0) then
		delete from fator_prog_loco_reg 
		where nr_seq_can_loco_reg = nr_seq_loco_reg_p;
	end if;

end delete_fatores_prot_loco_reg;
/