create or replace
procedure ems_gerar_movto_est_atras(cd_estabelecimento_p	number, --Informar '0' para considerar todos
					dt_movto_p		date,
					nm_usuario_p		Varchar2) is  
--Criada para buscar movimentos do dia anterior que n�o foram integrados (gerados na w_movimento_estoque)
dt_movimento_estoque_w	date;
cd_material_w		number(6);
cd_material_estoque_w	number(6);
cd_centro_custo_w	number(8);
cd_centro_custo_ww	varchar2(20);
vl_movimento_w		number(15,4);
nr_movimento_estoque_w	number(10);
nr_seq_regra_w		number(10);
ie_entrada_saida_w	varchar2(1);
cd_operacao_estoque_w	number(3);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
cd_local_estoque_w	number(4);
cd_local_destino_w	number(4);
cd_sistema_ant_w	varchar2(20);
qt_horas_w		number(10);
cd_unidade_medida_w	varchar2(30);
cd_unidade_medida_ww	varchar2(30);
qt_movimento_w		number(13,4);
cd_conta_contabil_w	varchar2(30);
cd_conta_contabil_ww	varchar2(30);
cd_acao_w		varchar2(1);
ie_tipo_requisicao_w	varchar2(3);
cd_setor_atendimento_w	number(10);
cd_fornecedor_w		varchar2(14);
ds_observacao_w		varchar2(255);
dt_mesano_referencia_w	date;
ie_origem_documento_w	varchar2(3);
ds_lote_fornec_w	varchar2(80);
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nr_seq_lote_fornec_w	movimento_estoque.nr_seq_lote_fornec%type;
dt_validade_w		material_lote_fornec.dt_validade%type;
ie_integra_w		varchar2(1);

cursor c01 is
	select	dt_movimento_estoque,
		cd_material,
		cd_material_estoque,
		cd_centro_custo,
		nvl(vl_movimento,0),
		nr_movimento_estoque,
		cd_operacao_estoque,
		cd_local_estoque,
		cd_unidade_medida_estoque,
		qt_movimento,
		cd_acao,
		cd_local_estoque_destino,
		cd_setor_atendimento,
		cd_fornecedor,
		ds_observacao,
		dt_mesano_referencia,
		ie_origem_documento,
		substr(obter_dados_lote_fornec(nr_seq_lote_fornec,'D'),1,80),
		nr_seq_lote_fornec,
		cd_estabelecimento
	from	movimento_estoque a
	where	(nvl(cd_estabelecimento_p,0) = 0 or cd_estabelecimento_p = cd_estabelecimento)
	and 	trunc(dt_movimento_estoque, 'dd') = trunc(dt_movto_p)
	and	(ie_origem_documento <> '11')
	and	ie_origem_documento <> '12'
	and	not exists(	select	1
				from	w_movimento_estoque
				where	nr_movimento_estoque = a.nr_movimento_estoque);

begin

open c01;
loop
fetch c01 into	
	dt_movimento_estoque_w,
	cd_material_w,
	cd_material_estoque_w,
	cd_centro_custo_w,
	vl_movimento_w,
	nr_movimento_estoque_w,
	cd_operacao_estoque_w,
	cd_local_estoque_w,
	cd_unidade_medida_w,
	qt_movimento_w,
	cd_acao_w,
	cd_local_destino_w,
	cd_setor_atendimento_w,
	cd_fornecedor_w,
	ds_observacao_w,
	dt_mesano_referencia_w,
	ie_origem_documento_w,
	ds_lote_fornec_w,
	nr_seq_lote_fornec_w,
	cd_estabelecimento_w;
exit when c01%notfound;
	begin	
	
	ie_integra_w := obter_se_integr_movto_estoque(nr_movimento_estoque_w);
	
	if	(ie_integra_w = 'S') then
		if	(nvl(nr_seq_lote_fornec_w,0) > 0) then
			select	dt_validade
			into	dt_validade_w
			from	material_lote_fornec
			where	nr_sequencia = nr_seq_lote_fornec_w;
		end if;

		insert into w_movimento_estoque(
			cd_acao,
			cd_centro_custo,
			cd_estabelecimento,
			cd_fornecedor,
			cd_local_estoque,
			cd_material,
			cd_material_estoque,
			cd_operacao_estoque,
			ds_observacao,
			dt_mesano_referencia,
			dt_movimento_estoque,
			ie_envio_recebe,
			ie_origem_documento,
			ie_pendente,
			ie_tipo_integracao,
			nm_usuario,
			nr_sequencia,
			qt_movimento,
			ds_razao_social,
			nr_movimento_estoque,
			dt_validade)
		values(	cd_acao_w,
			cd_centro_custo_w,
			cd_estabelecimento_w,
			cd_fornecedor_w,
			cd_local_estoque_w,
			cd_material_w,
			cd_material_estoque_w,
			cd_operacao_estoque_w,
			ds_observacao_w,
			dt_mesano_referencia_w,
			dt_movimento_estoque_w,
			'E',
			ie_origem_documento_w,
			'S',
			'EMS',
			nm_usuario_p,
			w_movimento_estoque_seq.nextval,
			qt_movimento_w,
			ds_lote_fornec_w,
			nr_movimento_estoque_w,
			dt_validade_w);
	end if;
	end;
end loop;
close c01;

commit;

end ems_gerar_movto_est_atras;
/