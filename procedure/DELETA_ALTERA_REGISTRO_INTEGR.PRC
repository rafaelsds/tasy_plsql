create or replace
procedure deleta_altera_registro_integr(
			nr_seq_integracao_p	number,
			ie_tipo_p			number,
			nm_usuario_p		varchar2) is 

begin
	if( ie_tipo_p = 1 ) then
		delete from registro_integr_com_xml 
		where 	nr_sequencia = nr_seq_integracao_p;
	elsif( ie_tipo_p = 2 ) then
		update	registro_integr_com_xml 
		set 	ie_status 	= 'NP',
			nm_usuario 	= nm_usuario_p,
			dt_atualizacao 	= sysdate
		where 	nr_sequencia 	= nr_seq_integracao_p;
	end if;
commit;

end deleta_altera_registro_integr;
/