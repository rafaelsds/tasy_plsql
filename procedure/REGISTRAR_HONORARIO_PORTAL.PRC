CREATE OR REPLACE
PROCEDURE Registrar_Honorario_Portal
			(nr_atendimento_p    		number,
			 nr_seq_proc_interno_p		number,
			 dt_item_p			date,
			 cd_medico_executor_p		varchar2,
			 ie_erro_p		out 	varchar2,
			 nr_seq_proced_p	out	number,
			 nm_usuario_p      		varchar2) is
			
-- Projeto: Registrar Hono�rios M�dicos da Retaguarda Atrav�s do Portal do M�dico.	
-- OS 599236
/* Requisito: Par�metros de Entrada
	 N�mero do Atendimento:
	 C�digo do procedimento Interno:
	 Data do item na conta: DT_PROCEDIMENTO e  DT_CONTA
	 M�dico: CD_MEDICO_EXECUTOR*/

cd_convenio_w				number(5,0);
cd_categoria_w				varchar2(10);
nr_doc_convenio_w			varchar2(30);
ie_tipo_guia_w				varchar2(30);
cd_senha_w				varchar2(30);
nr_seq_atepacu_atual_w			Number(10,0);
dt_entrada_unidade_w			date;
cd_setor_atendimento_w			number(10,0);
cd_procedimento_w			number(15,0);
ie_origem_proced_w			number(10,0);
cd_estabelecimento_w			Number(4);
cd_plano_w				varchar2(10);
ie_classificacao_w			varchar2(1);
ie_tipo_atendimento_w			Number(3);
ie_medico_executor_w			varchar2(2);
cd_cgc_prestador_w			varchar2(14);
cd_medico_executor_w			varchar2(10);	
cd_profissional_w			varchar2(10);	
cd_pessoa_fisica_w			varchar2(10);	
nr_seq_classificacao_w			number(10);
ie_clinica_w				number(05,0);
ie_funcao_medico_w			varchar2(10)	:= 0;
cd_espec_medica_w			number(05,0)	:= 0;
ie_via_acesso_w				varchar2(1);	
nr_seq_w				number(10,0);
sql_err_w				varchar2(2000);
dt_fim_conta_w				date;
ie_fim_conta_w				varchar2(1);
ie_erro_w				varchar2(1):= 'N';
	 
begin

ie_erro_w		:= 'N';
cd_procedimento_w	:= 0;	
ie_origem_proced_w	:= 0;
--Inicio do bloco de Tratamento de excess�o
begin	
-- Obter o Conv�nio de Execu��o 
Obter_Convenio_Execucao(nr_atendimento_p, dt_item_p, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);

--Buscar plano do atendimento
select 	Obter_Plano_Atendimento(nr_atendimento_p,'C')
into	cd_plano_w
from 	dual;

--Buscar dados do atendimento
select 	max(cd_estabelecimento),
	max(ie_tipo_atendimento),
	max(nr_seq_classificacao),
	max(ie_clinica),
	max(dt_fim_conta),
	max(ie_fim_conta)
into	cd_estabelecimento_w,
	ie_tipo_atendimento_w,
	nr_seq_classificacao_w,
	ie_clinica_w,
	dt_fim_conta_w,
	ie_fim_conta_w
from 	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

/* Buscar a Unidade do paciente conforme regra de neg�cio abaixo
Regra de neg�cio:   Para Lan�amento de visitas e procedimentos, iremos filtrar o setor que o paciente estava na data do lan�amento.
	          Para consulta do PA, vamos pesquisar o setor de atendimento do PA para lan�ar, pois temos mais de um.
*/
if	(ie_tipo_atendimento_w = 3) then
	select	nvl(max(a.nr_seq_interno),0)
	into	nr_seq_atepacu_atual_w
	from 	atend_paciente_unidade  a,
		setor_atendimento 	c
	where	a.nr_atendimento 		= nr_atendimento_p
	and 	a.cd_setor_atendimento		= c.cd_setor_atendimento
	and 	c.cd_classif_setor = 1
	and 	nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999)	=  (select max(nvl(b.dt_saida_unidade, b.dt_entrada_unidade + 9999))
									    from   atend_paciente_unidade 	b,
										   setor_atendimento 		d
									    where  b.nr_atendimento  = nr_atendimento_p
									    and    b.cd_setor_atendimento = d.cd_setor_atendimento
									    and    d.cd_classif_setor = 1);
else
	select	nvl(max(a.nr_seq_interno),0)
	into	nr_seq_atepacu_atual_w
	from 	atend_paciente_unidade  a,
		setor_atendimento 	c
	where	a.nr_atendimento 		= nr_atendimento_p
	and 	a.cd_setor_atendimento		= c.cd_setor_atendimento
	and 	c.cd_classif_setor in (3,4,8)
	and 	nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999)	=  (select max(nvl(b.dt_saida_unidade, b.dt_entrada_unidade + 9999))
									    from   atend_paciente_unidade 	b,
										   setor_atendimento 		d
									    where  b.nr_atendimento  = nr_atendimento_p
									    and    b.cd_setor_atendimento = d.cd_setor_atendimento
									    and    d.cd_classif_setor in (3,4,8));
end if;

--Buscar  o Setor e a data de entrada unidade da Unidade Atual do paciente
select 	max(cd_setor_atendimento),
	max(dt_entrada_unidade)
into	cd_setor_atendimento_w,
	dt_entrada_unidade_w
from 	atend_paciente_unidade
where	nr_seq_interno = nr_seq_atepacu_atual_w;


/*
Excess�o (Gerar LOG) Atendimento FECHADO
*/
if	(ie_fim_conta_w = 'F') or (dt_fim_conta_w is not null) then
	--Atendimento Fechado. N�o permite mais movimento!
	Wheb_mensagem_pck.exibir_mensagem_abort(245282);
end if;

/* Requisito:    Consultar o DE-PARA do cadastro do procedimento interno, podemos ter uma �nico procedimento interno, vinculado a mais de um procedimento AMB;  */
if	(nr_seq_proc_interno_p is not null) then

	Obter_Proc_Tab_Interno_Conv(	nr_seq_proc_interno_p, cd_estabelecimento_w, cd_convenio_w, cd_categoria_w,
					cd_plano_w, cd_setor_atendimento_w, cd_procedimento_w, 	ie_origem_proced_w,
					cd_setor_atendimento_w, dt_item_p,  null, null, null, null, null, null, null, null);
end if;

/*
Excess�o (Gerar LOG) caso n�o tenha o procedimento
*/
if	(cd_procedimento_w = 0) then
	--N�o foi encontrado o procedimento para o proc. interno informado!
	Wheb_mensagem_pck.exibir_mensagem_abort(245283);
end if;

-- Busca a Classifica��o
select	max(ie_classificacao)
into	ie_classificacao_w
from	procedimento
where	cd_procedimento		= cd_procedimento_w
and	ie_origem_proced	= ie_origem_proced_w;
		
-- Obter M�dico Executor 
consiste_medico_executor(cd_estabelecimento_w, cd_convenio_w, cd_setor_atendimento_w, cd_procedimento_w, ie_origem_proced_w, 
			ie_tipo_atendimento_w, null, nr_seq_proc_interno_p, ie_medico_executor_w,cd_cgc_prestador_w, cd_medico_executor_w, 
			cd_profissional_w, cd_pessoa_fisica_w, dt_item_p, nr_seq_classificacao_w, 'N', null, null); 

-- Se a regra n�o for "M�dico Fixo" ent�o utiliza o m�dico do par�metro			
if	(nvl(ie_medico_executor_w,'Z') <> 'F') then
	cd_medico_executor_w:= cd_medico_executor_p;
end if;

/*
- Requisito:  Preencher especialidade do m�dico: Est� informa��o est� no cadastro do m�dico;
- Requisito: Preencher a fun��o do m�dico: Informa��o est� no Shift F11/aplica��o principal/faturamento/regra defini��o especialidade procedimento;
*/	
obter_proced_espec_medica(cd_estabelecimento_w, cd_convenio_w, cd_procedimento_w, ie_origem_proced_w, null, null, null,
			ie_clinica_w, cd_setor_atendimento_w, cd_espec_medica_w, ie_funcao_medico_w,
			cd_medico_executor_w, nr_seq_proc_interno_p, ie_tipo_atendimento_w);

--Caso n�o tiver regra para defini��o da especialidade, o sistema l� do cadastro do m�dico
if	(nvl(cd_espec_medica_w,0) = 0) then
	select 	obter_especialidade_medico(cd_medico_executor_w,'C')
	into	cd_espec_medica_w
	from 	dual;
end if;

-- Caso n�o tenha prestador definido na regra o sistema l� o prestador como o estabelecimento do atend.
if	(cd_cgc_prestador_w is null) then
	select	max(a.cd_cgc)
	into	cd_cgc_prestador_w 
	from 	estabelecimento a,
		atendimento_paciente b
	where	a.cd_estabelecimento 	= b.cd_estabelecimento
	and	b.nr_atendimento 	= nr_atendimento_p;
end if;	

-- Buscar a via de acesso (Por default se n�o tiver regra � "�nica ou Principal")
select 	substr(obter_regra_via_acesso(cd_procedimento_w, ie_origem_proced_w, cd_estabelecimento_w, cd_convenio_w),1,2) 
into	ie_via_acesso_w
from 	dual;

--Buscando a pr�xima sequ�ncia da tabela PROCEDIMENTO_PACIENTE
select 	procedimento_paciente_seq.NextVal
into	nr_seq_w
from 	dual;

--Realizando a inser��o na tabela PROCEDIMENTO_PACIENTE				
insert into procedimento_paciente( 
	nr_sequencia,		nr_atendimento,		dt_entrada_unidade,		cd_procedimento,	dt_procedimento,
	cd_convenio,		cd_categoria,		nr_doc_convenio,		ie_tipo_guia,		cd_senha,
	ie_auditoria,		ie_emite_conta,		cd_cgc_prestador,		ie_origem_proced,	nr_seq_exame,
	nr_seq_proc_interno,	qt_procedimento,	cd_setor_atendimento,		nr_seq_atepacu,		nr_seq_cor_exec,
	ie_funcao_medico,	vl_procedimento,	ie_proc_princ_atend,		ie_video,		tx_medico,
	tx_anestesia,		tx_procedimento,	ie_valor_informado,		ie_guia_informada,	cd_situacao_glosa,
	nm_usuario_original,	ds_observacao,		dt_atualizacao,			nm_usuario,		cd_pessoa_fisica,
	cd_medico_executor,	cd_especialidade, 	ie_via_acesso,			dt_conta)
values( 
	nr_seq_w,		nr_atendimento_p,	dt_entrada_unidade_w,		cd_procedimento_w,	dt_item_p,
	cd_convenio_w,		cd_categoria_w,		nr_doc_convenio_w,		ie_tipo_guia_w,		cd_senha_w,
	'N',			null,			cd_cgc_prestador_w,		ie_origem_proced_w,	null,
	nr_seq_proc_interno_p,	1,			cd_setor_atendimento_w,		nr_seq_atepacu_atual_w,	null,
	ie_funcao_medico_w,	100,			'N',				'N',			100,
	100,			100,			'N',				'N',			0,
	--nm_usuario_p,		'Honor�rio via Portal',		sysdate,				nm_usuario_p,		cd_profissional_w,
	nm_usuario_p,		WHEB_MENSAGEM_PCK.get_texto(298844),	sysdate,	nm_usuario_p,		cd_profissional_w,	
	cd_medico_executor_w,	cd_espec_medica_w, 	ie_via_acesso_w,		dt_item_p);

/*
- Requisito: Consultar o valor do procedimento conforme as regras comerciais, inclusive com as regras de ajustes. O mesmo processo realizado quando inclu�mos um item manualmente atrav�s da conta paciente;
- Requisito: Verificar DE-PARA para o TUSS e gravar na conta;
- Requisito: Preencher o campo emiti conta/hon: Conforme cadastro de estrutura da conta;
- Requisito: Preencher o prestador,Doc(guia)/Tipo: Analisar as regras do conv�nio para preenchimento da guia.

*/	
if	(ie_classificacao_w = '1') then 		
	atualiza_preco_procedimento(nr_seq_w, cd_convenio_w, nm_usuario_p);
	gerar_taxa_sala_porte(nr_atendimento_p, dt_entrada_unidade_w, dt_item_p, cd_procedimento_w, nr_seq_w, nm_usuario_p);
end if;

/* Disparar regra de lan�amento Autom�tico */
Gerar_lancamento_automatico(nr_atendimento_p,null,34,nm_usuario_p, nr_seq_w, null,null,null,null,null);

-- Verifica regras de autoriza��o
gerar_autor_regra(nr_atendimento_p, null, nr_seq_w, null, null, nr_seq_proc_interno_p,'CP',nm_usuario_p,null,null,null,null,null,null,'','','');
			
exception
	when others then
	
	ie_erro_w := 'S';
	sql_err_w := substr(SQLERRM, 1, 4000);	

	/*
	Requisito:	Criar uma tabela de log, para gravar os procedimentos que por algum motivo n�o foram inclu�dos na conta.
		Campos: N�mero do atendimento � C�digo do Procedimento � Nome Do m�dico - Data do lan�amento do item - Data do registro
	
	Requisito:	Todas as transa��es deve ter um retorno para o portal confirmando se a grava��o foi efetuada com sucesso. 
	
	 W_LOG_ERRO_PORTAL:
	 
		 Nome                                      	Nulo?    	Tipo
		 ----------------------------------------- -------- 	-------------------------
		 NR_SEQUENCIA                              NOT NULL 	NUMBER(10)
		 NR_ATENDIMENTO                         NOT NULL 	NUMBER(10)
		 DT_ATUALIZACAO                           NOT NULL 	DATE
		 NM_USUARIO                                	NOT NULL 	VARCHAR2(15)
		 DT_ATUALIZACAO_NREC                                	DATE
		 NM_USUARIO_NREC                                    	VARCHAR2(15)
		 CD_MEDICO_EXECUTOR                                 	VARCHAR2(10)
		 DT_ITEM                                            	DATE
		 NR_SEQ_PROC_INTERNO                                	NUMBER(10)
		 DS_ERRO                                            	VARCHAR2(4000)
		 CD_PROCEDIMENTO                        NOT NULL 	NUMBER(15)
		 IE_ORIGEM_PROCED                       NOT NULL 	NUMBER(10)	
	*/
	insert	into W_LOG_ERRO_PORTAL ( 
			NR_SEQUENCIA,
			NR_ATENDIMENTO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			CD_MEDICO_EXECUTOR,
			DT_ITEM,
			NR_SEQ_PROC_INTERNO,
			DS_ERRO,
			CD_PROCEDIMENTO,
			IE_ORIGEM_PROCED)
		values	(W_LOG_ERRO_PORTAL_seq.nextVal,
			nr_atendimento_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_medico_executor_w,
			dt_item_p,
			nr_seq_proc_interno_p,
			sql_err_w,
			nvl(cd_procedimento_w,0),
			nvl(ie_origem_proced_w,0));
	
end; -- Fim do bloco de Tratamento de excess�o

commit;

ie_erro_p	:= nvl(ie_erro_w,'N');

if	(nvl(ie_erro_w,'N') = 'S') then
	nr_seq_proced_p := 0;
else
	nr_seq_proced_p := nr_seq_w;
end if;

END Registrar_Honorario_Portal;
/