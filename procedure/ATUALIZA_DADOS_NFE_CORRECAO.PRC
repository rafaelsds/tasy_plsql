create or replace
procedure atualiza_dados_nfe_correcao(nr_seq_lote_p	number,
				     ds_motivo_p	varchar2,
				     cd_motivo_p	number,
				     dt_recebimento_p	date) is 
				     
nr_seq_nota_w	number(10);				     
				     
begin

update nota_fiscal_lote_nfe set dt_envio   = sysdate,
				DT_RETORNO = dt_recebimento_p,
				DS_MENSAGEM_RETORNO = cd_motivo_p || ' - ' || obter_valor_dominio(3628,cd_motivo_p) || chr(13) || 'Erro: ' ||ds_motivo_p
where	nr_sequencia = nr_seq_lote_p;		
				
commit;

end atualiza_dados_nfe_correcao;
/
