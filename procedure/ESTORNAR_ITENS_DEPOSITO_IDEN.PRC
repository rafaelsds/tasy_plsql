create or replace
procedure estornar_itens_deposito_iden
			(	nr_seq_deposito_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

nr_titulo_w		number(10);
nr_seq_cheque_w		number(10);
nr_seq_baixa_w		number(10);

Cursor C01 is
	select	nr_titulo
	from	deposito_ident_titulo
	where	nr_seq_deposito	= nr_seq_deposito_p;
	
Cursor C02 is
	select	nr_seq_cheque
	from	deposito_ident_cheque
	where	nr_seq_deposito	= nr_seq_deposito_p;

begin
open C01;
loop
fetch C01 into	
	nr_titulo_w;
exit when C01%notfound;
	begin
	null;
	select	max(a.nr_sequencia)
	into	nr_seq_baixa_w
	from	titulo_receber_liq	a
	where	a.nr_titulo		= nr_titulo_w
	and	a.nr_seq_deposito_ident	= nr_seq_deposito_p;
	
	estornar_tit_receber_liq(nr_titulo_w,nr_seq_baixa_w,sysdate,nm_usuario_p,'N',null,null);
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_seq_cheque_w;
exit when C02%notfound;
	begin
	update	cheque_cr
	set	dt_devolucao	= null
	where	nr_seq_cheque	= nr_seq_cheque_w;	
	end;
end loop;
close C02;

update	deposito_identificado
set	nm_usuario		= nm_usuario_p,
	ie_status		= 'AD',
	dt_deposito		= null
where	nr_sequencia		= nr_seq_deposito_p;

commit;

end estornar_itens_deposito_iden;
/