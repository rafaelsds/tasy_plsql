create or replace procedure pie_fault_message ( nr_seq_message_p	in pie_message.nr_sequencia%type,
					cd_status_p	in pie_message_fault.cd_status%type,
                                                		cd_severity_p	in pie_message_fault.cd_severity%type,
                                                		ds_reason_p	in pie_message_fault.ds_reason%type)
is
  nr_sequencia_w 	pie_message_fault.nr_sequencia%type;
begin
	if(( cd_status_p is not null )
	and ( cd_severity_p is not null )
	and ( ds_reason_p is not null )
	and ( nr_seq_message_p is not null)) then

		select pie_message_fault_seq.nextval
		into   nr_sequencia_w
		from   dual;

		insert into pie_message_fault
		(nr_sequencia,
		cd_status,
		nr_seq_message,
		cd_severity,
		ds_reason
		)
		values
		(nr_sequencia_w,
                 	cd_status_p,
                 	nr_seq_message_p,
                 	cd_severity_p,
                 	ds_reason_p
		);
	commit;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(1088701);
	end if; 

end pie_fault_message;
/
