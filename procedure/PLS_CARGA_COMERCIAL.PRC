create or replace
procedure pls_carga_comercial
			(	cd_registro_p		Varchar2,
				nm_pessoa_p		Varchar2,
				ds_endereco_p		Varchar2,
				cd_cep_p		Varchar2,
				ds_municipio_p		Varchar2,
				nr_telefone_p		Varchar2,
				nr_fax_p		Varchar2,
				ds_email_p		Varchar2,
				nm_contato_p		Varchar2,
				nm_usuario_p		Varchar2,
				ds_lista_data_hist_p	Varchar2,
				ds_lista_data_agenda_p	Varchar2,
				ds_lista_obs_p		Varchar2,
				ds_lista_hora_p		Varchar2,
				ds_lista_valor_p	Varchar2,
				ds_lista_contato_p	Varchar2) is 

nr_seq_carga_w			number(10);

ds_lista_data_hist_w		varchar2(4000);
ds_lista_obs_w			varchar2(4000);
ds_lista_data_agenda_w		varchar2(4000);

dt_hist_w			varchar2(20);
ds_historico_w			varchar(2000);
dt_agenda_w			varchar2(20);

begin

ds_lista_data_hist_w	:= ds_lista_data_hist_p;
ds_lista_obs_w		:= ds_lista_obs_p;
ds_lista_data_agenda_w	:= ds_lista_data_agenda_p;

select	w_pls_carga_comercial_seq.NextVal
into	nr_seq_carga_w
from	dual;

insert into w_pls_carga_comercial (	nr_sequencia, cd_registro, nm_pessoa, ds_endereco, cd_cep,
					ds_municipio, nr_telefone, nr_fax, ds_email, dt_atualizacao,
					nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec)
			values	(	nr_seq_carga_w, cd_registro_p, nm_pessoa_p, ds_endereco_p, cd_cep_p,
					ds_municipio_p, nr_telefone_p, nr_fax_p, ds_email_p, sysdate,
					nm_usuario_p, sysdate, nm_usuario_p);

while (ds_lista_data_hist_w is not null) loop 
	begin
	/* Buscar a data do histórico na lista */
	select	substr(ds_lista_data_hist_w,1,instr(ds_lista_data_hist_w,'#@')-1)
	into	dt_hist_w
	from	dual;
	/* Buscar o histórico na lista */
	select	substr(ds_lista_obs_w,1,instr(ds_lista_obs_w,'#@')-1)
	into	ds_historico_w
	from	dual;
	/* Buscar a data da agenda na lista */
	select	substr(ds_lista_data_agenda_w,1,instr(ds_lista_data_agenda_w,'#@')-1)
	into	dt_agenda_w
	from	dual;

	/* Retira a data buscada da lista */
	select	substr(ds_lista_data_hist_w,instr(ds_lista_data_hist_w,'#@') +2,length(ds_lista_data_hist_w))
	into	ds_lista_data_hist_w
	from	dual;
	/* Retira o histórico buscado da lista */
	select	substr(ds_lista_obs_w,instr(ds_lista_obs_w,'#@') +2,length(ds_lista_obs_w))
	into	ds_lista_obs_w
	from	dual;
	/* Retira a data da agenda buscada da lista */
	select	substr(ds_lista_data_agenda_w,instr(ds_lista_data_agenda_w,'#@') +2,length(ds_lista_data_agenda_w))
	into	ds_lista_data_agenda_w
	from	dual;

	if	(nvl(dt_hist_w,'0') <> '0') then
		insert into w_pls_carga_comercial_hist (	nr_sequencia, nr_seq_comercial, dt_atualizacao, nm_usuario,
								dt_atualizacao_nrec, nm_usuario_nrec, dt_historico, ds_historico,
								dt_agenda)
						values	(	w_pls_carga_comercial_hist_seq.NextVal, nr_seq_carga_w, sysdate, nm_usuario_p,
								sysdate, nm_usuario_p, to_date(dt_hist_w), ds_historico_w,
								dt_agenda_w);
	end if;
	
	end;
end loop;

commit;

end pls_carga_comercial;
/