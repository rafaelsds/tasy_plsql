create or replace
procedure liberar_perfil_usuario( cd_perfil_p	number,
			  nm_usuario_lib_p	varchar2,
			  nm_usuario_p	varchar2) is
		
ie_estrategico_w		varchar2(1);
ie_permite_lib_perfil_est_w	varchar2(1);

begin

select	nvl(max(ie_estrategico),'N')
into	ie_estrategico_w
from	perfil
where	cd_perfil	= cd_perfil_p;



if	(ie_estrategico_w = 'S') then
	begin
	ie_permite_lib_perfil_est_w	:= nvl(substr(obter_valor_param_usuario(6001,137,obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_perfil),1,1),'N');
	
	if	(ie_permite_lib_perfil_est_w = 'N') then
		/*Usu�rio sem permiss�o para liberar perfil estrat�gico!*/
		wheb_mensagem_pck.exibir_mensagem_abort(207168);
	end if;
	end;
end if;

if	(cd_perfil_p is not null) and
	(nm_usuario_lib_p is not null) then
	begin
	insert into usuario_perfil(
		nm_usuario,
		cd_perfil,
		dt_atualizacao,
		nm_usuario_atual,
		ds_observacao,
		nr_seq_apres,
		dt_liberacao)
	values ( nm_usuario_lib_p,
		cd_perfil_p,
		sysdate,
		nm_usuario_p,
		null,
		null,
		sysdate);			
	end;
end if;

commit;

end liberar_perfil_usuario;
/
