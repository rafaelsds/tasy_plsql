create or replace
procedure pep_inserir_diagnosticos_sae(
					nr_seq_prescr_p		number,
					list_nr_sequencia_p		varchar2,
					nm_usuario_p			varchar2) is 

ds_lista_w	varchar2(2000);			
ds_lista_aux_w	varchar2(2000);
nr_seq_aux_w	number(10);
k		integer;
				
begin

ds_lista_w := list_nr_sequencia_p;

while ds_lista_w is not null loop 
	begin
	
	select	instr(ds_lista_w, ',')
	into	k
	from	dual;
	
	if	(k > 1) and 
		(substr(ds_lista_w, 1, k -1) is not null) then
		ds_lista_aux_w			:= substr(ds_lista_w, 1, k-1);
		nr_seq_aux_w			:= replace(ds_lista_aux_w, ',','');
		ds_lista_w			:= substr(ds_lista_w, k + 1, 2000);
	elsif	(ds_lista_w is not null) then
		nr_seq_aux_w			:= replace(ds_lista_w,',','');
		ds_lista_w			:= '';
	end if;	
	
	inserir_diagnostico_rotina_SAE(nr_seq_prescr_p,nr_seq_aux_w,nm_usuario_p);	

	end;
end loop;

commit;

end pep_inserir_diagnosticos_sae;
/