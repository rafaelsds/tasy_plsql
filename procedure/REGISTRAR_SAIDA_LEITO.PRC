create or replace
procedure registrar_saida_leito(	nr_sequencia_p		number,
					dt_saida_leito_p	date,
					nm_usuario_p		varchar2) is 

begin

if	(nr_sequencia_p > 0) then
	update	sl_unid_atend
	set	dt_saida_real	=	dt_saida_leito_p
	where	nr_sequencia	=	nr_sequencia_p;
	commit;
end if;

end registrar_saida_leito;
/