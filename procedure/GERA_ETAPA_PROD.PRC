create or replace
procedure gera_etapa_prod(dt_etapa_p		date,
			  cd_pessoa_fisica_p	varchar2,
			  cd_estabelecimento_p	number,
			  nm_usuario_p		Varchar2) is 

nr_sequencia_w	number(10);
nr_seq_cabine_w	number(10);
			
begin

Obter_Param_Usuario(3130,301,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,nr_seq_cabine_w);

if 	(nvl(nr_seq_cabine_w,0) > 0) then 
	select	far_etapa_producao_seq.nextval
	into	nr_sequencia_w
	from 	dual;

	insert into far_etapa_producao(
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_etapa,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_cabine)
	values	(nr_sequencia_w,
		cd_estabelecimento_p,
		sysdate,
		sysdate,
		dt_etapa_p,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_cabine_w);
		
	update 	can_ordem_prod
	set 	nr_seq_etapa_prod = nr_sequencia_w
	where  	trunc(dt_prevista) 	= trunc(dt_etapa_p) 
	and    	cd_pessoa_fisica 	= cd_pessoa_fisica_p
	and    	nvl(ie_conferido,'N') = 'S'
	and 	nr_seq_etapa_prod is null;
else	
	wheb_mensagem_pck.exibir_mensagem_abort(173464);
end if;

commit;
	
end gera_etapa_prod;
/