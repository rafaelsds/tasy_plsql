create or replace
procedure pls_gravar_envio_email_relac
			(	ds_lista_destino_p	varchar2,
				nr_seq_grupo_p		number,
				ds_observacao_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

ds_lista_destino_w		varchar2(2000);
nr_pos_virgula_destino_w	number(10,0);
ds_destino_w			varchar2(2000);
cd_controle_w			number(3) := 0;
nr_seq_envio_w			number(10);
ds_assunto_w			varchar2(255)	:= '';

begin
if	(ds_lista_destino_p is not null) and
	(nr_seq_grupo_p is not null) and
	(cd_estabelecimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	ds_lista_destino_w := ds_lista_destino_p;
	
	while	(ds_lista_destino_w is not null) and
		(cd_controle_w < 99) loop
		begin
		cd_controle_w			:= cd_controle_w + 1;
		
		nr_pos_virgula_destino_w	:= instr(ds_lista_destino_w,',');
		
		if	(nr_pos_virgula_destino_w > 0) then
			begin
			ds_destino_w		:= substr(ds_lista_destino_w, 1, nr_pos_virgula_destino_w-1);
			ds_lista_destino_w	:= substr(ds_lista_destino_w, nr_pos_virgula_destino_w+1, length(ds_lista_destino_w));
			end;
		else
			begin
			ds_destino_w		:= ds_lista_destino_w;
			ds_lista_destino_w	:= null;
			end;
		end if;
		
		if	(ds_destino_w is not null) then
			begin
			pls_gravar_envio_email_grupo (
				nm_usuario_p,
				ds_assunto_w,
				ds_destino_w,
				nr_seq_grupo_p,
				ds_observacao_p,
				cd_estabelecimento_p,
				nr_seq_envio_w);
			end;
		end if;
		end;
	end loop;
	end;
end if;

commit;

end pls_gravar_envio_email_relac;
/