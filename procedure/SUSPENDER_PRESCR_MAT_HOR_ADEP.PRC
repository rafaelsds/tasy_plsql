create or replace
procedure suspender_prescr_mat_hor_adep	(	nr_sequencia_p		number,						
						nr_seq_motivo_susp_p	number,
						ds_motivo_susp_p	varchar2,
						nr_prescricao_p		number,
						cd_estabelecimento_p	number,
						nm_usuario_p		varchar2
						) is
			
nr_prescricao_w		number(14,0);
nr_seq_material_w	number(6,0);
dt_horario_w		date;

ds_erro_w		varchar2(2000);
					
begin
-- Procedure criada para ser utilizada na pasta Gest�o de conflitos no ADEP - Java

ds_erro_w := '';

Consiste_susp_prescr_mat_hor(nr_prescricao_p, nr_sequencia_p, cd_estabelecimento_p, nm_usuario_p, ds_erro_w);

if (ds_erro_w is null) then
	begin
	
	update	prescr_mat_hor
	set	dt_suspensao		= sysdate,
		nm_usuario_susp		= nm_usuario_p,
		nr_seq_motivo_susp	= decode(nr_seq_motivo_susp_p,0,null,nr_seq_motivo_susp_p),
		ds_motivo_susp          = ds_motivo_susp_p
	where	nr_sequencia 		= nr_sequencia_p
	and	dt_fim_horario	is null
	and	dt_suspensao	is null;

	select	nr_prescricao,
		nr_seq_material,
		dt_horario
	into	nr_prescricao_w,
		nr_seq_material_w,
		dt_horario_w
	from	prescr_mat_hor
	where	nr_sequencia = nr_sequencia_p
	and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

	update	prescr_mat_hor
	set	dt_suspensao		= sysdate,
		nm_usuario_susp		= nm_usuario_p,
		nr_seq_motivo_susp	= decode(nr_seq_motivo_susp_p,0,null,nr_seq_motivo_susp_p),
		ds_motivo_susp          = ds_motivo_susp_p
	where	nr_prescricao		= nr_prescricao_w
	and	nr_seq_superior		= nr_seq_material_w
	and	dt_horario		= dt_horario_w
	and	dt_fim_horario		is null
	and	dt_suspensao		is null;

	commit;

	atualiza_ie_horario_susp(nr_prescricao_w, nr_seq_material_w, 'PRESCR_MATERIAL');
	
	end;
else 
	begin
	-- #@DS_ERRO#@
	Wheb_mensagem_pck.exibir_mensagem_abort( 193870 );	
	end;
	
end if;
	
end suspender_prescr_mat_hor_adep;
/