create or replace 
procedure Man_gerar_OS_Planej_Evento(
				cd_setor_atendimento_p	number,
				cd_unidade_basica_p	varchar2,
				cd_unidade_compl_p	varchar2,
				ie_evento_p		varchar2) is


cd_perfil_w			number(5)	;
cd_pessoa_solic_w			varchar2(10);
cd_pessoa_resp_w			varchar2(10);
cd_setor_atendimento_w		number(5);
dt_prevista_w			Date;
dt_emissao_prev_w			Date;
dt_ultima_ordem_w			Date;
ds_arquivo_w			varchar2(255);
ds_comunicado_w			varchar2(4000);
ds_dano_w			varchar2(2000);
ds_dano_breve_w			varchar2(80);
ds_atividade_w			Varchar2(2000);
ds_perfil_adicional_w		varchar2(4000);
ie_anexar_email_w			varchar2(1);
ie_prioridade_w			varchar2(1);
ie_comunicacao_w			varchar2(1);
ie_solicitante_w			varchar2(1);
nm_usuario_destino_w		Varchar2(255);
nm_usuario_exec_w		varchar2(15);
nr_seq_anexo_w			number(10);
nr_seq_ordem_w			number(10);
nr_seq_ordem_ww			number(10);
nr_max_seq_ordem_w		number(10);
nr_seq_ativ_w			Number(10);
nr_seq_planej_w			number(10);
nr_seq_planej_ww			number(10);
nr_seq_tipo_cont_w		number(10);
nr_seq_local_w			number(10);
nr_seq_trab_w			number(10);
nr_seq_equip_w			Number(10);	
qt_dia_freq_w			number(4);
qt_dia_prev_w			number(3);
qt_dia_ordem_w			number(3);
qt_preventiva_w			number(10);
qt_ordem_aberta_w			number(10);
qt_min_prev_w			Number(15);
nr_seq_tipo_equip_w		Number(10); /*Tipo do equipamento*/
nr_seq_tipo_equip_ww		Number(10); /*Tipo do equipamento do planejamento*/
nr_seq_apres_w			number(5);
nr_seq_origem_dano_w		number(10);
nr_seq_causa_dano_w		number(10);
nr_seq_tipo_solucao_w		number(10);
nr_seq_complex_w			number(10);
nr_seq_cs_w			number(10);
ie_equip_regra_excl_w		varchar2(1) := 'N';
qt_reg_w			number(5);
nr_seq_tipo_os_w			number(10);

cursor	C01 is
select	a.nr_seq_localizacao,
	a.nr_seq_equip,
	a.nr_seq_planej,
	Obter_Tipo_Equip_Preventivo(a.nr_seq_equip),
	b.nr_seq_tipo_equip
from	man_planej_prev b,
	unidade_atend_prev a
where	a.nr_seq_planej		= b.nr_sequencia
and	nvl(b.ie_situacao,'A')	= 'A'
and	a.cd_setor_atendimento	= cd_setor_atendimento_p
and	a.cd_unidade_basica	= cd_unidade_basica_p
and	a.cd_unidade_compl	= cd_unidade_compl_p
and	a.ie_evento		= ie_evento_p;

cursor C02 is
select	ds_atividade,
	qt_min_prev,
	nr_seq_apres
from	man_planej_atividade
where	nr_seq_planej_prev	= nr_seq_planej_w;

cursor c03 is
select	ds_arquivo,
	ie_anexar_email
from	man_planej_prev_arq
where	nr_seq_planej_Prev	= nr_seq_planej_w;

begin
/*Matheus OS 55486 25/04/07 */

select	count(*)
into	qt_preventiva_w	
from	unidade_atend_prev
where	cd_setor_atendimento	= cd_setor_atendimento_p
and	cd_unidade_basica		= cd_unidade_basica_p
and	cd_unidade_compl		= cd_unidade_compl_p
and	ie_evento			= ie_evento_p;


if	(qt_preventiva_w > 0) then
	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_local_w,
		nr_seq_equip_w,
		nr_seq_planej_w,
		nr_seq_tipo_equip_w,
		nr_seq_tipo_equip_ww;
	exit when C01%notfound;
		begin
		
		select	count(nr_sequencia)
		into	qt_reg_w
		from	man_planej_prev_equip_excl
		where	nr_seq_equipamento = nr_seq_equip_w
		and	nr_seq_planej_prev = nr_seq_planej_w;
		
		ie_equip_regra_excl_w := 'N';
		if 	(qt_reg_w > 0) then
			ie_equip_regra_excl_w := 'S';
		end if;
		
		
		if	((nr_seq_tipo_equip_w = nr_seq_tipo_equip_ww) and
			(ie_equip_regra_excl_w = 'N')) then
			begin
			select	b.qt_dia_previsto,
				b.qt_dia_gerar_ordem,
				b.ie_prioridade,
				b.cd_pessoa_solicitante,
				b.cd_setor_atendimento,
				b.ds_planejamento,
				b.nr_seq_tipo_contador,
				b.ds_dano,
				b.nm_usuario_exec,
				a.qt_dia,
				b.ie_comunicacao,
				b.ie_solicitante,
				c.nr_seq_planej,
				c.nr_seq_trab,
				nvl(b.cd_perfil,0),
				b.nr_seq_origem_dano,
				b.nr_seq_causa_dano,
				b.nr_seq_tipo_solucao,
				b.nr_seq_complex,
				b.nr_seq_cs,
				b.nr_seq_tipo_os
		        into	qt_dia_prev_w,
				qt_dia_ordem_w,
				ie_prioridade_w,
				cd_pessoa_solic_w,
				cd_setor_atendimento_w,
				ds_dano_breve_w,
				nr_seq_tipo_cont_w,
				ds_dano_w,
				nm_usuario_exec_w,
				qt_dia_freq_w,
				ie_comunicacao_w,
				ie_solicitante_w,
				nr_seq_planej_ww,
				nr_seq_trab_w,
				cd_perfil_w,
				nr_seq_origem_dano_w,
				nr_seq_causa_dano_w,
				nr_seq_tipo_solucao_w,
				nr_seq_complex_w,
				nr_seq_cs_w,
				nr_seq_tipo_os_w
        		from	man_planej_prev b,
				man_equipamento c,
				man_freq_planej a
			where	b.nr_sequencia 	= nr_seq_planej_w
			and	c.nr_sequencia 	= nr_seq_equip_w
			and	c.nr_seq_tipo_equip 	= b.nr_seq_tipo_equip
			and	b.nr_seq_frequencia 	= a.nr_sequencia;

			select	nvl(max(nr_sequencia), 0)
			into	nr_seq_ordem_ww
			from 	man_ordem_servico
			where 	nr_seq_equipamento 	= nr_seq_equip_w
			and 	nvl(nr_seq_Planej,0)	= nr_seq_planej_w
			and 	ie_tipo_ordem 	= 2;

			if	(nr_seq_ordem_ww = 0) then
				dt_emissao_prev_w	:= sysdate - 1000;
			else
				begin
				select  nvl(dt_fim_real, sysdate)
				into 	dt_ultima_ordem_w
				from 	man_ordem_servico
				where 	nr_sequencia 	= nr_seq_ordem_ww;
				dt_emissao_prev_w	:= dt_ultima_ordem_w + qt_dia_freq_w;
				end;
			end if;

			select	nvl(obter_usuario_pessoa(cd_pessoa_solic_w),'X')
			into	nm_usuario_destino_w
			from	dual;

			if	(cd_setor_atendimento_w is not null) then
				select	nvl(max(cd_pessoa_resp), 'X')
				into	cd_pessoa_resp_w
				from	setor_atendimento
				where	cd_setor_atendimento = cd_setor_atendimento_w;
				if	(cd_pessoa_resp_w <> 'X') then
					nm_usuario_destino_w	:= nm_usuario_destino_w || ',' || obter_usuario_pessoa(cd_pessoa_resp_w);
				end if;
			end if;

			if 	(sysdate >= dt_emissao_prev_w) then
       				begin
				select	man_ordem_servico_seq.nextval
				into	nr_seq_ordem_w
				from	dual;

				insert into man_ordem_servico(
					nr_sequencia, 
					nr_seq_equipamento,
					nr_seq_localizacao,
		           		cd_pessoa_solicitante, 
           				dt_ordem_servico, 
					ie_prioridade,
					ie_parado, 
           				ds_dano_breve, 
		           		dt_atualizacao,
           				nm_usuario, 
		           		dt_inicio_desejado, 
           				dt_conclusao_desejada, 
		           		ds_dano, 
           				dt_inicio_previsto, 
	        	   		dt_fim_previsto, 
        	   			dt_inicio_real, 
		           		dt_fim_real, 
           				ie_tipo_ordem,
		           		ie_status_ordem, 
           				nr_grupo_planej,
					nr_grupo_trabalho, 
           				nr_seq_tipo_solucao, 
		           		ds_solucao, 
           				nm_usuario_exec, 
		           		qt_contador, 
           				nr_seq_planej, 
		           		nr_seq_tipo_contador,
					ie_classificacao,
					ie_origem_os,
					nm_usuario_nrec,
					nr_seq_origem_dano,
					nr_seq_causa_dano,					
					nr_seq_complex,
					nr_seq_cs,
					nr_seq_tipo_ordem
					)
		        	values( nr_seq_ordem_w, 
					nr_seq_equip_w,
					nr_seq_local_w,
           				cd_pessoa_solic_w, 
		           		sysdate, 
					ie_prioridade_w, 
		           		'N', 
           				ds_dano_breve_w, 
		           		sysdate, 
           				'Prevent_Event', 
		           		sysdate + qt_dia_ordem_w, 
					sysdate + qt_dia_prev_w + qt_dia_ordem_w, 
		           		ds_dano_w, 	
           				null, 
		           		null, 
           				null, 
		           		null, 
           				2, 
		           		1,  
		           		nr_seq_planej_ww,
					nr_seq_trab_w, 
					nr_seq_tipo_solucao_w,
           				null, 
		           		null, 
           				null, 
					nr_seq_planej_w, 
					nr_seq_tipo_cont_w,
					'S',
					'4',
					'Prevent_Event',
					nr_seq_origem_dano_w,
					nr_seq_causa_dano_w,					
					nr_seq_complex_w,
					nr_seq_cs_w,
					nr_seq_tipo_os_w);

		       		open C02;
        			loop
        			fetch C02 into
	        		   	ds_atividade_w,
        			   	qt_min_prev_w,
					nr_seq_apres_w;
        			exit when C02%notfound;
	
					select	man_ordem_ativ_prev_seq.nextval
        				into	nr_seq_ativ_w
		        		from	dual;

		           		insert into man_ordem_ativ_prev(
              					nr_sequencia,
              					nr_seq_ordem_serv,
		              			dt_atualizacao,
              					nm_usuario,
              					ds_atividade,
		              			qt_min_prev,
						nr_seq_apres)
           				values(	nr_seq_ativ_w,
              					nr_seq_ordem_w,
		              			sysdate,
              					'Prevent_Event',
              					ds_atividade_w,
		              			qt_min_prev_w,
						nr_seq_apres_w);
       				end loop;
	        		close C02;

				open c03;
				loop
				fetch C03 into
					ds_arquivo_w,
					ie_anexar_email_w;
				exit when C03%notfound;

					select	man_ordem_serv_arq_seq.nextval
					into	nr_seq_anexo_w
					from	dual;

					insert into man_ordem_serv_arq(
						nr_sequencia,
						nr_seq_ordem,
						ds_arquivo,
						ie_anexar_email,
						dt_atualizacao,
						nm_usuario)
					values(	nr_seq_anexo_w,
						nr_seq_ordem_w,
						ds_arquivo_w,
						ie_anexar_email_w,
						sysdate,
						'Prevent_Event');
				end loop;
				close c03;		

		        	if	(nm_usuario_exec_w is not null) then
           				insert into man_ordem_servico_exec(
           	   				nr_sequencia,
              					nr_seq_ordem,
              					dt_atualizacao,
	              				nm_usuario,
        	      				nm_usuario_exec,
              					qt_min_prev,
						dt_recebimento)
           				values(	man_ordem_servico_exec_seq.nextval,
              					nr_seq_ordem_w,
              					sysdate,
	              				'Prevent_Event',
        	      				nm_usuario_exec_w,
              					0,
						sysdate);
        			end if;

				if	(ie_comunicacao_w = 'S') then
					begin
	
					ds_perfil_adicional_w	:= '';

					if	(nvl(cd_perfil_w,0) <> 0) then
						ds_perfil_adicional_w	:= cd_perfil_w || ',';
					end if;

					if	(nm_usuario_destino_w <> 'X') then
						ds_comunicado_w := 'Gerado a ordem de servi�o da Manuten��o preventiva ' ||
									ds_dano_breve_w || ', com o seguinte dano: ' || chr(13) || chr(10);
						ds_comunicado_w := substr(ds_comunicado_w || ds_dano_w,1,4000);

						insert into comunic_interna(
							dt_comunicado,
							ds_titulo,
							ds_comunicado,
							nm_usuario,
							dt_atualizacao,
							ie_geral,
							nm_usuario_destino,
							cd_perfil,
							nr_sequencia,
							ie_gerencial,
							nr_seq_classif,
							ds_perfil_adicional,
							cd_setor_destino,
							cd_estab_destino,
							ds_setor_adicional,
							dt_liberacao,
							ds_grupo,
							nm_usuario_oculto)
						values( sysdate,
							'Abertura de Ordem de Servi�o Preventiva n� ' || nr_seq_ordem_w,
							ds_comunicado_w,
							'Prevent_Event',
							sysdate,
							'N',
							nm_usuario_destino_w,
							null,
							comunic_interna_seq.nextval,
							'N',
							null,
							ds_perfil_adicional_w,
							null,
							null,
							null,
							sysdate,
							null,
							null);
					end if;
					end;
				end if;
				end;
			end if;
			end;
		end if;

		end;

	end loop;
	close C01;
	end;
end if;

end Man_gerar_OS_Planej_Evento;
/
