create or replace
procedure justifica_alerta_servico(nr_sequencia_p		NUMBER,
				   ds_justificativa_p		varchar2) IS

BEGIN

update 	atendimento_alerta
set 	ds_justificativa = ds_justificativa_p
WHERE	nr_sequencia	= nr_sequencia_p;

commit;

END justifica_alerta_servico;
/


