create or replace 
procedure Retirar_Exame_Lote_LAB (	nr_prescricao_p		number,
					nr_seq_prescr_p		number)  is

begin

delete lab_lote_exame_item
where nr_prescricao = nr_prescricao_p
  and nr_seq_prescr = nvl(nr_seq_prescr_p,nr_seq_prescr);

commit;

end Retirar_Exame_Lote_LAB;
/
