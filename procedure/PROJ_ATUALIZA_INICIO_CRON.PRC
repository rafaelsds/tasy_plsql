create or replace
procedure Proj_Atualiza_inicio_cron
			(	nr_sequencia_p		number) is 

nr_seq_superior_w		number(10,0);
dt_inicio_real_w		date;
begin

begin
select	nr_seq_superior,
	dt_inicio_real
into	nr_seq_superior_w,
	dt_inicio_real_w
from	proj_cron_etapa
where	nr_sequencia	= nr_sequencia_p;
exception
	when others then
	dt_inicio_real_w := null;
end;

while 	(nr_seq_superior_w is not null) loop
	begin
	if	(dt_inicio_real_w is not null) then
		update	proj_cron_etapa
		set	dt_inicio_real	= dt_inicio_real_w
		where	nr_sequencia	= nr_seq_superior_w
		and	dt_inicio_real is null;
	end if;
	
	select	max(nr_seq_superior)
	into	nr_seq_superior_w
	from	proj_cron_etapa
	where	nr_sequencia	= nr_seq_superior_w;
	end;
	
end loop;

commit;

end Proj_Atualiza_inicio_cron;
/
