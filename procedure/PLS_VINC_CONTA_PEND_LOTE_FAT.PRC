create or replace
procedure pls_vinc_conta_pend_lote_fat
			(	nr_seq_conta_p			number,
				nr_seq_lote_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2,
				ds_mensagem_p		out	varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Vincular contas que estejam na pasta de pendentes a um lote de faturamento de
forma unit�ria para a conta
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:  Gera��o das informa��es na tabela tempor�ria W_PLS_LOTE_FAT_ITEM,
tratamentos feitos na rotina PLS_GERAR_LOTE_FATURAMENTO

	ROTINA NAO � MAIS UTILIZADA
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 	


begin

null; -- ROTINA NAO � MAIS UTILIZADA

end pls_vinc_conta_pend_lote_fat;
/