create or replace
procedure gerar_empenho_requisicao(
		nr_requisicao_p		number,
		dt_referencia_p		date,
		nm_usuario_p		varchar2) is

cd_estabelecimento_w			estabelecimento.cd_estabelecimento%type;
dt_solicitacao_requisicao_w		date;

cursor c01 is
	select	b.cd_centro_custo,
		a.cd_conta_contabil,
		nvl(sum(a.qt_estoque * obter_custo_medio_material(b.cd_estabelecimento, dt_referencia_p, a.cd_material)),0) vl_requisicao
	from	requisicao_material b,
		item_requisicao_material a
	where	a.nr_requisicao = b.nr_requisicao
	and	b.nr_requisicao = nr_requisicao_p
	and	b.cd_centro_custo is not null
	and	a.cd_conta_contabil is not null
	and	b.dt_liberacao is not null
	and	nvl(a.qt_estoque * obter_custo_medio_material(b.cd_estabelecimento, dt_referencia_p, a.cd_material),0) <> 0
	group by
		b.cd_centro_custo,
		a.cd_conta_contabil;

type c01_type is table of c01%rowtype;
c01_regs_w		c01_type;
c01_w			c01%rowtype;

cursor c02 is
	select	a.dt_referencia,
		a.cd_estabelecimento,
		a.cd_centro_custo,
		a.cd_conta_contabil
	from	ctb_orc_doc_empenho a
	where	a.nr_requisicao = nr_requisicao_p;

type c02_type is table of c02%rowtype;
c02_regs_w		c02_type;
c02_w			c02%rowtype;

begin

open c02;
loop
fetch c02 bulk collect into c02_regs_w limit 1000;
	for idx02 in 1..c02_regs_w.count loop
		begin

		c02_w := c02_regs_w(idx02);

		delete	from ctb_orc_doc_empenho
		where	nr_requisicao		= nr_requisicao_p
		and	cd_centro_custo		= c02_w.cd_centro_custo
		and	cd_conta_contabil	= c02_w.cd_conta_contabil
		and	dt_referencia		= c02_w.dt_referencia
		and	cd_estabelecimento	= c02_w.cd_estabelecimento;

		ctb_atualizar_saldo_empenho(
				c02_w.cd_estabelecimento,
				c02_w.dt_referencia,
				c02_w.cd_centro_custo,
				c02_w.cd_conta_contabil,
				nm_usuario_p);
		end;
	end loop;
exit when c02%notfound;
end loop;
close c02;

select	cd_estabelecimento,
	dt_solicitacao_requisicao
into	cd_estabelecimento_w,
	dt_solicitacao_requisicao_w
from	requisicao_material a
where	a.nr_requisicao = nr_requisicao_p;

open c01;
loop
fetch c01 bulk collect into c01_regs_w limit 1000;
	for idx01 in 1..c01_regs_w.count loop
		begin
		c01_w := c01_regs_w(idx01);

		ctb_registrar_doc_empenho(
				cd_estabelecimento_w,
				trunc(dt_solicitacao_requisicao_w,'mm'),
				null,
				null,
				null,
				null,
				nr_requisicao_p,
				c01_w.cd_centro_custo,
				c01_w.cd_conta_contabil,
				c01_w.vl_requisicao,
				'N',
				'I',
				nm_usuario_p);

		end;
	end loop;
exit when c01%notfound;
end loop;
close c01;

end gerar_empenho_requisicao;
/
