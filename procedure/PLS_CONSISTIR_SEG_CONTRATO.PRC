create or replace
procedure pls_consistir_seg_contrato
			(	nr_seq_segurado_p		in	number,
				nr_seq_contrato_p		in	number,
				cd_pessoa_fisica_p		in	varchar2,
				dt_contratacao_p		in	date,
				dt_inclusao_operadora_p		in	date,
				nr_seq_pagador_p		in	number,
				nr_seq_subestipulante_p		in	number,
				nr_seq_titular_p		in	number,
				nr_seq_parentesco_p		in	number,
				ie_tipo_vinculo_operadora_p	in	varchar2,
				nr_seq_plano_p			in	number,
				nr_seq_tabela_p			in	number,
				nr_seq_canal_p			in	number,
				ie_bonific_cooperado_p		in	varchar2,
				cd_matricula_estipulante_p	in	varchar2,
				cd_matricula_familia_p		in	number,
				nr_seq_localizacao_benef_p	in	number,
				nr_seq_faixa_salarial_p		in	number,
				nr_seq_canal_ret_p		out	number,
				nr_seq_vend_ret_p		out	number,
				nm_usuario_p			in	varchar2,
				cd_estabelecimento_p		in	number,
				ds_erro_p			out	varchar2) is

ds_erro_w			Varchar2(4000);
ie_situacao_w			Varchar2(1);
qt_registro_w			Number(5)	:= 0;
qt_registro_ww			Number(5)	:= 0;
dt_contrato_w			Date;
qt_dias_futuro_w		Number(5)	:= 0;
dt_limite_w			Date;
ie_novo_beneficiario_w		varchar2(1);
ie_tipo_pessoa_contrato_w	varchar2(2);
ie_exige_vinc_operadora_w	Varchar2(1);
dt_fim_vigencia_w		date;
ie_tipo_operacao_w		varchar2(3);
nr_seq_contrato_sca_w		number(10);
dt_fim_vigencia_sca_w		date;
nr_seq_regra_lanc_w		number(10);
nr_seq_canal_ret_w		number(10);
nr_seq_vend_ret_w		number(10);
qt_dependente_w			number(10);
nr_seq_dependente_w		number(10);
nm_pessoa_dependente_w		varchar2(255);
nr_seq_contrato_plano_w		number(10);
dt_inativacao_w			date;
ie_subestipulante_depen_w	varchar2(10);
nr_seq_subestipulante_tit_w	number(10);
nr_seq_subestipulante_depen_w	number(10);
ie_pagador_grupo_w		varchar2(2);
qt_registro_www			number(10);
dt_contratacao_titular_w	date;
ie_dt_adesao_w			varchar2(1);
ie_exige_matricula_estip_w	varchar2(1);
ie_exige_matricula_familiar_w	varchar2(1);
ie_titularidade_w		varchar2(1);
dt_nascimento_w			date;
ie_pagador_contr_origem_w	varchar2(10);
ie_acao_contrato_w		varchar2(10);
nr_contrato_ant_w		number(10);
ie_tipo_incremento_w		varchar2(10);
qt_matricula_w			number(10);
cd_operadora_empresa_w		number(10);
dt_casamento_w			date;
qt_dias_w			number(10);
ie_grau_parentesco_w		number(10);
ie_exige_localizacao_empresa_w	varchar2(1);
qt_canal_venda_ativo_w		number(10);
ie_exige_faixa_salarial_w	pls_regra_benef_contrato.ie_exige_faixa_salarial%type;
dt_inicio_plano_w		date;
ie_situacao_plano_w		pls_plano.ie_situacao%type;
qt_cons_dias_incl_cong_filho_w	pls_parametros.qt_cons_dias_incl_cong_filho%type;
nr_seq_motivo_susp_w		pls_plano.nr_seq_motivo_susp%type;
ie_permite_inclusao_w		pls_motivo_susp_prod.ie_permite_inclusao%type;
ie_exclusivo_benef_remido_w	pls_contrato.ie_exclusivo_benef_remido%type;
ie_tipo_contratacao_w		pls_plano.ie_tipo_contratacao%type;
ds_erro_prod_dif_w		varchar2(4000);
nr_contrato_w			pls_contrato.nr_sequencia%type;
ie_produto_diferente_w		varchar2(255);

Cursor C01 is
	select	ie_exige_vinc_operadora,
		ie_exige_matricula_estip,
		ie_exige_matricula_familiar,
		ie_exige_localizacao_empresa,
		ie_exige_faixa_salarial
	from	pls_regra_benef_contrato
	where	nr_seq_contrato	= nr_seq_contrato_p
	and	((ie_titularidade = ie_titularidade_w) or (ie_titularidade = 'A'))
	order by nvl(ie_titularidade,'A');

Cursor C02 is
	select	ie_tipo_incremento
	from	pls_carteira_controle_iden
	where	ie_tipo_regra		= 'F'
	and	ie_situacao		= 'A'
	and	((nr_seq_plano		= nr_seq_plano_p and nr_seq_plano is not null) or
		(nr_seq_plano is null))
	and	((nvl(ie_tipo_pessoa,'A') = ie_tipo_pessoa_contrato_w) or (nvl(ie_tipo_pessoa,'A') = 'A'))
	and	((ie_tipo_contratacao is null) or (ie_tipo_contratacao = ie_tipo_contratacao_w))
	and	cd_estabelecimento	= cd_estabelecimento_p
	order by	nvl(nr_seq_grupo_intercambio,0),
			nvl(nr_seq_plano,0),
			nvl(ie_tipo_pessoa,'A'),
			ie_tipo_contratacao;

begin
select	decode(nr_seq_titular_p,0,'T','D')
into	ie_titularidade_w
from	dual;

begin
select	dt_nascimento,
	dt_emissao_cert_casamento
into	dt_nascimento_w,
	dt_casamento_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
exception 
when others then
	wheb_mensagem_pck.exibir_mensagem_abort( 335608, null );
end;

open C01;
loop
fetch C01 into
	ie_exige_vinc_operadora_w,
	ie_exige_matricula_estip_w,
	ie_exige_matricula_familiar_w,
	ie_exige_localizacao_empresa_w,
	ie_exige_faixa_salarial_w;
exit when C01%notfound;
end loop;
close C01;

--Parametro [13] - Quantidade de dias futuros para inclusao de contratos
begin
select	nvl(max(obter_valor_param_usuario(1202, 13, Obter_Perfil_Ativo, nm_usuario_p, 0)), 'N')
into	qt_dias_futuro_w
from	dual;
exception
when others then
	qt_dias_futuro_w	:= 0;
end;

ie_subestipulante_depen_w 	:= nvl(obter_valor_param_usuario(1202, 64, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'S');
ie_pagador_grupo_w		:= nvl(obter_valor_param_usuario(1202, 84, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'N');
ie_dt_adesao_w			:= nvl(obter_valor_param_usuario(1202, 88, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'S');
ie_pagador_contr_origem_w	:= nvl(obter_valor_param_usuario(1202, 97, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'N');
ie_produto_diferente_w		:= nvl(obter_valor_param_usuario(1202, 65, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'S');

--Obter dados do contrato
select	ie_situacao,
	dt_contrato,
	nvl(ie_novo_beneficiario,'S'),
	substr(pls_obter_dados_contrato(nr_seq_contrato_p,'TE'),1,2),
	ie_tipo_operacao,
	cd_operadora_empresa,
	nvl(ie_exclusivo_benef_remido, 'N'),
	nr_contrato
into	ie_situacao_w,
	dt_contrato_w,
	ie_novo_beneficiario_w,
	ie_tipo_pessoa_contrato_w,
	ie_tipo_operacao_w,
	cd_operadora_empresa_w,
	ie_exclusivo_benef_remido_w,
	nr_contrato_w
from	pls_contrato 
where	nr_sequencia	= nr_seq_contrato_p;

if	(nvl(nr_seq_titular_p,0) = 0)and
	(ie_exclusivo_benef_remido_w = 'S')then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(1106080);
end if;

if	(ie_situacao_w	= '3') then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280737);
end if;

if	(trunc(dt_contratacao_p,'dd') < trunc(dt_contrato_w,'dd')) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280738);
end if;

select	nvl(max(ie_situacao),'A'),
	nvl(max(nr_seq_motivo_susp),0),
	max(ie_tipo_contratacao)
into	ie_situacao_plano_w,
	nr_seq_motivo_susp_w,
	ie_tipo_contratacao_w
from	pls_plano
where	nr_sequencia	= nr_seq_plano_p;

if 	(nr_seq_motivo_susp_w > 0) then
	select	nvl(max(ie_permite_inclusao),'N')
	into	ie_permite_inclusao_w
	from	pls_motivo_susp_prod 
	where	nr_sequencia = nr_seq_motivo_susp_w;
else
	ie_permite_inclusao_w	:= 'S';
end if;

if	(nvl(nr_seq_titular_p,0) = 0) then
	if	(((ie_novo_beneficiario_w = 'N') and (ie_tipo_pessoa_contrato_w = 'PJ')) or
		 ((ie_situacao_plano_w = 'S') and (ie_permite_inclusao_w = 'N'))) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280739);
	end if;
elsif	(nvl(nr_seq_titular_p,0) <> 0) then
	select	max(ie_grau_parentesco)
	into	ie_grau_parentesco_w
	from	grau_parentesco
	where	nr_sequencia		= nr_seq_parentesco_p;
	
	if	(ie_permite_inclusao_w = 'N') then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280739);
	end if;
	
	if	(dt_contratacao_p is not null) and
		(ie_dt_adesao_w = 'N') then
		select	dt_contratacao
		into	dt_contratacao_titular_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_titular_p;
		
		if	(trunc(dt_contratacao_titular_w,'dd') > trunc(dt_contratacao_p,'dd')) then
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280740);
		end if;
	end if;
	
	if	((ie_novo_beneficiario_w = 'N' and ie_tipo_pessoa_contrato_w = 'PJ') or
		 (ie_situacao_plano_w = 'S' and ie_permite_inclusao_w = 'N')) then
		begin
		select	nvl(qt_cons_dias_incl_cong_filho,0)
		into	qt_cons_dias_incl_cong_filho_w
		from	pls_parametros
		where	cd_estabelecimento = cd_estabelecimento_p;
		exception
		when others then
			qt_cons_dias_incl_cong_filho_w := 0;
		end;
		
		if	(ie_grau_parentesco_w = 3) then
			if	(qt_cons_dias_incl_cong_filho_w > 0) then
				qt_dias_w	:= dt_contratacao_p - dt_nascimento_w;
				if	(qt_dias_w > qt_cons_dias_incl_cong_filho_w) then
					ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280629, 'QT_DIAS_P=' || qt_dias_w);
				end if;
			end if;
		elsif	(ie_grau_parentesco_w = 4) then
			if	(qt_cons_dias_incl_cong_filho_w > 0) then
				qt_dias_w	:= dt_contratacao_p - dt_casamento_w;
				if	(qt_dias_w > qt_cons_dias_incl_cong_filho_w) then
					ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280632, 'QT_DIAS_P=' || qt_dias_w);
				elsif	(dt_casamento_w is null) then
					ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280633);
				end if;
			end if;
		else
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280636);
		end if;
	end if;
end if;

--Beneficiario ja cadastrado no contrato. Verifique!
select	count(*)
into	qt_registro_w
from	pls_segurado
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	nr_seq_contrato		= nr_seq_contrato_p
and	nr_sequencia		<> nr_seq_segurado_p
and	((nr_seq_subestipulante	= nr_seq_subestipulante_p) or (nr_seq_subestipulante is null))
and	nr_seq_plano		= nr_seq_plano_p
and	dt_rescisao is null;
if	(qt_registro_w	> 0) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280741);
end if;

dt_limite_w	:= sysdate + qt_dias_futuro_w;
if	(dt_contratacao_p	> dt_limite_w) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280748, 'DT_LIMITE_P=' || dt_limite_w);
end if;

if	(nvl(nr_seq_pagador_p,0) <> 0) then
	select	count(1)
	into	qt_registro_w
	from	pls_contrato_pagador
	where	nr_sequencia	= nr_seq_pagador_p
	and	dt_rescisao is not null
	and	trunc(dt_rescisao,'dd') <= trunc(dt_contratacao_p,'dd');
	
	if	(qt_registro_w > 0) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(713256); --O pagador selecionado esta inativo
	end if;
	
	select	count(*)
	into	qt_registro_w
	from	pls_contrato_pagador
	where	nr_seq_contrato	= nr_seq_contrato_p
	and	nr_sequencia 	= nr_seq_pagador_p;
	
	select	count(*)
	into	qt_registro_ww
	from	pls_contrato_pagador
	where	nr_seq_contrato = (	select	nvl(max(nr_contrato_principal),0)
					from	pls_contrato	a
					where	a.nr_sequencia	= nr_seq_contrato_p)
	and	nr_sequencia	= nr_seq_pagador_p;
	
	if	(ie_pagador_grupo_w = 'S') then
		select	count(*)
		into	qt_registro_www
		from	pls_contrato a,	--Contrato ao qual o beneficiario esta sendo incluso
			pls_grupo_contrato b,
			pls_contrato_grupo c,
			pls_contrato_grupo d,
			pls_contrato e, --Contratos do grupo de contratos
			pls_contrato_pagador f
		where	b.nr_sequencia		= c.nr_seq_grupo
		and	c.nr_seq_contrato	= a.nr_sequencia
		and	c.nr_seq_grupo		= d.nr_seq_grupo
		and	d.nr_seq_contrato	= e.nr_sequencia
		and	e.nr_sequencia		= f.nr_seq_contrato
		and	b.ie_tipo_relacionamento = '1'
		and	a.nr_sequencia		= nr_seq_contrato_p
		and	f.nr_sequencia		= nr_seq_pagador_p;
	end if;
	
	qt_registro_w	:= qt_registro_w + qt_registro_ww + nvl(qt_registro_www,0);
	
	begin
	select	nvl(max(ie_acao_contrato),'A'),
		max(nr_contrato_ant)
	into	ie_acao_contrato_w,
		nr_contrato_ant_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
	exception
	when others then
		ie_acao_contrato_w	:= 'A';
		nr_contrato_ant_w	:= null;
	end;
	
	if	(qt_registro_w	= 0) and
		(ie_pagador_contr_origem_w = 'S') and
		(ie_acao_contrato_w = 'M') and
		(nr_contrato_ant_w is not null) then
		select	count(*)
		into	qt_registro_ww
		from	pls_contrato_pagador
		where	nr_seq_contrato = (	select	nvl(max(nr_sequencia),0)
						from	pls_contrato	a
						where	a.nr_contrato	= nr_contrato_ant_w)
		and	nr_sequencia	= nr_seq_pagador_p;
		
		if	(qt_registro_ww = 0) then
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280749);
		end if;
	elsif	(qt_registro_w	= 0) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280751);
	end if;
end if;

if	((ie_exige_vinc_operadora_w = 'S') and (ie_tipo_vinculo_operadora_p is null)) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280752);
end if;

if	((ie_exige_matricula_estip_w = 'S') and (cd_matricula_estipulante_p is null)) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280753);
end if;

if	((ie_exige_matricula_familiar_w = 'S') and (nvl(cd_matricula_familia_p,0) = 0)) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280754);
end if;

if	((ie_exige_localizacao_empresa_w = 'S') and (nvl(nr_seq_localizacao_benef_p,0) = 0)) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280755);
end if;

if	((ie_exige_faixa_salarial_w = 'S') and (nvl(nr_seq_faixa_salarial_p,0) = 0)) then
	ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(301239);
end if;

if	(nr_seq_tabela_p is not null) then
	select	max(dt_fim_vigencia)
	into	dt_fim_vigencia_w
	from	pls_tabela_preco
	where	nr_sequencia	= nr_seq_tabela_p;
	
	if	(nvl(dt_fim_vigencia_w,sysdate) < sysdate) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280756);
	end if;
end if;

if	(nr_seq_plano_p is not null) and
	(ie_tipo_operacao_w	= 'A') then
	select	max(nr_sequencia)
	into	nr_seq_contrato_sca_w
	from	pls_contrato_plano
	where	nr_seq_plano	= nr_seq_plano_p
	and	nr_seq_contrato	= nr_seq_contrato_p;
	if	(nr_seq_contrato_sca_w is not null) then
		select	dt_fim_vigencia
		into	dt_fim_vigencia_sca_w
		from	pls_contrato_plano
		where	nr_sequencia	= nr_seq_contrato_sca_w;
		
		if	(nvl(dt_fim_vigencia_sca_w,sysdate) < sysdate) then
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280757);
		end if;
	end if;
end if;

if	(nr_seq_canal_p = 0) then
	if	(nvl(pls_obter_dados_contrato(nr_seq_contrato_p, 'AR'),'T') in ('A','T')) then
		select	max(nr_sequencia)
		into	nr_seq_regra_lanc_w
		from	pls_contrato_regra_lanc
		where	nr_seq_contrato		= nr_seq_contrato_p
		and	dt_contratacao_p between nvl(dt_inicio_vigencia,dt_contratacao_p) and nvl(dt_fim_vigencia,dt_contratacao_p);
		
		select	max(nr_seq_vendedor_canal),
			max(nr_seq_vendedor_pf)
		into	nr_seq_canal_ret_w,
			nr_seq_vend_ret_w
		from	pls_contrato_regra_lanc
		where	nr_sequencia	= nr_seq_regra_lanc_w;
		
		nr_seq_canal_ret_p	:= nr_seq_canal_ret_w;
		nr_seq_vend_ret_p	:= nr_seq_vend_ret_w;
	end if;
elsif	(nr_seq_canal_p is not null) then
	select	count(1)
	into	qt_canal_venda_ativo_w
	from	pls_vendedor
	where	nr_sequencia	= nr_seq_canal_p
	and	dt_contratacao_p between nvl(dt_inicio_vigencia,dt_contratacao_p) and nvl(dt_fim_vigencia,dt_contratacao_p)
	and	ie_situacao	= 'A';
	
	if	(qt_canal_venda_ativo_w = 0) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280758, 'NR_SEQ_CANAL_P=' || nr_seq_canal_p);
	end if;
end if;

if	(ie_bonific_cooperado_p = 'S') then
	if	(nvl(nr_seq_titular_p,0) = 0) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280759);
	end if;
	
	select	count(*)
	into	qt_dependente_w
	from	pls_segurado
	where	nr_seq_titular	= nr_seq_titular_p
	and	nr_sequencia	<> nr_seq_segurado_p
	and	ie_bonific_cooperado	= 'S';
	
	if	(qt_dependente_w <> 0) then
		select	max(a.nr_sequencia),
			max(b.nm_pessoa_fisica)
		into	nr_seq_dependente_w,
			nm_pessoa_dependente_w
		from	pls_segurado	a,
			pessoa_fisica	b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.nr_seq_titular	= nr_seq_titular_p
		and	a.nr_sequencia		<> nr_seq_segurado_p
		and	ie_bonific_cooperado	= 'S';
		
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280760, 'NR_SEQ_DEPENDENTE_P=' || nr_seq_dependente_w ||
										';NM_PESSOA_DEPENDENTE_P=' || nm_pessoa_dependente_w);
	end if;
end if;

if	(nr_seq_plano_p is not null) then
	select	max(nr_sequencia)
	into	nr_seq_contrato_plano_w
	from	pls_contrato_plano
	where	nr_seq_contrato				= nr_seq_contrato_p
	and	nr_seq_plano				= nr_seq_plano_p
	and	nvl(nr_seq_tabela,nr_seq_tabela_p)	= nr_seq_tabela_p;
	
	if	(nr_seq_contrato_plano_w is not null) then
		select	dt_inativacao,
			trunc(dt_inicio_vigencia, 'dd')
		into	dt_inativacao_w,
			dt_inicio_plano_w
		from	pls_contrato_plano
		where	nr_sequencia	= nr_seq_contrato_plano_w;
		
		if	(dt_contratacao_p > nvl(dt_inativacao_w,dt_contratacao_p)) then
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280761, 'DT_INATIVACAO=' || to_char(dt_inativacao_w,'dd/mm/rrrr'));
		elsif	(dt_inicio_plano_w > trunc(dt_contratacao_p, 'dd')) then
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(328371);
		end if;
	end if;
end if;

if	(ie_subestipulante_depen_w	= 'N') then
	if	(nvl(nr_seq_titular_p,0) <> 0) then
		begin
		select	max(nr_seq_subestipulante)
		into	nr_seq_subestipulante_tit_w
		from	pls_segurado
		where	nr_sequencia = nr_seq_titular_p;
		exception
		when others then
			nr_seq_subestipulante_tit_w	:= null;
		end;
		
		if	(nr_seq_subestipulante_tit_w is not null) then
			if	(nr_seq_subestipulante_tit_w	<> nr_seq_subestipulante_p) then
				ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280762);
			end if;
		end if;
	else
		begin
		select	max(nr_seq_subestipulante)
		into	nr_seq_subestipulante_depen_w
		from	pls_segurado
		where	nr_seq_titular	= nr_seq_segurado_p;
		exception
		when others then
			nr_seq_subestipulante_depen_w	:= null;
		end;
		
		if	(nr_seq_subestipulante_depen_w is not null) then
			if	(nr_seq_subestipulante_depen_w	<> nr_seq_subestipulante_p) then
				ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280763);
			end if;
		end if;
	end if;
end if;	

pls_consistir_produto_diferent(	nr_seq_segurado_p, nr_seq_titular_p, nr_contrato_w, 
				nr_seq_plano_p, ie_produto_diferente_w, sysdate,
				'S', nm_usuario_p, cd_estabelecimento_p,
				ds_erro_prod_dif_w);

if	(ds_erro_prod_dif_w is not null) then
	ds_erro_prod_dif_w	:= ds_erro_w || ds_erro_prod_dif_w;
end if;

if	(dt_inclusao_operadora_p is not null) then
	if	(trunc(dt_contratacao_p,'dd') < trunc(dt_inclusao_operadora_p,'dd')) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280767);
	end if;
end if;

if	(dt_nascimento_w is not null) then
	if	(trunc(dt_nascimento_w,'dd') > trunc(dt_contratacao_p,'dd')) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280768);
	end if;
end if;

qt_matricula_w	:= 0;

if	(nvl(cd_matricula_familia_p,0) <> 0) and
	(ie_titularidade_w = 'T') then
	open C02;
	loop
	fetch C02 into
		ie_tipo_incremento_w;
	exit when C02%notfound;
	end loop;
	close C02;
	
	if	(ie_tipo_incremento_w = 'SQ') then
		select	count(1)
		into	qt_matricula_w
		from	pls_segurado
		where	cd_matricula_familia	= cd_matricula_familia_p;
	elsif	(ie_tipo_incremento_w = 'SE') then
		select	count(1)
		into	qt_matricula_w
		from	pls_segurado
		where	cd_matricula_familia	= cd_matricula_familia_p
		and	cd_operadora_empresa	= cd_operadora_empresa_w;
	elsif	(ie_tipo_incremento_w = 'SC') then
		select	count(1)
		into	qt_matricula_w
		from	pls_segurado
		where	cd_matricula_familia	= cd_matricula_familia_p
		and	nr_seq_contrato		= nr_seq_contrato_p;
	end if;
	
	if	(qt_matricula_w	> 0) then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280769, 'CD_MATRICULA_FAMILIA_P=' || cd_matricula_familia_p);
	end if;
end if;

ds_erro_p	:= substr(ds_erro_w,1,255);

end pls_consistir_seg_contrato;
/ 