create or replace
procedure obter_dados_controle_visitas(
		nr_atendimento_p			number,
		nr_seq_controle_p			number,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
		qt_atendimentos_p		out	number,
		qt_acompanhante_p		out	number,
		nr_atendimento_cracha_p	out	varchar2) is 

ie_restaurar_grupo_w	varchar2(1);
ie_atend_visita_w		varchar2(1);

begin

-- Controle de Visitas - Par�metro [52] - Permite a restaura��o autom�tica dos grupos de acesso do paciente ao registrar a sa�da do mesmo.
obter_param_usuario(8014, 52, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_restaurar_grupo_w);

if	(ie_restaurar_grupo_w = 'S') then
	begin
	select	decode(count(*), 1, 'S', 'N')
	into	ie_atend_visita_w
	from	atendimento_visita
	where	nr_seq_controle	= nr_seq_controle_p
	and	dt_saida	is null
	and	ie_paciente	= 'S';
	
	if	(ie_atend_visita_w = 'S') then
		excluir_grupo_visita_pac(nr_atendimento_p);
	end if;
	end;
end if;

if	(nr_atendimento_p is not null) then
	select	count(*)
	into	qt_atendimentos_p
	from	atendimento_visita
	where	nr_atendimento	= nr_atendimento_p
	and	dt_saida		is null;
end if;

if	(nr_seq_controle_p is not null) then
	begin
	select	count(*)
	into	qt_acompanhante_p
	from	atendimento_acompanhante
	where	nr_controle	= nr_seq_controle_p
	and	dt_saida		is null;

	select	substr(obter_paciente_cracha(nr_seq_controle_p), 1, 15)
	into	nr_atendimento_cracha_p
	from	dual;
	end;
end if;

end obter_dados_controle_visitas;
/
