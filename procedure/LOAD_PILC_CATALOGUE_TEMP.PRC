create or replace procedure load_pilc_catalogue_temp( cd_code_p		in varchar2 ,
						ds_oec_desc_p	in varchar2 )
 as
  begin
	
	insert into w_pilcload_proc
	( cd_code,
	ds_oec_description
	)
	values
	(
	cd_code_p,
	ds_oec_desc_p
	);
end load_pilc_catalogue_temp; 
/
