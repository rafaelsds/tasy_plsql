create or replace
procedure pls_gera_log_relat(	nr_seq_relatorio_p	number,
				ds_log_p 		long,
				nm_usuario_p		Varchar2) is 

begin

insert into pls_relat_log
(nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ds_log, nr_seq_relatorio)
values
(pls_relat_log_seq.nextval,sysdate,nm_usuario_p,
sysdate,nm_usuario_p,ds_log_p,nr_seq_relatorio_p );

commit;

end pls_gera_log_relat;
/