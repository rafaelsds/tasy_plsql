create or replace
procedure cpoe_copiar_npt_infantil (nr_seq_npt_cpoe_p     in number,
									nr_seq_npt_anterior_p in number) is 

qt_count_elemento_w     number;
qt_count_produto_w      number;
nr_seq_npt_elemento_w   cpoe_npt_elemento.nr_sequencia%type;
nr_seq_npt_elem_next_w   cpoe_npt_elemento.nr_sequencia%type;

cursor  c01 is
select  *
from    cpoe_npt_elemento
where   nr_seq_npt_cpoe = nr_seq_npt_anterior_p;

cursor  c02 is
select  *
from    cpoe_npt_produto
where   nr_seq_npt_cpoe = nr_seq_npt_anterior_p
and     nr_seq_elemento = nr_seq_npt_elemento_w;

begin

    select  count(0)
    into    qt_count_elemento_w
    from    cpoe_npt_elemento
    where   nr_seq_npt_cpoe = nr_seq_npt_cpoe_p;
    
    select  count(0)
    into    qt_count_produto_w
    from    cpoe_npt_produto
    where   nr_seq_npt_cpoe = nr_seq_npt_cpoe_p;
    
    if ( qt_count_elemento_w = 0 and qt_count_produto_w = 0) then
        for c01_w in c01 loop
            nr_seq_npt_elemento_w  := c01_w.nr_sequencia;
            select  cpoe_npt_elemento_seq.nextval
            into    nr_seq_npt_elem_next_w
            from    dual;
            
            insert into cpoe_npt_elemento (    
                cd_unidade_medida,
                dt_atualizacao,
                dt_atualizacao_nrec,
                ie_editado,
                ie_glutamina,
                ie_item_valido,
                ie_npt,
                ie_prim_fase,
                ie_prod_adicional,
                ie_quar_fase,
                ie_seg_fase,
                ie_terc_fase,
                ie_tipo_elemento,
                ie_unid_med,
                nm_usuario,
                nm_usuario_nrec,
                nr_seq_elemento,
                nr_seq_elem_mat,
                nr_seq_npt_cpoe,
                nr_sequencia,
                pr_concentracao,
                pr_total,
                qt_diaria,
                qt_elem_kg_dia,
                qt_grama_nitrogenio,
                qt_kcal,
                qt_osmolaridade,
                qt_protocolo,
                qt_volume,
                qt_volume_final 
            ) values (
                c01_w.cd_unidade_medida,
                sysdate,
                sysdate,
                c01_w.ie_editado,
                c01_w.ie_glutamina,
                c01_w.ie_item_valido,
                c01_w.ie_npt,
                c01_w.ie_prim_fase,
                c01_w.ie_prod_adicional,
                c01_w.ie_quar_fase,
                c01_w.ie_seg_fase,
                c01_w.ie_terc_fase,
                c01_w.ie_tipo_elemento,
                c01_w.ie_unid_med,
                wheb_usuario_pck.get_nm_usuario,
                wheb_usuario_pck.get_nm_usuario,
                c01_w.nr_seq_elemento,
                c01_w.nr_seq_elem_mat,
                nr_seq_npt_cpoe_p,
                nr_seq_npt_elem_next_w,
                c01_w.pr_concentracao,
                c01_w.pr_total,
                c01_w.qt_diaria,
                c01_w.qt_elem_kg_dia,
                c01_w.qt_grama_nitrogenio,
                c01_w.qt_kcal,
                c01_w.qt_osmolaridade,
                c01_w.qt_protocolo,
                c01_w.qt_volume,
                c01_w.qt_volume_final );
                
            for c02_w in c02 loop
                insert into cpoe_npt_produto (
                    cd_material,
                    cd_unidade_medida,
                    dt_atualizacao,
                    dt_atualizacao_nrec,
                    ie_item_valido,
                    ie_modificado,
                    ie_prod_adicional,
                    ie_somar_volume,
                    ie_supera_limite_uso,
                    nm_usuario,
                    nm_usuario_nrec,
                    nr_seq_elemento,
                    nr_seq_elem_mat,
                    nr_seq_npt_cpoe,
                    nr_sequencia,
                    qt_dose,
                    qt_protocolo,
                    qt_vol_cor,
                    qt_volume
                ) values (
                    c02_w.cd_material,
                    c02_w.cd_unidade_medida,
                    sysdate,
                    sysdate,
                    c02_w.ie_item_valido,
                    c02_w.ie_modificado,
                    c02_w.ie_prod_adicional,
                    c02_w.ie_somar_volume,
                    c02_w.ie_supera_limite_uso,
                    wheb_usuario_pck.get_nm_usuario,
                    wheb_usuario_pck.get_nm_usuario,
                    nr_seq_npt_elem_next_w,
                    c02_w.nr_seq_elem_mat,
                    nr_seq_npt_cpoe_p,
                    cpoe_npt_produto_seq.nextval,
                    c02_w.qt_dose,
                    c02_w.qt_protocolo,
                    c02_w.qt_vol_cor,
                    c02_w.qt_volume );
            end loop;
        end loop;
    
        commit;
    end if;
end cpoe_copiar_npt_infantil;
/