create or replace
procedure pls_sib_gravar_cco
			(	nr_seq_devolucao_p	number,
				nm_usuario_p		varchar2) is

Cursor C01 is
	select	nr_sequencia,
		nr_seq_segurado,
		cd_cco
	from	pls_sib_devolucao_cco
	where	nr_seq_devolucao = nr_seq_devolucao_p
	and	nr_seq_segurado is not null
	and	cd_cco is not null
	and	dt_confirmacao is null;

begin

for r_c01_w in C01 loop
	begin
	
	update	pls_segurado
	set	cd_cco		= r_c01_w.cd_cco
	where	nr_sequencia	= r_c01_w.nr_seq_segurado;
	
	update	pls_sib_devolucao_cco
	set	dt_confirmacao		= sysdate,
		nm_usuario_confirmacao	= nm_usuario_p
	where	nr_sequencia	= r_c01_w.nr_sequencia;
	end;
end loop;

commit;

end pls_sib_gravar_cco;
/
