create or replace
procedure pls_desfazar_contr_mig_exi
			(	nr_seq_proposta_p	varchar2,
				nm_usuario_p		varchar2) is

nr_seq_segurado_w		number(10);
nr_seq_segurado_ant_w		number(10);
nr_seq_segurado_mig_w		number(10);
nr_seq_contrato_w		number(10);
dt_liberacao_w			date;
dt_rescisao_w			date;
dt_migracao_w			date;
dt_suspensao_seg_w		date;
nm_pessoa_fisica_w		varchar2(255);
nr_seq_motivo_canc_migracao_w	pls_proposta_adesao.nr_seq_motivo_cancel%type;
nr_seq_motivo_cancelamento_w	pls_segurado.nr_seq_motivo_cancelamento%type;
ie_rescisao_migracao_w		pls_segurado.ie_rescisao_migracao%type;
nr_seq_contrato_ant_w		pls_contrato.nr_sequencia%type;
ie_situacao_w			pls_contrato.ie_situacao%type;
nr_seq_status_exclusao_w	pls_segurado_status.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,
		nr_seq_segurado_ant,
		nr_seq_contrato,
		dt_liberacao,
		dt_migracao
	from	pls_segurado
	where	nr_proposta_adesao	= nr_seq_proposta_p
	and	ie_tipo_segurado	in ('A','B')
	order by decode(nr_seq_titular,null,1,-1);

begin

select	max(nr_seq_motivo_cancel)
into	nr_seq_motivo_canc_migracao_w
from	pls_proposta_adesao
where	nr_sequencia	= nr_seq_proposta_p;

if	(nr_seq_motivo_canc_migracao_w is null) then
	nr_seq_motivo_canc_migracao_w	:= obter_valor_param_usuario(1232, 20, Obter_Perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);
end if;

open C01;
loop
fetch C01 into
	nr_seq_segurado_w,
	nr_seq_segurado_ant_w,
	nr_seq_contrato_w,
	dt_liberacao_w,
	dt_migracao_w;
exit when C01%notfound;
	begin
	
	select	substr(obter_nome_pf(cd_pessoa_fisica),1,255)
	into	nm_pessoa_fisica_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	
	--Verificar se o benefic�rio j� est� liberado
	if	(dt_liberacao_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(226177,'NM_BENEFICIARIO='||nm_pessoa_fisica_w);
	end if;	
	
	if	(nr_seq_segurado_ant_w is not null) then
		select	max(nr_seq_segurado_mig),
			max(nr_seq_motivo_cancelamento)
		into	nr_seq_segurado_mig_w,
			nr_seq_motivo_cancelamento_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_ant_w;
		
		--Verificar se o benefic�rio que vai ser delatado � igual ao que foi migrado
		if	(nr_seq_segurado_mig_w = nr_seq_segurado_w) then
			select	dt_rescisao,
				nvl(ie_rescisao_migracao,'N'),
				nr_seq_contrato
			into	dt_rescisao_w,
				ie_rescisao_migracao_w,
				nr_seq_contrato_ant_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_segurado_ant_w;
			
			if	(ie_rescisao_migracao_w = 'S') then
				dt_suspensao_seg_w := pls_obter_dt_suspensao_seg(nr_seq_segurado_ant_w);
				update	pls_segurado
				set	dt_rescisao			= null,
					dt_limite_utilizacao		= null,
					nr_seq_motivo_cancelamento	= null,
					nr_seq_causa_rescisao		= null,
					ie_tipo_rescisao		= '',
					dt_cancelamento			= null,
					ie_situacao_atend		= decode(dt_suspensao_seg_w,null,decode(dt_liberacao,null,'I','A'),'S'),
					ie_rescisao_migracao		= null
				where	nr_sequencia			= nr_seq_segurado_ant_w;
				
				select 	max(ie_situacao)
				into	ie_situacao_w
				from 	pls_contrato
				where	nr_sequencia = nr_seq_contrato_ant_w;
					
				if 	(nvl(ie_situacao_w,1) = 3) then
					update	pls_contrato
					set	dt_rescisao_contrato	= null,
						dt_limite_utilizacao	= null,
						ie_situacao		= '2',
						nm_usuario		= nm_usuario_p,
						dt_atualizacao		= sysdate,
						dt_reativacao		= sysdate,
						dt_cancelamento		= null,
						nr_seq_motivo_rescisao	= null,
						nr_seq_causa_rescisao	= null
					where	nr_sequencia		= nr_seq_contrato_ant_w;
					
					insert into pls_contrato_historico (
								nr_sequencia,
								cd_estabelecimento,
								nr_seq_contrato,
								dt_historico,
								ie_tipo_historico,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ds_historico)
							values(	pls_contrato_historico_seq.nextval,
								wheb_usuario_pck.get_cd_estabelecimento,
								nr_seq_contrato_ant_w,
								sysdate,
								'2',
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								'Reativa��o do contrato ao desfazer a contrata��o atrav�s da fun��o OPS - Proposta de Ades�o Eletr�nica');
				end if;	
				
				/* Gerar hist�rico  de reativa��o do segurado anterior*/
				pls_gerar_segurado_historico
				(	nr_seq_segurado_ant_w, '2', sysdate,'Reativa��o', 
					'Desfeita a contrata��o do benefici�rio atrav�s da fun��o OPS - Proposta de Ades�o', null, sysdate, null, 
					null, sysdate, null, null, 
					null, null, null, null, nm_usuario_p,
					'N');
				
				pls_preco_beneficiario_pck.atualizar_preco_beneficiarios(null, nr_seq_contrato_ant_w, null, null, sysdate, null, 'N', nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);	
			end if;
			
			/*Retira informa��es do benefici�rio, sobre a migra��o*/
			update	pls_segurado
			set	dt_migracao			= null,
				nr_seq_segurado_mig		= null,
				nr_contrato_migrado		= null,
				nm_usuario			= nm_usuario_p,
				dt_atualizacao			= sysdate
			where	nr_sequencia			= nr_seq_segurado_ant_w;
		else
			goto final;
		end if;
	end if;
	
	update	pls_declaracao_segurado
	set	nr_seq_segurado = null
	where	nr_seq_segurado = nr_seq_segurado_w;
	
	delete	w_pls_carencia_abrang_ant
	where	nr_seq_segurado	= nr_seq_segurado_w;
		
	delete	pls_segurado_compl
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	delete	pls_carencia
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	delete	sip_beneficiario_exposto
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	delete	w_pls_benef_movto_mensal
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	delete	w_pls_benef_movto_mes_sca
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	delete	pls_segurado_repasse
	where	nr_seq_segurado	= nr_seq_segurado_w;
		
	delete	pls_segurado_doc_arq
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	begin
	delete	pls_bonificacao_vinculo
	where	nr_seq_segurado	= nr_seq_segurado_w;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(226178,'NM_BENEFICIARIO='||nm_pessoa_fisica_w||';'||'DS_ERRO='||sqlerrm(sqlcode));
	end;
	
	begin
	
	update	pls_segurado_preco_origem
	set	nr_seq_preco = null
	where 	nr_seq_vinculo_sca	in (	select 	nr_sequencia
						from 	pls_sca_vinculo
						where	nr_seq_segurado	= nr_seq_segurado_w);
	
	delete	pls_plano_preco
	where 	nr_seq_tabela   in (	select 	a.nr_seq_tabela
					from 	pls_sca_vinculo a
					where	a.nr_seq_segurado	= nr_seq_segurado_w
					and	not exists ( 	select  1
								from 	pls_sca_vinculo b
								where 	b.nr_seq_tabela = a.nr_seq_tabela
								and	b.nr_sequencia <> a.nr_sequencia));
		
	delete  pls_segurado_preco_origem
	where 	nr_seq_vinculo_sca	in (	select 	nr_sequencia
						from 	pls_sca_vinculo
						where	nr_seq_segurado	= nr_seq_segurado_w);
						
	update	pls_tabela_preco
	set	nr_seq_sca = null, nr_seq_sca_vinculo = null
	where	nr_sequencia	in (	select 	a.nr_seq_tabela
					from 	pls_sca_vinculo a
					where	a.nr_seq_segurado	= nr_seq_segurado_w
					and	not exists ( 	select  1
								from 	pls_sca_vinculo b
								where 	b.nr_seq_tabela = a.nr_seq_tabela
								and	b.nr_sequencia <> a.nr_sequencia));
		
	update	pls_sca_vinculo
	set	nr_seq_tabela = null
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	delete 	pls_tabela_preco
	where	nr_sequencia	in (	select 	a.nr_seq_tabela
					from 	pls_sca_vinculo a
					where	a.nr_seq_segurado	= nr_seq_segurado_w
					and	not exists ( 	select  1
								from 	pls_sca_vinculo b
								where 	b.nr_seq_tabela = a.nr_seq_tabela
								and	b.nr_sequencia <> a.nr_sequencia));
	
	delete	pls_sca_vinculo
	where	nr_seq_segurado	= nr_seq_segurado_w;
		
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(226178,'NM_BENEFICIARIO='||nm_pessoa_fisica_w||';'||'DS_ERRO='||sqlerrm(sqlcode));
	end;
	
	begin
	delete	pls_segurado_historico
	where	nr_seq_segurado	= nr_seq_segurado_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(226178,'NM_BENEFICIARIO='||nm_pessoa_fisica_w||';'||'DS_ERRO='||sqlerrm(sqlcode));
	end;
	
	begin
	delete	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(226178,'NM_BENEFICIARIO='||nm_pessoa_fisica_w||';'||'DS_ERRO='||sqlerrm(sqlcode));
	end;
	
	<<final>>
	nr_seq_segurado_w	:= nr_seq_segurado_w;
	
	end;
end loop;
close C01;

end pls_desfazar_contr_mig_exi;
/
