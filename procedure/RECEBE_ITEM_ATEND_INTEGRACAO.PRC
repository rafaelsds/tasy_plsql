create or replace
procedure recebe_item_atend_integracao(
			nr_atendimento_p		number,
			cd_material_p		number,
			qt_material_p		number,
			nr_seq_lote_fornec_p	number,
			cd_local_estoque_p	number) is
			
nr_seq_atepacu_w		number(10);
dt_entrada_unidade_w	date;
cd_categoria_w		varchar2(10);
nr_sequencia_w		number;
cd_convenio_w		number;
qt_existe_mat_w		number(10);
cd_estabelecimento_w	number(4);
ie_tipo_atendimento_w	number(3);
cd_unidade_medida_w	varchar2(30);
cd_setor_atendimento_w	prescr_medica.cd_setor_atendimento%type;

begin

nr_seq_atepacu_w := obter_atepacu_paciente(nr_atendimento_p, 'IA');

begin
select	cd_setor_atendimento,
	dt_entrada_unidade
into	cd_setor_atendimento_w,
	dt_entrada_unidade_w
from 	atend_paciente_unidade
where 	nr_seq_interno = nr_seq_atepacu_w;
exception
when others then
	nr_seq_atepacu_w:= 0;
end;

select 	material_atend_paciente_seq.NextVal
into	nr_sequencia_w
from 	dual;

select 	obter_convenio_atendimento(nr_atendimento_p)
into	cd_convenio_w
from 	dual;

select 	obter_categoria_atendimento(nr_atendimento_p)
into	cd_categoria_w
from 	dual;

if (nr_seq_atepacu_w = 0) then
	begin
	GERAR_PASSAGEM_SETOR_ATEND(nr_atendimento_p,cd_setor_atendimento_w,to_date(sysdate,'dd/mm/yyyy hh24:mi:ss'),'S','Tasy');
	
	select	nvl(max(nr_seq_interno),0)		
	into	nr_seq_atepacu_w		
	from	atend_paciente_unidade
	where	nr_atendimento		= nr_atendimento_p
	and	cd_setor_atendimento	= cd_setor_atendimento_w;

	select	dt_entrada_unidade
	into	dt_entrada_unidade_w
	from	atend_paciente_unidade
	where	nr_seq_interno = nr_seq_atepacu_w;
	end;
end if;

select	count(*)
into	qt_existe_mat_w
from	material
where	cd_material = cd_material_p;

select	max(cd_estabelecimento),
	max(ie_tipo_atendimento)
into	cd_estabelecimento_w,
	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

if	(qt_existe_mat_w  > 0) then
	
	select	max(cd_unidade_medida_consumo)
	into	cd_unidade_medida_w
	from	material
	where	cd_material		= cd_material_p;

	insert  into material_atend_paciente 
			(nr_sequencia,
			nr_atendimento,
		 	dt_entrada_unidade,
			cd_material,
			cd_material_exec,
			dt_atendimento,
			qt_material, 
			dt_atualizacao,
			nm_usuario,
			cd_setor_atendimento,
			nr_seq_atepacu,
			--nr_doc_convenio,
			cd_convenio,
			cd_categoria,
			ie_auditoria,
			cd_unidade_medida,
			nr_seq_lote_fornec,
			cd_local_estoque,
			cd_acao)
		values (nr_sequencia_w,
			nr_atendimento_p,
			dt_entrada_unidade_w, 
			cd_material_p,
			cd_material_p,
			to_date(sysdate,'dd/mm/yyyy hh24:mi:ss'),
			qt_material_p,
			sysdate,
			'Tasy',
			cd_setor_atendimento_w,
			nr_seq_atepacu_w,
			--nr_doc_convenio_p,
			cd_convenio_w,
			cd_categoria_w,
			'N',
			cd_unidade_medida_w,
			nr_seq_lote_fornec_p,
			cd_local_estoque_p,
			1);
	commit;
	
	Atualiza_preco_material(nr_sequencia_w, 'Tasy');
	
end if;

end recebe_item_atend_integracao;
/
