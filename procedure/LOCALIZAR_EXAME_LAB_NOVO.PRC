create or replace
PROCEDURE Localizar_Exame_Lab_novo(ds_localizar_p		IN VARCHAR2,
					nr_seq_localizar_p	IN NUMBER,
					ds_exames_p			OUT VARCHAR2,
					ds_grupos_p			OUT VARCHAR2) IS

nr_sequencia_w 		NUMBER(10);
nr_seq_exame_w 		NUMBER(10);
nr_seq_superior_w		NUMBER(10);
nr_seq_superior_teste_w	NUMBER(10);
nr_seq_superior_w2	NUMBER(10);
nr_seq_grupo_w		NUMBER(10);
nr_seq_grupo_w2		NUMBER(10);
nr_seq_localizar_w	NUMBER(2);
nm_exame_w			VARCHAR2(255);
nr_seq_grupo_pesquisa_w	NUMBER(10);
nr_seq_exame_pesquisa_w	NUMBER(10);
nr_seq_superior_pesq_w	NUMBER(10);
ie_sair_cursor_w	Varchar2(1);

CURSOR c001 IS
	SELECT a.nr_seq_exame,
		 a.nr_seq_superior,
		 a.nr_seq_grupo,
		 a.nm_exame
	FROM 	grupo_exame_lab b,
		exame_laboratorio a
	WHERE a.nr_seq_grupo =  b.nr_sequencia
	AND  a.nr_seq_superior IS NULL
	ORDER BY b.ds_grupo_exame_lab,
		   NVL(a.nr_seq_superior,0),
		   a.nm_exame;



CURSOR C002 IS
	SELECT a.nr_seq_exame,
		 a.nr_seq_superior,
		 a.nr_seq_grupo,
		 a.nm_exame
	FROM grupo_exame_lab b,
		exame_laboratorio a
	WHERE a.nr_seq_grupo =  b.nr_sequencia
	AND a.nr_seq_superior IS NOT NULL
	START WITH nr_seq_exame = nr_seq_exame_w
	CONNECT BY a.nr_seq_superior = PRIOR nr_seq_exame
	ORDER BY DECODE(nr_seq_superior, nr_seq_exame_w, a.nM_exame, OBTER_DESC_EXAME(nr_seq_superior)), DECODE(nr_seq_superior, nr_seq_exame_w, nr_Seq_exame, nr_seq_superior), LEVEL;

BEGIN

nr_seq_localizar_w := nr_seq_localizar_p;
nr_sequencia_w	 := 0;
OPEN c001;
LOOP
FETCH c001 INTO
	nr_seq_exame_w,
	nr_seq_superior_w,
	nr_seq_grupo_w,
	nm_exame_w;
EXIT WHEN C001%NOTFOUND;
	BEGIN
	if	(ie_sair_cursor_w	= 'S') then
		exit;
	end if;
	
	ie_sair_cursor_w	:= 'N';

	IF	(elimina_acentuacao(UPPER(nm_exame_w)) LIKE '%'|| elimina_acentuacao(UPPER(ds_localizar_p)) || '%') THEN

		nr_sequencia_w := nr_sequencia_w + 1;

		IF 	(nr_sequencia_w > nr_seq_localizar_p) THEN

			nr_seq_localizar_w := nr_seq_localizar_p + 1;
			ds_exames_p := TO_CHAR(nr_seq_exame_w);
			nr_seq_grupo_pesquisa_w := nr_seq_grupo_w;
			nr_seq_exame_pesquisa_w	:= nr_seq_exame_w;
			nr_seq_superior_pesq_w	:= nr_seq_superior_w;
			EXIT;
		END IF;
	END IF;

	
	OPEN C002;
	LOOP
	FETCH C002 INTO
		nr_seq_exame_w,
		nr_seq_superior_w,
		nr_seq_grupo_w,
		nm_exame_w;
	EXIT WHEN C002%NOTFOUND;
		BEGIN	

		IF	(elimina_acentuacao(UPPER(nm_exame_w)) LIKE '%'|| elimina_acentuacao(UPPER(ds_localizar_p)) || '%') THEN		
			nr_sequencia_w := nr_sequencia_w + 1;		
			IF 	(nr_sequencia_w > nr_seq_localizar_p) THEN			
				nr_seq_localizar_w := nr_seq_localizar_p + 1;
				ds_exames_p := TO_CHAR(nr_seq_exame_w);
				nr_seq_grupo_pesquisa_w := nr_seq_grupo_w;
				nr_seq_exame_pesquisa_w	:= nr_seq_exame_w;
				nr_seq_superior_pesq_w	:= nr_seq_superior_w;
				ie_sair_cursor_w	:= 'S';
				EXIT;
			END IF;
		END IF;
		END;
	END LOOP;
	CLOSE C002;


	END;
END LOOP;
CLOSE c001;

IF 	(ds_exames_p IS NOT NULL) AND
	(nr_seq_superior_pesq_w IS NOT NULL) THEN
	BEGIN
	ds_exames_p := TO_CHAR(nr_seq_superior_pesq_w) || ',' || ds_exames_p;
	nr_seq_superior_w2 := nr_seq_superior_pesq_w;
	
	WHILE nr_seq_superior_w2 IS NOT NULL LOOP
		BEGIN
		SELECT nr_seq_superior
		INTO	 nr_seq_superior_w2
		FROM exame_laboratorio
		WHERE nr_seq_exame = nr_seq_superior_pesq_w;
		IF (nr_seq_superior_w2 IS NOT NULL) THEN
			ds_exames_p := TO_CHAR(nr_seq_superior_w2) || ',' || ds_exames_p;
		END IF;
		nr_seq_superior_pesq_w := nr_seq_superior_w2;
		END;
	END LOOP;
	END;
END IF;

IF 	( nr_seq_grupo_pesquisa_w IS NOT NULL) THEN
	BEGIN
	ds_grupos_p := TO_CHAR(nr_seq_grupo_pesquisa_w);
	nr_seq_grupo_w2 := nr_seq_grupo_pesquisa_w;
	WHILE nr_seq_grupo_w2 IS NOT NULL LOOP
		BEGIN
		SELECT nr_seq_superior
		INTO	 nr_seq_grupo_w2
		FROM grupo_exame_lab
		WHERE nr_sequencia =nr_seq_grupo_pesquisa_w;
		IF (nr_seq_grupo_w2 IS NOT NULL) THEN
			ds_grupos_p := TO_CHAR(nr_seq_grupo_w2) || ',' || ds_grupos_p;
		END IF;
		nr_seq_grupo_pesquisa_w := nr_seq_grupo_w2;
		END;
	END LOOP;
	END;
ELSE
	nr_seq_localizar_w := 0;
END IF;

END Localizar_Exame_Lab_novo;
/