create or replace
procedure Gerar_Agenda_Cons_AgePac
			(nr_seq_agepac_p	number,
			nr_seq_agecons_p	number,
			nm_usuario_p		varchar2) is



cd_pessoa_fisica_w		varchar2(10);
cd_convenio_w			number(10,0);
cd_usuario_convenio_w		varchar2(30);
ds_senha_w			varchar2(20);
dt_agenda_w			date;
hr_inicio_w			date;
dt_validade_carteira_w		date;
ie_copia_data_w			varchar2(01);
cd_agenda_consulta_w		number(10,0);
dt_agenda_cons_w		date;
cd_categoria_w			Varchar2(10);
cd_tipo_acomodacao_w		Number(4);
nr_doc_convenio_w		Varchar2(20);
ds_observacao_w			varchar2(2000);

begin

obter_param_usuario(898, 30, obter_perfil_ativo, nm_usuario_p, 0, ie_copia_data_w);	


select	cd_agenda
into	cd_agenda_consulta_w
from	agenda_consulta
where	nr_sequencia	= nr_seq_agecons_p;

select	cd_pessoa_fisica,
	cd_convenio,
	cd_usuario_convenio,
	ds_senha,
	dt_agenda,
	hr_inicio,
	cd_categoria,
	cd_tipo_acomodacao,
	nr_doc_convenio,
	dt_validade_carteira,
	substr(ds_observacao,1,2000)
into	cd_pessoa_fisica_w,
	cd_convenio_w,
	cd_usuario_convenio_w,
	ds_senha_w,
	dt_agenda_w,
	hr_inicio_w,
	cd_categoria_w,
	cd_tipo_acomodacao_w,
	nr_doc_convenio_w,
	dt_validade_carteira_w,
	ds_observacao_w
from	agenda_paciente
where	nr_sequencia	= nr_seq_agepac_p;


if	(cd_pessoa_fisica_w is not null) then
	begin

	dt_agenda_cons_w	:= null;

	if	(ie_copia_data_w = 'S') then
		begin

		delete	from agenda_consulta
		where	dt_agenda		= to_date(to_char(dt_agenda_w, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_w, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
		and	cd_agenda		= cd_agenda_consulta_w
		and	ie_status_agenda	= 'L';

		select	max(dt_agenda) + 1/86400
		into	dt_agenda_cons_w
		from	agenda_consulta
		where	cd_agenda		= cd_agenda_consulta_w
		and	ie_status_agenda	<> 'L'
		and	to_char(dt_agenda, 'dd/mm/yyyy hh24:mi') = to_char(dt_agenda_w, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_w, 'hh24:mi' );

		end;
	end if;

	update	agenda_consulta
	set	cd_pessoa_fisica	= cd_pessoa_fisica_w,
		cd_convenio		= cd_convenio_w,
		cd_categoria		= cd_categoria_w,	
		cd_tipo_acomodacao	= cd_tipo_acomodacao_w,
		cd_usuario_convenio	= cd_usuario_convenio_w,
		nr_doc_convenio		= nr_doc_convenio_w,
		dt_validade_carteira	= dt_validade_carteira_w,
		cd_senha		= ds_senha_w,
		ie_status_agenda	= 'N',
		nr_seq_agepaci		= nr_seq_agepac_p,
		ds_observacao		= ds_observacao_w,
		dt_agenda		= decode(dt_agenda_cons_w, null, decode(ie_copia_data_w, 'S', 
			to_date(to_char(dt_agenda_w, 'dd/mm/yyyy') || ' ' || to_char(hr_inicio_w, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'), dt_agenda), dt_agenda_cons_w)
	where	nr_sequencia		= nr_seq_agecons_p;
	

	end;
end if;


commit;

end Gerar_Agenda_Cons_AgePac;
/