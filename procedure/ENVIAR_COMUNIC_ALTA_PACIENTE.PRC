create or replace 
procedure enviar_comunic_alta_paciente is

nm_paciente_w		varchar2(40);
cd_setor_destino_w	number(5);
cd_medico_resp_w	varchar2(10);
nm_usuario_w		varchar2(15);
dt_previsto_alta_w	date;

cursor	c01 is
	select	cd_medico_resp,
		substr(obter_nome_pf(cd_pessoa_fisica),1,40),
		obter_usuario_pessoa(cd_medico_resp),
		obter_setor_atendimento(nr_atendimento),
		dt_previsto_alta
	from	atendimento_paciente
	where	dt_alta > dt_previsto_alta
	and	dt_alta is not null;

begin

OPEN C01;
LOOP
FETCH C01 into
	cd_medico_resp_w,
	nm_paciente_w,
	nm_usuario_w,
	cd_setor_destino_w,
	dt_previsto_alta_w;
exit when c01%notfound;
	begin

	insert	into comunic_interna
		(nr_sequencia,
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		nm_usuario_destino,
		dt_atualizacao,
		ie_geral,
		ie_gerencial,
		cd_setor_destino,
		dt_liberacao)
	values	(comunic_interna_seq.nextval,
		sysdate,
		wheb_mensagem_pck.get_texto(803140),
		wheb_mensagem_pck.get_texto(803141, 'NM_PACIENTE='||nm_paciente_w) || Chr(13) || Chr(10) ||
		wheb_mensagem_pck.get_texto(803142) || ': ' || dt_previsto_alta_w,
		'Tasy',
		nm_usuario_w,
		sysdate,
		'N',
		'N',
		cd_setor_destino_w,
		sysdate);
	
	end;
END LOOP;
CLOSE C01;

commit;

end	enviar_comunic_alta_paciente;
/
