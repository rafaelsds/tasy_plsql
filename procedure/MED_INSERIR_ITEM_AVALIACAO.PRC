create or replace 
Procedure Med_Inserir_Item_Avaliacao
		(ds_tipo_avaliacao_p		varchar2,
		ds_item_p			varchar2,
		nr_seq_apresent_p		number,
		ie_resultado_p			varchar2,
		qt_decimais_p			number,
		ds_unid_med_p			varchar2,
		qt_minimo_p			number,
		qt_maximo_p			number,
		qt_tamanho_p			number,
		qt_desloc_direita_p		number,
		nr_seq_superior_p		number,
		ds_item_superior_p		varchar2,
		nr_seq_apres_superior_p		number,
		ie_obrigatorio_p		varchar2,
		qt_pos_esquerda_p		number,
		qt_altura_p			number,
		cd_dominio_p			number,
		nr_seq_item_ref_p		number,
		ds_item_ref_p			varchar2,
		nr_seq_apres_ref_p		number,
		vl_padrao_p			varchar2,
		ds_complemento_p		varchar2,
		ie_tipo_campo_p			varchar2,
		ds_mascara_p			varchar2,
		ds_regra_p			varchar2,
		nm_usuario_p			varchar2) is


nr_seq_item_w		number(10,0);
nr_seq_tipo_w		number(10,0);
nr_seq_superior_w	number(10,0);
nr_seq_refer_w		number(10,0);

begin

select	nvl(max(nr_sequencia),0)
into	nr_seq_tipo_w	
from	med_tipo_avaliacao
where	ds_tipo		= substr('COPIA ' || ds_tipo_avaliacao_p,1,60);

if	(nr_seq_tipo_w <> 0) then
	begin
	
	select	med_item_avaliar_seq.nextval
	into	nr_seq_item_w
	from	dual;

	if	(nr_seq_superior_p is null) and
		(nr_seq_item_ref_p is null) then
		begin

		insert 	into med_item_avaliar
			(nr_sequencia,
			ds_item,
			nr_seq_tipo,
			nr_seq_apresent,
			ie_resultado,
			dt_atualizacao,
			nm_usuario,
			qt_decimais,
			ds_unid_med,
			qt_minimo,
			qt_maximo,
			qt_tamanho,
			qt_desloc_direita,
			nr_seq_superior,
			ie_obrigatorio,
			qt_pos_esquerda,
			cd_dominio,
			nr_seq_item_ref,
			vl_padrao,
			ds_complemento,
			ie_tipo_campo,
			ds_mascara,
			ds_regra,
			qt_altura,
			IE_GRAFICO_AVALIACAO)
		values	(nr_seq_item_w,
			nvl(ds_item_p,' '),
			nr_seq_tipo_w,
			nr_seq_apresent_p,
			ie_resultado_p,
			sysdate,
			nm_usuario_p,
			qt_decimais_p,
			ds_unid_med_p,
			qt_minimo_p,
			qt_maximo_p,
			qt_tamanho_p,
			qt_desloc_direita_p,
			null,
			ie_obrigatorio_p,
			qt_pos_esquerda_p,
			cd_dominio_p,
			null,
			vl_padrao_p,
			ds_complemento_p,
			ie_tipo_campo_p,
			ds_mascara_p,
			ds_regra_p,
			qt_altura_p,
			'S');
		end;
	end if;

	if	(nr_seq_superior_p is not null) and
		(nr_seq_item_ref_p is null) then
		begin

		select	nvl(max(nr_sequencia),0)
		into	nr_seq_superior_w
		from	med_item_avaliar
		where	ds_item		= ds_item_superior_p
		and	nr_seq_apresent	= nr_seq_apres_superior_p
		and	nr_seq_tipo	= nr_seq_tipo_w;

		if	(nr_seq_superior_w > 0) then
			begin

			insert 	into med_item_avaliar
				(nr_sequencia,
				ds_item,
				nr_seq_tipo,
				nr_seq_apresent,
				ie_resultado,
				dt_atualizacao,
				nm_usuario,
				qt_decimais,
				ds_unid_med,
				qt_minimo,
				qt_maximo,
				qt_tamanho,
				qt_desloc_direita,
				nr_seq_superior,
				ie_obrigatorio,
				qt_pos_esquerda,
				cd_dominio,
				nr_seq_item_ref,
				vl_padrao,
				ds_complemento,
				ie_tipo_campo,
				ds_mascara,
				ds_regra,
				qt_altura,
				IE_GRAFICO_AVALIACAO)
			values	(nr_seq_item_w,
				nvl(ds_item_p,' '),
				nr_seq_tipo_w,
				nr_seq_apresent_p,
				ie_resultado_p,
				sysdate,
				nm_usuario_p,
				qt_decimais_p,
				ds_unid_med_p,
				qt_minimo_p,
				qt_maximo_p,
				qt_tamanho_p,
				qt_desloc_direita_p,
				nr_seq_superior_w,
				ie_obrigatorio_p,
				qt_pos_esquerda_p,
				cd_dominio_p,
				null,
				vl_padrao_p,
				ds_complemento_p,
				ie_tipo_campo_p,
				ds_mascara_p,
				ds_regra_p,
				qt_altura_p,
				'S');
			end;
		end if;

		end;
	end if;

	if	(nr_seq_superior_p is null) and
		(nr_seq_item_ref_p is not null) then
		begin
	
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_refer_w
		from	med_item_avaliar
		where	ds_item		= ds_item_ref_p
		and	nr_seq_apresent	= nr_seq_apres_ref_p
		and	nr_seq_tipo	= nr_seq_tipo_w;

		if	(nr_seq_refer_w > 0) then
			begin

			insert 	into med_item_avaliar
				(nr_sequencia,
				ds_item,
				nr_seq_tipo,
				nr_seq_apresent,
				ie_resultado,
				dt_atualizacao,
				nm_usuario,
				qt_decimais,
				ds_unid_med,
				qt_minimo,
				qt_maximo,
				qt_tamanho,
				qt_desloc_direita,
				nr_seq_superior,
				ie_obrigatorio,
				qt_pos_esquerda,
				cd_dominio,
				nr_seq_item_ref,
				vl_padrao,
				ds_complemento,
				ie_tipo_campo,
				ds_mascara,
				ds_regra,
				qt_altura,
				IE_GRAFICO_AVALIACAO)
			values	(nr_seq_item_w,
				nvl(ds_item_p,' '),
				nr_seq_tipo_w,
				nr_seq_apresent_p,
				ie_resultado_p,
				sysdate,
				nm_usuario_p,
				qt_decimais_p,
				ds_unid_med_p,
				qt_minimo_p,
				qt_maximo_p,
				qt_tamanho_p,
				qt_desloc_direita_p,
				null,
				ie_obrigatorio_p,
				qt_pos_esquerda_p,
				cd_dominio_p,
				nr_seq_refer_w,
				vl_padrao_p,
				ds_complemento_p,
				ie_tipo_campo_p,
				ds_mascara_p,
				ds_regra_p,
				qt_altura_p,
				'S');
			end;
		end if;

		end;

	end if;
	end;
end if;

commit;

end Med_Inserir_Item_Avaliacao;
/