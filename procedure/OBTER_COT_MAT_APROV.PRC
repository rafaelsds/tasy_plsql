create or replace
procedure OBTER_COT_MAT_APROV(	nr_seq_autor_cir_p	number,
					nm_usuario_p		varchar2) is

Cursor c01 is
	select	a.nr_sequencia
	from	material_autor_cirurgia a
	where	a.nr_seq_autorizacao	= nr_seq_autor_cir_p;
	
c01_w	c01%rowtype;

begin	

if	(nr_seq_autor_cir_p is not null) then

	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		
		OBTER_COT_MAT_AUTOR_APROV(c01_w.nr_sequencia,null,nm_usuario_p);
		
	end loop;
	close C01;
end if;

commit;

end OBTER_COT_MAT_APROV;
/
