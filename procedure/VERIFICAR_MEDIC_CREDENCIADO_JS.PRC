create or replace
procedure verificar_medic_credenciado_js(	cd_medico_p		varchar2,
						cd_estabelecimento_p	number,
						cd_convenio_p		number,
						cd_categoria_p		varchar2,
						cd_plano_p		varchar2,
						dt_referencia_p		date,
						ie_tipo_atend_p		number,
						ds_medico_credenciado_p	varchar2,
						ds_msg_retorno_p	out varchar2,
						ie_medico_credenciado_p	out varchar2) is 

cd_especialidade_w	varchar2(4000);
ie_medico_credenciado_w	varchar2(1);
			
begin

cd_especialidade_w	:= obter_especialidade_medico(cd_medico_p, 'C');

ie_medico_credenciado_w	:= obter_se_medico_conveniado(cd_estabelecimento_p, cd_medico_p, cd_convenio_p, null, 
						      cd_especialidade_w, cd_categoria_p, null, cd_plano_p, 
						      dt_referencia_p, ie_tipo_atend_p, null);

ds_msg_retorno_p	:= Wheb_mensagem_pck.get_texto(269050,null);
ie_medico_credenciado_p	:= ie_medico_credenciado_w;

end verificar_medic_credenciado_js;
/
