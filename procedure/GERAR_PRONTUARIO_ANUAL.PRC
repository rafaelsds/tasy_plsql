create or replace
procedure gerar_prontuario_anual
			(	ie_forma_anual_p		varchar2,
				nm_usuario_p			varchar2,
				nr_prontuario_p		out	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar o numero de prontuario para a pessoa fisica.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
	A tabela PACIENTE_PRONTUARIO_ANUAL nao esta em tela, e necessario fazer um insert na 
base do cliente para utilizar este processo da primeira vez.

	IE_FORMA_ANUAL_P - Dominio 1692
		S - Gerar com sequencia anual (ate 4 digitos a esquerda conforme mascara)
		M - Gerar com sequencia mensal (yymm000)
		E - Gerar com sequencia padrao (dois digitos a direita)
		C - Gerar com sequencia mensal com cinco digitos (yymm00000)
		N - Nao gerar prontuario anual
-------------------------------------------------------------------------------------------------------------------
Referencias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_prontuario_w		number(10,0);
nr_prontuario_w			number(10,0)	:= 0;
nr_prontuario_novo_w		number(10,0)	:= 0;
nr_prontuario_novo_char_w	varchar(255);
qt_prontuario_w			number(10,0)	:= 1;
qt_digito_pront_w		number(2,0) 	:= 1;
ie_pos_digito_w			number(1,0);
ds_digito_w			varchar2(4) 	:= '';
ds_mascara_w			varchar2(10);
nr_digito_pront_w		number(4,0);
nr_prontuario_mes_w		number(10,0);

cursor c01 is
	select	ds_mascara
	from	paciente_prontuario_anual
	order by
		dt_referencia;

begin
if	(nvl(ie_forma_anual_p,'S') = 'S') then
	lock table paciente_prontuario_anual in exclusive mode;

	/* Rafael em 02/01/2007 OS42393 => reaproveitar prontuarios gerados nao utilizados */
	delete 
	from	paciente_prontuario_anual a
	where	a.nr_prontuario > 0
	and	a.ie_deletar = 'S'
	and	not exists	(select	1
				from	pessoa_fisica	b
				where	b.nr_prontuario	= a.nr_prontuario);

	select	nvl(max(somente_numero(nr_prontuario)),0)
	into	nr_prontuario_w
	from	paciente_prontuario_anual
	where	ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_referencia)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(sysdate);

	if	(nr_prontuario_w > 0) then
		open c01;
		loop
		fetch c01 into ds_mascara_w;
		exit when c01%notfound;
			begin
			ds_mascara_w	:= ds_mascara_w;
			end;
		end loop;
		close c01;
		
		if	(ds_mascara_w = '00000yy') then			
			begin
			nr_prontuario_novo_w := nr_prontuario_w;
			
			while (qt_prontuario_w > 0) loop 
				begin				
				if	(length(nr_prontuario_novo_w) = 2) then
					nr_prontuario_novo_char_w 	:= '';
					nr_prontuario_novo_char_w 	:= '1' || to_char(nr_prontuario_novo_w);
					nr_prontuario_novo_w		:= to_number(nr_prontuario_novo_char_w);
				else
					nr_prontuario_novo_w		:= to_number(to_char(to_number(substr(to_char(nr_prontuario_novo_w),1,length(nr_prontuario_novo_w) - 2)) + 1) || 
										to_char(substr(to_char(nr_prontuario_novo_w),length(nr_prontuario_novo_w) - 1,length(nr_prontuario_novo_w))));																		
				end if;	
			
				begin
					select	1
					into	qt_prontuario_w
					from	pessoa_fisica
					where	nr_prontuario = nr_prontuario_novo_w;
				exception 
					when others then qt_prontuario_w := 0;
				end;		
				end;
			end loop;
			end;
		else
			begin
			begin
			nr_prontuario_novo_w	:= nr_prontuario_w;
			
			while	(qt_prontuario_w > 0) loop
				begin
				nr_prontuario_novo_w	:= nr_prontuario_novo_w + 1;
				
				begin
				select	nvl(count(*),0)
				into	qt_prontuario_w
				from	pessoa_fisica
				where	nr_prontuario	= nr_prontuario_novo_w;
				exception
				when others then
					qt_prontuario_w	:= 0;
				end;		
				end;
			end loop;
			end;
			end;
		end if;

		select	paciente_prontuario_anual_seq.nextval
		into	nr_seq_prontuario_w
		from	dual;

		insert into paciente_prontuario_anual
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_referencia,
			ds_mascara,
			nr_prontuario,
			ie_deletar)
		values	(nr_seq_prontuario_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			ds_mascara_w,
			nr_prontuario_novo_w,
			'S');
	elsif	(nr_prontuario_w = 0) then
		open c01;
		loop
		fetch c01 into ds_mascara_w;
		exit when c01%notfound;
			begin
			ds_mascara_w	:= ds_mascara_w;
			end;
		end loop;
		close c01;

		if	(ds_mascara_w is not null) then
			if	(ds_mascara_w = '00000yy') then	
				begin
				insert into paciente_prontuario_anual
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_referencia,
					ds_mascara,
					nr_prontuario,
					ie_deletar)
				values	(paciente_prontuario_anual_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					ds_mascara_w,
					to_number(to_char(sysdate,'yy')),
					'N');
				end;
			else
				begin
				select	length(ds_mascara_w),
					instr(ds_mascara_w,'y')
				into	qt_digito_pront_w,
					ie_pos_digito_w
				from	dual;

				while	(ie_pos_digito_w > 0) loop
					begin
					ds_digito_w		:= ds_digito_w || substr(ds_mascara_w, 1, ie_pos_digito_w);
					ds_mascara_w		:= substr(ds_mascara_w, ie_pos_digito_w + 1, qt_digito_pront_w);
					ie_pos_digito_w		:= instr(ds_mascara_w,'y');
					end;
				end loop;

				if	(ds_digito_w is not null) then
					select	to_number(to_char(sysdate, ds_digito_w))
					into	nr_digito_pront_w
					from	dual;

					nr_prontuario_novo_w	:= nr_digito_pront_w;

					while	(length(nr_prontuario_novo_w) < qt_digito_pront_w) loop
						begin
						nr_prontuario_novo_w	:= nr_prontuario_novo_w || to_number('0');
						end;
					end loop;

					select	paciente_prontuario_anual_seq.nextval
					into	nr_seq_prontuario_w
					from	dual;

					insert into paciente_prontuario_anual
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_referencia,
						ds_mascara,
						nr_prontuario,
						ie_deletar)
					values	(nr_seq_prontuario_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						ds_digito_w || ds_mascara_w,
						nr_prontuario_novo_w,
						'N');

					gerar_prontuario_anual(	ie_forma_anual_p,
								nm_usuario_p,
								nr_prontuario_novo_w);
				end if;
				end;
			end if;
			
			
		end if;
	end if;
elsif	(nvl(ie_forma_anual_p,'S') = 'E') then
	select	prontuario_seq.nextval || to_char(sysdate,'yy')
	into	nr_prontuario_novo_w
	from	dual;
elsif	(nvl(ie_forma_anual_p,'S') = 'M') then
	lock table paciente_prontuario_anual in exclusive mode;
	
	delete 
	from	paciente_prontuario_anual a
	where	a.nr_prontuario > 0
	and	a.ie_deletar = 'S'
	and	not exists	(select	1
				from	pessoa_fisica	b
				where	b.nr_prontuario	= a.nr_prontuario);

	select	nvl(max(somente_numero(nr_prontuario)),0)
	into	nr_prontuario_w
	from	paciente_prontuario_anual
	where	ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_referencia)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(sysdate);	

	if	(nr_prontuario_w > 0) then
		open c01;
		loop
		fetch c01 into ds_mascara_w;
		exit when c01%notfound;
			begin
			ds_mascara_w	:= ds_mascara_w;
			end;
		end loop;
		close c01;
		
		if	(ds_mascara_w = 'yymm000') then			
			nr_prontuario_novo_w	:= nr_prontuario_w;
			
			select	nvl(max(somente_numero(nr_prontuario)),0)
			into	nr_prontuario_mes_w
			from	paciente_prontuario_anual
			where	ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_referencia)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(sysdate)
			and 	ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_referencia)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate);
			
			if	(nr_prontuario_mes_w > 0) then -- Ja tem o prontuario no mes/ano entao acrescentar + 1
				while (qt_prontuario_w > 0) loop 
					begin				
					nr_prontuario_novo_w	:= nr_prontuario_novo_w + 1;
			
					begin
					select	1
					into	qt_prontuario_w
					from	pessoa_fisica
					where	nr_prontuario	= nr_prontuario_novo_w;
					exception 
					when others then 
						qt_prontuario_w	:= 0;
					end;
					end;
				end loop;
				
			else
				nr_prontuario_novo_char_w 	:= to_char(to_number(substr(to_char(nr_prontuario_novo_w),1,length(nr_prontuario_novo_w) - 3)) + 1);
				nr_prontuario_novo_w		:= to_number(nr_prontuario_novo_char_w || substr(nr_prontuario_novo_w,length(nr_prontuario_novo_w)-2, length(nr_prontuario_novo_w)));
				
				while (qt_prontuario_w > 0) loop 
					begin						
					nr_prontuario_novo_w	:= nr_prontuario_novo_w + 1;
			
					begin
					select	1
					into	qt_prontuario_w
					from	pessoa_fisica
					where	nr_prontuario	= nr_prontuario_novo_w;
					exception 
					when others then 
						qt_prontuario_w	:= 0;
					end;
					end;
				end loop;
				
			end if;
		
		end if;

		select	paciente_prontuario_anual_seq.nextval
		into	nr_seq_prontuario_w
		from	dual;

		insert into paciente_prontuario_anual
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_referencia,
			ds_mascara,
			nr_prontuario,
			ie_deletar)
		values	(nr_seq_prontuario_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			ds_mascara_w,
			nr_prontuario_novo_w,
			'S');				
	elsif	(nr_prontuario_w = 0) then
		open c01;
		loop
		fetch c01 into	ds_mascara_w;
		exit when c01%notfound;
			begin
			ds_mascara_w	:= ds_mascara_w;
			end;
		end loop;
		close c01;

		if	(ds_mascara_w is not null) then
			if	(ds_mascara_w = 'yymm000') then	
				begin
				nr_prontuario_novo_w	:= to_number(to_char(sysdate,'yy') || to_char(sysdate,'mm') || '000');
				nr_prontuario_novo_w	:= nr_prontuario_novo_w - 1;
				
				while (qt_prontuario_w > 0) loop 
					begin						
					nr_prontuario_novo_w	:= nr_prontuario_novo_w + 1;
			
					begin
					select	1
					into	qt_prontuario_w
					from	pessoa_fisica
					where	nr_prontuario	= nr_prontuario_novo_w;
					exception 
					when others then 
						qt_prontuario_w	:= 0;
					end;
					end;
				end loop;
				
				insert into paciente_prontuario_anual
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_referencia,
					ds_mascara,
					nr_prontuario,
					ie_deletar)
				values	(paciente_prontuario_anual_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					ds_mascara_w,
					nr_prontuario_novo_w,
					'N');
				end;
			end if;
		end if;
	end if;
elsif	(nvl(ie_forma_anual_p,'S') = 'C') then
	lock table paciente_prontuario_anual in exclusive mode;
	
	delete 
	from	paciente_prontuario_anual a
	where	a.nr_prontuario > 0
	and	a.ie_deletar = 'S'
	and	not exists	(select	1
				from	pessoa_fisica	b
				where	b.nr_prontuario	= a.nr_prontuario);

	-- Busca se ha um prontuario para o ano
	select	nvl(max(somente_numero(nr_prontuario)),0)
	into	nr_prontuario_w
	from	paciente_prontuario_anual
	where	ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_referencia)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(sysdate);	

	if	(nr_prontuario_w > 0) then
		-- Caso existir busca a mascara
		open c01;
		loop
		fetch c01 into ds_mascara_w;
		exit when c01%notfound;
			begin
			ds_mascara_w	:= ds_mascara_w;
			end;
		end loop;
		close c01;
		
		if	(ds_mascara_w = 'yymm00000') then			
			nr_prontuario_novo_w	:= nr_prontuario_w;
			
			-- Caso seja a mascara verifica se ha prontuario para o mes
			select	nvl(max(somente_numero(nr_prontuario)),0)
			into	nr_prontuario_mes_w
			from	paciente_prontuario_anual
			where	ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(dt_referencia)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfYear(sysdate)
			and 	ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_referencia)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(sysdate);
			
			-- Ja tem o prontuario no mes/ano entao acrescentar + 1
			if	(nr_prontuario_mes_w > 0) then
				while (qt_prontuario_w > 0) loop 
					begin				
					nr_prontuario_novo_w	:= nr_prontuario_novo_w + 1;
			
					begin
					select	1
					into	qt_prontuario_w
					from	pessoa_fisica
					where	nr_prontuario	= nr_prontuario_novo_w;
					exception 
					when others then 
						qt_prontuario_w	:= 0;
					end;
					end;
				end loop;
			else
				-- Caso nao exista
				--Busca as 4 primeiras posicoes e soma "1" para a posicao do mes, exemplo: 150100001, pega 1501 soma 1 para gerar 1502
				nr_prontuario_novo_char_w 	:= to_char(to_number(substr(to_char(nr_prontuario_novo_w),1,length(nr_prontuario_novo_w) - 5)) + 1);
				-- Busca as posicoes de ano/mes e concatena com o sequencial, exemplo 1502 concatenado com 00001 para gerar 150200001
				nr_prontuario_novo_w		:= to_number(nr_prontuario_novo_char_w || substr(nr_prontuario_novo_w,length(nr_prontuario_novo_w)-4, length(nr_prontuario_novo_w)));
				
				while (qt_prontuario_w > 0) loop 
					begin						
					nr_prontuario_novo_w	:= nr_prontuario_novo_w + 1;
			
					begin
					select	1
					into	qt_prontuario_w
					from	pessoa_fisica
					where	nr_prontuario	= nr_prontuario_novo_w;
					exception 
					when others then 
						qt_prontuario_w	:= 0;
					end;		
				
					end;
				end loop;
				
			end if;
		
		end if;

		select	paciente_prontuario_anual_seq.nextval
		into	nr_seq_prontuario_w
		from	dual;

		insert into paciente_prontuario_anual
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_referencia,
			ds_mascara,
			nr_prontuario,
			ie_deletar)
		values	(nr_seq_prontuario_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			ds_mascara_w,
			nr_prontuario_novo_w,
			'S');				
	elsif	(nr_prontuario_w = 0) then
		-- Caso nao exista prontuario para o ano busca o ultimo gerado para saber a mascara
		open c01;
		loop
		fetch c01 into	ds_mascara_w;
		exit when c01%notfound;
			begin
			ds_mascara_w	:= ds_mascara_w;
			end;
		end loop;
		close c01;

		if	(ds_mascara_w is not null) then
			if	(ds_mascara_w = 'yymm00000') then	
				begin
				-- Gera a primeira sequencia para o ano
				nr_prontuario_novo_w	:= to_number(to_char(sysdate,'yy') || to_char(sysdate,'mm') || '00000');
				nr_prontuario_novo_w	:= nr_prontuario_novo_w - 1;
				
				while (qt_prontuario_w > 0) loop 
					begin						
					nr_prontuario_novo_w	:= nr_prontuario_novo_w + 1;
			
					begin
					select	1
					into	qt_prontuario_w
					from	pessoa_fisica
					where	nr_prontuario	= nr_prontuario_novo_w;
					exception 
					when others then 
						qt_prontuario_w	:= 0;
					end;		
				
					end;
				end loop;
				
				insert into paciente_prontuario_anual
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_referencia,
					ds_mascara,
					nr_prontuario,
					ie_deletar)
				values	(paciente_prontuario_anual_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					ds_mascara_w,
					nr_prontuario_novo_w,
					'N');
				end;
			end if;
		end if;
	end if;
end if;

--commit;

nr_prontuario_p	:= nr_prontuario_novo_w;

end gerar_prontuario_anual;
/
