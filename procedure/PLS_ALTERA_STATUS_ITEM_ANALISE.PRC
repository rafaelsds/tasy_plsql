create or replace
procedure pls_altera_status_item_analise
				(	nr_seq_audit_item_p	Number,
					nm_usuario_p		Varchar2) is 

nr_seq_requisicao_w			Number(10);
cd_procedimento_w			Number(15);
cd_material_ops_w			Varchar2(255);

begin

begin
	select	b.nr_seq_requisicao,
		a.cd_procedimento,
		substr(pls_obter_seq_codigo_material(a.nr_seq_material,''),1,10)
	into	nr_seq_requisicao_w,
		cd_procedimento_w,
		cd_material_ops_w
	from	pls_auditoria		b,
		pls_auditoria_item	a
	where	a.nr_sequencia		= nr_seq_audit_item_p
	and	a.nr_seq_auditoria	= b.nr_sequencia;
exception
when others then
	nr_seq_requisicao_w	:= 0;
	cd_procedimento_w	:= null;
	cd_material_ops_w	:= null;
end;

if	(nvl(nr_seq_requisicao_w,0)	<> 0) then
	update	pls_auditoria_item
	set	ie_status	= 'P',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_audit_item_p;
	
	pls_requisicao_gravar_hist(	nr_seq_requisicao_w, 'L', 'O auditor '||substr(obter_nome_usuario(nm_usuario_p),1,255)||
					' alterou o status do item '||nvl(cd_procedimento_w,cd_material_ops_w)||' para Pendente em '||
					to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'), null, nm_usuario_p);
end if;

commit;

end pls_altera_status_item_analise;
/