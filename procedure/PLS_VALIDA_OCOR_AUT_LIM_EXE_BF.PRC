/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Procedure utilizada para validar a quantidade limite de execu��o por benefici�rio, esta rotina � 
	utilizada para gera��o de ocorr�ncia na Autoriza��o / Requisi��o / Execu��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
create or replace
procedure pls_valida_ocor_aut_lim_exe_bf
			(	nr_seq_ocor_combinada_p		Number,
				nr_seq_ocorrencia_p		Number,
				nr_seq_segurado_p		Number,
				nr_seq_motivo_glosa_p		Number,
				nr_seq_guia_p			Number,
				nr_seq_requisicao_p		Number,
				nr_seq_execucao_p		Number,
				ie_utiliza_filtro_p		Varchar2,
				nr_seq_param1_p			Number,
				nr_seq_param2_p			Number,
				nr_seq_param3_p			Number,
				nr_seq_param4_p			Number,
				nr_seq_param5_p			Number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number) is


nr_seq_ocor_lim_benef_w		Number(10);
nr_seq_prestador_w		Number(10);
nr_seq_guia_proc_w		Number(10);
nr_seq_req_proc_w		Number(10);
nr_seq_exec_item_w		Number(10);
nr_seq_plano_w			Number(10);
nr_seq_segurado_w		Number(10);
ie_preco_w			Varchar2(255);
ie_regra_w			Varchar2(255);
ie_gerar_ocorrencia_w		Varchar2(255);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
ie_gerou_ocor_cabecalho_w	Varchar2(255);
ie_tipo_ocorrencia_w		Varchar2(255);
nr_seq_requisicao_w		Number(10);
nr_seq_oc_benef_w		Number(10);
qt_solicitada_w			Number(10);
ie_valida_execucao_w		Varchar2(1);

Cursor C01 is
	select	nr_seq_ocor_lim_benef,
		ie_valida_execucao
	from	pls_validacao_aut_lim_ben
	where	nr_seq_ocor_aut_combinada	= nr_seq_ocor_combinada_p
	and	ie_situacao			= 'A';
	
Cursor C02 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_solicitada
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p;
	
Cursor C03 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_solicitado
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C04 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_item
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	nr_seq_material		is null;

begin

if	(nr_seq_guia_p	is not null) then
	begin
		select	nr_seq_prestador,
			nr_seq_segurado
		into	nr_seq_prestador_w,
			nr_seq_segurado_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;
	exception
	when others then
		nr_seq_prestador_w	:= null;
	end;
elsif	(nr_seq_requisicao_p	is not null) then
	begin
		select	nr_seq_prestador,
			nr_seq_segurado
		into	nr_seq_prestador_w,
			nr_seq_segurado_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;
	exception
	when others then
		nr_seq_prestador_w	:= null;
	end;
elsif	(nr_seq_execucao_p	is not null) then
	begin
		select	nr_seq_prestador,
			nr_seq_requisicao
		into	nr_seq_prestador_w,
			nr_seq_requisicao_w
		from	pls_execucao_requisicao
		where	nr_sequencia	= nr_seq_execucao_p;
	exception
	when others then
		nr_seq_prestador_w	:= null;
		nr_seq_requisicao_w	:= null;
	end;
	
	begin
		select	nr_seq_segurado
		into	nr_seq_segurado_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
	exception
	when others then
		nr_seq_segurado_w	:= null;
	end;
end if;

begin
	select	nr_seq_plano
	into	nr_seq_plano_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
exception
when others then
	nr_seq_plano_w	:= null;
end;

begin
	select	ie_preco
	into	ie_preco_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_w;
exception
when others then
	ie_preco_w	:= null;
end;

open C01;
loop
fetch C01 into	
	nr_seq_ocor_lim_benef_w,
	ie_valida_execucao_w;
exit when C01%notfound;
	begin
	if	(nr_seq_guia_p	is not null) then
		open C02;
		loop
		fetch C02 into	
			nr_seq_guia_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_solicitada_w;
		exit when C02%notfound;
			begin
			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				/* Tratamento para filtros */
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, nr_seq_guia_proc_w, null,
								null, null, null,
								cd_procedimento_w, ie_origem_proced_w, null,
								ie_gerou_ocor_cabecalho_w,null, null,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
								
				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;
			
			if	(ie_gerar_ocorrencia_w	= 'S') then
				ie_gerar_ocorrencia_w	:= pls_obter_se_limite_exec_benef(	nr_seq_ocor_lim_benef_w, nr_seq_guia_p, nr_seq_requisicao_p,
												nr_seq_execucao_p, nr_seq_segurado_w, cd_procedimento_w,
												ie_origem_proced_w, ie_preco_w, qt_solicitada_w,
												null, null, null,
												null, null);
												
				if	(ie_gerar_ocorrencia_w	= 'S') then
					pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
								nr_seq_guia_p, null, nr_seq_guia_proc_w, 
								null, nr_seq_ocor_combinada_p, nm_usuario_p, 
								null, nr_seq_motivo_glosa_p, 1, 
								cd_estabelecimento_p, 'N' ,null,
								nr_seq_oc_benef_w, null,
								null, null, null);
							
					pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
									nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
									nr_seq_guia_proc_w, null, null,
									null, null, null,
									nm_usuario_p, cd_estabelecimento_p);
				end if;
			end if;
			end;
		end loop;
		close C02;
	elsif	(nr_seq_requisicao_p	is not null) then
		open C03;
		loop
		fetch C03 into	
			nr_seq_req_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_solicitada_w;
		exit when C03%notfound;
			begin
			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				/* Tratamento para filtros */
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, null, null,
								nr_seq_req_proc_w, null, null,
								cd_procedimento_w, ie_origem_proced_w, null,
								ie_gerou_ocor_cabecalho_w,null, null,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
								
				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;
			
			if	(ie_gerar_ocorrencia_w	= 'S') then
				ie_gerar_ocorrencia_w	:= pls_obter_se_limite_exec_benef(	nr_seq_ocor_lim_benef_w, nr_seq_guia_p, nr_seq_requisicao_p,
												nr_seq_execucao_p, nr_seq_segurado_w, cd_procedimento_w,
												ie_origem_proced_w, ie_preco_w, qt_solicitada_w,
												ie_valida_execucao_w, null, null, 
												null, null);
												
				if	(ie_gerar_ocorrencia_w	= 'S') then
					pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
								null, null, nr_seq_req_proc_w, 
								null, nr_seq_ocor_combinada_p, nm_usuario_p, 
								null, nr_seq_motivo_glosa_p, 5, 
								cd_estabelecimento_p, 'N' ,null,
								nr_seq_oc_benef_w, null,
								null, null, null);
							
					pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
									nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
									null, null, nr_seq_req_proc_w,
									null, null, null,
									nm_usuario_p, cd_estabelecimento_p);
				end if;
			end if;
			end;
		end loop;
		close C03;
	elsif	(nr_seq_execucao_p	is not null) then
		open C04;
		loop
		fetch C04 into	
			nr_seq_exec_item_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_solicitada_w;
		exit when C04%notfound;
			begin
			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				/* Tratamento para filtros */
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, null, null,
								null, null, nr_seq_exec_item_w,
								cd_procedimento_w, ie_origem_proced_w, null,
								ie_gerou_ocor_cabecalho_w,null, null,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);
								
				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;
			
			if	(ie_gerar_ocorrencia_w	= 'S') then
				ie_gerar_ocorrencia_w	:= pls_obter_se_limite_exec_benef(	nr_seq_ocor_lim_benef_w, nr_seq_guia_p, nr_seq_requisicao_p,
												nr_seq_execucao_p, nr_seq_segurado_w, cd_procedimento_w,
												ie_origem_proced_w, ie_preco_w, qt_solicitada_w,
												null, null, null, 
												null, null);
												
				if	(ie_gerar_ocorrencia_w	= 'S') then
					pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
								null, null, nr_seq_exec_item_w, 
								null, nr_seq_ocor_combinada_p, nm_usuario_p, 
								null, nr_seq_motivo_glosa_p, 10, 
								cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
								nr_seq_oc_benef_w, null,
								null, null, null);
						
					pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
									nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
									null, null, null,
									null, nr_seq_exec_item_w, null,
									nm_usuario_p, cd_estabelecimento_p);
				end if;
			end if;
			end;
		end loop;
		close C04;
	end if;
	end;
end loop;
close C01;

commit;

end pls_valida_ocor_aut_lim_exe_bf;
/
