create or replace 
procedure Calcular_Agua_Nut_Pac_Pediat(	
				nr_sequencia_p     	number,
				nm_usuario_p        varchar2) is

qt_peso_w					number(15,3);	
nr_seq_elem_agua_w			number(10);
qt_vol_total_w				number(15,4);
qt_aporte_hidrico_diario_w	number(15,4);
qt_elem_kg_dia_w			number(15,4);
qt_peso_calorico_w			number(15,2);
ie_unid_med_w				varchar2(15);

begin

select	qt_peso,
		qt_aporte_hidrico_diario,
		nvl(qt_peso_calorico,0)
into	qt_peso_w,
		qt_aporte_hidrico_diario_w,
		qt_peso_calorico_w
from	Nut_pac
where	nr_sequencia = nr_sequencia_p;

/*Rotina para atualizar o volume do elemento do tipo �gua para ficar compat�vel com Aporte h�drico */
select	max(a.nr_sequencia),
		max(a.ie_unid_med)
into	nr_seq_elem_agua_w,
		ie_unid_med_w
from	nut_elemento b,
		nut_pac_elemento a
where	a.nr_seq_nut_pac	= nr_sequencia_p
and		a.nr_seq_elemento	= b.nr_sequencia
and		b.ie_tipo_elemento	= 'A'
and		b.ie_npt_pediatrica	= 'S';

if	(nr_seq_elem_agua_w > 0) then
	begin
	select	nvl(sum(obter_vol_elem_nut_pac(nr_sequencia)),0)
	into	qt_vol_total_w	
	from	nut_pac_elemento
	where	nr_seq_nut_pac	= nr_sequencia_p;

	if	(qt_aporte_hidrico_diario_w > qt_vol_total_w) then
		begin
		
		select	sum(qt_elem_kg_dia)
		into	qt_elem_kg_dia_w
		from	nut_pac_elemento
		where	nr_seq_nut_pac	= nr_sequencia_p
		and		nr_sequencia	= nr_seq_elem_agua_w;
		
		if	(upper(ie_unid_med_w) = upper(OBTER_UNID_MED_USUA('Kg/d'))) or
			(ie_unid_med_w is null) then
			qt_elem_kg_dia_w	:= qt_elem_kg_dia_w + dividir((qt_aporte_hidrico_diario_w - qt_vol_total_w),qt_peso_w);
		elsif	(upper(ie_unid_med_w) = upper(OBTER_UNID_MED_USUA('PC/d'))) then
			qt_elem_kg_dia_w	:= qt_elem_kg_dia_w + dividir((qt_aporte_hidrico_diario_w - qt_vol_total_w),qt_peso_calorico_w);
		elsif	(upper(ie_unid_med_w) = upper(OBTER_UNID_MED_USUA('ml'))) then
			qt_elem_kg_dia_w	:= qt_elem_kg_dia_w + dividir(qt_aporte_hidrico_diario_w - qt_vol_total_w,qt_peso_w);
		end if;
		
		update	nut_pac_elemento
		set		qt_elem_kg_dia	=  nvl(qt_elem_kg_dia_w,0)
		where	nr_seq_nut_pac	= nr_sequencia_p
		and		nr_sequencia	= nr_seq_elem_agua_w;

		end;
	elsif	(qt_aporte_hidrico_diario_w < qt_vol_total_w) then
		begin
		select	sum(qt_elem_kg_dia)
		into	qt_elem_kg_dia_w
		from	nut_pac_elemento
		where	nr_seq_nut_pac	= nr_sequencia_p
		and		nr_sequencia	= nr_seq_elem_agua_w;
		
		if	(upper(ie_unid_med_w) = upper(OBTER_UNID_MED_USUA('Kg/d'))) or
			(ie_unid_med_w is null) then
			qt_elem_kg_dia_w	:= qt_elem_kg_dia_w - (dividir((qt_vol_total_w - qt_aporte_hidrico_diario_w),qt_peso_w));
		elsif	(upper(ie_unid_med_w) = upper(OBTER_UNID_MED_USUA('PC/d'))) then
			qt_elem_kg_dia_w	:= qt_elem_kg_dia_w - (dividir((qt_vol_total_w - qt_aporte_hidrico_diario_w),qt_peso_calorico_w));
		elsif	(upper(ie_unid_med_w) = upper(OBTER_UNID_MED_USUA('ml'))) then
			qt_elem_kg_dia_w	:= qt_elem_kg_dia_w - (dividir((qt_vol_total_w - qt_aporte_hidrico_diario_w),qt_peso_w));
		end if;
		
		if	(qt_elem_kg_dia_w < 0) then
			qt_elem_kg_dia_w	:= 0;
		end if;
		
		update	nut_pac_elemento
		set		qt_elem_kg_dia	= nvl(qt_elem_kg_dia_w,0)
		where	nr_seq_nut_pac	= nr_sequencia_p
		and		nr_sequencia	= nr_seq_elem_agua_w;
	
		end;
	end if;
	end;
end if;

commit;

end Calcular_Agua_Nut_Pac_Pediat;
/
