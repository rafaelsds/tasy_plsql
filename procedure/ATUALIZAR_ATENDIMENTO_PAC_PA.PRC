create or replace
procedure atualizar_atendimento_pac_pa( nr_atendimento_p 	number,
															ds_observacao_p	varchar2,
															nm_usuario_p		Varchar2 ) is 

begin
if (nr_atendimento_p is not null) and
	(ds_observacao_p is not null) then
	update 	atendimento_paciente 
   set 		ds_obs_pa 			= 	ds_observacao_p,
				nm_usuario			=	nm_usuario_p,
				dt_atualizacao		=	sysdate
   where 	nr_atendimento 	= 	nr_atendimento_p;
end if;

commit;

end atualizar_atendimento_pac_pa;
/