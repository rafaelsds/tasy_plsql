create or replace
procedure proj_excluir_cron_etapa(
			nr_seq_etapa_p	number,
			nm_usuario_p	Varchar2) is 

nr_seq_interno_w	number(10);
nr_seq_superior_w	number(10);
nr_seq_cronograma_w	number(10);


begin

select	max(nr_seq_interno),
	max(nr_seq_cronograma),
	max(nr_seq_superior)
into	nr_seq_interno_w,
	nr_seq_cronograma_w,
	nr_seq_superior_w
from	proj_cron_etapa
where	nr_sequencia = nr_seq_etapa_p;

update	proj_cron_etapa
set	nr_seq_interno = nr_seq_interno - 1,
	nr_predecessora = nr_predecessora - 1
where	nr_seq_interno >= nr_seq_interno_w
and	nr_Seq_cronograma = nr_seq_cronograma_w;

commit;

/*
update	proj_cron_etapa
set	nr_predecessora = nr_predecessora - 1
where	nr_seq_interno >= nr_seq_interno_w +1;
*/
commit;

delete	from proj_cron_etapa
where	nr_sequencia = nr_seq_etapa_p;

commit;

end proj_excluir_cron_etapa;
/
