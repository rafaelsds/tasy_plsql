CREATE OR REPLACE PROCEDURE create_mhr_doc_meta (
	nm_usuario_p				IN	mhr_doc_meta.nm_usuario%TYPE ,
	cd_pessoa_fisica_p			IN	mhr_doc_meta.cd_pessoa_fisica%TYPE ,
	nr_atendimento_p			IN	mhr_doc_meta.nr_atendimento%TYPE ,
	ie_doc_type_p				IN	mhr_doc_meta.ie_doc_type%TYPE ,
	ie_upload_p					IN	mhr_doc_meta.ie_upload%TYPE ,
	ie_status_p					IN	mhr_doc_meta.ie_status%TYPE ,
	ie_origin_mhr_p				IN	mhr_doc_meta.ie_origin_mhr%TYPE ,
	dt_doc_created_p			IN	mhr_doc_meta.dt_doc_created%TYPE ,
	cd_doc_mhr_p				IN	mhr_doc_meta.cd_doc_mhr%TYPE ,
	cd_doc_tasy_p				IN	mhr_doc_meta.cd_doc_tasy%TYPE ,
	cd_version_p				IN	mhr_doc_meta.cd_version%TYPE ,
	cd_hash_p					IN	mhr_doc_meta.cd_hash%TYPE ,
	ie_valid_hash_p				IN	mhr_doc_meta.ie_valid_hash%TYPE ,
	ie_cda_package_p			IN	mhr_doc_meta.ie_cda_package%TYPE ,
	ds_doc_file_path_p			IN	mhr_doc_meta.ds_doc_file_path%TYPE ,
	ds_organization_p			IN	mhr_doc_meta.ds_organization%TYPE ,
	ds_organization_type_p		IN	mhr_doc_meta.ds_organization_type%TYPE ,
	ds_author_p					IN	mhr_doc_meta.ds_author%TYPE ,
	ds_params_p					IN	mhr_doc_meta.ds_params%TYPE ,
	nr_sequencia_p				OUT	mhr_doc_meta.nr_sequencia%TYPE 
) IS


nr_sequencia_w		mhr_doc_meta.nr_sequencia%TYPE;
ie_status_w			mhr_doc_meta.ie_status%TYPE;
cd_doc_tasy_w		mhr_doc_meta.cd_doc_tasy%TYPE; 
nr_atendimento_w	mhr_doc_meta.nr_atendimento%TYPE;

BEGIN

	if  (cd_pessoa_fisica_p is not null) and
		(dt_doc_created_p is not null) and
		(ds_author_p is not null) and
		(cd_doc_mhr_p is not null or cd_doc_tasy_p is not null) then

			if (ie_doc_type_p = 'DS' or ie_doc_type_p = 'ES') then
				if (ie_upload_p = 'S' and cd_doc_tasy_p is null) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1092252);
					return;
				elsif (ie_upload_p = 'N' and cd_doc_mhr_p is null) then
					Wheb_mensagem_pck.exibir_mensagem_abort(1092252);
					return;
				end if;
			elsif (ie_origin_mhr_p <> 'S' or ie_upload_p <> 'N' or (ie_status_p not in ('DN', 'IA'))) then 
				Wheb_mensagem_pck.exibir_mensagem_abort(1092252);
				return;
			end if;
			
			select 	max(nr_sequencia)
			into 	nr_sequencia_w
			from 	mhr_doc_meta
			where 	cd_doc_mhr = cd_doc_mhr_p;		

			nr_atendimento_w := nr_atendimento_p;
			if	(nr_atendimento_w = 0) 	then
				nr_atendimento_w := null;
			end if;
			
			-- If there is a record with given mhr document id, then it is a duplicate. Instead of creating new record, update the existing record.
			if	(nr_sequencia_w is not null)	then

				select	ie_status,
					nvl(cd_doc_tasy,0)
				into ie_status_w,
					cd_doc_tasy_w
				from	mhr_doc_meta
				where	nr_sequencia = nr_sequencia_w;
				
				if (cd_doc_tasy_w <> 0) and
					(ie_status_p = 'DN' or ie_status_p = 'IA') and
					(ie_status_w = 'UP' or ie_status_w = 'SC') then	
					
					-- If an already uploaded record is downloaded, there is no need to update the record.
					nr_sequencia_w := 0;
					return;
				end if; 

				if (cd_doc_tasy_w = 0) and
					(ie_status_p = 'DN' or ie_status_p = 'IA') and
					(ie_status_w = 'DN') then
						
					-- If an already downloaded record is downloaded again, there is no need to update the record.
					nr_sequencia_w := 0;
					return;
				end if;			
			
				update_mhr_doc_status (nm_usuario_p, ie_status_p, cd_pessoa_fisica_p, nr_sequencia_w, cd_doc_mhr_p, cd_doc_tasy_p,
										cd_version_p, cd_hash_p, ie_valid_hash_p, ie_cda_package_p, ds_doc_file_path_p, 
										ds_organization_p, ds_organization_type_p, ds_author_p, ds_params_p);
				return;
			end if;
		
			select	mhr_doc_meta_seq.nextval
			into	nr_sequencia_w
			from 	dual;

			INSERT INTO mhr_doc_meta 
				(nr_sequencia, nm_usuario, cd_pessoa_fisica, nr_atendimento, ie_doc_type, ie_upload, ie_status, ie_origin_mhr, 
				dt_doc_created, cd_doc_mhr, cd_doc_tasy, cd_version, cd_hash, ie_valid_hash, 
				ie_cda_package, ds_doc_file_path, ds_organization, ds_organization_type, ds_author, 
				ds_params, dt_atualizacao, dt_atualizacao_nrec, nm_usuario_nrec, ie_consent_withdrawn ) 
			VALUES 
				(nr_sequencia_w, nm_usuario_p, cd_pessoa_fisica_p, nr_atendimento_w, ie_doc_type_p, ie_upload_p, ie_status_p, ie_origin_mhr_p, 
				dt_doc_created_p, cd_doc_mhr_p, cd_doc_tasy_p, cd_version_p, cd_hash_p, ie_valid_hash_p, 
				ie_cda_package_p, ds_doc_file_path_p, ds_organization_p, ds_organization_type_p, ds_author_p, 
				ds_params_p, sysdate, sysdate, nm_usuario_p, 'N' );

			COMMIT;

	end if;

	
nr_sequencia_p := 	nr_sequencia_w;

END create_mhr_doc_meta;
/