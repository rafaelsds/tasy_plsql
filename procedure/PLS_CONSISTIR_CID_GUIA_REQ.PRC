/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Consistir na guia ou na requisi��o a situa��o do CID
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_consistir_cid_guia_req
		(	nr_seq_guia_p		number,
			nr_seq_requisicao_p	number,
			nr_seq_prestador_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 
			
CD_DOENCA_w		varchar2(10);
qt_registro_w		number(10);
			
Cursor C01 is
	select	CD_DOENCA
	from	PLS_DIAGNOSTICO
	where	nr_seq_guia	= nr_seq_guia_p
	and	nr_seq_requisicao_p is null
	and	cd_doenca is not null
	union all
	select	CD_DOENCA
	from	PLS_REQUISICAO_DIAGNOSTICO
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	and	nr_seq_guia_p is null
	and	cd_doenca is not null;
			
begin

open C01;
loop
fetch C01 into	
	CD_DOENCA_w;
exit when C01%notfound;
	begin
	
	select	count(1)
	into	qt_registro_w
	from	cid_doenca
	where	upper(cd_doenca_cid)	= upper(CD_DOENCA_w)
	and	nvl(ie_situacao,'A')	= 'A'
	and	rownum			= 1;
	
	if	(qt_registro_w = 0) then
		if	(nr_seq_guia_p is not null) then
			pls_gravar_motivo_glosa('1509', nr_seq_guia_p, null,null,'CID ' || substr(obter_desc_cid(CD_DOENCA_w),1,50) || ' n�o existe ou est� inativo', 
						nm_usuario_p,'', 'CG', nr_seq_prestador_p,'',
						null);

		elsif	(nr_seq_requisicao_p is not null) then
			pls_gravar_requisicao_glosa('1509', nr_seq_requisicao_p, null,null,'CID ' || substr(obter_desc_cid(CD_DOENCA_w),1,50) || ' n�o existe ou est� inativo', 
							nm_usuario_p,nr_seq_prestador_p, cd_estabelecimento_p, null,'');
		end if;
	end if;
	
	end;
end loop;
close C01;


end pls_consistir_cid_guia_req;
/
