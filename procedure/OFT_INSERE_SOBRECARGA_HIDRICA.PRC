create or replace
procedure oft_insere_sobrecarga_hidrica (	nr_seq_consulta_p			number,
														nr_seq_consulta_form_p	number,
														vListaSobrecargaHidrica		strRecTypeFormOft) is

nr_sequencia_w				oft_sobrecarga_hidrica.nr_sequencia%type;
dt_exame_w					oft_sobrecarga_hidrica.dt_registro%type;
ds_observacao_w			oft_sobrecarga_hidrica.ds_observacao%type;
nr_medicao_previa_od_w	oft_sobrecarga_hidrica.nr_medicao_previa_od%type;
nr_medicao_previa_oe_w	oft_sobrecarga_hidrica.nr_medicao_previa_oe%type;
dt_medicao_previa_w		oft_sobrecarga_hidrica.dt_medicao_previa%type;
qt_agua_w					oft_sobrecarga_hidrica.qt_agua%type;
nr_prim_medicao_w			oft_sobrecarga_hidrica.nr_prim_medicao%type;
nr_prim_medicao_od_w		oft_sobrecarga_hidrica.nr_prim_medicao_od%type;
nr_prim_medicao_oe_w		oft_sobrecarga_hidrica.nr_prim_medicao_oe%type;
dt_prim_medicao_w			oft_sobrecarga_hidrica.dt_prim_medicao%type := sysdate;
nr_seg_medicao_od_w		oft_sobrecarga_hidrica.nr_seg_medicao_od%type;
nr_seg_medicao_w			oft_sobrecarga_hidrica.nr_seg_medicao%type;
nr_seg_medicao_oe_w		oft_sobrecarga_hidrica.nr_seg_medicao_oe%type;	
dt_seg_medicao_w			oft_sobrecarga_hidrica.dt_seg_medicao%type := sysdate;
nr_ter_medicao_w			oft_sobrecarga_hidrica.nr_ter_medicao%type;
nr_ter_medicao_od_w		oft_sobrecarga_hidrica.nr_ter_medicao_od%type;
nr_ter_medicao_oe_w		oft_sobrecarga_hidrica.nr_ter_medicao_oe%type;
dt_ter_medicao_w			oft_sobrecarga_hidrica.dt_ter_medicao%type := sysdate;
nr_qua_medicao_w			oft_sobrecarga_hidrica.nr_qua_medicao%type;
nr_qua_medicao_od_w		oft_sobrecarga_hidrica.nr_qua_medicao_od%type;
nr_qua_medicao_oe_w		oft_sobrecarga_hidrica.nr_qua_medicao_oe%type;
dt_qua_medicao_w			oft_sobrecarga_hidrica.dt_qua_medicao%type  := sysdate;
dt_ingestao_hidrica_w	oft_sobrecarga_hidrica.dt_ingestao_hidrica%type := sysdate;
cd_profissional_w			oft_sobrecarga_hidrica.cd_profissional%type;	
nm_usuario_w				usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
ie_registrado_w			varchar2(1) := 'N';
ds_erro_w					varchar2(4000);
											
begin
begin

if	(nvl(nr_seq_consulta_p,0) > 0) and (vListaSobrecargaHidrica.count > 0) then
	for i in 1..vListaSobrecargaHidrica.count loop
		begin
		if	(vListaSobrecargaHidrica(i).ds_valor is not null) or 
			(vListaSobrecargaHidrica(i).nr_valor is not null) then
			case upper(vListaSobrecargaHidrica(i).nm_campo)
				when 'CD_PROFISSIONAL' then
					cd_profissional_w							:= vListaSobrecargaHidrica(i).ds_valor;
				when 'DT_REGISTRO' then
					dt_exame_w 									:= pkg_date_utils.get_DateTime(vListaSobrecargaHidrica(i).ds_valor);
				when 'DS_OBSERVACAO' then
					ds_observacao_w							:= vListaSobrecargaHidrica(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'NR_MEDICAO_PREVIA_OD' then
					nr_medicao_previa_od_w					:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'NR_MEDICAO_PREVIA_OE' then
					nr_medicao_previa_oe_w					:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'DT_MEDICAO_PREVIA' then
					dt_medicao_previa_w						:= pkg_date_utils.get_DateTime(vListaSobrecargaHidrica(i).ds_valor);
				when 'QT_AGUA' then
					qt_agua_w									:= vListaSobrecargaHidrica(i).nr_valor;
				when 'NR_PRIM_MEDICAO' then
					nr_prim_medicao_w							:= vListaSobrecargaHidrica(i).nr_valor;
				when 'NR_PRIM_MEDICAO_OD' then
					nr_prim_medicao_od_w						:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'NR_PRIM_MEDICAO_OE' then
					nr_prim_medicao_oe_w						:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'DT_PRIM_MEDICAO' then
					dt_prim_medicao_w							:= pkg_date_utils.get_DateTime(vListaSobrecargaHidrica(i).ds_valor);
				when 'NR_SEG_MEDICAO_OD' then
					nr_seg_medicao_od_w						:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'NR_SEG_MEDICAO' then
					nr_seg_medicao_w							:= vListaSobrecargaHidrica(i).nr_valor;
				when 'NR_SEG_MEDICAO_OE' then
					nr_seg_medicao_oe_w						:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'DT_SEG_MEDICAO' then
					dt_seg_medicao_w							:= pkg_date_utils.get_DateTime(vListaSobrecargaHidrica(i).ds_valor);
				when 'NR_TER_MEDICAO' then
					nr_ter_medicao_w							:= vListaSobrecargaHidrica(i).nr_valor;
				when 'NR_TER_MEDICAO_OD' then
					nr_ter_medicao_od_w						:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'NR_TER_MEDICAO_OE' then
					nr_ter_medicao_oe_w						:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'DT_TER_MEDICAO' then
					dt_ter_medicao_w							:= pkg_date_utils.get_DateTime(vListaSobrecargaHidrica(i).ds_valor);
				when 'NR_QUA_MEDICAO' then
					nr_qua_medicao_w							:= vListaSobrecargaHidrica(i).nr_valor;
				when 'NR_QUA_MEDICAO_OD' then
					nr_qua_medicao_od_w						:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'NR_QUA_MEDICAO_OE' then
					nr_qua_medicao_oe_w						:= vListaSobrecargaHidrica(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'DT_QUA_MEDICAO' then
					dt_qua_medicao_w							:= pkg_date_utils.get_DateTime(vListaSobrecargaHidrica(i).ds_valor);
				when 'DT_INGESTAO_HIDRICA' then
					dt_ingestao_hidrica_w					:= pkg_date_utils.get_DateTime(vListaSobrecargaHidrica(i).ds_valor);
				else
					null;	
			end case;	
		end if;	
	end;
	end loop;
	
	select	max(nr_sequencia)
	into		nr_sequencia_w
	from		oft_sobrecarga_hidrica
	where		nr_seq_consulta_form = nr_seq_consulta_form_p
	and		nr_seq_consulta		= nr_seq_consulta_p
	and		dt_liberacao 			is null
	and		nm_usuario				= nm_usuario_w;
	
	if	(nr_prim_medicao_od_w is null) and (nr_prim_medicao_oe_w is null) then
		dt_prim_medicao_w := null;
	end if;
	
	if	(nr_seg_medicao_od_w is null) and (nr_seg_medicao_oe_w is null) then
		dt_seg_medicao_w := null;
	end if;
	
	if	(nr_ter_medicao_od_w is null) and (nr_ter_medicao_oe_w is null) then
		dt_ter_medicao_w := null;
	end if;
	
	if	(nr_qua_medicao_od_w is null) and (nr_qua_medicao_oe_w is null) then
		dt_qua_medicao_w := null;
	end if;
	
	if	(nr_sequencia_w is not null) then
		update	oft_sobrecarga_hidrica
		set		dt_atualizacao 			= sysdate,
					nm_usuario 					= nm_usuario_w, 
					cd_profissional			= nvl(cd_profissional_w,cd_profissional),	
					dt_registro 				= nvl(dt_exame_w,sysdate), 
					ds_observacao 				= ds_observacao_w,
					nr_medicao_previa_od 	= nr_medicao_previa_od_w,
					nr_medicao_previa_oe 	= nr_medicao_previa_oe_w, 
					dt_medicao_previa 		= dt_medicao_previa_w,
					qt_agua 						= nvl(qt_agua_w,qt_agua),
					nr_prim_medicao 			= nvl(nr_prim_medicao_w,nr_prim_medicao),
					nr_prim_medicao_od 		= nr_prim_medicao_od_w,
					nr_prim_medicao_oe 		= nr_prim_medicao_oe_w, 
					dt_prim_medicao 			= dt_prim_medicao_w,
					nr_seg_medicao_od 		= nr_seg_medicao_od_w,
					nr_seg_medicao 			= nvl(nr_seg_medicao_w,nr_seg_medicao),
					nr_seg_medicao_oe 		= nr_seg_medicao_oe_w,
					dt_seg_medicao 			= dt_seg_medicao_w,
					nr_ter_medicao 			= nvl(nr_ter_medicao_w,nr_ter_medicao),
					nr_ter_medicao_od 		= nr_ter_medicao_od_w,
					nr_ter_medicao_oe 		= nr_ter_medicao_oe_w, 
					dt_ter_medicao 			= dt_ter_medicao_w,
					nr_qua_medicao 			= nvl(nr_qua_medicao_w,nr_qua_medicao),
					nr_qua_medicao_od 		= nr_qua_medicao_od_w,
					nr_qua_medicao_oe 		= nr_qua_medicao_oe_w, 
					dt_qua_medicao 			= dt_qua_medicao_w,
					dt_ingestao_hidrica 		= dt_ingestao_hidrica_w
		where		nr_sequencia				= nr_sequencia_w;
		wheb_usuario_pck.set_ie_commit('S');
	else
		if	(ie_registrado_w = 'S') then
			wheb_usuario_pck.set_ie_commit('S');
			select	oft_sobrecarga_hidrica_seq.nextval
			into		nr_sequencia_w	
			from		dual;

			insert	into oft_sobrecarga_hidrica
													(	nr_sequencia, 
														dt_atualizacao, 
														nm_usuario, 
														dt_atualizacao_nrec, 
														nm_usuario_nrec, 
														cd_profissional, 
														dt_registro,
														nr_seq_consulta, 
														ie_situacao,
														ds_observacao,
														nr_medicao_previa_od, 
														nr_medicao_previa_oe, 
														dt_medicao_previa, 
														qt_agua, 
														nr_prim_medicao, 
														nr_prim_medicao_od, 
														nr_prim_medicao_oe, 
														dt_prim_medicao, 
														nr_seg_medicao_od, 
														nr_seg_medicao, 
														nr_seg_medicao_oe, 
														dt_seg_medicao, 
														nr_ter_medicao, 
														nr_ter_medicao_od, 
														nr_ter_medicao_oe, 
														dt_ter_medicao, 
														nr_qua_medicao, 
														nr_qua_medicao_od, 
														nr_qua_medicao_oe, 
														dt_qua_medicao, 
														dt_ingestao_hidrica,
														nr_seq_consulta_form
														)
			values								(	nr_sequencia_w, 
														sysdate, 
														nm_usuario_w, 
														sysdate, 
														nm_usuario_w, 
														nvl(cd_profissional_w,obter_pf_usuario(nm_usuario_w,'C')), 
														nvl(dt_exame_w,sysdate), 
														nr_seq_consulta_p,
														'A',
														ds_observacao_w,
														nr_medicao_previa_od_w,
														nr_medicao_previa_oe_w,
														nvl(dt_medicao_previa_w,sysdate),
														nvl(qt_agua_w,1000),
														nvl(nr_prim_medicao_w,15),
														nr_prim_medicao_od_w,
														nr_prim_medicao_oe_w,
														dt_prim_medicao_w,
														nr_seg_medicao_od_w,
														nvl(nr_seg_medicao_w,30),
														nr_seg_medicao_oe_w,
														dt_seg_medicao_w,
														nvl(nr_ter_medicao_w,45),
														nr_ter_medicao_od_w,
														nr_ter_medicao_oe_w,
														dt_ter_medicao_w,
														nvl(nr_qua_medicao_w,60),
														nr_qua_medicao_od_w,
														nr_qua_medicao_oe_w,
														dt_qua_medicao_w,
														dt_ingestao_hidrica_w,
														nr_seq_consulta_form_p);
		end if;												
	end if;											
end if;	

exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,4000);
	update	OFT_CONSULTA_FORMULARIO
	set		ds_stack			=	substr(dbms_utility.format_call_stack||ds_erro_w,1,4000)
	where		nr_sequencia	= 	nr_seq_consulta_form_p;
end;

end oft_insere_sobrecarga_hidrica;
/