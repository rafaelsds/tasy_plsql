create or replace
procedure import_sus_aih_detalhe(cd_detalhe_p	varchar2,
			ds_detalhe_p	varchar2,
			ie_opcao_p	varchar2,
			nm_usuario_p		varchar2) is
 		 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Importar dados AIH
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
ie_opcao_p
IN - insere dado tabela sus_detalhe
UP - update
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
if	(upper(ie_opcao_p) = 'IN') then
	if	(cd_detalhe_p is not null) and
		(ds_detalhe_p is not null) then
			insert	into sus_detalhe 
					(nr_sequencia, 
					cd_detalhe, 
					ds_detalhe, 
					dt_atualizacao, 
					nm_usuario, 
					dt_atualizacao_nrec, 
					nm_usuario_nrec) 
			values (sus_detalhe_seq.nextval, 
					cd_detalhe_p, 
					substr(ds_detalhe_p,1,100), 
					sysdate, 
					nm_usuario_p, 
					sysdate, 
					nm_usuario_p);
	end if;
elsif	(upper(ie_opcao_p) = 'UP') then
	update	sus_detalhe 
	set	ds_detalhe = substr(ds_detalhe_p,1,100), 
		nm_usuario = nm_usuario_p, 
		dt_atualizacao = sysdate
	where	lpad(cd_detalhe,5,0) = lpad(cd_detalhe_p,5,0);
end if;
commit;
end import_sus_aih_detalhe;
/