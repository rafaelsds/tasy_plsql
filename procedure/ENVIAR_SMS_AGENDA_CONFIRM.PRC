create or replace
procedure enviar_sms_agenda_confirm(
		ds_remetente_p		in	varchar2,
		ds_destinatario_p	in	varchar2,
		ds_mensagem_p		in	varchar2,
		cd_agenda_p		in	number,
		nr_seq_agenda_p		in	number,
		ie_confirmar_p		in	varchar2,
		nm_usuario_p		in	varchar2,
		ds_msg_erro_p		out	varchar2) is 

ds_erro_w	varchar2(255) := '';		
		
begin
if	(ds_destinatario_p is not null) and
	(cd_agenda_p is not null) and
	(nr_seq_agenda_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	Enviar_SMS_Agenda_Cons(
		ds_remetente_p,
		ds_destinatario_p,
		ds_mensagem_p,
		cd_agenda_p,
		nr_seq_agenda_p,
		nm_usuario_p,
		ds_erro_w);
	
	if	(ie_confirmar_p = 'S') and (ds_erro_w is null) then
		begin
		
		update	agenda_consulta
		set	dt_confirmacao 		= sysdate,
			nm_usuario_confirm 	= nm_usuario_p
		where	nr_sequencia		= nr_seq_agenda_p;		
		end;		
	end if;	
	
	ds_msg_erro_p := ds_erro_w;
	
	end;
end if;
commit;
end enviar_sms_agenda_confirm;
/
