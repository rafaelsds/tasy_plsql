create or replace
procedure atualizar_log_alt_parametros(
			nm_usuario_p		varchar2,
			cd_estabelecimento_p varchar2,
			cd_estab_param_p 	varchar2,
			cd_estab_param_old_p 	varchar2,
			cd_perfil_param_p	varchar2,
			cd_perfil_param_old_p	varchar2,
			nm_usuario_param_p  varchar2,
			nm_usuario_param_old_p  varchar2,
			nr_seq_param_p		varchar2,
			vl_param_p			varchar2,
			vl_param_old_p			varchar2,
			ie_tipo_param_p		varchar2,
			cd_funcao_p			varchar2) is 

qt_registros_w number;
ie_permite_log_w varchar2(1);
qt_registros_max_w varchar2(10);			
nr_registros_max_w number(10);
nr_seq_ult_w varchar2(20);

begin

obter_param_usuario(6001,153,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_permite_log_w);

if(ie_permite_log_w = 'S') then
begin

	if(ie_tipo_param_p = 'U') then
	begin
		insert into log_alt_parametros_w(nr_sequencia,
			cd_funcao,
			dt_atualizacao,
			nm_usuario_param_atual,
			nm_usuario_param_old,
			nm_usuario,
			vl_parametro_old,
			vl_parametro_atual,
			cd_parametro)
		values(log_alt_parametros_w_seq.nextval,
				cd_funcao_p,
				sysdate,
				nm_usuario_param_p,
				nm_usuario_param_old_p,
				nm_usuario_p,
				vl_param_old_p,
				vl_param_p,
				nr_seq_param_p);
	end;
	elsif (ie_tipo_param_p = 'P') then
	begin
		insert into log_alt_parametros_w(nr_sequencia,
			cd_funcao,
			dt_atualizacao,
			cd_perfil_atual,
			cd_perfil_old,
			nm_usuario,
			vl_parametro_old,
			vl_parametro_atual,
			cd_parametro)
		values(log_alt_parametros_w_seq.nextval,
				cd_funcao_p,
				sysdate,
				cd_perfil_param_p,
				cd_perfil_param_old_p,
				nm_usuario_p,
				vl_param_old_p,
				vl_param_p,
				nr_seq_param_p);
	end;
	elsif (ie_tipo_param_p = 'E') then
	begin
		insert into log_alt_parametros_w(nr_sequencia,
			cd_funcao,
			dt_atualizacao,
			CD_ESTABELECIMENTO_ATUAL,
			CD_ESTABELECIMENTO_OLD,
			nm_usuario,
			vl_parametro_old,
			vl_parametro_atual,
			cd_parametro)
		values(log_alt_parametros_w_seq.nextval,
				cd_funcao_p,
				sysdate,
				cd_estab_param_p,
				cd_estab_param_old_p,
				nm_usuario_p,
				vl_param_old_p,
				vl_param_p,
				nr_seq_param_p);
	end;
	end if;

	
	obter_param_usuario(6001,154,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,qt_registros_max_w);		
	nr_registros_max_w:= to_number(qt_registros_max_w)+1;
	
	
	
	select count(*)
	into qt_registros_w
	from log_alt_parametros_w;

	if(qt_registros_w = nr_registros_max_w) then
		begin
		select min(nr_sequencia) 
		into nr_seq_ult_w
		from log_alt_parametros_w;


		delete from log_alt_parametros_w
		where nr_sequencia = nr_seq_ult_w;
		end;
	end if;

end;
end if;


end atualizar_log_alt_parametros;
/