create or replace
procedure mprev_cancelar_mod_partic(	nr_seq_prog_partic_mod_p number,
					nr_seq_motivo_p		 number,
					nm_usuario_p 		 varchar2) is

nr_seq_ciclo_atend_ant_w	mprev_partic_ciclo_atend.nr_sequencia%type;
nr_seq_ciclo_atend_w		mprev_partic_ciclo_atend.nr_sequencia%type;
dt_inicio_ciclo_w		date;
nr_seq_programa_partic_w	mprev_programa_partic.nr_sequencia%type;
nr_seq_prog_modulo_w		mprev_programa_modulo.nr_sequencia%type;
nr_seq_agrupamento_w		mprev_agrupamento_modulo.nr_sequencia%type;
nr_seq_prog_partic_mod_ant_w	mprev_prog_partic_modulo.nr_sequencia%type;

Cursor C01 (nr_seq_prog_partic_mod_pc  mprev_prog_partic_modulo.nr_sequencia%type) is
	select	a.nr_seq_ciclo,
		a.dt_inicio_ciclo,
		a.nr_seq_modulo,
		a.nr_seq_agrupamento
	from	mprev_partic_ciclo_item_v a
	where	a.nr_seq_prog_partic_mod		= nr_seq_prog_partic_mod_pc
	order by a.nr_sequencia;
					
begin
if	(nr_seq_prog_partic_mod_p is not null) then
	--Grava data de cancelamento do ciclo.
	update	mprev_prog_partic_modulo
	set	dt_cancelamento = sysdate,
		nr_seq_motivo_canc = nr_seq_motivo_p
	where	nr_sequencia = nr_seq_prog_partic_mod_p;
	
	for r_c01 in c01(nr_seq_prog_partic_mod_p) loop
		--Cancela os atendimentos do ciclo de atendimento selecionado
		mprev_cancelar_atend_ciclo(	r_c01.nr_seq_ciclo,
						null,
						null,
						null,
						null,
						'BD',
						nr_seq_motivo_p,
						nm_usuario_p);
		
		nr_seq_prog_modulo_w	:= r_c01.nr_seq_modulo;
		nr_seq_agrupamento_w	:= r_c01.nr_seq_agrupamento;
	end loop;
	
	-- Busca a sequencia do atendimento do ciclo anterior.
	nr_seq_ciclo_atend_ant_w 	:= mprev_obter_seq_ciclo_ant(nr_seq_prog_partic_mod_p);
								
	if	(nvl(nr_seq_ciclo_atend_ant_w, 0) > 0) then
		--Altera o status dos itens do atendimento anterios para Pendente.	
		update	mprev_partic_ciclo_item a
		set	dt_cancelamento = sysdate,
			ie_status = 'P'
		where	nr_seq_partic_ciclo_atend	= nr_seq_ciclo_atend_ant_w
		and	ie_status = 'C'
		and	ie_cancel_troca_mod	= 'S'
		/* N�o voltar atr�s os que j� haviam sido cancelados por bot�o direito */
		and	not exists	(select	1
					from	mprev_prog_partic_modulo x,
						mprev_partic_ciclo_atend y
					where	y.nr_seq_prog_partic_mod	= x.nr_sequencia
					and	y.nr_sequencia			= a.nr_seq_partic_ciclo_atend
					and	x.dt_cancelamento is not null);
	end if;
end if;

commit;

end mprev_cancelar_mod_partic;
/