create or replace
procedure inserir_motivo_permanencia(
			nm_usuario_p		Varchar2,
			nr_atendimento_p	number,
			cd_setor_atendimento_p	number,
			cd_unidade_basica_p	varchar2,
			cd_unidade_compl_p	varchar2,
			nr_seq_motivo_perm_p	number) is 

nr_sequencia_w	number(10);
begin

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from 	atend_paciente_unidade
where	nr_atendimento       = nr_atendimento_p
and 	cd_setor_atendimento = cd_setor_atendimento_p
and 	cd_unidade_basica    = cd_unidade_basica_p
and 	cd_unidade_compl     = cd_unidade_compl_p;

if	(nr_sequencia_w > 0) then
	
	update	atend_paciente_unidade
	set	nr_seq_motivo_perm   = nr_seq_motivo_perm_p,
		nm_usuario           = nm_usuario_p,
		dt_atualizacao       = sysdate
	where	nr_sequencia   = nr_sequencia_w
	and	nr_atendimento = nr_atendimento_p;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end inserir_motivo_permanencia;
/
