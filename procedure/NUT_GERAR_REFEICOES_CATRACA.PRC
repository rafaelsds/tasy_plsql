create or replace
procedure nut_gerar_refeicoes_catraca( dt_refeicao_p		Date,
				nm_usuario_p		Varchar2) is 
			
nr_seq_integracao_w	Number(10);
nr_seq_servico_w	Number(10);
qt_catraca_w		Number(10);
ds_horarios_w		Varchar2(5);
ds_horarios_fim_w	Varchar2(5);
nr_seq_cardapio_dia_w	number(10);
ds_parametro_120_w  varchar2(1);

Cursor C01 is	--obter servico
	
	select 	a.nr_seq_servico,
		b.nr_sequencia,
		a.ds_horarios,
		a.ds_horarios_fim
	from	nut_servico_horario a,
		nut_cardapio_dia b
	where 	((ds_parametro_120_w = 'N' and a.nr_seq_servico = b.nr_seq_servico ) 
        		or (ds_parametro_120_w = 'S' and a.nr_seq_servico = b.nr_seq_servico and nvl(b.nr_seq_cycle,0) > 0)) 
	and	a.ds_horarios is not null
	and	a.ds_horarios_fim is not null
	and	b.dt_cardapio  between inicio_dia(dt_refeicao_p) and fim_dia(dt_refeicao_p)
	order	by 1;

begin

if	(dt_refeicao_p is not null) then
ds_parametro_120_w := obter_valor_param_usuario(1003,120,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);
	select	max(a.nr_sequencia)
	into	nr_seq_integracao_w
	from	nut_integracao_catraca a
	where	a.dt_refeicao between inicio_dia(dt_refeicao_p) and fim_dia(dt_refeicao_p);
	
	if	(nr_seq_integracao_w is not null) then

		open C01;
		loop
		fetch C01 into	
			nr_seq_servico_w,
			nr_seq_cardapio_dia_w,
			ds_horarios_w,
			ds_horarios_fim_w;
		exit when C01%notfound;
		begin

			select	count(*)
			into	qt_catraca_w
			from	nut_integ_catraca_item a
			where	to_char(a.dt_refeicao,'hh24:mi') between ds_horarios_w and ds_horarios_fim_w
			and	a.nr_seq_integracao	= nr_seq_integracao_w;
			
			--atualizar qtd no cardapio
			if ( qt_catraca_w > 0) then
				
				update	nut_cardapio_dia
				set	qt_pessoa_atend_real	= qt_catraca_w
				where	nr_sequencia		= nr_seq_cardapio_dia_w;
					
				gravar_log_nutricao(10, wheb_mensagem_pck.get_texto(802584,
										'QT_CATRACA='||qt_catraca_w||
										';NR_SEQ_CARDAPIO_DIA='||nr_seq_cardapio_dia_w), nm_usuario_p);
			end if;
		end;
		end loop;
		close C01;
	end if;
end if;

commit;

end nut_gerar_refeicoes_catraca;
/
