create or replace
procedure gerar_etapa_conta( nr_interno_conta_p	number,
			ie_evento_p	varchar2,
			nm_usuario_p	varchar2) is
			
qt_existe_w		number(10);
nr_seq_etapa_w		number(10);
nr_interno_conta_w	number(10);
ie_tipo_atendimento_w	number(3);
cd_convenio_w		number(5);
ie_evento_w		varchar2(2);
cd_estabelecimento_w	number(10,0);
nr_seq_classificacao_w	number(10);
cd_setor_atendimento_w	number(5,0);
cd_categoria_w		varchar2(10);
nr_seq_motivo_dev_w	number(10,0);
ie_tipo_convenio_w	number(2,0);
ie_regra_restrita_etapa_w	varchar2(1);
ie_encontrou_regra_w	varchar2(1);
cd_setor_atend_etapa_w	fatur_etapa_alta.cd_setor_atend_etapa%type;

Cursor C01 is
	select	nr_seq_etapa,
		nr_seq_motivo_dev,
		cd_setor_atend_etapa
	from	fatur_etapa_alta
	where	((cd_convenio is null) or (nvl(cd_convenio, nvl(cd_convenio_w,0)) = nvl(cd_convenio_w,0)))
	and	((cd_categoria is null) or (nvl(cd_categoria, nvl(cd_categoria_w,'0')) = nvl(cd_categoria_w,'0')))
	and	((ie_tipo_atendimento is null) or (nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,0)))
	and 	nvl(cd_estabelecimento, nvl(cd_estabelecimento_w,1)) = nvl(cd_estabelecimento_w,1)
	and 	nvl(nr_seq_classificacao, nvl(nr_seq_classificacao_w,0)) = nvl(nr_seq_classificacao_w,0)
	and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
	and	(nvl(ie_evento,'A') = nvl(ie_evento_w,'A'))
	and	nvl(cd_perfil, nvl(obter_perfil_ativo,0)) = nvl(obter_perfil_ativo,0)
	and	nvl(ie_tipo_convenio, nvl(ie_tipo_convenio_w,0)) = nvl(ie_tipo_convenio_w,0)
	and	ie_situacao = 'A'
	order by	nvl(cd_convenio,0),
		nvl(cd_categoria,'0'),
		nvl(cd_setor_atendimento,0),
		nvl(ie_tipo_atendimento,0),
		nvl(ie_tipo_convenio,0),
		nvl(nr_seq_classificacao,0),
		nvl(cd_perfil,0),
		nvl(cd_estabelecimento,0);
	
begin

select	count(*)
into	qt_existe_w
from	fatur_etapa_alta;

select	nvl(max(a.ie_tipo_atendimento),0),
	nvl(max(a.cd_estabelecimento),1),
	nvl(max(a.nr_seq_classificacao),0),
	nvl(max(obter_setor_atendimento(a.nr_atendimento)),0),
	nvl(max(b.cd_convenio_parametro),0),
	nvl(max(b.cd_categoria_parametro),'0'),
	nvl(max(obter_tipo_convenio(b.cd_convenio_parametro)),0)
into	ie_tipo_atendimento_w,
	cd_estabelecimento_w,
	nr_seq_classificacao_w,
	cd_setor_atendimento_w,
	cd_convenio_w,
	cd_categoria_w,
	ie_tipo_convenio_w
from	atendimento_paciente a,
	conta_paciente b
where	a.nr_atendimento = b.nr_atendimento
and	b.nr_interno_conta = nr_interno_conta_p;

select	nvl(max(ie_regra_restrita_etapa),'N')
into	ie_regra_restrita_etapa_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_w;

if (nvl(qt_existe_w,0) > 0) then

	if (nvl(nr_interno_conta_p,0) > 0) then
		
		ie_evento_w 		:= nvl(ie_evento_p,'A');
		ie_encontrou_regra_w	:= 'N';
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_etapa_w,
			nr_seq_motivo_dev_w,
			cd_setor_atend_etapa_w;
		exit when C01%notfound;
			begin
			ie_encontrou_regra_w	:= 'S';
			
			if	(ie_regra_restrita_etapa_w = 'N') then
				gerar_conta_etapa(nr_interno_conta_p, nm_usuario_p, nr_seq_etapa_w, nr_seq_motivo_dev_w, cd_setor_atend_etapa_w, null);
			end if;
			end;
		end loop;
		close C01;
		
		if	(ie_regra_restrita_etapa_w = 'S') and (ie_encontrou_regra_w = 'S') then
			gerar_conta_etapa(nr_interno_conta_p, nm_usuario_p, nr_seq_etapa_w, nr_seq_motivo_dev_w, cd_setor_atend_etapa_w, null);
		end if;		
	end if;
	
end if;

commit;

end gerar_etapa_conta;
/