create or replace
procedure accept_and_justify_fortify (	nr_sequencia_p		number,
									ds_analysis_p 	varchar2) is

begin

	begin
	
		update fortify
		set 	ie_status = 'A',
				ds_analysis = ds_analysis_p
		where 	nr_sequencia = nr_sequencia_p;
	
	end;

commit;

end accept_and_justify_fortify;
/ 
 