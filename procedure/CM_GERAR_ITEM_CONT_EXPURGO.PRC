create or replace
procedure cm_gerar_item_cont_expurgo(
			nr_seq_item_p		number,
			nr_seq_ciclo_p		number,
			nr_seq_lote_p		number,
			nr_expurgo_retirada_p	number,
			nr_expurgo_receb_p	number,
			cd_setor_atendimento_p	number,
			nm_usuario_barras_p	varchar2,
			cd_estabelecimento_p	number) is

nr_seq_ciclo_w		number(10);
nr_seq_lote_w		number(10);
nr_seq_embalagem_w	number(10);
qt_ponto_w		number(15,2);
nr_sequencia_w		number(10);
cd_pessoa_resp_w	varchar2(10);
ie_gera_nr_controle_w     varchar2(1);
ie_util_adm_lote_w          varchar2(1);
nr_seq_controle_w   cm_conjunto_cont.nr_seq_controle%type;

begin

nr_seq_ciclo_w := nvl(nr_seq_ciclo_p,0);
nr_seq_lote_w := nvl(nr_seq_lote_p,0);
cd_pessoa_resp_w := obter_pf_usuario(nm_usuario_barras_p,'C');

select	nr_seq_embalagem,
		nvl(qt_ponto, 0)
into	nr_seq_embalagem_w,
		qt_ponto_w
from	cm_item
where	nr_sequencia = nr_seq_item_p;

select	cm_conjunto_cont_seq.nextval
into	nr_sequencia_w
from	dual;

obter_param_usuario(406,2,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_gera_nr_controle_w);
obter_param_usuario(406,3,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_util_adm_lote_w);

if (ie_gera_nr_controle_w = 'S' and ie_util_adm_lote_w = 'N') then
    select nvl(max(nr_seq_controle),0) + 1 qt_max
    into nr_seq_controle_w
    from cm_conjunto_cont;
end if; 

insert into cm_conjunto_cont(
	nr_sequencia,
	nr_seq_item,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	ie_status_conjunto,
	dt_origem,
	nm_usuario_origem,
	nr_seq_embalagem,
	dt_receb_ester,
	nm_usuario_ester,
	nr_seq_ciclo,
	vl_esterilizacao,
	qt_ponto,
	cd_setor_atendimento,
	nr_seq_lote,
	ie_situacao,
	cd_pessoa_resp,
	nr_seq_controle)
values (nr_sequencia_w,
	nr_seq_item_p,
	cd_estabelecimento_p,
	sysdate,
	nm_usuario_barras_p,
	2,
	sysdate,
	nm_usuario_barras_p,
	nr_seq_embalagem_w,
	sysdate,
	nm_usuario_barras_p,
	decode(nr_seq_ciclo_w,0,null,nr_seq_ciclo_w),
	0,
	qt_ponto_w,
	cd_setor_atendimento_p,
	decode(nr_seq_lote_w,0,null,nr_seq_lote_w),
	'A',
	cd_pessoa_resp_w,
	nr_seq_controle_w);

if	(nr_expurgo_retirada_p is not null) then
	update	cm_expurgo_retirada
	set	nr_conjunto_cont = nr_sequencia_w
	where	nr_sequencia = nr_expurgo_retirada_p;
end if;

if	(nr_expurgo_receb_p is not null) then
	update	cm_expurgo_receb
	set	nr_conjunto_cont = nr_sequencia_w
	where	nr_sequencia = nr_expurgo_receb_p;
end if;


cme_incluir_itens_controle(nr_sequencia_w,nm_usuario_barras_p);

/* commit nao e necessario, pois ja esta no cme_incluir_itens_controle */

end cm_gerar_item_cont_expurgo;
/
