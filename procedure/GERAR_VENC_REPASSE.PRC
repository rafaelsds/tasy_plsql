CREATE OR REPLACE
PROCEDURE Gerar_Venc_Repasse(
				nr_repasse_terceiro_p  		NUMBER,
				ie_gerar_tributos_p		varchar2,
				nm_usuario_p			VARCHAR2,
				ie_commit_p			varchar2) IS


dt_base_w			Date;
cd_cond_pagto_w			Number(10,0);
vl_repasse_w			Number(15,2);
vl_parcela_w			Number(15,2);
vl_tot_venc_w			Number(15,2);
dt_parcela_w			Date;
nr_sequencia_w			Number(10,0);
nr_seq_venc_w			Number(10,0);
cd_estabelecimento_w		Number(05,0);
--pr_ir_w			Number(15,4);			-- Edgar 16/03/2006 OS 31477, o IR n�o � mais calculado aqui
vl_ir_w				Number(15,2);
pr_imp_munic_w			Number(15,4) := 0;
vl_imposto_munic_w		Number(15,2) := 0;
qt_venc_w			Number(05,0);
ds_venc_w			Varchar2(2000);
i				Number(05,0);
cd_beneficiario_w		Varchar2(14);
ie_trib_venc_repasse_w		Varchar2(1);
ie_nota_fiscal_w		number(5) := 0;
ie_venc_retroativo_w 		varchar2(1);
vl_total_proc_repasse_w		Number(15,2);
vl_total_mat_repasse_w		Number(15,2);
vl_total_item_repasse_w		Number(15,2);
vl_parcela_proc_w		Number(15,2);
vl_parcela_mat_w		Number(15,2);
vl_parcela_item_w		Number(15,2);
vl_adiantamento_w		Number(15,2);
vl_parcela_adiant_w		Number(15,2);
vl_tot_adiant_w			Number(15,2);
vl_desp_cartao_mat_w		material_repasse.vl_desp_cartao%type;
vl_desp_cartao_proc_w		procedimento_repasse.vl_desp_cartao%type;

cursor c01 is
select	nr_sequencia
from	repasse_terceiro_venc
where	nr_repasse_terceiro	= nr_repasse_terceiro_p
and	ie_trib_venc_repasse_w	= 'S'
and	ie_gerar_tributos_p	= 'S';

BEGIN

select	nvl(max(dt_base_vencto), nvl(max(dt_periodo_final),sysdate)),
	max(a.cd_condicao_pagamento),
	obter_valor_repasse(max(a.nr_repasse_terceiro),'L'),
	max(a.cd_estabelecimento)
into	dt_base_w,
	cd_cond_pagto_w,
	vl_repasse_w,
	cd_estabelecimento_w
from	terceiro b,
	repasse_terceiro a
where	a.nr_repasse_terceiro	= nr_repasse_terceiro_p
and	a.nr_seq_terceiro		= b.nr_sequencia;

select	nvl(sum(vl_liberado),0),
	nvl(sum(nvl(vl_desp_cartao,0)),0)
into	vl_total_proc_repasse_w,
	vl_desp_cartao_proc_w
from 	procedimento_repasse
where 	nr_repasse_terceiro	= nr_repasse_terceiro_p;

select	nvl(sum(vl_liberado),0),
	nvl(sum(nvl(vl_desp_cartao,0)),0)
into	vl_total_mat_repasse_w,
	vl_desp_cartao_mat_w
from	material_repasse
where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

select 	nvl(sum(decode(nvl(nr_adiant_pago,0),0,vl_repasse,0)),0) vl_total_item_repasse,
	nvl(sum(decode(nvl(nr_adiant_pago,0),0,0,vl_repasse)),0) vl_adiantamento
into	vl_total_item_repasse_w,
	vl_adiantamento_w
from	repasse_terceiro_item
where 	nr_repasse_terceiro	= nr_repasse_terceiro_p;

vl_repasse_w	:= nvl(vl_repasse_w,0) - nvl(vl_adiantamento_w,0);

BEGIN
select	max(cd_beneficiario_ir)
into	cd_beneficiario_w
from	parametros_contas_pagar
where	cd_estabelecimento		= cd_estabelecimento_w;
exception
when no_data_found then
	--r.aise_application_error(-20011, 'Par�metros do contas a pagar n�o cadastrados para o estabelecimento ' || cd_estabelecimento_w ||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(267371,'cd_estabelecimento_w='||cd_estabelecimento_w);
end;

/* if	(cd_beneficiario_w is null) then
	pr_ir_w			:= 0;
end if; */

/*	Jacson OS 42053	 - consistir se j� tiver nota fiscal gerada para o repasse	*/
/*	Paulo OS 56796 - alterado para consistir somente se a nota estiver "ativa"	*/
select	nvl(max(nr_seq_nota_fiscal),0)
into	ie_nota_fiscal_w
from	repasse_nota_fiscal a,
	nota_fiscal b
where	a.nr_seq_nota_fiscal	= b.nr_sequencia
and	b.ie_situacao		= '1'
and	a.nr_repasse_terceiro	= nr_repasse_terceiro_p;

if	(ie_nota_fiscal_w > 0) then
	--r.aise_application_error(-20011,	'O repasse j� possui nota fiscal. Os vencimentos n�o podem ser gerados!'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(267372);
end if;

delete from repasse_terc_venc_trib
where	ie_pago_prev	<> 'P'  -- Edgar 19/09/2007, deletar somente previstos
and	nr_seq_rep_venc in (
	select	nr_sequencia
	from	repasse_terceiro_venc
	where	nr_repasse_terceiro	= nr_repasse_terceiro_p);

delete from repasse_terceiro_venc
where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

if	(cd_cond_pagto_w is null) then
	--r.aise_application_error(-20011,'N�o foi informada a condi��o de pagamento'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(267373);
end if;
calcular_vencimento(cd_estabelecimento_w, cd_cond_pagto_w,
		dt_base_w, qt_venc_w, ds_venc_w);

select	max(a.ie_venc_retroativo)
into	ie_venc_retroativo_w
from	parametro_repasse a
where	a.cd_estabelecimento	= cd_estabelecimento_w;

if	(qt_venc_w > 0) then
	vl_tot_venc_w		:= 0;
	vl_tot_adiant_w		:= 0;
	vl_parcela_w 		:= dividir(vl_repasse_w, qt_venc_w);
	vl_parcela_proc_w	:= dividir(vl_total_proc_repasse_w, qt_venc_w);
	vl_parcela_mat_w	:= dividir(vl_total_mat_repasse_w, qt_venc_w);	
	vl_parcela_item_w	:= dividir(vl_total_item_repasse_w, qt_venc_w);
	vl_parcela_adiant_w	:= dividir(vl_adiantamento_w, qt_venc_w);
	vl_desp_cartao_mat_w	:= dividir(vl_desp_cartao_mat_w, qt_venc_w);
	vl_desp_cartao_proc_w	:= dividir(vl_desp_cartao_proc_w, qt_venc_w);
	FOR i IN 1..qt_venc_w	LOOP 
     		dt_parcela_w	:= to_date(substr(ds_venc_w,1,10),'dd/mm/yyyy');

		if	(dt_parcela_w < sysdate) and (ie_venc_retroativo_w = 'N') then
			--r.aise_application_error(-20011,'N�o � poss�vel gerar vencimento com data inferior a data atual. Data: ' || dt_parcela_w ||'#@#@');
			wheb_mensagem_pck.exibir_mensagem_abort(267374,'dt_parcela_w='||dt_parcela_w);
		end if;

		ds_venc_w	:= substr(ds_venc_w,12,255);
/*		vl_ir_w		:= vl_parcela_w	* pr_ir_w / 100; */

		vl_ir_w		:= 0;

		if	(i = qt_venc_w) then
			vl_parcela_w		:= vl_repasse_w - vl_tot_venc_w;
			vl_parcela_adiant_w	:= vl_adiantamento_w - vl_tot_adiant_w;
		end if;

		select	repasse_terceiro_venc_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert	into repasse_terceiro_venc(
			nr_sequencia,
			nr_repasse_terceiro,
			vl_vencimento,
			dt_vencimento,
			dt_atualizacao,
			nm_usuario,
			nr_titulo,
			pr_ir,
			vl_ir,
			pr_imp_munic,
			vl_imposto_munic,
			vl_liquido,
			vl_repasse_proc,
			vl_repasse_mat,
			vl_repasse_item,
			vl_adiantamento,
			vl_desp_cartao)
		values(	nr_sequencia_w,
			nr_repasse_terceiro_p,
			vl_parcela_w,
			dt_parcela_w,
			sysdate,
			nm_usuario_p,
			null,
			0,
			vl_ir_w,
			pr_imp_munic_w,
			vl_imposto_munic_w,
			vl_parcela_w,
			vl_parcela_proc_w,
			vl_parcela_mat_w,
			vl_parcela_item_w,
			vl_parcela_adiant_w,
			vl_desp_cartao_mat_w + vl_desp_cartao_proc_w);
		
		vl_tot_venc_w	:= vl_tot_venc_w + vl_parcela_w;
		vl_tot_adiant_w	:= vl_tot_adiant_w + vl_parcela_adiant_w;
	END LOOP;
end if;

select	IE_TRIB_VENC_REPASSE
into	ie_trib_venc_repasse_w
from	parametro_faturamento
where	cd_estabelecimento		= cd_estabelecimento_w;

open c01;
loop
fetch c01 into
	nr_seq_venc_w;
exit when c01%notfound;
	Gerar_Tributo_Venc_Repasse(nr_seq_venc_w);
end loop;
close c01;

if	(nvl(ie_commit_p,'S') <> 'N') then
	commit;
end if;

END Gerar_Venc_Repasse;
/
