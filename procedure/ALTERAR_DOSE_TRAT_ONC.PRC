create or replace
procedure alterar_dose_trat_onc(
				nr_seq_paciente_p	number,
				NR_SEQ_MATERIAL_p	number,
				nm_usuario_p		Varchar2) is 

cd_estabelecimento_w		number(4);
cd_material_w			number(10);
nr_agrupamento_w		number(10);
nr_seq_diluicao_w		number(10);
cd_material_diluido_w		number(10);
nr_seq_atendimento_w		number(10);
nr_seq_material_w		number(10);
				
cursor c01 is
	select	*
	from	paciente_protocolo_medic
	where	nr_seq_paciente		= nr_seq_paciente_p
	and	nr_seq_material		= nr_seq_material_p;
	
c01_w	c01%rowtype;				
	
cursor c02 is
	select	a.nr_seq_atendimento,
		b.nr_seq_material
	from	paciente_atendimento a,
		paciente_atend_medic b
	where	nr_seq_diluicao_w is null
	and	a.nr_seq_atendimento	= b.nr_seq_atendimento
	and	a.nr_seq_paciente	= nr_seq_paciente_p
	--and	a.nr_seq_atendimento	<> nr_seq_atendimento_p
	and	b.cd_material		= cd_material_w
	and	b.nr_agrupamento	= NR_AGRUPAMENTO_w
	and	a.dt_liberacao_reg is null	
	and	b.nr_seq_diluicao is null
	and	a.nr_prescricao	is null
	union all
	select	a.nr_seq_atendimento,
		d.nr_seq_material
	from	paciente_atendimento a,
		paciente_atend_medic m,
		paciente_atend_medic d
	where	nr_seq_diluicao_w is not null
	and	a.nr_seq_atendimento	= m.nr_seq_atendimento
	and	d.nr_seq_atendimento	= a.nr_seq_atendimento
	and	d.nr_seq_diluicao	= m.nr_seq_material
	and	a.dt_liberacao_reg is null	
	and	a.nr_seq_paciente	= nr_seq_paciente_p
	--and	a.nr_seq_atendimento	<> nr_seq_atendimento_p
	and	m.cd_material		= cd_material_diluido_w
	and	d.nr_agrupamento	= NR_AGRUPAMENTO_w
	and	d.cd_material		= cd_material_w
	and	a.nr_prescricao	is null;
	
	
begin
select	nvl(max(b.cd_estabelecimento),1)
into	cd_estabelecimento_w
from	paciente_setor		b
where	b.nr_seq_paciente = nr_seq_paciente_p;

wheb_usuario_pck.set_ie_executar_trigger('N');

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	cd_material_w		:= c01_w.cd_material;
	nr_agrupamento_w	:= c01_w.nr_agrupamento;
	nr_seq_diluicao_w	:= c01_w.nr_seq_diluicao;	
	
	cd_material_diluido_w := null;
	if	(nr_seq_diluicao_w is not null) then
		select	cd_material
		into	cd_material_diluido_w
		from	paciente_protocolo_medic
		where	nr_seq_paciente = nr_seq_paciente_p
		and	nr_seq_material	= nr_seq_diluicao_w;
	end if;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_atendimento_w,
		nr_seq_material_w;
	exit when C02%notfound;
		begin	
		
		update	paciente_atend_medic
		set	QT_DOSE				= c01_w.QT_DOSE,
			IE_VIA_APLICACAO	= c01_w.IE_VIA_APLICACAO,
			IE_SE_NECESSARIO	= c01_w.IE_SE_NECESSARIO,
			IE_URGENCIA			= c01_w.IE_URGENCIA,
			CD_INTERVALO		= c01_w.CD_INTERVALO,
			QT_DIAS_UTIL		= c01_w.QT_DIAS_UTIL,
			IE_APLIC_BOLUS		= c01_w.IE_APLIC_BOLUS,
			QT_HORA_APLICACAO	= c01_w.QT_HORA_APLICACAO,
			QT_MIN_APLICACAO	= c01_w.QT_MIN_APLICACAO,
			IE_BOMBA_INFUSAO	= c01_w.IE_BOMBA_INFUSAO,
			IE_APLIC_LENTA		= c01_w.IE_APLIC_LENTA,
			IE_PRE_MEDICACAO	= c01_w.IE_PRE_MEDICACAO,
			IE_APLICA_REDUCAO	= c01_w.IE_APLICA_REDUCAO,
			PR_REDUCAO			= c01_w.PR_REDUCAO,
			CD_UNID_MED_DOSE	= c01_w.CD_UNIDADE_MEDIDA,
			CD_UNID_MED_PRESCR 	= c01_w.CD_UNID_MED_PRESCR,
			QT_DOSE_PRESCRICAO	= c01_w.QT_DOSE_PRESCR,
			ie_acm				= c01_w.ie_acm
		where	nr_seq_atendimento	= nr_seq_atendimento_w
		and	nr_seq_material		= nr_seq_material_w;
		
		/*if ( qt_dose_prescr_atend_w = 1) then
			atualizar_dose_onc_ciclo(nr_seq_atendimento_w,'S');
			atualiza_w := True;
		end if;*/
			
		end;
		
	end loop;
	close C02;
	
	AJUSTAR_DIA_MEDIC_TRAT_CONC(nr_seq_paciente_p, c01_w.nr_seq_interno, nm_usuario_p);
	
	end;
end loop;
close C01;

wheb_usuario_pck.set_ie_executar_trigger('S');
commit;

end alterar_dose_trat_onc;
/