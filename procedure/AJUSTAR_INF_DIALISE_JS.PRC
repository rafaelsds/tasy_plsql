create or replace 
procedure ajustar_inf_dialise_js(	nr_prescricao_p		Number,
									nr_seq_solucao_p	Number,
									nr_ocorrencia_p		Number,
									dt_inicio_prescr_p	Date,
									qt_hora_sessao_p	Number,
									--qt_material_p		Number,
									nm_usuario_p		Varchar2) is

nr_etapas_inter_w			Number(10,2);
ds_horarios_w				Varchar2(2000);  
ds_horarios2_w				Varchar2(2000);
ds_erro_w					Varchar2(2000);
cd_material_w				prescr_material.cd_material%type;
nr_sequencia_w				prescr_material.nr_sequencia%type;
ie_via_aplicacao_w			prescr_material.ie_via_aplicacao%type;
qt_unitaria_w				prescr_material.qt_unitaria%type;
qt_material_w				prescr_material.qt_material%type;
qt_material_ww				prescr_material.qt_material%type;
ie_origem_inf_w				prescr_material.ie_origem_inf%type;
cd_unidade_medida_dose_w	prescr_material.cd_unidade_medida_dose%type;
qt_total_dispensar_w		prescr_material.qt_total_dispensar%type;
ie_regra_disp_w				prescr_material.ie_regra_disp%type;
cd_estabelecimento_w		prescr_medica.cd_estabelecimento%type;
ie_agrupador_w				prescr_material.ie_agrupador%type;
ds_horarios_ww				prescr_solucao.ds_horarios%type;
hr_prim_horario_w			prescr_solucao.hr_prim_horario%type;

cursor c01 is
select	b.cd_material,
		b.nr_sequencia,
		b.ie_via_aplicacao,
		b.qt_unitaria,
		b.qt_material,
		b.ie_origem_inf,
		b.cd_unidade_medida_dose,
		a.cd_estabelecimento,
		b.ie_agrupador
from	prescr_material b,
		prescr_medica a
where	a.nr_prescricao = b.nr_prescricao
and		b.nr_prescricao = nr_prescricao_p
and		b.nr_sequencia_solucao = nr_seq_solucao_p
and		b.ie_agrupador = 13;

begin


nr_etapas_inter_w	:= ceil(qt_hora_sessao_p/nr_ocorrencia_p);

calcula_horarios_etapas(dt_inicio_prescr_p, nr_ocorrencia_p, nr_etapas_inter_w, nm_usuario_p, '', qt_hora_sessao_p, ds_horarios_w, ds_horarios2_w, 'N', 'S');

ds_horarios_ww := ds_horarios_w || ds_horarios2_w;

if (ds_horarios_ww is not null) then
	hr_prim_horario_w := obter_prim_dshorarios(ds_horarios_ww);
end if;

update	prescr_solucao
set		nr_etapas = nr_ocorrencia_p,
		ds_horarios = ds_horarios_ww,
		hr_prim_horario = nvl(hr_prim_horario_w,hr_prim_horario)
where	nr_prescricao = nr_prescricao_p
and		nr_seq_solucao = nr_seq_solucao_p;

open C01;
loop
fetch C01 into
	cd_material_w,
	nr_sequencia_w,
	ie_via_aplicacao_w,
	qt_unitaria_w,
	qt_material_w,
	ie_origem_inf_w,
	cd_unidade_medida_dose_w,
	cd_estabelecimento_w,
	ie_agrupador_w;
exit when C01%notfound;
	begin

	begin

		qt_material_ww	:= qt_material_w;

		obter_quant_dispensar(cd_estabelecimento_w,	cd_material_w, nr_prescricao_p, nr_sequencia_w, null, ie_via_aplicacao_w, qt_unitaria_w, null, nr_ocorrencia_p, null, ie_origem_inf_w, cd_unidade_medida_dose_w,
								null, qt_material_ww, qt_total_dispensar_w, ie_regra_disp_w, ds_erro_w, 'N', 'N');


		update	prescr_material
		set		nr_ocorrencia = nr_ocorrencia_p,
				qt_material = qt_material_ww,
				qt_total_dispensar = qt_total_dispensar_w,
				ie_regra_disp = ie_regra_disp_w,
				nr_agrupamento = 0
		where	nr_prescricao = nr_prescricao_p
		and		nr_sequencia_solucao = nr_seq_solucao_p
		and		ie_agrupador = 13
		and		nr_sequencia = nr_sequencia_w;

	exception when others then
		null;
	end;


	end;
end loop;
close C01;

commit;

end ajustar_inf_dialise_js;
/