create or replace
procedure pmdm_processar_mat_regra_disp(nr_sequencia_p in number) is

ieaction_w					varchar2(30 char);
ds_cabecalho_j 				philips_json;
ds_json_j 					philips_json;
ds_mensagem_json_j 			philips_json;
material_regra_disp_j 		philips_json;
ds_mensagem_w 				clob;
ie_possui_indice_w			varchar2(1 char);
ds_erro_p					varchar2(4000 char);
ds_erros_p					varchar2(4000 char);
ds_erros_tecnicos_w			varchar2(4000);

nr_seq_regra_disp_w			material_regra_disp.nr_sequencia%type;

k_material_regra_disp		constant varchar2(19 char)	:=	'MATERIAL_REGRA_DISP';
k_number					constant varchar2(6 char)	:=	'number';
k_string					constant varchar2(6 char)	:=	'string';
k_delete					constant varchar2(6 char)	:=	'DELETE';
k_insert					constant varchar2(6 char)	:=	'INSERT';
k_update					constant varchar2(6 char)	:=	'UPDATE';

function obter_material_sistema(cd_material_sistema_p in material.cd_material%type)
	return material.cd_material%type
is
cd_material_sistema_w 	material.cd_material%type;
cd_material_ant_w		material.cd_material%type;
begin
	cd_material_ant_w	:=	obter_material_sistema_pmdm(cd_material_sistema_p);
	if (cd_material_ant_w > 0) then
		cd_material_sistema_w	:=	cd_material_ant_w;
	end if;
	return cd_material_sistema_w;
end obter_material_sistema;

function valor_string(	campo_j in philips_json,
						campo_p in varchar2,
						tabela_p in varchar2,
						campo_atributo_p in varchar2,
						ie_valor_padrao_p in varchar2,
						ie_conversao_p in varchar2 default 'N',
						ie_material_p in varchar2 default 'N')
	return varchar2
is
resultFunction			varchar2(255 char);
cd_material_sistema_w 	material.cd_material%type;
begin
	if campo_j.exist(campo_p) then
		resultFunction	:=	campo_j.get(campo_p).get_string();
	elsif (ie_valor_padrao_p = 'S') then
		resultFunction	:=	pmdm_obter_valor_padrao(	nm_tabela_p => tabela_p,
													nm_atributo_p => campo_atributo_p,
													nr_seq_visao_p => 0,
													ds_valor_retorno_p => resultFunction);
	else
		resultFunction	:=	null;
	end if;

	if (ie_material_p = 'S' and resultFunction is not null) then
		cd_material_sistema_w	:=	obter_material_sistema(resultFunction);
		if (cd_material_sistema_w is null) then
			--'Nao foi cadastrada a conversao para o material codigo #@cd_material_w#@ que veio do sistema externo.'
			ds_erros_p	:=	ds_erros_p || '"' ||	wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 731836,
																	vl_macros_p => 'CD_MATERIAL_W='||resultFunction) ||'",'||chr(10);
		else
			resultFunction	:=	cd_material_sistema_w;
		end if;
	end if;

	processar_atributo_json(nm_atributo_p => campo_atributo_p,
							nm_tabela_p => tabela_p,
							nm_campo_p => campo_p,
							ds_valor_p => resultFunction,
							ie_conversao_p => ie_conversao_p,
							nr_seq_visao_p => 0,
							ds_valor_retorno_p => resultFunction,
							ds_erro_p => ds_erro_p);

	if (ds_erro_p is not null) then
		ds_erros_p := ds_erros_p || '"' || ds_erro_p || '",' ||chr(10);
	end if;

	return resultFunction;
end valor_string;

function valor_number(	campo_j in philips_json,
						campo_p in varchar2,
						tabela_p in varchar2,
						campo_atributo_p in varchar2,
						ie_valor_padrao_p in varchar2,
						ie_conversao_p in varchar2 default 'N',
						ie_material_p in varchar2 default 'N')
	return varchar2
is
resultFunction			number(15,4);
cd_material_sistema_w 	material.cd_material%type;
begin
	if campo_j.exist(campo_p) then
		resultFunction	:=	campo_j.get(campo_p).get_number();
	elsif (ie_valor_padrao_p = 'S') then
		resultFunction	:=	pmdm_obter_valor_padrao(	nm_tabela_p => tabela_p,
													nm_atributo_p => campo_atributo_p,
													nr_seq_visao_p => 0,
													ds_valor_retorno_p => resultFunction);
	else
		resultFunction	:=	null;
	end if;

	if (ie_material_p = 'S' and resultFunction is not null) then
		cd_material_sistema_w	:=	obter_material_sistema(resultFunction);
		if (cd_material_sistema_w is null) then
			--'Nao foi cadastrada a conversao para o material codigo #@cd_material_w#@ que veio do sistema externo.'
			ds_erros_p	:=	ds_erros_p || '"' ||	wheb_mensagem_pck.get_texto(nr_seq_mensagem_p => 731836,
																	vl_macros_p => 'CD_MATERIAL_W='||resultFunction) ||'",'||chr(10);
		else
			resultFunction	:=	cd_material_sistema_w;
		end if;
	end if;

	processar_atributo_json(nm_atributo_p => campo_atributo_p,
							nm_tabela_p => tabela_p,
							nm_campo_p => campo_p,
							ds_valor_p => resultFunction,
							ie_conversao_p => ie_conversao_p,
							nr_seq_visao_p => 0,
							ds_valor_retorno_p => resultFunction,
							ds_erro_p => ds_erro_p);

	if (ds_erro_p is not null) then
		ds_erros_p := ds_erros_p || '"' || ds_erro_p || '",' ||chr(10);
	end if;

	return resultFunction;
end valor_number;

function gerar_linha_update(	ds_tipo_campo_p in varchar2,
								campo_j in philips_json,
								campo_p in varchar2,
								tabela_p in varchar2,
								campo_atributo_p in varchar2,
								ie_busca_padrao_p in varchar2,
								ie_conversao_p in varchar2 default 'N',
								ie_material_p in varchar2 default 'N')
	return varchar2
is
ds_valor_w	varchar2(4000 char);
ds_resultado_w	varchar2(4000 char);
begin
	if campo_j.exist(campo_p) then
		if (ds_tipo_campo_p = k_number) then
			ds_valor_w	:=	valor_number(	campo_j => campo_j,
										campo_p => campo_p,
										tabela_p => tabela_p,
										campo_atributo_p => campo_atributo_p,
										ie_valor_padrao_p => ie_busca_padrao_p,
										ie_conversao_p => ie_conversao_p,
										ie_material_p => ie_material_p);
			ds_valor_w		:=	replace(ds_valor_w, ',', '.');
			ds_resultado_w	:=	campo_atributo_p||'='||ds_valor_w||','||chr(10);
		else
			ds_valor_w	:=	valor_string(	campo_j => campo_j,
										campo_p => campo_p,
										tabela_p => tabela_p,
										campo_atributo_p => campo_atributo_p,
										ie_valor_padrao_p => ie_busca_padrao_p,
										ie_conversao_p => ie_conversao_p,
										ie_material_p => ie_material_p);
			ds_resultado_w	:=	campo_atributo_p||'='''||ds_valor_w||''','||chr(10);
		end if;
		if (ds_valor_w is null) then
			ds_resultado_w	:=	campo_atributo_p||'=null,'||chr(10);
		end if;
	end if;
	return ds_resultado_w;
end gerar_linha_update;

procedure inserir_atualizar_mat_regra_di(	item_j in philips_json,
											ie_operacao in number)
is
	cd_convenio_w				material_regra_disp.cd_convenio%type;
	cd_estabelecimento_w		material_regra_disp.cd_estabelecimento%type;
	cd_intervalo_w				material_regra_disp.cd_intervalo%type;
	cd_material_w				material_regra_disp.cd_material%type;
	cd_perfil_w					material_regra_disp.cd_perfil%type;
	cd_setor_atendimento_w		material_regra_disp.cd_setor_atendimento%type;
	cd_unidade_medida_w			material_regra_disp.cd_unidade_medida%type;
	cd_unid_med_estoque_w		material_regra_disp.cd_unid_med_estoque%type;
	ie_acm_sn_w					material_regra_disp.ie_acm_sn%type;
	ie_dose_menor_w				material_regra_disp.ie_dose_menor%type;
	ie_material_estoque_w		material_regra_disp.ie_material_estoque%type;
	ie_motivo_prescricao_w		material_regra_disp.ie_motivo_prescricao%type;
	ie_regra_w					material_regra_disp.ie_regra%type;
	ie_regra_disp_w				material_regra_disp.ie_regra_disp%type;
	ie_unidade_medida_w			material_regra_disp.ie_unidade_medida%type;
	ie_usuario_def_disp_w		material_regra_disp.ie_usuario_def_disp%type;
	ie_via_aplicacao_w			material_regra_disp.ie_via_aplicacao%type;
	nr_seq_estrut_int_w			material_regra_disp.nr_seq_estrut_int%type;
	pr_margem_w					material_regra_disp.pr_margem%type;
	ds_sql_dinamico_w			varchar2(4000 char);
	ie_existe_registro			varchar2(1 char);
begin
	if (ie_operacao = 1) then
		cd_convenio_w			:=	valor_number(item_j,'cdConvenio',k_material_regra_disp,'CD_CONVENIO','S');
		cd_estabelecimento_w	:=	valor_number(item_j,'cdEstabelecimento',k_material_regra_disp,'CD_ESTABELECIMENTO','S');
		cd_intervalo_w			:=	valor_string(item_j,'cdIntervalo',k_material_regra_disp,'CD_INTERVALO','S');
		cd_material_w			:=	valor_number(item_j,'cdMaterial',k_material_regra_disp,'CD_MATERIAL','N','S','S');
		cd_perfil_w				:=	valor_number(item_j,'cdPerfil',k_material_regra_disp,'CD_PERFIL','S');
		cd_setor_atendimento_w	:=	valor_number(item_j,'cdSetorAtendimento',k_material_regra_disp,'CD_SETOR_ATENDIMENTO','S');
		cd_unidade_medida_w		:=	valor_string(item_j,'cdUnidadeMedida',k_material_regra_disp,'CD_UNIDADE_MEDIDA','S');
		cd_unid_med_estoque_w	:=	valor_string(item_j,'cdUnidMedEstoque',k_material_regra_disp,'CD_UNID_MED_ESTOQUE','S');
		ie_acm_sn_w				:=	valor_string(item_j,'ieAcmSn',k_material_regra_disp,'IE_ACM_SN','S');
		ie_dose_menor_w			:=	valor_string(item_j,'ieDoseMenor',k_material_regra_disp,'IE_DOSE_MENOR','S');
		ie_material_estoque_w	:=	valor_string(item_j,'ieMaterialEstoque',k_material_regra_disp,'IE_MATERIAL_ESTOQUE','S');
		ie_motivo_prescricao_w	:=	valor_string(item_j,'ieMotivoPrescricao',k_material_regra_disp,'IE_MOTIVO_PRESCRICAO','S');
		ie_regra_w				:=	valor_string(item_j,'ieRegra',k_material_regra_disp,'IE_REGRA','S');
		ie_regra_disp_w			:=	valor_string(item_j,'ieRegraDisp',k_material_regra_disp,'IE_REGRA_DISP','S');
		ie_unidade_medida_w		:=	valor_number(item_j,'ieUnidadeMedida',k_material_regra_disp,'IE_UNIDADE_MEDIDA','S');
		ie_usuario_def_disp_w	:=	valor_string(item_j,'ieUsuarioDefDisp',k_material_regra_disp,'IE_USUARIO_DEF_DISP','S');
		ie_via_aplicacao_w		:=	valor_string(item_j,'ieViaAplicacao',k_material_regra_disp,'IE_VIA_APLICACAO','S');
		nr_seq_estrut_int_w		:=	valor_number(item_j,'nrSeqEstrutInt',k_material_regra_disp,'NR_SEQ_ESTRUT_INT','S');
		pr_margem_w				:=	valor_number(item_j,'prMargem',k_material_regra_disp,'PR_MARGEM','S');

		select material_regra_disp_seq.nextval
		into nr_seq_regra_disp_w
		from dual;

		insert into material_regra_disp(
			cd_convenio,
			cd_estabelecimento,
			cd_intervalo,
			cd_material,
			cd_perfil,
			cd_setor_atendimento,
			cd_unidade_medida,
			cd_unid_med_estoque,
			dt_atualizacao,
			dt_atualizacao_nrec,
			ie_acm_sn,
			ie_dose_menor,
			ie_material_estoque,
			ie_motivo_prescricao,
			ie_regra,
			ie_regra_disp,
			ie_unidade_medida,
			ie_usuario_def_disp,
			ie_via_aplicacao,
			nm_usuario,
			nm_usuario_nrec,
			nr_seq_estrut_int,
			nr_sequencia,
			pr_margem)
		values (
			cd_convenio_w,
			cd_estabelecimento_w,
			cd_intervalo_w,
			cd_material_w,
			cd_perfil_w,
			cd_setor_atendimento_w,
			cd_unidade_medida_w,
			cd_unid_med_estoque_w,
			sysdate,
			sysdate,
			ie_acm_sn_w,
			ie_dose_menor_w,
			ie_material_estoque_w,
			ie_motivo_prescricao_w,
			ie_regra_w,
			ie_regra_disp_w,
			ie_unidade_medida_w,
			ie_usuario_def_disp_w,
			ie_via_aplicacao_w,
			'intfarm',
			'intfarm',
			nr_seq_estrut_int_w,
			nr_seq_regra_disp_w,
			pr_margem_w);
	else
		ds_sql_dinamico_w	:=	'update material_regra_disp set '||chr(10);

		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_number,item_j,'cdConvenio',k_material_regra_disp,'CD_CONVENIO','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_number,item_j,'cdEstabelecimento',k_material_regra_disp,'CD_ESTABELECIMENTO','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'cdIntervalo',k_material_regra_disp,'CD_INTERVALO','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_number,item_j,'cdMaterial',k_material_regra_disp,'CD_MATERIAL','N','S','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_number,item_j,'cdPerfil',k_material_regra_disp,'CD_PERFIL','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_number,item_j,'cdSetorAtendimento',k_material_regra_disp,'CD_SETOR_ATENDIMENTO','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'cdUnidadeMedida',k_material_regra_disp,'CD_UNIDADE_MEDIDA','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'cdUnidMedEstoque',k_material_regra_disp,'CD_UNID_MED_ESTOQUE','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'ieAcmSn',k_material_regra_disp,'IE_ACM_SN','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'ieDoseMenor',k_material_regra_disp,'IE_DOSE_MENOR','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'ieMaterialEstoque',k_material_regra_disp,'IE_MATERIAL_ESTOQUE','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'ieMotivoPrescricao',k_material_regra_disp,'IE_MOTIVO_PRESCRICAO','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'ieRegra',k_material_regra_disp,'IE_REGRA','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'ieRegraDisp',k_material_regra_disp,'IE_REGRA_DISP','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_number,item_j,'ieUnidadeMedida',k_material_regra_disp,'IE_UNIDADE_MEDIDA','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'ieUsuarioDefDisp',k_material_regra_disp,'IE_USUARIO_DEF_DISP','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_string,item_j,'ieViaAplicacao',k_material_regra_disp,'IE_VIA_APLICACAO','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_number,item_j,'nrSeqEstrutInt',k_material_regra_disp,'NR_SEQ_ESTRUT_INT','S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || gerar_linha_update(k_number,item_j,'prMargem',k_material_regra_disp,'PR_MARGEM','S');

		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || 'dt_atualizacao = sysdate,'||chr(10);
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || 'nm_usuario = ''intfarm'''||chr(10);

		ds_sql_dinamico_w	:=	ds_sql_dinamico_w || chr(10)||' where nr_sequencia = '||nr_seq_regra_disp_w;

		begin
			if (instr(ds_sql_dinamico_w, 'where') > 1) then
				execute immediate ds_sql_dinamico_w;
			end if;
		exception  when others then
			ds_erros_tecnicos_w := ds_erros_tecnicos_w||'"Erro: '||replace(substr(to_char(sqlerrm),1,2000), '"')||'.",'||chr(10);
		end;
	end if;

	select	max(cd_unidade_medida)
	into	cd_unidade_medida_w
	from	material_regra_disp
	where 	nr_sequencia  = nr_seq_regra_disp_w;

	--cd_unidade_medida_w
	if (cd_unidade_medida_w is not null) then
		select	nvl(max('S'),'N')
		into	ie_existe_registro
		from unidade_medida
		where cd_unidade_medida = cd_unidade_medida_w;

		if (ie_existe_registro = 'N') then

		ds_erros_p := ds_erros_p ||'"'||pmdm_gerar_mensagem(	nm_tabela_p => k_material_regra_disp,
														nm_atributo_p => 'CD_UNIDADE_MEDIDA',
														ds_valor_p => cd_unidade_medida_w,
														ds_erro_p => 'usada e invalida',
														ds_complemento_p => null,
														ds_tabela_opcional_p => null,
														ds_label_atributo_opcional_p => null,
														ds_atributo_opcional_p => null,
														nm_tabela_ref_p => 'UNIDADE_MEDIDA',
														nm_atributo_ref_p => 'DS_UNIDADE_MEDIDA')||'",'||chr(10);
		end if;
	end if;

end	inserir_atualizar_mat_regra_di;

begin

exec_sql_dinamico_bv('TASY','alter trigger MATERIAL_REGRA_DISPBEFIN disable', null);

select	ds_message
into	ds_mensagem_w
from	intpd_fila_transmissao a
where	a.nr_sequencia = nr_sequencia_p;

ds_json_j 			:=	philips_json(ds_mensagem_w);
ds_mensagem_json_j 	:=	philips_json(ds_json_j.get('Mensagem'));
ds_cabecalho_j		:=	philips_json(ds_mensagem_json_j.get('Cabecalho'));
ieaction_w			:=	ds_cabecalho_j.get('IeAction').get_string();

if (ieaction_w = k_insert) then

	material_regra_disp_j	:=	philips_json(ds_mensagem_json_j.get(k_material_regra_disp));
	inserir_atualizar_mat_regra_di(material_regra_disp_j, 1);

elsif (ieaction_w = k_update) then

	nr_seq_regra_disp_w	:=	ds_cabecalho_j.get('cdSequencia').get_string();
	select	nvl(max('S'),'N')
	into	ie_possui_indice_w
	from	material_regra_disp
	where 	nr_sequencia  = nr_seq_regra_disp_w;

	if (ie_possui_indice_w = 'N') then
		ds_erros_p	:=	ds_erros_p || '"Registro nao localizado.",'||chr(10);
	else
		material_regra_disp_j	:=	philips_json(ds_mensagem_json_j.get(k_material_regra_disp));
		inserir_atualizar_mat_regra_di(material_regra_disp_j, 2);
	end if;

elsif (ieaction_w = k_delete) then

	nr_seq_regra_disp_w	:=	ds_cabecalho_j.get('cdSequencia').get_string();
	select	nvl(max('S'),'N')
	into	ie_possui_indice_w
	from	material_regra_disp
	where	nr_sequencia  = nr_seq_regra_disp_w;

	if (ie_possui_indice_w = 'N') then
		ds_erros_p	:=	ds_erros_p || '"Registro nao localizado.",'||chr(10);
	else
		delete material_regra_disp
		where nr_sequencia  = nr_seq_regra_disp_w;
	end if;
else
	ds_erros_p	:=	ds_erros_p || '"Acao nao informada.",'||chr(10);
end if;

if (ds_erros_p is null) then
	update 	intpd_fila_transmissao
	set		ie_status_http = 200,
			ds_message_response =	'{ "Mensagem" : { "Cabecalho" : {'
								||	'"cdSequencia": "'   || nr_seq_regra_disp_w || '", '
								||	'"Servico": "'	       || 'RESPOSTA' ||'",'
								||	'"DataHora": "'        || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
								||	'"Status": "Sucesso" }}}',
			ie_status = 'S'
	where 	nr_sequencia = nr_sequencia_p;
else
	if ds_erros_p is not null then
		ds_erros_p := REGEXP_REPLACE(ds_erros_p,'.'||chr(10)||'",','.",');
		ds_erros_p := substr(ds_erros_p, 0, LENGTH(ds_erros_p) - 2);
	end if;
	if ds_erros_tecnicos_w is not null then
		ds_erros_tecnicos_w := REGEXP_REPLACE(ds_erros_tecnicos_w,chr(10)||'.",','.",');
		ds_erros_tecnicos_w := substr(ds_erros_tecnicos_w, 0, LENGTH(ds_erros_tecnicos_w) - 2);
	end if;
	rollback;
	update 	intpd_fila_transmissao	
	set		ie_status_http = 400,
			ds_message_response =	'{ "Mensagem" : { "Cabecalho" : {'
								||	'"cdSequencia": "'   || nr_seq_regra_disp_w || '", '
								||	'"Servico": "'	       || 'RESPOSTA' ||'",'
								||	'"DataHora": "'        || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
								||	'"Status": "Erro",' || chr(10)
								||	'"Descricao": [' || ds_erros_p || '],' || chr(10)
								||	'"DescricaoTecnica": ['	||	ds_erros_tecnicos_w	||	']}}}',
			ie_status = 'E'
	where 	nr_sequencia = nr_sequencia_p;
end if;

commit;

exec_sql_dinamico_bv('TASY','alter trigger MATERIAL_REGRA_DISPBEFIN enable', null);

exception
	when others then
		begin
			if ds_erros_p is not null then
				ds_erros_p := REGEXP_REPLACE(ds_erros_p,'.'||chr(10)||'",','.",');
				ds_erros_p := substr(ds_erros_p, 0, LENGTH(ds_erros_p) - 2);
			end if;
			if ds_erros_tecnicos_w is not null then
				ds_erros_tecnicos_w := REGEXP_REPLACE(ds_erros_tecnicos_w,chr(10)||'.",','.",');
				ds_erros_tecnicos_w := substr(ds_erros_tecnicos_w, 0, LENGTH(ds_erros_tecnicos_w) - 2);
			end if;
			ds_erro_p := SYS.DBMS_UTILITY.FORMAT_ERROR_STACK();
			ds_erro_p := substr(ds_erro_p, 0, LENGTH(ds_erro_p) - 1);
			ds_erros_tecnicos_w := ds_erros_tecnicos_w||'"'||replace(ds_erro_p,'"')||'"';
			rollback;
			update 	intpd_fila_transmissao	
			set		ie_status_http = 400,
					ds_message_response =	'{ "Mensagem" : { "Cabecalho" : {'
								||	'"cdSequencia": "'   || nr_seq_regra_disp_w || '", '
								||	'"Servico": "'	       || 'RESPOSTA' ||'",'
								||	'"DataHora": "'        || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
								||	'"Status": "Erro",' || chr(10)
								||	'"Descricao": [' || ds_erros_p || '],' || chr(10)
								||	'"DescricaoTecnica": ['	||	ds_erros_tecnicos_w	||	']}}}',
			ie_status = 'E'
			where 	nr_sequencia = nr_sequencia_p;
			commit;
		end;

end pmdm_processar_mat_regra_disp;
/
