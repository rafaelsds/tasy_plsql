create or replace
procedure gerar_retorno_bradesco_240(	nr_seq_cobr_escrit_p	number,
				nm_usuario_p		varchar2) is

nr_seq_reg_t_w		number(10);
nr_seq_reg_u_w		number(10);
nr_titulo_w		number(10);
vl_titulo_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_abatimento_w		number(15,2);
vl_liquido_w		number(15,2);
vl_outras_despesas_w	number(15,2);
dt_liquidacao_w		date;
ds_dt_liquidacao_w		varchar2(8);
ds_titulo_w		varchar2(15);
vl_cobranca_w		number(15,2);
vl_alterar_w		number(15,2);
cd_ocorrencia_w		number(10);
nr_seq_ocorrencia_ret_w	number(10);
ds_nosso_numero_w	varchar2(12);
ie_digito_nosso_w		varchar2(1);

nr_ds_titulo_w		number(15);
nr_nosso_numero_w	varchar(20);
nr_seq_ocorr_motivo_w	number(10);
cd_motivo_w		varchar2(20);
vl_saldo_titulo_w		number(15,2);
ds_dt_credito_banco_w	varchar2(8);
dt_credito_banco_w	date;

cursor c01 is
select	nr_sequencia,
	trim(substr(ds_string,59,15)) ds_titulo,
	lpad(trim(substr(ds_string,38,19)),19,'0') ds_nosso_numero,
	to_number(substr(ds_string,82,15))/100 vl_cobranca,
	substr(ds_string,214,10) cd_motivo,
	lpad(trim(substr(ds_string,57,1)),1,'0') ie_digito_nosso
from	w_retorno_banco
where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
and	substr(ds_string,8,1)	= '3'
and	substr(ds_string,14,1)	= 'T';

begin

delete	from cobranca_escrit_log
where	nr_seq_cobranca	= nr_seq_cobr_escrit_p;

select	max(substr(ds_string,146,8))
into	ds_dt_credito_banco_w
from	w_retorno_banco
where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
and	substr(ds_string,8,1)	= '3'
and	substr(ds_string,14,1)	= 'U';

begin

	dt_credito_banco_w	:= to_date(ds_dt_credito_banco_w,'dd/mm/yyyy');

exception
when others then

	dt_credito_banco_w	:= null;

end;

update	cobranca_escritural
set	dt_credito_bancario	= dt_credito_banco_w
where	nr_sequencia		= nr_seq_cobr_escrit_p;

open c01;
loop
fetch c01 into
	nr_seq_reg_t_w,
	ds_titulo_w,
	ds_nosso_numero_w,
	vl_cobranca_w,
	cd_motivo_w,
	ie_digito_nosso_w;
exit when c01%notfound;
	begin

	vl_alterar_w		:= 0;

	/* encontrar pelo t�tulo externo */
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo_externo		= ds_titulo_w
	and	nvl(nr_titulo_externo,'0')	<> '0';

	if	(nr_titulo_w	is null) then

		select	max(a.nr_titulo)
		into	nr_titulo_w
		from	titulo_receber a
		where	lpad(a.nr_nosso_numero,12,'0')	= ds_nosso_numero_w
		and	nvl(a.nr_nosso_numero,'0')	<> '0';

		/* se n�o econtrou, procura pelo t�tulo no tasy */
		if	(nr_titulo_w is null) then

			nr_ds_titulo_w		:= somente_numero(ds_titulo_w);

			select	max(nr_titulo)
			into	nr_titulo_w
			from	titulo_receber
			where	nr_titulo	= nr_ds_titulo_w;

			if	(nr_titulo_w	is null) then

				nr_nosso_numero_w	:= to_number(ds_nosso_numero_w);

				select	max(a.nr_titulo)
				into	nr_titulo_w
				from	titulo_receber a
				where	a.nr_titulo	= nr_nosso_numero_w;

				if	(nr_titulo_w	is null) then

					select	max(a.nr_titulo)
					into	nr_titulo_w
					from	titulo_receber a
					where	ds_titulo_w like a.nr_titulo || '%';

					if	(nr_titulo_w	is null) then

						select	max(a.nr_titulo)
						into	nr_titulo_w
						from	titulo_receber a
						where	lpad(a.nr_nosso_numero,12,'0')	= ds_nosso_numero_w || ie_digito_nosso_w
						and	nvl(a.nr_nosso_numero,'0')	<> '0';

						if	(nr_titulo_w	is null) then

							nr_nosso_numero_w	:= somente_numero(ds_nosso_numero_w || ie_digito_nosso_w);

							select	max(a.nr_titulo)
							into	nr_titulo_w
							from	titulo_receber a
							where	a.nr_titulo	= nr_nosso_numero_w;

							if	(nr_titulo_w	is null) then

								select	max(a.nr_titulo)
								into	nr_titulo_w
								from	titulo_receber a
								where	somente_numero(a.nr_nosso_numero)	= nr_nosso_numero_w;

							end if;

						end if;

					end if;

				end if;

			end if;

		end if;

	end if;

	/* se encontrou o t�tulo importa, sen�o grava no log */

	if	(nr_titulo_w is not null) then

		select	max(vl_titulo),
			max(vl_saldo_titulo)
		into	vl_titulo_w,
			vl_saldo_titulo_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		nr_seq_reg_u_w := nr_seq_reg_t_w + 1;

		select	nvl(to_number(substr(ds_string,18,15))/100,0),
			nvl(to_number(substr(ds_string,33,15))/100,0),
			nvl(to_number(substr(ds_string,48,15))/100,0),
			nvl(to_number(substr(ds_string,93,15))/100,0),
			nvl(to_number(substr(ds_string,108,15))/100,0),
			decode(dt_credito_banco_w,null,substr(ds_string,146,8),substr(ds_string,138,8)),
			to_number(substr(ds_string,16,2))
		into	vl_acrescimo_w,
			vl_desconto_w,
			vl_abatimento_w,
			vl_liquido_w,
			vl_outras_despesas_w,
			ds_dt_liquidacao_w,
			cd_ocorrencia_w
		from	w_retorno_banco
		where	nr_sequencia	= nr_seq_reg_u_w;

		begin

			dt_liquidacao_w	:= to_date(ds_dt_liquidacao_w,'dd/mm/yyyy');

		exception
		when others then

			dt_liquidacao_w	:= sysdate;

		end;

		select 	max(a.nr_sequencia)
		into	nr_seq_ocorrencia_ret_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco	= 237
		and	a.cd_ocorrencia = cd_ocorrencia_w;

		select	max(a.nr_sequencia)
		into	nr_seq_ocorr_motivo_w
		from	banco_ocorr_motivo_ret a
		where	instr(cd_motivo_w,a.cd_motivo)	> 0
		and	a.nr_seq_escrit_ret		= nr_seq_ocorrencia_ret_w;

		/* tratar acrescimos/descontos */
		if	(nvl(vl_liquido_w,0)	<> 0) and
			(vl_titulo_w		<> vl_liquido_w) then

			vl_alterar_w	:= vl_liquido_w - vl_titulo_w;

			if	(vl_alterar_w > 0) then
				vl_acrescimo_w	:= vl_alterar_w;
			else
				vl_desconto_w	:= abs(vl_alterar_w);
			end if;

		end if;
          
		insert	into titulo_receber_cobr (	nr_sequencia,
							nr_titulo,
							cd_banco,
							vl_cobranca,
							vl_desconto,
							vl_acrescimo,
							vl_despesa_bancaria,
							vl_liquidacao,
							dt_liquidacao,
							dt_atualizacao,
							nm_usuario,
							nr_seq_cobranca,
							nr_seq_ocorrencia_ret,
							nr_seq_ocorr_motivo,
							vl_saldo_inclusao)
					values	(	titulo_receber_cobr_seq.nextval,
							nr_titulo_w,
							237,
							vl_titulo_w,
							vl_desconto_w,
							vl_acrescimo_w,
							vl_outras_despesas_w,
							vl_liquido_w,
							dt_liquidacao_w,
							sysdate,
							nm_usuario_p,
							nr_seq_cobr_escrit_p,
							nr_seq_ocorrencia_ret_w,
							nr_seq_ocorr_motivo_w,
							vl_saldo_titulo_w);
	else

		insert	into cobranca_escrit_log
			(ds_log,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			nr_nosso_numero,
			nr_seq_cobranca,
			nr_sequencia)
		values	('n�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no tasy',
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			ds_nosso_numero_w,
			nr_seq_cobr_escrit_p,
			cobranca_escrit_log_seq.nextval);

	end if;

	end;
end loop;
close c01;

commit;

end gerar_retorno_bradesco_240;
/
