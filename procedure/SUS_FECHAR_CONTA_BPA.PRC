create or replace 
procedure sus_fechar_conta_bpa(	nr_atendimento_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is


ie_fecha_atend_w		varchar2(1);
ds_erro_w			varchar2(2000);
cd_setor_atendimento_w		number(15);

BEGIN

Obter_Param_Usuario(1125,9,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_fecha_atend_w);

begin
select	cd_setor_atendimento
into	cd_setor_atendimento_w
from	atendimento_paciente_v
where	nr_atendimento	= nr_atendimento_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(193532,'NR_ATENDIMENTO='||nr_atendimento_p);
end;

if	(ie_fecha_atend_w	in ('S','D')) then
	Finalizar_Atendimento(nr_atendimento_p,'S',nm_usuario_p,ds_erro_w);
end if;

if	(ie_fecha_atend_w	= 'D') then
	Saida_Setor_Servico(nr_atendimento_p,cd_setor_atendimento_w,null, sysdate, nm_usuario_p);
end if;

end sus_fechar_conta_bpa;
/
