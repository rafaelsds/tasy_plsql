create or replace
procedure checkup_atual_envio_comunic(
		nr_sequencia_p	number,
		nm_usuario_p	varchar2) is

begin
if	(nr_sequencia_p	is not null) and
	(nm_usuario_p is not null) then
	begin
	update	checkup
	set	dt_envio_comunic	= sysdate,
		nm_usuario	 	= nm_usuario_p
	where	nr_sequencia		= nr_sequencia_p
	and	dt_envio_comunic is null;
	end;
end if;
commit;
end checkup_atual_envio_comunic;
/