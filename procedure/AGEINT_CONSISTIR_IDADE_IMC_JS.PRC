create or replace
procedure ageint_consistir_idade_imc_js (
		nr_seq_ageint_p		number,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2,
		ds_erro_p	out	varchar2,
		ds_erro_dur_p	out	varchar2,
		qt_item_p	out	number,
		qt_item_quimio_p out	number) is

begin
	ageint_consistir_idade_imc(nr_seq_ageint_p,cd_estabelecimento_p, null, nm_usuario_p,ds_erro_p);

	select	count(*)
	into	qt_item_p
	from	agenda_integrada_item
	where	nr_seq_agenda_int = nr_seq_ageint_p
	and	ie_tipo_agendamento <> 'Q';

	select	count(*)
	into	qt_item_quimio_p
	from	agenda_integrada_item
	where	nr_seq_agenda_int = nr_seq_ageint_p
	and	ie_tipo_agendamento = 'Q';
	
	select	decode(count(*), 0, 'S', 'N')
	into	ds_erro_dur_p
	from	agenda_integrada_item
	where	nr_seq_agenda_int = nr_seq_ageint_p
	and	ie_tipo_agendamento = 'Q'
	and nvl(nr_minuto_duracao, 0) < 5;

end ageint_consistir_idade_imc_js;
/