create or replace
procedure alterar_dados_painel_html(	nr_seq_agenda_p			number,
													nr_seq_sala_cir_p			number,
													hr_revisada_p				date,
													ds_observacao_p			varchar2,
													nm_usuario_p				varchar2,
													ds_inf_painel_p			varchar2,
													ie_visita_pre_anest_p	varchar2,
													ie_opcao_p					varchar2)
													is
													
ds_historico_w			varchar2(2000);
nr_atendimento_w		number(10);
nr_cirurgia_w			number(10);
ds_sala_antiga_w		varchar2(255);
ds_sala_nova_w			varchar2(255);
													
begin

if	(ie_opcao_p = 'P') then
	select	max(nr_atendimento),
				max(nr_cirurgia)
	into		nr_atendimento_w,
				nr_cirurgia_w
	from		agenda_paciente
	where		nr_sequencia = nr_seq_agenda_p;

	select   max(a.ds_sala_painel)
	into		ds_sala_antiga_w
	from 		sala_cirurgia a,
				agenda_paciente b
	where 	b.nr_seq_sala_cir 	= a.nr_sequencia
	and		b.nr_sequencia 		= nr_seq_agenda_p;
	
	SELECT   max(a.ds_sala_painel)
	into		ds_sala_nova_w
	FROM 		sala_cirurgia a 
	where 	nr_sequencia = nr_seq_sala_cir_p;
	
	alterar_dados_painel(nr_seq_agenda_p,nr_seq_sala_cir_p,hr_revisada_p,ds_observacao_p,nm_usuario_p,ds_inf_painel_p,ie_visita_pre_anest_p);
	
	if	(ds_sala_antiga_w is null) then
		ds_historico_w	:= substr(obter_texto_dic_objeto(354989,wheb_usuario_pck.get_nr_seq_idioma,'DS_AGENDA_ATUAL=' ||ds_sala_nova_w||';'||'NR_AGENDA_ANTIGA='||nr_seq_agenda_p||';'),1,2000);
	else
		ds_historico_w	:= substr(obter_texto_dic_objeto(354990,wheb_usuario_pck.get_nr_seq_idioma,'NR_AGENDA_ANTIGA=' ||nr_seq_agenda_p||';'||'DS_AGENDA_ANTIGA='||ds_sala_antiga_w||';'||'DS_AGENDA_ATUAL='||ds_sala_nova_w||';'),1,2000);
	end if;
	gravar_historico_identific_pac(nr_atendimento_w,nr_cirurgia_w,'IP',ds_historico_w,nm_usuario_p);
else
	alterar_dados_painel(nr_seq_agenda_p,nr_seq_sala_cir_p,hr_revisada_p,ds_observacao_p,nm_usuario_p,ds_inf_painel_p,ie_visita_pre_anest_p);
end if;

end alterar_dados_painel_html;
/