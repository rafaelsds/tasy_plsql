create or replace
procedure cpoe_delete_copy_npt ( nr_seq_npt_cpoe_p  number) is 
begin

    delete  
    from    cpoe_npt_produto
    where   nr_seq_npt_cpoe = nr_seq_npt_cpoe_p;
    
    delete 
    from    cpoe_npt_elemento
    where   nr_seq_npt_cpoe = nr_seq_npt_cpoe_p;
    
    delete
    from    cpoe_dieta
    where   nr_sequencia = nr_seq_npt_cpoe_p;
	
    commit;

end cpoe_delete_copy_npt;
/