create or replace
procedure gerar_w_ocupacao_fucri(
			dt_referencia_p		date,
			nm_usuario_p		varchar2) is
dt_inicio_ref_w			date;
dt_parametro_fim_w		date;
dt_parametro_w			date;
dt_parametro_mes_w		date;
nr_atendimento_w		number(10);
cd_setor_atendimento_w		number(5);
dt_referencia_w			date;
qt_atendimentos_w		number(18,8);
nr_leitos_dia_w			number(10);

cursor c02 is
select /*+ INDEX(C SETATEN_PK) INDEX(B ATEPACU_I1) INDEX(a atepaci_pk) */
	b.cd_setor_atendimento,
	dt_parametro_w,
	sum(obter_horas_ocupacao(dt_parametro_w, dt_parametro_mes_w, b.dt_entrada_unidade, b.dt_saida_interno))
	--count(a.nr_atendimento)
from	setor_atendimento c,
	atendimento_paciente a,
	atend_paciente_unidade b
where	a.nr_atendimento = b.nr_atendimento
and	b.cd_setor_atendimento = c.cd_setor_atendimento
and 	b.dt_entrada_unidade <= dt_parametro_mes_w
and	nvl(b.dt_saida_interno,dt_parametro_w)  >= dt_parametro_w
and	c.cd_classif_setor 		in (3,4)
and	nvl(c.ie_ocup_hospitalar,'N')	= 'S'
and	not exists
	(select	x.cd_motivo_alta
	from	motivo_alta x
	where	x.ie_censo_diario = 'N'
	and	a.cd_motivo_alta 	= x.cd_motivo_alta)
group by b.cd_setor_atendimento
order by 1,2 desc;

begin
dt_inicio_ref_w := PKG_DATE_UTILS.start_of(dt_referencia_p, 'mm', 0);
dt_parametro_fim_w                    := PKG_DATE_UTILS.ADD_MONTH(dt_inicio_ref_w,1,0) - 1/86400;
if	(dt_parametro_fim_w > sysdate) then
	dt_parametro_fim_w := PKG_DATE_UTILS.start_of(sysdate, 'ddd', 0) - 1/86400;
end if;
dt_parametro_w                        := dt_inicio_ref_w;
dt_parametro_mes_w                    := PKG_DATE_UTILS.start_of(dt_parametro_w, 'month', 0);
delete from w_ocupacao
where	dt_referencia between dt_inicio_ref_w and dt_parametro_fim_w;

while	(dt_parametro_w <= dt_parametro_fim_w) loop
	dt_parametro_mes_w := dt_parametro_w + 86399/86400;
	open c02;
	loop
	fetch c02 into
		cd_setor_atendimento_w,
		dt_referencia_w,
		qt_atendimentos_w;
	exit when c02%notfound;
		nr_leitos_dia_w	:= obter_unidades_setor_dt(cd_setor_atendimento_w, dt_referencia_w);

		insert into w_ocupacao(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			dt_referencia,
			cd_setor_atendimento,
			qt_leitos_data,
			qt_pac_dia,
			pr_ocupacao_setor)
		values (
			w_ocupacao_seq.nextval,
			nm_usuario_p,
			sysdate,
			dt_referencia_w,
			cd_setor_atendimento_w,
			nr_leitos_dia_w,
			qt_atendimentos_w,
			round(dividir(qt_atendimentos_w,nr_leitos_dia_w)*100,2));
	end loop;
	close c02;

	insert into w_ocupacao(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		dt_referencia,
		cd_setor_atendimento,
		qt_leitos_data,
		qt_pac_dia,
		pr_ocupacao_setor)
	values	(w_ocupacao_seq.nextval,
		nm_usuario_p,
		sysdate,
		dt_parametro_w,
		0,
		(select sum(qt_leitos_data) from w_ocupacao where dt_referencia = dt_parametro_w),
		(select sum(qt_pac_dia)from w_ocupacao where dt_referencia = dt_parametro_w),
		(select round(dividir(sum(qt_pac_dia),sum(qt_leitos_data))*100,2)from w_ocupacao where dt_referencia = dt_parametro_w));
	dt_parametro_w := dt_parametro_w +1;
end loop;
commit;

end gerar_w_ocupacao_fucri;
/