create or replace
procedure sbc_gerar_arquivo_mens
		(	nr_seq_cobr_escrit_p		number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is 
			
ds_conteudo_w			varchar2(4000);
ds_titulo_w			varchar2(3000);
nr_seq_registro_w		varchar2(10);
nr_seq_pagador_w		varchar2(10);
dt_vencimento_w			date;
vl_saldo_titulo_w		number(15,2);
dt_pagamento_previsto_w		varchar2(8);

cd_associado_w			varchar2(11);
nr_seu_numero_w			varchar2(10);
nr_nosso_numero_w		varchar2(20);
nm_sacado_w			varchar2(40);
ds_endereco_saca_w		varchar2(40);
ds_cidade_sacado_w		varchar2(20);
ds_estado_sacado_w		varchar2(2);
cd_cep_sacado_w			varchar2(9);
dt_extrato_w			varchar2(8);
nm_plano_w			varchar2(40);
dt_cobranca_w			varchar2(7);
dt_venc_titulo_w		varchar2(10);
ie_via_titulo_w			varchar2(1);
nm_benef_w			varchar2(30);
nm_benef_02_w			varchar2(30);
nm_benef_03_w			varchar2(30);
nm_benef_04_w			varchar2(30);
nm_benef_05_w			varchar2(30);
nm_benef_06_w			varchar2(30);
nm_benef_07_w			varchar2(30);
nm_benef_08_w			varchar2(30);
nm_benef_09_w			varchar2(30);
nm_benef_10_w			varchar2(30);
ds_extrato_01_w			varchar2(100);
ds_extrato_02_w			varchar2(100);
ds_extrato_03_w			varchar2(100);
ds_extrato_04_w			varchar2(100);
ds_extrato_05_w			varchar2(100);
ds_extrato_06_w			varchar2(100);
ds_extrato_07_w			varchar2(100);
ds_extrato_08_w			varchar2(100);
ds_extrato_09_w			varchar2(100);
ds_extrato_10_w			varchar2(100);
ds_mensagen_01_w		varchar2(100);
ds_mensagen_02_w		varchar2(100);
ds_mensagen_03_w		varchar2(100);
ds_mensagen_04_w		varchar2(100);
ds_mensagen_05_w		varchar2(100);
ds_mensagen_06_w		varchar2(100);
ds_mensagen_07_w		varchar2(100);
ds_aumento_01_w			varchar2(100);
ds_aumento_02_w			varchar2(100);
ds_aumento_03_w			varchar2(100);
ds_aumento_04_w			varchar2(100);
ds_aumento_05_w			varchar2(100);
ds_aumento_06_w			varchar2(100);
ds_aumento_07_w			varchar2(100);
ds_aumento_08_w			varchar2(100);
ds_aumento_09_w			varchar2(100);
ds_aumento_10_w			varchar2(100);
cd_carteira_w			varchar2(20);
vl_consulta_01_w		varchar2(9);
vl_consulta_02_w		varchar2(9);
vl_consulta_03_w		varchar2(9);
vl_consulta_04_w		varchar2(9);
vl_consulta_05_w		varchar2(9);
vl_consulta_06_w		varchar2(9);
vl_consulta_07_w		varchar2(9);
vl_consulta_08_w		varchar2(9);
vl_consulta_09_w		varchar2(9);
vl_consulta_10_w		varchar2(9);
vl_exame_01_w			varchar2(10);
vl_exame_02_w			varchar2(10);
vl_exame_03_w			varchar2(10);
vl_exame_04_w			varchar2(10);
vl_exame_05_w			varchar2(10);
vl_exame_06_w			varchar2(10);
vl_exame_07_w			varchar2(10);
vl_exame_08_w			varchar2(10);
vl_exame_09_w			varchar2(10);
vl_exame_10_w			varchar2(10);
vl_tratamento_01_w		varchar2(10);
vl_tratamento_02_w		varchar2(10);
vl_tratamento_03_w		varchar2(10);
vl_tratamento_04_w		varchar2(10);
vl_tratamento_05_w		varchar2(10);
vl_tratamento_06_w		varchar2(10);
vl_tratamento_07_w		varchar2(10);
vl_tratamento_08_w		varchar2(10);
vl_tratamento_09_w		varchar2(10);
vl_tratamento_10_w		varchar2(10);
vl_total_01_w			varchar2(10);
vl_total_02_w			varchar2(10);
vl_total_03_w			varchar2(10);
vl_total_04_w			varchar2(10);
vl_total_05_w			varchar2(10);
vl_total_06_w			varchar2(10);
vl_total_07_w			varchar2(10);
vl_total_08_w			varchar2(10);
vl_total_09_w			varchar2(10);
vl_total_10_w			varchar2(10);
vl_valor_w			varchar2(10);
cd_barras_w			varchar2(44);
nr_linha_dig_w			varchar2(60);
dt_emissao_w			varchar2(10);

cursor c01 is
	select	lpad(substr(pls_obter_dados_segurado(pls_obter_segurado_pagador(d.nr_seq_pagador),'C'),1,11),11,'0') cd_associado,
		substr(b.nr_titulo,1,10) nr_seu_numero,
		rpad(substr(x.cd_convenio_banco,1,7),7,'0') || lpad(substr(b.nr_titulo,1,10),10,'0') nr_nosso_numero,
		substr(upper(elimina_acentuacao(b.nm_pessoa)),1,40) nm_sacado,
		substr(upper(decode(d.nr_sequencia,null,pls_obter_end_pagador(d.nr_seq_pagador, 'ES'),
			pls_obter_end_pagador(d.nr_seq_pagador,'EN'))),1,40) ds_endereco_sacado,
		substr(upper(pls_obter_end_pagador(d.nr_seq_pagador, 'CI')),1,20) ds_cidade_sacado,
		substr(upper(pls_obter_end_pagador(d.nr_seq_pagador,'UF')),1,2) ds_estado_sacado,
		substr(pls_obter_end_pagador(d.nr_seq_pagador,'CEP'),1,9) cd_cep_sacado,
		upper(to_char(PKG_DATE_UTILS.ADD_MONTH(d.dt_referencia,-1,0), 'Mon/yyyy')) dt_extrato,
		substr(upper(pls_obter_dados_produto_contr(z.nr_seq_contrato,'N')),1,40) nm_plano,
		to_char(b.dt_vencimento, 'mm/yyyy') dt_cobranca,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'dd/mm/yyyy') dt_venc_titulo,
		' ' ie_via_titulo_w,
		nvl(substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,1)),1,30),
			substr(upper(elimina_acentuacao(b.nm_pessoa)),1,30)) nm_benef_01,
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,2)),1,30) nm_benef_02,			
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,3)),1,30) nm_benef_03,			
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,4)),1,30) nm_benef_04,			
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,5)),1,30) nm_benef_05,			
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,6)),1,30) nm_benef_06,			
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,7)),1,30) nm_benef_07,			
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,8)),1,30) nm_benef_08,			
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,9)),1,30) nm_benef_09,			
		substr(upper(pls_obter_nome_benef_remessa(z.nr_sequencia,10)),1,30) nm_benef_10,
		'Banco: Ap�s vencto. cobrar R$ '|| campo_mascara_virgula(round((b.vl_titulo * 0.0302) / 30,2)) ||'/dia corrido' ds_mensagen_01,
		'N�o receber ap�s '|| to_char(PKG_DATE_UTILS.ADD_MONTH(b.dt_vencimento,+1,0),'dd/mm/yyyy') ds_mensagen_02,
		substr(obter_instrucao_boleto(b.nr_titulo,1,3),1,100) ds_mensagen_03,
		'Valor da Contribuic�o '|| to_char(b.dt_vencimento,'Mon/yyyy') || ': R$ '|| nvl(campo_mascara_virgula(b.vl_titulo),0) ds_mensagen_04,
		substr(obter_instrucao_boleto(b.nr_titulo,1,5),1,100) ds_mensagen_05,
		substr(obter_instrucao_boleto(b.nr_titulo,1,6),1,100) ds_mensagen_06,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,1),1,100) ds_aumento_01,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,2),1,100) ds_aumento_02,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,3),1,100) ds_aumento_03,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,4),1,100) ds_aumento_04,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,5),1,100) ds_aumento_05,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,6),1,100) ds_aumento_06,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,7),1,100) ds_aumento_07,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,8),1,100) ds_aumento_08,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,9),1,100) ds_aumento_09,
		substr(pls_obter_nome_produto_remessa(d.nr_sequencia,10),1,100) ds_aumento_10,
		substr(pls_obter_dados_segurado(pls_obter_segurado_pagador(d.nr_seq_pagador),'C'),1,20) cd_carteira,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,1,'C'),1,9),0) vl_consulta_01,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,2,'C'),1,9),0) vl_consulta_02,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,3,'C'),1,9),0) vl_consulta_03,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,4,'C'),1,9),0) vl_consulta_04,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,5,'C'),1,9),0) vl_consulta_05,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,6,'C'),1,9),0) vl_consulta_06,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,7,'C'),1,9),0) vl_consulta_07,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,8,'C'),1,9),0) vl_consulta_08,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,9,'C'),1,9),0) vl_consulta_09,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,10,'C'),1,9),0) vl_consulta_10,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,1,'E'),1,10),0) vl_exame_01,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,2,'E'),1,10),0) vl_exame_02,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,3,'E'),1,10),0) vl_exame_03,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,4,'E'),1,10),0) vl_exame_04,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,5,'E'),1,10),0) vl_exame_05,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,6,'E'),1,10),0) vl_exame_06,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,7,'E'),1,10),0) vl_exame_07,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,8,'E'),1,10),0) vl_exame_08,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,9,'E'),1,10),0) vl_exame_09,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,10,'E'),1,10),0) vl_exame_10,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,1,'TR'),1,10),0) vl_tratamento_01,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,2,'TR'),1,10),0) vl_tratamento_02,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,3,'TR'),1,10),0) vl_tratamento_03,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,4,'TR'),1,10),0) vl_tratamento_04,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,5,'TR'),1,10),0) vl_tratamento_05,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,6,'TR'),1,10),0) vl_tratamento_06,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,7,'TR'),1,10),0) vl_tratamento_07,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,8,'TR'),1,10),0) vl_tratamento_08,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,9,'TR'),1,10),0) vl_tratamento_09,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,10,'TR'),1,10),0) vl_tratamento_10,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,1,'TO'),1,10),0) vl_total_01,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,2,'TO'),1,10),0) vl_total_02,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,3,'TO'),1,10),0) vl_total_03,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,4,'TO'),1,10),0) vl_total_04,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,5,'TO'),1,10),0) vl_total_05,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,6,'TO'),1,10),0) vl_total_06,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,7,'TO'),1,10),0) vl_total_07,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,8,'TO'),1,10),0) vl_total_08,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,9,'TO'),1,10),0) vl_total_09,
		nvl(substr(sbc_obter_valor_item_boleto(d.nr_sequencia,10,'TO'),1,10),0) vl_total_10,
		nvl(campo_mascara_virgula(b.vl_titulo),0) vl_valor,
		substr(b.nr_bloqueto,1,44) cd_barras,
		substr(converte_codigo_bloqueto('Barra_Editado',b.nr_bloqueto),1,60) nr_linha_dig,
		to_char(b.dt_emissao,'dd/mm/yyyy') dt_emissao,
		d.nr_seq_pagador nr_seq_pagador,
		nvl(b.dt_pagamento_previsto, b.dt_vencimento) dt_vencimento
	from	pls_contrato_pagador	z,
		banco_estabelecimento	x,
		pls_mensalidade		d,
		titulo_receber_v	b,
		titulo_receber_cobr	c,
		cobranca_escritural	a	
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	z.nr_sequencia		= d.nr_seq_pagador
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
cursor c02 is
	select	to_char(b.dt_pagamento_previsto,'Mon/yyyy') dt_pagamento_previsto,
		nvl(b.vl_saldo_titulo,0) vl_saldo_titulo
	from	pls_mensalidade	d,
		titulo_receber	b
	where	b.nr_seq_mensalidade	= d.nr_sequencia
	and	d.nr_seq_pagador	= nr_seq_pagador_w
	and	PKG_DATE_UTILS.start_of(b.dt_pagamento_previsto,'month',0) < PKG_DATE_UTILS.start_of(dt_vencimento_w,'month',0)
	and	rownum < 2
	order by b.dt_pagamento_previsto desc;
	
begin

delete from w_envio_banco where nm_usuario = nm_usuario_p;

ds_titulo_w :=	'C�digo do associado;Seu n�mero;Nosso n�mero;Nome do sacado;Endere�o do sacado;Cidade do sacado;UF;CEP;'||
		'M�s referente ao extrato do auxilio;Nome do Produto(Plano);M�s de referencia do boleto;Vencimento;Via;	Nome 01;Nome 02;Nome 03;'||
		'Nome 04;Nome 05;Nome 06;Nome 07;Nome 08;Nome 09;Nome 10;Extrato de auxilio 01;Extrato de auxilio 02;Extrato de auxilio 03;'|| 
		'Extrato de auxilio 04;Extrato de auxilio 05;Extrato de auxilio 06;Extrato de auxilio 07;Extrato de auxilio 08;'||
		'Extrato de auxilio 09;Extrato de auxilio 10;Mensagem 01;Mensagem 02;Mensagem 03;Mensagem 04;Mensagem 05;Mensagem 06;Mensagem 07;'||
		'Aumento 01;Aumento 02;Aumento 03;Aumento 04;Aumento 05;Aumento 06;Aumento 07;Aumento 08;Aumento 09;Aumento 10;Carteira;'||
		'Nome titular;Consulta;Exame;Tratamento;Total;Nome 02;Consulta;Exame;Tratamento;Total;Nome 03;Consulta;Exame;Tratamento;'||
		'Total;Nome 04;Consulta;Exame;Tratamento;Total;Nome 05;Consulta;Exame;Tratamento;Total;Nome 06;Consulta;Exame;Tratamento;Total;'||
		'Nome 07;Consulta;Exame;Tratamento;Total;Nome 08;Consulta;Exame;Tratamento;Total;Nome 09;Consulta;Exame;Tratamento;Total;Valor;'||
		'N�mero registro;C�digo barras;Linha dig�tavel;Data de emiss�o;';
		
insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_titulo_w,
			1);

open c01;
loop
fetch c01 into	
	cd_associado_w,
	nr_seu_numero_w,
	nr_nosso_numero_w,
	nm_sacado_w,
	ds_endereco_saca_w,
	ds_cidade_sacado_w,
	ds_estado_sacado_w,
	cd_cep_sacado_w,
	dt_extrato_w,
	nm_plano_w,
	dt_cobranca_w,
	dt_venc_titulo_w,
	ie_via_titulo_w,
	nm_benef_w,
	nm_benef_02_w,			
	nm_benef_03_w,			
	nm_benef_04_w,			
	nm_benef_05_w,			
	nm_benef_06_w,			
	nm_benef_07_w,			
	nm_benef_08_w,			
	nm_benef_09_w,			
	nm_benef_10_w,
	ds_mensagen_01_w,
	ds_mensagen_02_w,
	ds_mensagen_03_w,
	ds_mensagen_04_w,
	ds_mensagen_05_w,
	ds_mensagen_06_w,
	ds_aumento_01_w,
	ds_aumento_02_w,
	ds_aumento_03_w,
	ds_aumento_04_w,
	ds_aumento_05_w,
	ds_aumento_06_w,
	ds_aumento_07_w,
	ds_aumento_08_w,
	ds_aumento_09_w,
	ds_aumento_10_w,
	cd_carteira_w,
	vl_consulta_01_w,
	vl_consulta_02_w,
	vl_consulta_03_w,
	vl_consulta_04_w,
	vl_consulta_05_w,
	vl_consulta_06_w,
	vl_consulta_07_w,
	vl_consulta_08_w,
	vl_consulta_09_w,
	vl_consulta_10_w,
	vl_exame_01_w,
	vl_exame_02_w,
	vl_exame_03_w,
	vl_exame_04_w,
	vl_exame_05_w,
	vl_exame_06_w,
	vl_exame_07_w,
	vl_exame_08_w,
	vl_exame_09_w,
	vl_exame_10_w,
	vl_tratamento_01_w,
	vl_tratamento_02_w,
	vl_tratamento_03_w,
	vl_tratamento_04_w,
	vl_tratamento_05_w,
	vl_tratamento_06_w,
	vl_tratamento_07_w,
	vl_tratamento_08_w,
	vl_tratamento_09_w,
	vl_tratamento_10_w,
	vl_total_01_w,
	vl_total_02_w,
	vl_total_03_w,
	vl_total_04_w,
	vl_total_05_w,
	vl_total_06_w,
	vl_total_07_w,
	vl_total_08_w,
	vl_total_09_w,
	vl_total_10_w,
	vl_valor_w,
	cd_barras_w,
	nr_linha_dig_w,
	dt_emissao_w,
	nr_seq_pagador_w,
	dt_vencimento_w;
exit when c01%notfound;
	begin
	select	w_envio_banco_seq.nextval
	into	nr_seq_registro_w
	from	dual;	
	
	if	(trim(nm_benef_w) is not null) then
		ds_extrato_01_w	:=  nm_benef_w || ' ' || vl_consulta_01_w || ' ' || vl_exame_01_w || ' ' || vl_tratamento_01_w || ' ' || vl_total_01_w;
	end if;
	
	if	(trim(nm_benef_02_w) is not null) then
		ds_extrato_02_w	:=  nm_benef_02_w || ' ' || vl_consulta_02_w || ' ' || vl_exame_02_w || ' ' || vl_tratamento_02_w || ' ' || vl_total_02_w;
	end if;
	
	if	(trim(nm_benef_03_w) is not null) then
		ds_extrato_03_w	:=  nm_benef_03_w || ' ' || vl_consulta_03_w || ' ' || vl_exame_03_w || ' ' || vl_tratamento_03_w || ' ' || vl_total_03_w;
	end if;
	
	if	(trim(nm_benef_04_w) is not null) then
		ds_extrato_04_w	:=  nm_benef_04_w || ' ' || vl_consulta_04_w || ' ' || vl_exame_04_w || ' ' || vl_tratamento_04_w || ' ' || vl_total_04_w;
	end if;
	
	if	(trim(nm_benef_05_w) is not null) then
		ds_extrato_05_w	:=  nm_benef_05_w || ' ' || vl_consulta_05_w || ' ' || vl_exame_05_w || ' ' || vl_tratamento_05_w || ' ' || vl_total_05_w;
	end if;
	
	if	(trim(nm_benef_06_w) is not null) then
		ds_extrato_06_w	:=  nm_benef_06_w || ' ' || vl_consulta_06_w || ' ' || vl_exame_06_w || ' ' || vl_tratamento_06_w || ' ' || vl_total_06_w;
	end if;
	
	if	(trim(nm_benef_07_w) is not null) then
		ds_extrato_07_w	:=  nm_benef_07_w || ' ' || vl_consulta_07_w || ' ' || vl_exame_07_w || ' ' || vl_tratamento_07_w || ' ' || vl_total_07_w;
	end if;
	
	if	(trim(nm_benef_08_w) is not null) then
		ds_extrato_08_w	:=  nm_benef_08_w || ' ' || vl_consulta_08_w || ' ' || vl_exame_08_w || ' ' || vl_tratamento_08_w || ' ' || vl_total_08_w;
	end if;
	
	if	(trim(nm_benef_09_w) is not null) then
		ds_extrato_09_w	:=  nm_benef_09_w || ' ' || vl_consulta_09_w || ' ' || vl_exame_09_w || ' ' || vl_tratamento_09_w || ' ' || vl_total_09_w;
	end if;
	
	if	(trim(nm_benef_10_w) is not null) then
		ds_extrato_10_w	:=  nm_benef_10_w || ' ' || vl_consulta_10_w || ' ' || vl_exame_10_w || ' ' || vl_tratamento_10_w || ' ' || vl_total_10_w;
	end if;
	
	open C02;
	loop
	fetch C02 into	
		dt_pagamento_previsto_w,
		vl_saldo_titulo_w;
	exit when C02%notfound;
		begin
		if	(vl_saldo_titulo_w > 0) then
			ds_mensagen_07_w := 'Pagto(s) pendente(s): '|| dt_pagamento_previsto_w ||' ';
		else
			ds_mensagen_07_w := '�ltimo pagamento (ok)';
		ds_mensagen_07_w := null;
		end if;		
		end;
	end loop;
	close C02;
	
	ds_conteudo_w	:=	cd_associado_w 		||';'|| 
				nr_seu_numero_w 	||';'|| 
				nr_nosso_numero_w 	||';'|| 
				nm_sacado_w 		||';'|| 
				ds_endereco_saca_w 	||';'|| 
				ds_cidade_sacado_w 	||';'|| 
				ds_estado_sacado_w 	||';'|| 
				cd_cep_sacado_w 	||';'|| 
				dt_extrato_w 		||';'|| 
				nm_plano_w 		||';'|| 
				dt_cobranca_w 		||';'|| 
				dt_venc_titulo_w 	||';'|| 
				ie_via_titulo_w 	||';'|| 
				nm_benef_w 		||';'|| 
				nm_benef_02_w 		||';'|| 
				nm_benef_03_w 		||';'|| 
				nm_benef_04_w 		||';'|| 
				nm_benef_05_w 		||';'|| 
				nm_benef_06_w 		||';'|| 
				nm_benef_07_w		||';'|| 
				nm_benef_08_w 		||';'|| 
				nm_benef_09_w 		||';'|| 
				nm_benef_10_w 		||';'|| 
				ds_extrato_01_w 	||';'|| 
				ds_extrato_02_w 	||';'|| 
				ds_extrato_03_w 	||';'|| 
				ds_extrato_04_w 	||';'|| 
				ds_extrato_05_w 	||';'|| 
				ds_extrato_06_w 	||';'|| 
				ds_extrato_07_w 	||';'|| 
				ds_extrato_08_w 	||';'|| 
				ds_extrato_09_w 	||';'|| 
				ds_extrato_10_w 	||';'|| 
				ds_mensagen_01_w 	||';'|| 
				ds_mensagen_02_w 	||';'|| 
				ds_mensagen_03_w 	||';'|| 
				ds_mensagen_04_w 	||';'|| 
				ds_mensagen_05_w 	||';'|| 
				ds_mensagen_06_w 	||';'|| 
				ds_mensagen_07_w 	||';'|| 
				ds_aumento_01_w 	||';'|| 
				ds_aumento_02_w 	||';'|| 
				ds_aumento_03_w 	||';'|| 
				ds_aumento_04_w 	||';'|| 
				ds_aumento_05_w 	||';'|| 
				ds_aumento_06_w 	||';'|| 
				ds_aumento_07_w 	||';'|| 
				ds_aumento_08_w 	||';'|| 
				ds_aumento_09_w 	||';'|| 
				ds_aumento_10_w 	||';'|| 
				cd_carteira_w 		||';'|| 
				nm_benef_w 		||';'|| 
				vl_consulta_01_w 	||';'|| 
				vl_exame_01_w 		||';'|| 
				vl_tratamento_01_w 	||';'|| 
				vl_total_01_w 		||';'|| 
				nm_benef_02_w 		||';'|| 
				vl_consulta_02_w 	||';'|| 
				vl_exame_02_w 		||';'|| 
				vl_tratamento_02_w 	||';'|| 
				vl_total_02_w 		||';'|| 		
				nm_benef_03_w 		||';'|| 
				vl_consulta_03_w 	||';'||
				vl_exame_03_w 		||';'|| 
				vl_tratamento_03_w 	||';'|| 
				vl_total_03_w 		||';'|| 			
				nm_benef_04_w 		||';'|| 
				vl_consulta_04_w 	||';'|| 
				vl_exame_04_w 		||';'|| 
				vl_tratamento_04_w 	||';'|| 
				vl_total_04_w 		||';'|| 	
				nm_benef_05_w 		||';'|| 
				vl_consulta_05_w 	||';'|| 
				vl_exame_05_w 		||';'|| 
				vl_tratamento_05_w 	||';'|| 
				vl_total_05_w 		||';'|| 			
				nm_benef_06_w 		||';'|| 
				vl_consulta_06_w 	||';'|| 
				vl_exame_06_w 		||';'|| 
				vl_tratamento_06_w 	||';'|| 
				vl_total_06_w 		||';'|| 			
				nm_benef_07_w		||';'|| 
				vl_consulta_07_w 	||';'|| 
				vl_exame_07_w 		||';'|| 
				vl_tratamento_07_w 	||';'|| 
				vl_total_07_w 		||';'|| 			
				nm_benef_08_w 		||';'|| 
				vl_consulta_08_w 	||';'|| 
				vl_exame_08_w 		||';'|| 
				vl_tratamento_08_w 	||';'|| 
				vl_total_08_w 		||';'|| 			
				nm_benef_09_w 		||';'|| 
				vl_consulta_09_w 	||';'|| 
				vl_exame_09_w 		||';'|| 
				vl_tratamento_09_w 	||';'|| 
				vl_total_09_w 		||';'|| 			
				vl_valor_w 		||';'|| 
				nr_seq_registro_w 	||';'|| 
				cd_barras_w 		||';'|| 
				nr_linha_dig_w 		||';'|| 
				dt_emissao_w;
				
	insert into w_envio_banco
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
		values	(	w_envio_banco_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				1);
	end;
end loop;
close c01;

commit;

end sbc_gerar_arquivo_mens;
/