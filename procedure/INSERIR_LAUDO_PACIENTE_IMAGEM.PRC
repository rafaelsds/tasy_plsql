CREATE OR REPLACE
PROCEDURE Inserir_Laudo_Paciente_Imagem (	nr_sequencia_p	number,
					nr_seq_imagem_p	number,
					ds_arquivo_p	varchar2,
					ds_imagem_p	varchar2,
					nm_usuario_p	varchar2) is
BEGIN

insert into laudo_paciente_imagem	(
		nr_sequencia,
		nr_seq_imagem,
		dt_atualizacao,
		nm_usuario,
		ds_arquivo_imagem,
		ds_imagem)
values	(
		nr_sequencia_p,
		nr_seq_imagem_p,
		sysdate,
		nm_usuario_p,
		ds_arquivo_p,
		ds_imagem_p);

commit;

END	Inserir_Laudo_Paciente_Imagem;
/
