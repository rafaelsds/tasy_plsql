create or replace
procedure rxt_obter_proced_tip_trat(	ie_regra_lancamento_p	varchar2,
					ie_gerar_proc_p		varchar2,
					nr_Seq_tipo_p		number,
					nr_seq_proc_interno_p	out	number,
					cd_procedimento_p	out	number,
					ie_origem_proced_p	out	number) is 
nr_seq_proc_interno_w	number(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);

Cursor C01 is
	select	nr_Seq_proc_interno,
		cd_procedimento,
		ie_origem_proced
	from	rxt_tipo_trat_proced
	where	ie_regra_lancamento = ie_regra_lancamento_p
	and	ie_gerar_proc = ie_gerar_proc_p
	and	nr_seq_tipo = nr_seq_tipo_p
	and	ie_situacao = 'A'
	order by nvl(nr_seq_tipo,0);
begin
open C01;
loop
fetch C01 into	
	nr_seq_proc_interno_w,
	cd_procedimento_w,
	ie_origem_proced_w;
exit when C01%notfound;
	begin
	nr_seq_proc_interno_w	:= nr_seq_proc_interno_w;
	cd_procedimento_w	:= cd_procedimento_w;
	ie_origem_proced_w	:= ie_origem_proced_w;
	end;
end loop;
close C01;

if	(cd_procedimento_w is null) and
	(nr_seq_proc_interno_w is not null) then
	select	max(cd_procedimento),
		max(ie_origem_proced)
	into	cd_procedimento_w,
		ie_origem_proced_w
	from	proc_interno
	where	nr_sequencia = nr_seq_proc_interno_w;
end if;

nr_seq_proc_interno_p	:= nr_seq_proc_interno_w;
cd_procedimento_p	:= cd_procedimento_w;
ie_origem_proced_p	:= ie_origem_proced_w;


commit;

end rxt_obter_proced_tip_trat;
/