create or replace procedure duv_gerar_segmento_bed(nr_seq_mensagem_p  in duv_mensagem.nr_sequencia%type,
                                                   nm_usuario_p       in usuario.nm_usuario%type,
                                                   nr_seq_episodio_p  in episodio_paciente.nr_sequencia%type)is
  cursor c01 is
     select
       ates.ie_acidente_trabalho as ie_sem_acidente_trab, 
       substr(ates.ds_justificativa,1,80) as ds_atencao_trabalho
     from atestado_paciente    ates,
          atendimento_paciente aten
     where aten.nr_seq_episodio = nr_seq_episodio_p
       and ates.nr_atendimento  = aten.nr_atendimento;

  c01_w c01%rowtype;

begin
  c01_w := null;
  open c01;
  fetch c01 into c01_w;
  close c01;
  insert into duv_bed(nr_sequencia
                     ,dt_atualizacao
                     ,nm_usuario
                     ,dt_atualizacao_nrec
                     ,nm_usuario_nrec
                     ,nr_seq_mensagem
                     ,ie_sem_acidente_trab
                     ,ds_atencao_trabalho) values (duv_bed_seq.nextval,
                                                   sysdate,
                                                   nm_usuario_p,
                                                   sysdate,
                                                   nm_usuario_p,
                                                   nr_seq_mensagem_p,
                                                   c01_w.ie_sem_acidente_trab,
                                                   c01_w.ds_atencao_trabalho);
end duv_gerar_segmento_bed;
/
