CREATE OR REPLACE
PROCEDURE GERAR_PROC_ADICIONAL_SUS IS

DT_ATUALIZACAO_W			DATE;
NM_USUARIO_W			VARCHAR2(15);
cd_procedimento_w			number(15);
qt_procedimento_w			number(10);
dt_competencia_w			date;
ie_versao_w				varchar2(20);
dt_competencia_proc_w			Date;
ie_versao_proc_w			Varchar2(20);

BEGIN
begin
select	max(dt_competencia_aih),
	max(ie_versao_aih)
into	dt_competencia_w,
	ie_versao_w
from	sus_parametros;



/* Incluir procedimento 99080011-Diaria de acompanhante especial */
cd_procedimento_w			:= 0;
begin
select 	cd_procedimento
into		cd_procedimento_w
from		procedimento
where		cd_procedimento 	= 99080011
and		ie_origem_proced	= 2;
exception
		when others then
		cd_procedimento_w := 0;
end;

if	(cd_procedimento_w = 0) then
	begin
	insert into procedimento(
			CD_PROCEDIMENTO,                
			DS_PROCEDIMENTO,                
			DS_COMPLEMENTO,                
			IE_SITUACAO,                    
			CD_GRUPO_PROC,                  
			DT_ATUALIZACAO,                 
			NM_USUARIO,                     
			CD_TIPO_PROCEDIMENTO,           
			IE_CLASSIFICACAO,               
			CD_LAUDO_PADRAO,                
			CD_SETOR_EXCLUSIVO,             
			IE_ORIGEM_PROCED,               
			QT_DIA_INTERNACAO_SUS,          
			QT_IDADE_MINIMA_SUS,            
			QT_IDADE_MAXIMA_SUS,            
			IE_SEXO_SUS,                    
			IE_INREAL_SUS,                  
			IE_INATOM_SUS,                  
			CD_GRUPO_SUS,                   
			CD_DOENCA_CID,                  
			CD_CID_SECUNDARIO,              
			DS_PROC_INTERNO,                
			NR_PROC_INTERNO,                
			IE_UTIL_PRESCRICAO,             
			CD_KIT_MATERIAL,                
			DS_ORIENTACAO,                  
			IE_VALOR_ESPECIAL,              
			DT_CARGA,                       
			QT_MAX_PROCEDIMENTO,            
			IE_EXIGE_LAUDO,                 
			IE_FORMA_APRESENTACAO,
			IE_APURACAO_CUSTO,
			QT_HORA_BAIXAR_PRESCR,
			IE_EXIGE_AUTOR_SUS,
			IE_ATIV_PROF_BPA,
			IE_ALTA_COMPLEXIDADE,
			IE_IGNORA_ORIGEM,
			IE_CLASSIF_CUSTO,
			IE_LOCALIZADOR,			
			QT_EXEC_BARRA)          
	select	
			99080011,                
			'DIARIA ACOMPANHANTE ESPECIAL',                
			null,                
			'S',                    
			x.CD_GRUPO_PROC,                  
			sysdate,                 
			'Tasy',                     
			x.CD_TIPO_PROCEDIMENTO,           
			x.IE_CLASSIFICACAO,               
			x.CD_LAUDO_PADRAO,                
			x.CD_SETOR_EXCLUSIVO,             
			x.IE_ORIGEM_PROCED,               
			x.QT_DIA_INTERNACAO_SUS,          
			0,            
			99,            
			x.IE_SEXO_SUS,                    
			x.IE_INREAL_SUS,                  
			x.IE_INATOM_SUS,                  
			x.CD_GRUPO_SUS,                   
			x.CD_DOENCA_CID,                  
			x.CD_CID_SECUNDARIO,              
			null,                
			null,                
			null,             
			null,                
			null,                  
			null,              
			null,                       
			x.QT_MAX_PROCEDIMENTO,            
			x.IE_EXIGE_LAUDO,                 
			x.IE_FORMA_APRESENTACAO,
			'Q',
			60          ,
			'N',
			'N',
			'N',
			'N',
			'B',
			'S',
			1
			
		from	procedimento x
		where	x.cd_procedimento 	= 99080010
		and	x.ie_origem_proced	= 2;
	commit;
	end;
end if;


/* Incluir procedimento 99080011 na sus_preco_procaih */
qt_procedimento_w			:= 0;
begin
select 	count(*)
into		qt_procedimento_w
from		sus_preco_procaih
where		cd_procedimento 	= 99080011
and		dt_competencia	=
		(select max(w.dt_competencia)	from sus_preco_procaih w
					where	w.cd_procedimento = 99080010);
exception
		when others then
		qt_procedimento_w := 0;
end;


if	(qt_procedimento_w = 0) then
	begin
	insert into	sus_preco_procaih
			(DT_COMPETENCIA,                 
			CD_PROCEDIMENTO,                
			IE_ORIGEM_PROCED,               
			DT_COMPETENCIA_INICIAL,         
			DT_COMPETENCIA_FINAL,           
			QT_IDADE_MINIMA,                
			QT_IDADE_MAXIMA,                
			IE_SEXO_SUS,                    
			IE_INREAL,                      
			IE_INATOM,                      
			QT_PERMANENCIA,                 
			QT_ATO_MEDICO,                  
			QT_ATO_ANESTESISTA,             
			VL_MATMED,                      
			VL_DIARIA,                      
			VL_TAXAS,                      
			VL_MEDICO,                      
			VL_SADT,                        
			DT_ATUALIZACAO,                 
			NM_USUARIO,                     
			IE_VERSAO)                      
	select
			x.DT_COMPETENCIA,                 
			99080011,                
			x.IE_ORIGEM_PROCED,               
			x.DT_COMPETENCIA_INICIAL,         
			x.DT_COMPETENCIA_FINAL,           
			0,                
			99,                
			x.IE_SEXO_SUS,                    
			x.IE_INREAL,                      
			x.IE_INATOM,                      
			0,                 
			0,                  
			0,             
			2.65,                      
			0,                      
			0,                      
			0,                      
			0,                        
			sysdate,                 
			'Tasy',                     
			x.IE_VERSAO                      
		from	sus_preco_procaih x
		where	x.cd_procedimento = 99080010
		and	x.dt_competencia	=
			(select max(w.dt_competencia)	from sus_preco_procaih w
								where	w.cd_procedimento = 99080010);
	commit;
	end;
end if;		 

/* Incluir procedimento 99080012-Diaria Normal (Somente para Tasy) */
cd_procedimento_w			:= 0;
begin
select 	cd_procedimento
into		cd_procedimento_w
from		procedimento
where		cd_procedimento 	= 99080012
and		ie_origem_proced	= 2;
exception
		when others then
		cd_procedimento_w := 0;
end;

if	(cd_procedimento_w = 0) then
	begin
	insert into procedimento(
			CD_PROCEDIMENTO,                
			DS_PROCEDIMENTO,                
			DS_COMPLEMENTO,                
			IE_SITUACAO,                    
			CD_GRUPO_PROC,                  
			DT_ATUALIZACAO,                 
			NM_USUARIO,                     
			CD_TIPO_PROCEDIMENTO,           
			IE_CLASSIFICACAO,               
			CD_LAUDO_PADRAO,                
			CD_SETOR_EXCLUSIVO,             
			IE_ORIGEM_PROCED,               
			QT_DIA_INTERNACAO_SUS,          
			QT_IDADE_MINIMA_SUS,            
			QT_IDADE_MAXIMA_SUS,            
			IE_SEXO_SUS,                    
			IE_INREAL_SUS,                  
			IE_INATOM_SUS,                  
			CD_GRUPO_SUS,                   
			CD_DOENCA_CID,                  
			CD_CID_SECUNDARIO,              
			DS_PROC_INTERNO,                
			NR_PROC_INTERNO,                
			IE_UTIL_PRESCRICAO,             
			CD_KIT_MATERIAL,                
			DS_ORIENTACAO,                  
			IE_VALOR_ESPECIAL,              
			DT_CARGA,                       
			QT_MAX_PROCEDIMENTO,            
			IE_EXIGE_LAUDO,                 
			IE_FORMA_APRESENTACAO,
			IE_APURACAO_CUSTO,
			QT_HORA_BAIXAR_PRESCR,
			IE_ALTA_COMPLEXIDADE,
			IE_ATIV_PROF_BPA,
			IE_CLASSIF_CUSTO,
			IE_EXIGE_AUTOR_SUS,
			IE_IGNORA_ORIGEM,
			IE_LOCALIZADOR,
			QT_EXEC_BARRA)          
	select	
			99080012,                
			'DIARIA SUS',                
			null,                
			'S',                    
			x.CD_GRUPO_PROC,                  
			sysdate,                 
			'Tasy',                     
			x.CD_TIPO_PROCEDIMENTO,           
			x.IE_CLASSIFICACAO,               
			x.CD_LAUDO_PADRAO,                
			x.CD_SETOR_EXCLUSIVO,             
			x.IE_ORIGEM_PROCED,               
			x.QT_DIA_INTERNACAO_SUS,          
			0,            
			99,            
			x.IE_SEXO_SUS,                    
			x.IE_INREAL_SUS,                  
			x.IE_INATOM_SUS,                  
			x.CD_GRUPO_SUS,                   
			x.CD_DOENCA_CID,                  
			x.CD_CID_SECUNDARIO,              
			null,                
			null,                
			null,             
			null,                
			null,                  
			null,              
			null,                       
			x.QT_MAX_PROCEDIMENTO,            
			x.IE_EXIGE_LAUDO,                 
			x.IE_FORMA_APRESENTACAO,
			'Q',
			60,
			'N',
			'N',
			'B',
			'N',
			'N',
			'S',
			1          
		from	procedimento x
		where	x.cd_procedimento 	= 99080010
		and	x.ie_origem_proced	= 2;
	commit;
	end;
end if;

/* Incluir procedimento 99080012 na sus_preco_procaih */
qt_procedimento_w			:= 0;
begin
select	count(*)
into	qt_procedimento_w
from	sus_preco_procaih x
where	cd_procedimento 	= 99080012
and	x.dt_competencia	= (select max(w.dt_competencia)	from sus_preco_procaih w);
exception
		when others then
		qt_procedimento_w := 0;
end;


if	(qt_procedimento_w = 0) then
	begin
	select	max(x.dt_competencia)
	into	dt_competencia_proc_w
	from	sus_preco_procaih x;

	select	max(ie_versao)
	into	ie_versao_proc_w
	from	sus_preco_procaih
	where	dt_competencia = dt_competencia_proc_w;

	insert into	sus_preco_procaih
			(DT_COMPETENCIA,                 
			CD_PROCEDIMENTO,                
			IE_ORIGEM_PROCED,               
			DT_COMPETENCIA_INICIAL,         
			DT_COMPETENCIA_FINAL,           
			QT_IDADE_MINIMA,                
			QT_IDADE_MAXIMA,                
			IE_SEXO_SUS,                    
			IE_INREAL,                      
			IE_INATOM,                      
			QT_PERMANENCIA,                 
			QT_ATO_MEDICO,                  
			QT_ATO_ANESTESISTA,             
			VL_MATMED,                      
			VL_DIARIA,                      
			VL_TAXAS,                      
			VL_MEDICO,                      
			VL_SADT,                        
			DT_ATUALIZACAO,                 
			NM_USUARIO,                     
			IE_VERSAO)                      
	select		dt_competencia_proc_w,
			CD_PROCEDIMENTO,                
			IE_ORIGEM_PROCED,               
			DT_COMPETENCIA_INICIAL,         
			DT_COMPETENCIA_FINAL,           
			QT_IDADE_MINIMA,                
			QT_IDADE_MAXIMA,                
			IE_SEXO_SUS,                    
			IE_INREAL,                      
			IE_INATOM,                      
			QT_PERMANENCIA,                 
			QT_ATO_MEDICO,                  
			QT_ATO_ANESTESISTA,             
			VL_MATMED,                      
			VL_DIARIA,                      
			VL_TAXAS,                      
			VL_MEDICO,                      
			VL_SADT,                        
			sysdate,                 
			'Tasy',                     
			ie_versao_proc_w
	from		sus_preco_procaih x
	where		x.cd_procedimento	= 99080012
	and		x.dt_competencia	= 
			(select max(w.dt_competencia)	from sus_preco_procaih w
								where	w.cd_procedimento = 99080012);
	commit;
/*	exception
		when others then
		application_error(-20011,'N�o foi poss�vel gerar o procedimento 99080012');*/
	end;
end if;		 


/* Incluir procedimento 99080991-Diaria UTI (Somente para Tasy) */
cd_procedimento_w			:= 0;
begin
select 	cd_procedimento
into		cd_procedimento_w
from		procedimento
where		cd_procedimento 	= 99080991
and		ie_origem_proced	= 2;
exception
		when others then
		cd_procedimento_w := 0;
end;

if	(cd_procedimento_w = 0) then
	begin
	insert into procedimento(
			CD_PROCEDIMENTO,                
			DS_PROCEDIMENTO,                
			DS_COMPLEMENTO,                
			IE_SITUACAO,                    
			CD_GRUPO_PROC,                  
			DT_ATUALIZACAO,                 
			NM_USUARIO,                     
			CD_TIPO_PROCEDIMENTO,           
			IE_CLASSIFICACAO,               
			CD_LAUDO_PADRAO,                
			CD_SETOR_EXCLUSIVO,             
			IE_ORIGEM_PROCED,               
			QT_DIA_INTERNACAO_SUS,          
			QT_IDADE_MINIMA_SUS,            
			QT_IDADE_MAXIMA_SUS,            
			IE_SEXO_SUS,                    
			IE_INREAL_SUS,                  
			IE_INATOM_SUS,                  
			CD_GRUPO_SUS,                   
			CD_DOENCA_CID,                  
			CD_CID_SECUNDARIO,              
			DS_PROC_INTERNO,                
			NR_PROC_INTERNO,                
			IE_UTIL_PRESCRICAO,             
			CD_KIT_MATERIAL,                
			DS_ORIENTACAO,                  
			IE_VALOR_ESPECIAL,              
			DT_CARGA,                       
			QT_MAX_PROCEDIMENTO,            
			IE_EXIGE_LAUDO,                 
			IE_FORMA_APRESENTACAO,
			IE_ALTA_COMPLEXIDADE,
			IE_ATIV_PROF_BPA,
			IE_CLASSIF_CUSTO,
			IE_EXIGE_AUTOR_SUS,
			IE_IGNORA_ORIGEM,
			IE_LOCALIZADOR,
			QT_EXEC_BARRA)          
	select	
			99080991,
			'DIARIA UTI SUS',                
			null,                
			'S',                    
			x.CD_GRUPO_PROC,                  
			sysdate,                 
			'Tasy',                     
			x.CD_TIPO_PROCEDIMENTO,           
			x.IE_CLASSIFICACAO,               
			x.CD_LAUDO_PADRAO,                
			x.CD_SETOR_EXCLUSIVO,             
			x.IE_ORIGEM_PROCED,               
			x.QT_DIA_INTERNACAO_SUS,          
			0,            
			99,            
			x.IE_SEXO_SUS,                    
			x.IE_INREAL_SUS,                  
			x.IE_INATOM_SUS,                  
			12,                   
			x.CD_DOENCA_CID,                  
			x.CD_CID_SECUNDARIO,              
			null,                
			null,                
			null,             
			null,                
			null,                  
			null,              
			null,                       
			x.QT_MAX_PROCEDIMENTO,            
			x.IE_EXIGE_LAUDO,                 
			x.IE_FORMA_APRESENTACAO,
			'N',
			'N',
			'B',
			'N',
			'N',
			'S',
			1          
		from	procedimento x
		where	x.cd_procedimento 	= 99080010
		and	x.ie_origem_proced	= 2;
	commit;
	end;
end if;

/* Incluir procedimento 99080991 na sus_preco_procaih */
qt_procedimento_w			:= 0;
begin
select 	count(*)
into		qt_procedimento_w
from		sus_preco_procaih
where		cd_procedimento 	= 99080991
and		dt_competencia		= dt_competencia_w;
exception
		when others then
		qt_procedimento_w := 0;
end;


if	(qt_procedimento_w = 0) then
	begin

	insert into	sus_preco_procaih
			(DT_COMPETENCIA,                 
			CD_PROCEDIMENTO,                
			IE_ORIGEM_PROCED,               
			DT_COMPETENCIA_INICIAL,         
			DT_COMPETENCIA_FINAL,           
			QT_IDADE_MINIMA,                
			QT_IDADE_MAXIMA,                
			IE_SEXO_SUS,                    
			IE_INREAL,                      
			IE_INATOM,                      
			QT_PERMANENCIA,                 
			QT_ATO_MEDICO,                  
			QT_ATO_ANESTESISTA,             
			VL_MATMED,                      
			VL_DIARIA,                      
			VL_TAXAS,                      
			VL_MEDICO,                      
			VL_SADT,                        
			DT_ATUALIZACAO,                 
			NM_USUARIO,                     
			IE_VERSAO)                      
	select		dt_competencia_w,
			CD_PROCEDIMENTO,                
			IE_ORIGEM_PROCED,               
			DT_COMPETENCIA_INICIAL,         
			DT_COMPETENCIA_FINAL,           
			QT_IDADE_MINIMA,                
			QT_IDADE_MAXIMA,                
			IE_SEXO_SUS,                    
			IE_INREAL,                      
			IE_INATOM,                      
			QT_PERMANENCIA,                 
			QT_ATO_MEDICO,                  
			QT_ATO_ANESTESISTA,             
			VL_MATMED,                      
			VL_DIARIA,                      
			VL_TAXAS,                      
			VL_MEDICO,                      
			VL_SADT,                        
			sysdate,                 
			'Tasy',                     
			ie_versao_w
		from	sus_preco_procaih x
		where	x.cd_procedimento = 99080991
		and	x.dt_competencia	=
			(select max(w.dt_competencia)	from sus_preco_procaih w
								where	w.cd_procedimento = 99080991);
	commit;
	end;
end if;	

exception
	when others then
	qt_procedimento_w	:= 0;
end;	 

END GERAR_PROC_ADICIONAL_SUS;
/