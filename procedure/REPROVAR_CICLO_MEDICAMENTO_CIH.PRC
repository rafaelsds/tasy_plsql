create or replace
procedure reprovar_ciclo_medicamento_cih(cd_material_p  number,
				nr_atendimento_p number,
				dt_prescricao_p date , 
				qt_total_dias_lib_p number,
				nr_seq_item_p number) is
begin

update prescr_material
set ie_ciclo_reprov = 'S'
where cd_material = cd_material_p
and nr_sequencia = nr_seq_item_p
  and nr_prescricao in
    (select a.nr_prescricao
     from prescr_medica b,
          prescr_material a
     where a.cd_material = cd_material_p
       and a.nr_prescricao = b.nr_prescricao
       and b.nr_atendimento = nr_atendimento_p
       and b.dt_prescricao between dt_prescricao_p and dt_prescricao_p + qt_total_dias_lib_p
	   and a.nr_sequencia = nr_seq_item_p);

	   
commit;


end reprovar_ciclo_medicamento_cih;
/