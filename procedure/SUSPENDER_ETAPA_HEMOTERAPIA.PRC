create or replace
procedure	suspender_etapa_hemoterapia(
								nr_seq_item_p		number,
								nr_prescricao_p		number,
								nr_prescricoes_p	varchar2,
								nr_seq_horario_p	number,
								nm_usuario_p		varchar2) is
nr_prescricao_w			number(14);
nr_seq_horario_w		number(10);
nr_seq_procedimento_w	number(6);
dt_horario_w			date;
nr_etapa_w				number(5);
nr_sequencia_w			number(10);
cursor c01 is
select	a.nr_sequencia,
		a.nr_prescricao,
		b.nr_sequencia,
		b.dt_horario,
		b.nr_etapa
from		prescr_proc_hor			b,
		prescr_procedimento		a
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_sequencia = b.nr_seq_procedimento
and		(((a.nr_prescricao = nr_prescricao_p) and
		 (a.nr_sequencia = nr_seq_item_p))or
		 obter_se_contido(a.nr_prescricao,nr_prescricoes_p) = 'S')
and		b.nr_sequencia = nr_seq_horario_p
and 		b.dt_inicio_horario 	is null
and		b.dt_suspensao 			is null
and 		b.dt_fim_horario		is null
and		(nvl(b.ie_horario_especial,'N') = 'N');
begin

if	(nr_seq_horario_p is not null) and
	(nr_seq_horario_p > 0) then
	open c01;
	loop
	fetch c01 into	nr_seq_procedimento_w,
				nr_prescricao_w,
				nr_seq_horario_w,
				dt_horario_w,
				nr_etapa_w;
	exit when c01%notfound;
		begin
		
		update	prescr_proc_hor
		set		dt_suspensao		= sysdate,
				nm_usuario_susp 	= nm_usuario_p
		where	nr_sequencia 		= nr_seq_horario_w
		and		nr_prescricao 		= nr_prescricao_w
		and		nr_seq_procedimento = nr_seq_procedimento_w;

		select	prescr_solucao_evento_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into prescr_solucao_evento (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_procedimento,
			cd_pessoa_fisica,
			ie_alteracao,
			dt_alteracao,
			ie_evento_valido,
			ie_tipo_solucao,
			nr_etapa_evento,
			nr_prescricao,
            dt_horario) 
		values (
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_procedimento_w,
			obter_dados_usuario_opcao(nm_usuario_p, 'C'),
			12,
			sysdate, 
			'S',
			3,
			nvl(nr_etapa_w,0),
			nr_prescricao_w,
            dt_horario_w); 
		end;
	end loop;
	close c01;
	commit;
end if;
end suspender_etapa_hemoterapia;
/
