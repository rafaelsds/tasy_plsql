create or replace
procedure atualiza_tributo_adiant_mx(	nr_titulo_p			number,
										nr_seq_tit_adiant_p	number,
										nm_usuario_p		Varchar2) is 
										
/*=======================================================================
					A TE N � � O
	Essa procedure foi criada especicamente para o m�xico, para atualiza��o
	do valor do tributo IVA no adiantamento vinculado ao t�tulo!!
           Ela � chamada dentro da atualiza_tributo_tit_adiant.
		   
========================================================================*/
cd_tributo_w				titulo_pagar_imposto.cd_tributo%type;	
vl_imposto_w				titulo_pagar_imposto.vl_imposto%type;	
vl_base_calculo_w			titulo_pagar_imposto.vl_base_calculo%type;
pr_aliquota_w				titulo_pagar_imposto.pr_imposto%type;

vl_adiantamento_w			titulo_pagar_adiant.vl_adiantamento%type;
vl_imposto_adiant_w			titulo_pagar_adiant.vl_imposto%type;
vl_total_imposto_adiant_w	titulo_pagar_adiant.vl_imposto%type;

/*Cursor que vai ler os tributos do titulo. Atualmente so tem IVA, mas pode ser que um dia tenho um outro tributo*/										
Cursor C01 is
	select	a.cd_tributo,
			nvl(a.vl_imposto,0),
			a.vl_base_calculo,
			a.pr_imposto
	from	titulo_pagar_imposto a
	where	nr_titulo	= nr_titulo_p
	order by a.cd_tributo;								

begin

/*Buscar o valor de adiantamneto da baixa. Sobre esse valor vai ser calculado o valor do(s) imposto(s) do t�tulo*/
select	nvl(max(a.vl_adiantamento),0)
into	vl_adiantamento_w
from	titulo_pagar_adiant a
where	nr_titulo		= nr_titulo_p
and		nr_sequencia	= nr_seq_tit_adiant_p;

/*Buscar o total de imposto j� gerado em adiantamentos para esse titulo*/
select	nvl(max(a.vl_imposto),0)
into	vl_total_imposto_adiant_w
from	titulo_pagar_adiant a
where	nr_titulo		= nr_titulo_p;

open C01;
loop
fetch C01 into	
	cd_tributo_w,
	vl_imposto_w,
	vl_base_calculo_w,
	pr_aliquota_w;
exit when C01%notfound;
	begin
	
	/*Na rotina GERAR_TITULO_PAGAR_NF, ele nao busca o PR_ALIQUOTA da regra de tributo, e sim faz o calculo: dividir(sum(nvl(a.vl_tributo,0)), sum(nvl(vl_base_calculo,0)))  para obter o PR_ALIQUOTA e jogar no TITULO_PAGAR_IMPOSTO, gerando o PR_ALIQUOTA em
	0.16% no lugar de 16%. Isso ta assim desde que o Ricardo criou ela em 2004. Por isso verifica aqui, se o PR_ALIQUOTA no titulo for menor que 1.0, multiplica por 100 para ter a aliquota correta.*/
	if (pr_aliquota_w < 1.0) then
		pr_aliquota_w := pr_aliquota_w * 100;
	end if;
	
	if ( nvl(vl_imposto_w,0) <> 0 ) and (pr_aliquota_w is not null) then

		vl_imposto_adiant_w	:= round((vl_adiantamento_w / ((pr_aliquota_w / 100) + 1)) * (pr_aliquota_w/100),2);

		update	titulo_pagar_adiant a
		set		a.vl_imposto	= nvl(vl_imposto_adiant_w,0)
		where	a.nr_titulo		= nr_titulo_p
		and		a.nr_sequencia	= nr_seq_tit_adiant_p;		
			
	end if;
	
	end;
end loop;
close C01;

commit;

end atualiza_tributo_adiant_mx;
/