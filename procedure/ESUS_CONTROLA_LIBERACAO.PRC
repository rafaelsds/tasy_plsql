create or replace
procedure esus_controla_liberacao(
			nm_tabela_p		varchar2,
			nr_sequencia_p		varchar2,
			nm_usuario_p 		varchar2,
			ie_acao_p		varchar2) is 
		
ds_comando_w varchar2(255);
begin

if (ie_acao_p = 'L') then
	begin
		ds_comando_w := ' update ' || nm_tabela_p ||
		' set 	dt_liberacao	=  sysdate, ' ||
		'		dt_atualizacao 	=  sysdate, ' ||
		'		nm_usuario = ''' || nm_usuario_p ||
		''' where 	nr_sequencia 	= ' || nr_sequencia_p;
	end;
else
	begin
		ds_comando_w := ' update ' || nm_tabela_p ||
		' set 	dt_liberacao	=  null, ' ||
		'		dt_atualizacao 	=  sysdate, ' ||
		'		nm_usuario = ''' || nm_usuario_p  ||
		''' where 	nr_sequencia 	= ' || nr_sequencia_p;
	end;
end if;


exec_sql_dinamico(nm_usuario_p,ds_comando_w);

commit;

end esus_controla_liberacao;
/
