create or replace
procedure pls_sib_desfazer_confirmacao
			(	nr_sequencia_p		number,
				nm_usuario_p		varchar2) is 

qt_lote_re_envio_w		number(10);				
				
begin

select	count(*)
into	qt_lote_re_envio_w
from	pls_lote_sib
where	nr_seq_lote_envio  = nr_sequencia_p;


if	(qt_lote_re_envio_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(262442, 'NR_SEQ_LOTE=' || nr_sequencia_p);
	/* Mensagem: O lote NR_SEQ_LOTE possui sub-lotes! Favor verificar. */	
else	update	pls_lote_sib
	set	dt_envio	= null,
		ie_status	= 'A',
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
end if;

commit;	

end pls_sib_desfazer_confirmacao;
/