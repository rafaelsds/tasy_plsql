create or replace procedure desvincular_bordero_nf
  (nr_sequencia_p in number,
  nm_usuario_p  in varchar2)  is

cursor c01 is 
	select 	nr_titulo
	from 	bordero_tit_rec
	where 	nr_bordero = nr_sequencia_p;

begin

for bordero_tit_rec in c01 loop
	update 	titulo_receber
	set 	nr_nota_fiscal = null,
			NM_USUARIO = nm_usuario_p,
			DT_ATUALIZACAO = sysdate
	where 	nr_titulo = bordero_tit_rec.nr_titulo;
end loop;

update  BORDERO_RECEBIMENTO
set     NR_SEQ_NOTA_FISCAL = null,
        NM_USUARIO = nm_usuario_p,
        DT_ATUALIZACAO = sysdate
where   NR_BORDERO = nr_sequencia_p;

commit;

end;
/
