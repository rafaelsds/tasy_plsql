create or replace
procedure  alterar_vencedor_cotacao(
				cd_material_p			number,
				nr_cot_compra_p			number,
				nr_item_cot_compra_p		number,
				nr_seq_fornec_ant_p		number,
				nr_seq_fornecedor_p		number,
				ds_justificativa_p			varchar2,
				nm_usuario_p			varchar2,
				ie_motivo_alter_venc_p		varchar2 default '') is

cd_cgc_w		varchar2(14);
cd_cgc_ww		varchar2(14);
nr_seq_cot_item_forn_w	number(10);
ds_motivo_venc_alt_w	varchar2(4000);
nm_fornecedor_ant_w	varchar2(80);
nm_fornecedor_w		varchar2(80);

begin

select	nvl(max(cd_cgc_fornecedor),'')
into	cd_cgc_w
from	cot_compra_forn
where	nr_cot_compra	= nr_cot_compra_p
and	nr_sequencia	= nr_seq_fornec_ant_p;

nm_fornecedor_ant_w	:= substr(obter_nome_pf_pj(null, cd_cgc_w),1,80);

select	nvl(max(cd_cgc_fornecedor),'')
into	cd_cgc_ww
from	cot_compra_forn
where	nr_cot_compra	= nr_cot_compra_p
and	nr_sequencia	= nr_seq_fornecedor_p;

nm_fornecedor_w		:= substr(obter_nome_pf_pj(null, cd_cgc_ww),1,80);

ds_motivo_venc_alt_w	:= substr(
			WHEB_MENSAGEM_PCK.get_texto(297139,
						'NM_USUARIO_P='||NM_USUARIO_P||
						';DT_ATUAL_W='||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')||
						';NM_FORNECEDOR_ANT_W='||NM_FORNECEDOR_ANT_W||
						';NM_FORNECEDOR_W='||NM_FORNECEDOR_W),1,4000);

if	(ds_justificativa_p is not null) then
	ds_motivo_venc_alt_w	:= substr(ds_motivo_venc_alt_w || chr(13) || chr(10) ||
				ds_justificativa_p,1,4000);
end if;

select	max(nr_sequencia)
into	nr_seq_cot_item_forn_w
from	cot_compra_forn_item
where	nr_cot_compra			= nr_cot_compra_p
and	nr_item_cot_compra		= nr_item_cot_compra_p
and	nr_seq_cot_forn			= nr_seq_fornecedor_p
and	cd_material			= cd_material_p;

update	cot_compra_item
set	cd_cgc_fornecedor_venc_alt		= cd_cgc_ww,
	cd_cgc_fornecedor_venc_sis		= cd_cgc_w,
	nr_seq_cot_item_forn		= nr_seq_cot_item_forn_w,
	dt_atualizacao			= sysdate,
	ds_motivo_venc_alt			= decode(substr(ds_motivo_venc_alt,1,4000), null,substr(ds_motivo_venc_alt_w,1,4000), substr(ds_motivo_venc_alt || chr(13) || chr(10) || chr(13) || chr(10) || ds_motivo_venc_alt_w,1,4000)),
	nm_usuario			= nm_usuario_p,
	ie_motivo_alter_venc		= ie_motivo_alter_venc_p
where	nr_cot_compra			= nr_cot_compra_p
and	nr_item_cot_compra		= nr_item_cot_compra_p;

gerar_hist_cotacao_sem_commit(
	nr_cot_compra_p, 
	WHEB_MENSAGEM_PCK.get_texto(297145),
	substr(	WHEB_MENSAGEM_PCK.get_texto(297147,
				'CD_MATERIAL_P='||CD_MATERIAL_P||
				';NM_FORNECEDOR_ANT_W='||NM_FORNECEDOR_ANT_W||
				';NM_FORNECEDOR_W='||NM_FORNECEDOR_W||
				';DS_JUSTIFICATIVA_P='||DS_JUSTIFICATIVA_P),1,4000),
	'S',
	nm_usuario_p);


commit;

end Alterar_vencedor_Cotacao;
/
