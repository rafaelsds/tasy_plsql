create or replace
procedure Definir_Senha_Medico(
			ie_senha_p		Number,
			nr_dias_p		Varchar2,
			cd_pessoa_fisica_p	Varchar2) is 

nm_usuario_w	Varchar2(15);			
			
begin

select	nvl(max(nm_usuario),'0')
into	nm_usuario_w
from	usuario
where	cd_pessoa_fisica = cd_pessoa_fisica_p
and	ie_situacao = 'A';

if	(nm_usuario_w <> '0') then

	if	(ie_senha_p = 0) then
		update  usuario
		set     ds_senha            = nm_usuario_w,
			dt_alteracao_senha  = null,
			dt_validade_usuario = decode(dt_validade_usuario,null,null, dt_validade_usuario + nvl(to_char(somente_numero(nr_dias_p)),0))
		where   cd_pessoa_fisica    = cd_pessoa_fisica_p;
	elsif	(ie_senha_p = 1) then
		update  usuario
		set     ds_senha            = cd_pessoa_fisica_p,
			dt_alteracao_senha  = null,
			dt_validade_usuario = decode(dt_validade_usuario,null,null, dt_validade_usuario + nvl(to_char(somente_numero(nr_dias_p)),0))
		where   cd_pessoa_fisica    = cd_pessoa_fisica_p;

	end if;	
	
end if;
	
commit;

end Definir_Senha_Medico;
/
