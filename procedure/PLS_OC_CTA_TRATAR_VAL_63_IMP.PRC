create or replace
procedure pls_oc_cta_tratar_val_63_imp(	nr_seq_combinada_p		in pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p		in pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p		in pls_oc_cta_selecao_imp.nr_id_transacao%type) is 

tb_seq_selecao_w	pls_util_cta_pck.t_number_table;
tb_valido_w		pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w		pls_util_cta_pck.t_varchar2_table_4000;
ds_rest_conta_w		varchar2(1000);
ds_rest_conta_imp_w	varchar2(1000);
ds_sql_w		varchar2(4000);
cd_cursor_w		sql_pck.t_cursor;
					
				
-- Dados das regras.
cursor	C01 (	nr_seq_oc_cta_comb_pc	pls_oc_cta_combinada.nr_sequencia%type) is
	select	nr_sequencia,
		ie_numero_guia,
		nvl(ie_considerar_auto,'N') ie_considerar_auto,
		nvl(ie_considera_horario, 'S') ie_considera_horario
	from	pls_oc_cta_val_cta_per_int regra
	where	regra.nr_seq_oc_cta_comb = nr_seq_combinada_p;
	
--
function obter_restricao_internacao(	ie_momento_p		varchar2,
					ie_numero_guia_p	pls_oc_cta_val_cta_per_int.ie_numero_guia%type,
					ie_considera_horario_p	pls_oc_cta_val_cta_per_int.ie_considera_horario%type)
					return varchar2 is
/*
Finalidade:	Montar o acesso de pesquisa a conta de interna��o quando o benefici�rio estiver internado.
*/ 
ds_rest_conta_w	varchar2(1000);
begin

-- Sempre ser�o verificadas apenas contas do mesmo benefici�rio e sempre desconsidera a mesma conta.
ds_rest_conta_w	:= 	' and	x.nr_seq_segurado = conta.nr_seq_segurado ' || pls_util_pck.enter_w ||
			' and	x.nr_sequencia <> conta.nr_sequencia ' || pls_util_pck.enter_w ||
			' and 	x.ie_status not in (''C'') ';

-- Verificar se estamos buscando as guias de interna��o ou os demais atendimentos.
if	(ie_momento_p = 'INTER') then
	
	-- Sempre que for buscar as interna��es ser� visto se a conta sendo consistida foi realizada dentro do per�odo da interna��o.
	if	(ie_considera_horario_p = 'N') then
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and 	x.ie_tipo_guia = ''5'' ' || pls_util_pck.enter_w ||
					' and	x.dt_entrada_trunc <= trunc(conta.dt_atendimento_conv,''dd'') ' || pls_util_pck.enter_w ||
					' and	(x.dt_alta_trunc is null or x.dt_alta_trunc >= trunc(conta.dt_atendimento_conv,''dd'')) ';
	else
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and 	x.ie_tipo_guia = ''5'' ' || pls_util_pck.enter_w ||
					' and	x.dt_entrada <= conta.dt_atendimento_conv ' || pls_util_pck.enter_w ||
					' and	(x.dt_alta is null or x.dt_alta >= conta.dt_atendimento_conv) ';
	end if;
else
	-- Sempre que for buscar o que n�o for interna��o ent�o verifica se o atendimento foi realizado no per�odo da interna��o sendo consistida.
	if	(ie_considera_horario_p = 'N') then
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and 	(x.ie_tipo_guia <> ''5'' and x.cd_tiss_atendimento <> ''7'') ' || pls_util_pck.enter_w ||
					' and	x.dt_atendimento_dd between trunc(conta.dt_inicio_faturamento,''dd'') and nvl(trunc(conta.dt_fim_faturamento,''dd''), x.dt_atendimento_dd) ';
	else
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and 	(x.ie_tipo_guia <> ''5'' and x.cd_tiss_atendimento <> ''7'') ' || pls_util_pck.enter_w ||
					' and	x.dt_atendimento between conta.dt_inicio_faturamento and nvl(conta.dt_fim_faturamento, x.dt_atendimento) ';
	end if;
end if;

-- Valida apenas contas com mesmo n�mero de guia
if	(ie_numero_guia_p = 'M') then
	
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	x.cd_guia_referencia = conta.cd_guia_referencia ';
				
-- Valida apenas contas com n�mero de guia diferente
elsif	(ie_numero_guia_p = 'D') then	
	
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	x.cd_guia_referencia <> conta.cd_guia_referencia ' || pls_util_pck.enter_w ||
				' and	nvl(x.nr_seq_guia,0) <> nvl(conta.nr_seq_guia,0) ';
end if;

return ds_rest_conta_w;

end obter_restricao_internacao;

function obter_restricao_internacao_imp(ie_momento_p		varchar2,
					ie_numero_guia_p	pls_oc_cta_val_cta_per_int.ie_numero_guia%type,
					ie_considera_horario_p	pls_oc_cta_val_cta_per_int.ie_considera_horario%type)
					return varchar2 is
/*
Finalidade:	Montar o acesso de pesquisa a conta de interna��o quando o benefici�rio estiver internado.
*/ 
ds_rest_conta_w	varchar2(1000);
begin

-- Sempre ser�o verificadas apenas contas do mesmo benefici�rio e sempre desconsidera a mesma conta.
ds_rest_conta_w	:= 	' and	x.nr_seq_segurado_conv = conta.nr_seq_segurado_conv ' || pls_util_pck.enter_w ||
			' and	x.nr_sequencia <> conta.nr_sequencia ';

-- Verificar se estamos buscando as guias de interna��o ou os demais atendimentos.
if	(ie_momento_p = 'INTER') then
	
	-- Sempre que for buscar as interna��es ser� visto se a conta sendo consistida foi realizada dentro do per�odo da interna��o.
	if	(ie_considera_horario_p = 'N') then
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and 	a.ie_tipo_guia = ''5'' ' || pls_util_pck.enter_w ||
					' and	x.dt_inicio_faturamento_trunc <= trunc(conta.dt_atendimento_conv,''dd'') ' || pls_util_pck.enter_w ||
					' and	(x.dt_fim_faturamento_trunc is null or x.dt_fim_faturamento_trunc >= trunc(conta.dt_atendimento_conv,''dd'')) ';
	else
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and 	a.ie_tipo_guia = ''5'' ' || pls_util_pck.enter_w ||
					' and	x.dt_inicio_faturamento <= conta.dt_atendimento_conv ' || pls_util_pck.enter_w ||
					' and	(x.dt_fim_faturamento is null or x.dt_fim_faturamento >= conta.dt_atendimento_conv) ';
	end if;
else
	-- Sempre que for buscar o que n�o for interna��o ent�o verifica se o atendimento foi realizado no per�odo da interna��o sendo consistida.
	if	(ie_considera_horario_p = 'N') then
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and (a.ie_tipo_guia <> ''5'' and x.cd_tiss_atendimento <> ''7'') ' || pls_util_pck.enter_w ||
					' and	x.dt_atendimento_conv_trunc between trunc(conta.dt_inicio_faturamento,''dd'') and nvl(trunc(conta.dt_fim_faturamento,''dd''), x.dt_atendimento_conv_trunc) ';
	else
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and (a.ie_tipo_guia <> ''5'' and x.cd_tiss_atendimento <> ''7'') ' || pls_util_pck.enter_w ||
					' and	x.dt_atendimento_conv between conta.dt_inicio_faturamento and nvl(conta.dt_fim_faturamento, x.dt_atendimento_conv) ';
	end if;

end if;

-- Valida apenas contas com mesmo n�mero de guia
if	(ie_numero_guia_p = 'M') then
	
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	x.cd_guia_referencia = conta.cd_guia_referencia ';
				
-- Valida apenas contas com n�mero de guia diferente
elsif	(ie_numero_guia_p = 'D') then	
	
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	x.cd_guia_referencia <> conta.cd_guia_referencia ' || pls_util_pck.enter_w ||
				' and	nvl(x.nr_seq_guia,0) <> nvl(conta.nr_seq_guia,0) ';
end if;

return ds_rest_conta_w;

end obter_restricao_internacao_imp;


function obter_restricao_inter_auto(	ie_numero_guia_p	pls_oc_cta_val_cta_per_int.ie_numero_guia%type,
					ie_considera_horario_p	pls_oc_cta_val_cta_per_int.ie_considera_horario%type)	return varchar2 is
/*
Finalidade:	Montar o acesso de pesquisa a guia de autoriza��o de interna��o quando o benefici�rio estiver internado.
*/ 
ds_rest_conta_w	varchar2(1000);
begin
ds_rest_conta_w	:= 	' and	x.nr_seq_segurado = conta.nr_seq_segurado ' || pls_util_pck.enter_w ||
			' and	x.dt_cancelamento is null '|| pls_util_pck.enter_w || -- N�o considera as guias canceladas
			' and	x.ie_tipo_guia = ''1'' ' || pls_util_pck.enter_w || -- Retona apenas as guias de solicita��o de interna��o 
			' and	x.ie_status = ''1'' '; -- Retona apenas as guias autorizadas

			
-- Valida apenas contas com mesmo n�mero de guia
if	(ie_numero_guia_p = 'M') then
	
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	x.nr_sequencia = conta.cd_guia_referencia ';
				
-- Valida apenas contas com n�mero de guia diferente
elsif	(ie_numero_guia_p = 'D') then	

ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
			' and	x.nr_sequencia <> conta.cd_guia_referencia ' || pls_util_pck.enter_w ||
			' and	nvl(x.nr_sequencia,0) <> nvl(conta.nr_seq_guia,0) ';
	
end if;

-- Sempre que for buscar as autoriza��es de interna��es ser� visto se a conta sendo consistida foi realizada dentro do per�odo da interna��o.
-- caso a data de alta da autoriza��o n�o estiver preenchida, ser� considerado o dia de interna��o somada com as diarias autorizadas
-- � necess�rio ainda considerar as diarias acumuladas de todas as possiveis guias de solicita��o de prorroga��o de interna��o
if	(ie_considera_horario_p = 'N') then
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	trunc(y.dt_internacao,''dd'') <= trunc(conta.dt_atendimento_referencia,''dd'') ' || pls_util_pck.enter_w ||
				' and	(nvl(trunc(y.dt_alta,''dd''),  trunc(y.dt_internacao,''dd'') + nvl(x.qt_dia_autorizado,1) + '  || pls_util_pck.enter_w ||
				'									nvl((	select	sum(nvl(z.qt_dia_autorizado,1)) ' || pls_util_pck.enter_w ||
				'										from	pls_guia_plano		z ' || pls_util_pck.enter_w ||
				'										where	z.nr_seq_guia_principal	= x.nr_sequencia ' || pls_util_pck.enter_w ||
				'										and	z.dt_cancelamento is null '|| pls_util_pck.enter_w || -- N�o considera as guias canceladas
				'										and	z.ie_tipo_guia = ''8'' ' || pls_util_pck.enter_w ||  -- Retona apenas as guias de solicita��o de prorroga��o de interna��o
				'										and	z.ie_status = ''1'' ), 0)' || pls_util_pck.enter_w ||  -- Retona apenas as guias autorizadas
				') >= trunc(conta.dt_atendimento_referencia,''dd'')) ';
else
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	y.dt_internacao <= conta.dt_atendimento_referencia ' || pls_util_pck.enter_w ||
				' and	(nvl(y.dt_alta,  y.dt_internacao + nvl(x.qt_dia_autorizado,1) + nvl((	select	sum(nvl(z.qt_dia_autorizado,1)) ' || pls_util_pck.enter_w ||
				'										from	pls_guia_plano		z ' || pls_util_pck.enter_w ||
				'										where	z.nr_seq_guia_principal	= x.nr_sequencia ' || pls_util_pck.enter_w ||
				'										and	z.dt_cancelamento is null '|| pls_util_pck.enter_w || -- N�o considera as guias canceladas
				'										and	z.ie_tipo_guia = ''8'' ' || pls_util_pck.enter_w ||  -- Retona apenas as guias de solicita��o de prorroga��o de interna��o
				'										and	z.ie_status = ''1'' ), 0)' || pls_util_pck.enter_w ||  -- Retona apenas as guias autorizadas
				') >= conta.dt_atendimento_referencia) ';
end if;

return ds_rest_conta_w;

end obter_restricao_inter_auto;


begin
-- Verificar se a regra informada � v�lida.
if	(nr_seq_combinada_p is not null) then	

	-- Percorrer as regra cadastradas para a valida��o.
	for	r_C01_w in C01(nr_seq_combinada_p) loop
	
		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', 'N', nr_id_transacao_p, null);
		
		begin 
			-- Primeiro vai atr�s do que n�o � interna��o e est� sendo consistido para veriricar se tinha outra conta no per�odo.
			ds_rest_conta_w := obter_restricao_internacao('INTER', r_C01_w.ie_numero_guia, r_C01_w.ie_considera_horario);
			ds_rest_conta_imp_w	:= obter_restricao_internacao_imp('INTER', r_C01_w.ie_numero_guia, r_C01_w.ie_considera_horario);
			
			ds_sql_w	:= 	'select ''S'' ie_valido, ' || pls_util_pck.enter_w || 
						'	''O benefici�rio estava internado no per�odo informado.'' || chr(13) || chr(10) || ' || pls_util_pck.enter_w || 
						'	''Conta interna��o: '' || ' || pls_util_pck.enter_w || 
						'	(select	min(x.nr_sequencia) ' || pls_util_pck.enter_w || 
						'	from	pls_conta_imp x ' || pls_util_pck.enter_w || 
						'	where	1 = 1 ' || pls_util_pck.enter_w ||
						ds_rest_conta_w || pls_util_pck.enter_w ||
						'	) ds_observacao, ' || pls_util_pck.enter_w ||
						'	pls_tipos_ocor_pck.obter_sequencia_selecao( ' || pls_util_pck.enter_w ||
						'	null, null, conta.nr_sequencia, :nr_id_transacao, ''N'', null, :ie_excecao, ''V'') seqs ' || pls_util_pck.enter_w ||
						'from	pls_oc_cta_selecao_imp sel, ' || pls_util_pck.enter_w ||
						'	pls_conta_imp conta, ' || pls_util_pck.enter_w ||
						'	pls_protocolo_conta_imp proc ' || pls_util_pck.enter_w ||
						'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_util_pck.enter_w ||
						'and	sel.ie_valido = ''S'' ' || pls_util_pck.enter_w ||
						'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_util_pck.enter_w ||
						'and 	conta.nr_seq_protocolo = proc.nr_sequencia'	|| pls_util_pck.enter_w ||
						-- neste primeiro momento verifica tudo que n�o for interna��o para barrar caso o benefici�rio estava internado.
						'and	proc.ie_tipo_guia <> ''5'' ' || pls_util_pck.enter_w ||
						'and	exists (select	1 ' || pls_util_pck.enter_w ||
						'		from	pls_conta_v x ' || pls_util_pck.enter_w ||
						'		where	1 = 1 ' || pls_util_pck.enter_w ||
						'	' || ds_rest_conta_w || pls_util_pck.enter_w || 
						'		union all ' || pls_util_pck.enter_w ||
						'		select	1 ' || pls_util_pck.enter_w ||
						'		from	pls_conta_imp_v x ' || pls_util_pck.enter_w ||
						'			pls_protocolo_conta_imp a ' || pls_util_pck.enter_w ||
						'		where	1 = 1 ' || pls_util_pck.enter_w ||
						'		and 	x.nr_seq_protocolo = a.nr_sequencia'	|| pls_util_pck.enter_w ||
						'	' || ds_rest_conta_imp_w || pls_util_pck.enter_w ||
						'	) ';
			
			if	(r_C01_w.ie_considerar_auto = 'S') then
			
				ds_rest_conta_w		:= obter_restricao_inter_auto(r_C01_w.ie_numero_guia, r_C01_w.ie_considera_horario);
				
				ds_sql_w	:= 	ds_sql_w || pls_util_pck.enter_w || 
							'union all ' || pls_util_pck.enter_w || 
							pls_util_pck.enter_w || 'select ''S'' ie_valido, ' || pls_util_pck.enter_w || 
							'	''O benefici�rio estava internado no per�odo informado.'' || chr(13) || chr(10) || ' || pls_util_pck.enter_w || 
							'	''Autoriza��o interna��o: '' || ' || pls_util_pck.enter_w || 
							'	(select	min(x.nr_sequencia) ' || pls_util_pck.enter_w || 
							'	from	pls_guia_plano x, ' || pls_util_pck.enter_w || 
							'		pls_guia_atendimento	y ' || pls_util_pck.enter_w || 
							'	where	1 = 1 ' || pls_util_pck.enter_w ||
							'		and	y.nr_seq_guia	= x.nr_sequencia ' || pls_util_pck.enter_w ||
							ds_rest_conta_w || pls_util_pck.enter_w ||
							'	) ds_observacao, ' || pls_util_pck.enter_w ||
							'	pls_tipos_ocor_pck.obter_sequencia_selecao( ' || pls_util_pck.enter_w ||
							'	null, null, conta.nr_sequencia, '||to_char(nr_id_transacao_p)||', ''N'', null, '''||ie_regra_excecao_p||''', ''V'') seqs ' || pls_util_pck.enter_w ||
							'from	pls_oc_cta_selecao_imp	sel, ' || pls_util_pck.enter_w ||
							'	pls_conta_imp		conta, ' || pls_util_pck.enter_w ||
							'	pls_protocolo_conta_imp proc ' || pls_util_pck.enter_w ||
							'where	sel.nr_id_transacao = '||to_char(nr_id_transacao_p)||' ' || pls_util_pck.enter_w ||
							'and	sel.ie_valido = ''S'' ' || pls_util_pck.enter_w ||
							'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_util_pck.enter_w ||
							'and 	conta.nr_seq_protocolo = proc.nr_sequencia'	|| pls_util_pck.enter_w ||
							-- neste primeiro momento verifica tudo que n�o for interna��o para barrar caso o benefici�rio estava internado.
							'and	proc.ie_tipo_guia <> ''5'' ' || pls_util_pck.enter_w ||
							'and	exists (select	1 ' || pls_util_pck.enter_w ||
							'		from	pls_guia_plano 		x, ' || pls_util_pck.enter_w ||
							'			pls_guia_atendimento	y ' || pls_util_pck.enter_w ||
							'		where	1 = 1 ' || pls_util_pck.enter_w ||
							'		and	y.nr_seq_guia	= x.nr_sequencia ' || pls_util_pck.enter_w ||
							'	' || ds_rest_conta_w || pls_util_pck.enter_w || 
							'	) ';
			
			end if;
			
			open	cd_cursor_w for ds_sql_w
			using	nr_id_transacao_p, ie_regra_excecao_p, nr_id_transacao_p;
			loop
			
				fetch cd_cursor_w 
				bulk collect into tb_valido_w, tb_observacao_w, tb_seq_selecao_w
				limit pls_cta_consistir_pck.qt_registro_transacao_w;
				exit when tb_seq_selecao_w.count = 0;
				
				--Grava as informa��es na tabela de sele��o
				pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, 
										tb_valido_w,
										tb_observacao_w, 
										nr_id_transacao_p, 
										'LISTA');
				--limpa as vari�veis
				pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
									tb_valido_w,
									tb_observacao_w);
				
			end loop;
			close cd_cursor_w;
			

			-- Agora vai atr�s das contas de interna��o que est�o sendo consistidas para gerar a ocorr�ncia.
			ds_rest_conta_w := obter_restricao_internacao('OUTROS', r_C01_w.ie_numero_guia, r_C01_w.ie_considera_horario);
			ds_rest_conta_imp_w	:= obter_restricao_internacao_imp('OUTROS', r_C01_w.ie_numero_guia, r_C01_w.ie_considera_horario);
			
			ds_sql_w	:= 	'select ''S'' ie_valido, ' || pls_util_pck.enter_w || 
						'	''O benefici�rio pode ter realizado outro atendimento durante o per�odo em que estava internado.'' || chr(13) || chr(10) || ' || pls_util_pck.enter_w || 
						'	''Conta : '' || ' || pls_util_pck.enter_w || 
						'	(select	min(x.nr_sequencia) ' || pls_util_pck.enter_w || 
						'	from	pls_conta_v x ' || pls_util_pck.enter_w || 
						'	where	1 = 1 ' || pls_util_pck.enter_w ||
						ds_rest_conta_w || pls_util_pck.enter_w ||
						'	) ds_observacao, ' || pls_util_pck.enter_w ||
						'	pls_tipos_ocor_pck.obter_sequencia_selecao( ' || pls_util_pck.enter_w ||
						'	null, null, conta.nr_sequencia, :nr_id_transacao, ''N'', null, :ie_excecao, ''V'') seqs ' || pls_util_pck.enter_w ||
						'from	pls_oc_cta_selecao_ocor_v sel, ' || pls_util_pck.enter_w ||
						'	pls_conta_imp		conta, ' || pls_util_pck.enter_w ||
						'	pls_protocolo_conta_imp proc ' || pls_util_pck.enter_w ||
						'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_util_pck.enter_w ||
						'and	sel.ie_valido = ''S'' ' || pls_util_pck.enter_w ||
						'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_util_pck.enter_w ||
						'and 	conta.nr_seq_protocolo = proc.nr_sequencia'	|| pls_util_pck.enter_w ||
						-- neste momento verifica tudo que for interna��o para que n�o seja poss�vel apresentar contas de interna��o caso
						-- o benefici�rio tiver outra conta neste per�odo.
						'and	proc.ie_tipo_guia <> ''5'' ' || pls_util_pck.enter_w ||
						'and	exists (select	1 ' || pls_util_pck.enter_w ||
						'		from	pls_conta_v x ' || pls_util_pck.enter_w ||
						'		where	1 = 1 ' || pls_util_pck.enter_w ||
						'	' || ds_rest_conta_w || pls_util_pck.enter_w || 
						'		union all ' || pls_util_pck.enter_w ||
						'		select	1 ' || pls_util_pck.enter_w ||
						'		from	pls_conta_imp_v x ' || pls_util_pck.enter_w ||
						'			pls_protocolo_conta_imp a ' || pls_util_pck.enter_w ||
						'		where	1 = 1 ' || pls_util_pck.enter_w ||
						'		and 	x.nr_seq_protocolo = a.nr_sequencia'	|| pls_util_pck.enter_w ||
						'	' || ds_rest_conta_imp_w || pls_util_pck.enter_w ||
						'	) ';
						
						
					
			open	cd_cursor_w for ds_sql_w
			using	nr_id_transacao_p, ie_regra_excecao_p, nr_id_transacao_p;
			loop
				fetch cd_cursor_w 
				bulk collect into tb_valido_w, tb_observacao_w, tb_seq_selecao_w
				limit pls_cta_consistir_pck.qt_registro_transacao_w;
				exit when tb_seq_selecao_w.count = 0;
				
				--Grava as informa��es na tabela de sele��o
				pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, 
										tb_valido_w,
										tb_observacao_w, 
										nr_id_transacao_p, 
										'LISTA');
				--limpa as vari�veis
				pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
									tb_valido_w,
									tb_observacao_w);
			end loop;
			close cd_cursor_w;
			
		exception
		when others then
			
			-- Verifica se o cursor ainda est� aberto para fech�-lo.
			if	(cd_cursor_w%isopen) then
				
				close cd_cursor_w;
			end if;
			
			-- E gravar o log de erro.
			pls_ocor_imp_pck.trata_erro_sql_dinamico(	nr_seq_combinada_p,
									null,
									ds_sql_w,
									nr_id_transacao_p,
									wheb_usuario_pck.get_nm_usuario,
									'N');
		end;
	end loop; -- C01
	
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_ocor_imp_pck.atualiza_campo_valido (	'V', 
							'N', 
							ie_regra_excecao_p, 
							null,
							nr_id_transacao_p, 
							null);
end if;

end pls_oc_cta_tratar_val_63_imp;
/
