create or replace
procedure pls_material_alterar_estrutura
			(	nm_usuario_p		varchar2,
				nr_seq_estrutura_p	number,
				nr_sequencia_p		number) is 
				
begin

update	pls_material
set	nr_seq_estrut_mat	= nr_seq_estrutura_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia      	= nr_sequencia_p;

commit;	

end pls_material_alterar_estrutura;
/