create or replace
procedure pls_desfazer_subst_item_ptu
			(	nr_seq_item_ref_p	number,
				nr_seq_grupo_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				ie_commitar_p		varchar2	) is

nr_seq_analise_item_w		number(10);
nr_seq_conta_w			number(10);
ie_tipo_item_w			varchar2(2);
nr_seq_analise_w		number(10);
cd_item_w			number(10);
ds_item_w			varchar2(255);
nr_seq_glosa_oc_w		number(10);
nr_seq_item_w			number(10);
ie_substituido_w		varchar2(2);
nr_seq_item_criado_w		number(10);
nr_seq_criado_subst_w		number(10);
nr_seq_item_partic_w		number(10);
nr_seq_item_pos_w		number(10);
nr_seq_analise_pos_w		number(10);

nr_seq_item_ww			number(10);
ie_tipo_item_ww			varchar2(2);
nr_seq_conta_ww			number(10);
nr_seq_glosa_oc_ww		number(10);

nr_seq_glosa_w			number(10);
nr_seq_ocorrencia_w		number(10);
				
Cursor C01 is
	select	nr_sequencia,
		ie_substituido
	from	pls_analise_conta_item
	where	nr_seq_conta = nr_seq_conta_w	
	and	((nr_seq_conta_proc = nr_seq_item_w and ie_tipo_item_w = 'P') or (nr_seq_conta_mat = nr_seq_item_w and ie_tipo_item_w = 'M'))
	order by 1;

/*Obter os participantes do procedimento*/
Cursor C02 is
	select	nr_sequencia
	from	w_pls_resumo_conta
	where	nr_seq_item  = nr_seq_item_w
	and	ie_tipo_item = 'R';	

Cursor C03 is
	select	nr_sequencia	
	from	w_pls_resumo_conta
	where	nr_seq_res_conta_princ = nr_seq_analise_item_w;	
	
Cursor C04 is
	select	nr_sequencia,
		nr_seq_glosa,
		nr_seq_ocorrencia
	from	pls_analise_conta_item
	where	nr_seq_conta_proc = nr_seq_item_ww
	order by 1;
	
Cursor C05 is
	select	nr_sequencia
	from	w_pls_resumo_conta
	where	nr_seq_item_ref  = nr_seq_item_ref_p
	and	ie_tipo_item = 'R';
	
begin

/*Obter todos os participantes referentes (n�o devem ser apagados e sim retornar ao status)*/
open C05;
loop
fetch C05 into	
	nr_seq_analise_item_w;
exit when C05%notfound;
	begin	
	
	/*Obter os dados dos participantes referenciados que devem retornar*/
	select	nr_seq_item,
		ie_tipo_item,
		cd_item,
		decode(nvl(ie_origem_proced,0),0,pls_obter_desc_material(cd_item),obter_descricao_procedimento(cd_item,ie_origem_proced)),
		nr_seq_analise,
		nr_seq_conta
	into	nr_seq_item_w,
		ie_tipo_item_w,
		cd_item_w,
		ds_item_w,
		nr_seq_analise_w,
		nr_seq_conta_w
	from	w_pls_resumo_conta
	where	nr_sequencia = nr_seq_analise_item_w;

	/*Obter a sequencia do novo item e participantes que foi inserido em substitui��o*/
	open C03;
	loop
	fetch C03 into	
		nr_seq_item_criado_w;
	exit when C03%notfound;
		begin
		
		/*Obter se o pr�prio item criado foi substituido*/
		select	max(nr_sequencia)
		into	nr_seq_criado_subst_w
		from	w_pls_resumo_conta
		where	nr_seq_res_conta_princ = nr_seq_item_criado_w;

		/*Se o pr�prio item criado foi substituido o item*/
		if	(nvl(nr_seq_criado_subst_w,0) > 0) then	
			pls_desfazer_subst_item_ptu(nr_seq_item_criado_w, nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p, 'N');
		end if;

		select	nr_seq_item,
			ie_tipo_item,				
			nr_seq_conta
		into	nr_seq_item_ww,
			ie_tipo_item_ww,		
			nr_seq_conta_ww
		from	w_pls_resumo_conta
		where	nr_sequencia = nr_seq_item_criado_w;
		
		/*Obter as glosas/oc dos itens criados*/
		open C04;
		loop
		fetch C04 into	
			nr_seq_glosa_oc_ww,
			nr_seq_glosa_w,
			nr_seq_ocorrencia_w;
		exit when C04%notfound;
			begin				
			
			delete	pls_ocorrencia_benef
			where	nr_seq_glosa = nr_seq_glosa_w;
			
			delete	pls_conta_glosa
			where	nr_sequencia    = nr_seq_glosa_w;
			
			delete	pls_conta_glosa
			where	nr_seq_ocorrencia_benef = nr_seq_ocorrencia_w;
			
			delete	pls_ocorrencia_benef
			where	nr_sequencia    = nr_seq_ocorrencia_w;
			
			delete	pls_analise_parecer_item
			where	nr_seq_item	= nr_seq_glosa_oc_ww;
			
			delete	pls_analise_conta_item
			where	nr_sequencia	= nr_seq_glosa_oc_ww;
			
			end;
		end loop;
		close C04;
		
		/*Deletar o novo item que foi inserido em substitui��o*/
		delete  w_pls_resumo_conta
		where	nr_sequencia = nr_seq_item_criado_w;
				
		delete	pls_proc_participante
		where	nr_seq_conta_proc = nr_seq_item_ww;	
		
		delete	pls_conta_proc
		where	nr_sequencia = nr_seq_item_ww;	
		
		end;
	end loop;
	close C03;

	/*Atualizar o status para "Pendente de libera��o" e atualizar os valores  liberados para o pr� libera��o*/
	update	w_pls_resumo_conta
	set	ie_status		= 'P',
		vl_total		= vl_total_original,
		vl_unitario 		= vl_unitario_original,
		qt_liberado 		= qt_liberado_original,
		vl_total_original	= 0,
		vl_unitario_original 	= 0,
		qt_liberado_original 	= 0
	where	nr_sequencia 		= nr_seq_analise_item_w;

	/*Apagar os pareceres*/
	open C01;
	loop
	fetch C01 into	
		nr_seq_glosa_oc_w,
		ie_substituido_w;
	exit when C01%notfound;
		begin
		
		delete	pls_analise_parecer_item
		where	nr_seq_item = nr_seq_glosa_oc_w;
		
		update	pls_analise_conta_item
		set	ie_status	= 'P',
			ie_situacao	= 'A'
		where	nr_sequencia = nr_seq_glosa_oc_w
		and	ie_status <> 'I';
		
		/*Diego Os 307670 - No caso de ser um a glosa gerada na substitui��o ent�o o sistema deve apagar esta glosa*/
		if	(nvl(ie_substituido_w,'N') = 'S') then	
			delete	pls_analise_conta_item
			where	nr_sequencia = nr_seq_glosa_oc_w;
		end if;
		
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into	
		nr_seq_item_partic_w;
	exit when C02%notfound;
		begin
		
		update	w_pls_resumo_conta
		set	ie_status		= 'P',
			vl_total		= vl_total_original,
			vl_unitario 		= vl_unitario_original,
			qt_liberado 		= qt_liberado_original,
			vl_total_original	= 0,
			vl_unitario_original 	= 0,
			qt_liberado_original 	= 0
		where	nr_sequencia 		= nr_seq_item_partic_w;
		
		end;
	end loop;
	close C02;
	
	/*Inserir o hist�rico de an�lise*/
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_w, 16,
				 nr_seq_item_w, ie_tipo_item_w, null,
				 null, 'Desfeita a substitui��o do item retornando o item '||cd_item_w||' - '||ds_item_w||' para o status de "Pendente de libera��o"', 
				 nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);
				 
	begin
	select	nr_sequencia,
		nr_seq_analise
	into	nr_seq_item_pos_w,
		nr_seq_analise_pos_w
	from	w_pls_resumo_conta
	where	nr_seq_item_princ = nr_seq_analise_item_w;
	exception
	when others then
		nr_seq_item_pos_w	:= null;
		nr_seq_analise_pos_w	:= null;
	end;

	if	(nvl(nr_seq_item_pos_w,0) > 0) then
		/*Diego OS 402760 - Ao desfazer a substitui��o de um item na produ��o, se este existir na an�lise de p�s- tamb�m deve ser substituido.*/
		pls_desfazer_subst_item_ptu(nr_seq_item_pos_w, nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p, 'N');				 
	end if;	
	
	end;
end loop;
close C05;
			 
if	(nvl(ie_commitar_p,'S') = 'S') then
	commit;
end if;

end pls_desfazer_subst_item_ptu;
/