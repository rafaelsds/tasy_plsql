create or replace
procedure pep_parametros_oncologia_wdlg(	nr_seq_paciente_p	number,
					nr_ciclo_inicio_p	number,
					nr_ciclo_final_p	number,
					nr_seq_atendimento_p	number,
					nm_usuario_p		varchar2,
					ds_erro_p	out	varchar2) is
		
nr_seq_atendimento_w	number(10,0);
ds_erro_w		varchar2(2000);
		
cursor c01 is
select  nr_seq_atendimento
from    paciente_atendimento
where	nr_seq_paciente   =  nr_seq_paciente_p
and     nr_ciclo between nr_ciclo_inicio_p and nr_ciclo_final_p;

begin

open C01;
loop
fetch C01 into	
	nr_seq_atendimento_w;
exit when C01%notfound;
	begin
	
	gerar_agend_serv_pac_atend(nr_seq_paciente_p, nr_seq_atendimento_w, nm_usuario_p, ds_erro_w);	
	
	end;
end loop;
close C01;

commit;

end pep_parametros_oncologia_wdlg;
/