CREATE OR REPLACE PROCEDURE SET_TEC_ANEST_DATA (NR_SEQUENCIA_P IN NUMBER, 
                                                CD_PROFISSIONAL_P IN VARCHAR2,
                                                DT_INICIO_DH_P IN DATE, 
                                                DS_OBSERVACAO_P IN VARCHAR2,
                                                NM_USUARIO_P IN VARCHAR2) IS 

BEGIN
 
 UPDATE	CIRURGIA_TEC_ANESTESICA

	SET	DT_INICIO = DT_INICIO_DH_P,
		DT_FINAL = DT_INICIO_DH_P,
        CD_PROFISSIONAL = CD_PROFISSIONAL_P,
        DS_OBSERVACAO = DS_OBSERVACAO_P
        
	WHERE NR_SEQUENCIA = NR_SEQUENCIA_P;
    COMMIT;
    liberar_informacao('CIRURGIA_TEC_ANESTESICA', 'NR_SEQUENCIA', NR_SEQUENCIA_P, NM_USUARIO_P);
 
END SET_TEC_ANEST_DATA;
/
