create or replace
procedure pls_confirmar_envio_fat_geral(	nr_seq_fat_geral_p	ptu_fatura_geral.nr_sequencia%type,
						dt_envio_arquivo_p	date,
						nm_usuario_p		usuario.nm_usuario%type) is 

qt_registro_w		pls_integer;
					
begin

begin
qt_registro_w := length(trim(to_char(dt_envio_arquivo_p,'dd/mm/yyyy hh24:mi:ss')));
exception
when others then
	qt_registro_w := 0;
end;

if	(qt_registro_w < 19) then
	wheb_mensagem_pck.exibir_mensagem_abort(145962); -- Alertar quando data/hora est�o incorretos
	
elsif	(nr_seq_fat_geral_p is not null) and
	(dt_envio_arquivo_p is not null) then	
	update	ptu_fatura_geral
	set	dt_postagem		= dt_envio_arquivo_p,
		nm_usuario_postagem	= nm_usuario_p,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_fat_geral_p;	
end if;

commit;

end pls_confirmar_envio_fat_geral;
/