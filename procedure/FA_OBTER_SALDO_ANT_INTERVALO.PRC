create or replace
procedure fa_obter_saldo_ant_intervalo(	nr_seq_entrega_p	number,
					nr_seq_paciente_entr_p	number,
					cd_material_p		number,
					nm_usuario_p		Varchar2,
					ds_mensagem_p	out	varchar2,
					qt_saldo_ant_p	out	number,
					qt_dose_p		number
					) is 

qt_saldo_ant_w 		number(15,3);
qt_saldo_w 		number(15,3);
nr_sequencia_w		number(10);
nr_seq_receita_amb_w	number(10);
qt_dias_saldo_w		number(10);
ds_mensagem_w		varchar2(255);
dif_saldo_w		number(15,3);
cd_unidade_baixa_w	varchar2(30);
cd_unidade_baixa_rec_w	varchar2(30);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_receita_amb_ant_w number(10);
nr_receita_ant_w	varchar2(15);
ie_cancela_saldo_ant_w	varchar2(1);
ie_considera_pmc_w	varchar2(1);

begin

if (nr_seq_entrega_p is not null) then

	obter_param_usuario(10015, 3, obter_perfil_ativo, nm_usuario_p, 0, qt_dias_saldo_w);
	obter_param_usuario(10015, 39, obter_perfil_ativo, nm_usuario_p, nvl(wheb_usuario_pck.get_cd_estabelecimento,0), ie_cancela_saldo_ant_w);
	obter_param_usuario(10015, 82, obter_perfil_ativo, nm_usuario_p, nvl(wheb_usuario_pck.get_cd_estabelecimento,0), ie_considera_pmc_w);
		
	select	max(nr_seq_receita_amb),
		max(cd_pessoa_fisica)
	into	nr_seq_receita_amb_w,
		cd_pessoa_fisica_w
	from	fa_entrega_medicacao
	where	nr_sequencia	= nr_seq_entrega_p
	and	dt_cancelamento is null;

	select 	max(i.nr_sequencia),
		max(nr_seq_receita_amb)
	into	nr_sequencia_w,
		nr_seq_receita_amb_ant_w
	from	fa_entrega_medicacao f,
		fa_entrega_medicacao_item i
	where	f.nr_sequencia <> nr_seq_entrega_p
	and	f.nr_sequencia = i.nr_seq_fa_entrega
	and	trunc(f.dt_periodo_final) >= trunc(sysdate) - qt_dias_saldo_w
	and	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	i.cd_material = cd_material_p
	and	f.dt_cancelamento is null
	and	i.dt_cancelamento is null
	and	(ie_cancela_saldo_ant_w <> 'T' or nvl(i.ie_cancelar_saldo,'N') = 'N')
	AND   (EXISTS ( SELECT 1
			FROM 	fa_paciente_entrega x
			WHERE	x.nr_sequencia = f.nr_seq_paciente_entrega
			AND  	x.nr_seq_paciente_pmc	IS NULL
			) or (ie_considera_pmc_w = 'S'));

	qt_saldo_w	:= fa_obter_saldo_anterior(nr_seq_entrega_p, nr_seq_paciente_entr_p, cd_material_p, nm_usuario_p, qt_dose_p);
		
	if (nr_sequencia_w is not null) then
		select 	nvl(qt_saldo_atual,0),
			cd_unidade_baixa
		into	qt_saldo_ant_w,
			cd_unidade_baixa_rec_w
		from	fa_entrega_medicacao_item
		where	nr_sequencia = nr_sequencia_w;
		
		select  max(cd_unidade_medida)
		into	cd_unidade_baixa_w
		from 	fa_receita_farmacia_item
		where	nr_seq_receita 	= nr_seq_receita_amb_ant_w
		and	cd_material	= cd_material_p;
	
		qt_saldo_w	:= Obter_dose_convertida(cd_material_p, qt_saldo_w, cd_unidade_baixa_w, cd_unidade_baixa_rec_w);
		
		if (qt_saldo_w > 0) then
			if (nr_seq_receita_amb_ant_w <> nr_seq_receita_amb_w) then
				select	nr_receita
				into	nr_receita_ant_w
				from 	fa_receita_farmacia
				where	nr_sequencia = nr_seq_receita_amb_ant_w;
				ds_mensagem_w   := wheb_mensagem_pck.get_texto(307497,	'QT_SALDO_W=' || qt_saldo_w || ';' ||
																		'CD_UNIDADE_BAIXA_REC_W=' || cd_unidade_baixa_rec_w || ';' ||
																		'NR_RECEITA_ANT_W=' || nr_receita_ant_w);
									-- Considerado o saldo de #@QT_SALDO_W#@ #@CD_UNIDADE_BAIXA_REC_W#@ da receita #@NR_RECEITA_ANT_W#@	
			else
				ds_mensagem_w   := wheb_mensagem_pck.get_texto(307498,	'QT_SALDO_W=' || qt_saldo_w || ';' ||
																		'CD_UNIDADE_BAIXA_REC_W=' || cd_unidade_baixa_rec_w);
									-- Considerado o saldo de #@QT_SALDO_W#@ #@CD_UNIDADE_BAIXA_REC_W#@ da entrega anterior
			end if;
		end if;
	end if;
end if;

qt_saldo_ant_p	:= abs(round(qt_saldo_w));--qt_saldo_ant_w;
ds_mensagem_p	:= ds_mensagem_w;

commit;

end fa_obter_saldo_ant_intervalo;
/