create or replace
procedure fis_gerar_reg_R03_dipj(	nr_seq_controle_p	number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					dt_inicio_p		date,
					dt_fim_p		date,
					cd_empresa_p		number,
					qt_linha_p		in out	number,
					nr_sequencia_p		in out	number) is 


cd_cnpj_w			varchar2(14);
nm_repres_w			varchar2(255);
cd_cpf_repres_w			varchar2(255);
nr_ddd_telefone_repres_w	varchar2(255);
nr_telefone_repres_w		varchar2(255);
nr_ddd_fax_repres_w		varchar2(255);
nr_fax_repres_w			varchar2(255);
ds_email_repres_w		varchar2(255);
nm_respo_w			varchar2(255);
cd_cpf_respo_w			varchar2(255);
cd_prof_respo_w			varchar2(255);
sg_estado_respo_w		varchar2(255);
nr_ddd_telefone_respo_w		varchar2(255);
nr_telefone_respo_w		varchar2(255);
nr_ddd_fax_respo_w		varchar2(255);
nr_fax_respo_w			varchar2(255);
ds_email_respo_w		varchar2(255);

nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1) := '|';
ie_arquivo_w			varchar2(15);
nr_ramal_resp_w			compl_pessoa_fisica.nr_ramal%type;
nr_ramal_repres_w		compl_pessoa_fisica.nr_ramal%type;

cursor c01 is
	select  a.cd_cgc cd_cnpj,
		obter_nome_pf(e.cd_titular) nm_repres,
		obter_dados_pf(e.cd_titular, 'CPF') cd_cpf_repres,
		substr(obter_dados_pf_pj(e.cd_titular, null, 'DDT'),1,4) nr_ddd_telefone_repres,
		substr(obter_dados_pf_pj(e.cd_titular, null, 'T'),1,9) nr_telefone_repres,
		substr(obter_dados_pf_pj(e.cd_titular, null, 'DDF'),1,4) nr_ddd_fax_repres,
		substr(obter_dados_pf_pj(e.cd_titular, null, 'FAX'),1,9) nr_fax_repres,
		substr(obter_dados_pf_pj(e.cd_titular, null, 'M'),1,115) ds_email_repres,
		obter_nome_pf(e.cd_contabilista) nm_respo,
		obter_dados_pf(e.cd_contabilista, 'CPF') cd_cpf_respo,
		obter_dados_pf(e.cd_contabilista,'CPR') cd_prof_respo,
		substr(obter_dados_pf_pj(e.cd_contabilista, null, 'UF'),1,2) sg_estado_respo,
		substr(obter_dados_pf_pj(e.cd_contabilista, null, 'DDT'),1,4) nr_ddd_telefone_respo,
		substr(obter_dados_pf_pj(e.cd_contabilista, null, 'T'),1,9) nr_telefone_respo,
		substr(obter_dados_pf_pj(e.cd_contabilista, null, 'DDF'),1,4) nr_ddd_fax_respo,
		substr(obter_dados_pf_pj(e.cd_contabilista, null, 'FAX'),1,9) nr_fax_respo,
		substr(obter_dados_pf_pj(e.cd_contabilista, null, 'M'),1,115) ds_email_respo,
		substr(obter_dados_pf_pj(e.cd_contabilista, null, 'RAM'),1,5) nr_ramal_resp,
		substr(obter_dados_pf_pj(e.cd_titular, null, 'RAM'),1,5) nr_ramal_repres
	from	estabelecimento a,
		empresa e
	where	a.cd_empresa		= e.cd_empresa
	and	a.cd_estabelecimento	= cd_estabelecimento_p;

begin
open c01;
loop
fetch c01 into	
	cd_cnpj_w,
	nm_repres_w,
	cd_cpf_repres_w,
	nr_ddd_telefone_repres_w,
	nr_telefone_repres_w,
	nr_ddd_fax_repres_w,	
	nr_fax_repres_w,	
	ds_email_repres_w,
	nm_respo_w,
	cd_cpf_respo_w,
	cd_prof_respo_w,
	sg_estado_respo_w,
	nr_ddd_telefone_respo_w,
	nr_telefone_respo_w,
	nr_ddd_fax_respo_w,
	nr_fax_respo_w,
	ds_email_respo_w,
	nr_ramal_resp_w,
	nr_ramal_repres_w;
exit when c01%notfound;
	begin	
	
	ds_linha_w	:= substr(	'R03' 						||
					' '						||
					rpad('0',4,'0')					||
					cd_cnpj_w 					||
					'0'						||
					'0'						||
					rpad(nvl(nm_repres_w, ' '),150,' ')		||
					cd_cpf_repres_w					||
					rpad(nvl(nr_ddd_telefone_repres_w, ' '),4,' ')	||
					rpad(nvl(nr_telefone_repres_w, ' '),9,' ')	||
					rpad(nvl(to_char(nr_ramal_repres_w), ' '), 5, ' ')	||
					rpad(nvl(nr_ddd_fax_repres_w, ' '),4,' ')	||
					rpad(nvl(nr_fax_repres_w, ' '),9,' ')		||
					rpad(nvl(ds_email_repres_w, ' '),115,' ')	||
					rpad(nvl(nm_respo_w, ' '),150,' ')		||
					cd_cpf_respo_w					||
					rpad(nvl(cd_prof_respo_w, ' '),15,' ')		||
					rpad(nvl(sg_estado_respo_w,' '),2,' ')		||
					rpad(nvl(nr_ddd_telefone_respo_w, ' '),4,' ')	||
					rpad(nvl(nr_telefone_respo_w, ' '),9,' ')	||
					rpad(nvl(to_char(nr_ramal_resp_w), ' '),5,' ')		||
					rpad(nvl(nr_ddd_fax_respo_w, ' '),4,' ')	||
					rpad(nvl(nr_fax_respo_w, ' '),9,' ')		||
					rpad(nvl(ds_email_respo_w, ' '),115,' ')	||
					rpad(' ',10,' '),1,8000);
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w 		:= nr_linha_w + 1;
	
	insert into fis_dipj_registro
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_dipj,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values	(nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		'R03',
		ds_arquivo_w,
		ds_arquivo_compl_w);
	end;
end loop;
close c01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;	

end fis_gerar_reg_R03_dipj;
/