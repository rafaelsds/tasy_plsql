create or replace procedure MEDRECEITA_NEXODATA (nr_sequencia_p number) is
begin
  if nr_sequencia_p is not null then
     update med_receita set ds_externo = 'NEXODATA' where nr_sequencia = nr_sequencia_p
     and exists(select 	1
		from 	credenciais_integracao 
		where 	ie_sistema = 'NEXODATAREC'
		and	ds_endereco is not null
		and 	ds_login is not null
		and 	ds_senha is not null);
     commit;
  end if;
end MEDRECEITA_NEXODATA;
/