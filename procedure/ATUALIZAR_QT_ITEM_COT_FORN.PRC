create or replace
procedure ATUALIZAR_QT_ITEM_COT_FORN(	nr_cot_compra_p		number,
						nr_item_cot_compra_p		number,
						qt_item_p			number,
						nm_usuario_p			varchar2) is

nr_seq_forn_w			number(10,0);
qt_item_cot_compra_w		number(05,0);
cd_material_cot_w		number(06,0);
cd_material_w			number(06,0);
qt_item_w			number(13,4);


cursor c01 is
select	nr_sequencia
from	cot_compra_forn
where	nr_cot_compra = nr_cot_compra_p;

cursor c02 is
select	distinct
	cd_material
from	cot_compra_forn_item
where	nr_item_cot_compra 	= nr_item_cot_compra_p
and	nr_cot_compra 	= nr_cot_compra_p;


BEGIN

qt_item_w	:= qt_item_p;

open c01;
loop
fetch c01 into
	nr_seq_forn_w;
exit when c01%notfound;

	begin

	select	count(*)
	into	qt_item_cot_compra_w
	from	cot_compra_forn_item
	where	nr_seq_cot_forn	= nr_seq_forn_w
	and	nr_item_cot_compra	= nr_item_cot_compra_p;

	if	(qt_item_cot_compra_w > 1) then

		select	cd_material
		into	cd_material_cot_w
		from	cot_compra_item
		where	nr_cot_compra 	= nr_cot_compra_p
		and	nr_item_cot_compra 	= nr_item_cot_compra_p;

		open c02;
		loop
		fetch c02 into
			cd_material_w;
		exit when c02%notfound;

			if	(cd_material_w = cd_material_cot_w) then
				qt_item_w := qt_item_p;
			else
				select	(round(dividir(qt_item_p, qt_conv_compra_estoque))) * qt_conv_compra_estoque
				into	qt_item_w
				from	material
				where	cd_material = cd_material_w;
			end if;

			if	(nvl(qt_item_w,0) = 0) then
				qt_item_w := qt_item_p;
			end if;

			update	cot_compra_forn_item
			set	qt_material 		= qt_item_w
			where	nr_cot_compra 	= nr_cot_compra_p
			and	nr_item_cot_compra 	= nr_item_cot_compra_p
			and	nr_seq_cot_forn	= nr_seq_forn_w
			and	cd_material		= cd_material_w;
		end loop;
		close c02;
	else
		update	cot_compra_forn_item
		set	qt_material 		= qt_item_w
		where	nr_cot_compra 	= nr_cot_compra_p
		and	nr_item_cot_compra 	= nr_item_cot_compra_p
		and	nr_seq_cot_forn	= nr_seq_forn_w;
	end if;

	end;
	end loop;
close c01;

commit;

END ATUALIZAR_QT_ITEM_COT_FORN;
/