create or replace
procedure HVS_Qua_gerar_rev_doc_venc(
				nm_usuario_p	Varchar2) is 


Cursor C01 is
select	nr_sequencia,
	cd_estabelecimento
from	qua_documento
where	(nvl(to_date(substr(qua_obter_dados_documento(nr_sequencia, 'UR'),1,10),'dd/mm/yyyy'), dt_elaboracao) + 
		nvl(qt_dias_revisao,0)) - 30 < = trunc(sysdate,'dd')
and	ie_situacao = 'A'
and	nvl(qt_dias_revisao,0) > 0;

nr_seq_documento_w	number(10,0);
cd_revisao_w		varchar2(20);
cd_pessoa_resp_w		varchar2(10);
cd_estabelecimento_w	number(04,0);

begin

open C01;
loop
fetch C01 into	
	nr_seq_documento_w,
	cd_estabelecimento_w;
exit when C01%notfound;
	begin

	select	nvl(max(to_number(cd_revisao)),0) + 1
	into	cd_revisao_w
	from	qua_doc_revisao
	where	nr_seq_doc = nr_seq_documento_w;
	
	select	max(a.cd_pessoa_resp)
	into	cd_pessoa_resp_w
	from	setor_atendimento a,
		qua_documento b
	where	a.cd_setor_atendimento 	= b.cd_setor_atendimento
	and	b.nr_sequencia		= nr_seq_documento_w;
	
	if	(nvl(cd_pessoa_resp_w,'X') = 'X') then	/*OS 227542, se n�o tiver resp. do setor, buscar o resp. do setor da qualidade - 94*/
		select	max(a.cd_pessoa_resp)
		into	cd_pessoa_resp_w
		from	setor_atendimento a
		where	a.cd_setor_atendimento 	= 94;
	end if;
	
	
	if	(nvl(cd_pessoa_resp_w,'X') <> 'X') then
		begin
		insert into qua_doc_revisao(
			nr_sequencia,
			nr_seq_doc,
			dt_atualizacao,
			nm_usuario,
			dt_revisao,
			cd_revisao,
			cd_pessoa_revisao,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	qua_doc_revisao_seq.nextval,
			nr_seq_documento_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_revisao_w,
			cd_pessoa_resp_w,
			sysdate,
			nm_usuario_p);
			
		update	qua_documento
		set	ie_status = 'R'
		where	nr_sequencia = nr_seq_documento_w;
		
		begin
		qua_gerar_envio_comunicacao(	nr_seq_documento_w,
						'',
						'AbnerP',
						'1',
						cd_estabelecimento_w,
						'',
						'',
						'N');
		exception
		when others then
			null;
		end;
		end;
	end if;
	end;
end loop;
close C01;	

commit;

end HVS_Qua_gerar_rev_doc_venc;
/