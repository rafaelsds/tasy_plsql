create or replace
procedure vipe_obter_ordem_med (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p		number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p		date,					
		dt_validade_limite_p		date,					
		ie_exibir_hor_realizados_p	varchar2,
		ie_exibir_hor_suspensos_p	varchar2,
		ie_regra_inclusao_p		varchar2,
		ie_data_lib_prescr_p		varchar2,
		ie_exibir_suspensos_p		varchar2,
		ie_agrupar_acm_sn_p		varchar2,
		ie_prescr_setor_p		varchar2,
		cd_setor_paciente_p		number) is

nr_seq_wadep_w		number(10,0);
nr_prescricao_w		number(14,0);
nr_seq_item_ordem_w	number(6,0);
nr_seq_ordem_w		number(10,0);
nr_seq_horario_w	number(10,0);
ds_ordem_w		varchar2(255);
ie_acm_sn_w		varchar2(1);
ds_prescricao_w		varchar2(255);
ds_diluicao_w		varchar2(2000);
					
cursor c01 is
select	nr_prescricao,
	nr_seq_item_ordem,
	nr_seq_ordem,
	nr_seq_horario,
	ds_ordem,
	ie_acm_sn,	
	ds_prescricao,
	ds_diluicao
from	(
	select	a.nr_prescricao,
		b.nr_seq_solucao nr_seq_item_ordem,
		b.nr_sequencia nr_seq_ordem,
		c.nr_sequencia nr_seq_horario,
		x.ds_ordem,
		'N' ie_acm_sn,
		obter_desc_solucao_adep(a.nr_prescricao,b.nr_seq_solucao) ds_prescricao,
		b.ds_ordem ds_diluicao
	from	ordem_medica x,
		prescr_ordem_hor c,
		prescr_medica_ordem b,
		prescr_medica a
	where	x.nr_sequencia = b.nr_seq_ordem
	and	c.nr_seq_ordem = b.nr_sequencia
	and	b.nr_prescricao = a.nr_prescricao
	and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_p) = 'S'
	and	b.nr_seq_solucao is not null
	and	((('N' = 'N') and (c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
		 (('N' = 'S') and (obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and	((ie_exibir_hor_realizados_p = 'S') or (c.dt_fim_horario is null))
	and	((ie_exibir_hor_suspensos_p = 'S') or (c.dt_suspensao is null))
	and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = cd_setor_paciente_p)))
	group by
		a.nr_prescricao,
		b.nr_seq_solucao,
		b.nr_sequencia,
		c.nr_sequencia,
		x.ds_ordem,
		'N',
		b.ds_ordem
	union all
	select	a.nr_prescricao,
		b.nr_seq_material nr_seq_item_ordem,
		b.nr_sequencia nr_seq_ordem,
		c.nr_sequencia nr_seq_horario,
		x.ds_ordem,
		'N' ie_acm_sn,
		z.ds_material ds_prescricao,
		b.ds_ordem ds_diluicao
	from	material z,
		prescr_material y,
		ordem_medica x,
		prescr_ordem_hor c,
		prescr_medica_ordem b,
		prescr_medica a
	where	z.cd_material = y.cd_material
	and	y.nr_prescricao = a.nr_prescricao
	and	y.nr_prescricao = b.nr_prescricao
	and	y.nr_sequencia = b.nr_seq_material
	and	x.nr_sequencia = b.nr_seq_ordem
	and	c.nr_seq_ordem = b.nr_sequencia
	and	b.nr_prescricao = a.nr_prescricao
	and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
	and	a.nr_atendimento = nr_atendimento_p
	and	a.dt_validade_prescr > dt_validade_limite_p
	and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_p) = 'S'
	and	b.nr_seq_material is not null
	and	((('N' = 'N') and (c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
		 (('N' = 'S') and (obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and	((ie_exibir_hor_realizados_p = 'S') or (c.dt_fim_horario is null))
	and	((ie_exibir_hor_suspensos_p = 'S') or (c.dt_suspensao is null))
	and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = cd_setor_paciente_p)))
	group by
		a.nr_prescricao,
		b.nr_seq_material,
		b.nr_sequencia,
		c.nr_sequencia,
		x.ds_ordem,
		'N',
		z.ds_material,
		b.ds_ordem	
	)
group by
	nr_prescricao,
	nr_seq_item_ordem,
	nr_seq_ordem,
	nr_seq_horario,
	ds_ordem,
	ie_acm_sn,	
	ds_prescricao,
	ds_diluicao;

begin
open c01;
loop
fetch c01 into	nr_prescricao_w,
		nr_seq_item_ordem_w,
		nr_seq_ordem_w,
		nr_seq_horario_w,
		ds_ordem_w,
		ie_acm_sn_w,
		ds_prescricao_w,
		ds_diluicao_w;
exit when c01%notfound;
	begin
	select	w_vipe_t_seq.nextval
	into	nr_seq_wadep_w
	from	dual;
	
	insert into w_vipe_t (
		nr_sequencia,
		nm_usuario,
		ie_tipo_item,
		nr_prescricao,
		nr_seq_item,		
		cd_item,
		ds_item,
		ie_acm_sn,		
		ds_prescricao,
		ds_diluicao,
		cd_intervalo,
		ie_diferenciado,
		nr_dia_util,
		nr_seq_proc_interno,
		ie_medic_controlado,
		nr_agrupamento,
		qt_item)
	values (
		nr_seq_wadep_w,
		nm_usuario_p,
		'B',
		nr_prescricao_w,
		nr_seq_item_ordem_w,
		nr_seq_horario_w,
		ds_ordem_w,
		ie_acm_sn_w,
		ds_prescricao_w,
		ds_diluicao_w,
		'X',
		'N',
		0,
		0,
		'N',
		0,
		0);
	end;
end loop;
close c01;
end vipe_obter_ordem_med;
/