create or replace
procedure pls_gerar_ocorrencias_analise
			(	nr_seq_analise_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
			
qt_idade_w			number(10);
nr_seq_regra_ocorrencia_w	number(10);
nr_seq_pessoa_proposta_w	number(10);
nr_seq_proposta_w		number(10);
qt_vidas_proposta_w		number(10);
ie_tipo_contratacao_w		varchar2(10);
nr_contrato_w			number(10);
nr_seq_contrato_w		number(10);
qt_vidas_contrato_w		number(10);
qt_total_vidas_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_regra_analise_adesao
	where	qt_idade_w between nvl(qt_idade_inicial,qt_idade_w) and nvl(qt_idade_final,qt_idade_w)
	and	qt_total_vidas_w between nvl(qt_minima_vidas,qt_total_vidas_w) and nvl(qt_maxima_vidas,qt_total_vidas_w)
	and	((ie_tipo_contratacao	= ie_tipo_contratacao_w and ie_tipo_contratacao is not null) or (ie_tipo_contratacao is null))
	and	ie_situacao	= 'A';

begin

select	substr(obter_dados_pf(cd_pessoa_fisica, 'I'),1,40),
	nr_seq_pessoa_proposta
into	qt_idade_w,
	nr_seq_pessoa_proposta_w
from	pls_analise_adesao
where	nr_sequencia = nr_seq_analise_p;

if	(nr_seq_pessoa_proposta_w is not null) then
	select	nr_seq_proposta
	into	nr_seq_proposta_w
	from	pls_proposta_beneficiario
	where	nr_sequencia	= nr_seq_pessoa_proposta_w;
	
	/*dados da proposta*/
	select	ie_tipo_contratacao,
		nr_seq_contrato
	into	ie_tipo_contratacao_w,
		nr_contrato_w
	from	pls_proposta_adesao
	where	nr_sequencia	= nr_seq_proposta_w;
	
	if	(nr_contrato_w is not null) then
		select	max(nr_sequencia)
		into	nr_seq_contrato_w
		from	pls_contrato
		where	nr_contrato	= nr_contrato_w;
		
		if	(nr_seq_contrato_w is not null) then
			select	count(*)
			into	qt_vidas_contrato_w
			from	pls_segurado a,
				pls_plano b
			where	b.nr_sequencia		= a.nr_seq_plano
			and	a.nr_seq_contrato	= nr_seq_contrato_w
			and	((b.ie_tipo_contratacao	= ie_tipo_contratacao_w and ie_tipo_contratacao_w is not null)
			or	(ie_tipo_contratacao_w is null))
			and	a.dt_liberacao is not null
			and	a.dt_rescisao is null;
		end if;
	end if;
	
	select	count(*)
	into	qt_vidas_proposta_w
	from	pls_proposta_beneficiario
	where	nr_seq_proposta	= nr_seq_proposta_w;
end if;

qt_total_vidas_w	:= nvl(qt_vidas_proposta_w,0) + nvl(qt_vidas_contrato_w,0);

open C01;
loop
fetch C01 into	
	nr_seq_regra_ocorrencia_w;
exit when C01%notfound;
	begin
	
	if	(nr_seq_regra_ocorrencia_w is not null) then
		insert into pls_analise_ocorrencia
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				cd_estabelecimento,nr_seq_analise,nr_seq_regra_ocorrencia)
		values	(	pls_analise_ocorrencia_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
				cd_estabelecimento_p,nr_seq_analise_p,nr_seq_regra_ocorrencia_w);
	end if;			
	
	end;
end loop;
close C01;

end pls_gerar_ocorrencias_analise;
/
