create or replace procedure agente_anest_chec_auto_sol( nr_prescricao_p	prescr_medica.nr_prescricao%type,
														nr_cirurgia_p cirurgia_agente_anestesico.nr_cirurgia%type,
														nr_seq_pepo_p cirurgia_agente_anestesico.nr_seq_pepo%type) is
											 
cursor c01 is
select b.nr_sequencia, b.dt_inicio_adm, b.dt_final_adm
	from
	cirurgia_agente_anestesico a,
	cirurgia_agente_anest_ocor b,
	prescr_material_compl c
	where ((a.nr_cirurgia	= nr_cirurgia_p and nvl(nr_seq_pepo_p, 0) = 0)
		or  (nvl(a.nr_cirurgia, 0) = 0 and a.nr_seq_pepo = nr_seq_pepo_p))
	and a.nr_sequencia = b.nr_seq_cirur_agente
	and a.nr_sequencia = c.nr_seq_agente_anestesico
	and c.nr_prescricao = nr_prescricao_p
	and a.ie_tipo in (1,2,3)
	and a.dt_inativacao is null
	and nvl(ie_modo_adm,'AC') = 'CO'
	and cpoe_regra_cirurgia(a.cd_material) <> 'X';

begin

for c01_w in c01
loop
	if(c01_w.dt_inicio_adm is not null) then
		gerar_prescr_event_agent_anest(c01_w.nr_sequencia, 0);
		if(c01_w.dt_final_adm is not null) then
			gerar_prescr_event_agent_anest(c01_w.nr_sequencia, 2);
		end if;
	end if;
end loop;

end agente_anest_chec_auto_sol;
/
