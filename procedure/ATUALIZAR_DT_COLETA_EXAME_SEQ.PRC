create or replace
procedure atualizar_dt_coleta_exame_seq(	nr_prescricao_p		number,
						nr_sequencia_p		number,
						dt_coleta_p		date ) is 

begin

update 	prescr_procedimento 
set 	dt_coleta = dt_coleta_p
where 	nr_sequencia = nr_sequencia_p
and 	nr_prescricao = nr_prescricao_p;

commit;

end atualizar_dt_coleta_exame_seq;
/
