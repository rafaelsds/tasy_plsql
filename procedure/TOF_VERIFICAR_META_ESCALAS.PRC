create or replace
procedure tof_verificar_meta_escalas(	nr_atendimento_p	varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	varchar2,
					dt_avaliacao_p		date,
					ie_finalizar_p		varchar2 default 'N') is 


nr_sequencia_w		number(10);
nr_seq_meta_w		number(10);
contador_w			number(5);
ie_status_w			varchar2(2);
dt_inicio_w			date;
dt_fim_w			date;
nr_seq_meta_atend_w   number(10);
nr_seq_escala_w		number(10);
qt_reg_w		number(10);
qt_reg_meta_w		number(10);
qt_escala_w		number(10);
ds_sep_bv_w		varchar2(20);
ds_comando_w		varchar2(2000)	:= '';
ds_parametros_w		varchar2(2000);


nr_seq_pend_regra_w	number(10);
vl_minimo_w		number(15,4);
vl_maximo_w		number(15,4);	
nr_seq_sinal_vital_w	number(10);	
nr_atendimento_w	number(10);
nm_atributo_w		varchar2(50);
vl_atributo_w		number(15,4);
cd_pessoa_fisica_w	varchar2(10);
qt_idade_w		number(10);
cd_setor_atendimento_w	number(10);
cd_escala_dor_w		varchar2(10);
nr_seq_result_dor_w	number(10);
nm_tabela_w		varchar2(80);
ie_escala_w		number(10);
nr_seq_result_w		number(10);
nr_seq_regra_result_w	number(10);
IE_INFORMACAO_w		varchar2(10);
IE_TIPO_ATRIBUTO_w	varchar2(50);
ie_informacao_ret_w	varchar2(255);
sql_errm_w 		varchar2(255);
nm_atributo_data_w	varchar2(255);

dt_inicio_meta_w			date;
qt_horas_consistencia_w	number(10,2);
dt_horas_inicio_consist_w	date;
qt_minutos_retr_w	number(10);


Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_meta,		
		m.qt_minutos_retr,
		a.dt_inicio
	from	tof_meta_atend a,
		tof_meta m
	where	m.nr_sequencia = a.nr_seq_meta
	and	nr_atendimento = nr_atendimento_p
	and	ie_regra = 'ESC'
	and	nvl(m.ie_situacao,'A') = 'A'
	and	a.dt_finalizacao is null
	and a.dt_liberacao is not null
	and a.dt_inativacao is null;
	
	
Cursor C02 is
	select	a.nr_sequencia,
		a.vl_minimo,
		a.vl_maximo,
		d.nm_tabela,
		d.NM_ATRIBUTO_INF,
		d.ie_escala,
		nvl(a.nr_seq_result,0),
		a.IE_INFORMACAO,
		d.IE_TIPO_ATRIBUTO,
		d.NM_ATRIBUTO_DATA
	from	tof_meta_item a,
		tof_meta b,
		VICE_ESCALA d
	where	b.nr_sequencia	= a.nr_seq_meta
	and	a.nr_seq_meta = nr_seq_meta_w
	and	d.nr_sequencia = a.NR_SEQ_ESCALA
	and	b.ie_regra 	= 'ESC'
	and	nvl(b.ie_situacao,'A') 		= 'A'
	and	upper(d.nm_tabela)	<> 'ESCALA_EIF'
	union	
	select	a.nr_sequencia,
		a.vl_minimo,
		a.vl_maximo,
		d.nm_tabela,
		d.NM_ATRIBUTO_INF,
		d.ie_escala,
		nvl(a.nr_seq_result,0),
		a.IE_INFORMACAO,
		d.IE_TIPO_ATRIBUTO,
		d.NM_ATRIBUTO_DATA
	from	tof_meta_item a,
		tof_meta b,
		VICE_ESCALA d
	where	b.nr_sequencia	= a.nr_seq_meta
	and	a.nr_seq_meta = nr_seq_meta_w
	and	d.nr_sequencia = a.NR_SEQ_ESCALA
	and	b.ie_regra 	= 'ESC'
	and	nvl(b.ie_situacao,'A') 		= 'A'
	and	upper(d.nm_tabela)  = 'ESCALA_EIF';

begin

if	(nvl(ie_finalizar_p,'N') = 'S') then

	dt_inicio_w 	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(dt_avaliacao_p,sysdate));
	dt_fim_w 	:= sysdate;

else

	dt_inicio_w 	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(dt_avaliacao_p,sysdate) - 1);
	dt_fim_w 	:= ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(dt_inicio_w);

end if;


ds_sep_bv_w :=  obter_separador_bv;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	nr_seq_meta_w,	
	qt_minutos_retr_w,
	dt_inicio_meta_w;
exit when C01%notfound;
	begin

		qt_horas_consistencia_w		:= (qt_minutos_retr_w/1440);
		dt_horas_inicio_consist_w	:= (dt_fim_w - nvl(qt_horas_consistencia_w,0));
		
		if (dt_inicio_meta_w < dt_horas_inicio_consist_w) then	
	
		
			open C02;
			loop
			fetch C02 into	
				nr_seq_pend_regra_w,
				vl_minimo_w,
				vl_maximo_w,
				nm_tabela_w,
				nm_atributo_w,
				ie_escala_w,
				nr_seq_regra_result_w,
				IE_INFORMACAO_w,
				IE_TIPO_ATRIBUTO_w,
				nm_atributo_data_w;
			exit when C02%notfound;
				begin		

				begin
				
				ds_comando_w	:= 	'select	count(*)'||
							' from  '||nm_tabela_w ||
							' where	nr_atendimento = :nr_atendimento'||						
							' and	'||nm_atributo_data_w||' between :dt_inicio and :dt_fim'||
							' and	dt_liberacao is not null '||
							' and	dt_inativacao is null ';
				
				ds_parametros_w	:= 	
							'nr_atendimento='||nr_atendimento_p|| ds_sep_bv_w ||
							'dt_inicio='||to_char(dt_inicio_w,'dd/mm/yyyy hh24:mi:ss')||  ds_sep_bv_w ||
							'dt_fim='|| to_char(dt_fim_w,'dd/mm/yyyy hh24:mi:ss') ||ds_sep_bv_w ;						
				
				
				OBTER_VALOR_DINAMICO_PEP( ds_comando_w,ds_parametros_w, qt_escala_w);
								
				exception
				when others then				
					qt_escala_w := 0;
				end;
				
				if	((nm_tabela_w = 'ESCALA_EIF') or ( IE_TIPO_ATRIBUTO_w	<> 'VARCHAR2')) and
					(( vl_minimo_w is null ) or ( vl_maximo_w is null )) then
					
					
					if	(qt_escala_w > 0 ) then
				
						ie_status_w := 'A';		

					else

						ie_status_w := 'N';

					end if;
					
					select	count(*)
					into	qt_reg_meta_w
					from	tof_meta_atend_hor
					where	nr_seq_meta_atend = nr_sequencia_w
					and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_geracao) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_fim_w)
					and dt_liberacao is not null
					and dt_inativacao is null;
				
					if	(qt_reg_meta_w = 0) then
				
						Select  tof_meta_atend_hor_seq.nextval
						into	nr_seq_meta_atend_w
						from 	dual;
						
						insert into tof_meta_atend_hor(	nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										nr_seq_meta_atend,
										ie_status,
										dt_geracao,
										ds_observacao,
										dt_liberacao)
									values(	nr_seq_meta_atend_w,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_sequencia_w,
										ie_status_w,
										dt_fim_w,
										'',
										sysdate);
										
					
						if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;	
					
					end if;
					
					Alterar_status_tof_meta(nr_seq_meta_atend_w, nm_usuario_p);

				elsif	(IE_TIPO_ATRIBUTO_w	= 'VARCHAR2') and
						(IE_INFORMACAO_w is null) then
					
					
					if	(qt_escala_w > 0 ) then
				
						ie_status_w := 'A';		

					else

						ie_status_w := 'N';

					end if;
					
					select	count(*)
					into	qt_reg_meta_w
					from	tof_meta_atend_hor
					where	nr_seq_meta_atend = nr_sequencia_w
					and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_geracao) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_fim_w)
					and dt_liberacao is not null
					and dt_inativacao is null;
					
					if	(qt_reg_meta_w = 0) then
					
						Select  tof_meta_atend_hor_seq.nextval
						into	nr_seq_meta_atend_w
						from 	dual;
						
						insert into tof_meta_atend_hor(	nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										nr_seq_meta_atend,
										ie_status,
										dt_geracao,
										ds_observacao,
										dt_liberacao)
									values(	nr_seq_meta_atend_w,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_sequencia_w,
										ie_status_w,
										dt_fim_w,
										'',
										sysdate);
						if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;	
						
					end if;
					
					Alterar_status_tof_meta(nr_seq_meta_atend_w, nm_usuario_p);	
					

				elsif	(qt_escala_w > 0) then			
					
				
					if 	(nm_tabela_w = 'ESCALA_EIF') then	
				
						
						select	count(*)
						into	qt_reg_w					
						from	escala_eif e
						where	e.nr_atendimento = nr_atendimento_p
						and	dt_avaliacao between dt_inicio_w and dt_fim_w
						and	dt_liberacao is not null 
						and	dt_inativacao is null
						and	somente_numero(obter_resultado_escala_eif(e.nr_sequencia,'T')) >=  vl_minimo_w
						and	somente_numero(obter_resultado_escala_eif(e.nr_sequencia,'T')) <=  vl_maximo_w;

						if	(qt_reg_w = 0) then
						
							select	count(*)
							into	qt_reg_w					
							from	escala_eif e
							where	e.nr_atendimento = nr_atendimento_p
							and	dt_avaliacao between dt_inicio_w and dt_fim_w
							and	dt_liberacao is not null 
							and	dt_inativacao is null
							and	somente_numero(obter_resultado_escala_eif(e.nr_sequencia,'S')) = nr_seq_regra_result_w;					
						
						end if;
						
						
						
					elsif	(IE_TIPO_ATRIBUTO_w	= 'VARCHAR2') then

						
						ds_comando_w	:= 	'select	count(*)'||
									' from  '||nm_tabela_w ||
									' where	nr_atendimento = :nr_atendimento'||
									' and	'||nm_atributo_w||' = :ie_informacao'||
									' and	'||nm_atributo_data_w||' between :dt_inicio and :dt_fim'||
									' and	dt_liberacao is not null '||
									' and	dt_inativacao is null ';
						
						ds_parametros_w	:= 	
									'nr_atendimento='||nr_atendimento_p|| ds_sep_bv_w ||
									'ie_informacao='||IE_INFORMACAO_w|| ds_sep_bv_w ||
									'dt_inicio='||to_char(dt_inicio_w,'dd/mm/yyyy hh24:mi:ss')||  ds_sep_bv_w ||
									'dt_fim='|| to_char(dt_fim_w,'dd/mm/yyyy hh24:mi:ss') ||ds_sep_bv_w ;	
						
						OBTER_VALOR_DINAMICO_PEP(ds_comando_w,ds_parametros_w,qt_reg_w);	
						
					
					else
					
						ds_comando_w	:= 	'select	count(*)'||
									' from  '||nm_tabela_w ||
									' where	nr_atendimento = :nr_atendimento'||
									' and	'||nm_atributo_w||' between :vl_minimo and :vl_maximo'||
									' and	'||nm_atributo_data_w||' between :dt_inicio and :dt_fim'||
									' and	dt_liberacao is not null '||
									' and	dt_inativacao is null ';
						
						ds_parametros_w	:= 	
									'nr_atendimento='||nr_atendimento_p|| ds_sep_bv_w ||
									'vl_minimo='||vl_minimo_w|| ds_sep_bv_w ||
									'vl_maximo='||vl_maximo_w|| ds_sep_bv_w ||
									'dt_inicio='||to_char(dt_inicio_w,'dd/mm/yyyy hh24:mi:ss')||  ds_sep_bv_w ||
									'dt_fim='|| to_char(dt_fim_w,'dd/mm/yyyy hh24:mi:ss') ||ds_sep_bv_w;	
										
						OBTER_VALOR_DINAMICO_PEP(ds_comando_w,ds_parametros_w,qt_reg_w);			
							
						
					end if;				
				
				
				
					if	(qt_reg_w > 0 ) then
				
						ie_status_w := 'N';		

					else

						ie_status_w := 'A';

					end if;
					
					select	count(*)
					into	qt_reg_meta_w
					from	tof_meta_atend_hor
					where	nr_seq_meta_atend = nr_sequencia_w
					and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_geracao) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_fim_w)
					and dt_liberacao is not null
					and dt_inativacao is null;
					
					if	(qt_reg_meta_w = 0) then
				
						Select  tof_meta_atend_hor_seq.nextval
						into	nr_seq_meta_atend_w
						from 	dual;
						
						insert into tof_meta_atend_hor(	nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										nr_seq_meta_atend,
										ie_status,
										dt_geracao,
										ds_observacao,
										dt_liberacao)
									values(	nr_seq_meta_atend_w,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_sequencia_w,
										ie_status_w,
										dt_fim_w,
										'',
										sysdate);
						if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;	
						
					end if;
					
					Alterar_status_tof_meta(nr_seq_meta_atend_w, nm_usuario_p);

				end if;	
				
				
				end;
			end loop;
			close C02;
	end if;		
	
	end;
end loop;
close C01;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end tof_verificar_meta_escalas;
/
