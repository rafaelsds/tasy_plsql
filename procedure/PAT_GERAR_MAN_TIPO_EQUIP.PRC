create or replace
procedure pat_gerar_man_tipo_equip(	nr_seq_tipo_p		number,
					nm_usuario_p		varchar2) is 
		
nr_seq_tipo_equip_w			number(10);
nr_sequencia_w				number(10);	
begin

select	nvl(max(nr_seq_tipo_equip),0)
into	nr_seq_tipo_equip_w
from	pat_tipo_bem
where	nr_sequencia	= nr_seq_tipo_p;

if	(nr_seq_tipo_equip_w = 0) then

	select	man_tipo_equipamento_seq.nextval
	into	nr_sequencia_w
	from	dual;
		
	insert into man_tipo_equipamento (
		nr_sequencia,
		ds_tipo_equip,
		dt_atualizacao,
		nm_usuario,
		ie_voltagem,
		ie_ano_fabricacao,
		ie_marca,
		ie_modelo,
		ie_peso,
		qt_dia_intervalo,
		ie_situacao,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_envio_comunicacao,
		ie_mapeamento)
	select	nr_sequencia_w,
		ds_tipo,
		sysdate,
		nm_usuario_p,
		'N',
		'N',
		'N',
		'N',
		'N',
		0,
		'A',
		sysdate,
		nm_usuario_p,
		'',
		'N'
	from	pat_tipo_bem
	where	nr_sequencia	= nr_seq_tipo_p;
	
	update	pat_tipo_bem
	set	nr_seq_tipo_equip	= nr_sequencia_w,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_tipo_p;
	
	commit;
end if;		

end pat_gerar_man_tipo_equip;
/