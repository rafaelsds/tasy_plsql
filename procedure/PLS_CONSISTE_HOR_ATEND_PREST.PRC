create or replace
procedure pls_consiste_hor_atend_prest(	nr_seq_horario_p	number,
					ie_dia_semana_p		varchar2,
					nr_hora_inicial_p	number,
					nr_min_inicial_p	number,
					nr_hora_final_p		number,
					nr_min_final_p		number,
					nr_seq_prestador_p	number,
					nm_usuario_p		Varchar2) is

nr_seq_prest_erro_w	number(10);
nm_prestador_w		varchar2(255);
ds_endereco_w		varchar2(255);
ie_dia_semana_w		varchar2(3);
nr_hora_inicial_w	number(2);
nr_min_inicial_w	number(2);
nr_hora_final_w		number(2);
nr_min_final_w		number(2);
cd_pessoa_fisica_w	varchar2(10);
nr_seq_prestador_w	number(10);	

begin

if	(nr_seq_horario_p is not null) then
	ie_dia_semana_w		:= ie_dia_semana_p; 
	nr_hora_inicial_w	:= nr_hora_inicial_p;
	nr_min_inicial_w	:= nr_min_inicial_p;
	nr_hora_final_w		:= nr_hora_final_p;
	nr_min_final_w		:= nr_min_final_p;
	nr_seq_prestador_w	:= nr_seq_prestador_p;
	
	/* Verifica��o de hora inicial */
	if	(nr_hora_inicial_w > 23) or
		(nr_hora_inicial_w < 0) or
		(nr_hora_inicial_w > nr_hora_final_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(265517,'');
		--Mensagem: Hora inicial inv�lida.
	end if;
	
	/* Verifica��o de hora final */
	if	(nr_hora_final_w > 23) or
		(nr_hora_final_w < 0) or
		(nr_hora_final_w < nr_hora_inicial_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(265520,'');
		--Mensagem: Hora final inv�lida.
	end if;
	
	/* Verifica��o de minutos */
	if	(nr_min_inicial_w < 0) or (nr_min_inicial_w > 59) then
		wheb_mensagem_pck.exibir_mensagem_abort(265522,'');	
		--Mensagem: Minutos iniciais inv�lidos.
	end if;
	
	/* Verifica��o de minutos */
	if	(nr_min_final_w < 0) or (nr_min_final_w > 59) then
		wheb_mensagem_pck.exibir_mensagem_abort(265523,'');
		--Mensagem: Minutos finais inv�lidos.
	end if;

	select	a.cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pls_prestador a
	where	a.nr_sequencia	= nr_seq_prestador_p;

	if	(cd_pessoa_fisica_w is not null) then
		/* Hor�rio inicial */
		select	max(a.nr_seq_prestador)
		into	nr_seq_prest_erro_w
		from	pls_prest_horario_atend a,
			pls_prestador b
		where	a.nr_seq_prestador	= b.nr_sequencia
		and	a.nr_sequencia		<> nr_seq_horario_p
		and	a.ie_dia_semana		= ie_dia_semana_w
		and	to_date('01/01/2011 ' || nr_hora_inicial_w || ':' || nr_min_inicial_w,'dd/mm/yyyy hh24:mi') between
			to_date('01/01/2011 ' || nr_hora_inicial || ':' || nr_min_inicial,'dd/mm/yyyy hh24:mi') and
			to_date('01/01/2011 ' || nr_hora_final || ':' || nr_min_final,'dd/mm/yyyy hh24:mi')
		and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		if	(nr_seq_prest_erro_w is not null) then
			nm_prestador_w	:= pls_obter_dados_prestador(nr_seq_prest_erro_w,'N');
			ds_endereco_w	:= pls_obter_end_prestador(nr_seq_prest_erro_w,null,null);													
			wheb_mensagem_pck.exibir_mensagem_abort(265524,	'nm_prestador=' || nm_prestador_w||';'||
									'ds_endereco=' || ds_endereco_w);
			--Mensagem: O hor�rio inicial informado fica em conflito com outro hor�rio da pessoa.' || chr(13) || chr(10) ||
			--Prestador: || nm_prestador_w || chr(13) || chr(10) ||
			--Endere�o: || ds_endereco_w 
		end if;
		
		/* Hor�rio final */
		select	max(a.nr_seq_prestador)
		into	nr_seq_prest_erro_w
		from	pls_prest_horario_atend a,
			pls_prestador b
		where	a.nr_seq_prestador	= b.nr_sequencia
		and	a.nr_sequencia		<> nr_seq_horario_p
		and	a.ie_dia_semana		= ie_dia_semana_w
		and	to_date('01/01/2011 ' || nr_hora_final_w || ':' || nr_min_final_w,'dd/mm/yyyy hh24:mi') between
			to_date('01/01/2011 ' || nr_hora_inicial || ':' || nr_min_inicial,'dd/mm/yyyy hh24:mi') and
			to_date('01/01/2011 ' || nr_hora_final || ':' || nr_min_final,'dd/mm/yyyy hh24:mi')
		and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		if	(nr_seq_prest_erro_w is not null) then
			nm_prestador_w	:= pls_obter_dados_prestador(nr_seq_prest_erro_w,'N');
			ds_endereco_w	:= pls_obter_end_prestador(nr_seq_prest_erro_w,null,null);
			wheb_mensagem_pck.exibir_mensagem_abort(265531,	'nm_prestador=' || nm_prestador_w||';'||
									'ds_endereco=' || ds_endereco_w);
			--Mensagem: O hor�rio final informado fica em conflito com outro hor�rio da pessoa.' || chr(13) || chr(10) ||
			--Prestador: || nm_prestador_w || chr(13) || chr(10) ||
			--Endere�o: || ds_endereco_w
		end if;
	end if;
end if;

end pls_consiste_hor_atend_prest;
/