Create or Replace
procedure Desfazer_Liberacao_autor(
                         		nr_sequencia_p     	NUMBER,
					nm_usuario_p		Varchar2) IS


begin

update	autorizacao_cirurgia
set	dt_liberacao = null,
	dt_fim_cotacao = null,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	nm_usuario_lib = null,
	nr_prescricao = null
where	nr_sequencia = nr_sequencia_p;

delete	autor_cirurgia_emissao
where	nr_seq_autor = nr_sequencia_p;

commit;

end Desfazer_Liberacao_autor;
/