create or replace
procedure duplicar_check_list	(	nr_sequencia_p		number,					
					nm_usuario_p		varchar2) is

nr_seq_cheq_list_w	number(10);

begin

select	atend_check_list_seq.nextval
into	nr_seq_cheq_list_w
from	dual;


insert	into atend_check_list (
	nr_sequencia,
	cd_pessoa_fisica,
	cd_medico,
	cd_perfil_ativo,
	dt_cheklist,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	ds_observacao,
	nr_cirurgia,
	nr_seq_pepo,
	nr_seq_tipo_avaliacao,	
	nr_atendimento)
select	nr_seq_cheq_list_w,
	cd_pessoa_fisica,
	to_number(obter_pessoa_fisica_usuario(nm_usuario_p,'C')),
	cd_perfil_ativo,
	sysdate,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario_p,
	nm_usuario,
	ds_observacao,
	nr_cirurgia,
	nr_seq_pepo,
	nr_seq_tipo_avaliacao,	
	nr_atendimento	
from	atend_check_list 
where	nr_sequencia = nr_sequencia_p;


insert	into atend_check_list_result(
	nr_seq_checklist,
	nr_seq_item,
	dt_atualizacao,
	dt_atualizacao_nrec,
	qt_resultado,
	ds_resultado,
	nm_usuario,
	nm_usuario_nrec)
select	nr_seq_cheq_list_w,
	nr_seq_item,
	sysdate,
	dt_atualizacao,
	qt_resultado,
	ds_resultado,
	nm_usuario_p,
	nm_usuario
from	atend_check_list_result
where	nr_seq_checklist = nr_sequencia_p;	


commit;

end duplicar_check_list;
/
