create or replace
procedure pls_consiste_req_web_automat(	qt_hora_solicitacao_p		number,
					nm_usuario_p			varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Consistir dentro do per�odo informado, todas as requisi��es solicitadas pelo Portal web que n�o foram finalizadas.
� chamada pela job JOB PLS_CONSISTE_REQ_WEB_AUTOMAT_J.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ x ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [   ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

dt_lim_solicitacao_w		date;
ds_historico_w			varchar2(4000);	
nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;

Cursor C01 is
	select	a.nr_sequencia,
		a.cd_estabelecimento
	from	pls_requisicao a
	where	a.ie_estagio	= 1	 	
	and	nvl(a.ie_req_web_finalizada, 'N')	= 'N'
	and	a.ie_tipo_processo	= 'P'
	and	a.dt_atualizacao_nrec 	< dt_lim_solicitacao_w
	and	exists(	select	1 qt_item
			from	pls_requisicao_proc
			where	nr_seq_requisicao	= a.nr_sequencia
			union all
			select	1 qt_item
			from	pls_requisicao_mat
			where	nr_seq_requisicao	= a.nr_sequencia)
	order 	by 1;


begin
if (qt_hora_solicitacao_p > 0) then
	dt_lim_solicitacao_w	:= (sysdate - (qt_hora_solicitacao_p/24));
	begin
	for C01_w in C01 loop
		begin
		nr_seq_requisicao_w	:= C01_w.nr_sequencia;
		pls_consistir_requisicao(nr_seq_requisicao_w, C01_w.cd_estabelecimento, nm_usuario_p);
		ds_historico_w	:= substr('Requisi��o '||C01_w.nr_sequencia||' consistida automaticamente pela JOB ''PLS_CONSISTE_REQ_WEB_AUTOMAT_J'' em '||to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),1,4000);
		pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L',ds_historico_w,'',nm_usuario_p);
		end;
	end loop;
	exception
	when others then
		ds_historico_w	:= substr('N�o foi poss�vel consistir automaticamente a requisi��o.'||chr(13)||
					'Erro: ' || sqlerrm,1,4000);		
		pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L',ds_historico_w,'',nm_usuario_p);
	end;	
end if;

commit;

end pls_consiste_req_web_automat;
/