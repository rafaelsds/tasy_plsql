create or replace
procedure gerar_prescr_proc_hor	(nr_prescricao_p	number,
				cd_perfil_p	number,
				nm_usuario_p	varchar2) is
				
ds_horarios_w			varchar2(2000);
ds_horarios_padr_w		varchar2(2000);
ds_hora_w			varchar2(2000);
dt_liberacao_w			Date;
dt_horario_w			Date;	
k				Integer;
y				Integer;
nr_sequencia_w			Number(10);
nr_seq_prescr_w			Number(6);
qt_dose_w			Number(18,6);
qt_total_dispensar_w		Number(18,6);
cd_material_w			Number(6);
dt_primeiro_horario_w		Date;
ie_agrupador_w			Number(2);
cd_intervalo_w			varchar2(7);
ie_prescricao_dieta_w		varchar2(1);
nr_ocorrencia_w			number(15,4);
ie_esquema_alternado_w		varchar2(1);
nr_seq_solucao_w		number(6);

ie_agrupador_dil_w		Number(2);
nr_sequencia_dil_w		Number(10);
qt_dose_dil_w			Number(18,6);
qt_total_disp_dil_w		Number(18,6);
nr_ocorrencia_dil_w		number(15,4);
nr_seq_horario_w		Number(15); 
ie_tipo_proced_w		varchar2(15);
cd_material_dil_w		Number(6); 
hr_prim_horario_w		varchar2(5);
ie_urgente_w			varchar2(1);
qt_conversao_w			Number(18,6);
qt_dose_especial_w		Number(18,6);
hr_dose_especial_w		varchar2(5);

nr_seq_procedimento_w		number(6,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
nr_seq_proc_interno_w		number(10,0);
nr_ocor_proc_w			number(15,4);
ie_proc_urgente_w		varchar2(1);
dt_prev_execucao_w		date;
ds_hora_proc_w			varchar2(2000);
dt_horario_proc_w		date;
ds_horarios_proc_w		varchar2(2000);
ds_horarios_padrao_proc_w	varchar2(2000);
cd_material_exame_w		varchar2(20);

nr_seq_recomendacao_w		number(6,0);
cd_recomendacao_w		number(10,0);
ds_hora_rec_w			varchar(2000);
dt_horario_rec_w		date;
nr_seq_classif_rec_w		number(10,0);
ds_horarios_rec_w		varchar2(2000);
ds_horarios_padrao_rec_w	varchar2(2000);

ie_se_necessario_w		varchar2(1);
ie_acm_w			varchar2(1);
ie_horario_especial_w		varchar2(1) := 'N';
qt_dieta_w			number(10,0);
cd_setor_atendimento_w		number(5);
cd_estabelecimento_w		number(4);
dt_prescricao_w			Date;
dt_prescricao_ww		Date;
qT_dia_adic_w			Number(10) := 0;
qt_registro_w			Number(10);
dt_inicio_prescr_w		Date;

nr_seq_procedimento_novo_w	number(6);
nr_seq_exame_w			number(10);
ie_status_atend_w		number(2);
ie_status_execucao_w		varchar2(3);
cd_setor_atendimento_proc_w	number(5);
cd_setor_coleta_w		number(5);
cd_setor_entrega_w        	number(5);
cd_setor_exec_fim_w       	number(5);
cd_setor_exec_inic_w      	number(5);
nr_seq_lab_w			varchar2(20);
ie_gerar_proc_intervalo_w	varchar2(1);
ie_suspenso_w			varchar2(1);
ds_observacao_w			varchar2(2000);
ds_dado_clinico_w		varchar2(2000);
ds_material_especial_w		varchar2(255);
ie_controlado_w			varchar2(1);

nr_seq_prescr_hor_w		number(6);
dt_horario_proc_prev_w		date;
ie_proc_atual_w			varchar2(1);
qt_min_agora_w			number(15);
qt_min_especial_w		number(15);
ie_classif_urgente_w		varchar2(3);
dt_limite_agora_w		date;
dt_limite_especial_w		date;
nr_seq_classif_w		number(10);
dt_liberacao_farmacia_w		date;	
ie_amostra_w			varchar2(1);
nr_seq_derivado_w		number(10);
ie_util_hemocomponente_w 	varchar2(15);
qt_vol_hemocomp_w		number(15);
qt_procedimento_w		number(8,3);
nr_seq_etapa_w			number(10);
				
cursor C13 is				
select	nr_sequencia,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_proc_interno,
	nr_ocorrencia,
	nvl(ie_urgencia,'N'),
	nvl(dt_prev_execucao,a.dt_inicio_prescr),
	substr(ds_horarios,1,2000),
	substr(padroniza_horario_prescr(b.ds_horarios,decode(to_char(b.dt_prev_execucao, 'hh24:mi'), '00:00', to_char(b.dt_prev_execucao,'dd/mm/yyyy hh24:mi:ss'), to_char(b.dt_prev_execucao,'dd/mm/yyyy hh24:mi')||':00')),1,2000),
	nvl(ie_se_necessario,'N'),
	nvl(ie_acm,'N'),
	cd_material_exame,
	nr_seq_exame,
	b.ie_status_atend,
	b.ie_status_execucao,
	b.cd_setor_atendimento,
	b.cd_setor_coleta,
	b.cd_setor_entrega,
	b.cd_setor_exec_fim,
	b.cd_setor_exec_inic,
	b.nr_seq_lab,
	b.ie_suspenso,
	b.ds_observacao,
	b.ds_dado_clinico,
	b.ds_material_especial,
	b.ie_amostra,
	b.nr_seq_derivado,
	b.ie_util_hemocomponente,
	b.qt_vol_hemocomp,
	b.qt_procedimento,
	Obter_se_exibe_proced(b.nr_prescricao,b.nr_sequencia,b.ie_tipo_proced,'BSST'),
	b.cd_intervalo
from	prescr_procedimento b,
	prescr_medica a
where	a.nr_prescricao	= b.nr_prescricao
and	a.nr_prescricao	= nr_prescricao_p
and	b.nr_seq_origem	is null
and	nvl(b.ie_suspenso,'N') <> 'S'
and	a.dt_suspensao is null
and	not exists (select	1
				from	prescr_proc_hor x
				where	x.nr_prescricao = b.nr_prescricao
				and		x.nr_seq_procedimento = b.nr_sequencia
				and		x.dt_lib_horario is not null);

cursor c15 is
select	b.dt_horario,
	b.nr_sequencia
from	prescr_proc_hor b,
	prescr_medica a,
	prescr_procedimento c
where	a.nr_prescricao		= nr_prescricao_p
and	a.nr_prescricao		= b.nr_prescricao
and	a.nr_prescricao		= c.nr_prescricao
and	c.nr_sequencia = b.nr_seq_procedimento
and	b.nr_seq_procedimento	= nr_seq_procedimento_w
and	((ie_gerar_proc_intervalo_w = 'S') or
	 ((ie_gerar_proc_intervalo_w = 'D') and
	  (verifica_se_proc_regra_interv(a.nr_prescricao, b.nr_seq_procedimento) = 'S')))
and	Obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
and nvl(c.ie_tipo_proced,'X') not in ('AP','APH','APC');

begin

delete 
from 	prescr_proc_hor
where	nr_prescricao = nr_prescricao_p
and	dt_lib_horario is null
and	dt_suspensao is null;

select	count(*)
into	qt_registro_w
from	prescr_proc_hor
where	nr_prescricao = nr_prescricao_p;

if	(qt_registro_w = 0) then

	select	nvl(dt_liberacao,dt_liberacao_medico),
		dt_primeiro_horario,
		cd_setor_atendimento,
		nvl(cd_estabelecimento,1),
		trunc(dt_prescricao,'mi'),
		dt_inicio_prescr,
		dt_liberacao_farmacia
	into	dt_liberacao_w,
		dt_primeiro_horario_w,
		cd_setor_atendimento_w,
		cd_estabelecimento_w,
		dt_prescricao_w,
		dt_inicio_prescr_w,
		dt_liberacao_farmacia_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;

	obter_param_usuario(924, 192, cd_perfil_p, nm_usuario_p, cd_estabelecimento_w, ie_gerar_proc_intervalo_w);
	if	(dt_primeiro_horario_w is null) then
		dt_primeiro_horario_w := dt_prescricao_w;
	end if;

	qt_dia_adic_w	:= 0;

	open C13;
	loop
	fetch C13 into	nr_seq_procedimento_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			nr_ocor_proc_w,
			ie_proc_urgente_w,
			dt_prev_execucao_w,
			ds_horarios_proc_w,
			ds_horarios_padrao_proc_w,
			ie_se_necessario_w,
			ie_acm_w,
			cd_material_exame_w,
			nr_seq_exame_w,
			ie_status_atend_w,
			ie_status_execucao_w,
			cd_setor_atendimento_proc_w,
			cd_setor_coleta_w,
			cd_setor_entrega_w,
			cd_setor_exec_fim_w,
			cd_setor_exec_inic_w,
			nr_seq_lab_w,
			ie_suspenso_w,
			ds_observacao_w,
			ds_dado_clinico_w,
			ds_material_especial_w,
			ie_amostra_w,
			nr_seq_derivado_w,
			ie_util_hemocomponente_w,
			qt_vol_hemocomp_w,
			qt_procedimento_w,
			ie_tipo_proced_w,
			cd_intervalo_w;
		exit when C13%notfound;
		begin	

		qt_dia_adic_w := 0;

		if	(ds_horarios_padrao_proc_w is not null) then
			begin
			nr_seq_etapa_w := 0;
			while	ds_horarios_padrao_proc_w is not null loop
				begin
				select	instr(ds_horarios_padrao_proc_w, ' ') 
				into	k
				from	dual;

				if	(k > 1) and
					(substr(ds_horarios_padrao_proc_w, 1, k -1) is not null) then
					ds_hora_proc_w			:= substr(ds_horarios_padrao_proc_w, 1, k-1);
					ds_hora_proc_w			:= replace(ds_hora_proc_w, ' ','');
					ds_horarios_padrao_proc_w	:= substr(ds_horarios_padrao_proc_w, k + 1, 2000);
				elsif	(ds_horarios_padrao_proc_w is not null) then
					ds_hora_proc_w			:= replace(ds_horarios_padrao_proc_w,' ','');
					ds_horarios_padrao_proc_w	:= '';
				end if;

				if	(instr(ds_hora_proc_w,'A') > 0) and
					(ie_proc_urgente_w = 'N') and
					(qt_dia_adic_w = 0) then
					qt_dia_adic_w	:= 1;
				elsif	(instr(ds_hora_proc_w,'AA') > 0) and
					(ie_proc_urgente_w = 'N') then
					qt_dia_adic_w	:= qt_dia_adic_w + 1;
				end if;
				
				ds_hora_proc_w := replace(ds_hora_proc_w,'A','');
				ds_hora_proc_w := replace(ds_hora_proc_w,'A','');
		
				if	(ie_proc_urgente_w	= 'S') then
					dt_prescricao_ww	:= nvl(dt_liberacao_w,dt_prescricao_ww);
				else
					dt_prescricao_ww	:= dt_prescricao_w;--nvl(dt_inicio_prescr_w,dt_prescricao_w);
				end if;
			
				if 	(dt_prev_execucao_w >= dt_prescricao_ww) then
					dt_horario_proc_w := trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_prev_execucao_w, replace(ds_hora_proc_w,'A','')), 'mi');
				else
					dt_horario_proc_w := trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_prev_execucao_w + qt_dia_adic_w, replace(ds_hora_proc_w,'A','')), 'mi');				
				end if;
										
				if	((dt_horario_proc_w < trunc(dt_prescricao_ww,'mi')) or 
					 (qt_dia_adic_w > 0)) and
					(ie_proc_urgente_w = 'N') then					
					dt_horario_proc_w := dt_horario_proc_w + 1;
				end if;

				select	prescr_proc_hor_seq.nextval
				into	nr_sequencia_w
				from	dual;

				if	(ie_se_necessario_w = 'S') or
					(ie_acm_w = 'S') then
					ie_horario_especial_w	:= 'S';
				else	
					ie_horario_especial_w	:= 'N';
				end if;
				if	(nr_seq_derivado_w is not null) and
					(nr_seq_derivado_w > 0) then
					nr_seq_etapa_w := nr_seq_etapa_w + 1;
				end if;
				insert into prescr_proc_hor(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_prescricao,
					nr_seq_procedimento,
					cd_procedimento,
					ie_origem_proced,
					nr_seq_proc_interno,
					ds_horario,
					dt_horario,
					nr_ocorrencia,
					ie_urgente,
					ie_horario_especial, 
					cd_material_exame,
					ie_aprazado,
					dt_lib_horario,
					nr_etapa) 
				values	(
					nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_prescricao_p,
					nr_seq_procedimento_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					nr_seq_proc_interno_w,
					ds_hora_proc_w,
					dt_horario_proc_w,
					nr_ocor_proc_w,
					ie_proc_urgente_w,
					nvl(ie_horario_especial_w,'N'),
					cd_material_exame_w,
					'N',
					sysdate,
					decode(nr_seq_etapa_w, 0, null, nr_seq_etapa_w));
				end;
			end loop;
			end;
		else	
						
			ds_hora_proc_w		:= nvl(hr_prim_horario_w, substr(to_char(dt_primeiro_horario_w,'dd/mm/yyyy hh24:mi:ss'),12,5));
			dt_horario_proc_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_prev_execucao_w, ds_hora_proc_w), 'mi');

			select	prescr_proc_hor_seq.nextval
			into	nr_sequencia_w
			from	dual;

			if	(ie_se_necessario_w = 'S') or
				(ie_acm_w = 'S') then
				ie_horario_especial_w	:= 'S';
			else	
				ie_horario_especial_w	:= 'N';
			end if;
			
			insert into prescr_proc_hor(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_procedimento,
				cd_procedimento,
				ie_origem_proced,
				nr_seq_proc_interno,
				ds_horario,
				dt_horario,
				nr_ocorrencia,
				ie_urgente,
				ie_horario_especial,
				cd_material_exame,
				ie_aprazado,
				dt_lib_horario) 
			values	(
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				nr_seq_procedimento_w,
				cd_procedimento_w,
				ie_origem_proced_w,
				nr_seq_proc_interno_w,
				ds_hora_proc_w,
				dt_horario_proc_w,
				nr_ocor_proc_w,
				ie_proc_urgente_w,
				nvl(ie_horario_especial_w,'S'),
				cd_material_exame_w,
				'N',
				sysdate);
		end if;
		end;
		
		ie_proc_atual_w	:= 'S';

		open c15;
		loop
		fetch c15 into 
			dt_horario_proc_prev_w,
			nr_seq_horario_w;
		exit when c15%notfound;
							
			if	(ie_proc_atual_w = 'N') then
			
				select 	nvl(max(nr_sequencia),0) + 1
				into	nr_seq_procedimento_novo_w
				from	prescr_procedimento
				where 	nr_prescricao = nr_prescricao_p;

				insert into prescr_procedimento(
					nr_prescricao,
					nr_sequencia,
					nr_seq_origem,
					dt_prev_execucao,
					qt_procedimento,
					nm_usuario,
					dt_atualizacao,
					cd_procedimento,
					ie_origem_proced,
					nr_seq_proc_interno,
					ie_urgencia,
					nr_seq_exame,
					ie_status_atend,
					ie_status_execucao,
					cd_setor_atendimento,
					cd_setor_coleta,
					cd_setor_entrega,
					cd_setor_exec_fim,
					cd_setor_exec_inic,
					--nr_seq_lab,
					ie_origem_inf,
					cd_motivo_baixa,
					ie_suspenso,
					ds_observacao,
					ds_dado_clinico,
					ds_material_especial,
					ie_amostra,
					cd_material_exame,
					nr_seq_derivado,
					ie_util_hemocomponente,
					qt_vol_hemocomp,
					cd_intervalo)
				values	(nr_prescricao_p,
					nr_seq_procedimento_novo_w,
					nr_seq_procedimento_w,
					dt_horario_proc_prev_w,
					decode(nr_seq_derivado_w,null,1,qt_procedimento_w),
					nm_usuario_p,
					sysdate,
					cd_procedimento_w,
					ie_origem_proced_w,
					nr_seq_proc_interno_w,
					ie_proc_urgente_w,
					nr_seq_exame_w,
					ie_status_atend_w,
					ie_status_execucao_w,
					cd_setor_atendimento_proc_w,
					cd_setor_coleta_w,
					cd_setor_entrega_w,
					cd_setor_exec_fim_w,
					cd_setor_exec_inic_w,
					--nr_seq_lab_w,
					1,
					0,
					ie_suspenso_w,
					ds_observacao_w,
					ds_dado_clinico_w,
					ds_material_especial_w,
					ie_amostra_w,
					cd_material_exame_w,
					nr_seq_derivado_w,
					ie_util_hemocomponente_w,
					qt_vol_hemocomp_w,
					cd_intervalo_w);
					
				update	prescr_proc_hor
				set	nr_seq_proc_origem	= nr_seq_procedimento_novo_w
				where	nr_sequencia		= nr_seq_horario_w;
			
			end if;
			
			ie_proc_atual_w := 'N';
			
		end loop;
		close c15;
		
	end loop;
	close C13;
	
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_prescr_proc_hor;
/
