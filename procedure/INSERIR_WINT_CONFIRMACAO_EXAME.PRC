create or replace procedure INSERIR_WINT_CONFIRMACAO_EXAME (nm_usuario_p	varchar2,
                                                    nr_iterno_conta_p     number) is

nr_seq_proc_controler_w    number;
nr_seq_proc_w              number;
nr_seq_proc_exist_w        number;

begin
nr_seq_proc_controler_w := 1;
nr_seq_proc_w := 0;
nr_seq_proc_exist_w := 0;


while (nr_seq_proc_controler_w is not null) loop
      
        SELECT 
            min(p.nr_sequencia)
            into nr_seq_proc_controler_w
        FROM 
            tiss_conta_proc p,
            tiss_conta_atend q
        where 
            p.nr_interno_conta = nr_iterno_conta_p
        and 
            p.nr_sequencia  > nr_seq_proc_controler_w
        and 
            q.nr_interno_conta = p.nr_interno_conta
        and 
            obter_tipo_atendimento(q.nr_atendimento) <> 1

        order by p.nr_sequencia ASC;
        
        select 
            count(*)
            into nr_seq_proc_exist_w
        from
            WINT_CONFIRMACAO_EXAME p,
            TISS_CONTA_PROC t
        where 
            p.NR_SEQ_TISS_CONTA_PROC = t.NR_SEQUENCIA
        and
            t.nr_sequencia = nr_seq_proc_controler_w;
           
        if (nr_seq_proc_exist_w < 1) then 
        
            if (nr_seq_proc_controler_w) is not null then
            
                insert into WINT_CONFIRMACAO_EXAME(
                    NR_SEQUENCIA,
                    DT_ATUALIZACAO,
                    NM_USUARIO,
                    DT_ATUALIZACAO_NREC,
                    NM_USUARIO_NREC,
                    CD_PROCEDIMENTO,
                    QT_PROCEDIMENTO,
                    DT_PROCEDIMENTO,
                    IE_STATUS,
                    NR_SEQ_TISS_CONTA_PROC
                )
                select
                    WINT_CONFIRMACAO_EXAME_SEQ.nextval,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    CD_PROCEDIMENTO,
                    QT_PROCEDIMENTO,
                    DT_PROCEDIMENTO,
                    'N',
                    nr_sequencia
                FROM
                    TISS_CONTA_PROC
                where
                    NR_SEQUENCIA = nr_seq_proc_controler_w;

                select
                    max(NR_SEQUENCIA) 
                    into nr_seq_proc_w
                from
                    WINT_CONFIRMACAO_EXAME;

                insert into WINT_CONF_EXAME_INSUMO(
                    NR_SEQUENCIA,
                    DT_ATUALIZACAO,
                    NM_USUARIO,
                    DT_ATUALIZACAO_NREC,
                    NM_USUARIO_NREC,
                    DT_ITEM,
                    CD_ITEM,
                    DS_ITEM,
                    QT_ITEM,
                    NR_SEQ_WINT_EXAME,
                    NR_SEQ_TISS_CONTA_DESP
                )
                SELECT
                    WINT_CONF_EXAME_INSUMO_SEQ.nextval,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,                
                    p.dt_conta,
                    p.cd_material,
                    obter_desc_material(p.cd_material),
                    p.qt_material,
                    nr_seq_proc_w,
                    m.nr_sequencia
                FROM
                    tiss_conta_desp m,
                    MATERIAL_ATEND_PACIENTE p,
                    TISS_CONTA_PROC t
                where
                    p.nr_seq_proc_princ = t.nr_seq_proc
                
                and 
                    t.nr_interno_conta = m.nr_interno_conta
                and 
                    m.CD_PROCEDIMENTO like '%' || LPAD(p.cd_material,8,0)
                and 
                    m.dt_item like to_date(p.dt_conta,'dd/mm/yyyy')
                and 
                    t.nr_sequencia = nr_seq_proc_controler_w
                 AND 
                    p.qt_material > 0;
            end if;
            
        end if;
        
    end loop;

commit;

end INSERIR_WINT_CONFIRMACAO_EXAME;
/
