create or replace 
procedure liberar_projeto_recurso(nr_sequencia_p		number,
			     nm_usuario_p		varchar2) is

begin

update	projeto_recurso
set	nm_usuario_lib	= nm_usuario_p,
	dt_liberacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

commit;

end liberar_projeto_recurso;
/
