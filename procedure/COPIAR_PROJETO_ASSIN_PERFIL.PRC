create or replace
procedure copiar_projeto_assin_perfil (nr_sequencia_p	number, 
		cd_perfil_p	number,          
		nm_usuario_p	varchar2) is
begin

if	(nr_sequencia_p is not null)	and  
	(cd_perfil_p is not null)	and 
	(nm_usuario_p is not null)	then
	insert into tasy_proj_assin_perfil(nr_sequencia,
					   nr_seq_proj,                                                                             					   dt_atualizacao,
					   nm_usuario,
					   dt_atualizacao_nrec,
					   nm_usuario_nrec,
					   cd_perfil,
					   ie_assinatura,
					   ie_consulta,
					   ie_forma_visualizar,
					   ie_consulta_automatica,
					   ie_alertar,
					   ds_diretorio,
					   nm_arquivo,
					   ie_gerar_pdf,
					   ie_solic_senha,
					   ds_mensagem_certificado,
					   ie_imprimir,
					   ds_mensagem_sucesso,
					   ie_nivel_log)
					   select tasy_proj_assin_perfil_seq.nextval,
						  a.nr_seq_proj,
						  sysdate,
						  nm_usuario_p,
						  sysdate,
						  nm_usuario_p,
						  cd_perfil_p,
						  a.ie_assinatura,
						  a.ie_consulta,
						  a.ie_forma_visualizar,
						  a.ie_consulta_automatica,
						  a.ie_alertar,
						  a.ds_diretorio,
						  a.nm_arquivo,
						  a.ie_gerar_pdf,
						  a.ie_solic_senha,
						  a.ds_mensagem_certificado,
						  a.ie_imprimir,
						  a.ds_mensagem_sucesso,
						  nvl(a.ie_nivel_log,0)
					   from  tasy_proj_assin_perfil a
					   where a.nr_sequencia = nr_sequencia_p
					   and	 not exists (select 1
							            from 	tasy_proj_assin_perfil b 
								    where	b.nr_seq_proj = a.nr_seq_proj and 
										b.cd_perfil   = cd_perfil_p);
	commit;
end if;

end copiar_projeto_assin_perfil;
/