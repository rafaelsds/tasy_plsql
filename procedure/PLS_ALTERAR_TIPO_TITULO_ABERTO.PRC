create or replace
procedure pls_alterar_tipo_titulo_aberto
			(	nr_titulo_p		number,
				nr_seq_tit_reenvio_p	number,
				cd_banco_p		number,
				nr_seq_conta_banco_p	number,
				nr_seq_carteira_cobr_p	number,
				cd_tipo_portador_p	number,
				cd_portador_p		number,
				ds_observacao_p		varchar2,
				ie_alterar_vencimento_p	varchar2,
				dt_vencimento_p		alteracao_vencimento.dt_vencimento%type,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

ds_historico_w		varchar2(4000);
cd_banco_w		number(5);
nr_seq_conta_banco_w	number(10);
nr_seq_carteira_cobr_w	number(10);
cd_tipo_portador_w	number(5);
cd_portador_w		number(10);
ie_tipo_titulo_w	varchar2(2);
ds_antigo_w		varchar2(255);
ds_novo_w		varchar2(255);
nr_seq_mensalidade_w	number(10);
dt_vencimento_w		date;
nr_seq_alteracao_w	number(10);
ds_obervacao_w		varchar2(255);
vl_saldo_w		titulo_receber.vl_saldo_titulo%type;
vl_apropriacao_max_w	pls_regra_aprop_desc_folha.vl_apropriacao_max%type;
nr_seq_classif_itens_w	pls_contrato_pagador.nr_seq_classif_itens%type;

begin

/*Alterado os seguintes dados do titulo: */
ds_historico_w	:= wheb_mensagem_pck.get_texto(303476) ||chr(13)||chr(10);

select	a.cd_banco,
	a.nr_seq_conta_banco,
	a.nr_seq_carteira_cobr,
	a.cd_tipo_portador,
	a.cd_portador,
	a.ie_tipo_titulo,
	a.nr_seq_mensalidade,
	a.dt_vencimento,
	a.vl_saldo_titulo,
	(select	max(x.nr_seq_classif_itens)
	from	pls_contrato_pagador x
	where	x.nr_sequencia = a.nr_seq_pagador)
into	cd_banco_w,
	nr_seq_conta_banco_w,
	nr_seq_carteira_cobr_w,
	cd_tipo_portador_w,
	cd_portador_w,
	ie_tipo_titulo_w,
	nr_seq_mensalidade_w,
	dt_vencimento_w,
	vl_saldo_w,
	nr_seq_classif_itens_w
from	titulo_receber a
where	a.nr_titulo	= nr_titulo_p;

select	nvl(max(vl_apropriacao_max), 0)
into	vl_apropriacao_max_w
from	pls_regra_aprop_desc_folha
where	cd_estabelecimento = cd_estabelecimento_p
and	((nr_seq_classif_itens is null) or (nr_seq_classif_itens = nr_seq_classif_itens_w))
and	vl_saldo_w <= vl_apropriacao_max;

if	(vl_apropriacao_max_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1123992, 'VL_LIMITE='||to_char(vl_apropriacao_max_w,'999999999.99'));
end if;

if	((nr_seq_mensalidade_w is not null) and
	(ds_observacao_p is not null)) then
	update	pls_mensalidade
	set	ds_observacao	= ds_observacao_p
	where	nr_sequencia	= nr_seq_mensalidade_w;
end if;

if	(ie_alterar_vencimento_p = 'S') then
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_alteracao_w
	from	alteracao_vencimento
	where	nr_titulo = nr_titulo_p;
	
	/*Data de vencimento alterada pois o tipo do titulo foi alterado de "Desconto em Folha" para "Bloqueto"!*/
	ds_obervacao_w		:= wheb_mensagem_pck.get_texto(303477);
		
	insert	into	alteracao_vencimento
		(	nr_sequencia, nr_titulo, dt_anterior,
			dt_vencimento, cd_motivo, dt_atualizacao,
			nm_usuario, dt_alteracao, ds_observacao)
		values
		(	nr_seq_alteracao_w, nr_titulo_p, dt_vencimento_w,
			dt_vencimento_p, 1, sysdate,
			nm_usuario_p, sysdate, ds_obervacao_w);
	
	update	titulo_receber
	set	dt_pagamento_previsto	= dt_vencimento_p
	where	nr_titulo	= nr_titulo_p;
end if;

if	(cd_banco_w <> cd_banco_p) then
	begin
	select	ds_banco
	into	ds_antigo_w
	from	banco
	where	cd_banco	= cd_banco_w;
	exception
	when others then
		ds_antigo_w	:= ' ';
	end;
	
	begin
	select	ds_banco
	into	ds_novo_w
	from	banco
	where	cd_banco	= cd_banco_w;
	exception
	when others then
		ds_novo_w	:= ' ';
	end;
	
	/*Banco, de #@DS_ANTIGO_W#@ para #@DS_NOVO_W#@.*/
	ds_historico_w	:= ds_historico_w || wheb_mensagem_pck.get_texto(303478, 'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w)||chr(13)||chr(10);
end if;

if	(nr_seq_conta_banco_w <> nr_seq_conta_banco_p) then
	begin
	select	ds_conta
	into	ds_antigo_w
	from	banco_estabelecimento_v
	where	nr_sequencia	= nr_seq_conta_banco_w;
	exception
	when others then
		ds_antigo_w	:= ' ';
	end;
	
	begin
	select	ds_conta
	into	ds_novo_w
	from	banco_estabelecimento_v
	where	nr_sequencia	= nr_seq_conta_banco_p;
	exception
	when others then
		ds_novo_w	:= ' ';
	end;
	
	/* Agencia/Conta bancaria, de #@DS_ANTIGO_W#@ para #@DS_NOVO_W#@.*/
	ds_historico_w	:= ds_historico_w || wheb_mensagem_pck.get_texto(303480, 'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w)||chr(13)||chr(10);
end if;	
	
if	(nr_seq_carteira_cobr_w <> nr_seq_carteira_cobr_p) then
	begin
	select	ds_carteira
	into	ds_antigo_w
	from	banco_carteira
	where	nr_sequencia = nr_seq_carteira_cobr_w
	and	((cd_estabelecimento	= cd_estabelecimento_p) or (cd_estabelecimento is null));
	exception
	when others then
		ds_antigo_w	:= ' ';
	end;
	
	begin
	select	ds_carteira
	into	ds_novo_w
	from	banco_carteira
	where	nr_sequencia = nr_seq_carteira_cobr_p
	and	((cd_estabelecimento	= cd_estabelecimento_p) or (cd_estabelecimento is null));
	exception
	when others then
		ds_novo_w	:= ' ';
	end;
	
	/*Carteira cobranca, de #@DS_ANTIGO_W#@ para #@DS_NOVO_W#@.*/
	ds_historico_w	:= ds_historico_w || wheb_mensagem_pck.get_texto(303481, 'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w)||chr(13)||chr(10);
end if;

if	(cd_tipo_portador_w <> cd_tipo_portador_p) then
	select	obter_valor_dominio(703,cd_tipo_portador_w),
		obter_valor_dominio(703,cd_tipo_portador_p)
	into	ds_antigo_w,
		ds_novo_w
	from	dual;
	
	/*Tipo portador, de #@DS_ANTIGO_W#@ para #@DS_NOVO_W#@.*/
	ds_historico_w	:= ds_historico_w || wheb_mensagem_pck.get_texto(303482, 'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w)||chr(13)||chr(10);
end if;

if	(cd_portador_w <> cd_portador_p) then
	begin
	select	ds_portador
	into	ds_antigo_w
	from	portador
	where	cd_portador = cd_portador_w;
	exception
	when others then
		ds_antigo_w	:= ' ';
	end;
	
	begin
	select	ds_portador
	into	ds_novo_w
	from	portador
	where	cd_portador = cd_portador_p;
	exception
	when others then
		ds_novo_w	:= ' ';
	end;
	
	/*Portador, de #@DS_ANTIGO_W#@ para #@DS_NOVO_W#@.*/
	ds_historico_w	:= ds_historico_w || wheb_mensagem_pck.get_texto(303483, 'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w)||chr(13)||chr(10);
end if;

select	obter_valor_dominio(712,ie_tipo_titulo_w)
into	ds_antigo_w
from	dual;

/*Tipo titulo, de #@DS_ANTIGO_W#@ para Bloqueto(Boleto Bancario).*/
ds_historico_w	:= ds_historico_w || wheb_mensagem_pck.get_texto(303484, 'DS_ANTIGO_W=' || ds_antigo_w);

insert	into	pls_desc_reenvio_hist
	(	nr_sequencia, nr_seq_tit_reenvio, cd_estabelecimento,
		dt_historico, nm_usuario_historico, dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		ds_historico, dt_liberacao)
	values
	(	pls_desc_reenvio_hist_seq.nextval, nr_seq_tit_reenvio_p, cd_estabelecimento_p,
		sysdate, nm_usuario_p, sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		ds_historico_w, sysdate);

insert	into	titulo_receber_hist /* Foi solicitado pelo cliente, para gravar as alteracoes tambem no historico do titulo */
	(	nr_sequencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, nr_titulo,
		dt_historico, ds_historico, dt_liberacao)
	values
	(	titulo_receber_hist_seq.nextval, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, nr_titulo_p,
		sysdate, ds_historico_w, sysdate);

update	titulo_receber
set	ie_tipo_titulo		= '1',
	cd_banco		= cd_banco_p,
	nr_seq_conta_banco	= nr_seq_conta_banco_p,
	nr_seq_carteira_cobr	= nr_seq_carteira_cobr_p,
	cd_tipo_portador	= cd_tipo_portador_p,
	cd_portador		= cd_portador_p,
	ds_observacao_titulo	= decode(ds_observacao_titulo,null,ds_observacao_p,ds_observacao_titulo||' - '||ds_observacao_p),
	nm_usuario		= nm_usuario_p
where	nr_titulo		= nr_titulo_p;

gerar_bloqueto_tit_rec(nr_titulo_p, 'OPSDF');

commit;

end pls_alterar_tipo_titulo_aberto;
/
