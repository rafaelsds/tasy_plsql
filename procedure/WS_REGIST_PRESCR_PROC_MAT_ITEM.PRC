create or replace
procedure ws_regist_prescr_proc_mat_item (	nr_seq_grupo_mat_p	number,
					nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					cd_barras_p		varchar2,
					nm_usuario_p		Varchar2,
					cd_erro_p		out varchar2,
					ds_erro_p		out varchar2) is 

/*
0 - SUCESSO
1 - Dados n�o informados
	1.1 - Sequ�ncia da prescri��o n�o informado
2 - Dados inconsistentes
	2.1 - Procedimento n�o encontrado
3 - Erro ao registrar
*/											
											
ie_existe_w			varchar2(1);											
ie_status_atend_w	prescr_procedimento.ie_status_atend%type;
											
begin

cd_erro_p	:= 0;

if	(nr_seq_prescr_p is null) then
	cd_erro_p	:= 1.1;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278535,null);
end if;

if	(cd_erro_p = 0) then

	select	decode(count(*),0,'N','S')
	into	ie_existe_w
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_p
	and		a.nr_sequencia = nr_seq_prescr_p;
	
	if	(ie_existe_w = 'N') then
		cd_erro_p	:= 2.1;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278536,'NR_PRESCRICAO='||to_char(nr_prescricao_p)||';NR_SEQ_PRESCR='||to_char(nr_seq_prescr_p));
	end if;
	
	if	(cd_erro_p = 0) then
		
		begin
		
		select	max(a.ie_status_atend)
		into	ie_status_atend_w
		from	prescr_procedimento a
		where	a.nr_prescricao = nr_prescricao_p
		and		a.nr_sequencia = nr_seq_prescr_p;
		
		insert into prescr_proc_mat_item(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_prescr,
				nr_seq_prescr_proc_mat,
				ie_status,
				cd_barras
				--,nr_seq_frasco
				)
		values	(prescr_proc_mat_item_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				nr_seq_prescr_p,
				nr_seq_grupo_mat_p,
				ie_status_atend_w,
				cd_barras_p
				--,nr_seq_frasco_w
				);						
		commit;	
		exception
		when others then
			cd_erro_p	:= 3;
			ds_erro_p	:= substr(sqlerrm,1,2000);
		end;	
		
	end if;

end if;	

end ws_regist_prescr_proc_mat_item;
/