create or replace
procedure qua_duplicar_auditoria(	nr_seq_auditoria_p	number,
				nm_usuario_p	Varchar2) is 


/*Tipo auditoria*/
nr_seq_tipo_w			number(10,0);

/*Ambiente*/
nr_seq_ambiente_ant_w		number(10,0);
cd_estabelecimento_w		number(04,0);
ds_ambiente_w			varchar2(80);
ie_situacao_amb_w			varchar2(01);
nr_seq_ambiente_w			number(10,0);

/*Ambiente_material*/
nr_seq_material_w			number(10,0);
ds_localizacao_w			varchar2(80);
ds_observacao_w			varchar2(255);
qt_material_w			number(15,2);
nr_seq_amb_mat_w			number(10,0);

/*Captulo*/
ds_capitulo_w			varchar2(80);
ie_situacao_cap_w			varchar2(01);
nr_seq_capitulo_w			number(10,0);
nr_seq_cap_ant_w			number(10,0);
nr_seq_apres_cap_w		number(05,0);

/*Estrutura captulo*/
nr_seq_estrut_w			number(10,0);
nr_seq_estrut_ant_w		number(10,0);
ds_estrutura_w			varchar2(2000);
cd_classificacao_w			varchar2(20);
ie_obrigatorio_w			varchar2(01);
nr_seq_apres_estrut_w		number(05,0);

/*Item estrutura*/
ds_item_w			varchar2(2000);
ie_situacao_item_w			varchar2(01);
ds_orientacao_w			varchar2(2000);
nr_seq_apres_w			number(05,0);
vl_item_w				number(15,2);
ie_permite_na_w			varchar2(01);
ie_mat_ambiente_w			varchar2(01);
nr_seq_item_w			number(10,0);

sequence_item_ant_w         qua_auditoria_item.nr_sequencia%type;
ds_orientation_subitem_w    qua_auditoria_subitem.ds_orientacao%type;
presentation_subitem_w      qua_auditoria_subitem.nr_seq_apres%type;
description_subitem_w       qua_auditoria_subitem.ds_item%type;
sequence_subitem_w          qua_auditoria_subitem.nr_sequencia%type;

cursor c01 is
select	nr_sequencia,
	cd_estabelecimento,
	ds_ambiente,
	ie_situacao
from	qua_auditoria_ambiente
where	nr_seq_tipo	= nr_seq_auditoria_p;

cursor c02 is
select	nr_seq_material,
	ds_localizacao,
	ds_observacao,
	qt_material
from	qua_audit_amb_mat
where	nr_seq_audit_ambiente = nr_seq_ambiente_ant_w;

Cursor C03 is
select	nr_sequencia,
	ds_capitulo,
	ie_situacao,
	nr_seq_apres
from	qua_auditoria_cap
where	nr_seq_tipo	= nr_seq_auditoria_p;

Cursor C04 is
select	nr_sequencia,
	ds_estrutura,
	cd_classificacao,
	ie_obrigatorio,
	nr_seq_apres
from	qua_auditoria_estrut
where	nr_seq_cap 	= nr_seq_cap_ant_w
and	nr_seq_tipo	= nr_seq_auditoria_p;

Cursor C05 is
select	nr_sequencia,
        ds_item,
	      ie_situacao,
	      ds_orientacao,
	      nr_seq_apres,
	      vl_item,
	      ie_permite_na,
	      ie_mat_ambiente
from	  qua_auditoria_item
where	  nr_seq_estrutura = nr_seq_estrut_ant_w;

Cursor C06 is
select	ds_orientacao,
        nr_seq_apres,
        ds_item
from	  qua_auditoria_subitem
where	  nr_seq_item = sequence_item_ant_w;

begin

select	qua_auditoria_tipo_seq.nextval
into	nr_seq_tipo_w
from	dual;

insert into qua_auditoria_tipo(
		nr_sequencia,
		cd_empresa,
		dt_atualizacao,
		nm_usuario,
		ds_tipo,
		ie_situacao,
		ds_formato_classif,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_mat_ambiente,
		ie_prontuario)
	select	nr_seq_tipo_w,
		cd_empresa,
		sysdate,
		nm_usuario_p,
		substr(WHEB_MENSAGEM_PCK.get_texto(808180) || ' ' || ds_tipo,1,80),
		ie_situacao,
		ds_formato_classif,
		sysdate,
		nm_usuario_p,
		ie_mat_ambiente,
		ie_prontuario
	from	qua_auditoria_tipo
	where	nr_sequencia = nr_seq_auditoria_p;

open C01;
loop
fetch C01 into	
	nr_seq_ambiente_ant_w,
	cd_estabelecimento_w,
	ds_ambiente_w,
	ie_situacao_amb_w;
exit when C01%notfound;
	begin
	
	select	qua_auditoria_ambiente_seq.nextval
	into	nr_seq_ambiente_w
	from	dual;	
	
	insert into qua_auditoria_ambiente(
			nr_sequencia,
			cd_estabelecimento,
			nr_seq_tipo,
			dt_atualizacao,
			nm_usuario,
			ds_ambiente,
			ie_situacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	nr_seq_ambiente_w,
			cd_estabelecimento_w,
			nr_seq_tipo_w,
			sysdate,
			nm_usuario_p,
			ds_ambiente_w,
			ie_situacao_amb_w,
			sysdate,
			nm_usuario_p);
	
	open C02;
	loop
	fetch C02 into
		nr_seq_material_w,
		ds_localizacao_w,
		ds_observacao_w,
		qt_material_w;
	exit when C02%notfound;
		begin
		select	qua_audit_amb_mat_seq.nextval
		into	nr_seq_amb_mat_w
		from	dual;
		
		insert into qua_audit_amb_mat(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_material,
				ds_localizacao,
				ds_observacao,
				nr_seq_audit_ambiente,
				qt_material)
			values(	nr_seq_amb_mat_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_material_w,
				ds_localizacao_w,
				ds_observacao_w,
				nr_seq_ambiente_w,
				qt_material_w);
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;
	
open C03;
loop
fetch C03 into	
	nr_seq_cap_ant_w,
	ds_capitulo_w,
	ie_situacao_cap_w,
	nr_seq_apres_cap_w;
exit when C03%notfound;
	begin
	select	qua_auditoria_cap_seq.nextval
	into	nr_seq_capitulo_w
	from	dual;
	
	insert into qua_auditoria_cap(
		nr_sequencia,
		nr_seq_tipo,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_capitulo,
		ie_situacao,
		nr_seq_apres)
	values(	nr_seq_capitulo_w,
		nr_seq_tipo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_capitulo_w,
		ie_situacao_cap_w,
		nr_seq_apres_cap_w);
	end;

	open C04;
	loop
	fetch C04 into	
		nr_seq_estrut_ant_w,
		ds_estrutura_w,
		cd_classificacao_w,
		ie_obrigatorio_w,
		nr_seq_apres_estrut_w;
	exit when C04%notfound;
		begin
		select	qua_auditoria_estrut_seq.nextval
		into	nr_seq_estrut_w
		from	dual;
		
		insert into qua_auditoria_estrut(
				nr_sequencia,
				nr_seq_tipo,
				dt_atualizacao,
				nm_usuario,
				ds_estrutura,
				cd_classificacao,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_obrigatorio,
				nr_seq_cap,
				nr_seq_apres)
			values(	nr_seq_estrut_w,
				nr_seq_tipo_w,
				sysdate,
				nm_usuario_p,
				ds_estrutura_w,
				cd_classificacao_w,
				sysdate,
				nm_usuario_p,
				ie_obrigatorio_w,
				nr_seq_capitulo_w,
				nr_seq_apres_estrut_w);
		end;

		open C05;
		loop
		fetch C05 into
              sequence_item_ant_w,
              ds_item_w,
              ie_situacao_item_w,
              ds_orientacao_w,
              nr_seq_apres_w,
              vl_item_w,
              ie_permite_na_w,
              ie_mat_ambiente_w;
		exit when C05%notfound;
			begin
        select	qua_auditoria_item_seq.nextval
        into	  nr_seq_item_w
        from	  dual;

        insert into qua_auditoria_item(
                                      nr_sequencia,
                                      nr_seq_estrutura,
                                      dt_atualizacao,
                                      nm_usuario,
                                      ds_item,
                                      ie_situacao,
                                      ds_orientacao,
                                      nr_seq_apres,
                                      vl_item,
                                      ie_permite_na,
                                      dt_atualizacao_nrec,
                                      nm_usuario_nrec,
                                      ie_mat_ambiente)
                                values(	
                                      nr_seq_item_w,
                                      nr_seq_estrut_w,
                                      sysdate,
                                      nm_usuario_p,
                                      ds_item_w,
                                      ie_situacao_item_w,
                                      ds_orientacao_w,
                                      nr_seq_apres_w,
                                      vl_item_w,
                                      ie_permite_na_w,
                                      sysdate,
                                      nm_usuario_p,
                                      ie_mat_ambiente_w);

        open C06;
          loop
          fetch C06 into	
                    ds_orientation_subitem_w,
                    presentation_subitem_w,
                    description_subitem_w;
          exit when C06%notfound;
          begin          
            select nr_sequencia 
            into   sequence_subitem_w
            from   (select  nr_sequencia
                    from    qua_auditoria_subitem
                    order by nr_sequencia desc)
            where rownum = 1;

            insert into qua_auditoria_subitem(
                                          nr_sequencia,
                                          nr_seq_item,
                                          dt_atualizacao,
                                          nm_usuario,
                                          dt_atualizacao_nrec,
                                          nm_usuario_nrec,
                                          ds_orientacao,
                                          nr_seq_apres,
                                          ds_item)
                                    values(
                                          sequence_subitem_w+1,
                                          nr_seq_item_w,
                                          sysdate,
                                          nm_usuario_p,
                                          sysdate,
                                          nm_usuario_p,
                                          ds_orientation_subitem_w,
                                          presentation_subitem_w,
                                          description_subitem_w);
            end;
        end loop;
        close C06;
			end;
		end loop;
		close C05;
	end loop;
	close C04;
end loop;
close C03;
	
commit;

end qua_duplicar_auditoria;
/