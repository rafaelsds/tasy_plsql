create or replace
procedure update_w_itens_contrato_selec(
		qt_contrato_p		number,
		nr_sequencia_p		number,
		nm_usuario_p		Varchar2) is 

begin
	
if (( qt_contrato_p <> null) and ( nr_sequencia_p <> null)) then
	begin
		update	w_itens_contrato_selec
		set  	qt_contrato = qt_contrato_p,
			nm_usuario = nm_usuario_p
		where	nr_sequencia = nr_sequencia_p;
	end;
end if;

commit;

end update_w_itens_contrato_selec;
/