create or replace
procedure traducao_atualizar_idioma_usu( 	nr_seq_idioma_p	number,
					 nm_usuario_p	Varchar2) is 

begin

philips_param_pck.set_nr_seq_idioma(null);
if	(nr_seq_idioma_p <> 0) then
	philips_param_pck.set_nr_seq_idioma(nr_seq_idioma_p);
end if;	

commit;

end traducao_atualizar_idioma_usu;
/