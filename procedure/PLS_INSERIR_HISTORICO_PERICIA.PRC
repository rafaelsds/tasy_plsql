create or replace
procedure pls_inserir_historico_pericia
		(	ie_tipo_historico_p	varchar2,
			nr_seq_segurado_p	number,
			nm_usuario_p		Varchar2) is 

begin	
pls_gerar_segurado_historico(	nr_seq_segurado_p, ie_tipo_historico_p, sysdate, null,
				'pls_inserir_historico_pericia', null, null, null,
				null, null, null, null,
				null, null, null, null,
				nm_usuario_p, 'S');
end pls_inserir_historico_pericia;
/