create or replace
procedure Nut_alterar_quant_producao(nr_seq_item_p	number,
				  qt_novo_p		number,	
				  nm_usuario_p		Varchar2) is 

qt_anterior_w		number(10,3);
qt_percent_deriv_w		number(17,2);
nr_seq_deriv_prod_w	number(10);

Cursor C01 is
	select	a.qt_porcentagem,		
		a.nr_sequencia
	from	nut_prod_lac_item_adic a
	where	a.nr_Seq_prod_item	= nr_seq_item_p;

begin

select	qt_dose
into	qt_anterior_w
from	nut_prod_lac_item_adic
where	nr_seq_prod_item = nr_seq_item_p;

update	nut_prod_lac_item_adic
set	qt_dose = qt_novo_p
where nr_seq_prod_item = nr_seq_item_p;

open C01;
loop
fetch C01 into
	qt_percent_deriv_w,
	nr_seq_deriv_prod_w;
exit when C01%notfound;
	begin

	update	nut_prod_lac_item_adic
	set	qt_dose		= (qt_percent_deriv_w * qt_novo_p)/100
	where	nr_sequencia 	= nr_seq_deriv_prod_w;	
	
	end;
end loop;
close C01;

insert into nut_prod_alt_item	(NR_SEQUENCIA,
				DT_ATUALIZACAO,
				NM_USUARIO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO_NREC,				
				DT_ALTERACAO,
				QT_ANTERIOR,
				QT_ATUAL,
				NR_SEQ_NUT_PROD_LAC_ITEM)
			values	(nut_prod_alt_item_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,	
				sysdate,
				qt_anterior_w,
				qt_novo_p,
				nr_seq_item_p);

commit;

end Nut_alterar_quant_producao;
/
