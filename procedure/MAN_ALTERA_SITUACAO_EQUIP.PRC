create or replace
procedure man_altera_situacao_equip(
	nr_seq_equip_p		number,
	ie_situacao_p		varchar2,
	nm_usuario_p		Varchar2) is 

begin

if	(nr_seq_equip_p is not null) then
	begin

update	equipamento
set	ie_situacao = ie_situacao_p,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_seq_equipamento_man = nr_seq_equip_p;
	
	end;
end if;

commit;

end man_altera_situacao_equip;
/