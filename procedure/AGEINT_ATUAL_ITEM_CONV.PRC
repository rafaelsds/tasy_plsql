create or replace
procedure ageint_atual_item_conv(
			nr_seq_agenda_item_p	number,
			nr_seq_ageint_item_adic_p	number,
			nm_usuario_p		Varchar2) is 

 
nr_seq_ageint_w         number(10);
cd_convenio_w           number(5);
cd_categoria_w          varchar2(10);
cd_estabelecimento_w    number(4);
dt_inicio_agendamento_w date;
cd_plano_w              varchar2(10);
cd_usuario_convenio_w   varchar2(30);
cd_pessoa_fisica_w      varchar2(10);
ie_tipo_atendimento_w   number(3);
nr_seq_ageint_item_w    number(10);
	
qt_exames_assoc_w		number(10);
nr_seq_proc_interno_w	number(10);
qt_idade_pac_w			number(3);
nr_seq_ag_integrada_w	number(10);
nr_seq_grupo_selec_w	number(10);
ds_erro_w				varchar2(255);

BEGIN

update  agenda_integrada_item
set     dt_atualizacao  = sysdate,
        nm_usuario      = nm_usuario_p
where   nr_sequencia    = nr_seq_agenda_item_p;

select  a.nr_sequencia,
        a.cd_convenio,
        a.cd_categoria,
        a.cd_estabelecimento,
        a.dt_inicio_agendamento,
        a.cd_plano,
        a.cd_usuario_convenio,
        a.cd_pessoa_fisica,
        a.ie_tipo_atendimento
into    nr_seq_ageint_w,
        cd_convenio_w,
        cd_categoria_w,
        cd_estabelecimento_w,
        dt_inicio_agendamento_w,
        cd_plano_w,
        cd_usuario_convenio_w,
        cd_pessoa_fisica_w,
        ie_tipo_atendimento_w
from    agenda_integrada a,
        agenda_integrada_item b
where   a.nr_sequencia  = b.nr_seq_agenda_int
and     b.nr_sequencia  = nr_seq_agenda_item_p;
 
calc_valor_proc_ageint_item(
                        nr_seq_ageint_w,cd_convenio_w,cd_categoria_w,cd_estabelecimento_w,dt_inicio_agendamento_w,cd_plano_w,
                        nm_usuario_p,cd_usuario_convenio_w,cd_pessoa_fisica_w,ie_tipo_Atendimento_w,nr_seq_agenda_item_p,nr_seq_ageint_item_adic_p);
						
						
select	max(c.cd_convenio),
		max(a.qt_idade_pac),
		max(a.nr_sequencia),
		max(b.nr_seq_proc_interno),
		max(b.nr_seq_grupo_selec)
into	cd_convenio_w,
		qt_idade_pac_w,
		nr_seq_ag_integrada_w,
		nr_seq_proc_interno_w,
		nr_seq_grupo_selec_w
from	agenda_integrada a,
		agenda_integrada_item b,
		agenda_integrada_conv_item c
where	a.nr_sequencia = b.nr_seq_agenda_int
and		b.nr_sequencia	= c.nr_seq_agenda_item
and		b.nr_sequencia = nr_seq_agenda_item_p;		
				
select	nvl(count(*),0)
into	qt_exames_assoc_w
from	proc_int_proc_prescr
where	nr_seq_proc_interno	= nr_seq_proc_interno_w
and 	nvl(cd_convenio,cd_convenio_w) = cd_convenio_w
and		(cd_convenio_excluir is null or cd_convenio_excluir <> cd_convenio_w)
and		Obter_conv_excluir_proc_assoc(nr_sequencia, cd_convenio_w)	= 'S'
and		nvl(ie_somente_agenda_cir,'N') = 'N'		
and		(((qt_idade_pac_w is null) or (qt_idade_min is null and qt_idade_max is null)) or
		((qt_idade_pac_w is not null) and (qt_idade_pac_w between nvl(qt_idade_min,qt_idade_pac_w) and
		nvl(qt_idade_max,qt_idade_pac_w))))
and 	Obter_se_proc_interno_ativo(nr_seq_proc_int_adic) = 'A';		

if	(qt_exames_assoc_w > 0)then
	begin
	ageint_gerar_proc_Assoc(nr_seq_ag_integrada_w, nr_seq_agenda_item_p, nr_seq_proc_interno_w, nm_usuario_p, nr_seq_grupo_selec_w);
	
	exception
	when others then
		ds_erro_w := substr(sqlerrm,1,255);
	end;
end if;						

commit;

end Ageint_Atual_Item_Conv;
/