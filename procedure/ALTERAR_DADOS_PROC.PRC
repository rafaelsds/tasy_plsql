create or replace
procedure alterar_dados_proc(
				cd_senha_p		varchar2,
				nr_doc_conv_p		varchar2,
				nr_prescricao_p		number,
				nr_seq_proc_p		number) is 
dt_alta_w	date;
				
begin

if (cd_senha_p is not null) then

	update	prescr_procedimento
	set 	cd_senha = SUBSTR(cd_senha_p,1,20)
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_seq_proc_p;
	
	update	procedimento_paciente
	set	cd_senha = SUBSTR(cd_senha_p,1,20)
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia_prescricao = nr_seq_proc_p;
	
	update	autorizacao_convenio a
	set	a.cd_senha = SUBSTR(cd_senha_p,1,20)
	where	a.nr_prescricao = nr_prescricao_p;
	
end if;

if (nr_doc_conv_p is not null) then

	update	prescr_procedimento
	set	nr_doc_convenio = SUBSTR(nr_doc_conv_p,1,20)
	where	nr_prescricao   = nr_prescricao_p
	and	nr_sequencia    = nr_seq_proc_p;
	
	update	procedimento_paciente
	set	nr_doc_convenio = SUBSTR(nr_doc_conv_p,1,20)
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia_prescricao = nr_seq_proc_p;
	
	update	autorizacao_convenio a
	set	a.cd_autorizacao = SUBSTR(nr_doc_conv_p,1,20)
	where	a.nr_prescricao = nr_prescricao_p;

end if;

commit;

end alterar_dados_proc;
/

