create or replace
procedure lic_publica_resultado(	nr_seq_resultado_p			number,
				nr_seq_licitacao_p			number,
				nr_seq_local_public_p		number,
				dt_publicacao_p			date,
				nr_publicacao_p			varchar2,
				ds_observacao_p			varchar2,
				ds_arquivo_p			varchar2,
				nm_usuario_p			varchar2,
				nr_seq_public_res_p	out	number) is 

nr_sequencia_w		reg_lic_public_result.nr_sequencia%type;
					
begin

select	reg_lic_public_result_seq.nextval
into	nr_sequencia_w
from	dual;

insert into reg_lic_public_result(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_licitacao,
	nr_seq_resultado,
	nr_seq_local_public,
	dt_publicacao,
	nr_publicacao,
	ds_observacao,
	ds_arquivo)
values(	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_licitacao_p,
	nr_seq_resultado_p,
	nr_seq_local_public_p,
	dt_publicacao_p,
	nr_publicacao_p,
	ds_observacao_p,
	ds_arquivo_p);

insert into reg_lic_historico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_historico,
	ds_observacao,
	nr_seq_licitacao)
values(	reg_lic_historico_seq.nextval,
	sysdate,
	nm_usuario_p,
	'PR',
	WHEB_MENSAGEM_PCK.get_texto(805427) || ' ' || WHEB_MENSAGEM_PCK.get_texto(805564) || ': ' || nr_publicacao_p,
	nr_seq_licitacao_p);

commit;

nr_seq_public_res_p	:= nr_sequencia_w;

end lic_publica_resultado;
/