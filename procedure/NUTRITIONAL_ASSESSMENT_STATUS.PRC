create or replace procedure NUTRITIONAL_ASSESSMENT_STATUS (IE_PROFISSIONAL_P varchar2,
                                          NR_SEQUENCIA_P number,
                                          IE_ACAO_P varchar2,
                                          IE_STATUS_P varchar2) is

begin

if (IE_ACAO_P = 'APROVE') then
  if (IE_PROFISSIONAL_P = 'E' and IE_STATUS_P = 'TS') then
    update aval_nutricao
    set ie_status = 'SN'
    where nr_sequencia = NR_SEQUENCIA_P;
  end if;

  if (IE_PROFISSIONAL_P = 'M') then
    if (IE_STATUS_P = 'PF') then
      update aval_nutricao
      set ie_status = 'C'
      where nr_sequencia = NR_SEQUENCIA_P;
    else
      update aval_nutricao
      set ie_status = 'SD'
      where nr_sequencia = NR_SEQUENCIA_P;
    end if;
  end if;

  if (IE_PROFISSIONAL_P = 'N') then
    if (IE_STATUS_P = 'SD') then 
      update aval_nutricao
      set ie_status = 'C'
      where nr_sequencia = NR_SEQUENCIA_P;
    else
      update aval_nutricao
      set ie_status = 'PF'
      where nr_sequencia = NR_SEQUENCIA_P;
    end if;
  end if;
end if;

if (IE_ACAO_P = 'WITHDRAW' and IE_STATUS_P <> 'I') then
    update aval_nutricao
    set ie_status = 'W',
	dt_liberacao = sysdate
    where nr_sequencia = NR_SEQUENCIA_P;
end if;

if (IE_ACAO_P = 'SUBMIT' and IE_STATUS_P = 'C') then
    update aval_nutricao
    set ie_status = 'I',
	dt_liberacao = sysdate
    where nr_sequencia = NR_SEQUENCIA_P;
end if;

commit;
end NUTRITIONAL_ASSESSMENT_STATUS;
/