create or replace
procedure gerar_ciclo_inventario(
			nr_seq_regra_p	number,
			dt_inicial_ciclo_p	date,
			qt_ciclos_p	number,
			qt_dias_ciclos_p	number,
			nm_usuario_p	varchar2) is

/*variaveis*/
i		number(13);
qt_ciclos_w	number(13);
qt_ciclos_gerar_w	number(13);
dt_inicial_ciclo_w	date;
dt_final_ciclo_w	date;

begin
select	count(*)
into	qt_ciclos_w
from	sup_inv_ciclico_ciclo
where	nr_seq_regra = nr_seq_regra_p
and	trunc(dt_inicial_ciclo_p) between trunc(dt_inicio) and trunc(dt_fim);

if	(qt_ciclos_w = 0) then
	qt_ciclos_gerar_w	:= round(qt_ciclos_p,0);

	dt_inicial_ciclo_w	:= dt_inicial_ciclo_p;
	dt_final_ciclo_w	:= dt_inicial_ciclo_w + qt_dias_ciclos_p;

	for i in 1..qt_ciclos_gerar_w loop
		begin

		insert into sup_inv_ciclico_ciclo(
			nr_sequencia,
			dt_inicio,
			dt_fim,
			dt_atualizacao,
			nm_usuario,
			nr_seq_regra)
		values(	sup_inv_ciclico_ciclo_seq.nextval,
			dt_inicial_ciclo_w,
			dt_final_ciclo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_regra_p);
			
		dt_inicial_ciclo_w	:= dt_final_ciclo_w + 1;
		dt_final_ciclo_w	:= dt_inicial_ciclo_w + qt_dias_ciclos_p;

		end;
	end loop;

	commit;
end if;

end gerar_ciclo_inventario;
/