create or replace
procedure pls_desfazer_conf_envio_notif
			(	nr_seq_lote_p		number,
				nm_usuario_p		Varchar2) is 

nr_seq_pagador_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_notificacao_pagador
	where	nr_seq_lote	= nr_seq_lote_p;
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_pagador_w;
exit when C01%notfound;
	begin
	
	update	pls_notificacao_pagador
	set	ie_status	= ie_status_ant,
		ie_status_ant	= null,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_pagador_w;
	
	end;
end loop;
close C01;

update	pls_notificacao_lote
set	dt_envio 	= null,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_lote_p;

commit;

end pls_desfazer_conf_envio_notif;
/