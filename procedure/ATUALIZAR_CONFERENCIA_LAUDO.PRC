CREATE OR REPLACE PROCEDURE ATUALIZAR_CONFERENCIA_LAUDO(nr_laudo_p NUMBER)IS

nr_prescricao_w           NUMBER(14);
nr_sequencia_prescricao_w NUMBER(6);

BEGIN

  SELECT nr_prescricao,
         nr_seq_prescricao
  INTO   nr_prescricao_w,
         nr_sequencia_prescricao_w
  FROM   laudo_paciente
  WHERE  nr_sequencia = nr_laudo_p;                
                
  UPDATE prescr_procedimento a
  SET    a.ie_status_execucao = '37',
         a.nm_usuario = obter_usuario_ativo
  WHERE  a.nr_seq_interno IN (SELECT nr_seq_interno
                              FROM   prescr_procedimento b,
                                     procedimento_paciente c
                              WHERE  b.nr_sequencia  = c.nr_sequencia_prescricao
                              AND    b.nr_prescricao = c.nr_prescricao
                              AND    c.nr_laudo      = nr_laudo_p);

  gravar_auditoria_mmed(nr_prescricao_w,
                        nr_sequencia_prescricao_w,
                        obter_usuario_ativo,
                        36,
                        NULL);
   
  COMMIT;
  
END ATUALIZAR_CONFERENCIA_LAUDO;
/