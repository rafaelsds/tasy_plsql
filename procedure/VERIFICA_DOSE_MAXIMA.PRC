create or replace procedure Verifica_dose_maxima(	nr_prescricao_p			number,
								nr_sequencia_p			number,
								ie_utiliza_horarios_p	varchar2,
								ds_erro_p				out	varchar2,
								ds_erro2_p				out	varchar2,
								dt_aprazamento_p		date default null,
								ie_tipo_erro_p			out	varchar2,
								qt_dose_limite_p		out number,
								cd_unid_med_lim_p		out varchar2) is

qt_max_prescricao_w			number(18,6);
cd_unidade_medida_consumo_w	varchar2(30);
cd_unid_med_limite_w		varchar2(30);
cd_unid_med_limite_mensagem_w		varchar2(30);
qt_limite_pessoa_w			number(18,6);
qt_limite_pessoa_ww	number(18,6);
qt_conversao_dose_w			number(18,6);
qt_conversao_dose_limite_w	number(18,6);
qt_dose_w					number(18,6);
qt_dose_ww					number(18,6);
qt_dose_limite_w			number(18,6);
qt_dose_limite_mensagem_w			number(18,6);
qt_dose_npt_ped_w			number(18,6);
nr_ocorrencia_npt_ped_w		number(18,6);
qt_volume_npt_ped_w			number(18,6);
qt_dose_npt_ad_w			number(18,6);
nr_ocorrencia_npt_ad_w		number(18,6);
qt_volume_npt_ad_w			number(18,6);
qt_dose_npt_prot_w			number(18,6);
nr_ocorrencia_npt_prot_w	number(18,6);
qt_volume_npt_prot_w		number(18,6);
cd_unidade_medida_dose_w	varchar2(30);
ds_observacao_w				varchar2(255);
ds_mensagem_regra_w			varchar2(255);
cd_pessoa_fisica_w			varchar2(30);
cd_material_w				number(6);
ie_dose_limite_w			varchar2(15);
nr_ocorrencia_w				number(18,6);
nr_ocorrencia_ww			number(18,6);
ie_via_aplicacao_w			varchar2(5);
nr_seq_agrupamento_w		number(10,0);
ie_justificativa_w			varchar2(5);
ds_justificativa_w			varchar2(2000);
cd_prescritor_w				varchar2(50);
cd_setor_atendimento_w		number(5,0);
qt_regra_w					number(10,0);
qt_idade_w					number(10,0);
qt_idade_dia_w				number(18,6);
qt_solucao_w				number(18,6);
qt_ml_componente_w			number(18,6);
qt_total_w					number(18,6);
qt_dose_www					number(18,6);
qt_idade_mes_w				number(18,6);
qt_peso_w					number(18,6);
qt_limite_peso_w			number(18,6);
nr_horas_validade_w			number(5,0);
cd_estabelecimento_w		number(10,0);
ie_somar_dose_medic_w		varchar2(5);
nr_atendimento_w			number(15);
ie_agrupador_w				prescr_material.ie_agrupador%type;
nr_ocorrencia_sol_w			number(18,6);
qt_dose_sol_w				number(18,6);
qt_solucao_sol_w			number(18,6);
qt_solucao_mat_w			number(18,6);
ie_consistiu_dose_w			varchar2(50) := 'S';
ie_acm_w					varchar2(1);
ie_se_necessario_w			varchar2(1);
qt_dose_retorno_w			number(18,6);
nr_seq_solucao_w			prescr_solucao.nr_seq_solucao%type;
qt_hora_aplic_w			number(10);
qt_min_aplic_w				prescr_material.qt_min_aplicacao%type;
qt_tempo_aplicacao_w		prescr_solucao.qt_tempo_aplicacao%type;
qt_hora_infusao_w			prescr_solucao.qt_hora_infusao%type;
qt_tempo_infusao_w		prescr_solucao.qt_tempo_infusao%type;	
qt_hora_fase_w				prescr_solucao.qt_hora_fase%type;
nr_etapas_w					prescr_solucao.nr_etapas%type;	
qt_dif_tempo_etapa_w		number(10);
qt_tempo_item_w			number(10);
cd_doenca_cid_w				material_prescr.cd_doenca_cid%type;
nr_seq_mat_cpoe_w			prescr_Material.nr_seq_mat_cpoe%type;
ie_param_170_f_w			varchar2(2);
hr_dose_especial_w			prescr_material.hr_dose_especial%type;
qt_dose_especial_w			prescr_material.qt_dose_especial%type;
ie_objetivo_w				prescr_material.ie_objetivo%type;
qt_dose_limite_dia_cpoe_w		number(18,6);
ds_erro2_w					varchar2(2000) := null;
qt_dose_range_min_w			cpoe_material.qt_dose_range_min%type;

cursor c01 is
	select	nvl(qt_limite_pessoa,0),
			nvl(ie_dose_limite,'DOSE'),
			cd_unid_med_limite,
			nvl(ie_justificativa,'S'),
			ds_observacao,
			ds_mensagem_regra,
			cd_doenca_cid
	from	material_prescr
	where	cd_material = cd_material_w
	and		nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
	and		nvl(ie_via_aplicacao, nvl(ie_via_aplicacao_w,0)) = nvl(ie_via_aplicacao_w,0)
	and		qt_limite_pessoa is not null
	and		qt_idade_w between nvl(qt_idade_min,0) and nvl(qt_idade_max,999)
	and		qt_idade_dia_w between nvl(qt_idade_min_dia,0) and nvl(qt_idade_max_dia,55000)
	and		qt_idade_mes_w between nvl(qt_idade_min_mes,0) and nvl(qt_idade_max_mes,55000)
	and		((nr_seq_agrupamento is null) or (nr_seq_agrupamento = nr_seq_agrupamento_w))
	and		qt_peso_w between nvl(qt_peso_min,0) and nvl(qt_peso_max,999)
	and		cd_protocolo is null
	and		ie_tipo = '2'
	and 	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
	and		Obter_se_setor_regra_prescr(nr_sequencia, cd_setor_atendimento_w) = 'S'
	and		((cd_especialidade is null) or (obter_se_especialidade_medico(cd_prescritor_w, cd_especialidade) = 'S'))
	and		((cd_doenca_cid is null) or (obter_se_cid_atendimento(nr_atendimento_w,cd_doenca_cid) = 'S'))
	and		(((nvl(ie_tipo_item,'TOD') = 'SOL') and (ie_agrupador_w = 4)) or
			 ((nvl(ie_tipo_item,'TOD') = 'OUT') and (ie_agrupador_w <> 4)) or
			 ((nvl(ie_tipo_item,'TOD') = 'TOD')))    
	and     nvl(ie_objetivo, nvl(ie_objetivo_w, 'N')) = nvl(ie_objetivo_w,'N')
	order by    cd_especialidade,
				nr_sequencia;

begin

select	nvl(max(ie_somar_dose_medic),'N')
into	ie_somar_dose_medic_w
from	parametro_medico
where	cd_estabelecimento = Wheb_usuario_pck.get_cd_estabelecimento;

ds_erro_p	:= '';
ds_erro2_p	:= '';

select	max(cd_material),
		max(cd_unidade_medida_dose),
		nvl(max(qt_dose),0),
		max(nr_ocorrencia),
		max(ie_via_aplicacao),
		max(ds_justificativa),
		nvl(max(qt_solucao),0),
		max(qt_solucao),
		nvl(max(qt_dose),0),
		max(ie_agrupador),
		nvl(max(nr_sequencia_solucao),0),
		nvl(max(qt_hora_aplicacao),0),
		nvl(max(qt_min_aplicacao),0),
		nvl(max(ie_acm),'N'),
		nvl(max(ie_se_necessario),'N'),
		nvl(max(nr_seq_mat_cpoe),0),
		nvl(max(hr_dose_especial),''),
		nvl(max(qt_dose_especial),0),
		nvl(max(ie_objetivo),'N')
into	cd_material_w,
		cd_unidade_medida_dose_w,
		qt_dose_ww,
		nr_ocorrencia_w,
		ie_via_aplicacao_w,
		ds_justificativa_w,
		qt_solucao_w,
		qt_ml_componente_w,
		qt_dose_www,
		ie_agrupador_w,
		nr_seq_solucao_w,
		qt_hora_aplic_w,
		qt_min_aplic_w,
		ie_acm_w,
		ie_se_necessario_w,
		nr_seq_mat_cpoe_w,
		hr_dose_especial_w,
		qt_dose_especial_w,
		ie_objetivo_w
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and		nr_sequencia	= nr_sequencia_p;

Wheb_assist_pck.set_informacoes_usuario( Wheb_usuario_pck.get_cd_estabelecimento, obter_perfil_ativo, obter_usuario_ativo);
ie_param_170_f_w := Wheb_assist_pck.obterParametroFuncao(7010,170);

if	(ie_somar_dose_medic_w = 'S') then
	-- Realizar a soma dos componentes de solucao
	select	nvl(sum(obter_dose_convertida(cd_material, qt_dose, cd_unidade_medida_dose, cd_unidade_medida_dose_w)),0),
			nvl(sum(nr_ocorrencia),0),
			nvl(sum(qt_solucao),0)
	into	qt_dose_sol_w,
			nr_ocorrencia_sol_w,
			qt_solucao_sol_w
	from	prescr_material
	where	ie_agrupador = 4
	and		cd_material		= cd_material_w
	and		nr_prescricao	= nr_prescricao_p;
	
	--- NPT Adulta Antiga
	select	nvl(sum(obter_dose_convertida(c.cd_material, nvl(c.qt_dose,c.qt_volume), nvl(c.cd_unidade_medida, lower(obter_unid_med_usua('ml'))), cd_unidade_medida_dose_w)),0),
			sum(1),
			nvl(sum(c.qt_volume),0)
	into	qt_dose_npt_ad_w,
			nr_ocorrencia_npt_ad_w,
			qt_volume_npt_ad_w
	from	nut_paciente a,
			nut_paciente_elemento b,
			nut_pac_elem_mat c
	where	a.nr_sequencia = b.nr_seq_nut_pac
	and		b.nr_sequencia = c.nr_seq_nut_pac_ele
	and		a.nr_prescricao = nr_prescricao_p
	and		cd_material = cd_material_w;

	--- NPT Adulta Protocolo
	select	nvl(sum(obter_dose_convertida(c.cd_material, nvl(c.qt_dose,c.qt_volume), nvl(c.cd_unidade_medida, lower(obter_unid_med_usua('ml'))), cd_unidade_medida_dose_w)),0),
			sum(1),
			nvl(sum(c.qt_volume),0)
	into	qt_dose_npt_prot_w,
			nr_ocorrencia_npt_prot_w,
			qt_volume_npt_prot_w
	from	nut_pac a,
			nut_pac_elem_mat c
	where	a.nr_sequencia = c.nr_seq_nut_pac
	and		nvl(a.ie_npt_adulta,'S') = 'S'
	and		a.nr_prescricao = nr_prescricao_p
	and		c.cd_material = cd_material_w;

	--- NPT Pediatrica e Neonatal
	select	nvl(sum(obter_dose_convertida(d.cd_material, nvl(c.qt_dose,c.qt_volume), nvl(c.cd_unidade_medida, lower(obter_unid_med_usua('ml'))), cd_unidade_medida_dose_w)),0),
			sum(1),
			nvl(sum(c.qt_volume),0)
	into	qt_dose_npt_ped_w,
			nr_ocorrencia_npt_ped_w,
			qt_volume_npt_ped_w
	from	nut_pac a,
			nut_pac_elemento b,
			nut_pac_elem_mat c,
			nut_elem_material d
	where	a.nr_sequencia = b.nr_seq_nut_pac
	and		b.nr_sequencia = c.nr_seq_pac_elem
	and		b.nr_seq_elemento = d.nr_seq_elemento
	and		d.nr_sequencia = c.nr_seq_elem_mat
	and		a.nr_prescricao = nr_prescricao_p
	and		d.cd_material = cd_material_w;
	
	qt_dose_sol_w := (qt_dose_sol_w + qt_dose_npt_ped_w + qt_dose_npt_prot_w + qt_dose_npt_ad_w);
	nr_ocorrencia_sol_w := (nr_ocorrencia_sol_w + nr_ocorrencia_npt_ped_w + nr_ocorrencia_npt_prot_w + nr_ocorrencia_npt_ad_w);
	qt_solucao_sol_w := (qt_solucao_sol_w + qt_volume_npt_ped_w + qt_volume_npt_prot_w + qt_volume_npt_ad_w);
		
	if	(ie_agrupador_w = 1) then	
	--	 Realizar a soma dos medicamentos (incluindo o item principal)	
		select	nvl(sum(obter_dose_convertida(cd_material, qt_dose, cd_unidade_medida_dose, cd_unidade_medida_dose_w)),0),
				nvl(sum(nr_ocorrencia),0),
				nvl(sum(qt_solucao),0)
		into	qt_dose_ww,
				nr_ocorrencia_w,
				qt_solucao_w
		from	prescr_material
		where	ie_agrupador = 1
		and		cd_material		= cd_material_w
		and		nr_prescricao	= nr_prescricao_p;
	end if;
end if;

select	max(cd_prescritor),
		nvl(max(nr_horas_validade),24),
		max(cd_estabelecimento),
		max(cd_setor_atendimento),
		max(nr_atendimento)
into	cd_prescritor_w,
		nr_horas_validade_w,
		cd_estabelecimento_w,
		cd_setor_atendimento_w,
		nr_atendimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	max(nr_seq_agrupamento)
into	nr_seq_agrupamento_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_w;

begin
if	(nvl(cd_material_w,0) > 0) and
	(cd_unidade_medida_dose_w is not null) and
	((nvl(qt_dose_sol_w,0) + nvl(qt_dose_ww,0)) > 0) then
	begin
	
	-- Informacoes da prescricao
	select	max(cd_setor_atendimento),
			nvl(max(to_number(obter_idade_pf(cd_pessoa_fisica, sysdate, 'A'))),0),
			nvl(max(qt_peso),0),
			max(cd_pessoa_fisica)
	into	cd_setor_atendimento_w,
			qt_idade_w,
			qt_peso_w,
			cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao	= nr_prescricao_p;
	
	-- Informacoes do paciente
	select	max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'DIA')),
			max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'M'))
	into	qt_idade_dia_w,
			qt_idade_mes_w
	from	pessoa_fisica b
	where	b.cd_pessoa_fisica	= cd_pessoa_fisica_w;
	
	-- Informacoes do material
	select	nvl(max(qt_max_prescricao),0),
			max(cd_unidade_medida_consumo),
			nvl(max(cd_unid_med_limite), cd_unidade_medida_dose_w),
			nvl(max(qt_limite_pessoa),0),
	  nvl(max(ie_dose_limite),'DOSE')
	into	qt_max_prescricao_w,
			cd_unidade_medida_consumo_w,
			cd_unid_med_limite_w,
			qt_limite_pessoa_w,
			ie_dose_limite_w
	from	material
	where	cd_material	= cd_material_w;

	select	count(*)
	into	qt_regra_w
	from	material
	where	rownum = 1
	and		qt_limite_pessoa is not null
	and		cd_material	= cd_material_w;

	if	(nr_horas_validade_w	> 24) then
		nr_ocorrencia_ww	:= trunc(dividir((nr_ocorrencia_w * 24), nr_horas_validade_w));
	else
		nr_ocorrencia_ww	:= nr_ocorrencia_w;
	end if;
	
	
	if	(nr_seq_mat_cpoe_w is not null) and
		(nr_seq_mat_cpoe_w > 0)then
		
		select	max(obter_conversao_unid_med_cons(cd_material,cd_unidade_medida,qt_dose_maxima)),
			max(qt_dose_maxima),
			max(cd_unidade_medida),
			max(qt_dose_range_min)
		into	qt_dose_limite_dia_cpoe_w,
			qt_dose_limite_mensagem_w,
			cd_unid_med_limite_mensagem_w,
			qt_dose_range_min_w
		from	cpoe_material
		where	nr_sequencia = nr_seq_mat_cpoe_w;
	end if;

	
	if	(qt_regra_w > 0) or
		(qt_dose_limite_dia_cpoe_w is not null) then
		begin
		
		if	(qt_dose_limite_dia_cpoe_w is not null) then
			qt_limite_pessoa_w	:= qt_dose_limite_mensagem_w;
			ie_dose_limite_w	:= 'CPOEDIA';
			if (nvl(qt_dose_range_min_w, 0) > 0) then
				qt_dose_ww	:= qt_dose_range_min_w;
			end if;
		end if;
		
		if	(cd_unidade_medida_consumo_w = cd_unidade_medida_dose_w) then
			qt_conversao_dose_w	:= 1;
		else
			begin
			select	nvl(max(qt_conversao),1)
			into	qt_conversao_dose_w
			from	material_conversao_unidade
			where	cd_material		= cd_material_w
			and	cd_unidade_medida	= cd_unidade_medida_dose_w;
			exception
				when others then
				qt_conversao_dose_w	:= 1;
			end;
		end if;
		if	(nvl(qt_solucao_w,0) > 0) then
			if	(ie_agrupador_w = 4) then
				select	nvl(qt_hora_infusao,0),
							nvl(qt_tempo_infusao,0),
							nvl(qt_tempo_aplicacao,0),
							nvl(nr_etapas,0)
				into		qt_hora_infusao_w,
							qt_tempo_infusao_w,
							qt_tempo_aplicacao_w,
							nr_etapas_w
				from		prescr_solucao
				where		nr_prescricao = nr_prescricao_p
				and		nr_seq_solucao = nr_seq_solucao_w;
				qt_total_w		:= qt_ml_componente_w;
			else
				qt_total_w	:= nvl(obter_volume_ml_medic_dil(nr_prescricao_p, nr_sequencia_p),qt_dose_ww);
			end if;
			
			if	(qt_total_w > 0) then
				qt_dose_ww	:= dividir((qt_dose_ww * qt_solucao_w), qt_total_w);
			end if;
			qt_dose_w	:= dividir(trunc(dividir(qt_dose_ww * 1000, qt_conversao_dose_w)), 1000);
		else
			qt_dose_w	:= dividir(trunc(dividir(qt_dose_ww * 1000, qt_conversao_dose_w)), 1000);
		end if;

		if	(qt_max_prescricao_w > 0) and
			(qt_max_prescricao_w < nvl(qt_dose_w,0)) then
			-- A dose unitaria e maior que a usual prevista #@QT_MAX_PRESCRICAO_P#@ #@CD_UNIDADE_MEDIDA_CONSUMO_P#@
			ds_erro_p	:= wheb_mensagem_pck.get_texto(277286, 'QT_MAX_PRESCRICAO_P=' || qt_max_prescricao_w || ';CD_UNIDADE_MEDIDA_CONSUMO_P=' || cd_unidade_medida_consumo_w);
			qt_dose_limite_p := qt_max_prescricao_w;
			cd_unid_med_lim_p := cd_unidade_medida_consumo_w;
		end if;

		if	(cd_unidade_medida_consumo_w = cd_unid_med_limite_w) then
			qt_conversao_dose_limite_w	:= 1;
		else
			begin
			select	nvl(max(qt_conversao),1)
			into	qt_conversao_dose_limite_w
			from	material_conversao_unidade
			where	cd_material		= cd_material_w
			and		cd_unidade_medida	= cd_unid_med_limite_w;
			exception
				when others then
					qt_conversao_dose_limite_w	:= 1;
			end;
		end if;

		qt_dose_w			:= dividir(trunc(dividir(qt_dose_ww * 1000, qt_conversao_dose_w)), 1000);
		qt_dose_limite_w	:= dividir(trunc(dividir(qt_limite_pessoa_w * 1000, qt_conversao_dose_limite_w)), 1000);

		if	(ie_dose_limite_w = 'DIA') then
			begin
			if	(nr_horas_validade_w	> 24) then
				nr_ocorrencia_w	:= trunc(dividir((nr_ocorrencia_w * 24), nr_horas_validade_w));
			end if;
			qt_dose_w	:= qt_dose_w * nvl(nr_ocorrencia_w,1);
			qt_dose_w	:= qt_dose_w + obter_dose_medic_dia(nr_prescricao_p,nr_sequencia_p,ie_utiliza_horarios_p,cd_material_w);
			end;
		elsif	(ie_dose_limite_w = 'AT') then
			begin
			qt_dose_w	:= qt_dose_w * nvl(nr_ocorrencia_w,1);
			qt_dose_w	:= qt_dose_w + obter_dose_medic_atend_dia(nr_prescricao_p,nr_sequencia_p,qt_dose_w,ie_utiliza_horarios_p,cd_material_w);
			end;
		elsif	(ie_dose_limite_w = 'CPOEDIA') then
			begin
			qt_dose_w	:= qt_dose_w + obter_dose_medic_periodo(nr_atendimento_w,cd_material_w,dt_aprazamento_p-1,dt_aprazamento_p);
			ie_dose_limite_w	:= 'DIA';
			ds_erro2_w	:= obter_desc_expressao(970215);
			end;
		elsif	(qt_peso_w > 0) and
			(ie_dose_limite_w = 'KG/DIA') then
			begin
			-- Por kg
					
			if	(nr_horas_validade_w > 24) then
				nr_ocorrencia_w := trunc(dividir(nr_ocorrencia_w * 24, nr_horas_validade_w));
			end if;
					
			qt_dose_w	:= dividir(qt_dose_w, nvl(qt_peso_w,1));

			qt_dose_w	:= qt_dose_w * nvl(nr_ocorrencia_w,1);
			qt_dose_w	:= qt_dose_w + obter_dose_medic_dia(nr_prescricao_p,nr_sequencia_p,ie_utiliza_horarios_p,cd_material_w);
			
			-- Por dia				
			-- Caso possua solucao prescrita, devera ser adicionada a conversao dos componentes a dose
			if	(qt_solucao_sol_w > 0) then
				if	(nr_horas_validade_w > 24) then
					nr_ocorrencia_sol_w	:= trunc(dividir((nr_ocorrencia_sol_w * 24), nr_horas_validade_w));
					qt_solucao_sol_w	:= qt_solucao_sol_w * nr_ocorrencia_sol_w;
				end if;
				
				qt_dose_w	:= qt_dose_w + obter_dose_convertida(cd_material_w, qt_solucao_sol_w, 'ml', cd_unidade_medida_consumo_w);
			end if;
			end;
		elsif	(qt_peso_w > 0) and
			(ie_dose_limite_w = 'KG/D') then
			begin
			qt_dose_w	:= dividir(qt_dose_w, nvl(qt_peso_w,1));/*por KG*/

			/*por dose*/
			/*if	(nr_horas_validade_w	> 24) then
				nr_ocorrencia_w	:= trunc(((nr_ocorrencia_w * 24) / nr_horas_validade_w));
			end if;*/
			--qt_dose_w	:= qt_dose_w * nvl(nr_ocorrencia_w,1);
			qt_dose_w	:= qt_dose_w + obter_dose_medic_dia(nr_prescricao_p,nr_sequencia_p,ie_utiliza_horarios_p,cd_material_w);
			end;
		elsif	(ie_dose_limite_w = 'KG/H') then 	
				if	(ie_agrupador_w = 4) then
					qt_dif_tempo_etapa_w := round(qt_tempo_aplicacao_w / nr_etapas_w);
					if	(qt_hora_infusao_w = 0) and
						(qt_tempo_infusao_w = 0) and
						(qt_dif_tempo_etapa_w = qt_hora_fase_w) and
						(qt_tempo_aplicacao_w > 0) then
						qt_tempo_item_w := qt_tempo_aplicacao_w;
					elsif (qt_hora_infusao_w > 0) or
						(qt_tempo_infusao_w > 0) then
						qt_tempo_item_w := qt_hora_infusao_w + (qt_tempo_infusao_w / 60);
					else
						qt_tempo_item_w := qt_hora_fase_w;
					end if;
					qt_dose_w := qt_dose_w / (qt_peso_w * qt_tempo_item_w);
				elsif	((qt_hora_aplic_w > 0) or
					(qt_min_aplic_w > 0)) then
					qt_tempo_item_w := qt_hora_aplic_w + (qt_min_aplic_w/60);
					qt_dose_w := qt_dose_w / (qt_peso_w * qt_tempo_item_w);
				end if;
		elsif	(ie_dose_limite_w = 'KG/MIN') then 	
				if	(ie_agrupador_w = 4) then
					qt_dif_tempo_etapa_w := round(qt_tempo_aplicacao_w / nr_etapas_w);
					if	(qt_hora_infusao_w = 0) and
						(qt_tempo_infusao_w = 0) and
						(qt_dif_tempo_etapa_w = qt_hora_fase_w) and
						(qt_tempo_aplicacao_w > 0) then
						qt_tempo_item_w := qt_tempo_aplicacao_w * 60;
					elsif (qt_hora_infusao_w > 0) or
						(qt_tempo_infusao_w > 0) then
						qt_tempo_item_w := (qt_hora_infusao_w * 60) + qt_tempo_infusao_w;
					else
						qt_tempo_item_w  := qt_hora_fase_w * 60;
					end if;
					qt_dose_w := qt_dose_w / (qt_peso_w * qt_tempo_item_w);
				elsif	((qt_hora_aplic_w > 0) or
					(qt_min_aplic_w > 0)) then
					qt_tempo_item_w := (qt_hora_aplic_w * 60) + qt_min_aplic_w;
					qt_dose_w := qt_dose_w / (qt_peso_w * qt_tempo_item_w);
				end if;	
		end if;
		
		if	(qt_dose_limite_w > 0) and
			(qt_dose_limite_w < nvl(qt_dose_w,0)) then
			-- A dose unitaria e maior que a dose limite #@QT_LIMITE_PESSOA_P#@ #@CD_UNID_MED_LIMITE_P#@ por #@DS_DOSE_LIMITE_P#@
			if (ds_erro2_w is not null) then
				ds_erro2_p	:= ds_erro2_w;
			else
				ds_erro2_p	:= wheb_mensagem_pck.get_texto(277256, 'QT_LIMITE_PESSOA_P=' || nvl(qt_dose_limite_mensagem_w,qt_limite_pessoa_w) || ';CD_UNID_MED_LIMITE_P=' || nvl(cd_unid_med_limite_mensagem_w,cd_unid_med_limite_w) || ';DS_DOSE_LIMITE_P=' || obter_valor_dominio(1851,ie_dose_limite_w));
			end if;
			qt_dose_limite_p := qt_limite_pessoa_w;
			cd_unid_med_lim_p := cd_unid_med_limite_w;
		end if;
		end;
	else
		begin
		-- Verifica se tem alguma regra para os dados informados 
		open c01;
		loop
		fetch c01 into
			qt_limite_pessoa_w,
			ie_dose_limite_w,
			cd_unid_med_limite_w,
			ie_justificativa_w,
			ds_observacao_w,
			ds_mensagem_regra_w,
			cd_doenca_cid_w;
		exit when c01%notfound;
			begin
			
			if	(qt_limite_pessoa_w is null) then
				qt_limite_pessoa_ww := 0;
			else
				qt_limite_pessoa_ww := qt_limite_pessoa_w;
			end if;
			
			ds_erro2_p :=  null;
			 
			if	(cd_unidade_medida_consumo_w = cd_unidade_medida_dose_w) then
				qt_conversao_dose_w	:= 1;
			else
				begin
				select	nvl(max(qt_conversao),1)
				into	qt_conversao_dose_w
				from	material_conversao_unidade
				where	cd_material		= cd_material_w
				and		cd_unidade_medida	= cd_unidade_medida_dose_w;
				exception
					when others then
						qt_conversao_dose_w	:= 1;
				end;
			end if;

			if	(qt_max_prescricao_w > 0) and
				(qt_max_prescricao_w < nvl(qt_dose_w,0)) then
				-- A dose unitaria e maior que a usual prevista #@QT_MAX_PRESCRICAO_P#@ #@CD_UNIDADE_MEDIDA_CONSUMO_P#@
				-- #@DS_OBSERVACAO_P#@
				ds_erro_p	:= substr(wheb_mensagem_pck.get_texto(278059, 'QT_MAX_PRESCRICAO_P=' || qt_max_prescricao_w ||';CD_UNIDADE_MEDIDA_CONSUMO_P=' || cd_unidade_medida_consumo_w ||';DS_OBSERVACAO_P=' || nvl(ds_observacao_w, ' ')),1,255);
				qt_dose_limite_p := qt_max_prescricao_w;
				cd_unid_med_lim_p := cd_unidade_medida_consumo_w;
				if	(ds_mensagem_regra_w is not null) then
					ds_erro_p := ds_mensagem_regra_w;
				end if;
			end if;

			if	(cd_unidade_medida_consumo_w = cd_unid_med_limite_w) then
				qt_conversao_dose_limite_w	:= 1;
			else
				begin
				select	nvl(max(qt_conversao),1)
				into	qt_conversao_dose_limite_w
				from	material_conversao_unidade
				where	cd_material		= cd_material_w
				and		cd_unidade_medida	= cd_unid_med_limite_w;
				exception
					when others then
						qt_conversao_dose_limite_w	:= 1;
				end;
			end if;
			
			if	(nvl(qt_solucao_w,0) > 0) then
				if	(ie_agrupador_w = 4) then
				select	nvl(qt_hora_infusao,0),
							nvl(qt_tempo_infusao,0),
							nvl(qt_tempo_aplicacao,0),
							nvl(nr_etapas,0)
				into		qt_hora_infusao_w,
							qt_tempo_infusao_w,
							qt_tempo_aplicacao_w,
							nr_etapas_w
				from		prescr_solucao
				where		nr_prescricao = nr_prescricao_p
				and		nr_seq_solucao = nr_seq_solucao_w;
				
					qt_total_w		:= qt_ml_componente_w;
				else
					qt_total_w	:= nvl(obter_volume_ml_medic_dil(nr_prescricao_p, nr_sequencia_p),qt_dose_www);
				end if;
			
				if	(qt_total_w > 0) then
					qt_dose_ww	:= dividir((qt_total_w * qt_conversao_dose_limite_w), qt_conversao_dose_w);
				end if;
				
				qt_dose_w	:=  dividir((qt_dose_ww * qt_dose_www), qt_total_w);
			else
				qt_dose_w	:= dividir(trunc(dividir(qt_dose_ww * 1000, qt_conversao_dose_limite_w)), 1000);
			end if;
			
			if	(nvl(qt_solucao_w,0) = 0) then
				qt_dose_w		:= dividir(trunc(dividir(qt_dose_ww * 1000, qt_conversao_dose_w)), 1000);
			else
				qt_dose_w		:= dividir(trunc(dividir(qt_dose_w * 1000, qt_conversao_dose_limite_w)), 1000);
			end if;
			
			if (hr_dose_especial_w is not null) and
			   (qt_dose_especial_w > 0) then
			
				if	(nvl(qt_solucao_w,0) = 0) then
					qt_dose_especial_w	:= dividir(trunc(dividir(qt_dose_especial_w * 1000, qt_conversao_dose_w)), 1000);
				else
					qt_dose_especial_w	:= dividir(trunc(dividir(qt_dose_especial_w * 1000, qt_conversao_dose_limite_w)), 1000);
				end if;
			end if;
			
			qt_dose_limite_w	:= dividir(trunc(dividir(qt_limite_pessoa_ww * 1000, qt_conversao_dose_limite_w)), 1000);
			
			if (ie_dose_limite_w = 'DOSE') and
				(qt_dose_especial_w > 0) and
				(round(qt_dose_especial_w,3) > round(qt_dose_w,3)) then

				qt_dose_w := qt_dose_especial_w;

			elsif	(ie_dose_limite_w = 'DIA') then

				if (ie_acm_w = 'N') and (ie_se_necessario_w = 'N') then
					
					if (nvl(qt_solucao_w,0) > 0) then
						ie_consistiu_dose_w	:= consiste_dose_periodo_ant(nr_atendimento_w, nr_prescricao_p, nr_sequencia_p, qt_dose_limite_w, 'S',null,null,null,null,null,null,'N');
						
						if	(substr(ie_consistiu_dose_w,1,1) = 'S') then
							ie_consistiu_dose_w := consiste_dose_periodo_dep(nr_atendimento_w, nr_prescricao_p, nr_sequencia_p, qt_dose_limite_w, 'S',null,null,null,null,null,null,'N');
						end if;
					else
					
						ie_consistiu_dose_w	:= consiste_dose_periodo_ant(nr_atendimento_w, nr_prescricao_p, nr_sequencia_p, qt_dose_limite_w, 'N',null,null,null,null,null,null,'N');
						
						if	(substr(ie_consistiu_dose_w,1,1) = 'S') then
							ie_consistiu_dose_w	:= consiste_dose_periodo_dep(nr_atendimento_w, nr_prescricao_p, nr_sequencia_p, qt_dose_limite_w, 'N',null,null,null,null,null,null,'N');
						end if;
					end if;
					
				elsif (dt_aprazamento_p is not null) then
					
					if (nvl(qt_solucao_w,0) = 0) then						
						ie_consistiu_dose_w	:= consiste_dose_periodo_ant(nr_atendimento_w, nr_prescricao_p, nr_sequencia_p, qt_dose_limite_w, 'N', dt_aprazamento_p,null,null,null,null,null,'N');
						
						if	(substr(ie_consistiu_dose_w,1,1) = 'S') then
							ie_consistiu_dose_w	:= consiste_dose_periodo_dep(nr_atendimento_w, nr_prescricao_p, nr_sequencia_p, qt_dose_limite_w, 'N', dt_aprazamento_p,null,null,null,null,null,'N');
						end if;
					end if;				
				
				end if;

			elsif	(ie_dose_limite_w = 'AT') then
				begin
				qt_dose_w	:= qt_dose_w * nvl(nr_ocorrencia_w,1);
				qt_dose_w	:= qt_dose_w + qt_dose_especial_w;
				qt_dose_w	:= qt_dose_w + obter_dose_medic_atend_dia(nr_prescricao_p,nr_sequencia_p,qt_dose_w,ie_utiliza_horarios_p,cd_material_w);
				end;
			elsif	(ie_dose_limite_w = 'PF') then

				qt_dose_w := qt_dose_w * nvl(nr_ocorrencia_w,1);
				qt_dose_w := qt_dose_w + qt_dose_especial_w;

				select	nvl(sum(obter_dose_convertida(a.cd_material, a.qt_dose, a.cd_unidade_medida, cd_unidade_medida_consumo_w)),0)
				into	qt_dose_retorno_w
				from	prescr_material a,
					prescr_medica b
				where	a.nr_prescricao	= b.nr_prescricao
				and	a.cd_material	= cd_material_w
				and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w
				and	a.nr_prescricao <> nr_prescricao_p;

				qt_dose_w := qt_dose_retorno_w + qt_dose_w;	
			elsif	(qt_peso_w > 0) then
				if (ie_dose_limite_w = 'KG') then
					qt_dose_w 	:= qt_dose_w + qt_dose_especial_w;
					qt_dose_w	:= dividir(qt_dose_w, nvl(qt_peso_w,1));
				elsif	(ie_dose_limite_w = 'KG/DIA') then
					begin
					/*por KG*/
					
					if	(nr_horas_validade_w > 24) then
						nr_ocorrencia_w := trunc(dividir(nr_ocorrencia_w * 24, nr_horas_validade_w));
					end if;
					
					qt_dose_w	:= dividir(qt_dose_w, nvl(qt_peso_w,1));
					qt_dose_w	:= qt_dose_w * nvl(nr_ocorrencia_w,1);
					
					if (hr_dose_especial_w is not null) and
					   (qt_dose_especial_w > 0) then
					   
						qt_dose_especial_w	:= dividir(qt_dose_especial_w, nvl(qt_peso_w,1));
						qt_dose_w			:= qt_dose_w + qt_dose_especial_w;
					end if;					
					qt_dose_w	:= qt_dose_w + obter_dose_medic_dia(nr_prescricao_p,nr_sequencia_p,ie_utiliza_horarios_p,cd_material_w);
					-- Por dia
					-- Caso possua solucao prescrita, devera ser adicionada a conversao dos componentes a dose
					if	(qt_solucao_sol_w > 0) then
						if	(nr_horas_validade_w > 24) then
							nr_ocorrencia_sol_w	:= trunc(dividir((nr_ocorrencia_sol_w * 24), nr_horas_validade_w));
							qt_solucao_sol_w	:= qt_solucao_sol_w * nr_ocorrencia_sol_w;
						end if;
						
						qt_dose_w	:= qt_dose_w + obter_dose_convertida(cd_material_w, qt_solucao_sol_w, 'ml', cd_unidade_medida_consumo_w);
					end if;
					end;
				elsif	(ie_dose_limite_w = 'KG/D') then
					qt_dose_w	:= qt_dose_w + qt_dose_especial_w;
					qt_dose_w	:= dividir(qt_dose_w, nvl(qt_peso_w,1));/*por KG*/
					qt_dose_w	:= qt_dose_w + obter_dose_medic_dia(nr_prescricao_p,nr_sequencia_p,ie_utiliza_horarios_p,cd_material_w);
				elsif	(ie_dose_limite_w = 'KG/H') then 	
						if	(ie_agrupador_w = 4) then
							qt_dif_tempo_etapa_w := round(qt_tempo_aplicacao_w / nr_etapas_w);
							if	(qt_hora_infusao_w = 0) and
								(qt_tempo_infusao_w = 0) and
								(qt_dif_tempo_etapa_w = qt_hora_fase_w) and
								(qt_tempo_aplicacao_w > 0) then
								qt_tempo_item_w := qt_tempo_aplicacao_w;
							elsif (qt_hora_infusao_w > 0) or
								(qt_tempo_infusao_w > 0) then
								qt_tempo_item_w := qt_hora_infusao_w + (qt_tempo_infusao_w / 60);
							else
								qt_tempo_item_w := qt_hora_fase_w;
							end if;
							qt_dose_w := qt_dose_w + qt_dose_especial_w;
							qt_dose_w := qt_dose_w / (qt_peso_w * qt_tempo_item_w);
						elsif	((qt_hora_aplic_w > 0) or
							(qt_min_aplic_w > 0)) then
							qt_tempo_item_w := qt_hora_aplic_w + (qt_min_aplic_w/60);
							qt_dose_w := qt_dose_w + qt_dose_especial_w;
							qt_dose_w := qt_dose_w / (qt_peso_w * qt_tempo_item_w);
						end if;
				elsif	(ie_dose_limite_w = 'KG/MIN') then 	
						if	(ie_agrupador_w = 4) then
							qt_dif_tempo_etapa_w := round(qt_tempo_aplicacao_w / nr_etapas_w);
							if	(qt_hora_infusao_w = 0) and
								(qt_tempo_infusao_w = 0) and
								(qt_dif_tempo_etapa_w = qt_hora_fase_w) and
								(qt_tempo_aplicacao_w > 0) then
								qt_tempo_item_w := qt_tempo_aplicacao_w * 60;
							elsif (qt_hora_infusao_w > 0) or
								(qt_tempo_infusao_w > 0) then
								qt_tempo_item_w := (qt_hora_infusao_w * 60) + qt_tempo_infusao_w;
							else
								qt_tempo_item_w  := qt_hora_fase_w * 60;
							end if;
							qt_dose_w := qt_dose_w + qt_dose_especial_w;
							qt_dose_w := qt_dose_w / (qt_peso_w * qt_tempo_item_w);
						elsif	((qt_hora_aplic_w > 0) or
							(qt_min_aplic_w > 0)) then
							qt_tempo_item_w := (qt_hora_aplic_w * 60) + qt_min_aplic_w;
							qt_dose_w := qt_dose_w + qt_dose_especial_w;
							qt_dose_w := qt_dose_w / (qt_peso_w * qt_tempo_item_w);
						end if;	
				end if;
			end if;
			
			if	((substr(ie_consistiu_dose_w,1,1) = 'N') or 			
				(((qt_dose_limite_w > 0) or
				  (qt_limite_pessoa_w = 0)) and
				(round(qt_dose_limite_w,3) < nvl(round(qt_dose_w,3),0)))) then
				
				-- A dose unitaria e maior que a dose limite #@QT_LIMITE_PESSOA_P#@ #@CD_UNID_MED_LIMITE_P#@ por #@IE_DOSE_LIMITE_P#@
				--  #@DS_OBSERVACAO_P#@
				if	(ie_dose_limite_w <> 'PF') then
					ds_erro2_p	:= substr(wheb_mensagem_pck.get_texto(278070, 'QT_LIMITE_PESSOA_P='||QT_LIMITE_PESSOA_WW||';CD_UNID_MED_LIMITE_P=' ||CD_UNID_MED_LIMITE_W||';IE_DOSE_LIMITE_P='||OBTER_VALOR_DOMINIO(1851 , IE_DOSE_LIMITE_W)||';DS_OBSERVACAO_P='||nvl(DS_OBSERVACAO_W, ' ')),1,255);
				else
					ds_erro2_p	:= substr(wheb_mensagem_pck.get_texto(278070, 'QT_LIMITE_PESSOA_P='||QT_LIMITE_PESSOA_WW||';CD_UNID_MED_LIMITE_P=' ||CD_UNID_MED_LIMITE_W||';IE_DOSE_LIMITE_P='||obter_desc_expressao(295829)||';DS_OBSERVACAO_P='||nvl(DS_OBSERVACAO_W, ' ')),1,255);
				end if;
				if	(ds_mensagem_regra_w is not null) then
					ds_erro2_p := ds_mensagem_regra_w;
				end if;
				qt_dose_limite_p := qt_limite_pessoa_ww;
				cd_unid_med_lim_p := cd_unid_med_limite_w;
				if	(cd_doenca_cid_w is not null) then
					ds_erro2_p:= substr(ds_erro2_p||wheb_mensagem_pck.get_texto(495114,'DS_CID_DOENCA='||OBTER_DESC_CID(cd_doenca_cid_w)),1,255);
				end if;

				 
				if ((ie_justificativa_w = 'S') or ((nr_seq_mat_cpoe_w > 0) and (ds_justificativa_w is not null))) then
					ie_tipo_erro_p := 'N';
				elsif ((ie_justificativa_w = 'N') and (ds_justificativa_w is null)) then	
					ie_tipo_erro_p := 'J';
				elsif	(ie_param_170_f_w = 'S') then
					ie_tipo_erro_p := 'A';
				end if;
					
			end if;

			end;
			if (ds_erro2_p is not null) then
				exit;
			end if;
		end loop;
		close c01;

		end;
	end if;
	end;
end if;

exception when others then
	null;
end;

end Verifica_dose_maxima;
/
