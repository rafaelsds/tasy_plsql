create or replace
procedure Inativar_item_declaracao_obito(nr_sequencia_p	Number,
				ds_justificativa_p	Varchar2,
				nm_usuario_p		Varchar2) is 

begin

update 	declaracao_obito
set 	dt_inativacao = sysdate,
	nm_usuario_inativacao = nm_usuario_p,
	ds_justificativa = ds_justificativa_p,
	ie_situacao = 'I'
where 	nr_sequencia = nr_sequencia_p;

commit;

end Inativar_item_declaracao_obito;
/