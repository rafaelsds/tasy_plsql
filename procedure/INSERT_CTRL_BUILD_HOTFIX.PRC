create or replace procedure insert_ctrl_build_hotfix(
  nr_seq_ctrl_desc_p in man_os_ctrl_desc.nr_sequencia%type,
  nr_service_order_p in man_ordem_servico.nr_sequencia%type,
  nr_seq_customer_p in com_cliente.nr_sequencia%type,
  cd_version_p in man_os_ctrl_build.cd_versao%type,
  nm_usuario_p in usuario.nm_usuario%type) is
  
cd_cnpj_w pessoa_juridica.cd_cgc%type;
ds_hotfix_w service_pack_hotfix.ds_hotfix%type;
cd_version_w service_pack_hotfix.cd_version%type;
  
begin
  
  select max(cd_cnpj)
  into cd_cnpj_w
  from com_cliente
  where nr_sequencia = nr_seq_customer_p;
  
  if (cd_cnpj_w is not null) then
      select max(a.ds_hotfix),
             max(a.cd_version)
      into ds_hotfix_w,
           cd_version_w
      from service_pack_hotfix a,
           service_order_sp_hotfix b
      where a.nr_sequencia = b.nr_service_pack_hotfix
      and b.cd_cnpj = cd_cnpj_w
      and b.nr_service_order = nr_service_order_p
	    and a.cd_version = cd_version_p;
      
      if (cd_version_w is not null and ds_hotfix_w is not null) then
      
        insert into man_os_ctrl_build (
          nr_sequencia,
          dt_atualizacao,
          nm_usuario,
          dt_atualizacao_nrec,
          nm_usuario_nrec,
          cd_versao,
          nr_seq_man_os_ctrl_desc,
          ie_situacao,
          ds_hotfix
        )
        values (
          man_os_ctrl_build_seq.nextval,
          sysdate,
          nm_usuario_p,
          sysdate,
          nm_usuario_p,
          cd_version_w,
          nr_seq_ctrl_desc_p,
          'A',
          ds_hotfix_w
        );
        
        commit;
      else
        wheb_mensagem_pck.exibir_mensagem_abort(1101219);
      end if;    
  else
    wheb_mensagem_pck.exibir_mensagem_abort(1101217);
  end if;

end;
/