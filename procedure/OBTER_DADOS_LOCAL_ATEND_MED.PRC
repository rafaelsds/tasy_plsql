create or replace
procedure obter_dados_local_atend_med(	nr_seq_local_atend_p	in	number,
					ds_local_p		out	varchar2,
					cd_municipio_ibge_p	out	varchar2,
					ds_endereco_p		out	varchar2,
					nr_endereco_p		out	number,
					ds_bairro_p		out	varchar2,
					cd_cep_p		out	varchar2,
					sg_estado_p		out	varchar2) is 

begin

if	(nr_seq_local_atend_p is not null) then
	select	max(a.ds_local),
		max(a.ds_endereco),
		max(a.cd_municipio_ibge),
		max(a.nr_endereco),
		max(a.ds_bairro),
		max(a.cd_cep),
		max(a.sg_estado)
	into	ds_local_p,
		ds_endereco_p,
		cd_municipio_ibge_p,
		nr_endereco_p,
		ds_bairro_p,
		cd_cep_p,
		sg_estado_p
	from	local_atendimento_medico a
	where	a.nr_sequencia	= nr_seq_local_atend_p;
end if;

end obter_dados_local_atend_med;
/