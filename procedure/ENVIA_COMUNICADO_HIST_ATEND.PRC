create or replace
procedure envia_comunicado_hist_atend(
			              nm_usuario_p		Varchar2,
				      nm_usuario_destino_p	varchar2,
				      nr_sequencia_p		number) is 

nr_atendimento_w	number(10);
nr_seq_tipo_historico_w number(10);
ds_observacao_w         varchar2(1900);
nm_pessoa_fisica_w	varchar2(60); 
ds_tipo_historico_w	varchar2(90);
ds_titulo_w		varchar2(80);
ds_comunicado_w		varchar2(2000);
nr_seq_classif_w	number(10);
				      
begin

if	(nvl(nr_sequencia_p,0) > 0) and 
        (nvl(nm_usuario_destino_p,null) is not null) then


	select	nr_atendimento,
		nr_seq_tipo_historico,
		substr(ds_observacao,1,1900)
	into	nr_atendimento_w,
		nr_seq_tipo_historico_w,
		ds_observacao_w
	from	atendimento_hist_paciente
	where	nr_sequencia = nr_sequencia_p;

	select	substr(obter_nome_pf(cd_pessoa_fisica),1,60)
	into	nm_pessoa_fisica_w
	from	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_w;
	
	select  obter_desc_tipo_hist_atend(nr_seq_tipo_historico_w)
	into	ds_tipo_historico_w
	from	dual;
	
	ds_titulo_w := wheb_mensagem_pck.get_texto(307419) || ' ' || lower(wheb_mensagem_pck.get_texto(307420)) || ' '|| nr_atendimento_w ||' - ' ||ds_tipo_historico_w; -- Hist�rico atendimento
	
	ds_comunicado_w := substr(wheb_mensagem_pck.get_texto(307421,	'NM_PESSOA_FISICA_W=' || nm_pessoa_fisica_w || ';' ||
																	'DS_TIPO_HISTORICO_W=' || ds_tipo_historico_w || ';' ||
																	'DT_ATUAL=' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || ';' ||
																	'DS_OBSERVACAO_W=' || ds_observacao_w),
								1,2000);
														/*
															Paciente: #@NM_PESSOA_FISICA_W#@
															Tipo de hist�rico: #@DS_TIPO_HISTORICO_W#@
															Data/hora: #@DT_ATUAL#@
															Observa��o: #@DS_OBSERVACAO_W#@
														*/
	select	max(obter_classif_comunic('F'))
	into	nr_seq_classif_w
	from	dual;

	Gerar_Comunic_Padrao(
		sysdate,
		ds_titulo_w,
		ds_comunicado_w,
		nm_usuario_p,
		'N',
		nm_usuario_destino_p,
		'N',
		nr_seq_classif_w,
		'','','',sysdate,'','');
	
end if;
commit;

end envia_comunicado_hist_atend;
/
