create or replace
procedure reverse_account_batch (
				nr_seq_protocolo_p in number,
				nr_protocolo_p in number,
				nm_usuario_p in varchar2,
				nr_interno_conta_p in number) is
				
begin
update 	conta_paciente  a
set	a.nr_seq_protocolo	= nr_seq_protocolo_p,
	a.nr_protocolo		= nr_protocolo_p,
	a.dt_atualizacao	= sysdate,
	a.nm_usuario		= nm_usuario_p 
where	a.nr_seq_conta_origem 	= nr_interno_conta_p
and	a.ie_cancelamento 	= 'E';
commit;

end reverse_account_batch;
/
