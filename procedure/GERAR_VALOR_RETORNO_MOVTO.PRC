create or replace
procedure Gerar_Valor_Retorno_Movto(nr_seq_retorno_p		number,
						nr_seq_ret_item_p		number,
						cd_categoria_p		varchar2,
						vl_pago_p		out	number,
						nm_usuario_p		varchar2) is

cd_convenio_w			number(5);
dt_ref_preco_w		date;
qt_pontos_w			preco_amb.qt_pontuacao%type;
nr_sequencia_w		number(10);
tp_proced_w			number(1);
cd_proced_w			number(15);
qt_paga_w			number(9,3);

vl_pago_w			number(15,2) := 0;
vl_coper_w			number(15,2);
vl_anest_w			number(15,2);
vl_medico_w			number(15,2);
vl_aux_w			number(15,2);
vl_mat_w			number(15,2);
vl_pto_proc_w			number(15,2);
vl_pto_coper_w		number(15,2);
vl_pto_anest_w		number(15,2);
vl_pto_med_w			number(15,2);
vl_pto_aux_w			number(15,2);
vl_pto_mat_w			number(15,2);
qt_porte_anest_w		number(15,2);
cd_edicao_amb_w		number(6);
cd_usuario_convenio_w	Varchar2(40);
cd_plano_w			Varchar2(20);
ie_clinica_w			Number(15,0);
cd_empresa_ref_w		Number(15,0);
ie_preco_informado_w		varchar2(01);
nr_seq_ajuste_proc_w		number(10,0);
nr_seq_origem_w			number(10,0);
cursor C01 is
	select nr_sequencia,
		 tp_item,
		 cd_item,
		 qt_paga,
		 nvl(vl_total_pago, (qt_paga * vl_pago)),
		 nvl(to_number(obter_dados_categ_conv(nr_atendimento,'OC')),0)
	from convenio_retorno_movto
	where nr_seq_ret_item = nr_seq_ret_item_p
	  and cd_item <> 0
	  and qt_paga > 0
	  and nvl(ie_situacao_item,'X') <> 'G'
	  and nvl(ie_gera_resumo,'S') = 'S';
begin

vl_pago_p := 0;

select cd_convenio,
	 dt_ref_preco
into	 cd_convenio_w,
	 dt_ref_preco_w
from 	convenio_retorno
where nr_sequencia = nr_seq_retorno_p;

open C01;
loop
	fetch C01 into	nr_sequencia_w,
				tp_proced_w,
				cd_proced_w,
				qt_paga_w,
				vl_pago_w,
				nr_seq_origem_w;
	exit when C01%notfound;

	if (tp_proced_w = 2) then
		define_preco_procedimento(	1,
							cd_convenio_w,
							cd_categoria_p,
							dt_ref_preco_w,
							cd_proced_w,
							0,
							0,
							0,
							null,
							null,
							null,
							null,
							null,
							cd_usuario_convenio_w, cd_plano_w, ie_clinica_w, cd_empresa_ref_w,null,
							vl_pago_w,
							vl_coper_w,
							vl_anest_w,
							vl_medico_w,
							vl_aux_w,
							vl_mat_w,
							vl_pto_proc_w,
							vl_pto_coper_w,
							vl_pto_anest_w,
							vl_pto_med_w,
							vl_pto_aux_w,
							vl_pto_mat_w,
							qt_porte_anest_w,
							qt_pontos_w,
							cd_edicao_amb_w, ie_preco_informado_w, nr_seq_ajuste_proc_w, 0, null, 0, null, null,
							NULL, NULL, null, null, null, null, null, null, null, null, null,null,nr_seq_origem_w, null,
							null);

		dbms_output.put_line(cd_convenio_w || 'X' ||cd_categoria_p || 'X' || (qt_paga_w * vl_pago_w));
	else
		qt_paga_w := 1;
	end if;

	update convenio_retorno_movto
	set vl_pago = vl_pago_w,
	    vl_total_pago = vl_pago_w * qt_paga_w	
	where nr_sequencia = nr_sequencia_w
	  and nm_usuario = nm_usuario_p;

	vl_pago_p := vl_pago_p + vl_pago_w;

end loop;
close C01;

commit;

end Gerar_Valor_Retorno_Movto;
/
