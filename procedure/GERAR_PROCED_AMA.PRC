create or replace 
procedure Gerar_Proced_AMA
		(nr_sequencia_p		in  number,
		cd_pessoa_fisica_p	in  varchar2,
		nm_usuario_p		in  varchar2,
		cd_perfil_p		in  number,
		cd_funcao_p		in  number) is

nr_prescricao_w		Number(14);
nr_sequencia_w		Number(6);
cd_funcionario_w		varchar2(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
cd_area_proc_w		number(15);
cd_especial_proc_w	number(15);
cd_grupo_proc_w		number(15);
qt_proc_medico_w		number(5)	:= 0;
qt_proc_medico2_w	number(5)	:= 0;
qt_proc_medico3_w	number(5)	:= 0;

Cursor C01 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_w;

Cursor C02 is
	select	cd_procedimento,
		ie_origem_proced
	from	proced_medico_ambulatorial
	where	nr_seq_ama = nr_sequencia_p;

begin

OPEN C02;
LOOP
FETCH C02 into
	cd_procedimento_w,
	ie_origem_proced_w;
exit when C02%notfound;
	/* Buscar dados do procedimento executado */
	begin
	select	cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc
	into	cd_area_proc_w,
		cd_especial_proc_w,
		cd_grupo_proc_w
	from	estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced		= ie_origem_proced_w;
	exception
		when others then
		cd_area_proc_w		:= null;
		cd_especial_proc_w	:= null;
		cd_grupo_proc_w 		:= null;
	end;

	select	count(*)
	into	qt_proc_medico_w
	from	sus_exclui_medico_ama
	where	(nvl(cd_procedimento,cd_procedimento_w)		= cd_procedimento_w	or cd_procedimento_w is null)
	and	(nvl(cd_area_procedimento,cd_area_proc_w)	= cd_area_proc_w 	or cd_area_proc_w is null)
	and	(nvl(cd_especialidade,cd_especial_proc_w) 	= cd_especial_proc_w 	or cd_especial_proc_w is null)
	and	(nvl(cd_grupo_proc,cd_grupo_proc_w)		= cd_grupo_proc_w 	or cd_grupo_proc_w is null)
	and	cd_pessoa_fisica is not null;
	
	if 	(qt_proc_medico_w > 0) then
		begin		
		select	count(*)
		into	qt_proc_medico2_w 
		from	sus_exclui_medico_ama
		where	(nvl(cd_procedimento,cd_procedimento_w)		= cd_procedimento_w		or cd_procedimento_w is null)
		and	(nvl(cd_area_procedimento,cd_area_proc_w)	= cd_area_proc_w 		or cd_area_proc_w is null)
		and	(nvl(cd_especialidade,cd_especial_proc_w) 	= cd_especial_proc_w 		or cd_especial_proc_w is null)
		and	(nvl(cd_grupo_proc,cd_grupo_proc_w)		= cd_grupo_proc_w 		or cd_grupo_proc_w is null)
		and	cd_pessoa_fisica = cd_pessoa_fisica_p;
		
		if 	(qt_proc_medico2_w = 0) then
			--r.aise_application_error(-20011,'Esta AMA possui procedimentos que s� podem ser executados pelos m�dicos autorizados. Cadastro geral/SUS/Cadastros/SUS regra exclui medico AMA.');
			wheb_mensagem_pck.exibir_mensagem_abort(263394);
		end if;
		end;
	end if;
END LOOP;
CLOSE C02;

Gerar_prescricao_AMA(nr_sequencia_p,cd_pessoa_fisica_p,nm_usuario_p,nr_prescricao_w);

OPEN C01;
LOOP
FETCH C01 into
	nr_sequencia_w,
	cd_procedimento_w,
	ie_origem_proced_w;
exit when c01%notfound;
	gerar_proc_pac_prescricao
		(nr_prescricao_w, 
		nr_sequencia_w,
		cd_perfil_p, 
		cd_funcao_p, 
		nm_usuario_p,
		null,
		null, null);

	/* Busca o respons�vel da AMA*/
	select	cd_medico_resp
	into	cd_funcionario_w
	from	atend_medico_ambulatorial
	where	nr_sequencia	= nr_sequencia_p;

	/* Caso n�o haja m�dico, atualiza o funcion�rio do procedimento com o respons�vel pela AMA*/
	update	procedimento_paciente
	set	cd_pessoa_fisica		= cd_funcionario_w
	where	nr_prescricao		= nr_prescricao_w
	and	nr_sequencia_prescricao	= nr_sequencia_w
	and	cd_medico_executor is null;

	/* Buscar dados do procedimento executado */
	begin
	select	cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc
	into	cd_area_proc_w,
		cd_especial_proc_w,
		cd_grupo_proc_w
	from	estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w;
	exception
		when others then
		cd_area_proc_w		:= null;
		cd_especial_proc_w	:= null;
		cd_grupo_proc_w 	:= null;
	end;

	/* verifica se pode haver medico para o procedimento AMA, caso n�o possa, seta nulo o campo medico executor*/
	select	count(*)
	into	qt_proc_medico3_w
	from	sus_exclui_medico_ama
	where	(nvl(cd_procedimento,cd_procedimento_w)	= cd_procedimento_w	or cd_procedimento_w is null)
	and	(nvl(cd_area_procedimento,cd_area_proc_w)	= cd_area_proc_w 	or cd_area_proc_w is null)
	and	(nvl(cd_especialidade,cd_especial_proc_w) 	= cd_especial_proc_w 	or cd_especial_proc_w is null)
	and	(nvl(cd_grupo_proc,cd_grupo_proc_w)		= cd_grupo_proc_w 	or cd_grupo_proc_w is null);
	
	if	(qt_proc_medico3_w	> 0) then
		update	procedimento_paciente
		set	cd_medico_executor	= ''
		where	nr_prescricao		= nr_prescricao_w
		and	nr_sequencia_prescricao	= nr_sequencia_w;
	end if;
END LOOP;
CLOSE C01;
--OS 320176
commit;
end Gerar_Proced_AMA;
/