create or replace
procedure gerar_orcamento_rotina(
						nr_seq_orcamento_p		number,
						nm_usuario_p			varchar2,
						cd_medico_exec_p		varchar2,
						qt_procedimento_p		number) is

						
nr_seq_proc_rotina_w		number(10);
ds_lista_mat_exame_w		varchar2(2000);
cd_material_exame_w			varchar2(20);
ie_tipo_w					number(10);
cd_material_w				varchar2(20);
ie_pos_virgula_w			number(10);

cursor c01 is
select	nr_seq_proc_rotina,
		ds_lista_mat_exame,
		cd_material_exame,
		ie_tipo
from	w_proc_rotina_esp_t
where	nm_usuario = nm_usuario_p
and		nvl(ie_marcado,'N') = 'S';

begin
open c01;
loop
fetch c01 into	nr_seq_proc_rotina_w,
				ds_lista_mat_exame_w,
				cd_material_exame_w,
				ie_tipo_w;
exit when c01%notfound;
	begin
	
	if (ds_lista_mat_exame_w is null) then
		cd_material_w := nvl(cd_material_exame_w,'0');
	else
		ie_pos_virgula_w := instr(ds_lista_mat_exame_w,';');
		cd_material_w := substr(ds_lista_mat_exame_w,1,ie_pos_virgula_w);
	end if;
	
	Gerar_Exame_Rotina_Orcamento(	nr_seq_orcamento_p,
									nr_seq_proc_rotina_w,
									ie_tipo_w,
									nm_usuario_p,
									cd_material_w,
									cd_medico_exec_p,
									qt_procedimento_p);
	end;
end loop;
close c01;

end gerar_orcamento_rotina;
/