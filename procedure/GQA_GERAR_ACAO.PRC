create or replace
procedure GQA_Gerar_Acao    (   nr_seq_pend_regra_p		number,
                                nr_atendimento_p		number,
                                cd_pessoa_fisica_p		varchar2,
                                nm_usuario_p            varchar2,
                                cd_pessoa_fisica_dest_p	varchar2,
                                nr_seq_pend_pac_acao_p  number,
                                ie_tipo_pendencia_p     number,
                                ie_liberar_p            varchar2,
                                nr_prescricao_p         in out number) is 
                                

cd_protocolo_w			number(10);
ie_estender_validade_w	varchar2(5);
dt_prim_hor_prescr_w	date;
nr_horas_validade_w		number(5,0) := 0;
ds_erro_w               varchar2(2000);
ie_utilizado_pa_w       varchar2(1) := 'N';
nr_prescricao_pa_w	    number(15);
nr_seq_protocolo_w		number(10);
VarQtHorasMaxExtensao_w	number(10);

/*Constante Tipos de a��o a ser gerada */
ConstTipoPrescricao number(2) := 1;
begin
        
    obter_param_usuario(924,249, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_estender_validade_w);
	Obter_Param_Usuario(924,154, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, VarQtHorasMaxExtensao_w);

    select   e.cd_protocolo, 
             e.nr_seq_protocolo
    into     cd_protocolo_w,
             nr_seq_protocolo_w
    from     gqa_pendencia_pac a,
             gqa_pend_pac_acao b,
             gqa_pendencia_regra d,
             gqa_acao            e
    where    a.nr_sequencia     = b.nr_seq_pend_pac
    and      d.nr_sequencia     = a.nr_seq_pend_regra
    and      e.NR_SEQUENCIA     = b.nr_seq_regra_acao
    and      b.nr_sequencia     = nr_seq_pend_pac_acao_p;
            
	select	max(nr_sequencia)
	into	nr_seq_protocolo_w
	from    protocolo_medicacao
	where   ie_situacao = 'A'
	and     cd_protocolo = cd_protocolo_w
	and		nr_seq_interna = nr_seq_protocolo_w;
    
    case ie_tipo_pendencia_p
    
    when ConstTipoPrescricao then
    
        
        if ((nr_seq_protocolo_w >= 0)  and (cd_protocolo_w >= 0)) then
        
            dt_prim_hor_prescr_w := to_date('30/12/1899 ' || to_char(sysdate, 'hh24:mi') || ':00','dd/mm/yyyy hh24:mi:ss');
        
            select    obter_horas_validade_prescr(dt_prim_hor_prescr_w,nr_atendimento_p,ie_estender_validade_w,'A',sysdate, null)
            into      nr_horas_validade_w
            from      dual;
    			
			if (VarQtHorasMaxExtensao_w > 0) and (nr_horas_validade_w > 0) and (nr_horas_validade_w > VarQtHorasMaxExtensao_w) then
				nr_horas_validade_w := obter_horas_validade_prescr(dt_prim_hor_prescr_w,nr_atendimento_p,'N','A',sysdate, null);
			end if;

            Gerar_Prescricao_Protocolo(cd_pessoa_fisica_p,
                                    cd_pessoa_fisica_dest_p,
                                    nr_atendimento_p,
                                    cd_protocolo_w,
                                    nr_seq_protocolo_w,
                                    0, -- Buscar
                                    0,
                                    nr_horas_validade_w,
                                    nr_prescricao_p,
                                    null,
                                    null,
                                    to_char(sysdate,'hh24:mi'),
                                    sysdate,
                                    null,
                                    1,
                                    'N',
                                    nm_usuario_p,
                                    null, -- Buscar
                                    null,
                                    'N',
                                    nr_seq_pend_pac_acao_p);
                                                
            if ie_liberar_p = 'S' then
            
                liberar_prescricao( nr_prescricao_p,
                                    nr_atendimento_p,
                                    'S',
                                    nvl(obter_perfil_ativo,0),
                                    nm_usuario_p,
                                    'S',
                                    ds_erro_w);
    
                liberar_prescricao_farmacia(nr_prescricao_p, 
                                            0, 
                                            nm_usuario_p, 
                                            'N');
                                            
            end if;
            
            select    nvl(max(ie_utilizado_pa),'N')
            into      ie_utilizado_pa_w
            from      tipo_protocolo
            where     cd_tipo_protocolo = 
            (         select  max(cd_tipo_protocolo)
                    from    protocolo
                    where   cd_protocolo = cd_protocolo_w
            );
    
            if (ie_utilizado_pa_w = 'S') then										 						
            Gerar_Protocolo_PA( cd_protocolo_w,
                                nr_seq_protocolo_w,
                                nr_atendimento_p,
                                'N',
                                'S',
                                'S',
                                'S',
                                'S',
                                'S',
                                'S',
                                'S',
                                'S',
                                nm_usuario_p,
                                nr_prescricao_pa_w);
            end if;
        end if;
    end case;


end GQA_Gerar_Acao;
/