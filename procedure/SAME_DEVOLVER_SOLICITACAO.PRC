create or replace
procedure Same_Devolver_Solicitacao (	nr_seq_solic_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is
					
qt_nao_devolvido_w	number(10,0);
ie_status_w		varchar2(1);

begin

/* ATEN��O: ESTA PROCEDURE N�O TEM COMMIT, POIS SOMENTE � CHAMADA ATRAV�S DE OUTRA PROCEDURE.
 *          CASO SEJA NECESS�RIO IMPLEMENTAR O COMMIT, FAVOR VERIFICAR OS PONTOS ONDE PODEM
 *          OCORRER ROLLBACKS E TRATAR OS MESMOS DE OUTRA FORMA.
 */
 
select	count(*)
into	qt_nao_devolvido_w
from	same_solic_pront_envelope
where	nr_seq_solic	= nr_seq_solic_p
and	dt_devolucao	is null;

select	nvl(max(obter_valor_param_usuario(941, 82, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'D')
into	ie_status_w
from	dual;

if	(qt_nao_devolvido_w = 0) then
	update	same_solic_pront
	set	ie_status	= ie_status_w,
		dt_devolucao	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_solic_p;
end if;

end Same_Devolver_Solicitacao;
/
