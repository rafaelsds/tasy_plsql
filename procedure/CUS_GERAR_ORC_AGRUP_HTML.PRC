create or replace
procedure cus_gerar_orc_agrup_html(nr_seq_agrupamento_p	number,
				nr_seq_tabela_p		number,
				qt_dias_prazo_p		number,
				ie_sobrepor_acumular_p	varchar2,
				nm_usuario_p		varchar2) is 
				
cd_tabela_custo_w	number;

begin

cd_tabela_custo_w := obter_tab_custo_html(nr_seq_tabela_p);

cus_gerar_orcamento_agrup(nr_seq_agrupamento_p,
			cd_tabela_custo_w,
			qt_dias_prazo_p,
			ie_sobrepor_acumular_p,
			nr_seq_tabela_p,
			nm_usuario_p);

commit;

end cus_gerar_orc_agrup_html;
/