create or replace
procedure definir_dados_nota_opme(	nr_seq_material_p	Number,
				nr_nota_fiscal_p	Varchar2,
				nr_sequencia_nf_p	Number,
				dt_emissao_p		Date,
				nm_usuario_p		Varchar2) is			
				
nr_sequencia_nf_w	mat_atend_paciente_opme.nr_sequencia_nf%type;
vl_item_nf_w		mat_atend_paciente_opme.vl_item_nf%type;
				
begin

if	(nvl(nr_sequencia_nf_p,0) <> 0) then
	nr_sequencia_nf_w	:= nr_sequencia_nf_p;
	
	select	max(vl_total_nota)
	into	vl_item_nf_w
	from	nota_fiscal
	where	nr_sequencia = nr_sequencia_nf_w;
else
	nr_sequencia_nf_w	:= null;
	vl_item_nf_w		:= null;
end if;

if	(nvl(nr_seq_material_p, 0) > 0) then
	insert into mat_atend_paciente_opme (
		nr_sequencia,
		nr_seq_material,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_sequencia_nf,
		dt_emissao,
		nr_nota_fiscal,
		vl_item_nf
	) values (
		mat_atend_paciente_opme_seq.NextVal,
		nr_seq_material_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_nf_w,
		dt_emissao_p,
		nr_nota_fiscal_p,
		vl_item_nf_w
	);
end if;

commit;

end definir_dados_nota_opme;
/
