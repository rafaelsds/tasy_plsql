create or replace
procedure Alterar_conta_contabil_nf (
			nr_sequencia_p		number,
			nr_item_nf_p		number,
			cd_conta_contabil_p	varchar2,
			nm_usuario_p		varchar2) is 

cd_conta_contabil_ant_w	varchar2(20);
ds_historico_w		varchar2(255);
qt_registro_w		number(10);
vl_total_nota_w		number(13,4);

begin


if	(nr_sequencia_p > 0) then

	select	max(cd_conta_contabil)
	into	cd_conta_contabil_ant_w
	from	nota_fiscal_conta
	where	nr_sequencia	= nr_sequencia_p;
	
	select	count(*)
	into	qt_registro_w
	from	nota_fiscal_conta
	where	nr_seq_nf		= nr_sequencia_p;
	
	
	if	(qt_registro_w > 0) then
	
		update	nota_fiscal_conta
		set	cd_conta_contabil	= cd_conta_contabil_p
		where	nr_seq_nf		= nr_sequencia_p;
	
		ds_historico_w	:=  wheb_mensagem_pck.get_texto(315092,	'NM_USUARIO_P='||NM_USUARIO_P||
									';DATA='||sysdate||
									';CD_CONTA_CONTABIL_ANT_W='||CD_CONTA_CONTABIL_ANT_W||
									';CD_CONTA_CONTABIL_P='||CD_CONTA_CONTABIL_P);
		
		gerar_historico_nota_fiscal(	nr_sequencia_p,
						nm_usuario_p,
						37,--Altera��o de Conta Cont�bil
						substr(ds_historico_w,1,255));
	else
		select	nvl(vl_total_nota,0)
		into	vl_total_nota_w
		from	nota_fiscal
		where	nr_sequencia = nr_sequencia_p;
	
		insert into nota_fiscal_conta(
			nr_sequencia,
			nr_seq_nf,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_conta_contabil,
			vl_contabil)
		values(	nota_fiscal_conta_seq.nextval,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_conta_contabil_p,
			vl_total_nota_w);
	end if;
	
end if;

commit;

end Alterar_conta_contabil_nf;
/
