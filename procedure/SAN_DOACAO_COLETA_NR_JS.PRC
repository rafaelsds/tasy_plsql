create or replace
procedure san_doacao_coleta_nr_js(	cd_estabelecimento_p	Number,
					nm_usuario_p		Varchar2,
					nr_sangue_p	out	Number,
					nr_seq_entidade_p out	Number,
					nr_seq_status_doacao_p out Number,
					ie_reserva_nao_utilizada_p out Varchar2,
					ie_gerar_nr_sangue_p out Varchar2) is 

begin

nr_sangue_p := obter_numero_sangue_seq(cd_estabelecimento_p);

select 	max(nr_seq_entidade) 
into	nr_seq_entidade_p
from  	san_parametro
where  	cd_estabelecimento = cd_estabelecimento_p;

select  max(nr_sequencia) 
into	nr_seq_status_doacao_p
from    san_status_doacao 
where   ie_status_doacao = 1
and     ie_situacao = 'A';

select 	max(ie_reserva_nao_utilizada),
	nvl(max(ie_gerar_nr_sangue),'N')
into	ie_reserva_nao_utilizada_p,
	ie_gerar_nr_sangue_p	
from 	san_parametro 
where 	cd_estabelecimento = cd_estabelecimento_p;

end san_doacao_coleta_nr_js;
/