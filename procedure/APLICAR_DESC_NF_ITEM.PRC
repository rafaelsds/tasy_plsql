create or replace
procedure aplicar_desc_nf_item(	nr_sequencia_p		number,
				vl_desconto_p		number,
				pr_desconto_p		number,
				cd_estabelecimento_p	number) is

vl_total_item_nf_w		number(13,2);
vl_tot_itens_nf_w		number(13,2);
qt_item_nf_w			number(13,4);
nr_item_nf_w			number(5);
pr_desconto_w			number(7,4);
vl_desconto_w			number(13,2);
qt_itens_nf_w			number(5);

cursor c01 is
select	nr_item_nf,
	qt_item_nf,
	vl_total_item_nf
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_p
and	cd_estabelecimento = cd_estabelecimento_p;

begin

select	sum(vl_total_item_nf)
into	vl_tot_itens_nf_w
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_p
and	cd_estabelecimento = cd_estabelecimento_p;

if	(vl_desconto_p > 0) or
	(pr_desconto_p > 0) then
	open c01;
	loop
	fetch c01 into
		nr_item_nf_w,
		qt_item_nf_w,
		vl_total_item_nf_w;
	exit when c01%notfound;
		begin
		if	(vl_desconto_p > 0) then
			pr_desconto_w :=  ((vl_desconto_p * 100)/vl_tot_itens_nf_w);
			vl_desconto_w := ((vl_total_item_nf_w * pr_desconto_w)/100);
		elsif	(pr_desconto_p > 0) then
			vl_desconto_w := ((vl_total_item_nf_w * pr_desconto_p)/100);
			pr_desconto_w := pr_desconto_p;
		end if;

		update	nota_fiscal_item
		set	vl_desconto = vl_desconto_w,
			pr_desconto = pr_desconto_w,
			vl_liquido = (vl_total_item_nf_w - vl_desconto_w)
		where	nr_item_nf = nr_item_nf_w
		and	nr_sequencia = nr_sequencia_p
		and	cd_estabelecimento = cd_estabelecimento_p;
		end;
	end loop;
	close c01;
	commit;
end if;

end aplicar_desc_nf_item;
/