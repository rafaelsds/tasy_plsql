create or replace
procedure atualiza_status_lote_agrup(	nr_seq_lote_p 		number,
										ie_opcao_p			varchar2,
										nm_usuario_p		varchar2,
										ds_maquina_p		varchar2 default null,
										cd_perfil_ativo_p	number default null,
										nr_seq_embalagem_p	number default null) is

nr_lote_agrupamento_w 	ap_lote.nr_lote_agrupamento%type;
qt_lotes_susp_w			number(10);

begin

/*
C 	- Cancelamento
D 	- Dispensar
E 	- Entrege
A 	- Atendido
CA 	- Cancelado na alta
R 	- Confirmacao do recebimento
S 	- Suspenso
*/

select	nvl(max(nr_lote_agrupamento),0)
into	nr_lote_agrupamento_w
from	ap_lote
where 	nr_sequencia = nr_seq_lote_p;

if	(nr_lote_agrupamento_w > 0) then
	
	select	count(*)
	into	qt_lotes_susp_w
	from 	ap_lote
	where 	nr_lote_agrupamento = nr_lote_agrupamento_w
	and 	nr_sequencia <> nr_seq_lote_p
	and 	nvl(ie_status_lote,'G') <> ie_opcao_p;
	
	if	(qt_lotes_susp_w = 0) then
		
		case ie_opcao_p
			when 'A' then
				update	ap_lote	
				set		dt_atend_farmacia = sysdate,
						nm_usuario_atend = nm_usuario_p,
						ie_status_lote   = ie_opcao_p
				where 	nr_sequencia = nr_lote_agrupamento_w;
			
			/*when 'D' then
				update	ap_lote
				set		dt_disp_farmacia	= sysdate,
						nm_usuario_disp		= nm_usuario_p,
						ds_maquina_disp		= ds_maquina_p,
						cd_perfil_disp		= cd_perfil_ativo_p,
						ie_status_lote		= ie_opcao_p,
						nr_seq_embalagem	= nr_seq_embalagem_p
				where	nr_sequencia		= nr_lote_agrupamento_w;
			
			when 'E' then
				update	ap_lote
				set		dt_entrega_setor	= sysdate,
						nm_usuario_entrega	= nm_usuario_p,
						ds_maquina_entrega	= ds_maquina_p,
						cd_perfil_entrega	= cd_perfil_ativo_p,
						ie_status_lote		= ie_opcao_p
				where	nr_sequencia		= nr_lote_agrupamento_w;
			
			when 'R' then
				update	ap_lote
				set		dt_recebimento_setor= sysdate,
						nm_usuario_receb	= nm_usuario_p,
						ds_maquina_receb	= ds_maquina_p,
						cd_perfil_receb		= cd_perfil_ativo_p,
						ie_status_lote		= ie_opcao_p
				where	nr_sequencia		= nr_lote_agrupamento_w;*/
		end case;
	end if;
end if;

commit;

exception
when others then
	grava_log_tasy(1744,'Error updating grouped batch',nr_seq_lote_p);

end atualiza_status_lote_agrup;
/