create or replace
procedure pls_solic_transferencia_titu(	nr_seq_solicitacao_p		number,
					ie_opcao_p			varchar2,
					ds_observacao_rejeicao_p	varchar2,
					nm_usuario_p			Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Rejeita ou aprova a solicitação de transferência de titularidade
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ x] Tasy (Delphi/Java) [   ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
ie_opcao_p
'R' - Rejeitar
'A' - Aprovar

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 


begin
if (ie_opcao_p = 'R') then
	update	pls_solic_transf_titular
	set	dt_rejeicao		= sysdate,
		ie_status		= 2,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,		
		ds_observacao_rejeicao  = ds_observacao_rejeicao_p 
	where	nr_sequencia	= nr_seq_solicitacao_p;			
elsif(ie_opcao_p = 'A') then
	update	pls_solic_transf_titular
	set	dt_confirmacao		= sysdate,
		ie_status		= 1,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate	
	where	nr_sequencia	= nr_seq_solicitacao_p;
	
end if;

commit;

end pls_solic_transferencia_titu;
/
