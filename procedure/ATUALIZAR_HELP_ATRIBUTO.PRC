create or replace
procedure atualizar_help_atributo is

ds_atributo_w	varchar2(4000);
nm_tabela_w	varchar2(50);
nm_atributo_w	varchar2(50);
retorno_w	Number(5);
c01	  	Integer;
ds_comando_w	varchar(2000);
i		number(5)	:= 0;
	
begin

ds_comando_w :=	' select	ds_atributo,	' ||
		'		nm_tabela,	' ||
		'		nm_atributo	' ||
		' from		tasy_versao.tabela_atributo ';

C01 := DBMS_SQL.OPEN_CURSOR;

DBMS_SQL.PARSE(C01, ds_comando_w, dbms_sql.Native);

DBMS_SQL.DEFINE_COLUMN(C01, 1, ds_atributo_w,4000);
DBMS_SQL.DEFINE_COLUMN(C01, 2, nm_tabela_w,50);
DBMS_SQL.DEFINE_COLUMN(C01, 3, nm_atributo_w,50);

retorno_w := DBMS_SQL.execute(C01);

while	( DBMS_SQL.FETCH_ROWS(C01) > 0 ) loop
	DBMS_SQL.COLUMN_VALUE(C01, 1, ds_atributo_w);
	DBMS_SQL.COLUMN_VALUE(C01, 2, nm_tabela_w);
	DBMS_SQL.COLUMN_VALUE(C01, 3, nm_atributo_w);
	
	update	tabela_atributo
	set	ds_atributo	= ds_atributo_w
	where	nm_tabela	= nm_tabela_w
	and	nm_atributo	= nm_atributo_w;

	if	(i > 1000) then
		COMMIT;
		i	:= 0;
	else
		i	:= i + 1;
	end	if;
END LOOP; 

DBMS_SQL.CLOSE_CURSOR(C01);

COMMIT;

end;
/