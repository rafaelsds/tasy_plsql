CREATE OR REPLACE
PROCEDURE DELETE_VERSIONAMENTO_TABELA is

BEGIN

delete from w_versao_tabela_item 
where dt_atualizacao < sysdate - 2/24;

commit;

delete from w_versao_tabela 
where dt_atualizacao < sysdate - 2/24;

commit;

END DELETE_VERSIONAMENTO_TABELA;
/