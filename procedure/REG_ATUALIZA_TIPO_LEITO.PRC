create or replace
procedure reg_atualiza_tipo_leito(	cd_tipo_leito_p  varchar2,
					nr_ordem_p       varchar2,
					nr_sus_p         varchar2,
					nm_usuario_p     varchar2) is 
qt_registro_w		number(3);

begin
if 	(nvl(cd_tipo_leito_p, 0) > 0) then

	select	count(*)
	into	qt_registro_w
	from	regulacao_tipo_leito
	where	cd_tipo_leito = cd_tipo_leito_p;

	if	(qt_registro_w = 0) then

		insert into regulacao_tipo_leito (
			nr_sequencia, 
			cd_tipo_leito,			
			nr_ordem,
			nr_sus,
			nm_usuario,
			dt_atualizacao, 
			nm_usuario_nrec, 
			dt_atualizacao_nrec
		) values (	
			regulacao_tipo_leito_seq.nextval, 
			cd_tipo_leito_p, 			
			nr_ordem_p, 
			nr_sus_p,
			nm_usuario_p,
			sysdate, 
			nm_usuario_p,
			sysdate 
		);	
	else
		update	regulacao_tipo_leito
		set	cd_tipo_leito = cd_tipo_leito_p,
			nr_ordem = nr_ordem_p, 
			nr_sus = nr_sus_p, 
			nm_usuario = nm_usuario_p, 
			dt_atualizacao = sysdate
		where	cd_tipo_leito = cd_tipo_leito_p;
	end if;
end if;

commit;

end reg_atualiza_tipo_leito;
/
