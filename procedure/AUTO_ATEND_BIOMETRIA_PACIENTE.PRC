create or replace
procedure auto_atend_biometria_paciente(
			cd_pessoa_fisica_p	varchar2,
			ie_dedo_p	varchar2,
			ds_hash_p		varchar2) is 

qt_registros_w         number(2);

begin

  select count(1)
  into  qt_registros_w
  from  pessoa_fisica_biometria
  where cd_pessoa_fisica = cd_pessoa_fisica_p
  and ie_dedo = ie_dedo_p;
  
  if qt_registros_w > 0 then
  
    update pessoa_fisica_biometria
    set   ds_biometria = ds_hash_p,
          nm_usuario = 'autoAtend',
          dt_atualizacao = sysdate 
    where cd_pessoa_fisica = cd_pessoa_fisica_p
    and   ie_dedo = ie_dedo_p;
  
  else
  
    insert into pessoa_fisica_biometria(
      cd_empresa_biometria,
      cd_pessoa_fisica,
      ds_biometria,
      dt_atualizacao,
      dt_atualizacao_nrec,
      ie_dedo,
      nm_usuario,
      nm_usuario_nrec,
      nr_sequencia
    ) values (
      1,
      cd_pessoa_fisica_p,
      ds_hash_p,
      sysdate,
      sysdate,
      ie_dedo_p,
      'autoAtend',
      'autoAtend',
      pessoa_fisica_biometria_seq.nextval);
      
  end if;

commit;

end auto_atend_biometria_paciente;
/
