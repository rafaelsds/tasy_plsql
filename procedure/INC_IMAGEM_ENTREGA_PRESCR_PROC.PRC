create or replace procedure inc_imagem_entrega_prescr_proc ( nr_prescricao_p number,
					               nr_seq_prescr_p number,
								   nm_usuario_p varchar2,
								   cd_pessoa_recebido_p varchar2,
								   ds_observacao_p varchar2,
								   nm_pessoa_externo_p varchar2 ,
								   ds_biometria_p varchar2 default null,
								   cd_parentesco_p varchar2 default null) is

dt_biometria_w date;

begin
dt_biometria_w := null;

if (nr_prescricao_p is not null) and
   (nr_seq_prescr_p is not null) then

	if(ds_biometria_p is not null) then
	   dt_biometria_w := sysdate;
	end if;

	insert 	into prescr_proc_info_laudo
	    	(nr_sequencia,
	    	 dt_atualizacao,
	  	     nm_usuario,
		     dt_atualizacao_nrec,
		     nm_usuario_nrec,
		     nr_prescricao,
		     nr_seq_prescr,
		     ie_entrega_imagem,
		     cd_pessoa_recebido,
		     ds_observacao,
		     nm_pessoa_externo,
			 ds_biometria,
			 dt_biometria,
			 cd_parentesco)
	    values 	(	prescr_proc_info_laudo_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_prescricao_p,
			nr_seq_prescr_p,
			'S',
			cd_pessoa_recebido_p,
			ds_observacao_p,
			nm_pessoa_externo_p,
			ds_biometria_p,
			dt_biometria_w,
			cd_parentesco_p);

	commit;
end if;

end inc_imagem_entrega_prescr_proc;
/
