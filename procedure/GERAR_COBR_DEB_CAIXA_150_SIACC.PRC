create or replace
procedure gerar_cobr_deb_caixa_150_SIACC ( 	nr_seq_cobr_escrit_p		number,
											cd_estabelecimento_p		number,
											nm_usuario_p				varchar2) is 
											
ds_conteudo_w					varchar2(150);	
cd_convenio_w					varchar2(20);
nm_empresa_w					varchar2(20);
dt_movimento_w					varchar2(8);
nr_sequencial_arq_w				varchar2(6);
ds_ident_servico_w				varchar2(17);
cd_conta_compromisso_w			varchar2(16);
nr_seq_registro_w				number(10);	
qt_reg_arquivo_w				number(10) := 0;	
vl_total_w						number(15,2)	:= 0;

cd_agencia_bancaria_w		varchar2(4);
dt_vencimento_w			varchar2(8);
vl_titulo_w			number(15,2);
ds_uso_empresa_w		varchar2(60);
ds_ident_cliente_emp_w		varchar2(25);
ds_ident_cliente_banco_w	varchar2(14);

Cursor C01 is
	select	--lpad(nvl(substr(x.cd_agencia_bancaria,1,4),'0'),4,'0') cd_agencia_bancaria,
			lpad(nvl(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'A'),1,4),c.cd_agencia_bancaria),4,'0') cd_agencia_bancaria,
			substr(to_char(b.dt_pagamento_previsto,'YYYYMMDD'),1,8) dt_vencimento,
			c.vl_cobranca vl_titulo,
			rpad(lpad(b.nr_titulo,10,'0'),60,' ')  ds_uso_empresa,
			rpad(nvl(substr(pls_obter_carteira_segurado(pls_obter_segurado_pagador(f.nr_sequencia)),1,25),' '),25,' ') ds_ident_cliente_emp,
			decode(d.nr_seq_pagador,null,
						'001' || /*Codigo da operacao*/
						lpad(nvl(substr(OBTER_BANCO_PF_PJ(b.cd_pessoa_fisica,b.cd_cgc,'AC'),1,8),'0'),8,'0') || /*Numero da conta*/
						lpad(nvl(substr(OBTER_BANCO_PF_PJ(b.cd_pessoa_fisica,b.cd_cgc,'ADC'),1,1),' '),1,' ') || /*Digito Conta*/   
						'  ' /*Brancos*/,
						'001' || /*Codigo da operacao*/
						lpad(nvl(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'C'),1,8),'0'),8,'0') || /*Numero da conta*/
						lpad(nvl(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DC'),1,1),' '),1,' ') || /*Digito Conta*/
						'  ' /*Brancos*/ 
				   )
	from	banco_estabelecimento	x,
			pls_contrato_pagador	f,
			banco_carteira		e,
			pls_mensalidade		d,
			titulo_receber_v	b,
			titulo_receber_cobr	c,
			cobranca_escritural	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and		c.nr_titulo		= b.nr_titulo
	and		a.nr_seq_conta_banco	= x.nr_sequencia
	and		b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and		b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and		d.nr_seq_pagador	= f.nr_sequencia(+)
	and		a.nr_sequencia		= nr_seq_cobr_escrit_p;		

begin

delete 
from 	w_envio_banco 
where 	nm_usuario = nm_usuario_p;

select	substr(nvl(a.cd_convenio_banco,'0'),1,20),
		upper(substr(elimina_acentuacao(obter_razao_social(c.cd_cgc)),1,20)),
		to_char(sysdate, 'YYYYMMDD'),
		substr(nvl(b.nr_remessa, b.nr_sequencia),1,6),
		'DEB AUTOMAT',
		lpad(substr(e.cd_agencia_bancaria,1,4),4,'0') || '003' || lpad(substr(a.cd_conta,1,8),8,'0') || substr(nvl(a.ie_digito_conta,0),1,1),
		0
into	cd_convenio_w,
		nm_empresa_w,
		dt_movimento_w,
		nr_sequencial_arq_w,
		ds_ident_servico_w,
		cd_conta_compromisso_w,
		nr_seq_registro_w		
from	banco_estabelecimento a,
		cobranca_escritural b,
		estabelecimento c,
		agencia_bancaria e
where	a.nr_sequencia 			= b.nr_seq_conta_banco
and		b.cd_estabelecimento 	= c.cd_estabelecimento 
and		a.cd_agencia_bancaria	= e.cd_agencia_bancaria
and		a.cd_banco				= e.cd_banco
and		b.nr_sequencia			= nr_seq_cobr_escrit_p;
		
qt_reg_arquivo_w := nvl(qt_reg_arquivo_w,0) + 1;
/*INICIO HEADER DO ARQUIVO*/
ds_conteudo_w	:=	'A' 									|| /*Pos 1*/
					'1'										|| /*Pos 2 - Codigo da remessa. 1 = Remessa, 2 = Retorno*/
					lpad(cd_convenio_w,20,' ')				|| /*Pos 3 a 22 - C�digo do conv�nio*/
					rpad(nm_empresa_w,20,' ')				|| /*Pos 23 a 42 - Nome da Empresa*/
					'104'									|| /*Pos 43 a 45 - C�digo do banco*/
					'CAIXA ECONOM FEDERAL'					|| /*Pos 46 a 65 - Nome do banco*/		
					lpad(dt_movimento_w,8,' ')				|| /*Pos 66 a 73 - Data do movimento*/
					lpad(nr_sequencial_arq_w,6,'0')			|| /*Pos 74 a 79 - N�mero sequencial do arquivo*/
					'04'									|| /*Pos 80 a 81 - Vers�o do layout*/
					rpad(ds_ident_servico_w,17,' ')			|| /*Pos 82 a 98 - Identifica��o do servi�o. DEB AUTOMAT ou FOLHA PAGAMENTO*/
					cd_conta_compromisso_w					|| /*Pos 99 a 114 - Conta compromisso*/
					'P'										|| /*Pos 115 - Identificacao Ambiente Cliente*/
					'P'										|| /*Pos 116 - Identificacao Ambiente Caixa*/
					lpad(' ', 27 ,' ')						|| /*Pos 117 a 143 - Reservado para o futuro*/
					lpad(nr_seq_registro_w ,6 ,'0' )		|| /*Pos 144 a 149 - Numero sequencial do registro*/
					lpad(' ', 1 , ' ');						   /*Pos 150 - Reservado para o futuro*/		

insert into w_envio_banco(	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres)
				values	(	w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_registro_w);					
/*FIM HEADER DO ARQUIVO*/

/*INICIO REGISTRO E*/
open C01;
loop
fetch C01 into	
	cd_agencia_bancaria_w,
	dt_vencimento_w,
	vl_titulo_w,
	ds_uso_empresa_w,
	ds_ident_cliente_emp_w,
	ds_ident_cliente_banco_w;
exit when C01%notfound;
	begin
	
	nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
	qt_reg_arquivo_w  := nvl(qt_reg_arquivo_w,0) + 1;
	vl_total_w	:= nvl(vl_total_w,0) + vl_titulo_w;

	ds_conteudo_w	:=	'E'																		|| /*Pos 1*/
						ds_ident_cliente_emp_w													|| /*Pos 2 a 26*/
						cd_agencia_bancaria_w													|| /*Pos 27 a 30*/
						ds_ident_cliente_banco_w 												|| /*pos 31 a 44 */
						dt_vencimento_w 														|| /*pos 45 a 52*/
						lpad(replace(to_char(vl_titulo_w, 'fm00000000000.00'),'.',''),15,'0') 	|| /*pos 53 a 67 = Valor */
						'03' 																	|| /*pos 68 a 69 = Moeda*/
						ds_uso_empresa_w 														|| /*pos 70 a 129 = Uso da empresa*/
						lpad(nr_seq_registro_w,6,'0')											|| /*Pos 130  a 135*/
						lpad(' ',8,' ')															|| /*Pos 136 a 143*/
						lpad(nr_seq_registro_w,6,'0')											|| /*Pos 144  a 149*/
						'0';																	   /*Pos 150*/	

	insert into w_envio_banco(	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								cd_estabelecimento,
								ds_conteudo,
								nr_seq_apres)
					values	(	w_envio_banco_seq.nextval,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								cd_estabelecimento_p,
								ds_conteudo_w,
								nr_seq_registro_w);							
	
	end;
end loop;
close C01;
/*FIM REGISTRO E*/

/*INICIO REGISTRO Z TRAILER*/
nr_seq_registro_w := nvl(nr_seq_registro_w,0) + 1;
qt_reg_arquivo_w  := nvl(qt_reg_arquivo_w,0) + 1;

ds_conteudo_w	:= 	'Z' 																	|| /*Pos 1*/
					lpad(qt_reg_arquivo_w,6,'0') 											|| /*Pos 2 a 7*/
					lpad(replace(to_char(vl_total_w, 'fm00000000000.00'),'.',''),17,'0') 	|| /*Pos 8 a 24*/ 
					lpad(' ',119,' ')														|| /*Pos 25 a 143*/
					lpad(nr_seq_registro_w,6,'0')											|| /*Pos 144  a 149*/
					' ';																		/*Pos 150*/			

insert into w_envio_banco ( nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres)
				values	  ( w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_registro_w);

/*FIM REGISTRO Z TRAILER*/

commit;

end gerar_cobr_deb_caixa_150_SIACC;
/