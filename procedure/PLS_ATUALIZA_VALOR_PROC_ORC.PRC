create or replace
procedure pls_atualiza_valor_proc_orc
				(	nr_seq_orcamento_proc_p	Number,
					ie_gravar_log_p		Varchar2,
					nm_usuario_p		Varchar2) is

cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
dt_solicitacao_w		Date; 
nr_seq_segurado_w		Number(10);
cd_medico_solicitante_w		Varchar2(10);
nr_seq_prestador_w		Number(10);
nr_seq_plano_w			Number(10);
nr_seq_tipo_acomodacao_w	Number(10);
nr_seq_clinica_w		Number(10);
cd_estabelecimento_w		Number(10);
ie_tipo_contratacao_w		Varchar2(2);
nr_seq_classificacao_prest_w	Number(10);
nr_seq_categoria_w		Number(10);
nr_seq_contrato_w		Number(10);
cd_convenio_w			Varchar2(10);
cd_categoria_w			Varchar2(10);
vl_procedimento_w		Number(15,2)	:= 0;
ie_carater_internacao_w		Varchar2(10);
nr_seq_req_proc_w		Number(10);
dt_procedimento_w		date	:= null;

dados_regra_preco_proc_w	pls_cta_valorizacao_pck.dados_regra_preco_proc;
dados_guia_w			pls_cta_valorizacao_pck.dados_guia;
begin

/*Obter dados dos procedimentos do orçamento*/
select	nr_seq_req_proc,
	cd_procedimento,
	ie_origem_proced
into	nr_seq_req_proc_w,
	cd_procedimento_w,
	ie_origem_proced_w
from	pls_orcamento_proc
where	nr_sequencia	= nr_seq_orcamento_proc_p;

/* Obter dados da requisição */
select	a.dt_requisicao,
	a.nr_seq_segurado,
	a.cd_medico_solicitante,
	a.nr_seq_prestador,
	a.nr_seq_plano,
	a.nr_seq_tipo_acomodacao,
	a.nr_seq_clinica,
	a.cd_estabelecimento,
	ie_carater_atendimento,
	ie_tipo_consulta
into	dt_solicitacao_w,
	nr_seq_segurado_w,
	cd_medico_solicitante_w,
	nr_seq_prestador_w,
	nr_seq_plano_w,
	nr_seq_tipo_acomodacao_w,
	nr_seq_clinica_w,
	cd_estabelecimento_w,
	ie_carater_internacao_w,
	dados_guia_w.ie_tipo_consulta_guia
from	pls_requisicao		a,
	pls_requisicao_proc	b
where	a.nr_sequencia		= b.nr_seq_requisicao
and	b.nr_sequencia		= nr_seq_req_proc_w;

/* Obter dados do plano */
begin
	select	ie_tipo_contratacao
	into	ie_tipo_contratacao_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_w;
exception
when others then
	ie_tipo_contratacao_w := null;
end;

/* Obter dados do prestador */
begin
	select	nr_seq_classificacao
	into	nr_seq_classificacao_prest_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_prestador_w;
exception
when others then
	nr_seq_classificacao_prest_w := null;
end;

/* Obter a categoria do tipo de acomodação */
if	(nr_seq_tipo_acomodacao_w is not null) then
	select	max(nr_seq_categoria)
	into	nr_seq_categoria_w
	from	pls_regra_categoria
	where	nr_seq_tipo_acomodacao	= nr_seq_tipo_acomodacao_w;
end if;

/*Obter dados segurado*/
begin
	select	nvl(nr_seq_contrato,0),
		pls_obter_conv_cat_segurado(nr_sequencia, 1),
		pls_obter_conv_cat_segurado(nr_sequencia, 2)
	into	nr_seq_contrato_w,
		cd_convenio_w,
		cd_categoria_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
exception
when others then
	nr_seq_contrato_w	:= null;
	cd_convenio_w		:= null;
	cd_categoria_w		:= null;
end;

pls_define_preco_proc(	cd_estabelecimento_w, nr_seq_prestador_w, nr_seq_categoria_w,
			dt_solicitacao_w, null, cd_procedimento_w,
			ie_origem_proced_w, nr_seq_tipo_acomodacao_w, null,
			nr_seq_clinica_w, nr_seq_plano_w, 'OR',
			nr_seq_contrato_w, 0, nr_seq_orcamento_proc_p,
			ie_gravar_log_p, cd_convenio_w, cd_categoria_w,
			ie_tipo_contratacao_w, 0, nr_seq_segurado_w,
			'', '', nr_seq_classificacao_prest_w,
			cd_medico_solicitante_w, 'N', '', 
			'', 'A', 'X',
			null, ie_carater_internacao_w, dt_procedimento_w,
			null,null,null,
			dados_guia_w, null, dados_regra_preco_proc_w);
			
vl_procedimento_w		:= dados_regra_preco_proc_w.vl_procedimento;
				
update	pls_orcamento_proc
set	vl_procedimento		= vl_procedimento_w
where	nr_sequencia		= nr_seq_orcamento_proc_p;

commit;

end pls_atualiza_valor_proc_orc;
/