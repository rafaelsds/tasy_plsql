create or replace
procedure	gerar_nf_conta_selec_prot(
			nr_seq_protocolo_p		number,
			cd_convenio_p			number,
			cd_serie_nf_p			varchar2,
			cd_estabelecimento_p		number,
			dt_emissao_p			date,
			cd_nat_oper_nf_p			varchar2,
			cd_operacao_nf_p			number,
			cd_condicao_pagamento_p		number,
			ds_complemento_p			varchar2,
			ds_observacao_p			varchar2,
			vl_desconto_nf_p			number,
			nr_seq_classif_fiscal_p		number,
			nr_seq_sit_trib_p			number,
			nr_nota_fiscal_p			number,
			cd_procedimento_p		number,
			ie_origem_proced_p		number,
			cd_pessoa_fisica_p		varchar2,
			cd_cgc_p			varchar2,
			cd_setor_atendimento_p		number,
			nm_usuario_p			varchar2,	
			nr_sequencia_p		out	number) is

c001					integer;
c10_w					integer;
ignore					Integer;

ie_conta_financ_nf_w			varchar2(255);
cd_convenio_w				number(5);
ie_tipo_convenio_w			number(5);
cont_w					number(10);
ie_atualiza_serie_nf_saida_w			varchar2(15);
ie_calcula_nf_conv_w			varchar2(1);
ie_nf_guia_partic_w			varchar2(1);
ie_calcula_nf_w				varchar2(1);
ie_regra_calcula_nf_w			varchar2(1);

ds_erro_nota_w				varchar2(255);
ds_erro_valor_w				varchar2(255);
nr_interno_conta_w			number(10);
nr_notas_conta_w				number(10);
vl_item_prot_w				number(15,2)	:= 0;
vl_guias_w				number(15,2)	:= 0;
vl_total_guias_w				number(15,2)	:= 0;
vl_total_conta_w				number(15,2)	:= 0;

/* Gerar nota fiscal */
cd_cgc_w				varchar2(14);
cd_cgc_emitente_w			varchar2(14);
cd_pessoa_fisica_w			varchar2(10);
nr_sequencia_w				number(10);
nr_sequencia_nf_w				number(10);
ie_tipo_nota_w				varchar2(3);
nr_nota_fiscal_w           			varchar2(255);
vl_total_nota_w         			number(13,2);
vl_mercadoria_w            			number(13,2);
vl_ipi_w        	         			number(13,2);
vl_descontos_w             			number(13,2);
vl_frete_w                			number(13,2);
vl_seguro_w                			number(13,2);
vl_despesa_acessoria_w     			number(13,2);
vl_desconto_rateio_w       			number(13,4);
vl_desc_item_w				number(15,2);
vl_desc_acum_w				number(15,2);
cd_evento_nf_w				number(3);
ds_historico_nf_w				varchar2(255);
nr_contas_w				number(10);
ds_erro_w				varchar2(255);

/*Gerar itens nota fiscal*/
qt_item_nf_w				number(5);
qt_item_calc_w				number(5);
qt_item_neg_w				number(5);
vl_proc_mat_acum_w			number(15,2);

ie_tipo_ordem_w				number(1);
ie_origem_proced_w			number(10);
ie_origem_proced_ww			number(10);
cd_procedimento_w			number(15);
cd_material_w				number(6);
cd_unidade_medida_w			varchar2(30);
qt_proc_mat_w				number(10);
vl_proc_mat_w				number(15,2);
vl_unitario_item_nf_w			number(13,4);
ie_tipo_conta_w				number(3);
cd_centro_custo_w			number(8);
cd_conta_contabil_w			varchar2(20);
cd_conta_financ_w			number(10);
ds_observacao_w            			varchar2(40);

ds_sql_w					varchar2(2000);
ie_lista_itens_w				varchar2(2);
ds_retorno_w				varchar2(15);
ds_retorno_ww				varchar2(15);
ds_retorno_www				varchar2(15);

/* Atualizar nr. Nota Fiscal */
qt_itens_nf_w				number(10);
ie_numero_nota_w				varchar2(1);
qt_reg_w					number(10);

/* Gerar vencimentos da nota fiscal */
ie_forma_pagamento_w			number(2);
dt_vencimento_prot_w			date;
ie_estab_serie_nf_w			parametro_compras.ie_estab_serie_nf%type;

nr_seq_proc_interno_w		procedimento_paciente.nr_seq_proc_interno%type;

cursor	c01 is
select	nvl(sum(a.vl_item),0),
	a.nr_interno_conta
from	protocolo_convenio_item_v a
where 	a.nr_seq_protocolo = nr_seq_protocolo_p
and	((nr_seq_proc_pacote is null or nr_sequencia <> nr_seq_proc_pacote))
and 	a.cd_motivo_exc_conta is null
and	exists	(select	1
		from	w_contas_selecionadas_prot x
		where	nm_usuario = nm_usuario_p
		and	x.nr_interno_conta = a.nr_interno_conta
		and	x.ie_possui_nf = 'N')
group by	a.nr_interno_conta;

cursor	c03 is
select	1,
	a.ie_origem_proced,
	a.cd_procedimento,
	0 cd_material,
	' ' cd_unidade_medida,
	sum(nvl(a.qt_procedimento,0)),
	sum(nvl(a.vl_procedimento,0)),
	nvl(sum(obter_proc_paciente_valor(a.nr_sequencia, 3, 1)),0),
	a.nr_seq_proc_interno
from 	procedimento_paciente a
where	a.nr_interno_conta in (
		select	nr_interno_conta
		from	conta_paciente_nf
		where	nr_sequencia = nr_sequencia_w)
and	a.cd_motivo_exc_conta is null
and	nvl(a.nr_seq_proc_pacote, 0) <> a.nr_sequencia
group by
	a.ie_origem_proced,
	a.cd_procedimento,
	0,
	' ',
	a.nr_seq_proc_interno
union all
select	1, 
	0,
	0,
	a.cd_material,
	a.cd_unidade_medida,
	sum(a.qt_material),
	sum(nvl(a.vl_material,0)),
	nvl(sum(nvl(b.vl_material,0)),0),
	null
from	mat_atend_paciente_valor b,
	material_atend_paciente a
where	a.nr_interno_conta in (
		select	nr_interno_conta
		from	conta_paciente_nf
		where	nr_sequencia = nr_sequencia_w)
and	a.cd_motivo_exc_conta is null
and	a.nr_sequencia	= b.nr_seq_material (+)
and	3		= b.ie_tipo_valor (+)
and	nvl(a.nr_seq_proc_pacote, 0) <> a.nr_sequencia
group by
	0,
	0,
	a.cd_material,
	a.cd_unidade_medida,
	null
order by	1;

cursor	c04 is
select	cd_material,
	cd_procedimento,
	ie_origem_proced,
	upper(ds_sql),
	nvl(ie_lista_itens,'N')
from	parametro_nfs_lista
where	cd_estabelecimento	= cd_estabelecimento_p
and	cd_convenio		= cd_convenio_w
order by	nvl(ie_ordenacao, 1);

begin

if	(trunc(dt_emissao_p) > trunc(sysdate)) then
	--'A data de emiss�o da nota n�o pode ser maior que a data atual!'
	wheb_mensagem_pck.exibir_mensagem_abort(176746);
end if;

select	nvl(max(ie_conta_financ_nf), 'N')
into	ie_conta_financ_nf_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

cd_convenio_w		:= cd_convenio_p;

select	obter_tipo_convenio(cd_convenio_p)
into	ie_tipo_convenio_w
from	dual;

select	count(*)
into	cont_w
from	serie_nota_fiscal
where	cd_serie_nf 		= cd_serie_nf_p
and	cd_estabelecimento 	= cd_estabelecimento_p;

if	(cont_w = 0) then
	--'A S�rie informada n�o est� cadastrada!'
	wheb_mensagem_pck.exibir_mensagem_abort(176750);
end if;

select	nvl(max(ie_calcular_nf),'N'),
	nvl(max(ie_atualiza_serie_nf_saida),'S')
into	ie_calcula_nf_conv_w,
	ie_atualiza_serie_nf_saida_w
from	convenio_estabelecimento
where	cd_convenio		= cd_convenio_p
and	cd_estabelecimento	= cd_estabelecimento_p;

open c01;
loop
fetch c01 into
	vl_item_prot_w,
	nr_interno_conta_w;
exit when c01%notfound;
	begin

	select	count(*)
	into	nr_notas_conta_w
	from	nota_fiscal
	where	nr_interno_conta	= nr_interno_conta_w;

	if	(nr_notas_conta_w > 0) then
		ds_erro_nota_w	:= ds_erro_nota_w || Wheb_mensagem_pck.get_Texto(307127) || nr_interno_conta_w || chr(13);
	end if;

	select	nvl(sum(a.vl_guia),0)
	into	vl_guias_w
	from	conta_paciente_guia a
	where	a.nr_interno_conta	= nr_interno_conta_w;

	vl_total_guias_w	:= vl_total_guias_w + vl_guias_w;

	if	(vl_guias_w <> vl_item_prot_w) then
		ds_erro_valor_w	:= ds_erro_valor_w ||
				Wheb_mensagem_pck.get_Texto(307127) || nr_interno_conta_w ||
				Wheb_mensagem_pck.get_Texto(307129) || vl_item_prot_w ||
				Wheb_mensagem_pck.get_Texto(307130) || vl_guias_w || chr(13);
	end if;	

	end;
end loop;
close c01;

if	(ds_erro_nota_w is not null) then
	/*Existem contas com notas j� geradas*/
	wheb_mensagem_pck.exibir_mensagem_abort(176752,'DS_ERRO_P='||ds_erro_nota_w);
end if;

if	(ds_erro_valor_w is not null) then
	begin

	select	sum(vl_item)
	into	vl_total_conta_w
	from 	protocolo_convenio_item_v a
	where 	a.nr_seq_protocolo = nr_seq_protocolo_p
	and	((nr_seq_proc_pacote is null or nr_sequencia <> nr_seq_proc_pacote))
	and 	a.cd_motivo_exc_conta is null
	and	exists	(select	1
			from	w_contas_selecionadas_prot x
			where	nm_usuario = nm_usuario_p
			and	x.nr_interno_conta = a.nr_interno_conta
			and	x.ie_possui_nf = 'N');

	/*O valor do t�tulo ## n�o bate com o valor das guias*/
					
	wheb_mensagem_pck.exibir_mensagem_abort(173217,
						'VL_TOTAL_CONTA_W='||vl_total_conta_w||';'||
						'VL_TOTAL_GUIAS_W='||vl_total_guias_w||';'||
						'DS_ERRO_VALOR_W='||ds_erro_valor_w);
	end;				
end if;

/* Obter CGC do conv�nio */
begin
select	cd_cgc
into	cd_cgc_w
from	convenio
where	cd_convenio 	= cd_convenio_p;
exception
	when others then
		--'Erro ao ler o CGC do conv�nio do protocolo');
		wheb_mensagem_pck.exibir_mensagem_abort(176775);
end;


/* Obter sequ�ncia da nota fiscal */
select	nota_fiscal_seq.nextval
into	nr_sequencia_w
from	dual;

/* Obter CGC do emitente */
select	cd_cgc
into	cd_cgc_emitente_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

/* obter se ser� considerado estabelecimento no par�metro de compras */
select	nvl(max(ie_estab_serie_nf),'N')
into	ie_estab_serie_nf_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_p;

/* Zerar valor ainda n�o identificados */
nr_nota_fiscal_w		:= nr_sequencia_w + 800000;
vl_mercadoria_w		:= 0;
vl_total_nota_w		:= 0;
vl_ipi_w			:= 0;
vl_descontos_w		:= nvl(vl_desconto_nf_p,0);
vl_frete_w		:= 0;
vl_seguro_w		:= 0;
vl_despesa_acessoria_w	:= 0;
vl_desconto_rateio_w	:= 0;
nr_sequencia_nf_w		:= 9;
vl_desc_item_w		:= 0;
vl_desc_acum_w		:= 0;
ie_tipo_nota_w		:= 'SE';

cd_pessoa_fisica_w	:= null;
/* 2 Par�metros n�o informados (Pessoa Jur�dica) */
if	((cd_pessoa_fisica_p is null) and (cd_cgc_p is null)) then
	cd_cgc_w		:= cd_cgc_w;
end if;
/* 2 Par�metros informados (Pessoa Jur�dica) */
if	(cd_cgc_p is not null) then
	cd_cgc_w		:= cd_cgc_p;
	cd_pessoa_fisica_w	:= null;
end if;
/* Apenas Par�metro Pessoa F�sica (Pessoa F�sica) */
if	((cd_cgc_p is null) and (cd_pessoa_fisica_p is not null)) then
	ie_tipo_nota_w		:= 'SF';
	cd_cgc_w		:= null;
	cd_pessoa_fisica_w	:= cd_pessoa_fisica_p;
end if;

/* Gravar corpo da nota fiscal */
begin
insert into nota_fiscal(
	nr_sequencia,			cd_estabelecimento,
	cd_cgc_emitente,			cd_serie_nf,
	nr_nota_fiscal,			nr_sequencia_nf,
	cd_operacao_nf,			dt_emissao,
	dt_entrada_saida,			ie_acao_nf,
	ie_emissao_nf,			ie_tipo_frete,
	vl_mercadoria,			vl_total_nota,
	qt_peso_bruto,			qt_peso_liquido,
	dt_atualizacao,			nm_usuario,
	cd_condicao_pagamento,		cd_cgc,
	cd_pessoa_fisica,			vl_ipi,
	vl_descontos,			vl_frete,
	vl_seguro,			vl_despesa_acessoria,
	ds_observacao,			cd_natureza_operacao,
	vl_desconto_rateio,		ie_situacao,
	nr_interno_conta,			nr_seq_protocolo,
	ds_obs_desconto_nf,		nr_seq_classif_fiscal,
	ie_tipo_nota,			nr_recibo,
	dt_atualizacao_nrec,		nm_usuario_nrec,
	cd_setor_digitacao)
values(	
	nr_sequencia_w,			cd_estabelecimento_p,
	cd_cgc_emitente_w,		cd_serie_nf_p,
	nr_nota_fiscal_w,			nr_sequencia_nf_w,
	cd_operacao_nf_p,			dt_emissao_p,
	dt_emissao_p,			'1',
	'0',				'0',
	vl_mercadoria_w,			vl_total_nota_w,
	0,				0,
	sysdate,				nm_usuario_p,
	cd_condicao_pagamento_p,		cd_cgc_w,
	cd_pessoa_fisica_w,		vl_ipi_w,
	vl_descontos_w,			vl_frete_w,
	vl_seguro_w,			vl_despesa_acessoria_w,
	ds_observacao_p,			cd_nat_oper_nf_p,
	vl_desconto_rateio_w,		'1',
	null,				nr_seq_protocolo_p,
	null,				nr_seq_classif_fiscal_p,
	ie_tipo_nota_w,			null,
	sysdate,				nm_usuario_p,
	cd_setor_atendimento_p);
exception
	when others then
		ds_erro_w	:= sqlerrm(sqlcode);
		/*Erro ao Gravar Corpo da Nota Fiscal*/		
		wheb_mensagem_pck.exibir_mensagem_abort(173250,
							'CD_ESTAB_NOTA_FISCAL_W='||cd_estabelecimento_p||';'||
							'CD_CGC_EMITENTE_W='||cd_cgc_emitente_w||';'||
							'CD_SERIE_NF_W='||cd_serie_nf_p||';'||
							'NR_NOTA_FISCAL_W='||nr_nota_fiscal_w||';'||
							'NR_SEQUENCIA_NF_W='||nr_sequencia_nf_w||';'||
							'DS_ERRO_W='||ds_erro_w);
end;

cd_evento_nf_w		:= 16;
ds_historico_nf_w		:= Wheb_mensagem_pck.get_Texto(307131); /*'Digita��o da nota.';*/
gerar_historico_nota_fiscal(nr_sequencia_w, nm_usuario_p, cd_evento_nf_w, ds_historico_nf_w);

insert into conta_paciente_nf(
	nr_interno_conta,
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	cd_perfil)
select	a.nr_interno_conta,
	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	obter_perfil_ativo
from	w_contas_selecionadas_prot a
where	nm_usuario = nm_usuario_p
and	ie_possui_nf = 'N';

/* Verificar se vinculou NF em alguma conta */
select	count(*)
into	nr_contas_w
from	conta_paciente_nf
where	nr_sequencia = nr_sequencia_w;
if	(nr_contas_w = 0) then	
	/*Nenhuma conta foi associada*/
	wheb_mensagem_pck.exibir_mensagem_abort(173251);
end if;

/* Tratar itens da nota fiscal */
qt_item_nf_w		:= 0;
qt_item_calc_w		:= 0;
qt_item_neg_w		:= 0;
vl_proc_mat_acum_w	:= 0;

if	(nvl(cd_procedimento_p,0) > 0) and
	(nvl(ie_origem_proced_p,0) > 0) then
	begin

	open c03;
	loop
	fetch C03 into
		ie_tipo_ordem_w,
		ie_origem_proced_w,
		cd_procedimento_w,
		cd_material_w,
		cd_unidade_medida_w,
		qt_proc_mat_w,
		vl_proc_mat_w,
		vl_desc_item_w,
		nr_seq_proc_interno_w;
	exit when C03%notfound;
		begin
		qt_item_calc_w		:= qt_item_calc_w + 1;
		vl_proc_mat_w		:= vl_proc_mat_w + vl_desc_item_w;
		vl_proc_mat_acum_w	:= vl_proc_mat_acum_w + vl_proc_mat_w;
		vl_desc_acum_w		:= vl_desc_acum_w + vl_desc_item_w;
		end;
	end loop;
	close C03;

	qt_proc_mat_w		:= 1;
	vl_unitario_item_nf_w	:= vl_proc_mat_acum_w;
	vl_proc_mat_w		:= vl_proc_mat_acum_w;
	vl_desc_item_w		:= vl_desc_acum_w;
	cd_material_w		:= null;
	ie_origem_proced_w	:= nvl(ie_origem_proced_p,1);
	cd_procedimento_w	:= cd_procedimento_p;

	ie_tipo_conta_w	:= 3;
	if	(cd_centro_custo_w is null) then
		ie_tipo_conta_w	:= 2;
	end if;

	if	(cd_procedimento_w > 0) then
		begin
		define_conta_procedimento(
			cd_estabelecimento_p,
			cd_procedimento_w,
			ie_origem_proced_w,
			ie_tipo_conta_w,
			0,
			0,
			0,
			0,
			ie_tipo_convenio_w,
			0,
			0,
			trunc(sysdate),
			cd_conta_contabil_w,
			cd_centro_custo_w,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			nr_seq_proc_interno_w);
		end;
	end if;

	/* Tratar Conta Financeira */
	if 	(ie_conta_financ_nf_w = 'S') then
		begin		
		obter_conta_financeira(	'E',
					cd_estabelecimento_p,
					cd_material_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					null,
					cd_convenio_w,
					cd_cgc_w,
					cd_centro_custo_w,
					cd_conta_financ_w,
					null, 
					cd_operacao_nf_p,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);
		end;
	end if;

	insert into nota_fiscal_item
		(nr_sequencia,						cd_estabelecimento,
		cd_cgc_emitente,						cd_serie_nf,
		nr_nota_fiscal,						nr_sequencia_nf,
		nr_item_nf,						cd_natureza_operacao,
		dt_atualizacao,						nm_usuario,
		qt_item_nf,						vl_unitario_item_nf,
		vl_total_item_nf,						vl_frete,
		vl_desconto,						vl_despesa_acessoria,
		vl_desconto_rateio,					vl_seguro,
		vl_liquido,						cd_material,
		cd_procedimento,						cd_local_estoque,
		ds_observacao,						ie_origem_proced,
		nr_seq_conta_financ,					ds_complemento,
		nr_atendimento,						cd_conta_contabil,
		cd_sequencia_parametro)
	values	(
		nr_sequencia_w,						cd_estabelecimento_p,
		cd_cgc_emitente_w,					cd_serie_nf_p,
		nr_nota_fiscal_w,						nr_sequencia_nf_w,
		1,							cd_nat_oper_nf_p,
		sysdate,							nm_usuario_p,
		qt_proc_mat_w,						vl_unitario_item_nf_w,
		vl_proc_mat_w,						0,
		0,							0,
		0,							0,
		vl_proc_mat_w,						decode(cd_material_w, 0, null, cd_material_w),
		decode(cd_procedimento_w, 0, null, cd_procedimento_w),	null,
		ds_observacao_w,						decode(ie_origem_proced_w, 0, null, ie_origem_proced_w),
		cd_conta_financ_w,					ds_complemento_p,
		null,							cd_conta_contabil_w,
		philips_contabil_pck.get_parametro_conta_contabil);

	end;
else
	begin

	qt_item_calc_w		:= 0;
	vl_desc_acum_w		:= 0;

	open c04;
	loop
	fetch c04 into
		cd_material_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		ds_sql_w,
		ie_lista_itens_w;
	exit when c04%notfound;
		begin

		qt_proc_mat_w		:= 1;
		vl_proc_mat_w		:= 0;
		vl_desc_item_w		:= 0;
		ds_retorno_www		:= 0;
		ds_sql_w			:= replace(ds_sql_w, ':NR_INTERNO_CONTA', 0);
		ds_sql_w			:= replace(ds_sql_w, ':NR_SEQ_PROTOCOLO', nvl(nr_seq_protocolo_p,0));
		ds_sql_w			:= replace(ds_sql_w, ':NR_SEQ_LOTE_PROTOCOLO', 0);
		ds_sql_w			:= replace(ds_sql_w, ':NR_SEQUENCIA_NF', nr_sequencia_w);

		if	(ie_lista_itens_w	= 'N') then
			begin

			c001 := dbms_sql.open_cursor;
			dbms_sql.parse(c001, ds_sql_w, dbms_sql.native);
			dbms_sql.define_column(c001, 1, ds_retorno_w, 15);

			begin
			dbms_sql.define_column(c001, 2, ds_retorno_ww, 15);
			exception when others then
				ds_retorno_ww	:= 1;
			end;

			begin
			dbms_sql.define_column(c001, 3, ds_retorno_www, 15);
			exception when others then
				ds_retorno_www	:= 0;
			end;

			vl_proc_mat_w	:= dbms_sql.execute(c001); 
			vl_proc_mat_w	:= dbms_sql.fetch_rows(c001);

			dbms_sql.column_value(c001, 1, ds_retorno_w);

			begin
			dbms_sql.column_value(c001, 2, ds_retorno_ww);
			exception when others then
				ds_retorno_ww	:= 1;
			end;

			begin
			dbms_sql.column_value(c001, 3, ds_retorno_www);
			exception when others then
				ds_retorno_www	:= 0;
			end;
			vl_desc_item_w	:= ds_retorno_www;
			dbms_sql.close_cursor(c001);

			if	(cd_material_w is null) and
				(cd_procedimento_w is null) then
				begin

				if	(nvl(to_number(ds_retorno_w),0) > 0) then
					vl_desc_acum_w	:= to_number(ds_retorno_w);
				end if;

				end;
			else
				begin
				vl_proc_mat_w		:= to_number(ds_retorno_w);
				vl_proc_mat_w		:= vl_proc_mat_w + nvl(vl_desc_item_w,0);
				vl_unitario_item_nf_w	:= vl_proc_mat_w;
				vl_proc_mat_acum_w	:= vl_proc_mat_acum_w + vl_proc_mat_w;
				vl_desc_acum_w		:= nvl(vl_desc_acum_w,0) + nvl(vl_desc_item_w,0);
					
				if	(to_number(ds_retorno_ww) > 1) then
					qt_proc_mat_w		:= to_number(ds_retorno_ww);
					vl_unitario_item_nf_w	:= dividir(vl_proc_mat_w, to_number(ds_retorno_ww));
				end if;

				if 	(qt_item_calc_w is null) or (qt_proc_mat_w is null) then
					--(-20011,'Erro ao Gravar Item da Nota Fiscal - Qtde');
					wheb_mensagem_pck.exibir_mensagem_abort(173252);
				elsif	(vl_unitario_item_nf_w is null) or
					(vl_desc_item_w is null) or (vl_proc_mat_w is null) then
					--(-20011,'Erro ao Gravar Item da Nota Fiscal - Valor');
					wheb_mensagem_pck.exibir_mensagem_abort(173253);
				end if;

				if	(vl_proc_mat_w	<> 0) 		and
					(vl_unitario_item_nf_w	<> 0)	then
					begin

					ie_tipo_conta_w	:= 3;
					if	(cd_centro_custo_w is null) then
						ie_tipo_conta_w	:= 2;
					end if;

					if	(nvl(cd_material_w,0) <> 0) then
						define_conta_material(
							cd_estabelecimento_p,
							cd_material_w,
							ie_tipo_conta_w,
							0,
							0,
							0,
							ie_tipo_convenio_w,
							0,
							0,
							0,
							null,
							null,
							trunc(sysdate),
							cd_conta_contabil_w,
							cd_centro_custo_w,
							null);
					end if;
					
					if	(nvl(cd_procedimento_w,0) <> 0) then
						define_conta_procedimento(
							cd_estabelecimento_p,
							cd_procedimento_w,
							ie_origem_proced_w,
							ie_tipo_conta_w,
							0,
							0,
							0,
							0,
							ie_tipo_convenio_w,
							0,
							0,
							trunc(sysdate),
							cd_conta_contabil_w,
							cd_centro_custo_w,
							null,
							null);
					end if;
					
					/* Tratar conta financeira */
					if 	(ie_conta_financ_nf_w = 'S') then
						begin
						obter_conta_financeira(
							'E',
							cd_estabelecimento_p,
							cd_material_w,
							cd_procedimento_w,
							ie_origem_proced_w,
							null,
							cd_convenio_w,
							cd_cgc_p,
							cd_centro_custo_w,
							cd_conta_financ_w,
							null, 
							cd_operacao_nf_p,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null);
						end;
					end if;
					
					qt_item_calc_w			:= qt_item_calc_w + 1;

					insert into nota_fiscal_item(
						nr_sequencia, 			cd_estabelecimento,
						cd_cgc_emitente,			cd_serie_nf,
						nr_nota_fiscal,			nr_sequencia_nf,
						nr_item_nf,			cd_natureza_operacao,
						dt_atualizacao,			nm_usuario,
						qt_item_nf,			vl_unitario_item_nf,
						vl_total_item_nf,			vl_frete,
						vl_desconto,			vl_despesa_acessoria,
						vl_desconto_rateio,		vl_seguro,
						vl_liquido,			cd_material,
						cd_procedimento,			ie_origem_proced,
						cd_local_estoque,			ds_observacao,
						nr_seq_conta_financ,		ds_complemento,
						nr_atendimento,			cd_conta_contabil,
						cd_sequencia_parametro)
					values(	
						nr_sequencia_w,			cd_estabelecimento_p,
						cd_cgc_emitente_w,		cd_serie_nf_p,
						nr_nota_fiscal_w,			nr_sequencia_nf_w,
						qt_item_calc_w,			cd_nat_oper_nf_p,
						sysdate,				nm_usuario_p,
						qt_proc_mat_w,			vl_unitario_item_nf_w,
						vl_proc_mat_w,			0,
						0,				0,
						0,				0,
						(vl_proc_mat_w),			decode(cd_material_w, 0, null, cd_material_w),
						decode(cd_procedimento_w, 0, null, cd_procedimento_w),
						decode(ie_origem_proced_w, 0, null, ie_origem_proced_w),
						null,				ds_observacao_w,
						cd_conta_financ_w,		ds_complemento_p,
						null,				cd_conta_contabil_w,
						philips_contabil_pck.get_parametro_conta_contabil);
					end;
				end if;
				end;
			end if;

			end; /* (IE_LISTA_ITENS_W = 'N') */
		else
			begin

			c10_w	:= dbms_sql.open_cursor;
			dbms_sql.parse(c10_w,ds_sql_w,dbms_sql.native);
			dbms_sql.define_column(c10_w, 1, ds_retorno_w,15);

			begin
			dbms_sql.define_column(c10_w, 2, ds_retorno_ww, 15);
			exception when others then
				ds_retorno_ww		:= 1;
			end;

			begin
			dbms_sql.define_column(c10_w, 3, ds_retorno_www, 15);
			exception when others then
				ds_retorno_www		:= 0;
			end;

			begin
			dbms_sql.define_column(c10_w, 4, ie_origem_proced_ww, 15);
			exception when others then
				ie_origem_proced_ww	:= 0;
			end;

			ignore	:= dbms_sql.execute(c10_w);
			loop
			if 	(dbms_sql.fetch_rows(c10_w) > 0) then 
				begin

				ds_retorno_w		:= null;
				ds_retorno_ww		:= null;
				ds_retorno_www		:= null;
				qt_proc_mat_w		:= 1;
				dbms_sql.column_value(c10_w, 1, ds_retorno_w);

				begin
				dbms_sql.column_value(c10_w, 2, ds_retorno_ww);
				exception when others then
					ds_retorno_ww	:= 1;
				end;

				begin
				dbms_sql.column_value(c10_w, 3, ds_retorno_www);
				exception when others then
					ds_retorno_www	:= 0;
				end;

				begin
				dbms_sql.column_value(c10_w, 4, ie_origem_proced_ww);
				exception when others then
					ie_origem_proced_ww	:= 1;
				end;

				if	(cd_material_w is not null) then
					cd_material_w		:= to_number(ds_retorno_www);
				elsif	(cd_procedimento_w is not null) then
					cd_procedimento_w	:= to_number(ds_retorno_www);
					ie_origem_proced_w	:= to_number(ie_origem_proced_ww);
				end if;

				if	(cd_material_w is null) and
					(cd_procedimento_w is null) then
					begin

					if	(nvl(to_number(ds_retorno_w),0) > 0) then
						vl_desc_acum_w	:= to_number(ds_retorno_w);
					end if;

					end;
				else
					begin

					vl_proc_mat_w		:= to_number(ds_retorno_w);
					vl_unitario_item_nf_w	:= vl_proc_mat_w;
					vl_proc_mat_acum_w	:= vl_proc_mat_acum_w + vl_proc_mat_w;
					
					if	(to_number(ds_retorno_ww) <> 0) then
						qt_proc_mat_w		:= to_number(ds_retorno_ww);
						vl_unitario_item_nf_w	:= dividir(vl_proc_mat_w, to_number(ds_retorno_ww));
					end if;

					if 	(qt_item_calc_w is null) or
						(qt_proc_mat_w is null) then
						--(-20011,'Erro ao Gravar Item da Nota Fiscal - Qtde');
						wheb_mensagem_pck.exibir_mensagem_abort(173252);
					elsif	(vl_unitario_item_nf_w is null) or
						(vl_desc_item_w is null) or
						(vl_proc_mat_w is null) then
						--(-20011,'Erro ao Gravar Item da Nota Fiscal - Valor');
						wheb_mensagem_pck.exibir_mensagem_abort(173253);
					end if;

					if	(vl_proc_mat_w <> 0)  and
						(vl_unitario_item_nf_w <> 0) then
						begin

						ie_tipo_conta_w	:= 3;
						if	(cd_centro_custo_w is null) then
							ie_tipo_conta_w	:= 2;
						end if;

						if	(nvl(cd_material_w,0) <> 0) then
							begin

							define_conta_material(
								cd_estabelecimento_p,
								cd_material_w,
								ie_tipo_conta_w,
								0,
								0,
								0,
								ie_tipo_convenio_w,
								0,
								0,
								0,
								null,
								null,
								trunc(sysdate),
								cd_conta_contabil_w,
								cd_centro_custo_w,
								null);

							end;
						end if;
						
						if	(nvl(cd_procedimento_w,0) <> 0) then
							begin

							define_conta_procedimento(
								cd_estabelecimento_p,
								cd_procedimento_w,
								ie_origem_proced_w,
								ie_tipo_conta_w,
								0,
								0,
								0,
								0,
								ie_tipo_convenio_w,
								0,
								0,
								trunc(sysdate),
								cd_conta_contabil_w,
								cd_centro_custo_w,
								null,
								null);

							end;
						end if;

						/*Tratar conta financeira */
						if 	(ie_conta_financ_nf_w = 'S') then
							begin
							
							obter_conta_financeira(
								'E',
								cd_estabelecimento_p,
								cd_material_w,
								cd_procedimento_w,
								ie_origem_proced_w,
								null,
								cd_convenio_w,
								cd_cgc_p,
								cd_centro_custo_w,
								cd_conta_financ_w,
								null, 
								cd_operacao_nf_p,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null);

							end;
						end if;

						qt_item_calc_w		:= qt_item_calc_w + 1;

						insert into nota_fiscal_item(
							nr_sequencia, 			cd_estabelecimento,
							cd_cgc_emitente,			cd_serie_nf,
							nr_nota_fiscal,			nr_sequencia_nf,
							nr_item_nf,			cd_natureza_operacao,
							dt_atualizacao,			nm_usuario,
							qt_item_nf,			vl_unitario_item_nf,
							vl_total_item_nf,			vl_frete,
							vl_desconto,			vl_despesa_acessoria,
							vl_desconto_rateio,		vl_seguro,
							vl_liquido,			cd_material,
							cd_procedimento,			ie_origem_proced,
							cd_local_estoque,			ds_observacao,
							nr_seq_conta_financ,		ds_complemento,
							nr_atendimento,			cd_conta_contabil,
							cd_sequencia_parametro)
						values(	
							nr_sequencia_w,			cd_estabelecimento_p,
							cd_cgc_emitente_w,		cd_serie_nf_p,
							nr_nota_fiscal_w,			nr_sequencia_nf_w,
							qt_item_calc_w,			cd_nat_oper_nf_p,
							sysdate,				nm_usuario_p,
							qt_proc_mat_w,			vl_unitario_item_nf_w,
							vl_proc_mat_w,			0,
							vl_desc_item_w,			0,
							0,				0,
							vl_proc_mat_w,			decode(cd_material_w, 0, null, cd_material_w),
							decode(cd_procedimento_w, 0, null, cd_procedimento_w),
							decode(ie_origem_proced_w, 0, null, ie_origem_proced_w),
							null,				ds_observacao_w,
							cd_conta_financ_w,		ds_complemento_p,
							null,				cd_conta_contabil_w,
							philips_contabil_pck.get_parametro_conta_contabil);
						end;
					end if; /* (vl_proc_mat_w <> 0) */

					end;
				end if; /* (cd_material_w is null) */

				end;
			else
				exit;
			end if;  /* (dbms_sql.fetch_rows(c10_w) > 0) */

			end loop;

			dbms_sql.close_cursor(c10_w);

			end;
		end if; /* (IE_LISTA_ITENS_W = 'N') */

		end;
	end loop;
	close c04;

	end;
end if;

qt_itens_nf_w := 0;
select	count(*)
into	qt_itens_nf_w
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_w;
if	(qt_itens_nf_w = 0) then
	--(-20001,'Nenhum item gerado para nota fiscal');
	wheb_mensagem_pck.exibir_mensagem_abort(173254);
end if;

select	nvl(max(ie_nf_guia_partic),'S'),
	nvl(max(ie_calcula_nf),'N')
into	ie_nf_guia_partic_w,
	ie_calcula_nf_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;

obter_param_usuario(-80, 27, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_regra_calcula_nf_w);

/* Locar a tabela e obter o numero da nota fiscal */
lock table serie_nota_fiscal in exclusive mode;
select	nr_ultima_nf + 1,
	ie_numero_nota
into	nr_nota_fiscal_w,
	ie_numero_nota_w
from	serie_nota_fiscal
where	cd_serie_nf 		= cd_serie_nf_p
and	cd_estabelecimento 	= cd_estabelecimento_p;

if	(nr_nota_fiscal_p <> 0) then
	nr_nota_fiscal_w		:= nr_nota_fiscal_p;
end if;

select	count(*)
into	qt_reg_w
from	nota_fiscal_aidf
where	cd_estabelecimento 	= cd_estabelecimento_p
and	cd_serie_nf 		= cd_serie_nf_p;

if	(qt_reg_w > 0) then
	select	count(*)
	into	qt_reg_w
	from	nota_fiscal_aidf
	where	cd_estabelecimento 	= cd_estabelecimento_p
	and	cd_serie_nf 		= cd_serie_nf_p
	and	nr_nota_fiscal_w >= nr_nota_ini 
	and 	nr_nota_fiscal_w <= nr_nota_fim;
	
	if	(qt_reg_w = 0) then
		/*(-20011,	'Sem autoriza��o para informar este n�mero de nota fiscal (' || nr_nota_fiscal_w || '), ' || chr(10) || chr(13) || 
						'verifique o cadastro de autoriza��es(AIDF) nos cadastros de estoque.');*/
		wheb_mensagem_pck.exibir_mensagem_abort(173255,'NR_NOTA_FISCAL_W='||nr_nota_fiscal_w);
	end if;
end if;

if	(ie_atualiza_serie_nf_saida_w = 'S') then
	begin

	if	(ie_numero_nota_w = 'T') and
		(cd_setor_atendimento_p > 0) then
		begin
			
		select	count(*)
		into	qt_reg_w
		from	serie_nota_fiscal_setor
		where	cd_serie_nf 		= cd_serie_nf_p
		and	cd_estabelecimento 	= cd_estabelecimento_p
		and	cd_setor_atendimento 	= cd_setor_atendimento_p;
			
		if	(qt_reg_w > 0) then
			begin
				
			select	nr_ultima_nf +1
			into	nr_nota_fiscal_w
			from	serie_nota_fiscal_setor
			where	cd_serie_nf = cd_serie_nf_p
			and	cd_Estabelecimento = cd_estabelecimento_p
			and	cd_setor_atendimento = cd_setor_atendimento_p;
						
			if	((nvl(ie_regra_calcula_nf_w,'D') = 'S') or
				((nvl(ie_regra_calcula_nf_w,'D') = 'D') and (ie_calcula_nf_w = 'S')) or
				((nvl(ie_regra_calcula_nf_w,'D') = 'C') and (ie_calcula_nf_conv_w = 'S'))) then
				begin
				
				update	serie_nota_fiscal_setor
				set	nr_ultima_nf 		= nr_nota_fiscal_w
				where	cd_serie_nf 		= cd_serie_nf_p
				and	cd_estabelecimento 	= cd_estabelecimento_p
				and	cd_setor_atendimento	= cd_setor_atendimento_p;

				end;
			end if;

			end;
		end if;

		end;
	else
		begin

		if	(nvl(ie_estab_serie_nf_w,'N') = 'S') then
			update	serie_nota_fiscal
			set	nr_ultima_nf 		= nr_nota_fiscal_w
			where	cd_serie_nf 		= cd_serie_nf_p
			and	cd_estabelecimento in (select	z.cd_estabelecimento
							from	estabelecimento z
							where	z.cd_cgc = cd_cgc_emitente_w);
		else
			update	serie_nota_fiscal
			set	nr_ultima_nf 		= nr_nota_fiscal_w
			where	cd_serie_nf 		= cd_serie_nf_p
			and	cd_estabelecimento 	= cd_estabelecimento_p;
		end if;


		end;
	end if;

	end;
end if;

/* Atualizar os totais da nota fiscal */
update	nota_fiscal
set	vl_mercadoria	= vl_proc_mat_acum_w,
	vl_total_nota	= vl_proc_mat_acum_w - (vl_desc_acum_w + vl_descontos_w),
	vl_descontos	= vl_desc_acum_w + vl_descontos_w,
	vl_ipi		= vl_ipi_w,
	nr_nota_fiscal	= nr_nota_fiscal_w
where	nr_sequencia 	= nr_sequencia_w;

update	nota_fiscal_item
set	nr_nota_fiscal	= nr_nota_fiscal_w
where	nr_sequencia 	= nr_sequencia_w;

gerar_imposto_nf(nr_sequencia_w, nm_usuario_p, nr_seq_sit_trib_p, null);

atualiza_total_nota_fiscal(nr_sequencia_w,nm_usuario_p);

if	(nvl(cd_condicao_pagamento_p,0) > 0) then
	begin

	select	ie_forma_pagamento
	into	ie_forma_pagamento_w
	from	condicao_pagamento
	where	cd_condicao_pagamento = cd_condicao_pagamento_p;

	end;
end if;

if	(nvl(ie_forma_pagamento_w,0) = 10) then	/*Conforme vencimentos, buscar o vencimento do protocolo*/
	begin

	select	max(dt_vencimento)
	into	dt_vencimento_prot_w
	from	protocolo_convenio
	where	nr_seq_protocolo = nr_seq_protocolo_p;
	
	select	vl_total_nota
	into	vl_total_nota_w
	from	nota_fiscal
	where	nr_sequencia = nr_sequencia_w;
	
	insert into nota_fiscal_venc(
			nr_sequencia,
			cd_estabelecimento,
			cd_cgc_emitente,
			cd_serie_nf,
			nr_sequencia_nf,
			dt_vencimento,
			vl_vencimento,
			dt_atualizacao,
			nm_usuario,
			nr_nota_fiscal,
			ie_origem)
		values(	nr_sequencia_w,
			cd_estabelecimento_p,
			cd_cgc_emitente_w,
			cd_serie_nf_p,
			nr_sequencia_nf_w,
			nvl(dt_vencimento_prot_w, dt_emissao_p),
			vl_total_nota_w,
			sysdate,
			nm_usuario_p,
			nr_nota_fiscal_w,
			'N');
	end;
else
	gerar_nota_fiscal_venc(nr_sequencia_w, dt_emissao_p);
end if;

nr_sequencia_p		:= nr_sequencia_w;

commit;

end gerar_nf_conta_selec_prot;
/
