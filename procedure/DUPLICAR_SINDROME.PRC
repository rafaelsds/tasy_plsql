create or replace
procedure Duplicar_sindrome(	nr_seq_sindrome_p	Number,
								nm_usuario_p		Varchar2) IS 

nr_seq_sindrome_w 		number(10);
nr_seq_sinais_w 		number(10);
cd_estabelecimento_w	number(4);
cd_profissional_w		varchar2(10);
			
Cursor C01 is
	select	*
	from	pac_sinal_sindrome
	where	nr_seq_paciente = nr_seq_sindrome_p
	order by nr_sequencia;

c01_w	c01%rowtype; 
			
BEGIN

if	(nr_seq_sindrome_p > 0)  then

	Select 	paciente_sindrome_seq.nextval
	into	nr_seq_sindrome_w
	from 	dual;
	
	cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
	
	select	max(cd_pessoa_fisica)
	into	cd_profissional_w
	from	usuario
	where	nm_usuario	= nm_usuario_p;

	insert into paciente_sindrome(
						nr_sequencia, 
						 cd_estabelecimento,
						 dt_atualizacao,
						 nm_usuario,
						 dt_atualizacao_nrec,
						 nm_usuario_nrec,
						 cd_pessoa_fisica,
						 cd_profissional,
						 qt_idade_materna,
						 nr_seq_sindrome,
						 ie_etiologia,
						 qt_idade_paterna,
						 dt_avaliacao,
						 ie_recorrencia,
						 ie_cosanguinidade,
						 ie_gemelaridade,
						 ds_observacao,
						 ie_situacao)
				select	 nr_seq_sindrome_w, 
						 cd_estabelecimento_w,
						 sysdate,
						 nm_usuario_p,
						 sysdate,
						 nm_usuario_p,
						 cd_pessoa_fisica,
						 cd_profissional_w,
						 qt_idade_materna,
						 nr_seq_sindrome,
						 ie_etiologia,
						 qt_idade_paterna,
						 sysdate,
						 ie_recorrencia,
						 ie_cosanguinidade,
						 ie_gemelaridade,
						 ds_observacao,
						 'A'
				from	paciente_sindrome
				where	nr_sequencia	= nr_seq_sindrome_p;
	
	commit;
	
	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		begin		
				
		insert into  pac_sinal_sindrome (	 nr_sequencia, 
											 dt_atualizacao, 
											 dt_atualizacao_nrec,
											 nm_usuario,
											 nm_usuario_nrec,
											 nr_seq_regiao, 
											 nr_seq_sinal,
											 nr_seq_paciente)
								values (	 pac_sinal_sindrome_seq.nextval, 
											 sysdate,
											 sysdate,
											 nm_usuario_p,
											 nm_usuario_p,
											 c01_w.nr_seq_regiao, 
											 c01_w.nr_seq_sinal,
											 nr_seq_sindrome_w);
											 
											 
		commit;
		
		end;
	end loop;
	close C01;
	
end if;	

END Duplicar_sindrome;
/