create or replace
function obter_se_pac_reavaliacao( nr_atendimento_p	 number)
			           return varchar2 is

								
dt_inicio_reav_w				date;
dt_fim_reav_w				date;
															
begin


Select 	max(dt_inicio_reavaliacao),
	max(dt_fim_reavaliacao)
into	dt_inicio_reav_w,
	dt_fim_reav_w
from	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;


If (dt_inicio_reav_w is not null) then
	begin
		if(dt_inicio_reav_w < sysdate) then
			if	(dt_fim_reav_w is not null) then
				if	(dt_fim_reav_w > sysdate) then
					return 'S';
				else
					return 'N';
				end if;
			else
				return 'S';
			end if;
		else
			return 'N';
		end if;
	end;
end if;

return 'N';

end obter_se_pac_reavaliacao;
/
