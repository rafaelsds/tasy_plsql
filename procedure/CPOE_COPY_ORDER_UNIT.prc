CREATE OR REPLACE PROCEDURE CPOE_COPY_ORDER_UNIT (
                 nr_seq_cpoe_order_unit_p number,
                 nm_usuario_p varchar2,
                 nr_seq_new_cpoe_order_unit_p out number)
AS
  nr_seq_cpoe_order_unit_w      cpoe_order_unit.nr_sequencia%type;
  nr_seq_cpoe_rp_w              cpoe_rp.nr_sequencia%type;
  
  cursor cpoe_order_unit_sel is
     select * 
       from cpoe_order_unit ont
      where ont.nr_sequencia = nr_seq_cpoe_order_unit_p;

  cursor cpoe_rp_sel is
    select * 
      from cpoe_rp crp
     where crp.nr_seq_cpoe_order_unit = nr_seq_cpoe_order_unit_p
  order by crp.nr_sequencia_rp;

begin
   
   for cpoeOrderUnit_cur in cpoe_order_unit_sel loop
      select cpoe_order_unit_seq.nextval
	  into nr_seq_cpoe_order_unit_w
      from dual;
		 
      nr_seq_new_cpoe_order_unit_p := nr_seq_cpoe_order_unit_w;

      insert into cpoe_order_unit 
         (nr_sequencia,
          dt_atualizacao,
          nm_usuario,
          dt_atualizacao_nrec,
          nm_usuario_nrec,
          nr_order_unit,
          si_type_of_prescription,
          si_cpoe_type_of_item,
          nr_seq_cpoe_tipo_pedido,
          cd_pessoa_fisica,
          nr_atendimento,
          ie_situacao,
          cd_setor_execucao,
          nr_seq_nais_insurance,
          ie_drug_info,
          si_same_dose,
          si_drug_adjustment,
          si_provide_info_insurance,
          si_dispense_med_insurance,
          ie_tipo_receita,
          id_dispense_for_question,
          id_provide_info_medical,
          cd_setor_coleta,
          nr_seq_prev_order_unit,
          dt_start,
          dt_end )
       values ( 
          nr_seq_cpoe_order_unit_w,
          sysdate,
          nm_usuario_p,
          sysdate,
          nm_usuario_p,
          cpoeOrderUnit_cur.nr_order_unit,      
          cpoeOrderUnit_cur.si_type_of_prescription,
          cpoeOrderUnit_cur.si_cpoe_type_of_item,
          cpoeOrderUnit_cur.nr_seq_cpoe_tipo_pedido,
          cpoeOrderUnit_cur.cd_pessoa_fisica,
          cpoeOrderUnit_cur.nr_atendimento,
          'A',
          cpoeOrderUnit_cur.Cd_Setor_Execucao,
          cpoeOrderUnit_cur.Nr_Seq_Nais_Insurance,
          cpoeOrderUnit_cur.Ie_Drug_Info,
          cpoeOrderUnit_cur.Si_Same_Dose,
          cpoeOrderUnit_cur.Si_Drug_Adjustment,
          cpoeOrderUnit_cur.Si_Provide_Info_Insurance,
          cpoeOrderUnit_cur.si_dispense_med_insurance,
          cpoeOrderUnit_cur.ie_tipo_receita,
          cpoeOrderUnit_cur.id_dispense_for_question,
          cpoeOrderUnit_cur.id_provide_info_medical,
          cpoeOrderUnit_cur.cd_setor_coleta,
          cpoeOrderUnit_cur.nr_sequencia,
          cpoeOrderUnit_cur.dt_start,
          cpoeOrderUnit_cur.dt_end);
      commit;
       
  
      for cpoerp_cur in cpoe_rp_sel loop
         select cpoe_rp_seq.nextval
		 into nr_seq_cpoe_rp_w
         from dual;
		 
		 cpoerp_cur.dt_inicio := to_date(trunc(sysdate)|| to_char(nvl(cpoerp_cur.dt_inicio, SYSDATE), ' hh24:mi:ss'), 'dd/mm/yyy hh24:mi:ss');
    
         if (cpoerp_cur.dt_inicio < sysdate) then
            cpoerp_cur.dt_inicio := cpoerp_cur.dt_inicio + 1;
         end if;
         
         cpoerp_cur.dt_fim := (cpoerp_cur.dt_inicio + nvl(cpoerp_cur.qt_dias_padrao, 1)) - 1/86400;
        
         insert into cpoe_rp (
            nr_sequencia,
            nr_sequencia_rp,
            ie_via_aplicacao,
            dt_atualizacao,
            nm_usuario,
            dt_atualizacao_nrec,
            nm_usuario_nrec,
            cd_intervalo,
            ds_horarios,
            dt_inicio,
            dt_fim,
            nr_seq_cpoe_order_unit,
            qt_dias_padrao,
            ie_segunda,
            ie_terca,
            ie_quarta,
            ie_quinta,
            ie_sexta,
            ie_sabado,
            ie_domingo,
            nr_seq_start_period,
            nr_seq_subgrupo_interval,
            nr_seq_grupo_interval,
            ie_enable_end_dt,
            si_temporary_admin,
            ie_allow_execution,
            qt_operacao,
            qt_dosagem,
            qt_tempo_aplicacao,
            nr_etapas,
            ie_bomba_infusao,
            qt_hora_fase,
            ie_tipo_solucao,
            ie_tipo_dosagem,
            ie_ref_calculo,
            ie_drip_shot,
            nr_seq_prev_rp,
			ie_administracao)  
         values(
            cpoe_rp_seq.nextval,
            cpoerp_cur.nr_sequencia_rp,
            cpoerp_cur.ie_via_aplicacao,
            sysdate,
            nm_usuario_p,
            sysdate,
            nm_usuario_p,
            cpoerp_cur.cd_intervalo,
            cpoerp_cur.ds_horarios,
            cpoerp_cur.dt_inicio,
            cpoerp_cur.dt_fim,
            nr_seq_cpoe_order_unit_w,
            cpoerp_cur.qt_dias_padrao,
            cpoerp_cur.ie_segunda,
            cpoerp_cur.ie_terca,
            cpoerp_cur.ie_quarta,
            cpoerp_cur.ie_quinta,
            cpoerp_cur.ie_sexta,
            cpoerp_cur.ie_sabado,
            cpoerp_cur.ie_domingo,
            cpoerp_cur.nr_seq_start_period,
            cpoerp_cur.nr_seq_subgrupo_interval,
            cpoerp_cur.nr_seq_grupo_interval,
            cpoerp_cur.ie_enable_end_dt,
            cpoerp_cur.si_temporary_admin,
            cpoerp_cur.ie_allow_execution,
            cpoerp_cur.qt_operacao,
            cpoerp_cur.qt_dosagem,
            cpoerp_cur.qt_tempo_aplicacao,
            cpoerp_cur.nr_etapas,
            cpoerp_cur.ie_bomba_infusao,
            cpoerp_cur.qt_hora_fase,
            cpoerp_cur.ie_tipo_solucao,
            cpoerp_cur.ie_tipo_dosagem,
            cpoerp_cur.ie_ref_calculo,
            cpoerp_cur.ie_drip_shot,
            cpoerp_cur.nr_sequencia,
			cpoerp_cur.ie_administracao
         );

        commit;
      end loop;

  end loop;

end;
/
