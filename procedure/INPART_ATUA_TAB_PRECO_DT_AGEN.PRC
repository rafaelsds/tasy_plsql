create or replace
procedure inpart_atua_tab_preco_dt_agen(cd_material_p		number,
				    dt_vigencia_p		date,
				    cd_tab_preco_mat_p	number,
				    cd_estabelecimento_p 	number,
				    vl_preco_p		number,
				    nm_usuario_p		Varchar2) is 

vl_preco_venda_w		number(13,4):= 0;
dt_vigencia_w		date;
qt_existe_w		number(10,0);
cd_moeda_w		number(3,0):= 1;
qt_existe_dia_w		number(10,0);
				    
cursor	c01 is
	select	/*+index (a prcmate_pk) */
		a.vl_preco_venda,
		a.dt_inicio_vigencia,
		a.cd_moeda
	from	preco_material 		a	
	where	a.cd_tab_preco_mat	= cd_tab_preco_mat_p
	and	a.cd_estabelecimento 	= cd_estabelecimento_p
	and	a.cd_material        	= cd_material_p
	and	nvl(a.ie_situacao,'A')	= 'A'
	and	a.dt_inicio_vigencia	<= dt_vigencia_p
	and	nvl(a.ie_preco_venda, 'S') = 'S'
	order by
		a.dt_inicio_vigencia;
				    
begin


/* 1 - Pegar as informa��es do pre�o vigente do item */
open C01;
loop
fetch C01 into	
	vl_preco_venda_w,
	dt_vigencia_w,
	cd_moeda_w;
exit when C01%notfound;
	begin	
	vl_preco_venda_w		:= vl_preco_venda_w;
	dt_vigencia_w			:= dt_vigencia_w;
	cd_moeda_w			:= cd_moeda_w;
	end;
end loop;
close C01;

/* 2 - Verificar se o valor que est� vindo da integra��o � maior ou menor que o praticado*/
if	(nvl(vl_preco_p,0) > vl_preco_venda_w) then

	/* 2.1 - Se o valor for maior, ent�o verifica-se no m�s que vem o valor novo existe o registro no dia 01 do m�s*/
	select	count(*)
	into	qt_existe_w
	from	preco_material
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_tab_preco_mat	= cd_tab_preco_mat_p
	and	cd_material		= cd_material_p
	and	dt_inicio_vigencia 	= trunc(dt_vigencia_p);
	
	/* 2.2 - Se n�o tem o registro no primeiro dia daquele m�s ent�o cria-se um novo registro*/
	if	(qt_existe_w = 0) then
		
		insert into preco_material(
			cd_estabelecimento,
			cd_tab_preco_mat,
			cd_material, 
			dt_inicio_vigencia,
			vl_preco_venda,
			cd_moeda,
			ie_brasindice,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			ie_preco_venda)
		values(
			cd_estabelecimento_p,
			cd_tab_preco_mat_p,
			cd_material_p,
			trunc(dt_vigencia_p),
			nvl(vl_preco_p,0),
			cd_moeda_w,
			'N',
			sysdate,
			nm_usuario_p,
			'A',
			'S');
			
	elsif	(qt_existe_w > 0) then	
		
		update 	preco_material
		set	vl_preco_venda 		= nvl(vl_preco_p,0),
			dt_atualizacao		= sysdate
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	cd_tab_preco_mat	= cd_tab_preco_mat_p
		and	cd_material		= cd_material_p
		and	dt_inicio_vigencia 	= trunc(dt_vigencia_p);   -- Retirado ap�s as conversas por telefone com Daniela Z e retorno da Lilian (S�rio) na OS
		
		select	count(*)
		into	qt_existe_dia_w
		from	preco_material
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	cd_tab_preco_mat	= cd_tab_preco_mat_p
		and	cd_material		= cd_material_p
		and	trunc(dt_inicio_vigencia) 	= trunc(dt_vigencia_p);
		
		if	(qt_existe_dia_w = 0) then
		
			insert into preco_material(
				cd_estabelecimento,
				cd_tab_preco_mat,
				cd_material, 
				dt_inicio_vigencia,
				vl_preco_venda,
				cd_moeda,
				ie_brasindice,
				dt_atualizacao,
				nm_usuario,
				ie_situacao,
				ie_preco_venda)
			values(
				cd_estabelecimento_p,
				cd_tab_preco_mat_p,
				cd_material_p,
				trunc(dt_vigencia_p),
				nvl(vl_preco_p,0),
				cd_moeda_w,
				'N',
				sysdate,
				nm_usuario_p,
				'A',
				'S');
				
		end if;		
	end if;	
	
else	/* 3 - O valor que est� vindo da integra��o � menor que o praticado.. 

	/* 3.1 - Lan�ar o item na tabela com data que vem da integra��o, antes verificar se n�o existe um j�*/
	select	count(*)
	into	qt_existe_w
	from	preco_material
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_tab_preco_mat	= cd_tab_preco_mat_p
	and	cd_material		= cd_material_p
	and	trunc(dt_inicio_vigencia) = trunc(dt_vigencia_p);

	/* 2.2 - Se n�o tem o registro no primeiro dia daquele m�s ent�o cria-se um novo registro*/
	if	(qt_existe_w = 0) then
		
		insert into preco_material(
			cd_estabelecimento,
			cd_tab_preco_mat,
			cd_material, 
			dt_inicio_vigencia,
			vl_preco_venda,
			cd_moeda,
			ie_brasindice,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			ie_preco_venda)
		values(
			cd_estabelecimento_p,
			cd_tab_preco_mat_p,
			cd_material_p,
			trunc(dt_vigencia_p),
			nvl(vl_preco_p,0),
			cd_moeda_w,
			'N',
			sysdate,
			nm_usuario_p,
			'A',
			'S');
			
	elsif	(qt_existe_w > 0) then	/* 3.3 - Se j�  tem o registro no dia vem vem a integra��o ent�o atualiza-se  o registro, pois quando isso acontecer, seria um erro segundo conversa com Lilian S�rio/Daniela Z.*/
		
		update 	preco_material
		set	vl_preco_venda 		= nvl(vl_preco_p,0),
			dt_atualizacao		= sysdate
		where	cd_estabelecimento	= cd_estabelecimento_p
		and	cd_tab_preco_mat	= cd_tab_preco_mat_p
		and	cd_material		= cd_material_p
		and	dt_inicio_vigencia      = trunc(dt_vigencia_p);
		
	end if;	
	
end if;

commit;

end inpart_atua_tab_preco_dt_agen;
/