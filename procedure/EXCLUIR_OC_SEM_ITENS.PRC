create or replace
procedure excluir_oc_sem_itens(	cd_estabelecimento_p		varchar2) is 

nr_ordem_compra_w		number(10);
nr_registros_w			number(5);

cursor c01 is
select	a.nr_ordem_compra
from	ordem_compra a
where	not exists (select b.nr_ordem_compra from ordem_compra_item b where b.nr_ordem_compra = a.nr_ordem_compra)
and	a.cd_estabelecimento = cd_estabelecimento_p;

begin

open c01;
loop
fetch c01 into
	nr_ordem_compra_w;
exit when C01%notfound;
	begin

	update	cot_compra_forn_item
	set	nr_ordem_compra = null
	where	nr_ordem_compra = nr_ordem_compra_w;

	update	nota_fiscal
	set	nr_ordem_compra = null
	where	nr_ordem_compra = nr_ordem_compra_w;

	update	nota_fiscal_item
	set	nr_ordem_compra = null,
		nr_item_oci = null
	where	nr_ordem_compra = nr_ordem_compra_w;

	update	ordem_compra
	set	nr_ordem_agrup = null
	where	nr_ordem_agrup = nr_ordem_compra_w;
	
	delete from ordem_compra_adiant_pago where nr_ordem_compra = nr_ordem_compra_w;	
	delete from ordem_compra where nr_ordem_compra = nr_ordem_compra_w;
	end;
end loop;
close c01;

commit;

end excluir_OC_sem_itens;
/