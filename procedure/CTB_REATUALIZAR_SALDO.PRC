create or replace
procedure ctb_reatualizar_saldo(
    nr_seq_mes_p    number,
    cd_estabelecimento_p  number,
    nm_usuario_p    varchar2,
    ds_erro_p   out Varchar2) is


nr_seq_mes_ant_w    ctb_mes_ref.nr_sequencia%type;
cd_empresa_w      number(04,0);
dt_referencia_w     date;
dt_abertura_w     date;
dt_consistencia_w   date;
dt_fechamento_w     date;
ds_log_estab_w      varchar2(255);
ds_erro_w     varchar2(4000);
qt_contador_w     number(10) := 0;
ds_exception_w      varchar2(4000);

cursor  c01 is
  select  nr_lote_contabil
  from  lote_contabil
  where nr_seq_mes_ref  = nr_seq_mes_p
  and cd_estabelecimento = nvl(cd_estabelecimento_p, cd_estabelecimento);

type c01_type is table of c01%rowtype;
c01_regs_w c01_type;

begin
gravar_processo_longo(wheb_mensagem_pck.get_texto(281466) ,'CTB_REATUALIZAR_SALDO',qt_contador_w);

select  cd_empresa,
  dt_referencia,
  dt_abertura,
  dt_fechamento
into  cd_empresa_w,
  dt_referencia_w,
  dt_abertura_w,
  dt_fechamento_w
from  ctb_mes_ref a
where nr_sequencia  = nr_seq_mes_p;

if  (dt_abertura_w is null)  then
  wheb_mensagem_pck.exibir_mensagem_abort(207401);
end if;

if  (dt_fechamento_w is not null) then
  wheb_mensagem_pck.exibir_mensagem_abort(207399);
end if;

select  nvl(max(nr_sequencia),0)
into  nr_seq_mes_ant_w
from  ctb_mes_ref
where cd_empresa  = cd_empresa_w
and pkg_date_utils.start_of(dt_referencia,'MONTH',0) = pkg_date_utils.start_of(pkg_date_utils.add_month(dt_referencia_w, -1,0), 'MONTH',0);

if  (nr_seq_mes_ant_w = 0) then
  wheb_mensagem_pck.exibir_mensagem_abort(228229);
end if;

ds_log_estab_w := '';
if  (nvl(cd_estabelecimento_p, 0) > 0) then
  ds_log_estab_w  := wheb_mensagem_pck.get_texto(281467) || cd_estabelecimento_p;
end if;

insert into ctb_log(
  cd_log,
  ds_log,
  nm_usuario,
  dt_atualizacao)
values( 1001,
  wheb_mensagem_pck.get_texto(281468) || to_char(dt_referencia_w,'mm/yyyy') || ds_log_estab_w,
  nm_usuario_p,
  sysdate);

if  (nr_seq_mes_ant_w > 0) then
  begin

  qt_contador_w := qt_contador_w + 1;

  gravar_processo_longo(wheb_mensagem_pck.get_texto(281470),'CTB_REATUALIZAR_SALDO', qt_contador_w);

  delete  from ctb_saldo
  where nr_seq_mes_ref    = nr_seq_mes_p
  and cd_estabelecimento  = nvl(cd_estabelecimento_p, cd_estabelecimento);
  commit;

  qt_contador_w := qt_contador_w + 1;

  gravar_processo_longo(wheb_mensagem_pck.get_texto(281473),'CTB_REATUALIZAR_SALDO', qt_contador_w);

  update  lote_contabil
  set dt_atualizacao_saldo  = null,
    dt_consistencia   = null,
    dt_processo   = null,
    nm_usuario    = nm_usuario_p,
    dt_atualizacao    = sysdate
  where nr_seq_mes_ref    = nr_seq_mes_p
  and cd_estabelecimento  = nvl(cd_estabelecimento_p, cd_estabelecimento);
  commit;

  qt_contador_w := qt_contador_w + 1;
  gravar_processo_longo(wheb_mensagem_pck.get_texto(281475),'CTB_VIRADA_SALDO', qt_contador_w);
  ctb_virada_saldo(nr_seq_mes_ant_w, cd_estabelecimento_p, nm_usuario_p);

  /* Begin do exception */
  begin

  philips_contabil_pck.set_ie_consistindo_lote('S');

  if  (nvl(cd_estabelecimento_p,0) = 0) then
    begin
    philips_contabil_pck.set_ie_reat_saldo('E');

    update  ctb_movimento
    set ds_consistencia = null
    where nr_seq_mes_ref = nr_seq_mes_p
    and ds_consistencia is not null;
    commit;

    update  ctb_movimento
    set nr_agrup_sequencial = 0
    where nr_seq_mes_ref = nr_seq_mes_p
    and nr_seq_agrupamento is null;
    commit;
    end;
  end if;

  open c01;
  loop
  fetch c01 bulk collect into c01_regs_w limit 1000;
    begin
    for i in 1..c01_regs_w.count loop
      begin
	  
      update  ctb_movimento
      set dt_atualizacao_saldo  = null
      where nr_lote_contabil  = c01_regs_w(i).nr_lote_contabil
      and dt_atualizacao_saldo is not null;
	  
      ds_erro_w := '';
      qt_contador_w := qt_contador_w + 1;
      gravar_processo_longo(wheb_mensagem_pck.get_texto(281476) || c01_regs_w(i).nr_lote_contabil,'CTB_CONSISTIR_LOTE', qt_contador_w);
      ctb_consistir_lote(c01_regs_w(i).nr_lote_contabil, ds_erro_w,nm_usuario_p);
      commit;

      if  (nvl(ds_erro_w,'X') = 'X') then
        begin
        gravar_processo_longo(wheb_mensagem_pck.get_texto(281477) || c01_regs_w(i).nr_lote_contabil,'CTB_ATUALIZAR_SALDO', qt_contador_w);
        ctb_atualizar_saldo(c01_regs_w(i).nr_lote_contabil, 'N', nm_usuario_p, 'N', ds_erro_w,'N');
        commit;
        end;
      end if;

      end;
    end loop;
    commit;
    end;
  exit when c01%notfound;
  end loop;
  close c01;

  philips_contabil_pck.set_ie_consistindo_lote('N');

  qt_contador_w := qt_contador_w + 1;
  gravar_processo_longo(wheb_mensagem_pck.get_texto(281478),'CTB_REATUALIZAR_SALDO', qt_contador_w);
  ctb_acumular_saldo(nr_seq_mes_p, cd_estabelecimento_p, nm_usuario_p, ds_erro_w);

  philips_contabil_pck.set_ie_reat_saldo('N');
  exception
  when others then
    philips_contabil_pck.set_ie_reat_saldo('N');
    philips_contabil_pck.set_ie_consistindo_lote('N');
    ds_exception_w := substr(sqlerrm,1,1000);
    wheb_mensagem_pck.exibir_mensagem_abort(448772,'DS_ERRO='||ds_exception_w);
  end;
  end;
end if;
philips_contabil_pck.set_ie_reat_saldo('N');
commit;

ds_erro_p := ds_erro_w;

end ctb_reatualizar_saldo;
/