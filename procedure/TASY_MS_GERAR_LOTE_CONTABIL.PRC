create or replace
procedure TASY_MS_GERAR_LOTE_CONTABIL(
				nr_lote_contabil_p		number,
				nm_usuario_p		varchar2) is 

cd_estabelecimento_w			number(10);
cd_empresa_w				number(10);
cd_tipo_lote_contabil_w			number(10);

ds_parametros_w				varchar2(4000);
dt_geracao_lote_w				date;
dt_referencia_w				date;
nr_lote_contabil_w				number(10)	:= nr_lote_contabil_p;
nr_seq_evento_w				number(10);
qt_movimento_w				number(10);

begin

select	cd_estabelecimento,
	cd_tipo_lote_contabil,
	dt_geracao_lote,
	dt_referencia
into	cd_estabelecimento_w,
	cd_tipo_lote_contabil_w,
	dt_geracao_lote_w,
	dt_referencia_w
from	lote_contabil
where	nr_lote_contabil	= nr_lote_contabil_w;
	
cd_empresa_w	:= obter_empresa_estab(cd_estabelecimento_w);

select	count(*)
into	qt_movimento_w
from	movimento_contabil
where	nr_lote_contabil	= nr_lote_contabil_w;

if	(dt_geracao_lote_w is not null) and
	(cd_tipo_lote_contabil_w in(2,3,26,6,8)) and
	(qt_movimento_w > 0) then
	begin
	ds_parametros_w	:= substr('NR_LOTE_CONTABIL=' || nr_lote_contabil_w	|| ';',1,4000);
	
	if	(cd_tipo_lote_contabil_w = 2) then
		nr_seq_evento_w	:= 166;
	elsif	(cd_tipo_lote_contabil_w = 3) then
		nr_seq_evento_w	:= 152;
	elsif	(cd_tipo_lote_contabil_w = 26) then
		nr_seq_evento_w	:= 153;
	elsif	(cd_tipo_lote_contabil_w in(6,8)) then
		nr_seq_evento_w		:= 154;
		HMSL_GERAR_RECEITA_MENSAL(cd_empresa_w, cd_estabelecimento_w, trunc(dt_referencia_w,'mm'), null, nm_usuario_p);
	end if;

	if	(cd_tipo_lote_contabil_w = 8) then
		cd_tipo_lote_contabil_w	:= 6;
	end if;

	TASY_MS_GRAVA_INTEGRACAO(cd_estabelecimento_w, '36', cd_tipo_lote_contabil_w, nr_lote_contabil_w, 'A', nr_seq_evento_w, ds_parametros_w, nm_usuario_p);
	end;
end if;

commit;

end tasy_ms_gerar_lote_contabil;
/