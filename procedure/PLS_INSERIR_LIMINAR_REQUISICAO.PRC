create or replace
procedure pls_inserir_liminar_requisicao
			(	nr_seq_liminar_p	processo_judicial_liminar.nr_sequencia%type,
				nr_seq_requisicao_p	pls_requisicao.nr_sequencia%type,
				nr_seq_guia_p		pls_guia_plano.nr_sequencia%type,
				nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir a liminar judicial na guia ou requisi��o.

Aprocedure est� com este nome pois anteriormente era usada somente para requisi��o.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */				
cd_juridico_liminar_w		processo_judicial_liminar.cd_juridico_liminar%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;

begin

if	(nr_seq_liminar_p	is not null) then
	if	(nr_seq_requisicao_p	is not null) then
		select	cd_juridico_liminar
		into	cd_juridico_liminar_w
		from	processo_judicial_liminar
		where	nr_sequencia = nr_seq_liminar_p;
		
		insert	into pls_req_liminar_judicial
			(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
			 nm_usuario, nm_usuario_nrec, nr_seq_processo,
			 nr_seq_requisicao, nr_liminar)
		values	(pls_req_liminar_judicial_seq.NextVal, sysdate, sysdate,
			 nm_usuario_p, nm_usuario_p, nr_seq_liminar_p,
			 nr_seq_requisicao_p, substr(cd_juridico_liminar_w,1,20));
	elsif	(nr_seq_guia_p	is not null) then
		begin
			select	nr_seq_segurado
			into	nr_seq_segurado_w
			from	pls_guia_plano
			where	nr_sequencia	= nr_seq_guia_p;
		exception
		when others then
			nr_seq_segurado_w	:= null;
		end;
		
		select	nvl(cd_juridico_liminar,'')
		into	cd_juridico_liminar_w
		from	processo_judicial_liminar
		where	nr_sequencia = nr_seq_liminar_p;
		
		insert	into pls_guia_liminar_judicial
			(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
			 nm_usuario, nm_usuario_nrec, nr_liminar,
			 nr_seq_guia, nr_seq_processo, nr_seq_segurado)
		values	(pls_guia_liminar_judicial_seq.NextVal, sysdate, sysdate,
			 nm_usuario_p, nm_usuario_p, substr(cd_juridico_liminar_w,1,20),
			 nr_seq_guia_p, nr_seq_liminar_p, nr_seq_segurado_w);
	end if;
end if;

commit;

end pls_inserir_liminar_requisicao;
/
