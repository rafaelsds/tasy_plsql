create or replace procedure GQA_Aprov_Exame_result_item(
				nr_seq_resultado_p	    number,
				nr_sequencia_p		    number,
				nm_usuario_p		    Varchar2,
                nr_regras_atendidas_p   out varchar2,
                nr_atendimento_p        number default null,
                nr_restrigir_regras_p   varchar2 default null,
                qt_horas_retroativa_p   number default 0) is 

nr_seq_pend_regra_w         number(10);
vl_minimo_w                 number(15,4);
vl_maximo_w                 number(15,4);	
nr_seq_sinal_vital_w        number(10);
nr_atendimento_w            number(10);
nr_sequencia_w	            number(10);
vl_atributo_w	            number(15,4);
nr_seq_exame_w	            number(10);
qt_resultado_exame_w        number(15,4);
pr_resultado_exame_w        Number(9,4);
qt_idade_w                  number(10);
cd_setor_atendimento_w      number(10) := null;
nm_exame_w               	varchar2(1000);
cd_pessoa_fisica_w          varchar2(10);
ie_formato_result_exame_w   varchar2(15);
qt_idade_dias_w             number(6);
qt_peso_gramas_w            number(12,3);
ie_restricao_complementar_w varchar2(80);
ie_protocolo_assistencial_w boolean;
nr_prescricao_w				number(10);
ds_unidade_medida_w			varchar2(40);
nr_seq_lab_valor_w varchar2(40);
ds_retorno_w varchar2(40);
nr_sequencia_exame_w number(10);
nr_seq_resultado_w varchar(10);


    --- Exames aprovado
    SQLExameAprovado       varchar2(1024) :=
        ' select  	max(a.nr_seq_exame), '                      ||
        '         	max(a.qt_resultado), '                      ||
        '         	max(a.pr_resultado), '                      ||
        '         	max(b.ie_formato_resultado), '              ||
		'			max(b.nm_exame), '							||
		'			max(c.nr_prescricao), '						||
		'			max(a.ds_unidade_medida) '					||
        ' from	exame_lab_result_item a, '                    	||
        '       exame_laboratorio     b, '                     	||
		'		exame_lab_resultado   c '						||
        ' where	b.nr_seq_exame      = a.nr_seq_exame '        	||
        ' and   a.nr_seq_resultado  = ' || nr_seq_resultado_p 	||
        ' and   a.nr_sequencia	    = ' ||nr_sequencia_p		||
		' and   c.nr_seq_resultado	= a.nr_seq_resultado';


    --- Exames do atendimento
    SQLExameAtendimento    varchar2(1024) :=
        ' select b.nr_seq_exame, '                          ||
        '        b.qt_resultado, '                          ||
        '        b.pr_resultado, '                          ||
        '        d.ie_formato_resultado, '                  ||
		'		 d.nm_exame, '								||
		'		 a.nr_prescricao, '							||
		'		 b.ds_unidade_medida '						||
        ' from 	 exame_lab_result_item   b, '               ||
        '        exame_lab_resultado     a, '               ||
        '        prescr_procedimento     x, '               ||
        '        prescr_medica           c, '               ||
        '        exame_laboratorio       d '                ||
        ' where	 b.nr_seq_exame     = d.nr_seq_exame  '     ||
        ' and    a.nr_seq_resultado	= b.nr_seq_resultado '  ||
        ' and	 a.nr_prescricao    = c.nr_prescricao '     ||
        ' and	 x.nr_sequencia     = b.nr_seq_prescr '     ||
        ' and	 x.nr_prescricao    = c.nr_prescricao '     ||
        ' and	 c.nr_atendimento	= ' ||nr_atendimento_p  ||
        ' and	 x.ie_status_atend	>= 35 '                 ||
        ' and    (' || qt_horas_retroativa_p || ' = 0 or dt_aprovacao >= sysdate - ' || qt_horas_retroativa_p || ') '  ||
        ' and    b.nr_seq_exame in (select nr_seq_exame from gqa_pendencia_regra where nr_sequencia in ( ' || nr_restrigir_regras_p || ' ))  ' ||
        ' order  by nvl(b.dt_aprovacao,a.dt_resultado)';


Cursor C01 is
	select	a.nr_sequencia,
			nvl(a.vl_minimo,0),
			nvl(a.vl_maximo,999)
	from    gqa_pendencia_regra a,
            gqa_pendencia b
	where	b.nr_sequencia	    = a.nr_seq_pendencia
	and		b.ie_tipo_pendencia	= 2
	and		b.ie_situacao 		= 'A'
	and		a.ie_situacao		= 'A'
	and		obter_se_gqa_regra_liberada(a.nr_sequencia) = 'S'
	and		a.nr_seq_exame		= nr_seq_exame_w
	and		qt_idade_w       between nvl(a.qt_idade_min,0) and nvl(a.qt_idade_max,999)
	and     qt_idade_dias_w  between nvl(a.qt_dias_min ,0) and nvl(a.qt_dias_max ,99999)
	and	    qt_peso_gramas_w between nvl(a.qt_peso_gramas_min,0) and nvl(a.qt_peso_gramas_max,999999999)
	and		nvl(a.cd_setor_atendimento,nvl(cd_setor_atendimento_w,0))	= nvl(cd_setor_atendimento_w,0)
    and     (nr_restrigir_regras_p is null or instr(nr_restrigir_regras_p, a.nr_sequencia) > 0);

    CursorExames				SYS_REFCURSOR;
begin

ie_protocolo_assistencial_w := nr_restrigir_regras_p is not null;

if ie_protocolo_assistencial_w then

    nr_atendimento_w := nr_atendimento_p;   
    cd_pessoa_fisica_w := obter_pessoa_atendimento(nr_atendimento_w,'C');

else 
    select 	nvl(max(nr_atendimento),0),
            max(obter_pessoa_atendimento(nr_atendimento,'C'))
    into	nr_atendimento_w,
            cd_pessoa_fisica_w
    from	exame_lab_resultado
    where	nr_seq_resultado = nr_seq_resultado_p;

end if;

if (nr_atendimento_w is null) or (nr_atendimento_w = 0) or
   (cd_pessoa_fisica_w is null) then

	select  nvl(decode(nr_atendimento_w,0,max(a.nr_atendimento),nr_atendimento_w),0),
			nvl(cd_pessoa_fisica_w,max(a.cd_pessoa_fisica))
	into    nr_atendimento_w,
			cd_pessoa_fisica_w
	from    prescr_medica a,
		   exame_lab_resultado c
	where a.nr_prescricao = c.nr_prescricao
	and c.nr_seq_resultado = nr_seq_resultado_p;

end if;

if ie_protocolo_assistencial_w then
    open CursorExames for SQLExameAtendimento;    
else
    open CursorExames for SQLExameAprovado;
end if;

loop
fetch CursorExames into
        nr_seq_exame_w,
		qt_resultado_exame_w,
		pr_resultado_exame_w,
        ie_formato_result_exame_w,
		nm_exame_w,
		nr_prescricao_w,
		ds_unidade_medida_w;
exit when CursorExames%notfound;
begin

    if	(nr_atendimento_w > 0) and (cd_pessoa_fisica_w is not null) then

        if cd_setor_atendimento_w is null then 
            cd_setor_atendimento_w	:= obter_setor_atendimento(nr_atendimento_w);
            qt_idade_w	            := obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A');
            qt_idade_dias_w	        := obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'DIA');
            qt_peso_gramas_w	    := (nvl(obter_peso_pf(cd_pessoa_fisica_w),0) * 1000);
        end if;

        if	(ie_formato_result_exame_w in ('CA','CV','DV','VP','V')) and (qt_resultado_exame_w	is not null) then
            vl_atributo_w	:= qt_resultado_exame_w;
        elsif	(pr_resultado_exame_w is not null) and (ie_formato_result_exame_w in ('CA','P','VP')) then
            vl_atributo_w	:= pr_resultado_exame_w;
        end if;

        open C01;
        loop
        fetch C01 into
            nr_seq_pend_regra_w,
            vl_minimo_w,
            vl_maximo_w;
        exit when C01%notfound;
            begin

          /*if	(vl_atributo_w	is not null) and
                (vl_atributo_w	<> 0) and
                (vl_atributo_w	>= vl_minimo_w) and
                (vl_atributo_w	<= vl_maximo_w) then*/

                select GQA_regra_complementar(nm_usuario_p, nr_atendimento_w, nr_seq_pend_regra_w) 
                into ie_restricao_complementar_w
                from dual;

                        select decode(w.nr_seq_exame_lab_valor , 1, obter_desc_expressao_br(296109), 2, obter_desc_expressao_br(328863), '')
                            into nr_seq_lab_valor_w
                            from GQA_PENDENCIA_REGRA w
                            where w.nr_seq_exame = nr_seq_exame_w;

                        select max(ds_resultado) 

                            into ds_retorno_w
                            from  EXAME_LAB_RESULT_ITEM                            
                            where nr_seq_resultado = nr_seq_exame_w;

                if ie_restricao_complementar_w = 'X' then

                    if ie_protocolo_assistencial_w then

                        if nr_regras_atendidas_p is not null then
                            nr_regras_atendidas_p := nr_regras_atendidas_p || ',';
                        end if;
                        nr_regras_atendidas_p := nr_regras_atendidas_p || nr_seq_pend_regra_w;

                    else

                        select	gqa_pendencia_pac_seq.nextval
                        into	nr_sequencia_w
                        from	dual;

                        insert into gqa_pendencia_pac (
                                nr_sequencia,
                                dt_atualizacao,
                                nm_usuario,
                                dt_atualizacao_nrec,
                                nm_usuario_nrec,
                                cd_pessoa_fisica,
                                nr_atendimento,
                                nr_seq_resultado,
                                nr_seq_result_item,
                                NR_SEQ_PEND_REGRA)
                        values(	nr_sequencia_w,
                                sysdate,
                                nm_usuario_p,
                                sysdate,
                                nm_usuario_p,
                                cd_pessoa_fisica_w,
                                nr_atendimento_w,
                                nr_seq_resultado_p,
                                nr_sequencia_p,
                                nr_seq_pend_regra_w);

                        gerar_consulta_reg_mentor (nm_usuario_p, 'EXAME_LAB_RESULT_ITEM', nr_seq_pend_regra_w, nr_sequencia_p, nr_sequencia_w, vl_atributo_w, nm_exame_w, ds_unidade_medida_w, nr_prescricao_w);
                        GQA_GERAR_ACAO_REGRA(nr_seq_pend_regra_w,nr_sequencia_w,nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p);

                    end if;
                end if;
              --end if;
            end;
        end loop;
        close C01;

        commit;

    end if;    
end;
end loop;
close CursorExames;   

end GQA_Aprov_Exame_result_item;
/
