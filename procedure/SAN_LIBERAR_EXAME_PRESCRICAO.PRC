create or replace
procedure SAN_Liberar_Exame_Prescricao(	nr_prescricao_p		Number,
					nr_seq_procedimento_p	Number,
					nm_usuario_p		Varchar2) is 

nr_seq_horario_w				Number(10,0);
ie_atualiza_hor_hemoterap_w		varchar2(1);
					
begin

obter_param_usuario(1113, 597, Obter_perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_atualiza_hor_hemoterap_w);

if (nr_prescricao_p is not null) and (nr_seq_procedimento_p is not null) then

	update	prescr_procedimento
	set	dt_liberacao_hemoterapia = sysdate,
		nm_usuario_lib_hemoterapia = nm_usuario_p
	where	nr_prescricao = nr_prescricao_p
	and	nr_sequencia = nr_seq_procedimento_p;
	
	if	(ie_atualiza_hor_hemoterap_w = 'S') then
		select	max(b.nr_sequencia)
		into	nr_seq_horario_w
		from	prescr_procedimento a,	
		 		prescr_proc_hor b
		where	a.nr_prescricao = b.nr_prescricao
		and		a.nr_sequencia = b.nr_seq_procedimento
		and		a.nr_prescricao = nr_prescricao_p
		and		a.nr_sequencia = nr_seq_procedimento_p
		and		a.ie_tipo_proced = 'BSST';
		
		if	(nr_seq_horario_w is not null) then
			
			update	prescr_proc_hor
			set		dt_horario = sysdate
			where	nr_sequencia = nr_seq_horario_w;
			
			update	prescr_procedimento
			set		dt_prev_execucao = sysdate,
					dt_status = sysdate
			where	nr_prescricao = nr_prescricao_p
			and		nr_sequencia = nr_seq_procedimento_p;
			
		end if;
		
	end if;

end if;

commit;

end SAN_Liberar_Exame_Prescricao;
/
