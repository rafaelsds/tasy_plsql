create or replace
procedure ptu_obter_valores_nota_servico(	nr_seq_conta_pos_estab_p	pls_conta_pos_estabelecido.nr_sequencia%type,
						nr_seq_pos_proc_p		pls_conta_pos_proc.nr_sequencia%type,
						nr_seq_pos_mat_p		pls_conta_pos_mat.nr_sequencia%type,
						nr_seq_fatura_proc_p		pls_conta_proc.nr_sequencia%type,
						nr_seq_fatura_mat_p		pls_conta_mat.nr_sequencia%type,
						nr_seq_pos_estab_partic_p	pls_conta_pos_estab_partic.nr_sequencia%type,
						nr_seq_conta_pos_contab_p	pls_fatura_proc.nr_seq_conta_pos_contab%type,
						nr_seq_pos_proc_fat_p		pls_conta_pos_proc_fat.nr_sequencia%type,
						nr_seq_pos_mat_fat_p		pls_conta_pos_mat_fat.nr_sequencia%type,
						vl_procedimento_p		out number,
						vl_filme_p			out number,
						vl_custo_operacional_p		out number,
						vl_adic_procedimento_p		out number,
						vl_adic_filme_p			out number,
						vl_adic_co_p			out number ,
						qt_partic_duplic_p		in number,
						nr_seq_fatura_conta_p		in number,
						ie_tipo_cobranca_fatura_p	in ptu_fatura.ie_tipo_cobranca_fatura%type) is
						
nr_seq_conta_proc_w		pls_conta_proc.nr_sequencia%type;
nr_seq_conta_mat_w		pls_conta_mat.nr_sequencia%type;
qt_fatura_w			pls_integer;
vl_medico_w			pls_conta_pos_estabelecido.vl_medico%type;
vl_adic_procedimento_w		pls_conta_pos_estabelecido.vl_lib_taxa_servico%type;
vl_adic_proc_original_w		pls_conta_pos_estabelecido.vl_lib_taxa_servico%type;
nr_seq_proc_partic_w		pls_conta_pos_estab_contab.nr_seq_proc_partic%type;
qt_pos_estab_w			pls_integer;
qt_registro_w			pls_integer;
qt_partic_contab_w		pls_integer;
qt_pos_contab_w			pls_integer;
qt_participante_w		pls_integer;

begin

if	(nr_seq_conta_pos_estab_p is not null) and (nr_seq_pos_proc_p is null) and (nr_seq_pos_mat_p is null) then
	select	max(nr_seq_conta_proc),
		max(nr_seq_conta_mat)
	into	nr_seq_conta_proc_w,
		nr_seq_conta_mat_w
	from	pls_conta_pos_estabelecido
	where	nr_sequencia = nr_seq_conta_pos_estab_p
	and	ie_status_faturamento  = decode(ie_tipo_cobranca_fatura_p, 'A', 'A', 'L');
	
	select 	sum(qt_item)
	into	qt_fatura_w
	from	(select	count(1) qt_item
		from	pls_fatura_proc a
		where	a.nr_seq_conta_pos_estab = nr_seq_conta_pos_estab_p
		and	a.nr_seq_fat_proc_cancel is null
		and	a.ie_tipo_cobranca not in ('3','4')
		and not exists(	select 1
				from 	pls_fatura_conta 	d,
					pls_fatura_evento 	c,
					pls_fatura		b
				where 	d.nr_sequencia 		= a.nr_seq_fatura_conta
				and	c.nr_sequencia		= d.nr_seq_fatura_evento
				and	b.nr_sequencia		= c.nr_seq_fatura
				and	b.ie_cancelamento is not null)
		union all
		select	count(1) qt_item
		from	pls_fatura_mat a
		where	a.nr_seq_conta_pos_estab = nr_seq_conta_pos_estab_p
		and	a.nr_seq_fat_mat_cancel  is null
		and	a.ie_tipo_cobranca not in ('3','4')
		and not exists(	select 1
				from 	pls_fatura_conta 	d,
					pls_fatura_evento 	c,
					pls_fatura		b
				where 	d.nr_sequencia 		= a.nr_seq_fatura_conta
				and	c.nr_sequencia		= d.nr_seq_fatura_evento
				and	b.nr_sequencia		= c.nr_seq_fatura
				and	b.ie_cancelamento is not null));
				
	select	count(1)
	into	qt_participante_w
	from	pls_conta_pos_estab_partic
	where	nr_seq_conta_pos 	= nr_seq_conta_pos_estab_p
	and	vl_participante_pos	= 0;
				
	if	(nr_seq_conta_pos_contab_p is not null) and (qt_fatura_w > 1) and (qt_participante_w = 0) then
		if	(nr_seq_conta_proc_w is not null) then
			select	nvl(vl_medico, 0),
				nvl(vl_materiais, 0),
				nvl(vl_custo_operacional_ind, 0),
				nvl(vl_lib_taxa_servico, 0),
				nvl(vl_lib_taxa_material, 0),
				nvl(vl_lib_taxa_co, 0),
				nvl(vl_medico, 0),
				nvl(vl_lib_taxa_servico, 0),
				nvl(vl_lib_taxa_servico, 0),
				nr_seq_proc_partic
			into	vl_procedimento_p,
				vl_filme_p,
				vl_custo_operacional_p,
				vl_adic_procedimento_p,
				vl_adic_filme_p,
				vl_adic_co_p,
				vl_medico_w,
				vl_adic_procedimento_w,
				vl_adic_proc_original_w,
				nr_seq_proc_partic_w
			from	pls_conta_pos_estab_contab
			where	nr_sequencia = nr_seq_conta_pos_contab_p;
			
			select	count(1)
			into	qt_pos_estab_w
			from	pls_conta_pos_estab_contab
			where	nr_seq_conta_pos = nr_seq_conta_pos_estab_p
			and	nr_seq_proc_partic is not null;
			
			if	(nr_seq_pos_estab_partic_p is not null) and ((nr_seq_proc_partic_w is not null) or (qt_pos_estab_w = 0)) then
				select	count(1)
				into	qt_registro_w
				from	pls_conta_pos_estab_partic
				where	nr_sequencia = nr_seq_pos_estab_partic_p
				and	vl_participante_pos > 0;
				
				-- Caso tenha "1" nico participante com valor zerodo e este esteja vinculado a um ps contab, TODOS os participantes devem enviar confome o ps-contabil
				select	count(1)
				into	qt_partic_contab_w
				from	pls_conta_pos_estab_partic	a
				where	a.nr_seq_conta_pos		= nr_seq_conta_pos_estab_p
				and	a.vl_participante_pos		= 0
				and exists(	select	1
						from	pls_conta_pos_estab_contab	b
						where	b.nr_seq_proc_partic		= a.nr_seq_proc_partic
						and	b.nr_seq_conta_pos		= a.nr_seq_conta_pos);
						
				if	(qt_registro_w > 0) and (qt_partic_contab_w = 0) then
					select	count(1)
					into	qt_registro_w
					from	pls_conta_pos_estab_partic
					where	nr_seq_conta_pos = nr_seq_conta_pos_estab_p;
					
					select	count(1)
					into	qt_pos_contab_w
					from	pls_conta_pos_estab_contab
					where	nr_seq_conta_pos = nr_seq_conta_pos_estab_p;
					
					select	nvl(vl_participante_pos, 0),
						nvl(vl_administracao, 0)
					into	vl_procedimento_p,
						vl_adic_procedimento_p
					from	pls_conta_pos_estab_partic
					where	nr_sequencia = nr_seq_pos_estab_partic_p;
					
					vl_procedimento_p := vl_procedimento_p - vl_adic_procedimento_p;
					
					if	((qt_registro_w <= 1) or (qt_registro_w != qt_pos_contab_w)) then
						vl_procedimento_p 	:= vl_medico_w;
						vl_adic_procedimento_p	:= vl_adic_procedimento_w;
					end if;
					
					if	(qt_partic_duplic_p > 1) then
						vl_procedimento_p 	:= vl_procedimento_p * qt_partic_duplic_p;
						vl_adic_procedimento_p 	:= vl_adic_proc_original_w;
					end if;
				end if;
			end if;
		end if;
		
		if	(nr_seq_conta_mat_w is not null) then
			select	nvl(vl_custo_operacional, 0) - nvl(vl_administracao, 0),
				0,
				0,
				nvl(vl_administracao, 0),
				0,
				0,
				nvl(vl_medico, 0),
				nvl(vl_administracao, 0)
			into	vl_procedimento_p,
				vl_filme_p,
				vl_custo_operacional_p,
				vl_adic_procedimento_p,
				vl_adic_filme_p,
				vl_adic_co_p,
				vl_medico_w,
				vl_adic_procedimento_w
			from	pls_conta_pos_estab_contab
			where	nr_sequencia = nr_seq_conta_pos_contab_p;
		end if;
	else
		if	(nr_seq_conta_proc_w is not null) then
			select	nvl(vl_medico, 0),
				nvl(vl_materiais, 0),
				nvl(vl_custo_operacional, 0),
				nvl(vl_lib_taxa_servico, 0),
				nvl(vl_lib_taxa_material, 0),
				nvl(vl_lib_taxa_co, 0),
				nvl(vl_medico, 0),
				nvl(vl_lib_taxa_servico, 0),
				nvl(vl_lib_taxa_servico, 0)
			into	vl_procedimento_p,
				vl_filme_p,
				vl_custo_operacional_p,
				vl_adic_procedimento_p,
				vl_adic_filme_p,
				vl_adic_co_p,
				vl_medico_w,
				vl_adic_procedimento_w,
				vl_adic_proc_original_w
			from	pls_conta_pos_estabelecido
			where	ie_status_faturamento = decode(ie_tipo_cobranca_fatura_p, 'A', 'A', 'L')
			and	nr_sequencia = nr_seq_conta_pos_estab_p;
		end if;
		
		if	(nr_seq_conta_mat_w is not null) then
			select	nvl(b.vl_beneficiario, 0) - nvl(b.vl_administracao, 0),
				0,
				0,
				nvl(b.vl_administracao, 0),
				0,
				0,
				nvl(b.vl_medico, 0),
				nvl(b.vl_administracao, 0)
			into	vl_procedimento_p,
				vl_filme_p,
				vl_custo_operacional_p,
				vl_adic_procedimento_p,
				vl_adic_filme_p,
				vl_adic_co_p,
				vl_medico_w,
				vl_adic_procedimento_w
			from	pls_conta_pos_estabelecido	b,
				pls_conta_mat			a
			where	a.nr_sequencia			= b.nr_seq_conta_mat
			and	b.ie_status_faturamento 	= decode(ie_tipo_cobranca_fatura_p, 'A', 'A', 'L')
			and	b.nr_sequencia			= nr_seq_conta_pos_estab_p;
		end if;
		
		if	(nr_seq_pos_estab_partic_p is not null) then
			select	count(1)
			into	qt_registro_w
			from	pls_conta_pos_estab_partic
			where	nr_seq_conta_pos = nr_seq_conta_pos_estab_p;
			
			select	nvl(vl_participante_pos, 0),
				nvl(vl_administracao, 0)
			into	vl_procedimento_p,
				vl_adic_procedimento_p
			from	pls_conta_pos_estab_partic
			where	nr_sequencia = nr_seq_pos_estab_partic_p;
			
			vl_procedimento_p := vl_procedimento_p - vl_adic_procedimento_p;
			
			if	(qt_registro_w <= 1) then
				vl_procedimento_p 	:= vl_medico_w;
				vl_adic_procedimento_p 	:= vl_adic_procedimento_w;
			end if;
			
			if	(qt_partic_duplic_p > 1) then
				vl_procedimento_p 	:= vl_procedimento_p * qt_partic_duplic_p;
				vl_adic_procedimento_p 	:= vl_adic_proc_original_w;
			end if;
		end if;
	end if;
	
elsif	(nr_seq_pos_proc_p is not null) then
	select	max(nr_seq_conta_proc)
	into	nr_seq_conta_proc_w
	from	pls_conta_pos_proc
	where	nr_sequencia = nr_seq_pos_proc_p
	and	ie_status_faturamento = 'F';
	
	select	count(1)
	into	qt_fatura_w
	from	pls_fatura_proc 	a
	where	a.nr_seq_pos_proc 	= nr_seq_pos_proc_p
	and	a.nr_seq_fat_proc_cancel is null
	and	a.ie_tipo_cobranca not in ('3','4')
	and not exists(	select 1
			from 	pls_fatura_conta 	d,
				pls_fatura_evento 	c,
				pls_fatura		b
			where 	d.nr_sequencia 		= a.nr_seq_fatura_conta
			and	c.nr_sequencia		= d.nr_seq_fatura_evento
			and	b.nr_sequencia		= c.nr_seq_fatura
			and	b.ie_cancelamento is not null);
			
	if	(qt_fatura_w > 1) then
		select	nvl(vl_medico, 0),
			nvl(vl_materiais, 0),
			nvl(vl_custo_operacional, 0),
			nvl(vl_lib_taxa_servico, 0),
			nvl(vl_lib_taxa_material, 0),
			nvl(vl_lib_taxa_co, 0),
			nvl(vl_medico, 0),
			nvl(vl_lib_taxa_servico, 0),
			nvl(vl_lib_taxa_servico, 0)
		into	vl_procedimento_p,
			vl_filme_p,
			vl_custo_operacional_p,
			vl_adic_procedimento_p,
			vl_adic_filme_p,
			vl_adic_co_p,
			vl_medico_w,
			vl_adic_procedimento_w,
			vl_adic_proc_original_w
		from	pls_conta_pos_proc_fat
		where	nr_sequencia = nr_seq_pos_proc_fat_p;
	else
		begin
			select	nvl(vl_medico, 0),
				nvl(vl_materiais, 0),
				nvl(vl_custo_operacional, 0),
				nvl(vl_lib_taxa_servico, 0),
				nvl(vl_lib_taxa_material, 0),
				nvl(vl_lib_taxa_co, 0),
				nvl(vl_medico, 0),
				nvl(vl_lib_taxa_servico, 0),
				nvl(vl_lib_taxa_servico, 0)
			into	vl_procedimento_p,
				vl_filme_p,
				vl_custo_operacional_p,
				vl_adic_procedimento_p,
				vl_adic_filme_p,
				vl_adic_co_p,
				vl_medico_w,
				vl_adic_procedimento_w,
				vl_adic_proc_original_w
			from	pls_conta_pos_proc
			where	ie_status_faturamento = 'F'
			and	nr_sequencia = nr_seq_pos_proc_p;
		exception
		when others then
			vl_procedimento_p	:= 0;
			vl_filme_p		:= 0;
			vl_custo_operacional_p	:= 0;
			vl_adic_procedimento_p	:= 0;
			vl_adic_filme_p		:= 0;
			vl_adic_co_p		:= 0;
			vl_medico_w		:= 0;
			vl_adic_procedimento_w	:= 0;
			vl_adic_proc_original_w	:= 0;
		end;
	end if;
	
elsif	(nr_seq_pos_mat_p is not null) then
	select	max(nr_seq_conta_mat)
	into	nr_seq_conta_mat_w
	from	pls_conta_pos_mat
	where	nr_sequencia = nr_seq_pos_mat_p
	and	ie_status_faturamento = 'F';
	
	select	count(1)
	into	qt_fatura_w
	from	pls_fatura_mat	 	a
	where	a.nr_seq_pos_mat 	= nr_seq_pos_mat_p
	and	a.nr_seq_fat_mat_cancel is null
	and	a.ie_tipo_cobranca not in ('3','4')
	and not exists(	select 1
			from 	pls_fatura_conta 	d,
				pls_fatura_evento 	c,
				pls_fatura		b
			where 	d.nr_sequencia 		= a.nr_seq_fatura_conta
			and	c.nr_sequencia		= d.nr_seq_fatura_evento
			and	b.nr_sequencia		= c.nr_seq_fatura
			and	b.ie_cancelamento is not null);
			
	if	(qt_fatura_w > 1) then
		select	nvl(vl_pos_estabelecido, 0) - nvl(vl_administracao, 0),
			0,
			0,
			nvl(vl_administracao, 0),
			0,
			0,
			0,
			nvl(vl_administracao, 0)
		into	vl_procedimento_p,
			vl_filme_p,
			vl_custo_operacional_p,
			vl_adic_procedimento_p,
			vl_adic_filme_p,
			vl_adic_co_p,
			vl_medico_w,
			vl_adic_procedimento_w
		from	pls_conta_pos_mat_fat
		where	nr_sequencia = nr_seq_pos_mat_fat_p;
	else
		begin
			select	(nvl(vl_materiais, 0) + nvl(vl_lib_taxa_material, 0)) - nvl(vl_administracao, 0),
				0,
				0,
				nvl(vl_administracao, 0),
				0,
				0,
				0,
				nvl(vl_administracao, 0)
			into	vl_procedimento_p,
				vl_filme_p,
				vl_custo_operacional_p,
				vl_adic_procedimento_p,
				vl_adic_filme_p,
				vl_adic_co_p,
				vl_medico_w,
				vl_adic_procedimento_w
			from	pls_conta_pos_mat
			where	ie_status_faturamento = 'F'
			and	nr_sequencia = nr_seq_pos_mat_fat_p;
		exception
		when others then
			vl_procedimento_p	:= 0;
			vl_filme_p		:= 0;
			vl_custo_operacional_p	:= 0;
			vl_adic_procedimento_p	:= 0;
			vl_adic_filme_p		:= 0;
			vl_adic_co_p		:= 0;
			vl_medico_w		:= 0;
			vl_adic_procedimento_w	:= 0;
		end;
	end if;
end if;

end ptu_obter_valores_nota_servico;
/