 create or replace
procedure pls_consistir_guia
			(	nr_seq_guia_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Consistir a guia
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicionario [ x ] Tasy (Delphi/Java) [ x ] Portal [  ] Relatorios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de atencao:  Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

nr_seq_plano_w			number(10);
nr_seq_contrato_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_prestador_w		number(10);
qt_dia_solicitado_w		number(3);
cd_guia_oper_w			varchar2(20);
nr_seq_guia_ref_w		number(10);
dt_contratacao_w		date;
dt_solicitacao_w		date;
dt_rescisao_w			date;
nr_seq_tipo_acomodacao_w	number(10);
cd_medico_w			varchar2(10);
qt_glosa_w			number(5);
cd_usuario_plano_w		varchar2(30);
ie_carater_internacao_w		varchar2(1);
nr_seq_clinica_w		number(10);
ie_regime_internacao_w		varchar2(1);
ie_tipo_guia_ref_w		varchar2(2);
ie_tipo_guia_w			varchar2(2);
ie_situacao_w			varchar2(1);
cd_medico_solicitante_w		varchar2(10);
cd_guia_principal_w		varchar2(10);
qt_registros_w			number(10);
nr_seq_categoria_w		number(10);
dt_limite_utilizacao_w		date;
dt_liberacao_w			date;
dt_migracao_w			date;
nr_seq_motivo_glosa_w		number(10,0);
cd_motivo_glosa_w		varchar2(100);
dt_validade_carteira_w		date;
ie_tipo_segurado_w		Varchar2(3);
ie_tipo_segurado_guia_w		Varchar2(3);
ie_guia_glosa_w			Varchar2(1)	:= 'N';
ie_situacao_atend_w		varchar2(1);
ie_consistir_atuacao_w		varchar2(1);
ie_area_coberta_w		varchar2(1);
qt_dias_vencido_w		number(5);
ie_tipo_pagador_w		varchar(3);
nr_seq_uni_exec_w		number(10);
ie_tipo_processo_w		varchar(1);
nr_seq_regra_w			number(10);
ie_permite_w			varchar2(1);
ie_tipo_prestador_w		varchar2(2);
ie_acomodacao_carencia_w	varchar2(10);
nr_seq_congenere_w		number(10);
ie_cheque_w			varchar2(1);
ie_tipo_atend_tiss_w		varchar2(2);
nr_seq_regra_liminar_w		number(10);
nr_seq_auditoria_w		number(10);
ie_utiliza_nivel_w		varchar2(1);
cd_operadora_executora_w	varchar2(255);
cd_operadora_outorgante_w	varchar2(10);
ie_estagio_guia_w		number(2);
ie_tipo_intercambio_w		varchar2(10);
nr_seq_guia_principal_w		number(10);
ie_tipo_guia_princ_w		varchar2(2);
ie_acomodacao_sca_w		varchar2(10);
qt_ordem_serv_w			number(4);
cd_versao_tiss_w		pls_versao_tiss.cd_versao_tiss%type;
ie_indicacao_acidente_w		pls_diagnostico.ie_indicacao_acidente%type;
nr_seq_protocolo_atend_w	pls_protocolo_atendimento.nr_sequencia%type;
nr_protocolo_w			pls_protocolo_atendimento.nr_protocolo%type;
nr_seq_atend_pls_w		pls_atendimento.nr_sequencia%type;
ds_retorno_controle_w		varchar(255);
ie_status_token_w		ptu_pedido_autorizacao.ie_status_token%type;
ie_valida_token_w		varchar2(1);
ie_valida_token_scs_w	varchar2(1);
--Campo utilizado temporariamente, para verificar problema na apresentacao automatica de conta medica
ie_origem_solic_w		pls_guia_plano.ie_origem_solic%type;
nr_seq_leitura_w		pls_carteira_leitura.nr_sequencia%type;
cd_ausencia_val_benef_tiss_w	pls_requisicao.cd_ausencia_val_benef_tiss%type;
cd_validacao_benef_tiss_w	pls_requisicao.cd_validacao_benef_tiss%type;

begin

-- gerencia a atualizacao da tabela TM para estrutura de materiais
PLS_GERENCIA_UPD_OBJ_PCK.ATUALIZAR_OBJETOS('Tasy', 'PLS_CONSISTIR_GUIA', 'PLS_ESTRUTURA_MATERIAL_TM');
--Atualizando a tabela de grupo de procedimentos
PLS_GERENCIA_UPD_OBJ_PCK.ATUALIZAR_OBJETOS('Tasy', 'PLS_CONSISTIR_GUIA', 'PLS_GRUPO_SERVICO_TM');
-- gerencia a atualizacao da tabela TM para para grupos de materiais
PLS_GERENCIA_UPD_OBJ_PCK.ATUALIZAR_OBJETOS('Tasy', 'PLS_CONSISTIR_GUIA', 'PLS_GRUPO_MATERIAL_TM');

select	cd_guia,
	nvl(nr_seq_segurado,0),
	ie_tipo_guia,
	dt_solicitacao,
	nr_seq_prestador,
	cd_medico_solicitante,
	ie_carater_internacao,
	nr_seq_clinica,
	ie_regime_internacao,
	nr_seq_tipo_acomodacao,
	nr_seq_plano,
	ie_tipo_segurado,
	nr_seq_uni_exec,
	ie_tipo_processo,
	pls_obter_tipo_prestador(nr_seq_prestador),
	ie_tipo_atend_tiss,
	ie_estagio,
	ie_tipo_intercambio,
	ie_origem_solic,
	nr_seq_atend_pls,
	cd_ausencia_val_benef_tiss,
	cd_validacao_benef_tiss
into	cd_guia_oper_w,
	nr_seq_segurado_w,
	ie_tipo_guia_w,
	dt_solicitacao_w,
	nr_seq_prestador_w,
	cd_medico_solicitante_w,
	ie_carater_internacao_w,
	nr_seq_clinica_w,
	ie_regime_internacao_w,
	nr_seq_tipo_acomodacao_w,
	nr_seq_plano_w,
	ie_tipo_segurado_guia_w,
	nr_seq_uni_exec_w,
	ie_tipo_processo_w,
	ie_tipo_prestador_w,
	ie_tipo_atend_tiss_w,
	ie_estagio_guia_w,
	ie_tipo_intercambio_w,
	ie_origem_solic_w,
	nr_seq_atend_pls_w,
	cd_ausencia_val_benef_tiss_w,
	cd_validacao_benef_tiss_w
from	pls_guia_plano
where	nr_sequencia	= nr_seq_guia_p;

if 	(nvl(nr_seq_uni_exec_w,0) > 0) then
	cd_operadora_executora_w := pls_obter_dados_cooperativa(nr_seq_uni_exec_w,'C');
	cd_operadora_outorgante_w := pls_obter_unimed_estab(cd_estabelecimento_p);
end if;

--Verificar se existe algum tipo de anexo nos itens da Autorizacao, se existir sera marcado o campo de anexo na PLS_GUIA_PLANO
pls_atualizar_tipo_anexo_guia(nr_seq_guia_p, null, nm_usuario_p);

begin
	select	nvl(ie_tipo_segurado,'B'),
		nr_seq_congenere
	into	ie_tipo_segurado_w,
		nr_seq_congenere_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
exception
when others then
	ie_tipo_segurado_w	:= 'B';
	nr_seq_congenere_w	:= null;
end;

if	(nr_seq_segurado_w	= 0) then
	pls_gravar_motivo_glosa('1013',nr_seq_guia_p, null, null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
elsif	(ie_tipo_segurado_w <> ie_tipo_segurado_guia_w) then
	pls_gravar_motivo_glosa('1011',nr_seq_guia_p, null, null,
				'Guia: ' || ie_tipo_segurado_guia_w || expressao_pck.obter_desc_expressao(284292) || ie_tipo_segurado_w,
				nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
end if;

if	(nr_seq_segurado_w > 0) and
	(ie_tipo_segurado_w <> 'P') then
	
	if	(ie_tipo_processo_w = 'E') then
		pls_valida_controle_int_guia(nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p, ds_retorno_controle_w);
	end if;
	
	select	nr_seq_contrato,
		dt_contratacao,
		dt_rescisao,
		fim_dia(dt_limite_utilizacao),
		dt_liberacao,
		dt_migracao,
		ie_situacao_atend
	into	nr_seq_contrato_w,
		dt_contratacao_w,
		dt_rescisao_w,
		dt_limite_utilizacao_w,
		dt_liberacao_w,
		dt_migracao_w,
		ie_situacao_atend_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	
	cd_versao_tiss_w := pls_obter_versao_tiss;
	if	(cd_versao_tiss_w is null) then
		cd_versao_tiss_w := '2.02.03';
	end if;

	begin
		select	nvl(ie_valida_token, 'N')
		into		ie_valida_token_scs_w
		from	pls_param_intercambio_scs;
	exception
	when others then
		ie_valida_token_scs_w := 'N';
	end;


	if (ie_valida_token_scs_w = 'S') then
		begin
			select	max(ie_status_token)
			into	ie_status_token_w
			from	ptu_pedido_compl_aut
			where	nr_seq_guia = nr_seq_guia_p;

			if	(ie_status_token_w is null) then
				select	max(ie_status_token)
				into	ie_status_token_w
				from	ptu_pedido_autorizacao
				where	nr_seq_guia = nr_seq_guia_p;
			end if;
		exception
		when others then
			ie_status_token_w := '2';
		end;

		if	(ie_status_token_w is not null) and
			(ie_status_token_w <> '2') then
			pls_gravar_motivo_glosa('3156', nr_seq_guia_p, null, null, expressao_pck.obter_desc_expressao(969825), nm_usuario_p, '', 'CG', nr_seq_prestador_w, null, null);
		end if;
	end if;
	
	begin
		select	nvl(ie_valida_token, 'N')
		into		ie_valida_token_w
		from	pls_param_atend_geral;
	exception
	when others then
		ie_valida_token_w := 'N';
	end;

	if	(ie_valida_token_w = 'S') and
		(nvl(ie_origem_solic_w, 'X')  in ('E', 'W')) and
		(nvl(ie_tipo_processo_w, 'M') <> 'I' ) then
		begin
			select	max(nr_sequencia)
			into		nr_seq_leitura_w
			from	pls_carteira_leitura
			where	nr_seq_segurado = nr_seq_segurado_w;
		
			select	max(ie_status)
			into		ie_status_token_w
			from	pls_carteira_leitura_token
			where	nr_seq_leitura = nr_seq_leitura_w
			and		ds_token = cd_validacao_benef_tiss_w;
		exception
		when others then
			ie_status_token_w := '2';
		end;

		if	((ie_status_token_w is not null) and
			(ie_status_token_w <> '2')) or
			((cd_validacao_benef_tiss_w is null) and
			(cd_ausencia_val_benef_tiss_w is null)) then
			pls_gravar_motivo_glosa('3156', nr_seq_guia_p, null, null, expressao_pck.obter_desc_expressao(969825), nm_usuario_p, '', 'CG', nr_seq_prestador_w, null, null);
		end if;
	end if;
	
	pls_consistir_elegibilidade(nr_seq_segurado_w, 'CG', nr_seq_guia_p,
		'A', nr_seq_prestador_w, null,
		'', nm_usuario_p, cd_estabelecimento_p);

	pls_tiss_consistir_prestador(nr_seq_guia_p, 'A', 'CG',
		nr_seq_prestador_w, null, '',
		nm_usuario_p, cd_estabelecimento_p);

	pls_tiss_consistir_diagnostico(nr_seq_guia_p, 'A', 'CG',
		nr_seq_prestador_w, null, '',
		nm_usuario_p, cd_estabelecimento_p);

	pls_tiss_consistir_diaria(nr_seq_guia_p, 'A', 'CG',
		nr_seq_prestador_w, null, '',
		nm_usuario_p, cd_estabelecimento_p);

	pls_tiss_consistir_autorizacao(nr_seq_guia_p, 'A', 'CG',
		nr_seq_prestador_w, null, '',
		nm_usuario_p, cd_estabelecimento_p,null);

	if	(dt_liberacao_w is null) and
		(dt_migracao_w is not null) then
		pls_gravar_motivo_glosa('1013',nr_seq_guia_p,null,null, expressao_pck.obter_desc_expressao(969841),nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
	elsif	(dt_liberacao_w is null) then
		pls_gravar_motivo_glosa('1013',nr_seq_guia_p,null,null, expressao_pck.obter_desc_expressao(969843),nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
	end if;

	-- Tratamento feito para a virada da Unimed Blumenau,
	if	(nvl(ie_tipo_intercambio_w,'X')	<> 'E') then
		select	count(1)
		into	qt_registros_w
		from	pls_plano_acomodacao
		where	nr_seq_plano		= nr_seq_plano_w
		and	nr_seq_tipo_acomodacao	= nr_seq_tipo_acomodacao_w;

		if	(qt_registros_w = 0) and
			(ie_tipo_guia_w = '1') then

			select	nvl(max(a.nr_seq_categoria),0)
			into	nr_seq_categoria_w
			from	pls_plano_acomodacao a
			where	a.nr_seq_plano	= nr_seq_plano_w
			and	exists	(	select	1
						from	pls_regra_categoria b
						where	b.nr_seq_categoria		= a.nr_seq_categoria
						and	b.nr_seq_tipo_acomodacao	= nr_seq_tipo_acomodacao_w);
				

			if	(nr_seq_categoria_w = 0) then
				/*aaschlote 05/03/2014 - 698622*/
				pls_obter_se_sca_acomodacao(nr_seq_segurado_w,nr_seq_tipo_acomodacao_w,dt_solicitacao_w,ie_tipo_guia_w,ie_acomodacao_sca_w);

				if	(ie_acomodacao_sca_w = 'N') then
					/*aaschlote 02/04/2012 - 400228 - Verifica se na carencias do beneficiario possui acomodacao*/
					ie_acomodacao_carencia_w	:= pls_obter_carencia_cobr_acomod(nr_seq_segurado_w,nr_seq_guia_p,null);

					if	(ie_acomodacao_carencia_w = 'N') then
						pls_gravar_motivo_glosa('1413',nr_seq_guia_p,null,null, expressao_pck.obter_desc_expressao(969815),nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
					end if;
				end if;
			end if;
		end if;
	end if;
	-- fim

	if	(trunc(dt_contratacao_w,'dd') > trunc(dt_solicitacao_w,'dd')) then
		pls_gravar_motivo_glosa('1004',nr_seq_guia_p,null,null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
	end if;

	if	(dt_rescisao_w is not null) and
		(dt_solicitacao_w > dt_limite_utilizacao_w) then
		pls_gravar_motivo_glosa('1014',nr_seq_guia_p,null,null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
	end if;

	if	(cd_medico_solicitante_w is not null) then
		if	(ie_tipo_prestador_w = 'PJ') then
			select	nvl(max(cd_medico),0),
				max(ie_situacao)
			into	cd_medico_w,
				ie_situacao_w
			from	pls_prestador_medico
			where	cd_medico	= cd_medico_solicitante_w
			and	nr_seq_prestador	= nr_seq_prestador_w
			and	ie_situacao	= 'A'
			and	trunc(dt_solicitacao_w,'dd') between trunc(nvl(dt_inclusao,dt_solicitacao_w),'dd') and  fim_dia(nvl(dt_exclusao,dt_solicitacao_w));

			if	((ie_tipo_guia_w = '1') and (nvl(cd_medico_w,0) = 0)) then
				pls_gravar_motivo_glosa('9917',nr_seq_guia_p,null,null, expressao_pck.obter_desc_expressao(969839),nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
			end if;
		end if;
	elsif	((ie_tipo_guia_w <> '3') and (ie_tipo_processo_w <> 'I') and
		(nvl(cd_operadora_outorgante_w, 'X') = (nvl(cd_operadora_executora_w, 'X')))) then /* Felipe - OS 226572 Somente quando for diferente de consulta*/
		pls_gravar_motivo_glosa('9906',nr_seq_guia_p,null,null, expressao_pck.obter_desc_expressao(969851),nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
	end if;

	if	(ie_tipo_guia_w = '1') then
		select	count(1)
		into	qt_registros_w
		from	pls_diagnostico
		where	nr_seq_guia	= nr_seq_guia_p;
		
		if	(qt_registros_w = 0) then
			pls_gravar_motivo_glosa('1508',nr_seq_guia_p,null,null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
		end if;
	end if;

	if	(ie_tipo_guia_w	= '8') then
		begin	
			select	nr_seq_guia_principal
			into	nr_seq_guia_principal_w
			from	pls_guia_plano
			where	nr_sequencia	= nr_seq_guia_p;
		exception
		when others then
			nr_seq_guia_principal_w	:= 0;
		end;
		if	(nr_seq_guia_principal_w = 0) then
			/* Nao existe o Numero Guia Principal informado */
			pls_gravar_motivo_glosa('1303', nr_seq_guia_p, null, null, '', nm_usuario_p, '','CG',nr_seq_prestador_w, null,null);
		else
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_guia_ref_w
			from	pls_guia_plano
			where	nr_sequencia	= nr_seq_guia_principal_w
			and	nr_seq_segurado	= nr_seq_segurado_w
			and	(ie_status = '1' or ie_status = '2');

			if	(nr_seq_guia_ref_w = 0) then
				/* Nao existe guia de autorizacao relacionada */
				pls_gravar_motivo_glosa('1404', nr_seq_guia_p, null, null,
				expressao_pck.obter_desc_expressao(969795), nm_usuario_p, '','CG',nr_seq_prestador_w, null,null);
			else
				select	nvl(max(ie_tipo_guia),'X')
				into	ie_tipo_guia_ref_w
				from	pls_guia_plano
				where	nr_sequencia = nr_seq_guia_ref_w;

				if	(ie_tipo_guia_ref_w <> '1') then
					/* Nao existe guia de autorizacao relacionada */
					pls_gravar_motivo_glosa('1404', nr_seq_guia_p, null, null,
					expressao_pck.obter_desc_expressao(969849), nm_usuario_p, '','CG',nr_seq_prestador_w, null,null);
				end if;
			end if;
		end if;
	end if;

	if	(ie_tipo_guia_w = '2') then
		begin
			select	cd_guia_principal,
				nr_seq_guia_principal
			into	cd_guia_principal_w,
				nr_seq_guia_principal_w
			from	pls_guia_plano
			where	nr_sequencia	= nr_seq_guia_p;
		exception
		when others then
			cd_guia_principal_w	:= '0';
			nr_seq_guia_principal_w	:= null;
		end;

		if	(ie_tipo_atend_tiss_w =  '7') then
			if	(cd_guia_principal_w = '0') then
				/* Nao existe o Numero Guia Principal informado */
				pls_gravar_motivo_glosa('1303', nr_seq_guia_p, null, null, '', nm_usuario_p, '','CG',nr_seq_prestador_w, null,null);
			else
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_guia_ref_w
				from	pls_guia_plano
				where	cd_guia	= cd_guia_principal_w
				and	nr_seq_segurado	= nr_seq_segurado_w
				and	(ie_status = '1' or ie_status = '2');

				if	(nr_seq_guia_ref_w = 0) then
					/* Nao existe guia de autorizacao relacionada */
					pls_gravar_motivo_glosa('1404', nr_seq_guia_p, null, null,
					expressao_pck.obter_desc_expressao(969795), nm_usuario_p, '','CG',nr_seq_prestador_w, null,null);
				end if;
			end if;
		--Djavan 25/09/2013 - OS 647662
		elsif	(nvl(ie_tipo_atend_tiss_w,'1') <> '7') and (cd_guia_principal_w <> '0') and (nvl(ie_tipo_intercambio_w,'X')	<> 'E') then
			begin
				select	ie_tipo_guia
				into	ie_tipo_guia_princ_w
				from	pls_guia_plano
				where	nr_sequencia	= nr_seq_guia_principal_w;
			exception
			when others then
				ie_tipo_guia_princ_w	:= null;
			end;

			if	(ie_tipo_guia_princ_w	= '1') then
				pls_gravar_motivo_glosa('1302', nr_seq_guia_p, null, null,
						expressao_pck.obter_desc_expressao(969793),
						nm_usuario_p, '','CG',nr_seq_prestador_w, null,null);
			end if;
		end if;
	end if;

	if	(ie_tipo_guia_w in('1','8')) then
		select	nvl(max(qt_dia_solicitado), 0)
		into	qt_dia_solicitado_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;

		if	(qt_dia_solicitado_w = 0) then
			pls_gravar_motivo_glosa('1905', nr_seq_guia_p, null, null,
			expressao_pck.obter_desc_expressao(969845), nm_usuario_p, '','CG',nr_seq_prestador_w, null,null);
		end if;
	end if;
	
	--- Glosa 1503 - Indicador de Acidente Invalido
	if	(ie_tipo_guia_w in('1','2','3') and cd_versao_tiss_w >= '3.02.00') then
		select	max(ie_indicacao_acidente)
		into	ie_indicacao_acidente_w
		from	pls_diagnostico
		where	nr_seq_guia = nr_seq_guia_p
		and	ie_indicacao_acidente is not null;
		
		if	(nvl(ie_indicacao_acidente_w,'X') <> '0' and
			 nvl(ie_indicacao_acidente_w,'X') <> '1' and
			 nvl(ie_indicacao_acidente_w,'X') <> '2' and
			 nvl(ie_indicacao_acidente_w,'X') <> '9') and
			 ---Para as guias geradas pelo WebService ( ie_origem_solic = 'E') , so devera consistir a Glosa se o tipo de guia for de internacao
			((ie_origem_solic_w <> 'E') or (ie_origem_solic_w = 'E' and ie_tipo_guia_w = 1)) and
		--- Para as requisicoes recebidas pelo intercambio e que sejam de complemento, nao deve consistir esta glosa pois a informacao nao existe no XML de envio.
			(not ((nr_seq_guia_principal_w is not null) and (ie_tipo_processo_w = 'I') and (ie_tipo_intercambio_w = 'E'))) then
				--- Indicador de Acidente Invalido
				pls_gravar_motivo_glosa('1503',nr_seq_guia_p,null,null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
		end if;
	end if;

	select	nvl(max(cd_usuario_plano),'0')
	into	cd_usuario_plano_w
	from	pls_segurado_carteira
	where	nr_seq_segurado	= nr_seq_segurado_w
	and	dt_inicio_vigencia <= fim_dia(sysdate);

	if	(cd_usuario_plano_w = '0') then
		pls_gravar_motivo_glosa('1001',nr_seq_guia_p,null,null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
	else
		select	max(dt_validade_carteira)
		into	dt_validade_carteira_w
		from	pls_segurado_carteira
		where	cd_usuario_plano	= cd_usuario_plano_w
		and	nr_seq_segurado		= nr_seq_segurado_w;

		if	((fim_dia(dt_validade_carteira_w) < dt_solicitacao_w) and (dt_validade_carteira_w is not null)) then
			-- Conforme especificado no manual de intercambio, a operadora executora nao pode consistir validade da carteirinha do beneficiario eventual. Esta consistencia deve ficar a cargo da operdora de origem.
			if	not((nvl(ie_tipo_intercambio_w,'X')	= 'I')	and (pls_obter_se_scs_ativo	= 'A')) then
				pls_gravar_motivo_glosa('1017',nr_seq_guia_p,null,null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
			end if;
		end if;
	end if;

	-- Paulo 14/04/2008 OS 67864
	pls_define_regra_retorno(nr_seq_guia_p,null,null,'CG',nm_usuario_p);

	if	(nr_seq_plano_w = 0) then
		pls_gravar_motivo_glosa('1024',nr_seq_guia_p,null,null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
	else
		pls_consistir_proc_guia(nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p);
		pls_consistir_mat_guia(nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p);
	end if;

	-- Alex 01/03/2010 - OS - 196232, Verificar se o prestador esta cadastrado com o produto da guia
	pls_consistir_prest_plano('CG',nr_seq_prestador_w,null,nr_seq_guia_p,null,null,null,null,cd_estabelecimento_p,nm_usuario_p);

	-- Paulo 12/08/2008 - OS 103397
	pls_consistir_cpt_guia(nr_seq_guia_p, nm_usuario_p);

	-- Edgar 08/10/2007, OS 69422, tratar limitacao da guia
	pls_consistir_limitacao_guia(nr_seq_guia_p, nm_usuario_p);

	/*Diether - OS - 266284 */
	pls_consiste_rede_atend(null,nr_seq_guia_p,null,null,null,null,null,null,nm_usuario_p,cd_estabelecimento_p, nr_seq_regra_w,ie_permite_w);

	/*aaschlote 06/01/2012 - Consistir se a rede de atendimento esta em carencia*/
	pls_consistir_atend_carencia(nr_seq_segurado_w,nr_seq_plano_w,nr_seq_prestador_w,dt_solicitacao_w,nr_seq_guia_p,'CG',nm_usuario_p);

	/*aaschlore 20/08/2013 - Consistir o CID */
	pls_consistir_cid_guia_req(nr_seq_guia_p,null,nr_seq_prestador_w,cd_estabelecimento_p,nm_usuario_p);

	/* COLOCAR AS GLOSAS SEMPRE ANTES DO TESTE ABAIXO */
	ie_guia_glosa_w	:= pls_obter_se_guia_glosada(nr_seq_guia_p);
	if	(ie_guia_glosa_w	= 'N') then
		pls_consiste_regra_autorizacao(nr_seq_guia_p, 3, cd_estabelecimento_p, nm_usuario_p);
	end if;

	qt_dias_vencido_w	:= pls_obter_dias_inadimplencia(nr_seq_segurado_w);
	ie_cheque_w		:= pls_obter_se_cheque_devolucao(nr_seq_segurado_w);

	begin
	select	decode(b.cd_cgc, '','PF','PJ')
	into	ie_tipo_pagador_w
	from	pls_contrato_pagador	b,
		pls_segurado		a
	where	a.nr_sequencia		= nr_seq_segurado_w
	and	a.nr_seq_pagador	= b.nr_sequencia;
	exception
		when others then
		ie_tipo_pagador_w	:= 'A';
	end;

	pls_verifc_regra_exigencia_cid(null, nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p);

	if	(nvl(ie_tipo_atend_tiss_w,'X')	= 'X') and (nvl(ie_tipo_guia_w,'X')	= '2') then
		pls_gravar_motivo_glosa('1602',nr_seq_guia_p,null,null,'',nm_usuario_p,'','CG',nr_seq_prestador_w, null,null);
	end if;

	/*pls_gerar_ocorrencia(nr_seq_segurado_w, null, nr_seq_guia_p,
			null, null, null,
			null, null, null,
			ie_tipo_guia_w, nr_seq_plano_w, 'A',
			qt_dias_vencido_w, ie_tipo_pagador_w, nr_seq_prestador_w,
			null,'AC','',
			'',nm_usuario_p, cd_estabelecimento_p,
			nr_seq_uni_exec_w,'N', null, null,null);*/

	pls_gerar_ocorrencia_aut(nr_seq_segurado_w, null, nr_seq_guia_p,
			null, null, null,
			null, null, null,
			ie_tipo_guia_w, nr_seq_plano_w, qt_dias_vencido_w,
			ie_tipo_pagador_w, nr_seq_prestador_w, null,
			'AC','','',
			nm_usuario_p, cd_estabelecimento_p,nr_seq_congenere_w,
			ie_cheque_w,null);

	pls_gerar_ocor_aut_combinada(	nr_seq_segurado_w, nr_seq_guia_p, null,
					null, null, null,
					null, null, null,
					nm_usuario_p, cd_estabelecimento_p);
else
	pls_consistir_proc_guia(nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p);
	pls_consistir_mat_guia(nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p);
	
	if	(nr_seq_segurado_w > 0) then
		pls_gerar_ocor_aut_combinada(	nr_seq_segurado_w, nr_seq_guia_p, null,
						null, null, null,
						null, null, null,
						nm_usuario_p, cd_estabelecimento_p);
	end if;
end if;

if	(trim(cd_guia_oper_w)	= '') or (trim(cd_guia_oper_w)	is null) then
	cd_guia_oper_w	:= to_char(nr_seq_guia_p);
end if;

update	pls_guia_plano
set	ie_status	= '2',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	cd_guia		= cd_guia_oper_w
where	nr_sequencia	= nr_seq_guia_p;

pls_guia_gravar_historico(nr_seq_guia_p, 3, '', null, nm_usuario_p);

select	count(1)
into	qt_ordem_serv_w
from	ptu_requisicao_ordem_serv
where	nr_seq_guia	= nr_seq_guia_p;

if	(nvl(ie_tipo_processo_w,'X')	= 'I') and
	(pls_obter_se_scs_ativo		= 'A') and
	(nvl(ie_tipo_intercambio_w,'X')= 'I') then
	pls_atualiza_estagio_guia_inte(nr_seq_guia_p,'N', nm_usuario_p, cd_estabelecimento_p);
	
elsif	(nvl(ie_tipo_processo_w,'X')	= 'I') and
	(nvl(ie_tipo_intercambio_w,'X')	= 'E') and
	(nvl(qt_ordem_serv_w,0)		> 0  ) and
	(pls_obter_se_scs_ativo		= 'A') then
-- Tratamento feito para Autorizacoes que possuam Solicitacoes de Ordem de Servico vinculadas

	update	pls_guia_plano_proc
	set	ie_status	= 'I',
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_seq_guia	= nr_seq_guia_p;
	
	update	pls_guia_plano_mat
	set	ie_status	= 'I',
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_seq_guia	= nr_seq_guia_p;
	
	update	pls_guia_plano
	set	ie_status	= '2',
		ie_estagio	= '12',
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_guia_p;
	
else
	pls_atualizar_estagio_guia(nr_seq_guia_p, nm_usuario_p,null);

	if	(nvl(ie_tipo_processo_w,'X')	= 'I') and
		(nvl(ie_tipo_intercambio_w,'X')	= 'I') and
		(pls_obter_se_scs_ativo		= 'I') then
		pls_atualiza_estagio_guia_inte(nr_seq_guia_p,'N', nm_usuario_p, cd_estabelecimento_p);
	end if;
end if;

/* Eder 28/10/2010 procedure utilizada para verificar se a guia necessita de complemento ou nao */
pls_consiste_guia_complemento(nr_seq_guia_p, nr_seq_prestador_w, nr_seq_segurado_w, cd_estabelecimento_p, nm_usuario_p);

/*Diego 17/05/2011 - OS 315626 - Obter  e atualizar se a guia e de liberacao automatica*/
pls_atualizar_conta_aut(nr_seq_guia_p, nm_usuario_p, cd_estabelecimento_p);

commit;

/*aaschlote 16/10/2013 656658 - Rotina generica para a consistencia da liminar juridica de ocorrencia*/
pls_consist_lim_ocorr_req_guia(nr_seq_segurado_w,null,nr_seq_guia_p,ie_tipo_intercambio_w,cd_estabelecimento_p,nm_usuario_p);

/*Jucimara - OS 869458 - Regra de validacao da justificativa do prestador, deixar essa validacao SEMPRE no final da rotina, pois a mesma verifica se foi gerada ou nao analise da autorizacao */
if	(ie_origem_solic_w = 'P') then
	pls_verif_regra_prest_just_web(nr_seq_guia_p,null,nm_usuario_p);
end if;

if	(nr_seq_atend_pls_w is null) then
	/* Gerar e gravar o protocolo de atendimento */
	pls_gravar_protocolo_atend(	1, nr_seq_segurado_w, null, 
					nr_seq_guia_p, null, null, 
					null, null,null,
					null, cd_estabelecimento_p, nm_usuario_p, 
					nr_seq_protocolo_atend_w, nr_protocolo_w);
end if;

end pls_consistir_guia;
/