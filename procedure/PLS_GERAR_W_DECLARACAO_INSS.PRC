create or replace
procedure pls_gerar_w_declaracao_inss
			(	cd_pessoa_fisica_p		varchar2,
				nm_usuario_p			varchar2) is 

dt_inicial_w				date;
dt_atual_w				date;
vl_inss_w				number(15,2);
vl_rendimento_w				number(15,2);
				
begin

delete from w_pls_declaracao_inss where nm_usuario = nm_usuario_p;

select	trunc(min(a.dt_vencimento_atual),'month')
into	dt_inicial_w
from	titulo_pagar a 
where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;

dt_atual_w	:= dt_inicial_w;

while dt_atual_w <= trunc(sysdate,'month') loop 
	begin
	select	nvl(sum(a.vl_titulo),0),
		nvl(sum(b.vl_imposto),0)
	into	vl_rendimento_w,
		vl_inss_w
	from	tributo			c,
		titulo_pagar_imposto	b,
		titulo_pagar		a
	where	a.nr_titulo		= b.nr_titulo(+)
	and	b.cd_tributo		= c.cd_tributo
	and	c.ie_tipo_tributo	= 'INSS'
	and	trunc(a.dt_vencimento_atual,'month') = dt_atual_w
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
	insert into w_pls_declaracao_inss
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_pessoa_fisica,
				dt_competencia,
				vl_rendimento,
				vl_inss)
			values	(w_pls_declaracao_inss_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_pessoa_fisica_p,
				dt_atual_w,
				vl_rendimento_w,
				vl_inss_w);
	
	select	add_months(trunc(dt_atual_w,'month'),1)
	into	dt_atual_w
	from	dual;
	
	end;
end loop;

commit;

end pls_gerar_w_declaracao_inss;
/