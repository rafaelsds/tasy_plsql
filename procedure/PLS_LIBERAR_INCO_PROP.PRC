create or replace
procedure pls_liberar_inco_prop
		(	nr_seq_proposta_p	number,
			nm_usuario_p		Varchar2) is 

nr_seq_validacao_w	number(10);
			
Cursor C01 is
	select	nr_sequencia
	from	pls_proposta_validacao
	where	nr_seq_proposta	= nr_seq_proposta_p;
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_validacao_w;
exit when C01%notfound;
	begin
	
	pls_proposta_liberar_inco(nr_seq_validacao_w,nm_usuario_p);
	
	end;
end loop;
close C01;

end pls_liberar_inco_prop;
/
