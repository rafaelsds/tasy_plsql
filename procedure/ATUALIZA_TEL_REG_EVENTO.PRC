create or replace
procedure Atualiza_tel_reg_evento(
				nr_sequencia_p		number,
				ie_processado_p		number,
				ds_erro_p		varchar2,
				nm_usuario_p		varchar2) is 

begin

if	(nr_sequencia_p is not null and nr_sequencia_p > 0) then

	if	((ds_erro_P is null or ds_erro_p = '')
		and ie_processado_p is not null and ie_processado_p >= 0) then
		update 	tel_reg_evento 
		set 	ie_processado = ie_processado_p 
		where 	nr_sequencia = nr_sequencia_p;
	elsif	(ds_erro_P is not null or ds_erro_p <> '') then
		update 	tel_reg_evento 
		set 	ds_erro	= ds_erro_p
		where 	nr_sequencia = nr_sequencia_p;
	end if;

end if;

commit;

end Atualiza_tel_reg_evento;
/