create or replace procedure pfcs_tele_discontinue_orders(	nr_seq_indicator_p		number,
															cd_establishment_p		number,
															nm_user_p				varchar2) is

pfcs_panel_detail_seq_w		pfcs_panel_detail.nr_sequencia%type;
nr_seq_panel_w				pfcs_panel.nr_sequencia%type;
qt_time_telemetry_rule_w	pfcs_telemetry_config.qt_time_alarm_trend_rule%type;
ds_bed_location_w			pfcs_detail_bed.ds_location%type;
qt_total_w					number(15) := 0;

--Declare cursor begins
/* Cursor to calculate discharge count for pfcs */
cursor c01_discontinue_orders is
	select  enc.id_encounter nr_encounter,
			enc.nr_sequencia nr_seq_encounter,
			pat.patient_id id_patient,
			pfcs_get_human_name(enc.nr_seq_patient, 'Patient') nm_patient,
			pfcs_get_tele_time(dev.nr_sequencia, 'S') dt_recommendation_time,
			pfcs_get_monitor_entrance(dev.nr_sequencia) dt_monitor_entrance,
			enc.ds_reason ds_diagnosis,
			pat.birthdate dt_birthdate,
			trunc((months_between(nvl(pat.deceased_date, sysdate), pat.birthdate)/12)) qt_idade_paciente,
			pat.gender ds_gender,
			enc.ds_classification ds_classification,
			enc.period_start dt_entrance,
			pfcs_get_human_name(pfcs_get_practitioner_seq(enc.nr_sequencia, '405279007'), 'Practitioner') ds_attending_physician,
			pfcs_get_code_status_tl(pat.nr_sequencia,'S') ds_dnr_status,
			sa.ds_setor_atendimento,
			uni.cd_unidade_basica,
			uni.cd_unidade_compl,
			dev.ds_device_name ds_equipamento,
			dev.id_device cd_equipamento,
			pfcs_get_alarms_count(pat.nr_sequencia, 'A0402', qt_time_telemetry_rule_w) nr_yellow_alarm_count,
			pfcs_get_alarms_count(pat.nr_sequencia, 'A0401', qt_time_telemetry_rule_w) nr_red_alarm_count,
			pfcs_get_battery_status(pat.nr_sequencia) battery_status,
			pfcs_get_lace_plus(enc.nr_sequencia, enc.nr_seq_organization) ds_laceplus,
			nvl(pfcs_get_checklist(enc.nr_sequencia, pat.nr_sequencia, 'MPL'),'') ds_care_status,
			nvl(pfcs_get_checklist(enc.nr_sequencia, pat.nr_sequencia, 'TOOLTIP'),'') ds_checklist,
			pfcs_get_edi_score(enc.nr_sequencia) as nr_edi_score,
			pfcs_obs_contributor_pck.get_laceplus(enc.nr_sequencia) ds_readm_risk_contrb,
			pfcs_obs_contributor_pck.get_edi_vital_warnings(enc.nr_sequencia) ds_edi_vitals_warn,
			pfcs_obs_contributor_pck.get_edi_contributors(enc.nr_sequencia) ds_edi_contrb
	  from	pfcs_service_request sr,
			pfcs_encounter enc,
			pfcs_patient pat,
			pfcs_device dev,
			pfcs_encounter_location el,
			unidade_atendimento uni,
			setor_atendimento sa
	 where	sr.si_status = 'ACTIVE'
	   and	sr.cd_service = 'E0403'
	   and	sr.nr_seq_encounter = enc.nr_sequencia
	   and	enc.si_status in ('PLANNED', 'ARRIVED')
	   and	enc.nr_seq_patient = pat.nr_sequencia
	   and	pat.ie_active = '1'
	   and	pat.nr_sequencia = dev.nr_seq_patient
	   and	dev.si_status = 'ACTIVE'
	   and	dev.ds_device_type = 'Monitor'		
	   and	el.nr_seq_encounter = enc.nr_sequencia
	   and	uni.nr_seq_location = el.nr_seq_location
	   and	uni.ie_situacao = 'A'
	   and	uni.cd_setor_atendimento = sa.cd_setor_atendimento
	   and	sa.ie_situacao = 'A'
	   and	sa.cd_classif_setor in ('1','3','4','9','11','12')
	   and	sa.cd_estabelecimento_base = cd_establishment_p;

begin
	-- Get hours configurable from telemetry settings
	select	nvl(max(qt_time_alarm_trend_rule),12)
	  into	qt_time_telemetry_rule_w
	  from	pfcs_telemetry_config;

	for c01 in c01_discontinue_orders loop
		qt_total_w := qt_total_w + 1;
		select pfcs_panel_detail_seq.nextval into pfcs_panel_detail_seq_w from dual;

		ds_bed_location_w := '';
		if (c01.ds_setor_atendimento is not null) then
			ds_bed_location_w := c01.ds_setor_atendimento || '-';
		end if;
		ds_bed_location_w := ds_bed_location_w || c01.cd_unidade_basica || '-' || c01.cd_unidade_compl;

		pfcs_pck_v2.pfcs_insert_details(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p	=> cd_establishment_p,
			nm_usuario_p => nm_user_p,
			nr_panel_detail_seq_p => pfcs_panel_detail_seq_w,
			nr_encounter_p => c01.nr_encounter,
			nr_seq_encounter_p => c01.nr_seq_encounter,
			id_patient_p => c01.id_patient,
			nm_patient_p => c01.nm_patient,
			dt_birthdate_p => c01.dt_birthdate,
			ds_primary_diagnosis_p => c01.ds_diagnosis,
			ds_gender_p => c01.ds_gender,
			ds_dnr_status_p => c01.ds_dnr_status,
			dt_monitor_entrance_p => c01.dt_monitor_entrance,
			qt_time_telemetry_p => c01.dt_recommendation_time,
			ds_classification_p => c01.ds_classification,
			dt_entrance_p => c01.dt_entrance,
			ds_service_line_p => c01.ds_attending_physician,
			cd_department_p => c01.ds_setor_atendimento,
			ds_department_p => c01.ds_setor_atendimento,
			ds_bed_location_p => ds_bed_location_w,
			qt_red_alarms_p => c01.nr_red_alarm_count,
			qt_yellow_alarms_p => c01.nr_yellow_alarm_count,
			cd_equipamento_p => c01.cd_equipamento,
			ds_device_p => c01.ds_equipamento,
			ds_battery_status_p => c01.battery_status,
			ds_age_range_p => c01.qt_idade_paciente,
			ds_readmission_risk_p => c01.ds_laceplus,
			ds_care_status_p => c01.ds_care_status,
			ds_checklist_p => c01.ds_checklist,
			nr_edi_score_p =>  c01.nr_edi_score,
			ds_readm_risk_contrb_p =>  c01.ds_readm_risk_contrb,
			ds_edi_vitals_warn_p =>  c01.ds_edi_vitals_warn,
			ds_edi_contrb_p =>  c01.ds_edi_contrb);
	end loop;

	pfcs_pck_v2.pfcs_generate_results(
		vl_indicator_p => qt_total_w,
		ds_reference_value_p => null,
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_establishment_p,
		nm_usuario_p => nm_user_p,
		nr_seq_panel_p => nr_seq_panel_w);

	pfcs_pck_v2.pfcs_update_detail (
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_panel_p => nr_seq_panel_w,
		nr_seq_operational_level_p => cd_establishment_p,
		nm_usuario_p => nm_user_p );

	pfcs_pck_v2.pfcs_activate_records (
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_establishment_p,
		nm_usuario_p => nm_user_p );

end pfcs_tele_discontinue_orders;
/
