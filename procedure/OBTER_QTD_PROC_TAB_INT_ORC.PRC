create or replace
procedure Obter_Qtd_Proc_Tab_Int_Orc
		(nr_seq_interno_p		in 	number,
		nr_orcamento_p		in 	number,
		qt_procedimento_p		out 	number) is

nr_atendimento_w			number(10,0);
cd_convenio_w			number(05,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
cd_edicao_amb_w			number(06,0);
cd_estabelecimento_w		number(04,0);
ie_tipo_atendimento_w		number(03,0);
cd_categoria_w			varchar2(10);
vl_retorno_w			number(15,4);
ie_preco_informado_w		varchar2(10);
ie_glosa_w			varchar2(10);
cd_edicao_ajuste_w		number(06,0);
ie_atend_retorno_w			varchar2(01);
ie_prioridade_edicao_w		varchar2(01);	
IE_PRIORIDADE_AJUSTE_PROC_w	varchar2(01);
VL_CH_HONORARIOS_W		NUMBER(15,4)	:= 1;
VL_CH_CUSTO_OPER_W		NUMBER(15,4)	:= 1;
VL_M2_FILME_W			NUMBER(15,4)	:= 0;
tx_ajuste_geral_w			number(15,4)	:= 1;
dt_inicio_vigencia_w		date;
dt_conta_w			date := sysdate;
ie_autor_particular_w		varchar2(1);
cd_convenio_glosa_ww		number(5);
cd_categoria_glosa_ww		varchar2(10);
nr_seq_ajuste_proc_ww		number(10);
qt_procedimento_w			number(9,3);
cd_setor_atendimento_w		number(6,0);
nr_seq_cbhpm_edicao_w		number(10,0);
dt_vigencia_w			date;
cd_setor_prescricao_w		number(6,0);
cd_medico_resp_w			varchar2(10);
ie_carater_inter_sus_w		varchar2(2);
nr_seq_origem_w			number(10,0);
nr_seq_classif_medico_w		atendimento_paciente.nr_seq_classif_medico%type;

Cursor C01 is 
select	qt_procedimento
from	proc_interno_conv
where	nr_seq_proc_interno	= nr_seq_interno_p
and	((cd_convenio		= cd_convenio_w OR cd_convenio IS NULL))
and	((cd_edicao_amb		= cd_edicao_amb_w OR cd_edicao_amb IS NULL))
and	((ie_tipo_atendimento	= ie_tipo_atendimento_w OR ie_tipo_atendimento IS NULL))
and 	((cd_setor_atendimento  = cd_setor_atendimento_w) or (cd_setor_atendimento is null))
and 	((cd_estabelecimento   = nvl(cd_estabelecimento_w, wheb_usuario_pck.get_cd_estabelecimento)) or (cd_estabelecimento is null))
and	dt_vigencia_w between nvl(dt_inicio_vigencia, dt_vigencia_w) and nvl(dt_final_vigencia, dt_vigencia_w)
order by 
	nvl(cd_edicao_amb,0),
	nvl(cd_convenio,0),
	nvl(ie_tipo_atendimento,0),
	nvl(cd_setor_atendimento,0),
	nvl(cd_estabelecimento,0);


begin

dt_vigencia_w	:= trunc(sysdate);

select	max(cd_procedimento),
	max(ie_origem_proced)
into	cd_procedimento_w,
	ie_origem_proced_w
from	proc_interno
where	nr_sequencia		= nr_seq_interno_p;

select	max(cd_convenio),
	max(cd_categoria),
	max(cd_setor_atendimento),
	max(cd_estabelecimento),
	max(ie_tipo_atendimento),
	max(nr_atendimento)
into	cd_convenio_w,
	cd_categoria_w,
	cd_setor_atendimento_w,
	cd_estabelecimento_w,
	ie_tipo_atendimento_w,
	nr_atendimento_w
from	orcamento_paciente
where	nr_sequencia_orcamento = nr_orcamento_p;

if	(nr_atendimento_w > 0) then
	begin

	select	decode(nr_atend_original, null, 'N', 'S'),
		cd_medico_resp,
		ie_carater_inter_sus,
		nvl(to_number(obter_dados_categ_conv(nr_atendimento,'OC')),0),
		nvl(nr_seq_classif_medico,0)
	into	ie_atend_retorno_w,
		cd_medico_resp_w,
		ie_carater_inter_sus_w,
		nr_seq_origem_w,
		nr_seq_classif_medico_w
	from	atendimento_paciente
	where	nr_atendimento		= nr_atendimento_w;

	select	nvl(max(ie_prioridade_edicao_amb), 'N'),
		nvl(max(IE_PRIORIDADE_AJUSTE_PROC), 'N')
	into	ie_prioridade_edicao_w,
		IE_PRIORIDADE_AJUSTE_PROC_w
	from	parametro_faturamento
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(cd_convenio_w = 0) then
		select	nvl(max(obter_convenio_atendimento(nr_atendimento_w)),0)
		into	cd_convenio_w
		from	dual;

		select	nvl(max(obter_categoria_atendimento(nr_atendimento_w)),0)
		into	cd_categoria_w
		from	dual;

	end if;
	
	if	(ie_prioridade_edicao_w = 'N') then	
		SELECT	nvl(max(CD_EDICAO_AMB),0)
		INTO	CD_EDICAO_AMB_W
		FROM 	CONVENIO_AMB
		WHERE 	(CD_ESTABELECIMENTO     = cd_estabelecimento_w)
		AND 	(CD_CONVENIO            = cd_convenio_w)
  		AND 	(nvl(IE_SITUACAO,'A')   = 'A')
		and	((cd_categoria_w = 0) or (cd_categoria = cd_categoria_w))
  		AND 	(DT_INICIO_VIGENCIA     = 
				(SELECT	MAX(DT_INICIO_VIGENCIA)
				FROM 	CONVENIO_AMB A	
				WHERE 	(A.CD_ESTABELECIMENTO  	= cd_estabelecimento_w)
				AND 	(A.CD_CONVENIO         	= CD_CONVENIO_w)
				AND 	(nvl(A.IE_SITUACAO,'A')	= 'A')
				and	((cd_categoria_w = 0) or (a.cd_categoria = cd_categoria_w))
				AND 	(A.DT_INICIO_VIGENCIA 	<=  sysdate)));
	else
		Obter_Edicao_Proc_Conv
			(cd_estabelecimento_w,
			CD_CONVENIO_W,
			CD_CATEGORIA_W,
			dt_conta_w,
			cd_procedimento_w,
			CD_EDICAO_AMB_W,
			VL_CH_HONORARIOS_W,
			VL_CH_CUSTO_OPER_W,
			VL_M2_FILME_W,
			dt_inicio_vigencia_w,
			tx_ajuste_geral_w,
			nr_seq_cbhpm_edicao_w);
	end if;
	
	begin
	obter_regra_ajuste_proc
			(cd_estabelecimento_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			null,
			sysdate,0,0,0,null,
			0,0,null,0,
			null,null,null,null,null,
			vl_retorno_w,vl_retorno_w,vl_retorno_w,
			vl_retorno_w,vl_retorno_w,vl_retorno_w,
			ie_preco_informado_w,ie_glosa_w,
			vl_retorno_w,vl_retorno_w,
			cd_edicao_ajuste_w,vl_retorno_w,vl_retorno_w,vl_retorno_w,
			vl_retorno_w,vl_retorno_w,vl_retorno_w,vl_retorno_w,
			vl_retorno_w, ie_atend_retorno_w, 0, ie_autor_particular_w,
			cd_convenio_glosa_ww,
			cd_categoria_glosa_ww,
			nr_seq_ajuste_proc_ww,null,null,
			null,null,null,null, null,
			null,vl_retorno_w,null,cd_setor_prescricao_w,null,
			cd_medico_resp_w,
			ie_carater_inter_sus_w,
			null,
			null,
			nr_seq_origem_w,
			null,
			nr_seq_classif_medico_w);	
	exception
	when others then
		cd_edicao_ajuste_w	:= 0;
	end;
		

	if	(cd_edicao_ajuste_w is not null) and
		(cd_edicao_ajuste_w <> 0) then
		cd_edicao_amb_w		:= cd_edicao_ajuste_w;
	end if;
	
	if	(cd_convenio_w > 0) or
		(cd_edicao_amb_w > 0) or
		(ie_tipo_atendimento_w > 0) or
		(cd_setor_atendimento_w > 0) then 
		
		OPEN C01;
		LOOP
		FETCH C01 into
			qt_procedimento_w;
		exit when c01%notfound;
		END LOOP;
		CLOSE C01;

 	end if; 

	end;
end if;

qt_procedimento_p	:= qt_procedimento_w;

end Obter_Qtd_Proc_Tab_Int_Orc;
/
