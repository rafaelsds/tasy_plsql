create or replace
procedure w_lab_gerar_mapa_exame(	cd_mapa_p		number,
									nm_usuario_p	varchar2) is

cursor c01 is	
	SELECT 	DISTINCT	
			b.nr_seq_exame,
			'U'|| ' - ' || c.nm_exame nm_exame,
			c.cd_exame,
			c.nr_seq_apresent,
			0 linha,
			0 ie_urgente,
			c.nr_seq_apresent apresentacao
	FROM (	
				SELECT nr_prescricao,
					nr_seq_prescr,
					linha
				FROM  (	
					  SELECT	ROWNUM linha,
								a.nr_prescricao,
								a.nr_seq_prescr	  		 						   				   
								FROM lab_impressao_mapa a
								WHERE a.cd_mapa = cd_mapa_p
								ORDER BY a.nr_prescricao)) a,
		prescr_procedimento b,
		exame_laboratorio c
	WHERE a.nr_prescricao = b.nr_prescricao
	AND		a.nr_seq_prescr = b.nr_sequencia
	AND		b.nr_seq_exame = c.nr_seq_exame
	AND		LAB_OBTER_TIPO_ATEND_MAPA(cd_mapa_p) = 1
	AND		b.ie_urgencia = 'S'
	UNION ALL
	SELECT 	DISTINCT	
			b.nr_seq_exame,
			c.nm_exame nm_exame,
			c.cd_exame,
			c.nr_seq_apresent,
			0 linha,
			1 ie_urgente,
			c.nr_seq_apresent apresentacao
	FROM (	
				SELECT nr_prescricao,
					nr_seq_prescr,
					linha
				FROM  (	
					  SELECT	ROWNUM linha,
								a.nr_prescricao,
								a.nr_seq_prescr	  		 						   				   
								FROM lab_impressao_mapa a
								WHERE a.cd_mapa = cd_mapa_p
								ORDER BY a.nr_prescricao)) a,
		prescr_procedimento b,
		exame_laboratorio c
	WHERE a.nr_prescricao = b.nr_prescricao
	AND		a.nr_seq_prescr = b.nr_sequencia
	AND		b.nr_seq_exame = c.nr_seq_exame
	AND		b.ie_urgencia = 'N'
	AND		LAB_OBTER_TIPO_ATEND_MAPA(cd_mapa_p) = 1
	UNION ALL 
	SELECT 	DISTINCT	
			b.nr_seq_exame,
			c.nm_exame nm_exame,
			c.cd_exame,
			c.nr_seq_apresent,
			0 linha,
			2 ie_urgente,
			c.nr_seq_apresent apresentacao
	FROM (	
				SELECT nr_prescricao,
					nr_seq_prescr,
					linha
				FROM  (	
					  SELECT	ROWNUM linha,
								a.nr_prescricao,
								a.nr_seq_prescr	  		 						   				   
								FROM lab_impressao_mapa a
								WHERE a.cd_mapa = cd_mapa_p
								ORDER BY a.nr_prescricao)) a,
		prescr_procedimento b,
		exame_laboratorio c
	WHERE a.nr_prescricao = b.nr_prescricao
	AND		a.nr_seq_prescr = b.nr_sequencia
	AND		b.nr_seq_exame = c.nr_seq_exame
	AND		LAB_OBTER_TIPO_ATEND_MAPA(cd_mapa_p) <> 1
	ORDER BY ie_urgente, apresentacao;
c01_w	c01%rowtype;	


cursor c02 is
	SELECT  c01_w.nr_seq_exame nr_seq_exame,
			b.nr_seq_exame nr_seq_analito,
			Obter_Desc_Exame(b.nr_seq_exame) nm_analito,
			c01_w.linha linha,
			nr_seq_apresent,
			2 ie_exame,
			c01_w.ie_urgente ie_urgente
	FROM   	exame_laboratorio b
	WHERE   b.ie_imprime_mapa = 'S' 
	START WITH b.nr_seq_superior = c01_w.nr_seq_exame
	CONNECT BY PRIOR nr_seq_exame = b.nr_seq_superior
	UNION
	SELECT  c01_w.nr_seq_exame nr_seq_exame,
			b.nr_seq_exame nr_seq_analito,
			Obter_Desc_Exame(b.nr_seq_exame) nm_analito,
			c01_w.linha linha,
			nr_seq_apresent,
			1 ie_exame,
			c01_w.ie_urgente ie_urgente
	FROM   	exame_laboratorio b
	WHERE   b.ie_imprime_mapa = 'S' 
	AND 	nr_seq_exame = c01_w.nr_seq_exame
	AND 	NOT EXISTS (SELECT		   1
						   FROM			   exame_laboratorio 
						   WHERE 		   nr_seq_superior = c01_w.nr_seq_exame
						   AND 			   ie_imprime_mapa = 'S')   
	ORDER BY NR_SEQ_APRESENT;

c02_w	c02%rowtype;
	
nr_linha_w			number(10);
qt_total_w			number(10);
qt_linhas_atual_w	number(10);
	
begin

nr_linha_w	:= 0;
qt_total_w	:= 0;

delete w_mapa_trabalho where nm_usuario = nm_usuario_p;

open c01;
loop
fetch c01 into c01_w;
	exit when c01%notfound;
	begin
	
	select	count(*)+1
	into	qt_linhas_atual_w
	from	(SELECT 
					c01_w.nr_seq_exame nr_seq_exame,
					b.nr_seq_exame nr_seq_analito,
					Obter_Desc_Exame(b.nr_seq_exame) nm_analito,
					c01_w.linha linha,
					nr_seq_apresent,
					2 ie_exame,
					c01_w.ie_urgente ie_urgente
			FROM   	exame_laboratorio b
			WHERE   b.ie_imprime_mapa = 'S' 
			START WITH b.nr_seq_superior = c01_w.nr_seq_exame
			CONNECT BY PRIOR nr_seq_exame = b.nr_seq_superior
			UNION
			SELECT  c01_w.nr_seq_exame nr_seq_exame,
					b.nr_seq_exame nr_seq_analito,
					Obter_Desc_Exame(b.nr_seq_exame) nm_analito,
					c01_w.linha linha,
					nr_seq_apresent,
					1 ie_exame,
					c01_w.ie_urgente ie_urgente
			FROM   	exame_laboratorio b
			WHERE   b.ie_imprime_mapa = 'S' 
			AND 	nr_seq_exame = c01_w.nr_seq_exame
			AND 	NOT EXISTS (SELECT		   1
								   FROM			   exame_laboratorio 
								   WHERE 		   nr_seq_superior = c01_w.nr_seq_exame
								   AND 			   ie_imprime_mapa = 'S')   
			ORDER BY NR_SEQ_APRESENT);

		if (((qt_linhas_atual_w+qt_total_w) >= 42) and
			(qt_linhas_atual_w <> 0)) then
			nr_linha_w			:= nr_linha_w+1;
			qt_total_w			:= qt_linhas_atual_w;
		else		
			qt_total_w			:= qt_linhas_atual_w+qt_total_w;
		end if;
		
		insert into w_mapa_trabalho(
				nr_linha,
				nr_seq_exame,
				nm_usuario
				)
		values	(nr_linha_w,
				c01_w.nr_seq_exame,
				nm_usuario_p);
		
		open c02;
		loop
		fetch c02 into c02_w;
			exit when c02%notfound;
			begin
			
			insert into w_mapa_trabalho(
				nr_linha, 			
				nr_seq_exame,
				nr_seq_analito,
				nm_usuario
				)
			values	(nr_linha_w,
				c02_w.nr_seq_exame,
				c02_w.nr_seq_analito,
				nm_usuario_p);
			end;
			
		end loop;
		close c02;

		if (qt_linhas_atual_w > 42) then
			nr_linha_w := nr_linha_w+1;
			qt_total_w	:= mod(qt_linhas_atual_w,42);
		end if;		
	end;
end loop;
close c01;

commit;

end;
/
