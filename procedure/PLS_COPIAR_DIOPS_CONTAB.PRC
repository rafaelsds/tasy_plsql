create or replace
procedure pls_copiar_diops_contab
			(	nr_seq_diops_p		number,
				nr_seq_diops_ant_p	number,
				cd_estab_destino_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_diops_contab_w		number(10);
nr_seq_diops_contab_ant_w	number(10);
qt_registro_w			number(10);

Cursor C01 is
	select	nr_sequencia
	from	diops_trans_conta_contab
	where	nr_seq_trans_conta	= nr_seq_diops_ant_p;

begin
open C01;
loop
fetch C01 into	
	nr_seq_diops_contab_ant_w;
exit when C01%notfound;
	begin
	
	select	diops_trans_conta_contab_seq.nextval
	into	nr_seq_diops_contab_w
	from	dual;
	
	select	count(*)
	into	qt_registro_w
	from	diops_trans_conta_contab
	where	cd_estabelecimento	= cd_estab_destino_p
	and	nr_seq_anterior		= nr_seq_diops_contab_ant_w;
	
	if	(qt_registro_w	= 0) then
		insert into  diops_trans_conta_contab
			(nr_sequencia, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_trans_conta, 
			cd_conta_contabil, cd_estabelecimento, ie_acao_conta,
			nr_seq_anterior)
		select	nr_seq_diops_contab_w, sysdate, nm_usuario_p,
			sysdate,  nm_usuario_p, nr_seq_diops_p, 
			cd_conta_contabil, cd_estab_destino_p, ie_acao_conta,
			nr_seq_diops_contab_ant_w
		from	diops_trans_conta_contab
		where	nr_sequencia	= nr_seq_diops_contab_ant_w;
	end if;
	
	end;
end loop;
close C01;

commit;

end pls_copiar_diops_contab;
/
