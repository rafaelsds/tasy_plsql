create or replace
procedure pls_gerar_dados_guia_proc_imp (	cd_procedimento_imp_p		pls_guia_plano_proc_imp.cd_procedimento%type,
						cd_tabela_imp_p			pls_guia_plano_proc_imp.cd_tabela%Type,
						nr_seq_prestador_imp_p		pls_prestador.nr_sequencia%type,
						cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
						cd_procedimento_p	out	procedimento.cd_procedimento%type,
						ie_origem_proced_p	out	procedimento.ie_origem_proced%type) is 
			
			
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar as convers�es nos procedimentos, para retornar o c�digo e a origem 
correta dos mesmos
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_tipo_conv_tiss_w		pls_cta_valorizacao_pck.dados_tipo_conv_tiss;
ie_origem_proced_w		procedimento.ie_origem_proced%type;
cd_procedimento_w		procedimento.cd_procedimento%type;

begin

if	( cd_procedimento_imp_p is not null and cd_tabela_imp_p is not null ) then

	dados_tipo_conv_tiss_w	:= pls_obter_conversao_tab_tiss(cd_tabela_imp_p, cd_estabelecimento_p, cd_procedimento_imp_p, '', 'A', nr_seq_prestador_imp_p, null);
		
	if	( dados_tipo_conv_tiss_w.ie_tipo_tabela in ('06')) then
		ie_origem_proced_w	:= 5; --CBHPM
	elsif	( dados_tipo_conv_tiss_w.ie_tipo_tabela in ('18','22')) then
		ie_origem_proced_w	:= 8; --TUSS 		
	elsif	(dados_tipo_conv_tiss_w.ie_tipo_tabela in('90','98','00')) then
		ie_origem_proced_w	:= 4; --PROPRIO 				
	end if;
	cd_procedimento_w :=  cd_procedimento_imp_p;
	
	
	/* Tratar a convers�o de procedimentos TUSS - OPS - Cadastro de Regras / Procedimentos TUSS */
	if	( ie_origem_proced_w	= 8) then
		pls_converte_codigo_tuss( cd_procedimento_w, ie_origem_proced_w, cd_procedimento_w, ie_origem_proced_w);
	end if;
end if;

cd_procedimento_p	:= cd_procedimento_w;	
ie_origem_proced_p	:= ie_origem_proced_w;

end pls_gerar_dados_guia_proc_imp;
/
