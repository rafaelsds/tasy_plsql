create or replace
procedure Incluir_lab_log_componente(	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					ds_maquina_p		varchar2,
					nr_seq_exame_p		number,
					ds_texto_p		varchar2,
					nm_usuario_p		Varchar2) is 

begin


insert into LAB_LOG_COMPONENTE(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_prescr,
				nm_maquina,
				nr_seq_exame,
				ds_texto)
values				(lab_log_componente_seq.nextval,
				sysdate,
				nvl(nm_usuario_p,'Tasy'),
				sysdate,
				nvl(nm_usuario_p,'Tasy'),
				nr_prescricao_p,
				nr_seq_prescr_p,
				ds_maquina_p,	
				nr_seq_exame_p,
				ds_texto_p);

commit;

end Incluir_lab_log_componente;
/
