create or replace
procedure pls_atualizar_pagador_cta (	nr_seq_protocolo_p	in pls_protocolo_conta.nr_sequencia%type,
					nr_seq_conta_p		in pls_conta.nr_sequencia%type,
					ie_commit_p		in varchar2,
					cd_estabelecimento_p	in estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		in usuario.nm_usuario%type) is
-- Carrega as contas conforme o nivel, e atualiza o pagador

ie_data_pagador_copartic_w	pls_parametros.ie_data_pagador_copartic%type;

-- Contas
cursor	c01 (	nr_seq_protocolo_pc		pls_protocolo_conta.nr_sequencia%type,
		nr_seq_conta_pc			pls_conta.nr_sequencia%type,
		ie_data_pagador_copartic_pc	pls_parametros.ie_data_pagador_copartic%type) is
	select	a.nr_sequencia,
		a.nr_seq_segurado,
		pls_obter_pagador_benef(a.nr_seq_segurado, decode(ie_data_pagador_copartic_pc, 'S', a.dt_atendimento_referencia, sysdate), '3') nr_seq_pagador
	from	pls_conta	a
	where	a.nr_seq_protocolo	= nr_seq_protocolo_pc
	and	nr_seq_conta_pc		is null
	and	a.ie_status		= 'F'
	union all
	select	a.nr_sequencia,
		a.nr_seq_segurado,
		pls_obter_pagador_benef(a.nr_seq_segurado, decode(ie_data_pagador_copartic_pc, 'S', a.dt_atendimento_referencia, sysdate), '3') nr_seq_pagador
	from	pls_conta	a
	where	a.nr_sequencia	= nr_seq_conta_pc
	and	a.ie_status		= 'F';
	
begin

-- parametros do pagador
select	nvl(max(ie_data_pagador_copartic),'N')
into	ie_data_pagador_copartic_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

-- carrega as contas
for r_c01_w in c01(nr_seq_protocolo_p, nr_seq_conta_p, ie_data_pagador_copartic_w) loop
	pls_alt_pagador_contas_benef(r_c01_w.nr_seq_segurado, r_c01_w.nr_seq_pagador, null, null, null, ie_commit_p, nm_usuario_p);
end loop;


if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_atualizar_pagador_cta;
/
