create or replace
procedure dmed_alterar_retificador(	nr_seq_dmed_anual_p	number,
				nm_usuario_p		varchar2) is 

begin

desfazer_dmed(nr_seq_dmed_anual_p,nm_usuario_p);

update	dmed
set	ie_indicador_retificadora = 'S',
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_seq_dmed_anual_p;

commit;

end dmed_alterar_retificador;
/