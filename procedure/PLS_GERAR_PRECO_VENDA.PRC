create or replace
procedure pls_gerar_preco_venda(nr_sequencia_nf_p     	nota_fiscal.nr_sequencia%type,
				nr_item_nf_p		nota_fiscal_item.nr_item_nf%type,
				nm_usuario_p		usuario.nm_usuario%type) is 


vl_unitario_item_nf_w		nota_fiscal_item.vl_unitario_item_nf%type	:= 0;
cd_material_w			nota_fiscal_item.cd_material%type		:= 0;
cd_estabelecimento_w		nota_fiscal_item.cd_estabelecimento%type	:= 0;
ie_acao_nf_w			nota_fiscal.ie_acao_nf%type;
dt_entrada_saida_w		date;
ie_preco_compra_w		material.ie_preco_compra%type;
vl_unitario_w			number(13,4)	:= 0;
nr_seq_material_w		pls_material.nr_sequencia%type;

cd_grupo_material_w		grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w		subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w		classe_material.cd_classe_material%type;

vl_unit_desc_item_nf_w		number(13,4)	:= 0;

ie_ignorar_w			pls_tab_preco_mat_regra.ie_ignora_atualizacao%type;
ie_atualizar_w			varchar2(1)	:= 'S';


Cursor C01 (	cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
		nr_seq_material_p	pls_material_preco_item.nr_seq_material%type) is
		
	select	/*+RULE*/ a.nr_sequencia nr_seq_tabela,
		c.cd_moeda cd_moeda,
		b.nr_sequencia nr_seq_preco_item,
		a.ie_atualiza_nf ie_atualiza_nf
	from	pls_material_preco	a,
		pls_material_preco_item	b,
		pls_material_valor_item	c
	where	a.cd_estabelecimento		= cd_estabelecimento_p
	and	a.ie_atualiza_nf 		= 'S'
	and	a.ie_situacao			= 'A'
	and	b.nr_seq_material_preco		= a.nr_sequencia
	and	b.nr_seq_material		= nr_seq_material_p
	and	b.ie_situacao			= 'A'
	and	c.nr_seq_preco_item		= b.nr_sequencia;
	
Cursor C03 (	cd_grupo_material_p	pls_tab_preco_mat_regra.cd_grupo_material%type,
		cd_subgrupo_material_p	pls_tab_preco_mat_regra.cd_subgrupo_material%type,
		cd_classe_material_p	pls_tab_preco_mat_regra.cd_classe_material%type,
		cd_material_p		pls_tab_preco_mat_regra.cd_material%type,
		cd_estabelecimento_p	pls_tab_preco_mat_regra.cd_estabelecimento%type,
		nr_seq_material_preco_p	pls_tab_preco_mat_regra.nr_seq_material_preco%type) is
		
	-- jjung OS 523315 - Ajustado restri��es where para melhoria de performance
	select	ie_ignora_atualizacao
	from	(
		select	nvl(ie_ignora_atualizacao,'N') ie_ignora_atualizacao,
			cd_material, cd_classe_material,
			cd_subgrupo_material, cd_grupo_material
		from	pls_tab_preco_mat_regra
		where	nr_seq_material_preco = nr_seq_material_preco_p
		and	ie_situacao = 'A'
		and	((cd_grupo_material = cd_grupo_material_p) or (cd_grupo_material is null))
		and	((cd_subgrupo_material = cd_subgrupo_material_p) or (cd_subgrupo_material is null))
		and	((cd_classe_material = cd_classe_material_p) or (cd_classe_material is null))
		and	((cd_material = cd_material_p) or (cd_material is null))
		and	((cd_estabelecimento = cd_estabelecimento_p) or (cd_estabelecimento is null))
		order by	cd_material desc,
				cd_classe_material desc,
				cd_subgrupo_material desc,
				cd_grupo_material desc
		)
	where	rownum <= 1;

begin	

begin
	-- Obter dados da NF
	select	a.vl_unitario_item_nf,
		a.cd_estabelecimento,
		a.cd_material,
		b.dt_entrada_saida,
		b.ie_acao_nf,
		c.ie_preco_compra,
		dividir(vl_liquido, qt_item_nf)
	into	vl_unitario_item_nf_w,
		cd_estabelecimento_w,
		cd_material_w,
		dt_entrada_saida_w,
		ie_acao_nf_w,
		ie_preco_compra_w,
		vl_unit_desc_item_nf_w
	from	nota_fiscal_item a,
		nota_fiscal b,
		material c
	where	a.nr_sequencia	= nr_sequencia_nf_p
	and	a.nr_item_nf	= nr_item_nf_p
	and	b.nr_sequencia 	= a.nr_sequencia
	and	c.cd_material	= a.cd_material;
exception
when others then
		cd_material_w := 0;
end;


if	(ie_preco_compra_w <> 'S') then
	cd_material_w := 0;
end if;

if	(cd_material_w <> 0) then
	
	begin
		-- Obter estrutura do MAT
		select 	b.nr_sequencia,
			a.cd_classe_material,
			c.cd_subgrupo_material,
			d.cd_grupo_material
		into 	nr_seq_material_w,
			cd_classe_material_w,
			cd_subgrupo_material_w,
			cd_grupo_material_w
		from  	material		a,
			pls_material		b,
			classe_material		c,
			subgrupo_material	d,
			grupo_material		e
		where  	a.cd_material 			= cd_material_w
		and	b.cd_material			= a.cd_material 
		and	b.ie_situacao			= 'A'
		and	c.cd_classe_material(+)		= a.cd_classe_material
		and	d.cd_subgrupo_material(+)	= c.cd_subgrupo_material
		and	e.cd_grupo_material(+)		= d.cd_grupo_material;
	exception
	when others then
		nr_seq_material_w := null;
	end;

	if	(nr_seq_material_w is not null) then

		vl_unitario_w	:= nvl(vl_unit_desc_item_nf_w,0);

		for	r_C01_w in C01 (cd_estabelecimento_w,nr_seq_material_w) loop
		
			-- Flag para ver se o pre�o do material vai ser atualizado conforme regra do campo pls_material_preco.ie_atualiza_nf
			ie_atualizar_w := 'N';
			-- Sempre atualiza
			if	(r_C01_w.ie_atualiza_nf = 'S') then
			
				ie_atualizar_w := 'S';
			-- Conforme Regra  
			elsif	(r_C01_w.ie_atualiza_nf = 'R') then
			
				ie_ignorar_w := 'N';
				
				for	r_C03_w in C03 ( cd_grupo_material_w, cd_subgrupo_material_w, cd_classe_material_w,
							 cd_material_w, cd_estabelecimento_w, r_C01_w.nr_seq_tabela) loop
							 
					ie_ignorar_w:= r_C03_w.ie_ignora_atualizacao;				
				end loop; -- C03
				
				ie_atualizar_w := 'S';
				if	(ie_ignorar_w = 'S') then
					ie_atualizar_w	:= 'N';
				end if;
			end if;
			
			-- O valor s� ser� atualizado caso a tabela ou as regras permitam isto .
			if 	(ie_atualizar_w = 'S')	then
				
				pls_atualiza_preco_tb_mat
				( nr_seq_material_w, r_C01_w.nr_seq_tabela, dt_entrada_saida_w,
				  vl_unitario_w, r_C01_w.cd_moeda, cd_estabelecimento_w, nm_usuario_p);
			end if;
		end loop; --C01
	end if;
end if;

end pls_gerar_preco_venda;
/