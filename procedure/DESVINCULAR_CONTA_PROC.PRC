create or replace
procedure desvincular_conta_proc(
			nr_sequencia_p		number) is 

begin

update	procedimento_paciente 
set	nr_seq_proc_autor = null 
where	nr_seq_proc_autor = nr_sequencia_p;

commit;

end desvincular_conta_proc;
/