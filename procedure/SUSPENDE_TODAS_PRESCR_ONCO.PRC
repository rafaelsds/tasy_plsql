create or replace
procedure suspende_todas_prescr_onco(	nr_seq_paciente_p	number,
										nm_usuario_p		Varchar2) is
nr_prescricao_w 		number(10);
nr_seq_atendimento_w 	number(10);
ie_libera_quimio_w		varchar2(1);

Cursor C01 is
	select 	nr_prescricao,
			nr_seq_atendimento
	from 	paciente_atendimento
	where 	nr_seq_paciente = nr_seq_paciente_p
	and		nr_prescricao is not null
	and		nr_atendimento is null
	and 	SUBSTR(Obter_status_Paciente_qt(nr_seq_atendimento,
											dt_inicio_adm,
											dt_fim_adm,
											nr_seq_local,
											ie_exige_liberacao,
											dt_chegada,
											'C'),1,60) <> '83';
begin

ie_libera_quimio_w := Obter_Valor_Param_Usuario(3130,279,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento);

open C01;
loop
fetch C01 into
	nr_prescricao_w,
	nr_seq_atendimento_w;
exit when C01%notfound;
	begin
		suspender_prescricao(nr_prescricao_w,0,'',nm_usuario_p, 'N');
		update 	paciente_atendimento
		set		nr_prescricao			=	'',
				dt_geracao_prescricao	=	null
		where 	nr_seq_atendimento			=	nr_seq_atendimento_w;

		if (ie_libera_quimio_w = 'S') then
			atualizar_data_quimio_liberada(nr_seq_atendimento_w,nm_usuario_p,'D');
		end if;
	end;
end loop;
close C01;	

commit;

end suspende_todas_prescr_onco;
/