create or replace
procedure Altera_Medic_Atend_ciclo_onc(		nr_seq_atendimento_p	number,
						NR_SEQ_MATERIAL_p	number,
						nm_usuario_p		Varchar2) is 


nr_seq_atendimento_w		number(10);
nr_seq_paciente_w		number(10);
nr_seq_material_w		number(10);
cd_material_w			number(10);
NR_AGRUPAMENTO_w		number(10);
ie_alterar_dose_prot_w		varchar2(1) := 'S';
cd_estabelecimento_w		number(4);	
nr_seq_diluicao_w		number(10);					
cd_material_diluido_w		number(10);

cursor c01 is
	select	*
	from	paciente_atend_medic
	where	nr_seq_atendimento	= nr_seq_atendimento_p
	and	nr_seq_material		= nr_seq_material_p;
	
c01_w	c01%rowtype;		
	
cursor c02 is
	select	a.nr_seq_atendimento,
		b.nr_seq_material
	from	paciente_atendimento a,
		paciente_atend_medic b
	where	nr_seq_diluicao_w is null
	and	a.nr_seq_atendimento	= b.nr_seq_atendimento
	and	a.nr_seq_paciente	= nr_seq_paciente_w
	and	b.cd_material		= cd_material_w
	and	b.nr_seq_diluicao is null
	and	a.nr_prescricao	is null
	union all
	select	a.nr_seq_atendimento,
		d.nr_seq_material
	from	paciente_atendimento a,
		paciente_atend_medic m,
		paciente_atend_medic d
	where	nr_seq_diluicao_w is not null
	and	a.nr_seq_atendimento	= m.nr_seq_atendimento
	and	d.nr_seq_atendimento	= a.nr_seq_atendimento
	and	d.nr_seq_diluicao	= m.nr_seq_material	
	and	a.nr_seq_paciente	= nr_seq_paciente_w
	and	m.cd_material		= cd_material_diluido_w
	and	d.cd_material		= cd_material_w
	and	a.nr_prescricao	is null;

begin
select	nvl(max(b.cd_estabelecimento),1)
into	cd_estabelecimento_w
from	paciente_setor		b,
	paciente_atendimento	a
where	a.nr_seq_paciente	= b.nr_seq_paciente
and	a.nr_seq_atendimento	= nr_seq_atendimento_p;

obter_param_usuario(281, 993, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_alterar_dose_prot_w);

wheb_usuario_pck.set_ie_executar_trigger('N');

select	nr_seq_paciente
into	nr_seq_paciente_w
from	paciente_atendimento
where	nr_seq_atendimento	= nr_seq_atendimento_p;

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	cd_material_w		:= c01_w.cd_material;
	NR_AGRUPAMENTO_w	:= c01_w.nr_agrupamento;
	nr_seq_diluicao_w	:= c01_w.nr_seq_diluicao;
	
	cd_material_diluido_w := null;
	if	(nr_seq_diluicao_w is not null) then
		select	cd_material
		into	cd_material_diluido_w
		from	paciente_atend_medic
		where	nr_seq_atendimento = nr_seq_atendimento_p
		and	nr_seq_material	= nr_seq_diluicao_w;
	end if;
    
    if	(ie_alterar_dose_prot_w = 'S') then
        UPDATE	PACIENTE_PROTOCOLO_MEDIC /*Protocolo*/
        SET	QT_DOSE_PRESCR		= c01_w.QT_DOSE_PRESCRICAO,
            CD_UNID_MED_PRESCR	= c01_w.CD_UNID_MED_PRESCR,
            QT_DOSE			    = c01_w.QT_DOSE,
            CD_UNIDADE_MEDIDA	= c01_w.CD_UNID_MED_DOSE
        WHERE	cd_material		= cd_material_w
        AND	nr_seq_paciente		= nr_seq_paciente_w 
			AND	nr_seq_paciente	= nr_seq_paciente_w 
        AND	nr_seq_paciente		= nr_seq_paciente_w 
        and	nr_agrupamento		= NR_AGRUPAMENTO_w;
    end if;

	open C02;
	loop
	fetch C02 into	
		nr_seq_atendimento_w,
		nr_seq_material_w;
	exit when C02%notfound;
		begin	
		
		update	paciente_atend_medic /*Atendimento*/
		set	CD_UNID_MED_PRESCR		= c01_w.CD_UNID_MED_PRESCR,
			QT_DOSE_PRESCRICAO		= c01_w.QT_DOSE_PRESCRICAO,
			QT_DOSE					= c01_w.QT_DOSE,
			CD_UNID_MED_DOSE		= c01_w.CD_UNID_MED_DOSE,
			NR_SEQ_VIA_ACESSO		= c01_w.NR_SEQ_VIA_ACESSO,
			IE_VIA_APLICACAO		= c01_w.IE_VIA_APLICACAO,
			IE_SE_NECESSARIO		= c01_w.IE_SE_NECESSARIO,
			IE_URGENCIA				= c01_w.IE_URGENCIA,
			CD_INTERVALO			= c01_w.CD_INTERVALO,
			QT_DIAS_UTIL			= c01_w.QT_DIAS_UTIL,
			IE_APLIC_BOLUS			= c01_w.IE_APLIC_BOLUS,
			QT_HORA_APLICACAO		= c01_w.QT_HORA_APLICACAO,
			QT_MIN_APLICACAO		= c01_w.QT_MIN_APLICACAO,
			IE_BOMBA_INFUSAO		= c01_w.IE_BOMBA_INFUSAO,
			IE_APLIC_LENTA			= c01_w.IE_APLIC_LENTA,
			IE_PRE_MEDICACAO		= c01_w.IE_PRE_MEDICACAO,
			IE_APLICA_REDUCAO		= c01_w.IE_APLICA_REDUCAO,
			PR_REDUCAO				= c01_w.PR_REDUCAO,
			IE_ACM					= c01_w.IE_ACM,
			IE_MEDICACAO_PACIENTE	= nvl(c01_w.IE_MEDICACAO_PACIENTE,'N'),
			DS_DOSE_DIFERENCIADA    = c01_w.DS_DOSE_DIFERENCIADA,
			IE_USO_CONTINUO         = c01_w.IE_USO_CONTINUO,
			DS_RECOMENDACAO         = c01_w.DS_RECOMENDACAO,
			DS_OBSERVACAO           = c01_w.DS_OBSERVACAO,
			IE_LOCAL_ADM            = c01_w.IE_LOCAL_ADM
		where	nr_seq_atendimento	= nr_seq_atendimento_w
		and	nr_seq_material		= nr_seq_material_w;	

		end;

	end loop;
	close C02;
	
	end;
end loop;
close C01;

wheb_usuario_pck.set_ie_executar_trigger('S');
commit;

end Altera_Medic_Atend_ciclo_onc;
/
