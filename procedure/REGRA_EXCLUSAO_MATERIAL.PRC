create or replace 
procedure regra_exclusao_material(				cd_material_p		number,
												nr_interno_conta_p	number,
												nm_usuario_p		varchar2,
												ds_erro_p		out	varchar2,
												vl_material_p		number,
												vl_unitario_p		number) is

					
		
qt_regra_w		number(10);
ds_erro_w		varchar2(255) := '';
qt_existe_regra_w	varchar2(1);
cd_grupo_material_w	number(3);
cd_subgrupo_material_w	number(3);
cd_classe_material_w	number(5);
ie_tipo_atendimento_w	number(3);
cd_convenio_w		number(5);
				
		
begin

select	nvl(max('S'),'N')
into	qt_existe_regra_w
from	regra_exclusao_audit_mat;

if	(qt_existe_regra_w = 'S') then

	select	nvl(cd_grupo_material,0),
			nvl(cd_subgrupo_material,0),
			nvl(cd_classe_material,0)
	into	cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w
	from	estrutura_material_v
	where	cd_material = cd_material_p;
	
	select	nvl(max(substr(obter_tipo_atendimento(nr_atendimento),1,5)),0),
			nvl(max(cd_convenio_parametro),0)
	into	ie_tipo_atendimento_w,
			cd_convenio_w
	from	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;
   
	select	count(*)
	into	qt_regra_w
	from	regra_exclusao_audit_mat
	where	ie_situacao = 'A'
	and		nvl(cd_convenio, nvl(cd_convenio_w,0)) = nvl(cd_convenio_w,0)
	and		((cd_grupo_material is null) or (nvl(cd_grupo_material,0) = nvl(cd_grupo_material_w,0)))
	and		((ie_tipo_atendimento is null) or (nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = nvl(ie_tipo_atendimento_w,0)))
	and		((cd_subgrupo_material is null) or (nvl(cd_subgrupo_material,0) = nvl(cd_subgrupo_material_w,0)))
	and		((cd_classe_material is null) or (nvl(cd_classe_material,0) = nvl(cd_classe_material_w,0)))
	and		((cd_material is null) or (nvl(cd_material,0) = nvl(cd_material_p,0)))
	and		(((ie_tipo_valor = 'U') and ((vl_unitario_p = - 1) or (nvl(vl_unitario_p,0) between nvl(vl_minimo,nvl(vl_unitario_p,0)) and nvl(vl_maximo,nvl(vl_unitario_p,0)))))
	     or (((vl_material_p = - 1) or (nvl(vl_material_p,0) between nvl(vl_minimo,nvl(vl_material_p,0)) and nvl(vl_maximo,nvl(vl_material_p,0))))));
	
	if	(qt_regra_w = 0) then
		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(277653);
    end if;
end if;	

ds_erro_p := ds_erro_w;

end regra_exclusao_material;
/


               