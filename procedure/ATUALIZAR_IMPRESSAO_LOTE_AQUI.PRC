create or replace
procedure atualizar_impressao_lote_aqui(
			nr_sequencia_p	number,
			nm_usuario_p	varchar2) is

				
ds_maq_user_w 		varchar2(80);
cd_perfil_ativo_w		number(5);
					
begin

select	substr(obter_inf_sessao(0) ||' - ' || obter_inf_sessao(1),1,80)
into	ds_maq_user_w
from	dual;

cd_perfil_ativo_w	:= obter_perfil_ativo;


update	ap_lote
set	dt_impressao_aqui		= sysdate,
	nm_usuario_imp_aqui	= nm_usuario_p,
	ds_maquina_imp_aqui	= ds_maq_user_w,
	cd_perfil_imp_aqui		= cd_perfil_ativo_w
where	nr_sequencia		= nr_sequencia_p;

insert into ap_lote_historico(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_lote,
		ds_evento,
		ds_log)
	values(	ap_lote_historico_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_sequencia_p,
		wheb_mensagem_pck.get_texto(790850),
		wheb_mensagem_pck.get_texto(790849));

commit;

END atualizar_impressao_lote_aqui;
/