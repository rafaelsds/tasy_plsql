create or replace
procedure ptu_val_ordemserv_v50_ws(	nr_seq_ordem_serv_p  			ptu_requisicao_ordem_serv.nr_sequencia%type,
					nm_usuario_p				usuario.nm_usuario%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type,
					nr_seq_ret_ordem_serv_p		out	ptu_resposta_req_ord_serv.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Rotina utilizada para valida��o da transa��o 00806 - Ordem de servi�o do PTU
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

ptu_val_scs_ws_pck.ptu_processa_ordem_serv( nr_seq_ordem_serv_p, nm_usuario_p, cd_estabelecimento_p, nr_seq_ret_ordem_serv_p);

end ptu_val_ordemserv_v50_ws;
/