create or replace 
procedure dva_prices_xml(   item_num_p               	varchar2,
			    schedule_fee_p           	varchar2,
			    lmo_fee_p                	varchar2 ,
			    rei_p                    	varchar2,
			    vap_metro_p              	varchar2,
			    vap_regional_p           	varchar2,
			    rmfs_in_hopsital_fee_p   	varchar2,
			    rmfs_outof_hospital_p    	varchar2,
			    rvg_fee_p                	varchar2,
			    diagnostic_imaging_fee_p 	varchar2,
			    pathology_fee_p          	varchar2,
			    derived_fee_p            	varchar2,
			    derived_desc_rmfsin_p 	clob,
			    derived_desc_rmfsout_p 	clob,
			    nr_edicao_p  		number,
			    nm_usuario_p 		varchar2) is

nr_sequencia_w         	number(10);
derived_value          	number(6,2);
operational_cost_value 	number(6,2);
rmfs_in_hopsitalfee    	number(10,2);
rmfs_outof_hospitalfee 	number(10,2);
dva_percentage         	number(5,2);
rei_percentage         	number(5,2);
rei_value              	number(6,2);
rvgfee                 	number(6,2);
record_count           	number(2);
cd_procedimento_w	procedimento.cd_procedimento%type;
ie_origem_proced_w	procedimento.ie_origem_proced%type;

begin

select	dva_price_catalogue_seq.nextval 
into 	nr_sequencia_w
from 	dual;

select	(nr_dva_percentage/100),
	(nr_rei_percentage/100)
into	dva_percentage,
	rei_percentage
from 	dva_parameters
where 	cd_ref_catalouge_edition = nr_edicao_p;

if	(nr_sequencia_w is null) then
	nr_sequencia_w :=1;
end if;

if	(schedule_fee_p = 'D' ) then
	select	max(vl_custo_operacional)
	into 	derived_value
	from 	preco_amb
	where 	cd_procedimento_loc = item_num_p
	and 	ie_origem_proced      = 20;

	select	to_number(derived_value) 
	into 	derived_value 
	from 	dual;

	if	(derived_value is null ) then
		derived_value := 0;
	else
		derived_value := (derived_value * dva_percentage) + derived_value;
	end if;

	select	round((derived_value/5), 2) * 5 
	into 	derived_value 
	from 	dual;

else
	select	to_number(schedule_fee_p, '9999999.99') 
	into 	derived_value 
	from 	dual;
end if;

if	(lmo_fee_p = 'D' ) then
	operational_cost_value := derived_value * dva_percentage + derived_value;

	select	round(operational_cost_value/5,2) * 5
	into 	operational_cost_value
	from 	dual;
else
	select	to_number(lmo_fee_p, '99999999.99') 
	into 	operational_cost_value 
	from 	dual;
end if;

if	(rmfs_in_hopsital_fee_p = 'D' ) then
	rmfs_in_hopsitalfee := null;
else
	select	to_number(rmfs_in_hopsital_fee_p, '9999999.99') 
	into 	rmfs_in_hopsitalfee 
	from 	dual;
end if;

if	(rmfs_outof_hospital_p = 'D' ) then
	rmfs_outof_hospitalfee := null;
else
	select	to_number(rmfs_outof_hospital_p, '99999999.99') 
	into 	rmfs_outof_hospitalfee
	from 	dual;
end if;

if	(rvg_fee_p = 'D' ) then
	rvgfee := 0;
else
	select	to_number(rvg_fee_p,'99999999.99') 
	into 	rvgfee 
	from 	dual;
end if;

if	(rei_p = 'Y') then
	rei_value := (rei_percentage * derived_value) + derived_value;
	
	select	round(rei_value/5,2) * 5 
	into 	rei_value 
	from 	dual;

	rei_value := (dva_percentage * rei_value) + rei_value;

	select	round(rei_value/5,2) * 5 
	into 	rei_value 
	from 	dual;
else
	select	to_number(rei_p, '9999.99') 
	into 	rei_value 
	from 	dual;
end if;

select	count(*)
into 	record_count
from 	dva_price_catalogue
where 	cd_procedimento_loc = item_num_p
and 	nr_edicao_amb = nr_edicao_p;

select	max(cd_procedimento),
	max(ie_origem_proced)
into	cd_procedimento_w,
	ie_origem_proced_w
from	procedimento
where	cd_procedimento_loc = item_num_p;

if	(record_count = 0) then
	insert into dva_price_catalogue( nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_procedimento_loc,
		vl_procedimento,
		vl_dva_fee,
		ie_rei,
		ie_vap_metro,
		ie_vap_regional,
		vl_rmfs_in,
		vl_rmfs_out,
		vl_rvg_fee,
		vl_diag_img_fee,
		vl_pathology_fee,
		ie_derived,
		cd_in_hosp_desc,
		cd_out_hosp_desc,
		nr_edicao_amb)
	values(	nr_sequencia_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		item_num_p,
		derived_value,
		operational_cost_value,
		rei_value,
		vap_metro_p,
		vap_regional_p,
		rmfs_in_hopsitalfee,
		rmfs_outof_hospitalfee,
		rvgfee,
		to_number(diagnostic_imaging_fee_p, '9999999.99'),
		to_number(pathology_fee_p, '9999999.99'),
		derived_fee_p,
		derived_desc_rmfsin_p,
		derived_desc_rmfsout_p,
		nr_edicao_p);
else
	update	dva_price_catalogue
	set	dt_atualizacao        	= sysdate ,
		nm_usuario              = nm_usuario_p ,
		dt_atualizacao_nrec     = sysdate,
		nm_usuario_nrec         = nm_usuario_p ,
		vl_procedimento         = derived_value ,
		vl_dva_fee              = operational_cost_value ,
		ie_rei                  = rei_p ,
		ie_vap_metro            = vap_metro_p ,
		ie_vap_regional         = vap_regional_p ,
		vl_rmfs_in              = rmfs_in_hopsitalfee ,
		vl_rmfs_out             = rmfs_outof_hospitalfee ,
		vl_rvg_fee              = rvgfee ,
		vl_diag_img_fee         = to_number(diagnostic_imaging_fee_p, '999999.99') ,
		vl_pathology_fee        = to_number(pathology_fee_p, '999999.99') ,
		ie_derived              = derived_fee_p ,
		cd_in_hosp_desc         = derived_desc_rmfsin_p ,
		cd_out_hosp_desc        = derived_desc_rmfsout_p
	where 	cd_procedimento_loc 	= item_num_p
	and	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w
	and 	nr_edicao_amb         	= nr_edicao_p;

end if;

commit;

end dva_prices_xml;
/