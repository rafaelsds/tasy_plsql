create or replace
PROCEDURE Mat_Propaga_inf_material(
			cd_estabelecimento_p	Number,
			cd_material_p		Number,
			nm_usuario_p		Varchar2,
			ie_material_p		varchar2,
			ie_armazenamento_p	varchar2,
			ie_diluicao_p		varchar2,
			ie_conv_unidade_p	varchar2,
			ie_classe_adic_p	varchar2,
			ie_via_aplic_p		varchar2,
			ie_setor_exc_p		varchar2,
			ie_dose_terap_p		varchar2,
			ie_prescr_p		varchar2,
			ie_espec_tec_p		varchar2,
			ie_kit_prescr_p		varchar2,
			ie_mat_reacao_p		varchar2,
			ie_int_medic_p		varchar2,
			ie_dcb_p		varchar2,
			ie_dci_p		varchar2,
			ie_atc_p		varchar2,
			ie_restricao_p		varchar2,
			ie_int_alim_p		varchar2,
			ie_historico_p		varchar2,
			ie_risco_p		varchar2,
			ie_intervalo_sug_p	varchar2) is

cd_material_w			Number(15,0);
cd_material_generico_w		Number(15,0);
ds_historico_w			Long;
ds_historico_curto_w		varchar2(4000);
ds_titulo_w			varchar2(80);
qt_existe_w			Number(5);
ie_receita_w			varchar2(1);
qt_dias_validade_w		number(5);
ie_via_aplicacao_w		varchar2(5);
ie_via_aplicacao_ww		varchar2(5);
ie_obrig_via_aplicacao_w	varchar2(1);
ie_alto_risco_w			varchar2(1);
ie_prescricao_w			varchar2(1);
qt_conversao_mg_w		number(18,6);
ie_padronizado_w		varchar2(1);
qt_limite_pessoa_w		number(15,4);
cd_unid_med_limite_w		varchar2(30);
ds_orientacao_uso_w		varchar2(4000);
ie_tipo_fonte_prescr_w		varchar2(3);
ie_dias_util_medic_w		varchar2(1);
ds_orientacao_usuario_w		varchar2(4000);
cd_medicamento_w		number(10);
ie_controle_medico_w		number(1);
ie_baixa_estoq_pac_w		varchar2(1);
qt_horas_util_pac_w		number(7,2);
qt_max_prescricao_w		number(18,6);
ie_bomba_infusao_w		varchar2(1);
ie_diluicao_w			varchar2(1);
ie_mostrar_orientacao_w		varchar2(1);
ie_solucao_w			varchar2(1);
ie_mistura_w			varchar2(1);
ie_abrigo_luz_w			varchar2(1);
ie_umidade_controlada_w		varchar2(1);
nr_seq_ficha_tecnica_w		number(10);
cd_unid_med_concetracao_w	varchar2(30);
cd_unid_med_base_conc_w		varchar2(30);
qt_dia_profilatico_w		number(3);
qt_dia_terapeutico_w		number(3);
ie_gravar_obs_prescr_w		varchar2(15);
qt_min_aplicacao_w		number(15);
qt_dose_prescricao_w		number(18,6);
cd_unidade_medida_prescr_w	varchar2(30);
cd_unid_terapeutica_w		varchar2(15);
cd_intervalo_padrao_w		varchar2(7);
ie_obriga_justificativa_w	varchar2(1);
nr_seq_forma_farm_w		number(10);
qt_max_dia_aplic_w		number(3);
ds_precaucao_ordem_w		varchar2(60);
ie_ancora_solucao_w		varchar2(1);
ds_espec_tecnica_w		long;
nr_seq_dose_terap_w		number(15,0);

cd_especialidade_w		number(5);
ie_objetivo_w			varchar2(1);
qt_dias_prev_maximo_w		number(4);
qt_dose_min_w			number(18,6);
cd_um_minima_w			varchar2(30);
ie_justificativa_w		varchar2(1);
ds_referencia_w			varchar2(255);
ie_somente_sn_w			varchar2(1);
qt_hora_aplicacao_w		number(3);
qt_solucao_w			number(15,4);
ie_diluicao_mat_prescr_w	varchar2(1);
ie_se_necessario_w		varchar2(1);
ie_justificativa_padrao_w	varchar2(1);
ds_justificativa_w		varchar2(2000);
ie_gerar_agora_w		varchar2(1);
ie_alta_vigilancia_w		varchar2(1);
cd_estabelecimento_w		number(4);
cd_setor_atendimento_w		number(5);
cd_intervalo_w			varchar2(7);
cd_unidade_medida_w		varchar2(30);
qt_dose_w			number(18,6);
ie_dose_limite_w		varchar2(15);
qt_idade_min_w			number(5);
qt_idade_max_w			number(5);
qt_peso_min_w			number(6,3);
qt_peso_max_w			number(6,3);
qt_intervalo_horas_w		number(4,0);	
qt_dias_maximo_w		number(4,0);		
ie_tipo_w			varchar2(15);
qt_intervalo_max_horas_w	number(4,0);
ie_via_aplic_padrao_w		varchar2(5);
ie_dispositivo_infusao_w 	varchar2(1);
qt_minuto_aplicacao_w		number(4);
hr_prim_horario_w		varchar2(5);
qt_concentracao_w		number(18,6); 
qt_concentracao_min_w		number(18,6); 
cd_unid_med_cons_peso_w 	varchar2(30); 
cd_unid_med_cons_volume_w 	varchar2(30); 
qt_dias_solicitado_w    	number(3);  
ie_exige_justificativa_w 	varchar2(1); 
ie_dose_w                	varchar2(2); 
qt_idade_min_mes_w     		number(15,2);   
qt_idade_min_dia_w      	number(15,2); 
qt_idade_max_mes_w      	number(15,2); 
qt_idade_max_dia_w		number(15,2);
nr_seq_mat_prescr_w		number(10);
ie_fotosensivel_w		varchar2(1);
ie_higroscopico_w		varchar2(1);
ie_obriga_just_dose_w		varchar2(2);
ie_sexo_w			varchar2(2);
ie_classif_medic_w		varchar2(1);
qt_minimo_w			number(18,6);	
qt_maximo_w			number(18,6);
ie_mensagem_sonda_w		varchar2(10);
ie_consiste_dupl_w		varchar2(10);
cd_intervalo_filtro_w		varchar2(10);
DS_MENSAGEM_w			material.DS_MENSAGEM%type;
ds_mensagem_ww			varchar2(255);
nr_sequencia_w			number(10);

nr_sequencia_ww					material_diluicao.nr_sequencia%type;
cd_unidade_medida_ww			material_diluicao.cd_unidade_medida%type;
cd_diluente_ww					material_diluicao.cd_diluente%type;
cd_unid_med_diluente_ww			material_diluicao.cd_unid_med_diluente%type;
qt_diluicao_ww					material_diluicao.qt_diluicao%type;
qt_minima_diluicao_ww			material_diluicao.qt_minima_diluicao%type;
qt_fixa_diluicao_ww				material_diluicao.qt_fixa_diluicao%type;
qt_minuto_aplicacao_ww			material_diluicao.qt_minuto_aplicacao%type;
ie_reconstituicao_ww			material_diluicao.ie_reconstituicao%type;
ie_via_aplicacao_www			material_diluicao.ie_via_aplicacao%type;
qt_concentracao_ww				material_diluicao.qt_concentracao%type;
cd_unid_med_concentracao_ww		material_diluicao.cd_unid_med_concentracao%type;
cd_unid_med_base_concent_ww		material_diluicao.cd_unid_med_base_concent%type;
nr_seq_prioridade_ww			material_diluicao.nr_seq_prioridade%type;
cd_intervalo_ww					material_diluicao.cd_intervalo%type;
cd_setor_atendimento_ww			material_diluicao.cd_setor_atendimento%type;
qt_idade_min_ww					material_diluicao.qt_idade_min%type;
qt_idade_max_ww					material_diluicao.qt_idade_max%type;
ie_proporcao_ww					material_diluicao.ie_proporcao%type;
cd_motivo_baixa_ww				material_diluicao.cd_motivo_baixa%type;
ie_cobra_paciente_ww			material_diluicao.ie_cobra_paciente%type;
cd_setor_excluir_ww				material_diluicao.cd_setor_excluir%type;
qt_peso_min_ww					material_diluicao.qt_peso_min%type;
qt_peso_max_ww					material_diluicao.qt_peso_max%type;
qt_referencia_ww				material_diluicao.qt_referencia%type;
qt_volume_adic_ww				material_diluicao.qt_volume_adic%type;
qt_idade_min_mes_ww				material_diluicao.qt_idade_min_mes%type;
qt_idade_min_dia_ww				material_diluicao.qt_idade_min_dia%type;
qt_idade_max_mes_ww				material_diluicao.qt_idade_max_mes%type;
qt_idade_max_dia_ww				material_diluicao.qt_idade_max_dia%type;
qt_dose_min_ww					material_diluicao.qt_dose_min%type;
qt_dose_max_ww					material_diluicao.qt_dose_max%type;
ie_subtrair_volume_medic_ww		material_diluicao.ie_subtrair_volume_medic%type;
cd_perfil_ww					material_diluicao.cd_perfil%type;
nr_seq_restricao_ww				material_diluicao.nr_seq_restricao%type;
ds_referencia_ww				material_diluicao.ds_referencia%type;
ie_atualizar_tempo_ww			material_diluicao.ie_atualizar_tempo%type;
nr_seq_interno_ww				material_diluicao.nr_seq_interno%type;
qt_concentracao_total_ww		material_diluicao.qt_concentracao_total%type;
cd_unid_med_concetracao_ww		material_diluicao.cd_unid_med_concetracao%type;
cd_unid_med_base_conc_ww		material_diluicao.cd_unid_med_base_conc%type;
qt_volume_ww					material_diluicao.qt_volume%type;
ie_diluir_inteiro_ww			material_diluicao.ie_diluir_inteiro%type;
ie_gerar_lote_ww				material_diluicao.ie_gerar_lote%type;
nr_sequencia_pk_w				material_diluicao.nr_sequencia%type;
ie_gerar_rediluente_ww			material_diluicao.ie_gerar_rediluente%type;
ie_gerar_lote_manipulacao_ww	material_diluicao.ie_gerar_lote_manipulacao%type;
qt_volume_medic_ww				material_diluicao.qt_volume_medic%type;
cd_estabelecimento_ww			material_diluicao.cd_Estabelecimento%type;
nr_seq_kit_mat_prescr_w			kit_mat_prescricao.nr_sequencia%type;
nr_seq_novo_kit_mat_w			kit_mat_prescricao.nr_sequencia%type;


/*ie_opcao_p
0 = Farmacia
1 = Historico*/

CURSOR C01 IS
select	cd_material
from	material
where	cd_material_generico = nvl(cd_material_generico_w, cd_material_p)
and	cd_material	<> cd_material_p;

cursor c02 is
select	ds_espec_tecnica
from	material_espec_tecnica
where	cd_material = cd_material_p;

cursor	c03 is
select	nr_sequencia
from	material_prescr
where	cd_material = cd_material_p
and	cd_estabelecimento = cd_estabelecimento_p;

cursor c04 is
select	nr_sequencia,
		cd_unidade_medida,
		cd_diluente,
		cd_unid_med_diluente,
		qt_diluicao,
		qt_minima_diluicao,
		qt_fixa_diluicao,
		qt_minuto_aplicacao,
		ie_reconstituicao,
		ie_via_aplicacao,
		qt_concentracao,
		cd_unid_med_concentracao,
		cd_unid_med_base_concent,
		nr_seq_prioridade,
		cd_intervalo,
		cd_setor_atendimento,
		qt_idade_min,
		qt_idade_max,
		nvl(ie_proporcao,'F'),
		cd_motivo_baixa,
		ie_cobra_paciente,
		cd_setor_excluir,
		qt_peso_min,
		qt_peso_max,
		qt_referencia,
		qt_volume_adic,
		qt_idade_min_mes,
		qt_idade_min_dia,
		qt_idade_max_mes,
		qt_idade_max_dia,
		qt_dose_min,
		qt_dose_max,
		ie_subtrair_volume_medic,
		cd_perfil,
		nr_seq_restricao,
		ds_referencia,
		ie_atualizar_tempo,
		nr_seq_interno,
		qt_concentracao_total,
		cd_unid_med_concetracao,
		cd_unid_med_base_conc,
		qt_volume,
		ie_diluir_inteiro,
		ie_gerar_lote,
		ie_gerar_rediluente,
		ie_gerar_lote_manipulacao,
		qt_volume_medic,
		cd_estabelecimento
from	material_diluicao
where	cd_material	= cd_material_p
and		nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
order by cd_material, nr_sequencia;

cursor	c05 is
select	nr_sequencia
from	kit_mat_prescricao
where	cd_material = cd_material_p
and		nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

BEGIN

select	max(cd_material_generico),
	max(ie_receita),			
	max(qt_dias_validade),
	max(ie_via_aplicacao),		
	max(ie_obrig_via_aplicacao),
	max(ie_prescricao),
	max(qt_conversao_mg),
	max(ie_padronizado),		
	max(qt_limite_pessoa),
	max(cd_unid_med_limite),
	max(ds_orientacao_uso),
	max(ie_tipo_fonte_prescr),
	max(ie_dias_util_medic),
	max(ds_orientacao_usuario),
	max(cd_medicamento),
	max(obter_controle_medic(cd_material,cd_estabelecimento_p,ie_controle_medico,null,null,null)),		
	max(ie_baixa_estoq_pac),
	max(qt_horas_util_pac),
	max(qt_max_prescricao),
	max(ie_bomba_infusao),
	max(ie_diluicao),
	max(ie_solucao),		
	max(ie_mistura),
	max(ie_abrigo_luz),
	max(ie_umidade_controlada),
	max(nr_seq_ficha_tecnica),	
	max(cd_unid_med_concetracao),
	max(cd_unid_med_base_conc),	
	max(qt_dia_profilatico),
	max(qt_dia_terapeutico),	
	max(ie_gravar_obs_prescr),
	max(qt_min_aplicacao),
	max(qt_dose_prescricao),
	max(cd_unidade_medida_prescr),
	max(cd_unid_terapeutica),
	max(cd_intervalo_padrao),		
	max(ie_obriga_justificativa),
	max(nr_seq_forma_farm),
	max(qt_max_dia_aplic),
	max(ds_precaucao_ordem),
	max(ie_ancora_solucao),
	max(ie_dose_limite),	
	max(ie_fotosensivel),
	max(ie_higroscopico),		
	max(ie_obriga_just_dose),
	max(ie_sexo),
	max(ie_classif_medic),
	max(ie_alto_risco),	
	max(ie_mensagem_sonda),
	max(ie_consiste_dupl),	
	max(ie_mostrar_orientacao),
	max(DS_MENSAGEM),
	max(ie_alta_vigilancia),
	max(nr_seq_dose_terap)
into	cd_material_generico_w,
	ie_receita_w,			
	qt_dias_validade_w,
	ie_via_aplicacao_w,		
	ie_obrig_via_aplicacao_w,
	ie_prescricao_w,		
	qt_conversao_mg_w,
	ie_padronizado_w,		
	qt_limite_pessoa_w,
	cd_unid_med_limite_w,		
	ds_orientacao_uso_w,
	ie_tipo_fonte_prescr_w,		
	ie_dias_util_medic_w,
	ds_orientacao_usuario_w,	
	cd_medicamento_w,
	ie_controle_medico_w,		
	ie_baixa_estoq_pac_w,
	qt_horas_util_pac_w,		
	qt_max_prescricao_w,
	ie_bomba_infusao_w,		
	ie_diluicao_w,
	ie_solucao_w,			
	ie_mistura_w,
	ie_abrigo_luz_w,		
	ie_umidade_controlada_w,
	nr_seq_ficha_tecnica_w,		
	cd_unid_med_concetracao_w,
	cd_unid_med_base_conc_w,	
	qt_dia_profilatico_w,
	qt_dia_terapeutico_w,		
	ie_gravar_obs_prescr_w,
	qt_min_aplicacao_w,		
	qt_dose_prescricao_w,
	cd_unidade_medida_prescr_w,
	cd_unid_terapeutica_w,
	cd_intervalo_padrao_w,		
	ie_obriga_justificativa_w,
	nr_seq_forma_farm_w,		
	qt_max_dia_aplic_w,
	ds_precaucao_ordem_w,		
	ie_ancora_solucao_w,
	ie_dose_limite_w,		
	ie_fotosensivel_w,
	ie_higroscopico_w,		
	ie_obriga_just_dose_w,
	ie_sexo_w,			
	ie_classif_medic_w,
	ie_alto_risco_w,		
	ie_mensagem_sonda_w,
	ie_consiste_dupl_w,		
	ie_mostrar_orientacao_w,
	DS_MENSAGEM_w,
	ie_alta_vigilancia_w,
	nr_seq_dose_terap_w
from	material
where	cd_material	= cd_material_p;

OPEN C01;
LOOP
FETCH C01 INTO
	cd_material_w;
EXIT when c01%notfound;
	begin
	if	(ie_material_p = 'S') then

		update material
		set	ie_receita			= ie_receita_w,
			qt_dias_validade		= qt_dias_validade_w,
			ie_via_aplicacao		= ie_via_aplicacao_w,
			ie_obrig_via_aplicacao	= ie_obrig_via_aplicacao_w,
			ie_prescricao		= ie_prescricao_w,
			qt_conversao_mg		= qt_conversao_mg_w,
			ie_padronizado		= ie_padronizado_w,
			qt_limite_pessoa		= qt_limite_pessoa_w,
			cd_unid_med_limite		= cd_unid_med_limite_w,
			ds_orientacao_uso		= ds_orientacao_uso_w,
			ie_tipo_fonte_prescr	= ie_tipo_fonte_prescr_w,
			ie_dias_util_medic		= ie_dias_util_medic_w,
			ds_orientacao_usuario	= ds_orientacao_usuario_w,
			cd_medicamento		= cd_medicamento_w,
			ie_controle_medico		= ie_controle_medico_w,
			ie_baixa_estoq_pac		= ie_baixa_estoq_pac_w,
			qt_horas_util_pac		= qt_horas_util_pac_w,
			qt_max_prescricao		= qt_max_prescricao_w,
			ie_bomba_infusao		= ie_bomba_infusao_w,
			ie_diluicao		= ie_diluicao_w,
			ie_solucao		= ie_solucao_w,
			ie_mistura			= ie_mistura_w,
			ie_abrigo_luz		= ie_abrigo_luz_w,
			ie_umidade_controlada	= ie_umidade_controlada_w,
			nr_seq_ficha_tecnica	= nr_seq_ficha_tecnica_w,
			cd_unid_med_concetracao	= cd_unid_med_concetracao_w,
			cd_unid_med_base_conc	= cd_unid_med_base_conc_w,
			qt_dia_profilatico		= qt_dia_profilatico_w,
			qt_dia_terapeutico		= qt_dia_terapeutico_w,
			ie_gravar_obs_prescr	= ie_gravar_obs_prescr_w,
			qt_min_aplicacao		= qt_min_aplicacao_w,
			qt_dose_prescricao		= qt_dose_prescricao_w,
			cd_unidade_medida_prescr	= cd_unidade_medida_prescr_w,
			cd_unid_terapeutica	= cd_unid_terapeutica_w,
			cd_intervalo_padrao	= cd_intervalo_padrao_w,
			ie_obriga_justificativa	= ie_obriga_justificativa_w,
			nr_seq_forma_farm		= nr_seq_forma_farm_w,
			qt_max_dia_aplic		= qt_max_dia_aplic_w,
			ds_precaucao_ordem	= ds_precaucao_ordem_w,
			ie_ancora_solucao		= ie_ancora_solucao_w,
			ie_sexo			= ie_sexo_w,
			ie_higroscopico		= ie_higroscopico_w,
			ie_fotosensivel		= ie_fotosensivel_w,
			ie_obriga_just_dose		= ie_obriga_just_dose_w	,
			ie_alto_risco		= ie_alto_risco_w,
			ie_mensagem_sonda	= ie_mensagem_sonda_w,
			ie_consiste_dupl	= ie_consiste_dupl_w,
			ie_mostrar_orientacao	= ie_mostrar_orientacao_w,
			DS_MENSAGEM		= DS_MENSAGEM_w,
			ie_alta_vigilancia = ie_alta_vigilancia_w,
			nr_seq_dose_terap = nr_seq_dose_terap_w
		where	cd_material		= cd_material_w;
		
		
		delete
		from	regra_orient_usuario
		where	cd_material = cd_material_w;
		
		insert into regra_orient_usuario(
			cd_material,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ie_mostrar,
			ie_sexo,
			qt_idade_min,
			qt_idade_min_mes,
			qt_idade_min_dia,
			qt_idade_max,
			qt_idade_max_mes,
			qt_idade_max_dia,
			cd_estabelecimento)
		select	cd_material_w,
			regra_orient_usuario_seq.nextval,
			dt_atualizacao,
			nm_usuario,
			ie_mostrar,
			ie_sexo,
			qt_idade_min,
			qt_idade_min_mes,
			qt_idade_min_dia,
			qt_idade_max,
			qt_idade_max_mes,
			qt_idade_max_dia,
			cd_estabelecimento
		from	regra_orient_usuario y
		where	y.cd_material = cd_material_p;
		
	end if;
	
	if	(ie_diluicao_p = 'S') then
	
		if	(ie_armazenamento_p = 'S') then
			update	material_armazenamento
			set	nr_seq_diluicao		=	null,
				nr_seq_reconstituicao	=	null
			where	cd_material		=	cd_material_w;
		end if;
		
		delete
		from	material_diluicao
		where	cd_material = cd_material_w		
		and		nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

		open c04;
			loop
				fetch c04 into
					nr_sequencia_ww,
					cd_unidade_medida_ww,
					cd_diluente_ww,
					cd_unid_med_diluente_ww,
					qt_diluicao_ww,
					qt_minima_diluicao_ww,
					qt_fixa_diluicao_ww,
					qt_minuto_aplicacao_ww,
					ie_reconstituicao_ww,
					ie_via_aplicacao_www,
					qt_concentracao_ww,
					cd_unid_med_concentracao_ww,
					cd_unid_med_base_concent_ww,
					nr_seq_prioridade_ww,
					cd_intervalo_ww,
					cd_setor_atendimento_ww,
					qt_idade_min_ww,
					qt_idade_max_ww,
					ie_proporcao_ww,
					cd_motivo_baixa_ww,
					ie_cobra_paciente_ww,
					cd_setor_excluir_ww,
					qt_peso_min_ww,
					qt_peso_max_ww,
					qt_referencia_ww,
					qt_volume_adic_ww,
					qt_idade_min_mes_ww,
					qt_idade_min_dia_ww,
					qt_idade_max_mes_ww,
					qt_idade_max_dia_ww,
					qt_dose_min_ww,
					qt_dose_max_ww,
					ie_subtrair_volume_medic_ww,
					cd_perfil_ww,
					nr_seq_restricao_ww,
					ds_referencia_ww,
					ie_atualizar_tempo_ww,
					nr_seq_interno_ww,
					qt_concentracao_total_ww,
					cd_unid_med_concetracao_ww,
					cd_unid_med_base_conc_ww,
					qt_volume_ww,
					ie_diluir_inteiro_ww,
					ie_gerar_lote_ww,
					ie_gerar_rediluente_ww,
					ie_gerar_lote_manipulacao_ww,
					qt_volume_medic_ww,
					cd_estabelecimento_ww;
				exit when c04%notfound;
				
				select 	nvl(max(nr_sequencia), 0)
				into 	nr_sequencia_pk_w
				from	material_diluicao
				where	cd_material = cd_material_w
				and 	nr_sequencia = nr_sequencia_ww;
				
				if (nr_sequencia_pk_w > 0) then
					select 	nvl(max(nr_sequencia), 0)
					into 	nr_sequencia_pk_w
					from	material_diluicao
					where	cd_material = cd_material_w;
					
					nr_sequencia_ww := nr_sequencia_pk_w + 1;
				end if;
				
				insert into material_diluicao(
					cd_estabelecimento,
					cd_material,
					nr_sequencia,
					cd_unidade_medida,
					cd_diluente,
					cd_unid_med_diluente,
					dt_atualizacao,
					nm_usuario,
					qt_diluicao,
					qt_minima_diluicao,
					qt_fixa_diluicao,
					qt_minuto_aplicacao,
					ie_reconstituicao,
					ie_via_aplicacao,
					qt_concentracao,
					cd_unid_med_concentracao,
					cd_unid_med_base_concent,
					nr_seq_prioridade,
					cd_intervalo,
					cd_setor_atendimento,
					qt_idade_min,
					qt_idade_max,
					nr_seq_interno,
					ie_proporcao,
					cd_motivo_baixa,
					ie_cobra_paciente,
					cd_setor_excluir,
					qt_peso_min,
					qt_peso_max,
					qt_referencia,
					qt_volume_adic,
					qt_idade_min_mes,
					qt_idade_min_dia,
					qt_idade_max_mes,
					qt_idade_max_dia,
					qt_dose_min,
					qt_dose_max,
					ie_subtrair_volume_medic,
					cd_perfil,
					nr_seq_restricao,
					ds_referencia,
					ie_atualizar_tempo,
					nr_seq_int_propaga,
					qt_concentracao_total,
					cd_unid_med_concetracao,
					cd_unid_med_base_conc,
					qt_volume,
					ie_diluir_inteiro,
					ie_gerar_lote,
					ie_gerar_rediluente,
					ie_gerar_lote_manipulacao,
					qt_volume_medic)
				values (
					cd_estabelecimento_ww,
					cd_material_w,
					nr_sequencia_ww,
					cd_unidade_medida_ww,
					cd_diluente_ww,
					cd_unid_med_diluente_ww,
					sysdate,
					nm_usuario_p,
					qt_diluicao_ww,
					qt_minima_diluicao_ww,
					qt_fixa_diluicao_ww,
					qt_minuto_aplicacao_ww,
					ie_reconstituicao_ww,
					ie_via_aplicacao_www,
					qt_concentracao_ww,
					cd_unid_med_concentracao_ww,
					cd_unid_med_base_concent_ww,
					nr_seq_prioridade_ww,
					cd_intervalo_ww,
					cd_setor_atendimento_ww,
					qt_idade_min_ww,
					qt_idade_max_ww,
					material_diluicao_seq.nextval,
					ie_proporcao_ww,
					cd_motivo_baixa_ww,
					ie_cobra_paciente_ww,
					cd_setor_excluir_ww,
					qt_peso_min_ww,
					qt_peso_max_ww,
					qt_referencia_ww,
					qt_volume_adic_ww,
					qt_idade_min_mes_ww,
					qt_idade_min_dia_ww,
					qt_idade_max_mes_ww,
					qt_idade_max_dia_ww,
					qt_dose_min_ww,
					qt_dose_max_ww,
					ie_subtrair_volume_medic_ww,
					cd_perfil_ww,
					nr_seq_restricao_ww,
					ds_referencia_ww,
					ie_atualizar_tempo_ww,
					nr_seq_interno_ww,
					qt_concentracao_total_ww,
					cd_unid_med_concetracao_ww,
					cd_unid_med_base_conc_ww,
					qt_volume_ww,
					ie_diluir_inteiro_ww,
					ie_gerar_lote_ww,
					ie_gerar_rediluente_ww,
					ie_gerar_lote_manipulacao_ww,
					qt_volume_medic_ww );				
			end loop;
		close c04;
	end if;
	commit;
	
	if	(ie_armazenamento_p = 'S') then

		delete	from material_armazenamento
		where	cd_material = cd_material_w;

		insert into material_armazenamento(
			cd_material,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_estagio,
			nr_seq_forma,
			qt_estabilidade,
			ie_tempo_estab,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_via_aplicacao,
			nr_seq_diluicao,
			nr_seq_reconstituicao,
			IE_PROTEGIDO,
			NR_SEQ_APRES,
			DS_REFERENCIA,
			IE_ESTAB_FORNEC,
			NR_SEQ_MARCA,
			CD_ESTABELECIMENTO)
		select	cd_material_w,
			material_armazenamento_seq.nextval,
			sysdate,
			nm_usuario_p,
			y.nr_seq_estagio,
			y.nr_seq_forma,
			y.qt_estabilidade,
			y.ie_tempo_estab,
			sysdate,
			nm_usuario_p,
			y.ie_via_aplicacao,
			(select max(x.nr_seq_interno) from material_diluicao x where x.nr_seq_int_propaga = y.nr_seq_diluicao),
			(select max(w.nr_seq_interno) from material_diluicao w where w.nr_seq_int_propaga = y.nr_seq_reconstituicao),
			IE_PROTEGIDO,
			NR_SEQ_APRES,
			DS_REFERENCIA,
			IE_ESTAB_FORNEC,
			NR_SEQ_MARCA,
			CD_ESTABELECIMENTO
		from	material_armazenamento y
		where	y.cd_material = cd_material_p;
	end if;
	
	
	if	(ie_conv_unidade_p = 'S') then

		delete	from material_conversao_unidade
		where	cd_material		= cd_material_w;

		insert into material_conversao_unidade(
			cd_material,
			cd_unidade_medida,
			qt_conversao,
			ie_prioridade,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec)
		select	cd_material_w,
			cd_unidade_medida,
			qt_conversao,
			ie_prioridade,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate
		from	material_conversao_unidade
		where	cd_material	= cd_material_p;
	end if;
	
	if	(ie_classe_adic_p = 'S') then

		delete from Material_classe_adic
		where	cd_material	= cd_material_w;

		insert into Material_classe_adic(
			cd_material,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_classe_material,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		select	cd_material_w,
			Material_classe_adic_seq.nextval,
			sysdate,
			nm_usuario_p,
			cd_classe_material,
			sysdate,
			nm_usuario_p
		from	Material_classe_adic
		where	cd_material = cd_material_p;
	end if;
	
	if	(ie_via_aplic_p = 'S') then

		delete from mat_via_aplic
		where	cd_material = cd_material_w;

		insert into mat_via_aplic(
			cd_material,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ie_via_aplicacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_recomendacao,
			cd_setor_excluir,
			cd_setor_atendimento,
			ie_recomendada,
			NR_SEQ_PRIORIDADE,
			CD_ESTABELECIMENTO)
		select	cd_material_w,
			mat_via_aplic_seq.nextval,
			sysdate,
			nm_usuario_p,
			ie_via_aplicacao,
			sysdate,
			nm_usuario_p,
			ds_recomendacao,
			cd_setor_excluir,
			cd_setor_atendimento,
			ie_recomendada,
			NR_SEQ_PRIORIDADE,
			CD_ESTABELECIMENTO
		from	mat_via_aplic
		where	cd_material = cd_material_p;
	end if;
	
	if	(ie_setor_exc_p = 'S') then	

		delete from material_setor_exclusivo
		where	cd_material = cd_material_w
		and	cd_estabelecimento = cd_estabelecimento_p;
		
		insert into material_setor_exclusivo(
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_material,
			cd_setor_atendimento,
			ie_permite)
		select	material_setor_exclusivo_seq.nextval,
			cd_estabelecimento,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_material_w,
			cd_setor_atendimento,
			ie_permite
		from	material_setor_exclusivo
		where	cd_material = cd_material_p
		and	cd_estabelecimento = cd_estabelecimento_p;
	end if;
		
	if	(ie_dose_terap_p = 'S') then

		delete from material_dose_terap
		where	cd_material = cd_material_w
		and	cd_estabelecimento = cd_Estabelecimento_p;       
		
		insert into material_dose_terap(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			cd_material,
			nr_seq_dose_terap,
			qt_dose_min_aviso,
			qt_dose_max_aviso,
			qt_dose_min_bloqueia,
			qt_dose_max_bloqueia,
			qt_idade_minima,
			qt_idade_maxima,
			qt_peso_minimo,
			qt_peso_maximo,
			cd_setor_atendimento)
		select	material_dose_terap_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento,
			cd_material_w,
			nr_seq_dose_terap,
			qt_dose_min_aviso,
			qt_dose_max_aviso,
			qt_dose_min_bloqueia,
			qt_dose_max_bloqueia,
			qt_idade_minima,
			qt_idade_maxima,
			qt_peso_minimo,
			qt_peso_maximo,
			cd_setor_atendimento
		from	material_dose_terap
		where	cd_material = cd_material_p
		and	cd_estabelecimento = cd_estabelecimento_p;
	end if;
	
	if	(ie_prescr_p = 'S') then
		
		delete 	regra_setor_mat_prescr a
		where	exists(	select	1 
				from 	material_prescr b 
				where 	b.nr_sequencia = a.nr_seq_mat_prescr
				and	b.cd_material = cd_material_w
				and	b.cd_Estabelecimento = cd_Estabelecimento_p);

		delete	material_prescr
		where	cd_material = cd_material_w
		and	cd_Estabelecimento = cd_Estabelecimento_p;
		
		open C03;
		loop
		fetch C03 into	
			nr_sequencia_w;
		exit when C03%notfound;
			begin
			select	material_prescr_seq.nextval 
			into	nr_seq_mat_prescr_w
			from 	dual;
						
			insert into material_prescr(
				nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_material,
				cd_setor_atendimento,
				cd_intervalo,
				cd_unidade_medida,
				qt_dose,
				cd_unid_med_limite,
				qt_limite_pessoa,
				ie_dose_limite,
				qt_idade_min,
				qt_idade_max,
				ie_via_aplicacao,
				qt_peso_min,
				qt_peso_max,
				qt_intervalo_horas,
				qt_dias_maximo,
				ie_tipo,
				qt_intervalo_max_horas,
				ie_via_aplic_padrao,
				ie_dispositivo_infusao,
				qt_minuto_aplicacao,
				hr_prim_horario,
				qt_concentracao,
				cd_unid_med_cons_peso,
				cd_unid_med_cons_volume,
				qt_dias_solicitado,
				ie_exige_justificativa,
				ie_dose,
				qt_idade_min_mes,
				qt_idade_min_dia,
				qt_idade_max_mes,
				qt_idade_max_dia,
				ds_justificativa,
				qt_hora_aplicacao,
				qt_solucao,
				ie_justificativa,
				cd_especialidade,
				ie_diluicao,
				qt_dias_prev_maximo,
				ie_objetivo,
				ie_se_necessario,
				ie_somente_sn,
				ds_referencia,
				qt_dose_min,
				cd_um_minima,
				ie_justificativa_padrao,
				ie_dose_nula,
				ie_gerar_agora,
				ie_aplica_solucao,
				qt_aplic_max,
				cd_protocolo,
				nr_seq_medicacao,
				ie_forma_consistencia,
				ds_observacao,
				nr_seq_agrupamento,
				ie_obedecer_limite,
				ds_mensagem,
				ie_obriga_just_dose,
				qt_dose_terapeutica,
				nr_seq_dose_terap,
				ds_horarios,
				cd_intervalo_filtro,
				qt_aplic_min,
				qt_aplicacao_max,
				cd_doenca_cid,
				cd_unidade_basica,
				qt_dose_especial,
				ds_obs_padrao,
				qt_concentracao_min,
				ie_limpar_prim_hor,
				ds_mensagem_regra,
				qt_ig_min,
				qt_ig_max,
				ie_tipo_dosagem,
				ie_dispositivo_infusao_se,
				ie_permite_liberar,
				ie_tipo_item)
			select	nr_seq_mat_prescr_w,
				cd_estabelecimento,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_material_w,
				cd_setor_atendimento,
				cd_intervalo,
				cd_unidade_medida,
				qt_dose,
				cd_unid_med_limite,
				qt_limite_pessoa,
				ie_dose_limite,
				qt_idade_min,
				qt_idade_max,
				ie_via_aplicacao,
				qt_peso_min,
				qt_peso_max,
				qt_intervalo_horas,
				qt_dias_maximo,
				ie_tipo,
				qt_intervalo_max_horas,
				ie_via_aplic_padrao,
				ie_dispositivo_infusao,
				qt_minuto_aplicacao,
				hr_prim_horario,
				qt_concentracao,
				cd_unid_med_cons_peso,
				cd_unid_med_cons_volume,
				qt_dias_solicitado,
				ie_exige_justificativa,
				ie_dose,
				qt_idade_min_mes,
				qt_idade_min_dia,
				qt_idade_max_mes,
				qt_idade_max_dia,
				ds_justificativa,
				qt_hora_aplicacao,
				qt_solucao,
				ie_justificativa,
				cd_especialidade,
				ie_diluicao,
				qt_dias_prev_maximo,
				ie_objetivo,
				ie_se_necessario,
				ie_somente_sn,
				ds_referencia,
				qt_dose_min,
				cd_um_minima,
				ie_justificativa_padrao,
				ie_dose_nula,
				ie_gerar_agora,
				ie_aplica_solucao,
				qt_aplic_max,
				cd_protocolo,
				nr_seq_medicacao,
				ie_forma_consistencia,
				ds_observacao,
				nr_seq_agrupamento,
				ie_obedecer_limite,
				ds_mensagem,
				ie_obriga_just_dose,
				qt_dose_terapeutica,
				nr_seq_dose_terap,
				ds_horarios,
				cd_intervalo_filtro,
				qt_aplic_min,
				qt_aplicacao_max,
				cd_doenca_cid,
				cd_unidade_basica,
				qt_dose_especial,
				ds_obs_padrao,
				qt_concentracao_min,
				ie_limpar_prim_hor,
				ds_mensagem_regra,
				qt_ig_min,
				qt_ig_max,
				ie_tipo_dosagem,
				ie_dispositivo_infusao_se,
				ie_permite_liberar,
				ie_tipo_item
			from	material_prescr
			where	nr_sequencia = nr_sequencia_w;

			insert	into regra_setor_mat_prescr
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				cd_setor_excluir,
				nr_seq_mat_prescr)
			select 	regra_setor_mat_prescr_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				cd_setor_excluir,
				nr_seq_mat_prescr_w
			from	regra_setor_mat_prescr 
			where	nr_seq_mat_prescr = nr_sequencia_w;
			end;
		end loop;
		close C03;					
	end if;		

	if	(ie_espec_tec_p = 'S') then
	
		delete	material_espec_tecnica
		where	cd_material = cd_material_w;

		open C02;
		loop
		fetch C02 into	
			ds_espec_tecnica_w;
		exit when C02%notfound;
			begin
			insert into material_espec_tecnica(
				nr_sequencia,
				cd_material,
				dt_atualizacao,
				nm_usuario,
				ds_espec_tecnica,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	material_espec_tecnica_seq.nextval,
				cd_material_w,
				sysdate,
				nm_usuario_p,
				ds_espec_tecnica_w,
				sysdate,
				nm_usuario_p);
			end;
		end loop;
		close C02;
	end if;
	
	if	(nvl(ie_intervalo_sug_p,'N') = 'S') then
		
		delete	rep_intervalo_sugerido
		where	cd_material = cd_material_w;
		
		insert into rep_intervalo_sugerido(
			cd_material,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_intervalo,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_apresent)
		select	cd_material_w,
			rep_intervalo_sugerido_seq.nextval,
			sysdate,
			nm_usuario_p,
			cd_intervalo,
			sysdate,
			nm_usuario_p,
			nr_seq_apresent
		from	rep_intervalo_sugerido
		where	cd_material 	= cd_material_p;
	end if;
	
	if	(ie_kit_prescr_p = 'S') then
		
		select	count(*)
		into	qt_existe_w
		from	material_descritivo
		where	cd_material = cd_material_w;
		
		delete 	setor_kit_mat_prescricao a
		where	exists(	select	1 
				from 	kit_mat_prescricao b 
				where 	b.nr_sequencia = a.nr_seq_regra
				and		b.cd_material = cd_material_w
				and		nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p);
		
		delete from kit_mat_prescricao
		where	cd_material = cd_material_w
		and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;
		
		open C05;
		loop
		fetch C05 into	
			nr_seq_kit_mat_prescr_w;
		exit when C05%notfound;
		begin
		
		select	kit_mat_prescricao_seq.nextval
		into	nr_seq_novo_kit_mat_w
		from 	dual;

		insert into kit_mat_prescricao(
			cd_material,
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			cd_kit,
			qt_volume,
			ie_via_aplicacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_unidade_medida,
			cd_setor_atendimento,
			cd_setor_excluir,
			ie_tipo_atendimento,
			qt_idade_min,
			qt_idade_min_mes,
			qt_idade_min_dia,
			qt_idade_max,
			qt_idade_max_mes,
			qt_idade_max_dia,
			nr_seq_via_acesso,
			qt_dose_max,
			qt_dose_min,
			cd_unidade_medida_dose,
			qt_tempo_min,
			qt_tempo_max,
			ie_gerar_lib_farm,
			ie_gerar_recons_dilui,
			ie_bomba_infusao,
			nr_seq_agrupamento)
		select	cd_material_w,
			nr_seq_novo_kit_mat_w,
			cd_estabelecimento,
			sysdate,
			nm_usuario_p,
			cd_kit,
			qt_volume,
			ie_via_aplicacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_unidade_medida,
			cd_setor_atendimento,
			cd_setor_excluir,
			ie_tipo_atendimento,
			qt_idade_min,
			qt_idade_min_mes,
			qt_idade_min_dia,
			qt_idade_max,
			qt_idade_max_mes,
			qt_idade_max_dia,
			nr_seq_via_acesso,
			qt_dose_max,
			qt_dose_min,
			cd_unidade_medida_dose,
			qt_tempo_min,
			qt_tempo_max,
			ie_gerar_lib_farm,
			ie_gerar_recons_dilui,
			ie_bomba_infusao,
			nr_seq_agrupamento
		from	kit_mat_prescricao
		where	nr_sequencia = nr_seq_kit_mat_prescr_w;
		
		insert	into setor_kit_mat_prescricao(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_setor_atendimento,
				nr_seq_regra)
			select 	setor_kit_mat_prescricao_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_setor_atendimento,
				nr_seq_novo_kit_mat_w
			from	setor_kit_mat_prescricao 
			where	nr_seq_regra = nr_seq_kit_mat_prescr_w;
			end;
		end loop;
		close C05;		

	end if;

	if	(ie_mat_reacao_p = 'S') then
	
		delete from material_reacao
		where cd_material 		= cd_material_w;

		insert into material_reacao(
			cd_material,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_reacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		select	cd_material_w,
			material_reacao_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_reacao,
			sysdate,
			nm_usuario_p
		from	material_reacao
		where	cd_material	= cd_material_p;
	end if;
	
	if	(ie_int_medic_p = 'S') then

		delete from material_interacao_medic
		where cd_material		= cd_material_w;

		insert into material_interacao_medic(
			cd_material,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_material_interacao,
			ie_severidade,
			ie_exibir_gravidade,
			ie_consistir,
			ie_mensagem_cadastrada,
			ds_tipo,
			ds_ref_bibliografica,
			ds_orientacao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_ficha,
			nr_seq_ficha_interacao,
			cd_grupo_material,
			cd_subgrupo_material,
			cd_classe_material)
		select	cd_material_w,
			material_interacao_medic_seq.nextval,
			sysdate,
			nm_usuario_p,
			cd_material_interacao,
			ie_severidade,
			ie_exibir_gravidade,
			ie_consistir,
			ie_mensagem_cadastrada,
			ds_tipo,
			ds_ref_bibliografica,
			ds_orientacao,
			sysdate,
			nm_usuario_p,
			nr_seq_ficha,
			nr_seq_ficha_interacao,			
			cd_grupo_material,
			cd_subgrupo_material,
			cd_classe_material
		from	material_interacao_medic
		where	cd_material = cd_material_p;
	end if;

	if	(ie_dcb_p = 'S') then
	
		delete	from material_dcb
		where	cd_material = cd_material_w;

		insert into material_dcb(
			nr_sequencia,
			cd_material,
			nr_seq_dcb,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		select	material_dcb_seq.nextval,
			cd_material_w,
			nr_seq_dcb,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p
		from	material_dcb
		where	cd_material = cd_material_p;
	end if;

	if	(ie_dci_p = 'S') then
	
		delete	from material_dci
		where	cd_material = cd_material_w;

		insert into material_dci(
			nr_sequencia,
			cd_material,
			nr_seq_dci,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		select	material_dci_seq.nextval,
			cd_material_w,
			nr_seq_dci,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p
		from	material_dci
		where	cd_material = cd_material_p;
	end if;
	
	if	(ie_atc_p = 'S') then

		delete	from material_atc
		where	cd_material = cd_material_w;

		insert into material_atc(
			nr_sequencia,
			cd_material,
			cd_substancia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		select	material_atc_seq.nextval,
			cd_material_w,
			cd_substancia,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p
		from	material_atc
		where	cd_material = cd_material_p;
	end if;
		
	if	(ie_restricao_p = 'S') then
	
		delete	from material_restricao
		where	cd_material = cd_material_w;

		insert into material_restricao(
			nr_sequencia,
			cd_material,
			ds_restricao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			qt_minimo,
			qt_maximo)		
		select	nr_sequencia,
			cd_material_w,
			ds_restricao,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,			
			qt_minimo,
			qt_maximo
		from	material_restricao
		where	cd_material = cd_material_p;
	end if;

	if	(ie_int_alim_p = 'S') then
	
		delete from medic_interacao_alimento
		where cd_material = cd_material_w;

		insert into medic_interacao_alimento(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_material,
			ds_interacao)
		select	medic_interacao_alimento_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_material_w,
			ds_interacao	
		from medic_interacao_alimento
		where cd_material = cd_material_p;
	end if;

	if	(ie_historico_p = 'S') then

		select	count(*)
		into	qt_existe_w
		from	material_historico a
		where	a.nr_sequencia = (
			select	max(b.nr_sequencia)
			from	material_historico b
			where	b.cd_material = cd_material_p);
		if	(qt_existe_w > 0) then
			select	a.ds_historico,
				a.ds_titulo,
				a.ds_historico_curto
			into	ds_historico_w,
				ds_titulo_w,
				ds_historico_curto_w
			from	material_historico a
			where	a.nr_sequencia = (
				select	max(b.nr_sequencia)
				from	material_historico b
				where	b.cd_material = cd_material_p);

			insert into material_historico(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_historico,
				ds_historico_curto,
				cd_material,
				ds_titulo)
			values(	material_historico_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_historico_w,
				ds_historico_curto_w,
				cd_material_w,
				ds_titulo_w);
		end if;
	end if;
	
	if	(nvl(ie_risco_p,'N') = 'S') then
		begin

		update	medicamento_risco
		set		ie_situacao = 'I'
		where	cd_material = cd_material_w
		and		nvl(ie_situacao, 'A') = 'A';

		insert into medicamento_risco(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_material,
			nr_seq_risco,
			ie_nao_conformidade,
			ds_observacao,
			ds_orientacao,
			ie_situacao)
		select	medicamento_risco_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_material_w,
			nr_seq_risco,
			ie_nao_conformidade,
			ds_observacao,
			ds_orientacao,
			ie_situacao
		from	medicamento_risco
		where	cd_material = cd_material_p
		and		nvl(ie_situacao, 'A') = 'A';
		end;
	end if;
	
	delete	LIB_MATERIAL_INTERVALO
	where	cd_material = cd_material_w;

	insert into lib_material_intervalo(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_intervalo,
		cd_material,
		ie_tipo)
	select	LIB_MATERIAL_INTERVALO_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_intervalo,
		cd_material_w,
		ie_tipo
	from	lib_material_intervalo
	where	cd_material = cd_material_p;
	
	end;
END LOOP;
CLOSE C01;

commit;

END Mat_Propaga_inf_material;
/
