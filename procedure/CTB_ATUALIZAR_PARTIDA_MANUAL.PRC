create or replace
procedure ctb_atualizar_partida_manual(
			nr_seq_reg_concil_p		number) is 




nr_sequencia_w			ctb_movimento.nr_sequencia%type;	
nr_seq_movto_partida_w	ctb_movimento.nr_sequencia%type;			
nr_seq_reg_concil_w		ctb_movimento.nr_seq_reg_concil%type;		
vl_movimento_w			ctb_movimento.vl_movimento%type;
vl_movimento_partida_w	ctb_movimento.vl_movimento%type;

		
Cursor c_partidas is
	select	a.nr_sequencia,
			a.nr_seq_reg_concil,
			vl_movimento
	from	ctb_movimento a
	where	a.nr_seq_reg_concil = nr_seq_reg_concil_p
	and		a.nr_seq_movto_partida is null
	and		exists (select 1
					from 	ctb_movimento x
					where	x.nr_seq_movto_partida = a.nr_sequencia);

	

Cursor c_contra_partidas is
	select	nr_sequencia
	from 	ctb_movimento
	where	nr_seq_movto_partida = nr_sequencia_w 
	and		nr_seq_reg_concil is null;			
begin

open c_partidas;
loop
fetch c_partidas into	
	  nr_sequencia_w,
	  nr_seq_reg_concil_w,
	  vl_movimento_partida_w;
exit when c_partidas%notfound;

	begin
	
	open c_contra_partidas;
	loop
	fetch c_contra_partidas into	
		nr_seq_movto_partida_w;
	exit when c_contra_partidas%notfound;
		begin
		update   ctb_movimento
		set		 nr_seq_reg_concil = nr_seq_reg_concil_w,
				 ie_status_concil  = 'C'
		where	 nr_sequencia 	   =  nr_seq_movto_partida_w;
		
		end;
	end loop;
	close c_contra_partidas;
	end;
	
	select 	sum(vl_movimento)	
	into	vl_movimento_w
	from	ctb_movimento
	where	nr_seq_movto_partida = nr_sequencia_w;
	
	if (vl_movimento_w = vl_movimento_partida_w)then
		update	ctb_movimento
		set		ie_status_concil		= 'C'
		where	nr_sequencia			= nr_sequencia_w;
	end if;
	
end loop;
close c_partidas;


commit;

end ctb_atualizar_partida_manual;
/