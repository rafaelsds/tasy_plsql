create or replace
procedure reverter_medic_quimio(nr_seq_atendimento_p		number,
				   nm_usuario_p			varchar2,
				   cd_estabelecimento_p		number)	is
				  
nr_agrupamento_w	number(6,0);
nr_seq_material_w	number(10,0);	
ie_administrar_seq_w	varchar2(1);	

Cursor C01 is
	select	nr_seq_material,
		nr_agrupamento 
	from 	paciente_atend_medic a
	where 	nr_seq_atendimento = nr_seq_atendimento_p
	and	ie_administracao = 'A'
	and 	nr_seq_diluicao is null;

begin

select	nvl(max(Obter_Valor_Param_Usuario(3130, 61, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p)), 'S')
into	ie_administrar_seq_w
from 	dual;

open C01;
loop
fetch C01 into	
	nr_seq_material_w,
	nr_agrupamento_w;
exit when C01%notfound;
	begin
	if	(ie_administrar_seq_w = 'S') then
		reverter_status_medic_quimio(nr_seq_atendimento_p,nr_agrupamento_w,nm_usuario_p,'A');
	else
		reverter_status_medic_quimio(nr_seq_atendimento_p,nr_seq_material_w,nm_usuario_p,'M');
	end if;
	end;
end loop;
close C01;

commit;

end reverter_medic_quimio;
/