create or replace
procedure calcular_osmolaridade_nut_pac( 
								nr_sequencia_p		number,
								nm_usuario_p		varchar2,
								cd_estabelecimento_p	number) is

VarFormulaOsmNPT_w				varchar2(1);
qt_volume_diario_w				number(15,2);
qt_peso_w						number(15,3);
qt_vol_lipidio_w				number(15,4);								
qt_vol_glicose_w				number(15,4);
qt_vol_magnesio_w				number(15,4);
qt_vol_calcio_w					number(15,4);
qt_vol_sodio_w					number(15,4);
qt_vol_potassio_w				number(15,4);
qt_vol_proteina_w				number(15,4);
qt_diaria_aa_w					number(15,4);
qt_vol_eletro_w					number(15,4);
qt_vol_fosforo_w				number(15,4);
qt_osmolaridade_total_w			number(15,2);

begin
Obter_Param_Usuario(924,1080,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,VarFormulaOsmNPT_w);

select	max(qt_volume_diario),
		max(nvl(qt_peso_ajustado,qt_peso))
into	qt_volume_diario_w,
		qt_peso_w
from 	nut_pac
where	nr_sequencia = nr_sequencia_p;

if	(nvl(qt_volume_diario_w,0) > 0) then

	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_lipidio_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'L';
	
	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_glicose_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'C';
	
	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_proteina_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'P';
	
	select	(nvl(sum(a.qt_diaria),0))
	into	qt_vol_sodio_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'N'; 
	
	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_magnesio_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'M'; 
	
	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_calcio_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'I';

	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_potassio_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'K';
	
	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_eletro_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'E';
	
	select	nvl(sum(a.qt_diaria),0)
	into	qt_vol_fosforo_w
	from	nut_elemento b,
			nut_pac_elemento a
	where	a.nr_seq_nut_pac	= nr_sequencia_p
	and		a.nr_seq_elemento	= b.nr_sequencia
	and		b.ie_tipo_elemento	= 'F';	
	
	if	(VarFormulaOsmNPT_w = 'P') then
		qt_osmolaridade_total_w	:= (((qt_vol_proteina_w * 10) + (qt_vol_lipidio_w * 0.27) + (qt_vol_glicose_w * 5) + (( qt_vol_sodio_w + qt_vol_potassio_w + qt_vol_magnesio_w + qt_vol_calcio_w) * 2)) / qt_volume_diario_w) *1000;
	elsif	(VarFormulaOsmNPT_w = 'C') then		
		qt_osmolaridade_total_w	:= ((qt_vol_proteina_w * 11) + (qt_vol_glicose_w * 5.5) + (qt_vol_lipidio_w * 0.3) + (qt_vol_sodio_w + qt_vol_potassio_w + qt_vol_magnesio_w + qt_vol_calcio_w + qt_vol_fosforo_w)) * 1000;
	else
		select	nvl(sum(a.qt_diaria),0)
		into	qt_vol_lipidio_w
		from	nut_elemento b,
				nut_pac_elemento a
		where	a.nr_seq_nut_pac	= nr_sequencia_p
		and		a.nr_seq_elemento	= b.nr_sequencia
		and		b.ie_tipo_elemento	= 'L'
		and 	nvl(a.ie_prod_adicional,'N') = 'N';
		
		select	nvl(sum(a.qt_diaria),0)
		into	qt_vol_glicose_w
		from	nut_elemento b,
				nut_pac_elemento a
		where	a.nr_seq_nut_pac	= nr_sequencia_p
		and		a.nr_seq_elemento	= b.nr_sequencia
		and		b.ie_tipo_elemento	= 'C'
		and 	nvl(a.ie_prod_adicional,'N') = 'N';
		
		select	nvl(sum(a.qt_diaria),0)
		into	qt_diaria_aa_w
		from	nut_elemento b,
				nut_pac_elemento a
		where	a.nr_seq_nut_pac	= nr_sequencia_p
		and		a.nr_seq_elemento	= b.nr_sequencia
		and		b.ie_tipo_elemento	= 'P'
		and 	nvl(a.ie_prod_adicional,'N') = 'N';

		select	nvl(sum(a.qt_diaria),0)
		into	qt_vol_eletro_w
		from	nut_elemento b,
				nut_pac_elemento a
		where	a.nr_seq_nut_pac	= nr_sequencia_p
		and		a.nr_seq_elemento	= b.nr_sequencia
		and		b.ie_tipo_elemento	= 'E'
		and 	nvl(a.ie_prod_adicional,'N') = 'N';
		
		select	nvl(sum(a.qt_diaria),0)
		into	qt_vol_fosforo_w
		from	nut_elemento b,
				nut_pac_elemento a
		where	a.nr_seq_nut_pac	= nr_sequencia_p
		and		a.nr_seq_elemento	= b.nr_sequencia
		and		b.ie_tipo_elemento	= 'F'
		and 	nvl(a.ie_prod_adicional,'N') = 'N';
		
		if (VarFormulaOsmNPT_w = 'E') then
			qt_volume_diario_w := qt_volume_diario_w+30;
		end if;	
		qt_osmolaridade_total_w	:= dividir_sem_round(((qt_diaria_aa_w * 11) + (qt_vol_glicose_w * 5.5) + (qt_vol_lipidio_w * 0.3) + ((qt_vol_sodio_w + qt_vol_potassio_w + qt_vol_magnesio_w + qt_vol_calcio_w) *2)),qt_volume_diario_w) * 1000;
	end if;
	
	update	Nut_pac
	set		qt_osmolaridade_total	= qt_osmolaridade_total_w
	where	nr_sequencia			= nr_sequencia_p;
	commit;
	
end if;

end calcular_osmolaridade_nut_pac;
/