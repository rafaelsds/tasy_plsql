create or replace
procedure	desfazer_rec_protocolo(
			nr_seq_protocolo_p	number) is

nm_usuario_w			usuario.nm_usuario%type	:= wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type	:= wheb_usuario_pck.get_cd_estabelecimento;
ie_exclui_etapa_conta_w		varchar2(1);
ie_tipo_protocolo_w		protocolo_documento.ie_tipo_protocolo%type;
nr_interno_conta_w		protocolo_doc_item.nr_seq_interno%type;

Cursor C01 is
	select	distinct a.nr_seq_interno
	from	protocolo_doc_item a
	where	a.nr_sequencia = nr_seq_protocolo_p
	and	a.nr_seq_interno is not null
	and	exists (select 1 from conta_paciente x 
			where	x.nr_interno_conta = a.nr_seq_interno);


begin

obter_param_usuario(290, 111, obter_perfil_Ativo, nm_usuario_w, cd_estabelecimento_w, ie_exclui_etapa_conta_w);

select	max(ie_tipo_protocolo)
into	ie_tipo_protocolo_w
from	protocolo_documento
where	nr_sequencia = nr_seq_protocolo_p;

update	protocolo_documento
set	dt_fechamento		= null,
	dt_rec_destino		= null,
	nm_usuario_receb		= null
where	nr_sequencia		= nr_seq_protocolo_p;

update	protocolo_doc_item
set	dt_recebimento		= null,
	nm_usuario_receb		= null,
	nr_seq_etapa_conta	= null
where	nr_sequencia		= nr_seq_protocolo_p
and	dt_entrega_pac is null;

if	(ie_exclui_etapa_conta_w = 'S') then

	delete	conta_paciente_etapa
	where	nr_seq_prot_documento = nr_seq_protocolo_p;

end if;


if	(ie_tipo_protocolo_w  = '5') then
	
	open C01;
	loop
	fetch C01 into	
		nr_interno_conta_w;
	exit when C01%notfound;
		begin
		gerar_etapa_conta(nr_interno_conta_w,'2',nm_usuario_w);
		end;
	end loop;
	close C01;				

end if;

commit;

end desfazer_rec_protocolo;
/