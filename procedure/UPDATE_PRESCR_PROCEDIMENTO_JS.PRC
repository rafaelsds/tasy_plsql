create or replace
procedure update_prescr_procedimento_js(
		vl_parametro_p		varchar2,
		cd_tipo_baixa_p		number,
		nm_usuario_p		varchar2,
		nr_seq_motivo_just_p	number,
		nr_prescricao_p		number,
		nr_seq_proced_p		number,
		dt_baixa_p		varchar2) is 
		
begin

if	(vl_parametro_p = 'S') then
	begin
	update 	prescr_procedimento 
	set 	cd_motivo_baixa = cd_tipo_baixa_p, 
		dt_baixa 	= to_date(dt_baixa_p,'dd/mm/yyyy hh24:mi:ss'),
		nm_usuario 	= nm_usuario_p,
		nr_seq_motivo_just = nr_seq_motivo_just_p,
		dt_atualizacao 	= sysdate,
		ie_status_execucao = 'BE',
		nm_usuario_baixa_esp = nm_usuario_p
	where 	nr_prescricao 	= nr_prescricao_p
	and 	nr_sequencia 	= nr_seq_proced_p;
	end;
else
	begin
	update 	prescr_procedimento
	set 	cd_motivo_baixa = cd_tipo_baixa_p,
		dt_baixa 	= to_date(dt_baixa_p,'dd/mm/yyyy hh24:mi:ss'),
		nm_usuario 	= nm_usuario_p,
		dt_atualizacao 	= sysdate,
		ie_status_execucao = 'BE',
		nm_usuario_baixa_esp = nm_usuario_p
	where 	nr_prescricao 	= nr_prescricao_p
	and 	nr_sequencia 	= nr_seq_proced_p;
	end;
end if;

commit;

end update_prescr_procedimento_js;
/