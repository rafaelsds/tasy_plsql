create or replace
procedure IMPORTAR_DE_PARA_TUSS(cd_estabelecimento_p	number,
				cd_material_p		number,
				cd_material_tuss_p	number,
				dt_inicio_vigencia_p	date,
				nr_seq_tuss_mat_p	number,
				cd_convenio_p       number,
				nm_usuario_p		Varchar2) is 
				

nr_seq_tuss_mat_item_w			material_tuss.nr_seq_tuss_mat_item%type;
ie_existe_material_w			varchar2(1) := 'N';

begin


select	nvl(max(b.nr_sequencia),0)
into	nr_seq_tuss_mat_item_w
from	tuss_material_item b,
	tuss_material a
where	a.nr_sequencia 		= b.nr_seq_carga_tuss
and	a.nr_sequencia 		= nr_seq_tuss_mat_p
and	b.cd_material_tuss	= cd_material_tuss_p;



begin
select	'S'
into	ie_existe_material_w
from	material
where	cd_material = cd_material_p
and	rownum < 2;
exception
when others then
	ie_existe_material_w := 'N';
end;

if	(nr_seq_tuss_mat_item_w > 0) and
	(nvl(ie_existe_material_w,'N') = 'S') then

	insert into material_tuss (             
		cd_estabelecimento,        
		cd_material,       
		cd_material_tuss,          
		dt_atualizacao,            
		dt_atualizacao_nrec,                
		dt_vigencia_inicial,       
		ie_situacao,                      
		nm_usuario,                
		nm_usuario_nrec,                        
		nr_seq_tuss_mat,           
		nr_seq_tuss_mat_item,      
		nr_sequencia,
		cd_convenio)
	values	(cd_estabelecimento_p,        
		cd_material_p,       
		cd_material_tuss_p,          
		sysdate,            
		sysdate,                
		dt_inicio_vigencia_p,       
		'A',                      
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_tuss_mat_p,           
		nr_seq_tuss_mat_item_w,      
		material_tuss_seq.nextval,
		cd_convenio_p);	
	
end if;


commit;

end IMPORTAR_DE_PARA_TUSS;
/
