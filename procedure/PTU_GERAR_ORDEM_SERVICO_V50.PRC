create or replace
procedure ptu_gerar_ordem_servico_v50
			(	nr_seq_guia_p		number,
				nr_seq_requisicao_p	number,
				cd_unimed_exec_p	varchar2,
				nr_versao_ptu_p		varchar2,
				nr_seq_ordem_p		Number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a transa��o de Ordem de Servi�o do PTU, via SCS.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
cd_unimed_beneficiario_w		ptu_requisicao_ordem_serv.cd_unimed_beneficiario%type;
cd_unimed_solicitante_w			ptu_requisicao_ordem_serv.cd_unimed_solicitante%type;
cd_carteirinha_w			varchar2(17);
nr_seq_controle_exec_w			ptu_controle_execucao.nr_sequencia%type;
qt_ordem_w                		pls_integer;

begin

begin
	select	cd_unimed_beneficiario,
		cd_unimed_solicitante,
		pls_obter_dados_segurado(nr_seq_segurado, 'C')
	into	cd_unimed_beneficiario_w,
		cd_unimed_solicitante_w,
		cd_carteirinha_w
	from	ptu_requisicao_ordem_serv
	where	nr_sequencia	= nr_seq_ordem_p;
exception
when others then
	cd_unimed_beneficiario_w	:= null;
	cd_unimed_solicitante_w		:= null;
end;

select count(1)
into   qt_ordem_w
from   ptu_requisicao_ordem_serv
where  nr_sequencia = nr_seq_ordem_p
and    nr_transacao_solicitante is not null;

if	(qt_ordem_w	= 0) then
	select	ptu_controle_execucao_seq.NextVal
	into	nr_seq_controle_exec_w
	from	dual;

	insert	into ptu_controle_execucao
		(nr_sequencia, dt_atualizacao, nm_usuario,
		 nr_seq_pedido_compl, nr_seq_pedido_aut, nm_usuario_nrec,
		 dt_atualizacao_nrec, nr_seq_ordem_serv)
	values	(nr_seq_controle_exec_w, sysdate, nm_usuario_p,
		 null, null, nm_usuario_p,
		 sysdate, nr_seq_ordem_p);

	-- Se a operadora de origem do benefici�rio for igual a operadora solicitante da ordem de servi�o, � caracterizada uma transa��o ponto-a-ponto
	if	(cd_unimed_beneficiario_w	= cd_unimed_solicitante_w) then
		update	ptu_requisicao_ordem_serv
		set	nr_transacao_solicitante	= nr_seq_controle_exec_w,
			nr_seq_origem			= nr_seq_controle_exec_w,
			cd_unimed			= substr(cd_carteirinha_w,1,4),
			cd_usuario_plano		= substr(cd_carteirinha_w,5,17)
		where	nr_sequencia			= nr_seq_ordem_p;
	-- Se a operadora de origem do benefici�rio for diferente da operadora solicitante da ordem de servi�o, � caracterizada uma Triangula��o
	elsif	(cd_unimed_beneficiario_w	<> cd_unimed_solicitante_w) then
		update	ptu_requisicao_ordem_serv
		set		nr_transacao_solicitante	= nr_seq_controle_exec_w,
				nr_seq_origem			= null,
				cd_unimed			= substr(cd_carteirinha_w,1,4),
				cd_usuario_plano		= substr(cd_carteirinha_w,5,17)
		where	nr_sequencia				= nr_seq_ordem_p;
	end if;
else
	update	ptu_requisicao_ordem_serv
	set	dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where 	nr_sequencia 	= nr_seq_ordem_p
	and   	nr_transacao_solicitante is not null;
end if;

commit;

end ptu_gerar_ordem_servico_v50;
/
