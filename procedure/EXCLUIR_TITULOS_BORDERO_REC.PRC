create or replace procedure excluir_titulos_bordero_rec
		(nr_bordero_p		number,
		nm_usuario_p		varchar2) is


nr_titulo_w		    bordero_tit_rec.nr_titulo%type;
nr_seq_receb_w		bordero_tit_rec.nr_seq_receb%type;

cursor	c01 is
select nr_titulo,
       nr_seq_receb 
from bordero_tit_rec
where nr_bordero = nr_bordero_p
and nr_seq_receb is not null;

begin

open	c01;
loop
fetch	c01 into
	nr_titulo_w,
	nr_seq_receb_w;
exit when c01%notfound;

    delete from convenio_receb_titulo 
    where nr_titulo = nr_titulo_w
    and nr_bordero = nr_bordero_p
    and nr_seq_receb = nr_seq_receb_w;

end	loop;
close	c01;

delete from bordero_tit_rec
where nr_bordero = nr_bordero_p;

commit;

end excluir_titulos_bordero_rec;
/
