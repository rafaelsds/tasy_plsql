Create or replace
procedure consiste_item_avulso_reg_kit(
			cd_material_p		number,
			nr_seq_kit_reg_p		number,
			ds_erro_p	out	varchar2) is

cd_kit_material_w		number(5);
nr_seq_reg_kit_w	number(10);
ds_erro_w		varchar2(255);
qt_existe_w		number(10);

begin

nr_seq_reg_kit_w	:= nvl(nr_seq_kit_reg_p,0);

select 	count(*)
into	qt_existe_w
from 	kit_estoque b,
	kit_estoque_comp a
where   b.nr_seq_reg_kit = nr_seq_reg_kit_w
and	b.nr_sequencia = a.nr_seq_kit_estoque
and 	a.cd_material = cd_material_p
and	a.ie_gerado_barras = 'N';

if	(qt_existe_w > 0) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(278841);
end if;

select 	count(*)
into	qt_existe_w
from 	kit_estoque b,
	kit_estoque_comp a
where  	a.nr_seq_reg_kit = nr_seq_reg_kit_w
and	b.nr_sequencia = a.nr_seq_kit_estoque
and 	a.cd_material = cd_material_p
and	a.ie_gerado_barras = 'E';

if	(qt_existe_w > 0) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(278842);
end if;

ds_erro_p	:= substr(ds_erro_w,1,255);

end consiste_item_avulso_reg_kit;
/