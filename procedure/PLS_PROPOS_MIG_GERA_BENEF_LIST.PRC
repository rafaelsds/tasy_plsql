create or replace
procedure pls_propos_mig_gera_benef_list(
		nr_seq_proposta_p		number,
		nr_seq_beneficiario_lista_p	number,
		nm_usuario_p		varchar2) is 

nr_seq_beneficiario_lista_w	varchar2(2000);
tam_lista_w		number(4);
ie_pos_virgula_w		number(4);
nr_seq_beneficiario_w	number(10);

begin

nr_seq_beneficiario_lista_w	:= nr_seq_beneficiario_lista_p;

while	(nr_seq_beneficiario_lista_w is not null) loop
	begin
	tam_lista_w	:= length(nr_seq_beneficiario_lista_w);
	ie_pos_virgula_w	:= instr(nr_seq_beneficiario_lista_w, ',');
	
	if	(ie_pos_virgula_w <> 0) then
		begin
		nr_seq_beneficiario_w	:= substr(nr_seq_beneficiario_lista_w, 1, (ie_pos_virgula_w - 1));
		nr_seq_beneficiario_lista_w	:= substr(nr_seq_beneficiario_lista_w, (ie_pos_virgula_w + 1), tam_lista_w);
		
		pls_proposta_mig_gerar_benef(
			nr_seq_proposta_p,
			nr_seq_beneficiario_w,
			nm_usuario_p);
		end;
	end if;
	end;
end loop;

end pls_propos_mig_gera_benef_list;
/