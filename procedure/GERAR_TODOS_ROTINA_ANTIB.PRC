create or replace
procedure gerar_todos_rotina_antib(	nr_prescricao_p		number,
				nr_seq_item_p		number,
				nr_seq_lista_cid_p	varchar,
				nm_usuario_p		varchar2) is 


nr_seq_lista_w		varchar2(255);
nr_pos_virgula_w	number(10,0);	
nr_sequencia_w		number(10,0);	

begin
if (nr_seq_lista_cid_p is not null)	then
	begin	
	nr_seq_lista_w	:= nr_seq_lista_cid_p;
	while	(nr_seq_lista_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(nr_seq_lista_w,',');
		if (nr_pos_virgula_w > 0)	then
			begin
			nr_sequencia_w	:= to_number(substr(nr_seq_lista_w,1,nr_pos_virgula_w-1));
			nr_seq_lista_w	:= substr(nr_seq_lista_w,nr_pos_virgula_w+1,length(nr_seq_lista_w));
			if (nr_sequencia_w > 0) then
				begin
					gerar_cid_rotina_antib_prescr	(nr_prescricao_p, nr_seq_item_p, nr_sequencia_w, nm_usuario_p);
				end;
			end if;
			end;
		end if;
		end;
	end loop;
	end;
end if;

commit;

end gerar_todos_rotina_antib;
/