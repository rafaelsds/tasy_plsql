create or replace 
procedure loc_obj_inserir_reg_prod_dic (	nm_usuario_p					in varchar2,
											cd_funcao_p						in number,
											nr_seq_product_requirement_p	in number,
											nr_seq_reg_funcao_pr_p			in number ) as

nr_seq_pd_w		reg_product_dic.nr_sequencia%type;

cursor c_loc_obj is
select	coalesce(nr_seq_obj, nr_seq_dic_obj, nr_seq_evt, nr_seq_dic_dados) nr_sequencia,
		nr_seq_obj,
		nr_seq_dic_obj,
		nr_seq_evt,
		nr_seq_dic_dados
from	w_localizador_objetos
where	cd_funcao = cd_funcao_p
and		nm_usuario = nm_usuario_p
and		ie_marcado = 'S';

begin

    for r_c_loc_obj in c_loc_obj loop
	
		select	max(nr_sequencia)
		into	nr_seq_pd_w
		from	reg_product_dic
		where	nr_seq_product_requirement = nr_seq_product_requirement_p
		and		nr_seq_objeto = r_c_loc_obj.nr_sequencia
		and		nr_seq_reg_funcao_pr = nr_seq_reg_funcao_pr_p
		and		ie_tipo_objeto = (	case
										when r_c_loc_obj.nr_seq_obj is not null then 'S' --Schematics
										when r_c_loc_obj.nr_seq_dic_obj is not null then 'O' --Dic_Objetos
										when r_c_loc_obj.nr_seq_evt is not null then 'E' -- Evento
										when r_c_loc_obj.nr_seq_dic_dados is not null then 'D' -- Dic_dados
									end
								);
		
		if (nr_seq_pd_w is null) then

			insert into reg_product_dic (
				nr_sequencia,
				nr_seq_product_requirement,
				nr_seq_reg_funcao_pr,
				nr_seq_objeto,
				ie_tipo_objeto,
				nm_usuario,
				dt_atualizacao )
			values (
				reg_product_dic_seq.nextval,
				nr_seq_product_requirement_p,
				nr_seq_reg_funcao_pr_p,
				r_c_loc_obj.nr_sequencia,
				(	case
						when r_c_loc_obj.nr_seq_obj is not null then 'S' --Schematics
						when r_c_loc_obj.nr_seq_dic_obj is not null then 'O' --Dic_Objetos
						when r_c_loc_obj.nr_seq_evt is not null then 'E' -- Evento
						when r_c_loc_obj.nr_seq_dic_dados is not null then 'D' -- Dic_dados
					end
				),
				nm_usuario_p,
				sysdate );

		end if;	
    end loop;

    /* Reseting all checks */
	update	w_localizador_objetos
    set		ie_marcado = 'N'
    where	cd_funcao = cd_funcao_p
	and		nm_usuario = nm_usuario_p;

	commit;

end loc_obj_inserir_reg_prod_dic;
/
