create or replace
procedure ptu_gerar_solic_alt_a1350
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is 

qt_registro_w			pls_integer;
nr_seq_pais_w			pais.nr_sequencia%type;
ie_estado_civil_w		pessoa_fisica.ie_estado_civil%type;

cd_cep_w			compl_pessoa_fisica.cd_cep%type;
cd_tipo_logradouro_w		compl_pessoa_fisica.cd_tipo_logradouro%type;
ds_endereco_w			compl_pessoa_fisica.ds_endereco%type;
nr_endereco_w			compl_pessoa_fisica.nr_endereco%type;
ds_complemento_w		compl_pessoa_fisica.ds_complemento%type;
ds_bairro_w			compl_pessoa_fisica.ds_bairro%type;
ds_municipio_w			compl_pessoa_fisica.ds_municipio%type;
sg_estado_w			compl_pessoa_fisica.sg_estado%type;
cd_municipio_ibge_w		compl_pessoa_fisica.cd_municipio_ibge%type;
nr_ddd_telefone_w		compl_pessoa_fisica.nr_ddd_telefone%type;
nr_telefone_w			compl_pessoa_fisica.nr_telefone%type;
ds_email_w			compl_pessoa_fisica.ds_email%type;
nr_seq_compl_w			compl_pessoa_fisica.nr_sequencia%type;

cd_tipo_logradouro_imp_w	compl_pessoa_fisica.cd_tipo_logradouro%type;

nr_ddd_imp_w			ptu_mov_pessoa_contato.nr_ddd_imp%type;
nr_telefone_imp_w		ptu_mov_pessoa_contato.nr_telefone_imp%type;

Cursor C00 is
	select	x.nr_sequencia nr_seq_mov_benef,
		c.cd_pessoa_fisica,
		c.nm_pessoa_fisica,
		c.nm_social,
		c.dt_nascimento,
		c.nr_cpf,
		c.ie_sexo,
		c.ie_estado_civil,
		c.nr_identidade,
		c.ds_orgao_emissor_ci,
		c.nr_seq_pais,
		c.nr_cartao_nac_sus,
		c.nr_pis_pasep,
		c.cd_municipio_ibge,
		(	select	max(t.nm_contato)
			from	compl_pessoa_fisica t
			where	t.cd_pessoa_fisica = c.cd_pessoa_fisica
			and	t.ie_tipo_complemento = '5') nm_mae,
		c.nr_ddd_celular,
		c.nr_telefone_celular
	from	ptu_mov_pessoa_benef x,
		pls_segurado b,
		pessoa_fisica c
	where	b.nr_sequencia		= x.nr_seq_segurado
	and	c.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	x.nr_seq_lote		= nr_seq_lote_p;

Cursor C01 (nr_seq_mov_benef_pc	number) is
	select	a.nm_pessoa_imp,
		a.nm_social_imp,
		a.dt_nascimento_imp,
		a.nr_cpf_imp,
		a.ie_tipo_sexo_imp,
		a.ie_estado_civil_imp,
		a.nr_identidade_imp,
		a.ds_orgao_emissor_imp,
		a.nr_pais_imp,
		a.cd_cns_imp,
		a.nr_pispasep_imp,
		a.cd_municipio_imp,
		a.nm_mae_imp
	from	ptu_mov_pessoa_dados a
	where	a.nr_seq_mov_benef	= nr_seq_mov_benef_pc;

Cursor C02 (nr_seq_mov_benef_pc	number) is
	select	a.ie_residencia_imp,
		a.ie_tipo_endereco_imp,
		a.cd_cep_imp,
		a.ie_tipo_logradouro_imp,
		a.ds_logradouro_imp,
		a.nr_logradouro_imp,
		a.ds_complemento_imp,
		a.ds_bairro_imp,
		a.ds_cidade_imp,
		a.sg_uf_imp,
		a.cd_municipio_ibge_imp
	from	ptu_mov_pessoa_endereco a
	where	a.nr_seq_mov_benef	= nr_seq_mov_benef_pc;

Cursor C03 (nr_seq_mov_benef_pc	number) is
	select	ie_tipo_telefone_imp,
		nr_ddd_imp,
		nr_telefone_imp,
		ie_tipo_email_imp,
		ds_email_imp
	from	ptu_mov_pessoa_contato
	where	nr_seq_mov_benef	= nr_seq_mov_benef_pc;

begin

for r_c00_w in C00 loop
	begin
	
	for r_c01_w in C01(r_c00_w.nr_seq_mov_benef) loop
		begin
		
		if	(r_c01_w.nm_pessoa_imp is not null) and
			(upper(r_c01_w.nm_pessoa_imp) <> upper(r_c00_w.nm_pessoa_fisica) or r_c00_w.nm_pessoa_fisica is null) then
			pls_gerar_solicitacao_alt(r_c00_w.nm_pessoa_fisica, r_c01_w.nm_pessoa_imp, 'NM_PESSOA_FISICA', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.nm_social_imp is not null) and
			(upper(r_c01_w.nm_social_imp) <> upper(r_c00_w.nm_social) or r_c00_w.nm_social is null) then
			pls_gerar_solicitacao_alt(r_c00_w.nm_social, r_c01_w.nm_social_imp, 'NM_SOCIAL', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.dt_nascimento_imp is not null) and
			(r_c01_w.dt_nascimento_imp <> r_c00_w.dt_nascimento or r_c00_w.dt_nascimento is null) then
			pls_gerar_solicitacao_alt(r_c00_w.dt_nascimento, r_c01_w.dt_nascimento_imp, 'DT_NASCIMENTO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.nr_cpf_imp is not null) and
			(r_c01_w.nr_cpf_imp <> r_c00_w.nr_cpf or r_c00_w.nr_cpf is null) then
			pls_gerar_solicitacao_alt(r_c00_w.nr_cpf, r_c01_w.nr_cpf_imp, 'NR_CPF', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.ie_tipo_sexo_imp is not null) and
			(upper(r_c01_w.ie_tipo_sexo_imp) <> upper(r_c00_w.ie_sexo) or r_c00_w.ie_sexo is null) then
			pls_gerar_solicitacao_alt(r_c00_w.ie_sexo, r_c01_w.ie_tipo_sexo_imp, 'IE_SEXO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.ie_estado_civil_imp is not null) then
			if	(r_c01_w.ie_estado_civil_imp = 'A') then
				ie_estado_civil_w	:= '6'; --Separado
			elsif	(r_c01_w.ie_estado_civil_imp = 'D') then
				ie_estado_civil_w	:= '3'; --Divorciado
			elsif	(r_c01_w.ie_estado_civil_imp = 'M') then
				ie_estado_civil_w	:= '2'; --Casado
			elsif	(r_c01_w.ie_estado_civil_imp = 'S') then
				ie_estado_civil_w	:= '1'; --Solteiro
			elsif	(r_c01_w.ie_estado_civil_imp = 'W') then
				ie_estado_civil_w	:= '5'; --Vi�vo
			elsif	(r_c01_w.ie_estado_civil_imp = 'U') then
				ie_estado_civil_w	:= '7'; --Uni�o Est�vel
			end if;
			
			if	(ie_estado_civil_w is not null) and
				(ie_estado_civil_w <> r_c00_w.ie_estado_civil or r_c00_w.ie_estado_civil is null) then
				pls_gerar_solicitacao_alt(r_c00_w.ie_estado_civil, ie_estado_civil_w, 'IE_ESTADO_CIVIL', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
			end if;
		end if;
		
		if	(r_c01_w.nr_identidade_imp is not null) and
			(upper(r_c01_w.nr_identidade_imp) <> upper(r_c00_w.nr_identidade) or r_c00_w.nr_identidade is null) then
			pls_gerar_solicitacao_alt(r_c00_w.nr_identidade, r_c01_w.nr_identidade_imp, 'NR_IDENTIDADE', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.ds_orgao_emissor_imp is not null) and
			(upper(r_c01_w.ds_orgao_emissor_imp) <> upper(r_c00_w.ds_orgao_emissor_ci) or r_c00_w.ds_orgao_emissor_ci is null) then
			pls_gerar_solicitacao_alt(r_c00_w.ds_orgao_emissor_ci, r_c01_w.ds_orgao_emissor_imp, 'DS_ORGAO_EMISSOR_CI', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.nr_pais_imp is not null) then
			select	max(nr_sequencia)
			into	nr_seq_pais_w
			from	pais
			where	cd_ptu = r_c01_w.nr_pais_imp;
			
			if	(nr_seq_pais_w is not null) and
				(nr_seq_pais_w <> r_c00_w.nr_seq_pais or r_c00_w.nr_seq_pais is null) then
				pls_gerar_solicitacao_alt(r_c00_w.nr_seq_pais, nr_seq_pais_w, 'NR_SEQ_PAIS', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
			end if;
		end if;
		
		if	(r_c01_w.cd_cns_imp is not null) and
			(r_c01_w.cd_cns_imp <> r_c00_w.nr_cartao_nac_sus or r_c00_w.nr_cartao_nac_sus is null) then
			pls_gerar_solicitacao_alt(r_c00_w.nr_cartao_nac_sus, r_c01_w.cd_cns_imp, 'NR_CARTAO_NAC_SUS', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.nr_pispasep_imp is not null) and
			(r_c01_w.nr_pispasep_imp <> r_c00_w.nr_pis_pasep or r_c00_w.nr_pis_pasep is null) then
			pls_gerar_solicitacao_alt(r_c00_w.nr_pis_pasep, r_c01_w.nr_pispasep_imp, 'NR_PIS_PASEP', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c01_w.cd_municipio_imp is not null) and
			(r_c01_w.cd_municipio_imp <> r_c00_w.cd_municipio_ibge or r_c00_w.cd_municipio_ibge is null) then
			pls_gerar_solicitacao_alt(r_c00_w.cd_municipio_ibge, r_c01_w.cd_municipio_imp, 'CD_MUNICIPIO_IBGE', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		select	count(1)
		into	qt_registro_w
		from	ptu_mov_pessoa_contato
		where	nr_seq_mov_benef	= r_c00_w.nr_seq_mov_benef
		and	ie_tipo_telefone_imp	= '3';
		
		if	(qt_registro_w > 0) then
			select	nr_ddd_imp,
				nr_telefone_imp
			into	nr_ddd_imp_w,
				nr_telefone_imp_w
			from	ptu_mov_pessoa_contato
			where	nr_seq_mov_benef	= r_c00_w.nr_seq_mov_benef
			and	ie_tipo_telefone_imp	= '3';
			
			if	(nr_ddd_imp_w is not null) and
				(nr_ddd_imp_w <> r_c00_w.nr_ddd_celular or r_c00_w.nr_ddd_celular is null) then
				pls_gerar_solicitacao_alt(r_c00_w.nr_ddd_celular, nr_ddd_imp_w, 'NR_DDD_CELULAR', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
			end if;
			
			if	(nr_telefone_imp_w is not null) and
				(nr_telefone_imp_w <> r_c00_w.nr_telefone_celular or r_c00_w.nr_telefone_celular is null) then
				pls_gerar_solicitacao_alt(r_c00_w.nr_telefone_celular, nr_telefone_imp_w, 'NR_TELEFONE_CELULAR', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
			end if;
		end if;
		
		pls_tasy_gerar_solicitacao(r_c00_w.cd_pessoa_fisica,'A');
		
		
		--Nome da M�E
		if	(r_c01_w.nm_mae_imp is not null) and
			(upper(r_c01_w.nm_mae_imp) <> upper(r_c00_w.nm_mae) or r_c00_w.nm_mae is null) then
			pls_gerar_solicitacao_alt(r_c00_w.nm_mae, r_c01_w.nm_mae_imp, 'NM_CONTATO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		pls_tasy_gerar_solic_compl(r_c00_w.cd_pessoa_fisica, 5,'A','PTU_GERAR_SOLIC_ALT_A1350');
		
		end;
	end loop; --C01
	
	for r_c02_w in C02(r_c00_w.nr_seq_mov_benef) loop
		begin
		begin
		select	cd_cep,
			cd_tipo_logradouro,
			ds_endereco,
			nvl(nr_endereco, ds_compl_end) nr_endereco,
			ds_complemento,
			ds_bairro,
			ds_municipio,
			sg_estado,
			cd_municipio_ibge
		into	cd_cep_w,
			cd_tipo_logradouro_w,
			ds_endereco_w,
			nr_endereco_w,
			ds_complemento_w,
			ds_bairro_w,
			ds_municipio_w,
			sg_estado_w,
			cd_municipio_ibge_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= r_c00_w.cd_pessoa_fisica
		and	ie_tipo_complemento	= r_c02_w.ie_tipo_endereco_imp;
		exception
		when others then
			cd_cep_w		:= null;
			cd_tipo_logradouro_w	:= null;
			ds_endereco_w		:= null;
			nr_endereco_w		:= null;
			ds_complemento_w	:= null;
			ds_bairro_w		:= null;
			ds_municipio_w		:= null;
			sg_estado_w		:= null;
			cd_municipio_ibge_w	:= null;
		end;
		
		if	(r_c02_w.cd_cep_imp is not null) and
			(r_c02_w.cd_cep_imp <> cd_cep_w or cd_cep_w is null) then
			pls_gerar_solicitacao_alt(cd_cep_w, r_c02_w.cd_cep_imp, 'CD_CEP', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c02_w.ie_tipo_logradouro_imp is not null) then
			select	max(cd_tipo_logradouro)
			into	cd_tipo_logradouro_imp_w
			from	ptu_tipo_logradouro
			where	cd_ptu	= r_c02_w.ie_tipo_logradouro_imp
			and	sysdate between nvl(dt_inicio_vigencia,sysdate) and nvl(dt_fim_vigencia,sysdate);
			
			if	(cd_tipo_logradouro_imp_w is not null) and
				(cd_tipo_logradouro_imp_w <> cd_tipo_logradouro_w or cd_tipo_logradouro_w is null) then
				pls_gerar_solicitacao_alt(cd_tipo_logradouro_w, cd_tipo_logradouro_imp_w, 'CD_TIPO_LOGRADOURO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
			end if;
		end if;
		
		if	(r_c02_w.ds_logradouro_imp is not null) and
			(upper(r_c02_w.ds_logradouro_imp) <> upper(ds_endereco_w) or ds_endereco_w is null) then
			pls_gerar_solicitacao_alt(ds_endereco_w, r_c02_w.ds_logradouro_imp, 'DS_ENDERECO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c02_w.nr_logradouro_imp is not null) and
			(r_c02_w.nr_logradouro_imp <> nr_endereco_w or nr_endereco_w is null) then
			pls_gerar_solicitacao_alt(nr_endereco_w, r_c02_w.nr_logradouro_imp, 'NR_ENDERECO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c02_w.ds_complemento_imp is not null) and
			(upper(r_c02_w.ds_complemento_imp) <> upper(ds_complemento_w) or ds_complemento_w is null) then
			pls_gerar_solicitacao_alt(ds_complemento_w, r_c02_w.ds_complemento_imp, 'DS_COMPLEMENTO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c02_w.ds_bairro_imp is not null) and
			(upper(r_c02_w.ds_bairro_imp) <> upper(ds_bairro_w) or ds_bairro_w is null) then
			pls_gerar_solicitacao_alt(ds_bairro_w, r_c02_w.ds_bairro_imp, 'DS_BAIRRO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c02_w.ds_cidade_imp is not null) and
			(upper(r_c02_w.ds_cidade_imp) <> upper(ds_municipio_w) or ds_municipio_w is null) then
			pls_gerar_solicitacao_alt(ds_municipio_w, r_c02_w.ds_cidade_imp, 'DS_MUNICIPIO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c02_w.sg_uf_imp is not null) and
			(upper(r_c02_w.sg_uf_imp) <> upper(sg_estado_w) or sg_estado_w is null) then
			pls_gerar_solicitacao_alt(sg_estado_w, r_c02_w.sg_uf_imp, 'SG_ESTADO', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		if	(r_c02_w.cd_municipio_ibge_imp is not null) and
			(r_c02_w.cd_municipio_ibge_imp <> cd_municipio_ibge_w or cd_municipio_ibge_w is null) then
			pls_gerar_solicitacao_alt(cd_municipio_ibge_w, r_c02_w.cd_municipio_ibge_imp, 'CD_MUNICIPIO_IBGE', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
		end if;
		
		pls_tasy_gerar_solic_compl(r_c00_w.cd_pessoa_fisica, r_c02_w.ie_tipo_endereco_imp, 'A', 'PTU_GERAR_SOLIC_ALT_A1350');
		end;
	end loop; --C02
	
	for r_c03_w in C03(r_c00_w.nr_seq_mov_benef) loop
		begin
		begin
		select	nr_ddd_telefone,
			nr_telefone,
			ds_email
		into	nr_ddd_telefone_w,
			nr_telefone_w,
			ds_email_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= r_c00_w.cd_pessoa_fisica
		and	ie_tipo_complemento	= r_c03_w.ie_tipo_telefone_imp;
		exception
		when others then
			nr_ddd_telefone_w	:= null;
			nr_telefone_w		:= null;
		end;
		
		if	(r_c03_w.ie_tipo_telefone_imp <> '3') then
			if	(r_c03_w.nr_ddd_imp is not null) and
				(r_c03_w.nr_ddd_imp <> r_c00_w.nr_ddd_celular or r_c00_w.nr_ddd_celular is null) then
				pls_gerar_solicitacao_alt(r_c00_w.nr_ddd_celular, r_c03_w.nr_ddd_imp, 'NR_DDD_TELEFONE', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
			end if;
			
			if	(r_c03_w.nr_telefone_imp is not null) and
				(r_c03_w.nr_telefone_imp <> nr_telefone_w or nr_telefone_w is null) then
				pls_gerar_solicitacao_alt(nr_telefone_w, r_c03_w.nr_telefone_imp, 'NR_TELEFONE', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
			end if;
			
			pls_tasy_gerar_solic_compl(r_c00_w.cd_pessoa_fisica, r_c03_w.ie_tipo_telefone_imp, 'A', 'PTU_GERAR_SOLIC_ALT_A1350');
		end if;
		
		if	(r_c03_w.ie_tipo_email_imp = 1) then
			if	(r_c03_w.ds_email_imp is not null) and
				(r_c03_w.ds_email_imp <> ds_email_w or ds_email_w is null) then
				pls_gerar_solicitacao_alt(ds_email_w, r_c03_w.ds_email_imp, 'DS_EMAIL', r_c00_w.cd_pessoa_fisica, cd_estabelecimento_p, nm_usuario_p);
			end if;
			pls_tasy_gerar_solic_compl(r_c00_w.cd_pessoa_fisica, r_c03_w.ie_tipo_email_imp, 'A', 'PTU_GERAR_SOLIC_ALT_A1350');
		elsif	(r_c03_w.ie_tipo_email_imp = 2) then
			select	max(nr_sequencia)
			into	nr_seq_compl_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= r_c00_w.cd_pessoa_fisica
			and	ie_tipo_complemento	= 1;
			
			if	(nr_seq_compl_w is not null) then
				insert	into	compl_pf_tel_adic
					(	nr_sequencia, cd_pessoa_fisica, nr_seq_compl,
						dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
						ie_situacao, ie_divulgacao, ds_email)
					values(	compl_pf_tel_adic_seq.nextval, r_c00_w.cd_pessoa_fisica, nr_seq_compl_w,
						sysdate, sysdate, nm_usuario_p, nm_usuario_p,
						'A', 'N', ds_email_w);
			end if;
		end if;
		
		end;
	end loop; --C03
	
	end;
end loop; --C00

update	ptu_mov_pessoa_lote
set	ie_status		= 'C',
	dt_confirmacao		= sysdate,
	nm_usuario_confirmacao	= nm_usuario_p
where	nr_sequencia		= nr_seq_lote_p;

commit;

end ptu_gerar_solic_alt_a1350;
/
