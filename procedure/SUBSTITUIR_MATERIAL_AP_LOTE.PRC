create or replace
procedure substituir_material_ap_lote(	cd_material_atual_p	number,
					cd_material_subst_p	number,
					nr_seq_lote_p		number,
					qt_material_subst_p	number,
					nr_sequencia_p		number,
					nm_usuario_p		varchar2,
					nr_seq_lote_fornec_p	number,
					nr_id_p			number,
					nr_seq_material_p	number) is

qt_material_multi_w	number(15,2);
qt_existe_w		number(10);
qt_itens_lote_w		number(10);
qt_dispensar_w		number(15,2);

begin
select	count(*)
into	qt_existe_w
from	regra_mat_subst_atend
where	cd_material = cd_material_atual_p
and	qt_material is not null;

if	(nvl(qt_existe_w,0) > 0) then
	select	(qt_material_subst_p * b.qt_material)
	into	qt_material_multi_w
	from	regra_mat_subst_atend b
	where	b.cd_material = cd_material_atual_p
	and	nr_sequencia = nr_sequencia_p;
	
	if	(nvl(qt_material_multi_w,0) <> 0) then
		select	count(a.nr_sequencia)
		into	qt_itens_lote_w
		from	ap_lote_item a			
		where	a.nr_seq_lote = nr_seq_lote_p
		and	a.cd_material = cd_material_atual_p
		and	exists (	select	1
					from	prescr_mat_hor x
					where	x.nr_sequencia = a.nr_seq_mat_hor
					and	x.nr_seq_material = nr_seq_material_p);
	
		update	ap_lote_item a
		set	a.qt_dispensar = (qt_material_multi_w / qt_itens_lote_w)
		where	a.nr_seq_lote = nr_seq_lote_p
		and	a.cd_material = cd_material_atual_p
		and	exists (	select	1
					from	prescr_mat_hor x
					where	x.nr_sequencia = a.nr_seq_mat_hor
					and	x.nr_seq_material = nr_seq_material_p);
	end if;
end if;

select	nvl(sum(qt_dispensar),0)
into	qt_dispensar_w
from	ap_lote_item a
where	a.nr_seq_lote = nr_seq_lote_p
and	a.cd_material = cd_material_atual_p
and	exists (select	1
		from	prescr_mat_hor x
		where	x.nr_sequencia = a.nr_seq_mat_hor
		and	x.nr_seq_material = nr_seq_material_p);
		
		
update	ap_lote_item a
set	a.cd_material = cd_material_subst_p,
	a.cd_material_original = cd_material_atual_p
where	a.nr_seq_lote = nr_seq_lote_p
and	a.cd_material = cd_material_atual_p
and	exists (	select	1
			from	prescr_mat_hor x
			where	x.nr_sequencia = a.nr_seq_mat_hor
			and	x.nr_seq_material = nr_seq_material_p);
			
if	(nr_id_p > 0) then

	update	material_wbarras
	set	cd_material = cd_material_subst_p,
		ds_material = obter_desc_material(cd_material_subst_p),
		qt_material = qt_dispensar_w,
		sl_material = qt_dispensar_w
	where	cd_material = cd_material_atual_p
	and	nvl(nr_seq_lote_fornec_p,0) = nvl(fn_fornecedor,0)
	and	nr_id = nr_id_p
	and	nr_seq_material = nr_seq_material_p;
end if;

commit;

end substituir_material_ap_lote;
/