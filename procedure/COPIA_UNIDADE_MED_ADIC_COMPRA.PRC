create or replace
procedure copia_unidade_med_adic_compra(
			cd_estabelecimento_p	Number,
			cd_material_p		Number,
			cd_material_novo_p	Number,
			nm_usuario_p		Varchar2) is 

begin

insert into unidade_medida_adic_compra(
	nr_sequencia,
	cd_estabelecimento,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_material,
	cd_unidade_medida,
	qt_conversao)
select	unidade_medida_adic_compra_seq.nextval,
	cd_estabelecimento_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_material_novo_p,
	cd_unidade_medida,
	qt_conversao
from	unidade_medida_adic_compra
where	cd_material = cd_material_p
and	cd_estabelecimento = cd_estabelecimento_p;

commit;

end copia_unidade_med_adic_compra;
/