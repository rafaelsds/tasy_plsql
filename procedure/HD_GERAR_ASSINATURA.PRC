create or replace
procedure hd_gerar_assinatura(
			nr_dialise_dialisador_p		number,
			nr_lista_concentrado_p		varchar2,
			nr_seq_dialise_p		number,
			nr_seq_dial_teste_p		number,
			nr_seq_controle_p		number,
			nr_seq_lista_eventos_p		varchar2,
			nr_seq_dial_retirada_p		number,
			nr_lista_concentrado_ret_p	varchar2,
			nr_seq_alteracao_dialise_p	number,
			ie_opcao_p			varchar2,
			nm_usuario_p			varchar2,
			ie_commit_p			varchar2) is 

ie_assinatura_digital_w		hd_parametro.ie_assinatura_digital%type;
nr_sequencia_w			hd_assinatura_digital.nr_sequencia%type;
lista_eventos_w			dbms_sql.varchar2_table;
lista_concentrado_w		dbms_sql.varchar2_table;
lista_concentrado_ret_w		dbms_sql.varchar2_table;
nr_seq_evento_w			hd_dialise_evento.nr_sequencia%type;
nr_seq_concentrado_w		hd_dialise_concentrado.nr_sequencia%type;
nr_seq_concentrado_ret_w	hd_dialise_concentrado.nr_sequencia%type;
begin
	select	max(ie_assinatura_digital)
	into	ie_assinatura_digital_w
	from	hd_parametro
	where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
	
	if (ie_assinatura_digital_w is not null) and (ie_assinatura_digital_w = 'S') then
	
		select 	hd_assinatura_digital_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		insert into hd_assinatura_digital (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_opcao,
			dt_liberacao,
			nm_usuario_lib,
			nr_dialise_dialisador,
			nr_seq_dialise,
			nr_seq_dial_teste,
			nr_seq_controle,
			nr_seq_dial_retirada,
			nr_seq_alt_dial)
		values(
			nr_sequencia_w,
			sysdate,			
			nm_usuario_p,
			sysdate,
			nm_usuario_p,			
			ie_opcao_p,
			sysdate,
			nm_usuario_p,
			nr_dialise_dialisador_p,
			nr_seq_dialise_p,
			nr_seq_dial_teste_p,
			nr_seq_controle_p,
			nr_seq_dial_retirada_p,
			nr_seq_alteracao_dialise_p);
			
		if (nr_seq_lista_eventos_p is not null) then

			lista_eventos_w := obter_lista_string(nr_seq_lista_eventos_p, ',');
			for	i in lista_eventos_w.first..lista_eventos_w.last loop
				nr_seq_evento_w := to_number(lista_eventos_w(i));
				
				insert into hd_assinatura_item (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_assinatura,
					nr_seq_evento)
				values(
					hd_assinatura_item_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_sequencia_w,
					nr_seq_evento_w);

			end loop;
			
		end if;
		
		if (nr_lista_concentrado_p is not null) then
			
			lista_concentrado_w := obter_lista_string(nr_lista_concentrado_p, ',');
			for	i in lista_concentrado_w.first..lista_concentrado_w.last loop
				nr_seq_concentrado_w := to_number(lista_concentrado_w(i));
				
				insert into hd_assinatura_item (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_assinatura,
					nr_seq_concentrado)
				values(
					hd_assinatura_item_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_sequencia_w,
					nr_seq_concentrado_w);

			end loop;
			
		end if;
		
		if (nr_lista_concentrado_ret_p is not null) then

			lista_concentrado_ret_w := obter_lista_string(nr_lista_concentrado_ret_p, ',');
			for	i in lista_concentrado_ret_w.first..lista_concentrado_ret_w.last loop
				nr_seq_concentrado_ret_w := to_number(lista_concentrado_ret_w(i));
				
				insert into hd_assinatura_item (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_assinatura,
					nr_seq_concentrado_ret)
				values(
					hd_assinatura_item_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_sequencia_w,
					nr_seq_concentrado_ret_w);

			end loop;
			
		end if;
			
	end if;		

if (ie_commit_p = 'S') then
	commit;
end if;

end hd_gerar_assinatura;
/
