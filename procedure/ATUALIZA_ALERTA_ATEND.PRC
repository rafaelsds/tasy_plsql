create or replace
procedure atualiza_alerta_atend(	nr_seq_atend_alerta_p	number,
				ie_allow_free_text_p	varchar2,
				nr_seq_alerta_p 		number,
				ds_alerta_p		varchar2) is 

begin

if (nvl(ie_allow_free_text_p, 'Y') = 'Y') then
	update	atendimento_alerta
	set 	ds_alerta = ds_alerta_p
	where 	nr_sequencia = nr_seq_atend_alerta_p;
else
	update	atendimento_alerta
	set 	nr_seq_alerta = nr_seq_alerta_p
	where 	nr_sequencia = nr_seq_atend_alerta_p;
end if;

commit;

end atualiza_alerta_atend;
/