create or replace
procedure fis_gerar_regra_efd_reg(	nr_sequencia_p	number,
					nm_usuario_p	varchar2) is 

cd_registro_w			fis_efd_cad_registro.cd_registro%type;
ie_gerar_w			varchar2(01);
ie_tipo_sped_w			varchar2(1);
ie_tipo_sped_regra_w		varchar2(1);

cursor C01 is
select	a.cd_registro
from	fis_efd_cad_registro a
where	a.ie_arquivo	= ie_tipo_sped_regra_w
and 	a.ie_situacao 	= 'A'
order by	a.nr_seq_apres;

begin

select	decode(ie_tipo_sped, 1, 1, 2),
	ie_tipo_sped
into	ie_tipo_sped_regra_w,
	ie_tipo_sped_w
from	fis_regra_efd
where	nr_sequencia	= nr_sequencia_p;

open C01;
loop
fetch C01 into	
	cd_registro_w;
exit when C01%notfound;
	begin
	
	ie_gerar_w	:= 'N';

	if	(ie_tipo_sped_w = 1) then
		if	(cd_registro_w in ('0000','0001','0100','0110','0140','0190','0200','0990')) then	-- Bloco 0
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('C001','C100','C170','C990')) then					-- Bloco C
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('D001','D990','1001','1990','9001','9900','9990','9999')) then	-- Bloco D 1 9 
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('0005','0400','C140','C141','E001','E990')) then			-- Bloco  0 C E
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('H001','H005','H010','H990')) then					-- Bloco H
			ie_gerar_w	:= 'S';
		end if;
	elsif	(ie_tipo_sped_w = 2) then
		if	(cd_registro_w in ('0000','0001','0100','0110','0140','0190','0200','0990')) then	-- Bloco 0
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('C001','C100','C170','C990')) then					-- Bloco C
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('D001','D990','1001','1990','9001','9900','9990','9999')) then	-- Bloco D 1 9
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('A001','A010','A100','A170','A990','C010')) then			-- Bloco A C
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('F001','F990','M001','M990')) then					-- Bloco F M
			ie_gerar_w	:= 'S';
		end if;
	elsif	(ie_tipo_sped_w = 3) then
		if	(cd_registro_w in ('0000','0001','0100','0110','0140','0500')) then			-- Bloco 0
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('F001','F990')) then							-- Bloco F
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('I001','I010','I100','I200','I300','I990')) then			-- Bloco I
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('M001','M990')) then							-- Bloco M
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('1001','1990')) then							-- Bloco 1
			ie_gerar_w	:= 'S';
		elsif	(cd_registro_w in ('9001','9900','9990','9999')) then					-- Bloco 9
			ie_gerar_w	:= 'S';
		end if;
	end if;
	
	insert into fis_regra_efd_reg(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_regra_efd,
		cd_registro,
		ie_gerar)
	values(	fis_regra_efd_reg_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_sequencia_p,
		cd_registro_w,
		ie_gerar_w);
	
	end;
end loop;
close C01;

commit;

end fis_gerar_regra_efd_reg;
/