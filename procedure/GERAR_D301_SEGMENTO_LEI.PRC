create or replace procedure gerar_d301_segmento_lei(nr_seq_dataset_p	number,
                                   			       nm_usuario_p		varchar2) is 
  
  cursor c01 is
   select  ap.nr_atendimento,
             null nr_seq_301_area_atuacao,
             null nr_seq_301_tipo_servico,
             null nr_seq_301_serv_pia,  
             to_number(somente_numero_char(clr.cd_tumor_primario)) nr_seq_301_tnm_classif_tumor,
             to_number(somente_numero_char(clr.cd_grau_histo))           nr_seq_301_tnm_grau,
             to_number(somente_numero_char(clr.cd_metastase_distancia))  nr_seq_301_tnm_metastase_dist,
             to_number(somente_numero_char(clr.cd_linfonodo_regional))   nr_seq_301_tnm_metastase_linf,
             null                        nr_seq_301_tnm_recorrencia,
             to_number(somente_numero_char(clr.cd_tumor_residual))       nr_seq_301_tnm_tumor_resid,
             to_char(clr.dt_avaliacao,'yyyymmdd')            ds_dt_servico
      from d301_dataset_envio        d3,
           atendimento_paciente      ap,
           can_loco_regional         clr 
      where d3.nr_atendimento    = ap.nr_atendimento 
      and   d3.nr_sequencia      = nr_seq_dataset_p
      and   clr.nr_atendimento   = ap.nr_atendimento;
    
  c01_w c01%rowtype;

  
begin
  open c01;
  fetch c01 into c01_w;
  loop
    insert into d301_segmento_lei(nr_sequencia, 
                                  dt_atualizacao, 
                                  nm_usuario, 
                                  dt_atualizacao_nrec, 
                                  nm_usuario_nrec, 
                                  nr_seq_dataset, 
                                  nr_seq_301_area_atuacao, 
                                  nr_seq_301_tipo_servico, 
                                  nr_seq_301_serv_pia, 
                                  nr_seq_301_tnm_classif_tumor, 
                                  nr_seq_301_tnm_grau, 
                                  nr_seq_301_tnm_metastase_dist, 
                                  nr_seq_301_tnm_metastase_linf, 
                                  nr_seq_301_tnm_recorrencia, 
                                  nr_seq_301_tnm_tumor_resid, 
                                  ds_dt_servico) values (d301_segmento_lei_seq.nextval,
                                                                     sysdate,
                                                                     nm_usuario_p,
                                                                     sysdate,
                                                                     nm_usuario_p,
                                                                     nr_seq_dataset_p,
                                                                    c01_w.nr_seq_301_area_atuacao, 
                                                                    c01_w.nr_seq_301_tipo_servico, 
                                                                    c01_w.nr_seq_301_serv_pia, 
                                                                    c01_w.nr_seq_301_tnm_classif_tumor, 
                                                                    c01_w.nr_seq_301_tnm_grau, 
                                                                    c01_w.nr_seq_301_tnm_metastase_dist, 
                                                                    c01_w.nr_seq_301_tnm_metastase_linf, 
                                                                    c01_w.nr_seq_301_tnm_recorrencia, 
                                                                    c01_w.nr_seq_301_tnm_tumor_resid, 
                                                                    c01_w.ds_dt_servico);  
  
  fetch c01 into c01_w;
  exit when c01%notfound;     
  end loop;
  close c01;
  
end gerar_d301_segmento_lei;
/