create or replace
procedure gerar_proc_pac_medico_adep(	nr_seq_propaci_p	number,
							nr_seq_proc_hor_p	number) is 

proc_pac_medico_w		procedimento_pac_medico%rowtype;
procedimento_paciente_w	procedimento_paciente%rowtype;
	
begin

    if (obter_uso_case(wheb_usuario_pck.get_nm_usuario) = 'S') then 
        
        select	nvl(max(nr_sequencia),0)
        into	proc_pac_medico_w.nr_sequencia
        from	procedimento_pac_medico
        where	nr_seq_propaci = nr_seq_propaci_p;
        
        if (proc_pac_medico_w.nr_sequencia = 0 and nr_seq_propaci_p is not null) then
        
            select	procedimento_pac_medico_seq.nextval
            into	proc_pac_medico_w.nr_sequencia
            from	dual;
        
            select	max(nr_sequencia),
                    max(nr_atendimento),
                    max(nm_usuario),
                    max(nm_usuario_original),
                    max(cd_procedimento),
                    max(nr_seq_proc_interno),
                    max(ie_origem_proced),
                    max(qt_procedimento),
                    max(dt_procedimento),
                    max(ie_lado),
                    max(nr_seq_proc_hor_p),
                    max(cd_setor_atendimento),
                    max(obter_episodio_atendimento(nr_atendimento)),
                    sysdate,
                    sysdate,
                    sysdate,
                    'A'
            into	proc_pac_medico_w.nr_seq_propaci,
                    proc_pac_medico_w.nr_atendimento,
                    proc_pac_medico_w.nm_usuario,
                    proc_pac_medico_w.nm_usuario_nrec,
                    proc_pac_medico_w.cd_procedimento,
                    proc_pac_medico_w.nr_seq_proc_interno,
                    proc_pac_medico_w.ie_origem_proced,
                    proc_pac_medico_w.qt_procedimento,
                    proc_pac_medico_w.dt_procedimento,
                    proc_pac_medico_w.ie_lado,
                    proc_pac_medico_w.nr_seq_proc_hor,
                    proc_pac_medico_w.cd_setor_atendimento,
                    proc_pac_medico_w.nr_seq_episodio,
                    proc_pac_medico_w.dt_atualizacao,
                    proc_pac_medico_w.dt_atualizacao_nrec,
                    proc_pac_medico_w.dt_liberacao,
                    proc_pac_medico_w.ie_situacao
            from	procedimento_paciente
            where	nr_sequencia = nr_seq_propaci_p;

            if (proc_pac_medico_w.nr_seq_propaci is not null) then
                insert into procedimento_pac_medico values proc_pac_medico_w;
            end if;
            
        elsif (nr_seq_proc_hor_p is not null) then
            delete	procedimento_pac_medico
            where	nr_seq_proc_hor = nr_seq_proc_hor_p;
        
        end if;
    end if;

end gerar_proc_pac_medico_adep;
/
