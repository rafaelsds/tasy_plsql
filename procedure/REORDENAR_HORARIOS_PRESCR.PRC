CREATE OR REPLACE
PROCEDURE Reordenar_Horarios_Prescr
			(
			dt_Hora_Inicio_p		Date,
			ds_horarios_p		in Out	Varchar2) is
			


hr_inicio_w		Number(5,0);
hr_prime_w		Number(5,0);
ie_minuto_w		Number(10,0) := 0;
hr_menor_w		Number(5,0) := 0;
ds_validos_w		Varchar2(20) := '1234567890: ';
X			Varchar2(1);
i			Integer;
ie_ok_w			Varchar2(1) := 'S';

begin

if	(ds_horarios_p is not null) then
	begin
	FOR i IN 1..length(ds_horarios_p) LOOP
		X	:= substr(ds_horarios_p, i, 1);	
		if	(instr(ds_validos_w, X) = 0) then
			ie_ok_w	:= 'N';
			exit;
		end if;
	END LOOP;
	end;
end if;	



if	((ds_horarios_p <> '') or
	(ds_horarios_p is not null)) and
	(ds_horarios_p not in ('ACM','SN')) and
	(ie_ok_w = 'S') then

	ie_minuto_w := to_number(instr(ds_horarios_p,':'));

	for i in 1.. 25 LOOP
		begin
		hr_inicio_w	:= campo_numerico(substr(ds_horarios_p,1,2));
		hr_prime_w	:= campo_numerico(to_char(dt_Hora_Inicio_p,'hh24'));
		if 	(hr_inicio_w < hr_prime_w) and 
		  	(length(ds_horarios_p) > 4) and
			(hr_menor_w < hr_inicio_w) then
			hr_menor_w	:= hr_inicio_w;
			if	(ie_minuto_w = 0) then
				if (hr_inicio_w < 10) then
					ds_horarios_p := substr(ds_horarios_p,4,length(ds_horarios_p)-2) || ' 0'|| hr_inicio_w;
				else
					ds_horarios_p := substr(ds_horarios_p,4,length(ds_horarios_p)-2) || ' '|| hr_inicio_w;
				end if;
			else
				if (hr_inicio_w < 10) then
					ds_horarios_p := substr(ds_horarios_p,7,length(ds_horarios_p)-5) || ' 0'|| hr_inicio_w ||substr(ds_horarios_p,3,3);
				else
					ds_horarios_p := substr(ds_horarios_p,7,length(ds_horarios_p)-5) || ' '|| hr_inicio_w ||substr(ds_horarios_p,3,3);
				end if;
			end if;
	        end if; 
		end;
	end loop;
end if;
END Reordenar_Horarios_Prescr;
/
