create or replace
procedure Inserir_Medic_Uso_Padrao (	cd_pessoa_fisica_p	varchar2,
					nr_seq_receptor_p	number,
					ds_lista_medic_p	varchar2,
					nm_usuario_p		varchar2) is

lista_informacao_w		varchar2(1000);
ie_contador_w			number(10,0)	:= 0;
tam_lista_w			number(10,0);
ie_pos_virgula_w		number(3,0);
nr_seq_medic_w			number(10,0);

begin

lista_informacao_w	:= ds_lista_medic_p;

while	(lista_informacao_w is not null or ie_contador_w > 200) loop
	begin
	
	tam_lista_w			:= length(lista_informacao_w);
	ie_pos_virgula_w		:= instr(lista_informacao_w,',');

	/* Obter a sequencia lida */
	if	(ie_pos_virgula_w <> 0) then
		nr_seq_medic_w		:= substr(lista_informacao_w,1,(ie_pos_virgula_w - 1));
		lista_informacao_w	:= substr(lista_informacao_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;

	insert into medic_uso_continuo (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		dt_inicio,
		cd_material,
		qt_dose,
		cd_unidade_medida,
		nr_dias_uso,
		ie_uso_continuo,
		ie_laudo_lme,
		cd_cid_principal,
		cd_cid_secundario,
		cd_intervalo,
		ie_via_aplicacao,
		ds_justificativa,
		ds_observacao,
		ie_utiliza_dialise
	)
	select	medic_uso_continuo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_p,
		sysdate,
		cd_material,
		qt_dose,
		cd_unidade_medida,
		nr_dias_uso,
		ie_uso_continuo,
		ie_laudo_lme,
		cd_cid_principal,
		cd_cid_secundario,
		cd_intervalo,
		ie_via_aplicacao,
		ds_justificativa,
		ds_observacao,
		ie_utiliza_dialise		
	from	medic_uso_continuo_padrao
	where	nr_sequencia	= nr_seq_medic_w;
	
	ie_contador_w	:= ie_contador_w + 1;
	
	end;
end loop;

commit;

end Inserir_Medic_Uso_Padrao;
/