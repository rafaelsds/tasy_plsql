create or replace
procedure lab_atualizar_result_contagem (nr_prescricao_p	number,
				 nr_seq_prescr_p	number,
				 cd_exame1_p		varchar2,
				 cd_exame2_p		varchar2,
				 cd_exame3_p		varchar2,
				 cd_exame4_p		varchar2,
				 cd_exame5_p		varchar2,
				 cd_exame6_p		varchar2,
				 cd_exame7_p		varchar2,
				 cd_exame8_p		varchar2,
				 cd_exame9_p		varchar2,
				 cd_exame10_p		varchar2,
				 cd_exame11_p		varchar2,
				 cd_exame12_p		varchar2,
				 qt_resultado1_p	number,
				 qt_resultado2_p	number,
				 qt_resultado3_p	number,
				 qt_resultado4_p	number,
				 qt_resultado5_p	number,
				 qt_resultado6_p	number,
				 qt_resultado7_p	number,
				 qt_resultado8_p	number,
				 qt_resultado9_p	number,
				 qt_resultado10_p	number,
				 qt_resultado11_p	number,
				 qt_resultado12_p	number,
				 nm_usuario_p		Varchar2,
				 ds_erro_p		out varchar2) is 

nr_seq_exame1_w		number(10);
nr_seq_exame2_w		number(10);
nr_seq_exame3_w		number(10);
nr_seq_exame4_w		number(10);
nr_seq_exame5_w		number(10);
nr_seq_exame6_w		number(10);
nr_seq_exame7_w		number(10);
nr_seq_exame8_w		number(10);
nr_seq_exame9_w		number(10);
nr_seq_exame10_w	number(10);
nr_seq_exame11_w	number(10);
nr_seq_exame12_w	number(10);
ds_erro_w			varchar2(4000);
nr_seq_resultado_w	number(10);
nr_seq_exame_princ_w	number(10);
nr_seq_material_w	number(10);
qt_resultado_w		number(10);
pr_resultado_w		number(10);
ds_resultado_w		varchar2(255);
nr_seq_exame_w		number(10);
ie_percent_valor_w  varchar2(1);


Cursor C01 is
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame1_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame2_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame3_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame4_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame5_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame6_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame7_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame8_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame9_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame10_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame11_p
	union all
	select nr_seq_exame
	from exame_laboratorio
	where cd_exame = cd_exame12_p;
		
cursor c02 is
	select	nr_seq_exame,
		qt_resultado,
		pr_resultado,
		ds_resultado
	from exame_lab_result_item
	where nr_seq_resultado	= nr_seq_resultado_w
	  and nr_seq_prescr	= nr_seq_prescr_p
	  and ((qt_resultado <> 0) or (pr_resultado <> 0) or (ds_resultado is not null))
	order by nr_sequencia;
	
begin

ie_percent_valor_w	:= nvl(obter_valor_param_usuario(-2265, 41, obter_perfil_ativo,nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),'P');

/*select	a.nr_seq_exame,
		a.nr_seq_material,
		a.qt_resultado,
		a.pr_resultado,
		a.ds_resultado
into	nr_seq_exame_princ_w,
		nr_seq_material_w,
		qt_resultado_w,
		pr_resultado_w,
		ds_resultado_w
from	exame_lab_result_item a,
		exame_lab_resultado b,
		prescr_procedimento c
where	a.nr_seq_prescr = c.nr_sequencia
and		a.nr_seq_resultado = b.nr_seq_resultado
and		c.nr_prescricao = b.nr_prescricao
and		c.nr_prescricao = nr_prescricao_p
and		a.nr_seq_material is not null;*/

select	max(nr_seq_material)
into	nr_seq_material_w
from 	exame_lab_result_item
where 	nr_seq_resultado	= nr_seq_resultado_w
and 	nr_seq_prescr	= nr_seq_prescr_p
and 	nr_seq_material is not null;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame1_w	
from 	exame_laboratorio
where 	cd_exame = cd_exame1_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame2_w
from 	exame_laboratorio
where 	cd_exame = cd_exame2_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame3_w
from 	exame_laboratorio
where 	cd_exame = cd_exame3_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame4_w
from 	exame_laboratorio
where 	cd_exame = cd_exame4_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame5_w
from 	exame_laboratorio
where 	cd_exame = cd_exame5_p;

select  nvl(max(nr_seq_exame), '')
into	nr_seq_exame6_w
from 	exame_laboratorio
where 	cd_exame = cd_exame6_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame7_w
from 	exame_laboratorio
where 	cd_exame = cd_exame7_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame8_w
from 	exame_laboratorio
where 	cd_exame = cd_exame8_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame9_w
from 	exame_laboratorio
where 	cd_exame = cd_exame9_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame10_w
from 	exame_laboratorio
where 	cd_exame = cd_exame10_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame11_w
from 	exame_laboratorio
where 	cd_exame = cd_exame11_p;

select 	nvl(max(nr_seq_exame), '')
into	nr_seq_exame12_w
from 	exame_laboratorio
where 	cd_exame = cd_exame12_p;

select 	MAX(b.nr_seq_resultado)
into	nr_seq_resultado_w
from	exame_laboratorio a,
		exame_lab_resultado b,
		exame_lab_result_item e,
		prescr_procedimento c,
		prescr_medica d		
where	a.nr_seq_exame 	= e.nr_seq_exame
and		b.nr_prescricao = c.nr_prescricao
and		c.nr_prescricao = d.nr_prescricao
and		e.nr_seq_prescr = c.nr_sequencia
and		b.nr_seq_resultado = e.nr_seq_resultado
and		c.nr_prescricao = nr_prescricao_p
and		c.nr_sequencia  = nr_seq_prescr_p;

if (nr_seq_resultado_w is not null) then
	if 	(nr_prescricao_p is not null) and
		(nr_seq_prescr_p is not null) then
		begin
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado1_p, qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado1_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame1_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado2_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado2_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame2_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado3_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado3_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame3_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado4_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado4_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame4_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado5_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado5_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame5_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado6_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado6_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame6_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado7_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado7_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame7_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado8_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado8_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame8_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado9_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado9_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame9_w;
			
			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado10_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado10_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame10_w;

			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado11_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado11_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame11_w;

			update 	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',qt_resultado12_p,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',qt_resultado12_p,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where 	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr	 = nr_seq_prescr_p
			and		nr_seq_exame	 = nr_seq_exame12_w;
			
			update	exame_lab_result_item
			set		qt_resultado = decode(ie_percent_valor_w,'V',ds_resultado_w,qt_resultado),
            		pr_resultado = decode(ie_percent_valor_w,'P',ds_resultado_w,pr_resultado),
					nm_usuario = nm_usuario_p,
					dt_atualizacao = sysdate
			where	nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr = nr_seq_prescr_p
			and		nr_seq_exame = nr_seq_exame_princ_w;
			
			open c02;
			loop
			fetch c02 into	nr_seq_exame_w,
					qt_resultado_w,
					pr_resultado_w,
					ds_resultado_w;
				exit when c02%notfound;

				calcular_valores_exame(nr_seq_resultado_w,
							nr_prescricao_p,
							nr_seq_prescr_p,
							nr_seq_material_w,
							nr_seq_exame_w,
							qt_resultado_w,
							pr_resultado_w,
							ds_resultado_w);
			end loop;
			close c02;
			
		exception
		when others then
			ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279118,null)||substr(sqlerrm,1,3500);
		end;
	end if;
else
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(279119,null)||nr_prescricao_p||'.';
end if;
commit;
ds_erro_p := ds_erro_w;

end lab_atualizar_result_contagem;
/
