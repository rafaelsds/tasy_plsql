create or replace
procedure excluir_estrutura_inventario(
		nr_sequencia_p		number,
		cd_grupo_mat_p		number,
		cd_subgrupo_mat_p	number,
		cd_classe_mat_p		number) is

cd_material_w		number(6);

cursor c01 is
select	a.cd_material
from	inventario b,
	inventario_material a,
	estrutura_material_v x
where	b.nr_sequencia 		= a.nr_seq_inventario
and	x.cd_material  	 	= a.cd_material
and	nr_seq_inventario 		= nr_sequencia_p
and	b.dt_bloqueio	is null
and	(x.cd_grupo_material    	= nvl(cd_grupo_mat_p,0) or
	x.cd_subgrupo_material  	= nvl(cd_subgrupo_mat_p,0) or
	x.cd_classe_material    	= nvl(cd_classe_mat_p,0));
begin

open c01;
loop
fetch c01 into
	cd_material_w;	
exit when c01%notfound;
	begin
	delete
	from 	inventario_material
	where	nr_seq_inventario	= nr_sequencia_p
	and	cd_material	= cd_material_w;
	commit;
	end;
end loop;
close c01;


end excluir_estrutura_inventario;
/