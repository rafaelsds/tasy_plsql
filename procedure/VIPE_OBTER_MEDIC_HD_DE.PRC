create or replace
procedure vipe_obter_medic_hd_de (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p		number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number,
		nr_seq_dialise_p		number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p		date,					
		dt_validade_limite_p		date,					
		ie_exibir_hor_realizados_p	varchar2,
		ie_exibir_hor_suspensos_p	varchar2,
		ie_regra_inclusao_p		varchar2,
		ie_data_lib_prescr_p		varchar2,
		ie_lib_pend_rep_p		varchar2,
		ie_exibir_suspensos_p		varchar2,
		ie_agrupar_acm_sn_p		varchar2,
		ie_agrupar_dose_esp_p		varchar2, 
		ie_diluicao_acm_sn_gestao_p	varchar2,
		ie_prescr_setor_p		varchar2,
		cd_setor_paciente_p		number) is

nr_seq_wadep_w		number(10,0);
nr_prescricao_w		number(14,0);
nr_seq_material_w	number(6,0);
cd_material_w		number(6,0);
ds_material_w		varchar2(255);
ie_acm_sn_w		varchar2(1);
cd_intervalo_w		varchar2(7);
qt_dose_w		number(15,3);
nr_agrupamento_w	number(7,1);
ds_prescricao_w		varchar2(100);
ds_dil_obs_w		varchar2(2000);
ie_status_w		varchar2(1);
ie_lib_pend_rep_w	varchar2(1);
cd_unid_med_qtde_w	varchar2(30);
ie_via_administracao_w	varchar2(5);
ds_interv_prescr_w	varchar2(15);
					
cursor c01 is
select	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), null),
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_seq_material, null), null),	
	cd_material,
	ds_material,
	ie_acm_sn,	
	cd_intervalo,
	qt_dose,
	nr_agrupamento,
	ds_prescricao,
	ds_dil_obs,
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', decode(ie_suspenso, 'S', ie_suspenso, null), null), null) ie_status,
	cd_unidade_medida_dose,
	ie_via_aplicacao,
	ds_intervalo
from	(
	select	a.nr_prescricao,
		c.nr_seq_material,
		c.cd_material,
		y.ds_material,
		obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) ie_acm_sn,		
		x.cd_intervalo,
		x.qt_dose,
		decode(obter_se_agrupa_composto(a.nr_prescricao,c.nr_seq_material,x.nr_agrupamento,c.cd_material),'S',x.nr_agrupamento,0) nr_agrupamento,
		'DE ' || to_char(x.qt_dose_especial) || ' ' || x.cd_unidade_medida_dose ds_prescricao,
		decode(obter_se_acm_sn(x.ie_acm,x.ie_se_necessario),'S',substr(adep_obter_inf_dil_obs(a.nr_prescricao,c.nr_seq_material),1,2000),null) ds_dil_obs,
		nvl(x.ie_suspenso,'N') ie_suspenso,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao,
		substr(obter_desc_intervalo_prescr(x.cd_intervalo),1,15) ds_intervalo
	from	material y,
		prescr_material x,
		prescr_mat_hor c,
		prescr_medica a
	where	y.cd_material = x.cd_material
	and	x.nr_prescricao = c.nr_prescricao
	and	x.nr_sequencia = c.nr_seq_material	
	and	x.nr_prescricao = a.nr_prescricao
	and	c.nr_prescricao = a.nr_prescricao	
	and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
	and	a.nr_atendimento = nr_atendimento_p
	and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_p) = 'S'
	and	x.ie_agrupador = 1
	and	((ie_exibir_suspensos_p = 'S') or (x.dt_suspensao is null))	
	and	obter_se_gerar_item_adep('M',a.nr_prescricao,c.nr_seq_material,x.nr_agrupamento,'N') = 'S'
	and	nvl(c.ie_situacao,'A') = 'A'
	and	nvl(c.ie_adep,'S') = 'S'
	and	c.ie_agrupador = 1
	and	nvl(c.ie_dose_especial,'N') = 'S'
	and	(((obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) = 'N') and (c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p)) or
		 ((obter_se_acm_sn(x.ie_acm,x.ie_se_necessario) = 'S') and (obter_se_prescr_vig_adep(a.dt_inicio_prescr,a.dt_validade_prescr,dt_inicial_horarios_p,dt_final_horarios_p) = 'S')))
	and	((ie_exibir_hor_realizados_p = 'S') or (c.dt_fim_horario is null))
	and	((ie_exibir_hor_suspensos_p = 'S') or (c.dt_suspensao is null))	
	and	c.nr_seq_dialise = nr_seq_dialise_p
	and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = cd_setor_paciente_p)))
	and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
	group by
		a.nr_prescricao,
		c.nr_seq_material,
		c.cd_material,
		y.ds_material,
		x.ie_acm,
		x.ie_se_necessario,		
		x.cd_intervalo,
		x.qt_dose,
		x.qt_dose_especial,
		x.cd_unidade_medida_dose,
		x.nr_agrupamento,
		x.ie_suspenso,
		x.cd_unidade_medida_dose,
		x.ie_via_aplicacao
	)
group by
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_prescricao, null), null),
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', nr_seq_material, null), null),	
	cd_material,
	ds_material,
	ie_acm_sn,	
	cd_intervalo,
	qt_dose,
	nr_agrupamento,
	ds_prescricao,
	ds_dil_obs,
	decode(ie_acm_sn, 'S', decode(obter_se_agrupar_acm_sn_adep(nr_prescricao,nr_seq_material,ie_agrupar_acm_sn_p), 'N', decode(ie_suspenso, 'S', ie_suspenso, null), null), null),
	cd_unidade_medida_dose,
	ie_via_aplicacao,
	ds_intervalo;
	
begin
open c01;
loop
fetch c01 into	nr_prescricao_w,
		nr_seq_material_w,
		cd_material_w,
		ds_material_w,
		ie_acm_sn_w,		
		cd_intervalo_w,
		qt_dose_w,
		nr_agrupamento_w,
		ds_prescricao_w,
		ds_dil_obs_w,
		ie_status_w,
		cd_unid_med_qtde_w,
		ie_via_administracao_w,
		ds_interv_prescr_w;
exit when c01%notfound;
	begin
	select	w_vipe_t_seq.nextval
	into	nr_seq_wadep_w
	from	dual;
	
	insert into w_vipe_t (
		nr_sequencia,
		nm_usuario,
		ie_tipo_item,
		nr_prescricao,
		nr_seq_item,		
		cd_item,
		ds_item,
		ie_acm_sn,		
		cd_intervalo,
		qt_item,
		nr_agrupamento,
		ds_prescricao,
		ds_diluicao,
		ie_status_item,
		nr_seq_proc_interno,
		ie_diferenciado,
		nr_prescricoes,
		cd_unid_med_qtde,
		ie_via_aplicacao,
		ds_interv_prescr)
	values (
		nr_seq_wadep_w,
		nm_usuario_p,
		'ME',
		nr_prescricao_w,
		nr_seq_material_w,
		cd_material_w,
		ds_material_w,
		ie_acm_sn_w,
		cd_intervalo_w,
		qt_dose_w,
		nr_agrupamento_w,
		ds_prescricao_w,
		ds_dil_obs_w,
		ie_status_w,
		0,
		decode(ds_dil_obs_w,null,'N','S'),
		decode(ie_acm_sn_w,'S',nr_prescricao_w,null),
		cd_unid_med_qtde_w,
		ie_via_administracao_w,
		ds_interv_prescr_w);
	end;
end loop;
close c01;
end vipe_obter_medic_hd_de;
/
