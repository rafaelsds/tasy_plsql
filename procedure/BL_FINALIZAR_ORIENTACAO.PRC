create or replace
procedure BL_finalizar_orientacao(	nr_sequencia_p		Number,
					nm_usuario_p		Varchar2) is 

begin
if	(nr_sequencia_p is not null) then
	update	bl_orientacao
	set	dt_fim_atendimento 	= sysdate,
		nm_usuario_fim_atend 	= nm_usuario_p
	where	nr_sequencia 		= nr_sequencia_p;
end if;

commit;

end BL_finalizar_orientacao;
/