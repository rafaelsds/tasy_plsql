create or replace
procedure atualizar_unid_atend_mov_pac(
			nm_usuario_p		Varchar2,
			cd_setor_atendimento_p	number,
			cd_unidade_basica_p	varchar2,
			cd_unidade_compl_p	varchar2) is 

begin


begin

update	unidade_atendimento
set   	ie_status_unidade  		= 'G',
	nm_usuario 		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	cd_unidade_basica		= cd_unidade_basica_p
and	cd_unidade_compl		= cd_unidade_compl_p
and 	cd_setor_atendimento	= cd_setor_atendimento_p;

exception
when others then
	null;
end;


commit;

end atualizar_unid_atend_mov_pac;
/