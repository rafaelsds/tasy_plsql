create or replace
procedure gerar_prescr_atend_futuro(	nm_usuario_p			varchar2,
					cd_estabelecimento_p		number,
					nr_seq_atend_futuro_p 		number,
					nr_prescricao_p			out number,
					ie_rep_pt_p			varchar2) is
					
nr_prescricao_w			Number(14,0);
cd_intervalo_w             	Number(7)   	:= 9;
cd_setor_atendimento_w		Number(5) 	:= 0;
ie_origem_inf_w			Varchar(1);
cd_pessoa_fisica_w		varchar2(10);	
ie_gera_proced			varchar2(1);
nr_sequencia_w			number(6);
nr_atendimento_w		number(10);
dt_entrada_w			date;
cd_setor_atend_w		number(5,0);
dt_primeiro_hor_w		date;

BEGIN

select max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from   usuario
where  nm_usuario = nm_usuario_p;

select	cd_setor_atendimento
into	cd_setor_atendimento_w
from 	usuario
where 	nm_usuario = nm_usuario_p;

select	max(nr_atendimento)
into	nr_atendimento_w
from	atend_pac_futuro
where	nr_sequencia = nr_seq_atend_futuro_p;

if	(nr_atendimento_w is not null) then
	select	max(dt_entrada),
		max(obter_setor_atendimento(nr_atendimento))
	into	dt_entrada_w,
		cd_setor_atend_w
	from	atendimento_paciente
	where	nr_atendimento 	= nr_atendimento_w;
	
	dt_primeiro_hor_w := to_date(to_char(Obter_Prim_Horario_Prescricao(nr_atendimento_w,cd_setor_atend_w,dt_entrada_w,nm_usuario_p,'R')
				,'dd/mm/yyyy') ||' '||to_char(obter_prim_hor_setor(cd_setor_atend_w),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
end if;

-- Obter ie_origem_inf se � m�dico ou n�o
select 	nvl(max('1'),'3')
into	ie_origem_inf_w
from	Medico b, 
	Usuario a
where 	a.nm_usuario		= nm_usuario_p
and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;

Select 	Prescr_medica_seq.nextval  
into  	nr_prescricao_w
from 	dual;

	
insert into prescr_medica (
	nr_prescricao            ,      
	cd_pessoa_fisica         ,      
	nr_atendimento           ,      
	cd_medico                ,      
	dt_prescricao            ,      
	dt_atualizacao           ,      
	nm_usuario               ,
	nm_usuario_original	 ,      
	nr_horas_validade        ,      
	dt_primeiro_horario      ,      
	dt_liberacao             ,      
	cd_setor_atendimento,
	cd_setor_entrega,
	dt_entrega,
	ie_origem_inf,
	ie_recem_nato,
	cd_estabelecimento,
	cd_prescritor,
	nr_seq_atend_futuro,
	ie_adep)
select 	nr_prescricao_w,
	a.cd_pessoa_fisica,
	nr_atendimento_w,
	cd_pessoa_fisica_w,
	nvl(dt_entrada_w,a.dt_registro),
	sysdate,
	nm_usuario_p,
	nm_usuario_p,		
	24,
	nvl(dt_primeiro_hor_w,a.dt_registro),
	null,
	cd_setor_atendimento_w,
	cd_setor_atendimento_w,
	null,
	ie_origem_inf_w,
	'N',
	cd_estabelecimento_p,
	obter_dados_usuario_opcao(nm_usuario_p, 'C'),
	nr_seq_atend_futuro_p,
	ie_rep_pt_p
from  	atend_pac_futuro a
where 	a.nr_sequencia = nr_seq_atend_futuro_p;

commit;

nr_prescricao_p	:= nr_prescricao_w;

ie_gera_proced := Obter_Valor_Param_Usuario(10000, 26, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);

if 	(nr_prescricao_w > 0)
and 	(ie_gera_proced = 'S')then

	select	nvl(max(nr_sequencia), 0)
	into	nr_sequencia_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_w;
	
	insert into prescr_procedimento(
		nr_prescricao,
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		qt_procedimento,
		dt_atualizacao,
		nm_usuario,
		ie_origem_inf,
		ie_lado)
	select	nr_prescricao_w,
		rownum+nr_sequencia_w,
		cd_procedimento,
		ie_origem_proced,
		1,
		sysdate,
		nm_usuario_p,
		ie_origem_inf_w,
		ie_lado
	from	atend_pac_proced_previsto
	where	nr_seq_atend_futuro = nr_seq_atend_futuro_p;
	commit;
	
end if;


end gerar_prescr_atend_futuro;
/
