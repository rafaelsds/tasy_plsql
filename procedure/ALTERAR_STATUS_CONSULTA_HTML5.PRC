create or replace
procedure ALTERAR_STATUS_CONSULTA_HTML5(
		nr_sequencia_p		number,
		cd_motivo_p		number,
		ds_motivo_p		varchar2,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2) is

cd_agenda_w		number(10);
nr_seq_item_w		number(10);
nr_sequencia_ageint_w	number(10);
qt_nao_cancelados_w	number(1);
nr_seq_status_cancel_w	number(10);

begin
select	max(nr_sequencia),
	max(nr_seq_agenda_int)
into	nr_seq_item_w,
	nr_sequencia_ageint_w
from	agenda_integrada_item 
where nr_seq_agenda_cons = nr_sequencia_p;

if (nr_sequencia_ageint_w is null) then
begin
	select	max(cd_agenda)
	into	cd_agenda_w
	from	agenda_consulta
	where	nr_sequencia = nr_sequencia_p;
	
	alterar_status_agecons(cd_agenda_w, nr_sequencia_p, 'C', cd_motivo_p, ds_motivo_p, 'N', nm_usuario_p, null);
end;
else
begin
	ageint_cancelar_item(nr_sequencia_ageint_w, nr_seq_item_w, ds_motivo_p, cd_motivo_p, nm_usuario_p, cd_estabelecimento_p);
		
	select	count(1)
	into	qt_nao_cancelados_w
	from (	select	nr_sequencia
		from 	agenda_integrada_item
		where	nr_seq_agenda_int = nr_sequencia_ageint_w
		and		substr(ageint_obter_status_item(nr_seq_agenda_int,nr_sequencia,'C'),1,255) <> 'C' );
	
	if (qt_nao_cancelados_w = 0) then
		select	min(nr_sequencia)
		into	nr_seq_status_cancel_w
		from	agenda_integrada_status
		where	ie_situacao = 'A'
		and		ie_status_tasy = 'CA';
		
		if (nr_seq_status_cancel_w > 0) then
			begin
			ageint_alterar_status(nr_seq_status_cancel_w, nr_sequencia_ageint_w, ds_motivo_p, nm_usuario_p, cd_estabelecimento_p);
			end;
		end if;
	end if;
	
end;
end if;

end ALTERAR_STATUS_CONSULTA_HTML5;
/