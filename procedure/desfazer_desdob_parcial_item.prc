create or replace procedure desfazer_desdob_parcial_item	(
								nr_seq_ret_item_p in convenio_retorno_item.nr_sequencia%type
								) is
		
cursor 	itens is
select	nr_sequencia,
	ds_valores_originais
from	convenio_retorno_glosa
where	nr_seq_ret_item = nr_seq_ret_item_p
and	ds_valores_originais is not null;
		
ds_lista_valores	varchar2(2000);
ds_lista_valores_aux 	varchar2(2000);
ds_atributo     	varchar2(255);
ds_pos_pontovirgula   	number;
ds_pos_igual            number;
tamanho_lista       	number;
contador            	number;
inicio      		number;
i           		number;		

type	ret_item_glosa is record (
	VL_AMAIOR 		NUMBER,
	VL_ARECUPERAR 		NUMBER,
	VL_COBRADO 		NUMBER,
	VL_DESCONTO_ITEM 	NUMBER,
	VL_GLOSA 		NUMBER,
	VL_INFORMADO 		NUMBER,
	VL_LIBERADO 		NUMBER,
	VL_PAGO_DIGITADO 	NUMBER,
	VL_REAPRESENTACAO 	NUMBER,
	VL_REPASSE_ITEM 	NUMBER,
	VL_SALDO_REAP 		NUMBER
);

type	ret_item_glosa_tb is table of ret_item_glosa index by binary_integer;

ret_item_glosa_v ret_item_glosa_tb;

begin
	if	nvl(nr_seq_ret_item_p, 0) > 0 then
		for item in itens loop
		
			ds_lista_valores := item.ds_valores_originais;
			tamanho_lista := length(ds_lista_valores);
			inicio := 0;
			i := 0;
			contador := 1;
			ds_atributo := '';
			ds_pos_pontovirgula := 0;
			ds_pos_igual := 0;
			while contador <= tamanho_lista loop
				i := i + 1;
				if	instr(ds_lista_valores,';') = i then
					ds_pos_pontovirgula := i;
					ds_atributo := '';
				elsif   instr(ds_lista_valores,'=') = i then
				      ds_pos_igual := i;
				      ds_lista_valores := substr(ds_lista_valores,i + 1,tamanho_lista);
				      FOR j IN 1..length(ds_lista_valores) LOOP
					  if    instr(ds_lista_valores,';') = j then
						if  	(ds_atributo = 'VL_AMAIOR') then
						    ret_item_glosa_v(0).VL_AMAIOR := substr(ds_lista_valores,1,j-1); 
						elsif (ds_atributo = 'VL_ARECUPERAR') then
						    ret_item_glosa_v(0).VL_ARECUPERAR := substr(ds_lista_valores,1,j-1); 
						elsif (ds_atributo = 'VL_COBRADO') then
						    ret_item_glosa_v(0).VL_COBRADO := substr(ds_lista_valores,1,j-1);
						elsif (ds_atributo = 'VL_DESCONTO_ITEM') then
						    ret_item_glosa_v(0).VL_DESCONTO_ITEM := substr(ds_lista_valores,1,j-1);
						elsif (ds_atributo = 'VL_GLOSA') then
						    ret_item_glosa_v(0).VL_GLOSA := substr(ds_lista_valores,1,j-1);
						elsif (ds_atributo = 'VL_INFORMADO') then
						    ret_item_glosa_v(0).VL_INFORMADO := substr(ds_lista_valores,1,j-1);
						elsif (ds_atributo = 'VL_LIBERADO') then
						    ret_item_glosa_v(0).VL_LIBERADO := substr(ds_lista_valores,1,j-1);
						elsif (ds_atributo = 'VL_PAGO_DIGITADO') then
						    ret_item_glosa_v(0).VL_PAGO_DIGITADO := substr(ds_lista_valores,1,j-1);
						elsif (ds_atributo = 'VL_REAPRESENTACAO') then
						    ret_item_glosa_v(0).VL_REAPRESENTACAO := substr(ds_lista_valores,1,j-1);
						elsif (ds_atributo = 'VL_REPASSE_ITEM') then
						    ret_item_glosa_v(0).VL_REPASSE_ITEM := substr(ds_lista_valores,1,j-1);
						elsif (ds_atributo = 'VL_SALDO_REAP') then
						    ret_item_glosa_v(0).VL_SALDO_REAP := substr(ds_lista_valores,1,j-1);
						end if;
						ds_lista_valores := substr(ds_lista_valores,j+1,length(ds_lista_valores));
						ds_atributo := '';
						i := 0;
						exit;
					  end if;
				      END LOOP;
				else
				    ds_atributo :=  ds_atributo || substr(ds_lista_valores,i,1);        
				end if;
				contador := contador + 1;
			end loop;
			
			update	convenio_retorno_glosa
			set	vl_amaior 		= ret_item_glosa_v(0).vl_amaior,
				vl_arecuperar 		= ret_item_glosa_v(0).vl_arecuperar,
				vl_cobrado 		= ret_item_glosa_v(0).vl_cobrado,
				vl_desconto_item 	= ret_item_glosa_v(0).vl_desconto_item,
				vl_glosa		= ret_item_glosa_v(0).vl_glosa,
				vl_informado		= ret_item_glosa_v(0).vl_informado,
				vl_liberado		= ret_item_glosa_v(0).vl_liberado,
				vl_pago_digitado	= ret_item_glosa_v(0).vl_pago_digitado,
				vl_reapresentacao	= ret_item_glosa_v(0).vl_reapresentacao,
				vl_repasse_item		= ret_item_glosa_v(0).vl_repasse_item,
				vl_saldo_reap		= ret_item_glosa_v(0).vl_saldo_reap
			where	nr_sequencia = item.nr_sequencia;
			
		end loop;
	end if;
end desfazer_desdob_parcial_item;
/
