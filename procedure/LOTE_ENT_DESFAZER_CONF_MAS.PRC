create or replace
procedure lote_ent_desfazer_conf_mas(
					nr_seq_resultado_p		number,
					nr_seq_prescr_p			number,
					nr_sequencia_p			number) is
									
begin

delete	exame_lab_result_item_mas
where	nr_seq_prescr = nr_seq_prescr_p
and		nr_seq_resultado = nr_seq_resultado_p
and		nr_seq_result_item = nr_sequencia_p;

commit;

end lote_ent_desfazer_conf_mas;
/
