create or replace procedure cancelar_parcela_emprestimo(nr_seq_parcela_p	number,
							nm_usuario_p		varchar2) is
							
nr_titulo_w		emprest_financ_parc.nr_titulo%type;
ie_situacao_w		titulo_pagar.ie_situacao%type;

BEGIN
select	max(nr_titulo)
into	nr_titulo_w
from	emprest_financ_parc
where	nr_sequencia = nr_seq_parcela_p;

if (nr_titulo_w is null) then
	-- Parcela n�o possui t�tulo e n�o pode ser cancelada.
	wheb_mensagem_pck.exibir_mensagem_abort(369557);
else
	select	max(ie_situacao)
	into	ie_situacao_w
	from	titulo_pagar
	where	nr_titulo = nr_titulo_w;
	
	if (nvl(ie_situacao_w,'A') <> 'C') then
		-- T�tulo deve estar cancelado para cancelar a parcela.
		wheb_mensagem_pck.exibir_mensagem_abort(369558);
	else
		/* Cancela a parcela atual e gera nova parcela com os mesmos dados da parcela cancelada */
		update	emprest_financ_parc
		set	dt_cancelamento = sysdate,
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_seq_parcela_p;
		
		insert into emprest_financ_parc
			(nr_sequencia,
			nr_parcela,
			nr_seq_contrato,
			cd_estabelecimento,
			nm_usuario,
			dt_atualizacao,
			nr_titulo,
			dt_vencimento,
			vl_parcela,
			vl_juros,
			vl_amortizacao,
			vl_saldo_dev,
			vl_saldo_corrigido)
		select	emprest_financ_parc_seq.nextval,
			nr_parcela,
			nr_seq_contrato,
			cd_estabelecimento,
			nm_usuario_p,
			sysdate,
			null,
			dt_vencimento,
			vl_parcela,
			vl_juros,
			vl_amortizacao,
			vl_saldo_dev,
			vl_saldo_corrigido
		from	emprest_financ_parc
		where	nr_sequencia = nr_seq_parcela_p;
		
		commit;
	end if;
end if;

END cancelar_parcela_emprestimo;
/
