create or replace procedure	avisar_liberacao_ordem_compra(
			nr_ordem_compra_p		number,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is

/* Dados da regra */
nr_seq_regra_w			number(10);
ds_email_adicional_w		varchar2(2000);
cd_perfil_dispara_w		number(5);
ie_momento_envio_w		varchar2(1);
ie_usuario_w			varchar2(1);
ds_email_remetente_w		varchar2(255);
ds_mensagem_w			varchar2(4000);
ds_assunto_w			varchar2(255);

qt_existe_w			number(10);
nr_solic_compra_w			number(10);
ds_lista_solic_w			varchar2(2000);
nr_cot_compra_w			number(10);
ds_lista_cotacao_w		varchar2(2000);

cd_pessoa_solicitante_w		varchar2(10);
ds_email_w			varchar2(255);
ds_destinatarios_w			varchar2(2000);

nm_fornecedor_w			varchar2(255);
dt_entrega_w			date;	
cd_comprador_w			number(10);
nm_comprador_w			varchar2(255);
vl_ordem_compra_w		varchar2(255);

ds_email_origem_w			varchar2(255);
nm_usuario_origem_w		varchar2(255);
nm_solicitante_w		varchar2(255);

ds_item_w				varchar2(2000);
ds_descricao_compl_w	varchar2(2000);
ds_estrutura_w			varchar2(2000);
ds_item_ww				varchar2(2000);
ds_descricao_compl_ww	varchar2(2000);
ds_estrutura_ww			varchar2(2000);

cursor	c01 is
select	distinct
		nr_solic_compra
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p;

cursor	c02 is
select	distinct
	nr_cot_compra
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p;

cursor	c03 is
select	distinct
	cd_pessoa_solicitante
from	solic_compra
where	nr_solic_compra in	(select	nr_solic_compra
			from	ordem_compra_item
			where	nr_ordem_compra = nr_ordem_compra_p);
		
Cursor c04 is
		select	substr(a.cd_material || ' - ' || obter_desc_material(a.cd_material),1,2000) ds_material,
				substr(decode(a.ds_material_direto,'',a.cd_material || ' - ' || obter_desc_material(a.cd_material),a.cd_material || ' - ' || obter_desc_material(a.cd_material) || ' - ' ||  a.DS_MATERIAL_DIRETO),1,2000) ds_material_compl,
				substr(wheb_mensagem_pck.get_texto(299020, 	'CD_MATERIAL=' || a.cd_material || ';DS_MATERIAL=' || e.ds_material || 
						';CD_GRUPO_MATERIAL=' || e.cd_grupo_material || ';DS_GRUPO_MATERIAL=' || e.ds_grupo_material || 
						';CD_SUBGRUPO_MATERIAL=' || e.cd_subgrupo_material || ';DS_SUBGRUPO_MATERIAL=' || e.ds_subgrupo_material || 
						';CD_CLASSE_MATERIAL=' || e.cd_classe_material || ';DS_CLASSE_MATERIAL=' || e.ds_classe_material),1,2000) ds_estrutura
		from	Ordem_compra_item a,
				estrutura_material_v e
		where	a.cd_material = e.cd_material
		and		nr_ordem_compra = nr_ordem_compra_p;	

		
begin

select	nvl(max(nr_sequencia),0),
	nvl(max(ds_email_remetente),'X'),
	max(replace(ds_email_adicional,',',';')),
	max(cd_perfil_disparar),
	nvl(max(ie_momento_envio),'I'),
	max(ds_assunto),
	max(ds_mensagem_padrao)
into	nr_seq_regra_w,
	ds_email_remetente_w,
	ds_email_adicional_w,
	cd_perfil_dispara_w,
	ie_momento_envio_w,
	ds_assunto_w,
	ds_mensagem_w
from	regra_envio_email_compra
where	ie_tipo_mensagem = 84
and	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p
and	obter_se_envia_email_regra(nr_ordem_compra_p, 'OC', ie_tipo_mensagem, cd_estabelecimento) = 'S';

select	count(*)
into	qt_existe_w
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p
and	nr_solic_compra is not null;

if	(qt_existe_w > 0) and
	(nr_seq_regra_w > 0) and
	((cd_perfil_dispara_w is null) or
	((cd_perfil_dispara_w is not null) and (cd_perfil_dispara_w = obter_perfil_ativo))) then
	begin

	open c01;
	loop
	fetch c01 into
		nr_solic_compra_w;
	exit when c01%notfound;
		begin

		ds_lista_solic_w	:= ds_lista_solic_w || ', ' || to_char(nr_solic_compra_w);

		end;
	end loop;
	close c01;

	ds_lista_solic_w	:= substr(ds_lista_solic_w,3,length(ds_lista_solic_w));

	open c02;
	loop
	fetch c02 into
		nr_cot_compra_w;
	exit when c02%notfound;
		begin

		ds_lista_cotacao_w	:= ds_lista_cotacao_w || ', ' || to_char(nr_cot_compra_w);

		end;
	end loop;
	close c02;

	ds_lista_cotacao_w	:= substr(ds_lista_cotacao_w,3,length(ds_lista_cotacao_w));

	open c03;
	loop
	fetch c03 into
		cd_pessoa_solicitante_w;
	exit when c03%notfound;
		begin

		select	max (a.ds_email)
		into	ds_email_w
		from	usuario a
		where	a.cd_pessoa_fisica = cd_pessoa_solicitante_w
		and	a.ds_email is not null
		and	ie_situacao = 'A';

		if	(ds_email_w is not null) then
			ds_destinatarios_w	:= ds_destinatarios_w || ds_email_w || ';';
		end if;
		
		end;
	end loop;
	close c03;

	select	substr(obter_nome_pf_pj(cd_pessoa_solicitante_w,null),1,255),
		substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc_fornecedor),1,255),
		dt_entrega,
		cd_comprador,
		substr(sup_obter_nome_comprador(cd_estabelecimento,cd_comprador),1,255),
		substr(campo_mascara_virgula(nvl(obter_valor_liquido_ordem(nr_ordem_compra),0)),1,255)
	into	nm_solicitante_w,
		nm_fornecedor_w,
		dt_entrega_w,
		cd_comprador_w,
		nm_comprador_w,
		vl_ordem_compra_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_p;
	
	open c04;
	loop
	fetch c04 into
		ds_item_w,
		ds_descricao_compl_w,
		ds_estrutura_w;
	exit when c04%notfound;
		begin
		
		ds_item_ww				:= substr(ds_item_ww 		|| ds_item_w || chr(13),1,2000);
		ds_descricao_compl_ww	:= substr(ds_descricao_compl_ww	|| ds_descricao_compl_w || chr(13),1,2000);
		ds_estrutura_ww			:= substr(ds_estrutura_ww 	|| ds_estrutura_w || chr(13),1,2000);
		
		end;
	end loop;
	close c04;
	
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ordem_compra',nr_ordem_compra_p),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@solicitante',nm_solicitante_w),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@lista_solicitacao',ds_lista_solic_w),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@lista_cotacao',ds_lista_cotacao_w),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@fornecedor',nm_fornecedor_w),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@data_entrega',PKG_DATE_FORMATERS.TO_VARCHAR(dt_entrega_w, 'shortDate', cd_estabelecimento_p, nm_usuario_p)),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@comprador',nm_comprador_w),1,255);
	ds_assunto_w := substr(replace_macro(ds_assunto_w,'@valor_ordem',vl_ordem_compra_w),1,255);
	
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@ordem_compra',nr_ordem_compra_p),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@solicitante',nm_solicitante_w),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@lista_solicitacao',ds_lista_solic_w),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@lista_cotacao',ds_lista_cotacao_w),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@fornecedor',nm_fornecedor_w),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@data_entrega',PKG_DATE_FORMATERS.TO_VARCHAR(dt_entrega_w, 'shortDate', cd_estabelecimento_p, nm_usuario_p)),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@comprador',nm_comprador_w),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@valor_ordem',vl_ordem_compra_w),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@materiais',ds_item_ww),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@mat_complementar',ds_descricao_compl_ww),1,4000);
	ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@mat_estrutura',ds_estrutura_ww),1,4000);

	select	nvl(max(ie_usuario),'U')
	into	ie_usuario_w
	from	regra_envio_email_compra
	where	nr_sequencia = nr_seq_regra_w;

	if	(ie_usuario_w = 'U') or
		((nvl(cd_comprador_w,'0') <> 0) and (ie_usuario_w = 'O')) then --Usuario
		begin

		select	ds_email,
			nm_usuario
		into	ds_email_origem_w,
			nm_usuario_origem_w
		from	usuario
		where	nm_usuario = nm_usuario_p;

		end;
	elsif	(ie_usuario_w = 'C') then --Setor compras
		begin

		select	ds_email
		into	ds_email_origem_w
		from	parametro_compras
		where	cd_estabelecimento = cd_estabelecimento_p;

		select	nvl(ds_fantasia,ds_razao_social)
		into	nm_usuario_origem_w
		from	estabelecimento_v
		where	cd_estabelecimento = cd_estabelecimento_p;

		end;
	elsif	(ie_usuario_w = 'O') then --Comprador
		begin

		select	max(ds_email),
			max(nm_guerra)
		into	ds_email_origem_w,
			nm_usuario_origem_w
		from	comprador
		where	cd_pessoa_fisica = cd_comprador_w
		and	cd_estabelecimento = cd_estabelecimento_p;

		end;
	end if;

	if	(ds_email_remetente_w <> 'X') then
		ds_email_origem_w	:= ds_email_remetente_w;
	end if;

	if	(ds_email_adicional_w is not null) then
		ds_destinatarios_w	:= ds_destinatarios_w || ds_email_adicional_w;
	end if;
	
	if	(substr(ds_destinatarios_w,1,length(ds_destinatarios_w)) = ';') then
		ds_destinatarios_w	:= substr(ds_destinatarios_w,1,length(ds_destinatarios_w)-1);
	end if;
	
	if	(ie_momento_envio_w = 'A') then
		begin

		sup_grava_envio_email(
			'OC',
			'84',
			nr_ordem_compra_p,
			null,
			null,
			ds_destinatarios_w,
			nm_usuario_origem_w,
			ds_email_origem_w,
			ds_assunto_w,
			ds_mensagem_w,
			cd_estabelecimento_p,
			nm_usuario_p);

		end;
	else
		begin
		
		enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_origem_w,ds_destinatarios_w,nm_usuario_origem_w,'M');
		
		exception
		when others then
			nr_seq_regra_w := nr_seq_regra_w;
		end;
	end if;

	end;
end if;

end avisar_liberacao_ordem_compra;
/