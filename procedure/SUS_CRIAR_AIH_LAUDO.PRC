create or replace 
procedure sus_criar_aih_laudo(
			dt_retorno_secr_p		in date,
			nr_aih_p			in number,
			nr_seq_lote_p			in number default null,
			cd_pessoa_fisica_p		in varchar2 default null,
			nm_usuario_p			in varchar2,
			ds_retorno_p			out varchar2,
			nr_seq_interno_p		in number default null,
			nr_cartao_nac_sus_p		in number default null,
			cd_procedimento_solic_p		in number default null,
			nr_prontuario_p			in number default null,
			nm_mae_paciente_p		in varchar2 default null,
			nm_paciente_p			in varchar2 default null,
			dt_nasc_paciente_p		in varchar2 default null,
                        nr_prot_sisreg_p                in varchar2 default null,
                        dt_emissao_p                    in varchar2 default null) is


nr_seq_interno_w		sus_laudo_paciente.nr_seq_interno%type;
nr_atendimento_w		sus_laudo_paciente.nr_atendimento%type;
dt_retorno_secr_w		sus_laudo_paciente.dt_retorno_secr%type;
ie_carater_inter_sus_w	        atendimento_paciente.ie_carater_inter_sus%type;
ds_retorno_w		        varchar(80);
cd_pessoa_fisica_w	        pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_lote_w		        sus_laudo_paciente.nr_seq_lote%type;
nr_sequencia_w		        sus_log_retorno_laudo.nr_sequencia%type;
dt_atualizacao_log_w	        sus_log_retorno_laudo.dt_atualizacao%type;
dt_nasc_paciente_w	        pessoa_fisica.dt_nascimento%type;
dt_emissao_w    	        sus_laudo_paciente.dt_emissao%type;
qt_laudos_w                     number(10);

cursor c01 is
	select	b.nr_seq_interno,
                b.nr_seq_lote,
                a.nr_atendimento,
                b.dt_retorno_secr
	from	atendimento_paciente a,
                sus_laudo_paciente b,
                sus_lote_autor c
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
        and     a.nr_atendimento = b.nr_atendimento
        and     b.nr_seq_lote = c.nr_sequencia
        and     b.dt_liberacao is not null
        and     b.ie_classificacao  = 1        
        and	c.dt_envio is not null
        and	b.cd_procedimento_solic = cd_procedimento_solic_p;

begin

if      (coalesce(dt_nasc_paciente_p, 'X') <> 'X')then
        begin
        dt_nasc_paciente_w := to_date(dt_nasc_paciente_p, pkg_date_formaters.localize_mask('shortDate', pkg_date_formaters.getUserLanguageTag(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p)));
        exception
        when others then
                dt_nasc_paciente_w := null;
        end;
end if;

if      (coalesce(dt_emissao_p, 'X') <> 'X')then
        begin
        dt_emissao_w := to_date(dt_emissao_p, pkg_date_formaters.localize_mask('shortDate', pkg_date_formaters.getUserLanguageTag(WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p)));
        exception
        when others then
                dt_emissao_w := null;
        end;
end if;

ds_retorno_w:=	wheb_mensagem_pck.Get_texto(1088541);
dt_atualizacao_log_w := dt_retorno_secr_p; --Data de iniciacao da Procedure, sera utilizado para apresentar o WCPanel de LOGs

if	(((coalesce(nr_cartao_nac_sus_p, 0) <> 0) or
        (coalesce(nr_prot_sisreg_p,'0') <> '0')) and 
        (coalesce(nr_seq_interno_p, 0) = 0)) then
	begin
        
        if      (coalesce(nr_prot_sisreg_p,'0') <> '0') then
                begin
                
                nr_seq_interno_w := 0;                
                
                select  count(a.nr_seq_interno)
                into    qt_laudos_w
                from    sus_laudo_paciente a
                where   a.nr_protocolo_sisreg = nr_prot_sisreg_p
                and     a.dt_liberacao is not null
                and     a.dt_retorno_secr is null
                and     a.nr_seq_lote is not null;
                                
                if      (qt_laudos_w = 1) then
                        begin
                        
                        begin
                        select  a.nr_seq_interno,
                                a.nr_seq_lote,
                                b.cd_pessoa_fisica
                        into    nr_seq_interno_w,
                                nr_seq_lote_w,
                                cd_pessoa_fisica_w
                        from    sus_laudo_paciente a,
                                atendimento_paciente b
                        where   a.nr_protocolo_sisreg = nr_prot_sisreg_p
                        and     a.dt_liberacao is not null
                        and     a.dt_retorno_secr is null
                        and     a.nr_seq_lote is not null
                        and     a.nr_atendimento = b.nr_atendimento;
                        exception
                                when others then
                                nr_seq_interno_w := 0;
                        end;
                                        
                        end;
                elsif   (qt_laudos_w > 1) then
                        begin
                        
                        begin
                        select  a.nr_seq_interno,
                                a.nr_seq_lote,
                                b.cd_pessoa_fisica
                        into    nr_seq_interno_w,
                                nr_seq_lote_w,
                                cd_pessoa_fisica_w
                        from    sus_laudo_paciente a,
                                atendimento_paciente b
                        where   a.nr_protocolo_sisreg = nr_prot_sisreg_p
                        and     trunc(a.dt_emissao) =  dt_emissao_w
                        and     a.dt_liberacao is not null
                        and     a.dt_retorno_secr is null
                        and     a.nr_seq_lote is not null
                        and     a.nr_atendimento = b.nr_atendimento;
                        exception
                                when others then
                                nr_seq_interno_w := 0;
                        end;
                        
                        end;
                end if;
                
                if	((coalesce(nr_seq_lote_w, 0) <> 0) and
                        (nr_seq_interno_w > 0)) then	
                        begin
                        dt_retorno_secr_w       := dt_retorno_secr_p;
                        update	sus_laudo_paciente
                        set	dt_retorno_secr		= 	dt_retorno_secr_p,
                                nr_aih			=	nr_aih_p,
                                dt_atualizacao		=	sysdate,
                                nm_usuario		=	nm_usuario_p
                        where	nr_seq_interno		=	nr_seq_interno_w
                        and	nr_seq_lote		=	nr_seq_lote_w;
                        
                        sus_gerar_aih_lote(null,nr_aih_p,cd_pessoa_fisica_w,nr_seq_interno_w,nm_usuario_p);
                        
                        end;
                end if;
                
                end;
        end if;
        
        if      ((coalesce(nr_prot_sisreg_p,'0') = '0') or
                (coalesce(nr_seq_interno_w,0) = 0)) then        
                begin
                
                begin
                select  max(cd_pessoa_fisica)
                into	cd_pessoa_fisica_w
                from 	pessoa_fisica
                where 	nr_cartao_nac_sus = nr_cartao_nac_sus_p;
                exception
                        when others then
                        cd_pessoa_fisica_w := null;
                end;
        
                if	((coalesce(nr_prontuario_p, 0) <> 0) and 
                        (coalesce(cd_pessoa_fisica_w, 0) = 0)) then
                        begin
                        select  max(cd_pessoa_fisica)
                        into	cd_pessoa_fisica_w
                        from 	pessoa_fisica
                        where 	nr_prontuario = nr_prontuario_p;
                        exception
                                when others then
                                cd_pessoa_fisica_w := null;
                        end;
                end if;
        
                if	((coalesce(cd_pessoa_fisica_w, '0') = '0') and 
                        (coalesce(nm_mae_paciente_p, 'X') <> 'X') and 
                        (coalesce(nm_paciente_p, 'X') <> 'X') and 
                        (coalesce(dt_nasc_paciente_p, 'X') <> 'X')) then
                        begin
                        
                        begin                        
                        select  max(a.cd_pessoa_fisica)
                        into	cd_pessoa_fisica_w
                        from 	pessoa_fisica a,
                                compl_pessoa_fisica b
                        where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
                        and 	b.ie_tipo_complemento = 5
                        and 	UPPER(b.nm_contato) = UPPER(nm_mae_paciente_p)
                        and	UPPER(nm_pessoa_fisica) = UPPER(nm_paciente_p)
                        and	a.dt_nascimento = dt_nasc_paciente_w;
                        exception
                                when others then
                                cd_pessoa_fisica_w := null;
                        end;
                        
                        end;
                end if;
        
                if	(coalesce(cd_pessoa_fisica_w, '0') <> '0') then
                        begin
                        
                        for c01_w in c01 loop
                                begin
                                
                                nr_seq_interno_w        := c01_w.nr_seq_interno;
                                nr_seq_lote_w           := c01_w.nr_seq_lote;
                                nr_atendimento_w        := c01_w.nr_atendimento;
                                dt_retorno_secr_w       := c01_w.dt_retorno_secr;
                                
                                if	(coalesce(nr_seq_lote_w, 0) > 0) and
                                        (coalesce(nr_seq_interno_w,0) > 0) and
                                        (dt_retorno_secr_w is null) then	
                                        begin
                                        update	sus_laudo_paciente
                                        set	dt_retorno_secr		= 	dt_retorno_secr_p,
                                                nr_aih			=	nr_aih_p,
                                                dt_atualizacao		=	sysdate,
                                                nm_usuario		=	nm_usuario_p
                                        where	nr_seq_interno		=	nr_seq_interno_w
                                        and	nr_seq_lote		=	nr_seq_lote_w;
                                        
                                        sus_gerar_aih_lote(null,nr_aih_p,cd_pessoa_fisica_w,nr_seq_interno_w,nm_usuario_p);
                                                        
                                        end;
                                elsif  (dt_retorno_secr_w is not null) then
                                        begin                                       
                                        
                                        select	sus_log_retorno_laudo_seq.nextval
                                        into	nr_sequencia_w
                                        from	dual;
        
                                        insert into sus_log_retorno_laudo (nr_sequencia, --1
                                                                        dt_atualizacao, --2
                                                                        dt_atualizacao_nrec, --3
                                                                        nm_usuario, --4
                                                                        nm_usuario_nrec, --5
                                                                        ds_informacao_log, --6
                                                                        nr_prontuario_pac, --7
                                                                        nr_cns_paciente,   --8
                                                                        dt_nascimento_pac, --9
                                                                        nm_mae_paciente,  --10
                                                                        nm_paciente, --11
                                                                        nr_atendimento, --12
                                                                        cd_procedimento_solic) --13 
                                                                values (nr_sequencia_w, --1
                                                                        dt_atualizacao_log_w, --2
                                                                        dt_atualizacao_log_w, --3
                                                                        nm_usuario_p, --4
                                                                        nm_usuario_p, --5
                                                                        wheb_mensagem_pck.Get_texto(1088633), --6
                                                                        nr_prontuario_p, --7
                                                                        nr_cartao_nac_sus_p, --8
                                                                        dt_nasc_paciente_w, --9
                                                                        nm_mae_paciente_p, --10
                                                                        nm_paciente_p, --11
                                                                        nr_atendimento_w, --12
                                                                        cd_procedimento_solic_p); --13
                                        end;
                                end if;
                                dt_retorno_secr_w := null;
                                end;                                
                        end loop;
                        
                        if      (coalesce(nr_seq_interno_w,0) = 0) then
                                begin
                                select	sus_log_retorno_laudo_seq.nextval
                                into	nr_sequencia_w
                                from	dual;
                
                                insert into sus_log_retorno_laudo (nr_sequencia, --1
                                                                dt_atualizacao, --2
                                                                dt_atualizacao_nrec, --3 
                                                                nm_usuario, --4
                                                                nm_usuario_nrec, --5
                                                                ds_informacao_log, --6
                                                                nr_prontuario_pac, --7
                                                                nr_cns_paciente, --8
                                                                dt_nascimento_pac, --9
                                                                nm_mae_paciente, --10
                                                                nm_paciente) --11
                                                        values (nr_sequencia_w, --1
                                                                dt_atualizacao_log_w, --2
                                                                dt_atualizacao_log_w, --3
                                                                nm_usuario_p,--4
                                                                nm_usuario_p, --5
                                                                wheb_mensagem_pck.Get_texto(1088615), --6
                                                                nr_prontuario_p, --7
                                                                nr_cartao_nac_sus_p, --8
                                                                dt_nasc_paciente_w, --9
                                                                nm_mae_paciente_p, --10
                                                                nm_paciente_p); --11
                
                                end;
                        end if;
                        
                        end;
                elsif   (dt_retorno_secr_w is null) then
                        begin
        
                        select	sus_log_retorno_laudo_seq.nextval
                        into	nr_sequencia_w
                        from	dual;
        
                        insert into sus_log_retorno_laudo (nr_sequencia, --1
                                                        dt_atualizacao, --2
                                                        dt_atualizacao_nrec, --3 
                                                        nm_usuario, --4
                                                        nm_usuario_nrec, --5
                                                        ds_informacao_log, --6
                                                        nr_prontuario_pac, --7
                                                        nr_cns_paciente, --8
                                                        dt_nascimento_pac, --9
                                                        nm_mae_paciente, --10
                                                        nm_paciente) --11
                                                values (nr_sequencia_w, --1
                                                        dt_atualizacao_log_w, --2
                                                        dt_atualizacao_log_w, --3
                                                        nm_usuario_p,--4
                                                        nm_usuario_p, --5
                                                        wheb_mensagem_pck.Get_texto(1088615), --6
                                                        nr_prontuario_p, --7
                                                        nr_cartao_nac_sus_p, --8
                                                        dt_nasc_paciente_w, --9
                                                        nm_mae_paciente_p, --10
                                                        nm_paciente_p); --11
        
                        end;
                end if;
                
                end;
        end if;

	end;
else
        begin
        
        begin
        select	p.nr_seq_interno,
                b.nr_atendimento,
                b.dt_entrada,
                b.ie_carater_inter_sus,
                p.dt_retorno_secr
        into	nr_seq_interno_w,
                nr_atendimento_w,
                dt_emissao_w,
                ie_carater_inter_sus_w,
                dt_retorno_secr_w
        from	atendimento_paciente	b,
                sus_laudo_paciente 	p
        where	b.nr_atendimento		=	p.nr_atendimento
        and	((b.cd_pessoa_fisica	=	cd_pessoa_fisica_p) or (coalesce(cd_pessoa_fisica_p,'0') = '0'))
        and	p.nr_seq_lote 		=	nr_seq_lote_p
        and	((p.nr_seq_interno		=	nr_seq_interno_p) or (coalesce(nr_seq_interno_p,0) = 0));
        exception
                when others then
                        nr_seq_interno_w:=	null;
        end;

        end;
end if;

if	(dt_retorno_secr_w	is null) and
        (coalesce(nr_seq_interno_w,0) > 0) and
        (coalesce(nr_seq_lote_p,0) > 0) then
	begin
	update	sus_laudo_paciente
	set	dt_retorno_secr		= 	dt_retorno_secr_p,
		nr_aih			=	nr_aih_p,
		dt_atualizacao		=	sysdate,
		nm_usuario		=	nm_usuario_p
	where	nr_seq_interno		=	nr_seq_interno_w
	and	nr_seq_lote		=	nr_seq_lote_p;

	commit;
        
        sus_gerar_aih_lote(null,nr_aih_p,cd_pessoa_fisica_p,nr_seq_interno_w,nm_usuario_p);
	end;
else
	ds_retorno_w:=	wheb_mensagem_pck.Get_texto(1088542);
end if;

ds_retorno_p:=	ds_retorno_w;

commit;

end sus_criar_aih_laudo;
/
