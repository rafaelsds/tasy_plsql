create or replace
procedure pls_atuali_motivo_reativ_atend(
		ds_motivo_p		varchar2,
		nr_seq_segurado_susp_p	number) is 

begin

update 	pls_segurado_suspensao
set    	ds_motivo_fim_susp 	= ds_motivo_p 
where  	nr_sequencia 		= nr_seq_segurado_susp_p;

commit;

end pls_atuali_motivo_reativ_atend;
/