create or replace
procedure atualizar_mov_iniciar_cir(	nr_atendimento_p		number,
				nr_cirurgia_p		number,
				cd_estabelecimento_p	number,
				nr_seq_interno_p		number)
				is
				
qt_interno_w			number(10,0);
nr_seq_evento_cirurgia_w		number(10,0);
ie_momento_integracao_TM_w	varchar2(10);
nr_seq_aten_pac_unid_w		number(10,0);

begin

	select 	max(nr_sequencia)
	into	nr_seq_evento_cirurgia_w
	from	evento_cirurgia_paciente
	where	(nr_cirurgia =  nr_cirurgia_p or (nr_seq_pepo is not null and nr_seq_pepo = (select max(b.nr_seq_pepo) from cirurgia b where b.nr_cirurgia = nr_cirurgia_p)))
	and		nr_seq_aten_pac_unid  is not null
	and 	nr_seq_evento in (select	nr_sequencia
				from  	evento_cirurgia
				where 	nvl(ie_gera_movimentacao,'N') = 'S'
				and	nvl(ie_inicia_integracao,'N') = 'S' 
				and	nvl(ie_situacao,'A') = 'A')
	and	nvl(ie_situacao,'A') = 'A';
		
	select	decode(nvl(count(1),0), 0,'N','S')
	into	ie_momento_integracao_TM_w
	from	parametros_pepo
	where	ie_momento_integracao = 'TM'
	and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;
	
	
	if ((nvl(nr_seq_evento_cirurgia_w,0) > 0) 
		and (nvl(ie_momento_integracao_TM_w,'N') = 'S')) then
		
		select	count(*)
		into	qt_interno_w
		from	atend_paciente_unidade a
		where	a.nr_seq_interno = nr_seq_interno_p
		and		exists (	select	1
							from	atend_paciente_unidade b
							where	a.nr_atendimento     = b.nr_atendimento
							AND		a.cd_unidade_basica = b.cd_unidade_basica
							AND		a.cd_unidade_compl	= b.cd_unidade_compl
							and		a.dt_entrada_unidade <> b.dt_entrada_unidade
							and 	(b.dt_saida_unidade is null or b.dt_saida_unidade = a.dt_entrada_unidade));
					
		if	(nvl(qt_interno_w,0) > 0) then
		
			update	atend_paciente_unidade
			set		ds_observacao = substr(obter_desc_expressao(797522),1,2000)
			where	nr_seq_interno = nr_seq_interno_p
			and		dt_saida_unidade is null;
			commit;
		end if;
	end if;

end atualizar_mov_iniciar_cir;
/