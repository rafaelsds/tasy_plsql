create or replace
procedure gerar_ordem_agecol_usuario(nm_usuario_p		Varchar2) is 

cd_agenda_w	number(10,0);
nr_seq_apres_w	number(10,0);

Cursor C01 is
	select	cd_agenda
	from	agenda_cons_col_ordem
	where	nm_usuario = nm_usuario_p
	order by vl_left;

begin

delete	med_ordem_agenda
where	nm_usuario_nrec = nm_usuario_p;

nr_seq_apres_w	:= 1;

open C01;
loop
fetch C01 into	
	cd_agenda_w;
exit when C01%notfound;
	begin
	insert into med_ordem_agenda	(
				nr_sequencia,
				cd_agenda,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_apres
				)
		values		(
				med_ordem_agenda_seq.nextval,
				cd_agenda_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_apres_w
				);
	nr_seq_apres_w := nr_seq_apres_w + 1;
	
	end;
end loop;
close C01;

delete	agenda_cons_col_ordem
where	nm_usuario = nm_usuario_p;
				
commit;

end gerar_ordem_agecol_usuario;
/