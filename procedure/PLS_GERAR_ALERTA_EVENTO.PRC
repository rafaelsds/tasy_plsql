create or replace
procedure pls_gerar_alerta_evento
			(	ie_tipo_evento_p		Number,
				nr_seq_auditoria_p		Number,
				nr_seq_requisicao_p		number,
				ds_parametro_1_p		varchar2,
				nm_usuario_p			Varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar alerta de evento.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:Performance.
-------------------------------------------------------------------------------------------------------------------
Referências:
	PLS_ATUALIZA_ESTAGIO_REQ
	PLS_LIBERAR_AUDITORIA
	PLS_LIBERAR_AUDITORIA_MASTER
	PTU_IMP_RESP_AUDITORIA_V40
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

if	(ie_tipo_evento_p = 1) then -- Encerramento da análise de autorização 
	pls_gerar_alerta_analise_aut(ie_tipo_evento_p, nr_seq_auditoria_p,nm_usuario_p); 
elsif	(ie_tipo_evento_p = 2) then -- Recebimento da análise pelo grupo auditor
	pls_gerar_alerta_auditor(ie_tipo_evento_p, nr_seq_auditoria_p,nm_usuario_p); 
elsif	(ie_tipo_evento_p = 3) then -- Solicitação de requisição externa X Home Care
	pls_gerar_alerta_home_care(ie_tipo_evento_p, nr_seq_requisicao_p,nm_usuario_p); 
elsif	(ie_tipo_evento_p = 8) then --Consistência da requisição
	pls_gerar_evento_consist_req(ie_tipo_evento_p, nr_seq_requisicao_p, nm_usuario_p);
end if;

end pls_gerar_alerta_evento;
/
