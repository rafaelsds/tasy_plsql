create or replace
procedure plt_sincronizar_jejum (
					nm_usuario_p			varchar2,
					nr_atendimento_p		number,
					dt_validade_limite_p		date,					
					dt_horario_p			date,
					nr_horario_p			integer,
					ie_prescr_usuario_p		varchar2) is
				
ds_sep_bv_w			varchar2(50);
nr_prescricao_w			number(14,0);
cd_jejum_w			varchar2(255);
ds_jejum_w			varchar2(255);
ds_vigencia_w			varchar2(255);
ds_comando_update_w		varchar2(4000);
ie_lib_pend_rep_w		varchar2(1);

cursor c01 is	
select	a.nr_prescricao,
	to_char(c.dt_inicio,'dd/mm/yyyy hh24:mi:ss') || '-' || to_char(c.dt_fim,'dd/mm/yyyy hh24:mi:ss') cd_jejum,
	x.ds_tipo ds_jejum,
	obter_desc_expressao(301700) ||': ' || to_char(c.dt_inicio,'dd/mm/yyyy hh24:mi:ss') || obter_desc_expressao(345996) || to_char(c.dt_fim,'dd/mm/yyyy hh24:mi:ss') ds_vigencia,
	substr(plt_obter_lib_pend_prescr(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1)
from	rep_tipo_jejum x,
	rep_jejum c,
	prescr_medica a
where	x.nr_sequencia = c.nr_seq_tipo
and	c.nr_prescricao = a.nr_prescricao
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	c.dt_inicio = dt_horario_p
and	nvl(a.ie_adep,'S') = 'S'
and	(((a.dt_liberacao_medico is not null) and
	  (ie_prescr_usuario_p = 'N')) or
	(a.nm_usuario_original = nm_usuario_p))
group by
	a.nr_prescricao,
	c.dt_inicio,
	c.dt_fim,
	x.ds_tipo,
	a.dt_liberacao_medico,
	a.dt_liberacao,
	a.dt_liberacao_farmacia;	
	
begin
ds_sep_bv_w := obter_separador_bv;
open c01;
loop
fetch c01 into	nr_prescricao_w,
		cd_jejum_w,
		ds_jejum_w,
		ds_vigencia_w,
		ie_lib_pend_rep_w;
exit when c01%notfound;
	begin
	ds_comando_update_w	:=	' update w_rep_t ' ||
					' set nr_prescricoes = adep_juntar_prescricao(nr_prescricoes,:nr_prescricao) ' ||
					' where nm_usuario = :nm_usuario ' ||
					' and ie_tipo_item = :ie_tipo ' ||
					' and ie_pendente_liberacao = :ie_pendente_liberacao ' ||					
					' and cd_item = :cd_item ' ||
					' and ((ds_prescricao = :ds_vigencia) or (ds_prescricao is null)) ';
				
	exec_sql_dinamico_bv('PLT', ds_comando_update_w,	'nr_prescricao=' || to_char(nr_prescricao_w) || ds_sep_bv_w || 
								'nm_usuario=' || nm_usuario_p || ds_sep_bv_w || 
								'ie_tipo=J' || ds_sep_bv_w || 
								'ie_pendente_liberacao=' || ie_lib_pend_rep_w || ds_sep_bv_w ||
								'cd_item=' || cd_jejum_w || ds_sep_bv_w ||
								'ds_vigencia=' || ds_vigencia_w || ds_sep_bv_w );
	end;
end loop;
close c01;
end plt_sincronizar_jejum;
/
