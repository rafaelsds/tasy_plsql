create or replace 
procedure ctb_intreceita_agrupado (	nr_lote_contabil_p  number,
                					cd_estabelecimento_p  number,
        							nm_usuario_p    Varchar2) is

cd_empresa_w    	estabelecimento.cd_empresa%type;
dt_referencia_w		lote_contabil.dt_referencia%type;
nr_contador_w   	number(10) := 0;
qt_registros_w    	number(10);
tgpr_sqprocesso_w	number(10);
cd_centro_custo_w	movimento_contabil.cd_centro_custo%type;
cd_classif_centro_w centro_custo.cd_classificacao%type;

cursor c01 is
	select	cd_conta_contabil,
			cd_centro_custo,
			ie_debito_credito,
			ds_compl_historico,
			vl_segunda_moeda,
			cd_segunda_moeda,
			dt_movimento,
			cd_projeto,
			substr(ds_hist_padrao,1,200) ds_hist_padrao,
			ds_compl_hist_1,
			ds_compl_hist_2,
			ds_compl_hist_3,
			cd_cgc,
			cd_pessoa_fisica,
			cd_historico,
			ds_historico_orig,
			cd_centro_orig,
			sum(vl_movimento) vl_movimento
	from 	(select somente_numero(mc.cd_conta_sis_contabil) cd_conta_contabil,
			somente_numero(mc.cd_centro_sis_contabil) cd_centro_custo,
			mc.ie_debito_credito,
			--#case Para Identificar Parametro do Lote Contabil
			case
				when (select T.DS_OBSERVACAO
						from LOTE_CONTABIL T
						where t.NR_LOTE_CONTABIL = nr_lote_contabil_p
						and t.cd_tipo_lote_contabil = 6) = 'PARTICULAR' then
							substr(mc.ds_compl_historico,1,200)
				else
				substr('RPS: ' || mc.nr_seq_agrupamento || ' | ' || initcap(obter_nome_pf_pj(nf.cd_pessoa_fisica, nf.cd_cgc)),1,200)
			end ds_compl_historico,
				sum(mc.vl_movimento) vl_movimento,
				'' vl_segunda_moeda,
				'' cd_segunda_moeda,
				to_char(mc.dt_movimento, 'ddmmyyyy') dt_movimento,
				'' cd_projeto,
				'' ds_hist_padrao,
				'' ds_compl_hist_1,
				'' ds_compl_hist_2,
				'' ds_compl_hist_3,
				mc.cd_cgc,
				mc.cd_pessoa_fisica,
				mc.cd_historico,
			--#case Para Identificar Parametro do Lote Contabil
			case
				when(select	T.DS_OBSERVACAO
					from 	LOTE_CONTABIL T
					where 	NR_LOTE_CONTABIL = nr_lote_contabil_p
					and 	t.cd_tipo_lote_contabil = 6) = 'PARTICULAR' then
				substr(mc.ds_compl_historico,1,200)
				else
					substr('RPS: ' || mc.nr_seq_agrupamento || ' | ' || initcap(obter_nome_pf_pj(nf.cd_pessoa_fisica, nf.cd_cgc)),1,200)
			end ds_historico_orig,
			mc.cd_centro_custo cd_centro_orig
	from 	movimento_contabil_v mc,
			nota_fiscal          nf
	where	1 = 1
	and	mc.nr_lote_contabil = nr_lote_contabil_p
	and nf.nr_nota_fiscal = mc.nr_seq_agrupamento
	and nf.cd_estabelecimento = mc.cd_estabelecimento
	and nf.cd_serie_nf in ('HPUR', 'EPVM', 'PABT', 'PACT', 'PACP')
	and nf.ie_situacao <> 2
	group by	mc.cd_conta_sis_contabil,
				mc.cd_centro_sis_contabil,
				mc.ie_debito_credito,
				mc.nr_seq_agrupamento,
				mc.dt_movimento,
				mc.cd_cgc,
				mc.cd_pessoa_fisica,
				mc.cd_historico,
				mc.cd_centro_custo,
				mc.nr_sequencia,
				mc.nr_lote_contabil,
				nf.cd_cgc,
				nf.cd_pessoa_fisica,
				mc.ds_compl_historico)
	group by	cd_conta_contabil,
				cd_centro_custo,
				ie_debito_credito,
				ds_compl_historico,
				vl_segunda_moeda,
				cd_segunda_moeda,
				dt_movimento,
				cd_projeto,
				ds_hist_padrao,
				ds_compl_hist_1,
				ds_compl_hist_2,
				ds_compl_hist_3,
				cd_cgc,
				cd_pessoa_fisica,
				cd_historico,
				ds_historico_orig,
				cd_centro_orig
	union all
	--# Obter dados do Paciente PARTICULAR
	select cd_conta_contabil,
		cd_centro_custo,
		ie_debito_credito,
		ds_compl_historico,
		vl_segunda_moeda,
		cd_segunda_moeda,
		dt_movimento,
		cd_projeto,
		substr(ds_hist_padrao,1,200) ds_hist_padrao,
		ds_compl_hist_1,
		ds_compl_hist_2,
		ds_compl_hist_3,
		cd_cgc,
		cd_pessoa_fisica,
		cd_historico,
		ds_historico_orig,
		cd_centro_orig,
		sum(vl_movimento) vl_movimento
	from (select somente_numero(mc.cd_conta_sis_contabil) cd_conta_contabil,
				 somente_numero(mc.cd_centro_sis_contabil) cd_centro_custo,
				 mc.ie_debito_credito,
				 'Conta Paciente: ' || mc.nr_seq_agrupamento || ' - ' || obter_paciente_conta(mc.nr_seq_agrupamento, 'D') ds_compl_historico,
				 sum(mc.vl_movimento) vl_movimento,
				 '' vl_segunda_moeda,
				 '' cd_segunda_moeda,
				 to_char(mc.dt_movimento, 'ddmmyyyy') dt_movimento,
				 '' cd_projeto,
				 '' ds_hist_padrao,
				 '' ds_compl_hist_1,
				 '' ds_compl_hist_2,
				 '' ds_compl_hist_3,
				 mc.cd_cgc,
				 mc.cd_pessoa_fisica,
				 mc.cd_historico,
				 'Conta Paciente: ' || mc.nr_seq_agrupamento || ' - ' || obter_paciente_conta(mc.nr_seq_agrupamento, 'D') ds_historico_orig,
				 mc.cd_centro_custo cd_centro_orig
			from movimento_contabil_v mc,
				 conta_paciente       cp
			where 1 = 1
			and cp.nr_interno_conta = mc.nr_seq_agrupamento
			and cp.nr_lote_contabil = mc.nr_lote_contabil
			and mc.nr_lote_contabil = nr_lote_contabil_p
			group by mc.cd_conta_sis_contabil,
					 mc.cd_centro_sis_contabil,
					 mc.ie_debito_credito,
					 mc.nr_seq_agrupamento,
					 mc.dt_movimento,
					 mc.cd_cgc,
					 mc.cd_pessoa_fisica,
					 mc.cd_historico,
					 mc.cd_centro_custo,
					 mc.nr_sequencia,
					 mc.nr_lote_contabil)
	group by cd_conta_contabil,
			cd_centro_custo,
			ie_debito_credito,
			ds_compl_historico,
			vl_segunda_moeda,
			cd_segunda_moeda,
			dt_movimento,
			cd_projeto,
			ds_hist_padrao,
			ds_compl_hist_1,
			ds_compl_hist_2,
			ds_compl_hist_3,
			cd_cgc,
			cd_pessoa_fisica,
			cd_historico,
			ds_historico_orig,
			cd_centro_orig;

vet01 C01%rowtype;
begin

  -- Verifica se o lote possui movimento
select	count(*)
into 	qt_registros_w
from 	movimento_contabil_v a
where 	a.nr_lote_contabil = nr_lote_contabil_p;

if (qt_registros_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(206410);
end if;

-- Buscar os dados do lote
select 	e.cd_empresa,
		l.dt_referencia
into	cd_empresa_w,
		dt_referencia_w
from 	lote_contabil   l,
		estabelecimento e
where 	l.cd_estabelecimento = e.cd_estabelecimento
and 	l.nr_lote_contabil = nr_lote_contabil_p;

select substr(MAX(a.cd_externo), 1, 20)
into 	cd_empresa_w
from 	conversao_meio_externo a
where 	a.cd_interno = cd_empresa_w
and 	a.nm_atributo = 'CD_EMPRESA'
and 	a.nm_tabela = 'EMPRESA';

-- Gravar os dados relativos ao processo (Tabela: W_MXM_TI_GERALANCCTB_TGLC)
/*select  max(tglc_sqprocesso) + 1
into  tgpr_sqprocesso_w
from  ti_geralancctb_tglc@MXMPRD;*/
-- # Alteracao realizada por Solicitacao da Operadora para utilizar a sequence padrao do MXM
-- # Alterado por Filipi Meirelles : 28082018
execute immediate 'select integra_sq_geralancctb.nextval@MXMPRD from dual'
into tgpr_sqprocesso_w;

insert into w_mxm_ti_geralancctb_tglc
			(tglc_sqprocesso,
			tglc_nuregprocessados,
			tglc_dtgeracao,
			tglc_usuariogeracao)
			values
			(tgpr_sqprocesso_w,
			0,
			sysdate,
			nm_usuario_p);

-- Gravar os dados relativos ao lote (Tabela: W_MXM_ARQCONTABIL_TADC)
insert into W_MXM_ARQCONTABIL_TADC
			(tadc_sqprocesso,
			tadc_sqregistro,
			tadc_stoperacao,
			tadc_cdempresa,
			tadc_lote,
			tadc_dtlancamento,
			tadc_nodocumento)
values		(tgpr_sqprocesso_w,
			-- mudar para buscar o valor da rotina
			1,
			-- mudar para pegar uma valor sequencial
			0,
			cd_empresa_w,
			nr_lote_contabil_p,
			to_char(dt_referencia_w, 'ddmmyyyy'),
			'');

-- Gravar os dados relativos aos movimentos do lote (Tabela; W_MXM_TI_ARQLANCCTB_TALC)

open c01;
loop
fetch c01 into vet01;
	exit when c01%NOTFOUND;
	begin
	nr_contador_w       := nr_contador_w + 1;
	cd_classif_centro_w := vet01.cd_centro_custo;

	if (nvl(vet01.cd_centro_orig, 0) = 0) then
		select 	max(cd_centro_custo)
				into cd_centro_custo_w
				from movimento_contabil_v
		where 	nr_lote_contabil = nr_lote_contabil_p
				and ie_debito_credito <> vet01.ie_debito_credito
				and ds_compl_historico = vet01.ds_historico_orig
				and cd_historico = vet01.cd_historico
				and cd_centro_custo is not null;

		select 	nvl(max(somente_numero(cd_classificacao)), '')
				into cd_classif_centro_w
				from centro_custo
		where 	cd_centro_custo = cd_centro_custo_w;

	end if;

	insert into	w_mxm_arqlancctb_talc
				(talc_sqprocesso,
				talc_sqregistro,
				talc_sqlancamento,
				talc_noconta,
				talc_noccusto,
				talc_indicador,
				talc_historico,
				talc_valor,
				talc_valor2moeda,
				talc_cd2moeda,
				talc_dtdigitacao,
				talc_cdprojeto,
				talc_histpadrao,
				talc_comphist1,
				talc_comphist2,
				talc_comphist3,
				talc_cdfor,
				talc_cdcliente,
				dt_envio,
				nr_lote_contabil,
				nm_usuario)
		values	(tgpr_sqprocesso_w,
				1,
				nr_contador_w,
				vet01.cd_conta_contabil,
				cd_classif_centro_w,
				vet01.ie_debito_credito,
				vet01.ds_compl_historico,
				vet01.vl_movimento,
				vet01.vl_segunda_moeda,
				vet01.cd_segunda_moeda,
				vet01.dt_movimento,
				vet01.cd_projeto,
				vet01.ds_hist_padrao,
				vet01.ds_compl_hist_1,
				vet01.ds_compl_hist_2,
				vet01.ds_compl_hist_3,
				vet01.cd_cgc,
				vet01.cd_pessoa_fisica,
				sysdate,
				nr_lote_contabil_p,
				nm_usuario_p);
	if (mod(nr_contador_w, 0) = 0) then
		commit;
	end if;

	end;
end loop;
close c01;

commit;

end ctb_intreceita_agrupado;
/