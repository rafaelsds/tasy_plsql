create or replace
procedure Med_Atualiza_Status_Protocolo
			(nr_seq_protocolo_p		number,
			nm_usuario_p			varchar2) is


ie_status_w			varchar2(01);
ie_novo_status_w		varchar2(01);
qt_item_faturamento_w	number(10);

begin

select	count(*)
into	qt_item_faturamento_w
from	med_faturamento
where	nr_seq_protocolo	= nr_seq_protocolo_p;

if	(qt_item_faturamento_w > 0) then
	begin

	select	ie_status
	into	ie_status_w
	from	med_prot_convenio
	where	nr_sequencia		= nr_seq_protocolo_p;

	if	(ie_status_w		= '1') then
		ie_novo_status_w	:= '2';
	else
		ie_novo_status_w	:= '1';
	end if;

	update	med_prot_convenio
	set	ie_status		= ie_novo_status_w,
		dt_atualizacao	= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_protocolo_p;

	end;
end if;

commit;

end Med_Atualiza_Status_Protocolo;
/