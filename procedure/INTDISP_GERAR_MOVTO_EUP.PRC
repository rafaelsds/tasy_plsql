create or replace
procedure intdisp_gerar_movto_eup(	nr_atendimento_p		number,
				cd_setor_atendimento_p	number,
				cd_unidade_basica_p	varchar2,
				dt_entrada_unidade_p	date,
				ie_tipo_movimento_p	varchar2) is
				
/*
Esta procedure e preciso ser apenas disparada quando for o primeiro lancado de setor para o paciente.
*/
pragma autonomous_transaction;

nm_pessoa_fisica_w      	varchar2(60);
nr_atendimento_w       		number(10);
ds_setor_atendimento_w		varchar2(100);
ds_unidade_atendimento_w	varchar2(100);
dt_entrada_w           		date;
ie_sexo_w               	varchar2(1);
dt_nascimento_w			date;
cd_estabelecimento_w		number(4);
nm_usuario_w			varchar2(15);
cd_setor_atendimento_w		number(5);
cd_unidade_basica_w		varchar2(20);
cd_medico_atendimento_w		varchar2(10);
nr_crm_medico_w			varchar2(20);
cd_pessoa_usuario_w		varchar2(10);
nm_pessoa_usuario_w		varchar2(60);
nr_prontuario_w			number(10);
dt_entrada_unidade_w		date;
nr_sequencia_w			number(10);
qt_existe_w			number(10);

cursor c02 is
	select	substr(c.nm_pessoa_fisica,1,60) nm_pessoa_fisica,
		b.nr_atendimento,
		b.dt_entrada,
		c.ie_sexo,
		c.dt_nascimento,
		b.cd_medico_atendimento,
		obter_crm_medico(b.cd_medico_atendimento),
		obter_prontuario_atendimento(b.nr_atendimento)
	from    atendimento_paciente b,
		pessoa_fisica c
	where   b.cd_pessoa_fisica = c.cd_pessoa_fisica
	and	b.nr_atendimento = nr_atendimento_p
	and	qt_existe_w = 0
	order by c.nm_pessoa_fisica;

begin

select	nvl(max(cd_estabelecimento),wheb_usuario_pck.get_cd_estabelecimento),
	max(nm_usuario)
into	cd_estabelecimento_w,
	nm_usuario_w
from	atendimento_paciente
where 	nr_atendimento = nr_atendimento_p;

cd_pessoa_usuario_w := obter_pessoa_fisica_usuario(nm_usuario_w,'C');
nm_pessoa_usuario_w := obter_pessoa_fisica_usuario(nm_usuario_w,'X');

select	count(*)
into	qt_existe_w
from	atend_paciente_unidade
where	nr_atendimento = nr_atendimento_p;

open c02;
loop
fetch c02 into	
	nm_pessoa_fisica_w,
	nr_atendimento_w,
	dt_entrada_w,
	ie_sexo_w,
	dt_nascimento_w,
	cd_medico_atendimento_w,
	nr_crm_medico_w,
	nr_prontuario_w;
exit when c02%notfound;
	begin
	
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	int_disp_movt_pac;
	if	(ie_tipo_movimento_p is not null) then
		begin
		insert into int_disp_movt_pac(
			nr_sequencia,
			nr_atendimento,
			nm_pessoa_fisica,
			cd_setor_internacao,
			cd_unidade_basica,
			cd_estabelecimento,
			dt_entrada,
			ie_tipo_movimentacao,
			dt_movimento,
			ie_sexo,
			dt_nascimento,
			cd_medico,
			crm_medico,
			cd_usuario,
			nm_usuario,
			ds_setor,
			dt_atendimento,
			nr_prontuario,
			dt_entrada_unidade)
		values	(nr_sequencia_w,
			nr_atendimento_w,
			nm_pessoa_fisica_w,
			cd_setor_atendimento_p,
			cd_unidade_basica_p,
			cd_estabelecimento_w,
			dt_entrada_w,
			ie_tipo_movimento_p,
			sysdate,
			ie_sexo_w,
			dt_nascimento_w,
			cd_medico_atendimento_w,
			nr_crm_medico_w,
			cd_pessoa_usuario_w,
			nm_pessoa_usuario_w,
			substr(obter_ds_descricao_setor(cd_setor_atendimento_p),1,100),
			sysdate,
			nr_prontuario_w,
			dt_entrada_unidade_p);
		end;
	end if;	
	end;
end loop;
close c02;
commit;

end intdisp_gerar_movto_eup;
/