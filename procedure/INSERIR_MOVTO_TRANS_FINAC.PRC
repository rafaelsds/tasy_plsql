create or replace
procedure inserir_movto_trans_finac(
		dt_saldo_p		varchar2,
		nr_seq_trans_financ_p 	number,
		vl_transacao_p		number,
		nr_seq_caixa_p		number,
		cd_conta_contabil_p	number,
		ds_historico_p		varchar2,
		nr_seq_lote_p		number,
		nr_saldo_aberto_p		number,
		nm_usuario_p		varchar2) is 
begin
insert into  movto_trans_financ
	(nr_sequencia,
	dt_transacao,
	nr_seq_trans_financ,
	vl_transacao,
	dt_atualizacao,
	nm_usuario,
	nr_seq_caixa,
	cd_conta_contabil,
        	ds_historico,
        	dt_referencia_saldo,
	nr_seq_lote,
	nr_seq_saldo_caixa,
        	ie_conciliacao,
        	nr_lote_contabil)
values  	(movto_trans_financ_seq.nextval,
	dt_saldo_p,
	nr_seq_trans_financ_p,
	vl_transacao_p,
	sysdate,
        	nm_usuario_p,
	nr_seq_caixa_p,
	cd_conta_contabil_p,
	ds_historico_p,
	dt_saldo_p,
        	nr_seq_lote_p,
        	nr_saldo_aberto_p,
        	'N',
        	0);
end inserir_movto_trans_finac;
/