create or replace
procedure insert_equip_waiting_list(nr_seq_lista_p 	number,
									lista_equip_p	varchar2) is

lista_equip_w			varchar2(400);
ie_contador_w			number(10,0) := 0;
tam_lista_w				number(10,0);
ie_pos_virgula_w		number(3,0);
cd_equipamento_w		number(10,0);
nr_seq_classif_equip_w	number(10,0);
ie_tipo_w				varchar2(1);
cd_estabelecimento_w    number(5);
cd_perfil_w            	number(5);
nm_usuario_w            varchar2(15);

begin

nm_usuario_w 			:= wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w 	:= wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w 			:= wheb_usuario_pck.get_cd_perfil;


select	nvl(max(obter_valor_param_usuario(871, 100, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w)),'C')
into	ie_tipo_w
from	dual;

cd_equipamento_w		:= null;
nr_seq_classif_equip_w	:= null;

lista_equip_w := lista_equip_p;

while lista_equip_w is not null or ie_contador_w > 200 loop
	tam_lista_w			:= length(lista_equip_w);
	ie_pos_virgula_w	:= instr(lista_equip_w, ',');

	if	(ie_pos_virgula_w <> 0) then
		if	(ie_tipo_w = 'E') then
			cd_equipamento_w := substr(lista_equip_w, 1, (ie_pos_virgula_w - 1));
		else
			nr_seq_classif_equip_w := substr(lista_equip_w, 1, (ie_pos_virgula_w - 1));
		end if;
		
		lista_equip_w := substr(lista_equip_w, (ie_pos_virgula_w + 1), tam_lista_w);
	end if;

	insert into agenda_lista_espera_equip(
		nr_sequencia,
		nr_seq_lista,
		dt_atualizacao,
		nm_usuario,
		cd_equipamento,
		nr_seq_classif_equip,
		ie_origem_inf,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_obrigatorio
	) values (
		agenda_lista_espera_equip_seq.nextval,
		nr_seq_lista_p,
		sysdate,
		nm_usuario_w,
		cd_equipamento_w,
		nr_seq_classif_equip_w,
		'I',
		sysdate,
		nm_usuario_w,
		'S'
	);

	ie_contador_w := ie_contador_w + 1;
end loop;

end insert_equip_waiting_list;
/
