create or replace procedure pls_gerar_requisicao_web(	nr_seq_requisicao_p			number,
					dt_requisicao_p				date,
					ie_tipo_guia_p				varchar2,
					nr_seq_plano_p				number,
					cd_medico_solicitante_p			varchar2,
					nr_seq_segurado_p			number,
					nr_seq_prestador_p			number,
					cd_especialidade_p			number,
					ie_tipo_consulta_p			number,
					ie_tipo_saida_p				varchar2,
					nm_recem_nascido_p     			varchar2,
					dt_nasc_recem_nascido_p			date,
					ie_unidade_tempo_p			varchar2,
					qt_tempo_p				number,
					ie_tipo_doenca_p			varchar2,
					ie_indicacao_acidente_p			varchar2,
					nr_seq_prestador_exec_p			number,
					ie_tipo_atendimento_p			varchar2,
					ie_carater_atendimento_p		varchar2,
					cd_doenca_p				varchar2,
					ds_indicacao_clinica_p			varchar2,
					ie_tipo_internacao_p			number,
					ie_regime_internacao_p			varchar2,
					nr_seq_tipo_acomodacao_p		number,
					ds_biometria_p				varchar2,
					dt_entrada_hospital_p			date,
					dt_alta_hospital_p			date,
					cd_estabelecimento_p			number,
					ie_consulta_urgencia_p			varchar2,
					nr_seq_prestador_web_p			number,
					nm_usuario_p				varchar2,
					cd_senha_p				varchar2,
					ie_tipo_gat_p				varchar2,
					dt_validade_senha_p			date,
					cd_guia_principal_p			varchar2,
					ie_tipo_validacao_digital_p		number,
					nr_seq_maquina_p			number,
					id_maquina_p				varchar2,
					nr_seq_guia_principal_p			number,
					ds_conteudo_tarja_p			varchar2,
					ie_forma_loc_benef_p			varchar2,
					nr_seq_motivo_prorrog_p			number,
					dt_internacao_sugerida_p		date,
					qt_dia_solicitado_p			number,
					ie_motivo_encerramento_p		varchar2,
					nr_seq_cbo_saude_p			number,
					ds_observacao_p				varchar2,
					ds_token_p				varchar2,
					ds_qrcode_p				varchar2,
					cd_ausencia_val_benef_tiss_p  varchar2 ) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Gerar requisicaoo para autorizacao no portal web
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [ x ] Portal [  ] Relatorios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

ie_transacao_w				varchar2(3);
nm_cooperativa_w			varchar2(255);
cd_cooperativa_w			varchar2(5);
ie_tipo_intercambio_w			varchar2(3);

cd_matricula_estipulante_w		varchar2(30);
nr_seq_congenere_w			number(10);
qt_registros_w				number(10) := 0;

nr_seq_uni_exec_w			number(10) := null;
cd_cgc_outorgante_w			varchar2(14);
ie_estagio_req_w			number(3);
ie_forma_loc_benef_w			varchar2(2);
ie_recem_nasc_w				varchar2(2) := 'N';
ie_pcmso_w				pls_segurado.ie_pcmso%type;

begin

select	max(cd_cgc_outorgante)
into	cd_cgc_outorgante_w
from	pls_outorgante
where	ie_tipo_outorgante = 3;

if	(cd_cgc_outorgante_w is not null) then
	select	max(nr_sequencia)
	into	nr_seq_uni_exec_w
	from	pls_congenere
	where	cd_cgc	= cd_cgc_outorgante_w
	and 	ie_tipo_congenere = 'CO';
end if;

begin
	select	nr_seq_congenere,
		cd_matricula_estipulante,
		ie_pcmso
	into	nr_seq_congenere_w,
		cd_matricula_estipulante_w,
		ie_pcmso_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;
exception
when others then
	nr_seq_congenere_w := null;
	cd_matricula_estipulante_w := null;
end;

if	(nr_seq_congenere_w is not null) then
	select  count(1)
	into	qt_registros_w
	from	pls_congenere
	where	ie_tipo_congenere = 'OP'
	and	nr_sequencia = nr_seq_congenere_w;
end if;

if	(qt_registros_w = 0) then
	cd_matricula_estipulante_w := null;
end if;

if	(nm_recem_nascido_p is not null) and (dt_nasc_recem_nascido_p is not null)then
	ie_recem_nasc_w := 'S';
end if;


insert into pls_requisicao     (
	nr_sequencia, dt_atualizacao, dt_requisicao,
	dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
	ie_estagio, ie_tipo_guia, ie_tipo_requisicao,
	nr_seq_plano, cd_medico_solicitante, nr_seq_segurado,
	nr_seq_prestador, cd_estabelecimento, ie_tipo_consulta,
	ie_tipo_saida, nm_recem_nascido, dt_nasc_recem_nascido,
	cd_especialidade, nr_seq_prestador_exec, ie_tipo_atendimento,
	ie_carater_atendimento, ds_indicacao_clinica, nr_seq_clinica,
	ie_regime_internacao, nr_seq_tipo_acomodacao, ds_biometria,
	dt_entrada_hospital,dt_alta_hospital, ie_consulta_urgencia,
	ie_tipo_processo, nr_seq_prestador_web, cd_senha_externa,
	dt_valid_senha_ext, ie_tipo_gat, ie_origem_solic,
	nm_usuario_solic, cd_guia_principal, ie_tipo_validacao_digital,
	nr_seq_maquina, id_maquina, nr_seq_guia_principal,
	cd_matricula_estipulante, nr_seq_uni_exec,
	ds_conteudo_tarja, ie_forma_loc_benef, ie_recem_nascido,
	nr_seq_motivo_prorrogacao, ie_status, ie_pcmso,
	qt_dia_solicitado, dt_internacao_sugerida, ie_motivo_encerramento,
	nr_seq_cbo_saude, ds_observacao, ie_req_web_finalizada,
	ds_token, ds_qrcode, cd_ausencia_val_benef_tiss)
values	(
	nr_seq_requisicao_p, sysdate, dt_requisicao_p,
	sysdate, nm_usuario_p, nm_usuario_p,
	'1', ie_tipo_guia_p, 'W',
	nr_seq_plano_p, cd_medico_solicitante_p, nr_seq_segurado_p,
	nr_seq_prestador_p, cd_estabelecimento_p, ie_tipo_consulta_p,
	ie_tipo_saida_p, nm_recem_nascido_p, dt_nasc_recem_nascido_p,
	cd_especialidade_p, nr_seq_prestador_exec_p, ie_tipo_atendimento_p,
	ie_carater_atendimento_p, ds_indicacao_clinica_p, ie_tipo_internacao_p,
	ie_regime_internacao_p,nr_seq_tipo_acomodacao_p, ds_biometria_p,
	dt_entrada_hospital_p, dt_alta_hospital_p,ie_consulta_urgencia_p,
	'P', nr_seq_prestador_web_p,cd_senha_p,
	dt_validade_senha_p, nvl(ie_tipo_gat_p,'N'), 'P',
	nm_usuario_p, cd_guia_principal_p, ie_tipo_validacao_digital_p,
	nr_seq_maquina_p, id_maquina_p, nr_seq_guia_principal_p,
	cd_matricula_estipulante_w, nr_seq_uni_exec_w,
	ds_conteudo_tarja_p, ie_forma_loc_benef_p, ie_recem_nasc_w,
	nr_seq_motivo_prorrog_p, 'P', nvl(ie_pcmso_w,'N'),
	qt_dia_solicitado_p, dt_internacao_sugerida_p, ie_motivo_encerramento_p,
	nr_seq_cbo_saude_p, ds_observacao_p, 'N', ds_token_p, ds_qrcode_p, cd_ausencia_val_benef_tiss_p);

if	(cd_doenca_p is not null or ie_tipo_doenca_p is not null or ie_indicacao_acidente_p is not null or qt_tempo_p is not null) then
	insert into pls_requisicao_diagnostico
		(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
		nm_usuario, nm_usuario_nrec, nr_seq_requisicao,
		ie_unidade_tempo, qt_tempo, ie_tipo_doenca,
		ie_indicacao_acidente, cd_doenca, ie_classificacao)
	values	(pls_requisicao_diagnostico_seq.nextval, sysdate, sysdate,
		nm_usuario_p, nm_usuario_p, nr_seq_requisicao_p,
		ie_unidade_tempo_p, qt_tempo_p, ie_tipo_doenca_p,
		ie_indicacao_acidente_p, upper(cd_doenca_p),'P');
end if;

if	(ds_token_p is null and ds_qrcode_p is null and ds_conteudo_tarja_p is not null and ie_forma_loc_benef_p is not null) then
	ie_forma_loc_benef_w := ie_forma_loc_benef_p;
	if	(ie_forma_loc_benef_w <> 'T') then
		ie_forma_loc_benef_w := 'D';
	end if;

	pls_gravar_leitura_cartao(pls_obter_dados_segurado(nr_seq_segurado_p,'C'), nr_seq_segurado_p, ie_forma_loc_benef_w,
				  ds_conteudo_tarja_p, cd_estabelecimento_p, nm_usuario_p);

end if;

if	(ds_token_p is not null) then
	update	pls_token_atendimento
	set	dt_utilizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_seq_segurado	= nr_seq_segurado_p
	and	ds_token	= ds_token_p;
end if;

pls_dados_intercambio_web(null, nr_seq_requisicao_p, cd_estabelecimento_p,
			  ie_transacao_w, cd_cooperativa_w, nm_cooperativa_w,
			  ie_tipo_intercambio_w, ie_estagio_req_w);

if	(ie_transacao_w = 'I') then
	update	pls_requisicao
	set	ie_tipo_processo = 'I',
		ie_tipo_intercambio = 'I',
		dt_atualizacao	 = sysdate,
		nm_usuario	 = nm_usuario_p
	where	nr_sequencia 	 = nr_seq_requisicao_p;
end if;

pls_gravar_log_acesso_portal( null, null, nr_seq_prestador_web_p,
			      null, null, null,
			      null, null, '34',
			      'O usuario '||nm_usuario_p||' solicitou a requisicao '||nr_seq_requisicao_p||'.', 'Solicitacao de Requisicao.', 'N');
commit;

end pls_gerar_requisicao_web;
/