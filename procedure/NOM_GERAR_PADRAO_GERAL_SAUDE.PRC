create or replace
PROCEDURE nom_gerar_padrao_geral_saude( cd_estabelecimento_p   NUMBER,
                                        dt_referencia_p   DATE,
                                        nm_usuario_p   VARCHAR2,
                                        ie_origem_chamada_p   VARCHAR2) IS

nr_atendimento_w     atendimento_paciente.nr_atendimento%TYPE;
cd_pessoa_fisica_w   pessoa_fisica.cd_pessoa_fisica%TYPE;
qt_gerado_w          NUMBER(7);
qt_registro_w		number(10);
cd_dependecia_w		varchar2(10);
dt_minima_w	date;

CURSOR encounter_insert IS
   SELECT DISTINCT
            ap.cd_pessoa_fisica,
            null nr_atendimento,
            b.cd_plano_convenio,
            to_char(b.cd_dependente) cd_tipo_beneficiario,
            b.cd_usuario_convenio,
            nvl(pj.cd_sistema_ant,nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0'))) cd_sistema_ant,
            decode(qt_gerado_w,0,'F','N') ie_tipo_geracao,
            null ie_tipo_registro,
			b.dt_inicio_vigencia dt_inicio_vigencia,
			b.dt_final_vigencia dt_fim_vigencia,
			b.nr_seq_interno nr_seq_atend_cat_convenio,
			null nr_seq_pessoa_tit_convenio
   FROM     pessoa_juridica pj,
			atendimento_paciente ap,
            atend_categoria_convenio b,
            convenio c
   WHERE    b.nr_atendimento = ap.nr_atendimento
   AND      b.cd_convenio = c.cd_convenio
   AND      b.cd_plano_convenio IS NOT NULL
   AND      b.dt_final_vigencia >= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
   AND     ( b.dt_inicio_vigencia < pkg_date_utils.end_of(dt_referencia_p,'MONTH') /*or qt_gerado_w > 0*/)
   AND      ap.cd_estabelecimento = cd_estabelecimento_p
   and		pj.cd_cgc = c.cd_cgc
	and		not exists	(select	1
						from	pessoa_titular_convenio x
						where	x.cd_pessoa_fisica = ap.cd_pessoa_fisica)
   AND      NOT EXISTS (SELECT 1
                        FROM nom_padrao_geral_saude x
                        WHERE x.cd_pessoa_fisica = ap.cd_pessoa_fisica
                        AND (x.cd_sistema_ant = nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) OR x.cd_sistema_ant IS NULL)
						and	(x.nr_seq_atend_cat_convenio = b.nr_seq_interno or x.nr_seq_atend_cat_convenio is null))
	and	ap.dt_atualizacao > dt_minima_w
	and	b.cd_dependente is not null
	and	b.cd_usuario_convenio is not null
	and exists (select 1 from pessoa_fisica x where x.cd_pessoa_fisica = ap.cd_pessoa_fisica and x.dt_atualizacao > dt_minima_w)
   UNION
   SELECT DISTINCT
            pf.cd_pessoa_fisica cd_pessoa_fisica,
            null nr_atendimento,
            pc.cd_plano_convenio,
            pc.ie_grau_dependencia cd_tipo_beneficiario,
            pc.cd_usuario_convenio,
            nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) cd_sistema_ant,
            decode(qt_gerado_w,0,'F','N') ie_tipo_geracao,
            null ie_tipo_registro,
            pc.dt_inicio_vigencia dt_inicio_vigencia,
            pc.dt_fim_vigencia 	  dt_fim_vigencia,
			null nr_seq_atend_cat_convenio,
			pc.nr_sequencia nr_seq_pessoa_tit_convenio
   FROM     pessoa_juridica pj,
			pessoa_titular_convenio pc,
            pessoa_fisica pf,
            convenio c
   WHERE    pc.cd_pessoa_fisica = pf.cd_pessoa_fisica
   AND      pc.cd_convenio = c.cd_convenio
   AND      pc.cd_plano_convenio IS NOT NULL
   AND      pc.dt_fim_vigencia >= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
   AND     ( pc.dt_inicio_vigencia < pkg_date_utils.end_of(dt_referencia_p,'MONTH') /*or qt_gerado_w > 0*/)
   AND      pf.cd_estabelecimento = cd_estabelecimento_p
   and		pj.cd_cgc = c.cd_cgc
   AND      NOT EXISTS (SELECT 1
                        FROM nom_padrao_geral_saude x
                        WHERE x.cd_pessoa_fisica = pf.cd_pessoa_fisica
                        AND (x.cd_sistema_ant = nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) OR x.cd_sistema_ant IS NULL)
						and	(x.nr_seq_pessoa_tit_convenio = pc.nr_sequencia or x.nr_seq_pessoa_tit_convenio is null))
	and	pc.dt_atualizacao > dt_minima_w
	and exists (select 1 from pessoa_fisica x where x.cd_pessoa_fisica = pf.cd_pessoa_fisica and x.dt_atualizacao > dt_minima_w)
	and	pc.ie_grau_dependencia is not null
	and	pc.cd_usuario_convenio is not null
   ORDER BY cd_pessoa_fisica;

CURSOR encounter_update IS
   SELECT DISTINCT
            ap.cd_pessoa_fisica cd_pessoa_fisica,
            null nr_atendimento,
            b.cd_plano_convenio,
            to_char(b.cd_dependente) cd_tipo_beneficiario,
            b.cd_usuario_convenio,
			nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) cd_sistema_ant,
            'U' ie_tipo_geracao,
            'T' ie_tipo_registro,
            b.dt_inicio_vigencia dt_inicio_vigencia,
            b.dt_final_vigencia dt_fim_vigencia,
			b.nr_seq_interno nr_seq_atend_cat_convenio,
			null nr_seq_pessoa_tit_convenio
   FROM     pessoa_juridica pj,
			atendimento_paciente ap,
            atend_categoria_convenio b,
            convenio c
   WHERE    b.nr_atendimento = ap.nr_atendimento
   AND      b.cd_convenio = c.cd_convenio
   AND      b.cd_plano_convenio IS NOT NULL
   AND      b.dt_final_vigencia BETWEEN pkg_date_utils.start_of(dt_referencia_p,'MONTH') AND pkg_date_utils.end_of(dt_referencia_p,'MONTH')
   AND      ap.cd_estabelecimento = cd_estabelecimento_p
   and		pj.cd_cgc = c.cd_cgc
   AND      EXISTS (SELECT 1
                    FROM nom_padrao_geral_saude x
                    WHERE x.cd_pessoa_fisica = ap.cd_pessoa_fisica
                    AND (x.cd_sistema_ant = nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) OR x.cd_sistema_ant IS NULL)
                    AND x.ie_tipo_geracao IN ('F','N'))
   AND      NOT EXISTS (SELECT 1
                    FROM atend_categoria_convenio x, atendimento_paciente y
                    WHERE y.cd_pessoa_fisica = ap.cd_pessoa_fisica
                    AND x.nr_atendimento = y.nr_atendimento
                    AND x.cd_convenio = c.cd_convenio
                    AND x.dt_inicio_vigencia <= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
                    AND x.dt_final_vigencia > pkg_date_utils.end_of(dt_referencia_p,'MONTH')
                    UNION
                    SELECT 1
                    FROM pessoa_titular_convenio z
                    WHERE z.cd_pessoa_fisica = ap.cd_pessoa_fisica
                    AND z.cd_convenio = c.cd_convenio
                    AND z.dt_inicio_vigencia <= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
                    AND z.dt_fim_vigencia > pkg_date_utils.end_of(dt_referencia_p,'MONTH')
                    )
	and		not exists	(select	1
						from	pessoa_titular_convenio x
						where	x.cd_pessoa_fisica = ap.cd_pessoa_fisica)
	and	ap.dt_atualizacao > dt_minima_w
	and exists (select 1 from pessoa_fisica x where x.cd_pessoa_fisica = ap.cd_pessoa_fisica and x.dt_atualizacao > dt_minima_w)
	and	b.cd_dependente is not null
	and	b.cd_usuario_convenio is not null
   UNION
   SELECT DISTINCT
            pf.cd_pessoa_fisica cd_pessoa_fisica,
            null nr_atendimento,
            pc.cd_plano_convenio,
            pc.ie_grau_dependencia cd_tipo_beneficiario,
            pc.cd_usuario_convenio,
            nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) cd_sistema_ant,
            'U' ie_tipo_geracao,
            'T' ie_tipo_registro,
            pc.dt_inicio_vigencia dt_inicio_vigencia,
            pc.dt_fim_vigencia 	  dt_fim_vigencia,
			null nr_seq_atend_cat_convenio,
			pc.nr_sequencia nr_seq_pessoa_tit_convenio
   FROM     pessoa_juridica pj,
			pessoa_titular_convenio pc,
            pessoa_fisica pf,
            convenio c
   WHERE    pc.cd_pessoa_fisica = pf.cd_pessoa_fisica
   AND      pc.cd_convenio = c.cd_convenio
   AND      pc.cd_plano_convenio IS NOT NULL
   AND      pc.dt_fim_vigencia BETWEEN pkg_date_utils.start_of(dt_referencia_p,'MONTH') AND pkg_date_utils.end_of(dt_referencia_p,'MONTH')
   AND      pf.cd_estabelecimento = cd_estabelecimento_p
   and		pj.cd_cgc = c.cd_cgc
   AND      EXISTS (SELECT 1
                    FROM nom_padrao_geral_saude x
                    WHERE x.cd_pessoa_fisica = pf.cd_pessoa_fisica
                    AND (x.cd_plano_convenio = pc.cd_plano_convenio OR x.cd_plano_convenio IS NULL)
                    AND (x.cd_sistema_ant = nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) OR x.cd_sistema_ant IS NULL)
                    AND x.ie_tipo_geracao IN ('F','N'))
	AND      not EXISTS (SELECT 1
                    FROM nom_padrao_geral_saude x
                    WHERE x.cd_pessoa_fisica = pf.cd_pessoa_fisica
                    AND (x.cd_plano_convenio = pc.cd_plano_convenio OR x.cd_plano_convenio IS NULL)
                    AND (x.cd_sistema_ant = nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0'))OR x.cd_sistema_ant IS NULL)
                    AND x.ie_tipo_geracao IN ('U')
					and	x.dt_referencia = pkg_date_utils.start_of(dt_referencia_p,'MONTH'))
   AND      NOT EXISTS (SELECT 1
                    FROM atend_categoria_convenio x, atendimento_paciente y
                    WHERE y.cd_pessoa_fisica = pf.cd_pessoa_fisica
                    AND x.nr_atendimento = y.nr_atendimento
                    AND x.cd_convenio = c.cd_convenio
                    AND x.dt_inicio_vigencia <= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
                    AND x.dt_final_vigencia > pkg_date_utils.end_of(dt_referencia_p,'MONTH')
                    UNION
                    SELECT 1
                    FROM pessoa_titular_convenio z
                    WHERE z.cd_pessoa_fisica = pf.cd_pessoa_fisica
                    AND z.cd_convenio = c.cd_convenio
                    AND z.dt_inicio_vigencia <= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
                    AND z.dt_fim_vigencia > pkg_date_utils.end_of(dt_referencia_p,'MONTH')
                    )
	and	pc.dt_atualizacao > dt_minima_w
	and exists (select 1 from pessoa_fisica x where x.cd_pessoa_fisica = pf.cd_pessoa_fisica and x.dt_atualizacao > dt_minima_w)
	and	pc.ie_grau_dependencia is not null
	and	pc.cd_usuario_convenio is not null
   UNION
   SELECT DISTINCT
            ap.cd_pessoa_fisica cd_pessoa_fisica,
            null nr_atendimento,
            b.cd_plano_convenio,
            to_char(b.cd_dependente) cd_tipo_beneficiario,
            b.cd_usuario_convenio,
            nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) cd_sistema_ant,
            'U' ie_tipo_geracao,
            'R' ie_tipo_registro,
            b.dt_inicio_vigencia dt_inicio_vigencia,
            b.dt_final_vigencia dt_fim_vigencia,
			b.nr_seq_interno nr_seq_atend_cat_convenio,
			null nr_seq_pessoa_tit_convenio
   FROM     pessoa_juridica pj,
			atendimento_paciente ap,
            atend_categoria_convenio b,
            convenio c
   WHERE    b.nr_atendimento = ap.nr_atendimento
   AND      b.cd_convenio = c.cd_convenio
   AND      b.cd_plano_convenio IS NOT NULL
   AND      b.dt_inicio_vigencia <= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
   and		b.dt_final_vigencia >= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
   AND      ap.cd_estabelecimento = cd_estabelecimento_p
   and		pj.cd_cgc = c.cd_cgc
   AND      EXISTS (SELECT 1
                    FROM nom_padrao_geral_saude y
                    WHERE y.nr_sequencia = (SELECT Max(nr_sequencia)
                                          FROM nom_padrao_geral_saude x
                                          WHERE x.cd_pessoa_fisica = ap.cd_pessoa_fisica
                                          AND (x.cd_sistema_ant = nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) OR x.cd_sistema_ant IS NULL))
                    AND y.ie_tipo_registro IN ('T')
					and	y.dt_referencia < pkg_date_utils.start_of(dt_referencia_p,'MONTH'))
	and		not exists	(select	1
						from	pessoa_titular_convenio x
						where	x.cd_pessoa_fisica = ap.cd_pessoa_fisica)
	and	ap.dt_atualizacao > dt_minima_w
	and exists (select 1 from pessoa_fisica x where x.cd_pessoa_fisica = ap.cd_pessoa_fisica and x.dt_atualizacao > dt_minima_w)
	and	b.cd_dependente is not null
	and	b.cd_usuario_convenio is not null
   UNION
   SELECT DISTINCT
            pf.cd_pessoa_fisica cd_pessoa_fisica,
            null nr_atendimento,
            pc.cd_plano_convenio,
            pc.ie_grau_dependencia cd_tipo_beneficiario,
            pc.cd_usuario_convenio,
            nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) cd_sistema_ant,
            'U' ie_tipo_geracao,
            'R' ie_tipo_registro,
            pc.dt_inicio_vigencia dt_inicio_vigencia,
            pc.dt_fim_vigencia 	  dt_fim_vigencia,
			null nr_seq_atend_cat_convenio,
			pc.nr_sequencia nr_seq_pessoa_tit_convenio
   FROM     pessoa_juridica pj,
			pessoa_titular_convenio pc,
            pessoa_fisica pf,
            convenio c
   WHERE    pc.cd_pessoa_fisica = pf.cd_pessoa_fisica
   AND      pc.cd_convenio = c.cd_convenio
   AND      pc.cd_plano_convenio IS NOT NULL
   AND      pc.dt_inicio_vigencia <= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
   and		pc.dt_fim_vigencia >= pkg_date_utils.end_of(dt_referencia_p,'MONTH')
   AND      pf.cd_estabelecimento = cd_estabelecimento_p
   and		pj.cd_cgc = c.cd_cgc
   AND      EXISTS (SELECT 1
                    FROM nom_padrao_geral_saude y
                    WHERE y.nr_sequencia = (SELECT Max(nr_sequencia)
                                          FROM nom_padrao_geral_saude x
                                          WHERE x.cd_pessoa_fisica = pf.cd_pessoa_fisica
                                          AND (x.cd_sistema_ant = nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) OR x.cd_sistema_ant IS NULL))
                    AND y.ie_tipo_registro IN ('T')
					and	y.dt_referencia < pkg_date_utils.start_of(dt_referencia_p,'MONTH'))
	AND      not EXISTS (SELECT 1
                    FROM nom_padrao_geral_saude x
                    WHERE x.cd_pessoa_fisica = pf.cd_pessoa_fisica
                    AND (x.cd_sistema_ant = nvl(pj.cd_sistema_ant,lpad(obter_dados_cat_derecho(c.cd_tipo_convenio_mx,'CD_DERECHOHABIENCIA'),2,'0')) OR x.cd_sistema_ant IS NULL)
                    AND x.ie_tipo_geracao IN ('U')
					and	x.dt_referencia = pkg_date_utils.start_of(dt_referencia_p,'MONTH'))
	and	pc.dt_atualizacao > dt_minima_w
	and exists (select 1 from pessoa_fisica x where x.cd_pessoa_fisica = pc.cd_pessoa_fisica and x.dt_atualizacao > dt_minima_w)
	and	pc.ie_grau_dependencia is not null
	and	pc.cd_usuario_convenio is not null
   ORDER BY cd_pessoa_fisica;

CURSOR   patient_data IS
   SELECT   c.cd_curp,
            UPPER(SUBSTR(z.ds_given_name,1,50)) nm_primeiro_nome,
            UPPER(SUBSTR(z.ds_family_name,1,50)) nm_sobrenome_pai,
            UPPER(SUBSTR(nvl(z.ds_component_name_1, 'SIN INFORMACION'),1,50)) nm_sobrenome_mae,
            c.dt_nascimento,
            decode(obter_se_brasileiro(c.cd_pessoa_fisica),'S',(SELECT decode(Max(cd_endereco_catalogo),'97','00','98','00','99','00',Max(cd_endereco_catalogo))
                                                                           FROM end_endereco
                                                                           WHERE ie_informacao = 'ESTADO_PROVINCI'
                                                                           AND (sg_estado_tasy = c.sg_estado_nasc or c.sg_estado_nasc in ('97','98','99'))
                                                                           AND nr_seq_catalogo = obter_catalogo_nom(c.cd_estabelecimento)
																		   ),'NE') cd_entidade_nasc,
            DECODE(c.ie_sexo,'M','H','F','M','') IE_SEXO,
            c.cd_nacionalidade,
            NVL(lpad(get_info_end_endereco(d.nr_seq_pessoa_endereco,'ESTADO_PROVINCI','C'),2,'0'),'00') cd_entidade_res_hab,
            NVL(lpad(get_info_end_endereco(d.nr_seq_pessoa_endereco,'MUNICIPIO','C'),3,'0'),'000') cd_municipio_res_hab,
            NVL(lpad(get_info_end_endereco(d.nr_seq_pessoa_endereco,'LOCALIDADE_AREA','C'),4,'0'),'0000') cd_localidade_res_hab,
            d.nr_seq_pessoa_endereco
   FROM     pessoa_fisica c,
            compl_pessoa_fisica d,
            person_name z
   WHERE    c.cd_pessoa_fisica      = cd_pessoa_fisica_w
   AND      c.cd_estabelecimento    = cd_estabelecimento_p
   AND      c.cd_pessoa_fisica      = d.cd_pessoa_fisica
   AND      d.ie_tipo_complemento   = 1
   AND      c.nr_seq_person_name    = z.nr_sequencia
   AND      'main'                  = z.ds_type (+)
   AND      ROWNUM <= 1
   and		c.dt_obito is null;

BEGIN

dt_minima_w	:= to_date('24/07/2019','dd/mm/yyyy');

SELECT Count(1)
INTO qt_gerado_w
FROM nom_padrao_geral_saude;

IF (ie_origem_chamada_p = 'I') THEN
  FOR r_encounter IN encounter_insert LOOP
    BEGIN
      nr_atendimento_w := r_encounter.nr_atendimento;
      cd_pessoa_fisica_w := r_encounter.cd_pessoa_fisica;
	  cd_dependecia_w	:= r_encounter.cd_sistema_ant;
	  
      FOR r_patient_data IN patient_data LOOP
        BEGIN	
			
		  select	count(1)
		  into		qt_registro_w
		  from		nom_padrao_geral_saude a
		  where		cd_pessoa_fisica = cd_pessoa_fisica_w
		  and		cd_sistema_ant = cd_dependecia_w
		  and		ie_tipo_geracao in ('F','N')
		  ;
		  
		  if	(qt_registro_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1079528,'CD_CURP=' || r_patient_data.cd_curp  || ';' || 'CD_DEPENDENCIA=' || cd_dependecia_w);
		  end if;
		
		
          INSERT INTO nom_padrao_geral_saude (nr_sequencia,
                                              dt_atualizacao,
                                              nm_usuario,
                                              dt_atualizacao_nrec,
                                              nm_usuario_nrec,
                                              cd_estabelecimento,
                                              cd_curp,
                                              nm_primeiro_nome,
                                              nm_sobrenome_pai,
                                              nm_sobrenome_mae,
                                              dt_nascimento,
                                              cd_entidade_nasc,
                                              ie_sexo,
                                              cd_nacionalidade,
                                              cd_entidade_res_hab,
                                              cd_municipio_res_hab,
                                              cd_localidade_res_hab,
                                              cd_tipo_beneficiario,
                                              ds_tipo_beneficiario,
                                              cd_plano_convenio,
                                              cd_usuario_convenio,
                                              cd_sistema_ant,
                                              ie_tipo_registro,
                                              dt_referencia,
                                              ie_tipo_geracao,
                                              cd_pessoa_fisica,
                                              nr_seq_pessoa_endereco,
                                              nr_atendimento,
                                              dt_inicio_vigencia,
                                              dt_fim_vigencia,
											  nr_seq_atend_cat_convenio,
											  nr_seq_pessoa_tit_convenio)
          VALUES                             (nom_padrao_geral_saude_seq.NEXTVAL,
                                              SYSDATE,
                                              nm_usuario_p,
                                              SYSDATE,
                                              nm_usuario_p,
                                              cd_estabelecimento_p,
                                              r_patient_data.cd_curp,
                                              r_patient_data.nm_primeiro_nome,
                                              r_patient_data.nm_sobrenome_pai,
                                              r_patient_data.nm_sobrenome_mae,
                                              r_patient_data.dt_nascimento,
                                              r_patient_data.cd_entidade_nasc,
                                              r_patient_data.ie_sexo,
                                              r_patient_data.cd_nacionalidade,
                                              r_patient_data.cd_entidade_res_hab,
                                              r_patient_data.cd_municipio_res_hab,
                                              r_patient_data.cd_localidade_res_hab,
                                              r_encounter.cd_tipo_beneficiario,
                                              r_encounter.cd_tipo_beneficiario || ' - ' || obter_valor_dominio(1034, r_encounter.cd_tipo_beneficiario),
                                              r_encounter.cd_plano_convenio,
                                              r_encounter.cd_usuario_convenio,
                                              r_encounter.cd_sistema_ant,
                                              r_encounter.ie_tipo_registro,
                                              pkg_date_utils.start_of(dt_referencia_p,'MONTH'),
                                              r_encounter.ie_tipo_geracao,
                                              r_encounter.cd_pessoa_fisica,
                                              r_patient_data.nr_seq_pessoa_endereco,
                                              r_encounter.nr_atendimento,
                                              r_encounter.dt_inicio_vigencia,
                                              r_encounter.dt_fim_vigencia,
											  r_encounter.nr_seq_atend_cat_convenio,
											  r_encounter.nr_seq_pessoa_tit_convenio);
        END;
      END LOOP;
    END;
  END LOOP;
ELSE
  FOR r_encounter IN encounter_update LOOP
    BEGIN
      nr_atendimento_w := r_encounter.nr_atendimento;
      cd_pessoa_fisica_w := r_encounter.cd_pessoa_fisica;
      FOR r_patient_data IN patient_data LOOP
        BEGIN
        INSERT INTO nom_padrao_geral_saude  (  nr_sequencia,
                                                dt_atualizacao,
                                                nm_usuario,
                                                dt_atualizacao_nrec,
                                                nm_usuario_nrec,
                                                cd_estabelecimento,
                                                cd_curp,
                                                --nm_primeiro_nome,
                                                --nm_sobrenome_pai,
                                                --nm_sobrenome_mae,
                                                --dt_nascimento,
                                                --cd_entidade_nasc,
                                                --ie_sexo,
                                                --cd_nacionalidade,
                                                --cd_entidade_res_hab,
                                                --cd_municipio_res_hab,
                                                --cd_localidade_res_hab,
                                                cd_tipo_beneficiario,
                                                ds_tipo_beneficiario,
                                                cd_plano_convenio,
                                                cd_usuario_convenio,
                                                cd_sistema_ant,
                                                ie_tipo_registro,
                                                dt_referencia,
                                                ie_tipo_geracao,
                                                cd_pessoa_fisica,
                                                --nr_seq_pessoa_endereco,
                                                nr_atendimento,
                                                dt_inicio_vigencia,
                                                dt_fim_vigencia,
												nr_seq_atend_cat_convenio,
												nr_seq_pessoa_tit_convenio)
                                        VALUES(  nom_padrao_geral_saude_seq.NEXTVAL,
                                                SYSDATE,
                                                nm_usuario_p,
                                                SYSDATE,
                                                nm_usuario_p,
                                                cd_estabelecimento_p,
                                                r_patient_data.cd_curp,
                                                --r_patient_data.nm_primeiro_nome,
                                                --r_patient_data.nm_sobrenome_pai,
                                                --r_patient_data.nm_sobrenome_mae,
                                                --r_patient_data.dt_nascimento,
                                                --r_patient_data.cd_entidade_nasc,
                                                --r_patient_data.ie_sexo,
                                                --r_patient_data.cd_nacionalidade,
                                                --r_patient_data.cd_entidade_res_hab,
                                                --r_patient_data.cd_municipio_res_hab,
                                                --r_patient_data.cd_localidade_res_hab,
                                                r_encounter.cd_tipo_beneficiario,
                                                r_encounter.cd_tipo_beneficiario || ' - ' || obter_valor_dominio(1034, r_encounter.cd_tipo_beneficiario),
                                                r_encounter.cd_plano_convenio,
                                                r_encounter.cd_usuario_convenio,
                                                r_encounter.cd_sistema_ant,
                                                r_encounter.ie_tipo_registro,
                                                pkg_date_utils.start_of(dt_referencia_p,'MONTH'),
                                                r_encounter.ie_tipo_geracao,
                                                r_encounter.cd_pessoa_fisica,
                                                --r_patient_data.nr_seq_pessoa_endereco,
                                                r_encounter.nr_atendimento,
                                                r_encounter.dt_inicio_vigencia,
                                                r_encounter.dt_fim_vigencia,
												r_encounter.nr_seq_atend_cat_convenio,
												r_encounter.nr_seq_pessoa_tit_convenio);
         END;
      END LOOP;
    END;
  END LOOP;
END IF;   

COMMIT;

END nom_gerar_padrao_geral_saude;
/