create or replace
procedure CIH_Enviar_Email_Pos_Alta_Med (	cd_estabelecimento_p	Number,	
						dt_referencia_p		Date,
						nm_usuario_p		Varchar2) is 

nr_atendimento_w		Number(10);
ds_email_remetente_w	Varchar2(100);
ds_email_destino_w		Varchar2(100);
ds_assunto_w			Varchar2(100);
ds_mensagem_w			Varchar2(4000);
ds_texto_w				Varchar2(32000);
ds_texto2_w				Varchar2(32000);
ds_texto_paciente_w		Varchar2(1000);
nm_pessoa_fisica_w		Varchar2(255);
cd_pessoa_fisica_w		Varchar2(10);
nm_medico_cirurgiao_w	Varchar2(255);
cd_medico_cirurgiao_w	Varchar2(10);
ie_envio_w				Varchar2(1);
ds_procedimento_w		procedimento.ds_procedimento%TYPE;
dt_cirurgia_w			Date;
dt_nascimento_w			Date;
					
Cursor C01 is
	select	DISTINCT
		b.cd_medico_cirurgiao,
		SUBSTR(obter_nome_pf(b.cd_medico_cirurgiao),1,255) nm_pessoa_fisica
	from	atendimento_paciente a,
		cirurgia b
	where	a.nr_atendimento = b.nr_atendimento
	and	a.dt_alta is not null
	and 	b.ie_status_cirurgia = '2'
	and	a.dt_alta between inicio_dia(dt_referencia_p) and fim_dia(last_day(dt_referencia_p))
	order by 2;
					
Cursor C02 is
	select	a.cd_pessoa_fisica,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,255) nm_pessoa_fisica,
		a.nr_atendimento,
		substr(obter_exame_agenda(b.cd_procedimento_princ, b.ie_origem_proced, b.nr_seq_proc_interno),1,240) ds_procedimento,
		b.dt_inicio_real,
		obter_dados_pf(a.cd_pessoa_fisica,'DN')
	from	atendimento_paciente a,
		cirurgia b
	where	a.nr_atendimento = b.nr_atendimento
	and	a.dt_alta is not null
	and 	b.ie_status_cirurgia = '2'
	and	b.cd_medico_cirurgiao = cd_medico_cirurgiao_w
	and	obter_se_alta_obito(a.nr_atendimento) = 'N'
	and	a.dt_alta between inicio_dia(dt_referencia_p) and fim_dia(last_day(dt_referencia_p))
	order by 2;
			
begin

select	max(ds_remetente_email),
	max(ds_assunto),
	max(ds_mensagem),
	max(ds_texto_paciente)
into	ds_email_remetente_w,
	ds_assunto_w,
	ds_mensagem_w,
	ds_texto_paciente_w
from	cih_parametros
where	(cd_estabelecimento = cd_estabelecimento_p
or	(cd_estabelecimento is null 
and not exists (select	1
		from	cih_parametros
		where	cd_estabelecimento = cd_estabelecimento_p)));

open C01;
loop
fetch C01 into	
	cd_medico_cirurgiao_w,
	nm_medico_cirurgiao_w;
exit when C01%notfound;
	begin
	
	select	max(ds_email_ccih)
	into	ds_email_destino_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_medico_cirurgiao_w;
	
	ds_texto2_w := '';
	
	open C02;
	loop
	fetch C02 into	
		cd_pessoa_fisica_w,
		nm_pessoa_fisica_w,
		nr_atendimento_w,
		ds_procedimento_w,
		dt_cirurgia_w,
		dt_nascimento_w;
	exit when C02%notfound;
		begin
		ds_texto2_w := substr(ds_texto2_w||chr(13)||chr(10)||chr(13)||chr(10)|| wheb_mensagem_pck.get_texto(800315) ||': '||nr_atendimento_w||' - '|| wheb_mensagem_pck.get_texto(800317) ||': '||dt_nascimento_w||' - '|| wheb_mensagem_pck.get_texto(800318) ||': '||dt_cirurgia_w||' - '|| wheb_mensagem_pck.get_texto(800319) ||' '||nm_pessoa_fisica_w||chr(13)||chr(10)||ds_texto_paciente_w,1,32000);
		
		end;
	end loop;
	close C02;
	
	ds_texto_w := substr(ds_mensagem_w||ds_texto2_w,1,32000);
	ds_texto_w := substr(replace(ds_texto_w,'@medico',nm_medico_cirurgiao_w),1,32000);
	
	if (ds_email_destino_w is not null) then
		Enviar_Email(ds_assunto_w,ds_texto_w,ds_email_remetente_w,ds_email_destino_w,nm_usuario_p,'M');
		Gerar_log_envio_pos_alta(cd_pessoa_fisica_w, nr_atendimento_w, '', ds_email_destino_w, nm_usuario_p);
	end if;
	
	end;
end loop;
close C01;

commit;

end CIH_Enviar_Email_Pos_Alta_Med;
/
