create or replace
procedure create_acc_batch_revenue(	cd_estabelecimento_p	number,
					qt_days_p		number,
					nm_usuario_p		varchar2) is 

					
Cursor C01 is
	select	a.nr_seq_protocolo
	from	protocolo_convenio a
	where	a.IE_STATUS_PROTOCOLO = 2
	and	a.DT_DEFINITIVO	between trunc(sysdate-qt_days_p) and sysdate
	and	not exists	(	select	1
					from	conta_paciente x
					where	x.nr_seq_protocolo = a.nr_seq_protocolo
					and	nvl(x.nr_lote_contabil,0)	> 0);
		
	
ds_valor_parametro_w	varchar2(8000);
nr_lote_contabil_w	number(10);
					
parametro_w	techone_pck.batchparameters_tp;
					
begin



for r_c01 in c01 loop
	begin

	if	(ds_valor_parametro_w	is null) then
		ds_valor_parametro_w	:= r_c01.nr_seq_protocolo;
	else
		ds_valor_parametro_w	:= ds_valor_parametro_w ||','||r_c01.nr_seq_protocolo;
	end if;
	
	end;
end loop;


if	(ds_valor_parametro_w	is not null) then
	
	wheb_usuario_pck.set_nm_usuario(nm_usuario_p);
	wheb_usuario_pck.set_cd_estabelecimento(cd_estabelecimento_p);
	philips_param_pck.set_nr_seq_idioma(9);
	nr_lote_contabil_w	:= techone_pck.create_batch(	cd_estabelecimento_p,
								6,
								sysdate,
								nm_usuario_p);
								
	if	(nr_lote_contabil_w	is not null) then
		
		parametro_w.DS_VALOR_PARAMETRO	:= ds_valor_parametro_w;
		
		techone_pck.update_parameters(	nr_lote_contabil_w,
						9,
						parametro_w);
						
		techone_pck.Generate_batch(	nr_lote_contabil_w,
						nm_usuario_p,
						'N');		

		techone_pck.send_integration(	nr_lote_contabil_w,
						nm_usuario_p);
		
	end if;
	
end if;



commit;

end create_acc_batch_revenue;
/