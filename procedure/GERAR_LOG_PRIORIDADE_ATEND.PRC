create or replace 
procedure gerar_log_prioridade_atend(
			nr_atendimento_p		Number,
			nm_usuario_p			Varchar2,
			nr_seq_triagem_p		Varchar2) is

nr_sequencia_w	Number(10,0);

BEGIN

insert into log_prioridade_atend(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_atendimento,
					nr_seq_triagem,
					nr_seq_triagem_old)
	values			(	log_prioridade_atend_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_atendimento_p,
					nr_seq_triagem_p,
					nr_seq_triagem_p);
					
commit;

END gerar_log_prioridade_atend;
/
