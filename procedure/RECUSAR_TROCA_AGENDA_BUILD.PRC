create or replace
procedure recusar_troca_agenda_build(nm_usuario_p Varchar2,
			nr_sequencia_p	number) is 

begin

update	agendamento_build a
set	ie_status = 'C'
where a.nr_sequencia = nr_sequencia_p;

commit;

end recusar_troca_agenda_build;
/
