create or replace
procedure 	EHR_CONSULTA_POR_ELEMENTOS(
			nr_seq_conteudo_p		Number,
			nm_usuario_p			Varchar2,
			ds_sequencias_p			Varchar2 default null) is 

nr_seq_elemento_w	number(10);
			
Cursor C01 is
	select	*
	from	ehr_elemento_result
	where	nr_seq_elemento = nr_seq_elemento_w
	and	instr(','||ds_sequencias_p||',', ','||to_char(nr_sequencia)||',') > 0
	order by 1;

c01_w	c01% rowtype;

begin

select	nr_seq_elemento
into	nr_seq_elemento_w
from	ehr_template_conteudo
where	nr_sequencia = nr_seq_conteudo_p;

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	insert into ehr_template_cont_item	(
						nr_sequencia,	
						nr_seq_item, 
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_apres,
						cd_resultado,
						ds_resultado,
						vl_resultado	
						)
					values	(
						ehr_template_cont_item_seq.nextval,	
						nr_seq_conteudo_p, 
						Sysdate,
						nm_usuario_p,
						Sysdate,
						nm_usuario_p,
						c01_w.nr_seq_apres,
						c01_w.cd_resultado,
						c01_w.ds_resultado,
						c01_w.vl_resultado
						);
	end;
end loop;
close C01;


commit;

end EHR_CONSULTA_POR_ELEMENTOS;
/
	