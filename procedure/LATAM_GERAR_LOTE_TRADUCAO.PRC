create or replace
procedure latam_gerar_lote_traducao(	nr_seq_idioma_p	number,
										cd_versao_p		varchar2,
										nm_usuario_p 	Varchar2) is

ie_origem_expressao_w			varchar2(2);
qt_existe_w						number(10);

nr_sequencia_lote_w				lote_traducao.nr_sequencia%type;
qt_total_caracter_w				lote_traducao.qt_total_caracteres%type;
qt_total_expressao_w			lote_traducao.qt_total_expressoes%type;
dt_geracao_lote_w				lote_traducao.dt_atualizacao_nrec%type;

nr_seq_lote_traducao_idioma_w	lote_traducao_idioma.nr_sequencia%type;

nr_seq_lote_trad_expressao_w	lote_traducao_expressao.nr_sequencia%type;

cd_expressao_w					dic_expressao.cd_expressao%type;

nr_seq_idioma_w					tasy_idioma.nr_sequencia%type;
ds_idioma_w						tasy_idioma.ds_idioma%type;

/*
nr_seq_idioma_p
1 - Brasil
2 - M�xico
5 - EUA
*/

Cursor C01 is
	select	a.nr_sequencia,
			a.ds_idioma
	from 	tasy_idioma a,
			lote_traducao_lib_idioma b
	where 	a.nr_sequencia = b.nr_seq_idioma
	and		b.ie_situacao = 'A'
	and		a.nr_sequencia = nr_seq_idioma_p
	order 	by 1;

Cursor C04 is
	select	a.cd_expressao,
			'N' ie_origem_expressao
	from   	dic_expressao a,
			dic_expressao_traducao b
	where  	a.cd_expressao = b.cd_expressao
	and		b.nm_usuario_traducao is null
	--and	a.nm_usuario <> 'TasyCorp'
	and		nvl(b.ie_status, 'N') = 'N'
	and		b.nr_seq_idioma = nr_seq_idioma_w
	and		((nr_seq_idioma_w = 1 and ds_expressao_us is not null) or --- Aqui para pegar o TEXTO no qual ser� a base para a TRADU��O
			 (nr_seq_idioma_w = 5 and ds_expressao_br is not null) or
			 (nr_seq_idioma_w = 2 and ds_expressao_br is not null))
	and		(not exists (	select	1
							from 	lote_traducao x,
									lote_traducao_expressao z,
									lote_traducao_idioma y
							where	x.nr_sequencia = z.nr_seq_lote_traducao
							and		x.nr_sequencia = y.nr_seq_lote_traducao
							and		z.cd_expressao = a.cd_expressao
							and 	y.dt_importacao IS NULL
							and		y.nr_seq_idioma = nr_seq_idioma_w) /* or
					 exists (	select	1
							from 		lote_traducao x,
									lote_traducao_expressao z,
									lote_traducao_idioma y
							where	x.nr_sequencia = z.nr_seq_lote_traducao
							and		x.nr_sequencia = y.nr_seq_lote_traducao
							and		z.cd_expressao = a.cd_expressao
							and		x.dt_atualizacao_nrec < a.dt_atualizacao
							and		x.nr_sequencia = (select	MAX(y.nr_seq_lote_traducao)
												from	lote_traducao_idioma y
												where y.nr_Seq_idioma = nr_seq_idioma_w)
							and		y.nr_seq_idioma = nr_seq_idioma_w)*/)
	union
	select	a.cd_expressao,
			'H' ie_origem_expressao
	from 	dic_expressao_historico a,
			dic_expressao_traducao b
	where 	a.cd_expressao = b.cd_expressao
	--and		a.nm_usuario <> 'TasyCorp'
	and 	ie_status = 'T'
	and		b.nr_seq_idioma = nr_seq_idioma_w
	and  	nr_seq_idioma_w <> 1
	and 	a.dt_atualizacao between dt_geracao_lote_w  and sysdate
	--and 	a.dt_atualizacao between sysdate - 30  and to_date('11/08/2014 17:33:47', 'dd/mm/yyyy hh24:mi:ss')
	--and 	a.dt_atualizacao between to_date('11/08/2014 17:33:47', 'dd/mm/yyyy hh24:mi:ss') and sysdate
	and		a.dt_atualizacao = (select	max(x.dt_atualizacao)
								from 	dic_expressao_historico x
								where 	x.cd_expressao = a.cd_expressao)
	ORDER	BY 2;

begin

--- Esse update � muito importante, pega o que nao foi traduzido e realiza atualiaza��o para status enviado
/*update	dic_expressao_traducao
set	ie_status 	= 'E',
	nm_usuario 	= nm_usuario_p,
	dt_atualizacao 	= sysdate
where	nvl(ie_status,'N') = 'N';*/


open C01;
loop
fetch C01 into
	nr_seq_idioma_w,
	ds_idioma_w;
exit when C01%notfound;
	begin

	select	count(*)
	into	qt_existe_w
	from	lote_traducao_idioma
	where	nr_seq_idioma = nr_seq_idioma_w;

	dt_geracao_lote_w := sysdate - 30;
	if	(qt_existe_w > 0) then
		begin

		select	x.dt_atualizacao_nrec
		into	dt_geracao_lote_w
		from 	lote_traducao x
		where	x.nr_sequencia = (	select	max(a.nr_sequencia)
									from 	lote_traducao a,
											lote_traducao_idioma b
									where	a.nr_sequencia = b.nr_seq_lote_traducao
									and		b.nr_seq_idioma = nr_seq_idioma_w);
		end;
	end if;


	select	lote_traducao_seq.nextval
	into	nr_sequencia_lote_w
	from 	dual;

	insert into lote_traducao (nr_sequencia,
							   dt_atualizacao,
							   nm_usuario,
							   dt_atualizacao_nrec,
							   nm_usuario_nrec)
	values		 			  (nr_sequencia_lote_w,
							   sysdate,
							   nm_usuario_p,
							   sysdate,
							   nm_usuario_p);

	select	lote_traducao_idioma_seq.nextval
	into	nr_seq_lote_traducao_idioma_w
	from	dual;

	insert into lote_traducao_idioma	(nr_sequencia,
										nr_seq_lote_traducao,
										nr_seq_idioma,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec)
values				     			    (nr_seq_lote_traducao_idioma_w,
										nr_sequencia_lote_w,
										nr_seq_idioma_w,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p);

	open C04;
	loop
	fetch C04 into
		cd_expressao_w,
		ie_origem_expressao_w;
	exit when C04%notfound;
		begin

		select	lote_traducao_expressao_seq.nextval
		into	nr_seq_lote_trad_expressao_w
		from	dual;

		insert into lote_traducao_expressao (nr_sequencia,
											 nr_seq_lote_traducao,
											 cd_expressao,
											 ie_origem_expressao,
											 dt_atualizacao,
											 nm_usuario,
											 dt_atualizacao_nrec,
											 nm_usuario_nrec)
		values				    			(nr_seq_lote_trad_expressao_w,
											 nr_sequencia_lote_w,
											 cd_expressao_w,
											 ie_origem_expressao_w,
											 sysdate,
											 nm_usuario_p,
											 sysdate,
											 nm_usuario_p);

		end;
	end loop;
	close C04;


	select	count(*) qt_expressao,
			sum(length(decode(nr_seq_idioma_w,1,b.ds_expressao_us,b.ds_expressao_br))) qt_total_caracter
	into	qt_total_expressao_w,
			qt_total_caracter_w
	from  	lote_traducao_expressao a,
			dic_expressao b
	where	a.cd_expressao = b.cd_expressao
	and		a.nr_seq_lote_traducao = nr_sequencia_lote_w;

	update	lote_traducao
	set		qt_total_caracteres = qt_total_caracter_w,
			qt_total_expressoes = qt_total_expressao_w,
			cd_versao	   	 	= cd_versao_p
	where	nr_sequencia = nr_sequencia_lote_w;

	end;
end loop;
close C01;

commit;

end latam_gerar_lote_traducao;
/