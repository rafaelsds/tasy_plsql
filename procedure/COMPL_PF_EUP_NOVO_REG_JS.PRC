create or replace
procedure compl_pf_eup_novo_reg_js(
				cd_pessoa_fisica_p	varchar2,
				ie_tipo_complemento_p	number,
				ds_msg_aborta_p		out varchar2) is 
ds_msg_aborta_w	varchar2(255);
qt_compl_w	number(10);
				
begin

if	(cd_pessoa_fisica_p is not null) then

	select	count(*)
	into	qt_compl_w
        from    compl_pessoa_fisica a
        where   a.cd_pessoa_fisica   = cd_pessoa_fisica_p 
        and     a.ie_tipo_complemento = ie_tipo_complemento_p;
	
	if	(qt_compl_w > 0) and
		(ie_tipo_complemento_p <> 9) then
		
		ds_msg_aborta_w :=  substr(obter_texto_tasy(288219, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	end if;
end if;

ds_msg_aborta_p := ds_msg_aborta_w;

commit;

end compl_pf_eup_novo_reg_js;
/