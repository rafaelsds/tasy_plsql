create or replace
procedure cpoe_gerar_medic_uso(			nm_usuario_p			varchar2,
										nr_atendimento_p		number,
										cd_pessoa_fisica_p		varchar2,
										nr_seq_material_p		number) is
begin
	if (nr_seq_material_p is not null) then
		insert into paciente_medic_uso (nr_sequencia, 
										dt_atualizacao, 
										nm_usuario, 
										dt_atualizacao_nrec, 
										nm_usuario_nrec, 
										nr_atendimento, 
										cd_pessoa_fisica,
										cd_material,
										cd_unid_med,
										cd_intervalo,										
										dt_inicio,
										dt_fim,
										ds_observacao,
										dt_registro,
										ds_justificativa,
										cd_setor_atendimento,
										ie_via_aplicacao,
										ds_horarios,
										ds_orientacao,
										ds_dose_diferenciada,
										ie_nega_medicamentos,
										cd_profissional) 
								select 	paciente_medic_uso_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_atendimento_p,
										cd_pessoa_fisica_p,
										cd_material,
										cd_unidade_medida,
										cd_intervalo,										
										dt_inicio,
										dt_fim,
										ds_observacao,
										sysdate,
										ds_justificativa,
										cd_setor_atendimento,
										ie_via_aplicacao,
										ds_horarios,
										ds_orientacao_preparo,
										ds_dose_diferenciada,
										'N',
										obter_pessoa_fisica_usuario(nm_usuario_p, 'C')
								from 	cpoe_material
								where 	nr_sequencia = nr_seq_material_p;		
		commit;
	end if;

end cpoe_gerar_medic_uso;
/