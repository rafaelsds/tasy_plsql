create or replace
procedure consiste_carteira_conv_js(	cd_convenio_p			varchar2,
					cd_usuario_convenio_p		varchar2,
					nr_atendimento_p	in out	varchar2,
					cd_pessoa_fisica_p		varchar2,
					ds_mensagem_p		out	varchar2,
					ds_mensagem_aux_p	out	varchar2) is 

qt_convenio_w		number(10,0);
nr_atendimento_par_w	number(10,0);
cd_pes_fisica_w		varchar2(255);
ds_mensagem_w		varchar2(255);
ds_mensagem_aux_w	varchar2(255);
			
begin

ds_mensagem_w := '';
ds_mensagem_aux_w := '';



select 	count(*)
into	qt_convenio_w
from   	atend_categoria_convenio a,
	atendimento_paciente b
where  	a.nr_atendimento = b.nr_atendimento
and    	a.cd_convenio = cd_convenio_p
and    	a.cd_usuario_convenio = cd_usuario_convenio_p
and    	a.nr_atendimento     <> nr_atendimento_p
and    	b.cd_pessoa_fisica   <> cd_pessoa_fisica_p;


if	(qt_convenio_w <> 0)then
	begin
	
	select  max(a.nr_atendimento), 
		max(substr(obter_nome_pf(b.cd_pessoa_fisica),1,80)) nm_paciente
	into	nr_atendimento_par_w,
		cd_pes_fisica_w
	from    atend_categoria_convenio a,
		atendimento_paciente b
	where   a.nr_atendimento = b.nr_atendimento
	and   	a.cd_convenio = cd_convenio_p
	and   	a.cd_usuario_convenio = cd_usuario_convenio_p
	and   	a.nr_atendimento <> nr_atendimento_p
	and   	b.cd_pessoa_fisica <> cd_pessoa_fisica_p;
	
	ds_mensagem_w		:= wheb_mensagem_pck.get_texto(795869,
							'NR_ATENDIMENTO_PAR='||nr_atendimento_par_w||
							';CD_PES_FISICA='||cd_pes_fisica_w);
							
	ds_mensagem_aux_w	:= wheb_mensagem_pck.get_texto(795863,
							'NR_ATENDIMENTO='||nr_atendimento_par_w||
							';NM_PACIENTE='||cd_pes_fisica_w);
	
	end;
end if;

ds_mensagem_p := ds_mensagem_w;
ds_mensagem_aux_p := ds_mensagem_aux_w;
nr_atendimento_p := nr_atendimento_par_w;


end consiste_carteira_conv_js;
/
