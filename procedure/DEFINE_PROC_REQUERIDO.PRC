CREATE OR REPLACE
PROCEDURE Define_Proc_Requerido
                       	(NR_ATENDIMENTO_P     			NUMBER,
                        CD_CONVENIO_P   				NUMBER,
                      	CD_PROC_EXECUTADO_P    			NUMBER,
				IE_ORIGEM_EXECUTADO_P			NUMBER,
				DS_PROC_REQUERIDO_P		OUT	VARCHAR2,
				IE_OBRIGATORIO_P			OUT	VARCHAR2) IS

qt_proc_w			NUMBER(5)		:= 0;

BEGIN

ds_proc_requerido_p			:= '';
select	count(*)
into		qt_proc_w
from		procedimento_requerido a
where		cd_proc_executado		= cd_proc_executado_p
  and		ie_origem_executado	= ie_origem_executado_p
  and		cd_convenio			= cd_convenio_p
  and		nvl(ie_obrigatorio,'N')	= 'S';
if	(qt_proc_w > 0) then
	begin
	select	count(*)
	into		qt_proc_w
	from		procedimento_requerido a
	where		cd_proc_executado		= cd_proc_executado_p
	  and		ie_origem_executado	= ie_origem_executado_p
	  and		cd_convenio			= cd_convenio_p
	  and		nvl(ie_obrigatorio,'N')	= 'S'
	  and		not exists	
			(select 1
			from procedimento_paciente b
			where nr_atendimento		= nr_atendimento_p
			  and a.cd_proc_requerido	= b.cd_procedimento
			  and a.ie_origem_Requerido	= b.ie_origem_proced);
	if	(qt_proc_w > 0) then
		ds_proc_requerido_p			:= wheb_mensagem_pck.get_texto(306670, null); -- Erro
	end if;
	end;
end if;

select	count(*)
into		qt_proc_w
from		procedimento_requerido a
where		cd_proc_executado		= cd_proc_executado_p
  and		ie_origem_executado	= ie_origem_executado_p
  and		cd_convenio			= cd_convenio_p
  and		nvl(ie_obrigatorio,'N')	= 'N';
if	(ds_proc_requerido_p = '') and
 	(qt_proc_w > 0) then
	begin
	select	count(*)
	into		qt_proc_w
	from		procedimento_requerido a
	where		cd_proc_executado		= cd_proc_executado_p
	  and		ie_origem_executado	= ie_origem_executado_p
	  and		cd_convenio			= cd_convenio_p
	  and		nvl(ie_obrigatorio,'N')	= 'N'
	  and		exists
			(select 1
			from procedimento_paciente b
			where nr_atendimento		= nr_atendimento_p
			  and a.cd_proc_requerido	= b.cd_procedimento
			  and a.ie_origem_Requerido	= b.ie_origem_proced);
	if	(qt_proc_w = 0) then
		ds_proc_requerido_p			:= wheb_mensagem_pck.get_texto(306670, null); -- Erro
	end if;
	end;
end if;

END Define_Proc_Requerido;
/
