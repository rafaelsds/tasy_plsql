create or replace
procedure pls_consistir_cpt
			(	nr_seq_segurado_p		number,
				nr_seq_guia_proc_p		number,
				nr_seq_conta_proc_p		number,
				nr_seq_reembolso_proc_p		number,
				nr_seq_requisicao_proc_p	number,
				cd_procedimento_p		number,
				ie_origem_proced_p		number,
				dt_solicitacao_p		date,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

cd_area_w			number(15);
cd_especialidade_w		number(15);
cd_grupo_w			number(15);
ie_origem_proced_w		number(10);
qt_dias_w			number(5);
dt_inicio_vigencia_w		date;
dt_carencia_w			date;
dt_inclusao_operadora_w		date;
nr_seq_carencia_proc_w		number(10);
ie_liberado_w			varchar2(1) := 'N';
nr_seq_prestador_w		number(10);
nr_seq_guia_w			number(10);
ie_tipo_consistencia_w		varchar2(2);
nr_seq_conta_w			number(10);
nr_seq_reembolso_w		number(10);
nr_seq_tipo_acomodacao_w	number(10);
ie_tipo_guia_w			varchar2(1);
nr_seq_requisicao_w		number(10);
dt_solicitacao_w		date;
nr_seq_grupo_servico_w		number(10);
ie_grupo_serv_lib_w		varchar2(1);
ie_liberado_ant_w		varchar2(1)	:= 'N';

Cursor c01 is
	select	a.nr_sequencia,
		a.ie_liberado,
		c.qt_dias,
		c.dt_inicio_vigencia,
		a.nr_seq_grupo_servico
	from	pls_carencia_proc a,
		pls_carencia c,
		pls_tipo_carencia b
	where	a.nr_seq_tipo_carencia					= b.nr_sequencia
	and	b.nr_sequencia						= c.nr_seq_tipo_carencia
	and	c.nr_seq_segurado					= nr_seq_segurado_p
	and	((a.cd_procedimento	= cd_procedimento_p ) or (a.cd_procedimento is null))
	and	nvl(a.ie_origem_proced,ie_origem_proced_w) 		= ie_origem_proced_w
	and	nvl(a.cd_grupo_proc,cd_grupo_w)				= cd_grupo_w
	and	nvl(a.cd_especialidade, cd_especialidade_w)		= cd_especialidade_w
	and	nvl(a.cd_area_procedimento, cd_area_w) 			= cd_area_w
	and 	nvl(a.nr_seq_tipo_acomodacao,nr_seq_tipo_acomodacao_w)	= nr_seq_tipo_acomodacao_w
	and 	nvl(a.ie_tipo_guia,ie_tipo_guia_w)			= ie_tipo_guia_w
	and	b.ie_cpt						= 'S'
	and	b.ie_situacao						= 'A'
	and	nvl(c.dt_inicio_vigencia,dt_inclusao_operadora_w)	<= dt_solicitacao_w
	and	(dt_inclusao_operadora_w >= a.dt_inicio_vigencia or a.dt_inicio_vigencia is null)
	and 	(dt_inclusao_operadora_w <= a.dt_fim_vigencia or a.dt_fim_vigencia is null)
	and	((nr_seq_guia_w is not null 
		and	((exists (	select	1
					from	pls_diagnostico x
					where	TRIM(x.cd_doenca)	= TRIM(a.cd_doenca_cid)
					and	x.nr_seq_guia = nr_seq_guia_w)) or (TRIM(a.cd_doenca_cid) is null))
		and	((exists (	select	1
					from	pls_diagnostico x,
						CID_DOENCA	y
					where	x.cd_doenca	= y.cd_doenca_cid
					and	x.nr_seq_guia	= nr_seq_guia_w
					and	TRIM(a.CD_CATEGORIA_CID) = y.CD_CATEGORIA_CID)) or (TRIM(a.CD_CATEGORIA_CID) is null))) /*aaschlote - Coloquei o trim pois colocava um espaco vazio no final*/
	or
		(nr_seq_guia_w is null))
	and	(a.cd_procedimento		is not null
		or a.cd_grupo_proc		is not null
		or a.cd_especialidade		is not null
		or a.cd_area_procedimento	is not null
		or a.nr_seq_grupo_servico	is not null)					
	order by
		a.cd_area_procedimento,
		a.cd_especialidade,
		a.cd_grupo_proc,		
		a.cd_procedimento;

begin

if 	(nvl(nr_seq_guia_proc_p,0)		> 0) then
	ie_tipo_consistencia_w	:= 'G';
elsif 	(nvl(nr_seq_conta_proc_p,0)		> 0) then
	ie_tipo_consistencia_w	:= 'C';
	
	if	(nr_seq_reembolso_proc_p is not null) then
		ie_tipo_consistencia_w := 'CR';
	end if;
	
elsif	(nvl(nr_seq_requisicao_proc_p,0)	> 0) then
	ie_tipo_consistencia_w	:= 'R';
end if;

pls_obter_estrut_proc(cd_procedimento_p, ie_origem_proced_p, cd_area_w,
	cd_especialidade_w, cd_grupo_w, ie_origem_proced_w);
	
if	(dt_solicitacao_p is null) then
	dt_solicitacao_w	:= sysdate;
else
	dt_solicitacao_w	:= dt_solicitacao_p;
end if;

begin
select	trunc(dt_inclusao_operadora,'dd')
into	dt_inclusao_operadora_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;
exception
when others then
	dt_inclusao_operadora_w := '';
end;

if 	(ie_tipo_consistencia_w = 'G') then
	begin
	select 	nr_seq_guia
	into	nr_seq_guia_w
	from	pls_guia_plano_proc
	where	nr_sequencia = nr_seq_guia_proc_p;
	exception
		when others then
		nr_seq_guia_w	:= null;
	end;

	begin
	
	select	nr_seq_prestador,
		nvl(nr_seq_tipo_acomodacao,0),
		nvl(ie_tipo_guia,'')
	into	nr_seq_prestador_w,
		nr_seq_tipo_acomodacao_w,
		ie_tipo_guia_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_w;
	
	exception
	when others then
		nr_seq_prestador_w := null;
	end;
elsif 	(ie_tipo_consistencia_w in ('C','CR')) then
	select	nr_seq_conta
	into	nr_seq_conta_w
	from	pls_conta_proc
	where	nr_sequencia = nr_seq_conta_proc_p;
	
	begin
	select	a.nr_seq_prestador,
		nvl(b.nr_seq_tipo_acomodacao,0)
	into	nr_seq_prestador_w,
		nr_seq_tipo_acomodacao_w
	from	pls_protocolo_conta a,
		pls_conta b
	where	a.nr_sequencia = b.nr_seq_protocolo
	and	b.nr_sequencia = nr_seq_conta_w;
	
	exception
	when others then
		nr_seq_prestador_w := null;
	end;
	
	
elsif 	(ie_tipo_consistencia_w = 'R') then
	select 	nr_seq_requisicao
	into	nr_seq_requisicao_w
	from	pls_requisicao_proc
	where	nr_sequencia	= nr_seq_requisicao_proc_p;

	begin
	
	select	nr_seq_prestador,
		nvl(nr_seq_tipo_acomodacao,0),
		ie_tipo_guia
	into	nr_seq_prestador_w,
		nr_seq_tipo_acomodacao_w,
		ie_tipo_guia_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_w;
	
	exception
	when others then
		nr_seq_prestador_w 		:= null;
		nr_seq_tipo_acomodacao_w	:= 0;
		ie_tipo_guia_w			:= 'X';
	end;
end if;

open c01;
loop
fetch c01 into	
	nr_seq_carencia_proc_w,
	ie_liberado_w,
	qt_dias_w,
	dt_inicio_vigencia_w,
	nr_seq_grupo_servico_w;
exit when c01%notfound;
	ie_grupo_serv_lib_w	:= 'S';
	if	(nr_seq_grupo_servico_w is not null) then
		ie_grupo_serv_lib_w := pls_se_grupo_preco_servico_lib(nr_seq_grupo_servico_w,cd_procedimento_p,ie_origem_proced_p);
	end if;
	if	(ie_grupo_serv_lib_w = 'S') then
		ie_liberado_w	:= ie_liberado_w;
	else
		ie_liberado_w	:= ie_liberado_ant_w;
	end if;
	ie_liberado_ant_w	:= ie_liberado_w;
	
end loop;
close c01;

if	(ie_liberado_w	= 'S') then
	select	(nvl(dt_inicio_vigencia_w,dt_inclusao_operadora_w) + qt_dias_w)
	into	dt_carencia_w
	from	dual;

	if	(nvl(dt_solicitacao_p,sysdate) < dt_carencia_w) then
		if 	(ie_tipo_consistencia_w = 'G') then
			pls_gravar_motivo_glosa(	'1421', null, nr_seq_guia_proc_p,
							null, 'CPT - Liberado a partir de ' || to_char(dt_carencia_w,'dd/mm/yyyy'), nm_usuario_p,
							'B', 'CG', nr_seq_prestador_w, null,
							null);
		elsif 	(ie_tipo_consistencia_w = 'C') then
			pls_gravar_conta_glosa(	'1421',null, nr_seq_conta_proc_p,
						null,'N','CPT - Liberado a partir de '||to_char(dt_carencia_w,'dd/mm/yyyy'),
						nm_usuario_p,'A','CC',
						nr_seq_prestador_w, cd_estabelecimento_p, '', null);
		elsif 	(ie_tipo_consistencia_w = 'R') then
			pls_gravar_requisicao_glosa(	'1421', null, nr_seq_requisicao_proc_p,
							null, 'CPT - Liberado a partir de '||to_char(dt_carencia_w,'dd/mm/yyyy'), nm_usuario_p, 
							nr_seq_prestador_w, cd_estabelecimento_p, null,
							'');
							
		elsif 	(ie_tipo_consistencia_w = 'CR') then
			pls_gravar_conta_glosa(	'1421',null, nr_seq_conta_proc_p,
						null,'N','CPT - Liberado a partir de '||to_char(dt_carencia_w,'dd/mm/yyyy'),
						nm_usuario_p,'A','CR',
						nr_seq_prestador_w, cd_estabelecimento_p, '', null);
		end if;
	end if;
end if;

end pls_consistir_cpt;
/