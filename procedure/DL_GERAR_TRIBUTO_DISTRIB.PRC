create or replace
procedure dl_gerar_tributo_distrib(
			nr_seq_distribuicao_p	number) is 

cd_estabelecimento_w		number(4);
qt_venc_w			number(05,0);
cd_tributo_w			number(10);
qt_dependente_w			Number(10);
nr_ccm_w			number(10) := null;
cd_cond_pagto_w			number(10,0);
cd_conta_financ_w		number(10,0);
nr_seq_trans_reg_w		number(10,0);
nr_seq_trans_baixa_w		number(10,0);
vl_distribuicao_w		number(10,2);
vl_minimo_base_w		number(15,2);
vl_minimo_tributo_w		number(15,2);
vl_teto_base_w			number(15,2);
vl_desc_dependente_w		number(15,2);
vl_base_calculo_w		number(15,2);
vl_inss_w			number(15,2) := 0;
vl_tributo_w			number(15,2);
vl_trib_nao_retido_w		number(15,2);
vl_trib_adic_w			number(15,2);
vl_base_adic_w			number(15,2);
vl_base_nao_retido_w		number(15,2);
vl_base_retido_outro_w		number(15,2);
vl_base_calculo_paga_w		number(15,2);
vl_base_pago_adic_base_w	number(15,2);
vl_soma_trib_nao_retido_w	number(15,2);
vl_soma_base_nao_retido_w	number(15,2);
vl_soma_trib_adic_w		number(15,2);
vl_soma_base_adic_w		number(15,2);
vl_trib_anterior_w		number(15,2);
vl_total_base_w			number(15,2);
vl_reducao_w			number(15,2);
pr_aliquota_w			number(15,4);
vl_trib_acum_w			number(15,2);
ie_irpf_w			varchar2(3);
cd_pessoa_fisica_w		varchar2(10);
cd_beneficiario_w		varchar2(14);
nm_usuario_w			varchar2(15);
cd_darf_w			varchar2(20);
cd_variacao_w			varchar2(50);
ie_acumulativo_w		varchar2(255);
ie_tipo_tributo_w		varchar2(255);
ie_apuracao_piso_w		varchar2(255);
ie_periodicidade_w		varchar2(255);
ds_venc_w			varchar2(2000);
dt_vencimento_w			date;
ds_irrelevante_w		varchar2(4000);
ie_restringe_estab_w		varchar2(1);
nr_seq_classe_w			number(10);
cd_tipo_baixa_neg_w		number(5);
cd_empresa_w			number(4);
nr_seq_regra_irpf_w		regra_calculo_irpf.nr_sequencia%type;
	
cursor c01 is
	select	a.cd_tributo,
		a.ie_tipo_tributo,
		a.ie_apuracao_piso,
		a.ie_restringe_estab
	from	tributo a
	where	a.ie_conta_pagar	= 'S'
	and	a.ie_situacao	= 'A'
	and	((a.ie_pf_pj = 'A') or
		((a.ie_pf_pj = 'PF') and (cd_pessoa_fisica_w is not null)))
	and	nr_ccm_w is null or nvl(a.ie_ccm,'S') = 'S'
	and	nvl(a.ie_super_simples, 'S') = 'S' 
	and	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
	order by decode(a.ie_tipo_tributo, 'INSS',1,2);

begin

select	a.vl_distribuicao,
	b.cd_estabelecimento,
	a.dt_vencimento,
	substr(dl_obter_dados_socio(a.nr_seq_socio, 'PF'),1,10),
	a.nm_usuario,
	c.cd_empresa
into	vl_distribuicao_w,
	cd_estabelecimento_w,
	dt_vencimento_w,
	cd_pessoa_fisica_w,
	nm_usuario_w,
	cd_empresa_w
from	estabelecimento c,
	dl_lote_distribuicao b,
	dl_distribuicao a
where	b.cd_estabelecimento	= c.cd_estabelecimento(+)
and	a.nr_seq_lote	= b.nr_sequencia 
and	a.nr_sequencia	= nr_seq_distribuicao_p;

if	(cd_pessoa_fisica_w is not null) then
	select	max(a.nr_ccm)
	into	nr_ccm_w
	from	pessoa_fisica a
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
end if;

open C01;
loop
fetch C01 into	
	cd_tributo_w,
	ie_tipo_tributo_w,
	ie_apuracao_piso_w,
	ie_restringe_estab_w;
exit when C01%notfound;
	begin
	
	vl_base_calculo_w	:= vl_distribuicao_w;
	pr_aliquota_w		:= 0;
	
	obter_dados_trib_tit_pagar(
			cd_tributo_w,
			cd_estabelecimento_w,
			null,
			cd_pessoa_fisica_w,
			cd_beneficiario_w,
			pr_aliquota_w,
			cd_cond_pagto_w,
			cd_conta_financ_w,
			nr_seq_trans_reg_w,
			nr_seq_trans_baixa_w,
			vl_minimo_base_w,
			vl_minimo_tributo_w,
			ie_acumulativo_w,
			vl_teto_base_w,
			vl_desc_dependente_w,
			cd_darf_w,
			dt_vencimento_w,
			cd_variacao_w,
			ie_periodicidade_w,
			'DL',
			null,
			null,
			null,
			null,
			null,
			ds_irrelevante_w,
			null,
			0,
			nr_seq_classe_w,
			cd_tipo_baixa_neg_w,
			vl_base_calculo_w,
			'N',
			null,
			'0',
			null,
			null);
	
	if	(pr_aliquota_w > 0) then
		
		ie_irpf_w	:= 'N';
		if	(ie_tipo_tributo_w = 'IR') then -- Calcular redu��o base IRPF e saldo menos INSS 

			if 	(cd_pessoa_fisica_w is not null) then

				ie_irpf_w	:= 'S';

				select	nvl(a.qt_dependente,0)
				into	qt_dependente_w
				from	pessoa_fisica a
				where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;
					
				select	nvl(sum(a.vl_tributo),0)
				into	vl_inss_w
				from	tributo b,
					dl_distribuicao_trib a
				where	a.cd_tributo		= b.cd_tributo
				and	b.ie_tipo_tributo	= 'INSS'
				and	a.nr_seq_distribuicao	= nr_seq_distribuicao_p;

			end if;

			vl_base_calculo_w	:= vl_base_calculo_w - vl_inss_w;  
		end if;		
		
		vl_tributo_w	:= vl_base_calculo_w * pr_aliquota_w / 100;
		
		if	(dt_vencimento_w is null) then
			dt_vencimento_w	:= sysdate;
		end if;
		
		if	(cd_cond_pagto_w is not null) then

			calcular_vencimento(cd_estabelecimento_w,cd_cond_pagto_w,dt_vencimento_w,qt_venc_w,ds_venc_w);

			if	(qt_venc_w = 1) then	
				dt_vencimento_w	:= to_date(substr(ds_venc_w,1,10),'dd/mm/yyyy');
			end if;

		end if;

		select	/*+ USE_CONCAT */ -- Edgar 08/09/2009, OS 160898, coloquei o use_concat para transformar os OR em UNION
			nvl(sum(vl_soma_trib_nao_retido),0),
			nvl(sum(vl_soma_base_nao_retido),0),
			nvl(sum(vl_soma_trib_adic),0),
			nvl(sum(vl_soma_base_adic),0),
			nvl(sum(vl_tributo),0),
			nvl(sum(vl_total_base),0),
			nvl(sum(vl_reducao),0)
		into	vl_soma_trib_nao_retido_w,
			vl_soma_base_nao_retido_w,
			vl_soma_trib_adic_w,
			vl_soma_base_adic_w,
			vl_trib_anterior_w,
			vl_total_base_w,
			vl_reducao_w
		from	valores_tributo_v
		where	cd_tributo			= cd_tributo_w
		and	(ie_origem_valores		= 'RT'  or ie_apuracao_piso_w = 'S')
		--and	nvl(cd_pessoa_fisica, decode(ie_cnpj_w, 'Estab', cd_cgc, cd_cnpj_raiz))	= nvl(cd_pessoa_fisica_w, decode(ie_cnpj_w, 'Estab', cd_cgc_repasse_w, cd_cnpj_raiz_w))
		and	cd_pessoa_fisica		= cd_pessoa_fisica_w
		and	trunc(dt_tributo, 'month')	= trunc(dt_vencimento_w, 'month')
		and	((ie_restringe_estab_w	= 'N') or
			(cd_estabelecimento		= cd_estabelecimento_w) or
			(cd_estab_financeiro	= cd_estabelecimento_w))
		and	(ie_apuracao_piso_w		= 'N' or
			ie_apuracao_piso_w		= ie_base_calculo)
		and	ie_baixa_titulo			= 'N'
		and	nvl(cd_empresa,nvl(cd_empresa_w,0)) = nvl(cd_empresa_w,0);

		vl_base_calculo_paga_w		:= 0;
		vl_base_retido_outro_w		:= 0;
		vl_base_pago_adic_base_w	:= 0;
		
		obter_valores_tributo(	
			ie_acumulativo_w,
			pr_aliquota_w,
			vl_minimo_base_w,
			vl_minimo_tributo_w,
			vl_soma_trib_nao_retido_w,
			vl_soma_trib_adic_w,
			vl_soma_base_nao_retido_w,
			vl_soma_base_adic_w,
			vl_base_calculo_w,
			vl_tributo_w,
			vl_trib_nao_retido_w,
			vl_trib_adic_w,
			vl_base_nao_retido_w,
			vl_base_adic_w,
			vl_teto_base_w,
			vl_trib_anterior_w,
			ie_irpf_w,
			vl_total_base_w,
			vl_reducao_w,
			vl_desc_dependente_w,
			qt_dependente_w,
			vl_base_calculo_paga_w,
			vl_base_pago_adic_base_w,
			vl_base_retido_outro_w,
			obter_outras_reducoes_irpf(cd_pessoa_fisica_w,cd_estabelecimento_w,dt_vencimento_w),
			sysdate,
			nr_seq_regra_irpf_w);
		
		if	(vl_tributo_w >= 0) then
			
			insert into dl_distribuicao_trib (	
				nr_sequencia,           
				dt_atualizacao,         
				nm_usuario,             
				dt_atualizacao_nrec,    
				nm_usuario_nrec,        
				nr_seq_distribuicao,    
				dt_tributo,             
				cd_tributo,             
				pr_tributo,             
				vl_tributo,             
				vl_base_calculo,        
				vl_nao_retido,          
				vl_base_nao_retido,    
				vl_trib_adic,           
				vl_base_adic,           
				vl_reducao,             
				vl_desc_base,           
				cd_darf,                
				cd_variacao,            
				ie_periodicidade,       
				cd_cgc_beneficiario,    
				cd_conta_financ)
			values(	dl_distribuicao_trib_seq.nextval,           
				sysdate,         
				nm_usuario_w,             
				sysdate,    
				nm_usuario_w,        
				nr_seq_distribuicao_p,    
				dt_vencimento_w,             
				cd_tributo_w,             
				pr_aliquota_w,             
				vl_tributo_w,             
				vl_base_calculo_w,        
				vl_trib_nao_retido_w,          
				vl_base_nao_retido_w,     
				vl_trib_adic_w,           
				vl_base_adic_w,           
				vl_reducao_w,             
				vl_desc_dependente_w,           
				cd_darf_w,                
				cd_variacao_w,            
				ie_periodicidade_w,       
				cd_beneficiario_w,    
				cd_conta_financ_w);
			
			vl_trib_acum_w	:= vl_trib_acum_w + vl_tributo_w;
		end if;
		
	end if;
	
	end;
end loop;
close C01;

update	dl_distribuicao
set	vl_calculado	= nvl(vl_distribuicao,0) - nvl(vl_trib_acum_w,0)
where	nr_sequencia	= nr_seq_distribuicao_p;

commit;

end dl_gerar_tributo_distrib;
/