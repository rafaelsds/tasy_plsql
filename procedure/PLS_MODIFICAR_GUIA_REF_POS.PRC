create or replace
procedure pls_modificar_guia_ref_pos(	cd_guia_p		pls_analise_conta.cd_guia%type,
					nr_seq_analise_p	pls_analise_conta.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type) is 
qt_benef_iguais_w	pls_integer;
					
begin

--Verifica se o benefici�rio e a cong�nere s�o os mesmos entre a an�lise e a guia que est� tentando vincular
select 	count(1)
into	qt_benef_iguais_w	
from   	pls_conta_v a
where  	nr_seq_analise = nr_seq_analise_p
and    	exists(select  1
		from   pls_conta_v b
		where  b.cd_guia = cd_guia_p
		and    b.nr_seq_segurado = a.nr_seq_segurado 
		and    b.nr_seq_congenere_prot = a.nr_seq_congenere_prot);

--Benefici�rios e/ou cong�neres diferentes entre an�lise e guia da an�lise p�s que se tenta referenciar
if	(qt_benef_iguais_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(354704);
end if;

update	pls_conta 
set 	cd_guia_pos_estab = cd_guia_p
where	nr_seq_analise 	= nr_seq_analise_p;	

--Se n�o apagar a refer�ncia de an�lise anterior, n�o chegar� a gerar uma nova analise p�s estab.
update	pls_analise_conta
set	nr_seq_analise_ref = null
where	nr_seq_analise_ref = nr_seq_analise_p
and	ie_origem_analise  = 2; 	

commit;

end pls_modificar_guia_ref_pos;
/
