create or replace
procedure ptu_gerar_prestador_observacao(	nr_seq_prestador_p	ptu_prestador.nr_sequencia%type,
						dt_referencia_p		ptu_movimento_prestador.dt_referencia%type,
						nm_usuario_p		usuario.nm_usuario%type ) is 

Cursor C01	(nr_seq_prestador_pc	ptu_prestador.nr_sequencia%type,
		dt_referencia_pc	ptu_movimento_prestador.dt_referencia%type) is
	select	substr(c.ds_observacao,1,250) ds_observacao
	from	ptu_prestador		 a,
		pls_prestador		 b,
		pls_prestador_observacao c
	where	b.nr_sequencia	= a.nr_seq_prestador
	and	b.nr_sequencia	= c.nr_seq_prestador
	and	a.nr_sequencia	= nr_seq_prestador_pc
	and	dt_referencia_pc between trunc(nvl(c.dt_inicio_vigencia, dt_referencia_pc)) and fim_dia(nvl(c.dt_fim_vigencia, dt_referencia_pc));
	
begin

-- R410 – OBSERVAÇÕES (OPCIONAL)
for r_c01_w in c01( nr_seq_prestador_p, dt_referencia_p ) loop

	insert into ptu_prestador_observacao
		(nr_sequencia,				dt_atualizacao,		nm_usuario,
		dt_atualizacao_nrec,			nm_usuario_nrec,	nr_seq_prestador,
		ds_divulgacao_obs)
	values	(ptu_prestador_observacao_seq.nextval,	sysdate,		nm_usuario_p,
		sysdate,				nm_usuario_p,		nr_seq_prestador_p,
		r_c01_w.ds_observacao);
end loop;

end ptu_gerar_prestador_observacao;
/