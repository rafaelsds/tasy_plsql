create or replace
procedure HDH_INTEGRACAO_SICSUL
			(nr_seq_cobranca_p	number,
			dt_inicial_p		date,
			dt_final_p		date,
	 		nm_usuario_p		varchar2,
			cd_estabelecimento_p	number,
			ie_tipo_data_p		number,
			cd_tipo_portador_p	number,
			cd_portador_p		number) is

ds_conteudo_w		varchar2(4000);
nr_seq_cheque_w		number(10);
nr_titulo_w		number(10);
cd_agencia_bancaria_w	varchar2(8);
cd_banco_w		number(5);
cd_cgc_w		varchar2(14);
cd_pessoa_fisica_w	varchar2(10);
nm_pessoa_w		varchar2(255);
ds_endereco_w		varchar2(255);
ds_complemento_w	varchar2(255);
ds_bairro_w		varchar2(255);
cd_cep_w		varchar2(255);
ds_cidade_w		varchar2(255);
sg_estado_w		varchar2(2);
nr_telefone_w		varchar2(255);
nr_telefone_celular_w	varchar2(40);
nr_cpf_w		varchar2(11);
ds_endereco_com_w	varchar2(255);

ds_bairro_com_w		varchar2(255);
cd_cep_com_w		varchar2(255);
ds_cidade_com_w		varchar2(255);
sg_estado_com_w		varchar2(2);
nr_telefone_com_w	varchar2(255);
nm_empresa_w		varchar2(100);
nm_responsavel_w	varchar2(60);
nm_pai_w		varchar2(60);
nm_mae_w		varchar2(60);
dt_nascimento_w		date;
nr_identidade_w		varchar2(15);
ds_email_w		varchar2(255);
dt_vencimento_w		date;
vl_documento_w		number(15,2);
tx_juros_w		number(7,4);
tx_multa_w		number(7,4);
ie_tipo_documento_w	varchar2(1);
nr_cheque_w		varchar2(20);
dt_emissao_w		date;
ds_observacao_w		varchar2(25);
vl_desconto_w		number(15,2);
nr_prontuario_w		number(10);
nr_sequencia_w		number(10);

cursor	c02 is
select	distinct
	nr_seq_cheque,
	nr_titulo,
	vl_acobrar
from	(select	/*+ USE_CONCAT */
		b.nr_seq_cheque,
		null nr_titulo,
		a.vl_acobrar
	from	cheque_cr b,
		cobranca a
	where	a.nr_seq_cheque	= b.nr_seq_cheque
	and	(0 = nvl(cd_portador_p,0) or a.cd_portador = cd_portador_p)
	and	(0 = nvl(cd_tipo_portador_p,0) or a.cd_tipo_portador = cd_tipo_portador_p)
	and	((0 = ie_tipo_data_p) or
		(1 = ie_tipo_data_p and a.dt_previsao_cobranca between dt_inicial_p and fim_dia(dt_final_p)) or
		(2 = nvl(ie_tipo_data_p,2) and a.dt_inclusao between dt_inicial_p and fim_dia(dt_final_p)) or
		(3 = ie_tipo_data_p and to_date(obter_dados_titulo_receber(a.nr_titulo,'DV'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)) or
		(4 = ie_tipo_data_p and to_date(obter_dados_titulo_receber(a.nr_titulo,'PP'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)) or
		(5 = ie_tipo_data_p and to_date(obter_dados_cheque_cr(a.nr_seq_cheque,'DB'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)) or
		(6 = ie_tipo_data_p and to_date(obter_dados_cheque_cr(a.nr_seq_cheque,'DP'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)) or
		(7 = ie_tipo_data_p and exists
					(select	1
					from	cobranca_historico x
					where	x.dt_historico		between dt_inicial_p and fim_dia(dt_final_p)
					and	x.nr_seq_cobranca	= a.nr_sequencia)) or
		(8 = ie_tipo_data_p and to_date(obter_dados_titulo_receber(a.nr_titulo,'D'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)))
	and	(a.nr_sequencia	= nvl(nr_seq_cobranca_p,a.nr_sequencia) or nr_seq_cobranca_p = 0)
	union
	select	/*+ USE_CONCAT */
		null nr_seq_cheque,
		b.nr_titulo,
		a.vl_acobrar
	from	titulo_receber b,
		cobranca a
	where	a.nr_titulo	= b.nr_titulo
	and	(0 = nvl(cd_portador_p,0) or a.cd_portador = cd_portador_p)
	and	(0 = nvl(cd_tipo_portador_p,0) or a.cd_tipo_portador = cd_tipo_portador_p)
	and	((0 = ie_tipo_data_p) or
		(1 = ie_tipo_data_p and a.dt_previsao_cobranca between dt_inicial_p and fim_dia(dt_final_p)) or
		(2 = nvl(ie_tipo_data_p,2) and a.dt_inclusao between dt_inicial_p and fim_dia(dt_final_p)) or
		(3 = ie_tipo_data_p and to_date(obter_dados_titulo_receber(a.nr_titulo,'DV'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)) or
		(4 = ie_tipo_data_p and to_date(obter_dados_titulo_receber(a.nr_titulo,'PP'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)) or
		(5 = ie_tipo_data_p and to_date(obter_dados_cheque_cr(a.nr_seq_cheque,'DB'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)) or
		(6 = ie_tipo_data_p and to_date(obter_dados_cheque_cr(a.nr_seq_cheque,'DP'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)) or
		(7 = ie_tipo_data_p and exists
					(select	1
					from	cobranca_historico x
					where	x.dt_historico		between dt_inicial_p and fim_dia(dt_final_p)
					and	x.nr_seq_cobranca	= a.nr_sequencia)) or
		(8 = ie_tipo_data_p and to_date(obter_dados_titulo_receber(a.nr_titulo,'D'),'dd/mm/yyyy') between dt_inicial_p and fim_dia(dt_final_p)))
	and	(a.nr_sequencia	= nvl(nr_seq_cobranca_p,a.nr_sequencia) or nr_seq_cobranca_p = 0));

begin

delete	from w_cr_topazio;

select	max(b.cd_banco),
	max(b.cd_agencia_bancaria)
into	cd_banco_w,
	cd_agencia_bancaria_w
from	pessoa_juridica_conta b,
	estabelecimento a
where	a.cd_cgc		= b.cd_cgc
and	a.cd_estabelecimento	= cd_estabelecimento_p;

select	sicsul_seq.nextval
into	nr_sequencia_w
from	dual;

/* Registro Tipo 0 - Dados do Cliente, Loja e Border� */
ds_conteudo_w	:=	'0          ' || to_char(sysdate,'yyyymmdd') || lpad(nvl(nr_sequencia_w,0),2,'0') || lpad(nvl(cd_agencia_bancaria_w,0),4,'0') || lpad(nvl(cd_banco_w,0),3,'0') ||
			'          SIC2                                                                                                                                                                                                                                                                                                                                                                                                               ';

insert	into w_cr_topazio
	(ds_conteudo,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	nr_sequencia)
values	(ds_conteudo_w,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	w_cr_topazio_seq.nextval);

open	c02;
loop
fetch	c02 into
	nr_seq_cheque_w,
	nr_titulo_w,
	vl_documento_w;
exit	when c02%notfound;

	if	(nr_seq_cheque_w	is not null) then

		select	max(a.cd_agencia_bancaria),
			max(a.cd_banco),
			max(a.cd_cgc),
			max(a.cd_pessoa_fisica),
			max(nvl(a.dt_vencimento_atual,nvl(a.dt_vencimento,a.dt_contabil))),
			max(a.tx_juros_cobranca),
			max(a.tx_multa_cobranca),
			max(a.nr_cheque),
			max(a.dt_registro),
			substr(max(a.ds_observacao),1,25)
		into	cd_agencia_bancaria_w,
			cd_banco_w,
			cd_cgc_w,
			cd_pessoa_fisica_w,
			dt_vencimento_w,
			tx_juros_w,
			tx_multa_w,
			nr_cheque_w,
			dt_emissao_w,
			ds_observacao_w
		from	cheque_cr a
		where	a.nr_seq_cheque	= nr_seq_cheque_w;

		ie_tipo_documento_w	:= '0';

	elsif	(nr_titulo_w		is not null) then

		select	max(a.cd_agencia_bancaria),
			max(a.cd_banco),
			max(a.cd_cgc),
			max(a.cd_pessoa_fisica),
			max(a.dt_pagamento_previsto),
			max(a.tx_juros),
			max(a.tx_multa),
			max(a.dt_emissao),
			substr(max(a.ds_observacao_titulo),1,25),
			max(a.vl_desc_previsto)
		into	cd_agencia_bancaria_w,
			cd_banco_w,
			cd_cgc_w,
			cd_pessoa_fisica_w,
			dt_vencimento_w,
			tx_juros_w,
			tx_multa_w,
			dt_emissao_w,
			ds_observacao_w,
			vl_desconto_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_w;

		ie_tipo_documento_w	:= '5';

	else

		cd_agencia_bancaria_w	:= null;
		cd_banco_w		:= null;
		cd_cgc_w		:= null;
		cd_pessoa_fisica_w	:= null;
		dt_vencimento_w		:= null;
		vl_documento_w		:= null;
		tx_juros_w		:= null;
		tx_multa_w		:= null;
		dt_emissao_w		:= null;
		ds_observacao_w		:= null;
		nr_cheque_w		:= null;
		vl_desconto_w		:= null;
		ie_tipo_documento_w	:= '6';

	end if;

	nm_pessoa_w	:= substr(obter_nome_pf_pj(cd_pessoa_fisica_w,cd_cgc_w),1,255);

	/* Residencial */
	select	max(ds_endereco),
		max(ds_complemento),
		max(ds_bairro),
		max(cd_cep),
		max(ds_municipio),
		max(sg_estado),
		max(nr_telefone),
		max(ds_email)
	into	ds_endereco_w,
		ds_complemento_w,
		ds_bairro_w,
		cd_cep_w,
		ds_cidade_w,
		sg_estado_w,
		nr_telefone_w,
		ds_email_w
	from	(select	a.ds_endereco || decode(a.nr_endereco,null,null,', ' || a.nr_endereco) ds_endereco,
			a.ds_complemento,
			a.ds_bairro,
			a.cd_cep,
			a.ds_municipio,
			a.sg_estado,
			decode(a.nr_ddd_telefone,null,null,'(' || a.nr_ddd_telefone || ') ') || a.nr_telefone nr_telefone,
			a.ds_email
		from	compl_pessoa_fisica a
		where	a.ie_tipo_complemento	= 1
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
		union
		select	a.ds_endereco || decode(a.nr_endereco,null,null,', ' || a.nr_endereco) ds_endereco,
			a.ds_complemento,
			a.ds_bairro,
			a.cd_cep,
			a.ds_municipio,
			a.sg_estado,
			decode(a.nr_ddd_telefone,null,null,'(' || a.nr_ddd_telefone || ') ') || a.nr_telefone nr_telefone,
			a.ds_email
		from	pessoa_juridica a
		where	a.cd_cgc		= cd_cgc_w);

	/* Comercial */
	select	max(ds_endereco),
		max(ds_bairro),
		max(cd_cep),
		max(ds_municipio),
		max(sg_estado),
		max(nr_telefone),
		max(nm_empresa)
	into	ds_endereco_com_w,
		ds_bairro_com_w,
		cd_cep_com_w,
		ds_cidade_com_w,
		sg_estado_com_w,
		nr_telefone_com_w,
		nm_empresa_w
	from	(select	a.ds_endereco || decode(a.nr_endereco,null,null,', ' || a.nr_endereco) ds_endereco,
			a.ds_bairro,
			a.cd_cep,
			a.ds_municipio,
			a.sg_estado,
			decode(a.nr_ddd_telefone,null,null,'(' || a.nr_ddd_telefone || ') ') || a.nr_telefone nr_telefone,
			b.nm_empresa nm_empresa
		from	empresa_referencia b,
			compl_pessoa_fisica a
		where	a.cd_empresa_refer	= b.cd_empresa(+)
		and	a.ie_tipo_complemento	= 2
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w
		union
		select	a.ds_endereco || decode(a.nr_endereco,null,null,', ' || a.nr_endereco) ds_endereco,
			a.ds_bairro,
			a.cd_cep,
			a.ds_municipio,
			a.sg_estado,
			decode(a.nr_ddd_telefone,null,null,'(' || a.nr_ddd_telefone || ') ') || a.nr_telefone nr_telefone,
			a.nm_fantasia nm_empresa
		from	pessoa_juridica a
		where	a.cd_cgc		= cd_cgc_w);

	/* Pai */
	select	max(a.nm_contato)
	into	nm_pai_w
	from	compl_pessoa_fisica a
	where	a.ie_tipo_complemento	= 4
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;

	/* M�e */
	select	max(a.nm_contato)
	into	nm_mae_w
	from	compl_pessoa_fisica a
	where	a.ie_tipo_complemento	= 5
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;

	/* Respons�vel */
	select	max(a.nm_contato)
	into	nm_responsavel_w
	from	compl_pessoa_fisica a
	where	a.ie_tipo_complemento	= 3
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;

	if	(nm_responsavel_w is null) then
		nm_responsavel_w	:= nvl(nm_pai_w,nm_mae_w);
	end if;

	select	max(a.nr_telefone_celular),
		max(a.nr_cpf),
		max(a.dt_nascimento),
		max(a.nr_identidade),
		max(a.nr_prontuario)
	into	nr_telefone_celular_w,
		nr_cpf_w,
		dt_nascimento_w,
		nr_identidade_w,
		nr_prontuario_w
	from	pessoa_fisica a
	where	a.cd_pessoa_fisica	= cd_pessoa_fisica_w;

	/* Registro Tipo 2 - Dados do Devedor */
	ds_conteudo_w	:=	'2' || lpad(nvl(nr_prontuario_w,0),16,'0') || rpad(upper(nm_pessoa_w),50,' ') || rpad(nvl(upper(ds_endereco_w),' '),40,' ') ||
				rpad(nvl(upper(ds_complemento_w),' '),20,' ') || rpad(nvl(upper(ds_bairro_w),' '),20,' ') || rpad(nvl(cd_cep_w,' '),8,' ') ||
				rpad(nvl(upper(ds_cidade_w),' '),30,' ') || rpad(nvl(upper(sg_estado_w),' '),2,' ') || rpad(nvl(nr_telefone_w,' '),15,' ') ||
				lpad(nvl(nr_telefone_celular_w,0),15,0) || rpad(nvl(nr_cpf_w,nvl(cd_cgc_w,' ')),14,' ') || rpad(nvl(upper(ds_endereco_com_w),' '),40,' ') ||
				rpad(nvl(upper(ds_bairro_w),' '),20,' ') || rpad(nvl(cd_cep_w,' '),8,' ') || rpad(nvl(upper(ds_cidade_com_w),' '),30,' ') ||
				rpad(nvl(upper(sg_estado_com_w),' '),2,' ') || rpad(nvl(nr_telefone_com_w,' '),15,' ') || rpad(nvl(upper(nm_empresa_w),' '),20,' ') ||
				lpad(nvl(nr_prontuario_w,0),12,'0') || '                                        ' || rpad(nvl(upper(nm_responsavel_w),' '),40,' ') ||
				rpad(nvl(upper(nm_pai_w),' '),45,' ') || rpad(nvl(upper(nm_mae_w),' '),45,' ') || to_char(dt_nascimento_w,'ddmmyyyy') ||
				lpad(nvl(nr_identidade_w,0),10,'0') || rpad(nvl(ds_email_w,' '),100,' ') || '                                                                                                                                      ';

	insert	into w_cr_topazio
		(ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia)
	values	(ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_cr_topazio_seq.nextval);

	/* Registro Tipo 8 - Dados do Lan�amento */
	ds_conteudo_w	:=	'8' || lpad(nvl(nr_prontuario_w,0),16,'0') || nvl(to_char(dt_vencimento_w,'yyyymmdd'),'00000000') || lpad(nvl(vl_documento_w,0) * 100,15,'0') ||
				lpad(nvl(tx_juros_w,0) * 100,6,'0') || lpad(nvl(tx_multa_w,0) * 100,6,'0') || ie_tipo_documento_w || rpad(nvl(to_char(nr_titulo_w),nvl(nr_cheque_w,' ')),13,' ') ||
				'001' || nvl(to_char(dt_emissao_w,'yyyymmdd'),'00000000') || rpad(nvl(replace(replace(ds_observacao_w,chr(13),' '),chr(10),''),' '),25,' ') || lpad(nvl(vl_desconto_w,0) * 100,8,'0') ||
				lpad(nvl(vl_desconto_w,0) * 100,8,'0') || '00000000' || '000000000' || '                                                                                                                                                                                                                                                          ';

	insert	into w_cr_topazio
		(ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia)
	values	(ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		w_cr_topazio_seq.nextval);

end	loop;
close	c02;

commit;

end HDH_INTEGRACAO_SICSUL;
/