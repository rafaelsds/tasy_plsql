create or replace
procedure pls_copia_parametro_congenere
		(	nr_seq_con_origem_p	Number,
			nr_seq_con_destino_p	Number,
			nm_tabela_p		Varchar2,
			ie_excluir_p		Varchar2,
			nm_usuario_p		Varchar2) is

begin

/* PLS_REGRA_PRECO_PROC */
if	(nm_tabela_p	= 'PLS_REGRA_PRECO_PROC') then
	if	(ie_excluir_p = 'S') then
		delete	from pls_regra_preco_proc
		where 	nr_seq_congenere = nr_seq_con_destino_p;
	end if;
	insert into pls_regra_preco_proc
		(nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_congenere,
		nr_seq_plano, nr_seq_categoria, nr_seq_clinica, nr_seq_tipo_acomodacao, nr_seq_tipo_atendimento, cd_edicao_amb,
		cd_procedimento, ie_origem_proced, cd_area_procedimento, cd_especialidade, cd_grupo_proc, tx_ajuste_geral,
		vl_ch_honorarios, vl_ch_custo_oper, vl_filme, tx_ajuste_custo_oper, tx_ajuste_partic, vl_proc_negociado,
		ie_situacao, dt_inicio_vigencia, cd_estabelecimento, dt_fim_vigencia, tx_ajuste_filme, tx_ajuste_ch_honor,
		ie_tipo_tabela,nr_seq_ops_congenere,ie_tipo_contrato, ie_tipo_consulta, nr_seq_grupo_med_exec, ie_gerar_remido)
	select	pls_regra_preco_proc_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p, nr_seq_con_destino_p,
		nr_seq_plano, nr_seq_categoria, nr_seq_clinica, nr_seq_tipo_acomodacao, nr_seq_tipo_atendimento, cd_edicao_amb,
		cd_procedimento, ie_origem_proced, cd_area_procedimento, cd_especialidade, cd_grupo_proc, tx_ajuste_geral,
		vl_ch_honorarios, vl_ch_custo_oper, vl_filme, tx_ajuste_custo_oper, tx_ajuste_partic, vl_proc_negociado,
		ie_situacao, dt_inicio_vigencia, cd_estabelecimento, dt_fim_vigencia, tx_ajuste_filme, tx_ajuste_ch_honor,
		ie_tipo_tabela,nr_seq_ops_congenere,ie_tipo_contrato, ie_tipo_consulta, nr_seq_grupo_med_exec, ie_gerar_remido
	from	pls_regra_preco_proc
	where	nr_seq_congenere	= nr_seq_con_origem_p;
end if;

/* PLS_REGRA_PRECO_MAT */
if	(nm_tabela_p	= 'PLS_REGRA_PRECO_MAT') then
	if	(ie_excluir_p = 'S') then
		delete	from pls_regra_preco_mat
		where 	nr_seq_congenere = nr_seq_con_destino_p;
	end if;
	insert into pls_regra_preco_mat
		(nr_sequencia, cd_estabelecimento, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nr_seq_congenere, dt_inicio_vigencia, dt_fim_vigencia, tx_ajuste, ie_situacao, vl_negociado, 
		tx_ajuste_pfb, tx_ajuste_pmc_neut, tx_ajuste_pmc_pos, tx_ajuste_pmc_neg, tx_ajuste_simpro_pfb, tx_ajuste_simpro_pmc, 
		ie_origem_preco, ie_tipo_despesa, nr_seq_material_preco, tx_ajuste_tab_propria, ie_tipo_tabela, ie_gerar_remido)
	select	pls_regra_preco_mat_seq.nextval, cd_estabelecimento, sysdate, nm_usuario_p, sysdate, nm_usuario_p, 
		nr_seq_con_destino_p, dt_inicio_vigencia, dt_fim_vigencia, tx_ajuste, ie_situacao, vl_negociado,
		tx_ajuste_pfb, tx_ajuste_pmc_neut, tx_ajuste_pmc_pos, tx_ajuste_pmc_neg, tx_ajuste_simpro_pfb, tx_ajuste_simpro_pmc, 
		ie_origem_preco, ie_tipo_despesa, nr_seq_material_preco, tx_ajuste_tab_propria, ie_tipo_tabela, ie_gerar_remido
	from	pls_regra_preco_mat
	where	nr_seq_congenere	= nr_seq_con_origem_p;
end if;

/* PLS_REGRA_PRECO_SERVICO */
if	(nm_tabela_p	= 'PLS_REGRA_PRECO_SERVICO') then
	if	(ie_excluir_p = 'S') then
		delete	from pls_regra_preco_servico
		where 	nr_seq_congenere = nr_seq_con_destino_p;
	end if;
	insert into pls_regra_preco_servico
		(nr_sequencia, cd_estabelecimento, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nr_seq_congenere, dt_inicio_vigencia, dt_fim_vigencia, cd_tabela_servico, tx_ajuste, cd_procedimento,
		ie_origem_proced, ie_situacao, cd_area_procedimento, cd_especialidade, cd_grupo_proc, vl_negociado,
		ie_tipo_tabela, ie_gerar_remido)
	select	pls_regra_preco_servico_seq.nextval, cd_estabelecimento, sysdate, nm_usuario_p, sysdate, nm_usuario_p, 
		nr_seq_con_destino_p, dt_inicio_vigencia, dt_fim_vigencia, cd_tabela_servico, tx_ajuste, cd_procedimento,
		ie_origem_proced, ie_situacao, cd_area_procedimento, cd_especialidade, cd_grupo_proc, vl_negociado,
		ie_tipo_tabela, ie_gerar_remido
	from	pls_regra_preco_servico
	where	nr_seq_congenere	= nr_seq_con_origem_p;
end if;

/*PLS_REGRA_INTERCAMBIO*/
if	(nm_tabela_p	= 'PLS_REGRA_INTERCAMBIO') then
	if	(ie_excluir_p = 'S') then
		delete	from pls_regra_intercambio
		where 	nr_seq_congenere = nr_seq_con_destino_p;
	end if;
	insert into pls_regra_intercambio
		(nr_sequencia, nr_seq_congenere, dt_inicio_vigencia, dt_fim_vigencia, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		pr_taxa, vl_taxa, nr_seq_plano, nr_seq_grupo_congenere, ie_beneficio_obito,ie_pcmso,
		ie_tipo_intercambio)
	select	pls_regra_intercambio_seq.nextval, nr_seq_con_destino_p, dt_inicio_vigencia, dt_fim_vigencia,
		sysdate, nm_usuario_p, sysdate, nm_usuario_p,
		pr_taxa, vl_taxa, nr_seq_plano, nr_seq_grupo_congenere, ie_beneficio_obito,ie_pcmso,
		ie_tipo_intercambio
	from	pls_regra_intercambio
	where	nr_seq_congenere	= nr_seq_con_origem_p;
end if;

/*PLS_REGRA_INTERCAMBIO*/
if	(nm_tabela_p	= 'PLS_REGRA_PCMSO') then
	if	(ie_excluir_p = 'S') then
		delete	from pls_regra_pcmso
		where 	nr_seq_congenere = nr_seq_con_destino_p;
	end if;
	insert into pls_regra_pcmso
		(nr_sequencia, nr_seq_congenere, dt_inicio_vigencia, dt_fim_vigencia, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		pr_taxa, vl_taxa)
	select	pls_regra_intercambio_seq.nextval, nr_seq_con_destino_p, dt_inicio_vigencia, dt_fim_vigencia,
		sysdate, nm_usuario_p, sysdate, nm_usuario_p,
		pr_taxa, vl_taxa
	from	pls_regra_pcmso
	where	nr_seq_congenere	= nr_seq_con_origem_p;
end if;

commit;

end pls_copia_parametro_congenere;
/
