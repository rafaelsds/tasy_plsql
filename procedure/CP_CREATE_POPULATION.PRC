create or replace procedure cp_create_population (
	nr_seq_care_plan_p	cp_population.nr_seq_care_plan%type,
	ie_population_p		cp_population.ie_population%type,
	nm_usuario_p		cp_population.nm_usuario%type
) is

nr_sequencia_w			cp_population.nr_sequencia%type;
ie_population_w			cp_population.ie_population%type;
si_record_changed_w		cp_population.si_record_changed%type;
nr_seq_old_care_plan_w		care_plan.nr_sequencia%type;

begin

select	get_old_care_plan(nr_seq_care_plan_p)
into	nr_seq_old_care_plan_w
from 	dual;

if (nr_seq_old_care_plan_w is not null) then

	select	decode(count(1), 0, 'Y', 'N')
	into	si_record_changed_w
	from	cp_population
	where	nr_seq_care_plan = nr_seq_old_care_plan_w
	and		ie_population = ie_population_p;

end if;

select	cp_population_seq.nextval
into	nr_sequencia_w
from	dual;

insert into cp_population (
	nr_sequencia,
	nr_seq_care_plan,
	ie_population,
	ie_origin,
	si_import_status,
	ie_situacao,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	si_record_changed
) values (
	nr_sequencia_w,
	nr_seq_care_plan_p,
	ie_population_p,
	'E', -- From Elsevier
	'SP',
	'I',
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nvl(si_record_changed_w, 'N')
);

if (nr_seq_old_care_plan_w is not null) then

	update	cp_population
	set		si_record_changed = 'N'
	where 	ie_population = ie_population_p
	and		nr_seq_care_plan = nr_seq_old_care_plan_w;

end if;
end cp_create_population;
/
