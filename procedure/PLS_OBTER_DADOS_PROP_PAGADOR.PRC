create or replace
procedure pls_obter_dados_prop_pagador
			(cd_pessoa_fisica_p		varchar2,
			cd_cgc_p			varchar2,
			nr_seq_dia_venc_p		number,
			cd_estabelecimento_p		varchar2,
			ds_email_p		out	varchar2,
			dt_dia_vencimento_p	out	number) is

begin

if	(cd_pessoa_fisica_p is not null) or
	(cd_cgc_p is not null) then
	select	obter_dados_pf_pj_estab(cd_estabelecimento_p, cd_pessoa_fisica_p, cd_cgc_p, 'M')
	into	ds_email_p
	from	dual;
end if;

if	(nr_seq_dia_venc_p is not null) then
	select	dt_dia_vencimento
	into	dt_dia_vencimento_p
	from	pls_regra_dia_vencimento
	where	nr_sequencia = nr_seq_dia_venc_p;
else
	dt_dia_vencimento_p	:= 0;
end if;

end pls_obter_dados_prop_pagador;
/