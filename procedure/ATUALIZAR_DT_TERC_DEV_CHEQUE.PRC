create or replace
procedure atualizar_dt_terc_dev_cheque(	dt_terc_devolucao_p	date,
					nr_seq_cheque_p		number,
					nm_usuario_p		varchar2) is 

begin
if	(dt_terc_devolucao_p is not null) and
	(nr_seq_cheque_p is not null) and
	(nm_usuario_p is not null) then
	begin 
	update 	cheque_cr
	set    	dt_terc_devolucao = dt_terc_devolucao_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where  	nr_seq_cheque = nr_seq_cheque_p;	
	end;
end if;
commit;

end atualizar_dt_terc_dev_cheque;
/
