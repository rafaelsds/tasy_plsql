create or replace
procedure consiste_usuario_plano(	cd_estabelecimento_p	number,
					cd_convenio_p		number,
					cd_plano_p		varchar2,
					cd_usuario_convenio_p	varchar2,
					ie_usuario_plano_p	out varchar2,
					ie_regra_bloqueio_p	out varchar2,
					ds_plano_regra_p		out varchar2) is

ds_retorno_w		varchar2(2)	:= 'S';

qt_registro_w		number(15);
nr_pos_inicial_w	number(3);
nr_pos_final_w		number(3);
cd_usuario_padrao_w	varchar2(30);
ie_regra_bloqueio_w	varchar2(1);
cd_usuario_padrao_ww	varchar2(4000);

qt_outra_plano_w	number(15);
ie_encontrou_w		varchar2(1);

nr_pos_inicial_ww	number(3);
nr_pos_final_ww		number(3);
nr_seq_regra_w		number(10);

cursor c01 is
	select	nr_pos_inicial,
		nr_pos_final,
		cd_usuario_padrao,
		nvl(ie_regra_bloqueio,'B')
	from	conv_regra_usuario_plano
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio	        = cd_convenio_p
	and	cd_plano	        = cd_plano_p
	order by nvl(ie_regra_bloqueio,'B'); -- N�o alterar o order by, pois a �ltima tem que ser o 'X'
	
	
-- Somente as excess�es
cursor c02 is
	select	nr_pos_inicial,
		nr_pos_final,
		cd_usuario_padrao		
	from	conv_regra_usuario_plano
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio	        = cd_convenio_p
	and	cd_plano	        = cd_plano_p
	and 	nvl(ie_regra_bloqueio,'B') <> 'X'; 

begin

cd_usuario_padrao_ww	:= '';
ie_encontrou_w		:= 'N';

select	count(*)
into	qt_registro_w
from	conv_regra_usuario_plano
where	cd_estabelecimento = cd_estabelecimento_p
and	cd_convenio 	   = cd_convenio_p
and	cd_plano           = cd_plano_p;

if	(qt_registro_w	> 0) then
	
	open c01;
	loop
	fetch c01 into	
		nr_pos_inicial_w,
		nr_pos_final_w,
		cd_usuario_padrao_w,
		ie_regra_bloqueio_w;
	exit when c01%notfound;
		begin
		cd_usuario_padrao_ww	:= cd_usuario_padrao_ww ||','||cd_usuario_padrao_w;		
		ds_retorno_w			:= obter_se_contido_char(substr(cd_usuario_convenio_p,nr_pos_inicial_w,(nr_pos_final_w - nr_pos_inicial_w) + 1),cd_usuario_padrao_ww);
		
		if    (ds_retorno_w = 'S')  then
		      ie_encontrou_w:= 'S';
		end if;
		
		end;
	end loop;
	close c01;
	
	
	if 	(ie_regra_bloqueio_w = 'X') then
	
		open c02;
		loop
		fetch c02 into	
			nr_pos_inicial_ww,
			nr_pos_final_ww,
			cd_usuario_padrao_w;			
		exit when c02%notfound;
			begin
			cd_usuario_padrao_ww	:= cd_usuario_padrao_ww ||','||cd_usuario_padrao_w;		
			ds_retorno_w	:= 	obter_se_contido_char(substr(cd_usuario_convenio_p,nr_pos_inicial_ww,(nr_pos_final_ww - nr_pos_inicial_ww) + 1),cd_usuario_padrao_ww);
			
			if    (ds_retorno_w = 'S')  then
			      ie_encontrou_w:= 'X';
			end if;
			
			end;
		end loop;
		close c02;
		
		
		if 	(ie_encontrou_w = 'S')  then
			ds_retorno_w:= 'N';
		elsif 	(ie_encontrou_w = 'N')  then
			ds_retorno_w:= 'S';
		elsif	(ie_encontrou_w = 'X')  then
			ds_retorno_w:= 'S';
		end if;
		
	else	
		if	(ie_encontrou_w = 'S') then
			ds_retorno_w:= 'S';
		else
			ds_retorno_w	:= obter_se_contido_char(substr(cd_usuario_convenio_p,nr_pos_inicial_w,(nr_pos_final_w - nr_pos_inicial_w) + 1),cd_usuario_padrao_ww);
		end if;
	end if;
	
end if;

select	count(*)
into	qt_outra_plano_w
from	conv_regra_usuario_plano
where	cd_estabelecimento	= cd_estabelecimento_p
and	cd_convenio		= cd_convenio_p
and	cd_plano		<> cd_plano_p
and 	ie_regra_bloqueio	<> 'X'
and	substr(cd_usuario_convenio_p,nr_pos_inicial,(nr_pos_final - nr_pos_inicial) + 1)	= cd_usuario_padrao;

if	(qt_outra_plano_w	> 0) and
	((ds_retorno_w		<> 'S') or (qt_registro_w	= 0)) then
	ds_retorno_w		:= 'C';
end if;

if (ds_retorno_w = 'C') then
	begin
	select	max(nr_sequencia)
	into 	nr_seq_regra_w
	from	conv_regra_usuario_plano
	where	cd_estabelecimento	= cd_estabelecimento_p
	and		cd_convenio = cd_convenio_p
	and 	cd_plano <> cd_plano_p
	and 	cd_usuario_padrao = substr(cd_usuario_convenio_p,nr_pos_inicial,(nr_pos_final - nr_pos_inicial) + 1)
	and 	ie_regra_bloqueio	<> 'X';
	
	Select	substr(af_obter_desc_plano_conv(cd_plano, cd_convenio_p),1,255)
	into	ds_plano_regra_p
	from 	conv_regra_usuario_plano
	where 	nr_sequencia = nr_seq_regra_w;
	end;
end if;

ie_usuario_plano_p		:= nvl(ds_retorno_w,'S');
ie_regra_bloqueio_p		:= nvl(ie_regra_bloqueio_w,'B');

end consiste_usuario_plano;
/
