CREATE OR REPLACE
PROCEDURE Gerar_Etapa_Conta_Cancel(nr_interno_conta_p	number,
				   nm_usuario_p		varchar,
				   ie_opcao_p		varchar2 default 'S') is

nr_sequencia_w		number(10,0);
nr_seq_etapa_padrao_w	number(10,0);
cd_convenio_w		number(6,0);
cd_estabelecimento_w	number(10,0);
ie_tipo_atendimento_w	number(3,0);
nr_seq_classificacao_w	number(10,0);
cd_categoria_w		varchar2(10);
ie_clinica_w		number(5,0);
cd_setor_atendimento_w	number(10,0);
nr_seq_motivo_dev_w	number(10,0);
nr_atendimento_w	number(10,0);
cd_setor_atend_etapa_w	number(10);
ie_pessoa_etapa_w	varchar2(3);
ie_etapa_critica_w	varchar2(1);
nm_usuario_original_w	varchar2(15);
cd_pessoa_fisica_w	varchar2(10);
cd_perfil_w		perfil.cd_perfil%type := obter_perfil_ativo;
qt_regra_restringe_w	number(10,0);
nr_seq_w		fatur_etapa_conta.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,		
		nr_seq_etapa,
		nr_seq_motivo_dev,
		cd_setor_atend_etapa,
		nvl(ie_pessoa_etapa,'L'),
		nvl(ie_etapa_critica,'N')
	from	fatur_etapa_conta
	where	ie_situacao = 'A'
	and 	nvl(cd_convenio, nvl(cd_convenio_w,0)) = nvl(cd_convenio_w,0)
	and 	nvl(cd_estabelecimento, nvl(cd_estabelecimento_w,0)) = nvl(cd_estabelecimento_w,0)
	and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0)) = nvl(ie_tipo_atendimento_w,0)
	and	nvl(nr_seq_classificacao, nvl(nr_seq_classificacao_w,0)) = nvl(nr_seq_classificacao_w,0)
	and	nvl(cd_categoria, nvl(cd_categoria_w,'0')) = nvl(cd_categoria_w,'0')
	and	nvl(ie_clinica, nvl(ie_clinica_w,0)) = nvl(ie_clinica_w,0)
	and	nvl(cd_setor_atendimento, nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
	and 	nvl(IE_CONTA_AJUSTE_CANCEL,'N') = nvl(ie_opcao_p,'S')
	and	nvl(ie_conta_desdobramento,'S') = 'S'
	and	nvl(cd_perfil,nvl(cd_perfil_w,0))	= nvl(cd_perfil_w,0)
	order by nvl(cd_convenio,0),
		nvl(cd_estabelecimento,0),
		nvl(ie_tipo_atendimento,0),
		nvl(nr_seq_classificacao,0),
		nvl(cd_categoria,'0'),
		nvl(ie_clinica,0),
		nvl(cd_setor_atendimento,0),
		nvl(cd_perfil,0);

BEGIN

select	max(cd_convenio_parametro),
	max(cd_estabelecimento),
	max(cd_categoria_parametro),
	max(nvl(obter_setor_atendimento(nr_atendimento),0)),
	max(nr_atendimento),
	max(nvl(nm_usuario_original,''))
into	cd_convenio_w,
	cd_estabelecimento_w,
	cd_categoria_w,
	cd_setor_atendimento_w,
	nr_atendimento_w,
	nm_usuario_original_w
from 	conta_paciente
where 	nr_interno_conta = nr_interno_conta_p;
	
nr_seq_etapa_padrao_w:= null;

select	nvl(max(ie_tipo_atendimento),0),
	nvl(max(nr_seq_classificacao),0),
	nvl(max(ie_clinica),0)
into	ie_tipo_atendimento_w,
	nr_seq_classificacao_w,
	ie_clinica_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;

open C01;
loop
fetch C01 into	
	nr_seq_w,	
	nr_seq_etapa_padrao_w,
	nr_seq_motivo_dev_w,
	cd_setor_atend_etapa_w,
	ie_pessoa_etapa_w,
	ie_etapa_critica_w;
exit when C01%notfound;
	begin
	nr_seq_etapa_padrao_w	:= nr_seq_etapa_padrao_w;
	nr_seq_motivo_dev_w	:= nr_seq_motivo_dev_w;
	cd_setor_atend_etapa_w	:= cd_setor_atend_etapa_w;
	ie_pessoa_etapa_w	:= ie_pessoa_etapa_w;
	ie_etapa_critica_w	:= ie_etapa_critica_w;
	end;
end loop;
close C01;

select 	count(*)
into	qt_regra_restringe_w
from 	fatur_etapa_conta_conv
where 	nr_seq_regra = nr_seq_w
and 	cd_convenio = cd_convenio_w;

if	(qt_regra_restringe_w > 0) then
	nr_seq_etapa_padrao_w:= null;
end if;

if	(nr_seq_etapa_padrao_w is not null) then

	select	substr(Obter_Pessoa_Fisica_Usuario(decode(ie_pessoa_etapa_w, 'L', nm_usuario_p, nm_usuario_original_w),'C'),1,10)
	into	cd_pessoa_fisica_w
	from	dual;

	select	conta_paciente_etapa_seq.NextVal
	into	nr_sequencia_w
	from 	dual;

	insert into conta_paciente_etapa (
		NR_SEQUENCIA,
		NR_INTERNO_CONTA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ETAPA,
		NR_SEQ_ETAPA,
		CD_SETOR_ATENDIMENTO,
		CD_PESSOA_FISICA,
		nr_seq_motivo_dev,
		ds_observacao,
		nr_lote_barras,
		ie_etapa_critica)
	values	(nr_sequencia_w,
		nr_interno_conta_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_etapa_padrao_w,
		cd_setor_atend_etapa_w,
		cd_pessoa_fisica_w,
		nr_seq_motivo_dev_w,
		wheb_mensagem_pck.get_texto(306347),
		null,
		ie_etapa_critica_w);
		
end if;

END Gerar_Etapa_Conta_Cancel;
/