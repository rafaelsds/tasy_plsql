create or replace
procedure gerar_ret_amaior_proporcional(
			nm_usuario_p		Varchar2,
			vl_amaior_p		number,
			nr_seq_retorno_p	number) is 

vl_guia_w	number(15,2);
vl_amaior_w	number(15,2);
pr_amaior_w	number(7,4);
nr_sequencia_w	number(10);			
vl_pago_w	number(15,2);
vl_adicional_w	number(15,2);
vl_adicional_total_w number(15,2) := 0;
ds_erro_w		varchar2(4000);

Cursor C01 is
	select	nr_sequencia,
		vl_pago,
		vl_adicional
	from	convenio_retorno_item
	where	nr_seq_retorno = nr_seq_retorno_p;
begin

begin
dbms_application_info.SET_ACTION('RECEBER_AMAIOR_PROPORCIONAL'); 

select	nvl(sum(vl_pago),0)
into	vl_guia_w
from	convenio_retorno_item
where	nr_seq_retorno	= nr_seq_retorno_p;

vl_amaior_w	:= nvl(vl_amaior_p,0);

if	(vl_guia_w > 0) and	
	(vl_amaior_p > 0) then
	--(vl_amaior_p > 0) and 
	--(vl_guia_w > vl_amaior_p) then
	begin

	if	(dividir_sem_round(vl_amaior_w, vl_guia_w) > 0) then
		begin
		open C01;
		loop
		fetch C01 into	
			nr_sequencia_w,
			vl_pago_w,
			vl_adicional_w;
		exit when C01%notfound;
			begin
			
			vl_adicional_w	:= vl_adicional_w + (vl_pago_w * dividir_sem_round(vl_amaior_w, vl_guia_w));
			
			update	convenio_retorno_item
			set	vl_adicional		= vl_adicional_w
			where	nr_sequencia		= nr_sequencia_w
			and	nr_seq_retorno		= nr_seq_retorno_p;
			
			vl_adicional_total_w	:= vl_adicional_total_w + vl_adicional_w;

			end;
		end loop;
		close C01;
		end;
	end if;	

	if	(vl_amaior_w > vl_adicional_total_w) then	
		
		vl_adicional_total_w	:= vl_amaior_w - vl_adicional_total_w;

		select	max(nr_sequencia)
		into	nr_sequencia_w
		from	convenio_retorno_item
		where	nr_seq_retorno	= nr_seq_retorno_p;

		update	convenio_retorno_item
		set	vl_adicional		= vl_adicional + vl_adicional_total_w
		where	nr_sequencia		= nr_sequencia_w
		and	nr_seq_retorno		= nr_seq_retorno_p;
	
	elsif	(vl_amaior_w < vl_adicional_total_w) then
		
		vl_adicional_total_w	:= vl_adicional_total_w - vl_amaior_w;
		
		select	max(nr_sequencia)
		into	nr_sequencia_w
		from	convenio_retorno_item
		where	nr_seq_retorno	= nr_seq_retorno_p;
		
		update	convenio_retorno_item
		set	vl_adicional		= vl_adicional - vl_adicional_total_w
		where	nr_sequencia		= nr_sequencia_w
		and	nr_seq_retorno		= nr_seq_retorno_p;
		
	end if;
	
	end;
end if;

exception
when others then
	dbms_application_info.SET_ACTION('');
	ds_erro_w	:= sqlerrm;
	--r.aise_application_error(-20011,sqlerrm);
	wheb_mensagem_pck.exibir_mensagem_abort(263395,'ds_erro_w='||ds_erro_w);
end;

commit;

end gerar_ret_amaior_proporcional;
/