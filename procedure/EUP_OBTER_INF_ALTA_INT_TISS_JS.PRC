create or replace
procedure eup_obter_inf_alta_int_tiss_js(
					ie_tipo_evento_p	varchar2,
					nr_atendimento_p	number,
					nm_usuario_p		Varchar2,
					nr_sequencia_p	    out number,
					cd_convenio_p	    out number) is 

ie_tipo_convenio_w	number(2);					
cd_convenio_w		number(5);
nr_seq_xml_projeto_w	number(10);
qt_internacao_w		number(10);
cd_estabelecimento_w	number(4);
nr_sequencia_w		number(10);
				
begin

cd_estabelecimento_w:= wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(nr_atendimento_p,0) > 0) then

	select	max(a.cd_convenio), 
		max(c.ie_tipo_convenio) 
	into	cd_convenio_w,
		ie_tipo_convenio_w
	from 	atend_categoria_convenio a,
		atendimento_paciente b, 
		convenio c 
	where   a.nr_atendimento = b.nr_atendimento 
	and 	a.nr_seq_interno = obter_atecaco_atendimento(a.nr_atendimento) 
	and     a.cd_convenio = c.cd_convenio 
	and 	b.nr_atendimento = nr_atendimento_p;

	if	(nvl(cd_convenio_w,0) > 0) then
		nr_seq_xml_projeto_w := TISS_OBTER_XML_PROJETO(cd_convenio_w, cd_estabelecimento_w,20);
	end if;
	if	(ie_tipo_evento_p = 'I') then
		select	count(*) 
		into	qt_internacao_w
		from 	tiss_comunic_benif 
		where nr_atendimento = nr_atendimento_p 
		and ie_tipo_evento = 'I';
	end if;
	
	if	(ie_tipo_convenio_w = 2) and
		(nvl(nr_seq_xml_projeto_w,0) <> 0) and
		((nvl(qt_internacao_w,0) = 0) or 
		 (ie_tipo_evento_p = 'A')) then
		tiss_gerar_comunic_benef(cd_estabelecimento_w, nr_atendimento_p, nm_usuario_p, ie_tipo_evento_p, null, sysdate, nr_sequencia_w);
		if	(nr_sequencia_w is not null) then
			nr_sequencia_p := nvl(nr_sequencia_w,0);
			cd_convenio_p := cd_convenio_w;
		end if;
	end if;
	
end if;

commit;

end eup_obter_inf_alta_int_tiss_js;
/