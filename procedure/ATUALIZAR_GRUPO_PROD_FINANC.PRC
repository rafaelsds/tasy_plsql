create or replace
procedure ATUALIZAR_GRUPO_PROD_FINANC
			(nr_adiantamento_p	number,
			nr_titulo_receber_p	number,
			nr_seq_movto_cartao_p	number,
			nr_seq_cheque_p		number,
			nm_usuario_p		varchar2,
			nr_seq_produto_p	number) is
	
nr_seq_grupo_prod_w	number(10);
nr_seq_subgrupo_prod_w	number(10);
	
begin

if	(nr_seq_produto_p is not null) then
	
	select	max(nr_seq_grupo)
	into	nr_seq_subgrupo_prod_w
	from	produto_financeiro
	where	nr_sequencia	= nr_seq_produto_p;
	
	select	max(nr_seq_grupo_sup)
	into	nr_seq_grupo_prod_w
	from	grupo_prod_financ
	where	nr_sequencia	= nr_seq_subgrupo_prod_w;

	if	(nr_adiantamento_p is not null) then
		update	adiantamento
		set	nr_seq_grupo_prod	= nr_seq_grupo_prod_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_adiantamento		= nr_adiantamento_p;
		
	elsif	(nr_titulo_receber_p is not null) then
		update	titulo_receber
		set	nr_seq_grupo_prod	= nr_seq_grupo_prod_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_titulo		= nr_titulo_receber_p;
		
	elsif	(nr_seq_movto_cartao_p is not null) then
		update	movto_cartao_cr
		set	nr_seq_grupo_prod	= nr_seq_grupo_prod_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_seq_movto_cartao_p;
		
	elsif	(nr_seq_cheque_p is not null) then
		update	cheque_cr
		set	nr_seq_grupo_prod	= nr_seq_grupo_prod_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_seq_cheque		= nr_seq_cheque_p;
		
	end if;
	
end if;

commit;

end ATUALIZAR_GRUPO_PROD_FINANC;
/