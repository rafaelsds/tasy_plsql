create or replace 
procedure ATUALIZA_SINTOMAS_ATENDIMENTO (
		nr_atendimento_p	  	number,
		ds_sintoma_paciente_p 	varchar2,
		nm_usuario_p		varchar2 ) is
begin
	
	update 	atendimento_paciente 
	set    	ds_sintoma_paciente = substr(ds_sintoma_paciente_p,1,255) ,
		nm_usuario 	= nm_usuario_p , 
		dt_atualizacao 	= sysdate 
	where  	nr_atendimento 	= nr_atendimento_p;

end ATUALIZA_SINTOMAS_ATENDIMENTO;
/