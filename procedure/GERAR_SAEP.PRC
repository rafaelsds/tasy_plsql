create or replace
procedure gerar_saep(	cd_pessoa_fisica_p	varchar2,
			nr_atendimento_p	number,
			nm_usuario_p		Varchar2) is 
			
begin

insert into sae_peroperatorio(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_pessoa_fisica,
	nr_atendimento,
	dt_saep,
	cd_profissional)
values(
	sae_peroperatorio_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_pessoa_fisica_p,
	nr_atendimento_p,
	sysdate,
	obter_dados_usuario_opcao(nm_usuario_p,'C'));
commit;

end gerar_saep;
/