create or replace procedure move_to_free_bed(
 					nm_usuario_p		Varchar2,
 					nr_atendimento_p	number,
 					nr_enc_unity_code_p	number,
   					dt_entrada_unidade_p	date default null,
 					ds_retorno_p		out varchar2) is 

cd_setor_atendimento_w		number(5);
cd_unidade_basica_w		varchar2(10);	
cd_unidade_compl_w		varchar2(10);
cd_tipo_acomodacao_w		number(4);
nr_acompanhante_w		number(3);
nr_seq_interno_w		number(10);
nr_sequencia_w			number(10);
cd_setor_atendimento_troca_w	number(5);
cd_unidade_basica_troca_w	varchar2(10);
cd_unidade_compl_troca_w	varchar2(10);
cd_tipo_acomodacao_troca_w	number(4);
nr_acompanhante_troca_w		number(3);
nr_seq_atepacu_atend_w		number(10);
ie_leito_monit_orig_w		varchar2(1);
ds_param_integ_hl7_w		varchar2(4000);
ds_sep_bv_w			varchar2(100);
ie_considerar_troca_leito_w	varchar2(1);
ie_passagem_setor_w		varchar2(1) := 'N';
begin

ds_retorno_p := '';
obter_param_usuario(3111, 166, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_considerar_troca_leito_w);


if	(nvl(nr_atendimento_p,0) > 0) and 
        (nvl(nr_enc_unity_code_p,0) >0) then

	nr_seq_atepacu_atend_w :=	obter_atepacu_paciente(nr_atendimento_p, 'A'); 
 
	if (nvl(nr_seq_atepacu_atend_w,0) > 0)  then

                -- RETRIEVE CURRENT BED INFO OF THE PATIENT
		select 	a.cd_setor_atendimento,
			a.cd_unidade_basica,
			a.cd_unidade_compl,
			a.cd_tipo_acomodacao,
			a.nr_acompanhante
		into	cd_setor_atendimento_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w,
			cd_tipo_acomodacao_w,
			nr_acompanhante_w
		from	atend_paciente_unidade a
		where	a.nr_seq_interno = nr_seq_atepacu_atend_w;

                -- SET EXIT DATE OF ACTUAL BED
		update	atend_paciente_unidade
		set	dt_saida_unidade = nvl(dt_entrada_unidade_p, sysdate) --sysdate
		where	nr_seq_interno = nr_seq_atepacu_atend_w;

		
                -- RETRIVE DESIRED BED INFO
                select 	a.cd_setor_atendimento,
			b.cd_unidade_basica,
			b.cd_unidade_compl,
			b.cd_tipo_acomodacao,
                        a.nr_acompanhante
                into	cd_setor_atendimento_troca_w,
                        cd_unidade_basica_troca_w,
                        cd_unidade_compl_troca_w,
                        cd_tipo_acomodacao_troca_w,
                        nr_acompanhante_troca_w
                from	atend_paciente_unidade a,  unidade_atendimento b
                where	a.nr_seq_interno = nr_seq_atepacu_atend_w
                and b.nr_seq_interno = nr_enc_unity_code_p;

                -- SET ACTUAL BED AS FREE
		update	unidade_atendimento
		set		ie_status_unidade	= 'L',
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate,
				dt_entrada_unidade	= null,
				nr_atendimento		= null
		where	cd_setor_atendimento    = cd_setor_atendimento_w
		and		cd_unidade_basica	= cd_unidade_basica_w 
		and		cd_unidade_compl	= cd_unidade_compl_w;


                if	(ie_considerar_troca_leito_w = 'S') then
			begin
			        ie_passagem_setor_w	:=	'L';	
			end;
		end if;

		select	atend_paciente_unidade_seq.nextval
		into	nr_seq_interno_w
		from	dual;

		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_w
		from	atend_paciente_unidade
		where	nr_atendimento		= nr_atendimento_p;

                -- PUT THE PATIENT IN DESIRED BED
		insert	into atend_paciente_unidade
			(nr_atendimento,
			nr_sequencia,
			cd_setor_atendimento,
			cd_unidade_basica,
			cd_unidade_compl,
			dt_entrada_unidade,
			dt_atualizacao,
			nm_usuario,
			cd_tipo_acomodacao,
			dt_saida_unidade,
			nr_atend_dia,
			nm_usuario_original,
			dt_saida_interno,
			ie_passagem_setor,
			nr_acompanhante,
			nr_seq_interno)
		values	(nr_atendimento_p,
			nr_sequencia_w,
			cd_setor_atendimento_troca_w,
			cd_unidade_basica_troca_w,
			cd_unidade_compl_troca_w,
			nvl(dt_entrada_unidade_p, sysdate), --sysdate,
			sysdate,
			nm_usuario_p,
			cd_tipo_acomodacao_w,
			null,
			null,
			nm_usuario_p,
			to_date('30/12/2999 00:00:00', 'dd/mm/yyyy hh24:mi:ss'),
			ie_passagem_setor_w,
			nr_acompanhante_w,
			nr_seq_interno_w);


		ds_retorno_p := Wheb_mensagem_pck.get_texto(1188538); --'Change performed successfully';

	end if;

	select	nvl(obter_se_leito_atual_monit(nr_atendimento_p),'N')
	into	ie_leito_monit_orig_w
	from dual;

	ds_sep_bv_w := obter_separador_bv;
	
	if	(ie_leito_monit_orig_w = 'S') then
		begin
			ds_param_integ_hl7_w :=	'nr_atendimento=' || nr_atendimento_p || ds_sep_bv_w ||
						'nr_seq_interno=' || nr_seq_atepacu_atend_w || ds_sep_bv_w;
			gravar_agend_integracao(22, ds_param_integ_hl7_w);
		end;
	
	end if;

end if;

commit;

end move_to_free_bed;
/
