create or replace
procedure pls_alterar_inclusao_coop
		(	nr_seq_inclusao_p		number,
			nr_seq_cooperado_p		number,
			ie_acao_p			varchar2,
			nm_usuario_p			varchar2) is 

/*	ie_acao_p
	A - Aceita
	R - Rejeitada
	V - Vincular
*/			
			
begin

if	(ie_acao_p = 'A') then
	update	pls_inclusao_cooperado
	set	dt_aceitacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_inclusao_p;
elsif	(ie_acao_p = 'R') then
	update	pls_inclusao_cooperado
	set	dt_rejeicao		= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_inclusao_p;
elsif	(ie_acao_p = 'V') and
	(nr_seq_cooperado_p is not null) then
	update	pls_inclusao_cooperado
	set	nr_seq_cooperado	= nr_seq_cooperado_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_inclusao_p;
end if;

commit;

end pls_alterar_inclusao_coop;
/