create or replace
procedure atualizar_prev_execucao_alt( nr_prescricao_p	number,
									   nr_seq_prescr_p	number,
									   dt_prev_execucao_p date,
									   nm_usuario_p	Varchar2  ) is 
begin
	if  (nr_prescricao_p is not null) and
		(dt_prev_execucao_p	is not null) and
	    (nr_seq_prescr_p is not null) then
	   
		update prescr_procedimento
			set dt_prev_execucao = dt_prev_execucao_p,
				IE_PREV_EXECUCAO_ALT = 'S',
				dt_atualizacao  = 	sysdate,
				nm_usuario 	= 	nm_usuario_p
		where nr_prescricao = nr_prescricao_p
		and	  nr_sequencia  = nr_seq_prescr_p;

		update prescr_proc_hor
			set dt_horario		    =    dt_prev_execucao_p,
			      nm_usuario_reaprazamento   =   nm_usuario_p	
		where nr_prescricao		    =   nr_prescricao_p
		and     nvl(nr_seq_proc_origem, nr_seq_procedimento)   =   nr_seq_prescr_p;
			
		commit;
		
	end if;	

end atualizar_prev_execucao_alt;
/