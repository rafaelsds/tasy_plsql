Create or Replace
Procedure sup_comunic_ativacao_material (	cd_material_p	number,
					nm_usuario_p	varchar2) is
										
cd_grupo_material_w		Number;
cd_subgrupo_material_w 		Number;
cd_classe_material_w		Number;
ds_grupo_material_w		varchar2(255);
ds_subgrupo_material_w 		varchar2(255);
ds_classe_material_w		varchar2(255);
ds_comunic_w			Varchar2(500);
nr_seq_classif_w			Number;
cd_setor_dest_w			Number;
nm_usuario_regra_w		Varchar2(255);
qt_regras_w			Number;
ds_setor_w			Varchar2(255);
nm_usuario_dest_w			varchar2(4000);
ds_titulo_w			varchar2(255);
cd_perfil_w			number(5);
ie_ci_lida_w			varchar2(1);
nr_seq_comunic_w			number(10);
ie_regra_w			varchar2(1);

cursor	c01 is
	select	cd_setor_atendimento,
		cd_perfil,
		ds_usuario_destino,
		nvl(ie_ci_lida,'N')
	from 	regra_aviso_exec_mat
	where	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
	and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
	and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
	and	nvl(cd_material, cd_material_p)			= cd_material_p
	and	nvl(ie_ativa_cadastro,'N')				= 'S'
	and 	nvl(cd_funcao, nvl(obter_funcao_ativa,0)) = nvl(obter_funcao_ativa,0)
	order by 
		nvl(cd_grupo_material, 0),
		nvl(cd_subgrupo_material, 0),
		nvl(cd_classe_material, 0),
		nvl(cd_material, 0),
		nvl(cd_funcao,0);
begin

select	max(cd_grupo_material),
	max(cd_subgrupo_material),
	max(cd_classe_material),
	substr(max(ds_grupo_material),1,255),
	substr(max(ds_subgrupo_material),1,255),
	substr(max(ds_classe_material),1,255)
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	ds_grupo_material_w,
	ds_subgrupo_material_w,
	ds_classe_material_w
from	estrutura_material_v
where 	cd_material = cd_material_p;

/*select	substr('O material c�digo: ' || cd_material_p || ' - '  || substr(obter_desc_material(cd_material_p),1,120) || ' foi ativado pelo usu�rio ' || nm_usuario_p || chr(13) || chr(10) ||
	'Grupo: ' || ds_grupo_material_w || chr(13) || chr(10) ||
	'Subgrupo: ' || ds_subgrupo_material_w || chr(13) || chr(10) ||
	'Classe: ' || ds_classe_material_w,1,255),
	'Aviso de material ativo.'
into	ds_comunic_w,
		ds_titulo_w
from 	dual;*/

select	substr(wheb_mensagem_pck.get_texto(314876, 'CD_MATERIAL=' || cd_material_p || ';' || 
						   'DS_MATERIAL=' || substr(obter_desc_material(cd_material_p),1,120) || ';' || 
						   'NM_USUARIO='  || nm_usuario_p) || chr(13) || chr(10) ||
	wheb_mensagem_pck.get_texto(314210, 'DS_GRUPO_MATERIAL=' || ds_grupo_material_w) || chr(13) || chr(10) ||
	wheb_mensagem_pck.get_texto(314211, 'DS_SUBGRUPO_MATERIAL=' || ds_subgrupo_material_w) || chr(13) || chr(10) ||
	wheb_mensagem_pck.get_texto(314212, 'DS_CLASSE_MATERIAL=' || ds_classe_material_w),1,255),
	wheb_mensagem_pck.get_texto(314877)
into	ds_comunic_w,
	ds_titulo_w
from 	dual;

select	max(obter_classif_comunic('F'))
into	nr_seq_classif_w
from	dual;

select	count(*)
into	qt_regras_w
from 	regra_aviso_exec_mat
where	nvl(ie_ativa_cadastro,'N') = 'S'
and 	nvl(cd_funcao, nvl(obter_funcao_ativa,0)) = nvl(obter_funcao_ativa,0);

ie_regra_w	:=	'N';

if	(qt_regras_w > 0) then
	begin
	open c01;
	loop
	fetch c01 into 
		cd_setor_dest_w,
		cd_perfil_w,
		nm_usuario_regra_w,
		ie_ci_lida_w;
	exit when c01%notfound;
		begin
		if	(cd_setor_dest_w is not null) then
			ds_setor_w := ds_setor_w || substr(to_char(cd_setor_dest_w) || ',',1,255);
		end if;

		if	(nm_usuario_regra_w is not null) then
			nm_usuario_dest_w := substr(nm_usuario_dest_w || nm_usuario_regra_w || ',',1,4000);
		end if;
		
		ie_regra_w	:=	'S';
		end;
	end loop;
	close 	c01;
	end;
end if;

if	(qt_regras_w > 0) and
	(ie_regra_w = 'S') then
	begin
	select	comunic_interna_seq.nextval
	into	nr_seq_comunic_w
	from	dual;

	insert into comunic_interna
		(dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nm_usuario_destino,
		cd_perfil,
		nr_sequencia,
		ie_gerencial,
		nr_seq_classif,
		ds_perfil_adicional,
		cd_setor_destino,
		cd_estab_destino,
		ds_setor_adicional,
		dt_liberacao,
		ds_grupo,
		nm_usuario_oculto) values
			(sysdate,
			ds_titulo_w,
			wheb_rtf_pck.get_texto_rtf(ds_comunic_w),
			nm_usuario_p,
			sysdate,
			'N',
			nm_usuario_dest_w,
			null,
			nr_seq_comunic_w,
			'N',
			nr_seq_classif_w,
			cd_perfil_w || ',',
			null,
			'',
			ds_setor_w,
			sysdate,
			'',
			'');

	if	(ie_ci_lida_w = 'S') then
		
		insert into comunic_interna_lida(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao) values (
				nr_seq_comunic_w,
				nm_usuario_p,
				sysdate);

	end if;
	end;
end if;

commit;

end sup_comunic_ativacao_material;
/