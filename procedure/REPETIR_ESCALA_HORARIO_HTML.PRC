create or replace
procedure repetir_escala_horario_html(
				nr_seq_escala_p		number,
				data_inicial_p		date,
				data_final_p		date,
				data_ref_inic_p		date,
				qt_dias_P		varchar2,
				hora_ini_ref_p		varchar2,
				hora_fim_ref_p		varchar2,
				nm_usuario_p		varchar2) is

dt_ref_i_w			date;
dt_ref_f_w			date;
				
cd_estabelecimento_w		number(005,0);
dt_inicial_w			date;
dt_final_w			Date;
dt_inic_orig_w			Date;
dt_fim_orig_w			Date;
dt_origem_w			Date;
dt_escala_w			Date;
hr_inicial_w			Varchar2(08);
dt_inicio_w			Date;
dt_fim_w				Date;

nr_sequencia_w			Number(010,0)    := 0;
cd_pessoa_fisica_w		Varchar2(010);
ds_observacao_w			Varchar2(255);
ie_feriado_w			Varchar2(001);
qt_dia_w				Number(15,0);
qt_duracao_w			Number(15,9);
nr_seq_escala_diaria_w		number(10);
cd_pessoa_fisica_adic_w		varchar2(10);
nr_seq_classif_escala_w		number(10);
dt_inicial_p			date;
dt_final_p				date;
dt_ref_inic_p			date;
dt_ref_final_p			date;

cursor c01 is
Select	nr_sequencia,
	to_char(dt_inicio,'hh24:mi:ss'),
	cd_pessoa_fisica,
	ds_observacao,
	dt_fim - dt_inicio
from 	escala_diaria
where 	nr_seq_escala		= nr_seq_escala_p
and	dt_inicio between dt_origem_w and PKG_DATE_UTILS.END_OF(dt_origem_w, 'DAY', 0)
order by 1,2;

cursor c02 is
select	cd_pessoa_fisica,
	nr_seq_classif_escala
from	escala_diaria_adic
where	nr_seq_escala_diaria	= nr_seq_escala_diaria_w;

begin
dt_inicial_p		  := data_inicial_p;
dt_final_p	  	  := data_final_p;
dt_ref_inic_p	  	:= data_ref_inic_p;
dt_ref_final_p		:= dt_ref_inic_p + to_number(qt_dias_P);

if	(dt_inicial_p > dt_final_p) then
	--'A data inicial n�o pode ser maior que a final');
	Wheb_mensagem_pck.exibir_mensagem_abort(188849);
end if;
if	(dt_ref_inic_p > dt_ref_final_p) then
	--'A data de refer�ncia inicial n�o pode ser maior que a final');
	Wheb_mensagem_pck.exibir_mensagem_abort(188850);
end if;

if	(dt_inicial_p < PKG_DATE_UTILS.start_of(sysdate,'dd',0)) then
	--'A data de inicial n�o pode ser menor que hoje');
	Wheb_mensagem_pck.exibir_mensagem_abort(188851);
end if;

if	(PKG_DATE_UTILS.start_of(dt_ref_final_p,'dd',0) >= PKG_DATE_UTILS.start_of(dt_inicial_p,'dd',0)) then
	--'A data de refer�ncia final deve ser menor que a inicial para repeti��o');
	Wheb_mensagem_pck.exibir_mensagem_abort(188852);
end if;

qt_dia_w	:= trunc((PKG_DATE_UTILS.start_of(dt_ref_final_p,'dd',0) - PKG_DATE_UTILS.start_of(dt_ref_inic_p,'dd',0)),0);

if	(qt_dia_w > 31) then
	--'O Per�odo de refer�ncia para c�pia n�o pode ser maior que 31 dias');
	Wheb_mensagem_pck.exibir_mensagem_abort(188853);
end if;

select	c.cd_estabelecimento
into	cd_estabelecimento_w
from	escala_classif c,
	escala_grupo b,
	escala a
where	a.nr_sequencia	= nr_seq_escala_p
and	a.nr_seq_grupo	= b.nr_sequencia
and	b.nr_seq_classif	= c.nr_sequencia;

dt_inic_orig_w		:= PKG_DATE_UTILS.start_of(dt_ref_inic_p,'dd',0);
dt_fim_orig_w			:= PKG_DATE_UTILS.start_of(dt_ref_final_p,'dd',0);

dt_inicial_w			:= PKG_DATE_UTILS.start_of(dt_inicial_p,'dd',0);
dt_final_w			:= PKG_DATE_UTILS.start_of(dt_final_p,'dd',0);
dt_escala_w			:= dt_inicial_w;

while	(dt_escala_w <= dt_final_w) loop
	dt_origem_w		:= dt_inic_orig_w;
	while	(dt_origem_w <= dt_fim_orig_w) and
		(dt_escala_w <= dt_final_w) loop
		open c01;
		loop
		fetch c01 into
			nr_seq_escala_diaria_w,
			hr_inicial_w,
			cd_pessoa_fisica_w,
			ds_observacao_w,
			qt_duracao_w;		
		exit 	when c01%notfound;
			begin				
			dt_inicio_w	:= PKG_DATE_UTILS.get_Time(dt_escala_w, hr_inicial_w);
			dt_fim_w	:= dt_inicio_w + qt_duracao_w;
			dt_ref_i_w := PKG_DATE_UTILS.get_Time(dt_escala_w, hora_ini_ref_p);
			dt_ref_f_w := PKG_DATE_UTILS.get_Time(dt_escala_w, hora_fim_ref_p);
			if	(dt_inicio_w >= dt_ref_i_w) and
				(dt_inicio_w <= dt_ref_f_w) and
				(dt_fim_w <= dt_ref_f_w) and
				(dt_fim_w >= dt_ref_i_w) then
				begin
				select decode(obter_se_feriado(cd_estabelecimento_w, PKG_DATE_UTILS.start_of(dt_escala_w,'dd',0)),0,'N','S')
				into	ie_feriado_w
				from	dual;
				select	escala_diaria_seq.nextval
				into	nr_sequencia_w
				from	dual;
				insert into escala_diaria(
					nr_sequencia,
					nr_seq_escala,
					dt_atualizacao,
					nm_usuario,
					dt_inicio,
					dt_fim,
					ie_feriado,
					cd_pessoa_fisica,
					cd_pessoa_origem,
					ds_observacao)
				values(
					nr_sequencia_w,
					nr_seq_escala_p,
					sysdate,
					nm_usuario_p,
					dt_inicio_w,
					dt_fim_w,
					nvl(ie_feriado_w,'N'),
					cd_pessoa_fisica_w,
					cd_pessoa_fisica_w,
					ds_observacao_w);

				open c02;
				loop
				fetch c02 into
					cd_pessoa_fisica_adic_w,
					nr_seq_classif_escala_w;
				exit when c02%notfound;
					insert	into	escala_diaria_adic
						(nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						nr_seq_escala_diaria,
						cd_pessoa_fisica,
						nr_seq_classif_escala)
					values	(escala_diaria_adic_seq.nextval,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nr_sequencia_w,
						cd_pessoa_fisica_adic_w,
						nr_seq_classif_escala_w);
				end loop;
				close c02;
				end;
			end if;
			end;
		end loop;
		close c01;
		dt_origem_w	:= dt_origem_w + 1;
		dt_escala_w	:= dt_escala_w + 1;
	end loop;
end loop;
commit;

end repetir_escala_horario_html;
/