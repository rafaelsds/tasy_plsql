create or replace procedure Gravar_Log_Alt_Prot_Onc(   
					nr_seq_paciente_p	number,
					cd_material_p		number,
					nm_usuario_p		varchar2,
					nm_tabela_p		varchar2,
					nm_atributo_p		varchar2,
					ds_valor_ant_p	varchar2,
                    ds_valor_novo_p	 varchar2,
                    ie_acao_p		varchar2) is
cd_pessoa_fisica_w   pessoa_fisica .cd_pessoa_fisica%TYPE;
begin
cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');
insert into paciente_prot_medic_hist (nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_alteracao,                    
					nr_seq_paciente,
					cd_material,
					nm_tabela,
					nm_atributo,
					ds_valor_ant,
					ds_valor_novo,
                    ie_tipo_alteracao,
                    cd_profissional)                    
		values	(paciente_prot_medic_hist_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,                   
					nr_seq_paciente_p,
					cd_material_p,
					nm_tabela_p,
					nm_atributo_p,
					ds_valor_ant_p,
					ds_valor_novo_p,
                    ie_acao_p,
                    cd_pessoa_fisica_w); 
end Gravar_Log_Alt_Prot_Onc;
/