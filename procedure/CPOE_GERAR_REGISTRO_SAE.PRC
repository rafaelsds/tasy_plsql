create or 
replace procedure cpoe_gerar_registro_sae ( 
						nr_sequencia_p		number,
						nr_atendimento_p	number,
						nm_usuario_p		varchar2,
						nr_seq_prescr_p		number) is

nr_seq_prescr_w			pe_prescricao.nr_sequencia%type;
cd_prescritor_w			pe_prescricao.cd_prescritor%type;
dt_prescricao_w			pe_prescricao.dt_prescricao%type;
cd_pessoa_fisica_w		pe_prescricao.cd_pessoa_fisica%type;
cd_perfil_ativo_w		pe_prescricao.cd_perfil_ativo%type;
dt_inicio_w				pe_prescricao.dt_inicio_prescr%type;
dt_fim_w				pe_prescricao.dt_validade_prescr%type;
dt_liberacao_w			pe_prescricao.dt_liberacao%type;
nr_prescricao_w			pe_prescricao.nr_prescricao%type;

nr_seq_proc_w			pe_prescr_proc.nr_seq_proc%type;
cd_intervalo_w			pe_prescr_proc.cd_intervalo%type;
nr_seq_topografia_w		pe_prescr_proc.nr_seq_topografia%type;
hr_prim_horario_w		pe_prescr_proc.hr_prim_horario%type;
ie_se_necessario_w		pe_prescr_proc.ie_se_necessario%type;
ie_acm_w				pe_prescr_proc.ie_acm%type;
ie_agora_w				pe_prescr_proc.ie_agora%type;
ie_lado_w				pe_prescr_proc.ie_lado%type;
ds_horarios_w			pe_prescr_proc.ds_horarios%type;
ds_observacao_w			pe_prescr_proc.ds_observacao%type;
dt_suspensao_w			pe_prescr_proc.dt_suspensao%type;

nr_seq_cpoe_interv_w	cpoe_intervencao.nr_sequencia%type;
ie_administracao_w		cpoe_intervencao.ie_administracao%type;
ie_urgencia_w			cpoe_intervencao.ie_urgencia%type;
ie_gerar_prescr_evol_w 	varchar2(1);
cd_evolucao_w 			number(10);
begin
Obter_param_Usuario(281,1590, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_prescr_evol_w);
	select	nr_seq_prescr,
			nr_seq_proc,
			cd_intervalo,
			nr_seq_topografia,
			hr_prim_horario,
			nvl(ie_se_necessario,'N'),
			nvl(ie_acm,'N'),
			nvl(ie_agora,'N'),
			ie_lado,
			ds_horarios,
			ds_observacao,
			dt_suspensao
	into	nr_seq_prescr_w,
			nr_seq_proc_w,
			cd_intervalo_w,
			nr_seq_topografia_w,
			hr_prim_horario_w,
			ie_se_necessario_w,
			ie_acm_w,
			ie_agora_w,
			ie_lado_w,
			ds_horarios_w,
			ds_observacao_w,
			dt_suspensao_w
	from	pe_prescr_proc
	where	nr_sequencia = nr_sequencia_p
	and		nr_seq_prescr = nr_seq_prescr_p;

	select	cd_prescritor,
			dt_prescricao,
			cd_pessoa_fisica,
			cd_perfil_ativo,
			dt_inicio_prescr,
			dt_validade_prescr,
			dt_liberacao,
			nr_prescricao
	into	cd_prescritor_w,
			dt_prescricao_w,
			cd_pessoa_fisica_w,
			cd_perfil_ativo_w,
			dt_inicio_w,
			dt_fim_w,
			dt_liberacao_w,
			nr_prescricao_w
	from	pe_prescricao
	where	nr_sequencia = nr_seq_prescr_w;

	ie_administracao_w := 'P';
	ie_urgencia_w := '';
	if (ie_agora_w = 'S') then
		ie_urgencia_w := '0';
	elsif (ie_acm_w = 'S') then
		ie_administracao_w := 'C';
		ds_horarios_w := '';
	elsif (ie_se_necessario_w = 'S') then
		ie_administracao_w := 'N';
		ds_horarios_w := '';
	end if;

	if	(hr_prim_horario_w	is not null) then
		begin
		dt_inicio_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_inicio_w, nvl(substr(hr_prim_horario_w,1,5), '00:00')), 'mi');
		exception
		when others then
		dt_inicio_w	:= dt_inicio_w;
		end;
	end if;

	select	cpoe_intervencao_seq.nextval
	into	nr_seq_cpoe_interv_w
	from	dual;

	insert into CPOE_INTERVENCAO (
					nr_sequencia,
					nr_seq_proc,
					nr_atendimento,
					cd_pessoa_fisica,
					cd_intervalo,
					nr_seq_topografia,
					hr_prim_horario,
					ie_administracao,
					ie_se_necessario,
					ie_acm,
					ie_urgencia,
					ie_lado,
					ds_horarios,
					ds_observacao,
					dt_inicio,
					dt_fim,
					dt_suspensao,
					dt_liberacao,
					ie_duracao,
					cd_prescritor,
					dt_prescricao,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_perfil_ativo
				) values (
					nr_seq_cpoe_interv_w,
					nr_seq_proc_w,
					nr_atendimento_p,
					cd_pessoa_fisica_w,
					cd_intervalo_w,
					nr_seq_topografia_w,
					hr_prim_horario_w,
					ie_administracao_w,
					ie_se_necessario_w,
					ie_acm_w,
					ie_urgencia_w,
					ie_lado_w,
					ds_horarios_w,
					ds_observacao_w,
					dt_inicio_w,
					dt_fim_w,
					dt_suspensao_w,
					dt_liberacao_w,
					'P',
					cd_prescritor_w,
					dt_prescricao_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_perfil_ativo_w
				);

	update	pe_prescr_proc
	set		nr_seq_cpoe_interv = nr_seq_cpoe_interv_w
	where	nr_sequencia = nr_sequencia_p
	and		nr_seq_prescr = nr_seq_prescr_p;

	if (obter_se_prescr_CPOE(nr_prescricao_w) = 'N') then
		cpoe_rep_gerar_itens( nr_prescricao_w, nr_atendimento_p, dt_inicio_w, dt_fim_w, dt_liberacao_w, nm_usuario_p, cd_pessoa_fisica_w, 'N');
	end if;
	if (ie_gerar_prescr_evol_w = 'S' ) then
		clinical_notes_pck.gerar_soap(nr_atendimento_p ,nr_seq_cpoe_interv_w,'CPOE_ITEMS',7 ,'P',1,cd_evolucao_w,nr_prescricao_w,1);
		update cpoe_intervencao
		set cd_evolucao = cd_evolucao_w
		where nr_sequencia = nr_seq_cpoe_interv_w;
    end if;
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end cpoe_gerar_registro_sae;
/
