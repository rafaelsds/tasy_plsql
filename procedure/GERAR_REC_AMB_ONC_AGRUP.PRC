create or replace
procedure gerar_rec_amb_onc_agrup(	nr_seq_paciente_p	number,
									nr_ciclo_inicial_p	number,
									nr_ciclo_final_p	number,
									nr_atendimento_p	number,
									nm_usuario_p		varchar2,
									ie_gerado_p		out varchar2) is 

cd_material_w				number(10);
cd_unid_med_prescr_w		varchar2(30);									
ie_via_aplicacao_w			varchar2(15);
qt_dose_prescricao_w		number(18,6);
cd_intervalo_w				varchar2(10);
nr_seq_receita_w			number(10);
cd_medico_w					varchar2(10);
nr_receita_w				fa_receita_farmacia.nr_receita%type;
nr_serie_w					varchar2(15);
ds_retorno_w				varchar2(255);
nr_atendimento_w			number(10);
cd_pessoa_fisica_w			varchar2(10);
ie_segunda_w				varchar2(10);
ie_terca_w					varchar2(10);
ie_quarta_w					varchar2(10);
ie_quinta_w					varchar2(10);
ie_sexta_w					varchar2(10);
ie_sabado_w					varchar2(10);
ie_domingo_w				varchar2(10);
ds_texto_padrao_receita_w	varchar2(4000);
ds_dias_semana_w			varchar2(255);
ie_exige_laudo_w			varchar2(10);
qt_dias_util_w				number(5);
ie_uso_continuo_w			varchar2(1);
ie_se_necessario_w			varchar2(20);
ds_se_necessario_w      	varchar2(1);
ds_observacao_w				varchar2(4000);
nr_ciclo_w					number(10);
nr_dias_receita_w			number(5);
ds_dose_dif_1_w				number(10);
ds_dose_dif_2_w				number(10);
ds_dose_dif_3_w				number(10);
ds_dose_diferenciada_w		varchar2(100);
ie_libera_receita_w			varchar2(5);
cd_estabelecimento_w		number(10);
ie_tipo_receita_w			varchar2(15);
ie_se_necessario_atend_w	varchar2(1);

x						varchar2(1);
k 						integer := 1;
buff_w					varchar2(255);
qt_dose_total_w			number(18,5) := 0;
ds_dose_w				varchar2(255);
nr_dias_receita_ant_w	number(5) := 0;
nr_seq_atendimento_w	paciente_atend_medic.nr_seq_atendimento%type;
nr_seq_pac_entrega_w    fa_paciente_entrega.nr_sequencia%type;
ie_gera_registro_entrega_w		varchar2(1);

cursor C01 is
	select	a.cd_material,
			a.ie_via_aplicacao,
			nvl(a.cd_unid_med_prescr, a.cd_unid_med_dose), --nvl(a.cd_unid_med_prescr,''),
			nvl(a.qt_dose_prescricao,0),
			a.cd_intervalo,
			a.qt_dias_util,
			a.ie_uso_continuo,
			a.ds_observacao,
			a.ds_dose_diferenciada,
			a.ie_se_necessario,
			a.qt_dias_receita			
	from	paciente_atend_medic a,
			paciente_atendimento b
	where	a.nr_seq_atendimento	= b.nr_seq_atendimento
	and		b.nr_seq_paciente		= nr_seq_paciente_p
	and		b.nr_ciclo between nr_ciclo_inicial_p and nr_ciclo_final_p
	and		a.ie_local_adm			= 'C'
	and		a.nr_seq_solucao is null
	and		a.nr_seq_diluicao is null
	and		a.dt_cancelamento is null
	group by 
		a.cd_material,
		a.ie_via_aplicacao,
		nvl(a.cd_unid_med_prescr, a.cd_unid_med_dose), --a.cd_unid_med_prescr,
		a.qt_dose_prescricao,
		a.cd_intervalo,
		a.qt_dias_util,
		a.ie_uso_continuo,
		a.ds_observacao,
		ds_dose_diferenciada,
		a.ie_se_necessario,
		a.qt_dias_receita		
	order by 
		a.qt_dias_receita;

			function Obter_Se_Dia_Receita(	nr_dia_p	varchar2) 
							return varchar2 is
			
			qt_reg_w	number(10);
			
			begin
			
				select	count(*)
				into	qt_reg_w
				from	paciente_atend_medic a,
						paciente_atendimento b
				where	a.nr_seq_atendimento	= b.nr_seq_atendimento
				and		b.nr_seq_paciente		= nr_seq_paciente_p
				and		b.nr_ciclo between nr_ciclo_inicial_p and nr_ciclo_final_p
				and		a.ie_local_adm	= 'C'
				and		PKG_DATE_UTILS.get_WeekDay(b.dt_prevista) = nr_dia_p
				and		a.cd_material	= cd_material_w
				and		a.cd_unid_med_prescr	= cd_unid_med_prescr_w
				and		nvl(a.ie_via_aplicacao,nvl(ie_via_aplicacao_w,'XPTO'))	= nvl(ie_via_aplicacao_w,'XPTO')
				and		nvl(a.cd_intervalo,nvl(cd_intervalo_w,'XPTO'))			= nvl(cd_intervalo_w,'XPTO')		
				and		a.nr_seq_solucao is null
				and		a.nr_seq_diluicao is null;
				
				if	(qt_reg_w	> 0) then
					return 'S';
				else
					return 'N';
				end if;

			end;

begin

cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

select	max(cd_pessoa_fisica),
		max(cd_medico_resp)
into	cd_pessoa_fisica_w,
		cd_medico_w
from	paciente_setor
where	nr_seq_paciente	= nr_seq_paciente_p;

nr_atendimento_w	:= nvl(nr_atendimento_p,OBTER_ULTIMO_ATENDIMENTO(cd_pessoa_fisica_w));

obter_param_usuario(281,1305,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_libera_receita_w);
obter_param_usuario(10015,114,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_tipo_receita_w);
obter_param_usuario(281,1490,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_gera_registro_entrega_w);

open C01;
loop
fetch C01 into	
	cd_material_w,
	ie_via_aplicacao_w,
	cd_unid_med_prescr_w,
	qt_dose_prescricao_w,
	cd_intervalo_w,
	qt_dias_util_w,
	ie_uso_continuo_w,
	ds_observacao_w,
	ds_dose_diferenciada_w,
	ie_se_necessario_atend_w,
	nr_dias_receita_w;
exit when C01%notfound;
	begin
		if	((nr_seq_receita_w	is null) or
			 (nr_dias_receita_w <> nr_dias_receita_ant_w)) then

			select	FA_RECEITA_FARMACIA_seq.nextval
			into	nr_seq_receita_w
			from	dual;

			ds_retorno_w	:= FA_OBTER_NUMERO_RECEITA(nr_atendimento_w);

			nr_receita_w	:= substr(substr(ds_retorno_w,1,instr(ds_retorno_w,'-')-1),1,15);
			nr_serie_w	:= substr(substr(ds_retorno_w,instr(ds_retorno_w,'-')+1,length(ds_retorno_w)),1,15);
			
			select 	min(b.nr_seq_atendimento)
			into	nr_seq_atendimento_w
			from	paciente_atendimento b
			where	b.nr_seq_paciente	= nr_seq_paciente_p;
					
			insert	into fa_receita_farmacia(	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_atendimento,
								cd_medico,
								nr_receita,
								dt_receita,
								dt_liberacao,
								nr_serie,
								cd_pessoa_fisica,
								nr_seq_paciente,
								nr_ciclo,
								nr_dias_receita,
								ie_tipo_receita, 
								cd_estabelecimento,
								dt_inicio_receita,
								dt_validade_receita,
								nr_seq_atendimento
								)
					values		(	nr_seq_receita_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_atendimento_w,
								cd_medico_w,
								nr_receita_w,
								sysdate,
								null,
								nr_serie_w,
								cd_pessoa_fisica_w,
								nr_seq_paciente_p,
								nr_ciclo_inicial_p,
								nr_dias_receita_w,
								nvl(ie_tipo_receita_w,'Q'),
								cd_estabelecimento_w,
								sysdate,
								decode(nvl(nr_dias_receita_w,0),0,null,sysdate + nr_dias_receita_w),
								nr_seq_atendimento_w);
			ie_gerado_p	:= 'S';
			nr_dias_receita_ant_w := nr_dias_receita_w;	
			
			if  (ie_gera_registro_entrega_w = 'S') then

                select	fa_paciente_entrega_seq.nextval
                into	nr_seq_pac_entrega_w
                from	dual;

                insert	into fa_paciente_entrega( nr_sequencia,
                    dt_atualizacao,
                    nm_usuario,
                    dt_atualizacao_nrec,
                    nm_usuario_nrec,
                    nr_atendimento,
                    cd_pessoa_fisica,
                    dt_entrada,
                    ie_status_paciente,
                    cd_estabelecimento)
                values	(nr_seq_pac_entrega_w,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    nr_atendimento_p,
                    cd_pessoa_fisica_w,
                    sysdate,
                    'TR', /*corresponde ao status de triagem da entrega*/
                    cd_estabelecimento_w
                    );
            end if;

		end if;
		
		if	(ie_uso_continuo_w = 'S') or
			(qt_dias_util_w is not null) then
			ie_segunda_w			:= 'S';
			ie_terca_w			:= 'S';
			ie_quarta_w			:= 'S';
			ie_quinta_w			:= 'S';
			ie_sexta_w			:= 'S';
			ie_sabado_w			:= 'S';
			ie_domingo_w			:= 'S';
		else	
			ie_segunda_w			:= Obter_Se_Dia_Receita(2);
			ie_terca_w			:= Obter_Se_Dia_Receita(3);
			ie_quarta_w			:= Obter_Se_Dia_Receita(4);
			ie_quinta_w			:= Obter_Se_Dia_Receita(5);
			ie_sexta_w			:= Obter_Se_Dia_Receita(6);
			ie_sabado_w			:= Obter_Se_Dia_Receita(7);
			ie_domingo_w			:= Obter_Se_Dia_Receita(1);
		end if;
		
		if	(ie_se_necessario_atend_w = 'S') and
			(qt_dias_util_w is null) and
			(ie_uso_continuo_w = 'N') then
			ie_se_necessario_w 		:= '';
			ds_se_necessario_w      := 'S';
		else
			ds_se_necessario_w      := 'N';
			ie_se_necessario_w 		:= obter_desc_expressao(746576);
		end if;
		
		select	max(ds_texto_padrao_receita)
		into	ds_texto_padrao_receita_w
		from	FA_MEDIC_FARMACIA_AMB
		where	cd_material	= cd_material_w;
			
		if	(ds_dose_diferenciada_w is not null) then
			
			qt_dose_prescricao_w := 0;
			
			for i in 1..length(ds_dose_diferenciada_w) loop
				begin
				x	:= substr(ds_dose_diferenciada_w, i, 1);
				if	(x <> '-') then
					buff_w := buff_w || x;
				end if;
				if	(x = '-') or (i = length(ds_dose_diferenciada_w)) then
					if 	(buff_w = '1/2') then
						buff_w := to_char(0.5);
					elsif	(nvl(somente_numero_char(buff_w),'0') <> buff_w) then
						buff_W := to_char(0);
					end if;
					qt_dose_prescricao_w := qt_dose_prescricao_w + to_number(buff_w);
					buff_w := '';
					k := k + 1;
				end if;
				end;
			end loop;
		end if;

		if	(ds_texto_padrao_receita_w	is not null) then	
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@medicamento',OBTER_DESC_MATERIAL(cd_material_w));
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@dose',qt_dose_prescricao_w);
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@orientacao',fa_obter_orientacao_medic(cd_material_w));
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@unidade',Obter_Unidade_Medida(cd_unid_med_prescr_w));
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@intervalo',OBTER_DESC_INTERVALO(cd_intervalo_w));
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@via',Obter_Via_Aplicacao(ie_via_aplicacao_w,'D'));
			
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@dias_receita',qt_dias_util_w);
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@nr_ciclos','');
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@uso_continuo','');
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@observacao',ds_observacao_w);
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@se_necessario',ie_se_necessario_w);		
			
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@dif_dose_1',nvl(ds_dose_dif_1_w,''));
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@dif_dose_2',nvl(ds_dose_dif_2_w,''));
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@dif_dose_3',nvl(ds_dose_dif_3_w,''));
			
			ds_dias_semana_w	:= null;
			if	(ie_segunda_w	='S') then
				ds_dias_semana_w	:= ds_dias_semana_w||' '||obter_desc_expressao(298104); -- segunda
			end if;
			
			if	(ie_terca_w	='S') then
				ds_dias_semana_w	:= ds_dias_semana_w||' '||obter_desc_expressao(299301);--terca
			end if;
			
			if	(ie_quarta_w	='S') then
				ds_dias_semana_w	:= ds_dias_semana_w||' '||obter_desc_expressao(297137);-- quarta
			end if;
			
			if	(ie_quinta_w	='S') then
				ds_dias_semana_w	:= ds_dias_semana_w||' '||obter_desc_expressao(297213);-- quinta
			end if;
		
			if	(ie_sexta_w	='S') then
				ds_dias_semana_w	:= ds_dias_semana_w||' '||obter_desc_expressao(298487); --sexta
			end if;

			if	(ie_sabado_w	='S') then
				ds_dias_semana_w	:= ds_dias_semana_w||' '||obter_desc_expressao(297960); --sabado
			end if;
		
			if	(ie_domingo_w	='S') then
				ds_dias_semana_w	:= ds_dias_semana_w||' '||obter_desc_expressao(288200); --domingo
			end if;
			ds_texto_padrao_receita_w	:= replace(ds_texto_padrao_receita_w,'@dias_semana',ds_dias_semana_w);
		end if;

		insert into fa_receita_farmacia_item(	nr_sequencia,	
							dt_atualizacao,
							nm_usuario,
							nr_seq_receita,
							cd_material,
							qt_dose,
							ie_via_aplicacao,
							cd_unidade_medida,
							cd_intervalo,
							ie_segunda,
							ie_terca       ,
							ie_quarta,      
							ie_quinta ,     
							ie_sexta  ,     
							ie_sabado  ,    
							ie_domingo,
							ds_texto_receita ,
							nr_dias_receita,
							ie_uso_continuo,
							ie_se_necessario,
							ds_dose_diferenciada,
							dt_inicio_receita,
							dt_validade_receita)
				values(		fa_receita_farmacia_item_seq.nextval,
							sysdate,
							nm_usuario_p,
							nr_seq_receita_w,
							cd_material_w,
							qt_dose_prescricao_w,
							ie_via_aplicacao_w,
							cd_unid_med_prescr_w,
							cd_intervalo_w,
							ie_segunda_w,
							ie_terca_w,
							ie_quarta_w,
							ie_quinta_w,
							ie_sexta_w,
							ie_sabado_w,
							ie_domingo_w,
							ds_texto_padrao_receita_w,
							qt_dias_util_w,
							ie_uso_continuo_w,
							ds_se_necessario_w,
							ds_dose_diferenciada_w,
							sysdate,
							decode(nvl(qt_dias_util_w,0),0,null,sysdate + qt_dias_util_w));

	end;

	if	(nvl(ie_libera_receita_w,'S') = 'S') then
		fa_lib_receita_parcial (nr_seq_receita_w,nm_usuario_p);		
	elsif	(nvl(ie_libera_receita_w,'N') = 'T') then
		fa_lib_receita_total(nr_seq_receita_w,nm_usuario_p);		
	end if;

end loop;
close C01;

commit;

end gerar_rec_amb_onc_agrup;
/