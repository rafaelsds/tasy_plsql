CREATE OR REPLACE
PROCEDURE SUS_Import_Rubrica
			(	nr_aih_p		Number,
				cd_rubrica_p		Number,
				ie_situacao_p		Number,
				nr_seq_protocolo_p	Number,
				vl_aih_p		Number,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2,
				ds_retorno_p	out	Varchar2) is

dt_apresentacao_w	Date;
nr_seq_aih_w		Number(10);
nr_processo_w		Number(10);
ds_retorno_w		Varchar2(80)	:= 'OK';
qt_aih_w		Number(2)	:= 0;
nr_proc_paga_w		Number(10)	:= 0;
nr_proc_rej_w		Number(10)	:= 0;
nr_Atendimento_w	Number(10);
cd_medico_resp_w	Varchar2(10);
cd_pessoa_fisica_w	Varchar2(10);
cd_procedimento_w	Number(15);


BEGIN

begin
select	nr_Atendimento,
	cd_medico_responsavel,
	nr_sequencia,
	obter_procedimento_aih(nr_interno_conta,'E'),
	count(*)
into	nr_Atendimento_w,
	cd_medico_resp_w,
	nr_seq_aih_w,
	cd_procedimento_w,
	qt_aih_w
from	sus_aih
where	nr_aih	= nr_aih_p
and	nr_sequencia =	(select	max(nr_sequencia)
			from	sus_aih
			where	nr_aih	= nr_aih_p)
group by	nr_Atendimento,
		cd_medico_responsavel,
		nr_sequencia,
		nr_interno_conta;
exception
	when others then
		qt_aih_w	:= 0;
end;

/* Verificar se a AIH j� foi importada como Pagas */
select	nvl(max(nr_processo),0)
into	nr_proc_paga_w
from	sus_aih_paga
where	nr_aih		= nr_aih_p
and	nr_seq_aih	= nr_seq_aih_w;

if	(nr_proc_paga_w	> 0) then
	ds_retorno_w	:= 'Paga';
	goto final;	
end if;

/* Verificar se a AIH j� foi importada como Rejeitada */
select	nvl(max(nr_processo),0)
into	nr_proc_rej_w
from	sus_aih_rejeitada
where	nr_aih		= nr_aih_p
and	nr_seq_aih	= nr_seq_aih_w;

if	(nr_proc_rej_w	> 0) then
	ds_retorno_w	:= 'Rej';
	goto final;	
end if;

select	nvl(max(cd_pessoa_fisica),'')
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_w;

select	dt_mesano_referencia
into	dt_apresentacao_w
from	protocolo_convenio
where	nr_seq_protocolo	= nr_Seq_protocolo_p;

select	nvl(max(nr_processo),0)
into	nr_processo_w
from	sus_aih_processo
where	nr_seq_protocolo	= nr_seq_protocolo_p
and	ie_tipo_processo	= cd_rubrica_p;

if	(qt_aih_w	> 0) then
	if	(nr_processo_w	= 0) then
		select	sus_aih_processo_seq.nextval
		into	nr_processo_w
		from	dual;

		insert into sus_aih_processo
			(nr_processo,
			dt_competencia,
			ie_tipo_processo,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_protocolo,
			cd_estabelecimento)
		values(	nr_processo_w,
			dt_apresentacao_w,
			cd_rubrica_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_protocolo_p,
			cd_estabelecimento_p);
	end if;

	if	(ie_situacao_p	= 0) then
		insert into sus_aih_paga
			(nr_sequencia,
			nr_aih,
			nr_processo,
			dt_apresentacao,
			dt_atualizacao,
			nm_usuario,
			nr_seq_aih,
			nr_seq_protocolo,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_atendimento,
			cd_pessoa_fisica,
			cd_medico_responsavel,
			cd_proc_realizado,
			ie_origem_proced,
			vl_aih)
		values(	sus_aih_paga_seq.nextval,
			nr_aih_p,
			nr_processo_w,
			dt_apresentacao_w,
			sysdate,
			nm_usuario_p,
			nr_seq_aih_w,
			nr_seq_protocolo_p,
			sysdate,
			nm_usuario_p,
			nr_atendimento_w,
			cd_pessoa_fisica_w,
			cd_medico_resp_w,
			cd_procedimento_w,
			2,
			vl_aih_p);

	elsif	(ie_situacao_p	= 1) then
		insert into sus_aih_rejeitada
			(nr_sequencia,
			dt_apresentacao,
			nr_aih,
			nr_processo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_protocolo,
			nr_seq_aih,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_atendimento,
			cd_pessoa_fisica,
			cd_medico_responsavel,
			cd_proc_realizado,
			ie_origem_proced)
		values(	sus_aih_rejeitada_seq.nextval,
			dt_apresentacao_w,
			nr_aih_p,
			nr_processo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_protocolo_p,
			nr_seq_aih_w,
			sysdate,
			nm_usuario_p,
			nr_atendimento_w,
			cd_pessoa_fisica_w,
			cd_medico_resp_w,
			cd_procedimento_w,
			2);

	end if;
else
	ds_retorno_w	:= 'ERRO';
end if;

<<final>>

ds_retorno_p	:= ds_retorno_w;

commit;

END SUS_Import_Rubrica;
/