create or replace
procedure consistir_perda_ganho_disp(	nr_atendimento_p	number,
					nr_seq_tipo_p		number,
					dt_medida_p		date,
					nm_usuario_p		varchar2,
					ds_consistencia_p	out varchar2) is 
					
ie_exige_dispositivo_w	varchar2(1);
nr_seq_dispositivo_w	number(10,0);
ie_instalado_w		varchar2(1);
qt_dispositivo_w	number(3,0);
ds_perda_ganho_w	varchar2(50);
ie_perda_ganho_w	varchar2(1);
					
begin

select	nvl(max(ie_exige_dispositivo),'N')
into	ie_exige_dispositivo_w
from	tipo_perda_ganho
where	nr_sequencia	=	nr_seq_tipo_p;

select	count(*)
into	qt_dispositivo_w
from	tipo_ganho_perda_disp
where	nr_seq_ganho_perda = nr_seq_tipo_p;

if	(ie_exige_dispositivo_w = 'S') and (qt_dispositivo_w > 0) then
	select	nvl(max('S'),'N')
	into	ie_instalado_w
	from	tipo_ganho_perda_disp
	where	nr_seq_ganho_perda = nr_seq_tipo_p
	and	obter_se_disp_instal_data(nr_atendimento_p, nr_seq_dispositivo, dt_medida_p) = 'S';

	if	(ie_instalado_w = 'N') then
	
		select	substr(obter_tipo_perda_ganho(nr_seq_tipo_p,'C'),1,40)
		into	ie_perda_ganho_w
		from	dual;
		
		if	(ie_perda_ganho_w = 'P') then
			ds_perda_ganho_w	:= Wheb_mensagem_pck.get_texto(306547); --'esta perda';
		elsif	(ie_perda_ganho_w = 'G') then
			ds_perda_ganho_w	:= Wheb_mensagem_pck.get_texto(306548); --'este ganho';
		end if;
	
		ds_consistencia_p := Wheb_mensagem_pck.get_texto(306545, 'DS_PERDA_GANHO_W='||ds_perda_ganho_w); -- 'N�o � poss�vel registrar ' || ds_perda_ganho_w || ', pois n�o h� dispositivos no paciente que permitam este controle.';
	end if;
end if;

end consistir_perda_ganho_disp;
/