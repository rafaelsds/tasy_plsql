create or replace
procedure generate_functionality_test(nm_usuario_p		varchar2) is

cd_versao_w						varchar2(255);
dt_inicio_periodo_w				date;
dt_fim_periodo_w				date;
qt_casos_w						number(10);
qt_aprovacoes_w					number(10);
qt_reprovacoes_w				number(10);
qt_sem_resultado_w				number(10);
avg_min_correcoes_w				number(10);
qt_os_abertas_w					number(10);
qt_os_aprovadas_encerradas_w	number(10);

nr_os_origem_w					number(10);
nr_os_defeito_w					number(10);
dt_os_defeito_w					date;
dt_fim_os_defeito_w				date;
qt_min_os_defeito_w				number(10);
qt_min_dev_w					number(10);
qt_min_vv_w						number(10);
qt_min_aguardando_w				number(10);


cursor c01 is
	select  cd_versao,
			dt_versao + 1 dt_inicio_periodo,
			dt_versao + 14 dt_fim_periodo
	from    aplicacao_tasy_versao
	where   dt_versao +14 between trunc(sysdate, 'mm') and sysdate
	and     upper(cd_aplicacao_tasy) = 'TASY';

cursor c02 is
	select	nvl(s.nr_os_origem, 0),
			nvl(s.nr_os_defeito, 0),
			s.dt_os_defeito,
			s.dt_fim_os_defeito,
			nvl(s.qt_min_os_defeito, 0),
			nvl(s.qt_min_dev, 0),
			nvl(s.qt_min_vv, 0),
			nvl(s.qt_min_os_defeito - (s.qt_min_dev + s.qt_min_vv), 0) qt_min_aguardando
	from (
		select t.*
				,(select sum(x.qt_minuto) qt_min_dev
					from corp.man_ordem_serv_ativ x
					where x.nr_seq_ordem_serv = t.nr_os_defeito
					and nr_seq_funcao        in(11, 551, 1288, 31) -- 11,551,1288 programa��o ; 31 an�lise
				)qt_min_dev
				,(select sum(x.qt_minuto) qt_min_vv
					from corp.man_ordem_serv_ativ x
					where x.nr_seq_ordem_serv = t.nr_os_defeito
					and nr_seq_funcao        in(601, 732) -- 601 teste valida��o os; 732 abertura os
				)qt_min_vv
				
		from
		(
			select distinct o.nr_versao_cliente_abertura
				,o.nr_seq_origem nr_os_origem
				,o.nr_sequencia nr_os_defeito
				,o.dt_ordem_servico dt_os_defeito
				,o.dt_fim_real dt_fim_os_defeito
				,corp.man_obter_min_com(1, o.dt_ordem_servico, o.dt_fim_real) qt_min_os_defeito
			from corp.man_ordem_servico o
				,corp.grupo_desenvolvimento b
				,corp.gerencia_wheb c
				,corp.man_ordem_serv_tecnico h 
			where o.nr_seq_grupo_des   = b.nr_sequencia
			and b.nr_seq_gerencia  = c.nr_sequencia
			and o.nr_sequencia = h.nr_seq_ordem_serv
			and o.nr_versao_cliente_abertura is not null
			and o.nr_versao_cliente_abertura = cd_versao_w  -- versoes testadas no m�s
			and o.nr_seq_equipamento in (2897, 7259)
			and o.cd_pessoa_solicitante  <> '14335'
			and o.ie_classificacao = 'E' --somente defeito
			-- per�odos de teste das versoes
			and o.dt_ordem_servico between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy'))
			
			and h.nr_seq_tipo = 117 -- aprovadas
			-- per�odos de teste das versoes + 4 dias ap�s a a libera��o pois n�o deu tempo de encerrar as oss que foram entregues
			and h.dt_liberacao  between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w + 4, 'dd/mm/yyyy'))
		) t
	) s;

begin

	open c01;
	loop
	fetch c01 into	
		cd_versao_w,
		dt_inicio_periodo_w,
		dt_fim_periodo_w;
	exit when c01%notfound;
		begin

			/*os de casos de teste*/
			select	count(distinct o.nr_sequencia)
			into	qt_casos_w
			from	corp.man_ordem_servico o
					,corp.man_ordem_serv_tecnico h
			where	o.nr_sequencia = h.nr_seq_ordem_serv
			and		o.cd_pessoa_solicitante = 14335
			and		o.ds_dano_breve like 'CT%[%'
			and		(h.nr_seq_classif_vv is null or  h.nr_seq_classif_vv <> 163) --os aberta indevidamente 
			and		o.dt_ordem_servico between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy')); -- per�odos de teste das versoes

			/*aprovadas*/
			select	count(distinct o.nr_sequencia)
			into	qt_aprovacoes_w
			from	corp.man_ordem_serv_tecnico h
					,corp.man_ordem_servico o
			where	o.nr_sequencia = h.nr_seq_ordem_serv
			and		o.cd_pessoa_solicitante = 14335
			and		o.ds_dano_breve like 'CT%[%'
			and		h.nr_seq_tipo = 117
			and		o.dt_ordem_servico between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy')); -- per�odos de teste das versoes

			/*n�o aprovadas*/
			select	count(distinct o.nr_sequencia)
			into	qt_reprovacoes_w
			from	corp.man_ordem_serv_tecnico h
					,corp.man_ordem_servico o
			where	o.nr_sequencia = h.nr_seq_ordem_serv
			and		o.cd_pessoa_solicitante = 14335
			and		o.ds_dano_breve like 'CT%[%'
			and		h.nr_seq_tipo = 118
			and		o.dt_ordem_servico between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy')); -- per�odos de teste das versoes

			/*sem resultado*/
			select	count(distinct o.nr_sequencia)
			into	qt_sem_resultado_w
			from	corp.man_ordem_serv_tecnico h
					,corp.man_ordem_servico o
			where	o.nr_sequencia = h.nr_seq_ordem_serv
			and		o.cd_pessoa_solicitante = 14335
			and		o.ds_dano_breve like 'CT%[%'
			and		h.nr_seq_tipo = 122
			and		o.dt_ordem_servico between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy')); -- per�odos de teste das versoes

			/*minutos desde a abertura da os de corre��o at� o encerrramento da mesma*/
			select	distinct nvl(round(avg(corp.man_obter_min_com(1, o.dt_ordem_servico, o.dt_fim_real))), 0)
			into	avg_min_correcoes_w
			from	corp.man_ordem_servico o
					,corp.man_ordem_serv_tecnico h
			where	o.nr_sequencia = h.nr_seq_ordem_serv
			and		o.nr_versao_cliente_abertura is not null
			and		o.nr_seq_equipamento in (2897, 7259)
			and		o.cd_pessoa_solicitante  <> '14335'
			and		o.dt_ordem_servico between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy')); -- per�odos de teste das versoes
			--group by a.nr_sequencia,a.nr_seq_origem, a.dt_ordem_servico, a.dt_fim_real

			--n�mero de oss abertas pelo v&v no per�odo de testes
			select	count (distinct h.nr_seq_ordem_serv)
			into	qt_os_abertas_w
			from	corp.man_ordem_serv_tecnico h 
			where	h.nr_seq_ordem_serv in( select	distinct o.nr_sequencia
											from	corp.man_ordem_servico o
											where	o.nr_versao_cliente_abertura is not null
											and		o.nr_versao_cliente_abertura = cd_versao_w  -- versoes testadas no m�s
											and		o.nr_seq_equipamento in (2897, 7259)
											and		o.cd_pessoa_solicitante  <> '14335'
											and		o.ie_classificacao = 'E' --somente defeito
											and		o.dt_ordem_servico between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy')) -- per�odos de teste das versoes
											--           and o.nr_seq_severidade <>5
											);

			--n�mero de oss abertas pelo v&v no per�odo de testes
			select	count (distinct h.nr_seq_ordem_serv)
			into	qt_os_aprovadas_encerradas_w
			from	corp.man_ordem_serv_tecnico h 
			where	h.nr_seq_ordem_serv in( select	distinct o.nr_sequencia
											from	corp.man_ordem_servico o
											where	o.nr_versao_cliente_abertura is not null
											and		o.nr_versao_cliente_abertura = cd_versao_w  -- versoes testadas no m�s
											and		o.nr_seq_equipamento in (2897, 7259)
											and		o.cd_pessoa_solicitante  <> '14335'
											and		o.ie_classificacao = 'E' --somente defeito
											and		o.dt_ordem_servico between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy')) -- per�odos de teste das versoes
											--           and o.nr_seq_severidade <>5
											)
			and h.nr_seq_tipo = 117
			-- per�odos de teste das versoes + 4 dias ap�s a a libera��o pois n�o deu tempo de encerrar as oss que foram entregues
			and h.dt_liberacao  between to_date(to_char(dt_inicio_periodo_w, 'dd/mm/yyyy')) and to_date(to_char(dt_fim_periodo_w, 'dd/mm/yyyy')) + 4;

			insert into hit_functionality_test (	
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_versao,
				dt_inicio_periodo,
				dt_fim_periodo,
				qt_casos,
				qt_aprovacoes,
				qt_reprovacoes,
				qt_sem_resultado,
				avg_min_correcoes,
				qt_os_abertas,
				qt_os_aprovadas_encerradas,
				ie_plataforma)
			values(	hit_functionality_test_seq.nextval,
					sysdate,
					'TASY',
					sysdate,
					'TASY',
					cd_versao_w,
					dt_inicio_periodo_w,
					dt_fim_periodo_w,
					qt_casos_w,
					qt_aprovacoes_w,
					qt_reprovacoes_w,
					qt_sem_resultado_w,
					avg_min_correcoes_w,
					qt_os_abertas_w,
					qt_os_aprovadas_encerradas_w,
					'J');

					
			open c02;
			loop
			fetch c02 into	
				nr_os_origem_w,
				nr_os_defeito_w,
				dt_os_defeito_w,
				dt_fim_os_defeito_w,
				qt_min_os_defeito_w,
				qt_min_dev_w,
				qt_min_vv_w,
				qt_min_aguardando_w;
			exit when c02%notfound;
				begin

					insert into hit_test_fix_breaks (
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_os_origem,
						nr_os_defeito,
						dt_os_defeito,
						dt_fim_os_defeito,
						qt_min_os_defeito,
						qt_min_dev,
						qt_min_vv,
						qt_min_aguardando,
						cd_versao,
						ie_plataforma)
					values(	hit_test_fix_breaks_seq.nextval,
						sysdate,
						'TASY',
						sysdate,
						'TASY',
						nr_os_origem_w,
						nr_os_defeito_w,
						dt_os_defeito_w,
						dt_fim_os_defeito_w,
						qt_min_os_defeito_w,
						qt_min_dev_w,
						qt_min_vv_w,
						qt_min_aguardando_w,
						cd_versao_w,
						'J');

				end;
			end loop;
			close c02;
			
		end;
	end loop;
	close c01;


commit;

end generate_functionality_test;
/