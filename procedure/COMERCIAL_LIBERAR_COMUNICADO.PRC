create or replace
procedure comercial_liberar_comunicado(	nr_seq_comunicado_p	number,
					ie_operacao_p		varchar2,
					nm_usuario_p		varchar2) IS

begin

if	(ie_operacao_p = 'L') then

	update	comercial_comunicado
	set	dt_liberacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_comunicado_p;

elsif	(ie_operacao_p = 'D') then
	update	comercial_comunicado
	set	dt_liberacao	= null,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_comunicado_p;
end if;

commit;

end comercial_liberar_comunicado;
/