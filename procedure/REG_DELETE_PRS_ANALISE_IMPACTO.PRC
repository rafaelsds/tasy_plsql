create or replace procedure REG_DELETE_PRS_ANALISE_IMPACTO(nr_sequencia_p number) is

begin

  delete from man_os_imp_pr_quest where nr_seq_imp_pr = nr_sequencia_p;

  delete from man_ordem_serv_imp_pr where nr_sequencia = nr_sequencia_p;

  commit;

end REG_DELETE_PRS_ANALISE_IMPACTO;
/
