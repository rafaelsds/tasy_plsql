create or replace
procedure pls_gerar_pagador_complementar(nr_seq_pagador_p	in	pls_contrato_pagador.nr_sequencia%type,
					ds_pagador_p		in	pls_contrato_pagador.ds_pagador%type,
					nr_seq_classif_itens_p	in	pls_contrato_pagador.nr_seq_classif_itens%type,
					nr_seq_grupo_p		in	pls_grupo_item_mens.nr_sequencia%type,
					nm_usuario_p		in	usuario.nm_usuario%type,
					cd_estabelecimento_p	in	estabelecimento.cd_estabelecimento%type,
					nr_seq_pagador_compl_p	out	pls_contrato_pagador.nr_sequencia%type) is 

nr_seq_pagador_novo_w		pls_contrato_pagador.nr_sequencia%type;

Cursor C01 (	nr_seq_pagador_pc	pls_contrato_pagador.nr_sequencia%type) is
	select	nr_sequencia nr_seq_pagador_fin
	from	pls_contrato_pagador_fin
	where	nr_seq_pagador = nr_seq_pagador_pc;
		
Cursor C02 (	nr_seq_grupo_pc		pls_grupo_item_mens.nr_sequencia%type) is
	select	ie_tipo_item,
		decode(ie_tipo_item, 20, nr_seq_tipo_lanc, null) nr_seq_tipo_lanc
	from	pls_item_grupo_tipo_mens
	where	nr_seq_grupo_item = nr_seq_grupo_pc
	and	ie_situacao = 'A'
	and	ie_tipo_item in ('1','3','6','12','13','20');
	
begin

select	pls_contrato_pagador_seq.nextval
into	nr_seq_pagador_novo_w
from	dual;

insert into 	pls_contrato_pagador
	(	nr_sequencia, cd_agencia_bancaria, cd_banco, cd_cgc, cd_condicao_pagamento, 
		cd_conta, cd_pessoa_fisica, ds_email, ds_pagador, dt_atualizacao, 
		dt_atualizacao_nrec, dt_dia_vencimento, dt_primeira_mensalidade, ie_bonus_indicacao, ie_calc_primeira_mens, 
		ie_calculo_proporcional, ie_digito_agencia, ie_digito_conta, ie_endereco_boleto, ie_envia_cobranca, 
		ie_finalidade, ie_inadimplencia_via_adic, ie_liberado_proposta, ie_notificacao, ie_pag_ref_grupo_intercambio, 
		ie_pessoa_comprovante, ie_situacao_trabalhista, ie_taxa_emissao_boleto, nm_usuario, nm_usuario_nrec,
		nr_seq_classif_itens, nr_seq_compl_pf_tel_adic, nr_seq_compl_pj, nr_seq_congenere, nr_seq_conta_banco,
		nr_seq_contrato, nr_seq_destino_corresp, nr_seq_forma_cobranca, nr_seq_motivo_alt_pag, nr_seq_motivo_cancelamento,
		nr_seq_pagador_ant, nr_seq_pagador_compl, nr_seq_pagador_intercambio, nr_seq_regra_obito, nr_seq_seg_mig_origem,
		nr_seq_tipo_compl_adic, nr_seq_vinculo_sca, ie_tipo_pagador, ie_receber_sms)
	(select	nr_seq_pagador_novo_w, cd_agencia_bancaria, cd_banco, cd_cgc, cd_condicao_pagamento, 
		cd_conta, cd_pessoa_fisica, ds_email, ds_pagador_p, sysdate, 
		sysdate, dt_dia_vencimento, dt_primeira_mensalidade, ie_bonus_indicacao, ie_calc_primeira_mens, 
		ie_calculo_proporcional, ie_digito_agencia, ie_digito_conta, ie_endereco_boleto, ie_envia_cobranca, 
		ie_finalidade, ie_inadimplencia_via_adic, ie_liberado_proposta, ie_notificacao, ie_pag_ref_grupo_intercambio, 
		ie_pessoa_comprovante, ie_situacao_trabalhista, ie_taxa_emissao_boleto, nm_usuario_p, nm_usuario_p,
		nr_seq_classif_itens_p, nr_seq_compl_pf_tel_adic, nr_seq_compl_pj, nr_seq_congenere, nr_seq_conta_banco,
		nr_seq_contrato, nr_seq_destino_corresp, nr_seq_forma_cobranca, nr_seq_motivo_alt_pag, nr_seq_motivo_cancelamento,
		nr_seq_pagador_ant, nr_seq_pagador_compl, nr_seq_pagador_intercambio, nr_seq_regra_obito, nr_seq_seg_mig_origem,
		nr_seq_tipo_compl_adic, nr_seq_vinculo_sca, 'S', 'S'
	from	pls_contrato_pagador
	where	nr_sequencia = nr_seq_pagador_p);

for r_c01_w in c01 (nr_seq_pagador_p) loop
	begin
	insert into 	pls_contrato_pagador_fin
		(	nr_sequencia, dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
			nr_seq_pagador, cd_autenticidade, cd_banco, cd_condicao_pagamento, cd_conta,
			cd_matricula, cd_portador, cd_portador_deb_aut, cd_profissao, cd_tipo_portador,
			cd_tipo_portador_deb_aut, ds_observacao, dt_dia_vencimento, dt_fim_vigencia, dt_inicio_vigencia,
			ie_boleto_email, ie_destacar_reajuste, ie_digito_agencia, ie_digito_conta, ie_geracao_nota_titulo,
			ie_gerar_cobr_escrit, ie_mes_vencimento, ie_portador_exclusivo, nm_usuario_fim_vigencia, nr_pensionista,
			nr_seq_carteira_cobr, nr_seq_conta_banco, nr_seq_conta_banco_deb_aut, nr_seq_dia_vencimento, nr_seq_empresa,
			nr_seq_forma_cobranca, nr_seq_mot_cob, cd_agencia_bancaria, nr_seq_vinculo_empresa, nr_seq_vinculo_est)
		(select	pls_contrato_pagador_fin_seq.nextval, sysdate, sysdate, nm_usuario_p, nm_usuario_p,
			nr_seq_pagador_novo_w, cd_autenticidade, cd_banco, cd_condicao_pagamento, cd_conta,
			cd_matricula, cd_portador, cd_portador_deb_aut, cd_profissao, cd_tipo_portador,
			cd_tipo_portador_deb_aut, ds_observacao, dt_dia_vencimento, dt_fim_vigencia, dt_inicio_vigencia,
			ie_boleto_email, ie_destacar_reajuste, ie_digito_agencia, ie_digito_conta, ie_geracao_nota_titulo,
			ie_gerar_cobr_escrit, ie_mes_vencimento, ie_portador_exclusivo, nm_usuario_fim_vigencia, nr_pensionista,
			nr_seq_carteira_cobr, nr_seq_conta_banco, nr_seq_conta_banco_deb_aut, nr_seq_dia_vencimento, nr_seq_empresa,
			nr_seq_forma_cobranca, nr_seq_mot_cob, cd_agencia_bancaria, nr_seq_vinculo_empresa, nr_seq_vinculo_est
		from	pls_contrato_pagador_fin
		where	nr_sequencia = r_c01_w.nr_seq_pagador_fin);
	end;
end loop;

for r_c02_w in c02 (nr_seq_grupo_p) loop
	begin	
	insert into	pls_pagador_item_mens
		(	nr_sequencia, dt_atualizacao, dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
			ie_tipo_item, nr_seq_pagador, nr_seq_pagador_item, cd_estabelecimento, nr_seq_tipo_lanc)
	values	(	pls_pagador_item_mens_seq.nextval, sysdate, sysdate, nm_usuario_p, nm_usuario_p,
			r_c02_w.ie_tipo_item, nr_seq_pagador_p, nr_seq_pagador_novo_w, cd_estabelecimento_p, r_c02_w.nr_seq_tipo_lanc);
	end;
end loop;

nr_seq_pagador_compl_p	:= nr_seq_pagador_novo_w;

commit;

end pls_gerar_pagador_complementar;
/