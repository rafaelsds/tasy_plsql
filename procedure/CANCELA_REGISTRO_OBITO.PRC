create or replace
procedure cancela_registro_obito(
nr_atendimento_p number,
nr_declaracao_p varchar2, 
ds_motivo_cancelado_p varchar2,
ie_motivo_cancelamento_p varchar2,
nm_usuario_p varchar2
) is
 tipo_numeracao_w	regra_numeracao_declaracao.ie_tipo_numeracao%type;
begin
	select * 
	into tipo_numeracao_w
	from (
		select  a.ie_tipo_numeracao
		from	regra_numeracao_declaracao a
		where	a.ie_situacao	= 'A'
		and	a.ie_tipo_numeracao	= 'DO'
		and	not exists (select 1 from declaracao_obito x where x.nr_declaracao = ltrim(nr_declaracao_p, '0') and x.ie_situacao = 'A')
		and not exists(select 1
						from	nascimento x
						where	x.ie_unico_nasc_vivo = 'N'
						and	x.nr_atend_rn = nr_atendimento_p)
		union all
		select  a.ie_tipo_numeracao
		from	regra_numeracao_declaracao a
		where		a.ie_situacao	= 'A'
		and	a.ie_tipo_numeracao	= 'DF'
		and	not exists (select 1 from declaracao_obito x where x.nr_declaracao = ltrim(nr_declaracao_p, '0') and x.ie_situacao = 'A')
		and exists(select 1
						from	nascimento x
						where	x.ie_unico_nasc_vivo = 'N'
						and	x.nr_atend_rn = nr_atendimento_p));
	
if 	(nr_declaracao_p is not null) then
	update	regra_numeracao_dec_item a
	set	a.dt_cancelamento = sysdate,
        a.ie_disponivel = 'C',
        a.ds_motivo_cancelado = ds_motivo_cancelado_p,
        a.ie_motivo_cancelamento = ie_motivo_cancelamento_p,
        a.dt_atualizacao = sysdate,
        a.nm_usuario = nm_usuario_p
	where a.nr_declaracao = nr_declaracao_p 
    and exists	(select	1 from regra_numeracao_declaracao b 
    where a.NR_SEQ_REGRA_NUM = b.nr_sequencia 
    and b.ie_situacao = 'A' and b.ie_tipo_numeracao = tipo_numeracao_w);
	commit;
end if;
end cancela_registro_obito;
/