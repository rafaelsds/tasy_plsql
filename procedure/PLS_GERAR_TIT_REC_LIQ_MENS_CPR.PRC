create or replace
procedure pls_gerar_tit_rec_liq_mens_cpr
			(	nr_titulo_p			number,
				nr_seq_baixa_p			number,
				nm_usuario_p			varchar2,
				nr_titulo_contab_p	out	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a classifica��o das baixas de mensalidade, conforme IN 46.

Gera as classifica��es da baixa do t�tulo conforme os itens de mensalidade, como cada item
de mensalidade possui v�rias contas tais como de cr�dito e d�bito, de antecipa��o e de revers�o
de antecipa��o, para um mesmo item podem ser geradas v�rias linhas de classifica��o
conforme as contas cont�beis informadas nele.

Caso a classifica��o gerada for referente � antecipa��o, sua data � colocada para o m�s
seguinte ao de referencia da mensalidade e o tipo de lan�amento � 'EA'

dt_ref_contab_inicial_w e dt_ref_contab_final_w, s�o referentes ao per�do(m�s) em que
o benefici�rio tem direito � utilizar o produto, se baseia na data de ades�o do benefici�rio.


A data de antecipa��o se encontra no lote de mensalidades, Campo "Dt antecipa��o"

o campo NR_SEQ_ITEM_MENS da tabela PLS_TITULO_REC_LIQ_MENS �
	utilizado para verificar quais lan�amentos foram feitos para cada item da mensalidade
	com ele � poss�vel saber em quais poss�veis situa��es o t�tulo est�.

-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	N�o alterar os valores das vari�veis vindas do cursor, criar vari�veis para esta situa��o.

	Pr�-requisitos:
		A mensalidade possuir os itens de mensalidade com as contas cont�beis
		informadas, ou se o item for de coparticipa��o deve-se verificar isto na
		conta que gerou o item,  se n�o estiver, solicitar ao cliente que atualize
		as contas cont�beis.

	Para verificar esta rotina � necess�rio se ter:
		A data de antecipa��o do lote de mensalidade - OPS - Mensalidade - Lote
		A data de refer�ncia da mensalidade - est� na manuten��o de t�tulos a receber
		A data de liquida��o do t�tulo
		A data de recebimento da baixa
		A data de contrata��o do benefici�rio
		Se h� baixas em dias diferentes
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_conta_deb_w				varchar2(4000);
cd_conta_deb_antecip_w			varchar2(4000);
cd_conta_rec_w				varchar2(4000);
cd_conta_rec_antecip_w			varchar2(4000);
cd_conta_antec_baixa_w			varchar2(4000);
cd_conta_contabil_rec_antec_w		varchar2(4000);
ie_tipo_lancamento_w			varchar2(4000);
cd_historico_w				varchar2(4000);
cd_conta_credito_w			varchar2(4000);
cd_conta_debito_w			varchar2(4000);
cd_conta_contab_imp_w			varchar2(4000);
cd_estabelecimento_w			varchar2(4000);
cd_conta_contab_imp_deb_w		varchar2(4000);
cd_conta_contab_imp_rec_w		varchar2(4000);
cd_conta_deb_pls_w			varchar2(4000);
ie_novo_lancamento_w			varchar2(4000);
cd_conta_baixa_antec_w			varchar2(4000);
cd_conta_deb_antec_w			varchar2(4000);
cd_conta_cred_imposto_item_w		varchar2(20);
ie_esquema_contabil_w			varchar2(1);
vl_titulo_w				number(15,2);
vl_recebido_w				number(15,2);
vl_item_w				number(15,2);
vl_antecipacao_w			number(15,2);
vl_pro_rata_dia_w			number(15,2);
vl_lancamento_w				number(15,2);
vl_antecipacao_mens_w			number(15,2);
vl_tributo_w				number(15,2);
vl_contab_pro_rata_w			number(15,2);
vl_lancamento_ajuste_w			number(15,2);
vl_classif_mens_w			number(15,2);
vl_trib_total_w				number(15,2);
vl_baixa_antecipada_w			number(15,2)	:= 0;
vl_lanc_total_w				number(15,2)	:= 0;
--vl_tributo_item_w			number(15,2);
i					number(15);
cd_centro_custo_w			number(15);
cd_historico_antec_w			number(15);
cd_historico_antec_baixa_w		number(15);
cd_historico_baixa_w			number(15);
cd_historico_rec_antec_w		number(15);
cd_historico_rev_antec_baixa_w		number(15);
nr_seq_segurado_w			number(15);
nr_seq_item_mens_w			number(15);
nr_seq_mensalidade_w			number(15);
nr_seq_rec_liq_mens_w			number(15);
nr_seq_item_mens_copartic_w		number(15);
qt_baixas_w				number(15);
qt_seg_adesao_maior_w			number(15);
qt_baixas_dif_w				number(15);
qt_classif_mens_w			number(15)	:= 0;
qt_contabilizado_w			number(10);
nr_lote_contabilizado_w			number(10)	:= null;
nr_seq_mens_seg_w			number(10);
qt_item_pro_rata_w			number(10);
qt_trib_titulo_w			number(10);
dt_contabil_w				date;
dt_recebimento_w			date;
dt_referencia_mens_w			date;
dt_inicio_cobertura_w			date;
dt_fim_cobertura_w			date;
dt_ref_contab_inicial_w			date;
dt_ref_contab_final_w			date;
dt_contabilizacao_w			date;
dt_liquidacao_w				date;
dt_emissao_w				date;
dt_vencimento_w				date;
ie_dia_cobertura_rec_antec_w		varchar2(1);
ie_tipo_item_w				varchar2(2);
vl_perdas_w				titulo_receber_liq.vl_perdas%type;
nr_seq_baixa_w				titulo_receber_liq.nr_sequencia%type;

vl_tributo_item_w			pls_mensalidade_trib.vl_tributo%type;
vl_antecipacao_trib_w			pls_mensalidade_seg_item.vl_antecipacao%type;
vl_item_trib_w				pls_mensalidade_seg_item.vl_item%type;
vl_pro_rata_dia_trib_w			pls_mensalidade_seg_item.vl_pro_rata_dia%type;
vl_tributo_pr_w				pls_mensalidade_seg_item.vl_item%type;

vl_tributo_titulo_w			titulo_receber_trib.vl_tributo%type;

qt_classif_baixa_w			number(10);

cd_conta_contabil_classif_w		titulo_receber_classif.cd_conta_contabil%type;
cd_historico_classif_w			titulo_receber_classif.cd_historico%type;
vl_classificacao_w			titulo_receber_classif.vl_classificacao%type;
ie_origem_titulo_w			titulo_receber.ie_origem_titulo%type;

nr_titulo_contab_w			number(10);
qt_mensalidade_w			number(10);
vl_ato_cooperado_w			pls_mensalidade_seg_item.vl_ato_cooperado%type;
vl_ato_auxiliar_w			pls_mensalidade_seg_item.vl_ato_auxiliar%type;
vl_ato_nao_cooperado_w			pls_mensalidade_seg_item.vl_ato_nao_cooperado%type;
cd_conta_ato_auxiliar_w			pls_mensalidade_seg_item.cd_conta_ato_auxiliar%type;
cd_conta_ato_cooperado_w		pls_mensalidade_seg_item.cd_conta_ato_cooperado%type;
cd_conta_ato_nao_coop_w			pls_mensalidade_seg_item.cd_conta_ato_nao_coop%type;
nr_seq_rec_futura_w			pls_conta_receita_futura.nr_sequencia%type;

Cursor c_itens_mensalidade is
	select	d.cd_conta_deb,
		d.cd_conta_deb_antecip,
		d.cd_conta_rec,
		d.cd_conta_rec_antecip,
		d.cd_conta_antec_baixa,
		d.cd_conta_contabil_rec_antec,
		d.cd_historico_antec,
		d.cd_historico_antec_baixa,
		d.cd_historico_baixa,
		d.cd_historico_rec_antec,
		d.cd_historico_rev_antec_baixa,
		d.cd_centro_custo,
		nvl(d.vl_antecipacao,0) vl_antecipacao,
		nvl(d.vl_item,0) vl_item,
		nvl(d.vl_pro_rata_dia,0) vl_pro_rata_dia,
		c.nr_seq_segurado,
		d.nr_sequencia,
		d.ie_tipo_item,
		vl_ato_cooperado,
		vl_ato_auxiliar,
		vl_ato_nao_cooperado,
		cd_conta_ato_auxiliar,
		cd_conta_ato_cooperado,
		cd_conta_ato_nao_coop,
		d.nr_seq_rec_futura
	from	pls_mensalidade_seg_item	d,
		pls_mensalidade_segurado	c,
		pls_mensalidade			b,
		titulo_receber			a
	where	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	b.nr_sequencia	= a.nr_seq_mensalidade
	and	d.ie_tipo_item	not in ('3', '6', '7')--<> '3'
	and	a.nr_titulo	= nr_titulo_p
	union all
	select	nvl(e.cd_conta_deb, d.cd_conta_deb) cd_conta_deb,
		nvl(e.cd_conta_deb_antecip, d.cd_conta_deb_antecip) cd_conta_deb_antecip,
		nvl(e.cd_conta_cred, d.cd_conta_rec) cd_conta_rec,
		nvl(e.cd_conta_cred_antecip, d.cd_conta_rec_antecip) cd_conta_rec_antecip,
		nvl(e.cd_conta_antec_baixa, d.cd_conta_antec_baixa) cd_conta_antec_baixa,
		d.cd_conta_contabil_rec_antec cd_conta_contabil_rec_antec,
		nvl(e.cd_historico_baixa, d.cd_historico_antec) cd_historico_antec,
		nvl(e.cd_historico_baixa, d.cd_historico_antec_baixa) cd_historico_antec_baixa,
		nvl(e.cd_historico_baixa, d.cd_historico_baixa) cd_historico_baixa,
		nvl(e.cd_historico_baixa, d.cd_historico_rec_antec) cd_historico_rec_antec,
		nvl(e.cd_historico_baixa, d.cd_historico_rev_antec_baixa) cd_historico_rev_antec_baixa,
		d.cd_centro_custo,
		nvl(d.vl_antecipacao,0) * dividir_sem_round(nvl(nvl(e.vl_copartic_mens,e.vl_coparticipacao),0), d.vl_item) vl_antecipacao,
		nvl(nvl(e.vl_copartic_mens,e.vl_coparticipacao),0) vl_item,
		nvl(d.vl_pro_rata_dia,0) * dividir_sem_round(nvl(nvl(e.vl_copartic_mens,e.vl_coparticipacao),0), d.vl_item) vl_pro_rata_dia,
		c.nr_seq_segurado,
		d.nr_sequencia,
		d.ie_tipo_item,
		0 vl_ato_cooperado,
		0 vl_ato_auxiliar,
		0 vl_ato_nao_cooperado,
		null cd_conta_ato_auxiliar,
		null cd_conta_ato_cooperado,
		null cd_conta_ato_nao_coop,
		e.nr_seq_rec_futura
	from	pls_conta_coparticipacao 	e,
		pls_mensalidade_seg_item	d,
		pls_mensalidade_segurado	c,
		pls_mensalidade			b,
		titulo_receber			a
	where	d.nr_seq_conta	= e.nr_seq_conta(+)
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	e.IE_STATUS_COPARTICIPACAO  <> 'N'
	and	b.nr_sequencia	= a.nr_seq_mensalidade
	and	d.ie_tipo_item	= '3'
	and	a.nr_titulo	= nr_titulo_p
	union all
	select	nvl(e.cd_conta_deb, d.cd_conta_deb) cd_conta_deb,
		null cd_conta_deb_antecip,
		nvl(e.cd_conta_cred, d.cd_conta_rec) cd_conta_rec,
		null cd_conta_rec_antecip,
		null cd_conta_antec_baixa,
		null cd_conta_contabil_rec_antec,
		null cd_historico_antec,
		null cd_historico_antec_baixa,
		e.cd_historico cd_historico_baixa,
		null cd_historico_rec_antec,
		null cd_historico_rev_antec_baixa,
		d.cd_centro_custo,
		nvl(d.vl_antecipacao,0) * dividir_sem_round(nvl(e.vl_beneficiario,0), d.vl_item) vl_antecipacao,
		nvl(e.vl_beneficiario,0) vl_item,
		nvl(d.vl_pro_rata_dia,0) * dividir_sem_round(nvl(e.vl_beneficiario,0), d.vl_item) vl_pro_rata_dia,
		c.nr_seq_segurado,
		d.nr_sequencia,
		d.ie_tipo_item,
		0 vl_ato_cooperado,
		0 vl_ato_auxiliar,
		0 vl_ato_nao_cooperado,
		null cd_conta_ato_auxiliar,
		null cd_conta_ato_cooperado,
		null cd_conta_ato_nao_coop,
		e.nr_seq_rec_futura
	from	pls_conta_pos_estabelecido 	e,
		pls_mensalidade_seg_item	d,
		pls_mensalidade_segurado	c,
		pls_mensalidade			b,
		titulo_receber			a
	where	d.nr_seq_conta	= e.nr_seq_conta(+)
	and	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	b.nr_sequencia	= a.nr_seq_mensalidade
	and	d.ie_tipo_item	in ('6', '7')
	and	a.nr_titulo	= nr_titulo_p;

Cursor C02 is
	select	distinct
		a.cd_conta_credito,
		a.cd_conta_debito,
		a.dt_contabil,
		a.ie_tipo_lancamento,
		a.cd_historico
	from	pls_titulo_rec_liq_mens a
	where	a.nr_titulo	= nr_titulo_p
	and	a.nr_seq_baixa	= nr_seq_baixa_p
	order by 1,2;
	
cursor c_classificacao is
	select	a.cd_conta_contabil,
		a.cd_historico,
		a.vl_classificacao,
		a.cd_centro_custo
	from	titulo_receber_classif	a
	where	a.nr_titulo	= nr_titulo_p;
	
Cursor c_ajuste_item is
	select	ie_tipo_lancamento,
		max(nr_sequencia) nr_seq_liq_mens,
		sum(vl_lancamento) vl_lancamento
	from	pls_titulo_rec_liq_mens
	where	nr_titulo	= nr_titulo_p
	and	nr_seq_baixa	= nr_seq_baixa_p
	group by ie_tipo_lancamento;
	
vet_ajuste_item		c_ajuste_item%rowtype;
	
cursor	c_ajuste_contabil is
	select	c.cd_conta_credito,
		max(c.nr_sequencia) nr_seq_liq_mens,
		dividir(sum(i.vl_item),count(*)) vl_item,
		 nvl(sum(c.vl_lancamento),0) vl_lancamento,
		(	select	nvl(sum(r.vl_tributo),0)
			from   	pls_mensalidade_trib  r
			where  	r.nr_seq_item_mens  = i.nr_sequencia
			and	r.ie_retencao		<> 'N'
			and	r.ie_origem_tributo	= 'CD') vl_tributo
	from	pls_titulo_rec_liq_mens 	c,
		pls_mensalidade_seg_item 	i,
		titulo_receber_liq 		l,
		titulo_receber 			t
	where	c.nr_seq_item_mens	= i.nr_sequencia
	and	c.nr_seq_baixa		= l.nr_sequencia
	and	c.nr_titulo		= t.nr_titulo
	and	l.nr_titulo		= t.nr_titulo
	and	t.nr_titulo		= nr_titulo_p
	and	c.ie_tipo_lancamento	= 'PR'
	and	l.ie_origem_baixa <> 'ET'
	and	not exists(	select	1
				from	alteracao_valor a
				where	a.nr_titulo = t.nr_titulo)
	and	not exists (select	1
				from	titulo_receber_liq y
				where	y.nr_titulo = l.nr_titulo
				and	y.ie_origem_baixa = 'ET'
				and	l.nr_sequencia = y.nr_seq_liq_origem)
	group by c.cd_conta_credito,
		i.nr_sequencia;
		
v_ajuste	c_ajuste_contabil%rowtype;

begin

if	(nr_titulo_p is not null) and
	(nr_seq_baixa_p is not null) then
	nr_lote_contabilizado_w	:= null;

	select	max(a.nr_lote_contabil)
	into	nr_lote_contabilizado_w
	from	pls_titulo_rec_liq_mens	a
	where	a.nr_titulo	= nr_titulo_p
	and	a.nr_seq_baixa	= nr_seq_baixa_p
	and	a.nr_lote_contabil <> 0;

	if	(nr_lote_contabilizado_w is not null) then
		nr_titulo_contab_p := nr_titulo_p;
	else
		select	a.vl_titulo,
			a.nr_seq_mensalidade,
			a.cd_estabelecimento,
			a.dt_liquidacao,
			a.dt_emissao,
			a.dt_vencimento,
			a.ie_origem_titulo
		into	vl_titulo_w,
			nr_seq_mensalidade_w,
			cd_estabelecimento_w,
			dt_liquidacao_w,
			dt_emissao_w,
			dt_vencimento_w,
			ie_origem_titulo_w
		from	titulo_receber a
		where	a.nr_titulo	= nr_titulo_p;

		if	(nvl(nr_seq_mensalidade_w,0) <> 0) then
			
			select	max(ie_esquema_contabil),
				max(ie_dia_cobertura_rec_antec)
			into	ie_esquema_contabil_w,
				ie_dia_cobertura_rec_antec_w
			from	pls_parametro_contabil
			where	cd_estabelecimento	= cd_estabelecimento_w;

			ie_esquema_contabil_w		:= nvl(ie_esquema_contabil_w, 'N');
			ie_dia_cobertura_rec_antec_w	:= nvl(ie_dia_cobertura_rec_antec_w, 'N');

			select	a.dt_recebimento,
				a.vl_recebido + a.vl_descontos + nvl(a.vl_nota_credito,0),
				nvl(a.vl_perdas,0)
			into	dt_recebimento_w,
				vl_recebido_w,
				vl_perdas_w
			from	titulo_receber_liq a
			where	a.nr_titulo	= nr_titulo_p
			and	a.nr_sequencia	= nr_seq_baixa_p;

			if 	(nvl(vl_recebido_w,0) = 0) then
				vl_recebido_w := vl_perdas_w;
			end if;

			delete	from pls_titulo_rec_liq_mens
			where	nr_titulo	= nr_titulo_p
			and	nr_seq_baixa	= nr_seq_baixa_p;

			select	pls_obter_conta_imposto('C', nr_seq_mensalidade_w, cd_estabelecimento_w)
			into	cd_conta_contab_imp_rec_w
			from	dual;

			select	pls_obter_conta_imposto('D', nr_seq_mensalidade_w, cd_estabelecimento_w)
			into	cd_conta_contab_imp_deb_w
			from	dual;

			select	nvl(sum(vl_tributo),0) * dividir(vl_recebido_w,vl_titulo_w)
			into	vl_trib_total_w
			from	pls_mensalidade_trib
			where	nr_seq_mensalidade	= nr_seq_mensalidade_w
			and	ie_retencao		<> 'N'
			and	ie_origem_tributo	= 'CD';

			/* Retorna se possuir mais de uma baixa com a mesma data de recebimento */
			select	count(1)
			into	qt_baixas_w
			from	titulo_receber_liq a
			where	a.nr_titulo	= nr_titulo_p
			and	a.nr_sequencia	= nr_seq_baixa_p
			and	a.ie_acao	<> 'E'
			and	exists	(select	1
					from	titulo_receber_liq x
					where	x.nr_titulo = nr_titulo_p
					and	x.nr_sequencia <> a.nr_sequencia
					and	x.ie_acao	<> 'E'
					and	trunc(x.dt_recebimento,'dd') = trunc(a.dt_recebimento,'dd'))
			and	rownum		= 1;

			/* Retorna se possuir mais de uma baixa e com data de recebimento distintas*/
			select	count(1)
			into	qt_baixas_dif_w
			from	titulo_receber_liq a
			where	a.nr_titulo	= nr_titulo_p
			and	a.nr_sequencia	= nr_seq_baixa_p
			and	exists	(select	1
					from	titulo_receber_liq x
					where	x.nr_titulo = nr_titulo_p
					and	x.nr_sequencia <> a.nr_sequencia
					and	trunc(x.dt_recebimento,'dd') <> trunc(a.dt_recebimento,'dd'));


			select	nvl(max(a.vl_antecipacao),0),
				max(a.dt_referencia)
			into	vl_antecipacao_mens_w,
				dt_referencia_mens_w
			from	pls_mensalidade	a
			where	a.nr_sequencia	= nr_seq_mensalidade_w;

			select	max(a.dt_contabilizacao)
			into	dt_contabilizacao_w
			from	pls_mensalidade		b,
				pls_lote_mensalidade	a
			where	a.nr_sequencia	= b.nr_seq_lote
			and	b.nr_sequencia	= nr_seq_mensalidade_w;

			open c_itens_mensalidade;
			loop
			fetch c_itens_mensalidade into
				cd_conta_deb_w,
				cd_conta_deb_antecip_w,
				cd_conta_rec_w,
				cd_conta_rec_antecip_w,
				cd_conta_antec_baixa_w,
				cd_conta_contabil_rec_antec_w,
				cd_historico_antec_w,
				cd_historico_antec_baixa_w,
				cd_historico_baixa_w,
				cd_historico_rec_antec_w,
				cd_historico_rev_antec_baixa_w,
				cd_centro_custo_w,
				vl_antecipacao_w,
				vl_item_w,
				vl_pro_rata_dia_w,
				nr_seq_segurado_w,
				nr_seq_item_mens_w,
				ie_tipo_item_w,
				vl_ato_cooperado_w,
				vl_ato_auxiliar_w,
				vl_ato_nao_cooperado_w,
				cd_conta_ato_auxiliar_w,
				cd_conta_ato_cooperado_w,
				cd_conta_ato_nao_coop_w,
				nr_seq_rec_futura_w;
			exit when c_itens_mensalidade%notfound;
				begin
				i	:= 1;
					

				select	nvl(sum(vl_tributo),0) * dividir(vl_recebido_w,vl_titulo_w)
				into	vl_tributo_w
				from	pls_mensalidade_trib
				where	nr_seq_item_mens	= nr_seq_item_mens_w
				and	ie_retencao		<> 'N'
				and	ie_origem_tributo	= 'CD';

				select	a.dt_contratacao
				into	dt_inicio_cobertura_w
				from	pls_segurado a
				where	a.nr_sequencia	= nr_seq_segurado_w;

				if	(ie_dia_cobertura_rec_antec_w = 'N') then
					dt_ref_contab_inicial_w	:= trunc(dt_referencia_mens_w,'month');
				else
					begin
					dt_ref_contab_inicial_w	:= to_date(to_char(dt_inicio_cobertura_w,'dd') || '/'|| to_char(dt_referencia_mens_w,'mm/yyyy'));
					exception
					when others then
						dt_ref_contab_inicial_w	:= last_day(dt_referencia_mens_w);
					end;
				end if;

				dt_ref_contab_final_w	:= last_day(dt_referencia_mens_w);

				begin
				dt_fim_cobertura_w	:= to_date(to_char(dt_inicio_cobertura_w,'dd') - 1 || '/'|| to_char(dt_ref_contab_final_w,'mm/yyyy'));
				exception
				when others then
					dt_fim_cobertura_w	:= last_day(dt_ref_contab_final_w);
				end;

				if	(trunc(dt_recebimento_w,'month') < trunc(dt_referencia_mens_w,'month')) then
					cd_conta_deb_antecip_w	:= cd_conta_antec_baixa_w;
				end if;
				
				if	(trunc(dt_recebimento_w,'month') < trunc(dt_referencia_mens_w,'month')) and
					((nr_seq_rec_futura_w is not null) or (cd_conta_contabil_rec_antec_w is not null)) and
					(ie_tipo_item_w not in ('6','7')) then	--'3'
					while (i <= 6) loop
						cd_conta_debito_w	:= null;
						cd_conta_credito_w	:= null;
						cd_historico_w		:= null;
						dt_contabil_w		:= null;
						vl_lancamento_w		:= 0;
						ie_tipo_lancamento_w	:= null;
					
						if	(i = 1) then
							cd_conta_credito_w	:= cd_conta_contabil_rec_antec_w;
							cd_historico_w		:= cd_historico_rec_antec_w;
							dt_contabil_w		:= dt_recebimento_w;
							vl_lancamento_w		:= dividir_sem_round(vl_antecipacao_w,vl_titulo_w) * vl_recebido_w;
							ie_tipo_lancamento_w	:= 'BA';
						elsif	(i = 2) then
							cd_conta_debito_w	:= cd_conta_contabil_rec_antec_w;
							cd_historico_w		:= cd_historico_rec_antec_w;
							dt_contabil_w		:= dt_referencia_mens_w;
							vl_lancamento_w		:= dividir_sem_round(vl_antecipacao_w,vl_titulo_w) * vl_recebido_w;
							ie_tipo_lancamento_w	:= 'EA';
						elsif	(i = 3) then
							cd_conta_credito_w	:= cd_conta_deb_w;
							cd_historico_w		:= cd_historico_baixa_w;
							dt_contabil_w		:= dt_referencia_mens_w;
							vl_lancamento_w		:= dividir_sem_round(vl_antecipacao_w,vl_titulo_w) * vl_recebido_w;
							ie_tipo_lancamento_w	:= 'PR';
						elsif	(i = 4) then
							cd_conta_credito_w	:= cd_conta_contabil_rec_antec_w;
							cd_historico_w		:= cd_historico_rec_antec_w;
							dt_contabil_w		:= dt_recebimento_w;
							vl_lancamento_w		:= dividir_sem_round(vl_pro_rata_dia_w,vl_titulo_w) * vl_recebido_w;
							ie_tipo_lancamento_w	:= 'BA';
						elsif	(i = 5) then
							cd_conta_debito_w	:= cd_conta_contabil_rec_antec_w;
							cd_historico_w		:= cd_historico_rec_antec_w;
							dt_contabil_w		:= dt_referencia_mens_w;
							vl_lancamento_w		:= dividir_sem_round(vl_pro_rata_dia_w,vl_titulo_w) * vl_recebido_w;
							ie_tipo_lancamento_w	:= 'EA';
						elsif	(i = 6) then
							cd_conta_credito_w	:= cd_conta_deb_w;
							cd_historico_w		:= cd_historico_baixa_w;
							dt_contabil_w		:= dt_referencia_mens_w;
							vl_lancamento_w		:= dividir_sem_round(vl_pro_rata_dia_w,vl_titulo_w) * vl_recebido_w;
							ie_tipo_lancamento_w	:= 'PR';
						end if;
						
						if	(ie_tipo_item_w = '3') and
							(i in (2,5)) then
							cd_conta_debito_w	:= cd_conta_antec_baixa_w;
						end if;
						
						if	(ie_tipo_item_w = '3') and
							(i in (1,4)) then
							cd_conta_credito_w	:= cd_conta_antec_baixa_w;
						end if;
						
						if	(vl_lancamento_w  <> 0) then
							insert into pls_titulo_rec_liq_mens
								(nr_sequencia,
								nr_titulo,
								nr_seq_baixa,
								nm_usuario_nrec,
								dt_atualizacao_nrec,
								nm_usuario,
								dt_atualizacao,
								nr_seq_item_mens,
								nr_seq_segurado,
								cd_conta_debito,
								cd_conta_credito,
								ie_tipo_lancamento,
								nr_lote_contabil,
								cd_historico,
								cd_centro_custo,
								vl_lancamento,
								dt_adesao,
								dt_ref_mens,
								dt_inicio_cobertura,
								dt_fim_cobertura,
								dt_ref_contab_inicial,
								dt_ref_contab_final,
								dt_contabil)
							values	(pls_titulo_rec_liq_mens_seq.nextval,
								nr_titulo_p,
								nr_seq_baixa_p,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								sysdate,
								nr_seq_item_mens_w,
								nr_seq_segurado_w,
								cd_conta_debito_w,
								cd_conta_credito_w,
								ie_tipo_lancamento_w,
								0,
								cd_historico_w,
								cd_centro_custo_w,
								vl_lancamento_w,
								dt_inicio_cobertura_w,
								dt_referencia_mens_w,
								dt_inicio_cobertura_w,
								dt_fim_cobertura_w,
								dt_ref_contab_inicial_w,
								dt_ref_contab_final_w,
								dt_contabil_w);
						end if;
						
						i	:= i + 1;
					end loop;
				else
					cd_conta_debito_w	:= null;
					cd_conta_credito_w	:= null;
					cd_historico_w		:= null;
					dt_contabil_w		:= null;
					vl_lancamento_w		:= 0;
					ie_tipo_lancamento_w	:= null;
					
					cd_conta_credito_w	:= cd_conta_deb_w;
					
					cd_historico_w		:= cd_historico_baixa_w;
					dt_contabil_w		:= dt_recebimento_w;
					ie_tipo_lancamento_w	:= 'PR';
				



					while (i <= 2) loop
						if	(i = 1) then
							vl_lancamento_w		:= (dividir_sem_round(vl_antecipacao_w,vl_titulo_w) * (vl_recebido_w + vl_trib_total_w)) - (vl_antecipacao_w * dividir(vl_tributo_w,vl_item_w));
						elsif	(i = 2) then
							vl_lancamento_w		:= (dividir_sem_round(vl_pro_rata_dia_w,vl_titulo_w) * (vl_recebido_w + vl_trib_total_w))- (vl_pro_rata_dia_w * dividir(vl_tributo_w,vl_item_w));
						end if;
						if	(vl_lancamento_w  <> 0) then
							insert into pls_titulo_rec_liq_mens
								(nr_sequencia,
								nr_titulo,
								nr_seq_baixa,
								nm_usuario_nrec,
								dt_atualizacao_nrec,
								nm_usuario,
								dt_atualizacao,
								nr_seq_item_mens,
								nr_seq_segurado,
								cd_conta_debito,
								cd_conta_credito,
								ie_tipo_lancamento,
								nr_lote_contabil,
								cd_historico,
								cd_centro_custo,
								vl_lancamento,
								dt_adesao,
								dt_ref_mens,
								dt_inicio_cobertura,
								dt_fim_cobertura,
								dt_ref_contab_inicial,
								dt_ref_contab_final,
								dt_contabil)
							values	(pls_titulo_rec_liq_mens_seq.nextval,
								nr_titulo_p,
								nr_seq_baixa_p,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								sysdate,
								nr_seq_item_mens_w,
								nr_seq_segurado_w,
								cd_conta_debito_w,
								cd_conta_credito_w,
								ie_tipo_lancamento_w,
								0,
								cd_historico_w,
								cd_centro_custo_w,
								vl_lancamento_w,
								dt_inicio_cobertura_w,
								dt_referencia_mens_w,
								dt_inicio_cobertura_w,
								dt_fim_cobertura_w,
								dt_ref_contab_inicial_w,
								dt_ref_contab_final_w,
								dt_contabil_w);
						end if;
						
						i	:= i + 1;
					end loop;
				end if;
				end;
			end loop;
			close c_itens_mensalidade;

			select	count(1)
			into	qt_mensalidade_w
			from	pls_mensalidade_segurado
			where	nr_seq_mensalidade	= nr_seq_mensalidade_w;

			if	(ie_origem_titulo_w = 3) and
				((nr_seq_mensalidade_w is null) or
				(qt_mensalidade_w = 0)) then
				open c_classificacao;
				loop
				fetch c_classificacao into
					cd_conta_contabil_classif_w,
					cd_historico_classif_w,
					vl_classificacao_w,
					cd_centro_custo_w;
				exit when c_classificacao%notfound;
					begin
					vl_lancamento_w		:= vl_classificacao_w * dividir_sem_round(vl_recebido_w, vl_titulo_w);
					cd_conta_credito_w	:= cd_conta_contabil_classif_w;
					cd_historico_w		:= cd_historico_classif_w;

					if	(vl_lancamento_w <> 0) then
						insert into pls_titulo_rec_liq_mens
							(nr_sequencia,
							nr_titulo,
							nr_seq_baixa,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							nm_usuario,
							dt_atualizacao,
							nr_seq_item_mens,
							nr_seq_segurado,
							cd_conta_debito,
							cd_conta_credito,
							ie_tipo_lancamento,
							nr_lote_contabil,
							cd_historico,
							cd_centro_custo,
							vl_lancamento,
							dt_adesao,
							dt_ref_mens,
							dt_inicio_cobertura,
							dt_fim_cobertura,
							dt_ref_contab_inicial,
							dt_ref_contab_final,
							dt_contabil)
						values	(pls_titulo_rec_liq_mens_seq.nextval,
							nr_titulo_p,
							nr_seq_baixa_p,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							null,
							null,
							null,
							cd_conta_credito_w,
							'PR',
							0,
							cd_historico_w,
							cd_centro_custo_w,
							vl_lancamento_w,
							dt_recebimento_w,
							dt_referencia_mens_w,
							dt_recebimento_w,
							dt_recebimento_w,
							dt_recebimento_w,
							dt_recebimento_w,
							dt_recebimento_w);
					end if;
					end;
				end loop;
				close c_classificacao;
			end if;

			select	count(1)
			into	qt_classif_baixa_w
			from	pls_titulo_rec_liq_mens	a
			where	a.nr_titulo		= nr_titulo_p
			and	a.nr_seq_baixa		= nr_seq_baixa_p
			and	rownum			= 1;

			/*Amortiza��o OS 648276 */
			if	(qt_classif_baixa_w = 0) then
				select	max(a.cd_conta_credito),
					max(a.cd_centro_custo),
					max(a.cd_historico)
				into	cd_conta_credito_w,
					cd_centro_custo_w,
					cd_historico_w
				from	pls_titulo_rec_liq_mens	a
				where	a.nr_titulo		= nr_titulo_p;

				if	(cd_conta_credito_w is not null) then
					insert into pls_titulo_rec_liq_mens
						(nr_sequencia,
						nr_titulo,
						nr_seq_baixa,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						nm_usuario,
						dt_atualizacao,
						nr_seq_item_mens,
						nr_seq_segurado,
						cd_conta_debito,
						cd_conta_credito,
						ie_tipo_lancamento, --(Baixa antecipada (antecip. total), Pr�-rata, Estorno antecipa��o)
						nr_lote_contabil,
						cd_historico,
						cd_centro_custo,
						vl_lancamento,
						dt_adesao,
						dt_ref_mens,
						dt_inicio_cobertura,
						dt_fim_cobertura,
						dt_ref_contab_inicial,
						dt_ref_contab_final,
						dt_contabil)
					values	(pls_titulo_rec_liq_mens_seq.nextval,
						nr_titulo_p,
						nr_seq_baixa_p,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nr_seq_item_mens_w,
						nr_seq_segurado_w,
						null,
						cd_conta_credito_w,
						'PR',
						0,
						cd_historico_w,
						cd_centro_custo_w,
						vl_recebido_w,
						nvl(dt_inicio_cobertura_w,sysdate),
						dt_referencia_mens_w,
						nvl(dt_inicio_cobertura_w,sysdate),
						nvl(dt_fim_cobertura_w,sysdate),
						nvl(dt_ref_contab_inicial_w,sysdate),
						nvl(dt_ref_contab_final_w,sysdate),
						nvl(dt_contabil_w,sysdate));
				end if;
			end if;
			
			open c_ajuste_item;
			loop
			fetch c_ajuste_item into	
				vet_ajuste_item;
			exit when c_ajuste_item%notfound;
				begin
				if	(vl_recebido_w <> vet_ajuste_item.vl_lancamento) then

					/*Ajuste de itens na classif. da baixa para zerar diferen�a da soma dos itens X valor recebido*/

					update	pls_titulo_rec_liq_mens
					set	vl_lancamento	= vl_lancamento	+ (vl_recebido_w - vet_ajuste_item.vl_lancamento)
					where	nr_sequencia	= vet_ajuste_item.nr_seq_liq_mens;
				end if;
				end;
			end loop;
			close c_ajuste_item;
			
			select	count(*)
			into	qt_trib_titulo_w
			from	titulo_receber_trib
			where	nr_titulo		= nr_titulo_p
			and	ie_origem_tributo	= 'CD'
			and	nvl(vl_trib_total_w,0)	= 0;
			
			/*Ajuste de valores de contas cont�beis , somente para quando o t�tulo estiver liquidado*/
			if	(dt_liquidacao_w is not null) and (nr_seq_baixa_w = nr_seq_baixa_p) and (qt_trib_titulo_w = 0) then
				open c_ajuste_contabil;
				loop
				fetch c_ajuste_contabil into	
					v_ajuste;
				exit when c_ajuste_contabil%notfound;
					begin
								
					update	pls_titulo_rec_liq_mens
					set	vl_lancamento	= vl_lancamento + (v_ajuste.vl_item - (v_ajuste.vl_lancamento + v_ajuste.vl_tributo))
					where	nr_sequencia	= v_ajuste.nr_seq_liq_mens
					and	nr_seq_baixa  	= nr_seq_baixa_p
					and	nvl(nr_lote_contabil,0)	= 0;
					
					end;
				end loop;
				close c_ajuste_contabil;	
			end if;
		end if;
	end if;
end if;

/* Sem commit */

end pls_gerar_tit_rec_liq_mens_cpr;
/
