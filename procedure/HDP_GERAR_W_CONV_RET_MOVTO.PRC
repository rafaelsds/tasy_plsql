create or replace 
procedure HDP_GERAR_W_CONV_RET_MOVTO(nr_seq_retorno_p	number) is

cd_tipo_reg_w		convenio_retorno_movto.cd_tipo_registro%type;
dt_execucao_w		convenio_retorno_movto.dt_execucao%type;
nr_doc_convenio_w	convenio_retorno_movto.nr_doc_convenio%type;
cd_usuario_conv_w	convenio_retorno_movto.cd_usuario_convenio%type;
ds_complemento_w	convenio_retorno_movto.ds_complemento%type;
cd_item_w		convenio_retorno_movto.cd_item%type;
vl_pago_w		convenio_retorno_movto.vl_pago%type;
vl_cobrado_w		convenio_retorno_movto.vl_cobrado%type;
vl_glosa_w		convenio_retorno_movto.vl_glosa%type;
cd_convenio_w		convenio_retorno.cd_convenio%type;
nr_interno_conta_w	conta_paciente_guia.nr_interno_conta%type;

cursor C01 is
select	nvl(somente_numero(substr(obter_valor_campo_separador(ds_conteudo, 1, ';'),1,8)),101553) cd_tipo_reg,
	substr(obter_valor_campo_separador(ds_conteudo, 12, ';'), 1, 255) dt_execucao,
	somente_numero(substr(obter_valor_campo_separador(ds_conteudo, 10, ';'), 1, 255)) nr_doc_conv,
	substr(obter_valor_campo_separador(ds_conteudo, 8, ';'), 1, 255) cd_user_conv,
	substr(obter_valor_campo_separador(ds_conteudo, 9, ';'), 1, 255) nm_user,
	substr(obter_valor_campo_separador(ds_conteudo, 18, ';'), 1, 255) cd_item,
	nvl(somente_numero(substr(obter_valor_campo_separador(ds_conteudo, 22, ';'), 1, 255)),0) vl_pago,
	nvl(somente_numero(substr(obter_valor_campo_separador(ds_conteudo, 23, ';'), 1, 255)),0) vl_cobrado,
	nvl(somente_numero(substr(obter_valor_campo_separador(ds_conteudo, 24, ';'), 1, 255)),0) vl_glosa
from	w_conv_ret_movto a
where	a.nr_seq_retorno		= nr_seq_retorno_p;

begin

select	cd_convenio
into	cd_convenio_w
from	convenio_retorno
where	nr_sequencia = nr_seq_retorno_p;

open c01;
loop
fetch c01 into
	cd_tipo_reg_w,
	dt_execucao_w,
	nr_doc_convenio_w,
	cd_usuario_conv_w,
	ds_complemento_w,
	cd_item_w,
	vl_pago_w,
	vl_cobrado_w,
	vl_glosa_w;
exit when c01%notfound;

select	max(a.nr_interno_conta)
into 	nr_interno_conta_w
from	conta_paciente_guia a,
	conta_paciente b
where	a.nr_interno_conta 	= b.nr_interno_conta
and	a.cd_autorizacao 	= nr_doc_convenio_w
and	b.cd_convenio_parametro	= cd_convenio_w;

	vl_pago_w	:= vl_pago_w/100;
	vl_cobrado_w	:= vl_cobrado_w/100;
	vl_glosa_w	:= vl_glosa_w/100;
	
insert into convenio_retorno_movto(	cd_item,
					cd_tipo_registro,
					cd_usuario_convenio,
					ds_complemento,
					dt_atualizacao,
					dt_execucao,
					nm_usuario,
					nr_doc_convenio,
					nr_conta,
					nr_sequencia,
					nr_seq_retorno,
					vl_pago,
					vl_cobrado,
					vl_glosa)
				values	(cd_item_w,
					cd_tipo_reg_w,
					cd_usuario_conv_w,
					ds_complemento_w,
					sysdate,
					dt_execucao_w,
					'tasy',
					nr_doc_convenio_w,
					nr_interno_conta_w,
					convenio_retorno_movto_seq.nextval,
					nr_seq_retorno_p,
					vl_pago_w,
					vl_cobrado_w,
					vl_glosa_w);
end loop;
close C01;

delete from w_conv_ret_movto 
where nr_seq_retorno  = nr_seq_retorno_p;

commit;

end HDP_GERAR_W_CONV_RET_MOVTO;
/