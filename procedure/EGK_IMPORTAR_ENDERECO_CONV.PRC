create or replace
procedure EGK_IMPORTAR_ENDERECO_CONV
			(cd_pessoa_fisica_p	varchar2,
			nr_seq_pessoa_egk_p	number,
			nm_usuario_p		varchar2,
			ie_opcao_p		varchar2)
			is
			
/* ie_opcao_p
CC - Complemento da Pessoa e Convenio
PF - Atualizar dados PF
*/			

pessoa_fisica_egk_w		pessoa_fisica_egk%rowtype;
compl_pessoa_fisica_w		compl_pessoa_fisica%rowtype;
pessoa_titular_convenio_w	pessoa_titular_convenio%rowtype;

nr_seq_regra_conv_w	number(10);
cd_convenio_w		convenio.cd_convenio%type;
nr_seq_person_name_w	person_name.nr_sequencia%type;
nr_seq_forma_trat_w	pessoa_fisica.nr_seq_forma_trat%type;
qt_categoria_w          number(10);
cd_categoria_w		pessoa_titular_convenio.cd_categoria%type;

	procedure obter_dados_endereco is	
	begin
	
	compl_pessoa_fisica_w.ie_tipo_complemento	:= '1';
	compl_pessoa_fisica_w.cd_cep			:= pessoa_fisica_egk_w.postleitzahl_sa;		
	compl_pessoa_fisica_w.ds_endereco		:= pessoa_fisica_egk_w.strasse_sa;
	compl_pessoa_fisica_w.ds_compl_end		:= pessoa_fisica_egk_w.hausnummer_sa;
	compl_pessoa_fisica_w.ds_municipio		:= pessoa_fisica_egk_w.ort_sa;
	compl_pessoa_fisica_w.ds_complemento		:= pessoa_fisica_egk_w.anschriftenzusatz_sa;
	compl_pessoa_fisica_w.nr_seq_pais		:= intpd_conv('PAIS', 'NR_SEQUENCIA', pessoa_fisica_egk_w.wohnsitzlaendercode_sa, nr_seq_regra_conv_w,'I','I');
	
	end obter_dados_endereco;
	
	procedure obter_dados_convenio is
	begin
	
	pessoa_titular_convenio_w.cd_convenio		:= cd_convenio_w;
	pessoa_titular_convenio_w.dt_inicio_vigencia	:= pessoa_fisica_egk_w.versicherungsschutz_beginn;
	pessoa_titular_convenio_w.dt_fim_vigencia	:= pessoa_fisica_egk_w.versicherungsschutz_ende;
	pessoa_titular_convenio_w.dt_validade_carteira	:= pessoa_fisica_egk_w.versicherungsschutz_ende;
	pessoa_titular_convenio_w.cd_usuario_convenio	:= pessoa_fisica_egk_w.versicherten_id;
	
	select	decode(pessoa_fisica_egk_w.versichertenart,'3','2',pessoa_fisica_egk_w.versichertenart)
	into	pessoa_titular_convenio_w.ie_tipo_conveniado
	from	dual;
	
	end obter_dados_convenio;

begin

select	max(nr_seq_regra_conv)
into	nr_seq_regra_conv_w
from	regra_leitor_cartao_pac
where	nr_sequencia = obter_seq_regra_leitor_cartao(nm_usuario_p,wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_cd_estabelecimento);

select	*
into	pessoa_fisica_egk_w
from	pessoa_fisica_egk
where	nr_sequencia		= nr_seq_pessoa_egk_p;

if	(ie_opcao_p = 'PF') then --Chamado na opcao de atualizar dados da pessoa

	select	max(nr_seq_person_name)
	into	nr_seq_person_name_w
	from	pessoa_fisica
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_p;

	update	person_name
	set	ds_given_name 		= pessoa_fisica_egk_w.vorname,
		ds_family_name 		= pessoa_fisica_egk_w.nachname,
		ds_component_name_1 	= pessoa_fisica_egk_w.vorsatzwort,
		ds_component_name_2 	= null,
		ds_component_name_3 	= pessoa_fisica_egk_w.namenszusatz,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia 		= nr_seq_person_name_w;
	
	select	max(nr_sequencia) 
	into	nr_seq_forma_trat_w
	from	pf_forma_tratamento 
	where	upper(ds_forma_tratamento) = upper(pessoa_fisica_egk_w.titel);

	update	pessoa_fisica
	set	nm_pessoa_fisica	= pessoa_fisica_egk_w.vorname || ' ' || pessoa_fisica_egk_w.nachname,		
		ie_sexo 		= decode(lower(pessoa_fisica_egk_w.geschlecht),'w','F','M'),
		dt_nascimento 		= pessoa_fisica_egk_w.Geburtsdatum,
		nr_seq_forma_trat	= nr_seq_forma_trat_w,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_p;

end if;

if	(ie_opcao_p in ('CC','PF')) then

	begin
	select	*
	into	compl_pessoa_fisica_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_tipo_complemento	= '1'
	and	rownum	= 1;
	exception
	when others then
		compl_pessoa_fisica_w 	:= null;
	end;

	obter_dados_endereco;

	if	(compl_pessoa_fisica_w.nr_sequencia is null) then	

		select 	nvl(max(nr_sequencia),0) + 1	
		into 	compl_pessoa_fisica_w.nr_sequencia	
		from 	compl_pessoa_fisica	
		where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
		
		compl_pessoa_fisica_w.cd_pessoa_fisica		:= cd_pessoa_fisica_p;
		compl_pessoa_fisica_w.nm_usuario_nrec 		:= nm_usuario_p;
		compl_pessoa_fisica_w.nm_usuario		:= nm_usuario_p;
		compl_pessoa_fisica_w.dt_atualizacao_nrec	:= sysdate;
		compl_pessoa_fisica_w.dt_atualizacao		:= sysdate;
		
		insert into compl_pessoa_fisica values compl_pessoa_fisica_w;
		
	else
		
		compl_pessoa_fisica_w.nm_usuario		:= nm_usuario_p;
		compl_pessoa_fisica_w.dt_atualizacao		:= sysdate;
		
		update	compl_pessoa_fisica
		set	row			= compl_pessoa_fisica_w
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	nr_sequencia		= compl_pessoa_fisica_w.nr_sequencia;

	end if;
	
	begin
	select  a.cd_convenio
	into	cd_convenio_w
	from    convenio a
	where   a.cd_cgc in (	select  b.cd_cgc
				from    (select x.cd_cgc,
						length(x.ds_razao_social)
					from    pessoa_juridica x,
						convenio y
					where   x.cd_cgc                    = y.cd_cgc
					and     x.ie_situacao               = 'A'
					and     y.ie_situacao               = 'A'
					and     somente_numero(x.cd_cnes)   = pessoa_fisica_egk_w.abrechnender_kennung
					order by   length(x.ds_razao_social) asc) b
				where   rownum = 1);
	exception
	when others then
		begin
		select	max(a.cd_convenio)	
		into 	cd_convenio_w	
		from 	convenio a,
			pessoa_juridica b
		where 	a.ie_situacao 	= 'A'	
		and b.ie_situacao 	= 'A'
		and	a.cd_cgc	= b.cd_cgc
		and	b.cd_cnes	= pessoa_fisica_egk_w.abrechnender_kennung;	
		
		if (cd_convenio_w is null) then
			select	max(c.cd_convenio)
			into 	cd_convenio_w
			from 	convenio c,
				pessoa_juridica pj
			where 	c.ie_situacao 	= 'A'
			and pj.ie_situacao 	= 'A'
			and	c.cd_cgc	= pj.cd_cgc
			and	pj.cd_cnes	= pessoa_fisica_egk_w.kostentraeger_kennung;
		end if;

		exception
		when others then
			cd_convenio_w := null;
		end;
	end;

	if	(cd_convenio_w is not null) then

		begin
		
		select	*
		into	pessoa_titular_convenio_w
		from	pessoa_titular_convenio
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	cd_convenio		= cd_convenio_w
		and	rownum	= 1;
		exception
		when others then
			pessoa_titular_convenio_w := null;
		end;
		
		select 	count(1)
		into	qt_categoria_w
		from 	categoria_convenio
		where	cd_convenio		= cd_convenio_w
		and	ie_situacao		= 'A';
		
		if (qt_categoria_w = 1) then
		
			select 	cd_categoria
			into	pessoa_titular_convenio_w.cd_categoria
			from 	categoria_convenio
			where	cd_convenio		= cd_convenio_w
			and	ie_situacao		= 'A';
			
		end if;
		
		obter_dados_convenio;
		
		if	(pessoa_titular_convenio_w.nr_sequencia is null) then
		
			select	pessoa_titular_convenio_seq.nextval
			into	pessoa_titular_convenio_w.nr_sequencia
			from	dual;
			
			pessoa_titular_convenio_w.cd_pessoa_fisica	:= cd_pessoa_fisica_p;
			pessoa_titular_convenio_w.nm_usuario_nrec 	:= nm_usuario_p;
			pessoa_titular_convenio_w.nm_usuario		:= nm_usuario_p;
			pessoa_titular_convenio_w.dt_atualizacao_nrec	:= sysdate;
			pessoa_titular_convenio_w.dt_atualizacao	:= sysdate;
			
			insert into pessoa_titular_convenio values pessoa_titular_convenio_w;
			
		else
		
			pessoa_titular_convenio_w.nm_usuario		:= nm_usuario_p;
			pessoa_titular_convenio_w.dt_atualizacao	:= sysdate;
			
			update	pessoa_titular_convenio
			set	row			= pessoa_titular_convenio_w
			where	cd_pessoa_fisica	= cd_pessoa_fisica_p
			and	nr_sequencia		= pessoa_titular_convenio_w.nr_sequencia;
		
		end if;

	end if;
	
end if;

update	pessoa_fisica_egk
set	cd_pessoa_fisica	= cd_pessoa_fisica_p,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_pessoa_egk_p;

commit;

end EGK_IMPORTAR_ENDERECO_CONV;
/
