create or replace
procedure DACON_RT9 (nm_usuario_p	varchar2) is

ds_arquivo_w	varchar2(2000);

begin

select	rpad('T9',2,' ')
		||lpad(count(*),5,0)
		||lpad(' ',226,' ')
		||lpad(0,10,0)
into	ds_arquivo_w
from 	w_dacon
where	nm_usuario = nm_usuario_p
and	ie_tipo_registro <> 5
and	ie_tipo_registro <> 45;

insert into w_dacon
	   (nr_sequencia,
	    dt_atualizacao,
	    nm_usuario,			
	    dt_atualizacao_nrec,
	    nm_usuario_nrec,
	    ds_arquivo,
            nr_linha,
	    ie_tipo_registro)
values	   (w_dacon_seq.nextval,
	    sysdate,
	    nm_usuario_p,
	    sysdate,
	    nm_usuario_p,
	    ds_arquivo_w,
	    1,
 	    45);

commit;

end DACON_RT9;
/