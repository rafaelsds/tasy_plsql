create or replace
procedure atualiza_tributo_baixa(nr_titulo_p	number,
				nr_seq_baixa_p	number,
				nm_usuario_p	varchar2) is

vl_imposto_w			Number(15,2)	:= 0;
nr_seq_imposto_w		number(10,0)	:= 0;
ie_baixa_contab_w		varchar2(50);
ie_gerar_titulo_pagar_w		varchar2(50);
dt_contabil_w			date;
nr_seq_tit_pagar_imposto_w	number(10,0)	:= 0;
vl_saldo_titulo_w		number(15,2);
vl_baixa_w			number(15,2);
ie_gerar_trib_ao_liquidar_w	varchar2(1)	:= 'S';
cd_estabelecimento_w		number(4);
/* Projeto Multimoeda - Vari�veis*/
vl_baixa_estrang_w		number(15,2);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;

cursor c01 is
select	a.nr_sequencia,
	a.vl_imposto,
	b.ie_gerar_titulo_pagar
from	tributo b,
	w_titulo_pagar_imposto a
where	a.nr_seq_baixa	= nr_seq_baixa_p
and	a.cd_tributo	= b.cd_tributo
and	a.nr_seq_escrit is null
and	a.nr_bordero is null
and	a.nr_titulo	= nr_titulo_p;

begin

select	max(vl_saldo_titulo),
	max(cd_estabelecimento)
into	vl_saldo_titulo_w,
	cd_estabelecimento_w
from	titulo_pagar
where	nr_titulo	= nr_titulo_p;

obter_param_usuario(851,72,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_gerar_trib_ao_liquidar_w);

select	max(vl_baixa),
	max(vl_baixa_estrang),
	max(vl_cotacao)
into	vl_baixa_w,
	vl_baixa_estrang_w,
	vl_cotacao_w
from	titulo_pagar_baixa
where	nr_titulo	= nr_titulo_p
and	nr_sequencia	= nr_seq_baixa_p;

if	(vl_baixa_w	= vl_saldo_titulo_w) or
	(nvl(ie_gerar_trib_ao_liquidar_w,'S') = 'N') then

	update	w_titulo_pagar_imposto
	set	nr_seq_baixa	= nr_seq_baixa_p
	where	nr_titulo	= nr_titulo_p
	and	nr_seq_escrit is null
	and	nr_bordero is null;
end if;

open c01;
loop
fetch c01 into
	nr_seq_imposto_w,
	vl_imposto_w,
	ie_gerar_titulo_pagar_w;
exit when c01%notfound;

	select	titulo_pagar_imposto_seq.nextval
	into	nr_seq_tit_pagar_imposto_w
	from	dual;

	insert into titulo_pagar_imposto
		(nr_sequencia, 
		nr_titulo, 
		cd_tributo,
		ie_pago_prev, 
		dt_atualizacao, 
		nm_usuario, 
		dt_imposto, 
		vl_base_calculo, 
		vl_imposto,
		ds_emp_retencao, 
		pr_imposto, 
		cd_beneficiario,
		cd_conta_financ, 
		nr_seq_trans_reg, 
		nr_seq_trans_baixa,
		vl_nao_retido, 
		ie_vencimento,
		VL_BASE_NAO_RETIDO, 
		VL_TRIB_ADIC, 
		VL_BASE_ADIC,
		vl_reducao,
		vl_desc_base,
		cd_darf,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_variacao,
		ie_periodicidade,
		nr_seq_baixa)	
	select	nr_seq_tit_pagar_imposto_w,
		nr_titulo, 
		cd_tributo,
		'V', 
		sysdate, 
		nm_usuario_p, 
		dt_imposto, 
		vl_base_calculo, 
		vl_imposto,
		null, 
		pr_imposto, 
		cd_beneficiario,
		cd_conta_financ, 
		nr_seq_trans_reg, 
		nr_seq_trans_baixa,
		vl_nao_retido, 
		ie_vencimento,
		VL_BASE_NAO_RETIDO, 
		VL_TRIB_ADIC, 
		VL_BASE_ADIC,
		vl_reducao,
		vl_desc_base,
		cd_darf,
		sysdate,
		nm_usuario_p,
		cd_variacao,
		ie_periodicidade,
		nr_seq_baixa
	from	w_titulo_pagar_imposto
	where	nr_sequencia		= nr_seq_imposto_w;

	
	update	titulo_pagar_baixa
	set	vl_baixa	= vl_baixa - vl_imposto_w,
		vl_pago		= vl_pago - vl_imposto_w,
		vl_imposto	= vl_imposto + vl_imposto_w
	where	nr_titulo	= nr_titulo_p
	and	nr_sequencia	= nr_seq_baixa_p;
	
	/* Projeto Multimoeda - Verifica se a baixa � em moeda estrangeira, caso positivo atualiza o valor estrang da baixa*/
	if (nvl(vl_baixa_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
		update	titulo_pagar_baixa
		set	vl_baixa_estrang = vl_baixa_estrang - (vl_imposto_w / vl_cotacao),
			vl_complemento	= vl_baixa - (vl_baixa_estrang - (vl_imposto_w / vl_cotacao))
		where	nr_titulo	= nr_titulo_p
		  and	nr_sequencia	= nr_seq_baixa_p;
	end if;

	if	(ie_gerar_titulo_pagar_w = 'S') then
		Gerar_titulo_tributo(nr_seq_tit_pagar_imposto_w, nm_usuario_p);
	end if;

end loop;
close c01;

end atualiza_tributo_baixa;
/
