create or replace
procedure atualiza_periodo_fatura(nr_interno_conta_p	number,
			dt_inicial_p		date,
			dt_final_p		date,
			nm_usuario_p		varchar2) is 

begin

if	(nr_interno_conta_p is not null) then
	update	conta_paciente
	set	dt_periodo_inicial = dt_inicial_p,
		dt_periodo_final = dt_final_p
	where	nr_interno_conta = nr_interno_conta_p;
	commit;
end if;

end atualiza_periodo_fatura;
/