create or replace
procedure pls_gerar_justific_automatica
			(	nr_seq_requisicao_p	Number,
				nr_seq_guia_p		Number,
				nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar automaticamente uma solicita��o de justificativa para o prestador, conforme regra cadastrada na fun��o OPS - Cadastro de Regras item Regra de justificativa autom�tica.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [X] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Avisar ao Eder sempre que for alterado ou incluido algum par�metro.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_segurado_w		Number(10);
nr_seq_plano_w			Number(10);
cd_doenca_w			Varchar2(10);
ie_tipo_segurado_w		Varchar2(255);
ie_preco_w			Varchar2(255);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
cd_area_w			Number(15);
cd_especialidade_w		Number(15);
cd_grupo_w			Number(15);
ie_origem_proced_ww		Number(10);
nr_seq_regra_just_aut_w		Number(10);
nr_seq_ocorrencia_w		Number(10);
ie_ocorrencia_w			Varchar2(1)	:= 'X';
nr_seq_exce_just_aut_w		Number(10);
ie_excessao_w			Varchar2(1)	:= 'N';
cd_proc_regra_w			Number(10);
nr_seq_auditoria_w		Number(10);
ie_orig_proc_regra_w		Number(10);
cd_grupo_proc_regra_w		Number(15);
cd_espec_regra_w		Number(15);
cd_area_proc_regra_w		Number(15);
cd_area_aud_w			Number(15);
cd_especialidade_aud_w		Number(15);
cd_grupo_aud_w			Number(15);
ie_origem_proced_2_w		Number(10);
cd_proc_aud_w			Number(10);
ie_orig_proc_aud_w		Number(10);
ds_mensagem_w			Varchar2(255);
qt_ocorrencia_w			Number(10);
qt_excessao_w			Number(10);
nr_seq_item_aud_w		Number(10);
ds_indicacao_clinica_w		Varchar2(255);
ie_estagio_w			Number(3);
ie_liminar_w			Varchar2(1) := 'N';
ie_tipo_guia_w			varchar2(2);
qt_ocorr_regra_w		Number(10);
ie_anexo_w			varchar2(1) := 'N';
qt_anexo_w			varchar2(5);
nr_seq_material_w		number(10);
nr_seq_grupo_contrato_w		number(10);
qt_existe_grupo_w		number(5);
nr_seq_contrato_w		number(10);
nr_seq_intercambio_w		number(10);
nr_seq_grupo_servico_w		number(10);
ie_grupo_servico_w		varchar2(1) := 'N';
nr_seq_prestador_w		number(10);
nr_seq_tipo_prestador_w		number(10);
nr_seq_prestador_regra_w	number(10);
nr_seq_tipo_prestador_regra_w	number(10);

Cursor C01 is
	select	b.nr_sequencia,
		b.cd_procedimento,
		b.ie_origem_proced,
		b.nr_seq_material
	from	pls_auditoria_item	b,
		pls_auditoria		a
	where	(a.nr_seq_requisicao	= nr_seq_requisicao_p
	or	a.nr_seq_guia		= nr_seq_guia_p)
	and	a.nr_sequencia		= b.nr_seq_auditoria
	and	b.ie_status = 'P';

Cursor C02 is
	select	nr_sequencia,
		nr_seq_grupo_contrato,
		nr_seq_grupo_servico,
		nr_seq_prestador,
		nr_seq_tipo_prestador
	from	pls_regra_just_automatica
	where	(cd_procedimento	is null	or cd_procedimento	 = cd_procedimento_w)
	and     (ie_origem_proced	is null	or ie_origem_proced	 = ie_origem_proced_ww)
	and     (cd_grupo_proc		is null	or cd_grupo_proc      	 = cd_grupo_w)
	and     (cd_especialidade	is null	or cd_especialidade 	 = cd_especialidade_w)
	and     (cd_area_procedimento	is null	or cd_area_procedimento	 = cd_area_w)
	and	(nr_seq_material	is null or nr_seq_material 	 = nr_seq_material_w)
	and	(ie_tipo_segurado	is null	or ie_tipo_segurado	 = ie_tipo_segurado_w)
	and	(ie_preco		is null	or ie_preco		 = ie_preco_w)
	and	(ie_tipo_guia 		is null or ie_tipo_guia 	 = ie_tipo_guia_w)
	and	(cd_doenca		is null	or cd_doenca		 = cd_doenca_w)
	and	(nr_seq_prestador	is null	or nr_seq_prestador	 = nr_seq_prestador_w)
	and	(nr_seq_tipo_prestador	is null	or nr_seq_tipo_prestador = nr_seq_tipo_prestador_w)
	and	sysdate			between	(dt_inicio_vigencia) 	and fim_dia(nvl(dt_fim_vigencia,sysdate))
	and 	(nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento and pls_obter_se_controle_estab('RE') = 'S')
	union all
	select	nr_sequencia,
		nr_seq_grupo_contrato,
		nr_seq_grupo_servico,
		nr_seq_prestador,
		nr_seq_tipo_prestador
	from	pls_regra_just_automatica
	where	(cd_procedimento	is null	or cd_procedimento	 = cd_procedimento_w)
	and     (ie_origem_proced	is null	or ie_origem_proced	 = ie_origem_proced_ww)
	and     (cd_grupo_proc		is null	or cd_grupo_proc      	 = cd_grupo_w)
	and     (cd_especialidade	is null	or cd_especialidade 	 = cd_especialidade_w)
	and     (cd_area_procedimento	is null	or cd_area_procedimento	 = cd_area_w)
	and	(nr_seq_material	is null or nr_seq_material 	 = nr_seq_material_w)
	and	(ie_tipo_segurado	is null	or ie_tipo_segurado	 = ie_tipo_segurado_w)
	and	(ie_preco		is null	or ie_preco		 = ie_preco_w)
	and	(ie_tipo_guia 		is null or ie_tipo_guia 	 = ie_tipo_guia_w)
	and	(cd_doenca		is null	or cd_doenca		 = cd_doenca_w)
	and	(nr_seq_prestador	is null	or nr_seq_prestador	 = nr_seq_prestador_w)
	and	(nr_seq_tipo_prestador	is null	or nr_seq_tipo_prestador = nr_seq_tipo_prestador_w)
	and	sysdate			between	(dt_inicio_vigencia) 	and fim_dia(nvl(dt_fim_vigencia,sysdate))
	and 	pls_obter_se_controle_estab('RE') = 'N';
	
Cursor C03 is
	select	nr_seq_ocorrencia
	from	pls_ocorrencia_just_aut
	where	nr_seq_regra_just_aut	= nr_seq_regra_just_aut_w;

begin

if	(nvl(nr_seq_requisicao_p,0)	<> 0) and (pls_obter_permite_solic_justif(nr_seq_requisicao_p)	= 'S') then
	begin
		select	nr_seq_segurado,
			nr_seq_plano,
			substr(nvl(ds_indicacao_clinica,'X'),1,255),
			ie_estagio,
			ie_tipo_guia,
			nr_seq_prestador
		into	nr_seq_segurado_w,
			nr_seq_plano_w,
			ds_indicacao_clinica_w,
			ie_estagio_w,
			ie_tipo_guia_w,
			nr_seq_prestador_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;
	exception
	when others then
		nr_seq_segurado_w	:= 0;
		nr_seq_plano_w	:= 0;
		ie_tipo_guia_w := '';
	end;

	begin
		select	cd_doenca
		into	cd_doenca_w
		from	pls_requisicao_diagnostico
		where	nr_seq_requisicao	= nr_seq_requisicao_p;
	exception
	when others then
		cd_doenca_w	:= 'X';
	end;
	
	select	count(1)
	into	qt_anexo_w
	from	pls_requisicao_anexo
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

	if	(qt_anexo_w > 0) then
		ie_anexo_w := 'S';
	end if;
elsif	(nvl(nr_seq_guia_p,0)	<> 0) then
	begin
		select	nr_seq_segurado,
			nr_seq_plano,
			substr(nvl(ds_indicacao_clinica,'X'),1,255),
			ie_estagio,
			ie_tipo_guia,
			nr_seq_prestador
		into	nr_seq_segurado_w,
			nr_seq_plano_w,
			ds_indicacao_clinica_w,
			ie_estagio_w,
			ie_tipo_guia_w,
			nr_seq_prestador_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;
	exception
	when others then
		nr_seq_segurado_w	:= 0;
		nr_seq_plano_w		:= 0;
		ie_tipo_guia_w 		:= '';
	end;

	begin
		select	cd_doenca
		into	cd_doenca_w
		from	pls_diagnostico
		where	nr_seq_guia	= nr_seq_guia_p;
	exception
	when others then
		cd_doenca_w	:= 'X';
	end;
	
	select	count(1)
	into	qt_anexo_w
	from	pls_guia_plano_anexo
	where	nr_seq_guia	= nr_seq_guia_p;

	if	(qt_anexo_w > 0) then
		ie_anexo_w := 'S';
	end if;
end if;


if (nr_seq_prestador_w is not null) then --Verifica a exist�ncia de prestador na guia ou requisi��o, existindo ent�o busca o tipo do prestador.
	begin
	select	nr_seq_tipo_prestador
	into	nr_seq_tipo_prestador_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_prestador_w;
	exception
		when others then
			null;
	end;		
end if;

if	(nr_seq_segurado_w	> 0) then
	select	nvl(ie_tipo_segurado,'X'),
		nr_seq_contrato,
		nr_seq_intercambio
	into	ie_tipo_segurado_w,
		nr_seq_contrato_w,
		nr_seq_intercambio_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	
	nr_seq_contrato_w := nvl(nr_seq_contrato_w,0);
	nr_seq_intercambio_w := nvl(nr_seq_intercambio_w,0);
end if;

if	(nr_seq_plano_w	> 0) then
	select	nvl(ie_preco,'X')
	into	ie_preco_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_w;
end if;

if	(ie_estagio_w	in (1,4)) then
	open C01;
	loop
	fetch C01 into
		nr_seq_item_aud_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_material_w;
	exit when C01%notfound;
		begin	

		if	(nvl(cd_procedimento_w,0) > 0) then
			pls_obter_estrut_proc(	cd_procedimento_w, ie_origem_proced_w, cd_area_w,
						cd_especialidade_w, cd_grupo_w, ie_origem_proced_ww);

		end if;
	
		open C02;
		loop
		fetch C02 into	
			nr_seq_regra_just_aut_w,
			nr_seq_grupo_contrato_w,
			nr_seq_grupo_servico_w,
			nr_seq_prestador_regra_w,
			nr_seq_tipo_prestador_regra_w;
		exit when C02%notfound;
			begin		
			ie_liminar_w := 'N';
			
			if	(nvl(nr_seq_grupo_contrato_w,0) <> 0) then
				select	count(1)
				into	qt_existe_grupo_w
				from	pls_preco_contrato
				where	nr_seq_grupo	= nr_seq_grupo_contrato_w
				and	(nr_seq_contrato	= nr_seq_contrato_w
				or	nr_seq_intercambio	= nr_seq_intercambio_w);
				if	(qt_existe_grupo_w = 0) then
					goto final;
				end if; 
			end if;	

			/*Grupo servi�o*/
			if	(nvl(nr_seq_grupo_servico_w,0) > 0) then
				ie_grupo_servico_w	:= pls_se_grupo_preco_servico(nr_seq_grupo_servico_w, cd_procedimento_w, ie_origem_proced_w);
				if	(ie_grupo_servico_w	= 'N') then
					goto final;
				end if;
			end if;			
			
			select	count(1)
			into	qt_ocorr_regra_w
			from	pls_ocorrencia_just_aut
			where	nr_seq_regra_just_aut	= nr_seq_regra_just_aut_w;

			if	(qt_ocorr_regra_w	> 0) then
				open C03;
				loop
				fetch C03 into	
					nr_seq_ocorrencia_w;
				exit when C03%notfound;
					begin					
					if	(nvl(nr_seq_requisicao_p,0)	<> 0) then
						select	count(1)
						into	qt_ocorrencia_w
						from	pls_ocorrencia_benef
						where	nr_seq_requisicao	= nr_seq_requisicao_p
						and	nr_seq_ocorrencia	= nr_seq_ocorrencia_w;
					elsif	(nvl(nr_seq_guia_p,0)	<> 0) then
						select	count(1)
						into	qt_ocorrencia_w
						from	pls_ocorrencia_benef
						where	nr_seq_guia_plano	= nr_seq_guia_p
						and	nr_seq_ocorrencia	= nr_seq_ocorrencia_w;
					end if;

					if	(qt_ocorrencia_w	> 0) then
						if	(nvl(nr_seq_requisicao_p,0)	<> 0) then
							ie_liminar_w	:= pls_obter_se_benef_liminar(nr_seq_requisicao_p);
						elsif	(nvl(nr_seq_guia_p,0)	<> 0) then
							ie_liminar_w	:= pls_obter_se_bene_guia_liminar(nr_seq_guia_p);
						end if;

						select	count(1)
						into	qt_excessao_w
						from	pls_excecao_just_aut
						where	nr_seq_regra_just_aut		= nr_seq_regra_just_aut_w
						and	((cd_procedimento is null)	or (cd_procedimento		= cd_procedimento_w))
						and     (((cd_procedimento is not null) and (ie_origem_proced  		= ie_origem_proced_ww))
						or	(cd_procedimento is null))
						and     ((cd_grupo_proc is null)	or (cd_grupo_proc		= cd_grupo_w))
						and     ((cd_especialidade is null)	or (cd_especialidade		= cd_especialidade_w))
						and     ((cd_area_procedimento is null) or (cd_area_procedimento	= cd_area_w))
						and	((nr_seq_grupo_contrato is null) or (nr_seq_grupo_contrato	= nr_seq_grupo_contrato_w))
						and	((nvl(ie_indicacao_clinica,'N')	= 'N')
						or	(ie_indicacao_clinica	= 'S')	and (ds_indicacao_clinica_w	<> 'X'))
						and	((ie_liminar_judicial	= 'N') or (ie_liminar_judicial	is null) or (ie_liminar_judicial = ie_liminar_w))
						and	((ie_anexo = 'N') or (ie_anexo is null) or (ie_anexo = ie_anexo_w))
						and	((nr_seq_prestador is null) or (nr_seq_prestador = nr_seq_prestador_regra_w))
						and	((nr_seq_tipo_prestador is null) or (nr_seq_tipo_prestador = nr_seq_tipo_prestador_regra_w))
						and	sysdate	between (dt_inicio_vigencia) and (fim_dia(nvl(dt_fim_vigencia,sysdate)));

						if	(qt_excessao_w	= 0) then
							pls_inserir_just_automatica(nr_seq_requisicao_p, nr_seq_guia_p, nr_seq_regra_just_aut_w, nr_seq_item_aud_w, nm_usuario_p);
						end if;
					end if;
					end;
				end loop;
				close C03;
			else
				if	(nvl(nr_seq_requisicao_p,0)	<> 0) then
					ie_liminar_w	:= pls_obter_se_benef_liminar(nr_seq_requisicao_p);
				elsif	(nvl(nr_seq_guia_p,0)	<> 0) then
					ie_liminar_w	:= pls_obter_se_bene_guia_liminar(nr_seq_guia_p);
				end if;

				select	count(1)
				into	qt_excessao_w
				from	pls_excecao_just_aut
				where	nr_seq_regra_just_aut		= nr_seq_regra_just_aut_w
				and	((cd_procedimento is null)	or (cd_procedimento		= cd_procedimento_w))
				and     (((cd_procedimento is not null) and (ie_origem_proced  		= ie_origem_proced_ww))
				or	(cd_procedimento is null))
				and     ((cd_grupo_proc is null)	 or (cd_grupo_proc		= cd_grupo_w))
				and     ((cd_especialidade is null)	or (cd_especialidade		= cd_especialidade_w))
				and     ((cd_area_procedimento is null) or (cd_area_procedimento	= cd_area_w))
				and	((nr_seq_grupo_contrato is null) or (nr_seq_grupo_contrato	= nr_seq_grupo_contrato_w))
				and	((nvl(ie_indicacao_clinica,'N')	= 'N')
				or	(ie_indicacao_clinica	= 'S')	and (ds_indicacao_clinica_w	<> 'X'))
				and	(nvl(ie_liminar_judicial,'N') 	= ie_liminar_w)
				and	((ie_liminar_judicial	= 'N') or (ie_liminar_judicial	is null) or (ie_liminar_judicial = ie_liminar_w))
				and	((ie_anexo = 'N') or (ie_anexo is null) or (ie_anexo = ie_anexo_w))				
				and	((nr_seq_prestador is null) or (nr_seq_prestador = nr_seq_prestador_regra_w))
				and	((nr_seq_tipo_prestador is null) or (nr_seq_tipo_prestador = nr_seq_tipo_prestador_regra_w))
				and	sysdate	between (dt_inicio_vigencia) and (fim_dia(nvl(dt_fim_vigencia,sysdate)));

				if	(qt_excessao_w	= 0) then
					pls_inserir_just_automatica(nr_seq_requisicao_p, nr_seq_guia_p, nr_seq_regra_just_aut_w, nr_seq_item_aud_w, nm_usuario_p);
				end if;
			end if;
			<<final>>
			qt_existe_grupo_w := 0;
			end;			
		end loop;
		close C02;
		end;
	end loop;
	close C01;
end if;

commit;

end pls_gerar_justific_automatica;
/
