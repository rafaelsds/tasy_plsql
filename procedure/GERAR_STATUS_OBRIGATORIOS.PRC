create or replace
procedure Gerar_Status_Obrigatorios (nr_sequencia_regra_p number, nm_usuario_p varchar2) is

nr_sequence_w	number;
sequence_est_w  number;

begin

select STATUS_REGRA_ENTREGA_SEQ.nextval
into nr_sequence_w
from dual;

nr_sequence_w := nr_sequence_w-1;

	insert into STATUS_REGRA_ENTREGA (nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec ,nm_usuario_nrec,ie_status_execucao,nr_seq_regra) values (nr_sequence_w,sysdate, nm_usuario_p, sysdate, nm_usuario_p ,'40', nr_sequencia_regra_p);
	
nr_sequence_w := nr_sequence_w+1;
	
	insert into STATUS_REGRA_ENTREGA (nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec ,nm_usuario_nrec,ie_status_execucao,nr_seq_regra) values (nr_sequence_w,sysdate, nm_usuario_p, sysdate, nm_usuario_p ,'60', nr_sequencia_regra_p);	
	
	select Estagio_Status_entrega_Seq.NextVal
	into sequence_est_w
	from dual;  

	insert into Estagio_Status_entrega (NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO,DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NR_SEQ_ESTAGIO,NR_SEQ_STATUS)  values (
	sequence_est_w, sysdate, nm_usuario_p, sysdate,nm_usuario_p,(select nr_sequencia from estagio_entrega where ds_estagio = wheb_mensagem_pck.get_texto(717235)),
	nr_sequence_w);       
	
end Gerar_Status_Obrigatorios;
/