create or replace
procedure pls_gerar_prazo_impugnacao
			(	nr_seq_conta_p		Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

dt_recebimento_w		date;
nr_seq_processo_w		number(10);
qt_dias_impugnacao_w		number(10);

begin

select	obter_valor_param_usuario(1238,4,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p)
into	qt_dias_impugnacao_w
from	dual;

begin 
	select	a.dt_recebimento,
		a.nr_sequencia
	into	dt_recebimento_w,
		nr_seq_processo_w
	from	pls_processo a,
		pls_processo_conta b
	where	a.nr_sequencia	= b.nr_seq_processo
	and	b.nr_sequencia	= nr_seq_conta_p;
exception
when others then
	dt_recebimento_w := null;
	nr_seq_processo_w:= null;
end;
	
if	(dt_recebimento_w is not null) then
	update	pls_processo_conta
	set	dt_impugnacao	= (dt_recebimento_w + nvl(qt_dias_impugnacao_w,0))
	where	nr_sequencia	= nr_seq_conta_p;
end if;

commit;

end pls_gerar_prazo_impugnacao;
/
