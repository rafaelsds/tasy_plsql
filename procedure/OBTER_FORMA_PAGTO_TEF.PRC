create or replace
procedure OBTER_FORMA_PAGTO_TEF(	nr_seq_bandeira_p		in number,
					ie_tipo_cartao_p		in varchar2,
					qt_parcelas_p		in number,
					nr_seq_forma_pagto_p	out number) is

nr_seq_forma_pagto_w	number(15);

begin

select	max(a.nr_seq_forma_pagto)
into	nr_seq_forma_pagto_w
from	forma_pagto_tef a,
	FORMA_PAGTO_CARTAO_CR b
where	a.nr_seq_forma_pagto	= b.nr_sequencia
and	a.nr_seq_bandeira		= nr_seq_bandeira_p
and	b.ie_situacao		= 'A'
and	((a.ie_tipo_cartao    		= ie_tipo_cartao_p)
	 or (a.ie_tipo_cartao 	= 'A'))
and	decode(qt_parcelas_p,0,1, qt_parcelas_p) between a.nr_parcela_inicio and a.nr_parcela_fim;

nr_seq_forma_pagto_p	:= nr_seq_forma_pagto_w;

end	OBTER_FORMA_PAGTO_TEF;
/