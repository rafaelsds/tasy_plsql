CREATE OR REPLACE
PROCEDURE Gravar_Solicitacao_Compra(	CD_LOCAL_ESTOQUE_P       NUMBER,
						CD_CLASSE_MATERIAL_P     NUMBER,
						CD_PESSOA_SOLICITANTE_P  VARCHAR2,
						NM_USUARIO_P             VARCHAR2) IS

DT_ATUALIZACAO_W		date		:= SYSDATE;
DT_SOLICITACAO_COMPRA_W	date	 	:= SYSDATE;
DT_AUTORIZACAO_W		date	 	:= SYSDATE;
DT_SOLIC_ITEM_W		date	 	:= SYSDATE;
DT_MESANO_REFERENCIA_W	date;
CD_MATERIAL_W			number(6)    	:= 0;
CD_CLASSE_MATERIAL_W		number(5)    	:= 0;
CD_ESTABELECIMENTO_W		number(4)    	:= 0;
CD_LOCAL_ESTOQUE_W		number(4)    	:= 0;
QT_ESTOQUE_MINIMO_W		number(13,4) 	:= 0;
QT_ESTOQUE_MAXIMO_W		number(13,4) 	:= 0;
QT_PONTO_PEDIDO_W		number(13,4) 	:= 0;
QT_MATERIAL_W			number(13,4) 	:= 0;
QT_ESTOQUE_W			number(13,4) 	:= 0;
CD_UNIDADE_MEDIDA_COMPRA_W	varchar2(30)  	:= '';
NR_SOLIC_COMPRA_W		number(10)   	:= 0;
QT_CONV_COMPRA_ESTOQUE_W	number(10)   	:= 0;
NR_SEQUENCIA_W		number(4)		:= 0;


CURSOR C01 IS
	select	a.cd_material,
             	substr(obter_dados_material_estab(a.cd_material,b.cd_estabelecimento,'UMC'),1,255) cd_unidade_medida_compra,
             	a.cd_classe_material,
/*		nvl(a.qt_estoque_minimo,1),
		nvl(a.qt_estoque_maximo,1),
		nvl(a.qt_ponto_pedido,1), Fabio 28/05/2004 - alterado o select para buscar da function*/
		nvl(Obter_mat_estabelecimento(b.cd_estabelecimento, 0, a.cd_material, 'MI'),1),
		nvl(Obter_mat_estabelecimento(b.cd_estabelecimento, 0, a.cd_material, 'MA'),1),
		nvl(Obter_mat_estabelecimento(b.cd_estabelecimento, 0, a.cd_material, 'PP'),1),
		nvl(a.qt_conv_compra_estoque,1),
		b.cd_estabelecimento,
		(b.qt_estoque + nvl((c.qt_material * a.qt_conv_compra_estoque) ,0) +
                             nvl((d.qt_material * a.qt_conv_compra_estoque) ,0)  ) qt_compra
	from	material a,
		saldo_estoque b,
		ordem_compra_item c,
		solic_compra_item d
       where	(b.cd_local_estoque     	= CD_LOCAL_ESTOQUE_P)
         and	(b.dt_mesano_referencia 	= DT_MESANO_REFERENCIA_W) 
         and	(a.cd_material_estoque  	= b.cd_material)
         and	(b.cd_material          	= a.cd_material)
         and	(a.cd_classe_material   	= CD_CLASSE_MATERIAL_P)
         and	(a.cd_material          	= c.cd_material(+))
         and	(a.cd_material          	= d.cd_material(+))
         and	(c.qt_material_entregue(+) is null)
         and	(d.nr_cot_compra(+)      	is null) 
         and	((b.qt_estoque + nvl((c.qt_material * a.qt_conv_compra_estoque) ,0)
                            + nvl((d.qt_material * a.qt_conv_compra_estoque) ,0))  < 			    

				nvl(Obter_mat_estabelecimento(b.cd_estabelecimento, 0, a.cd_material, 'PP'),1));
BEGIN

OPEN C01;
LOOP
    FETCH C01 into 
          CD_MATERIAL_W,
          CD_UNIDADE_MEDIDA_COMPRA_W,
          CD_CLASSE_MATERIAL_W,
          QT_ESTOQUE_MINIMO_W,
          QT_ESTOQUE_MAXIMO_W,
          QT_PONTO_PEDIDO_W,
          QT_CONV_COMPRA_ESTOQUE_W,
          CD_ESTABELECIMENTO_W,
          QT_ESTOQUE_W;

    if    C01%FOUND then
		begin
		begin
		SELECT DT_MESANO_VIGENTE
		INTO DT_MESANO_REFERENCIA_W
		FROM PARAMETRO_ESTOQUE
		where cd_estabelecimento = cd_estabelecimento_w;
		exception
			when others then
			wheb_mensagem_pck.exibir_mensagem_abort(266055);
			--'N�o encontrado parametro de estoque');
		end;
		if	NR_SOLIC_COMPRA_W = 0 then
			begin
			select solic_compra_seq.nextval
			into nr_solic_compra_w                 
			from dual;

			INSERT INTO SOLIC_COMPRA(
				NR_SOLIC_COMPRA, CD_ESTABELECIMENTO, DT_SOLICITACAO_COMPRA,
				DT_ATUALIZACAO, NM_USUARIO, IE_SITUACAO, CD_PESSOA_SOLICITANTE,
				CD_LOCAL_ESTOQUE, CD_CENTRO_CUSTO, CD_CONTA_CONTABIL,
				CD_SETOR_ATENDIMENTO, DS_OBSERVACAO, CD_MOTIVO_BAIXA,
				DT_BAIXA, DT_LIBERACAO, CD_PESSOA_AUTORIZA, DT_AUTORIZACAO,
				DT_IMPRESSAO, ie_aviso_chegada, ie_aviso_aprov_oc, ie_urgente,ie_tipo_solicitacao, ie_comodato, ie_semanal,
				nm_usuario_nrec,	dt_atualizacao_nrec)
			VALUES(
				NR_SOLIC_COMPRA_W, CD_ESTABELECIMENTO_W, DT_SOLICITACAO_COMPRA_W,
				DT_ATUALIZACAO_W,  NM_USUARIO_P, '1', CD_PESSOA_SOLICITANTE_P,
				CD_LOCAL_ESTOQUE_P, NULL, NULL, 
				NULL, NULL, NULL, 
				NULL, NULL, CD_PESSOA_SOLICITANTE_P, DT_AUTORIZACAO_W,  
				NULL, 'N', 'N', 'N',0,'N','N',
				nm_usuario_p,		sysdate);
			exception
				when others then
				wheb_mensagem_pck.exibir_mensagem_abort(266056);
				--'Erro ao gravar solicitacao'
			end;
		end if;
		QT_MATERIAL_W := round((QT_ESTOQUE_MAXIMO_W - QT_ESTOQUE_W) / QT_CONV_COMPRA_ESTOQUE_W);
		NR_SEQUENCIA_W   := (NR_SEQUENCIA_W + 1);

		INSERT INTO SOLIC_COMPRA_ITEM(
			NR_SOLIC_COMPRA,          NR_ITEM_SOLIC_COMPRA,   CD_MATERIAL,
			CD_UNIDADE_MEDIDA_COMPRA, QT_MATERIAL,            DT_ATUALIZACAO, 
			NM_USUARIO,               IE_SITUACAO,            DS_MATERIAL_DIRETO,
			DS_OBSERVACAO,            NR_COT_COMPRA,          NR_ITEM_COT_COMPRA,
			CD_MOTIVO_BAIXA,          DT_BAIXA,               DT_SOLIC_ITEM,
			ie_geracao,		qt_conv_compra_est_orig,
			qt_saldo_disp_estoque 	)
		VALUES(
			NR_SOLIC_COMPRA_W, NR_SEQUENCIA_W, CD_MATERIAL_W,
			CD_UNIDADE_MEDIDA_COMPRA_W, QT_MATERIAL_W, DT_ATUALIZACAO_W, 
			'Tasy', '1', NULL,
			NULL, NULL, NULL,
			NULL, NULL, DT_SOLIC_ITEM_W, 'S',obter_dados_material(CD_MATERIAL_W,'QCE'),
			obter_saldo_disp_estoque(cd_estabelecimento_w, cd_material_w, cd_local_estoque_w, trunc(sysdate,'mm')));

		INSERT INTO SOLIC_COMPRA_ITEM_ENTREGA(
			NR_SOLIC_COMPRA,          NR_ITEM_SOLIC_COMPRA,   NR_ITEM_SOLIC_COMPRA_ENTR,
			QT_ENTREGA_SOLICITADA,    DT_ENTREGA_SOLICITADA,  DT_ATUALIZACAO, 
			NM_USUARIO,               DS_OBSERVACAO)
		VALUES(
			NR_SOLIC_COMPRA_W, NR_SEQUENCIA_W, 1,
			QT_MATERIAL_W,     SYSDATE,  DT_ATUALIZACAO_W, 
			'Tasy', NULL);
		end;
    else
         exit;
    end if;  
END LOOP;
CLOSE C01;
COMMIT;
END Gravar_Solicitacao_Compra;
/
