create or replace
procedure gerar_hor_cronograma(nr_seq_cronograma_p	number) 
is

qt_hora_adic_w		number(10,2) := 0;
ie_atividade_extra_w	varchar2(2);
qt_min_ativ_w		number(10);
nr_sequencia_w		number(10);
qt_total_hora_adic_w	number(15,2);


Cursor C01 is
	select	nr_sequencia
	from	PROJ_CRON_ETAPA
	where	nr_seq_cronograma = nr_seq_cronograma_p
	and	ie_modulo = 'N'
	order by 1;

Cursor C02 is
	select	qt_min_ativ,
		ie_atividade_extra
	from	proj_rat_ativ
	where	nr_seq_etapa_cron = nr_sequencia_w;
	
begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin

	atualizar_horas_etapa_cron(nr_sequencia_w);

	qt_total_hora_adic_w := 0;
	open C02;
	loop
	fetch C02 into	
		qt_min_ativ_w,
		ie_atividade_extra_w;
	exit when C02%notfound;
		begin
	qt_hora_adic_w := 0;
	
	if 	(ie_atividade_extra_w = 'S') then
		qt_hora_adic_w := nvl(round(dividir(qt_min_ativ_w,60),2),0);
	end if;
	
	qt_total_hora_adic_w := qt_total_hora_adic_w + qt_hora_adic_w;
	
	update	proj_cron_etapa
	set	qt_horas_etapa_adic = qt_total_hora_adic_w
	where	nr_sequencia = nr_sequencia_w;

	end;
	end loop;
	close C02;
	
	end;
end loop;
close C01;

commit;

end gerar_hor_cronograma;
/
