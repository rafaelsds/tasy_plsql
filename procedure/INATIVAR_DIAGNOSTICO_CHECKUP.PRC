create or replace
procedure inativar_diagnostico_checkup(
			nr_sequencia_p	number,
			nm_usuario_p	varchar2,
			ie_opcao_p	varchar2) is
begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	if	(ie_opcao_p = 'I') then		
		checkup_suspender_orient(nr_sequencia_p,nm_usuario_p);
	else
		checkup_suspender_diagnostico(nr_sequencia_p,nm_usuario_p);
	end if;	
	
	if	(ie_opcao_p is not null) then
		begin
		eliminar_orientacao_checkup(nr_sequencia_p,ie_opcao_p);
		end;
	end if;	
	end;
end if;
commit;
end inativar_diagnostico_checkup;
/