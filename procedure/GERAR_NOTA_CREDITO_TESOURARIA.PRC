create or replace
procedure gerar_nota_credito_tesouraria(nr_titulo_p			number,
				nr_seq_baixa_p			number,
				cd_pessoa_fisica_p		varchar2,
				cd_cgc_p			varchar2,
				nr_seq_pagador_p		number,
				dt_baixa_p			date,
				vl_transacao_p			number,
				ie_acao_p			varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

vl_saldo_nota_w			number(15,2);
nr_seq_nota_credito_w		number(10);
ds_observacao_w			nota_credito.ds_observacao%type;
/* Projeto Multimoeda - Vari�veis */
vl_transacao_estrang_w		number(15,2);
vl_complemento_w		number(15,2);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;
cd_moeda_w			number(5);

begin

if	(ie_acao_p = 'I') then
	/* Projeto Multimoeda - Busca os dados da baixa para verificar se � moeda estrangeira */
	select	max(vl_recebido_estrang),
		max(vl_cotacao),
		max(cd_moeda)
	into	vl_transacao_estrang_w,
		vl_cotacao_w,
		cd_moeda_w
	from	titulo_receber_liq
	where	nr_titulo = nr_titulo_p
	and	nr_sequencia = nr_seq_baixa_p;
	/* Verifica se a baixa � em moeda estrangeria, caso positivo calcula os valores antes de gravar a nota */
	if (nvl(vl_transacao_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
		vl_complemento_w := vl_transacao_p - vl_transacao_estrang_w;
	else
		vl_transacao_estrang_w := null;
		vl_complemento_w := null;
		vl_cotacao_w := null;
		cd_moeda_w := null;
	end if;
	
	select	nota_credito_seq.nextval
	into	nr_seq_nota_credito_w
	from	dual;
	ds_observacao_w	:= substr(wheb_mensagem_pck.get_texto(303052),1,4000);
	
	insert into nota_credito
		(nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		vl_nota_credito,
		cd_pessoa_fisica,
		cd_cgc,
		nr_seq_pagador_aprop,
		dt_nota_credito,
		dt_vencimento,
		tx_juros,
		tx_multa,
		cd_tipo_taxa_juro,
		cd_tipo_taxa_multa,
		ie_origem,
		vl_saldo,
		nr_lote_contabil,
		ds_observacao,
		ie_situacao,
		nr_titulo_receber,
		nr_seq_baixa_origem,
		vl_nota_cred_estrang,
		vl_complemento,
		vl_cotacao,
		cd_moeda)
	values	(nr_seq_nota_credito_w,
		cd_estabelecimento_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		vl_transacao_p,
		cd_pessoa_fisica_p,
		cd_cgc_p,
		nr_seq_pagador_p,
		dt_baixa_p,
		null,
		null,
		null,
		null,
		null,
		'M',
		vl_transacao_p,
		null,
		ds_observacao_w,
		'A',
		nr_titulo_p,
		nr_seq_baixa_p,
		vl_transacao_estrang_w,
		vl_complemento_w,
		vl_cotacao_w,
		cd_moeda_w);
		
	insert	into titulo_receber_nc
		(dt_atualizacao,
		nm_usuario,
		nr_seq_nota_credito,
		nr_sequencia,
		nr_titulo_rec,
		vl_nota_credito,
		nr_seq_liq)
	values	(sysdate,
		nm_usuario_p,
		nr_seq_nota_credito_w,
		titulo_receber_nc_seq.nextval,
		nr_titulo_p,
		vl_transacao_p,
		nr_seq_baixa_p);
else
	select	max(nr_sequencia)
	into	nr_seq_nota_credito_w
	from	nota_credito a
	where	a.nr_titulo_receber	= nr_titulo_p
	and	a.nr_seq_baixa_origem	= nr_seq_baixa_p
	and	a.ie_situacao		<> 'C';
	
	if	(nr_seq_nota_credito_w is not null) then
		select	vl_saldo,
			vl_nota_cred_estrang,
			vl_cotacao,
			cd_moeda
		into	vl_saldo_nota_w,
			vl_transacao_estrang_w,
			vl_cotacao_w,
			cd_moeda_w
		from	nota_credito a
		where	a.nr_sequencia	= nr_seq_nota_credito_w;

		/* Projeto Multimoeda - Verifica se a nota de cr�dito � em moeda estrangeira, caso positivo calcula os valores antes de gravar a baixa */
		if (nvl(vl_transacao_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
			vl_transacao_estrang_w := nvl(vl_saldo_nota_w,0) / vl_cotacao_w;
			vl_complemento_w := vl_saldo_nota_w - vl_transacao_estrang_w;
		else
			vl_transacao_estrang_w := null;
			vl_complemento_w := null;
			vl_cotacao_w := null;
			cd_moeda_w := null;
		end if;
		
		insert into nota_credito_baixa
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_nota_credito,
			dt_baixa,
			vl_baixa,
			ie_cancelamento,
			vl_baixa_estrang,
			vl_complemento,
			vl_cotacao,
			cd_moeda)
		values	(nota_credito_baixa_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_nota_credito_w,
			nvl(dt_baixa_p,sysdate),
			vl_saldo_nota_w,
			'S',
			vl_transacao_estrang_w,
			vl_complemento_w,
			vl_cotacao_w,
			cd_moeda_w);
			
		delete	from titulo_receber_nc
		where	nr_seq_nota_credito	= nr_seq_nota_credito_w
		and	nr_titulo_rec		= nr_titulo_p;

		atualizar_saldo_nota_credito(nr_seq_nota_credito_w,nm_usuario_p);
	end if;
end if;

--commit;

end gerar_nota_credito_tesouraria;
/
