create or replace procedure san_grava_inf_bagmixer (nr_operador_p	varchar2,
					nr_doacao_p		varchar2,
					vl_vol_solicitado_ml_p	number,
					vl_vol_coletado_ml_p	number,
					dt_coleta_p		date,
					hr_inicio_coleta_p	varchar2,
					hr_duracao_coleta_p	varchar2,
					nr_frasco1_p		number,
					nr_frasco2_p		number,
					nr_frasco3_p		number,
					nr_frasco4_p		number,
					nr_frasco5_p		number,
					nr_lote_bolsa_p		varchar2,
					nr_serie_equip_p	varchar2,
					nr_intercorrencia_p	varchar2,
					vl_fluxo_max_p		number,
					qt_peso_total_p		number,
					nm_usuario_p		varchar2) is 

nr_seq_doacao_ww	number(10) := null;
nr_seq_desistencia_w	number(10);
nr_serie_equip_w	number(10);
ie_realiza_insert_w	varchar2(1);
ie_realiza_update_w	varchar2(1);
cd_pessoa_coleta_w	varchar2(10);
ds_conduta_w		varchar2(255);
nr_seq_derivado_w	number(10);
ie_tipo_bolsa_w		varchar2(5);
nr_seq_conservante_w	number(10);
nr_seq_antic_w		number(10);
qt_peso_bolsa_padrao_w	number(10,3);
ie_regra_derivado_w	varchar2(1);
ds_valor_param_331_w	varchar2(255);

begin
if	(nr_doacao_p is not null) then
	
	ds_conduta_w := obter_valor_param_usuario(450,342,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);
	ie_regra_derivado_w := obter_valor_param_usuario(450,418,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);
	ds_valor_param_331_w :=  obter_valor_param_usuario(450, 331, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

	--Extrai a Seq. da doacao do codigo de barras			
	if (ds_valor_param_331_w = 'B') then
		select	max(nr_sequencia)
			into	nr_seq_doacao_ww
		from	san_doacao
		where	obter_isbt_doador(nr_sequencia, NULL,'I') = nr_doacao_p;
	else
		select	max(nr_sequencia)
			into	nr_seq_doacao_ww
		from	san_doacao
		where	cd_barras_integracao = nr_doacao_p;
	end if;

	select	decode(count(*), 0,'N', 'S')
		into	ie_realiza_update_w
	from	san_doacao x
	where	x.nr_sequencia = nr_seq_doacao_ww;

	select	max(x.cd_pessoa_fisica)
		into	cd_pessoa_coleta_w
	from	usuario x
	where	x.cd_barras  = nr_operador_p;

	select	max(x.nr_sequencia)
		into	nr_serie_equip_w
	from	san_local_doacao x
	where	x.cd_codigo_externo  = SOMENTE_NUMERO_CHAR(nr_serie_equip_p);

	if	(vl_vol_coletado_ml_p > 0) then
		select	nr_seq_derivado_padrao,
			nr_seq_antic,
			ie_tipo_bolsa
		into	nr_seq_derivado_w,
			nr_seq_antic_w,
			ie_tipo_bolsa_w
		from	san_doacao
		where	nr_sequencia = nr_seq_doacao_ww;
	
		if	(ie_regra_derivado_w = 'S') then
			select	max(nr_sequencia)
			into	nr_seq_conservante_w
			from	san_conservante
			where	nr_seq_anticoagulante = nr_seq_antic_w;
			
			qt_peso_bolsa_padrao_w := san_calc_peso_hemo(nr_seq_derivado_w,
									vl_vol_coletado_ml_p,
									null,
									nr_seq_conservante_w,
									null	,
									ie_tipo_bolsa_w);
		else
			qt_peso_bolsa_padrao_w := san_calc_peso_padrao(nr_seq_derivado_w,vl_vol_coletado_ml_p);
		end if;
	end if;

	if	(ie_realiza_update_w = 'S') then
	begin
		update	san_doacao x
		set	x.nr_seq_local			= nr_serie_equip_w,
			x.cd_pessoa_coleta		= cd_pessoa_coleta_w,
			x.cd_pessoa_inicio_coleta 	= cd_pessoa_coleta_w,
			x.nr_lote_bolsa			= trim(nr_lote_bolsa_p),
			x.qt_volume_estimado		= vl_vol_solicitado_ml_p,
			x.qt_coletada			= vl_vol_coletado_ml_p,
			x.qt_volume_real		= vl_vol_coletado_ml_p,
			x.qt_peso_bolsa_padrao		= qt_peso_bolsa_padrao_w,
			x.dt_inicio_coleta_real		= to_date(to_char(dt_coleta_p,'dd/mm/yyyy')||hr_inicio_coleta_p, 'dd/mm/yyyy hh24:mi:ss'),
			x.dt_fim_coleta_real		= to_date(to_char(dt_coleta_p,'dd/mm/yyyy')||hr_inicio_coleta_p, 'dd/mm/yyyy hh24:mi:ss') + (hr_duracao_coleta_p/1440),
			x.qt_min_coleta			= round(hr_duracao_coleta_p/60),
			x.dt_coleta			= to_date(to_char(dt_coleta_p,'dd/mm/yyyy')),
			x.vl_fluxo			= vl_fluxo_max_p,
			x.qt_peso_total_bolsa		= qt_peso_total_p
		where	1 = 1
		and	x.nr_sequencia			= nr_seq_doacao_ww;

	exception
		when others then
		gravar_log_tasy(-98766, 1||' - '||obter_texto_dic_objeto(102753, 0, 'CD='||nr_seq_doacao_ww||' -> '||sqlerrm), nm_usuario_p);
	end;

	elsif	(ie_realiza_update_w = 'N') then
		gravar_log_tasy(-98766, 2||' - '||obter_texto_dic_objeto(102168, 0, 'CD='||nr_seq_doacao_ww), nm_usuario_p);
	end if;
	
	begin
  
	if  ( nr_frasco1_p is not null and  nr_frasco1_p > 0 and nr_seq_doacao_ww is not null) then
		insert into SAN_COLETA_AMOSTRA(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_doacao,
			nr_frasco) 
		values (
			san_coleta_amostra_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_doacao_ww,
			nr_frasco1_p);
	end if;
	
	if  ( nr_frasco2_p is not null and  nr_frasco2_p > 0 and nr_seq_doacao_ww is not null) then
		insert into SAN_COLETA_AMOSTRA(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_doacao,
			nr_frasco) 
		values (
			san_coleta_amostra_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_doacao_ww,
			nr_frasco2_p);
	end if;
	
	if  ( nr_frasco3_p is not null and  nr_frasco3_p > 0 and nr_seq_doacao_ww is not null) then
		insert into SAN_COLETA_AMOSTRA(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_doacao,
			nr_frasco) 
		values (
			san_coleta_amostra_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_doacao_ww,
			nr_frasco3_p);
	end if;
	
	if  ( nr_frasco4_p is not null and  nr_frasco4_p > 0 and nr_seq_doacao_ww is not null) then
		insert into SAN_COLETA_AMOSTRA(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_doacao,
			nr_frasco) 
		values (
			san_coleta_amostra_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_doacao_ww,
			nr_frasco4_p);
	end if;
	
	if  ( nr_frasco5_p is not null and  nr_frasco5_p > 0 and nr_seq_doacao_ww is not null) then
		insert into SAN_COLETA_AMOSTRA(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_doacao,
			nr_frasco) 
		values (
			san_coleta_amostra_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_doacao_ww,
			nr_frasco5_p);
	end if;
	
	if	( nr_intercorrencia_p is not null ) then
  
		select	max(nr_sequencia)
			into	nr_seq_desistencia_w
		from 	san_motivo_desistencia
		where 	ie_situacao = 'A'
		and 	cd_externo = nr_intercorrencia_p;
		
		if	( nr_seq_desistencia_w is not null ) then 
			update	san_doacao
			set 	nr_motivo_desistencia = nr_seq_desistencia_w
			where 	nr_sequencia = nr_seq_doacao_ww;
		end if;

	end if;
	
	
	exception
		when others then
		gravar_log_tasy(-98766, wheb_mensagem_pck.get_texto(803067,
								'NR_DOACAO='||nr_doacao_p||
								';NR_INTERCORRENCIA='||nr_intercorrencia_p), nm_usuario_p);
	end;	

else
	gravar_log_tasy(-98766, '6 - '||obter_texto_dic_objeto(102164, 0, 'CD='||nr_seq_doacao_ww), nm_usuario_p);
end if;

commit;

end san_grava_inf_bagmixer;
/
