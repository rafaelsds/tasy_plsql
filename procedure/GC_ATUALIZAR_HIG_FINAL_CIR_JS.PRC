create or replace
procedure gc_atualizar_hig_final_cir_js(
				nr_cirurgia_p		number,
				dt_termino_p		date,
				nr_min_duracao_real_p	number,
				nr_atendimento_p		number,
				dt_entrada_unidade_p	date,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				ds_pergunta_p	out	varchar
				) is
		
		
ds_pergunta_w	varchar2(255) := '';
		
begin

-- RETIRADO NA OS 697785, POIS J� FINALIZA A CIRURGIA NO C�DIGO FONTE.
--if	(nr_min_duracao_real_p is not null) and
--	(dt_termino_p is not null) then
--	begin
--	finalizar_cirurgia(nr_cirurgia_p,dt_termino_p,nr_min_duracao_real_p,nm_usuario_p);
	
	if	(nr_atendimento_p is not null) and
		(dt_entrada_unidade_p is not null) then
		begin
		gerar_higieni_final_cirurgia(nr_atendimento_p,dt_entrada_unidade_p,nm_usuario_p,cd_estabelecimento_p);
		end;
	end if;
--	end;	
	
--end if;

ds_pergunta_w := substr(obter_texto_tasy (51612, wheb_usuario_pck.get_nr_seq_idioma),1,255);

ds_pergunta_p := ds_pergunta_w;


end gc_atualizar_hig_final_cir_js;
/