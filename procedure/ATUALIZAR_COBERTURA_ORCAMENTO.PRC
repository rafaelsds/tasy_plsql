create or replace
procedure Atualizar_Cobertura_Orcamento(nr_seq_orcamento_p	number,
					nm_usuario_p		Varchar2) is 


cd_estabelecimento_w	number(4,0);
cd_convenio_w		number(5,0);
cd_categoria_w		varchar2(10 char);
cd_plano_w		varchar2(10 char);
cd_tipo_acomodacao_w	number(4,0);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
dt_orcamento_w		date;
ie_tipo_atendimento_w	number(3,0);
nr_atendimento_w	number(10,0);
ie_autorizacao_w	varchar2(3 char);
cd_material_w		number(10,0);
ie_tipo_convenio_w	number(2,0);
ds_ret_w		number(10,0);
ds_erro_w		varchar2(255 char);
nr_seq_proc_interno_w	number(10,0);
ie_regra_w		orcamento_paciente_proc.ie_regra_plano%type;

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		nvl(nr_seq_proc_interno,0) nr_seq_proc_interno,
		qt_procedimento,
		nr_sequencia,
		nvl(nr_seq_exame,0) nr_seq_exame
	from	orcamento_paciente_proc
	where	nr_sequencia_orcamento = nr_seq_orcamento_p
	order by nr_sequencia;	
	
Cursor C02 is
	select	cd_material,
		qt_material,
		nr_sequencia
	from	orcamento_paciente_mat
	where	nr_sequencia_orcamento = nr_seq_orcamento_p
	order by nr_sequencia;	


C01_w	C01%rowtype;
C02_w	C02%rowtype;

begin

select	max(cd_estabelecimento),
	max(cd_convenio),
	max(cd_categoria),
	max(cd_plano),
	max(cd_tipo_acomodacao),
	max(dt_orcamento),
	max(ie_tipo_atendimento),
	nvl(max(nr_atendimento),0),
	max(obter_tipo_convenio(cd_convenio))
into	cd_estabelecimento_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w,
	cd_tipo_acomodacao_w,
	dt_orcamento_w,
	ie_tipo_atendimento_w,
	nr_atendimento_w,
	ie_tipo_convenio_w
from 	orcamento_paciente
where 	nr_sequencia_orcamento = nr_seq_orcamento_p;

if	(ie_tipo_atendimento_w is null) and
	(nr_atendimento_w > 0) then
	
	select 	max(ie_tipo_atendimento)
	into	ie_tipo_atendimento_w
	from 	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_w;
	
end if;

open C01;
loop
fetch C01 into	
	C01_w;
exit when C01%notfound;
	begin
	
	cd_procedimento_w	:= C01_w.cd_procedimento;
	ie_origem_proced_w	:= C01_w.ie_origem_proced;
	nr_seq_proc_interno_w	:= C01_w.nr_seq_proc_interno;
	ie_regra_w		:= null;
	
	if	(C01_w.nr_seq_proc_interno > 0) then
	
	
		Obter_Proc_Tab_Interno_Conv(C01_w.nr_seq_proc_interno,
					cd_estabelecimento_w,
					cd_convenio_w,
					cd_categoria_w,
					cd_plano_w,
					0,
					cd_procedimento_w,
					ie_origem_proced_w,
					0,
					dt_orcamento_w,
					cd_tipo_acomodacao_w,
					null,
					null,
					null,
					ie_tipo_atendimento_w,
					0,
					0,
					null);	
	
	end if;
	
	if	(C01_w.nr_seq_exame > 0) then				
		Obter_Exame_Lab_Convenio(C01_w.nr_seq_exame, cd_convenio_w, cd_categoria_w, ie_tipo_atendimento_w, cd_estabelecimento_w,
			ie_tipo_convenio_w,C01_w.nr_seq_proc_interno, null, cd_plano_w, ds_ret_w, cd_procedimento_w, ie_origem_proced_w, ds_erro_w, ds_ret_w, dt_orcamento_w);
	end if;
	
	consiste_proc_orc(nr_seq_orcamento_p, 
			cd_procedimento_w, 
			ie_origem_proced_w, 
			C01_w.qt_procedimento, 
			cd_estabelecimento_w, 
			nm_usuario_p, 
			ie_autorizacao_w, 
			nr_seq_proc_interno_w,
			ie_regra_w);
			
	update	orcamento_paciente_proc
	set 	cd_procedimento		= cd_procedimento_w,
		ie_origem_proced	= ie_origem_proced_w,
		ie_autorizacao		= ie_autorizacao_w,
		ie_regra_plano		= ie_regra_w
	where 	nr_sequencia_orcamento	= nr_seq_orcamento_p
	and 	nr_sequencia		= C01_w.nr_sequencia;	
	
	end;
end loop;
close C01;


open C02;
loop
fetch C02 into	
	C02_w;
exit when C02%notfound;
	begin
	
	cd_material_w	:= C02_w.cd_material;
	ie_regra_w	:= null;
	
	consiste_mat_orc(nr_seq_orcamento_p,
			 cd_material_w,
			 C02_w.qt_material,
			 cd_estabelecimento_w,
			 nm_usuario_p,
			 ie_autorizacao_w,
			 ie_regra_w);
						
	update	orcamento_paciente_mat
	set 	ie_autorizacao	= ie_autorizacao_w,
		ie_regra_plano	= ie_regra_w
	where 	nr_sequencia_orcamento	= nr_seq_orcamento_p
	and 	nr_sequencia		= C02_w.nr_sequencia;
	
	end;
end loop;
close C02;	

calcular_orcamento_paciente(nr_seq_orcamento_p, nm_usuario_p, cd_estabelecimento_w);

commit;

end Atualizar_Cobertura_Orcamento;
/
