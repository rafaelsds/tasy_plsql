create or replace
procedure pls_oc_cta_tratar_val_110_imp(nr_seq_combinada_p		in pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p		in pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p		in pls_oc_cta_selecao_imp.nr_id_transacao%type) is 
					
tb_seq_selecao_w	pls_util_cta_pck.t_number_table;
tb_valido_w		pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w		pls_util_cta_pck.t_varchar2_table_4000;
nr_indice_w		pls_integer;
ie_gerar_ocorrencia_w	varchar2(1);

-- Informa��es da valida��o guia principal autoriza��o
cursor C01 (	nr_seq_oc_cta_comb_pc	pls_oc_cta_combinada.nr_sequencia%type) is
	select	nvl(a.ie_atend_ant_benef,'N') ie_atend_ant_benef,
		nvl(a.ie_benef_nao_ident,'N') ie_benef_nao_ident,
		a.ie_tipo_repasse
	from	pls_oc_cta_val_benef a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_pc;
	
c01_w		c01%rowtype;

cursor c02 (	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type,
		ie_benef_nao_ident_pc	pls_oc_cta_val_benef.ie_benef_nao_ident%type,
		ie_atend_ant_benef_pc	pls_oc_cta_val_benef.ie_atend_ant_benef%type,
		ie_tipo_repasse_pc	pls_oc_cta_val_benef.ie_tipo_repasse%type)is
	select	'P'			ie_tipo_item,
		sel.nr_sequencia 	nr_seq_selecao,
		proc.dt_execucao_conv,
		seg.dt_contratacao,
		(select count(1)
		from	pls_segurado_carteira
		where	nr_seq_segurado = seg.nr_sequencia
		and 	cd_usuario_plano is not null) qt_benef_localizado,
		(select count(1)
		from	pls_segurado_repasse	r
		where	r.nr_seq_segurado	= conta.nr_seq_segurado_conv
		and	r.ie_tipo_repasse	= ie_tipo_repasse_pc
		and	conta.dt_atendimento_conv between trunc(nvl(r.dt_repasse,conta.dt_atendimento_conv)) and fim_dia(nvl(r.dt_fim_repasse,conta.dt_atendimento_conv))) qt_repasse
	from	pls_oc_cta_selecao_imp 	sel,
		pls_conta_proc_imp 	proc,
		pls_conta_imp		conta,
		pls_segurado		seg
	where	sel.nr_id_transacao 	= nr_id_transacao_pc
	and	sel.ie_valido 		= 'S'
	and	conta.nr_sequencia 	= sel.nr_seq_conta
	and	proc.nr_seq_conta	= conta.nr_sequencia
	and 	conta.nr_seq_segurado_conv = seg.nr_sequencia
	and 	ie_atend_ant_benef_pc	= 'S'
	union all
	select	'M'			ie_tipo_item,
		sel.nr_sequencia 	nr_seq_selecao,
		mat.dt_execucao_conv,
		seg.dt_contratacao,
		(select count(1)
		from	pls_segurado_carteira
		where	nr_seq_segurado = seg.nr_sequencia
		and 	cd_usuario_plano is not null) qt_benef_localizado,
		(select count(1)
		from	pls_segurado_repasse	r
		where	r.nr_seq_segurado	= conta.nr_seq_segurado_conv
		and	r.ie_tipo_repasse	= ie_tipo_repasse_pc
		and	conta.dt_atendimento_conv between trunc(nvl(r.dt_repasse,conta.dt_atendimento_conv)) and fim_dia(nvl(r.dt_fim_repasse,conta.dt_atendimento_conv))) qt_repasse
	from	pls_oc_cta_selecao_imp	sel,
		pls_conta_mat_imp 	mat,
		pls_conta_imp		conta,
		pls_segurado		seg
	where	sel.nr_id_transacao 	= nr_id_transacao_pc
	and	sel.ie_valido 		= 'S'
	and	conta.nr_sequencia   	= sel.nr_seq_conta
	and	mat.nr_seq_conta	= conta.nr_sequencia
	and 	conta.nr_seq_segurado_conv = seg.nr_sequencia
	and 	ie_atend_ant_benef_pc	= 'S'
	union all
	select	'C'			ie_tipo_item,
		sel.nr_sequencia 	nr_seq_selecao,
		null 			dt_execucao_conv,
		null			dt_contratacao,
		null			cd_usuario_plano,
		(select count(1)
		from	pls_segurado_repasse	r
		where	r.nr_seq_segurado	= conta.nr_seq_segurado_conv
		and	r.ie_tipo_repasse	= ie_tipo_repasse_pc
		and	conta.dt_atendimento_conv between trunc(nvl(r.dt_repasse,conta.dt_atendimento_conv)) and fim_dia(nvl(r.dt_fim_repasse,conta.dt_atendimento_conv))) qt_repasse
	from	pls_oc_cta_selecao_imp 	sel,
		pls_conta_imp		conta
	where	sel.nr_id_transacao 	= nr_id_transacao_pc
	and	sel.ie_valido 		= 'S'
	and	conta.nr_sequencia 	= sel.nr_seq_conta
	and	ie_benef_nao_ident_pc	= 'S'
	and 	conta.nr_seq_segurado_conv is null;

c02_w		c02%rowtype;

begin

if	(nr_seq_combinada_p is not null) then
	begin
	
	for	C01_w in C01(nr_seq_combinada_p) loop
		begin
		
		if	(c01_w.ie_atend_ant_benef = 'S' or c01_w.ie_benef_nao_ident = 'S') then
			begin
			
			-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
			pls_ocor_imp_pck.atualiza_campo_auxiliar (	'V', 'N', nr_id_transacao_p, null);
			
			nr_indice_w := 0;
			
			for	C02_w in C02(nr_id_transacao_p, c01_w.ie_benef_nao_ident, c01_w.ie_atend_ant_benef,c01_w.ie_tipo_repasse) loop
				begin
				
				ie_gerar_ocorrencia_w:= 'N';
				
				/*
					Se ie_tipo_item = 'C', ent�o quer dizer que n�o h� informa��o do nr_seq_segurado. Nesse caso, apenas dever� validar o benefici�rio 
					n�o identificado, caso esse campo estiver checado na valida��o.
				*/				
				if	(c02_w.ie_tipo_item = 'C') then
									
					tb_valido_w(nr_indice_w)      	:= 'S';
					tb_observacao_w(nr_indice_w)	:= 'Benefici�rio n�o informado ou n�o cadastrado.';
					ie_gerar_ocorrencia_w		:= 'S';
				
				elsif	(c02_w.qt_repasse > 0) then
				
					tb_valido_w(nr_indice_w)      	:= 'S';
					tb_observacao_w(nr_indice_w)	:= 'Benefici�rio em repasse.';
					ie_gerar_ocorrencia_w		:= 'S';
					
				else
					/*
						Se chegar aqui, quer dizer que existe informa��o de nr_seq_segurado na conta, ent�o validar� ainda se atendimento anterior a inclus�o do
						benefici�rio, caso o respectivo campo estiver marcado e tamb�m 
					*/
					if	(c02_w.dt_execucao_conv	< c02_w.dt_contratacao) then
						begin
						
						tb_valido_w(nr_indice_w)      := 'S';
						tb_observacao_w(nr_indice_w):= 'Data de realiza��o ' || c02_w.dt_execucao_conv || ' anterior a inclus�o do benefici�rio no plano ' || c02_w.dt_contratacao;
						ie_gerar_ocorrencia_w:= 'S';
						
						end;
					end if;
					
					if	(ie_gerar_ocorrencia_w = 'N') and ( c02_w.qt_benef_localizado = 0) then
						begin
						
						tb_valido_w(nr_indice_w)      := 'S';
						tb_observacao_w(nr_indice_w):= 'Benefici�rio n�o identificado';
						ie_gerar_ocorrencia_w:= 'S';
						
						end;
					end if;
				end if;
				
				if	(ie_gerar_ocorrencia_w = 'S') then
					begin
					
					tb_seq_selecao_w(nr_indice_w) := c02_w.nr_seq_selecao;
				
					--Verifica o n�mero de registros armazenados, caso tenha atingido limite grava os mesmos na tablela
					if	(nr_indice_w >= pls_util_pck.qt_registro_transacao_w) then
						--Grava o que restar nas vari�veis na tabela
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, 
												tb_valido_w,
												tb_observacao_w, 
												nr_id_transacao_p, 
												'SEQ');
						--limpa as vari�veis	
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
											tb_valido_w,
											tb_observacao_w);
						nr_indice_w := 0;
					else
						nr_indice_w := nr_indice_w + 1;
					end if;
					end;
				end if;
				
				end;
			end loop;
			
			if (tb_seq_selecao_w.count > 0) then
				--Grava o que restar nas vari�veis na tabela
				pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, 
										tb_valido_w,
										tb_observacao_w, 
										nr_id_transacao_p, 
										'SEQ');
			
			end if;
			
			end;
		end if;
		
		-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
		pls_ocor_imp_pck.atualiza_campo_valido ('V', 
							'N', 
							ie_regra_excecao_p, 
							null,
							nr_id_transacao_p, 
							null);
		
		end;
	end loop;
	
	end;
end if;


commit;

end pls_oc_cta_tratar_val_110_imp;
/