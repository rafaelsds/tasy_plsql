create or replace 
procedure libera_adm_ccg(	nr_seq_atend_glic_p	in number,
				nm_usuario_p		in varchar2) is 

nr_seq_glicemia_w	number(10);
ds_observacao_w		varchar2(255);
				
begin
if (nr_seq_atend_glic_p is not null) then
	update	atendimento_glicemia
	set	dt_lib_enfermagem	= sysdate
	where 	nr_sequencia		= nr_seq_atend_glic_p;
	
	commit;
	
	select	max(nr_sequencia),
		max(ds_observacao)
	into 	nr_seq_glicemia_w,
		ds_observacao_w
	from 	atendimento_glicemia
	where	nr_sequencia  = nr_seq_atend_glic_p;
	
	gerar_alteracao_CCG( nr_seq_glicemia_w, 12, ds_observacao_w, nm_usuario_p);

end if;
end libera_adm_ccg;
/