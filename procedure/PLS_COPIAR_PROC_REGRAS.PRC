create or replace
procedure pls_copiar_proc_regras
			(	nr_seq_regra_origem_p		Number,
				nr_seq_regra_destino_p		Number,
				ie_copiar_proc_origem_p		Number,
				ie_copiar_proc_destino_p	Number,
				nm_usuario_p			Varchar2) is 

ie_cobertura_w			Varchar2(1);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
cd_area_procedimento_w		Number(15);
cd_especialidade_w		Number(15);
cd_grupo_proc_w			Number(15);
ie_limitacao_w			Varchar2(1);
ie_liberado_w			Varchar2(1);
ie_tipo_guia_w			Varchar2(2);
cd_doenca_cid_w			Varchar2(10);
nr_seq_tipo_acomodacao_w	Number(10);

nr_seq_grupo_servico_w		Number(10);

Cursor C00 is
	select	ie_cobertura,
		cd_procedimento,
		ie_origem_proced,
		cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc,
		nr_seq_grupo_servico
	from	pls_cobertura_proc
	where	nr_seq_tipo_cobertura = nr_seq_regra_origem_p;

Cursor C01 is
	select	cd_procedimento,
		ie_origem_proced,
		cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc,
		ie_limitacao,
		cd_doenca_cid,
		ie_tipo_guia
	from	pls_limitacao_proc
	where	nr_seq_tipo_limitacao = nr_seq_regra_origem_p;

Cursor C02 is
	select	ie_tipo_guia,
		nr_seq_tipo_acomodacao,
		cd_procedimento,
		ie_origem_proced,
		cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc,
		cd_doenca_cid,
		ie_liberado,
		nr_seq_grupo_servico
	from	pls_carencia_proc
	where	nr_seq_tipo_carencia = nr_seq_regra_origem_p;

begin
if	(ie_copiar_proc_origem_p = 0) then
	open C00;
	loop
	fetch c00 into	
		ie_cobertura_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		nr_seq_grupo_servico_w;
	exit when C00%notfound;
		begin
		if	(ie_copiar_proc_destino_p = 0) then
			insert	into pls_cobertura_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_cobertura,
					ie_cobertura, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade, cd_grupo_proc,
					nr_seq_grupo_servico)
				values	(pls_cobertura_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_cobertura_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w,
					nr_seq_grupo_servico_w);
		elsif	(ie_copiar_proc_destino_p = 1) then
			insert	into pls_limitacao_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_limitacao,
					ie_limitacao, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade, cd_grupo_proc)
				values	(pls_limitacao_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_cobertura_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w);
		elsif	(ie_copiar_proc_destino_p = 2) then
			insert	into pls_carencia_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_carencia,
					ie_liberado, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade, cd_grupo_proc,
					nr_seq_grupo_servico)
				values	(pls_carencia_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_cobertura_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w,
					nr_seq_grupo_servico_w);
		end if;
		end;
	end loop;
	close c00;

elsif	(ie_copiar_proc_origem_p = 1) then
	open C01;
	loop
	fetch c01 into	
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		ie_limitacao_w,
		cd_doenca_cid_w,
		ie_tipo_guia_w;
	exit when C01%notfound;
		begin
		if	(ie_copiar_proc_destino_p = 0) then
			insert	into pls_cobertura_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_cobertura,
					ie_cobertura, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade,
					cd_grupo_proc)
				values	(pls_cobertura_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_limitacao_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w);
		elsif	(ie_copiar_proc_destino_p = 1) then
			insert	into pls_limitacao_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_limitacao,
					ie_limitacao, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade, cd_grupo_proc, 
					cd_doenca_cid, ie_tipo_guia)
				values	(pls_limitacao_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_limitacao_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w,
					cd_doenca_cid_w, ie_tipo_guia_w);
		elsif	(ie_copiar_proc_destino_p = 2) then
			insert	into pls_carencia_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_carencia,
					ie_liberado, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade, cd_grupo_proc,
					ie_tipo_guia)
				values	(pls_carencia_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_limitacao_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w,
					ie_tipo_guia_w);
		end if;
		end;
	end loop;
	close c01;

elsif	(ie_copiar_proc_origem_p = 2) then
	open C02;
	loop
	fetch c02 into	
		ie_tipo_guia_w,
		nr_seq_tipo_acomodacao_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		cd_doenca_cid_w,
		ie_liberado_w,
		nr_seq_grupo_servico_w;
	exit when C02%notfound;
		begin
		if	(ie_copiar_proc_destino_p = 0) then
			insert	into pls_cobertura_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_cobertura,
					ie_cobertura, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade, cd_grupo_proc,
					nr_seq_grupo_servico)
				values	(pls_cobertura_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_liberado_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w,
					nr_seq_grupo_servico_w);
		elsif	(ie_copiar_proc_destino_p = 1) then
			insert	into pls_limitacao_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_limitacao,
					ie_limitacao, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade, cd_grupo_proc, 
					cd_doenca_cid, ie_tipo_guia)
				values	(pls_limitacao_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_liberado_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w,
					cd_doenca_cid_w, ie_tipo_guia_w);
		elsif	(ie_copiar_proc_destino_p = 2) then
			insert	into pls_carencia_proc
					(nr_sequencia,	dt_atualizacao,	nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_carencia,
					ie_liberado, cd_procedimento, ie_origem_proced,
					cd_area_procedimento, cd_especialidade, cd_grupo_proc,
					ie_tipo_guia, nr_seq_tipo_acomodacao, nr_seq_grupo_servico)
				values	(pls_carencia_proc_seq.nextval, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, nr_seq_regra_destino_p,
					ie_liberado_w, cd_procedimento_w, ie_origem_proced_w,
					cd_area_procedimento_w, cd_especialidade_w, cd_grupo_proc_w,
					ie_tipo_guia_w, nr_seq_tipo_acomodacao_w, nr_seq_grupo_servico_w);
		end if;
		end;
	end loop;
	close c02;
end if;

commit;

end pls_copiar_proc_regras;
/