create or replace
procedure pls_obter_se_oco_med_inativo
			(	nr_seq_conta_p			number,
				nr_seq_proc_p			number,
				ie_gerar_ocorrencia_p	out	varchar2,
				ds_observacao_p		out	varchar2) is 

cd_medico_w		number(10);
ie_medico_inativo_w	varchar2(1) := 'N';
				
Cursor C01 is
	select	a.cd_medico
	from	pls_proc_participante a,
		pls_conta_proc	b	
	where	a.nr_seq_conta_proc	= b.nr_sequencia
	and	b.nr_seq_conta 		= nr_seq_conta_p
	and	((b.nr_sequencia = nr_seq_proc_p) or (nvl(nr_seq_proc_p,0) = 0))
	union
	select	a.cd_medico_executor
	from	pls_conta a,
		pls_conta_proc b
	where	a.nr_sequencia	= b.nr_seq_conta
	and	b.nr_seq_conta	= nr_seq_conta_p
	and	((b.nr_sequencia = nr_seq_proc_p) or (nvl(nr_seq_proc_p,0) = 0))
	order by 1;
					
begin

ie_gerar_ocorrencia_p := 'N';
ds_observacao_p := 'M�dico(s) inativo(s): '||chr(13)||chr(10);

open C01;
loop
fetch C01 into	
	cd_medico_w;
exit when C01%notfound;
	begin	
	
	if	(nvl(cd_medico_w,0) > 0) then
		
		/*Obter se o m�dico inativo*/
		select	decode(count(*),0,'S','N')
		into	ie_medico_inativo_w
		from	medico
		where	cd_pessoa_fisica = cd_medico_w
		and	nvl(ie_situacao,'I') = 'A';
		
		if	(ie_medico_inativo_w = 'S') then
			ds_observacao_p := ds_observacao_p||cd_medico_w||' - '||obter_nome_pf(cd_medico_w)||' '||chr(13)||chr(10);
			ie_gerar_ocorrencia_p := 'S';
		end if;
	end if;
	
	end;
end loop;
close C01;

if	(ie_gerar_ocorrencia_p = 'N') then
	ds_observacao_p := '';
end if;

end pls_obter_se_oco_med_inativo;
/