create or replace
procedure 	duplic_transcricao_prescr(	nr_sequencia_p  		number,
					nm_usuario_p		varchar,
					cd_estabelecimento_p	number) is


dt_anexo_w				date;
ds_arquivo_w  			varchar2(255);
nr_sequencia_w			number(10);
nr_seq_classificacao_w	transcricao_prescricao.nr_seq_classificacao%type;

Cursor 	C01 is
select 	dt_anexo, 
	ds_arquivo  
from 	transcricao_prescricao_arq 
where 	nr_seq_transcricao = nr_sequencia_p;

begin

select	transcricao_prescricao_seq.nextval 
into
	nr_sequencia_w
from 	dual;

select 	nr_seq_classificacao
into	nr_seq_classificacao_w
from 	transcricao_prescricao 
where 	nr_sequencia = nr_sequencia_p;

insert into transcricao_prescricao (	nr_sequencia,
				nr_seq_classificacao,
				dt_anexo,
				nm_usuario_nrec,
				dt_liberacao,
				nm_usuario_lib,
				cd_setor_atendimento,
				ds_observacao,
				cd_estabelecimento,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				cd_pessoa_fisica,
				ie_status,
				ds_arquivo,
				dt_transcricao,
				nm_usuario_transcricao)
			select 	nr_sequencia_w,			
				nr_seq_classificacao,
				sysdate,
				nm_usuario_p,
				null,
				null,
				cd_setor_atendimento,
				ds_observacao,
				cd_estabelecimento_p,
				nr_atendimento,
				sysdate,
				nm_usuario_p,
				sysdate,
				cd_pessoa_fisica,
				'S',
				ds_arquivo,
				null,
				null
			from	transcricao_prescricao
			where 	nr_sequencia = nr_sequencia_p;
			
open C01;
loop
fetch C01 into	
	dt_anexo_w,		
	ds_arquivo_w;  	
exit when C01%notfound;
	begin
	insert into transcricao_prescricao_arq(	nr_sequencia,
					dt_anexo,
					ds_arquivo,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_seq_transcricao)
	values				(transcricao_prescricao_arq_seq.nextval,
					dt_anexo_w,
					ds_arquivo_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_sequencia_w);
	end;			
end loop;
close C01;	
commit;

end	duplic_transcricao_prescr;
/