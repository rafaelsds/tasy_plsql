create or replace
procedure pls_glosa_fechar_conta
			(	nm_usuario_p		Varchar2) is 

begin

update	tiss_motivo_glosa
set	ie_fechar_conta	= 'N'
where	cd_motivo_tiss in ('1213', '1311', '1504', '1602', '1604', '9902', '9908', '9915', '9916', '1838');

commit;

end pls_glosa_fechar_conta;
/
