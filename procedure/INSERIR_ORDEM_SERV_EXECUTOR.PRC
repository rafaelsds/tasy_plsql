create or replace
procedure inserir_ordem_serv_executor(
			nr_seq_ordem_p		number,
			nm_usuario_p		varchar2,
			nm_usuario_exec_p		varchar2,
			nr_seq_funcao_p		number,
			nr_seq_tipo_exec_p	number,
			qt_min_prev_p		number) is 
begin
insert	into man_ordem_servico_exec (
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			nm_usuario_exec,
			nr_seq_funcao, 
			nr_seq_tipo_exec, 
			qt_min_prev)   
	values (
			man_ordem_servico_exec_seq.nextval,
			nr_seq_ordem_p,
			sysdate,
			nm_usuario_p,
			nm_usuario_exec_p,
			nr_seq_funcao_p, 
			nr_seq_tipo_exec_p,
			qt_min_prev_p);
	commit;
end inserir_ordem_serv_executor;
/