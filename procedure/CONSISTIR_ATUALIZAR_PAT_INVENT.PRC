create or replace
procedure consistir_atualizar_pat_invent(
		nr_seq_inventario_p	number,
		qt_registros_p out	number,
		nm_usuario_p		varchar2) is 

qt_registros_w	number(10,0);
begin
if	(nm_usuario_p is not null) then
	begin

	if	(nr_seq_inventario_p is not null) then
		begin
		consistir_pat_inventario(nr_seq_inventario_p, nm_usuario_p);
		end;
	end if;

	select	count(*)
	into	qt_registros_w
	from	pat_inventario_bem
	where	ds_consistencia is not null        
	and	nr_seq_inventario = nr_seq_inventario_p;

	end;
end if;
qt_registros_p	:= qt_registros_w;
commit;
end consistir_atualizar_pat_invent;
/