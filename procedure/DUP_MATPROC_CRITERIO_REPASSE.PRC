create or replace
procedure DUP_MATPROC_CRITERIO_REPASSE	(nr_sequencia_p	in	number,
					ie_opcao_p	in	varchar2,
					nm_usuario_p	in	varchar2) is


CD_REGRA_w			number(10,0);
CD_EDICAO_AMB_w		number(10,0);
CD_GRUPO_MATERIAL_w		number(10,0);
CD_SUBGRUPO_MATERIAL_w	number(10,0);
CD_CLASSE_MATERIAL_w		number(10,0);
CD_MATERIAL_w			number(10,0);
CD_CONVENIO_w			number(10,0);
CD_SETOR_ATENDIMENTO_w	number(10,0);
CD_MEDICO_w			varchar2(10);
IE_FORMA_CALCULO_w		varchar2(2);
ie_honorario_w		varchar2(1);
IE_PACOTE_w			varchar2(1);
TX_REPASSE_w			number(15,2);
VL_REPASSE_w			number(15,2);
CD_TABELA_PRECO_w		number(10,0);
CD_CONVENIO_CALC_w		number(10,0);
CD_CATEGORIA_CALC_w		number(10,0);
IE_TIPO_ATENDIMENTO_w	number(3,0);
IE_FUNCAO_W			number(3,0);
CD_AREA_PROCED_W		number(15);
CD_ESPECIAL_PROCED_W		number(15);
CD_GRUPO_PROCED_W		number(15);
CD_PROCEDIMENTO_W		number(15);
IE_ORIGEM_PROCED_W		number(10,0);
TX_MEDICO_W			number(15,2);
TX_ANESTESISTA_W		number(15,2);
TX_MATERIAIS_W			number(15,2);
TX_AUXILIARES_W			number(15,2);
TX_CUSTO_OPERACIONAL_W		number(15,2);
CD_EDICAO_AMB_CALC_W		number(10,0);
cd_prestador_w			Varchar2(14);
cd_categoria_w			Varchar2(10);
ie_regra_dia_w			varchar2(1);
ie_honorario_restricao_w	varchar2(1);
ie_situacao_w			varchar2(255);
nr_seq_grupo_rec_w		number(10,0);
ie_perc_pacote_w		varchar2(2);
IE_MED_EXEC_SOCIO_w		varchar2(1);
ie_prioridade_w			number(5);
cd_cgc_fornecedor_w		varchar2(14);
cd_cgc_prestador_w		varchar2(14);
CD_MED_EXEC_PROC_PRINC_w	varchar2(10);
cd_medico_aval_w		varchar2(10);
cd_municipio_ibge_w		varchar2(6);
cd_plano_w			varchar2(10);
CD_TAB_PRECO_CALC_w		number(10);
cd_tipo_pessoa_w		number(3);
ds_function_w			varchar2(255);
ds_observacao_w			varchar2(4000);
dt_vigencia_final_w		date;
dt_vigencia_inicial_w		date;
ie_atend_retorno_w		varchar2(1);
ie_carater_inter_sus_w		varchar2(2);
IE_MED_EXEC_PP_CORPO_CLI_w	varchar2(1);
IE_MEDICO_IND_CORPO_CLI_w	varchar2(1);
IE_MEDICO_IND_PROC_PRINC_w	varchar2(1);
IE_REGRA_MEDICO_w		varchar2(4);
IE_REPASSE_CALC_w		varchar2(1);
IE_REP_AUTO_w			varchar2(1);
IE_SEXO_w			varchar2(1);
IE_TIPO_CONVENIO_w		number(2);
NR_SEQ_CLASSIFICACAO_w		number(10);
NR_SEQ_LOTE_FORNEC_w		number(10);
cd_equipamento_w		number(10);
cd_especialidade_w		number(5);
cd_grupo_proc_aih_w		number(8);
cd_medico_laudo_w		varchar2(10);
cd_medico_prescr_w		varchar2(10);
cd_medico_req_w			varchar2(10);
cd_pessoa_func_w		varchar2(10);
cd_procedencia_w		number(5);
cd_registro_w			number(2);
cd_situacao_glosa_w		number(2);
cd_tipo_acomodacao_w		number(4);
cd_tipo_anestesia_w		varchar2(10);
cd_tipo_pessoa_prest_w		number(3);
cd_tipo_procedimento_w		number(3);
hr_final_w			date;
hr_inicial_w			date;
ie_carater_cirurgia_w		varchar2(15);
ie_clinica_w			number(5);
ie_cobra_pf_pj_w		varchar2(1);
ie_conveniado_w			varchar2(1);
IE_MED_PLANTONISTA_w		varchar2(1);
IE_PARTICIPOU_SUS_w		varchar2(1);
IE_PLANTAO_w			varchar2(1);
IE_TIPO_ATEND_CALC_w		number(3);
IE_TIPO_ATO_SUS_w		number(3);
IE_TIPO_SERVICO_SUS_w		number(3);
IE_VIA_ACESSO_w			varchar2(1);
NR_SEQ_CLASSIF_MEDICO_w		number(10);
NR_SEQ_ETAPA_CHECKUP_w		number(10);
NR_SEQ_EXAME_w			number(10);
NR_SEQ_FORMA_ORG_w		number(10);
NR_SEQ_GRUPO_w			number(10);
NR_SEQ_PROC_INTERNO_w		number(10);
NR_SEQ_REGRA_PRIOR_REPASSE_w	number(10);	
NR_SEQ_SUBGRUPO_w		number(10);
QT_DIA_FINAL_w			number(2);
QT_DIA_INICIAL_w		number(2);
QT_PORTE_ANESTESICO_w		number(2);
TX_PROCEDIMENTO_w		number(15,4);
TX_PROC_MAXIMA_w		number(15,4);
TX_PROC_MINIMA_w		number(15,4);
VL_LIMITE_w			number(15,2);
VL_MINIMO_w			number(15,2);

begin
if (upper(ie_opcao_p) = 'M') then
	select	CD_REGRA,
		CD_EDICAO_AMB,
		CD_GRUPO_MATERIAL,
		CD_SUBGRUPO_MATERIAL,
		CD_CLASSE_MATERIAL,
		CD_MATERIAL,
		CD_CONVENIO,
		CD_SETOR_ATENDIMENTO,
		CD_MEDICO,
		IE_FORMA_CALCULO,
		IE_PACOTE,
		TX_REPASSE,
		VL_REPASSE,
		CD_TABELA_PRECO,
		CD_CONVENIO_CALC,
		CD_CATEGORIA_CALC,
		IE_TIPO_ATENDIMENTO,
		ie_situacao,
		nr_seq_grupo_rec,
		ie_prioridade,
		cd_categoria,
		cd_cgc_fornecedor,
		cd_cgc_prestador,
		CD_MED_EXEC_PROC_PRINC,
		cd_medico_aval,
		cd_municipio_ibge,
		cd_plano,
		CD_TAB_PRECO_CALC,
		cd_tipo_pessoa,
		substr(ds_function,1,255),
		ds_observacao,
		dt_vigencia_final,
		dt_vigencia_inicial,
		ie_atend_retorno,
		ie_carater_inter_sus,
		IE_MED_EXEC_PP_CORPO_CLI,
		IE_MEDICO_IND_CORPO_CLI,
		IE_MEDICO_IND_PROC_PRINC,
		IE_PERC_PACOTE,
		IE_REGRA_MEDICO,
		IE_REPASSE_CALC,
		IE_REP_AUTO,
		IE_SEXO,
		IE_TIPO_CONVENIO,
		NR_SEQ_CLASSIFICACAO,
		NR_SEQ_LOTE_FORNEC		
	into	CD_REGRA_w,
		CD_EDICAO_AMB_w,
		CD_GRUPO_MATERIAL_w,
		CD_SUBGRUPO_MATERIAL_w,
		CD_CLASSE_MATERIAL_w,
		CD_MATERIAL_w,
		CD_CONVENIO_w,
		CD_SETOR_ATENDIMENTO_w,
		CD_MEDICO_w,
		IE_FORMA_CALCULO_w,
		IE_PACOTE_w,
		TX_REPASSE_w,
		VL_REPASSE_w,
		CD_TABELA_PRECO_w,
		CD_CONVENIO_CALC_w,
		CD_CATEGORIA_CALC_w,
		IE_TIPO_ATENDIMENTO_w,
		ie_situacao_w,
		nr_seq_grupo_rec_w,
		ie_prioridade_w,
		cd_categoria_w,
		cd_cgc_fornecedor_w,
		cd_cgc_prestador_w,
		CD_MED_EXEC_PROC_PRINC_w,
		cd_medico_aval_w,
		cd_municipio_ibge_w,
		cd_plano_w,
		CD_TAB_PRECO_CALC_w,
		cd_tipo_pessoa_w,
		ds_function_w,
		ds_observacao_w,
		dt_vigencia_final_w,
		dt_vigencia_inicial_w,
		ie_atend_retorno_w,
		ie_carater_inter_sus_w,
		IE_MED_EXEC_PP_CORPO_CLI_w,
		IE_MEDICO_IND_CORPO_CLI_w,
		IE_MEDICO_IND_PROC_PRINC_w,
		IE_PERC_PACOTE_w,
		IE_REGRA_MEDICO_w,
		IE_REPASSE_CALC_w,
		IE_REP_AUTO_w,
		IE_SEXO_w,
		IE_TIPO_CONVENIO_w,
		NR_SEQ_CLASSIFICACAO_w,
		NR_SEQ_LOTE_FORNEC_w
	from	mat_criterio_repasse
	where	nr_sequencia = nr_sequencia_p;

	insert into mat_criterio_repasse
		(NR_SEQUENCIA,
		CD_REGRA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		CD_EDICAO_AMB,
		CD_GRUPO_MATERIAL,
		CD_SUBGRUPO_MATERIAL,
		CD_CLASSE_MATERIAL,
		CD_MATERIAL,
		CD_CONVENIO,
		CD_SETOR_ATENDIMENTO,
		CD_MEDICO,
		IE_FORMA_CALCULO,
		IE_PACOTE,
		TX_REPASSE,
		VL_REPASSE,
		CD_TABELA_PRECO,
		CD_CONVENIO_CALC,
		CD_CATEGORIA_CALC,
		IE_TIPO_ATENDIMENTO,
		ie_situacao,
		nr_seq_grupo_rec,
		ie_prioridade,
		cd_categoria,
		cd_cgc_fornecedor,
		cd_cgc_prestador,
		CD_MED_EXEC_PROC_PRINC,
		cd_medico_aval,
		cd_municipio_ibge,
		cd_plano,
		CD_TAB_PRECO_CALC,
		cd_tipo_pessoa,
		ds_function,
		ds_observacao,
		dt_vigencia_final,
		dt_vigencia_inicial,
		ie_atend_retorno,
		ie_carater_inter_sus,
		IE_MED_EXEC_PP_CORPO_CLI,
		IE_MEDICO_IND_CORPO_CLI,
		IE_MEDICO_IND_PROC_PRINC,
		IE_PERC_PACOTE,
		IE_REGRA_MEDICO,
		IE_REPASSE_CALC,
		IE_REP_AUTO,
		IE_SEXO,
		IE_TIPO_CONVENIO,
		NR_SEQ_CLASSIFICACAO,
		NR_SEQ_LOTE_FORNEC)
	values	(mat_criterio_repasse_seq.nextval,
		cd_regra_w,
		sysdate,
		nm_usuario_p,
		CD_EDICAO_AMB_w,
		CD_GRUPO_MATERIAL_w,
		CD_SUBGRUPO_MATERIAL_w,
		CD_CLASSE_MATERIAL_w,
		CD_MATERIAL_w,
		CD_CONVENIO_w,
		CD_SETOR_ATENDIMENTO_w,
		CD_MEDICO_w,
		IE_FORMA_CALCULO_w,
		IE_PACOTE_w,
		TX_REPASSE_w,
		VL_REPASSE_w,
		CD_TABELA_PRECO_w,
		CD_CONVENIO_CALC_w,
		CD_CATEGORIA_CALC_w,
		IE_TIPO_ATENDIMENTO_w,
		ie_situacao_w,
		nr_seq_grupo_rec_w,
		ie_prioridade_w,
		cd_categoria_w,
		cd_cgc_fornecedor_w,
		cd_cgc_prestador_w,
		CD_MED_EXEC_PROC_PRINC_w,
		cd_medico_aval_w,
		cd_municipio_ibge_w,
		cd_plano_w,
		CD_TAB_PRECO_CALC_w,
		cd_tipo_pessoa_w,
		ds_function_w,
		ds_observacao_w,
		dt_vigencia_final_w,
		dt_vigencia_inicial_w,
		ie_atend_retorno_w,
		ie_carater_inter_sus_w,
		IE_MED_EXEC_PP_CORPO_CLI_w,
		IE_MEDICO_IND_CORPO_CLI_w,
		IE_MEDICO_IND_PROC_PRINC_w,
		IE_PERC_PACOTE_w,
		IE_REGRA_MEDICO_w,
		IE_REPASSE_CALC_w,
		IE_REP_AUTO_w,
		IE_SEXO_w,
		IE_TIPO_CONVENIO_w,
		NR_SEQ_CLASSIFICACAO_w,
		NR_SEQ_LOTE_FORNEC_w);
elsif	(upper(ie_opcao_p) = 'P') then
	select	IE_FUNCAO,
		CD_REGRA,
		CD_EDICAO_AMB,
		CD_AREA_PROCED,
		CD_ESPECIAL_PROCED,
		CD_GRUPO_PROCED,
		CD_PROCEDIMENTO,
		IE_ORIGEM_PROCED,
		CD_CONVENIO,
		CD_SETOR_ATENDIMENTO,
		CD_MEDICO,
		IE_FORMA_CALCULO,
		IE_PACOTE,
		TX_MEDICO,
		TX_ANESTESISTA,
		TX_MATERIAIS,
		TX_AUXILIARES,
		TX_CUSTO_OPERACIONAL,
		VL_REPASSE,
		CD_TABELA_PRECO,
		CD_CONVENIO_CALC,
		CD_CATEGORIA_CALC,
		IE_TIPO_ATENDIMENTO,
		CD_EDICAO_AMB_CALC,
		ie_honorario,
		cd_prestador,
		cd_categoria,
		ie_regra_dia,
		ie_honorario_restricao,
		ie_situacao,
		nvl(ie_perc_pacote,'N'),
		IE_MED_EXEC_SOCIO,
		ie_prioridade,
		cd_equipamento,
		cd_especialidade,
		cd_grupo_proc_aih,
		CD_MED_EXEC_PROC_PRINC,
		cd_medico_aval,
		cd_medico_laudo,
		cd_medico_prescr,
		cd_medico_req,
		cd_municipio_ibge,
		cd_pessoa_func,
		cd_plano,
		cd_procedencia,
		cd_registro,
		cd_situacao_glosa,
		cd_tipo_acomodacao,
		cd_tipo_anestesia,
		cd_tipo_pessoa,
		cd_tipo_pessoa_prest,
		cd_tipo_procedimento,
		substr(ds_function,1,255),
		ds_observacao,
		dt_vigencia_final,
		dt_vigencia_inicial,
		hr_final,
		hr_inicial,
		ie_atend_retorno,
		ie_carater_cirurgia,
		ie_carater_inter_sus,
		ie_clinica,
		ie_cobra_pf_pj,
		ie_conveniado,
		IE_MED_PLANTONISTA,
		IE_PARTICIPOU_SUS,
		IE_PLANTAO,
		IE_REGRA_MEDICO,
		IE_REPASSE_CALC,
		IE_REP_AUTO,
		IE_SEXO,
		IE_TIPO_ATEND_CALC,
		IE_TIPO_ATO_SUS,
		IE_TIPO_CONVENIO,
		IE_TIPO_SERVICO_SUS,
		IE_VIA_ACESSO,
		NR_SEQ_CLASSIFICACAO,
		NR_SEQ_CLASSIF_MEDICO,
		NR_SEQ_ETAPA_CHECKUP,
		NR_SEQ_EXAME,
		NR_SEQ_FORMA_ORG,
		NR_SEQ_GRUPO,
		NR_SEQ_PROC_INTERNO,
		NR_SEQ_REGRA_PRIOR_REPASSE,
		NR_SEQ_SUBGRUPO,
		QT_DIA_FINAL,
		QT_DIA_INICIAL,
		QT_PORTE_ANESTESICO,
		TX_PROCEDIMENTO,
		TX_PROC_MAXIMA,
		TX_PROC_MINIMA,
		VL_LIMITE,
		VL_MINIMO		
	into	IE_FUNCAO_W,
		CD_REGRA_W,
		CD_EDICAO_AMB_W,
		CD_AREA_PROCED_W,
		CD_ESPECIAL_PROCED_W,
		CD_GRUPO_PROCED_W,
		CD_PROCEDIMENTO_W,
		IE_ORIGEM_PROCED_W,
		CD_CONVENIO_W,
		CD_SETOR_ATENDIMENTO_W,
		CD_MEDICO_W,
		IE_FORMA_CALCULO_W,
		IE_PACOTE_W,
		TX_MEDICO_W,
		TX_ANESTESISTA_W,
		TX_MATERIAIS_W,
		TX_AUXILIARES_W,
		TX_CUSTO_OPERACIONAL_W,
		VL_REPASSE_W,
		CD_TABELA_PRECO_W,
		CD_CONVENIO_CALC_W,
		CD_CATEGORIA_CALC_W,
		IE_TIPO_ATENDIMENTO_W,
		CD_EDICAO_AMB_CALC_W,
		ie_honorario_w,
		cd_prestador_w,
		cd_categoria_w,
		ie_regra_dia_w,
		ie_honorario_restricao_w,
		ie_situacao_w,
		ie_perc_pacote_w,
		IE_MED_EXEC_SOCIO_w,
		ie_prioridade_w,
		cd_equipamento_w,
		cd_especialidade_w,
		cd_grupo_proc_aih_w,
		CD_MED_EXEC_PROC_PRINC_w,
		cd_medico_aval_w,
		cd_medico_laudo_w,
		cd_medico_prescr_w,
		cd_medico_req_w,
		cd_municipio_ibge_w,
		cd_pessoa_func_w,
		cd_plano_w,
		cd_procedencia_w,
		cd_registro_w,
		cd_situacao_glosa_w,
		cd_tipo_acomodacao_w,
		cd_tipo_anestesia_w,
		cd_tipo_pessoa_w,
		cd_tipo_pessoa_prest_w,
		cd_tipo_procedimento_w,
		ds_function_w,
		ds_observacao_w,
		dt_vigencia_final_w,
		dt_vigencia_inicial_w,
		hr_final_w,
		hr_inicial_w,
		ie_atend_retorno_w,
		ie_carater_cirurgia_w,
		ie_carater_inter_sus_w,
		ie_clinica_w,
		ie_cobra_pf_pj_w,
		ie_conveniado_w,
		IE_MED_PLANTONISTA_w,
		IE_PARTICIPOU_SUS_w,
		IE_PLANTAO_w,
		IE_REGRA_MEDICO_w,
		IE_REPASSE_CALC_w,
		IE_REP_AUTO_w,
		IE_SEXO_w,
		IE_TIPO_ATEND_CALC_w,
		IE_TIPO_ATO_SUS_w,
		IE_TIPO_CONVENIO_w,
		IE_TIPO_SERVICO_SUS_w,
		IE_VIA_ACESSO_w,
		NR_SEQ_CLASSIFICACAO_w,
		NR_SEQ_CLASSIF_MEDICO_w,
		NR_SEQ_ETAPA_CHECKUP_w,
		NR_SEQ_EXAME_w,
		NR_SEQ_FORMA_ORG_w,
		NR_SEQ_GRUPO_w,
		NR_SEQ_PROC_INTERNO_w,
		NR_SEQ_REGRA_PRIOR_REPASSE_w,
		NR_SEQ_SUBGRUPO_w,
		QT_DIA_FINAL_w,
		QT_DIA_INICIAL_w,
		QT_PORTE_ANESTESICO_w,
		TX_PROCEDIMENTO_w,
		TX_PROC_MAXIMA_w,
		TX_PROC_MINIMA_w,
		VL_LIMITE_w,
		VL_MINIMO_w
	from	proc_criterio_repasse
	where	nr_sequencia = nr_sequencia_p;

	insert into proc_criterio_repasse
		(NR_SEQUENCIA,
		IE_FUNCAO,
		CD_REGRA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		CD_EDICAO_AMB,
		CD_AREA_PROCED,
		CD_ESPECIAL_PROCED,
		CD_GRUPO_PROCED,
		CD_PROCEDIMENTO,
		IE_ORIGEM_PROCED,
		CD_CONVENIO,
		CD_SETOR_ATENDIMENTO,
		CD_MEDICO,
		IE_FORMA_CALCULO,
		IE_PACOTE,
		TX_MEDICO,
		TX_ANESTESISTA,
		TX_MATERIAIS,
		TX_AUXILIARES,
		TX_CUSTO_OPERACIONAL,
		VL_REPASSE,
		CD_TABELA_PRECO,
		CD_CONVENIO_CALC,
		CD_CATEGORIA_CALC,
		IE_TIPO_ATENDIMENTO,
		CD_EDICAO_AMB_CALC,
		ie_honorario,
		cd_prestador,
		cd_categoria,
		ie_regra_dia,
		ie_honorario_restricao,
		ie_situacao,
		ie_perc_pacote,
		IE_MED_EXEC_SOCIO,
		ie_prioridade,
		cd_equipamento,
		cd_especialidade,
		cd_grupo_proc_aih,
		CD_MED_EXEC_PROC_PRINC,
		cd_medico_aval,
		cd_medico_laudo,
		cd_medico_prescr,
		cd_medico_req,
		cd_municipio_ibge,
		cd_pessoa_func,
		cd_plano,
		cd_procedencia,
		cd_registro,
		cd_situacao_glosa,
		cd_tipo_acomodacao,
		cd_tipo_anestesia,
		cd_tipo_pessoa,
		cd_tipo_pessoa_prest,
		cd_tipo_procedimento,
		ds_function,
		ds_observacao,
		dt_vigencia_final,
		dt_vigencia_inicial,
		hr_final,
		hr_inicial,
		ie_atend_retorno,
		ie_carater_cirurgia,
		ie_carater_inter_sus,
		ie_clinica,
		ie_cobra_pf_pj,
		ie_conveniado,
		IE_MED_PLANTONISTA,
		IE_PARTICIPOU_SUS,
		IE_PLANTAO,
		IE_REGRA_MEDICO,
		IE_REPASSE_CALC,
		IE_REP_AUTO,
		IE_SEXO,
		IE_TIPO_ATEND_CALC,
		IE_TIPO_ATO_SUS,
		IE_TIPO_CONVENIO,
		IE_TIPO_SERVICO_SUS,
		IE_VIA_ACESSO,
		NR_SEQ_CLASSIFICACAO,
		NR_SEQ_CLASSIF_MEDICO,
		NR_SEQ_ETAPA_CHECKUP,
		NR_SEQ_EXAME,
		NR_SEQ_FORMA_ORG,
		NR_SEQ_GRUPO,
		NR_SEQ_PROC_INTERNO,
		NR_SEQ_REGRA_PRIOR_REPASSE,
		NR_SEQ_SUBGRUPO,
		QT_DIA_FINAL,
		QT_DIA_INICIAL,
		QT_PORTE_ANESTESICO,
		TX_PROCEDIMENTO,
		TX_PROC_MAXIMA,
		TX_PROC_MINIMA,
		VL_LIMITE,
		VL_MINIMO)
	values	(proc_criterio_repasse_seq.nextval,
		IE_FUNCAO_w,
		CD_REGRA_w,
		sysdate,
		NM_USUARIO_p,
		CD_EDICAO_AMB_w,
		CD_AREA_PROCED_w,
		CD_ESPECIAL_PROCED_w,
		CD_GRUPO_PROCED_w,
		CD_PROCEDIMENTO_w,
		IE_ORIGEM_PROCED_w,
		CD_CONVENIO_w,
		CD_SETOR_ATENDIMENTO_w,
		CD_MEDICO_w,
		IE_FORMA_CALCULO_w,
		IE_PACOTE_w,
		TX_MEDICO_w,
		TX_ANESTESISTA_w,
		TX_MATERIAIS_w,
		TX_AUXILIARES_w,
		TX_CUSTO_OPERACIONAL_w,
		VL_REPASSE_w,
		CD_TABELA_PRECO_w,
		CD_CONVENIO_CALC_w,
		CD_CATEGORIA_CALC_w,
		IE_TIPO_ATENDIMENTO_w,
		CD_EDICAO_AMB_CALC_W,
		ie_honorario_w,
		cd_prestador_w,
		cd_categoria_w,
		ie_regra_dia_w,
		ie_honorario_restricao_w,
		ie_situacao_w,
		ie_perc_pacote_w,
		IE_MED_EXEC_SOCIO_w,
		ie_prioridade_w,
		cd_equipamento_w,
		cd_especialidade_w,
		cd_grupo_proc_aih_w,
		CD_MED_EXEC_PROC_PRINC_w,
		cd_medico_aval_w,
		cd_medico_laudo_w,
		cd_medico_prescr_w,
		cd_medico_req_w,
		cd_municipio_ibge_w,
		cd_pessoa_func_w,
		cd_plano_w,
		cd_procedencia_w,
		cd_registro_w,
		cd_situacao_glosa_w,
		cd_tipo_acomodacao_w,
		cd_tipo_anestesia_w,
		cd_tipo_pessoa_w,
		cd_tipo_pessoa_prest_w,
		cd_tipo_procedimento_w,
		ds_function_w,
		ds_observacao_w,
		dt_vigencia_final_w,
		dt_vigencia_inicial_w,
		hr_final_w,
		hr_inicial_w,
		ie_atend_retorno_w,
		ie_carater_cirurgia_w,
		ie_carater_inter_sus_w,
		ie_clinica_w,
		ie_cobra_pf_pj_w,
		ie_conveniado_w,
		IE_MED_PLANTONISTA_w,
		IE_PARTICIPOU_SUS_w,
		IE_PLANTAO_w,
		IE_REGRA_MEDICO_w,
		IE_REPASSE_CALC_w,
		IE_REP_AUTO_w,
		IE_SEXO_w,
		IE_TIPO_ATEND_CALC_w,
		IE_TIPO_ATO_SUS_w,
		IE_TIPO_CONVENIO_w,
		IE_TIPO_SERVICO_SUS_w,
		IE_VIA_ACESSO_w,
		NR_SEQ_CLASSIFICACAO_w,
		NR_SEQ_CLASSIF_MEDICO_w,
		NR_SEQ_ETAPA_CHECKUP_w,
		NR_SEQ_EXAME_w,
		NR_SEQ_FORMA_ORG_w,
		NR_SEQ_GRUPO_w,
		NR_SEQ_PROC_INTERNO_w,
		NR_SEQ_REGRA_PRIOR_REPASSE_w,
		NR_SEQ_SUBGRUPO_w,
		QT_DIA_FINAL_w,
		QT_DIA_INICIAL_w,
		QT_PORTE_ANESTESICO_w,
		TX_PROCEDIMENTO_w,
		TX_PROC_MAXIMA_w,
		TX_PROC_MINIMA_w,
		VL_LIMITE_w,
		VL_MINIMO_w);
end if;

commit;

end DUP_MATPROC_CRITERIO_REPASSE;
/