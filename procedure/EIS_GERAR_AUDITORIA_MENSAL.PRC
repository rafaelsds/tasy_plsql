create or replace
procedure EIS_Gerar_Auditoria_Mensal
		(dt_inicio_mes_p	date,
		dt_final_mes_p		date,
		cd_estabelecimento_P	number,
		nm_usuario_p		varchar2) is

dt_atual_w	date;

begin

dt_atual_w	:= dt_inicio_mes_p;

while	(dt_atual_w <= dt_final_mes_p)loop
	begin

	if	(dt_atual_w <= trunc(sysdate,'dd')) then
		gerar_eis_auditoria(dt_atual_w, cd_estabelecimento_p, nm_usuario_p);
	end if;

	dt_atual_w	:= dt_atual_w + 1;

	end;
end loop;

end EIS_Gerar_Auditoria_Mensal;
/