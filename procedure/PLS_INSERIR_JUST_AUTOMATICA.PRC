create or replace
procedure pls_inserir_just_automatica
			(	nr_seq_requisicao_p	Number,
				nr_seq_guia_p		Number,
				nr_seq_regra_just_aut_p	Number,
				nr_seq_item_aud_p	Number,
				nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir o hist�rico de justificativa nas requisi��es e guias para que o prestador possa vosualizar no portal web.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [X] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Avisar ao Eder sempre que for alterado ou incluido algum par�metro.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_proc_regra_w			Number(10);
nr_seq_auditoria_w		Number(10);
ie_orig_proc_regra_w		Number(10);
cd_grupo_proc_regra_w		Number(15);
cd_espec_regra_w		Number(15);
cd_area_proc_regra_w		Number(15);
cd_area_aud_w			Number(15);
cd_especialidade_aud_w		Number(15);
cd_grupo_aud_w			Number(15);
ie_origem_proced_2_w		Number(10);
cd_proc_aud_w			Number(10);
ie_orig_proc_aud_w		Number(10);
ds_mensagem_w			Varchar2(4000);
qt_ocorrencia_w			Number(10);

begin

select	cd_procedimento,
	ie_origem_proced,
	nr_seq_auditoria
into	cd_proc_aud_w,
	ie_orig_proc_aud_w,
	nr_seq_auditoria_w
from	pls_auditoria_item
where	nr_sequencia	= nr_seq_item_aud_p;

select	cd_procedimento,
	ie_origem_proced,
	cd_grupo_proc,
	cd_especialidade,
	cd_area_procedimento,
	substr(ds_mensagem,1,4000)
into	cd_proc_regra_w,
	ie_orig_proc_regra_w,
	cd_grupo_proc_regra_w,
	cd_espec_regra_w,
	cd_area_proc_regra_w,
	ds_mensagem_w
from	pls_regra_just_automatica
where	nr_sequencia	= nr_seq_regra_just_aut_p;

pls_obter_estrut_proc(	cd_proc_aud_w, ie_orig_proc_aud_w, cd_area_aud_w,
			cd_especialidade_aud_w, cd_grupo_aud_w, ie_origem_proced_2_w);

update	pls_auditoria_item
set	ie_status					= 'J',
	dt_atualizacao					= sysdate,
	nm_usuario					= nm_usuario_p
where	nr_sequencia					= nr_seq_item_aud_p
and	nvl(cd_area_proc_regra_w,cd_area_aud_w)		= cd_area_aud_w
and	nvl(cd_espec_regra_w,cd_especialidade_aud_w)	= cd_especialidade_aud_w
and	nvl(cd_grupo_proc_regra_w,cd_grupo_aud_w)	= cd_grupo_aud_w
and	nvl(cd_proc_regra_w,cd_proc_aud_w)		= cd_proc_aud_w
and	nvl(ie_orig_proc_regra_w,ie_origem_proced_2_w)	= ie_origem_proced_2_w;
	
update	pls_auditoria
set	ie_status		= 'AJ',
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_auditoria_w;

if	(nvl(nr_seq_requisicao_p,0)	<> 0) then
	insert	into pls_requisicao_historico
		(nr_sequencia, nr_seq_requisicao, ie_tipo_historico,
		 ds_historico, dt_historico, ie_origem_historico,
		 dt_atualizacao, nm_usuario)
	values	(pls_requisicao_historico_seq.NextVal, nr_seq_requisicao_p, 'J',
		 ds_mensagem_w, sysdate, 'A',
		 sysdate, nm_usuario_p);
elsif	(nvl(nr_seq_guia_p,0)	<> 0) then
	insert	into pls_guia_plano_historico
		(nr_sequencia, nr_seq_guia, dt_historico,
		 dt_atualizacao, nm_usuario, ds_observacao,
		 ie_origem_historico, ie_tipo_historico)
	values	(pls_guia_plano_historico_seq.NextVal, nr_seq_guia_p, sysdate,
		 sysdate, nm_usuario_p, ds_mensagem_w,
		 'A', 'J');
end if;

commit;

end pls_inserir_just_automatica;
/
