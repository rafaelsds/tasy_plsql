create or replace
procedure inserir_log_simpro(	nm_arquivo_p	varchar2,
			nm_usuario_p	varchar2,
			dt_vigencia_p	date) is 

begin

if 	(nm_arquivo_p is not null) then
	begin
	insert into simpro_log_carga (nr_sequencia, nm_arquivo, dt_atualizacao, nm_usuario, dt_vigencia)
	values(simpro_log_carga_seq.nextval, nm_arquivo_p, sysdate, nm_usuario_p, dt_vigencia_p);
	commit;
	end;
end if;

end inserir_log_simpro;
/