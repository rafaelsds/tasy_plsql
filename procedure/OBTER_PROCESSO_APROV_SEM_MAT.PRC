create or replace
procedure obter_processo_aprov_sem_mat(
				cd_centro_custo_p			number,
				cd_setor_atendimento_p		number,
				cd_local_Estoque_p		number,
				ie_tipo_processo_p			varchar2,
				ie_urgente_p			varchar2,
				cd_estabelecimento_p		number,
				cd_perfil_ativo_p			number,
				nr_seq_proj_rec_p			number,
				nr_documento_p			number,
				cd_processo_Aprov_p	out	number) is

cd_Processo_Aprov_w		number(10,0);
ie_tipo_requisicao_w		varchar2(3);

/*ATEN��O: Esta procedure obriga que a regra possua centro de custo*/

cursor c01 is
	select	a.cd_processo_aprov
	from	Processo_Aprov_Estrut a,
		processo_aprovacao b
	where	a.cd_processo_aprov 					= b.cd_processo_aprov
	and	b.ie_situacao <> 'I'
	and	a.cd_centro_custo is not null
	and	a.cd_centro_custo = cd_centro_custo_p
	and	((a.cd_perfil is null) or (cd_perfil_ativo_p is null) or (a.cd_perfil = cd_perfil_ativo_p))
	and	nvl(a.cd_estabelecimento, cd_estabelecimento_p)			= cd_estabelecimento_p
	and	nvl(b.cd_estabelecimento, cd_estabelecimento_p)			= cd_estabelecimento_p
	and	nvl(a.cd_setor_atendimento,nvl(cd_setor_atendimento_p,0))	= nvl(cd_setor_atendimento_p,0)
	and	nvl(a.cd_local_estoque,nvl(cd_local_estoque_p,0))		= nvl(cd_local_Estoque_p,nvl(a.cd_local_estoque,0))
	and	nvl(a.nr_seq_proj_rec,nvl(nr_seq_proj_rec_p,0))			= nvl(nr_seq_proj_rec_p,0)
	and	((ie_urgente = 'A') or (ie_urgente = ie_urgente_p))
	and	((nvl(a.vl_compra_mes, 0) = 0) or (nvl(a.vl_compra_mes, 0) < Obter_vl_compra_mes_proc_aprov(a.nr_sequencia, a.cd_processo_aprov)))
	and	a.cd_processo_aprov in(
			select	cd_processo_aprov
			from	processo_aprov_resp
			where	ie_solicitacao_compra 	= 'S'
			and	ie_tipo_processo_p		= 'S'	/*Solicita��o*/
			union
			select	cd_processo_aprov
			from	processo_aprov_resp
			where	ie_solicitacao_pagto 	= 'S'
			and	ie_tipo_processo_p		= 'P' /*Solicita��o pagamento*/
			union
			select	cd_processo_aprov
			from	processo_aprov_resp
			where	ie_ordem_compra		= 'S'
			and	ie_tipo_processo_p		= 'O' /*Ordem compra*/
			union
			select	cd_processo_aprov
			from	processo_aprov_resp
			where	ie_ordem_compra_transf	= 'S'
			and	ie_tipo_processo_p		= 'T' /*Ordem transferencia*/
			union
			select	cd_processo_aprov
			from	processo_aprov_resp
			where	nvl(ie_cotacao,'N')		= 'S'
			and	ie_tipo_processo_p		= 'C' /*Cota��o*/
			union
			select	cd_processo_aprov
			from	processo_aprov_resp
			where	nvl(ie_nota_fiscal,'N')		= 'S'
			and	ie_tipo_processo_p		= 'N' /*Nota fiscal*/
			union
			select	x.cd_processo_aprov
			from	processo_aprov_resp x
			where	x.ie_requisicao		= 'S'
			and	ie_tipo_processo_p		= 'R'
			and	((ie_tipo_requisicao_w	<> '2') or
				((ie_tipo_requisicao_w	= '2') and (nvl(x.ie_desconsidera_req_transf,'N') = 'N'))))
	order by	nvl(cd_material,0),
		nvl(cd_classe_material,0),
		nvl(cd_subgrupo_material,0),
		nvl(cd_grupo_material,0),
		nvl(cd_local_estoque,0),
		nvl(cd_centro_custo,0),
		nvl(a.cd_setor_atendimento,0),
		nvl(a.cd_perfil,0);

begin

if	(ie_tipo_processo_p = 'R') and
	(nr_documento_p > 0) then
	
	select	nvl(max(ie_tipo_requisicao),0)
	into	ie_tipo_requisicao_w
	from	operacao_estoque a,
		requisicao_material b
	where	a.cd_operacao_estoque = b.cd_operacao_estoque
	and	nr_requisicao = nr_documento_p;
end if;

open C01;
loop
	fetch C01 into
		cd_processo_aprov_w;
	exit when c01%notfound;
		cd_processo_aprov_p	:= cd_processo_aprov_w;
end loop;
close C01;

end obter_processo_aprov_sem_mat;
/

