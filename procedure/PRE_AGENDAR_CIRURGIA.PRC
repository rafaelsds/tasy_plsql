create or replace 
procedure pre_agendar_cirurgia(	cd_agenda_p		in number,
				hr_inicio_p		in date,
				cd_pessoa_fisica_p	in varchar2,
				cd_medico_p		in varchar2,
				nr_seq_proc_interno_p	in number,
				cd_convenio_p		in number,
				nm_usuario_p		in varchar2,
				cd_estabelecimento_p	in number,
				ds_orientacao_p		in varchar2,
				ie_lado_p		in varchar2,
				qt_tempo_proc_p		in number,
				ds_erro_p		out varchar2,
				nr_seq_agenda_p		out number,
				nm_paciente_p		varchar2,
				ie_carater_p		varchar2,
				ds_erro_2_p		out varchar2,            
				nr_seq_atend_futuro_p	number default null,
				nr_atendimento_p	number default 0,
				nr_seq_motivo_prazo_p	number default null,
				ds_motivo_prazo_p		varchar2 default null,
          				cd_categoria_p         	varchar2 default null,
            				cd_plano_p                     	varchar2 default null) is

nm_paciente_w			varchar2(60);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
nr_seq_agenda_w			number(10,0);
qt_min_cirugia_w			number(10,0):= null;
qt_idade_paciente_w		number(3);
cd_tipo_anestesia_w		varchar2(2);
ie_reserva_leito_w			varchar2(3);
dt_ultima_consulta_w		date;
cd_usuario_convenio_w		varchar2(30);
qt_dias_atend_w			number(5,0);	
qt_min_higienizacao_w		number(15,0);
nr_seq_classif_agenda_w		number(10,0);
nr_telefone_w			varchar2(40);
nr_celular_w			varchar2(40);
nm_pessoa_contato_w		varchar2(50);
ie_autorizacao_w			varchar2(3);
dt_nascimento_w			date;
ds_erro_w			varchar2(255);
ds_erro_2_w			varchar2(255);
ie_gerar_servico_w			varchar2(1);
qt_classif_agenda_w		number(2);


BEGIN
Obter_Param_Usuario(870, 59, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_gerar_servico_w);

select	obter_valor_param_usuario(870, 3, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),
	obter_compl_pf(cd_pessoa_fisica_p, 1, 'T'),
	obter_dados_pf(cd_pessoa_fisica_p,'TC'),
	substr(obter_desc_usuario(nm_usuario_p),1,40),
	nvl(max(to_number(obter_valor_param_usuario(870,2,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p))) , 1)
into	nr_seq_classif_agenda_w,
	nr_telefone_w,
	nr_celular_w,
	nm_pessoa_contato_w,
	qt_dias_atend_w
from	dual;

if	(nr_celular_w <> null) then
	nr_telefone_w := nr_telefone_w || ' ' || nr_celular_w;
end if;

select	nvl(max(nr_sequencia), 0)
into	nr_seq_agenda_w
from	agenda_paciente
where	cd_agenda	= cd_agenda_p
and	hr_inicio		= hr_inicio_p
and 	ie_status_agenda	= 'L';

nr_seq_agenda_p	:= nr_seq_agenda_w;

if	(qt_tempo_proc_p is null) or (qt_tempo_proc_p = 0) then
	begin
	select	nvl(max(qt_min_cirugia),0)
	into	qt_min_cirugia_w
	from	proc_interno_tempo
	where	nr_seq_proc_interno		= nr_seq_proc_interno_p
	and	nvl(cd_medico,nvl(cd_medico_p,'X')) = nvl(cd_medico_p,'X')
	and	nvl(cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p;

	if	(qt_min_cirugia_w = 0) then
		select	nvl(qt_min_cirurgia,0)
		into	qt_min_cirugia_w
		from	proc_interno
		where	nr_sequencia		= nr_seq_proc_interno_p;
	end if;
	end;
else
	qt_min_cirugia_w := nvl(qt_tempo_proc_p,0);
end if; 

select	nvl(max(qt_min_higienizacao),0)
into	qt_min_higienizacao_w
from	proc_interno
where	nr_sequencia			= nr_seq_proc_interno_p;

select	ie_reserva_leito,
	cd_tipo_anestesia
into	ie_reserva_leito_w,
	cd_tipo_anestesia_w
from	proc_interno
where	nr_sequencia			= nr_seq_proc_interno_p;

obter_param_usuario(870, 9, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_autorizacao_w);

begin						
select	dt_ultima_consulta,
	cd_usuario_convenio
into	dt_ultima_consulta_w,
	cd_usuario_convenio_w
from	Med_Cliente
where	cd_pessoa_fisica		= cd_pessoa_fisica_p
and	trunc(dt_ultima_consulta,'dd')	>= trunc(sysdate-qt_dias_atend_w,'dd')
and	cd_convenio			= cd_convenio_p;
exception
	when others then
	cd_usuario_convenio_w	:= null;
end;

if	(cd_usuario_convenio_w is null) then
	begin
	select	max(cd_usuario_convenio)
	into	cd_usuario_convenio_w	
	from    atendimento_paciente a,
		atend_categoria_convenio b
	where   a.nr_atendimento	= b.nr_atendimento
	and     a.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and     b.cd_convenio		= cd_convenio_p
	and     dt_entrada >=		sysdate - qt_dias_atend_w;
	exception
		when others then
		cd_usuario_convenio_w	:= null;
	end;
end if;

if	(nr_seq_agenda_w <> 0) then
	begin

	select	max(substr(obter_nome_pf(cd_pessoa_fisica),1,60)),
		max(obter_idade(dt_nascimento, sysdate, 'A')),
		max(dt_nascimento)
	into	nm_paciente_w,
		qt_idade_paciente_w,
		dt_nascimento_w	
	from	pessoa_fisica
	where	cd_pessoa_fisica		=	cd_pessoa_fisica_p;

	Obter_Proc_Tab_Inter_Agenda(nr_seq_proc_interno_p,null,cd_convenio_p, null, null, cd_estabelecimento_p, sysdate, null, null, null, null, null, null, null, null,
			cd_procedimento_w,ie_origem_proced_w);
	
	if (nvl(nr_seq_classif_agenda_w,0) > 0) then
		select	count(*)
		into	qt_classif_agenda_w
		from	agenda_paciente_classif
		where	nr_sequencia in (nr_seq_classif_agenda_w);	
	end if;
	
	if (qt_classif_agenda_w = 0) then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(241216);
	end if;
			
	update	agenda_paciente
	set	cd_pessoa_fisica	=	cd_pessoa_fisica_p,
		nm_paciente		=	nvl(nm_paciente_w,nm_paciente_p),
		cd_medico		=	cd_medico_p,
		nr_seq_proc_interno	=	nr_seq_proc_interno_p,
		cd_procedimento		=	cd_procedimento_w,
		ie_origem_proced	=	ie_origem_proced_w,
		cd_convenio		=	cd_convenio_p,
		cd_agenda		=	cd_agenda_p,
		nm_usuario		=	nm_usuario_p,
		ie_status_agenda	=	'PA',
		nr_minuto_duracao	=	nvl(qt_min_cirugia_w,0) + nvl(qt_min_higienizacao_w,0),
		ie_reserva_leito	=	ie_reserva_leito_w,
		qt_idade_paciente	=	qt_idade_paciente_w,
		dt_agendamento		=	sysdate,
		nm_usuario_orig		=	nm_usuario_p,
		cd_tipo_anestesia	=	cd_tipo_anestesia_w,
		cd_usuario_convenio	=	cd_usuario_convenio_w,
		ie_autorizacao		=	ie_autorizacao_w,
		nr_seq_classif_agenda	=	nr_seq_classif_agenda_w,
		nr_telefone		=	nr_telefone_w,
		nm_pessoa_contato	=	nm_pessoa_contato_w,
		ie_lado			=	ie_lado_p,
		dt_nascimento_pac	=	dt_nascimento_w,
		ie_carater_cirurgia	=	ie_carater_p,
		nr_seq_atend_futuro	=	nr_seq_atend_futuro_p,
		cd_doenca_cid		=	decode(nr_atendimento_p,0,cd_doenca_cid,Obter_Cid_Atendimento(nr_atendimento_p,'P')),
		nr_seq_motivo_prazo	=	nr_seq_motivo_prazo_p,
		ds_motivo_prazo		=	substr(ds_motivo_prazo_p,1,255),
      		cd_categoria   		= 	cd_categoria_p,
      		cd_plano        		= 	cd_plano_p
	where	nr_sequencia		=	nr_seq_agenda_w;

	gerar_autor_regra(null,null,null,null,null,null,'AP',nm_usuario_p,nr_seq_agenda_w,nr_seq_proc_interno_p,null,null,null,null,'','','');

	gerar_dados_pre_agenda(cd_procedimento_w, ie_origem_proced_w, nr_seq_agenda_w, nr_seq_proc_interno_p, cd_medico_p, cd_pessoa_fisica_p, nm_usuario_p, cd_convenio_p, null, ds_orientacao_p, cd_estabelecimento_p, 'S', 'S', 'S', ie_gerar_servico_w, 'S', ds_erro_w,ds_erro_2_w);
	ds_erro_2_p 		:= ds_erro_2_w;
	end;
else
	ds_erro_p		:= wheb_mensagem_pck.get_texto(279147);
end if;

commit;

END pre_agendar_cirurgia;
/