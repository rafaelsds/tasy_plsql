create or replace
procedure fis_gerar_reg_0100_icmsipi(nr_seq_controle_p	number) is 

/*REGISTRO 0100: DADOS DO CONTRIBUINTE SUBSTITUTO OU RESPONS�VEL PELO ICMS DESTINO*/

-- VARIABLES
ie_gerou_dados_bloco_w varchar2(1) := 'N';

nr_seq_icmsipi_0100_w fis_efd_icmsipi_0100.nr_sequencia%type;

qt_cursor_w number(10) := 0;
nr_vetor_w  number(10) := 0;

-- USUARIO
nm_usuario_w usuario.nm_usuario%type;

/*Cursor que retorna as informa��es para o registro 0100 restringindo pela sequencia da nota fiscal*/  
cursor c_reg_0100 is
select	'0100' cd_reg,
	substr(obter_nome_pf(d.cd_contabilista),1,60) ds_nome,
	substr(obter_dados_pf(d.cd_contabilista, 'CPF'),1,11) cd_cpf,
	substr(lpad(nvl(d.nr_crc,0),11,0),1,11) cd_crc,	
	null cd_cnpj,
	substr(elimina_caractere_especial(obter_compl_pf(d.cd_contabilista,2,'CEP')),1,255) cd_cep,
	substr(obter_compl_pf(d.cd_contabilista,2,'EN'),1,255)ds_end,
	substr(obter_compl_pf(d.cd_contabilista,2,'NR'),1,255)nr_num,
	substr(obter_compl_pf(d.cd_contabilista,2,'CO'),1,255)ds_compl,
	substr(obter_compl_pf(d.cd_contabilista,2,'B'),1,255)ds_bairro,
	substr(elimina_caracteres_telefone(obter_compl_pf(d.cd_contabilista,2,'DDT') || obter_compl_pf(d.cd_contabilista,2,'T')),1,10) nr_fone,
	substr(elimina_caracteres_telefone(obter_compl_pf(d.cd_contabilista,2,'DDF') || obter_compl_pf(d.cd_contabilista,2,'FAX')),1,10) nr_fax,
	substr(nvl(obter_dados_pf_pj_estab(a.cd_estabelecimento,null,b.cd_cgc,'M'), 
	obter_dados_pf_pj_estab(a.cd_estabelecimento,d.cd_contabilista,null,'M')),1,255) ds_email,
	substr((obter_compl_pf(d.cd_contabilista, 1, 'CDMDV')),1,7) cd_mun
from	fis_efd_icmsipi_controle a,
	estabelecimento b,
	pessoa_juridica c,
	empresa d
where	a.nr_sequencia = nr_seq_controle_p
and 	b.cd_estabelecimento = a.cd_estabelecimento
and 	b.cd_cgc = c.cd_cgc
and	b.cd_empresa = d.cd_empresa
and	c.ie_situacao	= 'A';	
	
	
/*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_0100*/
type reg_c_reg_0100 is table of c_reg_0100%RowType;
vetReg0100 reg_c_reg_0100;

/*Cria��o do array com o tipo sendo da tabela eespecificada - FIS_EFD_ICMSIPI_0100 */
type registro is table of fis_efd_icmsipi_0100%rowtype index by pls_integer;
fis_registros_w registro;

begin
/*Obte��o do usu�rio ativo no tasy*/
nm_usuario_w := Obter_Usuario_Ativo;

open c_reg_0100;
loop
fetch c_reg_0100 bulk collect      into vetReg0100 limit 1000;
	for i in 1 .. vetReg0100.Count loop    
	begin

	/*Incrementa a variavel para o array*/
	qt_cursor_w:=	qt_cursor_w + 1;

	if (ie_gerou_dados_bloco_w = 'N') then
	  ie_gerou_dados_bloco_w := 'S';
	end if;

	/*Busca da sequencia da tabela especificada - fis_efd_icmsipi_0100 */
	select fis_efd_icmsipi_0100_seq.nextval
	  into nr_seq_icmsipi_0100_w
	  from dual;

	/*Inserindo valores no array para realiza��o do forall posteriormente*/      
	fis_registros_w(qt_cursor_w).nr_sequencia 		:= nr_seq_icmsipi_0100_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao 		:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario 		:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).dt_atualizacao_nrec	:= sysdate;
	fis_registros_w(qt_cursor_w).nm_usuario_nrec 		:= nm_usuario_w;
	fis_registros_w(qt_cursor_w).cd_reg 			:= '0100';	
	fis_registros_w(qt_cursor_w).ds_nome   			:= vetReg0100(i).ds_nome;
	fis_registros_w(qt_cursor_w).cd_cpf    			:= vetReg0100(i).cd_cpf;
	fis_registros_w(qt_cursor_w).cd_crc    			:= vetReg0100(i).  cd_crc;
	fis_registros_w(qt_cursor_w).cd_cnpj   			:= vetReg0100(i).cd_cnpj;
	fis_registros_w(qt_cursor_w).cd_cep    			:= vetReg0100(i).cd_cep;
	fis_registros_w(qt_cursor_w).ds_end    			:= vetReg0100(i).ds_end;
	fis_registros_w(qt_cursor_w).nr_num    			:= vetReg0100(i).nr_num;
	fis_registros_w(qt_cursor_w).ds_compl  			:= vetReg0100(i).ds_compl;
	fis_registros_w(qt_cursor_w).ds_bairro 			:= vetReg0100(i).ds_bairro;
	fis_registros_w(qt_cursor_w).nr_fone   			:= vetReg0100(i).nr_fone;
	fis_registros_w(qt_cursor_w).nr_fax    			:= vetReg0100(i).nr_fax;
	fis_registros_w(qt_cursor_w).ds_email  			:= vetReg0100(i).ds_email;
	fis_registros_w(qt_cursor_w).cd_mun   			:= vetReg0100(i). cd_mun;
	fis_registros_w(qt_cursor_w).nr_seq_controle 		:= nr_seq_controle_p;

	if (nr_vetor_w >= 1000) then
	begin
	/*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_0100 */
	forall i in fis_registros_w.first .. fis_registros_w.last
		insert into fis_efd_icmsipi_0100 values fis_registros_w (i);

		nr_vetor_w := 0;
		fis_registros_w.delete;

		commit;

	end;
	end if;

	/*incrementa variavel para realizar o forall quando chegar no valor limite*/
	nr_vetor_w := nr_vetor_w + 1;

	end;
	end loop;
exit when c_reg_0100%notfound;
end loop;
close c_reg_0100;

if (fis_registros_w.count > 0) then
begin
/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
forall i in fis_registros_w.first .. fis_registros_w.last
	insert into fis_efd_icmsipi_0100 values fis_registros_w(i);

	fis_registros_w.delete;

	commit;

end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

/*Atualiza��o informa��o no controle de gera��o de registro para SIM*/
/*if (ie_gerou_dados_bloco_w = 'S') then
	update fis_efd_icmsipi_controle set ie_mov_C = 'S' where nr_sequencia = nr_seq_controle_p;
end if;*/

end fis_gerar_reg_0100_icmsipi;
/