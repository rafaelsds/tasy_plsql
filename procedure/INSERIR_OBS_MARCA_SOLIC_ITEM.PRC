create or replace
procedure inserir_obs_marca_solic_item(	nr_solic_compra_p	number) is

nr_item_solic_compra_w		number(5);
cd_material_w			number(6);
nr_seq_marca_w			number(10);
ds_observacao_w			varchar2(255);

cursor c01 is
select	nr_item_solic_compra,
	cd_material,
	nr_seq_marca
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p;

begin

open c01;
loop
fetch c01 into	
	nr_item_solic_compra_w,
	cd_material_w,
	nr_seq_marca_w;
exit when c01%notfound;
	begin

	select	max(substr(nvl(ds_observacao,''),1,255))
	into	ds_observacao_w
	from	material_marca
	where	nr_sequencia = nr_seq_marca_w
	and	cd_material = cd_material_w;

	update	solic_compra_item
	set	ds_observacao = substr(nvl(ds_observacao,'') || chr(13) || ds_observacao_w,1,255)
	where	nr_item_solic_compra = nr_item_solic_compra_w
	and	nr_solic_compra = nr_solic_compra_p;

	end;
end loop;
close c01;

commit;

end inserir_obs_marca_solic_item;
/
