create or replace procedure atualizarDataEnvioLoteAlvaro(nr_prescricao_p number) is
begin
update lab_lote_externo l
set    l.dt_envio = sysdate
where  l.nr_sequencia in (select distinct pp.nr_seq_lote_externo
                          from   lab_lote_alvaro_v b, prescr_procedimento pp, lab_exame_equip lee, equipamento_lab c, material_exame_lab d
                          where  b.nr_prescricao = nr_prescricao_p
                          and    (((select nvl(max(a.ie_utiliza_cod_equip), 'N')
                                    from   empresa_integr_dados a
                                    where  a.nr_seq_empresa_integr = 12
                                    and    a.ie_situacao = 'A') = 'S' and b.cd_exame_equip is not null) or
                                 (select nvl(max(a.ie_utiliza_cod_equip), 'N')
                                  from   empresa_integr_dados a
                                  where  a.nr_seq_empresa_integr = 12
                                  and    a.ie_situacao = 'A') = 'N')
                          and    pp.nr_seq_exame      = lee.nr_seq_exame
                          and    lee.cd_equipamento   = c.cd_equipamento
                          and    pp.cd_material_exame = d.cd_material_exame
                          and    pp.nr_prescricao      = b.nr_prescricao
                          and    pp.nr_sequencia      = b.nr_sequencia
                          and    (lee.nr_seq_material = d.nr_sequencia or lee.nr_seq_material is null)
                          and    c.ds_sigla = 'ALVARO');

commit;

end atualizarDataEnvioLoteAlvaro;
/