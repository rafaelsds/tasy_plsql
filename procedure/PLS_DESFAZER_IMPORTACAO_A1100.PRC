create or replace
procedure pls_desfazer_importacao_A1100	(nr_seq_cabecalho_p	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Excluir o lote de importa��o A1100
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [   ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_dados_pedido_w		ptu_dados_pedido_a1100.nr_sequencia%type;
nr_seq_itens_pedido_w		ptu_itens_pedido_a1100.nr_sequencia%type;
nr_seq_trailer_w		ptu_trailer_a1100.nr_sequencia%type;
qt_autorizacoes_w		number(10)	:= 0;

Cursor C01 is
	select	nr_sequencia
	from	ptu_dados_pedido_a1100
	where	nr_seq_cabecalho = nr_seq_cabecalho_p;
	
Cursor C02 is
	select	nr_sequencia
	from	ptu_itens_pedido_a1100
	where	nr_seq_dados_pedido = nr_seq_dados_pedido_w;

Cursor C03 is
	select	nr_sequencia
	from	ptu_trailer_a1100
	where	nr_seq_cabecalho = nr_seq_cabecalho_p;

Cursor C04 is
	select	nr_sequencia
	from	ptu_hash_a1100
	where	nr_seq_cabecalho = nr_seq_cabecalho_p;

begin

if (nvl(nr_seq_cabecalho_p, 0) > 0) then
	select	count(1)
	into	qt_autorizacoes_w
	from	ptu_dados_pedido_a1100
	where	nr_seq_cabecalho = nr_seq_cabecalho_p
	and	(nr_seq_pedido_autor is not null
	or	nr_seq_pedido_compl is not null);

	if (qt_autorizacoes_w = 0) then	
		for r_C01_w in C01 loop
			begin
				nr_seq_dados_pedido_w	:= r_C01_w.nr_sequencia;				
				for r_C02_w in C02 loop
					begin
						delete	from ptu_itens_pedido_a1100
						where	nr_sequencia = r_C02_w.nr_sequencia;
					end;
				end loop;
				
				delete	from ptu_dados_pedido_a1100
				where	nr_sequencia = nr_seq_dados_pedido_w;
			end;
		end loop;
		for r_C03_w in C03 loop
			begin
				delete	from ptu_trailer_a1100
				where	nr_sequencia = r_C03_w.nr_sequencia;
			end;
		end loop;
		for r_C04_w in C04 loop
			begin
				delete	from ptu_hash_a1100
				where	nr_sequencia = r_C04_w.nr_sequencia;
			end;
		end loop;
		
		delete	from ptu_cabecalho_a1100
		where	nr_sequencia = nr_seq_cabecalho_p;		
	end if;
end if;

commit;

end pls_desfazer_importacao_A1100;
/
