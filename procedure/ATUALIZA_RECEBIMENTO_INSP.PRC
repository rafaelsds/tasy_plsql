create or replace
procedure atualiza_recebimento_insp(
			nr_sequencia_p number,
			ds_observacao_p varchar2) is 

begin

if (nr_sequencia_p is not null) then
	begin
	update  inspecao_recebimento
	set     ds_observacao = ds_observacao_p
	where   nr_sequencia  = nr_sequencia_p;
	commit;
	end;	
end if;
end atualiza_recebimento_insp;
/