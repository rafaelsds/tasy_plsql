create or replace
procedure envia_comunic_lib_pag(	nm_usuario_p		varchar2,
				cd_estabelecimento_p	varchar2) is 

			
			
			
nr_seq_regra_w				number(10);
cd_perfil_w				varchar2(10);
ds_setor_adicional_w			varchar2(4000) := '';
cd_setor_regra_usuario_w			number(5);
nm_usuario_destino_w			varchar2(255);
qt_existe_w				number(10);
nr_sequencia_w				number(20);
nr_seq_classif_w				number(10);
ds_titulo_w				varchar2(4000);
ds_comunicado_w				varchar2(4000);
vl_titulo_w				number(15,2);


cursor c01 is
select	b.nr_sequencia,
	b.cd_perfil
from	fin_regra_envio_comunic a,
	fin_regra_comunic_evento b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_funcao = 857
and	b.cd_evento = 3
and	b.ie_situacao = 'A'
and	a.cd_estabelecimento = cd_estabelecimento_p;			
			
Cursor c02 is
select	nvl(a.cd_setor_atendimento,0) cd_setor_atendimento
from	fin_regra_comunic_usu a
where	a.nr_seq_evento = nr_seq_regra_w;
			
begin

select	count(*)
into	qt_existe_w
from	fin_regra_envio_comunic a,
	fin_regra_comunic_evento b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_funcao = 857
and	b.cd_evento = 3
and	b.ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p;

if 	(qt_existe_w > 0) then
	open c01;
	loop
	fetch c01 into	
		nr_seq_regra_w,
		cd_perfil_w;
	exit when C01%notfound;
		begin
		
		open c02;
		loop
		fetch c02 into	
			cd_setor_regra_usuario_w;
		exit when c02%notfound;
			begin
			if	(cd_setor_regra_usuario_w <> 0) and
				(obter_se_contido_char(cd_setor_regra_usuario_w, ds_setor_adicional_w) = 'N') then
				ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_regra_usuario_w || ',',1,2000);
			end if;
		end;
		end loop;
		close c02;		
		
		select	obter_classif_comunic('F')
		into	nr_seq_classif_w
		from	dual;		
		
		select	max(substr(ds_titulo,1,80)),
			max(substr(ds_comunicacao,1,3000)) ds_comunicacao
		into	ds_titulo_w,
			ds_comunicado_w
		from	fin_regra_comunic_evento
		where	nr_sequencia = nr_seq_regra_w;
		
		SELECT	max(nm_usuarios_adic)
		INTO	nm_usuario_destino_w
		FROM	fin_regra_comunic_evento a
		WHERE	nr_sequencia = nr_seq_regra_w
		AND	a.ie_situacao = 'A';			

		select	comunic_interna_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		nm_usuario_destino_w := nm_usuario_destino_w;
		
		INSERT INTO comunic_interna(
					dt_comunicado,		
					ds_titulo,			
					ds_comunicado,
					nm_usuario,			
					dt_atualizacao,			
					ie_geral,
					nm_usuario_destino,		
					nr_sequencia,			
					ie_gerencial,
					nr_seq_classif,			
					dt_liberacao,			
					ds_perfil_adicional,
					ds_setor_adicional)
				VALUES(	SYSDATE,				
					ds_titulo_w,			
					ds_comunicado_w,
					nm_usuario_p,			
					SYSDATE,				
					'N',
					nm_usuario_destino_w,		
					nr_sequencia_w,			
					'N',
					nr_seq_classif_w,			
					SYSDATE,		
					cd_perfil_w,
					ds_setor_adicional_w);
		commit;
		
		end;
	end loop;
	close c01;
end if;

end envia_comunic_lib_pag;
/