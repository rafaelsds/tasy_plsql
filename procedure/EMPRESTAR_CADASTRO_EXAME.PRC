create or replace 
procedure emprestar_cadastro_exame(	nr_sequencia_p		    number,
					cd_medico_origem_p	    varchar2,
					cd_medico_destino_p	    varchar2,
					ds_mensagem_p		out varchar2,
					nm_usuario_p		    varchar2) is

ds_grupo_exame_w	varchar2(50);
ds_mensagem_w		varchar2(255);
					
BEGIN

if	(nr_sequencia_p is not null)then

	select	ds_grupo_exame
	into	ds_grupo_exame_w
	from	med_grupo_exame
	where	nr_sequencia = nr_sequencia_p;
		
	Copia_exame_padrao(cd_medico_origem_p, cd_medico_destino_p, nr_sequencia_p, nm_usuario_p);
	
	ds_mensagem_w	:= substr(Wheb_mensagem_pck.get_texto(334317,'DS_GRUPO_EXAME=' || ds_grupo_exame_w),1,255);
	
end if;

ds_mensagem_p	:= ds_mensagem_w;

END emprestar_cadastro_exame;
/