CREATE OR REPLACE
PROCEDURE ATUALIZAR_TAXA_ESPECIAL
                         (nr_interno_conta_p		number,
                          nm_usuario_p			varchar2) IS

/* atualiza taxa de percentual sobre matmed e/ou procedimentos */
/* Deve existir um servico vinculado a uma regra de taxa especial no ajuste de procedimento */

cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
cd_estabelecimento_w		number(4);
ie_tipo_atendimento_w		number(3);
qt_reg_regra_w			number(10);
cd_procedimento_w			number(15);
ie_origem_proced_w		number(10);
nr_seq_regra_preco_w		number(10);
nr_sequencia_w			number(15);

Cursor c01 is
select cd_procedimento,
	 ie_origem_proced,
	 nr_seq_regra_preco
from	 regra_ajuste_proc
where	 cd_estabelecimento			= cd_estabelecimento_w
and	 cd_convenio 				= cd_convenio_w
and	 nvl(cd_categoria,cd_categoria_w)	= cd_categoria_w
and	 nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)
							= ie_tipo_atendimento_w
and	 nr_seq_regra_preco			is not null
and	 ie_situacao				= 'A';



BEGIN

/* Verifica se existe alguma regra de pre�o especial composta para o Hospital */

select	count(*)
into	qt_reg_regra_w
from	regra_preco_proc;

if	(qt_reg_regra_w	> 0)	then
	BEGIN
	/* Busca dados da conta */
	select nvl(a.cd_convenio_calculo,a.cd_convenio_parametro),
		 nvl(a.cd_categoria_calculo,a.cd_categoria_parametro),
		 a.cd_estabelecimento,
		 b.ie_tipo_atendimento
	into	 cd_convenio_w,
		 cd_categoria_w,
		 cd_estabelecimento_w,
		 ie_tipo_atendimento_w
	from	 atendimento_paciente b,
		 conta_paciente a
	where	 a.nr_interno_conta	= nr_interno_conta_p
	and	 a.nr_atendimento		= b.nr_atendimento;

	/* Verifica se existe alguma regra de pre�o especial composta para o Convenio */
	OPEN C01;
	LOOP
	FETCH C01 into
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_regra_preco_w;
	exit 	when c01%notfound;
		begin
		/* Seleciona servico gerado pela regra de lancamento automatico */
		/* Se quiser taxa atualizada durante atendimento usar regra atendimento sen�o regra de alta */
		select nvl(max(a.nr_sequencia),0)
		into	 nr_sequencia_w
		from	 procedimento_paciente a
		where	 a.nr_interno_conta	= nr_interno_conta_p
		and	 a.cd_procedimento	= cd_procedimento_w
		and	 a.ie_origem_proced	= ie_origem_proced_w
		and	 a.cd_motivo_exc_conta	is null;

		/* Zera valor atual para n�o entrar no calculo */
		update procedimento_paciente
		set	 vl_procedimento 	= 0
		where	 nr_sequencia 	= nr_sequencia_w;

		if	(nr_sequencia_w	> 0) then
			atualiza_preco_servico(nr_sequencia_w,nm_usuario_p);
			commit;
		end if;
		end;
	END LOOP;
	CLOSE C01;
	END;	
END IF;

END ATUALIZAR_TAXA_ESPECIAL;
/
