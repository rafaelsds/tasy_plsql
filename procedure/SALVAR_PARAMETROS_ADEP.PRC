create or replace
procedure salvar_parametros_adep	(nm_usuario_p		varchar2,
					nm_componente_p	varchar2,
					vl_parametro_p	varchar2) is

nr_seq_param_w	number(10,0);

begin
if	(nm_usuario_p is not null) and
	(nm_componente_p is not null) and
	(vl_parametro_p is not null) then
	/* obter sequencia */
	select	parametro_adep_seq.nextval
	into	nr_seq_param_w
	from	dual;

	/* salvar parametro */
	insert into parametro_adep	(
					nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nm_componente,
					vl_parametro
					)
				values	(
					nr_seq_param_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nm_componente_p,
					vl_parametro_p
					);
end if;

commit;

end salvar_parametros_adep;
/