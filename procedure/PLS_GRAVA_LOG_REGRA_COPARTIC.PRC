create or replace
procedure pls_grava_log_regra_copartic(	nr_seq_regra_copartic_p		pls_regra_coparticipacao.nr_sequencia%type,
					nm_campo_p			pls_log_regra_copartic.nm_campo%type,
					vl_anterior_p			pls_log_regra_copartic.vl_anterior%type,
					vl_atual_p			pls_log_regra_copartic.vl_atual%type,
					ie_tipo_log_p			varchar2) is

ds_log_w	pls_log_regra_copartic.ds_log%type;
nm_usuario_w	pls_log_regra_copartic.nm_usuario%type;			

begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

if	(ie_tipo_log_p = 'A') then
	ds_log_w := 	'Altera��o de regra.' || pls_util_pck.enter_w ||
			'Usu�rio: ' || nm_usuario_w || '.' || pls_util_pck.enter_w ||
			'Campo: ' || nm_campo_p || '.' || pls_util_pck.enter_w ||
			'Valor anterior: ' || vl_anterior_p || '.'|| pls_util_pck.enter_w ||
			'Valor atual: ' || vl_atual_p || '.'|| pls_util_pck.enter_w ||
			'Data de altera��o:' || to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || '.';
else
	ds_log_w := 	'Exclus�o de regra.' || pls_util_pck.enter_w ||
			'Usu�rio: ' || nm_usuario_w || '.' || pls_util_pck.enter_w ||
			'Data de exclus�o:' || to_char(sysdate, 'dd/mm/yyyy hh:mi:ss') || '.';
end if;

insert into	pls_log_regra_copartic
	(	ds_log, dt_alteracao, dt_atualizacao,
		dt_atualizacao_nrec, nm_campo, nm_usuario,
		nm_usuario_nrec, nr_seq_regra_copartic, nr_sequencia,
		vl_anterior, vl_atual)
values	(	ds_log_w, sysdate, sysdate,
		sysdate, nm_campo_p, nm_usuario_w,
		nm_usuario_w, nr_seq_regra_copartic_p, pls_log_regra_copartic_seq.nextval,
		vl_anterior_p, vl_atual_p);
end pls_grava_log_regra_copartic;
/