create or replace
procedure atualizar_dose_onc_protocolo( nr_seq_paciente_p   	number,
					nr_seq_material_p	number default null,
					nr_seq_solucao_p	number default null) is

qt_mg_carboplatina_w		number(15,4); 
qt_peso_w			number(10,3); 
qt_superficie_corporea_w	number(15,4);
ie_calcula_preenchido_w		varchar2(3);
nr_seq_atendimento_w		number(10);
ie_atualizar_ciclos_w		varchar2(10);
pr_reducao_w			number(10);
nr_seq_solucao_w		number(10);
ie_aplica_reducao_w		varchar2(1) := 'S';
nr_seq_material_w		number(10);
pr_reducao_item_w		number(3);
qt_hora_aplicacao_w		number(15,4);
qt_creatinina_w			number(5,2);
qt_auc_w			number(4,1);
qt_altura_cm_w			number(3);

Cursor C01 is
	select	nr_seq_atendimento
	from	paciente_atendimento
	where	nr_seq_paciente	= nr_seq_paciente_p
	and	dt_liberacao_reg is null
	and	nr_prescricao is null;
	
cursor C02 is
	select	nr_seq_solucao
	from	paciente_protocolo_soluc
	where	nr_seq_paciente	= nr_seq_paciente_p;	
	
Cursor c03 is
	select	nr_seq_material,
		pr_reducao
	from	PACIENTE_PROTOCOLO_MEDIC a
	where	nr_seq_paciente	= nr_seq_paciente_p
	and	nvl(ie_aplica_reducao,'S') = 'S'
	and	nvl(PR_REDUCAO,100)	<> 100
	and	nvl(PR_REDUCAO,100)	<> 0
	and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
	and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p));
	
cursor c04 is
	select	nr_seq_material
	from	paciente_protocolo_medic
	where	nr_seq_paciente	= nr_seq_paciente_p
	and	ie_regra_diluicao_cad_mat = 'S';	

Cursor C05 is
	select	a.nr_seq_material,
			nvl(a.qt_hora_aplicacao,1)
	from	paciente_protocolo_medic a,
			unidade_medida b
	where	a.nr_seq_paciente	= nr_seq_paciente_p
	and		b.cd_unidade_medida = a.cd_unidade_medida
	and		b.ie_mult_h_aplic	= 'S'
	and		a.qt_hora_aplicacao is not null
	and		a.qt_hora_aplicacao	<> 0
	and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
	and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p));
	

begin

Obter_Param_Usuario(281,476,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo,ie_calcula_preenchido_w);
Obter_Param_Usuario(281,550,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo,ie_atualizar_ciclos_w);
select	round(max(obter_superficie_corp_red_ped(qt_peso, qt_altura, qt_redutor_sc, cd_pessoa_fisica, nm_usuario,IE_FORMULA_SUP_CORPOREA)),obter_numero_casas_sc),
	max(b.qt_peso),
	max(pr_reducao),
	max(qt_creatinina),
	max(qt_auc),
	max(qt_altura)
into	qt_superficie_corporea_w,
		qt_peso_w,
		pr_reducao_w,
		qt_creatinina_w,
		qt_auc_w,
		qt_altura_cm_w
from 	paciente_setor b  
where	b.nr_seq_paciente = nr_seq_paciente_p;

/* atualizar todos os medicamentos com dose padrao */

/* atualizar os medicamentos com base na superficie corporal */
update 	paciente_protocolo_medic a  
set 	a.cd_unid_med_prescr =  
               (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida),
	a.qt_dose_prescr =  a.qt_dose * qt_superficie_corporea_w
where 	a.nr_seq_paciente = nr_seq_paciente_p
and exists     (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida  
                  and b.cd_unidade_medida <> b.cd_unidade_med_princ  
                  and lower(b.cd_unidade_med_sec) = 'm2')
and	(nvl(ie_calcula_preenchido_w,'N') <> 'N' or (a.cd_unid_med_prescr is null and a.qt_dose_prescr is null))
and	a.qt_dose > 0
and	qt_superficie_corporea_w is not null
and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p));

qt_mg_carboplatina_w	:= obter_qt_mg_carboplatina(nr_seq_paciente_p);

/* atualizar os medicamentos com base na carboplatina */
update paciente_protocolo_medic a  
set 	a.qt_dose_prescr =   
               (select a.qt_dose * nvl(qt_mg_carboplatina_w,1)
                from paciente_setor b  
                where a.nr_seq_paciente = b.nr_seq_paciente),
               a.cd_unid_med_prescr =  
               (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida)  
where a.nr_seq_paciente = nr_seq_paciente_p
  and exists    (select b.cd_unidade_med_princ  
                from unidade_medida b
                where a.cd_unidade_medida = b.cd_unidade_medida
                  and b.cd_unidade_medida <> b.cd_unidade_med_princ
                  and lower(b.cd_unidade_med_sec) = 'mgc')
and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p))
and	nvl(ie_calcula_preenchido_w,'N') <> 'N';

update paciente_protocolo_medic a  
set 	a.qt_dose_prescr =   nvl(qt_mg_carboplatina_w,1),
               a.cd_unid_med_prescr =  
               (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida)  
where a.nr_seq_paciente = nr_seq_paciente_p
  and exists    (select b.cd_unidade_med_princ  
                from unidade_medida b
                where a.cd_unidade_medida = b.cd_unidade_medida
                  and b.cd_unidade_medida <> b.cd_unidade_med_princ
                  and lower(b.cd_unidade_med_sec) = obter_desc_expressao(782176)/*'mgcar'*/)
and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p))
and	nvl(ie_calcula_preenchido_w,'N') <> 'N';

if	(nvl(ie_calcula_preenchido_w,'N') = 'C') then

	update	paciente_protocolo_medic a
	set 	a.qt_dose_prescr	= nvl(Obter_dose_convertida(a.cd_material,a.qt_dose,a.cd_unidade_medida,a.cd_unid_med_prescr),a.qt_dose_prescr)
	where	a.nr_seq_paciente 	= nr_seq_paciente_p
	and		a.qt_dose > 0
	and		a.cd_unidade_medida is not null
	and		a.cd_unid_med_prescr is not null
	and not exists     (select b.cd_unidade_med_princ  
					from unidade_medida b  
					where a.cd_unidade_medida = b.cd_unidade_medida  
					  and b.cd_unidade_medida <> b.cd_unidade_med_princ  
					  and lower(b.cd_unidade_med_sec) in ('m2','mgc','kg',obter_desc_expressao(782176)/*'mgcar'*/))
	and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
	and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p));
	
else

	update 	paciente_protocolo_medic a  
	set 	a.cd_unid_med_prescr 	= (	select	c.cd_unidade_med_princ  
						from	unidade_medida c  
						where	a.cd_unidade_medida = c.cd_unidade_medida),
		a.qt_dose_prescr	= a.qt_dose
	where 	a.nr_seq_paciente 	= nr_seq_paciente_p
	and exists     (select b.cd_unidade_med_princ  
					from unidade_medida b  
					where a.cd_unidade_medida = b.cd_unidade_medida  
					and b.cd_unidade_medida <> b.cd_unidade_med_princ  
					and b.cd_unidade_med_sec = a.cd_unidade_medida)
	and	(nvl(ie_calcula_preenchido_w,'N') <> 'N' or (a.cd_unid_med_prescr is null and a.qt_dose_prescr is null))
	and	a.qt_dose > 0
	and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
	and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p));


	update 	paciente_protocolo_medic a  
	set 	a.cd_unid_med_prescr 	= a.cd_unidade_medida,
		a.qt_dose_prescr	= a.qt_dose
	where 	a.nr_seq_paciente 	= nr_seq_paciente_p
	and not exists     (select b.cd_unidade_med_princ  
					from unidade_medida b  
					where a.cd_unidade_medida = b.cd_unidade_medida  
					  and b.cd_unidade_medida <> b.cd_unidade_med_princ  
					  and lower(b.cd_unidade_med_sec) in ('m2','mgc','kg',obter_desc_expressao(782176)/*'mgcar'*/))
	and	(nvl(ie_calcula_preenchido_w,'N') <> 'N' or (a.cd_unid_med_prescr is null and a.qt_dose_prescr is null))
	and	a.qt_dose > 0
	and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
	and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p));
	
end if;

if	(pr_reducao_w	is not null) and
	(pr_reducao_w	> 0) and
	(ie_aplica_reducao_w = 'S')then
	update	paciente_protocolo_medic
	set	qt_dose_prescr	= (qt_dose_prescr - ((pr_reducao_w * nvl(qt_dose_prescr,0)) / 100))
	where	nr_seq_paciente 	= nr_seq_paciente_p
	and	nvl(ie_aplica_reducao,'S') = 'S'
	and	((nr_seq_material_p is null) or (nr_seq_material	= nr_seq_material_p))
	and	((nr_seq_solucao_p is null) or (nr_seq_solucao = nr_seq_solucao_p));	
end if;

open C05;
loop
fetch C05 into	
	nr_seq_material_w,
	QT_HORA_APLICACAO_w;
exit when C05%notfound;
	begin
	
	update	paciente_protocolo_medic
	set		qt_dose_prescr = qt_dose_prescr	* qt_hora_aplicacao_w
	where	nr_seq_paciente	= nr_seq_paciente_p
	and	nr_seq_material = nr_seq_material_w;

	end;
end loop;
close C05;

open C03;
loop
fetch C03 into	
	nr_seq_material_w,
	pr_reducao_item_w;
exit when C03%notfound;
	begin
	update	paciente_protocolo_medic
	set	QT_DOSE_PRESCR = (((nvl(pr_reducao_item_w,100) * nvl(QT_DOSE_PRESCR,0)) / 100))
	where	nr_seq_paciente	= nr_seq_paciente_p
	and	nr_seq_material = nr_seq_material_w;
	end;
end loop;
close C03;

select obter_peso_considerado_onc(nr_seq_paciente_p, nr_seq_material_p, qt_peso_w, qt_altura_cm_w, 'N')
into qt_peso_w
from dual;

/* atualizar os medicamentos com base no peso */
update paciente_protocolo_medic a  
set 	a.cd_unid_med_prescr =  
			   (select b.cd_unidade_med_princ  
				from unidade_medida b  
				where a.cd_unidade_medida = b.cd_unidade_medida),
	a.qt_dose_prescr = a.qt_dose * qt_peso_w
where 	a.nr_seq_paciente = nr_seq_paciente_p
and exists      (select b.cd_unidade_med_princ  
				from unidade_medida b  
				where a.cd_unidade_medida = b.cd_unidade_medida  
				  and b.cd_unidade_medida <> b.cd_unidade_med_princ  
				  and lower(b.cd_unidade_med_sec) = 'kg')
and	(nvl(ie_calcula_preenchido_w,'N') <> 'N' or (a.cd_unid_med_prescr is null and a.qt_dose_prescr is null))
and	a.qt_dose > 0
and	qt_peso_w is not null
and	((nr_seq_material_p is null) or (a.nr_seq_material	= nr_seq_material_p))
and	((nr_seq_solucao_p is null) or (a.nr_seq_solucao = nr_seq_solucao_p));

if (nvl(ie_atualizar_ciclos_w,'N')	<> 'N')then	
	open C01;
	loop
	fetch C01 into	
		nr_seq_atendimento_w;
	exit when C01%notfound;
		begin
		
		select obter_peso_considerado_onc(nr_seq_atendimento_w, nr_seq_material_p, qt_peso_w, qt_altura_cm_w, 'S')
		into qt_peso_w
		from dual;

		if (ie_atualizar_ciclos_w = 'C') then

			update	paciente_atendimento
			set		qt_creatinina 	= nvl(qt_creatinina_w,qt_creatinina),
					qt_auc  		= nvl(qt_auc_w,qt_auc),			
					dt_atualizacao	= sysdate,
					nm_usuario		= wheb_usuario_pck.get_nm_usuario
			where	nr_seq_atendimento = nr_seq_atendimento_w;

		end if;

		commit;		

		atualizar_dose_onc_ciclo(nr_seq_atendimento_w);
		end;
	end loop;
	close C01;
end if;

open C02;
	loop
	fetch C02 into	
		nr_seq_solucao_w;
	exit when C02%notfound;
		begin
		gerar_volume_protocolo_soluc(nr_seq_paciente_p,nr_seq_solucao_w,wheb_usuario_pck.get_nm_usuario);
		end;
	end loop;
	close C02;	

open C04;
loop
fetch C04 into	
	nr_seq_material_w;
exit when C04%notfound;
	begin

	delete from paciente_protocolo_medic
	where	NR_SEQ_DILUICAO = nr_seq_material_w
	and	nr_seq_paciente	= nr_seq_paciente_p;
	Gerar_Reconst_Diluicao_onc(nr_seq_paciente_p,nr_seq_material_w,'D',wheb_usuario_pck.get_nm_usuario);
	exception
		when others then
		null;
	end;
end loop;
close C04;	

Arredondar_dose_onc_protocolo(nr_seq_paciente_p);	

commit;
end atualizar_dose_onc_protocolo;
/
