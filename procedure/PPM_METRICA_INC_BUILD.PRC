create or replace
procedure PPM_METRICA_INC_BUILD(
			nr_seq_metrica_p		number,
			nr_seq_objetivo_metrica_p	number,
			cd_pessoa_fisica_p		varchar2,
			nm_usuario_p			varchar2,
			dt_referencia_p			date) is
			
dt_ref_inicio_w 	date;
dt_ref_fim_w 		date;
dt_referencia_w		date;
qt_defeito_w		number(10);
qt_os_w			number(10);
vl_resultado_w		number(10,2);
nm_usuario_exec_w	usuario.nm_usuario%type;
nr_seq_diretoria_w	ppm_objetivo.nr_seq_diretoria%type;
nr_seq_gerencia_w	ppm_objetivo.nr_seq_gerencia%type;
nr_seq_grupo_w		ppm_objetivo.nr_seq_grupo%type;

begin

select	max(nr_seq_gerencia),
	max(nr_seq_grupo),
	max(nr_seq_diretoria)
into	nr_seq_gerencia_w,
	nr_seq_grupo_w,
	nr_seq_diretoria_w
from	ppm_objetivo_metrica a,
	ppm_objetivo_meta b,
	ppm_objetivo c
where	a.nr_sequencia		= nr_seq_objetivo_metrica_p
and	a.nr_seq_meta		= b.nr_sequencia
and	b.nr_seq_objetivo	= c.nr_sequencia;

dt_ref_inicio_w := pkg_date_utils.start_of(dt_referencia_p,'MONTH');
dt_ref_fim_w 	:= pkg_date_utils.end_of(last_day(dt_referencia_p),'DAY');
dt_referencia_w	:= trunc(dt_referencia_p);

if	(nr_seq_grupo_w is not null) then

	select	count(distinct a.nr_sequencia)
	into	qt_os_w
	from	os_encerrada_gerencia_v a,
		MAN_OS_CTRL_DESC b
	where	a.nr_sequencia = b.nr_seq_ordem_serv
	and	b.dt_inativacao is null
	and	b.dt_liberacao is not null
	and	a.nr_seq_grupo_des = nr_seq_grupo_w
	and	a.dt_fim_real between dt_ref_inicio_w and dt_ref_fim_w;
	
	select 	count(distinct m.nr_sequencia)
	into	qt_defeito_w
	from 	reg_plano_teste_controle a,
		reg_tc_pendencies p,
		reg_tc_evidence_item i,
		reg_tc_so_pendencies o,
		man_ordem_servico m,
		man_doc_erro e
	where   a.nr_sequencia 		= p.nr_seq_controle_plano
	and 	p.nr_sequencia 		= i.nr_seq_ect
	and 	i.nr_sequencia 		= o.nr_seq_ev_item
	and 	m.nr_sequencia 		= o.nr_seq_service_order
	and 	m.nr_sequencia 		= e.nr_seq_ordem
	and 	e.nr_seq_grupo_des	= nr_seq_grupo_w	
	and	m.dt_ordem_servico	between dt_ref_inicio_w and dt_ref_fim_w
	and 	obter_status_tc_ev_item(p.nr_sequencia, 'd') = 'Falhou'
	AND 	a.ie_resultado 		= 'RP'
	and 	exists
		(select 1
		from  	man_ordem_serv_analise_vv x
		where  	m.nr_sequencia = x.nr_seq_ordem_serv
		and  	x.ie_severidade in (3, 4, 5));		

elsif	(nr_seq_gerencia_w is not null) then

	select	count(distinct a.nr_sequencia)
	into	qt_os_w
	from	os_encerrada_gerencia_v a,
		MAN_OS_CTRL_DESC b
	where	a.nr_sequencia = b.nr_seq_ordem_serv
	and	b.dt_inativacao is null
	and	b.dt_liberacao is not null
	and	a.nr_seq_gerencia = nr_seq_gerencia_w
	and	a.dt_fim_real between dt_ref_inicio_w and dt_ref_fim_w;
			
	select 	count(distinct m.nr_sequencia)
	into	qt_defeito_w
	from 	reg_plano_teste_controle a,
		reg_tc_pendencies p,
		reg_tc_evidence_item i,
		reg_tc_so_pendencies o,
		man_ordem_servico m,
		man_doc_erro e,
		grupo_desenvolvimento g
	where   a.nr_sequencia 		= p.nr_seq_controle_plano
	and 	p.nr_sequencia 		= i.nr_seq_ect
	and 	i.nr_sequencia 		= o.nr_seq_ev_item
	and 	m.nr_sequencia 		= o.nr_seq_service_order
	and 	m.nr_sequencia 		= e.nr_seq_ordem
	and 	e.nr_seq_grupo_des	= g.nr_sequencia
	and	g.nr_seq_gerencia	= nr_seq_gerencia_w
	and	m.dt_ordem_servico	between dt_ref_inicio_w and dt_ref_fim_w
	and 	obter_status_tc_ev_item(p.nr_sequencia, 'd') = 'Falhou'
	AND 	a.ie_resultado 		= 'RP'
	and 	exists
		(select 1
		from  	man_ordem_serv_analise_vv x
		where  	m.nr_sequencia = x.nr_seq_ordem_serv
		and  	x.ie_severidade in (3, 4, 5));	
	
elsif	(nr_seq_diretoria_w is not null) then

	select	count(distinct a.nr_sequencia)
	into	qt_os_w
	from	os_encerrada_gerencia_v a,
		MAN_OS_CTRL_DESC b,
		gerencia_wheb g
	where	a.nr_sequencia = b.nr_seq_ordem_serv
	and	b.dt_inativacao is null
	and	b.dt_liberacao is not null
	and	a.nr_seq_gerencia 	= g.nr_sequencia
	and	g.nr_seq_diretoria	= nr_seq_diretoria_w
	and	a.dt_fim_real between dt_ref_inicio_w and dt_ref_fim_w;

	select 	count(distinct m.nr_sequencia)
	into	qt_defeito_w
	from 	reg_plano_teste_controle a,
		reg_tc_pendencies p,
		reg_tc_evidence_item i,
		reg_tc_so_pendencies o,
		man_ordem_servico m,
		man_doc_erro e,
		grupo_desenvolvimento g,
		gerencia_wheb gw
	where   a.nr_sequencia 		= p.nr_seq_controle_plano
	and 	p.nr_sequencia 		= i.nr_seq_ect
	and 	i.nr_sequencia 		= o.nr_seq_ev_item
	and 	m.nr_sequencia 		= o.nr_seq_service_order
	and 	m.nr_sequencia 		= e.nr_seq_ordem
	and 	e.nr_seq_grupo_des	= g.nr_sequencia
	and	g.nr_seq_gerencia	= gw.nr_sequencia
	and	gw.nr_seq_diretoria	= nr_seq_diretoria_w
	and	m.dt_ordem_servico	between dt_ref_inicio_w and dt_ref_fim_w
	and 	obter_status_tc_ev_item(p.nr_sequencia, 'd') = 'Falhou'
	AND 	a.ie_resultado 		= 'RP'
	and 	exists
		(select 1
		from  	man_ordem_serv_analise_vv x
		where  	m.nr_sequencia = x.nr_seq_ordem_serv
		and  	x.ie_severidade in (3, 4, 5));	
		
else

	select	max(nm_usuario)
	into	nm_usuario_exec_w
	from	usuario x
	where	x.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	x.ie_situacao	= 'A';
	
	--Quantidade de OS's de defeito por pessoa
	select 	count(distinct m.nr_sequencia)
	into	qt_defeito_w
	from 	reg_plano_teste_controle a,
		reg_tc_pendencies p,
		reg_tc_evidence_item i,
		reg_tc_so_pendencies o,
		man_ordem_servico m,
		man_doc_erro e
	where   a.nr_sequencia 		= p.nr_seq_controle_plano
	and 	p.nr_sequencia 		= i.nr_seq_ect
	and 	i.nr_sequencia 		= o.nr_seq_ev_item
	and 	m.nr_sequencia 		= o.nr_seq_service_order
	and 	m.nr_sequencia 		= e.nr_seq_ordem
	and 	e.cd_pessoa_fisica	= cd_pessoa_fisica_p	
	and	m.dt_ordem_servico	between dt_ref_inicio_w and dt_ref_fim_w
	and 	obter_status_tc_ev_item(p.nr_sequencia, 'd') = 'Falhou'
	AND 	a.ie_resultado 		= 'RP'
	and 	exists
		(select 1
		from  	man_ordem_serv_analise_vv x
		where  	m.nr_sequencia = x.nr_seq_ordem_serv
		and  	x.ie_severidade in (3, 4, 5));	

	--Busca as OSs encerradas com base no indicador j� utilizado
	select	count(distinct a.nr_seq_ordem_Serv)
	into	qt_os_w
	from	w_avaliacao_usuario a,
		MAN_OS_CTRL_DESC b
	where	a.nr_seq_ordem_Serv = b.nr_seq_ordem_serv
	and	b.dt_inativacao is null
	and	b.dt_liberacao is not null	
	and	a.dt_referencia between dt_ref_inicio_w and dt_ref_fim_w
	and	a.ie_referencia = 'E'
	and	a.nm_usuario 	= nm_usuario_exec_w;	
	
end if;

vl_resultado_w := dividir( qt_defeito_w, qt_os_w ) * 100;

PPM_GRAVAR_RESULTADO(nr_seq_objetivo_metrica_p, dt_referencia_p, vl_resultado_w, qt_os_w, qt_defeito_w, nm_usuario_p);

end PPM_METRICA_INC_BUILD;
/