create or replace
procedure Gerar_Producao_Medico
		(cd_medico_p		varchar2,
		dt_inicial_p		date,
		dt_final_p		date,
		nm_usuario_p		varchar2) is

nr_sequencia_w		number(10,0);
nr_seq_propaci_w	number(10,0);
nr_seq_partic_w		number(10,0);
vl_medico_w		number(15,2);
dt_proced_w		date;

cursor	c01 is
	select	a.nr_sequencia,
		b.nr_seq_partic,
		b.vl_participante,
		a.dt_procedimento
	from	procedimento_participante b,
		procedimento_paciente a
	where	a.nr_sequencia		= b.nr_sequencia
	and	b.cd_pessoa_fisica	= cd_medico_p
	and	a.dt_procedimento between dt_inicial_p and fim_dia(dt_final_p)
	and	nvl(b.vl_participante,0) > 0
	and	not exists
		(select	1
		from	controle_proc_terceiro x
		where	x.nr_seq_propaci	= b.nr_sequencia
		and	x.nr_seq_partic		= b.nr_seq_partic
		and	x.cd_pessoa_fisica	= cd_medico_p)
	union 
	select	a.nr_sequencia,
		0,
		a.vl_medico,
		a.dt_procedimento
	from	procedimento_paciente a
	where	a.cd_medico_executor	= cd_medico_p
	and	a.dt_procedimento between dt_inicial_p and fim_dia(dt_final_p)
	and	nvl(a.vl_medico,0) > 0
	and	not exists
		(select	1
		from	controle_proc_terceiro x
		where	x.nr_seq_propaci	= a.nr_sequencia
		and	x.cd_pessoa_fisica	= cd_medico_p
		and	x.nr_seq_partic		is null);
begin

open	c01;
loop
fetch	c01 into
	nr_seq_propaci_w,
	nr_seq_partic_w,
	vl_medico_w,
	dt_proced_w;
exit	when c01%notfound;
	begin

	select	controle_proc_terceiro_seq.nextval
	into	nr_sequencia_w
	from	dual;


	insert 	into controle_proc_terceiro
		(nr_sequencia,
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		nr_seq_propaci,
		nr_seq_partic,
		vl_producao,
		dt_producao,
		vl_recebido,
		dt_recebido)
	values	(nr_sequencia_w,
		cd_medico_p,
		sysdate,
		nm_usuario_p,
		nr_seq_propaci_w,
		decode(nr_seq_partic_w, 0, null, nr_seq_partic_w),
		vl_medico_w,
		dt_proced_w,
		null, null);

	end;
end loop;
close	c01;

GERAR_CONTROLE_PROC_TERC_LOG(nm_usuario_p,dt_inicial_p,dt_final_p);

commit;

end Gerar_Producao_Medico;
/