create or replace procedure DUPLICAR_MODELO_PEPO(nr_sequencia_p number,
												 nm_usuario_p varchar2) is

w_new_seq_modelo pepo_modelo.nr_sequencia%type;
w_new_seq_secao  pepo_modelo_secao.nr_sequencia%type;
w_new_seq_sv	 pepo_modelo_secao_sv.nr_sequencia%type;
w_new_seq_med	 pepo_modelo_secao_agt_med.nr_sequencia%type;
w_new_seq_ev	 pepo_modelo_evento.nr_sequencia%type;
												 
begin
										   
	
	for cModelo in (select *  from pepo_modelo where nr_sequencia = nr_sequencia_p) loop
	
		insert into pepo_modelo(nr_sequencia,cd_estabelecimento ,dt_atualizacao,nm_usuario ,dt_atualizacao_nrec,nm_usuario_nrec,nm_modelo,nr_seq_categoria
								,ds_modelo,ie_situacao,ie_escala_tempo_modelo,ie_disp_modelo_estab,nr_seq_apresentacao)
						values (pepo_modelo_seq.nextval,cModelo.cd_estabelecimento ,sysdate,nm_usuario_p ,sysdate,nm_usuario_p
									  ,wheb_mensagem_pck.get_texto(519917)||' '||cModelo.nm_modelo,cModelo.nr_seq_categoria
									  ,cModelo.ds_modelo,cModelo.ie_situacao,cModelo.ie_escala_tempo_modelo,cModelo.ie_disp_modelo_estab,cModelo.nr_seq_apresentacao
								) returning nr_sequencia into w_new_seq_modelo;
								
		insert into pepo_modelo_acesso(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,ie_acesso,cd_estab_regra,cd_perfil_regra,nm_usuario_regra,nr_seq_modelo) 
							    (select pepo_modelo_acesso_seq.nextval,sysdate,nm_usuario_p ,sysdate,nm_usuario_p, ie_acesso,cd_estab_regra,cd_perfil_regra,nm_usuario_regra, w_new_seq_modelo
										 from pepo_modelo_acesso 
										where nr_seq_modelo = nr_sequencia_p);
									   
		insert into pepo_modelo_setor(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,cd_setor_atendimento,nr_seq_modelo,ie_acesso) 
							   (select pepo_modelo_setor_seq.nextval,sysdate,nm_usuario_p ,sysdate,nm_usuario_p,cd_setor_atendimento,w_new_seq_modelo,ie_acesso
										from pepo_modelo_setor 
									   where nr_seq_modelo = nr_sequencia_p);
									   
		for cSecao in (select * from pepo_modelo_secao where nr_seq_modelo = nr_sequencia_p) loop

			insert into pepo_modelo_secao (nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_apresentacao,ds_titulo
										  ,nr_seq_modelo,ie_padrao_visivel,nr_escala_maxima_esq,nr_escala_minima_esq,cd_secao,ie_modo_visualizacao)
								   values (pepo_modelo_secao_seq.nextval,sysdate,nm_usuario_p ,sysdate,nm_usuario_p,cSecao.nr_seq_apresentacao
										  ,cSecao.ds_titulo,w_new_seq_modelo,cSecao.ie_padrao_visivel,cSecao.nr_escala_maxima_esq
										  ,cSecao.nr_escala_minima_esq,cSecao.cd_secao,cSecao.ie_modo_visualizacao) returning nr_sequencia into w_new_seq_secao;		
			
			for cSV in (select * from pepo_modelo_secao_sv where nr_seq_modelo_secao = cSecao.nr_sequencia) loop
				
				insert into pepo_modelo_secao_sv (nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_apresentacao
												 ,nr_seq_pepo_sv,nr_seq_modelo_secao,nr_seq_fav)
										 values	 (pepo_modelo_secao_sv_seq.nextval, sysdate,nm_usuario_p ,sysdate,nm_usuario_p, 
												  cSV.nr_seq_apresentacao, cSV.nr_seq_pepo_sv,w_new_seq_secao,cSV.nr_seq_fav) returning nr_sequencia into w_new_seq_sv;
				
				insert into pepo_modelo_sec_sv_regra (nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_modelo_secao_sv,qt_minimo
													 ,qt_maximo,qt_min_aviso,qt_max_aviso,ds_mensagem_alerta,ds_mensagem_bloqueio,ie_verif_timeout, qt_timeout_sv)
											  (select pepo_modelo_sec_sv_regra_seq.nextval,sysdate,nm_usuario_p ,sysdate,nm_usuario_p,w_new_seq_sv,qt_minimo
															,qt_maximo,qt_min_aviso,qt_max_aviso,ds_mensagem_alerta,ds_mensagem_bloqueio,ie_verif_timeout, qt_timeout_sv 
														from pepo_modelo_sec_sv_regra 
													   where nr_seq_modelo_secao_sv = cSV.nr_sequencia);
			end loop;
			
			insert into pepo_modelo_secao_agt_med (nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_apresentacao
												  ,nr_seq_modelo_secao,nr_seq_pepo_agent_med,nr_seq_fav,nr_seq_agent_anest_mat)
										   (select pepo_modelo_secao_agt_med_seq.nextval,sysdate,nm_usuario_p ,sysdate,nm_usuario_p,nr_seq_apresentacao
														 ,w_new_seq_secao,nr_seq_pepo_agent_med,nr_seq_fav,nr_seq_agent_anest_mat 
													 from pepo_modelo_secao_agt_med 
													where nr_seq_modelo_secao = cSecao.nr_sequencia);
		end loop;
													
			for cEvent in (select * from pepo_modelo_evento where nr_seq_modelo = nr_sequencia_p ) loop
			
				insert into pepo_modelo_evento(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec
											  ,nr_seq_modelo,nr_ordem_exibicao,nr_seq_evento,nr_seq_secao)
									  values (pepo_modelo_evento_seq.nextval,sysdate,nm_usuario_p ,sysdate,nm_usuario_p,
											   w_new_seq_modelo,cEvent.nr_ordem_exibicao, cEvent.nr_seq_evento, cEvent.nr_seq_secao
											  ) returning nr_sequencia into w_new_seq_ev;
				
				insert into pepo_modelo_evt_protocolo(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_modelo_evento
													 ,nr_seq_protocolo_cirurgia)
											   (select pepo_modelo_evt_protocolo_seq.nextval,sysdate,nm_usuario_p ,sysdate,nm_usuario_p,
															w_new_seq_ev,nr_seq_protocolo_cirurgia
													   from pepo_modelo_evt_protocolo 
													  where nr_seq_modelo_evento = cEvent.nr_sequencia);
			end loop;	
	end loop;
commit;
end duplicar_modelo_pepo;
/
