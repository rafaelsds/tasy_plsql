create or replace procedure intpd_recebe_pagto_conv(

	nr_sequencia_p	in	number,

	xml_p		in	xmltype) is 



convenio_receb_w	convenio_receb%rowtype;



ie_conversao_w		intpd_eventos_sistema.ie_conversao%type;

nr_seq_projeto_xml_w	intpd_eventos_sistema.nr_seq_projeto_xml%type;

nr_seq_sistema_w		intpd_eventos_sistema.nr_seq_sistema%type;

ie_sistema_externo_w	varchar2(15);

reg_integracao_w		gerar_int_padrao.reg_integracao_conv;

nr_seq_regra_w		conversao_meio_externo.nr_seq_regra%type;

ds_erro_w		varchar2(4000);

i			integer;

cd_convenio_w		convenio.cd_convenio%type;

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

cd_moeda_w		moeda.cd_moeda%type;

nr_seq_conta_banco_w	convenio_receb.nr_seq_conta_banco%type;

ds_enter_w		varchar2(255) := chr(13)||chr(10);

nr_sequencia_conv_receb_w	convenio_receb.nr_sequencia%type;



/*'Efetua a consulta transformando o elemento XML num tipo de tabela'*/

cursor c01 is

select	*

from	xmltable('/STRUCTURE/INSURANCE_PAYMENT' passing xml_p columns

	ie_action			varchar2(15)	path	'IE_ACTION',

	cd_estabelecimento		number(4)	path	'CD_ESTABELECIMENTO',

	nr_sistema_externo		varchar2(40)	path	'NR_DOCUMENTO_EXTERNO',

	nr_sistema_externo_aux		varchar2(40)	path	'NR_DOCUMENTO_EXTERNO_AUX',

	cd_cgc_convenio			varchar2(14)	path	'CD_CGC_CONVENIO',

	cd_convenio			number(10)	path	'CD_CONVENIO',

	cd_moeda			number(3)	path	'CD_MOEDA_TASY',

	nr_seq_conta_banco		number(10)	path 	'NR_CONTA_BANCO_TASY',

	vl_recebimento			varchar(20)	path	'VL_RECEBIMENTO_TASY',

	ds_observacao			varchar2(255)	path	'DS_OBSERVACAO_TASY',

	ie_gerar_liberado		varchar(15)	path	'IE_GERAR_LIBERADO');

	

c01_w	c01%rowtype;

	

begin



/*'Atualiza o status da fila para Em processamento'*/

update	intpd_fila_transmissao

set	ie_status = 'R'

where	nr_sequencia = nr_sequencia_p;



/*'Realiza o commit para n�o permite o status de processamento em casa de rollback por existir consist�ncia. Existe tratamento de exce��o abaixo para colocar o status de erro em caso de falha'*/

commit;



/*'In�cio de controle de falha'*/

begin

/*'Busca os dados da regra do registro da fila que est� em processamento'*/

select	nvl(b.ie_conversao,'I'),

	nr_seq_sistema,

	nr_seq_projeto_xml,

	nr_seq_regra_conv

into	ie_conversao_w,

	nr_seq_sistema_w,

	nr_seq_projeto_xml_w,

	nr_seq_regra_w

from	intpd_fila_transmissao a,

	intpd_eventos_sistema b

where	a.nr_seq_evento_sistema = b.nr_sequencia

and	a.nr_sequencia = nr_sequencia_p;



ie_sistema_externo_w	:=	nr_seq_sistema_w;



/*'Alimenta as informa��es iniciais de controle e consist�ncia de cada atributo do XML'*/

reg_integracao_w.nr_seq_fila_transmissao	:=	nr_sequencia_p;

reg_integracao_w.ie_envio_recebe		:=	'R';

reg_integracao_w.ie_sistema_externo		:=	ie_sistema_externo_w;

reg_integracao_w.ie_conversao		:=	ie_conversao_w;

reg_integracao_w.nr_seq_projeto_xml		:=	nr_seq_projeto_xml_w;

reg_integracao_w.nr_seq_regra_conversao	:=	nr_seq_regra_w;

reg_integracao_w.intpd_log_receb.delete;

reg_integracao_w.qt_reg_log			:=	0;



open c01;

loop

fetch c01 into	

	c01_w;

exit when c01%notfound;

	begin

	/*'Alimenta as informa��es de controle e consist�ncia referente ao Elemento a ser processado no momento. � importante manter dentro do cursor e n�o fora.'*/

	reg_integracao_w.nm_tabela	:=	'CONVENIO_RECEB';

	reg_integracao_w.nm_elemento	:=	'INSURANCE_PAYMENT';

	reg_integracao_w.nr_seq_visao	:=	'';

	

	if	(c01_w.cd_convenio is not null) then

			

		begin

		select	cd_convenio

		into	cd_convenio_w

		from	convenio

		where	cd_convenio = c01_w.cd_convenio;

		exception

		when NO_DATA_FOUND then

			wheb_mensagem_pck.exibir_mensagem_abort(1014702,'cd_convenio_w='||c01_w.cd_convenio);

		end;

	

	elsif	(c01_w.cd_cgc_convenio is not null) then

		

		begin

		select	cd_convenio

		into	cd_convenio_w

		from	convenio

		where	cd_cgc = c01_w.cd_cgc_convenio;

		exception

		when TOO_MANY_ROWS then

			wheb_mensagem_pck.exibir_mensagem_abort(759917,'cd_cgc_convenio_w='||c01_w.cd_cgc_convenio);

		end;

		

		begin

		select	cd_convenio

		into	cd_convenio_w

		from	convenio

		where	cd_cgc = c01_w.cd_cgc_convenio;

		exception

		when NO_DATA_FOUND then

			wheb_mensagem_pck.exibir_mensagem_abort(759918,'cd_cgc_convenio_w='||c01_w.cd_cgc_convenio);

		end;

		

	end if;

	

	

	

	if	(nvl(c01_w.cd_estabelecimento,0) > 0) then

		begin

		select	cd_estabelecimento

		into	cd_estabelecimento_w

		from	estabelecimento

		where	cd_estabelecimento = c01_w.cd_estabelecimento;

		exception

		when NO_DATA_FOUND then

			wheb_mensagem_pck.exibir_mensagem_abort(759919,'cd_estabelecimento_w='||c01_w.cd_estabelecimento);

		end;

	else

		wheb_mensagem_pck.exibir_mensagem_abort(759920);

	end if;

	

	if	(nvl(c01_w.cd_moeda,0) > 0) then

		begin

		select	cd_moeda

		into	cd_moeda_w

		from	moeda

		where	cd_moeda = c01_w.cd_moeda;

		exception

		when NO_DATA_FOUND then

			wheb_mensagem_pck.exibir_mensagem_abort(759921,'cd_moeda_w='||c01_w.cd_moeda);

		end;

	else

		wheb_mensagem_pck.exibir_mensagem_abort(759922);

	end if;

	

	if	(Nvl(c01_w.nr_seq_conta_banco,0) > 0) then



		BEGIN

		SELECT	NR_SEQUENCIA

		INTO	nr_seq_conta_banco_w

		FROM	BANCO_ESTABELECIMENTO

		WHERE	NR_SEQUENCIA = c01_w.nr_seq_conta_banco;

		exception

		when no_data_found then

			begin

			nr_seq_conta_banco_w := Obter_Conversao_Externa(c01_w.cd_cgc_convenio,'CONVENIO_RECEB','NR_SEQ_CONTA_BANCARIA_TASY', c01_w.nr_seq_conta_banco);

			

			if	(nr_seq_conta_banco_w > 0) then

			

				begin

				SELECT	NR_SEQUENCIA

				INTO	nr_seq_conta_banco_w

				FROM	BANCO_ESTABELECIMENTO

				WHERE	NR_SEQUENCIA = nr_seq_conta_banco_w;

				exception

				when no_data_found then

					wheb_mensagem_pck.exibir_mensagem_abort(759923,'nr_seq_conta_banco_w='||c01_w.nr_seq_conta_banco);

				end;

				

			end if;

			

			end;

		end;

		

	end if;

	

	begin

	--'Consiste cada atributo do XML'	

	intpd_processar_atributo(reg_integracao_w, 'CD_ESTABELECIMENTO', cd_estabelecimento_w, 'N', convenio_receb_w.cd_estabelecimento);

	intpd_processar_atributo(reg_integracao_w, 'NR_SISTEMA_EXTERNO', c01_w.nr_sistema_externo, 'N', convenio_receb_w.nr_sistema_externo);

	intpd_processar_atributo(reg_integracao_w, 'NR_SISTEMA_EXTERNO_AUX', c01_w.nr_sistema_externo_aux, 'N', convenio_receb_w.nr_sistema_externo_aux);

	intpd_processar_atributo(reg_integracao_w, 'CD_CGC_CONVENIO', cd_convenio_w, 'N', convenio_receb_w.cd_convenio);

	intpd_processar_atributo(reg_integracao_w, 'CD_MOEDA', cd_moeda_w, 'N', convenio_receb_w.cd_moeda);

	intpd_processar_atributo(reg_integracao_w, 'VL_RECEBIMENTO', to_number(replace(obter_valor_campo_separador(c01_w.vl_recebimento,7,'|'),'.',',')), 'N', convenio_receb_w.vl_recebimento);

	intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c01_w.ds_observacao, 'N', convenio_receb_w.ds_observacao);
		
	/*'Efetua a atualiza��o caso n�o possua consist�ncia '*/

	if	(reg_integracao_w.qt_reg_log = 0) then

		begin		

		select	convenio_receb_seq.nextval

		into	convenio_receb_w.nr_sequencia

		from	dual;

		

		convenio_receb_w.nr_seq_conta_banco := nr_seq_conta_banco_w;

		convenio_receb_w.dt_atualizacao := sysdate;

		convenio_receb_w.dt_recebimento := sysdate;

		convenio_receb_w.ie_integrar_cb_fluxo := 'N';

		convenio_receb_w.ie_status := 'N';

		convenio_receb_w.nm_usuario := 'Integra��o';

		convenio_receb_w.vl_deposito := convenio_receb_w.vl_recebimento;

		convenio_receb_w.vl_despesa_bancaria := 0;
		
		if(c01_w.ie_gerar_liberado = 'S') then
    
			begin			
      
			convenio_receb_w.dt_liberacao := sysdate;			
      
			end;
      
		end if;		

		insert into convenio_receb values convenio_receb_w

		returning nr_sequencia into nr_sequencia_conv_receb_w;

		

		/*'Registra a convers�o que poder� ser utilizada em outros pontos de integra��o e para somente atualizar (update) num recebimento futuro do mesmo registro.'*/

		gerar_conv_meio_externo(null, 'CONVENIO_RECEB', 'NR_SEQUENCIA', convenio_receb_w.nr_sequencia, c01_w.nr_sistema_externo, null, nr_seq_regra_w, 'A', convenio_receb_w.nm_usuario);

		end;

	end if;

	end;

	end;

end loop;

close c01;

exception

when others then

	begin

	--'Em caso de qualquer falha o sistema captura a mensagem de erro, efetua o rollback, atualiza o status para Erro e registra a falha ocorrida'

	ds_erro_w	:= ds_erro_w || substr(sqlerrm,1,4000);

	rollback;

	update	intpd_fila_transmissao

	set	ie_status = 'E',

		ds_log = ds_erro_w

	where	nr_sequencia = nr_sequencia_p;

	end;

end;



if	(reg_integracao_w.qt_reg_log > 0) then

	begin

	/*'Em caso de qualquer consist�ncia o sistema efetua rollback, atualiza o status para Erro e registra todos os logs de consist�ncia'*/

	rollback;

	

	update	intpd_fila_transmissao

	set	ie_status = 'E',

		ds_log = ds_erro_w

	where	nr_sequencia = nr_sequencia_p;

	

	for i in 0..reg_integracao_w.qt_reg_log-1 loop

		intpd_gravar_log_recebimento(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');

	end loop;

	end;

else

	update	intpd_fila_transmissao

	set	ie_status = 'S',

		nr_seq_documento = nr_sequencia_conv_receb_w

	where	nr_sequencia = nr_sequencia_p;

end if;



commit;

end intpd_recebe_pagto_conv;
/
