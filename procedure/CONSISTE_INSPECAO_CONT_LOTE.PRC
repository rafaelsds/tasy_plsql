create or replace
procedure consiste_inspecao_cont_lote (	nr_seq_registro_p		number,
					nr_seq_inspecao_p		number,
					nm_usuario_p		Varchar2) is

cd_material_w			number(10);
cd_estabelecimento_w		number(10)	:= wheb_usuario_pck.get_cd_estabelecimento;

/*Primeira Contagem*/
cd_lote_fabricacao_um_w		varchar2(20);
cd_barra_material_um_w          	varchar2(40);
ds_barras_um_w                  		varchar2(4000);
dt_fabricacao_um_w              		varchar2(110);
dt_validade_um_w                		varchar2(110);

ie_indeterminada_um_w           	varchar2(1);
nr_seq_contagem_um_w            	number(10);
nr_seq_marca_um_w              	 	number(10);
ds_marca_um_w			varchar2(255);
nr_sequencia_um_w               		number(10);
qt_material_um_w                		number(15,4);

/*Segunda Contagem*/
cd_lote_fabricacao_dois_w		varchar2(20);
cd_barra_material_dois_w        	varchar2(40);
ds_barras_dois_w                		varchar2(4000);
dt_fabricacao_dois_w            		varchar2(110);
dt_validade_dois_w              		varchar2(110);

ie_indeterminada_dois_w         	varchar2(1);
nr_seq_contagem_dois_w         	number(10);
nr_seq_marca_dois_w             	number(10);
ds_marca_dois_w            		varchar2(255);
nr_sequencia_dois_w            	 	number(10);
qt_material_dois_w              		number(15,4);

ie_consiste_marca_insp_w		varchar2(1);
qt_marca_nao_aprov_w		number(10);
cd_perfil_w			number(10);
ie_consiste_barras_DM_w		varchar2(1);

ie_lote_gerado_segunda_cont_w	boolean := False;

/*Primeira Contagem*/
Cursor C01 is
	select	a.cd_lote_fabricacao,
	        	nvl(a.ds_barras, wheb_mensagem_pck.get_texto(312793)) ds_barras,
	        	nvl(to_char(a.dt_fabricacao, 'dd/mm/yyyy'), wheb_mensagem_pck.get_texto(312793)),
	        	nvl(to_char(a.dt_validade, 'dd/mm/yyyy'), wheb_mensagem_pck.get_texto(312793)),
	        	nvl(a.ie_indeterminada, wheb_mensagem_pck.get_texto(312793)) ie_indeterminada,
		substr(nvl(obter_desc_marca(a.nr_seq_marca),wheb_mensagem_pck.get_texto(312793)),1,255) ds_marca,
	        	nvl(a.nr_seq_marca,0) nr_seq_marca,
	        	nvl(a.qt_material,0) qt_material
	from    	inspecao_receb_lote_cont a
	where   	a.nr_seq_contagem = ( 	select  	max(x.nr_sequencia)
						from    	inspecao_contagem x
						where   	x.nr_seq_contagem = 1
						and     	x.nr_seq_inspecao = nr_seq_inspecao_p)
	order   by 1;

/*Segunda Contagem*/
Cursor C02 is
	select	nvl(a.ds_barras, wheb_mensagem_pck.get_texto(312793)) ds_barras,
	        	nvl(to_char(a.dt_fabricacao, 'dd/mm/yyyy'), wheb_mensagem_pck.get_texto(312793)),
	        	nvl(to_char(a.dt_validade, 'dd/mm/yyyy'), wheb_mensagem_pck.get_texto(312793)),
	        	nvl(a.ie_indeterminada, wheb_mensagem_pck.get_texto(312793)) ie_indeterminada,
		substr(nvl(obter_desc_marca(a.nr_seq_marca),wheb_mensagem_pck.get_texto(312793)),1,255) ds_marca,
	        	nvl(a.nr_seq_marca,0) nr_seq_marca,
	        	nvl(a.qt_material,0) qt_material
	from    	inspecao_receb_lote_cont a
	where   	a.nr_seq_contagem = ( 	select  	max(x.nr_sequencia)
						from    	inspecao_contagem x
						where   	x.nr_seq_contagem = 2
						and     	x.nr_seq_inspecao = nr_seq_inspecao_p)
	and	a.cd_lote_fabricacao = cd_lote_fabricacao_um_w
	order   by 1;

/*Segunda Contagem (Verifica se foi inspecionado o lote na segunda contagem, e na primeira n�o) */
Cursor C03 is
	select	a.cd_lote_fabricacao
	from    inspecao_receb_lote_cont a
	where   a.nr_seq_contagem = ( 		select	max(x.nr_sequencia)
						from    inspecao_contagem x
						where   x.nr_seq_contagem = 2
						and     x.nr_seq_inspecao = nr_seq_inspecao_p)
	and     a.cd_lote_fabricacao not in (	select  y.cd_lote_fabricacao
						from    inspecao_receb_lote_cont y
						where   y.nr_seq_contagem = (	select	x.nr_sequencia
										from    inspecao_contagem x
										where   x.nr_seq_contagem = 1
										and	x.nr_seq_inspecao = nr_seq_inspecao_p))
	order   by 1;

begin

cd_perfil_w	:= obter_perfil_ativo;

begin
	select	max(cd_material)
	into	cd_material_w
	from   	inspecao_contagem
	where   	nr_seq_inspecao = nr_seq_inspecao_p;
exception when others then
	cd_material_w := 0;
end;

if	(cd_material_w > 0) then
	begin

	select	nvl((max(obter_valor_param_usuario(270, 71, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w))),'N'),
		nvl((max(obter_valor_param_usuario(270, 88, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w))),'N')
	into	ie_consiste_marca_insp_w,
		ie_consiste_barras_DM_w
	from	dual;

	open C01;
	loop
	fetch C01 into
		cd_lote_fabricacao_um_w,
		ds_barras_um_w,
		dt_fabricacao_um_w,
		dt_validade_um_w,
		ie_indeterminada_um_w,
		ds_marca_um_w,
		nr_seq_marca_um_w,
		qt_material_um_w;
	exit when C01%notfound;
		begin
		ie_lote_gerado_segunda_cont_w := False;

		if	(nr_seq_marca_um_w <> 0) and
			(ie_consiste_marca_insp_w = 'S') then
			begin

			select	count(*)
			into	qt_marca_nao_aprov_w
			from	material_marca
			where	nr_sequencia = nr_seq_marca_um_w
			and	cd_material = cd_material_w
			and	nr_seq_status_aval > 0
			and	obter_tipo_status_aval_marca(nr_seq_status_aval) <> 'A';

			if	(qt_marca_nao_aprov_w > 0) then
				grava_inconsistencia_contagem ( 	nr_seq_inspecao_p,
								cd_material_w,
								wheb_mensagem_pck.get_texto(312794), --ds_campo_p
								ds_marca_um_w, --ds_contagem_um_p
								'', --ds_contagem_dois_p
								'LMA',
								wheb_mensagem_pck.get_texto(312795,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
								nm_usuario_p);
			end if;

			end;
		end if;

		open C02;
		loop
		fetch C02 into
			ds_barras_dois_w,
			dt_fabricacao_dois_w,
			dt_validade_dois_w,
		        	ie_indeterminada_dois_w,
			ds_marca_dois_w,
		        	nr_seq_marca_dois_w,
		        	qt_material_dois_w;
		exit when C02%notfound;
			begin
			
			

			if	(ie_consiste_barras_DM_w = 'S') and
				(ds_barras_um_w <> ds_barras_dois_w) then
				grava_inconsistencia_contagem ( nr_seq_inspecao_p,
								cd_material_w,
								wheb_mensagem_pck.get_texto(312797), --ds_campo_p
								ds_barras_um_w,  --ds_contagem_um_p
								ds_barras_dois_w, --ds_contagem_dois_p
								'LBA',
								wheb_mensagem_pck.get_texto(312798,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
								nm_usuario_p);
			end if;

			if	(dt_fabricacao_um_w <> dt_fabricacao_dois_w) then
				grava_inconsistencia_contagem (	nr_seq_inspecao_p,
								cd_material_w,
								wheb_mensagem_pck.get_texto(312800), --ds_campo_p
								dt_fabricacao_um_w, --ds_contagem_um_p
								dt_fabricacao_dois_w, --ds_contagem_dois_p
								'LDF',
								wheb_mensagem_pck.get_texto(312801,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
								nm_usuario_p);
			end if;

			if	(dt_validade_um_w <> dt_validade_dois_w) then
				grava_inconsistencia_contagem ( 	nr_seq_inspecao_p,
								cd_material_w,
								wheb_mensagem_pck.get_texto(312802), --ds_campo_p
								dt_validade_um_w, --ds_contagem_um_p
								dt_validade_dois_w, --ds_contagem_dois_p
								'LVA',
								wheb_mensagem_pck.get_texto(312804,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
								nm_usuario_p);
			end if;

			if	(ie_indeterminada_um_w <> ie_indeterminada_dois_w) then
				grava_inconsistencia_contagem ( 	nr_seq_inspecao_p,
								cd_material_w,
								wheb_mensagem_pck.get_texto(312805), --ds_campo_p
								ie_indeterminada_um_w, --ds_contagem_um_p
								ie_indeterminada_dois_w, --ds_contagem_dois_p
								'LIN',
								wheb_mensagem_pck.get_texto(312806,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
								nm_usuario_p);
			end if;

			if	(nr_seq_marca_um_w <> nr_seq_marca_dois_w) then
				grava_inconsistencia_contagem ( 	nr_seq_inspecao_p,
								cd_material_w,
								wheb_mensagem_pck.get_texto(312794), --ds_campo_p
								ds_marca_um_w, --ds_contagem_um_p
								ds_marca_dois_w, --ds_contagem_dois_p
								'LMA',
								wheb_mensagem_pck.get_texto(312811,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
								nm_usuario_p);
			end if;

			if	(qt_material_um_w <> qt_material_dois_w) then
				grava_inconsistencia_contagem (	nr_seq_inspecao_p,
								cd_material_w,
								wheb_mensagem_pck.get_texto(312813), --ds_campo_p
								qt_material_um_w, --ds_contagem_um_p
								qt_material_dois_w, --ds_contagem_dois_p
								'LQE',
								wheb_mensagem_pck.get_texto(312814,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
								nm_usuario_p);
			end if;

			if	(nr_seq_marca_dois_w <> 0) and
				(ie_consiste_marca_insp_w = 'S') then
				begin

				select	count(*)
				into	qt_marca_nao_aprov_w
				from	material_marca
				where	nr_sequencia = nr_seq_marca_dois_w
				and	cd_material = cd_material_w
				and	nr_seq_status_aval > 0
				and	obter_tipo_status_aval_marca(nr_seq_status_aval) <> 'A';

				if	(qt_marca_nao_aprov_w > 0) then
					grava_inconsistencia_contagem ( 	nr_seq_inspecao_p,
									cd_material_w,
									wheb_mensagem_pck.get_texto(312794), --ds_campo_p
									'', --ds_contagem_um_p
									ds_marca_dois_w, --ds_contagem_dois_p
									'LMA',
									wheb_mensagem_pck.get_texto(312815,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
									nm_usuario_p);
				end if;

				end;
			end if;

			ie_lote_gerado_segunda_cont_w := True;

			end;
		end loop;
		close C02;

		if	(not ie_lote_gerado_segunda_cont_w) then
			grava_inconsistencia_contagem (	nr_seq_inspecao_p,
							cd_material_w,
							wheb_mensagem_pck.get_texto(312820), --ds_campo_p
							wheb_mensagem_pck.get_texto(312822), --ds_contagem_um_p
							wheb_mensagem_pck.get_texto(312821), --ds_contagem_dois_p
							'LGE',
							wheb_mensagem_pck.get_texto(312823,'CD_LOTE_FABRICACAO_UM_W='||cd_lote_fabricacao_um_w), --ds_consistencia_p,
							nm_usuario_p);
		end if;
		end;
	end loop;
	close C01;

	open C03;
	loop
	fetch C03 into	
		cd_lote_fabricacao_dois_w;
	exit when C03%notfound;
		begin
		grava_inconsistencia_contagem (	nr_seq_inspecao_p,
						cd_material_w,
						wheb_mensagem_pck.get_texto(312820), --ds_campo_p
						wheb_mensagem_pck.get_texto(312821), --ds_contagem_um_p
						wheb_mensagem_pck.get_texto(312822), --ds_contagem_dois_p
						'LNG',
						wheb_mensagem_pck.get_texto(312824,'CD_LOTE_FABRICACAO_DOIS_W='||cd_lote_fabricacao_dois_w), --ds_consistencia_p,
						nm_usuario_p);
		end;
	end loop;
	close C03;

	end;
end if;

commit;

end consiste_inspecao_cont_lote;
/