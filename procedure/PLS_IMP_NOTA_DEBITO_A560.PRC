create or replace
procedure pls_imp_nota_debito_a560
			(	ds_conteudo_p			varchar2,
				nm_usuario_p			varchar2,
				nr_seq_camara_contest_p		number,
				nr_seq_referencia_p		out number,
				ie_versao_p		in out	varchar2,
				cd_estabelecimento_p		number) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OPS - Controle de Contesta��es
Finalidade: Importar o arquivo A560.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:	

06 - 5.0a, 07 - 6.0, 08 - 7.0, 09 - 8.0
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/  
				
begin
-- OBTER A VERS�O DE TRANSA��O
if	(ie_versao_p is null) then
	select	substr(ds_conteudo_p,45,2)
	into	ie_versao_p
	from	dual;
end if;

if	((trim(substr(ds_conteudo_p,9,2)) is not null) and ((substr(ds_conteudo_p,9,2) <> '56') and (substr(ds_conteudo_p,9,2) <> '99'))) then
	Wheb_mensagem_pck.exibir_mensagem_abort(276588);
end if;

-- VERS�O PTU 5.0a
if	(ie_versao_p in ('04','05','06')) then
	pls_importar_arquivo_a560( ds_conteudo_p, nr_seq_camara_contest_p, nm_usuario_p, cd_estabelecimento_p );

-- VERS�O PTU 6.0
elsif	(ie_versao_p in ('07')) then
	pls_importar_arquivo_a560_60( ds_conteudo_p, nr_seq_camara_contest_p, nm_usuario_p, cd_estabelecimento_p );

-- VERS�O PTU 7.0
elsif	(ie_versao_p in ('08')) then
	pls_importar_arquivo_a560_70( ds_conteudo_p, nr_seq_camara_contest_p, nm_usuario_p, cd_estabelecimento_p );
	
-- VERS�O PTU 8.0
elsif	(ie_versao_p in ('09')) then
	pls_importar_arquivo_a560_80( ds_conteudo_p, nr_seq_camara_contest_p, nm_usuario_p, cd_estabelecimento_p );
end if;

commit;

end pls_imp_nota_debito_a560;
/