create or replace
procedure gerar_transf_estab_protocolo(
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2,
			nr_sequencia_p		number) is

nr_sequencia_w			number(10);
cd_estab_atende_w		number(4);
cd_pessoa_solicitante_w		varchar2(10);
cd_centro_custo_w			number(8);
cd_local_solicitante_w		number(4);
cd_local_atende_w			number(4);
nr_ordem_compra_w		number(10);
nr_item_oci_w			number(5);
cd_material_w			number(6);
cd_unidade_medida_estoque_w	varchar2(30);
ie_forma_w			varchar2(1);
qt_material_w			number(13,4);
ie_itens_w			varchar2(1);
cd_grupo_material_w		number(3);
cd_subgrupo_material_w		number(3);
cd_classe_material_w		number(5);

ie_saldo_maior_igual_min_w		varchar2(1);
ie_considera_pendencia_local_w	varchar2(1);
ie_controlado_w			varchar2(1);
ie_curva_abc_w			varchar2(1);
ie_nec_receita_w			varchar2(1);

qt_itens_desdobra_req_w		number(10) := 0;
qt_itens_gerados_w			number(10) := 0;

cd_comprador_w			number(10);
cd_condicao_pagamento_w		number(10);
qt_dias_entrega_w			varchar2(1);
dt_entrega_w			date;
cd_moeda_w			number(5);
cd_cgc_w			varchar2(14);
ds_ordens_geradas_w		varchar2(255);

cursor c01 is
select	a.nr_sequencia,
	a.cd_pessoa_solicitante,
	a.cd_local_solicitante,
	a.cd_estab_atende,
	a.cd_local_atende,
	a.ie_forma,
	nvl(a.ie_nec_receita,'A'),
	decode(nvl(ie_curva_abc_w,'T'),'T','X',ie_curva_abc_w),
	nvl(a.ie_saldo_maior_igual_min,'N'),
	nvl(a.ie_considera_pendencia_local,'N'),
	a.cd_grupo_material,
	a.cd_subgrupo_material,
	a.cd_classe_material			
from	regra_prot_transf_estab a
where	a.ie_situacao		= 'A'
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	(substr(obter_se_gera_solic_prot(cd_estabelecimento_p,nr_sequencia),1,1) = 'S')
and	(nvl(nr_sequencia_p, 0) = 0 or a.nr_sequencia = nr_sequencia_p);

cursor c02 is
select	cd_material,
	qt_material
from	regra_prot_tran_estab_item
where	nr_seq_protocolo = nr_sequencia_w;
					
begin
select	cd_comprador_padrao,
	cd_condicao_pagamento_padrao,
	cd_moeda_padrao
into	cd_comprador_w,
	cd_condicao_pagamento_w,
	cd_moeda_w
from	parametro_compras
where	cd_Estabelecimento = cd_estabelecimento_p;

open c01;
loop
fetch c01 into 
	nr_sequencia_w,
	cd_pessoa_solicitante_w,
	cd_local_solicitante_w,
	cd_estab_atende_w,
	cd_local_atende_w,
	ie_forma_w,
	ie_nec_receita_w,
	ie_curva_abc_w,
	ie_saldo_maior_igual_min_w,
	ie_considera_pendencia_local_w,
	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w;
exit when c01%notfound;
	begin
	if (ie_forma_w = 'R') then
		begin
		gravar_consulta_ordem_transf(
			cd_estabelecimento_p,
			cd_grupo_material_w,
			cd_subgrupo_material_w,
			cd_classe_material_w,
           		cd_local_solicitante_w,
	    		cd_local_atende_w,
			cd_estab_atende_w,
            		nm_usuario_p,
			'N',
			'N',
			'N',
			'N',
			ie_saldo_maior_igual_min_w,
			'N',
			ie_considera_pendencia_local_w,	
			ie_curva_abc_w,
			null,
			null,
			ie_nec_receita_w,
			'N',
			'N',
			null);          
			
		gravar_ordem_transf(
			cd_estabelecimento_p,
			cd_local_solicitante_w,
			cd_estab_atende_w,
			cd_local_atende_w,
			cd_pessoa_solicitante_w,
			nr_sequencia_w,
			nm_usuario_p,
			ds_ordens_geradas_w);
		end;
	else
		begin
		select	nvl(max(QT_ITENS_REQUISICAO),0)
		into	qt_itens_desdobra_req_w
		from 	parametro_estoque
		where	cd_estabelecimento	= cd_estab_atende_w;
		
		if	(nvl(cd_condicao_pagamento_w,0) = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(266037);
			--'N�o encontrado condi��o de pagamento, verifique os par�metros de compras ou o par�metro [2] da fun��o.'
		end if;

		if	(nvl(cd_comprador_w,0) = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(266038);
			--'N�o encontrado comprador, verifique os par�metros de compras.'
		end if;
		
		select	cd_cgc
		into	cd_cgc_w
		from	estabelecimento
		where	cd_estabelecimento = cd_estab_atende_w;

		select	ordem_compra_seq.nextval
		into	nr_ordem_compra_w
		from	dual;
		
		begin
		insert into ordem_compra(
			nr_ordem_compra,
			cd_estabelecimento,
			cd_condicao_pagamento,
			cd_comprador,
			dt_ordem_compra,
			dt_atualizacao,
			nm_usuario,
			cd_moeda,
			ie_situacao,
			dt_inclusao,
			cd_pessoa_solicitante,
			ie_frete,
			dt_entrega,
			ie_aviso_chegada,
			ie_emite_obs,
			ie_urgente,
			cd_cgc_fornecedor,
			ie_tipo_ordem,
			cd_local_entrega,
			cd_local_transf,
			cd_estab_transf,
			nr_seq_protocolo)
		values(	nr_ordem_compra_w,
			cd_estabelecimento_p,
			cd_condicao_pagamento_w,
			cd_comprador_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			cd_moeda_w,
			'A',
			sysdate,
			cd_pessoa_solicitante_w,
			'C',
			sysdate,
			'N',
			'N',
			'N',
			cd_cgc_w,
			'T',
			cd_local_solicitante_w,
			cd_local_atende_w,
			cd_estab_atende_w,
			nr_sequencia_w);
		exception when others then
			wheb_mensagem_pck.exibir_mensagem_abort(266039);
			--'erro ao gravar solicita��o de transfer�ncia'
		end;
		
		open c02;
		loop
		fetch c02 into 
			cd_material_w,
			qt_material_w;
		exit when c02%notfound;
			begin
			if	(qt_itens_desdobra_req_w > 0) and
				(qt_itens_gerados_w = qt_itens_desdobra_req_w) then
				begin
				select	ordem_compra_seq.nextval
				into	nr_ordem_compra_w
				from	dual;
				
				begin
				insert into ordem_compra(
					nr_ordem_compra,
					cd_estabelecimento,
					cd_condicao_pagamento,
					cd_comprador,
					dt_ordem_compra,
					dt_atualizacao,
					nm_usuario,
					cd_moeda,
					ie_situacao,
					dt_inclusao,
					cd_pessoa_solicitante,
					ie_frete,
					dt_entrega,
					ie_aviso_chegada,
					ie_emite_obs,
					ie_urgente,
					cd_cgc_fornecedor,
					ie_tipo_ordem,
					cd_local_entrega,
					cd_local_transf,
					cd_estab_transf,
					nr_seq_protocolo)
				values(	nr_ordem_compra_w,
					cd_estabelecimento_p,
					cd_condicao_pagamento_w,
					cd_comprador_w,
					sysdate,
					sysdate,
					nm_usuario_p,
					cd_moeda_w,
					'A',
					sysdate,
					cd_pessoa_solicitante_w,
					'C',
					sysdate,
					'N',
					'N',
					'N',
					cd_cgc_w,
					'T',
					cd_local_solicitante_w,
					cd_local_atende_w,
					cd_estab_atende_w,
					nr_sequencia_w);
				exception when others then
					wheb_mensagem_pck.exibir_mensagem_abort(266040);
					--'erro ao gravar solicita��o de transfer�ncia'
				end;
				qt_itens_gerados_w := 0;
				end;
			end if;
			
			select	(nvl(max(nr_item_oci),0) +1)
			into	nr_item_oci_w
			from	ordem_compra_item
			where	nr_ordem_compra = nr_ordem_compra_w;

			select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque
			into	cd_unidade_medida_estoque_w
			from	material
			where	cd_material = cd_material_w;
			
			insert into ordem_compra_item(
				nr_ordem_compra,
				nr_item_oci,
				cd_material,
				qt_material,
				qt_original,
				vl_unitario_material,
				dt_atualizacao,
				nm_usuario,
				cd_unidade_medida_compra,
				cd_local_estoque,
				ie_situacao,
				vl_total_item)
			values(	nr_ordem_compra_w,
				nr_item_oci_w,
				cd_material_w,
				nvl(qt_material_w,0),
				nvl(qt_material_w,0),
				0,
				sysdate,
				nm_usuario_p,
				cd_unidade_medida_estoque_w,
				cd_local_solicitante_w,
				'A',
				round((nvl(qt_material_w,0) * 0),4));	
			
			insert into ordem_compra_item_entrega(
				nr_ordem_compra,
				nr_item_oci,
				qt_prevista_entrega,
				dt_prevista_entrega,
				nm_usuario,
				dt_atualizacao,
				nr_sequencia)
			values (nr_ordem_compra_w,
				nr_item_oci_w,
				nvl(qt_material_w,0),
				sysdate,
				nm_usuario_p,
				sysdate,
				ordem_compra_item_entrega_seq.nextval);
			
			qt_itens_gerados_w := nvl(qt_itens_gerados_w,0) + 1;
			end;
		end loop;
		close c02;
		end;
	end if;
	update	ordem_compra
	set	ds_observacao		= substr(Wheb_mensagem_pck.get_Texto(302646, 'NR_SEQUENCIA_W='|| NR_SEQUENCIA_W),1,255) /*'Solicita��o de transfer�ncia gerada a partir do protocolo n� ' || NR_SEQUENCIA_W*/
	where	nr_seq_protocolo	= nr_sequencia_w;
	end;
end loop;
close c01;
commit;
end gerar_transf_estab_protocolo;
/
