create or replace
procedure cpoe_obter_setor_prescricao(
				ie_prescr_setor_p			varchar2,
				ie_setores_usuario_p		varchar2,
				cd_setor_atendimento_p		number,
				nr_atendimento_p			number,
				cd_perfil_p					number,
				nm_usuario_p				varchar2,
				cd_setor_atend_p			out	number) is

cd_setor_atendimento_w			setor_atendimento.cd_setor_atendimento%type;
ie_existe_setor_w				char(1);
nr_atendimento_mae_w			atendimento_paciente.nr_atendimento%type;
ie_usa_setor_mae_w				char(1) := 'N';
qt_setor_exibe_rn_w				number(5);
cd_setor_atendimento_mae_w		setor_atendimento.cd_setor_atendimento%type;
cd_setor_atendimento_filho_w	setor_atendimento.cd_setor_atendimento%type;
begin

if	(nm_usuario_p is not null) then
	select 	nvl(max(nr_atendimento_mae),0)
	into	nr_atendimento_mae_w
	from 	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;

	if (nr_atendimento_mae_w > 0) then
		select	count(cd_setor_atendimento)
		into	qt_setor_exibe_rn_w
		from	setor_atendimento
		where	ie_rn_mae = 'S'
		and		cd_estabelecimento = obter_estabelecimento_ativo;
	end if;

	if (qt_setor_exibe_rn_w > 0) then
		cd_setor_atendimento_w := nvl(obter_unidade_atendimento(nr_atendimento_mae_w,'IA','CS'),obter_unidade_atendimento(nr_atendimento_mae_w,'A','CS'));
		select	nvl(max(ie_rn_mae),'N')
		into	ie_usa_setor_mae_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_w;
	end if;
	
	if (ie_usa_setor_mae_w = 'N') then
		
		--Angular n�o vamos tratar este valor para o par�metro
		/*if	(ie_prescr_setor_p = 'C') then
			select	max(cd_setor_atendimento)
			into	cd_setor_atendimento_w
			from	computador
			where	nm_computador_pesquisa  = padronizar_nome(upper(nm_maquina_atual_p));			
		els*/
		if	(ie_prescr_setor_p = 'R') then
			cd_setor_atendimento_w := to_number(obter_setor_prescr_regra(cd_perfil_p));
		elsif	(ie_prescr_setor_p = 'U') and
			(ie_setores_usuario_p = 'S') then
			cd_setor_atendimento_w	:= cd_setor_atendimento_p;
		elsif	(ie_prescr_setor_p = 'U') and
			(nr_atendimento_p is not null) and
			(nr_atendimento_P > 0) then
			begin
			select	nvl(max('S'),'N')
			into	ie_existe_setor_w
			from	setor_atendimento a,
					atend_paciente_unidade b
			where	rownum = 1
			and		a.cd_setor_atendimento	= b.cd_setor_atendimento
			and		b.nr_atendimento	= nr_atendimento_p 
			and		b.cd_setor_atendimento	= cd_setor_atendimento_p;

			if	(ie_existe_setor_w = 'S') then
				cd_setor_atendimento_w	:= cd_setor_atendimento_p;
			end if;
			end;
		elsif	(ie_prescr_setor_p = 'A') and
				(nr_atendimento_p is not null) and
				(nr_atendimento_P > 0) then
				
			cd_setor_atendimento_w	:= nvl(obter_unidade_atendimento(nr_atendimento_p,'A','CS'), obter_unidade_atendimento(nr_atendimento_p,'IA','CS'));
			
		elsif	(nr_atendimento_p > 0) and
				(ie_prescr_setor_p <> 'B') then
				cd_setor_atendimento_w	:= nvl(obter_unidade_atendimento(nr_atendimento_p,'IA','CS'), obter_unidade_atendimento(nr_atendimento_p,'A','CS'));
		end if;
	elsif	(ie_prescr_setor_p = 'A') or (ie_prescr_setor_p = 'P') then
			begin
			if (nr_atendimento_mae_w > 0) then
				select	nvl(max(a.cd_setor_atendimento),0)
				into	cd_setor_atendimento_w
				from	setor_atendimento a
				where	a.cd_setor_atendimento = obter_unidade_atendimento(nr_atendimento_mae_w,'A','CS')
				and 	a.ie_rn_mae = 'S';
				
				if (cd_setor_atendimento_w is null) or (cd_setor_atendimento_w = 0) then
					cd_setor_atendimento_w	:= nvl(obter_unidade_atendimento(nr_atendimento_p,'IA','CS'), obter_unidade_atendimento(nr_atendimento_p,'A','CS'));
				end if;
			elsif	(ie_prescr_setor_p = 'A') then
				cd_setor_atendimento_w := obter_unidade_atendimento(nr_atendimento_p,'A','CS');	
			end if;		
			end;
	elsif	(ie_prescr_setor_p = 'D') then
			begin
				if (nr_atendimento_mae_w > 0) then
					
					select	nvl(max(cd_setor_atendimento),0) 
					into 	cd_setor_atendimento_mae_w
					from 	setor_atendimento
					where 	cd_setor_atendimento = obter_unidade_atendimento(nr_atendimento_mae_w,'A','CS')
					and 	ie_rn_mae = 'S';
						
					select	nvl(max(a.cd_setor_atendimento),0) 
					into 	cd_setor_atendimento_filho_w
					from 	unidade_atendimento a
					where 	a.nr_seq_interno = (select nvl(max(b.nr_seq_unidade_rn),0) 
												from setor_atendimento b
												where b.cd_setor_atendimento = obter_unidade_atendimento(nr_atendimento_p, 'A', 'CS'));
											
					if ((cd_setor_atendimento_mae_w > 0) and
						(cd_setor_atendimento_mae_w = cd_setor_atendimento_filho_w)) then
						cd_setor_atendimento_w := cd_setor_atendimento_mae_w;
					end if;	
				end if;
				
				if (((cd_setor_atendimento_mae_w is null) or (cd_setor_atendimento_mae_w = 0)) and
					(nr_atendimento_p > 0)) then
					cd_setor_atendimento_w := obter_unidade_atendimento(nr_atendimento_p,'A','CS');
				end if;
			end;	
	elsif	(nr_atendimento_p > 0) and
			(ie_prescr_setor_p <> 'B') then
			cd_setor_atendimento_w	:= nvl(obter_unidade_atendimento(nr_atendimento_p,'IA','CS'), obter_unidade_atendimento(nr_atendimento_p,'A','CS'));

	end if;
end if;

commit;

cd_setor_atend_p	:= cd_setor_atendimento_w;

end cpoe_obter_setor_prescricao;
/
