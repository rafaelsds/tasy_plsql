create or replace
procedure pls_vincular_mat_unimed_fed_sc
			(	cd_estabelecimento_p    number,
				nm_usuario_p            varchar2) is
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Vincular materiais/medicamentos da federe��o na PLS_MATERIAL
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 

Utilizado apenas em:
	- Fun��o OPS - Cadastro de Materiais, pasta Materiais Unimed >> Federa��o SC >> Materiais
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ds_material_w                   varchar2(500);
ds_mat_sem_acento_w		varchar2(255);
ds_medicamento_w		varchar2(255);
ds_nome_comercial_w		varchar2(255);
ds_observacao_w			varchar2(255);
cd_unidade_medida_w		varchar2(30);
cd_material_w			varchar2(20);
cd_simpro_w			varchar2(20);
cd_tiss_brasindice_w		varchar2(20);
ie_unidade_utilizacao_w		varchar2(10);
ie_fracionar_w			varchar2(10);
ie_mat_med_w			varchar2(5);
ie_opme_w			varchar2(5);
ie_tipo_despesa_w		varchar2(5);
ie_aplicar_conv_fed_sc_w	varchar2(5);
ie_tipo_fracao_w		varchar2(4);
ie_origem_w			varchar2(3);
cd_grupo_estoque_w		number(20);
cd_estrutura_w			number(15);
nr_seq_material_w		number(15);
nr_seq_mat_fed_sc_w		number(15);
qt_conversao_simpro_w		number(13,4)	:= null;
qt_conversao_bras_w		number(13,4)	:= null;
qt_conv_simpro_w		number(13,4);
qt_embalagem_w			number(10,4)	:= null;
qt_fracao_w			number(10,4)	:= null;
qt_registro_tot_w		number(10);
qt_registro_parte_w		number(10);
qt_registro_etapa_w		number(10);
qt_registro_perc_w		number(10);
qt_registro_perc_tot_w		number(10);
ie_possui_mat_w			number(10);
ie_simpro_valido_w		number(5);
ie_possui_regra_w		number(5);
ie_possui_simpro_w		number(5);
qt_reg_commit_w			number(5)	:= 0;
qt_conversao_bras_atual_w	pls_material.qt_conversao%type;
dt_inclusao_w			date;
dt_validade_anvisa_w		date;
dt_fora_linha_w			date;
dt_exclusao_w			date;
qt_conversao_bras_aux_w		pls_material.qt_conversao%type;
ds_versao_tiss_w		varchar2(20);

Cursor C01 is
	select	nr_sequencia,
		cd_material,
		cd_grupo_estoque,
		ie_tipo,
		nvl(ie_opme, 'N'),
		trim(ie_unidade_utilizacao),
		initcap(ds_material),
		cd_tiss,
		cd_simpro,
		ds_nome_comercial,
		ie_origem,
		substr(ds_observacao,1,255),
		dt_inclusao,
		nvl(qt_embalagem,1),
		nvl(qt_fracao,1),
		ie_fracionar,
		dt_validade_anvisa,
		ds_versao_tiss
	from	pls_mat_unimed_fed_sc
	where	ie_inconsistente	<> 'S';

Cursor C02 is
	select	ie_tipo_despesa	
	from	pls_regra_vinc_mat_uni_sc
	where	nvl(cd_grupo_estoque,nvl(cd_grupo_estoque_w,0)) = nvl(cd_grupo_estoque_w,0) 
	and	(ie_mat_med = ie_mat_med_w or ie_mat_med = 'A')
	and	nvl(ie_opme,ie_opme_w)	= ie_opme_w
	order by
		nvl(ie_mat_med, ''),
		nvl(cd_grupo_estoque, 0),
		nvl(ie_opme, '');

begin
dt_exclusao_w		:= null;
qt_registro_etapa_w	:= 0;
gravar_processo_longo('Vinculando materiais..','PLS_VINCULAR_MAT_UNIMED_FED_SC',0);

select	count(1)
into	qt_registro_tot_w
from	pls_mat_unimed_fed_sc
where	ie_inconsistente	<> 'S';
	
open C01;
loop
fetch C01 into	
	nr_seq_mat_fed_sc_w,
	cd_material_w,
	cd_grupo_estoque_w,
	ie_mat_med_w,
	ie_opme_w,
	ie_unidade_utilizacao_w,
	ds_material_w,
	cd_tiss_brasindice_w,
	cd_simpro_w,
	ds_nome_comercial_w,
	ie_origem_w,
	ds_observacao_w,
	dt_inclusao_w,
	qt_embalagem_w,
	qt_fracao_w,
	ie_fracionar_w,
	dt_validade_anvisa_w,
	ds_versao_tiss_w;
exit when C01%notfound;
	begin	
	ie_tipo_despesa_w	:= null;
	qt_registro_parte_w	:= qt_registro_tot_w / 20;
	
	open C02;
	loop
	fetch C02 into	
		ie_tipo_despesa_w;
	exit when C02%notfound;
		begin
		null;
		end;
	end loop;
	close C02;
	
	if	(ie_tipo_despesa_w is not null) then
		ds_mat_sem_acento_w	:= padronizar_nome(ds_material_w);

		
		-- Nome Medicamento 
		ds_medicamento_w	:= pls_obter_desc_mat_bras_tiss(cd_tiss_brasindice_w);

		-- Busca pela estrutura
		select  max(nr_sequencia)
		into	cd_estrutura_w
		from    pls_estrutura_material
		where   cd_externo	= to_char(cd_grupo_estoque_w);
	
		-- Tratamento de unidade de medida	
		select	max(cd_unidade_medida)
		into	cd_unidade_medida_w
		from	unidade_medida
		where	ie_situacao	= 'A'
		and	upper(cd_sistema_ant)	= upper(ie_unidade_utilizacao_w);
		
		if	(cd_unidade_medida_w is null) then
			select	max(cd_unidade_medida) 
			into	cd_unidade_medida_w
			from	unidade_medida
			where	ie_situacao	= 'A'
			and	upper(cd_unidade_medida)	= upper(ie_unidade_utilizacao_w);
		end if;
		
		if	(cd_tiss_brasindice_w is not null) then
			cd_tiss_brasindice_w	:= lpad(cd_tiss_brasindice_w, 10, '0');
		end if;

		-- Verifica se Existe o cadastro do simpro do produto
		select	count(1)
		into	ie_possui_simpro_w
		from	simpro_cadastro
		where	cd_simpro	= cd_simpro_w
		and	rownum 		= 1;		
		
		if	(ie_possui_simpro_w = 0) then
			cd_simpro_w	:= null;
		end if;
		
		if	(ds_versao_tiss_w is not null) then
			ie_possui_mat_w := 0;
			pls_obter_mat_tiss_vigente( ie_possui_mat_w, sysdate, cd_material_w, 'O', 'N', ds_versao_tiss_w);			
		else
			-- Verifica se existe o material
			select  count(1)
			into    ie_possui_mat_w	
			from    pls_material
			where   cd_material_ops	= cd_material_w
			and	rownum 		= 1;
		end if;
		
		-- Atualizar convers�es de Simpro e Brasindice
		qt_conversao_simpro_w	:= null;
		qt_conversao_bras_w	:= null;
		
		if	(ie_mat_med_w = 'MAT') and
			(cd_simpro_w is not null) then
			select	max(ie_tipo_fracao)
			into	ie_tipo_fracao_w
			from	simpro_preco
			where	cd_simpro	= cd_simpro_w;
			
			-- Material obedecer Simpro
			if	(ie_fracionar_w = 'S') then
				qt_conversao_simpro_w	:= qt_embalagem_w * qt_fracao_w;
			else
				qt_conversao_simpro_w	:= qt_embalagem_w;
			end if;
		elsif	(ie_mat_med_w = 'MED') and
			(cd_tiss_brasindice_w is not null) then
			-- Medicamento obedecer Bras�ndice
			if	(ie_fracionar_w = 'S') then
				qt_conversao_bras_w	:= qt_embalagem_w * qt_fracao_w;
			else
				qt_conversao_bras_w	:= qt_embalagem_w;
			end if;
			
		end if;
					
		if      (ie_possui_mat_w = 0) then
			select	pls_material_seq.nextval
			into	nr_seq_material_w
			from	dual;
			
			insert into pls_material
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_tipo_despesa,
				cd_estabelecimento,
				nr_seq_estrut_mat,
				cd_unidade_medida,
				ds_material,
				ds_material_sem_acento,
				ie_situacao,
				cd_simpro,
				cd_tiss_brasindice,
				cd_material_ops,
				ds_medicamento,
				ds_nome_comercial,
				ie_origem,
				ds_observacao,
				dt_inclusao,
				nr_seq_mat_uni_fed_sc,
				qt_conversao,
				dt_validade,
				dt_exclusao,
				ds_versao_tiss)
			values  (nr_seq_material_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ie_tipo_despesa_w,
				cd_estabelecimento_p,
				cd_estrutura_w,
				cd_unidade_medida_w,		
				ds_material_w,
				ds_mat_sem_acento_w,
				'A',
				cd_simpro_w,
				cd_tiss_brasindice_w,
				cd_material_w,
				ds_medicamento_w,
				ds_nome_comercial_w,
				ie_origem_w,
				ds_observacao_w,
				nvl(dt_inclusao_w, trunc(sysdate, 'dd')),
				nr_seq_mat_fed_sc_w,
				qt_conversao_bras_w,
				dt_validade_anvisa_w,
				dt_exclusao_w,
				ds_versao_tiss_w);
				
			-- (FED,N,FX)(Aplicar convers�o da Federa��o,N�o aplicar convers�o,Aplicar convers�o fixa)
			pls_obter_regra_conv_simpro(	nr_seq_material_w,
							ie_unidade_utilizacao_w,
							ie_tipo_fracao_w,
							sysdate,
							cd_estabelecimento_p,
							ie_aplicar_conv_fed_sc_w,
							qt_conv_simpro_w);
							
			if	(ie_aplicar_conv_fed_sc_w is null) then
				ie_aplicar_conv_fed_sc_w	:= 'FED';
			end if;
			
			if	(ie_aplicar_conv_fed_sc_w = 'SIM') then
				if	(cd_simpro_w is null) then
					qt_conversao_simpro_w	:= 0;
				else
					select	nvl(max(qt_embalagem),0)
					into	qt_conversao_simpro_w
					from	simpro_preco
					where	cd_simpro	= cd_simpro_w
					and	dt_vigencia	= (	select	max(dt_vigencia)
									from	simpro_preco
									where	cd_simpro	= cd_simpro_w);
				end if;
			end if;
		
			if	(ie_aplicar_conv_fed_sc_w = 'FX') then
				qt_conversao_simpro_w	:= qt_conv_simpro_w;
			end if;
			
			update	pls_material
			set	qt_conversao_simpro 	= qt_conversao_simpro_w
			where	nr_sequencia		= nr_seq_material_w;
				
			update	pls_mat_unimed_fed_sc
			set	dt_vinculo_mat	= sysdate
			where	nr_sequencia	= nr_seq_mat_fed_sc_w;
		else
			-- Pega a sequ�ncia do material na tabela PLS_MATERIAL
			select	/*+ INDEX(PLSMAT_PLSMUFS_FK_I)*/
				max(nr_sequencia)
			into	nr_seq_material_w
			from	pls_material
			where	nr_seq_mat_uni_fed_sc	= nr_seq_mat_fed_sc_w;
			
			if	(nr_seq_material_w is null) then
				select	max(nr_sequencia)
				into	nr_seq_material_w
				from	pls_material
				where	cd_material_ops	= cd_material_w;
			end if;
			
			select	max(qt_conversao)
			into	qt_conversao_bras_atual_w
			from	pls_material
			where	nr_sequencia	= nr_seq_material_w;
			
			-- (FED,N,FX)(Aplicar convers�o da Federa��o,N�o aplicar convers�o,Aplicar convers�o fixa)
			pls_obter_regra_conv_simpro(	nr_seq_material_w,
							ie_unidade_utilizacao_w,
							ie_tipo_fracao_w,
							sysdate,
							cd_estabelecimento_p,
							ie_aplicar_conv_fed_sc_w,
							qt_conv_simpro_w);
							
			if	(ie_aplicar_conv_fed_sc_w is null) then
				ie_aplicar_conv_fed_sc_w	:= 'FED';
			end if;
			
			if	(ie_aplicar_conv_fed_sc_w = 'SIM') then
				if	(cd_simpro_w is null) then
					qt_conversao_simpro_w	:= 0;
				else					
					select	nvl(max(qt_embalagem),0)
					into	qt_conversao_simpro_w
					from	simpro_preco
					where	cd_simpro	= cd_simpro_w
					and	dt_vigencia	= (	select	max(dt_vigencia)
									from	simpro_preco
									where	cd_simpro	= cd_simpro_w);
				end if;
			end if;
							
			if	(ie_aplicar_conv_fed_sc_w = 'FX') then
				qt_conversao_simpro_w	:= qt_conv_simpro_w;
			end if;
			
			/*aaschlote 27/03/2014 OS - 611174*/
			pls_obter_regra_conv_bras(nr_seq_material_w,sysdate,cd_estabelecimento_p,qt_conversao_bras_w,qt_conversao_bras_atual_w,qt_conversao_bras_aux_w);
			
			update	pls_material
			set	dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				ie_tipo_despesa		= ie_tipo_despesa_w,
				cd_estabelecimento	= cd_estabelecimento_p,
				nr_seq_estrut_mat	= cd_estrutura_w,
				cd_unidade_medida	= cd_unidade_medida_w,
				ds_material		= ds_material_w,
				ds_material_sem_acento	= ds_mat_sem_acento_w,
				cd_simpro		= cd_simpro_w,
				cd_tiss_brasindice	= cd_tiss_brasindice_w,
				ds_medicamento		= ds_medicamento_w,
				ds_nome_comercial	= ds_nome_comercial_w,
				ie_origem		= ie_origem_w,
				ds_observacao		= ds_observacao_w,
				qt_conversao		= qt_conversao_bras_aux_w,
				qt_conversao_simpro	= decode(ie_aplicar_conv_fed_sc_w, 'N', qt_conversao_simpro, qt_conversao_simpro_w),
				dt_validade		= dt_validade_anvisa_w,
				dt_exclusao		= nvl(dt_exclusao, dt_exclusao_w),
				nr_seq_mat_uni_fed_sc	= decode(nr_seq_mat_uni_fed_sc, null, nr_seq_mat_fed_sc_w, nr_seq_mat_uni_fed_sc),
				ds_versao_tiss		= ds_versao_tiss_w
			where	nr_sequencia		= nr_seq_material_w;
			
			update	pls_mat_unimed_fed_sc
			set	dt_vinculo_mat	= sysdate
			where	nr_sequencia	= nr_seq_mat_fed_sc_w
			and	dt_vinculo_mat is null;
		end if;			
	end if;
	
	-- Incrementa vari�vel para que a cada 100 registros, d� um commit
	qt_reg_commit_w	:= qt_reg_commit_w + 1;
	
	if	(qt_reg_commit_w = 300) then
		commit;
		qt_reg_commit_w	:= 0;
	end if;
	
	if	(qt_registro_etapa_w > qt_registro_parte_w) then
		qt_registro_etapa_w	:= 0;
		
		gravar_processo_longo('Vinculando materiais..','PLS_VINCULAR_MAT_UNIMED_FED_SC',-1);
	end if;
	
	qt_registro_etapa_w	:= qt_registro_etapa_w + 1;
	end;
end loop;
close C01;

commit;

end pls_vincular_mat_unimed_fed_sc;
/