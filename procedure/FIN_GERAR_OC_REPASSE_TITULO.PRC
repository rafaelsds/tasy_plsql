create or replace
procedure FIN_Gerar_OC_Repasse_Titulo(
				nr_seq_nf_p			Number,
				nr_seq_titulo_p			number,
				nm_usuario_p			varchar2) is


dt_emissao_w			Date;
dt_inicio_w			Date;
dt_final_w			Date;
vl_rat_w				Number(15,2);
nr_ordem_compra_w		Number(10,0);
ds_cliente_w			Varchar2(80);
ds_obs_w			Varchar2(255);
cd_comprador_w			Varchar2(10);
cd_cnpj_w			Varchar2(14);
cd_pessoa_fisica_w		Varchar2(10);
cd_cond_pagto_w			Number(15,0);	
dt_ordem_compra_w		Date;
dt_entrega_w			Date;
cd_moeda_w			Number(15,0);
cd_material_w			Number(06,0);
cd_unidade_w			Varchar2(30);
cd_local_estoque_w		Number(15,0);
cd_estabelecimento_w		Number(15,0);
vl_repasse_w			Number(15,2);
nr_sequencia_w			Number(10,0);
nr_seq_item_nf_w		Number(15,0);
nr_seq_item_oc_w		Number(15,0);
ds_complemento_w		Varchar2(255);
nr_seq_item_oci_w		number(10);
qt_parcelas_w			number(10);
nr_seq_item_venc_w		number(10);
dt_vencimento_w			date;

Cursor C01 is
	select	a.cd_pessoa_fisica,
		a.cd_cnpj,
		min(b.cd_cond_pagto)
	from	com_cli_regra_valor_rep b,
		nota_fiscal_item_rep a
	where	a.nr_seq_nf	= nr_seq_nf_p
	and	b.nr_sequencia	= a.nr_seq_regra_rep
	--and	a.nr_ordem_compra	is null
	and	((b.dt_vigencia is null) or
		((b.dt_vigencia is not null) and (trunc(b.dt_vigencia,'dd') >= trunc(sysdate,'dd'))))
	group by a.cd_pessoa_fisica,
		a.cd_cnpj;

Cursor C02 is
	select	a.nr_sequencia,
		a.nr_seq_item_nf,
		a.vl_repasse,
		b.cd_material,
		b.cd_cond_pagto
	from	com_cli_regra_valor_rep b,
		nota_fiscal_item_rep a
	where	a.nr_seq_nf	= nr_seq_nf_p
	and	b.nr_sequencia	= a.nr_seq_regra_rep
	and	((b.dt_vigencia is null) or
		((b.dt_vigencia is not null) and (trunc(b.dt_vigencia,'dd') >= trunc(sysdate,'dd'))))
	and	((cd_pessoa_fisica_w is null) or (a.cd_pessoa_fisica	= cd_pessoa_fisica_w))
	and	((cd_cnpj_w is null) or (a.cd_cnpj		= cd_cnpj_w));

BEGIN

select	cd_pessoa_fisica
into	cd_comprador_w
from	usuario
where	nm_usuario	= nm_usuario_p;

select	min(cd_moeda)
into	cd_moeda_w
from	moeda
where	cd_moeda_banco > 0;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	nota_fiscal
where	nr_sequencia	= nr_seq_nf_p;

select	count(*)
into	qt_parcelas_w
from	nota_fiscal_venc a
where	a.nr_sequencia = nr_seq_nf_p;

dt_emissao_w		:= trunc(sysdate,'dd');
dt_ordem_compra_w	:= sysdate;
dt_entrega_w		:= trunc(sysdate,'dd');

OPEN C01;
LOOP
FETCH C01 into
	cd_pessoa_fisica_w,
	cd_cnpj_w,
	cd_cond_pagto_w;
EXIT when C01%notfound;
	begin
	if	(cd_cond_pagto_w is null) then
		select	substr(obter_nome_pf_pj(a.cd_pessoa_fisica, a.cd_cgc),1,255)
		into	ds_complemento_w
		from	nota_fiscal a
		where	a.nr_sequencia	= nr_seq_nf_p;
		/*(-20011,'NF: ' || NR_SEQ_NF_P || ' Cliente: ' || DS_COMPLEMENTO_W || ' Falta condi��o  pagamento');*/		
		wheb_mensagem_pck.exibir_mensagem_abort(194510,	'NR_SEQ_NF_P='||NR_SEQ_NF_P||';'||
								'DS_COMPLEMENTO_P='||DS_COMPLEMENTO_W);
	end if;
	select	Ordem_Compra_seq.nextval
	into	nr_ordem_compra_w
	from	dual;

	insert	into ordem_compra(
		nr_ordem_compra,
		cd_estabelecimento,
 		cd_cgc_fornecedor,
		cd_pessoa_fisica,
 		cd_condicao_pagamento,
	 	cd_comprador,
 		dt_ordem_compra,
	 	dt_atualizacao,
	 	nm_usuario,
	 	cd_moeda,
	 	ie_situacao,
	 	dt_inclusao,
	 	cd_pessoa_solicitante,
	 	ie_frete,
		vl_despesa_acessoria,
	 	pr_multa_atraso,
	 	pr_desconto,
	 	pr_desc_pgto_antec,
	 	pr_juros_negociado,
	 	dt_entrega,
	 	ie_aviso_chegada,
		ie_emite_obs,
		ie_urgente,
		ie_somente_pagto,
		ds_observacao,
		nr_seq_nf_repasse,
		nr_titulo_receber)
	values (
		nr_ordem_compra_w,
		cd_estabelecimento_w,
		cd_cnpj_w,
		cd_pessoa_fisica_w,
		cd_cond_pagto_w,
		cd_comprador_w,
	      	dt_ordem_compra_w,
		sysdate,
		nm_usuario_p,
		cd_moeda_w, 
		'A',
		sysdate,
		cd_comprador_w,
		'N',
		0,
		0, 
		0,
		0,
		0,
		dt_entrega_w,
		'N',
		'N',
		'N',
		'N',
		ds_obs_w,
		nr_seq_nf_p,
		nr_seq_titulo_p);
		nr_seq_item_oc_w	:= 0;
	OPEN C02;
	LOOP
	FETCH C02 into
		nr_sequencia_w,
		nr_seq_item_nf_w,
		vl_repasse_w,
		cd_material_w,
		cd_cond_pagto_w;
	EXIT when C02%notfound;
		nr_seq_item_oc_w	:= nr_seq_item_oc_w + 1;
		vl_repasse_w := round(vl_repasse_w / qt_parcelas_w,2);
		select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMC'),1,30) cd_unidade_medida_compra
		into	cd_unidade_w
		from	material
		where	cd_material		= cd_material_w;

		select	substr(WHEB_MENSAGEM_PCK.get_texto(457652,'nr_nota_fiscal='||a.nr_nota_fiscal||';nm_pf_pj='||
            obter_nome_pf_pj(a.cd_pessoa_fisica, a.cd_cgc)||';ds_complemento='||b.ds_complemento),1,255)
		into	ds_complemento_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia	= nr_seq_nf_p	
		and	b.nr_sequencia	= nr_seq_nf_p
		and	b.nr_item_nf		= nr_seq_item_nf_w;

		begin
		select	nvl(max(nr_item_oci), 0) + 1
		into	nr_seq_item_oci_w
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_w;
		exception
			when no_data_found then
				nr_seq_item_oci_w := 1;
		end;

		insert into ordem_compra_item(
			nr_ordem_compra,
			nr_item_oci,
			cd_material,
			cd_unidade_medida_compra,
			vl_unitario_material,
			qt_material,
			qt_original,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			qt_material_entregue,
			pr_descontos,
			cd_local_estoque,
			ds_material_direto,
			vl_total_item)
		values(	nr_ordem_compra_w,
			nr_seq_item_oci_w,
			cd_material_w,
			cd_unidade_w,
			vl_repasse_w,
			1,
			1,
			sysdate,
			nm_usuario_p,
			'A',
			0,
			0,
			cd_local_estoque_w,
			ds_complemento_w,
			round((1 * vl_repasse_w),4));
		
		insert	into ordem_compra_item_entrega(
			nr_sequencia,
			nr_ordem_compra,
			nr_item_oci,
			dt_prevista_entrega,
			dt_entrega_original,
			dt_entrega_limite,
			qt_prevista_entrega,
			dt_atualizacao,
			nm_usuario)
		values(	ordem_compra_item_entrega_seq.nextval,
			nr_ordem_compra_w,
			nr_seq_item_oci_w,
			dt_entrega_w,
			dt_entrega_w,
			dt_entrega_w,
			1,
			sysdate,
			nm_usuario_p);
		
		update nota_fiscal_item_rep
		set	nr_ordem_compra	= nr_ordem_compra_w,
			nr_seq_item_oc	= nr_seq_item_oc_w
		where	nr_sequencia	= nr_sequencia_w;
	END LOOP;
	CLOSE C02;

	gerar_ordem_compra_venc( 
		nr_ordem_compra_w,
		nm_usuario_p);
		
	select	max(dt_vencimento)	
	into	dt_vencimento_w
	from	titulo_receber
	where	nr_titulo = nr_seq_titulo_p;
		
	select	max(nr_item_oc_venc)	
	into	nr_seq_item_venc_w
	from	ordem_compra_venc
	where	nr_ordem_compra = nr_ordem_compra_w;
	
	update	ordem_compra_venc
	set	dt_vencimento   = dt_vencimento_w
	where	nr_item_oc_venc = nr_seq_item_venc_w
	and	nr_ordem_compra = nr_ordem_compra_w;
		
	calcular_liquido_ordem_compra(
		nr_ordem_compra_w,
		nm_usuario_p);
	end;
END LOOP;
CLOSE C01;

END FIN_Gerar_OC_Repasse_Titulo;
/
