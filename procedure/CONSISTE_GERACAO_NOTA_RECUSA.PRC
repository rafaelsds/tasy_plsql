create or replace
procedure CONSISTE_GERACAO_NOTA_RECUSA(	nr_sequencia_p		number,
					nm_usuario_p		varchar2) is 

cd_oper_nf_corresp_w		operacao_nota.cd_oper_nf_corresp%type;
ie_operacao_fiscal_w		operacao_nota.ie_operacao_fiscal%type;
ie_nota_recusa_w		operacao_nota.ie_nota_recusa%type;
ie_recusa_w			operacao_nota.ie_recusa%type;
cd_cfop_inverso_w		natureza_operacao.cd_cfop_inverso%type;
ie_numero_nota_w		serie_nota_fiscal.ie_numero_nota%type;
qt_registros_w			number(10);
cd_estabelecimento_w		nota_fiscal.cd_estabelecimento%type;

begin

select	nvl(a.ie_nota_recusa,'N'),
	nvl(a.cd_oper_nf_corresp,0),
	nvl(a.ie_operacao_fiscal,'S'),
	b.cd_estabelecimento
into	ie_nota_recusa_w,
	cd_oper_nf_corresp_w,
	ie_operacao_fiscal_w,
	cd_estabelecimento_w
from	operacao_nota a,
	nota_fiscal b
where	a.cd_operacao_nf = b.cd_operacao_nf	
and	b.nr_sequencia = nr_sequencia_p;

if	(ie_nota_recusa_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(309285);/*A opera��o dessa nota fiscal n�o permite gerar nota de recusa. Verifique o cadastro da opera��o se o campo "Permite gerar nota de recusa" est� marcado.*/	
end if;

if	(cd_oper_nf_corresp_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(309279); /*Favor informar a "Opera��o correspondente" no cadastro da opera��o vinculada � esta nota fiscal.*/
end if;

if	(ie_operacao_fiscal_w <> 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(309284); /*Para gerar a nota de recusa, a opera��o dessa nota deve ser de Sa�da.*/
end if;

if	(cd_oper_nf_corresp_w > 0) then
	select	nvl(ie_operacao_fiscal,'E'),
		nvl(ie_recusa,'N')
	into	ie_operacao_fiscal_w,
		ie_recusa_w
	from	operacao_nota
	where	cd_operacao_nf = cd_oper_nf_corresp_w;
		
	if	(ie_operacao_fiscal_w <> 'E') then	
		wheb_mensagem_pck.exibir_mensagem_abort(309307); /*A opera��o de recusa deve ser de Entrada. Verifique o cadastro da mesma.*/
	end if;
	
	if	(ie_recusa_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(309309); /*Favor identificar no cadastro da opera��o de recusa que a mesma deve ser uma emiss�o pr�pria.*/
	end if;
end if;

select	nvl(max(a.cd_cfop_inverso),'0')
into	cd_cfop_inverso_w
from	natureza_operacao a,
	nota_fiscal b
where	a.cd_natureza_operacao = b.cd_natureza_operacao
and	b.nr_sequencia = nr_sequencia_p;

if	(cd_cfop_inverso_w = '0') then
	wheb_mensagem_pck.exibir_mensagem_abort(309310); /*Favor cadastrar o CFOP inverso no cadastro da natureza opera��o dessa nota fiscal. Essa informa��o � relevante para a gera��o da nota de recusa.*/
else
	select	count(*)
	into	qt_registros_w
	from	natureza_operacao
	where	cd_cfop = cd_cfop_inverso_w
	and	ie_situacao = 'A';
	
	if	(qt_registros_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(309312,'CD_CFOP_W='||cd_cfop_inverso_w); /*N�o existe nenhuma natureza opera��o com CFOP = #@CD_CFOP_W#@.*/
	end if;
end if;

select	nvl(max(a.ie_numero_nota),'M')
into	ie_numero_nota_w
from	serie_nota_fiscal a,
	nota_fiscal b
where	a.cd_serie_nf = b.cd_serie_nf
and	b.nr_sequencia = nr_sequencia_p
and	a.cd_estabelecimento = cd_estabelecimento_w;

if	(ie_numero_nota_w <> 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(309313);
	/*Favor identificar no cadastro da s�rie que a gera��o do n�mero da nota fiscal seja pelo "Sistema". Dessa forma, ao gerar a nota de recusa o sistema j� ir� gerar automaticamente o n�mero da nota fiscal de acordo com o �ltimo n�mero de nota existente para essa s�rie.*/
end if;

end consiste_geracao_nota_recusa;
/
