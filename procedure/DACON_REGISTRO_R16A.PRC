create or replace
procedure dacon_registro_r16a(
				nr_seq_dacon_p		number,
				nm_usuario_P		varchar2) is 
			
cd_conta_contabil_w	conta_contabil.cd_conta_contabil%type;
vl_debito_w		ctb_saldo.vl_debito%type;
vl_credito_w		ctb_saldo.vl_credito%type;
vl_saldo_w		ctb_saldo.vl_saldo%type;	
vl_movimento_w		ctb_saldo.vl_movimento%type;
ie_tipo_conta_w		varchar2(10);
ie_tipo_valor_w		varchar2(10); 			
dt_referencia_w		date;
vl_apuracao_w		number(15,2);
vl_receita_total_w	number(15,2);
vl_receita_nao_cum_w	number(15,2);
vl_receita_bens_w	number(15,2);	
vl_desp_energ_w		number(15,2);	
ds_arquivo_w		varchar2(2000);
vl_receita_bem_165_w	number(15,2);
vl_receita_bem_065_w	number(15,2);

Cursor C01 is
	select  nvl(cd_conta_contabil,'X'),
		nvl(ie_tipo_conta,'X'),
		nvl(ie_tipo_valor,'X')
	from    dacon_regra_conta_ctb
	where   nr_seq_dacon = nr_seq_dacon_p
	and	cd_registro  = 'R16A';

Cursor C02 is
	select	rpad('R16A',4,' ')						--01
		||lpad(to_number(to_char(dt_referencia, 'MM')),2,0)		--02
		||lpad(1,2,0) 							--03
		||lpad(0,14,0) 					--04 
		||lpad(0,14,0)							--05 
		||lpad(0,14,0)				--06
		||lpad(0,14,0)							--07 
		||lpad(0,14,0)					--08
		||lpad(0,14,0)							--09
		||lpad(0,14,0)				--10
		||lpad(0,14,0)							--11
		||lpad(0,14,0)							--12
		||lpad(0,14,0)							--13
		||lpad(0,14,0)							--14
		||lpad(0,14,0)							--15
		||lpad(0,14,0)							--16
		||lpad(0,14,0)							--17
		||lpad(0,14,0)							--18
		||lpad(0,14,0)							--19
		||lpad(0,14,0)							--20
		||lpad(0,14,0)							--21
		||lpad(0,14,0)							--22
		||lpad(0,14,0)							--23
		||lpad(0,14,0)							--24
		||lpad(0,14,0)							--25
		||lpad(0,14,0)							--26
		||lpad(0,14,0)							--27
		||lpad(0,14,0)							--28
		||lpad(0,14,0)							--29
		||lpad(0,14,0)							--30
		||lpad(0,14,0)							--31
		||lpad(0,14,0)							--32
		||lpad(0,14,0)							--33
		||lpad(0,14,0)							--34
		||lpad(0,14,0)							--35
		||lpad(0,14,0)							--36
		||lpad(0,38,0)							--37
		||lpad(0,10,0)							--38
	from	dacon
	where	nr_sequencia = nr_seq_dacon_p
	union all
	select	rpad('R16A',4,' ')				--01
		||lpad(to_number(to_char(dt_referencia, 'MM')),2,0) --02
		||lpad(2,2,0) --coluna,				--03	
		||lpad(0,14,0)					--04
		||lpad(0,14,0)			--05
		||lpad(0,14,0)					--06
		||lpad(0,14,0)			--07
		||lpad(0,14,0)					--08
		||lpad(0,14,0)					--09
		||lpad(0,14,0)					--10
		||lpad(0,14,0)					--11
		||lpad(0,14,0)					--12
		||lpad(0,14,0)					--13
		||lpad(0,14,0)					--14
		||lpad(0,14,0)					--15
		||lpad(0,14,0)					--16
		||lpad(0,14,0)					--17
		||lpad(0,14,0)					--18
		||lpad(0,14,0)					--19
		||lpad(0,14,0)					--20
		||lpad(0,14,0)					--21
		||lpad(0,14,0)					--22
		||lpad(0,14,0)					--23
		||lpad(0,14,0)					--24
		||lpad(0,14,0)					--25
		||lpad(0,14,0)					--26
		||lpad(0,14,0)					--27
		||lpad(0,14,0)					--28
		||lpad(0,14,0)					--29
		||lpad(0,14,0)					--30
		||lpad(0,14,0)					--31
		||lpad(0,14,0)					--32
		||lpad(0,14,0)					--33
		||lpad(0,14,0)					--34
		||lpad(0,14,0)					--35
		||lpad(0,14,0)					--36
		||lpad(0,38,0)					--37
		||lpad(0,10,0)					--38
	from	dacon
	where	nr_sequencia = nr_seq_dacon_p
	union all
	select	rpad('R16A',4,' ')				--01
		||lpad(to_number(to_char(dt_referencia, 'MM')),2,0) --02
		||lpad(3,2,0) --coluna,				--03	
		||lpad(0,14,0)					--04
		||lpad(0,14,0)			--05
		||lpad(0,14,0)					--06
		||lpad(0,14,0)			--07
		||lpad(0,14,0)					--08
		||lpad(0,14,0)					--09
		||lpad(0,14,0)					--10
		||lpad(0,14,0)					--11
		||lpad(0,14,0)					--12
		||lpad(0,14,0)					--13
		||lpad(0,14,0)					--14
		||lpad(0,14,0)					--15
		||lpad(0,14,0)					--16
		||lpad(0,14,0)					--17
		||lpad(0,14,0)					--18
		||lpad(0,14,0)					--19
		||lpad(0,14,0)					--20
		||lpad(0,14,0)					--21
		||lpad(0,14,0)					--22
		||lpad(0,14,0)					--23
		||lpad(0,14,0)					--24
		||lpad(0,14,0)					--25
		||lpad(0,14,0)					--26
		||lpad(0,14,0)					--27
		||lpad(0,14,0)					--28
		||lpad(0,14,0)					--29
		||lpad(0,14,0)					--30
		||lpad(0,14,0)					--31
		||lpad(0,14,0)					--32
		||lpad(0,14,0)					--33
		||lpad(0,14,0)					--34
		||lpad(0,14,0)					--35
		||lpad(0,14,0)					--36
		||lpad(0,38,0)					--37
		||lpad(0,10,0)					--38
	from	dacon
	where	nr_sequencia = nr_seq_dacon_p;
begin

select	dt_referencia
into	dt_referencia_w
from	dacon
where	nr_sequencia = nr_seq_dacon_p;

open C01;
loop
fetch C01 into	
	cd_conta_contabil_w,
	ie_tipo_conta_w,
	ie_tipo_valor_w;
exit when C01%notfound;
	begin
	
	vl_apuracao_w	:= 0;
	
	select sum(vl_debito),
	       sum(vl_credito),
	       sum(vl_saldo),
	       sum(vl_movimento)
	into   vl_debito_w,
	       vl_credito_w,
	       vl_saldo_w,
	       vl_movimento_w
	from   ctb_saldo
	where  cd_conta_contabil = cd_conta_contabil_w
	and	to_char(ctb_obter_mes_ref(nr_seq_mes_ref),'mm/yyyy') = to_char(dt_referencia_w,'mm/yyyy');
	
	--Verifica o tipo de valor a ser exportado; credito,debito,saldo,movimento.
	if 	(ie_tipo_valor_w = 'C')then
		vl_apuracao_w	:=	vl_credito_w;
	elsif	(ie_tipo_valor_w = 'D')then
		vl_apuracao_w	:=	vl_debito_w;
	elsif	(ie_tipo_valor_w = 'S')then
		vl_apuracao_w	:=	vl_saldo_w;
	elsif	(ie_tipo_valor_w = 'M')	then
		vl_apuracao_w	:=	vl_movimento_w;
	end if;
	
	--Verifica o tipo de conta, e insere os valores para cada tipo
	if	(ie_tipo_conta_w = 1)then
		vl_receita_total_w   :=  nvl(vl_receita_total_w,0)    + nvl(vl_apuracao_w,0);	
	elsif	(ie_tipo_conta_w = 2)then
		vl_receita_nao_cum_w :=  nvl(vl_receita_nao_cum_w,0)  + nvl(vl_apuracao_w,0);	
	elsif	(ie_tipo_conta_w = 3)	then
		vl_desp_energ_w	     :=	 nvl(vl_desp_energ_w,0)       + nvl(vl_apuracao_w,0);
	elsif 	(ie_tipo_conta_w in (4,5))	then
		vl_receita_bens_w    :=	 nvl(vl_receita_bens_w,0)     + nvl(vl_apuracao_w,0);
	end if;
	
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	ds_arquivo_w;
exit when C02%notfound;
	begin
	
	insert into w_dacon	(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,			
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_arquivo,
				nr_linha,
				ie_tipo_registro)
	values	   		(
				w_dacon_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_w,
				1, --'Alterar', buscar o nr_linhas passando como parametro out
				34);
	end;
end loop;
close C02;

commit;

end dacon_registro_r16a;
/