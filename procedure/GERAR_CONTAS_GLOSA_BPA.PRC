CREATE OR REPLACE
PROCEDURE Gerar_Contas_Glosa_Bpa
			(	nr_interno_conta_p	Number,
				nm_usuario_p		varchar2,
				nr_interno_conta_neg_p	out	Number,
				nr_interno_conta_pos_p	out	Number) is


nr_interno_conta_neg_w	Number(10,0);
nr_interno_conta_pos_w	Number(10,0);

BEGIN

select	Conta_Paciente_seq.nextval 
into	nr_interno_conta_neg_w
from	dual;

insert into Conta_Paciente(
	nr_atendimento, 
	dt_acerto_conta, 
	ie_status_acerto, 
	dt_periodo_inicial,
	dt_periodo_final,
	dt_atualizacao,
	nm_usuario, 
	cd_convenio_parametro,
	nr_protocolo, 
	dt_mesano_referencia,
	dt_mesano_contabil, 
	cd_convenio_calculo,
	cd_categoria_calculo,
	nr_interno_conta,
	nr_seq_protocolo,
	cd_categoria_parametro,
	ds_inconsistencia,
	dt_recalculo, 
	cd_estabelecimento, 
	vl_desconto, 
	vl_conta)
select	a.nr_atendimento,	
	a.dt_acerto_conta + (1/86400), 
	a.ie_status_acerto,
	a.dt_periodo_inicial,	
	a.dt_periodo_final, 
	sysdate,
	nm_usuario_p, 
	a.cd_convenio_parametro, 
	'0', 
	trunc(b.dt_ref_valida,'dd'),
	a.dt_mesano_contabil, 
	a.cd_convenio_calculo, 
	a.cd_categoria_calculo,
	nr_interno_conta_neg_w, 
	null, 
	a.cd_categoria_parametro,
	a.ds_inconsistencia, 
	a.dt_recalculo, 
	a.cd_estabelecimento, 
	a.vl_desconto, 
	a.vl_conta
from	Convenio b, 
	conta_paciente a
where	nr_interno_conta	= nr_interno_conta_p
and	b.cd_convenio		= a.cd_convenio_parametro;

select	Conta_Paciente_seq.nextval 
into	nr_interno_conta_pos_w
from	dual;

insert into Conta_Paciente(
	nr_atendimento, 
	dt_acerto_conta, 
	ie_status_acerto, 
	dt_periodo_inicial,
	dt_periodo_final, 
	dt_atualizacao, 
	nm_usuario, 
	cd_convenio_parametro,
	nr_protocolo, 
	dt_mesano_referencia, 
	dt_mesano_contabil, 
	cd_convenio_calculo,
	cd_categoria_calculo, 
	nr_interno_conta, 
	nr_seq_protocolo, 
	cd_categoria_parametro,
	ds_inconsistencia, 
	dt_recalculo, 
	cd_estabelecimento, 
	vl_desconto, 
	vl_conta)
select	a.nr_atendimento,
	a.dt_acerto_conta  + (2/86400),
	1, a.dt_periodo_inicial,
	a.dt_periodo_final, 
	sysdate, 
	nm_usuario_p, 
	a.cd_convenio_parametro,
	'0', 
	trunc(b.dt_ref_valida,'dd'), 
	a.dt_mesano_contabil, 
	a.cd_convenio_calculo,
	a.cd_categoria_calculo, 
	nr_interno_conta_pos_w, 
	null,
	a.cd_categoria_parametro, 
	a.ds_inconsistencia, 
	a.dt_recalculo,
	a.cd_estabelecimento, 
	vl_desconto, 
	vl_conta
from	Convenio b, 
	conta_paciente a
where	nr_interno_conta	= nr_interno_conta_p
and	b.cd_convenio		= a.cd_convenio_parametro;

commit;

nr_interno_conta_neg_p	:= nr_interno_conta_neg_w;
nr_interno_conta_pos_p	:= nr_interno_conta_pos_w;

END Gerar_Contas_Glosa_Bpa;
/