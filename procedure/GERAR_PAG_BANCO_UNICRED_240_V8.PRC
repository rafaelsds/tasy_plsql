create or replace
procedure gerar_pag_banco_unicred_240_v8(nr_seq_envio_p		number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p		varchar2) is 

-- Header do Arquivo
nr_seq_registro_w		varchar2(10) := 0;
ds_conteudo_w			varchar2(240);
nr_seq_arquivo_w		number(10)	:= 0;
nr_inscricao_w			varchar2(14);
cd_convenio_banco_w		varchar2(20);
cd_agencia_w			varchar2(5);
cd_conta_w			varchar2(12);
nr_digito_conta_w		varchar2(1);
nm_empresa_w			varchar2(30);
ds_banco_w			varchar2(30);
dt_arquivo_w			varchar2(8);
hr_arquivo_w			varchar2(6);
nm_pessoa_w			varchar2(30);
cd_pf_w  			varchar2(11);
cd_cnpj_w            varchar2(14);

-- Header do Lote
ie_forma_lancamento_w		varchar2(2);
nr_inscricao_lote_w		varchar2(14);
ds_endereco_w			varchar2(30);
nr_endereco_w			varchar2(5);
ds_complemento_w		varchar2(15);
ds_cidade_w			varchar2(20);
cd_cep_w			varchar2(8);
sg_estado_w			varchar2(15);
ds_ocorrencia_w			varchar2(10);
nr_lote_w			varchar2(4)	:= 0;
nr_seq_lote_a_w			varchar2(5)	:= 0;
nr_lote_servico_w	number(10);

-- Detalhe Documento - Segmento A
cd_compensacao_w		varchar2(3);
cd_banco_favorecido_w		varchar2(3);
cd_agencia_favorecido_w		varchar2(5);
ie_digito_agencia_w		varchar2(1);
nr_conta_favorecido_w		varchar2(12);
nr_digito_conta_hsbc_w       varchar2(2);
nr_digito_agencia_w		varchar2(1);
nm_favorecido_w			varchar2(30);
nr_documento_w			varchar2(20);
dt_pagamento_w			varchar2(8);
vl_pagamento_w			varchar2(15);
nr_documento_banco_w		varchar2(20);
dt_real_pagamento_w		varchar2(8);
vl_real_pagamento_w		varchar2(15);
cd_tipo_movimento_w		varchar2(1);
vl_pagamento_number_w		number(15,2);
ie_tipo_servico_w		varchar2(5);
ie_tipo_servico_arq_w		varchar2(2);

-- Detalhe Documento - Segmento B
ie_tipo_favorecido_w		varchar2(1);
cd_favorecido_w			varchar2(14);
ds_bairro_w			varchar2(15);
cd_cep_b_w			varchar2(8);
dt_vencimento_w			varchar2(8);
vl_documento_w			varchar2(15);
vl_liquidacao_w			varchar2(15);
vl_acrescimo_w			varchar2(15);
vl_desconto_w			varchar2(15);
vl_multa_w			varchar2(15);
nr_seq_lote_b_w			varchar2(5)	:= 1;
qt_cursor_w			number(1) 	:= 0;
vl_documento_number_w		number(15,2);

-- Trailer do Lote
nr_titulo_w			number(10);
vl_detalhes_w			number(15,2)	:= 0;
vl_total_detalhes_w		number(16,2)	:= 0;

-- Trailer do Arquivo
qt_registros_lote_w		number(10)	:= 0;
qt_registros_total_w		number(10)	:= 0;

-- Brancos
ds_brancos_9_w			varchar2(9);
ds_brancos_19_w			varchar2(19);
ds_brancos_20_w			varchar2(20);
ds_brancos_40_w			varchar2(40);
ds_brancos_8_w			varchar2(8);
ds_brancos_10_w			varchar2(10);
ds_brancos_3_w			varchar2(3);
ds_brancos_11_w			varchar2(11);
ds_brancos_211_w		varchar2(211);
ds_brancos_165_w		varchar2(165);
ds_brancos_54_w			varchar2(54);
ds_brancos_2_w			varchar2(2);
ds_brancos_15_w			varchar2(15);
ds_brancos_12_w			varchar2(12);
ds_brancos_6_w			varchar2(6);
ds_brancos_38_w			varchar2(38);
nr_sequencia_w			number(10);
cd_banco_w			varchar2(5);
nr_seq_lote_j_w			number(10);
nr_bloqueto_w			varchar2(44);
nr_nosso_numero_w		varchar2(20);
cd_moeda_w			varchar2(2);
cd_ocorrencia_w			varchar2(10);
ie_tipo_pagamento_w		varchar2(3);
qt_reg_ab_w			number(10) := 0;
qt_reg_j_w			number(10) := 0;
qt_reg_escrit_w			number(10) := 0;
qt_reg_total_w			number(10) := 0;
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
cd_pessoa_pf_pj_w		varchar2(255);
ie_favorecido_w			varchar(255);
ie_tipo_pagamento_ww		varchar2(3);
nr_conta_w			varchar2(20);
nr_lotes_w			number(6) := 0;
qt_cont_w			number(10) := 0;
cd_ocorrencia_pag_w		varchar2(2);
ie_ocorrencia_w			varchar2(1);

-- Detalhe Documento - Segmento A
Cursor C02 is
	select	d.nr_titulo nr_titulo,
		cd_camara_compensacao cd_compensacao,
		lpad(nvl(c.cd_banco,'0'),3,'0') cd_banco_favorecido,
		lpad(nvl(c.cd_agencia_bancaria,'0'),5,'0') cd_agencia_favorecido,
		nvl(substr(c.ie_digito_agencia,1,1),'0') ie_digito_agencia,
		lpad(nvl(c.nr_conta,'0'),12,'0') nr_conta_favorecido,
		nvl(decode(nvl(c.cd_banco,'0'),399,substr(c.ie_digito_conta,1,2),substr(c.ie_digito_conta,1,1)),'0')  nr_digito_conta,
		nvl(substr(c.ie_digito_agencia,1,1),'0') nr_digito_agencia,
		rpad(upper(substr(nvl(d.nm_favorecido,' '),1,30)),30,' ') nm_favorecido,
		d.nr_titulo nr_documento,
		to_char(a.dt_remessa_retorno, 'DDMMYYYY') dt_pagamento,
		replace(to_char(nvl(d.vl_saldo_titulo,0), 'fm0000000000000.00'),'.','') vl_pagamento,
		lpad(nvl(d.nr_documento,'0'),20,'0') nr_documento_banco,
		to_char(a.dt_remessa_retorno, 'DDMMYYYY') dt_real_pagamento,
		replace(to_char(nvl(d.vl_saldo_titulo,0), 'fm0000000000000.00'),'.','') vl_real_pagamento,
		lpad(nvl(decode(c.cd_ocorrencia_ret,null,'0',01,'0',02,'5',03,'9'),'0'),1,'0') cd_tipo_movimento,
		nvl(to_number(obter_dados_tit_escrit(c.nr_titulo,c.nr_seq_escrit,'VL')),0) vl_pagamento_number,
		c.ie_tipo_pagamento,
		d.cd_pessoa_pf_pj,
		d.ie_favorecido,
		c.nr_conta,
		c.cd_ocorrencia,
		c.ie_tipo_servico
	from	estabelecimento e,
		banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c
	where	c.nr_titulo		= d.nr_titulo
	and	a.nr_sequencia		= c.nr_seq_escrit
	and	a.cd_estabelecimento	= e.cd_estabelecimento
	and	b.nr_sequencia		= a.nr_seq_conta_banco
	and 	c.ie_tipo_pagamento 	in ('CC','CHQ','OP','DOC','TED')
	and	a.nr_sequencia		= nr_seq_envio_p;

-- Detalhe Documento - Segmento B
Cursor C03 is
	select	nvl(d.ie_tipo_favorecido,'0') ie_tipo_favorecido,
		lpad(nvl(d.cd_favorecido, ' '),14, '0') cd_favorecido,
		rpad(upper(nvl(substr(d.ds_endereco,1,30),' ')),30, ' ') ds_endereco,
		rpad(nvl(d.nr_endereco,'0'),5,' ') nr_endereco,
		rpad(upper(nvl(substr(d.ds_bairro,1,15),' ')),15,' ') ds_bairro,
		rpad(upper(nvl(substr(d.ds_cidade,1,20),' ')),20,' ') ds_cidade,
		lpad(nvl(d.cd_cep, ' '),8,' ') cd_cep,
		nvl(d.ds_estado,'  ') sg_estado,
		to_char(nvl(d.dt_vencimento_atual,sysdate),'DDMMYYYY') dt_vencimento,
		replace(to_char(nvl(d.vl_saldo_titulo,0), 'fm0000000000000.00'),'.','')	vl_documento,
		replace(to_char(nvl(c.vl_liquidacao,0), 'fm0000000000000.00'),'.','') vl_liquidacao,
		replace(to_char(nvl(c.vl_acrescimo,0), 'fm0000000000000.00'),'.','') vl_acrescimo,
		replace(to_char(nvl(c.vl_desconto,0), 'fm0000000000000.00'),'.','') vl_desconto,
		replace(to_char(nvl(c.vl_multa,0), 'fm0000000000000.00'),'.','') vl_multa,
		d.vl_saldo_titulo vl_documento_number
	from	estabelecimento e,
		banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c
	where	c.nr_titulo		= d.nr_titulo
	and	a.nr_sequencia		= c.nr_seq_escrit
	and	a.cd_estabelecimento	= e.cd_estabelecimento
	and	b.nr_sequencia		= a.nr_seq_conta_banco
	and 	c.ie_tipo_pagamento 	in ('CC','CHQ','OP','DOC','TED')
	and	a.nr_sequencia		= nr_seq_envio_p
	and	c.nr_titulo		= nr_titulo_w;

-- Detalhe T�tulo - Segmento J
Cursor C01 is
	select	nvl(d.ie_tipo_favorecido,'0') ie_tipo_favorecido,
		lpad(nvl(d.cd_favorecido, ' '),14, '0') cd_favorecido,
		rpad(upper(nvl(substr(d.ds_endereco,1,30),' ')),30, ' ') ds_endereco,
		rpad(nvl(d.nr_endereco,'0'),5,' ') nr_endereco,
		rpad(upper(nvl(substr(d.ds_bairro,1,15),' ')),15,' ') ds_bairro,
		rpad(upper(nvl(substr(d.ds_cidade,1,20),' ')),20,' ') ds_cidade,
		lpad(nvl(d.cd_cep, ' '),8,' ') cd_cep,
		nvl(d.ds_estado,'  ') sg_estado,
		to_char(nvl(d.dt_vencimento_atual,sysdate),'DDMMYYYY') dt_vencimento,
		replace(to_char(nvl(d.vl_saldo_titulo,0), 'fm0000000000000.00'),'.','')	vl_documento,
		replace(to_char(nvl(c.vl_liquidacao,0), 'fm0000000000000.00'),'.','') vl_liquidacao,
		replace(to_char(nvl(c.vl_acrescimo,0) + nvl(c.vl_multa,0), 'fm0000000000000.00'),'.','') vl_acrescimo,
		replace(to_char(nvl(c.vl_desconto,0), 'fm0000000000000.00'),'.','') vl_desconto,
		d.vl_saldo_titulo vl_documento_number,
		substr(d.nr_bloqueto,1,44) nr_bloqueto,
		rpad(upper(substr(nvl(d.nm_favorecido,' '),1,30)),30,' ') nm_favorecido,
		rpad(nvl(to_char(d.cd_pessoa_pf_pj),' '),20,' ') nr_documento,
		rpad(nvl(nvl(d.nr_nosso_numero,d.nr_documento),' '),20,' ') nr_nosso_numero,
		'09' cd_moeda,
		lpad(nvl(c.cd_ocorrencia,' '),10,' ') cd_ocorrencia,
		c.cd_ocorrencia
	from	estabelecimento e,
		banco_estabelecimento_v b,
		banco_escritural a,
		titulo_pagar_v2 d,
		titulo_pagar_escrit c
	where	c.nr_titulo		= d.nr_titulo
	and	a.nr_sequencia		= c.nr_seq_escrit
	and	a.cd_estabelecimento	= e.cd_estabelecimento
	and	b.nr_sequencia		= a.nr_seq_conta_banco
	and 	c.ie_tipo_pagamento 	= 'BLQ'
	and	a.nr_sequencia		= nr_seq_envio_p;

	
begin

delete from w_envio_banco
where nm_usuario = nm_usuario_p;

select	count(1)
into	qt_reg_ab_w
from	titulo_pagar_escrit c,
	banco_escritural a
where	a.nr_sequencia		= c.nr_seq_escrit
and 	c.ie_tipo_pagamento 	in ('CC','CHQ','OP','DOC','TED')
and	a.nr_sequencia		= nr_seq_envio_p
and	rownum = 1;

select	count(1)
into	qt_reg_j_w
from	titulo_pagar_escrit c,
	banco_escritural a
where	a.nr_sequencia		= c.nr_seq_escrit
and 	c.ie_tipo_pagamento 	= 'BLQ'
and	a.nr_sequencia		= nr_seq_envio_p
and	rownum = 1;


select	lpad(' ',9,' '),
	lpad(' ',19,' '),
	lpad(' ',20,' '),
	lpad(' ',40,' '),
	lpad(' ',8,' '),
	lpad(' ',10,' '),
	lpad(' ',3,' '),
	lpad(' ',11,' '),
	lpad(' ',211,' '),
	lpad(' ',165,' '),
	lpad(' ',54,' '),
	lpad(' ',2,' '),
	lpad(' ',15,' '),
	lpad(' ',12,' '),
	lpad(' ',6,' '),
	lpad(' ',38,' ')
into	ds_brancos_9_w,
	ds_brancos_19_w,
	ds_brancos_20_w,
	ds_brancos_40_w,
	ds_brancos_8_w,
	ds_brancos_10_w,
	ds_brancos_3_w,
	ds_brancos_11_w,
	ds_brancos_211_w,
	ds_brancos_165_w,
	ds_brancos_54_w,
	ds_brancos_2_w,
	ds_brancos_15_w,
	ds_brancos_12_w,
	ds_brancos_6_w,
	ds_brancos_38_w
from	dual;

-- Header do Arquivo
select	lpad(nvl(a.cd_cgc,'0'),14,'0') nr_inscricao,
	lpad(g.cd_convenio_banco,20,'0') cd_convenio_banco,
	lpad(somente_numero(nvl(g.cd_agencia_bancaria,0)),5,'0') cd_agencia,
	lpad(nvl(g.cd_conta,'0'),12,'0') cd_conta,
	nvl(substr(g.ie_digito_conta,1,1),' ') nr_digito_conta,
	rpad(nvl(substr(upper(p.ds_razao_social),1,30),' '),30,' ') nm_empresa,
	rpad(nvl(substr(upper(g.ds_banco),1,30),' '),30,' ')	ds_banco,
	to_char(sysdate,'DDMMYYYY') dt_arquivo,
	to_char(sysdate,'HH24MISS') hr_arquivo,
	e.cd_banco
into	nr_inscricao_w,
	cd_convenio_banco_w,
	cd_agencia_w,
	cd_conta_w,
	nr_digito_conta_w,
	nm_empresa_w,
	ds_banco_w,
	dt_arquivo_w,
	hr_arquivo_w,
	cd_banco_w
from	estabelecimento a,
	banco_estabelecimento_v g,
	pessoa_juridica p,
	banco_escritural e
where	e.cd_estabelecimento   	= a.cd_estabelecimento
and	a.cd_cgc		= p.cd_cgc
and	g.nr_sequencia		= e.nr_seq_conta_banco
and	e.nr_sequencia		= nr_seq_envio_p;

select	nvl(cd_banco_externo,cd_banco_w)
into	cd_banco_w
from	banco
where	cd_banco = cd_banco_w;

select	w_envio_banco_seq.nextval
into	nr_sequencia_w
from	dual;

nr_seq_arquivo_w := nr_seq_arquivo_w + 1;
qt_cont_w := qt_cont_w + 1;

ds_conteudo_w :=	lpad(cd_banco_w,3,'0') || 
					'0000' || 
					'0' || 
					ds_brancos_9_w || 
					'2' || 
					nr_inscricao_w || 
					cd_convenio_banco_w ||
					cd_agencia_w || 
					'2' || 
					cd_conta_w || 
					nr_digito_conta_w || 
					' ' || 
					nm_empresa_w || 
					ds_banco_w || 
					ds_brancos_10_w || 
					'1' ||
					dt_arquivo_w || 
					hr_arquivo_w || 
					lpad(nr_seq_arquivo_w,6,'0') || 
					'084' || 
					'00000' || 
					ds_brancos_54_w || 
					'   ' ||
					ds_brancos_12_w;

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		qt_cont_w);

qt_registros_total_w := qt_registros_total_w + 1;
-- Fim - Header do Arquivo

-- Segmentos A e B
if	(qt_reg_ab_w > 0) then
	nr_lotes_w := nr_lotes_w + 1;

	-- Header do Lote
	select	'01' ie_forma_lancamento,
		lpad(nvl(e.cd_cgc,'0'),14,'0') nr_inscricao,
		lpad(b.cd_convenio_banco,20,'0') cd_convenio_banco,
		lpad(somente_numero(nvl(b.cd_agencia_bancaria,0)),5,'0') cd_agencia,
		lpad(to_number(nvl(b.cd_conta,0)),12,'0') cd_conta,
		nvl(substr(b.ie_digito_conta,1,1),' ') nr_digito_conta,
		rpad(nvl(substr(upper(j.ds_razao_social),1,30),' '),30,' ') nm_empresa,
		rpad(nvl(substr(upper(j.ds_endereco),1,30),' '),30,' ') ds_endereco,
		lpad(nvl(substr(j.nr_endereco,1,5),' '),5,'0') nr_endereco,
		rpad(nvl(substr(j.ds_complemento,1,15), ' '),15,' ') ds_complemento,
		rpad(nvl(substr(j.ds_municipio,1,20), ' '),20,' ') ds_cidade,
		rpad(nvl(substr(somente_numero(j.cd_cep),1,8),' '),8,' ') cd_cep,
		nvl(j.sg_estado,'  ') sg_estado
	into	ie_forma_lancamento_w,
		nr_inscricao_lote_w,
		cd_convenio_banco_w,
		cd_agencia_w,
		cd_conta_w,
		nr_digito_conta_w,
		nm_empresa_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_complemento_w,
		ds_cidade_w,
		cd_cep_w,
		sg_estado_w
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		banco_escritural a
	where	e.cd_cgc		= j.cd_cgc
	and	a.cd_estabelecimento    = e.cd_estabelecimento
	and	b.nr_sequencia		= a.nr_seq_conta_banco
	and	a.nr_sequencia		= nr_seq_envio_p;


	select	w_envio_banco_seq.nextval
	into	nr_sequencia_w
	from	dual;

	nr_lote_w := nr_lote_w + 1;
	qt_cont_w := qt_cont_w + 1;
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;

	ds_conteudo_w :=	lpad(cd_banco_w,3,'0') || -- 1 a 3
						lpad(nr_lote_servico_w,4,'0') || --4 a 7
						'1' || --8
						'R' || --9
						'01'|| -- 10 a 11
						'03'|| -- 12 a 13 Fixo para Unimed Blumenau
						'043' || --14 a 16
						' ' || --17
						'2' || --18
						nr_inscricao_lote_w ||
						cd_convenio_banco_w ||
						cd_agencia_w ||
						'2' ||
						cd_conta_w ||
						nr_digito_conta_w 
						|| ' ' ||
						nm_empresa_w || 
						ds_brancos_40_w || 
						ds_endereco_w || 
						nr_endereco_w || 
						ds_complemento_w || 
						ds_cidade_w || 
						cd_cep_w ||
						substr(sg_estado_w,1,2) || 
						ds_brancos_8_w || 
						ds_brancos_10_w;

	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			qt_cont_w);

	qt_registros_total_w 	:= qt_registros_total_w + 1;
	qt_registros_lote_w 	:= qt_registros_lote_w + 1;
	-- Fim - Header do Lote

	vl_total_detalhes_w := 0;
	vl_pagamento_number_w := 0;
	vl_detalhes_w := 0;

	-- Detalhe Documento - Segmento A
	open C02;
	loop
	fetch C02 into
		nr_titulo_w,
		cd_compensacao_w,
		cd_banco_favorecido_w,
		cd_agencia_favorecido_w,
		ie_digito_agencia_w,
		nr_conta_favorecido_w,
		nr_digito_conta_hsbc_w,
		nr_digito_agencia_w,
		nm_favorecido_w,
		nr_documento_w,
		dt_pagamento_w,
		vl_pagamento_w,
		nr_documento_banco_w,
		dt_real_pagamento_w,
		vl_real_pagamento_w,
		cd_tipo_movimento_w,
		vl_pagamento_number_w,
		ie_tipo_pagamento_w,
		cd_pessoa_pf_pj_w,
		ie_favorecido_w,
		nr_conta_w,
		cd_ocorrencia_pag_w,
		ie_tipo_servico_w;
	exit when C02%notfound;
		begin
		vl_total_detalhes_w	:= vl_total_detalhes_w + vl_pagamento_number_w;

		if	(ie_favorecido_w = '1') then
			cd_pessoa_fisica_w := null;
			cd_cgc_w := cd_pessoa_pf_pj_w;

		elsif	(ie_favorecido_w = '2') then
			cd_pessoa_fisica_w := cd_pessoa_pf_pj_w;
			cd_cgc_w := null;
		end if;

		ie_tipo_pagamento_ww := obter_tipo_pag_escrit_regra(vl_pagamento_number_w,cd_pessoa_fisica_w,cd_cgc_w,nr_conta_w);

		select	lpad(decode(ie_tipo_pagamento_ww,'CC','000',decode(ie_tipo_pagamento_ww,'DOC','700',
			decode(ie_tipo_pagamento_ww,'TED','018',' '))),3,' ')
		into	cd_compensacao_w
		from	dual;

		select	decode(cd_ocorrencia_pag_w,'01','0',decode(cd_ocorrencia_pag_w,'02','5',
			decode(cd_ocorrencia_pag_w,'03','9',' ')))
		into	ie_ocorrencia_w
		from	dual;

		nr_seq_registro_w := nr_seq_registro_w + 1;
		nr_seq_lote_a_w := nr_seq_lote_a_w + 1;
		qt_cont_w := qt_cont_w + 1;

		--00010           Pagamento Dividendos
		--00020           Pagamento Fornecedores
		--00030           Pagamento Sal�rios
		--00050           Pagamento Sinistros Segurados
		--00060           Pagamento Despesa Viajantes em Tr�nsito
		--00070           Pagamento Autorizados
		--00075           Pagamento Credenciados
		--00080           Pagamento Representantes/Vendedores Autorizados
		--00090           Pagamento de Benef�cios
		--00094           Pagamento de Arrecada��o (Prefeituras, �gua, Luz, etc.) com barras
		--00095           Telecomunica��es - Com c�digo de barras
		--00098           Pagamento Diversos

		case	ie_tipo_servico_w
			when	'00010'	then ie_tipo_servico_arq_w := '04';
			when	'00020'	then ie_tipo_servico_arq_w := '07';
			when 	'00030'	then ie_tipo_servico_arq_w := '06';
			when	'00050'	then ie_tipo_servico_arq_w := '13';
			when	'00060'	then ie_tipo_servico_arq_w := '02';
			when	'00070'	then ie_tipo_servico_arq_w := '13';
			when	'00075'	then ie_tipo_servico_arq_w := '13';
			when	'00080'	then ie_tipo_servico_arq_w := '13';
			when	'00090'	then ie_tipo_servico_arq_w := '16';
			when	'00094'	then ie_tipo_servico_arq_w := '09';
			when	'00095'	then ie_tipo_servico_arq_w := '13';
			when	'00098'	then ie_tipo_servico_arq_w := '13';
			else		ie_tipo_servico_arq_w := '13';
		end case;

		select cd_pessoa_fisica, cd_cgc
		into cd_pf_w,cd_cnpj_w
		from titulo_pagar
		where nr_titulo = nr_documento_w;

		if (cd_pf_w is not null) then
			select nr_cpf
			into nr_documento_w
			from pessoa_fisica
			where cd_pessoa_fisica = cd_pf_w;
		else
			nr_documento_w := cd_cgc_w;
		end if;
    


		nr_digito_agencia_w := ' ';
		if  ((cd_banco_favorecido_w = '399') and (length(nr_digito_conta_hsbc_w) > 1)) then
			nr_digito_agencia_w := substr(nr_digito_conta_hsbc_w,2,1);
			nr_digito_conta_hsbc_w := substr(nr_digito_conta_hsbc_w,1,1); 
		else
			nr_digito_agencia_w := ' ';    
		end if;

		ds_conteudo_w :=	lpad(cd_banco_w,3,'0') || /*1 a 3*/
							lpad(nr_lote_servico_w,4,'0') || /*4 a 7*/
							'3' || /*8*/
							lpad(nr_seq_registro_w,5,'0') || --9 a 13
							'A' || --14
							ie_ocorrencia_w || -- 15
							'00' || --16 a 17
							cd_compensacao_w || --18 a 20
							cd_banco_favorecido_w || --21 a 23
							cd_agencia_favorecido_w || --24 a 28
							' '/*ie_digito_agencia_w*/ || --29
							nr_conta_favorecido_w || --30 a 41
							nr_digito_conta_hsbc_w || --42
							nr_digito_agencia_w ||  -- 43
							nm_favorecido_w || --44 a 73
							rpad(nvl(to_char(nr_documento_w),' '),20,' ') || --74 a 93
							dt_real_pagamento_w || --94 a 101
							'BRL' ||
							'000000000000000' ||
							rpad(replace(to_char(nvl(vl_pagamento_number_w,'0'), 'fm0000000000000.00'),'.',''),15,'0') ||
							ds_brancos_20_w ||
							'00000000' ||
							'000000000000000' ||
							ds_brancos_40_w ||
							ie_tipo_servico_arq_w ||
							ds_brancos_10_w ||
							'5' ||
							ds_brancos_10_w;

		select	w_envio_banco_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into w_envio_banco
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
		values	(	nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				qt_cont_w);

		qt_registros_lote_w	:= qt_registros_lote_w + 1;
		qt_registros_total_w 	:= qt_registros_total_w + 1;

		-- Detalhe Documento - Segmento B
		open C03;
		loop
		fetch C03 into
			ie_tipo_favorecido_w,
			cd_favorecido_w,
			ds_endereco_w,
			nr_endereco_w,
			ds_bairro_w,
			ds_cidade_w,
			cd_cep_b_w,
			sg_estado_w,
			dt_vencimento_w,
			vl_documento_w,
			vl_liquidacao_w,
			vl_acrescimo_w,
			vl_desconto_w,
			vl_multa_w,
			vl_documento_number_w;
		exit when C03%notfound;
			begin
			select	w_envio_banco_seq.nextval
			into	nr_sequencia_w
			from	dual;

			nr_seq_registro_w := nr_seq_registro_w + 1;
			qt_cont_w := qt_cont_w + 1;

			ds_conteudo_w :=	lpad(cd_banco_w,3,'0') || lpad(nr_lote_servico_w,4,'0') || '3' || lpad(nr_seq_registro_w,5,'0') ||
						'B' || ds_brancos_3_w || ie_tipo_favorecido_w || cd_favorecido_w || ds_endereco_w ||
						nr_endereco_w ||ds_brancos_15_w || ds_bairro_w || ds_cidade_w || cd_cep_b_w || substr(sg_estado_w,1,2) ||
						dt_vencimento_w ||vl_documento_w || vl_liquidacao_w || vl_desconto_w || vl_acrescimo_w ||
						vl_multa_w ||rpad(trim(nr_documento_banco_w),15,' ') || '5' || '000000' || ds_brancos_8_w;

			insert into w_envio_banco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
			values	(nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				qt_cont_w);

			qt_registros_lote_w	:= qt_registros_lote_w + 1;
			qt_registros_total_w 	:= qt_registros_total_w + 1;
			nr_seq_lote_b_w := nr_seq_lote_b_w + 1;
			end;
		end loop;
		close C03;
		-- Fim - Detalhe Documento - Segmento B
		end;
	end loop;
	close C02;
	-- Fim - Detalhe Documento - Segmento A

	-- Trailler do Lote
	select	w_envio_banco_seq.nextval
	into	nr_sequencia_w
	from	dual;

	qt_registros_lote_w	:= qt_registros_lote_w + 1;
	qt_registros_total_w 	:= qt_registros_total_w + 1;
	qt_cont_w := qt_cont_w + 1;

	ds_conteudo_w := 	lpad(cd_banco_w,3,'0') || 
						lpad(nr_lote_servico_w,4, '0') || 
						'5' || ds_brancos_9_w || 
						lpad(qt_registros_lote_w,6,'0') ||
						replace(to_char(nvl(vl_total_detalhes_w,0), 'fm00000000000000.00'),'.','') ||
						replace(to_char(nvl(vl_detalhes_w,0), 'fm0000000000000.00000'),'.','') || 
						ds_brancos_6_w || 
						ds_brancos_165_w ||
						ds_brancos_10_w;

	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		qt_cont_w);
	-- Fim - Trailler do Lote
end if;

qt_registros_lote_w := 0;

-- Segmentos J
if	(qt_reg_j_w > 0) then
	nr_lotes_w := nr_lotes_w + 1;

	select	'01' ie_forma_lancamento,
		lpad(nvl(e.cd_cgc,'0'),14,'0') nr_inscricao,
		lpad(b.cd_convenio_banco,20,'0') cd_convenio_banco,
		lpad(somente_numero(nvl(b.cd_agencia_bancaria,0)),5,'0') cd_agencia,
		lpad(to_number(nvl(b.cd_conta,0)),12,'0') cd_conta,
		nvl(substr(b.ie_digito_conta,1,1),' ') nr_digito_conta,
		rpad(nvl(substr(upper(j.ds_razao_social),1,30),' '),30,' ') nm_empresa,
		rpad(nvl(substr(upper(j.ds_endereco),1,30),' '),30,' ') ds_endereco,
		lpad(nvl(substr(j.nr_endereco,1,5),' '),5,'0') nr_endereco,
		rpad(nvl(substr(j.ds_complemento,1,15), ' '),15,' ') ds_complemento,
		rpad(nvl(substr(j.ds_municipio,1,20), ' '),20,' ') ds_cidade,
		rpad(nvl(substr(somente_numero(j.cd_cep),1,8),' '),8,' ') cd_cep,
		nvl(j.sg_estado,'  ') sg_estado
	into	ie_forma_lancamento_w,
		nr_inscricao_lote_w,
		cd_convenio_banco_w,
		cd_agencia_w,
		cd_conta_w,
		nr_digito_conta_w,
		nm_empresa_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_complemento_w,
		ds_cidade_w,
		cd_cep_w,
		sg_estado_w
	from	pessoa_juridica j,
		estabelecimento e,
		banco_estabelecimento_v b,
		banco_escritural a
	where	e.cd_cgc		= j.cd_cgc
	and	a.cd_estabelecimento    = e.cd_estabelecimento
	and	b.nr_sequencia		= a.nr_seq_conta_banco
	and	a.nr_sequencia		= nr_seq_envio_p;

	select	w_envio_banco_seq.nextval
	into	nr_sequencia_w
	from	dual;

	nr_lote_w := nr_lote_w + 1;
	qt_cont_w := qt_cont_w + 1;
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;

	ds_conteudo_w :=	lpad(cd_banco_w,3,'0') || 
						lpad(nr_lote_servico_w,4,'0')  || 
						'1' || 
						'C' || 
						'20'|| 
						'01' || 
						'043' || 
						' ' || 
						'2' ||
						nr_inscricao_lote_w || 
						cd_convenio_banco_w || 
						cd_agencia_w || 
						'2' || 
						cd_conta_w || 
						nr_digito_conta_w || 
						' ' ||
						nm_empresa_w || 
						ds_brancos_40_w || 
						ds_endereco_w || 
						nr_endereco_w || 
						ds_complemento_w || 
						ds_cidade_w || 
						cd_cep_w ||
						substr(sg_estado_w,1,2) || 
						ds_brancos_8_w || 
						ds_brancos_10_w;

	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			qt_cont_w);

	qt_registros_total_w 	:= qt_registros_total_w + 1;
	qt_registros_lote_w 	:= qt_registros_lote_w + 1;
	-- Fim - Header do Lote

	vl_total_detalhes_w := 0;
	vl_documento_number_w := 0;
	vl_detalhes_w := 0;

	-- Detalhe Documento - Segmento J
	open C01;
	loop
	fetch C01 into
		ie_tipo_favorecido_w,
		cd_favorecido_w,
		ds_endereco_w,
		nr_endereco_w,
		ds_bairro_w,
		ds_cidade_w,
		cd_cep_b_w,
		sg_estado_w,
		dt_vencimento_w,
		vl_documento_w,
		vl_liquidacao_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_documento_number_w,
		nr_bloqueto_w,
		nm_favorecido_w,
		nr_documento_w,
		nr_nosso_numero_w,
		cd_moeda_w,
		cd_ocorrencia_w,
		cd_ocorrencia_pag_w;
	exit when C01%notfound;
		begin
		vl_total_detalhes_w	:= vl_total_detalhes_w + vl_documento_number_w;

		select	decode(cd_ocorrencia_pag_w,'01','0',decode(cd_ocorrencia_pag_w,'02','5',
			decode(cd_ocorrencia_pag_w,'03','9',' ')))
		into	ie_ocorrencia_w
		from	dual;

		select	w_envio_banco_seq.nextval
		into	nr_sequencia_w
		from	dual;

		nr_seq_registro_w := nr_seq_registro_w + 1;
		qt_cont_w := qt_cont_w + 1;

		ds_conteudo_w :=	lpad(cd_banco_w,3,'0') || 
							lpad(nr_lote_servico_w,4,'0') || 
							'3' || 
							lpad(nr_seq_registro_w,5,'0') ||
							'J' || 
							ie_ocorrencia_w || 
							'19' || 
							lpad(nvl(nr_bloqueto_w,' '),44,' ') || 
							nm_favorecido_w ||
							dt_vencimento_w || 
							vl_documento_w || 
							vl_desconto_w || 
							vl_acrescimo_w || 
							ds_brancos_38_w ||
							nr_documento_w || 
							nr_nosso_numero_w || 
							cd_moeda_w || 
							ds_brancos_6_w || 
							cd_ocorrencia_w;

		insert into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
		values	(nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			qt_cont_w);


		qt_registros_total_w 	:= qt_registros_total_w + 1;
		qt_registros_lote_w 	:= qt_registros_lote_w + 1;
		nr_seq_lote_j_w := nr_seq_lote_j_w + 1;
		end;
	end loop;
	close C01;
	-- Fim - Detalhe Documento - Segmento J

	-- Trailler do Lote
	select	w_envio_banco_seq.nextval
	into	nr_sequencia_w
	from	dual;

	qt_registros_lote_w := qt_registros_lote_w + 1;
	qt_registros_total_w := qt_registros_total_w + 1;
	qt_cont_w := qt_cont_w + 1;

	ds_conteudo_w := 	lpad(cd_banco_w,3,'0') || 
						lpad(nr_lote_servico_w,4, '0') || 
						'5' || 
						ds_brancos_9_w || 
						lpad(qt_registros_lote_w,6,'0') ||
						replace(to_char(nvl(vl_total_detalhes_w,0), 'fm0000000000000000.00'),'.','') ||
						replace(to_char(nvl(vl_detalhes_w,0), 'fm0000000000000.00000'),'.','') || 
						ds_brancos_6_w || 
						ds_brancos_165_w ||
						ds_brancos_10_w;

	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		qt_cont_w);
	-- Fim - Trailler do Lote
end if;

-- Trailer do Arquivo
select	w_envio_banco_seq.nextval
into	nr_sequencia_w
from	dual;


qt_registros_total_w := qt_registros_total_w + 1;
qt_cont_w := qt_cont_w + 1;

ds_conteudo_w := 	lpad(cd_banco_w,3,'0') || 
					'9999' || 
					'9' || 
					ds_brancos_9_w || 
					lpad(nr_lotes_w,6,'0') || 
					lpad(qt_registros_total_w,6,'0') ||
					ds_brancos_211_w;

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		qt_cont_w);
-- Fim - Trailer do Arquivo

commit;

end gerar_pag_banco_unicred_240_v8;
/
