create or replace
procedure gerar_resumo_avaliacao_oft( nr_sequencia_p		number,
						  nm_usuario_p		Varchar2,
						  ds_resumo_p		out	long) is 

nr_seq_item_w		number(10);
ds_item_w		varchar2(32000);
ds_resultado_w		varchar2(32000);			
ds_conteudo_w		varchar2(32000);		
ie_resultado_w		varchar2(10);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_w		varchar2(10);
dt_avaliacao_w		date;
cd_evolucao_w		number(10);
ds_evolucao_w		varchar2(32000);
ie_gerar_resumo_w		varchar2(10);
ds_fonte_w		varchar2(100);
ds_tam_fonte_w		varchar2(10);
nr_tam_fonte_w		number(5,0);
ds_cabecalho_w		varchar2(32000);
ds_rodape_w		 varchar2(32000);
ie_sem_resposta_w		varchar2(1);
										
Cursor C01 is
select	c.nr_sequencia,
	nvl(ds_label_relat,c.ds_item) ds_item,
	c.IE_RESULTADO,
	nvl((select d.ds_resultado from med_item_avaliar_res d where d.nr_seq_item = c.nr_sequencia and obter_somente_numero(Aval(a.nr_sequencia,c.nr_sequencia)) = d.nr_Seq_res and c.cd_dominio is null),substr(Aval(a.nr_sequencia,c.nr_sequencia),1,4000)) ds_resultado
from	med_Avaliacao_paciente a,
	med_tipo_avaliacao b,
	med_item_avaliar c
where	b.nr_sequencia = c.nr_seq_tipo
and	a.nr_seq_tipo_avaliacao = b.nr_sequencia 
and	(nvl((select d.ds_resultado from med_item_avaliar_res d where d.nr_seq_item = c.nr_sequencia and obter_somente_numero(Aval(a.nr_sequencia,c.nr_sequencia)) = d.nr_Seq_res and c.cd_dominio is null),substr(Aval(a.nr_sequencia,c.nr_sequencia),1,4000)) is not null)
and	(nvl((select d.ds_resultado from med_item_avaliar_res d where d.nr_seq_item = c.nr_sequencia and obter_somente_numero(Aval(a.nr_sequencia,c.nr_sequencia)) = d.nr_Seq_res and c.cd_dominio is null),substr(Aval(a.nr_sequencia,c.nr_sequencia),1,4000)) <> 'N')
and	a.nr_sequencia = nr_sequencia_p
order by c.nr_seq_apresent;									

begin

select max(cd_pessoa_fisica),
	max(nr_atendimento),
	max(cd_medico),
	max(dt_avaliacao),
	max(ie_resumo_oftalmo)
into cd_pessoa_fisica_w,
	nr_atendimento_w,
	cd_medico_w,
	dt_avaliacao_w,
	ie_gerar_resumo_w
from	med_Avaliacao_paciente a,
	med_tipo_avaliacao b
where	b.nr_sequencia = a.nr_seq_tipo_avaliacao
and	a.nr_sequencia = nr_sequencia_p;


if	(ie_gerar_resumo_w	= 'S') then
	Obter_Param_Usuario(281, 5, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ds_fonte_w); 
	Obter_Param_Usuario(281, 6, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ds_tam_fonte_w);
	nr_tam_fonte_w	:= somente_numero(ds_tam_fonte_w)*2;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_item_w,
		ds_item_w,
		ie_resultado_w,
		ds_resultado_w;
	exit when C01%notfound;
		begin
		if	(ie_resultado_w in ('X','T')) then
			ds_conteudo_w	:= ds_conteudo_w||chr(13)||ds_item_w||chr(13)||chr(13);
		elsif	(ie_resultado_w = 'B') then
			if	(ds_resultado_w	= 'S') or
				(ds_resultado_w	= '1') then
				ds_resultado_w	:= wheb_mensagem_pck.get_texto(94754); -- Sim
			else
				ds_resultado_w:= wheb_mensagem_pck.get_texto(94755); -- N�o
			end if;
				
			ds_conteudo_w := ds_conteudo_w ||' '||ds_item_w||' : '||ds_resultado_w||chr(13);
		else
			ds_conteudo_w := ds_conteudo_w ||' '||ds_item_w||' : '||ds_resultado_w||chr(13);
		end if;
		end;
	end loop;
	close C01;
	ds_resumo_p	:= ds_conteudo_w;
	
end if;

commit;

end gerar_resumo_avaliacao_oft;
/