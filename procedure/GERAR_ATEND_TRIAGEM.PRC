create or replace procedure gerar_atend_triagem(
			nr_atendimento_p		number,
			nm_usuario_p			varchar2,
      ie_inicio_triagem_p varchar2 DEFAULT 'N'
      ) is 

cd_pessoa_fisica_w			varchar2(10);
ie_clinica_w				varchar2(5);
qt_horas_validade_w			number(10);
qt_reg_w				number(10);
dt_entrada_w				Date;
dt_inicio_triagem_w   Date := null;
nr_seq_pac_senha_fila_w			number(10);
nr_seq_fila_w				number(10);
nr_seq_queixa_w				number(10);
nr_seq_triagem_priori_w			number(10);
ie_tolife_w				    varchar2(1) := 'N';
nr_seq_classif_w			varchar2(10);
cd_procedencia_w			atendimento_paciente.cd_procedencia%type;

begin

select nvl(max('S'), 'N')
into   ie_tolife_w
from   to_life_log_importacao
where  nr_atendimento = nr_atendimento_p;

if(ie_inicio_triagem_p = 'GE') then
    dt_inicio_triagem_w := sysdate;
end if;

if (ie_tolife_w = 'N') then 

    qt_horas_validade_w		:= nvl(obter_valor_param_usuario(916, 512, Obter_Perfil_Ativo, nm_usuario_p, nvl( wheb_usuario_pck.get_cd_estabelecimento,0)),0);
    
    select	max(a.cd_pessoa_fisica),
        max(a.ie_clinica),
        max(a.dt_entrada),
        max(a.nr_seq_pac_senha_fila),
        max(a.NR_SEQ_QUEIXA),
        max(a.NR_SEQ_TRIAGEM_PRIORIDADE),
		max(a.CD_PROCEDENCIA)
    into	cd_pessoa_fisica_w,
        ie_clinica_w,
        dt_entrada_w,
        nr_seq_pac_senha_fila_w,
        nr_seq_queixa_w,
        nr_seq_triagem_priori_w,
		cd_procedencia_w
    from	atendimento_paciente a
    where	a.nr_atendimento = nr_atendimento_p
    and     not exists (select 	1
                from   	triagem_pronto_atend b
                    where 	 b.nr_atendimento = nr_atendimento_p);
                
    select 	count(*)
    into	qt_reg_w
    from 	triagem_pronto_atend 
    where 	cd_pessoa_fisica = cd_pessoa_fisica_w
    and	dt_atualizacao_nrec between sysdate - (qt_horas_validade_w / 24) and sysdate
    and	nr_atendimento is null;
    
    if	(nr_seq_queixa_w is not null) and
        (Obter_se_Motivo_atend_Lib(nr_seq_queixa_w) = 'N') then
        nr_seq_queixa_w := null;	
    end if;
    
    if	(nr_seq_pac_senha_fila_w is not null) then
        select	nvl(nr_seq_fila_senha,nr_seq_fila_senha_origem) nr_fila
        into	nr_seq_fila_w
        from	paciente_senha_fila
        where	nr_sequencia = nr_seq_pac_senha_fila_w;
    end if;
    
    if	(nr_atendimento_p is not null) and
        (cd_pessoa_fisica_w is not null) and 
        (qt_reg_w	= 0) then
		
		select OBTER_PRIORIDADE_TRIAGEM(nr_seq_queixa_w)
		into nr_seq_classif_w
		from dual;
        
        insert into triagem_pronto_atend	( nr_sequencia,
                            cd_pessoa_fisica,
                            ie_clinica,
                            nr_atendimento,
                            dt_inicio_triagem,
                            nm_usuario,
                            dt_atualizacao,
                            dt_atualizacao_nrec,
                            ie_status_paciente,
                            nr_seq_fila_senha,
                            nr_seq_pac_fila,
                            nr_seq_queixa,
                            NR_SEQ_TRIAGEM_PRIORIDADE,
							cd_procedencia,
			    nr_seq_classif,
			    nm_usuario_nrec)
                            values
                            ( triagem_pronto_atend_seq.nextval,
                            cd_pessoa_fisica_w,
                            ie_clinica_w,
                            nr_atendimento_p,
                            dt_inicio_triagem_w,
                            nm_usuario_p,
                            sysdate,
                            sysdate,
                            'P',
                            nr_seq_pac_senha_fila_w,
                            nr_seq_fila_w,
                            nr_seq_queixa_w,
                            nr_seq_triagem_priori_w,
							cd_procedencia_w,
			    decode(nr_seq_classif_w, 'N' , null, nr_seq_classif_w),
			    nm_usuario_p);

				GQA_GERAR_PROTOCOLO_ASSIST(nr_atendimento_p,10,nm_usuario_p);
    end if;
                            
    commit;
end if;

end gerar_atend_triagem;
/
