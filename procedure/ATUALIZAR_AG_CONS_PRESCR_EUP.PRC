create or replace
procedure Atualizar_Ag_Cons_Prescr_EUP	(	nr_prescricao_p	number,
						nr_seq_agenda_p	number,
						nm_usuario_p	varchar2) is

begin

update	prescr_medica
set	nr_seq_agenda	= nr_seq_agenda_p
where	nr_prescricao	= nr_prescricao_p;

commit;

end Atualizar_Ag_Cons_Prescr_EUP;
/