create or replace
procedure atualiza_preco_mat_opme_conv(	nr_seq_material_p	number,
				 	nm_usuario_p	varchar2) is

nr_seq_mat_autor_cir_w		number(10);
cd_material_w			number(08,0);
qt_material_w			number(15,4);
vl_preco_w			number(15,4);
cd_fornecedor_w			varchar2(14);
cd_cond_pagto_w			number(10,0);
ie_consignado_w			varchar2(1);
cd_estabelecimento_w		number(4);
cd_convenio_w			number(5);
nr_seq_agenda_w			number(10);
nr_atendimento_w		number(10);
cd_categoria_w			varchar2(10);
cd_plano_w			varchar2(10);
ie_tipo_atendimento_w		number(3);
cd_tipo_acomodacao_w		number(4);
cd_setor_atendimento_w		number(5);
cd_cgc_fornec_w			varchar2(14);
dt_ult_vigencia_w		date;
cd_tabela_preco_w		number(4);
ie_origem_preco_w		number(10);
qt_registro_w			number(10);
ie_usa_tabela_mat_especial_w	varchar2(1)	:= 'N';
nr_seq_bras_preco_w		number(10,0);
nr_seq_mat_bras_w		number(10,0);
nr_seq_conv_bras_w		number(10,0);
nr_seq_conv_simpro_w		number(10,0);
nr_seq_mat_simpro_w		number(10,0);
nr_seq_simpro_preco_w		number(10,0);
nr_seq_mat_ajuste_w		number(10,0);
nr_seq_classif_atend_w		atendimento_paciente.nr_seq_classificacao%type;

Cursor c01 is
select	nr_sequencia
from	material_autor_cirurgia
where	nr_sequencia = nr_seq_material_p;

Cursor c02 is
select	cd_cgc
from	material_autor_cir_cot
where	nr_sequencia	= nr_seq_mat_autor_cir_w;

begin

open c01;
loop
fetch c01 into
	nr_seq_mat_autor_cir_w;
exit when c01%notfound;

	select	a.cd_material,
		nvl(a.qt_solicitada,1),
		b.nr_seq_agenda,
		b.nr_atendimento,
		b.cd_estabelecimento
	into	cd_material_w,
		qt_material_w,
		nr_seq_agenda_w,
		nr_atendimento_w,
		cd_estabelecimento_w
	from	autorizacao_cirurgia b,
		material_autor_cirurgia a
	where	a.nr_seq_autorizacao	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_mat_autor_cir_w;

	if	(nr_atendimento_w is not null) then
		select	a.cd_convenio,
			a.cd_categoria,
			a.cd_plano_convenio,
			b.ie_tipo_atendimento,
			to_number(obter_tipo_acomod_atend(a.nr_atendimento,'C')),
			obter_setor_atendimento(a.nr_atendimento),
			b.nr_seq_classificacao
		into	cd_convenio_w,
			cd_categoria_w,
			cd_plano_w,
			ie_tipo_atendimento_w,
			cd_tipo_acomodacao_w,
			cd_setor_atendimento_w,
			nr_seq_classif_atend_w
		from	atendimento_paciente b,
			atend_categoria_convenio a
		where	a.nr_atendimento	= b.nr_atendimento
		and	a.nr_atendimento	= nr_atendimento_w
		and	a.nr_seq_interno	= obter_atecaco_atendimento(a.nr_atendimento);

	elsif	(nr_seq_agenda_w is not null) then

		select	cd_convenio,
			cd_categoria,
			cd_plano,
			ie_tipo_atendimento,
			cd_tipo_acomodacao,
			cd_setor_atendimento
		into	cd_convenio_w,
			cd_categoria_w,
			cd_plano_w,
			ie_tipo_atendimento_w,
			cd_tipo_acomodacao_w,
			cd_setor_atendimento_w
		from	agenda_paciente
		where	nr_sequencia	= nr_seq_agenda_w;

	end if;

	select	count(*)
	into	qt_registro_w
	from	convenio_preco_fornec
	where	cd_convenio	= cd_convenio_w;

	if	(qt_registro_w > 0) then
		ie_usa_tabela_mat_especial_w	:= 'S';
	else
		ie_usa_tabela_mat_especial_w	:= 'N';
	end if;

	open c02;
	loop
	fetch c02 into
		cd_cgc_fornec_w;
	exit when c02%notfound;

		if	(ie_usa_tabela_mat_especial_w = 'S') then
			obter_preco_conv_mat_fornec(cd_convenio_w,
						cd_cgc_fornec_w,
						cd_material_w,
						sysdate,
						vl_preco_w);
		else

			define_preco_material(	cd_estabelecimento_w,	-- cd_estabelecimento_p		number,
						cd_convenio_w,          -- cd_convenio_p          	number,
						cd_categoria_w,         -- cd_categoria_p         	varchar2,
						sysdate,                -- dt_vigencia_p          	date,
						cd_material_w,          -- cd_material_p          	number,
						cd_tipo_acomodacao_w,   -- cd_tipo_acomodacao_p   	number,
						ie_tipo_atendimento_w,  -- ie_tipo_atendimento_p  	number,
						cd_setor_atendimento_w, -- cd_setor_atendimento_p 	number,
						cd_cgc_fornec_w,        -- cd_cgc_fornecedor_p		varchar2,
						0,                      -- qt_idade_p			number,
						0,                      -- nr_sequencia_p		number,
						cd_plano_w,             -- cd_plano_p			varchar2,
						null,                   -- cd_proc_referencia_p		number,
						null,                   -- ie_origem_proc_p		number,
						null,                   -- nr_seq_marca_p		number,
						null,                   -- ie_clinica_p			number,
						nr_seq_classif_atend_w, -- nr_seq_classif_atend_p	number,
						nr_atendimento_w,	-- nr_atendimento_p		number,
						null,                   -- ie_vago_4_p			varchar2,
						vl_preco_w,             -- vl_material_p      	 out 	number,
						dt_ult_vigencia_w,      -- dt_ult_vigencia_p  	 out 	date,
						cd_tabela_preco_w,      -- cd_tab_preco_mat_p 	 out 	number,
						ie_origem_preco_w,      -- ie_origem_preco_p  	 out 	number,
						nr_seq_bras_preco_w,    -- nr_seq_bras_preco_p	 out	number,
						nr_seq_mat_bras_w,      -- nr_seq_mat_bras_p	 out	number,
						nr_seq_conv_bras_w,     -- nr_seq_conv_bras_p	 out	number,
						nr_seq_conv_simpro_w,   -- nr_seq_conv_simpro_p	 out	number,
						nr_seq_mat_simpro_w,    -- nr_seq_mat_simpro_p	 out	number,
						nr_seq_simpro_preco_w,  -- nr_seq_simpro_preco_p out	number,
						nr_seq_mat_ajuste_w);   -- nr_seq_ajuste_mat_p	 out	number
		end if;

		select	to_number(obter_dados_pf_pj_estab(cd_estabelecimento_w, null, cd_cgc_fornec_w, 'ECP'))
		into	cd_cond_pagto_w
		from	dual;


		if	(vl_preco_w > 0) then

			update	material_autor_cir_cot
			set	dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				vl_cotado		= (qt_material_w * vl_preco_w),
				vl_unitario_cotado	= vl_preco_w,
				cd_condicao_pagamento	= cd_cond_pagto_w,
				ie_aprovacao		= 'N'
			where	nr_sequencia		= nr_seq_mat_autor_cir_w
			and	cd_cgc			= cd_cgc_fornec_w;

		end if;
	end loop;
	close c02;

end loop;
close c01;

commit;

end atualiza_preco_mat_opme_conv;
/
