create or replace
procedure ctb_grava_log_mes(nr_sequencia_p	in out	number,
				nm_usuario_p		varchar2,
				nr_seq_mes_ref_p	number,
				cd_log_contabil_p	varchar2,
				ds_observacao_p	varchar2) is

nr_sequencia_w		number(10,0);

begin

if	(nvl(nr_sequencia_p,0) = 0) then
	begin
	if	(cd_log_contabil_p is not null) and
		(nr_seq_mes_ref_p is not null) then
		begin

		select	ctb_mes_log_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into ctb_mes_log(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_mes_ref,
					cd_log_contabil,
					ds_observacao,
					dt_fim)
				values(nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					nr_seq_mes_ref_p,
					cd_log_contabil_p,
					ds_observacao_p,
					decode(cd_log_contabil_p,'0',sysdate,'1',sysdate,'2',sysdate,'3',sysdate,'4',sysdate,null));
		end;
		
		nr_sequencia_p	:= nr_sequencia_w;
	end if;
	end;
elsif	(nvl(nr_sequencia_p,0) > 0) then
	begin
	update	ctb_mes_log
	set	dt_fim 		= sysdate,
		ds_observacao	= decode(ds_observacao_p,'',ds_observacao,ds_observacao_p)
	where	nr_sequencia 	= nr_sequencia_p;
	end;
end if;

commit;

end ctb_grava_log_mes;
/