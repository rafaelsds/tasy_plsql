create or replace 
procedure	cpoe_update_prox_geracao(	nr_prescricao_p			number,
										nr_seq_cpoe_p			number,
										ie_tipo_item_p			varchar2,
										nr_seq_horario_p		number,
										dt_horario_desejado_p	date,
										nm_usuario_p			varchar2,
										cd_estabelecimento_p	number) is
										
--ATEN��O:  O CAMPO  IE_TIPO_ITEM_P  TEM AS OP��ES DO CAMPO DA REP.

dt_prox_geracao_w	date;
dt_horario_w  		date;
dt_min_horario_w	date;
nr_horas_copia_w	number(15) := 0;

begin

nr_horas_copia_w := get_qt_hours_after_copy_cpoe(obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p);
dt_prox_geracao_w := (dt_horario_desejado_p - (nr_horas_copia_w/24)) + 1;

if ('R' = ie_tipo_item_p) then

	select	min(dt_horario)
	into	dt_min_horario_w
	from	prescr_rec_hor
	where	nr_prescricao = nr_prescricao_p;

	select 	max(dt_horario)
	into	dt_horario_w
	from 	prescr_rec_hor
	where	nr_sequencia = nr_seq_horario_p
	and		nr_prescricao = nr_prescricao_p;

	if (dt_min_horario_w = dt_horario_w) then
		update	cpoe_recomendacao
		set		dt_prox_geracao = trunc(dt_prox_geracao_w,'hh24')
		where	nr_sequencia = nr_seq_cpoe_p;
	end if;
elsif ('D' = ie_tipo_item_p) then

	select	min(dt_horario)
	into	dt_min_horario_w
	from	prescr_dieta_hor
	where	nr_prescricao = nr_prescricao_p;

	select 	max(dt_horario)
	into	dt_horario_w
	from 	prescr_dieta_hor
	where	nr_sequencia = nr_seq_horario_p
	and		nr_prescricao = nr_prescricao_p;
	
	if (dt_min_horario_w = dt_horario_w) then
		update	cpoe_dieta
		set		dt_prox_geracao = trunc(dt_horario_desejado_p,'hh24')
		where	nr_sequencia = nr_seq_cpoe_p;
	end if;
elsif (('M' = ie_tipo_item_p) or ('MAT' = ie_tipo_item_p) or ('SOL' = ie_tipo_item_p)) then
	
	select	min(dt_horario)
	into	dt_min_horario_w
	from	prescr_mat_hor
	where	nr_prescricao = nr_prescricao_p;

	select 	max(dt_horario)
	into	dt_horario_w
	from 	prescr_mat_hor
	where	nr_sequencia = nr_seq_horario_p
	and		nr_prescricao = nr_prescricao_p;

	if (dt_min_horario_w = dt_horario_w) then
		update	cpoe_material
		set		dt_prox_geracao = trunc(dt_prox_geracao_w,'hh24')
		where	nr_sequencia = nr_seq_cpoe_p;
	end if;
elsif ('1' = ie_tipo_item_p) then	
	if (nr_seq_horario_p = '1') then
		update	cpoe_material
		set		dt_prox_geracao = trunc(dt_prox_geracao_w,'hh24')
		where	nr_sequencia = nr_seq_cpoe_p;
	end if;
elsif (('S' = ie_tipo_item_p) or ('LD' = ie_tipo_item_p) or ('2' = ie_tipo_item_p)) then
	select	min(dt_horario)
	into	dt_min_horario_w
	from	prescr_mat_hor
	where	nr_prescricao = nr_prescricao_p;

	select 	max(dt_horario)
	into	dt_horario_w
	from 	prescr_mat_hor
	where	nr_sequencia = nr_seq_horario_p
	and		nr_prescricao = nr_prescricao_p;
	
	if (dt_min_horario_w = dt_horario_w) then
		update	cpoe_dieta
		set		dt_prox_geracao = trunc(dt_prox_geracao_w,'hh24')
		where	nr_sequencia = nr_seq_cpoe_p;
	end if;
elsif (('P' = ie_tipo_item_p) or ('G' = ie_tipo_item_p) or ('C' = ie_tipo_item_p) or ('I' = ie_tipo_item_p)) then
	select	min(dt_horario)
	into	dt_min_horario_w
	from	prescr_proc_hor
	where	nr_prescricao = nr_prescricao_p;

	select 	max(dt_horario)
	into	dt_horario_w
	from 	prescr_proc_hor
	where	nr_sequencia = nr_seq_horario_p
	and		nr_prescricao = nr_prescricao_p;
	
	if (dt_min_horario_w = dt_horario_w) then
		update	cpoe_procedimento
		set		dt_prox_geracao = trunc(dt_prox_geracao_w,'hh24')
		where	nr_sequencia = nr_seq_cpoe_p;
	end if;
elsif ('O' = ie_tipo_item_p) then
	select	min(dt_horario)
	into	dt_min_horario_w
	from	prescr_gasoterapia_hor
	where	nr_prescricao = nr_prescricao_p;

	select 	max(dt_horario)
	into	dt_horario_w
	from 	prescr_gasoterapia_hor
	where	nr_sequencia = nr_seq_horario_p
	and		nr_prescricao = nr_prescricao_p;
	
	if (dt_min_horario_w = dt_horario_w) then
		update	cpoe_gasoterapia
		set		dt_prox_geracao = trunc(dt_prox_geracao_w,'hh24')
		where	nr_sequencia = nr_seq_cpoe_p;
	end if;
end if;

end cpoe_update_prox_geracao;
/
