create or replace
procedure gerar_recomendacoes_protocolo(
		cd_protocolo_p			number,
		nr_sequencia_p			number,
		nr_seq_classif_p		number,
		ds_lista_recomendacoes_p	varchar2,
		nm_usuario_p			varchar2) is

ds_lista_recomendacoes_w	varchar2(2000);
nr_pos_virgula_w		number(10,0);
cd_tipo_recomendacao_w		number(10,0);
nr_seq_rec_w			number(6,0);
	
begin
if	(ds_lista_recomendacoes_p is not null) and
	(cd_protocolo_p is not null) and
	(nr_sequencia_p is not null) and
	(nr_seq_classif_p is not null) and
	(nm_usuario_p is not null) then
	begin
	ds_lista_recomendacoes_w	:= ds_lista_recomendacoes_p;
	
	while (ds_lista_recomendacoes_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(ds_lista_recomendacoes_w,',');
		if	(nr_pos_virgula_w > 0) then
			begin
			cd_tipo_recomendacao_w		:= substr(ds_lista_recomendacoes_w,0,nr_pos_virgula_w-1);
			ds_lista_recomendacoes_w	:= substr(ds_lista_recomendacoes_w,nr_pos_virgula_w+1,length(ds_lista_recomendacoes_w));			
			end;
		else
			begin
			cd_tipo_recomendacao_w		:= to_number(nvl(ds_lista_recomendacoes_w,0));
			ds_lista_recomendacoes_w	:= null;
			end;
		end if;	
		
		if	(nvl(cd_tipo_recomendacao_w,0) > 0) then
			begin
			
			select	(nvl(max(nr_seq_rec),0) + 1)
			into	nr_seq_rec_w
			from	protocolo_medic_rec
			where	cd_protocolo = cd_protocolo_p
			and	nr_sequencia = nr_sequencia_p;
			
			insert into protocolo_medic_rec(
						cd_protocolo,
						nr_sequencia,
						nr_seq_rec,
						dt_atualizacao,
						nm_usuario,
						cd_recomendacao,
						nr_seq_classif) 
						values(
						cd_protocolo_p,
						nr_sequencia_p,
						nr_seq_rec_w,
						sysdate,
						nm_usuario_p,
						cd_tipo_recomendacao_w,
						nr_seq_classif_p);			
			end;
		end if;
		end;
	end loop;
	end;
end if;
commit;
end gerar_recomendacoes_protocolo;
/