create or replace
procedure HD_Gerar_GPID_retroativo	(	nr_seq_dialise_p	number,
				nm_usuario_p		varchar2,
				ds_erro_p	out	varchar2) is


nr_seq_proxima_dialise_w		number(10,0);
cd_pessoa_fisica_w		varchar2(10);
qt_peso_w		                 number(6,3);
qt_peso_pre_proxima_w		number(6,3);
qt_gpid_w			number(15,7);
qt_valor_gpid_w			number(15,7);

begin

/* Obter os dados da di�lise atual */
select	cd_pessoa_fisica,
	qt_peso_pos
into	cd_pessoa_fisica_w,
	qt_peso_w
from	hd_dialise
where	nr_sequencia		= nr_seq_dialise_p;


/* Pega a sequ�ncia da pr�xima di�lise */
select	nvl(min(nr_sequencia),0)
into	nr_seq_proxima_dialise_w
from	hd_dialise
where	nr_sequencia		> nr_seq_dialise_p
and	cd_pessoa_fisica	= cd_pessoa_fisica_w;

/* Pega o peso pr� da pr�xima di�lise */
begin
select	qt_peso_pre
into	qt_peso_pre_proxima_w
from	hd_dialise
where	nr_sequencia		= nr_seq_proxima_dialise_w;
exception
	when others then
	qt_peso_pre_proxima_w	:= 0;
end;

if	(qt_peso_pre_proxima_w = 0) then
	qt_gpid_w		:= 0;
	qt_valor_gpid_w		:= 0;
else
	qt_gpid_w		:= ((qt_peso_pre_proxima_w/qt_peso_w)*100)-100;
	qt_valor_gpid_w		:= (qt_peso_pre_proxima_w - qt_peso_w);
end if;

update	hd_dialise
set	qt_gpid			= qt_gpid_w,
	qt_valor_gpid		= qt_valor_gpid_w	
where	nr_sequencia		= nr_seq_proxima_dialise_w;

commit;

end HD_Gerar_GPID_retroativo;
/
