create or replace
procedure ptu_gerar_benef_compl
			(	nr_seq_segurado_p		number,
				nr_seq_beneficiario_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

ds_endereco_w			Varchar2(100);
nr_endereco_w			varchar2(5);
cd_pessoa_fisica_w		varchar2(20);
cd_cep_w			varchar2(10);
ds_municipio_w			varchar2(30);
sg_estado_w			compl_pessoa_fisica.sg_estado%type;
ds_bairro_w			varchar2(30);
nr_ddd_telefone_w		compl_pessoa_fisica.nr_ddd_telefone%type;
nr_telefone_w			compl_pessoa_fisica.nr_telefone%type;
nr_ramal_w			number(4);
qt_registros_w			number(10);
ie_utilizar_ender_empresa_w	varchar2(30);
nr_seq_empresa_w		number(10);

nr_ddd_celular_w		pessoa_fisica.nr_ddd_celular%type;
nr_telefone_celular_w		pessoa_fisica.nr_telefone_celular%type;
ds_email_w			compl_pessoa_fisica.ds_email%type;
ds_complemento_w		compl_pessoa_fisica.ds_complemento%type;
cd_municipio_ibge_w		ptu_beneficiario_compl.cd_municipio_ibge%type;
ie_tipo_endereco_w		compl_pessoa_fisica.ie_tipo_complemento%type;
cd_tipo_logradouro_w		pls_interc_tipo_logradouro.cd_intercambio%type;

Cursor C01 is
	select	ie_tipo_complemento,
		cd_tipo_logradouro,
		substr(ds_endereco,1,40),
		nr_endereco,
		substr(somente_numero_char(cd_cep),1,8),
		substr(obter_desc_municipio_ibge(cd_municipio_ibge),1,30),
		substr(sg_estado,1,2),
		substr(ds_bairro,1,30),
		substr(somente_numero_char(nr_ddd_telefone),1,4),
		substr(somente_numero_char(nr_telefone),1,9),
		substr(somente_numero_char(nr_ramal),1,4),
		ds_complemento,
		cd_municipio_ibge
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	ie_tipo_complemento	= 1;

function obter_cd_tipo_logradouro
			(	cd_tipo_logradouro_p	sus_tipo_logradouro.cd_tipo_logradouro%type)
 		    	return varchar2 is
ds_retorno_w	varchar2(255);
Cursor C01 is
	select	cd_intercambio
	from	pls_interc_tipo_logradouro
	where	(cd_tipo_logradouro = cd_tipo_logradouro_p or cd_tipo_logradouro is null)
	order by nvl(cd_tipo_logradouro,0);
begin
ds_retorno_w	:= null;
for r_c01_w in C01 loop
	begin
	ds_retorno_w	:= r_c01_w.cd_intercambio;
	end;
end loop;
return	ds_retorno_w;
end obter_cd_tipo_logradouro;

begin

ie_utilizar_ender_empresa_w	:= nvl(obter_valor_param_usuario(1286, 5, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');

select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

select	nr_seq_empresa
into	nr_seq_empresa_w
from	ptu_intercambio_benef
where	nr_sequencia	= nr_seq_beneficiario_p;

select	count(1)
into	qt_registros_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_w
and	ie_tipo_complemento	= 1
and	cd_municipio_ibge is not null;

if	(qt_registros_w > 0) then
	begin
	open C01;
	loop
	fetch C01 into
		ie_tipo_endereco_w,
		cd_tipo_logradouro_w,
		ds_endereco_w,
		nr_endereco_w,
		cd_cep_w,
		ds_municipio_w,
		sg_estado_w,
		ds_bairro_w,
		nr_ddd_telefone_w,
		nr_telefone_w,
		nr_ramal_w,
		ds_complemento_w,
		cd_municipio_ibge_w;
	exit when C01%notfound;
			begin
			cd_tipo_logradouro_w	:= obter_cd_tipo_logradouro(cd_tipo_logradouro_w);
			
			ds_municipio_w	:= PTU_SOMENTE_CARACTER_PERMITIDO(Elimina_Acentuacao(ds_municipio_w),'AN');
			ds_endereco_w	:= PTU_SOMENTE_CARACTER_PERMITIDO(Elimina_Acentuacao(ds_endereco_w),'AN');
			ds_bairro_w	:= replace(ds_bairro_w,'/',' ');
			ds_bairro_w	:= Elimina_Acentuacao(ds_bairro_w);
			ds_bairro_w	:= PTU_SOMENTE_CARACTER_PERMITIDO(elimina_caractere_especial(ds_bairro_w),'AN');
			ds_complemento_w:= PTU_SOMENTE_CARACTER_PERMITIDO(elimina_caractere_especial(ds_complemento_w),'AN');
			
			begin
			insert into ptu_beneficiario_compl
				(nr_sequencia, nr_seq_beneficiario, ds_endereco, 
				cd_cep, nm_municipio, sg_uf,
				dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
				nm_usuario_nrec, ds_bairro, nr_ddd,
				nr_fone, nr_ramal, nr_endereco,
				ds_complemento, cd_municipio_ibge, ie_tipo_endereco,
				cd_tipo_logradouro )
			values	(ptu_beneficiario_compl_seq.nextval, nr_seq_beneficiario_p, ds_endereco_w,
				cd_cep_w, ds_municipio_w, sg_estado_w,
				sysdate, nm_usuario_p, sysdate,
				nm_usuario_p, ds_bairro_w, nr_ddd_telefone_w,
				nr_telefone_w, nr_ramal_w, nr_endereco_w,
				ds_complemento_w, cd_municipio_ibge_w, ie_tipo_endereco_w,
				cd_tipo_logradouro_w );
			exception
			when others then
				if	(cd_cep_w is null) then
					ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário','Beneficiário sem CEP informado.',cd_estabelecimento_p,nm_usuario_p);
				end if;
				
				if	(ds_municipio_w is null) then
					ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário','Beneficiário sem município informado.',cd_estabelecimento_p,nm_usuario_p);
				end if;
				
				if	(sg_estado_w is null) then
					ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário','Beneficiário sem estado informado.',cd_estabelecimento_p,nm_usuario_p);
				end if;
				
				if	(ds_endereco_w is null) then
					ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário','Beneficiário sem endereço informado.',cd_estabelecimento_p,nm_usuario_p);
				end if;
				
				ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário',sqlerrm(sqlcode),cd_estabelecimento_p,nm_usuario_p);
			end;
		end;
	end loop;
	exception
	when others then
	ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Beneficiário com cadastro complementar errado',sqlerrm(sqlcode),cd_estabelecimento_p,nm_usuario_p);
	end;
	close C01;
elsif	(qt_registros_w = 0) and
	(ie_utilizar_ender_empresa_w = 'S') then
	
	select	cd_tipo_logradouro,
		substr(ds_endereco,1,40),
		nr_endereco,
		substr(cd_cep,1,8),
		substr(obter_desc_municipio_ibge(substr(cd_municipio_ibge,1,6)),1,30),
		substr(sg_uf,1,2),
		substr(ds_bairro,1,30),
		substr(nr_ddd,1,4),
		somente_numero(substr(nr_telefone,1,9)),
		cd_municipio_ibge,
		ds_complemento
	into	cd_tipo_logradouro_w,
		ds_endereco_w,
		nr_endereco_w,
		cd_cep_w,
		ds_municipio_w,
		sg_estado_w,
		ds_bairro_w,
		nr_ddd_telefone_w,
		nr_telefone_w,
		cd_municipio_ibge_w,
		ds_complemento_w
	from	ptu_intercambio_empresa
	where	nr_sequencia	= nr_seq_empresa_w;
	
	cd_tipo_logradouro_w	:= obter_cd_tipo_logradouro(cd_tipo_logradouro_w);
	
	ds_municipio_w	:= PTU_SOMENTE_CARACTER_PERMITIDO(Elimina_Acentuacao(ds_municipio_w),'AN');
	ds_endereco_w	:= PTU_SOMENTE_CARACTER_PERMITIDO(Elimina_Acentuacao(ds_endereco_w),'AN');
	ds_bairro_w	:= PTU_SOMENTE_CARACTER_PERMITIDO(replace(ds_bairro_w,'/',' '),'AN');
	ds_bairro_w	:= Elimina_Acentuacao(ds_bairro_w);
	ds_bairro_w	:= PTU_SOMENTE_CARACTER_PERMITIDO(elimina_caractere_especial(ds_bairro_w),'AN');
	ds_complemento_w:= PTU_SOMENTE_CARACTER_PERMITIDO(elimina_caractere_especial(ds_complemento_w),'AN');
	
	begin
	insert into ptu_beneficiario_compl
		(nr_sequencia, nr_seq_beneficiario, ds_endereco,
		cd_cep, nm_municipio, sg_uf,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, 
		nm_usuario_nrec, ds_bairro, nr_ddd, 
		nr_fone, nr_ramal, nr_endereco,
		ie_tipo_endereco, cd_municipio_ibge, ds_complemento,
		cd_tipo_logradouro )
	values	(ptu_beneficiario_compl_seq.nextval, nr_seq_beneficiario_p, ds_endereco_w,
		cd_cep_w, ds_municipio_w, sg_estado_w,
		sysdate, nm_usuario_p, sysdate,
		nm_usuario_p, ds_bairro_w, nr_ddd_telefone_w,
		nr_telefone_w, nr_ramal_w, nr_endereco_w,
		2, cd_municipio_ibge_w, ds_complemento_w,
		cd_tipo_logradouro_w );
	exception
	when others then
		if	(cd_cep_w is null) then
			ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário','Beneficiário sem CEP informado.',cd_estabelecimento_p,nm_usuario_p);
		end if;
		
		if	(ds_municipio_w is null) then
			ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário','Beneficiário sem município informado.',cd_estabelecimento_p,nm_usuario_p);
		end if;
		
		if	(sg_estado_w is null) then
			ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário','Beneficiário sem estado informado.',cd_estabelecimento_p,nm_usuario_p);
		end if;
		
		if	(ds_endereco_w is null) then
			ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário','Beneficiário sem endereço informado.',cd_estabelecimento_p,nm_usuario_p);
		end if;
		
		ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Falta dados do complemento obrigatórios para o beneficiário',sqlerrm(sqlcode),cd_estabelecimento_p,nm_usuario_p);
	
		ptu_gravar_incon_benef_envio(nr_seq_beneficiario_p,null,'Faltam dados para os dados complementar da empresa do beneficiário',sqlerrm(sqlcode),cd_estabelecimento_p,nm_usuario_p);
	end;
end if;

--Informações de contato - Telefone residencial
begin
select	nr_ddd_telefone,
	somente_numero(substr(nr_telefone,1,9))

into	nr_ddd_telefone_w,
	nr_telefone_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w
and	ie_tipo_complemento = 1;
exception
when others then
	nr_ddd_telefone_w	:= null;
	nr_telefone_w		:= null;
end;

if	(nr_ddd_telefone_w is not null) and
	(nr_telefone_w is not null) then
	insert	into	ptu_beneficiario_contato
		(	nr_sequencia, nr_seq_beneficiario, ie_tipo_contato,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			ie_tipo_telefone, nr_ddd, nr_telefone)
		values(	ptu_beneficiario_contato_seq.nextval, nr_seq_beneficiario_p, 'T',
			sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			'1', nr_ddd_telefone_w, nr_telefone_w);
end if;

--Informações de contato - Telefone profissional
begin
select	nr_ddd_telefone,
	somente_numero(substr(nr_telefone,1,9))

into	nr_ddd_telefone_w,
	nr_telefone_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w
and	ie_tipo_complemento = 2;
exception
when others then
	nr_ddd_telefone_w	:= null;
	nr_telefone_w		:= null;
end;

if	(nr_ddd_telefone_w is not null) and
	(nr_telefone_w is not null) then
	insert	into	ptu_beneficiario_contato
		(	nr_sequencia, nr_seq_beneficiario, ie_tipo_contato,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			ie_tipo_telefone, nr_ddd, nr_telefone)
		values(	ptu_beneficiario_contato_seq.nextval, nr_seq_beneficiario_p, 'T',
			sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			'2', nr_ddd_telefone_w, nr_telefone_w);
end if;

--Informações de contato - Celular
begin
select	nr_ddd_celular,
	somente_numero(substr(nr_telefone_celular,1,9))
into	nr_ddd_celular_w,
	nr_telefone_celular_w
from	pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w;
exception
when others then
	nr_ddd_celular_w	:= null;
	nr_telefone_celular_w	:= null;
end;

if	(nr_ddd_celular_w is not null) and
	(nr_telefone_celular_w is not null) then
	insert	into	ptu_beneficiario_contato
		(	nr_sequencia, nr_seq_beneficiario, ie_tipo_contato,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			ie_tipo_telefone, nr_ddd, nr_telefone)
		values(	ptu_beneficiario_contato_seq.nextval, nr_seq_beneficiario_p, 'T',
			sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			'3', nr_ddd_celular_w, nr_telefone_celular_w);
end if;

--Informações de contato - E-mail principal
begin
select	ds_email
into	ds_email_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w
and	ie_tipo_complemento = 1;
exception
when others then
	ds_email_w	:= null;
end;

if	(ds_email_w is not null) then
	insert	into	ptu_beneficiario_contato
		(	nr_sequencia, nr_seq_beneficiario, ie_tipo_contato,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			ie_tipo_email, ds_email)
		values(	ptu_beneficiario_contato_seq.nextval, nr_seq_beneficiario_p, 'E',
			sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			'1', ds_email_w);
end if;

--Informações de contato - E-mail alternativo
begin
select	ds_email
into	ds_email_w
from	compl_pessoa_fisica
where	cd_pessoa_fisica = cd_pessoa_fisica_w
and	ie_tipo_complemento = 2;
exception
when others then
	ds_email_w	:= null;
end;

if	(ds_email_w is not null) then
	insert	into	ptu_beneficiario_contato
		(	nr_sequencia, nr_seq_beneficiario, ie_tipo_contato,
			dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			ie_tipo_email, ds_email)
		values(	ptu_beneficiario_contato_seq.nextval, nr_seq_beneficiario_p, 'E',
			sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			'2', ds_email_w);
end if;

end ptu_gerar_benef_compl;
/
