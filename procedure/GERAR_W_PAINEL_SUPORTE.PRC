create or replace
procedure gerar_w_painel_suporte(	nr_seq_gerencia_p		number,
					dt_referencia_p			date,
					nm_usuario_p			varchar2	) is 

dt_referencia_w			date;
dt_referencia_ww		date := trunc(sysdate);
nr_seq_gerencia_w		number(10) := nvl(nr_seq_gerencia_p,0);
pr_os_antiga_prev_w		number(15,1);
qt_os_prev_total_w		number(10);
qt_os_exec_total_w		number(10);
qt_total_os_ant_prev_w		number(10);

cursor c01 is
select	c.ds_grupo,
	c.nr_sequencia,
	a.pr_duvida,
	a.pr_os_antiga,
	a.pr_satisfacao,
	a.qt_total_os,
	a.qt_os_antiga,
	obter_qt_os_painel_sup(b.nr_sequencia, c.nr_sequencia, 3) qt_total_os_ant_prev
from	w_indicador_suporte_apres a,
	gerencia_wheb b,
	grupo_suporte c
where	a.nr_seq_gerencia = b.nr_sequencia
and	a.nr_seq_grupo = c.nr_sequencia
and	b.nr_sequencia = c.nr_seq_gerencia_sup
and	a.dt_referencia = dt_referencia_w
and	a.ie_abrangencia = 'GRU'
and	b.nr_sequencia = nr_seq_gerencia_w
order by 1;

cursor c02 is
select	b.ds_gerencia,
	b.nr_sequencia,
	a.pr_duvida,
	a.pr_os_antiga,
	a.pr_satisfacao,
	a.qt_total_os,
	a.qt_os_antiga
from	w_indicador_suporte_apres a,
	gerencia_wheb b
where	a.nr_seq_gerencia = b.nr_sequencia
and	a.dt_referencia = dt_referencia_w
and	a.ie_abrangencia = 'GER'
and	b.nr_sequencia = nr_seq_gerencia_w;

cursor c03 is
select	b.ds_gerencia,
	b.nr_sequencia,
	a.pr_duvida,
	a.pr_os_antiga,
	a.pr_satisfacao,
	a.qt_total_os,
	a.qt_os_antiga,
	obter_qt_os_painel_sup(b.nr_sequencia, 0, 3) qt_total_os_ant_prev
from	w_indicador_suporte_apres a,
	gerencia_wheb b
where	a.nr_seq_gerencia = b.nr_sequencia
and	a.dt_referencia = dt_referencia_w
and	a.ie_abrangencia = 'GER'
and	b.cd_setor_atendimento in (4, 17);

c01_w		c01%rowtype;
c02_w		c02%rowtype;
c03_w		c03%rowtype;

begin

dt_referencia_w	:= trunc(nvl(dt_referencia_p,sysdate)-1,'dd');

qt_total_os_ant_prev_w	:= 0;

delete	w_painel_ind_suporte
where	dt_referencia = dt_referencia_ww
and	nm_usuario = nm_usuario_p;

open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
begin

	begin
	pr_os_antiga_prev_w := nvl(round(dividir((c01_w.qt_os_antiga - c01_w.qt_total_os_ant_prev) , (c01_w.qt_total_os - c01_w.qt_total_os_ant_prev))  * 100,1),0);
	exception when others then
		pr_os_antiga_prev_w := 0;
	end;

	if	(pr_os_antiga_prev_w < 0) then
		pr_os_antiga_prev_w	:= 0;
	end if;

	insert into w_painel_ind_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_grupo,
		nr_seq_gerencia,
		ds_informacao,
		pr_duvida,
		pr_os_antiga,
		pr_os_antiga_prev,
		pr_satisfacao,
		dt_referencia,
		ie_abrangencia,
		qt_total_os )
	values (w_painel_ind_suporte_seq.nextval,
		sysdate,
		nm_usuario_p,
		c01_w.nr_sequencia,
		nr_seq_gerencia_w,
		c01_w.ds_grupo,
		c01_w.pr_duvida,
		c01_w.pr_os_antiga,
		pr_os_antiga_prev_w,
		c01_w.pr_satisfacao,
		dt_referencia_ww,
		'GRU',
		c01_w.qt_total_os);

	qt_total_os_ant_prev_w	:= qt_total_os_ant_prev_w + c01_w.qt_total_os_ant_prev;
		
end;
end loop;
close c01;

pr_os_antiga_prev_w	:= 0;

open c02;
loop
fetch c02 into
	c02_w;
exit when c02%notfound;
begin

	pr_os_antiga_prev_w	:= round(dividir((c02_w.qt_os_antiga - qt_total_os_ant_prev_w) , (c02_w.qt_total_os - qt_total_os_ant_prev_w))  * 100,1);

	if	(pr_os_antiga_prev_w < 0) then
		pr_os_antiga_prev_w	:= 0;
	end if;
	
	insert into w_painel_ind_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_grupo,
		nr_seq_gerencia,
		ds_informacao,
		pr_duvida,
		pr_os_antiga,
		pr_os_antiga_prev,
		pr_satisfacao,
		dt_referencia,
		ie_abrangencia,
		qt_total_os )
	values (w_painel_ind_suporte_seq.nextval,
		sysdate,
		nm_usuario_p,
		c02_w.nr_sequencia,
		nr_seq_gerencia_w,
		c02_w.ds_gerencia,
		c02_w.pr_duvida,
		c02_w.pr_os_antiga,
		pr_os_antiga_prev_w,
		c02_w.pr_satisfacao,
		dt_referencia_ww,
		'GER',
		c02_w.qt_total_os);

end;
end loop;
close c02;

if	(nr_seq_gerencia_w = 0) then
begin
	open c03;
	loop
	fetch c03 into
		c03_w;
	exit when c03%notfound;
	begin
	
	nr_seq_gerencia_w := c03_w.nr_sequencia;
	pr_os_antiga_prev_w := round(dividir((c03_w.qt_os_antiga - qt_total_os_ant_prev_w) , (c03_w.qt_total_os - qt_total_os_ant_prev_w))  * 100,1);
	
	if	(pr_os_antiga_prev_w < 0) then
		pr_os_antiga_prev_w	:= 0;
	end if;

	insert into w_painel_ind_suporte (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_grupo,
		nr_seq_gerencia,
		ds_informacao,
		pr_duvida,
		pr_os_antiga,
		pr_os_antiga_prev,
		pr_satisfacao,
		dt_referencia,
		ie_abrangencia,
		qt_total_os )
	values (w_painel_ind_suporte_seq.nextval,
		sysdate,
		nm_usuario_p,
		null,
		nr_seq_gerencia_w,
		c03_w.ds_gerencia,
		c03_w.pr_duvida,
		c03_w.pr_os_antiga,
		pr_os_antiga_prev_w,
		c03_w.pr_satisfacao,
		trunc(sysdate),
		'GER',
		c03_w.qt_total_os);

	end;
	end loop;
	close c03;
end;
end if;

commit;

end gerar_w_painel_suporte;
/
