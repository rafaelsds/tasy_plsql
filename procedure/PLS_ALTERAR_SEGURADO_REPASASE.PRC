create or replace
procedure pls_alterar_segurado_repasase(nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
					nr_seq_tabela_repasse_p	pls_segurado.nr_seq_tabela_repasse%type,
					nr_seq_repasse_seg_p	pls_segurado_repasse.nr_sequencia%type,
					dt_repasse_p		date,
					ie_tipo_segurado_ant_p	pls_segurado.ie_tipo_segurado%type,
					ds_historico_benef_p	pls_segurado_repasse.ds_historico_benef%type,
					nm_usuario_p		usuario.nm_usuario%type) is 
					
begin

update	pls_segurado
set	ie_tipo_segurado	= 'R',
	nr_seq_tabela_repasse	= nr_seq_tabela_repasse_p,
	dt_atualizacao 		= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_alteracao_tipo_segurado = decode(ie_tipo_segurado, 'R', dt_alteracao_tipo_segurado, trunc(sysdate,'dd'))
where	nr_sequencia		= nr_seq_segurado_p;
	
pls_gerar_segurado_historico(	nr_seq_segurado_p, '30', sysdate, 'Altera��o de repasse de: '|| obter_valor_dominio(2406,ie_tipo_segurado_ant_p)||' para: '||obter_valor_dominio(2406,'R'), 
				ds_historico_benef_p, null, null, 
				null, null, sysdate, 
				null, null, null, 
				null, null, null, 
				nm_usuario_p, 'N');

end pls_alterar_segurado_repasase;
/
