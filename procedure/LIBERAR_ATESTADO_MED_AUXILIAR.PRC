create or replace
procedure liberar_Atestado_Med_Auxiliar(
	nr_sequencia_p	Varchar2) is
begin
	if (nvl(nr_sequencia_p,0) > 0) then
	begin

		update	atestado_paciente
		set		dt_liberacao_aux = sysdate
		where	nr_sequencia = nr_sequencia_p;

	end;
end if; 	

commit;

end liberar_Atestado_Med_Auxiliar;
/
