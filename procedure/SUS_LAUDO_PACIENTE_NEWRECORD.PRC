create or replace
procedure sus_laudo_paciente_NewRecord(	nr_atendimento_p		    number,
					nm_usuario_p			varchar2,
					nr_laudo_sus_entrada_p		    number,
					nr_laudo_sus_saida_p		out number,
					cd_doenca_cid_p			out varchar2,
					cd_topografia_p			out varchar2,
					cd_morfologia_p			out varchar2,
					cd_grau_histo_p			out varchar2,
					cd_estadiamento_p		out varchar2,
					cd_medico_p			out varchar2,
					ie_lifonodos_reg_inval_p	out varchar2) is

ds_mensagem_w			varchar2(255);
ds_localizacao_metastase_w	varchar2(4000);					
dt_diag_histopatologico_w	date;
cd_procedimento_w		number(15,0);
ie_finalidade_w			varchar2(3);
cd_medico_resp_w		varchar2(10);

begin

if	(nr_atendimento_p = 0) then
	ds_mensagem_w	:= substr(obter_texto_tasy(15658, null),1,255);
	wheb_mensagem_pck.exibir_mensagem_abort(223158,'MENSAGEM='||ds_mensagem_w);
end if;

Sus_Obter_Dados_Tumor(nr_atendimento_p,nm_usuario_p, cd_doenca_cid_p, cd_topografia_p, cd_morfologia_p, cd_grau_histo_p, cd_estadiamento_p, cd_medico_p, ie_lifonodos_reg_inval_p,dt_diag_histopatologico_w,ds_localizacao_metastase_w, cd_procedimento_w, ie_finalidade_w, cd_medico_resp_w); 

if	(nr_laudo_sus_entrada_p is null) then	
	select	nvl(max(NR_LAUDO_SUS),0) + 1
	into	nr_laudo_sus_saida_p
	from	sus_laudo_paciente
	where	nr_Atendimento = nr_atendimento_p;
else
	nr_laudo_sus_saida_p	:= nr_laudo_sus_entrada_p;
end if;

end sus_laudo_paciente_NewRecord;
/
