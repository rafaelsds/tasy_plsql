create or replace
procedure copiar_param_contabil_estab
			(	cd_estab_origem_p		number,
				cd_estab_destino_p		number,
				nm_usuario_p			varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatarios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_estabelecimento_w			number(5);
ds_observacao_w				varchar2(255);
nm_estab_origem_w			varchar2(80);
qt_registro_w				number(10)	:= 0;

cursor c01 is
	select	a.cd_sequencia_parametro,
		a.cd_conta_receita,
		a.cd_conta_estoque,
		a.cd_conta_passag_direta,
		a.cd_grupo_material,
		a.cd_subgrupo_material,
		a.cd_classe_material,
		a.cd_material,
		a.nr_seq_familia,
		a.cd_area_proced,
		a.cd_especial_proced,
		a.cd_grupo_proced,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.cd_conta_despesa,
		a.cd_setor_atendimento,
		a.ie_tipo_atendimento,
		a.ie_classif_convenio,
		a.cd_convenio,
		a.ie_clinica,
		a.cd_local_estoque,
		a.cd_operacao_estoque,
		a.cd_centro_custo,
		a.dt_inicio_vigencia,
		a.dt_fim_vigencia,
		a.nr_seq_grupo,
		a.nr_seq_subgrupo,
		a.nr_seq_forma_org,
		a.ds_observacao,
		a.cd_categoria_convenio,
		a.ie_tipo_convenio,
		a.cd_plano,
		a.cd_centro_custo_receita,
		a.cd_conta_rec_pacote,
		a.cd_empresa,
		a.nr_seq_motivo_solic,
		a.nr_seq_regra_valor
	from	parametros_conta_contabil a
	where	a.cd_estabelecimento	= cd_estab_origem_p;

vet01	C01%RowType;

begin

nm_estab_origem_w	:= obter_nome_estabelecimento(cd_estab_origem_p);
ds_observacao_w		:= wheb_mensagem_pck.get_texto(299137,	'DT_DUPLIC=' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || ';' ||
								'NM_DUPLIC=' || nm_usuario_p || ';' ||
								'NM_ESTAB=' || nm_estab_origem_w);

open C01;
loop
fetch C01 into
	vet01;
exit when C01%notfound;
	begin
	qt_registro_w	:= qt_registro_w + 1;

	insert into parametros_conta_contabil
		(cd_sequencia_parametro,
		cd_estabelecimento,
		cd_conta_receita,
		cd_conta_estoque,
		cd_conta_passag_direta,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		cd_material,
		nr_seq_familia,
		cd_area_proced,
		cd_especial_proced,
		cd_grupo_proced,
		cd_procedimento,
		nm_usuario,
		dt_atualizacao,
		ie_origem_proced,
		cd_conta_despesa,
		cd_setor_atendimento,
		ie_tipo_atendimento,
		ie_classif_convenio,
		cd_convenio,
		ie_clinica,
		cd_local_estoque,
		cd_operacao_estoque,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_centro_custo,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		nr_seq_grupo,
		nr_seq_subgrupo,
		nr_seq_forma_org,
		ds_observacao,
		cd_categoria_convenio,
		ie_tipo_convenio,
		cd_plano,
		cd_centro_custo_receita,
		cd_conta_rec_pacote,
		cd_empresa,
		nr_seq_motivo_solic,
		nr_seq_regra_valor)
	values	(parametros_conta_contabil_seq.nextval,
		cd_estab_destino_p,
		vet01.cd_conta_receita,
		vet01.cd_conta_estoque,
		vet01.cd_conta_passag_direta,
		vet01.cd_grupo_material,
		vet01.cd_subgrupo_material,
		vet01.cd_classe_material,
		vet01.cd_material,
		vet01.nr_seq_familia,
		vet01.cd_area_proced,
		vet01.cd_especial_proced,
		vet01.cd_grupo_proced,
		vet01.cd_procedimento,
		nm_usuario_p,
		sysdate,
		vet01.ie_origem_proced,
		vet01.cd_conta_despesa,
		vet01.cd_setor_atendimento,
		vet01.ie_tipo_atendimento,
		vet01.ie_classif_convenio,
		vet01.cd_convenio,
		vet01.ie_clinica,
		vet01.cd_local_estoque,
		vet01.cd_operacao_estoque,
		sysdate,
		nm_usuario_p,
		vet01.cd_centro_custo,
		vet01.dt_inicio_vigencia,
		vet01.dt_fim_vigencia,
		vet01.nr_seq_grupo,
		vet01.nr_seq_subgrupo,
		vet01.nr_seq_forma_org,
		substr(ds_observacao_w || chr(13) || chr(10) || vet01.ds_observacao,1,255),
		vet01.cd_categoria_convenio,
		vet01.ie_tipo_convenio,
		vet01.cd_plano,
		vet01.cd_centro_custo_receita,
		vet01.cd_conta_rec_pacote,
		vet01.cd_empresa,
		vet01.nr_seq_motivo_solic,
		vet01.nr_seq_regra_valor);

	if	(qt_registro_w >= 100) then
		qt_registro_w	:= 0;
		commit;
	end if;

	end;
end loop;
close C01;

commit;

end copiar_param_contabil_estab;
/
