create or replace
procedure sip_consistir_lote
			(	nr_seq_lote_p	Number,
				nm_usuario_p		Varchar2) is
qt_beneficiario_w	Number(10);
qt_evento_w		Number(10);
nr_seq_item_sip_w	Number(10);
sg_uf_w			Varchar2(10);
ie_tipo_contratacao_w	Varchar2(2);
ie_segmentacao_sip_w	Number(3);
qt_beneficiario_ww	Number(10);
vl_despesa_w		Number(15,2);
qt_evento_ww		Number(10);

Cursor C01 is
	select	nvl(qt_beneficiario,0),
		nvl(qt_evento,0),
		nr_seq_item_sip,
		ie_tipo_contratacao,
		ie_segmentacao_sip,
		nvl(sg_uf,''),
		nvl(vl_despesa,0)
	from	sip_lote_item_assistencial
	where	nr_seq_lote 	= nr_seq_lote_p
	group by
		qt_beneficiario,
		qt_evento,
		nr_seq_item_sip,
		ie_tipo_contratacao,
		ie_segmentacao_sip,
		sg_uf,
		vl_despesa;
begin 
sip_deletar_inconsistencia(nr_seq_lote_p,nm_usuario_p);
open C01;
loop
fetch C01 into
	qt_beneficiario_w,
	qt_evento_w,
	nr_seq_item_sip_w,
	ie_tipo_contratacao_w,
	ie_segmentacao_sip_w,
	sg_uf_w,
	vl_despesa_w;
exit when C01%notfound;
	begin
	
	if	(nr_seq_item_sip_w in ( 1, 2, 28, 29, 35, 38, 46, 50, 57, 64, 71, 79, 84, 85, 87, 110, 111, 112, 113, 116, 117, 118, 119, 122, 120, 121, 123)) then
		if	(( qt_evento_w 	> 0 ) 	or
			(vl_despesa_w	> 0 ))  and
			(qt_beneficiario_w = 0 )	then
				sip_inserir_inconsistencia(4,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
							   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
							   vl_despesa_w, nr_seq_item_sip_w );
		end if;
	end if;
	
	if	( nr_seq_item_sip_w = 28 ) then
		begin
		qt_beneficiario_ww	:= 0;
		select	nvl(sum(qt_beneficiario),0)
		into 	qt_beneficiario_ww
		from 	sip_Lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and 	nr_seq_item_sip		= 1
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_beneficiario_ww > qt_beneficiario_w ) then
			sip_inserir_inconsistencia(1,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;
	if	( nr_seq_item_sip_w = 65 ) then
		begin
		qt_evento_ww	:= 0;
		select 	nvl(sum(qt_evento),0)
		into 	qt_evento_ww
		from	sip_lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and 	nr_seq_item_sip		in (66,67,75,78,82)
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_evento_ww <> qt_evento_w ) then
			sip_inserir_inconsistencia(2,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;
	if	( nr_seq_item_sip_w = 64 ) then
		begin
		qt_evento_ww	:= 0;
		select 	nvl(sum(qt_evento),0)
		into 	qt_evento_ww
		from	sip_lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and	nr_seq_item_sip		= 83
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_evento_ww <> qt_evento_w ) then
			sip_inserir_inconsistencia(3,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;

	if	(nr_seq_item_sip_w = 46) then
		begin
		qt_beneficiario_ww	:= 0;
		select	nvl(sum(qt_beneficiario),0)
		into 	qt_beneficiario_ww
		from 	sip_Lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and 	nr_seq_item_sip		= 35
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_beneficiario_ww <= qt_beneficiario_w ) then
			sip_inserir_inconsistencia(5,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;
	if	(nr_seq_item_sip_w = 38) then
		begin
		qt_beneficiario_ww	:= 0;
		select	nvl(sum(qt_beneficiario),0)
		into 	qt_beneficiario_ww
		from 	sip_Lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and 	nr_seq_item_sip		= 35
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_beneficiario_ww <= qt_beneficiario_w ) then
			sip_inserir_inconsistencia(6,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;
	if	(nr_seq_item_sip_w = 50) then
		begin
		qt_beneficiario_ww	:= 0;
		select	nvl(sum(qt_beneficiario),0)
		into 	qt_beneficiario_ww
		from 	sip_Lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and 	nr_seq_item_sip		= 35
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_beneficiario_ww <= qt_beneficiario_w ) then
			sip_inserir_inconsistencia(7,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;
	if	(nr_seq_item_sip_w = 83) then
		begin
		qt_evento_ww	:= 0;
		select 	nvl(sum(qt_evento),0)
		into 	qt_evento_ww
		from	sip_lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and 	nr_seq_item_sip		in (84,85,87)
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_evento_ww <> qt_evento_w ) then
			sip_inserir_inconsistencia(8,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;	
	
	if	(nr_seq_item_sip_w = 65) then
		begin
		qt_evento_ww	:= 0;
		select 	nvl(sum(qt_evento),0)
		into 	qt_evento_ww
		from	sip_lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and 	nr_seq_item_sip		= 64
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_evento_ww <> qt_evento_w ) then
			sip_inserir_inconsistencia(9,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;
	
	if	(nr_seq_item_sip_w = 75) then
		begin
		qt_evento_ww	:= 0;
		select 	nvl(sum(qt_evento),0)
		into 	qt_evento_ww
		from	sip_lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_p
		and 	nr_seq_item_sip		in (76,77)
		and	((ie_tipo_contratacao is null)
		or	(ie_tipo_contratacao =  ie_tipo_contratacao_w))
		and	((ie_segmentacao_sip is null)
		or	(ie_segmentacao_sip	= ie_segmentacao_sip_w))
		and	((sg_uf is null)
		or	(sg_uf = sg_uf_w));
		if	( qt_evento_ww > qt_evento_w ) then
			sip_inserir_inconsistencia(10,nr_seq_lote_p,nm_usuario_p, ie_tipo_contratacao_w, 
						   ie_segmentacao_sip_w, sg_uf_w, qt_evento_w, qt_beneficiario_w,
						   vl_despesa_w, nr_seq_item_sip_w );
		end if;
		end;
	end if;
		
	end;
end loop;
close C01;



commit;

end sip_consistir_lote;
/
