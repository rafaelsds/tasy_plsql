create or replace
procedure CPOE_Suspender_item_prescr(	
						lista_itens_p				varchar2,
						nr_atendimento_p			number,
						nm_usuario_p				varchar2,
						nr_seq_motivo_susp_p		number		default null,
						ds_motivo_susp_p			varchar2 	default null,
						ie_interv_farmacia_p		varchar2 	default 'N',
						ds_lista_proc_susp_p		out varchar2,
						ie_angular_p				varchar2 	default 'N',
						ie_atualiza_baixa_alta_p	varchar2	default 'N') is

nr_sequencia_w       		cpoe_dieta.nr_sequencia%type;
nr_seq_vinculado_w			cpoe_dieta.nr_sequencia%type;
ie_tipo_item_w       		cpoe_dieta.ie_tipo_dieta%type;
ie_tipo_dieta_w      		cpoe_dieta.ie_tipo_dieta%type;
dt_lib_suspensao_w			cpoe_dieta.dt_lib_suspensao%type;
nr_seq_cpoe_vinculo_w		cpoe_dieta.nr_seq_cpoe_vinculo%type;
nr_dose_ataque_w     		cpoe_material.nr_sequencia%type;
nr_dose_adicional_w    		cpoe_material.nr_sequencia%type;
nr_seq_procedimento_w		cpoe_material.nr_seq_procedimento%type;
ie_controle_tempo_w  		cpoe_material.ie_controle_tempo%type;
ie_forma_suspensao_w		cpoe_material.ie_forma_suspensao%type := 'P';
nr_prescricao_w      		prescr_medica.nr_prescricao%type;
--nr_prescricao_max_w    		prescr_medica.nr_prescricao%type; --1106097
ie_via_leite_w				prescr_material.ie_via_leite%type;
ds_observacao_w				cpoe_material.ds_observacao%type;
nr_seq_prescricao_w			number(15);
lista_itens_aux_w    		varchar2(4000);
nm_tabela_w					varchar2(2000);
ie_se_liberado_w     		char(1);
ie_possui_dialise_w			char(1);
ie_sol_dialise_w			char(1);
ds_lista_proc_suspenso_w	varchar2(4000);
nr_seq_proc_prescr_w		prescr_procedimento.nr_sequencia%type;
ie_suspenso_w				prescr_procedimento.ie_suspenso%type;
nr_seq_agenda_w				cpoe_procedimento.nr_seq_agenda%type;
nr_seq_lista_espera_w		cpoe_procedimento.nr_seq_lista_espera%type; 
ie_agendar_exame_w			varchar2(10);		
ds_erro_w					varchar2(1000);
ie_inativa_item_w			motivo_alta.ie_inativa_item%type;
nr_seq_motivo_susp_w        cpoe_procedimento.nr_seq_motivo_susp%type;
ds_motivo_susp_w            cpoe_procedimento.ds_motivo_susp%type;
ie_existe_lista_espera_w    char(1);
ie_status_agenda_w          varchar2(10);
nr_count_adm_w				number(10);
ie_gerar_prescr_evol_w    varchar2(1);
ds_locale_user_w	 establishment_locale.ds_locale%type;
ie_param_46_w  varchar2(1) := 'N';

type cc01 is Ref Cursor;
c01 cc01;

cursor c02 is
select	nr_sequencia,
		nvl(ie_suspenso,'N')
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_w
and		nr_seq_proc_cpoe = nr_sequencia_w;

CURSOR c03 IS
    SELECT
        a.nr_sequencia,
        a.ie_status_agenda
    FROM
        agenda_paciente            a,
        agenda_paciente_auxiliar   b
    WHERE
        a.nr_sequencia = b.nr_seq_agenda
        AND b.nr_seq_cpoe_procedimento = nr_sequencia_w;



begin
gravar_log_cpoe('CPOE_SUSPENDER_ITEM_PRESCR lista_itens_p: '||lista_itens_p||
		' nm_usuario_p: '||nm_usuario_p||
		' nr_seq_motivo_susp_p: '||nr_seq_motivo_susp_p||
		' ds_motivo_susp_p: '||ds_motivo_susp_p||
		' ie_interv_farmacia_p: '||ie_interv_farmacia_p||
		' ds_lista_proc_susp_p: '||ds_lista_proc_susp_p||
		' ie_angular_p: '||ie_angular_p||
		' ie_atualiza_baixa_alta_p: '||ie_atualiza_baixa_alta_p||
		' get_ie_commit: ' || wheb_usuario_pck.get_ie_commit,
		nr_atendimento_p);

--23259;N;S, 23260;N;S,
if (lista_itens_p is not null) then
	if (ie_angular_p = 'S') then
		ie_forma_suspensao_w := 'I';
	end if;

	-- [254658;M;S,  456786;D;N,  15648;P;S]
	--  '254658' = sequencia do registro
	--  'M' = Tipo item
	--  'S' = Se liberadoz

	if (ie_interv_farmacia_p = 'S') then	
		ds_observacao_w	:= ' '||wheb_mensagem_pck.get_texto(438743, null);
	end if;

	lista_itens_aux_w :=   substr(lista_itens_p,2,length(lista_itens_p) - 2 );

	while ( lista_itens_aux_w is not null) loop
		begin
		nr_sequencia_w := substr( lista_itens_aux_w,1,instr( lista_itens_aux_w,';') - 1);--254658 = sequencia do registro
		lista_itens_aux_w := substr( lista_itens_aux_w,instr( lista_itens_aux_w,';') + 1,length( lista_itens_aux_w ) - 1);--  ;M;S,  456786;D;N,  15648;P;S]

		ie_tipo_item_w := substr( lista_itens_aux_w,0,instr( lista_itens_aux_w,';') - 1);--'M' = Tipo item
		lista_itens_aux_w := substr( lista_itens_aux_w,instr( lista_itens_aux_w,';'),length( lista_itens_aux_w ));--;S,  456786;D;N,  15648;P;S]

		ie_se_liberado_w := substr( lista_itens_aux_w,instr( lista_itens_aux_w,';') + 1,instr( lista_itens_aux_w,',') - 2);--'S' = Se liberado
		lista_itens_aux_w := substr( lista_itens_aux_w,instr( lista_itens_aux_w,',') + 1,length( lista_itens_aux_w ));-- 456786;D;N,  15648;P;S]

		
		ie_possui_dialise_w := 'N';

		if (ie_se_liberado_w = 'N') then
			begin

			-- Nutrition
			if (ie_tipo_item_w = 'N') then
				delete	cpoe_dieta a
				where	a.nr_sequencia = nr_sequencia_w
				and		not exists (select	1
									from	prescr_medica y
									where	y.nr_atendimento = a.nr_atendimento
									and		exists (	select	1
														from	prescr_material z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_dieta_cpoe = a.nr_sequencia
														union
														select	1
														from	prescr_dieta z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_dieta_cpoe = a.nr_sequencia
														union
														select	1
														from	rep_jejum z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_dieta_cpoe = a.nr_sequencia
														union
														select	1
														from	prescr_leite_deriv z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_dieta_cpoe = a.nr_sequencia
														union
														select	1
														from	nut_pac z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_npt_cpoe = a.nr_sequencia));

			-- Medicine and Solution - MAterials
			elsif (ie_tipo_item_w = 'M' or ie_tipo_item_w = 'MA') then
				delete	cpoe_material a
				where 	a.nr_sequencia = nr_sequencia_w
				and		not exists (select	1
									from	prescr_medica y,
											prescr_material z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_mat_cpoe = a.nr_sequencia);

			-- Procedure
			elsif (ie_tipo_item_w = 'P')  then
				delete	cpoe_procedimento a 
				where	a.nr_sequencia = nr_sequencia_w
				and		not exists(	select	1
									from	prescr_medica y,
											prescr_procedimento z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_proc_cpoe = a.nr_sequencia);

				delete cpoe_material where nr_seq_procedimento = nr_sequencia_w;

			-- Gastherapy
			elsif (ie_tipo_item_w = 'G') then
				delete	cpoe_gasoterapia a 
				where	a.nr_sequencia = nr_sequencia_w
				and		not exists(	select	1
									from	prescr_medica y,
											prescr_gasoterapia z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_gas_cpoe = a.nr_sequencia);

			-- Recommendation
			elsif (ie_tipo_item_w = 'R') then
				delete	cpoe_recomendacao a 
				where 	a.nr_sequencia = nr_sequencia_w
				and		not exists(	select	1
									from	prescr_medica y,
											prescr_recomendacao z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_rec_cpoe = a.nr_sequencia);
				
			-- Dialysis
			elsif (ie_tipo_item_w = 'D' or ie_tipo_item_w = 'DI' or ie_tipo_item_w = 'DP') then
				delete	cpoe_dialise a
				where	a.nr_sequencia = nr_sequencia_w
				and		not exists(	select	1
									from	prescr_medica y,
											hd_prescricao z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_dialise_cpoe = a.nr_sequencia);
			
			--Anatomia Patologica
			elsif (ie_tipo_item_w = 'AP') then
				delete cpoe_anatomia_patologica where nr_sequencia = nr_sequencia_w;				
			end if;
			
			end;
		else
			begin
			-- Nutrition
			if	(ie_tipo_item_w = 'N') then
				begin
				
				select	max(ie_tipo_dieta)
				into	ie_tipo_dieta_w
				from	cpoe_dieta
				where	nr_sequencia = nr_sequencia_w;

				update 	cpoe_dieta
				set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
						dt_lib_suspensao	= sysdate,
						nm_usuario_susp		= nm_usuario_p,
						ds_observacao 		= ds_observacao || ds_observacao_w,
						ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
						ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
				where	nr_sequencia 		= nr_sequencia_w;
				
				-- Oral Nutrition
				if (ie_tipo_dieta_w = 'O') then
					
					open c01 for
						select	distinct
								a.nr_prescricao,
								c.nr_sequencia,
								'PRESCR_DIETA' nm_tabela
						from	prescr_medica a,
								prescr_dieta c
						where	a.nr_prescricao = c.nr_prescricao
						and		a.nr_atendimento = nr_atendimento_p
						and		c.nr_seq_dieta_cpoe = nr_sequencia_w;
					
				-- Enteral and Supplement
				elsif (ie_tipo_dieta_w in ('E','S')) then
					
					open c01 for
						select	distinct
								a.nr_prescricao,
								c.nr_sequencia,
								'PRESCR_MATERIAL' nm_tabela
						from	prescr_medica a,
								prescr_material c
						where	a.nr_prescricao = c.nr_prescricao
						and		a.nr_atendimento = nr_atendimento_p
						and		c.nr_seq_dieta_cpoe = nr_sequencia_w;
					

				-- Fasting
				elsif (ie_tipo_dieta_w = 'J') then
					
					open c01 for
						select	distinct
								a.nr_prescricao,
								c.nr_sequencia,
								'REP_JEJUM' nm_tabela
						from	prescr_medica a,
								rep_jejum c
						where	a.nr_prescricao = c.nr_prescricao
						and		a.nr_atendimento = nr_atendimento_p
						and		c.nr_seq_dieta_cpoe = nr_sequencia_w;
					
				-- Milk and infant formulas
				elsif (ie_tipo_dieta_w = 'L') then
					begin
				
					select	nvl(max(b.ie_via_leite),'O')
					into	ie_via_leite_w
					from	prescr_medica a,
							prescr_material b,
							cpoe_dieta c
					where	a.nr_prescricao = b.nr_prescricao
					and		b.nr_seq_dieta_cpoe = c.nr_sequencia
					and		b.ie_agrupador = 16
					and		((c.nr_sequencia = nr_sequencia_w) or 
							 (c.nr_seq_cpoe_vinculo = nr_sequencia_w)); 

					-- Item generated twice from REP
					 if	(ie_via_leite_w = 'OS') then
						
						select 	max(a.dt_lib_suspensao),
								nvl(max(a.nr_sequencia),0)
						into	dt_lib_suspensao_w,
								nr_seq_vinculado_w
						from 	cpoe_dieta a
						where	exists(	select	1
								from	cpoe_dieta b
								where	a.nr_seq_cpoe_vinculo = b.nr_sequencia
								and	b.nr_sequencia = nr_sequencia_w);

						if (nr_seq_vinculado_w = 0) then
							select	max(a.dt_lib_suspensao),
									nvl(max(a.nr_sequencia),0)
							into	dt_lib_suspensao_w,
									nr_seq_vinculado_w
							from 	cpoe_dieta a
							where	exists(	select	1
									from	cpoe_dieta b
									where	a.nr_sequencia = b.nr_seq_cpoe_vinculo
									and	b.nr_sequencia = nr_sequencia_w);
									
							nr_seq_vinculado_w := nr_sequencia_w;
						end if;
					
						nr_sequencia_w := nr_seq_vinculado_w;
						
						if (dt_lib_suspensao_w is null) then
							ie_tipo_item_w	:= null;
						end if; 						
					 	
					end if; 
					
					if	(ie_tipo_item_w is not null) then				
						open c01 for
						select	distinct
								a.nr_prescricao,
								c.nr_sequencia,
								'PRESCR_MATERIAL' nm_tabela
						from   	prescr_medica a,
								prescr_material c
						where  	a.nr_prescricao = c.nr_prescricao
						and		a.nr_atendimento = nr_atendimento_p
						and    	c.nr_seq_dieta_cpoe = nr_sequencia_w;
					end if;
					end;
				
				--Parenteral Nutriton
				elsif (ie_tipo_dieta_w = 'P') or (ie_tipo_dieta_w = 'I') then										
						
						open c01 for
						select	distinct
								a.nr_prescricao,
								c.nr_sequencia,
								'NUT_PAC' nm_tabela
						from	prescr_medica a,
								nut_pac c
						where	a.nr_prescricao = c.nr_prescricao
						and		a.nr_atendimento = nr_atendimento_p
						and		c.nr_seq_npt_cpoe = nr_sequencia_w;
						
				end if;
				end;
				
			-- Medicine and Solution - MAterials
			elsif (ie_tipo_item_w = 'M' or ie_tipo_item_w = 'MA') then
				begin
					
				select	max(nr_seq_procedimento)
				into	nr_seq_procedimento_w
				from	cpoe_material
				where	nr_sequencia = nr_sequencia_w;

				select 	max(nr_sequencia)
				into	nr_dose_ataque_w
				from	cpoe_material
				where	nr_seq_ataque = nr_sequencia_w;

				select 	max(nr_sequencia)
				into	nr_dose_adicional_w
				from	cpoe_material
				where	nr_seq_adicional = nr_sequencia_w;

				if (nr_dose_ataque_w is not null) then
				
					select 	count(*)
					into	nr_count_adm_w
					from	prescr_mat_hor b,
							prescr_material c
					where	b.nr_prescricao   = c.nr_prescricao
					and		b.nr_seq_material = c.nr_sequencia
					and		c.nr_seq_mat_cpoe = nr_dose_ataque_w
					and		b.dt_fim_horario is not null
					and		b.dt_suspensao is null;
				
					if (nr_count_adm_w = 0) then
						update 	cpoe_material
						set		dt_lib_suspensao	= sysdate,
								dt_suspensao 		= nvl(dt_suspensao,sysdate),
								dt_liberacao 		= sysdate,
								nm_usuario_susp		= nm_usuario_p,
								ds_observacao 		= ds_observacao || ds_observacao_w,
								ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
								ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
						where	nr_sequencia 	= nr_dose_ataque_w;
					end if;
				end if;

				if (nr_dose_adicional_w is not null) then
					select 	count(*)
					into	nr_count_adm_w
					from	prescr_mat_hor b,
							prescr_material c
					where	b.nr_prescricao   = c.nr_prescricao
					and		b.nr_seq_material = c.nr_sequencia
					and		c.nr_seq_mat_cpoe = nr_dose_adicional_w
					and		b.dt_fim_horario is not null
					and		b.dt_suspensao is null;
				
					if (nr_count_adm_w = 0) then
						update 	cpoe_material
						set		dt_lib_suspensao	= sysdate,
								dt_suspensao 		= nvl(dt_suspensao,sysdate),
								dt_liberacao 		= sysdate,
								nm_usuario_susp		= nm_usuario_p,
								ds_observacao 		= ds_observacao || ds_observacao_w,
								ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
								ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
						where	nr_sequencia 		= nr_dose_adicional_w;
					end if;
				end if;

				update 	cpoe_material
				set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
						dt_lib_suspensao 	= sysdate,						
						nm_usuario_susp		= nm_usuario_p,
						ds_observacao 		= ds_observacao || ds_observacao_w,
						ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
						ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
				where	nr_sequencia 		= nr_sequencia_w;

				select	nvl(max(ie_controle_tempo),'N')
				into    ie_controle_tempo_w
				from    cpoe_material
				where   nr_Sequencia = nr_sequencia_w;

				if (ie_controle_tempo_w = 'S') then
					ie_tipo_item_w := 'S';
				end if;
				
							
				if	(ie_controle_tempo_w = 'N') then
					open c01 for
						select	distinct
								a.nr_prescricao,
								c.nr_sequencia nr_sequencia,
								'PRESCR_MATERIAL' nm_tabela
						from	prescr_medica a,
								prescr_material c
						where	a.nr_prescricao = c.nr_prescricao
						and		a.nr_atendimento = nr_atendimento_p
						and		c.nr_seq_mat_cpoe = nr_sequencia_w
						order by
							nr_sequencia;
				else
					open c01 for
						select	distinct
								a.nr_prescricao,
								c.nr_sequencia_solucao nr_sequencia,
								'PRESCR_SOLUCAO' nm_tabela
						from	prescr_medica a,
								prescr_material c
						where	a.nr_prescricao = c.nr_prescricao
						and		a.nr_atendimento = nr_atendimento_p
						and		c.nr_seq_mat_cpoe = nr_sequencia_w
						order by
							nr_sequencia;
				end if;

				end;
				
			-- Recommendation
			elsif (ie_tipo_item_w = 'R') then
				begin
				update 	cpoe_recomendacao
				set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
						dt_lib_suspensao	= sysdate,						
						nm_usuario_susp		= nm_usuario_p,
						ds_observacao 		= ds_observacao || ds_observacao_w,
						ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
						ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
				where	nr_sequencia		= nr_sequencia_w;
							
				open c01 for
					select 	distinct
							a.nr_prescricao,
							c.nr_sequencia,
							'PRESCR_RECOMENDACAO' nm_tabela
					from	prescr_medica a,
							prescr_recomendacao c
					where	a.nr_prescricao = c.nr_prescricao
					and		a.nr_atendimento = nr_atendimento_p
					and		c.nr_seq_rec_cpoe = nr_sequencia_w;

				end;

			-- Procedure
			elsif (ie_tipo_item_w = 'P') then
				begin
				
				select	max(a.nr_seq_agenda),
						max(a.nr_seq_lista_espera)
				into	nr_seq_agenda_w,
						nr_seq_lista_espera_w
				from	cpoe_procedimento a
				where	a.nr_sequencia	= nr_sequencia_w;
				
				select 	max(b.ie_inativa_item)
                into	ie_inativa_item_w
                from	atendimento_paciente a,
                        motivo_alta b
                where	a.cd_motivo_alta = b.cd_motivo_alta
                and		a.nr_atendimento = nr_atendimento_p;

				if(nvl(ie_inativa_item_w,'XPTO') <> 'E') then
                    update 	cpoe_procedimento
                    set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
                            dt_lib_suspensao	= sysdate,
                            nm_usuario_susp		= nm_usuario_p,
                            ds_observacao 		= ds_observacao || ds_observacao_w,
                            ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
                            nr_seq_lista_espera = null,
                            ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
                    where	nr_sequencia 		= nr_sequencia_w;
                else 
                    update 	cpoe_procedimento
                    set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
                            dt_lib_suspensao	= sysdate,
                            nm_usuario_susp		= nm_usuario_p,
                            ds_observacao 		= ds_observacao || ds_observacao_w,
                            ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
                            nr_seq_lista_espera = null,
                            ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
                    where	nr_sequencia 		= nr_sequencia_w
                    and     ie_duracao = 'P'
                    and		dt_inicio > sysdate;
                end if;
				
				Obter_param_Usuario(2314, 9, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_agendar_exame_w);
				
        select  nvl2(max(nr_sequencia), 'S', 'N')
        into    ie_existe_lista_espera_w
        from    agenda_lista_espera
        where   nr_seq_cpoe_procedimento = nr_sequencia_w
        and     ie_status_espera <> 'E'; 

		OPEN c03;
		LOOP
			FETCH c03 INTO
				nr_seq_agenda_w,
				ie_status_agenda_w;
			EXIT WHEN c03%notfound;
			BEGIN

				SELECT
					MAX(nr_seq_motivo_susp),
					MAX(ds_motivo_susp)
				INTO
					nr_seq_motivo_susp_w,
					ds_motivo_susp_w
				FROM
					cpoe_procedimento
				WHERE
					nr_sequencia = nr_sequencia_w;

				IF ( ie_status_agenda_w <> 'E' ) THEN
					cancelar_agenda_paciente_html(nr_seq_agenda_w, nr_seq_motivo_susp_w, ds_motivo_susp_w);
					COMMIT;
				END IF;

			END;

		END LOOP;

		CLOSE c03;
		IF ( ie_existe_lista_espera_w = 'S' ) THEN
			UPDATE agenda_paciente
			SET
				nr_seq_lista = NULL
			WHERE
				nr_seq_lista IN (
					SELECT
						nr_sequencia
					FROM
						agenda_lista_espera
					WHERE
						nr_seq_cpoe_procedimento = nr_sequencia_w
				);

			DELETE FROM agenda_lista_espera
			WHERE
				nr_seq_cpoe_procedimento = nr_sequencia_w;

			COMMIT;
		END IF;


				if ((ie_agendar_exame_w <> 'N') and nvl(pkg_i18n.get_user_locale, 'pt_BR') in ('de_DE','ja_JP')) then
					cpoe_cancel_schedule_by_exam(nr_seq_agenda_w, nr_seq_lista_espera_w, null, nm_usuario_p);
				end if;

				update 	cpoe_material
				set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
						dt_lib_suspensao 	= sysdate,
						nm_usuario_susp		= nm_usuario_p,
						ds_observacao 		= ds_observacao || ds_observacao_w,
						ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
						ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
				where	nr_seq_procedimento = nr_sequencia_w;
				
				open c01 for
					select 	distinct
							a.nr_prescricao,
							c.nr_sequencia,
							'PRESCR_PROCEDIMENTO' nm_tabela
					from	prescr_medica a,
							prescr_procedimento c
					where	a.nr_prescricao = c.nr_prescricao
					and		a.nr_atendimento = nr_atendimento_p
					and		c.nr_seq_proc_cpoe = nr_sequencia_w;

				end;
				
			-- Anatomia Patologica
			elsif (ie_tipo_item_w = 'AP') then
				begin
				update 	cpoe_anatomia_patologica
				set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
						dt_lib_suspensao	= sysdate,
						nm_usuario_susp		= nm_usuario_p,
						ds_observacao 		= ds_observacao || ds_observacao_w,
						ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
						ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
				where	nr_sequencia 		= nr_sequencia_w;
				
				open c01 for
					select 	distinct
							a.nr_prescricao,
							c.nr_sequencia,
							'PRESCR_PROCEDIMENTO' nm_tabela
					from	prescr_medica a,
							prescr_procedimento c
					where	a.nr_prescricao = c.nr_prescricao
					and		a.nr_atendimento = nr_atendimento_p
					and		c.nr_seq_proc_cpoe = nr_sequencia_w;
				end;	
				
			-- Gastherapy
			elsif (ie_tipo_item_w = 'G') then
				begin
					update 	cpoe_gasoterapia
					set		dt_suspensao		= nvl(dt_suspensao,sysdate),
							dt_lib_suspensao	= sysdate,
							nm_usuario_susp		= nm_usuario_p,
							ds_observacao 		= ds_observacao || ds_observacao_w,
							ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
							ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
					where	nr_sequencia		= nr_sequencia_w;
				
					open  c01 for
						select 	distinct
								a.nr_prescricao,
								c.nr_sequencia,
								'PRESCR_GASOTERAPIA' nm_tabela
						from    prescr_medica a,
								prescr_gasoterapia c
						where	a.nr_prescricao		= c.nr_prescricao
						and		a.nr_atendimento	= nr_atendimento_p
						and		c.nr_seq_gas_cpoe	= nr_sequencia_w;
				end;
				--Hemotherapy
			elsif (ie_tipo_item_w = 'H') THEN
				begin
					update 	cpoe_hemoterapia
					set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
							dt_lib_suspensao	= sysdate,
							nm_usuario_susp		= nm_usuario_p,
							ds_observacao 		= ds_observacao || ds_observacao_w,
							ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
							ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
					where	nr_sequencia		= nr_sequencia_w;
					
					update	cpoe_material
					set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
							dt_lib_suspensao	= sysdate,
							nm_usuario_susp		= nm_usuario_p,
							ds_observacao 		= ds_observacao || ds_observacao_w,
							ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
							ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
					where 	nr_seq_hemoterapia  = nr_sequencia_w;
					
					open  c01 for
						select 	distinct
								a.nr_prescricao,
								b.nr_sequencia,
								'PRESCR_PROCEDIMENTO' nm_tabela
						from	prescr_medica a,
								prescr_solic_bco_sangue c,
								prescr_procedimento b
						where	a.nr_prescricao	= c.nr_prescricao
						and		c.nr_sequencia = b.nr_seq_solic_sangue
						and		a.nr_atendimento	= nr_atendimento_p
						and		c.nr_seq_hemo_cpoe	= nr_sequencia_w;
				
				end;
			-- Hemodialysis
			elsif (ie_tipo_item_w = 'DI' OR ie_tipo_item_w = 'DP' OR ie_tipo_item_w = 'D') then
				begin
					ie_possui_dialise_w := 'S';
					
					update 	cpoe_dialise
					set		dt_suspensao 		= nvl(dt_suspensao,sysdate),
							dt_lib_suspensao 	= sysdate,
							nm_usuario_susp		= nm_usuario_p,
							ds_observacao 		= ds_observacao || ds_observacao_w,
							ie_baixado_por_alta = decode(ie_atualiza_baixa_alta_p,'S','N',ie_baixado_por_alta),
							ie_forma_suspensao	= nvl(ie_forma_suspensao, ie_forma_suspensao_w)
					where	nr_sequencia 		= nr_sequencia_w;

					open  c01 for
						select 	distinct
								a.nr_prescricao,
								c.nr_seq_dialise_cpoe,
								'PRESCR_SOLUCAO' nm_tabela
						from   	prescr_medica a,
								hd_prescricao c
						where	a.nr_prescricao	= c.nr_prescricao
						and		a.nr_atendimento	= nr_atendimento_p
						and		c.nr_seq_dialise_cpoe	= nr_sequencia_w;						
					
					ie_tipo_item_w := null;
				end;
			end if;

			commit;
			
			-- Quando e um item modificado o atributo ie_possui_dialise_w deve ser 'S' para entrar no if abaixo.
			-- Quando a dialise esta sendo descontinuada, o item dialise nao deve entrar no if abaixo(classe SuspendItemAction e tirado este item da lista lista_itens_p )			
			
			-- Suspend the item schedules
			if	((ie_possui_dialise_w = 'S') or  
				((ie_tipo_item_w is not null) and (ie_tipo_item_w not in ('DP','DI')))) then
				begin
				loop
				fetch c01 into
					nr_prescricao_w,
					nr_seq_prescricao_w,
					nm_tabela_w;
				exit when c01%notfound;		

					if	(nvl(ie_possui_dialise_w,'N') = 'S') and
						(nm_tabela_w = 'PRESCR_SOLUCAO') then
						
							select	nvl(max('S'),'N')
							into	ie_sol_dialise_w
							from	prescr_solucao
							where	nr_seq_dialise_cpoe = nr_seq_prescricao_w
							and		nr_seq_dialise is not null;

							if	(nvl(ie_sol_dialise_w,'N') = 'S') then
									ie_tipo_item_w := 'D';
									nr_sequencia_w := nr_seq_prescricao_w;
							end if;					
					end if;		

					gravar_log_cpoe('CPOE_SUSPENDER_HOR_ITEM_PRESCR call hor_item_prescr: '||
							' nr_prescricao_w: '||nr_prescricao_w||
							' nr_sequencia_w: '||nr_sequencia_w||
							' ie_tipo_item_w: '||ie_tipo_item_w||
							' nm_usuario_p: '||nm_usuario_p||
							' ds_observacao_w: '||ds_observacao_w||
							' ie_interv_farmacia_p: '||ie_interv_farmacia_p,nr_atendimento_p);
					
					cpoe_suspender_hor_item_prescr( nr_prescricao_w , nr_sequencia_w, ie_tipo_item_w, nr_atendimento_p, nm_usuario_p, ds_observacao_w, ie_interv_farmacia_p);
					
					if (nm_tabela_w = 'PRESCR_PROCEDIMENTO') then
						open c02;
						loop
						fetch c02 into	nr_seq_proc_prescr_w,
										ie_suspenso_w;
						exit when c02%notfound;
						begin
							if (ie_suspenso_w = 'S') then
								ds_lista_proc_suspenso_w := substr(ds_lista_proc_suspenso_w||nr_prescricao_w||','||nr_seq_proc_prescr_w||';',1,4000);
							end if;
						end;
						end loop;
						close c02;
					end if;			
					
				end loop;
				close c01;
				
				ie_tipo_dieta_w := '';
				end;
			end if;
			
			end;
		end if;
		
		end;
	end loop;
	
	ds_locale_user_w := pkg_i18n.get_user_locale;
    if (ds_locale_user_w = 'ja_JP') then
		obter_param_usuario(2314, 46, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_param_46_w); 
		if (ie_param_46_w = 'S') then
            CPOE_ORDER_UNIT_SUSPENSION(nr_atendimento_p, lista_itens_p, nm_usuario_p);
        end if;
    end if;
end if;

ds_lista_proc_susp_p := ds_lista_proc_suspenso_w;

Obter_param_Usuario(281,1590, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_prescr_evol_w);
if (ie_gerar_prescr_evol_w = 'S') then
CPOE_SOAP_DATA_SUSPENSION(nr_atendimento_p, lista_itens_p, nm_usuario_p);
end if;
commit;

exception
when others then
	rollback;
	ds_erro_w := substr(sqlerrm,1,1000);
	gravar_log_cpoe('CPOE_SUSPENDER_ITEM_PRESCR exception: '||ds_erro_w||
			' lista_itens_p: '||lista_itens_p||
			' nm_usuario_p: '||nm_usuario_p||
			' nr_seq_motivo_susp_p: '||nr_seq_motivo_susp_p||
			' ds_motivo_susp_p: '||ds_motivo_susp_p||
			' ie_interv_farmacia_p: '||ie_interv_farmacia_p||
			' ds_lista_proc_susp_p: '||ds_lista_proc_susp_p||
			' ie_angular_p: '||ie_angular_p||
			' ie_atualiza_baixa_alta_p: '||ie_atualiza_baixa_alta_p,nr_atendimento_p);
			
	commit;
end CPOE_Suspender_item_prescr;
/
