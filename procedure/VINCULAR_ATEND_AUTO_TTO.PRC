create or replace
procedure vincular_atend_auto_tto(	nr_seq_atendimento_p	Number,							
									cd_estabelecimento_p	Number,
									nm_usuario_p			Varchar2) is 

						
cd_pessoa_fisica_w	prescr_medica.cd_pessoa_fisica%type;
nr_atendimento_w	prescr_medica.nr_atendimento%type;
nr_prescricao_w		prescr_medica.nr_prescricao%type;
						
begin

select	max(a.cd_pessoa_fisica)	
into	cd_pessoa_fisica_w						
from	prescr_medica a
where	a.nr_prescricao in	(	select	max(b.nr_prescricao)
								from	paciente_atendimento b
								where	b.nr_seq_atendimento = nr_seq_atendimento_p);
								
if	(cd_pessoa_fisica_w is not null) then
	nr_atendimento_w := obter_ultimo_atendimento_estab(cd_pessoa_fisica_w, cd_estabelecimento_p);		
end if;						

if	(nr_atendimento_w is not null) then

	vincular_atend_quimioterapia(nr_seq_atendimento_p, nr_atendimento_w);	
	
	commit;
end if;

end vincular_atend_auto_tto;
/
