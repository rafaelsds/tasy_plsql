create or replace
procedure pls_conta_atualizar_material	(nr_seq_conta_p			number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is

nr_seq_conta_mat_w		number(10);
cd_material_imp_w		number(10);
ie_origem_preco_imp_w		number(2);
cd_brasindice_w			varchar2(15);
nr_seq_material_w		number(10);
cd_simpro_w			number(15);

cursor c01 is
	select	nr_sequencia,
		somente_numero(cd_material_imp),
		ie_origem_preco_imp
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_p
	and	nr_seq_material is null;

begin

open c01;
loop
fetch c01 into	nr_seq_conta_mat_w,
		cd_material_imp_w,
		ie_origem_preco_imp_w;
exit when c01%notfound;

	nr_seq_material_w	:= null;


	if	(ie_origem_preco_imp_w = 5) then

		select	nvl(max(cd_tiss),'0')
		into	cd_brasindice_w
		from	brasindice_preco
		where	cd_tiss	= cd_material_imp_w;

		if	(cd_brasindice_w <> '0') then

			select	max(nr_sequencia)
			into	nr_seq_material_w
			from	pls_material
			where	cd_tiss_brasindice	= cd_brasindice_w;
		end if;

	elsif	(ie_origem_preco_imp_w = 12) then

		select	nvl(max(cd_simpro),0)
		into	cd_simpro_w
		from	simpro_preco
		where	cd_simpro	= cd_material_imp_w;

		if	(cd_simpro_w > 0) then

			select	max(nr_sequencia)
			into	nr_seq_material_w
			from	pls_material
			where	cd_simpro	= cd_simpro_w;

		end if;

	elsif	(ie_origem_preco_imp_w in (95,96,99)) then

		select	max(nr_sequencia)
		into	nr_seq_material_w
		from	pls_material
		where	nr_sequencia	= cd_material_imp_w;

	end if;

	if	(nr_seq_material_w is not null) then

		update	pls_conta_mat
		set	nr_seq_material	= nr_seq_material_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_conta_mat_w;

	end if;
end loop;
close c01;

commit;

end pls_conta_atualizar_material;
/