/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Grava a informa��o do nome e data do arquivo gerado pelo A700
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure ptu_gravar_nome_arq_ger_a700
		(	nr_seq_serv_pgto_p	number,
			nm_arquivo_p		varchar2,
			nm_usuario_p		Varchar2) is 

begin

update	ptu_servico_pre_pagto
set	dt_geracao_arquivo	= sysdate,
	nm_arquivo		= nm_arquivo_p
where	nr_sequencia		= nr_seq_serv_pgto_p;

commit;

end ptu_gravar_nome_arq_ger_a700;
/