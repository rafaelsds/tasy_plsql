create or replace
procedure vincular_titulo_transacao(
			nr_titulo_p	number,
			nr_sequencia_p  number) is 
begin
if	(nr_titulo_p is not null) and
	(nr_sequencia_p is not null) then
	begin
	update  movto_trans_financ
	set 	nr_seq_titulo_receber = nr_titulo_p
	where   nr_sequencia = nr_sequencia_p;
	commit;
	end;
end if;
end vincular_titulo_transacao;
/