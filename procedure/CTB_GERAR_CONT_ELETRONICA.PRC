create or replace
procedure ctb_gerar_cont_eletronica(	cd_estabelecimento_p	number,
				cd_empresa_p			number,
				nr_sequencia_p			varchar2,
				ie_tipo_arquivo_p		varchar2,
				dt_inicial_p 			date,
				dt_referencia_p			date,
				dt_final_p				date,
				nr_seq_regra_p			number,
				nm_usuario_p			varchar2)  is
/* Campos para tabela de cat�logo */
cd_versao_w		varchar(20);
cd_cgc_w		varchar2(14);
dt_inicial_w		date;
/* dt_ano_inicial_w		varchar2(4); */
nr_certificado_sat_w	varchar2(20);
dt_final_w		date;
ie_tipo_envio_w		varchar2(2);
ie_tipo_solicitacao_w	varchar2(15);
nr_ordem_w		varchar(13);
dt_inicial_sup_w   	date;
dt_final_sup_w  	date;
nr_seq_catalogo_w 	number;

/* Campos para tabela de contas */
cd_classificacao_w		varchar(40);
cd_agrupador_w		varchar(40);
cd_classif_superior_w	varchar2(255);
cd_conta_contabil_w 	varchar2(20);
ds_conta_contabil_w	varchar2(255);
dt_inicio_vigencia_w	date;
dt_fim_vigencia_w		date;
ie_debito_credito_w		varchar2(2);
vl_credito_w		number(15,2);
vl_debito_w		number(15,2);
vl_saldo_w		number(15,2);
vl_saldo_ant_w		number(15,2);
vl_saldo_final_w		number(15,2);
vl_saldo_inicial_w		number(15,2);

/*	Cursores do cat�logo */
cursor C01 is
SELECT	a.cd_versao,
	SUBSTR(obter_dados_pf_pj(NULL, b.cd_cgc,'RFC'),1,255) cd_cgc,
	SUBSTR(a.nr_certificado_sat, 1, 20) nr_certificado_sat,
    a.dt_final,
	a.dt_inicial
FROM	ctb_regra_arquivo_mex a,
	estabelecimento b
WHERE	a.cd_empresa	 = b.cd_empresa
AND	b.cd_estabelecimento = nvl(a.cd_estabelecimento,b.cd_estabelecimento)
AND	nvl(cd_estabelecimento_p, b.cd_estabelecimento) = b.cd_estabelecimento
AND	substr(obter_dados_pf_pj(null,b.cd_cgc,'RFC'),1,255) is not null
AND	b.cd_empresa 		= cd_empresa_p
AND	a.nr_sequencia		= nr_sequencia_p
AND	EXISTS (SELECT	1
            FROM	ctb_balancete_v x
            WHERE	x.cd_estabelecimento = b.cd_estabelecimento
            AND	x.dt_referencia BETWEEN a.dt_inicial AND a.dt_final
            );

cursor C02 is
SELECT 	a.cd_versao, /*Vercion*/
	nvl(SUBSTR(obter_dados_pf_pj(NULL, b.cd_cgc,'RFC'),1,255),'X') cd_cgc, /*RFC*/
 	a.ie_tipo_envio, /*TipoEnvio*/
	a.dt_final, /*FechaModBal*/
 	SUBSTR(a.nr_certificado_sat, 1, 20) nr_certificado_sat, /*Certificado*/
	a.dt_inicial,
	a.dt_inicial dt_inicial_sup,
	a.dt_final dt_final_sup
FROM 	ctb_regra_arquivo_mex a,
  	estabelecimento b 
WHERE	a.cd_empresa	 = b.cd_empresa
AND	b.cd_estabelecimento = nvl(a.cd_estabelecimento,b.cd_estabelecimento)
AND	a.nr_sequencia = nr_sequencia_p
AND	substr(obter_dados_pf_pj(null,b.cd_cgc,'RFC'),1,255) is not null
AND	exists (
		SELECT	1
		FROM	ctb_balancete_v x
		WHERE	x.cd_estabelecimento = b.cd_estabelecimento
		AND	x.dt_referencia between a.dt_inicial AND a.dt_final
		)
GROUP BY
	a.cd_versao
	, substr(obter_dados_pf_pj(null,b.cd_cgc,'RFC'),1,255)	
	, a.ie_tipo_envio
	, a.dt_final
	, substr(a.nr_certificado_sat, 1, 20)
	, a.nr_sequencia
	, a.dt_inicial
	, to_char(a.dt_final,'dd/mm/yyyy')
	, b.cd_empresa;

cursor C03 is
SELECT 	a.cd_versao, /*Vercion */
	SUBSTR(obter_dados_pf_pj(NULL, b.cd_cgc,'RFC'),1,255) cd_cgc, /*RFC */
	a.ie_tipo_solicitacao, /*TipoSolicitud*/
	decode(a.ie_tipo_solicitacao, 'AF', a.nr_ordem, 'FC', a.nr_ordem, NULL) nr_ordem, /*NumOrden*/
	SUBSTR(a.nr_certificado_sat, 1, 20) nr_certificado_sat, /*noCertificado */
	a.dt_inicial
FROM 	ctb_regra_arquivo_mex a,
	estabelecimento b
WHERE 	b.cd_estabelecimento = nvl(a.cd_estabelecimento,b.cd_estabelecimento)
AND 	b.cd_empresa	 	= a.cd_empresa
AND 	a.nr_sequencia		= nr_sequencia_p
AND 	a.cd_empresa 		= cd_empresa_p
AND	EXISTS (SELECT	1
            FROM	ctb_balancete_v x
            WHERE	x.cd_estabelecimento = b.cd_estabelecimento
            AND	x.dt_referencia BETWEEN a.dt_inicial AND a.dt_final
            );

/* Cursores para Contas  */
cursor C04 is
SELECT   a.cd_classificacao, 
        (SELECT NVL(MAX(y.cd_classif_ecd),'0') 
         FROM   conta_contabil_classif_ecd y 
         WHERE  y.cd_conta_contabil     = a.cd_conta_contabil) cd_agrupador, 
        SUBSTR(d.DS_CONTA_REFERENCIAL, 1, 200) ds_conta_contabil, 
        NVL(a.cd_classif_superior, '1') cd_classif_superior, 
        a.cd_conta_contabil, 
        DECODE(b.ie_debito_credito, 'C', 'A', b.ie_debito_credito) ie_debito_credito, 
        a.dt_inicio_vigencia, 
        a.dt_fim_vigencia 
FROM    conta_contabil a, 
        ctb_grupo_conta b,
		CONTA_CONTABIL_CLASSIF_ECD c,
		CONTA_CONTABIL_REFERENCIAL d,
        estabelecimento e
WHERE   b.cd_grupo = a.cd_grupo 
AND     b.cd_empresa = a.cd_empresa 
AND     d.CD_CLASSIFICACAO = c.cd_classif_ecd
AND     c.cd_conta_contabil = a.cd_conta_contabil
AND     a.cd_empresa = cd_empresa_p 
AND     SUBSTR(obter_Se_conta_vigente2(a.cd_conta_contabil, a.dt_inicio_vigencia, a.dt_fim_vigencia, dt_inicial_p),1, 1) = 'S' 
AND     d.cd_empresa = a.cd_empresa
AND     SUBSTR(obter_dados_pf_pj(NULL, e.cd_cgc,'RFC'),1,255) = cd_cgc_w
AND	    exists ( SELECT	1
                FROM	ctb_balancete_v x
                WHERE	x.cd_estabelecimento = e.cd_estabelecimento
                AND x.cd_conta_contabil = a.cd_conta_contabil
                AND	x.dt_referencia BETWEEN dt_inicial_w AND dt_final_w
                )
ORDER BY cd_classif_superior,a.cd_classificacao;

cursor C05 is
SELECT 	a.cd_classificacao,
        sum(a.vl_saldo_ant) vl_saldo_ant,
        sum(a.vl_debito) vl_debito,
        sum(a.vl_credito) vl_credito,
        sum(a.vl_saldo) vl_saldo
FROM 	ctb_balancete_v a,
		CONTA_CONTABIL_CLASSIF_ECD b,
        estabelecimento c
WHERE	a.ie_normal_encerramento = 'N'
AND b.cd_conta_contabil = a.cd_conta_contabil
AND	a.cd_estabelecimento = c.cd_estabelecimento
AND	substr(obter_dados_pf_pj(null,c.cd_cgc,'RFC'),1,255) = cd_cgc_w
AND	a.dt_referencia BETWEEN to_date(dt_inicial_sup_w,'dd/mm/yy') AND fim_dia(to_date(dt_final_sup_w,'dd/mm/yy'))
GROUP BY a.cd_classificacao
ORDER BY a.cd_classificacao;

cursor C06 is 
SELECT 	c.cd_conta_contabil, /*NumCta*/
	SUBSTR(rps_substituir_caractere(c.ds_conta_contabil), 1, 100) ds_conta_contabil, /*DesCta*/
	NVL(SUM(c.vl_saldo_ant),0) vl_saldo_ini, /*SaldoIni*/
	NVL(SUM(c.vl_saldo),0) vl_saldo_fim /*SaldoFin*/
FROM   	ctb_balancete_v c,
        estabelecimento e
WHERE  	c.vl_movimento <> 0
AND    	c.ie_normal_encerramento = 'N'
and 	c.ie_tipo = 'A'
AND    	c.dt_referencia BETWEEN dt_inicial_w AND dt_final_p
and 	c.cd_empresa 		= cd_empresa_p
and		substr(obter_dados_pf_pj(null,e.cd_cgc,'RFC'),1,255) = cd_cgc_w
AND 	e.cd_estabelecimento=c.cd_estabelecimento
GROUP BY c.cd_conta_contabil,
	 c.ds_conta_contabil
ORDER BY c.cd_conta_contabil;


begin
	
	delete from w_ctb_catalogo 
	where nr_seq_regra = nr_seq_regra_p
	and    ie_tipo_arquivo = ie_tipo_arquivo_p;
	
	delete from w_ctb_contas 
	where nr_seq_regra = nr_seq_regra_p
	and    ie_tipo_arquivo = ie_tipo_arquivo_p;

	if (ie_tipo_arquivo_p = 'A') then
		
		open C01;	
		loop
		fetch C01 into
			cd_versao_w,
			cd_cgc_w,
			nr_certificado_sat_w,
            dt_final_w,
			dt_inicial_w;
		exit when C01%notfound;
			begin
				gerar_w_ctb_catalogo(cd_versao_w,
						    cd_cgc_w,
						    dt_inicial_w,
						    nr_certificado_sat_w,
						    ie_tipo_arquivo_p,
					 	    null, /* dt_final_p */
						    null, /* ie_tipo_envio_p */
						    null, /* ie_tipo_solicitacao_p */
						    null, /* nr_ordem_p */
						    nr_seq_regra_p,
						    nm_usuario_p,
						    nr_seq_catalogo_w);
			end;
			
            open C04;
            loop
            fetch C04 into
                cd_classificacao_w,
                cd_agrupador_w,
                ds_conta_contabil_w,
                cd_classif_superior_w,
                cd_conta_contabil_w,
                ie_debito_credito_w,
                dt_inicio_vigencia_w,
                dt_fim_vigencia_w;
            exit when C04%notfound;	
                begin
                    gerar_w_ctb_conta(cd_classificacao_w,
                            cd_agrupador_w,
                            ds_conta_contabil_w,
                            cd_classif_superior_w,
                            cd_conta_contabil_w,
                            ie_debito_credito_w,
                            dt_inicio_vigencia_w,
                            dt_fim_vigencia_w,
                            ie_tipo_arquivo_p,
                            null, /* vl_saldo_ant_p */
                            null, /* vl_debito_p */
                            null, /* vl_credito_p */
                            null, /* vl_saldo_p*/
                            null, /* vl_saldo_inicial_p*/
                            null, /* vl_saldo_final_p*/
                            nr_seq_regra_p,
                            nm_usuario_p,
			    nr_seq_catalogo_w);	
                end;
            end loop;
            close C04;
            
		end loop;
		close C01;
        
	elsif (ie_tipo_arquivo_p = 'C') then
		
		open C02;
		loop
		fetch C02 into
			cd_versao_w,
			cd_cgc_w,
			ie_tipo_envio_w,
			dt_final_w,
			nr_certificado_sat_w,
			dt_inicial_w,
            dt_inicial_sup_w,
            dt_final_sup_w;
            
		exit when C02%notfound;
			begin
				gerar_w_ctb_catalogo(cd_versao_w,
						    cd_cgc_w,
						    dt_inicial_w,
						    nr_certificado_sat_w,
						    ie_tipo_arquivo_p,
						    dt_final_w,
						    ie_tipo_envio_w,
						    null, 	/* ie_tipo_solicitacao_p */
						    null,	/*	nr_ordem_p	*/
						    nr_seq_regra_p,
						    nm_usuario_p,
						    nr_seq_catalogo_w);				
			end;
    
		open C05;        
		loop
		fetch C05 into
			cd_classificacao_w,
			vl_saldo_ant_w,
			vl_debito_w,
			vl_credito_w,
			vl_saldo_w;
		exit when C05%notfound;
			begin
				gerar_w_ctb_conta(cd_classificacao_w,
						null, /* cd_agrupador_p */
						null, /* ds_conta_contabil_p */
						null, /* cd_classif_superior_p*/
						null, /* cd_conta_contabil_p */
						null, /* ie_debito_credito_p */
						null, /* dt_inicio_vigencia_p*/
						null, /* dt_fim_vigencia_p */
						ie_tipo_arquivo_p,
						vl_saldo_ant_w,
						vl_debito_w,
						vl_credito_w,
						vl_saldo_w,
						null, /* vl_saldo_inicial_p*/
						null, /* vl_saldo_final_p*/
						nr_seq_regra_p,
						nm_usuario_p,
						nr_seq_catalogo_w);
			end;
		end loop;
		close C05;
        
        end loop;
		close C02;
		
	elsif (ie_tipo_arquivo_p = 'F') then
		
		open C03;
		loop
		fetch C03 into
			cd_versao_w,
			cd_cgc_w,
			ie_tipo_solicitacao_w,
			nr_ordem_w,
			nr_certificado_sat_w,
			dt_inicial_w;
		exit when C03%notfound;
			begin
				gerar_w_ctb_catalogo(cd_versao_w,
						    cd_cgc_w,
						    dt_inicial_w,
						    nr_certificado_sat_w,
						    ie_tipo_arquivo_p,
						    null, /*dt_final_p */
						    null, /* ie_tipo_envio_p*/
						    ie_tipo_solicitacao_w,
						    nr_ordem_w,
						    nr_seq_regra_p,
						    nm_usuario_p,
						    nr_seq_catalogo_w);		
			end;		
		
            open C06;            
            loop
            fetch C06 into
                cd_conta_contabil_w,
                ds_conta_contabil_w,
                vl_saldo_inicial_w,
                vl_saldo_final_w;
            exit when C06%notfound;
                begin
                    gerar_w_ctb_conta(null, /* cd_classificacao_p */
                            null, /* cd_agrupador_p */
                            ds_conta_contabil_w, /* ds_conta_contabil_p */
                            null, /* cd_classif_superior_p*/
                            cd_conta_contabil_w, /* cd_conta_contabil_p */
                            null, /* ie_debito_credito_p */
                            null, /* dt_inicio_vigencia_p*/
                            null, /* dt_fim_vigencia_p */
                            ie_tipo_arquivo_p,
                            null, /* vl_saldo_ant_p*/
                            null,  /* vl_debito_p*/
                            null, /* vl_credito_p*/
                            null,   /* vl_saldo_p*/
                            vl_saldo_inicial_w, /* vl_saldo_inicial_p*/
                            vl_saldo_final_w, /* vl_saldo_final_p*/
                            nr_seq_regra_p,
                            nm_usuario_p,
                            nr_seq_catalogo_w);
                end;
            end loop;
            close C06;
        
        end loop;
		close C03;
		
	end if;
	commit;
end ctb_gerar_cont_eletronica;
/