create or replace
procedure pls_consistir_reajuste
			(	nr_seq_reajuste_p	Number,
				ie_tipo_consistencia_p	Varchar2) is 

/*ie_tipo_consistencia_p
	'RP' = Realizado atrav�s da fun��o OPS - Reajuste de pre�o dos benefici�rios
	'GC' = Realizado atrav�s da fun��o OPS - Gest�o de contratos
	'RI' = Reajuste individual
*/
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;
ie_indice_reajuste_contrato_w	Number(10);
dt_reajuste_w			Date;
nr_seq_contrato_w		Number(10);
nr_contrato_w			Number(10);

begin

begin
select	ie_indice_reajuste_contrato,
	dt_reajuste
into	ie_indice_reajuste_contrato_w,
	dt_reajuste_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_p;
exception
when others then
	ie_indice_reajuste_contrato_w	:= null;
	dt_reajuste_w			:= null;
end;

if	(nvl(ie_indice_reajuste_contrato_w,0) <> 0) then
	begin
	select	max(vl_cotacao)
	into	vl_cotacao_w
	from	moeda		a,
		cotacao_moeda	b
	where	a.cd_moeda	= ie_indice_reajuste_contrato_w
	and	a.cd_moeda	= b.cd_moeda
	and	trunc(b.dt_cotacao, 'month')	= trunc(dt_reajuste_w, 'month');
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(213712);
	end;

	if	(nvl(vl_cotacao_w,0) <> 0) then
		update	pls_reajuste
		set	tx_reajuste	= vl_cotacao_w
		where	nr_sequencia	= nr_seq_reajuste_p;
	end if;
end if;


/*aaschlote OS - 278055 28/12/2010 - Incluir a sequencia do contrato logo depois de salvar*/
if	(ie_tipo_consistencia_p	= 'RP') then
	begin
	select	max(nr_contrato)
	into	nr_contrato_w
	from	pls_reajuste
	where	nr_sequencia	= nr_seq_reajuste_p;
	exception
	when others then
		nr_contrato_w	:= null;
	end;

	
	begin
	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato	= nr_contrato_w;
	exception
	when others then
		nr_seq_contrato_w	:= null;
	end;
	
	update	pls_reajuste
	set	nr_seq_contrato	= nr_seq_contrato_w
	where	nr_sequencia	= nr_seq_reajuste_p;

elsif	(ie_tipo_consistencia_p = 'GC') then
	begin
	select	max(nr_seq_contrato)
	into	nr_seq_contrato_w
	from	pls_reajuste
	where	nr_sequencia	= nr_seq_reajuste_p;
	exception
	when others then
		nr_seq_contrato_w	:= 0;
	end;
	
	if	(nvl(nr_seq_contrato_w,0) <> 0) then
		select	max(nr_contrato)
		into	nr_contrato_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;
		
		if	(nvl(nr_contrato_w,0) <> 0) then
			update	pls_reajuste
			set	nr_contrato	= nr_contrato_w
			where	nr_sequencia	= nr_seq_reajuste_p;
		end if;
	end if;
end if;

commit;

end pls_consistir_reajuste;
/