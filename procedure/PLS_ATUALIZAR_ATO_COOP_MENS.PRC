/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Atualizar o ato cooperado da mensalidade caso a mensalidade j� esteja gerada
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [  X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 


create or replace
procedure pls_atualizar_ato_coop_mens
		(	nr_seq_mensalidade_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 

begin

delete	pls_mensalidade_ato_coop
where	nr_seq_mensalidade	= nr_seq_mensalidade_p;

pls_gerar_valor_ato_cooperado(nr_seq_mensalidade_p,null);

end pls_atualizar_ato_coop_mens;
/
