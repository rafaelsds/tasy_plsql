create or replace
procedure agenda_paciente_atribfocuslost(
		cd_agenda_p		number,
		cd_pessoa_fisica_p		varchar2,
		dt_agenda_p		date,
		dt_nascimento_p		date,
		cd_convenio_p		number,
		cd_plano_p		varchar2,
		cd_categoria_p		varchar2,
		cd_usuario_conv_p		varchar2,
		cd_procedimento_p		number,
		ie_origem_proced_p	number,
		nr_seq_proc_interno_p	number,
		qt_idade_paciente_p	out varchar2,
		qt_convenio_doc_p		out number,
		cd_setor_atendimento_p	out number,
		ie_se_conv_doc_lib_p	out varchar2,
		ie_se_perm_pf_classif_p	out varchar2,
		cd_plano_padrao_p		out varchar2,
		nr_digitos_codigo_p		out varchar2,
		ds_rotina_digito_p		out varchar2,
		cd_plano_cod_usu_conv_p	out varchar2,
		ds_plano_p		out varchar2,
		cd_medico_regra_p		out varchar2,
		ds_msg_p	out	varchar2,
		hr_inicio_p	date default null) is

qt_idade_paciente_w	varchar2(3)	:= '';
qt_conv_doc_w		number(10,0)	:= 0;
qt_conv_doc_atend_w	number(10,0)	:= 0;
qt_convenio_doc_w	number(10,0)	:= 0;
qt_conv_plano_w		number(10,0)	:= 0;
ie_se_conv_doc_lib_w	varchar2(1);
ie_se_perm_pf_classif_w	varchar2(80);
cd_plano_padrao_w	varchar2(10);
nr_digitos_codigo_w	varchar2(15);
ds_rotina_digito_w		varchar2(50);
cd_plano_cod_usu_conv_w	varchar2(10);
ds_plano_w		varchar2(80);
cd_medico_regra_w	varchar2(10);
cd_setor_atend_w		number(5,0)	:= 0;
ds_msg_w		varchar2(255);

begin
if	(cd_convenio_p > 0) and
	(cd_usuario_conv_p is not null) then
	select	substr(consiste_carteira_usuario(cd_convenio_p, cd_usuario_conv_p), 1, 255)
	into	ds_msg_w
	from	dual;	
end if;

if	(dt_nascimento_p is not null) then
	qt_idade_paciente_w	:= substr(obter_idade(dt_nascimento_p, sysdate, 'A'),1,3);
end if;

if	(cd_convenio_p > 0) then
	begin
	select	count(*)
	into	qt_conv_doc_w
	from	convenio_documento
	where	cd_convenio = cd_convenio_p;

	select	count(*)
	into	qt_conv_doc_atend_w
	from	convenio_doc_atend
	where	cd_convenio = cd_convenio_p;

	qt_convenio_doc_w	:= qt_conv_doc_w + qt_conv_doc_atend_w;	
	ie_se_conv_doc_lib_w	:= substr(Obter_Se_Conv_Doc_Lib(cd_convenio_p,null,null, cd_plano_p),1,1);
	cd_plano_cod_usu_conv_w	:= substr(obter_plano_cod_usuario_conv(cd_convenio_p, cd_usuario_conv_p),1,10);

	if	(cd_categoria_p is not null) then
		select	max(cd_plano_padrao)
		into	cd_plano_padrao_w
		from	categoria_convenio
		where	cd_convenio	= cd_convenio_p
		and	cd_categoria	= cd_categoria_p;
	end if;

	select	nr_digitos_codigo,
		ds_rotina_digito
	into	nr_digitos_codigo_w,
		ds_rotina_digito_w
	from 	convenio
	where	cd_convenio = cd_convenio_p;

	select	count(*)	
	into	qt_conv_plano_w
	from	convenio_plano
	where	cd_convenio	= cd_convenio_p
	and	cd_plano	= cd_plano_p;

	if	(qt_conv_plano_w > 0) then
		select	ds_plano
		into	ds_plano_w
		from	convenio_plano
		where	cd_convenio	= cd_convenio_p
		and	cd_plano	= cd_plano_p;
	end if;
	end;
end if;

if	(cd_agenda_p > 0) then
	begin
	if	(cd_pessoa_fisica_p is not null) and
		(dt_agenda_p is not null) then
		ie_se_perm_pf_classif_w	:= substr(Obter_Se_Perm_PF_Classif(820, cd_agenda_p, cd_pessoa_fisica_p, nvl(hr_inicio_p,dt_agenda_p), 'DS'),1,255);
	end if;

	if	((cd_procedimento_p > 0) or
		 (nr_seq_proc_interno_p > 0)) and
		(ie_origem_proced_p > 0) then
		cd_medico_regra_w	:= substr(obter_medico_por_exame_regra(cd_procedimento_p, ie_origem_proced_p, nr_seq_proc_interno_p, cd_agenda_p, dt_agenda_p),1,255);
	end if;
	if	(cd_procedimento_p > 0) and
		(ie_origem_proced_p >0) then
		begin
		select	max(cd_setor_exclusivo)
		into	cd_setor_atend_w
		from	procedimento
		where	cd_procedimento = cd_procedimento_p
		and	ie_origem_proced = ie_origem_proced_p;
		end;
	end if;
	end;
end if;

qt_idade_paciente_p	:= qt_idade_paciente_w;
qt_convenio_doc_p		:= qt_convenio_doc_w;
ie_se_conv_doc_lib_p	:= ie_se_conv_doc_lib_w;
ie_se_perm_pf_classif_p	:= ie_se_perm_pf_classif_w;
cd_plano_padrao_p		:= cd_plano_padrao_w;
nr_digitos_codigo_p		:= nr_digitos_codigo_w;
ds_rotina_digito_p		:= ds_rotina_digito_w;
cd_plano_cod_usu_conv_p	:= cd_plano_cod_usu_conv_w;
ds_plano_p		:= ds_plano_w;
cd_medico_regra_p		:= cd_medico_regra_w;
cd_setor_atendimento_p	:= cd_setor_atend_w;
ds_msg_p		:= ds_msg_w;

end agenda_paciente_atribfocuslost;
/
