create or replace procedure CPOE_Copiar_itens_atend(	dt_referencia_p			date,
									nm_usuario_p			varchar2,
									cd_perfil_p				number,
									cd_estabelecimento_p	number) is

dt_ref_copia_w				date;
dt_ref_cop_fut_w			date;
cd_estabelecimento_w			number(10);
nm_usuario_w				usuario.nm_usuario%type;
nr_atendimento_w			atendimento_paciente.nr_atendimento%type;
nr_atendimento_old_w			atendimento_paciente.nr_atendimento%type;
cd_setor_atendimento_w			setor_atendimento.cd_setor_atendimento%type;
cd_perfil_ativo_w			atendimento_paciente.cd_perfil_ativo%type;
cd_pessoa_fisica_w			pessoa_fisica.cd_pessoa_fisica%type;
dt_alta_w				atendimento_paciente.dt_alta%type;
ie_tipo_item_w				cpoe_horas_fut_copia.ie_tipo_item%type;
qt_horas_w				cpoe_horas_fut_copia.qt_horas%type;
qt_hors_copia_w				number(10);
nr_seq_log_w				number(10);
ie_susp_prescricoes_alta_w		varchar2(1);
qt_itens_w				number(15);
ie_adep_w				prescr_medica.ie_adep%type;
dt_inicio_copia_w			date;
ie_agrupar_lote_copia_w 		parametros_farmacia.ie_agrupar_lote_copia%type;
qt_hors_before_w			number(3);
dt_inicio_regra_w			date;
dt_fim_regra_w				date;
is_antecipated_batch_w			varchar2(1);
ds_locale_w				varchar2(20);
sql_w               varchar2(300);
ie_exit_loop_w       varchar2(1);

cursor c01 is
select	distinct
		nr_atendimento,
		cd_pessoa_fisica,
		nm_usuario,
		cd_perfil_ativo,
		ie_adep
from	(	select	a.nr_atendimento,
					a.cd_pessoa_fisica,
					a.nm_usuario_nrec nm_usuario,
					a.cd_perfil_ativo,
					nvl(ie_adep, 'S') ie_adep
			from	cpoe_material a
			where	((decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) >= a.dt_prox_geracao + (qt_hors_copia_w/24)) or 
					 (decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) is null))
			and		a.dt_prox_geracao between dt_ref_copia_w - 3 and dt_ref_copia_w - 1/86400
			and		a.nr_atendimento is not null
			and		a.dt_liberacao is not null
			and		nvl(a.nr_seq_receita_amb,0) = 0
			and		nvl(a.ie_retrogrado,'N') = 'N'
			and		((((nvl(a.ie_evento_unico,'N') = 'N') and 
					   (a.nr_seq_adicional is null) and 
					   (a.nr_seq_ataque is null)) and 
					  (nvl(a.ie_material,'N') = 'N')) or
					 (not exists (	select	1
									from	prescr_medica y,
											prescr_material z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_mat_cpoe = a.nr_sequencia)))
			and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (qt_hors_copia_w/24)))))
			and		nvl(a.ie_material,'N') = 'N'
			and		nvl(a.cd_funcao_origem,2314) = 2314
			and		((cpoe_obter_se_copia_item_term(a.ds_horarios,
													a.cd_intervalo,
													a.dt_prox_geracao + (qt_hors_copia_w/24),
													decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim))) = 'S') or
						((a.dt_fim is null) and a.ie_duracao = 'C'))	
			union all/*weekly dispensation*/
			select	a.nr_atendimento,
					a.cd_pessoa_fisica,
					a.nm_usuario_nrec nm_usuario,
					a.cd_perfil_ativo,
					nvl(ie_adep, 'S') ie_adep
			from	cpoe_material a
			where	((decode(dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) >= (a.dt_prox_geracao + (qt_hors_copia_w/24))) or
					(decode(dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim)) is null))
			and		a.dt_prox_geracao  between dt_inicio_regra_w - (qt_hors_copia_w/24) and dt_fim_regra_w - (qt_hors_copia_w/24) - 1/86400
			and		dt_ref_copia_w > (dt_inicio_regra_w - 1)
			and		a.nr_atendimento is not null
			and 	nvl(is_antecipated_batch_w, 'N') = 'Y'
			and		a.dt_liberacao is not null
			and		nr_atendimento is not null
			and		nvl(a.nr_seq_receita_amb,0) = 0
			and		nvl(a.ie_retrogrado,'N') = 'N'
			and		((((nvl(a.ie_evento_unico,'N') = 'N') and
					(a.nr_seq_adicional is null) and
					(a.nr_seq_ataque is null)) and
					(nvl(a.ie_material,'N') = 'N')) or
					(not exists(	select	1
									from	prescr_medica y,
											prescr_material z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento is not null
									and		z.nr_seq_mat_cpoe = a.nr_sequencia )))		
			and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (qt_hors_copia_w/24)))))
			and		nvl(a.cd_funcao_origem,2314) = 2314
			and		((cpoe_obter_se_copia_item_term(a.ds_horarios, a.cd_intervalo, a.dt_prox_geracao + (qt_hors_copia_w/24), decode(a.dt_lib_suspensao, null, nvl(a.dt_fim_cih,a.dt_fim), nvl(a.dt_suspensao,a.dt_fim))) = 'S') or ((a.dt_fim is null) and a.ie_duracao = 'C'))
			union all
			select	a.nr_atendimento,
					a.cd_pessoa_fisica,
					a.nm_usuario_nrec,
					a.cd_perfil_ativo,
					nvl(ie_adep, 'S') ie_adep
			from	cpoe_dieta a
			where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= a.dt_prox_geracao + (qt_hors_copia_w/24)) or 
					 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
			and		a.dt_prox_geracao between dt_ref_copia_w - 3 and dt_ref_copia_w - 1/86400
			and		a.nr_atendimento is not null
			and		a.dt_liberacao is not null
			and		nvl(a.ie_retrogrado,'N') = 'N'
			and		((nvl(a.ie_evento_unico,'N') = 'N') or
					 (not exists (	select	1
									from	prescr_medica y
									where	y.nr_atendimento = a.nr_atendimento
									and		exists (	select	1
														from	prescr_material z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_dieta_cpoe = a.nr_sequencia
														union
														select	1
														from	prescr_dieta z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_dieta_cpoe = a.nr_sequencia
														union
														select	1
														from	rep_jejum z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_dieta_cpoe = a.nr_sequencia
														union
														select	1
														from	prescr_leite_deriv z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_dieta_cpoe = a.nr_sequencia
														union
														select	1
														from	nut_pac z
														where	z.nr_prescricao = y.nr_prescricao
														and		z.nr_seq_npt_cpoe = a.nr_sequencia))))
			and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (qt_hors_copia_w/24)))))
			and		nvl(a.ie_dose_unica,'N') = 'N'
			and		nvl(a.cd_funcao_origem,2314) = 2314
			and		((cpoe_obter_se_copia_item_term(a.ds_horarios,
													a.cd_intervalo,
													a.dt_prox_geracao + (qt_hors_copia_w/24),
													decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or
						((a.dt_fim is null) and a.ie_duracao = 'C'))
			union all
			select	a.nr_atendimento,
					a.cd_pessoa_fisica,
					a.nm_usuario_nrec,
					a.cd_perfil_ativo,
					nvl(ie_adep, 'S') ie_adep
			from	cpoe_recomendacao a
			where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= a.dt_prox_geracao + (qt_hors_copia_w/24)) or 
					 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
			and		a.dt_prox_geracao between dt_ref_copia_w - 3 and dt_ref_copia_w - 1/86400
			and		a.nr_atendimento is not null
			and		a.dt_liberacao is not null
			and		nvl(a.ie_retrogrado,'N') = 'N'
			and		((nvl(a.ie_evento_unico,'N') = 'N') or
					 (not exists(	select	1
									from	prescr_medica y,
											prescr_recomendacao z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_rec_cpoe = a.nr_sequencia)))
			and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (qt_hors_copia_w/24)))))			
			and		nvl(a.cd_funcao_origem,2314) = 2314
			and		((cpoe_obter_se_copia_item_term(a.ds_horarios,
													a.cd_intervalo,
													a.dt_prox_geracao + (qt_hors_copia_w/24),
													decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or
						((a.dt_fim is null) and a.ie_duracao = 'C'))
			union all
			select	a.nr_atendimento,
					a.cd_pessoa_fisica,
					a.nm_usuario_nrec,
					a.cd_perfil_ativo,
					nvl(ie_adep, 'S') ie_adep
			from	cpoe_gasoterapia a
			where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= a.dt_prox_geracao + (qt_hors_copia_w/24)) or 
					 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
			and		a.dt_prox_geracao between dt_ref_copia_w - 3 and dt_ref_copia_w - 1/86400
			and		a.nr_atendimento is not null
			and		a.dt_liberacao is not null
			and		nvl(a.ie_retrogrado,'N') = 'N'
			and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (qt_hors_copia_w/24)))))
			and		nvl(a.cd_funcao_origem,2314) = 2314
			and		((cpoe_obter_se_copia_item_term(a.ds_horarios,
													a.cd_intervalo,
													a.dt_prox_geracao + (qt_hors_copia_w/24),
													decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or
						((a.dt_fim is null) and a.ie_duracao = 'C'))
			union all
			select	a.nr_atendimento,
					a.cd_pessoa_fisica,
					a.nm_usuario_nrec,
					a.cd_perfil_ativo,
					nvl(ie_adep, 'S') ie_adep
			from	cpoe_procedimento a,
					proc_interno	b
			where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= a.dt_prox_geracao + (qt_hors_copia_w/24)) or 
					 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
			and		a.nr_seq_proc_interno = b.nr_sequencia
			and		a.dt_prox_geracao between dt_ref_copia_w - 3 and dt_ref_copia_w
			and		a.nr_atendimento is not null
			and		a.dt_liberacao is not null
			and		((nvl(b.ie_copiar_rep,'S') <> 'C') or
					 (not exists(	select	1
									from	prescr_medica y,
											prescr_procedimento z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_proc_cpoe = a.nr_sequencia)))
			and		nvl(a.ie_retrogrado,'N') = 'N'
			and		((nvl(a.ie_evento_unico,'N') = 'N') or
					 (not exists(	select	1
									from	prescr_medica y,
											prescr_procedimento z
									where	y.nr_prescricao = z.nr_prescricao
									and		y.nr_atendimento = a.nr_atendimento
									and		z.nr_seq_proc_cpoe = a.nr_sequencia)))
			and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (qt_hors_copia_w/24)))))
			and		nvl(a.cd_funcao_origem,2314) = 2314
			and		((cpoe_obter_se_copia_item_term(a.ds_horarios,
													a.cd_intervalo,
													a.dt_prox_geracao + (qt_hors_copia_w/24),
													decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or
						((a.dt_fim is null) and a.ie_duracao = 'C'))
			union all
			select	a.nr_atendimento,
					a.cd_pessoa_fisica,
					a.nm_usuario_nrec,
					a.cd_perfil_ativo,
					nvl(ie_adep, 'S') ie_adep
			from	cpoe_dialise a
			where	((decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) >= a.dt_prox_geracao + (qt_hors_copia_w/24)) or 
					 (decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim)) is null))
			and		a.dt_prox_geracao between dt_ref_copia_w - 3 and dt_ref_copia_w - 1/86400
			and		a.nr_atendimento is not null
			and		a.dt_liberacao is not null
			and		nvl(a.ie_retrogrado,'N') = 'N'
			and 	((ie_susp_prescricoes_alta_w = 'S') or (((coalesce(a.ie_baixado_por_alta, 'N') = 'N') or (coalesce(a.dt_alta_medico, sysdate) > a.dt_prox_geracao + (qt_hors_copia_w/24)))))
			and		nvl(a.cd_funcao_origem,2314) = 2314
			and		((cpoe_obter_se_copia_item_term(a.ds_horarios,
													null,
													a.dt_prox_geracao + (qt_hors_copia_w/24),
													decode(a.dt_lib_suspensao, null, a.dt_fim, nvl(a.dt_suspensao,a.dt_fim))) = 'S') or
						((a.dt_fim is null) and a.ie_duracao = 'C')))
order by nr_atendimento;

cursor c02 is
select	ie_tipo_item,
		qt_horas
from	cpoe_horas_fut_copia
where	nvl(cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
and		nvl(qt_horas,0) > 0;

	procedure adjust_properties_nls is
	begin
	
	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_LANGUAGE=''BRAZILIAN PORTUGUESE'''; 
	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_TERRITORY = ''BRAZIL''';
	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
	EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT = ''DD/MM/YYYY HH24:MI:SS'''; 
	
	end;

begin

gravar_log_cpoe('CPOE_COPIAR_ITENS_ATEND LOG - DT_REFERENCIA_P: '||to_char(dt_referencia_p,'dd/mm/yyyy hh24:mi:ss'));

commit;

adjust_properties_nls;
Obter_param_Usuario(3111, 162, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_susp_prescricoes_alta_w);

 begin
      sql_w := 'CALL GET_QT_HOURS_AFTER_COPY_CPOE(:1, :2, :3) INTO :qt_hors_copia_w';
      EXECUTE IMMEDIATE sql_w USING IN obter_perfil_ativo,
                                    IN nm_usuario_p,
                                    IN cd_estabelecimento_p,
                                    OUT qt_hors_copia_w;

  exception
    when others then
      qt_hors_copia_w := null;
  end;
  
  begin
      sql_w := 'CALL GET_QT_HOURS_BEFORE_COPY(:1, :2, :3) INTO :qt_hors_before_w';
      EXECUTE IMMEDIATE sql_w USING IN obter_perfil_ativo,
                                    IN nm_usuario_p,
                                    IN cd_estabelecimento_p,
                                    OUT qt_hors_before_w;
       
  exception
    when others then
      qt_hors_before_w := null;
  end;
  
  begin
      sql_w := 'CALL OBTER_HORA_REF_COPIA_CPOE_MD(:1, :2) INTO :dt_ref_copia_w';
      EXECUTE IMMEDIATE sql_w USING IN trunc(dt_referencia_p,'hh24'), 
                                    IN qt_hors_before_w,
                                    OUT dt_ref_copia_w;
       
  exception
    when others then
      dt_ref_copia_w := null;
  end;

dt_inicio_copia_w 	:= dt_referencia_p;
nr_atendimento_old_w := 0;

cpoe_get_antecipated_batch(dt_ref_copia_w, null, null, is_antecipated_batch_w, dt_inicio_regra_w, dt_fim_regra_w);

open c01;
loop
fetch c01 into	nr_atendimento_w,
                cd_pessoa_fisica_w,
                nm_usuario_w,
                cd_perfil_ativo_w,
                ie_adep_w;
exit when c01%notfound;
    begin
    adjust_properties_nls;
    nm_usuario_w			:= coalesce(nm_usuario_w, nm_usuario_p);

    if (philips_param_pck.get_nr_seq_idioma is null) then
        philips_param_pck.set_nr_seq_idioma(obter_nr_seq_idioma(nm_usuario_w));
	
	select	max(ds_language_tag)
	into	ds_locale_w
	from	tasy_idioma
	where	nr_sequencia = philips_param_pck.get_nr_seq_idioma;
	
	if	(ds_locale_w is not null) then
		pkg_i18n.set_user_locale(ds_locale_w);
	end if;
	
    end if;

    cd_perfil_ativo_w 		:=coalesce(cd_perfil_ativo_w, cd_perfil_p);
    cd_setor_atendimento_w 	:= obter_unidade_atendimento(nr_atendimento_w,'IA','CS');
    if	(nvl(nr_atendimento_w,0) <> nvl(nr_atendimento_old_w,0)) or 
        (nvl(nr_atendimento_old_w,0) = 0) then
        nr_atendimento_old_w	:= nr_atendimento_w;
        begin
        adjust_properties_nls;		
        cd_estabelecimento_w   := obter_estabelecimento_setor(cd_setor_atendimento_w);
        if	(cd_estabelecimento_w is null) then
            select	max(cd_estabelecimento)
            into	cd_estabelecimento_w
            from	atendimento_paciente
            where	nr_atendimento = nr_atendimento_w;
        end if;
        exception when others then
            cd_setor_atendimento_w := null;
        end;
    end if;

    select	max(dt_alta)
    into	dt_alta_w
    from	atendimento_paciente
    where	nr_atendimento	= nr_atendimento_w;
        
    if	(dt_alta_w	is null) then
        if (cd_estabelecimento_w is null) then
            cd_estabelecimento_w := cd_estabelecimento_p;
        end if;
        cpoe_copiar_itens( nr_atendimento_w, dt_ref_copia_w, nm_usuario_w, cd_perfil_ativo_w, null, cd_estabelecimento_w, cd_pessoa_fisica_w, 'N', ie_adep_w, dt_inicio_copia_w);
        
        dt_ref_cop_fut_w := dt_ref_copia_w;    
        open c02;
        loop
        fetch c02 into	ie_tipo_item_w,
                        qt_horas_w;
        exit when c02%notfound;
            begin           
            loop
                begin
               
                begin
                    sql_w := 'CALL OBTER_DEF_COP_FUT_CPOE_MD(:1) INTO :dt_ref_cop_fut_w';
                    EXECUTE IMMEDIATE sql_w USING IN dt_ref_cop_fut_w,                                                  
                                                  OUT dt_ref_cop_fut_w;                     
                exception
                  when others then
                    dt_ref_cop_fut_w := null;
                end;
                
                cpoe_copiar_itens( nr_atendimento_w, dt_ref_cop_fut_w, nm_usuario_w, cd_perfil_ativo_w, null, cd_estabelecimento_w, cd_pessoa_fisica_w, ie_tipo_item_w, ie_adep_w);
                begin
                    sql_w := 'CALL EXIT_LOOP_DT_REF_COPIA_MD(:1, :2, :3) INTO :ie_exit_loop_w';
                    EXECUTE IMMEDIATE sql_w USING IN dt_ref_cop_fut_w, 
                                                  IN dt_ref_copia_w, 
                                                  IN qt_horas_w,                                   
                                                  OUT ie_exit_loop_w;                     
                exception
                  when others then
                    ie_exit_loop_w := 'S';
                end;
                
                exit when (ie_exit_loop_w = 'S');
                --FIM MD 2
                end;
            end loop;
            end;
        end loop;
        close c02;
                
    end if;	
    end;
end loop;
close c01;

gerar_lote_agrupamento(0,0,cd_estabelecimento_p,nm_usuario_w);

begin

    select	count(*)
    into	qt_itens_w
    from	prescr_medica
    where	dt_atualizacao between dt_referencia_p and obter_data_maior(sysdate, dt_ref_copia_w)
    and	nr_prescricoes is not null
    and	cd_funcao_origem = 2314;
    
    if (qt_itens_w > 0) then
        gravar_log_cpoe('CPOE_COPIAR_ITENS_ATEND LOG - DT_REFERENCIA_P: '||to_char(dt_referencia_p,'dd/mm/yyyy hh24:mi:ss') || ' Qtd: ' || qt_itens_w);
        commit;
    end if;

    gravar_log_cpoe('CPOE_COPIAR_ITENS_ATEND LOG - IDENTIFICADOR PROCESSO  ', null, null, null, 'JOB');
    commit;

exception
	when others then
	null;
end;

end CPOE_Copiar_itens_atend;
/
