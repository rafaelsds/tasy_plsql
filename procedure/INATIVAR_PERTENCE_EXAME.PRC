create or replace
procedure Inativar_pertence_exame(nr_sequencia_p		number,
				  ds_tipo_p			varchar2,
			          nm_usuario_p			varchar2) is 
/* ds_tipo_p  	Exame ou Pertence	   */
begin
if (ds_tipo_p = 'Exame')  then
	update exame_paciente 
	set ie_situacao = 'I',
           	      nm_usuario = nm_usuario_p,
	      dt_atualizacao = sysdate
	where   nr_sequencia	=	nr_sequencia_p;
elsif (ds_tipo_p = 'Pertence') then
      begin
      update pertence_paciente 
      set ie_situacao = 'I',
	      nm_usuario = nm_usuario_p,
	      dt_atualizacao = sysdate
      where   nr_sequencia	=	nr_sequencia_p;
      end;
end if;	
	
commit;

end Inativar_pertence_exame;
/
