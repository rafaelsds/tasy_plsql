create or replace
procedure Consiste_Regra_Mat_quimio	(	cd_material_p	number) is 


cd_grupo_material_w	number(3,0);
cd_subgrupo_material_w	number(3,0);
cd_classe_material_w	number(5,0);
ie_permite_w			varchar2(10);

cursor	c01 is
	select	IE_PERMITE_PRESCR
	from	REGRA_PRESCR_MAT_QUIMIO
	where	nvl(cd_material, cd_material_p)				= cd_material_p
	and	nvl(CD_GRUPO_MAT, cd_grupo_material_w)		= cd_grupo_material_w
	and	nvl(CD_SUBGRUPO_MAT, cd_subgrupo_material_w)	= cd_subgrupo_material_w
	and	nvl(cd_classe_mat, cd_classe_material_w)		= cd_classe_material_w
	order	by  	nvl(cd_material,0),
			nvl(cd_classe_mat,0),
			nvl(cd_subgrupo_mat,0),
			nvl(CD_GRUPO_MAT,0);

begin

ie_permite_w	:= 'S';

begin
select	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from	estrutura_material_v
where	cd_material	= cd_material_p;
exception
	when others then
		begin
		cd_grupo_material_w:= '';
		cd_subgrupo_material_w:= '';
		cd_classe_material_w:= '';
		end;
end;


open C01;
loop
fetch C01 into	
	ie_permite_w;
exit when C01%notfound;
end loop;
close C01;

if	(ie_permite_w	= 'N') then
	 -- N�o � poss�vel prescrever o Material / Medicamento '||obter_desc_material(cd_material_p)||'. #@#@
	Wheb_mensagem_pck.exibir_mensagem_abort(263532,	'DS_MATERIAL_W='||substr(obter_desc_material(cd_material_p),1,60));
end if;


end Consiste_Regra_Mat_quimio;
/