create or replace
procedure DIRF_REGISTRO_BPFDEC(nr_seq_lote_p	number,
			       cd_dirf_p	varchar2,
			       nm_usuario_p	varchar2,
			       nr_seq_posicao_p	number,
			       nr_seq_apres_p	out number) is 
			       
dt_lote_w		date;
vl_min_ano_pf_w		number(15,2);
cd_pessoa_fisica_w	varchar2(15);
nm_pessoa_fisica_w	varchar2(255);
nr_cpf_w		varchar2(100);
ds_arquivo_w		varchar2(2000);
separador_w		varchar2(1) := '|';
nr_seq_apres_w		number(10);
cd_estabelecimento_w	varchar2(10);
			       

cursor C01 is
	select	distinct 
		p.cd_pessoa_fisica,
		f.nr_cpf,
		f.nm_pessoa_fisica
	from	titulo_pagar p, 
		titulo_pagar_imposto j,
		pessoa_fisica f
	where	p.nr_titulo = j.nr_titulo (+)
	and	p.cd_pessoa_fisica = f.cd_pessoa_fisica
	and	trunc(p.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_pessoa_fisica is not null
	and	decode(j.cd_darf,'588','0588',nvl(j.cd_darf,'0588')) = cd_dirf_p
	and	p.ie_situacao <> 'C'
	and	p.ie_tipo_titulo in (select z.ie_tipo_titulo from dirf_regra_tipo_tit z)
	and	( (p.cd_estabelecimento = nvl(cd_estabelecimento_w,p.cd_estabelecimento))
	or	  ((p.cd_estabelecimento is null) and (cd_estabelecimento_w is null)))
	--and	p.ie_tipo_titulo in ('0','11','13','5')
	and	p.vl_titulo >= nvl(vl_min_ano_pf_w,6000)
	union
	select	distinct 
		a.cd_pessoa_fisica,
		e.nr_cpf,
		e.nm_pessoa_fisica
	from	titulo_pagar a,
		titulo_pagar_imposto i,
		pessoa_fisica e
	where	a.nr_titulo = i.nr_titulo
	and	a.cd_pessoa_fisica = e.cd_pessoa_fisica
	and	trunc(a.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	decode(i.cd_darf,'588','0588',nvl(i.cd_darf,'0588'))  = cd_dirf_p
	and	a.ie_tipo_titulo in (select z.ie_tipo_titulo from dirf_regra_tipo_tit z)
	--and	a.ie_tipo_titulo in ('0','11','13','5')
	and	a.cd_pessoa_fisica is not null
	and	a.ie_situacao <> 'C'
	and	( (a.cd_estabelecimento = nvl(cd_estabelecimento_w,a.cd_estabelecimento))
	or	  ((a.cd_estabelecimento is null) and (cd_estabelecimento_w is null)))
	and	a.vl_titulo < nvl(vl_min_ano_pf_w,6000)
	union
	select	distinct 
		p.cd_pessoa_fisica,
		t.nr_cpf,
		t.nm_pessoa_fisica
	from	dirf_lote a,
		gps d,
		gps_titulo e,
		titulo_pagar p, /* T�tulo  original */
		titulo_pagar f, /* T�tulo gerado do imposto */
		pessoa_fisica t
	where	a.nr_sequencia 	= d.nr_seq_dirf
	and	d.nr_sequencia 	= e.nr_seq_gps 
	and	f.nr_titulo	= e.nr_titulo
	and	p.cd_pessoa_fisica = t.cd_pessoa_fisica
	and	f.nr_titulo_original = p.nr_titulo
	and	trunc(f.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	a.nr_sequencia = nr_seq_lote_p 
	and	lpad(cd_dirf_p,4,'0') = '0588'
	order by nr_cpf;	
			       

begin

nr_seq_apres_w := nr_seq_posicao_p;

select 	nvl(dt_lote,sysdate),
	nvl(vl_min_ano_pf,6000),
	cd_estabelecimento
into	dt_lote_w,
	vl_min_ano_pf_w,
	cd_estabelecimento_w
from	dirf_lote
where	nr_sequencia = nr_seq_lote_p;


OPEN C01;
LOOP
FETCH C01 INTO
	cd_pessoa_fisica_w,
	nr_cpf_w,
	nm_pessoa_fisica_w;
EXIT WHEN C01%NOTFOUND;
	ds_arquivo_w := 'BPFDEC' || separador_w || lpad(nr_cpf_w,11,'0') || separador_w || ELIMINA_CARACTERE_ESPECIAL(substr(nm_pessoa_fisica_w,1,60)) || separador_w || separador_w;
	nr_seq_apres_w := nr_seq_apres_w + 1;
	insert 	into w_dirf_arquivo (nr_sequencia,
				     nm_usuario,
				     nm_usuario_nrec,
				     dt_atualizacao,
				     dt_atualizacao_nrec,
				     ds_arquivo,
				     nr_seq_apresentacao,
				     nr_seq_registro,
				     cd_darf)
	values			    (w_dirf_arquivo_seq.nextval,
				     nm_usuario_p,
				     nm_usuario_p,
				     sysdate,
				     sysdate,
				     ds_arquivo_w,
				     to_char(nr_seq_apres_w),
				     0,
				     cd_dirf_p);
	commit;
	nr_seq_apres_w := nr_seq_apres_w + 1;
	-- inserir dados referente a pessoa fisica. Valores mensais de impostos e rendimentos
	DIRF_REGISTRO_VAL_MENSAIS_PF(cd_pessoa_fisica_w,cd_dirf_p,dt_lote_w,nm_usuario_p,nr_seq_apres_w,nr_seq_apres_w,cd_estabelecimento_w);
	
	if (lpad(cd_dirf_p,4,'0') = '0588') then
		DIRF_REGISTRO_RTPO_PF(nr_seq_lote_p,cd_pessoa_fisica_w,cd_dirf_p,dt_lote_w,nm_usuario_p,nr_seq_apres_w,nr_seq_apres_w);
	end if;


END LOOP;
CLOSE C01;



	

commit;
nr_seq_apres_p := nr_seq_apres_w;

end DIRF_REGISTRO_BPFDEC;
/
