create or replace 
procedure lib_mat_pac_lib_susp(	nr_sequencia_p number,
					nm_usuario_p		varchar2) is

nr_prescricao_w		number(15);
nr_seq_material_w	number(10);
nr_sequencia_w		number(10);

cursor	c01 is
select	nr_sequencia,
	nr_prescricao,
	nr_seq_material
from	lib_material_paciente
where 	nr_sequencia = nr_sequencia_p;

begin
if	(nr_sequencia_p is not null) then
	begin

	open C01;
	loop
	fetch C01 into
		nr_sequencia_w,
		nr_prescricao_w,
		nr_seq_material_w;
	exit when C01%notfound;
		begin

		update	lib_material_paciente
		set	dt_suspenso		= sysdate,
			dt_atualizacao		= sysdate,
			nm_usuario      	= nm_usuario_p,
			nm_usuario_susp 	= nm_usuario_p
		where 	nr_sequencia	   	= nr_sequencia_w;

		update	prescr_material
		set	nr_seq_lib_mat_pac	= null
		where	nr_prescricao		= nr_prescricao_w
		and	nr_sequencia		= nr_seq_material_w;

		end;
	end loop;
	close C01;

	end;
end if;

commit;

end lib_mat_pac_lib_susp;
/