create or replace  
procedure Criar_Med_Res_Exame_Rel
		(nr_seq_cliente_p	number,
		nm_usuario_p	varchar2) is /* Paulo henrique feraz */


cd_medico_w				varchar2(10);
dt_exame_w				date;
ds_atributos_w				varchar2(2000) := '';
ds_comando_w				varchar2(1000);
nr_seq_exame_w				number(10,0);
vl_exame_w				varchar2(100);
ds_valor_exame_w			varchar2(255);
nm_atributo_w				varchar2(30);
qt_decimais_w				number(05,0);
ds_sep_bv_w				varchar2(50);
ie_valido_w				varchar2(1);
nr_prescricao_w				number(10);
nr_seq_prescricao_w			number(10);
qt_data_exames_w			number(10);
ds_valor_rel_w				varchar2(800);
ds_valor_rel_ww				varchar2(900);
ds_valor_columns_w			varchar2(800);
ds_valor_columns_ww			varchar2(900);
ds_comando_rel_w			varchar2(1000);
qt_valor_w			number(10);

/*


COMENTEI ESTE OBJETO OSs 405924 e 405891


cursor	c01 is
	select	trunc(dt_exame)
	from	med_result_exame
	where	nr_seq_cliente	= nr_seq_cliente_p
	group 	by trunc(dt_exame)
	order 	by 1 desc;

cursor	c02 is
	select	a.nr_seq_exame,
		substr(campo_mascara(a.vl_exame, nvl(b.qt_decimais,0)),1,50),
		substr(a.ds_valor_exame,1,50),
		'DIA_' || to_char(a.dt_exame, 'ddmmyyyy'),
		nvl(b.qt_decimais,0),
		ie_valido
	from	med_result_exame a, med_exame_padrao b
	where	a.nr_seq_exame		= b.nr_sequencia
	and	a.nr_seq_cliente 	= nr_seq_cliente_p; 
	
cursor	c03 is
	SELECT ds_valor
	FROM(SELECT SUBSTR(column_name,1,150) ds_valor,
	   	 column_id
		 FROM user_tab_columns   
		 WHERE UPPER(table_name) = UPPER('w_med_res_exame_rel')
		 AND SUBSTR(column_name,1,8) = 'VL_RESUL'
   	ORDER BY LENGTH(column_name), column_name)
	WHERE	ROWNUM <= qt_data_exames_w;
	
cursor  c04 is	
	SELECT column_name FROM user_tab_columns   
	WHERE UPPER(table_name) = UPPER('w_med_res_ex_' || REPLACE(nm_usuario_p,' ',''))
	AND SUBSTR(column_name,1,4) = 'DIA_'
	AND ROWNUM <= 15; */
	
begin

qt_valor_w := 1;

/*

select  count(*)
into 	qt_valor_w
from(
SELECT	nm_exame
FROM 	w_med_res_exame_rel);

SELECT COUNT(*)
into   qt_data_exames_w
FROM(
SELECT column_name FROM user_tab_columns   
WHERE UPPER(table_name) = UPPER('w_med_res_ex_' || REPLACE(nm_usuario_p,' ',''))
AND SUBSTR(column_name,1,4) = 'DIA_'
AND ROWNUM <= 18);

ds_sep_bv_w := obter_separador_bv;

select	cd_medico
into	cd_medico_w
from	med_cliente
where	nr_sequencia	= nr_seq_cliente_p;


exec_sql_dinamico('TASY', ' drop table w_med_res_ex_' || replace(nm_usuario_p,' ',''));

open	c01;
loop
fetch	c01 into dt_exame_w;
exit	when c01%notfound;
	ds_atributos_w	:= ds_atributos_w || 'DIA_' || to_char(dt_exame_w,'ddmmyyyy') ||' VARCHAR2(50),';
end loop;
close c01;

exec_sql_dinamico('TASY', ' create table w_med_res_ex_' || replace(nm_usuario_p,' ','') ||
			  ' (NR_SEQ_EXAME		NUMBER(10,0), 	' ||
			  '  EXAME			VARCHAR2(240), 	' ||
			  '  UNIDADE			VARCHAR2(240),  ' ||
			  '  IE_FORMATO_RESULTADO	VARCHAR2(01),	' ||
			  '  NR_SEQ_APRESENT		NUMBER(10,0), 	' ||
			  ds_atributos_w ||
			  '  DS_TIPO_RESULTADO		VARCHAR2(4000), ' ||
			  '  IE_VALIDO			VARCHAR2(1))	');
			  
exec_sql_dinamico('TASY', ' create index WMEDRES_FK on w_med_res_ex_' || replace(nm_usuario_p,' ','') || ' (NR_SEQ_EXAME) ');

exec_sql_dinamico('TASY', ' delete from w_med_res_ex_' || replace(nm_usuario_p,' ',''));


ds_comando_w  := ' insert into w_med_res_ex_' || replace(nm_usuario_p,' ','') ||
		'	(nr_seq_exame, exame, unidade, ie_formato_resultado, nr_seq_apresent, ds_tipo_resultado) ' ||
		' 	(select a.nr_seq_exame, b.ds_exame, b.ds_unidade, b.ie_formato_resultado, b.nr_seq_apresent, ' ||
		' 	substr(obter_tipo_result_exame(a.nr_seq_exame, a.nr_seq_cliente),1,4000) ' ||
        	' 	from med_exame_padrao b, med_result_exame a ' ||
        	' 	where a.nr_seq_exame   = b.nr_sequencia and a.nr_seq_aval is null ' ||
        	'   	and a.nr_seq_cliente = :nr_seq_cliente ' || 
        	' 	group by b.ds_exame, b.ds_unidade, a.nr_seq_exame, b.ie_formato_resultado, b.nr_seq_apresent, a.nr_seq_cliente) ';

exec_sql_dinamico_bv('TASY', ds_comando_w, 'nr_seq_cliente='||to_char(nr_seq_cliente_p));


open	c02;
loop
fetch	c02 into nr_seq_exame_w,
		 vl_exame_w,
		 ds_valor_exame_w,
		 nm_atributo_w,
		 qt_decimais_w,
		 ie_valido_w;
exit	when c02%notfound;
	begin


	if	(vl_exame_w is not null) then
		begin
		ds_comando_w	:=	' update w_med_res_ex_' || replace(nm_usuario_p,' ','') ||
				   	' set '||nm_atributo_w ||' = :vl_exame, ie_valido = :ie_valido ' || 
					' where nr_seq_exame	= :nr_seq_exame'; 
					
		exec_sql_dinamico_bv('TASY', ds_comando_w,	'vl_exame='||replace(vl_exame_w,'.',',') ||ds_sep_bv_w || 
								'nr_seq_exame='||to_char(nr_seq_exame_w) ||ds_sep_bv_w ||
								'ie_valido=' || ie_valido_w);

		end;
	end if;

	if	(ds_valor_exame_w is not null) then
		begin
		ds_comando_w	:=	' update w_med_res_ex_' || replace(nm_usuario_p,' ','') ||
				   	' set ' || nm_atributo_w || ' = :ds_valor_exame, ie_valido = :ie_valido' ||
					' where nr_seq_exame	= :nr_seq_exame'; 

		exec_sql_dinamico_bv('TASY', ds_comando_w, 	'ds_valor_exame='|| ds_valor_exame_w || ds_sep_bv_w || 
								'nr_seq_exame=' ||to_char(nr_seq_exame_w) || ds_sep_bv_w || 
								'ie_valido=' || ie_valido_w);
		end;
	end if;
	end;
end loop;
close c02;

open c03;
loop
fetch c03 into
	ds_valor_rel_w;
exit when c03%notfound;		
	if	(ds_valor_rel_ww is not null) then	
		ds_valor_rel_ww := substr(ds_valor_rel_ww || ', ',1,4000);
	end if;
	ds_valor_rel_ww	:= substr(ds_valor_rel_ww || ds_valor_rel_w,1,4000);
end loop;
close C03;

open c04;
loop
fetch c04 into
	ds_valor_columns_w;
exit when c04%notfound;
	if	(ds_valor_columns_ww is not null) then	
		 ds_valor_columns_ww := substr(ds_valor_columns_ww || ', ',1,4000);
	end if;
	ds_valor_columns_ww	:= substr(ds_valor_columns_ww || ds_valor_columns_w,1,4000);
end loop;
close c04;

	if	(qt_valor_w >= 1) then
		DELETE FROM w_med_res_exame_rel where nm_usuario = nm_usuario_p;
		COMMIT;
	end if;
	

ds_comando_rel_w  :=    'insert into w_med_res_exame_rel' ||
			'	( nm_exame, ds_unidade, nm_usuario, nr_seq_apresent, ' || ds_valor_rel_ww || ') ' ||  
			'	( select exame, unidade, '|| '''' || nm_usuario_p || '''' || ', nr_seq_apresent, ' || ds_valor_columns_ww ||
			'	  from   w_med_res_ex_' || replace(nm_usuario_p,' ','') || ')';
       
exec_sql_dinamico_bv('TASY', ds_comando_rel_w, '');	

*/

end Criar_Med_Res_Exame_Rel;
/