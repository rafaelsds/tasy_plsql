create or replace
procedure PLT_deletar_itens_sem_horario(	nr_prescricao_p		number,
						cd_perfil_p		number,
						cd_estabelecimento_p	number,
						nm_usuario_p		Varchar2) is 

ie_horarios_dietas_orais_w		varchar2(15);						
nr_sequencia_w				number(15);
ie_prescr_vazia_w			varchar2(1);
ds_item_w				varchar2(255);
cd_intervalo_w				varchar2(255);
						
cursor c01 is
select	a.nr_sequencia,
	b.ds_material,
	a.cd_intervalo
from	prescr_material a,
	material b
where	a.nr_prescricao = nr_prescricao_p
and	a.cd_material = b.cd_material
and	a.dt_suspensao_progr is not null
and	nvl(a.ie_se_necessario,'N') 	<> 'S'
and	nvl(a.ie_acm,'N') 		<> 'S'
and	not exists (	select	1
			from	prescr_mat_hor b
			where	a.nr_prescricao 	= b.nr_prescricao
			and	b.nr_seq_material 	= a.nr_sequencia);
			
cursor c02 is
select	a.nr_sequencia,
	substr(obter_desc_proced_prescr(a.nr_prescricao,a.nr_sequencia),1,255),
	a.cd_intervalo
from	prescr_procedimento a
where	a.nr_prescricao = nr_prescricao_p
and	a.dt_suspensao_progr is not null
and	not exists (	select	1
			from	prescr_proc_hor b
			where	a.nr_prescricao 	= b.nr_prescricao
			and	b.nr_seq_procedimento 	= a.nr_sequencia);
			
cursor c03 is
select	a.nr_sequencia,
	b.nm_dieta,
	a.cd_intervalo
from	prescr_dieta a,
	dieta b
where	a.nr_prescricao = nr_prescricao_p
and	a.cd_dieta = b.cd_dieta
and	a.dt_suspensao_progr is not null
and	not exists (	select	1
			from	prescr_dieta_hor b
			where	a.nr_prescricao 	= b.nr_prescricao);
			
cursor c04 is
select	a.nr_sequencia,
	substr(nvl(b.ds_tipo_recomendacao,a.ds_recomendacao),1,255)	,
	a.cd_intervalo
from	prescr_recomendacao a,
	tipo_recomendacao b
where	a.nr_prescricao = nr_prescricao_p
and	b.cd_tipo_recomendacao(+) = a.cd_recomendacao
and	a.dt_suspensao_progr is not null
and	not exists (	select	1
			from	prescr_rec_hor b
			where	a.nr_prescricao 	= b.nr_prescricao
			and	b.nr_seq_recomendacao 	= a.nr_sequencia);
						
begin

Obter_Param_Usuario(1113, 148, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_horarios_dietas_orais_w);

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	ds_item_w,
	cd_intervalo_w;
exit when C01%notfound;
	begin
	
	delete	prescr_material
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_sequencia_w;

	plt_avisar_nao_copiado(ds_item_w, 'IS','GM',nr_prescricao_p,0,nm_usuario_p,null,null,'N', cd_intervalo_w,cd_perfil_p,cd_estabelecimento_p);	
	
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_sequencia_w,
	ds_item_w,
	cd_intervalo_w;
exit when C02%notfound;
	begin

	delete	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_sequencia_w;	
	
	plt_avisar_nao_copiado(ds_item_w, 'IS','GP',nr_prescricao_p,0,nm_usuario_p,null,null,'N', cd_intervalo_w,cd_perfil_p,cd_estabelecimento_p);
	
	end;
end loop;
close C02;

if	(ie_horarios_dietas_orais_w = 'S') then

	open C03;
	loop
	fetch C03 into	
		nr_sequencia_w,
		ds_item_w,
		cd_intervalo_w;
	exit when C03%notfound;
		begin
		
		delete	prescr_dieta
		where	nr_prescricao	= nr_prescricao_p;
		
		plt_avisar_nao_copiado(ds_item_w, 'IS','D',nr_prescricao_p,0,nm_usuario_p,null,null,'N', cd_intervalo_w,cd_perfil_p,cd_estabelecimento_p);
		
		end;
	end loop;
	close C03;

end if;	
	
open C04;
loop
fetch C04 into	
	nr_sequencia_w,
	ds_item_w,
	cd_intervalo_w;
exit when C04%notfound;
	begin

	delete	prescr_recomendacao
	where	nr_prescricao	= nr_prescricao_p
	and	nr_sequencia	= nr_sequencia_w;
	
	plt_avisar_nao_copiado(ds_item_w, 'IS','R',nr_prescricao_p,0,nm_usuario_p,null,null,'N',cd_intervalo_w,cd_perfil_p,cd_estabelecimento_p);
	
	end;
end loop;
close C04;

if	(obter_se_prescr_vazia(nr_prescricao_p) = 'S') then
	excluir_prescr_copia(nr_prescricao_p);
end if;

commit;

end PLT_deletar_itens_sem_horario;
/
