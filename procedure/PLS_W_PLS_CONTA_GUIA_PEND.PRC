create or replace
procedure pls_w_pls_conta_guia_pend
                        (       ie_usa_data_p                   Varchar2,
                                dt_inicio_p                     date,
                                dt_fim_p                        date,
                                ie_tipo_guia_p                  Varchar2,
                                nr_seq_prestador_p              Number,
                                ie_opcao_p                      Varchar2,
                                nm_usuario_p                    Varchar2,
                                cd_estabelecimento_p            Varchar2,
                                ie_alteracao_p                  Varchar2,
                                nr_sequencia_p                  NUmber,
                                cd_procedimento_p               Number,
                                cd_guias_p                      Varchar2,
                                cd_medico_solic_p               Varchar2,
				cd_produto_p			Varchar2,
                                nr_seq_clas_prestador_p         Number,
				ie_processo_p			Varchar2) is



ie_inserir_w                    Varchar2(4);
nr_sequencia_guia_w             Number(10);
cd_guia_w                       Varchar2(20);
nr_seq_segurado_w               Number(10);
cd_medico_solicitante_w         Varchar2(10);
ie_tipo_guia_w                  Varchar2(3);
ie_carater_internacao_w         Varchar2(3);
ie_tipo_atend_tiss_w            Varchar2(3);
ie_estagio_w                    Varchar2(4);
nr_sequencia_w                  Number(10);
nr_seq_prestador_w              Number(10);
nr_seq_plano_w                  Number(10);
nr_seq_prestador_ww             Number(10):= 1;
ie_tipo_repasse_w               Varchar2(1);
ie_tipo_segurado_w              Varchar2(1);
ie_preco_w                      Varchar2(2);

Cursor C01 is
        select  decode(ie_opcao_p,'N','N','S') ie_inserir,
                nr_sequencia,
                cd_guia,
                nr_seq_segurado,
                nr_seq_prestador,
                cd_medico_solicitante,
                ie_tipo_guia,
                ie_carater_internacao,
                ie_tipo_atend_tiss,
                ie_estagio,
                nr_seq_plano
        from	pls_guia_plano 	a 
        where 	a.ie_status     = '1'
        and     a.cd_estabelecimento        = cd_estabelecimento_p
        and     pls_conta_autor_pck.pls_obter_ie_utilizado_guia(nr_sequencia, null,null, cd_estabelecimento_p)	in ('P','N')
	and	((a.IE_TIPO_PROCESSO = ie_processo_p) or (ie_processo_p is null))
        and     (a.dt_solicitacao between dt_inicio_p and fim_dia(dt_fim_p) 
		or	(ie_usa_data_p = 'N'))
        and     ie_tipo_guia = ie_tipo_guia_w 
        and     ((a.nr_seq_prestador = nr_seq_prestador_p) or (nr_seq_prestador_ww = 0))
        and     ((a.nr_seq_prestador in ( select x.nr_sequencia
                                         from   pls_prestador  x
                                         where  x.nr_seq_classificacao =  nr_seq_clas_prestador_p)) or
                (nr_seq_clas_prestador_p is null))
        and     ((pls_obter_se_entre_guias(a.cd_guia,cd_guias_p ) = 'S' ) or
                (cd_guias_p is null))
        and     ((exists        (       select  1
                                        from    pls_guia_plano_proc     x
                                        where   x.nr_seq_guia           = a.nr_sequencia
                                        and     x.cd_procedimento       = cd_procedimento_p
                                        and     pls_conta_autor_pck.pls_obter_ie_utilizado_guia(x.nr_seq_guia, x.nr_sequencia,null, cd_estabelecimento_p) in ('P','N')
                                        and     nr_seq_motivo_exc       is null)) or
                (cd_procedimento_p is null))
        and     ((a.cd_medico_solicitante = cd_medico_solic_p ) or
                (cd_medico_solic_p is null))
	and	not exists( select 1 from pls_conta x where nr_seq_guia = a.nr_sequencia and x.ie_status != 'C');


begin
select  decode(ie_tipo_guia_p,'4','2','5','1','3','3','8')
into    ie_tipo_guia_w
from    dual;

if (ie_alteracao_p      = 'A') then

        pls_delete_w_guias_gera_conta(nm_usuario_p);

        if (nvl(nr_seq_prestador_p,0) = 0 ) then
           nr_seq_prestador_ww := 0;
        end if;
        open C01;
        loop
        fetch C01 into
                ie_inserir_w,
                nr_sequencia_guia_w,
                cd_guia_w,
                nr_seq_segurado_w,
                nr_seq_prestador_w,
                cd_medico_solicitante_w,
                ie_tipo_guia_w,
                ie_carater_internacao_w,
                ie_tipo_atend_tiss_w,
                ie_estagio_w,
                nr_seq_plano_w;
        exit when C01%notfound;
                begin
		if	(nvl(cd_produto_p,0) > 0) then
			if	(nvl(nr_seq_segurado_w,0) > 0) then
				select 	nr_seq_plano
				into	nr_seq_plano_w
				from	pls_segurado
				where 	nr_sequencia = nr_seq_segurado_w;
				
				if	( nr_seq_plano_w <> cd_produto_p ) then
					goto final;
				end if;
			else
				goto final;
			end if;
		end if;
		
                select  nvl(max(nr_sequencia),0) + 1
                into    nr_sequencia_w
                from    w_pls_guias_gera_conta
                where   nm_usuario_logado       = nm_usuario_p;

                insert into w_pls_guias_gera_conta(     nr_sequencia, dt_atualizacao, nm_usuario,
                                                        dt_atualizacao_nrec, nm_usuario_nrec,cd_guia,
                                                        cd_medico_solicitante, dt_solicitacao,ie_carater_internacao,
                                                        ie_estagio,ie_tipo_atend_tiss, ie_tipo_guia,
                                                        nm_usuario_logado,nr_seq_guia_plano,nr_seq_prestador,
                                                        nr_seq_segurado,ie_inserir)
                                                values( nr_sequencia_w, sysdate, nm_usuario_p,
                                                        sysdate, nm_usuario_p,cd_guia_w,
                                                        cd_medico_solicitante_w,null,ie_carater_internacao_w,
                                                        ie_estagio_w,ie_tipo_atend_tiss_w,ie_tipo_guia_w,
                                                        nm_usuario_p,nr_sequencia_guia_w,nr_seq_prestador_w,
                                                        nr_seq_segurado_w,ie_inserir_w);
                <<final>>
		null;
                end;
        end loop;
        close C01;
elsif (ie_alteracao_p = 'U') then
        update  w_pls_guias_gera_conta
        set     ie_inserir  = ie_opcao_p
        where   nm_usuario_logado = nm_usuario_p
        and     nr_sequencia = nr_sequencia_p;
end if;

commit;

end pls_w_pls_conta_guia_pend;
/
