create or replace
procedure gerar_hist_agenda_pac_sangue(	nr_seq_sangue_p			number,
					ie_acao_p			varchar,
					ds_acao_p			varchar,
					nm_usuario_p			varchar2) is
nr_sequencia_w		number(10);

begin

select	agenda_pac_sangue_hist_seq.nextval
into	nr_sequencia_w
from	dual;

insert	into agenda_pac_sangue_hist (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ie_acao,
					ds_acao,
					nr_seq_sangue)
				values (
					nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					ie_acao_p,
					ds_acao_p,
					nr_seq_sangue_p);
end;
/
