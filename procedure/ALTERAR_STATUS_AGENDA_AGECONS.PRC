create or replace
procedure  alterar_status_agenda_agecons(
		nr_sequencia_p		number,
		nr_seq_status_p		number,
		nm_usuario_p		varchar2) is 

ie_status_agenda_w		varchar2(3);
ie_status_agenda_old_w		varchar2(3);

begin
if	(nm_usuario_p is not null) and
	(nvl(nr_sequencia_p, 0) > 0) then
	
	select max(ie_status_agenda) ie_status_agenda_old 
	into	ie_status_agenda_old_w
	from	agenda_consulta 
	where 	nr_sequencia =  nr_sequencia_p;
	
	select	max(ie_status_agenda) ie_status_agenda
	into	ie_status_agenda_w
	from	agenda_cons_status_adic
	where	ie_situacao = 'A'
	and	nr_sequencia = nr_seq_status_p;
	
	if(ie_status_agenda_old_w = 'S') and (ie_status_agenda_w = 'S') then
		ie_status_agenda_w := obter_penultimo_status(nr_sequencia_p);		
	end if;
	
	if	(ie_status_agenda_w is not null) then
		update	agenda_consulta
		set	ie_status_agenda  = ie_status_agenda_w,
			dt_atualizacao    = sysdate,
			nm_usuario        = nm_usuario_p
		where	nr_sequencia      = nr_sequencia_p;
	end if;
end if;

commit;
end alterar_status_agenda_agecons;
/