create or replace
procedure rp_desfaz_fim_tratamento(nr_seq_tratamento_p	number,
				   cd_estabelecimento_p	number) is 
				   
nr_seq_pac_reab_final_w		number(10,0);	
nr_seq_pac_reab_w		number(10,0);			   
ie_altera_status_final_w	varchar2(1);
nr_seq_max_atual_w		number(10);
nr_seq_status_trat_w		number(10);	
begin
Obter_param_Usuario(9091, 37,  obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_altera_status_final_w);


select	max(nr_seq_pac_reav)
into	nr_seq_pac_reab_w
from	rp_tratamento
where	nr_sequencia = nr_seq_tratamento_p;

select	max(nr_seq_reab_pac_trat)
into	nr_seq_status_trat_w
from	rp_parametros
where	cd_estabelecimento = cd_estabelecimento_p;


update 	rp_tratamento
set	dt_fim_tratamento = Null,
	nr_seq_motivo_fim_trat = Null,
	dt_desfeita = sysdate,
	nr_seq_status = decode(ie_altera_status_final_w, 'S', nr_seq_status_trat_w, nr_seq_status)
where	nr_sequencia = nr_seq_tratamento_p;


if	(ie_altera_status_final_w = 'S') then
	
	update 	rp_paciente_reabilitacao
	set	nr_seq_status	= nr_seq_status_trat_w
	where	nr_sequencia 	= nr_seq_pac_reab_w;
end if;

commit;

end rp_desfaz_fim_tratamento;
/
