create or replace
procedure obter_cor_cih_motivo_contato(
			nm_usuario_p		varchar2,
			nr_seq_cor_lista_p	out 	varchar2,
			ds_cor_lista_p	out 	varchar2) is 

ds_sql_w	varchar2(255);

begin

ds_sql_w		:= 'select nr_sequencia from cih_motivo_contato';
nr_seq_cor_lista_p	:= obter_select_concatenado_bv(ds_sql_w, '', ',');

ds_sql_w		:= 'select ds_cor from cih_motivo_contato order by nr_sequencia';
ds_cor_lista_p	:= obter_select_concatenado_bv(ds_sql_w, '', ',');

end obter_cor_cih_motivo_contato;
/
