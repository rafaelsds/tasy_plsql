create or replace
procedure gerar_item_processo_adep	(nr_seq_processo_p	number,
				nr_seq_area_prep_p	number,
				cd_material_original_p	number,
				cd_material_p		number,
				qt_material_p		number,
				cd_unid_med_p		varchar2,
				nr_seq_lf_p		number,
				ie_lanca_conta_p		varchar2,
				nm_usuario_p		varchar2) is

nr_sequencia_w		number(10,0);
ie_lanca_conta_w	varchar2(1);
ie_baixa_estoque_w	varchar2(1);
nr_seq_regra_w		number(10,0);
ie_regra_conta_w	varchar2(1);
ie_regra_estoque_w	varchar2(1);
cd_estabelecimento_w	number(4,0);
cd_setor_atendimento_w	number(5,0);
cd_local_estoque_w	number(4,0);
cd_grupo_material_w	number(3,0);
cd_subgrupo_material_w	number(3,0);
cd_classe_material_w	number(5,0);

cursor C01 is
select	ie_lanca_conta,
	ie_baixa_estoque,
	nr_sequencia
from	regra_disp_gedipa
where	cd_estabelecimento					= cd_estabelecimento_w
and	nvl(cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
and	nvl(cd_local_estoque,cd_local_estoque_w)		= cd_local_estoque_w
and	((nvl(ie_padronizado,'A') = 'A') or
	 ((nvl(ie_padronizado,'A') = 'S') and (obter_se_material_padronizado(cd_estabelecimento_w,cd_material_p)) = 'S') or
	 ((nvl(ie_padronizado,'A') = 'N') and (obter_se_material_padronizado(cd_estabelecimento_w,cd_material_p)) = 'N'))
and	sysdate between dt_inicio_validade and nvl(dt_fim_validade,sysdate)
order by 
	nvl(cd_setor_atendimento,0),
	nvl(cd_local_estoque,0),
	dt_inicio_validade;
	
begin
if	(nr_seq_processo_p is not null) and
	(cd_material_p is not null) and
	(nm_usuario_p is not null) then
	
	select	obter_estab_atend(nr_atendimento),
		cd_setor_atendimento,
		cd_local_estoque
	into	cd_estabelecimento_w,
		cd_setor_atendimento_w,
		cd_local_estoque_w
	from	adep_processo
	where	nr_sequencia = nr_seq_processo_p;

	open c01;
	loop
	fetch c01 into	ie_lanca_conta_w,
			ie_baixa_estoque_w,
			nr_seq_regra_w;
	exit when c01%notfound;
		begin		
		ie_lanca_conta_w	:= ie_lanca_conta_w;
		ie_baixa_estoque_w	:= ie_baixa_estoque_w;
		nr_seq_regra_w		:= nr_seq_regra_w;		
		end;
	end loop;
	close c01;

	select	adep_processo_item_seq.nextval
	into	nr_sequencia_w
	from	dual;	

	insert into adep_processo_item (
		nr_sequencia,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,
		nr_seq_processo,
		cd_material,
		qt_material,
		cd_unidade_medida,
		nr_seq_lote_fornec,
		qt_conta,
		cd_unidade_medida_conta,
		qt_estoque,
		cd_unidade_medida_estoque,
		ie_lanca_conta,
		ie_baixa_estoque,
		cd_material_original,
		ie_conta_paciente,
		nr_seq_area_prep)
	values (
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_processo_p,
		cd_material_p,
		qt_material_p,
		cd_unid_med_p,
		decode(nr_seq_lf_p, 0, null, nr_seq_lf_p),
		qt_material_p,
		cd_unid_med_p,
		qt_material_p,
		cd_unid_med_p,
		decode(ie_lanca_conta_p, 'N', ie_lanca_conta_p, ie_lanca_conta_w),
		ie_baixa_estoque_w,
		cd_material_original_p,
		'N',
		nr_seq_area_prep_p);

end if;

commit;

end gerar_item_processo_adep;
/