create or replace
procedure alterar_portador_cobranca(
			cd_portador_p		number,
			tipo_portador_p		number,
			nr_sequencia_p		number) is 

begin

update	cobranca 
set	cd_tipo_portador	= tipo_portador_p,
	cd_portador		= cd_portador_p
where	nr_sequencia	= nr_sequencia_p; 

commit;

end alterar_portador_cobranca;
/