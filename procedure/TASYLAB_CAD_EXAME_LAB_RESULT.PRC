create or replace
procedure tasylab_cad_exame_lab_result (	nr_origem_p						number,
											/*nm_pessoa_fisica_p				varchar2,
											dt_nascimento_p					date,
											ie_sexo_p						varchar2,
											nr_ddd_celular_p				varchar2,
											nr_telefone_celular_p			varchar2,
											nr_cpf_p						varchar2,
											nr_identidade_p					varchar2,
											ie_tipo_pessoa_p				varchar2,						
											cd_cep_res_p					varchar2,
											ds_endereco_res_p				varchar2,
											nr_endereco_res_p				number,
											ds_bairro_res_p					varchar2,
											ds_complemento_res_p			varchar2,
											ds_municipio_res_p				varchar2,
											sg_estado_res_p					varchar2,
											ds_email_res_p					varchar2,						
											nm_contato_mae_p				varchar2,*/
											dt_atualizacao_p				date,
											dt_liberacao_p					date,
											dt_resultado_p					date,
											nm_usuario_p					varchar2,
											nr_prescricao_p					number,
											nr_seq_pedido_exam_ext_item_p	number,
											nr_seq_pedido_externo_p			number,
											nr_seq_protocolo_p				number,
											nr_seq_reg_elemento_p			number,
											cd_erro_p						out number,
											ds_erro_p						out varchar2,
											nr_seq_resultado_p				out number) is 

cd_medico_w			lab_tasylab_cliente.cd_medico%type;

nr_atendimento_w	prescr_medica.nr_atendimento%type;

nr_seq_resultado_w	exame_lab_resultado.nr_seq_resultado%type;
						
begin

cd_erro_p	:= 0;

tasy_atualizar_dados_sessao(nr_origem_p);

if	(dt_resultado_p is null) then
	cd_erro_p	:= 23;
elsif (nr_origem_p is null) then
	cd_erro_p	:= 6;
else

	select	max(a.cd_medico)
	into	cd_medico_w			
	from	lab_tasylab_cliente a
	where	a.nr_seq_externo = nr_origem_p;	
	
	select	max(a.nr_atendimento)
	into	nr_atendimento_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p;
	
	select	max(a.nr_seq_resultado)
	into	nr_seq_resultado_w
	from	exame_lab_resultado a
	where	a.nr_prescricao = nr_prescricao_p;
	
	if (nr_seq_resultado_w is null) then
		
		select	nvl(max(nr_seq_resultado),0) + 1
		into	nr_seq_resultado_w
		from	exame_lab_resultado;
		
		begin
		insert into exame_lab_resultado	(
										nr_seq_resultado, 
										dt_resultado, 
										dt_atualizacao, 
										nm_usuario, 
										nr_prescricao, 
										nr_atendimento, 
										cd_medico, 
										nr_seq_protocolo, 
										dt_liberacao, 
										cd_pessoa_fisica, 
										nr_seq_pedido_exam_ext_item, 
										nr_seq_reg_elemento, 
										nr_seq_pedido_externo)
								values	(
										nr_seq_resultado_w,
										dt_resultado_p,
										dt_atualizacao_p,
										nm_usuario_p,
										nr_prescricao_p,
										nr_atendimento_w,
										cd_medico_w,
										nr_seq_protocolo_p,
										dt_liberacao_p,
										null,--cd_pessoa_fisica_w,
										nr_seq_pedido_exam_ext_item_p,
										nr_seq_reg_elemento_p,
										nr_seq_pedido_externo_p);		
		
		--OS727249 - Ivan
		--commit;
		exception
		when others then
			cd_erro_p	:= 1;
			ds_erro_p	:= substr('Erro ao inserir o exame do resultado '||sqlerrm,1,2000);
		end;
	end if;
		
	
	nr_seq_resultado_p	:= nr_seq_resultado_w;
	

end if;


end tasylab_cad_exame_lab_result;
/