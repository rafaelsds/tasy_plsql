create or replace
procedure dirf_grava_RIIRP(	nr_seq_lote_dirf_p			number,
				nm_usuario_p		varchar2,
				nr_seq_apres_p		in out	number,
				cd_darf_p		varchar2,
				cd_estabelecimento_p 	number,
				cd_pessoa_fisica_p		varchar2) is 

vl_rendimento_jan_w		varchar2(20);
vl_rendimento_fev_w		varchar2(20);
vl_rendimento_mar_w		varchar2(20);
vl_rendimento_abr_w		varchar2(20);
vl_rendimento_mai_w		varchar2(20);
vl_rendimento_jun_w		varchar2(20);
vl_rendimento_jul_w		varchar2(20);
vl_rendimento_ago_w		varchar2(20);
vl_rendimento_set_w		varchar2(20);
vl_rendimento_out_w		varchar2(20);
vl_rendimento_nov_w		varchar2(20);
vl_rendimento_dez_w		varchar2(20);
ds_arquivo_w			varchar2(2000);
separador_w			varchar2(1);

function campo_dirf(vl_formatar_p		number)
 		    	return varchar2 is
begin
return	substr(trim(replace(to_char(vl_formatar_p,'9999999999999.99'),'.','')),1,20);
end campo_dirf;

begin
separador_w		:= '|';
vl_rendimento_jan_w	:= 0;
vl_rendimento_fev_w	:= 0;
vl_rendimento_mar_w	:= 0;
vl_rendimento_abr_w	:= 0;
vl_rendimento_mai_w	:= 0;
vl_rendimento_jun_w	:= 0;
vl_rendimento_jul_w	:= 0;
vl_rendimento_ago_w	:= 0;
vl_rendimento_set_w	:= 0;
vl_rendimento_out_w	:= 0;
vl_rendimento_nov_w	:= 0;
vl_rendimento_dez_w	:= 0;

select	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '01', a.vl_rendimento, 0)),0) mes1, 
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '02', a.vl_rendimento, 0)),0) mes2,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '03', a.vl_rendimento, 0)),0) mes3,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '04', a.vl_rendimento, 0)),0) mes4,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '05', a.vl_rendimento, 0)),0) mes5,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '06', a.vl_rendimento, 0)),0) mes6,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '07', a.vl_rendimento, 0)),0) mes7,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '08', a.vl_rendimento, 0)),0) mes8,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '09', a.vl_rendimento, 0)),0) mes9,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '10', a.vl_rendimento, 0)),0) mes10,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '11', a.vl_rendimento, 0)),0) mes11,
	nvl(sum(decode(nvl(to_char(a.dt_base_titulo, 'mm'),'00'), '12', a.vl_rendimento, 0)),0) mes12
into	vl_rendimento_jan_w,
	vl_rendimento_fev_w,
	vl_rendimento_mar_w,
	vl_rendimento_abr_w,
	vl_rendimento_mai_w,
	vl_rendimento_jun_w,
	vl_rendimento_jul_w,
	vl_rendimento_ago_w,
	vl_rendimento_set_w,
	vl_rendimento_out_w,
	vl_rendimento_nov_w,
	vl_rendimento_dez_w
from	dirf_lote_mensal	e,
	dirf_agrupar_lote	d,
	dirf_lote		c,
	titulo_pagar		b,
	dirf_titulo_pagar	a
where	1 = 1
and	e.nr_sequencia		= a.nr_seq_lote_dirf 
and	e.nr_sequencia		= d.nr_seq_lote_mensal
and	c.nr_sequencia		= d.nr_seq_lote_anual
and	a.nr_titulo		= b.nr_titulo
and	c.nr_sequencia		= nr_seq_lote_dirf_p
and	a.ie_registro		= 'RIIRP'
and	nvl(a.cd_pessoa_fisica, b.cd_pessoa_fisica)	= cd_pessoa_fisica_p
and	a.cd_darf = cd_darf_p;

if	((abs(vl_rendimento_jan_w) +
	abs(vl_rendimento_fev_w) +
	abs(vl_rendimento_mar_w) +
	abs(vl_rendimento_abr_w) +
	abs(vl_rendimento_mai_w) +
	abs(vl_rendimento_jun_w) +
	abs(vl_rendimento_jul_w) +
	abs(vl_rendimento_ago_w) +
	abs(vl_rendimento_set_w) +
	abs(vl_rendimento_out_w) +
	abs(vl_rendimento_nov_w) +
	abs(vl_rendimento_dez_w)) > 1) then
	begin

	ds_arquivo_w := 'RIIRP'	|| separador_w || campo_dirf(vl_rendimento_jan_w)	|| separador_w || campo_dirf(vl_rendimento_fev_w) || separador_w || campo_dirf(vl_rendimento_mar_w)
				|| separador_w || campo_dirf(vl_rendimento_abr_w)	|| separador_w || campo_dirf(vl_rendimento_mai_w) || separador_w || campo_dirf(vl_rendimento_jun_w)
				|| separador_w || campo_dirf(vl_rendimento_jul_w) 	|| separador_w || campo_dirf(vl_rendimento_ago_w) || separador_w || campo_dirf(vl_rendimento_set_w)
				|| separador_w || campo_dirf(vl_rendimento_out_w) 	|| separador_w || campo_dirf(vl_rendimento_nov_w) || separador_w || campo_dirf(vl_rendimento_dez_w)
				|| separador_w || '' || separador_w;

	nr_seq_apres_p := nr_seq_apres_p + 1;

	insert 	into w_dirf_arquivo
		(nr_sequencia,
		nm_usuario,
		nm_usuario_nrec,
		dt_atualizacao,
		dt_atualizacao_nrec,
		ds_arquivo,
		nr_seq_apresentacao,
		nr_seq_registro,
		cd_darf)
	values	(w_dirf_arquivo_seq.nextval,
		nm_usuario_p,
		nm_usuario_p,
		sysdate,
		sysdate,
		ds_arquivo_w,
		to_char(nr_seq_apres_p),
		0,
		cd_darf_p);

	end;
end if;

commit;

nr_seq_apres_p	:= nr_seq_apres_p + 1;

commit;

end dirf_grava_RIIRP;
/
