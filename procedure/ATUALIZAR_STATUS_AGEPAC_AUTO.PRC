create or replace
procedure atualizar_status_agepac_auto(cd_pessoa_fisica_p	varchar2,
					ie_atualizou_dados_p	varchar2,
					cd_tipo_agenda_p	varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

cd_agenda_w	number(10,0);
nr_seq_agenda_w	number(10,0);			 
			 
Cursor C01 is
	select  a.cd_agenda,
		b.nr_sequencia
	from	agenda a,
		agenda_paciente b
	where	b.ie_status_agenda not in ('L', 'C', 'B', 'LF','A','F','I')
	and	b.nr_atendimento is null
	and	b.dt_agenda between trunc(sysdate) and trunc(sysdate) + 86399/86400
	and	b.cd_pessoa_fisica  = cd_pessoa_fisica_p
	and	a.cd_agenda = b.cd_agenda
	and	a.cd_tipo_agenda = cd_tipo_agenda_p
	and	trunc(b.dt_agenda) = trunc(sysdate)
	order by b.dt_agenda;
			 
			 
begin

open C01;
loop
fetch C01 into	
	cd_agenda_w,
	nr_seq_agenda_w;
exit when C01%notfound;
	begin
	alterar_status_agenda(cd_agenda_w,nr_seq_agenda_w,'A',null,wheb_mensagem_pck.get_texto(799655),'N','Tasy');
	
	update	agenda_paciente
	set	ie_atualizou_autoatendimento = nvl(ie_atualizou_dados_p,'N')
	where	nr_sequencia = nr_seq_agenda_w;
	
	end;
end loop;
close C01;


commit;

end atualizar_status_agepac_auto;
/
