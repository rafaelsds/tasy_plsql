create or replace
procedure consistir_horario_agecons	(cd_agenda_p			number,
						dt_horario_p			date,
						qt_duracao_p			number,
						ie_horario_p			varchar2,
						ds_consistencia_p out	varchar2) is

qt_horario_w		number(10,0);
ie_bloqueio_w		varchar2(1);
ds_consistencia_w	varchar2(255);
qt_amount_w         agenda_turno.nr_amount%type;
ie_sobreposicao_encaixe_w  parametro_agenda.ie_sobreposicao_encaixe%type;
ie_sobreposicao_param_w  parametro_agenda.ie_consiste_duracao%type;
ie_sobrep_encaixe_par_ageint_w VARCHAR2(1);
qt_duracao_w  agenda_consulta.nr_minuto_duracao%TYPE;

begin

select  nvl(max(ie_sobreposicao_encaixe),'S'),
        nvl(max(ie_consiste_duracao), 'I')
into    ie_sobreposicao_encaixe_w,
        ie_sobreposicao_param_w
from  	parametro_agenda
where  cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

obter_param_usuario(869,162, obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_sobrep_encaixe_par_ageint_w);

SELECT  Decode(qt_duracao_p,0,1,qt_duracao_p)
INTO    qt_duracao_w 
FROM    dual;

if	(cd_agenda_p is not null) and
	(dt_horario_p is not null) and
	(qt_duracao_p is not null) and
	(ie_horario_p is not null) then
	/* encaixe */
	if	(ie_horario_p = 'E') then
		/* consistir sobreposicao */
		select	count(*)
		into	qt_horario_w
		from	agenda_consulta
		where	cd_agenda = cd_agenda_p
		and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_horario_p)
		and	dt_agenda > dt_horario_p
		and	dt_agenda < dt_horario_p + qt_duracao_p / 1440
		and	ie_status_agenda	<> 'C';

		if (qt_horario_w > 0) then
			ds_consistencia_w :=  Wheb_mensagem_pck.get_texto(455990);
		end if;

		/* consistir bloqueio */
		consistir_bloqueio_agenda(cd_agenda_p,dt_horario_p,obter_cod_dia_semana(dt_horario_p),ie_bloqueio_w);
		
		if	(ie_bloqueio_w = 'S') then
			ds_consistencia_w := Wheb_mensagem_pck.get_texto(455992);
		end if;

	elsif	(ie_horario_p = 'S') then
		select	count(*)
		into	qt_horario_w
		from	agenda_consulta
		where	cd_agenda = cd_agenda_p
		and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_agenda) = ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_horario_p)
		and	trunc(dt_agenda,'mi') = trunc(dt_horario_p,'mi')
		and	ie_status_agenda not in ('C','L');

		qt_amount_w := get_cons_shift_amount(cd_agenda_p,trunc(dt_horario_p, 'MI'),3);

		if	(qt_horario_w >= qt_amount_w) then
			ds_consistencia_w := Wheb_mensagem_pck.get_texto(455993);
		end if;
	end if;
end if;

ds_consistencia_p := ds_consistencia_w;

end consistir_horario_agecons;
/
