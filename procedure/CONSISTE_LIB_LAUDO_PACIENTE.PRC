create or replace
procedure consiste_lib_laudo_paciente(
		nr_prescricao_p			number,
		nr_sequencia_p			number,
		ie_consiste_congelamento_p	varchar2,
		ds_mensagem_erro_p out		varchar2) is
		
nr_seq_lab_w		varchar2(20);		
ie_prescr_amostra_w 	varchar2(1);
ds_mensagem_erro_w 	varchar2(255);

begin

if	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) and
	(ie_consiste_congelamento_p is not null) then
	begin
	if 	(ie_consiste_congelamento_p = 'S') then
		begin
		select	max(c.nr_seq_lab)
		into	nr_seq_lab_w
		from   	tipo_amostra b,
			prescr_procedimento c,
			prescr_proc_peca a
		where  	a.nr_seq_tipo 	= b.nr_sequencia
		and	a.nr_prescricao = c.nr_prescricao
		and	a.nr_seq_prescr	= c.NR_sequencia
		and	obter_atendimento_prescr(a.nr_prescricao) = obter_atendimento_prescr(nr_prescricao_p)
		and	b.ie_tipo_exame = 5;
		
		if 	(nr_seq_lab_w <> '') then
			begin
			ie_prescr_amostra_w	:= obter_se_presc_amostra(nr_prescricao_p);
			
			if	(ie_prescr_amostra_w = 'N') then
				begin
				--'Esse laudo n�o poder� ser liberado!#@#@'
				wheb_mensagem_pck.exibir_mensagem_abort(264054);
				end;
			end if;
			end;
		end if;
		end;
	end if;
	ds_mensagem_erro_w := consiste_regra_liberacao_laudo(nr_sequencia_p);
	end;
end if;
commit;
ds_mensagem_erro_p	:= ds_mensagem_erro_w;
end consiste_lib_laudo_paciente;
/