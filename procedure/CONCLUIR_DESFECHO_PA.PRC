create or replace
procedure concluir_desfecho_pa (
		ie_medico_p			varchar2,
		ie_desfecho_p			varchar2,
		cd_medico_destino_p		varchar2,
		nr_atendimento_p		number,
		ie_gerar_transf_setor_p		varchar2,
		cd_setor_desfecho_p		number,
		cd_setor_transferir_p		number,
		cd_unidade_basica_p		varchar2,
		cd_unidade_compl_p		varchar2,		
		cd_tipo_acomodacao_p		number,
		ie_gerar_evolucao_p		varchar2,
		ie_acao_executada_p		number,
		nr_sequencia_desfecho_p		number,
		cd_estab_paciente_p		number,
		cd_convenio_p			number,
		cd_pessoa_atend_p out		varchar2,
		nr_seq_vaga_p out		number,
		nr_seq_autorizacao_p out	number,
		ds_mensagem_p out		varchar2,
		nm_usuario_p			varchar2) is

nr_atend_gerado_w	number(10,0);
ie_tipo_evol_usuario_w	varchar2(3);
cd_pessoa_usuario_w	varchar2(10);
ds_mensagem_w		varchar2(2000);
dt_desfecho_w		date;
cd_medico_dest_w	varchar2(10);	

begin
if	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	nvl(max(nr_atendimento),0)
	into	nr_atend_gerado_w
	from	atendimento_paciente
	where	nr_atend_alta = nr_atendimento_p;		
	
	if	(ie_medico_p = 'S') and
		(ie_desfecho_p = 'I') and
		(cd_medico_destino_p is not null) then
		begin
		if	(nr_atend_gerado_w > 0) then
			begin
			assumir_paciente(nr_atend_gerado_w, cd_medico_destino_p, null, nm_usuario_p);
			end;
		end if;
		end;
	end if;
	
	if	(ie_gerar_transf_setor_p = 'S') and
		(ie_desfecho_p = 'T') and
		(cd_setor_desfecho_p = cd_setor_transferir_p) then
		begin
		gerar_transferencia_paciente(
			nr_atendimento_p,
			cd_setor_transferir_p,
			cd_unidade_basica_p,
			cd_unidade_compl_p,
			cd_tipo_acomodacao_p,
			null,
			null,
			null,
			nm_usuario_p,
			sysdate);
			
		gerar_ajustes_ap_lote( 'M', nr_atendimento_p, nm_usuario_p);
		end;
	end if;
	
	if	(ie_gerar_evolucao_p = 'S') and
		(ie_acao_executada_p <> 3) then
		begin
		select	max(ie_tipo_evolucao),
			max(cd_pessoa_fisica)
		into	ie_tipo_evol_usuario_w,
			cd_pessoa_usuario_w
		from	usuario
		where	nm_usuario = nm_usuario_p;
		gerar_evolucao_orient_alta(nr_sequencia_desfecho_p, ie_tipo_evol_usuario_w, cd_pessoa_usuario_w, nm_usuario_p);
		end;
	end if;
	
	if	(nr_atend_gerado_w > 0) then
		begin
		cd_pessoa_atend_p := obter_pessoa_atendimento(nr_atend_gerado_w,'C');
		end;
	else
		begin
		cd_pessoa_atend_p := obter_pessoa_atendimento(nr_atendimento_p,'C');
		end;
	end if;	
	
	select	max(nr_sequencia)
	into	nr_seq_vaga_p
	from 	gestao_vaga
	where 	nr_atendimento = nr_atendimento_p;
	
	select	min(nr_sequencia)
	into	nr_seq_autorizacao_p
	from 	autorizacao_convenio
	where	nr_seq_gestao = nvl(nr_seq_vaga_p,0);
	
	select	substr(max(ds_mensagem_desfecho),1,2000)
	into	ds_mensagem_p
	from	convenio_estabelecimento
	where	cd_estabelecimento	= cd_estab_paciente_p
	and	cd_convenio		= cd_convenio_p;
	end;
end if;
commit;
end concluir_desfecho_pa;
/