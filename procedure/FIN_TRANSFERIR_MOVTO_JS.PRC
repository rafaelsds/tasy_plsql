create or replace
procedure fin_transferir_movto_js(	nr_seq_retorno_p		number,
				nr_seq_retorno_transf_p	number,
				nm_usuario_p		Varchar2) is 

begin

if	(nvl(nr_seq_retorno_transf_p, 0) <> 0 and
	nvl(nr_seq_retorno_p, 0) <> 0) then
	begin
	update	convenio_retorno_movto
	set	nr_seq_retorno	= nr_seq_retorno_transf_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_seq_retorno	= nr_seq_retorno_p
	and	nr_seq_ret_item is null
	and	Nvl(ie_gera_resumo,'S')	= 'S';
	commit;
	
	end;
end if;

end fin_transferir_movto_js;
/
