create or replace
procedure gap_incluir_etapas(	nr_sequencia_p 	number,
			nm_usuario_p	varchar2,
			ds_erro_p	      out	varchar2) is

cd_pessoa_fisica_w	varchar2(10);
nr_sequencia_w		number(10);
nr_seq_tipo_w		number(10);
nr_sequencia_doc_w	number(10);
qt_dias_w		number(15,2);
dt_aux_w			date		:= sysdate;
ie_regra_pessoa_w		varchar(2);
nr_seq_depto_w		number(10);
ds_titulo_etapa_w		varchar(80);
cd_especialidade_w	number(10);
nr_sequencia_checklist_w number(10);

cursor c01 is
select	a.ie_regra_pessoa,
	a.nr_seq_depto,
	a.ds_titulo_etapa,
	a.cd_pessoa_fisica
from	gap_ritual a
where	a.nr_seq_tipo	= nr_seq_tipo_w
and	a.ie_situacao	= 'A'
and	not exists(	select	1
		from	gap_processo_etapa y
		where	y.nr_seq_processo	= nr_sequencia_p
		and	y.nr_seq_etapa	= a.nr_sequencia);

cursor c03 is
select	a.cd_pessoa_fisica,
	a.nr_sequencia,
	a.qt_dia,
	a.ie_regra_pessoa,
	a.nr_seq_depto,
	a.ds_titulo_etapa
from	gap_ritual a
where	a.nr_seq_tipo	= nr_seq_tipo_w
and	a.ie_situacao	= 'A'
and	not exists(	select	1
		from	gap_processo_etapa y
		where	y.nr_seq_processo	= nr_sequencia_p
		and	y.nr_seq_etapa	= a.nr_sequencia)
order by a.nr_seq_apres;

cursor c02 is
select	nr_sequencia
from	gap_tipo_doc
where	nr_seq_tipo	= nr_seq_tipo_w;

cursor c04 is
select	nr_sequencia
from	gap_checklist
where	nr_seq_process_type	= nr_seq_tipo_w and ie_situacao = 'A';

begin

select	nr_seq_tipo,
	cd_especialidade
into	nr_seq_tipo_w,
	cd_especialidade_w
from	gap_processo
where	nr_sequencia	= nr_sequencia_p;

ds_erro_p	:= null;

open c01;
loop
fetch c01 into
	ie_regra_pessoa_w,
	nr_seq_depto_w,
	ds_titulo_etapa_w,
	cd_pessoa_fisica_w;
exit when c01%notfound;

	if	(ie_regra_pessoa_w = 'P') and
		(cd_pessoa_fisica_w is null) then

		ds_erro_p		:= ds_erro_p || ds_titulo_etapa_w || WHEB_MENSAGEM_PCK.get_texto(278020) || chr(13) || chr(10);

	elsif	(ie_regra_pessoa_w = 'D') and
		(nr_seq_depto_w is null) then

		ds_erro_p	:= ds_erro_p || ds_titulo_etapa_w || WHEB_MENSAGEM_PCK.get_texto(278021) || chr(13) || chr(10);

	elsif	(ie_regra_pessoa_w = 'DE') and
		(cd_especialidade_w is null) then

		ds_erro_p		:= ds_erro_p || ds_titulo_etapa_w || WHEB_MENSAGEM_PCK.get_texto(278022) || chr(13) || chr(10);

	end if;
end loop;
close c01;

if	(ds_erro_p is null) then
	begin

	open c03;
	loop
	fetch c03 into
		cd_pessoa_fisica_w,
		nr_sequencia_w,
		qt_dias_w,
		ie_regra_pessoa_w,
		nr_seq_depto_w,
		ds_titulo_etapa_w;
	exit when c03%notfound;
	
		if	(ie_regra_pessoa_w = 'D')	and
			(nr_seq_depto_w is not null)	then
			begin
			cd_pessoa_fisica_w	:= obter_resp_depto(nr_seq_depto_w,sysdate);
			end;
		elsif	(ie_regra_pessoa_w = 'DE') and
			(cd_especialidade_w is not null) then
			begin
			
			select	max(nr_seq_depto)
			into	nr_seq_depto_w
			from	depto_medico_espec
			where	cd_especialidade = cd_especialidade_w;
	
			cd_pessoa_fisica_w	:= obter_resp_depto(nr_seq_depto_w,sysdate);
			
			end;
		end if;

		insert into gap_processo_etapa(
			nr_sequencia,
			nr_seq_processo,
			nr_seq_etapa,
			dt_atualizacao,
			nm_usuario,
			cd_pessoa_fisica,
			ie_resultado,
			dt_prevista,
			nr_seq_depto)
		values(	gap_processo_etapa_seq.nextval,
			nr_sequencia_p,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			cd_pessoa_fisica_w,
			'N',
			(dt_aux_w + qt_dias_w),
			nr_seq_depto_w);

		dt_aux_w:= dt_aux_w + qt_dias_w;

	end loop;
	close c03;

	open c02;
	loop
	fetch c02 into
		nr_sequencia_doc_w;
	exit when c02%notfound;

		insert into gap_processo_doc(
			nr_sequencia,
			nr_seq_processo,
			nr_seq_doc,
			dt_atualizacao,
			nm_usuario)
		values(	gap_processo_doc_seq.nextval,
			nr_sequencia_p,
			nr_sequencia_doc_w,
			sysdate,
			nm_usuario_p);

	end loop;
	close c02;
  
  open c04;
	loop
	fetch c04 into
		nr_sequencia_checklist_w;
	exit when c04%notfound;

		insert into gap_process_checklist(
			nr_sequencia,
			nr_seq_process,
			nr_seq_checklist,
			dt_atualizacao,
			nm_usuario,
                                                dt_atualizacao_nrec,
                                                nm_usuario_nrec)
		values(	gap_process_checklist_seq.nextval,
			nr_sequencia_p,
			nr_sequencia_checklist_w,
			sysdate,
			nm_usuario_p,
                                                sysdate,
			nm_usuario_p);

	end loop;
	close c04;

	end;
end if;

commit;

end gap_incluir_etapas;
/