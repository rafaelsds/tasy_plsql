CREATE OR REPLACE
PROCEDURE atualizar_avf_resultado_item( nr_seq_nota_item_p		NUMBER,
				      ds_justificativa_p      		VARCHAR2,
				      nr_sequencia_p 		NUMBER) IS 

BEGIN
UPDATE	avf_resultado_item 
SET	nr_seq_nota_item = nr_seq_nota_item_p,		
	ds_justificativa = ds_justificativa_p      
WHERE 	nr_sequencia = nr_sequencia_p;


COMMIT;

END atualizar_avf_resultado_item;
/  