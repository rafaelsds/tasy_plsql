create or replace
procedure plt_gerar_ordem_apresent (
			nr_regra_ordem_p	number) is
begin

update	w_rep_t
set	nr_seq_apres_grupo = 999,
	nr_seq_apres_inf = 999
where	1 = 1;

plt_gerar_ordem_apres_grupo(nr_regra_ordem_p);

end plt_gerar_ordem_apresent;
/