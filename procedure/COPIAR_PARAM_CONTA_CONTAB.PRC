create or replace
procedure copiar_param_conta_contab(	cd_estabelecimento_p	number,
					cd_sequencia_parametro_p	number,
					nm_usuario_p		Varchar2) is

cd_empresa_w				number(5);
cd_estabelecimento_w			number(5);
ds_observacao_w				varchar2(255);
nm_estab_origem_w			varchar2(80);
dt_atualizacao_w				varchar2(110);

cursor c01 is
select	cd_estabelecimento
from	estabelecimento
where	ie_situacao		= 'A'
and	cd_empresa		= cd_empresa_w
and	cd_estabelecimento	<> cd_estabelecimento_p
order by 1;

begin

cd_empresa_w		:= obter_empresa_estab(cd_estabelecimento_p);
dt_atualizacao_w	:= to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');
nm_estab_origem_w	:= substr(obter_nome_estabelecimento(cd_estabelecimento_p),1,80);

ds_observacao_w		:= substr(wheb_mensagem_pck.get_texto(298058, 'DT_ATUALIZACAO_W=' || dt_atualizacao_w || ';NM_USUARIO_P=' || nm_usuario_p || ';NM_ESTAB_ORIGEM_W=' || nm_estab_origem_w),1,255);

open C01;
loop
fetch C01 into
	cd_estabelecimento_w;
exit when C01%notfound;
	begin

	insert into parametros_conta_contabil(
		cd_sequencia_parametro,
		cd_estabelecimento,
		cd_conta_receita,
		cd_conta_estoque,
		cd_conta_passag_direta,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		cd_material,
		cd_area_proced,
		cd_especial_proced,
		cd_grupo_proced,
		cd_procedimento,
		nm_usuario,
		dt_atualizacao,
		ie_origem_proced,
		cd_conta_despesa,
		cd_setor_atendimento,
		ie_tipo_atendimento,
		ie_classif_convenio,
		cd_convenio,
		ie_clinica,
		cd_local_estoque,
		cd_operacao_estoque,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_centro_custo,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		nr_seq_grupo,
		nr_seq_subgrupo,
		nr_seq_forma_org,
		ds_observacao,
		cd_categoria_convenio,
		ie_tipo_convenio,
		cd_plano,
		cd_empresa,
		nr_seq_motivo_solic,
		cd_conta_redut_receita,
		cd_conta_gratuidade,
		ie_complexidade_sus,
		ie_tipo_financ_sus,
		cd_centro_custo_receita,
		cd_conta_rec_pacote,
		ie_tipo_centro,
		ie_tipo_tributo_item,
		cd_conta_ajuste_prod,
		cd_historico,
		cd_conta_desp_pre_fatur,
		nr_seq_regra_valor)
	select	parametros_conta_contabil_seq.nextval,
		cd_estabelecimento_w,
		cd_conta_receita,
		cd_conta_estoque,
		cd_conta_passag_direta,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		cd_material,
		cd_area_proced,
		cd_especial_proced,
		cd_grupo_proced,
		cd_procedimento,
		nm_usuario_p,
		sysdate,
		ie_origem_proced,
		cd_conta_despesa,
		cd_setor_atendimento,
		ie_tipo_atendimento,
		ie_classif_convenio,
		cd_convenio,
		ie_clinica,
		cd_local_estoque,
		cd_operacao_estoque,
		sysdate,
		nm_usuario_p,
		cd_centro_custo,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		nr_seq_grupo,
		nr_seq_subgrupo,
		nr_seq_forma_org,
		substr(ds_observacao_w || chr(13) || chr(10) || ds_observacao,1,255),
		cd_categoria_convenio,
		ie_tipo_convenio,
		cd_plano,
		cd_empresa,
		nr_seq_motivo_solic,
		cd_conta_redut_receita,
		cd_conta_gratuidade,
		ie_complexidade_sus,
		ie_tipo_financ_sus,
		cd_centro_custo_receita,
		cd_conta_rec_pacote,
		ie_tipo_centro,
		ie_tipo_tributo_item,
		cd_conta_ajuste_prod,
		cd_historico,
		cd_conta_desp_pre_fatur,
		nr_seq_regra_valor
	from	parametros_conta_contabil
	where	cd_sequencia_parametro	= cd_sequencia_parametro_p;

	end;
end loop;
close C01;

commit;

end copiar_param_conta_contab;
/
