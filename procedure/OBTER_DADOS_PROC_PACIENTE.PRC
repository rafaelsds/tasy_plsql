create or replace
procedure Obter_dados_proc_paciente(	nr_sequencia_p		in number,
				cd_procedimento_p		out number,
				ie_origem_proced_p	out number,
				nr_seq_exame_p		out number,
				nr_seq_proc_interno_p	out number) is 

begin

if (nr_sequencia_p is not null) then
	
	select	cd_procedimento,
		ie_origem_proced,
		nvl(nr_seq_exame,0),
		nvl(nr_seq_proc_interno,0)
	into	cd_procedimento_p,
		ie_origem_proced_p,
		nr_seq_exame_p,
		nr_seq_proc_interno_p
	from	procedimento_paciente
	where	nr_sequencia = nr_sequencia_p;	
	
end if;

end Obter_dados_proc_paciente;
/