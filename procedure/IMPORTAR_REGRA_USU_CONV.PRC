create or replace
procedure importar_regra_usu_conv(
	cd_convenio_p number,
	cd_categoria_p number,
	ie_tipo_atendimento_p number,
   	ie_clinica_p number,
  	nr_pos_inicial_p number,
   	nr_pos_final_p number,
   	cd_usuario_padrao_p varchar2,
   	ie_regra_bloqueio_p varchar2,
   	ds_observacao_p varchar2,
   	nm_usuario_p varchar2) is

begin

insert into conv_regra_usuario_categ(
	  nr_sequencia,
  	 cd_convenio,
  	 cd_categoria,
	 ie_tipo_atendimento,
 	 ie_clinica,
	 nr_pos_inicial,
	 nr_pos_final,
	 cd_usuario_padrao,
	 ie_regra_bloqueio,
	 ds_observacao,
	 nm_usuario,
	 cd_estabelecimento,
	 dt_atualizacao
	 )
	 values
	 (
	 conv_regra_usuario_categ_seq.nextval,
	 cd_convenio_p,	
	 cd_categoria_p,
	 ie_tipo_atendimento_p,
	 ie_clinica_p,
	 nr_pos_inicial_p,
	 nr_pos_final_p,
	 cd_usuario_padrao_p,
	 ie_regra_bloqueio_p,
	 ds_observacao_p,
	 nm_usuario_p,
	 wheb_usuario_pck.get_cd_estabelecimento,
	 sysdate
	 );	

commit;

end importar_regra_usu_conv;
/
 