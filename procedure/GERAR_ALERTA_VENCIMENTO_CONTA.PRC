create or replace
procedure gerar_alerta_vencimento_conta(nr_atendimento_p	Varchar2,
					dt_alta_p		date,
					nm_usuario_p		Varchar2) is 
					
cd_convenio_w		number(10);
cd_estabelecimento_w	number(10);
nr_dias_venc_atend_w	number(10);
nr_dias_alerta_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ds_alerta_w		varchar2(255);
dt_alerta_w		date;
dt_fim_alerta_w		date;

begin

cd_convenio_w :=  obter_convenio_atendimento(nr_atendimento_p);

select	max(cd_estabelecimento),
	max(cd_pessoa_fisica)
into	cd_estabelecimento_w,
	cd_pessoa_fisica_w
from 	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select	max(nr_dias_venc_atend),
        max(nr_dias_alerta)
into	nr_dias_venc_atend_w,
	nr_dias_alerta_w
from	convenio_estabelecimento
where	cd_convenio = cd_convenio_w
and	cd_estabelecimento = cd_estabelecimento_w;

if	(nvl(nr_dias_venc_atend_w,0) > 0) and
        (nvl(nr_dias_alerta_w,0) > 0) then
	
	dt_alerta_w	:= dt_alta_p + nr_dias_alerta_w;
	dt_fim_alerta_w	:= dt_alerta_w + nr_dias_venc_atend_w;
		
	ds_alerta_w := WHEB_MENSAGEM_PCK.get_texto(457725,'dt_fim_alerta='||PKG_DATE_FORMATERS.TO_VARCHAR(dt_fim_alerta_w, 'shortDate', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p));
	
	insert into atendimento_alerta (	cd_estabelecimento,
					nr_atendimento,
					ds_alerta,
					dt_atualizacao,
					dt_alerta,
					dt_liberacao,
					dt_fim_alerta,
					ie_situacao,
					nm_usuario,
					nr_sequencia)
			values	   (	cd_estabelecimento_w,
					nr_atendimento_p,
					ds_alerta_w,
					sysdate,
					dt_alerta_w,
					dt_alerta_w,
					dt_fim_alerta_w,
					'A',
					nm_usuario_p,
					atendimento_alerta_seq.nextval);
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_alerta_vencimento_conta;
/