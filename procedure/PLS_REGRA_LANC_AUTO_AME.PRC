create or replace
procedure pls_regra_lanc_auto_ame
			(	nr_seq_contrato_p	number,
				nm_usuario_p		varchar2,
				ie_commit_p		varchar2) is

cd_cgc_estipulante_w		pls_contrato.cd_cgc_estipulante%type;
nr_seq_empresa_w		pls_ame_empresa.nr_sequencia%type;
nr_seq_regra_ger_dest_w		pls_ame_regra_ger_dest.nr_sequencia%type;
nr_seq_regra_ger_arq_w		pls_ame_regra_ger_arq.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,
		nr_seq_regra_ame
	from	pls_regra_lanc_automatico
	where	ie_evento = 15;

Cursor C02 (	nr_seq_regra_lanc_aut_pc	number) is
	select	nr_sequencia
	from	pls_ame_regra_ger_arq
	where	nr_seq_regra_lanc_aut	= nr_seq_regra_lanc_aut_pc;

begin

select	max(cd_cgc_estipulante)
into	cd_cgc_estipulante_w
from	pls_contrato
where	nr_sequencia	= nr_seq_contrato_p;

if	(cd_cgc_estipulante_w is not null) then
	for r_c01_w in C01 loop
		begin
		
		select	max(nr_sequencia)
		into	nr_seq_empresa_w
		from	pls_ame_empresa
		where	cd_cgc	= cd_cgc_estipulante_w;
		
		if	(nr_seq_empresa_w is null) then
			select	pls_ame_empresa_seq.nextval
			into	nr_seq_empresa_w
			from	dual;
			
			insert	into	pls_ame_empresa
				(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_cgc,
					ie_enviar_email)
				values(	nr_seq_empresa_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_cgc_estipulante_w,
					'N');
		end if;
		
		select	pls_ame_regra_ger_dest_seq.nextval
		into	nr_seq_regra_ger_dest_w
		from	dual;
		
		insert	into	pls_ame_regra_ger_dest
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_regra_geracao,
				nr_seq_ame_empresa)
			values(	nr_seq_regra_ger_dest_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				r_c01_w.nr_seq_regra_ame,
				nr_seq_empresa_w);
		
		for r_c02_w in C02(r_c01_w.nr_sequencia) loop
			begin
			select	pls_ame_regra_ger_arq_seq.nextval
			into	nr_seq_regra_ger_arq_w
			from	dual;
			
			insert	into	pls_ame_regra_ger_arq
				(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_regra_ger_dest,
					ds_caminho_arquivo,
					nm_arquivo,
					qt_max_titular,
					qt_max_linha,
					ie_nm_arquivo_div,
					nm_objeto_geracao)
				(select	nr_seq_regra_ger_arq_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_regra_ger_dest_w,
					ds_caminho_arquivo,
					nm_arquivo,
					qt_max_titular,
					qt_max_linha,
					ie_nm_arquivo_div,
					nm_objeto_geracao
				from	pls_ame_regra_ger_arq
				where	nr_sequencia	= r_c02_w.nr_sequencia);
			
			insert	into	pls_ame_regra_ger_item
				(	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_regra_ger_arq,
					ie_regra_excecao,
					ie_tipo_item_mens,
					ie_consid_zerado,
					nr_seq_tipo_lanc)
				(select	pls_ame_regra_ger_item_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_regra_ger_arq_w,
					ie_regra_excecao,
					ie_tipo_item_mens,
					ie_consid_zerado,
					nr_seq_tipo_lanc
				from	pls_ame_regra_ger_item
				where	nr_seq_regra_ger_arq	= r_c02_w.nr_sequencia);
			end;
		end loop; --C02
		
		end;
	end loop; --C01
end if;

if	(nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;

end pls_regra_lanc_auto_ame;
/
