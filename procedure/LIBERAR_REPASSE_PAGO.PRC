create or replace
procedure	LIBERAR_REPASSE_PAGO	(nr_seq_retorno_p	number,
					nr_seq_grg_p	number,
					nr_seq_ret_glosa_p	number,
					nm_usuario_p		varchar2) is

begin

LIBERAR_REPASSE_PAGO_PROC(	nr_seq_retorno_p, nr_seq_grg_p, nr_seq_ret_glosa_p, nm_usuario_p);
LIBERAR_REPASSE_PAGO_MAT(	nr_seq_retorno_p, nr_seq_grg_p, nr_seq_ret_glosa_p, nm_usuario_p);

end	LIBERAR_REPASSE_PAGO;
/