create or replace
procedure pls_oc_cta_tratar_val_27(
			dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Realizar a consist�ncia das informa��es da limita��o do benefici�rio na estrutura de ocorr�ncia
	combinada.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Altera��es:
------------------------------------------------------------------------------------------------------------------
dlehmkuhl OS 688483 - 14/04/2014 -

Altera��o:	Modificada a forma de trabalho em rela��o a atualiza��o dos campos de controle
	que basicamente decidem se a ocorr�ncia ser� ou n�o gerada. Foi feita tamb�m a 
	substitui��o da rotina obterX_seX_geraX.

Motivo:	Necess�rio realizar essas altera��es para corrigir bugs principalmente no que se
	refere a quest�o de aplica��o de filtros (passo anterior ao da valida��o). Tamb�m
	tivemos um foco especial em performance, visto que a mesma precisou ser melhorada
	para n�o inviabilizar a nova solicita��o que diz que a exce��o deve verificar todo
	o atendimento.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cursor C01 (	nr_seq_oc_cta_comb_pc	dados_regra_p.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
		a.ie_valida_limitacao
	from	pls_oc_cta_val_limitacao	a
	where	a.nr_seq_oc_cta_comb		= nr_seq_oc_cta_comb_pc;

begin

-- Deve se ter informa��o de qual regra est� sendo verificada para que a valida��o possa ser aplicada.
if	(dados_regra_p.nr_sequencia is not null) then
	
	-- Percorrer as regras cadastradas para a valida��o na ocoor�ncia. Para esta regra defini-se que s� pode exitir um registro.
	for	r_C01_w in C01 (dados_regra_p.nr_sequencia) loop
	
		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
		
		-- S� continuar a valida��o se a regra cadastrada para a ocorr�ncia permitir.
		if	(r_C01_w.ie_valida_limitacao = 'S') then
			
			pls_oc_cta_aplic_val_limi_proc(
					dados_regra_p, nr_id_transacao_p, nm_usuario_p);
			
			pls_oc_cta_aplic_val_limi_mat(
					dados_regra_p, nr_id_transacao_p, nm_usuario_p);
					
			pls_oc_cta_aplic_val_limi_dia(
					dados_regra_p, nr_id_transacao_p, nm_usuario_p);
		end if;
	end loop; -- C01
	
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;

end pls_oc_cta_tratar_val_27;
/