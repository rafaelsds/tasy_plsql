create or replace
procedure pls_oc_cta_tratar_val_17(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de regra de valor de custo operacional
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:
 ------------------------------------------------------------------------------------------------------------------
jjung 29/06/2013 

Altera��o:	Adicionado parametro nos m�todos de atualiza��o dos campos IE_VALIDO e IE_VALIDO_TEMP 
	da PLS_TIPOS_OCOR_PCK
	
Motivo:	Se tornou necess�rio diferenciar os filtros das valida��es na hora de realizar esta opera��o
	para que os filtros de exce��o funcionem corretamente.
------------------------------------------------------------------------------------------------------------------
jjung OS 659889

Altera��o:	Alterado para que esta valida��o funcione como regra de exce��o.

Motivo:	Seguinda uma solicita��o da Unimed S�o Jos� do Rio Preto foi desenvolvida 
	a possibilidade de cadastro das regras como exce��es, portanto todas as valida��es
	devem ser migradas para funcionar com este novo conceito.
------------------------------------------------------------------------------------------------------------------
dlehmkuhl OS 688483 - 14/04/2014 -

Altera��o:	Modificada a forma de trabalho em rela��o a atualiza��o dos campos de controle
	que basicamente decidem se a ocorr�ncia ser� ou n�o gerada. Foi feita tamb�m a 
	substitui��o da rotina obterX_seX_geraX.

Motivo:	Necess�rio realizar essas altera��es para corrigir bugs principalmente no que se
	refere a quest�o de aplica��o de filtros (passo anterior ao da valida��o). Tamb�m
	tivemos um foco especial em performance, visto que a mesma precisou ser melhorada
	para n�o inviabilizar a nova solicita��o que diz que a exce��o deve verificar todo
	o atendimento.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_tb_selecao_w	pls_tipos_ocor_pck.dados_table_selecao_ocor;
ie_valido_w		pls_oc_cta_selecao_ocor_v.ie_valido%type;

-- Procedimentos que deveriam ter valor de custo operacional por�m o mesmo n�o foi calculado.
cursor cs_procs_vl_co_zero (	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
				ie_valido_pc		pls_oc_cta_selecao_ocor_v.ie_valido%type) is
	select	sel.nr_sequencia nr_seq_selecao,
		ie_valido_pc ie_valido,
		'Este procedimento n�o obteve uma regra v�lida de valor de custo operacional, ' ||
		'n�o foi calculado valor de custo operacional para o mesmo.' ds_observacao
	from	pls_oc_cta_selecao_ocor_v sel, 
		pls_conta_proc_ocor_v proc 
	where	sel.nr_id_transacao = nr_id_transacao_pc
	and	sel.ie_valido = 'S' 
	and	sel.ie_tipo_registro = 'P'
	and	proc.nr_seq_conta = sel.nr_seq_conta 
	and	proc.nr_sequencia = sel.nr_seq_conta_proc 
	and	proc.ie_co_preco_operadora = 'S' 
	and	proc.vl_custo_operacional is null 
	union all
	select	sel.nr_sequencia nr_seq_selecao,
		ie_valido_pc ie_valido,
		'Este procedimento n�o obteve uma regra v�lida de valor de custo operacional, ' ||
		'n�o foi calculado valor de custo operacional para o mesmo.' ds_observacao
	from	pls_oc_cta_selecao_ocor_v sel, 
		pls_conta_proc_ocor_v proc 
	where	sel.nr_id_transacao = nr_id_transacao_pc
	and	sel.ie_valido = 'S' 
	and	sel.ie_tipo_registro = 'P'
	and	proc.nr_seq_conta = sel.nr_seq_conta 
	and	proc.nr_sequencia = sel.nr_seq_conta_proc 
	and	proc.ie_co_preco_operadora = 'S'
	and 	proc.vl_custo_operacional <= 0;

-- Informa��es da valida��o de regra de valor de custo operacional
cursor C02 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
		a.ie_sem_regra_valor_co
	from	pls_oc_cta_val_valor_co a
	where	a.nr_seq_oc_cta_comb = nr_seq_oc_cta_comb_p;

begin

-- Deve haver informa��o da regra para que seja aplicada a valida��o.
if	(dados_regra_p.nr_sequencia is not null) then
	
	-- Varrer as regras da ocorr�ncia combinada.
	for	r_C02_w in C02(dados_regra_p.nr_sequencia) loop

		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
		ie_valido_w	:= 'S';
	
		-- S� aplicar a valida��o caso seja para verificar itens sem regra de valor de CO.
		if	(r_C02_w.ie_sem_regra_valor_co = 'S') then

			open cs_procs_vl_co_zero(	nr_id_transacao_p, ie_valido_w);
			loop
				
				pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
				
				fetch cs_procs_vl_co_zero bulk collect into 	dados_tb_selecao_w.nr_seq_selecao, dados_tb_selecao_w.ie_valido,
										dados_tb_selecao_w.ds_observacao 
										limit pls_cta_consistir_pck.qt_registro_transacao_w;
				
				exit when dados_tb_selecao_w.nr_seq_selecao.count = 0;
				
				-- Gravar na tabela de sele��o os procedimentos selecionados 
				pls_tipos_ocor_pck.gerencia_selecao_validacao(
						dados_tb_selecao_w.nr_seq_selecao, dados_tb_selecao_w.ds_seqs_selecao, 'SEQ', 
						dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
									
			end loop; -- cs_procs_vl_co_zero
			close cs_procs_vl_co_zero;
		end if;
		
		-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
		pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
	end loop; -- C02
end if;

end pls_oc_cta_tratar_val_17;
/
