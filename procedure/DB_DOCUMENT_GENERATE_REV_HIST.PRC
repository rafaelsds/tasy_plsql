create or replace procedure db_document_generate_rev_hist(
		nr_seq_documento_p	number,
		ds_historico_p		varchar2,
		dt_liberacao_p		date,
		nm_usuario_resp_p	varchar2,
		cd_revisao_p		varchar2,
		nm_usuario_p		varchar2)
is

nr_seq_w	number(10);
nr_seq_doc_origem_w	db_document.nr_sequencia%type;

begin
	select	db_document_hist_seq.nextval
	into	nr_seq_w
	from	dual;
	
	select	max(nvl(d.nr_seq_source, nr_seq_documento_p))
	into	nr_seq_doc_origem_w
	from	db_document d
	where	d.nr_sequencia = nr_seq_documento_p;
	
	insert
	into db_document_hist
		(
			nr_sequencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_atualizacao,
			nm_usuario,
			nr_seq_doc_origem,
			ds_historico,
			dt_liberacao,
			nr_seq_documento,
			nm_usuario_resp,
			cd_revisao
		)
		values
		(
			nr_seq_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_doc_origem_w,
			ds_historico_p,
			dt_liberacao_p,
			nr_seq_documento_p,
			nm_usuario_resp_p,
			cd_revisao_p
		);
	commit;
	
end db_document_generate_rev_hist;
/
