create or replace
procedure ctb_gerar_acomp_metrica(	cd_estabelecimento_p	number,
				nr_seq_mes_ref_p		number,
				nr_seq_cenario_p		number,
				cd_centro_custo_p		number,
				nm_usuario_p		varchar2) is

cd_conta_contabil_w		varchar2(20);
cd_centro_custo_w			number(08);
cd_empresa_w			number(04);
dt_inicial_w			date;
dt_inicial_ant_w			date;
dt_inicial_acum_w			date;
dt_final_w			date;
dt_final_acum_w			date;
dt_final_ant_w			date;
dt_referencia_w			date;
ds_gerencial_w			varchar2(255);
nr_seq_metrica_w			number(10);
qt_metrica_orc_w			number(15,2);
qt_metrica_real_w			number(15,2);
qt_metrica_orc_acum_w		number(15,2);
qt_metrica_real_acum_w		number(15,2);
qt_metrica_orc_ant_w		number(15,2);
qt_metrica_real_ant_w		number(15,2);
vl_orcado_w			number(15,2);
vl_realizado_w			number(15,2);
vl_saldo_w			number(15,4);
vl_ticket_orc_w			number(15,2);
vl_ticket_real_w			number(15,2);		
vl_ticket_orc_acum_w		number(15,2);
vl_ticket_real_acum_w		number(15,2);		
vl_ticket_orc_ant_w		number(15,2);
vl_ticket_real_ant_w		number(15,2);		

/* Procedure criada por Matheus para o relatorio WCTB 172 */

/* Centro de custo*/
cursor c01 is
select	distinct
	a.cd_centro_custo,
	substr(obter_desc_centro_custo(a.cd_centro_custo),1,40) ds_centro_custo
from	ctb_mes_ref b,
	ctb_orcamento a
where	a.nr_seq_mes_ref		= b.nr_sequencia
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	ctb_obter_se_centro_usuario(a.cd_centro_custo, cd_empresa_w, nm_usuario_p) = 'S'
and	b.dt_referencia between dt_inicial_w and dt_final_w
and	a.cd_centro_custo		= nvl(cd_centro_custo_p, a.cd_centro_custo)
and	a.nr_seq_metrica is not null
order by 1;

/*M�trica */
cursor c02 is
select	distinct
	a.nr_seq_metrica,
	substr(ctb_obter_desc_metrica(a.nr_seq_metrica),1,80) ds_metrica
from	ctb_mes_ref b,
	ctb_orcamento a
where	a.nr_seq_mes_ref		= b.nr_sequencia
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.nr_seq_metrica is not null
and	b.dt_referencia between dt_inicial_w and dt_final_w
and	a.cd_centro_custo		= cd_centro_custo_w
order by 1; 

/* Ticket m�dio*/
cursor c03 is
select	a.cd_conta_contabil,
	substr(obter_desc_conta_contabil(a.cd_conta_contabil),1,255) ds_conta_contabil,
	sum(decode(b.dt_referencia,dt_referencia_w,nvl(a.vl_ticket_medio_orc,0),0)) vl_ticket_orc,
	sum(decode(b.dt_referencia,dt_referencia_w,nvl(a.vl_ticket_medio_real,0),0)) vl_ticket_real,
	nvl(sum(a.vl_orcado),0) vl_orcado,
	nvl(sum(a.vl_realizado),0) vl_realizado
from	ctb_mes_ref b,
	ctb_orcamento a
where	a.nr_seq_mes_ref		= b.nr_sequencia
and	a.cd_estabelecimento	= cd_estabelecimento_p
and	a.cd_centro_custo		= cd_centro_custo_w
and	a.nr_seq_metrica		= nr_seq_metrica_w
and	b.dt_referencia between dt_inicial_w and dt_final_w
and	a.nr_seq_metrica is not null
group by	a.cd_conta_contabil
order by 1;

cursor c04 is
select	nvl(max(a.qt_metrica_orc),0) qt_metrica_orc,
	nvl(max(ctb_obter_metrica_real(cd_estabelecimento_p, cd_centro_custo_w, nr_seq_metrica_w,b.nr_sequencia)),0) qt_metrica_real
from	ctb_mes_ref b,
	ctb_orcamento a
where	a.nr_seq_mes_ref		= b.nr_sequencia
and	a.cd_estabelecimento		= cd_estabelecimento_p
and	a.cd_centro_custo		= cd_centro_custo_w
and	a.nr_seq_metrica		= nr_seq_metrica_w
and	b.dt_referencia between dt_inicial_w and dt_final_w
and	a.nr_seq_metrica is not null
group by	b.dt_referencia;

Vet04 	C04%RowType;

BEGIN					

dt_referencia_w		:= ctb_obter_mes_ref(nr_seq_mes_ref_p);
dt_inicial_w		:= trunc(dt_referencia_w,'yyyy'); /* Data inicio do ano para obter acumulado*/
dt_final_w		:= dt_referencia_w; /* M�s de refer�ncia*/
dt_inicial_acum_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_referencia_w,1,0);
dt_final_acum_w		:= PKG_DATE_UTILS.ADD_MONTH(dt_referencia_w,11,0);
dt_inicial_ant_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_inicial_w,-12,0);
dt_final_ant_w		:= PKG_DATE_UTILS.ADD_MONTH(dt_referencia_w,-12,0);

/*Limpa acompanhamento gerado*/
delete	from ctb_metrica_acomp;
commit;

select	obter_empresa_estab(cd_estabelecimento_p)
into	cd_empresa_w
from	dual;

open c01;
loop
fetch c01 into
	cd_centro_custo_w,
	ds_gerencial_w;
exit when c01%notfound;

	insert into ctb_metrica_acomp(
		nr_sequencia,
		cd_estabelecimento,
		nr_seq_cenario,
		nr_seq_metrica,
		nr_seq_mes_ref,
		cd_centro_custo,
		ds_gerencial,
		qt_metrica_orc,
		qt_metrica_real,
		vl_ticket_orc,
		vl_ticket_real,
		vl_orcado,
		vl_realizado,
		nm_usuario,
		nm_usuario_nrec,
		dt_atualizacao,
		dt_atualizacao_nrec,
		vl_orcado_ant,
		vl_realizado_ant)
	values(	ctb_metrica_acomp_seq.nextval,
		cd_estabelecimento_p,
		nr_seq_cenario_p,
		null,
		nr_seq_mes_ref_p,
		cd_centro_custo_w,
		ds_gerencial_w,
		0,
		0,
		0,
		0,
		0,
		0,
		nm_usuario_p,
		nm_usuario_p,
		sysdate,
		sysdate,
		null,
		null);
		
	/* In�cio grava��o das m�tricas do centro de custo*/
	open c02;
	loop
	fetch c02 into
		nr_seq_metrica_w,
		ds_gerencial_w;
	exit when c02%notfound;
		begin

		select	nvl(sum(ctb_obter_metrica_real(cd_estabelecimento_p, cd_centro_custo_w, nr_seq_metrica_w,b.nr_sequencia)),0)
		into	vl_saldo_w
		from	ctb_mes_ref b,
			ctb_orcamento a
		where	a.nr_seq_mes_ref		= b.nr_sequencia
		and	a.cd_estabelecimento	= cd_estabelecimento_p
		and	a.cd_centro_custo		= cd_centro_custo_w
		and	a.nr_seq_metrica		= nr_seq_metrica_w
		and	b.dt_referencia between dt_inicial_acum_w and dt_final_acum_w;
				
		/* Valor do m�s de referencia */
		select	nvl(max(a.qt_metrica_orc),0) qt_metrica_orc,
			nvl(max(ctb_obter_metrica_real(cd_estabelecimento_p, cd_centro_custo_w, nr_seq_metrica_w,nr_seq_mes_ref_p)),0) qt_metrica_real
		into	qt_metrica_orc_w,
			qt_metrica_real_w
		from	ctb_orcamento a
		where	a.cd_estabelecimento	= cd_estabelecimento_p
		and	a.nr_seq_mes_ref		= nr_seq_mes_ref_p
		and	a.cd_centro_custo		= cd_centro_custo_w
		and	a.nr_seq_metrica		= nr_seq_metrica_w;
		
		/* Metrica Orcada/Realizada Colunas Acumulado*/
		qt_metrica_orc_acum_w		:= 0;
		qt_metrica_real_acum_w		:= 0;
		open c04;
		loop
		fetch c04 into
			Vet04;
		exit when c04%notfound;
			qt_metrica_orc_acum_w	:= nvl(qt_metrica_orc_acum_w,0)  + Vet04.qt_metrica_orc;
			qt_metrica_real_acum_w	:= nvl(qt_metrica_real_acum_w,0) + Vet04.qt_metrica_real;
		end loop;
		close c04;
		
		/* Acumulado ano anterior*/
		qt_metrica_orc_ant_w		:= 0;
		qt_metrica_real_ant_w		:= 0;
		dt_inicial_w			:= dt_inicial_ant_w;
		dt_final_w			:= dt_final_ant_w;
		open c04;
		loop
		fetch c04 into
			Vet04;
		exit when c04%notfound;
			qt_metrica_orc_ant_w	:= nvl(qt_metrica_orc_ant_w,0)  + Vet04.qt_metrica_orc;
			qt_metrica_real_ant_w	:= nvl(qt_metrica_real_ant_w,0) + Vet04.qt_metrica_real;
		end loop;
		close c04;
		/*Fim acumulado ano anterior*/
		
		dt_inicial_w			:= trunc(dt_referencia_w,'yyyy');
		dt_final_w			:= dt_referencia_w;
		
		ds_gerencial_w	:= substr('     ' || ds_gerencial_w,1,255);
		
		insert into ctb_metrica_acomp(
			nr_sequencia,
			cd_estabelecimento,
			nr_seq_cenario,
			nr_seq_metrica,
			nr_seq_mes_ref,
			cd_centro_custo,
			ds_gerencial,
			qt_metrica_orc,
			qt_metrica_real,
			vl_ticket_orc,
			vl_ticket_real,
			vl_orcado,
			vl_realizado,
			vl_saldo,
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,
			dt_atualizacao_nrec,
			vl_orcado_ant,
			vl_realizado_ant)
		values(	ctb_metrica_acomp_seq.nextval,
			cd_estabelecimento_p,
			nr_seq_cenario_p,
			nr_seq_metrica_w,
			nr_seq_mes_ref_p,
			cd_centro_custo_w,
			ds_gerencial_w,
			qt_metrica_orc_w,
			qt_metrica_real_w,
			null,
			null,
			qt_metrica_orc_acum_w,
			qt_metrica_real_acum_w,
			vl_saldo_w,
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			sysdate,
			qt_metrica_orc_ant_w,
			qt_metrica_real_ant_w);

		/*Ultimo n�vel - Ticket m�dio*/
		open C03;
		loop
		fetch C03 into	
			cd_conta_contabil_w,
			ds_gerencial_w,
			vl_ticket_orc_w,
			vl_ticket_real_w,
			vl_orcado_w,
			vl_realizado_w;
		exit when C03%notfound;
			begin

			select	round(nvl(sum(a.vl_ticket_medio_real),0),2)
			into	vl_saldo_w
			from	ctb_mes_ref b,
				ctb_orcamento a
			where	a.nr_seq_mes_ref	= b.nr_sequencia
			and	a.cd_estabelecimento	= cd_estabelecimento_p
			and	a.cd_centro_custo	= cd_centro_custo_w
			and	a.nr_seq_metrica	= nr_seq_metrica_w
			and	a.cd_conta_contabil	= cd_conta_contabil_w
			and	b.dt_referencia between dt_inicial_acum_w and dt_final_acum_w;
		

			vl_ticket_orc_acum_w		:= dividir(vl_orcado_w, qt_metrica_orc_acum_w);
			vl_ticket_real_acum_w		:= dividir(vl_realizado_w, qt_metrica_real_acum_w);
			
			select	dividir(nvl(sum(a.vl_orcado),0), qt_metrica_orc_ant_w) vl_orcado,
				dividir(nvl(sum(a.vl_realizado),0), qt_metrica_real_ant_w) vl_realizado
			into	vl_ticket_orc_ant_w,
				vl_ticket_real_ant_w
			from	ctb_mes_ref b,
				ctb_orcamento a
			where	a.nr_seq_mes_ref	= b.nr_sequencia
			and	a.cd_estabelecimento	= cd_estabelecimento_p
			and	a.cd_centro_custo	= cd_centro_custo_w
			and	a.nr_seq_metrica	= nr_seq_metrica_w
			and	a.cd_conta_contabil	= cd_conta_contabil_w
			and	b.dt_referencia between dt_inicial_ant_w and dt_final_ant_w;

			insert into ctb_metrica_acomp(
				nr_sequencia,
				cd_estabelecimento,
				nr_seq_cenario,
				nr_seq_metrica,
				nr_seq_mes_ref,
				cd_centro_custo,
				ds_gerencial,
				qt_metrica_orc,
				qt_metrica_real,
				vl_ticket_orc,
				vl_ticket_real,
				vl_orcado,
				vl_realizado,
				vl_saldo,
				nm_usuario,
				nm_usuario_nrec,
				dt_atualizacao,
				dt_atualizacao_nrec,
				vl_orcado_ant,
				vl_realizado_ant)
			values(	ctb_metrica_acomp_seq.nextval,
				cd_estabelecimento_p,
				nr_seq_cenario_p,
				nr_seq_metrica_w,
				nr_seq_mes_ref_p,
				cd_centro_custo_w,
				ds_gerencial_w,
				null,
				null,
				vl_ticket_orc_w,
				vl_ticket_real_w,
				vl_ticket_orc_acum_w,
				vl_ticket_real_acum_w,
				vl_saldo_w,
				nm_usuario_p,
				nm_usuario_p,
				sysdate,
				sysdate,
				vl_ticket_orc_ant_w,
				vl_ticket_real_ant_w);		
			
			end;
		end loop;
		close C03;
		end;	
	end loop;
	close c02;
	commit;
end loop;
close c01;

commit;

END ctb_gerar_acomp_metrica;
/