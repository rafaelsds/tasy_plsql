create or replace
procedure pls_insere_paracer_guia_int
			(	nr_seq_guia_p		Number,
				nm_usuario_p		Varchar2) is 
				
ds_observacao_w			Varchar2(255);				

Cursor C01 is
	select	substr(ds_observacao,1,255)
	from	pls_guia_plano_historico
	where	nr_seq_guia		= nr_seq_guia_p
	and	ie_status_analise	is not null;
begin

open C01;
loop
fetch C01 into	
	ds_observacao_w;
exit when C01%notfound;
end loop;
close C01;

update	pls_guia_plano
set	ds_observacao	= ds_observacao_w,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_guia_p;

commit;

end pls_insere_paracer_guia_int;
/