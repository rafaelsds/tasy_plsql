Create or Replace
PROCEDURE Calcular_Dose_medic_edit(
				cd_material_p     		Number,				
				qt_altura_p			Number,
				qt_peso_p			Number,
				qt_sc_p				Number,
				qt_imc_p			Number,
				qt_dose_terap_p		in out	Number,
				nr_unid_med_terap_p		Number,
				qt_diluente_p			Number,
				qt_periodo_hora_p		Number,
				qt_periodo_min_p		Number,
				cd_intervalo_p			Varchar2,
				qt_dose_p		in out	Number,
				cd_unidade_medida_p	in out	Varchar2,
				qt_vel_infusao_p	in out	Number,
				ie_unidade_vel_p	in out	Varchar2,				
				qt_fator_correcao_p		Number,
				qt_dose_total_p		out	Number,
				qt_dose_interv_p		Number) is
 

qt_vol_total_w			Number(15,4);
qt_concent_med_w		Number(15,6);
qt_concent_med_mg_w		Number(13,4);
qt_concent_med_ml_w		Number(13,4) := 1;
qt_concent_dose_total_w		Number(13,4);
qt_velocidade_w			Number(13,4);
qt_dose_w			Number(18,6);
qt_dose_peso_w			Number(18,6);
qt_dose_mg_w			Number(18,6);
qt_dose_direta_w		Number(18,6);
qt_dose_ml_w			Number(18,6);
cd_unid_med_concetracao_w	Varchar2(30);	
cd_unid_med_base_conc_w		Varchar2(30);
ie_unidade_w			Varchar2(15);
ie_tempo_w			Varchar2(15);
ds_abreviacao_w			Varchar2(255);
ie_peso_w			Varchar2(15);
nr_ocorrencia_w			Number(15,4);
nr_ocorrencia_dia_w		Number(15,4);
qt_horas_w			Number(15,4);
qt_dose_unid_cons_w		Number(18,6);
qt_conversao_w			Number(18,6);
qt_peso_w			Number(15,4);
qt_dose_terap_w			Number(18,6);
ie_solucao_w			Varchar2(1);
qt_casas_retorno_w		number(5,0);

BEGIN

if	(cd_material_p > 0) then
	begin
	
	select	nvl(max(qt_casas),0)
	into	qt_casas_retorno_w
	from	rep_regra_arredond_dose
	where	qt_peso_p between qt_peso_inicial and qt_peso_final;
	
	if	(qt_casas_retorno_w = 0) then
		obter_param_usuario(1903, 2, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, 0, qt_casas_retorno_w);
	end if;
	
	select	ie_unidade,
		ie_tempo,
		ie_peso,
		ie_solucao,
		obter_desc_expressao(cd_exp_abreviacao,ds_abreviacao)
	into	ie_unidade_w,
		ie_tempo_w,
		ie_peso_w,
		ie_solucao_w,
		ds_abreviacao_w
	from	regra_dose_terap
	where	nr_sequencia	= nr_unid_med_terap_p;
	
	qt_peso_w	:= qt_peso_p;
	if	(ie_peso_w = 'G') then
		qt_peso_w	:= qt_peso_p * 1000;	
	end if;
	
	/* Convers�o da dose terap�utica para mg ou ml */
	
	if	(ie_unidade_w = 'MG') then
		qt_dose_terap_w	:= qt_dose_terap_p;
	elsif	(ie_unidade_w = 'MCG') then
		qt_dose_terap_w	:= dividir(qt_dose_terap_p,1000);
	elsif	(ie_unidade_w = 'G') then
		qt_dose_terap_w	:= qt_dose_terap_p * 1000;
	elsif	(ie_unidade_w = 'MEQ') then
		select	obter_conversao_unid_med_cons(cd_material_p,upper(obter_unid_med_usua('MEQ')),qt_dose_terap_p)
		into	qt_dose_unid_cons_w
		from	dual;
	elsif	(ie_unidade_w = 'UI') then
		select	obter_conversao_unid_med_cons(cd_material_p,upper(obter_unid_med_usua('UI')),qt_dose_terap_p)
		into	qt_dose_unid_cons_w
		from	dual;	
	elsif	(ie_unidade_w = 'L') then
		qt_dose_terap_w	:= qt_dose_terap_p * 1000;
	elsif	(ie_unidade_w = 'ML') then
		qt_dose_terap_w	:= qt_dose_terap_p;
	elsif	(ie_unidade_w = 'UE100') then
		qt_dose_terap_w	:= dividir(qt_dose_terap_p,100);
	elsif	(ie_unidade_w = 'GTS') then
		qt_dose_terap_w	:= dividir(qt_dose_terap_p,20);
	elsif	(ie_unidade_w = 'MCGT') then
		qt_dose_terap_w	:= dividir(qt_dose_terap_p,50);
	end if;

	if	(ie_unidade_w in ('MG','MCG','G')) then
		select	obter_conversao_unid_med_cons(cd_material_p,upper(obter_unid_med_usua('MG')),qt_dose_terap_w)
		into	qt_dose_unid_cons_w
		from	dual;
	elsif	(ie_unidade_w in ('L','ML','UE100')) then
		select	obter_conversao_unid_med_cons(cd_material_p,upper(obter_unid_med_usua('ML')),qt_dose_terap_w)
		into	qt_dose_unid_cons_w
		from	dual;
	end if;
	
	select	max(nvl(qt_conversao_mg,1)),
		max(upper(cd_unid_med_concetracao)),
		max(upper(cd_unid_med_base_conc))
	into	qt_concent_med_w,
		cd_unid_med_concetracao_w,
		cd_unid_med_base_conc_w
	from	material
	where	cd_material	= cd_material_p;
	
	select	Obter_ocorrencia_intervalo(cd_intervalo_p,qt_periodo_hora_p,'O'),
		Obter_ocorrencia_intervalo(cd_intervalo_p,24,'O'),
		Obter_ocorrencia_intervalo(cd_intervalo_p,qt_periodo_hora_p,'H')
	into	nr_ocorrencia_w,
		nr_ocorrencia_dia_w,
		qt_horas_w
	from	dual;
	
	/*Converter concentra��o para mg/ml*/
	
	if	(cd_unid_med_concetracao_w = 'MG') then
		qt_concent_med_mg_w	:= qt_concent_med_w;
	elsif	(cd_unid_med_concetracao_w = 'KG') then
		qt_concent_med_mg_w	:= qt_concent_med_w * 1000 * 1000;
	elsif	(cd_unid_med_concetracao_w = 'G') then
		qt_concent_med_mg_w	:= qt_concent_med_w * 1000;
	elsif	(cd_unid_med_concetracao_w = 'MCG') then
		qt_concent_med_mg_w	:= dividir(qt_concent_med_w,1000);
	end if;
	
	if	(cd_unid_med_base_conc_w = 'ML') then
		qt_concent_med_ml_w	:= 1;
	elsif	(cd_unid_med_base_conc_w = 'L') then
		qt_concent_med_ml_w	:= qt_concent_med_w * 1000;
	elsif	(cd_unid_med_base_conc_w = 'UE100') then
		qt_concent_med_ml_w	:= dividir(qt_concent_med_w,100);
	end if;
	
	if	(ie_solucao_w = 'N') then
		begin
		if	(ie_tempo_w is null) then
			if	(qt_casas_retorno_w  > 0) then
				--qt_dose_interv_p	:= round(qt_peso_w * qt_dose_terap_p, qt_casas_retorno_w);
				qt_dose_total_p		:= round(dividir((qt_periodo_hora_p * (qt_dose_interv_p * nr_ocorrencia_w)),24), qt_casas_retorno_w);
			else
				--qt_dose_interv_p	:= qt_peso_w * qt_dose_terap_p;
				qt_dose_total_p		:= dividir((qt_periodo_hora_p * (qt_dose_interv_p * nr_ocorrencia_w)),24);
			end if;	
		else
			if	(qt_casas_retorno_w  > 0) then
				qt_dose_total_p		:= round(dividir((qt_periodo_hora_p * (qt_dose_interv_p * nr_ocorrencia_w)),24),qt_casas_retorno_w);
				--qt_dose_interv_p	:= round(dividir((qt_peso_w * qt_dose_terap_p),nr_ocorrencia_w),qt_casas_retorno_w);
			else
				qt_dose_total_p		:= dividir((qt_periodo_hora_p * (qt_dose_interv_p * nr_ocorrencia_w)),24);
				--qt_dose_interv_p	:= dividir((qt_peso_w * qt_dose_terap_p),nr_ocorrencia_w);
			end if;
			qt_dose_unid_cons_w	:= round(qt_dose_unid_cons_w / nr_ocorrencia_w,5);
			if	(ie_tempo_w = 'M')  then
				qt_dose_total_p		:= qt_dose_total_p * 1440;
				--qt_dose_interv_p	:= qt_dose_interv_p * 1440;
				qt_dose_unid_cons_w	:= qt_dose_unid_cons_w * 1440;
			elsif	(ie_tempo_w = 'H') then
				qt_dose_total_p		:= qt_dose_total_p * 24;
				--qt_dose_interv_p	:= qt_dose_interv_p * 24;
				qt_dose_unid_cons_w	:= qt_dose_unid_cons_w * 24;
			end if;				
		end if;
	
		select	obter_conversao_unid_med(cd_material_p,cd_unidade_medida_p)
		into	qt_conversao_w
		from	dual;
		
		if	(ds_abreviacao_w = obter_desc_expressao(881462)) or
			(ds_abreviacao_w = obter_desc_expressao(881464)) then
			qt_peso_w	:= qt_sc_p;
		end if;
		if	(qt_casas_retorno_w  > 0) then
			qt_dose_p	:= round(qt_dose_unid_cons_w * qt_conversao_w * qt_peso_w, qt_casas_retorno_w);
		else
			qt_dose_p	:= qt_dose_unid_cons_w * qt_conversao_w * qt_peso_w;
		end if;
		
		--qt_dose_p	:= Ceil(qt_dose_p);
		
		end;
	elsif	(ie_solucao_w = 'S') then
		begin
		qt_dose_w		:= obter_conversao_ml(cd_material_p,qt_dose_p,cd_unidade_medida_p);
		qt_vol_total_w		:= qt_dose_w + qt_diluente_p;
		qt_concent_med_w	:= dividir(qt_concent_med_mg_w,qt_concent_med_ml_w) * qt_dose_w;
		
		/*Obter a concentra��o da dose total com base na unidade selecionada pelo m�dico */
		if	(ie_unidade_w = 'MCG') then
			qt_concent_dose_total_w	:= dividir(qt_concent_med_w,qt_vol_total_w) * 1000;
		elsif	(ie_unidade_w = 'MG') then
			qt_concent_dose_total_w	:= dividir(qt_concent_med_w,qt_vol_total_w);
		end if;
		if	(qt_dose_terap_p > 0) then
			begin
			
			qt_dose_direta_w	:= qt_dose_terap_p;
		
			/*Convers�o p/ unidade direta do elemento se for por peso*/
		
			if	(ie_peso_w is not null) then
				qt_dose_direta_w	:= qt_peso_w * qt_dose_terap_p;
			end if;
		
			qt_dose_ml_w	:= dividir(qt_dose_direta_w,qt_concent_dose_total_w);
			
			if	(ie_tempo_w ='M') or
				(ie_tempo_w ='H') then
				begin
				/*Transformar em ml/hora */
				if	(upper(ie_unidade_vel_p) = 'MLH') then
					if	(ie_tempo_w ='M') then
						qt_dose_ml_w	:= qt_dose_ml_w * 60;
					end if;
				end if;
		
				/*Transformar em gotas/minuto */
			
				if	(upper(ie_unidade_vel_p) = 'GTM') then
					qt_dose_ml_w	:= qt_dose_ml_w * 20;
					if	(ie_tempo_w ='H') then
						qt_dose_ml_w	:= dividir(qt_dose_ml_w,60);
					end if;
				end if;	
		
				/*Transformar em microgotas/minuto */
				if	(upper(ie_unidade_vel_p) = 'MGM') then
					qt_dose_ml_w	:= qt_dose_ml_w * 60;
					if	(ie_tempo_w ='H') then
						qt_dose_ml_w	:= dividir(qt_dose_ml_w,60);
					end if;
				end if;
				qt_vel_infusao_p	:= qt_dose_ml_w;
				end;
			else
				qt_vel_infusao_p	:= qt_dose_ml_w;
			end if;
	
			end;
		else
			begin
			qt_velocidade_w		:= qt_vel_infusao_p;
			if	(upper(ie_unidade_vel_p) = 'GTM') then
				qt_velocidade_w	:= dividir(qt_vel_infusao_p,20) * 60;
			end if;
					
			qt_dose_terap_p	:= qt_concent_dose_total_w * qt_velocidade_w;
			if	(ie_peso_w = 'KG') then
				qt_dose_terap_p	:= dividir(qt_dose_terap_p,qt_peso_p);
			end if;
			if	(ie_tempo_w = 'M') then
				qt_dose_terap_p	:= dividir(qt_dose_terap_p,60);
			end if;		
			
			end;
		end if;
		end;
	end if;
	end;
else
	qt_vel_infusao_p	:= null;
end if;


END Calcular_Dose_medic_edit;
/