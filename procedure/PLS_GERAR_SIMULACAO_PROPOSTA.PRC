create or replace
procedure pls_gerar_simulacao_proposta
			(	nr_seq_proposta_p		Number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number) is

nr_seq_plano_w			Number(10);
nr_seq_plano_ww			Number(10);
nr_seq_tabela_w			Number(10);
nr_seq_tabela_ww		Number(10);
nr_seq_segurado_w		Number(10);
cd_pessoa_fisica_w		Varchar2(10);
nr_seq_titular_w		Number(10);
qt_idade_benef_w		number(10);
vl_preco_atual_w		Number(15,2);
vl_proposta_w			Number(15,2);
tx_desconto_w			Number(7,4);
nr_seq_regra_desconto_w		Number(10);
vl_inscricao_w			Number(15,2);
vl_bonificacao_w		Number(15,2);
vl_bonific_total_w		Number(15,2)	:= 0;
--qt_vidas_w			Number(5);
vl_benef_proposta_w		number(10);
nr_seq_sca_vinculo_w		number(10);
vl_tot_sca_w			number(15,2) := 0;
vl_sca_w			number(15,2) := 0;
ie_calcula_valor_sca_contr_w	Varchar2(10);
nr_contrato_w			number(10);
nr_seq_contrato_w		number(10);
nr_seq_tabela_sca_w		number(10);
vl_proposta_sca_w		number(15,2) := 0;
ie_beneficiario_titular_w	Varchar2(10);
ie_grau_parentesco_w		Varchar2(10);
dt_inicio_proposta_w		date;
qt_registros_w			number(10);
nr_seq_bonificacao_w		number(10);
vl_via_carteira_w		number(15,2);

Cursor C01 is
	select	b.nr_sequencia,
		b.cd_beneficiario,
		nvl(b.nr_seq_titular,0),
		nvl(b.nr_seq_plano, nr_seq_plano_ww),
		nvl(b.nr_seq_tabela, nr_seq_tabela_ww),
		decode(b.nr_seq_titular,null,decode(b.nr_seq_titular_contrato,null,'T','D'),'D'),
		trunc(months_between(sysdate, a.dt_nascimento) / 12)
	from	pessoa_fisica			a,
		pls_proposta_beneficiario	b
	where	a.cd_pessoa_fisica	= b.cd_beneficiario
	and	b.nr_seq_proposta	= nr_seq_proposta_p
	and	b.dt_cancelamento is null
	order by b.nr_sequencia;

Cursor C02 is
	select	nr_seq_bonificacao
	from	pls_bonificacao_vinculo
	where	nr_seq_segurado_prop	= nr_seq_segurado_w;

Cursor C03 is
	select	nr_seq_bonificacao
	from	pls_bonificacao_vinculo
	where	nr_seq_proposta	= nr_seq_proposta_p;

Cursor C04 is
	select	nr_sequencia
	from	pls_sca_vinculo
	where	nr_seq_benef_proposta = nr_seq_segurado_w;

Cursor C05 is
	select	a.nr_seq_tabela
	from	pls_sca_regra_contrato	a
	where	a.nr_seq_contrato	= nr_seq_contrato_w
	and	a.nr_seq_tabela is not null
	and	dt_inicio_proposta_w between nvl(a.dt_inicio_vigencia,dt_inicio_proposta_w) and fim_dia(nvl(a.dt_fim_vigencia,dt_inicio_proposta_w))
	and	((nvl(a.ie_geracao_valores,'B') = ie_beneficiario_titular_w) or nvl(a.ie_geracao_valores,'B') = 'B')
	and	substr(pls_obter_se_sca_incl_contr(nr_seq_segurado_w,a.nr_seq_plano),1,1) = 'S'
	and	((a.nr_seq_plano_benef	= nr_seq_plano_w) or (a.nr_seq_plano_benef is null))
	and	not exists	(	select	1
					from	pls_sca_vinculo		x
					where	x.nr_seq_benef_proposta	= nr_seq_segurado_w
					and	a.nr_seq_plano		= x.nr_seq_plano);

Cursor C06 is
	select	nvl(vl_preco_atual,0)
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_sca_w
	and	qt_idade_benef_w between qt_idade_inicial and qt_idade_final
	and	nvl(ie_grau_titularidade,ie_grau_parentesco_w)	= ie_grau_parentesco_w
	order	by	nvl(ie_grau_titularidade,' ');

begin

select	nvl(max(nr_seq_plano),0),
	nvl(max(nr_seq_tabela),0)
into	nr_seq_plano_ww,
	nr_seq_tabela_ww
from	pls_plano		b,
	pls_proposta_plano	a
where	a.nr_seq_plano		= b.nr_sequencia
and	nr_seq_proposta		= nr_seq_proposta_p
and	b.ie_preco		= '1';

select	nr_seq_contrato,
	dt_inicio_proposta
into	nr_contrato_w,
	dt_inicio_proposta_w
from	pls_proposta_adesao
where	nr_sequencia	= nr_seq_proposta_p;

ie_calcula_valor_sca_contr_w	:= nvl(obter_valor_param_usuario(1232, 77, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');

if	(nr_contrato_w is not null) then
	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato	= nr_contrato_w;
end if;

--qt_vidas_w	:= pls_obter_qt_vidas_proposta(nr_seq_proposta_p);

if	(nr_seq_plano_ww > 0) then
	open C01;
	loop
	fetch C01 into
		nr_seq_segurado_w,
		cd_pessoa_fisica_w,
		nr_seq_titular_w,
		nr_seq_plano_w,
		nr_seq_tabela_w,
		ie_beneficiario_titular_w,
		qt_idade_benef_w;
	exit when C01%notfound;
		begin
		
		qt_registros_w		:= 0;
		
		ie_grau_parentesco_w	:= nvl(substr(pls_obter_garu_dependencia_seg(nr_seq_segurado_w,'P'),1,2),'X');
		vl_preco_atual_w	:= nvl(pls_obter_valores_propostas(nr_seq_segurado_w,null,'P'),0);
		vl_via_carteira_w	:= nvl(pls_obter_valores_propostas(nr_seq_segurado_w,null,'VC'),0);
		
		vl_inscricao_w		:= 0;
		
		if	(nr_seq_contrato_w is not null) then
			select	count(1)
			into	qt_registros_w
			from	pls_regra_inscricao
			where	nr_seq_contrato	= nr_seq_contrato_w;
		end if;
		
		if	(qt_registros_w = 0) then
			select	count(1)
			into	qt_registros_w
			from	pls_regra_inscricao
			where	nr_seq_proposta	= nr_seq_proposta_p;
			
			if	(qt_registros_w = 0) then
				/*Taxa de inscrição no produto*/
				vl_inscricao_w	:= nvl(pls_obter_valores_propostas(nr_seq_segurado_w,null,'IP'),0);
			else
				/*Taxa de inscrição proposta*/
				vl_inscricao_w	:= nvl(pls_obter_valores_propostas(nr_seq_segurado_w,null,'I'),0);
			end if;
		else
			/*Taxa de inscrição contrato*/
			vl_inscricao_w	:= nvl(pls_obter_valores_propostas(nr_seq_segurado_w,null,'IC'),0);
		end if;
		
		pls_obter_regra_desconto(nr_seq_segurado_w, 2, cd_estabelecimento_p, tx_desconto_w, nr_seq_regra_desconto_w);
		
		vl_bonific_total_w := 0;
		
		/*Bonificação do beneficiário*/
		open c02;
		loop
		fetch c02 into
			nr_seq_bonificacao_w;
		exit when c02%notfound;
			begin
			vl_bonific_total_w	:= nvl(vl_bonific_total_w,0) + nvl(pls_obter_valores_propostas(nr_seq_segurado_w,nr_seq_bonificacao_w,'B'),0);
			end;
		end loop;
		close c02;
		
		/*Bonificação da proposta*/
		open c03;
		loop
		fetch c03 into
			nr_seq_bonificacao_w;
		exit when c03%notfound;
			begin
			vl_bonific_total_w	:= nvl(vl_bonific_total_w,0) + nvl(pls_obter_valor_bonif_tot_prop(nr_seq_proposta_p,nr_seq_bonificacao_w,nr_seq_segurado_w),0);
			end;
		end loop;
		close c03;
		
		if	(vl_bonific_total_w < 0) then
			vl_bonific_total_w	:= vl_bonific_total_w * -1;
		end if;
		
		vl_tot_sca_w	:= 0;
		
		open C04;
		loop
		fetch C04 into
			nr_seq_sca_vinculo_w;
		exit when C04%notfound;
			begin
			select	pls_obter_valores_propostas(nr_seq_segurado_w,nr_seq_sca_vinculo_w,'S')
			into	vl_sca_w
			from	dual;
			
			vl_tot_sca_w	:= vl_tot_sca_w + vl_sca_w;
			end;
		end loop;
		close C04;
		
		if	(ie_calcula_valor_sca_contr_w = 'S') and
			(nr_seq_contrato_w is not null) then
			open C05;
			loop
			fetch C05 into
				nr_seq_tabela_sca_w;
			exit when C05%notfound;
				begin
				open c06;
				loop
				fetch c06 into
					vl_proposta_sca_w;
				exit when c06%notfound;
				end loop;
				close c06;
				
				vl_tot_sca_w	:= vl_tot_sca_w + vl_proposta_sca_w;
				end;
			end loop;
			close C05;
		end if;
		
		update	pls_proposta_beneficiario
		set	vl_mensalidade		= vl_preco_atual_w,
			vl_via_carteira		= vl_via_carteira_w,
			vl_inscricao		= vl_inscricao_w,
			nr_seq_regra_desconto	= nr_seq_regra_desconto_w,
			tx_desconto		= tx_desconto_w,
			vl_bonificacao		= vl_bonific_total_w,
			vl_sca			= vl_tot_sca_w
		where	nr_sequencia		= nr_seq_segurado_w;
		
		end;
	end loop;
	close C01;
	pls_atualiza_vl_tot_proposta(nr_seq_proposta_p,'N');
end if;

end pls_gerar_simulacao_proposta;
/