create or replace
procedure rop_gerar_itens_nf(nr_seq_nf_p			number,
			nr_item_nf_p			number,
			cd_operacao_p			number,
			cd_local_p			number,
			nm_usuario_p			varchar2,
			cd_empresa_p			number,
			cd_estabelecimento_p		number,
			nr_seq_lote_movto_p	in out	number,
			ds_erro_p		out	varchar2) is


nr_seq_lote_roupa_w		number(10);
nr_seq_roupa_w			number(10);
qt_roupa_w			number(13,4);
qt_roupa_ww			number(13,4) := 0;
ds_material_w			varchar2(255);
ds_historico_w			varchar2(4000);
ds_erro_w			varchar2(255) := '';
cd_material_w			number(6);
qt_roupa_nf_w			number(13,4);
cd_estabelecimento_w		number(5);
nr_seq_lote_movto_w		number(10);
nr_nota_fiscal_w		varchar2(255);
cd_operacao_w			number(10);

begin

nr_seq_lote_movto_w 	:= nvl(nr_seq_lote_movto_p,0);
cd_operacao_w		:= nvl(cd_operacao_p,0);


select	nr_nota_fiscal,
	cd_material,
	qt_item_estoque
into	nr_nota_fiscal_w,
	cd_material_w,
	qt_roupa_nf_w
from	nota_fiscal_item
where	nr_sequencia = nr_seq_nf_p
and	nr_item_nf = nr_item_nf_p;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nf_p;

select	substr(ds_material,1,255)
into	ds_material_w
from	material
where	cd_material = cd_material_w;

select	nvl(max(nr_sequencia),0)
into	nr_seq_lote_roupa_w
from	rop_lote_roupa
where	cd_material = cd_material_w
and	cd_estabelecimento = cd_estabelecimento_w;

if	(nr_seq_lote_roupa_w = 0) then

	ds_erro_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(281583) || cd_material_w || ' - ' || ds_material_w || '.',1,255);
else
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_roupa_w
	from	rop_roupa
	where	nr_seq_lote_roupa = nr_seq_lote_roupa_w
	and	dt_baixa is null;

	if	(nr_seq_roupa_w = 0) then

		select	rop_roupa_seq.nextval
		into	nr_seq_roupa_w
		from	dual;

		insert into rop_roupa(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_situacao,
			cd_empresa,
			nr_seq_lote_roupa,
			qt_roupa,
			nr_seq_nota,
			nr_seq_item_nf)
		values(	nr_seq_roupa_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'A',
			cd_empresa_p,
			nr_seq_lote_roupa_w,
			qt_roupa_nf_w,
			nr_seq_nf_p,
			nr_item_nf_p);

		ds_historico_w	:= 	substr(	WHEB_MENSAGEM_PCK.get_texto(281584) || qt_roupa_nf_w,1,4000);

		insert into rop_roupa_hist(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_roupa,
			dt_historico,
			ds_titulo,
			ds_historico,
			ie_tipo,
			dt_liberacao,
			nm_usuario_lib)
		values(	rop_roupa_hist_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_roupa_w,
			sysdate,
			WHEB_MENSAGEM_PCK.get_texto(281585),
			ds_historico_w,
			'S',
			sysdate,
			nm_usuario_p);
	else

		select	nvl(qt_roupa,0)
		into	qt_roupa_w
		from	rop_roupa
		where	nr_sequencia = nr_seq_roupa_w;

		qt_roupa_ww	:= qt_roupa_w + qt_roupa_nf_w;

		update	rop_roupa
		set	qt_roupa = qt_roupa_ww,
			nr_seq_nota = nr_seq_nf_p,
			nr_seq_item_nf = nr_item_nf_p
		where	nr_sequencia = nr_seq_roupa_w;

		ds_historico_w	:= 	substr(	WHEB_MENSAGEM_PCK.get_texto(281586) || chr(13) || chr(10) ||
						WHEB_MENSAGEM_PCK.get_texto(281587) || nvl(qt_roupa_w,0)	|| chr(13) || chr(10) ||
						WHEB_MENSAGEM_PCK.get_texto(281588) || qt_roupa_ww ,1,4000);

		insert into rop_roupa_hist(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_roupa,
			dt_historico,
			ds_titulo,
			ds_historico,
			ie_tipo,
			dt_liberacao,
			nm_usuario_lib
			)
		values(	rop_roupa_hist_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_roupa_w,
			sysdate,
			WHEB_MENSAGEM_PCK.get_texto(281589),
			ds_historico_w,
			'S',
			sysdate,
			nm_usuario_p);
	end if;

	if	(cd_operacao_w > 0) then

		if	(nr_seq_lote_movto_w = 0) then

			select	rop_lote_movto_seq.nextval
			into	nr_seq_lote_movto_w
			from	dual;

			insert into rop_lote_movto(
				nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_registro,
				nr_seq_operacao,
				cd_pessoa_fisica,
				nr_seq_local,
				dt_liberacao,
				ds_observacao)
			values(	nr_seq_lote_movto_w,
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				trunc(sysdate,'dd'),
				cd_operacao_p,
				obter_pessoa_fisica_usuario(nm_usuario_p,'C'),
				cd_local_p,
				null,
				WHEB_MENSAGEM_PCK.get_texto(281590) || nr_nota_fiscal_w || '.');
		end if;

		insert into rop_movto_roupa(
			nr_sequencia,
			nr_seq_lote,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_roupa,
			dt_origem,
			cd_estabelecimento,
			ie_oper_correta,
			qt_roupa)
		values(	rop_movto_roupa_seq.nextval,
			nr_seq_lote_movto_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_roupa_w,
			trunc(sysdate,'dd'),
			cd_estabelecimento_w,
			'S',
			qt_roupa_nf_w);

	end if;
end if;

ds_erro_p 		:= ds_erro_w;
nr_seq_lote_movto_p	:= nr_seq_lote_movto_w;

commit;

end rop_gerar_itens_nf;
/