create or replace
procedure sup_excluir_componente_reg_kit(
		nr_seq_kit_estoque_p	number,
		nr_sequencia_p		number,
		nr_seq_motivo_p		number,
		nm_usuario_p		varchar2) as


begin

update	kit_estoque_comp
set	ie_gerado_barras		= 'E',
	nr_seq_motivo_exclusao	= nr_seq_motivo_p,
	dt_exclusao		= sysdate,
	nm_usuario_exclusao	= nm_usuario_p
where	nr_seq_kit_estoque		= nr_seq_kit_estoque_p
and	nr_sequencia		= nr_sequencia_p;

commit;

end sup_excluir_componente_reg_kit;
/