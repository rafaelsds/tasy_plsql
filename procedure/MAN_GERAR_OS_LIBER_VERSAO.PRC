create or replace
procedure MAN_GERAR_OS_LIBER_VERSAO(nr_sequencia_p	    out	number,
									nm_usuario_p		in	varchar2) is

ds_dano_w			varchar2(4000);
ds_dano_breve_w			varchar2(80);
cd_pessoa_solicitante_w		varchar2(10);

begin

select	man_ordem_servico_seq.nextval,
	substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10)
into	nr_sequencia_p,
	cd_pessoa_solicitante_w
from	dual;

ds_dano_breve_w	:=	'Libera��o de vers�o extra';

ds_dano_w	:= 	obter_desc_expressao(781052);/*'Solicito a libera��o de uma vers�o extra � gerencia de suporte da Philips Clinical Informatics.' || chr(13) || chr(10) ||
				'Me comprometo em atualizar apenas a base de testes para valida��o de altera��es solicitadas por mim no sistema.' || chr(13) || chr(10) ||
				'Estou ciente dos riscos que colocarei a aplica��o caso a vers�o seja atualizada em base de produ��o por se tratar de uma vers�o n�o homologada pela Philips Clinical Informatics.';*/

insert	into man_ordem_servico(
	nr_sequencia,
	cd_pessoa_solicitante,
	dt_ordem_servico,
	ie_prioridade,
	ie_parado,
	ds_dano_breve,
	dt_atualizacao,
	nm_usuario,
	dt_inicio_desejado,
	dt_conclusao_desejada,
	ds_dano,
	dt_inicio_previsto,
	dt_fim_previsto,
	dt_inicio_real,
	dt_fim_real,
	ie_tipo_ordem,
	ie_status_ordem,
	nm_usuario_exec,
	cd_funcao,
	ie_forma_receb,
	nm_usuario_nrec,
	ie_origem_os,
	ie_classificacao)
values(	nr_sequencia_p,				-- nr_sequencia,
	cd_pessoa_solicitante_w,		-- cd_pessoa_solicitante,
	sysdate,				-- dt_ordem_servico,
	'M',					-- ie_prioridade,
	'N',					-- ie_parado,
	substr(ds_dano_breve_w,1,80),		-- ds_dano_breve,
	sysdate,				-- dt_atualizacao,
	nm_usuario_p,				-- nm_usuario,
	sysdate,				-- dt_inicio_desejado,
	(sysdate + 15),				-- dt_conclusao_desejada,
	ds_dano_w,				-- ds_dano,
	sysdate,				-- dt_inicio_previsto,
	(sysdate + 15),				-- dt_fim_previsto,
	sysdate,				-- dt_inicio_real,
	null,					-- dt_fim_real,
	'10',					-- ie_tipo_ordem,
	'1',					-- ie_status_ordem,
	nm_usuario_p, 				-- nm_usuario_exec,
	6001,					-- cd_funcao,
	'W',					-- ie_forma_receb,
	nm_usuario_p,				-- nm_usuario_nrec,
	'1',
	'S');


end man_gerar_os_liber_versao;
/