create or replace
procedure pls_consistir_cobertura_mat(	nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
					nr_seq_guia_mat_p		pls_guia_plano_mat.nr_sequencia%type,
					nr_seq_conta_mat_p		pls_conta_mat.nr_sequencia%type,
					nr_seq_material_p		pls_material.nr_sequencia%type,
					nr_seq_estrut_mat_p		number,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nr_seq_requisicao_mat_p		pls_requisicao_mat.nr_sequencia%type,
					nm_usuario_p			usuario.nm_usuario%type,
					ie_evento_p			varchar2 default null,
					qt_glosa_p			out number,
					ie_origem_consistencia_p	varchar2)is

/*
	ie_origem_consistencia_p - Apenas para diferenciar quando a chamada for pela nova importa��o de xml, onde
	necessitar� de alguams tratativas dioferentes devido �s tabelas diferentes utilizadas na importa��o
*/
					
ie_tipo_consistencia_w		varchar2(1);
nr_seq_plano_w			pls_guia_plano.nr_sequencia%type;
nr_seq_contrato_w		pls_segurado.nr_seq_contrato%type;
ie_regulamentacao_w		pls_plano.ie_regulamentacao%type;
ie_cobertura_w			varchar2(1) := 'N';
nr_seq_cobertura_w		pls_cobertura.nr_sequencia%type;
ie_tipo_atendimento_w		varchar(2);
ie_sexo_w			varchar(2);
ie_tipo_cobertura_w		varchar2(1)	:= 'X';
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
ie_tipo_despesa_w		varchar2(10);
nr_seq_estrut_mat_w		pls_material.nr_seq_estrut_mat%type;
nr_seq_grupo_material_w		number(10);
ie_cobertura_ww			varchar2(1) := 'N';
nr_seq_cobertura_ww		pls_cobertura.nr_sequencia%type;
ie_tipo_atendimento_ww		varchar(2);
ie_tipo_atendimento_tiss_w	varchar(2);
ie_sexo_ww			varchar(2);
ie_tipo_cobertura_ww		varchar2(1)	:= 'X';
nr_seq_estrut_regra_w		number(10);
ie_estrut_mat_w			varchar2(1);
nr_seq_rol_grupo_w		number(10);
dt_vigencia_rol_w		date;
qt_coberto_rol_w		number(10) := 0;
dt_atendimento_referencia_w	pls_conta.dt_atendimento_referencia%type := sysdate;

cursor c01 is
	select	a.ie_cobertura,
		c.nr_sequencia,
		c.ie_tipo_atendimento,
		c.ie_sexo,
		decode(c.nr_seq_plano, null, 'C', 'P'),
		a.nr_seq_grupo_material,
		a.nr_seq_estrut_mat
	from	pls_cobertura c,
		pls_tipo_cobertura b,
		pls_cobertura_mat a
	where	c.nr_seq_tipo_cobertura		= b.nr_sequencia
	and	a.nr_seq_tipo_cobertura 	= b.nr_sequencia
	and	(a.nr_seq_material		= nr_seq_material_p or a.nr_seq_material is null)
	and	nvl(a.ie_tipo_despesa, ie_tipo_despesa_w) = ie_tipo_despesa_w	
	and	((c.ie_situacao	is null) or (c.ie_situacao = 'A'))
	and 	trunc(dt_atendimento_referencia_w) between nvl(c.dt_inicio_vigencia, trunc(dt_atendimento_referencia_w)) and nvl(c.dt_fim_vigencia , trunc(dt_atendimento_referencia_w))
	and	((c.ie_tipo_atendimento	is null) or (c.ie_tipo_atendimento = 'A') or 
		(c.ie_tipo_atendimento	= nvl(ie_tipo_atendimento_tiss_w, c.ie_tipo_atendimento)))
	and	c.nr_sequencia	in	(
					select	x.nr_sequencia
					from	pls_cobertura x
					where	x.nr_seq_contrato = nr_seq_contrato_w
					and	(nr_seq_plano_contrato	is null	or nr_seq_plano_contrato = nr_seq_plano_w)
					union
					select	x.nr_sequencia
					from	pls_cobertura x
					where	x.nr_seq_plano	= nr_seq_plano_w
					and 	not exists
						(
						select 1
						from 	pls_cobertura x
						where	x.nr_seq_contrato = nr_seq_contrato_w
						and	(nr_seq_plano_contrato	is null	or nr_seq_plano_contrato = nr_seq_plano_w)
						)
					)
	order by
		ie_tipo_despesa,
		a.nr_seq_estrut_mat,
		a.nr_seq_material;
		
Cursor C02 is
	select	a.nr_seq_rol_grupo
	from	pls_rol_mat_med					a,
		pls_rol_grupo_mat				b,
		pls_rol_subgrupo				c,
		pls_rol_grupo					d,
		pls_rol_capitulo				e,
		pls_rol						f
	where	a.nr_seq_rol_grupo				= b.nr_sequencia
	and	b.nr_seq_subgrupo				= c.nr_sequencia
	and	c.nr_seq_grupo					= d.nr_sequencia
	and	d.nr_seq_capitulo				= e.nr_sequencia
	and	(a.nr_seq_material				is null or a.nr_seq_material        	= nr_seq_material_p)
	and	f.dt_inicio_vigencia				= dt_vigencia_rol_w
	and	(e.ie_situacao	is null or e.ie_situacao	= 'A')
	and	(d.ie_situacao	is null or d.ie_situacao	= 'A')
	and	(c.ie_situacao	is null or c.ie_situacao	= 'A')
	and	(b.ie_situacao	is null or b.ie_situacao	= 'A')
	order by
		a.nr_sequencia;

begin

qt_glosa_p := 0;

select	nvl(max(nr_seq_plano), 0),
	nvl(max(nr_seq_contrato), 0)
into	nr_seq_plano_w,
	nr_seq_contrato_w
from	pls_segurado
where	nr_sequencia = nr_seq_segurado_p;

if	(nvl(nr_seq_conta_mat_p,0) > 0) then
	ie_tipo_consistencia_w := 'C';

	if	(ie_origem_consistencia_p = 'I') then
	
		select	max(a.nr_seq_prestador_conv),
			max(b.dt_atendimento_conv)
		into	nr_seq_prestador_w,
			dt_atendimento_referencia_w
		from	pls_protocolo_conta_imp	a,
			pls_conta_imp 		b,
			pls_conta_mat_imp	c
		where	a.nr_sequencia		= b.nr_seq_protocolo
		and	b.nr_sequencia		= c.nr_seq_conta
		and	c.nr_sequencia		= nr_seq_conta_mat_p;
	else
	
		update pls_conta set cd_guia = cd_guia 
		where 	nr_sequencia = (select 	max(nr_seq_conta) 
								from 	pls_conta_mat 
								where 	nr_sequencia = nr_seq_conta_mat_p);
	
		select	max(a.nr_seq_prestador),
			max(b.dt_atendimento_referencia)
		into	nr_seq_prestador_w,
			dt_atendimento_referencia_w
		from	pls_protocolo_conta	a,
			pls_conta 		b,
			pls_conta_mat		c
		where	a.nr_sequencia		= b.nr_seq_protocolo
		and	b.nr_sequencia		= c.nr_seq_conta
		and	c.nr_sequencia		= nr_seq_conta_mat_p;
	end if;

elsif 	(nvl(nr_seq_guia_mat_p,0) > 0) then

	ie_tipo_consistencia_w := 'G';

	select	max(b.nr_seq_prestador),
		max(b.nr_seq_plano),
		max(b.dt_solicitacao)
	into	nr_seq_prestador_w,
		nr_seq_plano_w,
		dt_atendimento_referencia_w
	from	pls_guia_plano		b,
		pls_guia_plano_mat	a
	where	a.nr_seq_guia		= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_guia_mat_p;
elsif	(nvl(nr_seq_requisicao_mat_p,0) > 0) then

	ie_tipo_consistencia_w := 'R';

	select	max(b.nr_seq_prestador),
		max(b.nr_seq_plano),
		max(b.dt_requisicao)
	into	nr_seq_prestador_w,
		nr_seq_plano_w,
		dt_atendimento_referencia_w
	from	pls_requisicao_mat	a,
		pls_requisicao		b
	where	a.nr_seq_requisicao	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_requisicao_mat_p;
	
end if;

select	max(ie_regulamentacao)
into	ie_regulamentacao_w
from	pls_plano
where	nr_sequencia = nr_seq_plano_w;

/* Obter dados do cadastro do material */
select	nvl(max(ie_tipo_despesa), 0),
	nvl(max(nr_seq_estrut_mat), 0)
into	ie_tipo_despesa_w,
	nr_seq_estrut_mat_w
from	pls_material
where	nr_sequencia = nr_seq_material_p;

if	(ie_regulamentacao_w in ('P','A')) then

	-- Obt�m o �ltimo rol vigente observando a data do procedimento para as contas, data de solicita��o para as guias, e a data atual para a requisi��o.
	select	nvl(max(dt_inicio_vigencia),sysdate)
	into	dt_vigencia_rol_w
	from	pls_rol
	where	dt_inicio_vigencia <= sysdate;

	open C02;
	loop
	fetch C02 into
		nr_seq_rol_grupo_w;
	exit when C02%notfound;
		begin
		nr_seq_cobertura_w      := nr_seq_rol_grupo_w;
		ie_tipo_cobertura_w     := 'R';
		qt_coberto_rol_w	:= 1;
		exit;
		end;
	end loop;
	close C02;
end if;
	
if	((ie_regulamentacao_w in ('P','A')) and (qt_coberto_rol_w = 0)) or
	(ie_regulamentacao_w = 'R') then
	
	if	(ie_tipo_consistencia_w = 'G') then
		/* Felipe - OS 293340 - 04/03/2011 - Estavam encaixando na regra de cobertura mesmo quando a regra era do tipo Hospitalar, mas a guia era Ambulatorial */
		select	pls_obter_internado_guia(nr_seq_guia_mat_p,'AM')
		into	ie_tipo_atendimento_tiss_w
		from	dual;
	elsif	(ie_tipo_consistencia_w = 'R') then
		select	pls_obter_internado_guia(nr_seq_requisicao_mat_p,'RM')
		into	ie_tipo_atendimento_tiss_w
		from	dual;
	end if;
	
	if	(ie_tipo_atendimento_tiss_w	= 'A') then
		ie_tipo_atendimento_tiss_w	:= 'M';
	elsif
		(ie_tipo_atendimento_tiss_w	= 'I') then
		ie_tipo_atendimento_tiss_w	:= 'H';
	end if;
	
	open c01;
	loop
	fetch c01 into
		ie_cobertura_ww,
		nr_seq_cobertura_ww,
		ie_tipo_atendimento_ww,
		ie_sexo_ww,
		ie_tipo_cobertura_ww,
		nr_seq_grupo_material_w,
		nr_seq_estrut_regra_w;
	exit when c01%notfound;
	
		ie_estrut_mat_w		:= 'S';
	
		if	(nr_seq_estrut_regra_w is not null) then
			if	(pls_obter_se_mat_estrutura(nr_seq_material_p, nr_seq_estrut_regra_w) = 'N') then
				ie_estrut_mat_w	:= 'N';
			end if;
		end if;
		
		if	(ie_estrut_mat_w = 'S') then
			if	(nr_seq_grupo_material_w is not null) then
				if	(pls_se_grupo_preco_material(nr_seq_grupo_material_w, nr_seq_material_p) = 'S') then
					ie_cobertura_w		:= ie_cobertura_ww;
					nr_seq_cobertura_w	:= nr_seq_cobertura_ww;
					ie_tipo_atendimento_w	:= ie_tipo_atendimento_ww;
					ie_sexo_w		:= ie_sexo_ww;
					ie_tipo_cobertura_w	:= ie_tipo_cobertura_ww;
				else
					ie_cobertura_w		:= 'N';
					nr_seq_cobertura_w	:= nr_seq_cobertura_ww;
					ie_tipo_atendimento_w	:= ie_tipo_atendimento_ww;
					ie_sexo_w		:= ie_sexo_ww;
					ie_tipo_cobertura_w	:= ie_tipo_cobertura_ww;
				end if;
			else
				ie_cobertura_w		:= ie_cobertura_ww;
				nr_seq_cobertura_w	:= nr_seq_cobertura_ww;
				ie_tipo_atendimento_w	:= ie_tipo_atendimento_ww;
				ie_sexo_w		:= ie_sexo_ww;
				ie_tipo_cobertura_w	:= ie_tipo_cobertura_ww;
			end if;

			/*Caso houver cobertura para o material ent�o sai do cursor*/
			if	(ie_cobertura_w = 'S') then
				exit;
			end if;
		end if;
	end loop;
	close c01;
	
	if	(ie_cobertura_w	= 'N') then

		if 	(ie_tipo_consistencia_w = 'G') then

			pls_gravar_motivo_glosa('2006', null, null,
						nr_seq_guia_mat_p, '', nm_usuario_p,
						'A', 'CG', nr_seq_prestador_w,
						null, null);

			select	count(1)
			into	qt_glosa_p
			from	pls_guia_glosa	b,
				tiss_motivo_glosa a 
			where	b.nr_seq_guia_mat = nr_seq_guia_mat_p
			and	b.nr_seq_motivo_glosa = a.nr_sequencia
			and	a.cd_motivo_tiss = '2006'
			and	rownum <= 1;
			
		--Para importa��o de xml n�o ir� inserir a glosa
		elsif	(ie_tipo_consistencia_w = 'C' and ie_origem_consistencia_p <> 'I') then
			/*
			1              |Gases Medicinais
			2              |Medicamentos
			3              |Materiais
			7              |OPM
			*/
			
			if	(ie_tipo_despesa_w in (1,2,3)) then /* Materiais */

				pls_gravar_conta_glosa(	'2006', null, null,
							nr_seq_conta_mat_p, 'N', '',
							nm_usuario_p, 'A', nvl(ie_evento_p,'CC'),
							nr_seq_prestador_w, cd_estabelecimento_p, '', null);

			elsif	(ie_tipo_despesa_w = 7) then /* OPM */

				pls_gravar_conta_glosa(	'2205', null, null,
							nr_seq_conta_mat_p, 'N', '',
							nm_usuario_p, 'A', nvl(ie_evento_p,'CC'),
							nr_seq_prestador_w, cd_estabelecimento_p, '', null);
			end if;			
		elsif	(ie_tipo_consistencia_w = 'R') then

			pls_gravar_requisicao_glosa(	'2006', null, null,
							nr_seq_requisicao_mat_p, '', nm_usuario_p,
							nr_seq_prestador_w, cd_estabelecimento_p, null,
							'');

			select	count(1)
			into	qt_glosa_p
			from	pls_requisicao_glosa	b,
				tiss_motivo_glosa a 
			where	b.nr_seq_req_mat = nr_seq_requisicao_mat_p
			and	b.nr_seq_motivo_glosa = a.nr_sequencia
			and	a.cd_motivo_tiss = '2006'
			and	rownum <= 1;
		end if;

		if	(ie_tipo_consistencia_w not in ('G','R')) then
			qt_glosa_p      := 1;
		end if;
	end if;
end if;

if	(ie_tipo_cobertura_w	<> 'X') then
	if	(ie_tipo_consistencia_w = 'G') then
		update	pls_guia_plano_mat
		set	nr_seq_cobertura	= nr_seq_cobertura_w,
			ie_tipo_cobertura	= ie_tipo_cobertura_w
		where	nr_sequencia		= nr_seq_guia_mat_p;
	elsif	(ie_tipo_consistencia_w = 'C') then
		
		if	(ie_origem_consistencia_p = 'I') then
			update	pls_conta_mat_imp
			set	nr_seq_cobertura	= nr_seq_cobertura_w,
				ie_tipo_cobertura	= ie_tipo_cobertura_w
			where	nr_sequencia		= nr_seq_conta_mat_p;
		else
			update	pls_conta_mat
			set	nr_seq_cobertura	= nr_seq_cobertura_w,
				ie_tipo_cobertura	= ie_tipo_cobertura_w
			where	nr_sequencia		= nr_seq_conta_mat_p;
		end if;
	elsif	(ie_tipo_consistencia_w = 'R') then
		update	pls_requisicao_mat
		set	nr_seq_cobertura	= nr_seq_cobertura_w,
			ie_tipo_cobertura	= ie_tipo_cobertura_w
		where	nr_sequencia		= nr_seq_requisicao_mat_p;
	end if;
end if;

--commit;

end pls_consistir_cobertura_mat;
/
