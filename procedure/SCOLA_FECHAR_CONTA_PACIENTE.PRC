create or replace
procedure scola_fechar_conta_paciente(	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					cd_perfil_p		number,
					nm_usuario_p		Varchar2,
					pr_desconto_p		number,
					cd_erro_p		out number,
					ds_erro_p		out varchar2,
					nr_titulo_p		out number,
					vl_titulo_p		out number,
					ie_tipo_conta_p		varchar2 default null) is 

qt_conta_w		number(10);
ds_erro_w		varchar2(2000);
nr_interno_conta_w	number(10,0);
nr_atendimento_w	number(10,0);
ie_status_acerto_w	number(10);
cd_erro_titulo_w	number(10);
ds_erro_titulo_w	varchar2(2000);
nr_titulo_w		number(10);
vl_saldo_titulo_w	number(15,2);

ie_fecha_atendimento_w	Varchar2(01)	:= 'S';
ie_fecha_conta_w	Varchar2(01)	:= 'S';
qt_processo_pendente_w	Number(11,0)	:= 0;
ds_inconsistencia_w	Varchar2(255)	:= 0;
ie_fechar_cta_atend_w	Varchar2(2);
IE_TITULO_RECEBER_w	varchar2(1);

cd_convenio_w		number(5,0);
cd_estabelecimento_w	number(4,0);
ie_gerar_desc_w		varchar2(1);
nr_seq_desc_w		number(10,0);
vl_conta_w		number(15,2);
ie_tipo_convenio_w	number(2);

cd_motivo_exc_conta_w	motivo_exc_conta.cd_motivo_exc_conta%type;
ds_motivo_exc_conta_w	motivo_exc_conta.ds_motivo_exc_conta%type;
ie_tipo_conta_w		varchar2(5);

cursor c01 is
select 	a.nr_interno_conta
from	conta_paciente a,
	convenio c
where	a.nr_atendimento	= nr_atendimento_w
and	a.cd_convenio_parametro	= c.cd_convenio
and	a.ie_status_acerto 	= 1
and	(((c.cd_condicao_pagamento = 2) and (ie_tipo_conta_w = 'P')) or
	 ((c.cd_condicao_pagamento <> 2) and (ie_tipo_conta_w = 'C')) or
	 (ie_tipo_conta_w = 'T'));
	
/*Lista de erros
0 - SUCESSO
1 - Campos n�o informados
	1.1 - Prescri��o n�o informada
	1.2 - Sequ�ncia da prescri��o n�o informada
2 - Conta definitiva
3 - N�o possui nenhuma conta para a prescri��o e sequ�ncia informada
4 - Erro ao gerar fechar a conta
5 - N�o foi poss�vel gerar o desconto
6 - Exame est� exclu�do da conta pela motivo:  xxxxxxxx
*/					
					
begin

cd_erro_p	:= 0;
ds_erro_p	:= 'SUCESSO';
ie_tipo_conta_w	:= nvl(ie_tipo_conta_p,'T');

if	(nr_prescricao_p is null) then
	cd_erro_p	:= 1.1;
	ds_erro_p	:= 'Prescri��o n�o informada';
elsif	(nr_seq_prescr_p is null) then
	cd_erro_p	:= 1.2;
	ds_erro_p	:= 'Sequ�ncia da prescri��o n�o informada';
end if;

if 	(cd_erro_p = 0) then

	select 	nvl(count(*),0)
	into	qt_conta_w
	from	procedimento_paciente a,
		conta_paciente b,
		convenio c
	where	a.nr_interno_conta = b.nr_interno_conta
	and	b.cd_convenio_parametro = c.cd_convenio
	and	a.nr_prescricao = nr_prescricao_p
	and	a.nr_sequencia_prescricao = nr_seq_prescr_p
	and	b.ie_status_acerto = 2
	and	(((c.cd_condicao_pagamento = 2) and (ie_tipo_conta_w = 'P')) or
		 ((c.cd_condicao_pagamento <> 2) and (ie_tipo_conta_w = 'C')) or
		 (ie_tipo_conta_w = 'T'));
	
	if	(qt_conta_w > 0) then
		cd_erro_p	:= 2;
		ds_erro_p	:= 'Conta definitiva';
	else
	
		select 	nvl(max(a.cd_motivo_exc_conta),0)
		into	cd_motivo_exc_conta_w
		from	procedimento_paciente a
		where	a.nr_prescricao = nr_prescricao_p
		and	a.nr_sequencia_prescricao = nr_seq_prescr_p
		and	a.nr_interno_conta is null
		and 	a.cd_motivo_exc_conta is not null;
	
		select 	nvl(max(b.nr_interno_conta),0),
			nvl(max(b.nr_atendimento),0)
		into	nr_interno_conta_w,
			nr_atendimento_w
		from	procedimento_paciente a,
			conta_paciente b
		where	a.nr_interno_conta = b.nr_interno_conta
		and	a.nr_prescricao = nr_prescricao_p
		and	a.nr_sequencia_prescricao = nr_seq_prescr_p
		and	b.ie_status_acerto = 1;	
		
		if	(nr_interno_conta_w = 0) then
		
			if	(cd_motivo_exc_conta_w > 0) then
				
				select 	max(ds_motivo_exc_conta)
				into	ds_motivo_exc_conta_w
				from 	motivo_exc_conta
				where 	cd_motivo_exc_conta = cd_motivo_exc_conta_w;
				
				cd_erro_p	:= 6;
				ds_erro_p	:= 'Exame est� exclu�do da conta pela motivo: ' || ds_motivo_exc_conta_w;
				
			else		
				
				cd_erro_p	:= 3;
				ds_erro_p	:= 'N�o possui nenhuma conta para a prescri��o e sequ�ncia informada';
				
			end if;
			
		else	
		
			begin
			
			open C01; /*Solicitado pelo cliente na OS674667 que seja fechado todas as contas do atendimento*/
			loop
			fetch C01 into	
				nr_interno_conta_w;
			exit when C01%notfound;
				begin		
			
				select	max(b.ie_tipo_convenio)
				into	ie_tipo_convenio_w
				from	convenio b,
					conta_paciente a
				where	a.cd_convenio_parametro	= b.cd_convenio
				and	a.nr_interno_conta	= nr_interno_conta_w;
				
				if	(ie_tipo_convenio_w <> 3) then --lhalves OS 649331, n�o fechar contas do tipo SUS.
				
					if	(nvl(pr_desconto_p,0) > 0) then
						-- Gerar desconto
						ie_gerar_desc_w:= 'S';
						
						select	conta_paciente_desconto_seq.nextval
						into	nr_seq_desc_w
						from	dual;
						
						select	max(obter_valor_conta(nr_interno_conta_w,0))
						into	vl_conta_w
						from	dual;
							
						begin
						insert into conta_paciente_desconto
							(nr_sequencia,
							nr_interno_conta,
							ie_tipo_desconto,
							dt_atualizacao,
							nm_usuario,
							vl_conta,
							pr_desconto,
							vl_desconto,
							vl_liquido,
							dt_desconto,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ie_pacote,
							ie_valor_inf)
						values (nr_seq_desc_w,
							nr_interno_conta_w, 
							1,
							sysdate,
							nm_usuario_p,
							vl_conta_w,
							pr_desconto_p,
							(vl_conta_w / 100) * pr_desconto_p,
							vl_conta_w - ((vl_conta_w / 100) * pr_desconto_p),
							sysdate,
							sysdate,
							nm_usuario_p,
							'A',
							'A');
						exception
							when others then
							cd_erro_p	:= 5;
							ds_erro_p	:= 'N�o foi poss�vel gerar o desconto.';
							ie_gerar_desc_w	:= 'N';
						end;
						
						if	(ie_gerar_desc_w = 'S') then
			
							commit;					
							Gerar_ConPaci_Desc_Item(nr_seq_desc_w,nm_usuario_p);				
							Calcular_Conpaci_Desconto(nr_seq_desc_w,'I',nm_usuario_p);
						
						else
							rollback;
						end if;
						
					end if;
					
					consiste_conta_paciente(nr_interno_conta_w,
								nr_atendimento_w,
								ie_fecha_atendimento_w,
								ie_fecha_conta_w,
								qt_processo_pendente_w,
								ds_inconsistencia_w);			
					
					update	conta_paciente
					set	ds_inconsistencia	= retirar_inconsistencia_lista(ds_inconsistencia_w,'5')
					where	nr_interno_conta	= nr_interno_conta_w;
						
					ds_inconsistencia_w		:= retirar_inconsistencia_lista(ds_inconsistencia_w,'5');		

					select OBTER_FECHAR_CTA_ATEND(nr_interno_conta_w)
					into ie_fechar_cta_atend_w
					from dual;

					if	(ie_fecha_conta_w = 'S') and (substr(ie_fechar_cta_atend_w,1,1) = 'S') then
							
						fechar_conta_paciente(	nr_interno_conta_w,
									nr_atendimento_w, 2, 
									nvl(nm_usuario_p, 'Scola'),
									ds_inconsistencia_w);
						
						select	nvl(max(ie_status_acerto),1),
							nvl(max(cd_convenio_parametro),0),
							nvl(max(cd_estabelecimento),0)
						into	ie_status_acerto_w,
							cd_convenio_w,
							cd_estabelecimento_w
						from	conta_paciente
						where	nr_interno_conta	= nr_interno_conta_w;

						if	(ie_status_acerto_w = 2) then
							gerar_etapa_fechamento_conta(nr_interno_conta_w, nm_usuario_p);
						end if;
						
						select	nvl(max(ie_titulo_receber),'P')
						into	IE_TITULO_RECEBER_w
						from 	convenio_estabelecimento
						where 	cd_convenio = cd_convenio_w
						and 	cd_estabelecimento = cd_estabelecimento_w;
						
						if	(ie_status_acerto_w = 2) and (IE_TITULO_RECEBER_w = 'C') then
						
							scola_gerar_titulo_conta(nr_interno_conta_w,
										cd_perfil_p,
										nm_usuario_p,
										cd_erro_titulo_w,
										ds_erro_titulo_w);
										
							if	(cd_erro_titulo_w is not null) then
								cd_erro_p	:= cd_erro_titulo_w;
								ds_erro_p	:= ds_erro_titulo_w;
							end if;
							
							select	max(nr_titulo),
									max(vl_saldo_titulo)
							into	nr_titulo_w,
									vl_saldo_titulo_w
							from	titulo_receber
							where	nr_interno_conta	= nr_interno_conta_w;
							
							if	(nvl(nr_titulo_w,0) > 0) then
								gerar_etapa_titulo_conta(nr_interno_conta_w, nm_usuario_p);
							end if;
							
							nr_titulo_p		:= nr_titulo_w;
							vl_saldo_titulo_w	:= vl_saldo_titulo_w;
						end if;			

					end if;
						
					commit;
				end if;
				end;
				
			end loop;
			close C01;
			
			exception
			when others then
				ds_erro_w	:= substr(sqlerrm,1,1000);
				cd_erro_p	:= 4;
				ds_erro_p	:= substr('Erro ao gerar fechar a conta: '||ds_erro_w || ' - ' || 'Conta: ' || nr_interno_conta_w,1,2000);
			end;					
		end if;		
	end if;	
end if;

end scola_fechar_conta_paciente;
/