create or replace
procedure pls_grava_hist_email_benef
			(	nr_seq_guia_p	number,
				ds_email_p		varchar2,
				nm_usuario_p	Varchar2,
				ie_tipo_hist_p	Varchar2 default 'B') is 

begin

if(ie_tipo_hist_p = 'B') then
	pls_guia_gravar_historico(nr_seq_guia_p,2,'Foi enviado um e-mail para '||ds_email_p||', atrav�s da a��o de menu "Enviar e-mail para o benefici�rio".',
		null,nm_usuario_p);
elsif (ie_tipo_hist_p = 'P') then
	pls_guia_gravar_historico(nr_seq_guia_p,2,'Foi enviado um e-mail para '||ds_email_p||', atrav�s da a��o de menu "Enviar e-mail para o prestador".',
		null,nm_usuario_p);
end if;
			
commit;

end pls_grava_hist_email_benef;
/
