create or replace
procedure	far_atualiza_entrega_real	(nr_sequencia_p		number) is

begin

update	far_rom_entrega_pedido
set	dt_entrega_real = sysdate,
	nr_seq_motivo_entrega = null
where	nr_sequencia = nr_sequencia_p;

commit;

end far_atualiza_entrega_real;
/