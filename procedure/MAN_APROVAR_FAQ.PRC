create or replace
procedure man_aprovar_faq(	nr_seq_faq_p		number,
				cd_pessoa_fisica_p	varchar2,
				nm_usuario_p		varchar2) is 

qt_com_faq_aprov_w	com_faq_aprov.nr_sequencia%type;

begin

if	(nr_seq_faq_p is not null) and
	(cd_pessoa_fisica_p is not null) then
	update	com_faq_aprov
	set	dt_aprovacao = sysdate
	where	nr_seq_faq = nr_seq_faq_p
	and	cd_pessoa_aprovacao = cd_pessoa_fisica_p;

	select	count(*)
	into	qt_com_faq_aprov_w
	from	com_faq_aprov
	where	nr_seq_faq = nr_seq_faq_p
	and	dt_aprovacao is null;

	if	(qt_com_faq_aprov_w = 0) then
		update	com_faq
		set	dt_aprovacao = sysdate
		where	nr_sequencia = nr_seq_faq_p;
	end if;

	commit;
end if;

end man_aprovar_faq;
/