Create or Replace
Procedure importa_preco_abc_farma	(cd_codigo_p		Varchar2,
				dt_vigencia_p		Date,
				nm_usuario_p		Varchar2,
				cd_barras_p		Varchar2,
				cd_controle_p		Varchar2,
				cd_laboratorio_p		Varchar2,
				cd_unidade_medida_p	Varchar2,
				ds_apresentacao_p		Varchar2,
				ds_material_abcpharma_p	Varchar2,
				ds_principio_ativo_p	Varchar2,
				ie_generico_p		Varchar2,
				ie_lista_p			Varchar2,
				ie_produto_p		Varchar2,
				ie_variacao_p		Varchar2,
				nm_laboratorio_p		Varchar2,
				nr_registro_ms_p		Varchar2,
				pr_ipi_p			Number,
				vl_fracao_venca_icmsz_p	Number,
				vl_fracao_venca_icms0_p	Number,
				vl_fracao_venca_icms12_p 	Number,
				vl_fracao_venca_icms17_p 	Number,
				vl_fracao_venca_icms18_p 	Number,
				vl_fracao_venca_icms19_p 	Number,
				vl_laboratorio_icmsz_p	Number,
				vl_laboratorio_icms0_p	Number,
				vl_laboratorio_icms12_p	Number,
				vl_laboratorio_icms17_p	Number,
				vl_laboratorio_icms18_p	Number,
				vl_laboratorio_icms19_p	Number,
				vl_maximo_venda_icmsz_p	Number,
				vl_maximo_venda_icms0_p	Number,
				vl_maximo_venda_icms12_p 	Number,
				vl_maximo_venda_icms17_p 	Number,
				vl_maximo_venda_icms18_p 	Number,
				vl_maximo_venda_icms19_p 	Number) is
												
												


nr_sequencia_w		number(10);
qt_registro_w		number(15);
dt_vig_ant_w		date;

begin

select	count(*)
into	qt_registro_w
from	W_PRECO_FARMACIA_COM
where	cd_codigo = cd_codigo_p
and	ie_tabela = 'ABC';

if (qt_registro_w = 0) then
	begin

	select 	W_PRECO_FARMACIA_COM_seq.nextval 
	into	nr_sequencia_w
	from 	dual;
	
	insert into W_PRECO_FARMACIA_COM(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tabela,
		cd_codigo,
		dt_vigencia,
		vl_preco,
		cd_barras,
		cd_controle,
		cd_laboratorio,
		cd_unidade_medida,
		ds_apresentacao,
		ds_material_abcpharma,
		ds_principio_ativo,
		ie_generico,
		ie_lista,
		ie_produto,
		ie_variacao,
		nm_laboratorio,
		nr_registro_ms,
		pr_ipi,
		vl_fracao_venca_icmsz,
		vl_fracao_venca_icms0,
		vl_fracao_venca_icms12,
		vl_fracao_venca_icms17,
		vl_fracao_venca_icms18,
		vl_fracao_venca_icms19,
		vl_laboratorio_icmsz,
		vl_laboratorio_icms0,
		vl_laboratorio_icms12,
		vl_laboratorio_icms17,
		vl_laboratorio_icms18,
		vl_laboratorio_icms19,
		vl_maximo_venda_icmsz,
		vl_maximo_venda_icms0,
		vl_maximo_venda_icms12,
		vl_maximo_venda_icms17,
		vl_maximo_venda_icms18,
		vl_maximo_venda_icms19)
	values (nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		'ABC',
		cd_codigo_p,
		dt_vigencia_p,
		0,
		cd_barras_p,
		cd_controle_p,
		cd_laboratorio_p,
		cd_unidade_medida_p,
		ds_apresentacao_p,
		ds_material_abcpharma_p,
		ds_principio_ativo_p,
		ie_generico_p,
		ie_lista_p,
		ie_produto_p,
		ie_variacao_p,
		nm_laboratorio_p,
		nr_registro_ms_p,
		pr_ipi_p,
		vl_fracao_venca_icmsz_p,
		vl_fracao_venca_icms0_p,
		vl_fracao_venca_icms12_p,
		vl_fracao_venca_icms17_p,
		vl_fracao_venca_icms18_p,
		vl_fracao_venca_icms19_p,
		vl_laboratorio_icmsz_p,
		vl_laboratorio_icms0_p,
		vl_laboratorio_icms12_p,
		vl_laboratorio_icms17_p,
		vl_laboratorio_icms18_p,
		vl_laboratorio_icms19_p,
		vl_maximo_venda_icmsz_p,
		vl_maximo_venda_icms0_p,
		vl_maximo_venda_icms12_p,
		vl_maximo_venda_icms17_p,
		vl_maximo_venda_icms18_p,
		vl_maximo_venda_icms19_p
		);	
				
	end;
elsif (qt_registro_w > 0) then
	begin

	select	dt_vigencia
	into	dt_vig_ant_w
	from	w_preco_farmacia_com
	where	cd_codigo = cd_codigo_p
	and	ie_tabela = 'ABC';

	if (dt_vig_ant_w < dt_vigencia_p) then
		begin

		update	w_preco_farmacia_com
		set	dt_vigencia	= dt_vigencia_p,
				cd_barras = cd_barras_p,
				cd_controle = cd_controle_p,
				cd_laboratorio = cd_laboratorio_p,
				cd_unidade_medida = cd_unidade_medida_p,
				ds_apresentacao = ds_apresentacao_p,
				ds_material_abcpharma = ds_material_abcpharma_p,
				ds_principio_ativo = ds_principio_ativo_p,
				ie_generico = ie_generico_p,
				ie_lista = ie_lista_p,
				ie_produto = ie_produto_p,
				ie_variacao = ie_variacao_p,
				nm_laboratorio = nm_laboratorio_p,
				nr_registro_ms = nr_registro_ms_p,
				pr_ipi = pr_ipi_p,
				vl_fracao_venca_icmsz = vl_fracao_venca_icmsz_p,
				vl_fracao_venca_icms0 = vl_fracao_venca_icms0_p,
				vl_fracao_venca_icms12 = vl_fracao_venca_icms12_p,
				vl_fracao_venca_icms17 = vl_fracao_venca_icms17_p,
				vl_fracao_venca_icms18 = vl_fracao_venca_icms18_p,
				vl_fracao_venca_icms19 = vl_fracao_venca_icms19_p,
				vl_laboratorio_icmsz = vl_laboratorio_icmsz_p,
				vl_laboratorio_icms0 = vl_laboratorio_icms0_p,
				vl_laboratorio_icms12 = vl_laboratorio_icms12_p,
				vl_laboratorio_icms17 = vl_laboratorio_icms17_p,
				vl_laboratorio_icms18 = vl_laboratorio_icms18_p,
				vl_laboratorio_icms19 = vl_laboratorio_icms19_p,
				vl_maximo_venda_icmsz = vl_maximo_venda_icmsz_p,
				vl_maximo_venda_icms0 = vl_maximo_venda_icms0_p,
				vl_maximo_venda_icms12 = vl_maximo_venda_icms12_p,
				vl_maximo_venda_icms17 = vl_maximo_venda_icms17_p,
				vl_maximo_venda_icms18 = vl_maximo_venda_icms18_p,
				vl_maximo_venda_icms19 = vl_maximo_venda_icms19_p
		where	cd_codigo	= cd_codigo_p
		and	ie_tabela	= 'ABC';
		end;
	end if;
	end;

end if;

commit;

end importa_preco_abc_farma;
/