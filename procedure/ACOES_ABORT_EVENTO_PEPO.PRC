create or replace
procedure acoes_abort_evento_pepo (nr_cirurgia_p 	in	number,
				   nr_seq_pepo_p	in	number,
				   nr_seq_evento_p	in	number,
				   dt_evento_p		in	date,
				   ds_erro_p		out	varchar2) as
				   
ie_exige_fim_equip_w	varchar2(1);
consisteEtapaEvento_w	varchar2(15);
ds_erro_w		varchar2(255) := null;
cd_perfil_w		number(5);
nm_usuario_w		varchar2(15);
cd_estabelecimento_w	number(5);
ie_consiste_w		varchar2(1);

begin

cd_perfil_w				:= wheb_usuario_pck.get_cd_perfil;
nm_usuario_w			:= wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;

select	nvl(max(ie_exige_fim_equip),'N')
into	ie_exige_fim_equip_w
from	evento_cirurgia
where	nr_sequencia = nr_seq_evento_p;

obter_param_usuario(872, 470, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, consisteEtapaEvento_w);

if	(ds_erro_w is null) and
	(consisteEtapaEvento_w = 'S') then
	consiste_evento_pepo (nr_cirurgia_p,nr_seq_pepo_p,nr_seq_evento_p,dt_evento_p,nm_usuario_w,ds_erro_w);
end if;	

if	(ds_erro_w is null) and	(ie_exige_fim_equip_w = 'S') then
	select	nvl(max('S'),'N')
	into	ie_consiste_w
	from	equipamento e, 
		equipamento_cirurgia c 
	where	e.cd_equipamento = c.cd_equipamento
	and    	nvl(ie_exige_inicio_fim,'N') = 'S'
	and 	c.dt_fim is null
	and    	((nvl(nr_seq_pepo_p,0) > 0) and (c.nr_seq_pepo = nr_seq_pepo_p)
	or	 (nvl(nr_cirurgia_p,0) > 0) and (c.nr_cirurgia = nr_cirurgia_p));
	
	if	(ie_consiste_w = 'S') then
		--Existem equipamentos com data de fim da utiliza��o n�o informada!
		ds_erro_w := wheb_mensagem_pck.get_texto(343986);
	end if;
end if;

ds_erro_p := ds_erro_w;

end acoes_abort_evento_pepo;
/
