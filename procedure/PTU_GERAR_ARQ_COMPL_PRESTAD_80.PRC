create or replace
procedure ptu_gerar_arq_compl_prestad_80(nr_seq_lote_p		number,
					cd_interface_p		varchar2,
					nm_usuario_p		varchar2) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar arquivo A450 de acordo com a vers�o PTU 8.0
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

ds_conteudo_w			varchar2(4000);
nm_arquivo_w			varchar2(255);
ds_local_w			varchar2(255) := null;
ds_erro_w			varchar2(255);
arq_texto_w			utl_file.file_type;
nr_seq_registro_w		number(8) := 0;
ds_arquivo_w			clob;
ds_hash_w			ptu_movimento_prestador.ds_hash_a450%type;

--451
cd_unimed_destino_w		varchar2(4);
cd_unimed_origem_w		varchar2(4);
dt_geracao_w			varchar2(8);

--452
cd_rede_w			varchar2(4);
nm_comercial_prod_w		varchar2(40);
ie_reg_plano_ans_w		varchar2(1);
nr_reg_plano_ans_w		varchar2(20);
ie_classif_plano_w		varchar2(1);
ie_situacao_ans_w		varchar2(1);
ie_seg_produto_w		varchar2(2);
nm_com_produto_w		varchar2(60);
ie_ordem_w			number(10);
ie_acomodacao_w			varchar2(2);
ie_abrangencia_w		varchar2(2);

-- 453
rede_atend_w			ptu_movimento_exc_plano.rede_atend%type;
nm_produto_w			ptu_movimento_exc_plano.nm_produto%type;
reg_plano_ans_w			ptu_movimento_exc_plano.reg_plano_ans%type;
id_inclu_exclu_w		ptu_movimento_exc_plano.id_inclu_exclu%type;

-- 454
nr_seq_movto_exc_plano_w	ptu_movimento_plan_rede.nr_seq_movto_exc_plano%type;
cd_uni_pre_w			ptu_movimento_plan_rede.cd_uni_pre%type;
cd_cnpj_cpf_w			ptu_movimento_plan_rede.cd_cnpj_cpf%type;
cd_gr_serv_w			ptu_movimento_plan_rede.cd_gr_serv%type;

--459
qt_total_452_w			number(10) := 0;

Cursor C01 is
	select	1 ie_ordem,
		rpad(d.cd_rede,4,' '),
		rpad(' ',40, ' '),
		decode(substr(pls_obter_dados_produto(e.nr_seq_plano,'SCPA'),1,1),null,'1','2'),
		rpad(nvl(substr(elimina_acentuacao(pls_obter_dados_produto(e.nr_seq_plano,'PA')),1,20),' '),20,' '),
		to_number(nvl(decode(substr(pls_obter_dados_produto(e.nr_seq_plano,'IC'),1,1),'I',1,'CE',2,'CA',null,null),'0')),
		to_number(nvl(decode(substr(pls_obter_dados_produto(e.nr_seq_plano,'IS'),1,1),'A',1,'S',2,'C',3,'I',2),'0')),
		lpad('0',2,'0'),
		rpad(nvl(substr(elimina_acentuacao(pls_obter_dados_produto(e.nr_seq_plano,'NF')),1,60),' '),60,' '),
		nvl(decode(pls_obter_dados_produto(e.nr_seq_plano,'IA'),'C','A','I','B','N','C'),' '),
		nvl(decode(pls_obter_dados_produto(e.nr_seq_plano,'AG'),'N','1','GE','2','E','3','GM','4','M','5'),'0')
	from	ptu_rede_ref_produto	e,
		ptu_rede_referenciada	d,
		ptu_prestador_rede_ref	c,
		ptu_prestador		b,
		ptu_movimento_prestador	a
	where	a.nr_sequencia		= b.nr_seq_movimento
	and	c.nr_seq_prestador	= b.nr_sequencia
	and	d.cd_rede		= c.cd_rede
	and	e.cd_rede		= d.cd_rede
	and	a.nr_sequencia		= nr_seq_lote_p
	and	not exists	(select	1
				from	ptu_movimento_rede_ref	x
				where	x.nr_seq_movimento	= a.nr_sequencia)
	union all
	select	2 ie_ordem,
		rpad(b.cd_rede,4,' '),
		rpad(' ',40,' '),
		nvl(b.ie_reg_plano_ans,' '),
		rpad(nvl(b.nr_reg_plano_ans,' '),20,' '),
		to_number(nvl(b.ie_classif_plano,'0')),
		to_number(nvl(b.ie_situacao_ans,'0')),
		lpad(nvl(b.ie_seg_produto,'0'),2,'0'),
		rpad(nvl(substr(elimina_acentuacao(b.nm_com_prod),1,60),' '),60,' '),
		nvl(b.ie_acomodacao,' '),
		nvl(b.ie_abrangencia,'0')
	from	ptu_movimento_rede_ref	b,
		ptu_movimento_prestador	a
	where	b.nr_seq_movimento	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_lote_p
	order by 1,
		2;
		
Cursor C02 is
	select	rpad(nvl(rede_atend,' '),4,' '),
		rpad(nvl(nm_produto,' '),60,' '),
		rpad(nvl(reg_plano_ans,' '),20,' '),
		nvl(id_inclu_exclu,' '),
		nr_sequencia
	from	ptu_movimento_exc_plano
	where	nr_seq_movimento	= nr_seq_lote_p;
	
Cursor C03 is
	select	rpad(nvl(cd_uni_pre,' '),4,' '),
		rpad(nvl(cd_cnpj_cpf,' '),15,' '),
		rpad(nvl(cd_gr_serv,' '),3,' ')
	from	ptu_movimento_plan_rede
	where	nr_seq_movto_exc_plano	= nr_seq_movto_exc_plano_w;

begin
select	ptu_obter_nome_exportacao(nr_seq_lote_p,'MCPP')
into	nm_arquivo_w
from	dual;

obter_evento_utl_file(4, null, ds_local_w, ds_erro_w);

begin
arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W');
exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;
end;

-- R451 � HEADER (OBRIGAT�RIO)
select	lpad(nvl(cd_unimed_destino,'0'),4,'0'),
	lpad(nvl(cd_unimed_origem,'0'),4,'0'),
	to_char(nvl(dt_geracao,sysdate),'yyyymmdd')
into	cd_unimed_destino_w,
	cd_unimed_origem_w,
	dt_geracao_w
from	ptu_movimento_prestador
where	nr_sequencia = nr_seq_lote_p;

nr_seq_registro_w := nr_seq_registro_w + 1;

ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '451' || cd_unimed_destino_w || cd_unimed_origem_w || dt_geracao_w || '06';
ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
utl_file.fflush(arq_texto_w);
ds_conteudo_w := null;

-- R452 � COMPLEMENTO DE DADOS � GUIA M�DICO (OBRIGAT�RIO)
open C01;
loop
fetch C01 into	
	ie_ordem_w,
	cd_rede_w,
	nm_comercial_prod_w,
	ie_reg_plano_ans_w,
	nr_reg_plano_ans_w,
	ie_classif_plano_w,
	ie_situacao_ans_w,
	ie_seg_produto_w,
	nm_com_produto_w,
	ie_acomodacao_w,
	ie_abrangencia_w;
exit when C01%notfound;
	begin
	qt_total_452_w := qt_total_452_w + 1;
	nr_seq_registro_w := nr_seq_registro_w + 1;

	ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '452' || cd_rede_w || nm_comercial_prod_w || ie_reg_plano_ans_w || nr_reg_plano_ans_w ||
				ie_classif_plano_w || ie_situacao_ans_w || ie_seg_produto_w || nm_com_produto_w || ie_acomodacao_w || ie_abrangencia_w;
	ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

	utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
	utl_file.fflush(arq_texto_w);
	ds_conteudo_w := null;
	end;
end loop;
close C01;

-- R453 � EXCE��O PARA PLANOS (Obrigat�rio quando houver exce��es)
open C02;
loop
fetch C02 into	
	rede_atend_w,
	nm_produto_w,
	reg_plano_ans_w,
	id_inclu_exclu_w,
	nr_seq_movto_exc_plano_w;
exit when C02%notfound;
	begin
	nr_seq_registro_w := nr_seq_registro_w + 1;

	ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '453' || rede_atend_w || nm_produto_w || reg_plano_ans_w || id_inclu_exclu_w;
	ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

	utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
	utl_file.fflush(arq_texto_w);
	ds_conteudo_w := null;
	
	-- R454 � INCLUS�O OU EXCLUS�O DE PRESTADORES EM PLANOS OU REDES DE ATENDIMENTOS LOCAIS (Obrigat�rio quando informado o R453)
	open C03;
	loop
	fetch C03 into	
		cd_uni_pre_w,
		cd_cnpj_cpf_w,
		cd_gr_serv_w;
	exit when C03%notfound;
		begin
		nr_seq_registro_w := nr_seq_registro_w + 1;

		ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '454' || cd_uni_pre_w || cd_cnpj_cpf_w || cd_gr_serv_w;
		ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

		utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
		utl_file.fflush(arq_texto_w);
		ds_conteudo_w := null;
		end;
	end loop;
	close C03;
	
	end;
end loop;
close C02;

-- R459 � TRAILER (OBRIGAT�RIO)
nr_seq_registro_w := nr_seq_registro_w + 1;

ds_conteudo_w	:=	lpad(nr_seq_registro_w,8,'0') || '459' || lpad(qt_total_452_w,7,'0');
ds_arquivo_w	:=	ds_arquivo_w || ds_conteudo_w;

utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
utl_file.fflush(arq_texto_w);

-- R998 � Hash (OBRIGAT�RIO)
nr_seq_registro_w	:=	nr_seq_registro_w + 1;
ds_hash_w		:=	pls_hash_ptu_pck.obter_hash_txt(ds_arquivo_w); -- Gerar HASH
ds_conteudo_w		:=	lpad(nr_seq_registro_w,8,'0') || '998' || ds_hash_w;

utl_file.put_line(arq_texto_w,ds_conteudo_w || chr(13));
utl_file.fflush(arq_texto_w);

update	ptu_movimento_prestador
set	ds_hash_a450		= ds_hash_w
where	nr_sequencia		= nr_seq_lote_p;

commit;

end ptu_gerar_arq_compl_prestad_80;
/