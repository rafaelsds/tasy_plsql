create or replace
procedure	FECHAR_RETORNO_REPASSE	(nr_seq_retorno_p		number,
						 nr_seq_grg_p		number,
						 nr_seq_ret_glosa_p	number,
						 nm_usuario_p		varchar2) is
									
begin

LIBERAR_REPASSE_ADIC(		nr_seq_retorno_p, nr_seq_grg_p, nr_seq_ret_glosa_p, nm_usuario_p);
LIBERAR_REPASSE_AMENOR(	nr_seq_retorno_p, nr_seq_grg_p, nr_seq_ret_glosa_p, nm_usuario_p);
LIBERAR_REPASSE_GLOSADO(	nr_seq_retorno_p, nr_seq_grg_p, nr_seq_ret_glosa_p, nm_usuario_p);
LIBERAR_REPASSE_PAGO(		nr_seq_retorno_p, nr_seq_grg_p, nr_seq_ret_glosa_p, nm_usuario_p);

end			FECHAR_RETORNO_REPASSE;
/