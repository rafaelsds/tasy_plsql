create or replace
procedure Dasa_cad_exame_lab_antib(	nr_origem_p			           number,
                                    nr_seq_resultado_p		     number,
                                    nr_seq_result_item_p		   number,
                                    cd_medicamento_p		       varchar2,
                                    ie_resultado_p			       varchar2,
                                    nm_usuario_p			         varchar2,
                                    qt_microorganismo_p		     varchar2,
                                    ds_obs_microorganismo_p		 varchar2,
                                    qt_mic_p			             number,
                                    ie_micro_sem_antib_p		   varchar2,
                                    ds_resultado_antib_p		   varchar2,
                                    ie_primeira_escolha_p		   varchar2,
                                    ds_microorganismo_integr_p varchar2,
                                    nr_seq_prescr_p			       number,
                                    cd_erro_p			             out number,
                                    ds_erro_p			             out varchar2
                                   ) is 
						
cd_medico_w		       varchar2(10);
nr_seq_exame_w		   varchar2(10);
nr_seq_formato_w	   number(10);
cd_microorganismo_w	 number(10);
cd_medicamento_w	   number(10);
nr_seq_result_item_w number(10);

begin
  cd_erro_p	:= 0;

  if	(ds_microorganismo_integr_p is null) then	
    cd_erro_p	:= 28;	
  else
  	select max(a.cd_microorganismo)
      into cd_microorganismo_w
      from CIH_MICROORGANISMO_INT a 
     inner join equipamento_lab b on b.cd_equipamento = a.cd_equipamento and b.ds_sigla = 'DASA'
     where a.CD_MICROORGANISMO_INTEGRACAO = ds_microorganismo_integr_p;
	
    select	max(nr_sequencia)
    into	nr_seq_result_item_w
    from	exame_lab_result_item
    where	nr_seq_resultado = nr_seq_resultado_p
    and	nr_seq_prescr = nr_seq_prescr_p;
	
    if (cd_microorganismo_w is null) then
      cd_erro_p	:= 29;
    elsif	(nr_seq_result_item_w is null) then
      cd_erro_p	:= 30;
    else 		
      select max(a.cd_medicamento)
        into cd_medicamento_w
        from CIH_MEDICAMENTO_INT a
       inner join equipamento_lab b on b.cd_equipamento = a.cd_equipamento and b.ds_sigla = 'DASA'  
       where a.CD_MEDICAMENTO_INTEGRACAO = cd_medicamento_p;		
		
      begin
        insert into exame_lab_result_antib(	nr_sequencia, 
                                      			nr_seq_resultado, 
                                            nr_seq_result_item, 
                                            cd_microorganismo, 
                                            cd_medicamento, 
                                            ie_resultado, 
                                            dt_atualizacao, 
                                            nm_usuario, 
                                            ds_obs_microorganismo, 
                                            qt_mic, 
                                            ie_micro_sem_antib, 
                                            ds_resultado_antib, 
                                            ie_primeira_escolha)
                                    values( exame_lab_result_antib_seq.NextVal,
                                            nr_seq_resultado_p,
                                            nr_seq_result_item_w, 
                                            cd_microorganismo_w,
                                            cd_medicamento_w,
                                            ie_resultado_p,
                                            sysdate,
                                            nm_usuario_p,
                                            ds_obs_microorganismo_p,
                                            qt_mic_p,
                                            ie_micro_sem_antib_p,
                                            ds_resultado_antib_p,
                                            ie_primeira_escolha_p);

      exception
      when others then
      	cd_erro_p	:= 1;
        ds_erro_p	:= substr('Erro ao registrar o antibiograma '||sqlerrm,1,2000);
      end;
    end if;
  end if;

end Dasa_cad_exame_lab_antib;
/