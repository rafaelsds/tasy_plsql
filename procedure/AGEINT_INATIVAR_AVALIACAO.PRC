create or replace
procedure Ageint_Inativar_Avaliacao(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 

begin

update 	med_avaliacao_paciente
set	ie_situacao	= 'I'
where	nr_sequencia	= nr_sequencia_p;

commit;

end Ageint_Inativar_Avaliacao;
/