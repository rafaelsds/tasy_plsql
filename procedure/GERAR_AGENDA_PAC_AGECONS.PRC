create or replace
PROCEDURE gerar_agenda_pac_agecons	(nr_seq_agenda_p		number,
				cd_agenda_dest_p		number,
				cd_procedimento_p		number,
				ie_origem_proced_p	number,
				nr_seq_proc_interno_p	number,
				ie_manter_duracao_p	varchar2,
				dt_agenda_p		date,
				cd_medico_p		varchar2,
				nm_usuario_p		varchar2,
            ie_consiste_gera_p   varchar2 default 'CG',
				nr_sequencia_p	OUT	number) is

cd_medico_agenda_w		varchar2(10);
dt_agenda_w			date;
nr_minuto_duracao_w		number(10,0);
ie_status_agenda_w		varchar2(3);
ie_classif_agenda_w		varchar2(5);
cd_pessoa_fisica_w		varchar2(10);
nm_paciente_w			varchar2(60);
dt_nascimento_pac_w		date;
qt_idade_pac_w			number(3,0);
nr_telefone_w			varchar2(80);
nm_pessoa_contato_w		varchar2(50);
ie_status_paciente_w		varchar2(3);
cd_convenio_w			number(5,0);
cd_categoria_w			varchar2(10);
cd_plano_w			varchar2(10);
cd_usuario_convenio_w		varchar2(30);
dt_validade_carteira_w		date;
nr_doc_convenio_w		varchar2(20);
cd_tipo_acomodacao_w		number(4,0);
nr_seq_sala_w			number(10,0);
cd_medico_req_w			varchar2(10);
ds_confirmacao_w			varchar2(80);
cd_setor_atendimento_w		number(5,0);
ds_observacao_w			varchar2(2000);
nr_atendimento_w			number(10,0);

dt_dia_semana_w			number(1);
ie_feriado_w			varchar2(1);
nr_seq_esp_w			number(10);
ie_hor_adic_w			varchar2(1);
ie_final_semana_w			varchar2(1);
ie_valido_w			varchar2(1);
qt_horario_w			number(10);
qt_horario_livre_w			number(10);
nr_sequencia_w			number(10,0);
nr_minuto_agenda_w		number(10,0);
ds_consistencia_w			varchar2(255);
ie_nao_consiste_hr_agenda_w	varchar2(100):=null;
qt_agendamento_conflito_hr	number(10,0):=0;
dt_agenda_w_auxiliar varchar2(255):=null;
ie_manter_atendimento_w		varchar2(1);
/*
ie_consiste_gera_p
C - S� consiste, n�o gerando o agendamento
CG - Consiste e gera
*/

BEGIN
IF	(nr_seq_agenda_p is not null) and
	(cd_agenda_dest_p is not null) and
	(cd_procedimento_p is not null) and
	(ie_origem_proced_p is not null) and
	(nm_usuario_p is not null) then

	/* obter dados agenda consulta */
	select	decode(cd_medico_p,null,a.cd_pessoa_fisica,cd_medico_p),
		decode(dt_agenda_p,null,b.dt_agenda,dt_agenda_p),
		b.nr_minuto_duracao,
		b.ie_status_agenda,
		b.ie_classif_agenda,
		b.cd_pessoa_fisica,
		substr(b.nm_paciente,1,60),
		b.dt_nascimento_pac,
		b.qt_idade_pac,
		b.nr_telefone,
		b.nm_pessoa_contato,
		b.ie_status_paciente,
		b.cd_convenio,
		b.cd_categoria,
		b.cd_plano,
		b.cd_usuario_convenio,
		b.dt_validade_carteira,
		b.nr_doc_convenio,
		b.cd_tipo_acomodacao,
		b.nr_seq_sala,
		b.cd_medico_req,
		b.ds_confirmacao,
		b.cd_setor_atendimento,
		b.ds_observacao,
		b.nr_atendimento
	into	cd_medico_agenda_w,
		dt_agenda_w,
		nr_minuto_duracao_w,
		ie_status_agenda_w,
		ie_classif_agenda_w,
		cd_pessoa_fisica_w,
		nm_paciente_w,
		dt_nascimento_pac_w,
		qt_idade_pac_w,
		nr_telefone_w,
		nm_pessoa_contato_w,
		ie_status_paciente_w,
		cd_convenio_w,
		cd_categoria_w,
		cd_plano_w,
		cd_usuario_convenio_w,
		dt_validade_carteira_w,
		nr_doc_convenio_w,
		cd_tipo_acomodacao_w,
		nr_seq_sala_w,
		cd_medico_req_w,
		ds_confirmacao_w,
		cd_setor_atendimento_w,
		ds_observacao_w,
		nr_atendimento_w
	from	agenda a,
		agenda_consulta b
	where	a.cd_agenda		= b.cd_agenda
	and	b.nr_sequencia 	= nr_seq_agenda_p;
		
	/*
	abreiter em 27/01/2010 -> OS191559 -> Se parametro ([135] -> GEST�O AGENDA CIRURGICA)  permitir nao consistir horario ao gerar agenda 
	*/			
	select	nvl(max(obter_valor_param_usuario(821, 135, obter_perfil_ativo, nm_usuario_p, obter_estab_agenda(cd_agenda_dest_p))), 'S'),
		nvl(max(obter_valor_param_usuario(821, 272, obter_perfil_ativo, nm_usuario_p, obter_estab_agenda(cd_agenda_dest_p))), 'S')
	into	ie_nao_consiste_hr_agenda_w,
		ie_manter_atendimento_w
	from	dual;

	if ie_nao_consiste_hr_agenda_w = 'N' then	
	begin
		select 	max(obter_max_horario_agepac(cd_agenda_dest_p,dt_agenda_w,'C')) --contar qtde horario conflito no horario
		into	qt_agendamento_conflito_hr
		from 	dual;
		
		if qt_agendamento_conflito_hr > 0 then
			select 	max(obter_max_horario_agepac(cd_agenda_dest_p,dt_agenda_w,'DH'))
			into	dt_agenda_w_auxiliar
			from 	dual;
		/* IMPORTANTE */
		dt_agenda_w := to_date(dt_agenda_w_auxiliar,'dd/mm/yyyy hh24:mi:ss');
		end if;
	end;
	end if;			
	
	--(-20011,dt_agenda_w_auxiliar);
	
	/* Consistir sobreposi��o de hor�rios */
	consistir_horario_agenda_exame(cd_agenda_dest_p, dt_agenda_w, nr_minuto_duracao_w, 'E', ds_consistencia_w);
	IF	(ds_consistencia_w is not null) and
		(ie_nao_consiste_hr_agenda_w = 'S') then
		Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
	END IF;

	/* obter dia semana */
	select	obter_cod_dia_semana(dt_agenda_w)
	into	dt_dia_semana_w
	from	dual;

	/* obter se feriado */
	select	decode(count(*),0,'N','S')
	into	ie_feriado_w
	from 	feriado a,
		agenda b
	where 	a.cd_estabelecimento = obter_estab_agenda(cd_agenda_dest_p)
	and	a.dt_feriado = trunc(dt_agenda_w)
	and 	b.cd_agenda = cd_agenda_dest_p;

	/* obter se hor�rio especial */
	select	nvl(max(nr_sequencia),0),
		nvl(max(ie_horario_adicional),'N')
	into	nr_seq_esp_w,
		ie_hor_adic_w
	from	agenda_horario_esp
	where	cd_agenda = cd_agenda_dest_p
	and	dt_agenda = trunc(dt_agenda_w,'dd');

	/* obter se final semana */
	IF	(dt_dia_semana_w in (7,1)) then
		ie_final_semana_w := 'S';
	else
		ie_final_semana_w := 'N';
	END IF;

	/* validar hor�rio x cadastro */
	select	decode(count(*),0,'N','S')
	into	ie_valido_w
	from	agenda_horario
	where	cd_agenda = cd_agenda_dest_p
	and	((dt_dia_semana = dt_dia_semana_w) or (dt_dia_semana = 9))
	and	((nr_seq_esp_w = 0) or (ie_hor_adic_w = 'S'))
	and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_agenda_w)))
	and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_agenda_w)))
	and 	hr_inicial < hr_final
	and	nvl(nr_minuto_intervalo,0) > 0
	and	ie_feriado_w <> 'S';

	/* validar hor�rio gerado */
	select	count(*)
	into	qt_horario_w
	from	agenda_paciente
	where	cd_agenda = cd_agenda_dest_p
	and	hr_inicio = dt_agenda_w;

	/* validar hor�rio livre */
	select	count(*)
	into	qt_horario_livre_w
	from	agenda_paciente
	where	cd_agenda = cd_agenda_dest_p
	and	hr_inicio = dt_agenda_w
	and	ie_status_agenda = 'L'
	and	obter_se_feriado(obter_estab_agenda(cd_agenda_dest_p), dt_agenda_w) = 0;

	IF	((ie_valido_w = 'S') and
		 (qt_horario_w = 0)) or
		((ie_valido_w = 'S') and
		 (qt_horario_livre_w > 0)) then
      if (nvl(ie_consiste_gera_p,'CG') = 'CG') then
         IF	('S' = 'S') or
            (ie_final_semana_w = 'N') then

            /* validar minutos duracao */
            IF	(nvl(ie_manter_duracao_p,'N') = 'N') and
               (qt_horario_livre_w > 0) then

               select	nvl(max(nr_sequencia),0)
               into	nr_sequencia_w
               from	agenda_paciente
               where	cd_agenda = cd_agenda_dest_p
               and	hr_inicio = dt_agenda_w
               and	ie_status_agenda = 'L'
               and	obter_se_feriado(obter_estab_agenda(cd_agenda_dest_p), dt_agenda_w) = 0;

               IF	(nr_sequencia_w > 0) then

                  select	nr_minuto_duracao
                  into	nr_minuto_agenda_w
                  from	agenda_paciente
                  where 	nr_sequencia = nr_sequencia_w;

               END IF;

            else

               nr_minuto_agenda_w := nr_minuto_duracao_w;

            END IF;

            /* obter sequencia */
            select	agenda_paciente_seq.NEXTVAL
            into	nr_sequencia_w
            from	dual;
            
            delete 	
            from	agenda_paciente
            where	cd_agenda = cd_agenda_dest_p
            and	dt_agenda = trunc(dt_agenda_w,'dd')
            and	hr_inicio = dt_agenda_w
            and	ie_status_agenda = 'L';
            ie_status_agenda_w	:= nvl(obter_valor_param_usuario(821, 269, obter_perfil_ativo, nm_usuario_p, obter_estab_agenda(cd_agenda_dest_p)), 'N');
            /* gerar agenda */
            insert into agenda_paciente	(
                        cd_agenda,
                        cd_pessoa_fisica,
                        dt_agenda,
                        hr_inicio,
                        nr_minuto_duracao,
                        nm_usuario,
                        dt_atualizacao,
                        cd_medico,
                        nm_pessoa_contato,
                        cd_procedimento,
                        ds_observacao,
                        cd_convenio,
                        nr_cirurgia,
                        ds_cirurgia,
                        qt_idade_paciente,
                        cd_tipo_anestesia,
                        ie_origem_proced,
                        ie_status_agenda,
                        nm_instrumentador,
                        nm_circulante,
                        ie_ortese_protese,
                        ie_cdi,
                        ie_uti,
                        ie_banco_sangue,
                        ie_serv_especial,
                        cd_motivo_cancelamento,
                        nr_sequencia,
                        ds_senha,
                        cd_turno,
                        cd_anestesista,
                        cd_pediatra,
                        nm_paciente,
                        ie_anestesia,
                        nr_atendimento,
                        ie_carater_cirurgia,
                        cd_usuario_convenio,
                        nm_usuario_orig,
                        qt_idade_mes,
                        cd_plano,
                        ie_leito,
                        nr_telefone,
                        dt_agendamento,
                        ie_equipamento,
                        ie_autorizacao,
                        vl_previsto,
                        nr_seq_age_cons,
                        cd_medico_exec,
                        ie_video,
                        nr_seq_classif_agenda,
                        ie_uc,
                        cd_procedencia,
                        cd_categoria,
                        cd_tipo_acomodacao,
                        nr_doc_convenio,
                        dt_validade_carteira,
                        dt_confirmacao,
                        nr_seq_proc_interno,
                        nr_seq_status_pac,
                        nm_usuario_confirm,
                        ie_lado,
                        ie_biopsia,
                        ie_congelacao,
                        ds_laboratorio,
                        qt_min_padrao,
                        cd_doenca_cid,
                        dt_nascimento_pac,
                        nr_seq_sala,
                        nm_medico_externo,
                        ie_tipo_atendimento,
                        ie_consulta_anestesica,
                        ie_pre_internacao,
                        ie_reserva_leito,
                        ie_tipo_anestesia,
                        dt_chegada,
                        cd_medico_req,
                        nr_seq_pq_proc,
                        qt_diaria_prev,
                        dt_chegada_fim,
                        ie_arco_c,
                        nr_seq_indicacao,
                        cd_pessoa_indicacao
                        )
                     values	(
                        cd_agenda_dest_p,
                        cd_pessoa_fisica_w,
                        trunc(dt_agenda_w,'dd'),
                        dt_agenda_w,
                        nr_minuto_agenda_w,
                        nm_usuario_p,
                        sysdate,
                        cd_medico_agenda_w,
                        nm_pessoa_contato_w,
                        cd_procedimento_p,
                        ds_observacao_w,
                        cd_convenio_w,
                        null,
                        ds_confirmacao_w,
                        qt_idade_pac_w,
                        null,
                        ie_origem_proced_p,
                        ie_status_agenda_w,
                        null,
                        null,
                        'N',
                        'N',
                        'N',
                        'N',
                        'N',
                        null,
                        nr_sequencia_w,
                        null,
                        obter_turno_horario_agenda(cd_agenda_dest_p,dt_agenda_w),
                        null,
                        null,
                        nm_paciente_w,
                        'N',
                        decode(ie_manter_atendimento_w,'S',nr_atendimento_w,null),
                        null,
                        cd_usuario_convenio_w,
                        nm_usuario_p,
                        null,
                        cd_plano_w,
                        null,
                        nr_telefone_w,
                        sysdate,
                        'N',
                        null,
                        null,
                        nr_seq_agenda_p,
                        null,
                        'N',
                        null,
                        'N',
                        null,
                        cd_categoria_w,
                        cd_tipo_acomodacao_w,
                        nr_doc_convenio_w,
                        dt_validade_carteira_w,
                        null,
                        nr_seq_proc_interno_p,
                        null,
                        nm_usuario_p,
                        null,
                        'N',
                        'N',
                        null,
                        null,
                        null,
                        dt_nascimento_pac_w,
                        null,
                        null,
                        null,
                        'N',
                        'N',
                        'N',
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        'N',
                        null,
                        null
                        );

            nr_sequencia_p := nr_sequencia_w;

         END IF;
      end if;   
	else
		Wheb_mensagem_pck.exibir_mensagem_abort(210456);
	END IF;
else
	Wheb_mensagem_pck.exibir_mensagem_abort(210457);
END IF;

COMMIT;

END gerar_agenda_pac_agecons;
/