Create or Replace
procedure atualizar_justificativa_prescr(
					nr_prescricao_p 	number, 
					ds_justificativa_p	varchar2
					) IS
qt_existe_w	number(10);					

begin

select 	count(*) 
into	qt_existe_w
from 	prescr_medica 
where 	nr_prescricao = nr_prescricao_p;

if	(qt_existe_w > 0) then
	update	prescr_medica 
	set 	ds_justificativa 	= substr(ds_justificativa_p,1,2000)  
	where 	nr_prescricao 	= nr_prescricao_p;
end if;

commit;

end atualizar_justificativa_prescr;
/