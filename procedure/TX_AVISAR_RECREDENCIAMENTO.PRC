create or replace
procedure tx_avisar_recredenciamento (nm_usuario_p	varchar2) is


nr_seq_centro_w			number(10);
dt_credenciamento_centro_w	date;
nr_seq_centro_rec_w		number(10);
ie_forma_recred_centro_w	varchar2(15);
cd_estabelecimento_centro_w	number(5);
dt_proximo_cred_centro_w	date;
ds_centro_w			varchar2(80);

nr_seq_equipe_w			number(10);
dt_credenciamento_equipe_w	date;
nr_seq_equipe_rec_w		number(10);
ie_forma_recred_equipe_w	varchar2(15);
cd_estabelecimento_equipe_w	number(5);
dt_proximo_cred_equipe_w	date;
ds_equipe_w			varchar2(80);
cd_perfil_recredenc_w		number(5);
qt_dias_ant_equipe_w		number(10);
qt_dias_ant_centro_w		number(10);
dt_proximo_base_centro_w	date;
dt_proximo_base_equipe_w	date;

cursor c01 is
	select	nr_sequencia,
		dt_credenciamento,
		ie_forma_recredenciamento,
		cd_estabelecimento,
		ds_central,
		nvl(qt_dias_antecedencia,0)
	from	tx_central_transpl
	where	dt_exclusao is null
	and	ie_forma_recredenciamento is not null;

cursor c02 is
	select	nr_sequencia,
		dt_credenciamento,
		ie_forma_recredenciamento,
		cd_estabelecimento,
		ds_equipe,
		nvl(qt_dias_antecedencia,0)
	from	tx_equipe_transpl
	where	dt_exclusao is null
	and	ie_forma_recredenciamento is not null;

begin

open c01;
loop
fetch c01 into 	nr_seq_centro_w,
		dt_credenciamento_centro_w,
		ie_forma_recred_centro_w,
		cd_estabelecimento_centro_w,
		ds_centro_w,
		qt_dias_ant_centro_w;
exit when c01%notfound;
	begin

	select 	nvl(max(cd_perfil_recredenc),0)
	into	cd_perfil_recredenc_w
	from	tx_parametros
	where	cd_estabelecimento = cd_estabelecimento_centro_w;

	select 	nvl(max(nr_sequencia),0)
	into	nr_seq_centro_rec_w
	from	tx_central_recred
	where	nr_seq_central = nr_seq_centro_w;

	if (nr_seq_centro_rec_w > 0) then
	
		select 	dt_recredenciamento
		into	dt_credenciamento_centro_w
		from	tx_central_recred
		where 	nr_sequencia = nr_seq_centro_rec_w;

	end if;

	if (dt_credenciamento_centro_w is not null) and (cd_perfil_recredenc_w > 0) then
		

		dt_proximo_cred_centro_w	:= trunc(obter_prox_data_solic(ie_forma_recred_centro_w,dt_credenciamento_centro_w,null));
		dt_proximo_base_centro_w	:= trunc(obter_prox_data_solic(ie_forma_recred_centro_w,dt_credenciamento_centro_w,null));

		if (qt_dias_ant_centro_w > 0) and (dt_proximo_base_centro_w is not null) then

			dt_proximo_base_centro_w := dt_proximo_base_centro_w - qt_dias_ant_centro_w;
		
		end if;

		if (dt_proximo_base_centro_w <= trunc(sysdate)) and (dt_proximo_cred_centro_w is not null) then
		
			insert into comunic_interna (
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao,
				cd_estab_destino
			) values (
				sysdate,
				wheb_mensagem_pck.get_texto(798664) || ': ' || ds_centro_w,
				wheb_mensagem_pck.get_texto(798696,
							'DS_CENTRO='||ds_centro_w||
							';DT_PROXIMO_CRED_CENTRO='||dt_proximo_cred_centro_w),
				nm_usuario_p,
				sysdate,
				'N',
				'',
				cd_perfil_recredenc_w||', ',
				comunic_interna_seq.nextval,
				'N',
				sysdate,
				cd_estabelecimento_centro_w);
		end if;
	end if;
	end;
end loop;
close c01;

open c02;
loop
fetch c02 into 	nr_seq_equipe_w,
		dt_credenciamento_equipe_w,
		ie_forma_recred_equipe_w,
		cd_estabelecimento_equipe_w,
		ds_equipe_w,
		qt_dias_ant_equipe_w;
exit when c02%notfound;
	begin

	select 	nvl(max(cd_perfil_recredenc),0)
	into	cd_perfil_recredenc_w
	from	tx_parametros
	where	cd_estabelecimento = cd_estabelecimento_equipe_w;

	select 	nvl(max(nr_sequencia),0)
	into	nr_seq_equipe_rec_w
	from	tx_equipe_recred
	where	nr_seq_equipe = nr_seq_equipe_w;

	if (nr_seq_equipe_rec_w > 0) then
	
		select 	dt_recredenciamento
		into	dt_credenciamento_equipe_w
		from	tx_equipe_recred
		where 	nr_sequencia = nr_seq_equipe_rec_w;

	end if;

	if (dt_credenciamento_equipe_w is not null) and (cd_perfil_recredenc_w > 0) then

		dt_proximo_cred_equipe_w	:= trunc(obter_prox_data_solic (ie_forma_recred_equipe_w,dt_credenciamento_equipe_w,null));
		dt_proximo_base_equipe_w	:= trunc(obter_prox_data_solic (ie_forma_recred_equipe_w,dt_credenciamento_equipe_w,null));

		if (qt_dias_ant_equipe_w > 0) and (dt_proximo_base_equipe_w is not null) then

			dt_proximo_base_equipe_w := dt_proximo_base_equipe_w - qt_dias_ant_equipe_w;

		end if;


		if (dt_proximo_base_equipe_w <= trunc(sysdate)) and (dt_proximo_cred_equipe_w is not null) then
		
			insert into comunic_interna (
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				ds_perfil_adicional,
				nr_sequencia,
				ie_gerencial,
				dt_liberacao,
				cd_estab_destino
			) values (
				sysdate,
				wheb_mensagem_pck.get_texto(798697) || ': ' || ds_equipe_w,
				wheb_mensagem_pck.get_texto(798698,
							'DS_EQUIPE='||ds_equipe_w||
							';DT_PROXIMO_CRED_EQUIPE='||dt_proximo_cred_equipe_w),
				nm_usuario_p,
				sysdate,
				'N',
				'',
				cd_perfil_recredenc_w||', ',
				comunic_interna_seq.nextval,
				'N',
				sysdate,
				cd_estabelecimento_equipe_w
			);

		end if;

	end if;

	end;
end loop;
close c02;

commit;

end;
/
