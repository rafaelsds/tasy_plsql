create or replace
procedure supplypoint_gerar_inventario (
				cd_local_estoque_p		number,
				cd_material_p			number,
				qt_contagem_p			number,
				cd_fornecedor_p 		varchar2,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				ds_erro_p		out 	varchar2) is

ds_erro_w			varchar2(2000);
ie_tipo_entrada_w		varchar2(1);
cd_operacao_estoque_w		number(10);
dt_mesano_referencia_w		date;
nr_movimento_w			number(10);
qt_estoque_w                	number(15,4) := 0;
qt_material_estoque_w		number(15,4);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_local_estoque_w		number(4);

begin
cd_estabelecimento_w := cd_estabelecimento_p;
dt_mesano_referencia_w := trunc(sysdate,'mm');

if	(cd_estabelecimento_w = 0) then
	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	parametros_farmacia
	where	cd_pessoa_requisicao is not null;
end if;

select	nvl(max(cd_local_estoque),0)
into	cd_local_estoque_w
from	parametros_farmacia
where	cd_estabelecimento = cd_estabelecimento_w;

if	(cd_local_estoque_w = 0) then
	goto Final;
end if;

Obter_Saldo_Estoque(cd_estabelecimento_w,cd_material_p,cd_local_estoque_w,dt_mesano_referencia_w, qt_estoque_w);

select	(qt_contagem_p - qt_estoque_w)
into	qt_material_estoque_w
from	dual;

if	(nvl(qt_material_estoque_w,0) > 0) then
	ie_tipo_entrada_w := 'E';
else
	ie_tipo_entrada_w := 'S';
	qt_material_estoque_w := (qt_material_estoque_w * -1);
end if;

if	(nvl(ie_tipo_entrada_w,'X') <> 'X') then
	select	min(cd_operacao_estoque)
	into	cd_operacao_estoque_w
	from	operacao_estoque
	where	ie_tipo_requisicao = 5
	and	ie_entrada_saida = ie_tipo_entrada_w
	and	ie_consignado	= '0'
	and	ie_situacao	= 'A';
end if;

if	(nvl(cd_operacao_estoque_w,0) <> 0) then
	begin
	select	movimento_estoque_seq.nextval
	into	nr_movimento_w
	from	dual;
	
	begin
	
	insert into movimento_estoque(
		nr_movimento_estoque,
		cd_estabelecimento,
		cd_local_estoque,
		dt_movimento_estoque,
		cd_operacao_estoque,
		cd_acao,
		cd_material,
		dt_mesano_referencia,
		qt_movimento,
		dt_atualizacao,
		nm_usuario,
		ie_origem_documento,
		ie_origem_proced,
		qt_estoque,
		dt_processo,
		cd_material_estoque,
		qt_inventario,
		ie_movto_consignado,
		ds_observacao)
	values( nr_movimento_w,
		cd_estabelecimento_w,
		cd_local_estoque_w,
		sysdate,
		cd_operacao_estoque_w,
		1,
		cd_material_p,
		dt_mesano_referencia_w,
		qt_material_estoque_w,
		sysdate,
		nm_usuario_p,
		5,
		1,
		qt_material_estoque_w,
		null,
		cd_material_p,
		qt_material_estoque_w,
		'N',
		WHEB_MENSAGEM_PCK.get_texto(278957));
	exception when others then
		ds_erro_w := substr(sqlerrm,1,2000);
	end;
	end;
end if;	

<<Final>>

ds_erro_p := ds_erro_w;

commit;

end supplypoint_gerar_inventario;
/