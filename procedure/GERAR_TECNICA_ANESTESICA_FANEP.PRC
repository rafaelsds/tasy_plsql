create or replace
procedure gerar_tecnica_anestesica_fanep (	
		nr_seq_tec_p		number,
		nm_usuario_p		varchar2,
		nr_cirurgia_p		number,
		cd_profissional_p	varchar2,
		nr_seq_pepo_p		number,
		ds_lista_tec_p		varchar2,
		ds_lista_desc_p out	varchar2) is

qt_descricao_w		number(2,0);
ds_texto_padrao_w	tecnica_anestesica.ds_texto_padrao_clob%type;
ie_registra_desc_w	varchar2(15);
ie_registra_desc_tec_w	varchar2(15);

ds_lista_w		varchar2(10000);
ds_txt_recursivo_w	long := ' ';
nr_max_loop_w		number(4,0) := 0;
tamanho_lista_w		number(10,0);
posicao_virgula_w 	number(4,0) := 0;
nr_seq_tec_w		number(10,0);

ds_aux_w				tecnica_anestesica.ds_texto_padrao_clob%type;
nr_seq_anest_desc_w		anestesia_descricao.nr_sequencia%type;


begin
Obter_Param_Usuario(872,25,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_registra_desc_w);
Obter_Param_Usuario(10026,13,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_registra_desc_tec_w);

if	(nvl(nr_seq_tec_p,0) > 0) then
	begin
	insert into cirurgia_tec_anestesica (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_tecnica,
		nr_cirurgia,
		dt_inicio,
		dt_final,
		cd_profissional,
		nr_seq_pepo,
		ie_situacao)
	select	cirurgia_tec_anestesica_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_seq_tec_p,
		nr_cirurgia_p,
		sysdate,
		sysdate,
		cd_profissional_p,
		nr_seq_pepo_p,
		'A'
	from	tecnica_anestesica
	where	nr_sequencia = nr_seq_tec_p;

	commit;

	begin
	select	count(*)
	into	qt_descricao_w
	from	anestesia_descricao
	where	nr_seq_pepo = nr_seq_pepo_p
	and	dt_liberacao is null;
	exception
	when others then
		qt_descricao_w := 0;
	end;

	if	(ie_registra_desc_tec_w = 'S') then
		begin
		qt_descricao_w := 0;
		end;
	end if;
	
	if	(qt_descricao_w = 0) and (ie_registra_desc_w = 'S') and (ds_lista_tec_p is null) then
		begin
		select	nvl(nvl(a.ds_texto_padrao_clob, a.ds_texto_padrao),' ')
		into	ds_texto_padrao_w
		from	tecnica_anestesica a
		where	nr_sequencia = nr_seq_tec_p;
		
		select 	anestesia_descricao_seq.nextval
		into 	nr_seq_anest_desc_w 
		from 	dual;
		
		insert into anestesia_descricao (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_cirurgia,
			cd_responsavel,
			ds_anestesia,
			ie_tipo_descricao,
			nr_seq_pepo,
			ie_situacao,
			nr_seq_tecnica)
		select	nr_seq_anest_desc_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_cirurgia_p,
			cd_profissional_p,
			' ',
			'P',
			nr_seq_pepo_p,
			'A',
			nr_seq_tec_p
		from	dual;
		
		if (dbms_lob.getlength(ds_texto_padrao_w) < 65528) then
			GRAVAR_VARCHAR_PARA_LONG(substr(ds_texto_padrao_w,1, 32764),
									substr(ds_texto_padrao_w,32764, 65528),	
									'anestesia_descricao',		
									'ds_anestesia',			
									'where nr_sequencia = :nr_sequencia_p ',
									'nr_sequencia_p='||nr_seq_anest_desc_w);		
		else		
			update	anestesia_descricao
			set		ds_anestesia = obter_expressao_dic_objeto(1071511)
			where nr_sequencia = nr_seq_anest_desc_w;
		end if;
	
		commit;
		end;
	end if;
	end;
--Recebeu uma lista
elsif	(ds_lista_tec_p is not null) and
	(ie_registra_desc_tec_w = 'A') then
	begin
	ds_lista_w := ds_lista_tec_p;
	
	while	(ds_lista_w is not null) and
		(nr_max_loop_w < 100) loop
		begin
		tamanho_lista_w		:= length(ds_lista_w);
		posicao_virgula_w 	:= instr(ds_lista_w, ',');
		
		nr_seq_tec_w 		:= 0;
		
		if	(posicao_virgula_w > 0) then 
			begin
			nr_seq_tec_w 	:= to_number(substr(ds_lista_w, 1, posicao_virgula_w - 1));
			ds_lista_w 	:= substr(ds_lista_w, posicao_virgula_w +1, tamanho_lista_w);
			end;
		elsif	(ds_lista_w is not null) then
			begin
			nr_seq_tec_w 	:= to_number(replace(ds_lista_w, ',', ''));
			ds_lista_w 	:= null;
			end;
		end if;
		
		if	(nvl(nr_seq_tec_w, 0) > 0) then
			begin
			gerar_tecnica_anestesica(nr_seq_tec_w, nm_usuario_p, nr_cirurgia_p, cd_profissional_p, nr_seq_pepo_p, ds_lista_tec_p, ds_aux_w);
			
			
			if	(ds_lista_desc_p is null) then
				begin
				ds_lista_desc_p := nr_seq_tec_w;
				end;
			else
				begin
				ds_lista_desc_p := ds_lista_desc_p || ',' || nr_seq_tec_w;
				end;
			end if;
			end;
		end if;
		nr_max_loop_w := nr_max_loop_w +1;
		end;		
	end loop;

	end;
end if;
commit;
end gerar_tecnica_anestesica_fanep;
/