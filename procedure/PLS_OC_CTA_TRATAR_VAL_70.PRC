create or replace
procedure pls_oc_cta_tratar_val_70(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_selecao_ocor_cta.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Validar via de acesso obrigat�ria para o procedimento
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
nr_seq_selecao_w	dbms_sql.number_table;
ds_observacao_w		dbms_sql.varchar2_table;
ie_valido_w		dbms_sql.varchar2_table;

cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.ie_via_acesso_obrigatoria ie_via_acesso_obrigatoria
	from	pls_oc_cta_val_via_acesso a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;
	
cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_sequencia%type) is
	select 	x.nr_sequencia,
		'S' ie_valido,
		'Via de acesso n�o informada ou diferente da via exigida pela operadora' ds_observacao
	from 	pls_selecao_ocor_cta x, 
		pls_conta_proc a
	where 	x.ie_valido 		= 'S'
	and 	x.nr_id_transacao 	= nr_id_transacao_pc
	and 	a.nr_sequencia 		= x.nr_seq_conta_proc
	and 	(case when dados_regra_p.ie_evento = 'IMP' then a.ie_via_acesso_imp else a.ie_via_acesso end) is null
	and 	a.ie_via_obrigatoria 	= 'S'
	and 	(case when dados_regra_p.ie_evento = 'IMP' then null else a.nr_seq_regra_via_acesso end) is null
	and 	a.ie_status in ('A', 'C', 'L', 'P', 'S', 'U');
	
begin
if	(dados_regra_p.nr_sequencia is not null)  then
	for r_C01_w in C01(dados_regra_p.nr_sequencia) loop
		if	(r_C01_w.ie_via_acesso_obrigatoria = 'S')	then
			pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);		
			begin
				nr_seq_selecao_w	:= pls_util_cta_pck.num_table_vazia_w;
				ie_valido_w		:= pls_util_cta_pck.vchr2_table_vazia_w;
				ds_observacao_w		:= pls_util_cta_pck.vchr2_table_vazia_w;
				open c02(nr_id_transacao_p);
				loop
					fetch 	c02 bulk collect 
					into	nr_seq_selecao_w,ie_valido_w,ds_observacao_w
					limit pls_util_cta_pck.qt_registro_transacao_w;
					exit when nr_seq_selecao_w.count = 0;
					
					pls_tipos_ocor_pck.gerencia_selecao_validacao(	nr_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w,
											'SEQ', ds_observacao_w, ie_valido_w, nm_usuario_p);									
				end loop; 
				close c02;			
			exception
			when others then
				if (c02%isopen) then
					close c02;
				end if;
				pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p, sqlerrm, nr_id_transacao_p, nm_usuario_p);
			end;
			pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
		end if;
	end loop;
end if;
end pls_oc_cta_tratar_val_70;
/
