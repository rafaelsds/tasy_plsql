create or replace
procedure inserir_proc_princ_autorizacao(	nr_sequencia_p		number,
						nm_usuario_p		Varchar2) is 

qt_existe_w			number(10);
cd_procedimento_principal_w	number(15);
ie_origem_proced_w		number(10);
nr_seq_proc_interno_w		number(10);

begin

if	(nvl(nr_sequencia_p, 0) > 0) then

	select	max(cd_procedimento_principal),
		max(ie_origem_proced),
		max(nr_seq_proc_interno)
	into	cd_procedimento_principal_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w
	from	autorizacao_convenio
	where	nr_sequencia = nr_sequencia_p;
	
	select	count(1)
	into	qt_existe_w
	from	procedimento_autorizado
	where	nr_sequencia_autor = nr_sequencia_p
	and	cd_procedimento = cd_procedimento_principal_w
	and	ie_origem_proced = ie_origem_proced_w;
	
	if	(qt_existe_w = 0) then
		
		insert	into procedimento_autorizado
			(nr_sequencia,
			cd_procedimento,
			ie_origem_proced,
			nr_seq_proc_interno,
			qt_autorizada,
			qt_solicitada,
			dt_atualizacao,
			nm_usuario,
			nr_sequencia_autor)
		values(	procedimento_autorizado_seq.nextval,
			cd_procedimento_principal_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			0,
			1,
			sysdate,
			nm_usuario_p,
			nr_sequencia_p);
		
	end if;

end if;


commit;

end inserir_proc_princ_autorizacao;
/