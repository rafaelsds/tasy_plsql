create or replace
procedure eis_fechamento_part_alert_wait(	ds_evento_p		varchar2,
					qt_limite_seg_p		number,
					nm_usuario_p		varchar2,
					ds_mensagem_p	out	varchar2,
					ie_status_p	out	number) is

ds_mensagem_w	varchar2(255) 	:= '';
ie_status_w	number(10,0)	:= 0;

begin
--dbms_alert.REGISTER(ds_evento_p);
--dbms_alert.WAITONE(ds_evento_p, ds_mensagem_w, ie_status_w, qt_limite_seg_p);

ds_mensagem_p	:= ds_mensagem_w;
ie_status_p	:= ie_status_w;

end eis_fechamento_part_alert_wait;
/