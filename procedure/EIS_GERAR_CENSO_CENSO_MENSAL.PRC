create or replace
procedure eis_gerar_censo_censo_mensal(	dt_parametro_p	date,
				cd_estabelecimento_p number) is

dt_atual_w	date;

begin

dt_atual_w	:= trunc(dt_parametro_p,'month');

while	(dt_atual_w <= fim_mes(dt_parametro_p))loop
	begin

	if	(dt_atual_w <= trunc(sysdate,'dd')) then
		Gerar_EIS_Ocupacao_Hospitalar(dt_atual_w, 'Tasy');
	end if;
	dt_atual_w	:= dt_atual_w + 1;
	end;
end loop;

end eis_gerar_censo_censo_mensal;
/
