create or replace
procedure pls_concluir_pend_resp_atend(nr_seq_pendencia_p	number,
				nr_seq_evento_p		number,
				nr_seq_atendimento_p	number,
				ds_resolucao_p		Varchar2,
				nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Gravar a data de resolu��o, usu�rio e o que foi feito para resolver a pend�ncia.
Tamb�m deve deixar o evento como conclu�do. Caso todos os eventos do atendimento estejam conclu�dos,
deve alterar o status do atendimento para "Aguardando conclus�o"
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_status_w	pls_atendimento_evento.ie_status%type := null;
qt_registros_w	number(10);

begin

if 	(nr_seq_pendencia_p is not null) and
	(nr_seq_evento_p is not null) and
	(ds_resolucao_p is not null) then
	
	update	pls_atend_evt_pend_resp
	set	ds_resolucao		= ds_resolucao_p,
		dt_solucao		= sysdate,
		nm_usuario_solucao	= nm_usuario_p
	where	nr_sequencia 		= nr_seq_pendencia_p;
	
	update	pls_atendimento_evento a
	set	dt_atualizacao	= sysdate,
		ie_status	= 'C'
	where	a.nr_sequencia 	= nr_seq_evento_p
	and	not exists (	select	1
				from	pls_atend_evt_pend_resp x
				where	x.nr_seq_atend_evento = a.nr_sequencia
				and	x.dt_solucao is null);
	
	/* Verifica se tem para o evento algum Agendamento externo */
	select	count(1) 
	into	qt_registros_w
	from	pls_atendimento a, pls_atendimento_evento b
	where	a.nr_sequencia = b.nr_seq_atendimento
	and	a.nr_sequencia = nr_seq_atendimento_p
	and	exists (	select	1
				from	pls_atend_evt_agenda_ext x
				where	x.nr_seq_atend_evento	= b.nr_sequencia);
	/*Se tiver algum agendamento externo muda o status do atendimento para Aguradando conclus�o se n�o muda o status para conclu�do*/
	if	(qt_registros_w > 0) then
		ie_status_w	:= 'G'; /*Aguardando conclus�o*/
	else
		ie_status_w	:= 'C'; /*C - Conclu�do*/
	end if;
	
	update	pls_atendimento a
	set	dt_conclusao	= decode(ie_status_w,'C',sysdate,dt_conclusao),
		ie_status	= ie_status_w, 
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	not exists	(	select	1
					from	pls_atendimento_evento x
					where	x.nr_seq_atendimento = a.nr_sequencia
					and	x.ie_status = 'P')
	and	a.nr_sequencia = nr_seq_atendimento_p;

	
end if;

commit;

end pls_concluir_pend_resp_atend;
/
