create or replace
procedure atualizar_tipo_audit_item(	nr_sequencia_p	Number,
			ie_tipo_item_p		Varchar2,
			ie_tipo_auditoria_p	Varchar2,
			nm_usuario_p		Varchar2) is 

begin

if	(ie_tipo_item_p = 'P') then

	update	auditoria_propaci
	set	ie_tipo_auditoria = ie_tipo_auditoria_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

else

	update	auditoria_matpaci
	set	ie_tipo_auditoria = ie_tipo_auditoria_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end atualizar_tipo_audit_item;
/