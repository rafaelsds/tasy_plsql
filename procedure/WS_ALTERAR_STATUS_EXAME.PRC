create or replace
procedure ws_alterar_status_exame        ( nr_prescricao_p		varchar2,
				   nr_seq_prescr_p		varchar2,
				   ie_status_atend_atual_p	number,
				   ie_status_atend_novo_p	number,
				   nm_usuario_p			Varchar2,
				   ds_erro_p			out varchar2) is 
				   
ds_erro_w		varchar2(4000);
ds_status_exame_w	varchar2(200);
qt_status_w		number(2);

begin
	if     (nr_prescricao_p is not null) and
	       (nr_seq_prescr_p is not null) then
	       
		select 	count(*)
		into	qt_status_w
		from 	prescr_procedimento
		where 	nr_prescricao 	= nr_prescricao_p
		and 	nr_sequencia 	= nr_seq_prescr_p
		and	ie_status_atend = ie_status_atend_atual_p;				
		
		if 	(qt_status_w > 0) then
			begin
			update 	prescr_procedimento
			set 	ie_status_atend = ie_status_atend_novo_p,
				dt_atualizacao  = sysdate,
				nm_usuario	= nm_usuario_p
			where 	nr_prescricao 	= nr_prescricao_p
			and 	nr_sequencia 	= nr_seq_prescr_p
			and	ie_status_atend = ie_status_atend_atual_p;
			
			commit;
			
			exception
				when others then
					begin
						ds_erro_w := substr(sqlerrm(SQLCODE),1,3000);
					end;
			end;
		/*else
			select 	MAX(substr(nvl(Obter_regra_exame_status(vl_dominio),ds_valor_dominio),1,255))
			into	ds_status_exame_w
			from 	valor_dominio
			where 	cd_dominio = 1030
			and	vl_dominio = ie_status_atend_atual_p;
			
			ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(277786,'NR_SEQ_PRESCR='||nr_seq_prescr_p||';NR_PRESCRICAO='||nr_prescricao_p||';IE_STATUS_ATEND_ATUAL='||ie_status_atend_atual_p||';DS_STATUS_EXAME='||ds_status_exame_w);*/
		end if;
	else
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(277789,null);
	end if;
	
ds_erro_p := ds_erro_w;

end ws_alterar_status_exame;
/