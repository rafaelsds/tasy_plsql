CREATE OR REPLACE
PROCEDURE Faturar_Conta	(nr_interno_conta_p	number,
			ie_tipo_protocolo_p	Number,	
			nm_usuario_p		Varchar2) is

nr_seq_protocolo_w		Number(10);
cd_convenio_parametro_w		Number(5);
cd_estabelecimento_w		Number(4);
dt_referencia_w			Date;
vl_parametro_w			varchar2(01);
dt_entrada_w			date;
ie_data_referencia_w		varchar2(01);
dt_ref_tipo_protocolo_w		date;
dt_ref_valida_w			date;
cd_prestador_protocolo_w	protocolo_convenio.cd_prestador_convenio%type;

BEGIN

select	max(dt_entrada)
into	dt_entrada_w
from	conta_paciente b,
	atendimento_paciente a
where	a.nr_atendimento	= b.nr_atendimento
and	b.nr_interno_conta	= nr_interno_conta_p;


select	protocolo_convenio_seq.nextval
into	nr_seq_protocolo_w
from	dual;

select	cd_convenio_parametro,
	cd_estabelecimento
into	cd_convenio_parametro_w,
	cd_estabelecimento_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

select	nvl(obter_valor_param_usuario(67, 126, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w),'S')
into	vl_parametro_w
from	dual;

select	nvl(obter_valor_param_usuario(67, 142, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_w),'N')
into	ie_data_referencia_w
from	dual;

select	max(trunc(DT_REF_VALIDA))
into	dt_ref_tipo_protocolo_w
from	conv_fechamento_tipo_prot
where	cd_convenio		= cd_convenio_parametro_w
and	ie_tipo_protocolo	= ie_tipo_protocolo_p;

Select	trunc(dt_ref_valida) 
into	dt_referencia_w
from	convenio 
where	cd_convenio = cd_convenio_parametro_w;

select	decode(ie_data_referencia_w, 'S', trunc(dt_entrada_w,'dd'), nvl(dt_ref_tipo_protocolo_w, dt_referencia_w))
into	dt_ref_valida_w
from	dual;

TISS_OBTER_COD_PREST_PROTOCOLO(cd_estabelecimento_w, null, ie_tipo_protocolo_p, cd_convenio_parametro_w, null, cd_prestador_protocolo_w, null);

if	(dt_ref_valida_w is null) and
	(ie_data_referencia_w <> 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(182017);
end if;

insert into protocolo_convenio(
	cd_convenio,
	nr_protocolo,
	ie_status_protocolo,
	dt_periodo_inicial,
	dt_periodo_final,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_protocolo,
	nr_seq_protocolo,
	dt_mesano_referencia,
	cd_estabelecimento,
	nr_seq_lote_receita,
	nr_seq_lote_repasse,
	nr_seq_lote_grat,
	cd_prestador_convenio)
values(	cd_convenio_parametro_w,
	nr_seq_protocolo_w,
	1,
	decode(ie_data_referencia_w, 'S', to_date(to_char(trunc(dt_entrada_w,'dd'),'dd/mm/yyyy')||' 00:00:01','dd/mm/yyyy hh24:mi:ss'), 			

		to_date(to_char(sysdate,'dd/mm/yyyy')||' 00:00:01','dd/mm/yyyy hh24:mi:ss')),
	decode(ie_data_referencia_w, 'S', to_date(to_char(trunc(dt_entrada_w,'dd'),'dd/mm/yyyy')||' 23:59:59','dd/mm/yyyy hh24:mi:ss'), 			

		to_date(to_char(sysdate,'dd/mm/yyyy')||' 23:59:59','dd/mm/yyyy hh24:mi:ss')),
	sysdate,
	nm_usuario_p,
	ie_tipo_protocolo_p,
	nr_seq_protocolo_w,
	dt_ref_valida_w,
	cd_estabelecimento_w,
	0,
	0,
	0,
	cd_prestador_protocolo_w);

Update	conta_paciente
set	nr_protocolo		= nr_seq_protocolo_w,
	nr_seq_protocolo	= nr_seq_protocolo_w
where	nr_interno_conta	= nr_interno_conta_p;

if	(vl_parametro_w = 'S') then
	update	protocolo_convenio
	set	ie_status_protocolo = 2,
		dt_definitivo = sysdate,
		nm_usuario_definitivo = nm_usuario_p
	where	nr_seq_protocolo = nr_seq_protocolo_w;
end if;

Gerar_Protocolo_Conv_Repasse(nr_seq_protocolo_w, nm_usuario_p,null);

commit;

END Faturar_Conta;
/
	       
