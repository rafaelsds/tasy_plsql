CREATE OR REPLACE PROCEDURE	reg_add_valid_pend_desc(
	nr_seq_pendency_p	reg_validation_pendencies.nr_sequencia%TYPE,
	ds_description_p	reg_validation_pendencies.ds_detail%TYPE,
	nm_usuario_p		reg_validation_pendencies.nm_usuario%TYPE) AS
	
BEGIN
	UPDATE	reg_validation_pendencies
	SET	nm_usuario	= nm_usuario_p,
		dt_atualizacao	= SYSDATE,
		ds_detail	= ds_description_p
	WHERE	nr_sequencia	= nr_seq_pendency_p;
	
	COMMIT;
END reg_add_valid_pend_desc;
/