create or replace
procedure GERAR_AUTORIZACAO_PLS
			(cd_estabelecimento_p	in number,
			nr_sequencia_autor_p	in number,
			nm_usuario_p		in varchar2) is

/*Autorização*/			
cd_ans_w			varchar2(20);
cd_autorizacao_w			varchar2(20);
cd_convenio_w			number(5);
cd_tipo_acomodacao_w		number(15);
cd_senha_w			varchar2(20);
ds_observacao_w			varchar2(4000);
ie_carater_int_tiss_w		varchar2(2);
ie_tipo_autorizacao_w		varchar2(2);
ie_tiss_tipo_guia_w			varchar2(2);
ds_indicacao_w			varchar2(4000);
nr_atendimento_w			number(10);
nr_seq_agenda_consulta_w		number(10);
nr_seq_agenda_w			number(10);
nr_seq_guia_plano_w		number(10);
dt_inicio_vigencia_w		date;
dt_fim_vigencia_w			date;
qt_dia_autorizado_w		number(3);
ie_tipo_internacao_w		varchar2(10);
ie_regime_internacao_w		varchar2(10);
cd_doenca_cid_w			varchar2(255);
ds_diagnostico_w			varchar2(2000);
dt_entrada_w			date;
nm_responsavel_w			varchar2(255);

cd_usuario_convenio_w		varchar2(30);
ds_plano_w			varchar2(255);
dt_validade_carteira_w		date;
nm_pessoa_fisica_w		varchar2(255);
nr_cartao_nac_sus_w		varchar2(255);

cd_cgc_prestador_w		varchar2(14);
cd_cnes_w			varchar2(20);
cd_interno_w			varchar2(255);
nm_prestador_w			varchar2(255);

cd_medico_solicitante_w		varchar2(20);
cd_cbo_saude_w			varchar2(30);
nr_crm_w			varchar2(20);
nm_medico_solicitante_w		varchar2(255);
nr_cpf_w				varchar2(11);
sg_conselho_w			varchar2(255);
uf_crm_w			varchar2(2);
cd_estab_pls_w			number(5);
nr_seq_prestador_w		number(10);
ds_indicador_acidente_w		varchar2(20);

begin

select	cd_autorizacao,
	cd_convenio,
	cd_tipo_acomodacao,
	ie_carater_int_tiss,
	ie_tipo_autorizacao,	
	ds_indicacao,
	nr_atendimento,	
	nr_seq_agenda,
	nr_seq_agenda_consulta,	
	cd_medico_solicitante,
	cd_senha,
	ds_observacao,
	dt_inicio_vigencia,
	dt_fim_vigencia,
	qt_dia_autorizado,
	nr_seq_guia_plano
into	cd_autorizacao_w,
	cd_convenio_w,
	cd_tipo_acomodacao_w,
	ie_carater_int_tiss_w,
	ie_tipo_autorizacao_w,
	ds_indicacao_w,
	nr_atendimento_w,
	nr_seq_agenda_w,
	nr_seq_agenda_consulta_w,	
	cd_medico_solicitante_w,
	cd_senha_w,
	ds_observacao_w,
	dt_inicio_vigencia_w,
	dt_fim_vigencia_w,
	qt_dia_autorizado_w,
	nr_seq_guia_plano_w
from	autorizacao_convenio
where	nr_sequencia	= nr_sequencia_autor_p;

if	(nr_seq_guia_plano_w is null) then

	select	max(cd_estab_pls)
	into	cd_estab_pls_w
	from	convenio_estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio		= cd_convenio_w;

	select	c.cd_ans
	into	cd_ans_w	
	from	pessoa_juridica c,
		convenio b,
		autorizacao_convenio a
	where	a.cd_convenio		= b.cd_convenio
	and	b.cd_cgc		= c.cd_cgc
	and	a.nr_sequencia		= nr_sequencia_autor_p;
	
	if	(ie_tipo_autorizacao_w in ('1','2')) then

		select	max(ie_tipo_internacao),
			max(ie_regime_internacao),
			max(nm_responsavel)
		into	ie_tipo_internacao_w,
			ie_regime_internacao_w,
			nm_responsavel_w
		from	tiss_dados_solicitacao_v a
		where	a.nr_sequencia_autor	= nr_sequencia_autor_p
		and	ie_origem		= 'AC'
		and	ds_versao		= '2.01.01';
	end if;

	select	max(nm_pessoa_fisica),
		max(nr_cartao_nac_sus),
		max(ds_plano),
		max(dt_validade_carteira),
		max(cd_usuario_convenio)
	into	nm_pessoa_fisica_w,
		nr_cartao_nac_sus_w,
		ds_plano_w,
		dt_validade_carteira_w,
		cd_usuario_convenio_w
	from	tiss_dados_paciente_v
	where	ds_versao		= '2.01.01'
	and	nr_sequencia_autor	= nr_sequencia_autor_p
	and	ie_origem		= 'AC';

	select	max(cd_cgc),
		max(cd_interno),
		max(nr_cpf),
		max(ds_razao_social),
		max(nm_medico_solicitante),
		max(cd_cnes),
		max(sg_conselho),
		max(nr_crm),
		substr(max(uf_crm),1,2),
		max(cd_cbo_saude)
	into	cd_cgc_prestador_w,
		cd_interno_w,
		nr_cpf_w,
		nm_prestador_w,
		nm_medico_solicitante_w,
		cd_cnes_w,
		sg_conselho_w,
		nr_crm_w,
		uf_crm_w,
		cd_cbo_saude_w
	from	tiss_dados_solicitante_v
	where	nr_sequencia_autor	= nr_sequencia_autor_p
	and	ds_versao		= '2.01.01'
	and	ie_origem		= 'AC';
	
	if	(pls_obter_versao_tiss in ('3.02.00','3.02.01','3.02.02','3.03.00','3.03.01')) then		
		sg_conselho_w	:= nvl(to_char(TISS_OBTER_CODIGO_CONSELHO(sg_conselho_w)),sg_conselho_w);
		uf_crm_w	:= nvl(to_char(TISS_OBTER_CODIGO_ESTADO(uf_crm_w)),uf_crm_w);
	end if;

	if	(nr_atendimento_w is not null) then
		select	obter_data_entrada(nr_atendimento_w)
		into	dt_entrada_w
		from	dual;
	else
		dt_entrada_w	:= sysdate;
	end if;
	
	begin
	ie_tiss_tipo_guia_w := obter_regra_guia_proc_autor(nr_sequencia_autor_p);
	exception
	when others then
		ie_tiss_tipo_guia_w := null;
	end;
	
	if	(ie_tiss_tipo_guia_w is null) then
		begin
		if	(ie_tipo_autorizacao_w = '1') then
			ie_tiss_tipo_guia_w	:= '1'; 	--Guia de solicitação internação
		elsif	(ie_tipo_autorizacao_w = '2') then
			ie_tiss_tipo_guia_w	:= '8'; 	--Guia de solicitação de prorrogação de internação
		elsif	(ie_tipo_autorizacao_w in ('3','4','5')) then
			ie_tiss_tipo_guia_w	:= '2'; 	--Guia de solicitação SP/SADT
		elsif	(ie_tipo_autorizacao_w =  '6') then
			ie_tiss_tipo_guia_w	:= '3';		--Guia de consulta
		end if;
		end;
	end if;
	
	
	select	max(a.nr_sequencia)
	into	nr_seq_prestador_w
	from	pls_prestador a
	where	a.cd_cgc		= cd_cgc_prestador_w
	and	a.cd_estabelecimento	= cd_estab_pls_w
	and	a.ie_tipo_relacao	<> 'F'
	and	exists (select 	1 
			from 	pls_prestador_medico 
			where 	nr_seq_prestador 	= a.nr_sequencia 
			and 	cd_medico		= cd_medico_solicitante_w);

	pls_gerar_guia_prestador(cd_autorizacao_w,
				cd_autorizacao_w,
				ie_tiss_tipo_guia_w,
				cd_usuario_convenio_w,
				nm_pessoa_fisica_w,
				ds_plano_w,
				dt_validade_carteira_w,
				cd_ans_w,
				ie_carater_int_tiss_w,
				sysdate,
				ds_indicacao_w,
				ie_regime_internacao_w,
				nr_sequencia_autor_p,
				ie_tipo_internacao_w,
				qt_dia_autorizado_w,
				dt_entrada_w,
				dt_fim_vigencia_w,
				cd_tipo_acomodacao_w,
				nr_cartao_nac_sus_w,
				ds_observacao_w,
				cd_senha_w,
				nm_responsavel_w,
				cd_estab_pls_w,
				nm_usuario_p,
				cd_cgc_prestador_w,
				nm_prestador_w,
				cd_cnes_w,
				nr_seq_prestador_w,
				cd_cgc_prestador_w,
				null,
				nm_medico_solicitante_w,
				uf_crm_w,
				nr_crm_w,
				sg_conselho_w,
				cd_cbo_saude_w,
				cd_medico_solicitante_w,
				nr_seq_guia_plano_w);

	if	(nr_seq_guia_plano_w is not null) then

		update	autorizacao_convenio
		set	nr_seq_guia_plano = nr_seq_guia_plano_w
		where	nr_sequencia	= nr_sequencia_autor_p;

	end if;
				
	select	max(cd_doenca_cid),
		max(ds_diagnostico)
	into	cd_doenca_cid_w,
		ds_diagnostico_w
	from	tiss_diagnostico_v
	where	ds_versao		= '2.01.01'
	and	ie_origem		= 'AC'
	and	nr_sequencia_autor 	= nr_sequencia_autor_p
	and	ie_classificacao_doenca 	= 'P';

	select	nvl(max(ds_indicador_acidente),'9') --9 - Não acidente
	into	ds_indicador_acidente_w
	from	tiss_hipotese_diagnostica_v
	where	nr_sequencia_autor 	= nr_sequencia_autor_p
	and	ds_versao		= '2.01.01'
	and	ie_origem		in ('AC','ACD')
	and	rownum			= 1;

	--if	(cd_doenca_cid_w is not null) then
		pls_diag_guia_prestador(nr_seq_guia_plano_w,
					'CID-10',
					cd_doenca_cid_w,
					ds_diagnostico_w,
					ds_indicador_acidente_w,
					nm_usuario_p);
	--end if;
end if;

commit;

end GERAR_AUTORIZACAO_PLS;
/
