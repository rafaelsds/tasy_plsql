create or replace
procedure consisite_perm_duplicar_prescr(
			nr_limite_copia_p			number,
			nr_atendimento_p			number,
			dt_validade_prescricao_p	date,
			ie_prescr_sem_diagnostico_p	varchar2,
			ie_alerta_validade_p		varchar2,
			ds_erro_p				out	varchar2,
			nm_usuario_p				varchar2,
			cd_prescritor_p				number) is

ds_erro_w	varchar2(2000);
qt_diag_w	number(10);

begin
if	(nr_limite_copia_p > 0) and
	(dt_validade_prescricao_p is not null) and
	(Sysdate > (dt_validade_prescricao_p + nr_limite_copia_p/24)) then
	begin
	
	if	(ie_alerta_validade_p = 'S') then
		begin
		-- N�o � poss�vel realizar a c�pia desta prescri��o! Prescri��o inv�lida para c�pia, localize a do dia anterior. Par�metro [446] da REP.
		wheb_mensagem_pck.exibir_mensagem_abort(68659);
		end;
	else
		begin
		-- N�o � poss�vel realizar a c�pia desta prescri��o! Prescri��o inv�lida para c�pia, localize a do dia anterior. Par�metro [446] da REP.
		ds_erro_w	:= obter_texto_dic_objeto(68659,wheb_usuario_pck.get_nr_seq_idioma,null);
		end;
	end if;
	
	end;
end if;

if	(ie_prescr_sem_diagnostico_p <> 'S') then
	begin
	if	(obter_qt_diag_atend(nr_atendimento_p) = 0) and
		((ie_prescr_sem_diagnostico_p = 'N') or
		((ie_prescr_sem_diagnostico_p = 'T') and
		(Obter_medico_resp_atend(nr_atendimento_p,'C') = cd_prescritor_p)))then
		begin
		-- N�o � permitido fazer nova prescri��o para paciente sem diagn�stico. Par�metro [90] da REP.
		wheb_mensagem_pck.exibir_mensagem_abort(68660);
		end;
	end if;
	end;
end if;


ds_erro_p := ds_erro_w;
commit;
end consisite_perm_duplicar_prescr;
/
