create or replace
procedure pls_baixar_titulos_eventos(	nr_seq_pag_item_p	number,
					nr_tit_pagar_p		number,
					nr_tit_receber_p	number,
					vl_movimento_p		number,
					nm_usuario_p		varchar2) is 
	
nr_seq_baixa_w		number(10);			
nr_seq_lote_w		number(10);
cd_moeda_w		number(5);
cd_tipo_baixa_enc_tit_w	number(5);
cd_tipo_receb_enc_tit_w	number(5);
vl_movimento_w		number(15,2);
			
			
begin
vl_movimento_w	:= vl_movimento_p;

if	(nr_tit_pagar_p is not null) then
	select	nvl(max(cd_moeda_padrao),1)
	into	cd_moeda_w
	from	parametros_contas_pagar;
	
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_baixa_w
	from	titulo_pagar_baixa
	where	nr_titulo = nr_tit_pagar_p;
	
	select	max(cd_tipo_baixa_enc_tit)
	into	cd_tipo_baixa_enc_tit_w
	from	pls_parametro_pagamento;
	
	if	(cd_tipo_baixa_enc_tit_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort( 267292, null ); /* � necess�rio cadastrar o par�metro Tipo baixa pagar encontro.' + Chr(13) + 'Cadastrar em OPS - Gest�o de Operadoras -> Par�metros da OPS -> Pagamento produ��o */
	end if;
	
	select	max(a.nr_seq_lote)
	into	nr_seq_lote_w
	from	pls_pagamento_item b,
		pls_pagamento_prestador a
	where	a.nr_sequencia = b.nr_seq_pagamento
	and	b.nr_sequencia = nr_seq_pag_item_p;
	
	if	(vl_movimento_p < 0) then
		vl_movimento_w	:= vl_movimento_w * -1;
	end if;

	insert into titulo_pagar_baixa
		(nr_titulo,
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nr_seq_pag_item,
		cd_moeda,
		cd_tipo_baixa,
		ie_acao,
		dt_baixa,
		vl_baixa,
		vl_descontos,
		vl_devolucao,
		vl_juros,
		vl_multa,
		vl_pago,
		vl_inss,
		vl_ir,
		vl_glosa,
		vl_outras_deducoes,
		vl_outros_acrescimos,
		ds_observacao)
	values	(nr_tit_pagar_p,
		nr_seq_baixa_w,
		nm_usuario_p,
		sysdate,
		nr_seq_pag_item_p,
		cd_moeda_w,
		cd_tipo_baixa_enc_tit_w,
		'I',
		sysdate,
		vl_movimento_w,
		0,
		0,
		0,
		0,
		vl_movimento_w,
		0,
		0,
		0,
		0,
		0,
		'Baixa gerada atrav�s do lote, '|| nr_seq_lote_w||', de pagamento de produ��o m�dica.');
	
	atualizar_saldo_tit_pagar(nr_tit_pagar_p,nm_usuario_p);
	Gerar_W_Tit_Pag_imposto(nr_tit_pagar_p, nm_usuario_p);
		
elsif	(nr_tit_receber_p is not null) then
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_baixa_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_tit_receber_p;
	
	select	nvl(max(cd_moeda_padrao),1)
	into	cd_moeda_w
	from	parametro_contas_receber;
	
	begin
	select	max(cd_tipo_receb_enc_tit)
	into	cd_tipo_receb_enc_tit_w
	from	pls_parametro_pagamento;	
	exception 
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort( 267294, null ); /* � necess�rio cadastrar o par�metro Tipo baixa receber encontro.' + Chr(13) + 'Cadastrar em OPS - Gest�o de Operadoras -> Par�metros da OPS -> Pagamento produ��o. */
	end;
	
	select	max(a.nr_seq_lote)
	into	nr_seq_lote_w
	from	pls_pagamento_item b,
		pls_pagamento_prestador a
	where	a.nr_sequencia = b.nr_seq_pagamento
	and	b.nr_sequencia = nr_seq_pag_item_p;
	
	if	(vl_movimento_p < 0) then
		vl_movimento_w	:= vl_movimento_w * -1;
	end if;
	
	insert into titulo_receber_liq
		(nr_titulo,
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nr_seq_pag_item,
		dt_recebimento,
		vl_recebido,
		vl_descontos,
		vl_juros,
		vl_multa,
		cd_moeda,
		cd_tipo_recebimento,
		ie_acao,
		vl_rec_maior,
		vl_glosa,
		vl_nota_credito,
		vl_outros_acrescimos,
		vl_perdas,
		ie_lib_caixa,
		ds_observacao) 
	values	(nr_tit_receber_p,
		nr_seq_baixa_w,
		nm_usuario_p,
		sysdate,
		nr_seq_pag_item_p,
		sysdate,
		vl_movimento_w,
		0,
		0,
		0,
		cd_moeda_w,
		cd_tipo_receb_enc_tit_w,
		'I',
		0,
		0,
		0,
		0,
		0,
		'S',
		'Baixa gerada atrav�s do lote, '|| nr_seq_lote_w||', de pagamento de produ��o m�dica.');
		
	atualizar_saldo_tit_rec(nr_tit_receber_p,nm_usuario_p);
end if;

/* N�o pode ter commit */

end pls_baixar_titulos_eventos;
/