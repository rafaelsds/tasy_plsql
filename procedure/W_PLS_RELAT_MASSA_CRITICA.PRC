/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure w_pls_relat_massa_critica
		(	dt_inicial_p		date,
			dt_final_p		date,
			nr_contrato_p		number,
			nr_seq_grupo_p		number,
			nm_usuario_p		varchar2) is 
			
ds_sql_benef_w 		varchar2(2000);
ds_filtro_benef_w	varchar2(2000);
var_cur_w		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;
nr_seq_segurado_w	number(10);
vl_total_w		number(15,2);

begin

if	(nr_contrato_p is not null) and
	(nr_contrato_p <> 0) then
	ds_filtro_benef_w	:= ds_filtro_benef_w|| ' and	c.nr_contrato = :nr_contrato ';
end if;

if	(nr_seq_grupo_p is not null) and
	(nr_seq_grupo_p <> 0) then
	ds_filtro_benef_w	:= ds_filtro_benef_w||	' and exists (	select	1					' ||
							'	from	pls_contrato_grupo x			' ||
							'	where	x.nr_seq_contrato = d.nr_seq_contrato	' ||
							'	and	x.nr_seq_grupo = :nr_seq_grupo) ';
end if;


ds_sql_benef_w	:= 	'	select	d.nr_sequencia nr_seq_segurado							' ||
			'	from	pls_segurado d,									' ||
			'		pls_contrato c									' ||
			'	where	d.nr_seq_contrato	= c.nr_sequencia					' ||
			'	and exists (   select 1									' ||
			'			from 	pls_diagnostico_conta	a,					' ||
			'				pls_conta e							' ||
			'			where	e.nr_seq_segurado	= d.nr_sequencia 			' ||
			'			and	a.nr_seq_conta		= e.nr_sequencia 			' ||
			'			and	e.dt_atendimento_referencia between :dt_inicial and :dt_final	' ||
			'			and	a.cd_doenca is not null						' ||
			'			and	e.ie_status = 		''F''					' ||
			'			and	rownum = 1)							' || ds_filtro_benef_w||
			'	group by d.nr_sequencia ';
			
			
var_cur_w := dbms_sql.open_cursor;
dbms_sql.parse(var_cur_w, ds_sql_benef_w, 1);

dbms_sql.bind_variable(var_cur_w, ':dt_inicial', dt_inicial_p);
dbms_sql.bind_variable(var_cur_w, ':dt_final', dt_final_p);

if	(nr_contrato_p is not null) and
	(nr_contrato_p <> 0) then
	dbms_sql.bind_variable(var_cur_w, ':nr_contrato', nr_contrato_p);
end if;

if	(nr_seq_grupo_p is not null) and
	(nr_seq_grupo_p <> 0) then
	dbms_sql.bind_variable(var_cur_w, ':nr_seq_grupo', nr_seq_grupo_p);
end if;

dbms_sql.define_column(var_cur_w, 1, nr_seq_segurado_w);
var_exec_w := dbms_sql.execute(var_cur_w);

delete	W_PLS_RELAT_RELACIONAMENTO
where	ie_tipo_relatorio	= 'MC'
and	nm_usuario		= nm_usuario_p;

loop
	var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
	exit when var_retorno_w = 0;
	
	dbms_sql.column_value(var_cur_w, 1,  nr_seq_segurado_w);
	
	select	SUM(e.VL_TOTAL)
	into	vl_total_w
	from	pls_conta		e,
		pls_segurado		d,
		pls_diagnostico_conta	a
	where	e.nr_seq_segurado	= d.nr_sequencia
	and	a.nr_seq_conta		= e.nr_sequencia
	and	e.dt_atendimento_referencia between dt_inicial_p and dt_final_p
	and	d.nr_sequencia = nr_seq_segurado_w
	and	e.ie_status = 'F'
	and	a.cd_doenca is not null;
	
	insert into w_pls_relat_relacionamento
		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			NR_SEQ_PLANO,VL_CUSTO_TOTAL,ie_tipo_relatorio)
	values	(	w_pls_relat_relacionamento_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			nr_seq_segurado_w,vl_total_w,'MC');
	
end loop;

dbms_sql.close_cursor(var_cur_w);

commit;

end w_pls_relat_massa_critica;
/
