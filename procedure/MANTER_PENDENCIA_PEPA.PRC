create or replace
procedure manter_pendencia_pepa (
				nr_seq_atend_cons_pepa_p	in ATEND_CONSULTA_PEPA.NR_SEQUENCIA%type,
				ie_tipo_item_p			in varchar2,
				nr_seq_item_prontuario_p	in PRONTUARIO_ITEM.NR_SEQUENCIA%type,
				nr_seq_template_p		in EHR_TEMPLATE.NR_SEQUENCIA%type,
				nm_tabela_p			in varchar2,
				ds_sequencia_reg_p		in varchar2,
				ie_reg_pendente_p		in varchar2,
        nr_seq_dbpanel_p  in OBJETO_SCHEMATIC.NR_SEQUENCIA%type,
        nr_seq_grupo_escalas_p in PERFIL_ESCALA_INDICE.NR_SEQUENCIA%type,
				nm_usuario_p			in varchar2) is
			
nr_sequencia_w PEPA_ITEM_PENDENTE.NR_SEQUENCIA%type;
nr_seq_grupo_tab_w objeto_schematic.nr_sequencia%type;
nr_seq_tab_selecionada_w objeto_schematic.nr_sequencia%type;

begin
	select max(nr_sequencia)
	into nr_sequencia_w
	from PEPA_ITEM_PENDENTE
	where nr_seq_atend_cons_pepa = nr_seq_atend_cons_pepa_p
	and ie_tipo_item = ie_tipo_item_p
	and ((ie_tipo_item_p = 'TEMPLATE'
	and nr_seq_template = nr_seq_template_p)
	or (nm_tabela = nm_tabela_p
	and ds_sequencia_reg = ds_sequencia_reg_p));

	if (ie_reg_pendente_p = 'S') then
		if (nr_sequencia_w is null) then

      if (nr_seq_dbpanel_p is not null) then 
        obter_metadata_selecao_pepa(nr_seq_dbpanel_p, nr_seq_grupo_tab_w, nr_seq_tab_selecionada_w);
      end if;
			insert into PEPA_ITEM_PENDENTE(	
				nr_sequencia,
				nr_seq_atend_cons_pepa,
				ie_tipo_item,
				nr_seq_item_prontuario,
				nm_tabela,
				ds_sequencia_reg,
				nr_seq_template,
        nr_seq_dbpanel,
        nr_seq_tab_selecionada,
        nr_seq_grupo_tab,
        nr_seq_grupo_escalas,
				nm_usuario,
				dt_atualizacao,
				ds_call_stack)
			values(	PEPA_ITEM_PENDENTE_seq.nextval,
				nr_seq_atend_cons_pepa_p,
				substr(ie_tipo_item_p, 0, 20),
				nr_seq_item_prontuario_p,
				substr(nm_tabela_p, 0, 50),
				substr(ds_sequencia_reg_p, 0, 150),
				nr_seq_template_p,
        nr_seq_dbpanel_p,
        nr_seq_tab_selecionada_w,
        nr_seq_grupo_tab_w,
        nr_seq_grupo_escalas_p,
				substr(nm_usuario_p, 0, 15),
				sysdate,
				substr(sys.dbms_utility.format_call_stack,1,2000));
			commit;
		end if;
	elsif (nr_sequencia_w is not null) then
		delete from PEPA_ITEM_PENDENTE where nr_sequencia = nr_sequencia_w;
		commit;
	end if;
end manter_pendencia_pepa;
/
