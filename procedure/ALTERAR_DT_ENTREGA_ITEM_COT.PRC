create or replace
procedure alterar_dt_entrega_item_cot(	nr_cot_compra_p			number,
					nr_item_cot_compra_p		number,
					nm_usuario_p			varchar2) is 

nr_item_cot_compra_w		cot_compra_item.nr_item_cot_compra%type;
dt_cot_item_w			cot_compra_item.dt_limite_entrega%type;
dt_cot_item_ant_w		cot_compra_item.dt_limite_entrega%type;

cursor c01 is
	select	dt_limite_entrega,
		nr_item_cot_compra
	from	cot_compra_item
	where	nr_cot_compra = nr_cot_compra_p
	and	nr_item_cot_compra <> nr_item_cot_compra_p;
						
begin

select	trunc(dt_limite_entrega,'dd')
into	dt_cot_item_w
from	cot_compra_item
where	nr_cot_compra = nr_cot_compra_p
and	nr_item_cot_compra = nr_item_cot_compra_p;

open c01;
loop
fetch c01 into	
	dt_cot_item_ant_w,
	nr_item_cot_compra_w;
exit when c01%notfound;
	begin
	
	update	cot_compra_item
	set	dt_limite_entrega = dt_cot_item_w
	where	nr_cot_compra = nr_cot_compra_p
	and	nr_item_cot_compra = nr_item_cot_compra_w;
	
	if	(dt_cot_item_w is not null) then
		update	cot_compra_item_entrega
		set	dt_entrega = dt_cot_item_w
		where	nr_cot_compra = nr_cot_compra_p
		and	nr_item_cot_compra = nr_item_cot_compra_w
		and	trunc(dt_entrega,'dd') = dt_cot_item_ant_w;
	end if;
	end;
end loop;
close c01;

commit;

end alterar_dt_entrega_item_cot;
/
