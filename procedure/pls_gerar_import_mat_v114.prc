create or replace
procedure pls_gerar_import_mat_v114(	ds_linha_p			varchar2,
					ie_tipo_p			varchar2,
					nm_usuario_p			varchar2,
					nr_seq_mat_unimed_p	in out 	w_pls_material_unimed.nr_sequencia%type,
					nm_material_p		in out	w_pls_material_unimed.nm_material%type,
					ie_tipo_pai_p		in out	varchar2,
					qt_linha_filha_p	in out	number,
					ds_erro_p		out	varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir os materiais na W_PLS_MATERIAL_UNIMED
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:		FEITO POR LOTE - 10a
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

cd_material_w			number(10);
cd_material_tuss_w		number(10);
cd_unidade_medida_w		varchar2(10);
cd_unidade_w			varchar2(30);
ie_situacao_w			varchar2(1);
nm_material_w			varchar2(255);
cd_cnpj_w			varchar2(14);
nm_fabricante_w			varchar2(255);
nm_importador_w			varchar2(255);
nr_registro_anvisa_w		varchar2(15);
dt_validade_anvisa_w		date;
ds_motivo_ativo_inativo_w	w_pls_material_unimed.ds_motivo_ativo_inativo%type;
vl_fabrica_w			number(15,4);
vl_max_consumidor_w		number(18,4);
dt_inicio_obrigatorio_w		date;
ds_material_w			varchar2(2000);
ds_especialidade_w		varchar2(255);
ds_classe_w			varchar2(255);
ds_apresentacao_w		varchar2(2000);
vl_fator_conversao_w		number(5);
ie_generico_w			varchar2(1);
ds_grupo_farmacologico_w	varchar2(255);
ds_classe_farmacologico_w	varchar2(255);
ds_forma_farmaceutico_w		varchar2(255);
pr_icms_w			number(7,2);
vl_pmc_w			number(15,4);
ds_principio_ativo_w		varchar2(255);
ie_origem_w			number(1);
dt_exclusao_w			date;
ie_possui_mat_w			number(10) := 0;
ie_tipo_ww			varchar2(3);			
ie_mat_med_w			varchar2(1);
qt_902_w			number(10) := 0;
qt_905_w			number(10) := 0;
qt_total_902_w			number(10) := 0;
qt_total_905_w			number(10) := 0;
cd_ref_material_fab_w		varchar2(60);
cd_anterior_material_w		number(8);
cd_anterior_medicamento_w	number(8);
vl_tcl_w			number(2);

tp_produto_w			pls_material_unimed.ie_produto%type;
tp_codificacao_w		pls_material_unimed.ie_codificacao%type;
dt_inicio_vigencia_w		pls_material_unimed.dt_inicio_vigencia%type;
dt_fim_vigencia_w		pls_material_unimed.dt_fim_vigencia%type;
dt_fim_implantacao_w		pls_material_unimed.dt_fim_implantacao%type;
vl_preco_unico_w		pls_material_unimed.vl_preco_unico%type;
vl_tc_w				pls_material_unimed.vl_tc%type;

cd_simpro_w			w_pls_mat_unimed_simpro.cd_simpro%type;
ds_produto_simpro_w		w_pls_mat_unimed_simpro.desc_prod_simpro%type;

cd_brasindice_w			w_pls_mat_unimed_bras.cd_brasindice%type;
ds_produto_w			w_pls_mat_unimed_bras.des_produto%type;
ds_apresentacao_bras_w		w_pls_mat_unimed_bras.des_apresentacao%type;
ds_unidade_medida_w		pls_material_unimed.ds_unidade_medida%type;
id_alc_w			w_pls_mat_unimed_trib.id_alc%type;
ie_produto_med_w		w_pls_material_unimed.ie_produto_med%type;
ie_confaz_w			w_pls_material_unimed.ie_confaz%type;

tp_tabela_tiss_w		w_pls_material_unimed.tp_tabela_tiss%type;
cd_material_tiss_w		w_pls_material_unimed.cd_material_tiss%type;
cd_material_tiss_ant_w		w_pls_material_unimed.cd_material_tiss_ant%type;

begin

if	(ie_tipo_p in ('902','905')) then
	cd_material_w			:= null;
	cd_unidade_medida_w		:= null;
	ie_situacao_w			:= null;
	nm_material_w			:= null;
	cd_cnpj_w			:= null;
	nm_fabricante_w			:= null;
	nm_importador_w			:= null;
	nr_registro_anvisa_w		:= null;
	dt_validade_anvisa_w		:= null;
	ds_motivo_ativo_inativo_w	:= null;
	vl_fabrica_w			:= null;
	vl_max_consumidor_w		:= null;
	dt_inicio_obrigatorio_w		:= null;
	ds_material_w			:= null;
	ds_especialidade_w		:= null;
	ds_classe_w			:= null;
	ds_apresentacao_w		:= null;
	vl_fator_conversao_w		:= null;
	ie_generico_w			:= null;
	ds_grupo_farmacologico_w	:= null;
	ds_classe_farmacologico_w	:= null;
	ds_forma_farmaceutico_w		:= null;
	pr_icms_w			:= null;
	vl_pmc_w			:= null;
	ds_principio_ativo_w		:= null;
	ie_origem_w			:= null;
	cd_ref_material_fab_w		:= null;
	cd_anterior_material_w		:= null;
	cd_anterior_medicamento_w	:= null;
	cd_material_tuss_w		:= null;
	tp_produto_w			:= null;
	tp_codificacao_w		:= null;
	dt_inicio_vigencia_w		:= null;
	dt_fim_vigencia_w		:= null;
	dt_fim_implantacao_w		:= null;
	vl_preco_unico_w		:= null;
	ds_unidade_medida_w		:= null;
	ie_produto_med_w		:= null;
	ie_confaz_w			:= null;
	vl_tc_w				:= null;
	tp_tabela_tiss_w		:= null;
	cd_material_tiss_w		:= null;
	cd_material_tiss_ant_w		:= null;
end if;

-- Header 
if	(ie_tipo_p = '901') then
	--Verificar se o tipo de carga e igual a 3 - Itens Inativos
	if	(substr(ds_linha_p,24,1) = 3) then
		--Se for, pegar a data de geracao para ser gravada no lugar da data de exclusao
		dt_exclusao_w	:= nvl(to_date(trim(substr(ds_linha_p,16,8)),'yyyy/mm/dd'),sysdate);
	else
		dt_exclusao_w	:= sysdate;
	end if;

-- Material 
elsif	(ie_tipo_p = '902') then
	-- Tem que resetar numero da linha filha 
	qt_linha_filha_p	:= 1;
	ie_tipo_pai_p		:= 'MAT';
	
	cd_material_w			:= trim(substr(ds_linha_p,12,8));
	cd_unidade_medida_w		:= trim(substr(ds_linha_p,20,10));
	cd_cnpj_w			:= trim(substr(ds_linha_p,30,14));
	nm_fabricante_w			:= trim(substr(ds_linha_p,44,50));
	ds_motivo_ativo_inativo_w	:= trim(substr(ds_linha_p, 174, 40));
	vl_fabrica_w			:= somente_numero(trim(substr(ds_linha_p,214,15)))/10000;
	vl_max_consumidor_w		:= somente_numero(trim(substr(ds_linha_p, 229, 15)))/10000;
	dt_inicio_obrigatorio_w		:= to_date(trim(substr(ds_linha_p,244,8)),'yyyy/mm/dd');
	ie_tipo_ww			:= '902';					
	ie_mat_med_w			:= 'S';
	cd_anterior_material_w		:= trim(substr(ds_linha_p,282,8));
	
	-- 6.2
	nr_registro_anvisa_w		:= trim(substr(ds_linha_p, 290, 15));
	cd_ref_material_fab_w		:= trim(substr(ds_linha_p, 305, 60));
	tp_produto_w			:= trim(substr(ds_linha_p, 365, 1));
	tp_codificacao_w		:= trim(substr(ds_linha_p, 366, 1));
	
	begin
	dt_inicio_vigencia_w	:= to_date(trim(substr(ds_linha_p, 367, 8)),'yyyy/mm/dd');
	exception
	when others then
		dt_inicio_vigencia_w := null;
	end;
	
	begin
	dt_fim_vigencia_w	:= to_date(trim(substr(ds_linha_p, 375, 8)),'yyyy/mm/dd');
	exception
	when others then
		dt_fim_vigencia_w := null;
	end;
	
	begin
	dt_fim_implantacao_w	:= to_date(trim(substr(ds_linha_p, 383, 8)),'yyyy/mm/dd');
	exception
	when others then
		dt_fim_implantacao_w := null;
	end;
	
	begin
	vl_preco_unico_w	:= somente_numero(trim(substr(ds_linha_p, 391, 15)))/ 10000;
	exception
	when others then
		vl_preco_unico_w := 0;
	end;
	
	-- 7.0
	ds_unidade_medida_w	:= trim(substr(ds_linha_p, 406, 3));
	vl_tc_w			:= trim(substr(ds_linha_p, 409, 2));
	
	tp_tabela_tiss_w	:= trim(substr(ds_linha_p, 411, 2));
	cd_material_tiss_w	:= trim(substr(ds_linha_p, 413, 10));
	cd_material_tiss_ant_w	:= trim(substr(ds_linha_p, 423, 10));
	
	-- No PTU 6.2 o TP_SITUACAO nao e mais informado, desta forma o mais correto e verificarmos se existe uma data fim vigencia e se ela e menor que a data de importacao do material
	-- OS 888249
	if   	(dt_fim_vigencia_w is null) or
		(dt_fim_vigencia_w > to_date(sysdate,'yyyy/mm/dd hh24:mi:ss')) then
		ie_situacao_w := 'A';
	else
		ie_situacao_w := 'I';
	end if;
	
	select	max(nr_sequencia)
	into	nr_seq_mat_unimed_p
	from	w_pls_material_unimed
	where	cd_material	= cd_material_w;

	-- Verificar se o tipo de carga e igual a 2 - Atualizacoes ultima edicao, se esta sem data de exclusao e se esta inativo
	if	(substr(ds_linha_p,24,1) = '2') and
		(ie_situacao_w = 'I') then
		update 	w_pls_material_unimed
		set	dt_exclusao	= dt_exclusao_w
		where	cd_material	= cd_material_w;
		
	elsif	(ie_situacao_w = 'A') then
		update 	w_pls_material_unimed
		set	dt_exclusao	= null
		where	cd_material	= cd_material_w;		
	end if;	
	
	qt_902_w := qt_902_w + 1;

-- Material - SIMPRO
elsif	(ie_tipo_p = '903') then
	ie_tipo_ww		:= '903';
	ie_mat_med_w		:= 'S';
	cd_simpro_w		:= trim(substr(ds_linha_p, 12, 10));
	ds_produto_simpro_w	:= trim(substr(ds_linha_p, 22, 100));

-- Medicamento
elsif	(ie_tipo_p = '905') then
	-- Tem que resetar o numero da linha filha
	qt_linha_filha_p	:= 1;
	ie_tipo_pai_p		:= 'MED';
	ie_tipo_ww		:= '905';
	ie_mat_med_w		:= 'S';
	
	cd_material_w			:= trim(substr(ds_linha_p,12,8));
	cd_unidade_medida_w		:= trim(substr(ds_linha_p,20,10));
	cd_cnpj_w			:= trim(substr(ds_linha_p,58,14));
	ds_motivo_ativo_inativo_w	:= trim(substr(ds_linha_p,73,40));	
	ie_generico_w			:= trim(substr(ds_linha_p,119,1));
	cd_anterior_medicamento_w	:= trim(substr(ds_linha_p,128,8));
	
	-- 6.2
	nr_registro_anvisa_w		:= trim(substr(ds_linha_p, 144, 15));
	tp_codificacao_w		:= trim(substr(ds_linha_p, 159, 1));
	
	begin
	vl_fator_conversao_w		:= somente_numero(trim(substr(ds_linha_p, 136, 8)))/100;
	exception
	when others then
		vl_fator_conversao_w := 0;
	end;
	
	begin
	dt_inicio_vigencia_w		:= to_date(trim(substr(ds_linha_p, 160, 8)),'yyyy/mm/dd');
	exception
	when others then
		dt_inicio_vigencia_w := null;
	end;
	
	begin
	dt_fim_vigencia_w		:= to_date(trim(substr(ds_linha_p, 168, 8)),'yyyy/mm/dd');
	exception
	when others then
		dt_fim_vigencia_w := null;
	end;
	
	begin
	dt_fim_implantacao_w		:= to_date(trim(substr(ds_linha_p, 176, 8)),'yyyy/mm/dd');
	exception
	when others then
		dt_fim_implantacao_w := null;
	end;
	
	ie_produto_med_w	:= trim(substr(ds_linha_p, 187, 1));
	
	-- RETIRADO NO PTU 11.4
	--ie_confaz_w		:= trim(substr(ds_linha_p, 188, 1));
	ie_confaz_w		:= null;
	
	
	-- 7.0
	ds_unidade_medida_w	:= trim(substr(ds_linha_p, 184, 3));
	vl_tc_w			:= null;

	tp_tabela_tiss_w	:= trim(substr(ds_linha_p, 189, 2));
	cd_material_tiss_w	:= trim(substr(ds_linha_p, 191, 10));
	cd_material_tiss_ant_w	:= trim(substr(ds_linha_p, 201, 10));
	
	-- No PTU 6.2 o TP_SITUACAO nao e mais informado, desta forma o mais correto e verificarmos se existe uma data fim vigencia e se ela e menor que a data de importacao do material
	-- OS 888249
	if   	(dt_fim_vigencia_w is null) or
		(dt_fim_vigencia_w > to_date(sysdate,'yyyy/mm/dd hh24:mi:ss')) then
		ie_situacao_w := 'A';
	else
		ie_situacao_w := 'I';
	end if;
	
	select	max(nr_sequencia)
	into	nr_seq_mat_unimed_p
	from	w_pls_material_unimed
	where	cd_material	= cd_material_w;
	
	-- Verificar se o tipo de carga e igual a 2 - Atualizacoes ultima edicao, se esta sem data de exclusao e se esta inativo
	if	(ie_situacao_w = 'I') then
		update	w_pls_material_unimed
		set	dt_exclusao	= dt_exclusao_w
		where	cd_material	= cd_material_w;

	elsif	(ie_situacao_w = 'A') then
		update	w_pls_material_unimed
		set	dt_exclusao	= null
		where	cd_material	= cd_material_w;		
	end if;	
	
	qt_905_w := qt_905_w + 1;
	
-- Medicamento - Percentual
elsif	(ie_tipo_p = '906') then
	-- Retirado no PTU 11.4
	--pr_icms_w		:= somente_numero(trim(substr(ds_linha_p,12,5)))/100;
	pr_icms_w		:= null;
	
	vl_pmc_w		:= somente_numero(trim(substr(ds_linha_p,17,15)))/10000;
	ie_tipo_ww		:= '906';
	ie_mat_med_w		:= 'S';
	vl_tcl_w		:= somente_numero(trim(substr(ds_linha_p,32,2)));
	
	-- Retirado no PTU 11.4
	--id_alc_w		:= trim(substr(ds_linha_p, 34, 1));
	id_alc_w		:= null;

-- Medicamento - BRASINDICE
elsif	(ie_tipo_p = '907') then
	ie_tipo_ww		:= '907';
	ie_mat_med_w		:= 'S';
	cd_brasindice_w		:= trim(substr(ds_linha_p, 12, 12));
	ds_produto_w		:= trim(substr(ds_linha_p, 264, 100));
	ds_apresentacao_w	:= trim(substr(ds_linha_p, 64, 200));
	
-- Trailer
elsif	(ie_tipo_p = '909') then
	qt_total_902_w		:= qt_902_w;
	qt_total_905_w 		:= qt_905_w;
else
	-- Linhas filhas 
	if	(ie_tipo_pai_p = 'MAT') then
		-- Nome comercial 
		if	(qt_linha_filha_p = 1) then
			update	w_pls_material_unimed
			set	nm_material		= trim(substr(ds_linha_p,1,255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
			
			nm_material_p 	:= trim(substr(ds_linha_p,1,255));
			
			gravar_processo_longo(nm_material_p ,'PLS_GERAR_IMPORT_MATERIAL_A900',-1);				
		-- Descricao do produto
		elsif	(qt_linha_filha_p = 2) then
			nm_material_w := trim(substr(ds_linha_p,1,255));
		
			update	w_pls_material_unimed
			set	ds_material		= nvl(nm_material_w,nm_material_p)
			where	nr_sequencia		= nr_seq_mat_unimed_p;
			
			if 	(nm_material_p is null) then				
				update	w_pls_material_unimed
				set	nm_material	= nm_material_w
				where	nr_sequencia	= nr_seq_mat_unimed_p;
			end if;
		-- Especialidade do produto
		elsif	(qt_linha_filha_p = 3) then
			update	w_pls_material_unimed
			set	ds_especialidade	= trim(substr(ds_linha_p,1,255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		-- CLassificacao do produto 
		elsif	(qt_linha_filha_p = 4) then
			update	w_pls_material_unimed
			set	ds_classe		= trim(substr(ds_linha_p,1,255)),
				ds_classe_farmacologico	= null
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		-- Nome tecnico do produto 
		elsif	(qt_linha_filha_p = 5) then
			update	w_pls_material_unimed
			set	nm_tecnico		= trim(substr(ds_linha_p,1,255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		-- Observacoes
		elsif	(qt_linha_filha_p = 6) then
			update	w_pls_material_unimed
			set	ds_observacao		= trim(substr(ds_linha_p, 1, 255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		-- Equivalencia Tecnica
		elsif	(qt_linha_filha_p = 7) then
			update	w_pls_material_unimed
			set	ds_equiv_tecnica	= trim(substr(ds_linha_p, 1, 255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		end if;
	elsif	(ie_tipo_pai_p = 'MED') then	
		-- Descricao do principio ativo
		if	(qt_linha_filha_p = 1) then
			update	w_pls_material_unimed
			set	ds_principio_ativo	= trim(substr(ds_linha_p, 1, 255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
	
		-- Nome e apresentacao comercial do produto
		elsif	(qt_linha_filha_p = 2) then
			nm_material_w := trim(substr(ds_linha_p,1,255));
		
			update	w_pls_material_unimed
			set	nm_material		= nvl(nm_material_w,nm_material_p)
			where	nr_sequencia		= nr_seq_mat_unimed_p;
			
			nm_material_p := nm_material_w;
			
			gravar_processo_longo(nm_material_p ,'PLS_GERAR_IMPORT_MATERIAL_A900',-1);
		-- Descricao do grupo 
		elsif	(qt_linha_filha_p = 3) then
			update	w_pls_material_unimed
			set	ds_grupo_farmacologico	= trim(substr(ds_linha_p,1,255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		-- Descricao da classe
		elsif	(qt_linha_filha_p = 4) then
			update	w_pls_material_unimed
			set	ds_classe_farmacologico	= trim(substr(ds_linha_p,1,255)),
				ds_classe		= null
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		-- Descricao da forma 
		elsif	(qt_linha_filha_p = 5) then
			update	w_pls_material_unimed
			set	ds_forma_farmaceutico	= trim(substr(ds_linha_p,1,255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		-- Nome do fabricante do produto 
		elsif	(qt_linha_filha_p = 6) then
			update	w_pls_material_unimed
			set	nm_fabricante		= trim(substr(ds_linha_p,1,255))
			where	nr_sequencia		= nr_seq_mat_unimed_p;
		end if;
	end if;
	qt_linha_filha_p := qt_linha_filha_p + 1;
end if;

-- PTU 5.0
if	(((ie_tipo_p = '902') and (substr(ds_linha_p,282,8) is not null)) or
	((ie_tipo_p = '905') and (substr(ds_linha_p,128,8) is not null))) then
	cd_material_tuss_w := cd_material_w;
end if;
	
-- Conversao da unid do PTU para unid do sistema 
if	(cd_unidade_medida_w is not null) then
	select	max(cd_unidade_medida)
	into	cd_unidade_w
	from	unidade_medida
	where	cd_unidade_ptu	= cd_unidade_medida_w
	and	ie_situacao	= 'A';
	
	if	(cd_unidade_w is not null) then
		cd_unidade_medida_w := substr(cd_unidade_w,1,10);
	end if;
end if;
/* Fim conversao de unidades */

--Verifica se o material ja esta cadastrado
select	count(1)
into	ie_possui_mat_w
from	w_pls_material_unimed
where	cd_material	= cd_material_w
and	rownum 		= 1;
	
if	(ie_possui_mat_w = 0) and
	(ie_mat_med_w	= 'S') then
	if	(ie_tipo_p in ('902','905')) then
		
		-- Inserir na tabela W_PLS_MATERIAL_UNIMED 
		insert into w_pls_material_unimed
			(nr_sequencia,				cd_material,			cd_unidade_medida,
			ie_tipo,				ie_situacao,			dt_atualizacao,
			nm_usuario,				nm_material,			cd_cnpj,
			nm_fabricante,				nm_importador,			nr_registro_anvisa,
			dt_validade_anvisa,			ds_motivo_ativo_inativo,	vl_fabrica,
			vl_max_consumidor,			dt_inicio_obrigatorio,		ds_material,
			ds_especialidade,			ds_classe,			ds_apresentacao,
			vl_fator_conversao,			ie_generico,			ds_grupo_farmacologico,
			ds_classe_farmacologico,		ds_forma_farmaceutico,		pr_icms,
			vl_pmc,					ds_principio_ativo,		ie_origem,
			cd_ref_material_fab,			cd_anterior_material,		cd_anterior_medicamento,
			cd_material_tuss,			tp_produto,			tp_codificacao,
			dt_inicio_vigencia,			dt_fim_vigencia,		dt_fim_implantacao,
			preco_unico,				ds_unidade_medida,		ie_produto_med,
			ie_confaz,				vl_tc,				tp_tabela_tiss,
			cd_material_tiss,			cd_material_tiss_ant)
		values	(w_pls_material_unimed_seq.nextval,	cd_material_w,			nvl(cd_unidade_medida_w,'UND'),
			ie_tipo_ww,				ie_situacao_w,			sysdate,
			nm_usuario_p,				nm_material_w,			cd_cnpj_w,
			nm_fabricante_w,			nm_importador_w,		nr_registro_anvisa_w,
			dt_validade_anvisa_w,			ds_motivo_ativo_inativo_w,	vl_fabrica_w,
			vl_max_consumidor_w,			dt_inicio_obrigatorio_w,	ds_material_w,
			ds_especialidade_w,			ds_classe_w,			ds_apresentacao_w,
			vl_fator_conversao_w,			ie_generico_w,			ds_grupo_farmacologico_w,
			ds_classe_farmacologico_w,		ds_forma_farmaceutico_w,	pr_icms_w,
			vl_pmc_w,				ds_principio_ativo_w,		ie_origem_w,
			cd_ref_material_fab_w,			cd_anterior_material_w,		cd_anterior_medicamento_w,
			cd_material_tuss_w,			tp_produto_w,			tp_codificacao_w,
			dt_inicio_vigencia_w,			dt_fim_vigencia_w,		dt_fim_implantacao_w,
			vl_preco_unico_w,			ds_unidade_medida_w,		ie_produto_med_w,
			ie_confaz_w,				vl_tc_w,			tp_tabela_tiss_w,
			cd_material_tiss_w,			cd_material_tiss_ant_w)
		returning nr_sequencia into nr_seq_mat_unimed_p;
		
	end if;
	
	-- MATERIAL SIMPRO
	if	(ie_tipo_p = '903') then
		insert into w_pls_mat_unimed_simpro
			(nr_sequencia,				dt_atualizacao,			nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,		nr_seq_mat_unimed,
			cd_simpro,				desc_prod_simpro)
		values	(w_pls_mat_unimed_simpro_seq.nextval,	sysdate,			nm_usuario_p,
			sysdate,				nm_usuario_p,			nr_seq_mat_unimed_p,
			cd_simpro_w,				ds_produto_simpro_w);
	end if;
	
	-- MEDICAMENTO ICMS
	if	(ie_tipo_p = '906') then		
		insert into w_pls_mat_unimed_trib
			(nr_sequencia,				dt_atualizacao,			nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,		nr_seq_mat_unimed,
			vl_perc_icm,				vl_pmc,				vl_tcl,
			id_alc)
		values(	w_pls_mat_unimed_trib_seq.nextval,	sysdate,			nm_usuario_p,
			sysdate,				nm_usuario_p,			nr_seq_mat_unimed_p,
			pr_icms_w,				vl_pmc_w,			vl_tcl_w,
			id_alc_w);
	end if;
	
	-- MEDICAMENTO BRASINDICE
	if	(ie_tipo_p = '907') then
		insert into w_pls_mat_unimed_bras
			(nr_sequencia,				dt_atualizacao,			nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,		nr_seq_mat_unimed,
			cd_brasindice,				des_produto,			des_apresentacao)
		values	(w_pls_mat_unimed_bras_seq.nextval,	sysdate,			nm_usuario_p,
			sysdate,				nm_usuario_p,			nr_seq_mat_unimed_p,
			cd_brasindice_w,			ds_produto_w,			ds_apresentacao_bras_w);
	end if;

	ie_mat_med_w	:= 'N';
	
elsif	(ie_possui_mat_w > 0) and
	(ie_mat_med_w	= 'S') then
	if	(ie_tipo_p in ('902','905')) then
		-- Update na tabela dos materiais A900, atualizando os dados do material que ja esta cadastrado
		select	max(nr_sequencia)
		into	nr_seq_mat_unimed_p
		from	w_pls_material_unimed
		where	cd_material	= cd_material_w;
		
		update	w_pls_material_unimed
		set	nm_usuario		= nm_usuario_p,		
			dt_atualizacao		= sysdate,
			cd_cnpj			= cd_cnpj_w,
			cd_material		= cd_material_w,
			cd_unidade_medida	= nvl(cd_unidade_medida_w,'UND'),
			ds_motivo_ativo_inativo	= ds_motivo_ativo_inativo_w,		
			dt_inicio_obrigatorio	= dt_inicio_obrigatorio_w,
			dt_validade_anvisa	= dt_validade_anvisa_w,
			ie_generico		= ie_generico_w,
			ie_origem		= ie_origem_w,
			ie_situacao		= ie_situacao_w,
			ie_tipo			= ie_tipo_ww,
			nm_importador		= nm_importador_w,
			nr_registro_anvisa	= nr_registro_anvisa_w,			
			vl_fabrica		= vl_fabrica_w,
			vl_fator_conversao	= vl_fator_conversao_w,
			vl_max_consumidor	= vl_max_consumidor_w,
			pr_icms			= pr_icms_w,
			vl_pmc			= vl_pmc_w,
			dt_exclusao		= dt_exclusao_w,
			cd_ref_material_fab	= cd_ref_material_fab_w,
			cd_anterior_material	= nvl(cd_anterior_material_w,cd_anterior_material),
			cd_anterior_medicamento = nvl(cd_anterior_medicamento_w,cd_anterior_medicamento),
			cd_material_tuss	= cd_material_tuss_w,
			tp_produto		= tp_produto_w,
			tp_codificacao		= tp_codificacao_w,
			dt_inicio_vigencia	= dt_inicio_vigencia_w,
			dt_fim_vigencia		= dt_fim_vigencia_w,
			dt_fim_implantacao	= dt_fim_implantacao_w,
			preco_unico		= vl_preco_unico_w,
			ds_unidade_medida	= ds_unidade_medida_w,
			ie_produto_med		= ie_produto_med_w,
			ie_confaz		= ie_confaz,
			tp_tabela_tiss 		= tp_tabela_tiss_w,
			cd_material_tiss 	= cd_material_tiss_w,
			cd_material_tiss_ant 	= cd_material_tiss_ant_w
		where	cd_material		= cd_material_w;
	end if;
	
	-- MATERIAL SIMPRO
	if	(ie_tipo_p = '903') and
		(nr_seq_mat_unimed_p is not null) then
		insert into w_pls_mat_unimed_simpro
			(nr_sequencia,				dt_atualizacao,			nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,		nr_seq_mat_unimed,
			cd_simpro,				desc_prod_simpro)
		values	(w_pls_mat_unimed_simpro_seq.nextval,	sysdate,			nm_usuario_p,
			sysdate,				nm_usuario_p,			nr_seq_mat_unimed_p,
			cd_simpro_w,				ds_produto_simpro_w);
	end if;
	
	-- MEDICAMENTO ICMS
	if	(ie_tipo_p = '906') and
		(nr_seq_mat_unimed_p is not null) then			
		insert into w_pls_mat_unimed_trib
			(nr_sequencia,				dt_atualizacao,			nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,		nr_seq_mat_unimed,
			vl_perc_icm,				vl_pmc,				vl_tcl,
			id_alc)
		values(	w_pls_mat_unimed_trib_seq.nextval,	sysdate,			nm_usuario_p,
			sysdate,				nm_usuario_p,			nr_seq_mat_unimed_p,
			pr_icms_w,				vl_pmc_w,			vl_tcl_w,
			id_alc_w);
	end if;
	
	-- MEDICAMENTO BRASINDICE
	if	(ie_tipo_p = '907') and
		(nr_seq_mat_unimed_p is not null) then
		insert into w_pls_mat_unimed_bras
			(nr_sequencia,				dt_atualizacao,			nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,		nr_seq_mat_unimed,
			cd_brasindice,				des_produto,			des_apresentacao)
		values	(w_pls_mat_unimed_bras_seq.nextval,	sysdate,			nm_usuario_p,
			sysdate,				nm_usuario_p,			nr_seq_mat_unimed_p,
			cd_brasindice_w,			ds_produto_w,			ds_apresentacao_bras_w);
	end if;
	
	commit;
	
	ie_mat_med_w	:= 'N';
end if;

exception
when others then
	ds_erro_p := 'Erro ao executar a pls_gerar_import_mat_v114. Erro: ' || sqlerrm;

end pls_gerar_import_mat_v114;
/
