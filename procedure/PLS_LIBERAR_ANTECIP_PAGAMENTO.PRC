create or replace
procedure pls_liberar_antecip_pagamento(nr_titulo_p		number,
				nm_usuario_p		Varchar2) is 

begin
update	titulo_pagar
set	ie_status = 'D'
where	nr_titulo = nr_titulo_p;

commit;

end pls_liberar_antecip_pagamento;
/