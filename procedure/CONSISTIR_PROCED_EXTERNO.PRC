create or replace
procedure Consistir_proced_externo(	nr_prescricao_p		number,
									nr_seq_proced_p		number,
									nm_usuario_p		varchar2,
									cd_perfil_p			number,
									ie_consistir_p	out	varchar2) is

cd_procedimento_w			procedimento.cd_procedimento%type;
ie_origem_proced_w			procedimento.ie_origem_proced%type;
nr_seq_exame_w				exame_laboratorio.nr_seq_exame%type;
nr_seq_proc_interno_w		proc_interno.nr_sequencia%type;
nr_sequencia_w				prescr_procedimento.nr_sequencia%type;
nr_seq_grupo_w				grupo_exame_lab.nr_sequencia%type;
cd_area_proced_prescr_w		area_procedimento.cd_area_procedimento%type;
cd_especialidade_prescr_w	especialidade_proc.cd_especialidade%type;
cd_grupo_proc_prescr_w		grupo_proc.cd_grupo_proc%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_convenio_w				convenio.cd_convenio%type;
cd_setor_atendimento_w		setor_atendimento.cd_setor_atendimento%type;
cd_setor_execucao_w			setor_atendimento.cd_setor_atendimento%type;
cd_tipo_procedimento_w		regra_proced_externo.cd_tipo_procedimento%type;
ie_tipo_atendimento_w		regra_proced_externo.ie_tipo_atendimento%type;
ie_consiste_externo_w		regra_proced_externo.ie_consiste_externo%type;
cd_motivo_baixa_w			regra_proced_externo.cd_motivo_baixa%type;
nr_atendimento_w			atendimento_paciente.nr_atendimento%type;
cd_perfil_w					perfil.cd_perfil%type;

cursor C01 is
select	ie_consiste_externo,
		cd_motivo_baixa
from	regra_proced_externo
where	coalesce(cd_area_procedimento, cd_area_proced_prescr_w) = cd_area_proced_prescr_w
and		coalesce(cd_especialidade, cd_especialidade_prescr_w) = cd_especialidade_prescr_w
and		coalesce(cd_grupo_proc, cd_grupo_proc_prescr_w) = cd_grupo_proc_prescr_w
and		coalesce(nr_seq_grupo_lab, nr_seq_grupo_w) = nr_seq_grupo_w
and		coalesce(nr_seq_exame_interno, nr_seq_proc_interno_w) = nr_seq_proc_interno_w
and		coalesce(nr_seq_exame_lab, nr_seq_exame_w) = nr_seq_exame_w
and		((cd_procedimento is null) or (coalesce(ie_origem_proced, ie_origem_proced_w) = ie_origem_proced_w))
and		coalesce(cd_procedimento, cd_procedimento_w) = cd_procedimento_w
and		cd_estabelecimento = cd_estabelecimento_w
and		coalesce(cd_convenio, cd_convenio_w) = cd_convenio_w
and		coalesce(cd_setor_atendimento, cd_setor_atendimento_w) = cd_setor_atendimento_w
and		coalesce(cd_tipo_procedimento, cd_tipo_procedimento_w) = cd_tipo_procedimento_w
and		coalesce(ie_tipo_atendimento, ie_tipo_atendimento_w) = ie_tipo_atendimento_w
and		coalesce(cd_setor_execucao, cd_setor_execucao_w) = cd_setor_execucao_w
and		coalesce(cd_perfil, cd_perfil_w) = cd_perfil_w
order by
	coalesce(cd_perfil,0),
	coalesce(nr_seq_exame_interno,0),
	coalesce(cd_procedimento, 0),
	coalesce(cd_grupo_proc, 0),
	coalesce(cd_especialidade, 0),
	coalesce(cd_area_procedimento, 0),
	coalesce(nr_seq_exame_lab,0),
	coalesce(nr_seq_grupo_lab,0),
	coalesce(cd_convenio,0),
	coalesce(cd_setor_atendimento,0),
	coalesce(ie_tipo_atendimento,0),
	coalesce(cd_setor_execucao,0),
	coalesce(cd_tipo_procedimento,0);

begin

select	coalesce(max('S'),'N')
into	ie_consiste_externo_w
from 	regra_proced_externo
where	rownum = 1;

if	(ie_consiste_externo_w = 'S') then
	ie_consiste_externo_w	:= 'N';
	cd_motivo_baixa_w		:= 0;
	cd_perfil_w				:= coalesce(cd_perfil_p,0);
	
	select	coalesce(max(b.cd_procedimento),0),
			coalesce(max(b.ie_origem_proced),0),
			coalesce(max(b.nr_seq_exame),0),
			coalesce(max(b.nr_seq_proc_interno),0),
			max(b.nr_sequencia),
			coalesce(max(b.cd_setor_atendimento),0),
			coalesce(max(a.cd_setor_atendimento),0),
			coalesce(max(cd_estabelecimento),1),
			max(a.nr_atendimento)
	into	cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_exame_w,
			nr_seq_proc_interno_w,
			nr_sequencia_w,
			cd_setor_execucao_w,
			cd_setor_atendimento_w,
			cd_estabelecimento_w,
			nr_atendimento_w
	from	prescr_procedimento b,
			prescr_medica a
	where	a.nr_prescricao	= b.nr_prescricao
	and		a.nr_prescricao	= nr_prescricao_p
	and		b.nr_sequencia	= nr_seq_proced_p
	and		a.nr_atendimento is not null;

	if	(nr_atendimento_w > 0) then
		cd_convenio_w 			:= obter_convenio_atendimento(nr_atendimento_w);
		ie_tipo_atendimento_w	:= obter_tipo_atendimento(nr_atendimento_w);
	end if;
	
	cd_convenio_w			:= coalesce(cd_convenio_w,0);
	ie_tipo_atendimento_w	:= coalesce(ie_tipo_atendimento_w,0);

	if	(nr_seq_exame_w > 0) then
		begin
		select	nr_seq_grupo
		into	nr_seq_grupo_w
		from	exame_laboratorio
		where	nr_seq_exame = nr_seq_exame_w;
		exception when others then
			nr_seq_grupo_w := null;
		end;
	end if;	
	
	nr_seq_grupo_w	:= coalesce(nr_seq_grupo_w,0);

	select	coalesce(max(cd_grupo_proc),0),
			coalesce(max(cd_especialidade),0),
			coalesce(max(cd_area_procedimento),0),
			coalesce(max(cd_tipo_procedimento),0)
	into	cd_grupo_proc_prescr_w,
			cd_especialidade_prescr_w,
			cd_area_proced_prescr_w,
			cd_tipo_procedimento_w
	from	estrutura_procedimento_v
	where	cd_procedimento 	= cd_procedimento_w
	and		ie_origem_proced	= ie_origem_proced_w;

	open c01;
	loop
	fetch c01 into
		ie_consiste_externo_w,
		cd_motivo_baixa_w;
	exit when c01%notfound;
		begin
		ie_consiste_externo_w	:= ie_consiste_externo_w;
		cd_motivo_baixa_w	:= cd_motivo_baixa_w;
		end;
	end loop;
	close c01;

	if	(coalesce(cd_motivo_baixa_w,0) > 0) then
		update	prescr_procedimento
		set		cd_motivo_baixa = cd_motivo_baixa_w,
				dt_baixa	= sysdate
		where	nr_prescricao	= nr_prescricao_p
		and		nr_sequencia	= nr_seq_proced_p;
		
		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	end if;
end if;

ie_consistir_p	:= ie_consiste_externo_w;

end Consistir_proced_externo;
/
