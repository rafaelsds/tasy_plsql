create or replace
procedure pls_gerar_ocor_cta_hist (	nr_seq_conta_p		pls_conta.nr_sequencia%type,
					nr_seq_ocorrencia_p	pls_ocorrencia.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					ie_origem_p		varchar2,
					ie_origem_ocorrencia_p	pls_oc_cta_combinada.ie_evento%type default null) is 

/*
ie_origem_p
L - Local
I - Importação
*/	
				
cd_guia_w			pls_conta.cd_guia%type;
cd_guia_referencia_w		pls_conta.cd_guia_referencia%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nr_seq_prestador_exec_w		pls_prestador.nr_sequencia%type;	
nr_seq_prestador_prot_w		pls_prestador.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_protocolo_prestador_w	pls_protocolo_conta.nr_protocolo_prestador%type;

begin

if	(nr_seq_conta_p is not null) then
	
	if	(ie_origem_p = 'L') then
	
		select	a.cd_guia,
			a.cd_guia_referencia,
			a.nr_seq_prestador_exec,
			a.nr_seq_segurado,
			b.nr_seq_prestador,
			a.cd_estabelecimento,
			b.nr_protocolo_prestador
		into	cd_guia_w,
			cd_guia_referencia_w,
			nr_seq_prestador_exec_w,
			nr_seq_segurado_w,
			nr_seq_prestador_prot_w,
			cd_estabelecimento_w,
			nr_protocolo_prestador_w
		from	pls_conta a,
			pls_protocolo_conta b
		where	b.nr_sequencia = a.nr_seq_protocolo
		and	a.nr_sequencia = nr_seq_conta_p;
		
	elsif	(ie_origem_p = 'I') then
		
		select	a.cd_guia_operadora_conv,
			a.cd_guia_principal_conv,
			a.nr_seq_prest_exec_conv,
			a.nr_seq_segurado_conv,
			b.nr_seq_prestador_conv,
			a.cd_estabelecimento,
			b.nr_lote_prestador
		into	cd_guia_w,
			cd_guia_referencia_w,
			nr_seq_prestador_exec_w,
			nr_seq_segurado_w,
			nr_seq_prestador_prot_w,
			cd_estabelecimento_w,
			nr_protocolo_prestador_w
		from	pls_conta_imp a,
			pls_protocolo_conta_imp b
		where	b.nr_sequencia = a.nr_seq_protocolo
		and	a.nr_sequencia = nr_seq_conta_p;
	end if;
	
	insert	into	pls_ocorrencia_cta_hist
		(	cd_guia, cd_guia_referencia, dt_atualizacao,
			dt_atualizacao_nrec, nm_usuario, nm_usuario_nrec,
			nr_seq_ocorrencia, nr_seq_prestador_exec, nr_seq_prestador_prot,
			nr_seq_segurado, nr_sequencia, cd_estabelecimento,
			nr_protocolo_prestador, ie_origem_ocorrencia)
	values	(	cd_guia_w, cd_guia_referencia_w, sysdate,
			sysdate, nm_usuario_p, nm_usuario_p,
			nr_seq_ocorrencia_p, nr_seq_prestador_exec_w, nr_seq_prestador_prot_w,
			nr_seq_segurado_w, pls_ocorrencia_cta_hist_seq.nextval, cd_estabelecimento_w,
			nr_protocolo_prestador_w, ie_origem_ocorrencia_p);
	commit;
end if;

end pls_gerar_ocor_cta_hist;
/
