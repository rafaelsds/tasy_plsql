create or replace
procedure VALIDA_EXAME_DIA_SEMANA (	nr_exame_p	  			number,
									nr_seq_material_p 		varchar2,
									dt_prescr_exame_p 		date,
									nm_usuario_p	  		Varchar2,
									cd_estabelecimento_p 	varchar2,
									ie_permite_p		out	varchar2) is 
ie_domingo_w		varchar2(1):= '';
ie_segunda_w		varchar2(1):= '';
ie_terca_w			varchar2(1):= '';
ie_quarta_w			varchar2(1):= '';
ie_quinta_w			varchar2(1):= '';
ie_sexta_w			varchar2(1):= '';
ie_sabado_w			varchar2(1):= '';
ie_feriado_w		varchar2(1):= '';
ie_permite_w		varchar2(1):= 'S';
ds_dia_semana_w		number(10);
ie_dia_feriado_w 	number(10);
nr_seq_grupo_w		number(10);
qt_exame_dia_w		number(10) := 0;
nr_seq_exame_dia_w	number(10) := 0;
ie_tipo_feriado_w	number(5);
ie_tipo_regra_w		varchar2(1);

begin

if 	(nr_exame_p is not null) and
	(nr_seq_material_p is not null) then
	begin
	
	select  nvl(max(nr_sequencia),0)
	into	nr_seq_exame_dia_w
	from (
		select  a.nr_sequencia
		from 	lab_exame_dia a,
				material_exame_lab b
		where 	a.nr_seq_exame 	=	nr_exame_p
		and		((a.ie_tipo_regra	= 'C') or (a.ie_tipo_regra	= 'L'))
		and		a.nr_seq_material =	b.nr_sequencia
		and		trim(b.cd_material_exame) = trim(nr_seq_material_p)
		and		nvl(a.cd_estabelecimento,nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0)
		order by a.cd_estabelecimento
	) where rownum = 1;
	end;
end if;

if 	(nr_exame_p is not null) and
	(nr_seq_exame_dia_w = 0) then
	begin	
	
	select 	count(*)	
	into	qt_exame_dia_w
	from   	lab_exame_dia  a
	where  	a.nr_seq_exame = nr_exame_p;

	if	( qt_exame_dia_w > 0) then --verifica se existe algum exame cadastrado na regra
		begin
		
		select 	nvl(max(nr_sequencia),0)
		into 	nr_seq_exame_dia_w
		from (
			select   a.nr_sequencia
			from     lab_exame_dia  a
			where    a.nr_seq_exame = nr_exame_p
			and  	 ((a.ie_tipo_regra = 'C') or (a.ie_tipo_regra = 'L'))
			and  	 nvl(a.cd_estabelecimento,nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0)
			order by a.cd_estabelecimento
		) where	rownum = 1;
				
		end;
	end if;
	end;
end if;

if	(nr_exame_p is not null) and
	(qt_exame_dia_w = 0) and
	(nr_seq_exame_dia_w = 0) then --se n�o existir exame cadastrado na regra, verifica se existe grupo na regra
	begin
	
	select  nvl(max(nr_sequencia),0)
	into	nr_seq_exame_dia_w
	from (
		select  f.nr_sequencia
		from    exame_laboratorio h,
				grupo_exame_lab   i,
				lab_exame_dia     f
		where   h.nr_seq_exame    = nr_exame_p
		and     ((f.nr_seq_exame is null) or (f.nr_seq_exame = nr_exame_p))
		and     i.nr_sequencia    = h.nr_seq_grupo
		and     f.Nr_Seq_Grupo    = i.nr_sequencia
		and     ((f.ie_tipo_regra = 'C') or (f.ie_tipo_regra	= 'L'))
		and		nvl(f.cd_estabelecimento,nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0)
		order by f.cd_estabelecimento
	) where rownum = 1;	
	
	end;
end if;

if 	(nr_seq_exame_dia_w > 0) then
	begin

	select 	a.ie_domingo, 
			a.ie_segunda, 
			a.ie_terca, 
			a.ie_quarta, 
			a.ie_quinta, 
			a.ie_sexta, 
			a.ie_sabado, 
			a.ie_feriado,
			a.ie_tipo_feriado,
			a.ie_tipo_regra
	into	ie_domingo_w,
			ie_segunda_w,
			ie_terca_w,
			ie_quarta_w,
			ie_quinta_w,
			ie_sexta_w,
			ie_sabado_w,
			ie_feriado_w,
			ie_tipo_feriado_w,
			ie_tipo_regra_w
	from 	lab_exame_dia a
	where	a.nr_sequencia = nr_seq_exame_dia_w
	and		nvl(a.cd_estabelecimento, nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0)
	and		((a.ie_tipo_regra	= 'C') or (a.ie_tipo_regra	= 'L'));

	
	ie_dia_feriado_w := lab_obter_se_feriado(cd_estabelecimento_p, nvl(dt_prescr_exame_p, sysdate), ie_tipo_feriado_w);

	if	( ie_dia_feriado_w > 0) then
		ds_dia_semana_w := 8;
	elsif	( ie_dia_feriado_w = 0) then
		ds_dia_semana_w	:= pkg_date_utils.get_WeekDay(nvl(dt_prescr_exame_p,sysdate));
	end if;
	
	if (ie_tipo_regra_w = 'C') then
		if	( ds_dia_semana_w = 1) then 
			if ( ie_domingo_w = 'N') then
				ie_permite_w := 'N';
			end if;
		elsif	( ds_dia_semana_w = 2) then 
			if ( ie_segunda_w = 'N') then
				ie_permite_w := 'N';
			end if;
		elsif	( ds_dia_semana_w = 3) then
			if ( ie_terca_w = 'N') then
				ie_permite_w := 'N';
			end if;
		elsif	( ds_dia_semana_w = 4) then
			if ( ie_quarta_w = 'N') then
				ie_permite_w := 'N';
			end if;
		elsif	( ds_dia_semana_w = 5) then
			if ( ie_quinta_w = 'N') then
				ie_permite_w := 'N';
			end if;
		elsif	( ds_dia_semana_w = 6) then
			if ( ie_sexta_w = 'N') then
				ie_permite_w := 'N';
			end if;
		elsif	( ds_dia_semana_w = 7) then
			if ( ie_sabado_w = 'N') then
				ie_permite_w := 'N';
			end if;
		elsif	( ds_dia_semana_w = 8) then
			if ( ie_feriado_w = 'N') then
				ie_permite_w := 'N';
			end if;
		end if;
	elsif (ie_tipo_regra_w = 'L') then
		ie_permite_w := 'L';
	end if;
	end;
end if;
	
ie_permite_p := ie_permite_w;
commit;

end valida_exame_dia_semana;
/