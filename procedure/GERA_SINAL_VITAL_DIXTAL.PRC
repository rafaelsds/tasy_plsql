create or replace
procedure gera_sinal_vital_dixtal(
			cd_pessoa_fisica_p  	varchar2,
			nm_usuario_p   		Varchar2,
			dt_sinal_vital_p  	varchar2,
			qt_freq_cardiaca_p  	number,         --HR
			qt_freq_resp_p   	number,         --RR
								--PULSE
			qt_pa_sistolica_p  number,  		--NBPs
			qt_pa_diastolica_p  number,  		--NBPd
			qt_pam_p   number,  			--NBPm
			qt_art_pa_sistolica_p  number,  	--ARTs
			qt_art_pa_diastolica_p  number,  	--ARTd
			qt_art_pam_p   number,  		--ARTm
			qt_co2_p   number,  			--etCO2
			qt_saturacao_o2_p  number,  		--SpO2
								--PVC
								--IC1(mean)
			qt_pressao_intra_cranio_p number,  	--IC1(systolic)
								--IC1(dialostic)
								--PaO2
			qt_temp_core_p   number,  		--Tcore
			qt_temp_skin_p   number  		--Tskin
			) is

nr_atendimento_w  	number(10);
dt_sinal_vital_w  	date;

ie_utiliza_pressao_inv_w varchar2(1) := 'S';

begin

dt_sinal_vital_w := to_date(dt_sinal_vital_p, 'yyyymmddhh24miss');

select 	max(nr_atendimento)
into 	nr_atendimento_w
from 	atendimento_paciente
where 	cd_pessoa_fisica  = cd_pessoa_fisica_p;

if  ((qt_pa_sistolica_p is null) and (qt_pa_diastolica_p is null) and (qt_pam_p is null)) then
	ie_utiliza_pressao_inv_w := 'N';
end if;


insert  into atendimento_sinal_vital
	(
	nr_sequencia,
	cd_pessoa_fisica,
	nr_atendimento,
	dt_liberacao,
	dt_sinal_vital,
	dt_atualizacao,
	nm_usuario,
	ie_pressao,
	qt_freq_cardiaca,
	qt_freq_resp,
	ie_aparelho_pa,
	qt_pa_sistolica,
	qt_pa_diastolica,
	qt_pam,
	qt_saturacao_o2,
	qt_pressao_intra_cranio,
	ie_sitio,
	qt_temp
	)
values
	(
	atendimento_sinal_vital_seq.NextVal,
	cd_pessoa_fisica_p,
	nr_atendimento_w,
	dt_sinal_vital_w,
	dt_sinal_vital_w,
	sysdate,
	nm_usuario_p,
	'D', -- Dom -- Posi��o press�o arterial
	qt_freq_cardiaca_p,
	qt_freq_resp_p,
	decode(ie_utiliza_pressao_inv_w, 'S','A','I' ), --Dom -- Tipo de aparelho press�o arterial
	decode(ie_utiliza_pressao_inv_w, 'S', qt_pa_sistolica_p, qt_art_pa_sistolica_p),
	decode(ie_utiliza_pressao_inv_w, 'S', qt_pa_diastolica_p, qt_art_pa_diastolica_p),
	decode(ie_utiliza_pressao_inv_w, 'S', qt_pam_p, qt_art_pam_p),
	qt_saturacao_o2_p,
	qt_pressao_intra_cranio_p,
	decode(qt_temp_core_p, null, 1, 2), -- Dom  --  Locais para med de temperatura corporal (1- axilar / 2-esof�gico)
	decode(qt_temp_core_p, null, qt_temp_skin_p, qt_temp_core_p)
	);



if  ((ie_utiliza_pressao_inv_w = 'S') and  ((qt_art_pa_sistolica_p is not null) or (qt_art_pa_diastolica_p is not null) or (qt_art_pam_p is not null))) or
    ((qt_temp_core_p is not null) and (qt_temp_skin_p is not null))then

	insert  into atendimento_sinal_vital
		(
		nr_sequencia,
		cd_pessoa_fisica,
		nr_atendimento,
		dt_liberacao,
		dt_sinal_vital,
		dt_atualizacao,
		nm_usuario,
		ie_pressao,
		ie_aparelho_pa,
		qt_pa_sistolica,
		qt_pa_diastolica,
		qt_pam,
		ie_sitio,
		qt_temp
		)
	values
		(
		atendimento_sinal_vital_seq.NextVal,
		cd_pessoa_fisica_p,
		nr_atendimento_w,
		dt_sinal_vital_w,
		dt_sinal_vital_w,
		sysdate,
		nm_usuario_p,
		'D', -- Dom -- Posi��o press�o arterial
		decode(ie_utiliza_pressao_inv_w, 'N', null, 'I'), --Dom -- Tipo de aparelho press�o arterial
		qt_art_pa_sistolica_p,
		qt_art_pa_diastolica_p,
		qt_art_pam_p,
		decode(qt_temp_skin_p,null,null,1), -- Dom  --  Locais para med de temperatura corporal
		qt_temp_skin_p
		);

end if;


if 	(qt_co2_p is not null)  then

	insert into atendimento_monit_resp
		(
		nr_sequencia,
		nr_atendimento,
		dt_monitorizacao,
		dt_atualizacao,
		dt_liberacao,
		nm_usuario,
		qt_saturacao_o2,
		qt_co2
		)
		values
		(
		atendimento_monit_resp_seq.NextVal,
		nr_atendimento_w,
		dt_sinal_vital_w,
		sysdate,
		dt_sinal_vital_w,
		nm_usuario_p,
		qt_saturacao_o2_p,
		qt_co2_p
		);

end if;

commit;

end gera_sinal_vital_dixtal;
/