create or replace
procedure Obter_regra_assinatura_perfil (	nr_seq_projeto_p			number,
											cd_perfil_p 				number,
											ie_assinatura_p	 			out nocopy varchar2,
											ie_consulta_p	 			out nocopy varchar2,
											ie_alertar_p	 			out nocopy varchar2,
											ds_diretorio_p 	 			out nocopy varchar2,
											nm_arquivo_p	 			out nocopy varchar2,
											ie_gerar_pdf_p	 			out nocopy varchar2,
											ie_solic_senha_p 			out nocopy varchar2,
											ds_mensagem_cert_p			out nocopy varchar2,
											ds_mensagem_sucesso_p		out nocopy varchar2,
											ie_imprimir_p				out nocopy varchar2,
											ie_nivel_log_p				out nocopy number,
											cd_relatorio_p				out nocopy number,
											cd_classif_relat_p			out nocopy varchar2,
											ie_permite_lote_p			out nocopy varchar2) is

nr_seq_relatorio_w	relatorio.nr_sequencia%type;
cd_relatorio_w		relatorio.cd_relatorio%type;
cd_classif_relat_w	relatorio.cd_classif_relat%type;

	cursor c01 is
	select	a.ie_assinatura,
			a.ie_consulta,
			a.ie_alertar,
			a.ds_diretorio,
			a.nm_arquivo,
			a.ie_gerar_pdf,
			a.ie_solic_senha,
			a.ds_mensagem_Certificado,
			a.ds_mensagem_sucesso,
			a.ie_imprimir,
			a.ie_nivel_log,
			a.ie_permite_lote,
			b.nr_seq_relatorio
	from	tasy_proj_assin_perfil a,
			tasy_projeto_assinatura b
	where 	a.nr_seq_proj  = b.nr_sequencia
	and 	b.nr_sequencia  = nr_seq_projeto_p
	and 	nvl(a.cd_perfil, nvl(cd_perfil_p,0)) = nvl(cd_perfil_p,0)
	order by nvl(a.cd_perfil,0) desc;

begin
	ie_assinatura_p := 'N';
	ie_consulta_p := 'N';

	for r_c01_w in C01
	loop
		ie_assinatura_p := r_c01_w.ie_assinatura;
		ie_consulta_p := r_c01_w.ie_consulta;
		ie_alertar_p := r_c01_w.ie_alertar;
		ds_diretorio_p := r_c01_w.ds_diretorio;
		nm_arquivo_p := r_c01_w.nm_arquivo;
		ie_gerar_pdf_p := r_c01_w.ie_gerar_pdf;
		ie_solic_senha_p := r_c01_w.ie_solic_senha;
		ds_mensagem_cert_p := r_c01_w.ds_mensagem_Certificado;
		ds_mensagem_sucesso_p := r_c01_w.ds_mensagem_sucesso;
		ie_imprimir_p := r_c01_w.ie_imprimir;
		ie_nivel_log_p := r_c01_w.ie_nivel_log;
		ie_permite_lote_p := r_c01_w.ie_permite_lote;

		nr_seq_relatorio_w := r_c01_w.nr_seq_relatorio;		
	end loop;

	if	(nr_seq_relatorio_w > 0) then
		select	max(cd_relatorio),
				max(cd_classif_relat)
		into	cd_relatorio_w,
				cd_classif_relat_w
		from	relatorio
		where	nr_sequencia = nr_seq_relatorio_w;
		
		cd_classif_relat_p := cd_classif_relat_w;
		cd_relatorio_p := cd_relatorio_w;
	end if;

end Obter_regra_assinatura_perfil;
/
