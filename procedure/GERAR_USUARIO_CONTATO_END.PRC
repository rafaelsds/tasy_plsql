create or replace
procedure gerar_usuario_contato_end( 	cd_pessoa_fisica_p 	varchar2,
				nm_usuario_p 		varchar2,
				nr_seq_contato_p 	number) is
	
nr_seq_end_w 		number(10);
nr_seq_compl_pes_w 	number(10);
ds_endereco_w 		varchar2(40);
nr_endereco_w 		number(5);
cd_cep_w 		varchar2(15);
ds_bairro_w 		varchar2(80);
ds_municipio_w 		varchar2(40);
sg_estado_w 		compl_pessoa_fisica.sg_estado%type;
nr_telefone_w 		varchar2(15);
nr_ramal_w 		number(5);
ds_observacao_w 		varchar2(255);
ds_complemento_w 	varchar2(40);
NM_EMPRESA_w		varchar2(255);
begin
  
-- INSERCAO DO COMPLEMENTO RESIDENCIAL
select 	max(nr_sequencia),
	max(ds_endereco),
	max(nr_endereco),
	max(cd_cep),
	max(ds_bairro),
	max(ds_municipio),
	max(sg_estado),
	max(nr_telefone),
	max(nr_ramal),
	max(ds_observacao),
	max(ds_complemento),
	substr(max(OBTER_NOME_EMPRESA_REF(CD_EMPRESA_REFER)),1,50)
into 	nr_seq_compl_pes_w,
	ds_endereco_w,
	nr_endereco_w,
	cd_cep_w,
	ds_bairro_w,
	ds_municipio_w,
	sg_estado_w,
	nr_telefone_w,
	nr_ramal_w,
	ds_observacao_w,
	ds_complemento_w,
	NM_EMPRESA_w
from 	compl_pessoa_fisica
where 	ie_tipo_complemento = 1
and 	cd_pessoa_fisica = cd_pessoa_fisica_p;

if (nr_seq_compl_pes_w  is not null) then

	select usuario_contato_end_seq.nextval
	into nr_seq_end_w
	from dual;  

	insert into usuario_contato_end(
		nr_sequencia,
		nr_seq_contato,
		ds_endereco,
		nr_endereco,
		cd_cep,
		ds_bairro,
		ds_municipio,
		sg_estado,
		nr_telefone,
		nr_ramal,
		ds_observacao,
		ds_complemento,
		ie_tipo_complemento,
		dt_atualizacao,
		nm_usuario,
		NM_EMPRESA)
	values(	nr_seq_end_w,
		nr_seq_contato_p,
		ds_endereco_w,
		nr_endereco_w,
		cd_cep_w,
		substr(ds_bairro_w,1,40),
		ds_municipio_w,
		sg_estado_w,
		nr_telefone_w,
		nr_ramal_w,
		ds_observacao_w,
		ds_complemento_w,
		0,
		sysdate,
		nm_usuario_p,
		NM_EMPRESA_w);
end if;

-- INSERCAO DO COMPLEMENTO COMERCIAL
select 	max(nr_sequencia),
	max(ds_endereco),
	max(nr_endereco),
	max(cd_cep),
	max(ds_bairro),
	max(ds_municipio),
	max(sg_estado),
	max(nr_telefone),
	max(nr_ramal),
	max(ds_observacao),
	max(ds_complemento),
	substr(max(OBTER_NOME_EMPRESA_REF(CD_EMPRESA_REFER)),1,50)
into 	nr_seq_compl_pes_w,
	ds_endereco_w,
	nr_endereco_w,
	cd_cep_w,
	ds_bairro_w,
	ds_municipio_w,
	sg_estado_w,
	nr_telefone_w,
	nr_ramal_w,
	ds_observacao_w,
	ds_complemento_w,
	NM_EMPRESA_w
from 	compl_pessoa_fisica
where 	ie_tipo_complemento = 2
and 	cd_pessoa_fisica = cd_pessoa_fisica_p;

if (nr_seq_compl_pes_w  is not null) then

	select usuario_contato_end_seq.nextval
	into nr_seq_end_w
	from dual;  

	insert into usuario_contato_end(
		nr_sequencia,
		nr_seq_contato,
		ds_endereco,
		nr_endereco,
		cd_cep,
		ds_bairro,
		ds_municipio,
		sg_estado,
		nr_telefone,
		nr_ramal,
		ds_observacao,
		ds_complemento,
		ie_tipo_complemento,
		dt_atualizacao,
		nm_usuario,
		NM_EMPRESA)
	values(	nr_seq_end_w,
		nr_seq_contato_p,
		ds_endereco_w,
		nr_endereco_w,
		cd_cep_w,
		substr(ds_bairro_w,1,40),
		ds_municipio_w,
		sg_estado_w,
		nr_telefone_w,
		nr_ramal_w,
		ds_observacao_w,
		ds_complemento_w,
		1,
		sysdate,
		nm_usuario_p,
		NM_EMPRESA_w);
end if;

commit;

end gerar_usuario_contato_end;
/
