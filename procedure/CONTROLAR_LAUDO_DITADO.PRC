create or replace
procedure controlar_laudo_ditado ( nr_seq_interno_p	number,
				 ie_acao_p		varchar2,
				 nm_usuario_p		Varchar2) is 
				 
/*
 ie_acao_p 
 I = Incluir
 E = Excluir
*/

ie_status_execucao_w	varchar2(10);

begin

if 	(ie_acao_p = 'I') then
	
	select	max(ie_status_execucao)
	into	ie_status_execucao_w
	from	prescr_procedimento
	where	nr_seq_interno = nr_seq_interno_p;	
	
	insert into controle_ditar_laudo (				
		nm_usuario,					
		nr_seq_interno,	
		dt_atualizacao_nrec,  
		nm_usuario_nrec,
		dt_atualizacao)
	values(	nm_usuario_p,
		nr_seq_interno_p,
		sysdate,
		nm_usuario_p,
		sysdate);
	
elsif	(ie_acao_p = 'E') then
	delete
	from	controle_ditar_laudo
	where	nr_seq_interno = nr_seq_interno_p
	and	nm_usuario = nm_usuario_p;
end if;	
	

commit;

end controlar_laudo_ditado;
/
