create or replace trigger material_horario_padrao_atual
before insert or update on material_horario_padrao
for each row

declare

begin
begin
	if (:new.hr_final is not null) and ((:new.hr_final <> :old.hr_final) or (:old.dt_fim is null)) then
		:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_final,'dd/mm/yyyy hh24:mi');
	end if;	
	if (:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
		:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/

