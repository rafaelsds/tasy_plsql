create or replace 
procedure excluir_lote_agrupamento(	nr_seq_lote_p number,
									nr_lote_agrup_p number,
									nm_usuario_p varchar2) is

qt_lotes_agrupados_w 	number(10);
begin 

update	ap_lote
set 	nr_lote_agrupamento = null,
	dt_impressao = null,
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_seq_lote_p;

insert into ap_lote_historico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_seq_lote,
	ds_evento,
	ds_log)
values(	ap_lote_historico_seq.nextval,
	sysdate,
	nm_usuario_p,
	nr_seq_lote_p,
	wheb_mensagem_pck.get_texto(1088349),
	wheb_mensagem_pck.get_texto(1088351,'NR_AGRUPAMENTO='||nr_lote_agrup_p) || '-' || dbms_utility.format_call_stack);

select	count(*)
into	qt_lotes_agrupados_w
from	ap_lote a
where 	a.nr_lote_agrupamento  = nr_lote_agrup_p;

if	(qt_lotes_agrupados_w = 0) then 
	update	ap_lote
	set 	ie_status_lote = 'C',
			nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_lote_agrup_p;

	insert into ap_lote_historico(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_lote,
		ds_evento,
		ds_log)
	values(	ap_lote_historico_seq.nextval,
		sysdate,
		nm_usuario_p,
		nr_lote_agrup_p,
		wheb_mensagem_pck.get_texto(1088349),
		substr(wheb_mensagem_pck.get_texto(1088350) || '-' || dbms_utility.format_call_stack,1,2000));		
end if;

commit;

end excluir_lote_agrupamento;
/