create or replace
procedure CONSISTIR_ITEM_GUIA_GRG(	nr_seq_lote_hist_p	in	number,
					nr_seq_hist_guia_p	in	number,
					nr_seq_hist_item_p	in	number,
					nm_usuario_p		in	varchar2,
					ds_retorno_p		out	varchar2) is

ds_retorno_w			varchar2(255)	:= 'X';
nr_seq_hist_item_w		number(10);
vl_adicional_w			number(15,2);
vl_amenor_w			number(15,2);
vl_glosa_w			number(15,2);
vl_pago_w			number(15,2);
qt_participante_w		number(10);
vl_adicional_partic_w		number(15,2);
vl_amenor_partic_w		number(15,2);
vl_glosa_partic_w		number(15,2);
vl_pago_partic_w		number(15,2);
nr_seq_guia_w			lote_audit_hist_item.nr_seq_guia%type;

cursor	c01 is
select	b.nr_sequencia,
	b.vl_adicional,
	b.vl_amenor,
	b.vl_glosa,
	b.vl_pago,
	b.nr_seq_guia
from	lote_audit_hist_item b,
	lote_audit_hist_guia a
where	(nvl(nr_seq_hist_item_p,0) = 0 or b.nr_sequencia = b.nr_sequencia)
and	a.nr_sequencia		= b.nr_seq_guia
and	(nvl(nr_seq_hist_guia_p,0) = 0 or a.nr_sequencia = a.nr_sequencia)
and	a.nr_seq_lote_hist	= nr_seq_lote_hist_p;

begin

open	c01;
loop
fetch	c01 into
	nr_seq_hist_item_w,
	vl_adicional_w,
	vl_amenor_w,
	vl_glosa_w,
	vl_pago_w,
	nr_seq_guia_w;
exit	when (c01%notfound) or (nvl(ds_retorno_w,'X') <> 'X');

	select	count(*) qt_participante,
		nvl(sum(a.vl_adicional),0) vl_adicional_partic,
		nvl(sum(a.vl_amenor),0) vl_amenor_partic,
		nvl(sum(a.vl_glosa),0) vl_glosa_partic,
		nvl(sum(a.vl_pago),0) vl_pago_partic
	into	qt_participante_w,
		vl_adicional_partic_w,
		vl_amenor_partic_w,
		vl_glosa_partic_w,
		vl_pago_partic_w
	from	grg_proc_partic a
	where	a.nr_seq_hist_item	= nr_seq_hist_item_w;

	/* consistir os participantes do procedimento */
	if	(qt_participante_w	> 0) then       

		if	(vl_adicional_partic_w <> vl_adicional_w) then
			ds_retorno_w	:= WHEB_MENSAGEM_PCK.get_texto(818046, 'VL_ADICIONAL_W=' || vl_adicional_w || ';VL_ADICIONAL_PARTIC_W=' || vl_adicional_partic_w || ';NR_SEQ_GUIA_W=' || nr_seq_guia_w || ';NR_SEQ_HIST_ITEM_W=' || nr_seq_hist_item_w);
		elsif	(vl_amenor_partic_w <> vl_amenor_w) then
			ds_retorno_w	:=	WHEB_MENSAGEM_PCK.get_texto(818047, 'VL_ADICIONAL_W=' || vl_amenor_w || ';VL_ADICIONAL_PARTIC_W=' || vl_amenor_partic_w || ';NR_SEQ_GUIA_W=' || nr_seq_guia_w || ';NR_SEQ_HIST_ITEM_W=' || nr_seq_hist_item_w);
		elsif	(vl_glosa_partic_w <> vl_glosa_w) then
			ds_retorno_w	:=	WHEB_MENSAGEM_PCK.get_texto(818048, 'VL_ADICIONAL_W=' || vl_glosa_w || ';VL_ADICIONAL_PARTIC_W=' || vl_glosa_partic_w || ';NR_SEQ_GUIA_W=' || nr_seq_guia_w || ';NR_SEQ_HIST_ITEM_W=' || nr_seq_hist_item_w);
		elsif	(vl_pago_partic_w <> vl_pago_w) then
			ds_retorno_w	:=	WHEB_MENSAGEM_PCK.get_texto(818049, 'VL_ADICIONAL_W=' || vl_pago_w || ';VL_ADICIONAL_PARTIC_W=' || vl_pago_partic_w || ';NR_SEQ_GUIA_W=' || nr_seq_guia_w || ';NR_SEQ_HIST_ITEM_W=' || nr_seq_hist_item_w);
		end if;

	end if;

end	loop;
close	c01;

ds_retorno_p	:= ds_retorno_w;

end CONSISTIR_ITEM_GUIA_GRG;
/
