create or replace procedure
set_adv_appointment_value(insert_or_update_p	varchar2,
			schedule_type_p	varchar2,
			nr_seq_p	number,
			ie_adv_appointment_p	varchar2)
is
begin

	if (schedule_type_p = 'C' and insert_or_update_p = 'I') then
	
		insert into agenda_consulta_adic
			(nr_seq_agenda,
			nm_usuario,
			dt_atualizacao,
			ie_adv_appointment)
		values	(nr_seq_p,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			ie_adv_appointment_p);
		commit;
		
	elsif (schedule_type_p = 'C' and insert_or_update_p = 'U') then
	
		update	agenda_consulta_adic 
		set	ie_adv_appointment = ie_adv_appointment_p
		where	nr_seq_agenda = nr_seq_p;
		commit;

	elsif (schedule_type_p = 'E' and insert_or_update_p = 'I') then

		insert into agenda_paciente_auxiliar
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ie_adv_appointment)
		values	(nr_seq_p,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			ie_adv_appointment_p);
		commit;

	elsif (schedule_type_p = 'E' and insert_or_update_p = 'U') then

		update	agenda_paciente_auxiliar
		set	ie_adv_appointment = ie_adv_appointment_p
		where	nr_sequencia = nr_seq_p;
		commit;

	end if;

end set_adv_appointment_value;
/