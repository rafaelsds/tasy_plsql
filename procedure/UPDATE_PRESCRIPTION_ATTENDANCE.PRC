CREATE OR REPLACE PROCEDURE UPDATE_PRESCRIPTION_ATTENDANCE (
    nr_sequencia_p in MATERIAL_ATEND_PACIENTE.nr_sequencia%Type,
    nr_prescricao_p in MATERIAL_ATEND_PACIENTE.nr_prescricao%Type) is
begin
    update MATERIAL_ATEND_PACIENTE
    set nr_prescricao = nr_prescricao_p
    where nr_sequencia = nr_sequencia_p;
    commit;
end UPDATE_PRESCRIPTION_ATTENDANCE;
/
