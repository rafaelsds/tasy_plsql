create or replace
procedure gerar_prescricoes_paciente(
		cd_protocolo_p			number,
		nr_atendimento_p		number,
		ie_liberado_p			varchar2,		
		ds_lista_atend_pac_p	varchar2,
		cd_estabelecimento_p	number,
		nm_usuario_p			varchar2,
		ie_gerar_todas_prescr_p	varchar2 default 'N') is

ie_situacao_w		varchar2(1);
ds_lista_atend_pac_w	varchar2(2000);
nr_seq_paciente_w		number(10,0);
nr_pos_virgula_w		number(10,0);
ds_retorno_ww		varchar2(255);
		
begin
if	(ds_lista_atend_pac_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	if	(ds_lista_atend_pac_p is not null) and
		(cd_estabelecimento_p is not null) then
		begin
		ds_lista_atend_pac_w	:= ds_lista_atend_pac_p;
		
		while (ds_lista_atend_pac_w is not null) loop
			begin
			nr_pos_virgula_w	:= instr(ds_lista_atend_pac_w,',');
			if	(nr_pos_virgula_w > 0) then
				begin
				nr_seq_paciente_w	:= to_number(substr(ds_lista_atend_pac_w,1,nr_pos_virgula_w-1));
				ds_lista_atend_pac_w	:= substr(ds_lista_atend_pac_w,nr_pos_virgula_w+1,length(ds_lista_atend_pac_w));
				if	(nr_seq_paciente_w > 0) then
					begin
					gerar_prescricao_paciente(nr_seq_paciente_w,nm_usuario_p,cd_estabelecimento_p,nr_atendimento_p,'',ie_liberado_p,ds_retorno_ww, ie_gerar_todas_prescr_p);
					end;
				end if;
				end;
			else
				begin
				ds_lista_atend_pac_w	:= null;
				end;
			end if;
			end;
		end loop;
		end;
	end if;
	end;
end if;
commit;
end gerar_prescricoes_paciente;
/