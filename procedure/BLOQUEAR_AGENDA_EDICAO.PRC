create or replace
procedure BLOQUEAR_AGENDA_EDICAO (nr_seq_agenda_p		number,
								ie_bloq_desbloqueia_p 	varchar2,
								nm_usuario_p			varchar2) IS
/*
ie_bloq_desbloqueia_p:
B = Bloqueia
D =Desbloqueia via fonte
T =Desbloqueia via trigger
*/

nr_serial_session_w				v$session.serial#%type;
nr_sid_session_w				v$session.sid%type;
ie_bloq_simult_agenda_cir_w		parametro_agenda.ie_bloq_simult_agenda_cir%type;

begin

ie_bloq_simult_agenda_cir_w := obter_parametro_agenda(wheb_usuario_pck.get_cd_estabelecimento, 'IE_BLOQ_SIMULT_AGENDA_CIR', 'N');

if (nvl(ie_bloq_desbloqueia_p, 'N') = 'B'
	and (nvl(ie_bloq_simult_agenda_cir_w,'N') = 'S')) then

	select	max(serial#),
			max(sid)
	into	nr_serial_session_w,
			nr_sid_session_w
	from	v$session
	where	audsid = (select userenv('sessionid') from dual);
	
	insert into agenda_paciente_edicao
			(nr_seq_agenda,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_serial_session,
			nr_sid_session)
	values (nr_seq_agenda_p,
			sysdate,
			nm_usuario_p,
			nr_serial_session_w,
			nr_sid_session_w);

	commit;
elsif (nvl(ie_bloq_desbloqueia_p, 'N') = 'D') then

	delete	agenda_paciente_edicao
	where	nr_seq_agenda = nr_seq_agenda_p;

	commit;

elsif (nvl(ie_bloq_desbloqueia_p, 'N') = 'T'
	and (nvl(ie_bloq_simult_agenda_cir_w,'N') = 'S')) then /*Desbloqueio via trigger - NAO COLOCAR COMMIT*/

	delete	agenda_paciente_edicao
	where	nr_seq_agenda = nr_seq_agenda_p
	and		upper(nm_usuario_nrec) = upper(nm_usuario_p);

end if;

end BLOQUEAR_AGENDA_EDICAO;
/