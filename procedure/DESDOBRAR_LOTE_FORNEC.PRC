create or replace
procedure desdobrar_lote_fornec(nr_sequencia_p	number,
				qt_material_p	number,
				cd_local_estoque_p	number,
				ie_calculo_validade_p	varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p	varchar2,
				nr_seq_lote_p	out number) is

nr_seq_lote_w			number(10);	

nr_seq_lote_origem_w		number(10);
dt_validade_origem_w		date;
cd_material_w			number(6);

cd_operacao_entrada_w		number(3);
cd_operacao_saida_w		number(3);
			
qt_material_w			number(13,4);

dt_validade_w			date;

cd_cgc_fornec_w			varchar2(14);
ds_lote_fornec_w		varchar2(20);
ie_validade_indeterminada_w	varchar2(1);
nr_digito_verif_w		number(1);

nr_sequencia_w			number(10);
cd_unidade_medida_w		varchar2(30);
cd_unidade_medida_estoque_w	varchar2(30);

cd_conta_contabil_w		varchar2(20);
cd_centro_custo_w		number(8);
cd_setor_atendimento_w		number(5) := obter_Setor_ativo;
cd_local_estoque_w		number(4);
nm_computador_w			varchar2(80);
nr_seq_computador_w		number(10);
qt_estoque_lote_w		number(13,4);
nr_item_nf_w			number(5);
nr_lote_producao_w		number(10);
nr_nota_fiscal_w		varchar2(255);
nr_sequencia_nf_w		number(10);

begin
nr_seq_lote_origem_w := nr_sequencia_p;

qt_estoque_lote_w := obter_saldo_lote_fornec(nr_seq_lote_origem_w);
	
if	(qt_estoque_lote_w < qt_material_p) then
	wheb_mensagem_pck.exibir_mensagem_abort(190984);
end if;

nm_computador_w		:= upper(substr(wheb_usuario_pck.get_nm_estacao,1,80));
cd_local_estoque_w	:= nvl(cd_local_estoque_p,0);

if	(cd_local_estoque_w = 0) then
	begin
	begin
	select	nr_sequencia,
		cd_setor_atendimento
	into	nr_seq_computador_w,
		cd_setor_atendimento_w
	from	COMPUTADOR
	where	nm_computador_pesquisa  = padronizar_nome(upper(nm_computador_w));
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(184568,'NM_COMPUTADOR='||nm_computador_w);
		/*r.aise_application_error(-20011,'Computador [' || nm_computador_w || '] n�o cadastrado corretamente!' );*/
	end;	

	select	nvl(max(cd_local_estoque),0)
	into	cd_local_estoque_w
	from   	local_estoque_computador
	where  	nr_seq_computador = nr_seq_computador_w;

	if	(cd_local_estoque_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(184569,'NM_COMPUTADOR='||nm_computador_w);
		/*r.aise_application_error(-20011,'Computador [' || nm_computador_w || '] n�o possui local de estoque vinculado!' );*/
	end if;
	end;
end if;

select	cd_material,
	dt_validade,
	ie_validade_indeterminada,
	cd_cgc_fornec,
	ds_lote_fornec,
	nr_item_nf,
	nr_lote_producao,
	nr_nota_fiscal,
	nr_sequencia_nf
into	cd_material_w,
	dt_validade_origem_w,
	ie_validade_indeterminada_w,
	cd_cgc_fornec_w,
	ds_lote_fornec_w,
	nr_item_nf_w,
	nr_lote_producao_w,
	nr_nota_fiscal_w,
	nr_sequencia_nf_w
from	material_lote_fornec
where	nr_sequencia = nr_seq_lote_origem_w;

select	cd_unidade_medida_estoque
into	cd_unidade_medida_estoque_w
from	material
where	cd_material = cd_material_w;

sup_verifica_cria_operacao(cd_estabelecimento_p, 11, nm_usuario_p, cd_operacao_entrada_w);
sup_verifica_cria_operacao(cd_estabelecimento_p, 12, nm_usuario_p, cd_operacao_saida_w);
	
if	(ie_calculo_validade_p = 'S') and (dt_validade_origem_w is not null) then
	dt_validade_w := obter_val_lote_prod(cd_estabelecimento_p, cd_material_w, dt_validade_origem_w);
else
	dt_validade_w := dt_validade_origem_w;
end if;

select	material_lote_fornec_seq.nextval
into	nr_seq_lote_w
from	dual;

nr_digito_verif_w	:= calcula_digito('MODULO11',nr_seq_lote_w);

insert into material_lote_fornec(
	nr_sequencia,
	cd_material,
	nr_digito_verif,
	dt_atualizacao,
	nm_usuario,
	ds_lote_fornec,
	dt_validade,
	cd_cgc_fornec,
	qt_material,
	cd_estabelecimento,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_marca,
	ie_origem_lote,
	ie_validade_indeterminada,
	ie_situacao,
	ie_bloqueio,
	ds_observacao,
	dt_validade_original,
	dt_fabricacao,
	ds_localizacao,
	nr_seq_local,
	nr_item_nf,
	nr_lote_producao,
	nr_nota_fiscal,
	nr_sequencia_nf)
select	nr_seq_lote_w,
	cd_material,
	calcula_digito('Modulo11', nr_seq_lote_w),
	sysdate,
	nm_usuario_p,
	ds_lote_fornec,
	dt_validade_w,
	cd_cgc_fornec,
	qt_material_p,
	cd_estabelecimento,
	sysdate,
	nm_usuario_p,
	nr_seq_marca,
	'D',
	ie_validade_indeterminada,
	'A',
	'N',
	ds_observacao,
	dt_validade_origem_w,
	dt_fabricacao,
	ds_localizacao,
	nr_seq_local,
	nr_item_nf_w,
	nr_lote_producao_w,
	nr_nota_fiscal_w,
	nr_sequencia_nf_w
from	material_lote_fornec
where	nr_sequencia = nr_sequencia_p;

update	material_lote_fornec
set	qt_material = qt_material - qt_material_p
where	nr_sequencia = nr_sequencia_p;
	
/*Deve ser a sa�da primeiro para consistir caso n�o tenha saldo*/
insert into movimento_estoque(
	nr_movimento_estoque,
	cd_estabelecimento,
	cd_local_estoque,
	dt_movimento_estoque,
	cd_operacao_estoque,
	cd_acao,
	cd_material,
	dt_mesano_referencia,
	qt_movimento,
	dt_atualizacao,
	nm_usuario,
	ie_origem_documento,
	nr_documento,
	nr_sequencia_item_docto,
	cd_unidade_medida_estoque,
	cd_setor_atendimento,
	qt_estoque,
	cd_centro_custo,
	cd_unidade_med_mov,
	cd_fornecedor,
	nr_seq_tab_orig,
	nr_seq_lote_fornec,
	cd_lote_fabricacao,
	dt_validade,		
	cd_conta_contabil)
values(	movimento_estoque_seq.nextval,
	cd_estabelecimento_p,
	cd_local_estoque_w,
	sysdate,
	cd_operacao_saida_w,
	'1',
	cd_material_w,
	sysdate,
	qt_material_p,
	sysdate,
	nm_usuario_p,
	'18',
	null,
	null,
	cd_unidade_medida_estoque_w,
	cd_setor_atendimento_w,
	qt_material_p,
	null,
	cd_unidade_medida_estoque_w,
	null,
	null,
	nr_sequencia_p,
	ds_lote_fornec_w,
	dt_validade_origem_w,
	null);
	
insert into movimento_estoque(
	nr_movimento_estoque,
	cd_estabelecimento,
	cd_local_estoque,
	dt_movimento_estoque,
	cd_operacao_estoque,
	cd_acao,
	cd_material,
	dt_mesano_referencia,
	qt_movimento,
	dt_atualizacao,
	nm_usuario,
	ie_origem_documento,
	nr_documento,
	nr_sequencia_item_docto,
	cd_unidade_medida_estoque,
	cd_setor_atendimento,
	qt_estoque,
	cd_centro_custo,
	cd_unidade_med_mov,
	cd_fornecedor,
	nr_seq_tab_orig,
	nr_seq_lote_fornec,
	cd_lote_fabricacao,
	dt_validade,		
	cd_conta_contabil)
values(	movimento_estoque_seq.nextval,
	cd_estabelecimento_p,
	cd_local_estoque_w,
	sysdate,
	cd_operacao_entrada_w,
	'1',
	cd_material_w,
	sysdate,
	qt_material_p,
	sysdate,
	nm_usuario_p,
	'18',
	null,
	null,
	cd_unidade_medida_estoque_w,
	cd_setor_atendimento_w,
	qt_material_p,
	null,
	cd_unidade_medida_estoque_w,
	null,
	null,
	nr_seq_lote_w,
	ds_lote_fornec_w,
	dt_validade_w,
	null);

commit;

nr_seq_lote_p := nr_seq_lote_w;
end desdobrar_lote_fornec;
/
