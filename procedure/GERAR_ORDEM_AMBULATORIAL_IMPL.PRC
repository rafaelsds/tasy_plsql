create or replace
PROCEDURE Gerar_ordem_ambulatorial_Impl(
          dt_referencia_p     Date,
          cd_estabelecimento_p    Number,
          ie_gerar_ordem_pac_chegada_p  varchar2,
          nm_usuario_p      Varchar2,
          ie_gera_inconsistencia_p  varchar2 default 'N') IS

nr_seq_atendimento_w    Number(10);
nr_seq_paciente_w   Number(10);
cd_medico_resp_w    Varchar2(10);
cd_pessoa_fisica_w    Varchar2(10);
nr_prescricao_w     Number(14);
dt_prevista_w     Date;
nr_sequencia_w      Number(10);
nr_ordem_prod_w     Number(10);
cd_estabelecimento_ww   Number(4);
cd_material_w     Number(6);
nr_seq_prescr_w     Number(10);
ie_via_aplicacao_w    Varchar2(5);
cd_farmaceutico_w   Varchar2(10);
cd_farmaceutico_ww    Varchar2(10);
ds_precaucao_ordem_w    Varchar2(255);
ds_recomendacao_w   Varchar2(2000);
cd_setor_atendimento_w    Number(5);
qt_min_aplicacao_w    Number(4,0);
qt_hora_aplicacao_w   Number(3,0);
ie_tipo_dosagem_w   Varchar2(3);
qt_dose_diluente_w    Number(15,4);
vl_gotejo_w     Number(15,4);
nr_agrupamento_w    Number(07,1);
nr_agrup_ant_w      Number(07,1) := 0;
cd_material_medic_w   Number(6);
nr_sequencia_medic_w    Number(6);
qt_dose_medic_w     Number(15,4);
cd_unidade_medida_dose_w  Varchar2(30);
nr_seq_item_prescr_w    Number(10);
nr_ocorrencia_w     Number(15,4);
cd_forma_armaz_w    Varchar2(255);
cd_estagio_armaz_w    Varchar2(255);
qt_estabilidade_w   Number(14,2);
ie_tempo_estab_w    Varchar2(3);
qt_hora_validade_w    Number(5,0);
qt_min_validade_w   Number(5);
qt_dias_util_w      Number(3);
ie_exige_liberacao_w    Varchar2(1);
ie_generico_w     varchar2(1);
ie_gerar_reconstituinte_w varchar2(1);
qt_peso_w     number(6,3);
qt_superf_corporea_w    number(7,3);
ie_libera_enfer_w   varchar2(1);
nr_seq_ordem_prod_w   number(10);
ie_usuario_manipulador_w  varchar2(1);
ie_restringir_ordem_w   varchar2(1);
ie_pre_medicacao_w    varchar2(1);
ie_agrupador_w      number(4);
ie_agrup_ant_w      number(4);
ie_grava_aspirado_w   varchar2(1);
ie_libera_medic_w   varchar2(1);
qt_volume_aspirado_w    number(15,2);
ie_exibe_apto_w     varchar2(1);
ie_permite_acm_w    varchar2(1);
ie_estabe_tratamento_w    varchar2(1);
ie_bomba_infusao_w    Varchar2(1);
nr_sequencia_solucao_w  number(10);
dt_chagada_w      date;
ie_exige_lib_farma_w  varchar2(1);
qt_mat_regra_w      number(10);
nr_atendimento_w  number(10);
ie_tipo_atendimento_w number(3);
ie_via_aplicacao_got_w  varchar2(5);
ie_aplica_gotejamento_w varchar2(1);
ie_medic_paciente_w   varchar2(1);
nr_seq_mat_hor_w number(10);
ie_prescr_anterior_w  varchar2(1);

Cursor C01 is
  select  a.nr_seq_atendimento,
    a.nr_seq_paciente,
    a.nr_prescricao,
    nvl(a.dt_real,dt_prevista),
    a.ie_exige_liberacao,
    a.cd_estabelecimento,
    a.dt_chegada,
    a.nr_atendimento,
    'N'
  from  paciente_atendimento a
  where trunc(nvl(a.dt_real,a.dt_prevista)) = trunc(dt_referencia_p)
  and obter_se_prescr_liberada(a.nr_seq_atendimento, cd_estabelecimento_p, nm_usuario_p) = 'S'
  AND   ((a.cd_estabelecimento = cd_estabelecimento_p) OR (ie_restringir_ordem_w = 'N'))
  and ((a.dt_chegada is not null) or (ie_gerar_ordem_pac_chegada_p = 'N'))
  and   ((a.dt_apto is not null) or (nvl(ie_exibe_apto_w,'N') = 'N'))
  and   a.dt_suspensao is null
  and a.dt_cancelamento is null
  and obter_se_paciente_obito(obter_pessoa_atendimento(a.nr_atendimento,'C')) <> 'A'
  and ((ie_gera_inconsistencia_p = 'N') or
     (not exists (select 1 from w_inconsistencias_onco d  where d.nr_seq_atendimento = a.nr_seq_atendimento) ))
  union
  select  a.nr_seq_atendimento,
    a.nr_seq_paciente,
    b.nr_prescricao,
    nvl(a.dt_real,a.dt_prevista),
    a.ie_exige_liberacao,
    a.cd_estabelecimento,
    a.dt_chegada,
    a.nr_atendimento,
    'S'
  from  paciente_atendimento a,
    prescr_medica b
  where trunc(nvl(a.dt_real,a.dt_prevista)) = trunc(dt_referencia_p)
  and   b.nr_prescricao_anterior = a.nr_prescricao
  and   ((a.cd_estabelecimento = cd_estabelecimento_p) or (ie_restringir_ordem_w = 'N'))
  and ((a.dt_chegada is not null) or (ie_gerar_ordem_pac_chegada_p = 'N'))
  and   ((a.dt_apto is not null) or (nvl(ie_exibe_apto_w,'N') = 'N'))
  and   a.dt_suspensao is null
  and (obter_se_paciente_obito(obter_pessoa_atendimento(a.nr_atendimento,'C')) <> 'A')
  and ((ie_gera_inconsistencia_p = 'N') or
     (not exists (select 1 from w_inconsistencias_onco d  where d.nr_seq_atendimento = a.nr_seq_atendimento) ))
  and a.dt_cancelamento is null
	and	((b.dt_liberacao_farmacia is not null) or (nvl(ie_exige_lib_farma_w,'N') = 	'N'));

Cursor C02 is
  select  decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
    a.nr_sequencia,
    a.ie_via_aplicacao,
    substr(b.ds_precaucao_ordem,1,255),
    substr(a.ds_observacao,1,255),
    decode(nvl(a.qt_min_aplicacao,0),0,decode(a.ie_agrupador,4,nvl(Obter_tempo_aplic_sol(nr_prescricao_w,a.nr_sequencia_solucao,'M'),0),0),nvl(a.qt_min_aplicacao,0)),
    decode(nvl(a.qt_hora_aplicacao,0),0,decode(a.ie_agrupador,4,nvl(Obter_tempo_aplic_sol(nr_prescricao_w,a.nr_sequencia_solucao,'H'),0),0),nvl(a.qt_hora_aplicacao,0)),
    decode(obter_um_rep_dispositivo(a.ie_bomba_infusao,a.ie_agrupador),null,decode(a.ie_bomba_infusao,'S','mlh','gtm'),obter_um_rep_dispositivo(a.ie_bomba_infusao,a.ie_agrupador)),
    a.nr_agrupamento,
    DECODE(a.ie_agrupador,4,NVL(obter_etapa_soluc_quimio(nr_prescricao_w,a.nr_sequencia_solucao),1), a.nr_ocorrencia),
    a.qt_dias_util,
    a.ie_pre_medicacao,
    a.ie_agrupador,
    a.ie_bomba_infusao,
    a.nr_sequencia_solucao,
    nvl(a.ie_medicacao_paciente, 'N')
  from  material b,
    prescr_material a
  where a.nr_prescricao = nr_prescricao_w
  and a.cd_material = b.cd_material
  and a.nr_seq_ordem_prod is null
  and a.ie_via_aplicacao is not null
  and nvl(a.ie_suspenso,'N')  = 'N'
  and   a.dt_suspensao is null
  and (((b.ie_mistura = 'S') and (nvl(a.ie_medicacao_paciente, 'N') <> 'S')) or
     ((b.ie_mistura = 'R') and (obter_se_gera_ordem(a.ie_via_aplicacao,a.cd_intervalo, a.qt_dose, a.cd_material, a.cd_unidade_medida,'T', nvl(a.ie_medicacao_paciente, 'N'),nr_atendimento_w) = 'S')) or
     ((b.ie_mistura = 'I') and (ie_tipo_atendimento_w = 1) and (nvl(a.ie_medicacao_paciente, 'N') <> 'S')))
  and   a.ie_agrupador in (1,4)
  and   ((nvl(IE_ACM,'N') = 'N') or (nvl(ie_permite_acm_w,'S') = 'S'))
  and   (obter_se_setor_ordem(ie_acm,obter_setor_atendimento(obter_atendimento_prescr(nr_prescricao))) = 'S')
  and   (((qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'S') and (dt_chagada_w is not null))
      or (qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'N'))
  order by a.nr_agrupamento, a.nr_sequencia;

Cursor C03 is
  select  decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),decode(ie_generico_w,'C',obter_medic_regra_onc(a.cd_material,b.cd_material_generico,nr_atendimento_w,ie_tipo_material,b.nr_seq_ficha_tecnica),a.cd_material)),
    a.nr_sequencia,
    a.qt_dose,
    a.cd_unidade_medida_dose
  from  material b,
    prescr_material a
  where a.nr_prescricao   = nr_prescricao_w
  and a.nr_agrupamento  = nr_agrupamento_w
  and   a.ie_agrupador    = ie_agrupador_w
  and a.cd_material   = b.cd_material
  and   a.dt_suspensao is null
  and a.ie_agrupador    in (1,4)
  and   (obter_se_setor_ordem(ie_acm,obter_setor_atendimento(obter_atendimento_prescr(nr_prescricao))) = 'S')
  and   ((nvl(IE_ACM,'N') = 'N') or (nvl(ie_permite_acm_w,'S') = 'S'))
  and a.cd_unidade_medida_dose is not null
  and   (((qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'S') and (dt_chagada_w is not null))
      or (qt_obter_regra_gerac_ordem(a.cd_material, a.ie_via_aplicacao, a.qt_dose, a.cd_unidade_medida_dose) = 'N'));
begin

Obter_Param_Usuario(3130,2,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,cd_farmaceutico_w);
Obter_Param_Usuario(3130,22,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_generico_w);
Obter_Param_Usuario(3130,40,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gerar_reconstituinte_w);
Obter_Param_Usuario(3130,50,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_libera_enfer_w);
Obter_Param_Usuario(3130,142,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_usuario_manipulador_w);
Obter_Param_Usuario(3130,165,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_restringir_ordem_w);
Obter_Param_Usuario(3130,176,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_grava_aspirado_w);
Obter_Param_Usuario(3130,203,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_libera_medic_w);
Obter_Param_Usuario(3130,204,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_exibe_apto_w);
Obter_Param_Usuario(3130,261,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_permite_acm_w);
Obter_Param_Usuario(3130,331,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_estabe_tratamento_w);
Obter_Param_Usuario(3130,429,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_exige_lib_farma_w);

if  (cd_farmaceutico_w is not null) then
  select  max(cd_pessoa_fisica)
  into  cd_farmaceutico_ww
  from  pessoa_fisica
  where   cd_pessoa_fisica = cd_farmaceutico_w;

  if  (nvl(cd_farmaceutico_ww,0) = 0) then
    wheb_mensagem_pck.Exibir_Mensagem_Abort(192283);
  end if;
end if;

  select  nvl(vl_parametro, vl_parametro_padrao)
  into  cd_estagio_armaz_w
  from  funcao_parametro
  where cd_funcao = 3130
  and nr_sequencia  = 10;

  select  nvl(vl_parametro, vl_parametro_padrao)
  into  cd_forma_armaz_w
  from  funcao_parametro
  where cd_funcao = 3130
  and nr_sequencia  = 11;

  OPEN C01;
  LOOP
  FETCH C01 into
    nr_seq_atendimento_w,
    nr_seq_paciente_w,
    nr_prescricao_w,
    dt_prevista_w,
    ie_exige_liberacao_w,
    cd_estabelecimento_ww,
    dt_chagada_w,
    nr_atendimento_w,
    ie_prescr_anterior_w;
  exit when C01%notfound;
    begin

    select  max(cd_pessoa_fisica),
      max(cd_medico_resp),
      max(cd_setor_atendimento),
      max(qt_peso),
      max(round(obter_superficie_corp_red_ped(qt_peso, qt_altura, qt_redutor_sc, cd_pessoa_fisica, nm_usuario),2))
    into  cd_pessoa_fisica_w,
      cd_medico_resp_w,
      cd_setor_atendimento_w,
      qt_peso_w,
      qt_superf_corporea_w
    from  paciente_setor
    where nr_seq_paciente = nr_seq_paciente_w;

    if (ie_prescr_anterior_w = 'N') then
      update  prescr_medica
      set dt_liberacao    = decode(ie_libera_enfer_w,'S',sysdate,null),
        dt_liberacao_medico = decode(ie_libera_medic_w,'S',sysdate)
      where nr_prescricao   = nr_prescricao_w
      and dt_liberacao is null
      and dt_liberacao_medico is null;
    end if;

    nr_agrup_ant_w  := 0;
    ie_agrup_ant_w  := 0;

    select  max(ie_tipo_atendimento)
    into  ie_tipo_atendimento_w
    from  atendimento_paciente
    where nr_atendimento  = nr_atendimento_w;

    OPEN C02;
    LOOP
    FETCH C02 into
      cd_material_w,
      nr_seq_prescr_w,
      ie_via_aplicacao_w,
      ds_precaucao_ordem_w,
      ds_recomendacao_w,
      qt_min_aplicacao_w,
      qt_hora_aplicacao_w,
      ie_tipo_dosagem_w,
      nr_agrupamento_w,
      nr_ocorrencia_w,
      qt_dias_util_w,
      ie_pre_medicacao_w,
      ie_agrupador_w,
      ie_bomba_infusao_w,
      nr_sequencia_solucao_w,
      ie_medic_paciente_w;
    exit when C02%notfound;
      begin
      commit;
      if  (nr_agrup_ant_w <> nr_agrupamento_w) or
        ((nr_agrup_ant_w = nr_agrupamento_w) and
         (ie_agrup_ant_w <> ie_agrupador_w)) then
        begin

        select  nvl(min(qt_estabilidade),0),
          nvl(min(ie_tempo_estab),'I')
        into  qt_estabilidade_w,
          ie_tempo_estab_w
        from  material_armazenamento
        where cd_material = cd_material_w
        and nr_seq_estagio  = cd_estagio_armaz_w
        and nr_seq_forma  = cd_forma_armaz_w
        and nvl(cd_estabelecimento,nvl(cd_estabelecimento_p,0)) = nvl(cd_estabelecimento_p,0);

        qt_hora_validade_w  := 0;
        qt_min_validade_w := 0;

        if  (ie_tempo_estab_w = 'H') then
          qt_hora_validade_w  := qt_estabilidade_w;
        elsif (ie_tempo_estab_w = 'D') then
          qt_hora_validade_w  := qt_estabilidade_w * 24;
        elsif (ie_tempo_estab_w = 'M') then
          qt_hora_validade_w  := qt_estabilidade_w * 30 * 24;
        elsif (ie_tempo_estab_w = 'A') then
          qt_hora_validade_w  := qt_estabilidade_w * 365 * 24;
        end if;

        nr_agrup_ant_w  := nr_agrupamento_w;
        ie_agrup_ant_w  := ie_agrupador_w;

        select  nvl(max(nr_seq_ordem_prod),0)
        into  nr_seq_ordem_prod_w
        from  prescr_material
        where nr_prescricao = nr_prescricao_w
        and nr_sequencia  = nr_seq_prescr_w;

        while (nr_ocorrencia_w > 0) and
          (nr_seq_ordem_prod_w = 0) LOOP
          begin

          nr_ocorrencia_w := nr_ocorrencia_w - 1;

          select  can_ordem_prod_seq.nextval
          into  nr_sequencia_w
          from  dual;

          nr_ordem_prod_w := nr_sequencia_w;

          if (ie_agrupador_w = 4) then
            select  nvl(max(ie_via_aplicacao),ie_via_aplicacao_w)
            into  ie_via_aplicacao_w
            from  prescr_solucao
            where nr_prescricao = nr_prescricao_w
            and nr_seq_solucao = nr_sequencia_solucao_w;

          end if;

          insert into can_ordem_prod(
            nr_sequencia,
            cd_estabelecimento,
            dt_atualizacao,
            nm_usuario,
            cd_pessoa_fisica,
            dt_prevista,
            ie_via_aplicacao,
            qt_hora_validade,
            qt_min_validade,
            qt_hora_aplic,
            qt_min_aplic,
            cd_medico_resp,
            cd_farmaceutico,
            cd_manipulador,
            cd_auxiliar,
            dt_inicio_preparo,
            dt_entrega_setor,
            dt_administracao,
            nr_seq_atendimento,
            nr_prescricao,
            dt_fim_preparo,
            ie_tipo_dosagem,
            ds_precaucao,
            ie_cancelada,
            ds_orientacao_adm,
            cd_setor_paciente,
            ie_conferido,
            qt_dias_util,
            ie_suspensa,
            qt_peso,
            qt_superf_corporea,
            dt_geracao_ordem,
            ie_pre_medicacao,
            ie_bomba_infusao,
            ie_medic_paciente)
          Values(
            nr_sequencia_w,
            decode(nvl(ie_estabe_tratamento_w,'N'),'N',cd_estabelecimento_p,cd_estabelecimento_ww),
            sysdate,
            nm_usuario_p,
            cd_pessoa_fisica_w,
            dt_prevista_w,
            ie_via_aplicacao_w,
            nvl(qt_hora_validade_w,0),
            nvl(qt_min_validade_w,0),
            nvl(qt_hora_aplicacao_w,0),
            nvl(qt_min_aplicacao_w,0),
            cd_medico_resp_w,
            cd_farmaceutico_w,
            decode(ie_usuario_manipulador_w,'S',Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C'),null),
            null,
            null,
            null,
            null,
            nr_seq_atendimento_w,
            nr_prescricao_w,
            null,
            ie_tipo_dosagem_w,
            ds_precaucao_ordem_w,
            'N',
            ds_recomendacao_w,
            cd_setor_atendimento_w,
            'N',
            qt_dias_util_w,
            ie_exige_liberacao_w,
            qt_peso_w,
            qt_superf_corporea_w,
            sysdate,
            ie_pre_medicacao_w,
            ie_bomba_infusao_w,
            ie_medic_paciente_w);

          commit;

          update  prescr_material
          set nr_seq_ordem_prod = nr_ordem_prod_w
          where nr_prescricao   = nr_prescricao_w
          and nr_agrupamento    = nr_agrupamento_w
          and   ie_agrupador    = ie_agrupador_w;

          commit;

          OPEN C03;
          LOOP
          FETCH C03 into
            cd_material_medic_w,
            nr_sequencia_medic_w,
            qt_dose_medic_w,
            cd_unidade_medida_dose_w;
          exit when C03%notfound;
            begin
            select  can_ordem_item_prescr_seq.nextval
            into  nr_seq_item_prescr_w
            from  dual;

            select  count(*)
            into  qt_mat_regra_w
            from  regra_geracao_ordem_tot
            where cd_material = cd_material_medic_w;

            if  (qt_mat_regra_w > 0) then
              select  qt_material
              into  qt_dose_medic_w
              from  prescr_material
              where nr_sequencia  = nr_sequencia_medic_w
              and   nr_prescricao = nr_prescricao_w;
            end if;

            gravar_log_tasy(0207,   'nr_sequencia_medic_w: '||nr_sequencia_medic_w||chr(13)||
                        'nr_prescricao_w: '||nr_prescricao_w||chr(13)||
                        'cd_material_medic_w: '||cd_material_medic_w, 'log');
            select min  (a.nr_sequencia)
            into 	nr_seq_mat_hor_w
            from    prescr_mat_hor a
            where   a.nr_seq_material = nr_sequencia_medic_w
			and     a.dt_lib_horario is not null
            and     a.nr_prescricao = nr_prescricao_w
            and   a.cd_material = cd_material_medic_w
            and     not exists (select 1 from can_ordem_item_prescr b
            where   b.nr_seq_mat_hor = a.nr_sequencia);


            insert into can_ordem_item_prescr(
              nr_sequencia,
              dt_atualizacao,
              nm_usuario,
              nr_seq_ordem,
              cd_material,
              qt_dose,
              cd_unidade_medida,
              nr_prescricao,
              nr_seq_prescricao,
              nr_sequencia_diluente,
              nm_usuario_nrec,
              dt_atualizacao_nrec,
              nr_seq_mat_hor)
            values( nr_seq_item_prescr_w,
              sysdate,
              nm_usuario_p,
              nr_sequencia_w,
              cd_material_medic_w,
              nvl(qt_dose_medic_w,0),
              cd_unidade_medida_dose_w,
              nr_prescricao_w,
              nr_sequencia_medic_w,
              null,
              nm_usuario_p,
              sysdate,
              nr_seq_mat_hor_w);

            insert into can_ordem_item_prescr(
              nr_sequencia,
              dt_atualizacao,
              nm_usuario,
              nr_seq_ordem,
              cd_material,
              qt_dose,
              cd_unidade_medida,
              nr_prescricao,
              nr_seq_prescricao,
              nr_sequencia_diluente,
              ie_agrupador,
              nm_usuario_nrec,
              dt_atualizacao_nrec,
			  nr_seq_mat_hor)
            select  can_ordem_item_prescr_seq.nextval,
              sysdate,
              nm_usuario_p,
              nr_sequencia_w,
              decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
              nvl(a.qt_dose,0),
              a.cd_unidade_medida_dose,
              nr_prescricao_w,
              a.nr_sequencia,
              nr_seq_item_prescr_w,
              nvl(ie_agrupador_quimio,ie_agrupador),
              nm_usuario_p,
              sysdate,
			  nr_seq_mat_hor_w
            from  material b,
              prescr_material a
            where a.nr_prescricao   = nr_prescricao_w
            and a.nr_sequencia_diluicao = nr_sequencia_medic_w
            and a.cd_material   = b.cd_material
            and a.ie_agrupador    in (3)
            and   a.dt_suspensao    is null
            and a.cd_unidade_medida_dose is not null;

            if  (ie_gerar_reconstituinte_w = 'S') then
              insert into can_ordem_item_prescr(
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                nr_seq_ordem,
                cd_material,
                qt_dose,
                cd_unidade_medida,
                nr_prescricao,
                nr_seq_prescricao,
                nr_sequencia_diluente,
                ie_agrupador,
                nm_usuario_nrec,
                dt_atualizacao_nrec,
				nr_seq_mat_hor)
                select  can_ordem_item_prescr_seq.nextval,
                sysdate,
                nm_usuario_p,
                nr_sequencia_w,
                decode(ie_generico_w,'S',nvl(b.cd_material_generico,a.cd_material),a.cd_material),
                nvl(a.qt_dose,0),
                a.cd_unidade_medida_dose,
                nr_prescricao_w,
                a.nr_sequencia,
                nr_seq_item_prescr_w,
                ie_agrupador,
                nm_usuario_p,
                sysdate,
				nr_seq_mat_hor_w
              from  material b,
                prescr_material a
              where a.nr_prescricao   = nr_prescricao_w
              and a.nr_sequencia_diluicao = nr_sequencia_medic_w
              and a.cd_material   = b.cd_material
              and a.ie_agrupador    in (9)
              and a.cd_unidade_medida_dose is not null;

            end if;

            begin
            select  decode(upper(cd_unidade_medida_dose),upper(obter_unid_med_usua('ML')),qt_dose, qt_dose * obter_conversao_unid_med(cd_material,upper(obter_unid_med_usua('ML'))))
            into  qt_dose_diluente_w
            from  prescr_material
            where nr_sequencia_diluicao = nr_seq_prescr_w
            and   nr_prescricao   = nr_prescricao_w
            and   ie_agrupador    = 3;
            exception
            when others then
              qt_dose_diluente_w  := 0;
            end;

            select
                max(ie_via_aplicacao)
            into
                ie_via_aplicacao_got_w
            from  prescr_material
            where nr_prescricao   = nr_prescricao_w
            and   nr_sequencia  = nr_seq_prescr_w;

            ie_aplica_gotejamento_w := 'S';
            if (ie_via_aplicacao_got_w is not null) then
              select nvl(ie_gotejamento,'S')
              into   ie_aplica_gotejamento_w
              from   via_aplicacao
              where  ie_via_aplicacao = ie_via_aplicacao_got_w;
            end if;

            if (ie_aplica_gotejamento_w = 'S') then
              Calcular_gotejamento_ordem(ie_tipo_dosagem_w,dividir(qt_min_aplicacao_w,60) + qt_hora_aplicacao_w,qt_dose_diluente_w ,vl_gotejo_w);
            else
              vl_gotejo_w := null;
              ie_tipo_dosagem_w := null;
            end if;

            qt_volume_aspirado_w := null;

            if  (ie_grava_aspirado_w = 'S') then
              qt_volume_aspirado_w := obter_volume_aspirado(nr_sequencia_w);
            end if;

            update  can_ordem_prod
            set qt_dosagem          = vl_gotejo_w,
              qt_volume_aspirado  = qt_volume_aspirado_w,
              ie_tipo_dosagem   = ie_tipo_dosagem_w
            where nr_sequencia    = nr_sequencia_w;

            end;
          END LOOP;
          Close C03;
          end;
        END LOOP;

        commit;
        end;
      end if;
      end;

    END LOOP;
    Close C02;
    end;

  END LOOP;
  Close C01;

end Gerar_ordem_ambulatorial_Impl;
/
