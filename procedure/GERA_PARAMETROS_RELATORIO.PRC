create or replace
procedure gera_parametros_relatorio (
	ds_comando_p in varchar2,
	ds_retorno_p out varchar2) is
cursor_p	integer;
col_cnt     	pls_integer;	--Numero de colunas
dtab       	 	dbms_sql.desc_tab2;	--Descricao da Tabela
nr_tipo_w	number(1);
ds_erro_w	Varchar2(512);
nm_coluna_w	varchar2(4000) := '';
nr_repet_coluna_w		number(10)	:= 1;
TYPE COLUNAS_W IS TABLE OF VARCHAR2(4000) INDEX BY VARCHAR2(4000);
colunas_sql_w COLUNAS_W;
begin
	if(ds_comando_p is not null) and
		(trim(ds_comando_p) is null ) then
		wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(191071);
	end if;
	cursor_p := DBMS_SQL.OPEN_CURSOR;
	DBMS_SQL.PARSE(cursor_p, ds_comando_p, dbms_sql.native);
	dbms_sql.describe_columns2(cursor_p,col_cnt,dtab);
	--dbms_sql.desc_rec(cursor_p,col_cnt,dtab);
	/*Registra as colunas*/
	for i in 1 .. col_cnt loop
		nm_coluna_w	:= upper(dtab(i).col_name);
		if	(colunas_sql_w.EXISTS(nm_coluna_w)) then
			nm_coluna_w := 	substr(nm_coluna_w || '_' || nr_repet_coluna_w,1,3999);
			nr_repet_coluna_w := nr_repet_coluna_w + 1;
		end if;
		colunas_sql_w(nm_coluna_w) := nm_coluna_w;
		
		if	( dtab(i).col_type =  12 ) then
			nr_tipo_w := 0; --DATE
		elsif (  dtab(i).col_type in (23,24)) then
			nr_tipo_w := 1; --BYTE
		elsif (  dtab(i).col_type = 8) then
			nr_tipo_w := 2; -- LONG
		elsif (  dtab(i).col_type = 2) then
			if	(dtab(i).col_scale > 0) then
				nr_tipo_w := 3;--DOUBLE
			else
				nr_tipo_w := 4;--NUMBER
			end if;
		else
			nr_tipo_w := 5; --VARCHAR2
		end if;
		if	(ds_retorno_p is not null ) then
			ds_retorno_p := ds_retorno_p ||';';
		end if;
		ds_retorno_p := ds_retorno_p || nm_coluna_w  ||'='||nr_tipo_w;
	end loop;
	DBMS_SQL.CLOSE_CURSOR(cursor_p);
exception
when others then
	begin
	DBMS_SQL.CLOSE_CURSOR(cursor_p);
	ds_erro_w	:= SQLERRM(sqlcode);
	wheb_mensagem_pck.EXIBIR_MENSAGEM_ABORT(191072,'ERRO='||ds_erro_w);
	end;
end gera_parametros_relatorio;
/