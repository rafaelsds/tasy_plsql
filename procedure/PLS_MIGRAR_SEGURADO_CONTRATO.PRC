create or replace 
procedure pls_migrar_segurado_contrato
			(	nr_seq_contrato_novo_p		in	number,
				nr_seq_segurado_atual_p		in	number,
				nr_seq_titular_novo_p		in	number,
				nr_seq_pagador_novo_p		in	number,
				nr_seq_pessoa_proposta_p	in	number,
				ie_mantem_cartao_p		in	varchar2,
				dt_migracao_p			in	date,
				dt_limite_util_p		in	date,
				nr_seq_motivo_canc_p		in	number,
				nr_seq_canal_venda_p		in	number,
				nr_seq_vendedor_pf_p		in	number,
				ie_nao_rescindir_benef_p	in	varchar2,
				nm_usuario_p			in	varchar2,
				cd_estabelecimento_p		in	number,
				nr_seq_causa_rescisao_p		in	number,
				nr_seq_alteracao_motivo_p	in	number,
				nr_seq_motivo_rescisao_pag_p	in	number,
				nr_seq_plano_novo_p		in	number,
				nr_seq_tabela_novo_p		in	number,
				qt_meses_contribuicao_p		in	number,
				qt_meses_empresa_pagador_p	in	number,
				ie_migrar_pagador_atual_p	in	varchar2,
				nr_seq_subestipulante_p		in	number,
				nr_seq_segurado_novo_p		out	number,
				ds_retorno_p			out	varchar2) is

cd_pessoa_fisica_w		varchar2(10);
dt_contratacao_w		date;
nr_seq_contrato_ant_w		number(10);
cd_usuario_plano_ant_w		pls_segurado_carteira.cd_usuario_plano%type;
nr_seq_plano_w			number(10);
nr_seq_segurado_novo_w		number(10,0) := null;
qt_registros_w			number(5);
nr_seq_tabela_w			number(10,0);
dt_inclusao_operadora_w		date;
nr_seq_titular_novo_w		number(10,0);
nr_seq_segurado_ant_w		number(10);
nr_seq_titular_ant_w		number(10);
qt_benef_inativos_w		number(10);
qt_beneficiarios_w		number(10);
dt_liberacao_w			date;
dt_rescisao_w			date;
nm_segurado_w			varchar2(60);
qt_dias_rescisao_mig_w		number(5);
ds_mensagem_w			varchar2(255);
ie_rescisao_w			varchar2(1);
nr_seq_plano_ant_w		number(10);
qt_tamanho_mensagem_w		number(10);
nr_seq_seg_contrato_w		number(10);
nr_seq_parentesco_w		grau_parentesco.nr_sequencia%type;
ie_tipo_parentesco_w		grau_parentesco.ie_grau_parentesco%type;
cd_matricula_estipulante_w	varchar2(30);
ie_tipo_operacao_w		varchar2(3);
nr_seq_portabilidade_w		number(10);
nr_seq_vinculo_estip_w		number(10);
nr_seq_proposta_w		number(10);
nr_seq_vendedor_canal_w		number(10);
nr_seq_vendedor_pf_w		number(10);
nr_seq_titular_contrato_w	number(10);
cd_pf_titular_w			varchar2(10);
ie_nascido_plano_w		varchar2(1);
ie_permite_tabela_dif_w		varchar2(1);
nr_seq_pagador_novo_w		number(10);
qt_pagador_novo_w		number(10);
nr_seq_motivo_inclusao_w	number(10);
nr_contrato_ant_w		number(10);
nr_contrato_mig_w		number(10);
ie_taxa_inscricao_w		varchar2(10);
ie_pagador_contr_origem_w	varchar2(10);
cd_pessoa_titular_w		varchar2(10);
nr_seq_titular_contr_novo_w	number(10);
nr_seq_tipo_comercial_w		number(10);
cd_cco_w			pls_segurado.cd_cco%type;
ie_gerar_validade_cartao_w	varchar2(1);
nr_seq_tipo_carencia_w		number(10);
qt_dias_w			number(10);
dt_inicio_vigencia_w		date;
ds_observacao_w			varchar2(4000);
dt_fim_vigencia_w		date;
ie_cpt_w			varchar2(10);
nr_seq_segurado_compl_w		number(10);
cd_cbo_w			varchar(6);
dt_admissao_w			date;
nr_seq_localizacao_benef_w	number(10);
nr_seq_vinculo_bonific_w	number(10);
nr_seq_vinculo_sca_w		number(10);
nr_seq_vinculo_sca_novo_w	number(10);
nr_seq_tabela_sca_w		number(10);
nr_seq_plano_sca_w		number(10);
dt_reajuste_sca_w		date;
ie_lancamento_mensalidade_w	varchar2(2);
ie_geracao_valores_w		varchar2(10);
ds_erro_w			varchar2(4000);
ie_permite_migrar_benef_resc_w	varchar2(1);
qt_registro_w			pls_integer;
dt_alteracao_w			date;
ie_rescindir_contrato_mig_w	pls_parametros.ie_rescindir_contrato_migracao%type;
ie_situacao_trabalhista_w	pls_motivo_alt_pagador.ie_situacao_trabalhista%type;
nr_seq_titular_proposta_w	pls_proposta_beneficiario.nr_sequencia%type;
dt_rescisao_titular_w		pls_segurado.dt_rescisao%type;
nr_seq_titular_atual_w		pls_segurado.nr_sequencia%type;
nr_seq_solicitacao_w		pls_segurado_solic_alt.nr_sequencia%type;
qt_localizacao_w		number(10);
nr_seq_pagador_atual_w		pls_contrato_pagador.nr_sequencia%type;
dt_rescisao_contrato_w		pls_contrato.dt_rescisao_contrato%type;
ie_rescisao_migracao_w		pls_segurado.ie_rescisao_migracao%type;
dt_fim_repasse_w		pls_segurado_repasse.dt_fim_repasse%type;
nr_seq_seg_repasse_w		number(10);
ie_gerar_valor_w		varchar2(1);
ie_impedir_migracao_w		pls_motivo_cancelamento.ie_impedir_migracao%type;
nr_seq_motivo_cancelamento_w	pls_segurado.nr_seq_motivo_cancelamento%type;
dt_limite_util_w		date;
 
Cursor c01 is
	select	nr_sequencia,
		cd_pessoa_fisica,
		dt_inclusao_operadora,
		nr_seq_titular,
		dt_liberacao,
		dt_rescisao,
		substr(obter_nome_pf(cd_pessoa_fisica),1,60),
		nr_seq_plano,
		nr_seq_parentesco,
		ie_tipo_parentesco,
		ie_nascido_plano,
		nr_seq_motivo_inclusao,
		pls_obter_carteira_segurado(nr_sequencia),
		cd_cco,
		nr_seq_localizacao_benef,
		ie_rescisao_migracao
	from	pls_segurado
	where	nr_seq_contrato	= nr_seq_contrato_ant_w
	and	((ie_rescisao_w	= 'R' and nr_sequencia	= nr_seq_segurado_atual_p) or
		(ie_rescisao_w 	= 'A' and ((nr_sequencia = nr_seq_segurado_atual_p) or (nr_seq_titular = nr_seq_segurado_atual_p and dt_rescisao is null))))
	and	(((nvl(nr_seq_pessoa_proposta_p,0) = 0) and ((ie_permite_migrar_benef_resc_w = 'S') or ((ie_permite_migrar_benef_resc_w = 'N') and (dt_rescisao is null)))) or
		 (nvl(nr_seq_pessoa_proposta_p,0) <> 0))
	and	nr_seq_segurado_mig is null
	order by	nr_seq_titular desc,
			nr_sequencia;

Cursor C02 is
	select	nr_seq_tipo_carencia,
		qt_dias,
		dt_inicio_vigencia,
		ds_observacao
	from	pls_carencia
	where	nr_seq_pessoa_proposta	= nr_seq_pessoa_proposta_p
	and	ie_cpt			= ie_cpt_w;

Cursor C03 is
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_segurado_prop	= nr_seq_pessoa_proposta_p;

Cursor C04 is
	select	nr_sequencia,
		dt_fim_repasse
	from	pls_segurado_repasse
	where	nr_seq_segurado	= nr_seq_segurado_ant_w
	and	dt_liberacao is not null
	and	((dt_fim_repasse is null) or (dt_fim_repasse > dt_limite_util_w));

begin
dt_limite_util_w		:= fim_dia(dt_limite_util_p);
ds_mensagem_w := '';
nr_seq_titular_novo_w		:= nr_seq_titular_novo_p;
ie_pagador_contr_origem_w	:= nvl(obter_valor_param_usuario(1202, 97, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'N');
ie_permite_migrar_benef_resc_w	:= nvl(obter_valor_param_usuario(1202, 23, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'N');
ie_gerar_valor_w		:= 'N';

select	nr_seq_contrato,
	nr_seq_titular,
	nr_seq_pagador,
	nr_seq_motivo_cancelamento
into	nr_seq_contrato_ant_w,
	nr_seq_titular_atual_w,
	nr_seq_pagador_atual_w,
	nr_seq_motivo_cancelamento_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_atual_p;

if 	(nr_seq_motivo_cancelamento_w is not null) then
	select	nvl(max(ie_impedir_migracao),'N')
	into	ie_impedir_migracao_w
	from	pls_motivo_cancelamento
	where	nr_sequencia = nr_seq_motivo_cancelamento_w;
	
	if	(ie_impedir_migracao_w = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(1086146,'DS_MOTIVO_CANCELAMENTO=' ||pls_obter_desc_motivo_cancel(nr_seq_motivo_cancelamento_w) );
		--Nao e possivel migrar o beneficiario de contrato, pois o motivo de cancelamento "#@DS_MOTIVO_CANCELAMENTO#@" impede a migracao.
	end if;
end if;

if	(nvl(qt_meses_contribuicao_p,0) = 0) then
	select	max(ie_situacao_trabalhista)
	into	ie_situacao_trabalhista_w
	from	pls_motivo_alt_pagador
	where	nr_sequencia	= nr_seq_alteracao_motivo_p;
	
	if	(ie_situacao_trabalhista_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(348741);
		--Nao e possivel gerar pagador "Demitido/Aposentado/Exonerado" caso o beneficiario nao tenha realizado nenhuma contribuicao com a mensalidade
	end if;
end if;

if	(nr_seq_contrato_novo_p is not null) then
	select	nr_contrato,
		ie_tipo_operacao,
		ie_geracao_valores,
		dt_rescisao_contrato
	into	nr_contrato_mig_w,
		ie_tipo_operacao_w,
		ie_geracao_valores_w,
		dt_rescisao_contrato_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_novo_p;
	
	if	(dt_rescisao_contrato_w is not null) and
		(dt_migracao_p >= dt_rescisao_contrato_w) then --O contrato #@NR_CONTRATO#@ foi rescindido em #@DT_RESCISAO#@. Nao sera possivel realizar a migracao
		wheb_mensagem_pck.exibir_mensagem_abort(686550,'NR_CONTRATO='||nr_contrato_mig_w||';DT_RESCISAO='||dt_rescisao_contrato_w);
	end if;
	
	select	nvl(max(qt_dias_rescisao_mig),0),
		nvl(max(ie_gerar_validade_cartao),'S')
	into	qt_dias_rescisao_mig_w,
		ie_gerar_validade_cartao_w
	from	pls_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	if	(nr_seq_plano_novo_p is null) then
		select	count(1)
		into	qt_registros_w
		from	pls_contrato_plano
		where	nr_seq_contrato	= nr_seq_contrato_novo_p
		and	ie_situacao	= 'A';
		
		if	(qt_registros_w = 1) then
			select	nr_seq_plano,
				nr_seq_tabela
			into	nr_seq_plano_w,
				nr_seq_tabela_w
			from	pls_contrato_plano
			where	nr_seq_contrato	= nr_seq_contrato_novo_p
			and	ie_situacao	= 'A';
		elsif	(nr_seq_pessoa_proposta_p is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(31926);
		end if;
	else
		nr_seq_plano_w 	:= nr_seq_plano_novo_p;
		nr_seq_tabela_w := nr_seq_tabela_novo_p;
	end if;
	
	ie_rescisao_w	:= 'R';
	if	(nvl(nr_seq_pessoa_proposta_p,0) = 0) then
		select	decode(dt_rescisao,null,'A','R')
		into	ie_rescisao_w
		from	pls_segurado
		where	nr_sequencia = nr_seq_segurado_atual_p;
	end if;
	open c01;
	loop
	fetch c01 into
		nr_seq_segurado_ant_w,
		cd_pessoa_fisica_w,
		dt_inclusao_operadora_w,
		nr_seq_titular_ant_w,
		dt_liberacao_w,
		dt_rescisao_w,
		nm_segurado_w,
		nr_seq_plano_ant_w,
		nr_seq_parentesco_w,
		ie_tipo_parentesco_w,
		ie_nascido_plano_w,
		nr_seq_motivo_inclusao_w,
		cd_usuario_plano_ant_w,
		cd_cco_w,
		nr_seq_localizacao_benef_w,
		ie_rescisao_migracao_w;
	exit when c01%notfound;
		begin
		if (dt_rescisao_w is not null) then
			dt_rescisao_w := fim_dia(dt_rescisao_w);
		end if;
		
		select	count(1)
		into	qt_registro_w
		from	pls_segurado
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	nr_seq_contrato		= nr_seq_contrato_novo_p
		and	nr_seq_plano		= nr_seq_plano_w
		and	dt_rescisao is null;
		if	(qt_registro_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(280741);
		end if;
		
		if	(trunc(dt_migracao_p,'dd') < trunc(dt_inclusao_operadora_w,'dd')) then
			wheb_mensagem_pck.exibir_mensagem_abort(309770,'BENEFICIARIO='||nr_seq_segurado_ant_w||' - '||nm_segurado_w); --A data de migracao do beneficiario #@BENEFICIARIO#@ nao pode ser menor que a data de inclusao na operadora.
		end if;
		
		ie_taxa_inscricao_w	:= 'S';
		if	(dt_liberacao_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(178231,'NM_SEGURADO='||nm_segurado_w);
		end if;
		
		if	(nr_seq_contrato_novo_p = nr_seq_contrato_ant_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(178233);
		end if;
		
		if	(nr_seq_titular_ant_w is not null) then
			select	max(cd_pessoa_fisica),
				max(dt_rescisao)
			into	cd_pessoa_titular_w,
				dt_rescisao_titular_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_titular_ant_w;
		else
			cd_pessoa_titular_w	:= null;
			dt_rescisao_titular_w	:= null;
		end if;
		
		if	(((dt_rescisao_w is not null) and
			  ((trunc(dt_rescisao_w,'dd') + qt_dias_rescisao_mig_w) >= trunc(dt_migracao_p,'dd')) and
			  ((nvl(nr_seq_pessoa_proposta_p,0) <> 0) or (nr_seq_titular_atual_w is not null) or (nr_seq_titular_ant_w is null) or (trunc(dt_rescisao_w,'dd') = trunc(dt_rescisao_titular_w,'dd')))) or --Se migrar pela gestao de contratos, migrar dependente somente se mesma data de rescisao do titular
			 (dt_rescisao_w is null))then
			
			select	decode(dt_rescisao_w,null,'A','R')
			into	ie_rescisao_w
			from	dual;
			
			if	(ie_rescisao_w = 'A') then
				pls_consiste_data_sib(nvl(dt_rescisao_w,dt_migracao_p), nm_usuario_p, cd_estabelecimento_p);
			end if;
			
			if	(nvl(nr_seq_titular_novo_w,0) = 0) then
				nr_seq_titular_novo_w	:= null;
				if	(cd_pessoa_titular_w is not null) then
					select	max(nr_sequencia)
					into	nr_seq_titular_contr_novo_w
					from	pls_segurado
					where	cd_pessoa_fisica	= cd_pessoa_titular_w
					and	nr_seq_contrato		= nr_seq_contrato_novo_p
					and	nr_seq_titular is null
					and	dt_rescisao is null;
					
					if	(nr_seq_titular_contr_novo_w is not null) then
						nr_seq_titular_novo_w	:= nr_seq_titular_contr_novo_w;
					end if;
				end if;
			end if;
			
			select	pls_segurado_seq.nextval
			into	nr_seq_segurado_novo_w
			from	dual;
			
			if	(nvl(nr_seq_pessoa_proposta_p,0) > 0) then
				begin
				nr_seq_titular_novo_w	:= null;
				select	nr_seq_proposta,
					nr_seq_portabilidade,
					nr_seq_vinculo_estip,
					nr_seq_titular_contrato,
					nr_seq_titular,
					nvl(nr_seq_parentesco,nr_seq_parentesco_w),
					ie_tipo_parentesco,
					ie_taxa_inscricao,
					nr_seq_tipo_comercial,
					nr_seq_motivo_inclusao,
					cd_cbo,
					dt_admissao,
					nvl(nr_seq_plano_novo_p,nr_seq_plano),
					cd_matricula_est
				into	nr_seq_proposta_w,
					nr_seq_portabilidade_w,
					nr_seq_vinculo_estip_w,
					nr_seq_titular_contrato_w,
					nr_seq_titular_proposta_w,
					nr_seq_parentesco_w,
					ie_tipo_parentesco_w,
					ie_taxa_inscricao_w,
					nr_seq_tipo_comercial_w,
					nr_seq_motivo_inclusao_w,
					cd_cbo_w,
					dt_admissao_w,
					nr_seq_plano_w,
					cd_matricula_estipulante_w
				from	pls_proposta_beneficiario
				where	nr_sequencia	= nr_seq_pessoa_proposta_p;
				exception
					when others then
					nr_seq_portabilidade_w	:= null;
				end;
				
				if	(nr_seq_titular_contrato_w is not null) then
					nr_seq_titular_novo_w	:= nr_seq_titular_contrato_w;
				elsif	(nr_seq_titular_proposta_w is not null) then
					select	cd_beneficiario
					into	cd_pf_titular_w
					from	pls_proposta_beneficiario
					where	nr_sequencia	= nr_seq_titular_proposta_w;
					
					select	max(a.nr_sequencia)
					into	nr_seq_titular_novo_w
					from	pls_segurado	a
					where	a.cd_pessoa_fisica	= cd_pf_titular_w
					and	a.nr_proposta_adesao	= nr_seq_proposta_w
					and	a.nr_seq_titular is null;
				end if;
				
				begin
				select	nr_seq_vendedor_canal,
					nr_seq_vendedor_pf,
					nvl(ie_permite_tabela_dif,'N')
				into	nr_seq_vendedor_canal_w,
					nr_seq_vendedor_pf_w,
					ie_permite_tabela_dif_w
				from	pls_proposta_adesao
				where	nr_sequencia	= nr_seq_proposta_w;
				exception
					when others then
					nr_seq_vendedor_canal_w	:= null;
					nr_seq_vendedor_pf_w	:= null;
					ie_permite_tabela_dif_w	:= 'N';
				end;

				if	(nr_seq_tabela_novo_p is null) then
					nr_seq_tabela_w	:= null;
					if	(nvl(nr_seq_plano_w,0) > 0) and
						(ie_permite_tabela_dif_w = 'N') then
						
						select	max(nr_seq_tabela)
						into	nr_seq_tabela_w
						from	pls_contrato_plano
						where	nr_seq_plano	= nr_seq_plano_w
						and	nr_seq_contrato	= nr_seq_contrato_novo_p
						and   (	select	nr_seq_tabela
							from	pls_proposta_beneficiario
							where	nr_sequencia = nr_seq_pessoa_proposta_p ) in (nr_seq_tabela, nr_seq_tabela_origem);
						
						if	(nr_seq_tabela_w is null) then
							wheb_mensagem_pck.exibir_mensagem_abort(331285); --Tabela de preco deve ser igual a tabela informada no contrato!
						end if;
					elsif	(nvl(nr_seq_plano_w,0) > 0) and
						(ie_permite_tabela_dif_w = 'S') then
						select	nr_seq_tabela
						into	nr_seq_tabela_w
						from	pls_proposta_beneficiario
						where	nr_sequencia	= nr_seq_pessoa_proposta_p;
					end if;
				else
					nr_seq_tabela_w := nr_seq_tabela_novo_p;
				end if;
				
				pls_proposta_gerar_log(nr_seq_proposta_w, nr_seq_contrato_novo_p, nr_seq_plano_w,
					'MS', nr_seq_segurado_novo_w, nr_seq_titular_novo_w,
					nr_seq_parentesco_w, nm_usuario_p);
			else
				nr_seq_vendedor_canal_w	:= nr_seq_canal_venda_p;
				nr_seq_vendedor_pf_w	:= nr_seq_vendedor_pf_p;
			end if;
			
			if	(qt_meses_empresa_pagador_p > 0) then
				select	max(nr_sequencia)
				into	nr_seq_pagador_novo_w
				from	pls_contrato_pagador
				where	nr_seq_contrato = nr_seq_contrato_novo_p
				and	ie_tipo_pagador = 'P';
			else
				if	(nr_seq_pagador_novo_p is not null) then
					nr_seq_pagador_novo_w	:= nr_seq_pagador_novo_p;
				elsif	(nr_seq_pagador_novo_p is null) then
					if	(ie_migrar_pagador_atual_p = 'S') and
						(nr_seq_segurado_ant_w = nr_seq_segurado_atual_p)then
						select	pls_contrato_pagador_seq.nextval
						into	nr_seq_pagador_novo_w
						from	dual;
						
						insert into pls_contrato_pagador
						(		nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_contrato,
								cd_pessoa_fisica,
								cd_cgc,
								ie_endereco_boleto,
								nr_seq_compl_pf_tel_adic,
								ie_tipo_pagador,
								ie_envia_cobranca,
								dt_primeira_mensalidade,
								ie_calc_primeira_mens,
								ie_calculo_proporcional,
								ie_pessoa_comprovante,
								ie_notificacao,
								ie_taxa_emissao_boleto,
								ie_inadimplencia_via_adic,
								nr_seq_regra_obito,
								ds_pagador,
								nr_seq_tipo_compl_adic,
								nr_seq_compl_pj,
								cd_sistema_anterior,
								nr_seq_destino_corresp,
								dt_suspensao,
								dt_rescisao,
								dt_reativacao,
								nr_seq_motivo_cancelamento,
								ie_situacao_trabalhista,
								nr_seq_classif_itens,
								ds_email,
								nr_seq_seg_mig_origem,
								ie_receber_sms)
						(	select 	nr_seq_pagador_novo_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_contrato_novo_p,
								cd_pessoa_fisica,
								cd_cgc,
								ie_endereco_boleto,
								nr_seq_compl_pf_tel_adic,
								'S',
								ie_envia_cobranca,
								dt_primeira_mensalidade,
								ie_calc_primeira_mens,
								ie_calculo_proporcional,
								ie_pessoa_comprovante,
								ie_notificacao,
								ie_taxa_emissao_boleto,
								ie_inadimplencia_via_adic,
								nr_seq_regra_obito,
								ds_pagador,
								nr_seq_tipo_compl_adic,
								nr_seq_compl_pj,
								cd_sistema_anterior,
								nr_seq_destino_corresp,
								dt_suspensao,
								dt_rescisao,
								dt_reativacao,
								nr_seq_motivo_cancelamento,
								ie_situacao_trabalhista,
								nr_seq_classif_itens,
								ds_email,
								nr_seq_segurado_atual_p,
								'S'
							from	pls_contrato_pagador
							where	nr_sequencia	= nr_seq_pagador_atual_w);
						
						insert into pls_contrato_pagador_fin
							(	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								dt_inicio_vigencia,
								dt_dia_vencimento,
								nr_seq_forma_cobranca,
								ie_mes_vencimento,
								cd_condicao_pagamento,
								cd_tipo_portador,
								cd_portador,
								nr_seq_conta_banco,
								nr_seq_carteira_cobr,
								cd_autenticidade,
								ie_portador_exclusivo,
								cd_banco,
								cd_agencia_bancaria,
								ie_digito_agencia,
								cd_conta,
								ie_digito_conta,
								nr_seq_empresa,
								cd_profissao,
								nr_seq_vinculo_empresa,
								cd_matricula,
								ie_geracao_nota_titulo,
								ie_destacar_reajuste,
								nr_seq_pagador,
								dt_fim_vigencia,
								nr_seq_mot_cob,
								cd_tipo_portador_deb_aut,
								cd_portador_deb_aut,
								nr_seq_conta_banco_deb_aut,
								nr_pensionista,
								ie_gerar_cobr_escrit,
								ds_observacao)
						(	select	pls_contrato_pagador_fin_seq.nextval,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								dt_inicio_vigencia,
								dt_dia_vencimento,
								nr_seq_forma_cobranca,
								ie_mes_vencimento,
								cd_condicao_pagamento,
								cd_tipo_portador,
								cd_portador,
								nr_seq_conta_banco,
								nr_seq_carteira_cobr,
								cd_autenticidade,
								ie_portador_exclusivo,
								cd_banco,
								cd_agencia_bancaria,
								ie_digito_agencia,
								cd_conta,
								ie_digito_conta,
								nr_seq_empresa,
								cd_profissao,
								nr_seq_vinculo_empresa,
								cd_matricula,
								ie_geracao_nota_titulo,
								ie_destacar_reajuste,
								nr_seq_pagador_novo_w,
								dt_fim_vigencia,
								nr_seq_mot_cob,
								cd_tipo_portador_deb_aut,
								cd_portador_deb_aut,
								nr_seq_conta_banco_deb_aut,
								nr_pensionista,
								ie_gerar_cobr_escrit,
								ds_observacao
							from	pls_contrato_pagador_fin
							where	nr_seq_pagador	= nr_seq_pagador_atual_w
							and	(dt_fim_vigencia is null or dt_fim_vigencia > dt_migracao_p));
					end if;
					
					if	(ie_pagador_contr_origem_w = 'S') and
						(nr_seq_pagador_novo_w is null) then
						select	max(nr_seq_pagador)
						into	nr_seq_pagador_novo_w
						from	pls_segurado
						where	nr_sequencia	= nr_seq_segurado_ant_w;
					end if;
					
					if	(nr_seq_pagador_novo_w is null) then
						select	count(1)
						into	qt_pagador_novo_w
						from	pls_contrato_pagador
						where	nr_seq_contrato	= nr_seq_contrato_novo_p;
						
						if	(qt_pagador_novo_w	= 1) then
							select	max(nr_sequencia)
							into	nr_seq_pagador_novo_w
							from	pls_contrato_pagador
							where	nr_seq_contrato	= nr_seq_contrato_novo_p;
						else
							nr_seq_pagador_novo_w	:= null;
						end if;
					end if;
				end if;
			end if;
			
			if	(nr_seq_motivo_inclusao_w is null) then
				select	nr_seq_motivo_inclusao
				into	nr_seq_motivo_inclusao_w
				from	pls_parametros
				where	cd_estabelecimento	= cd_estabelecimento_p;
			end if;
			
			if	(nr_seq_titular_novo_w is null) then
				nr_seq_parentesco_w	:= null;
				ie_tipo_parentesco_w	:= null;
			end if;
			
			ie_taxa_inscricao_w	:= nvl(ie_taxa_inscricao_w,'S');
			
			select	nvl(max(nr_seq_seg_contrato),0) + 1
			into	nr_seq_seg_contrato_w
			from	pls_segurado
			where	nr_seq_contrato	= nr_seq_contrato_novo_p;
			
			select	max(nr_contrato)
			into	nr_contrato_ant_w
			from	pls_contrato
			where	nr_sequencia	= nr_seq_contrato_ant_w;
			
			select 	count(*)
			into	qt_localizacao_w
			from 	pls_localizacao_benef a,
				pls_localizacao_pj b
			where 	a.nr_seq_localizacao_pj   = b.nr_sequencia
			and 	b.cd_cgc = (	select	max(cd_cgc_estipulante)
						from	pls_contrato
						where	nr_sequencia = nr_seq_contrato_novo_p)
			and 	a.nr_sequencia	=	nr_seq_localizacao_benef_w;
			
			if 	(qt_localizacao_w = 0)	then
				nr_seq_localizacao_benef_w := null;
			end if;
			
			insert into pls_segurado(
				nr_sequencia, dt_atualizacao, nm_usuario,
				dt_atualizacao_nrec, nm_usuario_nrec, cd_pessoa_fisica,
				nr_seq_titular, nr_seq_contrato, nr_seq_pagador,
				nr_seq_plano, nr_seq_tabela, nr_proposta_adesao,
				dt_contratacao, dt_liberacao, nr_seq_parentesco,
				cd_segurado_familia, dt_inclusao_operadora, dt_limite_utilizacao,
				nr_seq_vinculo_est, cd_matricula_estipulante, nr_seq_tabela_origem,
				nr_seq_motivo_inclusao, nr_seq_segurado_mig, nr_seq_vendedor_pf,
				nr_seq_vendedor_canal, ie_acao_contrato, ie_tipo_plano,
				ie_tipo_segurado, nr_seq_pessoa_proposta, nr_seq_portabilidade,
				nr_seq_subestipulante, dt_migracao, nr_seq_segurado_ant,
				nr_seq_seg_contrato, ie_nascido_plano, ie_situacao_atend,
				cd_estabelecimento, nr_contrato_ant, ie_taxa_inscricao,
				ie_renovacao_carteira, nr_seq_tipo_comercial, cd_cco,
				nr_seq_localizacao_benef, ie_tipo_parentesco)
			values(	nr_seq_segurado_novo_w, sysdate, nm_usuario_p,
				sysdate, nm_usuario_p, cd_pessoa_fisica_w,
				nr_seq_titular_novo_w, nr_seq_contrato_novo_p, nr_seq_pagador_novo_w,
				nr_seq_plano_w, nr_seq_tabela_w, nr_seq_proposta_w,
				dt_migracao_p, null, nr_seq_parentesco_w,
				null, dt_inclusao_operadora_w, null,
				nr_seq_vinculo_estip_w, cd_matricula_estipulante_w, null,
				nr_seq_motivo_inclusao_w, null, nr_seq_vendedor_pf_w,
				nr_seq_vendedor_canal_w, 'M', null,
				ie_tipo_operacao_w, nr_seq_pessoa_proposta_p, nr_seq_portabilidade_w,
				nr_seq_subestipulante_p, dt_migracao_p, nr_seq_segurado_ant_w,
				nr_seq_seg_contrato_w, ie_nascido_plano_w, 'A',
				cd_estabelecimento_p, nr_contrato_ant_w, ie_taxa_inscricao_w,
				'S', nr_seq_tipo_comercial_w, cd_cco_w,
				nr_seq_localizacao_benef_w, ie_tipo_parentesco_w);
			
			if	(qt_meses_empresa_pagador_p > 0) then
				dt_alteracao_w :=  add_months(sysdate,qt_meses_empresa_pagador_p);
				nr_seq_solicitacao_w := null;
				
				pls_inserir_solic_alt_cad(	nr_seq_segurado_novo_w,'1','T',
								nr_seq_pagador_novo_p, null,dt_alteracao_w,
								wheb_mensagem_pck.get_texto(1108075),nm_usuario_p,'N',
								nr_seq_solicitacao_w);
			end if;
			
			if	(nr_seq_pessoa_proposta_p is not null) then
				begin
				update	pls_declaracao_segurado
				set	nr_seq_segurado		= nr_seq_segurado_ant_w
				where	nr_seq_proposta_adesao	= nr_seq_proposta_w
				and	cd_pessoa_fisica	= cd_pessoa_fisica_w;
				exception
				when others then
					null;
				end;
				
				insert	into	pls_segurado_doc_arq
					(	nr_sequencia,
						nr_seq_segurado,
						cd_estabelecimento,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ds_arquivo,
						ie_origem_anexo,
						nr_seq_tipo_documento)
					(select	pls_segurado_doc_arq_seq.nextval,
						nr_seq_segurado_novo_w,
						cd_estabelecimento_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						ds_anexo,
						'P',
						nr_seq_tipo_documento
					from	pls_proposta_benef_anexo
					where	nr_seq_beneficiario = nr_seq_pessoa_proposta_p);
			end if;
			
			select	pls_segurado_compl_seq.nextval
			into	nr_seq_segurado_compl_w
			from	dual;
			
			insert	into	pls_segurado_compl(
				nr_sequencia, nr_seq_segurado, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				cd_cbo, dt_admissao)
			values(	nr_seq_segurado_compl_w, nr_seq_segurado_novo_w, sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				cd_cbo_w, dt_admissao_w);
			
			pls_consiste_inclusao_benef(null,nr_seq_segurado_novo_w,nm_usuario_p);
			
			ie_cpt_w	:= 'N';
			
			if	(nr_seq_pessoa_proposta_p is not null) then
				--Carencias da propostas
				open c02;
				loop
				fetch c02 into
					nr_seq_tipo_carencia_w,
					qt_dias_w,
					dt_inicio_vigencia_w,
					ds_observacao_w;
				exit when c02%notfound;
					begin
					select	count(1)
					into	qt_registros_w
					from	pls_carencia
					where	nr_seq_segurado		= nr_seq_segurado_novo_w
					and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
					and	rownum			<= 1;
					
					if	(qt_registros_w = 0) and
						(pls_consistir_sexo_carencia(nr_seq_segurado_novo_w,nr_seq_tipo_carencia_w) = 'S') then
						dt_fim_vigencia_w	:= dt_migracao_p + qt_dias_w;
						
						insert into pls_carencia
							(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
								nr_seq_tipo_carencia, nr_seq_segurado, qt_dias, dt_inicio_vigencia, ds_observacao,
								ie_mes_posterior, ie_origem_carencia_benef, ie_cpt, dt_fim_vigencia)
						values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
								nr_seq_tipo_carencia_w, nr_seq_segurado_novo_w, qt_dias_w, dt_migracao_p, ds_observacao_w,
								'N', 'P', 'N', dt_fim_vigencia_w);
					end if;
					end;
				end loop;
				close c02;
				
				--Gerar bonificacao para o beneficiario
				open C03;
				loop
				fetch C03 into
					nr_seq_vinculo_bonific_w;
				exit when C03%notfound;
					begin
					insert	into pls_bonificacao_vinculo
						(nr_sequencia, nr_seq_bonificacao, dt_atualizacao,
						nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
						nr_seq_segurado, tx_bonificacao, vl_bonificacao,
						dt_inicio_vigencia, dt_fim_vigencia)
						(select	pls_bonificacao_vinculo_seq.nextval,
							nr_seq_bonificacao,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_segurado_novo_w,
							tx_bonificacao,
							vl_bonificacao,
							dt_inicio_vigencia,
							dt_fim_vigencia
						from	pls_bonificacao_vinculo
						where	nr_sequencia	= nr_seq_vinculo_bonific_w);
					end;
				end loop;
				close C03;
			end if;
			
			pls_migrar_carencia(nr_seq_segurado_ant_w, nr_seq_segurado_novo_w, nr_seq_contrato_novo_p, nr_seq_plano_w, nm_usuario_p);
			
			pls_gerar_lanc_automatico(nr_seq_segurado_novo_w, 1, 'M', 'N', '', nm_usuario_p, cd_estabelecimento_p);
			
			select	count(1)
			into	qt_registros_w
			from	pls_carencia
			where	nr_seq_segurado	= nr_seq_segurado_ant_w
			and	ie_cpt		= 'S';

			if	(qt_registros_w > 0) then
				pls_migrar_cpt_benef(nr_seq_segurado_novo_w, cd_estabelecimento_p, nm_usuario_p);
			end if;
			
			ie_cpt_w	:= 'S';

			if	(nr_seq_pessoa_proposta_p is not null) then
				open c02;
				loop
				fetch c02 into
					nr_seq_tipo_carencia_w,
					qt_dias_w,
					dt_inicio_vigencia_w,
					ds_observacao_w;
				exit when c02%notfound;
					begin
					select	count(1)
					into	qt_registros_w
					from	pls_carencia
					where	nr_seq_segurado		= nr_seq_segurado_novo_w
					and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_w
					and	rownum			<= 1;
					
					if	(qt_registros_w = 0) then
						dt_fim_vigencia_w	:= nvl(dt_inicio_vigencia_w,dt_inclusao_operadora_w) + qt_dias_w;
						
						insert into pls_carencia
							(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
								nr_seq_tipo_carencia,nr_seq_segurado,qt_dias,dt_inicio_vigencia,ds_observacao,
								ie_mes_posterior,ie_origem_carencia_benef,ie_cpt,dt_fim_vigencia)
						values	(	pls_carencia_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
								nr_seq_tipo_carencia_w,nr_seq_segurado_novo_w,qt_dias_w,nvl(dt_inicio_vigencia_w,dt_inclusao_operadora_w),ds_observacao_w,
								'N','P','S',dt_fim_vigencia_w);
					end if;
					end;
				end loop;
				close c02;
				
				pls_migrar_sca_benef(nr_seq_pessoa_proposta_p, nr_seq_segurado_novo_w, nm_usuario_p);
			end if;
			
			pls_gerar_regra_alt_acomod(nr_seq_segurado_novo_w, nr_seq_plano_ant_w, dt_migracao_p, 'M', cd_estabelecimento_p, nm_usuario_p);
			
			pls_gerar_segurado_historico(
				nr_seq_segurado_novo_w, '23', sysdate,
				wheb_mensagem_pck.get_texto(1108080), wheb_mensagem_pck.get_texto(1108081, 'NR_CONTRATO_ANT=' || nr_contrato_ant_w || ';CD_USUARIO_PLANO_ANT=' || nr_contrato_ant_w), dt_migracao_p,
				null, null, null,
				dt_migracao_p, nr_seq_plano_ant_w, nr_seq_contrato_ant_w,
				null, null, null,
				null, nm_usuario_p, 'N');
			
			pls_gerar_segurado_historico(
				nr_seq_segurado_ant_w, '5', sysdate,
				wheb_mensagem_pck.get_texto(1108080), wheb_mensagem_pck.get_texto(1108085) || ': ' || nr_contrato_mig_w || ' (NR_CONTRATO)', dt_migracao_p,
				null, nr_seq_segurado_novo_w, null,
				dt_migracao_p, nr_seq_plano_ant_w, nr_seq_contrato_ant_w,
				null, null, null,
				null, nm_usuario_p, 'N');
			
			if	(ie_nao_rescindir_benef_p	= 'N') then
				update	pls_segurado
				set	dt_migracao		= dt_migracao_p,
					dt_rescisao		= decode(ie_rescisao_w,'A',dt_limite_util_w,dt_rescisao),
					dt_limite_utilizacao	= decode(ie_rescisao_w,'A',dt_limite_util_w,dt_limite_utilizacao),
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate,
					nr_seq_segurado_mig	= nr_seq_segurado_novo_w,
					nr_seq_motivo_cancelamento = decode(ie_rescisao_w,'A',nr_seq_motivo_canc_p,nr_seq_motivo_cancelamento),
					nr_contrato_migrado	= nr_contrato_mig_w,
					nr_seq_causa_rescisao	= nr_seq_causa_rescisao_p,
					ie_rescisao_migracao	= decode(ie_rescisao_migracao_w, 'N', 'N', 'S')
				where	nr_sequencia		= nr_seq_segurado_ant_w;
				
				update	pls_segurado_carteira
				set	dt_validade_carteira	= decode(ie_rescisao_w,'A',dt_limite_util_w,dt_validade_carteira),
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_seq_segurado		= nr_seq_segurado_ant_w
				and	dt_validade_carteira is null
				and	ie_gerar_validade_cartao_w = 'S';
				
				--Rescindir os SCAs do beneficiario - Para enviar exclusao no A300
				update	pls_sca_vinculo
				set	dt_fim_vigencia	= decode(ie_rescisao_w,'A',dt_migracao_p,dt_fim_vigencia),
					nm_usuario	= nm_usuario_p
				where	nr_seq_segurado	= nr_seq_segurado_ant_w
				and	dt_fim_vigencia is null;
				
				open C04;
				loop
				fetch C04 into
					nr_seq_seg_repasse_w,
					dt_fim_repasse_w;
				exit when C04%notfound;
					begin
					select	dt_rescisao
					into	dt_rescisao_w
					from	pls_segurado
					where	nr_sequencia	= nr_seq_segurado_ant_w;
					
					if	(dt_fim_repasse_w is null) then
						pls_finalizar_repasse_seg(nr_seq_seg_repasse_w,dt_rescisao_w,'N',nm_usuario_p);
					elsif	(dt_fim_repasse_w > dt_rescisao_w) then
						update	pls_segurado_repasse
						set	dt_fim_repasse	= dt_rescisao_w,
							nm_usuario	= nm_usuario_p,
							dt_atualizacao	= sysdate
						where	nr_sequencia	= nr_seq_seg_repasse_w;
					end if;
					end;
				end loop;
				close C04;
				if	(ie_rescisao_w = 'A') then
					pls_gerar_devolucao_mens(nr_seq_segurado_ant_w, null, 'B', nr_seq_motivo_canc_p,'N', nm_usuario_p, cd_estabelecimento_p);
				end if;
				
				pls_att_classif_dependencia(nvl(nr_seq_titular_ant_w, nr_seq_segurado_ant_w),nm_usuario_p,'N'); -- Caso o beneficiario anterior seja rescindido, deve recalcular a classificacao de dependencia
				
				ie_gerar_valor_w := 'S';
			else
				update	pls_segurado
				set	dt_migracao		= dt_migracao_p,
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate,
					nr_seq_segurado_mig	= nr_seq_segurado_novo_w,
					nr_contrato_migrado	= nr_contrato_mig_w,
					nr_seq_causa_rescisao	= nr_seq_causa_rescisao_p
				where	nr_sequencia		= nr_seq_segurado_ant_w;
			end if;
			
			if	(nvl(nr_seq_pessoa_proposta_p,0) > 0) then
				update	pls_proposta_beneficiario
				set	nr_seq_benef_gerado	= nr_seq_segurado_novo_w
				where	nr_sequencia		= nr_seq_pessoa_proposta_p;
			end if;
		else
			if (nr_seq_titular_ant_w is not null and trunc(dt_rescisao_w,'dd') <> trunc(dt_rescisao_titular_w,'dd')) then
				select	length(ds_mensagem_w || wheb_mensagem_pck.get_texto(1108090, 'NM_BENEFICIARIO=' || nm_segurado_w) || chr(13))
				into	qt_tamanho_mensagem_w
				from	dual;
				
				if	(qt_tamanho_mensagem_w < 255) then
					ds_mensagem_w := ds_mensagem_w || wheb_mensagem_pck.get_texto(1108090, 'NM_BENEFICIARIO=' || nm_segurado_w) || chr(13) ;
				end if;
			else
				select	length(ds_mensagem_w || wheb_mensagem_pck.get_texto(1108091, 'NM_BENEFICIARIO=' || nm_segurado_w || ';QT_DIAS=' || qt_dias_rescisao_mig_w) || chr(13))
				into	qt_tamanho_mensagem_w
				from	dual;
				
				if	(qt_tamanho_mensagem_w < 255) then
					ds_mensagem_w := ds_mensagem_w || wheb_mensagem_pck.get_texto(1108091, 'NM_BENEFICIARIO=' || nm_segurado_w || ';QT_DIAS=' || qt_dias_rescisao_mig_w) || chr(13);
				end if;
			end if;
		end if;
		end;
	end loop;
	close c01;
	
	if	(ie_gerar_valor_w = 'S') then
		pls_preco_beneficiario_pck.atualizar_preco_beneficiarios(nr_seq_segurado_atual_p, null, null, null, dt_migracao_p, null, 'N', nm_usuario_p, cd_estabelecimento_p);
	end if;
end if;

if	((nvl(nr_seq_alteracao_motivo_p,0) <> 0) and
	(nvl(nr_seq_segurado_novo_w,0) <> 0)) then
	pls_inserir_rescicao_pagador(nr_seq_alteracao_motivo_p,nr_seq_pagador_novo_p,nvl(nr_seq_titular_novo_w, nr_seq_segurado_novo_w),nr_seq_motivo_rescisao_pag_p, null, dt_migracao_p, nm_usuario_p, nvl(qt_meses_contribuicao_p,0), ds_erro_w);
end if;

ds_retorno_p 		:= ds_mensagem_w;
nr_seq_segurado_novo_p	:= nvl(nr_seq_titular_novo_w, nr_seq_segurado_novo_w);

end pls_migrar_segurado_contrato;
/