create or replace
procedure pls_obter_se_proc_estrutura
				(    	cd_procedimento_p		number,
					ie_origem_proced_p		number,
					cd_area_p			number,
					cd_especialidade_p		number,
					cd_grupo_p			number,		
					ie_estrutura_p		out	varchar2 ) is 

cd_area_procedimento_out_w	number(10);
cd_especialidade_out_w		number(10);
cd_grupo_proc_out_w		number(10);
ie_origem_proced_out_w		number(10);
ie_ok_estrutura_w		varchar2(1):= 'S';

/*
CRIADO PARA SER UTILIZADO NAS REGRAS DE OCORRÊNCIA.;
*/

begin

pls_obter_estrut_proc(	cd_procedimento_p,
			ie_origem_proced_p,
			cd_area_procedimento_out_w, 
			cd_especialidade_out_w,
			cd_grupo_proc_out_w,
			ie_origem_proced_out_w);

if	(nvl( cd_area_p,0)>0) then
	if	( cd_area_p <> nvl(cd_area_procedimento_out_w,0)) then
		ie_ok_estrutura_w	:= 'N';
		goto final;	
	end if;		
end if;

if 	(nvl( cd_especialidade_p,0) >0) then
	if	( cd_especialidade_p <> nvl( cd_especialidade_out_w,0)) then
		ie_ok_estrutura_w	:= 'N';
		goto final;
	end if;
end if;	

if ( nvl( cd_grupo_p,0)>0) then
	if	( cd_grupo_p <> nvl(cd_grupo_proc_out_w,0)) then
		ie_ok_estrutura_w 	:= 'N';
		goto final;
	end if;
end if;

<<final>>
ie_estrutura_p	:= ie_ok_estrutura_w;

end pls_obter_se_proc_estrutura;
/