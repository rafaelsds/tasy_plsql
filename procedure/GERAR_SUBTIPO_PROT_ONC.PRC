create or replace
procedure gerar_subtipo_prot_onc(	cd_protocolo_p		number,
					ds_subtipo_p		varchar2,
					nr_seq_paciente_p	number,
					ie_copia_sol_p		varchar2,
					ie_copia_med_p		varchar2,
					ie_copia_proc_exa_p	varchar2,
					ie_copia_rec_p		varchar2,
					nm_usuario_p		Varchar2) is 

nr_sequencia_w		number(10);

begin
select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	protocolo_medicacao
where	nvl(cd_protocolo,cd_protocolo_p) = cd_protocolo_p;

if	(nvl(nr_sequencia_w,0) > 0) and
	(nvl(cd_protocolo_p,0) > 0) and
	(Trim(ds_subtipo_p) is not null) then
	
	insert into protocolo_medicacao	(
				nr_seq_interna,
				cd_protocolo,
				ie_situacao,
				nm_medicacao,
				nr_ciclos,
				nr_sequencia)
			values (
				protocolo_medicacao_seq.nextval,
				cd_protocolo_p,
				'A',
				substr(ds_subtipo_p,1,255),
				1,
				nr_sequencia_w);
	commit;
	
	gerar_protocolo_onc(cd_protocolo_p, nr_sequencia_w, nr_seq_paciente_p, ie_copia_sol_p, ie_copia_med_p, ie_copia_proc_exa_p, ie_copia_rec_p, nm_usuario_p);
	
end if;

end gerar_subtipo_prot_onc;
/