create or replace
procedure uvsf_importar_medico is 

cd_municipio_ibge_w	varchar2(6);
cd_pessoa_fisica_w	varchar2(10);
nr_sequencia_w		number(3);
cd_sistema_ant_w	varchar2(13);
cd_especialidade_w	number(5);
nm_trigger_w		varchar2(30);
ds_erro_w		varchar2(255);	
nr_cep_cidade_nasc_w	number(8);
nr_crm_w		varchar2(20);
uf_crm_w		varchar2(2);

cursor C01 is
select	cd_sistema_ant,
	nm_pessoa_fisica,
	to_date(dt_nascimento,'dd/mm/yyyy') dt_nascimento,
	ie_sexo,
	nm_contato_1 nm_pai,
	nm_contato_2 nm_mae,
	substr(nr_cpf,1,11) nr_cpf,
	decode(ie_grau_instrucao,'Superior Completo',4,'P�s.Grad. Completa',12,null) ie_grau_instrucao,
	decode(ie_estado_civil,'Solteiro',1,'Casado',2,'Divorciado',3,'Vi�vo',5,'') ie_estado_civil,
	nr_identidade,
	substr(ds_orgao_emissor_ci,1,10) ds_orgao_emissor_ci,
	ds_unidade_federacao,
	to_date(dt_emissao_ci,'dd/mm/yyyy') dt_emissao_ci,
	ds_cidade_nasc
from	uvsf_medico;

vet01	C01%RowType;

cursor C02 is
select	ds_endereco,
	nr_logradouro,
	ds_complemento,
	ds_bairro,
	ds_municipio,
	ds_unidade_federacao,
	cd_cep,
	decode(ie_tipo_complemento,'Residencial',1) ie_tipo_complemento
from	uvsf_end_res
where	cd_sistema_ant	= cd_sistema_ant_w;

vet02	C02%RowType;

cursor C03 is
select	ds_endereco,
	nr_logradouro,
	ds_complemento,
	ds_bairro,
	ds_municipio,
	ds_unidade_federacao,
	cd_cep,
	decode(ie_tipo_complemento,'Comercial',2) ie_tipo_complemento
from	uvsf_end_com
where	cd_sistema_ant	= cd_sistema_ant_w;

vet03	C03%RowType;

cursor C04 is
select	ds_especialidade
from	uvsf_especialidade
where	cd_sistema_ant	= cd_sistema_ant_w;

vet04	C04%RowType;

begin

delete	from	medico_especialidade
where	nm_usuario	= 'Importacao';

delete	from	medico
where	nm_usuario_nrec	= 'Importacao';

delete	from	compl_pessoa_fisica
where	nm_usuario_nrec	= 'Importacao';

delete	from	pessoa_fisica
where	nm_usuario_nrec	= 'Importacao';

commit;

wheb_usuario_pck.set_ie_executar_trigger('N');

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
		
	nr_sequencia_w		:= 0;
	cd_sistema_ant_w	:= vet01.cd_sistema_ant;
	nr_cep_cidade_nasc_w	:= null;
	
	if	(vet01.ds_cidade_nasc is not null) then
	
		select	max(cd_cep)
		into	nr_cep_cidade_nasc_w
		from	cep_Loc
		where	upper(nm_localidade) = upper(vet01.ds_cidade_nasc);
		
	end if;
	
	select	pessoa_fisica_seq.nextval
	into	cd_pessoa_fisica_w
	from	dual;
	
	begin
	insert into pessoa_fisica(	
		cd_pessoa_fisica,
		ie_tipo_pessoa,
		nm_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		dt_nascimento,
		ie_sexo,
		ie_estado_civil,
		nr_cpf,
		nr_identidade,
		ie_grau_instrucao,
		ds_orgao_emissor_ci,
		cd_sistema_ant,
		nm_pessoa_pesquisa,
		dt_emissao_ci,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nm_pessoa_fisica_sem_acento,
		nr_cep_cidade_nasc)
	values(	cd_pessoa_fisica_w,
		2,
		vet01.nm_pessoa_fisica,
		sysdate,
		'Importacao',
		vet01.dt_nascimento,
		vet01.ie_sexo,
		vet01.ie_estado_civil,
		vet01.nr_cpf,
		vet01.nr_identidade,
		vet01.ie_grau_instrucao,
		vet01.ds_orgao_emissor_ci,
		vet01.cd_sistema_ant,
		substr(padronizar_nome(vet01.nm_pessoa_fisica),1,60),
		vet01.dt_emissao_ci,
		sysdate,
		'Importacao',
		substr(elimina_acentuacao(vet01.nm_pessoa_fisica),1,60),
		nr_cep_cidade_nasc_w);
	exception when others then
	
		ds_erro_w	:= sqlerrm(sqlcode);
		insert into log_tasy (	
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values(	sysdate,
			'Tasy',
			4909,
			'pessoa_fisica_insert :'|| ds_erro_w);
	
	end;
		
	nr_sequencia_w	:= nr_sequencia_w + 1;
	if	(nvl(vet01.nm_pai,'X') <> 'X') then
	
		begin
		insert into compl_pessoa_fisica (	
			cd_pessoa_fisica,
			nr_sequencia,
			ie_tipo_complemento,
			nm_contato,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	cd_pessoa_fisica_w,
			nr_sequencia_w,
			4,
			vet01.nm_pai,
			sysdate,
			'Importacao',
			sysdate,
			'Importacao');
		exception when others then
	
			ds_erro_w	:= sqlerrm(sqlcode);
			insert into log_tasy (	
				dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
			values(	sysdate,
				'Tasy',
				4909,
				'compl_insert_1 :'||ds_erro_w);
	
		end;
	end if;
		
		/* Gravar complemento da M�e */
	if	(nvl(vet01.nm_mae,'X') <> 'X') then
		begin
		
		nr_sequencia_w	:= nr_sequencia_w + 1;
	
		insert into compl_pessoa_fisica (	
			cd_pessoa_fisica,
			nr_sequencia,
			ie_tipo_complemento,
			nm_contato,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	cd_pessoa_fisica_w,
			nr_sequencia_w,
			5,
			vet01.nm_mae,
			sysdate,
			'Importacao',
			sysdate,
			'Importacao');
		exception when others then
			ds_erro_w	:= sqlerrm(sqlcode);
			insert into log_tasy (	
				dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
			values(	sysdate,
				'Tasy',
				4909,
				'compl_insert_2 :'||ds_erro_w);
		end;
	end if;
		
		/* Gerar complemento residencial */
		open C02;
		loop
		fetch C02 into	
			vet02;
		exit when C02%notfound;
			begin
			nr_sequencia_w		:= nr_sequencia_w + 1;
			cd_municipio_ibge_w	:= '';
			if	(vet02.ds_municipio is not null) then
			
				select	nvl(max(cd_municipio_ibge),'')
				into	cd_municipio_ibge_w
				from	sus_municipio
				where	upper(ds_municipio) = upper(vet02.ds_municipio);
			end if;
			
			begin
			insert into compl_pessoa_fisica(
				cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				dt_atualizacao,
				nm_usuario,
				ds_endereco,
				cd_cep,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				sg_estado,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_municipio_ibge)
			values(	cd_pessoa_fisica_w,
				nr_sequencia_w,
				1,
				sysdate,
				'Importacao',
				vet02.ds_endereco,
				vet02.cd_cep,
				vet02.ds_complemento,
				vet02.ds_bairro,
				vet02.ds_municipio,
				vet02.ds_unidade_federacao,
				sysdate,
				'Importacao',
				cd_municipio_ibge_w);
			exception when others then
			
				ds_erro_w	:= sqlerrm(sqlcode);
				insert into log_tasy (	
					dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
				values(	sysdate,
					'Tasy',
					4909,
					'compl_insert_3 :'||ds_erro_w);
			end;
			end;
		end loop;
		close C02;
		
		/* Gerar complemento Comercial */
		open C03;
		loop
		fetch C03 into	
			vet03;
		exit when C03%notfound;
			begin
			nr_sequencia_w	:= nr_sequencia_w + 1;
			insert into compl_pessoa_fisica(cd_pessoa_fisica,
				nr_sequencia,
				ie_tipo_complemento,
				dt_atualizacao,
				nm_usuario,
				ds_endereco,
				cd_cep,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				sg_estado,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values(	cd_pessoa_fisica_w,
				nr_sequencia_w,
				2,
				sysdate,
				'Importacao',
				vet03.ds_endereco,
				vet03.cd_cep,
				vet03.ds_complemento,
				vet03.ds_bairro,
				vet03.ds_municipio,
				vet03.ds_unidade_federacao,
				sysdate,
				'Importacao');
			exception when others then
			
			ds_erro_w	:=  sqlerrm(sqlcode);
			insert into log_tasy (	
				dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
			values(	sysdate,
				'Tasy',
				4909,
				'compl_insert_4 :'||ds_erro_w);
			
			end;
			
		end loop;
		close C03;
		
		ds_erro_w	:= '';
		
		select	nvl(max(nr_crm),'')
		into	nr_crm_w
		from	uvsf_crm
		where	cd_sistema_ant	= cd_sistema_ant_w;

		select	max(uf_crm)
		into	uf_crm_w
		from	uvsf_crm
		where	cd_sistema_ant	= cd_sistema_ant_w
		and	nr_crm		= nr_crm_w;
		
		begin
		insert into medico(
			cd_pessoa_fisica,
			nr_crm,
			nm_guerra,
			ie_vinculo_medico,
			dt_atualizacao,
			nm_usuario,
			uf_crm,
			ie_situacao,
			ie_corpo_clinico,
			ie_corpo_assist,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	cd_pessoa_fisica_w,
			nr_crm_w,
			vet01.nm_pessoa_fisica,
			1,
			sysdate,
			'Importacao',
			uf_crm_w,
			'I',
			'N',
			'N',
			sysdate,
			'Importacao');
		exception when others then
		
			ds_erro_w	:= sqlerrm(sqlcode);
		
			insert into log_tasy (	
				dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
			values(	sysdate,
				'Tasy',
				4909,
				'medico_insert :'||ds_erro_w);
		
		end;
		
		if	(nvl(ds_erro_w,'X') = 'X') then
		
			open C04;
			loop
			fetch C04 into	
				vet04;
			exit when C04%notfound;
			
				select	nvl(max(b.cd_especialidade),0)
				into	cd_especialidade_w
				from	especialidade_medica b
				where	upper(b.ds_especialidade) = upper(vet04.ds_especialidade);
				
				if	(cd_especialidade_w <> 0) then
					begin
					insert into medico_especialidade (	
						cd_pessoa_fisica,
						cd_especialidade,
						dt_atualizacao,
						nm_usuario,
						nr_seq_prioridade)
					values(	cd_pessoa_fisica_w,
						cd_especialidade_w,
						sysdate,
						'Importacao',
						100);
					exception when others then
					
					ds_erro_w	:= sqlerrm(sqlcode);
					insert into log_tasy (	
						dt_atualizacao,
						nm_usuario,
						cd_log,
						ds_log)
					values(	sysdate,
						'Tasy',
						4909,
						'med_espec_insert :'||ds_erro_w);	
					end;
				end if;
			end loop;
			close C04;
		
		end if;
	end;
end loop;
close C01;

commit;
wheb_usuario_pck.set_ie_executar_trigger('S');

end uvsf_importar_medico;
/