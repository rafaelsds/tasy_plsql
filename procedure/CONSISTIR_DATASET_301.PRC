create or replace
procedure CONSISTIR_DATASET_301
			(nr_seq_estrut_arq_p	number,
			nr_seq_dataset_p	number,
			ie_dataset_p		varchar2,
			nm_usuario_p		varchar2)
			is

qt_rec_segmento_w	number(10);
qt_erro_w		number(10);
qt_alerta_w		number(10);
nr_seq_estrut_arq_w	number(10);
			
cursor c01 is
/*select	ie_elemento ie_segmento,
	ie_obrigatorio
from	table(RCM301_ARQUIVO_PCK.GET_SEGMENTOS_DATASET(ie_dataset_p))
order by nr_ordem;*/
select	nr_sequencia,
	ie_segmento,
	ie_obrigatorio
from	C301_ESTRUTURA_SEG_DATASET
where	nr_seq_estrut_arq	= nr_seq_estrut_arq_p
and	ie_dataset		= ie_dataset_p;

c01_w	c01%rowtype;

begin

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	Obter_valor_Dinamico(	'SELECT COUNT(*) QT FROM D301_SEGMENTO_'|| c01_w.ie_segmento ||
				' WHERE NR_SEQ_DATASET = '|| nr_seq_dataset_p, qt_rec_segmento_w);

	if	((c01_w.ie_obrigatorio = 'S') and (qt_rec_segmento_w = 0)) then
		--O segmento #@IE_SEGMENTO#@ � obrigat�rio para este dataset.
		GERAR_D301_DATASET_INCONSIST(	nr_seq_dataset_p,
						'E',						
						wheb_mensagem_pck.GET_TEXTO(996261,'IE_SEGMENTO='||c01_w.ie_segmento||';'),
						nm_usuario_p,
						c01_w.ie_segmento);
	end if;
	
	if	(qt_rec_segmento_w > 0) then
		CONSISTIR_SEGMENTO_301(c01_w.nr_sequencia,nr_seq_dataset_p,c01_w.ie_segmento,nm_usuario_p);
	end if;	

end loop;
close C01;

begin
select	1
into	qt_erro_w
from	d301_dataset_inconsist
where	nr_seq_dataset	= nr_seq_dataset_p
and	ie_tipo	= 'E'
and	rownum = 1;
exception
when others then
	qt_erro_w := 0;
end;

begin
select	1
into	qt_alerta_w
from	d301_dataset_inconsist
where	nr_seq_dataset	= nr_seq_dataset_p
and	ie_tipo	= 'A'
and	rownum = 1;
exception
when others then
	qt_alerta_w := 0;
end;

if	(qt_erro_w > 0) then
	
	update	d301_dataset_envio
	set	ie_status_validacao 	= 'E'
	where	nr_sequencia		= nr_seq_dataset_p;
	
elsif	(qt_alerta_w > 0) then
	update	d301_dataset_envio
	set	ie_status_validacao 	= 'A'
	where	nr_sequencia		= nr_seq_dataset_p;
else
	update	d301_dataset_envio
	set	ie_status_validacao 	= 'V',
		ie_status_envio		= 'P'
	where	nr_sequencia		= nr_seq_dataset_p;
end if;

end CONSISTIR_DATASET_301;
/