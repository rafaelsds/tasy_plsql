create or replace
procedure pls_obter_nm_comp_aud_grupo
		(	nr_seq_grupo_p			Number,
			nr_seq_analise_p		Number,
			nm_usuario_p			varchar2,
			nm_completo_auditor_grupo_p	out	varchar2	) is
	
ds_retorno_w	varchar2(255);

begin

select	max(nm_auditor_atual)
into	ds_retorno_w
from	pls_auditoria_conta_grupo
where	nr_sequencia 	 = (	select	max(nr_sequencia) 
				from	pls_auditoria_conta_grupo
				where	nr_seq_grupo 		= nr_seq_grupo_p	
				and	nr_seq_analise		= nr_seq_analise_p
				and	dt_liberacao is null);
				
nm_completo_auditor_grupo_p := obter_nome_usuario(ds_retorno_w) ;


end pls_obter_nm_comp_aud_grupo;
/
