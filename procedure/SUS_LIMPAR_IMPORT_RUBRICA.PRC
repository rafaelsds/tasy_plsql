CREATE OR REPLACE
PROCEDURE Sus_Limpar_Import_Rubrica
			(	nr_seq_protocolo_p	Number) is


qt_pagas_w		Number(5);
qt_rejeitadas_w		Number(5);


BEGIN

select	count(*)
into	qt_pagas_w
from	sus_aih_paga
where	nr_Seq_protocolo	= nr_seq_protocolo_p;

if	(qt_pagas_w	> 0) then
	delete	sus_aih_paga
	where	nr_seq_protocolo	= nr_seq_protocolo_p;
end if;

select	count(*)
into	qt_rejeitadas_w
from	sus_aih_rejeitada
where	nr_seq_protocolo	= nr_seq_protocolo_p;

if	(qt_rejeitadas_w	> 0) then
	delete	sus_aih_rejeitada
	where	nr_seq_protocolo	= nr_seq_protocolo_p;
end if;

delete	sus_aih_processo
where	nr_seq_protocolo	= nr_Seq_protocolo_p;

commit;

END Sus_Limpar_Import_Rubrica;
/