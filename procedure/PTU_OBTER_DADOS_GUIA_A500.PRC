create or replace
procedure ptu_obter_dados_guia_a500	(	nr_seq_guia_p			pls_guia_plano.nr_sequencia%type,
						cd_guia_p			pls_conta.cd_guia%type,
						cd_guia_referencia_p		pls_conta.cd_guia_referencia%type,
						nr_seq_conta_p			pls_conta.nr_sequencia%type,
						nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
						cd_senha_p		out	pls_guia_plano.cd_senha%type,
						ie_status_p		out	pls_guia_plano.ie_status%type,
						tp_autoriz_p		out	ptu_nota_servico.tp_autoriz%type,
						dt_autoriz_p		out	pls_guia_plano.dt_liberacao%type,
						dt_solicitacao_p	out	pls_guia_plano.dt_solicitacao%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

qt_registro_w	pls_integer;

begin
select	count(1)
into	qt_registro_w
from	ptu_pedido_autorizacao a
where	a.nr_seq_guia	= nr_seq_guia_p
and	a.nr_seq_execucao is not null;

if	(qt_registro_w = 0) then
	select	count(1)
	into	qt_registro_w
	from	pls_guia_plano	a,
		ptu_pedido_autorizacao b
	where	a.nr_sequencia		= b.nr_seq_guia
	and	a.cd_guia		= cd_guia_p
	and	a.nr_seq_segurado	= nr_seq_segurado_p
	and	b.nr_seq_execucao is not null;
end if;

if	(qt_registro_w = 0) then
	select	count(1)
	into	qt_registro_w
	from	pls_guia_plano	a,
		ptu_pedido_autorizacao b
	where	a.nr_sequencia		= b.nr_seq_guia
	and	a.cd_guia		= cd_guia_referencia_p
	and	a.nr_seq_segurado	= nr_seq_segurado_p
	and	b.nr_seq_execucao is not null;
end if;

if	(qt_registro_w = 0) then
	tp_autoriz_p := 1;
else
	tp_autoriz_p := 2;
end if;

select	nvl(max(cd_senha_externa),max(cd_senha)),
	max(ie_status),
	nvl(max(dt_liberacao),max(dt_autorizacao)),
	max(dt_solicitacao)
into	cd_senha_p,
	ie_status_p,
	dt_autoriz_p,
	dt_solicitacao_p
from	pls_guia_plano
where	nr_sequencia	= nr_seq_guia_p
and	nr_seq_segurado	= nr_seq_segurado_p;

if	(cd_senha_p is null) then
	select	nvl(max(cd_senha_externa),max(cd_senha)),
		max(ie_status)
	into	cd_senha_p,
		ie_status_p
	from	pls_guia_plano
	where	cd_guia		= cd_guia_p
	and	nr_seq_segurado	= nr_seq_segurado_p;
end if;

if	(cd_senha_p is null) then
	select	nvl(max(cd_senha_externa),max(cd_senha)),
		max(ie_status)
	into	cd_senha_p,
		ie_status_p
	from	pls_guia_plano
	where	cd_guia		= cd_guia_referencia_p
	and	nr_seq_segurado	= nr_seq_segurado_p;
end if;

cd_senha_p := somente_numero(substr(nvl(cd_senha_p,'0'),1,10));

end ptu_obter_dados_guia_a500;
/