create or replace
procedure inserir_status_fat_tasymed		(    nr_seq_faturamento_p	number,
						     nr_seq_status_p		number,
						     nm_usuario_p		Varchar2) is 

begin

if (nr_seq_faturamento_p is not null) and
   (nr_seq_status_p is not null) then
	
	update med_faturamento
	set    nr_seq_status   =  nr_seq_status_p
	where  nr_sequencia    =  nr_seq_faturamento_p;
	
end if;
	
commit;

end inserir_status_fat_tasymed;
/		