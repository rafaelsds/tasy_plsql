create or replace
procedure Consistir_regra_agenda_grupo(cd_agenda_p		number,
				       dt_agenda_p		date,
				       nr_seq_proc_interno_p	number, 	
				       nr_seq_agenda_p		number,
				       nm_usuario_p		Varchar2,
				       cd_estabelecimento_p	number,
				       nr_seq_adicional_p	number) is 

nr_seq_grupo_w		number(10,0) := 0;				       
qt_permitida_w		number(10,0);
qt_agendado_grupo_w	number(10,0);
qt_agendado_grupo_adic_w	number(10,0);
ie_dia_semana_w		number(1,0);
dt_inicio_agenda_w	date;
dt_fim_agenda_w		date;

qt_agendado_total_w	number(10,0);
				       
begin

ie_dia_semana_w	:= Obter_Cod_Dia_Semana(dt_agenda_p);

select	max(a.nr_sequencia)
into	nr_seq_grupo_w
from	regra_cons_agenda_grupo a,
	regra_cons_grupo_item b
where	b.nr_seq_grupo_consistencia 		= a.nr_sequencia
and	a.cd_agenda 				= cd_agenda_p
and	b.nr_seq_proc_interno 			= nr_seq_proc_interno_p
and	((nvl(a.ie_dia_semana,ie_dia_semana_w) = ie_dia_semana_w) or ((a.ie_dia_semana = 9) and (ie_dia_semana_w not in (7,1))))
and	dt_agenda_p between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(a.dt_inicio_agenda,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
			    to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(a.dt_fim_agenda,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss')
and	nvl(a.ie_situacao,'A')		= 'A';


if	(nr_seq_grupo_w > 0) then

	select	max(qt_consistencia),
		max(dt_inicio_agenda),
		max(dt_fim_agenda)
	into	qt_permitida_w,
		dt_inicio_agenda_w,
		dt_fim_agenda_w
	from	regra_cons_agenda_grupo
	where	nr_sequencia = nr_seq_grupo_w;
	
	select	count(*)
	into	qt_agendado_grupo_w
	from	agenda_paciente a
	where	a.cd_agenda = cd_agenda_p
	and	(((a.nr_sequencia <> nr_seq_agenda_p) and (nr_seq_adicional_p = 0)) or (nr_seq_adicional_p > 0))
	and	a.ie_status_agenda not in ('C','I','F')
	and	a.hr_inicio between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(dt_inicio_agenda_w,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
			    to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(dt_fim_agenda_w,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss')
	and	exists (select	1
			from	regra_cons_grupo_item x
			where	x.nr_seq_grupo_consistencia 	= nr_seq_grupo_w
			and	x.nr_seq_proc_interno		= a.nr_seq_proc_interno);		
			
			
	select	count(*)
	into	qt_agendado_grupo_adic_w
	from	agenda_paciente_proc b,
		agenda_paciente a
	where	a.nr_sequencia = b.nr_sequencia
	and	a.cd_agenda = cd_agenda_p
	and	a.ie_status_agenda not in ('C','I','F')
	and	a.hr_inicio between to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(dt_inicio_agenda_w,'hh24:mi:ss'),'00:00:00'),'dd/mm/yyyy hh24:mi:ss') and
			    to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || nvl(to_char(dt_fim_agenda_w,'hh24:mi:ss'),'23:59:59'),'dd/mm/yyyy hh24:mi:ss')
	and	b.nr_seq_proc_interno in (select	x.nr_seq_proc_interno
					from	regra_cons_grupo_item x
					where	x.nr_seq_grupo_consistencia 	= nr_seq_grupo_w);

	qt_agendado_total_w :=	qt_agendado_grupo_adic_w + qt_agendado_grupo_w; 	
			
	--r(-20011,qt_agendado_total_w||'#@#@');
										
	if	(qt_agendado_total_w >= qt_permitida_w) then
		Wheb_mensagem_pck.exibir_mensagem_abort(262530);		
	end if;

end if;

end Consistir_regra_agenda_grupo;
/
