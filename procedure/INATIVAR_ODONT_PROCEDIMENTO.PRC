create or replace
procedure inativar_odont_procedimento(	nr_sequencia_p		Number,
					nm_usuario_p		Varchar2) is 

begin

if (nr_sequencia_p is not null) then

	update	odont_procedimento
	set	dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end inativar_odont_procedimento;
/