create or replace procedure excluir_pasta_report_dar(nr_sequencia_p number) is

nr_seq_pai_w tree_report_dar.nr_seq_pai%type;

begin

select
	nr_seq_pai
into
	nr_seq_pai_w
from
	tree_report_dar
where
	nr_sequencia = nr_sequencia_p;

update
	tree_report_dar a
set
	a.nr_seq_pai = nr_seq_pai_w
where
	a.ie_tipo = 'R' and
	a.nr_sequencia in (
	select
		b.nr_sequencia
	from
		tree_report_dar b
	where b.ie_tipo = 'R'
	start with
		b.nr_sequencia = nr_sequencia_p
	connect by
		prior b.nr_sequencia = b.nr_seq_pai);

delete
	tree_report_dar a
where
	a.ie_tipo = 'P' and
	a.nr_sequencia in (
	select
		b.nr_sequencia
	from
		tree_report_dar b
	where b.ie_tipo = 'P'                            
	start with
		b.nr_sequencia = nr_sequencia_p
	connect by
		prior b.nr_sequencia = b.nr_seq_pai);

commit;
end excluir_pasta_report_dar;
/
