CREATE OR REPLACE
PROCEDURE hvc_gerar_razao_fornecedor(	dt_parametro_p   		DATE,
					nm_usuario_p			varchar2,
					ie_considera_cancel_p		varchar2,
					cd_estabelecimento_p		number,
					dt_inicial_p			date,
					dt_final_p			date,
					ie_somente_repasse_p		varchar2,
					ie_tipo_titulo_p		varchar2,
					nr_seq_conta_financ_p		number,
					ie_tipo_relatorio_p		varchar2,
					ie_data_mes_p			varchar2) IS

dt_inicio_w				date;
dt_final_w				date;
cd_cgc_w					varchar2(14);
cd_pessoa_fisica_w			varchar2(10);
vl_saldo_ant_w				number(15,2);
vl_saldo_atual_w				number(15,2);
ie_tipo_w					integer;
dt_movimento_w				date;
nr_documento_w				varchar2(40);
vl_movimento_w				number(15,2);
vl_credito_w				number(15,2);
vl_debito_w				number(15,2);
nr_sequencia_w				number(10,0);
nr_titulo_ant_w				number(10,0);
cd_conta_contabil_w			varchar2(20);
nr_titulo_w				varchar2(20);

/*Matheus OS 51243 em 02/03/2007 Inclus�o de cd_pessoa_fisica */
cursor Fornecedor is
	select	distinct
		cd_cgc,
		cd_pessoa_fisica,
		substr(obter_conta_contab_pf_pj(obter_empresa_estab(cd_estabelecimento_p),a.cd_cgc,a.cd_pessoa_fisica,'P',dt_inicial_p),1,100) conta_contabil
	from	titulo_pagar a
	where	dt_contabil < dt_final_w
	and	nvl(dt_liquidacao,sysdate) >= dt_inicio_w
	and	(cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N')
	and	(ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null);

cursor Movimento is
	select	1,
		a.dt_baixa,
		substr('Baixa T�tulo: ' || a.nr_titulo || '/' || nvl(b.nr_cheque,substr(obter_dados_nota_fiscal(c.nr_seq_nota_fiscal,'0'),1,100)),1,40),
		vl_baixa vl_movimento,
		a.nr_titulo
	from 	bordero_pagamento b,
		titulo_pagar_baixa a,
		titulo_pagar c
	where	a.nr_titulo = c.nr_titulo
	and	a.nr_bordero = b.nr_bordero(+)
	and	((cd_cgc_w is not null and c.cd_cgc = cd_cgc_w) or (cd_pessoa_fisica_w is not null and c.cd_pessoa_fisica = cd_pessoa_fisica_w))
	and	a.dt_baixa between dt_inicio_w and dt_final_w
	and	(c.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((c.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(c.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(c.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	a.cd_tipo_baixa		<> 10
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	c.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	2,
		nvl(b.dt_contabil, b.dt_atualizacao),
		substr('Adiantamento: ' || b.nr_titulo || '/' || nr_adiantamento,1,40),
		vl_adiantamento,
		a.nr_titulo
	from	titulo_pagar_adiant b,
		titulo_pagar a
	where	nvl(b.dt_contabil, b.dt_atualizacao) between dt_inicio_w and dt_final_w
	and	a.nr_titulo = b.nr_titulo
	and	(((cd_cgc_w is not null) and (a.cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (a.cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	(a.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((a.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(a.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(a.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	3,
		dt_alteracao,
		substr('Altera��o p/ Menor: ' || to_char(b.nr_titulo),1,40),
		abs(vl_anterior - vl_alteracao),
		a.nr_titulo
	from	titulo_pagar_alt_valor b,
		titulo_pagar a
	where	b.dt_alteracao between dt_inicio_w and dt_final_w
	and	(vl_anterior - vl_alteracao) > 0
	and	a.nr_titulo = b.nr_titulo
	and	(a.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	(((cd_cgc_w is not null) and (a.cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (a.cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	((a.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(a.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(a.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	4,
		dt_alteracao,
		substr('Altera��o p/ Maior: ' || to_char(b.nr_titulo),1,40),
		abs(vl_anterior - vl_alteracao),
		a.nr_titulo
	from	titulo_pagar_alt_valor b,
		titulo_pagar a
	where	b.dt_alteracao between dt_inicio_w and dt_final_w
	and	(vl_anterior - vl_alteracao) < 0
	and	a.nr_titulo = b.nr_titulo
	and	(((cd_cgc_w is not null) and (a.cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (a.cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	(a.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((a.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(a.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(a.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	5,
		nvl(dt_contabil, dt_emissao),
		substr(Decode((SELECT DISTINCT 1 FROM titulo_pagar WHERE nr_titulo_transf = a.nr_titulo), 1, 'Transferencia de T�tulo: ', 'Inclus�o de T�tulo: ') || nr_titulo || '/' || nvl(nr_documento,substr(obter_dados_nota_fiscal(nr_seq_nota_fiscal,'0'),1,100)),1,40),
		/*to_number(obter_dados_tit_pagar(nr_titulo,'VT')),  Francisco - 24/08/11 - Tem que considerar por data, troquei por outra function*/
		obter_valores_tit_pagar(a.nr_titulo,dt_inicio_w,'VOT'),
		a.nr_titulo
	from	titulo_pagar a
	where	nvl(dt_contabil, dt_emissao) between dt_inicio_w and dt_final_w
	and	(((cd_cgc_w is not null) and (cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	(cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	a.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null)
	union all
	select	6,
		a.dt_baixa,
		substr('T�tulo Transferido: ' || a.nr_titulo || '/' || nvl(b.nr_cheque,substr(obter_dados_nota_fiscal(c.nr_seq_nota_fiscal,'0'),1,100)) || ' (' || c.nr_titulo_transf || ')',1,40),
		vl_baixa vl_movimento,
		a.nr_titulo
	from 	bordero_pagamento b,
		titulo_pagar_baixa a,
		titulo_pagar c
	where	a.nr_titulo = c.nr_titulo
	and	a.nr_bordero = b.nr_bordero(+)
	and	((cd_cgc_w is not null and c.cd_cgc = cd_cgc_w) or (cd_pessoa_fisica_w is not null and c.cd_pessoa_fisica = cd_pessoa_fisica_w))
	and	a.dt_baixa between dt_inicio_w and dt_final_w
	and	(c.cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	((c.ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(c.ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(c.ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	and	a.cd_tipo_baixa		= 10
	and	(exists(	select	1
			from	titulo_pagar_classif x
			where	c.nr_titulo		= x.nr_titulo
			and	x.nr_seq_conta_financ	= nr_seq_conta_financ_p)
		or nr_seq_conta_financ_p	is null);
					  	-- Edgar/Francisco 09/03/2006 OS 30065, titulos desdobrados d�o lugar a outros...
						-- Anderson 26/09/2006 OS 39921 - inclui o parametro dos titulos cancelados.

BEGIN

dt_inicio_w	:= nvl(dt_inicial_p,trunc(dt_parametro_p, 'month'));
dt_final_w	:= fim_dia(nvl(dt_final_p,trunc(last_day(dt_parametro_p),'dd')));

select	max(nr_titulo)
into	nr_titulo_ant_w
from	titulo_pagar a
where	dt_contabil			< fim_dia(dt_final_w)
and	nvl(dt_liquidacao,sysdate)	>= dt_inicio_w
and	cd_estabelecimento		= cd_estabelecimento_p
and		exists (select	1
				from 	titulo_pagar_baixa x
				where	a.nr_titulo = x.nr_titulo
				and	(((x.dt_baixa < trunc(nvl(a.dt_contabil, a.dt_emissao), 'dd')) and ('N' = ie_data_mes_p))
					or ((x.dt_baixa < trunc(nvl(a.dt_contabil, a.dt_emissao), 'mm')) and (trunc(dt_parametro_p,'mm') = trunc(nvl(a.dt_contabil, a.dt_emissao), 'mm')) and ('S' = ie_data_mes_p))));


if	(nr_titulo_ant_w is not null) then
	raise_application_error(-20011,'O t�tulo '||nr_titulo_ant_w||' possui baixas com data inferior a data cont�bil/emiss�o do t�tulo');
end if;

delete from t_razao_fornecedor;

open Fornecedor;
loop
	fetch Fornecedor into
			cd_cgc_w,
			cd_pessoa_fisica_w,
			cd_conta_contabil_w;
	exit when Fornecedor%notfound;

	/*	Edgar 27/08/2008, retirei este bloco
	select  T_Razao_Fornecedor_seq.nextval
	into	nr_sequencia_w
	from	dual;
	*/
	/* obter saldo anterior */
	insert into T_Razao_Fornecedor(
		nr_sequencia,
		cd_cgc,
		cd_pessoa_fisica,
		ie_tipo,
		dt_atualizacao,
		nm_usuario,
		dt_movimento,
		nr_documento,
		vl_credito,
		vl_debito,
		cd_tipo_pessoa,
		CD_CONTA_CONTABIL)
	select	T_Razao_Fornecedor_seq.nextval,
		a.*
	from
	(select	cd_cgc_w cd_cgc,
		cd_pessoa_fisica_w cd_pessoa_fisica,
		0 ie_tipo,
		sysdate dt_atualizacao,
		nm_usuario_p nm_usuario,
		dt_inicio_w dt_inicio,
		'Saldo Anterior' nr_documento,
		sum(obter_saldo_titulo_pagar(nr_titulo,dt_inicio_w - 1/86400)) vl_credito,
		0 vl_debito,
		obter_dados_pf_pj(NULL, cd_cgc, 'TP') cd_tipo_pessoa,
		substr(obter_conta_contab_pf_pj(obter_empresa_estab(cd_estabelecimento_p),cd_cgc,cd_pessoa_fisica,'P',dt_inicial_p),1,100) CD_CONTA_CONTABIL
	from	titulo_pagar
	where	(	(cd_cgc_w is not null and cd_cgc = cd_cgc_w) or
			(cd_pessoa_fisica_w is not null and cd_pessoa_fisica = cd_pessoa_fisica_w)
		)
	and	(cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	nvl(dt_contabil, dt_emissao) < dt_inicio_w
	and	(	(ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
			(ie_situacao <> 'D' and 'S' = ie_considera_cancel_p)
		)
	and     (nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	group 	by obter_dados_pf_pj(NULL, cd_cgc, 'TP'),
	substr(obter_conta_contab_pf_pj(obter_empresa_estab(cd_estabelecimento_p),cd_cgc,cd_pessoa_fisica,'P',dt_inicial_p),1,100)) a;

	if	(nvl(ie_tipo_relatorio_p,'N') = 'N') then

		open Movimento;
		loop
			fetch Movimento into	ie_tipo_w,
						dt_movimento_w,
						nr_documento_w,
						vl_movimento_w,
						nr_titulo_w;
			exit when Movimento%notfound;

			vl_credito_w := 0;
			vl_debito_w := 0;

			if (ie_tipo_w <= 3) then
				vl_debito_w	:= vl_movimento_w;
			else
				vl_credito_w	:= vl_movimento_w;
			end if;

			select  T_Razao_Fornecedor_seq.nextval
			into	nr_sequencia_w
			from	dual;

			if	(cd_conta_contabil_w is null) then
				select 	substr(obter_conta_contab_pf_pj(obter_empresa_estab(cd_estabelecimento_p),cd_cgc_w,cd_pessoa_fisica_w,'P',dt_movimento_w),1,100)
				into	cd_conta_contabil_w
				from	dual;
			end if;

			begin
			insert into T_Razao_Fornecedor(
				nr_sequencia,
				cd_cgc,
				cd_pessoa_fisica,
				ie_tipo,
				dt_atualizacao,
				nm_usuario,
				dt_movimento,
				nr_documento,
				vl_credito,
				vl_debito,
				cd_tipo_pessoa,
				cd_conta_contabil,
				nr_titulo)
			values (nr_sequencia_w,
				cd_cgc_w,
				cd_pessoa_fisica_w,
				ie_tipo_w,
				sysdate,
				nm_usuario_p,
				dt_movimento_w,
				nr_documento_w,
				vl_credito_w,
				vl_debito_w,
				obter_dados_pf_pj(NULL, cd_cgc_w, 'TP'),
				cd_conta_contabil_w,
				nr_titulo_w);
			exception
				when others then
					dbms_output.put_line(vl_credito_w || '|' || vl_debito_w);
			end;
		end loop;
		close Movimento;
	end if;

	/*	Edgar 27/08/2008, retirei este bloco
	select	T_Razao_Fornecedor_seq.nextval
	into	nr_sequencia_w
	from	dual;
	*/

	/* obter saldo atual */
	insert into T_Razao_Fornecedor(
		nr_sequencia,
		cd_cgc,
		cd_pessoa_fisica,
		ie_tipo,
		dt_atualizacao,
		nm_usuario,
		dt_movimento,
		nr_documento,
		vl_credito,
		vl_debito,
		cd_tipo_pessoa,
		cd_conta_contabil)
	select	T_Razao_Fornecedor_seq.nextval,
		a.*
	from
	(select	cd_cgc_w cd_cgc,
		cd_pessoa_fisica_w cd_pessoa_fisica,
		99 ie_tipo,
		sysdate dt_atualizacao,
		nm_usuario_p nm_usuario,
		dt_final_w dt_mocimento,
		'Saldo Atual' nr_documento,
		sum(obter_saldo_titulo_pagar(nr_titulo,dt_final_w)) vl_credito,
		0 vl_debito,
		obter_dados_pf_pj(NULL, cd_cgc, 'TP') cd_tipo_pessoa,
		substr(obter_conta_contab_pf_pj(obter_empresa_estab(cd_estabelecimento_p),cd_cgc,cd_pessoa_fisica,'P',dt_inicial_p),1,100) CD_CONTA_CONTABIL
	from 	titulo_pagar
	where	(((cd_cgc_w is not null) and (cd_cgc = cd_cgc_w)) or ((cd_pessoa_fisica_w is not null) and (cd_pessoa_fisica = cd_pessoa_fisica_w)))
	and	(cd_estabelecimento = cd_estabelecimento_p or 0=cd_estabelecimento_p)
	and	nvl(dt_contabil, dt_emissao) 	<= dt_final_w
	and	((ie_situacao not in('D','C') and 'N' = ie_considera_cancel_p) or
		(ie_situacao <> 'D' and 'S' = ie_considera_cancel_p))
	and	(nr_repasse_terceiro is not null or nvl(ie_somente_repasse_p,'N') = 'N' )
	and	(ie_tipo_titulo = ie_tipo_titulo_p or ie_tipo_titulo_p is null)
	/*and	not exists 
			(select	1
			from	titulo_pagar_baixa x
			where	x.nr_titulo		= nr_titulo
			and	x.cd_tipo_baixa		= 10)*/
	group by obter_dados_pf_pj(NULL, cd_cgc, 'TP'),
		substr(obter_conta_contab_pf_pj(obter_empresa_estab(cd_estabelecimento_p),cd_cgc,cd_pessoa_fisica,'P',dt_inicial_p),1,100)) a;

/*	and	nvl(dt_liquidacao,sysdate) >= dt_inicio_w; F�bio 07/03/2005 OS15754 */
end loop;
close Fornecedor;

commit;

END HVC_Gerar_Razao_Fornecedor;
/
