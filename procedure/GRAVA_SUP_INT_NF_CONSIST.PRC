create or replace procedure grava_sup_int_nf_consist(
		nr_sequencia_p		number,
		ds_mensagem_p		varchar2) is

nr_inconsistencia_w		number(5);

begin

select	nvl(max(nr_inconsistencia),0) +1
into	nr_inconsistencia_w
from	sup_int_nf_consist
where	nr_sequencia = nr_sequencia_p;

insert into sup_int_nf_consist(
	nr_sequencia,
	nr_inconsistencia,
	ds_mensagem,
	dt_atualizacao,
	nm_usuario)
values(	nr_sequencia_p,
	nr_inconsistencia_w,
	ds_mensagem_p,
	sysdate,
	'INTEGR_TASY');

end grava_sup_int_nf_consist;
/