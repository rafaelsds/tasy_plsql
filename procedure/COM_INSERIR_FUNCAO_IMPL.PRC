create or replace
procedure com_inserir_funcao_impl (
				nr_seq_mod_impl_p	in	number,
				ds_lista_funcao_p	in	varchar2,
				nm_usuario_p		in	varchar2) is

nr_sequencia_w			number(10);
ds_lista_funcao_w		varchar2(800);
cd_funcao_w			number(10);
tam_lista_w			number(10);
ie_pos_virgula_w		number(3)	:= 0;
pr_funcao_modulo_w		Number(17,2);

begin

ds_lista_funcao_w	:= ds_lista_funcao_p;
if	(ds_lista_funcao_w is not null) then
	begin
	WHILE	(ds_lista_funcao_w is not null) LOOP
		begin

		tam_lista_w			:=	length(ds_lista_funcao_w);
		ie_pos_virgula_w		:=	instr(ds_lista_funcao_w,',');
	
		if	(ie_pos_virgula_w <> 0) then
			cd_funcao_w		:= substr(ds_lista_funcao_w,1,(ie_pos_virgula_w - 1));
			ds_lista_funcao_w	:= substr(ds_lista_funcao_w,(ie_pos_virgula_w + 1),tam_lista_w);
		end if;

		select	com_cliente_fun_impl_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert	into com_cliente_fun_impl(
			nr_sequencia,
			nr_seq_mod_impl,
			dt_atualizacao,
			nm_usuario,
			cd_funcao,
			dt_inicio_util)
		values( nr_sequencia_w,
			nr_seq_mod_impl_p,
			sysdate,
			nm_usuario_p,
			cd_funcao_w,
			sysdate);
		
		end;
	END loop;
	end;
	
	select	nvl(sum(pr_funcao_modulo),0)
	into	pr_funcao_modulo_w
	from	funcao a,
		com_cliente_fun_impl b
	where	a.cd_funcao		= b.cd_funcao
	and	b.nr_seq_mod_impl	= nr_seq_mod_impl_p;

	update	com_cliente_mod_impl
	set	pr_utilizacao		= pr_funcao_modulo_w,
		dt_atualizacao	= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_mod_impl_p;

	commit;
end if;

END com_inserir_funcao_impl;
/