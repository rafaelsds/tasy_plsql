CREATE OR REPLACE
PROCEDURE Verifica_Regra_via_adm(
				nr_prescricao_p		Number,
				ds_mensaguem_p	out	Varchar2) IS

cd_estabelecimento_w	Number(4);
qt_regras_w		Number(10);
qt_limite_w		Number(5);
ds_mensagem_w		Varchar(255);
ds_horarios_w		Varchar2(2000);
qt_dose_w		Number(18,6);
nr_sequencia_w		Number(10);
k			Integer;
ds_horario_w		Varchar2(10);
ds_retorno_w		varchar2(5000);
ds_mensagem_aux_w	varchar2(1000) := ' ';

cursor c01 is
select	a.qt_limite,
	a.ds_mensagem,
	padroniza_horario(b.ds_horarios),
	b.qt_dose,
	a.nr_sequencia
from	prescr_material b,
	regra_via_horario a
where	a.ie_via_aplicacao	= b.ie_via_aplicacao
and	b.nr_prescricao		= nr_prescricao_p
and	b.cd_unidade_medida_dose in (	select	c.cd_unidade_medida
	   				from	regra_via_hor_unid_med c
					where	a.nr_sequencia = c.nr_seq_regra);

cursor c02 is
select	sum(qt_dose),
	qt_max_regra,
	ds_horario,
	ds_mensagem
from	w_regra_via_adm
group by qt_max_regra,
	ds_horario,
	ds_mensagem
order by ds_mensagem;
begin

select	max(nvl(cd_estabelecimento,1))
into	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	count(*)
into	qt_regras_w
from	regra_via_horario
where	cd_estabelecimento	= cd_estabelecimento_w;

delete w_regra_via_adm;
commit;

if	(qt_regras_w > 0) then
	open	c01;
	loop
	fetch	c01 into 
			qt_limite_w,
			ds_mensagem_w,
			ds_horarios_w,
			qt_dose_w,
			nr_sequencia_w;
	exit	when c01%notfound;
		begin
		if	(ds_horarios_w is not null) then
			begin
			while	ds_horarios_w is not null LOOP
				begin
				select instr(ds_horarios_w, ' ') into k from dual;
				if	(k > 1) and
					(substr(ds_horarios_w, 1, k -1) is not null) then
					ds_horario_w		:= substr(ds_horarios_w, 1, k-1);
					ds_horario_w		:= replace(ds_horario_w, ' ','');
					ds_horarios_w		:= substr(ds_horarios_w, k + 1, 2000);
				elsif	(ds_horarios_w is not null) then
					ds_horario_w 		:= replace(ds_horarios_w,' ','');
					ds_horarios_w		:= '';
				end if;

				insert into w_regra_via_adm(
					nr_seq_regra, 
					qt_max_regra, 
					ds_horario, 
					qt_dose,
					ds_mensagem)
				values(	nr_sequencia_w,
					qt_limite_w,
					ds_horario_w,
					qt_dose_w,
					ds_mensagem_w);
				end;
			end loop;
			end;
		end if;
		end;
	end loop;
	close c01;
	commit;
	open	c02;
	loop
	fetch	c02 into 
			qt_dose_w,
			qt_limite_w,
			ds_horarios_w,
			ds_mensagem_w;
	exit	when c02%notfound;
		begin
		if	(qt_dose_w > qt_limite_w) then
			if	(ds_mensagem_aux_w <> ds_mensagem_w) then
				ds_retorno_w := ds_retorno_w ||' '||ds_mensagem_w;
			end if;
			ds_mensagem_aux_w	:= ds_mensagem_w;
			ds_retorno_w		:= ds_retorno_w ||' '||ds_horarios_w;
		end if;
		end;
	end loop;
	close c02;
end if;

ds_mensaguem_p	:= ds_retorno_w;

end Verifica_Regra_via_adm; 
/
