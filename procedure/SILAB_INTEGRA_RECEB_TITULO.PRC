create or replace procedure silab_integra_receb_titulo is

cd_moeda_padrao_w 		parametro_contas_receber.cd_moeda_padrao%type;
pr_juro_padrao_w 		parametro_contas_receber.pr_juro_padrao%type;
pr_multa_padrao_w		parametro_contas_receber.pr_multa_padrao%type;
cd_tipo_taxa_juro_w		parametro_contas_receber.cd_tipo_taxa_juro%type;
cd_tipo_taxa_multa_w 	parametro_contas_receber.cd_tipo_taxa_multa%type;
nr_seq_cheque_w			cheque_cr.nr_seq_cheque%type;
nr_seq_movto_cartao_w	number(10);
nr_seq_trans_fin_w		number(10);
nr_seq_forma_pagto_w	number(10);
nr_seq_saldo_caixa_w	number(10);
nr_seq_caixa_receb_w	number(10);
qt_saldos_abertos_w		number(10);
vl_troco_w				number(15,2);

e_erro					exception;
ds_erro_w				varchar2(2000);
ds_msg_err_w			varchar2(255);

Cursor C01 is
	select	nr_sequencia,
		dt_recebimento,
		nr_seq_caixa,
		nm_usuario,
		cd_estabelecimento,
		nr_titulo,
		cd_agencia_bancaria,
		cd_banco,           
		cd_pessoa_fisica_ch,   
		cd_pessoa_fisica_cr,   
		ds_beneficiario_cheque,    
		dt_vencimento_cheque,     
		nr_cheque,          
		nr_conta,           
		vl_cheque,
		dt_primeira_parcela,
		dt_transacao_cr,
		ie_tipo_cartao,
		nr_cartao,
		nr_seq_bandeira,
		nr_seq_forma_pagto,
		nr_seq_trans_caixa,
		qt_parcela,
		vl_cartao,
		vl_especie,
		nr_autorizacao
	from	silab_recebimento_titulo
	where	ie_status_integracao <> 'S';

vet01	c01%rowtype;		
	
begin

begin
	open c01;
	loop
	fetch c01 into	
		vet01;
	exit when c01%notfound;
		begin
			-- Fecha saldos anteriores
			select 	count(1)
			into	qt_saldos_abertos_w
			from 	caixa_saldo_diario
			where 	nr_seq_caixa = vet01.nr_seq_caixa			
			and	dt_saldo < vet01.dt_recebimento
			and	dt_fechamento is null;
			
			if (qt_saldos_abertos_w > 0) then
				fechar_caixa_saldo_diario(vet01.nr_seq_caixa,
						null,
						vet01.dt_recebimento,
						vet01.nm_usuario);			
			end if;			
		
			-- Reabre o saldo caso esteja fechado
			select	nvl(max(nr_sequencia),0) 
			into	nr_seq_saldo_caixa_w
			from 	caixa_saldo_diario
			where 	dt_saldo = vet01.dt_recebimento 
			and	nr_seq_caixa = vet01.nr_seq_caixa
			and dt_fechamento is not null;	

			if (nr_seq_saldo_caixa_w > 0) then
				reabrir_caixa_saldo_diario(vet01.nr_seq_caixa,
						nr_seq_saldo_caixa_w,
						vet01.dt_recebimento,
						vet01.nm_usuario);
			end if;		
		
			-- Abre o saldo caso ainda n�o exista
			select	nvl(max(nr_sequencia),0) 
			into	nr_seq_saldo_caixa_w
			from 	caixa_saldo_diario
			where 	dt_saldo = vet01.dt_recebimento 
			and	nr_seq_caixa = vet01.nr_seq_caixa
			and dt_fechamento is null;		
					
			if (nr_seq_saldo_caixa_w = 0) then
				abrir_caixa_saldo_diario(vet01.nr_seq_caixa, 
						vet01.dt_recebimento,
						vet01.nm_usuario,
						nr_seq_saldo_caixa_w);
						
				if (nr_seq_saldo_caixa_w = 0) then
					ds_erro_w := 'N�o foi poss�vel abrir o saldo do caixa';
					raise e_erro;
				end if;
			end if;
			
			-- Gera o recebimento
			gerar_receb_tit_caixa(vet01.cd_estabelecimento, 
					nr_seq_saldo_caixa_w, 
					vet01.nr_titulo, 
					vet01.nm_usuario, 
					nr_seq_caixa_receb_w,
					vet01.vl_especie);
					
			if (nr_seq_caixa_receb_w = 0) then
				ds_erro_w := 'N�o foi poss�vel iniciar o recebimento';
				raise e_erro;
			end if;			
					
			-- Vinculado o t�tulo ao recebimento		
			baixar_titulos_caixa_rec(nr_seq_caixa_receb_w,
					vet01.nr_titulo,
					vet01.nr_titulo,
					vet01.nm_usuario,
					ds_erro_w);
					
			if (ds_erro_w is not null) then
				raise e_erro;
			end if;		

			-- Vincula o cheque ao recebimento
			if (vet01.nr_cheque is not null) then
			
				select	cd_moeda_padrao, 
					pr_juro_padrao, 
					pr_multa_padrao, 
					cd_tipo_taxa_juro, 
					cd_tipo_taxa_multa   
				into	cd_moeda_padrao_w, 
					pr_juro_padrao_w, 
					pr_multa_padrao_w, 
					cd_tipo_taxa_juro_w, 
					cd_tipo_taxa_multa_w  				
				from 	parametro_contas_receber 
				where 	cd_estabelecimento = vet01.cd_estabelecimento;			
			
				select 	cheque_cr_seq.nextval
				into	nr_seq_cheque_w
				from	dual;
			
				insert into cheque_cr
					(
					cd_agencia_bancaria,
					cd_banco,           
					cd_estabelecimento, 
					cd_moeda, 
					tx_juros_cobranca,
					tx_multa_cobranca,
					cd_tipo_taxa_juros,
					cd_tipo_taxa_multa,
					cd_pessoa_fisica,   
					ds_beneficiario,    
					ds_observacao,      
					dt_atualizacao,     
					dt_contabil,        
					dt_vencimento,     
					dt_vencimento_atual,
					dt_registro,
					ie_lib_caixa,         
					nm_usuario,         
					nr_cheque,          
					nr_conta,           
					nr_seq_caixa_rec,   
					nr_seq_cheque,      
					vl_cheque,          
					vl_terceiro        
					)
				values
					(
					vet01.cd_agencia_bancaria,
					vet01.cd_banco,
					vet01.cd_estabelecimento,
					cd_moeda_padrao_w,   
					pr_juro_padrao_w, 
					pr_multa_padrao_w, 
					cd_tipo_taxa_juro_w, 
					cd_tipo_taxa_multa_w,					
					vet01.cd_pessoa_fisica_ch,   
					vet01.ds_beneficiario_cheque,    
					'Cheque gerado automaticamente pela integra��o com sistema Silab',
					sysdate,     
					vet01.dt_vencimento_cheque,        
					vet01.dt_vencimento_cheque,     
					vet01.dt_vencimento_cheque,
					sysdate,
					'N',         
					vet01.nm_usuario,         
					vet01.nr_cheque,          
					vet01.nr_conta,           
					nr_seq_caixa_receb_w,   
					nr_seq_cheque_w,      
					vet01.vl_cheque,          
					0  				
					);
					
					gerar_cheque_cr_hist(nr_seq_cheque_w,
							'O cheque foi inserido em um recebimento na Tesouraria! Caixa: ' || vet01.nr_seq_caixa ||
							' Data do recebimento: ' || sysdate || ' Sequ�ncia do recebimento: ' || nr_seq_caixa_receb_w,
							'N',
							vet01.nm_usuario);
			end if;
			
			-- Vincula o cart�o ao recebimento
			if (vet01.nr_seq_bandeira is not null) then

				if (vet01.nr_seq_forma_pagto is null) then
				
					select 	nvl(max(a.nr_seq_forma),0) 
					into 	nr_seq_forma_pagto_w
					from	forma_pagto_regra a 
					where   a.nr_seq_bandeira = vet01.nr_seq_bandeira 
					and	nvl(a.cd_estabelecimento, vet01.cd_estabelecimento) = vet01.cd_estabelecimento;			

					if (nr_seq_forma_pagto_w > 0) then
						select 	nvl(max(to_number(obter_valor_bandeira_estab(nr_sequencia, vet01.cd_estabelecimento ,'NR_SEQ_TRANS_FIN_CAIXA'))),0)
						into	nr_seq_trans_fin_w
						from   	bandeira_cartao_cr a 
						where  	a.nr_sequencia = vet01.nr_seq_bandeira;					
					
						if (nvl(obter_se_trans_fin_caixa(vet01.nr_seq_caixa, nr_seq_trans_fin_w),'S') = 'N') then
							nr_seq_trans_fin_w := nvl(vet01.nr_seq_trans_caixa,0);
						end if;
					
					end if;
				else
					nr_seq_forma_pagto_w := vet01.nr_seq_forma_pagto;
				end if;

				if (vet01.nr_seq_trans_caixa is not null) then
					nr_seq_trans_fin_w := vet01.nr_seq_trans_caixa;
				end if;
							
				select 	movto_cartao_cr_seq.nextval
				into	nr_seq_movto_cartao_w
				from	dual;
				
				insert into movto_cartao_cr
					(
					cd_estabelecimento, 
					cd_pessoa_fisica,   
					ds_observacao,      
					dt_atualizacao,     
					dt_transacao,
					dt_liberacao,
					ie_lib_caixa,       
					ie_situacao,        
					ie_tipo_cartao,     
					nm_usuario,         
					nr_autorizacao,     
					nr_seq_bandeira,    
					nr_seq_caixa_rec,   
					nr_cartao,		
					nr_sequencia,       
					qt_parcela,         
					vl_transacao,       
					nr_seq_forma_pagto, 
					nr_seq_trans_caixa				
					)
				values
					(
					vet01.cd_estabelecimento,
					vet01.cd_pessoa_fisica_cr,
					'Movimento gerado automaticamente pela integra��o com sistema Silab',
					sysdate,
					vet01.dt_transacao_cr,
					sysdate,
					'N',
					'A',
					vet01.ie_tipo_cartao,
					vet01.nm_usuario,
					vet01.nr_autorizacao,
					vet01.nr_seq_bandeira,
					nr_seq_caixa_receb_w,
					vet01.nr_cartao,
					nr_seq_movto_cartao_w,
					vet01.qt_parcela,
					vet01.vl_cartao,
					nr_seq_forma_pagto_w,
					nr_seq_trans_fin_w				
					);

				consiste_cartao_cr_parcela(nr_seq_forma_pagto_w,
						vet01.nr_seq_bandeira,
						vet01.ie_tipo_cartao,
						vet01.qt_parcela,
						vet01.vl_cartao,
						vet01.nm_usuario);
				
				gerar_cartao_cr_parcela(nr_seq_movto_cartao_w,
						vet01.nm_usuario,
						vet01.dt_primeira_parcela);
				
				gerar_classif_cartao_cr(nr_seq_movto_cartao_w,
						vet01.nm_usuario);			
			end if;
			
			-- Confirma o recebimento e baixa o t�tulo
			fechar_caixa_receb(nr_seq_caixa_receb_w,
					'N',
					vet01.nm_usuario,
					vl_troco_w,
					null);
				
			-- Gera troco
			if (vl_troco_w < 0) then
				fechar_caixa_receb(nr_seq_caixa_receb_w,
						'S',
						vet01.nm_usuario,
						vl_troco_w,
						null);			
			end if;
					
			update	silab_recebimento_titulo
			set	ie_status_integracao = 'S',
				ds_erro = null,
				dt_integracao = sysdate,
				nr_seq_caixa_rec = nr_seq_caixa_receb_w
			where	nr_sequencia = vet01.nr_sequencia;
			
			commit;			
		end;
	end loop;
	close c01;

exception
	when e_erro then
		rollback;
		
		update	silab_recebimento_titulo
		set	ie_status_integracao = 'E',
			ds_erro = ds_erro_w
		where	nr_sequencia = vet01.nr_sequencia;
		
		commit;
		
	when others then
		rollback;
		
		ds_msg_err_w	:= substr(sqlerrm,1,255);
		
		update	silab_recebimento_titulo
		set	ie_status_integracao = 'E',
			ds_erro = ds_msg_err_w
		where	nr_sequencia = vet01.nr_sequencia;
		
		commit;
end;

end silab_integra_receb_titulo;
/