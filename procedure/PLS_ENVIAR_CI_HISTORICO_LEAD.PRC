create or replace
procedure pls_enviar_ci_historico_lead is 

nr_seq_rtf_w			number(10);
ds_historico_w			varchar2(4000);

Cursor C01 is
	select	a.nm_usuario_historico,
		a.nr_sequencia,
		substr(b.ds_atividade,1,255) ds_atividade,
		to_char(a.dt_programacao, 'dd/mm/yyyy hh24:mi:ss') dt_programacao,
		to_char(a.dt_historico, 'dd/mm/yyyy hh24:mi:ss') dt_historico
	from	pls_tipo_atividade		b,
		pls_solicitacao_historico 	a
	where	b.nr_sequencia = a.ie_tipo_atividade
	and	trunc(a.dt_programacao, 'dd') between trunc(sysdate, 'dd') and trunc(sysdate, 'dd') + 1
	and	a.dt_liberacao is not null
	and	a.nm_usuario_historico is not null
	and	b.ie_programacao = 'S';

begin
for r_c01_w in C01 loop
	
	--Retira a formatação gerada pelo campo Panel editor do histórico para que a comunicação interna seja gerada corretamente.
	converte_rtf_string('select ds_historico from pls_solicitacao_historico where nr_sequencia = :nr_seq_solicitacao_hist', r_c01_w.nr_sequencia, 'Tasy', nr_seq_rtf_w);
	
	begin
	select	ds_texto
	into	ds_historico_w
	from	tasy_conversao_rtf
	where	nr_sequencia = nr_seq_rtf_w;
	exception
		when others then
			null;
	end;
	
	insert into comunic_interna
		(dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nm_usuario_destino,
		cd_perfil,
		nr_sequencia,
		ie_gerencial,
		ds_setor_adicional,
		dt_liberacao,
		ds_grupo,
		nm_usuario_oculto)
	values	(sysdate,
		r_c01_w.ds_atividade || ' - Dt programação: ' || r_c01_w.dt_programacao,
		'Dt histórico: ' || r_c01_w.dt_historico || chr(13) || ds_historico_w,
		'Tasy',
		sysdate,
		'N',
		r_c01_w.nm_usuario_historico,
		null,
		comunic_interna_seq.nextval,
		'N',
		'',
		sysdate,
		'',
		'');
end loop;

commit;

end pls_enviar_ci_historico_lead;
/
