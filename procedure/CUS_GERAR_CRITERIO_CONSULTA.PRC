create or replace
procedure cus_gerar_criterio_consulta(	cd_estabelecimento_p	number,
				cd_tabela_custo_p		number,
				cd_sequencia_criterio_p		number,
				nr_seq_tabela_p			number,
				nm_usuario_p		varchar2) is

/*cursor din�mico */
c01				integer;
cd_centro_controle_w		number(8);
cd_centro_controle_ww		number(8);
cd_natureza_gasto_dest_w		number(20);
ds_erro_w			varchar2(2000);
ds_view_w			varchar2(300);
ds_sql_w				varchar2(2000);
dt_mes_referencia_w		date;
dt_final_w		date;
qt_distribuicao_w			number(15,4);
ie_retorno_w			number(1);
nr_seq_ng_dest_w			number(10);
qt_centro_controle_w		number(10);
cd_empresa_w			number(10);
cd_estab_tabela_w		number(10);
cd_estabelecimento_w		number(10);
qt_peso_w				criterio_distr_orc_dest.qt_peso%type;
qt_existe_campo_w		number(10);

begin

delete 	from criterio_distr_orc_dest
where	cd_sequencia_criterio = cd_sequencia_criterio_p;

/* Busca a view*/
select	nvl(max(ds_view),''),
	nvl(max(cd_natureza_gasto_dest),0),
	nvl(max(nr_seq_ng_dest),0),
	max(cd_centro_controle)
into	ds_view_w,
	cd_natureza_gasto_dest_w,
	nr_seq_ng_dest_w,
	cd_centro_controle_ww
from	criterio_distr_orc
where	cd_sequencia_criterio = cd_sequencia_criterio_p;

if	(ds_view_w is null) then
	--'Falta informar a view no crit�rio: ' || cd_sequencia_criterio_p);
	wheb_mensagem_pck.exibir_mensagem_abort(193024,'cd_sequencia_criterio_p='||cd_sequencia_criterio_p);
	
end if;

if	(cd_natureza_gasto_dest_w = 0) and
	(nr_seq_ng_dest_w = 0) then
	--'Para crit�rio de consulta, deve ser informado a Natureza de Gasto Destino');
	wheb_mensagem_pck.exibir_mensagem_abort(193025);
end if;

/*Obter o m�s da tabela de custo*/
select	max(dt_mes_referencia),
	max(cd_estabelecimento),
	max(cd_empresa)
into	dt_mes_referencia_w,
	cd_estab_tabela_w,
	cd_empresa_w
from	tabela_custo
where	nr_sequencia	= nr_seq_tabela_p;

dt_final_w		:= fim_mes(dt_mes_referencia_w);

begin
select 	1
into 	qt_existe_campo_w 
from 	user_tab_columns 
where 	table_name = ds_view_w 
and 	column_name = 'QT_PESO';
exception
when others then
	qt_existe_campo_w:= 0;
end;	



if(qt_existe_campo_w > 0) then
	ds_sql_w		:=	' select cd_estabelecimento, cd_centro_controle, nvl(sum(qt_distribuicao),0) qt_distribuicao, nvl(sum(qt_peso),0) qt_peso ';
else
	ds_sql_w		:=	' select cd_estabelecimento, cd_centro_controle, nvl(sum(qt_distribuicao),0) qt_distribuicao ';
end if;

ds_sql_w		:=	ds_sql_w ||	' from ' || ds_view_w ||
					' where 1 = 1 ' ||
					' and obter_empresa_estab(cd_estabelecimento) = :cd_empresa ';

if	(cd_estab_tabela_w is not null) then
	ds_sql_w	:= ds_sql_w || ' and cd_estabelecimento	= :cd_estab ';
end if;

ds_sql_w		:= ds_sql_w || 	' and dt_referencia	between :dt and :dt_final '	||
					' and cd_centro_controle 	<> :cd_centro'		||
					' group by cd_estabelecimento, cd_centro_controle';


				
/*cria o cursor*/
c01 := dbms_sql.open_cursor;

/* faz o parse do sql do cursor*/
begin
dbms_sql.parse(c01, ds_sql_w, dbms_sql.native);
exception when others then
	/*'Erro ao executar o SQL' || chr(13) || chr(10) || ds_sql_w || chr(13) || 'SQL:' || ds_view_w || chr(13) ||
					'Criterio:' || cd_sequencia_criterio_p);*/
	wheb_mensagem_pck.exibir_mensagem_abort(193026, 'ds_sql_w=' || ds_sql_w || ';' ||
							'ds_view_w=' || ds_view_w || ';' ||
							'cd_sequencia_criterio_p=' || cd_sequencia_criterio_p);
end;
/*define a coluna 1 com tipo de dado igual a da variavel cd_centro_controle_w*/
dbms_sql.define_column(c01, 1, cd_estabelecimento_w);
dbms_sql.define_column(c01, 2, cd_centro_controle_w);
dbms_sql.define_column(c01, 3, qt_distribuicao_w);
if(qt_existe_campo_w > 0) then
	dbms_sql.define_column(c01, 4, qt_peso_w);
end if;


if	(cd_estab_tabela_w is not null) then
	dbms_sql.bind_variable(c01,'CD_ESTAB',	cd_estab_tabela_w);
end if;
	
dbms_sql.bind_variable(c01,'CD_EMPRESA',	cd_empresa_w);
dbms_sql.bind_variable(c01,'DT',	dt_mes_referencia_w);
dbms_sql.bind_variable(c01,'DT_FINAL',	dt_final_w);
dbms_sql.bind_variable(c01,'CD_CENTRO',	cd_centro_controle_ww);


ie_retorno_w	:= dbms_sql.execute(c01); 
ie_retorno_w	:= dbms_sql.fetch_rows(c01); /*fetch rows � um next que retorna 1 ou 0*/
while (ie_retorno_w <> 0) loop
	begin
	dbms_sql.column_value(c01, 1, cd_estabelecimento_w);
	dbms_sql.column_value(c01, 2, cd_centro_controle_w);
	dbms_sql.column_value(c01, 3, qt_distribuicao_w);
	if(qt_existe_campo_w > 0) then
		dbms_sql.column_value(c01, 4, qt_peso_w);
	else
		qt_peso_w:= 1;
	end if;

	
	if	(cd_estabelecimento_w <> 0) and
		(cd_centro_controle_w <> 0) then
	
		insert into criterio_distr_orc_dest(
			cd_estabelecimento,
			cd_sequencia_criterio,
			cd_centro_controle_dest,
			qt_distribuicao,
			pr_distribuicao,
			cd_natureza_gasto_dest,
			nr_seq_ng_dest,
			dt_atualizacao,
			nm_usuario,
			qt_peso,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(cd_estabelecimento_w,
			cd_sequencia_criterio_p,
			cd_centro_controle_w,
			qt_distribuicao_w,
			0,
			cd_natureza_gasto_dest_w,
			nr_seq_ng_dest_w,
			sysdate,
			nm_usuario_p,
			qt_peso_w,
			sysdate,
			nm_usuario_p);
	end if;
	
	exception when others then
		ds_erro_w	:= sqlerrm(sqlcode);
		/*'Erro ao inserir os centros de destino!' || chr(13) || chr(10) ||
						'Centro: ' || cd_centro_controle_w || ' ERRO: ' || chr(13) || chr(10) ||
						ds_erro_w);*/
		wheb_mensagem_pck.exibir_mensagem_abort(193027, 'cd_centro_controle_w=' || cd_centro_controle_w || ';' ||
								'ds_erro_w=' || ds_erro_w);
			
	end;
	

	ie_retorno_w	:= dbms_sql.fetch_rows(c01);
	
end loop;
/*fecha o cursor */
dbms_sql.close_cursor(c01);

select	nvl(sum(qt_distribuicao),0)
into	qt_distribuicao_w
from	criterio_distr_orc_dest
where	cd_sequencia_criterio	= cd_sequencia_criterio_p;

Cus_calcular_Perc_Distr(cd_estabelecimento_p, cd_sequencia_criterio_p);

begin
update	criterio_distr_orc
set	qt_base_distribuicao	= qt_distribuicao_w
where	cd_sequencia_criterio	= cd_sequencia_criterio_p;
exception when others then
	--'Erro ao atualizar a base de distribui��o do crit�rio!');
	wheb_mensagem_pck.exibir_mensagem_abort(193028);
end;
commit;
end cus_gerar_criterio_consulta;
/