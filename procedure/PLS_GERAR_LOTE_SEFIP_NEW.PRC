create or replace 
procedure pls_gerar_lote_sefip_new
			(	nr_seq_lote_p		in	number,
				cd_estabelecimento_p	in	number,
				cd_darf_p		in	varchar2,
				nm_usuario_p		in	varchar2) is 


dt_inicial_w			date;
dt_final_w			date;
nr_seq_lote_w			number(10,0);
nr_seq_pag_prestador_w		number(10,0);
nr_seq_prestador_w		number(10,0);
nr_seq_tomador_w		number(10,0);
nr_seq_trib_w			number(10,0);
pr_tributo_w			number(15,4);
nr_seq_conta_w			number(10,0);
nr_seq_resumo_w			number(10,0);
ie_competencia_w		varchar2(255);
cd_cbo_saude_w			varchar2(255);
dt_lote_w			date;
ie_tipo_contratacao_w		varchar2(255);
cd_cgc_estipulante_w		varchar2(255);
cd_pf_estipulante_w		varchar2(255);
cd_cgc_outorgante_w		varchar2(255);
vl_liberado_w			number(15,2);
vl_descontado_w			number(15,2);
cd_tributo_w			tributo.cd_tributo%type;
vl_total_desconto_w		number(15,2);
vl_total_prod_w			number(15,2);	
ie_valor_base_trib_nf_w		varchar2(1);	
vl_remuneracao_w		number(15,2);
vl_imposto_w			number(15,2);
vl_descontado_ww		number(15,2);
cd_tributo_ant_w		tributo.cd_tributo%type;
vl_item_pr_w			number(15,2);
vl_tributo_real_w		number(15,2);
vl_item_prod_w			number(15,2);
vl_item_desc_w			number(15,2);
nr_seq_grupo_prestador_w	number(10);
nr_seq_prestador_pgto_w		number(10);
ie_gera_w			varchar2(1);
vl_trib_retido_w		number(15,2);
nr_seq_tomador_movto_w		number(10);

nr_seq_conta_ww			number(10);
nr_seq_resumo_ww		number(10);
vl_liberado_ww			number(15,2);
cd_cgc_estipulante_ww		varchar2(255);
vl_item_w			number(15,2);
vl_total_w			number(15,2);
nr_seq_movto_cta_w		number(10);
vl_descontar_w			number(15,2);

cursor c_lotes is
	select	nr_sequencia
	from	pls_lote_pagamento
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	dt_mes_competencia between dt_inicial_w and dt_final_w
	and	ie_status		= 'D';

cursor c_prestadores is
	select	a.nr_sequencia,
		a.nr_seq_prestador
	from	pls_prestador		b,
		pls_pagamento_prestador	a
	where	a.nr_seq_prestador	= b.nr_sequencia
	and	b.cd_pessoa_fisica	is not null
	and	a.nr_seq_lote		= nr_seq_lote_w
	and	a.ie_cancelamento	is null
	and	b.cd_pessoa_fisica is not null;
	--and	pls_obter_se_cooperado_ativo(b.cd_pessoa_fisica, dt_final_w, '') = 'S';
	/*and	exists	(select	1							-- gerar somente prestadores cooperados
			from	pls_cooperado x
			where	x.cd_pessoa_fisica	= b.cd_pessoa_fisica);*/

cursor c_tributos is
	select	nr_sequencia,
		pr_tributo,
		ie_tipo_contratacao,
		cd_tributo,
		vl_imposto,
		vl_base_calculo
	from	(select	a.nr_sequencia,
			decode(a.vl_imposto, 0, 0, a.pr_tributo) pr_tributo,
			a.ie_tipo_contratacao,
			b.cd_tributo,
			decode(a.ie_pago_prev, 'R', -1, a.vl_imposto) vl_imposto,
			decode(a.ie_tipo_contratacao, null, 2, 1) ie_tipo_contr_order,
			a.vl_base_calculo
		from	tributo				b,
			pls_pag_prest_venc_trib		a,
			pls_pag_prest_vencimento	x
		where	a.cd_tributo		= b.cd_tributo
		and	b.ie_tipo_tributo	= 'INSS'
		and	x.nr_sequencia		= a.nr_seq_vencimento
		/*and	a.nr_seq_vencimento in	(select	x.nr_sequencia
						from	pls_pag_prest_vencimento	x
						where	x.nr_seq_pag_prestador	= nr_seq_pag_prestador_w)*/
		and	x.nr_seq_pag_prestador	= nr_seq_pag_prestador_w
		and	a.ie_pago_prev	= 'V'
		and	((a.cd_darf	= nvl(cd_darf_p, a.cd_darf)) or 
			((cd_darf_p is null) and (a.cd_darf is null))))
	order by
		ie_tipo_contr_order, -- Gerar primeiramente os tributos que possuem tipo de contrata��o, depois os que n�o tem
		cd_tributo;	
				-- Quando o cursor chegar nos tributos sem tipo de contrata��o, todos os itens restantes da conta entrar�o no sefip

cursor c_producao is 
	select	a.vl_liberado,
		a.nr_seq_conta,
		a.nr_sequencia,
		nvl(c.cd_cgc_estipulante, cd_cgc_outorgante_w),
		c.cd_pf_estipulante
	from	pls_pagamento_item	e,
		pls_pagamento_prestador	d,
		pls_contrato		c,
		pls_segurado		b,
		pls_conta_medica_resumo	a
	where	a.nr_seq_lote_pgto	= nr_seq_lote_w
	and	a.nr_seq_prestador_pgto	= nr_seq_prestador_w
	and	a.nr_seq_segurado	= b.nr_sequencia
	and	b.nr_seq_contrato	= c.nr_sequencia
	and	d.nr_sequencia		= e.nr_seq_pagamento
	and	e.nr_sequencia		= a.nr_seq_pag_item
	and	exists(	select	1
			from	pls_evento_tributo	f
			where	e.nr_seq_evento		= f.nr_seq_evento
			and	((pls_obter_se_prestador_grupo(f.nr_seq_grupo_prestador,d.nr_seq_prestador) = 'S') or (f.nr_seq_grupo_prestador is null))
			and	((d.nr_seq_prestador = f.nr_seq_prestador) or (f.nr_seq_prestador is null))
			and	((f.nr_seq_tipo_prestador =	(select	max(x.nr_seq_tipo_prestador)
								from	pls_prestador	x
								where	x.nr_sequencia	= d.nr_seq_prestador)) or (f.nr_seq_tipo_prestador is null)
			and	((f.nr_seq_classificacao = 	(select	max(x.nr_seq_classificacao)
								from	pls_prestador	x
								where	x.nr_sequencia	= d.nr_seq_prestador)) or (f.nr_seq_classificacao is null)))
)
	and	((a.ie_situacao is null) or (a.ie_situacao <> 'I'))
	and	not exists	(select	1						-- N�o ler as contas que j� geraram movimento neste lote
				from	sefip_tomador_movto_conta x
				where	x.nr_seq_conta	= a.nr_seq_conta
				and	x.nr_seq_resumo	= a.nr_sequencia
				and	x.nr_seq_lote	= nr_seq_lote_p)
	and	((nvl(a.ie_tipo_contratacao, 'S')		= ie_tipo_contratacao_w)
	or	(nvl(ie_tipo_contratacao_w,'X') = 'X'))
	union all
	select	a.vl_liberado,
		a.nr_seq_conta,
		a.nr_sequencia,
		nvl(c.cd_cgc, cd_cgc_outorgante_w),
		c.cd_pessoa_fisica
	from	pls_pagamento_item	e,
		pls_pagamento_prestador	d,
		pls_intercambio		c,
		pls_segurado		b,
		pls_conta_medica_resumo	a
	where	a.nr_seq_lote_pgto		= nr_seq_lote_w
	and	a.nr_seq_prestador_pgto		= nr_seq_prestador_w
	and	a.nr_seq_segurado		= b.nr_sequencia
	and	b.nr_seq_intercambio		= c.nr_sequencia
	and	d.nr_sequencia		= e.nr_seq_pagamento
	and	e.nr_sequencia		= a.nr_seq_pag_item
	and	exists(	select	1
			from	pls_evento_tributo	f
			where	e.nr_seq_evento		= f.nr_seq_evento
			and	((pls_obter_se_prestador_grupo(f.nr_seq_grupo_prestador,d.nr_seq_prestador) = 'S') or (f.nr_seq_grupo_prestador is null))
			and	((d.nr_seq_prestador = f.nr_seq_prestador) or (f.nr_seq_prestador is null))
			and	((f.nr_seq_tipo_prestador =	(select	max(x.nr_seq_tipo_prestador)
								from	pls_prestador	x
								where	x.nr_sequencia	= d.nr_seq_prestador)) or (f.nr_seq_tipo_prestador is null))
			and	((f.nr_seq_classificacao = 	(select	max(x.nr_seq_classificacao)
								from	pls_prestador	x
								where	x.nr_sequencia	= d.nr_seq_prestador)) or (f.nr_seq_classificacao is null)))
	and	nvl(a.ie_tipo_contratacao, 'S')		= ie_tipo_contratacao_w
	and	not exists	(select	1						-- N�o ler as contas que j� geraram movimento neste lote
				from	sefip_tomador_movto_conta x
				where	x.nr_seq_conta	= a.nr_seq_conta
				and	x.nr_seq_resumo	= a.nr_sequencia
				and	x.nr_seq_lote	= nr_seq_lote_p)
	and	((a.ie_situacao is null) or (a.ie_situacao <> 'I'))
	union all
	select	a.vl_liberado,
		a.nr_seq_conta,
		a.nr_sequencia,
		cd_cgc_outorgante_w,
		null cd_pessoa_fisica
	from	pls_pagamento_item	e,
		pls_pagamento_prestador	d,
		pls_segurado		b,
		pls_conta_medica_resumo	a
	where	a.nr_seq_lote_pgto		= nr_seq_lote_w
	and	a.nr_seq_prestador_pgto		= nr_seq_prestador_w
	and	a.nr_seq_segurado		= b.nr_sequencia
	and	b.nr_seq_intercambio is null
	and	b.nr_seq_contrato is null
	and	d.nr_sequencia		= e.nr_seq_pagamento
	and	e.nr_sequencia		= a.nr_seq_pag_item
	and	exists(	select	1
			from	pls_evento_tributo	f
			where	e.nr_seq_evento		= f.nr_seq_evento
			and	((pls_obter_se_prestador_grupo(f.nr_seq_grupo_prestador,d.nr_seq_prestador) = 'S') or (f.nr_seq_grupo_prestador is null))
			and	((d.nr_seq_prestador = f.nr_seq_prestador) or (f.nr_seq_prestador is null))
			and	((f.nr_seq_tipo_prestador =	(select	max(x.nr_seq_tipo_prestador)
								from	pls_prestador	x
								where	x.nr_sequencia	= d.nr_seq_prestador)) or (f.nr_seq_tipo_prestador is null))
			and	((f.nr_seq_classificacao = 	(select	max(x.nr_seq_classificacao)
								from	pls_prestador	x
								where	x.nr_sequencia	= d.nr_seq_prestador)) or (f.nr_seq_classificacao is null)))
	and	((nvl(a.ie_tipo_contratacao, 'S')		= ie_tipo_contratacao_w)
	or	(nvl(ie_tipo_contratacao_w,'X') = 'X'))
	and	not exists	(select	1						-- N�o ler as contas que j� geraram movimento neste lote
				from	sefip_tomador_movto_conta x
				where	x.nr_seq_conta	= a.nr_seq_conta
				and	x.nr_seq_resumo	= a.nr_sequencia
				and	x.nr_seq_lote	= nr_seq_lote_p)
	and	((a.ie_situacao is null) or (a.ie_situacao <> 'I'))
	union all
	select	nvl(sum(a.vl_item),0),
		null nr_seq_conta,
		null nr_sequencia,
		cd_cgc_outorgante_w,
		null cd_pessoa_fisica
	from	tributo			t,
		pls_evento		d,
		pls_evento_tributo	c,
		pls_pagamento_item	a,
		pls_pagamento_prestador	b
	where	a.nr_seq_evento		= c.nr_seq_evento
	and	a.nr_seq_pagamento	= b.nr_sequencia
	and	a.nr_seq_evento		= d.nr_sequencia
	and	t.cd_tributo		= c.cd_tributo
	and	t.ie_tipo_tributo	= 'INSS'
	and	d.ie_tipo_evento	= 'F'
	and	b.nr_seq_lote		= nr_seq_lote_w
	and	b.nr_seq_prestador	= nr_seq_prestador_w
	and	nvl(a.ie_apropriar_total, 'N')	= 'N'
	and	((pls_obter_se_prestador_grupo(c.nr_seq_grupo_prestador,b.nr_seq_prestador) = 'S') or (c.nr_seq_grupo_prestador is null))
	and	((b.nr_seq_prestador = c.nr_seq_prestador) or (c.nr_seq_prestador is null))
	and	((c.nr_seq_tipo_prestador =	(select	max(x.nr_seq_tipo_prestador)
						from	pls_prestador	x
						where	x.nr_sequencia	= b.nr_seq_prestador)) or (c.nr_seq_tipo_prestador is null))
	and	((c.nr_seq_classificacao = 	(select	max(x.nr_seq_classificacao)
						from	pls_prestador	x
						where	x.nr_sequencia	= b.nr_seq_prestador)) or (c.nr_seq_classificacao is null))
	and	ie_tipo_contratacao_w is null;

cursor c_tomador is							-- Obter as contas que geraram movimenta��o para gerar o tomador
	select	cd_cgc_estipulante,
		sum(vl_liberado)
	from	sefip_tomador_movto_conta
	where	nr_seq_lote	= nr_seq_lote_p
	group	by cd_cgc_estipulante;
	
cursor c_movimentos is
	select	cd_cgc_estipulante,
		nr_seq_prestador,
		sum(vl_liberado)
	from	sefip_tomador_movto_conta
	where	nr_seq_lote		= nr_seq_lote_p
	and	nr_seq_movto	is null
	and	vl_liberado	> 0
	group	by cd_cgc_estipulante, nr_seq_prestador;

Cursor c_arredondamento is
	select	nr_seq_prestador,
		cd_cgc_estipulante,
		sum(vl_liberado),
		sum(vl_descontado)
	from	sefip_tomador_movto_conta
	where	nr_seq_lote		= nr_seq_lote_p
	group	by nr_seq_prestador, cd_cgc_estipulante;

begin
select	ie_competencia,
	dt_lote
into	ie_competencia_w,
	dt_lote_w
from	sefip_lote
where	nr_sequencia	= nr_seq_lote_p;

if	(ie_competencia_w = '13') then
	dt_inicial_w	:= trunc(to_date('01/12/' || to_char(dt_lote_w,'yyyy'),'dd/mm/yyyy'),'dd');
	dt_final_w	:= fim_dia(last_day(to_date('01/12/' || to_char(dt_lote_w,'yyyy'),'dd/mm/yyyy')));
elsif	(ie_competencia_w <> '13') then	
	dt_inicial_w	:= trunc(to_date('01/' || lpad(ie_competencia_w,2,'0') || '/' || to_char(dt_lote_w,'yyyy'),'dd/mm/yyyy'), 'dd');
	dt_final_w	:= fim_dia(last_day(to_date('01/' || lpad(ie_competencia_w,2,'0') || '/' || to_char(dt_lote_w,'yyyy'),'dd/mm/yyyy')));
end if;

select	max(a.cd_cgc_outorgante)
into	cd_cgc_outorgante_w
from	pls_outorgante	a
where	a.cd_estabelecimento	= cd_estabelecimento_p;

open c_lotes;
loop
fetch c_lotes into
	nr_seq_lote_w;
exit when c_lotes%notfound;
	begin
	open c_prestadores;
	loop
	fetch c_prestadores into
		nr_seq_pag_prestador_w,
		nr_seq_prestador_w;
	exit when c_prestadores%notfound;
		begin
		cd_tributo_ant_w	:= null;
		vl_trib_retido_w	:= 0;
		vl_total_w		:= 0;
		
		open c_tributos;
		loop
		fetch c_tributos into
			nr_seq_trib_w,
			pr_tributo_w,
			ie_tipo_contratacao_w,
			cd_tributo_w,
			vl_imposto_w,
			vl_trib_retido_w;
		exit when c_tributos%notfound;
			begin
			vl_descontado_ww	:= 0;
						
			open c_producao;
			loop
			fetch c_producao into
				vl_remuneracao_w,
				nr_seq_conta_w,
				nr_seq_resumo_w,
				cd_cgc_estipulante_w,
				cd_pf_estipulante_w;
			exit when c_producao%notfound;
				begin		
												
				/* Cerifica quanto o item representa do total de produ��o gerado para o prestador */
				/*vl_item_pr_w	:= dividir_sem_round(vl_remuneracao_w, vl_trib_retido_w);
				
				if	(vl_item_pr_w > 1) then
					vl_item_pr_w	:= 1;
				end if;
				
				vl_liberado_w	:= vl_trib_retido_w * vl_item_pr_w;
				*/
				
				begin
				cd_cbo_saude_w	:= pls_obter_dados_conta(nr_seq_conta_w, 'CBO');
				exception
				when others then
					cd_cbo_saude_w	:= null;
				end;

				--vl_descontado_w	:= dividir_sem_round(vl_liberado_w * pr_tributo_w,100);
				
				vl_descontado_w := dividir_sem_round(vl_remuneracao_w * pr_tributo_w,100);
				
				if	((vl_descontado_ww + vl_descontado_w) >= vl_imposto_w) then
					vl_descontado_w	:= vl_imposto_w - vl_descontado_ww;
					
					if	(vl_descontado_w < 0) and
						(vl_imposto_w > 0) then
						vl_descontado_w	:= 0;
					end if;
				end if;
				
				if	(vl_descontado_w <> 0) then
					pls_gerar_sefip_tomador_movto(	nr_seq_lote_p,
									nr_seq_prestador_w,
									nm_usuario_p,
									vl_remuneracao_w,
									0,
									vl_descontado_w,
									0,
									0,
									cd_cbo_saude_w,
									nr_seq_conta_w,
									nr_seq_resumo_w,
									cd_cgc_estipulante_w,
									cd_pf_estipulante_w,
									nr_seq_lote_w);
				end if;
	
				vl_descontado_ww	:= vl_descontado_ww + vl_descontado_w;
				end;
			end loop;
			close c_producao;
			
			if	(ie_tipo_contratacao_w is not null) and
				(vl_descontado_ww <> vl_imposto_w) then
				select	max(a.nr_sequencia)
				into	nr_seq_movto_cta_w
				from	sefip_tomador_movto_conta	a
				where	a.nr_seq_lote		= nr_seq_lote_p
				and	a.nr_seq_prestador	= nr_seq_prestador_w
				and	a.cd_cgc_estipulante	= cd_cgc_estipulante_w;
				
				if	(nr_seq_movto_cta_w is not null) then
					update	sefip_tomador_movto_conta
					set	vl_descontado	= vl_descontado + (vl_imposto_w - vl_descontado_ww)
					where	nr_sequencia	= nr_seq_movto_cta_w;
				else
					select	max(a.nr_sequencia)
					into	nr_seq_movto_cta_w
					from	sefip_tomador_movto_conta	a
					where	a.nr_seq_lote		= nr_seq_lote_p
					and	a.nr_seq_prestador	= nr_seq_prestador_w;
					
					update	sefip_tomador_movto_conta
					set	vl_descontado	= vl_descontado + (vl_imposto_w - vl_descontado_ww)
					where	nr_sequencia	= nr_seq_movto_cta_w;
				end if;
			end if;
			
			cd_tributo_ant_w	:= cd_tributo_w;
			end;
		end loop;
		close c_tributos;
		
		-- Inserir as contas que n�o entraram no lote SEFIP, apenas para fins de confer�ncia
		insert into sefip_tomador_movto_conta
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_movto,
			nr_seq_conta,
			nr_seq_resumo,
			vl_descontado,
			vl_liberado,
			nr_seq_lote,
			nr_seq_lote_pgto,
			nr_seq_prestador,
			cd_cgc_estipulante,
			cd_pf_estipulante)
		select	sefip_tomador_movto_conta_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			null,
			a.nr_seq_conta,
			a.nr_sequencia,
			0,
			a.vl_liberado,
			nr_seq_lote_p,
			nr_seq_lote_w,
			a.nr_seq_prestador_pgto,
			nvl(c.cd_cgc_estipulante, cd_cgc_outorgante_w),
			c.cd_pf_estipulante
		from	pls_contrato c,
			pls_segurado b,
			pls_conta_medica_resumo a
		where	a.nr_seq_lote_pgto	= nr_seq_lote_w
		and	a.nr_seq_segurado	= b.nr_sequencia
		and	b.nr_seq_contrato	= c.nr_sequencia
		and	a.nr_seq_prestador_pgto	= nr_seq_prestador_w
		and	vl_liberado		> 0
		and	((a.ie_situacao is null) or (a.ie_situacao <> 'I'))
		and	not exists	(select	1
					from	sefip_tomador_movto_conta x
					where	x.nr_seq_conta	= a.nr_seq_conta
					and	x.nr_seq_resumo	= a.nr_sequencia
					and	x.nr_seq_lote	= nr_seq_lote_p);
					
		insert into sefip_tomador_movto_conta
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_movto,
			nr_seq_conta,
			nr_seq_resumo,
			vl_descontado,
			vl_liberado,
			nr_seq_lote,
			nr_seq_lote_pgto,
			nr_seq_prestador,
			cd_cgc_estipulante,
			cd_pf_estipulante)
		select	sefip_tomador_movto_conta_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			null,
			a.nr_seq_conta,
			a.nr_sequencia,
			0,
			a.vl_liberado,
			nr_seq_lote_p,
			nr_seq_lote_w,
			a.nr_seq_prestador_pgto,
			nvl(c.cd_cgc, cd_cgc_outorgante_w),
			null
		from	pls_intercambio c,
			pls_segurado b,
			pls_conta_medica_resumo a
		where	a.nr_seq_lote_pgto	= nr_seq_lote_w
		and	a.nr_seq_segurado	= b.nr_sequencia
		and	b.nr_seq_intercambio	= c.nr_sequencia
		and	a.nr_seq_prestador_pgto	= nr_seq_prestador_w
		and	vl_liberado		> 0
		and	((a.ie_situacao is null) or (a.ie_situacao <> 'I'))
		and	not exists	(select	1
					from	sefip_tomador_movto_conta x
					where	x.nr_seq_conta	= a.nr_seq_conta
					and	x.nr_seq_resumo	= a.nr_sequencia
					and	x.nr_seq_lote	= nr_seq_lote_p);
					
		end;
	end loop;
	close c_prestadores;
	commit;
	end;
end loop;
close c_lotes;

open c_movimentos;
loop
fetch c_movimentos into	
	cd_cgc_estipulante_ww,
	nr_seq_prestador_w,
	vl_liberado_ww;
exit when c_movimentos%notfound;
	begin
	select	max(nr_sequencia)
	into	nr_seq_tomador_movto_w
	from	sefip_tomador_movimento
	where	nr_seq_lote		= nr_seq_lote_p
	and	nr_seq_prestador	= nr_seq_prestador_w
	and	cd_cgc_estipulante	= cd_cgc_estipulante_ww;
	
	if	(nr_seq_tomador_movto_w is not null) then
		update	sefip_tomador_movimento
		set	vl_remuneracao		= nvl(vl_remuneracao, 0) + vl_liberado_ww
		where	nr_sequencia		= nr_seq_tomador_movto_w;
	else	
		insert into sefip_tomador_movimento
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_prestador,
			vl_remuneracao,
			vl_remuneracao_13,
			vl_descontado,
			vl_base_13,
			vl_base_13_movimento,
			nr_seq_tomador,
			cd_cbo_saude,
			cd_cgc_estipulante,
			cd_pf_estipulante,
			nr_seq_lote)
		values	(sefip_tomador_movimento_seq.nextval, 
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			nr_seq_prestador_w,
			vl_liberado_ww,
			0,
			0,
			0,
			0,
			null,
			null,
			cd_cgc_estipulante_ww,
			null,
			nr_seq_lote_p);
	end if;
	end;
end loop;
close c_movimentos;

open c_arredondamento;
loop
fetch c_arredondamento into	
	nr_seq_prestador_w,
	cd_cgc_estipulante_ww,
	vl_liberado_ww,
	vl_descontar_w;
exit when c_arredondamento%notfound;
	begin
	select	sum(a.vl_remuneracao),
		sum(a.vl_descontado)
	into	vl_total_w,
		vl_total_desconto_w
	from	sefip_tomador_movimento	a
	where	a.nr_seq_prestador	= nr_seq_prestador_w
	and	a.nr_seq_lote		= nr_seq_lote_p
	and	cd_cgc_estipulante	= cd_cgc_estipulante_ww;
	
	if	(vl_total_w <> vl_liberado_ww) then
		select	max(nr_sequencia)
		into	nr_seq_tomador_movto_w
		from	sefip_tomador_movimento
		where	nr_seq_lote		= nr_seq_lote_p
		and	nr_seq_prestador	= nr_seq_prestador_w
		and	cd_cgc_estipulante	= cd_cgc_estipulante_ww;
		
		update	sefip_tomador_movimento
		set	vl_remuneracao		= vl_liberado_ww
		where	nr_sequencia		= nr_seq_tomador_movto_w;
	end if;
	
	if	(vl_total_desconto_w <> vl_descontar_w) then
		select	max(nr_sequencia)
		into	nr_seq_tomador_movto_w
		from	sefip_tomador_movimento
		where	nr_seq_lote		= nr_seq_lote_p
		and	nr_seq_prestador	= nr_seq_prestador_w
		and	cd_cgc_estipulante	= cd_cgc_estipulante_ww;
		
		update	sefip_tomador_movimento
		set	vl_descontado		= vl_descontar_w
		where	nr_sequencia		= nr_seq_tomador_movto_w;
	end if;
	end;
end loop;
close c_arredondamento;

open c_tomador;
loop
fetch c_tomador into
	cd_cgc_estipulante_w,
	vl_liberado_w;			-- retirar o lote de pagamento
exit when c_tomador%notfound;
	begin
	select	sefip_tomador_seq.nextval
	into	nr_seq_tomador_w
	from	dual;

	insert into sefip_tomador
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_lote,
		cd_cgc,
		vl_retencao,
		vl_faturado)
	values	(nr_seq_tomador_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_lote_p,
		cd_cgc_estipulante_w,
		0,
		vl_liberado_w);

	commit;

	update	sefip_tomador_movimento
	set	nr_seq_tomador		= nr_seq_tomador_w
	where	cd_cgc_estipulante	= cd_cgc_estipulante_w
	and	nr_seq_lote		= nr_seq_lote_p;

	commit;
	
	end;
end loop;
close c_tomador;

update	sefip_lote
set	dt_geracao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_lote_p;

commit;

end pls_gerar_lote_sefip_new;
/