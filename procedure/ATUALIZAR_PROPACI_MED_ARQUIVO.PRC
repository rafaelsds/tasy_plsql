create or replace
procedure Atualizar_Propaci_Med_arquivo(	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					cd_medico_p		varchar2,
					nm_usuario_p		Varchar2) is 

nr_seq_propaci_w	number(15);
cd_medico_exec_w	varchar2(10);
ie_medico_w	varchar2(1);
nr_seq_interno_w	number(10);

begin

select	max(b.nr_sequencia)
into	nr_seq_propaci_w
from	procedimento_paciente b,
		prescr_procedimento a
where	a.nr_prescricao		= b.nr_prescricao
and		a.nr_sequencia		= b.nr_sequencia_prescricao
and		a.nr_prescricao		= nr_prescricao_p
and		a.nr_sequencia		= nr_seq_prescr_p;

if	(nr_seq_propaci_w is not null) then
	
	ie_medico_w	:= Obter_se_medico(cd_medico_p,'M');
	
	if	(ie_medico_w = 'S')	then

		update	procedimento_paciente
		set		cd_medico_executor = cd_medico_p,
				dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p
		where	nr_sequencia = nr_seq_propaci_w;

	end if;
	
end if; 

commit;

end atualizar_propaci_med_arquivo;
/