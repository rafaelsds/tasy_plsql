create or replace
procedure GRAVAR_DATA_GERAC_FLUXO_LOTE (
			nr_seq_lote_fluxo_p	number) is 
begin

update	fluxo_caixa_lote
set	dt_geracao	= sysdate,
	ie_alterado	= 'N'
where	nr_sequencia	= nr_seq_lote_fluxo_p;

commit;

end GRAVAR_DATA_GERAC_FLUXO_LOTE;
/
