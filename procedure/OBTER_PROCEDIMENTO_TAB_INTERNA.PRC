create or replace
procedure obter_procedimento_tab_interna(	nr_seq_interno_p	in		number,
					cd_convenio_p		in 	number,
					cd_estabelecimento_p	in	number,
					dt_vigencia_p		in	date,
					cd_procedimento_p	out		number,
					ie_origem_proced_p	out	number)
					is
					

cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
cd_edicao_amb_w		number(06,0);
ie_tipo_convenio_w		number(02,0);
cd_categoria_w		varchar2(10);
vl_retorno_w		number(15,4);
ie_preco_informado_w	varchar2(10);
ie_glosa_w		varchar2(10);
cd_edicao_ajuste_w	number(06,0);

cd_proced_conv_w		number(15,0);
ie_origem_proced_conv_w	number(10,0);
ie_autor_particular_w	varchar2(1);
cd_convenio_glosa_ww	number(5);
cd_categoria_glosa_ww	varchar2(10);
nr_seq_ajuste_proc_ww	number(10);
dt_vigencia_w		date;


cursor c01 is
select	cd_procedimento,
	ie_origem_proced
from	proc_interno_conv
where	nr_seq_proc_interno		= nr_seq_interno_p
and	((cd_convenio			= cd_convenio_p 		or cd_convenio is null))
and	((cd_edicao_amb			= cd_edicao_amb_w 		or cd_edicao_amb is null))
and	dt_vigencia_w between nvl(dt_inicio_vigencia, dt_vigencia_w) and nvl(dt_final_vigencia, dt_vigencia_w)
order by
	nvl(cd_edicao_amb,0),
	nvl(cd_convenio,0),
	nvl(ie_tipo_atendimento,0);

cursor	c02 is
	select	cd_categoria
	from	categoria_convenio
	where	cd_convenio	= cd_convenio_p;

begin

dt_vigencia_w	:= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(dt_vigencia_p, sysdate));

select	max(cd_procedimento),
	max(ie_origem_proced)
into	cd_procedimento_w,
	ie_origem_proced_w
from	proc_interno
where	nr_sequencia = nr_seq_interno_p;

if	(cd_convenio_p > 0) then
	begin
	
	select	max(ie_tipo_convenio)
	into	ie_tipo_convenio_w
	from	convenio
	where	cd_convenio = cd_convenio_p;

	SELECT	nvl(max(CD_EDICAO_AMB),0)
	INTO	CD_EDICAO_AMB_W
	FROM 	CONVENIO_AMB
	WHERE 	(CD_ESTABELECIMENTO     = cd_estabelecimento_p)
	AND 	(CD_CONVENIO            = cd_convenio_p)
  	AND 	(nvl(IE_SITUACAO,'A')   = 'A')
  	AND 	(DT_INICIO_VIGENCIA     = 
			(SELECT	MAX(DT_INICIO_VIGENCIA)
			FROM 	CONVENIO_AMB A	
			WHERE 	(A.CD_ESTABELECIMENTO  	= cd_estabelecimento_p)
			AND 	(A.CD_CONVENIO         	= CD_CONVENIO_p)
			AND 	(nvl(A.IE_SITUACAO,'A')	= 'A')
			AND 	(A.DT_INICIO_VIGENCIA 	<=  sysdate)));

	
	cd_edicao_ajuste_w	:= 0;

	open	c02;
	loop
	fetch	c02 into cd_categoria_w;
	exit	when c02%notfound;
		begin

		if	(cd_edicao_ajuste_w = 0) then
			begin
			obter_regra_ajuste_proc
				(cd_estabelecimento_p,
				cd_convenio_p,
				cd_categoria_w,
				cd_procedimento_w,
				ie_origem_proced_w,
				null,
				sysdate,0,0,0,null,
				0,0,null,0,
				null,null,null,null,null,
				vl_retorno_w,vl_retorno_w,vl_retorno_w,
				vl_retorno_w,vl_retorno_w,vl_retorno_w,
				ie_preco_informado_w,ie_glosa_w,
				vl_retorno_w,vl_retorno_w,
				cd_edicao_ajuste_w,vl_retorno_w,vl_retorno_w,vl_retorno_w,
				vl_retorno_w,vl_retorno_w,vl_retorno_w,vl_retorno_w,
				vl_retorno_w, null, 0,ie_autor_particular_w,
				cd_convenio_glosa_ww,
				cd_categoria_glosa_ww,
				nr_seq_ajuste_proc_ww,
				null,null,
				null,null,
				null,null,
				null,
				null,
				vl_retorno_w,
				null,null,null,null,
				null,null,null,null,
				null, null);	
			exception
				when others then
				cd_edicao_ajuste_w	:= 0;
			end;
		end if;
		end;
	end loop;
	close	c02;
		
	if	(cd_edicao_ajuste_w is not null) and
		(cd_edicao_ajuste_w <> 0) then
		cd_edicao_amb_w		:= cd_edicao_ajuste_w;
	end if;

	if	(cd_convenio_p is not null) or
		(cd_edicao_amb_w is not null) then
		open c01;
		loop
		fetch c01 into	cd_proced_conv_w,
					ie_origem_proced_conv_w;
			exit when c01%notfound;
			begin
			cd_procedimento_w	:= cd_proced_conv_w;
			ie_origem_proced_w	:= ie_origem_proced_conv_w;
			end;
		end loop;
		close c01;
	end if;
	end;
end if;

cd_procedimento_p	:= cd_procedimento_w; 
ie_origem_proced_p	:= ie_origem_proced_w;

end obter_procedimento_tab_interna;
/
