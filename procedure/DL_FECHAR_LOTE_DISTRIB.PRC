create or replace
procedure dl_fechar_lote_distrib
			(nr_seq_lote_p	in	number,
			ie_acao_p	in	varchar2,
			nm_usuario_p	in	varchar2) is
/*
ie_acao_p
	'F' - fechado
	'P' - provisorio
*/

begin

if	(ie_acao_p = 'F') then
	update	dl_lote_distribuicao
	set	dt_fechamento	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		ie_status	= 	2
	where	nr_sequencia	= nr_seq_lote_p;
elsif	(ie_acao_p = 'P') then
	update	dl_lote_distribuicao
	set	dt_fechamento	= null,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		ie_status	= 	1
	where	nr_sequencia	= nr_seq_lote_p;
end if;

commit;

end dl_fechar_lote_distrib;
/