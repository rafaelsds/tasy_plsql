create or replace
procedure consistir_funcao_cargo_pf(	cd_pessoa_fisica_p	varchar2,
					cd_funcao_p		number,
					ds_inconsistencia_p	out varchar2 ) is 

cd_cargo_w	number(10,0);
cd_cargo_pf_w	number(10,0);
dt_obito_w	date;
					
begin

if	(cd_funcao_p > 0) and (cd_pessoa_fisica_p is not null) then

	select	nvl(max(cd_cargo),0)
	into	cd_cargo_w
	from	funcao_medico
	where	cd_funcao	=	cd_funcao_p;
	
	select	dt_obito
	into	dt_obito_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	=	cd_pessoa_fisica_p;
	
	if	(dt_obito_w is not null) then
		ds_inconsistencia_p := WHEB_MENSAGEM_PCK.get_texto(955872);
	end if;
	
	if	(cd_cargo_w > 0) then
		select	cd_cargo
		into	cd_cargo_pf_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	=	cd_pessoa_fisica_p;
		
		if	(nvl(cd_cargo_w,0) <> nvl(cd_cargo_pf_w,0)) then
			if	(NVL(ds_inconsistencia_p,'') <> '') then
				ds_inconsistencia_p	:= ds_inconsistencia_p || chr(10) || WHEB_MENSAGEM_PCK.get_texto(955873, 'DS_CARGO='||obter_desc_cargo(cd_cargo_w));
			else
				ds_inconsistencia_p	:= WHEB_MENSAGEM_PCK.get_texto(955873, 'DS_CARGO='||obter_desc_cargo(cd_cargo_w));
			end if;
		end if;
	end if;
end if;

end consistir_funcao_cargo_pf;
/
