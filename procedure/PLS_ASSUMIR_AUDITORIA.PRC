create or replace
procedure pls_assumir_auditoria(
		nr_seq_grupo_auditor_p	number,
		nr_sequencia_p		number,
		nr_seq_ordem_p		number,
		nm_usuario_p		varchar2) is 

ie_grupo_auditor_w		varchar2(10);
qt_auditoria_grupo_w	number(10);
nr_seq_grupo_auditor_w	number(10);

begin
if	(nr_seq_grupo_auditor_p is not null) and
	(nr_sequencia_p is not null) and
	(nr_seq_ordem_p is not null) and
	(nm_usuario_p is not null) then
	begin

	select	nvl(pls_obter_se_auditor_grupo(nr_seq_grupo_auditor_p,nm_usuario_p),'N')
	into	ie_grupo_auditor_w
	from	dual;

	if	(ie_grupo_auditor_w = 'S') then
		begin
		
		select	count(*) 
		into	qt_auditoria_grupo_w
		from	pls_auditoria_grupo 
		where	nr_seq_auditoria = nr_sequencia_p
		and	nr_seq_ordem < nr_seq_ordem_p
		and	dt_liberacao is null;
		
		if	(qt_auditoria_grupo_w = 0) then
			begin
			
			select	pls_obter_grupo_analise_atual(nr_sequencia_p)
			into	nr_seq_grupo_auditor_w
			from	dual;
			
			pls_assumir_grupo_auditoria(
				nr_seq_grupo_auditor_w,
				nm_usuario_p);
			
			end;
		else	
			begin
			--N�o � possivel assumir a auditoria do grupo, pois existem grupos anteriores que n�o encerraram suas an�lises!
			wheb_mensagem_pck.exibir_mensagem_abort(76340);
			end;
		end if;
		
		end;
	else
		begin
		--Voc� n�o participa deste grupo de auditores!
		wheb_mensagem_pck.exibir_mensagem_abort(76342);
		end;
	end if;
	end;
end if;
commit;

end pls_assumir_auditoria;
/