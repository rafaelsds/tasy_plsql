create or replace
procedure UC_enviar_comunic_interna_aniv(	cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is

nr_seq_regra_w		number;
ie_tipo_publico_w		number;
ds_titulo_w		varchar2(200);
ds_titulo_aniv_w		varchar2(200);	
ds_texto_aniv_w		varchar2(2000);	
ds_texto_comunic_w	varchar2(500);
nr_seq_classif_w		number;
cd_pessoa_aniv_w		varchar2(15);
cd_aniversariantes_w	varchar2(2000);
nm_usuario_aniv_w		varchar2(20);
nm_pessoas_aniv_w	varchar2(2000);

nm_usuario_destino_w	varchar2(70);
cd_perfil_destino_w		varchar2(70);
cd_setor_destino_w		varchar2(70);
ie_geral_destino_w		varchar2(70);
ds_setor_usuario_w		varchar2(80);

nm_usuarios_destino_w	varchar2(600);
cd_perfis_destino_w	varchar2(4000);
cd_setores_destino_w	varchar2(600);
ie_gerais_destino_w	varchar2(600);

ie_medico_w		varchar2(100);
ie_cargo_w		varchar2(200);
nm_fantasia_estab_w	varchar2(200);
IE_SOMENTE_USUARIO_w	varchar2(5);
ds_comunicado_w		long;
cd_estab_destino_w	number(10);
cd_pessoas_aniv_w	varchar2(4000) := ',';
ie_listar_setor_medico_w	varchar2(15);
ie_pf_medico_w		varchar2(01);
cd_estab_regra_w	number(10);

/* Percorre regras */

cursor C01 IS
select	nr_sequencia,
	ie_tipo_publico,
	ds_titulo,
	ds_titulo_aniv,
	ds_texto_aniv,
	ds_texto_comunic,
	nr_seq_classif,
	ie_somente_usuario,
	ie_listar_setor_medico,
	cd_estabelecimento
from 	regra_comunic_aniv
where	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

/* Seleciona aniversariantes do dia conforme regra */

cursor C02 IS
select	f.cd_pessoa_fisica,
	u.nm_usuario
from	pessoa_fisica f,
	usuario u
where	f.cd_pessoa_fisica	= u.cd_pessoa_fisica 
and	(obter_se_contido(f.cd_pessoa_fisica, cd_aniversariantes_w) = 'S')
and	u.ie_situacao	<> 'I'
and	f.dt_obito is null
and	u.cd_estabelecimento = nvl(cd_estab_regra_w, u.cd_estabelecimento)
and	(exists (select 1 
		from	medico m,
			medico_convenio n
		where	m.cd_pessoa_fisica = f.cd_pessoa_fisica
		and	m.cd_pessoa_fisica = n.cd_pessoa_fisica
		and	n.cd_convenio = 2
		and	n.ie_conveniado = 'S'
		and	m.ie_situacao = 'A'))
and	(((ie_tipo_publico_w = 1) 	and	 (f.ie_funcionario = 'S')) or 
	((ie_tipo_publico_w = 2)	and	 (exists (select 1 
							  from	medico m,
								medico_convenio n
						   	  where	m.cd_pessoa_fisica = f.cd_pessoa_fisica
							  and	m.cd_pessoa_fisica = n.cd_pessoa_fisica
							  and	n.cd_convenio = 2
							  and	n.ie_conveniado = 'S'
							  and	m.ie_situacao = 'A'))) or
	(ie_tipo_publico_w = 9))
union all
select	f.cd_pessoa_fisica,
	' ' nm_usuario
from	pessoa_fisica f
where	(obter_se_contido(somente_numero(f.cd_pessoa_fisica), cd_aniversariantes_w) = 'S')
and	f.dt_obito is null
and	((f.ie_funcionario = 'S') or
		(exists (select 1 
			from	medico m
			where	m.cd_pessoa_fisica = f.cd_pessoa_fisica
			and	m.ie_situacao = 'A')));
	

/* Seleciona destinat�rios da mensagem conforme regra */

cursor C03 IS
select	nm_usuario_destino,
	cd_perfil,
	cd_setor_atendimento,
	ie_geral,
	cd_estabelecimento
from	comunic_aniv_destino
where	nr_seq_regra 	= nr_seq_regra_w
and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;


BEGIN

open C01;
loop	
fetch C01 into
	nr_seq_regra_w,
	ie_tipo_publico_w,
	ds_titulo_w,
	ds_titulo_aniv_w,
	ds_texto_aniv_w,
	ds_texto_comunic_w,
	nr_seq_classif_w,
	ie_somente_usuario_w,
	ie_listar_setor_medico_w,
	cd_estab_regra_w;
exit when C01%notfound;
begin

	ds_titulo_aniv_w	:= nvl(ds_titulo_aniv_w,ds_titulo_w);
	nm_pessoas_aniv_w	:= 'Aniversariantes de hoje:';  /* Inicia a lista de nomes dos aniversariantes  */
	nm_usuarios_destino_w	:= null;
	cd_perfis_destino_w	:= null;
	cd_setores_destino_w	:= null;
	ie_gerais_destino_w	:= 'N';
	if	(ie_somente_usuario_w = 'S') then
		cd_aniversariantes_w	:= obter_usuarios_aniv(nr_seq_regra_w);
	else
		cd_aniversariantes_w	:= obter_pessoa_aniv(nr_seq_regra_w);
	end if;

	open C02;
	loop
	fetch C02 into
		cd_pessoa_aniv_w,
		nm_usuario_aniv_w;
	exit when C02%notfound;
	begin
		
		/* Envia comunica��o para o USU�RIO ANIVERSARIANTE */
		if	(nvl(nm_usuario_aniv_w,'X') <> 'X') then
			
			nm_usuario_aniv_w	:= nm_usuario_aniv_w || ',';
			ds_comunicado_w		:= ds_texto_aniv_w;
			INSERT  INTO comunic_interna(   
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				nr_sequencia,
				ie_gerencial,
				nr_seq_classif,
				ds_perfil_adicional,
				cd_setor_destino,
				cd_estab_destino,
				ds_setor_adicional,
				dt_liberacao)
			VALUES (SYSDATE,
				ds_titulo_aniv_w,
				ds_comunicado_w,
				nm_usuario_p,
				SYSDATE,
				'N',
				nm_usuario_aniv_w,
				comunic_interna_seq.nextval,
				'N',
				nr_seq_classif_w,
				null,
				null,
				cd_estabelecimento_p,
				null,
				SYSDATE); 
		end if;

		/* Francisco - 26/09/2008 - OS 108904 */
		select	max(decode(b.cd_pessoa_fisica,null,null,decode(a.ie_sexo,'M','Dr. ','F','Dra. ','Dr(a).')))
		into	ie_medico_w
		from	medico b,
				pessoa_fisica a
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica(+)
		and		a.cd_pessoa_fisica	= cd_pessoa_aniv_w
		and		b.ie_situacao		= 'A';


		select	max(nvl(substr(obter_especialidade_medico(cd_pessoa_fisica,'D'),1,100),substr(obter_desc_cargo(cd_cargo),1,200)))
		into	ie_cargo_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_aniv_w;

		if	(ie_cargo_w is not null) then
			ie_cargo_w	:= ' (' || ie_cargo_w || ') ';
		end if;
	
		/* Fim Francisco - 26/09/2008 */
		
		-- afstringari OS165133 12/09/2009
		select	max(SUBSTR(obter_dados_pf_pj(null, b.cd_cgc, 'F'),1,80)),
			max(substr(obter_nome_setor(cd_setor_atendimento),1,80)) || ' -  '
		into	nm_fantasia_estab_w,
			ds_setor_usuario_w
		from	estabelecimento b,
			usuario a
		where	a.cd_estabelecimento	= b.cd_estabelecimento	
		and	a.cd_pessoa_fisica   	= cd_pessoa_aniv_w
		and	a.ie_situacao		<> 'I';

		select	substr(Obter_se_medico(cd_pessoa_aniv_w,'M'),1,1)
		into	ie_pf_medico_w
		from	dual;
		
		if	(nvl(ie_pf_medico_w,'N') = 'S') and
			(nvl(ie_listar_setor_medico_w,'S') = 'N') then
			ds_setor_usuario_w := '';
		end if;
		
		/* ahoffelder - OS 197148 - 03/03/2010 - para n�o repetir a pessoa na lista de aniversariantes */
		if	(InStr(cd_pessoas_aniv_w,',' || cd_pessoa_aniv_w || ',') <= 0) then
			cd_pessoas_aniv_w := cd_pessoas_aniv_w || cd_pessoa_aniv_w || ',';
			nm_pessoas_aniv_w  := 	substr(nm_pessoas_aniv_w || chr(13) || chr(10) || chr(13) || chr(10) || 
						ie_medico_w || substr(obter_nome_pf(cd_pessoa_aniv_w),1,50) || ie_cargo_w || ' -  ' || ds_setor_usuario_w || nm_fantasia_estab_w,1,2000);
		end if;
		end;
	end loop;
	close C02;

	if (nm_pessoas_aniv_w <> 'Aniversariantes de hoje:') then

		nm_pessoas_aniv_w 	:= substr(chr(13) || chr(10) || nm_pessoas_aniv_w || chr(13) || chr(10) || chr(13) || chr(10),1,2000);
		cd_estab_destino_w	:= cd_estabelecimento_p;
		open C03;
		loop
		fetch C03 into
			nm_usuario_destino_w,
			cd_perfil_destino_w,
			cd_setor_destino_w,
			ie_geral_destino_w,
			cd_estab_destino_w;
		exit when C03%notfound;
		begin
			if (nm_usuario_destino_w is not null) then
				nm_usuarios_destino_w := nm_usuarios_destino_w || nm_usuario_destino_w || ',';
			end if;

			if (cd_perfil_destino_w is not null) then
				cd_perfis_destino_w := cd_perfis_destino_w || cd_perfil_destino_w || ',';
			end if;

			if (cd_setor_destino_w is not null) then
				cd_setores_destino_w := cd_setores_destino_w || cd_setor_destino_w || ',';
			end if;

			if (ie_geral_destino_w = 'S') then
				ie_gerais_destino_w := 'S';
			end if;	

		end;  
		end loop;
		close C03;
		
			
		/*Envia o comunicado GERAL com a RELA��O de TODOS os ANIVERSARIANTES*/
		ds_comunicado_w	:= nm_pessoas_aniv_w || ds_texto_comunic_w;
		INSERT  INTO 	comunic_interna(   
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			nr_seq_classif,
			ds_perfil_adicional,
			cd_setor_destino,
			cd_estab_destino,
			ds_setor_adicional,
			dt_liberacao)
		VALUES (SYSDATE,
			ds_titulo_w,
			ds_comunicado_w,
			nm_usuario_p,
			SYSDATE,
			ie_gerais_destino_w,
			nm_usuarios_destino_w,
			comunic_interna_seq.nextval,
			'N',
			nr_seq_classif_w,
			cd_perfis_destino_w,
			null,
			cd_estab_destino_w,
			cd_setores_destino_w,
			SYSDATE);

	end if;
end;
end loop;
close C01;
commit;

end UC_enviar_comunic_interna_aniv;
/


