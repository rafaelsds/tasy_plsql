create or replace procedure liberar_protese_manutencao (

		nm_usuario_p	varchar2,

		nr_seq_manut_p	number,

		nr_seq_equip_p	number,

		ds_mensagem_p	out varchar2) is

		

nr_ordem_servico_w	number(10);

ds_texto_w		varchar(255);



dt_inicio_desejado_w	date;

ds_motivo_w		varchar2(100);

ds_justificativa_w	varchar(4000);

		

begin

ds_texto_w := '';



if	(nm_usuario_p is not null) and

	(nr_seq_manut_p is not null) then 

	

	select	max(a.dt_envio),

		substr(max(pb_obter_desc_motivo_manut(a.nr_seq_motivo)),1,100),

		substr(max(a.ds_justificativa),1,4000)

	into	dt_inicio_desejado_w,

		ds_motivo_w,

		ds_justificativa_w

	from	rp_equip_manutencao a

	where	a.nr_sequencia = nr_seq_manut_p;

	

	if	(dt_inicio_desejado_w is null) or

		(ds_motivo_w is null) or

		(ds_justificativa_w is null) then

		wheb_mensagem_pck.exibir_mensagem_abort(331062);

	end if;

	

	update	rp_equip_manutencao

	set	dt_liberacao 	= sysdate,

		nm_usuario_lib	= nm_usuario_p

	where	nr_sequencia 	= nr_seq_manut_p;

	

	gerar_os_protese_manutencao(	nr_seq_manut_p,

					nm_usuario_p,

					nr_ordem_servico_w);



	ds_texto_w := substr(obter_texto_dic_objeto(279811, wheb_usuario_pck.get_nr_seq_idioma, 'ORDEM_SERVICO=' || nr_ordem_servico_w),1,255);

						

	man_log_equip_hist_evento(	nr_seq_equip_p,

					ds_texto_w,

					'M',

					nm_usuario_p);

	

	commit;

end if;



ds_mensagem_p := ds_texto_w;



end liberar_protese_manutencao;


 /