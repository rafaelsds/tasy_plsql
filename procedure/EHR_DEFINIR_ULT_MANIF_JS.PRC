create or replace
procedure ehr_definir_ult_manif_js( nr_sequencia_p	number,
									nm_tabela_p		varchar,
									nm_usuario_p	varchar) is

ds_sep_bv_w		varchar2(10);
ds_comando_w	varchar2(255);	
	
begin

if	(nr_sequencia_p is not null) then
	begin
	
	select 	obter_separador_bv
	into	ds_sep_bv_w
	from 	dual;
	
	ds_comando_w	:=	' update	' || nm_tabela_p ||
						' set		dt_ultima = sysdate ' ||
						' where		nr_sequencia = :nr_sequencia ' ||
						' and		nm_usuario = :nm_usuario ' ;
				
	exec_sql_dinamico_bv(obter_desc_expressao(320706)/*'Definir �ltima manifesta��o'*/,ds_comando_w,'nr_sequencia=' || to_char(nr_sequencia_p) || ds_sep_bv_w ||
									'nm_usuario='||nm_usuario_p);
	commit;
	
	end;
end if;

end ehr_definir_ult_manif_js;
/