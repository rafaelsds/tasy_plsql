create or replace
procedure calc_medianas_sinais_vitais (ie_ciclo_p number) is 
			
dt_sinal_vital_w	  w_atendimento_ciclo_sv.dt_sinal_vital%type;
nr_seq_sinal_vital_w	  atendimento_sinal_vital.nr_sequencia%type;	
qt_pa_sistolica_w	  w_atendimento_ciclo_sv.qt_pa_sistolica%type;
qt_pa_diastolica_w	  w_atendimento_ciclo_sv.qt_pa_diastolica%type;
qt_pam_w		  w_atendimento_ciclo_sv.qt_pam%type;
qt_altura_cm_w            w_atendimento_ciclo_sv.qt_altura_cm%type;          
qt_bcf_w                  w_atendimento_ciclo_sv.qt_bcf%type;      
qt_bcf_2_w                w_atendimento_ciclo_sv.qt_bcf_2%type;                
qt_bcf_3_w                w_atendimento_ciclo_sv.qt_bcf_3%type;                
qt_circunf_braco_w        w_atendimento_ciclo_sv.qt_circunf_braco%type;        
qt_circunf_panturrilha_w  w_atendimento_ciclo_sv.qt_circunf_panturrilha%type;  
qt_cmb_w                  w_atendimento_ciclo_sv.qt_cmb%type;                  
qt_freq_cardiaca_w        w_atendimento_ciclo_sv.qt_freq_cardiaca%type;        
qt_freq_resp_w            w_atendimento_ciclo_sv.qt_freq_resp%type;            
qt_glicemia_capilar_w     w_atendimento_ciclo_sv.qt_glicemia_capilar%type;     
qt_glicemia_mmol_w        w_atendimento_ciclo_sv.qt_glicemia_mmol%type;        
qt_imc_w                  w_atendimento_ciclo_sv.qt_imc%type;                  
qt_pae_w                  w_atendimento_ciclo_sv.qt_pae%type;                  
qt_peso_w                 w_atendimento_ciclo_sv.qt_peso%type;                 
qt_pressao_intra_abd_w    w_atendimento_ciclo_sv.qt_pressao_intra_abd%type;    
qt_pressao_intra_cranio_w w_atendimento_ciclo_sv.qt_pressao_intra_cranio%type; 
qt_pvc_w                  w_atendimento_ciclo_sv.qt_pvc%type;                  
qt_saturacao_o2_w         w_atendimento_ciclo_sv.qt_saturacao_o2%type;         
qt_superf_corporia_w      w_atendimento_ciclo_sv.qt_superf_corporia%type;      
qt_temp_w                 w_atendimento_ciclo_sv.qt_temp%type;                 
qt_temp_incubadora_w      w_atendimento_ciclo_sv.qt_temp_incubadora%type;      
nm_usuario_w		  w_atendimento_ciclo_sv.nm_usuario%type;
nr_atendimento_w	  w_atendimento_ciclo_sv.nr_atendimento%type;
cd_paciente_w		  w_atendimento_ciclo_sv.cd_paciente%type;

/* Integracao com o sistema blackboard */
json_aux_bb philips_json;
envio_integracao_bb clob;
retorno_integracao_bb clob;

bb_ciclo_type_label varchar2(13);

Cursor C02 is
	select	truncar_hora_parametro(dt_sinal_vital, ie_ciclo_p) 	dt_sinal_vital,	
		cd_paciente,
		nr_atendimento, 
		median(qt_pa_sistolica)		  	        qt_pa_sistolica,					
		median(qt_pa_diastolica)		        qt_pa_diastolica,
		median(qt_pam)			                qt_pam,
		median(qt_altura_cm)            		qt_altura_cm,            
		median(qt_bcf)                  		qt_bcf,                  
		median(qt_bcf_2)               			qt_bcf_2,                
		median(qt_bcf_3)               			qt_bcf_3,                
		median(qt_circunf_braco)       			qt_circunf_braco,        
		median(qt_circunf_panturrilha)  		qt_circunf_panturrilha,                  
		median(qt_freq_cardiaca)        		qt_freq_cardiaca,        
		median(qt_freq_resp)           			qt_freq_resp,            
		median(qt_glicemia_capilar)    			qt_glicemia_capilar,     
		median(qt_glicemia_mmol)       			qt_glicemia_mmol,        
		median(qt_imc)                 			qt_imc,                  		
		median(qt_pae)                 			qt_pae,                  				      
		median(qt_peso)                			qt_peso,                 
		median(qt_pressao_intra_abd)    		qt_pressao_intra_abd,    
		median(qt_pressao_intra_cranio)			qt_pressao_intra_cranio,
		median(qt_pvc)                  		qt_pvc,                  
		median(qt_saturacao_o2)         		qt_saturacao_o2,         
		median(qt_superf_corporia)     			qt_superf_corporia,      
		median(qt_temp)                			qt_temp,                 
		median(qt_temp_incubadora) 			qt_temp_incubadora      		
	from	w_atendimento_ciclo_sv	
	where 	ie_ciclo = ie_ciclo_p
	group by truncar_hora_parametro(dt_sinal_vital, ie_ciclo_p), cd_paciente, nr_atendimento
	order by nr_atendimento, dt_sinal_vital asc;	

Cursor C03 is
	select	truncar_hora_parametro(sysdate, 1) 	dt_sinal_vital,
		cd_paciente,
		nr_atendimento,
		avg(qt_pa_sistolica)		  	        qt_pa_sistolica,					
		avg(qt_pa_diastolica)		        	qt_pa_diastolica,
		avg(qt_pam)			                qt_pam,
		avg(qt_altura_cm)            			qt_altura_cm,            
		avg(qt_bcf)                  			qt_bcf,                  
		avg(qt_bcf_2)               			qt_bcf_2,                
		avg(qt_bcf_3)               			qt_bcf_3,                
		avg(qt_circunf_braco)       			qt_circunf_braco,        
		avg(qt_circunf_panturrilha)  			qt_circunf_panturrilha,                  
		avg(qt_freq_cardiaca)        			qt_freq_cardiaca,        
		avg(qt_freq_resp)           			qt_freq_resp,            
		avg(qt_glicemia_capilar)    			qt_glicemia_capilar,     
		avg(qt_glicemia_mmol)       			qt_glicemia_mmol,        
		avg(qt_imc)                 			qt_imc,                  		
		avg(qt_pae)                 			qt_pae,                  				      
		avg(qt_peso)                			qt_peso,                 
		avg(qt_pressao_intra_abd)    			qt_pressao_intra_abd,    
		avg(qt_pressao_intra_cranio)			qt_pressao_intra_cranio,
		avg(qt_pvc)                  			qt_pvc,                  
		avg(qt_saturacao_o2)         			qt_saturacao_o2,         
		avg(qt_superf_corporia)     			qt_superf_corporia,      
		avg(qt_temp)                			qt_temp,                 
		avg(qt_temp_incubadora) 			qt_temp_incubadora      		
	from	w_atendimento_ciclo_sv
	where 	ie_ciclo = ie_ciclo_p	
	group by truncar_hora_parametro(dt_sinal_vital, 1), cd_paciente, nr_atendimento
	order by nr_atendimento, dt_sinal_vital asc;		

begin

if (ie_ciclo_p= 1) then
    bb_ciclo_type_label:= '_TYPE_ONE';
    
	open C03;
	loop
	fetch C03 into	
		dt_sinal_vital_w,
		cd_paciente_w,	
		nr_atendimento_w,
		qt_pa_sistolica_w,	 		
		qt_pa_diastolica_w,	 		
		qt_pam_w,		 		
		qt_altura_cm_w,           		
		qt_bcf_w,                 		
		qt_bcf_2_w,               		
		qt_bcf_3_w,              		
		qt_circunf_braco_w,
		qt_circunf_panturrilha_w,             
		qt_freq_cardiaca_w,       
		qt_freq_resp_w,           
		qt_glicemia_capilar_w,    
		qt_glicemia_mmol_w,       
		qt_imc_w,                 
		qt_pae_w,                 
		qt_peso_w,                
		qt_pressao_intra_abd_w,   
		qt_pressao_intra_cranio_w,
		qt_pvc_w,                 
		qt_saturacao_o2_w,        
		qt_superf_corporia_w,     
		qt_temp_w,                
		qt_temp_incubadora_w;	
	exit when C03%notfound;
		begin
		
		select	atendimento_sinal_vital_seq.nextval
		into	nr_seq_sinal_vital_w
		from	dual;

		insert into atendimento_sinal_vital(	
				nr_sequencia,
				nr_atendimento,
				dt_sinal_vital,
				dt_atualizacao,
				nm_usuario,
				cd_paciente,
				qt_pa_sistolica,	
				qt_pa_diastolica,
				qt_pam,
				qt_altura_cm,           
				qt_bcf,                 
				qt_bcf_2,               
				qt_bcf_3,               
				qt_circunf_braco,       
				qt_circunf_panturrilha,             
				qt_freq_cardiaca,       
				qt_freq_resp,           
				qt_glicemia_capilar,    
				qt_glicemia_mmol,       
				qt_imc,                 
				qt_pae,                 
				qt_peso,                
				qt_pressao_intra_abd,   
				qt_pressao_intra_cranio,
				qt_pvc,                 
				qt_saturacao_o2,        
				qt_superf_corporia,     
				qt_temp,                
				qt_temp_incubadora,
				ie_mediana,
				ie_integracao,
				ie_mediana_gerada)
			values (
				nr_seq_sinal_vital_w,
				nr_atendimento_w,
				dt_sinal_vital_w,
				sysdate,
				'Tasy',
				cd_paciente_w,
				qt_pa_sistolica_w,	 		
				qt_pa_diastolica_w,	 		
				qt_pam_w,		 		
				qt_altura_cm_w,           		
				qt_bcf_w,                 		
				qt_bcf_2_w,               		
				qt_bcf_3_w,              		
				qt_circunf_braco_w,
				qt_circunf_panturrilha_w,             
				qt_freq_cardiaca_w,       
				qt_freq_resp_w,           
				qt_glicemia_capilar_w,    
				qt_glicemia_mmol_w,       
				qt_imc_w,                 
				qt_pae_w,                 
				qt_peso_w,                
				qt_pressao_intra_abd_w,   
				qt_pressao_intra_cranio_w,
				qt_pvc_w,                 
				qt_saturacao_o2_w,        
				qt_superf_corporia_w,     
				qt_temp_w,                
				qt_temp_incubadora_w,
				'1',
				'S',
				'S'
			);
    
      if (OBTER_SE_INTEGRACAO_ATIVA(965, 245) = 'S') then
        if (qt_pa_sistolica_w is not null or qt_pa_diastolica_w is not null or qt_pam_w is not null or 
            qt_freq_cardiaca_w is not null or qt_temp_w is not null or qt_saturacao_o2_w is not null or 
            qt_freq_resp_w is not null) then
            json_aux_bb := philips_json();
            json_aux_bb.put('typeID', 'VITALS');
            json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD HH24:MI:SS.SSSSS'));
            json_aux_bb.put('patientHealthSystemStayID', LPAD(nr_atendimento_w, 32, 0));
            json_aux_bb.put('groupID', '');
            
            if (qt_pa_sistolica_w is not null) then
                json_aux_bb.put('arterialBloodPressureSystolicLabel', 'arterialBloodPressureSystolic' || bb_ciclo_type_label);
                json_aux_bb.put('arterialBloodPressureSystolicEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('arterialBloodPressureSystolicEventDateTimeGmtOffset', '0');
                json_aux_bb.put('arterialBloodPressureSystolicValue', TO_CHAR(qt_pa_sistolica_w));
            end if;
            
            if (qt_pa_diastolica_w is not null) then
                json_aux_bb.put('arterialBloodPressureDiastolicLabel', 'arterialBloodPressureDiastolic' || bb_ciclo_type_label);
                json_aux_bb.put('arterialBloodPressureDiastolicEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('arterialBloodPressureDiastolicEventDateTimeGmtOffset', '0');
                json_aux_bb.put('arterialBloodPressureDiastolicValue', TO_CHAR(qt_pa_diastolica_w));
            end if;
            
            if (qt_pam_w is not null) then
                json_aux_bb.put('arterialBloodPressureMeanLabel', 'arterialBloodPressureMean' || bb_ciclo_type_label);
                json_aux_bb.put('arterialBloodPressureMeanEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('arterialBloodPressureMeanEventDateTimeGmtOffset', '0');
                json_aux_bb.put('arterialBloodPressureMeanValue', TO_CHAR(qt_pam_w));
            end if;
            
            if (qt_freq_cardiaca_w is not null) then
                json_aux_bb.put('heartRateLabel', 'heartRate' || bb_ciclo_type_label);
                json_aux_bb.put('heartRateEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('heartRateEventDateTimeGmtOffset', '0');
                json_aux_bb.put('heartRateValue', TO_CHAR(qt_freq_cardiaca_w));
            end if;
            
            if (qt_temp_w is not null) then
                json_aux_bb.put('temperatureLabel', 'temperature' || bb_ciclo_type_label);
                json_aux_bb.put('temperatureEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('temperatureEventDateTimeGmtOffset', '0');
                json_aux_bb.put('temperatureValue', TO_CHAR(qt_temp_w));
            end if;
            
            if (qt_saturacao_o2_w is not null) then
                json_aux_bb.put('oxygenSaturationLabel', 'oxygenSaturation' || bb_ciclo_type_label);
                json_aux_bb.put('oxygenSaturationEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('oxygenSaturationEventDateTimeGmtOffset', '0');
                json_aux_bb.put('oxygenSaturationValue', TO_CHAR(qt_saturacao_o2_w));
            end if;
            
            if (qt_freq_resp_w is not null) then
                json_aux_bb.put('respirationRateLabel', 'respirationRate' || bb_ciclo_type_label);
                json_aux_bb.put('respirationRateEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('respirationRateEventDateTimeGmtOffset', '0');
                json_aux_bb.put('respirationRateValue', TO_CHAR(qt_freq_resp_w));
            end if;
            
            dbms_lob.createtemporary(envio_integracao_bb, TRUE);
            json_aux_bb.to_clob(envio_integracao_bb);

            SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Vitals_Periodic',envio_integracao_bb,NVL(wheb_usuario_pck.get_nm_usuario, 'Tasy')) into retorno_integracao_bb FROM DUAL;
        
        end if;
      end if;    
		end;
	end loop;
	close C03;
else
    
    if (ie_ciclo_p= 5) then
        bb_ciclo_type_label:= '_TYPE_FIVE';
	elsif (ie_ciclo_p= 10) then
		bb_ciclo_type_label:= '_TYPE_TEN';
    else
		bb_ciclo_type_label:= '_TYPE_FIFTEEN';
	end if;

	open C02;
	loop
	fetch C02 into	
		dt_sinal_vital_w,
		cd_paciente_w,
		nr_atendimento_w,
		qt_pa_sistolica_w,	 		
		qt_pa_diastolica_w,	 		
		qt_pam_w,		 		
		qt_altura_cm_w,           		
		qt_bcf_w,                 		
		qt_bcf_2_w,               		
		qt_bcf_3_w,              		
		qt_circunf_braco_w,
		qt_circunf_panturrilha_w,             
		qt_freq_cardiaca_w,       
		qt_freq_resp_w,           
		qt_glicemia_capilar_w,    
		qt_glicemia_mmol_w,       
		qt_imc_w,                 
		qt_pae_w,                 
		qt_peso_w,                
		qt_pressao_intra_abd_w,   
		qt_pressao_intra_cranio_w,
		qt_pvc_w,                 
		qt_saturacao_o2_w,        
		qt_superf_corporia_w,     
		qt_temp_w,                
		qt_temp_incubadora_w;			
	exit when C02%notfound;
		begin
		
		select	atendimento_sinal_vital_seq.nextval
		into	nr_seq_sinal_vital_w
		from	dual;

		insert into atendimento_sinal_vital(	
				nr_sequencia,
				nr_atendimento,
				dt_sinal_vital,
				dt_atualizacao,
				nm_usuario,
				cd_paciente,
				qt_pa_sistolica,	
				qt_pa_diastolica,
				qt_pam,
				qt_altura_cm,           
				qt_bcf,                 
				qt_bcf_2,               
				qt_bcf_3,               
				qt_circunf_braco,       
				qt_circunf_panturrilha,             
				qt_freq_cardiaca,       
				qt_freq_resp,           
				qt_glicemia_capilar,    
				qt_glicemia_mmol,       
				qt_imc,                 
				qt_pae,                 
				qt_peso,                
				qt_pressao_intra_abd,   
				qt_pressao_intra_cranio,
				qt_pvc,                 
				qt_saturacao_o2,        
				qt_superf_corporia,     
				qt_temp,                
				qt_temp_incubadora,
				ie_mediana,
				ie_integracao,
				ie_mediana_gerada)
			values (
				nr_seq_sinal_vital_w,
				nr_atendimento_w,
				dt_sinal_vital_w,
				sysdate,
				'Tasy',
				cd_paciente_w,
				qt_pa_sistolica_w,	 		
				qt_pa_diastolica_w,	 		
				qt_pam_w,		 		
				qt_altura_cm_w,           		
				qt_bcf_w,                 		
				qt_bcf_2_w,               		
				qt_bcf_3_w,              		
				qt_circunf_braco_w,
				qt_circunf_panturrilha_w,             
				qt_freq_cardiaca_w,       
				qt_freq_resp_w,           
				qt_glicemia_capilar_w,    
				qt_glicemia_mmol_w,       
				qt_imc_w,                 
				qt_pae_w,                 
				qt_peso_w,                
				qt_pressao_intra_abd_w,   
				qt_pressao_intra_cranio_w,
				qt_pvc_w,                 
				qt_saturacao_o2_w,        
				qt_superf_corporia_w,     
				qt_temp_w,                
				qt_temp_incubadora_w,
				to_char(ie_ciclo_p),
				'S',
				'S'
			);

      if (OBTER_SE_INTEGRACAO_ATIVA(965, 245) = 'S') then
        if (qt_pa_sistolica_w is not null or qt_pa_diastolica_w is not null or qt_pam_w is not null or 
            qt_freq_cardiaca_w is not null or qt_temp_w is not null or qt_saturacao_o2_w is not null or 
            qt_freq_resp_w is not null) then
            json_aux_bb := philips_json();
            json_aux_bb.put('typeID', 'VITALS');
            json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD HH24:MI:SS.SSSSS'));
            json_aux_bb.put('patientHealthSystemStayID', LPAD(nr_atendimento_w, 32, 0));
            json_aux_bb.put('groupID', '');
            
            if (qt_pa_sistolica_w is not null) then
                json_aux_bb.put('arterialBloodPressureSystolicLabel', 'arterialBloodPressureSystolic' || bb_ciclo_type_label);
                json_aux_bb.put('arterialBloodPressureSystolicEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('arterialBloodPressureSystolicEventDateTimeGmtOffset', '0');
                json_aux_bb.put('arterialBloodPressureSystolicValue', TO_CHAR(qt_pa_sistolica_w));
            end if;
            
            if (qt_pa_diastolica_w is not null) then
                json_aux_bb.put('arterialBloodPressureDiastolicLabel', 'arterialBloodPressureDiastolic' || bb_ciclo_type_label);
                json_aux_bb.put('arterialBloodPressureDiastolicEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('arterialBloodPressureDiastolicEventDateTimeGmtOffset', '0');
                json_aux_bb.put('arterialBloodPressureDiastolicValue', TO_CHAR(qt_pa_diastolica_w));
            end if;
            
            if (qt_pam_w is not null) then
                json_aux_bb.put('arterialBloodPressureMeanLabel', 'arterialBloodPressureMean' || bb_ciclo_type_label);
                json_aux_bb.put('arterialBloodPressureMeanEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('arterialBloodPressureMeanEventDateTimeGmtOffset', '0');
                json_aux_bb.put('arterialBloodPressureMeanValue', TO_CHAR(qt_pam_w));
            end if;
            
            if (qt_freq_cardiaca_w is not null) then
                json_aux_bb.put('heartRateLabel', 'heartRate' || bb_ciclo_type_label);
                json_aux_bb.put('heartRateEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('heartRateEventDateTimeGmtOffset', '0');
                json_aux_bb.put('heartRateValue', TO_CHAR(qt_freq_cardiaca_w));
            end if;
            
            if (qt_temp_w is not null) then
                json_aux_bb.put('temperatureLabel', 'temperature' || bb_ciclo_type_label);
                json_aux_bb.put('temperatureEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('temperatureEventDateTimeGmtOffset', '0');
                json_aux_bb.put('temperatureValue', TO_CHAR(qt_temp_w));
            end if;
            
            if (qt_saturacao_o2_w is not null) then
                json_aux_bb.put('oxygenSaturationLabel', 'oxygenSaturation' || bb_ciclo_type_label);
                json_aux_bb.put('oxygenSaturationEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('oxygenSaturationEventDateTimeGmtOffset', '0');
                json_aux_bb.put('oxygenSaturationValue', TO_CHAR(qt_saturacao_o2_w));
            end if;
            
            if (qt_freq_resp_w is not null) then
                json_aux_bb.put('respirationRateLabel', 'respirationRate' || bb_ciclo_type_label);
                json_aux_bb.put('respirationRateEventDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI'));
                json_aux_bb.put('respirationRateEventDateTimeGmtOffset', '0');
                json_aux_bb.put('respirationRateValue', TO_CHAR(qt_freq_resp_w));
            end if;
            
            dbms_lob.createtemporary(envio_integracao_bb, TRUE);
            json_aux_bb.to_clob(envio_integracao_bb);

            SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Vitals_Periodic',envio_integracao_bb,NVL(wheb_usuario_pck.get_nm_usuario, 'Tasy')) into retorno_integracao_bb FROM DUAL;
        
        end if;
      end if;
		end;							
	end loop;
	close C02;
end if;	

commit;

end calc_medianas_sinais_vitais;
/