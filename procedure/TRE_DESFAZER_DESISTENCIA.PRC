create or replace
procedure tre_desfazer_desistencia(	nr_seq_inscrito_p		number,
									nr_seq_motivo_p			number,	
									nm_usuario_p			Varchar2,
									dt_encerramento_p		date,
									cd_estabelecimento_p	number) is 

				
nr_seq_evento_w			number(10);
cd_pessoa_fisica_w		varchar2(10);
dt_inicio_w				date;
dt_fim_w				date;
dt_fim_real_w			date;	
nr_seq_inscrito_w		number(10);
ds_motivo_desistencia_w	varchar2(90);		
nr_seq_pac_dia_w		number(10);	
begin

	/* Tipos 
	
	C - Candidato
	I - Inscrito
	
	*/	

	update	tre_inscrito
	set	dt_desistencia			= null,
		nr_seq_motivo_desistencia 	= null,
		dt_atualizacao			= sysdate,
		nm_usuario			= nm_usuario_p
	where	nr_sequencia			= nr_seq_inscrito_p;
				
	select	max(ds_motivo_desistencia)
	into	ds_motivo_desistencia_w
	from	tre_motivo_desistencia
	where	nr_sequencia = nr_seq_motivo_p;
	
	tre_retira_bloqueio_pac(nr_seq_inscrito_p, dt_encerramento_p,ds_motivo_desistencia_w, nm_usuario_p);

commit;

end tre_desfazer_desistencia;
/
