create or replace
procedure ptu_gerar_nota_cobranca700
			(	nr_seq_servico_pre_p	Number,
				cd_versao_p		Varchar2,
				nm_usuario_p		Varchar2) is 

ds_tipo_atendimento_w		varchar2(255);
ds_motivo_saida_w		varchar2(255);
nm_segurado_w			varchar2(25);
cd_guia_w			varchar2(20);
cd_usuario_plano_w		varchar2(13);
cd_doenca_w			varchar2(10);
ie_motivo_saida_w		varchar2(2) := '1';
ie_tipo_atendimento_w		varchar2(2) := '1';
cd_excecao_w			varchar2(1);
nr_seq_conta_w			number(10);
nr_seq_cobranca_w		number(10);
nr_seq_protocolo_w		number(10);
nr_seq_saida_spsadt_w		number(10);
nr_lote_w			number(10);
nr_seq_tipo_atendimento_w	number(10);
cd_cooperativa_w		varchar2(10);
nr_seq_outorgante_w		number(4);
cd_estabelecimento_w		number(4);
ie_carater_internacao_w		number(1);
dt_atendimento_referencia_w	date;
ie_pcmso_w			varchar2(10);
dt_prev_envio_w			date;
pr_taxa_atual_w			number(15,2)	:= 0;
ie_envia_w			varchar2(1)	:= 'S';
ie_tipo_guia_w			Varchar2(10);
ie_tipo_consulta_w		Number(10);

nr_seq_motivo_saida_int_w	number(10);
nr_seq_guia_w			number(10);
nr_seq_prestador_w		number(10);
cd_pessoa_fisica_w		varchar2(14);
cd_medico_solic_w		varchar2(10);
cd_guia_referencia_w		varchar2(20);
qt_registro_w			number(10) := 0;

dt_internacao_w			date;
dt_alta_w			date;
dt_ultima_autoriz_w		date;
tp_nota_w			number(1);
id_nota_principal_w		varchar2(1);
nr_ver_tiss_w			varchar2(7);
nr_guia_tiss_prestador_w	varchar2(20);
nr_guia_tiss_principal_w	varchar2(20);
nr_guia_tiss_operadora_w	varchar2(20);
tp_ind_acidente_w		varchar2(1);
motivo_encerram_w		varchar2(2);
nr_cnpj_cpf_req_w		varchar2(14);
nm_prest_req_w			varchar2(40);
sg_cons_prof_req_w		varchar2(12);
nr_cons_prof_req_w		varchar2(15);
sg_uf_cons_req_w		valor_dominio.vl_dominio%type;
nr_cbo_req_w			number(6);
nr_fatura_glosada_w		ptu_nota_cobranca.nr_fatura_glosada%type;
nr_ndr_glosada_w		ptu_nota_cobranca.nr_ndr_glosada%type;
nr_nota_glosada_w		number(11) := null;
nr_lote_glosado_w		number(8) := null;
dt_protocolo_w			date;
id_rn_w				varchar2(1);

cursor C01 is
	select	substr(pls_obter_dados_segurado(a.nr_seq_segurado,'C'),5,13),
		substr(upper(Elimina_Acentos(pls_obter_dados_segurado(a.nr_seq_segurado,'N'))),1,25),
		decode(f.ie_grupo_evento, '1', 	decode(a.ie_carater_internacao,	'E',0,0), 
						decode(a.ie_carater_internacao,	'E',0,
										'U',1,1)) ie_carater_internacao,
		obter_somente_numero(nvl(a.cd_guia_referencia,a.cd_guia)),
		nvl(a.dt_atendimento_referencia,sysdate),
		nvl(a.cd_excecao,'0'),
		a.nr_seq_tipo_atendimento,
		a.nr_seq_saida_spsadt,
		a.cd_cooperativa,
		b.nr_sequencia,
		a.nr_sequencia,
		c.ie_pcmso,
		pls_obter_taxa_interc_conta(a.dt_atendimento_referencia,dt_prev_envio_w,d.nr_sequencia,c.nr_sequencia) pr_taxa,
		pls_obter_se_envia_conta(nvl(a.dt_atendimento,a.dt_atendimento_referencia),dt_prev_envio_w,d.nr_sequencia,c.nr_sequencia,null) ie_envia,
		a.ie_tipo_guia,
		a.ie_tipo_consulta,
		a.cd_guia_referencia,
		a.dt_entrada,
		a.dt_alta,
		substr(b.cd_versao_tiss,1,7),
		nvl(a.ie_recem_nascido,'N'),
		b.dt_protocolo,
		a.cd_guia_prestador,
		a.cd_guia_referencia,
		a.cd_guia,
		a.nr_seq_saida_int,
		a.nr_seq_guia,
		ptu_obter_tp_nota(a.nr_sequencia),
		ptu_obter_id_nota_principal(a.nr_sequencia),
		ptu_obter_tp_ind_acidente(a.ie_indicacao_acidente),
		ptu_obter_dt_ultima_autor(nr_seq_guia),
		a.cd_medico_solicitante,
		a.nr_seq_prestador
	from	pls_tipo_atendimento		f,
		pls_conta_medica_resumo		e,
		pls_protocolo_conta		b,
		pls_conta			a,
		pls_segurado			c,
		pls_congenere			d
	where	c.nr_sequencia			= a.nr_seq_segurado
	and	to_number(a.cd_cooperativa)	= to_number(d.cd_cooperativa)
	and	a.nr_seq_protocolo		= b.nr_sequencia
	and	a.nr_sequencia			= e.nr_seq_conta
	and	f.nr_sequencia(+)		= e.nr_seq_tipo_atendimento
	and	e.nr_seq_serv_pre_pgto		= nr_seq_servico_pre_p
	and	nvl(b.ie_tipo_protocolo,'C') in ('C','F','I')
	and	((e.ie_situacao != 'I') or (e.ie_situacao is null));
	
begin
cd_cooperativa_w	:= pls_obter_unimed_estab(cd_estabelecimento_w);

open C01;
loop
fetch C01 into
	cd_usuario_plano_w,
	nm_segurado_w,
	ie_carater_internacao_w,
	cd_guia_w,
	dt_atendimento_referencia_w,
	cd_excecao_w,
	nr_seq_tipo_atendimento_w,
	nr_seq_saida_spsadt_w,
	nr_seq_outorgante_w,
	nr_seq_protocolo_w,
	nr_seq_conta_w,
	ie_pcmso_w,
	pr_taxa_atual_w,
	ie_envia_w,
	ie_tipo_guia_w,
	ie_tipo_consulta_w,
	cd_guia_referencia_w,
	dt_internacao_w,
	dt_alta_w,
	nr_ver_tiss_w,
	id_rn_w,
	dt_protocolo_w,
	nr_guia_tiss_prestador_w,
	nr_guia_tiss_principal_w,
	nr_guia_tiss_operadora_w,
	nr_seq_motivo_saida_int_w,
	nr_seq_guia_w,
	tp_nota_w,
	id_nota_principal_w,
	tp_ind_acidente_w,
	dt_ultima_autoriz_w,
	cd_medico_solic_w,
	nr_seq_prestador_w;
exit when C01%notfound;
	begin
	
	select	max(cd_ptu)
	into	motivo_encerram_w
	from	pls_motivo_saida
	where	nr_sequencia = nr_seq_motivo_saida_int_w;
	
	nr_cnpj_cpf_req_w	:= null;
	nm_prest_req_w		:= null;
	sg_cons_prof_req_w	:= null;
	nr_cons_prof_req_w	:= null;
	sg_uf_cons_req_w	:= null;
	nr_cbo_req_w		:= null;
	
	----------------------------------------------------------------------------------		DADOS DO SOLICITANTE/REQUISITANTE		----------------------------------------------------------------------------------  
	-- Buscar primeiro os dados do m�dico solicitante da conta 
	if	(cd_medico_solic_w is not null) then
		nr_cnpj_cpf_req_w	:= substr(obter_dados_pf(cd_medico_solic_w,'CPF'),1,14);
		nm_prest_req_w		:= substr(obter_nome_medico(cd_medico_solic_w,'N'),1,40);
		sg_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'SGCRM'),1,12);
		nr_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'CRM'),1,15);
		sg_uf_cons_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'UFCRM'),1,2);
		
		select	max(cd_cbo)
		into	nr_cbo_req_w
		from	sus_cbo_pessoa_fisica
		where	cd_pessoa_fisica = cd_medico_solic_w;
		
	-- Se n�o tiver, buscar o prestador do atendimento
	elsif	(nr_seq_prestador_w is not null) then
		select	nvl(a.cd_cgc,substr(obter_dados_pf(a.cd_pessoa_fisica,'CPF'),1,14)),
			a.cd_pessoa_fisica,
			substr(pls_obter_dados_prestador(a.nr_sequencia,'N'),1,40)
		into	nr_cnpj_cpf_req_w,
			cd_medico_solic_w,
			nm_prest_req_w
		from	pls_prestador	a
		where	a.nr_sequencia	= nr_seq_prestador_w;
		
		if	(cd_medico_solic_w is not null) then
			nr_cnpj_cpf_req_w	:= substr(obter_dados_pf(cd_medico_solic_w,'CPF'),1,14);
			nm_prest_req_w		:= substr(obter_nome_medico(cd_medico_solic_w,'N'),1,40);
			sg_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'SGCRM'),1,12);
			nr_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'CRM'),1,15);
			sg_uf_cons_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'UFCRM'),1,2);
			
			select	max(cd_cbo)
			into	nr_cbo_req_w
			from	sus_cbo_pessoa_fisica
			where	cd_pessoa_fisica = cd_medico_solic_w;
		end if;
	
	elsif	(cd_guia_referencia_w is not null) then
		select	count(1)
		into	qt_registro_w
		from	pls_guia_plano
		where	cd_guia	= cd_guia_referencia_w;
		
		if	(qt_registro_w > 0) then
			select	max(cd_medico_solicitante),
				substr(pls_obter_dados_prestador(max(nr_seq_prestador),'N'),1,40)
			into	cd_medico_solic_w,
				nm_prest_req_w
			from	pls_guia_plano
			where	cd_guia	= cd_guia_referencia_w;
			
			if	(cd_medico_solic_w is not null) then
				nr_cnpj_cpf_req_w	:= substr(obter_dados_pf(cd_medico_solic_w,'CPF'),1,14);
				nm_prest_req_w		:= nvl(substr(obter_nome_medico(cd_medico_solic_w,'N'),1,40),nm_prest_req_w);
				sg_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'SGCRM'),1,12);
				nr_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'CRM'),1,15);
				sg_uf_cons_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'UFCRM'),1,2);
				
				select	max(cd_cbo)
				into	nr_cbo_req_w
				from	sus_cbo_pessoa_fisica
				where	cd_pessoa_fisica = cd_medico_solic_w;
			end if;
		end if;
	end if;
	----------------------------------------------------------------------------------	FIM	DADOS DO SOLICITANTE/REQUISITANTE	FIM	----------------------------------------------------------------------------------  

	begin
	select	cd_doenca
	into	cd_doenca_w
	from	pls_diagnostico_conta
	where	nr_seq_conta	= nr_seq_conta_w
	and	ie_classificacao = 'P';
	exception
	when others then
		cd_doenca_w	:= '';
	end;
	
	begin
	select	substr(cd_tiss,2,2)
	into	ie_tipo_atendimento_w
	from    pls_tipo_atendimento
	where	nr_sequencia	= nr_seq_tipo_atendimento_w;
	exception
	when others then
		ie_tipo_atendimento_w	:= '';
	end;

	if	(ie_tipo_guia_w	= '3') then -- Somente guia de consulta
		ie_tipo_atendimento_w	:= '4';
		ie_carater_internacao_w	:= ptu_obter_carater_internacao('A700', cd_versao_p, ie_tipo_consulta_w);
	end if;
	
	begin
	select	substr(cd_tiss,1,1)
	into	ie_motivo_saida_w
	from	pls_motivo_saida_sadt
	where	nr_sequencia	= nr_seq_saida_spsadt_w;
	exception
	when others then
		ie_motivo_saida_w	:= '1';
	end;
	
	select	max(nr_sequencia)
	into	nr_seq_cobranca_w
	from	ptu_nota_cobranca
	where	nr_guia_principal	= cd_guia_w
	and	cd_usuario_plano	= cd_usuario_plano_w;
	
	if	(nr_seq_cobranca_w is null) then
	
		select	ptu_nota_cobranca_seq.nextval
		into	nr_seq_cobranca_w
		from	dual;
		
		nr_lote_w := nr_seq_conta_w + nr_seq_protocolo_w;
		
		insert into ptu_nota_cobranca
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_serv_pre_pagto,
			nr_lote,
			nr_nota,
			cd_unimed,
			cd_usuario_plano,
			nm_beneficiario,
			dt_atendimento,
			cd_excecao,
			ie_carater_atendimento,
			cd_cid,
			ie_paciente,
			ie_tipo_saida_spdat,
			ie_tipo_atendimento,
			nr_guia_principal,
			ie_pcmso,
			pr_taxa,
			ie_envia_conta,
			dt_internacao,
			dt_alta,
			dt_ultima_autoriz,
			tp_nota,
			id_nota_principal,
			nr_ver_tiss,
			nr_guia_tiss_prestador,
			nr_guia_tiss_principal,
			nr_guia_tiss_operadora,
			tp_ind_acidente,
			motivo_encerram,
			nr_cnpj_cpf_req,
			nm_prest_req,
			sg_cons_prof_req,
			nr_cons_prof_req,
			sg_uf_cons_req,
			nr_cbo_req,
			nr_fatura_glosada,
			nr_ndr_glosada,
			nr_nota_glosada,
			nr_lote_glosado,
			dt_protocolo,
			id_rn)
		values	(nr_seq_cobranca_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_servico_pre_p,
			nr_lote_w,
			nr_seq_conta_w,
			cd_cooperativa_w,
			cd_usuario_plano_w,
			nm_segurado_w,
			dt_atendimento_referencia_w,
			cd_excecao_w,
			ie_carater_internacao_w,
			cd_doenca_w,
			'1',
			ie_motivo_saida_w,
			ie_tipo_atendimento_w,
			cd_guia_w,
			ie_pcmso_w,
			pr_taxa_atual_w,
			ie_envia_w,
			dt_internacao_w,
			dt_alta_w,
			dt_ultima_autoriz_w,
			tp_nota_w,
			id_nota_principal_w,
			nr_ver_tiss_w,
			nr_guia_tiss_prestador_w,
			nr_guia_tiss_principal_w,
			nr_guia_tiss_operadora_w,
			tp_ind_acidente_w,
			motivo_encerram_w,
			nr_cnpj_cpf_req_w,
			nm_prest_req_w,
			sg_cons_prof_req_w,
			nr_cons_prof_req_w,
			sg_uf_cons_req_w,
			nr_cbo_req_w,
			nr_fatura_glosada_w,
			nr_ndr_glosada_w,
			nr_nota_glosada_w,
			nr_lote_glosado_w,
			dt_protocolo_w,
			id_rn_w);
	end if;

	ptu_gerar_nota_hospitalar700(nr_seq_cobranca_w, cd_versao_p, nm_usuario_p);
	ptu_gerar_nota_servico700(nr_seq_cobranca_w, cd_versao_p, nm_usuario_p);
	ptu_gerar_nota_complemento700(nr_seq_cobranca_w, cd_versao_p, nm_usuario_p);
	end;
end loop;
close C01;

end ptu_gerar_nota_cobranca700;
/