create or replace
procedure pls_inserir_observacao_guia
			(	nr_seq_guia_p	Number,
				ds_observacao_p	Varchar2,
				nm_usuario_p	Varchar2) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir observa��o na guia
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

update	pls_guia_plano
set	ds_observacao	= substr(ds_observacao||chr(10)||ds_observacao_p,1,4000),
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_guia_p;

commit;

end pls_inserir_observacao_guia;
/