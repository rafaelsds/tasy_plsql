create or replace
procedure inserir_proc_taxa_bipagem	(nr_cirurgia_p 		number,
					nr_prescricao_p		number,
					nr_seq_proc_interno_p	number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nm_usuario_p		varchar2) is

					
nr_sequencia_w		NUMBER(6);					
cd_procedimento_w		varchar2(255);
ie_origem_proced_w		varchar2(1);
nr_atendimento_w	number(10);
					
begin
begin

cd_procedimento_w := nvl(cd_procedimento_p,0);
ie_origem_proced_w := nvl(ie_origem_proced_p,0);

select nr_atendimento
into nr_atendimento_w
from cirurgia
where nr_cirurgia = nr_cirurgia_p;

if ((cd_procedimento_w = 0 or ie_origem_proced_w = 0) and nr_atendimento_w is not null) then
	obter_proc_tab_interno(nr_seq_proc_interno_p, 0, nr_atendimento_w, 0, cd_procedimento_w, ie_origem_proced_w);
end if;




if (nvl(nr_seq_proc_interno_p,0) > 0) then

	if (nvl(nr_prescricao_p,0) > 0) then
		select	max(nr_sequencia) + 1
		into	nr_sequencia_w
		from 	prescr_procedimento
		where 	nr_prescricao = nr_prescricao_p;
	end if;


	insert into prescr_procedimento(
			nr_prescricao,
			nr_sequencia,
			nr_seq_proc_interno,
			cd_procedimento,
			ie_origem_proced,
			qt_procedimento,
			ie_urgencia,
			ie_suspenso,
			dt_atualizacao,
			nm_usuario,
			ie_origem_inf,
			ie_avisar_result,
			cd_motivo_baixa)
		values (nr_prescricao_p,
			nvl(nr_sequencia_w,1),
			nr_seq_proc_interno_p,
			cd_procedimento_w,
			1,
			1,
			'N',
			'N',
			sysdate,
			nm_usuario_p,
			'1',
			'N',
			0);
			
			
	commit;

end if;



exception
when others then
	null;
end;

end inserir_proc_taxa_bipagem;
/