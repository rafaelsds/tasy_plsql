create or replace
procedure pls_gerar_proposta_benef_multi
			(	nr_seq_solicitacao_p		number,
				cd_estabelecimento_p		number,
				nr_seq_proposta_p		number,
				nr_seq_proposta_nova_p 	out 	number,
				ds_erro_p		out	varchar2,
				nm_usuario_p			varchar2) is

cd_pessoa_fisica_w		varchar2(10);
nr_seq_proposta_w		number(10);
nr_seq_contrato_w		number(10);
cd_estabelecimento_w		number(4);
ie_tipo_beneficiario_w		varchar2(3);
cd_cgc_estipulante_w		varchar2(14);
cd_pf_estipulante_w		varchar2(10);
nr_seq_vendedor_canal_w		number(10);
nr_seq_vendedor_pf_w		number(10);
ie_tipo_contratacao_w		varchar2(2);
nr_seq_pagador_w		number(10);
nr_seq_plano_w			number(10);
nr_seq_tabela_w			number(10);
ie_consistir_sib_w		varchar2(1);
dt_admissao_w			date;
nr_seq_vinculo_estip_w		number(10);
cd_matricula_est_w		varchar(30);
cd_cbo_w			varchar(6);
ds_erro_w			varchar2(2000);
nr_seq_parentesco_w		number(10);
nr_seq_titular_contrato_w	number(10);
nr_seq_subestipulante_w		number(10);
nr_seq_proposta_benef_w		number(10);
ie_gerar_contratacao_w		varchar2(1);
ie_liberar_proposta_w		varchar2(1);
ie_aprovar_proposta_w		varchar2(1);
dt_contratacao_w		date;
nr_contrato_w			number(10);
nr_seq_titular_solic_w		number(10);
nr_seq_regra_lanc_w		number(10);
ie_acao_regra_pagador_w		varchar2(2);
cd_condicao_pagamento_w		number(10);
cd_tipo_portador_w		number(10);
cd_portador_w			number(10);
cd_cgc_pagador_w		varchar2(14);
cd_pf_pagador_w			varchar2(10);
nr_seq_conta_banco_w		number(10);
ie_regulamentacao_w		varchar2(2);
nr_seq_classif_itens_w		number(10);
ie_mes_vencimento_w		varchar2(1);
dt_dia_vencimento_w		number(2);
nr_seq_plano_sca_w		number(10);
nr_seq_tabela_sca_w		number(10);
nr_seq_vendedor_canal_sca_w	number(10);
nr_seq_vendedor_pf_sca_w	number(10);
nr_seq_solicitacao_benef_w	number(10);
nr_seq_motivo_inclusao_w	pls_inclusao_beneficiario.nr_seq_motivo_inclusao%type;
nr_seq_faixa_salarial_w		pls_faixa_salarial.nr_sequencia%type;
nr_seq_proposta_benef_tit_w	pls_proposta_beneficiario.nr_sequencia%type	:= null;
dt_anexo_w			pls_inclusao_anexo.dt_anexo%type;
ds_anexo_w			pls_inclusao_anexo.ds_anexo%type;
nr_seq_tipo_doc_w		pls_inclusao_anexo.nr_seq_tipo_documento%type;
ie_tipo_parentesco_w		pls_proposta_beneficiario.ie_tipo_parentesco%type;
ie_pagador_princ_w		varchar2(1);
qt_pagador_principal_w		number;
cd_contrato_principal_w		number;
qt_dias_rescisao_mig_w		pls_parametros.qt_dias_rescisao_mig%type;
ie_gerar_proposta_mig_resc_w	pls_parametros.ie_gerar_proposta_mig_resc%type;
cd_carteira_ant_w		pls_inclusao_beneficiario.cd_carteira_ant%type;
dt_rescisao_cart_ant_w		pls_segurado.dt_rescisao%type;
qt_dias_rescindidos_w		pls_integer;
qt_benef_migrados_w		pls_integer := 0;
qt_benef_proposta_w		pls_integer := 0;
nr_contrato_ant_w		pls_contrato.nr_contrato%type;
nr_seq_segurado_ant_w		pls_segurado.nr_sequencia%type;
nr_seq_proposta_web_w		pls_inclusao_beneficiario.nr_seq_proposta_web%type;
cd_cgc_pagador_ww		pls_contrato_pagador.cd_cgc%type;
cd_pessoa_fisica_pagador_w	pls_contrato_pagador.cd_pessoa_fisica%type;
nr_seq_pagador_ww		pls_contrato_pagador.nr_sequencia%type;

Cursor C01 is
	select	a.ie_acao_regra,
		a.nr_seq_classif_itens,
		nvl(a.ie_mes_vencimento,'A'),
		a.dt_dia_vencimento
	from	pls_regra_pagador_imp_lote a
	where	((a.ie_tipo_contratacao	= ie_tipo_contratacao_w) or (a.ie_tipo_contratacao is null))
	and	((a.ie_regulamentacao = ie_regulamentacao_w) or (a.ie_regulamentacao is null))
	and	((a.nr_seq_contrato = nr_seq_contrato_w) or (a.nr_seq_contrato is null))
	order by	nvl(a.nr_seq_contrato,0),
			nvl(a.ie_regulamentacao,' '),
			nvl(a.ie_tipo_contratacao,' ');

Cursor C02 is
	select	nr_seq_sca
	from	pls_inclusao_benef_sca
	where	nr_seq_inclusao_benef	= nr_seq_solicitacao_p
	and	ie_selecao		= 'S';

Cursor C03 is
	select	cd_pessoa_fisica,
		nr_seq_contrato,
		nr_seq_vendedor_canal,
		nr_seq_vendedor_pf,
		dt_admissao,
		nr_seq_vinculo_estip,
		cd_matricula_est,
		cd_cbo,
		nr_seq_plano,
		nr_seq_parentesco,
		nr_seq_titular_contrato,
		nr_seq_subestipulante,
		dt_contratacao,
		nr_seq_titular_solic,
		nr_seq_faixa_salarial,
		nr_sequencia,
		nr_seq_motivo_inclusao,
		cd_carteira_ant
	from	pls_inclusao_beneficiario
	where	((nr_seq_titular_solic = nr_seq_solicitacao_p) or (nr_sequencia = nr_seq_solicitacao_p))
	and	nr_seq_proposta_benef is null
	and	cd_pessoa_fisica is not null
	and	ie_status = 'P'
	order by nvl(nr_seq_titular_solic,0);
	
Cursor C04 is
	select  dt_anexo,
		ds_anexo,
		nr_seq_tipo_documento
	from 	pls_inclusao_anexo
	where 	nr_seq_inclusao = nr_seq_solicitacao_benef_w;

begin

ie_consistir_sib_w	:= nvl(obter_valor_param_usuario(1232, 4, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');
ie_aprovar_proposta_w	:= nvl(obter_valor_param_usuario(1232, 23, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');
ie_gerar_contratacao_w	:= nvl(obter_valor_param_usuario(1232, 28, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');
ie_liberar_proposta_w	:= nvl(obter_valor_param_usuario(1232, 42, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');

select	nvl(qt_dias_rescisao_mig,0),
	nvl(ie_gerar_proposta_mig_resc,'N')
into	qt_dias_rescisao_mig_w,
	ie_gerar_proposta_mig_resc_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_consistir_sib_w	= 'S') then
	ie_consistir_sib_w	:= 'N';
elsif	(ie_consistir_sib_w	= 'N') then
	ie_consistir_sib_w	:= 'S';
end if;

select	nr_seq_contrato,
	cd_estabelecimento,
	nr_seq_vendedor_canal,
	nr_seq_vendedor_pf,
	nr_seq_titular_solic,
	nr_seq_proposta_web,
	dt_contratacao
into	nr_seq_contrato_w,
	cd_estabelecimento_w,
	nr_seq_vendedor_canal_w,
	nr_seq_vendedor_pf_w,
	nr_seq_titular_solic_w,
	nr_seq_proposta_web_w,
	dt_contratacao_w
from	pls_inclusao_beneficiario
where	nr_sequencia	= nr_seq_solicitacao_p;

if	(nr_seq_titular_solic_w is not null) then
	select	max(nr_seq_proposta_benef)
	into	nr_seq_proposta_benef_tit_w
	from	pls_inclusao_beneficiario
	where	nr_sequencia	= nr_seq_titular_solic_w;
	
	if	(nr_seq_proposta_benef_tit_w is null) then -- Caso ainda n�o tenha contratado o titular, a contrata��o deve ser realizada pelo titular
		wheb_mensagem_pck.exibir_mensagem_abort(320934,	'NM_TITULAR='||substr(pls_obter_dados_inclusao(nr_seq_titular_solic_w,'N'),1,255)||';'||'NR_SEQ_TITULAR='||nr_seq_titular_solic_w);
	end if;
end if;

-- Caso n�o existir vendedor informado, buscar da regra de lan�amento automatico do contrato
if	(nr_seq_vendedor_canal_w is null) then
	if	(nvl(pls_obter_dados_contrato(nr_seq_contrato_w, 'AR'),'T') in ('A','T')) then
		if	(nr_seq_contrato_w is not null) then		
			nr_seq_vendedor_canal_w	:= pls_obter_regra_lancamento(nr_seq_contrato_w, dt_contratacao_w, 'CV');
			nr_seq_vendedor_pf_w	:= pls_obter_regra_lancamento(nr_seq_contrato_w, dt_contratacao_w, 'V');
		end if;	
	end if;
end if;

if	(nr_seq_contrato_w is not null) then
	select	ie_tipo_beneficiario,
		cd_cgc_estipulante,
		cd_pf_estipulante,
		nr_contrato
	into	ie_tipo_beneficiario_w,
		cd_cgc_estipulante_w,
		cd_pf_estipulante_w,
		nr_contrato_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;

	--Obter o tipo de contrata��o da proposta
	select	max(b.ie_tipo_contratacao),
		max(b.ie_regulamentacao)
	into	ie_tipo_contratacao_w,
		ie_regulamentacao_w
	from	pls_plano		b,
		pls_contrato_plano	a
	where	a.nr_seq_plano		= b.nr_sequencia
	and	a.nr_seq_contrato	= nr_seq_contrato_w
	and	a.ie_situacao		= 'A';
	
	if 	(nvl(nr_seq_proposta_p,0) = 0) then	
		select	pls_proposta_adesao_seq.nextval
		into	nr_seq_proposta_w
		from	dual;

		insert	into	pls_proposta_adesao
			(nr_sequencia, ie_tipo_contratacao, cd_estabelecimento,
			ie_tipo_beneficiario, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_contrato,
			cd_cgc_estipulante, cd_estipulante, dt_inicio_proposta,
			ie_tipo_processo, ie_status, nr_seq_vendedor_canal,
			nr_seq_vendedor_pf, vl_proposta, ie_tipo_proposta)
		values(	nr_seq_proposta_w, ie_tipo_contratacao_w, cd_estabelecimento_w,
			ie_tipo_beneficiario_w, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, nr_contrato_w,
			cd_cgc_estipulante_w, cd_pf_estipulante_w, sysdate,
			'W', 'U', nr_seq_vendedor_canal_w,
			nr_seq_vendedor_pf_w, 0, 2);
		--Carregar dados do contrato
		pls_proposta_dados_contrato(nr_seq_proposta_w, nr_seq_contrato_w,'N', nm_usuario_p);
		nr_seq_proposta_nova_p := nr_seq_proposta_w;
        else 
		nr_seq_proposta_w := nr_seq_proposta_p;
	end if;
end if;

--caso a proposta n�o seja criada,, tenta associar com a proposta_web
if	(nr_seq_proposta_w is null and nr_seq_proposta_web_w is not null) then
	nr_seq_proposta_w := nr_seq_proposta_web_w;
end if;


--Verifica se existe mais de um pagador principal na proposta
select	count(1)
into	qt_pagador_principal_w
from	pls_proposta_pagador
where	nr_seq_proposta	= nr_seq_proposta_w
and	ie_tipo_pagador	= 'P';

--Obter o pagador da proposta
if	(qt_pagador_principal_w >1) then
	--Parametro 115 - Utilizar pagador principal do contrato mais espec�fico
	ie_pagador_princ_w := nvl(obter_valor_param_usuario(1232,115, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'N');

	--caso o par�metro esteja setado para 'S' e exista mais de um pagador principal na proposta 
	if	(nr_seq_contrato_w is not null and ie_pagador_princ_w = 'S' and qt_pagador_principal_w >1) then
		select	max(a.nr_sequencia)
		into	nr_seq_pagador_w
		from	pls_proposta_pagador	a,
			pls_proposta_adesao	b,
			pls_contrato_pagador	c
		where	a.nr_seq_proposta = b.nr_sequencia
		and	c.nr_seq_contrato = b.nr_seq_contrato
		and	a.nr_seq_proposta = nr_seq_proposta_w
		and	c.nr_seq_contrato = nr_seq_contrato_w
		and	((a.cd_cgc_pagador is not null and a.cd_cgc_pagador = c.cd_cgc) 
		or 	(a.cd_pagador is not null and a.cd_pagador = c.cd_pessoa_fisica))
		and	a.ie_tipo_pagador	= 'P';
	else	
		select	max(cd_contrato_principal)
		into	cd_contrato_principal_w
		from	pls_contrato
		where 	nr_sequencia = nr_seq_contrato_w;

		if	(cd_contrato_principal_w is not null)	then
			select	max(nr_sequencia)
			into	nr_seq_pagador_ww
			from	pls_contrato_pagador			
			where	nr_seq_contrato = cd_contrato_principal_w			
			and	ie_tipo_pagador	= 'P';
			
			if	(nr_seq_pagador_ww is not null) then
				select	cd_cgc,
					cd_pessoa_fisica
				into	cd_cgc_pagador_ww,
					cd_pessoa_fisica_pagador_w
				from	pls_contrato_pagador
				where	nr_sequencia = nr_seq_pagador_ww;
				
				if	(cd_cgc_pagador_ww is not null) then
					select	max(nr_sequencia)
					into	nr_seq_pagador_w
					from	pls_proposta_pagador
					where	cd_cgc_pagador = cd_cgc_pagador_ww
					and	nr_seq_proposta = nr_seq_proposta_w;
				elsif	(cd_pessoa_fisica_pagador_w is not null) then
					select	max(nr_sequencia)
					into	nr_seq_pagador_w
					from	pls_proposta_pagador
					where	cd_pagador = cd_pessoa_fisica_pagador_w
					and	nr_seq_proposta = nr_seq_proposta_w;
				end if;
			end if;
		end if;	
	end if;
end if;

--Busca pagador principal da proposta
	
if	(nr_seq_pagador_w is null) then
	select	max(nr_sequencia)
	into	nr_seq_pagador_w
	from	pls_proposta_pagador
	where	nr_seq_proposta	= nr_seq_proposta_w
	and	ie_tipo_pagador	= 'P';
end if;
		
if	(nr_seq_pagador_w is null) then
	select	max(nr_sequencia)
	into	nr_seq_pagador_w
	from	pls_proposta_pagador
	where	nr_seq_proposta	= nr_seq_proposta_w;
end if;	

open C01;
loop
fetch C01 into
	ie_acao_regra_pagador_w,
	nr_seq_classif_itens_w,
	ie_mes_vencimento_w,
	dt_dia_vencimento_w;
exit when C01%notfound;
end loop;
close C01;

open C03;
loop
fetch C03 into
	cd_pessoa_fisica_w,
	nr_seq_contrato_w,
	nr_seq_vendedor_canal_w,
	nr_seq_vendedor_pf_w,
	dt_admissao_w,
	nr_seq_vinculo_estip_w,
	cd_matricula_est_w,
	cd_cbo_w,
	nr_seq_plano_w,
	nr_seq_parentesco_w,
	nr_seq_titular_contrato_w,
	nr_seq_subestipulante_w,
	dt_contratacao_w,
	nr_seq_titular_solic_w,
	nr_seq_faixa_salarial_w,
	nr_seq_solicitacao_benef_w,
	nr_seq_motivo_inclusao_w,
	cd_carteira_ant_w;
exit when C03%notfound;
	begin
	
	if	(ie_gerar_proposta_mig_resc_w = 'S') then	
		nr_seq_segurado_ant_w := null;
		nr_contrato_ant_w := null;
	
		if	(cd_carteira_ant_w is not null) then
			begin
			select	max(b.dt_rescisao),
				b.nr_sequencia,
				a.nr_contrato				
			into	dt_rescisao_cart_ant_w,
				nr_seq_segurado_ant_w,
				nr_contrato_ant_w				
			from	pls_contrato		a,
				pls_segurado		b,
				pls_segurado_carteira	c
			where	a.nr_sequencia = b.nr_seq_contrato
			and	b.nr_sequencia = c.nr_seq_segurado
			and	c.cd_usuario_plano = cd_carteira_ant_w
			group by
				b.nr_sequencia, a.nr_contrato;
			exception
			when others then				
				dt_rescisao_cart_ant_w := null;
				nr_seq_segurado_ant_w := null;
				nr_contrato_ant_w := null;
			end;
			
			if	(dt_rescisao_cart_ant_w is not null) then
				select	obter_dias_entre_datas(dt_rescisao_cart_ant_w, dt_contratacao_w)
				into	qt_dias_rescindidos_w
				from	dual;

				if	(qt_dias_rescindidos_w <= qt_dias_rescisao_mig_w) then
					qt_benef_migrados_w := qt_benef_migrados_w + 1;
				else
					nr_seq_segurado_ant_w := null;
				end if;				
			end if;	
		end if;
	end if;	
	
	if	(nr_seq_titular_solic_w is not null) then
		select	max(nr_seq_proposta_benef)
		into	nr_seq_proposta_benef_tit_w
		from	pls_inclusao_beneficiario
		where	nr_sequencia	= nr_seq_titular_solic_w;
	else
		nr_seq_proposta_benef_tit_w	:= null;
	end if;
	
	if	(ie_acao_regra_pagador_w = 'GP') then
		--Gerar pagador para cada titular
		if	(nr_seq_titular_contrato_w is null) and (nr_seq_titular_solic_w is null) then
			select	cd_condicao_pagamento,
				cd_tipo_portador,
				cd_portador,
				nr_seq_conta_banco
			into	cd_condicao_pagamento_w,
				cd_tipo_portador_w,
				cd_portador_w,
				nr_seq_conta_banco_w
			from	pls_parametros
			where	cd_estabelecimento = cd_estabelecimento_p;
			
			select	pls_proposta_pagador_seq.nextval
			into	nr_seq_pagador_w
			from	dual;
			
			insert	into	pls_proposta_pagador
				(	nr_sequencia, nr_seq_proposta, dt_dia_vencimento,
					cd_condicao_pagamento, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, cd_pagador,
					nr_seq_forma_cobranca, cd_tipo_portador, cd_portador,
					nr_seq_conta_banco, dt_inicio_vigencia, ie_mes_vencimento,
					ie_endereco_boleto, nr_seq_classif_itens)
				values
				(	nr_seq_pagador_w, nr_seq_proposta_w, nvl(dt_dia_vencimento_w,10),
					cd_condicao_pagamento_w, sysdate, nm_usuario_p,
					sysdate, nm_usuario_p, cd_pessoa_fisica_w,
					1, cd_tipo_portador_w, cd_portador_w,
					nr_seq_conta_banco_w, dt_contratacao_w, nvl(ie_mes_vencimento_w,'A'),
					'PFR', nr_seq_classif_itens_w);
		else --Se for dependente, busca o pagador do titular
			if	(nr_seq_titular_contrato_w is not null) then
				select	max(b.cd_cgc),
					max(b.cd_pessoa_fisica)
				into	cd_cgc_pagador_w,
					cd_pf_pagador_w
				from	pls_segurado		a,
					pls_contrato_pagador	b
				where	a.nr_seq_pagador	= b.nr_sequencia
				and	a.nr_sequencia		= nr_seq_titular_contrato_w;
			
				select	max(nr_sequencia)
				into	nr_seq_pagador_w
				from	pls_proposta_pagador
				where	((cd_pagador = cd_pf_pagador_w) or (cd_pf_pagador_w is null))
				and	((cd_cgc_pagador = cd_cgc_pagador_w) or (cd_cgc_pagador_w is null));
			end if;
			
			if 	(nr_seq_titular_solic_w is not null) then
				select	max(b.nr_seq_pagador)
				into	nr_seq_pagador_w
				from 	pls_proposta_beneficiario b,
					pls_inclusao_beneficiario a
				where 	a.nr_sequencia = nr_seq_titular_solic_w
				and 	b.nr_seq_inclusao_benef = a.nr_sequencia;
			end if;
		end if;
	end if;
	
	--Obter o produto da proposta
	if	(nr_seq_plano_w is null) then
		begin
		select	nr_seq_plano,
			nr_seq_tabela
		into	nr_seq_plano_w,
			nr_seq_tabela_w
		from	pls_proposta_plano
		where	nr_sequencia = (select max(nr_sequencia) from pls_proposta_plano where nr_seq_proposta	= nr_seq_proposta_w);
		exception
		when others then	
			nr_seq_plano_w := null;
			nr_seq_tabela_w := null;
		end;

	else		
		select	max(nr_seq_tabela)
		into	nr_seq_tabela_w
		from	pls_contrato_plano
		where	nr_seq_contrato			= nr_seq_contrato_w
		and	nr_seq_plano			= nr_seq_plano_w
		and 	nvl(ie_inclusao_portal,'S') 	= 'S'
		and	ie_situacao			= 'A';
		
	end if;
	
	if	(nr_seq_parentesco_w is not null) then
		select	ie_tipo_parentesco
		into	ie_tipo_parentesco_w
		from	grau_parentesco
		where	nr_sequencia = nr_seq_parentesco_w;	
	end if;			
	
	select	pls_proposta_beneficiario_seq.nextval
	into	nr_seq_proposta_benef_w
	from	dual;
	
	insert	into	pls_proposta_beneficiario
		(nr_sequencia, nr_seq_proposta, cd_beneficiario,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
		nm_usuario_nrec, nr_seq_pagador, nr_seq_plano,
		nr_seq_titular, nr_seq_parentesco, nr_seq_tabela,
		dt_admissao, nr_seq_vinculo_estip, cd_matricula_est,
		cd_cbo, nr_seq_titular_contrato, nr_seq_subestipulante,
		nr_seq_inclusao_benef, dt_contratacao, ie_taxa_inscricao,
		ie_copiar_sca_plano, nr_seq_faixa_salarial, ie_nascido_plano,
		nr_seq_motivo_inclusao,ie_tipo_parentesco, nr_seq_beneficiario)
	values( nr_seq_proposta_benef_w, nr_seq_proposta_w, cd_pessoa_fisica_w,
		sysdate, nm_usuario_p, sysdate,
		nm_usuario_p, nr_seq_pagador_w, nr_seq_plano_w,
		nr_seq_proposta_benef_tit_w, nr_seq_parentesco_w, nr_seq_tabela_w,
		dt_admissao_w, nr_seq_vinculo_estip_w, cd_matricula_est_w,
		cd_cbo_w, nr_seq_titular_contrato_w, nr_seq_subestipulante_w,
		nr_seq_solicitacao_benef_w, dt_contratacao_w,'S',
		'S', nr_seq_faixa_salarial_w, 'N', 
		nr_seq_motivo_inclusao_w, ie_tipo_parentesco_w, nr_seq_segurado_ant_w);
		
	qt_benef_proposta_w := qt_benef_proposta_w + 1;
	
	open c04;
	loop
	fetch c04 into
		dt_anexo_w,
		ds_anexo_w,
		nr_seq_tipo_doc_w;
	exit when c04%notfound;
		begin
			insert into pls_proposta_benef_anexo
				(nr_sequencia, nr_seq_beneficiario, dt_anexo, ds_anexo,
				 dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tipo_documento
				)
			values  (pls_proposta_benef_anexo_seq.nextval, nr_seq_proposta_benef_w, dt_anexo_w, ds_anexo_w,
				 sysdate, nm_usuario_p, sysdate, nm_usuario_p, nr_seq_tipo_doc_w
				);
		end;
	end loop;
	close c04;
	
	
	open c02;
	loop
	fetch c02 into
		nr_seq_plano_sca_w;
	exit when c02%notfound;
		begin
		begin
		select	nr_seq_tabela,
			nr_seq_vendedor_canal,
			nr_seq_vendedor_pf
		into	nr_seq_tabela_sca_w,
			nr_seq_vendedor_canal_sca_w,
			nr_seq_vendedor_pf_sca_w
		from	pls_sca_regra_contrato	b,
			pls_contrato		a
		where	b.nr_seq_contrato	= a.nr_sequencia
		and	a.nr_contrato		= nr_contrato_w
		and	b.nr_seq_plano		= nr_seq_plano_sca_w
		and	dt_contratacao_w between nvl(dt_inicio_vigencia,dt_contratacao_w) and nvl(dt_fim_vigencia,dt_contratacao_w);
		exception
		when others then
			nr_seq_tabela_sca_w		:= null;
			nr_seq_vendedor_canal_sca_w	:= null;
			nr_seq_vendedor_pf_sca_w	:= null;
		end;
		
		insert into pls_sca_vinculo
			(	nr_sequencia,dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_benef_proposta, nr_seq_plano, nr_seq_tabela, nr_seq_vendedor_canal, nr_seq_vendedor_pf)
		values	(	pls_sca_vinculo_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_proposta_benef_w, nr_seq_plano_sca_w, nr_seq_tabela_sca_w, nr_seq_vendedor_canal_sca_w, nr_seq_vendedor_pf_sca_w);
		end;
	end loop;
	close c02;


	update	pls_inclusao_beneficiario
	set	ie_status		= 'A',
		nr_seq_proposta		= nr_seq_proposta_w,
		nr_seq_proposta_benef	= nr_seq_proposta_benef_w
	where	nr_sequencia		= nr_seq_solicitacao_benef_w;
	end;
end loop;
close C03;

if	(qt_benef_migrados_w > 0) then
	if	(qt_benef_migrados_w = qt_benef_proposta_w) then
		update	pls_proposta_adesao
		set	ie_tipo_proposta = 4, -- Migra��o a um contrato existente
			nr_seq_contrato_mig = nr_contrato_ant_w
		where	nr_sequencia = nr_seq_proposta_w;
	elsif	(qt_benef_migrados_w < qt_benef_proposta_w) then
		update	pls_proposta_adesao
		set	ie_tipo_proposta = 8, -- Migra��o + Ades�o a um contrato existente
			nr_seq_contrato_mig = nr_contrato_ant_w
		where	nr_sequencia = nr_seq_proposta_w;
	end if;
end if;	

commit;
ds_erro_w := null;
if	(nr_seq_proposta_web_w is null) then
	pls_consistir_proposta(nr_seq_proposta_w, nm_usuario_p, ie_consistir_sib_w, 'N', ds_erro_w);
end if;

if	(ds_erro_w is null) then
	if	(ie_gerar_contratacao_w	= 'S') then
		--Se o par�metro 28 estiver como "Sim", precisa liberar e aprovar a proposta
		--Caso os par�metros ie_liberar_proposta_w e ie_aprovar_proposta_w estejam como "S", a libera��o e aprova��o j� ser� feita na rotina pls_consistir_proposta
		if	(ie_liberar_proposta_w = 'N') then
			pls_liberar_proposta(nr_seq_proposta_w, 1, 'N', nm_usuario_p, ds_erro_w);
		end if;
		
		if	(ds_erro_w is null) then
			if	(ie_aprovar_proposta_w	= 'N') then
				pls_aprovar_proposta(nr_seq_proposta_w,'N', nm_usuario_p);
			end if;
			
			pls_proposta_gerar_contrato(nr_seq_proposta_w, nm_usuario_p, cd_estabelecimento_w);
		end if;
	end if;
end if;

ds_erro_p := ds_erro_w;

commit;

end pls_gerar_proposta_benef_multi;
/
