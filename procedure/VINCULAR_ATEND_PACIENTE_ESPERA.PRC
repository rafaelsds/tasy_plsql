create or replace
procedure vincular_atend_paciente_espera (	nr_atendimento_p		number,
						nr_seq_paciente_espera_p	number,
						ie_opcao_p			varchar2,
						nm_usuario_p			varchar2) is 

/* **************** IE_OPCAO_P ************/
/*	V - Vincular atendimento		*/
/*	D - Desvincular atendimento	*/
/* *****************************************/	
						
begin

if	(ie_opcao_p	= 'V') then
	update	paciente_espera
	set	nr_atendimento	= nr_atendimento_p
	where	nr_sequencia	= nr_seq_paciente_espera_p;
 
 elsif	(ie_opcao_p	= 'D') then
	update	paciente_espera
	set	nr_atendimento	= null
	where	nr_sequencia	= nr_seq_paciente_espera_p;
end if;

commit;

end vincular_atend_paciente_espera;
/