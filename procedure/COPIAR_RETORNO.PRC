create or replace
procedure copiar_retorno(
			nr_seq_retorno_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2,
			ie_opcao_p		number) is

/* ie_opcao_p

0	Todos os itens
1	Somente itens com valor amenor
2	Somente itens com valor glosado

*/

nr_seq_retorno_w		number(10);			
nr_seq_guia_w			number(10);
nr_seq_guia_novo_w		number(10);
nr_seq_item_glosado_w		number(10);
nr_seq_item_glosado_novo_w	number(10);
			
Cursor C01 is
	select	a.nr_sequencia
	from	convenio_retorno_item a
	where	((nvl(ie_opcao_p,0) = 0) or
		(ie_opcao_p = 1 and a.vl_amenor <> 0) or
		(ie_opcao_p = 2 and a.vl_glosado <> 0))
	and	a.nr_seq_retorno		= nr_seq_retorno_p
	order by a.nr_sequencia;
	
Cursor C02 is
	select	a.nr_sequencia
	from	convenio_retorno_glosa a
	where	a.nr_seq_ret_item	= nr_seq_guia_w	
	order by a.nr_sequencia;	
			
begin

if	(nr_seq_retorno_p is not null) then

	select	convenio_retorno_seq.nextval
	into	nr_seq_retorno_w
	from 	dual;
	
	insert into convenio_retorno (	
		nr_sequencia,           
		cd_convenio,            
		dt_retorno,             
		ie_status_retorno,      
		dt_atualizacao,         
		nm_usuario,             
		nm_usuario_retorno,     
		ds_lote_convenio,       
		dt_inicial,             
		dt_final,               
		dt_fechamento,          
		nm_usuario_fechamento,  
		ds_observacao,          
		nr_seq_protocolo,       
		dt_ref_preco,           
		dt_baixa_cr,            
		cd_estabelecimento,     
		nr_seq_prot_adic,       
		dt_recebimento,         
		dt_consistencia,        
		dt_vinculacao,          
		cd_convenio_particular,
		ie_doc_retorno,         
		ie_baixa_unica_ret,
		nr_seq_tipo,            
		nr_seq_ret_origem,      
		dt_limite_glosa,        
		dt_fim_glosa,           
		ie_tipo_glosa,          
		nr_seq_cobranca)
	select	nr_seq_retorno_w,           
		a.cd_convenio,            
		sysdate,             
		'R',      
		sysdate,         
		nm_usuario_p,             
		a.nm_usuario_retorno,     
		a.ds_lote_convenio,       
		a.dt_inicial,             
		a.dt_final,               
		a.dt_fechamento,          
		a.nm_usuario_fechamento,  
		a.ds_observacao,          
		a.nr_seq_protocolo,       
		a.dt_ref_preco,           
		a.dt_baixa_cr,            
		a.cd_estabelecimento,     
		a.nr_seq_prot_adic,       
		a.dt_recebimento,         
		a.dt_consistencia,        
		a.dt_vinculacao,          
		a.cd_convenio_particular,
		a.ie_doc_retorno,         
		a.ie_baixa_unica_ret,
		a.nr_seq_tipo,            
		a.nr_seq_ret_origem,      
		a.dt_limite_glosa,        
		a.dt_fim_glosa,           
		a.ie_tipo_glosa,          
		a.nr_seq_cobranca
	from	convenio_retorno a
	where	a.nr_sequencia		= nr_seq_retorno_p
	and	a.cd_estabelecimento	= cd_estabelecimento_p;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_guia_w;
	exit when C01%notfound;
		begin
		
		select 	convenio_retorno_item_seq.nextval
		into	nr_seq_guia_novo_w
		from 	dual;
		
		insert into convenio_retorno_item (	
			nr_sequencia,           
			nr_seq_retorno,         
			vl_pago,                
			vl_glosado,             
			dt_atualizacao,         
			nm_usuario,             
			ie_glosa,               
			nr_interno_conta,       
			cd_autorizacao,         
			ds_observacao,          
			vl_adicional,           
			nr_titulo,              
			vl_amenor,              
			vl_adequado,            
			cd_motivo_glosa,        
			ie_libera_repasse,      
			ie_analisada,           
			vl_desconto,            
			cd_autorizacao_conv,    
			cd_centro_custo_desc,   
			nr_seq_motivo_desc,     
			vl_perdas,              
			vl_guia,                
			ie_autorizacao,         
			vl_amenor_post,         
			vl_glosado_post,        
			nr_seq_ret_item_orig,   
			nr_seq_receb)
		select	nr_seq_guia_novo_w,           
			nr_seq_retorno_w,        
			a.vl_pago,                
			a.vl_glosado,             
			sysdate,         
			nm_usuario_p,             
			a.ie_glosa,               
			a.nr_interno_conta,       
			a.cd_autorizacao,         
			a.ds_observacao,          
			a.vl_adicional,           
			a.nr_titulo,              
			a.vl_amenor,              
			a.vl_adequado,            
			a.cd_motivo_glosa,        
			a.ie_libera_repasse,      
			a.ie_analisada,           
			a.vl_desconto,            
			a.cd_autorizacao_conv,    
			a.cd_centro_custo_desc,   
			a.nr_seq_motivo_desc,     
			a.vl_perdas,              
			a.vl_guia,                
			a.ie_autorizacao,         
			a.vl_amenor_post,         
			a.vl_glosado_post,        
			a.nr_seq_ret_item_orig,   
			a.nr_seq_receb
		from	convenio_retorno_item a
		where	a.nr_sequencia	= nr_seq_guia_w;
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_item_glosado_w;
		exit when C02%notfound;
			begin
			
			select 	convenio_retorno_glosa_seq.nextval
			into	nr_seq_item_glosado_novo_w
			from 	dual;
			
			insert into convenio_retorno_glosa (	
				nr_sequencia,           
				nr_seq_ret_item,        
				vl_glosa,               
				dt_atualizacao,         
				nm_usuario,             
				cd_procedimento,        
				ie_origem_proced,       
				cd_material,            
				ds_observacao,          
				ie_atualizacao,         
				cd_item_convenio,       
				cd_motivo_glosa,        
				nr_seq_conpaci_ret_hist,
				qt_glosa,               
				cd_setor_atendimento,   
				dt_execucao,            
				cd_resposta,            
				nr_seq_propaci,         
				nr_seq_matpaci,      
				vl_amaior,              
				ie_emite_conta,         
				vl_cobrado,             
				qt_cobrada,             
				nr_seq_propaci_partic,  
				nr_seq_matpaci_partic,  
				cd_setor_responsavel,   
				cd_autorizacao_compl,   
				nr_seq_partic,          
				cd_pessoa_fisica,       
				ds_complemento,         
				vl_pago_digitado,       
				vl_repasse_item)
			select	nr_seq_item_glosado_novo_w,
				nr_seq_guia_novo_w,        
				a.vl_glosa,               
				sysdate,         
				nm_usuario_p,             
				a.cd_procedimento,        
				a.ie_origem_proced,       
				a.cd_material,            
				a.ds_observacao,          
				a.ie_atualizacao,         
				a.cd_item_convenio,       
				a.cd_motivo_glosa,        
				a.nr_seq_conpaci_ret_hist,
				a.qt_glosa,               
				a.cd_setor_atendimento,   
				a.dt_execucao,            
				a.cd_resposta,            
				a.nr_seq_propaci,         
				a.nr_seq_matpaci,         
				a.vl_amaior,              
				a.ie_emite_conta,         
				a.vl_cobrado,             
				a.qt_cobrada,             
				a.nr_seq_propaci_partic,  
				a.nr_seq_matpaci_partic,  
				a.cd_setor_responsavel,   
				a.cd_autorizacao_compl,   
				a.nr_seq_partic,          
				a.cd_pessoa_fisica,       
				a.ds_complemento,         
				a.vl_pago_digitado,       
				a.vl_repasse_item
			from	convenio_retorno_glosa a
			where	a.nr_sequencia	= nr_seq_item_glosado_w;
			end;
		end loop;
		close C02;

		end;
	end loop;
	close C01;
end if;

commit;

end copiar_retorno;
/