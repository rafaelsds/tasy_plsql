create or replace
procedure inclui_sinal_vital_philips_sdn (	nr_seq_sinal_vital_p		number,
						nr_parametro_sdn_p		number,
						ds_unidade_p			varchar2,
						ds_valor_vital_p		varchar2
					) is 


ds_parametro_mdil_w	varchar2(10);
					
begin

if	(nr_parametro_sdn_p = 40) then
	ds_parametro_mdil_w := 'HR';
elsif	(nr_parametro_sdn_p = 89) then
	ds_parametro_mdil_w := 'NBPs';
elsif	(nr_parametro_sdn_p = 90) then
	ds_parametro_mdil_w := 'NBPd';
elsif	(nr_parametro_sdn_p = 91) then	
	ds_parametro_mdil_w := 'NBPm';
elsif	(nr_parametro_sdn_p = 161) then
	ds_parametro_mdil_w := 'ARTs';
elsif	(nr_parametro_sdn_p = 162) then	
	ds_parametro_mdil_w := 'ARTd';
elsif	(nr_parametro_sdn_p = 163) then	
	ds_parametro_mdil_w := 'ARTm';
elsif	(nr_parametro_sdn_p = 76) then	
	ds_parametro_mdil_w := 'PAP';
elsif	(nr_parametro_sdn_p = 77) then	
	ds_parametro_mdil_w := 'PAPs';
elsif	(nr_parametro_sdn_p = 78) then	
	ds_parametro_mdil_w := 'PAPd';
elsif	(nr_parametro_sdn_p = 79) then	
	ds_parametro_mdil_w := 'PAPm';
elsif	(nr_parametro_sdn_p = 128) then	
	ds_parametro_mdil_w := 'PAWP';
elsif	(nr_parametro_sdn_p = 72) then	
	ds_parametro_mdil_w := 'LAP';
elsif	(nr_parametro_sdn_p = 68) then	
	ds_parametro_mdil_w := 'CVP';
elsif	(nr_parametro_sdn_p = 456) then	
	ds_parametro_mdil_w := 'VO2';
elsif	(nr_parametro_sdn_p = 124) then	
	ds_parametro_mdil_w := 'C.O.';
elsif	(nr_parametro_sdn_p = 364) then	
	ds_parametro_mdil_w := 'PVR';
elsif	(nr_parametro_sdn_p = 400) then	
	ds_parametro_mdil_w := 'SVR';
elsif	(nr_parametro_sdn_p = 192) then	
	ds_parametro_mdil_w := 'SvO2';
elsif	(nr_parametro_sdn_p = 256) then	
	ds_parametro_mdil_w := 'AaDO2';
elsif	(nr_parametro_sdn_p = 532) then	
	ds_parametro_mdil_w := 'TCore';
elsif	(nr_parametro_sdn_p = 528) then	
	ds_parametro_mdil_w := 'Tesoph';
elsif	(nr_parametro_sdn_p = 516) then	
	ds_parametro_mdil_w := 'Tskin';
elsif	(nr_parametro_sdn_p = 396) then	
	ds_parametro_mdil_w := 'SV';
elsif	(nr_parametro_sdn_p = 316) then	
	ds_parametro_mdil_w := 'LCW';
elsif	(nr_parametro_sdn_p = 372) then	
	ds_parametro_mdil_w := 'RCW';
elsif	(nr_parametro_sdn_p = 188) then	
	ds_parametro_mdil_w := 'SpO2';
elsif	(nr_parametro_sdn_p = 92) then	
	ds_parametro_mdil_w := 'Resp';
elsif	(nr_parametro_sdn_p = 1160) then	
	ds_parametro_mdil_w := 'VC';
elsif	(nr_parametro_sdn_p = 3724) then	
	ds_parametro_mdil_w := 'Cstat';
elsif	(nr_parametro_sdn_p = 112) then	
	ds_parametro_mdil_w := 'etCO2';
elsif	(nr_parametro_sdn_p = 4780) then	
	ds_parametro_mdil_w := 'PIF';
elsif	(nr_parametro_sdn_p = 1176) then	
	ds_parametro_mdil_w := 'Pplat';
elsif	(nr_parametro_sdn_p = 204) then	
	ds_parametro_mdil_w := 'AWP';
elsif	(nr_parametro_sdn_p = 1780) then	
	ds_parametro_mdil_w := 'iPEEP';
elsif	(nr_parametro_sdn_p = 208) then	
	ds_parametro_mdil_w := 'PIP';
elsif	(nr_parametro_sdn_p = 1172) then	
	ds_parametro_mdil_w := 'MnAwP';
elsif	(nr_parametro_sdn_p = 1180) then	
	ds_parametro_mdil_w := 'I:E1:';
elsif	(nr_parametro_sdn_p = 1144) then	
	ds_parametro_mdil_w := 'Raw';
elsif	(nr_parametro_sdn_p = 212) then	
	ds_parametro_mdil_w := 'TV';
elsif	(nr_parametro_sdn_p = 324) then	
	ds_parametro_mdil_w := 'MINVOL';
elsif	(nr_parametro_sdn_p = 348) then	
	ds_parametro_mdil_w := 'PEEP';
elsif	(nr_parametro_sdn_p = 80) then	
	ds_parametro_mdil_w := 'ICP';
elsif	(nr_parametro_sdn_p = 252) then	
	ds_parametro_mdil_w := 'CPP';
elsif	(nr_parametro_sdn_p = 1060) then	
	ds_parametro_mdil_w := 'HC';
elsif	(nr_parametro_sdn_p = 148) then	
	ds_parametro_mdil_w := 'FIO2';
end if;	

inclui_sinal_vital_philips( nr_seq_sinal_vital_p, ds_parametro_mdil_w, ds_unidade_p, ds_valor_vital_p );

end inclui_sinal_vital_philips_sdn;
/