create or replace
procedure gerar_nom_rc_plano_tratamento(	nr_seq_cabecalho_p	number,
									nm_usuario_p		varchar2) is

nr_atendimento_w				atendimento_paciente.nr_atendimento%type;
nr_seq_episodio_w				episodio_paciente.nr_sequencia%type;
ds_tratamento_w					varchar(32000) := null;

ds_plano_tratamento_w			nom_rc_plano_tratamento.ds_plano_tratamento%type;

Cursor c_plano_tratamento is
select 	obter_receita_avaliacao_nom(a.nr_atendimento) ds_plano_tratamento
from 	atendimento_paciente a
where 	a.nr_atendimento in (select	x.nr_atendimento
							from	nom_rc_cabecalho x
							where	x.nr_sequencia = nr_seq_cabecalho_p
							and		x.nr_atendimento is not null
							union
							select	y.nr_atendimento
							from	atendimento_paciente y,
									nom_rc_cabecalho x
							where	x.nr_seq_episodio = y.nr_seq_episodio
							and		x.nr_sequencia = nr_seq_cabecalho_p);


begin

delete 	from nom_rc_plano_tratamento
where	nr_seq_cabecalho = nr_seq_cabecalho_p;

	for	r_c_plano_tratamento in c_plano_tratamento loop

		if (r_c_plano_tratamento.ds_plano_tratamento is not null) then
		
			ds_plano_tratamento_w	:= r_c_plano_tratamento.ds_plano_tratamento || chr(13) || chr(10);
				
			if 	(instr(ds_plano_tratamento_w,'\rtf1') > 0) or
				(instr(ds_plano_tratamento_w,'<html xmlns="http://www.w3.org/1999/xhtml">') > 0) then
				ds_plano_tratamento_w	:= remove_html_from_text(ds_plano_tratamento_w);
			end if;
				
			if(ds_plano_tratamento_w is not null) and (length(nvl(ds_tratamento_w,'') || nvl(ds_plano_tratamento_w,'')) < 32000)then
				ds_tratamento_w := ds_tratamento_w || ds_plano_tratamento_w;
			end if;
			
		end if;
		
	end loop;

	insert into nom_rc_plano_tratamento
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_plano_tratamento,
		nr_seq_cabecalho)
	values
		(nom_rc_plano_tratamento_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_tratamento_w,
		nr_seq_cabecalho_p);

end gerar_nom_rc_plano_tratamento;
/