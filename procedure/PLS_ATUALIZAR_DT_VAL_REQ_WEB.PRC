create or replace
procedure pls_atualizar_dt_val_req_web
			(	nr_seq_requisicao_p	Number,
				dt_validade_p		Varchar2,
				nm_usuario_p		Varchar2) is 


begin
	if	(nr_seq_requisicao_p is not null) then
		update  pls_requisicao
		set	dt_validade_senha = to_date(dt_validade_p,'dd/mm/yyyy'),
			nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate
		where	nr_sequencia = nr_seq_requisicao_p;
	end if;
commit;

end pls_atualizar_dt_val_req_web;
/
