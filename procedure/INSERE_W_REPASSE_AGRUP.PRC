create or replace
procedure insere_w_repasse_agrup(
			ie_proc_mat_p		varchar2,
			nr_seq_p		varchar2, 
			nm_usuario_p		varchar2) is 

begin

if	(ie_proc_mat_p	= 'P') then

	insert	into w_repasse_agrup
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_proc_repasse,
		nr_seq_mat_repasse)
	values	(w_repasse_agrup_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_p,
		null);
end if;

if	(ie_proc_mat_p	= 'M') then

	insert	into w_repasse_agrup
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_proc_repasse,
		nr_seq_mat_repasse)
	values	(w_repasse_agrup_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		null,
		nr_seq_p);
end if;


commit;

end insere_w_repasse_agrup;
/