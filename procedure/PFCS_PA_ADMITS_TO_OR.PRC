create or replace procedure PFCS_PA_ADMITS_TO_OR(
    cd_establishment_p  number,
    nm_user_p          	varchar2) is

nr_seq_indicator_w          pfcs_panel.nr_seq_indicator%type := 150;
pfcs_panel_detail_seq_w     pfcs_panel_detail.nr_sequencia%type;
pfcs_panel_seq_w            pfcs_panel.nr_sequencia%type;


qt_register_w    number(20) := 0;
qt_total_time_w  number(20) := 0;

cursor c01 is
select enc.id_encounter nr_encounter,
    pfcs_get_human_name(pat.nr_sequencia, 'Patient') nm_patient,
    pat.patient_id id_patient,
    pat.birthdate dt_birthdate,
    round(months_between(nvl(pat.deceased_date, sysdate), pat.birthdate)) qt_patient_age,
    pat.gender ds_gender,
    (sec.ds_setor_atendimento || '-' || uni.cd_unidade_basica || '-' || uni.cd_unidade_compl) ds_location,
    (sec_req.ds_setor_atendimento || '-' || req.cd_unidade_basica || '-' || req.cd_unidade_compl) ds_request,
    enc.period_start dt_period_start,
    pfcs_get_patient_diagnosis(enc.nr_sequencia) ds_problem,
    nvl(ser.dt_authored_on, ser.dt_atualizacao) dt_request,
    pfcs_get_human_name(pfcs_get_practitioner_seq(enc.nr_sequencia, '405279007'), 'Practitioner') nm_practioner,
    pfcs_get_edi_score(enc.nr_sequencia) qt_trs,
    pfcs_obs_contributor_pck.get_edi_vital_warnings(enc.nr_sequencia) ds_edi_vitals_warn,
    pfcs_obs_contributor_pck.get_edi_contributors(enc.nr_sequencia) ds_edi_contrb,
    pfcs_get_code_status(pat.nr_sequencia, enc.nr_sequencia, 'S') cd_code_status,
    pfcs_get_special_requests(enc.nr_sequencia, uni.nr_seq_location,'E') ds_special_requests,
	nvl(pfcs_get_checklist(enc.nr_sequencia, pat.nr_sequencia, 'MPL'),'') ds_care_status,
    nvl(pfcs_get_checklist(enc.nr_sequencia, pat.nr_sequencia, 'TOOLTIP'),'') ds_checklist,
    pfcs_get_frequent_flyer(enc.nr_sequencia) ie_frequent_flyer,
    pfcs_get_lace_plus(enc.nr_sequencia, enc.nr_seq_organization) ds_readmission_risk,
    pfcs_obs_contributor_pck.get_laceplus(enc.nr_sequencia) ds_readm_risk_contrb,
	pfcs_obs_contributor_pck.get_freqflyer_visits(enc.nr_sequencia) ds_recur_pat_adm_data,
	pfcs_obs_contributor_pck.get_freqflyer_comorbidities(enc.nr_sequencia) ds_recur_pat_comorbd,
	pfcs_obs_contributor_pck.get_freqflyer_visit_reason(enc.nr_sequencia) ds_recur_pat_reasons,
    uni.ie_status_unidade cd_status,
    sec.ds_setor_atendimento ds_department
from pfcs_service_request ser,
    pfcs_encounter enc,
    pfcs_patient pat,
    unidade_atendimento uni,
    setor_atendimento sec,
    unidade_atendimento req,
    setor_atendimento sec_req
where enc.nr_seq_patient = pat.nr_sequencia
    and ser.nr_seq_encounter = enc.nr_sequencia
    and ser.nr_seq_location <> uni.nr_seq_location
    and ser.nr_seq_location = req.nr_seq_location
    and uni.nr_seq_location = pfcs_get_pat_location(pat.nr_sequencia, enc.nr_sequencia)
    and sec.cd_setor_atendimento = uni.cd_setor_atendimento
    and sec_req.cd_setor_atendimento = req.cd_setor_atendimento
    and sec_req.cd_classif_setor in ('3','4','9','11','12')
    and sec.cd_classif_setor = '2'
    and enc.period_start is not null
    and enc.period_end is null
    and ser.cd_service in ('E0402')
    and ser.si_status = 'ACTIVE'
    and ser.si_intent = 'PLAN'
    and sec.cd_estabelecimento = cd_establishment_p;

BEGIN  
for c01_w in c01 loop
    qt_register_w := qt_register_w + 1;
    
    select  pfcs_panel_detail_seq.nextval
    into  pfcs_panel_detail_seq_w
    from  dual;
    
    insert into pfcs_panel_detail(
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        ie_situation,
        nr_seq_indicator,
        nr_seq_operational_level)
    values (
        pfcs_panel_detail_seq_w,
        nm_user_p,
        sysdate,
        nm_user_p,
        sysdate,
        'T',
        nr_seq_indicator_w,
        cd_establishment_p);
    
    insert into pfcs_detail_bed(
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        nr_seq_detail,
        ds_location,
        ds_request_unit,
        ds_department,
        cd_status)
    values (
        pfcs_detail_bed_seq.nextval,
        nm_user_p,
        sysdate,
        nm_user_p,
        sysdate,
        pfcs_panel_detail_seq_w,
        c01_w.ds_location,
        c01_w.ds_request,
        c01_w.ds_department,
        c01_w.cd_status);
    
    insert into pfcs_detail_patient(
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        nr_seq_detail,
        nr_encounter_varchar,
        dt_entrance,
        id_patient,
        nm_patient,
        ds_gender,
        dt_birthdate,
        ds_age_range,
        ds_dnr_status,
        ds_special_request,
        ds_physician,
        ds_symptoms,
        dt_request,
        qt_edi_score,
        ie_freq_flyer,
        ds_care_status,
        ds_checklist,
        ds_readmission_risk,
        ds_readm_risk_contrb,
        ds_rec_pat_adm_data,
        ds_rec_pat_comorbd,
        ds_rec_pat_reasons,
		ds_edi_vitals_warn,
        ds_edi_contrb)
    values (
        pfcs_detail_patient_seq.nextval,
        nm_user_p,
        sysdate,
        nm_user_p,
        sysdate,
        pfcs_panel_detail_seq_w,
        c01_w.nr_encounter,
        c01_w.dt_period_start,
        c01_w.id_patient,
        c01_w.nm_patient,
        c01_w.ds_gender,
        c01_w.dt_birthdate,
        c01_w.qt_patient_age,
        c01_w.cd_code_status,
        c01_w.ds_special_requests,
        c01_w.nm_practioner,
        c01_w.ds_problem,
        c01_w.dt_request,
        c01_w.qt_trs,
        c01_w.ie_frequent_flyer,
        c01_w.ds_care_status,
        c01_w.ds_checklist,
        c01_w.ds_readmission_risk,
        c01_w.ds_readm_risk_contrb,
        c01_w.ds_recur_pat_adm_data,
        c01_w.ds_recur_pat_comorbd,
        c01_w.ds_recur_pat_reasons,
		c01_w.ds_edi_vitals_warn,
        c01_w.ds_edi_contrb);
end loop;


pfcs_pck.pfcs_generate_results(
    vl_indicator_p              => qt_register_w,
    nr_seq_indicator_p          => nr_seq_indicator_w,
    nr_seq_operational_level_p  => cd_establishment_p,
    nm_usuario_p                => nm_user_p,
    nr_seq_panel_p              => pfcs_panel_seq_w);

pfcs_pck.pfcs_update_detail(
    nr_seq_indicator_p          => nr_seq_indicator_w,
    nr_seq_panel_p              => pfcs_panel_seq_w,
    nr_seq_operational_level_p  => cd_establishment_p,
    nm_usuario_p                => nm_user_p);

pfcs_pck.pfcs_activate_records(
    nr_seq_indicator_p          => nr_seq_indicator_w,
    nr_seq_operational_level_p  => cd_establishment_p,
    nm_usuario_p                => nm_user_p);

END PFCS_PA_ADMITS_TO_OR;
/
