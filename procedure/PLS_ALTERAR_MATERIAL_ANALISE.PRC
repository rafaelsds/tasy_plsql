create or replace
procedure pls_alterar_material_analise
			(	nr_sequencia_p			number,
				nr_seq_analise_p		number,
				ie_cobranca_prevista_p		varchar2,
				nr_nota_fiscal_p		number,
				dt_emissao_nf_p			varchar2,
				dt_recebimento_nf_p		varchar2,
				dt_atendimento_p		varchar2,
				nr_seq_grupo_p			number,
				nr_seq_prest_fornec_p		number,
				cd_estabelecimento_p		number,
				nr_seq_setor_atend_p		number,
				vl_material_imp_p		number,
				qt_material_imp_p		number,
				dt_inicio_atend_p		varchar2,
				nr_seq_material_p		number,
				nm_usuario_p			varchar2,
				ds_justificativa_p		pls_conta_mat.ds_justificativa%type,
				ds_especif_material_p		pls_conta_mat.ds_especif_material%type,
				cd_ref_fabricante_p		pls_conta_mat.cd_ref_fabricante%type default null,
				ds_aut_funcionamento_p		pls_conta_mat.ds_aut_funcionamento%type default null,
				nr_registro_anvisa_p		pls_conta_mat.nr_registro_anvisa%type default null,
				cd_unidade_medida_p		pls_conta_mat.cd_unidade_medida%type default null,
				det_reg_anvisa_p		pls_conta_mat.det_reg_anvisa%type default null
				) is
				


ds_observacao_w			varchar2(4000);
ie_parametro_w			varchar2(2);
ie_origem_conta_w		varchar2(2);
ie_valor_informado_w		varchar2(1);
ie_vl_apresentado_sistema_w	varchar2(1);
ie_preco_w			varchar2(5);
ie_cobranca_prevista_w		varchar2(10);
vl_material_imp_w		number(15,2);
qt_material_imp_w		number(12,4);
nr_nota_fiscal_w		number(10);
nr_seq_conta_w			number(10);
nr_identificador_w		number(10);
nr_seq_material_w		number(10);
nr_seq_prest_fornec_w		number(10);
nr_seq_setor_atend_w		number(10);
dt_recebimento_nf_ww		date;
dt_emissao_nf_ww		date;
dt_recebimento_nf_w		date;
dt_emissao_nf_w			date;
dt_atendimento_w		date;
dt_atendimento_ww		date;
dt_inicio_atend_w		date;
ie_cobranca_prevista_inf_w	pls_conta_mat.ie_cobranca_prevista_inf%type;
cd_ref_fabricante_w		pls_conta_mat.cd_ref_fabricante%type;
ds_aut_funcionamento_w          pls_conta_mat.ds_aut_funcionamento%type;
nr_registro_anvisa_w            pls_conta_mat.nr_registro_anvisa%type;
cd_unidade_medida_w             pls_conta_mat.cd_unidade_medida%type;

begin
if	(dt_recebimento_nf_p = '  /  /    ') or
	(dt_recebimento_nf_p is null) then
	dt_recebimento_nf_w := null;
else
	begin
	dt_recebimento_nf_w	:= to_date(dt_recebimento_nf_p);
	exception
	when others then
		dt_recebimento_nf_w	:= null;
	end;
end if;

if	(dt_inicio_atend_p = '  :  :  ') or
	(dt_inicio_atend_p is null) then
	dt_inicio_atend_w := null;
else
	dt_inicio_atend_w := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' ||nvl(dt_inicio_atend_p, '00:00:01'),'dd/mm/yyyy hh24:mi:ss');
end if;

if	(dt_emissao_nf_p = '  /  /    ') or
	(dt_emissao_nf_p is null) then
	dt_emissao_nf_w	:= null;
else
	begin
	dt_emissao_nf_w	:= to_date(dt_emissao_nf_p);
	exception
	when others then
		dt_emissao_nf_w	:= null;
	end;
end if;

if	(dt_atendimento_p = '  /  /    ') or 
	(dt_atendimento_p is null) then
	dt_atendimento_w	:= null;
else
	begin
	dt_atendimento_w	:= to_date(dt_atendimento_p);
	exception
	when others then
		dt_atendimento_w := null;
	end;
end if;

select	a.dt_recebimento_nf,
	a.nr_nota_fiscal,
	a.dt_emissao_nf,
	a.nr_seq_conta,
	a.nr_seq_material,
	a.dt_atendimento,
	a.nr_seq_prest_fornec,
	a.nr_seq_setor_atend,
	a.vl_material_imp,
	a.qt_material_imp,
	a.ie_valor_informado,
	a.ie_vl_apresentado_sistema,
	nvl(a.ie_cobranca_previa_servico,'N'),
	c.ie_preco,
	a.cd_ref_fabricante,
	a.ds_aut_funcionamento,
	a.nr_registro_anvisa,
	a.cd_unidade_medida
into	dt_recebimento_nf_ww,
	nr_nota_fiscal_w,
	dt_emissao_nf_ww,
	nr_seq_conta_w,
	nr_seq_material_w,
	dt_atendimento_ww,
	nr_seq_prest_fornec_w,
	nr_seq_setor_atend_w,
	vl_material_imp_w,
	qt_material_imp_w,
	ie_valor_informado_w,
	ie_vl_apresentado_sistema_w,
	ie_cobranca_prevista_w,
	ie_preco_w,
	cd_ref_fabricante_w,
	ds_aut_funcionamento_w,
	nr_registro_anvisa_w,
	cd_unidade_medida_w
from	pls_conta_mat a,
	pls_plano	c,
	pls_segurado	b,
	pls_conta	d
where	d.nr_seq_segurado	= b.nr_sequencia(+)
and	b.nr_seq_plano		= c.nr_sequencia(+)
and	d.nr_sequencia		= a.nr_seq_conta
and	a.nr_sequencia		= nr_sequencia_p;

select	ie_origem_conta
into	ie_origem_conta_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_w;

if	(ie_preco_w in ('2','3')) and
	(nvl(ie_cobranca_prevista_p,'N') = 'S') then
	--(-20011,'Nao e permitido gerar cobranca prevista para beneficiario com produto Pos-estabelecido! #@#@');	
	wheb_mensagem_pck.exibir_mensagem_abort(191456);
end if;

if	(ie_origem_conta_w in ('A','E')) then
	if	(nvl(vl_material_imp_w,0) <> nvl(vl_material_imp_p,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(191454); /*Nao e permitico alterar o valor apresentado de arquivo importado*/
	end if;

	if	(nvl(qt_material_imp_w,0) <> nvl(qt_material_imp_p,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(191455); /*Nao e permitico alterar a quantidade apresentado de arquivo importado*/
	end if;
end if;

if	(nvl(vl_material_imp_w,0) <> nvl(vl_material_imp_p,0))or
	(nvl(qt_material_imp_w,0) <> nvl(qt_material_imp_p,0)) then
	ie_valor_informado_w	:= 'S';
end if;
if	(nvl(vl_material_imp_w,0) <> nvl(vl_material_imp_p,0)) then
	ie_vl_apresentado_sistema_w	:= 'N';
end if;

if	(nvl(ie_cobranca_prevista_p,'X') <> nvl(ie_cobranca_prevista_w,'X')) then
	ie_cobranca_prevista_inf_w := 'S';
end if;

update	pls_conta_mat
set	dt_recebimento_nf		= dt_recebimento_nf_w,
	dt_emissao_nf			= dt_emissao_nf_w,
	nr_nota_fiscal			= nr_nota_fiscal_p,
	ie_cobranca_previa_servico	= nvl(ie_cobranca_prevista_p,'N'),
	ie_cobranca_prevista_inf	=ie_cobranca_prevista_inf_w ,
	dt_atendimento			= to_date(to_char(dt_atendimento_w,'dd/mm/yyyy') || ' ' ||nvl(dt_inicio_atend_p, '00:00:01'),'dd/mm/yyyy hh24:mi:ss'),
	nr_seq_prest_fornec		= nr_seq_prest_fornec_p,
	nr_seq_setor_atend		= nr_seq_setor_atend_p,
	vl_material_imp			= vl_material_imp_p,
	qt_material_imp			= qt_material_imp_p,
	ie_valor_informado		= ie_valor_informado_w,
	ie_vl_apresentado_sistema	= ie_vl_apresentado_sistema_w,
	dt_inicio_atend			= dt_inicio_atend_w,
	nr_seq_material			= nr_seq_material_p,
	cd_ref_fabricante		= cd_ref_fabricante_p,	
	ds_aut_funcionamento		= ds_aut_funcionamento_p,
	nr_registro_anvisa  		= nr_registro_anvisa_p, 
	cd_unidade_medida   		= cd_unidade_medida_p,
	det_reg_anvisa				= det_reg_anvisa_p,	
	ie_cobranca_prevista		= nvl(ie_cobranca_prevista_p,'N'),
	ds_justificativa		= ds_justificativa_p,
	ds_especif_material		= ds_especif_material_p
where	nr_sequencia 			= nr_sequencia_p;


if	(nvl(vl_material_imp_w,0) <> nvl(vl_material_imp_p,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Valor apresentado: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||vl_material_imp_w||' - Modificada: '||vl_material_imp_p||chr(13)||chr(10);
end if;

if	(nvl(qt_material_imp_w,0) <> nvl(qt_material_imp_p,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Quantidade apresentada: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||qt_material_imp_w||' - Modificada: '||qt_material_imp_p||chr(13)||chr(10);
end if;
	
if	(nvl(to_char(dt_recebimento_nf_w),'X') <> nvl(to_char(dt_recebimento_nf_ww),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Dt. recebimento NF: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_recebimento_nf_ww, 'dd/mm/yyyy')||' - Modificada: '||to_char(dt_recebimento_nf_w, 'dd/mm/yyyy')||chr(13)||chr(10);
end if;

if	(nvl(to_char(dt_emissao_nf_w),'X') <> nvl(to_char(dt_emissao_nf_ww),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Dt. emissao NF: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_emissao_nf_ww, 'dd/mm/yyyy')||' - Modificada: '||to_char(dt_emissao_nf_w, 'dd/mm/yyyy')||chr(13)||chr(10);
end if;

if	(nvl(to_char(dt_atendimento_w),'X') <> nvl(to_char(dt_atendimento_ww),'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Dt. material: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||to_char(dt_atendimento_ww, 'dd/mm/yyyy')||' - Modificada: '||to_char(dt_atendimento_w, 'dd/mm/yyyy')||chr(13)||chr(10);
end if;

if	(nvl(nr_nota_fiscal_p,0) <> nvl(nr_nota_fiscal_w,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Nota fiscal: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||nr_nota_fiscal_w||' - Modificada: '||nr_nota_fiscal_p||chr(13)||chr(10);
end if;

if	(nvl(nr_seq_prest_fornec_p,0) <> nvl(nr_seq_prest_fornec_w,0)) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Fornecedor: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||pls_obter_cod_prestador(nr_seq_prest_fornec_w, null)||' - '||pls_obter_dados_prestador(nr_seq_prest_fornec_w,'N')||' - Modificada: '||pls_obter_cod_prestador(nr_seq_prest_fornec_p, null)||' - '||pls_obter_dados_prestador(nr_seq_prest_fornec_p,'N')||chr(13)||chr(10);
end if;

if	(nvl(nr_seq_setor_atend_w,0) <> nvl(nr_seq_setor_atend_p,0)) then	
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Setor atendimento: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||pls_obter_dados_setor_atend(nr_seq_setor_atend_w,'DS')||' - Modificada: '||pls_obter_dados_setor_atend(nr_seq_setor_atend_p,'DS')||chr(13)||chr(10);
end if;

if	(nvl(nr_seq_material_w, 0) <> nvl(nr_seq_material_p, 0)) then	
	ds_observacao_w :=	ds_observacao_w || chr(13) || chr(10) ||
				'Alterado codigo do item: ' || chr(13) || chr(10) ||
				'Codigo: ' || chr(13) || chr(10) ||
				chr(9) || 'Anterior: ' || pls_obter_seq_codigo_material(nr_seq_material_w, null) || ' - Modificado: ' || 
				pls_obter_seq_codigo_material(nr_seq_material_p, null) || chr(13) || chr(10) ||
				'Sequencia: ' || chr(13) || chr(10) ||
				chr(9) || 'Anterior: ' || nr_seq_material_w || ' - Modificado: ' || nr_seq_material_p || chr(13) || chr(10) ||				
				'Material: '|| chr(13) || chr(10) ||
				chr(9) || 'Anterior: ' || pls_obter_desc_material(nr_seq_material_w) || ' - Modificado: ' || 
				pls_obter_desc_material(nr_seq_material_p) || chr(13) || chr(10);
end if;

if	(nvl(ie_cobranca_prevista_p,'X') <> nvl(ie_cobranca_prevista_w,'X')) then
	ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
				'Cobranca prevista: '||chr(13)||chr(10)||
				chr(9)||'Anterior: '||nvl(ie_cobranca_prevista_w,'N')||' - Modificada: '||ie_cobranca_prevista_p||chr(13)||chr(10);
end if;

if	(nvl(ds_observacao_w,'X') <> 'X') then
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_p, 15, nr_sequencia_p, 'M', null,
				null,  'Modificado pelo auditor ' || obter_nome_usuario(nm_usuario_p) || '.' || chr(13) || chr(10) ||
				'Item '|| pls_obter_desc_material(nr_seq_material_w)||'.'|| chr(13) || chr(10) || ds_observacao_w, 
				nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);
else
	pls_inserir_hist_analise(nr_seq_conta_w, nr_seq_analise_p, 15, nr_sequencia_p, 'M', null,
				null,  'Modificado pelo auditor ' || obter_nome_usuario(nm_usuario_p) || '.' || chr(13) || chr(10) ||
				'Item '|| pls_obter_desc_material(nr_seq_material_w)||'.'|| chr(13) || chr(10) || 
				 'Modificar informacoes do item executada, porem, Nenhuma informacao foi alterada', 
				nr_seq_grupo_p, nm_usuario_p, cd_estabelecimento_p);

end if;


--pls_consistir_analise(nr_seq_analise_p, nr_seq_grupo_p, cd_estabelecimento_p, nm_usuario_p);

commit;

end pls_alterar_material_analise;
/
