create or replace
PROCEDURE dcx_excluir_imagem_laudo ( 	
                    nr_seq_laudo_p		NUMBER,
					ds_imagem_p   		VARCHAR2
					) IS
BEGIN

DELETE FROM
  laudo_paciente_imagem
WHERE 
  nr_sequencia = nr_seq_laudo_p 
  AND ds_imagem = ds_imagem_p;

END dcx_excluir_imagem_laudo;
/
