create or replace
procedure gerar_evolucao_alteracao_item (	nr_prescricao_p		in 	number,
						nm_usuario_p		in 	varchar2,
						cd_estabelecimento_p	in	number,
						cd_item_p 			in number,
						cd_evolucao_p		out	number,
						ds_erro_p		out	varchar2)
						is


cd_material_origem_w		number(6);
cd_material_interv_w		number(6);
ie_via_aplicacao_origem_w	varchar2(30);
ie_via_aplicacao_interv_w	varchar2(30);
ds_via_aplicacao_interv_w	varchar2(255);
ds_unidade_medida_interv_w	varchar2(255);
cd_unidade_medida_origem_w	varchar2(30);
cd_unidade_medida_interv_w	varchar2(30);
qt_dose_origem_w			number(15,3);
qt_dose_interv_w			number(15,3);
ie_se_necessario_origem_w	varchar2(30);
ie_se_necessario_interv_w	varchar2(30);
ie_acm_origem_w				varchar2(30);
ie_acm_interv_w				varchar2(30);
ie_urgencia_origem_w		varchar2(30);
ie_urgencia_interv_w		varchar2(30);
cd_intervalo_origem_w		varchar2(30);
cd_intervalo_interv_w		varchar2(30);
hr_prim_horario_origem_w	varchar2(30);
hr_prim_horario_interv_w	varchar2(30);
ds_horarios_origem_w		varchar2(2000);
ds_horarios_interv_w		varchar2(2000);
ie_medicacao_paciente_origem_w		varchar2(30);
ie_medicacao_paciente_interv_w		varchar2(30);
ds_evolucao_w				long:= null;
ie_tipo_evolucao_w			varchar2(3);
nr_atendimento_w			number(15);
cd_medico_w					varchar2(10);
cd_evolucao_w				number:=0;
ie_gerar_titulo_w			boolean;
ds_intervalo_interv_w		varchar2(255);
ds_intervalo_origem_w		varchar2(255);
cd_pessoa_fisica_w			varchar2(10);
VarIeEvolucaoClinica_w		varchar2(3);
ds_erro_w					varchar2(255);
cabecalho_w					varchar2(2000);
cabecalho_w2				varchar2(2000);
ie_primeira_vez_w			boolean:= True;
ie_modificar_origem_w		varchar2(1) := 'N';
nr_prescricao_origem_w		varchar2(30);
cd_funcao_origem_w		prescr_medica.cd_funcao_origem%type;

cursor	c01 is
	select	a.cd_material,
		b.cd_material,
		nvl(a.ie_via_aplicacao,wheb_mensagem_pck.get_texto(277408)),
		nvl(b.ie_via_aplicacao,wheb_mensagem_pck.get_texto(277408)),
		nvl(a.cd_unidade_medida_dose,wheb_mensagem_pck.get_texto(277408)),
		nvl(b.cd_unidade_medida_dose,wheb_mensagem_pck.get_texto(277408)),
		nvl(a.qt_dose,0),
		nvl(b.qt_dose,0),
		nvl(a.ie_se_necessario,'N'),
		nvl(b.ie_se_necessario,'N'),
		nvl(a.ie_acm,'N'),
		nvl(b.ie_acm,'N'),
		nvl(a.ie_urgencia,'N'),
		nvl(b.ie_urgencia,'N'),
		nvl(a.cd_intervalo,9999999),
		nvl(b.cd_intervalo,9999999),
		nvl(a.hr_prim_horario,wheb_mensagem_pck.get_texto(277408)),
		nvl(b.hr_prim_horario,wheb_mensagem_pck.get_texto(277408)),
		nvl(a.ds_horarios,wheb_mensagem_pck.get_texto(277408)),
		nvl(b.ds_horarios,wheb_mensagem_pck.get_texto(277408)),
		nvl(a.ie_medicacao_paciente,'N'),
		nvl(b.ie_medicacao_paciente,'N'),
		c.nr_atendimento,
		c.cd_medico,
		c.cd_pessoa_fisica
	from	prescr_material a,
		prescr_material b,
		prescr_medica c
	where  	a.nr_sequencia	= b.nr_seq_interf_farm
	and	a.nr_prescricao	= c.nr_prescr_interf_farm
	and	b.nr_prescricao = c.nr_prescricao
	and	a.ie_agrupador	= b.ie_agrupador
	and	a.ie_agrupador	in (1,3,7,9)
	and	c.nr_prescricao	= nr_prescricao_p
	and	c.dt_liberacao is not null;

cursor	c02 is
	SELECT	a.cd_material,
		substr(NVL(Obter_Desc_via(a.ie_via_aplicacao),wheb_mensagem_pck.get_texto(277408)),1,255),
		substr(NVL(Obter_Desc_Unid_Med(a.cd_unidade_medida_dose),wheb_mensagem_pck.get_texto(277408)),1,255),
		NVL(a.qt_dose,0),
		NVL(a.ie_se_necessario,'N'),
		NVL(a.ie_acm,'N'),
		NVL(a.ie_urgencia,'N'),
		NVL(a.cd_intervalo,9999999),
		NVL(a.hr_prim_horario,wheb_mensagem_pck.get_texto(277408)),
		NVL(a.ds_horarios,wheb_mensagem_pck.get_texto(277408)),
		NVL(a.ie_medicacao_paciente,'N'),
		c.nr_atendimento,
		c.cd_medico,
		c.cd_pessoa_fisica
	FROM	prescr_material a,
		prescr_medica c
	WHERE	a.nr_prescricao	= c.nr_prescricao
	AND	c.nr_prescricao	= nr_prescricao_p
	AND	c.dt_liberacao IS NOT NULL
	AND	a.ie_agrupador  = 1
	AND	c.nr_prescr_interf_farm IS NOT NULL
	AND	nr_seq_interf_farm IS NULL
	and	a.cd_kit_material is null;

cursor	c03 is
	select	a.cd_material,
		nvl(a.ie_via_aplicacao,wheb_mensagem_pck.get_texto(277408)),
		nvl(a.cd_unidade_medida_dose,wheb_mensagem_pck.get_texto(277408)),
		nvl(a.qt_dose,0),
		nvl(a.ie_se_necessario,'N'),
		nvl(a.ie_acm,'N'),
		nvl(a.ie_urgencia,'N'),
		nvl(a.cd_intervalo,9999999),
		nvl(a.hr_prim_horario,wheb_mensagem_pck.get_texto(277408)),
		nvl(a.ds_horarios,wheb_mensagem_pck.get_texto(277408)),
		nvl(a.ie_medicacao_paciente,'N'),
		c.nr_atendimento,
		c.cd_medico,
		c.cd_pessoa_fisica
	from	prescr_material a,
			prescr_medica c
	where 	a.nr_prescricao = c.nr_prescricao
	and		c.dt_liberacao is not null
	and		a.ie_agrupador = 1
	and		c.nr_prescricao	= nr_prescricao_origem_w
	and 	a.cd_material = cd_item_p;

begin

select	max(a.cd_funcao_origem)
into	cd_funcao_origem_w
from	prescr_medica	a
where	a.nr_prescricao	= nr_prescricao_p;

if	(cd_funcao_origem_w = 2314) then
	Obter_Param_Usuario(252,58,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,cd_estabelecimento_p,VarIeEvolucaoClinica_w);
	Obter_Param_Usuario(252,59,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,cd_estabelecimento_p,ie_modificar_origem_w);
else
	Obter_Param_Usuario(7010,87,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,cd_estabelecimento_p,VarIeEvolucaoClinica_w);
	Obter_Param_Usuario(7010,174,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,cd_estabelecimento_p,ie_modificar_origem_w);
end if;

cabecalho_w 	:= wheb_mensagem_pck.get_texto(277393, 'NR_PRESCRICAO_P=' || nr_prescricao_p);
cabecalho_w2 	:= wheb_mensagem_pck.get_texto(277399, 'NR_PRESCRICAO_P=' || nr_prescricao_p);

open c01;
loop
fetch c01 into
	cd_material_origem_w,
	cd_material_interv_w,
	ie_via_aplicacao_origem_w,
	ie_via_aplicacao_interv_w,
	cd_unidade_medida_origem_w,
	cd_unidade_medida_interv_w,
	qt_dose_origem_w,
	qt_dose_interv_w,
	ie_se_necessario_origem_w,
	ie_se_necessario_interv_w,
	ie_acm_origem_w,
	ie_acm_interv_w,
	ie_urgencia_origem_w,
	ie_urgencia_interv_w,
	cd_intervalo_origem_w,
	cd_intervalo_interv_w,
	hr_prim_horario_origem_w,
	hr_prim_horario_interv_w,
	ds_horarios_origem_w,
	ds_horarios_interv_w,
	ie_medicacao_paciente_origem_w,
	ie_medicacao_paciente_interv_w,
	nr_atendimento_w,
	cd_medico_w,
	cd_pessoa_fisica_w;
exit when c01%notfound;
	if	(cd_material_origem_w <> cd_material_interv_w) or
		(ie_via_aplicacao_origem_w <> ie_via_aplicacao_interv_w) or
		(cd_unidade_medida_origem_w <> cd_unidade_medida_interv_w) or
		(qt_dose_origem_w <> qt_dose_interv_w) or
		(ie_se_necessario_origem_w) <> (ie_se_necessario_interv_w) or
		(ie_acm_origem_w) <> (ie_acm_interv_w) or
		(ie_urgencia_origem_w) <> (ie_urgencia_interv_w) or
		(cd_intervalo_origem_w) <> (cd_intervalo_interv_w) or
		(hr_prim_horario_origem_w) <> (hr_prim_horario_interv_w) or
		(ds_horarios_origem_w) <> (ds_horarios_interv_w) or
		(ie_medicacao_paciente_origem_w) <> (ie_medicacao_paciente_interv_w) then
		if	(ds_evolucao_w is null) then
			ds_evolucao_w := cabecalho_w || wheb_mensagem_pck.get_texto(277411, 'CD_MATERIAL_ORIGEM_P=' || obter_desc_material(cd_material_origem_w));
		else
			ds_evolucao_w := ds_evolucao_w ||chr(13)|| wheb_mensagem_pck.get_texto(277411, 'CD_MATERIAL_ORIGEM_P=' || obter_desc_material(cd_material_origem_w));
		end if;
	end if;

	if	(cd_material_origem_w <> cd_material_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277413, 'CD_MATERIAL_ORIGEM_P=' || obter_desc_material(cd_material_origem_w) || ';CD_MATERIAL_INTERV_P=' || obter_desc_material(cd_material_interv_w));
	end if;

	if	(ie_via_aplicacao_origem_w <> ie_via_aplicacao_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277414, 'IE_VIA_APLICACAO_ORIGEM_P=' || Obter_Desc_via(ie_via_aplicacao_origem_w)  || ';IE_VIA_APLICACAO_INTERV_P=' || Obter_Desc_via(ie_via_aplicacao_interv_w));
	end if;

	if	(cd_unidade_medida_origem_w <> cd_unidade_medida_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277419, 'CD_UNIDADE_MEDIDA_ORIGEM_P=' || qt_dose_origem_w || ' ' || Obter_Desc_Unid_Med(cd_unidade_medida_origem_w) || ';CD_UNIDADE_MEDIDA_INTERV_P=' || qt_dose_interv_w || ' ' || Obter_Desc_Unid_Med(cd_unidade_medida_interv_w));
	end if;

	if	(qt_dose_origem_w <> qt_dose_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277426, 'QT_DOSE_ORIGEM_P=' || qt_dose_origem_w || ' ' || Obter_Desc_Unid_Med(cd_unidade_medida_origem_w) || ';QT_DOSE_INTERV_P=' || qt_dose_interv_w || ' ' || Obter_Desc_Unid_Med(cd_unidade_medida_interv_w));
	end if;

	if	(ie_se_necessario_origem_w <> ie_se_necessario_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277432, 'IE_SE_NECESSARIO_ORIGEM_P=' || ie_se_necessario_origem_w || ';IE_SE_NECESSARIO_INTERV_P=' || ie_se_necessario_interv_w);
	end if;

	if	(ie_acm_origem_w <> ie_acm_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277433, 'IE_ACM_ORIGEM_P=' || ie_acm_origem_w || ';IE_ACM_INTERV_P=' || ie_acm_interv_w);
	end if;

	if	(ie_urgencia_origem_w <> ie_urgencia_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277434, 'IE_URGENCIA_ORIGEM_P=' || ie_urgencia_origem_w || ';IE_URGENCIA_INTERV_P=' || ie_urgencia_interv_w);
	end if;

	if	(cd_intervalo_origem_w <> cd_intervalo_interv_w) then
		if	(cd_intervalo_origem_w = '9999999') then
			ds_intervalo_origem_w := wheb_mensagem_pck.get_texto(277408);
		else
			ds_intervalo_origem_w := substr(OBTER_DESC_INTERVALO(cd_intervalo_origem_w),1,255);
		end if;
		if	(cd_intervalo_interv_w = '9999999') then
			ds_intervalo_interv_w := wheb_mensagem_pck.get_texto(277408);
		else
			ds_intervalo_interv_w := substr(OBTER_DESC_INTERVALO(cd_intervalo_interv_w),1,255);
		end if;
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277439, 'DS_INTERVALO_ORIGEM_P=' || ds_intervalo_origem_w  || ';DS_INTERVALO_INTERV_P=' || ds_intervalo_interv_w);
	end if;

	if	(hr_prim_horario_origem_w <> hr_prim_horario_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277441, 'HR_PRIM_HORARIO_ORIGEM_P=' || hr_prim_horario_origem_w || ';HR_PRIM_HORARIO_INTERV_P=' || hr_prim_horario_interv_w);
	end if;

	if	(ds_horarios_origem_w <> ds_horarios_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277442, 'DS_HORARIOS_ORIGEM_P=' || ds_horarios_origem_w || ';DS_HORARIOS_INTERV_P=' || ds_horarios_interv_w);
	end if;

	if	(ie_medicacao_paciente_origem_w <> ie_medicacao_paciente_interv_w) then
		ds_evolucao_w := ds_evolucao_w || wheb_mensagem_pck.get_texto(277447, 'IE_MEDICACAO_PACIENTE_ORIGEM_P=' || ie_medicacao_paciente_origem_w || ';IE_MEDICACAO_PACIENTE_INTERV_P=' || ie_medicacao_paciente_interv_w);
	end if;
end loop;
close c01;

open c02;
loop
fetch c02 into
	cd_material_interv_w,
	ds_via_aplicacao_interv_w,
	ds_unidade_medida_interv_w,
	qt_dose_interv_w,
	ie_se_necessario_interv_w,
	ie_acm_interv_w,
	ie_urgencia_interv_w,
	cd_intervalo_interv_w,
	hr_prim_horario_interv_w,
	ds_horarios_interv_w,
	ie_medicacao_paciente_interv_w,
	nr_atendimento_w,
	cd_medico_w,
	cd_pessoa_fisica_w;
exit when c02%notfound;
	if	(ie_primeira_vez_w) then
		ds_evolucao_w := ds_evolucao_w || cabecalho_w2;
	end if;
	ie_primeira_vez_w := False;
	if	(cd_intervalo_interv_w = '9999999') then
		ds_intervalo_interv_w := wheb_mensagem_pck.get_texto(277408);
	else
		ds_intervalo_interv_w := substr(OBTER_DESC_INTERVALO(cd_intervalo_interv_w),1,255);
	end if;

	ds_evolucao_w := 	ds_evolucao_w || wheb_mensagem_pck.get_texto(277448, 	'CD_MATERIAL_INTERV_P=' || cd_material_interv_w ||
											';DS_VIA_APLICACAO_INTERV_P=' || ds_via_aplicacao_interv_w ||
											';DS_UNIDADE_MEDIDA_INTERV_P=' || ds_unidade_medida_interv_w ||
											';QT_DOSE_INTERV_P=' || qt_dose_interv_w ||
											';DS_UNIDADE_MEDIDA_INTERV_P=' || ds_unidade_medida_interv_w ||
											';IE_SE_NECESSARIO_INTERV_P=' || ie_se_necessario_interv_w ||
											';IE_ACM_INTERV_P=' || ie_acm_interv_w ||
											';IE_URGENCIA_INTERV_P=' || ie_urgencia_interv_w ||
											';DS_INTERVALO_INTERV_P=' || ds_intervalo_interv_w ||
											';HR_PRIM_HORARIO_INTERV_P=' || hr_prim_horario_interv_w ||
											';DS_HORARIOS_INTERV_P=' || ds_horarios_interv_w ||
											';IE_MEDICACAO_PACIENTE_INTERV_P=' || ie_medicacao_paciente_interv_w);
end loop;
close c02;

if 	(ie_modificar_origem_w = 'S') and
	(ds_evolucao_w is not null) and
	(cd_item_p is not null) and
	(not ie_primeira_vez_w) then

	select 	max(nr_prescricao_anterior)
	into 	nr_prescricao_origem_w
	from 	prescr_medica
	where 	nr_prescricao = nr_prescricao_p;

	open c03;
	loop
	fetch c03 into
		cd_material_origem_w,
		ie_via_aplicacao_origem_w,
		cd_unidade_medida_origem_w,
		qt_dose_origem_w,
		ie_se_necessario_origem_w,
		ie_acm_origem_w,
		ie_urgencia_origem_w,
		cd_intervalo_origem_w,
		hr_prim_horario_origem_w,
		ds_horarios_origem_w,
		ie_medicacao_paciente_origem_w,
		nr_atendimento_w,
		cd_medico_w,
		cd_pessoa_fisica_w;
	exit when c03%notfound;
			if	(cd_intervalo_origem_w = '9999999') then
				ds_intervalo_interv_w := wheb_mensagem_pck.get_texto(277408);
			else
				ds_intervalo_interv_w := substr(OBTER_DESC_INTERVALO(cd_intervalo_origem_w),1,255);
			end if;

				ds_evolucao_w := 	ds_evolucao_w || wheb_mensagem_pck.get_texto(750422, 	'NR_PRESCRICAO_ORIGEM_P=' || nr_prescricao_origem_w ||
												';CD_MATERIAL_INTERV_P=' || cd_material_origem_w ||
												';DS_VIA_APLICACAO_INTERV_P=' || ie_via_aplicacao_origem_w ||
												';DS_UNIDADE_MEDIDA_INTERV_P=' || cd_unidade_medida_origem_w ||
												';QT_DOSE_INTERV_P=' || qt_dose_origem_w ||
												';DS_UNIDADE_MEDIDA_INTERV_P=' || cd_unidade_medida_origem_w ||
												';IE_SE_NECESSARIO_INTERV_P=' || ie_se_necessario_origem_w ||
												';IE_ACM_INTERV_P=' || ie_acm_origem_w ||
												';IE_URGENCIA_INTERV_P=' || ie_urgencia_origem_w ||
												';DS_INTERVALO_INTERV_P=' || ds_intervalo_interv_w ||
												';HR_PRIM_HORARIO_INTERV_P=' || hr_prim_horario_origem_w ||
												';DS_HORARIOS_INTERV_P=' || ds_horarios_origem_w ||
												';IE_MEDICACAO_PACIENTE_INTERV_P=' || ie_medicacao_paciente_origem_w);
	end loop;
	close c03;
end if;

if	(ds_evolucao_w is not null) then

	select	ie_tipo_evolucao
	into	ie_tipo_evolucao_w
	from	usuario
	where	nm_usuario = nm_usuario_p;

	if	(ie_tipo_evolucao_w is null) then
		ds_erro_w := 	wheb_mensagem_pck.get_texto(277513);
	end if;

	if	(ie_tipo_evolucao_w is not null) then
		select	evolucao_paciente_seq.nextval
		into	cd_evolucao_w
		from 	dual;

		insert	into	evolucao_paciente(
				ie_situacao,
				nm_usuario,
				dt_atualizacao,
				cd_pessoa_fisica,
				cd_evolucao,
				ds_evolucao,
				ie_tipo_evolucao,
				dt_evolucao,
				nr_atendimento,
				cd_medico,
				ie_evolucao_clinica)
		values		('A',
				nm_usuario_p,
				sysdate,
				cd_pessoa_fisica_w,
				cd_evolucao_w,
				ds_evolucao_w,
				ie_tipo_evolucao_w,
				sysdate,
				nr_atendimento_w,
				Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C'),
				VarIeEvolucaoClinica_w);
		cd_evolucao_p	:= cd_evolucao_w;
		commit;
	end if;
	ds_erro_p := substr(ds_erro_w,1,255);
end if;

end gerar_evolucao_alteracao_item;
/
