create or replace
procedure pls_oc_cta_aplica_valida_imp(	nr_seq_ocorrencia_p		in pls_oc_cta_combinada.nr_seq_ocorrencia%type,
					nr_seq_combinada_p		in pls_oc_cta_combinada.nr_sequencia%type,		
					ie_validacao_p			in pls_oc_cta_tipo_validacao.cd_validacao%type,
					ie_regra_excecao_p		in pls_oc_cta_combinada.ie_excecao%type,	
					nr_id_transacao_p		in pls_oc_cta_selecao_imp.nr_id_transacao%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type) is 
begin
-- Deve ter informa��o da regra para executar algo
if	(nr_seq_combinada_p is not null) then

	case (ie_validacao_p) 
		/* Somente filtros
		se for somente filtros n�o � realizado manuten��o em lugar algum e apenas gerado a ocorr�ncia para todos os itens da tabela de sele��o */
		-- Valida carteira do benefici�rio
		when 2 then
			pls_oc_cta_tratar_val_2_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p, nm_usuario_p);
		-- Valida sexo exclusivo do procedimento
		when 3 then
			pls_oc_cta_tratar_val_3_imp(	nr_seq_combinada_p, ie_regra_excecao_p,
							nr_id_transacao_p, nm_usuario_p);
		-- Validar exig�ncia de procedimento
		when 5 then
			pls_oc_cta_tratar_val_5_imp(	nr_seq_combinada_p, ie_regra_excecao_p,
							nr_id_transacao_p);
		-- Validar item no per�odo de interna��o
		when 6 then
			pls_oc_cta_tratar_val_6_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);  
		-- Validar itens autorizados por�m n�o utilizados	
		when 7 then
			pls_oc_cta_tratar_val_7_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar prestador inativo
		when 9 then
			pls_oc_cta_tratar_val_9_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);		
		-- Validar benefici�rio da autoriza��o
		when 10 then
			pls_oc_cta_tratar_val_10_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar exig�ncia de hora do item
		when 11 then
			pls_oc_cta_tratar_val_11_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar restri��es do material
		when 12 then
			pls_oc_cta_tratar_val_12_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar se existem itens na conta que fazem parte da composi��o do pacote lan�ado
		when 14 then
			pls_oc_cta_tratar_val_14_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar quantidade fracionada do item
		when 16 then
			pls_oc_cta_tratar_val_16_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p); 
		--Validar caracter�sticas do profissional
		when 19 then 
			pls_oc_cta_tratar_val_19_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar caracter�sticas do CBO
		when 20 then
			pls_oc_cta_tratar_val_20_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar prazo de envio da conta
		when 21 then
			pls_oc_cta_tratar_val_21_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar n�mero da senha informado diferente do liberado
		when 22 then
			pls_oc_cta_tratar_val_22_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar utiliza��o de itens
		when 23 then
			pls_oc_cta_tratar_val_23_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar profissional solicitante n�o informado
		when 25 then
			pls_oc_cta_tratar_val_25_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar limita��es contratuais				
		when 27 then
			pls_oc_cta_tratar_val_27_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p, nm_usuario_p);
			
		--Validar libera��o do material pelo prestador
		when 30 then
			pls_oc_cta_tratar_val_30_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
							
		--ocorr�ncia em itens simult�neos ou concorrentes
		when 32 then
			pls_oc_cta_tratar_val_32_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
							
		-- Validar situa��o do m�dico executor
		when 34 then
			pls_oc_cta_tratar_val_34_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar datas da conta
		when 35 then
			pls_oc_cta_tratar_val_35_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		when 36 then
			pls_oc_cta_tratar_val_36_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar exig�ncia de prestador fornecedor
		when 38 then
			pls_oc_cta_tratar_val_38_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar procedimento x especialidade
		when 40 then
			pls_oc_cta_tratar_val_40_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar datas do item
		when 41 then    
			pls_oc_cta_tratar_val_41_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar regra atributo conta
		when 42 then    
			pls_oc_cta_tratar_val_42_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar se existe informa��o sobre a tabela que ser� utilizada na valora��o
		when 43 then
			pls_oc_cta_tratar_val_43_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar regra de participantes
		
		when 45 then
			pls_oc_cta_tratar_val_45_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		-- Validar quantidade de itens
		when 46 then
			pls_oc_cta_tratar_val_46_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar procedimento x grau de participa��o
		
		when 47 then
			pls_oc_cta_tratar_val_47_imp(nr_seq_combinada_p, 
						     ie_regra_excecao_p, 
						     nr_id_transacao_p);
		
		-- Validar apresenta��o do documento f�sico
		when 48 then
			pls_oc_cta_tratar_val_48_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar quantidade de itens
		when 49 then
			pls_oc_cta_tratar_val_49_imp(nr_seq_combinada_p, 
						     ie_regra_excecao_p, 
						     nr_id_transacao_p);
		--Validar solicita��o de libera��o de mat/med
		when 50 then
			pls_oc_cta_tratar_val_50_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p, nm_usuario_p);
		
		--Validar se a guia possui liminar judicial
		when 51 then
			pls_oc_cta_tratar_val_51_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar se procedimento est� habilitado para o prestador
		when 52 then
			pls_oc_cta_tratar_val_52_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar duplicidade de itens
		when 53 then
			pls_oc_cta_tratar_val_53_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar car�ncia de servi�o
		when 54 then
			pls_oc_cta_tratar_val_54_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar libera��o do item por caracter�sticas da conta
		when 55 then
			pls_oc_cta_tratar_val_55_imp(	nr_seq_combinada_p, ie_regra_excecao_p, 
							nr_id_transacao_p);
		/*-- Validar prestador solicitante n�o informado
		when 56 then
			pls_oc_cta_tratar_val_56(dados_regra_p, nr_id_transacao_p, nm_usuario_p);
		-- Validar regra de correla��o de itens
		*/
		when 57 then
			pls_oc_cta_tratar_val_57_imp(nr_seq_combinada_p, ie_regra_excecao_p, nr_id_transacao_p);
		
		-- Validar guia autorizada
		when 58 then
			pls_oc_cta_tratar_val_58_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- Validar regra datas do protocolo
		when 60 then
			pls_oc_cta_tratar_val_60_imp(nr_seq_combinada_p, ie_regra_excecao_p, nr_id_transacao_p);
		
		-- Validar se prestador solicitante � igual ao da requisi��o
		when 61 then
			pls_oc_cta_tratar_val_61_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Valida guia refer�ncia conforme TISS
		when 62 then
			pls_oc_cta_tratar_val_62_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		/*
		--Validar apresenta��o de contas enquanto benefici�rio internado
		when 63 then
			pls_oc_cta_tratar_val_63(dados_regra_p, nr_id_transacao_p, nm_usuario_p);
		*/
		--Validar CBO do participante
		when 65 then
			pls_oc_cta_tratar_val_65_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar data faturamento guia de honor�rio individual
		
		when 66 then
			pls_oc_cta_tratar_val_66_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		-- Validar regra de atributo do item
		when 67 then
			pls_oc_cta_tratar_val_67_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Valida vig�ncia itens
		when 68 then
			pls_oc_cta_tratar_val_68_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar itens da conta m�dica
		when 69 then
			pls_oc_cta_tratar_val_69_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar guias para controle da produ��o m�dica
		
		when 72 then
			pls_oc_cta_tratar_val_72_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar unidade de medida materiais
		when 73 then
			pls_oc_cta_tratar_val_73_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		-- Validar valores limite do atendimento 
		when 74 then 
			pls_oc_cta_tratar_val_74_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		/*
		--Validar exig�ncia de auditoria
		when 75 then
			pls_oc_cta_tratar_val_75(dados_regra_p, nr_id_transacao_p, nm_usuario_p);
		*/
		--Validar se o profissional executante est� vinculado ao prestador executor
		when 76 then
			pls_oc_cta_tratar_val_76_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar classifica��o do porte do item
		when 78 then
			pls_oc_cta_tratar_val_78_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p,
							cd_estabelecimento_p);
		--Validar senha de itens	

		when 79 then
			pls_oc_cta_tratar_val_79_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar a suspens�o de atendimento da operadora
		when 80 then
			pls_oc_cta_tratar_val_80_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		/*
		--Validar se o profissional executante est� vinculado ao prestador participante
		when 81 then
			pls_oc_cta_tratar_val_81(dados_regra_p, nr_id_transacao_p, nm_usuario_p);
		*/
		--Validar rescis�o do benefici�rio conforme item da conta
		when 82 then
			pls_oc_cta_tratar_val_82_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar rescis�o do benefici�rio conforme item da conta
		when 83 then
			pls_oc_cta_tratar_val_83_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar indica��o de acidente
		when 84 then
			pls_oc_cta_tratar_val_84_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar valores do itens
		when 85 then
			pls_oc_cta_tratar_val_85_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar CNES do prestador
		when 86 then
			pls_oc_cta_tratar_val_86_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar libera��o usu�rio prestador web
		when 87 then
			pls_oc_cta_tratar_val_87_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Regra de refer�ncia de itens
		when 88 then
			pls_oc_cta_tratar_val_88_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		/*
		--Validar regime de interna��o
		when 89 then
			pls_oc_cta_tratar_val_89(dados_regra_p, nr_id_transacao_p, nm_usuario_p);
		*/
		--Validar n�mero da guia
		when 90 then
			pls_oc_cta_tratar_val_90_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Validar n�mero da guia
		when 91 then
			pls_oc_cta_tratar_val_91_imp(nr_seq_combinada_p, ie_regra_excecao_p, nr_id_transacao_p);
		--Validar notas fiscais duplicadas	
		/*when 92 then
			pls_oc_cta_tratar_val_92(dados_regra_p, nr_id_transacao_p, nm_usuario_p); */
		--Valida per�odo idade benefici�rio
		when 93 then
			pls_oc_cta_tratar_val_93_imp(nr_seq_combinada_p, ie_regra_excecao_p, nr_id_transacao_p);
		
		--V�lidar duplicidade de cobran�a	
		when 94 then
			pls_oc_cta_tratar_val_94_imp(nr_seq_combinada_p, ie_regra_excecao_p, nr_id_transacao_p);
		
		--Validar dados de Obito na conta
		when 95 then
			pls_oc_cta_tratar_val_95_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		--Valida conta principal x guia na autoriza��o	
		when 96	then
			pls_oc_cta_tratar_val_96_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		
		-- Valida prestador x prestador 
		when 97 then
			pls_oc_cta_tratar_val_97_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar guias vinculadas
		when 98 then
			pls_oc_cta_tratar_val_98_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);	
		--Consistir a incid�ncia de grupo de procedimento
		when 99 then
			pls_oc_cta_tratar_val_99_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);							
		--Validar tempo do item
		when 100	then
			pls_oc_cta_tratar_val_100_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar tipo de tabela do material
		when 101	then
			pls_oc_cta_tratar_val_101_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);	
		--Validar os procedimentos com semelhan�a.
		when 102	then
			pls_oc_cta_tratar_val_102_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar vencimentos do prestador
		when 103	then
			pls_oc_cta_tratar_val_103_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar utiliza��o de itens em per�odo de interna��o
		when 105	then
			pls_oc_cta_tratar_val_105_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar afastamento do cooperado.
		when 106	then
			pls_oc_cta_tratar_val_106_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar n�mero da senha
		when 107	then
			pls_oc_cta_tratar_val_107_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
						
		when 108 	then
			pls_oc_cta_tratar_val_108_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
			
		--Valida��o operadora inv�lida.	
		when 109 	then
			pls_oc_cta_tratar_val_109_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar dados benefici�rio	
		when 110 	then
			pls_oc_cta_tratar_val_110_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar quantidade de di�ria X perman�ncia hospitalar	
		when 111 	then
			pls_oc_cta_tratar_val_111_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		--Validar cobertura procedimento	
		when 112 	then
			pls_oc_cta_tratar_val_112_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		-- valida tipo de atendimento conta X autorizacao
		when 113 	then
			pls_oc_cta_tratar_val_113_imp(	nr_seq_combinada_p, 
							ie_regra_excecao_p, 
							nr_id_transacao_p);
		else
			null;
	end case;
end if;
end pls_oc_cta_aplica_valida_imp;
/
