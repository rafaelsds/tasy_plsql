create or replace
procedure obter_dados_enabled_checkup_js(	nr_sequencia_p		Number,
				nm_usuario_p		Varchar2,
				cd_perfil_p		number,
				dt_confirmacao_p	out date,	
				dt_cancelamento_p	out date,
				dt_inicio_real_p	out date,
				dt_fim_real_p		out date,	
				dt_fim_interno_p	out date,
				dt_confirmacao_etapa_p	out date,
				dt_cancelamento_etapa_p	out date,
				dt_inicio_real_etapa_p	out date,
				dt_fim_etapa_p		out date,
				nr_seq_checkup_p	out number,
				qt_etapa_checkup_p	out number,
				nr_atendimento_p	out number) is 

nr_seq_checkup_w	number(10);
qt_etapa_checkup_w	number(10);
nr_atendimento_w	number(10);
dt_confirmacao_w	date;	
dt_cancelamento_w	date;
dt_inicio_real_w	date;	
dt_fim_real_w		date;	
dt_fim_interno_w	date;
dt_confirmacao_etapa_w	date;
dt_cancelamento_etapa_w	date;
dt_inicio_real_etapa_w	date;
dt_fim_etapa_w		date;
				
begin

select 	max(nr_seq_checkup) 
into	nr_seq_checkup_w
from 	checkup_etapa 
where	nr_sequencia 	= nr_sequencia_p;

select 	max(a.dt_confirmacao) 
into	dt_confirmacao_w
from 	checkup a, 
	checkup_etapa b 
where 	a.nr_sequencia = b.nr_seq_checkup 
and	b.nr_sequencia = nr_sequencia_p;

select 	max(a.dt_cancelamento) 
into	dt_cancelamento_w
from 	checkup a, 
	checkup_etapa b 
where 	a.nr_sequencia = b.nr_seq_checkup 
and	b.nr_sequencia = nr_sequencia_p;

select 	max(a.dt_inicio_real) 
into	dt_inicio_real_w
from 	checkup a, 
	checkup_etapa b 
where 	a.nr_sequencia = b.nr_seq_checkup 
and	b.nr_sequencia = nr_sequencia_p;

select 	max(a.dt_fim_real)
into	dt_fim_real_w 
from 	checkup a, 
	checkup_etapa b 
where 	a.nr_sequencia = b.nr_seq_checkup 
and	b.nr_sequencia = nr_sequencia_p;

select 	max(a.dt_fim_interno)
into	dt_fim_interno_w 
from 	checkup a, 
	checkup_etapa b 
where 	a.nr_sequencia = b.nr_seq_checkup 
and	b.nr_sequencia = nr_sequencia_p;

select 	max(dt_confirmacao) 
into	dt_confirmacao_etapa_w
from 	checkup_etapa 
where 	nr_sequencia  = nr_sequencia_p;

select 	max(dt_cancelamento)
into	dt_cancelamento_etapa_w 
from 	checkup_etapa 
where 	nr_sequencia  = nr_sequencia_p;

select 	max(dt_inicio_real)
into	dt_inicio_real_etapa_w 
from 	checkup_etapa 
where 	nr_sequencia  = nr_sequencia_p; 

select 	max(dt_fim_etapa) 
into	dt_fim_etapa_w
from 	checkup_etapa 
where 	nr_sequencia  = nr_sequencia_p;

select	nvl(max(a.nr_sequencia),0) 
into	qt_etapa_checkup_w
from	etapa_checkup a, 
	etapa_checkup_lib b, 
	checkup_etapa c
where	a.nr_sequencia	= b.nr_seq_etapa
and	a.nr_sequencia	= c.nr_seq_etapa
and	c.nr_sequencia	= nr_sequencia_p
and	((nvl(b.cd_pessoa_fisica,obter_dados_usuario_opcao(nm_usuario_p,'C')) = obter_dados_usuario_opcao(nm_usuario_p,'C')) or
(nvl(b.cd_perfil,cd_perfil_p) = cd_perfil_p));

select	max(a.nr_atendimento)
into 	nr_atendimento_w
from	checkup a, checkup_etapa b
where	a.nr_sequencia = b.nr_seq_checkup 
and	b.nr_sequencia = nr_sequencia_p;

dt_confirmacao_p	:= dt_confirmacao_w;	
dt_cancelamento_p	:= dt_cancelamento_w;	
dt_inicio_real_p	:= dt_inicio_real_w;
dt_fim_real_p		:= dt_fim_real_w;	
dt_fim_interno_p	:= dt_fim_interno_w;
dt_confirmacao_etapa_p	:= dt_confirmacao_etapa_w;
dt_cancelamento_etapa_p	:= dt_cancelamento_etapa_w;
dt_inicio_real_etapa_p	:= dt_inicio_real_etapa_w;
dt_fim_etapa_p		:= dt_fim_etapa_w;
nr_seq_checkup_p	:= nr_seq_checkup_w;
qt_etapa_checkup_p	:= qt_etapa_checkup_w;
nr_atendimento_p	:= nr_atendimento_w;

end obter_dados_enabled_checkup_js;
/