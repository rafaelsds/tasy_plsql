create or replace
procedure pls_marcar_item_pacote_ptu_aud
				(	nr_seq_item_aud_p	pls_auditoria_item.nr_sequencia%type,
					nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Marcar o item como pacote para o envio do PTU online
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

update	pls_auditoria_item
set	ie_pacote_ptu	= 'S',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_item_aud_p;

commit;

end pls_marcar_item_pacote_ptu_aud;
/