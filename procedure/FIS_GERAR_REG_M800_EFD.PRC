create or replace
procedure fis_gerar_reg_m800_efd(
				nr_seq_controle_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				ds_separador_p		varchar2,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is 

cd_empresa_w		number(4);
nr_seq_data_w		number(10);
qt_registros_w		number(10);
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_seq_registro_w		number(10) := nr_sequencia_p;
nr_linha_w		number(10) := qt_linha_p;
sep_w			varchar2(1) := ds_separador_p;
ie_local_gerar_sped_w	varchar2(1);
ie_tributacao_cst_w	varchar2(15);
vl_receita_w		varchar2(20); 
cd_conta_w		varchar2(15); 
cd_natureza_w		varchar2(15);
nr_seq_regra_efd_w	number(10);

Cursor c01 is
	/*select	abs(sum(a.vl_movimento)) vl_receita,
		'201'				cd_natureza,
		'2436'				cd_conta,
		'C'				ds_conta,
		'06'				cd_cst_cofins
	from 	ctb_balancete_v a
	where 	a.nr_seq_mes_ref = nr_seq_data_w
	and	a.cd_conta_contabil = '2436'
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.ie_normal_encerramento = 'N'
	union*/
	select	sum(cpr.vl_material)	vl_receita,
		'201'				cd_natureza,
		''				cd_conta,
		''				ds_conta,
		'04'				cd_cst_cofins
	from	conta_paciente_resumo cpr,
		conta_paciente cp,
		conta_paciente_nf cpn,
		material m,
		titulo_receber tr,
		nota_fiscal nf
	where	cpn.nr_interno_conta = cp.nr_interno_conta  
	and 	cp.nr_interno_conta = cpr.nr_interno_conta
	and 	cpn.nr_sequencia = nf.nr_sequencia
	and 	cpr.cd_material = m.cd_material
	and 	(tr.nr_interno_conta = cp.nr_interno_conta or tr.nr_seq_protocolo = cp.nr_seq_protocolo)
	and 	cp.ie_status_acerto = 2
	and 	nf.ie_situacao = 1
	and 	nf.dt_emissao <= fim_dia(dt_fim_p)
	and 	tr.dt_emissao <= fim_dia(dt_fim_p)
	and 	cp.dt_mesano_referencia between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
	and	cp.nr_seq_protocolo is not null 
	and	m.cd_classif_fiscal is not null;

vet01	c01%rowtype;

Cursor c02 is
	select	sum(decode(nvl(c.ie_tipo_valor,'VM'),'VM',a.vl_movimento,'VC',a.vl_credito,'VD',a.vl_debito)) vl_movimento,
		c.cd_conta_contabil,
		c.cd_natureza_receita,
		ie_tributacao_cst
	from 	ctb_balancete_v a,
		fis_efd_conta_contabil c,
		fis_efd_regra_tipo_ct r
	where 	a.cd_conta_contabil = c.cd_conta_contabil
	and	c.nr_seq_tipo_ct = r.nr_sequencia
	and	a.nr_seq_mes_ref = nr_seq_data_w
	and	r.ie_tipo_ct = 'RC'
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.cd_estabelecimento = nvl(c.cd_estabelecimento, cd_estabelecimento_p)
	and	a.ie_normal_encerramento = 'N'
	group by c.cd_conta_contabil, c.cd_natureza_receita, ie_tributacao_cst;
		
begin

select	cd_empresa
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

select	count(*)
into	qt_registros_w
from	ctb_mes_ref
where	trunc(dt_referencia,'mm') = trunc(dt_inicio_p,'mm')
and	cd_empresa = cd_empresa_w;

if (qt_registros_w = 1) then

	select	nr_sequencia
	into	nr_seq_data_w
	from 	ctb_mes_ref 
	where 	trunc(dt_referencia,'mm') = trunc(dt_inicio_p,'mm')
	and	cd_empresa = cd_empresa_w;
end if;	

select	nr_seq_regra_efd
into	nr_seq_regra_efd_w
from	fis_efd_controle
where	nr_sequencia = nr_seq_controle_p;

obter_param_usuario(5500,25,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_local_gerar_sped_w);

if (ie_local_gerar_sped_w = 'N') then
	
	-- Verifica se utiliza por conta contabil
	select	count(*)
	into	qt_registros_w
	from	fis_efd_regra_tipo_ct
	where	ie_tipo_ct = 'RC';

	if (qt_registros_w > 0) then
		open c02;
			loop
			fetch 	c02 into
				vl_receita_w, 
				cd_conta_w, 
				cd_natureza_w,
				ie_tributacao_cst_w;
			exit when c02%notfound;
				begin
					ds_linha_w := sep_w 	|| 	'M800' 			|| sep_w ||
						ie_tributacao_cst_w	|| sep_w ||
						vl_receita_w		|| sep_w ||
						cd_conta_w		|| sep_w ||
						''			|| sep_w;

					ds_arquivo_w		:= substr(ds_linha_w,1,4000);
					ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
					nr_seq_registro_w	:= nr_seq_registro_w + 1;
					nr_linha_w		:= nr_linha_w + 1;
						
					insert into 	fis_efd_arquivo	(
									nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									nr_seq_controle_efd,
									nr_linha,
									ds_arquivo,
									ds_arquivo_compl,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									cd_registro)
								values	(	
									nr_seq_registro_w,
									sysdate,
									nm_usuario_p,
									nr_seq_controle_p,
									nr_linha_w,
									ds_arquivo_w,
									ds_arquivo_compl_w,
									sysdate,
									nm_usuario_p,
									'M800'
									); 
					commit;
					
					fis_gerar_reg_m810_efd(nr_seq_controle_p, nm_usuario_p,cd_estabelecimento_p, dt_inicio_p, dt_fim_p,cd_empresa_p,ds_separador_p,nr_linha_w,nr_seq_registro_w, vl_receita_w, cd_conta_w, '', cd_natureza_w);
				end;
			end loop;
		close c02;
	else
		-- Verifica se tem regra por CST de material
		
		select	count(*)
		into	qt_registros_w
		from	fis_regra_efd_m400
		where	ie_tipo_tributo = 'C'
		and	nr_seq_regra_efd = nr_seq_regra_efd_w;
		
		if (qt_registros_w > 0) then
		
			select	ie_tributacao_cst,
				sum(vl_item)
			into	ie_tributacao_cst_w,
				vl_receita_w
			from	w_sped_efd_m400
			group by ie_tributacao_cst;
			
			select	max(cd_natureza_receita)
			into	cd_natureza_w
			from	fis_regra_efd_m400
			where	ie_tributacao_cst = ie_tributacao_cst_w
			and	ie_tipo_tributo = 'C'
			and	nr_seq_regra_efd = nr_seq_regra_efd_w;
		
		end if;
	
	end if;
	
	if (qt_registros_w > 0) then
		-- Chama a procedure caso teve registro na conta contabil ou na regra por CST
		
		-- Montagem das linhas do arquivo
		ds_linha_w := sep_w 	|| 	'M800' 			|| sep_w ||
						ie_tributacao_cst_w	|| sep_w ||
						vl_receita_w		|| sep_w ||
						cd_conta_w		|| sep_w ||
						''			|| sep_w;

		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		nr_linha_w		:= nr_linha_w + 1;
			
		insert into 	fis_efd_arquivo	(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_controle_efd,
						nr_linha,
						ds_arquivo,
						ds_arquivo_compl,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_registro)
					values	(	
						nr_seq_registro_w,
						sysdate,
						nm_usuario_p,
						nr_seq_controle_p,
						nr_linha_w,
						ds_arquivo_w,
						ds_arquivo_compl_w,
						sysdate,
						nm_usuario_p,
						'M800'
						); 
		commit;
		
		fis_gerar_reg_m810_efd(nr_seq_controle_p, nm_usuario_p,cd_estabelecimento_p, dt_inicio_p, dt_fim_p,cd_empresa_p,ds_separador_p,nr_linha_w,nr_seq_registro_w, vl_receita_w, cd_conta_w, '', cd_natureza_w);
	end if;


elsif (ie_local_gerar_sped_w = 'C') then 

	select	count(*)
	into	qt_registros_w
	from	fis_regra_efd_m400
	where	ie_tipo_tributo = 'C'
	and	nr_seq_regra_efd = nr_seq_regra_efd_w;
	
	if (qt_registros_w > 0) then
		
		select	max(cd_natureza_receita)
		into	cd_natureza_w
		from	fis_regra_efd_m400
		where	ie_tipo_tributo = 'C'
		and	nr_seq_regra_efd = nr_seq_regra_efd_w;
	
	end if;

	open c01;
	loop
	fetch c01 into	
		vet01;
	exit when c01%notfound;
		begin
		
		-- Montagem das linhas do arquivo
		ds_linha_w := sep_w 	|| 	'M800' 			|| sep_w ||
						vet01.cd_cst_cofins	|| sep_w ||
						vet01.vl_receita	|| sep_w ||
						vet01.cd_conta		|| sep_w ||
						vet01.ds_conta		|| sep_w;

		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
		nr_seq_registro_w		:= nr_seq_registro_w + 1;
		nr_linha_w		:= nr_linha_w + 1;
			
		insert into 	fis_efd_arquivo	(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_controle_efd,
						nr_linha,
						ds_arquivo,
						ds_arquivo_compl,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_registro)
					values	(	
						nr_seq_registro_w,
						sysdate,
						nm_usuario_p,
						nr_seq_controle_p,
						nr_linha_w,
						ds_arquivo_w,
						ds_arquivo_compl_w,
						sysdate,
						nm_usuario_p,
						'M800'
						); 
		commit;
		
		fis_gerar_reg_m810_efd(nr_seq_controle_p, nm_usuario_p,cd_estabelecimento_p, dt_inicio_p, dt_fim_p,cd_empresa_p,ds_separador_p,nr_linha_w,nr_seq_registro_w, vet01.vl_receita, vet01.cd_conta, vet01.ds_conta, cd_natureza_w);
		
		end;
	end loop;
	close c01;
end if;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end fis_gerar_reg_m800_efd;
/
