create or replace
procedure pls_excluir_espec_prest_pf(	cd_pessoa_fisica_p	varchar2,
					cd_especialidade_p     	number) is 
				
begin

if	(cd_pessoa_fisica_p is not null) and
	(cd_especialidade_p is not null) then
	delete from pls_prestador_med_espec
	where	cd_especialidade = cd_especialidade_p
	and	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	commit;
end if;

end pls_excluir_espec_prest_pf;
/