create or replace
procedure tasy_obter_config_painel(	cd_funcao_p		number,
					nm_usuario_p		varchar2,
					ds_cor_fonte_p		out varchar2,
					ds_fonte_p		out varchar2,               
					ds_estilo_fonte_p	out varchar2,
					qt_tamanho_fonte_p	out number ) is 

begin

begin
select	ds_cor_fonte,
	ds_fonte,               
	ds_estilo_fonte,
	qt_tamanho_fonte
into	ds_cor_fonte_p,
	ds_fonte_p,               
	ds_estilo_fonte_p,
	qt_tamanho_fonte_p
from	tasy_painel_fonte
where	nm_usuario 	= nm_usuario_p
and	cd_funcao	= cd_funcao_p;
exception
	when others then
	ds_cor_fonte_p		:= null;
	ds_fonte_p		:= null;
	ds_estilo_fonte_p	:= null;
	qt_tamanho_fonte_p	:= null;
end;

end tasy_obter_config_painel;
/
