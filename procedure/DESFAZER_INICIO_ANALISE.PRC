create or replace
procedure desfazer_inicio_analise(	nr_sequencia_p		Number,
					ds_justificativa_p	Varchar2,
					ie_status_p		Varchar2,
					ie_atualizar_status_p	Varchar2,
					nm_usuario_p		Varchar2) is 
-- Utilizado na Gest�o de transcri��o SWING
begin

atualiza_inicio_analise_gt(nr_sequencia_p, ds_justificativa_p, nm_usuario_p);

if 	(ie_atualizar_status_p = 'S') then

	Atualiza_Status_Transcricao(nr_sequencia_p, ie_status_p, nm_usuario_p);

end if;

commit;

end desfazer_inicio_analise;
/