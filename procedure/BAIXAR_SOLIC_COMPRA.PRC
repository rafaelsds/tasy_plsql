create or replace
procedure baixar_solic_compra(	nr_solic_compra_p		Number,
				cd_motivo_baixa_p		number,
				ds_observacao_p		varchar2,
				nm_usuario_p		Varchar2,
				ie_commit_p		varchar2 default 'S') as

qt_existe_w		Number(10);
ds_historico_w		Varchar2(255);
ds_cargo_pessoa_w	Varchar2(255);
cd_pessoa_fisica_w	Varchar2(10);
cd_cargo_w		Number(10);
nm_usuario_w		Varchar2(255);
ds_observacao_w		solic_compra.ds_observacao%type;
cd_estabelecimento_w	Number(4,0);
nr_seq_classif_w	Number(10,0);
nr_sequencia_w		Number(10,0);	
ds_comunicacao_w	Varchar2(2000);
ds_titulo_w		Varchar2(80);
qt_existe_regra_w	number(10);
nr_seq_regra_w		number(10);
cd_setor_atendimento_w	number(5);
cd_perfil_w		varchar2(10);
ie_ci_lida_w		varchar2(1);
/* Se tiver setor na regra, envia CI para os setores */
ds_setor_adicional_w            varchar2(2000) := '';
/* Campos da regra Usu�rio da Regra */
cd_setor_regra_usuario_w	number(5);
nr_item_solic_compra_w		number(5);
ie_empenho_orcamento_w		varchar2(1);
cd_setor_solicitante_w		setor_atendimento.cd_setor_atendimento%type;
qt_length_w			number(10);

qt_material_w				solic_compra_item.qt_material%type;
qt_material_pendente_w		solic_compra_item.qt_material%type;
qt_material_cancelado_w		solic_compra_item.qt_material_cancelado%type;

cursor c01 is
select	b.nr_sequencia,
	b.cd_perfil
from	regra_envio_comunic_compra a,
	regra_envio_comunic_evento b
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_funcao = 913
and	b.cd_evento = 8
and	b.ie_situacao = 'A'
and	a.cd_estabelecimento = cd_estabelecimento_w
and	(((cd_setor_atendimento_w is not null) and (b.cd_setor_destino = cd_setor_atendimento_w)) or
	((cd_setor_atendimento_w is null) and (b.cd_setor_destino is null)) or (b.cd_setor_destino is null))
and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,nr_solic_compra_p,'SC',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S';

cursor c02 is
select	nr_item_solic_compra
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p
and	dt_baixa is null;

	
Cursor c05 is
select	nvl(a.cd_setor_atendimento,0) cd_setor_atendimento
from	regra_envio_comunic_usu a
where	a.nr_seq_evento = nr_seq_regra_w;

begin

ds_historico_w	:= WHEB_MENSAGEM_PCK.get_texto(297835);

select	ds_observacao
into	ds_observacao_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

if	(ds_observacao_p is not null) then
	select	length(ds_observacao_p)
	into	qt_length_w
	from	dual;
	
	if	(upper(ds_observacao_w) like '%'||upper(substr(ds_observacao_p,qt_length_w-50,qt_length_w))||'%') then
		ds_observacao_w := ds_observacao_w;		
	else
		if (length(ds_observacao_w) > 0) then 
			ds_observacao_w := substr(ds_observacao_w || chr(13) || chr(10)|| ds_observacao_p,1,4000);	
		else
			ds_observacao_w := substr(ds_observacao_p,1,4000);	
		end if;
	end if;

end if;


/*Baixa a solicita��o de compras*/
update	solic_compra
set	dt_baixa		= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	cd_motivo_baixa	= cd_motivo_baixa_p,
	ds_observacao	= ds_observacao_w
where	nr_solic_compra	= nr_solic_compra_p;

/*Baixa todos os itens da solicita��o de compras*/

open C02;
loop
fetch C02 into	
	nr_item_solic_compra_w;
exit when C02%notfound;
	begin
	
	select	ds_observacao
	into	ds_observacao_w
	from	solic_compra_item
	where	nr_solic_compra	= nr_solic_compra_p
	and	nr_item_solic_compra = nr_item_solic_compra_w;

	if	(ds_observacao_p is not null) then
		ds_observacao_w	:= substr(ds_observacao_w || ds_observacao_p,1,255);
	end if;
	
	select	qt_material,
			obter_qt_sc_comprometido(nr_solic_compra, nr_item_solic_compra, null, wheb_usuario_pck.get_cd_estabelecimento),
			(qt_material - obter_qt_sc_comprometido(nr_solic_compra, nr_item_solic_compra, null, wheb_usuario_pck.get_cd_estabelecimento))
	into	qt_material_w,
			qt_material_cancelado_w,
			qt_material_pendente_w
	from solic_compra_item
	where nr_solic_compra = nr_solic_compra_p
	and nr_item_solic_compra = nr_item_solic_compra_w;	
	
	if ((qt_material_cancelado_w + qt_material_pendente_w) > qt_material_w) then
		begin
			qt_material_cancelado_w := qt_material_w;
		end;
	else
		begin
			qt_material_cancelado_w := qt_material_cancelado_w + qt_material_pendente_w;
		end;
	end if;
	
	
	update	solic_compra_item
	set	dt_baixa	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		cd_motivo_baixa	= cd_motivo_baixa_p,
		ds_observacao	= ds_observacao_w,
		qt_material_cancelado = qt_material_pendente_w
	where	nr_solic_compra	= nr_solic_compra_p
	and	nr_item_solic_compra = nr_item_solic_compra_w;

	gerar_hist_solic_sem_commit(
			nr_solic_compra_p,
			WHEB_MENSAGEM_PCK.get_texto(297844),
			WHEB_MENSAGEM_PCK.get_texto(297849,'NR_ITEM_SOLIC_COMPRA_W=' || nr_item_solic_compra_w || ';' ||
							'NR_SOLIC_COMPRA_P=' || nr_solic_compra_p),
			'B',
			nm_usuario_p);

		
	end;
end loop;
close C02;

/*Baixa os processos de aprova��o que ainda estejam pendentes*/
select	count(*),
	nvl(max(cd_pessoa_fisica),'0'),
	nvl(max(cd_cargo),0)
into	qt_existe_w,
	cd_pessoa_fisica_w,
	cd_cargo_w
from	processo_aprov_compra
where	nr_sequencia in (
	select	distinct(nr_seq_aprovacao)
	from	solic_compra_item
	where	nr_solic_compra = nr_solic_compra_p)
and	ie_aprov_reprov = 'P';
if	(qt_existe_w > 0) then
	begin
	update	processo_aprov_compra
	set	ie_aprov_reprov = 'B'
	where	nr_sequencia in (
		select	distinct(nr_seq_aprovacao)
		from	solic_compra_item
		where	nr_solic_compra = nr_solic_compra_p)
	and	ie_aprov_reprov = 'P';


	if	(cd_pessoa_fisica_w <> '0') then
		select	substr(cd_pessoa_fisica || ' - ' || obter_nome_pf(cd_pessoa_fisica),1,255)
		into	ds_cargo_pessoa_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		ds_historico_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(297851,'DS_HISTORICO_W=' || ds_historico_w || ';' || 
									'DS_CARGO_PESSOA_W=' || ds_cargo_pessoa_w),1,255);
	elsif	(cd_cargo_w > 0) then
		select	cd_cargo_w || ' - ' || ds_cargo
		into	ds_cargo_pessoa_w
		from	cargo
		where	cd_cargo = cd_cargo_w;
		ds_historico_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(297855,'DS_HISTORICO_W=' || ds_historico_w || ';' || 
									'DS_CARGO_PESSOA_W=' || ds_cargo_pessoa_w),1,255);
	end if;
	end;
end if;

/*Comunicacao interna*/
select	cd_estabelecimento,
	cd_setor_atendimento,
	obter_setor_usuario(obter_usuario_pessoa(cd_pessoa_solicitante))
into	cd_estabelecimento_w,
	cd_setor_atendimento_w,
	cd_setor_solicitante_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

open C01;
loop
fetch C01 into	
	nr_seq_regra_w,
	cd_perfil_w;
exit when C01%notfound;
	begin

	select	count(*)
	into	qt_existe_regra_w
	from	regra_envio_comunic_compra a,
		regra_envio_comunic_evento b,
		regra_envio_comunic_usu c
	where	a.nr_sequencia = b.nr_seq_regra
	and	b.nr_sequencia = c.nr_seq_evento
	and	b.nr_sequencia = nr_seq_regra_w;

	select	nvl(ie_ci_lida,'N')
	into	ie_ci_lida_w
	from 	regra_envio_comunic_evento
	where 	nr_sequencia = nr_seq_regra_w;

	if	(qt_existe_regra_w > 0) then

	
		open C05;
		loop
		fetch C05 into	
			cd_setor_regra_usuario_w;
		exit when C05%notfound;
			begin
			if	(cd_setor_regra_usuario_w <> 0) and
				(obter_se_contido_char(cd_setor_regra_usuario_w, ds_setor_adicional_w) = 'N') then
				ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_regra_usuario_w || ',',1,2000);
			end if;
			end;
		end loop;
		close C05;

		
		select	count(*)
		into	qt_existe_regra_w
		from	regra_envio_comunic_compra a,
			regra_envio_comunic_evento b,
			regra_envio_comunic_usu c
		where	a.nr_sequencia = b.nr_seq_regra
		and	b.nr_sequencia = c.nr_seq_evento
		and	b.nr_sequencia = nr_seq_regra_w
		and	c.ie_usuario = 'SSS';

		
		if	(qt_existe_regra_w > 0) and
			(cd_setor_solicitante_w > 0) and
			(obter_se_contido_char(cd_setor_solicitante_w, ds_setor_adicional_w) = 'N') then

			
			ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_solicitante_w || ',',1,2000);
		end if;

		nm_usuario_w := obter_usuarios_comunic_compras(nr_solic_compra_p,null,8,nr_seq_regra_w,'');

		
		select	substr(max(obter_dados_regra_com_compra(nr_solic_compra, null, 913, 8, nr_seq_regra_w,'T')),1,80),
			substr(max(obter_dados_regra_com_compra(nr_solic_compra, null, 913, 8, nr_seq_regra_w,'M')),1,2000)
		into	ds_titulo_w,
			ds_comunicacao_w
		from	solic_compra
		where	nr_solic_compra = nr_solic_compra_p;

		
		select	Substr(replace_macro(ds_titulo_w,'@nr_documento',nr_solic_compra_p),1,2000)
		into	ds_titulo_w
		from	dual;

						
		select	cd_estabelecimento
		into	cd_estabelecimento_w
		from	solic_compra
		where	nr_solic_compra = nr_solic_compra_p;

		select	obter_classif_comunic('F')
		into	nr_seq_classif_w
		from	dual;

		select	comunic_interna_seq.nextval
		into	nr_sequencia_w
		from	dual;

		if	(cd_perfil_w is not null) then
			cd_perfil_w := cd_perfil_w ||',';
		end if;

		
		insert	into comunic_interna(
			cd_estab_destino,			dt_comunicado, 			ds_titulo, 
			ds_comunicado, 			nm_usuario,  			dt_atualizacao, 
			ie_geral,				nm_usuario_destino,		nr_sequencia, 
			ie_gerencial, 			nr_seq_classif, 			dt_liberacao,
			ds_perfil_adicional,		ds_setor_adicional)
		values(	cd_estabelecimento_w,		sysdate, 				ds_titulo_w, 
			ds_comunicacao_w, 		WHEB_MENSAGEM_PCK.get_texto(297858), 	sysdate, 
			'N', 				nm_usuario_w,			nr_sequencia_w, 
			'N', 				nr_seq_classif_w, 			sysdate,
			cd_perfil_w,			ds_setor_adicional_w);

		/*Para que a comunica��o seja gerada como lida ao pr�prio usu�rio */
		if	(ie_ci_lida_w = 'S') then
			insert into comunic_interna_lida(nr_sequencia,nm_usuario,dt_atualizacao)values(nr_sequencia_w,nm_usuario_p,sysdate);
		end if;

	end if;
	end;
end loop;
close C01;

	
select	nvl(max(ie_empenho_orcamento),'N')
into	ie_empenho_orcamento_w
from	parametro_estoque
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_empenho_orcamento_w = 'S') then
	gerar_empenho_solic_compra(nr_solic_compra_p,nm_usuario_p);
end if;

	
gerar_historico_solic_compra(	nr_solic_compra_p,
			WHEB_MENSAGEM_PCK.get_texto(297835),
			ds_historico_w,
			'B',
			nm_usuario_p,
			ie_commit_p);

if	(nvl(ie_commit_p,'S') = 'S') then
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end if;

end baixar_solic_compra;
/