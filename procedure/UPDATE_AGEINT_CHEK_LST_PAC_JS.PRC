create or replace
procedure update_ageint_chek_lst_pac_js(
		nr_seq_ageint_p			number,
		nr_seq_proc_interno_p		number,
		cd_estabelecimento_p		number,
		nm_usuario_p			varchar2) is

begin
	update	ageint_check_list_paciente
	set	dt_liberacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia in (select  distinct nr_seq_checklist
				from	ageint_check_list_pac_item_v
				where	nr_seq_ageint = nr_seq_ageint_p
				and	obter_se_contido(nr_seq_grupo,ageint_obter_grupos_regra_proc(nr_seq_proc_interno_p,nr_seq_ageint_p,cd_estabelecimento_p)) = 'S')
	and	dt_liberacao is null;
commit;
end update_ageint_chek_lst_pac_js;
/
