create or replace procedure sup_enviar_mensagens_pend(
   ie_tipo_mensagem_p  number,
   cd_estabelecimento_p  number,
   nm_usuario_p varchar2 default null) is

ie_tipo_mensagem_w  number(10);

cursor c01 is
select ie_tipo_mensagem
from mensagens_pendentes_compras_v
where cd_estabelecimento = cd_estabelecimento_p
and (ie_tipo_mensagem = ie_tipo_mensagem_p or ie_tipo_mensagem_p = 0)
group by ie_tipo_mensagem;

begin

open c01;
loop
fetch c01 into
 ie_tipo_mensagem_w;
exit when c01%notfound;
 begin

 envia_email_compra_agrupado(ie_tipo_mensagem_w, 'S', cd_estabelecimento_p,nm_usuario_p);

 end;
end loop;
close c01;

end sup_enviar_mensagens_pend; 
 /