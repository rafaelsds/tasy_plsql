create or replace 
procedure gerar_eis_conta_pend_controle(        ie_status_consulta_p    varchar2) is

begin
        
if(     ie_status_consulta_p = 'I') then

        begin
        delete  from eis_conta_pend_controle
        where   nm_usuario = wheb_usuario_pck.get_nm_usuario
        or      nm_usuario_nrec = wheb_usuario_pck.get_nm_usuario;
        commit;
        end;
        
        begin
        insert into eis_conta_pend_controle(    nr_sequencia,
                                                dt_atualizacao,
                                                dt_atualizacao_nrec,
                                                nm_usuario,
                                                nm_usuario_nrec,
                                                dt_inicio_consulta,
                                                dt_final_consulta,
                                                ie_status_consulta)
                                        values( eis_conta_pend_controle_seq.nextval,
                                                sysdate,
                                                sysdate,
                                                wheb_usuario_pck.get_nm_usuario,
                                                wheb_usuario_pck.get_nm_usuario,
                                                sysdate,
                                                null,
                                                ie_status_consulta_p);
        commit;
        end;

elsif(  ie_status_consulta_p = 'F') then

        begin
        update  eis_conta_pend_controle
        set     dt_final_consulta = sysdate,
                ie_status_consulta = ie_status_consulta_p
        where   nm_usuario = wheb_usuario_pck.get_nm_usuario
        or      nm_usuario_nrec = wheb_usuario_pck.get_nm_usuario;
        commit;
        end;

end if;

end gerar_eis_conta_pend_controle;
/
