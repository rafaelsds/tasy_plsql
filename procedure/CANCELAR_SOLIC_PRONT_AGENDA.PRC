create or replace
procedure cancelar_solic_pront_agenda	(nr_seq_agenda_p	number,
						nm_usuario_p		varchar2) is

nr_seq_solic_w	number(10,0);

begin
if	(nr_seq_agenda_p is not null) then
	/* obter solicitação */
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_solic_w
	from	same_cpi_solic
	where	nr_seq_agenda = nr_seq_agenda_p;

	/* cancelar solicitação */
	if	(nvl(nr_seq_solic_w,0) > 0) then
		update	same_cpi_solic
		set	ie_status_solic	= 'C',
			dt_cancelamento	= sysdate,
			nm_usuario_cancel	= nm_usuario_p
		where	nr_sequencia		= nr_seq_solic_w
		and	ie_status_solic	= 'A';
	end if;	
end if;

end cancelar_solic_pront_agenda;
/