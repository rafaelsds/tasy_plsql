create or replace
procedure sac_gerar_img_grafico_pesquisa(
			nm_imagem_p	varchar2,
			ds_pergunta_p	varchar2,
			nm_usuario_p	varchar2) is
			
qt_reg_w		number(10);

begin

if	(nvl(nm_imagem_p,'X') = 'X') then
	exec_sql_dinamico_bv('Tasy','delete from w_sac_pesquisa_grafico where nm_usuario = :nm_usuario_p','nm_usuario_p=' || nm_usuario_p || obter_separador_bv);
else
	begin
	select	count(1)
	into	qt_reg_w
	from	user_tables
	where	table_name = 'W_SAC_PESQUISA_GRAFICO';
	
	if	(qt_reg_w = 0) then
		exec_sql_dinamico('W_SAC_PESQUISA_GRAFICO','CREATE TABLE W_SAC_PESQUISA_GRAFICO(NR_SEQUENCIA NUMBER(10), NM_IMAGEM VARCHAR2(255), DS_PERGUNTA VARCHAR2(255), NM_USUARIO VARCHAR2(15))');
	end if;
	
	obter_valor_dinamico('select nvl(max(nr_sequencia),0) +1 from w_sac_pesquisa_grafico', qt_reg_w);
	
	exec_sql_dinamico_bv('',
		'insert into w_sac_pesquisa_grafico(nr_sequencia, nm_imagem, ds_pergunta, nm_usuario) values (:nr_sequencia_p,:nm_imagem_p,:ds_pergunta_p,:nm_usuario_p)',
			'nr_sequencia_p=' || qt_reg_w || obter_separador_bv || 
			'nm_imagem_p=' || nm_imagem_p || obter_separador_bv ||
			'ds_pergunta_p=' || ds_pergunta_p || obter_separador_bv ||
			'nm_usuario_p=' || nm_usuario_p || obter_separador_bv);
	end;
end if;

commit;

end sac_gerar_img_grafico_pesquisa;
/