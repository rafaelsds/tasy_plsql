create or replace
procedure san_liberar_ocorrencia	(nr_seq_ocorrencia_p	number,
					nm_usuario_p		varchar2) is 

begin
if	(nr_seq_ocorrencia_p is not null) then

	update	san_ocorrencia_doador
	set	dt_liberacao		= sysdate,
		nm_usuario_lib		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_ocorrencia_p;

end if;

commit;

end san_liberar_ocorrencia;
/