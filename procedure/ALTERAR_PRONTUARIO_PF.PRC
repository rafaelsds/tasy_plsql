create or replace
procedure alterar_prontuario_pf(nr_prontuario_antigo_p		number,
				nr_prontuario_novo_p		number,
				nr_digito_pront_novo_p		varchar2,
				nr_digito_pront_antigo_p	varchar2,
				cd_estabelecimento_p		number,
				cd_pessoa_fisica_p		varchar2,
				nm_usuario_p			varchar2) is

vl_retorno_w		number(10);
ie_tab_hsl_lantis_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ds_erro_w		varchar2(500);
ie_regra_pront_w	varchar2(15);
qt_registro_w		number(10,0);
ie_atualiza_ult_pront_w	varchar2(1);
nr_ult_prontuario_w regra_prontuario_estab.nr_ult_prontuario%type;



begin

/* Verifica se os numeros nao sao iguais */
if	((nr_prontuario_antigo_p || nr_digito_pront_antigo_p) = (nr_prontuario_novo_p || nr_digito_pront_novo_p)) then
	wheb_mensagem_pck.exibir_mensagem_abort(256025);
end if;

select	nvl(max(vl_parametro),'BASE')
into	ie_regra_pront_w
from	funcao_parametro
where	cd_funcao	= 0
and	nr_sequencia	= 120;

if	(ie_regra_pront_w = 'BASE') then
	begin
	/* Verifica se ja nao tem ninguem com o novo namero de prontuario */
	select	count(*)
	into	vl_retorno_w
	from	pessoa_fisica
	where	nr_prontuario		= nr_prontuario_novo_p
	and	nvl(nr_pront_dv,'0')	= nvl(nr_digito_pront_novo_p, '0');

	if	(vl_retorno_w > 0) then
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	pessoa_fisica
		where	nr_prontuario 		= nr_prontuario_novo_p
		and	nvl(nr_pront_dv,'0')	= nvl(nr_digito_pront_novo_p, '0');
		wheb_mensagem_pck.exibir_mensagem_abort(256026, 'NR_PRONTUARIO_NOVO_P=' || nr_prontuario_novo_p || ';CD_PESSOA_FISICA_W=' || cd_pessoa_fisica_w);
	end if;

	/* Verifica se o banco possui a tabela HSL_LANTIS */
	select	count(*)
	into	ie_tab_hsl_lantis_w
	from	tab
	where	UPPER(tname) 		= 'HSL_LANTIS';

	begin

	/*lock table prontuario_controle 		in exclusive mode;*/

	/* Desabilita as constraints de integridade */
	/*Obter_Valor_Dinamico('alter table PRONTUARIO_CONTROLE disable constraint PROCONT_PESFISI_FK2', vl_retorno_w);*/


	/* Atualiza as tabelas */
	if	(ie_tab_hsl_lantis_w > 0) then
		Obter_Valor_Dinamico('update hsl_lantis set nr_prontuario = ' || nr_prontuario_novo_p ||' where nr_prontuario = ' || nr_prontuario_antigo_p, vl_retorno_w);
	end if;

	update	prontuario_controle 
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	same_cpi_solic
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	same_cpi_emp
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	same_cpi_prontuario
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	can_ficha_admissao
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	can_ficha_seguimento
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	paciente_prontuario_anual
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	w_interf_conta_cab
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	w_susaih_interf_conta
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	pessoa_fisica_duplic
	set 	nr_prontuario		= nr_prontuario_novo_p
	where	nr_prontuario		= nr_prontuario_antigo_p;

	update	pessoa_fisica
	set 	nr_prontuario		= nr_prontuario_novo_p,
			nr_pront_dv		= nr_digito_pront_novo_p,
			dt_atualizacao =   sysdate,
			nm_usuario = 	substr(wheb_usuario_pck.get_nm_usuario,1,15)
	where	cd_pessoa_fisica		= cd_pessoa_fisica_p;

	/* Habilita as constraints de integridade */
	/*Obter_Valor_Dinamico('alter table PRONTUARIO_CONTROLE enable constraint PROCONT_PESFISI_FK2', vl_retorno_w);*/
		
	/*insert into logxxxxx_tasy	(	dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
		values		(	sysdate,
					wheb_usuario_pck.get_nm_usuario,
					57777,
					'Pessoa Fisica: ' || cd_pessoa_fisica_p || chr(13) ||
					'Prontuario Antigo: ' || nr_prontuario_antigo_p || chr(13) ||
					'Prontuario Novo: ' || nr_prontuario_novo_p || chr(13) ||
					'Digito Antigo: ' || nr_digito_pront_antigo_p || chr(13) ||
					'Digito Novo: ' || nr_digito_pront_novo_p);*/

	exception when others then
		ds_erro_w	:= sqlerrm(sqlcode);
		wheb_mensagem_pck.exibir_mensagem_abort(256027, 'DS_ERRO_W=' || ds_erro_w);
	end;
	end;
elsif	(ie_regra_pront_w = 'ESTAB') then
	begin
	/*Verificar se no estabelecimento ja possui pf com novo prontuario */
	/*Senao tiver gera o prontuario por pessoa */
	/*Atualiza a pessoa*/
	/*Atualiza o ultimo valor do estabelecimento. ?*/
	ie_atualiza_ult_pront_w:= 'N';
	Obter_Param_Usuario(5, 184, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_ult_pront_w);
	
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	pessoa_fisica_pront_estab
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	nr_prontuario		= nr_prontuario_novo_p;
	
	if	(nvl(cd_pessoa_fisica_w,'X') <> 'X') then	
		wheb_mensagem_pck.exibir_mensagem_abort(256026, 'NR_PRONTUARIO_NOVO_P=' || nr_prontuario_novo_p || ';CD_PESSOA_FISICA_W=' || cd_pessoa_fisica_w);
	end if;
	
	
	if	(ie_atualiza_ult_pront_w = 'S') then -- OS 362761
	
		select	count(*)
		into	qt_registro_w
		from	regra_prontuario_estab
		where	cd_estabelecimento	= cd_estabelecimento_p;
		
		select max(nr_ult_prontuario)
		into	nr_ult_prontuario_w
		from	regra_prontuario_estab
		where	cd_estabelecimento	= cd_estabelecimento_p;
		
		if	(qt_registro_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(256028, 'CD_ESTABELECIMENTO_P=' || cd_estabelecimento_p);
		end if;
		
		if(nr_ult_prontuario_w < nr_prontuario_novo_p)then
		
			lock table regra_prontuario_estab in exclusive mode;
		
			update	regra_prontuario_estab
			set	nr_ult_prontuario	= nr_prontuario_novo_p
			where	cd_estabelecimento	= cd_estabelecimento_p;
		
		end if;
		
	end if;
	
	begin
	update	pessoa_fisica_pront_estab
	set	nr_prontuario		= nr_prontuario_novo_p,
			dt_atualizacao		= sysdate,
			nm_usuario			= 	substr(wheb_usuario_pck.get_nm_usuario,1,15)
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_pessoa_fisica		= cd_pessoa_fisica_p;
	exception when others then
		ds_erro_w	:= sqlerrm(sqlcode);
		wheb_mensagem_pck.exibir_mensagem_abort(256029, 'DS_ERRO_W=' || ds_erro_w);
	end;
	end;
end if;

commit;

end alterar_prontuario_pf;
/
