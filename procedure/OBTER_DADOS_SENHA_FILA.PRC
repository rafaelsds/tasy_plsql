create or replace
procedure obter_dados_senha_fila(
			        nr_atendimento_p	number,
				dt_senha_p		out date,
				ds_senha_p		out varchar2) is 

nr_seq_pac_senha_fila_w	number(10);
ie_tipo_data_w		varchar2(1);
				
begin

if	(nvl(nr_atendimento_p,0) > 0) then

	select	nvl(max(nr_seq_pac_senha_fila),0)
	into	nr_seq_pac_senha_fila_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	if	(nr_seq_pac_senha_fila_w > 0) then
	
		obter_param_usuario(10021,47,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_tipo_data_w);
	
		select	substr(obter_letra_verifacao_senha(nr_seq_fila_senha) || cd_senha_gerada,1,20),
			decode(ie_tipo_data_w,'G',dt_geracao_senha,dt_primeira_chamada)    
		into	ds_senha_p,
			dt_senha_p
		from	paciente_senha_fila
		where	nr_sequencia	= nr_seq_pac_senha_fila_w;
	
	end if;
	
end if;

commit;

end obter_dados_senha_fila;
/
