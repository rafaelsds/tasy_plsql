create or replace
procedure suporte_aprovar_comunicado(	nr_seq_comunicado_p	number,
					ie_operacao_p		varchar2,
					nm_usuario_p		varchar2) IS

BEGIN

if	(ie_operacao_p = 'A') then

	update	suporte_comunicado
	set	dt_aprovacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_comunicado_p;

elsif	(ie_operacao_p = 'D') then

	update	suporte_comunicado
	set	dt_aprovacao	= null,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_comunicado_p;
end if;

commit;

END suporte_aprovar_comunicado;
/