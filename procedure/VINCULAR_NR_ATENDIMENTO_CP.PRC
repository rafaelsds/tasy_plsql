CREATE OR REPLACE PROCEDURE VINCULAR_NR_ATENDIMENTO_CP(
    NR_ATENDIMENTO_P    ATENDIMENTO_PACIENTE.NR_ATENDIMENTO%TYPE, 
    CP_PESSOA_FISICA_P  ATENDIMENTO_PACIENTE.CD_PESSOA_FISICA%TYPE
) AS
BEGIN
    UPDATE 
        PE_PRESCRICAO b 
    SET b.NR_ATENDIMENTO = NR_ATENDIMENTO_P,
	b.DT_PRESCRICAO = OBTER_DATA_ENTRADA(NR_ATENDIMENTO_P)
    WHERE 
        b.NR_SEQUENCIA IN ( SELECT 
                                a.nr_sequencia
                            FROM PE_PRESCRICAO a 
                            WHERE 
                                a.DT_LIBERACAO IS NULL AND 
                                a.DT_INATIVACAO is null AND 
                                CD_PESSOA_FISICA = CP_PESSOA_FISICA_P AND 
                                nvl(wheb_assist_pck.get_nivel_atencao_perfil, 'T') = a.ie_nivel_atencao AND
                                a.NR_ATENDIMENTO IS NULL);
END VINCULAR_NR_ATENDIMENTO_CP;
/