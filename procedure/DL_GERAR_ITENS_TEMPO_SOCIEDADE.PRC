create or replace
procedure dl_gerar_itens_tempo_sociedade(
			nr_seq_distribuicao_p	number,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number,
			vl_distribuicao_p	number) is 

nr_seq_socio_w		number(10);
nr_seq_lote_w		number(10);
nr_seq_tipo_distrib_w	number(10);
nr_seq_distrib_item_w	number(10);						
qt_anos_serv_socio_w	number(10); 
vl_distribuicao_w	number(15,4);
vl_por_ano_w		number(15,4);
vl_pagar_socio_w	number(15,4);
tx_criterio_w		number(15,6);
ds_msg_tp_distrib_w	varchar2(255);
			
begin

select	a.nr_seq_socio,
	a.nr_seq_lote
into	nr_seq_socio_w,
	nr_seq_lote_w
from 	dl_distribuicao a
where	a.nr_sequencia	= nr_seq_distribuicao_p;

if	(dl_obter_dados_socio(nr_seq_socio_w,'T') = 'A') then

	qt_anos_serv_socio_w	:= 0; 
	vl_distribuicao_w		:= 0; 
	vl_por_ano_w		:= 0; 
	vl_pagar_socio_w		:= 0; 
	tx_criterio_w		:= 0; 

	select	(to_char(nvl(a.dt_saida,sysdate), 'yyyy') - to_char(a.dt_entrada, 'yyyy'))
	into	qt_anos_serv_socio_w
	from	dl_socio a
	where	a.nr_sequencia		= nr_seq_socio_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p;

	select	nvl(nvl(vl_distribuicao_p,a.vl_distribuicao),0)
	into	vl_distribuicao_w
	from	dl_lote_distribuicao a
	where	a.nr_sequencia	= nr_seq_lote_w;

	/*select	max(nvl(a.tx_criterio ,0))
	into	tx_criterio_w
	from 	dl_socio_criterio a 
	where 	qt_anos_serv_socio_w between a.qt_ano_inicial and a.qt_ano_final;*/
	
	/* dominio DL - Tipo de item para calculo da distribuicao / Tempo de sociedade */
	select	max(a.nr_sequencia),
		max(nvl(a.tx_item,0))
	into	nr_seq_tipo_distrib_w,
		tx_criterio_w
	from	dl_tipo_item a
	where	a.ie_tipo_item		= 2
	and	a.nr_seq_socio		= nr_seq_socio_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p;
	
	if	(nr_seq_tipo_distrib_w is null) then
		select	max(a.nr_sequencia),
			max(nvl(a.tx_item,0))
		into	nr_seq_tipo_distrib_w,
			tx_criterio_w
		from	dl_tipo_item a
		where	a.ie_tipo_item		= 2
		and	a.nr_seq_socio is null
		and	a.cd_estabelecimento	= cd_estabelecimento_p;
	end if;	

	if	(nr_seq_tipo_distrib_w is null) then -- afstringari 236244 28/07/2010

		ds_msg_tp_distrib_w	:= substr(dl_obter_msg_tp_distrib(1,2),1,255);

		/* ds_msg_tp_distrib_w */
		wheb_mensagem_pck.exibir_mensagem_abort(262112,'DS_MSG_TP_DISTRIB_W='||ds_msg_tp_distrib_w);

	end if;	

	vl_por_ano_w		:= nvl(tx_criterio_w,0) * vl_distribuicao_w;
	vl_pagar_socio_w	:= vl_por_ano_w	* qt_anos_serv_socio_w;
	
	select	max(nvl(b.nr_sequencia,0))
	into	nr_seq_distrib_item_w
	from	dl_distribuicao a,
		dl_distribuicao_item b		
	where	a.nr_sequencia	= b.nr_seq_distribuicao
	and	a.nr_seq_lote	= nr_seq_lote_w
	and	a.nr_sequencia	= nr_seq_distribuicao_p
	and	b.nr_seq_item	= nr_seq_tipo_distrib_w;	
	
	if	(nr_seq_distrib_item_w is null) and (vl_distribuicao_p is null) then
		insert into dl_distribuicao_item (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_distribuicao,
			nr_seq_item,
			tx_item,
			vl_item,
			qt_item,
			vl_calculado)
		values( dl_distribuicao_item_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_distribuicao_p,
			nr_seq_tipo_distrib_w,
			nvl(tx_criterio_w,0),
			0,
			qt_anos_serv_socio_w,
			vl_pagar_socio_w);	
	else
		update 	dl_distribuicao_item
		set	dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			vl_item			= vl_pagar_socio_w
		where	nr_sequencia		= nr_seq_distrib_item_w
		and	nr_seq_distribuicao	= nr_seq_distribuicao_p;
	end if;	
end if;

end dl_gerar_itens_tempo_sociedade;
/