create or replace 
procedure gerar_d301_segmento_EBG(	nr_seq_dataset_p	number,
					nm_usuario_p		varchar2) is 

  cursor c01 is
select 	na.DT_NASCIMENTO as DS_DT_PARTO
from 	d301_dataset_envio   d3,
	atendimento_alta     aa,
	atendimento_paciente ap,
	nascimento           na
where 	d3.nr_sequencia   = nr_seq_dataset_p
and 	d3.nr_atendimento = aa.nr_atendimento
and 	aa.nr_atendimento = ap.nr_atendimento
and 	ap.nr_atendimento = na.nr_atendimento;

c01_w c01%rowtype;        

begin

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;
  
	insert into D301_SEGMENTO_EBG
		(NR_SEQUENCIA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		NR_SEQ_DATASET,
		DS_DT_PARTO) 
	values 	(D301_SEGMENTO_EBG_seq.Nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_dataset_p,
		c01_w.DS_DT_PARTO);
end loop;
close C01;

end gerar_d301_segmento_EBG;
/
