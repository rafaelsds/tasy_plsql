create or Replace
PROCEDURE Inserir_diluentes_prot(
			nr_prescricao_p		number,
			cd_protocolo_p		number,
			nr_sequencia_p		number,
			nr_seq_diluicao_p	number,
			nr_seq_diluicao_novo_p	number,
            nm_usuario_p		varchar2) IS

cd_estabelecimento_w		Number(4);
dt_primeiro_horario_w		Date;
qt_conversao_dose_w	   		Number(18,6);
nr_horas_validade_w			number(5);
dt_prescricao_w				Date;
nr_ocorrencia_w				Number(15,4);
qt_unitaria_w   			Number(18,6);
ie_origem_inf_w				Varchar2(1) := 'P';
qt_material_w   			Number(15,3);
qt_dispensar_w				Number(18,6);
ds_erro_w					Varchar2(255);
nr_seq_mat_prescr_w			number(6);
cd_material_w				Number(6,0);
nr_seq_material_ww			Number(10,0);
nr_seq_material_www			Number(10,0);
cd_unidade_medida_w			Varchar2(30);
qt_dose_w					Number(18,6);
ie_via_aplicacao_w			Varchar2(5);
nr_agrupamento_w			Number(07,1);
ie_agrupador_w				Number(2);
ds_recomendacao_w			Varchar2(2000);
nr_seq_diluicao_w			Number(10,0);
cd_intervalo_w				Varchar2(7);
ds_horarios_w				Varchar2(2000);
ds_horarios2_w				Varchar2(2000);
cd_unid_med_consumo_w		Varchar2(30);
qt_minimo_multiplo_solic_w	Number(18,6);
ie_se_necessario_w			Varchar2(01);
qt_minuto_aplicacao_w		Number(04,0);
qt_hora_aplicacao_w			Number(03,0);
ie_bomba_infusao_w			Varchar2(01);
ie_aplic_bolus_w			Varchar2(01);
ie_aplic_lenta_w			Varchar2(01);
ie_acm_w					Varchar2(01);
qt_solucao_w				Number(15,4);
ds_dose_diferenciada_w		Varchar2(50);
hr_prim_horario_w			Varchar2(5);
ie_agora_medic_w			Varchar2(1);
ie_recons_diluente_fixo_w	Varchar2(1);
nr_seq_material_w			Number(10,0);
cd_setor_atendimento_w		Number(10,0);
qt_referencia_w				Number(18,6);
ie_proporcao_w				varchar2(15);
ie_regra_disp_w				varchar2(1); /* Rafael em 15/3/8 OS86206 */
cd_material_medic_w			Number(6,0);
cd_UM_medic_w				Varchar2(30);
qt_dose_medic_w				Number(18,6);
qt_unitaria_medic_w			Number(18,6);
qt_dose_aux_w				Number(18,6);
qt_referencia_aux_w			Number(18,6);
ie_loop_w					Varchar2(1);
qt_itens_w					Number(10,0);
ie_mesmo_zerado_w			varchar2(1);
nr_atendimento_w			number(10);
ie_via_aplicacao_ww			protocolo_medic_material.ie_via_aplicacao%type;

CURSOR C01 IS
	select	a.cd_material,
		a.nr_seq_material,
		a.nr_seq_material + nr_seq_diluicao_novo_p,
		a.cd_unidade_medida,
		a.qt_dose,
		a.ie_via_aplicacao,
		a.nr_agrupamento,
		a.ds_recomendacao,
		a.nr_seq_diluicao,
		decode(obter_classif_material_proced(a.cd_material, null,null), '1', 2, a.ie_agrupador) ie_agrupador,
		nvl(a.cd_intervalo,obter_interv_prescr_padrao(cd_estabelecimento_w)),
		a.ds_horarios,
	 	substr(obter_dados_material_estab(a.cd_material,cd_estabelecimento_w,'UMS'),1,30) cd_unidade_medida_consumo,
		b.qt_minimo_multiplo_solic,
		a.qt_minuto_aplicacao,
		a.qt_hora_aplicacao,
		nvl(a.ie_bomba_infusao,'N'),
		nvl(a.ie_aplic_bolus,'N'),
		nvl(a.ie_aplic_lenta,'N'),
		a.qt_solucao,
		a.ds_dose_diferenciada,
		a.hr_prim_horario,
		a.ie_urgencia,
		decode(a.cd_intervalo,null,'N',nvl(a.ie_intervalo_fixo,'N')),
		a.qt_referencia,
		a.ie_proporcao,
		x.cd_material,
		x.cd_unidade_medida,
		x.qt_dose,
		x.Qt_unitaria,
		nvl(x.ie_se_necessario,'N'),
		nvl(x.ie_acm,'N')
	from 	material b,
		Protocolo_medic_material a,
		prescr_material x
	where	a.cd_protocolo	 	= cd_protocolo_p
	and	a.nr_sequencia		= nr_sequencia_p
	and	a.cd_material 		= b.cd_material
	and	x.nr_prescricao = nr_prescricao_p
	and	x.nr_sequencia 	= nr_seq_diluicao_novo_p
	and	decode(obter_classif_material_proced(a.cd_material, null,null), '1', 2, a.ie_agrupador)	in (3,7,9)
	and	a.nr_seq_diluicao	= nr_seq_diluicao_p		
	order by nr_seq_material;

BEGIN

select	max(cd_estabelecimento),
		max(nr_horas_validade),
		max(dt_prescricao),
		max(dt_primeiro_horario),
		max(cd_setor_atendimento),
		max(nr_atendimento)
into		cd_estabelecimento_w,
		nr_horas_validade_w,
		dt_prescricao_w,
		dt_primeiro_horario_w,
		cd_setor_atendimento_w,
		nr_atendimento_w
from		prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	nvl(max(nr_sequencia),0)
into		nr_seq_mat_prescr_w
from		prescr_material
where	nr_prescricao	= nr_prescricao_p;

select	nvl(max(ie_se_necessario),'N'),
		nvl(max(ie_acm),'N'),
		max(ie_via_aplicacao)
into	ie_se_necessario_w,
		ie_acm_w,
		ie_via_aplicacao_ww
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and		nr_sequencia	= nr_seq_diluicao_novo_p;

OPEN C01;
LOOP
FETCH C01 into
	cd_material_w,
	nr_seq_material_w,
	nr_seq_material_ww,
	cd_unidade_medida_w,
	qt_dose_w,
	ie_via_aplicacao_w,
	nr_agrupamento_w,
	ds_recomendacao_w,
	nr_seq_diluicao_w,
	ie_agrupador_w,
	cd_intervalo_w,
	ds_horarios_w,
	cd_unid_med_consumo_w,
	qt_minimo_multiplo_solic_w,
	qt_minuto_aplicacao_w,
	qt_hora_aplicacao_w,
	ie_bomba_infusao_w,
	ie_aplic_bolus_w,
	ie_aplic_lenta_w,
	qt_solucao_w,
	ds_dose_diferenciada_w,
	hr_prim_horario_w,
	ie_agora_medic_w,
	ie_recons_diluente_fixo_w,
	qt_referencia_w,
	ie_proporcao_w,
	cd_material_medic_w,
	cd_UM_medic_w,
	qt_dose_medic_w,
	qt_unitaria_medic_w,
	ie_se_necessario_w,
	ie_acm_w;
exit when C01%notfound;
begin

	if	(ie_agora_medic_w = 'S') then
		dt_primeiro_horario_w := Sysdate;
		select	nvl(max(cd_intervalo),cd_intervalo_w)
		into	cd_intervalo_w
		from	intervalo_prescricao
		where	ie_agora	= 'S';
	end if;
	
	if	(cd_unid_med_consumo_w = cd_unidade_medida_w) then
		qt_conversao_dose_w	:= 1;
	else
		begin
		select	nvl(max(qt_conversao),1)
		into	qt_conversao_dose_w
		from	material_conversao_unidade
		where	cd_material		= cd_material_w
 		and	cd_unidade_medida	= cd_unidade_medida_w;
		exception
			when others then
				qt_conversao_dose_w	:= 1;
		end;
	end if;

	nr_ocorrencia_w		:= 0;
	
	Calcular_Horario_Prescricao
		(nr_prescricao_p, cd_intervalo_w,
		nvl(dt_primeiro_horario_w,dt_prescricao_w),
		dt_primeiro_horario_w,
		nr_horas_validade_w,
		cd_material_w,0,0,
		nr_ocorrencia_w,
		ds_horarios_w,
		ds_horarios2_w,
		'N', null);

	ds_horarios_w	:= ds_horarios_w || ds_horarios2_w;

	if	(ie_acm_w = 'S') or 
		(ie_se_necessario_w = 'S') then
		select	nvl(max(b.qt_se_necessario),0),
				nvl(max(b.ie_mesmo_zerado),'N')
		into		nr_ocorrencia_w,
				ie_mesmo_zerado_w
		from		intervalo_prescricao a,
				intervalo_setor b
		where	a.cd_intervalo 		= b.cd_intervalo
		and		nvl(b.cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
		and		nvl(b.cd_estab, cd_estabelecimento_w)			= cd_estabelecimento_w
		and		a.cd_intervalo 		= cd_intervalo_w
		and		((b.cd_material = cd_material_w) or 
				 (b.cd_material is null))
		and		((b.ie_via_aplicacao = nvl(ie_via_aplicacao_w,b.ie_via_aplicacao)) or
				 (b.ie_via_aplicacao is null))
		and		((b.cd_unidade_basica = nvl(obter_unid_atend_setor_atual(nr_atendimento_w,cd_setor_atendimento_w,'UB'),b.cd_unidade_basica)) or
				 (b.cd_unidade_basica is null));
		
		if	(nvl(nr_ocorrencia_w,0) = 0) and
			(ie_mesmo_zerado_w = 'N') then
			Select	nvl(max(qt_se_necessario),1) 
			into	nr_ocorrencia_w
			from	intervalo_prescricao
			where	cd_intervalo = cd_intervalo_w;
		end if;
	end if;

	if	(nvl(qt_conversao_dose_w,0) = 0) then
		qt_conversao_dose_w	:= 1;
	end if;
	
	qt_unitaria_w := (trunc(qt_dose_w * 1000 / qt_conversao_dose_w)/ 1000);	
	 
	if	(nvl(qt_referencia_w,0) > 0) then
		qt_referencia_aux_w := qt_referencia_w;
		qt_referencia_w	:= obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida_w,qt_referencia_w);
		if	(ie_proporcao_w = 'SC') then
			qt_material_w	:= qt_unitaria_medic_w * qt_referencia_w;
			qt_unitaria_w	:= qt_unitaria_medic_w * qt_referencia_w;
		elsif	(ie_proporcao_w = 'ST') then
			qt_material_w	:= qt_unitaria_medic_w * qt_referencia_w;
			qt_unitaria_w	:= qt_unitaria_medic_w * qt_referencia_w;	
			qt_minuto_aplicacao_w := qt_unitaria_medic_w * qt_minuto_aplicacao_w;
		end if;
	end if;
	
	if	(qt_minuto_aplicacao_w > 0) then
		if	(qt_minuto_aplicacao_w < 60) and
			(nvl(qt_hora_aplicacao_w,0) = 0) then
			qt_hora_aplicacao_w	:= null;
		elsif	(qt_minuto_aplicacao_w = 60) then
			qt_hora_aplicacao_w		:= 1 + nvl(qt_hora_aplicacao_w,0);
			qt_minuto_aplicacao_w	:= null;
		else
			qt_hora_aplicacao_w	:= trunc(dividir(qt_minuto_aplicacao_w,60)) + nvl(qt_hora_aplicacao_w,0);		
			qt_minuto_aplicacao_w	:= (qt_minuto_aplicacao_w - (trunc(dividir(qt_minuto_aplicacao_w,60)) * 60));
		end if;
	end if;
	
	if	(qt_minuto_aplicacao_w = 0) then
		qt_minuto_aplicacao_w	:= null;
	end if;
	
	Obter_Quant_Dispensar(1,cd_material_w, nr_prescricao_p, nr_seq_material_ww, cd_intervalo_w,
				ie_via_aplicacao_w, qt_unitaria_w, 0, nr_ocorrencia_w,     
				'', ie_origem_inf_w, cd_unidade_medida_w, 1, qt_material_w, qt_dispensar_w, ie_regra_disp_w, ds_erro_w,ie_se_necessario_w,ie_acm_w);
				
	ie_loop_w	:= 'S';
	nr_seq_material_www := 1;
	while	(ie_loop_w = 'S') loop
		select	count(*)
		into	qt_itens_w
		from	prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_material_www;

		if	(qt_itens_w	= 0) then
			ie_loop_w	:= 'N';
		else	
			nr_seq_material_www	:= nr_seq_material_www + 1;
		end if;
	end loop;

	Insert into Prescr_Material (
		nr_prescricao,
		nr_sequencia,
		ie_origem_inf,
		cd_material,
		cd_unidade_medida,
		qt_dose,
		qt_unitaria,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		cd_intervalo,
		ds_observacao,
		ie_via_aplicacao,
		nr_agrupamento,
		cd_motivo_baixa,
		ie_utiliza_kit,
		cd_unidade_medida_dose,
		qt_conversao_dose,
		ie_urgencia,
		nr_ocorrencia,
		qt_total_dispensar,
		ds_horarios,
		nr_sequencia_diluicao,
		ie_medicacao_paciente,
		ie_suspenso,
		ie_agrupador,
		ie_se_necessario,
		qt_min_aplicacao,
		qt_hora_aplicacao,
		ie_bomba_infusao,
		ie_aplic_bolus,
		ie_aplic_lenta,
		ie_acm,
		ds_dose_diferenciada,
		qt_solucao,
		ie_cultura_cih,
		ie_antibiograma,
		ie_uso_antimicrobiano,
		ie_recons_diluente_fixo,
		cd_protocolo,
		nr_seq_protocolo,
		nr_seq_mat_protocolo,
		ie_regra_disp,
		qt_ref_diluente)
	values (
		nr_prescricao_p,
     		nr_seq_material_www,
	 	'S',
	 	cd_material_w,
	 	cd_unid_med_consumo_w,
	 	nvl(qt_dose_w,0),
	 	nvl(qt_unitaria_w,0),
	 	nvl(qt_material_w,0),
	 	sysdate,
		nm_usuario_p,
	 	cd_intervalo_w,
	 	ds_recomendacao_w ,
	 	nvl(ie_via_aplicacao_w, ie_via_aplicacao_ww),
		nvl(nr_agrupamento_w,0),
		0,
		'N',
		cd_unidade_medida_w,
		nvl(qt_conversao_dose_w,0),
		ie_agora_medic_w,
		nr_ocorrencia_w,
		qt_dispensar_w,
		ds_horarios_w,
		nr_seq_diluicao_novo_p,
		'N',
		'N',
	    	ie_agrupador_w,
		ie_se_necessario_w,
		qt_minuto_aplicacao_w,
		qt_hora_aplicacao_w,
		ie_bomba_infusao_w,
		ie_aplic_bolus_w,
		ie_aplic_lenta_w,
		ie_acm_w,
		ds_dose_diferenciada_w,
		qt_solucao_w,
		'N',
		'N',
		'N',
		ie_recons_diluente_fixo_w,
		cd_protocolo_p,
		nr_sequencia_p,
		nr_seq_material_w,
		ie_regra_disp_w,
		decode(ie_proporcao_w, 'SC',qt_referencia_w, 'ST', qt_referencia_w, null));
		
		Ajustar_Dose_Diluente(nr_prescricao_p, nr_seq_material_www);
		
		if	(nvl(qt_referencia_w,0) > 0) and
			(ie_proporcao_w = 'ST') then
			update	prescr_material
			set	qt_min_aplicacao	= qt_minuto_aplicacao_w,
				qt_hora_aplicacao	= qt_hora_aplicacao_w
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia		= nr_seq_diluicao_novo_p
			and	ie_aplic_bolus		= 'N'
			and	ie_aplic_lenta		= 'N';
		end if;
	end;
END LOOP; 
CLOSE C01;
	

END Inserir_diluentes_prot;
/