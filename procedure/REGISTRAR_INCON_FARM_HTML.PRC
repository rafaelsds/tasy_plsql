create or replace
procedure registrar_incon_farm_html(
						nr_prescricao_p				number,
						nr_seq_cpoe_p				number,
						nr_seq_inconsistencia_p		number,
						ie_tipo_item_p				varchar2,
						ie_opcao_p					varchar2,
						nm_usuario_p				varchar2) is 
						
cd_material_w				cpoe_material.cd_material%type;
ie_controle_tempo_w			cpoe_material.ie_controle_tempo%type;
nr_seq_solucao_w			prescr_material.nr_sequencia_solucao%type;
nr_prescricao_vigente_w		prescr_medica.nr_prescricao%type;

cursor c01 is
select	a.nr_prescricao,
		a.nr_sequencia,
		a.nr_sequencia_solucao
from	prescr_material a
where	a.nr_prescricao >= nr_prescricao_vigente_w
and		a.nr_seq_mat_cpoe = nr_seq_cpoe_p
and		a.cd_material = cd_material_w;

cursor c02 is
select	a.nr_seq_solucao,
		a.nr_prescricao
from	prescr_solucao a
where	a.nr_seq_dialise_cpoe = nr_seq_cpoe_p
and		a.nr_prescricao >= nr_prescricao_vigente_w;

begin

nr_prescricao_vigente_w := gpt_obter_prescricao_vigente(nr_seq_cpoe_p, ie_tipo_item_p);

if (ie_tipo_item_p in ('M', 'SOL')) then

	select 	max(cd_material),
			max(nvl(ie_controle_tempo,'N'))
	into	cd_material_w,
			ie_controle_tempo_w
	from	cpoe_material
	where	nr_sequencia =  nr_seq_cpoe_p;

	for r_c01_w in c01
	loop
		if (ie_controle_tempo_w = 'S') then
			registrar_incons_farm_soluc(	nr_prescricao_p => r_c01_w.nr_prescricao,
											nr_seq_solucao_p => r_c01_w.nr_sequencia_solucao,
											nr_seq_inconsistencia_p => nr_seq_inconsistencia_p,
											ie_opcao_p => ie_opcao_p,
											nm_usuario_p => nm_usuario_p);
		else
			registrar_inconsistencias_farm(	nr_prescricao_p => r_c01_w.nr_prescricao,
											nr_sequencia_p => r_c01_w.nr_sequencia,
											nr_seq_inconsistencia_p => nr_seq_inconsistencia_p,
											ie_opcao_p => ie_opcao_p,
											nm_usuario_p => nm_usuario_p);
		end if;
	end loop;


elsif (ie_tipo_item_p = 'DI') then
	
	for r_c02_w in c02
	loop
		registrar_incons_farm_soluc(nr_prescricao_p => r_c02_w.nr_prescricao,
									nr_seq_solucao_p => r_c02_w.nr_seq_solucao,
									nr_seq_inconsistencia_p => nr_seq_inconsistencia_p,
									ie_opcao_p => ie_opcao_p,
									nm_usuario_p => nm_usuario_p);
	end loop;

end if;


end registrar_incon_farm_html;
/
