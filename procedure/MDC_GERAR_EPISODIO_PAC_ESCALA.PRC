create or replace 
procedure mdc_gerar_episodio_pac_escala(    nr_atendimento_p        mdc_episodio_pac_escala.nr_atendimento%type,
                                            nr_seq_reg_escala_p     mdc_episodio_pac_escala.nr_seq_reg_escala%type,
                                            ie_escala_p             mdc_episodio_pac_escala.ie_escala%type,
                                            nm_usuario_p            mdc_episodio_pac_escala.nm_usuario%type,
                                            ie_inativar_p           varchar2 default 'N') is

qt_regras_w                     number;
cd_estabelecimento_w            mdc_regra_escala_indice.cd_estabelecimento%type;
ie_controle_revisao_w           mdc_regra_escala_indice.ie_controle_revisao%type;
nr_seq_episodio_w               atendimento_paciente.nr_seq_episodio%type;
nr_sequencia_w                  mdc_episodio_pac_escala.nr_sequencia%type;
dt_revisao_w                    mdc_episodio_pac_escala.dt_revisao%type;
nm_usuario_revisao_w            mdc_episodio_pac_escala.nm_usuario_revisao%type;
qt_pontos_w                     mdc_episodio_pac_escala.qt_pontos%type;

BEGIN
--
cd_estabelecimento_w := obter_estab_usuario(nm_usuario_p);
--
select  count(*)
into    qt_regras_w
from    mdc_regra_escala_indice a
where   a.ie_escala =  ie_escala_p
and     a.cd_estabelecimento = cd_estabelecimento_w;
--
qt_pontos_w := mdc_obter_pontuacao_escala(ie_escala_p, nr_seq_reg_escala_p);
--
if(qt_regras_w > 0)then

    if(ie_inativar_p='N')then
        select  ie_controle_revisao
        into    ie_controle_revisao_w
        from    mdc_regra_escala_indice a
        where   a.ie_escala = ie_escala_p
        and     a.cd_estabelecimento = cd_estabelecimento_w;
        --
        select  nr_seq_episodio
        into    nr_seq_episodio_w
        from    atendimento_paciente
        where   nr_atendimento = nr_atendimento_p;
        --
        select  mdc_episodio_pac_escala_seq.nextval
        into    nr_sequencia_w
        from    dual;
        --    
        if(ie_controle_revisao_w <> 'S') then
            dt_revisao_w := sysdate;
            nm_usuario_revisao_w := nm_usuario_p;
        end if;
        -- 
        insert into mdc_episodio_pac_escala(    nr_sequencia,
                                                dt_atualizacao,
                                                nm_usuario,
                                                dt_atualizacao_nrec,
                                                nm_usuario_nrec,
                                                nr_seq_episodio,
                                                nr_atendimento,
                                                nr_seq_reg_escala,
                                                ie_escala,
                                                qt_pontos,
                                                dt_revisao,
                                                nm_usuario_revisao)
        values (    nr_sequencia_w,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    nr_seq_episodio_w,
                    nr_atendimento_p,
                    nr_seq_reg_escala_p,
                    ie_escala_p,
                    qt_pontos_w,
                    dt_revisao_w,
                    nm_usuario_revisao_w);
    else
        update  mdc_episodio_pac_escala
        set     dt_inativacao = sysdate,
                nm_usuario_inativacao = nm_usuario_p
        where   nr_seq_reg_escala = nr_seq_reg_escala_p
        and     ie_escala = ie_escala_p;
    end if;
    
end if;

END mdc_gerar_episodio_pac_escala;
/
