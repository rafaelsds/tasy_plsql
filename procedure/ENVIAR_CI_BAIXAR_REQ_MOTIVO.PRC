create or replace
procedure enviar_ci_baixar_req_motivo(	nr_requisicao_p			number,
				cd_motivo_baixa_p			number,
				nm_usuario_p			varchar2) is

ds_motivo_baixa_w				varchar2(255);
nm_usuario_destino_w			varchar2(15);
ds_titulo_w				varchar2(80);
ds_comunicacao_w				varchar2(2000);
nr_seq_classif_w				number(10);

begin

select	substr(obter_desc_motivo_baixa_req(cd_motivo_baixa_p),1,80)
into	ds_motivo_baixa_w
from	dual;

select	obter_usuario_pessoa(cd_pessoa_requisitante)
into	nm_usuario_destino_w
from	requisicao_material
where	nr_requisicao = nr_requisicao_p;

ds_titulo_w		:= Wheb_mensagem_pck.get_Texto(300264); /*'Baixa da requisi��o por motivo';*/
ds_comunicacao_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(300267,'NR_REQUISICAO_P='|| NR_REQUISICAO_P ||';NM_USUARIO_P='|| NM_USUARIO_P ||';DS_MOTIVO_BAIXA_W='|| DS_MOTIVO_BAIXA_W),1,2000);
			/*A requisi��o de materiais e medicamentos n�mero #@NR_REQUISICAO_P#@, foi baixada pelo usu�rio #@NM_USUARIO_P#@.
			Motivo: #@DS_MOTIVO_BAIXA_W#@.*/

if	(nm_usuario_destino_w is not null) then
	
	select	max(obter_classif_comunic('F'))
	into	nr_seq_classif_w
	from	dual;

	Gerar_Comunic_Padrao(sysdate,ds_titulo_w,ds_comunicacao_w,nm_usuario_p,'N',nm_usuario_destino_w,'N',nr_seq_classif_w,'','','',sysdate,'','');
end if;

end enviar_ci_baixar_req_motivo;
/
