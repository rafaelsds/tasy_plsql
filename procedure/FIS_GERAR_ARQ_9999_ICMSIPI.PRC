create or replace
procedure fis_gerar_arq_9999_icmsipi
			(	nr_seq_controle_p	number,
				nr_linha_p			in out number	) is 

-- VARIABLES
ds_linha_w			varchar2(8000);
ds_sep_w			varchar2(1) := '|';

-- USUARIO
nm_usuario_w		usuario.nm_usuario%type;

-- FIS_EFD_ICMSIPI_ARQUIVO			
nr_linha_w			fis_efd_icmsipi_arquivo.nr_linha%type;
ds_arquivo_w		fis_efd_icmsipi_arquivo.ds_arquivo%type;
ds_arquivo_compl_w	fis_efd_icmsipi_arquivo.ds_arquivo_compl%type;

begin
nm_usuario_w := Obter_Usuario_Ativo;
nr_linha_w := nr_linha_p;

nr_linha_w := nr_linha_w + 1;
ds_linha_w := substr(ds_sep_w || '9999' ||
					ds_sep_w || nr_linha_w || ds_sep_w,1,8000);
ds_arquivo_w		:= substr(ds_linha_w,1,4000);
ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);				

insert into fis_efd_icmsipi_arquivo(
	nr_sequencia,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_atualizacao,
	nm_usuario,
	nr_seq_controle,
	nr_linha,
	ds_arquivo,
	ds_arquivo_compl,
	cd_registro)
values(
	fis_efd_icmsipi_arquivo_seq.nextval,
	sysdate,
	nm_usuario_w,
	sysdate,
	nm_usuario_w,
	nr_seq_controle_p,
	nr_linha_w,
	ds_arquivo_w,
	ds_arquivo_compl_w,
	'9999');		

nr_linha_p := nr_linha_w;

end fis_gerar_arq_9999_icmsipi;
/