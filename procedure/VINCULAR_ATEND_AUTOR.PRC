create or replace
procedure vincular_atend_autor(
			nr_atendimento_p	number,
			nr_sequencia_p		number) is 

begin

update	autorizacao_convenio 
set	nr_atendimento	=	nr_atendimento_p
where	nr_sequencia	=	nr_sequencia_p; 

commit;

end vincular_atend_autor;
/