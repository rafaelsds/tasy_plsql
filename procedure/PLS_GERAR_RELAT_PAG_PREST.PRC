create or replace 
procedure pls_gerar_relat_pag_prest(	nr_seq_lote_p		pls_lote_pagamento.nr_sequencia%type,
					nr_seq_prestador_p	pls_pagamento_prestador.nr_seq_prestador%type,
					nm_usuario_p		usuario.nm_usuario%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
finalidade:
-------------------------------------------------------------------------------------------------------------------
locais de chamada direta:
[ ]  objetos do dicion�rio [  x] tasy (delphi/java) [  ] portal [ x ]  relat�rios [ ] outros:
-------------------------------------------------------------------------------------------------------------------
pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_tipo_internacao_w 		varchar(255);
nr_tipo_atendimento_w 		varchar(255);
ds_tipo_despesa_w		varchar(255);
cd_prestador_w			varchar(255);
cd_beneficiario_w		varchar(255);
cid_obito_w			varchar(255);
nro_declaracao_obito_w		varchar(255);
cd_cid_w			varchar(255);
cd_cid_secundario_w		varchar(255);
ie_tipo_despesa_mat_w		number(15);
ie_tipo_despesa_proc_w		number(15);
dt_atendimento_w		date;         -- usada para procedimento e material
qt_material_w			varchar(255); -- usada para procedimento e material
vl_material_w			varchar(255); -- usada para procedimento e material
vl_procedimento_w		varchar(255); -- usada para procedimento e material
cd_material_w			varchar(255); -- usada para procedimento e material
ds_material_w			varchar(255); -- usada para procedimento e material
ie_via_acesso_w			varchar(255); -- usada para procedimento e material
cd_porte_anestesico_w		varchar(255); -- usada para procedimento e material
tx_adicional_w			varchar(255); -- usada para procedimento e material
nr_seq_prestdor_w		varchar(255);
sg_conselho_w			varchar(255);
cd_pessoa_fisica_benef_w	varchar(255);
cd_pessoa_fisica_prest_w	varchar(255);
nm_beneficiario_w		varchar(255);
nr_cpf_w			varchar(255);
uf_conselho_w			varchar(255);
nr_seq_prestador_w		varchar(255);
cd_prestador_externo_w		varchar(255);
sg_conselho_pgto_w		varchar(255);
ie_pessoa_prest_pgto		varchar(2);
cd_prestador_pgto_w		varchar(255);
sg_uf_prest_pgto_w		varchar(255);
nr_seq_conta_principal_w	pls_conta.nr_sequencia%type;
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type;

medico_executante_w		varchar2(255);
conselho_prof_w			varchar2(255);
conselho_uf_w			varchar2(255);
nome_medico_executante_w	varchar2(255);

medico_executante_aux1_w	varchar2(255);
conselho_prof_aux1_w		varchar2(255);
uf_conselho_aux1_w		varchar2(255);
nome_medico_executante_aux1_w	varchar2(255);

nr_medico_executante_aux2_w	number(10);
conselho_prof_aux2_w		varchar2(255);
uf_conselho_aux2_w		varchar2(255);
nm_medico_executante_aux2_w	varchar2(255);

medico_executante_aux3_w 	varchar(255);
conselho_prof_aux3_w		varchar(255);
uf_conselho_aux3_w		varchar(255);
nm_medico_exec_aux3_w		varchar(255);

medico_executante_aux4_w	varchar(255);
conselho_prof_aux4_w		varchar(255);
uf_conselho_aux4_w		varchar(255);
nome_medico_executante_aux4_w	varchar(255);

medico_executante_aux5_w	varchar(255);
conselho_prof_aux5_w		varchar(255);
uf_conselho_aux5_w		varchar(255);
nome_medico_executante_aux5_w	varchar(255);

medico_executante_anest_w	varchar(255);
conselho_prof_anest_w		varchar(255);
uf_conselho_anest_w		varchar(255);
nome_medico_executante_anest_w	varchar(255);
ie_pessoa_prest_exec_w		varchar(2);
dt_alta_w			date;
motivo_alta_w			number(15);
nr_seq_clinica_w  		number(15);
nr_seq_conta_proc_ant_w		pls_conta_proc.nr_sequencia%type := null;
nr_seq_conta_mat_ant_w		pls_conta_mat.nr_sequencia%type := null;

cursor c01 (	nr_seq_lote_pag_pc	pls_lote_pagamento.nr_sequencia%type,
		nr_seq_pag_prest_pc	pls_pagamento_prestador.nr_sequencia%type) is
	select	b.nr_seq_prestador seq_prestador,
		c.nr_seq_prestador_exec prest_exec,
		c.nr_seq_segurado,
		c.nr_sequencia nr_seq_conta,
		c.cd_guia_referencia numero_guia,
		c.nr_seq_analise nr_seq_analise,
		c.nr_seq_prestador_prot,
		c.nr_seq_segurado cd_benef,
		c.ie_tipo_guia,
		c.nr_seq_tipo_atendimento,
		null motivo_internacao,
		decode(c.ie_tipo_guia, '5', to_date(c.dt_entrada), to_date(c.dt_atendimento)) data_atendimento,
		to_date(c.dt_entrada) internacao,
		null codigo_obito,
		null tipo_nascimento,
		nvl(c.qt_nasc_vivos_termo, 0) nascidos_vivos,
		nvl(c.qt_nasc_mortos, 0) nascidos_mortos,
		nvl(c.qt_nasc_vivos_premat_num, 0) nascidos_prematuros,
		c.nr_seq_grau_partic,
		null nro_declaracao_nascido1,
		null nro_declaracao_nascido2,
		null nro_declaracao_nascido3,
		null nro_declaracao_nascido4,
		null nro_declaracao_nascido5,
		'0' fator,
		0 valor_de_co_maquina,
		0 valor_adicional_despesa,
		0 valor_adicional_filme,
		0 valor_adicional_co_maquina,
		null tecnica_utilizada,
		e.nr_seq_conta_mat nr_seq_conta_mat,
		e.nr_seq_conta_proc nr_seq_conta_proc,
		e.nr_seq_prestador_pgto prest_pgto,
		c.cd_usuario_plano,
		c.nr_seq_tipo_prest_solic,
		null nr_seq_mat_rec,
		null nr_seq_proc_rec
	from 	pls_conta_medica_resumo e,
		pls_pagamento_item      d,
		pls_conta_v 		c,
		pls_pagamento_prestador b,
		pls_lote_pagamento 	a
	where	d.nr_sequencia     	= e.nr_seq_pag_item
	and	d.nr_seq_pagamento 	= b.nr_sequencia
	and	c.nr_sequencia     	= e.nr_seq_conta
	and	a.nr_sequencia     	= b.nr_seq_lote
	and	a.nr_sequencia		= e.nr_seq_lote_pgto
	and	c.ie_status 	   	= 'F'
	and	e.vl_liberado 		> 0
	and	e.ie_tipo_item		<> 'I'
	and	e.ie_situacao		= 'A'
	and	a.nr_sequencia	 	= nr_seq_lote_p
	and	b.nr_seq_prestador	= nr_seq_prestador_p
	union all
	select	b.nr_seq_prestador seq_prestador,
		c.nr_seq_prestador_exec prest_exec,
		c.nr_seq_segurado,
		c.nr_sequencia nr_seq_conta,
		c.cd_guia_referencia numero_guia,
		c.nr_seq_analise nr_seq_analise,
		c.nr_seq_prestador_prot,
		c.nr_seq_segurado cd_benef,
		c.ie_tipo_guia,
		c.nr_seq_tipo_atendimento,
		null motivo_internacao,
		decode(c.ie_tipo_guia, '5', to_date(c.dt_entrada), to_date(c.dt_atendimento)) data_atendimento,
		to_date(c.dt_entrada) internacao,
		null codigo_obito,
		null tipo_nascimento,
		nvl(c.qt_nasc_vivos_termo, 0) nascidos_vivos,
		nvl(c.qt_nasc_mortos, 0) nascidos_mortos,
		nvl(c.qt_nasc_vivos_premat_num, 0) nascidos_prematuros,
		c.nr_seq_grau_partic,
		null nro_declaracao_nascido1,
		null nro_declaracao_nascido2,
		null nro_declaracao_nascido3,
		null nro_declaracao_nascido4,
		null nro_declaracao_nascido5,
		'0' fator,
		0 valor_de_co_maquina,
		0 valor_adicional_despesa,
		0 valor_adicional_filme,
		0 valor_adicional_co_maquina,
		null tecnica_utilizada,
		null nr_seq_conta_mat,
		null nr_seq_conta_proc,
		e.nr_seq_prestador_pgto prest_pgto,
		c.cd_usuario_plano,
		c.nr_seq_tipo_prest_solic,
		e.nr_seq_mat_rec nr_seq_mat_rec,
		e.nr_seq_proc_rec nr_seq_proc_rec
	from 	pls_conta_rec_resumo_item e,
		pls_pagamento_item      d,
		pls_conta_v 		c,
		pls_pagamento_prestador b,
		pls_lote_pagamento 	a
	where	d.nr_sequencia     	= e.nr_seq_pag_item
	and	d.nr_seq_pagamento 	= b.nr_sequencia
	and	c.nr_sequencia     	= e.nr_seq_conta
	and	a.nr_sequencia     	= b.nr_seq_lote
	and	a.nr_sequencia		= e.nr_seq_lote_pgto
	and	c.ie_status 	   	= 'F'
	and	e.vl_liberado 		> 0
	and	e.ie_situacao		= 'A'
	and	a.nr_sequencia	 	= nr_seq_lote_p
	and	b.nr_seq_prestador	= nr_seq_prestador_p
	order by
		nr_seq_conta desc,
		nr_seq_conta_proc,
		nr_seq_conta_mat;
	
begin

delete	w_relat_inf_pag_prest
where	nm_usuario = nm_usuario_p;


for r_c01_w in c01( nr_seq_lote_p, nr_seq_prestador_p ) loop
	
	-- Se for recurso de glosa
	if	(r_c01_w.nr_seq_mat_rec is not null) or
		(r_c01_w.nr_seq_proc_rec is not null) then
		nr_seq_conta_proc_ant_w := null;
		nr_seq_conta_mat_ant_w := null;
	end if;
	
	-- Se for conta proc, limpa o conta mat anterior
	if	(r_c01_w.nr_seq_conta_proc is not null) then
		nr_seq_conta_mat_ant_w := null;
	end if;
	
	-- Se for conta mat, limpa o conta proc anterior
	if	(r_c01_w.nr_seq_conta_mat is not null) then
		nr_seq_conta_proc_ant_w := null;
	end if;	
	
	if	(((nr_seq_conta_proc_ant_w is null) and (nr_seq_conta_mat_ant_w is null)) or -- For recurso de glosa
		((nr_seq_conta_proc_ant_w != r_c01_w.nr_seq_conta_proc) or -- Proc anterior diferente do atual
		(nr_seq_conta_mat_ant_w != r_c01_w.nr_seq_conta_mat))) then -- Mat anterior diferente do atual

		--obter dados do  beneficiario
		cd_beneficiario_w := substr(pls_convert_masc_cart_usuario(r_c01_w.cd_usuario_plano, cd_estabelecimento_p), 2, 20);
		nm_beneficiario_w := pls_obter_dados_segurado(r_c01_w.nr_seq_segurado, 'N');
		--fim

		--obter dados do prestador executor
		select 	max(x.cd_prestador),
			max(x.cd_pessoa_fisica),
			max(x.cd_prestador_externo),
			decode(max(x.cd_pessoa_fisica), null, 'PJ', 'PF')
		into	cd_prestador_w,
			cd_pessoa_fisica_prest_w,
			cd_prestador_externo_w,
			ie_pessoa_prest_exec_w
		from	pls_prestador x
		where 	x.nr_sequencia = r_c01_w.prest_exec;
		
		--pego o conselhor do prestador executor
		if	(cd_pessoa_fisica_prest_w is not null) then
			select	max(a.sg_conselho)
			into	sg_conselho_w
			from	conselho_profissional a,
				pessoa_fisica b
			where	a.nr_sequencia = b.nr_seq_conselho
			and	b.cd_pessoa_fisica = cd_pessoa_fisica_prest_w;
		end if;		

		if	(cd_pessoa_fisica_prest_w is not null) then
			select	max(y.nr_cpf),
				max(y.uf_conselho)
			into	nr_cpf_w,
				uf_conselho_w
			from	pls_prestador x,
				pessoa_fisica y
			where	x.cd_pessoa_fisica = y.cd_pessoa_fisica
			and	x.nr_sequencia = r_c01_w.prest_exec;
		else
			select 	max(cd_cgc),
				null
			into	nr_cpf_w,
				uf_conselho_w
			from	pls_prestador
			where 	nr_sequencia = r_c01_w.prest_exec;
		end if;
		-- if valida se preenche o codigo do prestador ou n�o da unimed jales
		if	(cd_pessoa_fisica_prest_w is not null) or 
			(sg_conselho_w = 'CRM' ) then
			cd_prestador_w := null;
		end if;
		
		if	(cd_pessoa_fisica_prest_w is null) or
			(sg_conselho_w <> 'CRM') then
			cd_prestador_w := cd_prestador_externo_w;
		end if;
		--fimif

		--obter os codigos de doenca
		select	max(cd_doenca) cid_obito,
			max(nr_declaracao_obito) nro_declaracao
		into	cid_obito_w,
			nro_declaracao_obito_w
		from	pls_diagnost_conta_obito x
		where	x.nr_seq_conta = r_c01_w.nr_seq_conta;
		
		--verificar o tipo de doenca
		select	max(decode(x.ie_classificacao, 'P', x.cd_doenca, null)) cid,
			max(decode(x.ie_classificacao, 'S', x.cd_doenca, null)) cid_secundario
		into	cd_cid_w,
			cd_cid_secundario_w
		from	pls_diagnostico_conta x
		where	r_c01_w.nr_seq_conta = x.nr_seq_conta;
		--fim obter codigo doenca

		-- obter o tipo do atendimento/interna��o
		--pego a conta principal 		########alterado a estrutura conforme solicitado pelo ed carlos na os790465 o mesmo solicitou o tipo de guia da conta principal############
		nr_seq_conta_principal_w := pls_obter_conta_principal(r_c01_w.numero_guia, r_c01_w.nr_seq_analise, r_c01_w.nr_seq_segurado, r_c01_w.nr_seq_prestador_prot);
		
		--com a conta principal pego o tipo da guia
		select	max(ie_tipo_guia) ie_tipo_guia,
			max(nr_seq_clinica)
		into	ie_tipo_guia_w,
			nr_seq_clinica_w
		from	pls_conta
		where	nr_sequencia = nr_seq_conta_principal_w;
		
		--com o tipo da guia trato o tipo do atendimento
		if 	(ie_tipo_guia_w = '3') then
			nr_tipo_atendimento_w := '1';
			
		elsif 	(ie_tipo_guia_w = '4') and 
			(r_c01_w.nr_seq_tipo_atendimento = 2) then
			nr_tipo_atendimento_w := '3';
			
		elsif 	(ie_tipo_guia_w = '4') and 
			(r_c01_w.nr_seq_tipo_atendimento <> 2) then
			nr_tipo_atendimento_w := '2';
				
		elsif 	(ie_tipo_guia_w = '5') then
			nr_tipo_atendimento_w := '4';
		end if;
		
		--com o tipo atendimento trato a data de alta
		if	(nr_tipo_atendimento_w in('3', '4')) then
			select	max(decode(dt_alta, null, dt_atendimento, dt_alta))
			into	dt_alta_w
			from	pls_conta_v
			where	nr_sequencia = nr_seq_conta_principal_w;
			
			motivo_alta_w := 11;
		else
			motivo_alta_w := null;
			dt_alta_w := null;
		end if;
	  
		--obter o tipo d einternacao
		if (ie_tipo_guia_w = '5') then
			case 	when	(nr_seq_clinica_w = 1) 	then
					nr_tipo_internacao_w := '1';
					
				when	(nr_seq_clinica_w = 2)	then
					nr_tipo_internacao_w := '2';
					
				when	(nr_seq_clinica_w = 3)	then
					nr_tipo_internacao_w := '3';
					
				when	(nr_seq_clinica_w = 4)	then
					nr_tipo_internacao_w := '6';
					
				when	(nr_seq_clinica_w = 5)	then
					nr_tipo_internacao_w := '7';
				else
					nr_tipo_internacao_w := null;
			end case;
		end if;
		--fim obter tipo do atendimento/interna��o

		--obter dados referente as despesa de procedimento e materiais
		if	(r_c01_w.nr_seq_conta_proc is not null) then --procedimentos
			--busca as informacoes do procedimento      #######alterado a estrutura conforme solicitado pelo Ed Carlos na OS790465 pois o mesmo solicitou os valores atraves da pls_conta_medica_resumo##########
			select  max(c.nr_seq_grupo_rec),
				max(b.cd_procedimento),
				max(b.dt_procedimento),
				max(a.qt_item),
				sum(a.vl_liberado) - sum(a.vl_materiais_pag),
				sum(a.vl_materiais_pag),
				max(b.cd_porte_anestesico),
				max(nvl((	select	ch.tx_procedimento
						from   	pls_proc_criterio_horario ch
						where  	ch.nr_sequencia = b.nr_seq_regra_horario),1)), 
				max(nvl(b.ie_via_acesso,'U')) via_acesso, 
				max(c.ds_procedimento) 
			into	ie_tipo_despesa_proc_w,
				cd_material_w,
				dt_atendimento_w,
				qt_material_w,
				vl_procedimento_w,
				vl_material_w,
				cd_porte_anestesico_w,
				tx_adicional_w,
				ie_via_acesso_w,
				ds_material_w	
			from    pls_conta_medica_resumo a,
				pls_conta_proc b,
				procedimento c
			where   1 = 1
			and     a.nr_seq_conta_proc = b.nr_sequencia
			and     b.cd_procedimento = c.cd_procedimento
			and     b.ie_origem_proced = c.ie_origem_proced
			and	a.nr_seq_conta = b.nr_seq_conta
			and	a.vl_liberado > 0
			and	a.ie_tipo_item <> 'I'
			and	a.ie_situacao = 'A'
			and     b.nr_sequencia = r_c01_w.nr_seq_conta_proc;
			
			--trata o tipo da despesa do procedimento
			if	(ie_tipo_despesa_proc_w in(6, 62)) then
				ds_tipo_despesa_w := 'PR';
				
			elsif	(ie_tipo_despesa_proc_w = 7) then
				ds_tipo_despesa_w := 'TA';
				
			elsif	(ie_tipo_despesa_proc_w = 4) then
				ds_tipo_despesa_w := 'DI';
				
			elsif	(ie_tipo_despesa_proc_w = 5) then
				ds_tipo_despesa_w := 'EX';
			else
				ds_tipo_despesa_w := 'TA';
			end if;
		
		elsif	(r_c01_w.nr_seq_conta_mat is not null) then -- materiais
			--busca informacoes do material 	#########alterado a estrutura conforme solicitado pelo Ed Carlos na OS790465 pois o mesmo solicitou os valores atraves da pls_conta_medica_resumo########
			select 	max((	select 	min(CONNECT_BY_ROOT nr_sequencia)
					from  	pls_estrutura_material
					where 	level in (4,3,2,1) and nr_sequencia = z.nr_sequencia
					connect by prior nr_sequencia = nr_seq_superior)),
				max(y.cd_material_ops) codigo_despesa,
				max(x.dt_atendimento) data_servico,
				max(r.qt_item) qt_apresentada_paga,
				sum(r.vl_liberado) valor_despesa,
				'0' valor_filme,
				null porte_anestesico,
				max(( 	select 	x.tx_procedimento
					from	pls_proc_criterio_horario x,
						pls_prestador y
					where 	y.nr_sequencia	= x.nr_seq_prestador
					and	y.nr_sequencia	= r_c01_w.prest_exec)) adicional,
				null via_acesso,
				max(y.ds_material) descricao_despesa
			into	ie_tipo_despesa_mat_w,
				cd_material_w,
				dt_atendimento_w,
				qt_material_w,
				vl_procedimento_w,
				vl_material_w,
				cd_porte_anestesico_w,
				tx_adicional_w,
				ie_via_acesso_w,
				ds_material_w
			from	pls_conta_mat x,
				pls_material y,
				pls_estrutura_material z,
				pls_conta_medica_resumo r
			where	y.nr_sequencia = x.nr_seq_material
			and	z.nr_sequencia = y.nr_seq_estrut_mat
			and	x.nr_sequencia = r.nr_seq_conta_mat
			and	r.nr_seq_conta = x.nr_seq_conta
			and	r.ie_tipo_item <> 'I'
			and	r.ie_situacao = 'A'
			and	r.vl_liberado  > 0
			and	x.nr_sequencia = r_c01_w.nr_seq_conta_mat;
			
			--trata o tipo da despesa do material
			if 	(ie_tipo_despesa_mat_w in(10, 11, 12, 13, 14)) then
				ds_tipo_despesa_w := 'MA';
				
			elsif	(ie_tipo_despesa_mat_w in(15, 16, 17) ) then
				ds_tipo_despesa_w := 'ME';
			else
				ds_tipo_despesa_w := 'TA';
			end if;
		end if;
		--fim obter tipo despesa
		
		--obter dados referente as despesa de procedimento e materiais
		if	(r_c01_w.nr_seq_proc_rec is not null) then --procedimentos
			--busca as informacoes do procedimento      #######alterado a estrutura conforme solicitado pelo Ed Carlos na OS790465 pois o mesmo solicitou os valores atraves da pls_conta_medica_resumo##########
			select  max(c.nr_seq_grupo_rec),
				max(b.cd_procedimento),
				max(b.dt_procedimento),
				'1',
				sum(a.vl_liberado),
				'0',
				max(b.cd_porte_anestesico),
				max(nvl((	select 	ch.tx_procedimento
						from   	pls_proc_criterio_horario ch
						where  	ch.nr_sequencia = b.nr_seq_regra_horario),1)), 
				max(nvl(b.ie_via_acesso,'U')) via_acesso, 
				max(c.ds_procedimento) 
			into	ie_tipo_despesa_proc_w,
				cd_material_w,
				dt_atendimento_w,
				qt_material_w,
				vl_procedimento_w,
				vl_material_w,
				cd_porte_anestesico_w,
				tx_adicional_w,
				ie_via_acesso_w,
				ds_material_w	
			from    pls_conta_rec_resumo_item a,
				pls_rec_glosa_proc d,		
				pls_conta_proc b,
				procedimento c
			where   1 = 1
			and     b.cd_procedimento = c.cd_procedimento
			and     b.ie_origem_proced = c.ie_origem_proced
			and	a.nr_seq_proc_rec  = d.nr_sequencia
			and	d.nr_seq_conta_proc = b.nr_sequencia
			and	a.nr_seq_conta_rec = d.nr_seq_conta_rec
			and	a.vl_liberado > 0
			and	a.ie_situacao = 'A'
			and     d.nr_sequencia = r_c01_w.nr_seq_proc_rec;
			
			--trata o tipo da despesa do procedimento
			if	(ie_tipo_despesa_proc_w in(6, 62)) then
				ds_tipo_despesa_w := 'PR';
				
			elsif	(ie_tipo_despesa_proc_w = 7) then
				ds_tipo_despesa_w := 'TA';
				
			elsif	(ie_tipo_despesa_proc_w = 4) then
				ds_tipo_despesa_w := 'DI';
				
			elsif	(ie_tipo_despesa_proc_w = 5) then
				ds_tipo_despesa_w := 'EX';
			else
				ds_tipo_despesa_w := 'TA';
			end if;
			
		elsif(r_c01_w.nr_seq_mat_rec is not null) then -- materiais
			--busca informacoes do material 	#########alterado a estrutura conforme solicitado pelo Ed Carlos na OS790465 pois o mesmo solicitou os valores atraves da pls_conta_medica_resumo########
			select 	max((	select 	min(CONNECT_BY_ROOT nr_sequencia)
					from  	pls_estrutura_material
					where 	level in (4,3,2,1) and nr_sequencia = z.nr_sequencia
					connect by prior nr_sequencia = nr_seq_superior)),
				max(y.cd_material_ops) codigo_despesa,
				max(x.dt_atendimento) data_servico,
				'0' qt_apresentada_paga,
				sum(r.vl_liberado) valor_despesa,
				'0' valor_filme,
				null porte_anestesico,
				max(( 	select 	x.tx_procedimento
					from	pls_proc_criterio_horario x,
						pls_prestador y
					where 	y.nr_sequencia	= x.nr_seq_prestador
					and	y.nr_sequencia	= r_c01_w.prest_exec)) adicional,
				null via_acesso,
				max(y.ds_material) descricao_despesa
			into	ie_tipo_despesa_mat_w,
				cd_material_w,
				dt_atendimento_w,
				qt_material_w,
				vl_procedimento_w,
				vl_material_w,
				cd_porte_anestesico_w,
				tx_adicional_w,
				ie_via_acesso_w,
				ds_material_w
			from	pls_conta_mat x,
				pls_material y,
				pls_estrutura_material z,
				pls_conta_rec_resumo_item r,
				pls_rec_glosa_mat s
			where	y.nr_sequencia = x.nr_seq_material
			and	z.nr_sequencia = y.nr_seq_estrut_mat
			and	s.nr_sequencia = r.nr_seq_mat_rec
			and	r.nr_seq_conta_rec = s.nr_seq_conta_rec
			and	x.nr_sequencia = s.nr_seq_conta_mat
			and	r.ie_situacao = 'A'
			and	r.vl_liberado  > 0
			and	s.nr_sequencia = r_c01_w.nr_seq_mat_rec;

			--trata o tipo da despesa do material
			if 	(ie_tipo_despesa_mat_w in(10, 11, 12, 13, 14)) then
				ds_tipo_despesa_w := 'MA';
				
			elsif	(ie_tipo_despesa_mat_w in(15, 16, 17) ) then
				ds_tipo_despesa_w := 'ME';
			else
				ds_tipo_despesa_w := 'TA';
			end if;
		end if;
		--fim obter tipo despesa
		
		--obter dados medico executante
		--pego o conselho e o tipo de pessoa do prestador pagador que irei usar na condi��o abaixo
		select	max(sg_conselho),
			decode(max(cd_pessoa_fisica), null, 'PJ', 'PF'),
			max(cd_prestador_externo),
			max(sg_uf_sip)
		into	sg_conselho_pgto_w,
			ie_pessoa_prest_pgto,
			cd_prestador_pgto_w,
			sg_uf_prest_pgto_w
		from	pls_prestador
		where	nr_sequencia = r_c01_w.prest_pgto;

		--codigo
		if 	((ie_pessoa_prest_pgto = 'PJ') and 
			(r_c01_w.nr_seq_tipo_prest_solic in(19, 20, 21, 22))) then
			medico_executante_w := cd_prestador_externo_w; --altera��o solicitada pelo Felipe Rodrigues referente ao hist�rico  do 26/09/2014

		elsif	((sg_conselho_pgto_w <> 'CRM') or 
			(r_c01_w.nr_seq_grau_partic in(1, 5, 8, 9, 10, 11, 12, 13)) or 
			(r_c01_w.nr_seq_grau_partic is null )) then
			medico_executante_w := cd_prestador_externo_w; --altera��o solicitada pelo Felipe Rodrigues referente ao hist�rico  do 26/09/2014
			
		elsif	((ie_pessoa_prest_pgto = 'PJ') and 
			(r_c01_w.nr_seq_tipo_prest_solic  not in(19, 20, 21, 22))) then
			medico_executante_w := null;
		else
			medico_executante_w := null;
		end if;

		-- conselho
		if 	( ie_pessoa_prest_exec_w = 'PJ') then
			conselho_prof_w := 'CRM';		
		else
			conselho_prof_w := sg_conselho_w; --altera��o efetuado dia 18/11/2014 de acordo com Carlos Rezende e Felipe.
		end if;
		
		-- uf conselho do medico executor
		if 	(ie_pessoa_prest_pgto = 'PJ') then
			conselho_uf_w := 'SP';
		elsif 	(sg_conselho_pgto_w <> 'CRM') or 
			(r_c01_w.nr_seq_grau_partic = 1) or 
			(r_c01_w.nr_seq_grau_partic is null) then
			conselho_uf_w := sg_uf_prest_pgto_w;
		else
			conselho_uf_w := null;
		end if;
		
		--nome
		if 	(ie_pessoa_prest_pgto = 'PJ') or 
			(sg_conselho_pgto_w <> 'CRM') then
			nome_medico_executante_w := pls_obter_dados_prestador(r_c01_w.prest_exec, 'N');
			
		elsif	(r_c01_w.nr_seq_grau_partic = 1) then
			nome_medico_executante_w := pls_obter_dados_prestador(r_c01_w.prest_exec, 'N');
			
		elsif	(r_c01_w.nr_seq_grau_partic is null) then
			nome_medico_executante_w := pls_obter_dados_prestador(r_c01_w.prest_exec, 'N');
		else
			nome_medico_executante_w := null;
		end if;
		--fim obter dados medico executante
		
		--obter dados do aux1
		if	(cd_pessoa_fisica_prest_w is not null) and 
			(r_c01_w.nr_seq_grau_partic = 2)then
			medico_executante_aux1_w	:= cd_prestador_w;
			conselho_prof_aux1_w		:= sg_conselho_w;
			uf_conselho_aux1_w		:= uf_conselho_w;
			nome_medico_executante_aux1_w	:= pls_obter_dados_prestador(nr_seq_prestador_w, 'N');
		end if;
		
		--obter dados do aux2
		if	(cd_pessoa_fisica_prest_w is not null) and 
			(r_c01_w.nr_seq_grau_partic = 3) then
			nr_medico_executante_aux2_w 	:= cd_prestador_w;
			conselho_prof_aux2_w	  	:= sg_conselho_w;
			uf_conselho_aux2_w	  	:= uf_conselho_w;
			nm_medico_executante_aux2_w 	:= pls_obter_dados_prestador(nr_seq_prestador_w, 'N');
		end if;
		
		--obter dados do aux 3
		if	(cd_pessoa_fisica_prest_w is not null) and 
			(r_c01_w.nr_seq_grau_partic = 4 ) then
			medico_executante_aux3_w := cd_prestador_w;
			conselho_prof_aux3_w	 := sg_conselho_w;
			uf_conselho_aux3_w	 := uf_conselho_w;
			nm_medico_exec_aux3_w	 := pls_obter_dados_prestador(nr_seq_prestador_w, 'N');
		end if;
		
		--obter dados do aux4
		if	(cd_pessoa_fisica_prest_w is not null) and 
			(r_c01_w.nr_seq_grau_partic = 5) then
			medico_executante_aux4_w	:= cd_prestador_w;
			conselho_prof_aux4_w		:= sg_conselho_w;
			uf_conselho_aux4_w		:= uf_conselho_w;
			nome_medico_executante_aux4_w	:= pls_obter_dados_prestador( nr_seq_prestador_w, 'N');
		end if;
		
		--obter dados do aux5
		if	(cd_pessoa_fisica_prest_w is not null) and 
			(r_c01_w.nr_seq_grau_partic = 8) then
			medico_executante_aux5_w	:= cd_prestador_w;
			conselho_prof_aux5_w		:= sg_conselho_w;
			uf_conselho_aux5_w		:= uf_conselho_w;
			nome_medico_executante_aux5_w	:= pls_obter_dados_prestador( nr_seq_prestador_w, 'N');
		end if;
		
		--dados do anestesista
		if	(cd_pessoa_fisica_prest_w is not null) and 
			(r_c01_w.nr_seq_grau_partic = 6) then
			medico_executante_anest_w	:= cd_prestador_w;
			conselho_prof_anest_w		:= sg_conselho_w;
			uf_conselho_anest_w		:= uf_conselho_w;
			nome_medico_executante_anest_w	:= pls_obter_dados_prestador( nr_seq_prestador_w, 'N');
		end if;
		--fim

		--inserindo os dados na tabela
		insert into w_relat_inf_pag_prest (	nr_sequencia,
							nm_usuario,
							dt_atualizacao,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							numero_guia,
							codigo_local_atendimento,
							codigo_beneficiario,
							nome_beneficiario,
							tipo_atendimento,
							tipo_internacao,
							motivo_internacao,
							data_atendimento,
							data_alta,
							motivo_alta,
							cid,
							cid_secundario,
							codigo_obito,
							cid_obito,
							nro_declaracao_obito,
							tipo_nascimento,
							nascidos_vivos,
							nascidos_mortos,
							nascidos_prematuros,
							nro_declaracao_nascido_1,
							nro_declaracao_nascido_2,
							nro_declaracao_nascido_3,
							nro_declaracao_nascido_4,
							nro_declaracao_nascido_5,
							tipo_despesa,
							codigo_despesa,
							descricao_despesa,
							data_servico,
							quantidade_apresentada,
							quantidade_pagamento,
							fator,
							valor_da_despesa,
							valor_de_filme,
							valor_de_co_maquina,
							valor_adicional_despesa,
							valor_adicional_filme,
							valor_adicional_co_maquina,
							porte_anestesico,
							adicional,
							via_acesso,
							tecnica_utilizada,
							codigo_do_prestador,
							medico_executante,
							conselho_prof,
							uf_conselho,
							nome_medico_executante,
							medico_executante_aux1,
							conselho_prof_aux1,
							uf_conselho_aux1,
							nome_medico_executante_aux1,
							medico_executante_aux2,
							conselho_prof_aux2,
							uf_conselho_aux2,
							nome_medico_executante_aux2,
							medico_executante_aux3,
							conselho_prof_aux3,
							uf_conselho_aux3,
							nome_medico_executante_aux3,
							medico_executante_aux4,
							conselho_prof_aux4,
							uf_conselho_aux4,
							nome_medico_executante_aux4,
							medico_executante_aux5,
							conselho_prof_aux5,
							uf_conselho_aux5,
							nome_medico_executante_aux5,
							medico_executante_anest,
							conselho_prof_anest,
							uf_conselho_anest,
							nome_medico_executante_anest
							)
		values	( 				w_relat_inf_pag_prest_seq.nextval,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							r_c01_w.numero_guia,
							cd_prestador_externo_w,
							cd_beneficiario_w,
							nm_beneficiario_w,
							nr_tipo_atendimento_w,
							nr_tipo_internacao_w,
							r_c01_w.motivo_internacao,
							r_c01_w.data_atendimento,
							dt_alta_w,
							motivo_alta_w,
							cd_cid_w,
							cd_cid_secundario_w,
							r_c01_w.codigo_obito,
							cid_obito_w,
							nro_declaracao_obito_w,
							r_c01_w.tipo_nascimento,
							r_c01_w.nascidos_vivos,
							r_c01_w.nascidos_mortos,
							to_char(r_c01_w.nascidos_prematuros),
							r_c01_w.nro_declaracao_nascido1,
							r_c01_w.nro_declaracao_nascido2,
							r_c01_w.nro_declaracao_nascido3,
							r_c01_w.nro_declaracao_nascido4,
							r_c01_w.nro_declaracao_nascido5,
							ds_tipo_despesa_w,
							cd_material_w,
							ds_material_w,
							dt_atendimento_w,
							qt_material_w,
							qt_material_w,
							r_c01_w.fator,
							vl_procedimento_w,
							vl_material_w,
							r_c01_w.valor_de_co_maquina,
							r_c01_w.valor_adicional_despesa,
							r_c01_w.valor_adicional_filme,
							r_c01_w.valor_adicional_co_maquina,
							cd_porte_anestesico_w,
							tx_adicional_w,
							ie_via_acesso_w,
							r_c01_w.tecnica_utilizada,
							medico_executante_w,
							medico_executante_w,
							conselho_prof_w,
							conselho_uf_w,
							nome_medico_executante_w,
							medico_executante_aux1_w,
							conselho_prof_aux1_w,
							uf_conselho_aux1_w,
							nome_medico_executante_aux1_w,
							nr_medico_executante_aux2_w,
							conselho_prof_aux2_w,
							uf_conselho_aux2_w,
							nm_medico_executante_aux2_w,
							medico_executante_aux3_w,
							conselho_prof_aux3_w,
							uf_conselho_aux3_w,
							nm_medico_exec_aux3_w,
							medico_executante_aux4_w,
							conselho_prof_aux4_w,
							uf_conselho_aux4_w,
							nome_medico_executante_aux4_w,
							medico_executante_aux5_w,
							conselho_prof_aux5_w,
							uf_conselho_aux5_w,
							nome_medico_executante_aux5_w,
							medico_executante_anest_w,
							conselho_prof_anest_w,
							uf_conselho_anest_w,
							nome_medico_executante_anest_w
							);
		
		-- Salvar o conta proc/mat anterior
		nr_seq_conta_proc_ant_w	:= r_c01_w.nr_seq_conta_proc;
		nr_seq_conta_mat_ant_w	:= r_c01_w.nr_seq_conta_mat;
	end if;
end loop;

commit;

end pls_gerar_relat_pag_prest;
/