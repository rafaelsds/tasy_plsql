create or replace
procedure gerar_historico_lote_fornec(
			nm_usuario_p		Varchar2,
			nr_seq_lote_fornec_p	Number,
			ds_titulo_p		Varchar2,
			ds_historico_p		Varchar2) is 

nr_sequencia_w		Number(10);
			
begin

select	material_lote_fornec_hist_seq.nextval
into	nr_sequencia_w
from	dual;

insert into material_lote_fornec_hist(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_lote_fornec,
			ds_titulo,
			ds_historico,
			ie_tipo,
			dt_liberacao,
			nm_usuario_lib)
				values(
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_lote_fornec_p,
			ds_titulo_p,
			ds_historico_p,
			'U',
			sysdate,
			nm_usuario_p);
commit;

end gerar_historico_lote_fornec;
/