create or replace
procedure copiar_capac_prod_html(cd_estabelecimento_p	number,
				cd_tabela_origem_p    	number,
				nr_seq_tabela_dest_p	number,
				nm_usuario_p      	varchar2,
				nr_seq_tabela_origem_p	number default null) is 

cd_tabela_destino_w 	tabela_custo.cd_tabela_custo%type;	
nr_seq_tabela_origem_w	tabela_custo.nr_sequencia%type;			
				
begin
/*OS2006653 - Jeferson Job - Colocado aqui pois o nr_seq_tabela_origem_p pode vir nulo*/
cd_tabela_destino_w := obter_tab_custo_html(nr_seq_tabela_dest_p);
nr_seq_tabela_origem_w:= nr_seq_tabela_origem_p;

if( nvl(nr_seq_tabela_origem_p, 0) = 0 ) then 
	select	max(nr_sequencia)
	into	nr_seq_tabela_origem_w
	from	tabela_custo
	where	cd_tabela_custo = cd_tabela_origem_p
	and	cd_estabelecimento = cd_estabelecimento_p;
end if;

copiar_capacidade_producao(cd_estabelecimento_p,
				cd_tabela_origem_p,
				cd_tabela_destino_w,
				nr_seq_tabela_origem_w,
				nr_seq_tabela_dest_p,
				nm_usuario_p);

commit;

end copiar_capac_prod_html;
/
