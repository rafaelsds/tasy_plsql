create or replace
procedure pagador_before_post_eup_js(	nr_atendimento_p	number,
					cd_estabelecimento_p	number,
					ie_exige_cpf_pagador_p	out varchar2,
					ds_texto_p		out varchar2) is 

cd_convenio_w		number(5,0);
ie_exige_cpf_pagador_w	varchar2(1);
ds_texto_w		varchar2(255);

begin

if	(nr_atendimento_p is not null)then
	begin
	
	cd_convenio_w	:= obter_convenio_atendimento(nr_atendimento_p);
	
	select  nvl(max(ie_exige_cpf_pagador), 'N') 
	into	ie_exige_cpf_pagador_w
	from    convenio_estabelecimento
	where   cd_convenio = cd_convenio_w
	and     cd_estabelecimento = cd_estabelecimento_p;
	
	ds_texto_w	:= wheb_mensagem_pck.get_texto(68279);
	
	end;
end if;

ie_exige_cpf_pagador_p	:= ie_exige_cpf_pagador_w;
ds_texto_p		:= ds_texto_w;

end pagador_before_post_eup_js;
/
