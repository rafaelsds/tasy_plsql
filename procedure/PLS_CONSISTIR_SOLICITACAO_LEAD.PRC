create or replace
procedure pls_consistir_solicitacao_lead
		(	ie_tipo_contratacao_p	in	varchar2,
			nr_seq_solicitacao_p	in	number,
			nr_telefone_p		in	varchar2,
			nr_celular_p		in	varchar2,
			nr_telefone_adic_p	in	varchar2,
			nr_ddi_p		in	varchar,
			dt_nascimento_p		in	date,
			ds_email_p		in	varchar2,
			cd_cgc_p		in	varchar2,
			nr_cpf_p		in	varchar2,	
			nm_usuario_p		in	varchar2,
			cd_estabelecimento_p	in	number,
			qt_pessoas_p		out	number,
			ds_erro_p		out	varchar2) is 

ds_erro_w		varchar2(4000);
ds_erro_dig_telefone_w	varchar2(4000);

-- Campos do select din�mico
nr_seq_solicitacao_w	pls_solicitacao_comercial.nr_sequencia%type;

cd_exp_consistencia_w	pls_cad_duplicidade_lead.cd_exp_consistencia%type;

ie_dt_nascimento_w	varchar2(1) := 'N';
ie_nr_telefone_w	varchar2(1) := 'N';
ie_ds_email_w		varchar2(1) := 'N';
ie_cd_cgc_w		varchar2(1) := 'N';
ie_nr_cpf_w		varchar2(1) := 'N';
qt_campos_inconsist_w	pls_integer := 0;
ds_campos_erro_w	varchar2(255) := '';

ds_sql_orig_w		varchar2(4000);
ds_restricao_w		varchar2(4000) := null;
ds_sql_w		varchar2(4000);
var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;

qt_registros_max_w	pls_integer;
i			pls_integer 	:= 0;
qt_pessoas_w		pls_integer 	:= 0;

nr_seq_solic_table_w	dbms_sql.number_table;
ie_tipo_contratacao_w	pls_solicitacao_comercial.ie_tipo_contratacao%type;

ie_param_81_w    varchar2(1);


Cursor C01 is
	select	cd_exp_consistencia
	from	pls_cad_duplicidade_lead
	where	ie_situacao = 'A'
	and	(nvl(ie_tipo_contratacao,'A') = ie_tipo_contratacao_w or nvl(ie_tipo_contratacao,'A') = 'A');
	
procedure pls_insere_leads_tab_temp 
		(	nr_seq_solicitacao_p	dbms_sql.number_table) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Rotina respons�vel por inserir solicita��es de lead a uma tabela tempor�ria.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 
begin

if	(nr_seq_solicitacao_p.count > 0) then
	forall i in nr_seq_solicitacao_p.first..nr_seq_solicitacao_p.last
		insert into pls_aux_leads_duplicados
			(	nr_seq_solicitacao )
		values	(	nr_seq_solicitacao_p(i));
		
	commit;
end if;

end pls_insere_leads_tab_temp;
	
begin

if	(ie_tipo_contratacao_p is null) then
	ds_erro_w := ds_erro_w || wheb_mensagem_pck.get_texto(285623) || ' ';
	/* Mensagem: Favor informar o tipo de contrata��o! */
end if;
	
pls_consistir_telefone_solic(nr_telefone_p, nr_ddi_p, nm_usuario_p, cd_estabelecimento_p, ds_erro_dig_telefone_w);

if	(ds_erro_dig_telefone_w <> '') then
	ds_erro_w := ds_erro_w || ds_erro_dig_telefone_w;
end if;

select	decode(ie_tipo_contratacao_p, 'I', 'PF', 'CA', 'PJ', 'CE', 'PJ', 'A')
into	ie_tipo_contratacao_w
from	dual;

select  nvl(obter_valor_param_usuario(1237, 81, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N')
into  ie_param_81_w
from  dual;


ds_sql_orig_w := 	' 	select 	nr_sequencia nr_seq_solicitacao								' || pls_util_pck.enter_w ||
			' 	from	pls_solicitacao_comercial								' || pls_util_pck.enter_w ||
			' 	where	nr_sequencia 	<> :nr_seq_solicitacao 							' || pls_util_pck.enter_w;
if (ie_param_81_w = 'S') then
ds_sql_orig_w := ds_sql_orig_w || ' and ie_status = ''PE'' ' || pls_util_pck.enter_w;
else 
ds_sql_orig_w := ds_sql_orig_w || ' and ie_status <> ''R'' ' || pls_util_pck.enter_w;
end if;






open C01;
loop
fetch C01 into	
	cd_exp_consistencia_w;
exit when C01%notfound;
	begin
	
	case (cd_exp_consistencia_w)
		when 303344 then -- Data de nascimento
			if	(dt_nascimento_p is not null) then
				ds_restricao_w	:= ds_restricao_w || 	' and	dt_nascimento = :dt_nascimento ' || pls_util_pck.enter_w;
				ie_dt_nascimento_w := 'S';
				qt_campos_inconsist_w := qt_campos_inconsist_w + 1;
				ds_campos_erro_w := ds_campos_erro_w || 'data de nascimento e ';
			end if;
		when 299149 then -- Telefone
			if	((nr_telefone_p is not null) or (nr_celular_p is not null) or (nr_telefone_adic_p is not null)) then
				ds_restricao_w	:= ds_restricao_w || 	' and (	 pls_verifica_tel(nr_telefone,:nr_telefone) = ''S''	and :nr_telefone is not null or		' || pls_util_pck.enter_w ||
									'	pls_verifica_tel(nr_telefone,:nr_celular) = ''S''	and :nr_celular is not null or		' || pls_util_pck.enter_w ||
									'	pls_verifica_tel(nr_telefone,:nr_telefone_adic) = ''S'' and :nr_telefone_adic is not null or	' || pls_util_pck.enter_w ||
									'	pls_verifica_tel(nr_telefone,:nr_telefone) = ''S''	and :nr_telefone is not null or		' || pls_util_pck.enter_w ||
									'	pls_verifica_tel(nr_telefone,:nr_celular) = ''S''	and :nr_celular is not null or		' || pls_util_pck.enter_w ||
									'	pls_verifica_tel(nr_telefone,:nr_telefone_adic) = ''S'' and :nr_telefone_adic is not null or 	' || pls_util_pck.enter_w ||
									'	pls_verifica_tel(nr_telefone,:nr_telefone) = ''S''	and :nr_telefone is not null or		' || pls_util_pck.enter_w ||
									'	pls_verifica_tel(nr_telefone,:nr_celular) = ''S''	and :nr_celular is not null or		' || pls_util_pck.enter_w ||
									'	pls_verifica_tel(nr_telefone,:nr_telefone_adic) = ''S'' and :nr_telefone_adic is not null)	' || pls_util_pck.enter_w;
				ie_nr_telefone_w := 'S';
				qt_campos_inconsist_w := qt_campos_inconsist_w + 1;
				ds_campos_erro_w := ds_campos_erro_w || 'telefone e ';
			end if;
		when 289120 then -- E-mail
			if	(ds_email_p is not null) then
				ds_restricao_w	:= ds_restricao_w || 	' and 	ds_email = :ds_email ' || pls_util_pck.enter_w;	
				ie_ds_email_w := 'S';
				qt_campos_inconsist_w := qt_campos_inconsist_w + 1;
				ds_campos_erro_w := ds_campos_erro_w || 'e-mail e ';
			end if;
		when 285188 then -- CNPJ
			if	(cd_cgc_p is not null) then
				ds_restricao_w	:= ds_restricao_w || 	' and 	cd_cgc = :cd_cgc ' || pls_util_pck.enter_w;	
				ie_cd_cgc_w := 'S';
				qt_campos_inconsist_w := qt_campos_inconsist_w + 1;
				ds_campos_erro_w := ds_campos_erro_w || 'CNPJ e ';
			end if;
		when 286332 then -- CPF
			if 	(nr_cpf_p is not null) then
				ds_restricao_w	:= ds_restricao_w || 	' and 	nr_cpf = :nr_cpf ' || pls_util_pck.enter_w;	
				ie_nr_cpf_w := 'S';
				qt_campos_inconsist_w := qt_campos_inconsist_w + 1;
				ds_campos_erro_w := ds_campos_erro_w || 'CPF e ';
			end if;
		else
			null;
	end case;
	
	end;
end loop;
close C01;

if	(length(ds_restricao_w) > 0) then
	exec_sql_dinamico(nm_usuario_p, 'truncate table pls_aux_leads_duplicados');	

	ds_sql_w := ds_sql_orig_w || ds_restricao_w;
	
	-- Abre o cursor
	var_cur_w := dbms_sql.open_cursor;
	
	begin	
		-- Obtem o controle padr�o da quantidade de registros que ser� enviada a cada vez para o banco de dados
		qt_registros_max_w := pls_util_cta_pck.qt_registro_transacao_w;
	
		dbms_sql.parse(var_cur_w,ds_sql_w,1);
		
		dbms_sql.bind_variable(var_cur_w, ':nr_seq_solicitacao', nr_seq_solicitacao_p);
		
		if	(ie_dt_nascimento_w = 'S') then
			dbms_sql.bind_variable(var_cur_w, ':dt_nascimento', dt_nascimento_p);
		end if;
		
		if	(ie_nr_telefone_w = 'S') then
			dbms_sql.bind_variable(var_cur_w, ':nr_telefone', nr_telefone_p);
			dbms_sql.bind_variable(var_cur_w, ':nr_celular', nr_celular_p);
			dbms_sql.bind_variable(var_cur_w, ':nr_telefone_adic', nr_telefone_adic_p);
		end if;
		
		if	(ie_ds_email_w = 'S') then
			dbms_sql.bind_variable(var_cur_w, ':ds_email', ds_email_p);
		end if;
		
		if	(ie_cd_cgc_w = 'S') then
			dbms_sql.bind_variable(var_cur_w, ':cd_cgc', cd_cgc_p);
		end if;
		
		if	(ie_nr_cpf_w = 'S') then 
			dbms_sql.bind_variable(var_cur_w, ':nr_cpf', nr_cpf_p);
		end if;

		dbms_sql.define_column(var_cur_w, 1, nr_seq_solicitacao_w);

		var_exec_w := dbms_sql.execute(var_cur_w);
		
		loop
			var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
			exit when var_retorno_w = 0;
			
			-- Atualiza as vari�veis com o resultado do select
			dbms_sql.column_value(var_cur_w, 1, nr_seq_solicitacao_w);

			-- Armazena as solicita��o de lead em uma vari�vel table para depois fazer o insert na tabela tempor�ria de todas as solicita��es de uma vez s�.
			nr_seq_solic_table_w(i)	:= nr_seq_solicitacao_w;	
			
			--Se atingiu o limite, ent�o faz os inserts que est�o na vari�vel table
			if	(i >= pls_util_cta_pck.qt_registro_transacao_w) then
				-- Insere as solicita��es de lead a uma tabela tempor�ria
				pls_insere_leads_tab_temp(nr_seq_solic_table_w);
				
				-- Limpa a vari�vel table
				nr_seq_solic_table_w.delete;
				
				i := 0;
			else
				i := i + 1;
			end if;
			
			qt_pessoas_w := qt_pessoas_w + 1;
		end loop;

		-- Fecha o cursor
		dbms_sql.close_cursor(var_cur_w);
		
		--Se sobrou algum registro na lista, ent�o manda fazer os inserts das solicita��es de lead restantes
		pls_insere_leads_tab_temp(nr_seq_solic_table_w);
	exception
		when others then		
		-- Fechar os cursores que continuam abertos, os cursores que utilizam FOR - LOOP n�o necessitam serem fechados, ser�o fechados autom�ticamente.
		dbms_sql.close_cursor(var_cur_w);
	end;

	if	(qt_pessoas_w > 0) then		
		ds_campos_erro_w := substr(ds_campos_erro_w, 1, length(ds_campos_erro_w) - 3);		
		
		if	(qt_pessoas_w = 1) then
			ds_erro_w := ds_erro_w || wheb_mensagem_pck.get_texto(350007, 'DS_CAMPOS_ERRO=' || ds_campos_erro_w);
			-- Mensagem: J� existe uma solicita��o de lead com esta(e) mesma(o) DS_CAMPOS_ERRO.
		else	
			ds_erro_w := ds_erro_w || wheb_mensagem_pck.get_texto(350006, 'DS_CAMPOS_ERRO=' || ds_campos_erro_w);
			-- Mensagem: J� existem solicita��es de lead com esta(e) mesma(o) DS_CAMPOS_ERRO.
		end if;
	end if;	
end if;	

qt_pessoas_p	:= qt_pessoas_w;
ds_erro_p	:= substr(ds_erro_w,1,255) || chr(13);

end pls_consistir_solicitacao_lead;
/
