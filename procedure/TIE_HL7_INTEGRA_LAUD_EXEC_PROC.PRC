CREATE OR REPLACE
PROCEDURE TIE_HL7_INTEGRA_LAUD_EXEC_PROC(
			nr_prescricao_p			        IN      prescr_medica.nr_prescricao%TYPE,
			nr_seq_proc_p			        IN      prescr_procedimento.nr_sequencia%TYPE,
			nr_seq_interno_proc_p			IN      prescr_procedimento.nr_seq_interno%TYPE,
			nr_acesso_dicom_p			    IN      prescr_procedimento.nr_acesso_dicom%TYPE,
			nr_crm_executor_p			    IN      medico.nr_crm%TYPE,
			ds_ufcrm_executor_p			    IN      medico.uf_crm%TYPE,
			cd_medico_executor_p			IN      medico.cd_pessoa_fisica%TYPE,
			nm_usuario_p			        IN      pessoa_fisica.nm_usuario%TYPE,
			ds_erro_p				        OUT	    VARCHAR2) is 
			
cd_medico_exec_w		medico.cd_pessoa_fisica%TYPE;
nr_prescricao_w	 		prescr_procedimento.nr_prescricao%TYPE;
nr_seq_proc_interno_w	prescr_procedimento.nr_seq_proc_interno%TYPE;
cd_procedimento_w		prescr_procedimento.cd_procedimento%TYPE;
ie_origem_proced_w		prescr_procedimento.ie_origem_proced%TYPE;
qt_procedimento_w		prescr_procedimento.qt_procedimento%TYPE;
cd_setor_atendimento_w	prescr_procedimento.cd_setor_atendimento%TYPE;
dt_prev_execucao_w		prescr_procedimento.dt_prev_execucao%TYPE;
ie_lado_w				prescr_procedimento.ie_lado%TYPE;
nr_seq_proced_w			prescr_procedimento.nr_sequencia%TYPE;
nr_seq_propaci_w		procedimento_paciente.nr_sequencia%TYPE;
nr_atendimento_w		procedimento_paciente.nr_atendimento%TYPE;
dt_entrada_unidade_w	procedimento_paciente.dt_entrada_unidade%TYPE;
qt_existe_w				PLS_INTEGER;
nr_seq_exame_w			prescr_procedimento.nr_seq_exame%TYPE;
cd_acao_w  				PLS_INTEGER;
qt_kit_w 				PLS_INTEGER;
ie_n_gera_kit_w			parametro_integracao_pacs.ie_n_gerar_kit_material%TYPE;

CURSOR c1 IS 
SELECT a.cd_kit_material,
b.dt_prev_execucao,
b.nr_prescricao,
b.nr_sequencia,
'N' ie_auditoria,
0 cd_local_estoque,
null nr_seq_proc_princ,
0 nr_seq_atepacu,
null ie_baixa_sem_estoque,
1 qt_kit,
nvl(b.nr_doc_convenio,0) nr_doc_convenio,
null nr_seq_regra_lanc
FROM proc_interno_kit a
INNER JOIN proc_interno c ON c.nr_sequencia = a.nr_seq_proc_interno
INNER JOIN prescr_procedimento b ON (((b.cd_procedimento = c.cd_procedimento) AND (b.ie_origem_proced = c.ie_origem_proced)) or (b.nr_seq_proc_interno = c.nr_sequencia))
INNER JOIN prescr_medica d ON ((d.nr_prescricao = b.nr_prescricao) AND (d.cd_estabelecimento = a.cd_estabelecimento))
WHERE b.nr_prescricao = nr_prescricao_w
AND b.nr_sequencia = nr_seq_proced_w;


BEGIN
ds_erro_p := '';

IF	((nvl(nr_prescricao_p,0)	> 0) AND (nvl(nr_seq_proc_p,0)	> 0)) OR
	 (nvl(nr_seq_interno_proc_p,0)	> 0) OR
	 (nvl(nr_acesso_dicom_p,0)	> 0) THEN
	 
	SELECT  decode(nvl(MAX(b.ie_altera_status_exec),'N'),'S',1,9),
			nvl(MAX(b.ie_n_gerar_kit_material),'N')
	INTO	cd_acao_w,
			ie_n_gera_kit_w
	FROM	prescr_medica a,
			parametro_integracao_pacs b
	WHERE   a.cd_estabelecimento = b.cd_estabelecimento
	AND		a.nr_prescricao = nr_prescricao_p;
	
	IF	(nvl(nr_prescricao_p,0)	> 0) AND (nvl(nr_seq_proc_p,0)	> 0) THEN
		SELECT	MAX(nr_prescricao),
                MAX(nr_sequencia)
		INTO 	nr_prescricao_w,
                nr_seq_proced_w
		FROM	prescr_procedimento
		WHERE 	nr_prescricao	= nr_prescricao_p
            AND	nr_sequencia	= nr_seq_proc_p;
	ELSIF	(nvl(nr_seq_interno_proc_p,0)	> 0) THEN
		SELECT	Max(nr_prescricao),
                MAX(nr_sequencia)
		INTO 	nr_prescricao_w,
                nr_seq_proced_w
		FROM	prescr_procedimento
		WHERE 	nr_seq_interno	= nr_seq_interno_proc_p;
	ELSE
		SELECT	MAX(nr_prescricao),
                MAX(nr_sequencia)
		INTO 	nr_prescricao_w,
                nr_seq_proced_w
		FROM	prescr_procedimento
		WHERE 	nr_acesso_dicom	= nr_acesso_dicom_p;
	END IF;

	If	(nr_prescricao_w	IS NOT NULL) AND
		(nr_seq_proced_w	IS NOT NULL) THEN
		
		IF	((nr_crm_executor_p	IS NOT NULL) AND (ds_ufCRM_Executor_p IS NOT NULL)) THEN
			SELECT	MAX(cd_pessoa_fisica)
			INTO	cd_medico_exec_w
			FROM	medico
			WHERE	nr_crm		= nr_crm_executor_p
			AND 	uf_crm		= ds_ufCrm_executor_p;
            
		ELSE
			SELECT	MAX(cd_pessoa_fisica)
			INTO	cd_medico_exec_w
			FROM	medico
			WHERE	cd_pessoa_fisica	= cd_medico_Executor_p;
		END IF;
		
		if	(cd_medico_exec_w is not null) or
			((nr_crm_executor_p	is null) and (ds_ufCRM_Executor_p is null) and (cd_medico_Executor_p is null)) then
			
			Select	nr_seq_proc_interno,
				cd_procedimento,
				ie_origem_proced,
				qt_procedimento,
				cd_setor_atendimento,
				dt_prev_execucao,
				ie_lado
			into	nr_seq_proc_interno_w,
				cd_procedimento_w, 
				ie_origem_proced_w,
				qt_procedimento_w, 
				cd_setor_atendimento_w,
				dt_prev_execucao_w,
				ie_lado_w
			from	prescr_procedimento
			where 	nr_prescricao	= nr_prescricao_w
			and	nr_sequencia	= nr_seq_proced_w;
			
			
			select	nvl(max(nr_sequencia),0),
				max(nr_atendimento),
				max(dt_entrada_unidade)
			into	nr_seq_propaci_w,
				nr_atendimento_w,
				dt_entrada_unidade_w
			from	procedimento_paciente
			where	nr_prescricao		= nr_prescricao_w
			and	nr_sequencia_prescricao	= nr_seq_proced_w;
        
			if	(nr_seq_propaci_w = 0) then
				begin
				
				Gerar_Proc_Pac_item_Prescr(	nr_prescricao_w, 
							nr_seq_proced_w, 
							null, 
							null,
							nr_seq_proc_interno_w,
							cd_procedimento_w, 
							ie_origem_proced_w,
							qt_procedimento_w, 
							cd_setor_atendimento_w,
							cd_acao_w, 
							dt_prev_execucao_w,
							substr(wheb_mensagem_pck.get_texto(299648),1,255), 
							cd_medico_exec_w, 
							null,
							ie_lado_w, 
							null);	

            if (ie_n_gera_kit_w = 'N') then
					begin
					select count(1)
						into qt_kit_w
					from proc_interno_kit a
						inner join prescr_procedimento b on b.nr_seq_proc_interno = a.nr_seq_proc_interno
						inner join prescr_medica c on ((c.nr_prescricao = b.nr_prescricao) and (c.cd_estabelecimento = a.cd_estabelecimento))
					where a.nr_seq_proc_interno = nr_seq_proc_interno_w
						and b.nr_prescricao = nr_prescricao_w
						and b.nr_sequencia  = nr_seq_proced_w;

		

					if nvl(qt_kit_w,0) > 0 then
						for r1 in c1 loop
						   gerar_kit_material_exec(r1.cd_kit_material, 
								r1.dt_prev_execucao,
								r1.nr_prescricao,
								r1.ie_auditoria,
								r1.cd_local_estoque,
								r1.nr_seq_proc_princ,
								r1.nr_seq_atepacu,
								r1.ie_baixa_sem_estoque,
								nm_usuario_p,
								r1.qt_kit,
								ds_erro_p,
								r1.nr_doc_convenio,
								r1.nr_seq_regra_lanc);
						end loop;
					end if;
					end;
				end if;
				end;
			else
				begin
				
                    select	count(*)
                    into	qt_existe_w
                    from	procedimento_paciente a,
                        prescr_procedimento b
                    where	a.nr_prescricao			= nr_prescricao_w
                    and	a.nr_sequencia_prescricao	= nr_seq_proced_w
                    and	a.nr_prescricao 		= b.nr_prescricao
                    and	a.nr_sequencia_prescricao 	= b.nr_sequencia
                    and	b.dt_cancelamento is null
                    and	b.ie_status_execucao 		< '20';
                    
                    if (qt_existe_w > 0) then
                        begin
                        
                        select	nvl(max(nr_seq_exame),0)
                        into	nr_seq_exame_w
                        from	prescr_procedimento
                        where	nr_prescricao = nr_prescricao_w
                        and		nr_sequencia  = nr_seq_proced_w;
    
                        atualiza_status_proced_exec(nr_seq_proced_w,
                                                    nr_prescricao_w,
                                                    nr_seq_exame_w,
                                                    nm_usuario_p);
                        
                        end;					
                    end if;
				end;
			end if;
		else
			ds_erro_p	:= ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(282395,null);
		end if;
	else
		ds_erro_p	:= ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(282396,null);
	end if;
else
	ds_erro_p	:= ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(282397,null);
end if;

commit;

end TIE_HL7_INTEGRA_LAUD_EXEC_PROC;
 /