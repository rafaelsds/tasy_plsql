create or replace
procedure pls_cancelar_nf(	nr_sequencia_p	number,
				nm_usuario_p	varchar2,
				ds_observacao_p	varchar2,
				ie_commit_p	varchar2)  is

begin
update	nota_fiscal
set	ie_situacao 		= 9,
	nr_seq_protocolo 	= null, 
	nr_interno_conta	= null,
	nr_seq_exportada	= null,
	ie_status_envio	= null,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao	= sysdate,
	dt_cancelamento		= sysdate,
	nm_usuario_cancel	= nm_usuario_p
where	nr_sequencia 		= nr_sequencia_p;

Gerar_Titulo_Pagar_NF (nr_sequencia_p, nm_usuario_p);

gerar_historico_nota_fiscal(nr_sequencia_p, nm_usuario_p, '1', ds_observacao_p);

if	(ie_commit_p = 'S') then
	commit;
end if;

end;
/
