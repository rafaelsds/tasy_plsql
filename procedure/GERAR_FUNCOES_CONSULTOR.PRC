create or replace
procedure gerar_funcoes_consultor (nr_sequencia_consultor_p number) is

nr_sequencia_w		number(10);
nr_seq_modulo_w		number(10);
ie_tipo_consultor_w	varchar2(5);

cursor c01 is
	select	nr_sequencia,
		nr_seq_mod_impl
	from	com_cons_gest_con_mod
	where	nr_seq_consultor = nr_sequencia_consultor_p
	and	nr_seq_gest_con is null;

begin
select	max(decode(nvl(ie_tipo_consultor,'A'),'CO','O','CP','P','A'))
into	ie_tipo_consultor_w
from	com_canal_consultor
where	nr_sequencia = nr_sequencia_consultor_p;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	nr_seq_modulo_w;
exit when c01%notfound;
	begin
	Com_Gerar_Funcao_Conhecimento(nr_sequencia_w,nr_seq_modulo_w,'Tasy',ie_tipo_consultor_w);	
	end;
end loop;
close c01;

commit;

end gerar_funcoes_consultor;
/