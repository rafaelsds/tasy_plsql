create or replace
procedure pmdm_processar_msg_est_armaz(nr_sequencia_p number) is 


ds_mensagem_w 		clob;
ds_json_j 			philips_json;
ds_mensagem_json_j 	philips_json;
ds_cabecalho_j 		philips_json;
mat_estagio_armaz_j 	philips_json;
IeAction_w          varchar2(30);
ds_erro_w 			varchar2(255);
ie_possui_indice_w	varchar2(1);
reg_integracao_w	gerar_int_padrao.reg_integracao_conv;
ds_erro_p			varchar2(4000);
ds_erros_p			varchar2(4000);
ds_erros_tecnicos_w	varchar2(4000);
ds_sql_dinamico_w	varchar2(4000 char);

NR_SEQUENCIA_W      MAT_ESTAGIO_ARMAZ.NR_SEQUENCIA%TYPE;
DS_ESTAGIO_W        MAT_ESTAGIO_ARMAZ.DS_ESTAGIO%TYPE;
IE_ESTAGIO_W        MAT_ESTAGIO_ARMAZ.IE_ESTAGIO%TYPE;

K_MAT_ESTAGIO_ARMAZ	varchar2(17 char)	:=	'MAT_ESTAGIO_ARMAZ';

function valor_string(campo_j in philips_json, campo_p varchar2, tabela_p varchar2, campo_atributo_p varchar2, ie_valor_padrao_p varchar2, ie_conversao_p varchar2 default 'N')
    return varchar2
      is
        resultFunction varchar2(255);
    begin
	if campo_j.exist(campo_p) then 
		resultFunction := campo_j.get(campo_p).get_string();
	elsif (ie_valor_padrao_p = 'S') then  
		resultFunction := pmdm_obter_valor_padrao(	nm_tabela_p => tabela_p,
													nm_atributo_p => campo_atributo_p,
													nr_seq_visao_p => 0,
													ds_valor_retorno_p => resultFunction);
	else 
		resultFunction := null;
	end if;

	processar_atributo_json(nm_atributo_p => campo_atributo_p,
							nm_tabela_p => tabela_p,
							nm_campo_p => campo_p,
							ds_valor_p => resultFunction,
							ie_conversao_p => ie_conversao_p,
							nr_seq_visao_p => 0,
							ds_valor_retorno_p => resultFunction,
							ds_erro_p => ds_erro_p);

	if (ds_erro_p is not null) then
		ds_erros_p := ds_erros_p || '"' || ds_erro_p || '",' ||chr(10);
	end if;

   	return resultFunction;
    end;  

function gerar_linha_update(	ds_tipo_campo_p in varchar2, 
								campo_j in philips_json, 
								campo_p in varchar2, 
								tabela_p in varchar2, 
								campo_atributo_p in varchar2, 
								ie_busca_padrao_p in varchar2, 
								ie_conversao_p in varchar2 default 'N')
	return varchar2
is
ds_valor_w	varchar2(4000 char);
ds_resultado_w	varchar2(4000 char);
begin
	if campo_j.exist(campo_p) then
		ds_valor_w := valor_string(	campo_j => campo_j,
									campo_p => campo_p,
									tabela_p => tabela_p,
									campo_atributo_p => campo_atributo_p,
									ie_valor_padrao_p => ie_busca_padrao_p,
									ie_conversao_p => ie_conversao_p);
		ds_resultado_w := campo_atributo_p||'='''||ds_valor_w||''','||chr(10);
		if (ds_valor_w is null) then
			ds_resultado_w := campo_atributo_p||'=null,'||chr(10);
		end if;
	end if;
	return ds_resultado_w;
end gerar_linha_update;

begin

select	ds_message
into	ds_mensagem_w
from	intpd_fila_transmissao a
where	a.nr_sequencia = nr_sequencia_p;

ds_json_j 			:= philips_json(ds_mensagem_w);
ds_mensagem_json_j 	:= philips_json(ds_json_j.get('Mensagem'));
ds_cabecalho_j      := philips_json(ds_mensagem_json_j.get('Cabecalho'));
IeAction_w			:= ds_cabecalho_j.get('IeAction').get_string();

if (IeAction_w = 'INSERT') then	
	mat_estagio_armaz_j 	:= philips_json(ds_mensagem_json_j.get(K_MAT_ESTAGIO_ARMAZ));
	ds_estagio_w      := valor_string(mat_estagio_armaz_j,'dsEstagio',K_MAT_ESTAGIO_ARMAZ,'DS_ESTAGIO','S', 'S');
	ie_estagio_w      := valor_string(mat_estagio_armaz_j,'ieEstagio',K_MAT_ESTAGIO_ARMAZ,'IE_ESTAGIO','S', 'S');

	select	MAT_ESTAGIO_ARMAZ_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into mat_estagio_armaz
			(nr_sequencia,
				ds_estagio,
				ie_estagio,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario) 
		values  
		(	nr_sequencia_w,
			ds_estagio_w,
			ie_estagio_w,
			sysdate,
			'intfarm',
			sysdate,
			'intfarm');
elsif (IeAction_w = 'UPDATE') then 

	nr_sequencia_w	:= ds_cabecalho_j.get('cdSequencia').get_string(); 
	select	decode(count(*),0,'N','S')
	into	ie_possui_indice_w
	from	MAT_ESTAGIO_ARMAZ
	where	nr_sequencia = nr_sequencia_w;
	
	if (ie_possui_indice_w = 'N') then
	    ds_erros_p := ds_erros_p || '"Registro nao localizado.",'||chr(10);
	else
		mat_estagio_armaz_j 	:= philips_json(ds_mensagem_json_j.get(K_MAT_ESTAGIO_ARMAZ));

		ds_sql_dinamico_w	:=	'update mat_estagio_armaz set '||chr(10);	

		ds_sql_dinamico_w	:=	ds_sql_dinamico_w	||	gerar_linha_update('string',mat_estagio_armaz_j,'dsEstagio',K_MAT_ESTAGIO_ARMAZ,'DS_ESTAGIO','N', 'S');
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w	||	gerar_linha_update('string',mat_estagio_armaz_j,'ieEstagio',K_MAT_ESTAGIO_ARMAZ,'IE_ESTAGIO','N', 'S');

		ds_sql_dinamico_w	:=	ds_sql_dinamico_w	||	'dt_atualizacao = sysdate,'||chr(10);
		ds_sql_dinamico_w	:=	ds_sql_dinamico_w	||	'nm_usuario = ''intfarm'''||chr(10);

		ds_sql_dinamico_w	:=	ds_sql_dinamico_w	||	chr(10)||' where nr_sequencia = '||nr_sequencia_w;

		begin
			if (instr(ds_sql_dinamico_w, 'where') > 1) then
				execute immediate ds_sql_dinamico_w;
			end if;
		exception  when others then
			ds_erros_tecnicos_w := ds_erros_tecnicos_w||'"Erro: '||replace(substr(to_char(sqlerrm),1,2000), '"')||'.",'||chr(10);
		end;
	end if;
	
elsif (IeAction_w = 'DELETE') then 	
	nr_sequencia_w	:= ds_cabecalho_j.get('cdSequencia').get_string(); 
	select	decode(count(*),0,'N','S')
	into	ie_possui_indice_w
	from	MAT_ESTAGIO_ARMAZ
	where	nr_sequencia = nr_sequencia_w;

	if (ie_possui_indice_w = 'N') then
	    ds_erros_p := ds_erros_p || '"Registro nao localizado.",'||chr(10);
	else
	    delete MAT_ESTAGIO_ARMAZ  
	    where nr_sequencia  = nr_sequencia_w;
	end if;
else
	ds_erros_p := ds_erros_p || '"Acao nao informada.",'||chr(10);
end if;

if (ds_erros_p is null) then
	update 	intpd_fila_transmissao	
	set		ie_status_http = 200,
			ds_message_response =	'{ "Mensagem" : { "Cabecalho" : {' 
								||	'"cdSequencia": "'  	   || nr_sequencia_w || '", '								
								||	'"Servico": "'	       || 'RESPOSTA' ||'",'								
								||	'"DataHora": "'        || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
								||	'"Status": "Sucesso" }}}',
			ie_status = 'S'
	where 	nr_sequencia = nr_sequencia_p;
else
	if ds_erros_p is not null then
		ds_erros_p := REGEXP_REPLACE(ds_erros_p,'.'||chr(10)||'",','.",');
		ds_erros_p := substr(ds_erros_p, 0, LENGTH(ds_erros_p) - 2);
	end if;
	if ds_erros_tecnicos_w is not null then
		ds_erros_tecnicos_w := REGEXP_REPLACE(ds_erros_tecnicos_w,chr(10)||'.",','.",');
		ds_erros_tecnicos_w := substr(ds_erros_tecnicos_w, 0, LENGTH(ds_erros_tecnicos_w) - 2);
	end if;
	rollback;
	update 	intpd_fila_transmissao	
	set		ie_status_http = 400,
			ds_message_response =	'{ "Mensagem" : { "Cabecalho" : {' 
								||	'"cdSequencia": "'      || nr_sequencia_w || '", '								
								||	'"Servico": "'	       || 'RESPOSTA' ||'",'								
								||	'"DataHora": "'        || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
								||	'"Status": "Erro",' || chr(10)
								||	'"Descricao": [' || ds_erros_p || '],' || chr(10)
								||	'"DescricaoTecnica": ['	||	ds_erros_tecnicos_w	||	']}}}',
			ie_status = 'E'
	where 	nr_sequencia = nr_sequencia_p;
end if;

commit;

exception
	when others then
		begin
			if ds_erros_p is not null then
				ds_erros_p := REGEXP_REPLACE(ds_erros_p,'.'||chr(10)||'",','.",');
				ds_erros_p := substr(ds_erros_p, 0, LENGTH(ds_erros_p) - 2);
			end if;
			if ds_erros_tecnicos_w is not null then
				ds_erros_tecnicos_w := REGEXP_REPLACE(ds_erros_tecnicos_w,chr(10)||'.",','.",');
				ds_erros_tecnicos_w := substr(ds_erros_tecnicos_w, 0, LENGTH(ds_erros_tecnicos_w) - 2);
			end if;
			ds_erro_p := SYS.DBMS_UTILITY.FORMAT_ERROR_STACK();
			ds_erro_p := substr(ds_erro_p, 0, LENGTH(ds_erro_p) - 1);
			ds_erros_tecnicos_w := ds_erros_tecnicos_w||'"'||replace(ds_erro_p,'"')||'"';
			rollback;
			update 	intpd_fila_transmissao	
			set		ie_status_http = 400,
					ds_message_response =	'{ "Mensagem" : { "Cabecalho" : {' 
								||	'"cdSequencia": "'  || nr_sequencia_w || '", '								
								||	'"Servico": "'	       || 'RESPOSTA' ||'",'								
								||	'"DataHora": "'        || to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')|| '",'
								||	'"Status": "Erro",' || chr(10)
								||	'"Descricao": [' || ds_erros_p || '],' || chr(10)
								||	'"DescricaoTecnica": ['	||	ds_erros_tecnicos_w	||	']}}}',
			ie_status = 'E'
			where 	nr_sequencia = nr_sequencia_p;
			commit;
		end;
		
end pmdm_processar_msg_est_armaz;
/