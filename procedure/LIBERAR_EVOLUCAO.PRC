create or replace
procedure Liberar_Evolucao
		(cd_evolucao_p		number,
		nm_usuario_p		varchar2) is

nr_atendimento_w	number(10,0);
cd_medico_w		varchar2(10);
ie_medico_w		varchar2(01);
ie_regra_libera_evolucao_w 	varchar2(1);
ie_gera_lancamento_w		varchar2(1);
ie_evolucao_clinica_w		varchar2(3);
cd_estabelecimento_w		number(10);
nr_seq_evento_w			number(10);
cd_tipo_evolucao_w		varchar2(3);
cd_pessoa_fisica_w		varchar2(10);
dt_evolucao_w			date;
qt_reg_w			number(10);
qt_idade_w			number(10);
cd_especialidade_w		number(10);
ie_exige_pessoa_parecer_w	varchar(3);
ie_alta_medica_w		varchar2(10);
dt_alta_medica_w		date;
CD_MOTIVO_ALTA_MEDICA_w		number(10);
nr_seq_classificacao_w		number(10);
nr_seq_grupo_w			number(10);
ie_tipo_vinculo_w		varchar(3);
cd_unidade_w			varchar2(100);
ie_atualiza_prof_w		varchar2(5);
ds_tipo_evol_usuario_w		varchar2(5);
ie_obrigar_pessoa_parecer_w	varchar2(5);
cd_medico_parecer_w			varchar2(10);		
nr_seq_agenda_w				number(10);
cd_tipo_agenda_w			number(10);
cd_especialidade_medico_w EVOLUCAO_PACIENTE.CD_ESPECIALIDADE_MEDICO%TYPE;

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	ie_evento_disp = 'LEV'
	and	(nvl(cd_tipo_evolucao,ie_evolucao_clinica_w)	= ie_evolucao_clinica_w)
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)	
	and	nvl(ie_situacao,'A') = 'A';

Cursor C02 is
	select	nr_seq_grupo,
		ie_tipo_vinculo
	from	tipo_evolucao_pac_grupo
	where	cd_tipo_evolucao = ie_evolucao_clinica_w;
	
begin
cd_estabelecimento_w		:= wheb_usuario_pck.get_cd_estabelecimento;

select	max(nr_atendimento),
	max(cd_medico),
	max(cd_pessoa_fisica),
	max(dt_evolucao),
	max(cd_medico_parecer)
into	nr_atendimento_w,
	cd_medico_w,
	cd_pessoa_fisica_w,
	dt_evolucao_w,
	cd_medico_parecer_w
from	evolucao_paciente
where	cd_evolucao 	= cd_evolucao_p;
qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);


if	(nr_atendimento_w	is not null) then


	select	max(cd_unidade)
	into	cd_unidade_w
	from	resumo_atendimento_paciente_v
	where	nr_atendimento	= nr_atendimento_w;
		
end if;


select 	max(ie_evolucao_clinica)
into	ie_evolucao_clinica_w
from 	evolucao_paciente
where 	cd_evolucao = cd_evolucao_p;

if	(ie_evolucao_clinica_w = 'P') then --Pedido de parecer
	ie_exige_pessoa_parecer_w	:= Obter_Valor_Param_Usuario(281, 715, Obter_perfil_Ativo, nm_usuario_p, cd_estabelecimento_w);
	
	select	cd_medico_parecer,
			cd_especialidade
		into	cd_pessoa_fisica_w,
			cd_especialidade_w
		from	evolucao_paciente
		where	cd_evolucao 	= cd_evolucao_p;
	if	(ie_exige_pessoa_parecer_w = 'S') then
		
		if	(cd_pessoa_fisica_w is null) and 
			(cd_especialidade_w is null) then
			select	count(*)
			into	qt_reg_w
			from	pessoa_destino_evolucao
			where	cd_evolucao	= cd_evolucao_p;
			
			if	(qt_reg_w = 0) then
				--e necessario informar a especialidade, pessoa ou grupo para liberar o Pedido de parecer (Parametro 715).
				Wheb_mensagem_pck.exibir_mensagem_abort(264640);
			end if;
		end if;
		
	end if;
	
	/*  OS 539146 - Niumar Cachoeira
	/* if	(cd_especialidade_w	is not null) then
		select	max(nr_seq_classificacao)
		into	nr_seq_classificacao_w
		from	REGRA_CLASSIF_PARECER
		where	cd_especialidade_medico = cd_especialidade_w;
		
		if	(nr_seq_classificacao_w	is not null) then
			update	atendimento_paciente
			set	nr_seq_classificacao	= nr_seq_classificacao_w
			where	nr_atendimento	= nr_atendimento_w;
		end if;
	end if; */
end if;

select 	nvl(max(ie_obrigar_pessoa_parecer),'N') 
into	ie_obrigar_pessoa_parecer_w
from 	tipo_evolucao 
where 	CD_TIPO_EVOLUCAO = ie_evolucao_clinica_w;

if	(cd_medico_parecer_w is null) and
	(ie_obrigar_pessoa_parecer_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(256268);
end if;

Update	evolucao_paciente
set 	dt_liberacao 	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	CD_UNIDADE	= nvl(CD_UNIDADE,cd_unidade_w)
where 	cd_evolucao 	= cd_evolucao_p;

select 	max(IE_ATUALIZAR_PROF) 
into	ie_atualiza_prof_w
from 	tipo_evolucao 
where 	CD_TIPO_EVOLUCAO = ie_evolucao_clinica_w;

ds_tipo_evol_usuario_w	:= obter_cod_usuario_evol_atend(nm_usuario_p);

if	(ie_atualiza_prof_w = 'S') and
	(nr_atendimento_w is not null) and
	(ds_tipo_evol_usuario_w is not null) then
	atualizar_atend_profissional(nr_atendimento_w, cd_medico_w, ds_tipo_evol_usuario_w, nm_usuario_p);
end if;

select 	nvl(max('S'),'N')
into	ie_medico_w
from	Medico
where 	cd_pessoa_fisica	= cd_medico_w;


select	max(ie_alta_medica),
	max(CD_MOTIVO_ALTA_MEDICA)
into	ie_alta_medica_w,
	CD_MOTIVO_ALTA_MEDICA_w
from	tipo_evolucao
where	CD_TIPO_EVOLUCAO	= ie_evolucao_clinica_w;


select 	nvl(max(ie_regra_libera_evolucao),'M')
into	ie_regra_libera_evolucao_w
from 	parametro_faturamento
where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_evolucao_clinica_w,'0') <> '0') then
	select 	nvl(max(ie_gera_lancto_auto),'N')
	into	ie_gera_lancamento_w
	from 	tipo_evolucao
	where 	cd_tipo_evolucao = ie_evolucao_clinica_w;
else
	ie_gera_lancamento_w:= 'N';
end if;




if	(((ie_regra_libera_evolucao_w = 'M') and (ie_medico_w = 'S')) or 
	 ((ie_regra_libera_evolucao_w = 'R') and (ie_gera_lancamento_w = 'S'))) then	 
	gerar_lancamento_automatico(nr_atendimento_w, null, 123, 
		nm_usuario_p, cd_evolucao_p, cd_medico_w, null,null,null,null);
end if;

begin
	select 	max(nr_seq_agenda),
			max(cd_tipo_agenda)
	into	nr_seq_agenda_w,
			cd_tipo_agenda_w
	from	evolucao_paciente_agenda
	where 	cd_evolucao = cd_evolucao_p;
exception
when others then
	nr_seq_agenda_w := null;
	cd_tipo_agenda_w := null;
end;

executar_evento_agenda_atend(nr_atendimento_w,'LEV',obter_estab_atend(nr_atendimento_w),nm_usuario_p,ie_evolucao_clinica_w, null, nr_seq_agenda_w, cd_tipo_agenda_w);


open C01;
loop
fetch C01 into	
	nr_seq_evento_w;
exit when C01%notfound;
	begin
	gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,wheb_usuario_pck.get_nm_usuario,null,null,null,null,null,null,null,null,null,null,null,null,sysdate,cd_evolucao_p);
	end;
end loop;
close C01;


if	(ie_alta_medica_w	= 'S') and
	(nr_atendimento_w	is not null) and
	(CD_MOTIVO_ALTA_MEDICA_w	is not null)then
	select	max(DT_ALTA_MEDICO)
	into	dt_alta_medica_w
	from	atendimento_paciente
	where	nr_atendimento	=nr_atendimento_w;
	
	if	(dt_alta_medica_w	is null) then
		Gerar_Estornar_Alta_Medica(	nr_atendimento_w,
						'A',
						CD_MOTIVO_ALTA_MEDICA_w,
						sysdate,
						null,
						nm_usuario_p,
						'N');
	end if;
end if;


begin
enviar_comunic_tipo_evol(cd_evolucao_p,nm_usuario_P);
exception
when others then 
	null;
end;

open C02;
loop
fetch C02 into	
	nr_seq_grupo_w,
	ie_tipo_vinculo_w;
exit when C02%notfound;
	begin
	if	(nvl(ie_tipo_vinculo_w,'V') = 'D') then	
		desvincular_atendimento_grupo(nr_atendimento_w, nm_usuario_p, nr_seq_grupo_w);	
	else
		if	(nr_atendimento_w is null) or (nr_atendimento_w = 0) then
			vincular_atendimento_grupo(nr_atendimento_w,cd_pessoa_fisica_w,1,nr_seq_grupo_w,nm_usuario_p);
		else
			vincular_atendimento_grupo(nr_atendimento_w,cd_pessoa_fisica_w,0,nr_seq_grupo_w,nm_usuario_p);
		end if;
	end if;
	end;
end loop;
close C02;


-- WorkList AtePac_WL (357)
wl_gerar_finalizar_tarefa('CN','F',nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p,sysdate,'S',ie_evolucao_clinica_w,null,null,null,null,null,null,null,null,null,null,dt_evolucao_w,null,cd_evolucao_p);



if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

begin
	gravar_integracao_regulacao(501, 'nr_atendimento='||nr_atendimento_w||';'); 
exception
when others then
	null;	
end;

select cd_especialidade_medico
into cd_especialidade_medico_w
from evolucao_paciente
where cd_evolucao = cd_evolucao_p;

ATUALIZAR_EV_LINHA_CUIDADO('EV', 'EVOLUCAO_PACIENTE', cd_evolucao_p, 'cd_especialidade_medico_w='||nvl(cd_especialidade_medico_w, 0)||';ie_evolucao_clinica_w='||nvl(ie_evolucao_clinica_w, 0)||';',cd_pessoa_fisica_w, nm_usuario_p);

end Liberar_Evolucao; 
/
