create or replace
procedure ptu_gerar_A300
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				ie_tipo_movimento_p	varchar2,
				nm_arquivo_p		varchar2,
				nm_usuario_p		varchar2) is

nm_procedure_a300_w		pls_parametros.nm_procedure_atuarial%type 	:= null;
nr_seq_classificacao_w		ptu_mov_produto_lote.nr_seq_classificacao%type;		
begin

if 	(nvl(nr_seq_lote_p,0) <> 0) then
	select 	nr_seq_classificacao 
	into 	nr_seq_classificacao_w
	from 	ptu_mov_produto_lote
	where 	nr_sequencia 	= nr_seq_lote_p;
end if;

begin
select 	nm_procedure_utl
into 	nm_procedure_a300_w
from 	pls_sca_classificacao
where 	cd_estabelecimento = cd_estabelecimento_p
and 	nr_sequencia = nr_seq_classificacao_w;
exception
when others then
nm_procedure_a300_w := null;
end;

if	(nm_procedure_a300_w is not null)then
	execute immediate ' begin '||nm_procedure_a300_w||'('''||nr_seq_lote_p||''','''||nm_arquivo_p||''','''||ie_tipo_movimento_p||'''); end;';
end if;

end ptu_gerar_A300;
/