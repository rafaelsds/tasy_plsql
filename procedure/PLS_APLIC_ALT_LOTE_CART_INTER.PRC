create or replace
procedure pls_aplic_alt_lote_cart_inter
			(	nr_seq_lote_p			number,
				cd_estabelecimento_p		number,
				nr_seq_motivo_via_p		number,
				nm_usuario_p			varchar2) is

nr_seq_via_cart_inter_w		number(10);
nr_via_solicitacao_w		number(5);
dt_validade_carteira_w		date;
dt_emissao_w			date;
nr_seq_segurado_w		number(10);
ie_alterou_via_adic_w		varchar2(10);
-------------------------------------------------------------------------
nr_seq_carteira_w		number(10);
nr_via_solicitacao_atual_w	number(5);
dt_validade_carteira_atual_w	date;
ie_gerar_valor_w		varchar2(10);
vl_via_adicional_w		Number(15,2) := 0;
nr_seq_regra_via_w		Number(10);
pr_via_adicional_w		number(7,4);
nr_seq_segurado_preco_w		Number(10);
vl_preco_pre_w			Number(15,2) := 0;
-------------------------------------------------------------------------------
ds_trilha1_w			pls_segurado_carteira.ds_trilha1%type;
ds_trilha2_w			pls_segurado_carteira.ds_trilha2%type;
ds_trilha3_w			pls_segurado_carteira.ds_trilha3%type;
ds_trilha_qr_code_w		pls_segurado_carteira.ds_trilha_qr_code%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_via_cart_inter
	where	nr_seq_lote	= nr_seq_lote_p
	and	nr_seq_segurado	is not null;

begin

select	ie_gerar_valor
into	ie_gerar_valor_w
from	pls_motivo_via_adicional
where	nr_sequencia	= nr_seq_motivo_via_p;

open C01;
loop
fetch C01 into
	nr_seq_via_cart_inter_w;
exit when C01%notfound;
	begin
	ie_alterou_via_adic_w	:= 'N';
	vl_via_adicional_w	:= 0;
	nr_seq_regra_via_w	:= null;
	
	select	nr_via_solicitacao,
		dt_validade_carteira,
		dt_emissao,
		nr_seq_segurado,
		dt_validade_anterior,
		nr_via_anterior
	into	nr_via_solicitacao_w,
		dt_validade_carteira_w,
		dt_emissao_w,
		nr_seq_segurado_w,
		dt_validade_carteira_atual_w,
		nr_via_solicitacao_atual_w
	from	pls_via_cart_inter
	where	nr_sequencia	= nr_seq_via_cart_inter_w;
	
	select	nr_sequencia
	into	nr_seq_carteira_w
	from	pls_segurado_carteira
	where	nr_seq_segurado	= nr_seq_segurado_w;
	
	if	((nr_via_solicitacao_w <> nr_via_solicitacao_atual_w) or
		(dt_validade_carteira_w	<> dt_validade_carteira_atual_w)) then
		
		if	(nr_via_solicitacao_w <> nr_via_solicitacao_atual_w) then
			ie_alterou_via_adic_w	:= 'S';
		end if;
			insert into pls_segurado_cart_ant
				(	nr_sequencia, nm_usuario, dt_atualizacao, nm_usuario_nrec, dt_atualizacao_nrec,
					cd_usuario_ant, dt_validade, dt_inicio_vigencia,nr_seq_segurado, dt_alteracao,
					ds_observacao,ie_status_carteira, nr_via_anterior, ie_sistema_anterior,nr_seq_regra_via,
					vl_via_adicional, nr_seq_motivo_via,nm_usuario_solicitacao, dt_solicitacao,
					nr_seq_lote_emissao,ds_trilha1, ds_trilha2, ds_trilha3,ds_trilha_qr_code,
					dt_desbloqueio, nm_usuario_desbloqueio, ie_tipo_desbloqueio)
			(select		pls_segurado_cart_ant_seq.nextval, nm_usuario_p, sysdate,nm_usuario_p, sysdate,
					cd_usuario_plano, dt_validade_carteira, dt_inicio_vigencia,nr_seq_segurado,
					sysdate, ds_observacao,'V', nr_via_solicitacao, 'N',nr_seq_regra_via,
					vl_via_adicional, nr_seq_motivo_via_p,nm_usuario_solicitante, dt_solicitacao,
					nr_seq_lote_emissao,ds_trilha1, ds_trilha2, ds_trilha3,ds_trilha_qr_code,
					dt_desbloqueio, nm_usuario_desbloqueio, ie_tipo_desbloqueio
				from	pls_segurado_carteira
				where	nr_sequencia	= nr_seq_carteira_w);
		
		if	(ie_alterou_via_adic_w = 'S') then
			if 	(ie_gerar_valor_w = 'S') then
				pls_obter_regra_via_adic(null, null, null, nr_via_solicitacao_w, 'N', nm_usuario_p, cd_estabelecimento_p, sysdate, nr_seq_regra_via_w, vl_via_adicional_w, pr_via_adicional_w);
			end if;
			
			if	(nvl(pr_via_adicional_w,0) <> 0) then
				select	max(a.nr_sequencia)
				into	nr_seq_segurado_preco_w
				from	pls_segurado_preco	a,
					pls_segurado		b
				where	a.nr_seq_segurado	= b.nr_sequencia
				and	b.nr_sequencia		= nr_seq_segurado_w
				and	a.dt_liberacao	is not null
				and	sysdate		>= trunc(a.dt_reajuste, 'month');
				
				select	vl_preco_atual
				into	vl_preco_pre_w
				from	pls_segurado_preco
				where	nr_sequencia	= nr_seq_segurado_preco_w;
				
				vl_via_adicional_w	:= (pr_via_adicional_w * vl_preco_pre_w) / 100;
			end if;
		end if;
		
		delete	pls_segurado_cart_estagio
		where	nr_seq_cartao_seg	= nr_seq_carteira_w
		and	ie_estagio_emissao	= '1';
		
		update	pls_segurado_carteira
		set	nr_via_solicitacao 	= nr_via_solicitacao_w,
			ie_situacao 		= 'P',
			nm_usuario 		= nm_usuario_p,
			dt_atualizacao 		= sysdate,
			vl_via_adicional 	= vl_via_adicional_w,
			nr_seq_regra_via 	= nr_seq_regra_via_w,
			nr_seq_motivo_via 	= decode(ie_alterou_via_adic_w,'S',nr_seq_motivo_via_p,null),
			nm_usuario_solicitante	= nm_usuario_p,
			dt_solicitacao		= dt_emissao_w,
			ds_observacao		= wheb_mensagem_pck.get_texto(1127395),
			nr_seq_lote_emissao	= null,
			ie_processo		= 'M',
			dt_validade_carteira	= dt_validade_carteira_w
		where	nr_sequencia 		= nr_seq_carteira_w;
		
		pls_alterar_estagios_cartao(nr_seq_carteira_w,sysdate,1,cd_estabelecimento_p,nm_usuario_p);
		
		pls_obter_trilhas_cartao(nr_seq_segurado_w, ds_trilha1_w, ds_trilha2_w, ds_trilha3_w,ds_trilha_qr_code_w, nm_usuario_p);
		
		update	pls_segurado_carteira
		set	ds_trilha1		= ds_trilha1_w,
			ds_trilha2		= ds_trilha2_w,
			ds_trilha3		= ds_trilha3_w,
			ds_trilha_qr_code	= ds_trilha_qr_code_w
		where	nr_sequencia 		= nr_seq_carteira_w;
	end if;
	end;
end loop;
close C01;

update	pls_lote_via_cart_inter
set	ie_status		= '3',
	dt_aplicacao		= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_lote_p;

commit;

end pls_aplic_alt_lote_cart_inter;
/