create or replace
procedure sup_liberar_kit_basico(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 

qt_existe_w	number(10);
begin
select	count(*)
into	qt_existe_w
from	kit_estoque
where	nr_seq_reg_kit = nr_sequencia_p;

if	(qt_existe_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(266290);
	--'N�o existe kit material vinculados ao kit b�sico! Verifique. #@#@');
end if;

update 	kit_estoque_reg 
set 	nm_usuario_lib = nm_usuario_p, 
	dt_liberacao = sysdate 
where 	nr_sequencia = nr_sequencia_p;

commit;
end sup_liberar_kit_basico;
/