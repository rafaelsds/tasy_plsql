create or replace
procedure Consiste_valor_conta_prot(	nr_seq_protocolo_p	in	Number,
					nm_usuario_p		in	Varchar2,
					ds_retorno_p		out	Varchar2) is 


nr_interno_conta_w	number(10,0);
vl_conta_resumo_w	number(15,2);
vl_conta_w		number(15,2);
ds_retorno_w		varchar2(255):= '';
ie_diferenca_w		varchar2(1):= 'N';
					
Cursor C01 is
	select	nr_interno_conta
	from	conta_paciente
	where	nr_seq_protocolo = nr_seq_protocolo_p
	order by nr_interno_conta;					
					
begin

delete
from 	w_conta_resumo_dif
where 	nm_usuario = nm_usuario_p 
and 	nr_seq_protocolo = nr_seq_protocolo_p;

open C01;
loop
fetch C01 into	
	nr_interno_conta_w;
exit when C01%notfound;
	begin
	vl_conta_resumo_w	:= 0;
	vl_conta_w		:= 0;
	
	select	obter_valor_conta(nr_interno_conta_w, 0)
	into	vl_conta_resumo_w
	from 	dual;
	
	select 	sum(vl_item)
	into	vl_conta_w
	from 	protocolo_convenio_item_v4
	where 	nr_interno_conta = nr_interno_conta_w
	and	((ie_proc_mat = 2) or (nr_seq_proc_pacote is null or nr_sequencia <> nr_seq_proc_pacote));
	
	if	(nvl(vl_conta_resumo_w,0) <> nvl(vl_conta_w,0)) then
		insert into w_conta_resumo_dif
					(NR_SEQ_PROTOCOLO,
					DT_ATUALIZACAO,
					NM_USUARIO,
					DT_ATUALIZACAO_NREC,
					NM_USUARIO_NREC,
					VL_CONTA_RESUMO,
					VL_CONTA,
					NR_INTERNO_CONTA)
				values	(nr_seq_protocolo_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					vl_conta_resumo_w,
					vl_conta_w,
					nr_interno_conta_w);
		ie_diferenca_w:= 'S';
	end if;
	
	end;
end loop;
close C01;

commit;

if	(ie_diferenca_w = 'S') then
	ds_retorno_w:= wheb_mensagem_pck.get_texto(299139);
end if;

ds_retorno_p:= ds_retorno_w;

end Consiste_valor_conta_prot;
/
