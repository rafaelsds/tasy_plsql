create or replace
procedure pls_inserir_glosa_anexo_guia(	cd_glosa_p 		varchar2,
					nr_seq_lote_guia_aut_p	number,
					nr_seq_lote_proc_aut_p	number,
					nr_seq_lote_mat_aut_p	number,	
					nm_usuario_p		varchar2) is
					
nr_seq_motivo_glosa_w	number(10);

begin

select	max(nr_sequencia)
into	nr_seq_motivo_glosa_w
from	tiss_motivo_glosa
where	cd_motivo_tiss	= cd_glosa_p;

insert into pls_lote_anexo_glosa_aut (	nr_sequencia,
					nm_usuario,
					nm_usuario_nrec,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nr_seq_lote_guia_aut,
					nr_seq_lote_proc_aut,
					nr_seq_lote_mat_aut,
					nr_seq_motivo_glosa)
				values(	pls_lote_anexo_glosa_aut_seq.nextval,
					nm_usuario_p,
					nm_usuario_p,
					sysdate,
					sysdate,
					nr_seq_lote_guia_aut_p,
					nr_seq_lote_proc_aut_p,
					nr_seq_lote_mat_aut_p,				
					nr_seq_motivo_glosa_w);	
					
commit;

end pls_inserir_glosa_anexo_guia;
/
