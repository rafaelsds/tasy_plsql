create or replace
procedure pls_alt_descricao_itens_ptu(	nr_seq_conta_pos_estab_p	pls_conta_pos_estabelecido.nr_sequencia%type,
					ds_descricao_p			Varchar2,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			Varchar2) is

begin
if	(nr_seq_conta_pos_estab_p is not null) then
	update	pls_conta_pos_estabelecido
	set	ds_item_ptu 		= ds_descricao_p
	where	nr_sequencia		= nr_seq_conta_pos_estab_p;

	commit;
end if;

end  pls_alt_descricao_itens_ptu;
/
