create or replace
procedure obter_nr_interno_protocolo	(	nr_sequencia_p		in number,
					dt_referencia_p		in date,
					ds_controle_p		out varchar2 ) is

nr_seq_interno_w		number(10);
nr_ano_w			number(10);
ds_controle_w		varchar(80);
qt_registros_w		number(10);

begin

select	nvl(count(*),0)
into	qt_registros_w
from	param_protocolo_doc;

if (qt_registros_w = 0) then
	insert into param_protocolo_doc( 	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_interno,
					nr_ano) 
			values (		param_protocolo_doc_seq.NextVal,
					sysdate,
					'Tasy',
					sysdate,
					'Tasy',
					0,
					2011);
	commit;
end if;

select	nvl(nr_seq_interno,0),
	nvl(nr_ano,2011)
into	nr_seq_interno_w,
	nr_ano_w
from	param_protocolo_doc;

if (to_number(to_char(dt_referencia_p,'yyyy')) = nr_ano_w) then	-- Se estiver dentro do mesmo ano, incrementa;
	ds_controle_w := (nr_seq_interno_w + 1) || '/' || nr_ano_w;
	
	update param_protocolo_doc set nr_seq_interno = (nr_seq_interno_w + 1);
	commit;
	
else
	ds_controle_w :=  1 || '/' || (nr_ano_w + 1);	-- se for ano diferente, reinicia a contagem
	
	update param_protocolo_doc set nr_seq_interno = 1, nr_ano = nr_ano_w + 1;
	commit;	
	
end if;

ds_controle_p := ds_controle_w;

end obter_nr_interno_protocolo;
/