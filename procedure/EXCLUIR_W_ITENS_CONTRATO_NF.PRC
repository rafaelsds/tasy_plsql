create or replace
procedure excluir_w_itens_contrato_nf(		nr_sequencia_p		number,
					ie_excluir_todos_p		varchar2,
					nm_usuario_p		varchar2) is 

begin

if	(ie_excluir_todos_p = 'N') then
	
	delete from w_itens_contrato_nf
	where nr_sequencia = nr_Sequencia_p;
else
	delete from w_itens_contrato_nf
	where nm_usuario = nm_usuario_p;

end if;
	
commit;

end excluir_w_itens_contrato_nf;
/