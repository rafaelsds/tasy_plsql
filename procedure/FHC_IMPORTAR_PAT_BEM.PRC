create or replace
procedure fhc_importar_pat_bem is 

nr_sequencia_w		number(10);
ds_erro_w		varchar2(500);
qt_comit_w		number(5);
qt_registro_w		number(5);

cursor c01 is
select	cd_bem,
	cd_centro_,
	cd_conta_c,
	cd_estabel,
	cd_moeda,
	ds_bem,
	dt_aquisic,
	dt_inicio_,
	ie_imobili,
	ie_situaca,
	decode(ie_tipo_va,'R','O') ie_tipo_va,
	nr_seq_loc,
	nr_seq_tip,
	tx_deprec,
	vl_origina
from	w_fhc_pat_bem a;

vet01	c01%rowtype;


begin

qt_comit_w	:= 0;

delete	log_tasy
where	cd_log		= 012010
and	nm_usuario	= 'Importacao';

commit;


open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_registro_w
	from	centro_custo a
	where	a.cd_centro_custo	= vet01.cd_centro_;
	
	if	(qt_registro_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(281880,'CD_CENTRO='||vet01.cd_centro_ );
	end if;
	
	select	count(*)
	into	qt_registro_w
	from	conta_contabil a
	where	a.cd_conta_contabil	= vet01.cd_conta_c;
	
	if	(qt_registro_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(281882,'CD_CONTA='||vet01.cd_conta_c);
	end if;
	
	select	count(*)
	into	qt_registro_w
	from	moeda a
	where	a.cd_moeda	= vet01.cd_moeda;
	
	if	(qt_registro_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(281883,'CD_MOEDA='||vet01.cd_moeda);
	end if;
	
	select	count(*)
	into	qt_registro_w
	from	pat_local a
	where	a.nr_sequencia	= vet01.nr_seq_loc;
	
	if	(qt_registro_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(281884,'NR_SEQ_LOC='||vet01.nr_seq_loc);
	end if;
	
	select	count(*)
	into	qt_registro_w
	from	pat_tipo_bem a
	where	a.nr_sequencia	= vet01.nr_seq_tip;
	
	if	(qt_registro_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(281885,'NR_SEQ_TIP='||vet01.nr_seq_tip);
	end if;
	
	begin
	
	select	pat_bem_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into pat_bem (	
		nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		cd_bem,
		ds_bem,
		dt_aquisicao,
		nr_seq_tipo,
		nr_seq_local,
		ie_imobilizado,
		ie_situacao,
		cd_conta_contabil,
		cd_centro_custo,
		ie_tipo_valor,
		tx_deprec,
		vl_original,
		cd_moeda,
		dt_inicio_uso,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_sequencia_w,
		vet01.cd_estabel,
		sysdate,
		'Importacao',
		vet01.cd_bem,
		vet01.ds_bem,
		vet01.dt_aquisic,
		vet01.nr_seq_tip,
		vet01.nr_seq_loc,
		vet01.ie_imobili,
		vet01.ie_situaca,
		vet01.cd_conta_c,
		vet01.cd_centro_,
		vet01.ie_tipo_va,
		vet01.tx_deprec,
		vet01.vl_origina,
		vet01.cd_moeda,
		vet01.dt_inicio_,
		sysdate,
		'Importacao');
		
	exception when others then
		
		ds_erro_w	:= substr(sqlerrm(sqlcode),1,500);
		insert into log_tasy (	
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values(	sysdate,
			'Importacao',
			012010,
			'cd_bem :'||vet01.cd_bem||' - '||ds_erro_w);		
		
	end;
	if	(qt_comit_w >= 500) then
		qt_comit_w	:= 0;
		commit;
	else
		qt_comit_w	:= qt_comit_w + 1;
		
	end if;
	end;
end loop;
close C01;

commit;

end fhc_importar_pat_bem;
/