Create or Replace
procedure consiste_regra_horario_plano(
                         	nr_atendimento_p	Number,
				cd_convenio_p		Number,
				cd_plano_p		varchar2,
				cd_setor_atendimento_p	Number,
				ds_erro_p	out	Varchar2,
				cd_empresa_p		number) IS

ie_tipo_atendimento_w		number(3);
dt_entrada_w			Date;
ie_atende_w			Varchar2(1);
ie_ver_atende_w			varchar2(1) := 'S';
cd_setor_atendimento_w		number(5);
ie_dia_semana_w			varchar2(3);
ie_dia_semana_entrada_w		varchar2(3);
cd_medico_resp_w		varchar2(10);
dt_entrada_ww			date;

cd_estabelecimento_w		number(4,0);
ie_feriado_w			varchar2(1)		:= 'N';
qt_dia_feriado_w		Number(3)		:= 0;

Cursor C01 is
	select	ie_atende
	from	regra_horario_plano
	where	cd_convenio	= cd_convenio_p
	and	cd_plano	= cd_plano_p
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)		= ie_tipo_atendimento_w
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
	and	nvl(cd_pessoa_fisica, cd_medico_resp_w)			= cd_medico_resp_w
	and	((nvl(upper(ie_feriado),'N') = 'N') or (upper(ie_feriado) = ie_feriado_w))
	and	((nvl(ie_dia_semana, ie_dia_semana_entrada_w) 		= ie_dia_semana_entrada_w) or
		 ((ie_dia_semana = '9') and (ie_dia_semana_entrada_w between 2 and 6)))
	and	dt_entrada_w between 	to_date('01/01/2000 '||to_char(hr_inicio,'hh24:mi')||':00','dd/mm/yyyy hh24:mi:ss') and
					to_date('01/01/2000 '||to_char(hr_fim,'hh24:mi')||':00','dd/mm/yyyy hh24:mi:ss')
	and	nvl(cd_empresa,nvl(cd_empresa_p,0))			= nvl(cd_empresa_p,0)
	and	nvl(cd_estabelecimento,nvl(cd_estabelecimento_w,1))	= nvl(cd_estabelecimento_w,1)
	order by ie_atende;



BEGIN

begin
select	ie_tipo_atendimento,
	to_date('01/01/2000 '||to_char(dt_entrada,'hh24:mi')||':00','dd/mm/yyyy hh24:mi:ss'),
	nvl(cd_setor_atendimento_p,nvl(obter_setor_atendimento(nr_atendimento),0)),
	cd_medico_resp,
	dt_entrada,
	cd_estabelecimento
into	ie_tipo_atendimento_w,
	dt_entrada_w,
	cd_setor_atendimento_w,
	cd_medico_resp_w,
	dt_entrada_ww,
	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;
exception
when no_data_found then
	ie_tipo_atendimento_w	:= 0;
	dt_entrada_w		:= sysdate;
	cd_setor_atendimento_w	:= 0;
	cd_estabelecimento_w	:= 1;
end;

/* Verifica se � Feriado */
select 	count(*)
into 	qt_dia_feriado_w
from 	feriado
where 	cd_estabelecimento = nvl(cd_estabelecimento_w, cd_estabelecimento)
and	to_char(dt_feriado,'dd/mm/yyyy')  = to_char(dt_entrada_ww,'dd/mm/yyyy');

if	(qt_dia_feriado_w > 0) then
	ie_feriado_w:= 'S';
else
	ie_feriado_w:= 'N';
end if;

select	pkg_date_utils.get_WeekDay(dt_entrada_ww)
into	ie_dia_semana_entrada_w
from	dual;

open C01;
loop
	fetch C01 into
		ie_atende_w;
	exit when C01%notfound;
	begin
	ie_ver_atende_w	:= 'N';
	end;
end loop;
close C01;

if	(ie_ver_atende_w = 'N') then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(279989);
end if;

end consiste_regra_horario_plano;
/