create or replace
procedure pls_consist_repas_abrang_conta
				(	nr_seq_segurado_p			Number,
					nr_seq_operadora_atend_p	Number,
					nm_usuario_p				Varchar2,
					ie_area_coberta_p	out		Varchar2,
					ie_tipo_conta_p				varchar2) is 

qt_repasse_w				Number(10);
ie_tipo_repasse_w			Varchar2(2);
nr_seq_congenere_w			Number(10);
ie_atend_operadora_repasse_w		Varchar2(2);
nr_seq_plano_w				Number(10);
ie_tipo_segurado_w			varchar2(2);

begin
ie_area_coberta_p	:= 'S';

-- Verificação de tipo de repasse do beneficiário e da regra de autorização
begin
	select	ie_tipo_repasse
	into	ie_tipo_repasse_w
	from	pls_segurado_repasse
	where	nr_seq_segurado	= nr_seq_segurado_p;
	
	select	nr_seq_plano
	into	nr_seq_plano_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
	
	begin
	select	ie_tipo_segurado
	into	ie_tipo_segurado_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;
	exception
	when others then
		ie_tipo_segurado_w	:= 'X';
	end;
	
	select	nvl(ie_atend_operadora_repasse,'N')
	into	ie_atend_operadora_repasse_w
	from	pls_regra_intercambio_aut
	where	((ie_tipo_repasse	is null)
	or	(ie_tipo_repasse	= ie_tipo_repasse_w));
exception
when others then
	ie_atend_operadora_repasse_w	:= 'N';
end;
	
if	(ie_atend_operadora_repasse_w	= 'S') then
	begin
	select	nvl(nr_seq_congenere_atend,nr_seq_congenere)
	into	nr_seq_congenere_w
	from	pls_segurado_repasse
	where	nr_seq_segurado	= nr_seq_segurado_p;
	exception
	when others then
		nr_seq_congenere_w	:= 0;
	end;
	if	(ie_tipo_conta_p = 'I')	then
		if	(nr_seq_operadora_atend_p <> nr_seq_congenere_w)	then
			ie_area_coberta_p := 'N';
		end if;
	else
			ie_area_coberta_p := 'N';
	end if;
end if;

end pls_consist_repas_abrang_conta;
/
