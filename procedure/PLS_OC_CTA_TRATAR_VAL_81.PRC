create or replace
procedure pls_oc_cta_tratar_val_81(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_selecao_ocor_cta.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Validar se o profissional executante est� vinculado ao prestador participante

-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjkruk OS 734664
Altera��o: Cria��o da procedure
------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
dados_tb_selecao_w		pls_tipos_ocor_pck.dados_table_selecao_ocor;
nr_idx_w			pls_integer;
ie_valido_w			varchar2(1);

cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.ie_valida ie_valida,
		nvl(a.ie_tipo_prestador,'P') ie_tipo_prestador
	from	pls_oc_cta_val_exec_partic a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_sequencia%type,
		ie_tipo_prestador_pc	pls_oc_cta_val_exec_partic.ie_tipo_prestador%type) is
	select 	x.nr_sequencia nr_seq_selecao,
		(select	Count(1)
		 from	pls_prestador_medico y
		 where	y.nr_seq_prestador = a.nr_seq_prestador
		 and	y.cd_medico = a.cd_medico
		 and	y.ie_situacao = 'A') qt_registros,
		(select	max(y.dt_inclusao)
		 from	pls_prestador_medico y
		 where	y.nr_seq_prestador = a.nr_seq_prestador
		 and	y.cd_medico = a.cd_medico
		 and	y.ie_situacao = 'A') dt_inclusao,
		(select	max(y.dt_exclusao)
		 from	pls_prestador_medico y
		 where	y.nr_seq_prestador = a.nr_seq_prestador
		 and	y.cd_medico = a.cd_medico
		 and	y.ie_situacao = 'A') dt_exclusao,
		a.dt_procedimento,
		a.dt_procedimento_imp
	from	pls_oc_cta_selecao_ocor_v	x,
		pls_proc_participante_ocor_v	a
	where	x.ie_valido		= 'S'
	and	x.nr_id_transacao	= nr_id_transacao_pc
	and	a.nr_seq_conta_proc	= x.nr_seq_conta_proc	
	and	a.nr_seq_prestador	is not null
	and	a.cd_medico		is not null
	and	ie_tipo_prestador_pc = 'P'
	union all
	select 	x.nr_sequencia nr_seq_selecao,
		(select	Count(1)
		 from	pls_prestador_medico y
		 where	y.nr_seq_prestador = c.nr_seq_prestador_exec
		 and	y.cd_medico = a.cd_medico
		 and	y.ie_situacao = 'A') qt_registros,
		(select	max(y.dt_inclusao)
		 from	pls_prestador_medico y
		 where	y.nr_seq_prestador = c.nr_seq_prestador_exec
		 and	y.cd_medico = a.cd_medico
		 and	y.ie_situacao = 'A') dt_inclusao,
		(select	max(y.dt_exclusao)
		 from	pls_prestador_medico y
		 where	y.nr_seq_prestador = c.nr_seq_prestador_exec
		 and	y.cd_medico = a.cd_medico
		 and	y.ie_situacao = 'A') dt_exclusao,
		a.dt_procedimento,
		a.dt_procedimento_imp
	from	pls_oc_cta_selecao_ocor_v	x,
		pls_proc_participante_ocor_v	a,
		pls_conta			c
	where	x.ie_valido		= 'S'
	and	x.nr_id_transacao	= nr_id_transacao_pc
	and	a.nr_seq_conta_proc	= x.nr_seq_conta_proc
	and	c.nr_sequencia		= x.nr_seq_conta
	and	ie_tipo_prestador_pc = 'E';

begin
if	(dados_regra_p.nr_sequencia is not null)  then
	for r_C01_w in C01(dados_regra_p.nr_sequencia) loop
		if	(r_C01_w.ie_valida = 'S')	then
			begin
				pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
				nr_idx_w:=	0;
				-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
				-- Considera todos como v�lidos, o que n�o trouxer no cursor vai permanecer como v�lido
				-- o que trouxer no cursor pode ser alterado para N dependendo das datas e inclus�o e exclus�o
				pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
				for r_C02_w in	C02(nr_id_transacao_p, r_C01_w.ie_tipo_prestador) loop
					ie_valido_w :=	'N';
					if	(r_C02_w.qt_registros = 0) then
						ie_valido_w :=	'S';
					else					
						if	(dados_regra_p.ie_evento = 'IMP') then
							if	(r_C02_w.dt_procedimento_imp < r_C02_w.dt_inclusao) then 
								ie_valido_w := 'S';
							elsif	(r_C02_w.dt_exclusao is not null and r_C02_w.dt_procedimento_imp > r_C02_w.dt_exclusao) then 
								ie_valido_w := 'S';
							end if;
						else
							if	(r_C02_w.dt_procedimento < r_C02_w.dt_inclusao) then 
								ie_valido_w := 'S';
							elsif	(r_C02_w.dt_exclusao is not null and r_C02_w.dt_procedimento > r_C02_w.dt_exclusao) then 
								ie_valido_w := 'S';
							end if;
						end if;
					end if;
					
					if	(ie_valido_w = 'S') then
						dados_tb_selecao_w.nr_seq_selecao(nr_idx_w):= r_C02_w.nr_seq_selecao;
						dados_tb_selecao_w.ds_observacao(nr_idx_w):= 'Profissional participante n�o est� vinculado ao prestador';
						dados_tb_selecao_w.ie_valido(nr_idx_w):= ie_valido_w;
						
						nr_idx_w := nr_idx_w + 1;
						
						if	(nr_idx_w = pls_util_cta_pck.qt_registro_transacao_w) then
							pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
													'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
							nr_idx_w := 0;
							pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
						end if;
					end if;					
				end loop;
				if	(nr_idx_w > 0)	then
					pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
											'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);
				end if;
			exception
			when others then
				pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p, sqlerrm, nr_id_transacao_p, nm_usuario_p);
			end;
			pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
		end if;
	end loop;
end if;
end pls_oc_cta_tratar_val_81;
/
