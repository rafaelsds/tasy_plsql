create or replace 
procedure HD_GERAR_ATEND_SEMANAL(dt_referencia_p        date,
                                nm_usuario_p            varchar2) is

dt_sem_ant_w                date;
dt_sem_atual_w              date;
dt_prox_semana_w            date;
nr_atend_novo_w             number(10);
dt_alta_w                   date;
nr_atend_ant_w              number(10);
cd_pessoa_fisica_w          varchar2(10);
nm_pessoa_fisica_w          varchar2(255);
cd_estabelecimento_w        number(5);
cd_motivo_alta_w            number(3);
ds_erro_w                   varchar2(2000);
ds_mensagem_w               varchar2(2000);
cd_perfil_atend_w           number(5);
ds_titulo_w                 varchar2(255);
nr_atend_semana_w           number(10);
cd_estab_w                  number(5);
cd_estab_unidade_w          number(5);
ds_unidade_w                varchar2(90);
qt_existe_atend_auto_w      number(10);
ie_tipo_atendimento_w       number(3);
cd_procedencia_w            number(5);
ie_clinica_w                number(5);
nr_seq_interno_w            number(10);
nr_seq_atual_atend_auto_w   number(10);
cd_unidade_basica_w         varchar2(10);
cd_unidade_compl_w          varchar2(10);
cd_setor_atendimento_w      number(5);
nr_sequencia_ultimo_aten_w  number(10);
nr_atendimento_ocup_setor_w number(10);
ds_erro_teste_w             varchar2(255);
qt_apac_w                   number(10);
ie_gerar_alta_fim_apac_w    varchar2(15) := 'N';
nr_seq_queixa_w             number(10);
ie_gera_atend_conservador_w varchar2(1);
qt_trat_conservador_w       number(10);
qt_trat_w                   number(10);
ie_gerar_atend_w            varchar2(1);

cursor c01 is /* Pacientes que tem prescri��es a ser geradas */
        select  cd_pessoa_fisica,
                substr(obter_nome_pf(cd_pessoa_fisica), 1, 80),
                hd_obter_estab_unidade(hd_obter_unidade_prc(cd_pessoa_fisica, 'C')),
                substr(hd_obter_desc_unid_dialise(hd_obter_unidade_prc(cd_pessoa_fisica, 'C')), 1, 90)
        from    hd_pac_renal_cronico a
        where   hd_obter_se_paciente_ativo(cd_pessoa_fisica) = 'S'
        and     (not exists (   select 1
                                from    paciente_tratamento b
                                where   a.cd_pessoa_fisica = b.cd_pessoa_fisica
                                and     b.dt_final_tratamento is null
                                and     b.ie_tratamento = 'CO') or
        (ie_gera_atend_conservador_w = 'S'));

begin

dt_sem_atual_w   := Obter_inicio_semana(dt_referencia_p);
dt_sem_ant_w     := dt_sem_atual_w - 7;
dt_prox_semana_w := dt_sem_atual_w + 7;

obter_param_usuario(7009, 207, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_gera_atend_conservador_w);

open c01;
loop
fetch c01
    into cd_pessoa_fisica_w, nm_pessoa_fisica_w, cd_estab_unidade_w, ds_unidade_w;
exit when c01%notfound;
    
        ds_mensagem_w    := null;
        ie_gerar_atend_w := 'S';
    
        if      (ie_gera_atend_conservador_w = 'S') then
        
                select  count(*)
                into    qt_trat_conservador_w
                from    paciente_tratamento b
                where   b.cd_pessoa_fisica = cd_pessoa_fisica_w
                and     b.dt_final_tratamento is null
                and     b.ie_tratamento = 'CO';
        
                select  count(*)
                into    qt_trat_w
                from    paciente_tratamento b
                where   b.cd_pessoa_fisica = cd_pessoa_fisica_w
                and     b.dt_final_tratamento is null
                and     b.ie_tratamento <> 'CO';
        
                if      (qt_trat_conservador_w > 0) and (qt_trat_w = 0) then
                        ie_gerar_atend_w := 'N';
                end if;
        
        end if;
    
        select  nvl(max(nr_atendimento), 0),
                max(dt_alta),
                max(cd_estabelecimento)
        into    nr_atend_ant_w, dt_alta_w, cd_estabelecimento_w
        from    atendimento_paciente a
        where   dt_entrada BETWEEN dt_sem_ant_w and dt_sem_atual_w
        and     cd_pessoa_fisica = cd_pessoa_fisica_w
        and     cd_estabelecimento = cd_estab_unidade_w
        and exists  (   select 1
                        from hd_log_geracao_atend x
                        where a.nr_atendimento = x.nr_atendimento_nov)
        and hd_obter_se_regra_atend(a.nr_atendimento) = 'S';
    
        if      (nr_atend_ant_w = 0) then
        
                select  nvl(max(nr_atendimento), 0),
                        max(dt_alta),
                        max(cd_estabelecimento)
                into    nr_atend_ant_w, dt_alta_w, cd_estabelecimento_w
                from    atendimento_paciente a
                where   dt_entrada BETWEEN dt_sem_ant_w and dt_sem_atual_w
                and     cd_pessoa_fisica = cd_pessoa_fisica_w
                and     cd_estabelecimento = cd_estab_unidade_w
                and     hd_obter_se_regra_atend(a.nr_atendimento) = 'S';
        end if;
    
        select  nvl(max(nr_atendimento), 0)
        into    nr_atend_semana_w
        from    atendimento_paciente a
        where   dt_entrada BETWEEN dt_sem_atual_w and dt_prox_semana_w
        and     cd_pessoa_fisica = cd_pessoa_fisica_w
        and     exists (select 1
                        from hd_log_geracao_atend x
                        where a.nr_atendimento = x.nr_atendimento_nov)
        and     hd_obter_se_regra_atend(a.nr_atendimento) = 'S';
    
        if      (nr_atend_semana_w = 0) and (ie_gerar_atend_w = 'S') then
                select nvl(max(nr_atendimento), 0)
                into    nr_atend_semana_w
                from    atendimento_paciente a
                where   dt_entrada BETWEEN dt_sem_atual_w and dt_prox_semana_w
                and     cd_pessoa_fisica = cd_pessoa_fisica_w
                and     hd_obter_se_regra_atend(a.nr_atendimento) = 'S';
        end if;
    
        select  max(cd_perfil_atend), nvl(max(ie_gerar_alta_fim_apac), 'N')
        into    cd_perfil_atend_w, ie_gerar_alta_fim_apac_w
        from    hd_parametro
        where   cd_estabelecimento = nvl(cd_estabelecimento_w, cd_estabelecimento);
    
        if      (ie_gerar_alta_fim_apac_w = 'S') then
                select  count(*)
                into    qt_apac_w
                from    sus_apac_unif a, atendimento_paciente b
                where   a.nr_atendimento = b.nr_atendimento
                and     b.cd_pessoa_fisica = cd_pessoa_fisica_w;
            
                if      (qt_apac_w > 0) then
                        begin
                        select  nvl(max(a.nr_atendimento), 0),
                                max(b.dt_alta),
                                max(b.cd_estabelecimento)
                        into    nr_atend_ant_w,
                                dt_alta_w,
                                cd_estabelecimento_w
                        from    sus_apac_unif a, atendimento_paciente b
                        where   a.nr_atendimento = b.nr_atendimento
                        and     b.cd_pessoa_fisica = cd_pessoa_fisica_w
                        and     a.dt_fim_validade BETWEEN dt_sem_atual_w and dt_prox_semana_w;
                        exception
                            when others then
                                nr_atend_ant_w := nr_atend_ant_w;
                        end;
                end if;
        end if;
    
        if      (nr_atend_ant_w > 0) and (nr_atend_semana_w = 0) then
        
                select nvl(cd_motivo_alta_automatica, 0)
                into    cd_motivo_alta_w
                from    hd_parametro
                where   cd_estabelecimento = cd_estabelecimento_w;
        
                if  (dt_alta_w is null) and (cd_motivo_alta_w > 0) then
                        gerar_estornar_alta(nr_atend_ant_w,
                                        'A',
                                        null,
                                        cd_motivo_alta_w,
                                        sysdate,
                                        nm_usuario_p,
                                        ds_erro_w,
                                        null,
                                        null,
                                        wheb_mensagem_pck.get_texto(309053,
                                                                'DT_REFERENCIA_P=' ||
                                                                to_char(dt_referencia_p))); --Gerado autom�ticamente no dia: #@DT_REFERENCIA_P#@
            
                        if      (ds_erro_w <> '') then
                                ds_mensagem_w := wheb_mensagem_pck.get_texto(309055,
                                                                        'NR_ATEND_ANT_W=' ||
                                                                        nr_atend_ant_w || ';' ||
                                                                        'DS_ERRO_W=' ||
                                                                        ds_erro_w || ';' ||
                                                                        'DS_UNIDADE_W=' ||
                                                                        ds_unidade_w);
                                /* N�o possivel gerar alta para o atendimento #@NR_ATEND_ANT_W#@ Motivo #@DS_ERRO_W#@ Unidade: #@DS_UNIDADE_W#@  */
                        end if;            
                end if;
        
                duplicar_atendimento(nr_atend_ant_w,
                        nm_usuario_p,
                        nr_atend_novo_w);

                insert into hd_log_geracao_atend
                values (hd_log_geracao_atend_seq.nextval,
                        sysdate,
                        nm_usuario_p,
                        sysdate,
                        nm_usuario_p,
                        nr_atend_ant_w,
                        nr_atend_novo_w,
                        cd_estab_unidade_w,
                        dt_referencia_p);

                nr_atend_semana_w := nr_atend_novo_w;
                
                begin

                        select  count(*)
                        into    qt_existe_atend_auto_w
                        from    hd_regra_atual_atend_auto
                        where   cd_estabelecimento = cd_estab_unidade_w;

                        if      (qt_existe_atend_auto_w > 0) then

                                select  max(nr_sequencia)
                                into    nr_seq_atual_atend_auto_w
                                from    hd_regra_atual_atend_auto
                                where   cd_estabelecimento = cd_estab_unidade_w;

                                select  max(ie_tipo_atendimento),
                                        max(cd_procedencia),
                                        max(ie_clinica),
                                        max(nr_seq_interno),
                                        max(nr_seq_queixa)
                                into    ie_tipo_atendimento_w,
                                        cd_procedencia_w,
                                        ie_clinica_w,
                                        nr_seq_interno_w,
                                        nr_seq_queixa_w
                                from    hd_regra_atual_atend_auto
                                where   nr_sequencia = nr_seq_atual_atend_auto_w;

                                update  atendimento_paciente
                                set     ie_tipo_atendimento = ie_tipo_atendimento_w,
                                        cd_procedencia      = cd_procedencia_w,
                                        ie_clinica          = ie_clinica_w,
                                        nr_seq_queixa       = nr_seq_queixa_w
                                where   nr_atendimento = nr_atend_novo_w;

                                select  cd_unidade_basica,
                                        cd_unidade_compl,
                                        cd_setor_atendimento
                                into    cd_unidade_basica_w,
                                        cd_unidade_compl_w,
                                        cd_setor_atendimento_w
                                from    unidade_atendimento
                                where   nr_seq_interno = nr_seq_interno_w;

                                select  max(nr_seq_interno)
                                into    nr_sequencia_ultimo_aten_w
                                from    atend_paciente_unidade
                                where   nr_atendimento = nr_atend_novo_w;

                                if      (nr_sequencia_ultimo_aten_w > 0) then

                                        update  atend_paciente_unidade
                                        set     cd_unidade_basica    = cd_unidade_basica_w,
                                                cd_unidade_compl     = cd_unidade_compl_w,
                                                cd_setor_atendimento = cd_setor_atendimento_w,
                                                dt_entrada_unidade   = sysdate,
                                                dt_saida_unidade     = decode(dt_saida_unidade,
                                                null,
                                                null,
                                                sysdate)
                                        where   nr_seq_interno = nr_sequencia_ultimo_aten_w
                                        and     nr_atendimento = nr_atend_novo_w;

                                end if;

                        end if;

                exception
                when others then
                        ds_erro_teste_w := substr(sqlerrm(sqlcode), 1, 255);

                        select  max(nr_atendimento)
                        into    nr_atendimento_ocup_setor_w
                        from    atend_paciente_unidade
                        where   cd_unidade_basica = cd_unidade_basica_w
                        and     cd_unidade_compl = cd_unidade_compl_w
                        and     cd_setor_atendimento = cd_setor_atendimento_w;
                end;
                
                ds_titulo_w   := wheb_mensagem_pck.get_texto(309061); -- Atendimento de Di�lise gerado
                ds_mensagem_w := ds_mensagem_w || wheb_mensagem_pck.get_texto(309065,
                                                                'NR_ATEND_NOVO_W=' ||
                                                                nr_atend_novo_w || ';' ||
                                                                'DS_UNIDADE_W=' ||
                                                                ds_unidade_w);
                /* Atendimento Gerado #@NR_ATEND_NOVO_W#@ Unidade: #@DS_UNIDADE_W#@  */
        else
        
                if      (nr_atend_ant_w = 0) then
                        ds_titulo_w   := wheb_mensagem_pck.get_texto(309062); -- Atendimento de Di�lise n�o gerado
                        ds_mensagem_w := wheb_mensagem_pck.get_texto(309079,
                                                                     'dt_sem_ant_w=' ||
                                                                     to_char(dt_sem_ant_w,
                                                                             'dd/mm/yyyy') || ';' ||
                                                                     'DS_UNIDADE_W=' ||
                                                                     ds_unidade_w);
                        /* Atendimento no m�s #@dt_sem_ant_w#@ n�o encontrado.Unidade: #@DS_UNIDADE_W#@*/
                end if;
        
        end if;
    
        if      (nvl(nr_atend_semana_w, 0) > 0) then
        
                select cd_estabelecimento
                into    cd_estab_w
                from    atendimento_paciente
                where   nr_atendimento = nr_atend_semana_w;

                update prescr_medica
                set     nr_atendimento = nr_atend_semana_w,
                        dt_prescricao  = sysdate,
                        dt_liberacao   = decode(dt_liberacao, null, null, sysdate)
                where   nr_atendimento is null
                and     cd_pessoa_fisica = cd_pessoa_fisica_w
                and     cd_estabelecimento = cd_estab_w
                and     pkg_date_utils.start_of(dt_prescricao, 'MONTH', 0) =    pkg_date_utils.start_of(dt_sem_ant_w, 'MONTH', 0);
        
        end if;
    
        if      (ds_mensagem_w is not null) and (nvl(cd_perfil_atend_w, 0) > 0) then
        
                insert into comunic_interna
                        (dt_comunicado,
                        ds_titulo,
                        ds_comunicado,
                        nm_usuario,
                        dt_atualizacao,
                        ie_geral,
                        nm_usuario_destino,
                        ds_perfil_adicional,
                        nr_sequencia,
                        ie_gerencial,
                        dt_liberacao,
                        cd_estab_destino)
                values
                        (sysdate,
                        ds_titulo_w || cd_pessoa_fisica_w || ' - ' ||
                        nm_pessoa_fisica_w,
                        ds_mensagem_w,
                        nm_usuario_p,
                        sysdate,
                        'N',
                        '',
                        cd_perfil_atend_w || ', ',
                        comunic_interna_seq.nextval,
                        'N',
                        sysdate,
                        cd_estabelecimento_w);
        
        end if;
        
end loop;
close c01;

commit;

end HD_GERAR_ATEND_SEMANAL;
/