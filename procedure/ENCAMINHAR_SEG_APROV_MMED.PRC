create or replace
procedure encaminhar_seg_aprov_mmed(
			nm_usuario_p		varchar2,
			nr_seq_laudo_p		number,
			ds_mensagem_p		varchar2,
			cd_medico_seg_aprov_p	varchar2) is 

			
ie_laudo_liberado_w				varchar2(2);
nr_prescricao_w					number(10);
nr_sequencia_prescricao_w				number(10);
nr_atendimento_w    				number(10);
cd_procedimento_w   				number(15);
nr_seq_laudo_compl_w        laudo_paciente_compl.nr_sequencia%type;
ie_exist_med_rep_cat_w      pls_integer;
nr_atend_laudo_w            laudo_paciente.nr_atendimento%type;
cd_pessoa_fisica_w          laudo_paciente.cd_pessoa_fisica%type;
           

Cursor C01 is
select distinct	nr_prescricao,
		nr_sequencia_prescricao
from		procedimento_paciente
where		nr_laudo = nr_seq_laudo_p;
	
begin

select 
  max(a.nr_atendimento),
  max(a.cd_procedimento)
into
  nr_atendimento_w,
  cd_procedimento_w
from	procedimento_paciente a
where	nr_sequencia = (select max(b.nr_seq_proc)
			from 	laudo_paciente b
			where	b.nr_sequencia = nr_seq_laudo_p);


update	laudo_paciente
set	ie_exige_seg_aprov = 'S',
	dt_liberacao = null,
	cd_resp_seg_aprov = cd_medico_seg_aprov_p,
	nm_usuario_liberacao = null
where	nr_sequencia = nr_seq_laudo_p;

select max(nr_sequencia)
into nr_seq_laudo_compl_w
from laudo_paciente_compl
where nr_seq_laudo = nr_seq_laudo_p;

if (nr_seq_laudo_compl_w > 0) then
  update laudo_paciente_compl
  set dt_seg_aprov_solicitado = sysdate
  where nr_sequencia = nr_seq_laudo_compl_w;  
else 
  select laudo_paciente_compl_seq.nextval
  into nr_seq_laudo_compl_w
  from dual;

  insert into laudo_paciente_compl (nr_sequencia,
                                    nr_seq_laudo,
                                    nm_usuario,
                                    dt_atualizacao,                                    
                                    dt_seg_aprov_solicitado)
                                    values (nr_seq_laudo_compl_w,
                                          nr_seq_laudo_p,
                                          nm_usuario_p,
                                          sysdate,                                          
                                          sysdate);  
end if;  


open C01;
loop
fetch C01 into	
	nr_prescricao_w,
	nr_sequencia_prescricao_w;
exit when C01%notfound;
	begin
	gravar_auditoria_mmed(nr_prescricao_w,nr_sequencia_prescricao_w,nm_usuario_p,7,null);	
	end;
end loop;
close C01;

insert	into comunic_interna(
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nm_usuario_destino,
		ie_gerencial,
		nr_sequencia,
		dt_liberacao)
	values(
		sysdate,
		obter_texto_dic_objeto (291741, null, null) || nr_seq_laudo_p,

		obter_texto_dic_objeto (291746, null, null) || nr_atendimento_w || chr(10) ||
    		obter_texto_dic_objeto (291749, null, null) || substr(obter_desc_procedimento(cd_procedimento_w, null),0,100) || chr(10) || 
   		chr(10) || 
    		ds_mensagem_p,


		nm_usuario_p,
		sysdate,
		'N',
		obter_usuario_pf(cd_medico_seg_aprov_p),
		'N',
		comunic_interna_seq.nextval,
		sysdate);

commit;

select count(b.cd_categoria)
into ie_exist_med_rep_cat_w
from wl_regra_worklist a,
    wl_item b
where a.nr_seq_item = b.nr_sequencia
and b.cd_categoria = 'MR';

if(ie_exist_med_rep_cat_w > 0) then 
  select max(nr_atendimento),
         max(cd_pessoa_fisica)
  into nr_atend_laudo_w,
       cd_pessoa_fisica_w
  from laudo_paciente
  where nr_sequencia = nr_seq_laudo_p;

  wl_gerar_finalizar_tarefa('MR','I', nr_atend_laudo_w, cd_pessoa_fisica_w, nm_usuario_p, (sysdate + 1), 'N', null, null, null, null, null, null, null, null, null, null,
                             null, null, null, null, null, null, null, sysdate, null, null, cd_medico_seg_aprov_p, null, null, null, null, nr_seq_laudo_p);
end if;

end encaminhar_seg_aprov_mmed;
/