create or replace
procedure pep_just_inativ_sv_integracao(nr_sequencia_p number,
          ds_justificativa_p        varchar2 default null,
          nm_tabela_p varchar2 default null) is
          
ds_comando_w varchar2(4000);
nm_usuario_w varchar2(15) := obter_usuario_ativo;	

begin

if ((upper(nm_tabela_p) = 'ATENDIMENTO_SINAL_VITAL') or
 (upper(nm_tabela_p) = 'ATENDIMENTO_MONIT_RESP') or
 (upper(nm_tabela_p) = 'ATEND_MONIT_HEMOD')) then
 
 ds_comando_w  := ' update ' || nm_tabela_p || 
    ' set dt_liberacao = sysdate, ' ||
    ' nm_usuario_inativacao  = '''||nm_usuario_w||''','||
    ' dt_inativacao = sysdate,' ||
    ' ds_justificativa = substr('''||ds_justificativa_p||''',1,255),' ||
    ' ie_situacao = ''I'''||
    ' where  nr_sequencia = ' ||  nr_sequencia_p;
	
    exec_sql_dinamico('pep_just_inativ_sv_integracao', ds_comando_w);     

    end if;
 
end pep_just_inativ_sv_integracao;
/
