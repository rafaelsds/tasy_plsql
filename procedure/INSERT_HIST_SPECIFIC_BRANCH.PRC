create or replace procedure insert_hist_specific_branch(
  nr_service_order_p in man_ordem_servico.nr_sequencia%type,
  ds_branch_p in varchar2,
  nm_usuario_p in usuario.nm_usuario%type) is
  
HIST_TYPE_INTERN_DIALOG_CONST man_ordem_serv_tecnico.nr_seq_tipo%type := 7;
ds_text_w man_ordem_serv_tecnico.ds_relat_tecnico%type;
  
begin
	ds_text_w := 'The specific branch: "' || ds_branch_p || '" was created.';

	insert into corp.MAN_ORDEM_SERV_TECNICO@whebl01_dbcorp(
		NR_SEQUENCIA,
		NR_SEQ_ORDEM_SERV,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_HISTORICO,
		DT_LIBERACAO,
		NR_SEQ_TIPO,
		IE_ORIGEM,
		DS_RELAT_TECNICO) 
	values (
		corp.MAN_ORDEM_SERV_TECNICO_SEQ.nextval@whebl01_dbcorp,
		nr_service_order_p,
		sysdate, 
		NM_USUARIO_P, 
		sysdate,
		sysdate,
		HIST_TYPE_INTERN_DIALOG_CONST,--'Dialogo interno Philips'
		'I',
		ds_text_w); 
    commit;		
end;
/