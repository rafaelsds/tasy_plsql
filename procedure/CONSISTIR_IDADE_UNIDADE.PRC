create or replace
procedure consistir_idade_unidade(	cd_pessoa_fisica_p	in varchar2,
					nr_seq_unidade_p	in number,
					ds_retorno_p		out varchar2 )
					is

qt_idade_minima_w	number(5,2);
qt_idade_maxima_w	number(5,2);
qt_idade_w		number(5,2);
qt_idade_dias_min_w	number(3);
qt_idade_dias_max_w	number(3);
qt_idade_anos_w		number(5,2);
qt_idade_dias_w		number(10);
begin

if	((cd_pessoa_fisica_p is not null) and (nr_seq_unidade_p is not null)) then

	select	obter_idade_pf(cd_pessoa_fisica_p,sysdate,'A')
	into	qt_idade_anos_w
	from	dual;
	
	qt_idade_w := qt_idade_anos_w;
	


	select	max(qt_idade_minima),
		max(qt_idade_maxima),
		max(qt_idade_dias_min),
		max(qt_idade_dias_max)
	into	qt_idade_minima_w,
		qt_idade_maxima_w,
		qt_idade_dias_min_w,
		qt_idade_dias_max_w
	from	unidade_atendimento
	where	nr_seq_interno	=	nr_seq_unidade_p;	
	
	
	if	(qt_idade_dias_min_w is null) and
		(qt_idade_dias_max_w is null) then

		if	(qt_idade_minima_w is not null) and
			(qt_idade_minima_w > qt_idade_w) then
			ds_retorno_p := wheb_mensagem_pck.get_texto(306573, 'QT_IDADE_MINIMA=' || to_char(qt_idade_minima_w)); 
							-- A idade m�nima permitida para o leito � #@QT_IDADE_MINIMA#@ ano(s).
		end if;

		if	(qt_idade_maxima_w is not null) and
			(qt_idade_maxima_w < qt_idade_w) then
			ds_retorno_p := wheb_mensagem_pck.get_texto(306574, 'QT_IDADE_MAXIMA=' || to_char(qt_idade_maxima_w)); 
							-- A idade m�xima permitida para o leito � #@QT_IDADE_MAXIMA#@ ano(s).
		end if;
	
	else		
			
			select trunc(sysdate-to_date(obter_dados_pf(cd_pessoa_fisica_p,'DN')))
			into	qt_idade_dias_w
			from	dual;
		
			if	(qt_idade_dias_min_w is not null) and
				(qt_idade_w = 0) and 
				(qt_idade_dias_min_w > qt_idade_dias_w) then
				ds_retorno_p := wheb_mensagem_pck.get_texto(779753, 'QT_IDADE_MINIMA=' || to_char(qt_idade_dias_min_w)); 
								-- A idade m�nima permitida para o leito � #@QT_IDADE_MINIMA#@ dias(s).
			end if;
			
	
			if	(qt_idade_dias_max_w is not null) and
				(qt_idade_w = 0) and 
				(qt_idade_dias_max_w < qt_idade_dias_w) then
				ds_retorno_p := wheb_mensagem_pck.get_texto(306576, 'QT_IDADE_DIAS_MAX=' || to_char(qt_idade_dias_max_w)); 
								-- A idade m�xima permitida para o leito � #@QT_IDADE_DIAS_MAX#@ dia(s).
			elsif 	(qt_idade_dias_max_w is not null) and
				(qt_idade_w > 0) then 
				ds_retorno_p := wheb_mensagem_pck.get_texto(306576, 'QT_IDADE_DIAS_MAX=' || to_char(qt_idade_dias_max_w)); 
								-- A idade m�xima permitida para o leito � #@QT_IDADE_DIAS_MAX#@ dia(s).
			end if;

			if	(qt_idade_minima_w is not null) and
				(qt_idade_w > 0) and 
				(qt_idade_minima_w > qt_idade_w) then
				ds_retorno_p := wheb_mensagem_pck.get_texto(306573, 'QT_IDADE_MINIMA=' || to_char(qt_idade_minima_w)); 
								-- A idade m�nima permitida para o leito � #@QT_IDADE_MINIMA#@ ano(s).
			end if;

			if	(qt_idade_maxima_w is not null) and
				(qt_idade_w > 0) and 
				(qt_idade_maxima_w < qt_idade_w) then
				ds_retorno_p := wheb_mensagem_pck.get_texto(306574, 'QT_IDADE_MAXIMA=' || to_char(qt_idade_maxima_w)); 
								-- A idade m�xima permitida para o leito � #@QT_IDADE_MAXIMA#@ ano(s).
			end if;			
				
	end if;
end if;

end consistir_idade_unidade;
/