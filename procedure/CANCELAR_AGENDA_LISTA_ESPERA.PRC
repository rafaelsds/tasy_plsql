create or replace
procedure Cancelar_agenda_lista_espera(nr_seq_lista_espera_p	number,
				       nm_usuario_p		varchar2,
				       ie_status_espera_p 	varchar2 	default 'C') is 
ie_status_espera_w	varchar2(1) := 'C';
					   
begin
ie_status_espera_w	:= nvl(ie_status_espera_p,'C');

update	agenda_lista_espera
set	ie_status_espera	= ie_status_espera_w,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_lista_espera_p;


commit;

end Cancelar_agenda_lista_espera;
/