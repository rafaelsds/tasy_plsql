create or replace
procedure pls_proposta_gerar_log
			(	nr_seq_proposta_p		Number,
				nr_seq_contrato_p		Number,
				nr_seq_plano_p			Number,
				ie_tipo_p			Varchar2,
				nr_seq_beneficiario_p		Number,
				nr_seq_titular_p		Number,
				nr_seq_grau_parentesco_p	Number,
				nm_usuario_p			Varchar2) is 
				
ds_grau_parentesco_w		Varchar2(40);
ds_tipo_parentesco_w		Varchar2(120);

begin

if	(nvl(nr_seq_grau_parentesco_p,0) > 0) then
	begin
	select	ds_parentesco,
		substr(obter_valor_dominio(1885, ie_tipo_parentesco),1,120)
	into	ds_grau_parentesco_w,
		ds_tipo_parentesco_w
	from	grau_parentesco
	where	nr_sequencia	= nr_seq_grau_parentesco_p;
	exception
		when others then
		ds_grau_parentesco_w	:= '';
		ds_tipo_parentesco_w	:= '';
	end;
end if;

insert into pls_proposta_contrato_log
	(nr_sequencia, dt_atualizacao, nm_usuario,
	nr_seq_proposta, nr_seq_contrato, nr_seq_plano,
	ie_tipo, nr_seq_beneficiario, nr_seq_titular,
	ds_parentesco, ds_tipo_parentesco)
values(	pls_proposta_contrato_log_seq.nextval, sysdate, nm_usuario_p,
	nr_seq_proposta_p, nr_seq_contrato_p, nr_seq_plano_p,
	ie_tipo_p, nr_seq_beneficiario_p, nr_seq_titular_p,
	ds_grau_parentesco_w, ds_tipo_parentesco_w);

end pls_proposta_gerar_log;
/
