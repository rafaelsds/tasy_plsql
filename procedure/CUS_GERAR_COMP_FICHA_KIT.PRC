create or replace
procedure cus_gerar_comp_ficha_kit(	cd_estabelecimento_p	number,
				nr_ficha_tecnica_p		number,
				nm_usuario_p		Varchar2) is 

cd_material_w			number(10);
cd_kit_material_w			number(10);
cd_material_comp_w		number(10);
nr_seq_componente_w		number(10);
qt_componente_w			number(15,4);

cursor C01 is
select	cd_material,
	qt_material
from	componente_kit
where	cd_kit_material	= cd_kit_material_w
and	ie_situacao	= 'A'
and	((cd_estab_regra is null) or (cd_estab_regra = cd_estabelecimento_p));

begin

select	cd_material
into	cd_material_w
from	ficha_tecnica
where	nr_ficha_tecnica	= nr_ficha_tecnica_p;

select	nvl(max(cd_kit_material),0)
into	cd_kit_material_w
from	material_estab
where	cd_estabelecimento	= cd_estabelecimento_p
and	cd_material	= cd_material_w;

delete	ficha_tecnica_componente
where	nr_ficha_tecnica	 = nr_ficha_tecnica_p
and	ie_tipo_componente = 1;

select	nvl(max(nr_seq_componente),0)
into	nr_seq_componente_w
from	ficha_tecnica_componente
where	nr_ficha_tecnica	= nr_ficha_tecnica_p;

open C01;
loop
fetch C01 into	
	cd_material_comp_w,
	qt_componente_w;
exit when C01%notfound;
	begin
	
	nr_seq_componente_w	:= nr_seq_componente_w + 1;
	
	insert into ficha_tecnica_componente(
		nr_ficha_tecnica,
		nr_seq_componente,
		dt_alteracao,
		cd_estabelecimento,
		nm_usuario,
		dt_atualizacao,
		ie_situacao,
		ie_tipo_componente,
		cd_material,
		qt_variavel,
		qt_fixa,
		pr_quebra_variavel,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_ficha_tecnica_p,
		nr_seq_componente_w,
		sysdate,
		cd_estabelecimento_p,
		nm_usuario_p,
		sysdate,
		'A',
		1,
		cd_material_comp_w,
		qt_componente_w,
		0,
		0,
		sysdate,
		nm_usuario_p);
	
	end;
end loop;
close C01;


commit;

end cus_gerar_comp_ficha_kit;
/