create or replace
procedure gravar_indicacao_transferencia(
			nr_atendimento_p	number,
			nr_seq_indicacao_p	number,
			cd_medico_p		varchar2,
			cd_pessoa_fisica_p	varchar2,
			cd_cgc_indicacao_p	varchar2,
			ie_opcao_p		varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2,
			NR_SEQ_TIPO_MIDIA_P	number default null,
			DS_OBSERVACAO_P		varchar2 default null) is
			
ie_existe_atend_w	varchar2(1);
begin
if	(nvl(nr_atendimento_p,0) > 0) and 
	(nvl(nr_seq_indicacao_p,0) > 0) then

	if	(ie_opcao_p = 'P') and 
		(cd_pessoa_fisica_p is not null) then
		
		insert into atendimento_indicacao( nr_sequencia,
						   cd_estabelecimento,
						   dt_atualizacao,
						   nm_usuario,
						   dt_atualizacao_nrec,
						   nm_usuario_nrec,
						   nr_seq_indicacao,
						   cd_pessoa_fisica,
						   nr_atendimento,
						   DS_OBSERVACAO)
				values 		(atendimento_indicacao_seq.nextval,
						cd_estabelecimento_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_indicacao_p,
						cd_pessoa_fisica_p,
						nr_atendimento_p,
						DS_OBSERVACAO_P);
		
	elsif	(ie_opcao_p = 'J') and 
		(cd_cgc_indicacao_p is not null) then
		
		insert into atendimento_indicacao( nr_sequencia,
						   cd_estabelecimento,
						   dt_atualizacao,
						   nm_usuario,
						   dt_atualizacao_nrec,
						   nm_usuario_nrec,
						   nr_seq_indicacao,
						   cd_cgc_indicacao,
						   nr_atendimento,
						   DS_OBSERVACAO)
				values 		(atendimento_indicacao_seq.nextval,
						cd_estabelecimento_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_indicacao_p,
						cd_cgc_indicacao_p,
						nr_atendimento_p,
						DS_OBSERVACAO_P);
		
	elsif	((ie_opcao_p = 'CC') or
		(ie_opcao_p = 'N')) and
		(cd_medico_p is not null) then
		insert into atendimento_indicacao( nr_sequencia,
						   cd_estabelecimento,
						   dt_atualizacao,
						   nm_usuario,
						   dt_atualizacao_nrec,
						   nm_usuario_nrec,
						   nr_seq_indicacao,
						   cd_medico,
						   nr_atendimento,
						   DS_OBSERVACAO)
				values 		(atendimento_indicacao_seq.nextval,
						cd_estabelecimento_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_indicacao_p,
						cd_medico_p,
						nr_atendimento_p,
						DS_OBSERVACAO_P);
						
	elsif	(ie_opcao_p = 'MI') and
		(nr_seq_tipo_midia_p is not null) then
		insert into atendimento_indicacao( nr_sequencia,
						   cd_estabelecimento,
						   dt_atualizacao,
						   nm_usuario,
						   dt_atualizacao_nrec,
						   nm_usuario_nrec,
						   nr_seq_indicacao,
						   nr_seq_tipo_midia,
						   nr_atendimento,
						   DS_OBSERVACAO)
				values 		(atendimento_indicacao_seq.nextval,
						cd_estabelecimento_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_indicacao_p,
						nr_seq_tipo_midia_p,
						nr_atendimento_p,
						DS_OBSERVACAO_P);
	
	end if;
	
end if;
	
commit;

end gravar_indicacao_transferencia;
/