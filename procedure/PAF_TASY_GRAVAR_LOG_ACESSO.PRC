create or replace
procedure paf_tasy_gravar_log_acesso(	
				nm_usuario_p			varchar2,
				ds_maquina_p			varchar2,
				nm_usuario_so_p			varchar2,
				ie_acao_p			varchar2,
				nm_maq_cliente_p		varchar2,
				cd_aplicacao_tasy_p		varchar2,
				nr_sequencia_p		out	number,
				ie_acao_excesso_p	out	varchar2) is 

nm_usuario_so_w		varchar2(30);
nm_usuario_bco_w	varchar2(10)	:= 'TASY';
qt_usuario_con_w	number(05,0);
qt_usuario_lib_w	number(05,0);
ie_acao_excesso_w	varchar2(01);
nr_sequencia_w		number(10,0);
nr_seq_comunic_w	number(10,0);
nm_usuario_w		varchar2(15);
qt_acesso_invalido_w	number(10);

begin

/* a��es do log 
	N - acesso nornal
	T - tentativa de acesso
*/

/*verifica qual o valor para tentativas de acesso inv�lido*/
select 	to_number(paf_obter_valor_param_usuario(0,84,null,nm_usuario_p,1)) 
into	qt_acesso_invalido_w
from 	dual;

if	(ie_acao_p = 'N') then
	begin
	
	select 	nvl(max(qt_usuario),0),
		nvl(max(nm_usuario_banco),'tasy'),
		nvl(max(ie_acao_excesso),'N'),
		nvl(max(nm_usuario_aviso),'tasy')
	into	qt_usuario_lib_w,
		nm_usuario_bco_w,
		ie_acao_excesso_w,
		nm_usuario_w
	from	tasy_licenca c,
		estabelecimento b,
		usuario a
	where	a.nm_usuario		= nm_usuario_p
	  and	a.cd_estabelecimento	= b.cd_estabelecimento
	  and	b.nr_seq_licenca		= c.nr_sequencia;

	select	nvl(max(osuser), nm_usuario_so_p)
	into	nm_usuario_so_w
	from	paf_usuario_conectado_v 
	where	nm_usuario = nm_usuario_p; 
	
	select 	count(*)
	into	qt_usuario_con_w
	from	paf_usuario_conectado_v
	where	username = nm_usuario_bco_w;
	
	select 	tasy_log_acesso_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	nr_sequencia_p	:= nr_sequencia_w;
	
	insert into tasy_log_acesso (
		nr_sequencia, 
		nm_usuario, 
		dt_acesso, 
		dt_saida, 
		ds_maquina, 
		nm_usuario_so, 
		cd_aplicacao_tasy, 
		qt_usuario_lib,
		qt_usuario_con, 
		ie_result_acesso, 
		nm_maq_cliente)
	values (
		nr_sequencia_w, 
		nm_usuario_p, 
		sysdate,
		null, 
		ds_maquina_p, 
		nm_usuario_so_w,
		cd_aplicacao_tasy_p, 
		qt_usuario_lib_w, 
		qt_usuario_con_w,
		'N', 
		nm_maq_cliente_p);
		
	if	(qt_usuario_con_w > qt_usuario_lib_w) then
		begin
		
		ie_acao_excesso_p	:= ie_acao_excesso_w;
		
		if	(ie_acao_excesso_w = 'A') then
			select	comunic_interna_seq.nextval
			into 	nr_seq_comunic_w
			from 	dual;	
			
			insert into comunic_interna(
				dt_comunicado, 
				ds_titulo, 
				ds_comunicado, 
				nm_usuario,	
				dt_atualizacao, 
				ie_geral, 
				nm_usuario_destino, 
				nr_sequencia,
				ie_gerencial, 
				dt_liberacao)
			values(
				sysdate, 
				WHEB_MENSAGEM_PCK.get_texto(819242), /*'Aviso de supera��o do numero de licen�as'*/
				WHEB_MENSAGEM_PCK.get_texto(819243, 'QT_USUARIO_LIB_W=' || qt_usuario_lib_w), /* 'O numero de licen�as liberadas (#@QT_USUARIO_LIB_W#@) foi superada. Atualize a informa��o na fun��o Usu�rio.'  */ 
				'Tasy_PAF', 
				sysdate, 
				'N',
				nm_usuario_w || ',', 
				nr_seq_comunic_w, 
				'N', 
				sysdate);			
		end if;
		end;		
	end if;
	
	update	usuario
	set	qt_acesso_invalido = 0
	where	nm_usuario = nm_usuario_p;
	
	end;	
elsif	(ie_acao_p = 'T') then
	begin
	
	select 	tasy_log_acesso_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	nr_sequencia_p	:= nr_sequencia_w;
	
	insert into tasy_log_acesso (
		nr_sequencia, 
		nm_usuario, 
		dt_acesso, 
		dt_saida, 
		ds_maquina, 
		nm_usuario_so, 
		cd_aplicacao_tasy, 
		qt_usuario_lib,
		qt_usuario_con, 
		ie_result_acesso, 
		nm_maq_cliente)
	values (
		nr_sequencia_w, 
		nm_usuario_p, 
		sysdate,
		null, 
		ds_maquina_p, 
		nm_usuario_so_p,
		cd_aplicacao_tasy_p, 
		0, 
		0, 
		'T', 
		nm_maq_cliente_p);
	end;

	update	usuario
	set	qt_acesso_invalido	= (nvl(qt_acesso_invalido,0) + 1)
	where	upper(nm_usuario)	= upper(nm_usuario_p);

	update 	usuario
	set	ie_situacao		= 'B'
	where	upper(nm_usuario) 	= upper(nm_usuario_p)
	and	qt_acesso_invalido_w 	< qt_acesso_invalido
	and	qt_acesso_invalido_w 	<> 0;

end if;
commit;

end paf_tasy_gravar_log_acesso;
/