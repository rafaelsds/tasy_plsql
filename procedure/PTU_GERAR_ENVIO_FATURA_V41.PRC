create or replace
procedure ptu_gerar_envio_fatura_v41(	nr_seq_fatura_p			number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is

-- 501
vl_total_fatura_w		varchar2(14);
vl_ir_w				varchar2(14);
nr_fatura_w			varchar2(11);
dt_geracao_w			varchar2(8);
dt_venc_fatura_w		varchar2(8);
dt_emissao_fatura_w		varchar2(8);
cd_unimed_destino_w		varchar2(4);
cd_unimed_origem_w		varchar2(4);
nr_competencia_w		varchar2(4);
nr_versao_transacao_w		number(2);
ie_classif_cobranca_w		varchar2(1);
nr_nota_credito_debito_w	varchar2(30);
dt_vencimento_ndc_w		varchar2(8);
dt_emissao_ndc_w		varchar2(8);
vl_total_ndc_w			varchar2(14);

-- 502
nm_beneficiario_w		varchar2(25);
dt_atendimento_w		varchar2(21);
cd_usuario_plano_w		varchar2(13);
nr_nota_w			ptu_nota_cobranca.nr_nota%type;
nr_guia_principal_int_w		varchar2(20);
nr_guia_principal_w		varchar2(11);
nr_lote_w			varchar2(8);
cd_cid_w			varchar2(6);
cd_unimed_w			varchar2(4);
ie_tipo_atendimento_w		varchar2(2);
cd_excecao_w			varchar2(1);
ie_carater_atendimento_w	varchar2(1);
ie_paciente_w			varchar2(1);
ie_tipo_saida_spdat_w		varchar2(1);
nr_nota_geral_w			number(20);

-- 503
nm_hospital_w			varchar2(25);
dt_internacao_w			varchar2(21);
dt_alta_w			varchar2(21);
nr_declara_obito_w		varchar2(17);
nr_declara_obito_0_w		varchar2(17);
nr_declara_obito_1_w		varchar2(17);
nr_declara_obito_2_w		varchar2(17);
nr_declara_obito_3_w		varchar2(17);
nr_declara_obito_4_w		varchar2(17);
nr_declara_obito_5_w		varchar2(17);
nr_declara_vivo_0_w		varchar2(15);
nr_declara_vivo_1_w		varchar2(15);
nr_declara_vivo_2_w		varchar2(15);
nr_declara_vivo_3_w		varchar2(15);
nr_declara_vivo_4_w		varchar2(15);
nr_declara_vivo_5_w		varchar2(15);
cd_cgc_hospital_w		varchar2(14);
nr_nota_503_w			ptu_nota_hospitalar.nr_nota%type;
nr_lote_503_w			varchar2(8);
cd_hospital_w			varchar2(8);
cd_cid_obito_w			varchar2(6);
cd_cid_obito_0_w		varchar2(6);
cd_cid_obito_1_w		varchar2(6);
cd_cid_obito_2_w		varchar2(6);
cd_cid_obito_3_w		varchar2(6);
cd_cid_obito_4_w		varchar2(6);
cd_cid_obito_5_w		varchar2(6);
cd_unimed_hospital_w		varchar2(4);
tx_mult_amb_w			varchar2(4);
ie_tipo_acomodacao_w		varchar2(2);
cd_motivo_saida_w		varchar2(2);
qt_nasc_vivos_w			varchar2(2);
qt_nasc_mortos_w		varchar2(2);
qt_nasc_vivos_pre_w		varchar2(2);
qt_obito_precoce_w		varchar2(2);
qt_obito_tardio_w		varchar2(2);
ie_ind_acidente_w		varchar2(1);
ie_tipo_internacao_w		varchar2(1);
ie_int_gestacao_w		varchar2(1);
ie_int_aborto_w			varchar2(1);
ie_int_transtorno_w		varchar2(1);
ie_int_puerperio_w		varchar2(1);
ie_int_recem_nascido_w		varchar2(1);
ie_int_neonatal_w		varchar2(1);
ie_int_baixo_peso_w		varchar2(1);
ie_int_parto_cesarea_w		varchar2(1);
ie_int_parto_normal_w		varchar2(1);
ie_faturamento_w		varchar2(1);
ie_obito_mulher_w		varchar2(1);

-- 504
ds_servico_w			varchar2(80);
nm_prestador_w			varchar2(40);
nm_profissional_prestador_w	varchar2(40);
nm_prestador_requisitante_w	varchar2(40);
nr_gui_tiss_w			varchar2(20);
nr_cons_prof_prest_w		varchar2(15);
nr_cons_prof_req_w		varchar2(15);
vl_procedimento_w		varchar2(14);
vl_custo_operacional_w		varchar2(14);
vl_filme_w			varchar2(14);
vl_adic_procedimento_w		varchar2(14);
vl_adic_co_w			varchar2(14);
vl_adic_filme_w			varchar2(14);
nr_cgc_cpf_w			varchar2(14);
nr_cgc_cpf_req_w		varchar2(14);
vl_pago_prest_w			varchar2(14);
sg_cons_prof_prest_w		varchar2(12);
sg_cons_prof_req_w		varchar2(12);
nr_nota_504_w			ptu_nota_servico.nr_nota%type;
nr_seq_nota_w			varchar2(11);
nr_autorizacao_w		varchar2(10);
nr_lote_504_w			varchar2(8);
cd_prestador_w			varchar2(8);
dt_procedimento_w		varchar2(8);
cd_procedimento_w		varchar2(8);
qt_procedimento_w		varchar2(8);
cd_prestador_req_w		varchar2(8);
ds_hora_procedimento_w		varchar2(8);
cd_pacote_w			varchar2(8);
cd_cnes_prest_w			varchar2(7);
cd_unimed_prestador_w		varchar2(4);
cd_unimed_autorizadora_w	varchar2(4);
cd_unimed_pre_req_w		varchar2(4);
tx_procedimento_w		varchar2(3);
ie_via_acesso_w			varchar2(2);
cd_especialidade_w		varchar2(2);
ie_tipo_prestador_w		varchar2(2);
sg_uf_cons_prest_w		varchar2(2);
sg_uf_cons_req_w		varchar2(2);
ie_tipo_participacao_w		varchar2(1);
ie_tipo_tabela_w		varchar2(1);
cd_porte_anestesico_w		varchar2(1);
ie_rede_propria_w		varchar2(1);
ie_tipo_pessoa_prestador_w	varchar2(1);
ie_pacote_w			varchar2(1);
cd_ato_w			varchar2(1);
ie_reembolso_w			varchar2(1);
tp_autoriz_w			varchar2(1);
ie_prestador_alto_custo_w	varchar2(1);
cd_especialidade_ptu_w		number(2);

-- 505
ds_complemento_w		varchar2(100);
nr_nota_505_w			ptu_nota_complemento.nr_nota%type;
nr_lote_505_w			varchar2(8);
ie_tipo_complemento_w		varchar(1);

-- 506
ds_link_nfe_w			varchar2(120);
nr_nota_fiscal_w		varchar2(20);

-- 509
vl_total_serv_w			varchar2(14);
qt_total_serv_w			varchar2(11);
qt_total_r502_w			varchar2(5);
qt_total_r503_w			varchar2(5);
qt_total_r504_w			varchar2(5);
qt_total_r505_w			varchar2(5);
qt_nota_exec_w			varchar2(5);

-- 510
nm_cedente_sacado_w		varchar2(60);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(30);
nm_cidade_w			varchar2(30);
ds_complemento_r510_w		varchar2(20);
nr_inscricao_estadual_w		varchar2(20);
cd_cgc_cpf_w			varchar2(14);
cd_ans_w			varchar2(10);
cd_cep_w			varchar2(8);
nr_telefone_w			varchar2(8);
nr_fax_w			varchar2(8);
nr_ddd_w			varchar2(4);
sg_uf_w				varchar2(2);
ie_tipo_w			varchar2(1);

-- 511
ds_linha_w			varchar2(74);
vl_item_w			varchar2(14);
nr_linha_w			varchar2(2);
ie_documento_w			varchar2(1);

-- 512
ds_local_pagamento_w		varchar2(60);
ds_obs_local_pagto_w		varchar2(60);
cd_agencia_cedente_w		varchar2(25);
cd_nosso_numero_w		varchar2(20);
ds_obs_banco_w			varchar2(15);
vl_documento_w			varchar2(14);
ds_carteira_w			varchar2(10);
dt_documento_w			varchar2(8);
cd_especie_doc_w		varchar2(5);
dt_processamento_w		varchar2(8);
cd_banc_w			varchar2(5);
ds_especie_w			varchar2(4);
ie_aceite_w			varchar2(2);

-- 513
ds_instrucao_w			varchar2(60);

-- 514
ds_observacao_w			varchar2(60);

-- 515
ds_linha_digitavel_w		varchar2(60);
cd_barras_w			varchar2(44);

ds_conteudo_w			varchar2(600);
nr_seq_registro_w		number(10)	:= 0;
qt_registro_w			number(10)	:= 0;
nr_seq_nota_cobranca_w		number(10);
nr_seq_ptu_fatura_w		number(10);
nr_seq_nota_servico_w		number(10);
vl_total_adic_w			varchar2(14);

ds_meridiano_w			varchar2(3);
cd_cooperativa_w		varchar2(4);
cd_cgc_w			varchar(14);
sg_estado_w			pessoa_juridica.sg_estado%type;

Cursor C00 is
	select	lpad(cd_unimed_destino,4,'0'),
		lpad(cd_unimed_origem,4,'0'),
		to_char(dt_geracao,'yyyymmdd'),
		lpad(nr_competencia,4,'0'),
		lpad(nr_fatura,11,'0'),
		to_char(dt_vencimento_fatura,'yyyymmdd'),
		to_char(dt_emissao_fatura,'yyyymmdd'),
		lpad(replace(replace(campo_mascara(vl_total_fatura,2),',',''),'.',''),14,'0') vl_total_fatura,
		lpad(nvl(replace(replace(campo_mascara(vl_ir,2),',',''),'.',''),'0'),14,'0') vl_ir,
		lpad(obter_qt_registro_ptu_fatura(nr_sequencia, '502'),5,'0'),
		lpad(obter_qt_registro_ptu_fatura(nr_sequencia, '503'),5,'0'),
		lpad(obter_qt_registro_ptu_fatura(nr_sequencia, '504'),5,'0'),
		lpad(obter_qt_registro_ptu_fatura(nr_sequencia, '505'),5,'0'),
		lpad(obter_qt_registro_ptu_fatura(nr_sequencia, 'qt_nota'),5,'0'),
		lpad(obter_qt_registro_ptu_fatura(nr_sequencia, 'qt_serv'),11,'0'),
		lpad(replace(replace(campo_mascara(vl_total_taxa,2),',',''),'.',''),14,'0'),
		nvl(ie_classif_cobranca,'2'),
		lpad(nvl(nr_nota_credito_debito,'0'),11,'0'),
		lpad(nvl(to_char(dt_vencimento_ndc,'yyyymmdd'),' '),8,' '),
		lpad(nvl(to_char(dt_emissao_ndc,'yyyymmdd'),' '),8,' '),
		lpad(replace(replace(campo_mascara(vl_total_ndc,2),',',''),'.',''),14,'0'),
		nr_sequencia
	from	ptu_fatura
	where	nr_sequencia	= nr_seq_fatura_p;

Cursor C01 is
	select	lpad(nvl(elimina_caractere_especial(to_char(nr_lote)),'0'),8,'0'),
		lpad(nvl(substr(elimina_caractere_especial(to_char(nr_nota)),1,11),'0'),11,'0'),
		lpad(nvl(cd_unimed,'0'),4,'0'),
		lpad(nvl(cd_usuario_plano,'0'),13,'0'),
		rpad(nvl(elimina_acentuacao(nm_beneficiario),' '),25,' '),
		rpad(nvl(to_char(dt_atendimento,'yyyy/mm/ddhh24:mi:ss') || ds_meridiano_w,' '),21,' '),
		rpad(nvl(to_char(cd_excecao),' '),1,' '),
		rpad(nvl(to_char(ie_carater_atendimento),' '),1,' '),
		rpad(nvl(to_char(cd_cid),' '),6,' '),
		rpad(nvl(ie_paciente,' '),1,' '),
		rpad(nvl(ie_tipo_saida_spdat,' '),1,' '),
		lpad(nvl(ie_tipo_atendimento,'  '),2,'0'),
		nr_guia_principal,
		nr_sequencia,
		nr_nota
	from	ptu_nota_cobranca
	where	nr_seq_fatura	= nr_seq_ptu_fatura_w;

Cursor C02 is
	select	lpad(elimina_caractere_especial(a.nr_lote),8,'0') nr_lote_503,
		lpad(substr(elimina_caractere_especial(to_char(a.nr_nota)),1,11),11,'0') nr_nota_503,
		lpad(a.cd_unimed_hospital,4,'0'),
		lpad(a.cd_hospital,8,'0'),
		rpad(a.nm_hospital,25,' '),
		rpad(a.ie_tipo_acomodacao,2,' '),
		to_char(a.dt_internacao,'yyyy/mm/ddhh24:mi:ss') || ds_meridiano_w,
		to_char(a.dt_alta,'yyyy/mm/ddhh24:mi:ss') || ds_meridiano_w,
		lpad(replace(replace(campo_mascara(nvl(a.tx_mult_amb,0),2),',',''),'.',''),4,'0'),
		lpad(nvl(a.ie_ind_acidente,' '),1,' '),
		rpad(nvl(a.cd_motivo_saida,'  '),2,' '),
		lpad(nvl(a.cd_cgc_hospital,'  '),14,' '),
		lpad(nvl(a.ie_tipo_internacao,0),1,'0'),
		lpad(nvl(a.qt_nasc_vivos,'0'),2,'0'),
		lpad(nvl(a.qt_nasc_mortos,'0'),2,'0'),
		lpad(nvl(a.qt_nasc_vivos_pre,'0'),2,'0'),
		lpad(nvl(a.qt_obito_precoce,'0'),1,'0'),
		lpad(nvl(a.qt_obito_tardio,'0'),1,'0'),
		lpad(nvl(a.ie_int_gestacao,'N'),1,' '),
		lpad(nvl(a.ie_int_aborto,'N'),1,' '),
		lpad(nvl(a.ie_int_transtorno,'N'),1,' '),
		lpad(nvl(a.ie_int_puerperio,'N'),1,' '),
		lpad(nvl(a.ie_int_recem_nascido,'N'),1,' '),
		lpad(nvl(a.ie_int_neonatal,'N'),1,' '),
		lpad(nvl(a.ie_int_baixo_peso,'N'),1,' '),
		lpad(nvl(a.ie_int_parto_cesarea,'N'),1,' '),
		lpad(nvl(a.ie_int_parto_normal,'N'),1,' '),
		to_char(a.ie_faturamento),
		rpad(nvl(a.cd_cid_obito,' '),6,' '),
		nvl(to_char(a.ie_obito_mulher),'0'),
		rpad(nvl(a.nr_declara_obito,' '),17,' ')
	from	ptu_nota_hospitalar	a,
		ptu_nota_cobranca	c
	where	a.nr_seq_nota_cobr	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_nota_cobranca_w;

Cursor C03 is
	select	lpad(nvl(elimina_caractere_especial(a.nr_lote),'0'),8,'0'),
		decode(	lpad(nvl(substr(elimina_caractere_especial(to_char(a.nr_nota)),length(to_char(a.nr_nota)) - 10,length(to_char(a.nr_nota))),'0'),11,'0'),
			'00000000000',
			lpad(nvl(substr(elimina_caractere_especial(to_char(a.nr_nota)),1,11),'0'),11,'0'),
			lpad(nvl(substr(elimina_caractere_especial(to_char(a.nr_nota)),length(to_char(a.nr_nota)) - 10,length(to_char(a.nr_nota))),'0'),11,'0')),
		lpad(nvl(a.cd_unimed_prestador,'0'),4,'0'),
		nvl(a.cd_prestador,'0'),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(a.nm_prestador)),' '),40,' '),
		rpad(nvl(a.ie_tipo_participacao,' '),1,' '),
		rpad(nvl(to_char(a.dt_procedimento,'yyyymmdd'),' '),8,' '),
		rpad(nvl(to_char(a.ie_tipo_tabela),' '),1,' '),
		lpad(nvl(a.cd_servico,'0'),8,'0'),
		lpad(nvl(a.qt_procedimento,'0'),8,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_procedimento,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_custo_operacional,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_filme,2),',',''),'.',''),'0'),14,'0'),
		rpad(nvl(decode(a.cd_porte_anestesico,'0',null,a.cd_porte_anestesico),' '),1,' '),
		lpad(nvl(a.cd_unimed_autorizadora,'0'),4,'0'),
		lpad(nvl(a.cd_unimed_pre_req,'0'),4,'0'),
		lpad(nvl(a.cd_prestador_req,'0'),8,'0'),
		lpad(nvl(a.ie_via_acesso,'0'),2,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_adic_procedimento,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_adic_co,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(replace(replace(campo_mascara(a.vl_adic_filme,2),',',''),'.',''),'0'),14,'0'),
		lpad(nvl(a.cd_especialidade,'0'),2,'0'),
		lpad(nvl(a.ie_tipo_prestador,'0'),2,'0'),
		rpad(nvl(to_char(a.ie_rede_propria),' '),1,' '),
		rpad(nvl(to_char(a.ie_tipo_pessoa_prestador),' '),1,' '),
		lpad(nvl(a.nr_cgc_cpf,'0'),14,'0'),
		rpad(nvl(to_char(a.ie_pacote),' '),1,' '),
		rpad(nvl(to_char(a.cd_ato),' '),1,' '),
		lpad(nvl(a.tx_procedimento,'0'),3,'0'),
		lpad(nvl(a.nr_seq_nota,'0'),11,'0'),
		rpad(nvl(a.ds_hora_procedimento,' '),8,' '),
		rpad(nvl(a.cd_cnes_prest,' '),7,' '),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(a.nm_profissional_prestador)),' '),40,' '),
		rpad(nvl(a.sg_cons_prof_prest,' '),12,' '),
		rpad(nvl(a.nr_cons_prof_prest,' '),15,' '),
		rpad(nvl(a.sg_uf_cons_prest,' '),2,' '),
		lpad(nvl(a.nr_cgc_cpf_req,'0'),14,'0'),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(a.nm_prestador_requisitante)),' '),40,' '),
		rpad(nvl(a.sg_cons_prof_req,' '),12,' '),
		rpad(nvl(a.nr_cons_prof_req,' '),15,' '),
		rpad(nvl(a.sg_uf_cons_req,' '),2,' '),
		rpad(nvl(to_char(a.ie_reembolso),' '),1,' '),
		lpad(nvl(a.nr_autorizacao,'0'),10,'0'),
		lpad(nvl(replace(replace(campo_mascara(vl_pago_prest,2),',',''),'.',''),'0'),14,'0'),
		a.nr_sequencia,
		lpad(nvl(to_char(cd_pacote),'0'),8,'0'),
		rpad(nvl(nr_guia_tiss,' '),20,' '),
		rpad(nvl(to_char(nvl(tp_autoriz,1)),' '),1,' '),
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(ds_servico)),' '),80,' ')
	from	ptu_nota_servico	a,
		ptu_nota_cobranca	b
	where	a.nr_seq_nota_cobr	= b.nr_sequencia
	and	nvl(a.qt_procedimento,0) > 0
	and	b.nr_sequencia		= nr_seq_nota_cobranca_w;
	
Cursor C04 is
	select	lpad(nvl(elimina_caractere_especial(a.nr_lote),'0'),8,'0'),
		lpad(nvl(substr(elimina_caractere_especial(to_char(a.nr_nota)),length(to_char(a.nr_nota)) - 10,length(to_char(a.nr_nota))),'0'),11,'0'),
		nvl(to_char(a.ie_tipo_complemento),' '),
		rpad(nvl(replace(replace(elimina_caractere_especial(elimina_acentuacao(a.ds_complemento)),chr(10),' '),chr(13),' '),' '),100,' ')
	from	ptu_nota_complemento	a,
		ptu_nota_cobranca	b
	where	a.nr_seq_nota_cobr	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_nota_cobranca_w;
	
Cursor C05 is
	select	rpad(nvl(a.nr_nota_fiscal,'0'),20,' '),
		a.ds_link_nfe
	from	ptu_nota_fiscal		a,
		ptu_nota_cobranca	b
	where	a.nr_seq_nota_cobr	= b.nr_sequencia
	and	b.nr_sequencia		= nr_seq_nota_cobranca_w;
	
Cursor C06 is
	select	nvl(ie_tipo,' '),
		rpad(nvl(cd_ans,'0'),10,' '),
		rpad(nvl(elimina_acentuacao(nvl(nm_cedente_sacado,' ')),' '),60,' '),
		rpad(nvl(ds_endereco,' '),40,' '),
		rpad(nvl(ds_complemento,' '),20,' '),
		rpad(nvl(ds_bairro,' '),30,' '),
		rpad(nvl(cd_cep,' '),8,' '),
		rpad(nvl(nm_cidade,' '),30,' '),
		rpad(nvl(sg_uf,' '),2,' '),
		rpad(nvl(cd_cgc_cpf,' '),14,' '),
		lpad(nvl(nr_inscricao_estadual,'0'),20,'0'),
		lpad(nvl(nr_ddd,'0'),4,'0'),
		lpad(nvl(nr_telefone,'0'),8,'0'),
		lpad(nvl(nr_fax,'0'),8,'0')
	from	ptu_fatura_cedente
	where	nr_seq_fatura		= nr_seq_ptu_fatura_w;
	
Cursor C07 is
	select	lpad(nr_linha,2,'0'),
		rpad(elimina_caractere_especial(elimina_acentuacao(ds_linha)),74,' '),
		lpad(nvl(replace(replace(campo_mascara(vl_item,2),',',''),'.',''),'0'),14,'0'),
		nvl(ie_documento,'2')
	from	ptu_fatura_corpo
	where	nr_seq_fatura		= nr_seq_ptu_fatura_w
	order by nr_linha;
	
Cursor C08 is
	select	rpad(nvl(cd_banc,' '),5,' '),
		rpad(nvl(cd_agencia_cedente,' '),25,' '),
		rpad(nvl(cd_nosso_numero,' '),20,' '),
		rpad(nvl(ds_obs_banco,' '),15,' '),
		rpad(nvl(ds_carteira,' '),10,' '),
		rpad(nvl(ds_especie,' '),4,' '),
		lpad(nvl(replace(replace(campo_mascara(vl_documento,2),',',''),'.',''),'0'),14,'0'),
		rpad(nvl(to_char(dt_documento,'yyyymmdd'),' '),8,' '),
		rpad(nvl(cd_especie_doc,' '),5,' '),
		rpad(nvl(ie_aceite,' '),2,' '),
		rpad(nvl(to_char(dt_processamento,'yyyymmdd'),' '),8,' '),
		rpad(nvl(ds_local_pagamento,' '),60,' '),
		rpad(nvl(ds_obs_local_pagto,' '),60,' ')
	from	ptu_fatura_boleto
	where	nr_seq_fatura		= nr_seq_ptu_fatura_w;

Cursor C09 is
	select	rpad(nvl(ds_instrucao,' '),60,' '),
		rpad(nvl(ds_observacao,' '),60,' '),
		rpad(nvl(ds_linha_digitavel,' '),60,' '),
		rpad(nvl(cd_barras,' '),44,' ')
	from	ptu_fatura_boleto
	where	nr_seq_fatura		= nr_seq_ptu_fatura_w;
	
Cursor C10 is
	select	rpad(nvl(b.nr_declara_vivo,' '),15,' '),
		rpad(nvl(b.cd_cid_obito,' '),6,' '),
		rpad(nvl(b.nr_declara_obito,' '),17,' ')
	from	ptu_nota_hospitalar	a,
		ptu_nota_hosp_compl	b,
		ptu_nota_cobranca	c
	where	a.nr_sequencia		= b.nr_seq_nota_hosp(+)
	and	a.nr_seq_nota_cobr	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_nota_cobranca_w;

begin
delete from  w_ptu_envio_arq where nm_usuario = nm_usuario_p;

if	(nr_seq_fatura_p is not null) then
	nr_versao_transacao_w := 19;

	-- Padr�o brasileiro � -3, Regi�es Sul, Sudeste e Nordeste, Goi�s, Distrito Federal, Tocantins, Amap� e Par�
	ds_meridiano_w := '-03';

	select	lpad(cd_unimed_destino,4,'0')
	into	cd_cooperativa_w
	from	ptu_fatura
	where	nr_sequencia = nr_seq_fatura_p;

	select	max(cd_cgc)
	into	cd_cgc_w
	from	pls_congenere
	where	lpad(cd_cooperativa,4,'0') = cd_cooperativa_w;

	select	max(sg_estado)
	into	sg_estado_w
	from	pessoa_juridica
	where	cd_cgc = cd_cgc_w;

	if	(sg_estado_w in ('RN','PB','PE','AL')) then
		ds_meridiano_w := '-02';
	end if;

	if	(sg_estado_w in ('MS','MT','RO','AC','AM','RR')) then
		ds_meridiano_w := '-04';
	end if;

	open C00;
	loop
	fetch C00 into	
		cd_unimed_destino_w,
		cd_unimed_origem_w,
		dt_geracao_w,
		nr_competencia_w,
		nr_fatura_w,
		dt_venc_fatura_w,
		dt_emissao_fatura_w,
		vl_total_fatura_w,
		vl_ir_w,
		qt_total_r502_w,
		qt_total_r503_w,
		qt_total_r504_w,
		qt_total_r505_w,
		qt_nota_exec_w,
		qt_total_serv_w,
		vl_total_serv_w,
		ie_classif_cobranca_w,
		nr_nota_credito_debito_w,
		dt_vencimento_ndc_w,
		dt_emissao_ndc_w,
		vl_total_ndc_w,
		nr_seq_ptu_fatura_w;
	exit when C00%notfound;
		begin
		nr_seq_registro_w	:= nr_seq_registro_w + 1;

		ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '501' || cd_unimed_destino_w || cd_unimed_origem_w || dt_geracao_w || nr_competencia_w || nr_fatura_w ||
					dt_venc_fatura_w || dt_emissao_fatura_w || vl_total_fatura_w || vl_ir_w || lpad(nr_versao_transacao_w,30,' ') || 
					ie_classif_cobranca_w || nr_nota_credito_debito_w || dt_vencimento_ndc_w || dt_emissao_ndc_w || vl_total_ndc_w;

		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo,
			nr_seq_apres)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w,
			nr_seq_registro_w);

		open C01;
		loop
		fetch C01 into
			nr_lote_w,
			nr_nota_w,
			cd_unimed_w,
			cd_usuario_plano_w,
			nm_beneficiario_w,
			dt_atendimento_w,
			cd_excecao_w,
			ie_carater_atendimento_w,
			cd_cid_w,
			ie_paciente_w,
			ie_tipo_saida_spdat_w,
			ie_tipo_atendimento_w,
			nr_guia_principal_int_w,
			nr_seq_nota_cobranca_w,
			nr_nota_geral_w;
		exit when C01%notfound;
			begin
			qt_registro_w		:= 0;
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
				
			if	(length(nr_guia_principal_int_w) > 11) and
				(substr(lpad(nr_guia_principal_int_w,20,'0'),10,11) <> lpad('0',11,'0')) then			
				nr_guia_principal_w := substr(nr_guia_principal_int_w,length(nr_guia_principal_int_w)-10,length(nr_guia_principal_int_w));
			else	
				nr_guia_principal_w := lpad(nvl(nr_guia_principal_int_w,'0'),11,'0');
			end if;
			
			if	(length(nr_nota_geral_w) > 11) and
				(substr(lpad(nr_nota_geral_w,20,'0'),10,11) <> lpad('0',11,'0')) then			
				nr_nota_w := substr(nr_nota_geral_w,length(nr_nota_geral_w)-10,length(nr_nota_geral_w));
			else	
				nr_nota_w := lpad(nvl(nr_nota_geral_w,'0'),11,'0');
			end if;
			
			select	count(1)
			into	qt_registro_w
			from	ptu_nota_hospitalar	a,
				ptu_nota_hosp_compl	b,
				ptu_nota_cobranca	c
			where	a.nr_sequencia		= b.nr_seq_nota_hosp
			and	a.nr_seq_nota_cobr	= c.nr_sequencia
			and	c.nr_sequencia		= nr_seq_nota_cobranca_w
			and	rownum			<= 1;
			
			/* Quando antecede registro 503, deve ser deixado o espa�o em branco - OS 430452 */
			if	(qt_registro_w > 0) then
				ie_tipo_saida_spdat_w := ' ';
			end if;
			
			ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '502' || nr_lote_w || nr_nota_w || ' ' || cd_unimed_destino_w || '   ' || 
					cd_usuario_plano_w || nm_beneficiario_w || dt_atendimento_w || cd_excecao_w || ie_carater_atendimento_w || '  ' || 
					cd_cid_w || lpad(' ',14,' ') || ie_paciente_w || ie_tipo_saida_spdat_w || ie_tipo_atendimento_w || 
					nr_guia_principal_w;

			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);

			open C02;
			loop
			fetch C02 into
				nr_lote_503_w,
				nr_nota_503_w,
				cd_unimed_hospital_w,
				cd_hospital_w,
				nm_hospital_w,
				ie_tipo_acomodacao_w,
				dt_internacao_w,
				dt_alta_w,
				tx_mult_amb_w,
				ie_ind_acidente_w,
				cd_motivo_saida_w,
				cd_cgc_hospital_w,
				ie_tipo_internacao_w,
				qt_nasc_vivos_w,
				qt_nasc_mortos_w,
				qt_nasc_vivos_pre_w,
				qt_obito_precoce_w,
				qt_obito_tardio_w,
				ie_int_gestacao_w,
				ie_int_aborto_w,
				ie_int_transtorno_w,
				ie_int_puerperio_w,
				ie_int_recem_nascido_w,
				ie_int_neonatal_w,
				ie_int_baixo_peso_w,
				ie_int_parto_cesarea_w,
				ie_int_parto_normal_w,
				ie_faturamento_w,
				cd_cid_obito_w,
				ie_obito_mulher_w,
				nr_declara_obito_w;
			exit when C02%notfound;
				begin	
				nr_declara_vivo_1_w := null;
				nr_declara_vivo_2_w := null;
				nr_declara_vivo_3_w := null;
				nr_declara_vivo_4_w := null;
				nr_declara_vivo_5_w := null;
				cd_cid_obito_1_w := null;
				cd_cid_obito_2_w := null;
				cd_cid_obito_3_w := null;
				cd_cid_obito_4_w := null;
				cd_cid_obito_5_w := null;
				nr_declara_obito_1_w := null;
				nr_declara_obito_2_w := null;
				nr_declara_obito_3_w := null;
				nr_declara_obito_4_w := null;
				nr_declara_obito_5_w := null;
				nr_nota_503_w := nr_nota_w;
				
				open C10;
				loop
				fetch C10 into	
					nr_declara_vivo_0_w,
					cd_cid_obito_0_w,
					nr_declara_obito_0_w;
				exit when C10%notfound;
					begin
					if	(nr_declara_vivo_1_w is null) then
						nr_declara_vivo_1_w := nr_declara_vivo_0_w;
					
					elsif	(nr_declara_vivo_2_w is null) then
						nr_declara_vivo_2_w := nr_declara_vivo_0_w;
						
					elsif	(nr_declara_vivo_3_w is null) then
						nr_declara_vivo_3_w := nr_declara_vivo_0_w;
						
					elsif	(nr_declara_vivo_4_w is null) then
						nr_declara_vivo_4_w := nr_declara_vivo_0_w;
					
					elsif	(nr_declara_vivo_5_w is null) then
						nr_declara_vivo_5_w := nr_declara_vivo_0_w;
					end if;
					
					if	(cd_cid_obito_1_w is null) then
						cd_cid_obito_1_w := cd_cid_obito_0_w;
					
					elsif	(cd_cid_obito_2_w is null) then
						cd_cid_obito_2_w := cd_cid_obito_0_w;
						
					elsif	(cd_cid_obito_3_w is null) then
						cd_cid_obito_3_w := cd_cid_obito_0_w;
						
					elsif	(cd_cid_obito_4_w is null) then
						cd_cid_obito_4_w := cd_cid_obito_0_w;
					
					elsif	(cd_cid_obito_5_w is null) then
						cd_cid_obito_5_w := cd_cid_obito_0_w;
					end if;
					
					if	(nr_declara_obito_1_w is null) then
						nr_declara_obito_1_w := nr_declara_obito_0_w;
					
					elsif	(nr_declara_obito_2_w is null) then
						nr_declara_obito_2_w := nr_declara_obito_0_w;
						
					elsif	(nr_declara_obito_3_w is null) then
						nr_declara_obito_3_w := nr_declara_obito_0_w;
						
					elsif	(nr_declara_obito_4_w is null) then
						nr_declara_obito_4_w := nr_declara_obito_0_w;
					
					elsif	(nr_declara_obito_5_w is null) then
						nr_declara_obito_5_w := nr_declara_obito_0_w;
					end if;				
					end;
				end loop;
				close C10;
				
				nr_seq_registro_w	:= nr_seq_registro_w + 1;

				ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '503' || nr_lote_503_w || nr_nota_503_w || 
						cd_unimed_hospital_w || cd_hospital_w || nm_hospital_w || ie_tipo_acomodacao_w || dt_internacao_w || 
						dt_alta_w || tx_mult_amb_w || ie_ind_acidente_w || cd_motivo_saida_w || cd_cgc_hospital_w || 
						ie_tipo_internacao_w || '  ' || qt_nasc_vivos_w || qt_nasc_mortos_w || qt_nasc_vivos_pre_w || 
						qt_obito_precoce_w || qt_obito_tardio_w || ie_int_gestacao_w || ie_int_aborto_w || ie_int_transtorno_w || 
						ie_int_puerperio_w || ie_int_recem_nascido_w || ie_int_neonatal_w || ie_int_baixo_peso_w || 
						ie_int_parto_cesarea_w || ie_int_parto_normal_w || ie_faturamento_w || cd_cid_obito_w || '       ' || 
						rpad(nvl(nr_declara_vivo_1_w,' '),15,' ') || rpad(nvl(nr_declara_vivo_2_w,' '),15,' ') || 
						rpad(nvl(nr_declara_vivo_3_w,' '),15,' ') || rpad(nvl(nr_declara_vivo_4_w,' '),15,' ') || 
						rpad(nvl(nr_declara_vivo_5_w,' '),15,' ') || ie_obito_mulher_w || nr_declara_obito_w || 
						rpad(nvl(cd_cid_obito_1_w,' '),6,' ') || rpad(nvl(cd_cid_obito_2_w,' '),6,' ') || rpad(nvl(cd_cid_obito_3_w,' '),6,' ') ||
						rpad(nvl(cd_cid_obito_4_w,' '),6,' ') ||  rpad(nvl(cd_cid_obito_5_w,' '),6,' ') || 
						rpad(nvl(nr_declara_obito_1_w,' '),17,' ') || rpad(nvl(nr_declara_obito_2_w,' '),17,' ') || 
						rpad(nvl(nr_declara_obito_3_w,' '),17,' ') || rpad(nvl(nr_declara_obito_4_w,' '),17,' ') ||
						rpad(nvl(nr_declara_obito_5_w,' '),17,' ');

				insert into w_ptu_envio_arq
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ds_conteudo,
					nr_seq_apres)
				values	(w_ptu_envio_arq_seq.nextval,
					sysdate,
					nm_usuario_p,
					ds_conteudo_w,
					nr_seq_registro_w);
				end;
			end loop;
			close C02;

			open C03;
			loop
			fetch C03 into
				nr_lote_504_w,
				nr_nota_504_w,
				cd_unimed_prestador_w,
				cd_prestador_w,
				nm_prestador_w,
				ie_tipo_participacao_w,
				dt_procedimento_w,
				ie_tipo_tabela_w,
				cd_procedimento_w,
				qt_procedimento_w,
				vl_procedimento_w,
				vl_custo_operacional_w,
				vl_filme_w,
				cd_porte_anestesico_w,
				cd_unimed_autorizadora_w,
				cd_unimed_pre_req_w,
				cd_prestador_req_w,
				ie_via_acesso_w,
				vl_adic_procedimento_w,
				vl_adic_co_w,
				vl_adic_filme_w,
				cd_especialidade_w,
				ie_tipo_prestador_w,
				ie_rede_propria_w,
				ie_tipo_pessoa_prestador_w,
				nr_cgc_cpf_w,
				ie_pacote_w,
				cd_ato_w,
				tx_procedimento_w,
				nr_seq_nota_w,
				ds_hora_procedimento_w,
				cd_cnes_prest_w,
				nm_profissional_prestador_w,
				sg_cons_prof_prest_w,
				nr_cons_prof_prest_w,
				sg_uf_cons_prest_w,
				nr_cgc_cpf_req_w,
				nm_prestador_requisitante_w,
				sg_cons_prof_req_w,
				nr_cons_prof_req_w,
				sg_uf_cons_req_w,
				ie_reembolso_w,
				nr_autorizacao_w,
				vl_pago_prest_w,
				nr_seq_nota_servico_w,
				cd_pacote_w,
				nr_gui_tiss_w,
				tp_autoriz_w,
				ds_servico_w;
			exit when C03%notfound;
				begin
				nr_seq_registro_w	:= nr_seq_registro_w + 1;
				nr_nota_504_w		:= nr_nota_w;

				if	(cd_prestador_w is not null) then
					select	max(a.ie_prestador_alto_custo)
					into	ie_prestador_alto_custo_w
					from	pls_prestador a
					where	upper(a.cd_prestador) = upper(cd_prestador_w);
				end if;
				
				cd_prestador_w := lpad(cd_prestador_w,8,'0');
				ie_prestador_alto_custo_w := nvl(ie_prestador_alto_custo_w,'N');
				
				-- Utiliza o c�digo de especialidade do PTU
				if	(cd_especialidade_w is not null) then
					select	max(cd_ptu)
					into	cd_especialidade_ptu_w
					from	especialidade_medica
					where	cd_especialidade = cd_especialidade_w;
					
					if	(cd_especialidade_ptu_w is not null) then
						cd_especialidade_w := lpad(nvl(cd_especialidade_ptu_w,'0'),2,'0');
					end if;
				end if;
				
				-- Mesma informa��o do registro 502
				nr_nota_504_w := nr_nota_w;
				
				ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '504' || nr_lote_504_w || nr_nota_504_w || 
						cd_unimed_prestador_w || cd_prestador_w || nm_prestador_w || ie_tipo_participacao_w || dt_procedimento_w ||
						ie_tipo_tabela_w || cd_procedimento_w || qt_procedimento_w || vl_procedimento_w || vl_custo_operacional_w ||
						vl_filme_w || cd_porte_anestesico_w || cd_unimed_autorizadora_w ||'         '|| cd_unimed_pre_req_w || 
						cd_prestador_req_w || ie_via_acesso_w || vl_adic_procedimento_w || vl_adic_co_w || vl_adic_filme_w || 
						cd_especialidade_w || ie_tipo_prestador_w || ie_rede_propria_w || ie_tipo_pessoa_prestador_w || 
						nr_cgc_cpf_w || ie_pacote_w ||  cd_ato_w || tx_procedimento_w || nr_seq_nota_w || ds_hora_procedimento_w || 
						cd_cnes_prest_w || nm_profissional_prestador_w || sg_cons_prof_prest_w || nr_cons_prof_prest_w || 
						sg_uf_cons_prest_w || '  ' || nr_cgc_cpf_req_w || nm_prestador_requisitante_w || sg_cons_prof_req_w || 
						nr_cons_prof_req_w || sg_uf_cons_req_w || ie_reembolso_w || nr_autorizacao_w || vl_pago_prest_w || 
						cd_pacote_w || lpad(nr_nota_504_w,20,'0') || tp_autoriz_w ||ds_servico_w || ie_prestador_alto_custo_w;

				insert into w_ptu_envio_arq
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ds_conteudo,
					nr_seq_apres)
				values	(w_ptu_envio_arq_seq.nextval,
					sysdate,
					nm_usuario_p,
					ds_conteudo_w,
					nr_seq_registro_w);
					
				update	ptu_nota_servico
				set	nr_seq_a500	= nr_seq_registro_w
				where	nr_sequencia	= nr_seq_nota_servico_w;
				end;
			end loop;
			close C03;
			
			open C04;
			loop
			fetch C04 into	
				nr_lote_505_w,
				nr_nota_505_w,
				ie_tipo_complemento_w,
				ds_complemento_w;
			exit when C04%notfound;
				begin
				nr_seq_registro_w	:= nr_seq_registro_w + 1;
				
				-- Mesma informa��o do registro 502
				nr_nota_505_w := nr_nota_w;

				ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '505' || nr_lote_505_w || nr_nota_505_w || 
						ie_tipo_complemento_w || ds_complemento_w;

				insert into w_ptu_envio_arq
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ds_conteudo,
					nr_seq_apres)
				values	(w_ptu_envio_arq_seq.nextval,
					sysdate,
					nm_usuario_p,
					ds_conteudo_w,
					nr_seq_registro_w);
				end;
			end loop;
			close C04;
			
			open C05;
			loop
			fetch C05 into	
				nr_nota_fiscal_w,
				ds_link_nfe_w;
			exit when C05%notfound;
				begin
				nr_seq_registro_w	:= nr_seq_registro_w + 1;

				ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '506' || nr_nota_fiscal_w || ds_link_nfe_w;

				insert into w_ptu_envio_arq
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ds_conteudo,
					nr_seq_apres)
				values	(w_ptu_envio_arq_seq.nextval,
					sysdate,
					nm_usuario_p,
					ds_conteudo_w,
					nr_seq_registro_w);
				end;
			end loop;
			close C05;
			end;
		end loop;
		close C01;
		
		/*select	lpad(nvl(replace(replace(campo_mascara(nvl(sum(a.vl_procedimento),0) + nvl(sum(a.vl_filme),0) + nvl(sum(a.vl_custo_operacional),0),2),',',''),'.',''),'0'),14,'0')
		into	vl_total_adic_w
		from	ptu_nota_servico	a,
			ptu_nota_cobranca	b
		where	a.nr_seq_nota_cobr	= b.nr_sequencia
		and	b.nr_seq_fatura		= nr_seq_fatura_p;*/
		
		select	lpad(nvl(replace(replace(campo_mascara(nvl(sum(a.vl_procedimento),0),2),',',''),'.',''),'0'),14,'0')
		into	vl_total_adic_w
		from	ptu_nota_servico	a,
			ptu_nota_cobranca	b
		where	a.nr_seq_nota_cobr	= b.nr_sequencia
		and	b.nr_seq_fatura		= nr_seq_fatura_p;
		
		nr_seq_registro_w	:= nr_seq_registro_w + 1;

		ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '509' || qt_total_r502_w || qt_total_r503_w || qt_total_r504_w || 
				qt_total_r505_w || qt_nota_exec_w || '     ' || qt_total_serv_w || vl_total_adic_w;
				
		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo,
			nr_seq_apres)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w,
			nr_seq_registro_w);
			
		open C06;
		loop
		fetch C06 into	
			ie_tipo_w,
			cd_ans_w,
			nm_cedente_sacado_w,
			ds_endereco_w,
			ds_complemento_w,
			ds_bairro_w,
			cd_cep_w,
			nm_cidade_w,
			sg_uf_w,
			cd_cgc_cpf_w,
			nr_inscricao_estadual_w,
			nr_ddd_w,
			nr_telefone_w,
			nr_fax_w;
		exit when C06%notfound;
			begin		
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			
			ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '510' || ie_tipo_w || cd_ans_w || nm_cedente_sacado_w || 
					ds_endereco_w || ds_complemento_w || ds_bairro_w || cd_cep_w || nm_cidade_w || sg_uf_w || ' ' || cd_cgc_cpf_w || 
					nr_inscricao_estadual_w || nr_ddd_w || nr_telefone_w || nr_fax_w;
					
			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
			end;
		end loop;
		close C06;

		open C07;
		loop
		fetch C07 into	
			nr_linha_w,
			ds_linha_w,
			vl_item_w,
			ie_documento_w;
		exit when C07%notfound;
			begin
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			
			ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '511' || nr_linha_w || ds_linha_w || vl_item_w || ie_documento_w;
					
			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
			end;
		end loop;
		close C07;
		
		open C08;
		loop
		fetch C08 into
			cd_banc_w,
			cd_agencia_cedente_w,
			cd_nosso_numero_w,
			ds_obs_banco_w,
			ds_carteira_w,
			ds_especie_w,
			vl_documento_w,
			dt_documento_w,
			cd_especie_doc_w,
			ie_aceite_w,
			dt_processamento_w,
			ds_local_pagamento_w,
			ds_obs_local_pagto_w;
		exit when C08%notfound;
			begin
			nr_seq_registro_w	:= nr_seq_registro_w + 1;		
					
			ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '512' || cd_banc_w || cd_agencia_cedente_w || cd_nosso_numero_w || 
					ds_obs_banco_w || ds_carteira_w || ds_especie_w || vl_documento_w || dt_documento_w || cd_especie_doc_w || 
					ie_aceite_w || dt_processamento_w || ds_local_pagamento_w || ds_obs_local_pagto_w;
			
			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
			end;
		end loop;
		close C08;
		
		open C09;
		loop
		fetch C09 into
			ds_instrucao_w,
			ds_observacao_w,
			ds_linha_digitavel_w,
			cd_barras_w;
		exit when C09%notfound;
			begin
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			
			ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '513' || ds_instrucao_w;
			
			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
				
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			
			ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '514' || ds_observacao_w;
			
			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
			
			nr_seq_registro_w	:= nr_seq_registro_w + 1;
			
			ds_conteudo_w	:= lpad(to_char(nr_seq_registro_w),8,'0') || '515' || ds_linha_digitavel_w || cd_barras_w;
			
			insert into w_ptu_envio_arq
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_conteudo,
				nr_seq_apres)
			values	(w_ptu_envio_arq_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_conteudo_w,
				nr_seq_registro_w);
			end;
		end loop;
		close C09;	
		end;
	end loop;
	close C00;

	commit;
end if;
	
end ptu_gerar_envio_fatura_v41;
/