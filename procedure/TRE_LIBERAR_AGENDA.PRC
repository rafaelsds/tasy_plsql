create or replace
procedure tre_liberar_agenda (nr_seq_agenda_p   number,
			      nm_usuario_p	varchar2) is
			      
		

nr_seq_tipo_w			number(10);
quebra_w				varchar2(10) := chr(13)||chr(10);	
ds_perfil_w				varchar2(4000);	
ds_setor_w				varchar2(255);
cd_setor_Atendimento_w	number(10);
cd_perfil_w				number(10);	
ds_comunicado_w			long;
ds_agenda_w				varchar2(255);	
dt_inicio_w				date;
dt_termino_w			date;
nm_palestrante_w		varchar2(80);
ds_modulo_w				varchar2(80);
QT_CARGA_HORARIA_w		number(13,2);
nr_seq_modulo_w			number(10);	
nm_palestrante_adic_w	varchar(2000);
	
		
cursor c01 is
	select	cd_perfil,
		cd_setor_atendimento
	from	tre_regra_envio_comunic
	where	nr_seq_tipo	= nr_seq_tipo_w;
	
Cursor C02 is
	select	substr(Obter_descricao_modulo(nr_seq_modulo),1,255),
		substr(obter_nome_pf(cd_palestrante),1,80),
		qt_carga_horaria,
		nr_sequencia
	from	TRE_AGENDA_MODULO a
	where	a.nr_seq_agenda	 =nr_seq_agenda_p;
	
Cursor C03 is
	select	substr(obter_nome_pf(cd_pessoa_fisica),1,80)
	from	tre_eve_mod_palestrante
	where	nr_seq_modulo = nr_seq_modulo_w;
begin
begin
select 	a.nr_seq_tipo,
	ds_agenda,
	nvl(dt_inicio_real,dt_inicio),
	nvl(dt_termino_real,dt_termino)
into	nr_seq_tipo_w,
	ds_agenda_w,
	dt_inicio_w,
	dt_termino_w
from	tre_curso a,
	tre_agenda b
where	a.nr_sequencia	= b.nr_seq_curso
and	b.nr_sequencia	= nr_seq_agenda_p;
exception
	when others then
		null;
	end;
	
update 	tre_agenda
set 	dt_liberacao = sysdate,
	nm_usuario   = nm_usuario_p
where 	nr_sequencia = nr_seq_agenda_p;

open C01;
loop
fetch C01 into	
	cd_perfil_w,
	cd_setor_Atendimento_w;
exit when C01%notfound;
	begin
	if	(cd_perfil_w is not null) then
		ds_perfil_w	:= ds_perfil_w ||','||cd_perfil_w;
	end if;
	if	(cd_setor_Atendimento_w is not null) then
		ds_setor_w	:= ds_setor_w ||','||cd_setor_Atendimento_w;
	end if;
	end;
end loop;
close C01;
dbms_output.put_line('ds_setor_w='||ds_setor_w);
dbms_output.put_line('ds_perfil_w='||ds_perfil_w);
if	(ds_setor_w is not null) or
	(ds_perfil_w is not null) then
	ds_comunicado_w		:= quebra_w||ds_agenda_w ||quebra_w||quebra_w;
	ds_comunicado_w		:= ds_comunicado_w ||obter_desc_expressao(286964)	||': '/*' Data in�cio: '*/||to_char(dt_inicio_w,'dd/mm/yyyy hh24:mi:ss')||quebra_w;
	ds_comunicado_w		:= ds_comunicado_w ||obter_desc_expressao(287236)	||': '/*' Data t�rmino: '*/||to_char(dt_termino_w,'dd/mm/yyyy hh24:mi:ss')||quebra_w;
	ds_comunicado_w		:= ds_comunicado_w||quebra_w||quebra_w;
	ds_comunicado_w		:= ds_comunicado_w || obter_desc_expressao(314230)	||': '/*' M�dulos: '*/||quebra_w;
	
	open C02;
	loop
	fetch C02 into	
		ds_modulo_w,
		nm_palestrante_w,
		QT_CARGA_HORARIA_w,
		nr_seq_modulo_w;
	exit when C02%notfound;
		begin
			open C03;
				loop
				fetch C03 into	
					nm_palestrante_adic_w;
				exit when C03%notfound;
					begin
					nm_palestrante_w := nm_palestrante_w+', '+ nm_palestrante_adic_w;
					end;
				end loop;
			close C03;
		
		ds_comunicado_w	:=	ds_comunicado_w||' - '||ds_modulo_w||obter_desc_expressao(284620)/*' Carga hor�ria: '*/||QT_CARGA_HORARIA_w
					|| '  Palestrantes: '||nm_palestrante_w ||quebra_w;
		end;
	end loop;
	close C02;
	INSERT  INTO comunic_interna(   
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			nr_sequencia,
			ie_gerencial,
			nr_seq_classif,
			ds_perfil_adicional,
			cd_setor_destino,
			cd_estab_destino,
			ds_setor_adicional,
			dt_liberacao)
	VALUES 	(SYSDATE,
		ds_agenda_w,
		ds_comunicado_w,
		nm_usuario_p,
		SYSDATE,
		'N',
		null,
		comunic_interna_seq.nextval,
		'N',
		7,
		substr(ds_perfil_w,2,2000)||',',
		null,
		null,
		substr(ds_setor_w,2,2000)||',',
		SYSDATE);
end if;

commit;

end tre_liberar_agenda;
/
