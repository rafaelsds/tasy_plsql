create or replace
procedure pls_cta_val_aplicar_fil_pres(	dados_regra_p		pls_tipos_cta_val_pck.dados_regra,
					dados_filtro_p		pls_tipos_cta_val_pck.dados_filtro,
					dados_consistencia_p	pls_tipos_cta_val_pck.dados_consistencia,
					nr_id_transacao_p	pls_rp_cta_selecao.nr_id_transacao%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is

dados_filtro_prest_w	pls_tipos_cta_val_pck.dados_filtro_prest;			

select_completo_w	varchar2(32000);
dados_restricao_w	pls_tipos_cta_val_pck.dados_restricao_select;

var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;

qt_cnt_w		pls_integer;
nr_seq_conta_w		dbms_sql.number_table;
nr_seq_conta_proc_w	dbms_sql.number_table;
nr_seq_selecao_w	dbms_sql.clob_table;

ret_null_w		varchar2(500);

-- Tipos de prestadores cadastrados.
cursor cs_tipo_filtro
	(nr_seq_filtro_pc	pls_oc_cta_filtro.nr_sequencia%type) is
	select	distinct x.ie_tipo_prestador ie_tipo_filtro
	from	pls_rp_filtro_prest x
	where	x.nr_seq_rp_cta_filtro = nr_seq_filtro_pc
	and	x.ie_situacao = 'A';

-- Filtros informados
Cursor C_filtro
	(nr_seq_filtro_pc	pls_rp_cta_filtro.nr_sequencia%type,
	 ie_tipo_filtro_pc	pls_rp_filtro_prest.ie_tipo_prestador%type) is
	select	a.cd_prestador           cd_prestador,
	        a.ie_tipo_prestador      ie_tipo_prestador,
	        a.nr_seq_rp_cta_filtro   nr_seq_rp_cta_filtro,
	        a.nr_seq_prestador       nr_seq_prestador,
	        a.nr_sequencia           nr_sequencia
	from	pls_rp_filtro_prest a
	where	a.nr_seq_rp_cta_filtro	= nr_seq_filtro_pc
	and	a.ie_tipo_prestador = ie_tipo_filtro_pc
	and	a.ie_situacao = 'A';

begin

if	(dados_regra_p.nr_seq_regra is not null) then
	
	-- Obter o controle padr�o para quantidade de registros que ser� enviada a cada vez para a tabela de sele��o.
	qt_cnt_w := pls_cta_consistir_pck.qt_registro_transacao_w;
	
	-- Buscar os tipos de prestadores cadastrados para este filtro.
	for	rw_tipo_filtro_w in cs_tipo_filtro(dados_filtro_p.nr_sequencia) loop

		-- Atualizar o campo ie_valido_temp para N  na tabela PLS_SELECAO_OCOR_CTA  para todos os registros.
		pls_tipos_cta_val_pck.atualiza_sel_ie_valido_temp (nr_id_transacao_p, dados_filtro_p, 'F');

		-- Passar para todos os filtros da regra.
		for	r_C_filtro_w in C_filtro(dados_filtro_p.nr_sequencia, rw_tipo_filtro_w.ie_tipo_filtro) loop
			
			-- Atualizar a vari�vel com os dados do filtro
			dados_filtro_prest_w.cd_prestador           	:= r_C_filtro_w.cd_prestador;
			dados_filtro_prest_w.ie_tipo_prestador      	:= r_C_filtro_w.ie_tipo_prestador;
			dados_filtro_prest_w.nr_seq_rp_cta_filtro   	:= r_C_filtro_w.nr_seq_rp_cta_filtro; 
			dados_filtro_prest_w.nr_seq_prestador       	:= r_C_filtro_w.nr_seq_prestador;   
			dados_filtro_prest_w.nr_sequencia           	:= r_C_filtro_w.nr_sequencia;
				
			-- Obter restri��es
			dados_restricao_w := pls_val_cta_obter_restr_padrao( 	'RESTRICAO', dados_consistencia_p, nr_id_transacao_p,
										null, dados_filtro_p, cd_estabelecimento_p,
										nm_usuario_p);
			
			-- Como a restri��o por prestador � aplic�vel para o n�vel de conta ent�o a restri��o montada pelo filtro ser� aplicada juntamente com a restri��o padr�o por conta, pois conforme 
			-- a regra de granularidade eliminando a conta n�o precisamos verificar os itens, portanto cada tipo de filtro deve ser aplicado ao seu n�vel.
			dados_restricao_w.ds_restricao_conta := dados_restricao_w.ds_restricao_conta || 
								pls_cta_val_obter_restr_pres(	'RESTRICAO', var_cur_w,
												dados_filtro_prest_w);
			
			-- Montar o select a ser executado dinamicamente
			select_completo_w := pls_tipos_cta_val_pck.pls_val_cta_montar_sel_pad(dados_restricao_w, nm_usuario_p);
			
			-- Abrir um novo cursor
			var_cur_w := dbms_sql.open_cursor;
			begin
				-- Criar o cursor
				dbms_sql.parse(var_cur_w, select_completo_w, 1);
				
				-- Trocar BINDS
				-- Do select original
				dados_restricao_w := pls_val_cta_obter_restr_padrao(	'BINDS', dados_consistencia_p, nr_id_transacao_p, 
											var_cur_w, dados_filtro_p, cd_estabelecimento_p,
											nm_usuario_p); 
				-- Trocar binds do select Benef
				ret_null_w := pls_cta_val_obter_restr_pres(	'BINDS', var_cur_w,
										dados_filtro_prest_w);
				
				-- Definir para o DBMS_SQL que o retorno do select ser�  preenchido em arrays, definindo a quantidade de linhas que o array ter� a cada itera��o do loop
				-- e a posi��o inicial que estes ocupar�o no array.
				dbms_sql.define_array(var_cur_w, 1, nr_seq_conta_w, qt_cnt_w, 1);
				dbms_sql.define_array(var_cur_w, 2, nr_seq_conta_proc_w, qt_cnt_w, 1);
				dbms_sql.define_array(var_cur_w, 3, nr_seq_selecao_w, qt_cnt_w, 1);
				
				var_exec_w := dbms_sql.execute(var_cur_w);
				
				loop
				-- O fetch rows ir� preencher os buffers do Oracle com as linhas que ser�o passadas para a lista quando o COLUMN_VALUE for chamado.
				var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
				
					-- zerar as listas para que o mesmo valor n�o seja inserido mais de uma vez na tabela.
					nr_seq_conta_w		:= pls_util_cta_pck.num_table_vazia_w;
					nr_seq_conta_proc_w	:= pls_util_cta_pck.num_table_vazia_w;		
					nr_seq_selecao_w	:= pls_util_cta_pck.clob_table_vazia_w;
				
					-- Obter as listas que foram populadas.
					dbms_sql.column_value(var_cur_w, 1, nr_seq_conta_w);
					dbms_sql.column_value(var_cur_w, 2, nr_seq_conta_proc_w);
					dbms_sql.column_value(var_cur_w, 3, nr_seq_selecao_w);
					
					-- Gravar na tabela de sele��o as contas selecionadas
					pls_tipos_cta_val_pck.gerencia_selecao(	nr_id_transacao_p, nr_seq_conta_w, nr_seq_conta_proc_w,
										nr_seq_selecao_w, 'S', nm_usuario_p,
										'F', dados_filtro_p);
					
					-- Quando n�mero de linhas que foram aplicadas no array for diferente do definido significa que esta foi a �ltima itera��o do loop e que todas as linhas foram
					-- passadas.
					exit when var_retorno_w != qt_cnt_w;
				end loop; -- Contas filtradas
				dbms_sql.close_cursor(var_cur_w);
			exception
				when others then
					dbms_sql.close_cursor(var_cur_w);
					-- Insere o log na tabela e aborta a opera��o
					pls_tipos_cta_val_pck.trata_erro_sql_dinamico(dados_regra_p,select_completo_w,nr_id_transacao_p,nm_usuario_p);
			end;
			
		end loop; -- C_filtro
	
		-- Atualiza o campo ie_valido da tabela PLS_SELECAO_RP_CTA para N aonde o ie_valido_temp continuar N
		pls_tipos_cta_val_pck.atualiza_sel_ie_valido(nr_id_transacao_p, dados_filtro_p, 'F');
	
	end loop; -- cs_tipo_filtro
end if;
end pls_cta_val_aplicar_fil_pres;
/