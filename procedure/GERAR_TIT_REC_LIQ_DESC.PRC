create or replace 
procedure GERAR_TIT_REC_LIQ_DESC (nr_titulo_p		number,
				  nr_seq_baixa_p	number,
				  nm_usuario_p		varchar2) is

-- N�o dar Commit!!!!!!!!!!!!

vl_desconto_w		number(15,2);
vl_perdas_w		number(15,2);
nr_seq_desc_w		number(10);
nr_seq_ret_item_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_cgc_w		varchar2(14);
cd_centro_custo_desc_w	number(8);
nr_seq_motivo_desc_w	number(10);

cursor c01 is
select	cd_cgc,
	cd_pessoa_fisica
from	convenio_ret_item_desc a
where	nr_seq_ret_item		= nr_seq_ret_item_w;

cursor c02 is
select	a.cd_pessoa_fisica,
	a.cd_cgc,
	a.cd_centro_custo,
	a.nr_seq_motivo_desc
from	titulo_Receber_liq_desc a
where	a.nr_titulo	= nr_titulo_p
and	a.nr_seq_liq	is null
and	a.nr_bordero	is null;

begin

select	max(nr_seq_ret_item)
into	nr_seq_ret_item_w
from	titulo_receber_liq
where	nr_titulo	= nr_titulo_p
and	nr_sequencia	= nr_seq_baixa_p;

select	nvl(max(vl_desconto),0),
	nvl(max(vl_perdas),0),
	max(nr_seq_motivo_desc),
	max(cd_centro_custo_desc)
into	vl_desconto_w,
	vl_perdas_w,
	nr_seq_motivo_desc_w,
	cd_centro_custo_desc_w
from	convenio_retorno_item
where	nr_sequencia	= nr_seq_ret_item_w;

if	(vl_desconto_w <> 0) or
	(vl_perdas_w <> 0) then

	open c01;
	loop
	fetch c01 into
		cd_cgc_w,
		cd_pessoa_fisica_w;
	exit when c01%notfound;

		select	titulo_receber_liq_desc_seq.nextval
		into	nr_seq_desc_w
		from	dual;

		insert	into titulo_receber_liq_desc
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			cd_cgc,
			nr_titulo,
			nr_seq_liq,
			nr_seq_motivo_desc,
			cd_centro_custo)
		values	(nr_seq_desc_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_pessoa_fisica_w,
			cd_cgc_w,
			nr_titulo_p,
			nr_seq_baixa_p,
			nr_seq_motivo_desc_w,
			cd_centro_custo_desc_w);
	end loop;
	close c01;

end if;

if	(cd_centro_custo_desc_w is null) and (nr_seq_ret_item_w is null) then

open c02;
loop
fetch c02 into
	cd_pessoa_fisica_w,
	cd_cgc_w,
	cd_centro_custo_desc_w,
	nr_seq_motivo_desc_w;
exit when c02%notfound;

	insert	into titulo_receber_liq_desc
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		cd_cgc,
		nr_titulo,
		nr_seq_liq,
		nr_seq_motivo_desc,
		cd_centro_custo)
	values	(titulo_receber_liq_desc_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_w,
		cd_cgc_w,
		nr_titulo_p,
		nr_seq_baixa_p,
		nr_seq_motivo_desc_w,
		cd_centro_custo_desc_w);

end loop;
close c02;

end if;

-- N�o dar Commit!!!!!!!!!!!!

end GERAR_TIT_REC_LIQ_DESC;
/