create or replace
procedure lab_administrar_exame_adep(nr_prescricao_p	number,
				     nr_seq_proced_p	varchar2,	
				     nm_usuario_p	varchar2) is 

	
nr_seq_prescr_w		number(10,0);
nr_seq_proced_w		varchar2(4000);				     
				    
Cursor C01 is
	select	nr_sequencia
	from	prescr_procedimento
	where	obter_se_contido(nr_sequencia ,(nr_seq_proced_w)) = 'S'
	and	nr_prescricao = nr_prescricao_p;

begin

select	substr(nr_seq_proced_p,1,length(nr_seq_proced_p))
into	nr_seq_proced_w
from	dual;

open C01;
loop
fetch C01 into	
	nr_seq_prescr_w;
exit when C01%notfound;
	begin
	ge_executar_adep(nr_prescricao_p,nr_seq_prescr_w,nm_usuario_p);
	end;
end loop;
close C01;

commit;

end lab_administrar_exame_adep;
/


