create or replace
procedure plt_obter_dieta_oral (
				cd_estabelecimento_p		number,
				cd_setor_usuario_p		number,
				cd_perfil_p			number,
				nm_usuario_p			varchar2,
				nr_atendimento_p		number,
				dt_inicial_horarios_p		date,
				dt_final_horarios_p		date,					
				dt_validade_limite_p		date,					
				ie_horarios_dietas_orais_p	varchar2,
				ie_prescr_usuario_p		varchar2,
				nr_seq_regra_p			number) is
	
nr_prescricao_w		number(14,0);
cd_refeicao_w		varchar2(15);
ds_refeicao_w		varchar2(255);
ie_lib_pend_rep_w	varchar2(1);
ie_liberado_w		varchar2(1);
nr_seq_wadep_w		number(10,0);
ie_status_item_w	varchar2(1);
ds_inter_prescr_w	varchar2(15);
nr_seq_dieta_w		number(6);
ie_copiar_w		varchar2(1);
ds_cor_titulo_w			varchar2(20);
					
cursor c01 is
select	c.cd_refeicao,
	x.ds_valor_dominio,
	substr(obter_desc_intervalo_prescr(d.cd_intervalo),1,15) ds_intervalo,
	d.nr_sequencia,
	substr(plt_obter_lib_pend_prescr(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1) ie_lib_pend_rep,
	decode(nvl(nvl(a.dt_liberacao,a.dt_liberacao_medico),a.dt_liberacao_farmacia),null,'N','S') ie_liberado
from	valor_dominio x,
	prescr_dieta d,
	prescr_dieta_hor c,
	prescr_medica a
where	x.vl_dominio = c.cd_refeicao
and	d.nr_prescricao = c.nr_prescricao
and	d.nr_prescricao = a.nr_prescricao
and	c.nr_prescricao = a.nr_prescricao
and	x.cd_dominio = 99
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	nvl(c.ie_situacao,'A') = 'A'
and	c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p
and	nvl(a.ie_adep,'S') = 'S'
and	(((a.dt_liberacao_medico is not null) and
	  (ie_prescr_usuario_p = 'N')) or
	 (a.nm_usuario_original = nm_usuario_p))
group by
	c.cd_refeicao,
	x.ds_valor_dominio,
	d.cd_intervalo,
	d.nr_sequencia,
	a.dt_liberacao_medico,
	a.dt_liberacao,
	a.dt_liberacao_farmacia;
	
cursor c02 is
select	to_char(d.cd_dieta),
	x.nm_dieta,
	substr(obter_status_hor_dieta_oral(c.dt_fim_horario,d.dt_suspensao),1,1),
	substr(obter_desc_intervalo_prescr(d.cd_intervalo),1,15) ds_intervalo,
	d.nr_sequencia,
	substr(plt_obter_lib_pend_prescr(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1) ie_lib_pend_rep,
	decode(nvl(nvl(a.dt_liberacao,a.dt_liberacao_medico),a.dt_liberacao_farmacia),null,'N','S') ie_liberado
from	dieta x,
	prescr_dieta d,
	prescr_dieta_hor c,
	prescr_medica a
where	x.cd_dieta = d.cd_dieta
and	d.nr_prescricao = c.nr_prescricao
and	d.nr_prescricao	= a.nr_prescricao
and	c.nr_prescricao = a.nr_prescricao
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(a.ie_adep,'S') = 'S'
and	c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p
and	(((a.dt_liberacao_medico is not null) and
	  (ie_prescr_usuario_p = 'N')) or
	 (a.nm_usuario_original = nm_usuario_p))
group by
	d.cd_dieta,
	x.nm_dieta,
	c.dt_fim_horario,
	d.dt_suspensao,
	d.cd_intervalo,
	d.nr_sequencia,
	a.dt_liberacao_medico,
	a.dt_liberacao,
	a.dt_liberacao_farmacia
union
select	to_char(d.cd_dieta),
	x.nm_dieta,
	substr(obter_status_hor_dieta_oral(null,d.dt_suspensao),1,1),
	substr(obter_desc_intervalo_prescr(d.cd_intervalo),1,15) ds_intervalo,
	d.nr_sequencia,
	substr(plt_obter_lib_pend_prescr(a.dt_liberacao_medico,a.dt_liberacao,a.dt_liberacao_farmacia),1,1) ie_lib_pend_rep,
	decode(nvl(nvl(a.dt_liberacao,a.dt_liberacao_medico),a.dt_liberacao_farmacia),null,'N','S') ie_liberado
from	dieta x,
	prescr_dieta d,	
	prescr_medica a
where	x.cd_dieta = d.cd_dieta
and	d.nr_prescricao	= a.nr_prescricao
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
--and	((a.dt_liberacao_medico is not null) and (a.dt_liberacao is not null))
and	nvl(a.ie_adep,'S') = 'S'
and	not exists (
		select	1
		from	prescr_dieta_hor c
		where	c.nr_prescricao = d.nr_prescricao
		and	c.nr_prescricao = a.nr_prescricao
		and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S')
and	(((a.dt_liberacao_medico is not null) and
	  (ie_prescr_usuario_p = 'N')) or
	 (a.nm_usuario_original = nm_usuario_p))
group by
	d.cd_dieta,
	x.nm_dieta,	
	d.dt_suspensao,
	d.cd_intervalo,
	d.nr_sequencia,
	a.dt_liberacao_medico,
	a.dt_liberacao,
	a.dt_liberacao_farmacia;
		
begin
ie_copiar_w 	:= plt_obter_se_item_marcado('D', nr_seq_regra_p);
ds_cor_titulo_w	:= plt_obter_se_item_marcado('D', nr_seq_regra_p);

if	(ie_horarios_dietas_orais_p <> 'N') then
	begin
	open c01;
	loop
	fetch c01 into	cd_refeicao_w,
			ds_refeicao_w,
			ds_inter_prescr_w,
			nr_seq_dieta_w,
			ie_lib_pend_rep_w,
			ie_liberado_w;
	exit when c01%notfound;
		begin
		select	w_rep_t_seq.nextval
		into	nr_seq_wadep_w
		from	dual;
		
		insert into w_rep_t (
			nr_sequencia,
			nm_usuario,
			nr_seq_item,
			ie_tipo_item,
			cd_item,
			ds_item,
			ie_acm_sn,
			ie_diferenciado,
			nr_seq_proc_interno,
			nr_agrupamento,
			ds_interv_prescr,
			ie_pendente_liberacao,
			ie_liberado,
			ie_copiar
			)
		values (
			nr_seq_wadep_w,
			nm_usuario_p,
			nr_seq_dieta_w,
			'D',
			cd_refeicao_w,
			ds_refeicao_w,
			'N',
			'N',
			0,
			0,
			ds_inter_prescr_w,
			ie_lib_pend_rep_w,
			ie_liberado_w,
			ie_copiar_w
			);
		end;
	end loop;
	close c01;
	end;
else
	begin
	open c02;
	loop
	fetch c02 into	cd_refeicao_w,
			ds_refeicao_w,
			ie_status_item_w,
			ds_inter_prescr_w,
			nr_seq_dieta_w,
			ie_lib_pend_rep_w,
			ie_liberado_w;
	exit when c02%notfound;
		begin
		select	w_rep_t_seq.nextval
		into	nr_seq_wadep_w
		from	dual;
		
		insert into w_rep_t (
			nr_sequencia,
			nm_usuario,
			nr_seq_item,
			ie_tipo_item,
			cd_item,
			ds_item,
			ie_acm_sn,
			ie_diferenciado,
			nr_seq_proc_interno,
			nr_agrupamento,
			ie_status_item,
			ds_interv_prescr,
			ie_pendente_liberacao,
			ie_liberado,
			ie_copiar,
			ds_cor_titulo)
		values (
			nr_seq_wadep_w,
			nm_usuario_p,
			nr_seq_dieta_w,
			'D',
			cd_refeicao_w,
			ds_refeicao_w,
			'N',
			'N',
			0,
			0,
			ie_status_item_w,
			ds_inter_prescr_w,
			ie_lib_pend_rep_w,
			ie_liberado_w,
			ie_copiar_w,
			ds_cor_titulo_w);
		end;
	end loop;
	close c02;
	end;
end if;

end plt_obter_dieta_oral;
/