create or replace
procedure Copiar_Cardapio_Nutricao
		(nr_seq_cardapio_p		number,
		nr_seq_opcao_p			number,
		ie_semana_p			number,
		ie_dia_semana_p			number,
		nm_usuario_p			Varchar2) is 

nr_sequencia_w		number(10,0);	
nr_seq_cardapio_w	number(10,0);	
nr_seq_comp_w		number(10,0);
nr_seq_receita_w	number(10,0);
dt_vigencia_inicial_w	date;
dt_ini_destino_w		date;
dt_fim_destino_w		date;	
qt_refeicao_w		number(15);
ie_dia_semana_w		number(3);
		
Cursor  C01 is
	select	nr_seq_comp,
		nr_seq_receita,
		qt_refeicao
	from	nut_cardapio
	where	nr_seq_card_dia		= nr_seq_cardapio_p
	order by nr_seq_comp;	

Cursor	c02 is
	select	to_number(vl_dominio)
	from	valor_dominio
	where	cd_dominio = 35
	and	((vl_dominio = ie_dia_semana_p) or (ie_dia_semana_p = 9))
	and	vl_dominio <> 9
	union
	select	ie_dia_semana
	from	nut_cardapio_dia
	where	nr_sequencia		= nr_seq_cardapio_p
	and	ie_dia_semana_p is null;
		
begin

select	max(dt_vigencia_inicial)
into	dt_vigencia_inicial_w
from	nut_cardapio_dia
where	nr_sequencia = nr_seq_cardapio_p;

obter_dia_ini_fim_mes_vig(dt_vigencia_inicial_w, dt_ini_destino_w, dt_fim_destino_w);

open C02;
loop
fetch C02 into	
	ie_dia_semana_w;
exit when C02%notfound;
	begin
	
	select	nut_cardapio_dia_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert	into nut_cardapio_dia
		(nr_sequencia,           
		cd_estabelecimento,     
		dt_atualizacao,         
		nm_usuario,             
		nr_seq_servico,         
		dt_cardapio,            
		qt_pessoa_atend,        
		dt_atualizacao_nrec,    
		nm_usuario_nrec,        
		nr_seq_opcao,           
		ie_dia_semana,          
		ie_semana,              
		dt_vigencia_inicial,
		dt_vigencia_final,
		CD_DIETA,
		NR_SEQ_LOCAL,
		NR_SEQ_GRUPO_PRODUCAO)      
	(select	nr_sequencia_w,
		cd_estabelecimento,
		sysdate,
		nm_usuario_p,
		nr_seq_servico,
		dt_cardapio,
		qt_pessoa_atend,
		sysdate,
		nm_usuario_p,
		nvl(nr_seq_opcao_p, nr_seq_opcao),
		ie_dia_semana_w,
		nvl(ie_semana_p, ie_semana),
		dt_ini_destino_w,
		dt_fim_destino_w,
		CD_DIETA,
		NR_SEQ_LOCAL,
		NR_SEQ_GRUPO_PRODUCAO
	from	nut_cardapio_dia
	where	nr_sequencia		= nr_seq_cardapio_p);

	open C01;
	loop
	fetch C01 into	
		nr_seq_comp_w,
		nr_seq_receita_w,
		qt_refeicao_w;
	exit when C01%notfound;
		begin
		
		select	nut_cardapio_seq.nextval
		into	nr_seq_cardapio_w
		from	dual;

		insert 	into nut_cardapio
			(nr_sequencia,
			nr_seq_card_dia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_comp,
			nr_seq_receita,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			qt_refeicao)
		values
			(nr_seq_cardapio_w,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			nr_seq_comp_w,
			nr_seq_receita_w,
			sysdate,
			nm_usuario_p,
			qt_refeicao_w);
			
		end;
	end loop;
	close C01;
	end;
end loop;
close C02;

commit;

end Copiar_Cardapio_Nutricao;
/
