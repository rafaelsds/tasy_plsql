create or replace
procedure pls_altera_status_protocolo(	nr_seq_protocolo_p	number,
					ie_tipo_liberacao_p	varchar2,
					ie_commit_p		varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is
/* IE_TIPO_LIBERACAO_P
	F - Fechamento do protocolo
	C - Fechamento da conta
	L - Libera��o do item
	P - Fechamento do protocolo (Liberar para pagamneto) OS 787434
*/

vl_total_protocolo_w		Number(15,2);
qt_conta_w			Number(10);
qt_conta_liberada_w		Number(10);
qt_conta_consistida_w		Number(10);
dt_mesano_referencia_w		Date;
dt_mesano_referencia_dd_w	date;
dt_mesano_recebimento_w		Date;
ie_mes_fechado_w		Varchar2(1);
ie_tipo_data_pagamento_w	Varchar2(10);
dt_competencia_pgto_w		Date;
nr_seq_fatura_w			number(10);
ie_tipo_protocolo_w		varchar2(3);
ie_importando_protocolo_w	varchar2(1)	:= 'N';
ie_forma_imp_w			varchar2(3);
dt_integracao_w			date;
dt_fim_analise_w		date;
vl_pagamento_w			Number(15,2);
vl_liberado_w			Number(15,2);
ie_atualizar_grupo_ans_w	Varchar2(1);
qt_vago_w			number(10)	:= 0;
qt_contas_canceladas_w		number(10);
ie_status_protocolo_w		pls_protocolo_conta.ie_status%type;
nr_seq_lote_conta_w		pls_protocolo_conta.nr_seq_lote_conta%type;
ds_motivo_canc_w		pls_protocolo_conta.ds_motivo_canc%type;
qt_protocolos_lote_w		number(10);
qt_tot_conta_w			pls_integer;
ie_atualiza_contab_lib_w	varchar2(2);

begin

select	ie_forma_imp,
	dt_integracao,
	dt_fim_analise,
	ie_tipo_protocolo,
	nr_seq_lote_conta
into	ie_forma_imp_w,
	dt_integracao_w,
	dt_fim_analise_w,
	ie_tipo_protocolo_w,
	nr_seq_lote_conta_w
from	pls_protocolo_conta a
where	a.nr_sequencia	= nr_seq_protocolo_p;

/* Francisco - 21/05/2012 - Na importa��o, s� atualizar status no final */
/*if	(ie_forma_imp_w = 'P') and
	(dt_integracao_w is not null) and
	(dt_fim_analise_w is null) then*/
	ie_importando_protocolo_w	:= 'N';
--end if;

if	(nvl(nr_seq_protocolo_p,0) <> 0) then
	select	count(1)
	into	qt_conta_consistida_W
	from	pls_conta
	where	nr_seq_protocolo	= nr_seq_protocolo_p
	and	ie_status		in ('P','L','A');

	select	count(1)
	into	qt_conta_liberada_W
	from	pls_conta
	where	nr_seq_protocolo	= nr_seq_protocolo_p
	and	ie_status		in ('F','C');

	select	count(1)
	into	qt_conta_W
	from	pls_conta
	where	nr_seq_protocolo	= nr_seq_protocolo_p;

	if	(qt_conta_W = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(174118);/*'Protocolo sem contas. Verifique!*/
	end if;

	if	(qt_conta_liberada_w = qt_conta_W) then
		select	sum(vl_total)
		into	vl_total_protocolo_w
		from	pls_conta
		where	nr_seq_protocolo	= nr_seq_protocolo_p
		and	vl_total is not null;
		
		begin
		select	nvl(ie_atualizar_grupo_ans,'S')
		into	ie_atualizar_grupo_ans_w
		from	table(pls_parametros_pck.f_retorna_param(cd_estabelecimento_p));
		exception
		when others then
			ie_atualizar_grupo_ans_w	:= 'S';
		end;
		
		if	(ie_atualizar_grupo_ans_w = 'S') then
			--alterado na rotina para verificar o grupo ans antes de verificar o valor.
			pls_atualizar_grupo_ans_conta(nr_seq_protocolo_p, 'N', null, null,'N',nm_usuario_p,cd_estabelecimento_p, null);
		end if;
		
		if	(ie_tipo_liberacao_p in ('F','P')) then			
			select	trunc(dt_mes_competencia,'month'),
				trunc(dt_recebimento,'dd'),
				dt_mes_competencia
			into	dt_mesano_referencia_w,
				dt_mesano_recebimento_w,
				dt_mesano_referencia_dd_w
			from	pls_protocolo_conta
			where	nr_sequencia	= nr_seq_protocolo_p;
			
			if	(ie_tipo_liberacao_p	= 'F') then
				ie_mes_fechado_w :=	pls_obter_se_mes_fechado(dt_mesano_referencia_w,'T', cd_estabelecimento_p);

				if 	(ie_mes_fechado_w = 'S') then
					wheb_mensagem_pck.exibir_mensagem_abort(174126);--'N�o � poss�vel realizar esta opera��o pois o m�s de compet�ncia ou a contabilidade do m�s est� fechada!
				end if;
			end if;
			
			select	nvl(sum(a.vl_lib_original),0)
			into	vl_pagamento_w
			from	pls_conta_medica_resumo	a
			where	a.nr_seq_conta in	( select 	b.nr_sequencia
							  from		pls_conta b
							  where		b.nr_seq_protocolo = nr_seq_protocolo_p)
			and	a.ie_tipo_item 	<> 'I'  --retira itens de interc�mbio da soma
			and	a.vl_lib_original > 0
			and	a.ie_situacao = 'A';
			
			if	(nvl(vl_total_protocolo_w,0) <> nvl(vl_pagamento_w,0)) and
				(ie_tipo_protocolo_w 	= 'C') then
				wheb_mensagem_pck.exibir_mensagem_abort(225847);--N�o � poss�vel liberar o protocolo para pagamento, pois existe dive
			end if;
			pls_atualizar_codificacao_pck.pls_atualizar_codificacao(dt_mesano_referencia_w);
			
			if	(vl_total_protocolo_w = 0) then
				update	pls_protocolo_conta
				set	ie_status		= '4',
					dt_lib_pagamento	= sysdate, -- OS 1289527 - Como protocolos 'Encerrado sem pagamento' tamb�m devem entrar em pagamento ent�o esta data deve ser preenchida.
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p
				where	nr_sequencia		= nr_seq_protocolo_p
				and	ie_status		not in ('7','6');
				
				update	pls_conta_medica_resumo	
				set	ie_status_protocolo 	= '4',
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p
				where	nr_seq_protocolo	= nr_seq_protocolo_p
				and	ie_situacao		= 'A';
				
				if	(ie_tipo_protocolo_w = 'R') then
					pls_gerar_comunic_reemb(1,'O protocolo de reembolso ' || nr_seq_protocolo_p || ' est� encerrado sem pagamento.', nr_seq_protocolo_p, nm_usuario_p, cd_estabelecimento_p);
					
					-- jtonon - OS 1108463 - Envio de alerta SMS/Email para o benefici�rio
					pls_gerar_alerta_rembolso_lib(6, nr_seq_protocolo_p, '4', nm_usuario_p);
				end if;
				
			else
				update	pls_protocolo_conta
				set	ie_status		= '3',
					dt_lib_pagamento	= sysdate,
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p
				where	nr_sequencia		= nr_seq_protocolo_p
				and	ie_status		not in ('7','6');
				
				if	(ie_tipo_protocolo_w = 'R') then
					pls_gerar_comunic_reemb(1,'O protocolo de reembolso ' || nr_seq_protocolo_p || ' est� liberado para pagamento.', nr_seq_protocolo_p, nm_usuario_p, cd_estabelecimento_p);
					
					-- jtonon - OS 1108463 - Envio de alerta SMS/Email para o benefici�rio
					pls_gerar_alerta_rembolso_lib(6, nr_seq_protocolo_p, '3', nm_usuario_p);
				end if;
				
				select	nvl(ie_atualiza_contab_lib,'N')
				into	ie_atualiza_contab_lib_w
				from	pls_parametros
				where	cd_estabelecimento = cd_estabelecimento_p;
				
				if (ie_tipo_liberacao_p = 'F') or (ie_atualiza_contab_lib_w = 'S') then  /*OS 787434  a libera��o para pagamento n�o altera a contabiliza��o pois a provis�o ocorre na data de recebimento*/
					pls_atualizar_contas_protocolo(nr_seq_protocolo_p, nm_usuario_p, cd_estabelecimento_p,'N'); /* Atualizar contas cont�beis */
				--ctb_pls_atualizar_conta(nr_seq_protocolo_p, nm_usuario_p, cd_estabelecimento_p);
				end if;
				
				
				if	(ie_tipo_protocolo_w = 'I') then
					ctb_pls_atualizar_desp_interc(nr_seq_protocolo_p, null, null, null, null, nm_usuario_p, cd_estabelecimento_p, qt_vago_w);
				end if;
				
				/* Verificar se o pagamento � gerado pela data M�S COMPET�NCIA, LIBERA��O PAGAMENTO ou RECEBIMENTO do protocolo */
				select	nvl(max(ie_tipo_data_pagamento),'M')
				into	ie_tipo_data_pagamento_w
				from	pls_parametro_pagamento
				where	cd_estabelecimento	= cd_estabelecimento_p;
				
				if	(ie_tipo_data_pagamento_w	= 'M') then
					dt_competencia_pgto_w		:= dt_mesano_referencia_dd_w;
				elsif	(ie_tipo_data_pagamento_w	= 'P') then
					dt_competencia_pgto_w		:= sysdate;
				elsif	(ie_tipo_data_pagamento_w	= 'R') then
					dt_competencia_pgto_w		:= dt_mesano_recebimento_w;
				end if;
				
				/* Felipe - 15/06/2011 - Se a forma de pagamento for por protocolo ent�o atualiza a data de pagamento */
				update	pls_conta_medica_resumo	
				set	ie_status_protocolo 	= '3',
					dt_competencia_pgto	= dt_competencia_pgto_w,
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p,
					ie_tipo_data_pagamento	= ie_tipo_data_pagamento_w
				where	nr_seq_protocolo	= nr_seq_protocolo_p
				and	nr_seq_lote_pgto is null
				and	ie_situacao		= 'A';
			end if;
			
			/* FELIPE - 15/02/2011 - OS 283335 - Utilizar a rotina PLS_GERAR_SIP_CONTA que � chamada na PLS_CONSISTIR_CONTA 
			pls_gerar_protocolo_sip(nr_seq_protocolo_p, nm_usuario_p, cd_estabelecimento_p);*/
			if	(ie_tipo_protocolo_w <> 'R') then
				pls_gerar_comunic_conta(2,'O protocolo ' || nr_seq_protocolo_p || ' est� liberado.',
						nm_usuario_p,cd_estabelecimento_p);
			end if;
			
			--Protocolo liberado para pagamento
			pls_inclui_prot_conta_hist(nr_seq_protocolo_p, wheb_mensagem_pck.get_texto(315921), nm_usuario_p, '11');

		elsif	(ie_tipo_liberacao_p in ('L','C')) then		
		
			select	count(1)
			into	qt_contas_canceladas_w
			from	pls_conta
			where	nr_seq_protocolo	= nr_seq_protocolo_p
			and	ie_status		= 'C';
			
			select	count(1)
			into	qt_tot_conta_w
			from	pls_conta
			where	nr_seq_protocolo	= nr_seq_protocolo_p;
			
			ie_status_protocolo_w	:= '5';
			ds_motivo_canc_w	:= '';
			
			--aaschlote 22/04/2015 OS 859396
			if	(qt_tot_conta_w = qt_contas_canceladas_w) and
				(qt_contas_canceladas_w	> 0) then
				ds_motivo_canc_w	:= 'Cancelamento efeutado pelo sistema, pois todas as contas no protocolo foram canceladas';
				ie_status_protocolo_w	:= '4';
			end if;	
		
			update	pls_protocolo_conta
			set	ie_status		= ie_status_protocolo_w,
				dt_fechamento_contas	= sysdate,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				ds_motivo_canc		= ds_motivo_canc_w
			where	nr_sequencia		= nr_seq_protocolo_p
			and	ie_status		not in ('7','6');
			
			--aaschlote 22/04/2015 OS 859396 - Cancelar o protocolo caso tenha apenas 1 conta como cancelada dentro dela
			if	(nr_seq_lote_conta_w is not null) and
				(ie_status_protocolo_w = '4') then
				select	count(1)
				into	qt_protocolos_lote_w
				from	pls_protocolo_conta
				where	nr_seq_lote_conta	= nr_seq_lote_conta_w;
				
				if	(qt_protocolos_lote_w = 1) then
					update	pls_lote_protocolo_conta
					set	ie_status 		= 'C',
						ds_motivo_cancelamento	= ds_motivo_canc_w,
						dt_atualizacao		= sysdate,
						nm_usuario		= nm_usuario_p
					where	nr_sequencia 		= nr_seq_lote_conta_w;
				end if;
			end if;
			
			select	max(nr_sequencia)
			into	nr_seq_fatura_w
			from	ptu_fatura
			where	nr_seq_protocolo	= nr_seq_protocolo_p
			and	ie_status	not in	('CA','R');
			
			if	(nr_seq_fatura_w is not null) then
				ptu_atualizar_status_fatura(nr_seq_fatura_w, 'AF', null, nm_usuario_p);
			end if;
			
			update	pls_conta_medica_resumo	
			set	ie_status_protocolo 	= ie_status_protocolo_w,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_seq_protocolo	= nr_seq_protocolo_p
			and	ie_situacao		= 'A';
			--Conforme tratado com Leandro na libera��o do protocolo n�o � necess�ria esta tratativa devido a estas contas n�o serem contabilizadas
			--pls_atualizar_contas_protocolo(nr_seq_protocolo_p, nm_usuario_p, cd_estabelecimento_p,'N'); /* Atualizar contas cont�beis */
			
		end if;
	elsif	(qt_conta_consistida_w > 0) then
		update	pls_protocolo_conta
		set	ie_status		= '2',
			dt_fechamento_contas	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_protocolo_p
		and	ie_status		not in ('7','6')
		and   	((ie_situacao	!= 'I') or (ie_protocolo_compl	is null or ie_protocolo_compl != 'S'));
	elsif	(qt_conta_liberada_w = 0) then
		update	pls_protocolo_conta
		set	ie_status		= '1',
			dt_fechamento_contas	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_protocolo_p
		and	ie_status		not in ('7','6');
	end if;
end if;

/* William - OS 405855 - Adicionado ie_commit_p */
if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end pls_altera_status_protocolo;
/