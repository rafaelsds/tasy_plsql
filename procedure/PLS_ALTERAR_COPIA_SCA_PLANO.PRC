create or replace
procedure pls_alterar_copia_sca_plano
		(	nr_seq_proposta_benef_p		Number,
			nm_usuario_p			Varchar2) is 
			
ie_copiar_sca_plano_w	varchar2(10);
ds_alteracao_nascido_w	varchar2(100);

begin

select	nvl(ie_copiar_sca_plano,'S')
into	ie_copiar_sca_plano_w
from	pls_proposta_beneficiario	
where	nr_sequencia		= nr_seq_proposta_benef_p;

update	pls_proposta_beneficiario
set	ie_copiar_sca_plano	= decode(ie_copiar_sca_plano_w,'S','N','S'),
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_proposta_benef_p;

commit;

end pls_alterar_copia_sca_plano;
/