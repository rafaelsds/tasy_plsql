create or replace
procedure Gerar_indicador_analistas(dt_referencia_p	date) is 

pr_os_analista_w		number(15,4);
pr_os_def_analista_w		number(15,4);
qt_ordens_analista_w		number(15,4);
qt_ordens_def_analista_w	number(15,4);
qt_ordens_grupo_w		number(15,4);
nm_usuario_lider_w		varchar2(50);
nr_seq_grupo_w			number(10);
nr_seq_gerencia_w		number(10);
NR_SEQ_GERENCIAMENTO_W		number(10);
			
cursor c01 is
select	b.nm_usuario_grupo,
	a.nr_sequencia,
	a.nr_seq_gerencia
from	grupo_desenvolvimento a,
	usuario_grupo_des b
where	a.nr_Sequencia = b.nr_seq_grupo
and	b.ie_funcao_usuario = 'S'
and	ie_gerencia = 'N'
and	ie_situacao = 'A'
and	b.nm_usuario_grupo is not null;

cursor c02 is
select	b.nm_usuario_grupo,
	a.nr_sequencia,
	a.nr_seq_gerencia
from	grupo_desenvolvimento a,
	usuario_grupo_des b
where	a.nr_Sequencia = b.nr_seq_grupo
and	b.ie_funcao_usuario = 'M'
and	a.ie_gerencia = 'N'
and	a.ie_situacao = 'A'
and	b.nm_usuario_grupo is not null;

cursor c03 is
select	b.nm_usuario_grupo,
	a.nr_sequencia,
	a.nr_seq_gerencia
from	grupo_desenvolvimento a,
	usuario_grupo_des b
where	a.nr_Sequencia = b.nr_seq_grupo
and	b.ie_funcao_usuario = 'N'
and	a.ie_gerencia = 'N'
and	a.ie_situacao = 'A'
and	b.nm_usuario_grupo is not null;

begin

delete	gerenciamento_fila_desenv
where	trunc(dt_referencia) = trunc(dt_referencia_p)
and	nm_usuario_nrec		= 'Tasy'
and	ie_tipo_dado		= 'A';
commit;

open C01;
loop
fetch C01 into	
	nm_usuario_lider_w,
	nr_seq_grupo_w,
	nr_seq_gerencia_w;
exit when C01%notfound;

	qt_ordens_def_analista_w	:= 0;
	pr_os_def_analista_w		:= 0;
	select	count(*),
		sum(decode(a.ie_classificacao,'E',1,0)) qt_os_defeito_analista
	into	qt_ordens_analista_w,
		qt_ordens_def_analista_w
	from	man_ordem_serv_programar_v a
	where	a.nm_usuario_prev = nm_usuario_lider_w
	and   	exists (select	1
	               	from  	man_ordem_servico_exec	x
					where	x.nr_seq_ordem	= a.nr_sequencia
					and		x.nm_usuario_exec = nm_usuario_lider_w
					and		x.dt_fim_execucao is null);
					
	select	sum(qt_ordem)
	into	qt_ordens_grupo_w
	from	(
	select	b.ds_grupo ds_grupo_des,
			a.nr_seq_grupo_des,
			count(*) qt_ordem
	from	man_ordem_servico  a,
			grupo_desenvolvimento b,
			gerencia_wheb c
	where	c.nr_sequencia	= b.nr_seq_gerencia
	and		b.nr_sequencia	= a.nr_seq_grupo_des
	and		substr(obter_se_os_pend_cliente(a.nr_sequencia),1,1) = 'N'
	and		substr(Obter_Se_OS_Desenv(a.nr_sequencia),1,1) = 'S'
	and		a.ie_status_ordem	<> 3
	and		c.nr_sequencia		<> 5
	and		c.nr_sequencia 		= nr_seq_gerencia_w
	and		a.nr_seq_grupo_des	= nr_seq_grupo_w
	group by b.ds_grupo, a.nr_seq_grupo_des);
	
	pr_os_analista_w	:= 	dividir((qt_ordens_analista_w * 100), qt_ordens_grupo_w);
	pr_os_def_analista_w	:= 	dividir((qt_ordens_def_analista_w * 100), qt_ordens_grupo_w);
	
	select	gerenciamento_fila_desenv_seq.nextval
	into	nr_seq_gerenciamento_w
	from	dual;
	
	insert into gerenciamento_fila_desenv
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_usuario_fila,
			ie_tipo_dado,
			dt_referencia,
			qt_ordens_grupo,
			qt_ordens_analista,
			pr_os_analista,
			nr_seq_gerencia,
			pr_os_def_analista,
			qt_ordens_def_analista)
	values	(nr_seq_gerenciamento_w,
			sysdate,
			'Tasy',
			sysdate,
			'Tasy',
			nm_usuario_lider_w,
			'A',
			trunc(dt_referencia_p),
			qt_ordens_grupo_w,
			qt_ordens_analista_w,
			pr_os_analista_w,
			nr_seq_gerencia_w,
			pr_os_def_analista_w,
			qt_ordens_def_analista_w);
	commit;
	
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nm_usuario_lider_w,
	nr_seq_grupo_w,
	nr_seq_gerencia_w;
exit when C02%notfound;
	begin
	qt_ordens_def_analista_w	:= 0;
	pr_os_def_analista_w		:= 0;
	
	select	count(*),
		sum(decode(a.ie_classificacao,'E',1,0)) qt_os_defeito_analista
	into	qt_ordens_analista_w,
		qt_ordens_def_analista_w
	from	man_ordem_serv_programar_v a
	where	a.nm_usuario_prev = nm_usuario_lider_w
	and   	exists (select	1
	               	from  	man_ordem_servico_exec	x
			where	x.nr_seq_ordem	= a.nr_sequencia
			and	x.nm_usuario_exec = nm_usuario_lider_w
			and	x.dt_fim_execucao is null);
					
	select	sum(qt_ordem)
	into	qt_ordens_grupo_w
	from	(
	select	b.ds_grupo ds_grupo_des,
			a.nr_seq_grupo_des,
			count(*) qt_ordem
	from	man_ordem_servico  a,
			grupo_desenvolvimento b,
			gerencia_wheb c
	where	c.nr_sequencia	= b.nr_seq_gerencia
	and		b.nr_sequencia	= a.nr_seq_grupo_des
	and		substr(obter_se_os_pend_cliente(a.nr_sequencia),1,1) = 'N'
	and		substr(Obter_Se_OS_Desenv(a.nr_sequencia),1,1) = 'S'
	and		a.ie_status_ordem	<> 3
	and		c.nr_sequencia		<> 5
	and		c.nr_sequencia 		= nr_seq_gerencia_w
	and		a.nr_seq_grupo_des	= nr_seq_grupo_w
	group by b.ds_grupo, a.nr_seq_grupo_des);
	
	pr_os_analista_w	:= 	dividir((qt_ordens_analista_w * 100), qt_ordens_grupo_w);
	pr_os_def_analista_w	:= 	dividir((qt_ordens_def_analista_w * 100), qt_ordens_grupo_w);
	
	
	select	gerenciamento_fila_desenv_seq.nextval
	into	nr_seq_gerenciamento_w
	from	dual;
	
	insert into gerenciamento_fila_desenv
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_usuario_fila,
			ie_tipo_dado,
			dt_referencia,
			qt_ordens_grupo,
			qt_ordens_analista,
			pr_os_analista,
			nr_seq_gerencia,
			pr_os_def_analista,
			qt_ordens_def_analista)
	values	(nr_seq_gerenciamento_w,
			sysdate,
			'Tasy',
			sysdate,
			'Tasy',
			nm_usuario_lider_w,
			'A',
			trunc(dt_referencia_p),
			qt_ordens_grupo_w,
			qt_ordens_analista_w,
			pr_os_analista_w,
			nr_seq_gerencia_w,
			pr_os_def_analista_w,
			qt_ordens_def_analista_w);
	commit;
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nm_usuario_lider_w,
	nr_seq_grupo_w,
	nr_seq_gerencia_w;
exit when C03%notfound;

	qt_ordens_def_analista_w	:= 0;
	pr_os_def_analista_w		:= 0;

	select	count(*),
		sum(decode(a.ie_classificacao,'E',1,0)) qt_os_defeito_analista
	into	qt_ordens_analista_w,
		qt_ordens_def_analista_w
	from	man_ordem_serv_programar_v a
	where	a.nm_usuario_prev = nm_usuario_lider_w
	and   	exists (select	1
	               	from  	man_ordem_servico_exec	x
					where	x.nr_seq_ordem	= a.nr_sequencia
					and		x.nm_usuario_exec = nm_usuario_lider_w
					and		x.dt_fim_execucao is null);
					
	select	sum(qt_ordem)
	into	qt_ordens_grupo_w
	from	(
	select	b.ds_grupo ds_grupo_des,
			a.nr_seq_grupo_des,
			count(*) qt_ordem
	from	man_ordem_servico  a,
			grupo_desenvolvimento b,
			gerencia_wheb c
	where	c.nr_sequencia	= b.nr_seq_gerencia
	and		b.nr_sequencia	= a.nr_seq_grupo_des
	and		substr(obter_se_os_pend_cliente(a.nr_sequencia),1,1) = 'N'
	and		substr(Obter_Se_OS_Desenv(a.nr_sequencia),1,1) = 'S'
	and		a.ie_status_ordem	<> 3
	and		c.nr_sequencia		<> 5
	and		c.nr_sequencia 		= nr_seq_gerencia_w
	and		a.nr_seq_grupo_des	= nr_seq_grupo_w
	group by b.ds_grupo, a.nr_seq_grupo_des);
	
	pr_os_analista_w	:= 	dividir((qt_ordens_analista_w * 100), qt_ordens_grupo_w);
	pr_os_def_analista_w	:= 	dividir((qt_ordens_def_analista_w * 100), qt_ordens_grupo_w);
	
	select	gerenciamento_fila_desenv_seq.nextval
	into	nr_seq_gerenciamento_w
	from	dual;
	
	insert into gerenciamento_fila_desenv
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_usuario_fila,
			ie_tipo_dado,
			dt_referencia,
			qt_ordens_grupo,
			qt_ordens_analista,
			pr_os_analista,
			nr_seq_gerencia,
			pr_os_def_analista,
			qt_ordens_def_analista)
	values	(nr_seq_gerenciamento_w,
			sysdate,
			'Tasy',
			sysdate,
			'Tasy',
			nm_usuario_lider_w,
			'A',
			trunc(dt_referencia_p),
			qt_ordens_grupo_w,
			qt_ordens_analista_w,
			pr_os_analista_w,
			nr_seq_gerencia_w,
			pr_os_def_analista_w,
			qt_ordens_def_analista_w);
	commit;
	
end loop;
close C03;

commit;

end Gerar_indicador_analistas;
/
