create or replace
procedure san_gerar_lote_exames(
			nr_sequencia_p		number,
			dt_inicio_p		date,
			dt_fim_p			date,
			nm_usuario_p		varchar2) is 

nr_seq_exame_lote_w		number(10);
nr_seq_exame_w  			number(10);	
			
cursor c01 is
select	a.nr_seq_exame_lote,
	a.nr_seq_exame  
from	san_exame_realizado a
where	a.dt_realizado	between dt_inicio_p and dt_fim_p
and	(san_obter_destino_exame(a.nr_seq_exame,3) = 'S' or san_obter_destino_exame(a.nr_seq_exame,0) = 'S')
and	not exists (select 1
		from	san_lote_hemoterapia_item x
		where	x.nr_seq_exame = a.nr_seq_exame
		and	x.nr_seq_exame_lote = a.nr_seq_exame_lote);
begin

update	san_lote_hemoterapia
set	dt_geracao	=	sysdate
where	nr_sequencia	=	nr_sequencia_p;

open C01;
loop
fetch C01 into	
	nr_seq_exame_lote_w,
	nr_seq_exame_w;
exit when C01%notfound;
	begin
	insert into san_lote_hemoterapia_item(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_lote,
				nr_seq_exame,
				nr_seq_exame_lote)
			values(
				san_lote_hemoterapia_item_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_sequencia_p,
				nr_seq_exame_w,
				nr_seq_exame_lote_w);
	end;
end loop;
close C01;

commit;

end san_gerar_lote_exames;
/