create or replace
procedure ctb_duplicar_orc_anual(	cd_empresa_p		empresa.cd_empresa%type,
					cd_estab_p		estabelecimento.cd_estabelecimento%type,
					cd_centro_custo_orig_p	centro_custo.cd_centro_custo%type,
					cd_centro_custo_dest_p	centro_custo.cd_centro_custo%type,
					dt_ano_orig_p		date,
					dt_ano_dest_p		date,
					ie_orcado_real_p	varchar2,
					ie_sobrepor_p		varchar2,
					nm_usuario_p		varchar2) is

cd_centro_custo_dest_w		centro_custo.cd_centro_custo%type	:= cd_centro_custo_dest_p;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
dt_inicial_orig_w		date;
dt_final_orig_w			date;
dt_liberacao_w			date;
dt_referencia_w			date;
ie_vigente_w			varchar2(1);
ie_situacao_w			varchar2(1);
nr_seq_mes_ref_w		ctb_mes_ref.nr_sequencia%type;
nr_seq_orcamento_w		ctb_orcamento.nr_sequencia%type;
vl_orcado_w			ctb_orcamento.vl_orcado%type;

cursor c_orcamento is
select	a.cd_estabelecimento,
	a.cd_conta_contabil,
	a.cd_centro_custo,
	a.vl_orcado,
	a.vl_realizado,
	a.nr_seq_mes_ref,
	a.ds_observacao,
	c.dt_referencia
from	ctb_mes_ref c,
	estabelecimento b,
	ctb_orcamento a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	c.nr_sequencia		= a.nr_seq_mes_ref
and	c.cd_empresa		= b.cd_empresa
and	b.cd_empresa		= cd_empresa_p
and	a.cd_centro_custo = nvl(cd_centro_custo_orig_p, a.cd_centro_custo)
and	substr(ctb_obter_se_centro_usu_estab(a.cd_centro_custo, cd_empresa_p, a.cd_estabelecimento, nm_usuario_p),1,1) = 'S'
and	c.dt_referencia between dt_inicial_orig_w and dt_final_orig_w
and	a.cd_estabelecimento = nvl(cd_estab_p, a.cd_estabelecimento);

begin

dt_inicial_orig_w	:= trunc(dt_ano_orig_p,'yyyy');
dt_final_orig_w		:= fim_ano(dt_ano_orig_p);

for vet in c_orcamento loop
	begin
	cd_centro_custo_dest_w	:= nvl(cd_centro_custo_dest_p, vet.cd_centro_custo);
	dt_referencia_w		:= trunc(to_date('01/' || to_char(vet.dt_referencia,'MM') || '/' || to_char(dt_ano_dest_p,'YYYY'),'dd/mm/yyyy'),'mm');
	ie_vigente_w		:= substr(obter_se_conta_vigente(vet.cd_conta_contabil, dt_referencia_w),1,1);
	
	select	max(a.nr_sequencia)
	into	nr_seq_mes_ref_w
	from	ctb_mes_ref a
	where	a.cd_empresa	= cd_empresa_p
	and	a.dt_referencia	= dt_referencia_w;
	
	select ie_situacao
	into ie_situacao_w
	from conta_contabil 
	where cd_conta_contabil = vet.cd_conta_contabil;
	
	select	max(a.nr_sequencia)
	into	nr_seq_orcamento_w
	from	ctb_orcamento a
	where	a.cd_estabelecimento	= vet.cd_estabelecimento
	and	a.cd_centro_custo	= cd_centro_custo_dest_w
	and	a.cd_conta_contabil	= vet.cd_conta_contabil
	and	a.nr_seq_mes_ref	= nr_seq_mes_ref_w;
	
	if	(nr_seq_orcamento_w is null) and
		(ie_vigente_w = 'S') and
		(ie_situacao_w = 'A') and
		(nr_seq_mes_ref_w is not null) then
	
		select	ctb_orcamento_seq.nextval
		into	nr_seq_orcamento_w
		from	dual;
		
		insert into ctb_orcamento(
			nr_sequencia,
			nr_seq_mes_ref,
			dt_atualizacao,
			nm_usuario,
			cd_estabelecimento,
			cd_conta_contabil,
			cd_centro_custo,
			vl_original,
			vl_orcado,
			vl_realizado,
			ds_observacao,
			ie_cenario,
			ie_origem_orc)
		values(	nr_seq_orcamento_w,
			nr_seq_mes_ref_w,
			sysdate,
			nm_usuario_p,
			vet.cd_estabelecimento,
			vet.cd_conta_contabil,
			cd_centro_custo_dest_w,
			decode(ie_orcado_real_p,'P', vet.vl_realizado, vet.vl_orcado),
			decode(ie_orcado_real_p,'P', vet.vl_realizado, vet.vl_orcado),
			0,
			vet.ds_observacao,
			'N',
			'SIS');
			
	elsif	(nr_seq_orcamento_w is not null) and
		(ie_sobrepor_p = 'S') and
		(ie_situacao_w = 'A') and
		(ie_vigente_w = 'S') then
		begin
		select	max(dt_liberacao)
		into	dt_liberacao_w
		from	ctb_orcamento
		where	nr_seq_mes_ref		= nr_seq_mes_ref_w
		and	cd_conta_contabil	= vet.cd_conta_contabil
		and	cd_centro_custo		= cd_centro_custo_dest_w
		and	cd_estabelecimento	= vet.cd_estabelecimento;

		if	(dt_liberacao_w is null) then
			begin
			update	ctb_orcamento set
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				vl_original		= decode(ie_orcado_real_p, 'P', vet.vl_realizado, vet.vl_orcado),
				vl_orcado		= decode(ie_orcado_real_p, 'P', vet.vl_realizado, vet.vl_orcado)
			where	nr_sequencia		= nr_seq_orcamento_w;
			end;
		end if;
		end;
	end if;
	
	end;
end loop;
commit;

end ctb_duplicar_orc_anual;
/