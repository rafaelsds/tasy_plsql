create or replace
procedure qt_gerar_encaixe_agecons	(cd_estabelecimento_p	number,
				cd_agenda_p		number,
				dt_agenda_p		date,
				hr_encaixe_p		date,
				qt_duracao_p		number,
				cd_pessoa_fisica_p		varchar2,
				nm_pessoa_fisica_p	varchar2,
				cd_convenio_p		number,
				ds_observacao_p		varchar2,
				ie_classif_agenda_p	Varchar2,
				nm_usuario_p		varchar2,
				nr_seq_encaixe_p out	number,
				cd_categoria_p		varchar2,
				cd_plano_p		varchar2) is
				
dt_encaixe_w					date;	
nr_seq_encaixe_w 				number(10) := null;
ie_regra_horario_turno_w 		varchar2(1);	
ie_feriado_w					varchar2(1); 
cd_setor_agenda_w				number(10,0);
ie_perm_encaixe_turno_w			varchar2(1);
dt_intervalo_inicial_w			date;
dt_intervalo_final_w			date;
nr_seq_turno_esp_w				number(10,0);
ie_perm_encaixe_turno_esp_w		varchar2(1);
ie_gerar_dentro_per_w			varchar2(1);
ds_consistencia_w				varchar2(255);
ie_atualizar_sala_w				varchar2(1);
nr_seq_sala_w					number(10,0);
nr_seq_agenda_w					number(10,0);
ie_feriado_agenda_w				agenda.ie_feriado%type;
nr_seq_turno_w					number(10,0);
cd_turno_w						varchar2(1);
dt_nascimento_w					date;
nr_seq_hora_w					number(10,0);

begin

if	(cd_agenda_p is not null) and
	(dt_agenda_p is not null) and
	(hr_encaixe_p is not null) and
	(qt_duracao_p is not null) and
	((cd_pessoa_fisica_p is not null) or
	(nm_pessoa_fisica_p is not null)) and
	(nm_usuario_p is not null) then

	obter_param_usuario(821, 436,  obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gerar_dentro_per_w);
	obter_param_usuario(821, 427,  obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualizar_sala_w);
	
	/* obter horario agenda x encaixe */	
	dt_encaixe_w := pkg_date_utils.get_DateTime(dt_agenda_p, hr_encaixe_p);
	
	select	nvl(max(obter_valor_param_usuario(821,219, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'S')
	  into	ie_regra_horario_turno_w
	  from	dual;

	select nvl(max(ie_feriado),'N'),
	 	   max(cd_setor_agenda)
	  into ie_feriado_agenda_w,
		   cd_setor_agenda_w
	from   agenda
	where  cd_agenda = cd_agenda_p;
	
	if (ie_regra_horario_turno_w = 'T') then
		select	obter_turno_encaixe_d_agecons(cd_agenda_p,dt_encaixe_w,'N')
		into	nr_seq_turno_w
		from	dual;	
	else
		select	obter_turno_encaixe_agecons(cd_agenda_p,dt_encaixe_w)
		into	nr_seq_turno_w
		from	dual;	
	end if;
	
	select	nvl(max(ie_encaixe),'S'),
			nvl(max(ie_feriado),ie_feriado_agenda_w)
	into	ie_perm_encaixe_turno_w,
			ie_feriado_w
	from	agenda_turno
	where	nr_sequencia = nr_seq_turno_w;
	
	if	(ie_perm_encaixe_turno_w = 'N') then
		Wheb_mensagem_pck.exibir_mensagem_abort(224365);
	end if;
	
	if (Obter_Se_Feriado(cd_estabelecimento_p, dt_agenda_p) > 0) then
		if (ie_feriado_w = 'N') then
			Wheb_mensagem_pck.exibir_mensagem_abort(669868);
		end if;
	end if;
	
	select	max(hr_inicial_intervalo),
		max(hr_final_intervalo)
	into	dt_intervalo_inicial_w,
		dt_intervalo_final_w
	from 	AGENDA_TURNO
	where	nr_sequencia = nr_seq_turno_w;
	
	select	max(hr_inicial_intervalo),
		max(hr_final_intervalo)
	into	dt_intervalo_inicial_w,
		dt_intervalo_final_w
	from 	AGENDA_TURNO
	where	nr_sequencia = nr_seq_turno_w;
	
	if (to_char(dt_intervalo_inicial_w,'hh24:mi:ss') <= to_char(dt_encaixe_w,'hh24:mi:ss')) and 
	   (to_char(dt_intervalo_final_w,'hh24:mi:ss') >= to_char(dt_encaixe_w,'hh24:mi:ss')) then
		Wheb_mensagem_pck.exibir_mensagem_abort(291078);
	end if;
	
	if (ie_regra_horario_turno_w = 'T') then
		select	nvl(obter_turno_encaixe_d_agecons(cd_agenda_p,dt_encaixe_w,'S'), 0)
		into	nr_seq_turno_esp_w
		from	dual;
	else
		select	nvl(obter_turno_esp_encaixe(cd_agenda_p,dt_encaixe_w), 0)
		into	nr_seq_turno_esp_w
		from	dual;
	end if;
		
	if	(nr_seq_turno_esp_w > 0) then
		begin
		select	nvl(max(ie_encaixe),'S')
		into	ie_perm_encaixe_turno_esp_w
		from	agenda_turno_esp
		where	nr_sequencia = nr_seq_turno_esp_w;

		if	(ie_perm_encaixe_turno_esp_w = 'N') then
			wheb_mensagem_pck.exibir_mensagem_abort(271560);
		end if;
		end;
	end if;
	
	if	(ie_gerar_dentro_per_w = 'N') and
		(nr_seq_turno_w is not null) then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(216792);
	end if;	
	
	/* consistir horario */
	consistir_horario_agecons(cd_agenda_p, dt_encaixe_w, qt_duracao_p, 'E', ds_consistencia_w);
	if	(ds_consistencia_w is not null) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
	end if;
	
	if	(ie_atualizar_sala_w = 'S') then
		select	max(nr_seq_sala)
		into	nr_seq_sala_w
		from	agenda_turno
		where	nr_sequencia = nr_seq_turno_w;
	end if;	
	
	/* obter turno */
	select	obter_turno_horario_agenda(cd_agenda_p, dt_encaixe_w)
	into	cd_turno_w
	from	dual;


	/* obter classificacao */
	/*
	obter_classif_encaixe_agenda(nr_seq_classif_w);
	*/

	/* obter sequencia */
	select	agenda_consulta_seq.nextval
	into	nr_seq_agenda_w
	from	dual;
	
	
	begin
	select	OBTER_DATA_NASCTO_PF(cd_pessoa_fisica_p)
	into	dt_nascimento_w
	from	dual;
	exception
	when others then
		dt_nascimento_w	:= null;
	end;
	
	select	nvl(max(nr_seq_hora),0)+1
	into	nr_seq_hora_w
	from	agenda_consulta
	where	cd_agenda = cd_agenda_p
	and	dt_agenda = dt_encaixe_w;
	
	/* gerar encaixe */
	insert into agenda_consulta	(
					nr_sequencia,
					cd_agenda,
					dt_agenda,
					nr_minuto_duracao,
					ie_status_agenda,
					cd_pessoa_fisica,
					nm_paciente,
					nr_telefone,
         			ds_email,
					ds_observacao,
					cd_turno,
					dt_agendamento,
					nm_usuario_origem,
					nm_usuario,
					dt_atualizacao,
					ie_encaixe,
					ie_classif_agenda,
					dt_nascimento_pac,
					qt_idade_pac,
					cd_setor_atendimento,
					nr_seq_turno,
					nr_seq_hora,
					nr_seq_sala
					)
				values	(
					nr_seq_agenda_w,
					cd_agenda_p,
					dt_encaixe_w,
					qt_duracao_p,
					'N',
					cd_pessoa_fisica_p,
					substr(nvl(obter_nome_pf(cd_pessoa_fisica_p), nm_pessoa_fisica_p),1,60),
					substr(obter_fone_pac_agenda(cd_pessoa_fisica_p),1,255),
         			substr(obter_compl_pf(cd_pessoa_fisica_p,1,'M' ),1,255),
					ds_observacao_p,
					cd_turno_w,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					sysdate,
					'S',
					ie_classif_agenda_p,
					dt_nascimento_w,
					substr(obter_dados_pf(cd_pessoa_fisica_p,'I'),1,255),
					cd_setor_agenda_w,
					nr_seq_turno_w,
					nr_seq_hora_w,
					nr_seq_sala_w
					);
	
	/* obter dados convenio, caso usuario nao informar (Esta rotina devera permanecer aqui, antes do insert gera erro) */
	if	(cd_convenio_p is not null) and 
		((cd_categoria_p is not null) or (cd_plano_p is not null))then
		update	agenda_consulta
		set	cd_convenio		= cd_convenio_p,
			cd_categoria		= cd_categoria_p,
			cd_plano		= cd_plano_p
		where	nr_sequencia		= nr_seq_agenda_w;	
	end if;
else
	if (cd_agenda_p is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(76137);
	elsif (cd_pessoa_fisica_p is null and nm_pessoa_fisica_p is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(91297);
	elsif (dt_agenda_p is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(166911);
	elsif (hr_encaixe_p is null) then	
		wheb_mensagem_pck.exibir_mensagem_abort(201339);	
	elsif (qt_duracao_p is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(1089281);	
	end if;
end if;	
	
nr_seq_encaixe_p := nr_seq_agenda_w;

end qt_gerar_encaixe_agecons;
/
