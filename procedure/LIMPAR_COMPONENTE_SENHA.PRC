create or replace
procedure limpar_componente_senha(	nm_usu_salvar_p			VARCHAR2 DEFAULT 'X',
					nm_maquina_p			VARCHAR2 DEFAULT 'X',
					cd_perfil_salvar_p		NUMBER DEFAULT 0,
					cd_estab_salvar_p		NUMBER DEFAULT 0) is 
begin
if (nm_usu_salvar_p <> 'X') then
	delete	monitor_senha_custom
	where	nm_usuario = nm_usu_salvar_p;
elsif (nm_maquina_p <> 'X') then
	delete	monitor_senha_custom
	where	nm_maquina = nm_maquina_p;
elsif (cd_perfil_salvar_p <> 0) then
	delete	monitor_senha_custom
	where	cd_perfil = cd_perfil_salvar_p;
elsif (cd_estab_salvar_p <> 0) then
	delete	monitor_senha_custom
	where	cd_estabelecimento = cd_estab_salvar_p;
end if;

commit;

end limpar_componente_senha;
/