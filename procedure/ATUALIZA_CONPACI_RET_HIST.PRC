create or replace
procedure atualiza_conpaci_ret_hist(nr_seq_conpaci_ret_hist_p number,
			nr_sequencia_p number) is 

begin

if (nr_sequencia_p is not null) then
	begin
	update 	convenio_retorno_glosa 
	set 	nr_seq_conpaci_ret_hist = nr_seq_conpaci_ret_hist_p
	where	nr_sequencia = nr_sequencia_p;
	commit;
	end;	
end if;
end atualiza_conpaci_ret_hist;
/