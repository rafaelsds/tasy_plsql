create or replace 
	procedure valida_etapa_requerida ( 	nr_seq_checkup_etapa_p number, 
						nm_usuario_p Varchar2) is

nr_seq_etapa_req_w    	number(10);
nr_atendimento_w      	number(10);
ds_etapa_req_w        	varchar2(255) :='';
ds_etapa_req_msn_w    	varchar2(255) :='';
ds_etapa_requerente_w 	varchar2(255);
ie_existe_etapa_painel_w varchar2(1) := 'N';

dt_cancel_ck_w    	date;
dt_inicio_real_w  	date;
dt_fim_etapa_w    	date;
dt_confirmacao_w  	date;
dt_cancel_etapa_w  	date;
nr_seq_etapa_w    	number(10);
nr_seq_tipo_checkup_etapa_w    	number(10);

cursor C01 is
	select  t.nr_seq_etapa,
		substr(Obter_desc_etapa_checkup(t.nr_seq_etapa),1,255),
		i.nr_atendimento,
		substr(Obter_desc_etapa_checkup(f.nr_seq_etapa),1,255)
	from    checkup_etapa f,
		checkup       i,
		empresa_pessoa_checkup l,
		contrato_tipo_checkup_pf n,
		tipo_checkup p,
		tipo_checkup_etapa r,
		etapa_checkup_requerida t
	where   f.nr_sequencia     	= nr_seq_checkup_etapa_p
	and     f.nr_seq_checkup   	= i.nr_sequencia
	and     i.cd_pessoa_fisica   	= l.cd_pessoa_fisica
	and     l.nr_sequencia       	= n.nr_seq_pessoa_checkup
	and     n.nr_seq_tipo_checkup   = p.nr_sequencia
	and     p.nr_sequencia          = r.nr_seq_tipo_checkup
	and     r.nr_seq_etapa          = f.nr_seq_etapa
	and     r.nr_sequencia          = t.nr_seq_checkup_etapa;

Cursor C02 is
	select 	e.nr_seq_etapa,
		substr(Obter_desc_etapa_checkup(e.nr_seq_etapa),1,255),
		b.nr_atendimento,
		substr(Obter_desc_etapa_checkup(a.nr_seq_etapa),1,255)
	from   	checkup_etapa a,
		checkup b,
		tipo_checkup c,
		tipo_checkup_etapa d,
		etapa_checkup_requerida e
	where  	a.nr_sequencia 		= nr_seq_checkup_etapa_p
	and    	a.nr_seq_checkup 	= b.nr_sequencia
	and    	b.nr_seq_tipo_checkup   = c.nr_sequencia
	and    	c.nr_sequencia          = d.nr_seq_tipo_checkup
	and    	d.nr_seq_etapa          = a.nr_seq_etapa
	and    	d.nr_sequencia          = e.nr_seq_checkup_etapa
	and	exists	(	select	1
				from	checkup_etapa x
				where	x.nr_seq_checkup	= b.nr_sequencia
				and	e.nr_seq_etapa		= x.nr_seq_etapa);


Cursor C03 is
	select 	d.nr_seq_etapa_req,
		substr(Obter_desc_etapa_checkup(d.nr_seq_etapa_req),1,255),
		b.nr_atendimento,
		substr(Obter_desc_etapa_checkup(a.nr_seq_etapa),1,255)
	from   	checkup_etapa a,
		checkup b,
		etapa_checkup c,
		etapa_checkup_req d
	where  	a.nr_sequencia 		= nr_seq_checkup_etapa_p
	and    	a.nr_seq_checkup 	= b.nr_sequencia
	and     a.nr_seq_etapa    	= c.nr_sequencia
	and     c.nr_sequencia    	= d.nr_seq_etapa;
	
begin
if ( nr_seq_checkup_etapa_p is not null ) then
	select 	nvl(max(b.nr_seq_tipo_checkup),0)
	into	nr_seq_tipo_checkup_etapa_w
	from   	checkup_etapa a,
		checkup b
	where  	a.nr_sequencia 		= nr_seq_checkup_etapa_p
	and    	a.nr_seq_checkup 	= b.nr_sequencia;
	
	begin
--CONSISTE AS ETAPAS REQUERIDAS NO CADASTRO DE TIPOS DE CHECKUP
	if	(nvl(nr_seq_tipo_checkup_etapa_w,0) = 0) then
		begin
		open C01;
		loop
		fetch C01 into
			nr_seq_etapa_req_w,
			ds_etapa_req_w,
			nr_atendimento_w,
			ds_etapa_requerente_w;
		exit when C01%notfound;
		begin
		select  max(a.dt_cancelamento),
			max(b.dt_inicio_real),
			max(b.dt_fim_etapa),
			max(b.dt_confirmacao),
			max(b.nr_sequencia),
			max(b.dt_cancelamento)
		into    dt_cancel_ck_w,
			dt_inicio_real_w,
			dt_fim_etapa_w,
			dt_confirmacao_w,
			nr_seq_etapa_w,
			dt_cancel_etapa_w
		from    checkup a,
			checkup_etapa b,
			atendimento_paciente c
		where   a.dt_previsto between trunc(sysdate) and fim_dia(sysdate)
		and     a.nr_atendimento is not null
		and  	b.nr_seq_checkup = a.nr_sequencia
		and  	c.nr_atendimento = a.nr_atendimento
		and     c.nr_atendimento = nr_atendimento_w
		and 	b.nr_seq_etapa   = nr_seq_etapa_req_w
		order by nvl(a.nr_seq_prioridade,9999) asc, c.dt_entrada;    

		select 	decode(max(nr_seq_etapa),null,'N','S')
		into	ie_existe_etapa_painel_w
		from 	checkup_etapa a
		where 	trunc(a.dt_prevista) between trunc(sysdate) and fim_dia(sysdate)
		and   	nr_seq_etapa = nr_seq_etapa_req_w;

		if ( (dt_inicio_real_w 	is null ) or 
		     (dt_inicio_real_w 	is not null )) and
		   ( dt_fim_etapa_w 	is null )  and
		   (ie_existe_etapa_painel_w = 'S') then
			ds_etapa_req_msn_w := ds_etapa_req_msn_w || ds_etapa_req_w ||chr(13)||chr(10);
		end if;
		end;
		end loop;
		close C01;
		end;
		if (ds_etapa_req_msn_w is not null) then
			/*--(-20011,'Para realizar a etapa ' || ds_etapa_requerente_w || ' � preciso realizar as seguintes etapas: '||
										chr(13)||chr(10) || ds_etapa_req_msn_w ||
										'Cadastro de Tipos  de Check-up: ' ||
										chr(13)||chr(10) ||'#@#@');*/
			Wheb_mensagem_pck.exibir_mensagem_abort(264558,	'DS_ETAPA_REQUERENTE_W='||DS_ETAPA_REQUERENTE_W || ';' ||'DS_ETAPA_REQ_MSN_W=' || DS_ETAPA_REQ_MSN_W);							
		end if;
		--end;
	elsif	(nvl(nr_seq_tipo_checkup_etapa_w,0) > 0) then
		begin
		open C02;
		loop
		fetch C02 into
			nr_seq_etapa_req_w,
			ds_etapa_req_w,
			nr_atendimento_w,
			ds_etapa_requerente_w;
		exit when C02%notfound;
		begin
		select  max(a.dt_cancelamento),
			max(b.dt_inicio_real),
			max(b.dt_fim_etapa),
			max(b.dt_confirmacao),
			max(b.nr_sequencia),
			max(b.dt_cancelamento)
		into    dt_cancel_ck_w,
			dt_inicio_real_w,
			dt_fim_etapa_w,
			dt_confirmacao_w,
			nr_seq_etapa_w,
			dt_cancel_etapa_w
		from    checkup a,
			checkup_etapa b,
			atendimento_paciente c
		where   a.dt_previsto between trunc(sysdate) and fim_dia(sysdate)
		and     a.nr_atendimento is not null
		and  	b.nr_seq_checkup = a.nr_sequencia
		and  	c.nr_atendimento = a.nr_atendimento
		and     c.nr_atendimento = nr_atendimento_w
		and 	b.nr_seq_etapa   = nr_seq_etapa_req_w
		order by nvl(a.nr_seq_prioridade,9999) asc, c.dt_entrada;    

		select 	decode(max(nr_seq_etapa),null,'N','S')
		into	ie_existe_etapa_painel_w
		from 	checkup_etapa a
		where 	trunc(a.dt_prevista) between trunc(sysdate) and fim_dia(sysdate)
		and   	nr_seq_etapa = nr_seq_etapa_req_w;

		if ( (dt_inicio_real_w 	is null ) or 
		     (dt_inicio_real_w 	is not null )) and
		   ( dt_fim_etapa_w 	is null )  and
		   (ie_existe_etapa_painel_w = 'S') then
			ds_etapa_req_msn_w := ds_etapa_req_msn_w || ds_etapa_req_w ||chr(13)||chr(10);
		end if;
		end;
		end loop;
		close C02;
		end;
		if (ds_etapa_req_msn_w is not null) then
			/*(-20011,'Para realizar a etapa ' || ds_etapa_requerente_w || ' � preciso realizar as seguintes etapas: '||
										chr(13)||chr(10) || ds_etapa_req_msn_w || chr(13)||chr(10) ||
										'Cadastro de Tipos de Check-up 2' ||
										chr(13)||chr(10) ||'#@#@');*/
			Wheb_mensagem_pck.exibir_mensagem_abort(264560,	'DS_ETAPA_REQUERENTE_W='||DS_ETAPA_REQUERENTE_W || ';' ||'DS_ETAPA_REQ_MSN_W='|| DS_ETAPA_REQ_MSN_W);				

		end if;
	end if;
	
--CONSISTE SE EXISTE ETAPA REQUERIDA NA TABELA ETAPA_CHECKUP_REQ DO CADASTRO DE ETAPAS CHECKUP;	
	open C03;
	loop
	fetch C03 into
		nr_seq_etapa_req_w,
		ds_etapa_req_w,
		nr_atendimento_w,
		ds_etapa_requerente_w;
	exit when C03%notfound;
	begin
	select  max(a.dt_cancelamento),
		max(b.dt_inicio_real),
		max(b.dt_fim_etapa),
		max(b.dt_confirmacao),
		max(b.nr_sequencia),
		max(b.dt_cancelamento)
	into    dt_cancel_ck_w,
		dt_inicio_real_w,
		dt_fim_etapa_w,
		dt_confirmacao_w,
		nr_seq_etapa_w,
		dt_cancel_etapa_w
	from    checkup a,
		checkup_etapa b,
		atendimento_paciente c
	where   a.dt_previsto between trunc(sysdate) and fim_dia(sysdate)
	and     a.nr_atendimento is not null
	and  	b.nr_seq_checkup = a.nr_sequencia
	and  	c.nr_atendimento = a.nr_atendimento
	and     c.nr_atendimento = nr_atendimento_w
	and 	b.nr_seq_etapa   = nr_seq_etapa_req_w
	order by nvl(a.nr_seq_prioridade,9999) asc, c.dt_entrada;    

	select 	decode(max(nr_seq_etapa),null,'N','S')
	into	ie_existe_etapa_painel_w
	from 	checkup_etapa a
	where 	trunc(a.dt_prevista) between trunc(sysdate) and fim_dia(sysdate)
	and   	nr_seq_etapa = nr_seq_etapa_req_w;

	if ( (dt_inicio_real_w 	is null ) or 
	     (dt_inicio_real_w 	is not null )) and
	   ( dt_fim_etapa_w 	is null )  and
	   (ie_existe_etapa_painel_w = 'S') then
		ds_etapa_req_msn_w := ds_etapa_req_msn_w || ds_etapa_req_w ||chr(13)||chr(10);
	end if;
	end;
	end loop;
	close C03;
	end;
	if (ds_etapa_req_msn_w is not null) then
		/*--(-20011,'Para realizar a etapa ' || ds_etapa_requerente_w || ' � preciso realizar as seguintes etapas: '||
									chr(13)||chr(10) || ds_etapa_req_msn_w ||
									'Cadastro de Etapas  do  check-up ' ||
									chr(13)||chr(10) ||'#@#@');*/
		Wheb_mensagem_pck.exibir_mensagem_abort(264558,	'DS_ETAPA_REQUERENTE_W='||DS_ETAPA_REQUERENTE_W || ';' ||'DS_ETAPA_REQ_MSN_W='|| DS_ETAPA_REQ_MSN_W);							
	end if;	
	
--	end;
end if;

commit;
  
  
end valida_etapa_requerida;
/