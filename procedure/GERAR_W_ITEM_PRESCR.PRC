create or replace
procedure Gerar_W_Item_Prescr(
			cd_protocolo_p		number,       
			nm_usuario_p		varchar2,
			nr_prescricao_p		number,   
			nr_seq_item_p		number,    
			nr_seq_solucao_p	number,
			nr_seq_gas_p		number,
			nr_seq_material_p	number,
			qt_dose_p			number,
			ie_origem_inf_p		varchar2,
			cd_unidade_medida_p	varchar2) is 

ie_inserido_w	varchar2(1) := 'N';
nr_next_sequencia_w	number(10);
begin

if	(ie_origem_inf_p = 'SOL') then /*Solu��o*/
	select 	nvl(max('S'), 'N')
	into	ie_inserido_w
	from	W_Item_Prescr
	where	cd_protocolo 	= cd_protocolo_p
	and 	nr_seq_item 	= nr_seq_item_p
	and 	nr_seq_solucao	= nr_seq_solucao_p
	and 	nr_prescricao	= nr_prescricao_p
	and		nm_usuario	    = nm_usuario_p
	and 	nr_seq_material	= nr_seq_material_p
	and 	ie_origem_inf   = ie_origem_inf_p;

	if (ie_inserido_w = 'S') then
		delete from W_Item_Prescr
		where	cd_protocolo 	= cd_protocolo_p
		and 	nr_seq_item 	= nr_seq_item_p
		and 	nr_seq_solucao	= nr_seq_solucao_p
		and 	nr_prescricao	= nr_prescricao_p
		and		nm_usuario	 	= nm_usuario_p
		and 	nr_seq_material	= nr_seq_material_p
		and 	ie_origem_inf   = ie_origem_inf_p;
		commit;
	end if;
elsif	(ie_origem_inf_p = 'M') then /*Medicamentos compostos*/
	select 	nvl(max('S'), 'N')
	into	ie_inserido_w
	from	W_Item_Prescr
	where	cd_protocolo 	= cd_protocolo_p
	and 	nr_seq_item 	= nr_seq_item_p
	and 	nr_prescricao	= nr_prescricao_p
	and		nm_usuario	    = nm_usuario_p
	and 	nr_seq_material	= nr_seq_material_p
	and 	ie_origem_inf   = ie_origem_inf_p;

	if (ie_inserido_w = 'S') then
		delete from W_Item_Prescr
		where	cd_protocolo 	= cd_protocolo_p
		and 	nr_seq_item 	= nr_seq_item_p
		and 	nr_prescricao	= nr_prescricao_p
		and		nm_usuario	 	= nm_usuario_p
		and 	nr_seq_material	= nr_seq_material_p
		and 	ie_origem_inf   = ie_origem_inf_p;
		commit;
	end if;
elsif (ie_origem_inf_p = 'O') then /* Oxig�nio, Gasoterapia*/
	select 	nvl(max('S'), 'N')
	into	ie_inserido_w
	from	W_Item_Prescr
	where	cd_protocolo 	= cd_protocolo_p
	and 	nr_seq_item 	= nr_seq_item_p	
	and		nr_seq_gas		= nr_seq_gas_p
	and 	nr_prescricao	= nr_prescricao_p
	and		nm_usuario	    = nm_usuario_p
	and 	nr_seq_material	= nr_seq_material_p
	and 	ie_origem_inf   = ie_origem_inf_p;

	if (ie_inserido_w = 'S') then
		delete from W_Item_Prescr
		where	cd_protocolo 	= cd_protocolo_p
		and 	nr_seq_item 	= nr_seq_item_p
		and		nr_seq_gas		= nr_seq_gas_p
		and 	nr_prescricao	= nr_prescricao_p
		and		nm_usuario	 	= nm_usuario_p
		and 	nr_seq_material	= nr_seq_material_p
		and 	ie_origem_inf   = ie_origem_inf_p;
		commit;
	end if;
end if;

select 	W_Item_Prescr_seq.nextval
into	nr_next_sequencia_w
from	dual;

Insert into W_Item_Prescr(
				nr_sequencia,
				dt_atualizacao,         
				nm_usuario,             
				nr_prescricao,          
				cd_protocolo,           
				nr_seq_solucao,
				nr_seq_gas,				
				nr_seq_item,            
				qt_dose,
				nr_seq_material,
				ie_origem_inf,
				cd_unidade_medida)
			Values(
				nr_next_sequencia_w,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				cd_protocolo_p,
				nr_seq_solucao_p,
				nr_seq_gas_p,
				nr_seq_item_p,
				qt_dose_p,
				nr_seq_material_p,
				ie_origem_inf_p,
				cd_unidade_medida_p);
commit;

end Gerar_W_Item_Prescr;
/ 
