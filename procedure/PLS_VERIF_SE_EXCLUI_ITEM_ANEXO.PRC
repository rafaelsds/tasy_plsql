create or replace
procedure pls_verif_se_exclui_item_anexo(	nr_seq_auditoria_p		pls_auditoria.nr_sequencia%type,
						nm_usuario_p			varchar2			) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Verificar se na an�lise da autoriza��o foi exclu�do algum item que possui anexo. Caso todos os itens do lote tenham sido exlcu�dos, exlcui o lote de anexo tamb�m.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [   ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

qt_item_lote_anexo_mat_w		number(3);
qt_item_lote_anexo_proc_w		number(3);
qt_item_mat_anexo_quim_w		number(3);
qt_item_mat_anexo_opme_w		number(3);
qt_item_proc_anexo_radio_w		number(3);	
nr_seq_guia_w				pls_guia_plano.nr_sequencia%type;
nr_motivo_exclusao_w			pls_motivo_exclusao.nr_sequencia%type;
ds_tipo_anexo_exc_w			varchar2(255);

Cursor C01 is
	select	nr_seq_mat_origem,
		nr_seq_motivo_exc
	from	pls_auditoria_item
	where	nr_seq_auditoria 	= nr_seq_auditoria_p
	and   	nr_seq_proc_origem is null	
	and	nr_seq_motivo_exc is not null;

Cursor C02 is
	select	nr_seq_proc_origem,
		nr_seq_motivo_exc
	from	pls_auditoria_item
	where	nr_seq_auditoria 	= nr_seq_auditoria_p
	and 	nr_seq_mat_origem is null
	and	nr_seq_motivo_exc is not null;

Cursor C03 is
	select	decode(ie_tipo_anexo, 'QU', 'Quimioterapia' || chr(13) || chr(10), 'RA', 'Radioterapia' || chr(13) || chr(10), 'OP', 'OPME' || chr(13) || chr(10)) ie_tipo_anexo
	from	pls_lote_anexo_guias_aut
	where	nr_seq_motivo_exc is not null
	and	nr_seq_guia	= nr_seq_guia_w;
	
begin

nr_motivo_exclusao_w	:= obter_valor_param_usuario(1270, 1, Obter_Perfil_Ativo, nm_usuario_p, 0);

for r_c01_w in c01 loop	
	select 	count(1)
	into	qt_item_lote_anexo_mat_w
	from	pls_lote_anexo_mat_aut
	where	nr_seq_plano_mat	= r_c01_w.nr_seq_mat_origem;	

	if	(qt_item_lote_anexo_mat_w > 0) then	
		update	pls_lote_anexo_mat_aut
		set	nr_seq_motivo_exc	= r_c01_w.nr_seq_motivo_exc,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_plano_mat	= r_c01_w.nr_seq_mat_origem;	
		
	end if;	
end loop;

for r_c02_w in c02 loop
	select 	count(1)
	into	qt_item_lote_anexo_proc_w
	from	pls_lote_anexo_proc_aut
	where	nr_seq_plano_proc	= r_c02_w.nr_seq_proc_origem;		

	if	(qt_item_lote_anexo_proc_w > 0) then	
		update	pls_lote_anexo_proc_aut
		set	nr_seq_motivo_exc	= r_c02_w.nr_seq_motivo_exc,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_plano_proc	= r_c02_w.nr_seq_proc_origem;		
	end if;	
end loop;
	
begin
	select	nr_seq_guia
	into	nr_seq_guia_w
	from	pls_auditoria
	where 	nr_sequencia 	= nr_seq_auditoria_p;			
exception
when others then
	nr_seq_guia_w 	:= null;
end;

if	(nr_seq_guia_w is not null) then
	select	count (1)
	into	qt_item_mat_anexo_quim_w
	from	pls_lote_anexo_mat_aut a,
		pls_lote_anexo_guias_aut b
	where	a.nr_seq_lote_anexo_guia	= b.nr_sequencia
	and	a.nr_seq_motivo_exc is null
	and	b.ie_tipo_anexo			= 'QU'
	and 	b.nr_seq_guia			= nr_seq_guia_w;
	
	if	(qt_item_mat_anexo_quim_w = 0)then
			update	pls_lote_anexo_guias_aut
			set	nr_seq_motivo_exc	= nr_motivo_exclusao_w,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	ie_tipo_anexo		= 'QU'	
			and 	nr_seq_guia		= nr_seq_guia_w;		
	end if;
			
	select	count (1)
	into	qt_item_mat_anexo_opme_w
	from	pls_lote_anexo_mat_aut a,
		pls_lote_anexo_guias_aut b
	where	a.nr_seq_lote_anexo_guia	= b.nr_sequencia
	and	a.nr_seq_motivo_exc is null
	and	b.ie_tipo_anexo			= 'OP'
	and 	b.nr_seq_guia			= nr_seq_guia_w;
		
	if	(qt_item_mat_anexo_opme_w = 0)then
			update	pls_lote_anexo_guias_aut
			set	nr_seq_motivo_exc	= nr_motivo_exclusao_w,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	ie_tipo_anexo		= 'OP'	
			and 	nr_seq_guia		= nr_seq_guia_w;		
	end if;	
	
	if (pls_obter_versao_tiss < '3.03.00') then	
		select	count (1)
		into	qt_item_proc_anexo_radio_w
		from	pls_lote_anexo_proc_aut a,
			pls_lote_anexo_guias_aut b
		where	a.nr_seq_lote_anexo_guia	= b.nr_sequencia
		and	a.nr_seq_motivo_exc is null
		and	b.ie_tipo_anexo			= 'RA'
		and 	b.nr_seq_guia			= nr_seq_guia_w;
		
	else	-- S� exclui o lote de anexo de radio se n�o houver item com o anexo.
		select	count (1)
		into	qt_item_proc_anexo_radio_w
		from	pls_auditoria_item
		where	nr_seq_auditoria 	= nr_seq_auditoria_p
		and	nr_seq_proc_origem is not null
		and	nr_seq_motivo_exc is null
		and	ie_tipo_anexo		= 'RA';	
	end if;	
	
	if	(qt_item_proc_anexo_radio_w = 0)then
			update	pls_lote_anexo_guias_aut
			set	nr_seq_motivo_exc	= nr_motivo_exclusao_w,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	ie_tipo_anexo		= 'RA'	
			and 	nr_seq_guia		= nr_seq_guia_w;
	end if;	
		
	for r_c03_w in c03 loop	
		begin
			ds_tipo_anexo_exc_w	:= ds_tipo_anexo_exc_w || r_c03_w.ie_tipo_anexo;
		exception
		when others then
			ds_tipo_anexo_exc_w	:= null;
		end;
				
	end loop;
	
	--Caso algum lote de anexo seja exclu�do, insere o registro no hist�rico da guia
	if	(ds_tipo_anexo_exc_w is not null) then				
		pls_guia_gravar_historico(	nr_seq_guia_w, 1, 'Exlu�do o(s) anexo(s) de ' || ds_tipo_anexo_exc_w,
						null, nm_usuario_p);
	end if;		
end if;

end pls_verif_se_exclui_item_anexo;
/
