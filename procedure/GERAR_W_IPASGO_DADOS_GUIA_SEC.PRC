create or replace
procedure gerar_w_ipasgo_dados_guia_sec(
			nr_interno_conta_p		number,
			dt_mesano_referencia_p	date,
			nm_usuario_p		Varchar2,
			nr_seq_tipo_fatura_p	number,
			qt_linha_arq_p in out	number,
			qt_linha_atend_p in out	number) is


nr_doc_guia_atend_w		varchar2(20);
nr_doc_convenio_w		varchar2(20);
nr_doc_convenio_ww		varchar2(20) := 'X';
nr_atendimento_w		number(10,0);
dt_procedimento_w		date;
dt_procedimento_ww		date;
qt_ato_w			number(10,0) := 1;
nr_doc_convenio_proc_w		varchar2(11);
cd_tipo_fatura_w		fatur_tipo_fatura.cd_tipo_fatura%type;
nr_ato_ipasgo_w			procedimento_paciente.nr_ato_ipasgo%type := 0;
nr_ato_ipasgo_ww		procedimento_paciente.nr_ato_ipasgo%type := 0;

cursor c01 is
select	substr(a.cd_senha,1,11),
	nvl(a.nr_ato_ipasgo,0)
from	procedimento_paciente a,
	conta_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	a.nr_atendimento	= b.nr_atendimento
and	a.nr_interno_conta	= b.nr_interno_conta
and	nvl(a.cd_senha,'X')	<> 'X'
and	a.cd_senha 		<> nvl(nr_doc_guia_atend_w,0)
and	cd_tipo_fatura_w 	<> 4
and	obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) = 1
union all
select	substr(a.cd_senha,1,11),
	nvl(a.nr_ato_ipasgo,0)
from	procedimento_paciente a,
	conta_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	a.nr_atendimento	= b.nr_atendimento
and	a.nr_interno_conta	= b.nr_interno_conta
and	nvl(a.cd_senha,'X')	<> 'X'
and	cd_tipo_fatura_w	 = 4
and	a.cd_senha		<> nvl(nr_doc_guia_atend_w,0)
and	obter_classif_material_proced(null, a.cd_procedimento, a.ie_origem_proced) in (1,2)
union all
select	substr(c.nr_doc_honor_conv,1,11),
	nvl(a.nr_ato_ipasgo,0)
from	Procedimento_participante c,
	procedimento_paciente a,
	conta_paciente b
where	b.nr_atendimento 	= nr_atendimento_w
and	b.nr_interno_conta	= nr_interno_conta_p
and	a.nr_atendimento	= b.nr_atendimento
and	a.nr_interno_conta	= b.nr_interno_conta
and	c.nr_sequencia		= a.nr_sequencia
and	nvl(c.nr_doc_honor_conv,'X') <> 'X'
and	c.nr_doc_honor_conv	<> nvl(nr_doc_guia_atend_w,0)
order by	2,1;

begin

select	b.nr_atendimento,
	--substr(somente_numero(obter_guia_atend(b.nr_atendimento, b.cd_convenio_parametro, b.cd_categoria_parametro)),1,11)
	substr(elimina_caractere_especial(obter_guia_atend(b.nr_atendimento, b.cd_convenio_parametro, b.cd_categoria_parametro)),1,11)
into	nr_atendimento_w,
	nr_doc_guia_atend_w
from	conta_paciente b
where	b.nr_interno_conta	= nr_interno_conta_p;

/*Mesmo tratamento do registro 2, so gera o 3 se realmente for diferente da guia principal do reg 2- Solicitacao Hemolabor  OS416225 29-03-2012*/
--select	substr(somente_numero(nvl(max(b.nr_doc_convenio),max(a.nr_doc_convenio))),1,10)
select	substr(elimina_caractere_especial(nvl(max(b.nr_doc_convenio),max(a.nr_doc_convenio))),1,11)
into	nr_doc_convenio_proc_w
from	prescr_procedimento a,
	procedimento_Paciente b
where	a.nr_sequencia 		= b.nr_sequencia_prescricao
and	a.nr_prescricao		= b.nr_prescricao
and	a.cd_procedimento		= b.cd_procedimento
and	a.ie_origem_proced 	= b.ie_origem_proced
and	b.nr_atendimento		= nr_atendimento_w
and	b.nr_interno_conta		= nr_interno_conta_p;

if	(nvl(nr_doc_convenio_proc_w,'X') = 'X') then
	--select	substr(somente_numero(max(b.nr_doc_convenio)),1,10)
	select	substr(elimina_caractere_especial(max(b.nr_doc_convenio)),1,11)
	into	nr_doc_convenio_proc_w
	from	procedimento_Paciente b
	where	b.nr_atendimento	= nr_atendimento_w
	and	b.nr_interno_conta	= nr_interno_conta_p;
end if;

if	(nvl(nr_doc_convenio_proc_w,'X') <> 'X') and
	(nvl(nr_doc_convenio_w,'X') <> 'X') and
	(nvl(nr_doc_convenio_w,'X') <> nvl(nr_doc_convenio_proc_w,'X')) then
	nr_doc_guia_atend_w := nr_doc_convenio_proc_w;
end if;
/*Fim alteracao OS416225 29-03-2012*/

begin
select	cd_tipo_fatura
into	cd_tipo_fatura_w
from	fatur_tipo_fatura
where	nr_sequencia = nr_seq_tipo_fatura_p;
exception
when others then
	cd_tipo_fatura_w := 0;
end;

open C01;
loop
fetch C01 into
	nr_doc_convenio_w,
	nr_ato_ipasgo_w;
exit when C01%notfound;
	begin

	nr_doc_convenio_w := substr(elimina_caractere_especial(nr_doc_convenio_w),1,11);


	if	(nr_ato_ipasgo_w > 0) and
		(nr_ato_ipasgo_w <> nvl(nr_ato_ipasgo_ww,0))  then
		qt_ato_w		:= nr_ato_ipasgo_w;
		nr_ato_ipasgo_ww	:= nr_ato_ipasgo_w;
	elsif	(nvl(nr_ato_ipasgo_ww,0) > 0) then
		qt_ato_w 		:= nr_ato_ipasgo_ww;
	end if;

	if	(nr_doc_convenio_ww <> nr_doc_convenio_w) then
		begin

		qt_linha_arq_p := qt_linha_arq_p + 1;

		insert into w_ipasgo_dados_guia_sec(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_linha,
			tp_registro,
			nr_linha_atend,
			nr_atendimento,
			nr_guia,
			dt_mesano_referencia,
			nr_interno_conta,
			ds_linha,
			nr_seq_tipo_fatura,
			nr_linha_ato)
		values(	w_ipasgo_dados_guia_sec_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			qt_linha_arq_p,
			3,
			qt_linha_atend_p,
			nr_atendimento_w,
			somente_numero(nr_doc_convenio_w),
			dt_mesano_referencia_p,
			nr_interno_conta_p,
			qt_linha_arq_p || '|' ||
			'3' || '|' ||
			qt_linha_atend_p || '|' ||
			nr_doc_convenio_w || '|' ||
			qt_ato_w ||
			'|||||||||',
			nr_seq_tipo_fatura_p,
			qt_ato_w);

		nr_doc_convenio_ww := nr_doc_convenio_w;

		end;
	end if;


	end;
end loop;
close C01;

commit;

end gerar_w_ipasgo_dados_guia_sec;
/
