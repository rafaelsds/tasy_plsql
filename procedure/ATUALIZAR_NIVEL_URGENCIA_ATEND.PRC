create or replace
PROCEDURE Atualizar_Nivel_Urgencia_Atend(
			nivel_urgencia_p	NUMBER,
			nr_atendimento_p	NUMBER,
			ds_observacao_p		varchar2,
			nm_usuario_p		VARCHAR2) IS

BEGIN

wheb_assist_pck.set_obs_log_urgencia_atend(ds_observacao_p);

  UPDATE atendimento_paciente
  SET 	 nr_seq_triagem = nivel_urgencia_p
  WHERE  nr_atendimento =  nr_atendimento_p;

COMMIT;

wheb_assist_pck.set_obs_log_urgencia_atend(null);

END Atualizar_Nivel_Urgencia_Atend;
/
