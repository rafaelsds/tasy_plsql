CREATE OR REPLACE
PROCEDURE gerar_inspecao_ordem(		nr_sequencia_p		NUMBER,
					nm_usuario_p		VARCHAR2,
					nr_nota_fiscal_p		VARCHAR2,
					nr_seq_registro_p		NUMBER,
					ds_erro_p		out varchar2,
					dt_entrega_p	date default null) IS

cd_pessoa_responsavel_w		VARCHAR2(10);
cd_cgc_fornecedor_w		VARCHAR2(14);
cd_pessoa_fisica_w		VARCHAR2(10);
nr_ordem_compra_w		NUMBER(10);
cd_estabelecimento_w		NUMBER(4);
dt_aprovacao_w			DATE;
cd_material_w			NUMBER(6);
qt_prevista_entrega_w		NUMBER(13,4);
nr_item_oci_w			NUMBER(5);
vl_unitario_material_w		NUMBER(13,4);
nr_sequencia_w			NUMBER(10);
pr_desconto_w			NUMBER(7,4);
vl_desconto_w			NUMBER(13,2);
cd_condicao_pagamento_w		NUMBER(10);
ds_material_direto_w		VARCHAR2(255);
VarBusca_quantidade_oc_w	VARCHAR2(15);
VarBusca_valor_oc_w		VARCHAR2(15);
nr_seq_nao_conf_w		NUMBER(10);
ds_ordens_w			VARCHAR2(255);
dt_entrega_atrada_w		date;
ie_consiste_ent_atrasada_w	varchar2(1);
cd_perfil_ativo_w		perfil.cd_perfil%type;

nr_seq_inspecao_w		NUMBER(10);
qt_inspecao_w			NUMBER(5);
qt_original_w			ordem_compra_item.qt_original%TYPE;
nr_seq_marca_w			ordem_compra_item.nr_seq_marca%type;

/* Parametro [ 44 ]  */
ie_forma_contagem_inspecao_w	VARCHAR2(1) := 'I';
/* Parametro [ 46 ]  */
ie_exige_segunda_contagem_w	VARCHAR2(1) := 'N';
/*Parametro [ 85 ] */
ie_copia_inf_valor_contagens_w VARCHAR2(1) := 'N';

CURSOR c01 IS
SELECT	a.nr_ordem_compra,
	a.cd_cgc_fornecedor,
	a.cd_pessoa_fisica,
	a.dt_aprovacao,
	b.cd_material,
	b.nr_item_oci,
	b.vl_unitario_material,
	c.nr_sequencia,
	c.qt_prevista_entrega - obter_qt_inspecao(a.nr_ordem_compra,b.nr_item_oci,c.dt_prevista_entrega),
	b.pr_descontos,
	b.vl_desconto,
	a.cd_condicao_pagamento,
	ds_material_direto,
	qt_original,
	b.nr_seq_marca
FROM	ordem_compra a,
	ordem_compra_item b,
	ordem_compra_item_entrega c
WHERE	a.nr_ordem_compra = b.nr_ordem_compra
AND	b.nr_ordem_compra = c.nr_ordem_compra
AND	b.nr_item_oci = c.nr_item_oci
AND	c.nr_sequencia = nr_sequencia_p;

BEGIN

SELECT	a.cd_estabelecimento
INTO	cd_estabelecimento_w
FROM	ordem_compra a,
	ordem_compra_item b,
	ordem_compra_item_entrega c
WHERE	a.nr_ordem_compra = b.nr_ordem_compra
AND	b.nr_ordem_compra = c.nr_ordem_compra
AND	b.nr_item_oci = c.nr_item_oci
AND	c.nr_sequencia = nr_sequencia_p;

select	obter_perfil_ativo
into	cd_perfil_ativo_w
from	dual;

obter_param_usuario(270, 25, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, VarBusca_quantidade_oc_w);
obter_param_usuario(270, 26, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, VarBusca_valor_oc_w);
obter_param_usuario(270, 44, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, ie_forma_contagem_inspecao_w);
obter_param_usuario(270, 46, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, ie_exige_segunda_contagem_w);
obter_param_usuario(270, 73, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, nr_seq_nao_conf_w);
obter_param_usuario(270, 85, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, ie_copia_inf_valor_contagens_w);
obter_param_usuario(270, 106, cd_perfil_ativo_w, nm_usuario_p, cd_estabelecimento_w, ie_consiste_ent_atrasada_w);

OPEN C01;
LOOP
FETCH C01 INTO
	nr_ordem_compra_w,
	cd_cgc_fornecedor_w,
	cd_pessoa_fisica_w,
	dt_aprovacao_w,
	cd_material_w,
	nr_item_oci_w,
	vl_unitario_material_w,
	nr_sequencia_w,
	qt_prevista_entrega_w,
	pr_desconto_w,
	vl_desconto_w,
	cd_condicao_pagamento_w,
	ds_material_direto_w,
	qt_original_w,
	nr_seq_marca_w;
EXIT WHEN c01%NOTFOUND;
	BEGIN

	SELECT	COUNT(*)
	INTO	qt_inspecao_w
	FROM	inspecao_recebimento
	WHERE	nr_ordem_compra = nr_ordem_compra_w;
	
	IF	(qt_inspecao_w > 0) THEN
	
		vl_desconto_w	:= dividir((qt_prevista_entrega_w * vl_desconto_w), qt_original_w);
	END IF;
	
	IF	(dt_aprovacao_w IS NULL) THEN
		/*Esta ordem de compra nao foi aprovada*/
		wheb_mensagem_pck.exibir_mensagem_abort(218062);
	ELSE
		SELECT	NVL(MAX(SUBSTR(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10)),'X')
		INTO	cd_pessoa_responsavel_w
		FROM	dual;

		IF	(cd_pessoa_responsavel_w = 'X') THEN
			/*Usuario deve estar vinculado com uma pessoa fisica.*/
			wheb_mensagem_pck.exibir_mensagem_abort(218063);
		END IF;

		IF	(VarBusca_quantidade_oc_w = 'N') THEN
			qt_prevista_entrega_w	:= 0;
		END IF;
		IF	(VarBusca_valor_oc_w = 'N') THEN
			vl_unitario_material_w	:= 0;
		END IF;
		if	(ie_consiste_ent_atrasada_w = 'S') then
		
			select	min(b.dt_prevista_entrega)
			into	dt_entrega_atrada_w
			from	ordem_compra_item_entrega b,
				ordem_compra_item a
			where	a.nr_ordem_compra = b.nr_ordem_compra
			and	a.nr_item_oci = b.nr_item_oci
			and	(b.qt_prevista_entrega - obter_qt_inspecao(a.nr_ordem_compra,a.nr_item_oci,b.dt_prevista_entrega)) > 0
			and	nvl(b.qt_prevista_entrega,0) > nvl(b.qt_real_entrega,0)
			and	b.dt_cancelamento is null
			and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_prevista_entrega) < ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_entrega_p)
			and	a.nr_ordem_compra = nr_ordem_compra_w
			and	a.cd_material = cd_material_w;
			
			if	(dt_entrega_atrada_w is not null) then
				wheb_mensagem_pck.exibir_mensagem_abort(410448,'DT_ENTREGA_ATRASADA_W='||dt_entrega_atrada_w);				
			end if;
			
		end if;

		SELECT	inspecao_recebimento_seq.NEXTVAL
		INTO	nr_seq_inspecao_w
		FROM	dual;

		INSERT INTO inspecao_recebimento(
			nr_sequencia,
			cd_cgc,
			cd_pessoa_fisica,
			cd_material,
			nr_nota_fiscal,
			dt_recebimento,
			dt_atualizacao,
			nm_usuario,
			ds_lote_fornec,
			dt_validade,
			qt_inspecao,
			ie_externo,
			ie_interno,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_responsavel,
			ie_temperatura,
			ie_laudo,
			nr_ordem_compra,
			ie_motivo_devolucao,
			nr_item_oci,
			vl_unitario_material,
			dt_entrega_real,
			nr_seq_entrega,
			nr_seq_registro,
			pr_desconto,
			vl_desconto,
			cd_condicao_pagamento,
			ds_material_direto,
			nr_seq_tipo_nao_conf,
			nr_seq_marca)
		VALUES(	nr_seq_inspecao_w,
			cd_cgc_fornecedor_w,
			cd_pessoa_fisica_w,
			cd_material_w,
			NVL(nr_nota_fiscal_p,NULL),
			ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(SYSDATE),
			SYSDATE,
			nm_usuario_p,
			'',
			NULL,
			qt_prevista_entrega_w,
			'N',
			'N',
			SYSDATE,
			nm_usuario_p,
			cd_pessoa_responsavel_w,
			'',
			'N',
			nr_ordem_compra_w,
			'',
			nr_item_oci_w,
			vl_unitario_material_w,
			ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(SYSDATE),
			nr_sequencia_w,
			DECODE(nr_seq_registro_p,0,'',nr_seq_registro_p),
			pr_desconto_w,
			vl_desconto_w,
			cd_condicao_pagamento_w,
			ds_material_direto_w,
			nr_seq_nao_conf_w,
			nr_seq_marca_w);

		/* Inspecao Contagem - Gera as inspecoes para Primeira e Segunda contagem */
		IF	(ie_exige_segunda_contagem_w = 'S') AND
			(ie_forma_contagem_inspecao_w <> 'A') AND
			(nr_seq_registro_p <> 0) AND
			(ie_copia_inf_valor_contagens_w = 'S') THEN
			BEGIN
			INSERT INTO inspecao_contagem(
				nr_sequencia,
				nr_seq_inspecao,
				nr_seq_registro,
				nr_seq_contagem,
				nr_ordem_compra,
				nr_item_oci,
				cd_material,
				dt_atualizacao,
				nm_usuario,
				qt_inspecao,
				ie_externo,
				ie_interno,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_temperatura,
				ie_laudo,
				ie_motivo_devolucao,
				vl_unitario_material,
				dt_entrega_real,
				pr_desconto,
				vl_desconto,
				cd_condicao_pagamento,
				nr_seq_entrega,
				nr_seq_marca)
			VALUES(inspecao_contagem_seq.NEXTVAL,
				nr_seq_inspecao_w,
				nr_seq_registro_p,
				1, -- Primeira Contagem
				nr_ordem_compra_w,
				nr_item_oci_w,
				cd_material_w,
				SYSDATE, -- dt_atualizacao
				nm_usuario_p,
				qt_prevista_entrega_w, -- qt_inspecao
				'N', -- ie_externo
				'N', -- ie_interno
				SYSDATE,
				nm_usuario_p,
				'', -- ie_temperatura
				'N', -- ie_laudo
				'', -- ie_motivo_devolucao
				vl_unitario_material_w, -- vl_unitario_material
				ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(SYSDATE), -- dt_entrega_real
				pr_desconto_w, -- pr_desconto
				vl_desconto_w, -- vl_desconto
				cd_condicao_pagamento_w, -- cd_condicao_pagamento
				nr_sequencia_p,
				nr_seq_marca_w);


			INSERT INTO inspecao_contagem(
				nr_sequencia,
				nr_seq_inspecao,
				nr_seq_registro,
				nr_seq_contagem,
				nr_ordem_compra,
				nr_item_oci,
				cd_material,
				dt_atualizacao,
				nm_usuario,
				qt_inspecao,
				ie_externo,
				ie_interno,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_temperatura,
				ie_laudo,
				ie_motivo_devolucao,
				vl_unitario_material,
				dt_entrega_real,
				pr_desconto,
				vl_desconto,
				cd_condicao_pagamento,
				nr_seq_entrega,
				nr_seq_marca)
			VALUES(inspecao_contagem_seq.NEXTVAL,
				nr_seq_inspecao_w,
				nr_seq_registro_p,
				2, -- Primeira Contagem
				nr_ordem_compra_w,
				nr_item_oci_w,
				cd_material_w,
				SYSDATE, -- dt_atualizacao
				nm_usuario_p,
				qt_prevista_entrega_w, -- qt_inspecao
				'N', -- ie_externo
				'N', -- ie_interno
				SYSDATE,
				nm_usuario_p,
				'', -- ie_temperatura
				'N', -- ie_laudo
				'', -- ie_motivo_devolucao
				vl_unitario_material_w, -- vl_unitario_material
				ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(SYSDATE), -- dt_entrega_real
				pr_desconto_w, -- pr_desconto
				vl_desconto_w, -- vl_desconto
				cd_condicao_pagamento_w, -- cd_condicao_pagamento
				nr_sequencia_p,
				nr_seq_marca_w);
			END;
		ELSIF	(ie_exige_segunda_contagem_w = 'S') AND
				(ie_forma_contagem_inspecao_w <> 'A') AND
				(nr_seq_registro_p <> 0) AND
				(ie_copia_inf_valor_contagens_w = 'N') THEN
				BEGIN
				INSERT INTO inspecao_contagem(
					nr_sequencia,
					nr_seq_inspecao,
					nr_seq_registro,
					nr_seq_contagem,
					nr_ordem_compra,
					nr_item_oci,
					cd_material,
					dt_atualizacao,
					nm_usuario,
					qt_inspecao,
					ie_externo,
					ie_interno,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_temperatura,
					ie_laudo,
					ie_motivo_devolucao,
					vl_unitario_material,
					dt_entrega_real,
					pr_desconto,
					vl_desconto,
					cd_condicao_pagamento,
					nr_seq_entrega,
					nr_seq_marca)
				VALUES(inspecao_contagem_seq.NEXTVAL,
					nr_seq_inspecao_w,
					nr_seq_registro_p,
					1, -- Primeira Contagem
					nr_ordem_compra_w,
					nr_item_oci_w,
					cd_material_w,
					SYSDATE, -- dt_atualizacao
					nm_usuario_p,
					qt_prevista_entrega_w, -- qt_inspecao
					'N', -- ie_externo
					'N', -- ie_interno
					SYSDATE,
					nm_usuario_p,
					'', -- ie_temperatura
					'N', -- ie_laudo
					'', -- ie_motivo_devolucao
					NULL, -- vl_unitario_material
					ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(SYSDATE), -- dt_entrega_real
					'', -- pr_desconto
					'', -- vl_desconto
					'', -- cd_condicao_pagamento
					nr_sequencia_p,
					nr_seq_marca_w);

				INSERT INTO inspecao_contagem(
					nr_sequencia,
					nr_seq_inspecao,
					nr_seq_registro,
					nr_seq_contagem,
					nr_ordem_compra,
					nr_item_oci,
					cd_material,
					dt_atualizacao,
					nm_usuario,
					qt_inspecao,
					ie_externo,
					ie_interno,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ie_temperatura,
					ie_laudo,
					ie_motivo_devolucao,
					vl_unitario_material,
					dt_entrega_real,
					pr_desconto,
					vl_desconto,
					cd_condicao_pagamento,
					nr_seq_entrega,
					nr_seq_marca)
				VALUES(inspecao_contagem_seq.NEXTVAL,
					nr_seq_inspecao_w,
					nr_seq_registro_p,
					2, -- Primeira Contagem
					nr_ordem_compra_w,
					nr_item_oci_w,
					cd_material_w,
					SYSDATE, -- dt_atualizacao
					nm_usuario_p,
					qt_prevista_entrega_w, -- qt_inspecao
					'N', -- ie_externo
					'N', -- ie_interno
					SYSDATE,
					nm_usuario_p,
					'', -- ie_temperatura
					'N', -- ie_laudo
					'', -- ie_motivo_devolucao
					NULL, -- vl_unitario_material
					ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(SYSDATE), -- dt_entrega_real
					'', -- pr_desconto
					'', -- vl_desconto
					'', -- cd_condicao_pagamento
					nr_sequencia_p,
					nr_seq_marca_w);
				END;
		END IF;

	END IF;
	END;
END LOOP;
CLOSE C01;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;


END gerar_inspecao_ordem;
/