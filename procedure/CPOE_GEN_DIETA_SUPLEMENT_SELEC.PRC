create or replace
procedure CPOE_GEN_DIETA_SUPLEMENT_SELEC(nr_atendimento_p	number,
                                        cd_material_p        cpoe_dieta.cd_material%type,
                                        cd_estabelecimento_p    number,
                                        cd_perfil_p             number,
										nm_usuario_p			varchar2,
										cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type,
                                        nr_seq_item_gerado_p    out number,                                      
										ie_prescritor_aux_p		varchar2 default 'N',
										cd_medico_p				number default null,
                                        nr_seq_cpoe_order_unit_p		cpoe_dieta.nr_seq_cpoe_order_unit%type
										) is

begin

select	cpoe_dieta_seq.nextval
into	nr_seq_item_gerado_p
from	dual;

insert into cpoe_dieta(
        nr_sequencia,
        cd_material,
        ie_duracao,
        nm_usuario,
        nm_usuario_nrec,
        dt_atualizacao,
        dt_atualizacao_nrec,
		dt_inicio,
        nr_atendimento,
        ie_tipo_dieta,
		cd_perfil_ativo,
		cd_pessoa_fisica,
		cd_funcao_origem,
		ie_prescritor_aux,
		cd_medico,
        nr_seq_cpoe_order_unit)
    values (
        nr_seq_item_gerado_p,
        cd_material_p,
        'C',
        nm_usuario_p,
        nm_usuario_p,
        sysdate,
        sysdate,
		sysdate,
        nr_atendimento_p,
        'S',
		cd_perfil_p,
		cd_pessoa_fisica_p,
		2314,
		ie_prescritor_aux_p,
		cd_medico_p,
        nr_seq_cpoe_order_unit_p);

commit;

end CPOE_GEN_DIETA_SUPLEMENT_SELEC;
/
