CREATE OR REPLACE procedure ageint_gerar_medico_cons_item(
															nr_seq_ageint_p		number,
															nr_seq_item_p		number,
															nm_usuario_p		Varchar2,
															cd_estabelecimento_p	number) is

nr_seq_item_w		number(10);
cd_especialidade_w	number(5);
qt_idade_w			number(3);
cd_paciente_w		varchar2(10);
dt_nascimento_w		date;
ie_sexo_w			varchar2(1);
cd_medico_item_w	varchar2(10);
cd_estabelecimento_w	number(5);
ie_estab_usuario_w	varchar2(1);
cd_estab_agenda_w	number(5);
ie_perm_agenda_w	varchar2(1)	:= 'S';
qt_estab_user_w		number(10);
nr_Seq_grupo_selec_w	number(10);
ie_consiste_regra_lib_w	varchar2(1);
nr_seq_area_selec_w		number(10);
nr_seq_area_atuacao_w	agenda_integrada_item.nr_seq_area_atuacao%type;
nr_seq_area_atuacao_ww	medico_area_atuacao.nr_seq_area_atuacao%type;
nr_seq_atuacao_agenda_w	agenda.nr_seq_area_atuacao%type;

Cursor C01 is
	select	a.nr_sequencia,
			a.cd_especialidade,
			a.nr_seq_grupo_selec,
			nvl(a.nr_seq_area_atuacao,0)
	from	agenda_integrada_item a
	where	a.nr_seq_agenda_int	= nr_seq_Ageint_p
	and		a.nr_sequencia		= nvl(nr_seq_item_p, a.nr_sequencia)
	and		a.ie_tipo_agendamento	= 'C'
	and		not exists (select 1 from ageint_medico_item b where b.nr_seq_item = a.nr_sequencia)
	order by 1;

Cursor C02 is
	select	nvl(am.cd_medico, a.cd_pessoa_fisica),
			a.cd_estabelecimento,
			nvl(a.nr_seq_area_atuacao,0)
	from    agenda a,
			agenda_medico am
	where   a.cd_agenda			= am.cd_agenda(+)
	and		nvl(a.ie_agenda_integrada,'N')	= 'S'
	and     nvl(am.ie_situacao, 'A')		= 'A'
	and		a.ie_situacao			= 'A'
	and		(a.cd_especialidade		= cd_especialidade_w or exists (select 1 
                                                     from AGENDA_CONS_ESPECIALIDADE acp
                                                    where acp.cd_especialidade =  cd_especialidade_w
                                                      and acp.cd_agenda = a.cd_agenda))
	and		a.cd_tipo_Agenda = 3
	and		(((ie_consiste_regra_lib_w = 'S') and	
			(	exists	(select	1
					from 	agenda_cons_regra_proc x
					where 	x.nr_seq_grupo_ageint = nr_Seq_grupo_selec_w
					and 	x.cd_agenda = a.cd_agenda
					)
		or	not	exists 	(select	1
					from 	agenda_cons_regra_proc y
					where 	y.nr_seq_grupo_ageint is not null
					and 	y.cd_agenda = a.cd_agenda
					)
			))
		or 	(ie_consiste_regra_lib_w = 'N')
		or 	(nr_seq_grupo_selec_w is null)
			)
	and		((qt_idade_w	>= nvl(qt_idade_min,0)
	and		qt_idade_w	<= nvl(qt_idade_max,999))
	or		qt_idade_w = 0)
	and		(((ie_consiste_regra_lib_w = 'S') and	
			(	exists	(select	1
					from 	agenda_cons_regra_proc x
					where 	x.nr_seq_area_ageint = nr_seq_area_selec_w
					and 	x.cd_agenda = a.cd_agenda
					)
		or	not	exists 	(select	1
					from 	agenda_cons_regra_proc y
					where 	y.nr_seq_area_ageint is not null
					and 	y.cd_agenda = a.cd_agenda
					)
			))
		or 	(ie_consiste_regra_lib_w = 'N')
		or 	(nr_seq_area_selec_w is null)
			);

/*Consistir area de atuacao:
- Se informada no cadastro da agenda, consiste somente a area do cadastro;
- Se nao informado no cadastro, consiste a area de atuacao do cadastro do medico*/			
Cursor C03 is
	select	a.nr_seq_area_atuacao
	from	medico_area_atuacao a
	where	a.cd_pessoa_fisica = cd_medico_item_w
	and		nvl(nr_seq_area_atuacao_w,0) <> 0
	and		nvl(nr_seq_area_atuacao_w,0) = a.nr_seq_area_atuacao
	and		nvl(nr_seq_atuacao_agenda_w,0) = 0
	and		a.nr_seq_area_atuacao is not null
	order by 1;			

begin

ie_estab_usuario_w	:= nvl(Obter_Valor_Param_Usuario(869, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'S');
ie_consiste_regra_lib_w	:= nvl(Obter_Valor_Param_Usuario(869, 164, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'N');

ageint_excluir_medico_item(nr_seq_ageint_p, nm_usuario_p);

/*update	agenda_integrada_item
set	cd_medico	= null
where	nr_sequencia	= nr_seq_item_p;*/

select	cd_pessoa_fisica,
		cd_estabelecimento
into	cd_paciente_w,
		cd_estabelecimento_w
from	agenda_integrada
where	nr_sequencia	= nr_seq_Ageint_p;

if	(cd_paciente_w is not null) then
	select	max(ie_Sexo),
			max(dt_nascimento)
	into	ie_Sexo_w,
			dt_nascimento_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_paciente_w;
end if;

qt_idade_w	:= obter_idade(dt_nascimento_w, sysdate, 'A');


open C01;
loop
fetch C01 into
	nr_seq_item_w,
	cd_especialidade_w,
	nr_Seq_grupo_selec_w,
	nr_seq_area_atuacao_w;
exit when C01%notfound;
	begin
	
	if	(nr_Seq_grupo_selec_w	is not null)then
		select	max(x.nr_seq_area)
		into	nr_seq_area_selec_w
		from	agenda_int_grupo x
		where	x.nr_sequencia	= nr_Seq_grupo_selec_w;
	else
		nr_seq_area_selec_w	:= null;
	end if;	

	open C02;
	loop
	fetch C02 into
		cd_medico_item_w,
		cd_estab_agenda_w,
		nr_seq_atuacao_agenda_w;
	exit when C02%notfound;
		begin
		ie_perm_agenda_w	:= 'S';
		if	(ie_estab_usuario_w	= 'S') or
			(ie_estab_usuario_w	= 'C') then
			if	(cd_estab_agenda_w	<> cd_estabelecimento_p) then
				ie_perm_agenda_w	:= 'N';
			end if;
		elsif	(ie_estab_usuario_w	= 'N') then
			select	count(*)
			into	qt_estab_user_w
			from	usuario_estabelecimento
			where	nm_usuario_param	= nm_usuario_p
			and	cd_estabelecimento	= cd_estab_agenda_w;
			if	(qt_estab_user_w	= 0) then
				ie_perm_agenda_w	:= 'N';
			end if;
		end if;
		
		/* 	- Consistir a area de atuacao do cadastro da agenda
			- Consistir a area de atuacao dos profissionais liberados para a agenda(Cadastro Medico > Relacionamentos > Area atuacao)
		*/
		if	(nr_seq_atuacao_agenda_w <> 0) and 
			(nr_seq_area_atuacao_w <> 0)then
			if	(nr_seq_atuacao_agenda_w <> nr_seq_area_atuacao_w)then
				ie_perm_agenda_w := 'N';
			end if;
		else			
			open C03;
			loop
			fetch C03 into	
				nr_seq_area_atuacao_ww;
			exit when C03%notfound;
				begin				
				
				if	(nr_seq_area_atuacao_ww is not null) and
					(nr_seq_area_atuacao_ww = nr_seq_area_atuacao_w) then
					ie_perm_agenda_w := 'S';
				else
					ie_perm_agenda_w := 'N';
				end if;				
				
				/*insert into log_agenda (	
					ds_dados)
				values(	cd_medico_item_w || ' - ' || nr_seq_area_atuacao_ww || ' - ' || nr_seq_area_atuacao_w || ' - ' || ie_perm_agenda_w);
				commit;
				*/
				end;
			end loop;
			close C03;
		end if;		
		
		if	(ie_perm_agenda_w	= 'S') then
			insert into ageint_medico_item
				(nr_sequencia,
				nr_seq_item,
				cd_pessoa_fisica,
				nm_usuario,
				dt_Atualizacao,
				nm_usuario_nrec,
				dt_Atualizacao_nrec)
			values
				(ageint_medico_item_seq.nextval,
				nr_seq_item_w,
				cd_medico_item_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate);
		end if;

		end;
	end loop;
	close C02;

	end;
end loop;
close C01;


commit;

end Ageint_Gerar_Medico_Cons_Item;
/
