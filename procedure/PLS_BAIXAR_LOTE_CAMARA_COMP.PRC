create or replace
procedure pls_baixar_lote_camara_comp(	nr_seq_lote_p		pls_lote_camara_comp.nr_sequencia%type,
					ie_acao_p		varchar2,
					dt_baixa_p		titulo_receber.dt_emissao%type,
					ie_processo_p		pls_parametros_camara.ie_processo_camara%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is

nr_titulo_receber_w		number(10);
nr_titulo_pagar_w		number(10);
cd_estabelecimento_w		number(4);
vl_saldo_titulo_w		number(15,2);
cd_tipo_recebimento_w		number(5);
nr_seq_trans_fin_rec_w		number(10);
nr_seq_trans_fin_cpa_w		number(10);
cd_tipo_baixa_w			number(5);
vl_saldo_camara_w		number(15,2)	:= 0;
vl_receber_w			number(15,2)	:= 0;
vl_pagar_w			number(15,2)	:= 0;
nr_novo_titulo_w		number(10);
nr_seq_camara_w			number(10);
nr_seq_periodo_w		number(10);
dt_saldo_devedor_w		date;
dt_saldo_credor_w		date;
cd_moeda_cp_w			number(5);
cd_tipo_taxa_juro_cp_w		number(10);
cd_tipo_taxa_multa_cp_w		number(10);
tx_juros_cp_w			number(7,4);
tx_multa_cp_w			number(7,4);
cd_moeda_cr_w			number(5);
cd_tipo_taxa_juro_cr_w		number(10);
cd_tipo_taxa_multa_cr_w		number(10);
tx_juros_cr_w			number(7,4);
tx_multa_cr_w			number(7,4);
cd_cgc_w			varchar2(14);
cd_tipo_portador_w		number(10);
cd_portador_w			number(10);
tx_administrativa_w		number(7,4);
vl_titulo_taxa_w		number(15,2);
vl_receber_camara_w		number(15,2);
cd_conta_financ_cp_w		number(10);
cd_conta_financ_cr_w		number(10);
nr_seq_classif_w		number(10);
nr_sequencia_w			number(10);
nr_seq_trans_tit_receber_w	number(10);
nr_seq_trans_tit_pagar_w	number(10);
nr_seq_trans_tit_taxa_w		number(10);
nr_seq_baixa_w			number(10);
nr_seq_trans_baixa_taxa_w	number(10);
nr_titulo_taxa_w		number(10);
nr_seq_trans_baixa_pagar_w	number(10);
vl_desc_benefic_custo_w		number(15,2);
qt_beneficiarios_w		number(10);
ie_titulo_zerado_w		varchar2(1);
nr_seq_trans_baixa_tp_w		number(15);
nr_seq_trans_baixa_tr_w		number(15);
dt_baixa_w			titulo_receber.dt_emissao%type;
ie_saldo_w			varchar2(255);
ie_incidencia_taxa_w		pls_camara_compensacao.ie_incidencia_taxa%type;
dt_baixa_taxa_w			date;
dt_saldo_dev_taxa_w		date;
--vl_nr_titulo_w			number(15,2);
--vl_nr_titulo_ndc_w			number(15,2);
ie_tipo_base_taxa_w		pls_camara_compensacao.ie_tipo_base_taxa%type := 'F';	
ie_data_baixa_reg_caixa_w	pls_camara_compensacao.ie_data_baixa_reg_caixa%type;	
dt_repasse_w			pls_camara_calend_periodo.dt_repasse%type;
vl_taxa_fixa_w			pls_lote_camara_comp.vl_taxa_fixa%type;

Cursor c01 is
	select	a.nr_sequencia,
		a.nr_titulo_receber nr_titulo_receber,
		null nr_titulo_pagar,
		b.vl_saldo_titulo
	from	titulo_receber b,
		pls_titulo_lote_camara a
	where	a.nr_titulo_receber	= b.nr_titulo
	and	a.nr_seq_lote_camara	= nr_seq_lote_p
	union all
	select	a.nr_sequencia,
		null nr_titulo_receber,
		a.nr_titulo_pagar nr_titulo_pagar,
		b.vl_saldo_titulo
	from	titulo_pagar b,
		pls_titulo_lote_camara a
	where	a.nr_titulo_pagar	= b.nr_titulo
	and	a.nr_seq_lote_camara	= nr_seq_lote_p
	order by
		1,2;


begin
if	(nr_seq_lote_p is not null) then
	-- CO - Regime de compet�ncia
	-- CA - Regime de caixa
	dt_baixa_w := trunc(nvl(dt_baixa_p,sysdate));
	if	(ie_processo_p = 'CA') then
		ie_saldo_w := pls_obter_dados_camara_comp(nr_seq_lote_p,'DI');
	end if;
	
	select	a.cd_estabelecimento,
		a.nr_seq_periodo,
		a.nr_seq_camara,
		nvl(a.tx_administrativa,0),
		nvl(vl_desc_benefic_custo,0),
		nvl(vl_taxa_fixa,0)
	into	cd_estabelecimento_w,
		nr_seq_periodo_w,
		nr_seq_camara_w,
		tx_administrativa_w,
		vl_desc_benefic_custo_w,
		vl_taxa_fixa_w
	from	pls_lote_camara_comp	a
	where	a.nr_sequencia		= nr_seq_lote_p;
	
	--caso a tx_administrativa seja zero tenta pegar direto da camara de compensacao
	if	(tx_administrativa_w = 0) and
		(nr_seq_camara_w is not null) then
		select	nvl(tx_administrativa, 0)
		into	tx_administrativa_w
		from	pls_camara_compensacao
		where	nr_sequencia = nr_seq_camara_w;
	end if;
	
	select	max(b.cd_cgc)
	into	cd_cgc_w
	from	pls_congenere b,
		pls_camara_compensacao a
	where	a.nr_seq_coop_resp	= b.nr_sequencia
	and	a.nr_sequencia		= nr_seq_camara_w;
	
	if	(cd_cgc_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(180019);
	end if;
	
	select	max(ie_incidencia_taxa),
		nvl(max(ie_tipo_base_taxa), 'F'), -- Padr�o F - Faturamento
		max(ie_data_baixa_reg_caixa)
	into	ie_incidencia_taxa_w,
		ie_tipo_base_taxa_w,
		ie_data_baixa_reg_caixa_w
	from	pls_camara_compensacao
	where	nr_sequencia		= nr_seq_camara_w;
	
	ie_incidencia_taxa_w	:= nvl(ie_incidencia_taxa_w,'T');
	
	select	max(a.dt_saldo_devedor),
		max(a.dt_saldo_credor)
	into	dt_saldo_devedor_w,
		dt_saldo_credor_w
	from	pls_lote_camara_comp a
	where	a.nr_sequencia	= nr_seq_lote_p;
	
	-- Caso n�o tenha informado no lote busca conforme o per�odo
	if	(dt_saldo_devedor_w is null) then
		select	max(a.dt_saldo_devedor)
		into	dt_saldo_devedor_w
		from	pls_camara_calend_periodo a
		where	a.nr_sequencia	= nr_seq_periodo_w;
		
		if	(dt_saldo_devedor_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(180020);
		end if;
	end if;
	
	-- Caso n�o tenha informado no lote busca conforme o per�odo
	if	(dt_saldo_credor_w is null) then
		select	max(a.dt_saldo_credor)
		into	dt_saldo_credor_w
		from	pls_camara_calend_periodo a
		where	a.nr_sequencia	= nr_seq_periodo_w;
		
		if	(dt_saldo_credor_w is null) then
			wheb_mensagem_pck.exibir_mensagem_abort(180021);
		end if;
	end if;
	
	-- Caso n�o tenha informado no lote busca conforme o per�odo
	if	(ie_data_baixa_reg_caixa_w = 'DR') and
		(ie_processo_p = 'CA') then
		select max(a.dt_repasse)
		into	dt_repasse_w
		from	pls_camara_calend_periodo a
		where	a.nr_sequencia	= nr_seq_periodo_w;
		
		if	(dt_repasse_w is not null) then
			dt_saldo_credor_w	:= dt_repasse_w;
		else
			wheb_mensagem_pck.exibir_mensagem_abort(1056231);
		end if;
	end if;	
	
	-- Efetuar consistencias de data para o regime de caixa
	if	(ie_processo_p = 'CA') then
		if	(ie_saldo_w = 'D') then -- Gerar t�tulo a pagar
			if	(trunc(dt_saldo_devedor_w) < dt_baixa_w) then
				dt_baixa_w := trunc(dt_saldo_devedor_w);
				
			elsif	(trunc(dt_saldo_devedor_w) > dt_baixa_w) then
				-- A data do saldo devedor da c�mara para efetuar o processo � no dia #@DT_SALDO#@, e n�o no dia #@DT_BAIXA#@.
				wheb_mensagem_pck.exibir_mensagem_abort(335165,'DT_SALDO=' || to_char(dt_saldo_devedor_w,'dd/mm/yyyy') || 
										';DT_BAIXA=' || to_char(dt_baixa_w,'dd/mm/yyyy'));
			end if;
		elsif	(ie_saldo_w = 'C') then -- Gerar t�tulo a receber
			if	(trunc(dt_saldo_credor_w) < dt_baixa_w) then
				dt_baixa_w := trunc(dt_saldo_credor_w);			
				
			elsif	(trunc(dt_saldo_credor_w) > dt_baixa_w) then
				-- A data do saldo credor da c�mara para efetuar o processo � no dia #@DT_SALDO#@, e n�o no dia #@DT_BAIXA#@.
				wheb_mensagem_pck.exibir_mensagem_abort(335166,'DT_SALDO=' || to_char(dt_saldo_credor_w,'dd/mm/yyyy') || 
										';DT_BAIXA=' || to_char(dt_baixa_w,'dd/mm/yyyy'));
			end if;
		end if;
	end if;	
	
	dt_baixa_taxa_w := dt_baixa_w;
	dt_saldo_dev_taxa_w := dt_saldo_devedor_w;
	
	if	(trunc(dt_saldo_dev_taxa_w) < dt_baixa_taxa_w) then
		dt_baixa_taxa_w := dt_saldo_dev_taxa_w;		
	end if;
	
	begin
	select	a.cd_tipo_recebimento,
		a.cd_tipo_baixa,
		a.nr_seq_trans_fin_baixa,
		a.nr_seq_trans_baixa_cpa,
		a.nr_seq_trans_tit_receber,
		a.nr_seq_trans_tit_pagar,
		a.nr_seq_trans_tit_taxa,
		a.nr_seq_trans_baixa_taxa,
		a.nr_seq_trans_baixa_tp,
		a.nr_seq_trans_baixa_tr
	into	cd_tipo_recebimento_w,
		cd_tipo_baixa_w,
		nr_seq_trans_fin_rec_w,
		nr_seq_trans_fin_cpa_w,
		nr_seq_trans_tit_receber_w,
		nr_seq_trans_tit_pagar_w,
		nr_seq_trans_tit_taxa_w,
		nr_seq_trans_baixa_taxa_w,
		nr_seq_trans_baixa_tp_w,
		nr_seq_trans_baixa_tr_w
	from	pls_parametros_camara a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;
	exception
		when no_data_found then
		wheb_mensagem_pck.exibir_mensagem_abort(180022);
	end;
	
	begin
	select	a.cd_moeda_padrao,
		a.cd_tipo_taxa_juro,
		a.cd_tipo_taxa_multa,
		a.pr_juro_padrao,
		a.pr_multa_padrao
	into	cd_moeda_cp_w,
		cd_tipo_taxa_juro_cp_w,
		cd_tipo_taxa_multa_cp_w,
		tx_juros_cp_w,
		tx_multa_cp_w
	from	parametros_contas_pagar a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;
	exception
		when no_data_found then
		wheb_mensagem_pck.exibir_mensagem_abort(180023);
	end;
	
	begin
	select	a.cd_moeda_padrao,
		a.cd_tipo_taxa_juro,
		a.cd_tipo_taxa_multa,
		a.pr_juro_padrao,
		a.pr_multa_padrao,
		a.cd_tipo_portador,
		a.cd_portador
	into	cd_moeda_cr_w,
		cd_tipo_taxa_juro_cr_w,
		cd_tipo_taxa_multa_cr_w,
		tx_juros_cr_w,
		tx_multa_cr_w,
		cd_tipo_portador_w,
		cd_portador_w
	from	parametro_contas_receber a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;
	exception
		when no_data_found then
		wheb_mensagem_pck.exibir_mensagem_abort(180024);
	end;
	
	-- Baixar
	if	(ie_acao_p = 'B') then
		
		select	nvl(sum(t.vl_saldo_titulo),0)
		into	vl_receber_camara_w
		from (	select	nvl(sum(b.vl_saldo_titulo),0) vl_saldo_titulo
			from	titulo_receber 		b,
				pls_titulo_lote_camara	a
			where	a.nr_titulo_receber	= b.nr_titulo
			and	a.nr_seq_lote_camara	= nr_seq_lote_p
			and	b.ie_origem_titulo	= '13' -- Origem OPS - Faturamento
			union all
			-- quando considerar constesta��es, SOMENTE pode incluir os titulo a receber, de A550 de tipo FATURAMENTO e pagamento PARCIAL
			select	nvl(sum(b.vl_saldo_titulo),0) vl_saldo_titulo
			from	titulo_receber 		b,
				pls_titulo_lote_camara	a,
				pls_lote_discussao	c,
				pls_lote_contestacao	d
			where	a.nr_titulo_receber	= b.nr_titulo
			and	c.nr_titulo_receber	= b.nr_titulo
			and	d.nr_sequencia		= c.nr_seq_lote_contest
			and	a.nr_seq_lote_camara	= nr_seq_lote_p
			and	b.ie_origem_titulo	='11' -- [11] - OPS - Contesta��es e Recursos de Glosa, [13] - OPS - Faturamento
			and	d.ie_envio_recebimento	= 'R' -- Tipo Faturamento
			and	d.ie_processo_pgto	= 'P' -- SOMENTE PARCIAIS
			and	ie_tipo_base_taxa_w	= 'FC') t;
		
		if	(vl_receber_camara_w > 0) then
			-- Gerar t�tulo a pagar da taxa administrativa
			if	(vl_desc_benefic_custo_w > 0) then
				select	count(distinct a.nr_seq_segurado)
				into	qt_beneficiarios_w
				from	titulo_receber		d,
					pls_titulo_lote_camara	c,
					ptu_fatura		b,
					pls_conta		a
				where	c.nr_seq_lote_camara	= nr_seq_lote_p
				and	a.nr_seq_fatura		= b.nr_sequencia
				and	d.nr_seq_ptu_fatura	= b.nr_sequencia
				and	c.nr_titulo_receber	= d.nr_titulo;
				
				if	(nvl(qt_beneficiarios_w,0) = 0) then
					select	count(distinct w.nr_seq_segurado)
					into	qt_beneficiarios_w
					from	pls_fatura_conta	w,
						pls_fatura_evento	z,
						pls_fatura		x,
						pls_titulo_lote_camara	y
					where	y.nr_titulo_receber	= x.nr_titulo
					and	x.nr_sequencia		= z.nr_seq_fatura
					and	z.nr_sequencia		= w.nr_seq_fatura_evento
					and	y.nr_seq_lote_camara	= nr_seq_lote_p;
				end if;
				
				vl_titulo_taxa_w	:= vl_desc_benefic_custo_w * qt_beneficiarios_w;
			else
				
				if	(ie_incidencia_taxa_w = 'N') then
					select	nvl(sum(c.vl_total_ndc),0)
					into	vl_receber_camara_w
					from	pls_fatura		c,
						titulo_receber 		b,
						pls_titulo_lote_camara	a
					where	a.nr_titulo_receber	= b.nr_titulo
					and	c.nr_titulo		= b.nr_titulo
					and	a.nr_seq_lote_camara	= nr_seq_lote_p;
				elsif	(ie_incidencia_taxa_w = 'F') then
					select	nvl(sum(c.vl_fatura),0)
					into	vl_receber_camara_w
					from	pls_fatura		c,
						titulo_receber 		b,
						pls_titulo_lote_camara	a
					where	a.nr_titulo_receber	= b.nr_titulo
					and	c.nr_titulo		= b.nr_titulo
					and	a.nr_seq_lote_camara	= nr_seq_lote_p;
				end if;
				
				vl_titulo_taxa_w	:= vl_receber_camara_w * (tx_administrativa_w/100);
			end if;
			
			-- Caso a operadora saiba o valor da taxa, pode informar fixo o valor
			if	(vl_taxa_fixa_w > 0) then
				vl_titulo_taxa_w := vl_taxa_fixa_w;
			end if;
			
			-- Tratamento realizado na OS 420128 - William Carlos Bernardino
			select	nvl(max(ie_titulo_zerado),'S')
			into	ie_titulo_zerado_w
			from 	parametros_contas_pagar
			where	cd_estabelecimento	= cd_estabelecimento_w;
			
			-- Seja permitido criar t�tulo a pagar zerado ou o valor t�tulo seja maior que zero
			if	(ie_titulo_zerado_w = 'S') or
				(vl_titulo_taxa_w > 0) then	
				select	titulo_pagar_seq.nextval
				into	nr_titulo_taxa_w
				from	dual;
				
				insert into titulo_pagar
					(nr_titulo,
					nm_usuario,
					dt_atualizacao,
					cd_estabelecimento,
					vl_titulo,
					vl_saldo_titulo,
					dt_emissao,
					dt_contabil,
					dt_vencimento_original,
					dt_vencimento_atual,
					vl_saldo_juros,
					vl_saldo_multa,
					cd_moeda,
					cd_tipo_taxa_juro,
					cd_tipo_taxa_multa,
					tx_juros,
					tx_multa,
					ie_origem_titulo,
					ie_tipo_titulo,
					ie_situacao,
					cd_pessoa_fisica,
					cd_cgc,
					ie_pls,
					nr_lote_contabil,
					ds_observacao_titulo,
					nr_seq_trans_fin_contab,
					nr_seq_trans_fin_baixa)
				values	(nr_titulo_taxa_w,
					nm_usuario_p,
					sysdate,
					cd_estabelecimento_w,
					vl_titulo_taxa_w,
					vl_titulo_taxa_w,
					dt_baixa_taxa_w,
					dt_baixa_taxa_w,
					dt_saldo_dev_taxa_w,
					dt_saldo_dev_taxa_w,
					0,
					0,
					cd_moeda_cp_w,
					cd_tipo_taxa_juro_cp_w,
					cd_tipo_taxa_multa_cp_w,
					tx_juros_cp_w,
					tx_multa_cp_w,
					'15', -- OPS - C�mara de compensa��o
					'23', -- Fatura
					'A',
					null,
					cd_cgc_w,
					'S',
					0,
					'T�tulo gerado para pagar a taxa administrativa da C�mara de Compensa��o',
					nr_seq_trans_tit_taxa_w,
					nr_seq_trans_baixa_tp_w);
					
				atualizar_inclusao_tit_pagar(nr_titulo_taxa_w, nm_usuario_p);
				
				pls_obter_conta_financ_regra(	'CCP',		null,		cd_estabelecimento_w,
								null,		null,		nr_seq_camara_w,
								null,		null,		null,
								null,		null,		null,
								null,		null,		null,
								null,		null,		cd_conta_financ_cp_w);
								
				if	(cd_conta_financ_cp_w is not null) then
					select	nvl(max(nr_sequencia),0) + 1
					into	nr_seq_classif_w
					from	titulo_pagar_classif
					where	nr_titulo	= nr_titulo_taxa_w;
					
					insert into titulo_pagar_classif
						(nr_titulo,
						nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						nr_seq_conta_financ,
						vl_original,
						vl_titulo,
						vl_desconto,
						vl_acrescimo)
					values	(nr_titulo_taxa_w,
						nr_seq_classif_w,
						nm_usuario_p,
						sysdate,
						cd_conta_financ_cp_w,
						vl_titulo_taxa_w,
						vl_titulo_taxa_w,
						0,
						0);
				end if;
					
				update	pls_lote_camara_comp
				set	nr_tit_pagar_taxa	= nr_titulo_taxa_w,
					tx_administrativa	= tx_administrativa_w
				where	nr_sequencia		= nr_seq_lote_p;
				
				-- Inserir o t�tulo a pagar no lote
				insert into pls_titulo_lote_camara
					(nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nr_seq_lote_camara,
					nr_titulo_pagar,
					ie_tipo_inclusao)
				values	(pls_titulo_lote_camara_seq.nextval,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nr_seq_lote_p,
					nr_titulo_taxa_w,
					'A');
			end if;
		end if;
		
		-- Baixar t�tulos do lote
		open c01;
		loop
		fetch c01 into
			nr_sequencia_w,
			nr_titulo_receber_w,
			nr_titulo_pagar_w,
			vl_saldo_titulo_w;
		exit when c01%notfound;
			begin
			if	(nr_titulo_receber_w is not null) then
				dbms_application_info.SET_ACTION('PLS_BAIXAR_LOTE_CAMARA_COMP');
				baixa_titulo_receber(cd_estabelecimento_w,
						cd_tipo_recebimento_w,
						nr_titulo_receber_w,
						nr_seq_trans_fin_rec_w,
						vl_saldo_titulo_w,
						dt_baixa_w,
						nm_usuario_p,
						null,
						null,
						null,
						0,
						0);
						
				select	max(a.nr_sequencia)
				into	nr_seq_baixa_w
				from	titulo_receber_liq a
				where	a.nr_titulo	= nr_titulo_receber_w;
				
				update	titulo_receber_liq
				set	nr_seq_pls_lote_camara	= nr_seq_lote_p,
					ie_origem_baixa		= 'CC'
				where	nr_titulo		= nr_titulo_receber_w
				and	nr_sequencia		= nr_seq_baixa_w;
						
				update	pls_titulo_lote_camara
				set	vl_baixado	= vl_saldo_titulo_w
				where	nr_sequencia	= nr_sequencia_w;
						
				atualizar_saldo_tit_rec(nr_titulo_receber_w,nm_usuario_p);
						
				vl_receber_w	:= vl_receber_w + vl_saldo_titulo_w;
			elsif	(nr_titulo_pagar_w is not null) then
				if	(nr_titulo_taxa_w = nr_titulo_pagar_w) then
					nr_seq_trans_baixa_pagar_w	:= nr_seq_trans_baixa_taxa_w;
				else
					nr_seq_trans_baixa_pagar_w	:= nr_seq_trans_fin_cpa_w;
				end if;
				dbms_application_info.SET_ACTION('PLS_BAIXAR_LOTE_CAMARA_COMP');
				baixa_titulo_pagar(cd_estabelecimento_w,
						cd_tipo_baixa_w,
						nr_titulo_pagar_w,
						vl_saldo_titulo_w,
						nm_usuario_p,
						nr_seq_trans_baixa_pagar_w,
						null,
						null,
						dt_baixa_w,
						null);
						
				select	max(a.nr_sequencia)
				into	nr_seq_baixa_w
				from	titulo_pagar_baixa a
				where	a.nr_titulo	= nr_titulo_pagar_w;
				
				update	titulo_pagar_baixa
				set	nr_seq_pls_lote_camara	= nr_seq_lote_p
				where	nr_titulo		= nr_titulo_pagar_w
				and	nr_sequencia		= nr_seq_baixa_w;
						
				update	pls_titulo_lote_camara
				set	vl_baixado	= vl_saldo_titulo_w
				where	nr_sequencia	= nr_sequencia_w;
						
				atualizar_saldo_tit_pagar(nr_titulo_pagar_w,nm_usuario_p);
				gerar_w_tit_pag_imposto(nr_titulo_pagar_w, nm_usuario_p);
				
				vl_pagar_w	:= vl_pagar_w + vl_saldo_titulo_w;
			end if;
			end;
		end loop;
		close c01;
		
		-- Gerar novo t�tulo a pagar ou receber
		vl_saldo_camara_w	:= vl_receber_w - vl_pagar_w;
		
		if	(vl_saldo_camara_w < 0) then
			-- Gerar t�tulo a pagar
			select	titulo_pagar_seq.nextval
			into	nr_novo_titulo_w
			from	dual;
			
			insert into titulo_pagar
				(nr_titulo,
				nm_usuario,
				dt_atualizacao,
				cd_estabelecimento,
				vl_titulo,
				vl_saldo_titulo,
				dt_emissao,
				dt_contabil,
				dt_vencimento_original,
				dt_vencimento_atual,
				vl_saldo_juros,
				vl_saldo_multa,
				cd_moeda,
				cd_tipo_taxa_juro,
				cd_tipo_taxa_multa,
				tx_juros,
				tx_multa,
				ie_origem_titulo,
				ie_tipo_titulo,
				ie_situacao,
				cd_pessoa_fisica,
				cd_cgc,
				ie_pls,
				nr_lote_contabil,
				nr_seq_pls_lote_camara,
				ds_observacao_titulo,
				nr_seq_trans_fin_contab,
				nr_seq_trans_fin_baixa)
			values	(nr_novo_titulo_w,
				nm_usuario_p,
				sysdate,
				cd_estabelecimento_w,
				abs(vl_saldo_camara_w),
				abs(vl_saldo_camara_w),
				dt_baixa_w,
				dt_baixa_w,
				dt_saldo_devedor_w,
				dt_saldo_devedor_w,
				0,
				0,
				cd_moeda_cp_w,
				cd_tipo_taxa_juro_cp_w,
				cd_tipo_taxa_multa_cp_w,
				tx_juros_cp_w,
				tx_multa_cp_w,
				'15', -- OPS - C�mara de compensa��o
				'23', -- Fatura
				'A',
				null,
				cd_cgc_w,
				'S',
				0,
				nr_seq_lote_p,
				'T�tulo gerado pela C�mara de Compensa��o',
				nr_seq_trans_tit_pagar_w,
				nr_seq_trans_baixa_tp_w);
				
			atualizar_inclusao_tit_pagar(nr_novo_titulo_w, nm_usuario_p);
			
			pls_obter_conta_financ_regra(	'CCP',		null,		cd_estabelecimento_w,
							null,		null,		nr_seq_camara_w,
							null,		null,		null,
							null,		null,		null,
							null,		null,		null,
							null,		null,		cd_conta_financ_cp_w);
							
			if	(cd_conta_financ_cp_w is not null) then
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_seq_classif_w
				from	titulo_pagar_classif
				where	nr_titulo	= nr_novo_titulo_w;
				
				insert into titulo_pagar_classif
					(nr_titulo,
					nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nr_seq_conta_financ,
					vl_original,
					vl_titulo,
					vl_desconto,
					vl_acrescimo)
				values	(nr_novo_titulo_w,
					nr_seq_classif_w,
					nm_usuario_p,
					sysdate,
					cd_conta_financ_cp_w,
					vl_saldo_camara_w,
					vl_saldo_camara_w,
					0,
					0);
			end if;
			-- Inserir classifica��o
		elsif	(vl_saldo_camara_w > 0) then
			-- Gerar t�tulo a receber
			select	titulo_seq.nextval
			into	nr_novo_titulo_w
			from	dual;
			
			insert	into	titulo_receber
				(nr_titulo,
				nm_usuario,
				dt_atualizacao,
				cd_estabelecimento,
				cd_tipo_portador,
				cd_portador,
				dt_emissao,
				dt_contabil,
				dt_vencimento,
				dt_pagamento_previsto,
				vl_titulo,
				vl_saldo_titulo,
				vl_saldo_juros,
				vl_saldo_multa,
				tx_juros,
				tx_multa,
				cd_tipo_taxa_juro,
				cd_tipo_taxa_multa,
				tx_desc_antecipacao,
				ie_tipo_titulo,
				ie_tipo_inclusao,
				ie_origem_titulo,
				cd_moeda,
				ie_situacao,
				cd_pessoa_fisica,
				cd_cgc,
				ie_tipo_emissao_titulo,
				nr_seq_pls_lote_camara,
				nr_lote_contabil,
				ds_observacao_titulo,
				nr_seq_trans_fin_contab,
				nr_seq_trans_fin_baixa)
			values	(nr_novo_titulo_w,
				nm_usuario_p,
				sysdate,
				cd_estabelecimento_w,
				cd_tipo_portador_w,
				cd_portador_w,
				dt_baixa_w,
				dt_baixa_w,
				dt_saldo_credor_w,
				dt_saldo_credor_w,
				vl_saldo_camara_w,
				vl_saldo_camara_w,
				0,
				0,
				0,
				0,
				cd_tipo_taxa_juro_cr_w,
				cd_tipo_taxa_multa_cr_w,
				0,
				'1', -- Bloqueto
				'2',
				'6', -- C�mara de compensa��o
				cd_moeda_cr_w,
				'1',
				null,
				cd_cgc_w,
				'2', -- Emiss�o bloqueto origem
				nr_seq_lote_p,
				0,
				'T�tulo gerado pela C�mara de Compensa��o',
				nr_seq_trans_tit_receber_w,
				nr_seq_trans_baixa_tr_w);
				
			pls_obter_conta_financ_regra(	'CCR',		null,		cd_estabelecimento_w,
							null,		null,		nr_seq_camara_w,
							null,		null,		null,
							null,		null,		null,
							null,		null,		null,
							null,		null,		cd_conta_financ_cr_w);
							
			if	(cd_conta_financ_cr_w is not null) then
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_seq_classif_w
				from	titulo_receber_classif
				where	nr_titulo	= nr_novo_titulo_w;
				
				insert into titulo_receber_classif
					(nr_titulo,
					nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					cd_conta_financ,
					vl_original,
					vl_classificacao,
					vl_desconto)
				values	(nr_novo_titulo_w,
					nr_seq_classif_w,
					nm_usuario_p,
					sysdate,
					cd_conta_financ_cr_w,
					vl_saldo_camara_w,
					vl_saldo_camara_w,
					0);
			end if;
			-- Inserir classifica��o
		end if;
	end if;
	
	update	pls_lote_camara_comp
	set	dt_baixa	= dt_baixa_w,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_lote_p;
end if;

commit;

end pls_baixar_lote_camara_comp;
/