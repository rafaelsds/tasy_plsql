create or replace
procedure calcular_min_dur_agenda_cir	(cd_estabelecimento_p	number,
					nr_seq_agenda_p		number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2,
					ie_ajustar_min_p out	varchar2,
					ds_ajustar_min_p out	varchar2) is

/* variaveis */
cd_agenda_w		number(10,0);
dt_agenda_w		date;
cd_medico_exec_w		varchar2(10);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
nr_minuto_duracao_w	number(10,0);
hr_inicio_w		date;
hr_prox_agenda_w	date;

ie_ajustar_min_w		varchar2(1) 	:= 'N';
ds_ajustar_min_w		varchar2(255)	:= null;
qt_min_proc_w		number(10,0) 	:= 0;
qt_min_agenda_w		number(10,0) 	:= 0;

ie_param_sobreposicao_w	varchar2(1);
ie_sobreposicao_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_proc_adic_w	varchar2(10);
ie_atualiza_tempo_agenda_w		varchar2(1);
ie_atualiza_tempo_proc_w		varchar2(1);
ie_atual_maior_tempo_proc_w		varchar2(1); 
ie_modo_sobreposicao_w	varchar2(1); 
cd_estab_agenda_w	number(4);
ie_estab_agenda_w	varchar2(1) := 'S';
ds_lista_status_w varchar2(255)	:= null;

expressao1_w	varchar2(255) := substr(obter_desc_expressao_idioma(773926, null, wheb_usuario_pck.get_nr_seq_idioma),1,255);
expressao2_w	varchar2(255) := substr(obter_desc_expressao_idioma(773939, null, wheb_usuario_pck.get_nr_seq_idioma),1,255);
expressao3_w	varchar2(255) := substr(obter_desc_expressao_idioma(773940, null, wheb_usuario_pck.get_nr_seq_idioma),1,255);
 		
/* obter exame agenda */
cursor c01 is
select	cd_agenda,
	hr_inicio,
	cd_medico,
	cd_procedimento,
	ie_origem_proced,
	nr_seq_proc_interno,
	nr_minuto_duracao,
	cd_pessoa_fisica
	cd_pessoa_fisica
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_p
and	nr_seq_proc_interno is not null;

/* obter exames adicionais */
cursor c02 is
select	cd_medico,
	nr_seq_proc_interno,
	qt_min_proc
from	agenda_paciente_proc
where	nr_sequencia = nr_seq_agenda_p;

begin

obter_param_usuario(871, 162, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tempo_agenda_w);
obter_param_usuario(871, 163, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tempo_proc_w); 	
obter_param_usuario(871, 642, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atual_maior_tempo_proc_w);
obter_param_usuario(871, 774, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_estab_agenda_w);
if	(wheb_usuario_pck.get_cd_funcao = 871) then /*so ira buscar o parametro se estiver na funcao Gestao da Agenda cirurgica*/
	Obter_Param_Usuario(871, 820, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ds_lista_status_w);	
end if;
	
if	(cd_estabelecimento_p is not null) and
	(nr_seq_agenda_p is not null) then
	
	begin
		select    nvl(a.ie_modo_sobreposicao,'R'),
			  nvl(a.cd_estabelecimento, 0)
		into    ie_modo_sobreposicao_w,
			cd_estab_agenda_w
		from    agenda a,
			agenda_paciente b
		where    a.cd_agenda     = b.cd_agenda
		and    b.nr_sequencia     = nr_seq_agenda_p;
		exception WHEN no_data_found then
		    ie_modo_sobreposicao_w := 'R';
		    cd_estab_agenda_w := 0;
    	end;	

	/* obter parametros */
	select	obter_parametro_agenda(cd_estabelecimento_p, 'IE_CONSISTE_DURACAO', 'N')
	into	ie_param_sobreposicao_w
	from	dual;	

	
	
	/* obter duracao exame agenda */
	open c01;
	loop
	fetch c01 into	cd_agenda_w,
			dt_agenda_w,
			cd_medico_exec_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_proc_interno_w,
			nr_minuto_duracao_w,
			cd_pessoa_fisica_w;
	exit when c01%notfound;
		begin
		
		if	(ie_atualiza_tempo_agenda_w in ('S','Q')) then		

			select	obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,decode(ie_estab_agenda_w,'S',cd_estab_agenda_w,0))
			into	qt_min_proc_w
			from	dual;
			
			qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
			
		end if;
			
		end;
	end loop;
	close c01;
	
	/* obter duracao exames adicionais */
	if	(ie_atualiza_tempo_proc_w = 'S') then
		open c02;
		loop
		fetch c02 into	cd_medico_proc_adic_w,
				nr_seq_proc_interno_w,
				qt_min_proc_w;
		exit when c02%notfound;
			begin
			if	(nvl(qt_min_proc_w,0) = 0) then
				if	(ie_estab_agenda_w = 'S') then
					qt_min_proc_w := obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,cd_estab_agenda_w);
				else
					qt_min_proc_w := obter_tempo_total_proced(nr_seq_proc_interno_w,cd_medico_exec_w,0);
				end if;
			end if;
			
			if (ie_atual_maior_tempo_proc_w = 'S') then
				if (qt_min_proc_w > qt_min_agenda_w) then
					 qt_min_agenda_w := qt_min_proc_w;
				end if;
			else
				qt_min_agenda_w	:= qt_min_agenda_w + qt_min_proc_w;
			end if;
			
			
			end;
		end loop;
		close c02;
	end if;
	
	/* ajustar duracao agenda */
	
	if	(qt_min_agenda_w > nr_minuto_duracao_w) then
	
		/* verificar sobreposicao */
		select	obter_se_sobreposicao_horario(cd_agenda_w, dt_agenda_w, qt_min_agenda_w)
		into	ie_sobreposicao_w
		from	dual;
		
		if	(ie_modo_sobreposicao_w = 'R') and
			((ie_param_sobreposicao_w = 'N') or
			 (ie_sobreposicao_w = 'N')) then
			
			ie_ajustar_min_w 		:= 'S';	
			ds_ajustar_min_w		:= null;
		
			update	agenda_paciente
			set	nr_minuto_duracao	= qt_min_agenda_w
			where	nr_sequencia		= nr_seq_agenda_p;		
			commit;
			
		elsif	((ie_modo_sobreposicao_w = 'R') and
			 (ie_param_sobreposicao_w = 'A')) and
			(ie_sobreposicao_w = 'S') then
		
			ie_ajustar_min_w 		:= 'S';	
			ds_ajustar_min_w		:= expressao1_w;
		
			update	agenda_paciente
			set	nr_minuto_duracao	= qt_min_agenda_w
			where	nr_sequencia	= nr_seq_agenda_p;		
			commit;
			
		elsif	((ie_modo_sobreposicao_w = 'R') and
			 (ie_param_sobreposicao_w = 'I')) and
			(ie_sobreposicao_w = 'S') then
		
			ie_ajustar_min_w 		:= 'N';	
			ds_ajustar_min_w		:= substr(expressao2_w || chr(10) || expressao3_w,1,255);
		elsif	(ie_modo_sobreposicao_w = 'D') and (ie_sobreposicao_w = 'S') then
			select	cd_agenda,
				hr_inicio
			into	cd_agenda_w,
				hr_inicio_w	
			from	agenda_paciente
			where	nr_sequencia = nr_seq_agenda_p;
			
			if (nvl(ds_lista_status_w,'XPTO') = 'XPTO') then
				select	min(hr_inicio)
				into	hr_prox_agenda_w
				from	agenda_paciente
				where	cd_agenda = cd_agenda_w
				and		dt_agenda = trunc(hr_inicio_w,'dd')
				and		hr_inicio > hr_inicio_w
				and		ie_status_agenda not in ('C','L','II');
			else
				select	min(hr_inicio)
				into	hr_prox_agenda_w
				from	agenda_paciente
				where	cd_agenda = cd_agenda_w
				and		dt_agenda = trunc(hr_inicio_w,'dd')
				and		hr_inicio > hr_inicio_w
				and 	obter_se_contido_char(ie_status_agenda,ds_lista_status_w || ',L') = 'N';
			end if;	
			
			update	agenda_paciente
			set	nr_minuto_duracao = Obter_Min_Entre_Datas(hr_inicio_w,hr_prox_agenda_w,null)
			where	nr_sequencia	  = nr_seq_agenda_p;
			commit;
			
		end if;
		
	end if;

end if;

ie_ajustar_min_p := ie_ajustar_min_w;
ds_ajustar_min_p := ds_ajustar_min_w;

end calcular_min_dur_agenda_cir;
/
