create or replace
procedure GERAR_USUARIO_TERCEIRO(	NR_SEQ_TERCEIRO_P	NUMBER,
					CD_ESTABELECIMENTO_P	NUMBER,
					NM_USUARIO_P VARCHAR2) is

CD_PESSOA_FISICA_W		varchar2(10);
nm_usuario_terc_w		varchar2(255);

cursor	c01 is

        select	a.cd_pessoa_fisica
        from	terceiro_pessoa_consulta a
        where	a.cd_pessoa_fisica	is not null
        and	a.nr_seq_terceiro	= nr_seq_terceiro_p;

begin

open	c01;
loop
fetch	c01 into
	cd_pessoa_fisica_w;
exit	when c01%notfound;
	insert	into usuario_terceiro
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		cd_estabelecimento,
		nr_seq_terceiro)
	values	(usuario_terceiro_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_w,
		cd_estabelecimento_p,
		nr_seq_terceiro_p);
		
end	loop;
close	c01;
commit;

end gerar_usuario_terceiro;
/