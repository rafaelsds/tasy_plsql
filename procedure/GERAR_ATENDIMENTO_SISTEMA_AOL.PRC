CREATE OR REPLACE PROCEDURE GERAR_ATENDIMENTO_SISTEMA_AOL(DS_LISTA_EXAME_P      VARCHAR2,
                                                          DS_LISTA_CONSULTA_P   VARCHAR2,
                                                          CD_PROCEDENCIA_P      NUMBER,
                                                          IE_TIPO_ATENDIMENTO_P NUMBER,
                                                          IE_CLINICA_P          NUMBER,
                                                          IE_PERMITE_VISITA_P   VARCHAR2,
                                                          IE_CARATER_SUS_P      VARCHAR2,
                                                          NM_USUARIO_P          VARCHAR2,
                                                          CD_MEDICO_RESP_P      VARCHAR2,
                                                          CD_PESSOA_RESP_P      VARCHAR2,
														  NR_SEQ_SENHA_P		Number,
                                                          NR_ATENDIMENTO_P      OUT NUMBER,
                                                          DS_ERRO_P             OUT VARCHAR2) IS
BEGIN
  INTPD_AOL_PCK.GERAR_ATENDIMENTO_SISTEMA_AOL(DS_LISTA_EXAME_P,
                                              DS_LISTA_CONSULTA_P,
                                              CD_PROCEDENCIA_P,
                                              IE_TIPO_ATENDIMENTO_P,
                                              IE_CLINICA_P,
                                              IE_PERMITE_VISITA_P,
                                              IE_CARATER_SUS_P,
                                              NM_USUARIO_P,
                                              CD_MEDICO_RESP_P,
                                              CD_PESSOA_RESP_P,
											  NR_SEQ_SENHA_P,
                                              NR_ATENDIMENTO_P,
                                              DS_ERRO_P);

END GERAR_ATENDIMENTO_SISTEMA_AOL;
/