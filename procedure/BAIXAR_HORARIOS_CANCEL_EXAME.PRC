create or replace
procedure Baixar_horarios_cancel_exame(	nr_prescricao_p		number,
					nr_seq_procedimento_p	number,
					nm_usuario_p		Varchar2,
					ds_justificativa_p	varchar2,
					ds_observacao_p		varchar2) is 

nr_seq_horario_w		number(10,0);
dt_horario_w			date;
cd_procedimento_w		procedimento.cd_procedimento%type;
nr_seq_proc_interno_w		number(10,0);
nr_seq_alteracao_w		number(10,0);
nr_atendimento_w		number(15,0);
ie_tipo_item_w			varchar2(10);

cursor c01 is
select	nr_sequencia,
	dt_horario,
	cd_procedimento,
	nr_seq_proc_interno
from	prescr_proc_hor
where	nr_prescricao		= nr_prescricao_p
and	nr_seq_procedimento 	= nr_seq_procedimento_p
and	dt_fim_horario is null
and	dt_suspensao is null;
			
begin

select	max(nr_atendimento)
into	nr_atendimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

open C01;
loop
fetch C01 into	
	nr_seq_horario_w,
	dt_horario_w,
	cd_procedimento_w,
	nr_seq_proc_interno_w;
exit when C01%notfound;

	UPDATE	prescr_proc_hor
	SET	dt_suspensao		= sysdate,
		nm_usuario_susp		= nm_usuario_p
	WHERE	nr_sequencia		= nr_seq_horario_w
	AND	dt_suspensao		IS NULL
	AND	dt_fim_horario		IS NULL;

	SELECT	prescr_mat_alteracao_seq.NEXTVAL
	INTO	nr_seq_alteracao_w
	FROM	dual;

	INSERT	INTO	prescr_mat_alteracao	
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_prescricao,
		nr_seq_procedimento,
		nr_seq_horario_proc,
		dt_alteracao,
		cd_pessoa_fisica,
		ie_alteracao,
		ie_tipo_item,
		dt_horario,
		nr_atendimento,
		cd_item,
		nr_seq_proc_interno,
		ds_observacao,
		ds_justificativa)
	VALUES	(
		nr_seq_alteracao_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_prescricao_p,
		nr_seq_procedimento_p,
		nr_seq_horario_w,
		dt_horario_w,
		obter_dados_usuario_opcao(nm_usuario_p,'C'),
		39,
		'P',
		dt_horario_w,
		nr_atendimento_w,
		cd_procedimento_w,
		nr_seq_proc_interno_w,
		ds_observacao_p,
		ds_justificativa_p);

end loop;
close C01;

commit;

end Baixar_horarios_cancel_exame;
/
