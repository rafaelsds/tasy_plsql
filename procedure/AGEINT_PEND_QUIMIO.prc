CREATE OR REPLACE PROCEDURE ageint_pend_quimio(nr_seq_agenda_p   NUMBER,
                                               lista_pend_quimio VARCHAR2) IS
    nm_usuario_w         VARCHAR2(255);
    nr_duracao_w         NUMBER(10, 0);
    grupo_quimio_w       NUMBER(10);
    tipo_grupo_quimio_w  VARCHAR2(15);
    nr_seq_pendencia_w   w_pendencia_agequi.nr_seq_pendencia%TYPE;
    nr_seq_atendimento_w paciente_atendimento.nr_seq_atendimento%TYPE;
    lista_pend_quimio_w  VARCHAR2(4000) := lista_pend_quimio || ',';
    tam_lista_w          NUMBER(10, 0);
    ie_pos_virgula_w     NUMBER(3, 0);
BEGIN

    obter_param_usuario(869, 320, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, grupo_quimio_w);

    obter_param_usuario(869,
                        321,
                        obter_perfil_ativo,
                        obter_usuario_ativo,
                        obter_estabelecimento_ativo,
                        tipo_grupo_quimio_w);

    nm_usuario_w := obter_usuario_ativo;

    WHILE (lista_pend_quimio_w IS NOT NULL) LOOP
        BEGIN
        
            ie_pos_virgula_w := instr(lista_pend_quimio_w, ',');
            tam_lista_w      := length(lista_pend_quimio_w);
        
            IF (ie_pos_virgula_w <> 0) THEN
                nr_seq_pendencia_w  := to_number(substr(lista_pend_quimio_w, 1, (ie_pos_virgula_w - 1)));
                lista_pend_quimio_w := substr(lista_pend_quimio_w, (ie_pos_virgula_w + 1), tam_lista_w);
            
                SELECT MAX(a.qt_tempo_medic)
                  INTO nr_duracao_w
                  FROM paciente_setor       a,
                       paciente_atendimento b,
                       qt_pendencia_agenda  c
                 WHERE a.nr_seq_paciente = b.nr_seq_paciente
                   AND b.nr_seq_pend_agenda = c.nr_sequencia
                   AND c.nr_sequencia = nr_seq_pendencia_w;
            
                SELECT MAX(b.nr_seq_atendimento)
                  INTO nr_seq_atendimento_w
                  FROM qt_pendencia_agenda  c,
                       paciente_atendimento b
                 WHERE c.nr_sequencia = nr_seq_pendencia_w
                   AND b.nr_seq_pend_agenda = c.nr_sequencia;
            
                INSERT INTO agenda_integrada_item
                    (nr_sequencia,
                     ie_tipo_agendamento,
                     ie_tipo_pend_agenda,
                     nr_minuto_duracao,
                     nr_seq_pend_quimio,
                     nr_seq_agenda_int,
                     dt_atualizacao,
                     nm_usuario,
                     dt_atualizacao_nrec,
                     nm_usuario_nrec,
                     nr_seq_grupo_quimio,
                     nr_seq_atendimento)
                VALUES
                    (agenda_integrada_item_seq.nextval,
                     'Q',
                     decode(tipo_grupo_quimio_w, NULL, 'Q', tipo_grupo_quimio_w),
                     nr_duracao_w,
                     nr_seq_pendencia_w,
                     nr_seq_agenda_p,
                     SYSDATE,
                     nm_usuario_w,
                     SYSDATE,
                     nm_usuario_w,
                     decode(grupo_quimio_w, NULL, NULL, grupo_quimio_w),
                     nr_seq_atendimento_w);
                COMMIT;
            ELSE
                lista_pend_quimio_w := NULL;
            END IF;
        END;
    END LOOP;

END ageint_pend_quimio;
/
