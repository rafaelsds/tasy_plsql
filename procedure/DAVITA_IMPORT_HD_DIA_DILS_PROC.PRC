CREATE OR REPLACE PROCEDURE DAVITA_IMPORT_HD_DIA_DILS_PROC IS

  ie_count_w            	NUMBER(10);
  ds_erro_w             	VARCHAR2(4000);
  cd_pessoa_fisica_w    	pessoa_fisica.cd_pessoa_fisica%TYPE;
  nm_usuario_p          	VARCHAR2(15) := 'TASY_IMP';
  ds_observacao_w       	VARCHAR2(4000) := '';

  nr_seq_dial_dializ_w   HD_DIALISE_DIALISADOR.nr_sequencia%TYPE;
  nr_seq_dialisador_w    HD_DIALISE_DIALISADOR.nr_seq_dialisador%TYPE;
  nr_seq_unid_dialise_w  HD_DIALISE_DIALISADOR.nr_seq_unid_dialise%TYPE;
  nr_seq_dialise_w       HD_DIALISE_DIALISADOR.nr_seq_dialise%TYPE;

  CURSOR C01_HD IS
    SELECT t.ROWID, t.* FROM DAVITA_IMPORT_HD_DIALISE_DIALI t
    WHERE NVL(t.IE_REG_IMP, 'N') = 'N';
    c01_w  C01_HD%ROWTYPE;

  FUNCTION getDialise(nr_seq_dialise_ant_p NUMBER, cd_estabelecimento_p NUMBER) RETURN NUMBER IS
    wReturn NUMBER(10);
    wNrSeqDialise NUMBER(10);
  BEGIN

    BEGIN
      SELECT nr_seq_dialise INTO wNrSeqDialise FROM DAVITA_IMPORT_HD_DIALISE
      WHERE nr_sequencia = nr_seq_dialise_ant_p
      AND   cd_estabelecimento = cd_estabelecimento_p;

      wReturn := wNrSeqDialise;

    EXCEPTION
      WHEN Others THEN
        wReturn := NULL;
    END;

    RETURN wReturn;
  END;

  FUNCTION getDialisador(nr_seq_dializador_ant_p NUMBER, cd_estabelecimento_p NUMBER) RETURN NUMBER IS
    wReturn NUMBER(10);
    wNrSeqDilizador NUMBER(10);
  BEGIN

    BEGIN

      SELECT nr_seq_dializador INTO wNrSeqDilizador FROM DAVITA_IMPORT_HD_DIALIZADOR
      WHERE nr_sequencia = nr_seq_dializador_ant_p
      AND   cd_estabelecimento = cd_estabelecimento_p;

      wReturn := wNrSeqDilizador;

    EXCEPTION
      WHEN Others THEN
        wReturn := NULL;
    END;

    RETURN wReturn;
  END;

BEGIN
  BEGIN
    IE_COUNT_W := 0 ;

    OPEN C01_HD;
		LOOP FETCH C01_HD INTO c01_w;
		EXIT WHEN C01_HD%NOTFOUND;

    ds_observacao_w := NULL;
    ds_erro_w := NULL;

    IE_COUNT_W := IE_COUNT_W + 1;
    IF IE_COUNT_W = 500 THEN
      COMMIT;
      IE_COUNT_W := 0;
    END IF;

    nr_seq_dialisador_w := getDialisador(c01_w.nr_seq_dialisador, c01_w.cd_estabelecimento);
    nr_seq_dialise_w := getDialise(c01_w.nr_seq_dialise, c01_w.cd_estabelecimento);
    nr_seq_unid_dialise_w := hd_obter_unidade_estab(c01_w.cd_estabelecimento);
    /*
    verificar se o NR_SEQ_DIALISADOR e NR_SEQ_DIALISE foram importados e NR_SEQ_MAQUINA nao pode ser NULL
    */

    IF nr_seq_dialisador_w IS NULL OR nr_seq_dialise_w IS NULL OR nr_seq_unid_dialise_w IS NULL OR c01_w.nr_seq_maquina IS NULL THEN
      UPDATE DAVITA_IMPORT_HD_DIALISE_DIALI
      SET ie_reg_imp = 'N', ds_erro = 'Dados obrigatórios nao cadastrados.'
      WHERE ROWID = c01_w.ROWID;
      CONTINUE;
    ELSE

      BEGIN

      SELECT HD_DIALISE_DIALISADOR_seq.NEXTVAL INTO nr_seq_dial_dializ_w FROM dual;

        INSERT INTO HD_DIALISE_DIALISADOR (
           nr_sequencia
          ,cd_estabelecimento
          ,dt_atualizacao
          ,nm_usuario
          ,dt_atualizacao_nrec
          ,nm_usuario_nrec
          ,nr_seq_dialisador
          ,nr_seq_maquina
          ,dt_montagem
          ,dt_retirada
          ,nr_seq_dialise
          ,ds_motivo_subst
          ,ie_troca_emergencia
          ,nr_seq_unid_dialise
        ) VALUES(
           nr_seq_dial_dializ_w       --nr_sequencia
          ,c01_w.cd_estabelecimento   --cd_estabelecimento
          ,SYSDATE                    --dt_atualizacao
          ,nm_usuario_p               --nm_usuario
          ,SYSDATE                    --dt_atualizacao_nrec
          ,nm_usuario_p               --nm_usuario_nrec
          ,nr_seq_dialisador_w --nr_seq_dialisador
          ,c01_w.nr_seq_maquina       --nr_seq_maquina
          ,c01_w.dt_montagem          --dt_montagem
          ,c01_w.dt_retirada          --dt_retirada
          ,nr_seq_dialise_w   --nr_seq_dialise
          ,c01_w.ds_motivo_subst      --ds_motivo_subst
          ,c01_w.ie_troca_emergencia  --ie_troca_emergencia
          ,nr_seq_unid_dialise_w      --nr_seq_unid_dialise
        );

      EXCEPTION
        WHEN Others THEN
          ds_erro_w := 'ERRO Inserir HD_DIALISE_DIALISADOR | ' || cd_pessoa_fisica_w || ' | ERRO => ' || SQLERRM;

          UPDATE DAVITA_IMPORT_HD_DIALISE_DIALI
          SET ie_reg_imp = 'N', ds_erro = ds_erro_w
          WHERE ROWID = c01_w.ROWID;
          CONTINUE;
      END;

      UPDATE DAVITA_IMPORT_HD_DIALISE_DIALI
      SET ie_reg_imp = 'S', nr_seq_diali_dializador = nr_seq_dial_dializ_w,ds_erro = ds_erro_w
      WHERE ROWID = c01_w.ROWID;

    END IF; /*Final bloco else*/
    END LOOP;
  END;
  COMMIT;
END DAVITA_IMPORT_HD_DIA_DILS_PROC;
/