create or replace
procedure gerar_titulo_receber_padrao ( dt_vencimento_p			 	in date,
										nr_seq_trans_fin_contab_p 	in number,
										cd_portador_p			 	in number,
										cd_tipo_portador_p		 	in number,
										ie_tipo_inclusao_p		 	in number,
										ie_tipo_emissao_titulo_p 	in number,	
										cd_estabelecimento_p	 	in number,
										vl_titulo_p				 	in number,
										ie_origem_titulo_p		 	in number,
										ie_tipo_titulo_p		 	in number,
										ie_pls_p				 	in varchar2,
										nr_seq_conta_banco_p	 	in number,
										nr_seq_carteira_cobr_p	 	in number,
										nm_usuario_p			 	in Varchar2,
										tx_desc_antecipacao_p		in number,
										ds_observacao_titulo_p		in varchar2,
										cd_pessoa_fisica_p			in varchar2,
										cd_cgc_p					in varchar2,
										ie_commit_p					in varchar2,
										ie_emite_bloqueto_p			in varchar2,
										nr_titulo_gerado_p			out number) is 
										
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------
				     	                 ==>	A T EN � � O	<==
		
	Muito cuidado ao utilizar essa rotina em outra fun��o diferente da Gest�o Financeira Philips.	
	
	Inicialmente essa procedure foi criada para gerar o t�tulo atrav�s da fun��o Gest�o Financeira Philips. Por�m, pode ser
	utilizada para gerar t�tulos atrav�s de outras fun��es, onde ser� necess�rio tratar os par�metros de entrada, caso ainda n�o
	existam, e sua inser��o no t�tulo.
	
	Campos obrigat�rios que inicialmente n�o est�o definidos na entrada dessa proc pq s�o obtido dos parametros do contas a receber
	ou ser�o sysdate:
	- CD_TIPO_TAXA_JURO
	- CD_TIPO_TAXA_MULTA
	- CD_MOEDA
	- DT_EMISSAO
	- DT_ATUALIZACAO
	- DT_PAGAMENTO_PREVISTO (Ser� o mesmo do dt_vencimento)
	- IE_SITUACAO (Situa��o inicial de um t�tulo � Aberto. Em situa��es at�picas, procurar o grupo Financeiro)
	- TX_JUROS
	- TX_MULTA
	- VL_SALDO_JUROS 
	- VL_SALDO_MULTA
	- VL_SALDO_TITULO (Ser� o valor do t�tulo ao ser gerado).
-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/									

cd_moeda_padrao_w			parametro_contas_receber.cd_moeda_padrao%type;
nr_seq_trans_fin_baixa_w	parametro_contas_receber.nr_seq_trans_fin_baixa%type;
cd_portador_w				parametro_contas_receber.cd_portador%type;
cd_tipo_portador_w			parametro_contas_receber.cd_tipo_portador%type;
pr_juro_padrao_w			parametro_contas_receber.pr_juro_padrao%type;
pr_multa_padrao_w			parametro_contas_receber.pr_multa_padrao%type;
cd_tipo_taxa_juro_w			parametro_contas_receber.cd_tipo_taxa_juro%type;
cd_tipo_taxa_multa_w		parametro_contas_receber.cd_tipo_taxa_multa%type;
cd_estab_financeiro_w		estabelecimento.cd_estab_financeiro%type;
nr_titulo_w					titulo_receber.nr_titulo%type;
ie_param_163_tf_tit_w		Varchar2(50);
ie_param_134_orig_tit_w		Varchar2(50);
ie_param_142_tf_tit_w		varchar2(50);
										
begin

Obter_Param_Usuario(801, 142, obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_p, ie_param_142_tf_tit_w);
Obter_Param_Usuario(801, 163, obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_p, ie_param_163_tf_tit_w);
Obter_Param_Usuario(801, 134, obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_p, ie_param_134_orig_tit_w);


select	max(a.cd_moeda_padrao),
		max(a.nr_seq_trans_fin_baixa),
		max(a.cd_portador),
		max(a.cd_tipo_portador),
		max(a.pr_juro_padrao),
		max(a.pr_multa_padrao),
		max(a.cd_tipo_taxa_juro),
		max(a.cd_tipo_taxa_multa)
into	cd_moeda_padrao_w,
		nr_seq_trans_fin_baixa_w,
		cd_portador_w,
		cd_tipo_portador_w,
		pr_juro_padrao_w,
		pr_multa_padrao_w,
		cd_tipo_taxa_juro_w,
		cd_tipo_taxa_multa_w
from	parametro_contas_receber a
where	a.cd_estabelecimento = cd_estabelecimento_p;	

select	max(a.cd_estab_financeiro)
into	cd_estab_financeiro_w
from	estabelecimento a
where	a.cd_estabelecimento = cd_estabelecimento_p;

select	titulo_seq.nextval
into	nr_titulo_w
from	dual;

nr_titulo_gerado_p := nr_titulo_w;

insert into titulo_receber ( nr_titulo,
							cd_estabelecimento,
							cd_estab_financeiro,
							cd_moeda,
							cd_portador,
							cd_tipo_portador,
							cd_tipo_taxa_juro,
							cd_tipo_taxa_multa,
							dt_atualizacao,
							dt_emissao,
							dt_pagamento_previsto,
							dt_vencimento,
							ie_origem_titulo,
							ie_situacao,
							ie_tipo_emissao_titulo,
							ie_tipo_inclusao,
							ie_tipo_titulo,
							nm_usuario,
							tx_desc_antecipacao,
							tx_juros,
							tx_multa,
							vl_saldo_juros,
							vl_saldo_multa,
							vl_saldo_titulo,
							vl_titulo,
							cd_pessoa_fisica,
							cd_cgc,
							ds_observacao_titulo,
							nr_seq_carteira_cobr,
							nr_seq_conta_banco,
							nr_seq_trans_fin_contab,
							nr_seq_trans_fin_baixa,
							ie_pls )
				  values  ( nr_titulo_w,
							cd_estabelecimento_p,
							nvl(cd_estab_financeiro_w, cd_estabelecimento_p),
							cd_moeda_padrao_w,
							nvl(cd_portador_p, cd_portador_w),
							nvl(cd_tipo_portador_p, cd_tipo_portador_w),
							cd_tipo_taxa_juro_w,
							cd_tipo_taxa_multa_w,
							trunc(sysdate),
							trunc(sysdate),
							dt_vencimento_p,
							dt_vencimento_p,
							nvl(nvl(ie_origem_titulo_p, ie_param_134_orig_tit_w),1), --Igual ocorre no newRecord da Manutencao de TIt a receber.
							'1', -- Situa��o Aberto
							ie_tipo_emissao_titulo_p,
							ie_tipo_inclusao_p,
							ie_tipo_titulo_p,
							nm_usuario_p,
							nvl(tx_desc_antecipacao_p, 0),
							nvl(pr_juro_padrao_w, 0),
							nvl(pr_multa_padrao_w, 0),
							0, --Saldo de juros padr�o 0, pois o t�tulo est� sendo gerado agora, emitido como sysdate
							0, --Saldo de juros padr�o 0, pois o t�tulo est� sendo gerado agora, emitido como sysdate
							vl_titulo_p,
							vl_titulo_p,
							cd_pessoa_fisica_p,
							cd_cgc_p,
							ds_observacao_titulo_p,
							nr_seq_carteira_cobr_p,
							nr_seq_conta_banco_p,
							nr_seq_trans_fin_contab_p,
						    nvl(ie_param_142_tf_tit_w,nr_seq_trans_fin_baixa_w),
							ie_pls_p -- Igual ocorre no newRecord do titulo a receber.
						);

if (nvl(ie_emite_bloqueto_p,'N') = 'S') then
	gerar_bloqueto_tit_rec(nr_titulo_w,'MTR');
end if;						

if (nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;						
						
end gerar_titulo_receber_padrao;
/