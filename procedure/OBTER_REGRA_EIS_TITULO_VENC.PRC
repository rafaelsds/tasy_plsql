create or replace procedure 
	obter_regra_eis_titulo_venc(
			cd_tipo_portador_p	in	number,
			cd_portador_p		in	number,
			ie_origem_titulo_p	in	varchar2,
			ie_tipo_devedor_p	out	varchar2) is

ie_tipo_devedor_w	varchar2(1);

cursor c01 is
select	ie_tipo_devedor
from	regra_eis_titulo_venc
where	nvl(cd_tipo_portador,nvl(cd_tipo_portador_p,0)) = nvl(cd_tipo_portador_p,0)
and	nvl(cd_portador,nvl(cd_portador_p,0))		= nvl(cd_portador_p,0)
and	nvl(ie_origem_titulo,nvl(ie_origem_titulo_p,0))	= nvl(ie_origem_titulo_p,0)
order by nvl(cd_tipo_portador,0),
	nvl(cd_portador,0),
	nvl(ie_origem_titulo,0);

begin

ie_tipo_devedor_w	:= null;

open c01;
loop
fetch c01 into
	ie_tipo_devedor_w;
exit when c01%notfound;	
end loop;
close c01;

ie_tipo_devedor_p	:= ie_tipo_devedor_w;

end obter_regra_eis_titulo_venc;
/
