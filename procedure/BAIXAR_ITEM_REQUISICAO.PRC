create or replace 
procedure baixar_item_requisicao (	nr_requisicao_p				number,
				cd_material_p				number,
				cd_material_lido_p				number,
				qt_baixar_p				number,
				nr_seq_lote_fornec_p			number,
				cd_motivo_baixa_p				number,
				cd_operacao_estoque_p			number,
				ie_baixa_sem_saldo_p			varchar2,
				dt_atendimento_p				date,
				cd_barras_p				varchar2,
				nm_usuario_p				varchar2) is

nr_sequencia_w			number(10);
nr_sequencia_ww			number(10);
nr_seq_w				number(10);
nr_seq_ww			number(10);
qt_requisitada_w			number(13,4);
qt_requisitada_ww			number(13,4);
qt_baixa_total_w			number(13,4);
qt_baixar_w			number(13,4);
qt_baixar_ww			number(13,4);
qt_diferenca_w			number(13,4);
ds_erro_w			varchar2(255);
cd_kit_material_w			number(5);
cd_local_estoque_w		number(4);
cd_estabelecimento_w		number(4);
qt_liberada_w			number(10);
cd_local_estoque_destino_w		number(6);
dt_atendimento_w			date;
qt_obter_motivo_baixa_w		number(10);
dt_atendimento_item_w		date;
ie_req_transferencia_mexico_w	varchar2(1) := 'N';

cursor c01 is
	select	nr_sequencia,
		qt_material_requisitada,
		dt_atendimento
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_p
	and	cd_material = cd_material_p
	and	obter_tipo_motivo_baixa_req(cd_motivo_baixa) = 0
	and	nvl(nvl(nr_seq_lote_fornec, nr_seq_lote_fornec_p),0) = nvl(nr_seq_lote_fornec_p,0)
	union
	select	nr_sequencia,
		qt_material_requisitada,
		dt_atendimento
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_p
	and	cd_material = cd_material_p
	and	obter_tipo_motivo_baixa_req(cd_motivo_baixa) = 0
	and	nvl(nr_seq_lote_fornec,0) <> nvl(nr_seq_lote_fornec_p,0)
	order by	2 desc;

begin

lock table item_requisicao_material in exclusive mode;

select	count(*)
into	qt_liberada_w
from	requisicao_material
where	nr_requisicao = nr_requisicao_p
and	dt_liberacao is not null;
if	(qt_liberada_w = 0) then
	WHEB_MENSAGEM_PCK.EXIBIR_MENSAGEM_ABORT(185845,'NR_REQUISICAO='||nr_requisicao_p);
end if;

qt_baixa_total_w	:= 0;
qt_baixar_w		:= nvl(qt_baixar_p,0);
dt_atendimento_w	:= nvl(dt_atendimento_p,sysdate);

select	sum(nvl(qt_material_requisitada,0))
into	qt_baixar_ww
from	item_requisicao_material
where	nr_requisicao = nr_requisicao_p
and	cd_material = cd_material_p
and	obter_tipo_motivo_baixa_req(cd_motivo_baixa) = 0
and	nvl(nvl(nr_seq_lote_fornec, nr_seq_lote_fornec_p),0) = nvl(nr_seq_lote_fornec_p,0);

qt_diferenca_w		:= qt_baixar_w - qt_baixar_ww;

select	cd_local_estoque_destino
into	cd_local_estoque_destino_w
from	requisicao_material
where 	nr_requisicao = nr_requisicao_p;

if	(qt_diferenca_w > 0) then

	select	max(nr_sequencia) + 1
	into	nr_seq_w
	from	item_requisicao_material
	where	nr_requisicao	= nr_requisicao_p;

	select	max(nr_sequencia)
	into	nr_seq_ww
	from	item_requisicao_material
	where	nr_requisicao	= nr_requisicao_p
	and	cd_material	= cd_material_p
	and	obter_tipo_motivo_baixa_req(cd_motivo_baixa)	= 0
	and	nvl(nvl(nr_seq_lote_fornec, nr_seq_lote_fornec_p),0) = nvl(nr_seq_lote_fornec_p,0);

	insert into item_requisicao_material(
		nr_requisicao,
		nr_sequencia,
		cd_estabelecimento,
		cd_material,
		qt_material_requisitada,
		qt_material_atendida,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		cd_unidade_medida,
		cd_pessoa_recebe,
		cd_pessoa_atende,
		cd_motivo_baixa,
		qt_estoque,
		cd_unidade_medida_estoque,
		cd_conta_contabil,
		cd_material_req,
		ie_geracao,
		cd_cgc_fornecedor,
		cd_barras,
		nr_seq_kit_estoque)
	select	nr_requisicao,
		nr_seq_w,
		cd_estabelecimento,
		cd_material,
		qt_diferenca_w,
		qt_diferenca_w,
		vl_material,
		sysdate,
		nm_usuario_p,
		cd_unidade_medida,
		cd_pessoa_recebe,
		cd_pessoa_atende,
		0,
		qt_diferenca_w,
		cd_unidade_medida_estoque,
		cd_conta_contabil,
		cd_material_req,
		'S',
		cd_cgc_fornecedor,
		cd_barras_p,
		nr_seq_kit_estoque
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_p
	and	nr_sequencia  = nr_seq_ww;


	select	sum(nvl(qt_material_requisitada,0))
	into	qt_baixar_ww
	from	item_requisicao_material
	where	nr_requisicao	= nr_requisicao_p
	and	cd_material	= cd_material_p
	and	obter_tipo_motivo_baixa_req(cd_motivo_baixa)	= 0
	and	nvl(nvl(nr_seq_lote_fornec, nr_seq_lote_fornec_p),0) = nvl(nr_seq_lote_fornec_p,0);
end if;

select	cd_local_estoque
into	cd_local_estoque_w
from	requisicao_material
where	nr_requisicao = nr_requisicao_p;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	local_estoque
where	cd_local_estoque = cd_local_estoque_w;

select	nvl(obter_mat_estabelecimento(cd_estabelecimento_w, 0, cd_material_lido_p, 'KT'),0)
into	cd_kit_material_w
from	dual;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	qt_requisitada_w,
	dt_atendimento_item_w;
exit when c01%notfound;

	begin
	if	(qt_baixa_total_w < qt_baixar_w) then
		begin

		if	(qt_requisitada_w >= qt_baixar_w) then
			begin

			Desdobrar_Item_Requisicao(nr_requisicao_p, nr_sequencia_w, qt_baixar_w, nr_seq_lote_fornec_p, nm_usuario_p, nr_sequencia_ww);

			update	item_requisicao_material
			set	nr_seq_cor_exec	= 103
			where	nr_requisicao	= nr_requisicao_p
			and	nr_sequencia	= nr_sequencia_ww
			and	nr_seq_cor_exec	is null;

			if	(cd_material_lido_p is not null) and
				(nvl(cd_kit_material_w,0) = 0) then
				begin
				update	item_requisicao_material
				set	cd_material_lido	= cd_material_lido_p
				where	nr_requisicao	= nr_requisicao_p
				and	nr_sequencia	= nr_sequencia_ww;
				exception
				when others then
					null;
				end;
			end if;

			qt_obter_motivo_baixa_w	:= obter_tipo_motivo_baixa_req(cd_motivo_baixa_p);

			/* Tratamento realizado para a OS 1433128 integracao HAOC Esteira*/
			 if (dt_atendimento_item_w is null) and (qt_obter_motivo_baixa_w not in (1,4)) then

				select decode(qt_obter_motivo_baixa_w,5,qt_requisitada_w,0)
				into qt_obter_motivo_baixa_w
				from dual;

				set_cd_motivo_bai_item_req_mat(
						nr_sequencia_ww,
						nr_requisicao_p,
						cd_motivo_baixa_p,
						qt_obter_motivo_baixa_w,
						dt_atendimento_p,
						nm_usuario_p);

			else
				ie_req_transferencia_mexico_w := obter_se_transf_local_mexico(
										cd_estabelecimento_p	=> cd_estabelecimento_w,
										cd_operacao_estoque_p	=> cd_operacao_estoque_p);

				if	(ie_req_transferencia_mexico_w = 'S') then
					transf_local_est_consig_mexico(nr_requisicao_p	=> nr_requisicao_p,
								nr_sequencia_p		=> nr_sequencia_ww,
								qt_atendida_p		=> qt_baixar_w,
								cd_motivo_baixa_p	=> cd_motivo_baixa_p,
								cd_operacao_estoque_p	=> cd_operacao_estoque_p,
								dt_atendimento_p	=> dt_atendimento_w,
								cd_barras_p		=> cd_barras_p,
								ie_acao_p		=> '1',
								nm_usuario_p		=> nm_usuario_p);
				else
					gerar_movto_estoque_req(nr_requisicao_p, nr_sequencia_ww, cd_operacao_estoque_p,
						qt_baixar_w, dt_atendimento_w, cd_motivo_baixa_p, '1',
						ie_baixa_sem_saldo_p, nm_usuario_p, cd_barras_p, ds_erro_w);
				end if;

			end if;

			qt_baixa_total_w := qt_baixa_total_w + qt_baixar_w;
			end;

		else
			begin

			update item_requisicao_material
			set	nr_seq_cor_exec		= 103,
				cd_material_lido		= cd_material_lido_p,
				nr_seq_lote_fornec		= nr_seq_lote_fornec_p
			where	nr_requisicao		= nr_requisicao_p
			and	nr_sequencia		= nr_sequencia_w
			and	nr_seq_cor_exec		is null;

			if	(cd_material_lido_p is not null) and
				(nvl(cd_kit_material_w,0) = 0) then
				update item_requisicao_material
				set	cd_material_lido	= cd_material_lido_p,
					nr_seq_lote_fornec	= nr_seq_lote_fornec_p
				where	nr_requisicao	= nr_requisicao_p
				and	nr_sequencia	= nr_sequencia_w;
			end if;

			qt_baixa_total_w	:= qt_baixa_total_w + qt_requisitada_w;
			qt_requisitada_ww	:= qt_requisitada_w;

			if	(qt_baixa_total_w > qt_baixar_w) then
				qt_requisitada_ww	:= qt_baixa_total_w - qt_baixar_w;
				qt_baixa_total_w	:= qt_baixar_w;
				if	((qt_baixar_ww - qt_baixa_total_w) > 0) then
					Desdobrar_Item_Requisicao(nr_requisicao_p, nr_sequencia_w, (qt_baixar_ww - qt_baixa_total_w), nr_seq_lote_fornec_p, nm_usuario_p, nr_sequencia_ww);
					qt_requisitada_ww	:= qt_requisitada_w - (qt_baixar_ww - qt_baixa_total_w);
				end if;
			end if;

			ie_req_transferencia_mexico_w := obter_se_transf_local_mexico(
									cd_estabelecimento_p	=> cd_estabelecimento_w,
									cd_operacao_estoque_p	=> cd_operacao_estoque_p);

			if	(ie_req_transferencia_mexico_w = 'S') then
				transf_local_est_consig_mexico(nr_requisicao_p	=> nr_requisicao_p,
							nr_sequencia_p		=> nr_sequencia_w,
							qt_atendida_p		=> qt_requisitada_ww,
							cd_motivo_baixa_p	=> cd_motivo_baixa_p,
							cd_operacao_estoque_p	=> cd_operacao_estoque_p,
							dt_atendimento_p	=> dt_atendimento_w,
							cd_barras_p		=> cd_barras_p,
							ie_acao_p		=> '1',
							nm_usuario_p		=> nm_usuario_p);
			else
				gerar_movto_estoque_req(nr_requisicao_p, nr_sequencia_w, cd_operacao_estoque_p,
							qt_requisitada_ww, dt_atendimento_w, cd_motivo_baixa_p, '1',
							ie_baixa_sem_saldo_p, nm_usuario_p, cd_barras_p, ds_erro_w);
			end if;

			end;
		end if;
		end;
	end if;
	end;
end loop;
close c01;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end baixar_item_requisicao;
/
