CREATE OR REPLACE PROCEDURE PRC_BSI_REPORT_CALLER(
  start_date_p DATE,
  end_date_p DATE,
  age_p NUMBER,
  cd_setor_p varchar2) IS
BEGIN
  INSERT INTO BSI_REPORT_LOG(
    START_DATE, END_DATE, AGE, CD_SETOR, ELEMENTS, NM_USUARIO) 
  VALUES(
    start_date_p, end_date_p, NVL(age_p, 0), NVL(cd_setor_p, '0'), 'N', wheb_usuario_pck.get_nm_usuario);
  --
  COMMIT;
  --
  PKG_BSI_REPORT.RUN(start_date_p, end_date_p, NVL(age_p, 0), NVL(cd_setor_p, '0'), 'N');
  --
END;
/
