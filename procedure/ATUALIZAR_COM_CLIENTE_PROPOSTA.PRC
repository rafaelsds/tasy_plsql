create or replace
procedure atualizar_com_cliente_proposta(
		nr_sequencia_p		number,
		ie_opcao_p		varchar2,
		ie_prev_implant_p	varchar2,
		nm_usuario_p		varchar2) is 
/*
Opcoes:
- C 	= Cancelamento;
- EC	= Estornar cancelamento;
- A	= Aprovada;
- EA	= Estornar aprovacao;
*/

begin
if	(nr_sequencia_p is not null) and
	(ie_opcao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	if	(ie_opcao_p = 'C') then
		begin
		
		update	com_cliente_proposta
		set	dt_cancelamento		= sysdate,
			nm_usuario		= nm_usuario_p,
			nm_usuario_cancelamento = nm_usuario_p
		where	nr_sequencia		= nr_sequencia_p;
		
		if	(ie_prev_implant_p = 'S') then
			begin
			update	com_prev_implant
			set	dt_cancelamento		= sysdate,
				nm_usuario		= nm_usuario_p,
				nm_usuario_cancelamento	= nm_usuario_p
			where	nr_seq_proposta		= nr_sequencia_p;
			end ;
		end if;
		end;
	elsif	(ie_opcao_p = 'EC') then
		begin
		
		update	com_cliente_proposta
		set	dt_cancelamento		= null,
			nm_usuario		= nm_usuario_p,
			nm_usuario_cancelamento = null
		where	nr_sequencia		= nr_sequencia_p;
		end;
	elsif	(ie_opcao_p = 'A') then
		begin
		
		update	com_cliente_proposta
		set	dt_aprovacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			nm_usuario_aprov	= nm_usuario_p
		where	nr_sequencia		= nr_sequencia_p;		
		end;
	elsif	(ie_opcao_p = 'EA') then
		begin
		
		update	com_cliente_proposta
		set	dt_aprovacao		= null,
			nm_usuario		= nm_usuario_p,
			nm_usuario_aprov 	= null
		where	nr_sequencia		= nr_sequencia_p;
		end;
	end if;
	end;
end if;
commit;
end atualizar_com_cliente_proposta;
/