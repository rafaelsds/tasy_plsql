create or replace
procedure nut_calcular_custos(
			ds_lista_sequencia_p	varchar2,
			gerar_comp_rec_p		varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is 
ds_lista_sequencia_w	varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_sequencia_w		number(10,0);
begin
if	(ds_lista_sequencia_p is not null) and
	(gerar_comp_rec_p is not null) and
	(cd_estabelecimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	ds_lista_sequencia_w := ds_lista_sequencia_p;
	while (ds_lista_sequencia_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(ds_lista_sequencia_w,',');
		if	(nr_pos_virgula_w > 0) then
			begin			
			nr_sequencia_w		:= to_number(substr(ds_lista_sequencia_w,0,nr_pos_virgula_w-1));
			ds_lista_sequencia_w	:= substr(ds_lista_sequencia_w,nr_pos_virgula_w+1,length(ds_lista_sequencia_w));			
			end;
		else
			begin
			nr_sequencia_w		:= to_number(ds_lista_sequencia_w);
			ds_lista_sequencia_w	:= null;
			end;
		end if;
		if	(nr_sequencia_w > 0) then
			begin			
			if	(gerar_comp_rec_p = 'S') then
				begin
				gerar_componentes_rec_real(
						nm_usuario_p,
						nr_sequencia_w);
				end;
			end if;
			calcular_custo_comp_receita(
				nr_sequencia_w);
			end;
		end if;		
		end;
	end loop;
	end;
end if;
commit;
end nut_calcular_custos;
/
