create or replace
procedure pls_libera_regra_reemb_acao
		(nr_seq_regra_acao_p 	pls_regra_reemb_acao.nr_sequencia%type,
		nm_usuario_p	Varchar2) as 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  	consiste e libera acao da regra de reembolso
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_regra_w		pls_regra_reemb_acao.nr_seq_regra%type;
dt_vigencia_w		pls_regra_reemb_acao.dt_inicio_vigencia%type;

begin

pls_consiste_regra_reemb_acao(nr_seq_regra_acao_p);

select 	a.nr_seq_regra
into	nr_seq_regra_w
from	pls_regra_reemb_acao a
where	a.nr_sequencia = nr_seq_regra_acao_p;

update 	pls_regra_reemb_acao
set	dt_fim_vigencia = sysdate 
where	nr_seq_regra = nr_seq_regra_w
and	nr_sequencia <> nr_seq_regra_acao_p
and	dt_fim_vigencia is null;

/*update	pls_regra_reemb_acao
set	dt_liberacao		= dt_vigencia_w,
	nm_usuario_liberacao 	= nm_usuario_p
where	nr_sequencia		<> nr_seq_regra_acao_p; */ -- removido devido os 816133

update	pls_regra_reemb_acao
set	dt_liberacao		= sysdate,
	nm_usuario_liberacao 	= nm_usuario_p
where	nr_sequencia		= nr_seq_regra_acao_p;

end pls_libera_regra_reemb_acao;
/
