create or replace
procedure pls_oc_cta_tratar_val_21
		(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o para gerar ocorr�ncia em contas que passaram do prazo de envio
	definido pela Operadora em contrato com o Prestador.
------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjung OS 602077  28/06/2013 - Cria��o da rotina.
------------------------------------------------------------------------------------------------------------------
jjung 29/06/2013 

Altera��o:	Adicionado par�metro nos m�todos de atualiza��o dos campos IE_VALIDO e IE_VALIDO_TEMP 
	da PLS_TIPOS_OCOR_PCK
	
Motivo:	Se tornou necess�rio diferenciar os filtros das valida��es na hora de realizar esta opera��o
	para que os filtros de exce��o funcionem corretamente.
------------------------------------------------------------------------------------------------------------------
dlehmkuhl OS 688483 - 14/04/2014 -

Altera��o:	Modificada a forma de trabalho em rela��o a atualiza��o dos campos de controle
	que basicamente decidem se a ocorr�ncia ser� ou n�o gerada. Foi feita tamb�m a 
	substitui��o da rotina obterX_seX_geraX.

Motivo:	Necess�rio realizar essas altera��es para corrigir bugs principalmente no que se
	refere a quest�o de aplica��o de filtros (passo anterior ao da valida��o). Tamb�m
	tivemos um foco especial em performance, visto que a mesma precisou ser melhorada
	para n�o inviabilizar a nova solicita��o que diz que a exce��o deve verificar todo
	o atendimento.
------------------------------------------------------------------------------------------------------------------	
jjkruk OS 753749 - 01/07/2014 -

Altera��o:	Incluida validacao por dt_documento
Motivo:		Validar data de apresentacao de contas de reembolso	
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_sql_w	varchar2(4000);
ds_restricao_w	varchar2(100);

var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;
qt_cnt_w		pls_integer;

nr_seq_selecao_w	dbms_sql.number_table;
ie_valido_w		dbms_sql.varchar2_table;
ds_observacao_w		dbms_sql.varchar2_table;

ie_registro_valido_w	varchar2(1);

-- Informa��es da valida��o de situa��o inativa do prestador
cursor C02 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
		a.ie_data_tipo_envio_rec,
		a.ie_tipo_data_envio,
		a.qt_dias_envio
	from	pls_oc_cta_val_dt_rec a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

begin
-- Deve se ter a informa��o da regra para que a valida��o seja aplicada.
if	(dados_regra_p.nr_sequencia is not null) then

	ie_registro_valido_w := 'S';

	-- Obter o controle padr�o para quantidade de registros que ser� enviada a cada vez para a tabela de sele��o.
	qt_cnt_w := pls_cta_consistir_pck.qt_registro_transacao_w;	
		
	-- Buscar os dados da regra de valida��o conforme montado pelo usu�rio.
	for	r_C02_w in C02 (dados_regra_p.nr_sequencia) loop
	
		-- Devem ser verificadas as informa��es conforme o cadastro da regra de valida��o. 
		-- Caso a informa��o da quantidade de dias n�o seja informada, gera a ocorr�ncia para todas as contas.
		-- Desta forma se torna mais f�cil identificar que o cliente tem problemas no cadastro da regra dele.
		
		if	(r_C02_w.qt_dias_envio is not null) then			
			-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
			pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
										
			-- Se for para comparar :
			-- a data de recebimento do protocolo. 
			if	(r_C02_w.ie_data_tipo_envio_rec = 'R') then										
				-- Com :
				-- A data de emiss�o da conta.
				if	(r_C02_w.ie_tipo_data_envio = 'EM') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_emissao_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_emissao) >= :qt_dias_envio ';
					end if;
				-- A data de atendimento informada na conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'AT') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_atendimento_imp_inf) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_atendimento_inf) >= :qt_dias_envio ';
					end if;
				-- A data de atendimento refer�ncia informada na conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'AR') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_atendimento_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_atendimento) >= :qt_dias_envio ';
					end if;
				-- A data de entrada na interna��o informaada para a conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'E') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_entrada_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_entrada) >= :qt_dias_envio ';
					end if;
				-- A data de alta da interna��o.
				elsif	(r_C02_w.ie_tipo_data_envio = 'A') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_alta_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_alta) >= :qt_dias_envio ';
					end if;
				-- A data do documento
				elsif	(r_C02_w.ie_tipo_data_envio = 'DO') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_documento) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_documento) >= :qt_dias_envio ';
					end if;
				end if;
			-- a data do protocolo 
			elsif	(r_C02_w.ie_data_tipo_envio_rec = 'P') then			
				-- Com :
				-- A data de emiss�o da conta.
				if	(r_C02_w.ie_tipo_data_envio = 'EM') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_protocolo_imp - conta.dt_emissao_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_protocolo - conta.dt_emissao) >= :qt_dias_envio ';
					end if;					
				-- A data de atendimento informada na conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'AT') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_protocolo_imp - conta.dt_atendimento_imp_inf) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_protocolo - conta.dt_atendimento_inf) >= :qt_dias_envio ';
					end if;	
				-- A data de atendimento refer�ncia informada na conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'AR') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_atendimento_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_atendimento) >= :qt_dias_envio ';
					end if;
				-- A data de entrada na interna��o informada para a conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'E') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_protocolo_imp - conta.dt_entrada_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_protocolo - conta.dt_entrada) >= :qt_dias_envio ';
					end if;					
				-- A data de alta da interna��o.
				elsif	(r_C02_w.ie_tipo_data_envio = 'A') then					
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_protocolo_imp - conta.dt_alta_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_protocolo - conta.dt_alta) >= :qt_dias_envio ';
					end if;
				-- A data do documento
				elsif	(r_C02_w.ie_tipo_data_envio = 'DO') then					
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_protocolo_imp - conta.dt_documento) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_protocolo - conta.dt_documento) >= :qt_dias_envio ';
					end if;
				end if;
			-- a data de compet�ncia do protocolo
			elsif	(r_C02_w.ie_data_tipo_envio_rec = 'C') then				
				-- Com :
				-- A data de emiss�o da conta.
				if	(r_C02_w.ie_tipo_data_envio = 'EM') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_emissao_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_emissao) >= :qt_dias_envio ';
					end if;
				-- A data de atendimento informada na conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'AT') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_atendimento_imp_inf) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_atendimento_inf) >= :qt_dias_envio ';
					end if;
				-- A data de atendimento refer�ncia informada na conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'AR') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_atendimento_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_recebimento - conta.dt_atendimento) >= :qt_dias_envio ';
					end if;
				-- A data de entrada na interna��o informada para a conta.
				elsif	(r_C02_w.ie_tipo_data_envio = 'E') then				
					if	(dados_regra_p.ie_evento = 'IMP') then						
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_entrada_imp) >= :qt_dias_envio ';
					else
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_entrada) >= :qt_dias_envio ';
					end if;
				-- A data de alta da interna��o.
				elsif	(r_C02_w.ie_tipo_data_envio = 'A') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_alta_imp) >= :qt_dias_envio ';
					else 
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_alta) >= :qt_dias_envio ';
					end if;
				-- A data do documento
				elsif	(r_C02_w.ie_tipo_data_envio = 'DO') then				
					if	(dados_regra_p.ie_evento = 'IMP') then					
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_documento) >= :qt_dias_envio ';
					else 
						ds_restricao_w := 'and	(prot.dt_mes_competencia - conta.dt_documento) >= :qt_dias_envio ';
					end if;
				end if;
			end if;	

			ds_sql_w :=	'select	sel.nr_sequencia nr_seq_selecao, 		' || pls_tipos_ocor_pck.enter_w ||
						'	:ie_registro_valido,	 		' || pls_tipos_ocor_pck.enter_w ||
						'	null ds_observacao	 		' || pls_tipos_ocor_pck.enter_w ||
					'from	pls_oc_cta_selecao_ocor_v 	sel,    	' || pls_tipos_ocor_pck.enter_w ||
					'	pls_conta_ocor_v 		conta,  	' || pls_tipos_ocor_pck.enter_w ||
					'	pls_protocolo_conta 		prot    	' || pls_tipos_ocor_pck.enter_w ||
					'where	sel.nr_id_transacao = :nr_id_transacao  	' || pls_tipos_ocor_pck.enter_w ||
					'and	sel.ie_valido = ''S'' 				' || pls_tipos_ocor_pck.enter_w ||
					'and	conta.nr_sequencia = sel.nr_seq_conta 		' || pls_tipos_ocor_pck.enter_w ||
					'and	prot.nr_sequencia = conta.nr_seq_protocolo 	' || pls_tipos_ocor_pck.enter_w ||
					ds_restricao_w;
			
			-- Abrir um novo cursor
			var_cur_w := dbms_sql.open_cursor;			
			begin
				-- Criar o cursor
				dbms_sql.parse(var_cur_w, ds_sql_w, 1);
				
				dbms_sql.bind_variable(var_cur_w, ':nr_id_transacao', nr_id_transacao_p);
				dbms_sql.bind_variable(var_cur_w, ':ie_registro_valido', ie_registro_valido_w);				
				dbms_sql.bind_variable(var_cur_w, ':qt_dias_envio', r_C02_w.qt_dias_envio);
								
				-- Definir para o DBMS_SQL que o retorno do select ser�  preenchido em arrays, definindo a quantidade de linhas que o array ter� a cada itera��o do loop e a posi��o inicial que estes ocupar�o no array.
				dbms_sql.define_array(var_cur_w, 1, nr_seq_selecao_w, qt_cnt_w, 1);
				dbms_sql.define_array(var_cur_w, 2, ie_valido_w, qt_cnt_w, 1);
				dbms_sql.define_array(var_cur_w, 3, ds_observacao_w, qt_cnt_w, 1);
				
				/* Executar select  din�mico*/
				var_exec_w := dbms_sql.execute(var_cur_w);
				loop
					-- O fetch rows ir� preencher os buffers do Oracle com as linhas que ser�o passadas para a lista quando o COLUMN_VALUE for chamado.
					var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
				
					-- Zerar as listas para que o mesmo valor n�o seja inserido mais de uma vez na tabela.
					nr_seq_selecao_w	:= pls_util_cta_pck.num_table_vazia_w;
					ie_valido_w		:= pls_util_cta_pck.vchr2_table_vazia_w;		
					ds_observacao_w		:= pls_util_cta_pck.vchr2_table_vazia_w;	
				
					-- Obter as listas que foram populadas.
					dbms_sql.column_value(var_cur_w, 1, nr_seq_selecao_w);
					dbms_sql.column_value(var_cur_w, 2, ie_valido_w);
					dbms_sql.column_value(var_cur_w, 3, ds_observacao_w);
				
					pls_tipos_ocor_pck.gerencia_selecao_validacao(	nr_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w,
											'SEQ', ds_observacao_w, ie_valido_w, nm_usuario_p);
				exit when var_retorno_w != qt_cnt_w;	
				end loop;
				dbms_sql.close_cursor(var_cur_w);
			exception			
			when others then			
				if	(dbms_sql.is_open(var_cur_w)) then	
					-- Fechar os cursores que continuam abertos, os cursores que utilizam FOR - LOOP n�o necessitam serem fechados, ser�o fechados automaticamente.
					dbms_sql.close_cursor(var_cur_w);
				end if;
				
				-- Tratar erro gerado no sql din�mico, ser� inserido registro no log e abortado o processo exibindo mensagem de erro.
				pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p, ds_sql_w, nr_id_transacao_p, nm_usuario_p);			
			end;			
			
			-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
			pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
		end if;
	end loop; -- C02 
end if;

end pls_oc_cta_tratar_val_21;
/
