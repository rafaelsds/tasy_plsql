create or replace
procedure pls_desconsistir_inc_benef_web
			(	nr_seq_inclusao_p	Number) is 

begin
if	(nr_seq_inclusao_p is not null) then	
	delete	from pls_inconsist_incl_benef
	where	nr_seq_solic_inclusao = nr_seq_inclusao_p
	and 	dt_liberacao is null;
end if;

commit;

end pls_desconsistir_inc_benef_web;
/