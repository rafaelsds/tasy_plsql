create or replace
procedure Nut_descartar_servico(nr_sequencia_p		Number,
				nm_usuario_p		Varchar2,
				nr_seq_motivo_p         Number    default null,
				ds_justificativa_p      Varchar2  default null) is 

begin
if (nr_sequencia_p is not null) then

	update	nut_atend_serv_dia
	set	dt_descarte = sysdate,
		nm_usuario_descarte = nm_usuario_p,
		nr_seq_motivo_cancel = nr_seq_motivo_p,
		ds_justif_cancel = ds_justificativa_p
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end Nut_descartar_servico;
/
