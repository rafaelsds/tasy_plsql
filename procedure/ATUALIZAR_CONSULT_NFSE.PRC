create or replace
procedure atualizar_consult_nfse(
			nr_seq_transmissao_p	number) is 

begin

if (nr_seq_transmissao_p is not null) then
	
	update	nfe_transmissao
	set	ie_status_transmissao 	= 'EC'
	where	nr_sequencia 		= nr_seq_transmissao_p;
	commit;

end if;

end atualizar_consult_nfse;
/