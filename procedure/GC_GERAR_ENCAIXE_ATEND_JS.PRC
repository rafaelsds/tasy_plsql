create or replace
procedure gc_gerar_encaixe_atend_js(
					cd_medico_enc_atend_p	varchar2,
					qt_min_dur_atend_p	number,
					cd_pessoa_enc_atend_p	varchar2,
					ds_pessoa_enc_atend_p	varchar2,
					nr_atendimento_p	number,
					nr_seq_proc_interno_p	number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					ie_lado_atend_p		varchar2,
					obs_enc_atend_p		varchar2,
					nm_pessoa_contato_enc_p	varchar2,
					cd_agenda_p		number,
					dt_agenda_p		date,
					hr_inicio_p		date,
					ie_status_agenda_p	varchar2,					
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					ie_carater_cirurgia_p	varchar2 default null,
					cd_procedencia_p	varchar2 default null)is

nr_seq_agenda_w	number(10);
ds_erro_2_w	varchar2(255);
					
begin

consiste_se_agenda_unica(0,cd_agenda_p,dt_agenda_p,hr_inicio_p,ie_status_agenda_p,nm_usuario_p,cd_estabelecimento_p);
	
if	(cd_medico_enc_atend_p <> '') then
	begin
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(53486);
	end;
end if;
	
gerar_encaixe_atend_agenda(	dt_agenda_p,
				qt_min_dur_atend_p,
				cd_pessoa_enc_atend_p,
				ds_pessoa_enc_atend_p,
				nr_atendimento_p,
				cd_medico_enc_atend_p,
				nr_seq_proc_interno_p,
				cd_procedimento_p, 
				ie_origem_proced_p,
				ie_lado_atend_p,
				cd_agenda_p,
				cd_estabelecimento_p,
				nm_usuario_p,
				obs_enc_atend_p,
				nm_pessoa_contato_enc_p,
				nr_seq_agenda_w,
				ie_carater_cirurgia_p,
				cd_procedencia_p,
				ds_erro_2_w,
				null);

commit;

end gc_gerar_encaixe_atend_js;
/
