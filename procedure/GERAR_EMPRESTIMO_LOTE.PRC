create or replace
procedure gerar_emprestimo_lote(
				nr_emprestimo_p	Number,
				ds_lote_fornec_p	Varchar2,
				dt_validade_p	Date,
				cd_material_p	Number,
				qt_emprestimo_p	Number,
				cd_cgc_p	Varchar2,
				nm_usuario_p	Varchar2,
				vl_emprestimo_p	Number,
				nr_seq_marca_p	Number) is


cd_estabelecimento_w		Number(4);
cd_local_estoque_w		Number(5);
nr_sequencia_w			Number(5);
nr_seq_lote_w			Number(10);
ie_estoque_lote_w		varchar2(1);
cd_material_estoque_w		number(6);
ie_gera_w			varchar2(1);
ie_quantidade_w			varchar2(1);
ie_gerar_desdobrado_w		varchar2(1);
i				integer;
ie_disp_emprestimo_lib_w	parametro_estoque.ie_disp_emprestimo_lib%type;

BEGIN

select	max(ie_disp_emprestimo_lib)
into	ie_disp_emprestimo_lib_w
from	parametro_estoque
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(ie_disp_emprestimo_lib_w <> 'S') then
	begin
	select	cd_estabelecimento,
		cd_local_estoque
	into	cd_estabelecimento_w,
		cd_local_estoque_w
	from	emprestimo
	where	nr_emprestimo	= nr_emprestimo_p;

	select	substr(obter_se_material_estoque_lote(cd_estabelecimento_w, cd_material_p),1,1)
	into	ie_estoque_lote_w
	from	dual;

	Obter_Regra_empr_lote_fornec(	cd_material_p,
					cd_estabelecimento_w,
					ie_gera_w,
					ie_quantidade_w,
					ie_gerar_desdobrado_w);
					
	select	cd_material_estoque
	into	cd_material_estoque_w
	from	material
	where	cd_material = cd_material_p;
					
	if	(nvl(ie_gerar_desdobrado_w,'N') = 'N') then
		begin
		/*Gera o lote de fornecedor do lote recebido*/
		select	material_lote_fornec_seq.nextval
		into	nr_seq_lote_w
		from	dual;

		insert into material_lote_fornec(
			nr_sequencia,
			cd_material,
			ie_origem_lote,
			nr_digito_verif,
			dt_atualizacao,
			nm_usuario,
			ds_lote_fornec,
			dt_validade,
			cd_cgc_fornec,
			qt_material,
			cd_estabelecimento,
			ie_validade_indeterminada,
			ie_situacao,
			nr_seq_marca,
			nr_emprestimo)
		values(nr_seq_lote_w,
			cd_material_p,
			'E',
			calcula_digito('Modulo11', nr_seq_lote_w),
			sysdate,
			nm_usuario_p,
			ds_lote_fornec_p,
			dt_validade_p,
			cd_cgc_p,
			qt_emprestimo_p,
			cd_estabelecimento_w,
			'N',
			'A',
			nr_seq_marca_p,
			nr_emprestimo_p);


		/*Gera o saldo de estoque para o lote gerado acima*/
		if	(ie_estoque_lote_w = 'S') then
			select	cd_material_estoque
			into	cd_material_estoque_w
			from	material
			where	cd_material = cd_material_p;

			insert into saldo_estoque_lote(
				cd_estabelecimento,
				cd_local_estoque,
				cd_material,
				dt_mesano_referencia,
				nr_seq_lote,
				qt_estoque,
				dt_atualizacao,
				nm_usuario)
			values( cd_estabelecimento_w,
				cd_local_estoque_w,
				cd_material_estoque_w,
				trunc(sysdate,'mm'),
				nr_seq_lote_w,
				qt_emprestimo_p,
				sysdate,
				nm_usuario_p);
		end if;
		end;
	else
		for i in 1..qt_emprestimo_p loop
			begin
			/*Gera o lote de fornecedor do lote recebido*/
			select	material_lote_fornec_seq.nextval
			into	nr_seq_lote_w
			from	dual;

			insert into material_lote_fornec(
				nr_sequencia,
				cd_material,
				ie_origem_lote,
				nr_digito_verif,
				dt_atualizacao,
				nm_usuario,
				ds_lote_fornec,
				dt_validade,
				cd_cgc_fornec,
				qt_material,
				cd_estabelecimento,
				ie_validade_indeterminada,
				ie_situacao,
				nr_seq_marca,
				nr_emprestimo)
			values(nr_seq_lote_w,
				cd_material_p,
				'E',
				calcula_digito('Modulo11', nr_seq_lote_w),
				sysdate,
				nm_usuario_p,
				ds_lote_fornec_p,
				dt_validade_p,
				cd_cgc_p,
				1,
				cd_estabelecimento_w,
				'N',
				'A',
				nr_seq_marca_p,
				nr_emprestimo_p);


			/*Gera o saldo de estoque para o lote gerado acima*/
			if	(ie_estoque_lote_w = 'S') then

				insert into saldo_estoque_lote(
					cd_estabelecimento,
					cd_local_estoque,
					cd_material,
					dt_mesano_referencia,
					nr_seq_lote,
					qt_estoque,
					dt_atualizacao,
					nm_usuario)
				values( cd_estabelecimento_w,
					cd_local_estoque_w,
					cd_material_estoque_w,
					trunc(sysdate,'mm'),
					nr_seq_lote_w,
					1,
					sysdate,
					nm_usuario_p);
			end if;
			end;
		end loop;
	end if;
	end;
end if;

/*REGISTRA O EMPRESTIMO*/
select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	emprestimo_material
where	nr_emprestimo = nr_emprestimo_p;

insert into emprestimo_material(
	nr_emprestimo,
	nr_sequencia,
	cd_material,
	qt_material,
	dt_atualizacao,
	nm_usuario,
	qt_emprestimo,
	dt_validade_material,
	ds_lote_fornec,
	nr_seq_lote,
	vl_referencia,
	cd_cgc_lote,
	nr_seq_marca)
values(	nr_emprestimo_p,
	nr_sequencia_w,
	cd_material_p,
	qt_emprestimo_p,
	sysdate,
	nm_usuario_p,
	qt_emprestimo_p,
	dt_validade_p,
	ds_lote_fornec_p,
	nr_seq_lote_w,
	vl_emprestimo_p,
	cd_cgc_p,
	nr_seq_marca_p);

commit;

END gerar_emprestimo_lote;
/