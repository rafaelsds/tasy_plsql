create or replace
procedure san_deletar_emprest_saida(
		nr_seq_emprestimo_p			number,
		nr_seq_emp_saida_p			number,
		cd_estabelecimento_p		number,
		nm_usuario_p			varchar2,
		ds_erro_p			out	varchar2,
		ie_mostrar_erro_p		varchar2 default  null) is 

ie_possui_emprestimos_w	varchar2(1);
ie_permite_alterar_transf_w	varchar2(1);

begin

select	decode(count(*), 0, 'N', 'S')
into	ie_possui_emprestimos_w
from	san_emprestimo
where	ie_entrada_saida	= 'S'
and	dt_fechamento	is not null
and	nr_sequencia	= nr_seq_emprestimo_p;

if	(ie_possui_emprestimos_w = 'S') then
	ds_erro_p	:= obter_texto_tasy(92911, wheb_usuario_pck.get_nr_seq_idioma);
else
	begin
	/* Hemoterapia - Par�metro [60] - Permite atualizar dados da transfus�o */
	obter_param_usuario(450, 60, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_permite_alterar_transf_w);

	if	(ie_permite_alterar_transf_w = 'N') then
		ds_erro_p	:= obter_texto_tasy(92916, wheb_usuario_pck.get_nr_seq_idioma);
	else
		begin
		update	san_producao
		set	nr_seq_emp_saida	= null
		where	nr_sequencia	= nr_seq_emp_saida_p;

		commit;
		end;
	end if;
	end;
end if;

if	(ie_mostrar_erro_p	= 'S')	and
	(ds_erro_p is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_p);
end if;


end san_deletar_emprest_saida;
/
