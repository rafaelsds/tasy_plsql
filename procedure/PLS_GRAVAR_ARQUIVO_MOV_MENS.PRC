create or replace
procedure pls_gravar_arquivo_mov_mens
			(	nr_seq_mov_operadora_p		pls_mov_mens_operadora.nr_sequencia%type,
				ds_arquivo_p			pls_mov_mens_operadora.ds_arquivo%type,
				nm_usuario_p			usuario.nm_usuario%type) is
begin

if	(nr_seq_mov_operadora_p is not null) then
	update	pls_mov_mens_operadora
	set	ds_arquivo		= ds_arquivo_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_mov_operadora_p;
	
	commit;
end if;

end pls_gravar_arquivo_mov_mens;
/