create or replace
procedure fis_gerar_arquivo_diot(	nr_seq_controle_p	number,
					cd_estabelecimento_p	number,
					dt_inicio_p		date,
					dt_fim_p		date,
					cd_empresa_p		number,
					ds_separador_p		varchar2,
					qt_linha_p		in out	number,
					nr_sequencia_p		in out	number,
					nm_usuario_p		Varchar2) is


cd_w				number(10) := 1;
ds_linha_w			varchar(2000);
sep_w				varchar2(1)	:= ds_separador_p;
ds_arquivo_w			varchar2(2000);
ds_arquivo_compl_w		varchar2(2000);
nr_linha_w			number(10) 	:= qt_linha_p;
tx_tributo_w			nota_fiscal_item_trib.tx_tributo%Type;
qt_commit_w			number(10);
vl_campo_8_w 			nota_fiscal.vl_mercadoria%Type;
vl_campo_13_w                   nota_fiscal.vl_mercadoria%Type;
vl_campo_19_w			nota_fiscal.vl_mercadoria%Type;
vl_campo_20_w			nota_fiscal.vl_mercadoria%Type;
vl_campo_21_w			nota_fiscal.vl_mercadoria%Type;
vl_campo_22_w			nota_fiscal.vl_mercadoria%Type;
vl_campo_23_w                   nota_fiscal.vl_mercadoria%Type;
vl_campo_24_w                   nota_fiscal.vl_mercadoria%Type;
vl_tributo_w			number(13,4);
vl_aliquota_w			number(13,4);
vl_baixa_w			titulo_pagar_baixa.vl_baixa%type;
-- Com 24 campos 01/03/2019

Cursor C01 is
select  y.ie_tipo_terceiro,
	y.ie_tipo_operacao_mx,
	y.cd_rfc,
	y.cd_internacional,
	elimina_acentos(y.nm_estrangeiro) nm_estrangeiro,
	elimina_acentos(y.sg_pais) sg_pais,
	elimina_acentos(y.ds_nacionalidade) ds_nacionalidade,
	trunc(sum(y.vl_total_nota)) vl_total_nota,
	trunc(sum(y.vl_baixa)) vl_baixa,
	trunc(sum(y.vl_mercadoria)) vl_mercadoria,
	trunc(sum(y.vl_campo_8))  vl_campo_8,
	trunc(sum(y.vl_campo_9))  vl_campo_9,
	trunc(sum(y.vl_campo_10)) vl_campo_10,
	trunc(sum(y.vl_campo_11)) vl_campo_11,
	trunc(sum(y.vl_campo_12)) vl_campo_12,
	trunc(sum(y.vl_campo_13)) vl_campo_13,
	trunc(sum(y.vl_campo_14)) vl_campo_14,
	trunc(sum(y.vl_campo_15)) vl_campo_15,
	trunc(sum(y.vl_campo_16)) vl_campo_16,
	trunc(sum(y.vl_campo_17)) vl_campo_17,
	trunc(sum(y.vl_campo_18)) vl_campo_18,
	trunc(sum(y.vl_campo_19)) vl_campo_19,
	trunc(sum(y.vl_campo_20)) vl_campo_20,
	trunc(sum(y.vl_campo_21)) vl_campo_21,
	trunc(sum(y.vl_campo_22)) vl_campo_22,
	trunc(sum(y.vl_campo_23)) vl_campo_23,
	trunc(sum(y.vl_campo_24)) vl_campo_24,
	y.nr_titulo, y.nota, y.dt_baixa,
	y.nr_lote_contabil,
	y.nr_seq_cheque_cp,
	max(y.ie_union) ie_union
from (
select 	decode(t.ie_internacional,'N','04','05') ie_tipo_terceiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	decode(t.ie_internacional, 'N', null, j.cd_internacional) cd_internacional,
	decode(t.ie_internacional, 'N', null, elimina_acentos(j.ds_razao_social)) nm_estrangeiro,
	decode(t.ie_internacional, 'N', null, e.sg_pais) sg_pais,
	decode(t.ie_internacional, 'N', null, obter_nome_pais(e.nr_sequencia))  ds_nacionalidade,
	max(c.vl_total_nota) vl_total_nota,
	sum(y.vl_baixa) vl_baixa,
	max(c.vl_mercadoria) vl_mercadoria,	
	decode(y.nr_tit_receber, null, fis_obter_dados_iva(c.nr_sequencia, 1)) vl_campo_8,
	to_number(null) vl_campo_9,
	to_number(null) vl_campo_10,
	to_number(null) vl_campo_11,
	to_number(null) vl_campo_12,
	fis_obter_dados_iva(c.nr_sequencia,7) vl_campo_13,
	to_number(null) vl_campo_14,
	to_number(null) vl_campo_15,
	to_number(null) vl_campo_16,
	to_number(null) vl_campo_17,
	to_number(null) vl_campo_18,
	to_number(null) vl_campo_19,
	to_number(null) vl_campo_20,
	fis_obter_dados_iva(c.nr_sequencia, 3) vl_campo_21,
	fis_obter_dados_iva(c.nr_sequencia, 2) vl_campo_22,
	fis_obter_dados_iva(c.nr_sequencia, 5) vl_campo_23,
	fis_obter_dados_iva(c.nr_sequencia, 6, dt_inicio_p, dt_fim_p) vl_campo_24,	
	y.nr_titulo,
	x.nr_seq_nota_fiscal nota,
	trunc(y.dt_baixa) dt_baixa,
	y.nr_lote_contabil,
	y.nr_seq_cheque_cp,
	'1' ie_union
from	pessoa_juridica j,
	tipo_pessoa_juridica t,
	nota_fiscal c,
	operacao_nota b,
	pais e,
	estabelecimento h,
	titulo_pagar x,
	titulo_pagar_baixa y
where	b.cd_operacao_nf = c.cd_operacao_nf
and	j.nr_seq_pais = e.nr_sequencia(+)
and	x.nr_titulo = y.nr_titulo
and	x.nr_seq_nota_fiscal = c.nr_sequencia
--and	trunc(y.dt_baixa) between dt_inicio_p and dt_fim_p
and    	y.nr_seq_baixa_origem is null
and	not exists( 	select 	1
			from   	titulo_pagar_baixa q
			where  	q.nr_titulo = x.nr_titulo
			and  	nr_seq_baixa_origem is not null
			and    	nr_seq_baixa_origem = y.nr_sequencia)
and	t.cd_tipo_pessoa = j.cd_tipo_pessoa
and	c.ie_situacao = 1
and 	c.ie_tipo_nota in ('EN','EF')
and	j.cd_cgc = x.cd_cgc
--and	c.cd_estabelecimento 	= cd_estabelecimento_p
and	h.cd_empresa 		= cd_empresa_p
and	c.cd_estabelecimento 	= h.cd_estabelecimento
/*and 	(((y.nr_seq_cheque_cp is null) and (trunc(y.dt_baixa) between dt_inicio_p and dt_fim_p)) or
	((y.nr_seq_cheque_cp is not null) and
	(select nvl(max(1),0) from cheque where nr_sequencia = y.nr_seq_cheque_cp and dt_compensacao between dt_inicio_p and dt_fim_p) = 1))*/
and	((exists (select 1 		
		from 	cheque_bordero_titulo a, 
			titulo_pagar_bordero_v b, 
			cheque c 
		where 	a.nr_bordero = b.nr_bordero 
		and 	a.nr_seq_cheque = c.nr_sequencia 
		and 	b.nr_titulo = x.nr_titulo
		and     c.dt_cancelamento is null	
		and 	c.dt_compensacao between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
		union	
		select 	1 
		from 	cheque d 
		where 	d.nr_sequencia = y.nr_seq_cheque_cp
		and     d.dt_cancelamento is null	
		and 	d.dt_compensacao between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)))
	 or (not exists (select 1 		
			from 	cheque_bordero_titulo a, 
				titulo_pagar_bordero_v b, 
				cheque c 
			where 	a.nr_bordero = b.nr_bordero 
			and 	a.nr_seq_cheque = c.nr_sequencia 
			and 	b.nr_titulo = x.nr_titulo
			and     c.dt_cancelamento is null
			union	
			select 	1 
			from 	cheque d 
			where 	d.nr_sequencia = y.nr_seq_cheque_cp
			and     d.dt_cancelamento is null) 
		and trunc(y.dt_baixa) between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
		and  y.nr_tit_receber is null))
group by t.ie_internacional,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	j.cd_internacional,
	j.ds_razao_social,
	e.sg_pais,
	y.nr_tit_receber,
	e.nr_sequencia,
	c.nr_sequencia,	
	y.nr_titulo,
	x.nr_seq_nota_fiscal,
	trunc(y.dt_baixa),
	y.nr_lote_contabil,
	y.nr_seq_cheque_cp	
union all
select decode(e.ie_brasileiro, 'S', '04', '05') ie_tipo_terceiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	decode(e.ie_brasileiro, 'S',null, j.nr_cartao_estrangeiro) cd_internacional,
	decode(e.ie_brasileiro, 'S',null, elimina_acentos(substr(j.nm_pessoa_fisica, 1, 43))) nm_estrangeiro,
	decode(e.ie_brasileiro, 'S',null, substr(p.sg_pais,1,2)) sg_pais,
	decode(e.ie_brasileiro, 'S',null, e.ds_nacionalidade) ds_nacionalidade,
	max(c.vl_total_nota) vl_total_nota,
	sum(y.vl_baixa) vl_baixa,
	max(c.vl_mercadoria) vl_mercadoria,
	decode(y.nr_tit_receber, null, fis_obter_dados_iva(c.nr_sequencia, 1)) vl_campo_8,
	to_number(null) vl_campo_9,
	to_number(null) vl_campo_10,
	to_number(null) vl_campo_11,
	to_number(null) vl_campo_12,
	fis_obter_dados_iva(c.nr_sequencia,7) vl_campo_13,
	to_number(null) vl_campo_14,
	to_number(null) vl_campo_15,
	to_number(null) vl_campo_16,
	to_number(null) vl_campo_17,
	to_number(null) vl_campo_18,
	to_number(null) vl_campo_19,
	to_number(null) vl_campo_20,
	fis_obter_dados_iva(c.nr_sequencia, 3) vl_campo_21,
	fis_obter_dados_iva(c.nr_sequencia, 2) vl_campo_22,
	fis_obter_dados_iva(c.nr_sequencia, 5) vl_campo_23,
	fis_obter_dados_iva(c.nr_sequencia, 6, dt_inicio_p, dt_fim_p) vl_campo_24,
	y.nr_titulo,
	x.nr_seq_nota_fiscal nota,
	trunc(y.dt_baixa) dt_baixa,
	y.nr_lote_contabil,
	y.nr_seq_cheque_cp,
	'2' ie_union
from	pessoa_fisica j,
	nota_fiscal c,
	operacao_nota b,
	nacionalidade e,
	estabelecimento h,
	titulo_pagar x,
	titulo_pagar_baixa y,
	pais p
where	b.cd_operacao_nf = c.cd_operacao_nf
and	j.cd_nacionalidade = e.cd_nacionalidade(+)
and	obter_dados_pf_pj(j.cd_pessoa_fisica, null, 'CPAIS') = p.cd_codigo_pais(+)
and	x.nr_titulo = y.nr_titulo
and	x.nr_seq_nota_fiscal = c.nr_sequencia
--and	trunc(y.dt_baixa) between dt_inicio_p and dt_fim_p
and    	y.nr_seq_baixa_origem is null
and	not exists( 	select 	1
			from   	titulo_pagar_baixa q
			where  	q.nr_titulo = x.nr_titulo
			and  	nr_seq_baixa_origem is not null
			and    	nr_seq_baixa_origem = y.nr_sequencia)
and	c.ie_situacao = 1
and 	c.ie_tipo_nota in ('EN','EF')
and	j.cd_pessoa_fisica = x.cd_pessoa_fisica
--and	c.cd_estabelecimento 	= cd_estabelecimento_p
and	h.cd_empresa 		= cd_empresa_p
and	c.cd_estabelecimento 	= h.cd_estabelecimento
/*and 	((y.nr_seq_cheque_cp is null) or
	((y.nr_seq_cheque_cp is not null) and
	(select nvl(max(1),0) from cheque where nr_sequencia = y.nr_seq_cheque_cp and dt_compensacao is not null) = 1))*/
and	((exists (select 1 		
		from 	cheque_bordero_titulo a, 
			titulo_pagar_bordero_v b, 
			cheque c 
		where 	a.nr_bordero = b.nr_bordero 
		and 	a.nr_seq_cheque = c.nr_sequencia 
		and 	b.nr_titulo = x.nr_titulo
		and     c.dt_cancelamento is null	
		and 	c.dt_compensacao between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
		union	
		select 	1 
		from 	cheque d 
		where 	d.nr_sequencia = y.nr_seq_cheque_cp
		and     d.dt_cancelamento is null
		and 	d.dt_compensacao between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)))
	 or (not exists (select 1 		
			from 	cheque_bordero_titulo a, 
				titulo_pagar_bordero_v b, 
				cheque c 
			where 	a.nr_bordero = b.nr_bordero 
			and 	a.nr_seq_cheque = c.nr_sequencia 
			and 	b.nr_titulo = x.nr_titulo
			and     c.dt_cancelamento is null
			union	
			select 	1 
			from 	cheque d 
			where 	d.nr_sequencia = y.nr_seq_cheque_cp
			and     d.dt_cancelamento is null) 
		and trunc(y.dt_baixa) between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
		and  y.nr_tit_receber is null))
group by e.ie_brasileiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	j.nr_cartao_estrangeiro,
	j.nm_pessoa_fisica,
	p.sg_pais,
	y.nr_tit_receber,
	e.ds_nacionalidade,
	c.nr_sequencia,
	y.nr_titulo,
	x.nr_seq_nota_fiscal,
	trunc(y.dt_baixa),
	y.nr_lote_contabil,
	y.nr_seq_cheque_cp	
union
select 	decode(t.ie_internacional,'N','04','05') ie_tipo_terceiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	decode(t.ie_internacional, 'N', null, j.cd_internacional) cd_internacional,
	decode(t.ie_internacional, 'N', null, elimina_acentos(j.ds_razao_social)) nm_estrangeiro,
	decode(t.ie_internacional, 'N', null, e.sg_pais) sg_pais,
	decode(t.ie_internacional, 'N', null, obter_nome_pais(e.nr_sequencia))  ds_nacionalidade,
	max(c.vl_total_nota) vl_total_nota,
	sum(y.vl_adiantamento) vl_baixa,
	max(c.vl_mercadoria) vl_mercadoria,	
	fis_obter_dados_iva(c.nr_sequencia, 1) vl_campo_8,
	to_number(null) vl_campo_9,
	to_number(null) vl_campo_10,
	to_number(null) vl_campo_11,
	to_number(null) vl_campo_12,
	fis_obter_dados_iva(c.nr_sequencia,7) vl_campo_13,
	to_number(null) vl_campo_14,
	to_number(null) vl_campo_15,
	to_number(null) vl_campo_16,
	to_number(null) vl_campo_17,
	to_number(null) vl_campo_18,
	to_number(null) vl_campo_19,
	to_number(null) vl_campo_20,
	fis_obter_dados_iva(c.nr_sequencia, 3) vl_campo_21,
	fis_obter_dados_iva(c.nr_sequencia, 2) vl_campo_22,
	fis_obter_dados_iva(c.nr_sequencia, 5) vl_campo_23,
	fis_obter_dados_iva(c.nr_sequencia, 6, dt_inicio_p, dt_fim_p) vl_campo_24,
	y.nr_titulo,
	x.nr_seq_nota_fiscal nota,
	trunc(y.dt_atualizacao) dt_baixa,
	null,
	null,
	'3' ie_union
from	pessoa_juridica j,
	tipo_pessoa_juridica t,
	nota_fiscal c,
	operacao_nota b,
	pais e,
	estabelecimento h,
	titulo_pagar x,
	titulo_pagar_adiant y
where	b.cd_operacao_nf = c.cd_operacao_nf
and	j.nr_seq_pais = e.nr_sequencia(+)
and	x.nr_titulo = y.nr_titulo
and	x.nr_seq_nota_fiscal = c.nr_sequencia
and	trunc(y.dt_atualizacao) between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
/*and    	y.nr_seq_baixa_origem is null
and	not exists( 	select 	1
			from   	titulo_pagar_baixa q
			where  	q.nr_titulo = x.nr_titulo
			and  	nr_seq_baixa_origem is not null
			and    	nr_seq_baixa_origem = y.nr_sequencia)*/
and	t.cd_tipo_pessoa = j.cd_tipo_pessoa
and	c.ie_situacao = 1
and 	c.ie_tipo_nota in ('EN','EF')
and	j.cd_cgc = x.cd_cgc
--and	c.cd_estabelecimento 	= cd_estabelecimento_p
and	h.cd_empresa 		= cd_empresa_p
and	c.cd_estabelecimento 	= h.cd_estabelecimento
/*and 	((y.nr_seq_cheque_cp is null) or
	((y.nr_seq_cheque_cp is not null) and
	(select nvl(max(1),0) from cheque where nr_sequencia = y.nr_seq_cheque_cp and dt_compensacao is not null) = 1))*/
group by t.ie_internacional,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	j.cd_internacional,
	j.ds_razao_social,
	e.sg_pais,
	e.nr_sequencia,
	c.nr_sequencia,	
	y.nr_titulo,
	x.nr_seq_nota_fiscal,
	trunc(y.dt_atualizacao)
union all
select decode(e.ie_brasileiro, 'S', '04', '05') ie_tipo_terceiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	decode(e.ie_brasileiro, 'N', j.nr_cartao_estrangeiro, null) cd_internacional,
	decode(e.ie_brasileiro, 'N', elimina_acentos(substr(j.nm_pessoa_fisica, 1, 43), null)) nm_estrangeiro,
	decode(e.ie_brasileiro, 'N', substr(p.sg_pais,1,2), null) sg_pais,
	decode(e.ie_brasileiro, 'N', e.ds_nacionalidade, null) ds_nacionalidade,
	max(c.vl_total_nota) vl_total_nota,
	sum(y.vl_adiantamento) vl_baixa,
	max(c.vl_mercadoria) vl_mercadoria,
	fis_obter_dados_iva(c.nr_sequencia, 1) vl_campo_8,
	to_number(null) vl_campo_9,
	to_number(null) vl_campo_10,
	to_number(null) vl_campo_11,
	to_number(null) vl_campo_12,
	fis_obter_dados_iva(c.nr_sequencia,7) vl_campo_13,
	to_number(null) vl_campo_14,
	to_number(null) vl_campo_15,
	to_number(null) vl_campo_16,
	to_number(null) vl_campo_17,
	to_number(null) vl_campo_18,
	to_number(null) vl_campo_19,
	to_number(null) vl_campo_20,
	fis_obter_dados_iva(c.nr_sequencia, 3) vl_campo_21,
	fis_obter_dados_iva(c.nr_sequencia, 2) vl_campo_22,
	fis_obter_dados_iva(c.nr_sequencia, 5) vl_campo_23,
	fis_obter_dados_iva(c.nr_sequencia, 6, dt_inicio_p, dt_fim_p) vl_campo_24,
	y.nr_titulo,
	x.nr_seq_nota_fiscal nota,
	trunc(y.dt_atualizacao) dt_baixa,
	null,
	null,
	'4' ie_union	
from	pessoa_fisica j,
	nota_fiscal c,
	operacao_nota b,
	nacionalidade e,
	estabelecimento h,
	titulo_pagar x,
	titulo_pagar_adiant y,
	pais p
where	b.cd_operacao_nf = c.cd_operacao_nf
and	j.cd_nacionalidade = e.cd_nacionalidade(+)
and	obter_dados_pf_pj(j.cd_pessoa_fisica, null, 'CPAIS') = p.cd_codigo_pais(+)
and	x.nr_titulo = y.nr_titulo
and	x.nr_seq_nota_fiscal = c.nr_sequencia
and	trunc(y.dt_atualizacao) between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
/*and    	y.nr_seq_baixa_origem is null
and	not exists( 	select 	1
			from   	titulo_pagar_baixa q
			where  	q.nr_titulo = x.nr_titulo
			and  	nr_seq_baixa_origem is not null
			and    	nr_seq_baixa_origem = y.nr_sequencia)*/
and	c.ie_situacao = 1
and 	c.ie_tipo_nota in ('EN','EF')
and	j.cd_pessoa_fisica = x.cd_pessoa_fisica
--and	c.cd_estabelecimento 	= cd_estabelecimento_p
and	h.cd_empresa 		= cd_empresa_p
and	c.cd_estabelecimento 	= h.cd_estabelecimento
/*and 	((y.nr_seq_cheque_cp is null) or
	((y.nr_seq_cheque_cp is not null) and
	(select nvl(max(1),0) from cheque where nr_sequencia = y.nr_seq_cheque_cp and dt_compensacao is not null) = 1))*/
group by e.ie_brasileiro,
	b.ie_tipo_operacao_mx,
	j.cd_rfc,
	j.nr_cartao_estrangeiro,
	j.nm_pessoa_fisica,
	p.sg_pais,
	e.ds_nacionalidade,
	c.nr_sequencia,
	y.nr_titulo,
	x.nr_seq_nota_fiscal,
	trunc(y.dt_atualizacao)
union
select  decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', '04', '05') ie_tipo_terceiro,
	'85',
	OBTER_DADOS_PF_PJ(mtf.cd_pessoa_fisica,mtf.cd_cgc,'RFC') cd_rfc,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'CINT')) cd_internacional,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, elimina_acentos(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'N'))) nm_estrangeiro,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pais(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'P'),'SG')) sg_pais,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pais(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'P'),'N'))  ds_nacionalidade,
	0 vl_total_nota,
	0 vl_baixa,
	0 vl_mercadoria,
	(mtf.vl_transacao / 1.16) vl_campo_8,
	null vl_campo_9,
	null vl_campo_10,
	null vl_campo_11,
	null vl_campo_12,
	null vl_campo_13,
	null vl_campo_14,
	null vl_campo_15,
	null vl_campo_16,
	null vl_campo_17,
	null vl_campo_18,
	null vl_campo_19,
	null vl_campo_20,
	null vl_campo_21,
	null vl_campo_22,
	null vl_campo_23,
	null vl_campo_24,
	null nr_titulo,
	null nota,
	trunc(mtf.dt_transacao) dt_baixa,
	null,
	null,
	'5' ie_union
from 	movto_trans_financ mtf
where	trunc(mtf.dt_transacao) between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
and 	mtf.nr_seq_trans_financ in     (
					select  distinct 
						nr_seq_transacao
					from 	fis_diot_regras
					)
group by trunc(mtf.dt_transacao),
	mtf.vl_transacao,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', '04', '05'),
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'CINT')),
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, elimina_acentos(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'N'))) ,
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pais(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'P'),'SG')),
	decode(obter_se_pj_internacional(mtf.cd_cgc), 'N', null, obter_dados_pais(obter_dados_pf_pj(mtf.cd_pessoa_fisica,mtf.cd_cgc,'P'),'N')),
	OBTER_DADOS_PF_PJ(mtf.cd_pessoa_fisica,mtf.cd_cgc,'RFC')
union
select  decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'S', '05', '04') ie_tipo_terceiro,
	'85' ie_tipo_operacao_mx,
	OBTER_DADOS_PF_PJ(null,c.cd_cgc_agencia,'RFC') cd_rfc,
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pf_pj(null,c.cd_cgc_agencia,'CINT')) cd_internacional,
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, elimina_acentos(obter_dados_pf_pj(null,c.cd_cgc_agencia,'N'))) nm_estrangeiro,
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pais(obter_dados_pf_pj(null,c.cd_cgc_agencia,'P'),'SG')) sg_pais,
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pais(obter_dados_pf_pj(null,c.cd_cgc_agencia,'P'),'N'))  ds_nacionalidade,
	0 vl_total_nota,
	0 vl_baixa,
	0 vl_mercadoria,
	decode((select count(*) from fis_diot_controle_banc WHERE nr_seq_controle = nr_seq_controle_p and nr_seq_trans_iva_acred  = mtf.nr_seq_trans_financ), 1, mtf.vl_transacao, 0)vl_campo_8,
	null vl_campo_9,
	null vl_campo_10,
	null vl_campo_11,
	null vl_campo_12,
	null vl_campo_13,
	null vl_campo_14,
	null vl_campo_15,
	null vl_campo_16,
	null vl_campo_17,
	null vl_campo_18,
	null vl_campo_19,
	null vl_campo_20,
	0 vl_campo_21,
	decode((select count(*) from fis_diot_controle_banc WHERE nr_seq_controle = nr_seq_controle_p and nr_seq_trans_base_imp = mtf.nr_seq_trans_financ), 1, mtf.vl_transacao, 0) vl_campo_22,
	0 vl_campo_23,
	0 vl_campo_24,
	null nr_titulo,
	null  nota,
	trunc(mtf.dt_transacao) dt_baixa,
	null,
	null,
	'6' ie_union
from 	movto_trans_financ mtf,
	banco_estabelecimento_v b,
	agencia_bancaria c
where	trunc(mtf.dt_transacao) between inicio_dia(dt_inicio_p) and fim_dia(dt_fim_p)
and	b.cd_banco = c.cd_banco	
and	b.nr_sequencia = mtf.nr_seq_banco		
and 	(mtf.nr_seq_trans_financ =	(select  nr_seq_trans_base_imp from fis_diot_controle_banc where nr_seq_controle = nr_seq_controle_p)
      or mtf.nr_seq_trans_financ =  (select  nr_seq_trans_iva_acred from fis_diot_controle_banc where nr_seq_controle = nr_seq_controle_p))
group by mtf.nr_seq_trans_financ,
	mtf.vl_transacao,
	trunc(mtf.dt_transacao),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'S', '05', '04'),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pf_pj(null,c.cd_cgc_agencia,'CINT')),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, elimina_acentos(obter_dados_pf_pj(null,c.cd_cgc_agencia,'N'))),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pais(obter_dados_pf_pj(null,c.cd_cgc_agencia,'P'),'SG')),
	decode(obter_se_pj_internacional(c.cd_cgc_agencia), 'N', null, obter_dados_pais(obter_dados_pf_pj(null,c.cd_cgc_agencia,'P'),'N')),
	OBTER_DADOS_PF_PJ(null,c.cd_cgc_agencia,'RFC') ) y
group	by	y.ie_tipo_terceiro,
		y.ie_tipo_operacao_mx,
		y.cd_rfc,
		y.cd_internacional,
		y.nm_estrangeiro,
		y.sg_pais,
		y.ds_nacionalidade,
		y.nr_titulo,
		y.nota,
		y.dt_baixa,
		y.nr_lote_contabil,
		y.nr_seq_cheque_cp
order by y.cd_rfc;

cursor c02 is
select	ie_tipo_terceiro,
        ie_tipo_operacao_mx,
        cd_rfc,
        cd_internacional,
        nm_estrangeiro,
        sg_pais,
        ds_nacionalidade,
        decode(vl_campo_8, 0, null, vl_campo_8) vl_campo_8,
        decode(vl_campo_9, 0, null, vl_campo_9) vl_campo_9,
        decode(vl_campo_10, 0, null, vl_campo_10) vl_campo_10,
        decode(vl_campo_11, 0, null, vl_campo_11) vl_campo_11,
        decode(vl_campo_12, 0, null, vl_campo_12) vl_campo_12,
        decode(vl_campo_13, 0, null, vl_campo_13) vl_campo_13,
        decode(vl_campo_14, 0, null, vl_campo_14) vl_campo_14,
        decode(vl_campo_15, 0, null, vl_campo_15) vl_campo_15,
        decode(vl_campo_16, 0, null, vl_campo_16) vl_campo_16,
        decode(vl_campo_17, 0, null, vl_campo_17) vl_campo_17,
        decode(vl_campo_18, 0, null, vl_campo_18) vl_campo_18,
        decode(vl_campo_19, 0, null, vl_campo_19) vl_campo_19,
        decode(vl_campo_20, 0, null, vl_campo_20) vl_campo_20,
        decode(vl_campo_21, 0, null, vl_campo_21) vl_campo_21,
        decode(vl_campo_22, 0, null, vl_campo_22) vl_campo_22,
	decode(vl_campo_23, 0, null, vl_campo_23) vl_campo_23,
	decode(vl_campo_24, 0, null, vl_campo_24) vl_campo_24
from
	(select	ie_tipo_terceiro,
			ie_tipo_operacao_mx,
			cd_rfc,
			cd_internacional,
			nm_estrangeiro,
			sg_pais,
			ds_nacionalidade,
			sum(nvl(vl_campo_8, 0)) vl_campo_8,
			sum(nvl(vl_campo_9, 0)) vl_campo_9,
			sum(nvl(vl_campo_10, 0)) vl_campo_10,
			sum(nvl(vl_campo_11, 0)) vl_campo_11,
			sum(nvl(vl_campo_12, 0)) vl_campo_12,
			sum(nvl(vl_campo_13, 0)) vl_campo_13,
			sum(nvl(vl_campo_14, 0)) vl_campo_14,
			sum(nvl(vl_campo_15, 0)) vl_campo_15,
			sum(nvl(vl_campo_16, 0)) vl_campo_16,
			sum(nvl(vl_campo_17, 0)) vl_campo_17,
			sum(nvl(vl_campo_18, 0)) vl_campo_18,
			sum(nvl(vl_campo_19, 0)) vl_campo_19,
			sum(nvl(vl_campo_20, 0)) vl_campo_20,
			sum(nvl(vl_campo_21, 0)) vl_campo_21,
			sum(nvl(vl_campo_22, 0)) vl_campo_22,
			sum(nvl(vl_campo_23, 0)) vl_campo_23,
			sum(nvl(vl_campo_24, 0)) vl_campo_24
	from	w_diot
	where	nr_seq_controle		= nr_seq_controle_p
	group by ie_tipo_terceiro,
		ie_tipo_operacao_mx,
		cd_rfc,
		cd_internacional,
		nm_estrangeiro,
		sg_pais,
		ds_nacionalidade);

begin

delete 	from fis_diot_arquivo
where	nr_seq_controle_diot	=  nr_seq_controle_p;

delete	from w_diot
where	nr_seq_controle	=  nr_seq_controle_p;

commit;

for r_c01_w in c01 loop

	vl_campo_8_w	:= to_number(null);
	vl_campo_13_w	:= to_number(null);
	vl_campo_21_w	:= to_number(null);
	vl_campo_22_w	:= to_number(null);
	vl_campo_23_w	:= to_number(null);
	vl_campo_24_w	:= to_number(null);
	

	vl_campo_8_w  := r_c01_w.vl_campo_8;
	vl_campo_21_w := r_c01_w.vl_campo_21;
	vl_campo_22_w := r_c01_w.vl_campo_22;
	vl_campo_23_w := r_c01_w.vl_campo_23;
	vl_campo_24_w := r_c01_w.vl_campo_24;

	if ((r_c01_w.vl_total_nota <> r_c01_w.vl_baixa) and (r_c01_w.ie_union not in (5,6))) then

		vl_campo_8_w := trunc((r_c01_w.vl_baixa * vl_campo_8_w) / r_c01_w.vl_total_nota);
		vl_campo_13_w := trunc((r_c01_w.vl_baixa * vl_campo_13_w) / r_c01_w.vl_total_nota);	
		vl_campo_21_w := trunc((r_c01_w.vl_baixa * vl_campo_21_w) / r_c01_w.vl_total_nota);
		vl_campo_22_w := trunc((r_c01_w.vl_baixa * vl_campo_22_w) / r_c01_w.vl_total_nota);
		vl_campo_23_w := trunc((r_c01_w.vl_baixa * vl_campo_23_w) / r_c01_w.vl_total_nota);
		--vl_campo_22_w := trunc((r_c01_w.vl_baixa * vl_campo_22_w) / r_c01_w.vl_total_nota);

	end if;

	/*
	dos 4000 (valor total sem iva) tenho 2000 que corresponde a 16%
	entao se pago 1740 cuanto de base tenho?

	4000 = 100
	1740

	1740 * 2000 / 4000 = X

	X = 870
	*/
	
	-- Pelo menos um dos campos de valores precisam ter valor maior que zero para a linha ser informada no arquivo.
	
	insert into w_diot
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_controle,
		ie_tipo_terceiro,
		ie_tipo_operacao_mx,
		cd_rfc,
		cd_internacional,
		nm_estrangeiro,
		sg_pais,
		ds_nacionalidade,
		vl_campo_8,
		vl_campo_9,
		vl_campo_10,
		vl_campo_11,
		vl_campo_12,
		vl_campo_13,
		vl_campo_14,
		vl_campo_15,
		vl_campo_16,
		vl_campo_17,
		vl_campo_18,
		vl_campo_19,
		vl_campo_20,
		vl_campo_21,
		vl_campo_22,
		vl_campo_23,
		vl_campo_24,
		nr_titulo,
		vl_total_nota,
		vl_baixa,
		vl_mercadoria,
		dt_baixa,
		nr_lote_contabil,
		nr_seq_cheque,
		nr_seq_nota)
	values(w_diot_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		r_c01_w.ie_tipo_terceiro,
		r_c01_w.ie_tipo_operacao_mx,
		r_c01_w.cd_rfc,
		r_c01_w.cd_internacional,
		r_c01_w.nm_estrangeiro,
		r_c01_w.sg_pais,
		r_c01_w.ds_nacionalidade,
		trim(vl_campo_8_w),
		trim(r_c01_w.vl_campo_9),	
		trim(r_c01_w.vl_campo_10),	
		trim(r_c01_w.vl_campo_11),	
		trim(r_c01_w.vl_campo_12),	
		trim(vl_campo_13_w),	
		trim(r_c01_w.vl_campo_14),	
		trim(r_c01_w.vl_campo_15),	
		trim(r_c01_w.vl_campo_16),	
		trim(r_c01_w.vl_campo_17),	
		trim(r_c01_w.vl_campo_18),	
		vl_campo_19_w,			
		vl_campo_20_w,			
		vl_campo_21_w,			
		vl_campo_22_w,
		vl_campo_23_w,
		vl_campo_24_w,
		r_c01_w.nr_titulo,
		r_c01_w.vl_total_nota,
		r_c01_w.vl_baixa,
		r_c01_w.vl_mercadoria,
		r_c01_w.dt_baixa,
		r_c01_w.nr_lote_contabil,
		r_c01_w.nr_seq_cheque_cp,
		r_c01_w.nota);
	
end loop;

for r_c02_w in c02 loop

	if ((r_c02_w.vl_campo_8 is not null) or (r_c02_w.vl_campo_21 is not null) or (r_c02_w.vl_campo_22 is not null) or (r_c02_w.vl_campo_23 is not null) or (r_c02_w.vl_campo_24 is not null)) then	

		ds_linha_w := substr(	r_c02_w.ie_tipo_terceiro 	||
					sep_w || r_c02_w.ie_tipo_operacao_mx 	||
					sep_w || r_c02_w.cd_rfc 				||
					sep_w || r_c02_w.cd_internacional 		||
					sep_w || r_c02_w.nm_estrangeiro 		||
					sep_w || r_c02_w.sg_pais 				||
					sep_w || r_c02_w.ds_nacionalidade 		||
					sep_w || r_c02_w.vl_campo_8				|| -- Campo 8
					sep_w || r_c02_w.vl_campo_9				|| -- Campo 9
					sep_w || r_c02_w.vl_campo_10			|| -- Campo 10
					sep_w || r_c02_w.vl_campo_11			|| -- Campo 11
					sep_w || r_c02_w.vl_campo_12			|| -- Campo 12
					sep_w || r_c02_w.vl_campo_13			|| -- Campo 13
					sep_w || r_c02_w.vl_campo_14			|| -- Campo 14
					sep_w || r_c02_w.vl_campo_15			|| -- Campo 15
					sep_w || r_c02_w.vl_campo_16			|| -- Campo 16
					sep_w || r_c02_w.vl_campo_17			|| -- Campo 17
					sep_w || r_c02_w.vl_campo_18 			|| -- Campo 18
					sep_w || r_c02_w.vl_campo_19			|| -- Campo 19
					sep_w || r_c02_w.vl_campo_20			|| -- Campo 20
					sep_w || r_c02_w.vl_campo_21			|| -- Campo 21
					sep_w || r_c02_w.vl_campo_22			|| -- Campo 22
					sep_w || r_c02_w.vl_campo_23			|| -- Campo 23
					sep_w || r_c02_w.vl_campo_24			|| sep_w ,1,2000); -- Campo 24

		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
		nr_linha_w		:= nr_linha_w + 1;

		insert into fis_diot_arquivo
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_controle_diot,
			nr_linha,
			ds_arquivo,
			ds_arquivo_compl)
		values (fis_diot_arquivo_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_controle_p,
			nr_linha_w,
			ds_arquivo_w,
			ds_arquivo_compl_w);

		if	(qt_commit_w >= 5000) then
			qt_commit_w	:= 0;
			commit;
		end if;
		
	end if;

end loop;

update	fis_diot_controle
set	dt_geracao 	= sysdate
where 	nr_sequencia 	= nr_seq_controle_p;

commit;

end fis_gerar_arquivo_diot;
/
