create or replace
procedure pls_contabiliza_pro_rata
			(	nr_lote_contabil_p	number,
				cd_tipo_lote_contabil_p	number,
				ie_lote_pls_p		varchar2,
				dt_contabil_p		date,
				cd_pessoa_fisica_p	varchar2,
				cd_cgc_p		varchar2,
				ds_conteudo_p		varchar2,
				nr_seq_trans_fin_p	number,
				ie_compl_hist_p		varchar2,
				nr_titulo_p		number,
				nr_seq_baixa_p		number,
				nr_seq_liq_cc_p		number,
				nm_usuario_p		varchar2,
				nr_seq_agrupamento_p	number,
				nr_seq_w_movimento_p	in out number) is

ds_compl_historico_w		varchar2(2000);
ds_erro_w			varchar2(2000);
cd_conta_deb_w			varchar2(20);
cd_conta_rec_w			varchar2(20);
cd_conta_rec_antecip_w		varchar2(20);
cd_conta_deb_antecip_w		varchar2(20);
ie_lote_pro_rata_w		varchar2(3);
ie_deb_cred_w			varchar2(1);
ie_centro_custo_w		varchar2(1);
ie_recebimento_antec_passivo_w	varchar2(1)	:= 'N';
vl_movimento_w			number(15,2);
vl_antecipacao_w		number(15,2);
cd_centro_custo_w		number(15);
nr_sequencia_w			number(10);
cd_historico_deb_w		number(10);
cd_historico_rec_w		number(10);
nr_seq_mensalidade_w		number(10);
nr_seq_lote_mens_w		number(10);
cd_estabelecimento_w		number(4);
dt_contabilizacao_w		date;
dt_referencia_w			date;
dt_liquidacao_w			date;
dt_referencia_lote_w		date;
dt_contrato_w			date;
dt_recebimento_w		date;

Cursor C01 is
	select	a.cd_conta_deb_pls,
		a.cd_conta_rec_pls,
		nvl(a.cd_historico_rev_antec,a.cd_historico_antec_pls),
		a.cd_historico_pls,
		a.vl_contab_pro_rata,
		a.cd_centro_custo,
		a.cd_conta_rec_antecip,
		a.cd_conta_deb_antecip
	from	titulo_rec_liq_cc a
	where	a.nr_titulo		= nr_titulo_p
	and	a.nr_seq_baixa		= nr_seq_baixa_p
	and	a.nr_sequencia		= nr_seq_liq_cc_p
	and	a.ie_lote_pro_rata	= ie_lote_pls_p;

begin
nr_sequencia_w	:= nr_seq_w_movimento_p;

select	max(a.cd_estabelecimento),
	max(a.dt_referencia)
into	cd_estabelecimento_w,
	dt_referencia_lote_w
from	lote_contabil	a
where	a.nr_lote_contabil	= nr_lote_contabil_p;

begin
select	nvl(a.ie_recebimento_antec_passivo,'N')
into	ie_recebimento_antec_passivo_w
from	pls_parametro_contabil	a
where	a.cd_estabelecimento	= cd_estabelecimento_w;
exception
when no_data_found then
	wheb_mensagem_pck.exibir_mensagem_abort(185917);
end;

select	max(a.nr_seq_mensalidade),
	max(a.dt_liquidacao),
	obter_dados_titulo_receber(max(a.nr_titulo),'MM')
into	nr_seq_mensalidade_w,
	dt_liquidacao_w,
	dt_referencia_w
from	titulo_receber	a
where	a.nr_titulo	= nr_titulo_p;

select	max(a.nr_seq_lote),
	nvl(max(a.vl_antecipacao),0)
into	nr_seq_lote_mens_w,
	vl_antecipacao_w
from	pls_mensalidade	a
where	a.nr_sequencia	= nr_seq_mensalidade_w;

select	max(a.dt_contabilizacao)
into	dt_contabilizacao_w
from	pls_lote_mensalidade	a
where	a.nr_sequencia	= nr_seq_lote_mens_w;

if	(nr_seq_lote_mens_w is not null) then
	select	max(b.dt_contrato)
	into	dt_contrato_w
	from	pls_contrato		b,
		pls_lote_mensalidade	a
	where	b.nr_sequencia = a.nr_seq_contrato
	and	a.nr_sequencia = nr_seq_lote_mens_w;
end if;

select	max(dt_recebimento)
into	dt_recebimento_w
from	titulo_receber_liq
where	nr_titulo	= nr_titulo_p
and	nr_sequencia	= nr_seq_baixa_p;

if	(nr_lote_contabil_p is not null) and
	(ie_lote_pls_p is not null) then
	open C01;
	loop
	fetch C01 into
		cd_conta_deb_w,
		cd_conta_rec_w,
		cd_historico_deb_w,
		cd_historico_rec_w,
		vl_movimento_w,
		cd_centro_custo_w,
		cd_conta_rec_antecip_w,
		cd_conta_deb_antecip_w;
	exit when C01%notfound;
		begin
		if	((ie_recebimento_antec_passivo_w = 'S') and
			(dt_contabilizacao_w is not null)) or
			(dt_contabilizacao_w is null and
			((trunc(dt_liquidacao_w,'month') >= trunc(dt_referencia_w,'month')) or
			(vl_antecipacao_w = 0) or
			(trunc(dt_referencia_lote_w,'month') = trunc(dt_referencia_w,'month')))) or 
			(ie_recebimento_antec_passivo_w = 'N') or
			((dt_contrato_w < dt_liquidacao_w) or dt_contrato_w is null) then
			/* Se tiver conta de d�bito informada */
			
			if	(cd_conta_deb_w is not null) then -- OS 422857
				if	(ie_compl_hist_p = 'S') then
					select	substr(obter_compl_historico(cd_tipo_lote_contabil_p, cd_historico_deb_w, ds_conteudo_p),1,255)
					into	ds_compl_historico_w
					from	dual;
				end if;
				
				select	max(ie_centro_custo)
				into	ie_centro_custo_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_deb_w;
				
				if	(nvl(ie_centro_custo_w,'S') = 'N') then
					cd_centro_custo_w	:= null;
				end if;
				
				nr_sequencia_w	:= nr_sequencia_w + 1;
				
				begin
				insert	into w_movimento_contabil
					(nr_lote_contabil, nr_sequencia, cd_conta_contabil,
					ie_debito_credito, cd_historico, dt_movimento,
					vl_movimento, ds_doc_agrupamento, nr_seq_agrupamento,
					cd_centro_custo, ds_compl_historico, nr_seq_trans_fin,
					nr_documento, cd_pessoa_fisica, cd_cgc)
				values	(nr_lote_contabil_p, nr_sequencia_w, cd_conta_deb_w,
					'D', cd_historico_deb_w, dt_contabil_p,
					vl_movimento_w,  null, nr_seq_agrupamento_p,
					cd_centro_custo_w, ds_compl_historico_w, nr_seq_trans_fin_p,
					null, cd_pessoa_fisica_p, cd_cgc_p);
				exception
				when others then
					ds_erro_w	:= sqlerrm(sqlcode);
					wheb_mensagem_pck.exibir_mensagem_abort(185971,
										'CD_CONTA='	|| cd_conta_deb_w	||';'||
										'NR_TITULO='	|| nr_titulo_p		||';'||
										'NR_SEQ_MOVTO='	|| nr_sequencia_w	||';'||
										'NR_SEQ_TRANS='	|| nr_seq_trans_fin_p	||';'||
										'VL_MOVIMENTO='	|| vl_movimento_w	||';'||
										'DS_ERRO='	|| ds_erro_w);
					/*R AISE_APPLICATION_ERROR(-20011,'Erro ao inserir movimento cont�bil ' ||
						' cta=' || cd_conta_deb_w || ' titulo =' || nr_titulo_p ||
						' seq=' || nr_sequencia_w || ' trans=' || nr_seq_trans_fin_p ||
						' val=' || vl_movimento_w || ds_erro_w);*/
				end;
			end if;
			/* Se tiver conta de cr�dito informada */
			if	(cd_conta_rec_w is not null) then -- OS 422857
				if	(ie_compl_hist_p = 'S') then
					select	substr(obter_compl_historico(cd_tipo_lote_contabil_p, cd_historico_rec_w, ds_conteudo_p),1,255)
					into	ds_compl_historico_w
					from	dual;
				end if;
				
				select	max(ie_centro_custo)
				into	ie_centro_custo_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_rec_w;
				
				if	(nvl(ie_centro_custo_w,'S') = 'N') then
					cd_centro_custo_w	:= null;
				end if;
				
				nr_sequencia_w	:= nr_sequencia_w + 1;
				
				begin
				insert	into w_movimento_contabil
					(nr_lote_contabil, nr_sequencia, cd_conta_contabil,
					ie_debito_credito, cd_historico, dt_movimento,
					vl_movimento, ds_doc_agrupamento, nr_seq_agrupamento,
					cd_centro_custo, ds_compl_historico, nr_seq_trans_fin,
					nr_documento, cd_pessoa_fisica, cd_cgc)
				values	(nr_lote_contabil_p, nr_sequencia_w, cd_conta_rec_w,
					'C', cd_historico_rec_w, dt_contabil_p,
					vl_movimento_w,  null, nr_seq_agrupamento_p,
					cd_centro_custo_w, ds_compl_historico_w, nr_seq_trans_fin_p,
					null, cd_pessoa_fisica_p, cd_cgc_p);
				exception
				when others then
					ds_erro_w	:= sqlerrm(sqlcode);
					wheb_mensagem_pck.exibir_mensagem_abort(185971,
										'CD_CONTA='	|| cd_conta_rec_w	||';'||
										'NR_TITULO='	|| nr_titulo_p		||';'||
										'NR_SEQ_MOVTO='	|| nr_sequencia_w	||';'||
										'NR_SEQ_TRANS='	|| nr_seq_trans_fin_p	||';'||
										'VL_MOVIMENTO='	|| vl_movimento_w	||';'||
										'DS_ERRO='	|| ds_erro_w);
					/*R AISE_APPLICATION_ERROR(-20011,'Erro ao inserir movimento cont�bil ' ||
						' cta=' || cd_conta_rec_w || ' titulo =' || nr_titulo_p ||
						' seq=' || nr_sequencia_w || ' trans=' || nr_seq_trans_fin_p ||
						' val=' || vl_movimento_w || ds_erro_w);*/
				end;
			end if;
			
			if	(cd_conta_rec_antecip_w is not null) and
				(dt_contabilizacao_w is not null) and
				(ie_recebimento_antec_passivo_w = 'S') and
				(dt_recebimento_w < dt_referencia_w) then
				--(cd_conta_rec_w is not null) then
				if	(ie_compl_hist_p = 'S') then
					select	substr(obter_compl_historico(cd_tipo_lote_contabil_p, cd_historico_rec_w, ds_conteudo_p),1,255)
					into	ds_compl_historico_w
					from	dual;
				end if;
				
				select	max(ie_centro_custo)
				into	ie_centro_custo_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_rec_antecip_w;
				
				if	(nvl(ie_centro_custo_w,'S') = 'N') then
					cd_centro_custo_w	:= null;
				end if;
				
				nr_sequencia_w	:= nr_sequencia_w + 1;
				
				begin
				insert	into w_movimento_contabil
					(nr_lote_contabil, nr_sequencia, cd_conta_contabil,
					ie_debito_credito, cd_historico, dt_movimento,
					vl_movimento, ds_doc_agrupamento, nr_seq_agrupamento,
					cd_centro_custo, ds_compl_historico, nr_seq_trans_fin,
					nr_documento, cd_pessoa_fisica, cd_cgc)
				values	(nr_lote_contabil_p, nr_sequencia_w, cd_conta_rec_antecip_w,
					'C', cd_historico_rec_w, dt_contabil_p,
					vl_movimento_w,  null, nr_seq_agrupamento_p,
					cd_centro_custo_w, ds_compl_historico_w, nr_seq_trans_fin_p,
					null, cd_pessoa_fisica_p, cd_cgc_p);
				exception
				when others then
					ds_erro_w	:= sqlerrm(sqlcode);
					wheb_mensagem_pck.exibir_mensagem_abort(185971,
										'CD_CONTA=' || cd_conta_rec_antecip_w	||';'||
										'NR_TITULO=' || nr_titulo_p		||';'||
										'NR_SEQ_MOVTO=' || nr_sequencia_w	||';'||
										'NR_SEQ_TRANS=' || nr_seq_trans_fin_p	||';'||
										'VL_MOVIMENTO=' || vl_movimento_w	||';'||
										'DS_ERRO=' || ds_erro_w);
					/*R AISE_APPLICATION_ERROR(-20011,'Erro ao inserir movimento cont�bil ' ||
						' cta=' || cd_conta_rec_antecip_w || ' titulo =' || nr_titulo_p ||
						' seq=' || nr_sequencia_w || ' trans=' || nr_seq_trans_fin_p ||
						' val=' || vl_movimento_w || ds_erro_w);*/
				end;
			end if;
			
			if	(cd_conta_deb_antecip_w is not null) and
				(dt_contabilizacao_w is not null) and
				(ie_recebimento_antec_passivo_w = 'S') and
				(dt_recebimento_w < dt_referencia_w) then
				--(cd_conta_deb_w is not null) then
				if	(ie_compl_hist_p = 'S') then
					select	substr(obter_compl_historico(cd_tipo_lote_contabil_p, cd_historico_deb_w, ds_conteudo_p),1,255)
					into	ds_compl_historico_w
					from	dual;
				end if;
				
				select	max(ie_centro_custo)
				into	ie_centro_custo_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_deb_antecip_w;
				
				if	(nvl(ie_centro_custo_w,'S') = 'N') then
					cd_centro_custo_w	:= null;
				end if;
				
				nr_sequencia_w	:= nr_sequencia_w + 1;
				
				begin
				insert	into w_movimento_contabil
					(nr_lote_contabil, nr_sequencia, cd_conta_contabil,
					ie_debito_credito, cd_historico, dt_movimento,
					vl_movimento, ds_doc_agrupamento, nr_seq_agrupamento,
					cd_centro_custo, ds_compl_historico, nr_seq_trans_fin,
					nr_documento, cd_pessoa_fisica, cd_cgc)
				values	(nr_lote_contabil_p, nr_sequencia_w, cd_conta_deb_antecip_w,
					'D', cd_historico_deb_w, dt_contabil_p,
					vl_movimento_w,  null, nr_seq_agrupamento_p,
					cd_centro_custo_w, ds_compl_historico_w, nr_seq_trans_fin_p,
					null, cd_pessoa_fisica_p, cd_cgc_p);
				exception
				when others then
					ds_erro_w	:= sqlerrm(sqlcode);
					wheb_mensagem_pck.exibir_mensagem_abort(185971,
										'CD_CONTA='	|| cd_conta_deb_antecip_w	||';'||
										'NR_TITULO='	|| nr_titulo_p			||';'||
										'NR_SEQ_MOVTO='	|| nr_sequencia_w		||';'||
										'NR_SEQ_TRANS='	|| nr_seq_trans_fin_p		||';'||
										'VL_MOVIMENTO='	|| vl_movimento_w		||';'||
										'DS_ERRO='	|| ds_erro_w);
					/*R AISE_APPLICATION_ERROR(-20011,'Erro ao inserir movimento cont�bil ' ||
						' cta=' || cd_conta_deb_antecip_w || ' titulo =' || nr_titulo_p ||
						' seq=' || nr_sequencia_w || ' trans=' || nr_seq_trans_fin_p ||
						' val=' || vl_movimento_w || ds_erro_w);*/
				end;
			end if;
		end if;
		end;
	end loop;
	close C01;
end if;

nr_seq_w_movimento_p	:= nr_sequencia_w;

/* N�o pdoe commitar */

end pls_contabiliza_pro_rata;
/