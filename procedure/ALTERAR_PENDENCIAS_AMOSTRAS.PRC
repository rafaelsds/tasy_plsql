create or replace
procedure alterar_pendencias_amostras (nr_prescricao_p 		number,
				 ds_lista_seq_prescr_p 	Varchar2,
				 nm_usuario_p 		Varchar2) is
				  
nr_seq_exame_w			number(10);
ie_indetif_w			varchar2(1);
				  
cursor c01 is
	select	nr_sequencia,
		'S' ie_indetif
	from 	prescr_procedimento 
	where 	nr_prescricao = nr_prescricao_p
	and	obter_se_contido(nr_sequencia,ds_lista_seq_prescr_p) = 'S'
	union all
	select	nr_sequencia,
		'N' ie_indetif
	from 	prescr_procedimento 
	where 	nr_prescricao = nr_prescricao_p
	and	obter_se_contido(nr_sequencia,ds_lista_seq_prescr_p) = 'N';

begin
open c01;
loop
fetch c01 into	
	nr_seq_exame_w,
	ie_indetif_w;
exit when C01%notfound;
begin
	if      (nvl(nr_prescricao_p, 0) > 0 ) and
		(nvl(nr_seq_exame_w, 0) > 0) then

		update	prescr_procedimento
		set     ie_pendente_amostra     = ie_indetif_w,
			dt_atualizacao          = sysdate,
			nm_usuario              = nm_usuario_p
		where   nr_prescricao           = nr_prescricao_p
		and     nr_sequencia            = nr_seq_exame_w;

end if;
end;
end loop;
close c01;
commit;

end Alterar_pendencias_Amostras;
/
