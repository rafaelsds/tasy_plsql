create or replace
procedure seisa_controle_notif_atraso_pf(nr_seq_lote_p			number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		varchar2) is 

ds_conteudo_w			varchar2(4000);	
ds_conteudo_aux_w		varchar2(3000);
cd_beneficiario_w		varchar2(16);
nm_pessoa_fisica_w		varchar2(40);
ds_endereco_w			varchar2(45);
ds_bairro_w			varchar2(30);
cd_cep_w			varchar2(15);
ds_municipio_w			varchar2(10);
sg_estado_w			varchar2(4);	
nr_seq_apres_w			number(15) := 0;
dt_referencia_w			varchar2(6);
dt_vencimento_w			varchar2(10);
vl_valor_w			varchar2(6);
nr_sequencia_w			number(15);
nr_titulo_w			varchar2(15);
nr_parcela_w			varchar2(15);
nr_seq_registro_w		number(15);
cd_pessoa_fisica_w		varchar2(10);
dt_processamento_w		varchar2(10);
nr_contrato_w			varchar2(8);
dt_limite_w			varchar2(10);

Cursor C01 is
	select	a.nr_sequencia,
		b.cd_pessoa_fisica,
		rpad(substr(nvl(b.nm_pessoa_fisica,' '),1,40),40,' ') nm_pessoa_fisica,
		rpad(substr(nvl(c.ds_endereco,' '),1,40) || ' ' || substr(c.nr_endereco,1,5),45,' ') ds_endereco,
		rpad(substr(nvl(c.ds_bairro,' '),1,30),30,' ') ds_bairro,
		rpad(substr(nvl(c.ds_municipio,' '),1,10),10,' ') ds_municipio,
		rpad(substr(nvl(c.sg_estado,' '),1,2),4,' '),
		rpad(substr(nvl(c.cd_cep,' '),1,9),15,' ') cd_cep,
		to_char(sysdate,'dd/mm/yyyy'),
		to_char(sysdate + 20,'dd/mm/yyyy')
	from	pessoa_juridica d,
		compl_pessoa_fisica c,
		pessoa_fisica b,
		pls_notificacao_pagador a
	where	a.nr_seq_lote		= nr_seq_lote_p
	and	a.cd_pessoa_fisica 	= b.cd_pessoa_fisica
	and	c.cd_pessoa_fisica	= b.cd_pessoa_fisica(+)
	and	a.cd_cgc		= d.cd_cgc(+)
	and	c.ie_tipo_complemento 	= '1';

Cursor C02 is
	select	rpad(nvl(to_char(to_date(pls_obter_dados_item_notif(a.nr_sequencia,'DM')),'mmyyyy'),' '),6,' ') dt_referencia,
		lpad(nvl(to_char(a.dt_vencimento_titulo,'dd/mm/yyyy'),' '),10,' '),		
		lpad(replace(a.vl_pagar,',',''),6,'0') vl_pagar
	from 	pls_notificacao_item a
	where 	a.nr_seq_notific_pagador = nr_sequencia_w
	and 	rownum < 20
	order by a.nr_seq_mensalidade;
	
begin

delete from w_envio_banco
where nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into
	nr_sequencia_w,
	cd_pessoa_fisica_w,
	nm_pessoa_fisica_w,
	ds_endereco_w,
	ds_bairro_w,
	ds_municipio_w,
	sg_estado_w,
	cd_cep_w,
	dt_processamento_w,
	dt_limite_w;
exit when C01%notfound;
	begin	
	select	lpad(substr(nvl(pls_obter_dados_segurado(max(nr_sequencia),'NC'),'0'),1,8),8,'0') cd_beneficiario
	into	nr_contrato_w
	from	pls_segurado
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	ds_conteudo_w 		:=	nm_pessoa_fisica_w || ds_endereco_w || ds_bairro_w || ds_municipio_w || sg_estado_w || cd_cep_w ||
					dt_processamento_w || nr_contrato_w || dt_limite_w;
	
	open C02;
	loop
	fetch C02 into
		dt_referencia_w,
		dt_vencimento_w,
		vl_valor_w;
	exit when C02%notfound;
		begin
		ds_conteudo_aux_w  	:= 	dt_referencia_w 	|| 
						dt_vencimento_w 	|| 
						vl_valor_w;
		
		ds_conteudo_w := ds_conteudo_w || ds_conteudo_aux_w;
			
		end;
	end loop;
	close C02;
	
	nr_seq_apres_w 	:= nr_seq_apres_w + 1;
	end;
	
	select	w_envio_banco_seq.nextval
	into	nr_seq_registro_w
	from	dual;
		
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
end loop;
close C01;

commit;

end seisa_controle_notif_atraso_pf;
/