create or replace procedure inserir_conteudo_diagnostico(	nr_atendimento_p		number,
															cd_pessoa_fisica_p		varchar2,
															nm_usuario_p			varchar2,
															cd_medico_p				varchar2,
															nr_seq_interno_list_p	varchar2,
															ie_tipo_conteudo_p		varchar2,
															DS_LIST_AUX_P			varchar2 default null,
															ie_somente_relevante_p	varchar2 default 'N',
															ie_tipo_nota_clinica_p	varchar2 default 'XPTO') is

	nr_seq_modelo_w						carta_medica.nr_seq_modelo%type;
	nr_seq_carta_mae_w					carta_medica.nr_seq_carta_mae%type;
	nr_seq_carta_w						carta_medica.nr_sequencia%type;
	nr_atendimento_w					carta_medica.nr_atendimento%type;
	nr_seq_carta_conteudo_w				carta_conteudo.nr_sequencia%type;
	nr_seq_episodio_w					atendimento_paciente.nr_seq_episodio%type;
	ie_incluir_inf_adic_w				carta_medica_regra_item.ie_incluir_inf_adic%type;
	nr_seq_secao_w						carta_medica_regra.nr_seq_secao%type;

	texto_sugerido_w					clob;
	texto_sugerido_aux_w				clob;
	get_texto_conteudo_w				clob;
	dt_registro_w						date;
	dt_inicio_carta_w					date;
	SQLItensInserir   					varchar2(32767);
	ds_lista_w							varchar2(1000);
	ds_lista_aux_w						varchar2(1000);
	nr_seq_interno_w					varchar2(10) := '';
	ds_param_aux_w						varchar2(10) := '';
	ie_tipo_conteudo_w					varchar2(3) := '';
	ie_tipo_nc_w						varchar2(3) := '';
	nr_seq_result_w						number(10);
	cd_registro_inserir_w				number(20);
	nr_seq_result_item_w				number(10);
	ie_pos_virgula_w					number(3,0);
	ie_pos_virgula_aux_w				number(3,0);
	qt_registros_w						number(10);
	qt_nota_clinicas_w                  number(10);

	cursor_itens_carta_medica_w			SYS_REFCURSOR;

	const_alergias_reac_adv_w			constant varchar2(2) := 'AL';
	const_anamnese_w					constant varchar2(2) := 'AN';
	const_cirurgias_w					constant varchar2(2) := 'CI';
	const_comorbidades_w				constant varchar2(2) := 'CO';
	const_diagnostico_w 				constant varchar2(2) := 'DI';
	const_laboratorio_w					constant varchar2(3) := 'EL';
	const_exames_nao_lab_w				constant varchar2(3) := 'EI';
	const_epicrise_w                   	constant varchar2(2) := 'EP';
	const_medicamentos_cpoe_w			constant varchar2(2) := 'MD';
	const_medicamentos_em_uso_w        	constant varchar2(2) := 'ME';
	const_plano_medicamentos_w         	constant varchar2(2) := 'MP';
	const_notas_clinicas_w				varchar2(3) := 'NC';
	const_procedimentos_w				constant varchar2(2) := 'PR';
	const_mk_receitas_w					constant varchar2(2) := 'RE';
	const_diag_paciente_w				constant varchar2(2) := 'DP';



begin
	select	max(nvl((select max(y.dt_atualizacao) from carta_conteudo y where y.nr_seq_carta = nvl(a.nr_seq_carta_mae,a.nr_sequencia)),pkg_date_utils.get_date(1900,1,1))),
			max(nr_atendimento),
			max(nr_seq_modelo),
			max(a.nr_seq_carta_mae),
			max(a.nr_sequencia)
	into	dt_inicio_carta_w,
			nr_atendimento_w,
			nr_seq_modelo_w,
			nr_seq_carta_mae_w,
			nr_seq_carta_w
	from	carta_medica a
	where	nr_sequencia = obter_carta_edicao(cd_pessoa_fisica_p, cd_medico_p, nm_usuario_p, nr_atendimento_p);

	nr_seq_episodio_w := obter_episodio_atendimento(nr_atendimento_w);


	ds_lista_w := nr_seq_interno_list_p;
	ds_lista_aux_w := ds_list_aux_p;

	ie_tipo_conteudo_w := ie_tipo_conteudo_p;

	while	(ds_lista_w is not null) loop
		begin

		ie_pos_virgula_w 	:= instr(ds_lista_w,',');

		if	(ie_pos_virgula_w <> 0) then
			begin
			nr_seq_interno_w	:= substr(ds_lista_w,1,(ie_pos_virgula_w - 1));
			ds_lista_w	:= substr(ds_lista_w,(ie_pos_virgula_w + 1),length(ds_lista_w));
			end;
		else
			begin
			nr_seq_interno_w	:= ds_lista_w;
			ds_lista_w	:= null;
			end;
		end if;

		if (ds_lista_aux_w is not null) then
			ie_pos_virgula_aux_w 	:= instr(ds_lista_aux_w,',');

			if	(ie_pos_virgula_aux_w <> 0) then
				begin
				ds_param_aux_w	:= substr(ds_lista_aux_w,1,(ie_pos_virgula_aux_w - 1));
				ds_lista_aux_w	:= substr(ds_lista_aux_w,(ie_pos_virgula_aux_w + 1),length(ds_lista_aux_w));
				end;
			else
				begin
				ds_param_aux_w	:= ds_lista_aux_w;
				ds_lista_aux_w	:= null;
				end;
			end if;
		end if;

        select  count(*)
        into    qt_nota_clinicas_w
        from    carta_medica_regra
        where   ie_secao||nr_seq_secao = ie_tipo_conteudo_p;
		
		select  max(nr_seq_secao)
        into    nr_seq_secao_w
        from    carta_medica_regra
        where   ie_secao||nr_seq_secao = ie_tipo_conteudo_p;
		
		if (nr_seq_secao_w is not null) then
			const_notas_clinicas_w := 'NC'||nr_seq_secao_w;
		end if;
		
		if (ie_tipo_conteudo_p = const_diagnostico_w) then
			ie_incluir_inf_adic_w := 'S';
			if (nvl(nr_seq_modelo_w,0) > 0) then
				select	nvl(max('S'),'N')
				into	ie_incluir_inf_adic_w
				from	carta_medica_regra a,
						carta_medica_regra_item b,
						carta_medica_modelo c
				where	a.nr_sequencia 	= b.nr_seq_regra
				and		a.nr_seq_modelo = c.nr_sequencia
				and   	c.nr_sequencia  = nr_seq_modelo_w
				and		nvl(ie_incluir_inf_adic,'N') = 'S'
				and		exists	(select	1
								 from	diagnostico_doenca x
								 where	x.nr_seq_interno = to_number(nr_seq_interno_w)
								 and	(b.ie_classificacao_doenca is null or nvl(b.ie_classificacao_doenca, x.ie_classificacao_doenca) = x.ie_classificacao_doenca)
								 and	(b.ie_tipo_diagnostico is null or nvl(b.ie_tipo_diagnostico, x.ie_tipo_diagnostico) 		= x.ie_tipo_diagnostico));
			end if;
			if (ie_incluir_inf_adic_w = 'N') then
				get_text_conteudo(const_diagnostico_w, get_texto_conteudo_w);

				SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, nr_seq_interno,'||
									'      		dt_diagnostico, '||
									get_texto_conteudo_w	||
									'      		null nr_seq_result, '||
									'      		0 nr_seq_result_item, '||
									chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
									' 	from	diagnostico_doenca a '||
									' 	where	dt_liberacao is not null '||
									'	and		dt_inativacao is null ' ||
									'	and		nr_seq_interno = :nr_seq_interno '||
									'	order by dt_diagnostico desc ';
				open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
			else
                        get_texto_conteudo_w :=  '  decode(ie_tipo, ''D'', '||chr(39) ||
                                     '      <span id="' ||chr(39)|| '||nr_seq_interno||' ||chr(39)|| '"><b>' ||chr(39)||
                                     '    || obter_desc_info_cid_doenca(cd_doenca,nr_atendimento,dt_diagnostico,'|| chr(39)|| 'DS_CID_COMPLETO' || chr(39)|| ') ||' || chr(39)||                                     
                                     ' - ' ||chr(39)||'||obter_valor_dominio(13,ie_tipo_diagnostico)||' || chr(39) || 
                                     ' - ' ||chr(39)||'||Obter_Valor_Dominio(1758,IE_TIPO_DOENCA)||' || chr(39) ||
                                     ' - ' ||chr(39)||'||Obter_Valor_Dominio(58,IE_CLASSIFICACAO_DOENCA)||' || chr(39) ||                                      
                                     '      </span></b>'|| chr(39)||
                                     ',' || chr(39)|| 
                                     '      <span id="' ||chr(39)|| '||nr_seq_interno||' || chr(39)|| '">' ||chr(39)|| 
                                     '      || decode(DS_TITULO, null, '''', DS_TITULO' || ')||'|| chr(39)|| 
                                     '      </span>'|| chr(39)||
                                     '  )  ds_cid_doenca,';         
                                    
				SQLItensInserir := 	'	select  COUNT(*) OVER() qt_registros, nr_seq_interno,	'||
									'			dt_diagnostico,	'||
									get_texto_conteudo_w ||
									'			nr_seq_inf_adc nr_seq_result, '||
									'			0 nr_seq_result_item, '||
									chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
									'	from ( '||
									'		select  a.cd_doenca, '||
									'				a.nr_atendimento, '||
									'				max(a.nr_seq_interno) nr_seq_interno, '||
									'				max(a.dt_diagnostico) dt_diagnostico, '||
									'				''D'' ie_tipo, '||
									'				'''' DS_TITULO, '||
									'				null CD_PROFISSIONAL, '||
									'				null nr_seq_inf_adc '||
									'		from    diagnostico_doenca a '||
									'		inner join cid_doenca b on a.cd_doenca = b.cd_doenca_cid '||
									'		where	1 = 1 '||
									'		and		a.nr_seq_interno = :nr_seq_interno  '||
									'		and		a.dt_liberacao is not null  '||
									'       and     (case when nvl(a.cd_doenca_superior,a.cd_doenca) = a.cd_doenca then 1 else 2 end) = 1' ||
									'		and		a.dt_inativacao is null ';
				SQLItensInserir := 	SQLItensInserir ||
									'		group by a.CD_DOENCA, '||
									'				 a.nr_atendimento' ||
									'		union all '||
									'		select	a.cd_doenca, '||
									'				a.nr_atendimento, '||
									'				c.NR_SEQ_DIAG_DOENCA nr_seq_interno, '||
									'				c.DT_REGISTRO dt_diagnostico, '||
									'				''A'' ie_tipo, '||
									'				DS_TITULO, '||
									'				CD_PROFISSIONAL, '||
									'				c.nr_sequencia nr_seq_inf_adc '||
									'		from	diagnostico_doenca a '||
									'		inner join diag_doenca_inf_adic c on c.nr_seq_diag_doenca  = a.nr_seq_interno and c.dt_liberacao is not null and c.dt_inativacao is null and (c.ie_revisado is null or c.ie_revisado <> ''S'') '||
									'		where	a.nr_seq_interno = :nr_seq_interno  ';
				SQLItensInserir := 	SQLItensInserir ||
									'		and		  a.dt_liberacao is not null  '||
									'		and		  a.dt_inativacao is null) '||
									'	order by nr_seq_interno desc, dt_diagnostico ';
				open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w, nr_seq_interno_w;
			end if;
		elsif ((ie_tipo_conteudo_p = const_notas_clinicas_w or ie_tipo_conteudo_p = const_anamnese_w or ie_tipo_conteudo_p = const_epicrise_w) and qt_nota_clinicas_w > 0) then
			if (ie_tipo_nota_clinica_p = 'XPTO') then
				select	nvl (
							  (	select  max(D.IE_SECAO)
								from  	CARTA_MEDICA_REGRA d,
										CARTA_MEDICA_regra_ITEM E
								where  	E.nr_seq_regra = d.NR_SEQUENCIA
								and    	d.ie_secao = 'EP'
								and    	e.IE_EVOLUCAO_CLINICA = a.IE_EVOLUCAO_CLINICA
								and     d.NR_SEQ_MODELO = nr_seq_modelo_w),
							  (	select  max(D.IE_SECAO)
								from  	CARTA_MEDICA_REGRA d,
										CARTA_MEDICA_regra_ITEM E
								where  	E.nr_seq_regra = d.NR_SEQUENCIA
								and    	d.ie_secao = 'AN'
								and    	e.IE_EVOLUCAO_CLINICA = a.IE_EVOLUCAO_CLINICA
								and     d.NR_SEQ_MODELO = nr_seq_modelo_w)
							)
				into	ie_tipo_nc_w
				from	evolucao_paciente a
				where 	a.nr_atendimento in (select a.nr_atendimento from atendimento_paciente a where a.nr_seq_episodio = nr_seq_episodio_w)
				and		a.dt_liberacao is not null
				and		a.dt_inativacao is null
				and		a.cd_evolucao = to_number(nr_seq_interno_w)
				order by dt_evolucao desc ;

				if (ie_tipo_nc_w is not null) then
					ie_tipo_conteudo_w := ie_tipo_nc_w ;
				else
					ie_tipo_conteudo_w := ie_tipo_conteudo_p;
				end if;
			else
				ie_tipo_conteudo_w := ie_tipo_conteudo_p;
			end if;

			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, a.cd_evolucao, '||
								'			a.dt_evolucao, '||
								chr(39)|| ' ' ||chr(39)||
								'      		,0 nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								'	from	evolucao_paciente a '||
								'	where	a.dt_liberacao is not null '||
								'	and		a.dt_inativacao is null '||
								'	and		a.cd_evolucao = :cd_evolucao'||
								'	order by dt_evolucao desc ';
			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_comorbidades_w) then
			get_text_conteudo(const_comorbidades_w, get_texto_conteudo_w);

			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, nr_sequencia, '||
								'			dt_registro, '||
								get_texto_conteudo_w	||
								'      		0 nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								'	from	paciente_antec_clinico '||
								'	where	dt_liberacao is not null '||
								'	and		nr_sequencia = :nr_sequencia '||
								'	and		dt_inativacao is null '||
								'	order by dt_registro desc ';

			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_medicamentos_em_uso_w) then
			get_text_conteudo(const_medicamentos_em_uso_w, get_texto_conteudo_w);
			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, nr_sequencia, '||
								'			dt_registro, '||
								get_texto_conteudo_w	||
								'      		,0 nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								' 	from	paciente_medic_uso '||
								' 	where	nr_sequencia = :nr_sequencia '||
								' 	order by dt_registro desc ';
			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_mk_receitas_w) then
			get_text_conteudo(const_medicamentos_em_uso_w, get_texto_conteudo_w,'S');
			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, b.nr_sequencia, '||
								'			b.dt_atualizacao dt_registro, '||
								get_texto_conteudo_w	||
								'      		,0 nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								' 	from	kv_receita_farmacia_item b '||
								'	where	b.nr_seq_receita = (select	max(a.nr_sequencia) '||
								'				  				from	kv_receita_farmacia a '||
								'				  				where	a.nr_sequencia = :nr_sequencia '||
								'								and		a.dt_liberacao is not null ) '||
								' 	order by dt_registro desc ';
			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_plano_medicamentos_w) then
			get_text_conteudo(const_plano_medicamentos_w, get_texto_conteudo_w);

			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, b.nr_sequencia, '||
								'			b.dt_registro, '||
								get_texto_conteudo_w	||
								'      		0 nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								'	from	PLANO_VERSAO_MEDIC b 	'||
								'	where 	b.nr_seq_versao = (	select	max(a.nr_sequencia) '||
								'								from	plano_versao a '||
								'								where	a.nr_sequencia = :nr_sequencia '||
								'								and		a.dt_liberacao is not null ) '||
								'	order by b.dt_registro ';
			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_alergias_reac_adv_w) then
			get_text_conteudo(const_alergias_reac_adv_w, get_texto_conteudo_w);
			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, nr_sequencia, '||
								'			dt_registro, '||
								get_texto_conteudo_w	||
								'      		,0 nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								'	from	paciente_alergia '||
								'	where	dt_liberacao is not null '||
								'	and		dt_inativacao is null '||
								'	and		nr_sequencia = :nr_sequencia '||
								'	order by dt_registro desc ';

			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_cirurgias_w) then
			get_text_conteudo(const_cirurgias_w, get_texto_conteudo_w);

			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, nr_cirurgia, '||
								'			dt_inicio_cirurgia, '||
								get_texto_conteudo_w	||
								'      		,0 nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								'	from	cirurgia '||
								'	where	dt_cancelamento is null '||
								'	and		nr_cirurgia = :nr_cirurgia '||
								'	order by dt_inicio_cirurgia ';

			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_procedimentos_w) then

			ie_incluir_inf_adic_w := 'S';
			if (nvl(nr_seq_modelo_w,0) > 0) then
				select	nvl(max('S'),'N')
				into	ie_incluir_inf_adic_w
				from	carta_medica_regra a,
						carta_medica_regra_item b,
						carta_medica_modelo c
				where	a.nr_sequencia 	= b.nr_seq_regra
				and		a.nr_seq_modelo = c.nr_sequencia
				and   	c.nr_sequencia  = nr_seq_modelo_w
				and		nvl(ie_informacao_procedimento,'N') = 'S'
				and		exists	(select	1
								 from	procedimento_pac_medico x
								 where	x.nr_seq_propaci = to_number(nr_seq_interno_w));
			end if;


			if (ie_incluir_inf_adic_w = 'N') then
				get_text_conteudo(const_procedimentos_w, get_texto_conteudo_w);

				SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, nr_sequencia, '||
									'			dt_procedimento, '||
									get_texto_conteudo_w	||
									'      		,0 nr_seq_result, '||
									'      		0 nr_seq_result_item, '||
									chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
									'	from	procedimento_paciente a '||
									'	where	nr_sequencia = :nr_seq_proc_interno '||
									'	order by dt_procedimento ';

				open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
			else
				get_texto_conteudo_w := ' cd_procedimento || '
                                        || chr(39)|| ' - ' ||chr(39)||
                                        ' || substr(nvl(obter_desc_curta_proc_rotina(nr_seq_proc_interno,cd_procedimento,ie_origem_proced), ' ||
                                        ' obter_desc_procedimento(cd_procedimento, ie_origem_proced)),1,150) || ' ||
                                        ' decode(ds_titulo, null, '''', '' - '' || ds_titulo) ds_procedimento ';

				SQLItensInserir := 	'select '||
										'qt_registros, ' ||
										'nr_sequencia, ' ||
										'dt_procedimento, ' ||
										get_texto_conteudo_w ||
										', nr_seq_result, ' ||
										'nr_seq_result_item, '||
										'ie_opcao_w ' ||
									'from ( ' ||
												'SELECT Count(*) OVER() qt_registros, ' ||
										'			a.nr_seq_propaci nr_sequencia, ' ||
										'			a.dt_procedimento dt_procedimento, ' ||
											'		b.ds_titulo ds_titulo, ' ||
											'		a.cd_procedimento cd_procedimento, ' ||
											'		a.ie_origem_proced ie_origem_proced, ' ||
											'		a.nr_seq_proc_interno nr_seq_proc_interno, ' ||
											'	    b.nr_sequencia nr_seq_result, ' ||
											'		null nr_seq_result_item, ' ||
													chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
											'	from 	procedimento_pac_medico a, ' ||
											'				proc_pac_medico_inf_adic b ' ||
											'	where	a.nr_seq_propaci = :nr_seq_proc_interno ' ||
											'	and	a.nr_sequencia = b.nr_seq_proc_pac_med ' ||
											'	and	b.nr_seq_proc_pac_med = a.nr_sequencia ' ||
											'union all ' ||
											'	select	COUNT(*) OVER() qt_registros, '||
											'		a.nr_seq_propaci nr_sequencia, ' ||
											'		a.dt_procedimento dt_procedimento, ' ||
											'		'''' ds_titulo, ' ||
											'		a.cd_procedimento cd_procedimento, ' ||
											'		a.ie_origem_proced ie_origem_proced, ' ||
											'		a.nr_seq_proc_interno nr_seq_proc_interno, ' ||
											'		null nr_seq_result, ' ||
											'		null nr_seq_result_item, ' ||
													chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
											'	from	procedimento_pac_medico a ' ||
											'	where	a.nr_seq_propaci = :nr_seq_proc_interno) ' ||
									'order by dt_procedimento ';
				open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w, nr_seq_interno_w;
			end if;
		elsif (ie_tipo_conteudo_p = const_exames_nao_lab_w) then
			get_text_conteudo(const_exames_nao_lab_w, get_texto_conteudo_w, 'S');

			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, x.nr_sequencia, '||
								'			x.dt_laudo, '||
								chr(39)|| chr(39)||
								'      		,0 nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								'	from   	laudo_paciente_v x '||
								'	where  	x.nr_sequencia 			= :nr_sequencia '||
								'	order by dt_laudo desc ';

			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_laboratorio_w) then
			get_text_conteudo(const_laboratorio_w, get_texto_conteudo_w, 'S');

			SQLItensInserir :=	'	select	COUNT(*) OVER() qt_registros, 0 nr_seq_interno, '||
								'			d.dt_resultado, '||
								get_texto_conteudo_w	||
								'      		, d.nr_prescricao, '||
								'      		 e.nr_seq_prescr, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								'	from   	exame_lab_resultado d, '||
								'			exame_lab_result_item e '||
								'	where  	d.nr_seq_resultado 	= e.nr_seq_resultado '||
								'	and 	d.nr_prescricao = :nr_sequencia ' ||
								'	and 	e.nr_seq_prescr = :nr_sequencia_dois ';
			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w, ds_param_aux_w;
		elsif(ie_tipo_conteudo_p = const_diag_paciente_w) then
			get_text_conteudo(const_diag_paciente_w, get_texto_conteudo_w);

			SQLItensInserir := 	'	select	COUNT(*) OVER() qt_registros, nr_seq_interno, '||
								'      		dt_diagnostico, '||
								get_texto_conteudo_w	||
								'      		, null nr_seq_result, '||
								'      		0 nr_seq_result_item, '||
								chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
								' 	from	diagnostico_doenca a '||
								' 	where	dt_liberacao is not null '||
								'	and		dt_inativacao is null ' ||
								'	and		nr_seq_interno = :nr_seq_interno '||
								'	order by dt_diagnostico desc ';
			open cursor_itens_carta_medica_w for SQLItensInserir using cd_pessoa_fisica_p, nr_seq_interno_w;
		elsif (ie_tipo_conteudo_p = const_medicamentos_cpoe_w) then
			select CPOE_obter_desc_info_material(
								cd_material,
								cd_mat_comp1,
								qt_dose_comp1,
								cd_unid_med_dose_comp1,
								cd_mat_comp2,
								qt_dose_comp2,
								cd_unid_med_dose_comp2,
								cd_mat_comp3,
								qt_dose_comp3,
								cd_unid_med_dose_comp3,
								cd_mat_dil,
								qt_dose_dil,
								cd_unid_med_dose_dil,
								cd_mat_red,
								qt_dose_red,
								cd_unid_med_dose_red,
								dt_liberacao,
								qt_dose,
								cd_unidade_medida,
								ie_via_aplicacao,
								cd_intervalo,
								dt_lib_suspensao,
								dt_suspensao,
								ie_administracao,
								nr_seq_ataque,
								nr_seq_procedimento,
								nr_seq_adicional,
								nr_seq_hemoterapia,
								ds_dose_diferenciada,
								ds_dose_diferenciada_dil,
								ds_dose_diferenciada_red,
								ds_dose_diferenciada_comp1,
								ds_dose_diferenciada_comp2,
								ds_dose_diferenciada_comp3,
								cd_mat_comp4,
								qt_dose_comp4,
								cd_unid_med_dose_comp4,
								ds_dose_diferenciada_comp4,
								cd_mat_comp5,
								qt_dose_comp5,
								cd_unid_med_dose_comp5,
								ds_dose_diferenciada_comp5,
								cd_mat_comp6,
								qt_dose_comp6,
								cd_unid_med_dose_comp6,
								ds_dose_diferenciada_comp6,
								dt_inicio,
								dt_fim,
								dt_fim_cih,
								nr_dia_util,
								ie_antibiotico,
								ie_tipo_solucao,
								cd_mat_comp7,
								qt_dose_comp7,
								cd_unid_med_dose_comp7,
								ds_dose_diferenciada_comp7,
								null,
								null,
								null,
								null,
								null,
								ds_solucao,
								ie_duracao,
								ie_ref_calculo,
								nr_etapas,
								ie_controle_tempo) ds_material
			into get_texto_conteudo_w
			from cpoe_material
			where nr_sequencia = to_number(nr_seq_interno_w);


			get_texto_conteudo_w := REPLACE(get_texto_conteudo_w, chr(39),'"');


			SQLItensInserir :=	'	select	COUNT(*) OVER() qt_registros, ' ||
						' 		nr_sequencia, '||
						'		dt_atualizacao, '||
						chr(39) ||  get_texto_conteudo_w || chr(39) || ' ds_registro_w ' ||
						'      		, 0 nr_seq_result, '||
						'      		 0 nr_seq_prescr, '||
						chr(39)|| ie_tipo_conteudo_w||chr(39)||' ie_opcao_w '||
						'	from   	cpoe_material '||
						'	where  	nr_sequencia 	= :nr_sequencia ';
			open cursor_itens_carta_medica_w for SQLItensInserir using nr_seq_interno_w;
		end if;

		qt_registros_w := 0;

		if (ie_tipo_conteudo_p = const_laboratorio_w) then
			texto_sugerido_aux_w := ' <table class="advancedtexteditor-table cke_show_border" data-height="61" data-width="642" id="EXAME_LABORATORIAL_TABLE" style=" border: 1px solid #000 !important; "> ';
		elsif (ie_tipo_conteudo_p = const_diag_paciente_w) then
			texto_sugerido_aux_w := ' <table class="advancedtexteditor-table cke_show_border" data-height="61" data-width="642" id="DIAGNOSTICOS_PACIENTE_TABLE" style=" border: 1px solid #000 !important; "> ';
		end if;
		if (SQLItensInserir is not null) then
			loop
			fetch cursor_itens_carta_medica_w into
				qt_registros_w,
				cd_registro_inserir_w,
				dt_registro_w,
				texto_sugerido_w,
				nr_seq_result_w,
				nr_seq_result_item_w,
				ie_tipo_conteudo_w;
			exit when cursor_itens_carta_medica_w%notfound;
				begin
				if (ie_tipo_conteudo_p = const_laboratorio_w) then
					texto_sugerido_aux_w := texto_sugerido_aux_w || ' ' || texto_sugerido_w;
					if (ie_tipo_conteudo_p = const_diag_paciente_w) then
						texto_sugerido_aux_w := texto_sugerido_aux_w || ' ' || texto_sugerido_aux_w;
						if (qt_registros_w = cursor_itens_carta_medica_w%rowCount) then
							texto_sugerido_aux_w := texto_sugerido_aux_w || '</table> ';
							if (nvl(ie_somente_relevante_p,'N') = 'N') then
								inserir_log_carta(	nr_seq_carta_w,
													ie_tipo_conteudo_w,
													cd_registro_inserir_w,
													texto_sugerido_aux_w,
													nm_usuario_p,
													nr_seq_result_w,
													nr_seq_result_item_w,
													'N',--ie_incluir_aut_p
													nr_seq_carta_conteudo_w,
													null,
													null, --ie_incluso_p
													nr_seq_carta_mae_w,
													null);
							else

								inserir_log_carta_relevante(nr_atendimento_p,
															ie_tipo_conteudo_w,
															cd_registro_inserir_w,
															texto_sugerido_aux_w,
															nm_usuario_p,
															nr_seq_result_w,
															nr_seq_result_item_w,
															'N',--ie_incluir_aut_p
															nr_seq_carta_conteudo_w,
															null,
															'N' --ie_incluso_p
															);
							end if;
						end if;
					end if;
				else
					if (nvl(ie_somente_relevante_p,'N') = 'N') then
						inserir_log_carta(	nr_seq_carta_w,
											ie_tipo_conteudo_w,
											cd_registro_inserir_w,
											texto_sugerido_w,
											nm_usuario_p,
											nr_seq_result_w,
											nr_seq_result_item_w,
											'N',--ie_incluir_aut_p
											nr_seq_carta_conteudo_w,
											null,
											null,
											nr_seq_carta_mae_w,
											null);
					else
						ie_tipo_nc_w := null;

						if (ie_tipo_conteudo_w = const_epicrise_w) then
							ie_tipo_nc_w := 'C';
						end if;

						inserir_log_carta_relevante(nr_atendimento_p,
													ie_tipo_conteudo_w,
													cd_registro_inserir_w,
													texto_sugerido_w,
													nm_usuario_p,
													nr_seq_result_w,
													nr_seq_result_item_w,
													'N',--ie_incluir_aut_p
													nr_seq_carta_conteudo_w,
													ie_tipo_nc_w,
													'N' --ie_incluso_p
													);
					end if;
				end if;
				end;
			end loop;
			close cursor_itens_carta_medica_w;
		end if;
		end;
	end loop;

end inserir_conteudo_diagnostico;
/
