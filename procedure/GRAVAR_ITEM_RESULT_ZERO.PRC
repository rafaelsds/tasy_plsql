CREATE OR REPLACE PROCEDURE gravar_item_result_zero (
    nr_prescricao_p   NUMBER,
    nr_seq_prescr_p   NUMBER,
    nm_usuario_p      VARCHAR2,
    ds_erro_p         OUT VARCHAR2
) IS

    nr_prescricao_w                  NUMBER(14);
    ie_gerar_linha_result_integr_w   VARCHAR2(1);
    cd_exame_w                       VARCHAR2(20);
    ds_erro_w                        VARCHAR2(255);
    nr_seq_exame_w                   NUMBER(10);
    cd_material_exame_w              prescr_procedimento.cd_material_exame%TYPE;
    qt_itens_w                       NUMBER(10);
BEGIN
    nr_prescricao_w := nr_prescricao_p;
    SELECT
        MAX(nr_seq_exame),
        MAX(cd_material_exame)
    INTO
        nr_seq_exame_w,
        cd_material_exame_w

from
        prescr_procedimento
    WHERE
        nr_prescricao = nr_prescricao_w
        AND nr_sequencia = nr_seq_prescr_p;



select
        COUNT(*)
    INTO qt_itens_w
    FROM
        exame_lab_result_item   a,

	exame_lab_resultado     b
    WHERE
        a.nr_seq_resultado = b.nr_seq_resultado
        AND b.nr_prescricao = nr_prescricao_w
        AND a.nr_seq_prescr = nr_seq_prescr_p
        AND nr_seq_exame = nr_seq_exame_w
        AND ( a.qt_resultado > 0
              OR ( a.ds_resultado IS NOT NULL
                   AND a.ds_resultado <> ' ' ) );

    SELECT
        MAX(nvl(cd_exame_integracao, cd_exame)),
        MAX(ie_gerar_linha_result_integr)
    INTO
        cd_exame_w,
        ie_gerar_linha_result_integr_w
    FROM
        exame_laboratorio
    WHERE
        nr_seq_exame = nr_seq_exame_w;

    IF ( nvl(nr_seq_exame_w, 0) > 0 ) AND ( qt_itens_w = 0 ) THEN
        IF ( ie_gerar_linha_result_integr_w = 'S' ) THEN
            atualizar_lab_result_item(nr_prescricao_w, nr_seq_prescr_p, cd_exame_w, 0, 0, ' ', '', cd_material_exame_w, 'N', nm_usuario_p
            , NULL, NULL, NULL, NULL, NULL, ds_erro_w);

        END IF;
    ELSE
        IF ( ie_gerar_linha_result_integr_w = 'S' ) THEN
            ds_erro_w := wheb_mensagem_pck.get_texto(281774, 'NR_PRESCRICAO='
                                                             || nr_prescricao_w
                                                             || ';NR_SEQ_PRESCR='
                                                             || nr_seq_prescr_p);

        END IF;
    END IF;

    ds_erro_p := ds_erro_w;
    COMMIT;
END gravar_item_result_zero;
/