create or replace
procedure pls_consist_comunic_int_glosa(nr_seq_lote_p	number,
					nm_usuario_p	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Consistir a comunica��o de interna��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ x ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


nr_seq_segurado_guia_w		Number(10);
qt_registros_w			Number(10);
nr_seq_guia_w			Number(10);
ie_guia_apres_w			Varchar(1) := 'N';
ie_origem_lote_w		Varchar(1) ;
ie_status_w			Varchar2(2) := 'CS';
ie_status_lote_w		Varchar2(2) := 'CS';
nr_seq_prestador_lote_w		Number(10);
cd_prestador_w			Varchar2(30);
cd_prestador_guia_w		Varchar2(30);
nr_seq_prestador_w		Number(10);
ie_tipo_vinculo_w		pls_param_import_int_alta.ie_tipo_vinculo%type;

cursor C01 is
	select	cd_guia,
		cd_usuario_plano,
		nr_seq_segurado,
		nr_seq_guia,
		nr_sequencia
	from	pls_comunicacao_internacao
	where	nr_seq_lote_inter = nr_seq_lote_p;

begin


select	max(nr_seq_prestador)
into	nr_seq_prestador_lote_w
from	pls_lote_comunicacao_inter
where	nr_Sequencia = nr_seq_lote_p;

begin
	select	nvl(ie_tipo_vinculo, '0')
	into	ie_tipo_vinculo_w
	from	pls_param_import_int_alta;
exception
when others then
	ie_tipo_vinculo_w := '0';
end;

if	(nr_seq_prestador_lote_w is null) then
	-- seta a sequencia do prestador no lote, pois quando o mesmo � gerado por WS n�o possui o prestador logodo
	select	max(b.nr_seq_prestador)
	into	nr_seq_prestador_w
	from	pls_comunicacao_internacao a,
		pls_guia_plano b
	where	a.nr_seq_guia = b.nr_sequencia
	and	a.nr_seq_lote_inter =  nr_seq_lote_p;

	update	pls_lote_comunicacao_inter
	set	nr_seq_prestador = nr_seq_prestador_w
	where	nr_sequencia =  nr_seq_lote_p;
end if;

begin
	select	ie_origem_lote,
		nr_seq_prestador,
		nvl(cd_prestador,'X')
	into	ie_origem_lote_w,
		nr_seq_prestador_lote_w,
		cd_prestador_w
	from 	pls_lote_comunicacao_inter
	where	nr_sequencia = nr_seq_lote_p;
exception
when others then
	nr_seq_prestador_lote_w := NULL;
end;




for r_C01 in C01 loop

	ie_status_w := 'CS';

	if	(r_C01.nr_seq_segurado is null) then
		-- N�mero da carteira inv�lido
		pls_salvar_comunic_int_glosa(r_C01.nr_sequencia, '1001',nm_usuario_p);
	elsif	(r_C01.nr_seq_guia is not null) then

		begin
			select	nr_seq_segurado,
				nr_seq_prestador
			into	nr_seq_segurado_guia_w,
				nr_seq_prestador_w
			from	pls_guia_plano
			where	nr_sequencia = r_C01.nr_seq_guia;
		exception
		when others then
			nr_seq_prestador_w := null;
			nr_seq_segurado_guia_w := null;
		end;

		if	(nr_seq_segurado_guia_w <> nvl(r_C01.nr_seq_segurado,0)) then
			-- Identifica��o do benefici�rio n�o consistente
			pls_salvar_comunic_int_glosa(r_C01.nr_sequencia, '1011',nm_usuario_p);
		end if;

		if (ie_origem_lote_w = 'T') then
			if	(nvl(nr_seq_prestador_lote_w,0) <> nr_seq_prestador_w) then

					select	max(cd_prestador)
					into	cd_prestador_guia_w
					from	pls_prestador
					where	nr_Sequencia = nvl(nr_seq_prestador_lote_w,0)
					and	ie_situacao = 'A';

					if	(nvl(cd_prestador_guia_w, cd_prestador_w||'X') <> cd_prestador_w) then
						/* C�digo Prestador inv�lido */
						pls_salvar_comunic_int_glosa(r_C01.nr_sequencia, '1203',nm_usuario_p);
					end if;

			end if;
		end if;

	end if;

	-- S� vai consistir a glosa 1307 se o sistema estiver parametrizado para vincular a comunica��o a uma guia automacicamente
	if	(r_C01.nr_seq_guia is null) and (ie_tipo_vinculo_w = '0') then
		/* Conforme conversado com o Leonardo Cunha, pode ter situa��es que a guia ainda n�o existe na comunica��o de interna��o, exemplo uma interna��o de em�rgencia.. */
		--if (ie_origem_lote_w <> 'X') then
			-- N�mero da guia inv�lido
		pls_salvar_comunic_int_glosa(r_C01.nr_sequencia, '1307',nm_usuario_p);
		--end if;
	else
		select  count(1)
		into	qt_registros_w
		from    pls_conta x
		where   x.nr_seq_guia = r_C01.nr_seq_guia;

		if	(qt_registros_w > 0) then
			ie_guia_apres_w := 'S';

			-- Se o lote veio via xml, o sistema verifica se existe uma guia de prorroga��o para a guia de interna��o
			if (ie_origem_lote_w = 'X') then
				select	max(nr_sequencia)
				into	nr_seq_guia_w
				from	pls_guia_plano a
				where	nr_seq_guia_principal = r_C01.nr_seq_guia
				and	ie_tipo_guia = '8'
				and	ie_status  = '1'
				and	not exists(select 1
					           from	  pls_conta x
						   where  x.nr_seq_guia = a.nr_sequencia);

				if (nr_seq_guia_w is not null) then
					ie_guia_apres_w := 'N';

					update	pls_comunicacao_internacao
					set	nr_seq_guia = nr_seq_guia_w
					where	nr_sequencia = r_C01.nr_sequencia;
				end if;
			end if;

			if (ie_guia_apres_w = 'S') then
				/* Guia j� apresentada  */
				pls_salvar_comunic_int_glosa(r_C01.nr_sequencia, '1308',nm_usuario_p);
			end if;

		end if;
	end if;

	select	count(1)
	into	qt_registros_w
	from	pls_comunic_int_glosa
	where	nr_seq_comunicado_int = r_C01.nr_sequencia;

	if	(qt_registros_w > 0) then
		ie_status_w := 'CG';
		ie_status_lote_w := 'CG';
	end if;

	update	pls_comunicacao_internacao
	   set	ie_status = ie_status_w
	 where  nr_sequencia = r_C01.nr_sequencia;

end loop;




update	pls_lote_comunicacao_inter
   set	ie_status = ie_status_lote_w
 where  nr_sequencia = nr_seq_lote_p;

commit;

-- Caso esteja consistido sem glosa, e a origem seja texto, o sistema vai atualizar a guia
--Na OS 773076, - Foi liberado para atualizar tanto noTXT como no XML
-- OS 973038 - adicionado tratamento para o parametro Tipo processo (OPS - Gest�o de Operadoras > Parametros da OPS > Comunica��o de Interna��o/Alta XML)
-- que define se a comunica��o vai ser vinculada com uma guia automaticamente ou manualmente.
if ( ie_status_w = 'CS' ) and ( ie_tipo_vinculo_w = '0' ) then
	pls_atualizar_comunic_int_guia(nr_seq_lote_p, nm_usuario_p);
end if;

end pls_consist_comunic_int_glosa;
/
