create or replace
procedure pls_gerar_guia_prestador
			(	cd_guia_p			Varchar2,	
				cd_guia_principal_imp_p		Varchar2,
				ie_tipo_guia_p			Varchar2,
				cd_usuario_plano_imp_p		Varchar2,
				nm_segurado_imp_p		Varchar2,
				ds_plano_imp_p			Varchar2,
				dt_validade_cartao_imp_p	Date,
				cd_ans_imp_p			Varchar2,
				ie_carater_internacao_imp_p	Varchar2,
				dt_solicitacao_imp_p		Date,
				ds_indicacao_clinica_imp_p	Varchar2,
				ie_regime_internacao_imp_p	Varchar2,
				nr_seq_autorizacao_imp_p	Number,
				nr_seq_clinica_imp_p		Varchar2,
				qt_dia_autorizado_imp_p		Varchar2,
				dt_admissao_hosp_imp_p		Date,
				dt_validade_senha_imp_p		Date,
				cd_tipo_acomod_autor_imp_p	Varchar2,
				nr_cartao_nac_sus_imp_p		Varchar2,
				ds_observacao_imp_p		Varchar2,
				cd_senha_imp_p			Varchar2,
				nm_responsavel_autoriz_imp_p	Varchar2,
				cd_estabelecimento_p		Number,
				nm_usuario_p			Varchar2,
				cd_cgc_solicitado_imp_p		Varchar2,				
				nm_prestador_imp_p		Varchar2,
				cd_cnes_imp_p			Varchar2, 
				nr_seq_prestador_imp_p		Number, 
				cd_cgc_prestador_imp_p		Varchar2,
				cd_cpf_prestador_imp_p		Varchar2,				
				nm_medico_solicitante_imp_p	Varchar2, 
				uf_crm_imp_p			Varchar2,
				nr_crm_imp_p			Varchar2,
				ds_conselho_profissional_imp_p	Varchar2,
				cd_cbo_saude_imp_p		Varchar2,
				cd_medico_solicitante_p		varchar2,
				nr_seq_guia_plano_p	out 	Number) is 
				
/* 

cd_estabelecimento_p - Passar estabelecimento da Operadora

Solicitante
cd_cgc_solicitado_imp_p

Contratado solicitante
nm_prestador_imp_p
cd_cnes_imp_p, 
nr_seq_prestador_imp_p, 
cd_cgc_prestador_imp_p	
cd_cpf_prestador_imp_p	

Profissional
nm_medico_solicitante_imp_p
uf_crm_imp_p
nr_crm_imp_p
ds_conselho_profissional_imp_p
cd_cbo_saude_imp_p	
*/

nr_seq_guia_plano_w	Number(10);
cd_versao_tiss_ops_w	varchar2(100);


begin

select	pls_obter_versao_tiss
into	cd_versao_tiss_ops_w
from	dual;

select	pls_guia_plano_seq.nextval
into	nr_seq_guia_plano_w
from	dual;

insert into pls_guia_plano (
	nr_sequencia, ie_estagio, cd_estabelecimento,	
	cd_guia, cd_guia_imp, ie_status,
        ie_situacao,nm_usuario, dt_atualizacao,
        ie_tipo_guia, cd_usuario_plano_imp, nm_segurado_imp,
        ds_plano_imp, dt_validade_cartao_imp, cd_ans_imp,
        ie_carater_internacao_imp, dt_solicitacao_imp, ds_indicacao_clinica_imp,
        ie_regime_internacao_imp, nr_seq_autorizacao_imp, nr_seq_clinica_imp,
        qt_dia_autorizado_imp, dt_admissao_hosp_imp, dt_validade_senha_imp,
        cd_tipo_acomod_autor_imp, ie_tipo_processo, nr_cartao_nac_sus_imp,
        ds_observacao_imp, cd_senha_imp, nm_responsavel_autoriz_imp,
	cd_guia_manual_imp, cd_cgc_solicitado_imp, nm_prestador_imp,
	cd_cnes_imp, nr_seq_prestador_imp, cd_cgc_prestador_imp,
	cd_cpf_prestador_imp, nm_medico_solicitante_imp, uf_crm_imp,
	nr_crm_imp, ds_conselho_profissional_imp, cd_cbo_saude_imp, 
	cd_medico_solicitante, nr_seq_prestador, cd_versao,
	dt_solicitacao) 
values (nr_seq_guia_plano_w, 7, cd_estabelecimento_p,
        to_char(nr_seq_guia_plano_w), cd_guia_principal_imp_p, '2',
        'U', nm_usuario_p, sysdate,
        ie_tipo_guia_p, cd_usuario_plano_imp_p, nm_segurado_imp_p,
        ds_plano_imp_p, dt_validade_cartao_imp_p, cd_ans_imp_p,
        ie_carater_internacao_imp_p, dt_solicitacao_imp_p, ds_indicacao_clinica_imp_p,
        ie_regime_internacao_imp_p, nr_seq_autorizacao_imp_p, nr_seq_clinica_imp_p,
        qt_dia_autorizado_imp_p, dt_admissao_hosp_imp_p, dt_validade_senha_imp_p,
        cd_tipo_acomod_autor_imp_p, 'E', nr_cartao_nac_sus_imp_p,
        ds_observacao_imp_p, cd_senha_imp_p, nm_responsavel_autoriz_imp_p,
	cd_guia_principal_imp_p, cd_cgc_solicitado_imp_p, nm_prestador_imp_p,
	cd_cnes_imp_p, nr_seq_prestador_imp_p, cd_cgc_prestador_imp_p,
	cd_cpf_prestador_imp_p,  nm_medico_solicitante_imp_p, uf_crm_imp_p,
	nr_crm_imp_p, ds_conselho_profissional_imp_p, cd_cbo_saude_imp_p, 
	cd_medico_solicitante_p, nr_seq_prestador_imp_p, cd_versao_tiss_ops_w,
	dt_solicitacao_imp_p);

nr_seq_guia_plano_p := nr_seq_guia_plano_w;

commit;

end pls_gerar_guia_prestador;
/