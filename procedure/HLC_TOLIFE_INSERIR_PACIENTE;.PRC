create or replace
procedure hlc_tolife_inserir_paciente(cd_pessoa_fisica_p out number, 
                                  cd_cpf_p            varchar2,
                                  nm_pessoa_fisica_p  varchar2,
                                  nm_usuario_p        varchar2) is 


ie_existe_pessoa_w  char(2000);
cd_pessoa_fisica_w  number;
ds_erro_w           varchar2(2000);
begin
    begin

    select nvl(max('S'), 'N')
    into   ie_existe_pessoa_w
    from   pessoa_fisica  
    where  nr_cpf = cd_cpf_p;
    
    if (ie_existe_pessoa_w = 'N') then 
    
			select	pessoa_fisica_seq.nextval
			into	cd_pessoa_fisica_w
			from	dual;
            
			insert into pessoa_fisica(
                cd_pessoa_fisica,
                ie_tipo_pessoa,
                nm_pessoa_fisica,
                dt_atualizacao,
                nm_usuario)
                values(
                cd_pessoa_fisica_w,
                2,
                nm_pessoa_fisica_p,
                sysdate,
                nm_usuario_p
                );
            
            commit;
                
    end if;        
    
    cd_pessoa_fisica_p := cd_pessoa_fisica_w;
    exception
    when others then
        ds_erro_w   := sqlerrm(sqlcode);
    end;
commit;

end hlc_tolife_inserir_paciente;
/
