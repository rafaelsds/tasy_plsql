create or replace
procedure agcons_atualiza_chamada_atend(nr_seq_agenda_p		number,
										nm_usuario_p		varchar2,
										ie_gerando_atend_p	varchar2) is 


nr_atendimento_w	number(10);
/*
ie_gerando_atend_p:
S - Atendimento gerado;
N - Atendimento n�o gerado
*/
												
begin

if	(nr_seq_agenda_p is not null)then
	
	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	agenda_consulta
	where	nr_sequencia = nr_seq_agenda_p;
	
	if	(nr_atendimento_w is null) and
		(ie_gerando_atend_p = 'S')then
		update	agenda_consulta
		set		dt_chamada_atend 	= sysdate,
				nm_usuario			= nm_usuario_p
		where	nr_sequencia		= nr_seq_agenda_p;	
		
		commit;
	else
		update	agenda_consulta
		set		dt_chamada_atend 	= null,
				nm_usuario			= nm_usuario_p
		where	nr_sequencia		= nr_seq_agenda_p;	
		
		commit;
	end if;
end if;

end agcons_atualiza_chamada_atend;
/