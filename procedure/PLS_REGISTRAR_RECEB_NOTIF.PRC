create or replace
procedure pls_registrar_receb_notif ( 	nr_seq_notificacao_p number,
					ds_responsavel_receb_p varchar2,
					dt_recebimento_p date,
					ie_tipo_notificacao_receb_p varchar2,
					nr_seq_registro_cobr_p number default null) is

qt_registro_cobr_w	number(10);

begin

select 	count(1)
into	qt_registro_cobr_w
from	pls_notificacao_receb
where 	nr_seq_registro_cobr	= nr_seq_registro_cobr_p
and 	nr_seq_notificacao 	= nr_seq_notificacao_p;

if (qt_registro_cobr_w = 0) then

	insert into pls_notificacao_receb
		(
		nr_sequencia,
		ds_responsavel_receb,
		dt_atualizacao,
		dt_recebimento,
		ie_tipo_notificacao_receb,
		nm_usuario,
		nr_seq_notificacao,
		nr_seq_registro_cobr
		)
	values
		(
		pls_notificacao_receb_seq.nextval,
		ds_responsavel_receb_p,
		sysdate,
		dt_recebimento_p,
		ie_tipo_notificacao_receb_p,
		wheb_usuario_pck.get_nm_usuario,
		nr_seq_notificacao_p,
		nr_seq_registro_cobr_p
		);

	commit;
end if;
	
end pls_registrar_receb_notif;
/