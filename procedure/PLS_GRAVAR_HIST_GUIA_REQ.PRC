create or replace
procedure pls_gravar_hist_guia_req
				(	nr_seq_requisicao_p	Number,
					nr_seq_guia_p		Number,
					nr_seq_execucao_p	Number,
					ie_tipo_historico_p	Varchar2,
					ds_historico_p		Varchar2,
					nm_usuario_p		Varchar2) is 

					
nr_seq_requisicao_w	pls_requisicao.nr_sequencia%type;
					
begin
	if (nr_seq_execucao_p is not null) then
		select	max(nr_seq_requisicao)
		into	nr_seq_requisicao_w
		from	pls_execucao_requisicao
		where	nr_sequencia = nr_seq_execucao_p;
		
		pls_requisicao_gravar_hist(nr_seq_requisicao_w, ie_tipo_historico_p, ds_historico_p, nr_seq_execucao_p, nm_usuario_p);
	elsif (nr_seq_requisicao_p is not null) then
		pls_requisicao_gravar_hist(nr_seq_requisicao_p, ie_tipo_historico_p, ds_historico_p, null, nm_usuario_p);
	elsif (nr_seq_guia_p is not null) then 
		insert into pls_guia_plano_historico 
			(nr_sequencia, nr_seq_guia, ie_tipo_log,
			dt_historico, dt_atualizacao, nm_usuario,
			ds_observacao, ie_origem_historico, ie_tipo_historico)
		values(	pls_guia_plano_historico_seq.nextval, nr_seq_guia_p, '2',
			sysdate, sysdate, nm_usuario_p,
			ds_historico_p,'A','M');
	end if;

commit;

end pls_gravar_hist_guia_req;
/
