CREATE OR REPLACE PROCEDURE CHANGE_STATUS_CLINICAL_PATHWAY 
( nr_sequencia_p NUMBER , status_p VARCHAR2 , code_p NUMBER ) IS

status_w varchar2(10);
cd_evolucao_old_w number(10);
ie_gerar_cli_path_evol_w    varchar2(1);
cd_evolucao_w number(10);
nr_atendimento_origem_w protocolo_int_paciente.nr_atendimento_origem%TYPE;
cd_pessoa_fisica_w protocolo_int_paciente.cd_pessoa_fisica%TYPE;

BEGIN

if (code_p = 1146351 and status_p = 'N') then
    status_w:='A';

elsif (code_p = 1146352 and status_p = 'A') then
    status_w:='N';

elsif (code_p = 1146353 and status_p = 'A') then
    status_w:='F';

elsif (code_p = 1146354 and status_p = 'A') then
    status_w:='S';

elsif (code_p = 1146355 and status_p = 'F') then
    status_w:='A';

elsif (code_p = 1146356 and status_p = 'S') then
    status_w:='A';

else
    status_w:='A';

end if;

update    PROTOCOLO_INT_PACIENTE
set       IE_STATUS = status_w
where    nr_sequencia =     nr_sequencia_p;
commit;

select nr_atendimento_origem ,nvl(cd_evolucao,0),cd_pessoa_fisica
into nr_atendimento_origem_w, cd_evolucao_old_w,cd_pessoa_fisica_w
from  protocolo_int_paciente
where  nr_sequencia = nr_sequencia_p;

Obter_param_Usuario(281,1628, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_cli_path_evol_w);

if ( ie_gerar_cli_path_evol_w = 'S' ) then
	if (( status_p = 'N' and status_w = 'A' ) or (status_p = 'A' and status_w = 'F'  ) or (status_p = 'A' and status_w = 'S'  )or (status_p = 'F' and status_w = 'A'  )or (status_p = 'S' and status_w = 'A'  )or (status_p = 'A' and status_w = 'N'  )) then
        if(cd_evolucao_old_w>0 )  then
			delete from clinical_note_soap_data	 where cd_evolucao = cd_evolucao_old_w and ie_med_rec_type = 'CLNICAL_PATHWAY' and ie_stage = 1 and ie_soap_type = 'P' and nr_seq_med_item = nr_sequencia_p;
			clinical_notes_pck.gerar_soap(nr_atendimento_origem_w, nr_sequencia_p, 'CLNICAL_PATHWAY', NULL, 'P', 1, cd_evolucao_w,null,cd_pessoa_fisica_w);
			clinical_notes_pck.soap_data_after_delete(cd_evolucao_old_w);
        else
			clinical_notes_pck.gerar_soap(nr_atendimento_origem_w, nr_sequencia_p, 'CLNICAL_PATHWAY', NULL, 'P', 1, cd_evolucao_w,null,cd_pessoa_fisica_w);
        end if;
			update protocolo_int_paciente
			set cd_evolucao = cd_evolucao_w
			where nr_sequencia = nr_sequencia_p;
		commit;
    end if;

end if;

END CHANGE_STATUS_CLINICAL_PATHWAY;
/
