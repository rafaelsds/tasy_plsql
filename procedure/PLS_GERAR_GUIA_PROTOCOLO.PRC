create or replace
procedure pls_gerar_guia_protocolo
			(	nr_seq_protocolo_p	number,
				cd_guia_p		varchar2,
				nr_seq_prestador_p	number,
				nm_usuario_p		varchar2) is 
				
nr_seq_guia_w			number(10);
ie_tipo_guia_w			varchar2(2);
nr_seq_segurado_w		number(10,0);
cd_estabelecimento_w		number(4,0);
nr_seq_conta_w			number(10,0);
nr_seq_protocolo_w		number(10,0);
nr_seq_outorgante_w		number(10,0);

/* Eder Jhoney da Silva - 12/06/2009
 Parametros utilizados ao inserir procedimentos na pls_conta_proc */
cd_procedimento_w		number(15,0);
cd_procedimento_imp_w		number(15,0);
cd_tipo_tabela_imp_w		varchar2(10);
dt_liberacao_w			date;
ie_cobranca_prevista_w		varchar2(1);
ie_origem_proced_w		number(10,0);
nr_seq_regra_liberacao_w	number(10,0);

/* Eder Jhoney da Silva - 12/06/2009
 Parametros utilizados ao inserir procedimentos na pls_conta_mat */
cd_material_imp_w		varchar2(20);
nr_seq_material_w 		number(10,0);
cd_cooperativa_w		varchar2(10);


cursor C01 is
	select 	cd_procedimento, 
		cd_procedimento_imp, 
		cd_tipo_tabela_imp, 
		dt_liberacao,  
		ie_cobranca_prevista, 
		ie_origem_proced,
		nr_seq_regra_liberacao
	from	pls_guia_plano_proc
	where	nr_seq_guia = nr_seq_guia_w;
	
cursor C02 is
	select	cd_material_imp,
		dt_liberacao,
		nr_seq_material
	from	pls_guia_plano_mat
	where	nr_seq_guia = nr_seq_guia_w;


begin
select	ie_tipo_guia,
	nr_seq_segurado,
	cd_estabelecimento,
	nr_sequencia,
	substr(obter_cooperativa_benef(nr_seq_segurado,cd_estabelecimento),1,4)
into	ie_tipo_guia_w,
	nr_seq_segurado_w,
	cd_estabelecimento_w,
	nr_seq_guia_w,
	cd_cooperativa_w
from    pls_guia_plano
where   cd_guia = cd_guia_p;

select	pls_conta_seq.nextval
into	nr_seq_conta_w
from 	dual;

if	(ie_tipo_guia_w = '1' or ie_tipo_guia_w = '8') then
	ie_tipo_guia_w := '5';
elsif 	(ie_tipo_guia_w = '2') then
	ie_tipo_guia_w := '4';
end if;

if	(nr_seq_protocolo_p is not null) then
	nr_seq_protocolo_w := nr_seq_protocolo_p;
else
	select	pls_protocolo_conta_seq.nextval
	into	nr_seq_protocolo_w
	from 	dual;
		
	select  nr_sequencia 
	into	nr_seq_outorgante_w
	from    pls_outorgante 
	where   cd_estabelecimento = cd_estabelecimento_w;
 
	insert into pls_protocolo_conta
		(nr_sequencia, dt_mes_competencia, dt_protocolo,
		nr_seq_prestador, ie_tipo_guia, ie_status,
		dt_atualizacao, dt_atualizacao_nrec, nm_usuario,
		nm_usuario_nrec, cd_estabelecimento, ie_situacao,
		nr_seq_outorgante, ie_tipo_protocolo, ie_origem_protocolo)
	values(	nr_seq_protocolo_w, sysdate, sysdate,
		nr_seq_prestador_p, ie_tipo_guia_w, '1',
		sysdate, sysdate, nm_usuario_p,
		nm_usuario_p, cd_estabelecimento_w, 'DW',
		nr_seq_outorgante_w, 'C', 'D');
end if;

insert into pls_conta  
	(nr_sequencia, cd_guia, ie_tipo_guia,
	nr_seq_segurado, nr_seq_prestador_exec, ie_status,
	dt_atualizacao, dt_atualizacao_nrec, nm_usuario,
	nm_usuario_nrec, nr_seq_protocolo, cd_estabelecimento,
	cd_cooperativa, ie_origem_conta)
values(	nr_seq_conta_w, cd_guia_p, ie_tipo_guia_w,
	nr_seq_segurado_w, nr_seq_prestador_p, 'U',
	sysdate, sysdate, nm_usuario_p,
	nm_usuario_p, nr_seq_protocolo_w, cd_estabelecimento_w,
	cd_cooperativa_w, 'D');

open C01;
loop
fetch C01 into
	cd_procedimento_w,
	cd_procedimento_imp_w,
	cd_tipo_tabela_imp_w,
	dt_liberacao_w,
	ie_cobranca_prevista_w,
	ie_origem_proced_w,
	nr_seq_regra_liberacao_w;
exit when C01%notfound;
	begin
	insert into pls_conta_proc
		(nr_sequencia, nr_seq_conta, ie_situacao,
		ie_status, nm_usuario, nm_usuario_nrec,
		dt_atualizacao, dt_atualizacao_nrec, cd_procedimento_imp,
		cd_procedimento, cd_tipo_tabela_imp, dt_liberacao,
		ie_cobranca_prevista, ie_origem_proced, nr_seq_regra_liberacao)
	values(	pls_conta_proc_seq.nextval, nr_seq_conta_w, 'D',
		'U', nm_usuario_p, nm_usuario_p,
		sysdate, sysdate, cd_procedimento_imp_w,
		cd_procedimento_w, cd_tipo_tabela_imp_w, dt_liberacao_w,
		ie_cobranca_prevista_w, ie_origem_proced_w, nr_seq_regra_liberacao_w);
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into
	cd_material_imp_w,		
	dt_liberacao_w,
	nr_seq_material_w;
exit when C02%notfound;
	begin
	insert into pls_conta_mat
		(nr_sequencia, nr_seq_conta, ie_situacao,
		ie_status, nm_usuario, nm_usuario_nrec,
		dt_atualizacao, dt_atualizacao_nrec, cd_material_imp,
		dt_liberacao, nr_seq_material)
	values(	pls_conta_mat_seq.nextval, nr_seq_conta_w, 'D',
		'U', nm_usuario_p, nm_usuario_p,
		sysdate, sysdate, cd_material_imp_w,
		dt_liberacao_w, nr_seq_material_w);
	end;
end loop;
close C02;

commit;

end pls_gerar_guia_protocolo;
/
