create or replace
procedure sus_atualiza_itens_sh_conta (	nr_interno_conta_p	number,
					nm_usuario_p		varchar2) is


nr_seq_propaci_w	number(10);
nr_seq_matpaci_w	number(10);
vl_receita_w		number(15,2);
qt_mat_rateio_w		number(10);


/* busca os itens rateados da conta*/
cursor c01 is
select	nvl(nr_seq_propaci,0) nr_seq_propaci, 
	nvl(nr_seq_matpaci,0) nr_seq_matpaci, 
	vl_receita
from 	w_conta_sus_sh
where	nr_interno_conta	= nr_interno_conta_p;

type 		fetch_array is table of c01%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_c01_w			vetor;

begin

open c01;
loop
fetch c01 bulk collect into s_array limit 100000;
	vetor_c01_w(i) := s_array;
	i := i + 1;
exit when c01%notfound;
end loop;
close c01;

for i in 1..vetor_c01_w.count loop
	begin
	s_array := vetor_c01_w(i);
	for z in 1..s_array.count loop
		begin
		
		nr_seq_propaci_w	:= s_array(z).nr_seq_propaci;
		nr_seq_matpaci_w	:= s_array(z).nr_seq_matpaci;
		vl_receita_w		:= s_array(z).vl_receita;
		
		/* Atualizar os procedimentos da conta */
		if	(nr_Seq_propaci_w	> 0) then
	
			select	count(1)
			into	qt_mat_rateio_w
			from	w_conta_sus_sh
			where	nr_interno_conta = nr_interno_conta_p
			and	nr_seq_matpaci is not null
			and	vl_receita > 0
			and	rownum = 1;
		
			if	(qt_mat_rateio_w > 0) then
				begin
				
				update	procedimento_paciente
				set	vl_procedimento	= vl_receita_w,
					vl_materiais	= vl_receita_w
				where	nr_sequencia	= nr_seq_propaci_w;
			
				end;
			end if;
		/* Atualizar os materiais da conta */
		elsif	(nr_seq_matpaci_w	> 0) then
			update	material_atend_paciente
			set	vl_material	= vl_receita_w
			where	nr_sequencia	= nr_seq_matpaci_w;
		end if;	
		
		end;
	end loop;
	end;
end loop;

end sus_atualiza_itens_sh_conta;
/
