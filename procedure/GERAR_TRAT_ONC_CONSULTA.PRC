create or replace
procedure gerar_trat_onc_consulta(	cd_pessoa_fisica_p	varchar2,
					nm_usuario_p		Varchar2) is 

nr_seq_paciente_w		number(10);
nr_seq_atendimento_w		number(10);
cd_protocolo_w			number(10);
ds_protocolo_w			varchar2(255);
nr_seq_medicacao_w		number(6);
ds_subtipo_w			varchar2(255);
nr_ciclo_w			number(3);
ds_dia_ciclo_w			varchar2(5);
dt_real_w			date;
nr_agrupamento_w		number(6);
cd_material_w			number(6);
ds_material_w			varchar2(255);
qt_dose_prescr_w		number(18,6);
cd_unid_med_prescr_w		varchar2(30);
ie_via_aplicacao_w		varchar2(5);
nr_sequencia_w			number(10);
ds_informacao_w			varchar2(255);
ie_status_agenda_w		varchar2(10);
ie_status_paciente_w		varchar2(10);
ie_prescr_suspendida_w		varchar2(10);
ie_prescr_liberada_w		varchar2(10);
ie_exige_liberacao_w		varchar2(10);
ie_ciclo_autorizado_w		varchar2(10);
pr_reducao_w			number(15,4);
nr_prescricao_w			number(10);
dt_real_2_w			date;
ds_administracao_w		varchar2(60);

Cursor C01 is
	select	cd_protocolo,
		substr(obter_desc_protocolo(cd_protocolo),1,255),
		nr_seq_medicacao,
		substr(obter_desc_protocolo_medic(nr_seq_medicacao,cd_protocolo),1,255),
		nr_seq_paciente
	from	paciente_setor a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	cd_protocolo is not null
	and	exists (select	1 
			from	paciente_atendimento x
			where	x.nr_seq_paciente = a.nr_seq_paciente
			and	x.dt_cancelamento is null
			and	x.dt_suspensao is null)
	order by nr_seq_paciente desc;

Cursor C02 is
	select	nr_ciclo,
		ds_dia_ciclo,
		nvl(dt_real,dt_prevista) dt_dia,
		nr_seq_atendimento,
		substr(Qt_Obter_Status_Agenda(nr_seq_atendimento),1,50),
		substr(Obter_status_Paciente_qt(NR_SEQ_ATENDIMENTO,dt_inicio_adm,dt_fim_adm,nr_seq_local,ie_exige_liberacao,dt_chegada,'C'),1,60),
		substr(Obter_prescricao_suspenca(nr_prescricao),1,2),
		substr(obter_se_prescricao_liberada(nr_prescricao,'E'),1,2),
		ie_exige_liberacao,
		substr(Obter_Se_Dia_Ciclo_Autorizado(nr_seq_atendimento,nr_ciclo),1,255),
		pr_reducao,
		nr_prescricao,
		dt_real
	from	paciente_atendimento
	where	nr_seq_paciente = nr_seq_paciente_w
	and	dt_cancelamento is null
	and	dt_suspensao is null
	order by nr_ciclo, dt_dia;

Cursor C03 is
	select	nr_agrupamento,
		cd_material,
		substr(obter_desc_material(cd_material),1,60),
		nvl(qt_dose_prescricao,qt_dose) dose,
		nvl(cd_unid_med_prescr,cd_unid_med_dose),
		ie_via_aplicacao,
		substr(obter_valor_dominio(1853,ie_administracao),1,255)
	from	paciente_atend_medic
	where	nr_seq_atendimento = nr_seq_atendimento_w
	and	dt_cancelamento is null
	and	nr_seq_diluicao is null
	and	nr_seq_medic_material is null
	and	nr_seq_procedimento is null
	order by 1, 3;

begin

delete	from w_consulta_onc
where	dt_atualizacao < sysdate - 12/24;

delete	from w_consulta_onc
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	nm_usuario		= nm_usuario_p;

commit;

select	w_consulta_onc_seq.nextval
into	nr_sequencia_w
from	dual;

open C01;
loop
fetch C01 into
	cd_protocolo_w,
	ds_protocolo_w,
	nr_seq_medicacao_w,
	ds_subtipo_w,
	nr_seq_paciente_w;
exit when C01%notfound;
	begin
	insert into w_consulta_onc (	
			nr_sequencia,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			ds_informacao)
	values(		w_consulta_onc_seq.nextval,
			cd_pessoa_fisica_p,
			sysdate,
			nm_usuario_p,
			substr(ds_protocolo_w || ' - ' || ds_subtipo_w,1,255));

	open C02;
	loop
	fetch C02 into	
		nr_ciclo_w,
		ds_dia_ciclo_w,
		dt_real_w,
		nr_seq_atendimento_w,
		ie_status_agenda_w,
		ie_status_paciente_w,
		ie_prescr_suspendida_w,
		ie_prescr_liberada_w,
		ie_exige_liberacao_w,
		ie_ciclo_autorizado_w,
		pr_reducao_w,
		nr_prescricao_w,
		dt_real_2_w;
	exit when C02%notfound;
		begin
		ds_informacao_w	:= '    C'||nr_ciclo_w || ' - ' || ds_dia_ciclo_w || ' - ' || dt_real_w;
		
		insert into w_consulta_onc (	
				nr_sequencia,
				cd_pessoa_fisica,
				dt_atualizacao,
				nm_usuario,
				ds_informacao,
				ie_status_agenda,
				ie_status_paciente,
				ie_prescr_suspendida,
				ie_prescr_liberada,
				ie_exige_liberacao,
				ie_ciclo_autorizado,
				pr_reducao,
				nr_prescricao,
				dt_real)
		values(		w_consulta_onc_seq.nextval,
				cd_pessoa_fisica_p,
				sysdate,
				nm_usuario_p,
				substr(ds_informacao_w,1,255),
				ie_status_agenda_w,
				ie_status_paciente_w,
				ie_prescr_suspendida_w,
				ie_prescr_liberada_w,
				ie_exige_liberacao_w,
				ie_ciclo_autorizado_w,
				pr_reducao_w,
				nr_prescricao_w,
				dt_real_2_w);
		open C03;
		loop

		fetch C03 into	
			nr_agrupamento_w,
			cd_material_w,
			ds_material_w,
			qt_dose_prescr_w,
			cd_unid_med_prescr_w,
			ie_via_aplicacao_w,
			ds_administracao_w;
		exit when C03%notfound;

			begin
			insert into w_consulta_onc (	
					nr_sequencia,
					cd_pessoa_fisica,
					dt_atualizacao,
					nm_usuario,
					ds_informacao,
					qt_dose_prescricao,
					cd_unid_med_prescr,
					ie_via_aplicacao,
					ds_administracao)
			values(		w_consulta_onc_seq.nextval,
					cd_pessoa_fisica_p,
					sysdate,
					nm_usuario_p,
					substr('        '||ds_material_w,1,255),
					qt_dose_prescr_w,
					cd_unid_med_prescr_w,
					ie_via_aplicacao_w,
					ds_administracao_w);
			end;
		end loop;
		
		close C03;
		end;
	end loop;

	close C02;
	end;
end loop;

close C01;

commit;

end gerar_trat_onc_consulta;
/
