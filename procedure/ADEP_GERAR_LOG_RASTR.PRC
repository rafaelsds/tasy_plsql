create or replace
procedure adep_gerar_log_rastr( ds_log_p varchar2,
                                ie_commit_p varchar2 default 'S' ) is


ie_tipo_processo_w rastreabilidade_adep.ie_tipo_processo%type;
qt_dias_permanencia_w rastreabilidade_adep.qt_dias_permanencia%type;
ie_deletar_log_w varchar2(1 char);

begin

if (sysdate between trunc(sysdate,'dd') + 3/24 and trunc(sysdate, 'dd') + 4/24) then
    select  nvl(max('S'), 'N')
    into    ie_deletar_log_w
    from    log_adep a
    where   a.dt_exclusao < sysdate;

    if (ie_deletar_log_w = 'S') then
        delete  from log_adep a
        where   a.dt_exclusao < sysdate
        and     rownum < 100000;
    end if;
end if;

select  max(a.ie_tipo_processo),
        nvl(max(qt_dias_permanencia), 7)
into    ie_tipo_processo_w,
        qt_dias_permanencia_w
from    rastreabilidade_adep a
where   a.nr_sequencia = wheb_assist_pck.obter_seq_rastr_adep;

insert into log_adep ( nr_sequencia,
                       dt_atualizacao,
                       dt_exclusao,
                       nm_usuario,
                       cd_perfil,
                       cd_estabelecimento,
                       cd_funcao,
                       nr_prescricao,
                       ie_tipo_processo,
                       ds_log,
                       ds_stack )
                       values ( log_adep_seq.nextval,
                       sysdate,
                       trunc(sysdate,'dd') + qt_dias_permanencia_w,
                       wheb_usuario_pck.get_nm_usuario,
                       wheb_usuario_pck.get_cd_perfil,
                       wheb_usuario_pck.get_cd_estabelecimento,
                       wheb_usuario_pck.get_cd_funcao,
                       wheb_assist_pck.obter_chave_rastr_adep,
                       ie_tipo_processo_w,
                       substr(ds_log_p,1,4000),
                       substr(dbms_utility.format_call_stack,1,2000) );                 

if  (ie_commit_p = 'S') then
    commit;
end if;

end adep_gerar_log_rastr;
/
