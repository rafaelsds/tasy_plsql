create or replace
procedure consistir_dispensacao_farmacia	(nr_prescricao_p	number) is

ie_libera_w			varchar2(1);
ie_prescr_cpoe_w	varchar2(1);

begin

if	(nr_prescricao_p is not null) then

	select	nvl(max('S'), 'N')
	into	ie_prescr_cpoe_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p
	and		cd_funcao_origem = 2314;

	if	(ie_prescr_cpoe_w = 'N') then
	
	    select	decode(count(*),0,'S','N')
		into	ie_libera_w
		from	prescr_material a			
		where	nr_prescricao		= nr_prescricao_p
		and		ie_agrupador		in (1,4,5,8)
		and		dt_suspensao is null
		and		nvl(ie_suspenso,'N')	= 'N'
		and		nvl(ie_regra_disp,'S') 	= 'U'
		and		nvl(ie_administrar,'S') = 'S';
	
	    if	(ie_libera_w = 'N') then
		/*(-20011,	'Esta prescri��o possui itens com dispensa��o � definir.' || chr(10) ||
						'Voc� n�o tem autoriza��o para liberar prescri��es nestas    condi��es.' || chr(10) ||
						'Favor verificar.#@#@');*/
			Wheb_mensagem_pck.exibir_mensagem_abort(263534);
		end if;
		
	else	
	
		select	decode(count(*),0,'S','N')
		into	ie_libera_w
		from	prescr_material a,
				cpoe_material b
		where	a.nr_seq_mat_cpoe   = b.nr_sequencia
		and		a.nr_prescricao		= nr_prescricao_p
		and		a.ie_agrupador		in (1,4,5)				
		and		nvl(a.ie_regra_disp,'S') 	= 'U'
		and		nvl(a.ie_administrar,'S') = 'S'
		and		(b.dt_suspensao is null or  b.dt_suspensao > trunc(sysdate,'mi'));
		
		 if	(ie_libera_w = 'N') then
		/*(-20011,	'Esta prescri��o possui itens com dispensa��o � definir.' || chr(10) ||
						'Voc� n�o tem autoriza��o para liberar prescri��es nestas    condi��es.' || chr(10) ||
						'Favor verificar.#@#@');*/
			Wheb_mensagem_pck.exibir_mensagem_abort(263534);
		end if;
		
		select	decode(count(*),0,'S','N')
		into	ie_libera_w
		from	prescr_material a,
				cpoe_material b
		where	a.nr_seq_dieta_cpoe = b.nr_sequencia
		and		a.nr_prescricao		= nr_prescricao_p
		and		a.ie_agrupador		= 8				
		and		nvl(a.ie_regra_disp,'S') 	= 'U'
		and		nvl(a.ie_administrar,'S') = 'S'
		and		(b.dt_suspensao is null or  b.dt_suspensao > trunc(sysdate,'mi'));
		
		 if	(ie_libera_w = 'N') then
		/*(-20011,	'Esta prescri��o possui itens com dispensa��o � definir.' || chr(10) ||
						'Voc� n�o tem autoriza��o para liberar prescri��es nestas    condi��es.' || chr(10) ||
						'Favor verificar.#@#@');*/
			Wheb_mensagem_pck.exibir_mensagem_abort(263534);
		end if;
		
	end if;
	
end if;

end consistir_dispensacao_farmacia;
/