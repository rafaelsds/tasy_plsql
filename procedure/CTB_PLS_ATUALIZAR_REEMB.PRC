create or replace
procedure ctb_pls_atualizar_reemb
			(	nr_seq_protocolo_p	number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	number) is 

dt_referencia_w			date;
ie_tipo_contratacao_w		Varchar2(15);
nr_seq_conta_w			Number(10);
cd_conta_contabil_w		Varchar2(20);
ie_preco_w			varchar2(2);
ie_segmentacao_w		varchar2(3);
ie_regulamentacao_w		varchar2(2);
ie_participacao_w		varchar2(1);
nr_seq_regra_w			number(10);
ie_tipo_beneficiario_w		varchar2(3);
cd_historico_w			number(20);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
ie_tipo_despesa_w		varchar2(1);
cd_historico_copartic_w		number(10);
cd_conta_coparticipacao_w	varchar2(20);
nr_seq_conta_item_w		number(10);
nr_seq_grupo_ans_w		number(10);
ie_tipo_segurado_w		varchar2(3);
ie_tipo_item_w			Number(1);
nr_seq_material_w		Number(10);
cd_conta_glosa_w		Varchar(20);
cd_historico_glosa_w		Number(10);

Cursor C01 is
	select	b.nr_sequencia,
		d.ie_tipo_contratacao,
		d.ie_preco,
		d.ie_segmentacao,
		d.ie_regulamentacao,
		d.ie_participacao,
		substr(pls_obter_dados_contrato(c.nr_seq_contrato,'TB'),1,3),
		trunc(a.dt_mes_competencia,'month'),
		c.ie_tipo_segurado
	from	pls_plano 		d,
		pls_segurado 		c,		
		pls_conta		b,
		pls_protocolo_conta	a		
	where	a.nr_sequencia		= nr_seq_protocolo_p
	and	a.nr_sequencia		= b.nr_seq_protocolo
	and	b.nr_seq_segurado	= c.nr_sequencia
	and	c.nr_seq_plano		= d.nr_sequencia;
	
Cursor C02 is
	select	1,
		nr_sequencia,
		cd_procedimento,
		ie_origem_proced,
		null
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_w
	union all
	select	2,
		nr_sequencia,
		null,
		null,
		ie_tipo_despesa
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_w;

begin
open C01;
loop
fetch C01 into
	nr_seq_conta_w,
	ie_tipo_contratacao_w,
	ie_preco_w,
	ie_segmentacao_w,
	ie_regulamentacao_w,
	ie_participacao_w,
	ie_tipo_beneficiario_w,
	dt_referencia_w,
	ie_tipo_segurado_w;
exit when c01%notfound;
	begin
	delete	sip_resumo_conta
	where	nr_seq_conta	= nr_seq_conta_w;

	open C02;
	loop
	fetch C02 into	
		ie_tipo_item_w,
		nr_seq_conta_item_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		ie_tipo_despesa_w;
	exit when C02%notfound;
		begin
		
		/*Diego OS 306868 - No caso de n�o haver regras adequadas. No final s� ira fazer  o update caso haja regra.*/
		if 	((cd_procedimento_w is not null) and (ie_origem_proced_w is not null)) then
			update	pls_conta_proc
			set	cd_conta_deb		= null,
				nr_seq_regra_ctb_deb	= null,				
				cd_historico_copartic 	= null,
				cd_conta_copartic_deb	= null,				
				cd_conta_cred		= null,
				nr_seq_regra_ctb_cred	= null,
				cd_historico		= null,				
				cd_conta_copartic_cred	= null,
				nr_seq_grupo_ans	= null				
			where	nr_sequencia		= nr_seq_conta_item_w;
		elsif 	(ie_tipo_despesa_w is not null) then
			update	pls_conta_mat
			set	cd_conta_deb		= null,
				nr_seq_regra_ctb_deb	= null,				
				nr_seq_grupo_ans	= null,
				cd_conta_cred		= null,
				nr_seq_regra_ctb_cred	= null,
				cd_historico		= null				
			where	nr_sequencia		= nr_seq_conta_item_w;
		end if;		
		
		nr_seq_material_w	:= null;
		if	(ie_tipo_item_w	= 2) then
			nr_seq_material_w	:= nr_seq_conta_item_w;
		end if;
		/* Conta de receita */
		ctb_pls_obter_conta_reemb(cd_estabelecimento_p, dt_referencia_w, 'C',
					ie_tipo_contratacao_w, ie_preco_w, ie_segmentacao_w,
					ie_regulamentacao_w, ie_participacao_w, ie_tipo_beneficiario_w,
					cd_procedimento_w, ie_origem_proced_w, ie_tipo_despesa_w,
					nr_seq_conta_w, ie_tipo_segurado_w, nr_seq_conta_item_w,
					nr_seq_material_w, null, null,
					null, nr_seq_regra_w, cd_historico_w, 
					cd_conta_contabil_w, cd_historico_copartic_w, cd_conta_coparticipacao_w, 
					nr_seq_grupo_ans_w, cd_conta_glosa_w, cd_historico_glosa_w);
		if 	(to_number(nr_seq_regra_w) > 0) then
			if	(nvl(cd_historico_glosa_w,0) = 0) then
				cd_historico_glosa_w := null;
			end if;
			
			if (nr_seq_grupo_ans_w = 0) then
				nr_seq_grupo_ans_w := null;
			end if;
			
			if 	((cd_procedimento_w is not null) and (ie_origem_proced_w is not null)) then
				update	pls_conta_proc
				set	cd_conta_cred		= cd_conta_contabil_w,
					nr_seq_regra_ctb_cred	= nr_seq_regra_w,
					cd_historico		= cd_historico_w,
					cd_historico_copartic 	= cd_historico_copartic_w,
					cd_conta_copartic_cred	= cd_conta_coparticipacao_w,
					nr_seq_grupo_ans	= nr_seq_grupo_ans_w,
					cd_historico_glosa	= cd_historico_glosa_w,
					cd_conta_glosa_cred	= cd_conta_glosa_w
				where	nr_sequencia		= nr_seq_conta_item_w;
			elsif 	(ie_tipo_despesa_w is not null) then
				update	pls_conta_mat
				set	cd_conta_cred		= cd_conta_contabil_w,
					nr_seq_regra_ctb_cred	= nr_seq_regra_w,
					cd_historico		= cd_historico_w,
					nr_seq_grupo_ans	= nr_seq_grupo_ans_w,
					cd_historico_glosa	= cd_historico_glosa_w,
					cd_conta_glosa_cred	= cd_conta_glosa_w
				where	nr_sequencia		= nr_seq_conta_item_w;
			end if;
		end if;
		
		/* Conta de d�bito */
		
		
		/*if	(nr_seq_protocolo_p = 5003) then
		ctb_pls_obter_conta_reemb2(cd_estabelecimento_p, dt_referencia_w, 'D',
					ie_tipo_contratacao_w, ie_preco_w, ie_segmentacao_w,
					ie_regulamentacao_w, ie_participacao_w, ie_tipo_beneficiario_w,
					cd_procedimento_w, ie_origem_proced_w, ie_tipo_despesa_w,
					nr_seq_conta_w, ie_tipo_segurado_w, nr_seq_conta_item_w,
					nr_seq_material_w, null, null,
					null, nr_seq_regra_w, cd_historico_w, 
					cd_conta_contabil_w, cd_historico_copartic_w, cd_conta_coparticipacao_w, 
					nr_seq_grupo_ans_w);	
		end if;*/
		
		ctb_pls_obter_conta_reemb(cd_estabelecimento_p, dt_referencia_w, 'D',
					ie_tipo_contratacao_w, ie_preco_w, ie_segmentacao_w,
					ie_regulamentacao_w, ie_participacao_w, ie_tipo_beneficiario_w,
					cd_procedimento_w, ie_origem_proced_w, ie_tipo_despesa_w,
					nr_seq_conta_w, ie_tipo_segurado_w, nr_seq_conta_item_w,
					nr_seq_material_w, null, null,
					null, nr_seq_regra_w, cd_historico_w, 
					cd_conta_contabil_w, cd_historico_copartic_w, cd_conta_coparticipacao_w, 
					nr_seq_grupo_ans_w, cd_conta_glosa_w, cd_historico_glosa_w);		

		if 	(to_number(nr_seq_regra_w) > 0) then
			if	(nvl(cd_historico_glosa_w,0) = 0) then
				cd_historico_glosa_w := null;
			end if;
			
			if (nr_seq_grupo_ans_w = 0) then
				nr_seq_grupo_ans_w := null;
			end if;
			
			if 	((cd_procedimento_w is not null) and (ie_origem_proced_w is not null)) then
				update	pls_conta_proc
				set	cd_conta_deb		= cd_conta_contabil_w,
					nr_seq_regra_ctb_deb	= nr_seq_regra_w,
					cd_historico		= cd_historico_w,
					cd_historico_copartic 	= cd_historico_copartic_w,
					cd_conta_copartic_deb	= cd_conta_coparticipacao_w,
					nr_seq_grupo_ans	= nr_seq_grupo_ans_w,
					cd_historico_glosa	= cd_historico_glosa_w,
					cd_conta_glosa_deb	= cd_conta_glosa_w
				where	nr_sequencia		= nr_seq_conta_item_w;
			elsif 	(ie_tipo_despesa_w is not null) then
				update	pls_conta_mat
				set	cd_conta_deb		= cd_conta_contabil_w,
					nr_seq_regra_ctb_deb	= nr_seq_regra_w,
					cd_historico		= cd_historico_w,
					nr_seq_grupo_ans	= nr_seq_grupo_ans_w,
					cd_historico_glosa	= cd_historico_glosa_w,
					cd_conta_glosa_deb	= cd_conta_glosa_w
				where	nr_sequencia		= nr_seq_conta_item_w;
			end if;
		end if;
		sip_gerar_resumo_conta(nr_seq_conta_item_w, ie_tipo_item_w, nm_usuario_p);
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

end ctb_pls_atualizar_reemb;
/