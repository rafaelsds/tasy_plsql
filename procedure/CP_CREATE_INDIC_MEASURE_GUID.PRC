create or replace procedure cp_create_indic_measure_guid (
   ds_guidance_p            CP_GUIDANCE.DS_GUIDANCE%type,
   nr_seq_indic_measure_p   CP_INDICATOR_MEASURE_GUID.NR_SEQ_INDIC_MEASURE%type,
   ds_version_p             CP_GUIDANCE.DS_VERSION%type,
   nm_usuario_p             CP_INDICATOR_MEASURE_GUID.NM_USUARIO%type
) is

nr_seq_guidance_w             CP_GUIDANCE.NR_SEQUENCIA%type;
nr_seq_indic_measure_guid_w   CP_INDICATOR_MEASURE_GUID.NR_SEQUENCIA%type;
nr_sequencia_w			      CP_GUIDANCE.NR_SEQUENCIA%type;
ds_version_w			      CP_GUIDANCE.DS_VERSION%type;
ie_situacao_w			      CP_GUIDANCE.IE_SITUACAO%type;

begin

    begin
        select	max(nr_sequencia),
                ds_version
        into	nr_sequencia_w,
                ds_version_w
        from	CP_GUIDANCE
        where	DS_GUIDANCE = ds_guidance_p
        group by ds_version;
        exception when others then
            nr_sequencia_w := null;
            ds_version_w := null;
    end;

    if (nr_sequencia_w is null or ds_version_w <> ds_version_p) then
        if (nr_sequencia_w is null) then
            ie_situacao_w	:= 'A';
        else
            ie_situacao_w	:= 'I';
        end if;

        select  CP_GUIDANCE_SEQ.nextval
        into    nr_seq_guidance_w
        from    dual;

        insert into CP_GUIDANCE (
            DS_GUIDANCE,
            DT_ATUALIZACAO,
            DT_ATUALIZACAO_NREC,
            IE_ORIGIN,
            IE_SITUACAO,
            NM_USUARIO,
            NM_USUARIO_NREC,
            NR_SEQUENCIA
        ) values (
            ds_guidance_p,
            sysdate,
            sysdate,
            'E', -- From Elsevier
            ie_situacao_w,
            nm_usuario_p,
            nm_usuario_p,
            nr_seq_guidance_w
        );

        select  CP_INDICATOR_MEASURE_GUID_SEQ.nextval
        into    nr_seq_indic_measure_guid_w
        from    dual;

        insert into CP_INDICATOR_MEASURE_GUID (
            DT_ATUALIZACAO,
            DT_ATUALIZACAO_NREC,
            IE_ORIGIN,
            IE_SITUACAO,
            NM_USUARIO,
            NM_USUARIO_NREC,
            NR_SEQ_GUIDANCE,
            NR_SEQ_INDIC_MEASURE,
            NR_SEQUENCIA
        ) values (
            sysdate,
            sysdate,
            'E', -- From Elsevier
            ie_situacao_w,
            nm_usuario_p,
            nm_usuario_p,
            nr_seq_guidance_w,
            nr_seq_indic_measure_p,
            nr_seq_indic_measure_guid_w
        );

    end if;

end cp_create_indic_measure_guid;
/
