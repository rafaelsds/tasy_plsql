create or replace
procedure pls_retirar_impugnacao_proced
			(	nr_seq_impug_proc_p	in	pls_impugnacao_proc.nr_sequencia%type,
				nm_usuario_p		in	usuario.nm_usuario%type) is 


begin

delete	from pls_impugnacao_proc
where	nr_sequencia	= nr_seq_impug_proc_p;

commit;

end pls_retirar_impugnacao_proced;
/
