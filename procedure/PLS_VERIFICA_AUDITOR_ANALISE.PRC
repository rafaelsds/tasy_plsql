create or replace
procedure pls_verifica_auditor_analise(	nr_seq_auditoria_p	in	Number,
					nr_seq_grupo_analise_p	in	Number,
					nm_usuario_p		in	Varchar2,
					ie_opcao_p		in	Number,
					ie_retorno_p		out	Varchar2) is 
/*
IE_OPCAO_P
1 - Verificar se j� existe um auditor realizando a an�lise
*/

qt_registros_w			Number(10);

begin

if	(ie_opcao_p	= 1) then
	select	count(1)
	into	qt_registros_w
	from	pls_analise_auditor_aut
	where	nr_seq_auditoria	= nr_seq_auditoria_p
	and	dt_inicio_analise	is not null
	and	dt_fim_analise		is null;
	
	if	(qt_registros_w	> 0) then
		select	count(1)
		into	qt_registros_w
		from	pls_analise_auditor_aut
		where	nr_seq_auditoria	= nr_seq_auditoria_p
		and	dt_inicio_analise	is not null
		and	dt_fim_analise		is null
		and	nm_usuario		= nm_usuario_p;
		
		if	(qt_registros_w > 0) then
			ie_retorno_p	:= 'A';
		else
			ie_retorno_p	:= 'S';
		end if;
	else
		ie_retorno_p	:= 'N';
	end if;
end if;

commit;

end pls_verifica_auditor_analise;
/