CREATE OR REPLACE
PROCEDURE suspender_dia_trat_onco_alta(	nr_prescricao_p		Number,
						nr_seq_atendimento_p	Number,
						nm_usuario_p		Varchar,
						ie_obito_p		varchar2 default 'N') is


nr_seq_ordem_w			Number(10);
dt_agenda_w			Date;
	
nr_seq_medic_w			number(10);
cd_estabelecimento_w		number(4,0); 
ie_permite_cancelar_w		varchar2(1);

nr_seq_agenda_cons_w		number(10);
nr_seq_agequi_w			number(10);
ie_cancelar_autor_conv_w	varchar2(15);
ie_status_agenda_w		varchar2(3);
ie_Status_agenda_quimi_w	varchar2(3);
nr_seq_paciente_setor_w		number(10,0);
nr_ciclo_w			number(3,0);
ds_retorno_receita_w		varchar2(4000);
nr_seq_paciente_w		number(10);
nr_seq_ageint_item_w	number(10);
nr_seq_pendencia_w	number(10);

cursor C02 is
	select 	a.nr_seq_interno
	from	paciente_atendimento b,
		paciente_atend_medic a
	where	a.nr_seq_atendimento	= b.nr_seq_atendimento
	and	a.nr_seq_atendimento	= nr_seq_atendimento_p;

Cursor C01 is
	select	b.nr_seq_ordem
	from	can_ordem_item_prescr b,
		prescr_material a
	where	a.nr_prescricao		= nr_prescricao_p
	and	nr_seq_atend_medic	= nr_seq_medic_w
	and	a.nr_prescricao		= b.nr_prescricao
	and	a.nr_sequencia		= b.nr_seq_prescricao;

BEGIN

select	max(nvl(cd_estabelecimento,0))
into	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao	=	nr_prescricao_p;

Obter_Param_Usuario(281,282,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_permite_cancelar_w);
Obter_Param_Usuario(281,1101,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_cancelar_autor_conv_w);
Obter_Param_Usuario(281,1199,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_Status_agenda_quimi_w);

select	trunc(nvl(max(a.dt_real),max(a.dt_prevista))),
	max(nr_seq_paciente),
	max(nr_ciclo)
into	dt_agenda_w,
	nr_seq_paciente_setor_w,
	nr_ciclo_w
from	paciente_atendimento a
where	a.nr_seq_atendimento	= nr_seq_atendimento_p;

if	(dt_agenda_w < trunc(sysdate)) and (ie_permite_cancelar_w = 'N') and
	(nvl(ie_obito_p,'N') = 'N')then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(191835);
end if;


OPEN C02;
LOOP
FETCH C02 into
	nr_seq_medic_w;
EXIT when C02%notfound;

	update	paciente_atend_medic
	set	ie_cancelada		= 'S',
		dt_cancelamento		= sysdate,
		nm_usuario_cancel	= nm_usuario_p
	where	nr_seq_interno		= nr_seq_medic_w;

	update	prescr_material
	set	ie_suspenso		= 'S',
		dt_suspensao		= sysdate,
		nm_usuario_susp		= nm_usuario_p
	where	nr_prescricao		= nr_prescricao_p
	and	nr_seq_atend_medic	= nr_seq_medic_w;

	OPEN C01;
	LOOP
	FETCH C01 into
		nr_seq_ordem_w;
	EXIT when C01%notfound;
		update	can_ordem_prod
		set	ie_cancelada	= 'S',
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_ordem_w;
	END LOOP;
	CLOSE C01;		

END LOOP;
CLOSE C02;


update	paciente_atendimento
set	dt_cancelamento		= sysdate,
	nm_usuario_cancel	= nm_usuario_p
where	nr_seq_atendimento	= nr_seq_atendimento_p;

Suspender_receita_trat_onc(nr_seq_paciente_setor_w,nr_ciclo_w,nm_usuario_p,ds_retorno_receita_w);

select	max(nr_sequencia),
		max(ie_status_agenda),
		nvl(max(nr_seq_Ageint_item),0)
into	nr_seq_agequi_w,
		ie_status_agenda_w,
		nr_seq_ageint_item_w
from	agenda_quimio
where	nr_seq_atendimento	= nr_seq_atendimento_p;

if 	(ie_status_agenda_w	<> 'C') and
	(nr_seq_agequi_w	> 0) then
	if 	(nvl(ie_obito_p, 'N') = 'S') then
		qt_alterar_status_agenda(nr_seq_agequi_w, 'C', nm_usuario_p, 'N', null, null, cd_estabelecimento_w,'');
	else
		qt_alterar_status_agenda(nr_seq_agequi_w, ie_Status_agenda_quimi_w, nm_usuario_p, 'N', null, null, cd_estabelecimento_w,'');
	end if;
end if;

select	max(nr_seq_agenda_cons),
	nvl(max(nr_seq_pend_Agenda),0)
into	nr_seq_agenda_cons_w,
	nr_seq_pendencia_w
from	paciente_atendimento
where	nr_seq_atendimento = nr_seq_atendimento_p;

if	(nr_seq_ageint_item_w	> 0) then
	begin
		delete	agenda_quimio_marcacao
		where	nr_seq_ageint_item	= nr_seq_ageint_item_w;
	exception
	when others then
		nr_seq_ageint_item_w := nvl(nr_seq_ageint_item_w, 0);
	end;
elsif	(nr_seq_pendencia_w	> 0) then		
	begin
		delete	agenda_quimio_marcacao
		where	nr_seq_pend_agenda	= nr_seq_pendencia_w
		and	trunc(dt_Agenda)	= trunc(dt_Agenda_w);
		
		delete w_agenda_quimio 
		where ie_status = 'AG' 
		and nr_seq_atendimento = nr_Seq_atendimento_p;
	exception
	when others then
		nr_seq_ageint_item_w := nvl(nr_seq_ageint_item_w, 0);
	end;
end if;


if (nr_seq_agenda_cons_w is not null) then
	Cancelar_Agenda_Consulta(nr_seq_agenda_cons_w, nm_usuario_p);
end if;

if	(nvl(ie_cancelar_autor_conv_w,'N') = 'S') then
	update	autorizacao_convenio
	set	nr_seq_estagio	=(	select	max(x.nr_sequencia)
					from	estagio_autorizacao x
					where	x.ie_interno = '70'
          				and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = x.cd_empresa),
		nm_usuario	= nm_usuario_p,
		dt_atualizacao 	= sysdate
	where	((nr_seq_paciente = nr_seq_atendimento_p) or 
		((nr_seq_paciente is null) and
		(nr_seq_paciente_setor = nr_seq_paciente_setor_w) and (nr_ciclo = nr_ciclo_w)));
end if;



END suspender_dia_trat_onco_alta;
/
								       
