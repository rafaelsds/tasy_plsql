create or replace
procedure	html_submit_param_rule(
			nr_sequencia_p		number,
			ie_acao_p		varchar2,
			nm_usuario_p		varchar2) is

begin

if	(ie_acao_p = 'L') then
	
	update	html_param_to_rule
	set	dt_liberacao = sysdate,
		nm_usuario_liberacao = nm_usuario_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

elsif	(ie_acao_p = 'I') then

	update	html_param_to_rule
	set	dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end html_submit_param_rule;
/