create or replace
procedure qua_gerar_item_avaliar_pront(
			nr_seq_audit_pront_p		number,
			nr_seq_tipo_p			number,
			nm_usuario_p			varchar2) is 

nr_seq_estrut_w	number(10);
nr_seq_item_w	number(10);
nr_sequencia_w	number(10);

cursor C01 is
	select	nr_sequencia
	from	qua_auditoria_estrut
	where	nr_seq_tipo = nr_seq_tipo_p;

cursor C02 is
	select	nr_sequencia
	from	qua_auditoria_item
	where	nr_seq_estrutura = nr_seq_estrut_w;

begin
open C01;
loop
fetch C01 into
	nr_seq_estrut_w;
	exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into
		nr_seq_item_w;
		exit when C02%notfound;
		begin
		select	qua_audit_pront_result_seq.nextval
		into	nr_sequencia_w
		from	dual;
		insert into qua_audit_pront_result(
			nr_sequencia,
			nr_seq_audit_pront,
			dt_atualizacao,
			nm_usuario,
			nr_seq_item,
			ie_resultado,
			ds_complemento,
			nr_seq_estrutura,
			vl_item,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	
			nr_sequencia_w,
			nr_seq_audit_pront_p,
			sysdate,
			nm_usuario_p,
			nr_seq_item_w,
			null,
			null,
			nr_seq_estrut_w,
			0,
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

commit;

end qua_gerar_item_avaliar_pront;
/
