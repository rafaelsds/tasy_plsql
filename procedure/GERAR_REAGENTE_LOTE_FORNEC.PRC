create or replace
procedure gerar_reagente_lote_fornec(	nr_seq_lote_p	number,
					nm_usuario_p	varchar2) is 

ds_marca_w	varchar(255);
nr_lote_w	number(11);
nr_seq_lab_reagente_w	number(10);

begin

select 	max(nr_sequencia || nr_digito_verif),
	max(obter_desc_marca(nr_seq_marca))
into 	nr_lote_w,
	ds_marca_w
from 	material_lote_fornec
where 	nr_sequencia = nr_seq_lote_p;
	
select 	max(nr_sequencia)
into 	nr_seq_lab_reagente_w
from 	LAB_REAGENTE 
where 	ds_marca = ds_marca_w;
	
	
if (nr_seq_lab_reagente_w > 0) then
	begin

	insert into LAB_REAGENTE_LOTE	(dt_inicio_lote,
				dt_atualizacao,
				nr_lote,
				nr_sequencia,
				nm_usuario,
				nr_seq_reagente)
			values  (sysdate,
				sysdate,
				nr_lote_w,
				LAB_REAGENTE_LOTE_seq.nextval,
				nm_usuario_p,
				nr_seq_lab_reagente_w);

	end;
elsif (ds_marca_w is not null) then
	begin
		
		select LAB_REAGENTE_seq.nextval
		into nr_seq_lab_reagente_w
		from dual;
		
		insert into LAB_REAGENTE (nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ds_marca,
					nr_lote)
				values (nr_seq_lab_reagente_w,
					sysdate,
					nm_usuario_p,
					ds_marca_w,
					nr_lote_w);
		end;
		
		insert into LAB_REAGENTE_LOTE	(dt_inicio_lote,
						dt_atualizacao,
						nr_lote,
						nr_sequencia,
						nm_usuario,
						nr_seq_reagente)
					values  (sysdate,
						sysdate,
						nr_lote_w,
						LAB_REAGENTE_LOTE_seq.nextval,
						nm_usuario_p,
						nr_seq_lab_reagente_w);
end if;

commit;

end gerar_reagente_lote_fornec;
/