create or replace
procedure man_atribuir_epic_art(
			nr_epic_p		number,
			nr_art_p		number,
			nr_art_atual_p		number,
			nm_usuario_p		varchar2,
			ds_erro_p	out	varchar2) is 

nr_art_planejado_w	desenv_art.nr_sequencia%type;
dt_art_planejado_w	desenv_art.dt_inicial%type;
dt_art_final_plan_w	desenv_art.dt_final%type;
dt_final_w		desenv_art.dt_final%type;
qt_existe_w		number(10);
qt_features_art_w	number(10);
			
begin

select	max(dt_final)
into	dt_final_w
from	desenv_art
where	nr_sequencia = nr_art_p;

if	(dt_final_w < sysdate) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336110); --Este ART j� terminou.
	return;
end if;

select	count(*)
into	qt_existe_w
from	desenv_epic_art a, 
	desenv_art b
where	nr_epic = nr_epic_p
and	a.nr_art = b.nr_sequencia
and	((nr_art_p = a.nr_art) or (not sysdate between b.dt_inicial and b.dt_final))
and	nvl(b.nr_sequencia,0) <> nvl(nr_art_atual_p,0);

if	(qt_existe_w > 0) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336111); --Esta Feature j� foi planejada.
	return;
end if;

select	max(a.nr_art),
	max(b.dt_inicial),
	max(b.dt_final)
into	nr_art_planejado_w,
	dt_art_planejado_w,
	dt_art_final_plan_w
from	desenv_epic_art a,
	desenv_art b
where	a.nr_art = b.nr_sequencia
and	a.nr_epic = nr_epic_p
and	b.nr_sequencia = nr_art_atual_p;

select	count(*)
into	qt_features_art_w
from	desenv_feature_release a,
	desenv_release b,
	desenv_feature c
where	b.nr_art = nr_art_atual_p
and	a.nr_feature = c.nr_sequencia
and	c.nr_epic = nr_epic_p
and	a.nr_release is not null;

if	(dt_art_planejado_w > sysdate) or
	((dt_art_planejado_w is not null) and (qt_features_art_w = 0)) then
	update	desenv_epic_art
	set	nr_art = nr_art_p
	where	nr_art = nr_art_planejado_w
	and	nr_epic = nr_epic_p;
else
	delete	from desenv_epic_art
	where	nr_epic = nr_epic_p
	and	nr_art is null;
	
	insert	into desenv_epic_art(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_epic,
		nr_art)
	values(	desenv_epic_art_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_epic_p,
		nr_art_p);
end if;

commit;

end man_atribuir_epic_art;
/