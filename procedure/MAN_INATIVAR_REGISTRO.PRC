create or replace
procedure man_inativar_registro(
	nr_seq_filhas_p		number,
	nm_usuario_p		Varchar2) is 

begin

update 	man_ordem_serv_filha
set 	dt_inativacao = sysdate,
	ie_situacao = 'I',
	nm_usuario = nm_usuario_p,
	nm_usuario_inativacao = nm_usuario_p
where 	nr_sequencia = nr_seq_filhas_p;

commit;

end man_inativar_registro;
/