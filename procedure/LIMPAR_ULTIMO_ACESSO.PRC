create or replace
procedure limpar_ultimo_acesso ( nr_seq_doc_p	Number,
								 nm_usuario_p	Varchar2) is 

begin

update	qua_doc_log_acesso
set		ie_doc_resp = 'N'
where	nr_seq_doc = nr_seq_doc_p
and		nm_usuario = nm_usuario_p;	

commit;

end limpar_ultimo_acesso ;
/