create or replace
procedure	sup_altera_fornec_oc(
			nr_ordem_compra_p	number,
			cd_cgc_novo_p		varchar2,
			nm_usuario_p		varchar2) is

qt_existe_w		number(10);
cd_cgc_w		varchar2(20);
cd_cgc_fornecedor_w	varchar2(14);
ds_razao_velha_w	varchar2(100);
ds_razao_nova_w		varchar2(100);
ds_historico_w		varchar2(2000);
ie_sistema_origem_w	varchar2(15);

begin

select	cd_cgc_fornecedor,
	substr(obter_nome_pf_pj(null, cd_cgc_fornecedor),1,100)
into	cd_cgc_fornecedor_w,
	ds_razao_velha_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

select	count(*)
into	qt_existe_w
from	pessoa_juridica
where	cd_cgc = cd_cgc_novo_p;

if	(qt_existe_w > 0) then
	begin

	select	max(ie_sistema_origem)
	into	ie_sistema_origem_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_p;
	
	if	(ie_sistema_origem_w is not null) then
		
		update	ordem_compra
		set	ie_necessita_enviar_int	= 'S',
			nm_usuario_altera_int	= nm_usuario_p
		where	nr_ordem_compra		= nr_ordem_compra_p;
	
	end if;
	
	update	ordem_compra
	set	cd_cgc_fornecedor = cd_cgc_novo_p
	where	nr_ordem_compra = nr_ordem_compra_p;
	
	select	substr(obter_nome_pf_pj(null, cd_cgc_novo_p),1,100)
	into	ds_razao_nova_w
	from	dual;
	
	
	ds_historico_w := substr(WHEB_MENSAGEM_PCK.get_texto(306710,'CD_CGC_FORNECEDOR_W=' || cd_cgc_fornecedor_w || ';' || 'DS_RAZAO_VELHA_W=' || ds_razao_velha_w || ';' ||
									'CD_CGC_NOVO_P=' || cd_cgc_novo_p || ';' || 'DS_RAZAO_NOVA_W=' || ds_razao_nova_w),1,2000);
				
	

	end;
else	
	begin

	cd_cgc_w	:= ('0' || cd_cgc_novo_p);

	select	count(*)
	into	qt_existe_w
	from	pessoa_juridica
	where	cd_cgc = cd_cgc_w;

	if	(qt_existe_w > 0) then
		begin
		
		select	max(ie_sistema_origem)
		into	ie_sistema_origem_w
		from	ordem_compra
		where	nr_ordem_compra = nr_ordem_compra_p;
		
		if	(ie_sistema_origem_w is not null) then
			
			update	ordem_compra
			set	ie_necessita_enviar_int	= 'S',
				nm_usuario_altera_int	= nm_usuario_p
			where	nr_ordem_compra		= nr_ordem_compra_p;
		
		end if;

		update	ordem_compra
		set	cd_cgc_fornecedor = cd_cgc_w
		where	nr_ordem_compra = nr_ordem_compra_p;
		
		select	substr(obter_nome_pf_pj(null, cd_cgc_novo_p),1,100)
		into	ds_razao_nova_w
		from	dual;
		
		ds_historico_w := substr(WHEB_MENSAGEM_PCK.get_texto(306710,'CD_CGC_FORNECEDOR_W=' || cd_cgc_fornecedor_w || ';' || 'DS_RAZAO_VELHA_W=' || ds_razao_velha_w || ';' ||
									'CD_CGC_NOVO_P=' || cd_cgc_novo_p || ';' || 'DS_RAZAO_NOVA_W=' || ds_razao_nova_w),1,2000);

					
		end;

	end if;
	end;
end if;

if	(ds_historico_w is not null) then	

	insert into ordem_compra_hist(
		nr_sequencia,
		nr_ordem_compra,
		dt_atualizacao,
		nm_usuario,
		dt_historico,
		ds_titulo,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo,
		dt_liberacao,
		ie_motivo_hist)
	values(	ordem_compra_hist_seq.nextval,
		nr_ordem_compra_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		substr(WHEB_MENSAGEM_PCK.get_texto(306709),1,80),
		substr(ds_historico_w,1,400),
		sysdate,
		nm_usuario_p,
		'S',
		sysdate,
		'F');
	
/*	
	inserir_historico_ordem_compra(
		nr_ordem_compra_p,
		'S',
		'Altera��o fornecedor',
		ds_historico_w,
		nm_usuario_p);
*/
end if;

commit;

end sup_altera_fornec_oc;
/
