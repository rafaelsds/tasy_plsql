create or replace
procedure Consistir_hor_padrao_interv(	ds_horarios_p		varchar2,
										cd_intervalo_p		varchar2,
										dt_primeiro_horario_p	date,
										ds_erro_p		out varchar2,
										ie_item_cpoe_p		varchar2 default 'N') is 

ds_hor_aux_w			varchar2(2000);
x						number(15,0);
qt_operacao_w			intervalo_prescricao.qt_operacao%type;
ie_operacao_w			varchar2(1);
qt_tamanho_w			number(10);
ds_horarios_w			varchar2(2000);
qt_horarios_w			number(10);
qt_pos_atual_w			number(10);
qt_horario_interv_w		number(10);
ds_intervalo_w			varchar2(40);
ds_horario_w			varchar2(10);
qt_pos_espaco_w			number(10);
ie_invalido_w			varchar2(1)	:= 'N';
dt_final_w				date;
dt_horario_w			date;
dt_primeiro_horario_w	date;
dt_horario_anterior_w	date;
ie_novo_dia_w			varchar2(1)	:= 'N';
qt_controle_w			number(10);

begin

ds_hor_aux_w	:= substr(replace(replace(ds_horarios_p,' ',''),':',''),1,2000);

x	:= 1;
while	(x	< length(ds_horarios_p)) loop
	if	(substr(ds_hor_aux_w,x,1) not in('0','1','2','3','4','5','6','7','8','9')) then
		ds_erro_p	:= wheb_mensagem_pck.get_texto(278801);
		exit;
	end if;
	x	:= x + 1;
end loop;

dt_primeiro_horario_w := trunc(dt_primeiro_horario_p, 'mi');
if (nvl(ie_item_cpoe_p,'N') = 'S') then
	dt_primeiro_horario_w := to_date(to_char(dt_primeiro_horario_w,'dd/mm/yyyy')||' '||obter_prim_dshorarios(ds_horarios_p)||':00','dd/mm/yyyy hh24:mi:ss');
end if;

dt_final_w	:= (dt_primeiro_horario_w + 1) - 1/86400;

if	(ds_erro_p	is null) and
	(cd_intervalo_p	is not null) then
	
	ds_horarios_w	:= replace(ds_horarios_p,'  ',' ');
	
	qt_tamanho_w	:= length(ds_horarios_w);
	--Caso o primeiro caracter seja um espa�o, este � eliminado
	while	(substr(ds_horarios_w,1,1) = ' ') loop 
		begin
		ds_horarios_w	:= substr(ds_horarios_w,2,qt_tamanho_w);
		qt_tamanho_w	:= length(ds_horarios_w);		
		end;
	end loop;


	
	--Caso o �ltimo caracter seja um espa�o, este � eliminado	
	while	(substr(ds_horarios_w,qt_tamanho_w,1) = ' ') loop 
		begin
		ds_horarios_w	:= substr(ds_horarios_w,1,qt_tamanho_w - 1);
		qt_tamanho_w	:= length(ds_horarios_w);
		end;
	end loop;	
	qt_tamanho_w	:= length(ds_horarios_w);
	qt_horarios_w	:= 0;
	qt_pos_atual_w	:= 1;
	
	
	if	(instr(ds_horarios_w,' ')	> 0) or
   	        (length(ds_horarios_w)		= 2) or
		(length(ds_horarios_w)		= 5) then
	       				
		--Loop para validar os hor�rios
		while	(qt_pos_atual_w	<= qt_tamanho_w) loop 
			begin
			
			--Cada espa�o se indica um novo hor�rio
			if	(substr(ds_horarios_w,qt_pos_atual_w,1) = ' ') or
				(qt_pos_atual_w	= 1) then
				
				--Controlar para pegar a posi��o correta do primeiro hor�rio da String
				if	(qt_pos_atual_w	= 1) then
					qt_controle_w	:= 0;
				else
					qt_controle_w	:= 1;
				end if;	

				--Posi��o do pr�ximo espa�o
				qt_pos_espaco_w	:= instr(substr(ds_horarios_w,qt_pos_atual_w + qt_controle_w,qt_tamanho_w),' ');
				 
				--Hor�rio
				if	(qt_pos_espaco_w	= 0) then
					ds_horario_w	:= substr(ds_horarios_w,qt_pos_atual_w + qt_controle_w,qt_tamanho_w);
				else
					ds_horario_w	:= substr(ds_horarios_w,qt_pos_atual_w + qt_controle_w,qt_pos_espaco_w - 1);
				end if;
				
				
				--Verificar se o hor�rio est� no formato hh ou hh:24, e se o mesmo � v�lido
				if	(length(ds_horario_w)	= 5) and
					(instr(ds_horario_w,':') = 3) then
					if	(to_number(substr(ds_horario_w,1,2)) > 23) or
						(to_number(substr(ds_horario_w,4,2)) > 59) then
						ie_invalido_w	:= 'S';
					end if;	
				elsif	(length(ds_horario_w)	= 2) then
					if	(to_number(ds_horario_w) > 23) then
						ie_invalido_w	:= 'S';
					else
						ds_horario_w	:= ds_horario_w||':00';
				
					end if;
				else
					ie_invalido_w	:= 'S';
				end if;
				
				if	(ie_invalido_w	= 'S') then
					ds_erro_p	:= wheb_mensagem_pck.get_texto(278803);
					exit;			
				else
					dt_horario_w	:= to_date(to_char(dt_primeiro_horario_w,'dd/mm/yyyy')||' '||ds_horario_w||':00','dd/mm/yyyy hh24:mi:ss');
					if	(dt_primeiro_horario_w	> dt_horario_w) or
						(ie_novo_dia_w	= 'S') or
						((dt_horario_w	<= dt_horario_anterior_w) and
						 (dt_horario_anterior_w is not null)) then
						dt_horario_w	:= dt_horario_w + 1;
						ie_novo_dia_w	:= 'S';
					end if;	
					if	(dt_horario_w	between dt_primeiro_horario_w	and dt_final_w) then
						qt_horarios_w	:= qt_horarios_w + 1;
						dt_horario_anterior_w	:= dt_horario_w;
					else 
						if (nvl(ie_item_cpoe_p,'N') = 'N') then
							ds_erro_p	:= wheb_mensagem_pck.get_texto(278804);
						end if;
						exit;			
					end if;	
				end if;	
			end if;	
			qt_pos_atual_w	:= qt_pos_atual_w	+ 1;
			end;
		end loop;	
	
	end if;

	if	(ds_erro_p	is null) then
	
		select	max(ie_operacao),
			nvl(max(qt_operacao),0),
			max(ds_intervalo)
		into	ie_operacao_w,
			qt_operacao_w,
			ds_intervalo_w
		from	intervalo_prescricao
		where	cd_intervalo	= cd_intervalo_p;
				
		if	(ie_operacao_w	in ('F','V','X')) and
			(qt_horarios_w	<> qt_operacao_w) then
			ds_erro_p	:= wheb_mensagem_pck.get_texto(278805, 'QT_OPERACAO_P=' || qt_operacao_w ||
										';DS_INTERVALO_P=' || ds_intervalo_w);
		elsif	(ie_operacao_w	= 'H') and
			(ceil(qt_horarios_w)	<> ceil((24 / qt_operacao_w))) and
			(qt_operacao_w <= 24) then
			ds_erro_p	:= wheb_mensagem_pck.get_texto(278806, 'QT_OPERACAO_P=' || ceil((24 / qt_operacao_w)) ||
										';DS_INTERVALO_P=' || ds_intervalo_w);	
		end if;	
	
	end if;
	
end if;


end Consistir_hor_padrao_interv;
/