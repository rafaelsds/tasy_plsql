create or replace
procedure gerar_atend_cobranca_taxa(	nr_atendimento_p	number,
				ie_opcao_p		varchar2,
				nm_usuario_p		varchar2) is
					
nr_sequencia_w		atend_cobranca_taxa.nr_sequencia%type;
					
begin
if	(nr_atendimento_p is not null) then
	begin
	if	(ie_opcao_p = 'I') then
		begin
		
		insert into atend_cobranca_taxa(
			nr_sequencia,
			nr_atendimento,
			nm_usuario_inicio,
			dt_inicio_cobranca,
			nm_usuario_fim,
			dt_fim_cobranca)
		values	(regra_cobranca_taxa_seq.nextval,
			nr_atendimento_p,
			nm_usuario_p,
			sysdate,
			null,
			null);
			
		end;
	elsif (ie_opcao_p = 'F') then
		begin
		
		select	nvl(max(nr_sequencia),0)
		into	nr_sequencia_w
		from	atend_cobranca_taxa
		where	nr_atendimento = nr_atendimento_p
		and	dt_fim_cobranca is null;
	
		update	atend_cobranca_taxa
		set	nm_usuario_fim = nm_usuario_p,
			dt_fim_cobranca = sysdate
		where	nr_sequencia = nr_sequencia_w;
		
		end;	
	end if;
	end;
end if;	
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_atend_cobranca_taxa;
/
