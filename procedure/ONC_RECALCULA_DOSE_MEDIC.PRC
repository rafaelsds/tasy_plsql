create or replace procedure onc_recalcula_dose_medic (	nr_seq_paciente_p	number,
							nr_seq_atendimento_p	number,
							nr_seq_material_p	number,
							nr_ciclo_p		number,
							cd_material_p		number,
							qt_dose_p		number,
							qt_dose_prescricao_p	number,
							cd_unid_med_dose_p	varchar2,
							ie_via_aplicacao_p	varchar2,
							cd_intervalo_p		varchar2,
							ie_bomba_infusao_p	varchar2,
							qt_hora_aplicacao_p	number,
							qt_min_aplicacao_p	number,
							nm_usuario_p		varchar2) is

qt_dose_w		number(15,4);
cd_material_w		number(6,0);
nr_seq_atendimento_w	number(10,0);
qt_peso_w		number;
cd_unid_med_dose_w	varchar2(30) := '';
cd_unidade_med_sec_w	varchar2(30) := '';
qt_superf_corporea_w	number(10,5) := 0;
qt_dose_prescricao_w	number(15,4) := 0;
cd_unidade_med_prescr_w	varchar2(30);
cd_unidade_medida_w	varchar2(30);
ds_tempo_infusao_w	varchar2(255);
ds_observacao_w		varchar2(255);
cd_unid_med_dose_ww	varchar2(30) := '';
ie_via_aplicacao_w	varchar2(5);
cd_intervalo_w		varchar2(7);
qt_hora_aplicacao_w	number(3,0);
qt_min_aplicacao_w	number(4,0);
nr_seq_material_w	number(10,0);
nr_dia_ciclo_inicial_w	number(10,0);
qt_altura_cm_w 		number(3);
cd_pessoa_fisica_w 	varchar(10);

Cursor c01 is
select	b.qt_dose,
	a.nr_seq_atendimento,
	nvl(a.qt_peso,0),
	b.nr_seq_material
from	paciente_atendimento a,
	paciente_atend_medic b	 
where	a.nr_seq_atendimento	= b.nr_seq_atendimento
and	a.nr_seq_paciente	= nr_seq_paciente_p
and	b.cd_material		= cd_material_p
and	a.nr_ciclo		= nr_ciclo_p
and	somente_numero(a.ds_dia_ciclo) between nr_dia_ciclo_inicial_w and (	Select	max(somente_numero(ds_dia_ciclo))
										from	paciente_atendimento
										where	nr_seq_paciente = nr_seq_paciente_p);

begin

select max(cd_pessoa_fisica)
into cd_pessoa_fisica_w
from paciente_setor
where nr_seq_paciente = nr_seq_paciente_p;

select	somente_numero(ds_dia_ciclo) + 1
into	nr_dia_ciclo_inicial_w
from	paciente_atendimento
where	nr_seq_atendimento = nr_seq_atendimento_p;

select	ds_tempo_infusao,
	ds_observacao,
	cd_unid_med_dose,
	cd_unid_med_prescr,
	ie_via_aplicacao,
	cd_intervalo,
	qt_hora_aplicacao,
	qt_min_aplicacao
into	ds_tempo_infusao_w,
	ds_observacao_w,
	cd_unid_med_dose_ww,
	cd_unid_med_dose_w,
	ie_via_aplicacao_w,
	cd_intervalo_w,
	qt_hora_aplicacao_w,
	qt_min_aplicacao_w
from	paciente_atend_medic
where	nr_seq_atendimento = nr_seq_atendimento_p
and	cd_material = cd_material_p
and	nr_seq_material = nr_seq_material_p;

if	(nr_seq_paciente_p is not null) and
	(nr_ciclo_p is not null) then

	open c01;	
	loop

	fetch c01 into	qt_dose_w,
			nr_seq_atendimento_w,
			qt_peso_w,
			nr_seq_material_w;
		exit when c01%notfound;

		/* Atualiza a unidade de medida do medicamento */
		Select	max(cd_unidade_med_sec)
		into	cd_unidade_med_sec_w
		from	unidade_medida
		where	cd_unidade_medida = cd_unid_med_dose_ww
		and	cd_unidade_medida <> cd_unidade_med_princ;

		Select	cd_unidade_med_princ
		into	cd_unidade_med_prescr_w
		from	unidade_medida
		where	cd_unidade_medida = cd_unid_med_dose_ww;
		
		select 	max(qt_altura)
		into 	qt_altura_cm_w
		from 	paciente_setor
		where 	nr_seq_paciente = nr_seq_paciente_p
		and 	cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		select obter_peso_considerado_onc(nr_seq_atendimento_p, nr_seq_material_p, qt_peso_w, qt_altura_cm_w, 'S')
		into qt_peso_w
		from dual;


		if	(cd_unidade_med_sec_w = 'm2') then
			Select	round(obter_superficie_corporea(qt_altura, qt_peso_w),2)
			into	qt_superf_corporea_w
			from	paciente_atendimento
			where	nr_seq_atendimento = nr_seq_atendimento_w;

			if	(qt_superf_corporea_w > 0) then
				qt_dose_prescricao_w	:= qt_dose_p * qt_superf_corporea_w;
			end if;
		elsif	(cd_unidade_med_sec_w = 'kg') then
			if	(qt_peso_w > 0) then

				qt_dose_prescricao_w	:= qt_dose_p * qt_peso_w;
			end if;
		else
			qt_dose_prescricao_w		:= nvl(qt_dose_prescricao_p,0);
			cd_unidade_med_prescr_w		:= cd_unid_med_dose_w;
		end if;
		/* Fim da atualizacao da unidade medida do medicamento */

		Select	max(cd_unid_med_dose)
		into	cd_unidade_medida_w
		from	paciente_atend_medic
		where	nr_seq_atendimento	= nr_seq_atendimento_w
		and		cd_material		= cd_material_p;

		update	paciente_atend_medic a
		set	a.qt_dose			= qt_dose_p,
			a.qt_dose_prescricao		= qt_dose_prescricao_w,
			a.nm_usuario			= nm_usuario_p,
			a.ds_tempo_infusao		= ds_tempo_infusao_w,
			a.cd_unid_med_dose		= cd_unid_med_dose_ww,
			a.ie_via_aplicacao		= ie_via_aplicacao_w,
			a.cd_intervalo			= cd_intervalo_w,
			a.qt_hora_aplicacao		= qt_hora_aplicacao_w,
			a.qt_min_aplicacao		= qt_min_aplicacao_w,
			a.cd_unid_med_prescr		= cd_unid_med_dose_w
		where	a.nr_seq_atendimento		= nr_seq_atendimento_w
		and	a.cd_material			= cd_material_p
		and	nvl(cd_unid_med_dose,'0')	= nvl(cd_unid_med_dose_p,nvl(cd_unid_med_dose,'0'))
		and	nvl(ie_via_aplicacao,'0')	= nvl(ie_via_aplicacao_p,nvl(ie_via_aplicacao,'0'))
		and	nvl(cd_intervalo,'0')		= nvl(cd_intervalo_p,nvl(cd_intervalo,'0'))
		and	nvl(ie_bomba_infusao,'0')	= nvl(ie_bomba_infusao_p,nvl(ie_bomba_infusao,'0'))
		and	nvl(qt_hora_aplicacao,0)	= nvl(qt_hora_aplicacao_p,nvl(qt_hora_aplicacao,0))
		and	nvl(qt_min_aplicacao,0)		= nvl(qt_min_aplicacao_p,nvl(qt_min_aplicacao,0));

	end loop;
	close c01;

end if;

commit;

end onc_recalcula_dose_medic;
/
