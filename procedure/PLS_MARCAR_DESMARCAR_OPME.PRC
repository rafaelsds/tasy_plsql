create or replace
procedure pls_marcar_desmarcar_opme(nr_seq_grupo_p		number,
			nm_usuario_p		varchar2) is 

begin
if 	(nr_seq_grupo_p is not null) then
	update  pls_grupo_est_fed_sc
	set	ie_opme = decode(ie_opme, 'S','N','S')
	where  	nr_sequencia = nr_seq_grupo_p; 

	commit;
end if;

end pls_marcar_desmarcar_opme;
/