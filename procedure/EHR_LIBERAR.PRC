create or replace
procedure EHR_Liberar(	nr_sequencia_p		number,
			ie_opcao_p		varchar2,
			nm_usuario_p		Varchar2) is 

qt_nao_lib_w		number(10);

begin

if	(ie_opcao_p = 'TD') then /*Tipo de dado*/
	begin
	update	ehr_tipo_dado
	set	dt_liberacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	end;
elsif	(ie_opcao_p = 'EL') then /*Elemento*/
	begin
	update	ehr_elemento
	set	dt_liberacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	end;
elsif	(ie_opcao_p = 'DEL') then /*Desfazer elemento*/
	begin
	update	ehr_elemento
	set	dt_liberacao	= null,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	end;
elsif	(ie_opcao_p = 'ARQ') then /*Arquetipo*/
	begin
	update	ehr_arquetipo
	set	dt_liberacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	end;
elsif	(ie_opcao_p = 'TE') then /*Template*/
	begin
	select	count(*)
	into	qt_nao_lib_w
	from	ehr_elemento b,
		ehr_template_conteudo a
	where	a.nr_seq_template	= nr_sequencia_p
	and	a.nr_seq_elemento	= b.nr_sequencia
	and	b.dt_liberacao is null;
	
	if	(qt_nao_lib_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(174252);
	else
		update	ehr_template
		set	dt_liberacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_sequencia_p;
	end if;
	end;
elsif	(ie_opcao_p = 'DTE') then /*Desfazer Template*/
	begin
	update	ehr_template
	set	dt_liberacao	= null,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;	
	end;
elsif	(ie_opcao_p = 'REG') then /*Registro*/
	begin
	update	ehr_registro
	set	dt_liberacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	end;
elsif	(ie_opcao_p = 'REGTE') then /*Template do Registro*/
	begin
	update	ehr_reg_template
	set	dt_liberacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;
	end;
end if;

commit;

end EHR_Liberar;
/
