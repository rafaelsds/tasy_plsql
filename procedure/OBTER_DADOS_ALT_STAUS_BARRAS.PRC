CREATE OR REPLACE PROCEDURE obter_dados_alt_staus_barras (
    nr_controle_p             NUMBER,
    nr_seq_etiqueta_p         NUMBER,
    ie_status_atend_p         NUMBER,
    ie_mostra_nome_pessoa_p   VARCHAR2,
    lista_status_seq_p        VARCHAR2,
    ie_status_prescr_p        OUT   NUMBER,
    nr_prescricao_p           OUT   NUMBER,
    nr_atendimento_p          OUT   NUMBER,
    nm_pessoa_fisica_p        OUT   VARCHAR2,
    ds_status_p               OUT   VARCHAR2,
    ie_status_sem_amostra_p   OUT   VARCHAR2
) IS

    nr_prescricao_w           NUMBER(14);
    nr_atendimento_w          NUMBER(10);
    ie_status_prescr_w        NUMBER(10);
    ds_status_w               VARCHAR2(255);
    nm_pessoa_fisica_w        VARCHAR2(255);
    ie_status_sem_amostra_w   VARCHAR2(1);
BEGIN 

    ie_status_prescr_w := obter_status_barras_prescr(
        nr_controle_p        => nr_controle_p,
        nr_seq_etiqueta_p    => nr_seq_etiqueta_p,
        cd_unidade_fleury_p  => NULL,
        cd_estabelecimento_p => NULL
    );

    SELECT nr_prescricao
    INTO nr_prescricao_w
    FROM prescr_medica
    WHERE nr_controle = nr_controle_p;

    IF ( nr_prescricao_w > 0 ) THEN
        SELECT nr_atendimento
        INTO nr_atendimento_w
        FROM prescr_medica
        WHERE nr_prescricao = nr_prescricao_w;

        nm_pessoa_fisica_w := obter_nome_prescricao(nr_prescricao_w, ie_mostra_nome_pessoa_p);
    END IF;

    SELECT substr(obter_valor_dominio_status_lis(ie_status_atend_p), 1, 255)
    INTO ds_status_w
    FROM dual;

    IF ( lista_status_seq_p IS NOT NULL ) THEN
        ie_status_sem_amostra_w := obter_se_contido(ie_status_atend_p, lista_status_seq_p); 
    ELSE
        ie_status_sem_amostra_w := 'N';
    END IF;

    ie_status_prescr_p      := ie_status_prescr_w;
    nr_prescricao_p         := nr_prescricao_w;
    nr_atendimento_p        := nr_atendimento_w;
    nm_pessoa_fisica_p      := nm_pessoa_fisica_w;
    ds_status_p             := ds_status_w;
    ie_status_sem_amostra_p := ie_status_sem_amostra_w;
END obter_dados_alt_staus_barras;
/