create or replace 
procedure INSERT_AUT_SERVICE_RETURN(NM_USUARIO_P VARCHAR2,
									IE_STATUS_P VARCHAR2,
									CD_EXP_MENSAGEM_P INTEGER,
									NR_SEQ_AUTORIZACAO_P VARCHAR2,
									VL_COBERTO_P NUMBER,
									VL_USUARIO_PROC_P NUMBER,
									VL_TOTAL_P NUMBER,
									DS_MENSAGEM_P VARCHAR2,
									QT_PROCEDIMENTO_P NUMBER,
									CD_PROCEDIMENTO_P VARCHAR2,
									CD_AUTORIZACAO_P NUMBER) is

ds_procedimento_w  procedimento.ds_procedimento_pesquisa%type;

begin

select 	nvl(ds_procedimento_pesquisa,'')
into 	ds_procedimento_w
from 	procedimento 
where 	cd_procedimento = cd_procedimento_p;

insert into AUT_SERVICE_RETURN(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_status,
						ds_mensagem,
						nr_seq_autorizacao,
						vl_coberto,
						vl_usuario_proc,
						vl_total,
						cd_procedimento,
						ds_procedimento,
						cd_autorization)
						values(
						aut_service_return_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						ie_status_p,
						get_expression(cd_exp_mensagem_p,'es-DO') || decode(ds_mensagem_p, null, '', ' - '||ds_mensagem_p),
						nr_seq_autorizacao_p,
						vl_coberto_p,
						vl_usuario_proc_p,
						vl_total_p,
						cd_procedimento_p,
						ds_procedimento_w,
						cd_autorizacao_p);
commit;
end;
/
