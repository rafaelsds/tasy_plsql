create or replace
procedure grava_etapa_processo_gedipa(		nr_seq_processo_p	number,
						ie_conferencia_p	varchar2,
						ie_higienizacao_p	varchar2,
						ie_preparo_p		varchar2,
						nr_seq_area_prep_p	number,	
						nm_usuario_p		varchar2) is

nr_sequencia_w		number(10);
ie_existe_w		number(10);

begin

select  count(*)
into	ie_existe_w
from 	gedipa_intervencao_preparo
where	nr_seq_processo		=	nr_seq_processo_p
and 	nr_seq_area_prep	=	nr_seq_area_prep_p;

select	gedipa_intervencao_preparo_seq.nextval 
into	nr_sequencia_w
from 	dual;

if 	(ie_existe_w = 0) then 
	insert into gedipa_intervencao_preparo(
		nr_sequencia,
		ie_conferencia,
		ie_higienizacao,
		ie_preparo,
		nm_usuario,
		nr_seq_area_prep,
		dt_atualizacao,
		nr_seq_processo)
	values	(nr_sequencia_w,
		decode(ie_conferencia_p,'N','N',null),
		decode(ie_higienizacao_p,'N','N',null),
		decode(ie_preparo_p,'N','N',null),
		nm_usuario_p,
		nr_seq_area_prep_p,
		sysdate,
		nr_seq_processo_p);
else
	update	gedipa_intervencao_preparo
	set	ie_conferencia		=	decode(ie_conferencia_p,'N','N',null),
		ie_higienizacao		=	decode(ie_higienizacao_p,'N','N',null),
		ie_preparo		=	decode(ie_preparo_p,'N','N',null),
		nm_usuario		=	nm_usuario_p,
		dt_atualizacao		=	sysdate
	where	nr_seq_processo		=	nr_seq_processo_p
	and 	nr_seq_area_prep	=	nr_seq_area_prep_p;
end if;

/*gravar_logx_tasy(88901,'nr_seq_processo_p=' || nr_seq_processo_p || ' ie_conferencia_p=' || ie_conferencia_p || ' ie_preparo_p=' || ie_preparo_p || ' ie_higienizacao_p= ' || ie_higienizacao_p,nm_usuario_p);*/

commit;

end grava_etapa_processo_gedipa;
/