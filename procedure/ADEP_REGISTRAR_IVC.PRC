create or replace
procedure adep_registrar_ivc	(ie_nova_irrigacao_p	varchar2,
			nr_atendimento_p		number,
			nr_seq_irrigacao_p in out	number,
			qt_infusao_p		number,
			qt_drenagem_p		number,
			nr_seq_aspecto_p	number,
			dt_registro_p		date,
			ds_observacao_p		varchar2,
			nm_usuario_p		varchar2,
			ie_irrigacao_lavagem_p 	varchar2,
			nr_seq_horario_p	number,
			nr_seq_assinatura_p	number default null,
			qt_vol_instalado_p	number default null) is
				
nr_seq_etapa_w				number(10,0);
nr_seq_hor_agrupado_w		number(10);
nr_seq_irrigacao_w			number(10,0);
count_w						number(10,0);
qt_evento_w					number(10,0);
ie_evento_w					varchar2(10);
ie_status_ivc_w				varchar2(10);
nr_prescricao_w				number(14,0);
nr_seq_procedimento_w		number(10);
nr_seq_alteracao_w			number(10);
cd_procedimento_w			number(15);
nr_seq_proc_interno_w		number(15);
nr_seq_procedimento_ww		number(10);
nr_prescricao_ww			number(14,0);
ie_alteracao_w				number(10);
ie_gerar_evento_w			varchar2(1);
dt_horario_w				date;
nr_seq_horario_mat_w		number(10);
nr_seq_map_w				number(10);
ie_conta_infusao_w			varchar2(1);
cd_perfil_w					number(5)	:= obter_perfil_ativo;
nr_seq_horario_w			prescr_proc_hor.nr_sequencia%type;

begin
obter_param_usuario(1113, 431, cd_perfil_w, nm_usuario_p, 0, ie_gerar_evento_w);
obter_param_usuario(1113, 672, cd_perfil_w, nm_usuario_p, 0, ie_conta_infusao_w);

if	(ie_nova_irrigacao_p is not null) and
	(nr_atendimento_p is not null) and
	(nr_seq_irrigacao_p is not null) and
	(qt_drenagem_p is not null) and
	(nm_usuario_p is not null) then
	
	begin
	
	select	max(ie_status_ivc),
			max(nr_seq_procedimento),
			max(nr_prescricao)
	into	ie_status_ivc_w,
			nr_seq_procedimento_ww,
			nr_prescricao_ww
	from	adep_irrigacao
	where	nr_sequencia = nr_seq_irrigacao_p;
	
	select	max(cd_procedimento),
			max(nr_seq_proc_interno)		
	into	cd_procedimento_w,
			nr_seq_proc_interno_w
	from 	prescr_procedimento 
	where 	nr_prescricao = nr_prescricao_ww
	and 	nr_sequencia = nr_seq_procedimento_ww;
	
	select	max(dt_horario)
	into	dt_horario_w
	from	prescr_proc_hor
	where	nr_sequencia = nr_seq_horario_p;
	
	if	(ie_nova_irrigacao_p = 'S') and
		(nvl(ie_status_ivc_w,'I') <> 'P') then
		begin
		select	adep_irrigacao_seq.nextval
		into	nr_seq_irrigacao_w
		from	dual;	
	
		insert into adep_irrigacao (
			nr_sequencia,
			nr_atendimento,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_inicio,
			cd_pessoa_fisica,
			dt_fim,
			cd_pessoa_fisica_fim,
			ie_status_ivc,
			qt_balanco)
		values (
			nr_seq_irrigacao_w,
			nr_atendimento_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nvl(dt_registro_p,sysdate),
			substr(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10),
			null,
			null,
			'I',
			(nvl(qt_infusao_p,0) - nvl(qt_drenagem_p,0)));

		select	adep_irrigacao_etapa_seq.nextval
		into	nr_seq_etapa_w
		from	dual;		
		
		insert into adep_irrigacao_etapa (
			nr_sequencia,
			nr_seq_irrigacao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_registro,
			qt_infusao,
			qt_drenagem,
			nr_seq_aspecto,
			ds_observacao,
			cd_pessoa_fisica,
			ie_evento,
			ie_evento_valido,
			ie_irrigacao_lavagem,
			nr_seq_horario,
      qt_vol_instalado)
		values (
			nr_seq_etapa_w,
			nr_seq_irrigacao_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nvl(dt_registro_p,sysdate),
			qt_infusao_p,
			qt_drenagem_p,
			nr_seq_aspecto_p,
			ds_observacao_p,
			substr(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10),
			'I',
			'S',
			ie_irrigacao_lavagem_p,
			nr_seq_horario_p,
      qt_vol_instalado_p);
			
			if	(ie_gerar_evento_w = 'S') then
			
				select	prescr_mat_alteracao_seq.nextval
				into	nr_seq_alteracao_w
				from	dual;			
			
			        insert into prescr_mat_alteracao	
					(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_prescricao,
					nr_seq_procedimento,
					dt_alteracao,
					cd_pessoa_fisica,
					ie_alteracao,
					nr_seq_motivo,
					ds_justificativa,
					ie_tipo_item,
					nr_atendimento,
					cd_item,
					nr_seq_proc_interno,
					nr_seq_motivo_susp,
					nr_seq_assinatura
					)
				values	(
					nr_seq_alteracao_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_prescricao_ww,
					nr_seq_procedimento_ww,
					sysdate,
					obter_dados_usuario_opcao(nm_usuario_p,'C'),
					41,
					null,
					null,
					'I',
					nr_atendimento_p,
					cd_procedimento_w,	
					nr_seq_proc_interno_w,
					null,
					nr_seq_assinatura_p
					);		
			end if;
			
			
		end;
	else
		begin
		nr_seq_irrigacao_w	:= nr_seq_irrigacao_p;

		select	nvl(count(*),0)
		into	qt_evento_w
		from	adep_irrigacao_etapa
		where	nr_seq_irrigacao = nr_seq_irrigacao_w
		and	ie_evento_valido = 'S';

		if	(qt_evento_w > 0) then
			ie_evento_w	:= 'R';
			ie_alteracao_w	:= 40;	
		else
			ie_evento_w	:= 'I';
			ie_alteracao_w	:= 41;	
		end if;
		
		select	adep_irrigacao_etapa_seq.nextval
		into	nr_seq_etapa_w
		from	dual;

		insert into adep_irrigacao_etapa (
			nr_sequencia,
			nr_seq_irrigacao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_registro,
			qt_infusao,
			qt_drenagem,
			nr_seq_aspecto,
			ds_observacao,
			cd_pessoa_fisica,
			ie_evento,
			ie_evento_valido,
			ie_irrigacao_lavagem,
			nr_seq_horario,
      qt_vol_instalado)
		values (
			nr_seq_etapa_w,
			nr_seq_irrigacao_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nvl(dt_registro_p,sysdate),
			qt_infusao_p,
			qt_drenagem_p,
			nr_seq_aspecto_p,
			ds_observacao_p,
		substr(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10),
			ie_evento_w,
			'S',
			ie_irrigacao_lavagem_p,
			nr_seq_horario_p,
      qt_vol_instalado_p);
			
			
		/*Gerar a etapa como evento*/
		if	(ie_gerar_evento_w = 'S') then
		
			select	prescr_mat_alteracao_seq.nextval
			into	nr_seq_alteracao_w
			from	dual;
		
		        insert into prescr_mat_alteracao	
					(
					nr_sequencia,
					dt_atualizacao,	
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_prescricao,
					nr_seq_procedimento,
					dt_alteracao,
					cd_pessoa_fisica,
					ie_alteracao,
					nr_seq_motivo,
					ds_justificativa,
					ie_tipo_item,
					nr_atendimento,
					cd_item,
					nr_seq_proc_interno,
					nr_seq_motivo_susp,
					nr_seq_assinatura
					)
				values	(
					nr_seq_alteracao_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_prescricao_ww,
					nr_seq_procedimento_ww,
					sysdate,
					obter_dados_usuario_opcao(nm_usuario_p,'C'),
					ie_alteracao_w,
					null,
					null,
					'I',
					nr_atendimento_p,
					cd_procedimento_w,
					nr_seq_proc_interno_w,
					null,
					nr_seq_assinatura_p);
		end if;

		select	nvl(count(*),0)
		into	count_w
		from	adep_irrigacao
		where	nr_sequencia	= nr_seq_irrigacao_w
		and	ie_status_ivc	= 'P';
		
		update	adep_irrigacao
		set	qt_balanco	= nvl(qt_balanco,0) + (nvl(qt_infusao_p,0) - nvl(qt_drenagem_p,0))
		where	nr_sequencia	= nr_seq_irrigacao_w;

		if	(count_w > 0) then
			ATUALIZAR_STATUS_IVC(nr_atendimento_p, nr_seq_irrigacao_w, nvl(dt_registro_p,sysdate), 'I', nm_usuario_p);
		end if;

		select	nvl(max(c.nr_sequencia),0)
		into	nr_seq_horario_mat_w
		from	prescr_procedimento a,
				prescr_material b,
				prescr_mat_hor c
		where	a.nr_prescricao = b.nr_prescricao
		and		a.nr_prescricao = c.nr_prescricao
		and		b.nr_prescricao	= c.nr_prescricao
		and		b.nr_sequencia	= c.nr_seq_material
		and		b.nr_sequencia_proc = a.nr_sequencia
		and		a.nr_prescricao	= nr_prescricao_ww
		and		a.nr_sequencia	= nr_seq_procedimento_ww
		and		b.ie_agrupador	= 5
		and		obter_conversao_ml(b.cd_material,b.qt_dose,b.cd_unidade_medida_dose)	> 0
		and		nvl(c.ie_horario_especial,'N')	<> 'S';
		
		if	(nr_seq_horario_mat_w	> 0) and
			(nvl(qt_infusao_p,0)	> 0) and
			(ie_conta_infusao_w		= 'S') then
			gerar_estornar_adep_map(NULL, nr_seq_horario_mat_w, NULL, 'G', sysdate, nm_usuario_p, nr_seq_map_w,null,qt_infusao_p,null);
		end if;
		
		end;
	end if;
	end;	
end if; 

select	max(ie_status_ivc),
	max(nr_prescricao),
	max(nr_seq_procedimento)
into	ie_status_ivc_w,
	nr_prescricao_w,
	nr_seq_procedimento_w
from	adep_irrigacao
where	nr_sequencia	= nr_seq_irrigacao_w;

if	(ie_status_ivc_w = 'F') then
	ie_status_ivc_w	:= 'A';
elsif	(ie_status_ivc_w = 'P') then
	ie_status_ivc_w	:= 'N';
end if;

if	(nr_seq_horario_p > 0) then

	select	max(nr_prescricao)
	into	nr_prescricao_w
	from	prescr_proc_hor
	where	nr_sequencia = nr_seq_horario_p;

	select	max(c.nr_sequencia)
	into	nr_seq_hor_agrupado_w
	from	prescr_procedimento a,
		prescr_medica b,
		prescr_proc_hor c
	where	a.nr_prescricao = b.nr_prescricao
	and	a.nr_sequencia = c.nr_seq_procedimento	
	and	a.nr_prescricao = c.nr_prescricao
	and	a.nr_prescricao <> nr_prescricao_w
	and	b.nr_atendimento = nr_atendimento_p
	and	c.dt_horario = dt_horario_w
	and	c.cd_procedimento = cd_procedimento_w
	and	c.nr_seq_proc_interno = nr_seq_proc_interno_w;

	update	prescr_proc_hor
	set	ie_status_ivc		= ie_status_ivc_w
	where	nr_sequencia		= nr_seq_hor_agrupado_w;
	
	update	prescr_proc_hor
	set	ie_status_ivc		= ie_status_ivc_w
	where	nr_sequencia		= nr_seq_horario_p;

	if (nvl(nr_prescricao_w,0) > 0) and (nvl(nr_seq_procedimento_w, 0) > 0) then
		
		update prescr_procedimento
		set    ie_status= ie_status_ivc_w
		where  nr_sequencia = nr_seq_procedimento_w
		and    nr_prescricao = nr_prescricao_w;
		

	end if;
	
else

	if (nvl(nr_prescricao_w,0) > 0) and (nvl(nr_seq_procedimento_w, 0) > 0) then
	
		select 	nvl(min(nr_sequencia),0)
		into 	nr_seq_horario_w
		from 	prescr_proc_hor
		where 	nr_seq_procedimento = nr_seq_procedimento_w
		and    	nr_prescricao = nr_prescricao_w;
	
		update	prescr_proc_hor
		set		ie_status_ivc		= ie_status_ivc_w
		where	nr_prescricao		= nr_prescricao_w
		and		nr_seq_procedimento = nr_seq_procedimento_w
		and 	nr_sequencia 		= nr_seq_horario_w;
		
	else
	
		update	prescr_proc_hor
		set		ie_status_ivc		= ie_status_ivc_w
		where	nr_prescricao		= nr_prescricao_w
		and		nr_seq_procedimento = nr_seq_procedimento_w;
	
	end if;
	
end if;

commit;

nr_seq_irrigacao_p	:= nr_seq_irrigacao_w;

end adep_registrar_ivc;
/
