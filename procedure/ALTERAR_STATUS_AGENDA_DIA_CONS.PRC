create or replace
procedure alterar_status_agenda_dia_cons(	nr_sequencia_p		number,
						ie_status_agenda_p	varchar2,
						nm_usuario_p		Varchar2) is 

qt_hor_cancel_w		NUMBER(10);
dt_agenda_w		Date;
cd_agenda_w		Number(10);
qt_reg_w		number(1);
nr_seq_hora_w		agenda_consulta.nr_seq_hora%type;
begin

if	(nvl(nr_sequencia_p,0) > 0) and
	(ie_status_agenda_p is not null)then
	
	SELECT	MAX(dt_agenda),
		max(cd_agenda)
	INTO	dt_agenda_w,
		cd_agenda_w
	FROM	agenda_consulta
	WHERE	nr_sequencia = nr_sequencia_p;
	
	select	nvl(max(to_number(to_char(dt_agenda,'ss'))),0)+1
	into	qt_hor_cancel_w
	from	agenda_consulta
	where	cd_agenda = cd_agenda_w
	and	to_date(to_char(dt_agenda,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss') = to_date(to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi') || ':00', 'dd/mm/yyyy hh24:mi:ss')
	and	ie_status_agenda = 'C';	
	
	select 	nvl(max(1), 0)
	into	qt_reg_w
	from 	agenda_consulta
	where	cd_agenda = cd_agenda_w
	and 	dt_agenda = dt_agenda_w + qt_hor_cancel_w / 86400;
	
	if (qt_reg_w > 0) then
		select 	nvl(max(nr_seq_hora), 0) + 1
		into	nr_seq_hora_w
		from 	agenda_consulta
		where 	cd_agenda = cd_agenda_w
		and 	dt_agenda = dt_agenda_w + qt_hor_cancel_w / 86400;
		
		update	agenda_consulta
		set	ie_status_agenda 		= ie_status_agenda_p,
			nm_usuario_cancelamento		= nm_usuario_p,
			dt_cancelamento			= sysdate,
			dt_Agenda			= dt_agenda + qt_hor_cancel_w / 86400,
			nr_seq_hora			= nr_seq_hora_w
		where	nr_sequencia			= nr_sequencia_p;
	else
		update	agenda_consulta
		set	ie_status_agenda 		= ie_status_agenda_p,
			nm_usuario_cancelamento		= nm_usuario_p,
			dt_cancelamento			= sysdate,
			dt_Agenda			= dt_agenda + qt_hor_cancel_w / 86400
		where	nr_sequencia			= nr_sequencia_p;
	end if;
	
	commit;
end if;

end alterar_status_agenda_dia_cons;
/