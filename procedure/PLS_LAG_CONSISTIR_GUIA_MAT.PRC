create or replace
procedure pls_lag_consistir_guia_mat(	nr_seq_lote_guia_imp_p	number,
					nr_seq_guia_plano_p	number,
					nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Consistir se o 'Material, inativo ou fora de vig�ncia'e Consistir se o 'Material informado n�o coberto'.
-------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
					
nr_seq_lote_mat_imp_w	number(10);
nr_seq_guia_plano_w	pls_guia_plano.nr_sequencia%type;
qt_registro_w		pls_integer;
					
cursor C01 is
	select	nr_sequencia,
		nr_seq_material,
		nr_seq_plano_mat       
	from	pls_lote_anexo_mat_imp
	where	nr_seq_lote_guia_imp	= nr_seq_lote_guia_imp_p;
	
begin

select	nr_seq_guia
into	nr_seq_guia_plano_w
from	pls_lote_anexo_guias_imp
where	nr_sequencia			= nr_seq_lote_guia_imp_p;

for	r_C01_w in C01 loop
	
	--Se n�o encontrar o procedimento na base gera a glosa 2001 - Material inv�lido
	--A sequ�ncia do material da guia � obtido na importa��o do XML na procedure PLS_SALVAR_LT_AX_MAT_AUT
	if	( r_C01_w.nr_seq_plano_mat is null ) then
		pls_inserir_anexo_glosa_aut('2001', nr_seq_lote_guia_imp_p,  null, r_C01_w.nr_sequencia, 'Material/medicamento n�o encontrado na guia refer�ncia no anexo', nm_usuario_p);
	end if;
	
end loop;
	
/* Consistir a glosa '2006' */
--pls_lag_consistir_cobert_mat(nr_seq_lote_guia_imp_p, nr_seq_lote_mat_imp_w, nr_seq_guia_plano_p, nm_usuario_p);	

/* Consistir a glosa '9920' por material */	
--pls_lag_consistir_atv_vig_mat(nr_seq_lote_guia_imp_p, nr_seq_lote_mat_imp_w, nr_seq_guia_plano_p, nm_usuario_p);	
end pls_lag_consistir_guia_mat;
/