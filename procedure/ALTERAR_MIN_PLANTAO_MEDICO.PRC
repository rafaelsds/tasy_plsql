create or replace
procedure alterar_min_plantao_medico(	nm_usuario_p	varchar2,
					nr_minutos_p	number,
					nr_sequencia_p	varchar2) is

begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) and
	(nr_minutos_p is not null)then

	update	medico_plantao
	set	qt_minuto = nr_minutos_p,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p
	and	nvl(qt_minuto,0) = 0;

end if;

end alterar_min_plantao_medico;
/