create or replace procedure consistir_se_existe_pessoa_cv (	cd_pessoa_fisica_p	varchar2,
						nm_usuario_p		varchar2,
						ds_erro_p	out	varchar2 ) is

begin
if	(cd_pessoa_fisica_p is not null) then

	select	max(a.ds_erro)
	into	ds_erro_p
	from	(
		select	(obter_texto_dic_objeto(228401, wheb_usuario_pck.get_nr_seq_idioma, null) || Chr(10) ||
                                                 obter_texto_dic_objeto(880920, wheb_usuario_pck.get_nr_seq_idioma, null) || ' ' || nr_atendimento || Chr(10) ||
												 obter_texto_dic_objeto(1088673, wheb_usuario_pck.get_nr_seq_idioma, null) || ' ' || obter_nome_estabelecimento(obter_estab_atend(nr_atendimento))) ds_erro/* Visitante paciente */
												 
		from	atendimento_visita
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_saida is null
		and	nvl(ie_paciente,'N') = 'N'
		and	cd_setor_atendimento is null
		union all
		select	obter_texto_dic_objeto(228402, wheb_usuario_pck.get_nr_seq_idioma, null) ds_erro /* Visitante setor */
    
		from	atendimento_visita
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_saida is null
		and	nvl(ie_paciente,'N') = 'N'
		and	cd_setor_atendimento is not null
		union all
		select (obter_texto_dic_objeto(228403, wheb_usuario_pck.get_nr_seq_idioma, null) || Chr(10) ||
                                          obter_texto_dic_objeto(880920, wheb_usuario_pck.get_nr_seq_idioma, null) || ' ' || nr_atendimento || Chr(10) ||
										  obter_texto_dic_objeto(1088673, wheb_usuario_pck.get_nr_seq_idioma, null) || ' ' || obter_nome_estabelecimento(obter_estab_atend(nr_atendimento))) ds_erro/* Paciente */
		from	atendimento_visita
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_saida is null
		and	nvl(ie_paciente,'N') = 'S'
		union all
		select	(obter_texto_dic_objeto(228404, wheb_usuario_pck.get_nr_seq_idioma, null) || Chr(10) ||
			 obter_texto_dic_objeto(880920, wheb_usuario_pck.get_nr_seq_idioma, null) || ' ' || nr_atendimento || Chr(10) ||
										  obter_texto_dic_objeto(1088673, wheb_usuario_pck.get_nr_seq_idioma, null) || ' ' || obter_nome_estabelecimento(obter_estab_atend(nr_atendimento))) ds_erro/* Acompanhante */
		from	atendimento_acompanhante
		where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	dt_saida is null ) a;
end if;

commit;

end consistir_se_existe_pessoa_cv;
/