create or replace
procedure inserir_preco_amb_tabela(	cd_edicao_amb_p		number,
				cd_procedimento_p		number,
				vl_procedimento_p		number,
				cd_moeda_p		number,
				nm_usuario_p		varchar2,
				vl_custo_operacional_p	number,
				vl_anestesista_p		number,
				vl_medico_p		number,
				vl_filme_p			number,
				qt_filme_p		number,
				nr_auxiliares_p		number,
				nr_incidencia_p		number,
				qt_porte_anestesico_p	number,
				ie_origem_proced_p	number,
				vl_auxiliares_p		number,
				dt_inicio_vigencia_p	date,
				ie_origem_tabela_imp_p	varchar2 default 'P',
				cd_level_p	varchar2 default '',
				cd_procedimento_loc_p	varchar2 default '') is

ie_valor_nulo_w		varchar2(15) := 'N';

begin

ie_valor_nulo_w	:= nvl(obter_valor_param_usuario(705,3,obter_perfil_ativo,nm_usuario_p,nvl(wheb_usuario_pck.get_cd_estabelecimento,0)),'N');

if	(cd_edicao_amb_p		is not null) and
	(cd_procedimento_p	is not null) and
	(cd_moeda_p		is not null) and
	(nm_usuario_p		is not null) and
  	(cd_procedimento_loc_p	is not null or philips_param_pck.get_cd_pais = 1) and /* DBF import - Brazil doesn't have codigo_loc column */
	(ie_origem_proced_p	is not null) then
	begin
	if (nvl(ie_origem_tabela_imp_p,'P') = 'P') then
		insert into	preco_amb(
                    cd_edicao_amb,
                    cd_procedimento,
                    vl_procedimento,
                    cd_moeda,
                    dt_atualizacao,
                    nm_usuario,
                    vl_custo_operacional,
                    vl_anestesista,
                    vl_medico,
                    vl_filme,
                    qt_filme,
                    nr_auxiliares,
                    nr_incidencia,
                    qt_porte_anestesico,
                    ie_origem_proced,
                    vl_auxiliares,
                    dt_inicio_vigencia,
                    nr_sequencia,
                    cd_level,
                    cd_procedimento_loc
                    )
		values	(
				cd_edicao_amb_p,
				cd_procedimento_p,
				vl_procedimento_p,
				cd_moeda_p,
				sysdate,
				nm_usuario_p,
				decode(ie_valor_nulo_w,'N',vl_custo_operacional_p,decode(vl_custo_operacional_p,0,null,vl_custo_operacional_p)),
				decode(ie_valor_nulo_w,'N',vl_anestesista_p,decode(vl_anestesista_p,0,null,vl_anestesista_p)),
				decode(ie_valor_nulo_w,'N',vl_medico_p,decode(vl_medico_p,0,null,vl_medico_p)),
				decode(ie_valor_nulo_w,'N',vl_filme_p,decode(vl_filme_p,0,null,vl_filme_p)),
				decode(ie_valor_nulo_w,'N',qt_filme_p,decode(qt_filme_p,0,null,qt_filme_p)),
				decode(ie_valor_nulo_w,'N',nr_auxiliares_p,decode(nr_auxiliares_p,0,null,nr_auxiliares_p)),
				nr_incidencia_p,
				decode(ie_valor_nulo_w,'N',qt_porte_anestesico_p,decode(qt_porte_anestesico_p,0,null,qt_porte_anestesico_p)),
				ie_origem_proced_p,
				decode(ie_valor_nulo_w,'N',vl_auxiliares_p,decode(vl_auxiliares_p,0,null,vl_auxiliares_p)),
				dt_inicio_vigencia_p,
				preco_amb_seq.nextval,
				cd_level_p,
				cd_procedimento_loc_p
				);
	else
		insert into preco_tuss(
		            cd_edicao_amb,
	 				cd_moeda,
	 				cd_procedimento,
	 				dt_atualizacao,
	 				dt_atualizacao_nrec,
	 				dt_inicio_vigencia,
	 				ie_origem_proced,
	 				ie_situacao,
	 				nm_usuario,
	 				nm_usuario_nrec,
	 				vl_anestesista,
	 				vl_auxiliares,
	 				vl_custo_operacional,
	 				vl_filme,
	 				vl_medico,
	 				nr_porte_anest_amb,
	 				qt_filme_amb,
	 				nr_auxiliares_amb,
	 				qt_incidencia_amb,
	 				vl_procedimento,
	 				nr_sequencia
	 				)
		values   (	cd_edicao_amb_p,

                    cd_moeda_p,
                    cd_procedimento_p,
                    sysdate,
                    sysdate,
                    dt_inicio_vigencia_p,
                    ie_origem_proced_p,
                    'A',
                    nm_usuario_p,
                    nm_usuario_p,
                    decode(ie_valor_nulo_w,'N',vl_anestesista_p,decode(vl_anestesista_p,0,null,vl_anestesista_p)),
                    decode(ie_valor_nulo_w,'N',vl_auxiliares_p,decode(vl_auxiliares_p,0,null,vl_auxiliares_p)),
                    decode(ie_valor_nulo_w,'N',vl_custo_operacional_p,decode(vl_custo_operacional_p,0,null,vl_custo_operacional_p)),
                    decode(ie_valor_nulo_w,'N',vl_filme_p,decode(vl_filme_p,0,null,vl_filme_p)),
                    decode(ie_valor_nulo_w,'N',vl_medico_p,decode(vl_medico_p,0,null,vl_medico_p)),
                    decode(ie_valor_nulo_w,'N',qt_porte_anestesico_p,decode(qt_porte_anestesico_p,0,null,qt_porte_anestesico_p)),
                    decode(ie_valor_nulo_w,'N',qt_filme_p,decode(qt_filme_p,0,null,qt_filme_p)),
                    decode(ie_valor_nulo_w,'N',nr_auxiliares_p,decode(nr_auxiliares_p,0,null,nr_auxiliares_p)),
                    nr_incidencia_p,
                    vl_procedimento_p,
                    preco_tuss_seq.nextval
			);
	end if;
	end;
end if;
end inserir_preco_amb_tabela;
/