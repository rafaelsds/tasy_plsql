create or replace
procedure ctb_pls_atualizar_imposto_in
			(	nr_seq_lote_p		number,
				nr_seq_mensalidade_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 

cd_classificacao_credito_w	varchar2(255);
cd_classificacao_debito_w	varchar2(255);

ie_tipo_segurado_w		varchar2(2);
cd_conta_credito_w		varchar2(20);
cd_conta_debito_w		varchar2(20);

ie_tipo_item_w			varchar2(2);
dt_referencia_w			date;
nr_seq_esquema_w		number(10);
cd_historico_padrao_w		number(10);
cd_historico_estorno_w		number(10);
cd_historico_baixa_w		number(10);
ds_erro_w			varchar2(4000);

ie_codificacao_w		varchar2(2);
vl_fixo_w			varchar2(30);
cd_conta_contabil_w		varchar2(20);
ie_debito_credito_w		varchar2(1);
ie_tipo_outorgante_w		varchar2(3);
ie_tipo_operacao_w		varchar2(3);

nr_seq_segurado_w		number(10);

ds_mascara_w			varchar2(30);

cd_tributo_w			number(10);
nr_seq_trib_mens_w		number(10);
nr_seq_item_w			number(10);

cd_conta_credito_item_w		varchar2(20);
cd_conta_debito_item_w		varchar2(20);

ie_retencao_w			pls_mensalidade_trib.ie_retencao%type;		

nr_seq_lote_w			pls_lote_mensalidade.nr_sequencia%type;
cd_centro_custo_w		pls_mensalidade_trib.cd_centro_custo%type;
nr_seq_conta_w			pls_mensalidade_seg_item.nr_seq_conta%type;
ie_cancelamento_w		pls_mensalidade.ie_cancelamento%type;
nr_seq_mens_segurado_w		pls_mensalidade_segurado.nr_sequencia%type;

Cursor C01 is
	select	f.nr_sequencia,
		a.nr_sequencia,
		h.ie_tipo_segurado,
		a.ie_tipo_item,
		c.dt_referencia,
		e.ie_tipo_operacao,
		h.nr_sequencia,
		f.cd_tributo,
		a.cd_conta_rec,
		a.cd_conta_deb,
		f.ie_retencao,
		a.cd_centro_custo,
		a.nr_seq_conta,
		c.ie_cancelamento,
		b.nr_sequencia
	from	pls_mensalidade_trib	f,
		pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_segurado		h,
		pls_plano		e,
		pls_mensalidade		c,
		pls_lote_mensalidade	d
	where	f.nr_seq_item_mens	= a.nr_sequencia
	and	a.nr_seq_mensalidade_seg = b.nr_sequencia
	and	h.nr_sequencia		= b.nr_seq_segurado
	and	e.nr_sequencia		= h.nr_seq_plano
	and	b.nr_seq_mensalidade	= c.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_w
	and	((c.nr_sequencia	= nr_seq_mensalidade_p) or (nr_seq_mensalidade_p is null))
	order by ie_tipo_item;

Cursor C02 is
	select	a.nr_sequencia,
		a.cd_historico_padrao,
		a.cd_historico_estorno,
		a.cd_historico_baixa
	from	pls_esquema_contabil	a
	where	a.ie_tipo_regra		= 'IM'
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	dt_referencia_w between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_referencia_w)
	and	((a.ie_tipo_outorgante = ie_tipo_outorgante_w) or (a.ie_tipo_outorgante is null))
	and	((a.ie_tipo_segurado = ie_tipo_segurado_w) or (a.ie_tipo_segurado is null))
	and	((a.ie_tipo_plano = ie_tipo_operacao_w) or (a.ie_tipo_plano is null))
	and	((a.cd_tributo = cd_tributo_w) or (a.cd_tributo is null))
	and	((a.ie_retencao = ie_retencao_w) or (a.ie_retencao is null))
	order by	nvl(a.ie_retencao,' '),
			nvl(a.cd_tributo,0),
			nvl(a.ie_tipo_segurado,' '),
			nvl(a.ie_tipo_plano,' '),
			nvl(a.ie_tipo_outorgante,' '),
			nvl(a.dt_inicio_vigencia,sysdate);

Cursor C03 is
	select	ie_codificacao,
		vl_fixo,
		cd_conta_contabil,
		ie_debito_credito,
		ds_mascara
	from	pls_esquema_contabil_seg
	where	nr_seq_regra_esquema = nr_seq_esquema_w
	order by	ie_debito_credito,
			nr_seq_apresentacao;

begin

select	max(ie_tipo_outorgante)
into	ie_tipo_outorgante_w
from	pls_outorgante
where	cd_estabelecimento	= cd_estabelecimento_p;

nr_seq_lote_w	:= nr_seq_lote_p;

if (nvl(nr_seq_lote_w,0) = 0) and (nvl(nr_seq_mensalidade_p,0) <> 0) then

	select	max(nr_seq_lote)
	into	nr_seq_lote_w
	from	pls_mensalidade
	where	nr_sequencia = nr_seq_mensalidade_p;

end if;

open C01;
loop
fetch C01 into	
	nr_seq_trib_mens_w,
	nr_seq_item_w,
	ie_tipo_segurado_w,
	ie_tipo_item_w,
	dt_referencia_w,
	ie_tipo_operacao_w,
	nr_seq_segurado_w,
	cd_tributo_w,
	cd_conta_credito_item_w,
	cd_conta_debito_item_w,
	ie_retencao_w,
	cd_centro_custo_w,
	nr_seq_conta_w,
	ie_cancelamento_w,
	nr_seq_mens_segurado_w;
exit when C01%notfound;
	begin
	nr_seq_esquema_w 	:= null;
	
	if	(ie_tipo_item_w = '3') then
		select	max(c.cd_conta_cred),
			max(c.cd_conta_deb)
		into	cd_conta_credito_item_w,
			cd_conta_debito_item_w
		from	pls_mensalidade_seg_item	a,
			pls_conta			b,
			pls_conta_coparticipacao	c,
			pls_mensalidade_segurado	d
		where	a.nr_seq_conta			= b.nr_sequencia
		and	c.nr_seq_conta			= b.nr_sequencia
		and	a.nr_seq_mensalidade_seg	= d.nr_sequencia
		and	a.nr_sequencia			= nr_seq_item_w;
	end if;
	
	if	(ie_tipo_item_w = '6') then
		select	max(e.cd_conta_cred),
			max(e.cd_conta_deb)
		into	cd_conta_credito_item_w,
			cd_conta_debito_item_w
		from	pls_mensalidade_seg_item	i,
			pls_conta			c,
			pls_conta_pos_estabelecido	e
		where	c.nr_sequencia	= i.nr_seq_conta
		and	c.nr_sequencia	= e.nr_seq_conta
		and	i.nr_sequencia	= nr_seq_item_w;
	end if;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_esquema_w,
		cd_historico_padrao_w,
		cd_historico_estorno_w,
		cd_historico_baixa_w;
	exit when C02%notfound;
	end loop;
	close C02;
	
	cd_classificacao_debito_w	:= '';
	cd_classificacao_credito_w	:= '';
	cd_conta_credito_w		:= null;
	cd_conta_debito_w		:= null;
	
	open C03;
	loop
	fetch C03 into	
		ie_codificacao_w,
		vl_fixo_w,
		cd_conta_contabil_w,
		ie_debito_credito_w,
		ds_mascara_w;
	exit when C03%notfound;
		begin
		if	(ie_debito_credito_w = 'C') then /* Classifica��o CR�DITO */
			if	(ie_codificacao_w = 'CR') then /* C�digo reduzido */
				select	max(cd_classificacao_atual)
				into	cd_classificacao_credito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_contabil_w;
				
				cd_conta_credito_w	:= cd_conta_contabil_w;
			elsif	(ie_codificacao_w = 'IC') then
				select	max(cd_classificacao_atual)
				into	cd_classificacao_credito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_credito_item_w;
				
				cd_conta_credito_w	:= cd_conta_credito_item_w;
			elsif	(ie_codificacao_w = 'ID') then
				select	max(cd_classificacao_atual)
				into	cd_classificacao_credito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_debito_item_w;
				
				cd_conta_credito_w	:= cd_conta_debito_item_w;
			end if;
		elsif	(ie_debito_credito_w = 'D') then /* Classifica��o D�BITO */
			if	(ie_codificacao_w = 'CR') then /* C�digo reduzido */
				select	max(cd_classificacao_atual)
				into	cd_classificacao_debito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_contabil_w;
				
				cd_conta_debito_w	:= cd_conta_contabil_w;
			elsif	(ie_codificacao_w = 'IC') then
				select	max(cd_classificacao_atual)
				into	cd_classificacao_debito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_credito_item_w;
				
				cd_conta_debito_w	:= cd_conta_credito_item_w;
			elsif	(ie_codificacao_w = 'ID') then
				select	max(cd_classificacao_atual)
				into	cd_classificacao_debito_w
				from	conta_contabil
				where	cd_conta_contabil	= cd_conta_debito_item_w;
				
				cd_conta_debito_w	:= cd_conta_debito_item_w;
			end if;
		end if;
		end;
	end loop;
	close C03;

	if	(cd_conta_credito_w is null) then
		cd_conta_credito_w	:= ctb_obter_conta_classif(cd_classificacao_credito_w,dt_referencia_w,cd_estabelecimento_p);
	end if;

	if	(cd_conta_debito_w is null) then
		cd_conta_debito_w	:= ctb_obter_conta_classif(cd_classificacao_debito_w,dt_referencia_w,cd_estabelecimento_p);
	end if;
	
	if	(ie_tipo_item_w = 3) then
	
		begin
		
		select	max(cd_centro_custo)
		into	cd_centro_custo_w
		from	(	select	max(c.cd_centro_custo) cd_centro_custo
				from	pls_mensalidade_item_conta	a,
					pls_conta_coparticipacao	c
				where 	a.nr_seq_conta_copartic		= c.nr_sequencia
				and 	a.nr_Seq_item			= nr_seq_item_w
				and 	not exists (	select	1
							from	pls_mensalidade_item_conta z
							where	z.nr_seq_conta_copartic = c.nr_sequencia)
				union all
				select	max(b.cd_centro_custo) cd_centro_custo
				from	pls_conta			a,
					pls_conta_coparticipacao	b
				where	a.nr_sequencia	= b.nr_seq_conta
				and	(ie_cancelamento_w in ('C','E')
				or	b.nr_seq_mensalidade_seg = nr_seq_mens_segurado_w)
				and	a.nr_sequencia	= nr_seq_conta_w);

		exception
		when others then
			cd_centro_custo_w:= null;			 
		end;
		
	elsif	(ie_tipo_item_w = 6)	then
	
		begin
		
		select	max(cd_centro_custo)
		into	cd_centro_custo_w
		from	(	select	max(c.cd_centro_custo) cd_centro_custo
				from	pls_mensalidade_item_conta	a,
					pls_conta_pos_estabelecido	c
				where 	a.nr_seq_conta_pos_estab	= c.nr_sequencia
				and 	a.nr_Seq_item			= nr_seq_item_w
				and	not exists	(	select	1
								from	pls_mensalidade_item_conta	x
								where	c.nr_sequencia	= x.nr_seq_item)
				union all
				select	max(b.cd_centro_custo) cd_centro_custo
				from	pls_conta			a,
					pls_conta_pos_estabelecido	b
				where	a.nr_sequencia	= b.nr_seq_conta
				and	(ie_cancelamento_w in ('C','E')
				or	b.nr_seq_mensalidade_seg = nr_seq_mens_segurado_w)
				and	a.nr_sequencia	= nr_seq_conta_w);
	
		exception
		when others then
			cd_centro_custo_w:= null;
		end;
	
	end if;
	
	begin
	update	pls_mensalidade_trib
	set	cd_conta_credito	= cd_conta_credito_w,
		cd_conta_debito		= cd_conta_debito_w,
		cd_historico		= cd_historico_padrao_w,
		nr_seq_esquema		= nr_seq_esquema_w,
		cd_historico_estorno	= cd_historico_estorno_w,
		cd_centro_custo		= cd_centro_custo_w
	where	nr_sequencia		= nr_seq_trib_mens_w;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(188997,'NR_SEQ_ITEM='||nr_seq_item_w);
	end;
	end;
end loop;
close C01;

--commit;

end ctb_pls_atualizar_imposto_in;
/
	
