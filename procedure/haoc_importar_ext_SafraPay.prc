create or replace
procedure haoc_importar_ext_SafraPay ( 	nr_seq_extrato_p	number,
 					nm_usuario_p		varchar2,
 					ds_arquivo_p		varchar2) is

nr_resumo_w		varchar2(50);
qt_cv_aceitos_w		number(10);
--vl_bruto_w		number(15,2);
vl_total_w		number(15,2);
vl_descontos_w		number(15,4);
--vl_liquido_w		number(15,4);
cd_banco_w		varchar2(3);
cd_agencia_w		varchar2(6);
cd_conta_w		varchar2(11);
dt_pagto_w		date;
nr_seq_conta_banco_w	banco_estabelecimento.nr_sequencia%type;
cd_bandeira_w		varchar2(2);
nr_seq_bandeira_w	bandeira_cartao_cr.nr_sequencia%type;
nr_seq_extrato_res_w	extrato_cartao_cr_res.nr_sequencia%type;
vl_total_bruto_w	number(15,2);
vl_total_liquido_w	number(15,2);


Cursor C01 is --Resumos
	select	substr(ds_conteudo,18,50) nr_resumo,
		substr(ds_conteudo,100,8) qt_cv_aceitos,
		--to_number(substr(ds_conteudo,109,15))/100 vl_total, -- Inicial de 108 para 109, um 0 a menos, pra manter a variavel 15,2 padrao campo Tasy. 
		--to_number(substr(ds_conteudo,125,15))/100 vl_bruto, -- Inicial de 124 para 125, um 0 a menos, pra manter a variavel 15,2 padrao campo Tasy.
		--to_number(substr(ds_conteudo,143,15))/100 vl_descontos, -- Inicial de 140 para 143, tres 0 a menos, pra manter a variavel 15,2 padrao campo Tasy.
		--to_number(substr(ds_conteudo,158,15))/100 vl_liquido, -- Inicial de 158 para 161, tres 0 a menos, pra manter a variavel 15,2 padrao campo Tasy.
		substr(ds_conteudo,225,3) cd_banco,
		substr(ds_conteudo,228,6) cd_agencia,
		substr(ds_conteudo,234,11) cd_conta,
		to_date(substr(ds_conteudo,74,8),'yyyymmdd') dt_pagto,
		substr(ds_conteudo,49,2) cd_bandeira
	from	w_extrato_cartao_cr
	where	nr_seq_extrato				= nr_seq_extrato_p
	and	upper(substr(ds_conteudo,01,02))	= 'RO'
	order by substr(ds_conteudo,247,8) asc;
	
nr_nsu_w		varchar2(12);
dt_transacao_w		date;
ie_cred_deb_w		varchar2(1);
nr_parcela_w		varchar2(2);
nr_total_parcela_w	varchar2(2);
vl_bruto_w		number(15,2);
vl_desconto_w		number(15,2);
vl_liquido_w		number(15,2);
nr_autorizacao_w	varchar2(6);
ds_bandeira_w		varchar2(4);
	
Cursor C02 is --Movimentos CV
	select	substr(a.ds_conteudo,18,12) nr_nsu,
		to_date(substr(ds_conteudo,30,8),'yyyymmdd') dt_transacao,
		substr(a.ds_conteudo,53,1) ie_cred_deb,
		substr(a.ds_conteudo,107,2) nr_parcela,
		substr(a.ds_conteudo,109,2) nr_total_parcela,
		somente_numero(decode(substr(a.ds_conteudo,109,2),'00',substr(a.ds_conteudo,55,11),substr(a.ds_conteudo,123,11)))/100 vl_bruto,
		somente_numero(decode(substr(a.ds_conteudo,109,2),'00',substr(a.ds_conteudo,66,11),substr(a.ds_conteudo,134,11)))/100 vl_desconto,
		somente_numero(decode(substr(a.ds_conteudo,109,2),'00',substr(a.ds_conteudo,77,11),substr(a.ds_conteudo,145,11)))/100 vl_liquido,
		substr(a.ds_conteudo,182,6) nr_autorizacao,
		substr(a.ds_conteudo,188,4) ds_bandeira
	from	w_extrato_cartao_cr a
	where	substr(a.ds_conteudo,356,50)	= nr_resumo_w
	and	upper(substr(a.ds_conteudo,1,2))	= 'CV'
	and	a.nr_seq_extrato		= nr_seq_extrato_p;
	
dt_arquivo_w		date;
nr_extrato_w		varchar2(6);
nr_seq_extrato_arq_w	extrato_cartao_cr_arq.nr_sequencia%type;

cursor	c03 is
select	to_date(substr(ds_conteudo,9,8),'yyyymmdd') dt_arquivo,
	somente_numero(substr(a.ds_conteudo,23,6)) nr_extrato
from	w_extrato_cartao_cr a
where	substr(a.ds_conteudo,1,2)	= 'A0'
and	a.nr_seq_extrato		= nr_seq_extrato_p
order by	a.nr_sequencia;

begin

open C03;
loop
fetch C03 into	
	dt_arquivo_w,
	nr_extrato_w;
exit when C03%notfound;
	begin
	
	update	extrato_cartao_cr
	set	dt_atualizacao		= sysdate,
		dt_final		= dt_arquivo_w,
		dt_importacao		= sysdate,
		dt_inicial		= dt_arquivo_w,
		dt_processamento	= dt_arquivo_w,
		nm_usuario		= nm_usuario_p,
		nr_extrato		= nr_extrato_w
	where	nr_sequencia		= nr_seq_extrato_p;
	
	select	extrato_cartao_cr_arq_seq.nextval
	into	nr_seq_extrato_arq_w
	from	dual;

	insert	into extrato_cartao_cr_arq
		(ds_arquivo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_final,
		dt_inicial,
		ie_tipo_arquivo,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_extrato,
		nr_sequencia)
	values	(ds_arquivo_p,
		sysdate,
		sysdate,
		dt_arquivo_w,
		dt_arquivo_w,
		'F',
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_extrato_p,
		nr_seq_extrato_arq_w);

	open C01;
	loop
	fetch C01 into	
		nr_resumo_w,
		qt_cv_aceitos_w,
		--vl_bruto_w,
		--vl_total_w,
		--vl_descontos_w,
		--vl_liquido_w,
		cd_banco_w,
		cd_agencia_w,
		cd_conta_w,
		dt_pagto_w,
		cd_bandeira_w;
	exit when C01%notfound;
		begin
		
		select	max(nr_sequencia)
		into	nr_seq_bandeira_w
		from	bandeira_cartao_cr
		where	cd_bandeira is not null
		and	cd_bandeira = cd_bandeira_w;
		
		select	max(a.nr_sequencia)
		into	nr_seq_conta_banco_w
		from	banco_estabelecimento a,
			conta_banco_tipo b
		where	a.nr_seq_tipo_conta = b.nr_sequencia(+) 
		and	pls_elimina_zeros_esquerda(trim(a.cd_conta))	= pls_elimina_zeros_esquerda(trim(cd_conta_w))
		and	somente_numero(a.cd_agencia_bancaria)		= cd_agencia_w
		and	a.cd_banco					= cd_banco_w
		and  	nvl(b.ie_classif_conta,'CC') 			= 'CC';
		
		if	(nr_seq_conta_banco_w	is null) then

			select	max(a.nr_sequencia)
			into	nr_seq_conta_banco_w
			from	conta_banco_tipo b,
				banco_estabelecimento a
			where	b.ie_classif_conta	= 'CC'
			and	a.nr_seq_tipo_conta	= b.nr_sequencia
			and	lpad(trim(a.cd_conta) || trim(a.ie_digito_conta),14,0) 	= lpad(cd_conta_w,14,0)
			and	somente_numero(a.cd_agencia_bancaria)			= cd_agencia_w
			and	a.cd_banco						= cd_banco_w;

		end if;
		
		
		select	extrato_cartao_cr_res_seq.nextval
		into	nr_seq_extrato_res_w
		from	dual;

		insert	into	extrato_cartao_cr_res
			(nr_sequencia,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_atualizacao,
			nm_usuario,
			nr_seq_extrato,
			nr_resumo,
			nr_seq_conta_banco,
			qt_cv_aceito,
			qt_cv_rejeitado,
			vl_bruto,
			vl_comissao,
			vl_rejeitado,
			vl_liquido,
			dt_prev_pagto,
			nr_seq_bandeira,
			nr_seq_extrato_arq)
		values	(nr_seq_extrato_res_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_extrato_p,
			substr(nr_resumo_w,1,20),
			nr_seq_conta_banco_w,
			0,--qt_cv_aceito_w,
			0,--qt_cv_rejeitado_w,
			0,--vl_bruto_w,
			0,--vl_comissao_w,
			0,--vl_rejeitado_w,
			0,--vl_liquido_w,
			dt_pagto_w,
			nr_seq_bandeira_w,
			nr_seq_extrato_arq_w);
			
		open C02;
		loop
		fetch C02 into	
			nr_nsu_w,
			dt_transacao_w,
			ie_cred_deb_w,
			nr_parcela_w,
			nr_total_parcela_w,
			vl_bruto_w,
			vl_desconto_w,
			vl_liquido_w,
			nr_autorizacao_w,
			ds_bandeira_w;
		exit when C02%notfound;
			begin
			
			insert	into extrato_cartao_cr_movto
					  (ds_comprovante,---------
					  dt_atualizacao,
					  dt_atualizacao_nrec,
					  dt_compra,
					  ie_pagto_indevido,
					  nm_usuario,
					  nm_usuario_nrec,
					  nr_autorizacao,
					  --nr_cartao,
					  nr_parcela,
					  nr_resumo,
					  nr_seq_extrato,
					  nr_seq_extrato_arq,
					  nr_seq_extrato_res,
					  nr_sequencia,
					  qt_parcelas,
					  vl_aconciliar,
					  vl_liquido,
					  vl_parcela,
					  vl_saldo_concil_fin)
				values	( nr_nsu_w,
					  sysdate,
					  sysdate,
					  dt_transacao_w,
					  'N',
					  nm_usuario_p,
					  nm_usuario_p,
					  nr_autorizacao_w,
					  --nr_cartao_w,
					  decode(nr_parcela_w,'00','01',nr_parcela_w),
					  substr(nr_resumo_w,1,20),
					  nr_seq_extrato_p,
					  nr_seq_extrato_arq_w,
					  nr_seq_extrato_res_w,
					  extrato_cartao_cr_movto_seq.nextval,
					  decode(nr_total_parcela_w,'00','01',nr_total_parcela_w),
					  vl_bruto_w,
					  vl_liquido_w,
					  vl_bruto_w,
					  vl_bruto_w);
					  
				--qt_cv_aceitos_w 	:= nvl(qt_cv_aceitos_w,0) + 1;
				vl_total_bruto_w	:= nvl(vl_total_bruto_w,0) + vl_bruto_w;
				vl_total_liquido_w	:= nvl(vl_total_liquido_w,0) + vl_liquido_w;
			
			end;
		end loop;
		close C02;
		
		update	extrato_cartao_cr_res
		set	qt_cv_aceito	= qt_cv_aceitos_w,
			vl_bruto	= vl_total_bruto_w,
			vl_liquido	= vl_total_liquido_w
		where	nr_sequencia	= nr_seq_extrato_res_w
		and	nr_seq_extrato	= nr_seq_extrato_p;
		
		--qt_cv_aceitos_w 	:= 0;
		vl_total_bruto_w 	:= 0;
		vl_total_liquido_w 	:= 0;
		
		end;
	end loop;
	close C01;
	
	end;
end loop;
close C03;

delete	w_extrato_cartao_cr
where	nr_seq_extrato	= nr_seq_extrato_p;

commit;

end haoc_importar_ext_SafraPay;
/