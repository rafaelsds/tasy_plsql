create or replace
procedure Liberar_retorno_recado(nr_seq_recado_p	number,
				 nm_usuario_p		Varchar2) is 

begin

update	recado_medico
set	dt_atualizacao		= sysdate,
	nm_usuario_lib_ret	= nm_usuario_p,
	dt_liberacao_retorno	= sysdate
where	nr_sequencia		= nr_seq_recado_p;

commit;

end Liberar_retorno_recado;
/