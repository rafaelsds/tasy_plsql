create or replace
procedure proj_gerar_result_realizado
			(	nr_sequencia_p		Number,
				nm_usuario_p		Varchar2) is 

nr_seq_proj_w			Number(10);
cd_executor_w			Varchar2(10);
qt_hora_w			Number(17,2)	:= 0;
vl_hora_cobrar_w		Number(15,2)	:= 0;
vl_hora_pagar_w			Number(15,2)	:= 0;
vl_pagar_w			Number(15,2)	:= 0;
vl_cobrar_cliente_w		Number(15,2)	:= 0;
vl_imposto_w			Number(15,2)	:= 0;
vl_adicional_clt_w		Number(15,2)	:= 0;
vl_custo_inst_w			Number(15,2)	:= 0;
vl_margem_w			Number(15,2)	:= 0;
vl_fixo_consultor_w		Number(15,2)	:= 0;

Cursor C01 is
	select	cd_executor,
		sum(nvl(qt_total_hora,0)) qt_total_hora,
		dividir(sum(nvl(vl_cobrar_cliente,0)),sum(nvl(qt_total_hora,0))) vl_hora_cobrar,
		sum(nvl(vl_cobrar_cliente,0)) vl_cobrar_cliente,
		dividir(sum(nvl(vl_pagar,0)),sum(nvl(qt_total_hora,0))) vl_hora_pagar,
		sum(nvl(vl_pagar,0)) vl_pagar,
		sum(nvl(vl_imposto,0)),
		sum(nvl(vl_adicional_clt,0)),
		sum(nvl(vl_custo_inst,0)),
		sum(nvl(vl_fixo_consultor,0))
	from	proj_rat
	where	nr_seq_proj	= nr_seq_proj_w
	and 	nr_ordem_compra is not null
	group by cd_executor;

begin

delete	proj_orc_prof
where	nr_seq_orc	= nr_sequencia_p
and	ie_tipo_valor	= 'R';

select	nr_seq_proj
into	nr_seq_proj_w
from	proj_orcamento
where	nr_sequencia	= nr_sequencia_p;

open C01;
loop
fetch C01 into	
	cd_executor_w,
	qt_hora_w,
	vl_hora_cobrar_w,
	vl_cobrar_cliente_w,
	vl_hora_pagar_w,
	vl_pagar_w,
	vl_imposto_w,
	vl_adicional_clt_w,
	vl_custo_inst_w,
	vl_fixo_consultor_w;
exit when C01%notfound;
	begin
	vl_margem_w	:= vl_cobrar_cliente_w - (vl_pagar_w + vl_imposto_w + vl_adicional_clt_w + vl_custo_inst_w);
	insert into proj_orc_prof
		(nr_sequencia, nr_seq_orc, dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nr_seq_nivel_cons, ie_coordenador, qt_semana,
		qt_hora_sem, vl_hora_rec, qt_profissional,
		vl_custo_hora, vl_custo_total, vl_receita_total,
		vl_imposto, vl_margem, cd_executor,
		vl_adicional_clt, vl_custo_inst, ie_tipo_valor,
		qt_horas, vl_fixo)
	values(	proj_orc_prof_seq.nextval, nr_sequencia_p, sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,
		null, 'N', 0,
		0, vl_hora_cobrar_w, 1,
		vl_hora_pagar_w, vl_pagar_w, vl_cobrar_cliente_w,
		vl_imposto_w, vl_margem_w, cd_executor_w,
		vl_adicional_clt_w, vl_custo_inst_w, 'R',
		qt_hora_w, vl_fixo_consultor_w);
	end;
end loop;
close C01;

commit;

end proj_gerar_result_realizado;
/
