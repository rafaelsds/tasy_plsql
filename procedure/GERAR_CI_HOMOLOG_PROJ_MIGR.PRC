create or replace
procedure gerar_ci_homolog_proj_migr (
		nr_seq_projeto_p	number,
		nm_usuario_homolog_p	varchar2,
		nm_usuario_p		varchar2) is
		
nm_usuario_migracao_w		varchar2(15);
nr_seq_funcao_w			number(10,0);
nm_recurso_migracao_w		varchar2(60);
nm_analista_migracao_w		varchar2(60);
nm_programador_migracao_w	varchar2(60);
nm_usuario_gerente_w		varchar2(15);
nm_usuario_analista_w		varchar2(15);
ds_grupo_desenvolvimento_w	varchar2(80);
nm_usuarios_destino_w		varchar2(4000);
ds_funcao_w			varchar2(80);
ds_form_w			varchar2(10);
ds_comunicado_w			varchar2(32000);
		
cursor c01 is
select	obter_usuario_pf(p.cd_pessoa_fisica),
	p.nr_seq_funcao,
	obter_nome_pf(p.cd_pessoa_fisica)
from	proj_equipe_papel p,
	proj_equipe e
where	p.nr_seq_equipe = e.nr_sequencia
and	e.nr_seq_proj = nr_seq_projeto_p
and	e.nr_seq_equipe_funcao = 11
and	((nvl(p.ie_funcao_rec_migr,'M') in ('M','T','C')) or (p.nr_seq_funcao in (46,45)))
and	nvl(p.ie_situacao,'A') = 'A';

cursor c02 is
select	obter_usuario_pf(m.cd_responsavel),
	g.nm_usuario_lider,
	g.ds_grupo
from	gerencia_wheb m,
	grupo_desenvolvimento g,
	funcao_grupo_des f,
	proj_projeto p
where	m.nr_sequencia = g.nr_seq_gerencia
and	g.nr_sequencia = f.nr_seq_grupo
and	p.cd_funcao = f.cd_funcao
and	p.nr_sequencia = nr_seq_projeto_p;
		
begin
if	(nr_seq_projeto_p is not null) and
	(nm_usuario_homolog_p is not null) and
	(nm_usuario_p is not null) then
	begin
	open c01;
	loop
	fetch c01 into	nm_usuario_migracao_w,
			nr_seq_funcao_w,
			nm_recurso_migracao_w;
	exit when c01%notfound;
		begin
		nm_usuarios_destino_w := nm_usuarios_destino_w || nm_usuario_migracao_w || ', ';
		end;
	end loop;
	close c01;
	
	open c02;
	loop
	fetch c02 into	nm_usuario_gerente_w,
			nm_usuario_analista_w,
			ds_grupo_desenvolvimento_w;
	exit when c02%notfound;
		begin
		nm_usuarios_destino_w := nm_usuarios_destino_w || nm_usuario_gerente_w || ', ' || nm_usuario_analista_w || ', ';
		end;
	end loop;
	close c02;
	
	nm_usuarios_destino_w := nm_usuarios_destino_w || 'Marcus' || ', ';
	
	select	f.ds_funcao,
		f.ds_form
	into	ds_funcao_w,
		ds_form_w
	from	funcao f,
		proj_projeto p
	where	f.cd_funcao = p.cd_funcao
	and	p.nr_sequencia = nr_seq_projeto_p;
	
	ds_comunicado_w	:=	'{\rtf1\ansi\deff0{\fonttbl{\f0\fnil\fcharset0 Verdana;}{\f1\fnil Verdana;}} ' ||
				'{\colortbl ;\red0\green0\blue0;} ' ||
				'\viewkind4\uc1\pard\cf1\lang1046\f0\fs20 ' ||
				'\par ' || 'Prezados, ' ||
				'\par ' ||
				'\par ' || '� com enorme alegria que comunicamos a homologa��o do projeto de migra��o da fun��o ' || ds_funcao_w || ' (' || ds_form_w || ') por ' || obter_pessoa_fisica_usuario(nm_usuario_homolog_p,'N') || ' em ' || to_char(trunc(sysdate,'dd'),'dd/mm/yyyy') || '.' ||
				'\par ' ||
				'\par ' || 'Muito obrigado pela colabora��o, comprometimento e dedica��o de todos durante todo o processo nesse sentido.' ||
				'\par ' ||
				'\par ' || 'Obrigado e bom trabalho a todos :)' ||
				'\pard\cf1\f1\fs20 \f1 \par } ';
				
	gerar_comunic_padrao(
		sysdate,
		'Homologa��o do Projeto de Migra��o - ' || ds_funcao_w,
		ds_comunicado_w,
		nm_usuario_p,
		'N',
		nm_usuarios_destino_w,
		'N',
		null,
		null,
		1,
		null,
		sysdate,
		null,
		null);
	end;
end if;
commit;
end gerar_ci_homolog_proj_migr;
/