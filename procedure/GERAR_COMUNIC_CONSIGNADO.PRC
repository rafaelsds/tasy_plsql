create or replace
procedure Gerar_Comunic_Consignado(
  			nm_usuario_p 		varchar2,
			nm_usuario_dest_p	varchar2,			
  			ds_mensagem_p		varchar2,
			ds_perfil_adicional_p	varchar2) is

nr_seq_comunicacao_w 	number(10,0);
nm_usuario_destino_w	varchar2(15);
cd_perfil_comunic_w	varchar2(10);

begin

if	(nvl(ds_perfil_adicional_p,0) > 0) then
	cd_perfil_comunic_w := substr(ds_perfil_adicional_p || ',',1,10);
end if;

if	(nm_usuario_dest_p is not null) or (nvl(ds_perfil_adicional_p,0) > 0) then
 	begin
 	
	select comunic_interna_seq.nextval
 	into nr_seq_comunicacao_w
 	from dual;
  	
	insert into comunic_interna
  				(dt_comunicado,
  				ds_titulo,
				ds_comunicado,
  				nm_usuario,
  				dt_atualizacao,
  				ie_geral,
  				nm_usuario_destino,
  				cd_perfil,
  				nr_sequencia,
  				ie_gerencial,
  				nr_seq_classif,
  				ds_perfil_adicional,
  				cd_setor_destino,
  				cd_estab_destino,
  				ds_setor_adicional,
  				dt_liberacao)
	values 		(sysdate,
  				wheb_mensagem_pck.get_Texto(311898), /*'Consumo de item consignado',*/
  				ds_mensagem_p, 
  				nm_usuario_p,
  				sysdate,
  				'N',
  				nm_usuario_dest_p,
  				null,
  				nr_seq_comunicacao_w,
  				'N', null, cd_perfil_comunic_w, null, null, null, sysdate);
  	end;
end if;

end Gerar_Comunic_Consignado;
/