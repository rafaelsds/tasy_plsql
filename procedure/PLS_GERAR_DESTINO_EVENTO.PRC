create or replace
procedure pls_gerar_destino_evento
			(	nr_seq_evento_controle_p	Number,
				ie_status_envio_p		Varchar2,
				ie_forma_envio_p		Varchar2,				
				nm_usuario_p			Varchar2,
				cd_pessoa_fisica_p		Varchar2,
				nm_usuario_destino_p		Varchar2,
				ds_mensagem_p			Varchar2,
				id_sms_p			Number,
				nr_telef_celular_p		Varchar2) is
	
id_sms_w		Number(10);	
dt_envio_msg_w		Date 		:= null;

begin

if	(ie_status_envio_p = 'E') then
	dt_envio_msg_w 	:= sysdate;
end if;

if	(cd_pessoa_fisica_p is not null) then
	insert into pls_alerta_evento_cont_des(
			nr_sequencia, nr_seq_evento_controle, ie_status_envio,
			ie_forma_envio, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, ie_tipo_pessoa_dest,
			dt_envio_mensagem, id_sms, nm_usuario_destino,
			cd_pessoa_fisica, ds_mensagem, nr_telef_celular)
		values(	pls_alerta_evento_cont_des_seq.nextval, nr_seq_evento_controle_p, ie_status_envio_p,
			ie_forma_envio_p, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, null,
			dt_envio_msg_w, id_sms_p, nm_usuario_destino_p,
			cd_pessoa_fisica_p, ds_mensagem_p, nr_telef_celular_p);
end if;

end pls_gerar_destino_evento;
/