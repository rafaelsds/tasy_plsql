create or replace 
procedure pls_gerar_evento_movto_fixo(	nr_seq_lote_evento_p		pls_lote_evento.nr_sequencia%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type,
					ie_opcao_p			varchar2,
					ie_commit_p			varchar2) is
					
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar os lan�amentos programados conforme as regras dos eventos, esta rotina
tamb�m desfaz o lote de eventos.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	ie_opcao_p:
		G - Gerar lan�amentos programados
		D - Defazer lan�amentos programados

	PLS_GERAR_MOVTO_COND_PGTO

	Caso seja desfeito o lote de eventos manualmente � consistido se seus movimentos
	n�o est�o vinculados a um lote de pagamento.
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	OPS - Controle de Eventos e Ocorr�ncias Financeiras
	GERAR_LANCAMENTO_PROGRAMADO
	PLS_DESFAZER_LOTE_PAGAMENTO
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_observacao_w			pls_evento_regra_fixo.ds_observacao%type;
ds_erro_w			varchar2(255)	:= null;
cd_conta_contabil_w		pls_evento_movimento.cd_conta_contabil%type;
cd_pf_titulo_pagar_w		pls_evento_regra_fixo.cd_pf_titulo_pagar%type;
cd_cgc_titulo_pagar_w		pls_evento_regra_fixo.cd_cgc_titulo_pagar%type;
ie_forma_incidencia_w		pls_evento_regra_fixo.ie_forma_incidencia%type;
ie_gerar_w			varchar2(5);
ie_situacao_w			pls_prestador.ie_situacao%type;
ie_incidencia_lanc_prog_w	pls_evento.ie_incidencia_lanc_prog%type;
ie_periodo_aprop_neg_w		pls_prestador_pagto.ie_periodo_aprop_neg%type	:= 'N';
ie_prestador_matriz_w		pls_evento_regra_fixo.ie_prestador_matriz%type 	:= 'N';
ie_titulo_pagar_w		pls_evento_movimento.ie_titulo_pagar%type	:= 'N';
ie_evento_orig_prox_pagto_w	pls_parametro_pagamento.ie_evento_orig_prox_pagto%type;
vl_regra_w			pls_evento_regra_fixo.vl_regra%type;
nr_seq_evento_w			pls_evento.nr_sequencia%type;
nr_seq_prestador_w		pls_prestador.nr_sequencia%type;
nr_seq_regra_fixo_w		pls_evento_regra_fixo.nr_sequencia%type;
nr_seq_prest_princ_w		pls_prestador.nr_seq_prest_princ%type;
nr_seq_periodo_pag_w		pls_periodo_pagamento.nr_sequencia%type;
nr_seq_vencimento_w		pls_pag_prest_vencimento.nr_sequencia%type;
nr_seq_evento_movto_w		pls_evento_movimento.nr_sequencia%type;
nr_seq_lote_pgto_w		pls_evento_movimento.nr_seq_lote_pgto%type;
qt_excessoes_w			pls_integer;
nr_seq_lote_pag_w		pls_lote_evento.nr_seq_lote_pagamento%type;
cd_condicao_pagamento_w		pls_evento_regra_fixo.cd_condicao_pagamento%type;
nr_seq_tipo_prestador_w		pls_prestador.nr_seq_tipo_prestador%type;
nr_seq_periodo_w		pls_lote_pagamento.nr_seq_periodo%type;
nr_seq_classe_tit_rec_w		pls_evento_regra_fixo.nr_seq_classe_titulo%type;
qt_dia_venc_w			pls_evento_regra_fixo.qt_dia_venc%type;
qt_registro_w			pls_integer;
nr_seq_lote_w			pls_lote_pagamento.nr_sequencia%type;
nr_seq_evento_lote_w		pls_lote_evento.nr_seq_evento%type;
nr_seq_trans_fin_baixa_w	pls_evento_regra_fixo.nr_seq_trans_fin_baixa%type;
nr_seq_trans_fin_contab_w	pls_evento_regra_fixo.nr_seq_trans_fin_contab%type;
dt_competencia_w		pls_lote_evento.dt_competencia%type;
dt_movimento_w			pls_evento_regra_fixo.dt_movimento%type := null;
nr_seq_prestador_orig_w		pls_prestador.nr_sequencia%type;
qt_total_aprop_w		pls_integer := 0;
qt_aprop_w			pls_integer := 0;
nr_seq_venc_valor_w		pls_pag_prest_venc_valor.nr_sequencia%type;
cd_centro_custo_w		pls_evento_regra_fixo.cd_centro_custo%type;
ie_gerar_evento_prox_pag_w	pls_parametro_pagamento.ie_gerar_evento_prox_pag%type;
ie_consiste_periodo_pag_w	pls_evento.ie_consiste_periodo_pag%type;
ie_complementar_w		pls_periodo_pagamento.ie_complementar%type;
ie_data_lanc_unico_w		pls_parametro_pagamento.ie_data_lanc_unico%type;
dt_inicio_comp_w		pls_lote_evento.dt_inicio_comp%type;
dt_fim_comp_w			pls_lote_evento.dt_fim_comp%type;
nr_seq_classificacao_w		pls_prestador.nr_seq_classificacao%type;
nr_seq_regra_w			pls_prestador_crit_receb.nr_sequencia%type;
nr_seq_prestador_regra_w	pls_prestador_crit_receb.nr_seq_prest_receb%type;
nr_seq_prestador_pgto_w		pls_prestador.nr_sequencia%type;
qt_apropriacao_w		pls_integer;
ie_restringe_estab_w		varchar2(1);
nr_seq_evento_prox_pgto_w	pls_parametro_pagamento.nr_seq_evento_prox_pgto%type;
ie_aplicar_regra_prest_w	pls_evento.ie_aplicar_regra_prest%type;

Cursor C01 is
	select	a.nr_sequencia,
		b.ie_forma_incidencia,
		decode(nvl(b.ie_forma_valor, 'I'), 'I', decode(a.ie_natureza,'D',(abs(b.vl_regra)*(-1)),abs(b.vl_regra)), null), /* Trata se o valor � informado ou conforme Regra de incid�ncia sobre ouro evento - Pasta Regra valor */
		b.nr_sequencia,
		substr(b.ds_observacao,1,2000),
		b.cd_condicao_pagamento,
		b.nr_seq_periodo,
		b.nr_seq_classe_titulo,
		b.qt_dia_venc,
		nvl(a.ie_incidencia_lanc_prog,'N') /* Verifica se deve ser verificado se a regra j� gerou movimento ou se a restri��o deve ser pelo evento */,
		nvl(b.ie_prestador_matriz,'N'),
		b.dt_movimento,
		nvl(b.ie_titulo_pagar, 'N'),
		b.cd_pf_titulo_pagar,
		b.cd_cgc_titulo_pagar,
		b.nr_seq_trans_fin_baixa,
		b.nr_seq_trans_fin_contab,
		nvl(a.ie_consiste_periodo_pag, 'S'),
		b.cd_centro_custo,
		nvl(a.ie_aplicar_regra_prest, 'S') ie_aplicar_regra_prest
	from	pls_evento_regra_fixo	b,
		pls_evento		a
	where	a.nr_sequencia		= b.nr_seq_evento
	and	a.ie_situacao		= 'A'
	and	a.ie_tipo_evento 	= 'F'
	and	nvl(b.ie_gerar_apos_tributacao,'N') = 'N'
	and	nvl(b.nr_seq_prestador,nr_seq_prestador_w) = nr_seq_prestador_w
	and	nvl(b.nr_seq_tipo_prestador,nr_seq_tipo_prestador_w) = nr_seq_tipo_prestador_w
	and	(nvl(b.ie_situacao_prest,ie_situacao_w) = ie_situacao_w or b.ie_situacao_prest = 'T')
	and	((ie_restringe_estab_w = 'N') or (a.cd_estabelecimento = cd_estabelecimento_p))
	and	(exists	(select	1
			from	pls_prestador x
			where	x.nr_sequencia	= nr_seq_prestador_w
			and	(pls_obter_situacao_coop_prest(x.cd_pessoa_fisica,x.cd_cgc)	= b.nr_seq_sit_coop or b.nr_seq_sit_coop is null)
			and	substr(pls_obter_se_cooperado(x.cd_pessoa_fisica,x.cd_cgc),1,1) = 'S') or nvl(b.ie_cooperado,'N') = 'N')
	-- Se o forma de incidencia for de lan�amento �nico, dever� respeitar a parametriza��o do campo ie_data_lanc_unico_w
	and	(((b.ie_forma_incidencia = 'U') and (
			-- Se ie_data_lanc_unico_w for 'M' (Filtrar pelo m�s de refer�ncia do pagamento)
			((ie_data_lanc_unico_w = 'M') and (dt_competencia_w = trunc(b.dt_inicio_vigencia,'month'))
		or
			-- Se ie_data_lanc_unico_w for 'D' (Filtrar pelas datas de in�cio e fim de compet�ncia do lote (di�rio))
			((ie_data_lanc_unico_w = 'D') and (trunc(b.dt_inicio_vigencia,'dd') between nvl(dt_inicio_comp_w,dt_competencia_w) and nvl(dt_fim_comp_w,fim_dia(last_day(dt_competencia_w))))))
		)) or
		-- Se o forma de incidencia N�O for de lan�amento �nico, dever� respeitar  data de compet�ncia
		((b.ie_forma_incidencia <> 'U') and (dt_competencia_w between trunc(b.dt_inicio_vigencia,'month') and nvl(trunc(b.dt_fim_vigencia,'month'),dt_competencia_w)))
	)
	-- jtonon - OS 951976 - A restri��o abaixo foi removida visto que este tratamento ignorava a configura��o do 'ie_data_lanc_unico_w'.
	-- Ex.: Lote de pagamento refer�ncia 10/2015 com in�cio/fim compet�ncia 09/15 � 11/15. Lan�amento �nico de 01/09/15. Configura��o na gest�o de operadora 'Filtrar pelas datas de in�cio e fim de compet�ncia do lote (di�rio)'.
	-- At� nesta linha do c�digo o sistema estava filtrando corretamente, por�m, na linha abaixo o sistema for�ava que a data de lan�amento �nico, que neste caso era 01/09/15, fosse da mesma compet�ncia do lote de pagamento, assim ignorando as restri��es  do 'ie_data_lanc_unico_w'.
	--and	((dt_competencia_w = trunc(b.dt_inicio_vigencia,'month')) or (b.ie_forma_incidencia <> 'U'))
	and	a.nr_sequencia = nvl(nr_seq_evento_lote_w,a.nr_sequencia)
	and	((b.nr_seq_periodo = nr_seq_periodo_pag_w) or (b.nr_seq_periodo is null) or (nr_seq_periodo_pag_w is null)) -- tratado para quando for na aba movimenta��o das ocorr�ncias financeiras n�o considerar este campo
	order by
		a.nr_sequencia,
		nvl(b.nr_seq_ordem,0);

Cursor C02 is
	select	sum(x.vl_vencimento),
		a.nr_seq_prestador,
		b.nr_sequencia,
		a.nr_seq_lote
	from	pls_pag_prest_venc_valor	x,
		pls_pag_prest_vencimento	b,
		pls_pagamento_prestador		a
	where	b.nr_sequencia		= x.nr_seq_vencimento
	and	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	b.ie_proximo_pgto	= 'S'
	and	x.ie_tipo_valor		= 'PP'
	and	b.nr_seq_evento_movto is null
	and	a.ie_cancelamento is null
	and	x.nr_seq_evento = nvl(nr_seq_evento_lote_w, x.nr_seq_evento)
	group by
		a.nr_seq_prestador,
		b.nr_sequencia,
		a.nr_seq_lote;
	-- OS 448782 - Retirado tratamento para gerar o valor independente do m�s pois s� ir� gerar uma vez

Cursor C03 is
	select	nr_sequencia,
		nvl(ie_situacao,'A'),
		nvl(nr_seq_tipo_prestador,0),
		nr_seq_classificacao
	from	pls_prestador;

Cursor C04 is
	select	sum(x.vl_vencimento),
		a.nr_seq_prestador,
		b.nr_sequencia,
		a.nr_seq_lote,
		x.nr_seq_evento,
		x.nr_sequencia
	from	pls_pag_prest_venc_valor	x,
		pls_pag_prest_vencimento	b,
		pls_pagamento_prestador		a
	where	b.nr_sequencia		= x.nr_seq_vencimento
	and	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	b.ie_proximo_pgto	= 'S'
	and	x.ie_tipo_valor		= 'PP'
	and	x.nr_seq_evento_movto is null
	and	a.ie_cancelamento is null
	and	x.nr_seq_evento = nvl(nr_seq_evento_lote_w, x.nr_seq_evento)
	group by
		a.nr_seq_prestador,
		b.nr_sequencia,
		a.nr_seq_lote,
		x.nr_seq_evento,
		x.nr_sequencia;

Cursor C05 is
	select	b.nr_sequencia
	from	pls_pag_prest_vencimento	b,
		pls_pagamento_prestador		a
	where	a.nr_sequencia		= b.nr_seq_pag_prestador
	and	b.ie_proximo_pgto	= 'S'
	and	exists	(select	1
			from	pls_pag_prest_venc_valor	x,
				pls_evento_movimento		y
			where	y.nr_sequencia	= x.nr_seq_evento_movto
			and	b.nr_sequencia	= x.nr_seq_vencimento
			and	x.ie_tipo_valor	= 'PP')
	and	b.nr_seq_evento_movto is null
	and	a.ie_cancelamento is null;

begin

-- Informar se o controle de eventos e ocorr�ncias financeiras deve restringir por estabelecimento
ie_restringe_estab_w := pls_obter_se_controle_estab('LPM');

-- Por ora define o lote como N�o complementar
ie_complementar_w := 'N';

select	max(nr_seq_lote_pagamento),
	max(nr_seq_evento)
into	nr_seq_lote_pag_w,
	nr_seq_evento_lote_w
from	pls_lote_evento
where	nr_sequencia	= nr_seq_lote_evento_p;

select	max(nr_seq_periodo)
into	nr_seq_periodo_pag_w
from	pls_lote_pagamento a
where	a.nr_sequencia = nr_seq_lote_pag_w;

if	(nr_seq_periodo_pag_w is not null) then
	select	nvl(ie_complementar,'N')
	into	ie_complementar_w
	from	pls_periodo_pagamento
	where	nr_sequencia	= nr_seq_periodo_pag_w;
end if;

-- carrega os par�metros
select	nvl(max(a.ie_data_lanc_unico),'M'),
	max(a.nr_seq_evento_prox_pgto),
	nvl(max(a.ie_evento_orig_prox_pagto),'N'),
	nvl(max(a.ie_gerar_evento_prox_pag), 'N')
into	ie_data_lanc_unico_w,
	nr_seq_evento_prox_pgto_w,
	ie_evento_orig_prox_pagto_w,
	ie_gerar_evento_prox_pag_w
from	pls_parametro_pagamento a
where	a.cd_estabelecimento = cd_estabelecimento_p;


-- Eventos que n�o estiverem o ie_aplicar_regra_prest informado, ent�o o padr�o dever� ser "S"
update	pls_evento
set	ie_aplicar_regra_prest	= 'S'
where	ie_aplicar_regra_prest	is null;


if	(ie_opcao_p = 'G') then

	delete	pls_evento_movimento /* Limpa as ocorr�ncias que foram geradas pelas regras de Lan�amentos Programados */
	where	nr_seq_regra_fixo_w is not null
	and	nr_seq_lote	= nr_seq_lote_evento_p;

	select	trunc(dt_competencia, 'month'),
		trunc(dt_inicio_comp,'dd'),
		trunc(dt_fim_comp,'dd')
	into	dt_competencia_w,
		dt_inicio_comp_w,
		dt_fim_comp_w
	from	pls_lote_evento
	where	nr_sequencia	= nr_seq_lote_evento_p;
	
	open C03; /* Verifica prestadores ativos */
	loop
	fetch C03 into
		nr_seq_prestador_w,
		ie_situacao_w,
		nr_seq_tipo_prestador_w,
		nr_seq_classificacao_w;
	exit when C03%notfound;
		begin
		open C01; /* Busca lan�amentos programados conforme regras */
		loop
		fetch C01 into
			nr_seq_evento_w,
			ie_forma_incidencia_w,
			vl_regra_w,
			nr_seq_regra_fixo_w,
			ds_observacao_w,
			cd_condicao_pagamento_w,
			nr_seq_periodo_w,
			nr_seq_classe_tit_rec_w,
			qt_dia_venc_w,
			ie_incidencia_lanc_prog_w,
			ie_prestador_matriz_w,
			dt_movimento_w,
			ie_titulo_pagar_w,
			cd_pf_titulo_pagar_w,
			cd_cgc_titulo_pagar_w,
			nr_seq_trans_fin_baixa_w,
			nr_seq_trans_fin_contab_w,
			ie_consiste_periodo_pag_w,
			cd_centro_custo_w,
			ie_aplicar_regra_prest_w;
		exit when C01%notfound;
			begin
			ie_gerar_w	:= 'S';

			if	(ie_prestador_matriz_w = 'S') then
				select	a.nr_seq_prest_princ
				into	nr_seq_prest_princ_w
				from	pls_prestador	a
				where	a.nr_sequencia	= nr_seq_prestador_w;

				if	(nr_seq_prest_princ_w is not null) then
					nr_seq_prestador_w	:= nr_seq_prest_princ_w;
				end if;
			end if;

			/*aaschlote 11/04/2014 OS - 724650 - Caso o lote for complementar, ent�o nem entra na  consist�ncia, caso n�o for de origem de lote de pagamento ou o per�odo do lote n�o for complementar, consiste a regra*/
			if	(ie_complementar_w = 'N') then
				if	((ie_consiste_periodo_pag_w = 'S') or
					 (nr_seq_lote_pag_w is not null)) then
					ie_gerar_w := pls_obter_se_evento_inside_per(nr_seq_periodo_pag_w, nr_seq_evento_w, nr_seq_prestador_w, dt_competencia_w, nvl(dt_movimento_w,trunc(dt_competencia_w,'dd')));
				end if;
			else	-- Edgar 26/05/2014, OS 740405, se for lote complementar, verificar se este evento est� cadastrado no mesmo
				ie_gerar_w := pls_obter_se_entra_pgto_compl(nr_seq_lote_pag_w, nr_seq_evento_w, nr_seq_prestador_w, nr_seq_tipo_prestador_w, nr_seq_classificacao_w);
			end if;

			nr_seq_prestador_pgto_w := null;
			nr_seq_prestador_orig_w := nr_seq_prestador_w;

			if	(ie_aplicar_regra_prest_w = 'S') then
			
				pls_obter_prest_pgto_prof( 	nr_seq_prestador_w, null, null,
								null, nm_usuario_p, null,
								sysdate, nr_seq_prestador_pgto_w, nr_seq_regra_w);

				if	(nr_seq_prestador_pgto_w is not null) then

					nr_seq_prestador_w := nr_seq_prestador_pgto_w;
				end if;
				
			end if;

			if	(nvl( ie_gerar_w, 'N') <> 'N') then
				if	(ie_forma_incidencia_w = 'S') then /* Verifica se j� existe uma ocorr�ncia dentro da semana */
					-- � feito um IN com o prestador do cursor e o prestador que retornou na regra de convers�o pois o lan�amento
					-- pode ter sido feito antes de aplicar a regra ou depois...
					select	count(1)
					into	qt_registro_w
					from	pls_evento_movimento a
					where	a.nr_seq_evento		= nr_seq_evento_w
					and	a.nr_seq_prestador in (nr_seq_prestador_w, nr_seq_prestador_orig_w)
					and	a.dt_movimento	between (trunc(dt_competencia_w,'dd') - to_number(to_char(dt_competencia_w,'d')) + 1) and
									(trunc(dt_competencia_w,'dd') - to_number(to_char(dt_competencia_w,'d')) + 8)
					and	a.ie_cancelamento is null
					and	rownum <= 1;

					if	(qt_registro_w > 0) then
						ie_gerar_w	:= 'N';
					end if;
				elsif	(ie_forma_incidencia_w = 'M') then /* Verifica se j� existe uma ocorr�ncia dentro do m�s */
					-- � feito um IN com o prestador do cursor e o prestador que retornou na regra de convers�o pois o lan�amento
					-- pode ter sido feito antes de aplicar a regra ou depois...
					select	count(1)
					into	qt_registro_w
					from	pls_evento_movimento a
					where	a.nr_seq_evento		= nr_seq_evento_w
					and	a.nr_seq_prestador in (nr_seq_prestador_w, nr_seq_prestador_orig_w)
					and	a.dt_movimento between trunc(dt_competencia_w,'month') and last_day(dt_competencia_w)
					and	((ie_incidencia_lanc_prog_w = 'S' and nr_seq_regra_fixo = nr_seq_regra_fixo_w) or (ie_incidencia_lanc_prog_w = 'N'))
					and	a.ie_cancelamento is null
					and	rownum <= 1;

					if	(qt_registro_w > 0) then
						ie_gerar_w	:= 'N';
					end if;
				elsif	(ie_forma_incidencia_w = 'A') then /* Verifica se j� existe uma ocorr�ncia dentro do Ano */
					-- � feito um IN com o prestador do cursor e o prestador que retornou na regra de convers�o pois o lan�amento
					-- pode ter sido feito antes de aplicar a regra ou depois...
					select	count(1)
					into	qt_registro_w
					from	pls_evento_movimento a
					where	a.nr_seq_evento		= nr_seq_evento_w
					and	a.nr_seq_prestador in (nr_seq_prestador_w, nr_seq_prestador_orig_w)
					and	a.dt_movimento between  trunc(dt_competencia_w,'yy') and (last_day(add_months(trunc(dt_competencia_w,'year'),11)))
					and	a.ie_cancelamento is null
					and	rownum <= 1;

					if	(qt_registro_w > 0) then
						ie_gerar_w	:= 'N';
					end if;
				elsif	(ie_forma_incidencia_w = 'U') then /* Verifica se j� existe uma ocorr�ncia �nica */
					-- � feito um IN com o prestador do cursor e o prestador que retornou na regra de convers�o pois o lan�amento
					-- pode ter sido feito antes de aplicar a regra ou depois...
					select	count(1)
					into	qt_registro_w
					from	pls_evento_movimento a
					where	a.nr_seq_evento		= nr_seq_evento_w
					and	a.nr_seq_prestador in (nr_seq_prestador_w, nr_seq_prestador_orig_w)
					and	a.nr_seq_regra_fixo	= nr_seq_regra_fixo_w
					and	a.ie_cancelamento is null
					and	rownum <= 1;

					select 	count(1)
					into	qt_excessoes_w
					from	pls_evento_regra_fixo_exc b,
						pls_evento_regra_fixo a
					where	b.nr_seq_regra_fixo 	= a.nr_sequencia
					and	b.nr_seq_prestador 	= nr_seq_prestador_w
					and	a.nr_sequencia		= nr_seq_regra_fixo_w
					and	rownum <= 1;

					if	(qt_registro_w > 0) or
						(qt_excessoes_w > 0)  then
						ie_gerar_w	:= 'N';
					end if;
				end if;
			end if;

			if	(ie_gerar_w = 'S') then /* Gerar ocorr�ncias financeira */

				if	(cd_condicao_pagamento_w is not null) or
					(qt_dia_venc_w is not null) then
					/* Gerar por condii��o de pagamento */
					pls_gerar_movto_cond_pgto(nr_seq_lote_evento_p, nr_seq_regra_fixo_w, nr_seq_prestador_w, cd_estabelecimento_p, nm_usuario_p);
				else
					insert into pls_evento_movimento(
						nr_sequencia, dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, dt_movimento,
						nr_seq_evento, nr_seq_lote, nr_seq_prestador,
						vl_movimento, nr_seq_regra_fixo, ds_observacao,
						nr_seq_periodo, nr_seq_classe_tit_rec, ie_titulo_pagar,
						cd_pf_titulo_pagar, cd_cgc_titulo_pagar, nr_seq_trans_fin_baixa,
						nr_seq_trans_fin_contab, cd_centro_custo, ie_forma_pagto
					) values (
						pls_evento_movimento_seq.nextval, sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, nvl(dt_movimento_w,trunc(dt_competencia_w,'dd')),
						nr_seq_evento_w, nr_seq_lote_evento_p, nr_seq_prestador_w,
						vl_regra_w, nr_seq_regra_fixo_w, ds_observacao_w,
						nr_seq_periodo_w, nr_seq_classe_tit_rec_w, ie_titulo_pagar_w,
						cd_pf_titulo_pagar_w, cd_cgc_titulo_pagar_w, nr_seq_trans_fin_baixa_w,
						nr_seq_trans_fin_contab_w, cd_centro_custo_w, 'P'
					) returning nr_sequencia into nr_seq_evento_movto_w;
					
					/* Obtem a conta cont�bil comforme regra OPS - Crit�rios Cont�beis item Regra cont�bil de eventos financeiros (pagamento produ��o)*/
					pls_obter_conta_contab_eve_fin(nr_seq_evento_movto_w, cd_conta_contabil_w);
					
					update	pls_evento_movimento
					set	cd_conta_contabil	= cd_conta_contabil_w
					where	nr_sequencia		= nr_seq_evento_movto_w;
				end if;
			end if;
			-- volta ao prestador original
			nr_seq_prestador_w := nr_seq_prestador_orig_w;
			end;
		end loop;
		close C01;
		end;
	end loop;
	close C03;
	
	-- s� vai atr�z de valores pendentes em lotes de pagamento caso o cliente n�o utilize lote de apropria��o
	if	(ie_gerar_evento_prox_pag_w = 'N') then
	
		-- Se o sistema n�o est� configurado para gerar as apropria��es com o evento original
		if	(ie_evento_orig_prox_pagto_w = 'N') then
		
			-- Define todos os eventos apropriados como o evento vinculado no o evento
			nr_seq_evento_w := nvl(nr_seq_evento_prox_pgto_w, nr_seq_evento_w);
			
			select 	nvl(max(ie_consiste_periodo_pag),'S')
			into 	ie_consiste_periodo_pag_w
			from	pls_evento
			where	nr_sequencia = nr_seq_evento_w;
			
			open C02;
			loop
			fetch C02 into
				vl_regra_w,
				nr_seq_prestador_w,
				nr_seq_vencimento_w,
				nr_seq_lote_w;
			exit when C02%notfound;
				ie_gerar_w	:= 'S';
				
				select	max(a.nr_seq_classificacao),
					max(a.nr_seq_tipo_prestador)
				into	nr_seq_classificacao_w,
					nr_seq_tipo_prestador_w
				from	pls_prestador a
				where	nr_sequencia	= nr_seq_prestador_w;

				if	(ie_complementar_w = 'S') then
					ie_gerar_w := pls_obter_se_entra_pgto_compl(nr_seq_lote_pag_w, nr_seq_evento_w, nr_seq_prestador_w, nr_seq_tipo_prestador_w, nr_seq_classificacao_w);
				else
					if	((ie_consiste_periodo_pag_w = 'S') or
						 (nr_seq_lote_pag_w is not null)) then
						ie_gerar_w := pls_obter_se_evento_inside_per(nr_seq_periodo_pag_w, nr_seq_evento_w, nr_seq_prestador_w, dt_competencia_w, nvl(dt_movimento_w,trunc(dt_competencia_w,'dd')));
					end if;
				end if;
				
				if	(ie_gerar_w = 'S') then
					/* Tratamento para que seja informado um evento de pr�ximo pagamento OPS - Gest�o de Operadoras pasta Par�metros da OPS/Pagamento Produ��o */
					if	(nr_seq_evento_w is null) then
						ds_erro_w	:= 'N�o foi cadastrado o evento para pr�ximo pagamento. Verificar na fun��o OPS - Gest�o de Operadoras, pasta Par�metros ' || chr(13) ||
									' da OPS -> Pagamento Produ��o.';
						wheb_mensagem_pck.exibir_mensagem_abort(191184,'DS_ERRO=' || ds_erro_w);
					end if;

					/* Tratar se deve considerar mesmo per�odo do lote de pagamento do vencimento que gerou este movimento OPS - Prestadores pasta Dados Complementares/Dados para pagamento */
					select	nvl(max(a.ie_periodo_aprop_neg),'N')
					into	ie_periodo_aprop_neg_w
					from	pls_prestador_pagto	a
					where	a.nr_seq_prestador	= nr_seq_prestador_w;

					if	(ie_periodo_aprop_neg_w = 'S') then
						select	max(a.nr_seq_periodo)
						into	nr_seq_periodo_w
						from	pls_lote_pagamento	a
						where	a.nr_sequencia	= nr_seq_lote_w;
					else
						nr_seq_periodo_w	:= null;
					end if;

					nr_seq_prestador_regra_w	:= null;
					nr_seq_prestador_orig_w		:= nr_seq_prestador_w;
					if	(vl_regra_w < 0) then
						pls_obter_crit_receb_prest( nr_seq_prestador_w, sysdate, nm_usuario_p, nr_seq_regra_w, nr_seq_prestador_regra_w);

						if	(nr_seq_prestador_regra_w is not null) then
							nr_seq_prestador_w	:= nr_seq_prestador_regra_w;
						end if;
					end if;

					select	nvl(sum(qt_evento),0)
					into	qt_apropriacao_w
					from	(select	count(1) qt_evento
						from	pls_evento_movimento		b,
							pls_lote_evento			x
						where	x.nr_sequencia			= b.nr_seq_lote
						and	b.nr_seq_prestador		= nr_seq_prestador_w
						and	b.nr_seq_evento			= nr_seq_evento_w
						and	x.nr_seq_lote_pgto_apropr	is not null
						and	b.nr_seq_lote_pgto		is null
						and	b.ie_cancelamento 		is null
						union all
						select	count(1) qt_evento
						from	pls_evento_movimento		b,
							pls_lote_evento			x
						where	x.nr_sequencia			= b.nr_seq_lote
						and	b.nr_seq_prestador		= nr_seq_prestador_orig_w
						and	b.nr_seq_evento			= nr_seq_evento_w
						and	x.nr_seq_lote_pgto_apropr	is not null
						and	b.nr_seq_lote_pgto		is null
						and	b.ie_cancelamento 		is null);

					if	(qt_apropriacao_w = 0) then
						insert into pls_evento_movimento(
							nr_sequencia, dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, dt_movimento,
							nr_seq_evento, nr_seq_lote, nr_seq_prestador,
							vl_movimento, ie_forma_pagto, nr_seq_periodo,
							ds_observacao
						) values (
							pls_evento_movimento_seq.nextval, sysdate, nm_usuario_p,
							sysdate, nm_usuario_p, nvl(dt_movimento_w,trunc(dt_competencia_w,'dd')),
							nr_seq_evento_w, nr_seq_lote_evento_p, nr_seq_prestador_w,
							vl_regra_w, 'P', nr_seq_periodo_w,
							'Evento gerado de pagamento de valor negativo no lote:' || nr_seq_lote_w
						) returning nr_sequencia into nr_seq_evento_movto_w;

						/* Tratamento para n�o apropriar duas vezes */
						update	pls_pag_prest_vencimento
						set	ie_proximo_pgto		= 'N',
							nr_seq_evento_movto	= nr_seq_evento_movto_w
						where	nr_sequencia		= nr_seq_vencimento_w;
					end if;

					/* Obtem a conta cont�bil comforme regra OPS - Crit�rios Cont�beis item Regra cont�bil de eventos financeiros (pagamento produ��o)*/
					pls_obter_conta_contab_eve_fin(nr_seq_evento_movto_w, cd_conta_contabil_w);

					update	pls_evento_movimento
					set	cd_conta_contabil	= cd_conta_contabil_w
					where	nr_sequencia		= nr_seq_evento_movto_w;
				end if;
			end loop;
			close C02;
		elsif	(ie_evento_orig_prox_pagto_w = 'S') then /* Apropiar valores que estejam setados como tal */
			open C04;
			loop
			fetch C04 into
				vl_regra_w,
				nr_seq_prestador_w,
				nr_seq_vencimento_w,
				nr_seq_lote_w,
				nr_seq_evento_w,
				nr_seq_venc_valor_w;
			exit when C04%notfound;
				begin
				ie_gerar_w	:= 'S';

				if	(ie_consiste_periodo_pag_w = 'S') or
					(nr_seq_lote_pag_w is not null) then
					ie_gerar_w := pls_obter_se_evento_inside_per(	nr_seq_periodo_pag_w, nr_seq_evento_w, nr_seq_prestador_w, 
											dt_competencia_w, nvl(dt_movimento_w,trunc(dt_competencia_w,'dd')));
				end if;

				select	max(a.nr_seq_classificacao),
					max(a.nr_seq_tipo_prestador)
				into	nr_seq_classificacao_w,
					nr_seq_tipo_prestador_w
				from	pls_prestador a
				where	nr_sequencia	= nr_seq_prestador_w;

				if	(ie_gerar_w = 'S') and
					(ie_complementar_w = 'S') then
					ie_gerar_w := pls_obter_se_entra_pgto_compl(	nr_seq_lote_pag_w, nr_seq_evento_w, nr_seq_prestador_w, 
											nr_seq_tipo_prestador_w, nr_seq_classificacao_w);
				end if;

				if	(nvl(ie_gerar_w, 'N') <> 'N') then
					/* Tratar se deve considerar mesmo per�odo do lote de pagamento do vencimento que gerou este movimento OPS - Prestadores pasta Dados Complementares/Dados para pagamento */
					select	nvl(max(a.ie_periodo_aprop_neg), 'N')
					into	ie_periodo_aprop_neg_w
					from	pls_prestador_pagto	a
					where	a.nr_seq_prestador	= nr_seq_prestador_w;

					if	(ie_periodo_aprop_neg_w = 'S') then

						select	max(a.nr_seq_periodo)
						into	nr_seq_periodo_w
						from	pls_lote_pagamento	a
						where	a.nr_sequencia	= nr_seq_lote_w;
					else
						nr_seq_periodo_w	:= null;
					end if;

					nr_seq_prestador_regra_w	:= null;
					nr_seq_prestador_orig_w		:= nr_seq_prestador_w;

					if	(vl_regra_w < 0) then
						pls_obter_crit_receb_prest( nr_seq_prestador_w, sysdate, nm_usuario_p, nr_seq_regra_w, nr_seq_prestador_regra_w);

						if	(nr_seq_prestador_regra_w is not null) then
							nr_seq_prestador_w	:= nr_seq_prestador_regra_w;
						end if;
					end if;

					select	nvl(sum(qt_evento),0)
					into	qt_apropriacao_w
					from	(select	count(1) qt_evento
						from	pls_evento_movimento	b,
							pls_lote_evento		x
						where	x.nr_sequencia		= b.nr_seq_lote
						and	b.nr_seq_prestador	= nr_seq_prestador_w
						and	b.nr_seq_evento		= nr_seq_evento_w
						and	x.nr_seq_lote_pgto_apropr	is not null
						and	b.nr_seq_lote_pgto		is null
						and	b.ie_cancelamento		is null
						union all
						select	count(1) qt_evento
						from	pls_evento_movimento	b,
							pls_lote_evento		x
						where	x.nr_sequencia		= b.nr_seq_lote
						and	b.nr_seq_prestador	= nr_seq_prestador_orig_w
						and	b.nr_seq_evento		= nr_seq_evento_w
						and	x.nr_seq_lote_pgto_apropr	is not null
						and	b.nr_seq_lote_pgto		is null
						and	b.ie_cancelamento		is null);

					if	(qt_apropriacao_w = 0) then

						insert into pls_evento_movimento(
							nr_sequencia, dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, dt_movimento,
							nr_seq_evento, nr_seq_lote, nr_seq_prestador,
							vl_movimento, ie_forma_pagto, nr_seq_periodo,
							ds_observacao
						) values (
							pls_evento_movimento_seq.nextval, sysdate, nm_usuario_p,
							sysdate, nm_usuario_p, nvl(dt_movimento_w,trunc(dt_competencia_w,'dd')),
							nr_seq_evento_w, nr_seq_lote_evento_p, nr_seq_prestador_w,
							vl_regra_w, 'P', nr_seq_periodo_w,
							'Evento gerado de pagamento de valor negativo no lote:' || nr_seq_lote_w
						) returning nr_sequencia into nr_seq_evento_movto_w;

						/* Obtem a conta cont�bil comforme regra OPS - Crit�rios Cont�beis item Regra cont�bil de eventos financeiros (pagamento produ��o)*/
						pls_obter_conta_contab_eve_fin(nr_seq_evento_movto_w, cd_conta_contabil_w);

						update	pls_evento_movimento
						set	cd_conta_contabil	= cd_conta_contabil_w
						where	nr_sequencia		= nr_seq_evento_movto_w;

						update	pls_pag_prest_venc_valor
						set	nr_seq_evento_movto	= nr_seq_evento_movto_w
						where	nr_sequencia		= nr_seq_venc_valor_w
						and	nr_seq_evento_movto is null;
					end if;
				end if;
				end;
			end loop;
			close C04;

			/* Setar como apropriado se todos os valores tiverem gerado movimenta��o */
			open C05;
			loop
			fetch C05 into
				nr_seq_vencimento_w;
			exit when C05%notfound;
				begin
				select	count(1),
					count(a.nr_seq_evento_movto)
				into	qt_total_aprop_w,
					qt_aprop_w
				from	pls_pag_prest_venc_valor	a
				where	a.nr_seq_vencimento	= nr_seq_vencimento_w
				and	a.ie_tipo_valor		= 'PP';

				if	(qt_total_aprop_w = qt_aprop_w) then
					update	pls_pag_prest_vencimento
					set	ie_proximo_pgto	= 'N'
					where	nr_sequencia	= nr_seq_vencimento_w;
				end if;
				end;
			end loop;
			close C05;
		end if;
	end if;
elsif	(ie_opcao_p = 'D') then

	pls_desfazer_lanc_programados(nr_seq_lote_evento_p, null, nm_usuario_p, ie_commit_p);
	
elsif	(ie_opcao_p = 'DC') then

	select	max(b.nr_seq_lote_pgto)
	into	nr_seq_lote_pgto_w
	from	pls_lote_evento a,
		pls_evento_movimento b
	where	a.nr_sequencia = b.nr_seq_lote
	and	a.nr_sequencia	= nr_seq_lote_evento_p
	and	a.nr_seq_franq_pag is null
	and	b.ie_cancelamento is null;

	if	(nr_seq_lote_pgto_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(191185,'NR_SEQ_LOTE_PGTO=' || nr_seq_lote_pgto_w);
	end if;

	update	pls_pag_prest_vencimento	a
	set	a.nr_seq_evento_movto		= null,
		a.ie_proximo_pgto		= 'S'
	where	exists	(select	1
			from	pls_evento_movimento	x
			where	a.nr_seq_evento_movto	= x.nr_sequencia
			and	x.nr_seq_lote		= nr_seq_lote_evento_p);

	update	pls_pag_prest_vencimento	a
	set	a.nr_seq_evento_movto		= null,
		a.ie_proximo_pgto		= 'S'
	where	exists	(select	1
			from	pls_evento_movimento		x,
				pls_pag_prest_venc_valor	y
			where	x.nr_sequencia			= y.nr_seq_evento_movto
			and	a.nr_sequencia			= y.nr_seq_vencimento
			and	x.nr_seq_lote			= nr_seq_lote_evento_p);

	update	pls_pag_prest_venc_valor	a
	set	a.nr_seq_evento_movto		= null
	where	exists	(select	1
			from	pls_evento_movimento	x
			where	a.nr_seq_evento_movto	= x.nr_sequencia
			and	x.nr_seq_lote		= nr_seq_lote_evento_p);

	delete	pls_evento_movimento
	where	nr_seq_lote	= nr_seq_lote_evento_p;
end if;

if	(nvl(ie_commit_p, 'S') = 'S') then
	commit;
end if;

end pls_gerar_evento_movto_fixo;
/
