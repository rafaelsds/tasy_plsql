create or replace
procedure	sup_atualizar_cad_material(
			cd_material_p			number,
			cd_classe_mat_nova_p		number,
			qt_dia_interv_ressup_p		varchar2,
			qt_dia_ressup_forn_p		varchar2,
			qt_dia_estoque_minimo_p		varchar2,
			ie_atualiza_dados_estab_p		varchar2,
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2) is

cd_classe_ant_w			number(5);
ds_historico_w			varchar2(1000);
qt_dia_interv_ressup_w		number(3);
qt_dia_ressup_forn_w		number(3);
qt_dia_estoque_minimo_w		number(3);

begin

if	(nvl(cd_classe_mat_nova_p,0) <> 0) then
	begin
	select	cd_classe_material
	into	cd_classe_ant_w
	from	estrutura_material_v
	where	cd_material = cd_material_p;

	/* Altera��o no cadastro */
	ds_historico_w	:=	WHEB_MENSAGEM_PCK.get_texto(818906) || ': MATERIAL ' || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(182370) || ' CD_CLASSE_MATERIAL ' || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(804610) || ' ' || cd_classe_ant_w || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(182372) || ' ' || cd_classe_mat_nova_p;

	insert into material_historico(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_titulo,
		ds_historico,
		cd_material,
		cd_estabelecimento,
		ie_tipo) values (
			material_historico_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			WHEB_MENSAGEM_PCK.get_texto(818907), /*'Altera��o do cadastro do material'*/
			ds_historico_w,
			cd_material_p,
			cd_estabelecimento_p,
			'S');

	update	material
	set	cd_classe_material = cd_classe_mat_nova_p
	where	cd_material = cd_material_p;
	end;
end if;

/* Atualiza os dados do material estabelecimento, parte inferior do cadastro */
if	(nvl(ie_atualiza_dados_estab_p, 'N') = 'S') then
	begin

	begin
	select	qt_dia_interv_ressup,
		qt_dia_ressup_forn,
		qt_dia_estoque_minimo
	into	qt_dia_interv_ressup_w,
		qt_dia_ressup_forn_w,
		qt_dia_estoque_minimo_w
	from	material_estab
	where	cd_estabelecimento = cd_estabelecimento_p
	and	cd_material = cd_material_p;
	exception when others then
		qt_dia_interv_ressup_w	:= '';
		qt_dia_ressup_forn_w	:= '';
		qt_dia_estoque_minimo_w 	:= '';
	end;

	update	material_estab
	set	qt_dia_interv_ressup 	= nvl(qt_dia_interv_ressup_p, qt_dia_interv_ressup),
		qt_dia_ressup_forn		= nvl(qt_dia_ressup_forn_p, qt_dia_ressup_forn),
		qt_dia_estoque_minimo	= nvl(qt_dia_estoque_minimo_p, qt_dia_estoque_minimo),
		nm_usuario 		= nm_usuario_p,
		dt_atualizacao 		= sysdate
	where	cd_estabelecimento 	= cd_estabelecimento_p
	and	cd_material 		= cd_material_p;

	/* Altera��o no cadastro */
	ds_historico_w	:= substr(WHEB_MENSAGEM_PCK.get_texto(818906) || ': MATERIAL_ESTAB ' || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(182370) || ' QT_DIA_INTERV_RESSUP ' || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(804610) || ' ' || qt_dia_interv_ressup_w || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(182372) || ' ' || nvl(qt_dia_interv_ressup_p, qt_dia_interv_ressup_w) || chr(13) || chr(13) ||

				WHEB_MENSAGEM_PCK.get_texto(182370) || ' QT_DIA_RESSUP_FORN ' || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(804610) || ' ' || qt_dia_ressup_forn_w || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(182372) || ' ' || nvl(qt_dia_ressup_forn_p, qt_dia_ressup_forn_w) || chr(13) || chr(13) ||

				WHEB_MENSAGEM_PCK.get_texto(182370) || ' QT_DIA_ESTOQUE_MINIMO ' || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(804610) || ' ' || qt_dia_estoque_minimo_w || chr(13) ||
				WHEB_MENSAGEM_PCK.get_texto(182372) || ' ' || nvl(qt_dia_estoque_minimo_p, qt_dia_estoque_minimo_w),1,1000);

	insert into material_historico(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ds_titulo,
			ds_historico,
			cd_material,
			cd_estabelecimento,
			ie_tipo) 
	values (	material_historico_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			WHEB_MENSAGEM_PCK.get_texto(818908), /*'Altera��o do cadastro do material(Estabelecimento)'*/
			ds_historico_w,
			cd_material_p,
			cd_estabelecimento_p,
			'S');
	end;
end if;


commit;

end sup_atualizar_cad_material;
/