create or replace
procedure ptu_val_contbenef_v50_ws(	nr_seq_pedido_cont_p	ptu_pedido_contagem_benef.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					nr_seq_ret_cont_p   out ptu_resp_contagem_benef.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a valida��o do arquivo de  00430 - Requisi��o Contagem Benef.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

ptu_val_scs_ws_pck.ptu_processa_resp_cont_benef(nr_seq_pedido_cont_p, nm_usuario_p, nr_seq_ret_cont_p);

end ptu_val_contbenef_v50_ws;
/