create or replace
procedure gerar_aprazamento_interv(
					ie_tipo_item_p		varchar2,
					nr_prescricao_p		number,
					nr_seq_item_p		number,
					ds_horarios_p		varchar2,
					nr_seq_motivo_p		number,
					ds_justificativa_p	varchar2,
					nm_usuario_p		varchar2,
					nr_seq_assinatura_p	number default null) is

ie_gerar_lote_w			varchar2(1);					
dt_emissao_farmacia_w	date;					
cd_motivo_baixa_w		number(15);					

nr_atendimento_w		number(10,0);
ds_horarios_w			varchar2(2000);
ds_horarios_padr_w		varchar2(2000);
ds_hora_w			varchar2(2000);
dt_liberacao_w			Date;
dt_horario_w			Date;	
k				Integer;
y				Integer;
nr_sequencia_w			Number(10);
nr_seq_prescr_w			Number(6);
qt_dose_w			Number(18,6);
qt_total_dispensar_w		Number(18,6);
cd_material_w			Number(6);	
dt_primeiro_horario_w		Date;
ie_agrupador_w			Number(2);
cd_unidade_medida_w		Varchar2(30);
ie_local_estoque_w		Varchar2(30);
cd_unid_med_dose_w		Varchar2(30);
cd_intervalo_w			varchar2(7);
ie_prescricao_dieta_w		varchar2(1);
nr_ocorrencia_w			number(15,4);
ie_esquema_alternado_w		varchar2(1);
nr_seq_solucao_w		number(6);

ie_agrupador_dil_w		Number(2);
nr_sequencia_dil_w		Number(10);
qt_dose_dil_w			Number(18,6);
qt_total_disp_dil_w		Number(18,6);
nr_ocorrencia_dil_w		number(15,4);
cd_material_dil_w		Number(6);
hr_prim_horario_w		varchar2(5);
ie_urgente_w			varchar2(1);
qt_conversao_w			Number(18,6);
qt_dose_especial_w		Number(18,6);
hr_dose_especial_w		varchar2(5);

/* Rafael em 12/09/06 Funcionalidades da Administracao da Prescricao (ATEPAC_PG)  */
nr_seq_procedimento_w		number(6,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
nr_seq_proc_interno_w		number(10,0);
nr_ocor_proc_w			number(15,4);
ie_proc_urgente_w		varchar2(1);
dt_prev_execucao_w		date;
ds_hora_proc_w			varchar2(2000);
dt_horario_proc_w		date;
ds_horarios_proc_w		varchar2(2000);
ds_horarios_padrao_proc_w	varchar2(2000);
cd_material_exame_w		varchar2(20);

nr_seq_recomendacao_w		number(6,0);
cd_recomendacao_w		number(10,0);
ds_hora_rec_w			varchar(2000);
dt_horario_rec_w		date;
nr_seq_classif_rec_w		number(10,0);
ds_horarios_rec_w		varchar2(2000);
ds_horarios_padrao_rec_w	varchar2(2000);
/* Fim Rafael em 12/09/06 */

/* Rafael em 07/10/2006 Funcionalidades da Administracao da Prescricao -> Identificar horarios especiais (SN, ACM e sem Horario) */
ie_se_necessario_w		varchar2(1);
ie_acm_w			varchar2(1);
ie_horario_especial_w		varchar2(1) := 'N';
qt_dieta_w			number(10,0);
cd_setor_atendimento_w		number(5);
cd_estabelecimento_w		number(4);
dt_prescricao_w			Date;
dt_prescricao_ww		Date;
qT_dia_adic_w			Number(10) := 0;
qt_registro_w			Number(10);
dt_inicio_prescr_w		Date;

nr_seq_procedimento_novo_w	number(6);
nr_seq_exame_w			number(10);
ie_status_atend_w		number(2);
ie_status_execucao_w		varchar2(3);
cd_setor_atendimento_proc_w	number(5);
cd_setor_coleta_w		number(5);
cd_setor_entrega_w       	number(5);
cd_setor_exec_fim_w      	number(5);
cd_setor_exec_inic_w      	number(5);
nr_seq_lab_w			varchar2(20);
ie_gerar_proc_intervalo_w	varchar2(1);
ie_suspenso_w			varchar2(1);
ds_observacao_w			varchar2(2000);
ds_dado_clinico_w		varchar2(2000);
ds_material_especial_w		varchar2(255);
ie_controlado_w			varchar2(1);

nr_seq_prescr_hor_w		number(6);
dt_horario_proc_prev_w		date;
ie_proc_atual_w			varchar2(1);
qt_min_agora_w			number(15);
qt_min_especial_w		number(15);
ie_classif_urgente_w		varchar2(3);
dt_limite_agora_w		date;
dt_limite_especial_w		date;
nr_seq_classif_w		number(10);
dt_liberacao_farmacia_w		date;
ie_momento_lote_w		varchar2(15) := 'E';
ajustar_disp_hor_farm_w		varchar2(1) := 'N';
ie_padronizado_w		varchar2(1);
ie_classif_nao_padrao_w		varchar2(15);
ie_regra_disp_w			varchar2(1);
ie_lib_farm_w			varchar2(1);
nr_agrupamento_w		number(7,1);
cd_protocolo_w			number(10,0);
ie_usuario_medico_w		varchar2(1);
ie_adep_w			varchar2(15);
ie_rep_adep_w			varchar2(1);
varPrescrNaoMedicaADEP_w	varchar2(15);
ie_gerar_necessidade_disp_w	varchar2(15);
varie_cursor_w			varchar2(1) := 'N';
cd_local_estoque_w		number(4);
ie_acm_sn_w			varchar2(1);
ie_motivo_prescricao_w		varchar2(5);

nr_seq_lote_w				number(10); 
ie_quimio_w					varchar2(1);	
ie_gerar_nec_disp_kit_w		varchar2(1);
qt_dispensar_hor_w			prescr_mat_hor.qt_dispensar_hor%type;

cd_motivo_baixa_param_w	number(15);



cursor C01 is
select	nr_atendimento,
	ie_agrupador,
	nr_sequencia,
	qt_dose,
	qt_total_dispensar,
	cd_unidade_medida,
	cd_unidade_medida_dose,
	nr_ocorrencia,
	cd_material,
	ds_horarios,
	padroniza_horario_prescr(ds_horarios_p, decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,b.hr_prim_horario),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss'))),
	hr_prim_horario,
	nvl(ie_urgencia,'N'),
	qt_dose_especial,
	hr_dose_especial,
	nvl(ie_se_necessario,'N'),
	nvl(ie_acm,'N'),
	substr(Obter_se_medic_controlado(b.cd_material),1,1),
	substr(obter_se_material_padronizado(a.cd_estabelecimento,b.cd_material),1,1),
	decode(nvl(b.ie_regra_disp,'S'),'N','N','S'),
	b.nr_agrupamento,
	b.cd_protocolo,
	b.cd_local_estoque	
from	prescr_material b,
	prescr_medica a
where	a.nr_prescricao	= b.nr_prescricao
and	a.nr_prescricao	= nr_prescricao_p
and	b.nr_sequencia	= nr_seq_item_p
and	ie_agrupador	= 1
and	ie_tipo_item_p	in ('M', 'IAH');

cursor C05 is
select	nr_atendimento,
		ie_agrupador,
		nr_sequencia,
		qt_dose,
		qt_total_dispensar,
		cd_unidade_medida,
		cd_unidade_medida_dose,
		nr_ocorrencia,
		cd_material,
		ds_horarios,
		padroniza_horario_prescr(ds_horarios_p, decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,b.hr_prim_horario),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss'))),
		hr_prim_horario,
		nvl(ie_urgencia,'N'),
		qt_dose_especial,
		hr_dose_especial,
		nvl(ie_se_necessario,'N'),
		nvl(ie_acm,'N'),
		substr(Obter_se_medic_controlado(b.cd_material),1,1),
		substr(obter_se_material_padronizado(a.cd_estabelecimento,b.cd_material),1,1),
		decode(nvl(b.ie_regra_disp,'S'),'N','N','S'),
		b.nr_agrupamento,
		b.cd_protocolo,
		b.cd_local_estoque
from	prescr_material b,
		prescr_medica a
where	a.nr_prescricao	= b.nr_prescricao
and		a.nr_prescricao	= nr_prescricao_p
and		b.nr_sequencia	= nr_seq_item_p
and		ie_agrupador	= 5
and		ie_tipo_item_p	= 'IA';

cursor C12 is
select	nr_atendimento,
	ie_agrupador,
	nr_sequencia,
	qt_dose,
	qt_total_dispensar,
	cd_unidade_medida,
	cd_unidade_medida_dose,
	nr_ocorrencia,
	cd_material,
	ds_horarios,
	padroniza_horario_prescr(ds_horarios_p, decode(substr(Obter_Se_horario_hoje(a.dt_prescricao,a.dt_primeiro_horario,b.hr_prim_horario),1,1),'N','01/01/2000 23:59:59', to_char(nvl(a.dt_primeiro_horario,a.dt_prescricao),'dd/mm/yyyy hh24:mi:ss'))),
	hr_prim_horario,
	nvl(ie_urgencia,'N'),
	qt_dose_especial,
	hr_dose_especial,
	nvl(ie_se_necessario,'N'),
	nvl(ie_acm,'N'),
	substr(Obter_se_medic_controlado(b.cd_material),1,1),
	substr(obter_se_material_padronizado(a.cd_estabelecimento,b.cd_material),1,1),
	decode(nvl(b.ie_regra_disp,'S'),'N','N','S'),
	b.nr_agrupamento,
	b.cd_protocolo,
	b.cd_local_estoque
from	prescr_material b,
	prescr_medica a
where	a.nr_prescricao	= b.nr_prescricao
and	a.nr_prescricao	= nr_prescricao_p
and	b.nr_sequencia	= nr_seq_item_p
and	ie_agrupador	= 12
and	ie_tipo_item_p	= 'S';

Cursor C016 is
	select	nr_sequencia,
		ie_classif_urgente,
		ie_controlado,
		ie_padronizado
	from	classif_lote_disp_far
	where	cd_estabelecimento = cd_estabelecimento_w
	and	ie_situacao = 'A'
	order by ie_classif_urgente,
		ie_controlado desc,
		ie_padronizado desc;

Cursor C20 is
	select	ie_momento_lote
	from	regra_disp_lote_farm
	where	((ie_motivo_prescricao = ie_motivo_prescricao_w) or (ie_motivo_prescricao is null))
	and		sysdate between to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(nvl(dt_hora_inicio,sysdate),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') and
						to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(nvl(dt_hora_fim,sysdate),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
	and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(sysdate) between ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_inicio_vigencia) and ESTABLISHMENT_TIMEZONE_UTILS.StartOfDay(nvl(dt_fim_vigencia,sysdate))
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_w)	= cd_setor_atendimento_w
	and	nvl(ie_situacao,'A') = 'A'	
	and 	((ie_quimio_w <> 'S' and nvl(ie_quimioterapicas,'N') <> 'S') or (ie_quimio_w = 'S'))
	order by decode(ie_quimioterapicas,'S','Z',decode(ie_quimioterapicas,'N','C','B')),
	nvl(cd_setor_atendimento,0), NVL(ie_motivo_prescricao,0);
		
	
	
Cursor C02 is --Atualizar numero do lote no evento.
	select	nr_sequencia,
			nvl(nr_seq_lote,0)			
	from	prescr_mat_hor
	where	nr_prescricao = nr_prescricao_p
	and 	ie_tipo_item_p in ('M','S','IA', 'IAH');			
	
begin

select 	nvl(max('S'),'N') 
into	ie_quimio_w
from	paciente_atendimento
where	nr_prescricao = nr_prescricao_p;

select	count(*)
into	qt_registro_w
from	prescr_mat_hor
where	nr_prescricao			= nr_prescricao_p
and	nr_seq_material 		= nr_seq_item_p
and	nvl(ie_horario_especial,'N')	= 'N'
and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

if	(qt_registro_w = 0) and
	(nr_prescricao_p is not null) and
	(nr_seq_item_p is not null) and
	(ds_horarios_p is not null) then

	select	dt_liberacao,
		dt_primeiro_horario,
		cd_setor_atendimento,
		nvl(cd_estabelecimento,1),
		trunc(dt_prescricao,'mi'),
		dt_inicio_prescr,
		dt_liberacao_farmacia,
		nvl(ie_lib_farm,'N'),
		nvl(ie_adep,obter_se_setor_adep(cd_setor_atendimento)),
		ie_motivo_prescricao
	into	dt_liberacao_w,
		dt_primeiro_horario_w,
		cd_setor_atendimento_w,
		cd_estabelecimento_w,
		dt_prescricao_w,
		dt_inicio_prescr_w,
		dt_liberacao_farmacia_w,
		ie_lib_farm_w,
		ie_rep_adep_w,
		ie_motivo_prescricao_w
	from	prescr_medica
	where	nr_prescricao	= nr_prescricao_p;

	open C20;
	loop
	fetch C20 into	
		ie_momento_lote_w;
	exit when C20%notfound;
		begin
		ie_momento_lote_w	:= ie_momento_lote_w;
		end;
	end loop;
	close C20;

	if	(((ie_momento_lote_w = 'E') or (ie_lib_farm_w = 'N')) and
		 (dt_liberacao_w is not null) and
		 (qt_registro_w = 0)) or
		((ie_momento_lote_w = 'F') and
		 (dt_liberacao_farmacia_w is not null) and
		 (qt_registro_w = 0)) then
		begin
		
		if	(ie_momento_lote_w = 'F') and
			(dt_liberacao_farmacia_w is not null) then
			dt_liberacao_w	:= dt_liberacao_farmacia_w;
		end if;
		
		obter_Param_Usuario(924, 192, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_proc_intervalo_w);
		obter_param_usuario(924, 217, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, varPrescrNaoMedicaADEP_w);
		obter_param_usuario(924, 799, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_local_estoque_w);
		obter_param_usuario(1113, 70, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_necessidade_disp_w);
		obter_param_usuario(1113, 393, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_gerar_nec_disp_kit_w);
		obter_param_usuario(924, 587, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, cd_motivo_baixa_w);
		
		cd_motivo_baixa_param_w	:= cd_motivo_baixa_w;
		
		ie_usuario_medico_w := obter_se_usuario_medico(nm_usuario_p);
		
		select	nvl(max(qt_min_agora),0),
			nvl(max(qt_min_especial),0),
			max(ie_classif_urgencia)
		into	qt_min_agora_w,
			qt_min_especial_w,
			ie_classif_nao_padrao_w
		from	parametros_farmacia
		where	cd_estabelecimento	= cd_estabelecimento_w;
		
		dt_limite_agora_w	:= dt_liberacao_w + qt_min_agora_w/1440; 
		dt_limite_especial_w	:= dt_liberacao_w + qt_min_especial_w/1440; 
		
		if	(dt_primeiro_horario_w is null) then
			dt_primeiro_horario_w	:= dt_prescricao_w;
		end if;

		qt_dia_adic_w	:= 0;
		
		if	(ie_tipo_item_p in ('M', 'IAH')) then
			open C01;
			loop
				fetch C01 into
					nr_atendimento_w,
					ie_agrupador_w,
					nr_seq_prescr_w,
					qt_dose_w,
					qt_total_dispensar_w,
					cd_unidade_medida_w,
					cd_unid_med_dose_w,
					nr_ocorrencia_w,
					cd_material_w,
					ds_horarios_w,
					ds_horarios_padr_w,
					hr_prim_horario_w,
					ie_urgente_w,
					qt_dose_especial_w,
					hr_dose_especial_w,
					ie_se_necessario_w,
					ie_acm_w,
					ie_controlado_w,
					ie_padronizado_w,
					ie_regra_disp_w,
					nr_agrupamento_w,
					cd_protocolo_w,
					cd_local_estoque_w;
				exit when C01%notfound;
				
				cd_setor_atendimento_w := cd_setor_atendimento_w;
				if	(ie_local_estoque_w = 'S') then
					define_local_disp_prescr(nr_prescricao_p, nr_seq_prescr_w, obter_perfil_ativo, nm_usuario_p);
					select	max(cd_local_estoque)
					into	cd_local_estoque_w
					from	prescr_material b
					where	nr_prescricao	= nr_prescricao_p
					and	nr_sequencia	= nr_seq_prescr_w;
					
					cd_setor_atendimento_w		:= Obter_Setor_Atendimento(nr_atendimento_w);
					
				end if;
				
				varie_cursor_w	:= 'S';
			
				qt_dia_adic_w	:= 0;

				dt_prescricao_ww	:= dt_prescricao_w;
				if	(ie_urgente_w = 'N') then
					dt_prescricao_ww	:= dt_inicio_prescr_w; 
				end if;
				
				if	(ie_acm_w = 'S') or
					(ie_se_necessario_w = 'S') then
				
					ie_acm_sn_w := 'S';
				else
					ie_acm_sn_w := 'N';
				end if;
				
			
				if	(ds_horarios_padr_w is not null) then
					while	ds_horarios_padr_w is not null LOOP
						select	instr(ds_horarios_padr_w, ' ') 
						into	k
						from	dual;
			
						if	(k > 1) and
							(substr(ds_horarios_padr_w, 1, k -1) is not null) then
							ds_hora_w		:= substr(ds_horarios_padr_w, 1, k-1);
							ds_hora_w		:= replace(ds_hora_w, ' ','');
							ds_horarios_padr_w	:= substr(ds_horarios_padr_w, k + 1, 2000);
						elsif	(ds_horarios_padr_w is not null) then
							ds_hora_w 		:= replace(ds_horarios_padr_w,' ','');
							ds_horarios_padr_w	:= '';
						end if;
						
						if	(instr(ds_hora_w,'A') > 0) and
							(ie_urgente_w = 'N') and
							(qt_dia_adic_w = 0) then
							qt_dia_adic_w	:= 1;
						elsif	(instr(ds_hora_w,'AA') > 0) and
							(ie_urgente_w = 'N') then
							qt_dia_adic_w	:= qt_dia_adic_w + 1;
						end if;
			
						ds_hora_w	:= replace(ds_hora_w,'A','');
						ds_hora_w	:= replace(ds_hora_w,'A','');
			
            dt_horario_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_prescricao_ww + qt_dia_adic_w, replace(ds_hora_w,'A','')), 'mi');
						
						select	prescr_mat_hor_seq.nextval
						into	nr_sequencia_w
						from	dual;
			
						-- Rafael em 07/10/2006 Identificar horarios especiais 
						if	(ie_se_necessario_w = 'S') or
							(ie_acm_w = 'S') then
							ie_horario_especial_w	:= 'S';
						else
							ie_horario_especial_w	:= 'N';
						end if;	
						
						if	(dt_horario_w < dt_prescricao_ww) then
							dt_horario_w	:= dt_horario_w + 1;
						end if; 
						
						if	(ie_classif_nao_padrao_w is null) or
							(ie_padronizado_w = 'S') then
							begin
							ie_classif_urgente_w	:= 'N';
							if	(dt_horario_w <= dt_limite_agora_w) or
								(ie_urgente_w = 'S') then
								ie_classif_urgente_w	:= 'A';
							elsif	(dt_horario_w <= dt_limite_especial_w) then
								ie_classif_urgente_w	:= 'E';
							end if;
							end;
						else
							ie_classif_urgente_w	:= ie_classif_nao_padrao_w;
						end if;
						
						/*if	(ie_rep_adep_w = 'N') or
							((varPrescrNaoMedicaADEP_w = 'NMP') and
							 (ie_usuario_medico_w = 'N') and
							 (cd_protocolo_w is not null)) then
							ie_adep_w := 'N';
						else
							ie_adep_w := 'S';
						end if;*/

						if	((ie_gerar_necessidade_disp_w = 'N') and
						 (ie_regra_disp_w = 'N')) then
							ie_gerar_lote_w			:= 'N';
							dt_emissao_farmacia_w	:= sysdate;
							qt_dispensar_hor_w		:= 0;
						else
							ie_gerar_lote_w			:= 'S';
							cd_motivo_baixa_w		:= 0;
							dt_emissao_farmacia_w	:= null;
							qt_dispensar_hor_w := dividir(qt_total_dispensar_w,nr_ocorrencia_w);
						end if;
						

						insert into prescr_mat_hor(
							nr_sequencia,  
							nr_seq_digito,
							nr_prescricao,
							nr_seq_material,
							ie_agrupador,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ds_horario,
							dt_horario,
							qt_dose,
							qt_dispensar,
							cd_unidade_medida,
							cd_unidade_medida_dose,
							cd_material,
							nr_ocorrencia,
							qt_dispensar_hor,
							ie_urgente,
							ie_horario_especial,
							nr_seq_turno,
							ie_aprazado,
							ie_controlado,
							ie_padronizado,
							ie_classif_urgente,
							ie_dispensar_farm,
							nr_agrupamento,
							ie_adep,
							dt_emissao_farmacia,
							dt_lib_horario,
							nr_atendimento,
							cd_motivo_baixa,
							ie_gerar_lote)
						values(	nr_sequencia_w,
							calcula_digito('MODULO11',nr_sequencia_w),
							nr_prescricao_p,
							nr_seq_prescr_w,
							ie_agrupador_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							ds_hora_w,
							dt_horario_w,
							qt_dose_w,
							qt_total_dispensar_w,
							cd_unidade_medida_w,
							cd_unid_med_dose_w,
							cd_material_w,
							nr_ocorrencia_w,
							qt_dispensar_hor_w,

							ie_urgente_w,
							'N', --nvl(ie_horario_especial_w,'N'),
							Obter_turno_horario_prescr(cd_estabelecimento_w,cd_setor_atendimento_w,ds_hora_w,cd_local_estoque_w),
							'S',
							ie_controlado_w,
							ie_padronizado_w,
							ie_classif_urgente_w,
							ie_regra_disp_w,
							nr_agrupamento_w,
							'S',
							dt_emissao_farmacia_w,

							sysdate,
							nr_atendimento_w,
							cd_motivo_baixa_w,
							ie_gerar_lote_w);

						Gerar_Gedi_Medic_atend(cd_estabelecimento_w, nr_prescricao_p, nr_seq_prescr_w, nr_sequencia_w, nm_usuario_p);

						gravar_log_tasy(1234, 'altes  de aprazar itens dependentes cd_motivo_baixa_param_w: ' || cd_motivo_baixa_param_w, 'tasy');			
						
						aprazar_itens_dependentes(cd_estabelecimento_w, cd_setor_atendimento_w, nr_atendimento_w, nr_prescricao_p, nr_seq_item_p, nr_agrupamento_w, dt_horario_w, ie_gerar_necessidade_disp_w, nm_usuario_p, null, null, null, null, '');
																		
						if	(ie_gerar_nec_disp_kit_w = 'S') then
							update	prescr_material
							set		cd_motivo_baixa	= 0,
									dt_baixa	= null
							where	nr_prescricao		= nr_prescricao_p
							and		nr_sequencia_diluicao	= nr_seq_prescr_w;
							
							update	prescr_material
							set		cd_motivo_baixa	= 0,
									dt_baixa	= null
							where	nr_prescricao	= nr_prescricao_p
							and		nr_seq_kit	= nr_seq_prescr_w;
						else
							update	prescr_material
							set		cd_motivo_baixa	= cd_motivo_baixa_param_w,
									dt_baixa	= sysdate
							where	nvl(cd_motivo_baixa,0) = 0
							and		nr_prescricao		= nr_prescricao_p
							and		nr_sequencia_diluicao	= nr_seq_prescr_w;
																					
							update	prescr_material
							set		cd_motivo_baixa	= cd_motivo_baixa_param_w,
									dt_baixa	= sysdate
							where	nvl(cd_motivo_baixa,0) = 0
							and		nr_prescricao	= nr_prescricao_p
							and		nr_seq_kit	= nr_seq_prescr_w;

							update	prescr_mat_hor a
							set		a.cd_motivo_baixa	= cd_motivo_baixa_param_w,
									a.dt_emissao_farmacia	= sysdate,
									a.ie_gerar_lote = ie_gerar_lote_w,
									a.qt_dispensar_hor = 0
							where	a.nr_prescricao		= nr_prescricao_p
							and		nvl(a.cd_motivo_baixa,0) = 0
							and		nvl(a.ie_horario_especial,'N') = 'N'
							and		nvl(a.ie_aprazado,'S') = 'S'
							and		exists(	select	1
											from	prescr_material b
											where	b.nr_prescricao = a.nr_prescricao
											and		b.nr_sequencia = a.nr_seq_material
											and		b.nr_sequencia_diluicao = nr_seq_prescr_w);

							
							update	prescr_mat_hor a
							set		a.cd_motivo_baixa	= cd_motivo_baixa_param_w,
									a.dt_emissao_farmacia	= dt_emissao_farmacia_w,
									a.ie_gerar_lote = ie_gerar_lote_w,
									a.qt_dispensar_hor = 0
							where	a.nr_prescricao	= nr_prescricao_p
							and		nvl(a.cd_motivo_baixa,0) = 0
							and		nvl(a.ie_horario_especial,'N') = 'N'
							and		nvl(a.ie_aprazado,'S') = 'S'
							and		exists(	select	1
											from	prescr_material b
											where	b.nr_prescricao = a.nr_prescricao
											and		b.nr_sequencia = a.nr_seq_material
											and		b.nr_seq_kit	= nr_seq_prescr_w);
						end if;
						
						gerar_evento_reaprazamento(nr_atendimento_w, nr_prescricao_p, nr_seq_item_p, nr_sequencia_w, cd_material_w, ie_tipo_item_p, 15, nr_seq_motivo_p, ds_justificativa_p, nm_usuario_p, null, ie_acm_sn_w, nr_seq_assinatura_p);
						
						adep_gerar_area_prep(nr_prescricao_p, null, nm_usuario_p);
						
					end loop;
				end if;
			end loop;
			close C01;

		elsif (ie_tipo_item_p = 'S') then
			open C12;
			loop
			fetch C12 into
					nr_atendimento_w,
					ie_agrupador_w,
					nr_seq_prescr_w,
					qt_dose_w,
					qt_total_dispensar_w,
					cd_unidade_medida_w,
					cd_unid_med_dose_w,
					nr_ocorrencia_w,
					cd_material_w,
					ds_horarios_w,
					ds_horarios_padr_w,
					hr_prim_horario_w,
					ie_urgente_w,
					qt_dose_especial_w,
					hr_dose_especial_w,
					ie_se_necessario_w,
					ie_acm_w,
					ie_controlado_w,
					ie_padronizado_w,
					ie_regra_disp_w,
					nr_agrupamento_w,
					cd_protocolo_w,
					cd_local_estoque_w;
				exit when C12%notfound;
				
				varie_cursor_w	:= 'S';
			
				dt_prescricao_ww	:= dt_prescricao_w;
				if	(ie_urgente_w = 'N') then
					dt_prescricao_ww	:= dt_inicio_prescr_w; 
				end if;

				qt_dia_adic_w	:= 0;
			
				if	(ds_horarios_padr_w is not null) then
					while	ds_horarios_padr_w is not null LOOP
						select	instr(ds_horarios_padr_w, ' ') 
						into	k
						from	dual;

						if	(k > 1) and
							(substr(ds_horarios_padr_w, 1, k -1) is not null) then
							ds_hora_w		:= substr(ds_horarios_padr_w, 1, k-1);
							ds_hora_w		:= replace(ds_hora_w, ' ','');
							ds_horarios_padr_w	:= substr(ds_horarios_padr_w, k + 1, 2000);
						elsif	(ds_horarios_padr_w is not null) then
							ds_hora_w 		:= replace(ds_horarios_padr_w,' ','');
							ds_horarios_padr_w	:= '';
						end if;
				
						if	(instr(ds_hora_w,'A') > 0) and
							(ie_urgente_w = 'N') and
							(qt_dia_adic_w = 0) then
							qt_dia_adic_w	:= 1;
						elsif	(instr(ds_hora_w,'AA') > 0) and
							(ie_urgente_w = 'N') then
							qt_dia_adic_w	:= qt_dia_adic_w + 1;
						end if;
			
						ds_hora_w	:= replace(ds_hora_w,'A','');
						ds_hora_w	:= replace(ds_hora_w,'A','');

            dt_horario_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_prescricao_ww + qt_dia_adic_w, replace(ds_hora_w,'A','')), 'mi');
						
						select	prescr_mat_hor_seq.nextval
						into	nr_sequencia_w
						from	dual;

						-- Rafael em 07/10/2006 Identificar horarios especiais 
						if	(ie_se_necessario_w = 'S') or
							(ie_acm_w = 'S') then
							ie_horario_especial_w	:= 'S';
						else
							ie_horario_especial_w	:= 'N';
						end if;

						if	(dt_horario_w < dt_prescricao_ww) then
							dt_horario_w	:= dt_horario_w + 1;
						end if;

						if	(ie_classif_nao_padrao_w is null) or
							(ie_padronizado_w = 'S') then
							begin
							ie_classif_urgente_w	:= 'N';
							if	(dt_horario_w <= dt_limite_agora_w) or
								(ie_urgente_w = 'S') then
								ie_classif_urgente_w	:= 'A';
							elsif	(dt_horario_w <= dt_limite_especial_w) then
								ie_classif_urgente_w	:= 'E';
							end if;
							end;
						else
							ie_classif_urgente_w	:= ie_classif_nao_padrao_w;
						end if;
					
						insert into prescr_mat_hor(
							nr_sequencia, 
							nr_seq_digito,
							nr_prescricao,
							nr_seq_material,
							ie_agrupador,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ds_horario,
							dt_horario,
							qt_dose,
							qt_dispensar,
							cd_unidade_medida,
							cd_unidade_medida_dose,
							cd_material,
							nr_ocorrencia,
							qt_dispensar_hor,
							ie_urgente,
							ie_horario_especial,
							nr_seq_turno,
							ie_aprazado,
							ie_controlado,
							ie_padronizado,
							ie_classif_urgente,
							ie_dispensar_farm,
							nr_agrupamento,
							ie_adep,
							dt_emissao_farmacia,
							dt_lib_horario,
							nr_atendimento)
						values(	nr_sequencia_w,
							calcula_digito('MODULO11',nr_sequencia_w),
							nr_prescricao_p,
							nr_seq_prescr_w,
							ie_agrupador_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							ds_hora_w,
							dt_horario_w,
							qt_dose_w,
							qt_total_dispensar_w,
							cd_unidade_medida_w,
							cd_unid_med_dose_w,
							cd_material_w,
							nr_ocorrencia_w,
							dividir(qt_total_dispensar_w,nr_ocorrencia_w),
							ie_urgente_w,
							'N', --nvl(ie_horario_especial_w,'N'),
							Obter_turno_horario_prescr(cd_estabelecimento_w,cd_setor_atendimento_w,ds_hora_w,cd_local_estoque_w),
							'S',
							ie_controlado_w,
							ie_padronizado_w,
							ie_classif_urgente_w,
							ie_regra_disp_w,
							nr_agrupamento_w,
							'S',
							decode(ie_gerar_necessidade_disp_w,'S',null,sysdate),
							sysdate,
							nr_atendimento_w);
							
						Gerar_Gedi_Medic_atend(cd_estabelecimento_w, nr_prescricao_p, nr_seq_prescr_w, nr_sequencia_w, nm_usuario_p);
						gerar_evento_reaprazamento( nr_atendimento_w, nr_prescricao_p, nr_seq_item_p, nr_sequencia_w, cd_material_w, ie_tipo_item_p, 15, nr_seq_motivo_p, ds_justificativa_p, nm_usuario_p, null, null, nr_seq_assinatura_p);
						
					end loop;
				end if;
			end loop;
			close C12;
			
		elsif (ie_tipo_item_p = 'IA') then
			
			open C05;
			loop
			fetch C05 into
					nr_atendimento_w,
					ie_agrupador_w,
					nr_seq_prescr_w,
					qt_dose_w,
					qt_total_dispensar_w,
					cd_unidade_medida_w,
					cd_unid_med_dose_w,
					nr_ocorrencia_w,
					cd_material_w,
					ds_horarios_w,
					ds_horarios_padr_w,
					hr_prim_horario_w,
					ie_urgente_w,
					qt_dose_especial_w,
					hr_dose_especial_w,
					ie_se_necessario_w,
					ie_acm_w,
					ie_controlado_w,
					ie_padronizado_w,
					ie_regra_disp_w,
					nr_agrupamento_w,
					cd_protocolo_w,
					cd_local_estoque_w;
				exit when C05%notfound;
				
				varie_cursor_w	:= 'S';
			
				dt_prescricao_ww	:= dt_prescricao_w;
				if	(ie_urgente_w = 'N') then
					dt_prescricao_ww	:= dt_inicio_prescr_w; 
				end if;
				
				qt_dia_adic_w	:= 0;
			
				if	(ds_horarios_padr_w is not null) then
					while	ds_horarios_padr_w is not null LOOP
						select	instr(ds_horarios_padr_w, ' ') 
						into	k
						from	dual;

						if	(k > 1) and
							(substr(ds_horarios_padr_w, 1, k -1) is not null) then
							ds_hora_w		:= substr(ds_horarios_padr_w, 1, k-1);
							ds_hora_w		:= replace(ds_hora_w, ' ','');
							ds_horarios_padr_w	:= substr(ds_horarios_padr_w, k + 1, 2000);
						elsif	(ds_horarios_padr_w is not null) then
							ds_hora_w 		:= replace(ds_horarios_padr_w,' ','');
							ds_horarios_padr_w	:= '';
						end if;
				
						if	(instr(ds_hora_w,'A') > 0) and
							(ie_urgente_w = 'N') and
							(qt_dia_adic_w = 0) then
							qt_dia_adic_w	:= 1;
						elsif	(instr(ds_hora_w,'AA') > 0) and
							(ie_urgente_w = 'N') then
							qt_dia_adic_w	:= qt_dia_adic_w + 1;
						end if;
			
						ds_hora_w	:= replace(ds_hora_w,'A','');
						ds_hora_w	:= replace(ds_hora_w,'A','');

						dt_horario_w	:= trunc(ESTABLISHMENT_TIMEZONE_UTILS.dateAtTime(dt_prescricao_ww + qt_dia_adic_w, replace(ds_hora_w,'A','')), 'mi');
						
						select	prescr_mat_hor_seq.nextval
						into	nr_sequencia_w
						from	dual;

						-- Rafael em 07/10/2006 Identificar horarios especiais 
						if	(ie_se_necessario_w = 'S') or
							(ie_acm_w = 'S') then
							ie_horario_especial_w	:= 'S';
						else
							ie_horario_especial_w	:= 'N';
						end if;

						if	(dt_horario_w < dt_prescricao_ww) then
							dt_horario_w	:= dt_horario_w + 1;
						end if;

						if	(ie_classif_nao_padrao_w is null) or
							(ie_padronizado_w = 'S') then
							begin
							ie_classif_urgente_w	:= 'N';
							if	(dt_horario_w <= dt_limite_agora_w) or
								(ie_urgente_w = 'S') then
								ie_classif_urgente_w	:= 'A';
							elsif	(dt_horario_w <= dt_limite_especial_w) then
								ie_classif_urgente_w	:= 'E';
							end if;
							end;
						else
							ie_classif_urgente_w	:= ie_classif_nao_padrao_w;
						end if;
					
						insert into prescr_mat_hor(
							nr_sequencia, 
							nr_seq_digito,
							nr_prescricao,
							nr_seq_material,
							ie_agrupador,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							ds_horario,
							dt_horario,
							qt_dose,
							qt_dispensar,
							cd_unidade_medida,
							cd_unidade_medida_dose,
							cd_material,
							nr_ocorrencia,
							qt_dispensar_hor,
							ie_urgente,
							ie_horario_especial,
							nr_seq_turno,
							ie_aprazado,
							ie_controlado,
							ie_padronizado,
							ie_classif_urgente,
							ie_dispensar_farm,
							nr_agrupamento,
							ie_adep,
							dt_emissao_farmacia,
							dt_lib_horario,
							nr_atendimento)
						values(	nr_sequencia_w,
							calcula_digito('MODULO11',nr_sequencia_w),
							nr_prescricao_p,
							nr_seq_prescr_w,
							ie_agrupador_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							ds_hora_w,
							dt_horario_w,
							qt_dose_w,
							qt_total_dispensar_w,
							cd_unidade_medida_w,
							cd_unid_med_dose_w,
							cd_material_w,
							nr_ocorrencia_w,
							dividir(qt_total_dispensar_w,nr_ocorrencia_w),
							ie_urgente_w,
							'N', --nvl(ie_horario_especial_w,'N'),
							Obter_turno_horario_prescr(cd_estabelecimento_w,cd_setor_atendimento_w,ds_hora_w,cd_local_estoque_w),
							'S',
							ie_controlado_w,
							ie_padronizado_w,
							ie_classif_urgente_w,
							ie_regra_disp_w,
							nr_agrupamento_w,
							'S',
							decode(ie_gerar_necessidade_disp_w,'S',null,sysdate),
							sysdate,
							nr_atendimento_w);
							
						Gerar_Gedi_Medic_atend(cd_estabelecimento_w, nr_prescricao_p, nr_seq_prescr_w, nr_sequencia_w, nm_usuario_p);
						gerar_evento_reaprazamento( nr_atendimento_w, nr_prescricao_p, nr_seq_item_p, nr_sequencia_w, cd_material_w, ie_tipo_item_p, 15, nr_seq_motivo_p, ds_justificativa_p, nm_usuario_p, null, null, nr_seq_assinatura_p);
						
					end loop;
				end if;
			end loop;
			close C05;
		end if;
		
		open C016;
		loop
		fetch C016 into	
			nr_seq_classif_w,
			ie_classif_urgente_w,
			ie_controlado_w,
			ie_padronizado_w;
		exit when C016%notfound;
			begin
			
			if	(ie_controlado_w = 'A') and
				(ie_padronizado_w = 'A') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;
			elsif	(ie_controlado_w = 'A') and
				(ie_padronizado_w = 'S') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_padronizado		= 'S'
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;				
			elsif	(ie_controlado_w = 'A') and
				(ie_padronizado_w = 'N') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_padronizado		= 'N'
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;				
			elsif	(ie_controlado_w = 'N') and
				(ie_padronizado_w = 'A') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_controlado		= 'N'
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;				
			elsif	(ie_controlado_w = 'N') and
				(ie_padronizado_w = 'N') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_controlado		= 'N'
				and	ie_padronizado		= 'N'
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;				
			elsif	(ie_controlado_w = 'N') and
				(ie_padronizado_w = 'S') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_controlado		= 'N'
				and	ie_padronizado		= 'S'
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;				
			elsif	(ie_controlado_w = 'S') and
				(ie_padronizado_w = 'A') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_controlado		= 'S'
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;				
			elsif	(ie_controlado_w = 'S') and
				(ie_padronizado_w = 'N') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_controlado		= 'S'
				and	ie_padronizado		= 'N'
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;				
			elsif	(ie_controlado_w = 'S') and
				(ie_padronizado_w = 'S') then
				update	prescr_mat_hor
				set	nr_seq_classif		= nr_seq_classif_w
				where	nr_prescricao		= nr_prescricao_p
				and	ie_controlado		= 'S'
				and	ie_padronizado		= 'S'
				and	ie_classif_urgente	= ie_classif_urgente_w
				and		nr_seq_material	= nr_seq_item_p;				
			end if;
			end;
		end loop;
		close C016;

		begin
		select	nvl(max(obter_valor_param_usuario(924, 179, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)),'N')
		into	ajustar_disp_hor_farm_w
		from	dual;
		exception
			when others then
			ajustar_disp_hor_farm_w := 'N';
		end;
		
		begin
		if	(ajustar_disp_hor_farm_w <> 'N') then
			calcular_dispensar_horario(nr_prescricao_p,0);
		end if;
		exception
			when others then
			ajustar_disp_hor_farm_w := 'N';
		end;

		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

		if	(varie_cursor_w	= 'S') then

			Gerar_Lote_Atend_Prescricao(nr_prescricao_p, nr_seq_item_p, 0, 'S', nm_usuario_p, 'AIP');
			
			open C02;
			loop
			fetch C02 into
				nr_sequencia_w,
				nr_seq_lote_w;
			exit when C02%notfound;
				begin
				
				if (nr_seq_lote_w > 0) then
				
				update prescr_mat_alteracao
				set 	nr_seq_lote = nr_seq_lote_w
				where	nr_prescricao = nr_prescricao_p
				and		nr_seq_horario = nr_sequencia_w;
								
				end if;	
				end;
			end loop;
			close C02;
		end if;

		end;
	end if;
	
	if	(ie_gerar_necessidade_disp_w = 'S') then
	
		select	count(*)
		into	qt_registro_w
		from	prescr_mat_hor
		where	nr_prescricao			= nr_prescricao_p
		and	nr_seq_material			= nr_seq_item_p
		and	nvl(ie_horario_especial,'N')	= 'N'
		and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';
		
		if	(qt_registro_w > 0) then
		
			update	prescr_medica
			set	dt_emissao_farmacia	= null,
				nm_usuario_imp_far	= null
			where	nr_prescricao		= nr_prescricao_p;
			
			update	prescr_material
			set	cd_motivo_baixa	= 0,
				dt_baixa	= null
			where	nr_prescricao	= nr_prescricao_p
			and	nr_sequencia	= nr_seq_item_p;
			
		end if;
		
	end if;
	
end if;

end gerar_aprazamento_interv;
/
