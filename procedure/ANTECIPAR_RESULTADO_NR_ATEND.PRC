CREATE OR REPLACE PROCEDURE antecipar_resultado_nr_atend(
    nr_atendimento_p            NUMBER,
    ds_observacao_antecipacao_p VARCHAR2,
    nr_seq_motivo_antecipacao_p NUMBER,
    dt_antecipacao_exame_p      DATE,
    nm_usuario_p                VARCHAR2)
IS
  nr_prescricao_w NUMBER;
  nr_sequencia_w  NUMBER;
  CURSOR c1
  IS
    SELECT pp.nr_prescricao,
      pp.nr_sequencia
    FROM prescr_medica pm,
      prescr_procedimento pp
    WHERE pm.nr_prescricao = pp.nr_prescricao
    AND pm.NR_ATENDIMENTO  = nr_atendimento_p;
BEGIN
  OPEN c1;
  LOOP
    FETCH C1 INTO nr_prescricao_w, nr_sequencia_w;
    BEGIN
      UPDATE prescr_procedimento
      SET dt_antecipa_result      = dt_antecipacao_exame_p,
        ds_observacao_antecipacao = ds_observacao_antecipacao_p,
        nm_usuario_antecipacao    = nm_usuario_p,
        nr_seq_motivo_antecipacao = nr_seq_motivo_antecipacao_p
      WHERE nr_prescricao         = nr_prescricao_w
      AND nr_sequencia            = nr_sequencia_w;
      UPDATE prescr_procedimento
      SET dt_neg_atencip_result  = NULL,
        ds_obs_antecip_result    = NULL,
        nr_seq_mot_anteci_result = NULL
      WHERE nr_prescricao        = nr_prescricao_w
      AND nr_sequencia           = nr_sequencia_w;
    END;
    EXIT
  WHEN C1%notfound;
  END LOOP;
  CLOSE C1;
  COMMIT;
END antecipar_resultado_nr_atend;
/