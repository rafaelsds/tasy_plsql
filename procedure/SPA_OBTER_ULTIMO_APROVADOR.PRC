create or replace
function spa_obter_ultimo_aprovador(	nr_seq_spa_p	Number,
				ie_opcao_p	Varchar2)
 		    	return Varchar2 is

ds_retorno_w		varchar2(255);

nr_seq_aprovacao_w	spa_aprovacao.nr_sequencia%type;

nm_usuario_w		usuario.nm_usuario%type;
ds_usuario_w		usuario.ds_usuario%type;

Cursor C01 is
	select	nr_sequencia,
		nm_usuario
	from	spa_aprovacao
	where	nr_seq_spa = nr_seq_spa_p
	and	dt_aprovacao is not null
	order by	dt_aprovacao,
		nr_ordem,
		nvl(nr_nivel,0);
		

/* ie_opcao_p:
N - NM_USUARIO
D - DS_USUARIO
*/
			
begin

if	(nvl(nr_seq_spa_p,0) > 0) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_aprovacao_w,
		nm_usuario_w;
	exit when C01%notfound;
		begin
		nr_seq_aprovacao_w	:= nr_seq_aprovacao_w;
		nm_usuario_w		:= nm_usuario_w;
		end;
	end loop;
	close C01;
	
	if	(nvl(nr_seq_aprovacao_w,0) > 0) then
		
		if	(ie_opcao_p = 'N') then		
			ds_retorno_w	:= nm_usuario_w;			
		elsif	(ie_opcao_p = 'D') then			
			select	ds_usuario
			into	ds_usuario_w
			from 	usuario
			where 	nm_usuario	= nm_usuario_w;
		
			ds_retorno_w	:= ds_usuario_w;
		end if;		
		
	end if;

end if;

return	ds_retorno_w;

end spa_obter_ultimo_aprovador;
/