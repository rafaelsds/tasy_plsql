create or replace 
procedure liberar_evento_qua(
				nr_sequencia_p	number, 
				nm_usuario_p 	varchar2,
				ds_mensagem_p 	out varchar2) is
				
nr_seq_evento_w			number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);
nr_seq_evento_qua_w		number(10);	
qt_idade_w				number(10);
cd_setor_atendimento_w	number(10);
ie_tipo_evento_w		Varchar2(15);
ie_tipo_evento_dom_w	Varchar2(15);
ie_contem_medic_w		Varchar2(1);
qt_reg_w				number(10);
nr_regras_atendidas_w   varchar2(2000);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento	= cd_estabelecimento_w))
	and	nvl(NR_SEQ_QUA_EVENTO,nr_seq_evento_qua_w)	= nr_seq_evento_qua_w
	and	ie_evento_disp		= 'LIBE'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A';
	
Cursor C02 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_setor_atendimento_w = 0) or (cd_setor_atendimento	= cd_setor_atendimento_w))
	and	nvl(NR_SEQ_QUA_EVENTO,nr_seq_evento_qua_w)	= nr_seq_evento_qua_w
	and	ie_evento_disp		= 'LBV'
	and	nvl(ie_situacao,'A') = 'A';
	
begin

select	max(nr_atendimento),
	max(cd_pessoa_fisica),
	nvl(max(nr_seq_evento),0),
	nvl(max(ie_tipo_evento),'E')
into	nr_atendimento_w,
	cd_pessoa_fisica_w,
	nr_seq_evento_qua_w,
	ie_tipo_evento_w
from	qua_evento_paciente
where	nr_sequencia	= nr_sequencia_p;

Select 	max(b.ie_tipo_evento)
into	ie_tipo_evento_dom_w
from	qua_evento_paciente a,
	qua_tipo_evento b,
	qua_evento c
where  	b.nr_sequencia = c.nr_Seq_tipo
and 	a.nr_Seq_evento = c.nr_sequencia
and     a.nr_sequencia = nr_sequencia_p;

qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);

if	(ie_tipo_evento_dom_w = 'PRM') then
	ie_contem_medic_w := obter_se_cadastro_medic_prm(nr_sequencia_p);
	
	if	( ie_contem_medic_w = 'N' ) then
		ds_mensagem_p := wheb_mensagem_pck.get_texto(300336);
		goto Final;
	end if;
end if;

update	qua_evento_paciente e
set	dt_atualizacao_nrec = sysdate,
	dt_liberacao = sysdate,
	nm_usuario = nm_usuario_p,
	nm_usuario_nrec = nm_usuario_p
where	nr_sequencia = nr_sequencia_p;

select	nvl(max(cd_estabelecimento),0)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_w;

open C01;
loop
fetch C01 into	
	nr_seq_evento_w;
exit when C01%notfound;
	begin
	if (ie_tipo_evento_w = 'E') then
		gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,nm_usuario_p,null,'N');
	end if;
	end;
end loop;
close C01;

cd_setor_atendimento_w := obter_setor_atendimento(nr_atendimento_w);
open C02;
loop
fetch C02 into	
	nr_seq_evento_w;
exit when C02%notfound;
	begin
	if (ie_tipo_evento_w = 'E') then
		gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,nm_usuario_p,null,'N');
	end if;	
	end;
end loop;
close C02;

begin
GQA_Liberacao_qua_evento(nr_sequencia_p,nm_usuario_p, nr_regras_atendidas_w);
gera_protocolo_assistencial(nr_atendimento_w, nm_usuario_p);
exception
	when others then
	null;
end;

begin
gerar_ficha_UPP_evento(nr_sequencia_p,nm_usuario_p);
exception
	when others then
	null;
end;

begin
if	(nr_atendimento_w > 0) then
	tof_gerar_meta_eventos(nr_atendimento_w, nr_seq_evento_qua_w, nm_usuario_p);
end if;
exception
	when others then
	null;
end;

<<Final>>

qt_reg_w := 0;

end liberar_evento_qua;
/
