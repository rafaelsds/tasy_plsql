create or replace
procedure Qt_atualizar_aut_convenio(	nr_atendimento_p	number,
					nr_seq_atendimento_p	number,
					ie_acao_p		varchar2,
					nm_usuario_p		varchar2) is 

nr_seq_paciente_w		number(10);					
nr_seq_aut_conv_w		number(10);
cd_convenio_w			number(5);
cd_protocolo_w			number(10);
ie_forma_autor_ciclo_w		varchar2(255);
ie_gerar_autor_quimioterapia_w	varchar2(5);
ie_vincular_atend_aut_w varchar2(5);

/*
ie_acao_p:

GA - Gerar atendimento
VA - Vincular atendimento
DA - Desvincular atendimento

*/					
begin

obter_param_usuario(865, 184, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_vincular_atend_aut_w);

select	substr(Qt_Obter_Seq_Pac_Seq_Atend(nr_seq_atendimento_p, nm_usuario_p),1,20)
into	nr_seq_paciente_w
from	dual;

select 	max(a.cd_convenio),
	max(b.cd_protocolo)
into	cd_convenio_w,
	cd_protocolo_w
from	paciente_setor b,  
	paciente_setor_convenio a
where	a.nr_seq_paciente	= b.nr_seq_paciente
and	a.nr_seq_paciente	= nr_seq_paciente_w;

select	nvl(max(ie_forma_autor_quimioterapia),'C'),
	nvl(max(ie_gerar_autor_quimioterapia),'S')
into	ie_forma_autor_ciclo_w,
	ie_gerar_autor_quimioterapia_w
from	convenio_estabelecimento
where	cd_convenio		= cd_convenio_w
and	cd_estabelecimento	= obter_estabelecimento_ativo;

if	(cd_protocolo_w is not null) then
	select 	nvl(max(ie_forma_autor_quimio),ie_forma_autor_ciclo_w)
	into 	ie_forma_autor_ciclo_w
	from 	protocolo
	where	cd_protocolo	= cd_protocolo_w;
end if;

if (ie_vincular_atend_aut_w = 'N') then

    if	(nr_atendimento_p is not null) and
        (nr_seq_atendimento_p is not null)then

        if	(ie_acao_p = 'GA') or
            (ie_acao_p = 'VA')then

            update 	autorizacao_convenio
            set	nr_atendimento		= nr_atendimento_p,
                nm_usuario		= nm_usuario_p,
                dt_atualizacao		= sysdate
            where	((nr_seq_paciente		= nr_seq_atendimento_p) 
            or 	(ie_forma_autor_ciclo_w 	= 'C' and ie_gerar_autor_quimioterapia_w <> 'N'))
            and	nr_seq_paciente_setor	= nr_seq_paciente_w
            and	nr_atendimento		is null;
            commit;		

        else
        if	(ie_acao_p = 'DA')then		
            update 	autorizacao_convenio
            set	nr_atendimento		= null,
                nm_usuario		= nm_usuario_p,
                dt_atualizacao		= sysdate
            where	((nr_seq_paciente		= nr_seq_atendimento_p) 
            or 	(ie_forma_autor_ciclo_w 	= 'C' and ie_gerar_autor_quimioterapia_w <> 'N'))
            and	nr_seq_paciente_setor	= nr_seq_paciente_w;		
            commit;		
        end if;	

        end if;

    end if;

end if;

if (ie_vincular_atend_aut_w = 'T' or ie_vincular_atend_aut_w = 'S' or ie_vincular_atend_aut_w = 'D') then
    valida_vinc_onco_autorizacao(nm_usuario_p, nr_seq_atendimento_p, nr_atendimento_p, ie_acao_p);
end if;

commit;

end Qt_atualizar_aut_convenio;
/
