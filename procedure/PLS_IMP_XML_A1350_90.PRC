create or replace
procedure pls_imp_xml_a1350_90
			(nr_seq_lote_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is 

qt_inicial_w		pls_integer;
qt_final_w		pls_integer;

--Cabe�alho
cd_unimed_origem_w	ptu_mov_pessoa_lote.cd_unimed_origem%type;
cd_unimed_destino_w	ptu_mov_pessoa_lote.cd_unimed_destino%type;
nr_versao_transacao_w	ptu_mov_pessoa_lote.nr_versao_transacao%type;

nr_seq_mov_benef_w	ptu_mov_pessoa_benef.nr_sequencia%type;
cd_unimed_w		ptu_mov_pessoa_benef.cd_unimed%type;
cd_identificacao_w	ptu_mov_pessoa_benef.cd_identificacao%type;
nr_seq_segurado_w	ptu_mov_pessoa_benef.nr_seq_segurado%type;

--Dados pessoa
ds_dados_pessoa_w	varchar2(4000);
nm_pessoa_imp_w		ptu_mov_pessoa_dados.nm_pessoa_imp%type;
nm_social_imp_w		ptu_mov_pessoa_dados.nm_social_imp%type;
nm_mae_imp_w		ptu_mov_pessoa_dados.nm_mae_imp%type;
dt_nascimento_imp_w	ptu_mov_pessoa_dados.dt_nascimento_imp%type;
ie_tipo_sexo_imp_w	ptu_mov_pessoa_dados.ie_tipo_sexo_imp%type;
ie_estado_civil_imp_w	ptu_mov_pessoa_dados.ie_estado_civil_imp%type;
nr_cpf_imp_w		ptu_mov_pessoa_dados.nr_cpf_imp%type;
nr_identidade_imp_w	ptu_mov_pessoa_dados.nr_identidade_imp%type;
ds_orgao_emissor_imp_w	ptu_mov_pessoa_dados.ds_orgao_emissor_imp%type;
nr_pais_imp_w		ptu_mov_pessoa_dados.nr_pais_imp%type;
cd_cns_imp_w		ptu_mov_pessoa_dados.cd_cns_imp%type;
nr_pispasep_imp_w	ptu_mov_pessoa_dados.nr_pispasep_imp%type;
cd_municipio_imp_w	ptu_mov_pessoa_dados.cd_municipio_imp%type;
dt_atualizacao_w	ptu_mov_pessoa_dados.dt_atualizacao_arq%type;

--Dados de endere�o
ds_dados_endereco_w		varchar2(4000);
ie_residencia_imp_w		ptu_mov_pessoa_endereco.ie_residencia_imp%type;
ie_tipo_endereco_imp_w		ptu_mov_pessoa_endereco.ie_tipo_endereco_imp%type;
ie_tipo_logradouro_imp_w	ptu_mov_pessoa_endereco.ie_tipo_logradouro_imp%type;
ds_logradouro_imp_w		ptu_mov_pessoa_endereco.ds_logradouro_imp%type;
nr_logradouro_imp_w		ptu_mov_pessoa_endereco.nr_logradouro_imp%type;
ds_complemento_imp_w		ptu_mov_pessoa_endereco.ds_complemento_imp%type;
ds_bairro_imp_w			ptu_mov_pessoa_endereco.ds_bairro_imp%type;
cd_municipio_ibge_imp_w		ptu_mov_pessoa_endereco.cd_municipio_ibge_imp%type;
cd_cep_imp_w			ptu_mov_pessoa_endereco.cd_cep_imp%type;

--Dados de contato
ds_dados_contato_w		varchar2(4000);
ie_tipo_telefone_imp_w		ptu_mov_pessoa_contato.ie_tipo_telefone_imp%type;
nr_ddd_imp_w			ptu_mov_pessoa_contato.nr_ddd_imp%type;
nr_telefone_imp_w		ptu_mov_pessoa_contato.nr_telefone_imp%type;
ie_tipo_email_imp_w		ptu_mov_pessoa_contato.ie_tipo_email_imp%type;
ds_email_imp_w			ptu_mov_pessoa_contato.ds_email_imp%type;

Cursor C01 is
	select	ds_linha,
		ie_tipo_registro
	from	w_pls_a1350
	order by ie_tipo_registro;

begin

delete	from	ptu_mov_pessoa_benef
where	nr_seq_lote	= nr_seq_lote_p;

for r_c01_w in C01 loop
	begin
	if	(r_c01_w.ie_tipo_registro = 1) then --Cabe�alho
		cd_unimed_origem_w	:= pls_extrair_dado_tag_xml(r_c01_w.ds_linha,'<mi:codigoUnimedOrigemMensagem>');
		cd_unimed_destino_w	:= pls_extrair_dado_tag_xml(r_c01_w.ds_linha,'<mi:codigoUnimedDestinoMensagem>');
		nr_versao_transacao_w	:= pls_extrair_dado_tag_xml(r_c01_w.ds_linha,'<mi:versaoTransacao>');
		
		update	ptu_mov_pessoa_lote
		set	cd_unimed_origem	= cd_unimed_origem_w,
			cd_unimed_destino	= cd_unimed_destino_w,
			nr_versao_transacao	= nr_versao_transacao_w,
			dt_importacao		= sysdate,
			ie_status		= 'I'
		where	nr_sequencia	= nr_seq_lote_p;
	elsif	(r_c01_w.ie_tipo_registro = 2) then --Benefici�rios
		ds_dados_pessoa_w	:= substr(r_c01_w.ds_linha,instr(r_c01_w.ds_linha,'<mi:dadosPessoa>')+16, instr(r_c01_w.ds_linha,'</mi:dadosPessoa>') - (instr(r_c01_w.ds_linha,'<mi:dadosPessoa>')+16));
		ds_dados_endereco_w	:= substr(r_c01_w.ds_linha,instr(r_c01_w.ds_linha,'<mi:enderecoBeneficiario>')+25, instr(r_c01_w.ds_linha,'</mi:enderecoBeneficiario>') - (instr(r_c01_w.ds_linha,'<mi:enderecoBeneficiario>')+25));
		ds_dados_contato_w	:= substr(r_c01_w.ds_linha,instr(r_c01_w.ds_linha,'<mi:dadosContatoBeneficiario>')+29, instr(r_c01_w.ds_linha,'</mi:dadosContatoBeneficiario>') - (instr(r_c01_w.ds_linha,'<mi:dadosContatoBeneficiario>')+29));
		
		cd_unimed_w		:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:codigoUnimed>');
		cd_identificacao_w	:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:IdBenef>');
		
		select	max(nr_seq_segurado)
		into	nr_seq_segurado_w
		from	pls_segurado_carteira
		where	cd_usuario_plano = cd_unimed_w||cd_identificacao_w;
		
		select	ptu_mov_pessoa_benef_seq.nextval
		into	nr_seq_mov_benef_w
		from	dual;
		
		insert	into	ptu_mov_pessoa_benef
			(	nr_sequencia, nr_seq_lote, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				cd_unimed, cd_identificacao, nr_seq_segurado)
			values(	nr_seq_mov_benef_w, nr_seq_lote_p, sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				cd_unimed_w, cd_identificacao_w, nr_seq_segurado_w);
		
		--Dados da pessoa
		nm_pessoa_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:nomeCompletoBeneficiario>');
		nm_social_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:nomeSocial>');
		nm_mae_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:nomeMaeBeneficiario>');
		dt_nascimento_imp_w	:= to_date(pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:dataNascimentoBeneficiario>'),'yyyy/mm/dd');
		ie_tipo_sexo_imp_w	:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:tipoSexo>');
		ie_estado_civil_imp_w	:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:codigoEstadoCivil>');
		nr_cpf_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:codigoCPF>');
		nr_identidade_imp_w	:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:codigoRG>');
		ds_orgao_emissor_imp_w	:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:orgaoEmissor>');
		nr_pais_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:codigoPais>');
		cd_cns_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:codigoNacionalSaude>');
		nr_pispasep_imp_w	:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:pisPasep>');
		cd_municipio_imp_w	:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:codigoMunicipal>');
		dt_atualizacao_w	:= pls_extrair_dado_tag_xml(ds_dados_pessoa_w,'<mi:dataAtualizacao>');
		
		insert	into	ptu_mov_pessoa_dados
				(nr_sequencia, nr_seq_mov_benef, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nm_pessoa_imp, nm_social_imp, nm_mae_imp, dt_nascimento_imp,
				ie_tipo_sexo_imp, ie_estado_civil_imp, nr_cpf_imp,
				nr_identidade_imp, ds_orgao_emissor_imp, nr_pais_imp,
				cd_cns_imp, nr_pispasep_imp, cd_municipio_imp,
				dt_atualizacao_arq)
			values(	ptu_mov_pessoa_dados_seq.nextval, nr_seq_mov_benef_w, sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				nm_pessoa_imp_w, nm_social_imp_w, nm_mae_imp_w, dt_nascimento_imp_w,
				ie_tipo_sexo_imp_w, ie_estado_civil_imp_w, nr_cpf_imp_w,
				nr_identidade_imp_w, ds_orgao_emissor_imp_w, nr_pais_imp_w,
				cd_cns_imp_w, nr_pispasep_imp_w, cd_municipio_imp_w,
				dt_atualizacao_w);
		
		--Dados de endere�o
		ie_residencia_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:IndicaResidencia>');
		ie_tipo_endereco_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:tipoEndereco>');
		ie_tipo_logradouro_imp_w	:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:tipoLogradouro>');
		ds_logradouro_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:descricaoLogradouro>');
		nr_logradouro_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:numeroLogradouro>');
		ds_complemento_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:complementoLogradouro>');
		ds_bairro_imp_w			:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:descricaoBairro>');
		cd_municipio_ibge_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:codigoMunicipal>');
		cd_cep_imp_w			:= pls_extrair_dado_tag_xml(ds_dados_endereco_w,'<mi:numeroCep>');
		
		insert	into	ptu_mov_pessoa_endereco
			(	nr_sequencia, nr_seq_mov_benef, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				ie_residencia_imp, ie_tipo_endereco_imp, ie_tipo_logradouro_imp,
				ds_logradouro_imp, nr_logradouro_imp, ds_complemento_imp,
				ds_bairro_imp, cd_municipio_ibge_imp, cd_cep_imp)
			values(	ptu_mov_pessoa_endereco_seq.nextval, nr_seq_mov_benef_w, sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				ie_residencia_imp_w, ie_tipo_endereco_imp_w, ie_tipo_logradouro_imp_w,
				ds_logradouro_imp_w, nr_logradouro_imp_w, ds_complemento_imp_w,
				ds_bairro_imp_w, cd_municipio_ibge_imp_w, cd_cep_imp_w);
		
		--Dados de contato
		ie_tipo_telefone_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_contato_w,'<mi:tipoTelefone>');
		nr_ddd_imp_w			:= pls_extrair_dado_tag_xml(ds_dados_contato_w,'<mi:numeroDDD>');
		nr_telefone_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_contato_w,'<mi:numeroTelefone>');
		ie_tipo_email_imp_w		:= pls_extrair_dado_tag_xml(ds_dados_contato_w,'<mi:tipoEmail>');
		ds_email_imp_w			:= pls_extrair_dado_tag_xml(ds_dados_contato_w,'<mi:enderecoEmail>');
		
		insert	into	ptu_mov_pessoa_contato
			(	nr_sequencia, nr_seq_mov_benef, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				ie_tipo_telefone_imp, nr_ddd_imp, nr_telefone_imp,
				ie_tipo_email_imp, ds_email_imp)
			values(	ptu_mov_pessoa_contato_seq.nextval, nr_seq_mov_benef_w, sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				ie_tipo_telefone_imp_w, nr_ddd_imp_w, nr_telefone_imp_w,
				ie_tipo_email_imp_w, ds_email_imp_w);
	end if;
	end;
end loop;

commit;

end pls_imp_xml_a1350_90;
/
