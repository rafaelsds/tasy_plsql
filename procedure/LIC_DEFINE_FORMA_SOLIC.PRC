create or replace
procedure lic_define_forma_solic(	nr_solic_compra_p				number,
				nr_seq_forma_compra_p			varchar2,
				nm_usuario_p				varchar2,
				ds_erro_p			out		varchar2) is 

ie_forma_compra_w			varchar2(15);
nr_item_solic_compra_w		number(10);
nr_item_solic_compra_ww		number(10);
qt_material_w			number(13,4);
nr_seq_licitacao_w			number(10);
nr_seq_lic_item_w			number(10);
cd_material_w			number(06);
ds_erro_w			varchar2(255);
qt_falta_w			number(13,4) := 0;
qt_acumulado_w			number(13,4) := 0;
qt_saldo_w			number(13,4);	
nr_item_solic_compra_entr_w		number(5);
lista_material_w			varchar2(255) := '';
vl_item_w				number(15,4);
qt_item_ww			number(15,4);
qt_item_w			number(15,4);
qt_itens_w			number(10);
ie_primeira_vez_w			varchar2(1);
nr_sequencia_w			number(10);
nr_sequencia_ww			number(10);
cd_estabelecimento_w		number(10);
cd_local_estoque_w		number(10);
nr_seq_reg_compra_w		solic_compra_item.nr_seq_reg_compra%type;
ie_param_284_w			funcao_param_usuario.vl_parametro%type;
nr_seq_licitacao_sc_w		solic_compra_item.nr_seq_licitacao%type;
nr_seq_lic_item_sc_w		solic_compra_item.nr_seq_lic_item%type;
dt_inicio_vigencia_w		reg_compra.dt_inicio_vigencia%type;

cursor c01 is
select	nr_item_solic_compra,
	cd_material,
	qt_material,
	nr_seq_reg_compra,
	nr_seq_licitacao,
	nr_seq_lic_item
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p;

cursor c02 is
select	a.dt_inicio_vigencia, 
	a.nr_seq_licitacao,
	b.nr_seq_lic_item,
	lic_obter_saldo_item_solic(a.nr_seq_licitacao, b.nr_seq_lic_item) qt_saldo,
	b.vl_item
from	reg_compra a,
	reg_compra_item b
where	a.nr_sequencia	= b.nr_seq_reg_compra
and	b.cd_material	= cd_material_w
and	b.dt_cancelamento	is null
and	((ie_param_284_w = 'A') or
	((ie_param_284_w = 'S') and (a.nr_sequencia = nr_seq_reg_compra_w)
	and	(a.nr_seq_licitacao = nr_seq_licitacao_sc_w) and (b.nr_seq_lic_item = nr_seq_lic_item_sc_w)))
and	trunc(sysdate,'dd') between trunc(a.dt_inicio_vigencia,'dd') and trunc(a.dt_fim_vigencia,'dd')
and	(lic_obter_saldo_item_solic(a.nr_seq_licitacao, b.nr_seq_lic_item) - lic_obter_qt_utilizada_solic(nr_solic_compra_p, a.nr_seq_licitacao, b.nr_seq_lic_item)) > 0
order by	dt_inicio_vigencia; /*N�o mudar o order by porque sempre tem que come�ar pelo registro de pre�o mais velho*/


cursor c03 is
select	distinct
	a.cd_material
from	solic_compra_item a
where	a.nr_solic_compra	= nr_solic_compra_p
and not exists(
	select	1
	from	reg_lic_item_solic x
	where	x.nr_solic_compra		= a.nr_solic_compra
	and	x.nr_item_solic_compra	= a.nr_item_solic_compra);

cursor c04 is
select	nr_item_solic_compra
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p;

cursor c05 is
select	nr_sequencia,	
	qt_item
from	reg_lic_item_solic
where	nr_solic_compra = nr_solic_compra_p
and	nr_item_solic_compra = nr_item_solic_compra_w;

begin

select	cd_estabelecimento,
	cd_local_estoque
into	cd_estabelecimento_w,
	cd_local_estoque_w
from	solic_compra
where	nr_solic_compra = nr_solic_compra_p;

select	obter_valor_param_usuario(913, 284, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)
into	ie_param_284_w
from	dual;

select	lic_obter_tipo_forma_compra(nr_seq_forma_compra_p)
into	ie_forma_compra_w
from	dual;

delete from reg_lic_item_solic
where	nr_solic_compra = nr_solic_compra_p;

/*Rotina para carregar automaticamente a licita��o para o item da solicita��o de compras*/
if	(ie_forma_compra_w is not null) and
	(ie_forma_compra_w = 'R') then
	
	open C01;
	loop
	fetch C01 into	
		nr_item_solic_compra_w,
		cd_material_w,
		qt_material_w,
		nr_seq_reg_compra_w,
		nr_seq_licitacao_sc_w,
		nr_seq_lic_item_sc_w;
	exit when C01%notfound;
		begin

		qt_falta_w	:= qt_material_w;
		qt_acumulado_w	:= 0;
		
		open C02;
		loop
		fetch C02 into	
			dt_inicio_vigencia_w,
			nr_seq_licitacao_w,
			nr_seq_lic_item_w,
			qt_saldo_w,
			vl_item_w;
		exit when C02%notfound;
			begin
			
			update	solic_compra_item
			set	vl_unit_previsto 	= vl_item_w
			where	nr_solic_compra		= nr_solic_compra_p
			and	nr_item_solic_compra	= nr_item_solic_compra_w;			

			if	(qt_falta_w > 0) then
				
				if	(qt_saldo_w > qt_falta_w) then
					qt_saldo_w := qt_falta_w;
				end if;			
				
				insert into reg_lic_item_solic(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_licitacao,
					nr_seq_lic_item,
					nr_solic_compra,
					nr_item_solic_compra,
					qt_item)
				values(	reg_lic_item_solic_seq.nextval,
					sysdate,
					nm_usuario_p,
					nr_seq_licitacao_w,
					nr_seq_lic_item_w,
					nr_solic_compra_p,
					nr_item_solic_compra_w,
					qt_saldo_w);
					
				qt_falta_w	:= qt_falta_w - qt_saldo_w;
				qt_acumulado_w	:= qt_acumulado_w + qt_saldo_w;
				
			end if;
			end;
		end loop;
		close C02;
		
		

		select	sum(qt_material)
		into	qt_material_w
		from	solic_compra_item
		where	nr_solic_compra		= nr_solic_compra_p
		and	nr_item_solic_compra	= nr_item_solic_compra_w;
		
		if	(qt_acumulado_w > 0) and
			(qt_acumulado_w < qt_material_w) then
		
			update	solic_compra_item
			set	qt_material		= qt_acumulado_w
			where	nr_solic_compra		= nr_solic_compra_p
			and	nr_item_solic_compra	= nr_item_solic_compra_w;
			
			select	nvl(min(nr_item_solic_compra_entr),0)
			into	nr_item_solic_compra_entr_w
			from	solic_compra_item_entrega
			where	nr_solic_compra		= nr_solic_compra_p
			and	nr_item_solic_compra	= nr_item_solic_compra_w
			and	qt_entrega_solicitada	> qt_falta_w;
			
			if	(nr_item_solic_compra_entr_w > 0) then
				update	solic_compra_item_entrega
				set	qt_entrega_solicitada	= (qt_entrega_solicitada - qt_falta_w)
				where	nr_solic_compra		= nr_solic_compra_p
				and	nr_item_solic_compra	= nr_item_solic_compra_w
				and	nr_item_solic_compra_entr	= nr_item_solic_compra_entr_w;
			end if;
		
			gravar_solic_compra_consist(
				nr_solic_compra_p, '1',
				WHEB_MENSAGEM_PCK.get_texto(279561) || cd_material_w, 'M',
				substr(WHEB_MENSAGEM_PCK.get_texto(279563) || cd_material_w || WHEB_MENSAGEM_PCK.get_texto(279562) || chr(13) || chr(10) ||
				WHEB_MENSAGEM_PCK.get_texto(279564) || campo_mascara_virgula(qt_material_w) || WHEB_MENSAGEM_PCK.get_texto(279566) || campo_mascara_virgula(qt_acumulado_w) || '.',1,255),
				nm_usuario_p);
			
			insert into solic_compra_hist(
				nr_sequencia,
				nr_solic_compra,
				dt_atualizacao,
				nm_usuario,
				dt_historico,
				ds_titulo,
				ds_historico,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_tipo,
				cd_evento,
				dt_liberacao)
			values(	solic_compra_hist_seq.nextval,
				nr_solic_compra_p,
				sysdate,
				'Tasy',
				sysdate,
				WHEB_MENSAGEM_PCK.get_texto(279565),
				WHEB_MENSAGEM_PCK.get_texto(279563) || cd_material_w || WHEB_MENSAGEM_PCK.get_texto(279562) || chr(13) || chr(10) ||
				WHEB_MENSAGEM_PCK.get_texto(279564) || campo_mascara_virgula(qt_material_w) || WHEB_MENSAGEM_PCK.get_texto(279566) || campo_mascara_virgula(qt_acumulado_w) || '.',
				sysdate,
				'Tasy',
				'S',
				'B',
				sysdate);
		end if;
		
	

		end;
	end loop;
	close C01;
end if;

open C04;
loop
fetch C04 into	
	nr_item_solic_compra_w;
exit when C04%notfound;
	begin
	
	select	count(*)
	into	qt_itens_w
	from	reg_lic_item_solic
	where	nr_solic_compra = nr_solic_compra_p
	and	nr_item_solic_compra = nr_item_solic_compra_w;
	
	if	(qt_itens_w > 1) then
		
		ie_primeira_vez_w	:= 'S';
		qt_item_ww		:= 0;
		
		open C05;
		loop
		fetch C05 into	
			nr_sequencia_w,
			qt_item_w;
		exit when C05%notfound;
			begin		
			
			if	(ie_primeira_vez_w = 'S') then
				qt_item_ww 	:= qt_item_w;
				nr_sequencia_ww	:= nr_sequencia_w;
			end if;
			
			if	(ie_primeira_vez_w = 'N') then
				
				select	nvl(max(nr_item_solic_compra),0) + 1
				into	nr_item_solic_compra_ww
				from	solic_compra_item
				where	nr_solic_compra = nr_solic_compra_p;
			
				insert into solic_compra_item(
					nr_solic_compra,
					nr_item_solic_compra,
					cd_material,
					cd_unidade_medida_compra,
					qt_material,
					dt_atualizacao,
					nm_usuario,
					ie_situacao,
					ds_material_direto,
					ds_observacao,
					nr_cot_compra,
					nr_item_cot_compra,
					cd_motivo_baixa,
					dt_baixa,
					dt_solic_item,
					vl_unit_previsto,
					ie_geracao,
					nr_seq_proj_rec,
					nr_seq_motivo_cancel,
					ie_forma_exportar,
					nr_solic_compra_ref,
					nr_item_solic_compra_ref,
					cd_conta_contabil,
					ds_observacao_int,
					nr_contrato,
					ie_bula,
					ie_amostra,
					ie_catalogo,
					nr_seq_orc_item_gpi,
					qt_conv_compra_est_orig,
					nr_seq_marca,
					nr_seq_regra_contrato,
					nr_Seq_aprovacao,
					dt_autorizacao,
					qt_saldo_disp_estoque)
				select	nr_solic_compra_p,
					nr_item_solic_compra_ww,
					cd_material,
					cd_unidade_medida_compra,
					qt_item_w,
					sysdate,
					nm_usuario_p,
					ie_situacao,
					ds_material_direto,
					ds_observacao,
					nr_cot_compra,
					nr_item_cot_compra,
					cd_motivo_baixa,
					dt_baixa,
					dt_solic_item,
					vl_unit_previsto,
					ie_geracao,
					nr_seq_proj_rec,
					nr_seq_motivo_cancel,
					ie_forma_exportar,
					nr_solic_compra_ref,
					nr_item_solic_compra_ref,
					cd_conta_contabil,
					ds_observacao_int,
					nr_contrato,
					ie_bula,
					ie_amostra,
					ie_catalogo,
					nr_seq_orc_item_gpi,
					qt_conv_compra_est_orig,
					nr_seq_marca,
					nr_seq_regra_contrato,
					nr_Seq_aprovacao,
					dt_autorizacao,
					obter_saldo_disp_estoque(cd_estabelecimento_w, cd_material, cd_local_estoque_w, trunc(sysdate,'mm'))
				from	solic_compra_item
				where	nr_solic_compra = nr_solic_compra_p
				and	nr_item_solic_compra = nr_item_solic_compra_w;
				
				insert into solic_compra_item_entrega(
					nr_solic_compra,
					nr_item_solic_compra,
					nr_item_solic_compra_entr,
					qt_entrega_solicitada,
					dt_entrega_solicitada,
					dt_atualizacao,
					nm_usuario,
					ds_observacao)
				select	nr_solic_compra_p,
					nr_item_solic_compra_ww,
					nr_item_solic_compra_entr,
					qt_item_w,
					dt_entrega_solicitada,
					sysdate,
					nm_usuario_p,
					ds_observacao
				from	solic_compra_item_entrega
				where	nr_solic_compra = nr_solic_compra_p
				and	nr_item_solic_compra = nr_item_solic_compra_w
				and	rownum <= 1;
				
				insert into reg_lic_item_solic(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_licitacao,
					nr_seq_lic_item,
					nr_solic_compra,
					nr_item_solic_compra,
					qt_item)
				select	reg_lic_item_solic_seq.nextval,
					sysdate,
					nm_usuario_p,
					nr_seq_licitacao,
					nr_seq_lic_item,
					nr_solic_compra_p,
					nr_item_solic_compra_ww,
					qt_item
				from	reg_lic_item_solic
				where	nr_Sequencia = nr_sequencia_w;
				
				delete from reg_lic_item_solic
				where nr_sequencia = nr_sequencia_w;
			end if;	
			
			ie_primeira_vez_w := 'N';
			
			end;
		end loop;
		close C05;

		if	(qt_item_ww > 0) then
			update	solic_compra_item
			set	qt_material = qt_item_ww
			where	nr_solic_compra = nr_solic_compra_p
			and	nr_item_solic_compra = nr_item_solic_compra_w;
			
			update	solic_compra_item_entrega
			set	qt_entrega_solicitada = qt_item_ww
			where	nr_solic_compra = nr_solic_compra_p
			and	nr_item_solic_compra = nr_item_solic_compra_w;
			
			if	(nr_sequencia_ww > 0) then
				update	reg_lic_item_solic
				set	qt_item = qt_item_ww
				where	nr_sequencia = nr_sequencia_ww;
			end if;			
		end if;		
	end if;	
	end;
end loop;
close C04;

open C03;
loop
fetch C03 into	
	cd_material_w;
exit when C03%notfound;
	begin
	
	lista_material_w := substr(lista_material_w || cd_material_w || ', ',1,255);
	
	end;
end loop;
close C03;

if	(ie_forma_compra_w is not null) and
	(ie_forma_compra_w = 'R') and
	(lista_material_w is not null) then
	
	lista_material_w := substr(lista_material_w, 1, length(lista_material_w) -2);
	
	gravar_solic_compra_consist(
				nr_solic_compra_p, '1',
				WHEB_MENSAGEM_PCK.get_texto(279567), 'M',
				substr(WHEB_MENSAGEM_PCK.get_texto(279568) || lista_material_w,1,2000),
				nm_usuario_p);

	open C03;
	loop
	fetch C03 into	
		cd_material_w;
	exit when C03%notfound;
		begin
		
		delete from solic_compra_item
		where	nr_solic_compra = nr_solic_compra_p
		and	cd_material = cd_material_w;
		
		end;
	end loop;
	close C03;
end if;

select	count(*)
into	qt_itens_w
from	solic_compra_item
where	nr_solic_compra = nr_solic_compra_p;

if	(qt_itens_w = 0) then
	ESTORNAR_LIB_SOLIC_COMPRA(nr_solic_compra_p, nm_usuario_p);
end if;

ds_erro_p := ds_erro_w;

commit;

end lic_define_forma_solic;
/