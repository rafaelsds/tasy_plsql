create or replace
procedure eup_consis_atrib_cad_compl_pf	(cd_pessoa_fisica_p	varchar2,
					ie_atrib_obrig_p	out varchar,
					ds_menssagem_p		out varchar) is 

--Globais
cd_setor_atendimento_w		number(10);
cd_estabelecimento_w		number(10);
cd_perfil_w			number(10);
nm_usuario_w			varchar2(20);

ie_obriga_w			varchar2(2);
ds_regra_w			varchar2(4000);


begin
if	(cd_pessoa_fisica_p is not null) then
	cd_setor_atendimento_w	:= wheb_usuario_pck.get_cd_setor_atendimento;
	cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
	cd_perfil_w		:= wheb_usuario_pck.get_cd_perfil;
	nm_usuario_w		:= wheb_usuario_pck.get_nm_usuario;

	consistir_regra_compl_pf(cd_estabelecimento_w, cd_perfil_w, cd_pessoa_fisica_p, cd_setor_atendimento_w,ie_obriga_w, ds_regra_w, ie_atrib_obrig_p, 'U', 0);
	ds_menssagem_p := obter_texto_tasy(186881, 0);

end if;

end eup_consis_atrib_cad_compl_pf;
/
