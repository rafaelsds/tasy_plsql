CREATE OR REPLACE 
PROCEDURE Obter_Se_Long_Contem(	nm_tabela_p VARCHAR2,
									nm_coluna_p VARCHAR2,
									ds_restricao_where_p VARCHAR2,
									ds_parametros_sql_p VARCHAR2,
									ds_texto_busca_p in VARCHAR2,
									ie_achou_p OUT VARCHAR2) is


/*
nm_tabela_p -> Passar o nome da tabela onde deve ser realizada a consulta
nm_coluna_p-> Nome da columa que contem o conteudo LONG
ds_restricao_where_p - > Colocar a restri��o WHERE para retornar 1 (um) registro da tabela
ds_parametros_sql_p -> Passar os par�metros para a restri��o WHERE
ds_texto_busca_p -> Texto a ser procedurado dentro do conte�do LONG
ie_achou_p -> Retorna S se encontrou o texto e N caso n�o tenha encontrado.

*/
ds_param_w		VARCHAR2(2000);
ds_campo_clob_w		CLOB;
ds_sql_w		VARCHAR2(2000);
nr_sequencia_w		NUMBER(10);
qt_registro_w		NUMBER(10);
c001			INTEGER;
retorno_w		NUMBER(5);
dt_aux_w		DATE;
ds_texto_busca_w	VARCHAR2(2000);
ds_texto_busca_w2	VARCHAR2(2000);
ie_elimina_acentuacao_w	varchar2(1);

BEGIN

ie_achou_p := 'N';

ds_texto_busca_w	:= upper(ds_texto_busca_p);
ds_texto_busca_w2	:= lower(ds_texto_busca_p);

if	(ds_texto_busca_p <> elimina_acentuacao(ds_texto_busca_p)) then
	ie_elimina_acentuacao_w	:= 'S';
	ds_texto_busca_w := upper(converte_acentuacao_rtf(ds_texto_busca_w));
	ds_texto_busca_w2 := upper(converte_acentuacao_rtf(ds_texto_busca_w2));
else
	ie_elimina_acentuacao_w	:= 'N';	
end	if;

--dbms_output.put_line(ds_texto_busca_w);
--dbms_output.put_line(ds_texto_busca_w2);
--raise_application_error(-20011,ds_texto_busca_w);

/*INICIO - VERIFICAR SE TABELA TEMPOR�RIO EXISTE SE N�O EXISTIR CRIA*/
SELECT 	COUNT(*)
INTO	qt_registro_w
FROM	user_tables
WHERE	table_name = 'W_COPIA_CAMPO_LONG';

IF	( qt_registro_w = 0 ) THEN
	Exec_Sql_Dinamico('','create table w_copia_campo_long (nr_sequencia number(10), ds_texto clob)');
END IF;
/*FIM - VERIFICAR SE TABELA TEMPOR�RIO EXISTE SE N�O EXISTIR CRIA*/


/*INICIO - TRANSFERE CONTEUDO DO CAMPO LONG DA TABELA DE ORIGEM PARA O CAMPO CLOB DA TABELA TEMPORARIO*/

Obter_Valor_Dinamico ('select	(nvl(max(nr_sequencia),0) + 1) from w_copia_campo_long',nr_sequencia_w);
ds_sql_w   := 'insert into w_copia_campo_long select :SEQUENCE, to_lob('|| nm_coluna_p || ') from ' || nm_tabela_p || ' ' || ds_restricao_where_p;
ds_param_w := 'SEQUENCE='|| TO_CHAR(NR_SEQUENCIA_W)||';' || ds_parametros_sql_p;
Exec_Sql_Dinamico_Bv('',ds_sql_w,ds_param_w);

/*FIM - TRANSFERE CONTEUDO DO CAMPO LONG DA TABELA DE ORIGEM PARA O CAMPO CLOB DA TABELA TEMPORARIO*/


/*INICIO - RECUPERA O VALOR DO CAMPO CLOB PARA A VARIAVEL DA PROCEDURE*/
ds_sql_w	:= ' select ds_texto from w_copia_campo_long where nr_sequencia = :sequence ';

C001 := DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(C001, ds_sql_w, dbms_sql.Native);
DBMS_SQL.DEFINE_COLUMN(C001, 1, ds_campo_clob_w);
DBMS_SQL.BIND_VARIABLE(C001, 'SEQUENCE', nr_sequencia_w);
retorno_w := DBMS_SQL.EXECUTE(c001);
retorno_w := DBMS_SQL.FETCH_ROWS(c001);
DBMS_SQL.COLUMN_VALUE(C001, 1, ds_campo_clob_w );
DBMS_SQL.CLOSE_CURSOR(C001);
/*FIM - RECUPERA O VALOR DO CAMPO CLOB PARA A VARIAVEL DA PROCEDURE*/

/*INICIO - QUEBRA O VALOR DO CONTEUDO CLOB EM VARIOS VARCHAR PARA PODER INSERIR NA TABELA DE ORIGEM*/

ds_campo_clob_w := UPPER(ds_campo_clob_w);

--dbms_output.put_line(substr(ds_texto_busca_w,1,255));
--dbms_output.put_line(substr(ds_campo_clob_w,1,255));

if	(ie_elimina_acentuacao_w = 'S') and
	((dbms_lob.INSTR(ds_campo_clob_w,ds_texto_busca_w,1,1) > 0) or 
	 (dbms_lob.INSTR(ds_campo_clob_w,ds_texto_busca_w2,1,1) > 0)) then
	ie_achou_p := 'S';
elsif	(dbms_lob.INSTR(ds_campo_clob_w,ds_texto_busca_w,1,1) > 0 ) then
	ie_achou_p := 'S';
end	if;

/*ds_conteudo_1_w := dbms_lob.SUBSTR(ds_campo_clob_w,32764,1);
ds_conteudo_2_w := dbms_lob.SUBSTR(ds_campo_clob_w,32764,32765);*/
/*FIM QUEBRA O VALOR DO CONTEUDO CLOB EM VARIOS VARCHAR PARA PODER INSERIR NA TABELA DE ORIGEM*/


/*INICIO - DELETA O REGISTRO DA TABELA TEMPOR�RIA*/
ds_sql_w := 'delete from w_copia_campo_long where nr_sequencia = :nr_sequencia';
ds_param_w := 'NR_SEQUENCIA='|| TO_CHAR(nr_sequencia_w);
Exec_Sql_Dinamico_Bv('',ds_sql_w,ds_param_w);
/*FIM - DELETA O REGISTRO DA TABELA TEMPOR�RIA*/

END Obter_Se_Long_Contem;
/
