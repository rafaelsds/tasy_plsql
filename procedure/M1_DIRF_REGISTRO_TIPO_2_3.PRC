create or replace
procedure M1_DIRF_REGISTRO_TIPO_2_3(nr_seq_lote_dirf_p	number,
				nr_seq_arquivo_p	number,
				cd_cgc_estab_p		varchar2,
				nm_usuario_p		Varchar2) is 
				
dt_lote_w		date;
cd_darf_w		varchar(10);
cd_cgc_w		varchar(20);
vl_min_ano_pf_w		number(15,2);
ie_data_w		number(1);

vl_rendimento_w		number(15,2);
vl_irrf_w		number(15,2);
ie_mes_w		varchar(2);
ds_arquivo_w		varchar2(2000);
nr_seq_arquivo_w	number(10);

cd_pessoa_fisica_w	varchar2(25);
vl_total_w		number(15,2);
nr_mes_w		number(2);
qt_dif_mes_w		number(10);
ie_contador_w		number(10);
ie_entou_w		boolean;

nr_titulo_w		varchar2(20);
				
cursor C01 is -- busca todas as pessoas juridicas que possuem tituloas a pagar com reten��o de imposto.
select	distinct 
	p.cd_cgc,
	nvl(i.cd_darf,'1708'),
	1
from	titulo_pagar p,
	titulo_pagar_imposto i
where	p.nr_titulo = i.nr_titulo
and	trunc(p.dt_contabil,'YYYY') = trunc(dt_lote_w,'YYYY')
and	p.cd_cgc  is not null
and	nvl(i.cd_darf,'9999') in ('1708','8045','3280')
and	p.ie_situacao <> 'C' 
union all
select	distinct 
	p.cd_cgc,
	nvl(i.cd_darf,'1708'),
	2
from	titulo_pagar p,
	titulo_pagar_imposto i
where	p.nr_titulo = i.nr_titulo
and	trunc(p.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
and	p.cd_cgc  is not null
and	nvl(i.cd_darf,'1708') in ('5952','5960','5979','5987')
and	p.ie_situacao <> 'C' ;


cursor C02 is -- busca os titulos a pagar da pessoa filtrada no cursor C01

	select 	sum(vl_rendimento),
		sum(vl_imposto)
	from
	(select nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'RET','IR'),0) vl_rendimento,
		nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'IMP','IR'),0) vl_imposto
	from	titulo_pagar p,
		titulo_pagar_imposto i,
		tributo t
	where	p.nr_titulo = i.nr_titulo(+)
	and	i.cd_tributo = t.cd_tributo(+)
	--and 	trunc(p.dt_emissao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and 	trunc(decode(ie_data_w,1,p.dt_contabil,p.dt_liquidacao),'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_cgc = cd_cgc_w
	and	nvl(i.cd_darf,'1708') = decode(cd_darf_w,'5952','9999',cd_darf_w)
	and	p.ie_situacao <> 'C'
	and	nvl(to_char(decode(ie_data_w,1,p.dt_contabil,p.dt_liquidacao),'mm'),'00') = lpad(ie_mes_w,2,'0')
	and	exists (select 	1 
		from 	titulo_pagar x,
			titulo_pagar_imposto z,
			tributo t
		where	x.nr_titulo = z.nr_titulo
		and	t.cd_tributo = z.cd_tributo
		and	x.cd_cgc = P.cd_cgc
		and	z.vl_imposto > 0
		and 	t.ie_tipo_tributo = 'IR')
	union all
	select 	nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'RET','COFINS'),0) vl_rendimento,
		nvl(obter_rendimento_cofins_ir_pj(p.nr_titulo,'IMP','COFINS'),0) vl_imposto
	from	titulo_pagar p
	--where	trunc(p.dt_emissao,'YYYY') = trunc(dt_lote_w,'YYYY')
	where 	trunc(decode(ie_data_w,1,p.dt_contabil,p.dt_liquidacao),'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_cgc = cd_cgc_w
	and	p.ie_situacao <> 'C'
	and	cd_darf_w = '5952'
	and	nvl(to_char(decode(ie_data_w,1,p.dt_contabil,p.dt_liquidacao),'mm'),'00') = lpad(ie_mes_w,2,'0')
	and	exists (select 	1 
		from 	titulo_pagar x,
			titulo_pagar_imposto z,
			tributo t
		where	x.nr_titulo = z.nr_titulo
		and	t.cd_tributo = z.cd_tributo
		and	x.cd_cgc = P.cd_cgc
		and	z.vl_imposto > 0
		and 	t.ie_tipo_tributo = 'COFINS'));
	
	
	
	/*select 	
		nvl(sum(decode(i.vl_base_calculo,null,to_number(obter_dados_tit_pagar(p.nr_titulo,'V')),i.vl_base_calculo)),0),
		nvl(sum(i.vl_imposto),0)
	from 	titulo_pagar p,
		titulo_pagar_imposto i
	where	p.nr_titulo = i.nr_titulo(+)
	and 	trunc(decode(ie_data_w,1,p.dt_contabil,p.dt_liquidacao),'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_cgc = cd_cgc_w
	and	nvl(i.cd_darf,'9999') = cd_darf_w
	and	p.ie_situacao <> 'C'
	and	nvl(to_char(p.dt_contabil,'mm'),'00') = lpad(ie_mes_w,2,'0'); */

cursor C03 is --Pessoa fisica que teve ganho maior que 6000 no ano ou pessoa fisicas com ganho menor que 6000, mais que houve retencao
	select	distinct p.cd_pessoa_fisica,
		nvl(j.cd_darf,'0588')
	from	titulo_pagar p, 
		titulo_pagar_imposto j
	where	p.nr_titulo = j.nr_titulo (+)
	and	trunc(p.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_pessoa_fisica is not null
	and	nvl(j.cd_darf,'0588') in ('0588','3208')
	and	p.ie_tipo_titulo in (select z.ie_tipo_titulo from dirf_regra_tipo_tit z)
	--and	p.ie_tipo_titulo in ('0','11','13','5')
	and	p.vl_titulo >= vl_min_ano_pf_w
	union
	select	distinct a.cd_pessoa_fisica,
		nvl(i.cd_darf,'0588')
	from	titulo_pagar a,
		titulo_pagar_imposto i
	where	a.nr_titulo = i.nr_titulo
	and	trunc(a.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	nvl(i.cd_darf,'0588') in ('0588','3208')
	and	a.ie_tipo_titulo in (select z.ie_tipo_titulo from dirf_regra_tipo_tit z)
	--and	a.ie_tipo_titulo in ('0','11','13','5')
	and	a.cd_pessoa_fisica is not null
	and	a.vl_titulo < nvl(vl_min_ano_pf_w,6000);
	
cursor C04 is -- titulos das pessoas fisicas.
	
	select 	distinct p.nr_titulo
	from 	titulo_pagar p,
		titulo_pagar_imposto i
	where	p.nr_titulo = i.nr_titulo(+)
	and 	trunc(p.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	nvl(i.cd_darf,'0588') = cd_darf_w
	and	nvl(to_char(p.dt_liquidacao,'mm'),'00') = lpad(ie_mes_w,2,'0')
	--and	p.ie_tipo_titulo in ('0','11','13','5')
	and	p.ie_tipo_titulo in (select z.ie_tipo_titulo from dirf_regra_tipo_tit z)
	and	p.ie_situacao <> 'C';
	
	/*select 	
		nvl(sum(decode(i.vl_base_calculo,null,to_number(obter_dados_tit_pagar(p.nr_titulo,'V')),i.vl_base_calculo)),0),
		nvl(sum(i.vl_imposto),0)
	from 	titulo_pagar p,
		titulo_pagar_imposto i
	where	p.nr_titulo = i.nr_titulo(+)
	and 	trunc(p.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	nvl(i.cd_darf,'0588') = cd_darf_w
	and	nvl(to_char(p.dt_liquidacao,'mm'),'00') = lpad(ie_mes_w,2,'0')
	and	p.ie_situacao <> 'C';*/
	
	
	
	
		
cursor c05 is	--GPS
	select	distinct p.cd_pessoa_fisica
	from	dirf_lote a,
		gps d,
		gps_titulo e,
		titulo_pagar p, /* T�tulo  original */
		titulo_pagar f /* T�tulo gerado do imposto */
	where	a.nr_sequencia 	= d.nr_seq_dirf
	and	d.nr_sequencia 	= e.nr_seq_gps 
	and	f.nr_titulo	= e.nr_titulo
	and	f.nr_titulo_original = p.nr_titulo
	and	trunc(f.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	a.nr_sequencia = nr_seq_lote_dirf_p 
	order by p.cd_pessoa_fisica;	

cursor c06 is --Relaciona todos os darf�s utilizados na dirf
select cd_darf
from	(select	distinct 
		nvl(i.cd_darf,'1708') cd_darf
	from	titulo_pagar p,
		titulo_pagar_imposto i
	where	p.nr_titulo = i.nr_titulo
	and	trunc(p.dt_contabil,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_cgc  is not null
	and	nvl(i.cd_darf,'1708') in ('1708','8045','3280')
	and	p.ie_situacao <> 'C' 
	union 
	select	distinct 
		nvl(i.cd_darf,'1708') cd_darf
	from	titulo_pagar p,
		titulo_pagar_imposto i
	where	p.nr_titulo = i.nr_titulo
	and	trunc(p.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_cgc  is not null
	and	nvl(i.cd_darf,'1708') in ('5952','5960','5979','5987')
	and	p.ie_situacao <> 'C' 
	union 
	select	distinct 
		nvl(j.cd_darf,'0588') cd_darf
	from	titulo_pagar p, 
		titulo_pagar_imposto j
	where	p.nr_titulo = j.nr_titulo (+)
	and	trunc(p.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	p.cd_pessoa_fisica is not null
	union 
	select	distinct 
		nvl(i.cd_darf,'0588') cd_darf
	from	titulo_pagar a,
		titulo_pagar_imposto i
	where	a.nr_titulo = i.nr_titulo
	and	trunc(a.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	a.cd_pessoa_fisica is not null)
where	cd_darf in ('5952','5960','5979','5987','0588','1708','8045','3280');


cursor c07 is -- tipo de registro 3 - Totaliza os valores por codigo da darf
select  lpad(replace(nvl(sum(vl_rendimento),'0'),',',''),15,'0'),
	lpad(replace(nvl(sum(vl_imposto),'0'),',',''),15,'0')
from	
	(select	lpad(nvl(sum(p.vl_titulo),0),15,'0' ) vl_rendimento,
		lpad(nvl(sum(i.vl_imposto),0),15,'0') vl_imposto
	from 	titulo_pagar p,
		titulo_pagar_imposto i
	where	p.nr_titulo = i.nr_titulo(+)
	and	trunc(decode(nvl(i.cd_darf,'1708'),'1708',p.dt_contabil,'8045',p.dt_contabil,'3280',p.dt_contabil,p.dt_liquidacao),'YYYY') = trunc(dt_lote_w,'YYYY')
	and	nvl(i.cd_darf,'1708') = cd_darf_w
	and	p.ie_situacao <> 'C'
	and	to_char(decode(nvl(i.cd_darf,'1708'),'1708',p.dt_contabil,'8045',p.dt_contabil,'3280',p.dt_contabil,p.dt_liquidacao),'mm') = lpad(ie_mes_w,2,'0')
	union all
	select 	lpad(nvl(sum(p.vl_titulo),0),15,'0' ) vl_rendimento,
		lpad(nvl(sum(i.vl_imposto),0),15,'0') vl_imposto
	from 	titulo_pagar p,
		titulo_pagar_imposto i
	where	p.nr_titulo = i.nr_titulo(+)
	and 	trunc(p.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	nvl(i.cd_darf,'0588') = cd_darf_w
	and	p.ie_situacao <> 'C'
	and	to_char(p.dt_liquidacao,'mm') = lpad(ie_mes_w,2,'0')
	union all
	select	lpad(0,15,'0' ) vl_rentidimento,
		lpad(nvl(sum(f.vl_titulo),0),15,'0') vl_imposto
	from	dirf_lote a,
		gps d,
		gps_titulo e,
		titulo_pagar p, -- T�tulo  original 
		titulo_pagar f --T�tulo gerado do imposto 
	where	a.nr_sequencia 	= d.nr_seq_dirf
	and	d.nr_sequencia 	= e.nr_seq_gps 
	and	f.nr_titulo	= e.nr_titulo
	and	f.nr_titulo_original = p.nr_titulo
	and	trunc(f.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	a.nr_sequencia = nr_seq_lote_dirf_p 
	and	cd_darf_w = '0588'
	and	to_char(f.dt_liquidacao,'mm') = ie_mes_w);
	
cursor C08 is
	select	nvl(sum(to_number(obter_dados_tit_pagar(f.nr_titulo,'V'))),0)
	from	dirf_lote a,
		gps d,
		gps_titulo e,
		titulo_pagar p, /* T�tulo  original */
		titulo_pagar f /* T�tulo gerado do imposto */
	where	a.nr_sequencia 	= d.nr_seq_dirf
	and	d.nr_sequencia 	= e.nr_seq_gps 
	and	f.nr_titulo	= e.nr_titulo
	and	f.nr_titulo_original = p.nr_titulo
	and	trunc(f.dt_liquidacao,'YYYY') = trunc(dt_lote_w,'YYYY')
	and	a.nr_sequencia = nr_seq_lote_dirf_p 
	and	p.cd_pessoa_fisica = cd_pessoa_fisica_w
	and	to_char(p.dt_liquidacao,'mm') = lpad(ie_mes_w,2,'0');	

begin

nr_seq_arquivo_w := 2;

select 	nvl(dt_lote,sysdate),
	nvl(vl_min_ano_pf,6000)
into	dt_lote_w,
	vl_min_ano_pf_w
from	dirf_lote
where	nr_sequencia = nr_seq_lote_dirf_p;

/* PESSOA JURIDICA */
nr_mes_w := 1;
OPEN c01;
LOOP
FETCH c01 INTO
	cd_cgc_w,
	cd_darf_w,
	ie_data_w;
EXIT WHEN c01%NOTFOUND;
	if (nvl(cd_cgc_w,'X') <> 'X') then
		select 	--lpad(to_char(nr_seq_arquivo_w),8,'0') ||  --nr_seq_arquivo
			'2' ||
			lpad(cd_cgc_estab_p,14,'0') ||
			lpad(cd_darf_w,4,'0') ||
			'2' ||
			lpad(cd_cgc_w,14,'0') ||
			rpad(nvl(substr(elimina_caractere_especial(nfe_obter_dados_pj(cd_cgc_w,'RZ')),1,60),' '),60,' ')
		into	ds_arquivo_w
		FROM	dual;
		
		--insert into bruno_log values  (2000,20000,'cgc: ' || cd_cgc_w || ' - nome: ' || rpad(nvl(substr(nfe_obter_dados_pj(cd_cgc_w,'RZ'),1,60),' '),60,' ') || ' - Arquivo: '  || ds_arquivo_w);
		--commit;
		ie_mes_w := '01';
		ie_entou_w := false;
		
		while to_number(ie_mes_w) <= 13 loop
			OPEN c02;
			LOOP
			FETCH c02 INTO
				vl_rendimento_w,
				vl_irrf_w;				
			EXIT WHEN c02%NOTFOUND;
				ds_arquivo_w := ds_arquivo_w || lpad(replace(campo_mascara(nvl(vl_rendimento_w,'0'),2),'.',''),15,'0') || lpad('0',15,'0') || lpad(replace(campo_mascara(nvl(vl_irrf_w,'0'),2),'.',''),15,'0');
				ie_entou_w := true;
			END LOOP;
			CLOSE c02;	
			
			if (not ie_entou_w) then
				ds_arquivo_w := ds_arquivo_w || lpad('0',15,'0') || lpad('0',15,'0') || lpad('0',15,'0');			
			end if;
			ie_entou_w := false;	
			ie_mes_w := to_char(to_number(ie_mes_w + 1));		
		end loop;
		
		ds_arquivo_w := ds_arquivo_w || '0'; -- identificacao da situacao do rendimento
		ds_arquivo_w := ds_arquivo_w || '0'; --especializacao das deducoes
		ds_arquivo_w := ds_arquivo_w || lpad(' ',8,' '); --para uso do rfb
		ds_arquivo_w := ds_arquivo_w || lpad(' ',32,' '); --para uso do declarante
		ds_arquivo_w := ds_arquivo_w || '9'; --problemas com  o windows (grande porte) 
		nr_seq_arquivo_w := nr_seq_arquivo_w + 1; --incremento de cada linha do arquivo
		
		insert 	into w_dirf_arquivo (nr_sequencia,
					     nm_usuario,
					     nm_usuario_nrec,
					     dt_atualizacao,
					     dt_atualizacao_nrec,
					     ds_arquivo,
					     nr_seq_apresentacao,
					     nr_seq_registro,
					     cd_cgc)
		values			    (w_dirf_arquivo_seq.nextval,
					     nm_usuario_p,
					     nm_usuario_p,
					     sysdate,
					     sysdate,
					     ds_arquivo_w,
					     cd_darf_w,
					     1,
					     cd_cgc_w);	
	end if;
END LOOP;
CLOSE c01;

nr_mes_w := 1;
/*PESSOA FISICA*/
OPEN c03;
LOOP
FETCH c03 INTO
	cd_pessoa_fisica_w,
	cd_darf_w;
EXIT WHEN c03%NOTFOUND;
	if (cd_pessoa_fisica_w is not null) then
		select 	--lpad(to_char(nr_seq_arquivo_w),8,'0') ||  --nr_seq_arquivo
			'2' ||
			lpad(cd_cgc_estab_p,14,'0') ||
			lpad(cd_darf_w,4,'0') ||
			'1' ||
			lpad(nvl(Obter_Cpf_Pessoa_Fisica(cd_pessoa_fisica_w),'0'),14,'0') ||
			rpad(substr(elimina_caractere_especial(obter_nome_pf(cd_pessoa_fisica_w)),1,60),60,' ')
		into	ds_arquivo_w
		from	dual;
		--insert into bruno_log values  (2000,20000,'cpf:'  || lpad(nvl(Obter_Cpf_Pessoa_Fisica(cd_pessoa_fisica_w),'0'),14,'0')  || ' - nome: ' || rpad(substr(obter_nome_pf(cd_pessoa_fisica_w),1,60),60,' ') || ' - Arquivo: '  || ds_arquivo_w);
		--commit;
		ie_mes_w := '01';
		ie_entou_w := false;
		
		while to_number(ie_mes_w) <= 13 loop
			vl_rendimento_w := 0;
			vl_irrf_w := 0;
			OPEN c04;
			LOOP
			FETCH c04 INTO
				nr_titulo_w;	
			EXIT WHEN c04%NOTFOUND;
				vl_rendimento_w := vl_rendimento_w + nvl(obter_valor_rendimento_dirf_pf(nr_titulo_w,'N'),0);
				vl_irrf_w := vl_irrf_w + nvl(obter_valor_imposto_pf(nr_titulo_w,'IR'),0);
				ie_entou_w := true;
			END LOOP;
			CLOSE c04;
		
			if (not ie_entou_w) then
				ds_arquivo_w := ds_arquivo_w || lpad('0',15,'0') || lpad('0',15,'0') || lpad('0',15,'0');			
			else
				ds_arquivo_w := ds_arquivo_w || lpad(replace(campo_mascara(nvl(vl_rendimento_w,'0'),2),'.',''),15,'0') || lpad('0',15,'0') || lpad(replace(campo_mascara(nvl(vl_irrf_w,'0'),2),'.',''),15,'0');
			end if;
			
			ie_entou_w := false;	
			ie_mes_w := to_char(to_number(ie_mes_w + 1));
		end loop;
		
		ds_arquivo_w := ds_arquivo_w || '0'; -- identificacao da situacao do rendimento
		ds_arquivo_w := ds_arquivo_w || '0'; --especializacao das deducoes
		ds_arquivo_w := ds_arquivo_w || lpad(' ',8,' '); --para uso do rfb
		ds_arquivo_w := ds_arquivo_w || lpad(' ',32,' '); --para uso do declarante
		ds_arquivo_w := ds_arquivo_w || '9'; --problemas com  o windows (grande porte) 
		nr_seq_arquivo_w := nr_seq_arquivo_w + 1; --incremento de cada linha do arquivo
		
		insert 	into w_dirf_arquivo (nr_sequencia,
				     nm_usuario,
				     nm_usuario_nrec,
				     dt_atualizacao,
				     dt_atualizacao_nrec,
				     ds_arquivo,
				     nr_seq_apresentacao,
				     nr_seq_registro,
				     cd_pessoa_fisica)
		values		    (w_dirf_arquivo_seq.nextval,
				     nm_usuario_p,
				     nm_usuario_p,
				     sysdate,
				     sysdate,
				     ds_arquivo_w,
				     '0588',
				     1,
				     cd_pessoa_fisica_w);
	end if;
END LOOP;
CLOSE c03;


OPEN c05;
LOOP
FETCH c05 INTO
--	ie_mes_w,
--	cd_darf_w,
--	vl_rendimento_w,
--	vl_irrf_w,
	cd_pessoa_fisica_w;	
EXIT WHEN c05%NOTFOUND;
	if (cd_pessoa_fisica_w is not null) then
	
		select 	--lpad(to_char(nr_seq_arquivo_w),8,'0') ||  --nr_seq_arquivo
			'2' ||
			lpad(cd_cgc_estab_p,14,'0') ||
			'0588' ||
			'1' ||
			lpad(nvl(Obter_Cpf_Pessoa_Fisica(cd_pessoa_fisica_w),'0'),14,'0') ||
			rpad(substr(elimina_caractere_especial(obter_nome_pf(cd_pessoa_fisica_w)),1,60),60,' ')
		into	ds_arquivo_w
		from	dual;
		
		ie_mes_w := '01';
		ie_entou_w := false;
		
		while to_number(ie_mes_w) <= 13 loop
			OPEN c08;
			LOOP
			FETCH c08 INTO
				vl_rendimento_w;
			EXIT WHEN c08%NOTFOUND;
				vl_irrf_w := 0;
				ds_arquivo_w := ds_arquivo_w || lpad(replace(campo_mascara(nvl(vl_rendimento_w,'0'),2),'.',''),15,'0') || lpad('0',15,'0') || lpad(replace(campo_mascara(nvl(vl_irrf_w,'0'),2),'.',''),15,'0');
				ie_entou_w := true;
			END LOOP;
			CLOSE c08;
		
			if (not ie_entou_w) then
				ds_arquivo_w := ds_arquivo_w || lpad('0',15,'0') || lpad('0',15,'0') || lpad('0',15,'0');			
			end if;
			ie_entou_w := false;	
			ie_mes_w := to_char(to_number(ie_mes_w + 1));
		end loop;
		
		ds_arquivo_w := ds_arquivo_w || '0'; -- identificacao da situacao do rendimento
		ds_arquivo_w := ds_arquivo_w || '1'; --especializacao das deducoes
		ds_arquivo_w := ds_arquivo_w || lpad(' ',8,' '); --para uso do rfb
		ds_arquivo_w := ds_arquivo_w || lpad(' ',32,' '); --para uso do declarante
		ds_arquivo_w := ds_arquivo_w || '9'; --problemas com  o windows (grande porte) 
		nr_seq_arquivo_w := nr_seq_arquivo_w + 1; --incremento de cada linha do arquivo
		
		insert 	into w_dirf_arquivo (nr_sequencia,
					     nm_usuario,
					     nm_usuario_nrec,
					     dt_atualizacao,
					     dt_atualizacao_nrec,
					     ds_arquivo,
					     nr_seq_apresentacao,
					     nr_seq_registro,
					     cd_pessoa_fisica)
		values		    	    (w_dirf_arquivo_seq.nextval,
					     nm_usuario_p,
					     nm_usuario_p,
					     sysdate,
					     sysdate,
					     ds_arquivo_w,
					     '0588',
					     1,
					     cd_pessoa_fisica_w);		
	end if;

END LOOP;
CLOSE c05;


--Registro 03
OPEN c06;
LOOP
FETCH c06 INTO
	cd_darf_w;
EXIT WHEN c06%NOTFOUND;
	ds_arquivo_w	:= 	--lpad(to_char(nr_seq_arquivo_w),8,'0') || 
				'3' ||
				lpad(nvl(cd_cgc_estab_p,'0'),14,' ') ||
				lpad(cd_darf_w,4,'0') ||
				lpad('1',8,'0') ||
				lpad(' ',67,' ');
				
	ie_mes_w := '01';
	ie_entou_w := false;
	
	while to_number(ie_mes_w) <= 13 loop				
		OPEN c07;
		LOOP
		FETCH c07 INTO
			vl_rendimento_w,
			vl_irrf_w;
		EXIT WHEN c07%NOTFOUND;
			ds_arquivo_w := ds_arquivo_w ||lpad(nvl(vl_rendimento_w,'0'),15,'0') || lpad('0',15,'0') || lpad(nvl(vl_irrf_w,'0'),15,'0');
			ie_entou_w := true;
		END LOOP;
		CLOSE c07;
		
		if (not ie_entou_w) then
			ds_arquivo_w := ds_arquivo_w || lpad('0',15,'0') || lpad('0',15,'0') || lpad('0',15,'0');			
		end if;
		ie_entou_w := false;	
		ie_mes_w := to_char(to_number(ie_mes_w + 1));		
	end loop;
	
	ds_arquivo_w := ds_arquivo_w || lpad(' ',30,' ');
	ds_arquivo_w := ds_arquivo_w || lpad(' ',12,' '); 
	ds_arquivo_w := ds_arquivo_w || '9'; --problemas com  o windows (grande porte) 
	nr_seq_arquivo_w := nr_seq_arquivo_w + 1; --incremento de cada linha do arquivo
	
	insert 	into w_dirf_arquivo (nr_sequencia,
				     nm_usuario,
				     nm_usuario_nrec,
				     dt_atualizacao,
				     dt_atualizacao_nrec,
				     ds_arquivo,
				     nr_seq_apresentacao,
				     nr_seq_registro)
	values		    	    (w_dirf_arquivo_seq.nextval,
				     nm_usuario_p,
				     nm_usuario_p,
				     sysdate,
				     sysdate,
				     ds_arquivo_w,
				     lpad(cd_darf_w,4,'0'),
				     2);
END LOOP;
CLOSE c06;

--nr_seq_arquivo_p := nr_seq_arquivo_w;

commit;

end M1_DIRF_REGISTRO_TIPO_2_3;
/
