create or replace
procedure atualiza_dominio_regulacao(	cd_dominio_p	Number,
					vl_dominio_p	Varchar2,
					ds_valor_dominio_p varchar2) is 

vl_dominio_w		Varchar2(15) := '';
ds_valor_dominio_w	Varchar2(255);
qt_registro_w		Number(3);
cd_expressao_w		dic_expressao.cd_expressao%type;
cd_exp_valor_dominio_w	dic_expressao.cd_expressao%type;

begin
if 	(nvl(cd_dominio_p, 0) > 0) then

	select	count(*)
	into	qt_registro_w
	from	valor_dominio
	where	cd_dominio = cd_dominio_p
	and	vl_dominio = vl_dominio_p;

	if	(qt_registro_w = 0) then

		select	max(cd_expressao)
		into	cd_expressao_w
		from	dic_expressao
		where	ds_expressao_br = ds_valor_dominio_p;
		
		if	(cd_expressao_w is null) then		
			cd_expressao_w := obter_menor_codigo_expressao;
			
			insert	into dic_expressao (
				cd_expressao,
				nm_usuario,
				dt_atualizacao,
				ds_expressao_br, 
				ds_glossario)
			values (cd_expressao_w,
				'Regulacao',
				sysdate,
				ds_valor_dominio_p, 
				ds_valor_dominio_p);
			
		end if;		
	
		insert  into valor_dominio (
				cd_dominio,
				vl_dominio,
				ds_valor_dominio,
				dt_atualizacao,
				nm_usuario,
				ie_situacao,
				cd_exp_valor_dominio
		) values (
				cd_dominio_p,
				vl_dominio_p,
				ds_valor_dominio_p,
				sysdate,
				'Regulacao',
				'A',
				cd_expressao_w
		);

	else
		select	max(ds_valor_dominio),
			max(cd_exp_valor_dominio)
		into	ds_valor_dominio_w,
			cd_exp_valor_dominio_w
		from	valor_dominio
		where	cd_dominio = cd_dominio_p
		and	vl_dominio = vl_dominio_p;

		if	(ds_valor_dominio_p <> ds_valor_dominio_w) then

			if	(cd_exp_valor_dominio_w is null) then
				select	max(cd_expressao)
				into	cd_expressao_w
				from	dic_expressao
				where	ds_expressao_br = ds_valor_dominio_p;
				
				if	(cd_expressao_w is null) then			
					cd_expressao_w := obter_menor_codigo_expressao;
				end if;
			end if;
			
			update	valor_dominio
			set	ds_valor_dominio = ds_valor_dominio_p,
				cd_exp_valor_dominio = nvl(cd_exp_valor_dominio, cd_expressao_w)
			where	cd_dominio = cd_dominio_p
			and	vl_dominio = vl_dominio_p;
		end if;
	end if;

end if;

commit;

end atualiza_dominio_regulacao;
/
