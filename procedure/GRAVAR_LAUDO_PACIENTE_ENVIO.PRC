CREATE OR REPLACE
PROCEDURE gravar_laudo_paciente_envio(	nr_seq_laudo_p		number,
					ie_medico_paciente_p	varchar2,
					ds_observacao_p		varchar2,
					nm_usuario_p		varchar2,
					ds_email_envio_p	varchar2) is
nr_sequencia_w	number(10);

begin

select	laudo_paciente_envio_seq.nextval
into	nr_sequencia_w
from	dual;
		
insert into laudo_paciente_envio (
	nr_sequencia,
	nr_seq_laudo,
	dt_atualizacao,
	nm_usuario,
	ie_medico_paciente,
	ds_observacao,
	ds_email_envio)
Values	(nr_sequencia_w,
	nr_seq_laudo_p,
	sysdate,
	nm_usuario_p,
	substr(ie_medico_paciente_p,1,1),
	substr(ds_observacao_p,1,2000),
	substr(ds_email_envio_p,1,2000));

commit;

END gravar_laudo_paciente_envio;
/