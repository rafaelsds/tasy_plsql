create or replace 
procedure inativar_pf_privacidade(    cd_pessoa_fisica_p pessoa_fisica.cd_pessoa_fisica%type, 
                    			nm_usuario_p varchar2) is
begin

update pessoa_fisica_privacidade
set nm_usuario         = nm_usuario_p,
    dt_atualizacao         = sysdate,
    ie_situacao         = 'I',
    dt_inativacao         = sysdate 
where     cd_pessoa_fisica     = cd_pessoa_fisica_p
and     ie_situacao         = 'A'
and     dt_liberacao is not null
and     dt_inativacao is null;

commit;

end inativar_pf_privacidade;
/ 