create or replace
procedure remover_item_estoque_cabine(
		ie_opcao_p		varchar2,
		nr_seq_cabine_p		number,
		nr_seq_estoque_cabine_p	number) is 

/*	ie_opcao_p
	I - item
	E - estoque
	*/

begin

if	(ie_opcao_p = 'I') then
	begin
	delete	far_estoque_cabine
	where	nr_seq_cabine	= nr_seq_cabine_p
	and	nr_sequencia	= nr_seq_estoque_cabine_p;
	end;

elsif	(ie_opcao_p = 'E') then
	begin
	delete	far_estoque_cabine
	where	nr_seq_cabine	= nr_seq_cabine_p;
	end;
end if;

commit;

end remover_item_estoque_cabine;
/