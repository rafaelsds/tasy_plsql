create or replace
procedure atualizar_dt_solic_compra(	nr_seq_solic_compra_p 	number,
					nm_usuario_p		Varchar2) is 

begin

update 	solic_compra 
set 	dt_impressao 	= sysdate,
	nm_usuario 	= nm_usuario_p
where 	nr_solic_compra = nr_seq_solic_compra_p;

commit;

end atualizar_dt_solic_compra;
/