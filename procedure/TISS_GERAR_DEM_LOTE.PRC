create or replace 
procedure tiss_gerar_dem_lote(	nr_seq_fatura_p		number,
				nr_lote_p		varchar2,
				dt_envio_lote_p		date,
				nr_protocolo_p		number,
				vl_protocolo_p		number,
				vl_glosa_protocolo_p	number,
				cd_glosa_protocolo_p	varchar2,
				nm_usuario_p		Varchar2,
				nr_seq_lote_p	out	number) is 

nr_sequencia_w		number(10);	
nr_seq_retorno_w	convenio_retorno.nr_sequencia%type;
nr_seq_lote_hist_w	lote_audit_hist.nr_sequencia%type;
cd_convenio_w		convenio.cd_convenio%type;

begin

select	tiss_dem_lote_seq.nextval
into	nr_sequencia_w
from	dual;


select	max(a.nr_seq_retorno),
	max(a.nr_seq_lote_hist)
into	nr_seq_retorno_w,
	nr_seq_lote_hist_w
from	tiss_cabecalho a,
	tiss_demonstrativo b,
	TISS_DEM_FATURA c
where	a.nr_sequencia = b.nr_seq_cabecalho
and	b.nr_sequencia = c.nr_seq_demonstrativo	
and	c.nr_sequencia = nr_seq_fatura_p;

if	(nr_seq_retorno_w is not null) then
	select	max(cd_convenio)
	into	cd_convenio_W
	from	convenio_retorno
	where	nr_sequencia = nr_Seq_retorno_w;
elsif	(nr_seq_lote_hist_w is not null) then
	select	max(a.cd_convenio)
	into	cd_convenio_W
	from	lote_audit_hist b,
		lote_auditoria a
	where	a.nr_sequencia = b.nr_seq_lote_audit
	and	b.nr_sequencia = nr_seq_lote_hist_w;
end if;



insert into tiss_dem_lote (	
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_seq_fatura,
	nr_lote,
	dt_envio_lote,
	nr_protocolo,
	vl_protocolo,
	vl_glosa_protocolo,
	cd_glosa_protocolo,
	nr_seq_protocolo_conv)
values(	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	nr_seq_fatura_p,
	nr_lote_p,
	dt_envio_lote_p,
	nr_protocolo_p,
	vl_protocolo_p,
	vl_glosa_protocolo_p,
	cd_glosa_protocolo_p,
	Tiss_Obter_Seq_Protocolo_Xml(cd_convenio_w,nr_lote_p));

nr_seq_lote_p	:= nr_sequencia_w;	

commit;

end tiss_gerar_dem_lote;
/