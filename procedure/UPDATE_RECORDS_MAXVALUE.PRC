create or replace procedure update_records_maxvalue
is
max_count_w number(5);

begin

select	max(nr_sequencia)
into	max_count_w
from	report_downloaded_count;

update	patient_former_names
set		nr_report_sequence = max_count_w
where 	nr_report_sequence is null;

update	clinical_diagosis_detail
set		nr_report_sequence = max_count_w
where 	nr_report_sequence is null;

update	cancer_details
set 	nr_report_sequence = max_count_w
where 	nr_report_sequence is null;

update	header_detail
set 	nr_report_sequence = max_count_w
where 	nr_report_sequence is null;

update	cancer_admission_details
set 	nr_report_sequence = max_count_w
where 	nr_report_sequence is null;

end update_records_maxvalue;
/