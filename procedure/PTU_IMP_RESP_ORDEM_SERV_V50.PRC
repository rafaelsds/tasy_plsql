create or replace 
procedure ptu_imp_resp_ordem_serv_v50
			(	ds_arquivo_p		Varchar2,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Importar resposta ordem serv_v40

Rotina utilizada nas transa��es ptu via scs homologadas com a unimed brasil.
quando for alterar, favor verificar com o an�lista respons�vel para a realiza��o de testes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
Performance
---------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
cd_transacao_w			Varchar2(5);
ie_tipo_cliente_w		Varchar2(15);
cd_unimed_exec_w		Number(4);
cd_unimed_benef_w		Number(4);
nr_seq_execucao_w		Number(10);
nr_seq_origem_w			Number(10);
cd_unimed_w			Number(4);
cd_usuario_plano_w		Number(13);
nm_prestador_w			Varchar2(25);
nr_seq_guia_w			Number(10);
nr_seq_requisicao_w		Number(10);
nr_seq_resposta_w		Number(10);
ds_observacao_w			Varchar2(999);
ie_tipo_tabela_w		Varchar2(2);
cd_servico_w			Number(8);
qt_autorizada_w			Varchar2(8);
qt_autorizada_ww		number(7,3);
ie_status_req_w			Number(2);
cd_mens_erro_1			Number(4);
cd_mens_erro_2			Number(4);
cd_mens_erro_3			Number(4);
cd_mens_erro_4			Number(4);
cd_mens_erro_5			Number(4);
nr_seq_transacao_w		Number(10);
ie_tipo_transacao_w		Varchar2(2);
qt_registro_w			Number(2);
qt_registro_ww			Number(2);
nr_seq_importacao_w		Number(10);
ds_conteudo_w			Varchar2(4000);
nr_seq_material_w		Number(10);
nr_seq_procedimento_w		Number(10);
cd_servico_consersao_w		Number(10);
nr_versao_w			Varchar2(3);
ds_mens_erro_w			Varchar2(999);
cd_unimed_solic_w		Number(4);
nr_seq_resp_req_ser_w		Number(10);
qt_registros_w			pls_integer;
qt_registros_aprov_w		pls_integer;
qt_registros_neg_w		pls_integer;

cursor c01 is
	select	nr_seq_importacao,
		ds_valores
	from	w_scs_importacao
	where	nm_usuario	= nm_usuario_p
	order by nr_seq_importacao;

begin

open c01;
loop
fetch c01 into
	nr_seq_importacao_w,
	ds_conteudo_w;
exit when c01%notfound;
	begin
	if	(substr(ds_conteudo_w,1,4)	<> 'FIM$') then
		if	(nr_seq_importacao_w	= 1) then
			cd_transacao_w		:= substr(ds_conteudo_w,1,5);
			ie_tipo_cliente_w	:= trim(substr(ds_conteudo_w,6,15));
			cd_unimed_benef_w	:= to_number(substr(ds_conteudo_w,21,4));
			cd_unimed_exec_w	:= to_number(substr(ds_conteudo_w,25,4));
			cd_unimed_solic_w	:= to_number(substr(ds_conteudo_w,29,4));
			nr_seq_origem_w		:= to_number(substr(ds_conteudo_w,33,10));
			nr_seq_execucao_w	:= to_number(substr(ds_conteudo_w,43,10));
			cd_unimed_w		:= to_number(substr(ds_conteudo_w,53,4));
			cd_usuario_plano_w	:= trim(substr(ds_conteudo_w,57,13));
			nm_prestador_w		:= trim(substr(ds_conteudo_w,70,25));
			nr_versao_w		:= trim(substr(ds_conteudo_w,95,3));

			select	count(1)
			into	qt_registros_w
			from	ptu_requisicao_ordem_serv
			where	nr_transacao_solicitante	= nr_seq_origem_w
			and	cd_unimed_solicitante		= cd_unimed_solic_w;

			if	(qt_registros_w	> 0) then
				select	ptu_resposta_req_ord_serv_seq.NextVal
				into	nr_seq_resposta_w
				from	dual;

				if	(ie_tipo_cliente_w	= 'UNIMED') then
					ie_tipo_cliente_w	:= 'U';
				elsif	(ie_tipo_cliente_w	= 'PORTAL') then
					ie_tipo_cliente_w	:= 'P';
				elsif	(ie_tipo_cliente_w	= 'PRESTADOR') then
					ie_tipo_cliente_w	:= 'R';
				end if;

				insert	into ptu_resposta_req_ord_serv
					(nr_sequencia, cd_transacao, ie_tipo_cliente,
					 cd_unimed_executora, cd_unimed_beneficiario, nr_seq_execucao,
					 cd_unimed, cd_usuario_plano, nr_seq_origem,
					 nm_prest_alto_custo, nr_seq_requisicao, nr_seq_guia,
					 nm_usuario, dt_atualizacao, ds_arquivo_pedido,
					 nm_usuario_nrec, dt_atualizacao_nrec, nr_versao,
					 cd_unimed_solicitante)
				values	(nr_seq_resposta_w, cd_transacao_w, ie_tipo_cliente_w,
					 cd_unimed_exec_w, cd_unimed_benef_w, nr_seq_execucao_w,
					 cd_unimed_w, cd_usuario_plano_w, nr_seq_origem_w,
					 nm_prestador_w, nr_seq_requisicao_w, nr_seq_guia_w,
					 nm_usuario_p, sysdate, ds_arquivo_p,
					 nm_usuario_p, sysdate, nr_versao_w,
					 cd_unimed_solic_w);
			end if;
		elsif	(nr_seq_importacao_w	= 2) then
			select	trim(substr(ds_conteudo_w,1,999))
				into	ds_observacao_w
				from	dual;

				update	ptu_requisicao_ordem_serv
				set	ds_observacao	= ds_observacao_w
				where	nr_sequencia	= nr_seq_resposta_w;
		elsif	(nr_seq_importacao_w	> 2)  then
			ie_tipo_tabela_w	:= substr(ds_conteudo_w,1,1);
			cd_servico_w		:= to_number(substr(ds_conteudo_w,2,8));
			qt_autorizada_w		:= substr(ds_conteudo_w,10,8);
			ie_status_req_w		:= to_number(substr(ds_conteudo_w,18,1));
			cd_mens_erro_1		:= to_number(substr(ds_conteudo_w,19,4));
			cd_mens_erro_2		:= to_number(substr(ds_conteudo_w,23,4));
			cd_mens_erro_3		:= to_number(substr(ds_conteudo_w,27,4));
			cd_mens_erro_4		:= to_number(substr(ds_conteudo_w,31,4));
			cd_mens_erro_5		:= to_number(substr(ds_conteudo_w,35,4));
			ds_mens_erro_w		:= substr(ds_conteudo_w,39,300);
      
			begin
				qt_autorizada_ww  := to_number(ptu_obter_qtd_env_itens_scs(null, qt_autorizada_w, 'R'));
			exception
			when others then
				qt_autorizada_ww   := to_number(replace(ptu_obter_qtd_env_itens_scs(null, qt_autorizada_w, 'R'),',','.'));
			end;

			select	ptu_resposta_req_servico_seq.NextVal
			into	nr_seq_resp_req_ser_w
			from	dual;
			
			insert	into ptu_resposta_req_servico
				(nr_sequencia, nr_seq_resp_req_ord, cd_servico,
				 qt_servico_aut, ie_status_requisicao, ie_tipo_tabela,
				 nm_usuario, dt_atualizacao, nm_usuario_nrec,
				 dt_atualizacao_nrec)
			values	(nr_seq_resp_req_ser_w, nr_seq_resposta_w, cd_servico_w,
				 qt_autorizada_ww, ie_status_req_w, ie_tipo_tabela_w,
				 nm_usuario_p, sysdate, nm_usuario_p,
				 sysdate);

			if	(ie_tipo_tabela_w	in ('0','1','4')) then
				if	(cd_mens_erro_1	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_1,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
				end if;

				if	(cd_mens_erro_2	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_2,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
				end if;

				if	(cd_mens_erro_3	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_3,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
				end if;

				if	(cd_mens_erro_4	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_4,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
				end if;

				if	(cd_mens_erro_5	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_5,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, nr_seq_resp_req_ser_w, null, null, nm_usuario_p);
				end if;
			elsif	(ie_tipo_tabela_w	in ('2','3')) then
				if	(cd_mens_erro_1	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_1,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
				end if;
				
				if	(cd_mens_erro_2	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_2,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
				end if;
				
				if	(cd_mens_erro_3	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_3,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
				end if;
				
				if	(cd_mens_erro_4	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_4,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
				end if;
				
				if	(cd_mens_erro_5	<> 0) then
					ptu_inserir_inconsistencia(	null, null, cd_mens_erro_5,ds_mens_erro_w,cd_estabelecimento_p, nr_seq_resposta_w, ie_tipo_transacao_w,
									cd_transacao_w, null, nr_seq_resp_req_ser_w, null, nm_usuario_p);
				end if;
			end if;
		end if;
	end if;
	end;
end loop;
close c01;

select	count(1)
into	qt_registros_neg_w
from	ptu_resposta_req_servico
where	nr_seq_resp_req_ord	= nr_seq_resposta_w
and	ie_status_requisicao	= 1;

select	count(1)
into	qt_registros_aprov_w
from	ptu_resposta_req_servico
where	nr_seq_resp_req_ord	= nr_seq_resposta_w
and	ie_status_requisicao	= 2;

-- Se a operadora de origem do benefici�rio for diferente da operadora solicitante da ordem de servi�o, � caracterizada uma Triangula��o
if	(cd_unimed_benef_w	<> cd_unimed_solic_w) then
	if	(qt_registros_neg_w	> 0)	and (qt_registros_aprov_w	= 0) then
		-- Se a resposta da ordem de servi�o vier totalmente recusada, se encerra o processo
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 4
		where	nr_transacao_solicitante	= nr_seq_origem_w
		and	cd_unimed_solicitante		= cd_unimed_solic_w;
	else
		-- Se pelo menos um item for aceito, a ordem de servi�o fica aguardando a transa��o de autoriza��o
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 2
		where	nr_transacao_solicitante	= nr_seq_origem_w
		and	cd_unimed_solicitante		= cd_unimed_solic_w;
	end if;
-- Se a operadora de origem do benefici�rio for igual a operadora solicitante da ordem de servi�o, � caracterizada uma transa��o ponto-a-ponto
elsif	(cd_unimed_benef_w	= cd_unimed_solic_w) then
	if	(qt_registros_neg_w	> 0)	and (qt_registros_aprov_w	= 0) then
		-- Se a resposta da ordem de servi�o vier totalmente recusada, se encerra o processo
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 4
		where	nr_transacao_solicitante	= nr_seq_origem_w
		and	cd_unimed_solicitante		= cd_unimed_solic_w;
	elsif	(qt_registros_neg_w	> 0)	and (qt_registros_aprov_w	> 0) then
		-- Se a resposta da ordem de servi�o vier com servi�os recusados e aceitos, a ordem de servi�o fica Parcialmente aceita
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 5
		where	nr_transacao_solicitante	= nr_seq_origem_w
		and	cd_unimed_solicitante		= cd_unimed_solic_w;
	elsif	(qt_registros_neg_w	= 0)	and (qt_registros_aprov_w	> 0) then
		-- Se a resposta da ordem de servi�o vier totalmente aceita, o est�gio da ordem de servi�o fica Aceita e se encerra o processo
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 3
		where	nr_transacao_solicitante	= nr_seq_origem_w
		and	cd_unimed_solicitante		= cd_unimed_solic_w;
	end if;	
end if;

commit;

end ptu_imp_resp_ordem_serv_v50;
/