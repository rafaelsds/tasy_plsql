create or replace
procedure	des_consiste_dados_adm_cliente(
			nr_sequencia_p			number,
			nm_usuario_p			varchar2,
			ie_cliente_ok_p		out	varchar2) is

cd_cnpj_cli_w		varchar2(14);
qt_existe_w		number(10);
nm_usuario_fin_w	varchar2(15);
ie_cliente_ok_w		varchar2(1);
nm_usuario_comunic_w	varchar2(2000);
ie_erro_w		varchar2(1) := 'N';
ds_titulo_w		varchar2(255);
ds_comunic_w		varchar2(255);

cursor	c01 is
select	nm_usuario
from	usuario
where	cd_setor_atendimento = 1
and	ie_situacao = 'A';

begin

ie_cliente_ok_w	:= 'S';

select	b.cd_cnpj
into	cd_cnpj_cli_w
from	man_localizacao b,
	man_ordem_servico a
where	a.nr_seq_localizacao	= b.nr_sequencia
and	a.nr_sequencia		= nr_sequencia_p;

select	count(*)
into	qt_existe_w
from	titulo_receber
where	cd_cgc = cd_cnpj_cli_w
and	dt_pagamento_previsto <= trunc(sysdate - 30) 
and	ie_situacao = '1';

if	(qt_existe_w > 0) then
	begin

	nm_usuario_comunic_w	:= '';

	open c01;
	loop
	fetch c01 into
		nm_usuario_fin_w;
	exit when c01%notfound;
		begin

		select	count(*)
		into	qt_existe_w
		from	man_ordem_servico_exec
		where	nr_seq_ordem = nr_sequencia_p
		and	nm_usuario_exec = nm_usuario_fin_w;

		if	(qt_existe_w = 0) then
			begin

			insert into man_ordem_servico_exec(
					nr_sequencia,
					nr_seq_ordem,
					dt_atualizacao,
					nm_usuario,
					nm_usuario_exec,
					qt_min_prev,
					nr_seq_tipo_exec,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values(	man_ordem_servico_exec_seq.nextval,
					nr_sequencia_p,
					sysdate,
					nm_usuario_p,
					nm_usuario_fin_w,
					45,
					'3',
					sysdate,
					nm_usuario_p);

			nm_usuario_comunic_w	:= nm_usuario_comunic_w || nm_usuario_fin_w || ', ';

			end;
		else
			begin
			
			update	man_ordem_servico_exec
			set	dt_fim_execucao = null
			where	nr_seq_ordem = nr_sequencia_p
			and	nm_usuario_exec = nm_usuario_fin_w;

			nm_usuario_comunic_w	:= nm_usuario_comunic_w || nm_usuario_fin_w || ', ';

			end;
		end if;

		end;
	end loop;
	close c01;

	ds_titulo_w	:= 'OS remetida ao setor Financeiro';
	ds_comunic_w	:= 'Informamos que a OS ' || to_char(nr_sequencia_p) || ' foi remetida ao setor de Financeiro';

	if	(nvl(nm_usuario_comunic_w,'X') <> 'X') then
		
		Gerar_Comunic_Padrao(
			sysdate,
			ds_titulo_w,
			ds_comunic_w,
			nm_usuario_p,
			'N',
			nm_usuario_comunic_w,
			'N',
			5,
			null,
			null,
			null,
			sysdate,
			null,
			null);

		Insert into man_ordem_serv_envio(
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			dt_envio,
			ie_tipo_envio,
			ds_destino,
			ds_observacao)
		values(	man_ordem_serv_envio_seq.nextval,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			'I',
			substr(nm_usuario_comunic_w,1,255),
			ds_titulo_w);

	end if;

	begin
	Enviar_Email(ds_titulo_w, ds_comunic_w, null, 'financeiropci@philips.com', nm_usuario_p,'M');
	exception when others then
		ie_erro_w	:= 'S';
	end;

	if	(ie_erro_w = 'N') then

		Insert into man_ordem_serv_envio(
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			dt_envio,
			ie_tipo_envio,
			ds_destino,
			ds_observacao)
		values(	man_ordem_serv_envio_seq.nextval,
			nr_sequencia_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			'E',
			'financeiropci@philips.com',
			ds_titulo_w);
		
	end if;		

	update	man_ordem_servico_exec
	set	dt_fim_execucao 	= sysdate
	where	nr_seq_ordem 		= nr_sequencia_p
	and	nm_usuario_exec 	= nm_usuario_p
	and	dt_fim_execucao is null;

	ie_cliente_ok_w	:= 'N';

	end;
end if;

commit;

ie_cliente_ok_p	:= ie_cliente_ok_w;

end des_consiste_dados_adm_cliente;
/