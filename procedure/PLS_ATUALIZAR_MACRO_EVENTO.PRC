create or replace
procedure pls_atualizar_macro_evento
			(	nr_seq_pessoa_evento_p	Number,
				nm_usuario_p		Varchar2) is 
				
ds_cabecalho_w			Varchar2(4000);
nm_prestador_exec_w		Varchar2(80);

begin

select	pls_obter_dados_prestador(nr_seq_prestador_exec,'N')
into	nm_prestador_exec_w
from	pls_evento_pessoa_portal
where	nr_sequencia	= nr_seq_pessoa_evento_p;

ds_cabecalho_w	:= ds_cabecalho_w || ' ' || pls_substituir_macro_evento(nr_seq_pessoa_evento_p,'@prestador_executor',nm_prestador_exec_w);

update	pls_evento_pessoa_portal
set	ds_cabecalho	= ds_cabecalho_w
where	nr_sequencia	= nr_seq_pessoa_evento_p;

commit;

end pls_atualizar_macro_evento;
/