create or replace
procedure autorizar_agenda_cirurgia(	nr_seq_agenda_p in number,					
					nm_usuario_p in varchar2) is

nr_seq_autorizacao_w 		autorizacao_convenio.nr_sequencia%type;
nr_seq_item_autor_w		material_autorizado.nr_sequencia%type;
nr_seq_agenda_pac_opme_w	agenda_pac_opme.nr_sequencia%type;
cd_material_w			agenda_pac_opme.cd_material%type;
qt_material_w			agenda_pac_opme.qt_material%type;
vl_unitario_item_w		agenda_pac_opme.vl_unitario_item%type;
vl_unitario_atualizado_w	agenda_pac_opme.vl_unitario_atualizado%type;
vl_custo_w			agenda_pac_opme.vl_custo%type;
cd_cnpj_w			agenda_pac_opme.cd_cgc%type;
cd_estabelecimento_w 		agenda.cd_estabelecimento%type;
qt_cotacoes_pendentes_w	    	number(5) := -1;
qt_autorizacao_w		number(5);
ds_retorno_atualizar_preco_w	varchar2(255);

cursor c01 is
	select	nr_sequencia,
		cd_material,
		qt_material,
		vl_unitario_item,
		vl_unitario_atualizado,
		cd_cgc
	into 	nr_seq_agenda_pac_opme_w,
		cd_material_w,
		qt_material_w,
		vl_unitario_item_w,
		vl_unitario_atualizado_w,
		cd_cnpj_w
	from 	agenda_pac_opme
	where 	nr_seq_agenda = nr_seq_agenda_p
	and 	ie_status_cotacao = 'A';
		
begin

select	count(1)
into 	qt_cotacoes_pendentes_w
from 	agenda_pac_opme
where 	nr_sequencia = nr_seq_agenda_p
and 	ie_status_cotacao = 'P';

if	(qt_cotacoes_pendentes_w = 0) then

	open c01;
	loop
	fetch c01 into
		nr_seq_agenda_pac_opme_w,
		cd_material_w,
		qt_material_w,
		vl_unitario_item_w,
		vl_unitario_atualizado_w,
		cd_cnpj_w;
	exit when c01%notfound;
		begin
		select	count(*)
		into	qt_autorizacao_w
		from	autorizacao_convenio a
		where	a.nr_seq_agenda = nr_seq_agenda_p
		and 	not exists(select	1
				from	estagio_autorizacao x
				where	x.nr_sequencia = a.nr_seq_estagio
				and	x.ie_interno in (10,70,90));

		if	(qt_autorizacao_w > 0) then
		
			select	max(nr_sequencia)
			into	nr_seq_autorizacao_w
			from	autorizacao_convenio a
			where	a.nr_seq_agenda = nr_seq_agenda_p
			and not exists(	select	1
					from	estagio_autorizacao x
					where	x.nr_sequencia = a.nr_seq_estagio
					and	x.ie_interno in (10,70,90))
			and exists(	select	1
					from	material_autorizado z
					where	z.nr_sequencia_autor = a.nr_sequencia
					and	z.cd_material = cd_material_w
					and	z.qt_solicitada = qt_material_w);
			
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_item_autor_w
			from	material_autorizado
			where	nr_sequencia_autor = nr_seq_autorizacao_w
			and	cd_material = cd_material_w
			and	qt_solicitada = qt_material_w
			and	nvl(cd_cgc_fabricante, cd_cnpj_w) = cd_cnpj_w;
			
			if	(nr_seq_item_autor_w > 0) then
				update	material_autorizado
				set	vl_unitario = vl_unitario_item_w,
					vl_cotado = vl_unitario_atualizado_w,
					ie_origem_preco = 8,
					dt_atualizacao 	= sysdate,
					nm_usuario	= nm_usuario_p
				where	nr_sequencia =nr_seq_item_autor_w;
				
				select	a.cd_estabelecimento
				into	cd_estabelecimento_w
				from	agenda a,
					agenda_paciente b
				where 	a.cd_agenda = b.cd_agenda
				and	b.nr_sequencia = nr_seq_agenda_p;    

				atualizar_preco_mat_aut_conv(null,nr_seq_item_autor_w,cd_estabelecimento_w,nm_usuario_p,ds_retorno_atualizar_preco_w);
			else	
				if	(Nvl(nr_seq_agenda_pac_opme_w,0) > 0) then
					gerar_autor_regra(null,null,null,null,null,null,'AP',nm_usuario_p,nr_seq_agenda_p,null,null,null,nr_seq_agenda_pac_opme_w,null,'','','');
					
					select	max(nr_sequencia)
					into	nr_seq_autorizacao_w
					from	autorizacao_convenio a
					where	a.nr_seq_agenda = nr_seq_agenda_p
					and not exists(	select	1
							from	estagio_autorizacao x
							where	x.nr_sequencia = a.nr_seq_estagio
							and	x.ie_interno in (10,70,90));
			
					select	nvl(max(nr_sequencia),0)
					into	nr_seq_item_autor_w
					from	material_autorizado
					where	nr_sequencia_autor = nr_seq_autorizacao_w
					and	cd_material = cd_material_w
					and	qt_solicitada = qt_material_w;
					
					update	material_autorizado
					set	vl_cotado = vl_unitario_atualizado_w,
						dt_atualizacao 	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_sequencia =  nr_seq_item_autor_w;
					
				end if;
			end if;
		else
			if	(Nvl(nr_seq_agenda_pac_opme_w,0) > 0) then
				gerar_autor_regra(null,null,null,null,null,null,'AP',nm_usuario_p,nr_seq_agenda_p,null,null,null,nr_seq_agenda_pac_opme_w,null,'','','');
			
				select	max(nr_sequencia)
				into	nr_seq_autorizacao_w
				from	autorizacao_convenio a
				where	a.nr_seq_agenda = nr_seq_agenda_p
				and not exists(	select	1
						from	estagio_autorizacao x
						where	x.nr_sequencia = a.nr_seq_estagio
						and	x.ie_interno in (10,70,90));
		
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_item_autor_w
				from	material_autorizado
				where	nr_sequencia_autor = nr_seq_autorizacao_w
				and	cd_material = cd_material_w
				and	qt_solicitada = qt_material_w
				and	nvl(cd_cgc_fabricante, cd_cnpj_w) = cd_cnpj_w;
				
				update	material_autorizado
				set	vl_cotado 	= vl_unitario_atualizado_w,
					dt_atualizacao 	= sysdate,
					nm_usuario	= nm_usuario_p
				where	nr_sequencia = nr_seq_item_autor_w;
			
			end if;
		end if;
		commit;
		end;
	end loop;
	close c01;
end if;
end autorizar_agenda_cirurgia;
/
