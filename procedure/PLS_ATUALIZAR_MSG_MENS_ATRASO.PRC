create or replace
procedure pls_atualizar_msg_mens_atraso
			(	nm_usuario_p		varchar2) is

cursor c01 is
	select	nr_seq_evento_mens,
		ie_situacao_pagador,
		nvl(ie_tipo_alerta, 'P') ie_tipo_alerta
	from	pls_regra_geracao_evento
	where	ie_evento_disparo	= 7
	and	ie_situacao		= 'A';

cursor c02 (	ie_tipo_alerta_pc	pls_regra_geracao_evento.ie_tipo_alerta%type) is
	select	cd_estabelecimento,
		ie_tipo_contratacao,
		decode(ie_tipo_alerta, 'P', qt_dias_atraso, qt_dias_atraso*-1) qt_dias_atraso,
		ie_envio_unico,
		ie_agrupar_tit_compet,
		ie_pagador_cooperado
	from	(select	cd_estabelecimento,
			ie_tipo_contratacao,
			nvl(qt_dias_atraso,0) qt_dias_atraso,
			nvl(ie_envio_unico,'N') ie_envio_unico,
			nvl(ie_agrupar_tit_compet,'N') ie_agrupar_tit_compet,
			nvl(ie_pagador_cooperado,'A') ie_pagador_cooperado,
			nvl(ie_tipo_alerta, 'P') ie_tipo_alerta
		from	pls_regra_msg_mensalidade
		where	sysdate between dt_inicio_vigencia_ref and dt_fim_vigencia_ref)
	where	ie_tipo_alerta = ie_tipo_alerta_pc;

cursor c03 (	dt_pagamento_inicio_pc		titulo_receber.dt_pagamento_previsto%type,
		dt_pagamento_fim_pc		titulo_receber.dt_pagamento_previsto%type,
		cd_estabelecimento_pc		estabelecimento.cd_estabelecimento%type,
		qt_dias_atraso_pc		number,
		ie_situacao_pagador_pc		pls_regra_geracao_evento.ie_situacao_pagador%type) is
	select	b.nr_seq_pagador,
		a.nr_seq_mensalidade,
		b.dt_referencia,
		c.cd_pessoa_fisica
	from	titulo_receber		a,
		pls_mensalidade		b,
		pls_contrato_pagador	c
	where	a.dt_liquidacao is null
	and	a.dt_pagamento_previsto between dt_pagamento_inicio_pc and dt_pagamento_fim_pc
	and	a.cd_estabelecimento 	= cd_estabelecimento_pc
	and	b.nr_sequencia		= a.nr_seq_mensalidade
	and	c.nr_sequencia		= b.nr_seq_pagador
	and	(trunc(sysdate,'dd') - trunc(a.dt_pagamento_previsto,'dd')) = qt_dias_atraso_pc
	and	b.dt_cancelamento is null
	and	a.dt_liquidacao is null
	and 	((ie_situacao_pagador_pc = 'T')
	or	((ie_situacao_pagador_pc = 'A') 
	and 	(c.dt_rescisao is null or c.dt_rescisao > sysdate))
	or	((ie_situacao_pagador_pc = 'I') 
	and 	(nvl(c.dt_rescisao,sysdate) < sysdate)))
	order by 
		nr_seq_pagador;

Cursor C04 (	nr_seq_evento_mens_pc		pls_regra_geracao_evento.nr_seq_evento_mens%type) is
	select	ie_forma_envio, 
		ie_tipo_pessoa_dest 
	from	pls_alerta_evento_destino  a  
	where	nr_seq_evento_mens = nr_seq_evento_mens_pc 
	and	ie_situacao = 'A' ;

cursor c05 (	dt_pagamento_inicio_pc		titulo_receber.dt_pagamento_previsto%type,
		dt_pagamento_fim_pc		titulo_receber.dt_pagamento_previsto%type,
		cd_estabelecimento_pc		estabelecimento.cd_estabelecimento%type,
		qt_dias_atraso_pc		number,
		ie_situacao_pagador_pc		pls_regra_geracao_evento.ie_situacao_pagador%type) is
	select 	c.nr_sequencia nr_seq_pagador,  
		max(b.dt_referencia) dt_referencia,
		max(b.nr_sequencia) nr_seq_mensalidade,
		c.cd_pessoa_fisica
	from	titulo_receber		a,
		pls_mensalidade		b,
		pls_contrato_pagador	c
	where	b.nr_sequencia		= a.nr_seq_mensalidade
	and	c.nr_sequencia		= b.nr_seq_pagador
	and	a.dt_liquidacao is null
	and	a.dt_pagamento_previsto between dt_pagamento_inicio_pc and dt_pagamento_fim_pc
	and	a.cd_estabelecimento	= cd_estabelecimento_pc
	and	(trunc(sysdate,'dd') - trunc(a.dt_pagamento_previsto,'dd')) = qt_dias_atraso_pc
	and	b.dt_cancelamento is null
	and	a.dt_liquidacao is null
	and 	((ie_situacao_pagador_pc = 'T')
	or	((ie_situacao_pagador_pc = 'A') 
	and 	(c.dt_rescisao is null or c.dt_rescisao > sysdate))
	or	((ie_situacao_pagador_pc = 'I') 
	and 	( nvl(c.dt_rescisao,sysdate) < sysdate)))
	group by c.nr_sequencia, c.cd_pessoa_fisica;

Cursor C06 (	nr_seq_pagador_pc		pls_segurado.nr_seq_pagador%type,
		dt_referencia_pc		pls_mensalidade.dt_referencia%type)is
	select	a.nr_seq_mensalidade,
		a.nr_titulo
	from	titulo_receber	a,
		pls_mensalidade	b
	where	a.dt_liquidacao is null
	and	b.nr_sequencia		= a.nr_seq_mensalidade
	and	b.dt_referencia		= dt_referencia_pc
	and	b.nr_seq_pagador	= nr_seq_pagador_pc
	and	b.dt_cancelamento is null;

dt_pagamento_max_w		titulo_receber.dt_pagamento_previsto%type;
dt_pagamento_min_w		titulo_receber.dt_pagamento_previsto%type;
dt_pagamento_6_meses_w		titulo_receber.dt_pagamento_previsto%type;
nr_seq_evento_controle_w	pls_alerta_evento_controle.nr_sequencia%type;
nr_seq_tipo_evento_w		pls_tipo_alerta_evento.nr_sequencia%type;
ds_mensagem_w			pls_alerta_evento_mensagem.ds_mensagem%type;
ds_mensagem_padrao_w		pls_alerta_evento_mensagem.ds_mensagem%type;
ds_titulo_w			pls_alerta_evento_mensagem.ds_titulo%type;
ie_tipo_envio_w			pls_alerta_evento_mensagem.ie_tipo_envio%type;
ds_remetente_email_w		pls_alerta_evento_mensagem.ds_remetente_email%type;
ds_remetente_sms_w		pls_alerta_evento_mensagem.ds_remetente_sms%type;
nm_beneficiario_w		pessoa_fisica.nm_pessoa_fisica%type;
cd_usuario_plano_w		pls_segurado_carteira.cd_usuario_plano%type;
ie_forma_envio_w		pls_alerta_evento_destino.ie_forma_envio%type;
ie_tipo_pessoa_dest_w		pls_alerta_evento_destino.ie_tipo_pessoa_dest%type;
ds_email_dest_w			usuario.ds_email%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
ds_sms_dest_w			varchar2(15);
id_sms_w			pls_alerta_evento_cont_des.id_sms%type;
nm_pagador_w			Varchar2(80);
qt_qtde_envio_w			number(10);
ie_enviar_w			varchar2(1);
qt_macro_w			number(10);
ie_tipo_contratacao_w		varchar2(3);
cd_operadora_empresa_w		pls_contrato.cd_operadora_empresa%type;
nr_seq_contrato_w		pls_contrato.nr_sequencia%type;
nr_contrato_w			pls_contrato.nr_contrato%type;
carteira_benef_titular_w	pls_segurado_carteira.cd_usuario_plano%type;
ie_agrupar_tit_compet_w		pls_regra_msg_mensalidade.ie_agrupar_tit_compet%type;
nr_seq_mensalidade_w		pls_mensalidade.nr_sequencia%type;
nr_seq_titulo_w			titulo_receber.nr_titulo%type;
ds_mensalidades_w		varchar(200);
ds_titulos_w			varchar(200);
ds_sql_w			varchar(4000);
qt_benef_cooperado_w		number(10);
ie_gerar_w			varchar2(1);
ds_esconder_ddi_w		varchar2(1);

begin

for r_C01_w in C01 loop
	
	select	max(b.nr_sequencia),
		max(a.ds_mensagem),
		max(a.ds_titulo),
		max(a.ie_tipo_envio),
		max(a.ds_remetente_email),
		max(a.ds_remetente_sms)
	into	nr_seq_tipo_evento_w,
		ds_mensagem_padrao_w,
		ds_titulo_w,
		ie_tipo_envio_w,
		ds_remetente_email_w,
		ds_remetente_sms_w
	from	pls_alerta_evento_mensagem a,
		pls_tipo_alerta_evento b
	where	a.nr_seq_tipo_evento = b.nr_sequencia
	and	a.ie_situacao = 'A'
	and	b.ie_situacao = 'A'
	and	a.nr_sequencia = r_C01_w.nr_seq_evento_mens;

	for r_C02_w in C02(r_c01_w.ie_tipo_alerta) loop

		if	(r_c01_w.ie_tipo_alerta = 'P') then
			select	trunc(max(dt_pagamento_previsto)) maximo,
				trunc(min(dt_pagamento_previsto)) minimo
			into	dt_pagamento_max_w,
				dt_pagamento_min_w
			from	titulo_receber
			where	dt_pagamento_previsto < trunc(sysdate)
			and	nr_seq_mensalidade is not null
			and	dt_liquidacao is null;
		else	
			select	trunc(max(dt_pagamento_previsto)) maximo,
				trunc(min(dt_pagamento_previsto)) minimo
			into	dt_pagamento_max_w,
				dt_pagamento_min_w
			from	titulo_receber
			where	dt_pagamento_previsto > trunc(sysdate)
			and	nr_seq_mensalidade is not null
			and	dt_liquidacao is null;
		end if;

		--Loop para ir coletando os dados em atraso de 6 em 6 meses
		while	(dt_pagamento_min_w < dt_pagamento_max_w) loop
			begin
			
			--Adiciona 6 meses para capturar um periodo de 6 meses de titulos em aberto
			if 	(trunc(add_months(dt_pagamento_min_w, 6)) < dt_pagamento_max_w) then
				dt_pagamento_6_meses_w := trunc(add_months(dt_pagamento_min_w, 6));
			else
				dt_pagamento_6_meses_w := dt_pagamento_max_w;
			end if;
			
			if 	(r_C02_w.ie_agrupar_tit_compet = 'S') then
				--Abre cursor com os titulos a receber em atraso dentro dos primeiros 6 meses
				for r_C05_w in C05(dt_pagamento_min_w, dt_pagamento_6_meses_w, r_C02_w.cd_estabelecimento, r_C02_w.qt_dias_atraso, r_C01_w.ie_situacao_pagador) loop
					begin
					select	max(c.ie_tipo_contratacao),
						max(a.nr_seq_contrato)
					into	ie_tipo_contratacao_w,
						nr_seq_contrato_w
					from	pls_contrato_pagador a,
						pls_contrato_plano b,
						pls_plano c
					where	c.nr_sequencia	= b.nr_seq_plano
					and	b.nr_seq_contrato = a.nr_seq_contrato
					and	a.nr_sequencia	= r_C05_w.nr_seq_pagador;
					
					select	count(1)
					into	qt_benef_cooperado_w
					from	pls_cooperado
					where	cd_pessoa_fisica = r_c05_w.cd_pessoa_fisica
					and	ie_status = 'A';
					
					ie_gerar_w := 'S';
					
					if	(r_c02_w.ie_pagador_cooperado = 'A') or 
						((r_c02_w.ie_pagador_cooperado = 'C') and (qt_benef_cooperado_w > 0)) or
						((r_c02_w.ie_pagador_cooperado = 'N') and (qt_benef_cooperado_w = 0)) then
						ie_gerar_w := 'S';
					else
						ie_gerar_w := 'N';
					end if;

					if	((r_C02_w.ie_tipo_contratacao = ie_tipo_contratacao_w) and (ie_gerar_w = 'S')) then
						--So e possivel individualizar o titular para pagadores PF, para PJ nao existe essa possibilidade
						if 	(r_c05_w.cd_pessoa_fisica is not null) then
							select	pls_obter_dados_segurado(max(nr_seq_segurado),'C')
							into	carteira_benef_titular_w
							from	pls_mensalidade_segurado
							where	nr_seq_mensalidade = r_c05_w.nr_seq_mensalidade
							and	nr_seq_titular is null;
						else
							carteira_benef_titular_w	:= '';
						end if;

						if	(nvl(nr_seq_tipo_evento_w,0) > 0) then
							nm_pagador_w		:= pls_obter_dados_pagador(r_C05_w.nr_seq_pagador,'N');
							cd_operadora_empresa_w	:= pls_obter_dados_contrato(nr_seq_contrato_w,'CD');
							nr_contrato_w		:= pls_obter_dados_contrato(nr_seq_contrato_w,'N');
							ds_mensagem_w		:= ds_mensagem_padrao_w;
							ds_mensalidades_w	:= '(';
							ds_titulos_w		:= '(';
							
							for r_C06_w in C06(r_C05_w.nr_seq_pagador, r_C05_w.dt_referencia) loop
								if 	(length(ds_mensalidades_w) > 1) then
									ds_mensalidades_w := ds_mensalidades_w||', '||r_C06_w.nr_seq_mensalidade;
								else
									ds_mensalidades_w := ds_mensalidades_w||r_C06_w.nr_seq_mensalidade;
								end if;	
								if 	(length(ds_titulos_w) > 1) then
									ds_titulos_w := ds_titulos_w||', '||r_C06_w.nr_titulo;
								else
									ds_titulos_w := ds_titulos_w||r_C06_w.nr_titulo;
								end if;
							end loop;
							
							ds_mensalidades_w	:= ds_mensalidades_w||')';
							ds_titulos_w		:= ds_titulos_w||')';
							
							select	count(1)
							into	qt_macro_w
							from	dual
							where	ds_mensagem_w like '%@%';
							
							if	(qt_macro_w > 0) then
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@NOME_PAGADOR', substr(nm_pagador_w,1,instr(nm_pagador_w,' '))), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@PAGADOR', nm_pagador_w), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@DT_REFERENCIA_MENSALIDADE', r_C05_w.dt_referencia), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@REQUISICAO', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@CD_GUIA', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@ANALISE', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@DT_VALIDADE_SENHA', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@DS_PROCEDIMENTO', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@PRESTADOR', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@CARTEIRA_BENEF_TITULAR', carteira_benef_titular_w), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@CD_OPERADORA_EMPRESA', cd_operadora_empresa_w), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@NR_CONTRATO', nr_contrato_w), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@MENSALIDADES', ds_mensalidades_w), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@TITULOS', ds_titulos_w), 1, 4000);
							end if;
							
							select	pls_alerta_evento_controle_seq.nextval
							into	nr_seq_evento_controle_w
							from	dual;

							insert 	into pls_alerta_evento_controle
								(nr_sequencia,
								dt_geracao_evento,
								ie_evento_disparo,
								nr_seq_tipo_evento,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ds_mensagem,
								ds_titulo,
								ie_tipo_envio
								)
							values	(nr_seq_evento_controle_w,
								sysdate,
								7,
								nr_seq_tipo_evento_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								ds_mensagem_w,
								ds_titulo_w,
								ie_tipo_envio_w
								);
							
							for r_C04_w in C04(r_C01_w.nr_seq_evento_mens) loop
								begin
								if	(r_C04_w.ie_forma_envio = 'EM') and (nvl(ds_remetente_email_w,'X') <> 'X') then
									if	(r_C04_w.ie_tipo_pessoa_dest = 'PG') then
										ds_email_dest_w := pls_obter_dados_pagador(r_C05_w.nr_seq_pagador,'M');
										if	(nvl(ds_email_dest_w,'X') <> 'X') then
											enviar_email(	ds_titulo_w, ds_mensagem_w, ds_remetente_email_w,
													ds_email_dest_w, nm_usuario_p, 'M');
											pls_gerar_destino_evento(	nr_seq_evento_controle_w, 'E', r_C04_w.ie_forma_envio,
															nm_usuario_p, cd_pessoa_fisica_w, null,
															ds_mensagem_w, id_sms_w, ds_email_dest_w);
										end if;
									end if;
								end if;
								
								if	(r_C04_w.ie_forma_envio = 'SMS') and (nvl(ds_remetente_sms_w,'X') <> 'X') then
									if	(r_C04_w.ie_tipo_pessoa_dest = 'PG') then
										cd_pessoa_fisica_w 	:= pls_obter_dados_pagador(r_C05_w.nr_seq_pagador,'CPFP');

										ds_esconder_ddi_w := OBTER_VALOR_PARAM_USUARIO(0,214,0,obter_usuario_ativo,obter_estabelecimento_ativo);
										if 	(ds_esconder_ddi_w = 'S') then 
											ds_sms_dest_w		:= obter_dados_pf(cd_pessoa_fisica_w, 'TCD');
										else
											ds_sms_dest_w		:= obter_dados_pf(cd_pessoa_fisica_w, 'TCI');										
										end if;
										
										if	(nvl(ds_sms_dest_w,'X') <> 'X') then
											pls_enviar_sms_alerta_evento(	ds_remetente_sms_w, ds_sms_dest_w, substr(ds_mensagem_w,1,150),
															nr_seq_tipo_evento_w, nm_usuario_p, id_sms_w);
											pls_gerar_destino_evento(	nr_seq_evento_controle_w, 'E', r_C04_w.ie_forma_envio,
															nm_usuario_p, cd_pessoa_fisica_w, null,
															ds_mensagem_w, id_sms_w, ds_sms_dest_w);
										end if;
									end if;
								end if;
								
								end;
							end loop;
						end if;
					end if;
					end;
				end loop; --C05
			else
				--Abre cursor com os titulos a receber em atraso dentro dos primeiros 6 meses
				for r_C03_w in C03(dt_pagamento_min_w, dt_pagamento_6_meses_w, r_C02_w.cd_estabelecimento, r_C02_w.qt_dias_atraso, r_C01_w.ie_situacao_pagador) loop

					select	max(c.ie_tipo_contratacao),
						max(a.nr_seq_contrato)
					into	ie_tipo_contratacao_w,
						nr_seq_contrato_w
					from	pls_contrato_pagador a,
						pls_contrato_plano b,
						pls_plano c
					where	c.nr_Sequencia = b.nr_Seq_plano
					and	b.nr_seq_contrato = a.nr_seq_contrato
					and	a.nr_Sequencia = r_C03_w.nr_seq_pagador;
					
					if	(r_C02_w.ie_tipo_contratacao = ie_tipo_contratacao_w) then
						--So e possivel individualizar o titular para pagadores PF, para PJ nao existe essa possibilidade
						if 	(r_C03_w.cd_pessoa_fisica is not null) then
							select	pls_obter_dados_segurado(max(nr_seq_segurado),'C')
							into	carteira_benef_titular_w
							from	pls_mensalidade_segurado
							where	nr_seq_mensalidade = r_C03_w.nr_seq_mensalidade
							and	nr_seq_titular is null;
						else
							carteira_benef_titular_w	:= '';
						end if;
						
						ie_enviar_w := 'S';
						if	(r_C02_w.ie_envio_unico = 'S') 	then
							select	count(*)
							into	qt_qtde_envio_w
							from	pls_alerta_evento_controle
							where nr_seq_mensalidade = r_C03_w.nr_seq_mensalidade;
							
							if	(qt_qtde_envio_w > 0) then
								ie_enviar_w := 'N';
							end if;
						end if;
						
						select	count(1)
						into	qt_benef_cooperado_w
						from	pls_cooperado
						where	cd_pessoa_fisica = r_c03_w.cd_pessoa_fisica
						and	ie_status = 'A';
						
						ie_gerar_w := 'S';
						
						if	(r_c02_w.ie_pagador_cooperado = 'A') or 
							((r_c02_w.ie_pagador_cooperado = 'C') and (qt_benef_cooperado_w > 0)) or
							((r_c02_w.ie_pagador_cooperado = 'N') and (qt_benef_cooperado_w = 0)) then
							ie_gerar_w := 'S';
						else
							ie_gerar_w := 'N';
						end if;
						
						if	((nvl(nr_seq_tipo_evento_w,0) > 0) and (ie_enviar_w = 'S') and (ie_gerar_w = 'S')) then
							nm_pagador_w		:= pls_obter_dados_pagador(r_C03_w.nr_seq_pagador,'N');
							cd_operadora_empresa_w	:= pls_obter_dados_contrato(nr_seq_contrato_w,'CD');
							nr_contrato_w		:= pls_obter_dados_contrato(nr_seq_contrato_w,'N');
							ds_mensagem_w		:= ds_mensagem_padrao_w;
							
							select	count(1)
							into	qt_macro_w
							from	dual
							where	ds_mensagem_w like '%@%';
							
							if	(qt_macro_w > 0) then
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@NOME_PAGADOR', substr(nm_pagador_w,1,instr(nm_pagador_w,' '))), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@PAGADOR', nm_pagador_w), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@DT_REFERENCIA_MENSALIDADE', r_C03_w.dt_referencia), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@REQUISICAO', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@CD_GUIA', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@ANALISE', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@DT_VALIDADE_SENHA', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@DS_PROCEDIMENTO', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@PRESTADOR', ''), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@CARTEIRA_BENEF_TITULAR', carteira_benef_titular_w), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@CD_OPERADORA_EMPRESA', cd_operadora_empresa_w), 1, 4000);
								ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w, '@NR_CONTRATO', nr_contrato_w), 1, 4000);
							end if;

							select	pls_alerta_evento_controle_seq.nextval
							into	nr_seq_evento_controle_w
							from	dual;
							
							insert	into pls_alerta_evento_controle
								(nr_sequencia,
								dt_geracao_evento,
								ie_evento_disparo,
								nr_seq_tipo_evento,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ds_mensagem,
								ds_titulo,
								ie_tipo_envio,
								nr_seq_mensalidade)
							values	(nr_seq_evento_controle_w,
								sysdate,
								7,
								nr_seq_tipo_evento_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								ds_mensagem_w,
								ds_titulo_w,
								ie_tipo_envio_w,
								r_C03_w.nr_seq_mensalidade);
								
							for r_C04_w in C04(r_C01_w.nr_seq_evento_mens) loop
								begin
								if	(r_C04_w.ie_forma_envio = 'EM') and (nvl(ds_remetente_email_w,'X') <> 'X') then
									if	(r_C04_w.ie_tipo_pessoa_dest = 'PG') then
										ds_email_dest_w := pls_obter_dados_pagador(r_C03_w.nr_seq_pagador,'M');
										if	(nvl(ds_email_dest_w,'X') <> 'X') then
											enviar_email(	ds_titulo_w, ds_mensagem_w, ds_remetente_email_w,
													ds_email_dest_w, nm_usuario_p, 'M');
										end if;
									end if;
								end if;
								
								if	(r_C04_w.ie_forma_envio = 'SMS') and (nvl(ds_remetente_sms_w,'X') <> 'X') then
									if	(r_C04_w.ie_tipo_pessoa_dest = 'PG') then
									
										cd_pessoa_fisica_w 	:= pls_obter_dados_pagador(r_C03_w.nr_seq_pagador,'CPFP');
										ds_esconder_ddi_w := OBTER_VALOR_PARAM_USUARIO(0,214,0,obter_usuario_ativo,obter_estabelecimento_ativo);

										if 	(ds_esconder_ddi_w = 'S') then 		
											ds_sms_dest_w		:= obter_dados_pf(cd_pessoa_fisica_w, 'TCD');
										else 
											ds_sms_dest_w		:= obter_dados_pf(cd_pessoa_fisica_w, 'TCI');
										end if;
										
										if	(nvl(ds_sms_dest_w,'X') <> 'X') then
											pls_enviar_sms_alerta_evento(	ds_remetente_sms_w, ds_sms_dest_w, substr(ds_mensagem_w,1,150),
															nr_seq_tipo_evento_w, nm_usuario_p, id_sms_w);
											pls_gerar_destino_evento(	nr_seq_evento_controle_w, 'E', r_C04_w.ie_forma_envio,
															nm_usuario_p, cd_pessoa_fisica_w, null,
															ds_mensagem_w, id_sms_w, ds_sms_dest_w);
										end if;
									end if;
								end if;
								
								end;
							end loop;
						end if;
					end if;
				end loop;
			end if;
			
			--Adiciona 6 meses a data minima
			dt_pagamento_min_w := dt_pagamento_6_meses_w;
			
			commit;
			end;
		end loop;
		
	end loop;
	
	commit;
	
end loop;

end pls_atualizar_msg_mens_atraso;
/