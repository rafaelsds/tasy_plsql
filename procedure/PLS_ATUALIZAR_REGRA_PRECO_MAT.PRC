create or replace
procedure pls_atualizar_regra_preco_mat(
			nr_seq_regra_p		Number,
			ie_opcao_p		Varchar2,
			cd_estabalecimento_p	Varchar2,
			nm_usuario_p		Varchar2) is 
/* ie_opcao_p
	'A' - Atualizar
	'D' - Deletar
*/		

cd_categoria_w			Varchar2(30);                    
cd_convenio_w			Varchar2(30);
cd_estabelecimento_w		Varchar2(30);
cd_material_w			Varchar2(30);
cd_prestador_w			Varchar2(30);
ds_observacao_w			Varchar2(4000);
dt_base_fixo_w			Date;
dt_fim_vigencia_w		Date;
dt_inicio_vigencia_w		Date;
ie_generico_unimed_w		Varchar2(10);
ie_internado_w			Varchar2(10);
ie_mat_autorizacao_esp_w	Varchar2(10);
ie_origem_preco_w		Varchar2(10);
ie_pcmso_w			Varchar2(1);
ie_preco_w			Varchar2(10);
ie_situacao_w			Varchar2(10);
ie_tabela_adicional_w		Varchar2(10);
ie_tipo_contratacao_w		Varchar2(10);
ie_tipo_despesa_w		Varchar2(10);
ie_tipo_guia_w			Varchar2(10);
ie_tipo_intercambio_w		Varchar2(10);
ie_tipo_preco_brasindice_w	Varchar2(10);
ie_tipo_preco_simpro_w		Varchar2(10);
ie_tipo_segurado_w		Varchar2(10);
ie_tipo_tabela_w		Varchar2(10);
ie_tipo_vinculo_w		Varchar2(10);
nr_contrato_w			Number(30);
nr_seq_categoria_w		Number(30);
nr_seq_classificacao_w		Number(30);
nr_seq_clinica_w		Number(30);
nr_seq_congenere_w		Number(30);
nr_seq_congenere_prot_w		Number(30);
nr_seq_contrato_w		Number(30);
nr_seq_estrutura_mat_w		Number(30);
nr_seq_grupo_contrato_w		Number(30);
nr_seq_grupo_material_w		Number(30);
nr_seq_grupo_prestador_w	Number(30);
nr_seq_grupo_produto_w		Number(30);
nr_seq_grupo_uf_w		Number(30);
nr_seq_material_w		Number(30);
nr_seq_material_preco_w		Number(30);
nr_seq_outorgante_w		Number(30);
nr_seq_plano_w			Number(30);
nr_seq_prestador_w		Number(30);
nr_seq_regra_ant_w		Number(30);
nr_seq_tipo_acomodacao_w	Number(30);
nr_seq_tipo_atendimento_w	Number(30);
nr_seq_tipo_prestador_w		Number(30);
nr_seq_tipo_uso_w		Number(30);
nr_seq_regra_ref_w		Number(30);
sg_uf_operadora_intercambio_w	Varchar2(2);
tx_ajuste_w			Number(15,4);
tx_ajuste_benef_lib_W		Number(15,4);
tx_ajuste_pfb_w			Number(15,4);
tx_ajuste_pmc_neg_w		Number(15,4);
tx_ajuste_pmc_neut_w		Number(15,4);
tx_ajuste_pmc_pos_w		Number(15,4);
tx_ajuste_simpro_pfb_w		Number(15,4);
tx_ajuste_simpro_pmc_w		Number(15,4);
tx_ajuste_tab_propria_w		Number(15,4);
vl_negociado_w			Number(15,4);
nr_seq_regra_w			Number(30);
nr_seq_regra_mat_ref_w		Number(30);

cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_regra_mat_ref
	from	pls_regra_preco_mat	a
	start with a.nr_sequencia in	(select	x.nr_sequencia
					from	pls_regra_preco_mat x
					where	x.nr_sequencia	= nr_seq_regra_p)
	connect by prior a.nr_sequencia = a.nr_seq_regra_mat_ref;
	
Cursor C02 is
	select	a.nr_sequencia
	from	pls_regra_preco_mat	a
	start with a.nr_sequencia in	(select	x.nr_sequencia
					from	pls_regra_preco_mat x
					where	x.nr_seq_regra_mat_ref	= nr_seq_regra_p)
	connect by prior a.nr_sequencia = a.nr_seq_regra_mat_ref
	order by level desc;
			
begin

if	(ie_opcao_p = 'A') then
	open C01;
	loop
	fetch C01 into	
		nr_seq_regra_w,
		nr_seq_regra_mat_ref_w;
	exit when C01%notfound;
		begin
		if	(nvl(nr_seq_regra_mat_ref_w,0) <> 0 ) then
			select	cd_categoria,                    
				cd_convenio,                     
				cd_estabelecimento,          
				cd_material,                     
				cd_prestador,                    
				ds_observacao,                  
				dt_base_fixo,                    
				dt_fim_vigencia,                 
				dt_inicio_vigencia,          
				ie_generico_unimed,             
				ie_internado,                    
				ie_mat_autorizacao_esp,          
				ie_origem_preco,             
				ie_pcmso,                        
				ie_preco,                        
				ie_situacao,                 
				ie_tabela_adicional,             
				ie_tipo_contratacao,             
				ie_tipo_despesa,                 
				ie_tipo_guia,                    
				ie_tipo_intercambio,             
				ie_tipo_preco_brasindice,        
				ie_tipo_preco_simpro,            
				ie_tipo_segurado,                
				ie_tipo_tabela,                  
				ie_tipo_vinculo,                 
				nr_contrato,                     
				nr_seq_categoria,                
				nr_seq_classificacao,            
				nr_seq_clinica,                  
				nr_seq_congenere,                
				nr_seq_congenere_prot,           
				nr_seq_contrato,                 
				nr_seq_estrutura_mat,            
				nr_seq_grupo_contrato,           
				nr_seq_grupo_material,           
				nr_seq_grupo_prestador,          
				nr_seq_grupo_produto,            
				nr_seq_grupo_uf,                 
				nr_seq_material,                 
				nr_seq_material_preco,           
				nr_seq_outorgante,               
				nr_seq_plano,                    
				nr_seq_prestador,                
				nr_seq_regra_ant,                
				nr_seq_tipo_acomodacao,          
				nr_seq_tipo_atendimento,         
				nr_seq_tipo_prestador,           
				nr_seq_tipo_uso,                 
				nr_sequencia,                
				sg_uf_operadora_intercambio,     
				tx_ajuste,                       
				tx_ajuste_benef_lib,             
				tx_ajuste_pfb,               
				tx_ajuste_pmc_neg,           
				tx_ajuste_pmc_neut,          
				tx_ajuste_pmc_pos,           
				tx_ajuste_simpro_pfb,        
				tx_ajuste_simpro_pmc,        
				tx_ajuste_tab_propria,       
				vl_negociado                    			
			into	cd_categoria_w,                    
				cd_convenio_w,                     
				cd_estabelecimento_w,          
				cd_material_w,                     
				cd_prestador_w,                    
				ds_observacao_w,                  
				dt_base_fixo_w,                    
				dt_fim_vigencia_w,                 
				dt_inicio_vigencia_w,          
				ie_generico_unimed_w,             
				ie_internado_w,                    
				ie_mat_autorizacao_esp_w,          
				ie_origem_preco_w,             
				ie_pcmso_w,                        
				ie_preco_w,                        
				ie_situacao_w,                 
				ie_tabela_adicional_w,             
				ie_tipo_contratacao_w,             
				ie_tipo_despesa_w,                 
				ie_tipo_guia_w,                    
				ie_tipo_intercambio_w,             
				ie_tipo_preco_brasindice_w,        
				ie_tipo_preco_simpro_w,            
				ie_tipo_segurado_w,                
				ie_tipo_tabela_w,                  
				ie_tipo_vinculo_w,                 
				nr_contrato_w,                     
				nr_seq_categoria_w,                
				nr_seq_classificacao_w,            
				nr_seq_clinica_w,                  
				nr_seq_congenere_w,                
				nr_seq_congenere_prot_w,           
				nr_seq_contrato_w,                 
				nr_seq_estrutura_mat_w,            
				nr_seq_grupo_contrato_w,           
				nr_seq_grupo_material_w,           
				nr_seq_grupo_prestador_w,          
				nr_seq_grupo_produto_w,            
				nr_seq_grupo_uf_w,                 
				nr_seq_material_w,                 
				nr_seq_material_preco_w,           
				nr_seq_outorgante_w,               
				nr_seq_plano_w,                    
				nr_seq_prestador_w,                
				nr_seq_regra_ant_w,                
				nr_seq_tipo_acomodacao_w,          
				nr_seq_tipo_atendimento_w,         
				nr_seq_tipo_prestador_w,           
				nr_seq_tipo_uso_w,                 
				nr_seq_regra_ref_w,                
				sg_uf_operadora_intercambio_w,     
				tx_ajuste_w,                       
				tx_ajuste_benef_lib_W,             
				tx_ajuste_pfb_w,               
				tx_ajuste_pmc_neg_w,           
				tx_ajuste_pmc_neut_w,          
				tx_ajuste_pmc_pos_w,           
				tx_ajuste_simpro_pfb_w,        
				tx_ajuste_simpro_pmc_w,        
				tx_ajuste_tab_propria_w,       
				vl_negociado_w
			from	pls_regra_preco_mat
			where	nr_sequencia = nr_seq_regra_mat_ref_w;
			
			update	pls_regra_preco_mat
			set	cd_categoria			= cd_categoria_w,
				cd_convenio			= cd_convenio_w,
				cd_estabelecimento		= cd_estabelecimento_w,
				cd_material                     = cd_material_w,
				cd_prestador                    = cd_prestador_w,
				ds_observacao                   = ds_observacao_w,
				dt_atualizacao               	= sysdate,
				dt_base_fixo                    = dt_base_fixo_w,
				dt_fim_vigencia                 = dt_fim_vigencia_w,
				dt_inicio_vigencia           	= dt_inicio_vigencia_w,
				ie_generico_unimed              = ie_generico_unimed_w,
				ie_internado                    = ie_internado_w,
				ie_mat_autorizacao_esp          = ie_mat_autorizacao_esp_w,
				ie_origem_preco              	= ie_origem_preco_w,
				ie_pcmso			= ie_pcmso_w,                        
				ie_preco			= ie_preco_w,                        
				ie_situacao                  	= ie_situacao_w,
				ie_tabela_adicional             = ie_tabela_adicional_w,
				ie_tipo_contratacao             = ie_tipo_contratacao_w,
				ie_tipo_despesa                 = ie_tipo_despesa_w,
				ie_tipo_guia                    = ie_tipo_guia_w,
				ie_tipo_intercambio             = ie_tipo_intercambio_w,
				ie_tipo_preco_brasindice        = ie_tipo_preco_brasindice_w,
				ie_tipo_preco_simpro            = ie_tipo_preco_simpro_w,
				ie_tipo_segurado                = ie_tipo_segurado_w,
				ie_tipo_vinculo                 = ie_tipo_vinculo_w,
				nm_usuario                   	= nm_usuario_p,
				nr_contrato                     = nr_contrato_w,
				nr_seq_categoria                = nr_seq_categoria_w,
				nr_seq_classificacao            = nr_seq_classificacao_w,
				nr_seq_clinica                  = nr_seq_clinica_w,
				nr_seq_congenere                = nr_seq_congenere_w,
				nr_seq_congenere_prot           = nr_seq_congenere_prot_w,
				nr_seq_contrato                 = nr_seq_contrato_w,
				nr_seq_estrutura_mat            = nr_seq_estrutura_mat_w,
				nr_seq_grupo_contrato           = nr_seq_grupo_contrato_w,
				nr_seq_grupo_material           = nr_seq_grupo_material_w,
				nr_seq_grupo_prestador          = nr_seq_grupo_prestador_w,
				nr_seq_grupo_produto            = nr_seq_grupo_produto_w,
				nr_seq_grupo_uf                 = nr_seq_grupo_uf_w,
				nr_seq_material                 = nr_seq_material_w,
				nr_seq_material_preco           = nr_seq_material_preco_w,
				nr_seq_outorgante               = nr_seq_outorgante_w,
				nr_seq_plano                    = nr_seq_plano_w,
				nr_seq_prestador                = nr_seq_prestador_w,
				nr_seq_regra_ant                = nr_seq_regra_ant_w,
				nr_seq_regra_mat_ref            = nr_seq_regra_ref_w,
				nr_seq_tipo_acomodacao          = nr_seq_tipo_acomodacao_w,
				nr_seq_tipo_atendimento         = nr_seq_tipo_atendimento_w,
				nr_seq_tipo_prestador           = nr_seq_tipo_prestador_w,
				nr_seq_tipo_uso                 = nr_seq_tipo_uso_w,                 
				sg_uf_operadora_intercambio     = sg_uf_operadora_intercambio_w,
				tx_ajuste                       = tx_ajuste_w,
				tx_ajuste_benef_lib             = tx_ajuste_benef_lib_w,
				tx_ajuste_pfb                	= tx_ajuste_pfb_w,
				tx_ajuste_pmc_neg            	= tx_ajuste_pmc_neg_w,
				tx_ajuste_pmc_neut           	= tx_ajuste_pmc_neut_w,
				tx_ajuste_pmc_pos            	= tx_ajuste_pmc_pos_w,
				tx_ajuste_simpro_pfb         	= tx_ajuste_simpro_pfb_w,
				tx_ajuste_simpro_pmc         	= tx_ajuste_simpro_pmc_w,
				tx_ajuste_tab_propria        	= tx_ajuste_tab_propria_w,
				vl_negociado			= vl_negociado_w
			where	nr_sequencia = nr_seq_regra_w;
		end if;
		end;
	end loop;
	close C01;
elsif	(ie_opcao_p = 'D') then
	open C02;
	loop
	fetch C02 into	
		nr_seq_regra_w;
	exit when C02%notfound;
		begin
			delete	pls_regra_preco_mat
			where	nr_sequencia	= nr_seq_regra_w;
		end;
	end loop;
	close C02;
end if;
	
commit;

end pls_atualizar_regra_preco_mat;
/