create or replace
procedure Atualizar_adep_controle_SC(	nm_usuario_p		varchar2,
									nr_atendimento_p	number,
									ie_tipo_item_p		varchar2,
									ie_atualizar_p		varchar2,
									nr_prescricao_p		number) is 

nr_atendimento_w	number(15);

pragma autonomous_transaction;

begin

nr_atendimento_w	:= nr_atendimento_p;
if	(nvl(nr_atendimento_w,0) = 0) then
	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
end if;

if	(ie_tipo_item_p	= 'M') then
	update	adep_controle
	set		ie_atualizar_medic	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'MAT') then
	update	adep_controle
	set		ie_atualizar_material	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'CCG') then
	update	adep_controle
	set		ie_atualizar_ccg	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'CIG') then
	update	adep_controle
	set		ie_atualizar_cig	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'DIA') then
	update	adep_controle
	set		ie_atualizar_dialise	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;        
elsif	(ie_tipo_item_p	= 'D') then
	update	adep_controle
	set		ie_atualizar_dieta	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;   
elsif	(ie_tipo_item_p	= 'G') then
	update	adep_controle
	set		ie_atualizar_gas	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w; 
elsif	(ie_tipo_item_p	= 'HM') then
	update	adep_controle
	set		ie_atualizar_hemoterap	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w; 
elsif	(ie_tipo_item_p	= 'IVC') then
	update	adep_controle
	set		ie_atualizar_ivc	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w; 
elsif	(ie_tipo_item_p	= 'J') then
	update	adep_controle
	set		ie_atualizar_jejum	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w; 
elsif	(ie_tipo_item_p	= 'L') then
	update	adep_controle
	set		ie_atualizar_lab	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;  
elsif	(ie_tipo_item_p	= 'LD') then
	update	adep_controle
	set		ie_atualizar_leite	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'NPA') then
	update	adep_controle
	set		ie_atualizar_npt_adulta	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'NPN') then
	update	adep_controle
	set		ie_atualizar_npt_neo	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'NPP') then
	update	adep_controle
	set		ie_atualizar_npt_ped	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'P') then
	update	adep_controle
	set		ie_atualizar_proced	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'R') then
	update	adep_controle
	set		ie_atualizar_rec	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'SNE') then
	update	adep_controle
	set		ie_atualizar_sne	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'SOL') then
	update	adep_controle
	set		ie_atualizar_solucao	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'S') then
	update	adep_controle
	set		ie_atualizar_supl	= ie_atualizar_p,
			dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_w;
elsif	(ie_tipo_item_p	= 'TODOS') then
	update	adep_controle
	set		ie_atualizar_dieta     	= ie_atualizar_p,
			ie_atualizar_jejum     	= ie_atualizar_p,
			ie_atualizar_supl      	= ie_atualizar_p,
			ie_atualizar_sne       	= ie_atualizar_p,
			ie_atualizar_npt_adulta	= ie_atualizar_p,
			ie_atualizar_npt_neo   	= ie_atualizar_p,
			ie_atualizar_npt_ped   	= ie_atualizar_p,
			ie_atualizar_leite     	= ie_atualizar_p,
			ie_atualizar_solucao   	= ie_atualizar_p,
			ie_atualizar_medic     	= ie_atualizar_p,
			ie_atualizar_material  	= ie_atualizar_p,
			ie_atualizar_proced    	= ie_atualizar_p,
			ie_atualizar_lab       	= ie_atualizar_p,
			ie_atualizar_ccg       	= ie_atualizar_p,
			ie_atualizar_cig       	= ie_atualizar_p,
			ie_atualizar_ivc       	= ie_atualizar_p,
			ie_atualizar_gas       	= ie_atualizar_p,
			ie_atualizar_hemoterap 	= ie_atualizar_p,
			ie_atualizar_rec       	= ie_atualizar_p,
			ie_atualizar_dialise   	= ie_atualizar_p,
			dt_atualizacao			= sysdate
	where	nr_atendimento			= nr_atendimento_w;	
end if;

commit;

end Atualizar_adep_controle_SC;
/
