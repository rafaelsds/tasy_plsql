create or replace
procedure html_excluir_operacao_terc (	id_sessao_p		varchar2,
										nm_usuario_p	varchar2,
										nr_sequencia_p	varchar2 default '0') is 

begin

delete 	reg_operacoes_terc_web 
where 	nm_usuario = nm_usuario_p 
and 	((nvl(nr_sequencia_p,'0') <> '0' and obter_se_contido(nr_sequencia,nr_sequencia_p) = 'S' ) or (nr_sequencia_p = '0'))
and		id_sessao = id_sessao_p;

commit;

end html_excluir_operacao_terc;
/