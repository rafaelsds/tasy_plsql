create or replace
procedure GRAVAR_ALTERACAO_PLANEJ_COMPRA(
			nr_seq_planejamento_p		number,
			cd_material_p			number,
			qt_entrega_sistema_p		number,
   			qt_entrega_usuario_p		number,
			qt_tot_compra_sistema_p		number,
			qt_tot_compra_usuario_p		number,
			nm_usuario_p			varchar2) is



begin

Insert into log_altera_planej_compra(
	nr_sequencia,
	nr_seq_planejamento,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	qt_entrega_sistema,
	qt_entrega_usuario,
	qt_tot_compra_sistema,
	qt_tot_compra_usuario,
	dt_alteracao,
	cd_material)
values(	log_altera_planej_compra_seq.nextval,
	nr_seq_planejamento_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	qt_entrega_sistema_p,
	qt_entrega_usuario_p,
	qt_tot_compra_sistema_p,
	qt_tot_compra_usuario_p,
	trunc(sysdate),
	cd_material_p);

commit;

end gravar_alteracao_planej_compra;
/