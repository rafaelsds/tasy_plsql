
create or replace
procedure SALVA_COMPLEMENTO_LAUDO(
			nr_seq_laudo_p			Number,
			nm_usuario_p			Varchar2,
			ds_laudo_p			Varchar2,
			ds_laudo_relatorio_p 		Varchar2,
			ds_laudo_original_p			Varchar2,
			nr_prescricao_p			Number,
			NR_SEQ_PRESCRICAO_P     	Number,
			ie_status_p 			Number
			) is 

begin
	
	update	laudo_paciente 
	set 		ds_laudo = ds_laudo_p, 
				dt_atualizacao = sysdate, 
				nm_usuario = nm_usuario_p 
	where 	nr_sequencia = nr_seq_laudo_p;
		
	update	laudo_paciente_relatorio 
	set 		ds_laudo = ds_laudo_relatorio_p, 
				dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p 
	where 	nr_seq_laudo = nr_seq_laudo_p;
		
	insert 	into prescr_proc_auditoria (nr_sequencia, nr_prescricao, nr_sequencia_prescricao, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, ie_status_auditoria, ds_laudo) 
	values (prescr_proc_auditoria_seq.nextval, nr_prescricao_p, nr_seq_prescricao_p, sysdate, nm_usuario_p, sysdate, nm_usuario_p, ie_status_p, ds_laudo_original_p);
	
commit;

end SALVA_COMPLEMENTO_LAUDO;
/