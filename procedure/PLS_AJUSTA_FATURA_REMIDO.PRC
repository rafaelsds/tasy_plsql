create or replace
procedure pls_ajusta_fatura_remido(nr_sequencia_p pls_fatura.nr_sequencia%type) is


-- busca todas as faturas remido do lote
cursor c01 is
select pl.nr_sequencia
  from pls_fatura pl
 where pl.nr_sequencia = nr_sequencia_p
   and nvl(pl.ie_remido,'N') = 'S';

cursor c02(nr_seq_pls_fatura_pc number) is
   select pc.nr_sequencia nr_seq_pls_fatura,
          pt.cd_unimed_origem,
          ph.nr_sequencia nr_seq_nota_hosp
     from ptu_fatura         pt,
          ptu_nota_cobranca  pc,
          ptu_nota_hospitalar ph
    where pt.nr_seq_pls_fatura = nr_seq_pls_fatura_pc
      and pc.nr_seq_fatura     = pt.nr_sequencia
      and ph.nr_seq_nota_cobr  = pc.nr_sequencia;
      
cursor c03(nr_seq_pls_fatura_pc number) is   
   select pc.nr_sequencia nr_seq_pls_fatura,
          pt.cd_unimed_origem,
          pt.cd_unimed_destino,
          ps.nr_sequencia nr_seq_servic,
          ps.cd_unimed_prestador,
          ps.nr_cgc_cpf,
          ps.nr_seq_conta_proc,
          ps.nr_seq_conta_mat,
          ps.dt_procedimento,
          ps.nr_seq_item,
          ps.ie_tipo_pessoa_prestador,
          pc.nr_sequencia nr_seq_cobranca,
          pc.tp_pessoa
     from ptu_fatura         pt,
          ptu_nota_cobranca  pc,
          ptu_nota_servico   ps
    where pt.nr_seq_pls_fatura = nr_seq_pls_fatura_pc
      and pc.nr_seq_fatura     = pt.nr_sequencia
      and ps.nr_seq_nota_cobr  = pc.nr_sequencia
      and	nvl(ps.qt_procedimento,0) > 0;
      
cd_unimed_hospital_w           varchar2(4);                         -- ptu_nota_hospitalar.cd_unimed_hospital
nr_cgc_cpf_hosp_w              varchar2(14);                        -- nada
cd_hospital_w                  varchar2(8);                         -- ptu_nota_hospitalar.cd_hospital
nm_hospital_w                  varchar2(60);                        -- ptu_nota_hospitalar.nm_hospital
cd_cgc_hospital_w              varchar2(14);                        -- ptu_nota_hospitalar.cd_cgc_hospital

cd_unimed_autorizadora_w       varchar2(4);                         -- ptu_nota_servico.cd_unimed_autorizadora
cd_prestador_w                 ptu_nota_servico.cd_prestador%type;  -- ptu_nota_servico.cd_prestador%type
nm_profissional_prestador_w    ptu_nota_servico.nm_profissional_prestador%type;  -- ptu_nota_servico.nm_profissional_prestador
nr_cbo_exec_w                  ptu_nota_servico.nr_cbo_exec%type;                -- ptu_nota_servico.nr_cbo_exec

cd_item_unico_w                ptu_nota_servico.cd_item_unico%type;

nr_cgc_cpf_w                   varchar2(14);

begin

for r01 in c01 loop

    -- atualiza as informacoes da nota hospitalar
    for r02 in c02(r01.nr_sequencia) loop
      
      cd_unimed_hospital_w := null;
      nr_cgc_cpf_hosp_w    := null;
      cd_hospital_w        := null;
      nm_hospital_w        := null;
      cd_cgc_hospital_w    := null;
    
      -- pega a unimed do servico realizado
      select   max(lpad(nvl(pns.cd_unimed_prestador,'0'),4,'0')),
               max(lpad(nvl(pns.nr_cgc_cpf,'0'),14,'0'))
      into     cd_unimed_hospital_w,
               nr_cgc_cpf_hosp_w
      from     ptu_nota_cobranca pnc,
               ptu_nota_servico  pns
      where    pns.nr_seq_nota_cobr = pnc.nr_sequencia
      and      pns.Ie_Tipo_Tabela   = 1 -- taxa e servico hospitalar
      and      pnc.nr_sequencia     = r02.nr_seq_pls_fatura;    

      begin
        -- se for prestador de intercambio
        if nvl(cd_unimed_hospital_w,'X') <> nvl(r02.cd_unimed_origem,'X') then
          begin
            select   lpad(d.nr_sequencia,8,'0'),
              rpad(nvl(elimina_caractere_especial(pls_obter_dados_prest_inter(d.nr_sequencia,'N')),' '),60,' '),
              lpad(d.cd_cgc_intercambio, 14, '0')
            into   cd_hospital_w,
              nm_hospital_w,
              cd_cgc_hospital_w
            from 	pls_prestador_intercambio d
            where 	lpad(d.cd_cgc_intercambio, 14, '0') = lpad(nr_cgc_cpf_hosp_w, 14, '0')
            and 	rownum = 1;
          exception
            when others then
              null;
          end;
        end if;
      exception
        when others then
          null;
      end;
      
      update ptu_nota_hospitalar pnh
         set cd_unimed_hospital  = nvl(cd_unimed_hospital_w,cd_unimed_hospital)
            ,cd_hospital         = nvl(cd_hospital_w,cd_hospital)
            ,nm_hospital         = nvl(nm_hospital_w,nm_hospital)
            ,cd_cgc_hospital     = nvl(cd_cgc_hospital_w,cd_cgc_hospital)
      where pnh.nr_sequencia = r02.nr_seq_nota_hosp;

    end loop;    
    
    
    -- atualiza as informacoes da nota servico
    for r03 in c03(r01.nr_sequencia) loop
    
      cd_unimed_autorizadora_w     := r03.cd_unimed_destino;
      cd_prestador_w               := null;
      nm_profissional_prestador_w  := null;
      nr_cbo_exec_w                := null;
      nr_cgc_cpf_w                 := null;
      
      cd_item_unico_w              := pls_obter_id_item_unico(r03.nr_seq_conta_proc, r03.nr_seq_conta_mat,r03.dt_procedimento,r03.cd_unimed_origem,r03.nr_seq_item);

      -- se for prestador de intercambio
      if nvl(r03.cd_unimed_prestador,'X') <> nvl(r03.cd_unimed_origem,'X') then
        begin
          select 	lpad(d.nr_sequencia,8,'0'),
            rpad(nvl(elimina_caractere_especial(pls_obter_dados_prest_inter(d.nr_sequencia,'N')),' '),60,' '),
            lpad(nvl(pls_obter_dados_prest_inter(d.nr_sequencia,'CBO'),'0'),6,'0')
          into 	cd_prestador_w,
            nm_profissional_prestador_w,
            nr_cbo_exec_w
          from 	pls_prestador_intercambio d
          where 	lpad(d.nr_cpf, 14, '0') = lpad(r03.nr_cgc_cpf, 14, '0');
        exception
          when others then
            null;
        end;
      end if;
      
      if r03.ie_tipo_pessoa_prestador = 'F' then
         nr_cgc_cpf_w := substr(lpad(r03.nr_cgc_cpf,'14','0'),-11);
      end if;
      
      update ptu_nota_servico
         set cd_unimed_autorizadora       = nvl(cd_unimed_autorizadora_w,cd_unimed_autorizadora)
            ,cd_prestador                 = nvl(cd_prestador_w,cd_prestador)
            ,nm_profissional_prestador    = nvl(nm_profissional_prestador_w,nm_profissional_prestador)
            ,nr_cbo_exec                  = nvl(nr_cbo_exec_w,nr_cbo_exec)
            ,nr_cgc_cpf                   = nvl(nr_cgc_cpf_w,nr_cgc_cpf)
            ,cd_item_unico                = nvl(cd_item_unico_w,cd_item_unico)
       where nr_sequencia                 = r03.nr_seq_servic;
       
       if r03.tp_pessoa = 'F' then
         update ptu_nota_cobranca
            set nr_cnpj_cpf                 = nvl(nr_cgc_cpf_w,nr_cnpj_cpf)
          where nr_sequencia                = r03.nr_seq_cobranca;
       end if;
    
    end loop;
end loop;

end pls_ajusta_fatura_remido;
/
