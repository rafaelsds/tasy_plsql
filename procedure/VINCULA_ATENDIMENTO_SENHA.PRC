create or replace
procedure Vincula_Atendimento_Senha (nr_seq_pac_senha_fila_p	number,
									cd_pessoa_fisica_p			number,
									cd_estabelecimento_p		number,
									ie_acao_p					varchar2,
									nr_atendimento_p			number,
									nm_usuario_p				Varchar2) is 
						
/*
ie_acao_p:
V - Vincular senha
D - Desvincular senha
*/
						
ie_inicia_atend_vinc_senha_w	varchar2(1);
ie_vinc_descv_senha_agenda_w	varchar2(1);
nr_seq_pac_senha_fila_w			atendimento_paciente.nr_seq_pac_senha_fila%type;

begin

obter_param_usuario(10021, 68, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_inicia_atend_vinc_senha_w);
obter_param_usuario(916, 1180, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_vinc_descv_senha_agenda_w);


if	(nr_seq_pac_senha_fila_p is not null) and
	(ie_acao_p = 'V')then
	update	paciente_senha_fila
	set	dt_vinculacao_senha 	= sysdate,
		nm_usuario				= nm_usuario_p,
		cd_pessoa_fisica 		= cd_pessoa_fisica_p
	where	nr_sequencia 		= nr_seq_pac_senha_fila_p;
	
	if (nvl(ie_vinc_descv_senha_agenda_w, 'N') = 'S') then
		-- AGENDA DE SERVI�OS e AGENDA DE CONSULTAS
		update agenda_consulta
		set	nr_seq_pac_senha_fila = nr_seq_pac_senha_fila_p
		where nr_atendimento = nr_atendimento_p;
		-- AGENDA DE EXAMES e AGENDA CIR�RGICA
		update agenda_paciente
		set	nr_seq_pac_senha_fila = nr_seq_pac_senha_fila_p
		where nr_atendimento = nr_atendimento_p;
		-- AGENDA DE QUIMIOTERAPIA
		update agenda_quimio
		set	nr_seq_pac_senha_fila = nr_seq_pac_senha_fila_p
		where nr_atendimento = nr_atendimento_p;
	end if;
	
	if	(ie_inicia_atend_vinc_senha_w = 'S') then
		iniciar_finalizar_atend_senha(nr_seq_pac_senha_fila_p, 'I', nm_usuario_p, cd_estabelecimento_p, 'N');
	end if;
	
	--Atualizar o hist�rico de senhas vinculadas
	insert into senhas_atendimento (	
			nr_sequencia,               
			nr_seq_pac_senha_fila,
			nr_atendimento,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec
		) values(	senhas_atendimento_seq.nextval,
			nr_seq_pac_senha_fila_p,
			nr_atendimento_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p);
	
	
elsif	(ie_acao_p = 'D') then
	begin
	
	update	paciente_senha_fila
	set	dt_vinculacao_senha 	= null,
		nm_usuario				= nm_usuario_p,
		cd_pessoa_fisica 		= null
	where	nr_sequencia 		= nr_seq_pac_senha_fila_p;
	
	delete senhas_atendimento where nr_seq_pac_senha_fila = nr_seq_pac_senha_fila_p;
	
	select	max(nr_seq_pac_senha_fila)
	into	nr_seq_pac_senha_fila_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	--Desvincular a senha na EUP
	if	(nr_seq_pac_senha_fila_w = nr_seq_pac_senha_fila_p)then
		update	atendimento_paciente
		set		ds_senha_qmatic = null,
			nr_seq_pac_senha_fila = null
		where 	nr_atendimento	= nr_atendimento_p;
		
		if (nvl(ie_vinc_descv_senha_agenda_w, 'N') = 'S') then
			-- AGENDA DE SERVI�OS e AGENDA DE CONSULTAS
			update agenda_consulta
			set	nr_seq_pac_senha_fila = null
			where nr_atendimento = nr_atendimento_p;
			-- AGENDA DE EXAMES e AGENDA CIR�RGICA
			update agenda_paciente
			set	nr_seq_pac_senha_fila = null
			where nr_atendimento = nr_atendimento_p;
			-- AGENDA DE QUIMIOTERAPIA
			update agenda_quimio
			set	nr_seq_pac_senha_fila = null
			where nr_atendimento = nr_atendimento_p;
		end if;		
	end if;	
	
	commit;
	iniciar_finalizar_atend_senha(nr_seq_pac_senha_fila_p, 'DIF', nm_usuario_p, cd_estabelecimento_p, 'N');
	
	exception
	when others then
		null;
	end;
end if;

commit;

end Vincula_Atendimento_Senha;
/