create or replace
procedure generate_schedule_by_procedure(	
				nr_atendimento_p		number,
				cd_pessoa_fisica_p		varchar2,
				cd_agenda_p				number,
				dt_agenda_p				date,
				hr_inicio_p				date,
				cd_medico_p				number,
				nr_seq_proc_interno_p	number,
				nr_seq_proc_cpoe_p		number,
				cd_convenio_p			number,
				cd_estabelecimento_p	number,
				nm_usuario_p			varchar2,
				result_out_p			out varchar2) is

cd_medico_w					agenda.cd_pessoa_fisica%type;
nr_seq_agenda_w 			agenda_paciente.nr_sequencia%type;
ie_status_agenda_w			agenda_paciente.ie_status_agenda%type;
cd_procedimento_w			proc_interno.cd_procedimento%type;
ie_origem_proced_w			proc_interno.ie_origem_proced%type;
ds_cirurgia_w				proc_interno.ds_proc_exame%type;
nm_pessoa_fisica_w			pessoa_fisica.nm_pessoa_fisica%type;
dt_nascimento_w				pessoa_fisica.dt_nascimento%type;
qt_idade_w					varchar(50);
ds_observacao_w				cpoe_procedimento.ds_observacao%type;
ie_lado_w					cpoe_procedimento.ie_lado%type;
cd_categoria_w				atend_categoria_convenio.cd_categoria%type;
nr_doc_convenio_w			atend_categoria_convenio.nr_doc_convenio%type;
dt_validade_carteira_w		atend_categoria_convenio.dt_validade_carteira%type;
cd_usuario_convenio_w		atend_categoria_convenio.cd_usuario_convenio%type;
qt_count_agenda_w           number(3):= 0;
nr_minuto_int_w             agenda.nr_minuto_intervalo%type;
ie_overlap_param_w			varchar2(1):= 'N';
nr_seq_agenda_aux_w			agenda_paciente_auxiliar.nr_sequencia%type;
begin

Obter_Param_Usuario(2314,52,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_overlap_param_w);

select nr_minuto_intervalo
into nr_minuto_int_w
from agenda
where cd_agenda = cd_agenda_p
and ie_situacao='A';

select count(*) 
into  qt_count_agenda_w
from
(select nr_sequencia 
from agenda_paciente
where cd_pessoa_fisica= cd_pessoa_fisica_p
and dt_agenda = dt_agenda_p 
and ((hr_inicio_p  between (hr_inicio) and hr_inicio+(NR_MINUTO_DURACAO/24/60) - (1/24/60/60))
    or (hr_inicio_p + (nr_minuto_int_w/24/60) between (hr_inicio + (1/24/60/60)) and hr_inicio+(NR_MINUTO_DURACAO/24/60))
	or ((hr_inicio_p  < hr_inicio)
                    AND (hr_inicio_p + (nr_minuto_int_w/24/60) > hr_inicio+(NR_MINUTO_DURACAO/24/60))))
and ie_status_agenda NOT IN ('L','F','I','C','B')
union
select nr_sequencia 
from agenda_consulta
where cd_pessoa_fisica= cd_pessoa_fisica_p  
and ((hr_inicio_p  between (dt_agenda) and dt_agenda+(NR_MINUTO_DURACAO/24/60) - (1/24/60/60))
    or (hr_inicio_p + (nr_minuto_int_w/24/60) between (dt_agenda + (1/24/60/60)) and dt_agenda+(NR_MINUTO_DURACAO/24/60))
	or ((hr_inicio_p  < dt_agenda)
                            AND (hr_inicio_p + (nr_minuto_int_w/24/60) > dt_agenda+(NR_MINUTO_DURACAO/24/60))))
and ie_status_agenda NOT IN ('L','F','I','C','B')
);

if( qt_count_agenda_w > 0 and ie_overlap_param_w ='I') then
	result_out_p:= result_out_p||' '|| dt_agenda_p;
    goto finishPro;
end if;

if(qt_count_agenda_w > 0 and ie_overlap_param_w = 'A') then
	result_out_p:= result_out_p||' '|| dt_agenda_p;
end if;

select	nvl(max(nr_sequencia), 0)
into	nr_seq_agenda_w
from	agenda_paciente
where	cd_agenda		= cd_agenda_p
and		dt_agenda		= dt_agenda_p
and		hr_inicio		= hr_inicio_p;

if	(nr_seq_agenda_w > 0) then
	begin

	select	max(ie_status_agenda)
	into	ie_status_agenda_w
	from 	agenda_paciente
	where	nr_sequencia	= nr_seq_agenda_w;

	cd_procedimento_w	:= null;
	ie_origem_proced_w	:= null;

	if	(nr_seq_proc_interno_p > 0) then
		begin

		Obter_Proc_Tab_Inter_Agenda(nr_seq_proc_interno_p,
				nr_seq_agenda_w, cd_convenio_p, null, null, cd_estabelecimento_p, dt_agenda_p, null, null, null, null, null, null, null, null,
				cd_procedimento_w, ie_origem_proced_w);

		end;
	end if;

	if	(ie_status_agenda_w = 'L') then

		select	max(cd_pessoa_fisica)
		into	cd_medico_w
		from	agenda
		where	cd_agenda = cd_agenda_p;

		select	max(ds_observacao),
				max(ie_lado)
		into	ds_observacao_w,
				ie_lado_w
		from	cpoe_procedimento
		where	nr_sequencia = nr_seq_proc_cpoe_p;

		select	max(cd_categoria),
				max(nr_doc_convenio),
				max(dt_validade_carteira),
				max(cd_usuario_convenio)
		into	cd_categoria_w,
				nr_doc_convenio_w,
				dt_validade_carteira_w,
				cd_usuario_convenio_w
		from	atend_categoria_convenio
		where	nr_seq_interno = Obter_Atecaco_atendimento(nr_atendimento_p);

		select	max(ds_proc_exame)
		into	ds_cirurgia_w
		from	proc_interno
		where	nr_sequencia = nr_seq_proc_interno_p;			

		select 	max(obter_idade(dt_nascimento, sysdate, 'A')),
				max(dt_nascimento),
				max(obter_nome_pf(cd_pessoa_fisica))
		into	qt_idade_w,
				dt_nascimento_w,
				nm_pessoa_fisica_w
        from 	pessoa_fisica 
        where 	cd_pessoa_fisica = cd_pessoa_fisica_p;

		update	agenda_paciente
		set		ie_status_agenda		= 'N',
				cd_pessoa_fisica		= cd_pessoa_fisica_p,
				nm_paciente				= nm_pessoa_fisica_w,
				cd_medico				= nvl(cd_medico_w, cd_medico_p),
				nm_usuario				= nm_usuario_p,
				dt_atualizacao			= sysdate,
				cd_procedimento			= cd_procedimento_w,
				ie_origem_proced		= ie_origem_proced_w,
				ie_lado					= ie_lado_w,
				cd_convenio				= cd_convenio_p,
				dt_nascimento_pac		= dt_nascimento_w,
				qt_idade_paciente		= qt_idade_w,
				nr_seq_proc_interno		= decode(nr_seq_proc_interno_p, 0, null, nr_seq_proc_interno_p),
				ds_observacao			= ds_observacao_w,
				dt_agendamento			= sysdate,
				nm_usuario_orig			= nm_usuario_p,
				nr_telefone				= obter_fone_pac_agenda(cd_pessoa_fisica_p),
				cd_categoria 			= cd_categoria_w,
				nr_doc_convenio 		= nr_doc_convenio_w,
				ds_cirurgia 			= ds_cirurgia_w,
				dt_validade_carteira 	= dt_validade_carteira_w,
				cd_usuario_convenio		= cd_usuario_convenio_w 
		where	nr_sequencia			= nr_seq_agenda_w;

		select 	max(nr_sequencia)
		into 	nr_seq_agenda_aux_w
		from 	agenda_paciente_auxiliar apa
		where 	apa.nr_seq_agenda = nr_seq_agenda_w;
		if (nvl(nr_seq_agenda_aux_w,0) > 0) then
			update	agenda_paciente_auxiliar
			set	 	nr_seq_cpoe_procedimento= nr_seq_proc_cpoe_p
			where 	nr_sequencia = nr_seq_agenda_aux_w;
		else
			INSERT INTO agenda_paciente_auxiliar (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_cpoe_procedimento,
				nr_seq_agenda)
			values(
				agenda_paciente_auxiliar_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_proc_cpoe_p,
				nr_seq_agenda_w);
		end if;
	end if;
	end;
end if;
	<<finishPro>>
	null;	 
end generate_schedule_by_procedure;
/ 