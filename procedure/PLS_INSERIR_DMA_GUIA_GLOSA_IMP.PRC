create or replace
procedure pls_inserir_dma_guia_glosa_imp(	nr_seq_dma_guia_p				varchar2,
						cd_glosa_p					varchar2,
						ds_glosa_p					varchar2,
						nm_usuario_p					usuario.nm_usuario%type) is 

begin
	
insert	into pls_dma_guia_glosa_imp
	(nr_sequencia, dt_atualizacao, nm_usuario,
	 dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_dma_guia,
	 cd_glosa, ds_glosa )
values	(pls_dma_prot_glosa_imp_seq.nextval, sysdate, nm_usuario_p,
	 sysdate, nm_usuario_p, nr_seq_dma_guia_p,
	 cd_glosa_p, ds_glosa_p );
	
commit;

end pls_inserir_dma_guia_glosa_imp;
/