CREATE OR REPLACE
PROCEDURE Qua_limpar_ultimo_acesso(	nr_sequencia_p	number,
						ie_opcao_p		varchar2,
						nm_usuario_p		varchar2) is

/* ie_opcao_p
DQ: Documento qualidade
NC: N�o conformidade
*/

nr_seq_doc_log_w	number(10,0);
nr_seq_nc_leitura_w	number(10,0);

Begin

/* Limpar acesso do usu�rio no documento da qualidade */
if	(ie_opcao_p = 'DQ') then
	begin
	select	max(a.nr_sequencia)
	into	nr_seq_doc_log_w
	from	qua_doc_log_acesso a
	where	a.nr_seq_doc	= nr_sequencia_p
	and	a.nm_usuario	= nm_usuario_p;

	update	qua_doc_log_acesso
	set	dt_leitura		= null,
		dt_atualizacao	= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_doc_log_w;
	end;
elsif	(ie_opcao_p = 'NC') then
	begin
	select	max(a.nr_sequencia)
	into	nr_seq_nc_leitura_w
	from	qua_nao_conform_leitura a
	where	a.nr_seq_nao_conform	= nr_sequencia_p
	and	a.nm_usuario_acesso	= nm_usuario_p;

	update	qua_nao_conform_leitura
	set	dt_acesso	  = null,
		dt_atualizacao  = sysdate
	where	nr_sequencia	  = nr_seq_nc_leitura_w;
	end;
end if;

END Qua_limpar_ultimo_acesso;
/