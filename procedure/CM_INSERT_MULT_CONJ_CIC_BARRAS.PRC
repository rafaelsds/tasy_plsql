create or replace procedure	cm_insert_mult_conj_cic_barras(
			nr_seq_conjuntos_p		varchar2,
			nr_seq_ciclo_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

nr_seq_conj_cont_w		cm_conjunto_cont.nr_sequencia%TYPE;
err_msg_w               cm_ciclo_consiste.ds_consistencia%TYPE;

cursor C01 is
select	a.nr_sequencia
  from cm_conjunto_cont a 
 where a.nr_sequencia in (SELECT regexp_substr(nr_seq_conjuntos_p,'[^,]+', 1, level) FROM dual connect by regexp_substr(nr_seq_conjuntos_p, '[^,]+', 1, level) is not NULL);

begin
    open C01;
	loop
	fetch C01 into
    nr_seq_conj_cont_w;
    exit when C01%notfound;
        begin 
            cm_inserir_conj_ciclo_barras(nr_seq_conj_cont_w, nr_seq_ciclo_p, cd_estabelecimento_p, nm_usuario_p);
        exception
            when others then
                err_msg_w := get_shortened_error_msg(SQLERRM);
                insert into cm_ciclo_consiste (nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, cd_conjunto, ds_consistencia, nr_seq_ciclo)
                values (cm_ciclo_consiste_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p, nr_seq_conj_cont_w, err_msg_w, nr_seq_ciclo_p);
                commit;
        end;
    end loop;
    close C01;
end cm_insert_mult_conj_cic_barras;
/