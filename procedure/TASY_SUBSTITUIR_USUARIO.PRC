CREATE OR REPLACE
PROCEDURE TASY_SUBSTITUIR_USUARIO(nm_usuario_old_p	varchar2,
				nm_usuario_new_p 	varchar2,
				nm_usuario_p		varchar2) is

nm_tabela_w	varchar2(40);
nm_constraint_w	varchar2(40);
nm_campo_w	varchar2(40);
qt_registro_w	number(10);
qt_tamanho_w	number(10);
ds_comando_w	varchar2(3000);
qt_inicial_w	number(10);
qt_final_w	number(10);


cursor C01 is
select 	TABLE_NAME,
	CONSTRAINT_NAME,
	substr(COLUMN_NAME,1,40)
from user_cons_columns
where CONSTRAINT_NAME like '%USUARIO%'
and TABLE_NAME <> 'USUARIO';


BEGIN

qt_registro_w	:= 0;
qt_tamanho_w	:= 0;

/* Validar usuarios a serem substituidos */
if 	(nm_usuario_old_p is null)	or
 	(nm_usuario_old_p = ' ')	or
 	(nm_usuario_new_p is null)	or
 	(nm_usuario_new_p = ' ')	then
	--Faltou informar Usu�rio de destino ou origem');
	Wheb_mensagem_pck.exibir_mensagem_abort(261601);
end if;

qt_tamanho_w	:= Length(nm_usuario_new_p);
if 	(qt_tamanho_w > 15) 	or
	(qt_tamanho_w < 3)	then
	--Usu�rio de destino deve ter entre 3 e 15 d�gitos');
	Wheb_mensagem_pck.exibir_mensagem_abort(261602);
end if;

select 	count(*)
into	qt_registro_w
from	usuario
where	nm_usuario = nm_usuario_old_p;
if	(qt_registro_w <> 1) then
	--Usu�rio original n�o existe ou esta duplicado');
	Wheb_mensagem_pck.exibir_mensagem_abort(261603);
end if;

select 	count(*)
into	qt_registro_w
from	usuario
where	nm_usuario = nm_usuario_new_p;
if	(qt_registro_w > 0) then
	--Usu�rio de destino j� existe');
	Wheb_mensagem_pck.exibir_mensagem_abort(261605);
end if;

/* Verificar quantidade de constraints desabilitadas - inicial */
select	count(*)
into	qt_inicial_w
from	user_constraints
where	constraint_name like '%USUARIO%'
and	status	= 'DISABLED';

/* Desabilitar constraints */
open C01;
loop
fetch c01 into
	nm_tabela_w,
	nm_constraint_w,
	nm_campo_w;
	exit when c01%notfound;
	BEGIN
	ds_comando_w  := ('ALTER TABLE '||nm_tabela_w||
				' DISABLE CONSTRAINT '||nm_constraint_w);
	exec_sql_dinamico('TASY', ds_comando_w);
	END;
end loop;
close C01;

/* Alterar usu�rio */
open C01;
loop
fetch c01 into
	nm_tabela_w,
	nm_constraint_w,
	nm_campo_w;
	exit when c01%notfound;
	BEGIN
	ds_comando_w  := ('UPDATE '||nm_tabela_w||' SET '||nm_campo_w||' = '||
			chr(39)||nm_usuario_new_p||chr(39)||' WHERE '||
			nm_campo_w||' = '||chr(39)||nm_usuario_old_p||chr(39));
	exec_sql_dinamico('TASY', ds_comando_w);
	commit;
	END;
end loop;
close C01;

begin
update 	usuario
set	nm_usuario 	= nm_usuario_new_p,
	nm_usuario_orig	= nm_usuario_old_p,
	nm_usuario_atual = nm_usuario_p,
	dt_atualizacao	= sysdate
where	nm_usuario = nm_usuario_old_p;
exception
	when others then
	--Erro ao alterar usu�rio original');
	Wheb_mensagem_pck.exibir_mensagem_abort(261606);
end;
commit;

/* Habilitar constraints */
open C01;
loop
fetch c01 into
	nm_tabela_w,
	nm_constraint_w,
	nm_campo_w;
	exit when c01%notfound;
	BEGIN
	ds_comando_w  := ('ALTER TABLE '||nm_tabela_w||
				' ENABLE CONSTRAINT '||nm_constraint_w);
	exec_sql_dinamico('TASY', ds_comando_w);
	END;
end loop;
close C01;

/* Verificar quantidade de constraints desabilitadas - final */
select	count(*)
into	qt_final_w
from	user_constraints
where	constraint_name like '%USUARIO%'
and	status	= 'DISABLED';

if	(qt_final_w > qt_inicial_w) then
	--Processo deixou constraints desabilitadas, verifique c/suporte');
	Wheb_mensagem_pck.exibir_mensagem_abort(261607);
end if;


END TASY_SUBSTITUIR_USUARIO;
/
