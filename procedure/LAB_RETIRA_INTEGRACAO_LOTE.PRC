create or replace
procedure lab_retira_integracao_lote( nr_seq_lote_p		varchar2,
									  cd_equipamento_p  varchar2,	
									  nm_usuario_p		Varchar2) is 


Cursor C01 is
	select	a.nr_prescricao,
			a.nr_sequencia,
			a.nr_seq_exame
	from	prescr_procedimento a,
			lab_lote_exame_item b
	where	a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia 	= b.nr_seq_prescr
	and		a.nr_seq_exame is not null
	and		a.dt_integracao is not null
	and		((cd_equipamento_p is null) or (Obter_Equipamento_Exame(a.nr_seq_exame, cd_equipamento_p,'CI') is not null))
	and		b.nr_seq_lote 	= nr_seq_lote_p	
	order by 1,2;	

c01_w	C01%rowtype;


Cursor C02 is
	select	a.nr_prescricao,
			a.nr_sequencia nr_seq_prescr,
			a.nr_seq_exame,
			c.nr_sequencia
	from	prescr_procedimento a,
			lab_lote_exame_item b,
			prescr_proc_mat_item c
	where	a.nr_prescricao = b.nr_prescricao
	and		a.nr_sequencia 	= b.nr_seq_prescr
	and		a.nr_prescricao = c.nr_prescricao
	and		a.nr_sequencia 	= c.nr_seq_prescr
	and 	a.nr_seq_exame is not null
	and		c.dt_integracao is not null	
	and		((cd_equipamento_p is null) or (Obter_Equipamento_Exame(a.nr_seq_exame, cd_equipamento_p,'CI') is not null))
	and		b.nr_seq_lote 	= nr_seq_lote_p	
	order by 1,2;	

c02_w	C02%rowtype;


ie_exame_amostra_w		lab_parametro.ie_exame_amostra%type;

									  
begin

select 	nvl(MAX(a.ie_exame_amostra),'N') 
into	ie_exame_amostra_w
from	lab_parametro a,
		prescr_medica b,
		prescr_procedimento c,
		lab_lote_exame_item d
where   a.cd_estabelecimento = b.cd_estabelecimento
and		b.nr_prescricao = c.nr_prescricao
and		c.nr_prescricao = d.nr_prescricao
and		c.nr_sequencia  = d.nr_seq_prescr
and		d.nr_seq_lote = nr_seq_lote_p;

if (nr_seq_lote_p is not null) then
	open C01;
	loop
	fetch C01 into	
		c01_w;
	exit when C01%notfound;
		begin
		
		update prescr_procedimento
		set dt_integracao = null,
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p
		where nr_prescricao = c01_w.nr_prescricao
		and	  nr_sequencia  = c01_w.nr_sequencia;
		
		commit;
		
		end;
	end loop;
	close C01;
	
	if (ie_exame_amostra_w = 'S') then
		open C02;
		loop
		fetch C02 into	
			c02_w;
		exit when C02%notfound;
			begin
			
			update 	prescr_proc_mat_item
			set	dt_integracao = null,
				dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p
			where	nr_sequencia = c02_w.nr_sequencia;
			
			update prescr_procedimento
			set dt_integracao = null,
				dt_atualizacao = sysdate,
				nm_usuario = nm_usuario_p
			where nr_prescricao = c02_w.nr_prescricao
			and	  nr_sequencia  = c02_w.nr_seq_prescr;
			
			commit;
			
			end;
		end loop;
		close C02;
	
	end if;

end if;

commit;

end lab_retira_integracao_lote;
/