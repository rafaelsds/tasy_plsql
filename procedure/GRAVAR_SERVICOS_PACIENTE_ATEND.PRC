create or replace
procedure gravar_servicos_paciente_atend(
			nr_atendimento_p	number,
			nr_seq_servico_p	number,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number,
			lancar_conta_p		varchar2) is 

begin

insert into atend_pac_servico_conta 	(nr_sequencia,
					cd_estabelecimento,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_atendimento,
					nr_seq_servico)
			values         (atend_pac_servico_conta_seq.nextval,
					cd_estabelecimento_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_atendimento_p,
					nr_seq_servico_p);

if	(nvl(lancar_conta_p,'N') = 'S') then
	gerar_servicos_pac_conta(nr_atendimento_p,nm_usuario_p,nr_seq_servico_p);
end if;
					
commit;
end gravar_servicos_paciente_atend;
/