create or replace
procedure atualiza_liberacao_sinal_vital(nm_usuario_p		Varchar2,	
					nr_cirurgia_p		number,
					nr_seq_pepo_p		number) is 

begin

if 	(nr_cirurgia_p is not null) or
	(nr_seq_pepo_p is not null) then
	
	update	atendimento_sinal_vital
	set		dt_liberacao	=	sysdate,
			dt_atualizacao	=	sysdate,
			nm_usuario		=	nm_usuario_p
	where	((nr_cirurgia	= nr_cirurgia_p) or (nr_cirurgia_p is null))
	and		((nr_seq_pepo 	= nr_seq_pepo_p) or (nr_seq_pepo_p is null))
	and		dt_liberacao is null;
	
	update	atendimento_monit_resp
	set		dt_liberacao	=	sysdate,
			dt_atualizacao	=	sysdate,
			nm_usuario		=	nm_usuario_p
	where	((nr_cirurgia 	= nr_cirurgia_p) or (nr_cirurgia_p is null))
	and		((nr_seq_pepo 	= nr_seq_pepo_p) or (nr_seq_pepo_p is null))
	and		dt_liberacao is null;

end if;

commit;

end atualiza_liberacao_sinal_vital;
/