create or replace
procedure gerar_pre_fatur_tributo(
				nm_usuario_p		varchar2,
				dt_referencia_p		date,
				cd_estab_p		number) is 

vl_pre_faturamento_w	pre_faturamento.vl_material%type;

cursor c01 is
	select  nr_sequencia,
    	cd_conta_contabil,
    	cd_estabelecimento,
    	(nvl(vl_material,0) + nvl(vl_procedimento,0)) vl_pre_faturamento,
    	dt_referencia,
    	nr_interno_conta,
		obter_tipo_convenio(cd_convenio) ie_tipo_convenio
  	from  pre_faturamento
  	where dt_referencia = dt_referencia_p
	and  (cd_estabelecimento = cd_estab_P or cd_estab_P = 0)
	order by 1;

c01_w		c01%rowtype;

cursor c02 is
	select 	a.cd_tributo,
				a.pr_tributo,
				a.cd_conta_debito,
				a.cd_conta_credito
	from		ctb_regra_tributo_pre_fat a,
			  	tributo b
	where	a.cd_tributo = b.cd_tributo
  	and   	a.cd_empresa = obter_empresa_estab(c01_w.cd_estabelecimento)
	and 		nvl(a.cd_estabelecimento, c01_w.cd_estabelecimento) = c01_w.cd_estabelecimento
	and 		nvl(a.ie_tipo_convenio, c01_w.ie_tipo_convenio) = c01_w.ie_tipo_convenio
  	and 		b.ie_situacao = 'A'
	order by 1;

c02_w		c02%rowtype;

begin

open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin

	open c02;
	loop
	fetch c02 into
		c02_w;
	exit when c02%notfound;
		begin
		
		vl_pre_faturamento_w := (c01_w.vl_pre_faturamento * c02_w.pr_tributo) / 100;

		insert into pre_fatur_tributo(
					nr_sequencia,
					nr_seq_pre_fatur,
					cd_tributo,
					pr_tributo,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					vl_tributo,
					cd_conta_debito,
					cd_conta_credito,
					dt_referencia,
					nr_interno_conta)
				values(	pre_fatur_tributo_seq.nextval,
					c01_w.nr_sequencia,
					c02_w.cd_tributo,
					c02_w.pr_tributo,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					vl_pre_faturamento_w,
					c02_w.cd_conta_debito,
					c02_w.cd_conta_credito,
					c01_w.dt_referencia,
					c01_w.nr_interno_conta);
		end;
	end loop;
	close c02;

	end;
end loop;
close c01;

commit;

end gerar_pre_fatur_tributo;
/
