create or replace procedure rel_dvt_report_ger(dt_inicio_p        in date,
                                               dt_fim_p           in date,
                                               nr_age_p           in varchar,
                                               nr_clinical_unit_p in varchar2) is
begin
  -- Call the procedure
  pck_cdsdvt_report.get_data_uti(dt_ini_p => dt_inicio_p,
                                 dt_fim_p => dt_fim_p,
                                 cd_unidade_p => nr_clinical_unit_p,
                                 ds_age_p => nr_age_p,
                                 ie_relatorio_p => 0);

end rel_dvt_report_ger;
/
  