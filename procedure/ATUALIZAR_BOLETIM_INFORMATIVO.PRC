Create or Replace
procedure atualizar_boletim_informativo(
                         	nr_seq_tipo_p     	NUMBER) IS


dt_evolucao_w		Date;
nr_atendimento_w	Number(10);
ds_evolucao_w		Varchar2(30000);
nm_usuario_w		Varchar2(15);
dt_atualizacao_w	Date;	
nr_sequencia_w		Number(10);
nr_cont_w		Number(10) := 0;

cursor C01 is
select	dt_evolucao,
	nr_atendimento,
	ds_evolucao,
	nm_usuario,
	dt_atualizacao
from	evolucao_paciente
where	ie_tipo_evolucao = nr_seq_tipo_p
and	nr_atendimento is not null;

begin

open C01;
loop
	fetch C01 into 	dt_evolucao_w,
			nr_atendimento_w,
			ds_evolucao_w,
			nm_usuario_w,
			dt_atualizacao_w;
	exit when C01%notfound;
	
	select	atendimento_boletim_seq.nextval
	into	nr_sequencia_w
	from	dual;	

	insert into atendimento_boletim(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		dt_boletim,
		ds_boletim)
	values(	nr_sequencia_w,
		dt_atualizacao_w,
		nm_usuario_w,
		nr_atendimento_w,
		dt_evolucao_w,
		substr(ds_evolucao_w,1,4000));

	nr_cont_w	:= nr_cont_w + 1;
	if	(nr_cont_w = 1000) then	
		commit;
		nr_cont_w := 0;
	end if;

	end loop;
close C01;

commit;

end atualizar_boletim_informativo;
/