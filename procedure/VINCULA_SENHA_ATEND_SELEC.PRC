create or replace
procedure Vincula_senha_atend_selec(	nr_seq_senha_p		varchar2,
										cd_pessoa_fisica_p	varchar2,
										nm_usuario_p 		varchar2,
										ds_lista_atend_p	varchar2) is 	
										
nr_prescricao_w	number	(14);
nr_seq_prescr_w	number	(7);
nr_atendimento_w number (10);
	
Cursor C01 is									
SELECT  a.NR_PRESCRICAO,
		b.NR_SEQUENCIA
FROM 	ATENDIMENTO_PACIENTE C,
		PRESCR_PROCEDIMENTO B, 
		PRESCR_MEDICA A, 
		PROC_INTERNO M
WHERE A.NR_PRESCRICAO   = B.NR_PRESCRICAO
AND	  A.NR_ATENDIMENTO  = C.NR_ATENDIMENTO
AND   C.NR_ATENDIMENTO  = nr_atendimento_w
AND   B.NR_SEQ_PROC_INTERNO = M.NR_SEQUENCIA(+)
AND   A.DT_LIBERACAO    IS NOT NULL 
AND   NVL(B.IE_SUSPENSO,'N')	<> 'S'
AND   NVL(M.IE_EXAME_GESTAO_ENTREGA,'S') = 'S'
AND SUBSTR(GEL_OBTER_ENVELOPE_EXAME(B.NR_SEQUENCIA,B.NR_PRESCRICAO,B.NR_SEQ_EXAME,'E'),1,10) IS NULL
AND C.CD_PESSOA_FISICA = cd_pessoa_fisica_p
ORDER BY 1 DESC;

Cursor C02 is
SELECT regexp_substr(ds_lista_atend_p, '[^, ]+', 1, LEVEL)
FROM dual
CONNECT BY regexp_substr(ds_lista_atend_p, '[^, ]+', 1, LEVEL) IS NOT NULL;

begin

open C02;
loop
fetch C02 into
	nr_atendimento_w;
exit when C02%notfound;
	begin

	open C01;
	loop
	fetch C01 into
			nr_prescricao_w,
			nr_seq_prescr_w;
	exit when C01%notfound;
		begin
		/*TO DO*/
		 insert into SENHA_RETIRADA_LAUDO(	nr_sequencia,                       
											dt_atualizacao,                                              
											nr_seq_senha,                       
											nm_usuario,                         
											nr_prescricao,                      
											nr_seq_prescricao)                  
								values (	SENHA_RETIRADA_LAUDO_SEQ.nextval,                    
											sysdate,                                                
											nr_seq_senha_p,                    
											nm_usuario_p,                      
											nr_prescricao_w,                       
											nr_seq_prescr_w);
		end;
	end loop;
	close C01;
	end;
end loop;
close C02;
commit;

end Vincula_senha_atend_selec;
/