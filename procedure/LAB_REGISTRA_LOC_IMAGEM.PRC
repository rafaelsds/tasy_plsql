create or replace
procedure lab_registra_loc_imagem	(nr_acesso_dicom_p	varchar2,
					 cd_setor_atendimento_p	varchar2,
					 nm_usuario_p		Varchar2) is 

nr_prescricao_w		number;
nr_seq_prescr_w		number;

Cursor C01 is
	select	b.nr_sequencia, 
		b.nr_prescricao
	from	prescr_procedimento b,
		prescr_medica	a
	where	a.nr_prescricao = b.nr_prescricao
	and	b.nr_acesso_dicom = nr_acesso_dicom_p;
				
begin

open C01;
loop
fetch C01 into	
	nr_seq_prescr_w,
	nr_prescricao_w;
exit when C01%notfound;
	begin
		insert into prescr_proc_imagem	
			(nr_sequencia,
			 nr_prescricao,
			 nr_seq_prescr,
			 cd_setor_atendimento,
			 nm_usuario,
			 dt_atualizacao,
			 nm_usuario_nrec,
			 dt_atualizacao_nrec)
		values	(prescr_proc_imagem_seq.nextval,
			 nr_prescricao_w,
			 nr_seq_prescr_w,
			 cd_setor_atendimento_p,
			 nm_usuario_p,
			 sysdate,
			 nm_usuario_p,
			 sysdate);
	end;
end loop;
close C01;

commit;

end lab_registra_loc_imagem;
/