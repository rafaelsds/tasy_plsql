create or replace
procedure gerar_w_gestao_vendas(	dt_inicial_p	date,
					dt_final_p	date,
					nr_seq_canal_p	number,
					nm_usuario_p	varchar2) is 

dt_dia_w		date;
dt_inicial_prospect_w	date;
nr_seq_cliente_w	number(10);
qt_cliente_dia_w	number(10);
qt_dias_w		number(10);
qt_total_ativo_w	number(10);
dt_final_prospect_w	date;
ds_retorno_w		number(15,2);
dt_abertura_canal_w	date;
dt_fechamento_canal_w	date;
qt_aberto_periodo_w	number(10);
nr_seq_canal_cliente_w	number(10);

Cursor C01 is
	select	trunc(dt_dia,'dd')
	from	dia_v
	where	trunc(dt_dia,'month')	between dt_inicial_p and dt_final_p;
	
Cursor C02 is
	select	a.nr_sequencia
	from	com_cliente a
	where	exists (	select	1
				from	com_cliente_log x
				where	a.nr_sequencia	= x.nr_seq_cliente
				and	x.ie_log	= 2);

begin

qt_aberto_periodo_w := 0;

delete from w_gestao_vendas; commit;

open C01;
loop
fetch C01 into
	dt_dia_w;
exit when C01%notfound;
	begin
	qt_cliente_dia_w	:= 0;
	qt_dias_w		:= qt_dias_w + 1;
	
	open C02;
	loop
	fetch C02 into
		nr_seq_cliente_w;
	exit when C02%notfound;
		begin
		/* Data inicial do prospect */
		select	max(a.dt_log)
		into	dt_inicial_prospect_w
		from	com_cliente_log a
		where	a.ie_log		= 2
		and	a.ie_classificacao	= 'P'
		and	a.nr_seq_cliente	= nr_seq_cliente_w;
		
		if (dt_inicial_prospect_w is not null) then
			/* Data final do prospect */
			select	nvl(max(a.dt_log),sysdate)
			into	dt_final_prospect_w
			from	com_cliente_log a
			where	a.ie_log		= 2
			and	nvl(a.dt_log,sysdate)	>= dt_inicial_prospect_w
			and	a.ie_classificacao	<> 'P'
			and	a.nr_seq_cliente	= nr_seq_cliente_w;
			
			if (nvl(nr_seq_canal_p,0) <> 0) then
				/* Data de abertura do canal */
				/*select	max(a.dt_log)
				into	dt_abertura_canal_w
				from	com_cliente_log a
				where	a.nr_seq_canal	= nr_seq_canal_p
				and	a.ie_log	= 3
				and	a.nr_seq_cliente	= nr_seq_cliente_w;*/
				
				--if (dt_abertura_canal_w is null) then
				select	nvl(max(a.dt_inicio_atuacao),sysdate + 1)
				into	dt_abertura_canal_w
				from	com_canal_cliente a
				where	a.nr_seq_canal	= nr_seq_canal_p
				and	a.nr_seq_cliente	= nr_seq_cliente_w
				and	a.ie_tipo_atuacao	= 'V';
				--end if;
				
				/* Data de fechamento do canal */
				/*select	max(a.dt_log)
				into	dt_fechamento_canal_w
				from	com_cliente_log a
				where	a.nr_seq_canal	= nr_seq_canal_p
				and	a.ie_log	= 4
				and	a.nr_seq_cliente	= nr_seq_cliente_w;*/
				
				--if (dt_fechamento_canal_w is null) then
				select	nvl(max(nr_sequencia),0)
				into	nr_seq_canal_cliente_w
				from	com_canal_cliente
				where	dt_fim_atuacao is null
				and	nr_seq_cliente	= nr_seq_cliente_w
				and	nr_seq_canal	= nr_seq_canal_p
				and	ie_tipo_atuacao	= 'V';
				
				if (nr_seq_canal_cliente_w > 0) then
					dt_fechamento_canal_w	:= sysdate;
				else
					select	nvl(max(a.dt_fim_atuacao),sysdate)
					into	dt_fechamento_canal_w
					from	com_canal_cliente a
					where	a.nr_seq_canal	= nr_seq_canal_p
					and	a.nr_seq_cliente	= nr_seq_cliente_w
					and	a.ie_tipo_atuacao	= 'V'
					and	a.dt_fim_atuacao	>= dt_abertura_canal_w;
				end if;
				--end if;
			end if;
			
			if (trunc(dt_inicial_prospect_w,'dd') = trunc(dt_dia_w,'dd')) then
				if ((nvl(nr_seq_canal_p,0) <> 0) and (dt_dia_w between trunc(dt_abertura_canal_w,'dd') and fim_dia(dt_fechamento_canal_w))) or
					(nvl(nr_seq_canal_p,0) = 0) then
					qt_aberto_periodo_w	:= qt_aberto_periodo_w + 1;
				end if;
			end if;
			
			if (dt_dia_w between trunc(dt_inicial_prospect_w,'dd') and fim_dia(dt_final_prospect_w)) then
				if ((nvl(nr_seq_canal_p,0) <> 0) and (dt_dia_w between trunc(dt_abertura_canal_w,'dd') and fim_dia(dt_fechamento_canal_w))) or
					(nvl(nr_seq_canal_p,0) = 0) then
					qt_cliente_dia_w	:= qt_cliente_dia_w + 1;
				end if;
			end if;
		end if;
		
		end;
	end loop;
	close C02;
	
	insert into w_gestao_vendas (	NR_SEQUENCIA,
					DT_REFERENCIA,
					QT_ATIVOS,
					DT_ATUALIZACAO,
					NM_USUARIO,
					DT_ATUALIZACAO_NREC,
					NM_USUARIO_NREC,
					QT_ABERTO_PERIODO,
					IE_TIPO_INFORMACAO)
				values(	w_gestao_vendas_seq.NextVal,
					dt_dia_w,
					qt_cliente_dia_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					0,
					'A');
	end;
end loop;
close C01;

insert into w_gestao_vendas (	NR_SEQUENCIA,
				DT_REFERENCIA,
				QT_ATIVOS,
				DT_ATUALIZACAO,
				NM_USUARIO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO_NREC,
				QT_ABERTO_PERIODO,
				IE_TIPO_INFORMACAO)
			values(	w_gestao_vendas_seq.NextVal,
				dt_dia_w,
				qt_cliente_dia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nvl(qt_aberto_periodo_w,0),
				'P');

commit;

end gerar_w_gestao_vendas;
/