create or replace
procedure GQA_Gerar_Protocolo_assist(	nr_atendimento_p	 number,
										ie_evento_p			 varchar,
										nm_usuario_p		 Varchar2,
										cd_protocolo_p       number default 0,
										nr_seq_protocolo_p   number default 0,
										cd_classe_material_p number default 0,
										nr_seq_familia_p     number default 0,
										cd_material_p        number default 0,
										nr_prescricao_p		 number default null) is 


					
nr_seq_pend_regra_w	    number(10);	
nr_atendimento_w	    number(10);
cd_pessoa_fisica_w	    varchar2(10);
cd_doenca_cid_w		    varchar2(10);	
qt_idade_w		        number(10);
cd_setor_atendimento_w	number(10);		
cd_classif_setor_w	    varchar2(10);		
qt_idade_dias_w		    number(6);
qt_peso_gramas_w		number(12,3);
qt_reg_w				number(38);
qt_reg_sae_w			number(38);
ie_insert_rule_w		varchar2(80) := 'Y';	
					
Cursor C01 is
	select	a.nr_sequencia
	from	gqa_pendencia_regra a,
		gqa_pendencia b
	where	b.nr_sequencia	= a.nr_seq_pendencia
	and	b.ie_tipo_pendencia	= 6
	and	b.ie_situacao 		= 'A'
	and	a.ie_situacao		= 'A'
	and	obter_se_gqa_regra_liberada(a.nr_sequencia) = 'S'
	and	a.ie_evento			= ie_evento_p
	and	qt_idade_w between nvl(a.qt_idade_min,0) and nvl(a.qt_idade_max,999)
	and     qt_idade_dias_w between nvl(a.qt_dias_min,0) and nvl(a.qt_dias_max,99999)
	and	qt_peso_gramas_w between nvl(a.qt_peso_gramas_min,0) and nvl(a.qt_peso_gramas_max,999999999)
	and	nvl(a.cd_setor_atendimento,nvl(cd_setor_atendimento_w,0))	= nvl(cd_setor_atendimento_w,0)
	and	nvl(cd_classif_setor,nvl(cd_classif_setor_w,0))	= nvl(cd_classif_setor_w,0);
	
Cursor c02 is
	select a.nr_sequencia
	from   gqa_pendencia_regra a,
		   gqa_pendencia b
	where  b.nr_sequencia = a.nr_seq_pendencia
	and	   b.ie_tipo_pendencia	= 6
	and	   b.ie_situacao = 'A'
	and	   a.ie_situacao = 'A'
	and	   obter_se_gqa_regra_liberada(a.nr_sequencia) = 'S'
	and	   a.ie_evento = ie_evento_p
	and	   qt_idade_w between nvl(a.qt_idade_min,0) and nvl(a.qt_idade_max,999)
	and    qt_idade_dias_w between nvl(a.qt_dias_min,0) and nvl(a.qt_dias_max,99999)
	and	   qt_peso_gramas_w between nvl(a.qt_peso_gramas_min,0) and nvl(a.qt_peso_gramas_max,999999999)
	and	   nvl(a.cd_setor_atendimento,nvl(cd_setor_atendimento_w,0)) = nvl(cd_setor_atendimento_w,0)
	and	   nvl(cd_classif_setor,nvl(cd_classif_setor_w,0)) = nvl(cd_classif_setor_w,0)
	and    nvl(cd_protocolo, nvl(cd_protocolo_p,0)) = nvl(cd_protocolo_p,0)
	and    nvl(nr_seq_protocolo, nvl(nr_seq_protocolo_p,0)) = nvl(nr_seq_protocolo_p,0)
	and    nvl(cd_classe_material,cd_classe_material_p) = cd_classe_material_p
	and    nvl(nr_seq_familia,nvl(nr_seq_familia_p,0)) = nvl(nr_seq_familia_p,0)
	and    nvl(cd_material,cd_material_p) = cd_material_p;	
	
	
procedure insere_regra(nr_seq_pend_regra_p number) is

nr_sequencia_w number(10);

begin
	select	gqa_pendencia_pac_seq.nextval
	into	nr_sequencia_w
	from	dual;
		
	insert into gqa_pendencia_pac (	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_pessoa_fisica,
					nr_atendimento,
					NR_SEQ_PEND_REGRA,
					NR_PRESCRICAO)
	values			(nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_pessoa_fisica_w,
					nr_atendimento_w,
					nr_seq_pend_regra_p,
					nr_prescricao_p);
	
	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then 
		commit; 
	end if;
	gerar_consulta_reg_mentor(nm_usuario_p, null, nr_seq_pend_regra_p, ie_evento_p, nr_sequencia_w,null,null,null,nr_prescricao_p);
	GQA_GERAR_ACAO_REGRA(nr_seq_pend_regra_p,nr_sequencia_w,nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p, ie_evento_p);	
end;

begin


select	max(nr_atendimento),
		max(cd_pessoa_fisica)
into	nr_atendimento_w,
		cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

cd_setor_atendimento_w	:= obter_setor_atendimento(nr_atendimento_w);
qt_idade_w		:= obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A');
cd_classif_setor_w	:= obter_classif_setor(cd_setor_atendimento_w);
qt_idade_dias_w		:= obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'DIA');
qt_peso_gramas_w	:= (nvl(obter_peso_pf(cd_pessoa_fisica_w),0) * 1000);

if (ie_evento_p = '7') then
	for regras in c02 loop
		select  count(1)
		into     qt_reg_w
		from    gqa_pendencia_pac a
		where   a.nr_prescricao = nr_prescricao_p
		and     NR_SEQ_PEND_REGRA = regras.nr_sequencia;
	
		if (qt_reg_w = 0) then
			insere_regra(regras.nr_sequencia);
		end if;
	end loop;
	
elsif ((nvl(ie_evento_p,'0') = '8')) then
	select  case when count(*) > 0 then 1 else 0 end
	into 	qt_reg_sae_w
	from 	pe_prescricao pe,
			atendimento_paciente atend
	where 	pe.nr_atendimento = atend.nr_atendimento
	and   	pe.nr_atendimento	= nr_atendimento_p
	and 	pe.ie_situacao = 'A' 
	and 	pe.ie_tipo = 'SAE'
	and 	pe.dt_liberacao is not null
	and   	pe.dt_inativacao is null
	and   	pe.dt_suspensao is null
	and   	atend.dt_alta is null;

	if (qt_reg_sae_w > 0) then	
		for regras in c01 loop
			insere_regra(regras.nr_sequencia);
		end loop;
	end if;
	
elsif ((nvl(ie_evento_p,'0') = '10')) then
	
	for regras in c01 loop
		
		select GQA_regra_complementar(nm_usuario_p, nr_atendimento_p, regras.nr_sequencia)
		into ie_insert_rule_w
		from dual;
		
		if (nvl(ie_insert_rule_w,'Y') = 'X') then
			insere_regra(regras.nr_sequencia);
		end if;
		
	end loop;
else
	for regras in c01 loop
		insere_regra(regras.nr_sequencia);
	end loop;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end GQA_Gerar_Protocolo_assist;
/