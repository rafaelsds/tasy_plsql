create or replace procedure HCBCS_GERAR_W_CONV_RET_MOVTO
			(nr_seq_retorno_p	number) is


type conta is record (nr_interno_conta	number(15), cd_usuario_convenio varchar2(255), nr_nota varchar2(255), nr_sequencia number(15), nr_guia_tr_75 varchar2(20), nr_folha number(10));
type vetor_conta is table of conta index by binary_integer;

vetor_conta_w		vetor_conta;
nr_doc_convenio_arq_w	varchar2(255);
nr_doc_convenio_w	varchar2(255);
cd_usuario_conv_w	varchar2(255);
cd_usuario_conv_arq_w	varchar2(255);
cd_item_w		number(15);
vl_total_pago_w		number(15,2);
vl_glosado_w		number(15,2);
vl_cobrado_w		number(15,2);
vl_pago_arq_w		number(15,2);
vl_glosado_arq_w	number(15,2);
vl_cobrado_arq_w	number(15,2);
cd_motivo_glosa_w	number(15);
ie_tipo_w		varchar2(255);
nm_beneficiario_w	varchar2(255);
nr_nota_arq_w		varchar2(255);
nr_nota_w		varchar2(255);
nr_linha_w		varchar2(10);
cd_convenio_w		number(10);
nr_interno_conta_w	number(10);
nr_seq_protocolo_w	number(10);
nm_paciente_w		varchar2(255);
nr_seq_item_conta_w	number(10);
nr_sequencia_w		number(10);
dt_item_w		varchar2(20);
ie_gerar_resumo_w	varchar2(1);
dt_ano_conta_w		varchar2(4);
dt_execucao_w		date;
nr_seq_movto_w		number(10);
ds_conteudo_w		varchar2(4000);
count_w			number(10);
nr_seq_inicial_w	number(10);
nr_seq_final_w		number(10);
nr_min_seq_w		number(10);
nr_max_seq_w		number(10);
cd_brasindice_w		varchar2(255);
qt_pcnc_w		number(10);
dia_ini_w		number;
cd_user_conv_w		convenio_retorno_movto.cd_usuario_convenio%type;
nr_seq_protocolo_ww	protocolo_convenio.nr_seq_protocolo%type;
valor_conta_w		conta_paciente.vl_conta%type;
valor_conta_arq_w	conta_paciente.vl_conta%type;
cd_estabelecimento_w	convenio_retorno.cd_estabelecimento%type;
nr_guia_tr_75_w		varchar2(20);
i			integer;
k			integer;
j			integer;
nr_folha_w		number := 0;
cd_autorizacao_w	conta_paciente_guia.cd_autorizacao%type;

cursor c00 is
select	substr(ds_conteudo,68,5) nr_nota,
	substr(ds_conteudo,20,13) cd_usuario_convenio,
	substr(ds_conteudo,18,2) nr_folha,
	nr_sequencia,
	decode(substr(ds_conteudo,14,4),'0075',substr(ds_conteudo,62,6),'0085',substr(ds_conteudo,62,6),null) guia_TR75,
	to_date(decode(substr(ds_conteudo,14,4),'0075',substr(ds_conteudo,50,6),'0085',substr(ds_conteudo,50,6),null),'dd/mm/yy') dt_inicio_conta,
	to_date(decode(substr(ds_conteudo,14,4),'0075',substr(ds_conteudo,56,6),'0085',substr(ds_conteudo,56,6),null),'dd/mm/yy')dt_fim_conta
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,14,4)	in ('0075','0076','0077','0085','0086', '0087')
and	substr(ds_conteudo,1,3) 	<> 'SMH'
order by nr_sequencia;

c00_w	c00%rowtype;	

cursor c01 is
select	substr(ds_conteudo,14,2) nr_linha,
	substr(ds_conteudo,14,4) ie_tipo,
	substr(ds_conteudo,62,6) nr_doc_convenio,
	substr(ds_conteudo,68,5) nr_nota,
	substr(ds_conteudo,20,13) cd_usuario_convenio,
	somente_numero(substr(ds_conteudo,38,8)) cd_item,
	somente_numero(substr(ds_conteudo,70,13)) vl_total_pago,
	somente_numero(substr(ds_conteudo,83,9)) vl_glosado,
	somente_numero(substr(ds_conteudo,59,11)) vl_cobrado,
	null nm_paciente,
	substr(ds_conteudo,20,4) dt_item,
	'S' ie_gera_resumo,
	ds_conteudo,
	substr(ds_conteudo,70,12) cd_brasindice
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,14,2) <> '00'
and	nr_sequencia between nr_seq_inicial_w + 1 and nr_seq_final_w - 1
and	 ((somente_numero(substr(ds_conteudo,24,14)) = obter_cgc_estabelecimento(cd_estabelecimento_w) and substr(ds_conteudo,16,2) in ('75','77','85','87')) or
	(substr(ds_conteudo,16,2) in ('76','86')))
and	substr(ds_conteudo,1,3) 	<> 'SMH';

cursor 	c02 is
select	substr(ds_conteudo,1,2) nr_linha,
	substr(ds_conteudo,1,4) ie_tipo,
	null nr_doc_convenio,
	substr(ds_conteudo,49,5) nr_nota,
	substr(ds_conteudo,3,13) cd_usuario_convenio,
	somente_numero(substr(ds_conteudo,26,08)) cd_item,
	somente_numero(substr(ds_conteudo,145,13)) vl_total_pago,
	somente_numero(substr(ds_conteudo,158,13)) vl_glosado,
	somente_numero(substr(ds_conteudo,132,13)) vl_cobrado,
	trim(substr(ds_conteudo,39,43)) nm_paciente,
	somente_numero(substr(ds_conteudo,24,2)) dia_conta,
	somente_numero(substr(ds_conteudo,21,13)) valor_conta,
	nr_sequencia
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,1,3)		<> 'SMH'
and	exists
	(select	1
	from	w_conv_ret_movto x
	where	x.nr_seq_retorno	= nr_seq_retorno_p
	and	substr(x.ds_conteudo,1,4) = '0035')
union all
select	substr(ds_conteudo,1,2) nr_linha,
	substr(ds_conteudo,1,4) ie_tipo,
	null nr_doc_convenio,
	substr(ds_conteudo,47,5) nr_nota,
	substr(ds_conteudo,3,13) cd_usuario_convenio,
	somente_numero(substr(ds_conteudo,18,08)) cd_item,
	somente_numero(substr(ds_conteudo,113,13)) vl_total_pago,
	somente_numero(substr(ds_conteudo,126,9)) vl_glosado,
	somente_numero(substr(ds_conteudo,100,13)) vl_cobrado,
	trim(substr(ds_conteudo,55,45)) nm_paciente,
	somente_numero(substr(ds_conteudo,16,2)) dia_conta,
	somente_numero(substr(ds_conteudo,21,13)) valor_conta,
	nr_sequencia
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,1,3)		<> 'SMH'
and	exists
	(select	1
	from	w_conv_ret_movto x
	where	x.nr_seq_retorno	= nr_seq_retorno_p
	and	substr(x.ds_conteudo,1,4) = '0055')
order by nr_sequencia;


begin
vetor_conta_w.delete;
delete 	from convenio_ret_movto_hist
where	nr_seq_retorno	= nr_seq_retorno_p;

select	max(cd_convenio),
	max(cd_estabelecimento)
into	cd_convenio_w,
	cd_estabelecimento_w
from	convenio_retorno
where	nr_sequencia	= nr_seq_retorno_p;

select	min(nr_sequencia),
	max(nr_sequencia)
into	nr_min_seq_w,
	nr_max_seq_w
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p;

count_w	:= 0;

nr_seq_inicial_w	:= 0;
i 			:= 1;

open C00;
loop
fetch C00 into	
	c00_w;
exit when C00%notfound;
	begin

	nr_nota_w		:= ltrim(c00_w.nr_nota,'0');
	cd_usuario_conv_w	:= c00_w.cd_usuario_convenio;


	if	c00_w.guia_TR75 is not null then

		select	max(a.nr_interno_conta),
			max(c.cd_autorizacao)
		into	nr_interno_conta_w,
			cd_autorizacao_w
		from	conta_paciente a,
			atend_categoria_convenio b,
			conta_paciente_guia c
		where	a.nr_atendimento	= b.nr_atendimento
		and	a.nr_interno_conta	= c.nr_interno_conta
		and	c.cd_autorizacao	= somente_numero(c00_w.guia_TR75)
		and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
		and	trunc(dt_periodo_inicial,'dd') = c00_w.dt_inicio_conta
		and	trunc(dt_periodo_final,'dd') 	= c00_w.dt_fim_conta
		and	a.ie_status_acerto	= 2
		and	a.nr_seq_protocolo	is not null
		and	a.cd_convenio_parametro	= cd_convenio_w
		and	a.nr_conta_convenio	= nr_nota_w
		and	b.cd_usuario_convenio	= cd_usuario_conv_w;
	
	end if;

	vetor_conta_w(i).nr_interno_conta	:= nr_interno_conta_w;	
	vetor_conta_w(i).cd_usuario_convenio	:= cd_usuario_conv_w;
	vetor_conta_w(i).nr_nota		:= nr_nota_w;
	vetor_conta_w(i).nr_sequencia		:= c00_w.nr_sequencia;	
	vetor_conta_w(i).nr_folha		:= c00_w.nr_folha;
	vetor_conta_w(i).nr_guia_tr_75		:= cd_autorizacao_w;
	

	i	:= i + 1;	
	end;
end loop;
close C00;

i := vetor_conta_w.count;

for k in 1.. i loop
	begin

	if	(k = 0) then
		nr_seq_inicial_w	:= nr_min_seq_w;
	else
		nr_seq_inicial_w	:= vetor_conta_w(k).nr_sequencia;
	end if;	

	begin
		nr_seq_final_w	:= vetor_conta_w(k + 1).nr_sequencia;
	exception
	when others then
		nr_seq_final_w	:= nr_max_seq_w;
	end;	
	dbms_output.put_line('inicio:'||nr_seq_inicial_w||' fim:'|| nr_seq_final_w);
	open C01;
	loop
	fetch C01 into
		nr_linha_w,
		ie_tipo_w,
		nr_doc_convenio_arq_w,
		nr_nota_arq_w,
		cd_usuario_conv_arq_w,
		cd_item_w,
		vl_pago_arq_w,
		vl_glosado_arq_w,
		vl_cobrado_arq_w,
		nm_paciente_w,
		dt_item_w,
		ie_gerar_resumo_w,
		ds_conteudo_w,
		cd_brasindice_w;
	exit when C01%notfound;	

		nr_seq_item_conta_w := null;

		select	to_char(nvl(max(dt_mesano_referencia),sysdate),'yyyy')
		into	dt_ano_conta_w
		from	conta_paciente
		where	nr_interno_conta	= vetor_conta_w(k).nr_interno_conta;

		dt_execucao_w	:= null;

		if	(dt_item_w is not null) then
			begin
				dt_execucao_w	:= to_date(dt_ano_conta_w||dt_item_w,'yyyymmdd');
			exception
			when others then
				dt_execucao_w	:= null;
			end;			
		end if;	

		vl_total_pago_w	:= 0;
		vl_glosado_w	:= 0;
		vl_cobrado_w	:= 0;		

		if	(substr(ie_tipo_w,3,4) in ('76','86')) then	
			vl_pago_arq_w		:= somente_numero(substr(ds_conteudo_w,84,13));
			vl_glosado_arq_w	:= somente_numero(substr(ds_conteudo_w,97,9));
			vl_cobrado_arq_w	:= somente_numero(substr(ds_conteudo_w,59,11));	

			vl_total_pago_w		:= dividir(nvl(vl_pago_arq_w,0),100);
			vl_glosado_w		:= dividir(nvl(vl_glosado_arq_w,0),100);
			vl_cobrado_w		:= dividir(nvl(vl_cobrado_arq_w,0),100);
		else		
			vl_total_pago_w		:= dividir(nvl(vl_pago_arq_w,0),100);
			vl_glosado_w		:= dividir(nvl(vl_glosado_arq_w,0),100);
			vl_cobrado_w		:= dividir(nvl(vl_cobrado_arq_w,0),100);
		end if;	

		if	(cd_item_w = '701') then --Quando 701, existe brasindice

			select	max(cd_material),
				max(a.nr_sequencia)
			into	cd_item_w,
				nr_seq_item_conta_w
			from	material_atend_paciente a,
				conta_paciente b
			where	a.nr_interno_conta	= b.nr_interno_conta
			and	b.nr_interno_conta	= vetor_conta_w(k).nr_interno_conta
			and	a.cd_motivo_exc_conta	is null
			and 	exists(select	1
					from	material_brasindice x
					where	x.cd_medicamento||x.cd_laboratorio||x.cd_apresentacao = cd_brasindice_w
					and	x.nr_sequencia = a.NR_SEQ_MAT_BRAS);

			if	(nr_seq_item_conta_w is null) then
				select	max(cd_material),
					max(a.nr_sequencia)
				into	cd_item_w,
					nr_seq_item_conta_w
				from	material_atend_paciente a,
					conta_paciente b
				where	a.nr_interno_conta	= b.nr_interno_conta
				and	b.nr_interno_conta	= vetor_conta_w(k).nr_interno_conta
				and	a.cd_motivo_exc_conta	is null
				and 	exists(select	1
					from	material_brasindice x
					where	x.cd_medicamento||x.cd_laboratorio||x.cd_apresentacao = cd_brasindice_w
					and	x.nr_sequencia = a.NR_SEQ_MAT_BRAS);
			end if;			
		end if;

		if	(nr_seq_item_conta_w is null) then

			select	max(nr_sequencia)			
			into	nr_seq_item_conta_w			
			from	procedimento_paciente
			where	nr_interno_conta		= vetor_conta_w(k).nr_interno_conta
			and	cd_motivo_exc_conta		is null
			and	(cd_procedimento		= cd_item_w or
				cd_procedimento_convenio	= cd_item_w or
				cd_procedimento_tuss		= cd_item_w);

			if	(nr_seq_item_conta_w is null) then

				select	max(nr_sequencia)
				into	nr_seq_item_conta_w
				from	material_atend_paciente
				where	nr_interno_conta			= vetor_conta_w(k).nr_interno_conta
				and	cd_motivo_exc_conta			is null
				and	(cd_material				= cd_item_w or
					cd_material_convenio			= cd_item_w or
					somente_numero(cd_material_tiss)	= cd_item_w);
			end if;
		end if;

		
		select	convenio_retorno_movto_seq.nextval
		into	nr_seq_movto_w
		from	dual;	

		insert into convenio_retorno_movto
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_retorno,
			nr_doc_convenio,
			cd_usuario_convenio,
			cd_item,
			vl_total_pago,
			vl_glosa,
			ds_complemento,
			vl_pago,
			vl_cobrado,
			nr_conta,
			ie_gera_resumo,
			dt_execucao,
			nr_seq_item_conta)
		values	(nr_seq_movto_w,
			sysdate,
			'Tasy_Imp',
			nr_seq_retorno_p,
			vetor_conta_w(k).nr_guia_tr_75,
			vetor_conta_w(k).cd_usuario_convenio,
			cd_item_w,
			vl_total_pago_w,
			vl_glosado_w,
			'c01 TR '|| ie_tipo_w ||' '|| vetor_conta_w(k).nr_nota||' Brasindice: '||cd_brasindice_w,
			vl_total_pago_w,
			vl_cobrado_w,
			vetor_conta_w(k).nr_interno_conta,
			ie_gerar_resumo_w,
			dt_execucao_w,
			nr_seq_item_conta_w);
	
	end loop;
	close C01;

	end;
end loop;

open C02;
loop
fetch C02 into
	nr_linha_w,
	ie_tipo_w,
	nr_doc_convenio_arq_w,
	nr_nota_arq_w,
	cd_usuario_conv_arq_w,
	cd_item_w,
	vl_total_pago_w,
	vl_glosado_w,
	vl_cobrado_w,
	nm_paciente_w,
	dia_ini_w,
	valor_conta_arq_w,
	nr_sequencia_w;
exit when C02%notfound;

	if	(substr(ie_tipo_w,1,2) = '00') then
		valor_conta_w	:= (valor_conta_arq_w/100);
	end if;

	if	(substr(ie_tipo_w,1,2) = '00') then
		begin
		nr_nota_w		:= ltrim(nr_nota_arq_w,'0');

		end;
	else



		select	max(a.nr_seq_protocolo)
		into	nr_seq_protocolo_w
		from	protocolo_convenio a,
				conta_paciente b,
				atendimento_paciente c,
				atend_categoria_convenio d
		where	a.cd_convenio		= cd_convenio_w
		and		a.nr_seq_protocolo	= b.nr_seq_protocolo
		and		b.nr_atendimento	= c.nr_atendimento
		and		c.nr_atendimento	= d.nr_atendimento
		and		a.ie_status_protocolo	= 2
		and		d.cd_usuario_convenio	= cd_usuario_conv_arq_w	
		and		substr(c.dt_entrada,1,2) = dia_ini_w
		and		exists	(select	1
						from	conta_paciente x
						where	x.nr_seq_protocolo	= a.nr_seq_protocolo
						and	x.nr_conta_convenio	= nr_nota_w);

		if	(nr_seq_protocolo_w is not null) then
			begin

			select	count(1)
			into	qt_pcnc_w
			from	protocolo_conv_nota_conta
			where	nr_seq_protocolo = nr_seq_protocolo_w;

			if	(qt_pcnc_w = 0) then
				begin

				select	max(a.nr_interno_conta)
				into	nr_interno_conta_w
				from	conta_paciente a,
					atend_categoria_convenio b,
					procedimento_paciente f
				where	a.nr_atendimento	= b.nr_atendimento
				and	a.nr_seq_protocolo	= nr_seq_protocolo_w
				and	a.nr_conta_convenio	= nr_nota_w
				and	f.nr_interno_conta	= a.nr_interno_conta
				and	nvl(f.cd_procedimento_convenio,f.cd_procedimento) = cd_item_w
				and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
				and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w
				and	obter_saldo_conpaci(a.nr_interno_conta,null) <> 0
				and	(select	substr(x.dt_entrada,1,2) 
				from 	atendimento_paciente x
				where	x.nr_atendimento = a.nr_atendimento ) = dia_ini_w;

				end;
			else
				begin	

				select	max(a.nr_interno_conta)
				into	nr_interno_conta_w
				from	protocolo_conv_nota_conta a,
					conta_paciente b,
					atend_categoria_convenio c,
					procedimento_paciente f
				where	a.nr_interno_conta	= b.nr_interno_conta
				and	b.nr_atendimento	= c.nr_atendimento
				and	c.nr_seq_interno	= obter_atecaco_atend_conv(b.nr_atendimento,cd_convenio_w)
				and	f.nr_interno_conta	= a.nr_interno_conta
				and	nvl(f.cd_procedimento_convenio,f.cd_procedimento) = cd_item_w
				and	c.cd_usuario_convenio	= cd_usuario_conv_arq_w
				and	obter_saldo_conpaci(b.nr_interno_conta,null) <> 0
				and	a.nr_seq_protocolo = nr_seq_protocolo_w
				and	a.nr_nota_conta	= nr_nota_w
				and	(select	substr(x.dt_entrada,1,2) 
				from 	atendimento_paciente x
				where	x.nr_atendimento = b.nr_atendimento ) = dia_ini_w;

				end;
			end if;

			end;
		else
			begin

			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b,
				procedimento_paciente f
			where	a.nr_atendimento	= b.nr_atendimento
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	a.ie_status_acerto	= 2
			and	a.nr_seq_protocolo	is not null
			and	f.nr_interno_conta	= a.nr_interno_conta
			and	nvl(f.cd_procedimento_convenio,f.cd_procedimento) = cd_item_w
			and	a.cd_convenio_parametro	= cd_convenio_w
			and	a.nr_conta_convenio	= nr_nota_w
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w
			and	obter_saldo_conpaci(a.nr_interno_conta,null) <> 0
			and	(select	substr(x.dt_entrada,1,2) 
				from 	atendimento_paciente x
				where	x.nr_atendimento = a.nr_atendimento ) = dia_ini_w;

			end;

		end if;

		vl_total_pago_w	:= dividir(nvl(vl_total_pago_w,0),100);
		vl_glosado_w	:= dividir(nvl(vl_glosado_w,0),100);
		vl_cobrado_w	:= dividir(nvl(vl_cobrado_w,0),100);

		if	(nr_interno_conta_w is null) then

			select	max(c.nr_seq_protocolo)
			into	nr_seq_protocolo_ww
			from	atendimento_paciente a,
				atend_categoria_convenio b,
				conta_paciente c,
				procedimento_paciente f
			where	a.nr_atendimento 	= b.nr_atendimento
			and	a.nr_atendimento 	= c.nr_atendimento
			and	f.nr_interno_conta	= c.nr_interno_conta
			and	c.vl_conta 		= valor_conta_w
			and	b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
			and	c.cd_convenio_parametro = cd_convenio_w
			and	substr(a.dt_entrada,1,2) = dia_ini_w;

			if	(nvl(nr_seq_protocolo_ww,0) = 0) then				
				select	max(c.nr_seq_protocolo)
				into	nr_seq_protocolo_ww
				from	atendimento_paciente a,
					atend_categoria_convenio b,
					conta_paciente c,
					procedimento_paciente f
				where	a.nr_atendimento 	= b.nr_atendimento
				and	a.nr_atendimento 	= c.nr_atendimento
				and	f.nr_interno_conta	= c.nr_interno_conta
				and	c.nr_conta_convenio	= nr_nota_w 
				and	b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
				and	c.cd_convenio_parametro = cd_convenio_w
				and	substr(a.dt_entrada,1,2) = dia_ini_w;

				if	(nvl(nr_seq_protocolo_ww,0) = 0) then
					select	max(c.nr_seq_protocolo)
					into	nr_seq_protocolo_ww
					from	atendimento_paciente a,
							atend_categoria_convenio b,
							conta_paciente c,
							procedimento_paciente f
					where	a.nr_atendimento 	= b.nr_atendimento
					and		a.nr_atendimento 	= c.nr_atendimento
					and		f.nr_interno_conta	= c.nr_interno_conta
					and		b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
					and		c.cd_convenio_parametro = cd_convenio_w
					and		substr(a.dt_entrada,1,2) = dia_ini_w;
				end if;
			end if;

			select	max(c.nr_interno_conta)
			into	nr_interno_conta_w
			from	atendimento_paciente a,
				atend_categoria_convenio b,
				conta_paciente c,
				procedimento_paciente f
			where	a.nr_atendimento 	= b.nr_atendimento
			and	a.nr_atendimento 	= c.nr_atendimento
			and	f.nr_interno_conta	= c.nr_interno_conta
			and	c.nr_seq_protocolo 	= nr_seq_protocolo_ww
			and	nvl(f.cd_procedimento_convenio,f.cd_procedimento) = cd_item_w
			and	b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
			and	c.cd_convenio_parametro = cd_convenio_w
			and	substr(a.dt_entrada,1,2) = dia_ini_w;


			if	(nvl(nr_interno_conta_w,0) = 0) then -- identificar a  conta da guia de mat/med 
				select	max(c.nr_interno_conta)
				into	nr_interno_conta_w
				from	atendimento_paciente a,
						atend_categoria_convenio b,
						conta_paciente c,
						material_atend_paciente f
				where	a.nr_atendimento 	= b.nr_atendimento
				and	a.nr_atendimento 	= c.nr_atendimento
				and	f.nr_interno_conta	= c.nr_interno_conta
				and	(select	sum(t.vl_material)
					from	material_atend_paciente t
					where	t.nr_interno_conta = c.nr_interno_conta) = vl_total_pago_w
				and	c.nr_seq_protocolo 		= nr_seq_protocolo_ww
				and	b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
				and	c.cd_convenio_parametro = cd_convenio_w
				and	substr(a.dt_entrada,1,2) = dia_ini_w;

			end if;

			if	(nvl(nr_interno_conta_w,0) = 0) then -- identificar a  conta da guia de mat/med  em protocolos diferentes
				select	max(c.nr_interno_conta)
				into	nr_interno_conta_w
				from	atendimento_paciente a,
						atend_categoria_convenio b,
						conta_paciente c,
						material_atend_paciente f
				where	a.nr_atendimento 	= b.nr_atendimento
				and	a.nr_atendimento 	= c.nr_atendimento
				and	f.nr_interno_conta	= c.nr_interno_conta
				and	(select	sum(t.vl_material)
					from	material_atend_paciente t
					where	t.nr_interno_conta = c.nr_interno_conta) = vl_total_pago_w
				and	b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
				and	c.cd_convenio_parametro = cd_convenio_w
				and	substr(a.dt_entrada,1,2) = dia_ini_w;

			end if;

			if	(nvl(nr_interno_conta_w,0) = 0) then
				select	max(c.nr_interno_conta)
				into	nr_interno_conta_w
				from	atendimento_paciente a,
						atend_categoria_convenio b,
						conta_paciente c,
						procedimento_paciente f
				where	a.nr_atendimento 	= b.nr_atendimento
				and		a.nr_atendimento 	= c.nr_atendimento
				and		f.nr_interno_conta	= c.nr_interno_conta
				and		nvl(f.cd_procedimento_convenio,f.cd_procedimento) = cd_item_w
				and		b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
				and		c.cd_convenio_parametro = cd_convenio_w
				and		substr(a.dt_entrada,1,2) = dia_ini_w;
			end if;

			if	(nvl(nr_interno_conta_w,0) = 0) then -- proc
				select	max(c.nr_interno_conta)
				into	nr_interno_conta_w
				from	atendimento_paciente a,
						atend_categoria_convenio b,
						conta_paciente c,
						procedimento_paciente f
				where	a.nr_atendimento 	= b.nr_atendimento
				and		a.nr_atendimento 	= c.nr_atendimento
				and		f.nr_interno_conta	= c.nr_interno_conta
				and		b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
				and		c.cd_convenio_parametro = cd_convenio_w
				and		c.nr_conta_convenio	= nr_nota_w 
				and		substr(a.dt_entrada,1,2) = dia_ini_w;
			end if;
			if	(nvl(nr_interno_conta_w,0) = 0) then -- proc em nota diferente do arquivo
				select	max(c.nr_interno_conta)
				into	nr_interno_conta_w
				from	atendimento_paciente a,
						atend_categoria_convenio b,
						conta_paciente c,
						procedimento_paciente f
				where	a.nr_atendimento 	= b.nr_atendimento
				and		a.nr_atendimento 	= c.nr_atendimento
				and		f.nr_interno_conta	= c.nr_interno_conta
				and		b.cd_usuario_convenio 	= cd_usuario_conv_arq_w
				and		c.cd_convenio_parametro = cd_convenio_w
				and		substr(a.dt_entrada,1,2) = dia_ini_w;
			end if;
		end if;

		if	(substr(ie_tipo_w,3,2) = '76') then /*TR 76 nao tem valor cobrado*/
			vl_cobrado_w	:= null;
		end if;


		insert into convenio_retorno_movto
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_retorno,
			nr_doc_convenio,
			cd_usuario_convenio,
			cd_item,
			vl_total_pago,
			vl_glosa,
			ds_complemento,
			vl_pago,
			vl_cobrado,
			nr_conta,
			nr_atendimento)
		values	(convenio_retorno_movto_seq.nextval,
			sysdate,
			'Tasy_Imp',
			nr_seq_retorno_p,
			nr_doc_convenio_w,
			cd_usuario_conv_arq_w,
			cd_item_w,
			vl_total_pago_w,
			vl_glosado_w,
			'c02 TR '||ie_tipo_w||' '||nr_nota_w||' - '||nm_paciente_w,
			vl_total_pago_w,
			vl_cobrado_w,
			nr_interno_conta_w,
			decode(nvl(nr_interno_conta_w,0),0,null,(select x.nr_atendimento from conta_paciente x where x.nr_interno_conta = nr_interno_conta_w)));

	end if;

end loop;
close C02;

delete	from	w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end HCBCS_GERAR_W_CONV_RET_MOVTO;
/
