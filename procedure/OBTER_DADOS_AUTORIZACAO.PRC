create or replace
procedure Obter_dados_autorizacao(
				nr_atendimento_p		number,
				dt_inicio_vigencia_p		out date,
				dt_fim_vigencia_p		out date,
				cd_senha_p			out varchar2,
				cd_autorizacao_p		out varchar2,
				qt_dia_autorizado_p		out number,
				ie_tipo_guia_p			out varchar2) is 

begin

select	max(dt_inicio_vigencia),
	max(dt_fim_vigencia),
	max(cd_senha),
	max(cd_autorizacao),
	max(qt_dia_autorizado),
	max(ie_tipo_guia)
into	dt_inicio_vigencia_p,
	dt_fim_vigencia_p,
	cd_senha_p,
	cd_autorizacao_p,
	qt_dia_autorizado_p,
	ie_tipo_guia_p
from	autorizacao_convenio
where	nr_sequencia = (select max(nr_sequencia) from autorizacao_convenio where nr_atendimento = nr_atendimento_p);

commit;

end Obter_dados_autorizacao;
/