create or replace
procedure phi_vincular_os_etapa (
	nr_seq_proj_cron_etapa_p	number,
	nr_seq_ordem_p			number,
	nm_usuario_p			varchar2,
	nr_product_requirement_p	reg_product_requirement.nr_sequencia%type) is

nr_proj_cron_w		number(10,0);
nr_seq_cronograma_w	number(10,0);
qt_existe_os_w		number(10,0);
nr_seq_sev_wheb_w       number(10,0);
nr_seq_sev_w            number(10,0);
ds_atividade_w		varchar2(255 char);
ds_etapa_w		varchar2(4000 char);
ds_macro_w		varchar2(1000 char);
ds_relat_tecnico_w	varchar2(4000 char);
ie_main_os_proj_w	varchar2(1 char);
ie_possui_proj_vinc_w	varchar2(1 char);
nr_seq_ordem_serv_pai_w	man_ordem_servico.nr_seq_ordem_serv_pai%type;
nr_seq_proj_w		proj_projeto.nr_sequencia%type;
ie_fase_w		proj_cron_etapa.ie_fase%type;

begin

select	max(mos.nr_seq_ordem_serv_pai)
into	nr_seq_ordem_serv_pai_w
from	man_ordem_servico mos
where	mos.nr_sequencia = nr_seq_ordem_p;
	
if	(nr_seq_ordem_serv_pai_w is not null) then
	/* Nao e possivel vincular uma ordem de servico filha a um projeto. */
	wheb_mensagem_pck.exibir_mensagem_abort(1085000);
end if;

select	count(*)
into	qt_existe_os_w
from	man_ordem_servico
where	nr_sequencia = nr_seq_ordem_p;
	
if	(qt_existe_os_w = 0) then
	/* Nao existe uma ordem de servico com este numero, verifique */
	wheb_mensagem_pck.exibir_mensagem_abort(263160);
end if;
	
select	nvl(max('S'), 'N')
into	ie_main_os_proj_w
from	proj_projeto
where	nr_seq_ordem_serv = nr_seq_ordem_p;
	
if	(ie_main_os_proj_w = 'S') then
	/* Nao e permitido vincular a OS de origem do projeto a uma etapa. */
	wheb_mensagem_pck.exibir_mensagem_abort(1150201);
end if;
	
select	nvl(max('S'), 'N')
into	ie_fase_w
from	proj_cron_etapa ce
where	ce.nr_seq_superior = nr_seq_proj_cron_etapa_p;
	
if	(ie_fase_w = 'S') then
	/* Nao e permitido vincular ordem de servico em etapas agrupadoras. */
	wheb_mensagem_pck.exibir_mensagem_abort(1130199);
end if;
	
select	nr_seq_proj_cron_etapa
into	nr_proj_cron_w
from	man_ordem_servico
where	nr_sequencia	= nr_seq_ordem_p;
	  
if	(nvl(nr_proj_cron_w,0) > 0) then
	/* A ordem de servico ja possui uma etapa vinculada */
	wheb_mensagem_pck.exibir_mensagem_abort(263162);
end if;
	
select	a.nr_seq_cronograma,
	a.ds_atividade,
	a.ds_etapa,
	b.nr_seq_proj
into	nr_seq_cronograma_w,
	ds_atividade_w,
	ds_etapa_w,
	nr_seq_proj_w
from	proj_cron_etapa a,
	proj_cronograma b
where	a.nr_seq_cronograma = b.nr_sequencia
and	a.nr_sequencia = nr_seq_proj_cron_etapa_p;
	
verifica_permite_add_os_proj(nr_seq_proj_w, nr_seq_ordem_p);

select	decode(count(1),0,'N','S')
into	ie_possui_proj_vinc_w
from	proj_ordem_servico
where	nr_seq_ordem = nr_seq_ordem_p
and	nr_seq_proj = nr_seq_proj_w;
	
if	(ie_possui_proj_vinc_w = 'S') then
	/* Nao e possivel vincular uma OS ja vinculada ao projeto. */
	wheb_mensagem_pck.exibir_mensagem_abort(1141357);
end if;	

select	nvl(max(nr_seq_severidade_wheb), 0),
	nvl(max(nr_seq_severidade), 0)
into	nr_seq_sev_wheb_w,
	nr_seq_sev_w
from	man_ordem_servico
where	nr_sequencia = nr_seq_ordem_p;

if	(nr_seq_sev_wheb_w = 0) and
	(nr_seq_sev_w = 0) then
        /*Nao foi informado severidade Philips nem severidade cliente. Para continuar, informe ao menos um dos campos.*/
	wheb_mensagem_pck.exibir_mensagem_abort(1026256);
end if;

update	man_ordem_servico
set	nr_seq_proj_cron_etapa = nr_seq_proj_cron_etapa_p,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_sequencia = nr_seq_ordem_p;

insert into proj_ordem_servico 
	(nr_sequencia,
	nr_seq_proj,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_ordem,
	nr_seq_cron_etapa,
	ie_origem_ordem)
values(	proj_ordem_servico_seq.nextval,
	nr_seq_proj_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_ordem_p, 
	null,
	'P');
	
ds_macro_w := 'DS_ATIVIDADE=' || ds_atividade_w || ';'
	|| 'DS_ETAPA=' || ds_etapa_w || ';'
	|| 'NR_SEQ_CRONOGRAMA=' || nr_seq_cronograma_w;

insert into man_ordem_serv_tecnico(
	nr_sequencia,
	nr_seq_ordem_serv,
	dt_atualizacao,
	nm_usuario,
	ds_relat_tecnico,
	dt_historico,
	ie_origem,
	dt_liberacao,
	nm_usuario_lib)
values(	man_ordem_serv_tecnico_seq.nextval,
	nr_seq_ordem_p,
	sysdate,
	nm_usuario_p,
	obter_desc_exp_idioma(1042174, wheb_usuario_pck.get_nr_seq_idioma(), ds_macro_w),
	sysdate,
	'I',
	sysdate,
	nm_usuario_p);

update	man_ordem_serv_ativ
set	nr_seq_proj_cron_etapa = nr_seq_proj_cron_etapa_p
where	nr_seq_ordem_serv = nr_seq_ordem_p
and	nr_seq_proj_cron_etapa is null;
	
proj_do_impact_analysis(nr_seq_ordem_p, nr_product_requirement_p, nm_usuario_p);
man_ordem_atual_horas_cron(null,nr_seq_proj_cron_etapa_p,nm_usuario_p);

end phi_vincular_os_etapa;
/
