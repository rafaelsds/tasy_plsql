create or replace
procedure ptu_imp_req_contagem_benef_v60(	cd_transacao_p			ptu_pedido_contagem_benef.cd_transacao%type, 
						ie_tipo_cliente_p		ptu_pedido_contagem_benef.ie_tipo_cliente%type, 
						cd_unimed_requisitante_p	ptu_pedido_contagem_benef.cd_unimed_requisitante%type,
						cd_unimed_destino_p		ptu_pedido_contagem_benef.cd_unimed_destino%type,
						dt_ano_referencia_p		ptu_pedido_contagem_benef.dt_ano_referencia%type,
						dt_mes_referencia_p		ptu_pedido_contagem_benef.dt_mes_referencia%type, 
						nr_versao_p			ptu_pedido_contagem_benef.nr_versao%type,
						nm_usuario_p			usuario.nm_usuario%type,
						nr_seq_pedido_cont_p  out 	ptu_pedido_contagem_benef.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a importa��o do arquivo de  00430 - Requisi��o Contagem Benef.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_cliente_w	ptu_pedido_autorizacao.ie_tipo_cliente%type;

begin

ie_tipo_cliente_w := ptu_converter_tipo_cliente(ie_tipo_cliente_p);

insert	into ptu_pedido_contagem_benef
	(nr_sequencia, cd_transacao, cd_unimed_destino,
	cd_unimed_requisitante, ds_ano_referencia, ds_mes_referencia,
	ie_tipo_cliente, dt_atualizacao_nrec, dt_atualizacao,
	nm_usuario, nm_usuario_nrec, dt_ano_referencia,
	dt_mes_referencia, nr_versao)
values	(ptu_pedido_contagem_benef_seq.nextval, cd_transacao_p, cd_unimed_destino_p,
	cd_unimed_requisitante_p, dt_ano_referencia_p, dt_mes_referencia_p,
	ie_tipo_cliente_w, sysdate, sysdate,
	nm_usuario_p, nm_usuario_p, sysdate, 
	sysdate, nr_versao_p) returning nr_sequencia into nr_seq_pedido_cont_p;

commit;

end ptu_imp_req_contagem_benef_v60;
/