create or replace
procedure ctb_imp_criterio_rateio_item(	cd_empresa_p		number,
					cd_estabelecimento_p	number,
					nr_seq_criterio_p	number,
					cd_centro_custo_p	number,
					cd_conta_contabil_p	varchar2,
					cd_conta_financ_p	number,
					pr_rateio_p		number,
					dt_inicio_vigencia_p	date,
					dt_fim_vigencia_p	date,
					nm_usuario_p		Varchar2) is 


begin

insert into ctb_criterio_rateio_item(
	nr_sequencia,
	nr_seq_criterio,
	cd_centro_custo,
	cd_conta_contabil,
	cd_conta_financ,
	pr_rateio,
	dt_inicio_vigencia,
	dt_fim_vigencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec)
values(	ctb_criterio_rateio_item_seq.nextval,
	nr_seq_criterio_p,
	cd_centro_custo_p,
	cd_conta_contabil_p,
	cd_conta_financ_p,
	pr_rateio_p,
	dt_inicio_vigencia_p,
	dt_fim_vigencia_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p);

commit;

end ctb_imp_criterio_rateio_item;
/