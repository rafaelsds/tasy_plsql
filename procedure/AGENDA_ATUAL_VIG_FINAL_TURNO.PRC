create or replace
procedure Agenda_atual_vig_final_turno (	cd_agenda_p	number,
					dt_fim_vigencia_p	date,
					nm_usuario_p	varchar2,
					ie_tipo_agenda_p	varchar2,
					ie_domingo_p varchar2 default 'S',
					ie_segunda_p varchar2 default 'S',
					ie_terca_p varchar2 default 'S',
					ie_quarta_p varchar2 default 'S',
					ie_quinta_p varchar2 default 'S',
					ie_sexta_p varchar2 default 'S',
					ie_sabado_p varchar2 default 'S',
					ie_dias_trab_p varchar2 default 'S') is
          
ie_domingo_w   varchar2(2 CHAR);
ie_segunda_w   varchar2(2 CHAR);
ie_terca_w     varchar2(2 CHAR);
ie_quarta_w    varchar2(2 CHAR);
ie_quinta_w    varchar2(2 CHAR);
ie_sexta_w     varchar2(2 CHAR);
ie_sabado_w    varchar2(2 CHAR);
ie_dias_trab_w varchar2(2 CHAR);
          
begin

ie_domingo_w := nvl(ie_domingo_p, 'S');
ie_segunda_w := nvl(ie_segunda_p, 'S');
ie_terca_w := nvl(ie_terca_p, 'S');
ie_quarta_w := nvl(ie_quarta_p, 'S');
ie_quinta_w := nvl(ie_quinta_p, 'S');
ie_sexta_w := nvl(ie_sexta_p, 'S');
ie_sabado_w := nvl(ie_sabado_p, 'S');
ie_dias_trab_w := nvl(ie_dias_trab_p, 'S');

/*
ie_tipo_agenda_p
C - Consultas
E - Exames
S - Servicos
*/

if	(cd_agenda_p is not null) and
	(dt_fim_vigencia_p is not null) then
	
	if ((ie_tipo_agenda_p = 'C') OR
		(ie_tipo_agenda_p = 'S')) then
		
		UPDATE 	agenda_turno        
		SET    	dt_final_vigencia 	= dt_fim_vigencia_p,
			nm_usuario        	= nm_usuario_p,
			dt_atualizacao    	= sysdate
		WHERE  	cd_agenda         	= cd_agenda_p
		and ((ie_dia_semana = 1 and ie_domingo_w = 'S') or
			(ie_dia_semana = 2 and ie_segunda_w = 'S') or
			(ie_dia_semana = 3 and ie_terca_w = 'S') or
			(ie_dia_semana = 4 and ie_quarta_w = 'S') or
			(ie_dia_semana = 5 and ie_quinta_w = 'S') or
			(ie_dia_semana = 6 and ie_sexta_w = 'S') or
			(ie_dia_semana = 7 and ie_sabado_w = 'S') or
			(ie_dia_semana = 9 and ie_dias_trab_w = 'S'));
		
	else
	
		UPDATE 	agenda_horario        
		SET    	dt_final_vigencia 	= dt_fim_vigencia_p,
			nm_usuario        	= nm_usuario_p,
			dt_atualizacao    	= sysdate
		WHERE  	cd_agenda         	= cd_agenda_p;
		
	end if;
	
end if;

commit;

end Agenda_atual_vig_final_turno;
/
