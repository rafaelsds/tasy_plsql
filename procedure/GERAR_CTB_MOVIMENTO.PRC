create or replace
procedure Gerar_Ctb_Movimento(	nr_lote_contabil_p		Number,
				dt_movimento_p		date,
				cd_conta_debito_p		Varchar2,
				cd_conta_credito_p		Varchar2,
				cd_centro_custo_p		varchar2,
				cd_historico_p		number,
				vl_movimento_p		number,
				ds_compl_historico_p	varchar2,
				nm_usuario_p		varchar2,
				cd_interface_p		Number,
				nr_seq_agrupamento_p	number,
				cd_estabelecimento_p	number,
				nr_seq_movto_p in Out	Number) is

nr_seq_movto_w			number(10,0);
nr_seq_movto_centro_w		number(10,0);
nr_seq_mes_ref_w			Number(10,0);
nr_sequencia_w			Number(10,0);
nr_seq_log_w			Number(10,0);
ds_erro_w			Varchar2(2000);
qt_reg_w				Number(10,0);
cd_conta_debito_w			Varchar2(40);
cd_conta_credito_w		Varchar2(40);
ie_conta_estab_w			varchar2(1);
ie_regra_conta_w			Varchar2(15);
ie_regra_centro_w			Varchar2(15);
ie_regra_hist_padrao_w		Varchar2(15);
dt_nula_w			Date;
cd_centro_custo_w			Varchar2(20);
cd_historico_w			number(10,0);
cd_empresa_w			number(04,0);
cd_estabelecimento_w		number(10);
ie_gerar_centro_conta_w		varchar2(1);
cd_centro_custo_fixo_w		number(8);
cd_classif_debito_w		varchar2(40);
cd_classif_credito_w		varchar2(40);
qt_registros_w			number(10);

nr_seq_movto_ww			number(10) := 0;
ie_exige_centro_w		varchar2(1);
cd_conta_aux_w			varchar2(20);
cd_centro_aux_w			varchar2(20);
cd_estabelecimento_ww		number(10);

ie_estab_centro_custo_w		ctb_regra_exp_imp.ie_estab_centro_custo%type;

BEGIN

ie_gerar_centro_conta_w		:= nvl(obter_valor_param_usuario(923, 55, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w), 'S');

cd_estabelecimento_ww		:= nvl(cd_estabelecimento_p,0);


cd_conta_debito_w			:= replace(cd_conta_debito_p,' ','');
cd_conta_credito_w		:= replace(cd_conta_credito_p,' ','');
cd_centro_custo_w			:= cd_centro_custo_p;

if	(cd_conta_debito_w = '0' or cd_conta_debito_w = '00000') then
	cd_conta_debito_w := replace(cd_conta_debito_w,'0','');
end if;

if	(cd_conta_credito_w = '0' or cd_conta_credito_w = '00000') then
	cd_conta_credito_w := replace(cd_conta_credito_w,'0','');
end if;

if (cd_interface_p = 2368) then
	begin
	cd_conta_debito_w		:= to_number(cd_conta_debito_w);
	cd_conta_credito_w		:= to_number(cd_conta_credito_w);
	cd_centro_custo_w		:= to_number(cd_centro_custo_w);
	exception
		when others then
		cd_conta_debito_w		:= cd_conta_debito_w;
		cd_conta_credito_w		:= cd_conta_credito_w;
		cd_centro_custo_w		:= cd_centro_custo_w;	
	end;

end if;

cd_historico_w			:= cd_historico_p;
cd_classif_debito_w		:= '';
cd_classif_credito_w		:= '';

select	max(dt_nula)
into	dt_nula_w
from	parametro_geral_tasy;

select	count(*),
	max(b.ie_regra_conta),
	max(b.ie_regra_hist_padrao),
	max(obter_empresa_estab(a.cd_estabelecimento)),
	max(a.cd_estabelecimento),
	nvl(max(ie_conta_estab), 'N'),
	nvl(max(ie_estab_centro_custo),'S')
into	qt_reg_w,
	ie_regra_conta_w,
	ie_regra_hist_padrao_w,
	cd_empresa_w,
	cd_estabelecimento_w,
	ie_conta_estab_w,
	ie_estab_centro_custo_w
from	ctb_regra_exp_imp b,
	lote_contabil a
where	a.nr_lote_contabil		= nr_lote_contabil_p
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	b.cd_interface		= cd_interface_p;

select	nvl(cd_estabelecimento_w, max(cd_estabelecimento))
into	cd_estabelecimento_w
from	lote_contabil
where	nr_lote_contabil	= nr_lote_contabil_p;

if	(cd_empresa_w is null) then
	cd_empresa_w	:= obter_empresa_estab(cd_estabelecimento_w);
end if;
		
/*Rotina especifica OS 255662 Matheus 08-12-2010 */
if	(cd_interface_p = 1952) then
	
	if	(nvl(cd_conta_debito_w,'X') <> 'X') then
	
		cd_conta_aux_w	:= substr(cd_conta_debito_w,1,8);
		cd_centro_aux_w	:= substr(cd_conta_debito_w,9,9);
		
		select	nvl(max(ie_centro_custo),'N')
		into	ie_exige_centro_w
		from	conta_contabil
		where	cd_empresa		= cd_empresa_w
		and	cd_sistema_contabil	= cd_conta_aux_w;
		
		
		if	(ie_exige_centro_w = 'S') then
			cd_conta_debito_w	:= cd_conta_aux_w;
			cd_centro_custo_w	:= cd_centro_aux_w;
			
		end if;

	end if;
	
	if	(nvl(cd_conta_credito_w, 'X') <> 'X') then
		
		cd_conta_aux_w	:= substr(cd_conta_credito_w,1,8);
		cd_centro_aux_w	:= substr(cd_conta_credito_w,9,9);
		
		select	nvl(max(ie_centro_custo),'N')
		into	ie_exige_centro_w
		from	conta_contabil
		where	cd_sistema_contabil	= cd_conta_aux_w
		and	cd_empresa		= cd_empresa_w;
	
		if	(ie_exige_centro_w = 'S') then
			cd_conta_credito_w	:= cd_conta_aux_w;
			cd_centro_custo_w	:= cd_centro_aux_w;
			
		end if;
	end if;

end if;


if	(qt_reg_w = 1) then
	if	(ie_regra_conta_w = 'CLASSIF') then
		
		if	(ie_conta_estab_w = 'S') then
			
			select	min(cd_conta_contabil)
			into	cd_conta_debito_w
			from	conta_contabil
			where	cd_classificacao	= cd_conta_debito_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p), 1, 1) = 'S'
			and	(substr(obter_se_conta_contab_estab(cd_conta_contabil, cd_estabelecimento_w), 1, 1) = 'S');
			
			select	min(cd_conta_contabil)
			into	cd_conta_credito_w
			from	conta_contabil
			where	cd_classificacao	= cd_conta_credito_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S'
			and	(substr(obter_se_conta_contab_estab(cd_conta_contabil, cd_estabelecimento_w), 1, 1) = 'S');
			
		else	
			select	min(cd_conta_contabil)
			into	cd_conta_debito_w
			from	conta_contabil
			where	cd_classificacao	= cd_conta_debito_w
			and	cd_empresa		= cd_empresa_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S';
			
			select	min(cd_conta_contabil)
			into	cd_conta_credito_w
			from	conta_contabil
			where	cd_classificacao	= cd_conta_credito_w
			and	cd_empresa		= cd_empresa_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S';

		end if;
		
	elsif	(ie_regra_conta_w = 'CSP') then
	
		if	(ie_conta_estab_w	= 'S') then
			
			select	min(cd_conta_contabil)
			into	cd_conta_debito_w
			from	conta_contabil
			where	replace(cd_classificacao, '.', '')	= cd_conta_debito_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S'
			and	(substr(obter_se_conta_contab_estab(cd_conta_contabil, cd_estabelecimento_w), 1, 1) = 'S');
			
			select	min(cd_conta_contabil)
			into	cd_conta_credito_w
			from	conta_contabil
			where	replace(cd_classificacao, '.', '')	= cd_conta_credito_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S'
			and	(substr(obter_se_conta_contab_estab(cd_conta_contabil, cd_estabelecimento_w), 1, 1) = 'S');
		
		else
			
			select	min(cd_conta_contabil)
			into	cd_conta_debito_w
			from	conta_contabil
			where	replace(cd_classificacao,'.','')	= cd_conta_debito_w
			and	cd_empresa		= cd_empresa_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S';
			
			select	min(cd_conta_contabil)
			into	cd_conta_credito_w
			from	conta_contabil
			where	replace(cd_classificacao,'.','')	= cd_conta_credito_w
			and	cd_empresa		= cd_empresa_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S';
			
		end if;
		
	elsif	(ie_regra_conta_w = 'SISCONT') then
		
		
		if	(ie_conta_estab_w = 'S') then
						
			select	min(cd_conta_contabil)
			into	cd_conta_debito_w
			from	conta_contabil
			where	cd_sistema_contabil	= cd_conta_debito_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S'
			and	(substr(obter_se_conta_contab_estab(cd_conta_contabil, cd_estabelecimento_w), 1, 1) = 'S');
			
			select	min(cd_conta_contabil)
			into	cd_conta_credito_w
			from	conta_contabil
			where	cd_sistema_contabil	= cd_conta_credito_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S'
			and	(substr(obter_se_conta_contab_estab(cd_conta_contabil, cd_estabelecimento_w), 1, 1) = 'S');
		
		else
			
			select	min(cd_conta_contabil)
			into	cd_conta_debito_w
			from	conta_contabil
			where	cd_sistema_contabil	= cd_conta_debito_w
			and	cd_empresa		= cd_empresa_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S';
			
			select	min(cd_conta_contabil)
			into	cd_conta_credito_w
			from	conta_contabil
			where	cd_sistema_contabil	= cd_conta_credito_w
			and	cd_empresa		= cd_empresa_w
			and	substr(obter_se_conta_vigente(cd_conta_contabil, dt_movimento_p),1,1) = 'S';
			
		end if;
		
	end if;
	
	if	(ie_regra_hist_padrao_w = 'SISCONT') then
		
		select	min(cd_historico)
		into	cd_historico_w
		from	historico_padrao
		where	somente_numero(cd_sistema_contabil) = cd_historico_p
		and	cd_empresa = cd_empresa_w
		and	ie_situacao = 'A';
		
	end if;
	
end if;

if	(nr_seq_movto_p > 0) then
	nr_seq_movto_w	:= nr_seq_movto_p;
else
	select	ctb_movimento_seq.nextval
	into	nr_seq_movto_w
	from	dual;
end if;

if	(nr_seq_movto_p = 0) then
	begin
	select	nr_seq_mes_ref
	into	nr_seq_mes_ref_w
	from	lote_contabil
	where	nr_lote_contabil	= nr_lote_contabil_p;
	
	begin
	if	(nvl(cd_conta_debito_w, '0') <> '0') then
		cd_classif_debito_w	:= ctb_obter_classif_conta(cd_conta_debito_w, null, dt_movimento_p);
	end if;
	if	(nvl(cd_conta_credito_w, '0') <> '0') then
		cd_classif_credito_w	:= ctb_obter_classif_conta(cd_conta_credito_w, null, dt_movimento_p);
	end if;
	exception when others then
		cd_classif_debito_w	:= '';
		cd_classif_credito_w	:= '';
	end;
	
	begin
	
	if (cd_historico_w = 0) then
		cd_historico_w := 1;
	end if;
	
	select	count(*)
	into	qt_registros_w
	from	ctb_movimento
	where	nr_lote_contabil = nr_lote_contabil_p
	and		dt_movimento = dt_movimento_p
	and		nr_seq_mes_ref = nr_seq_mes_ref_w
	and		nvl(cd_conta_debito,'0') = nvl(cd_conta_debito_w,'0')
	and		nvl(cd_conta_credito,'0') = nvl(cd_conta_credito_w,'0')
	and		cd_historico = decode(cd_historico_w,'0','1',cd_historico_w)
	and		ds_compl_historico = replace(ds_compl_historico_p,'"','');
	
	if (qt_registros_w > 0) and (cd_interface_p = 2368) then
	
		select	max(nr_sequencia)
		into	nr_seq_movto_ww
		from	ctb_movimento
		where	nr_lote_contabil = nr_lote_contabil_p
		and	dt_movimento = dt_movimento_p
		and	nr_seq_mes_ref = nr_seq_mes_ref_w
		and	nvl(cd_conta_debito,'0') = nvl(cd_conta_debito_w,'0')
		and	nvl(cd_conta_credito,'0') = nvl(cd_conta_credito_w,'0')
		and	cd_historico = decode(cd_historico_w,'0','1',cd_historico_w)
		and	ds_compl_historico = replace(ds_compl_historico_p,'"','');
	
		update	ctb_movimento
		set	vl_movimento = vl_movimento + vl_movimento_p
		where	nr_lote_contabil = nr_lote_contabil_p
		and	dt_movimento = dt_movimento_p
		and	nr_seq_mes_ref = nr_seq_mes_ref_w
		and	nvl(cd_conta_debito,'0') = nvl(cd_conta_debito_w,'0')
		and	nvl(cd_conta_credito,'0') = nvl(cd_conta_credito_w,'0')
		and	cd_historico = decode(cd_historico_w,'0','1',cd_historico_w)
		and	ds_compl_historico = replace(ds_compl_historico_p,'"','');	
	
	else
	
		
	
		if(cd_estabelecimento_ww = 0)then	
		   insert into ctb_movimento(
			 nr_sequencia,
			 nr_lote_contabil,
			 nr_seq_mes_ref,
			  dt_movimento,
			 vl_movimento,
			 dt_atualizacao,
			 nm_usuario,
			 cd_historico,
			 cd_conta_debito,
			 cd_conta_credito,
			 ds_compl_historico,
			 nr_seq_agrupamento,
			 ie_revisado,
			 cd_classif_debito,
			 cd_classif_credito,
			 ie_status_origem)
		 values(	nr_seq_movto_w,
			 nr_lote_contabil_p,
			 nr_seq_mes_ref_w,
			 dt_movimento_p,
			 vl_movimento_p,
			 Sysdate,
			 nm_usuario_p,
			 cd_historico_w,
			 replace(decode(decode(cd_conta_debito_w, '', null, cd_conta_debito_w),'0', null, cd_conta_debito_w),' ',''),
			 replace(decode(decode(cd_conta_credito_w,'', null, cd_conta_credito_w), '0', null, cd_conta_credito_w),' ',''),
			  replace(ds_compl_historico_p,'"',''),
			 decode(nr_seq_agrupamento_p,0,null,nr_seq_agrupamento_p),
			 'N',
			 cd_classif_debito_w,
			 cd_classif_credito_w,
			 'I');
		 
		  
		  commit;
		
		else
		   insert into ctb_movimento(
			 nr_sequencia,
			 nr_lote_contabil,
			 nr_seq_mes_ref,
			  dt_movimento,
			 vl_movimento,
			 dt_atualizacao,
			 nm_usuario,
			 cd_historico,
			 cd_conta_debito,
			 cd_conta_credito,
			 ds_compl_historico,
			 nr_seq_agrupamento,
			 ie_revisado,
			 cd_classif_debito,
			 cd_classif_credito,
			 cd_estabelecimento,
			 ie_status_origem)
		 values(	nr_seq_movto_w,
			 nr_lote_contabil_p,
			 nr_seq_mes_ref_w,
			 dt_movimento_p,
			 vl_movimento_p,
			 Sysdate,
			 nm_usuario_p,
			 cd_historico_w,
			 replace(decode(decode(cd_conta_debito_w, '', null, cd_conta_debito_w),'0', null, cd_conta_debito_w),' ',''),
			 replace(decode(decode(cd_conta_credito_w,'', null, cd_conta_credito_w), '0', null, cd_conta_credito_w),' ',''),
			  replace(ds_compl_historico_p,'"',''),
			 decode(nr_seq_agrupamento_p,0,null,nr_seq_agrupamento_p),
			 'N',
			 cd_classif_debito_w,
			 cd_classif_credito_w,
			 cd_estabelecimento_ww,
			 'I');
		
		
			   commit;
		
		 end if;
		
		
		end if;
		
		exception
			when others then
				nr_seq_movto_p	:= 0;
				ds_erro_w	:= SQLERRM(sqlcode);
				select	ctb_log_import_seq.nextval
				into	nr_seq_log_w
				from	dual;
				insert into ctb_log_import(
					nr_sequencia,
					nr_lote_contabil,
					dt_atualizacao,
					nm_usuario,
					ds_log)
				values(	nr_seq_log_w,
					nr_lote_contabil_p,
					sysdate, 
					nm_usuario_p, 
					to_char(dt_movimento_p,'ddmmyyyy') || ',' ||
					cd_conta_debito_w || ',' ||
					cd_conta_credito_w || ',' ||
					vl_movimento_p || ',' ||
					cd_historico_p || ',' ||
					ds_compl_historico_p || chr(13) || chr(10) || ds_erro_w);
	end;
	if	(cd_centro_custo_w <> '0') then
		begin
		/*Anderson 17/04/2008 - OS80105 - importar o centro de custo pela classifica��o ou c�digo cont�bil */
		select	count(*),
			max(b.ie_regra_centro)
		into	qt_reg_w,
			ie_regra_centro_w
		from	ctb_regra_exp_imp b,
			lote_contabil a
		where	a.nr_lote_contabil	= nr_lote_contabil_p
		and	a.cd_estabelecimento	= b.cd_estabelecimento
		and	b.cd_interface		= cd_interface_p;

		if	(qt_reg_w = 1) then
			if	(ie_regra_centro_w = 'CLASSIF') then
				select	min(cd_centro_custo)
				into	cd_centro_custo_w
				from	centro_custo
				where	cd_classificacao	= cd_centro_custo_w
				and	((cd_estabelecimento	= cd_estabelecimento_w)
				or	(ie_estab_centro_custo_w = 'N'));
			elsif	(ie_regra_centro_w = 'CSP') then
				select	min(cd_centro_custo)
				into	cd_centro_custo_w
				from	centro_custo
				where	replace(cd_classificacao,'.','')	= cd_centro_custo_w
				and	((cd_estabelecimento	= cd_estabelecimento_w)
				or	(ie_estab_centro_custo_w = 'N'));
			elsif	(ie_regra_centro_w = 'SISCONT') then
				select	min(cd_centro_custo)
				into	cd_centro_custo_w
				from	centro_custo
				where	cd_sistema_contabil	= cd_centro_custo_w
				and	((cd_estabelecimento	= cd_estabelecimento_w)
				or	(ie_estab_centro_custo_w = 'N'));
			end if;
		end if;

		select	ctb_movto_centro_custo_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		if (cd_interface_p <> 2368) or (nr_seq_movto_ww = 0) then
			nr_seq_movto_ww := nr_seq_movto_w;
		end if;
		
		insert into ctb_movto_centro_custo(
			nr_sequencia,
			nr_seq_movimento,
			cd_centro_custo,
			dt_atualizacao,
			nm_usuario,
			vl_movimento,
			pr_rateio)
		values(	nr_sequencia_w,
			nr_seq_movto_ww,
			cd_centro_custo_w,
			sysdate,
			nm_usuario_p,
			vl_movimento_p,
			100);
		commit;
		exception
			when others then
				ds_erro_w		:= SQLERRM(sqlcode);
				select	ctb_log_import_seq.nextval
				into	nr_seq_log_w
				from	dual;
				insert into ctb_log_import(
					nr_sequencia,
					nr_lote_contabil,
					dt_atualizacao,
					nm_usuario,
					ds_log)
				values(	nr_seq_log_w,
					nr_lote_contabil_p,
					sysdate, 
					nm_usuario_p, 
					cd_centro_custo_w || ',' ||
					vl_movimento_p || ' : ' ||
					chr(13) || chr(10) || ds_erro_w);
		end;
	elsif	(ie_gerar_centro_conta_w = 'S') then
		ctb_atualiza_ccusto_movto(nr_seq_movto_w,cd_estabelecimento_w,nm_usuario_p);
	end if;
	end;
else 	if	(cd_centro_custo_w <> '0') then
		begin
		/*Anderson 17/04/2008 - OS80105 - importar o centro de custo pela classifica��o ou c�digo cont�bil */
		select	count(*),
			max(b.ie_regra_centro)
		into	qt_reg_w,
			ie_regra_centro_w
		from	ctb_regra_exp_imp b,
			lote_contabil a
		where	a.nr_lote_contabil		= nr_lote_contabil_p
		and	a.cd_estabelecimento	= b.cd_estabelecimento
		and	b.cd_interface		= cd_interface_p;

		if	(qt_reg_w = 1) then
			if	(ie_regra_centro_w = 'CLASSIF') then
				select	min(cd_centro_custo)
				into	cd_centro_custo_w
				from	centro_custo
				where	cd_classificacao	= cd_centro_custo_w
				and	((cd_estabelecimento	= cd_estabelecimento_w)
				or	(ie_estab_centro_custo_w = 'N'));
			elsif	(ie_regra_centro_w = 'CSP') then
				select	min(cd_centro_custo)
				into	cd_centro_custo_w
				from	centro_custo
				where	replace(cd_classificacao,'.','')	= cd_centro_custo_w
				and	((cd_estabelecimento	= cd_estabelecimento_w)
				or	(ie_estab_centro_custo_w = 'N'));
			elsif	(ie_regra_centro_w = 'SISCONT') then
				select	min(cd_centro_custo)
				into	cd_centro_custo_w
				from	centro_custo
				where	cd_sistema_contabil	= cd_centro_custo_w
				and	((cd_estabelecimento	= cd_estabelecimento_w)
				or	(ie_estab_centro_custo_w = 'N'));
			end if;
		end if;
		
		select	ctb_movto_centro_custo_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		if (cd_interface_p <> 2368) or (nr_seq_movto_ww = 0) then
			nr_seq_movto_ww := nr_seq_movto_p;
		end if;
		
		insert into ctb_movto_centro_custo(
			nr_sequencia,
			nr_seq_movimento,
			cd_centro_custo,
			dt_atualizacao,
			nm_usuario,
			vl_movimento,
			pr_rateio)
		values(	nr_sequencia_w,
			nr_seq_movto_ww,
			cd_centro_custo_w,
			sysdate,
			nm_usuario_p,
			vl_movimento_p,
			100);
		commit;
		exception
			when others then
				ds_erro_w		:= SQLERRM(sqlcode);
				select	ctb_log_import_seq.nextval
				into	nr_seq_log_w
				from	dual;
				insert into ctb_log_import(
					nr_sequencia,
					nr_lote_contabil,
					dt_atualizacao,
					nm_usuario,
					ds_log)
				values(	nr_seq_log_w,
					nr_lote_contabil_p,
					sysdate, 
					nm_usuario_p, 
					cd_centro_custo_w || ',' ||
					vl_movimento_p || ' : ' ||
					chr(13) || chr(10) || ds_erro_w);					
		end;
	end if;
end if;

select	max(b.ie_regra_centro)
into	ie_regra_centro_w
from	ctb_regra_exp_imp b,
	lote_contabil a
where	a.nr_lote_contabil	= nr_lote_contabil_p
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	b.cd_interface		= cd_interface_p;

if	(ie_regra_centro_w = 'CENCON') then

	select	max(a.cd_centro_custo)
	into	cd_centro_custo_fixo_w
	from	conta_contabil a
	where	a.cd_conta_contabil	= nvl(cd_conta_debito_w,cd_conta_credito_w);
	
	if	(nvl(cd_centro_custo_fixo_w,0) <> 0) then
		select	ctb_movto_centro_custo_seq.nextval
		into	nr_sequencia_w
		from	dual;
			
		insert into ctb_movto_centro_custo(
				nr_sequencia,
				nr_seq_movimento,
				cd_centro_custo,
				dt_atualizacao,
				nm_usuario,
				vl_movimento,
				pr_rateio)
			values(	nr_sequencia_w,
				nr_seq_movto_w,
				cd_centro_custo_fixo_w,
				sysdate,
				nm_usuario_p,
				vl_movimento_p,
				100);
		commit;
	end if;
end if;

ctb_atualizar_rateio_movto(nr_seq_movto_w,nm_usuario_p);

nr_seq_movto_p	:= nr_seq_movto_w;

end Gerar_Ctb_Movimento;
/