create or replace
procedure alterar_usuario_status_senha(
			nm_usuario_p	Varchar2,
			nm_usuario_alt_p	Varchar2,
			nr_seq_status_p	Number) is 
			
dt_fim_status_w 	date;
nr_sequencia_w	number(10);

begin

select	max(dt_inicio_status)
into	dt_fim_status_w
from	usuario_status_senha
where	nm_usuario = nm_usuario_alt_p;

if (nm_usuario_alt_p is not null) and (nr_seq_status_p is not null) then
	begin
	
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	usuario_status_senha
	where	nm_usuario = nm_usuario_alt_p;
	
	if (nr_sequencia_w is not null) then

		begin
	
		update 	usuario_status_senha
		set	dt_fim_status = sysdate
		where	nr_sequencia = nr_sequencia_w;
		
		end;

	end if;
	
	insert into usuario_status_senha(	nr_sequencia,
					nm_usuario_nrec,
					dt_inicio_status,
					dt_fim_status,
					nm_usuario,
					nr_seq_status)
				values(	usuario_status_senha_seq.nextval,
					nm_usuario_p,
					sysdate,
					null,
					nm_usuario_alt_p,
					nr_seq_status_p);
	end;
end if;	

commit;

end alterar_usuario_status_senha;
/