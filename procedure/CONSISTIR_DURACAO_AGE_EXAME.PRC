create or replace
procedure consistir_duracao_age_exame(	nr_minuto_duracao_p	number,
										nr_minuto_dur_agendamento_p number,	
										dt_agenda_p		date,
										cd_agenda_p		number,					  
										nm_usuario_p		Varchar2,
										cd_estabelecimento_p	number,
										nr_seq_horario_p number default null) is 

nr_minuto_duracao_w		number(10,0);
dt_inicial_w				date;
dt_final_w				date;
hr_final_turno_w		date;
hr_incio_intervalo_w	date;
hr_final_intervalo_w	date;
dia_semana_w		number(1,0);
			
begin
dt_inicial_w	:= dt_agenda_p;

if	(nvl(nr_minuto_duracao_p,0) = 0) or
	(nvl(nr_minuto_duracao_p,0) < nvl(nr_minuto_dur_agendamento_p,0))then
	dt_final_w	:= dt_agenda_p + nr_minuto_dur_agendamento_p/1440;
else	
	dt_final_w	:= dt_agenda_p + nr_minuto_duracao_p/1440;
end if;

select obter_cod_dia_semana(dt_inicial_w)
into dia_semana_w
from dual;

--Validar horario final do turno
if	(dt_final_w is not null) then		
	select	nvl(max(to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')),dt_final_w),
			nvl(max(to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_inicial_intervalo,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')),dt_final_w),
			nvl(max(to_date(to_char(dt_inicial_w,'dd/mm/yyyy') || ' ' || to_char(hr_final_intervalo,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')),dt_final_w)
	into	hr_final_turno_w,
			hr_incio_intervalo_w,
			hr_final_intervalo_w
	from	agenda_horario
	where	dt_inicial_w between nvl(dt_inicio_vigencia, dt_inicial_w) and nvl(dt_final_vigencia, dt_inicial_w)
	and		cd_agenda = cd_agenda_p
	and 	nr_sequencia = nr_seq_horario_p
	and		(((dt_dia_semana = 9) and (dia_semana_w not in (1,7)))
	or		((dt_dia_semana <> 9) and (dia_semana_w = dt_dia_semana))
	or		(dt_dia_semana is null));			
end if;

if	(dt_final_w > hr_final_turno_w)then
	--A soma do horario selecionado e a duracao do item supera o final do turno! Parametro [295]
	Wheb_mensagem_pck.exibir_mensagem_abort(316837);
elsif (dt_inicial_w < hr_incio_intervalo_w and
		dt_final_w > hr_incio_intervalo_w) then
		-----------------------------------cadastrar nova mensagem para os intervalos
	Wheb_mensagem_pck.exibir_mensagem_abort(316837);
end if;

select	sum(nvl(nr_minuto_duracao,0))
into	nr_minuto_duracao_w 
from	agenda_paciente
where	cd_agenda = cd_agenda_p
and	hr_inicio between dt_agenda_p and hr_final_turno_w
and	(nm_paciente is null or hr_inicio = dt_agenda_p)
and	ie_status_agenda not in ('C','B','F');

if	(nvl(nr_minuto_duracao_w, 0) <> 0) and
	(nvl(nr_minuto_duracao_w, 0) < nr_minuto_duracao_p) then
	--A soma do horario selecionado e a duracao do item supera o final do turno!
	Wheb_mensagem_pck.exibir_mensagem_abort(262563);
end if;

end consistir_duracao_age_exame;
/
