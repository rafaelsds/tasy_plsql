create or replace procedure pfcs_calculate_box_quantity( 	nr_seq_indicator_p	number,
											cd_estabelecimento_p	varchar2,
											nm_usuario_p	varchar2) is

	cursor c01 is
	select	x.nr_sequencia cd_group_pa,
			x.ds_grupo ds_group_pa
	from	pa_grupo_local x
	where	ie_situacao = 'A'
	and 	x.cd_estabelecimento = to_number(cd_estabelecimento_p)
	union 
	select 	0 cd_group_pa,
			obter_desc_expressao(344145,null) ds_group_pa
	from dual;

	cursor c02(nr_seq_grupo_p number) is
	select	y.nr_sequencia,
		y.ds_local ds_unit_pa,
		z.cd_pessoa_fisica id_patient,
		nvl(get_formatted_person_name(z.cd_pessoa_fisica, 'list'), obter_nome_pf(z.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(z.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(z.cd_pessoa_fisica, 'D') ds_gender,
		z.dt_nascimento dt_birthdate,
        obter_dados_pf(z.cd_pessoa_fisica, 'I') qt_idade_paciente,
		z.dt_entrada dt_entrance, 
		z.nr_atendimento nr_encounter
	from	pa_local y,
		pep_atendimento_ps_v z
	where	(((nr_seq_grupo_p = 0) and (y.nr_seq_grupo_pa is null)) or (y.nr_seq_grupo_pa = nr_seq_grupo_p))
	and   	y.nr_sequencia = z.nr_seq_local_pa (+)
	and 	y.ie_situacao = 'A'
	and 	y.cd_estabelecimento = to_number(cd_estabelecimento_p)
	and		get_if_encounter_still_pa(z.nr_atendimento) = 'S'
	and 	z.dt_alta is null;

	qt_total_w							number(15) := 0;
	qt_total_occupied_w					number(15) := 0;
	pfcs_panel_detail_seq_w				pfcs_panel_detail.nr_sequencia%type;
	nr_seq_operational_level_w			pfcs_operational_level.nr_sequencia%type;
	nr_seq_panel_w					pfcs_panel.nr_sequencia%type; 

	begin

		nr_seq_operational_level_w := pfcs_get_structure_level(
				cd_establishment_p => cd_estabelecimento_p,
				ie_level_p => 'O',
				ie_info_p => 'C');

		for c01_w in c01 loop

			begin

				for c02_w in c02(c01_w.cd_group_pa) loop

					begin

						if (c02_w.id_patient is not null) then

							qt_total_occupied_w := qt_total_occupied_w + 1;

						end if;

						select	pfcs_panel_detail_seq.nextval
						into	pfcs_panel_detail_seq_w
						from	dual;

						insert into pfcs_panel_detail(
							nr_sequencia,
							nm_usuario,
							dt_atualizacao,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							ie_situation,
							nr_seq_indicator,
							nr_seq_operational_level)
						values (
							pfcs_panel_detail_seq_w,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							'T',
							nr_seq_indicator_p,
							nr_seq_operational_level_w);

						insert into pfcs_detail_patient(
							nr_sequencia,
							nm_usuario,
							dt_atualizacao,
							nm_usuario_nrec,
							dt_atualizacao_nrec,
							nr_seq_detail,
							nr_encounter,
							dt_entrance,
							id_patient,
							nm_patient,
							ds_gender,
							ds_classification,
							dt_birthdate,
                            ds_age_range,
							cd_group_pa,
							ds_group_pa,
							ds_unit_pa)
						values (
							pfcs_detail_patient_seq.nextval,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							sysdate,
							pfcs_panel_detail_seq_w,
							c02_w.nr_encounter,
							c02_w.dt_entrance,
							c02_w.id_patient,
							c02_w.nm_patient,
							c02_w.ds_gender,
							c02_w.ds_classification,
							c02_w.dt_birthdate,
                            c02_w.qt_idade_paciente,
							c01_w.cd_group_pa,
							c01_w.ds_group_pa,
							c02_w.ds_unit_pa);

					end;

				end loop;

				commit;

                if(c01_w.cd_group_pa = 0)then
                    select count(*)
                    into qt_total_w
                    from pa_local y
                    where y.ie_situacao = 'A'
                    and y.nr_seq_grupo_pa is null
                    and	y.cd_estabelecimento = to_number(cd_estabelecimento_p);
                else
                    select count(*)
                    into qt_total_w
                    from pa_local y
                    where y.ie_situacao = 'A'
                    and y.nr_seq_grupo_pa = c01_w.cd_group_pa
                    and	y.cd_estabelecimento = to_number(cd_estabelecimento_p);
                end if;

				pfcs_pck.pfcs_generate_results(
					vl_indicator_p => qt_total_occupied_w,
					vl_indicator_aux_p => qt_total_w,
					ds_reference_value_p => c01_w.ds_group_pa,
					cd_reference_value_p => c01_w.cd_group_pa,
					nr_seq_indicator_p => nr_seq_indicator_p,
					nr_seq_operational_level_p => nr_seq_operational_level_w,
					nm_usuario_p => nm_usuario_p,
					nr_seq_panel_p => nr_seq_panel_w);


				pfcs_pck.pfcs_update_detail(
						nr_seq_indicator_p => nr_seq_indicator_p,
						nr_seq_panel_p => nr_seq_panel_w,
						nr_seq_operational_level_p => nr_seq_operational_level_w,
						nm_usuario_p => nm_usuario_p);

				qt_total_occupied_w := 0;
				qt_total_w := 0;

			end;

		end loop;

		pfcs_pck.pfcs_activate_records(
						nr_seq_indicator_p => nr_seq_indicator_p,
						nr_seq_operational_level_p => nr_seq_operational_level_w,
						nm_usuario_p => nm_usuario_p);

end pfcs_calculate_box_quantity;
/