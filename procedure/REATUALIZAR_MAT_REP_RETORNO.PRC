create or replace
procedure REATUALIZAR_MAT_REP_RETORNO(		nr_seq_material_p	number,
						nr_seq_retorno_p		number,
						nm_usuario_p		varchar2) is

ie_status_retorno_w	varchar2(10);
nr_interno_conta_w	number(15);
cont_w			number(15);
cont_orig_w		number(15);
cont_mat_w		number(15);
nr_seq_retorno_adic_w	number(15);
nr_sequencia_w		number(15);
nr_repasse_terceiro_w	number(15);
nr_lote_contabil_w		number(15);
cd_estabelecimento_w	number(15);
cd_medico_resp_w		varchar2(10);
ie_tipo_atendimento_w	number(15);
cd_convenio_w		number(15);
cd_categoria_w		number(15);
dt_entrada_w		date;
ie_tipo_convenio_w	number(15);
cd_edicao_amb_w		number(15);
ie_status_w		varchar2(255);
nr_seq_matrep_w		number(15);
nr_seq_particip_w		number(15);
nr_repasse_origem_w	number(15);
nr_seq_terc_origem_w	number(15);
nr_seq_mat_origem_w	number(15);
nr_lote_contabil_origem_w	number(15);
ie_status_origem_w	varchar2(255);
ie_cancelamento_origem_w	varchar2(255);
ie_repasse_estorno_w	varchar2(255);
nr_interno_conta_rep_w	number(15);
nr_seq_repasse_w		number(15);
ie_data_liberacao_w	varchar2(255);
ie_estornar_rep_desdob_w	varchar2(255);
nr_sequencia_orig_w	number(15);
nr_repasse_terceiro_orig_w	number(15);
nr_lote_contabil_orig_w	number(15);
ie_status_orig_w		varchar2(255);
vl_repasse_new_w		number(15,2);
vl_liberado_new_w		number(15,2);
nr_seq_mat_rep_old_w	number(15);
vl_repasse_old_w		number(15,2);
vl_liberado_old_w		number(15,2);
nr_seq_origem_w		number(15);
ie_permite_recalcular_w		varchar2(255);
cd_estabelecimento_ativo_w		number(10);
IE_LIB_REPASSE_RET_w		varchar2(1);

cursor c01 is
select	a.nr_sequencia,
	a.nr_repasse_terceiro,
	a.nr_lote_contabil,
	a.nr_seq_origem
from	material_repasse a
where	a.nr_seq_material	= nr_seq_material_p
union
select	a.nr_sequencia,
	a.nr_repasse_terceiro,
	a.nr_lote_contabil,
	a.nr_Seq_origem
from	material_repasse a,
	convenio_retorno_glosa b,
	convenio_retorno_item c
where	a.nr_seq_material		= b.nr_seq_matpaci
and	b.nr_seq_ret_item		= c.nr_sequencia
and	c.nr_seq_retorno		= nr_seq_retorno_p
order by	nr_seq_origem desc;

cursor c02 is
select	a.nr_sequencia,
	a.nr_repasse_terceiro,
	a.nr_lote_contabil
from	material_repasse a
where	nr_seq_origem		= nr_sequencia_w;

CURSOR	C05 IS
select	a.nr_sequencia,
	a.nr_repasse_terceiro,
	a.nr_seq_terceiro,
	a.nr_seq_material,
	a.nr_lote_contabil,
	a.ie_status,
	c.ie_cancelamento,
	b.nr_interno_conta
from	conta_paciente c,
	material_atend_paciente b,
	material_repasse a
where	a.nr_seq_material			= nr_seq_material_p
and	a.nr_seq_material			= b.nr_sequencia
and	b.nr_interno_conta			= c.nr_interno_conta
and	((nvl(ie_estornar_rep_desdob_w,'N')	= 'N' and a.nr_seq_origem is null) or
	(nvl(ie_estornar_rep_desdob_w,'N')	= 'S' and nvl(a.ie_estorno,'N') = 'N'));

begin
cd_estabelecimento_ativo_w := obter_estabelecimento_ativo;	
obter_param_usuario(27, 214, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_ativo_w, ie_permite_recalcular_w);
select	max(a.nr_sequencia),
	sum(a.vl_repasse),
	sum(a.vl_liberado)
into	nr_seq_mat_rep_old_w,
	vl_repasse_old_w,
	vl_liberado_old_w
from	material_repasse a,
	material_atend_paciente b
where	a.nr_seq_material	= b.nr_sequencia
and	b.nr_sequencia		= nr_seq_material_p;

select	nvl(max(IE_LIB_REPASSE_RET),'A')
into	IE_LIB_REPASSE_RET_w
from	parametro_repasse
where	cd_estabelecimento	= cd_estabelecimento_ativo_w;

select	max(a.ie_status_retorno)
into	ie_status_retorno_w
from	convenio_retorno a
where	a.nr_sequencia	= nr_seq_retorno_p;


if	(ie_status_retorno_w <> 'F') and
	(nvl(IE_LIB_REPASSE_RET_w,'A') = 'A') then
	/* O retorno nr_seq_retorno_p n�o est� fechado! */
	wheb_mensagem_pck.exibir_mensagem_abort(191627,'NR_SEQ_RETORNO_P=' || nr_seq_retorno_p);
end if;

if	(nr_seq_material_p is null) then
	/* N�mero de sequencia do material n�o foi encontrado! */
	wheb_mensagem_pck.exibir_mensagem_abort(195127);
end if;

select	max(a.nr_interno_conta)
into	nr_interno_conta_w
from	material_atend_paciente a
where	a.nr_sequencia	= nr_seq_material_p;

select	count(*)
into	cont_w
from	convenio_retorno_item a
where	a.nr_interno_conta	= nr_interno_conta_w;

if	(cont_w > 1) and 
	(nvl(ie_permite_recalcular_w,'N') = 'N')then

	select	max(a.nr_seq_retorno)
	into	nr_seq_retorno_adic_w
	from	convenio_retorno_item a
	where	a.nr_interno_conta	= nr_interno_conta_w
	and	a.nr_seq_retorno	<> nr_seq_retorno_p;

	/* A conta nr_interno_conta_w est� em mais de um retorno! Retorno: nr_seq_retorno_adic_w */
	wheb_mensagem_pck.exibir_mensagem_abort(191630,	'NR_INTERNO_CONTA_W=' || nr_interno_conta_w ||
							';NR_SEQ_RETORNO_ADIC_W=' || nr_seq_retorno_adic_w);

end if;

delete	from material_repasse a
where	exists(	select	1
		from	material_atend_paciente b
		where	b.nr_sequencia	= a.nr_seq_material
		and	b.nr_sequencia = nr_seq_material_p);

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	nr_repasse_terceiro_w,
	nr_lote_contabil_w,
	nr_seq_origem_w;
exit when c01%notfound;

	update	material_repasse
	set	ie_estorno = 'N'
	where	nr_sequencia	= nr_sequencia_w;

	ie_status_w     := null;

	if      (nr_repasse_terceiro_w is not null) then

		select	max(ie_status)
		into	ie_status_w
		from	repasse_terceiro
		where	nr_repasse_terceiro	= nr_repasse_terceiro_w;

	end if;

	if      (nr_repasse_terceiro_w is null) or (nvl(ie_status_w,'F') = 'A') then

		if      (nvl(nr_lote_contabil_w,0) = 0) then

			open c02;
			loop
			fetch c02 into
				nr_sequencia_orig_w,
				nr_repasse_terceiro_orig_w,
				nr_lote_contabil_orig_w;
			exit when c02%notfound;

				if      (nr_repasse_terceiro_orig_w is not null) then

					select	max(ie_status)
					into	ie_status_orig_w
					from	repasse_terceiro
					where	nr_repasse_terceiro	= nr_repasse_terceiro_orig_w;

				end if;

				if      (nr_repasse_terceiro_orig_w is null) or (nvl(ie_status_orig_w,'F') = 'A') then

					if      (nvl(nr_lote_contabil_orig_w,0) = 0) then

						select	count(*)
						into	cont_orig_w
						from	material_repasse a
						where	a.nr_sequencia	= nr_sequencia_orig_w
						and	exists	(select	1
								from	material_repasse b
								where	b.nr_seq_origem	= a.nr_sequencia);

						/*if	(cont_orig_w = 0) then

							delete	from material_repasse
							where	nr_seq_origem	= nr_sequencia_orig_w;

						end if;*/

					end if;

				end if;

			end loop;
			close c02;

			select	count(*)
			into	cont_mat_w
			from	material_repasse a
			where	a.nr_sequencia	= nr_sequencia_w
			and	exists	(select	1
					from	material_repasse b
					where	b.nr_seq_origem	= a.nr_sequencia);

			/*if	(cont_mat_w = 0) then
				
				delete  from material_repasse
				where   nr_sequencia    = nr_sequencia_w;

			end if;*/

		end if;

	end if;

end loop;
close c01;

select	max(a.cd_estabelecimento),
	max(b.cd_medico_resp),
	max(ie_tipo_atendimento),
	max(cd_convenio_parametro),
	max(cd_categoria_parametro),
	max(dt_entrada),
	max(OBTER_TIPO_CONVENIO(cd_convenio_parametro))
into	cd_estabelecimento_w,
	cd_medico_resp_w,
	ie_tipo_atendimento_w,
	cd_convenio_w,
	cd_categoria_w,
	dt_entrada_w,
	ie_tipo_convenio_w
from	parametro_faturamento c,
	atendimento_paciente b,
	conta_paciente a
where	a.nr_atendimento		= b.nr_atendimento
and	a.cd_estabelecimento		= c.cd_estabelecimento
and	a.nr_interno_conta		= nr_interno_conta_w;

ie_data_liberacao_w		:= obter_valor_param_usuario(87, 89, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);

select	nvl(max(ie_repasse_estorno),'N')
into	ie_repasse_estorno_w
from 	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

select 	nvl(max(ie_estornar_rep_desdob),'N')
into	ie_estornar_rep_desdob_w
from	parametro_repasse
where	cd_estabelecimento	= cd_estabelecimento_w;

OPEN C05;
LOOP
FETCH C05 into
	nr_seq_matrep_w,
	nr_repasse_origem_w,
	nr_seq_terc_origem_w,
	nr_seq_mat_origem_w,
	nr_lote_contabil_origem_w,
	ie_status_origem_w,
	ie_cancelamento_origem_w,
	nr_interno_conta_rep_w;
exit when c05%notfound;

	select	material_repasse_seq.nextval
	into	nr_seq_repasse_w
	from	dual;

	insert into material_repasse(
		nr_sequencia,
		nr_seq_material,
		vl_repasse,
		dt_atualizacao,
		nm_usuario,
		nr_seq_terceiro,
		nr_lote_contabil,
		nr_repasse_terceiro,
		cd_conta_contabil,
		nr_seq_trans_fin,
		vl_liberado,
		nr_seq_item_retorno,
		ie_status,
		nr_seq_origem,
		cd_regra,
		DT_CONTABIL_TITULO,
		DT_CONTABIL,
		cd_medico,
		nr_seq_criterio,
		NR_SEQ_TRANS_FIN_REP_MAIOR,
		ie_estorno,
		ie_repasse_calc,
		dt_liberacao)
	select	nr_seq_repasse_w,
		nr_seq_material,
		vl_repasse  * -1,
		sysdate,
		nm_usuario_p,
		nr_seq_terceiro,
		0,
		null,
		cd_conta_contabil,
		nr_seq_trans_fin,
		vl_liberado * -1,
		nr_seq_item_retorno,
		'E',
		nr_seq_matrep_w,
		cd_regra,
		to_date('01/01/2999','dd/mm/yyyy'),
		to_date('01/01/2999','dd/mm/yyyy'),
		cd_medico,
		nr_seq_criterio,
		NR_SEQ_TRANS_FIN_REP_MAIOR,
		'S',
		ie_repasse_calc,
		decode(ie_data_liberacao_w, 'S', sysdate, null)
	from	material_repasse
	where	nr_sequencia		= nr_seq_matrep_w;

	commit;

	if	(nr_interno_conta_rep_w is not null) then
		gerar_procmat_repasse_nf(nr_interno_conta_rep_w, nm_usuario_p, 'S');
	end if;

	if	(nvl(nr_repasse_origem_w, 0) > 0) and
		(ie_repasse_estorno_w = 'S') then

		obter_repasse_terceiro(	sysdate,
					nr_seq_terc_origem_w,
					nm_usuario_p,
					nr_seq_mat_origem_w,
					'P',
					nr_repasse_terceiro_w,
					null,
					null);

		if	(nvl(nr_repasse_terceiro_w,0) > 0) then

			update	material_repasse
			set	dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				nr_repasse_terceiro	= nr_repasse_terceiro_w,
				ie_estorno		= 'S'
			where	nr_sequencia		= nr_seq_repasse_w;

		end if;

	end if;

	update	material_repasse
	set	dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		ie_estorno	= 'S'
	where	nr_sequencia	= nr_seq_matrep_w;

	if	(nvl(nr_lote_contabil_origem_w,0) > 0) and
		(ie_status_origem_w = 'A') and
		(ie_cancelamento_origem_w = 'C') then

		update	material_repasse
		set	dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			ie_status		= 'U'
		where	nr_sequencia	= nr_seq_matrep_w;

	end if;

end loop;
close c05;

begin
select	obter_edicao (cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, dt_entrada_w, null)
into	cd_edicao_amb_w
from	dual;
exception
	when others then
		select	max(cd_edicao_amb)
		into	cd_edicao_amb_w
		from	convenio_amb
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	cd_convenio		= cd_convenio_w
		and	cd_categoria		= cd_categoria_w
		and	nvl(ie_situacao,'A')		= 'A'
		and	dt_inicio_vigencia		=	(
							select	max(dt_inicio_vigencia)
							from	convenio_amb a
							where	a.cd_estabelecimento	= cd_estabelecimento_w
							and	a.cd_convenio         	= cd_convenio_w
							and	a.cd_categoria        	= cd_categoria_w
							and	nvl(a.ie_situacao,'A')	= 'A'
							and	a.dt_inicio_vigencia	<=  dt_entrada_w
							);
end;

gravar_log_tasy(55889, WHEB_MENSAGEM_PCK.get_texto(298679, 'NR_SEQ_MATERIAL_P=' || nr_seq_material_p || ';NR_SEQ_RETORNO_P=' || nr_seq_retorno_p) || to_char(sysdate, 'dd/mm/yyyy hh24:mi:ss'), nm_usuario_p);

gerar_material_repasse(	nr_seq_material_p, 
				cd_estabelecimento_w,
				cd_medico_resp_w,
				nm_usuario_p,
				cd_edicao_amb_w,
				cd_convenio_w, 
				ie_tipo_atendimento_w,
				null,
				null);

if	(nvl(IE_LIB_REPASSE_RET_w,'A') = 'N') then
	fechar_retorno_repasse(	nr_seq_retorno_p,
				null,
				nr_seq_material_p,
				nm_usuario_p);
end if;				

select	sum(a.vl_repasse),
	sum(a.vl_liberado)
into	vl_repasse_new_w,
	vl_liberado_new_w
from	material_repasse a,
	material_atend_paciente b
where	a.nr_seq_material	= b.nr_sequencia
and	b.nr_sequencia		= nr_seq_material_p
and	a.nr_sequencia		> nr_seq_mat_rep_old_w;

--if	(vl_liberado_new_w - vl_liberado_old_w = 0) and
--	(vl_repasse_new_w - vl_repasse_old_w = 0) then

--	delete	from procedimento_repasse
--	where	nr_seq_procedimento	= nr_seq_procedimento_p
--	and	nr_sequencia		> nr_seq_proc_rep_old_w;

--end if;

commit;

end	REATUALIZAR_MAT_REP_RETORNO;
/