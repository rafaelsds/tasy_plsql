create or replace
procedure atualiza_data_prev_intern_cir (nr_seq_agenda_p number) is 

dt_prev_intern_w	date;

begin
if	(nvl(nr_seq_agenda_p,0) > 0) then
	select 	max(dt_prevista_internacao)
	into 	dt_prev_intern_w
	from 	paciente_espera 
	where	nr_seq_agenda 	= nr_seq_agenda_p;

	update	gestao_vaga
	set	dt_prevista 	= dt_prev_intern_w
	where	nr_seq_agenda 	= nr_seq_agenda_p;

	update	agenda_paciente
	set	dt_chegada_prev 	= dt_prev_intern_w
	where	nr_sequencia 		= nr_seq_agenda_p;

	commit;
end if;	
exception
	when others then
	dt_prev_intern_w := null;

end atualiza_data_prev_intern_cir;
/