create or replace
procedure accept_by_pass (	nr_sequencia_p		number) is

begin

	begin
	
		update_by_pass_status(nr_sequencia_p, 'A');
		justify_by_pass(nr_sequencia_p, 'Accepted');
	
	end;

commit;

end accept_by_pass;
/
