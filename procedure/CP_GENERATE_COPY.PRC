CREATE OR REPLACE PROCEDURE CP_GENERATE_COPY(
							nr_atendimento_p number, 
							nr_seq_prescr_p number, 
							nm_usuario_p varchar2) IS 

nr_sequencia_prescr_w         pe_prescricao.nr_sequencia%type;
dt_liberacao_plano_w          pe_prescricao.dt_liberacao_plano%type;

nr_sequencia_p_c_plan_w    	  patient_care_plan.nr_sequencia%type;
nr_sequencia_p_problem_w      patient_cp_problem.nr_sequencia%type;
nr_sequencia_p_cp_goal_w    	patient_cp_problem.nr_sequencia%type;
nr_seq_p_cp_goal_problem_w  	patient_cp_problem.nr_sequencia%type;
nr_seq_p_cp_indicator_w      	patient_cp_indicator.nr_sequencia%type;
nr_seq_p_cp_interv_plan_w    	patient_cp_interv_plan.nr_sequencia%type;
nr_seq_p_cp_intervention_w    patient_cp_intervention.nr_sequencia%type;

nr_sequencia_p_c_plan_new_w    	  patient_care_plan.nr_sequencia%type;
nr_sequencia_p_problem_new_w      patient_cp_problem.nr_sequencia%type;
nr_sequencia_p_cp_goal_new_w    	patient_cp_problem.nr_sequencia%type;
nr_seq_p_cp_goal_problem_new_w  	patient_cp_problem.nr_sequencia%type;
nr_seq_p_cp_indicator_new_w      	patient_cp_indicator.nr_sequencia%type;
nr_seq_p_cp_interv_plan_new_w    	patient_cp_interv_plan.nr_sequencia%type;
nr_seq_p_cp_intervention_new_w    	patient_cp_intervention.nr_sequencia%type;


qt_record_w		number(10);

--PATIENT_CARE_PLAN
Cursor c_pat_care_plan is
select 	*
from 	patient_care_plan
where 	nr_seq_prescr = nr_sequencia_prescr_w
and 	si_selected = 'Y'
and		dt_end is null;
		
--PATIENT_CP_PROBLEM			
Cursor c_pat_cp_problem is
select 	*
from 	patient_cp_problem
where 	nr_seq_pat_care_plan = nr_sequencia_p_c_plan_w
and 	si_selected = 'Y'
and		dt_end is null;

--PATIENT_CP_GOAL
Cursor c_pat_cp_goal is
select 	*
from 	patient_cp_goal
where 	nr_seq_pat_care_plan = nr_sequencia_p_c_plan_w
and 	si_selected = 'Y'
and		nr_seq_pat_cp_problem is null
and		dt_end is null;

--PATIENT_CP_GOAL (for problems)	
Cursor c_pat_cp_goal_problem is
select 	*
from 	patient_cp_goal
where 	nr_seq_pat_cp_problem = nr_sequencia_p_problem_w
and 	si_selected = 'Y'
and		dt_end is null;

--INDICATOR
Cursor c_pat_cp_indicator is
select 	*
from 	patient_cp_indicator
where 	nr_seq_pat_cp_goal = nr_seq_p_cp_goal_problem_w
and 	si_selected = 'Y'
and		dt_end is null;


--PATIENT_CP_INTERV_PLAN
Cursor c_pat_cp_interv_plan is
select 	*
from 	patient_cp_interv_plan
where 	nr_seq_pat_cp_problem = nr_sequencia_p_problem_w
and 	si_selected = 'Y';

--PATIENT_CP_INTERVENTION
Cursor c_pat_cp_intervention is
select 	*
from 	patient_cp_intervention
where 	nr_seq_pat_cp_interv_plan = nr_seq_p_cp_interv_plan_w
and 	si_selected = 'Y';

--------------------------------------------------------------
	
BEGIN

select 	nvl(dt_liberacao_plano, dt_liberacao)
into   	dt_liberacao_plano_w
from   	pe_prescricao
where	nr_sequencia = nr_seq_prescr_p;

select	count(1)
into	qt_record_w
from	patient_care_plan a
where	a.nr_seq_prescr = nr_seq_prescr_p
and		a.si_selected = 'Y';

if (dt_liberacao_plano_w is null) and
	(qt_record_w = 0) then
	select 	max(a.NR_SEQUENCIA)
	into	nr_sequencia_prescr_w 
	from 	pe_prescricao a
	where 	a.DT_LIBERACAO is not null
	and 	a.DT_SUSPENSAO is null
	and		a.nr_atendimento = nr_atendimento_p
	and		a.ie_situacao = 'A';

	-- PATIENT_CARE_PLAN
	
	delete from patient_care_plan
	where	nr_seq_prescr = nr_seq_prescr_p
	and		dt_start is not null
	and		dt_end is null;
	
	for r_c_pat_care_plan in c_pat_care_plan loop
		nr_sequencia_p_c_plan_w	:= r_c_pat_care_plan.nr_sequencia;
	
		select 	patient_care_plan_seq.nextval
		into 	nr_sequencia_p_c_plan_new_w
		from 	dual;
		
		insert into patient_care_plan (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_care_plan,
			nr_atendimento,
			dt_start,
			dt_end,
			dt_release,
			cd_pessoa_fisica,
			nr_seq_prescr,
			si_selected,
			ie_situacao
			)
		values	(
			nr_sequencia_p_c_plan_new_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			r_c_pat_care_plan.nr_seq_care_plan,
			nr_atendimento_p,
			r_c_pat_care_plan.dt_start,
			r_c_pat_care_plan.dt_end,
			r_c_pat_care_plan.dt_release,
			r_c_pat_care_plan.cd_pessoa_fisica,
			nr_seq_prescr_p,
			r_c_pat_care_plan.si_selected,
			r_c_pat_care_plan.ie_situacao
			);
			
		--PATIENT_CP_PROBLEM	
		for r_c_pat_cp_problem in c_pat_cp_problem loop
			nr_sequencia_p_problem_w	:= r_c_pat_cp_problem.nr_sequencia;
		
			select 	patient_cp_problem_seq.nextval
			into 	nr_sequencia_p_problem_new_w
			from 	dual;	
			
			insert into patient_cp_problem (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_pat_care_plan,
				nr_seq_cp_problem,
				dt_start,
				dt_end,
				dt_release,
				si_selected
				)
			values (
				nr_sequencia_p_problem_new_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_sequencia_p_c_plan_new_w,
				r_c_pat_cp_problem.nr_seq_cp_problem,
				r_c_pat_cp_problem.dt_start,
				r_c_pat_cp_problem.dt_end,
				r_c_pat_cp_problem.dt_release,
				r_c_pat_cp_problem.si_selected				
				);
				
			--PATIENT_CP_GOAL
			
			for r_c_pat_cp_goal_problem in c_pat_cp_goal_problem loop
				nr_seq_p_cp_goal_problem_w	:= r_c_pat_cp_goal_problem.nr_sequencia;
			
				select 	patient_cp_goal_seq.nextval
				into 	nr_seq_p_cp_goal_problem_new_w
				from 	dual;		
				
				insert into patient_cp_goal (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_pat_care_plan,
					nr_seq_cp_goal,
					dt_start,
					dt_end,
					dt_release,
					si_selected,
					nr_seq_pat_cp_problem
				)
				values (
					nr_seq_p_cp_goal_problem_new_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_sequencia_p_c_plan_new_w,
					r_c_pat_cp_goal_problem.nr_seq_cp_goal,
					r_c_pat_cp_goal_problem.dt_start,
					r_c_pat_cp_goal_problem.dt_end,
					r_c_pat_cp_goal_problem.dt_release,
					r_c_pat_cp_goal_problem.si_selected,
					nr_sequencia_p_problem_new_w
				);
				
				--PATIENT_CP_INDICATOR
				
				for r_c_pat_cp_indicator in c_pat_cp_indicator loop	
					nr_seq_p_cp_indicator_w	:= r_c_pat_cp_indicator.nr_sequencia;
				
					select 	patient_cp_indicator_seq.nextval
					into 	nr_seq_p_cp_indicator_new_w
					from 	dual;	
				
					insert into patient_cp_indicator (
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_cp_indicator,
						nr_seq_pat_care_plan,
						dt_start,
						dt_end,
						dt_release,
						si_selected,
						nr_seq_pat_cp_goal
					)
					values (
						nr_seq_p_cp_indicator_new_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						r_c_pat_cp_indicator.nr_seq_cp_indicator,
						nr_sequencia_p_c_plan_new_w,
						r_c_pat_cp_indicator.dt_start,
						r_c_pat_cp_indicator.dt_end,
						r_c_pat_cp_indicator.dt_release,
						r_c_pat_cp_indicator.si_selected,
						nr_seq_p_cp_goal_problem_new_w
					);		
				end loop;				
			end loop;	
			
			--PATIENT_CP_INTERV_PLAN
			
			for r_c_pat_cp_interv_plan in c_pat_cp_interv_plan loop	
				nr_seq_p_cp_interv_plan_w	:= r_c_pat_cp_interv_plan.nr_sequencia;
			
				select 	patient_cp_interv_plan_seq.nextval
				into 	nr_seq_p_cp_interv_plan_new_w
				from 	dual;
				
				insert into patient_cp_interv_plan (
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_pat_care_plan,
					nr_seq_interv_plan,
					dt_start,
					dt_end,
					dt_release,
					si_selected,
					nr_seq_pat_cp_problem
					)
				values (
					nr_seq_p_cp_interv_plan_new_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_sequencia_p_c_plan_new_w,
					r_c_pat_cp_interv_plan.nr_seq_interv_plan,
					r_c_pat_cp_interv_plan.dt_start,
					r_c_pat_cp_interv_plan.dt_end,
					r_c_pat_cp_interv_plan.dt_release,
					r_c_pat_cp_interv_plan.si_selected,
					nr_sequencia_p_problem_new_w					
				);
				
				--PATIENT_CP_INTERVENTION
				
				for r_c_pat_cp_intervention in c_pat_cp_intervention loop
					nr_seq_p_cp_intervention_w	:= r_c_pat_cp_intervention.nr_sequencia;
				
					select 	patient_cp_intervention_seq.nextval
					into 	nr_seq_p_cp_intervention_new_w
					from 	dual;
					
					insert into patient_cp_intervention (
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_cp_intervention,
						nr_seq_pat_care_plan,
						dt_start,
						dt_end,
						dt_release,
						si_selected,
						nr_seq_pat_cp_interv_plan
					) values (
						nr_seq_p_cp_intervention_new_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						r_c_pat_cp_intervention.nr_seq_cp_intervention,
						nr_sequencia_p_c_plan_new_w,
						r_c_pat_cp_intervention.dt_start,
						r_c_pat_cp_intervention.dt_end,
						r_c_pat_cp_intervention.dt_release,
						r_c_pat_cp_intervention.si_selected,
						nr_seq_p_cp_interv_plan_new_w
					);				
				end loop;				
			end loop;			
		end loop;		

		--PATIENT_CP_GOAL
		
		for r_c_pat_cp_goal in c_pat_cp_goal loop
			nr_sequencia_p_cp_goal_w	:= r_c_pat_cp_goal.nr_sequencia;
		
			select 	patient_cp_goal_seq.nextval
			into 	nr_sequencia_p_cp_goal_new_w
			from 	dual;	
			
			insert into patient_cp_goal (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_pat_care_plan,
				nr_seq_cp_goal,
				dt_start,
				dt_end,
				dt_release,
				si_selected,
				nr_seq_pat_cp_problem
				)
			values (
				nr_sequencia_p_cp_goal_new_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_sequencia_p_c_plan_new_w,
				r_c_pat_cp_goal.nr_seq_cp_goal,
				r_c_pat_cp_goal.dt_start,
				r_c_pat_cp_goal.dt_end,
				r_c_pat_cp_goal.dt_release,
				r_c_pat_cp_goal.si_selected,
				null
				);
		end loop;				
	end loop;		
end if;	

END CP_GENERATE_COPY;
/
