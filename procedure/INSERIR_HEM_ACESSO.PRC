Create or Replace
PROCEDURE Inserir_hem_acesso(nr_seq_proc_p          number,
                             nm_usuario_p           varchar,
                             cd_estabelecimento_p	Varchar2) IS

ie_acesso_w			    hem_acesso.ie_acesso%type;
ie_tipo_acesso_w        hem_acesso.ie_tipo_acesso%type;
ie_vaso_acesso_w        hem_acesso.ie_vaso_acesso%type;
ie_lat_acesso_w         hem_acesso.ie_lat_acesso%type;
nr_introdutor_w         hem_acesso.nr_introdutor%type;
nr_seq_cirurgia_w       hem_acesso.nr_seq_cirurgia%type;
nr_seq_oclusor_w        hem_acesso.nr_seq_oclusor%type;
qt_oclusor_w            hem_acesso.qt_oclusor%type;

BEGIN

if	(nvl(nr_seq_proc_p,0) > 0) then
	SELECT Obter_Regra_Atributo(cd_estabelecimento_p,
                                obter_perfil_ativo, nm_usuario_p, 
                                'HEM_ACESSO',
                                'IE_ACESSO',
                                'V',
                                0, 
                                0, 
                                0, 
                                0, 
                                NULL, 
                                NULL) into ie_acesso_w
    FROM dual;

    SELECT Obter_Regra_Atributo(cd_estabelecimento_p,
                                obter_perfil_ativo, nm_usuario_p, 
                                'HEM_ACESSO',
                                'IE_TIPO_ACESSO',
                                'V',
                                0, 
                                0, 
                                0, 
                                0, 
                                NULL, 
                                NULL) into ie_tipo_acesso_w
    FROM dual;

    SELECT  Obter_Regra_Atributo(cd_estabelecimento_p,
                                 obter_perfil_ativo,
                                 nm_usuario_p,
                                 'HEM_ACESSO',
                                 'IE_VASO_ACESSO',
                                 'V',
                                 0,
                                 0,
                                 0,
                                 0,
                                 NULL, 
                                 NULL) into ie_vaso_acesso_w
    FROM dual;

    SELECT  Obter_Regra_Atributo(cd_estabelecimento_p,
                                 obter_perfil_ativo,
                                 nm_usuario_p,
                                 'HEM_ACESSO',
                                 'IE_LAT_ACESSO',
                                 'V',
                                 0,
                                 0,
                                 0,
                                 0,
                                 NULL, 
                                 NULL) into ie_lat_acesso_w
    FROM dual;

    SELECT  Obter_Regra_Atributo(cd_estabelecimento_p,
                                 obter_perfil_ativo,
                                 nm_usuario_p,
                                 'HEM_ACESSO',
                                 'NR_INTRODUTOR',
                                 'V',
                                 0,
                                 0,
                                 0,
                                 0,
                                 NULL, 
                                 NULL) into nr_introdutor_w
    FROM dual;

    SELECT  Obter_Regra_Atributo(cd_estabelecimento_p,
                                 obter_perfil_ativo,
                                 nm_usuario_p,
                                 'HEM_ACESSO',
                                 'NR_SEQ_OCLUSOR',
                                 'V',
                                 0,
                                 0,
                                 0,
                                 0,
                                 NULL, 
                                 NULL) into nr_seq_oclusor_w
    FROM dual;

    SELECT  Obter_Regra_Atributo(cd_estabelecimento_p,
                                 obter_perfil_ativo,
                                 nm_usuario_p,
                                 'HEM_ACESSO',
                                 'QT_OCLUSOR',
                                 'V',
                                 0,
                                 0,
                                 0,
                                 0,
                                 NULL, 
                                 NULL) into qt_oclusor_w
    FROM dual;

    SELECT  nr_seq_cirurgia into nr_seq_cirurgia_w
    FROM    hem_proc 
    where   nr_procedimento = nr_seq_proc_p;

    if (ie_vaso_acesso_w  is not null and ie_acesso_w  is not null and nr_introdutor_w is not null) then
	    insert into hem_acesso (
                    nr_sequencia,
                    dt_atualizacao,
                    nm_usuario ,
                    dt_atualizacao_nrec,
                    nm_usuario_nrec,
                    nr_seq_proc,
                    ie_vaso_acesso,
                    ie_acesso,
                    ie_lat_acesso,
                    ie_tipo_acesso,
                    nr_introdutor,
                    nr_seq_cirurgia,
			        nr_seq_oclusor,
			        qt_oclusor
		           ) values 
                     (   hem_acesso_seq.nextval,
                         sysdate,
                         nm_usuario_p,
                         sysdate,
                         nm_usuario_p,
                         nr_seq_proc_p,
                         ie_vaso_acesso_w,
                         ie_acesso_w,
                         ie_lat_acesso_w,
                         ie_tipo_acesso_w,
                         nr_introdutor_w,
                         nr_seq_cirurgia_w,
                         nr_seq_oclusor_w,
                         qt_oclusor_w
                    );	
    end if;
end if;
commit;
END Inserir_hem_acesso;
/
