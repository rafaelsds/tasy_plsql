create or replace
procedure liberar_laudo_paciente(	nr_sequencia_p			number,
					nr_atendimento_p		number,
					nr_atendimento_dbp_p		number,
					nr_seq_proc_p			number,
					nr_prescricao_p			number,
					ie_tipo_atend_p			varchar2,
					dt_liberacao_p			date,		
					dir_padrao_p			varchar,
					cd_setor_atend_usu_p		number,
					nr_seq_prescr_proc_p		number,
					nm_usuario_p			varchar2,
					cd_perfil_p			number,
					cd_estabelecimento_p 		number,
					ds_arquivo_p 			out varchar2,
					ds_arquivo_dir_p		out varchar2,
					ds_arquivo_audio_p		out varchar2) is

ie_aprov_laudo_w		varchar2(1) := 'N';
cd_setor_atendimento_w		number(5);
liberar_alta_w			varchar2(1) := 'N';
alta_laudo_pendente_w		varchar2(1) := 'N';
qt_laudo_pendentes_w		number(5);
dt_liberacao_w			date;
alterar_medico_conta_w		varchar2(1) := 'N';
alterar_medico_exec_conta_w	varchar2(1) := 'N';
anestesista_conta_w		varchar2(1) := 'N';
ds_caminho_pdf_w		varchar2(1) := '';
ds_arquivo_w			varchar2(200) := '';
ds_arquivo_dir_w		varchar2(200) := '';
ds_arquivo_audio_w		varchar2(200) := '';

begin
	
ie_aprov_laudo_w := obter_se_aprov_laudo(nr_sequencia_p,nm_usuario_p);

if	(ie_aprov_laudo_w = 'N') then
	begin
	wheb_mensagem_pck.Exibir_Mensagem_Abort(41077,null);
	end;
end if;

if	(dt_liberacao_p is null) then
	begin
	liberar_laudo(nr_sequencia_p,cd_perfil_p,nr_atendimento_p,cd_estabelecimento_p,nm_usuario_p) ;

	if	(nr_prescricao_p is not null) then
		begin
		select	cd_setor_entrega
		into 	cd_setor_atendimento_w		
		from	prescr_medica
		where  	nr_prescricao = nr_prescricao_p;
		end;
	end if;
	
	obter_param_usuario(28, 52, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, liberar_alta_w);
	obter_param_usuario(28, 75, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, alta_laudo_pendente_w);
	qt_laudo_pendentes_w := obter_qt_laudos_pendente(nr_atendimento_p, nr_sequencia_p, nr_seq_proc_p);
	
	if	(liberar_alta_w = 'S') and
		(ie_tipo_atend_p = '7') and
		(alta_laudo_pendente_w = 'S') and
		(qt_laudo_pendentes_w = '0') then
		begin
		if	(cd_setor_atendimento_w = '') then
			begin
			saida_setor_servico(nr_atendimento_dbp_p,cd_setor_atend_usu_p,'','',nm_usuario_p);
			end;
		else			
			begin
			saida_setor_servico(nr_atendimento_dbp_p,cd_setor_atendimento_w,'','',nm_usuario_p);
			end;
		end if;
		end;
	end if;
	
	end;
end if;

select	max(a.dt_liberacao)
into	dt_liberacao_w
from	laudo_paciente a
where	a.nr_atendimento = nr_atendimento_p
and	a.nr_sequencia = nr_sequencia_p;

if	(dt_liberacao_w is not null) then
	begin
	obter_param_usuario(28, 101, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, alterar_medico_conta_w);
	obter_param_usuario(28, 112, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, alterar_medico_exec_conta_w);
	obter_param_usuario(28, 148, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, anestesista_conta_w);
	if	((alterar_medico_conta_w = 'S') or (alterar_medico_conta_w = 'R')) then
		begin
		atualizar_propaci_medico_laudo(nr_sequencia_p,'EX',nm_usuario_p);
		end;
	end if;
	
	if	(alterar_medico_exec_conta_w = 'S') then
		begin
		atualizar_propaci_medico_laudo(nr_sequencia_p,'EXC',nm_usuario_p);
		end;
	end if;
	
	if	(anestesista_conta_w = 'S') then
		begin
		atualizar_propaci_medico_laudo(nr_sequencia_p,'AN',nm_usuario_p);
		end;
	end if;

	end;
end if;

obter_param_usuario(28, 141, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ds_caminho_pdf_w);

if	(ds_caminho_pdf_w <> '') then
	begin 
	gerar_nome_arquivo_laudo(nr_seq_proc_p,nr_prescricao_p,dir_padrao_p,nm_usuario_p,cd_estabelecimento_p,ds_arquivo_w,ds_arquivo_dir_w);
	end;
end if;

select	max(ds_arquivo)
into	ds_arquivo_audio_w
from 	prescr_proc_ditado 
where 	nr_seq_prescr_proc = nr_seq_prescr_proc_p;

ds_arquivo_p 		:= ds_arquivo_w;
ds_arquivo_dir_p 	:= ds_arquivo_dir_w;
ds_arquivo_audio_p 	:= ds_arquivo_audio_w;

commit;	

end liberar_laudo_paciente;
/
