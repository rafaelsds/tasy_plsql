create or replace
procedure gerar_demissao_html(	cd_pessoa_fisica_p		varchar2,
				nm_usuario_p			varchar2,
				ie_acao_p			number,
				dt_referencia_p			date,
				cd_estabelecimento_p		number,
				cd_cargo_p			number,
				cd_cargo_ant_p			number,
				cd_setor_ant_p			number,
				ds_observacao_p			varchar2,
				ie_gerar_cargo_perfil_p		varchar2,
				ie_consiste_usuario_p		varchar2,
				ie_apagar_inf_demitir_p		varchar2,
				ie_regra_p			varchar2,
				ds_registro_novo_p		varchar2,
				ds_valor_atual_p		varchar2 default null,
				ds_valor_ant_p			varchar2 default null) 	is 

Cursor C01 is
	select	nm_usuario,
		cd_setor_atendimento
	from	usuario
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	union
	select	null,
		null
	from	pessoa_fisica a
	where	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	not exists (	select	1
				from	usuario b
				where	b.cd_pessoa_fisica = a.cd_pessoa_fisica);

c01_w	c01%rowtype;

begin

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	
	
	enviar_comunic_regra_func_usu
			(	ie_regra_p,
				ds_registro_novo_p,
				nm_usuario_p,
				cd_pessoa_fisica_p,
				ds_valor_atual_p,
				ds_valor_ant_p);

	Gravar_movto_funcionario(	cd_pessoa_fisica_p,
					nm_usuario_p,
					ie_acao_p,
					dt_referencia_p,
					cd_estabelecimento_p,
					0,
					cd_cargo_ant_p,
					0,
					c01_w.cd_setor_atendimento,
					ds_observacao_p,
					c01_w.nm_usuario,
					ie_gerar_cargo_perfil_p,
					ie_consiste_usuario_p,
					ie_apagar_inf_demitir_p);
end loop;
close C01;

end gerar_demissao_html;
/
