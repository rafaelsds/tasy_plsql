create or replace
procedure duplicar_escala_braden_q (	nr_sequencia_p	number,
				nm_usuario_p	varchar2,
				cd_pessoa_fisica_p	varchar2,
				nr_atendimento_p	number) is

nr_sequencia_w	number(10,0);

begin
if (nr_sequencia_p is not null) then

	select	atend_escala_braden_q_seq.NEXTVAL
	into	nr_sequencia_w
	from	dual;	

	insert into atend_escala_braden_q(			nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_avaliacao,
								qt_ponto,
								cd_pessoa_fisica,
								nr_atendimento,
								ie_perfusao,
								ie_percepcao_sensorial,
								ie_umidade,
								ie_atividade_fisica,
								ie_mobilidade,
								ie_nutricao,
								ie_friccao_cisalhamento,
								ie_situacao,
								cd_setor_atendimento)
						select	nr_sequencia_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								qt_ponto,
								cd_pessoa_fisica_p,
								nr_atendimento_p,
								ie_perfusao,
								ie_percepcao_sensorial,
								ie_umidade,
								ie_atividade_fisica,
								ie_mobilidade,
								ie_nutricao,
								ie_friccao_cisalhamento,
								'A',
								cd_setor_atendimento
						from	atend_escala_braden_q
						where	nr_sequencia = nr_sequencia_p;

	commit;
end if;

end duplicar_escala_braden_q;
/