create or replace procedure sus_insere_protocolo
			(	nr_interno_conta_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				nr_seq_protocolo_p		number,
				ie_tipo_protocolo_p		varchar2,
				ie_opcao_fpo_p		varchar2,
				ds_erro_fpo_p		out varchar2) is


cd_convenio_w			number(5);
ie_tipo_protocolo_aih_w		number(2);
ie_tipo_protocolo_bpa_w		number(2);
ie_tipo_protocolo_apac_w		number(2);  
nr_seq_protocolo_w		number(10);
nr_protocolo_w			varchar2(40);
dt_mesano_referencia_w		date;
dt_mesano_ref_conta_w		date;
ie_tipo_protocolo_w			number(2); 
ie_data_entrada_maior_w		varchar(15) := 'S'; 
ie_bloq_FPO_BPA_w		varchar(15) := 'N'; 
ie_bloq_FPO_AIH_w		varchar(15) := 'N'; 
ie_bloq_FPO_APAC_w		varchar(15) := 'N'; 
dt_entrada_paciente_w		date;
dt_referencia_protocolo_w		date;
cd_estabelecimento_w		number(5) := null;
ds_erro_w			varchar2(255) := 'S';
ie_tipo_atendimento_w		number(3);
ie_consiste_comp_prot_w		varchar2(15) := 'N';
ie_cons_data_Proc_bpa_w		varchar2(15) := 'S';
qt_proc_fora_comp_w		number(10) := 0;

begin

obter_param_usuario(1123,144,obter_perfil_Ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_bloq_FPO_AIH_w);
obter_param_usuario(1124,75,obter_perfil_Ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_bloq_FPO_APAC_w);
obter_param_usuario(1125,48,obter_perfil_Ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_bloq_FPO_BPA_w);
obter_param_usuario(1125,69,obter_perfil_Ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_cons_data_Proc_bpa_w);
obter_param_usuario(1125,96,obter_perfil_Ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_data_entrada_maior_w);
obter_param_usuario(1125,141,obter_perfil_Ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_consiste_comp_prot_w);

/* Obter dados da conta */
select	cd_convenio_parametro
into	cd_convenio_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

if	(nr_seq_protocolo_p	> 0) then
	nr_seq_protocolo_w	:= nr_seq_protocolo_p; /* Caso houver recebe o ultimo protocolo que teve conta inserida*/
else
	/* Buscar o tipo de protocolo para BPA */
	select	max(ie_tipo_protocolo_bpa),
		max(ie_tipo_protocolo_aih),
		max(ie_tipo_protocolo)
	into	ie_tipo_protocolo_bpa_w,
		ie_tipo_protocolo_aih_w,
		ie_tipo_protocolo_apac_w		
	from	parametro_faturamento
	where	cd_estabelecimento	= cd_estabelecimento_p;

	select	nvl(cd_estabelecimento,cd_estabelecimento_p)
	into	cd_estabelecimento_w
	from	conta_paciente
	where	nr_interno_conta	= nr_interno_conta_p;
       
	if	(ie_tipo_protocolo_p = 'AIH') then
		/* Obter o protocolo */
		select	nvl(max(nr_seq_protocolo),0)
		into	nr_seq_protocolo_w
		from	protocolo_convenio
		where	cd_convenio		= cd_convenio_w
		and	cd_estabelecimento	= cd_estabelecimento_w
		and	ie_tipo_protocolo	= ie_tipo_protocolo_aih_w
		and	ie_status_protocolo	= 1;
		ie_tipo_protocolo_w	:= ie_tipo_protocolo_aih_w;
	elsif	(ie_tipo_protocolo_p = 'BPA') then
		begin
		
		select	max(a.dt_entrada),
			max(ie_tipo_atendimento),
			max(dt_mesano_referencia)
		into	dt_entrada_paciente_w,
			ie_tipo_atendimento_w,
			dt_mesano_ref_conta_w
		from	atendimento_paciente a,
			conta_paciente b
		where	a.nr_atendimento = b.nr_atendimento
		and	b.nr_interno_conta = nr_interno_conta_p; 
		
		if 	(nvl(ie_consiste_comp_prot_w,'N') = 'S') then
			begin
			select	nvl(max(nr_seq_protocolo),0)
			into	nr_seq_protocolo_w
			from	protocolo_convenio
			where	cd_convenio		= cd_convenio_w
			and	ie_tipo_protocolo	= ie_tipo_protocolo_bpa_w
			and	ie_status_protocolo	= 1
			and	cd_estabelecimento	= cd_estabelecimento_w
			and	ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_mesano_referencia) = ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_mesano_ref_conta_w);
			end;
		end if;
	 
	 	if 	((nvl(ie_consiste_comp_prot_w,'N') = 'N') or
	    		(nr_seq_protocolo_w = 0)) then
	 		begin
			select	nvl(max(nr_seq_protocolo),0)
			into	nr_seq_protocolo_w
			from	protocolo_convenio
			where	cd_convenio		= cd_convenio_w
			and	ie_tipo_protocolo	= ie_tipo_protocolo_bpa_w
			and	ie_status_protocolo	= 1
			and	cd_estabelecimento	= cd_estabelecimento_w;
			end;
		end if;				
		
		select	max(dt_mesano_referencia)
		into	dt_referencia_protocolo_w
		from	protocolo_convenio
		where	nr_seq_protocolo = nr_seq_protocolo_w;
		
		if	(nvl(ie_data_entrada_maior_w,'S') = 'N')and
			(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_entrada_paciente_w) > ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_referencia_protocolo_w)) then
			wheb_mensagem_pck.exibir_mensagem_abort(204876,'NR_SEQ_PROTOCOLO_P='||nr_seq_protocolo_w);
			/*'A data de entrada do paciente eh maior que a data referencia do protocolo ' || nr_seq_protocolo_w || ' Verificar parametro[96].*/
		end if;		
				 
		ie_tipo_protocolo_w	:= ie_tipo_protocolo_bpa_w;
		end;
	elsif	(ie_tipo_protocolo_p = 'APAC') then
		select	nvl(max(nr_seq_protocolo),0)
		into	nr_seq_protocolo_w
		from	protocolo_convenio
		where	cd_convenio		= cd_convenio_w
		and	cd_estabelecimento	= cd_estabelecimento_w
		and	ie_tipo_protocolo	= ie_tipo_protocolo_apac_w
		and	ie_status_protocolo	= 1;
		ie_tipo_protocolo_w	:= ie_tipo_protocolo_apac_w;
	end if;
end if;

select	nvl(max(nr_protocolo),'')
into	nr_protocolo_w
from	protocolo_convenio
where	nr_seq_protocolo	= nr_seq_protocolo_w;

if	(nr_seq_protocolo_w	= 0) or
	(nr_seq_protocolo_w is null) then
	begin
	begin
	select	a.dt_mesano_referencia,
		ie_tipo_atendimento
	into	dt_mesano_referencia_W,
		ie_tipo_atendimento_w
	from	atendimento_paciente b,
		conta_paciente a
	where	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_interno_conta	= nr_interno_conta_p;
	exception
		when others then
		dt_mesano_referencia_w	:= sysdate;
	end;

	if	(ie_tipo_protocolo_p = 'APAC') then
		select	nvl(max(dt_competencia),dt_mesano_referencia_w)
		into	dt_mesano_referencia_w
		from	sus_apac_unif
		where	nr_interno_conta	= nr_interno_conta_p;	
	end if;
	
	select	protocolo_convenio_seq.nextval
	into	nr_seq_protocolo_w
	from	dual;
	
	begin
        
	insert into	protocolo_convenio(
		cd_convenio, 		nr_protocolo, 		ie_status_protocolo, 		dt_periodo_inicial,		
		dt_periodo_final, 		dt_atualizacao, 		nm_usuario, 			ie_tipo_protocolo,
		nr_seq_protocolo, 		dt_envio, 			nm_usuario_envio, 			ds_arquivo_envio,
		dt_retorno, 		nm_usuario_retorno, 	ds_arquivo_retorno, 		cd_setor_atendimento,
		cd_especialidade, 		ie_periodo_final, 		ds_parametro_atend, 		dt_geracao,
		nm_usuario_geracao, 	dt_mesano_referencia, 	cd_classif_setor, 			ie_tipo_atend_bpa,
		dt_consistencia, 		nr_seq_envio_convenio, 	dt_mesano_ref_par, 		ds_inconsistencia,
		dt_vencimento, 		dt_integracao_cr, 		dt_entrega_convenio, 		cd_interface_envio,
		cd_estabelecimento,	nr_seq_lote_receita,	nr_seq_lote_repasse,		nr_seq_lote_grat)
	values
		(cd_convenio_w,		to_char(nr_seq_protocolo_w),	1,			sysdate,
		sysdate,			sysdate,			nm_usuario_p,		ie_tipo_protocolo_w,
		nr_seq_protocolo_w,	null,			null,			null,
		null,			null,			null,			null,
		null,			null,			null,			sysdate,
		nm_usuario_p,		dt_mesano_referencia_w,	null,			null,
		null,			null,			null,			null, 
		null,			null,			null,			null,
		nvl(cd_estabelecimento_w,cd_estabelecimento_p),0,	0,			0);
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(204877);
		/*Erro ao inserir registro no protocolo convenio*/
	end;
	end;
end if;

if	(nvl(nr_seq_protocolo_w,0) <> 0) then

	if	(ie_opcao_fpo_p <> 'N') then
		begin
		
		select	max(dt_mesano_referencia)
		into	dt_mesano_referencia_w
		from	protocolo_convenio
		where	nr_seq_protocolo = nr_seq_protocolo_w;
		
		if	(dt_mesano_referencia_w is not null) then
			begin			

			sus_consiste_conta_fpo_unif(dt_mesano_referencia_w,
						nr_interno_conta_p,
						ie_tipo_atendimento_w,
						cd_estabelecimento_w,
						nr_seq_protocolo_w,
						ie_opcao_fpo_p,
						ds_erro_w);
			
			end;
		end if;
		
		end;
	end if;
	
	if	(ie_tipo_protocolo_p = 'BPA') then
		begin
		
		if	(nvl(ie_cons_data_Proc_bpa_w,'S') = 'N') then
			begin
			
			select	max(dt_mesano_referencia)
			into	dt_referencia_protocolo_w
			from	protocolo_convenio
			where	nr_seq_protocolo = nr_seq_protocolo_w;
		
			select	count(*)
			into	qt_proc_fora_comp_w
			from	procedimento_paciente a,
				conta_paciente b
			where	a.nr_interno_conta = b.nr_interno_conta 
			and	a.nr_interno_conta = nr_interno_conta_p
			and	ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(a.dt_procedimento) > ESTABLISHMENT_TIMEZONE_UTILS.startOfMonth(dt_referencia_protocolo_w);
			
			if	(qt_proc_fora_comp_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(838622,'NR_SEQ_PROTOCOLO_P='||nr_seq_protocolo_w);
				/*Existem procedimentos lancados com data superior a referencia do protocolo #@NR_SEQ_PROTOCOLO_P#@. Parametro [69]!*/
			end if;
			
			end;
		end if;
		
		if	(((nvl(ie_bloq_FPO_BPA_w,'N') = 'S') and
			(ds_erro_w = 'S')) or
			(nvl(ie_bloq_FPO_BPA_w,'N') = 'N'))	then
			begin
			update	conta_paciente
			set	nr_seq_protocolo 	= nr_seq_protocolo_w,
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_interno_conta	= nr_interno_conta_p
			and	ie_status_acerto	= 2; 
			end;
		end if;		
		end;
	elsif	(ie_tipo_protocolo_p = 'AIH') then
		begin
		if	(((nvl(ie_bloq_FPO_AIH_w,'N') = 'S') and
			(ds_erro_w = 'S')) or
			(nvl(ie_bloq_FPO_AIH_w,'N') = 'N'))	then
			begin                                      
                                             
			update	conta_paciente
			set	nr_seq_protocolo 	= nr_seq_protocolo_w,
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_interno_conta	= nr_interno_conta_p
			and	ie_status_acerto	= 2;

			end;
		end if;		
		end;
	elsif	(ie_tipo_protocolo_p = 'APAC') then
		begin
		if	(((nvl(ie_bloq_FPO_APAC_w,'N') = 'S') and
			(ds_erro_w = 'S')) or
			(nvl(ie_bloq_FPO_APAC_w,'N') = 'N'))	then
			begin
			update	conta_paciente
			set	nr_seq_protocolo 	= nr_seq_protocolo_w,
				nm_usuario		= nm_usuario_p,
				dt_atualizacao		= sysdate
			where	nr_interno_conta	= nr_interno_conta_p
			and	ie_status_acerto	= 2; 
			end;
		end if;		
		end;
	else
		begin
		update	conta_paciente
		set	nr_seq_protocolo 	= nr_seq_protocolo_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_interno_conta	= nr_interno_conta_p
		and	ie_status_acerto	= 2; 		
		end;		
	end if;
	
end if;

ds_erro_fpo_p := ds_erro_w;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end sus_insere_protocolo;
/