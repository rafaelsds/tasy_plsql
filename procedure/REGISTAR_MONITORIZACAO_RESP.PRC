create or replace
procedure registar_monitorizacao_resp(	nr_atendimento_p	number,
					qt_co2_p		number,
					nm_usuario_p		varchar2,
					cd_pessoa_fisica_p	varchar2) is 

nr_sequencia_w	number(10);					
					
begin

if	(nr_atendimento_p > 0) and
	(qt_co2_p > 0) then
	
	insert into atendimento_monit_resp (	
		nr_sequencia,
		nr_atendimento,
		qt_co2,
		dt_liberacao,
		dt_monitorizacao,
		dt_atualizacao,
		nm_usuario,
		cd_pessoa_fisica,
		ie_situacao)
	values(	atendimento_monit_resp_seq.nextval,
		nr_atendimento_p,
		qt_co2_p,
		sysdate,
		sysdate,
		sysdate,
		nm_usuario_p,
		cd_pessoa_fisica_p,
		'A');
end if;

commit;

end registar_monitorizacao_resp;
/