create or replace procedure dar_dashboard_drag_drop(seq_source_p in out dar_dashboard.nr_sequencia%type,
													nr_seq_order_source_p in out dar_dashboard.nr_seq_order%type,
													seq_target_p in out dar_dashboard.nr_sequencia%type,
													nr_seq_order_target_p in out dar_dashboard.nr_seq_order%type) is
begin


   -- Update seq source
   update dar_dashboard set nr_seq_order = nr_seq_order_source_p where nr_sequencia = seq_target_p;


   -- Update seq target
   update dar_dashboard set nr_seq_order = nr_seq_order_target_p where nr_sequencia = seq_source_p;
   
   --
   commit;

end dar_dashboard_drag_drop;
/
