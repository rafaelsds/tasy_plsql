create or replace
procedure alterar_dt_pls_pfpj_incon(
		cd_pessoa_fisica_p	varchar2,
		cd_cgc_p		varchar2,
		nr_sequencia_p		number,
		ds_email_p		varchar2,
		ds_msg_p out		varchar2,
		nm_usuario_p		varchar2) is 

ds_msg_w	varchar2(4000);
begin
if	(nm_usuario_p is not null) and
	(nr_sequencia_p is not null) then
	begin
	
	if	(cd_pessoa_fisica_p is not null) then 
		begin
		update	pls_pessoa_inconsistente 
		set	dt_envio_email		= sysdate,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_seq_lote		= nr_sequencia_p
		and	cd_pessoa_fisica	= cd_pessoa_fisica_p;
		end;
	elsif	(cd_cgc_p is not null) then
		begin
		update	pls_pessoa_inconsistente
		set	dt_envio_email	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_seq_lote	= nr_sequencia_p
		and	cd_cgc		= cd_cgc_p;
		end;
	end if;
	
	ds_msg_w := obter_texto_dic_objeto(82274, wheb_usuario_pck.get_nr_seq_idioma, 'DS_DESTINO=' || ds_email_p || ';');
	
	end;
end if;
ds_msg_p	:= ds_msg_w;
commit;
end alterar_dt_pls_pfpj_incon;
/