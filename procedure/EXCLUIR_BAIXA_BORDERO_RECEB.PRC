create or replace 
procedure Excluir_Baixa_Bordero_Receb
			(	nr_bordero_p		number,
				nm_usuario_p		varchar2,
				dt_exclusao_p		date) is

ie_permite_est_conci_w		varchar2(255);
vl_saldo_tit_rec_w		number(15,2);
vl_baixa_w			number(15,2);
nr_titulo_w			number(10);
nr_sequencia_w			number(10);
nr_seq_baixa_w			number(10);
nr_seq_solic_desc_w		number(10)	:= null;
nr_adiantamento_w		number(10);
cont_w				number(10);
nr_seq_conciliacao_w		number(10);
nr_seq_movto_pend_w		number(10);
qt_registros_w			number(10)	:= 0;
cd_estabelecimento_w		number(4);
nr_seq_trib_w			titulo_receber_trib.nr_sequencia%type;
/* Projeto Multimoeda - Variaveis */
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;
vl_receb_estrang_w		number(15,2);
dt_recebimento_orig_w			titulo_receber_liq.dt_recebimento%type;
dt_recebimento_w				titulo_receber_liq.dt_recebimento%type;
ie_estorno_no_mes_w				varchar2(1);
nr_seq_baixa_origem_w		movto_banco_pend_baixa.nr_sequencia%type;

cursor	C01 is
	select	b.nr_titulo
	from	bordero_tit_rec	a,
		titulo_receber	b
	where	a.nr_bordero	= nr_bordero_p
	and	a.nr_titulo	= b.nr_titulo;

Cursor C02 is
	select	nr_sequencia	
	from	titulo_receber_liq_desc
	where	nr_titulo	= nr_titulo_w
	and	nr_seq_liq	= nr_sequencia_w;

cursor	C03 is
	select	a.nr_seq_movto_pend,
			nvl(a.vl_baixa,0) * -1,
			a.nr_sequencia
	from	movto_banco_pend_baixa a
	where	a.nr_bordero_rec	= nr_bordero_p
	and		a.nr_seq_baixa_origem is null --AAMFIRMO OS 1108516, apenas baixas que ainda nao foram estornadas, pois essa rotina ja e de estorno, nao faz sentido esse cursor trazer tb baixas ja estornadas.
	and		a.vl_baixa >= 0 --AAMFIRMO OS 1108516, apenas baixas positivas, pois essa rotina ja e de estorno, nao faz sentido esse cursor trazer tb baixas ja estornadas.
	and not exists ( select 1
					from movto_banco_pend_baixa x
						where x.nr_seq_movto_pend = a.nr_seq_movto_pend
						and x.nr_seq_baixa_origem = a.nr_sequencia);

Cursor	C04 is
select	a.nr_sequencia
from	titulo_receber_trib a
where	exists
	(select	1
	from	bordero_tit_rec_valor x
	where	nvl(x.nr_seq_trans_financ,0)	= nvl(a.nr_seq_trans_financ,0)
	and	x.vl_tributo	= a.vl_tributo
	and	x.cd_tributo	= a.cd_tributo
	and	x.nr_titulo	= a.nr_titulo
	and	x.nr_bordero	= nr_bordero_p)
and	a.nr_titulo	= nr_titulo_w;

begin
select	obter_valor_param_usuario(809, 8, obter_perfil_ativo, nm_usuario_p, 0)
into	ie_permite_est_conci_w
from	dual;

if	(nvl(ie_permite_est_conci_w, 'S') = 'N') then
	select	max(b.nr_seq_conciliacao)
	into	nr_seq_conciliacao_w
	from	concil_banc_movto b,
		movto_trans_financ	a
	where	a.nr_bordero_rec	= nr_bordero_p
	and	a.nr_seq_concil 	= b.nr_sequencia;
	
	if	(nr_seq_conciliacao_w is not null) then
		--Nao e possivel excluir a baixa deste bordero
		--Este bordero possui transacao na conciliacao NR_SEQ_CONCILIACAO_W Parametro [8]
		Wheb_mensagem_pck.exibir_mensagem_abort(213694,'NR_SEQ_CONCILIACAO_W='||nr_seq_conciliacao_w);
	end if;
end if;

/* Edgar 08/10/2009, OS 171314, comentei esta consistencia, pois nao faz sentido

select	nvl(max(a.nr_titulo),0)
into	nr_titulo_w
from	titulo_receber_liq b,
	titulo_receber a
where	b.nr_bordero	= nr_bordero_p
and	a.nr_titulo	= b.nr_titulo
and	nvl(b.nr_lote_contabil,0) <> 0
and	a.dt_liquidacao	is not null;

if (nr_titulo_w <> 0) then
	_application_error(-20011, 'Nao e possivel excluir a baixa deste bordero O titulo  || to_char(nr_titulo_w) ||  ja tem lote contabil);
end if;

*/

open C01;
loop
fetch C01 into
	nr_titulo_w;
exit when C01%notfound;

	projeto_recurso_pck.consistir_tit_pag_rec_proj_rec(nr_titulo_w, 'R');

	/* Francisco - OS 53631 - Separei os selects */
	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_w
	and	nr_bordero	= nr_bordero_p;

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_baixa_w
	from	titulo_receber_liq
	where	nr_titulo = nr_titulo_w;

	insert into titulo_receber_liq
		(nr_titulo,              
		nr_sequencia,           
		dt_recebimento,         
		vl_recebido,            
		vl_descontos,           
		vl_juros,               
		vl_multa,               
		cd_moeda,               
		dt_atualizacao,         
		nm_usuario,             
		cd_tipo_recebimento,    
		ie_acao,    
		cd_serie_nf_devol,      
		nr_nota_fiscal_devol,
		cd_banco,               
		cd_agencia_bancaria,    
		nr_documento,           
		nr_lote_banco,          
		cd_cgc_emp_cred,        
		nr_cartao_cred,         
		nr_adiantamento,        
		nr_lote_contabil,       
		nr_seq_trans_fin,       
		vl_rec_maior,           
		vl_glosa,               
		nr_seq_retorno,         
		vl_adequado,            
		nr_seq_ret_item,        
		nr_seq_conta_banco,     
		vl_despesa_bancaria,   
		ds_observacao,                
		ie_lib_caixa,           
		nr_seq_trans_caixa,     
		cd_centro_custo_desc,
		nr_seq_motivo_desc,     
		nr_bordero,
		vl_perdas,
		vl_outros_acrescimos,
		vl_nota_credito,
		nr_lote_contab_antecip,
		nr_lote_contab_pro_rata,
		nr_seq_pls_lote_camara,
		vl_recebido_estrang,
		vl_complemento,
		vl_cotacao,
		vl_cambial_passivo,
		vl_cambial_ativo,
		nr_seq_liq_origem)
	select	nr_titulo_w,              
		nr_seq_baixa_w,           
		nvl(dt_exclusao_p,dt_recebimento),         
		vl_recebido * -1,            
		vl_descontos * -1,           
		vl_juros * -1,               
		vl_multa * -1,               
		cd_moeda,               
		sysdate,         
		nm_usuario_p,             
		cd_tipo_recebimento,    
		ie_acao,    
		cd_serie_nf_devol,      
		nr_nota_fiscal_devol,
		cd_banco,               
		cd_agencia_bancaria,    
		nr_documento,           
		nr_lote_banco,          
		cd_cgc_emp_cred,        
		nr_cartao_cred,         
		nr_adiantamento,        
		0,       
		nr_seq_trans_fin,       
		vl_rec_maior * -1,           
		vl_glosa * -1,               
		nr_seq_retorno,         
		vl_adequado * -1,            
		nr_seq_ret_item,        
		nr_seq_conta_banco,     
		vl_despesa_bancaria * -1,   
		ds_observacao,                
		ie_lib_caixa,           
		nr_seq_trans_caixa,     
		cd_centro_custo_desc,
		nr_seq_motivo_desc,     
		nr_bordero,
		vl_perdas * -1,
		vl_outros_acrescimos * -1,
		vl_nota_credito * -1,
		0,
		0,
		nr_seq_pls_lote_camara,
		decode(nvl(vl_recebido_estrang,0),0,null,vl_recebido_estrang * -1), -- Projeto Multimoeda - Inverte os valores quando a baixa for moeda estrangeira
		decode(nvl(vl_recebido_estrang,0),0,null,(vl_recebido - vl_recebido_estrang) * -1),
		decode(nvl(vl_cotacao,0),0,null,vl_cotacao),
		nvl(vl_cambial_passivo,0) * -1,
		nvl(vl_cambial_ativo,0) * -1,
		nr_sequencia
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_w
	and	nr_sequencia	= nr_sequencia_w;

	insert into titulo_rec_liq_cc
		(nr_sequencia,
		nr_titulo,
		nr_seq_baixa,
		dt_atualizacao,
		nm_usuario,
		cd_centro_custo,
		vl_baixa,
		vl_amaior,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_cta_ctb_origem,
		cd_conta_contabil,
		vl_recebido,
		cd_conta_deb_pls,
		cd_conta_rec_pls,
		nr_seq_mens_seg_item,
		cd_historico_pls,
		nr_seq_conta_pls,
		nr_seq_produto,
		ie_lote_pro_rata,
		vl_contab_pro_rata,
		cd_conta_rec_antecip,
		cd_conta_deb_antecip,
		cd_historico_rev_antec,
		cd_historico_antec_pls,
		cd_conta_antec_pls,
		ie_origem_classif,
		vl_recebido_estrang,
		vl_complemento,
		vl_cotacao,
		cd_moeda,
		vl_desconto,
		vl_juros,
		vl_multa)
	select	titulo_rec_liq_cc_seq.nextval,
		nr_titulo_w,
		nr_seq_baixa_w,
		sysdate,
		nm_usuario_p,
		cd_centro_custo,
		vl_baixa * -1,
		vl_amaior * -1,
		sysdate,
		nm_usuario_p,
		cd_cta_ctb_origem,
		cd_conta_contabil,
		vl_recebido * -1,
		cd_conta_deb_pls,
		cd_conta_rec_pls,
		nr_seq_mens_seg_item,
		cd_historico_pls,
		nr_seq_conta_pls,
		nr_seq_produto,
		ie_lote_pro_rata,
		vl_contab_pro_rata,
		cd_conta_rec_antecip,
		cd_conta_deb_antecip,
		cd_historico_rev_antec,
		cd_historico_antec_pls,
		cd_conta_antec_pls,
		ie_origem_classif,
		decode(nvl(vl_recebido_estrang,0),0,null,vl_recebido_estrang * -1), -- Projeto Multimoeda - Inverte os valores quando a baixa for moeda estrangeira
		decode(nvl(vl_recebido_estrang,0),0,null,(vl_recebido - vl_recebido_estrang) * -1),
		decode(nvl(vl_cotacao,0),0,null,vl_cotacao),
		cd_moeda,
		vl_desconto * -1,
		vl_juros * -1,
		vl_multa * -1
	from	titulo_rec_liq_cc
	where	nr_titulo	= nr_titulo_w
	and	nr_seq_baixa	= nr_sequencia_w;
	
	open C04;
	loop
	fetch C04 into	
		nr_seq_trib_w;
	exit when C04%notfound;
		begin
		insert into titulo_receber_trib
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_trans_financ,
			nr_lote_contabil,
			nr_titulo,
			cd_tributo,
			tx_tributo,
			vl_tributo,
			vl_base_calculo,
			vl_trib_adic,
			vl_base_adic,
			vl_trib_nao_retido,
			vl_base_nao_retido,
			ie_origem_tributo,
			dt_tributo)
		select	titulo_receber_trib_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_trans_financ,
			nr_lote_contabil,
			nr_titulo,
			cd_tributo,
			0,
			vl_tributo * -1,
			0,
			0,
			0,
			0,
			0,
			ie_origem_tributo,
			nvl(dt_exclusao_p,dt_tributo)
		from	titulo_receber_trib
		where	nr_sequencia = nr_seq_trib_w;
		end;
	end loop;
	close C04;
	
	select	max(dt_recebimento)
	into	dt_recebimento_orig_w
	from	titulo_receber_liq
	where	nr_titulo		= nr_titulo_w
	and		nr_sequencia	= nr_sequencia_w;

	select	max(dt_recebimento)
	into	dt_recebimento_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_w
	and	nr_sequencia	= nr_seq_baixa_w;	
	
	if	(pkg_date_utils.start_of(dt_recebimento_w,'MONTH',0) = pkg_date_utils.start_of(dt_recebimento_orig_w,'MONTH',0)) then
		ie_estorno_no_mes_w	:= 'S';
	else	
		ie_estorno_no_mes_w	:= 'N';
	end if;

	insert into pls_titulo_rec_liq_mens
		(nr_sequencia,
		nr_titulo,
		nr_seq_baixa,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		cd_centro_custo,
		cd_conta_credito,
		cd_conta_debito,
		cd_historico,
		dt_adesao,
		dt_contabil,
		dt_fim_cobertura,
		dt_inicio_cobertura,
		dt_ref_contab_final,
		dt_ref_contab_inicial,
		dt_ref_mens,
		ie_tipo_lancamento,
		nr_lote_contabil,
		nr_seq_item_mens,
		nr_seq_segurado,
		vl_lancamento)
	select	pls_titulo_rec_liq_mens_seq.nextval,
		nr_titulo,
		nr_seq_baixa_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		cd_centro_custo,
		cd_conta_credito,
		cd_conta_debito,
		cd_historico,
		dt_adesao,
		decode(ie_estorno_no_mes_w,'S',dt_contabil,dt_recebimento_w),
		dt_fim_cobertura,
		dt_inicio_cobertura,
		dt_ref_contab_final,
		dt_ref_contab_inicial,
		dt_ref_mens,
		ie_tipo_lancamento,
		0,
		nr_seq_item_mens,
		nr_seq_segurado,
		vl_lancamento*-1
	from	pls_titulo_rec_liq_mens
	where	nr_titulo		= nr_titulo_w
	and		nr_seq_baixa	= nr_sequencia_w;
	
	gerar_baixa_nota_credito(	nr_seq_baixa_w,
					null,
					nr_bordero_p,
					nr_titulo_w,
					nm_usuario_p,
					'E');

	atualizar_saldo_tit_rec(nr_titulo_w,
				nm_usuario_p);

	Open C02;
	loop
	fetch C02 into
		nr_seq_solic_desc_w;
	exit when C02%notfound;
		insert into titulo_receber_liq_desc
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_pessoa_fisica,
			cd_cgc,
			nr_titulo,
			nr_seq_liq)
		select	titulo_receber_liq_desc_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_pessoa_fisica,
			cd_cgc,
			nr_titulo_w,
			nr_seq_baixa_w
		from	titulo_receber_liq_desc
		where	nr_sequencia	= nr_seq_solic_desc_w;
	end loop;
	close C02;
	
	select	count(*)
	into	qt_registros_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_w
	and	nr_sequencia	= nr_sequencia_w;
	
	if	(qt_registros_w > 0) then
		select	nr_adiantamento,
			vl_cotacao,
			vl_recebido_estrang
		into	nr_adiantamento_w,
			vl_cotacao_w,
			vl_receb_estrang_w
		from	titulo_receber_liq
		where	nr_titulo	= nr_titulo_w
		and	nr_sequencia	= nr_sequencia_w;
		
		/* Projeto Multimoeda - Verifica se a baixa e moeda estrangeira, caso positivo passa a cotacao para atualizar o saldo do adiantamento */
		if (nvl(vl_receb_estrang_w,0) = 0 and nvl(vl_cotacao_w,0) = 0) then
			vl_cotacao_w := null;
		end if;
		
		if	(nr_adiantamento_w is not null) then -- Francisco - OS - 50524 - Atualizar o saldo do adiantamento
			atualizar_saldo_adiantamento(	nr_adiantamento_w,
							nm_usuario_p,
							null,
							vl_cotacao_w);
		end if;
	end if;

end 	loop;
close	c01;

/* ahoffelder - OS 284918 - 02/02/2011 */
open C03;
loop
fetch C03 into
	nr_seq_movto_pend_w,
	vl_baixa_w,
	nr_seq_baixa_origem_w;
exit when C03%notfound;

	baixar_movto_banco_pend(nr_seq_movto_pend_w,
				nvl(dt_exclusao_p,sysdate),
				vl_baixa_w,
				null,
				null,
				nm_usuario_p,
				'N',
				nr_bordero_p,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				nr_seq_baixa_origem_w);

end loop;
close C03;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	bordero_recebimento
where	nr_bordero	= nr_bordero_p;

Gerar_movto_trans_bordero_rec(	nr_bordero_p,
				cd_estabelecimento_w,
				dt_exclusao_p,
				nm_usuario_p);

update	bordero_recebimento
set	dt_recebimento	= null,
	nm_usuario	= nm_usuario_p
where	nr_bordero	= nr_bordero_p;

commit;

end Excluir_Baixa_Bordero_Receb;
/
