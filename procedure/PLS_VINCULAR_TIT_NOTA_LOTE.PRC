create or replace
procedure pls_vincular_tit_nota_lote
			(	nr_seq_lote_p		number,
				cd_serie_nf_p		Varchar2,
				cd_operacao_nf_p	Number,
				dt_emissao_p		Date,
				cd_natureza_operacao_p	Number,
				ds_observacao_p		Varchar2,
				ds_complemento_p	Varchar2,
				dt_base_venc_p		Date,
				nr_nota_fiscal_p	Varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

nr_titulo_w			number(10);
nr_seq_nota_fiscal_w		number(10);
ds_consistencia_w		varchar2(255);
nr_nota_fiscal_w		varchar2(10);
dt_emissao_w			date;
nr_titulo_trib_w		number(10);
dt_vencimento_w			date;

Cursor C01 is
	select	b.nr_titulo
	from	titulo_pagar b,
		pls_lote_protocolo a
	where	a.nr_sequencia		= b.nr_seq_lote_res_pls
	and	b.ie_tipo_titulo	<> '4'
	and	a.nr_sequencia		= nr_seq_lote_p
	and	b.ie_situacao		= 'A';

Cursor C02 is
	select	b.nr_titulo
	from	titulo_pagar_imposto a,
		titulo_pagar b
	where	a.nr_sequencia	= b.nr_seq_tributo
	and	a.nr_titulo	= nr_titulo_w;
begin

select	max(nr_sequencia)
into	nr_seq_nota_fiscal_w
from	nota_fiscal
where	nr_seq_prot_res_pls = nr_seq_lote_p;

if	(nvl(nr_seq_nota_fiscal_w,0) > 0) then
	--'J� existe nota fiscal gerada para este lote.#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(203716);
end if;

update	pls_lote_protocolo
set	nr_nota_fiscal	= nr_nota_fiscal_p
where	nr_sequencia	= nr_seq_lote_p;

select	nr_nota_fiscal
into	nr_nota_fiscal_w
from	pls_lote_protocolo
where	nr_sequencia = nr_seq_lote_p;

pls_gerar_notas_lote(	nr_seq_lote_p,
			cd_serie_nf_p,
			cd_operacao_nf_p,
			dt_emissao_p,
			cd_natureza_operacao_p,
			ds_observacao_p,
			ds_complemento_p,
			dt_base_venc_p,
			null,
			nm_usuario_p,
			'S',
			cd_estabelecimento_p);

select	max(nr_sequencia)
into	nr_seq_nota_fiscal_w
from	nota_fiscal
where	nr_seq_lote_res_pls = nr_seq_lote_p;

if	(nvl(nr_seq_nota_fiscal_w,0) = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(203717);
else
	select	dt_emissao
	into	dt_emissao_w
	from	nota_fiscal
	where	nr_sequencia	= nr_seq_nota_fiscal_w;
end if;

open C01;
loop
fetch C01 into	
	nr_titulo_w;
exit when C01%notfound;
	begin
	Vincular_titulo_pagar_nf(nr_seq_nota_fiscal_w, nr_titulo_w, nm_usuario_p, 'A','S', ds_consistencia_w);
	
	/* Francisco - 18/02/2010 - OS - 188292 - Comentei pois o t�tulo ser� gerado com a data emiss�o digitada no lote
	update	titulo_pagar
	set	dt_emissao		= dt_emissao_w,
		dt_vencimento_original	= dt_vencimento_atual
	where	nr_titulo		= nr_titulo_w;
	
	select	dt_vencimento_atual
	into	dt_vencimento_w
	from	titulo_pagar
	where	nr_titulo	= nr_titulo_w;
	
	open C02;
	loop
	fetch C02 into	
		nr_titulo_trib_w;
	exit when C02%notfound;
		begin
		update	titulo_pagar
		set	dt_emissao		= dt_emissao_w,
			dt_vencimento_original	= dt_vencimento_w,
			dt_vencimento_atual	= dt_vencimento_w
		where	nr_titulo		= nr_titulo_trib_w;
		end;
	end loop;
	close C02;
	*/
	
	end;
end loop;
close C01;

--pls_atualizar_tributos_lote(nr_seq_lote_p, nm_usuario_p, cd_estabelecimento_p);

commit;

end pls_vincular_tit_nota_lote;
/