create or replace
procedure proj_tp_cli_rot_acao_add(
			nr_seq_rot_item_p	number,
			ds_nc_p				varchar2,
			nr_seq_tipo_nc_p	number,
			ds_acao_p			varchar2,
			nm_usuario_p		varchar2) is 

begin

insert into proj_tp_cli_rot_acao(
	nr_sequencia,
	nr_seq_rot_item,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	ds_nc,
	ds_acao,
	nr_seq_tipo_nc)
values (								
	proj_tp_cli_rot_acao_seq.nextval,
	nr_seq_rot_item_p,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	ds_nc_p,
	ds_acao_p,
	nr_seq_tipo_nc_p);
	
commit;

end proj_tp_cli_rot_acao_add;
/
