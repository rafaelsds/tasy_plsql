create or replace
procedure Aprovar_orcamento
		(nr_sequencia_p	 	number,
		nm_usuario_p	 	varchar2,
		cd_estabelecimento_p 	number) is

qt_reg_venc_w			number(8);
cd_condicao_pagamento_w		number(10);
vl_desconto_w			number(15,4);
vl_desconto_proced_w		number(15,4);
vl_desconto_material_w		number(15,4);

begin

if	(nr_sequencia_p is not null) then
	
	update 	orcamento_paciente
	set 	ie_status_orcamento  = 2,
		dt_aprovacao 	     = sysdate,
		nm_usuario_aprovacao = nm_usuario_p
	where 	nr_sequencia_orcamento = nr_sequencia_p;	
	
	select 	count(*)
	into	qt_reg_venc_w
	from 	orcamento_paciente_venc
	where 	nr_sequencia_orcamento = nr_sequencia_p;
	
	if	(qt_reg_venc_w = 0) then
		
		select	cd_condicao_pagamento
		into	cd_condicao_pagamento_w
		from 	orcamento_paciente
		where 	nr_sequencia_orcamento = nr_sequencia_p;
		
		Atualizar_Vencimento_Orcamento( nr_sequencia_p,
                          			cd_estabelecimento_P,
						cd_condicao_pagamento_w,
						sysdate,
						nm_usuario_p);

	end if;
	
	select	nvl(vl_desconto,0)
	into	vl_desconto_w
	from	orcamento_paciente
	where	nr_sequencia_orcamento		= nr_sequencia_p;
	
	if	(vl_desconto_w		= 0) then
		select	nvl(sum(vl_desconto),0)
		into	vl_desconto_proced_w
		from	orcamento_paciente_proc
		where	nr_sequencia_orcamento = nr_sequencia_p;
		
		select	nvl(sum(vl_desconto),0)
		into	vl_desconto_material_w
		from	orcamento_paciente_mat
		where	nr_sequencia_orcamento = nr_sequencia_p;
		
		vl_desconto_w		:= vl_desconto_proced_w + vl_desconto_material_w;
		
		update	orcamento_paciente
		set	vl_desconto	= vl_desconto_w
		where	nr_sequencia_orcamento = nr_sequencia_p;
	end if;
	
end if;

commit;

end Aprovar_orcamento;
/