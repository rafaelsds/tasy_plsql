create or replace 
procedure FIN_FECHAR_MES
		(nr_sequencia_p	in	number,
		 nm_usuario_p		in	varchar2) is

begin

update	fin_mes_ref
set	DT_ATUALIZACAO		= sysdate,
	NM_USUARIO			= nm_usuario_p,
	DT_FECHAMENTO			= sysdate,
	NM_USUARIO_FECHAMENTO	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

/* inserir histórico de fechamento - ahoffelder - 22/09/2009 - OS 167526 */
insert	into fin_mes_ref_hist
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_fin_mes,
	ds_historico)
	values
	(fin_mes_ref_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_sequencia_p,
	OBTER_DESC_EXPRESSAO(728964));

commit;

end FIN_FECHAR_MES;
/
