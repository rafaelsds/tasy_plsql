create or replace
procedure pls_duplicar_tab_contr_interc
			(	nr_seq_intercambio_p		number,
				nr_seq_plano_p			number,
				nr_seq_tabela_p			number,
				nr_seq_tabela_nova_p	out	number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

dt_contrato_w			date;
nr_seq_tabela_w			number(10);
qt_idade_inicial_w		number(5);
qt_idade_final_w		number(5);
vl_preco_atual_w		number(15,2);
tx_acrescimo_w			number(7,4);
qt_registros_w			number(10);
ie_tabela_contrato_w		varchar2(1);
nm_estipulante_w		varchar2(80);
ie_tipo_contratacao_w		varchar2(3);
vl_preco_nao_subsidiado_w	Number(15,2);
ie_preco_w			Varchar2(2);
nr_seq_contrato_tabela_w	Number(10);
nr_seq_intercambio_w		Number(10);
vl_minimo_w			Number(15,2);
ie_grau_titularidade_w		varchar2(2);
IE_TIPO_REPASSE_w		varchar2(10);

cursor c01 is
	select	qt_idade_inicial,
		qt_idade_final,
		vl_preco_atual,
		tx_acrescimo,
		vl_preco_nao_subsidiado,
		vl_minimo,
		ie_grau_titularidade
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_p
	order by qt_idade_inicial;

begin

begin
select	max(IE_TIPO_REPASSE)
into	IE_TIPO_REPASSE_w
from	pls_segurado
where	nr_seq_intercambio	= nr_seq_intercambio_p;
exception
when others then
	IE_TIPO_REPASSE_w	:= 'X';
end;

if	(IE_TIPO_REPASSE_w = 'P') then
	select	nr_sequencia,
		nvl(dt_inclusao,sysdate),
		substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc),1,80)
	into	nr_seq_intercambio_w,
		dt_contrato_w,
		nm_estipulante_w
	from	pls_intercambio
	where	nr_sequencia	= nr_seq_intercambio_p;
	
	if	(nr_seq_plano_p is not null) and
		(nr_seq_tabela_p is not null) then
		
		select	pls_tabela_preco_seq.nextval
		into	nr_seq_tabela_w
		from	dual;
	
		insert into pls_tabela_preco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nm_tabela,
			dt_inicio_vigencia,
			nr_seq_plano,
			ie_tabela_base,
			NR_SEQ_CONTRATO_INTER,
			nr_seq_tabela_origem,
			nr_seq_faixa_etaria)
		select	nr_seq_tabela_w,
			sysdate,
			nm_usuario_p,
			'Tabela para o contrato ' || to_char(nr_seq_intercambio_w) || ' - ' || nm_estipulante_w || ' - 2 ',
			dt_contrato_w,
			nr_seq_plano_p,
			'N',
			nr_seq_intercambio_p,
			nr_sequencia,
			nr_seq_faixa_etaria
		from	pls_tabela_preco
		where	nr_sequencia	= nr_seq_tabela_p;
		
		open c01;
		loop
		fetch c01 into	qt_idade_inicial_w,
				qt_idade_final_w,
				vl_preco_atual_w,
				tx_acrescimo_w,
				vl_preco_nao_subsidiado_w,
				vl_minimo_w,
				ie_grau_titularidade_w;
		exit when c01%notfound;
			
			insert into pls_plano_preco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tabela,
				qt_idade_inicial,
				qt_idade_final,
				vl_preco_inicial,
				vl_preco_atual,
				tx_acrescimo,
				vl_preco_nao_subsidiado,
				vl_minimo,
				ie_grau_titularidade)
			values	(pls_plano_preco_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_tabela_w,
				qt_idade_inicial_w,
				qt_idade_final_w,
				vl_preco_atual_w,
				vl_preco_atual_w,
				tx_acrescimo_w,
				vl_preco_nao_subsidiado_w,
				vl_minimo_w,
				ie_grau_titularidade_w);
		end loop;
		close c01;
		
		update	pls_intercambio_plano
		set	nr_seq_tabela		= nr_seq_tabela_w,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_seq_intercambio	= nr_seq_intercambio_p
		and	nr_seq_tabela		= nr_seq_tabela_p
		and	nr_seq_plano		= nr_seq_plano_p;
		
		update	pls_tabela_preco
		set	dt_fim_vigencia	= sysdate
		where	nr_sequencia	= nr_seq_tabela_p;
		
		insert into pls_contrato_historico (	nr_sequencia,
					cd_estabelecimento,
					nr_seq_intercambio,
					dt_historico,
					ie_tipo_historico,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_historico)
				values(	pls_contrato_historico_seq.NextVal,
					cd_estabelecimento_p,
					nr_seq_intercambio_p,
					sysdate,
					3,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					'Altera��o de tabela de pre�o do contrato');
		
	end if;
end if;

commit;

nr_seq_tabela_nova_p	:= nr_seq_tabela_w;

end pls_duplicar_tab_contr_interc;
/