create or replace
procedure atualizar_saldo_movto_bco_pend
			(	nr_seq_movto_pend_p	number,
				nm_usuario_p		varchar2,
				ie_commit_p		varchar2) is

vl_baixa_w		number(15,2);
vl_credito_w		number(15,2);
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
vl_saldo_atual_w	number(15,2);
dt_baixa_w		date;
/* Projeto Multimoeda - Vari�veis*/
vl_credito_estrang_w	number(15,2);
vl_baixa_estrang_w	number(15,2);
vl_cotacao_w		cotacao_moeda.vl_cotacao%type;

begin
if	(nr_seq_movto_pend_p is not null) then
	select	vl_credito,
		vl_credito_estrang  -- Projeto Multimoeda - Busca o valor do cr�dito em moeda estrangeira
	into	vl_credito_w,
		vl_credito_estrang_w
	from	movto_banco_pend a
	where	a.nr_sequencia	= nr_seq_movto_pend_p;

	select	nvl(sum(a.vl_juros),0),
		nvl(sum(a.vl_multa),0),
		nvl(sum(a.vl_baixa),0),
		nvl(sum(a.vl_baixa_estrang),0)  -- Projeto Multimoeda - Busca o valor da baixa em moeda estrangeira
	into	vl_juros_w,
		vl_multa_w,
		vl_baixa_w,
		vl_baixa_estrang_w
	from	tipo_baixa_cni b,
		movto_banco_pend_baixa a
	where	a.nr_seq_tipo_baixa	= b.nr_sequencia(+)
	and	a.nr_seq_movto_pend	= nr_seq_movto_pend_p;

	select	max(dt_baixa)
	into	dt_baixa_w
	from	movto_banco_pend_baixa
	where	nr_seq_movto_pend	= nr_seq_movto_pend_p;
	
	vl_saldo_atual_w	:= vl_credito_w - vl_baixa_w - vl_juros_w - vl_multa_w;
	
	/* Projeto Multimoeda - Verifica se o movto pendente � em moeda estrangeira, se sim busca a cota��o da baixa mais recente para converter o saldo em moeda estrangeira*/
	if (nvl(vl_credito_estrang_w,0) <> 0) then
		select max(vl_cotacao)
		into vl_cotacao_w
		from movto_banco_pend_baixa
		where nr_seq_movto_pend = nr_seq_movto_pend_p
		  and dt_baixa = dt_baixa_w;
		if (nvl(vl_cotacao_w,0) <> 0) then
			vl_saldo_atual_w := (vl_credito_estrang_w - vl_baixa_estrang_w) * vl_cotacao_w;
		end if;
	end if;

	if	(vl_saldo_atual_w < 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(115731,'VL_BAIXA=' || (vl_baixa_w + vl_juros_w + vl_multa_w) || ';' || 'VL_CREDITO=' || vl_credito_w);
	end if;

		
	update	movto_banco_pend
	set	vl_saldo	= abs(vl_saldo_atual_w),
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		dt_liquidacao	= decode(vl_saldo_atual_w,0,nvl(dt_baixa_w,sysdate),null)
	where	nr_sequencia	= nr_seq_movto_pend_p;

end if;

if	(ie_commit_p = 'S') then
	commit;
end if;

end atualizar_saldo_movto_bco_pend;
/
