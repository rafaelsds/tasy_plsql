create or replace
procedure BL_vincular_frasco_prod_lac(	nr_seq_frasco_p		Number,
					nr_seq_nut_prod_p	Number,
					nm_usuario_p		Varchar2) is 

begin
if	(nr_seq_frasco_p is not null) and (nr_seq_nut_prod_p is not null) then

	update	bl_frasco
	set	dt_utilizacao 	= sysdate,
		nr_seq_nut_prod	= nr_seq_nut_prod_p
	where	nr_sequencia	= nr_seq_frasco_p;
	
	update	bl_frasco
	set	ie_situacao_processa = 'P',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_frasco_p;

end if;

commit;

end BL_vincular_frasco_prod_lac;
/