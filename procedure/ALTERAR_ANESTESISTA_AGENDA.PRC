create or replace
procedure alterar_anestesista_agenda(
					nr_seq_agenda_p		number,
					cd_anestesista_p	varchar2,
					nr_seq_escala_p		number,
					nm_usuario_p		varchar2) is 
				
nr_atendimento_w 		number(10);
nr_cirurgia_w	 		number(10);
ie_gera_hist_w			varchar2(1);
nm_paciente_w   		varchar2(255);
dt_agenda_w				varchar2(30);
nm_anestesista_w 		varchar2(255);
ds_historico_w			varchar2(4000);
nm_agenda_w				varchar2(255);
nm_anestesista_old_w	varchar2(255);
cd_anestesista_old_w	varchar2(10);

begin

obter_param_usuario(871,757,wheb_usuario_pck.get_cd_perfil,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_gera_hist_w);

if	(nr_seq_agenda_p > 0) then
	select	max(cd_anestesista)
	into	cd_anestesista_old_w
	from	agenda_paciente
	where	nr_sequencia	= nr_seq_agenda_p;
	
	update	agenda_paciente
	set	cd_anestesista	= cd_anestesista_p,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_agenda_p;

	insert into 
		agenda_paciente_escala (nr_sequencia,
					nr_seq_agenda,
					nr_seq_escala,
					cd_anestesista,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec)
				values	(agenda_paciente_escala_seq.nextval,
					nr_seq_agenda_p,
					nr_seq_escala_p,
					cd_anestesista_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p);

	if	(ie_gera_hist_w = 'S') then
		select	substr(decode(max(cd_pessoa_fisica),null,max(nm_paciente),obter_nome_pf(max(cd_pessoa_fisica))),1,240),
			to_char(max(dt_agenda),'dd/mm/yyyy') || ' ' || to_char(max(hr_inicio),'hh24:mi:ss'),
			max(nr_atendimento),
			max(nr_cirurgia),
			max(substr(nm_agenda,1,255))
		into	nm_paciente_w,
			dt_agenda_w,
			nr_atendimento_w,
			nr_cirurgia_w,
			nm_agenda_w
		from	programacao_cirurgica_v	
		where	nr_sequencia	= nr_seq_agenda_p;
		
		nm_anestesista_w	:= SUBSTR(obter_nome_pf(cd_anestesista_p),1,255);
	
		if	(cd_anestesista_old_w is null) then
			ds_historico_w	:= wheb_mensagem_pck.get_texto(300459, 'NM_PACIENTE_W=' || nm_paciente_w || ';DT_AGENDA_W=' || dt_agenda_w || '  ' || nm_agenda_w ||
									';NR_SEQ_AGENDA_P=' || nr_seq_agenda_p || ';NM_ANESTESISTA_W=' || nm_anestesista_w || 
									';NR_SEQ_ESCALA_P=' || SUBSTR(OBTER_DESC_ESCALA(nr_seq_escala_p),1,255));
		else
			nm_anestesista_old_w	:= SUBSTR(obter_nome_pf(cd_anestesista_old_w),1,255);
			ds_historico_w	:= wheb_mensagem_pck.get_texto(300460, 'NM_PACIENTE_W=' || nm_paciente_w || ';DT_AGENDA_W=' || dt_agenda_w || '  ' || nm_agenda_w ||
									';NR_SEQ_AGENDA_P=' || nr_seq_agenda_p || ';NM_ANESTESISTA_OLD=' || nm_anestesista_old_w || ';NM_ANESTESISTA_W=' || nm_anestesista_w || 
									';NR_SEQ_ESCALA_P=' || SUBSTR(OBTER_DESC_ESCALA(nr_seq_escala_p),1,255));
		end if;		       
		grava_historico_agenda_cir(nr_seq_agenda_p,ds_historico_w,Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C'),null,null,null,null,nm_usuario_p);
	end if;					
end if;

commit;

end alterar_anestesista_agenda;
/