create or replace
procedure envio_sup_int_nf(	nr_seq_nota_p			number,
			ie_tipo_nota_p			varchar2,
			cd_estabelecimento_p		number,
			nr_nota_fiscal_p			varchar2,
			dt_emissao_p			date,
			cd_cgc_emitente_p			varchar2,
			cd_cgc_p			varchar2,
			cd_pessoa_fisica_p			varchar2,
			cd_serie_nf_p			varchar2,
			cd_natureza_operacao_p		number,
			cd_condicao_pagamento_p		number,
			cd_operacao_nf_p			number,
			vl_seguro_p			number,
			vl_despesa_acessoria_p		number,
			vl_frete_p				number,
			ds_observacao_p			varchar2,
			nm_usuario_p			varchar2) is 

nr_sequencia_w			number(10);
nr_item_nf_w			number(10);
cd_unidade_medida_compra_w	varchar2(30);
ie_de_para_unid_med_w		varchar2(15);
ie_de_para_material_w		varchar2(15);
cd_material_w			number(6);

cursor c01 is
select	nr_item_nf,
	cd_unidade_medida_compra,
	cd_material
from	nota_fiscal_item
where	nr_sequencia = nr_seq_nota_p;
				
begin

select	sup_int_nf_seq.nextval
into	nr_sequencia_w
from	dual;

insert into sup_int_nf(
	nr_sequencia,
	ie_forma_integracao,
	nr_seq_nota,
	ie_tipo_nota,
	cd_estabelecimento,
	nr_nota_fiscal,
	dt_emissao,
	cd_cgc_emitente,
	cd_cgc,
	cd_pessoa_fisica,
	cd_serie_nf,
	cd_natureza_operacao,
	cd_condicao_pagamento,
	cd_operacao_nf,
	vl_seguro,
	vl_despesa_acessoria,
	vl_frete,
	ds_observacao,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec)
values(	nr_sequencia_w,
	'E',
	nr_seq_nota_p,
	ie_tipo_nota_p,
	cd_estabelecimento_p,
	nr_nota_fiscal_p,
	dt_emissao_p,
	cd_cgc_emitente_p,
	cd_cgc_p,
	cd_pessoa_fisica_p,
	cd_serie_nf_p,
	cd_natureza_operacao_p,
	cd_condicao_pagamento_p,
	cd_operacao_nf_p,
	vl_seguro_p,
	vl_despesa_acessoria_p,
	vl_frete_p,
	ds_observacao_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p);

open C01;
loop
fetch C01 into	
	nr_item_nf_w,
	cd_unidade_medida_compra_w,
	cd_material_w;
exit when C01%notfound;
	begin
	
	select	obter_ie_de_para_sup_integr('NF','E','UNIDADE_MEDIDA'),
		obter_ie_de_para_sup_integr('NF','E','MATERIAL')
	into	ie_de_para_unid_med_w,
		ie_de_para_material_w
	from	dual;
	
	/*Conversao para unidade de medida*/
	if	(ie_de_para_unid_med_w = 'C') then
		cd_unidade_medida_compra_w	:= nvl(Obter_Conversao_externa(null,'UNIDADE_MEDIDA','CD_UNIDADE_MEDIDA',cd_unidade_medida_compra_w),cd_unidade_medida_compra_w);
	elsif	(ie_de_para_unid_med_w = 'S') then		
		cd_unidade_medida_compra_w	:= nvl(obter_dados_unid_medida(cd_unidade_medida_compra_w,'SA'),cd_unidade_medida_compra_w);
	end if;
	/*Fim*/
	
	/*Conversao para material*/
	if	(ie_de_para_material_w = 'C') then
		cd_material_w	:= nvl(Obter_Conversao_externa(null,'MATERIAL','CD_MATERIAL',cd_material_w),cd_material_w);
	elsif	(ie_de_para_material_w = 'S') then		
		cd_material_w	:= nvl(obter_dados_material_estab(cd_material_w, cd_estabelecimento_p, 'CSA'),cd_material_w);
	end if;
	/*Fim*/
	
	insert into sup_int_nf_item(
		nr_sequencia,
		nr_item_nf,
		cd_material,
		cd_procedimento,
		qt_item_nf,
		cd_unidade_medida_compra,
		vl_unitario_item_nf,
		pr_desconto,
		vl_desconto,
		cd_local_estoque,
		cd_centro_custo,
		cd_conta_contabil,
		ds_observacao,
		cd_lote_fabricacao,
		dt_validade,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_origem_proced)
	select	nr_sequencia_w,
		nr_item_nf,
		cd_material_w,
		cd_procedimento,
		qt_item_nf,
		cd_unidade_medida_compra_w,
		vl_unitario_item_nf,
		pr_desconto,
		vl_desconto,
		cd_local_estoque,
		cd_centro_custo,
		cd_conta_contabil,
		ds_observacao,
		cd_lote_fabricacao,
		dt_validade,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ie_origem_proced
	from	nota_fiscal_item
	where	nr_sequencia	= nr_seq_nota_p
	and	nr_item_nf	= nr_item_nf_w;	
	
	end;
end loop;
close C01;	
	
update	sup_int_nf
set	dt_liberacao = sysdate
where	nr_sequencia = nr_sequencia_w;

end envio_sup_int_nf;
/
