create or replace
procedure pls_cancelar_autorizacao
			(	nr_seq_guia_plano_p		number,
				nr_seq_motivo_cancel_p		number,
				nm_usuario_p			varchar2,
				ie_origem_p			varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Cancelar a guia
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_guia_w                       varchar2(30);
ie_situacao_prot_w              varchar2(3);
ie_status_w			varchar2(2);
ie_cancela_guia_solic_mat_w	varchar2(2);
nr_seq_guia_proc_w		number(10);
nr_seq_guia_mat_w		number(10);
nr_seq_atend_pls_w		number(10);
nr_seq_evento_atend_w		number(10);
qt_auditoria_assumida_w		number(10)	:= 0;
qt_registros_w			number(10)	:= 0;
ie_estagio_w			number(10);
nr_seq_auditoria_w		number(10);
nr_seq_requisicao_w		number(10);
nr_seq_req_proc_w		number(10);
nr_seq_req_mat_w		number(10);
nr_seq_regra_compl_w            number(10);
nr_seq_segurado_w               number(10);
nr_seq_conta_compl_w            number(10);
qt_registros_aud_w		number(10);
qt_reg_solic_mat_med_pend_w	number(10);
qt_contas_canceladas_w		number(10);
qt_contas_w			number(10);
qt_protocolos_conta_cancel_w	number(10);
qt_protocolos_conta_w		number(10);
qt_vinculadas_w			number(10);
qt_vinculadas_canc_w		number(10);
qt_item_w			number(9,3);
qt_exec_req_w			number(9,3);
qt_atualizada_w			number(9,3);
qt_reg_w 			number(5);
ie_tipo_processo_w		Varchar2(2);
ie_tipo_intercambio_w		Varchar2(2);
nr_seq_req_item_w		pls_execucao_req_item.nr_sequencia%type;
nr_seq_execucao_w		pls_execucao_req_item.nr_seq_execucao%type;
nr_seq_guia_exec_w		pls_guia_plano.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	pls_guia_plano_proc
	where	nr_seq_guia 	= nr_seq_guia_plano_p;

Cursor C02 is
	select	nr_sequencia
	from	pls_guia_plano_mat
	where	nr_seq_guia 	= nr_seq_guia_plano_p;

Cursor C03 is
	select	nr_sequencia
	from	pls_auditoria
	where	nr_seq_guia	= nr_seq_guia_plano_p;

Cursor C05 is
	select	nr_seq_req_proc,
		nr_seq_req_mat,
		nvl(qt_item,0),
		nr_sequencia,
		nr_seq_execucao
	from	pls_execucao_req_item
	where	nr_seq_guia		= nr_seq_guia_plano_p
	and	nr_seq_requisicao	= nr_seq_requisicao_w;

Cursor C06 is
	select  a.nr_sequencia,
		b.ie_situacao
	from    pls_protocolo_conta b,
		pls_conta a
	where   a.nr_seq_protocolo      = b.nr_sequencia
	and     a.nr_seq_segurado       = nr_seq_segurado_w
	and     a.cd_guia               = cd_guia_w
	and     a.ie_origem_conta       = 'C';

begin
select	count(1),
	sum(decode(a.ie_estagio,8,1,4,1,0))
into	qt_vinculadas_w,
	qt_vinculadas_canc_w
from	pls_guia_plano	a
where	a.nr_seq_guia_principal	= nr_seq_guia_plano_p;

if	(qt_vinculadas_w <> qt_vinculadas_canc_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(256408);
end if;

begin
select  ie_estagio,
	nr_seq_regra_compl,
	nr_seq_segurado,
	cd_guia,
	ie_tipo_processo,
	ie_tipo_intercambio
into    ie_estagio_w,
	nr_seq_regra_compl_w,
	nr_seq_segurado_w,
	cd_guia_w,
	ie_tipo_processo_w,
	ie_tipo_intercambio_w
from    pls_guia_plano
where   nr_sequencia    = nr_seq_guia_plano_p;
exception
when others then
        ie_estagio_w    	:= 0;
	ie_tipo_processo_w	:= null;
	ie_tipo_intercambio_w	:= null;
end;

begin
nr_seq_requisicao_w	:= pls_obter_req_execucao_guia(nr_seq_guia_plano_p);
exception
when others then
	nr_seq_requisicao_w	:= null;
end;

/* Francisco - 04/07/2012 - OS 465531 */
if      (nr_seq_regra_compl_w is not null) then
        open C06;
        loop
        fetch C06 into
                nr_seq_conta_compl_w,
                ie_situacao_prot_w;
        exit when C06%notfound;
                begin
		/* Se o protocolo j� foi rejeitado, nem precisa mais alterar nem dar mensagem */
		if	(nvl(ie_situacao_prot_w,'I') <> 'RE') then
			if      (nvl(ie_situacao_prot_w,'I')  <> 'I') then
				/*Conforme conversa por telefone com o Felipe Bueno da Unimed S�o Jos� do Rio Preto, caso a conta esteja cancelada, deve ser permitido cancelar a guia, mesmo que o status do protocolo esteja como
				Aceito, Digitado ou Integrado*/
				select	count(1)
				into	qt_contas_w
				from	pls_conta	a
				where	a.nr_seq_guia	= nr_seq_guia_plano_p;

				if	(qt_contas_w	= 0) then
					select  count(1)
					into	qt_reg_w
					from    pls_conta
					where   nr_seq_segurado	= nr_seq_segurado_w
					and     cd_guia		= cd_guia_w
					and	ie_status	= 'C';
				else
					select  count(1)
					into	qt_reg_w
					from    pls_conta
					where   nr_seq_segurado	= nr_seq_segurado_w
					and     nr_seq_guia	= nr_seq_guia_plano_p
					and	ie_status	= 'C';
				end if;

				if	(qt_reg_w = 0) then
					wheb_mensagem_pck.exibir_mensagem_abort(196005);
				end if;
			else
				/* Criar novo protocolo rejeitado e transferir conta para este novo */
				pls_gerar_protocolo_rejeitado(	nr_seq_conta_compl_w,
								nm_usuario_p);
			end if;
		end if;
                end;
        end loop;
        close c06;
else
	if (nr_seq_guia_plano_p is not null) then
		if	(ie_tipo_processo_w	= 'I') and (ie_tipo_intercambio_w	= 'E') and (ie_origem_p	<> 'I') then
			-- Voc� n�o pode realizar esta a��o em uma guia recebida via interc�mbio!
			wheb_mensagem_pck.exibir_mensagem_abort(271396);
		end if;

		select	count(1)
		into	qt_contas_canceladas_w
		from	pls_conta	a
		where	a.nr_seq_guia	= nr_seq_guia_plano_p
		and	(a.ie_status	= 'C');

		if	(qt_contas_canceladas_w	= 0) then
			select	count(1)
			into	qt_contas_canceladas_w
			from	pls_conta		a
			where	a.cd_guia		= cd_guia_w
			and	a.nr_seq_segurado	= nr_seq_segurado_w
			and	(a.ie_status		= 'C');
		end if;

		select	count(1)
		into	qt_contas_w
		from	pls_conta	a
		where	a.nr_seq_guia	= nr_seq_guia_plano_p;

		if	(qt_contas_w	= 0) then
			select	count(1)
			into	qt_contas_w
			from	pls_conta		a
			where	a.cd_guia		= cd_guia_w
			and	a.nr_seq_segurado	= nr_seq_segurado_w;
		end if;

		if 	(qt_contas_canceladas_w <> qt_contas_w) then
			select	count(1)
			into	qt_protocolos_conta_cancel_w
			from	pls_conta		a,
				pls_protocolo_conta	b
			where	a.nr_seq_protocolo	= b.nr_sequencia
			and	a.nr_seq_guia		= nr_seq_guia_plano_p
			and	(b.nr_seq_motivo_cancel is not null);

			if	(qt_protocolos_conta_cancel_w	= 0) then
				select	count(1)
				into	qt_protocolos_conta_cancel_w
				from	pls_conta		a,
					pls_protocolo_conta	b
				where	a.nr_seq_protocolo	= b.nr_sequencia
				and	a.cd_guia		= cd_guia_w
				and	a.nr_seq_segurado	= nr_seq_segurado_w
				and	(b.nr_seq_motivo_cancel is not null);
			end if;

			select	count(1)
			into	qt_protocolos_conta_w
			from	pls_conta		a,
				pls_protocolo_conta	b
			where	a.nr_seq_protocolo	= b.nr_sequencia
			and	a.nr_seq_guia		= nr_seq_guia_plano_p;

			if	(qt_protocolos_conta_w	= 0) then
				select	count(1)
				into	qt_protocolos_conta_w
				from	pls_conta		a,
					pls_protocolo_conta	b
				where	a.nr_seq_protocolo	= b.nr_sequencia
				and	a.cd_guia		= cd_guia_w
				and	a.nr_seq_segurado	= nr_seq_segurado_w;
			end if;

			if	(qt_protocolos_conta_cancel_w <> qt_protocolos_conta_w) then
				wheb_mensagem_pck.exibir_mensagem_abort(196005);--N�o � poss�vel cancelar essa guia, j� foram geradas contas vinculadas a ela.
			end if;
		end if;
	end if;
end if;

if	(nvl(nr_seq_requisicao_w,0) > 0) then
	select  count(1)
	into	qt_reg_w
	from    pls_protocolo_conta b,
		pls_conta a
	where   a.nr_seq_protocolo      = b.nr_sequencia
	and     a.nr_seq_segurado       = nr_seq_segurado_w
	and     a.cd_guia               = cd_guia_w
	and     a.ie_origem_conta       <> 'C'
	and	a.ie_status		<> 'C'
	and	b.ie_situacao		in ('D', 'T');

	if	(qt_reg_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(196005);
	end if;

	select	count(1)
	into	qt_reg_w
	from	pls_guia_plano
	where	nr_seq_guia_principal = nr_seq_guia_plano_p
	and	ie_estagio <> 8;

	if	(qt_reg_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(205193);
	end if;

	select	count(1)
	into	qt_reg_w
	from	pls_requisicao
	where	nr_seq_guia_principal = nr_seq_guia_plano_p
	and	ie_estagio <> 3;

	if	(qt_reg_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(212944);
		--A guia n�o pode ser cancelada, pois existem requisi��es refer�nciando esta guia. Favor verificar!
	end if;
end if;

select	max(ie_cancela_guia_solic_mat)
into	ie_cancela_guia_solic_mat_w
from	pls_param_autorizacao;

if	(ie_cancela_guia_solic_mat_w	= 'S') then
	select	count(1)
	into	qt_reg_solic_mat_med_pend_w
	from	pls_solic_lib_mat_med
	where	nr_seq_guia	= nr_seq_guia_plano_p
	and	ie_status	in (1, 2, 3);

	if	(qt_reg_solic_mat_med_pend_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(216628);
		--A guia n�o pode ser cancelada pois existem solicita��es de libera��o de mat/med vinculadas a ela!
	end if;
end if;

if	(nvl(ie_estagio_w,0)	= 1) then
	select	count(1)
	into	qt_auditoria_assumida_w
	from	pls_auditoria a,
		pls_auditoria_grupo b
	where	a.nr_seq_guia	= nr_seq_guia_plano_p
	and	a.nr_sequencia	= b.nr_seq_auditoria
	and	nm_usuario_exec	is not null;

	select	count(1)
	into	qt_registros_w
	from	pls_guia_plano_historico
	where	nr_seq_guia = nr_seq_guia_plano_p
	and	nr_seq_item is not null;

	if	((qt_auditoria_assumida_w > 0) or
		(qt_registros_w	> 0)) then
		update	pls_auditoria
		set	ie_status		= 'C',
			dt_liberacao		= sysdate,
			nr_seq_proc_interno	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_guia		= nr_seq_guia_plano_p;
	elsif	(qt_auditoria_assumida_w	= 0) then
		pls_desfazer_auditoria(nr_seq_guia_plano_p);
	end if;
end if;

open C01;
loop
fetch C01 into
	nr_seq_guia_proc_w;
exit when C01%notfound;
	begin
	update	pls_guia_plano_proc
	set	ie_status	= 'D'
	where	nr_sequencia	= nr_seq_guia_proc_w;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into
	nr_seq_guia_mat_w;
exit when C02%notfound;
	begin
	update	pls_guia_plano_mat
	set	ie_status	= 'D'
	where	nr_sequencia	= nr_seq_guia_mat_w;
	end;
end loop;
close C02;

if	(nr_seq_guia_plano_p is not null) then
	update	pls_guia_plano
	set	dt_cancelamento		= sysdate,
		ie_status		= 3,
		ie_estagio		= 8,
		nr_seq_motivo_cancel	= nr_seq_motivo_cancel_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_guia_plano_p;

	if	(nr_seq_motivo_cancel_p is not null) then

		pls_guia_gravar_historico(	nr_seq_guia_plano_p,
						7,
						substr('Cancelada pelo usu�rio '||nm_usuario_p||'.'||chr(13)||'Motivo: '||
						substr(pls_obter_desc_guia_mot_cancel(nr_seq_motivo_cancel_p),1,255),1,4000)||'.',
						'',
						nm_usuario_p);
	end if;
end if;

begin
select	nr_seq_atend_pls,
	nr_seq_evento_atend,
	ie_status
into	nr_seq_atend_pls_w,
	nr_seq_evento_atend_w,
	ie_status_w
from	pls_guia_plano
where	nr_sequencia	= nr_seq_guia_plano_p;
exception
when others then
	nr_seq_atend_pls_w	:= null;
end;

if	(ie_status_w <> '2') and
	(nr_seq_atend_pls_w is not null) then
	pls_finalizar_atendimento(	nr_seq_atend_pls_w,
					nr_seq_evento_atend_w,
					null,
					null,
					nm_usuario_p);
end if;

pls_atualiza_estagio_guia_conv(	nr_seq_guia_plano_p,
				nm_usuario_p);

if	(ie_origem_p = 'R') then
	if	(nvl(nr_seq_requisicao_w,0) > 0) then
		open C05;
		loop
		fetch C05 into
			nr_seq_req_proc_w,
			nr_seq_req_mat_w,
			qt_item_w,
			nr_seq_req_item_w,
			nr_seq_execucao_w;
		exit when C05%notfound;
			begin
			if	(nvl(nr_seq_req_item_w,0) > 0) then
				update	pls_execucao_req_item
				set	ie_situacao	= 'C'
				where	nr_sequencia	= nr_seq_req_item_w;
			end if;
			
			if	(nvl(nr_seq_req_proc_w,0) > 0) then
				select	nvl(qt_proc_executado,0)
				into	qt_exec_req_w
				from	pls_requisicao_proc
				where	nr_sequencia	= nr_seq_req_proc_w;

				qt_atualizada_w	:= qt_item_w;

				/* Verificar se a quantidade do item na execu��o � maior que a quantidade do item executado na requisi��o, se for maior s� ser� diminuida a quantidade executada na requisi��o , evitando gerar valores negativos */
				if	(qt_item_w > qt_exec_req_w) then
					qt_atualizada_w	:= qt_exec_req_w;
				end if;
				
				/* OS 1282035 - Ao cancelar a guia deve limpar a sequ�ncia gravada no item da requisi��o.
				Tratamento para pegar a ultima guia executada para o item,  quando o item possuir mais de uma execu��o */
				select	max(nr_seq_guia)
				into	nr_seq_guia_exec_w
				from	pls_execucao_req_item
				where	nr_seq_requisicao	= nr_seq_requisicao_w
				and	ie_situacao in ('P', 'S');
				
				update	pls_requisicao_proc
				set	qt_proc_executado	= qt_proc_executado - qt_atualizada_w,
					nr_seq_guia		= nr_seq_guia_exec_w,
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= nr_seq_req_proc_w;
								
			elsif	(nvl(nr_seq_req_mat_w,0) > 0) then
				select	nvl(qt_mat_executado,0)
				into	qt_exec_req_w
				from	pls_requisicao_mat
				where	nr_sequencia	= nr_seq_req_mat_w;

				qt_atualizada_w	:= qt_item_w;

				/* Verificar se a quantidade do item na execu��o � maior que a quantidade do item executado na requisi��o, se for maior s� ser� diminuida a quantidade executada na requisi��o , evitando gerar valores negativos */
				if	(qt_item_w > qt_exec_req_w) then
					qt_atualizada_w	:= qt_exec_req_w;
				end if;

				update	pls_requisicao_mat
				set	qt_mat_executado	= qt_mat_executado - qt_atualizada_w,
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	nr_sequencia		= nr_seq_req_mat_w;
			end if;
			end;
		end loop;
		close C05;
				
		if	(nvl(nr_seq_execucao_w,0) > 0) then
			update	pls_execucao_requisicao
			set	ie_situacao	= 5
			where	nr_sequencia 	= nr_seq_execucao_w;
		end if;
	end if;
end if;

if	(nr_seq_guia_plano_p is not null) then
	select	count(1)
	into	qt_registros_aud_w
	from	pls_auditoria
	where	nr_seq_guia	= nr_seq_guia_plano_p
	and	dt_liberacao	is null;

	if	(qt_registros_aud_w > 0) then
		update	pls_auditoria
		set	ie_status		= 'C',
			dt_liberacao		= sysdate,
			nr_seq_proc_interno	= null,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_seq_guia		= nr_seq_guia_plano_p;
	end if;
end if;

commit;

end pls_cancelar_autorizacao;
/