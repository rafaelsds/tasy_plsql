create or replace
procedure Consiste_Agenda_Turno_Cons(cd_agenda_p			in	number,
									ie_dia_semana_p			in	number,
									dt_inicial_p			in	date,
									dt_final_p				in	date,
									hr_inicial_p			in	date,
									hr_final_p				in	date,
									nr_seq_sala_p			in	number,
									ie_consiste_horario_p 	in	varchar2,
									nm_usuario_p			in	varchar2,
									ds_erro_p				out	varchar2,
									nr_seq_turno_p			in	number,
									ie_frequencia_p			in varchar2 default null,
									ie_semana_p				in number default 0) is

ds_erro_w				varchar2(4000) := '';
qt_agenda_w				number(10,0);
cd_agenda_ocup_w		number(10,0);
cd_agenda_ocup_bloq_w	number(10,0);
nm_pessoa_w				varchar2(100);
cd_medico_w				varchar2(10);
qt_agenda_exame_w		number(10,0) := 0;
qt_agenda_exame_tot_w	number(10,0) := 0;
ds_agenda_w				varchar2(50);
ds_agenda_geral_w		varchar2(4000);
ie_tipo_w				number(10,0);
ie_consistir_horario_w	varchar2(1);
ie_consiste_sala_w		varchar2(1);
ie_consiste_medico_w	varchar2(1);
dt_inicial_w			date;
dt_final_w				date;
qt_agenda_turno_med_w	number(10,0);
ie_verifica_lib_bloq_w	number(10,0) := 0;
dt_ini_lib_w			date := null;
dt_fim_lib_w			date := null;
qt_regra_bloq_sala_w 	number := 0;

dt_calc_vg_w 			date;
nr_dias_add_w 			number(10);
dt_fim_loop_w 			date := dt_inicial_p;
cont_w 					number(10) := 0;

ie_frequencia_w			agenda_turno.ie_frequencia%type := ie_frequencia_p;

cursor	c02 is
	select	count(*),
			1 ie_tipo,
			b.ds_agenda
	from	agenda b,
			agenda_horario a
	where	exists	(	select	1
				from	agenda_medico x
				where	x.cd_agenda	= a.cd_agenda
				and	x.nr_sequencia	= a.nr_seq_medico_exec
				and	x.cd_medico	= cd_medico_w
			)
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	dt_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	group by b.ds_agenda
	union
	select	count(*),
		2 ie_tipo,
		b.ds_agenda
	from	agenda b,
		agenda_horario a
	where	exists	(	select	1
				from	agenda_medico x
				where	x.cd_agenda	= a.cd_agenda
				and	x.nr_sequencia	= a.nr_seq_medico_exec
				and	x.cd_medico	= cd_medico_w
			)
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	dt_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	group by b.ds_agenda
	union
	select	count(*),
		3 ie_tipo,
		b.ds_agenda
	from	agenda b,
		agenda_horario a
	where	exists	(	select	1
				from	agenda_medico x
				where	x.cd_agenda	= a.cd_agenda
				and	x.nr_sequencia	= a.nr_seq_medico_exec
				and	x.cd_medico	= cd_medico_w
			)
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	dt_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	group by b.ds_agenda
	union
	select	count(*),
		4 ie_tipo,
		b.ds_agenda
	from	agenda b,
		agenda_horario a
	where	exists	(	select	1
				from	agenda_medico x
				where	x.cd_agenda	= a.cd_agenda
				and	x.nr_sequencia	= a.nr_seq_medico_exec
				and	x.cd_medico	= cd_medico_w
			)
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	dt_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	group by b.ds_agenda
	union
	select	count(*),
		5 ie_tipo,
		b.ds_agenda
	from	agenda b,
		agenda_horario a
	where	exists	(	select	1
				from	agenda_medico x
				where	x.cd_agenda	= a.cd_agenda
				and	x.nr_sequencia	= a.nr_seq_medico_exec
				and	x.cd_medico	= cd_medico_w
			)
	and	a.cd_agenda	= b.cd_agenda
	and	b.ie_situacao	= 'A'
	and	dt_dia_semana	= ie_dia_semana_p
	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')

	and	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >
			to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
	group by b.ds_agenda;

begin

/* Rafael em 22/09/2006 OS40064 -> Somente ira consistir horarios quando o parametro[119] da agenda de consultas estiver definido como 'SIM' */
select	nvl(max(obter_valor_param_usuario(821, 119, obter_perfil_ativo, nm_usuario_p, 0)),'S'),
		nvl(max(obter_valor_param_usuario(821, 302, obter_perfil_ativo, nm_usuario_p, 0)),'S'),
		nvl(max(obter_valor_param_usuario(821, 464, obter_perfil_ativo, nm_usuario_p, 0)),'S')
into	ie_consistir_horario_w,
		ie_consiste_sala_w,
		ie_consiste_medico_w
from	dual;

select	max(cd_pessoa_fisica)
into	cd_medico_w
from	agenda
where	cd_agenda	= cd_agenda_p;

dt_inicial_w	:= dt_inicial_p;
dt_final_w		:= dt_final_p;

if(to_char(dt_inicial_p,'ddmmyyyy hh24mi') <> to_char(sysdate,'ddmmyyyy hh24mi')) then
	dt_ini_lib_w := dt_inicial_p;
end if;

if(to_char(dt_final_p,'ddmmyyyy hh24mi') <> to_char(sysdate,'ddmmyyyy hh24mi')) then
	dt_fim_lib_w := dt_final_p;
end if;

if	(to_char(dt_final_w, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy'))then
	dt_final_w	:= null;
end if;

if	(ie_consiste_sala_w = 'S') and
	(nr_seq_sala_p > 0) and
	(nvl(ie_frequencia_w,'D') = 'D') then

	select 	count(*)
	into	qt_regra_bloq_sala_w
	from	AGECONS_REGRA_TURNO_SALA art
	where 	nvl(ie_situacao, 'A') = 'A';

	if(qt_regra_bloq_sala_w > 0) then

		select max(qt_lib_sala) qt_lib_sala
		 into ie_verifica_lib_bloq_w from (
				 SELECT COUNT(1) qt_lib_sala
			  FROM AGENDA_TURNO AGT,
				   AGENDA_BLOQUEIO AB,
				   AGECONS_REGRA_TURNO_SALA ATS
			 WHERE AGT.CD_AGENDA = AB.CD_AGENDA
			   AND AB.IE_MOTIVO_BLOQUEIO = ATS.IE_MOTIVO_BLOQUEIO
			   AND nvl(AB.NR_SEQ_MOTIVO_BLOQ_AG,0) = nvl(NVL(ATS.NR_SEQ_MOTIVO,AB.NR_SEQ_MOTIVO_BLOQ_AG),0)
			   and (agt.ie_dia_semana = nvl(ab.ie_dia_semana,agt.ie_dia_semana)
				or (agt.ie_dia_semana = 9 and nvl(ab.ie_dia_semana,agt.ie_dia_semana) not in (7,1))
				or (ab.ie_dia_semana = 9 and agt.ie_dia_semana not in (7,1)))
			   and ((ie_dia_semana_p = nvl(ab.ie_dia_semana,ie_dia_semana_p))
				or (ab.ie_dia_semana = 9 and ie_dia_semana_p not in (1,7)))
			   AND AGT.HR_INICIAL <= NVL(AB.HR_INICIO_BLOQUEIO,AGT.HR_INICIAL)
			   AND AGT.HR_FINAL >= NVL(AB.HR_FINAL_BLOQUEIO,AGT.HR_FINAL)
			   AND AB.DT_INICIAL >= NVL(AGT.DT_INICIO_VIGENCIA, AB.DT_INICIAL)
			   AND AB.DT_FINAL >= NVL(AGT.DT_FINAL_VIGENCIA, AB.DT_FINAL) --VER ESSA LINHA
			   AND to_date('30121899 '||to_char(hr_inicial_p,'hh24miss'),'ddmmyyyy hh24miss') >= NVL(AB.HR_INICIO_BLOQUEIO,to_date('30121899 '||to_char(hr_inicial_p,'hh24miss'),'ddmmyyyy hh24miss'))
			   and to_date('30121899 '||to_char(hr_inicial_p,'hh24miss'),'ddmmyyyy hh24miss') <= NVL(AB.HR_FINAL_BLOQUEIO,to_date('30121899 '||to_char(hr_final_p,'hh24miss'),'ddmmyyyy hh24miss'))
			   AND to_date('30121899 '||to_char(hr_final_p,'hh24miss'),'ddmmyyyy hh24miss') <= NVL(AB.HR_FINAL_BLOQUEIO,to_date('30121899 '||to_char(hr_final_p,'hh24miss'),'ddmmyyyy hh24miss'))
			   AND AB.DT_INICIAL <= nvl(dt_ini_lib_w,to_date('30121899','ddmmyyyy')) and AB.DT_FINAL >= nvl(dt_ini_lib_w,to_date('30121899','ddmmyyyy'))
			   AND AB.DT_FINAL >= nvl(dt_fim_lib_w,to_date('31122999 235959','ddmmyyyy hh24miss'))
			   and ab.cd_agenda <> cd_agenda_p
			   and agt.nr_seq_sala = nr_seq_sala_p
			UNION ALL
			SELECT COUNT(1) qt_lib_sala
			  FROM AGENDA_TURNO_ESP AGT,
				   AGENDA_BLOQUEIO AB,
				   AGECONS_REGRA_TURNO_SALA ATS
			 WHERE AGT.CD_AGENDA = AB.CD_AGENDA
			   AND AB.IE_MOTIVO_BLOQUEIO = ATS.IE_MOTIVO_BLOQUEIO
			   AND nvl(AB.NR_SEQ_MOTIVO_BLOQ_AG,0) = nvl(NVL(ATS.NR_SEQ_MOTIVO,AB.NR_SEQ_MOTIVO_BLOQ_AG),0)
			   and (agt.ie_dia_semana = nvl(ab.ie_dia_semana,agt.ie_dia_semana)
				or (agt.ie_dia_semana = 9 and nvl(ab.ie_dia_semana,agt.ie_dia_semana) not in (7,1))
				or (ab.ie_dia_semana = 9 and agt.ie_dia_semana not in (7,1)))
			   and ((ie_dia_semana_p = nvl(ab.ie_dia_semana,ie_dia_semana_p))
				or (ab.ie_dia_semana = 9 and ie_dia_semana_p not in (1,7)))
			   AND AGT.HR_INICIAL <= NVL(AB.HR_INICIO_BLOQUEIO,AGT.HR_INICIAL)
			   AND AGT.HR_FINAL >= NVL(AB.HR_FINAL_BLOQUEIO,AGT.HR_FINAL)
			   AND AB.DT_INICIAL >= AGT.DT_AGENDA
			   AND AB.DT_FINAL >= NVL(AGT.DT_AGENDA_FIM, AB.DT_FINAL)
			   AND to_date('30121899 '||to_char(hr_inicial_p,'hh24miss'),'ddmmyyyy hh24miss') >= NVL(AB.HR_INICIO_BLOQUEIO,to_date('30121899 '||to_char(hr_inicial_p,'hh24miss'),'ddmmyyyy hh24miss'))
			   and to_date('30121899 '||to_char(hr_inicial_p,'hh24miss'),'ddmmyyyy hh24miss') <= NVL(AB.HR_FINAL_BLOQUEIO,to_date('30121899 '||to_char(hr_final_p,'hh24miss'),'ddmmyyyy hh24miss'))
			   AND to_date('30121899 '||to_char(hr_final_p,'hh24miss'),'ddmmyyyy hh24miss') <= NVL(AB.HR_FINAL_BLOQUEIO,to_date('30121899 '||to_char(hr_final_p,'hh24miss'),'ddmmyyyy hh24miss'))
			   AND AB.DT_INICIAL <= nvl(dt_ini_lib_w,to_date('30121899','ddmmyyyy')) and AB.DT_FINAL >= nvl(dt_ini_lib_w,to_date('30121899','ddmmyyyy'))
			   AND AB.DT_FINAL >= nvl(dt_fim_lib_w,to_date('31122999 235959','ddmmyyyy hh24miss'))
			   and ab.cd_agenda <> cd_agenda_p
			   and agt.nr_seq_sala = nr_seq_sala_p);

	end if;

	if (ie_verifica_lib_bloq_w = 0)  then

	/*
	VIGENCIAS DO TURNO:
	Data incial/final nao informadas
	*/
	select	nvl(max(cd_agenda),0)
	into	cd_agenda_ocup_w
	from   	agenda_turno
	where  	cd_agenda       	<> 	cd_agenda_p
	and    	((ie_dia_semana   	= 	ie_dia_semana_p)
		or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
		or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
		or (ie_dia_semana_p is null))
	and		(ie_semana = ie_semana_p
		or	ie_semana_p = 0
		or	ie_semana = 0)
	and		nr_seq_sala			= 	nr_seq_sala_p
	and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
	or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
	and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
	or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
	and		dt_inicio_vigencia is null
	and		dt_final_vigencia is null
	order by cd_agenda;
	
	/*
	VIGENCIAS DO TURNO:
	Data inicial/final informadas
	*/

	if	(cd_agenda_ocup_w = 0) and
		(dt_final_w is not null)then
		--DATA FINAL INFORMADA NO TURNO QUE ESTA SENDO MODIFICADO

		select	nvl(max(cd_agenda),0)
		into	cd_agenda_ocup_w
		from   	agenda_turno
		where  	cd_agenda       	<> 	cd_agenda_p
		and    	((ie_dia_semana   	= 	ie_dia_semana_p)
			or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
			or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
			or (ie_dia_semana_p is null))
		and		(ie_semana = ie_semana_p
			or	ie_semana_p = 0
			or	ie_semana = 0)
		and		nr_seq_sala			= 	nr_seq_sala_p
		and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
		or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
		and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
		or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
		and		dt_inicio_vigencia is not null
		and		dt_final_vigencia is not null
		--and		(nvl(dt_inicial_p,sysdate)	>= dt_inicio_vigencia)
		and		(trunc(nvl(dt_inicial_p,sysdate))	= trunc(dt_inicio_vigencia))
		--and		(nvl(dt_final_p,sysdate)	<= dt_final_vigencia)
		and		(trunc(nvl(dt_final_p,sysdate))		= trunc(dt_final_vigencia))
		order by cd_agenda;
		
		if	(cd_agenda_ocup_w = 0)then

			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and		dt_inicio_vigencia is not null
			and		dt_final_vigencia is not null
			and		(nvl(dt_inicial_p,sysdate)	>= dt_inicio_vigencia)
				--or	(nvl(dt_inicial_p,sysdate)	<= dt_final_vigencia))
			and		(nvl(dt_final_p,sysdate)	<= dt_final_vigencia)
				--or	(nvl(dt_final_p,sysdate)	>= dt_final_vigencia))
			order by cd_agenda;

		end if;

		if	(cd_agenda_ocup_w = 0)then

			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and		dt_inicio_vigencia is not null
			and		dt_final_vigencia is not null
			--and		(nvl(dt_inicial_p,sysdate)	>= dt_inicio_vigencia)
			and		(nvl(dt_inicial_p,sysdate)	<= dt_final_vigencia)
			--and		(nvl(dt_final_p,sysdate)	<= dt_final_vigencia)
			and		(nvl(dt_final_p,sysdate)	>= dt_final_vigencia)
			order by cd_agenda;

		end if;

		if	(cd_agenda_ocup_w = 0)then

			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and		dt_inicio_vigencia is not null
			and		dt_final_vigencia is not null
			--and		(nvl(dt_inicial_p,sysdate)	>= dt_inicio_vigencia)
			and		(nvl(dt_inicial_p,sysdate)	<= dt_inicio_vigencia)
			--and		(nvl(dt_final_p,sysdate)	<= dt_final_vigencia)
			and		(nvl(dt_final_p,sysdate)	>= dt_inicio_vigencia)
			order by cd_agenda;

		end if;

	elsif (cd_agenda_ocup_w = 0) then

	--DATA INICIO E FIM DA AGENDA QUE ESTA SENDO MODIFICADA E NULA

		if	(cd_agenda_ocup_w = 0)then

			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and 	dt_inicio_vigencia is not null
			and 	dt_final_vigencia is not null
			and		dt_final_vigencia > nvl(dt_ini_lib_w,to_date('30121899 000000','ddmmyyyy hh24miss'))
			and		dt_inicio_vigencia < nvl(dt_fim_lib_w,to_date('31122999 235959','ddmmyyyy hh24miss'))
			order by cd_agenda;

			if	(cd_agenda_ocup_w = 0)then

				select	nvl(max(cd_agenda),0)
				into	cd_agenda_ocup_w
				from   	agenda_turno_esp
				where  	cd_agenda       	<> 	cd_agenda_p
				and    	((ie_dia_semana   	= 	ie_dia_semana_p)
					or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
					or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
					or (ie_dia_semana_p is null))
				and		nr_seq_sala			= 	nr_seq_sala_p
				and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
				or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
				and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
				or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
				and 	dt_agenda is not null
				and 	dt_agenda_fim is not null
				and		dt_agenda > nvl(dt_ini_lib_w,to_date('30121899 000000','ddmmyyyy hh24miss'))
				and		dt_agenda_fim < nvl(dt_fim_lib_w,to_date('31122999 235959','ddmmyyyy hh24miss'))
				order by cd_agenda;

			end if;

		end if;

	elsif	(cd_agenda_ocup_w = 0) and
			(dt_final_w is null)then
		--DATA FINAL NAO INFORMADA NO TURNO QUE ESTA SENDO MODIFICADO

		select	nvl(max(cd_agenda),0)
		into	cd_agenda_ocup_w
		from   	agenda_turno
		where  	cd_agenda       	<> 	cd_agenda_p
		and    	((ie_dia_semana   	= 	ie_dia_semana_p)
			or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
			or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
			or (ie_dia_semana_p is null))
		and		(ie_semana = ie_semana_p
			or	ie_semana_p = 0
			or	ie_semana = 0)
		and		nr_seq_sala			= 	nr_seq_sala_p
		and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
		or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
		and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
		or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
		and		dt_inicio_vigencia is not null
		and		dt_final_vigencia is not null
		and		(trunc(nvl(dt_inicial_p,sysdate))	= trunc(dt_inicio_vigencia))
		--and		(nvl(dt_final_p,sysdate)	<= dt_final_vigencia)
		and		(trunc(nvl(dt_final_w, dt_inicial_p))		= trunc(dt_final_vigencia))
		order by cd_agenda;

		if	(cd_agenda_ocup_w = 0)then

			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and		dt_inicio_vigencia is not null
			and		dt_final_vigencia is not null
			and		(nvl(dt_inicial_p,sysdate)		>= dt_inicio_vigencia)
				--or	(nvl(dt_inicial_p,sysdate)	<= dt_final_vigencia))
			and		(nvl(dt_final_w, dt_inicial_p)	<= dt_final_vigencia)
				--or	(nvl(dt_final_p,sysdate)	>= dt_final_vigencia))
			order by cd_agenda;

		end if;

		if	(cd_agenda_ocup_w = 0)then

			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and		dt_inicio_vigencia is not null
			and		dt_final_vigencia is not null
			and		(nvl(dt_inicial_p,sysdate)		>= dt_inicio_vigencia)
				--or	(nvl(dt_inicial_p,sysdate)	<= dt_final_vigencia))
			and		(nvl(dt_final_w, dt_inicial_p)	<= dt_final_vigencia)
				--or	(nvl(dt_final_p,sysdate)	>= dt_final_vigencia))
			order by cd_agenda;
		end if;

		if	(cd_agenda_ocup_w = 0)then

			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and		dt_inicio_vigencia is not null
			and		dt_final_vigencia is not null
			--and		(nvl(dt_inicial_p,sysdate)	>= dt_inicio_vigencia)
			and		(nvl(dt_inicial_p,sysdate)	<= dt_final_vigencia)
			--and		(nvl(dt_final_p,sysdate)	<= dt_final_vigencia)
			and		(nvl(dt_final_w, dt_inicial_p)	>= dt_final_vigencia)
			order by cd_agenda;

		end if;

		if	(cd_agenda_ocup_w = 0)then

			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and		dt_inicio_vigencia is not null
			and		dt_final_vigencia is not null
			--and		(nvl(dt_inicial_p,sysdate)	>= dt_inicio_vigencia)
			and		(nvl(dt_inicial_p,sysdate)	<= dt_inicio_vigencia)
			--and		(nvl(dt_final_p,sysdate)	<= dt_final_vigencia)
			and		(nvl(dt_final_w, dt_inicial_p)	>= dt_inicio_vigencia)
			order by cd_agenda;

		end if;

	end if;

	/*
	VIGENCIAS DO TURNO:
	Data inicial informada / Data final nao informada
	*/
	if	(cd_agenda_ocup_w = 0)then
		select	nvl(max(cd_agenda),0)
		into	cd_agenda_ocup_w
		from   	agenda_turno
		where  	cd_agenda       	<> 	cd_agenda_p
		and    	((ie_dia_semana   	= 	ie_dia_semana_p)
			or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
			or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
			or (ie_dia_semana_p is null))
		and		(ie_semana = ie_semana_p
			or	ie_semana_p = 0
			or	ie_semana = 0)
		and		nr_seq_sala			= 	nr_seq_sala_p
		and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
		or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
		and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
		or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
		and		dt_inicio_vigencia is not null
		and		dt_final_vigencia is null
		and		((nvl(dt_inicio_vigencia,sysdate) between nvl(dt_inicial_p,sysdate) and nvl(dt_final_p,sysdate) + 86399/86400) or
				(nvl(dt_final_vigencia,sysdate) between nvl(dt_inicial_p,sysdate) and nvl(dt_final_p,sysdate) + 86399/86400))
		/*and		((nvl(dt_inicial_p,sysdate)	>= dt_inicio_vigencia)
			or	(nvl(dt_inicial_p,sysdate)	<= nvl(dt_final_vigencia,sysdate)))
		and		((nvl(dt_final_p,sysdate)	<= nvl(dt_final_vigencia,sysdate))
			or	(nvl(dt_final_p,sysdate)	>= nvl(dt_final_vigencia,sysdate)))*/
		order by cd_agenda;

	end if;

	if	(cd_agenda_ocup_w = 0) and
		(dt_final_w is null)then
		select	nvl(max(cd_agenda),0)
		into	cd_agenda_ocup_w
		from   	agenda_turno
		where  	cd_agenda       	<> 	cd_agenda_p
		and    	((ie_dia_semana   	= 	ie_dia_semana_p)
			or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
			or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
			or (ie_dia_semana_p is null))
		and		(ie_semana = ie_semana_p
			or	ie_semana_p = 0
			or	ie_semana = 0)
		and		nr_seq_sala			= 	nr_seq_sala_p
		and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
		or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
		and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
		or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
		and		dt_inicio_vigencia is not null
		and		dt_final_vigencia is null
		and		nvl(dt_inicial_p,sysdate) >= dt_inicio_vigencia
		order by cd_agenda;

		if	(cd_agenda_ocup_w = 0)then
			select	nvl(max(cd_agenda),0)
			into	cd_agenda_ocup_w
			from   	agenda_turno
			where  	cd_agenda       	<> 	cd_agenda_p
			and    	((ie_dia_semana   	= 	ie_dia_semana_p)
				or ((ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		(ie_semana = ie_semana_p
				or	ie_semana_p = 0
				or	ie_semana = 0)
			and		nr_seq_sala			= 	nr_seq_sala_p
			and    	(to_char(hr_inicial_p, 'hh24:mi:ss') >= (to_char(hr_inicial, 'hh24:mi:ss'))
			or		(to_char(hr_final_p, 'hh24:mi:ss')	>= to_char(hr_inicial, 'hh24:mi:ss')))
			and		(to_char(hr_final_p, 'hh24:mi:ss')	<= (to_char(hr_final, 'hh24:mi:ss'))
			or		(to_char(hr_inicial_p, 'hh24:mi:ss') <= to_char(hr_final, 'hh24:mi:ss')))
			and		dt_inicio_vigencia is not null
			and		dt_final_vigencia is null
			and		nvl(dt_inicial_p,sysdate) <= dt_inicio_vigencia
			order by cd_agenda;

		end if;

	end if;

	if	(cd_agenda_ocup_w > 0) then

		/*INICIO - Consiste bloqueio em outras agendas que usam a mesma sala, verificando se as datas do turno/turno especial nao estao dentro do periodo
		do bloqueio(quando a sala estiver livre)*/

			if (ie_verifica_lib_bloq_w = 0) then

				--TURNO NORMAL
				select	nvl(max(b.cd_agenda), 0)
				into	cd_agenda_ocup_bloq_w
				from	agenda_bloqueio a,
						agenda b,
						agenda_turno c
				where	a.cd_agenda			=  b.cd_agenda
				and		b.cd_agenda			=  c.cd_agenda
				and		b.cd_tipo_agenda	=  3
				and		a.cd_agenda			<> cd_agenda_p
				and    	((a.ie_dia_semana   	= 	ie_dia_semana_p)
					or ((a.ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
					or ((ie_dia_semana_p = 9) and (a.ie_dia_semana not in (1,7)))
					or (ie_dia_semana_p is null))
				and		(c.ie_semana = ie_semana_p
					or	ie_semana_p = 0
					or	c.ie_semana = 0)
				and		c.nr_seq_sala		=  nr_seq_sala_p
				and		dt_inicial_p 		>= trunc(a.dt_inicial)
				and		dt_final_p			<= trunc(a.dt_final)
				and		((nvl(c.dt_inicio_vigencia,sysdate) between nvl(dt_inicial_p,sysdate) and nvl(dt_final_p,sysdate) + 86399/86400) or
						(nvl(c.dt_final_vigencia,sysdate) between nvl(dt_inicial_p,sysdate) and nvl(dt_final_p,sysdate) + 86399/86400));

				--TURNO ESPECIAL
				if	(cd_agenda_ocup_bloq_w = 0) then

					select	nvl(max(b.cd_agenda), 0)
					into	cd_agenda_ocup_bloq_w
					from	agenda_bloqueio a,
							agenda b,
							agenda_turno_esp c
					where	a.cd_agenda			=  b.cd_agenda
					and		b.cd_agenda			=  c.cd_agenda
					and		b.cd_tipo_agenda	=  3
					and		a.cd_agenda			<> cd_agenda_p
					and    	((a.ie_dia_semana   	= 	ie_dia_semana_p)
						or ((a.ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
						or ((ie_dia_semana_p = 9) and (a.ie_dia_semana not in (1,7)))
						or (ie_dia_semana_p is null))
					and		c.nr_seq_sala		=  nr_seq_sala_p
					and		dt_inicial_p 		>= trunc(a.dt_inicial)
					and		dt_final_p			<= trunc(a.dt_final);

				end if;

				select	substr(nvl(obter_nome_pf(cd_pessoa_fisica),'"'||wheb_mensagem_pck.get_texto(795843)||'"'),1,100)
				into	nm_pessoa_w
				from	agenda
				where	cd_agenda	= cd_agenda_ocup_w;

				ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280407,'NM_PESSOA='|| nm_pessoa_w||';CD_AGENDA_OCUP='||cd_agenda_ocup_w);

			/*FIM - Consiste bloqueio em outras agendas que usam a mesma sala, verificando se as datas do turno/turno especial nao estao dentro do periodo
			do bloqueio(quando a sala estiver livre)*/
			end if;
	end if;

	if	(cd_agenda_ocup_w = 0) then

		select	nvl(max(cd_agenda),0)
		into	cd_agenda_ocup_w
		from   	agenda_turno
		where  	cd_agenda       <> cd_agenda_p
		and    	ie_dia_semana   = ie_dia_semana_p
		and		nr_seq_sala		= nr_seq_sala_p
		and		(ie_semana = ie_semana_p
			or	ie_semana_p = 0
			or	ie_semana = 0)
		and    	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >=
				to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
		and		to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <=
				to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final,   'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
		and		(dt_inicial_w >= dt_inicio_vigencia)
		and		(nvl(dt_final_w, dt_inicial_w) <= dt_final_vigencia)
		and		cd_agenda_ocup_w = 0;
		
	end if;

	if	(cd_agenda_ocup_w > 0) and
		(ds_erro_w = '') then
		/*INICIO - Consiste bloqueio em outras agendas que usam a mesma sala, verificando se as datas do turno/turno especial nao estao dentro do periodo
		do bloqueio(quando a sala estiver livre)*/

		--TURNO NORMAL
		select	nvl(max(b.cd_agenda), 0)
		into	cd_agenda_ocup_bloq_w
		from	agenda_bloqueio a,
				agenda b,
				agenda_turno c
		where	a.cd_agenda			=  b.cd_agenda
		and		b.cd_agenda			=  c.cd_agenda
		and		b.cd_tipo_agenda	=  3
		and		a.cd_agenda			<> cd_agenda_p
		and    	((c.ie_dia_semana   	= 	ie_dia_semana_p)
			or ((c.ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
			or ((ie_dia_semana_p = 9) and (c.ie_dia_semana not in (1,7)))
			or (ie_dia_semana_p is null))
		and		(ie_semana = ie_semana_p
			or	ie_semana_p = 0
			or	ie_semana = 0)
		and		c.nr_seq_sala		=  nr_seq_sala_p
		and		dt_inicial_p 		>= trunc(a.dt_inicial)
		and		dt_final_p			<= trunc(a.dt_final);

		--TURNO ESPECIAL
		if	(cd_agenda_ocup_bloq_w = 0) then

			select	nvl(max(b.cd_agenda), 0)
			into	cd_agenda_ocup_bloq_w
			from	agenda_bloqueio a,
					agenda b,
					agenda_turno_esp c
			where	a.cd_agenda			=  b.cd_agenda
			and		b.cd_agenda			=  c.cd_agenda
			and		b.cd_tipo_agenda	=  3
			and		a.cd_agenda			<> cd_agenda_p
			and    	((c.ie_dia_semana   	= 	ie_dia_semana_p)
				or ((c.ie_dia_semana = 9) and (ie_dia_semana_p not in (1,7)))
				or ((ie_dia_semana_p = 9) and (c.ie_dia_semana not in (1,7)))
				or (ie_dia_semana_p is null))
			and		c.nr_seq_sala		=  nr_seq_sala_p
			and		dt_inicial_p 		>= trunc(a.dt_inicial)
			and		dt_final_p			<= trunc(a.dt_final);
		end if;

	commit;

			select	substr(nvl(obter_nome_pf(cd_pessoa_fisica),'"'||wheb_mensagem_pck.get_texto(795843)||'"'),1,100)
			into	nm_pessoa_w
			from	agenda
			where	cd_agenda	= cd_agenda_ocup_w;

			ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280407,'NM_PESSOA='|| nm_pessoa_w||';CD_AGENDA_OCUP='||cd_agenda_ocup_w);
		/*FIM - Consiste bloqueio em outras agendas que usam a mesma sala, verificando se as datas do turno/turno especial nao estao dentro do periodo
		do bloqueio(quando a sala estiver livre)*/
	end if;

	if	(cd_agenda_ocup_w	> 0) and
		(ds_erro_w = '') then
		begin

		select	substr(obter_nome_pf(cd_pessoa_fisica),1,100)
		into	nm_pessoa_w
		from	agenda
		where	cd_agenda	= cd_agenda_ocup_w;

		ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280407,'NM_PESSOA='|| nm_pessoa_w||';CD_AGENDA_OCUP='||cd_agenda_ocup_w);

		end;
	end if;

	end if;

end if;

if	(ie_consiste_sala_w = 'S') and
	(nr_seq_sala_p > 0) and
	(nvl(ie_frequencia_w,'D') <> 'D') then

	select decode(nvl(ie_frequencia_w,'D'),'D',1,'M',30,'Q',14,'S',7)
	  into nr_dias_add_w
	  from dual;

	<<principal>>
	for c in (
		select	a.nr_sequencia, a.DT_INICIO_VIGENCIA, a.cd_agenda, a.ie_semana
		from	agenda_turno a,
				agenda b
		where	a.cd_agenda = b.cd_agenda
		and		a.nr_sequencia <> nr_seq_turno_p
		and		a.cd_agenda <> cd_agenda_p
		and		b.cd_tipo_agenda = 3
		and     ((to_date(to_char(a.HR_INICIAL,'hh24mi'),'hh24mi') >= TO_DATE(TO_CHAR(hr_inicial_p,'HH24:MI'),'HH24:MI')
		and     to_date(to_char(a.HR_INICIAL,'hh24mi'),'hh24mi') < TO_DATE(TO_CHAR(hr_final_p,'HH24:MI'),'HH24:MI')) or (
                to_date(to_char(a.HR_INICIAL,'hh24mi'),'hh24mi') < TO_DATE(TO_CHAR(hr_inicial_p,'HH24:MI'),'HH24:MI')
        and     to_date(to_char(a.HR_FINAL,'hh24mi'),'hh24mi') > TO_DATE(TO_CHAR(hr_inicial_p,'HH24:MI'),'HH24:MI'))
        )
		and     a.nr_seq_sala = nr_seq_sala_p
		order by a.DT_INICIO_VIGENCIA asc) loop

		dt_calc_vg_w := c.DT_INICIO_VIGENCIA;
		cont_w := 0;

		begin
		select	1
		into	cd_agenda_ocup_w
		from	agenda_turno
		where	nr_sequencia = c.nr_sequencia
		and		nr_seq_sala = nr_seq_sala_p
		and		((ie_semana = ie_semana_p
			or	 ie_semana_p = 0
			or	ie_semana = 0) and nvl(dt_inicio_vigencia, dt_inicial_p) = dt_inicial_p)
		and		(ie_dia_semana = ie_dia_semana_p
			or	(ie_dia_semana = 9 and ie_dia_semana_p between 2 and 6)
			or	(ie_dia_semana_p = 9 and ie_dia_semana between 2 and 6))
		and	rownum = 1;
		exception
		when no_data_found then
			cd_agenda_ocup_w := 0;
			
		end;

		if (cd_agenda_ocup_w > 0) then

			select	substr(nvl(obter_nome_pf(cd_pessoa_fisica),'"'||wheb_mensagem_pck.get_texto(795843)||'"'),1,100)
			into	nm_pessoa_w
			from	agenda
			where	cd_agenda	= c.cd_agenda;

			ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280407,'NM_PESSOA='|| nm_pessoa_w||';CD_AGENDA_OCUP='||c.cd_agenda);

			exit principal;
		end if;

		if (dt_calc_vg_w <= dt_inicial_p) then
			while (dt_calc_vg_w <= dt_inicial_p and (ds_erro_w = '' OR ds_erro_w is null)) loop

				if((dt_calc_vg_w = dt_inicial_p and cont_w > 0) or (c.DT_INICIO_VIGENCIA = dt_inicial_p)) then

					select	substr(nvl(obter_nome_pf(cd_pessoa_fisica),'"'||wheb_mensagem_pck.get_texto(795843)||'"'),1,100)
					into	nm_pessoa_w
					from	agenda
					where	cd_agenda	= c.cd_agenda;

					ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280407,'NM_PESSOA='|| nm_pessoa_w||';CD_AGENDA_OCUP='||c.cd_agenda);
					exit principal;
				end if;

				dt_calc_vg_w := dt_calc_vg_w + nr_dias_add_w;
				cont_w := cont_w +1;

			end loop;
		else
			while (dt_calc_vg_w >= dt_inicial_p and (ds_erro_w = '' OR ds_erro_w is null)) loop

				if((dt_calc_vg_w = dt_inicial_p and cont_w > 0) or (c.DT_INICIO_VIGENCIA = dt_inicial_p)) then

					select	substr(nvl(obter_nome_pf(cd_pessoa_fisica),'"'||wheb_mensagem_pck.get_texto(795843)||'"'),1,100)
					into	nm_pessoa_w
					from	agenda
					where	cd_agenda	= c.cd_agenda;

					ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280407,'NM_PESSOA='|| nm_pessoa_w||';CD_AGENDA_OCUP='||c.cd_agenda);
					exit principal;
				end if;

				dt_calc_vg_w := dt_calc_vg_w - nr_dias_add_w;
				cont_w := cont_w +1;

			end loop;
		end if;
	end loop principal;

end if;

if (ie_consiste_medico_w = 'S') then

		select	count(*)
		into	qt_agenda_turno_med_w
		from	agenda_turno a,
				agenda b
		where	a.cd_agenda = b.cd_agenda
		and		a.nr_sequencia <> nr_seq_turno_p
		and		b.cd_tipo_agenda = 3
		and		b.cd_pessoa_fisica = cd_medico_w
		and		a.ie_dia_semana   =  ie_dia_semana_p
		and		(ie_semana = ie_semana_p
			or	ie_semana_p = 0
			or	ie_semana = 0)
		and    	to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') >=
				to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
		and		to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_inicial_p, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss') <=
				to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || to_char(hr_final,   'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
		and		((nvl(dt_inicio_vigencia,sysdate) between nvl(dt_inicial_p,sysdate) and fim_dia(nvl(dt_final_p,sysdate))) or
				(nvl(dt_final_vigencia,sysdate) between nvl(dt_inicial_p,sysdate) and fim_dia(nvl(dt_final_p,sysdate))));
		
		if	(qt_agenda_turno_med_w > 0) then
			ds_erro_w	:= wheb_mensagem_pck.get_texto(280414,null);
		end if;

end if;

open	c02;
loop
fetch	c02 into
	qt_agenda_exame_w,
	ie_tipo_w,
	ds_agenda_w;
exit	when c02%notfound;
	begin

	ds_agenda_geral_w	:= ds_agenda_geral_w || ds_agenda_w || chr(13) || chr(10);

	qt_agenda_exame_tot_w	:= qt_agenda_exame_tot_w + qt_agenda_exame_w;

	end;
end loop;
close	c02;

if	(qt_agenda_exame_tot_w	> 0) and
	(nvl(ie_consistir_horario_w,'S') = 'S') then
	begin
	ds_erro_w	:= wheb_mensagem_pck.get_texto(280415,null) || chr(13) || chr(10) || ds_agenda_geral_w;
	end;
end if;

ds_erro_p	:= ds_erro_w;

end Consiste_Agenda_Turno_Cons;
/