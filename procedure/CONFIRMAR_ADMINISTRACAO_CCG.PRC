create or replace
procedure  confirmar_administracao_ccg(
						nr_seq_glicemia_p	number,
						nr_prescricao_p		number,
						nr_seq_horario_p	number,
						nm_usuario_p 	varchar2,
						ie_local_aplicacao_p	varchar2) is
				
nr_seq_evento_w				number(10,0);
ie_gera_adm_w				varchar2(1);
nr_sequencia_w				number(10);
nr_prescricao_w				number(14);
cd_procedimento_w			number(15);
ie_origem_proced_w			number(10);
qt_procedimento_w			number(8,3);
cd_setor_atendimento_w		number(5);
nr_seq_exame_w				number(10);
nr_seq_proc_interno_w		number(10);
nr_seq_interno_w			number(10);
nr_seq_horario_w			number(10);
dt_horario_w				date;
ie_cobrar_horario_w			varchar2(1);
ie_cobrar_horarioI_w		varchar2(1);
cont_w						number(15);
ie_cobra_adep_w				varchar2(1);
VarIe_Cobra_Prim_hor_w		varchar2(1);
ie_cobra_proc_hor_w			varchar2(1);
ie_ccg_confirmacao_w		varchar2(1);
ds_observacao_w				atend_glicemia_evento.ds_observacao%type := null;
qt_glicose_adm_w			atendimento_glicemia.qt_glicose_adm%type;
qt_ui_insulina_adm_w		atendimento_glicemia.qt_ui_insulina_adm%type;
qt_ui_insulina_int_adm_w	atendimento_glicemia.qt_ui_insulina_int_adm%type;
cd_material_w				material.cd_material%type;
nr_seq_material_w			prescr_material.nr_sequencia%type;
qt_glicemia_w				atendimento_glicemia.qt_glicemia%type;
nr_atendimento_w			atendimento_glicemia.nr_atendimento%type;
dt_glicemia_w				atendimento_glicemia.dt_controle%type;
qt_glicose_w				atendimento_glicemia.qt_glicose%type;
nr_seq_map_w				number(10);
qt_conta_w					number(9,3);
ie_cobra_adm_w				varchar2(1);
Ie_registrar_sv_w			Varchar2(1);
ie_tipo_item_w				varchar2(1); 
qt_glicose_adm_2w		    atendimento_glicemia.qt_glicose_adm%type;       
dt_fim_horario_w			date;
ie_evento_w					atend_glicemia_evento.ie_evento%type;

begin

obter_param_usuario(1113, 351, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, VarIe_Cobra_Prim_hor_w);
obter_param_usuario(1113, 360, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gera_adm_w);
obter_param_usuario(1113, 246, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_cobra_proc_hor_w);
obter_param_usuario(1113, 478, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, Ie_registrar_sv_w);


if	(nr_seq_glicemia_p is not null) and
	(nm_usuario_p is not null) then

	update	atendimento_glicemia
	set		ie_adm_confirmada		= 'S',
			nm_usuario				= nm_usuario_p,
			qt_ui_insulina_adm		= decode(ie_gera_adm_w, 'S', qt_ui_insulina_adm, nvl(qt_ui_insulina_adm,qt_ui_insulina_calc)),
			qt_ui_insulina_int_adm	= decode(ie_gera_adm_w, 'S', qt_ui_insulina_int_adm, nvl(qt_ui_insulina_int_adm,qt_ui_insulina_int_calc)),		
			qt_glicose_adm			= decode(ie_gera_adm_w, 'S', qt_glicose_adm, nvl(qt_glicose_adm,qt_glicose))
	where	nr_sequencia			= nr_seq_glicemia_p;
				
	select 	nvl(qt_ui_insulina_adm,0),
			nvl(qt_ui_insulina_calc,0),
			nvl(qt_glicose,0),
			nvl(qt_glicose_adm,0)
	into	qt_ui_insulina_adm_w,
			qt_ui_insulina_int_adm_w,
			qt_glicose_w,
			qt_glicose_adm_w
	from 	atendimento_glicemia
	where 	nr_sequencia = nr_seq_glicemia_p;

	if ( qt_ui_insulina_int_adm_w 	= 0 and
		 qt_glicose_w 				= 0 and
		 qt_glicose_adm_w 			= 0 and
		 qt_ui_insulina_adm_w 		= 0 ) then
		ie_evento_w := 14;
	else
		ie_evento_w := 10;
	end if;
	
	ds_observacao_w := nvl(substr(obter_valor_dominio(6166,ie_local_aplicacao_p),1,50),'');
	
	select	atend_glicemia_evento_seq.nextval
	into	nr_seq_evento_w
	from	dual;
	insert into atend_glicemia_evento (
		nr_sequencia,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,
		nr_seq_glicemia,
		ie_evento,
		dt_evento,
		cd_pessoa_evento,
		ds_observacao,
		ie_local_aplicacao)
	values (
		nr_seq_evento_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_glicemia_p,
		ie_evento_w,
		sysdate,
		substr(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10),
		ds_observacao_w,
		ie_local_aplicacao_p);
		
	if	(SQL%rowcount > 0) then
		Gerar_taxa_mat_conta(nr_prescricao_p,null,nm_usuario_p,'G','G');
		
		select	nvl(max(a.qt_glicose_adm),0),
			nvl(max(a.qt_ui_insulina_adm),0),
			max(e.nr_sequencia),
			max(e.nr_prescricao),
			nvl(max(d.ie_cobra_adm),'N'),
			nvl(max(a.qt_ui_insulina_int_adm),0)
		into	qt_glicose_adm_w,
			qt_ui_insulina_adm_w,
			nr_seq_material_w,
			nr_prescricao_w,
			ie_cobra_adm_w,
			qt_ui_insulina_int_adm_w				
		from	prescr_material e,
			pep_protocolo_glicemia d,
			protocolo_glic_mat_med c,
			atend_glicemia b,
			atendimento_glicemia a
		where	a.nr_sequencia		= nr_seq_glicemia_p
		and	a.nr_seq_glicemia	= b.nr_sequencia
		and	b.nr_seq_prot_glic	= d.nr_sequencia
		and	d.nr_sequencia		= c.nr_seq_protocolo_glic
		and	b.nr_seq_prot_glic	= c.nr_seq_protocolo_glic
		and	b.nr_prescricao 	= e.nr_prescricao
		and	e.cd_material		= c.cd_material
		and	e.ie_glicemia		= c.ie_glicemia;
		
		if	(nr_seq_material_w	is not null) and
			(ie_cobra_adm_w	= 'S') then
			
			if	(qt_glicose_adm_w	> 0) then
				qt_conta_w	:= qt_glicose_adm_w;
				ie_tipo_item_w	:= 'G';
			elsif	(qt_ui_insulina_adm_w	> 0) then
				qt_conta_w	:= qt_ui_insulina_adm_w;
				ie_tipo_item_w	:= 'I';
			elsif	(qt_ui_insulina_int_adm_w	> 0) then
				qt_conta_w	:= qt_ui_insulina_int_adm_w;
				ie_tipo_item_w	:= 'I';
			end if;
			
			if	(ie_tipo_item_w = 'I') then			
				select	max(e.nr_sequencia)							
				into	nr_seq_material_w			
				from	prescr_material e,
					pep_protocolo_glicemia d,
					protocolo_glic_mat_med c,
					atend_glicemia b,
					atendimento_glicemia a
				where	a.nr_sequencia		= nr_seq_glicemia_p
				and	a.nr_seq_glicemia	= b.nr_sequencia
				and	b.nr_seq_prot_glic	= d.nr_sequencia
				and	d.nr_sequencia		= c.nr_seq_protocolo_glic
				and	b.nr_seq_prot_glic	= c.nr_seq_protocolo_glic
				and	b.nr_prescricao 	= e.nr_prescricao
				and	e.cd_material		= c.cd_material
				and	e.ie_glicemia		= c.ie_glicemia
				and	c.ie_glicemia		= 'N';			
			
			else			
				select	max(e.nr_sequencia)							
				into	nr_seq_material_w			
				from	prescr_material e,
					pep_protocolo_glicemia d,
					protocolo_glic_mat_med c,
					atend_glicemia b,
					atendimento_glicemia a
				where	a.nr_sequencia		= nr_seq_glicemia_p
				and	a.nr_seq_glicemia	= b.nr_sequencia
				and	b.nr_seq_prot_glic	= d.nr_sequencia
				and	d.nr_sequencia		= c.nr_seq_protocolo_glic
				and	b.nr_seq_prot_glic	= c.nr_seq_protocolo_glic
				and	b.nr_prescricao 	= e.nr_prescricao
				and	e.cd_material		= c.cd_material
				and	e.ie_glicemia		= c.ie_glicemia
				and	c.ie_glicemia		= 'S';
			end if;			
			
			select	max(nr_sequencia),
					nvl(max(dt_fim_horario),sysdate)
			into	nr_seq_horario_w,
					dt_fim_horario_w
			from	prescr_mat_hor
			where	nr_prescricao = nr_prescricao_w
			and		nr_seq_material	= nr_seq_material_w
			and		nvl(ie_horario_especial,'N')	<> 'S';
			
			if	(nr_seq_horario_p is not null) then
			
			    select 	nvl(max(dt_alteracao),sysdate) 
				into	dt_fim_horario_w
				from  	prescr_mat_alteracao
				where  	nr_seq_horario_proc = nr_seq_horario_p
				and    	ie_alteracao = 3
				and    	nvl(ie_evento_valido,'S') = 'S'
				and    	dt_alteracao is not null;
			
			end if;
			
			if	(nvl(qt_conta_w,0)	> 0) and
				(nr_seq_horario_w	is not null) then
				gerar_estornar_adep_map(NULL, nr_seq_horario_w, NULL, 'G', dt_fim_horario_w, nm_usuario_p, nr_seq_map_w,null,qt_conta_w,null);
			end if;	
		end if;
		
	end if;	

	if  (Ie_registrar_sv_w = 'C') then
		select 	nr_atendimento, 
			qt_glicemia, 
			decode(ie_gera_adm_w, 'S', qt_ui_insulina_adm, qt_ui_insulina_calc), 
			dt_controle  
		into    nr_atendimento_w,
			qt_glicemia_w,
			qt_ui_insulina_adm_w,
			dt_glicemia_w
		from atendimento_glicemia 
		where	nr_sequencia = nr_seq_glicemia_p;
		
		if	(qt_glicose_adm_w  > 0) then
			qt_glicose_adm_2w :=  qt_glicose_adm_w;
		else
			qt_glicose_adm_2w := null;
		end if;
		

		adep_gerar_SV_Glicemia(nr_atendimento_w, qt_glicemia_w, nm_usuario_p, qt_ui_insulina_adm_w, dt_glicemia_w, null, null, null, qt_glicose_adm_2w);
	end if;

end if;

commit;

end confirmar_administracao_ccg;
/
