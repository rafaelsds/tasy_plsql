create or replace procedure CANCELAR_NF
  (nr_sequencia_p in number,
  nm_usuario_p  in varchar2)  is

nr_bordero_w number(10);

begin
if ( nvl(nr_sequencia_p,0) <> 0) then

  begin
  select  max(nr_bordero)
  into    nr_bordero_w
  from    bordero_recebimento
  where   nr_seq_nota_fiscal = nr_sequencia_p;

  exception when others then
    nr_bordero_w := null;
  end;

  if  (nr_bordero_w is not null)  then
      wheb_mensagem_pck.exibir_mensagem_abort(1085688,'NR_SEQ_BORDERO=' || nr_bordero_w);
  else
      update nota_fiscal
      set ie_situacao   = 9,
          nr_seq_protocolo  = null,
          nr_interno_conta = null,
          nr_seq_exportada = null,
          ie_status_envio  = null,
          nm_usuario  = nm_usuario_p,
          dt_atualizacao  = sysdate,
          dt_cancelamento  = sysdate,
          nm_usuario_cancel = nm_usuario_p
      where nr_sequencia   = nr_sequencia_p;

      Gerar_Titulo_Pagar_NF (nr_sequencia_p, nm_usuario_p);

      gerar_historico_nota_fiscal(nr_sequencia_p, nm_usuario_p, '1', wheb_mensagem_pck.get_texto(312554));

  end if;


end if;
commit;

end;
/