create or replace
procedure  RXT_GRAVA_LOG_LIBERACAO_FISIC(
            nr_seq_tratamento_p     number,
            nr_seq_trat_braq_p      number,
            ds_motivo_reprovacao_p  varchar2,
            ie_tipo_log_p           varchar2
    ) is
begin
	if(nr_seq_tratamento_p is not null) then
		insert 
		into rxt_log_liberacao_fisico
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_tratamento,
			ds_motivo_reprovacao,
			ie_tipo_log)
		values(
			rxt_log_liberacao_fisico_seq.nextval,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			nr_seq_tratamento_p,
			ds_motivo_reprovacao_p,
			ie_tipo_log_p
			);
	end if;
	if(nr_seq_tratamento_p is not null) then
		insert 
		into rxt_log_liberacao_fisico
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_trat_braq,
			ds_motivo_reprovacao,
			ie_tipo_log)
		values(
			rxt_log_liberacao_fisico_seq.nextval,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			nr_seq_trat_braq_p,
			ds_motivo_reprovacao_p,
			ie_tipo_log_p
			);
	end if;
end  RXT_GRAVA_LOG_LIBERACAO_FISIC;
/