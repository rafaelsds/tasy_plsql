create or replace
procedure pat_enviar_comunic_doc_aprov(	cd_estabelecimento_p	number,
					nr_seq_doc_transf_p	number,
					nm_usuario_dest_p	varchar2,
					ie_opcao_p		varchar2,
					nm_usuario_p		varchar2) is 

cd_perfil_adicional_w			varchar2(255);
cd_pessoa_aprov_w			varchar2(10);
nm_pessoa_aprov_w			varchar2(255);
ds_titulo_w				varchar2(255);
ds_comunicado_w				varchar2(4000);
ds_local_w				varchar2(255);

begin

/*
TP -	comunicado de documento de transferencia pendente para aprova��o.
TA - 	comunicado de documento de transfer�ncia aprovado.
*/

ds_titulo_w			:= '';
ds_comunicado_w			:= '';


select	max(substr(pat_obter_dados_local(nr_seq_local, 'D'), 1, 255))
into	ds_local_w
from	pat_doc_transferencia
where	nr_sequencia		= nr_seq_doc_transf_p;

/*Pegar o usu�rio respons�vel pela aprova��o*/
cd_pessoa_aprov_w		:= substr(obter_pessoa_fisica_usuario(nm_usuario_p, 'C'), 1, 10);
nm_pessoa_aprov_w		:= substr(obter_pessoa_fisica_usuario(nm_usuario_p, 'N'), 1, 255);

if	(ie_opcao_p = 'TP') then

	ds_titulo_w		:= substr(wheb_mensagem_pck.get_texto(299313),1,255);
	ds_comunicado_w		:= substr(wheb_mensagem_pck.get_texto(299322,	'NR_SEQ_DOC_TRANSF=' || nr_seq_doc_transf_p || ';DS_LOCAL=' || ds_local_w || 
									';CD_PESSOA_APROV=' || cd_pessoa_aprov_w || ';NM_PESSOA_APROV=' || nm_pessoa_aprov_w),1,4000);
elsif	(ie_opcao_p = 'TA') then
	
	ds_titulo_w		:= substr(wheb_mensagem_pck.get_texto(299338),1,255);
	ds_comunicado_w		:= substr(wheb_mensagem_pck.get_texto(299344,	'NR_SEQ_DOC_TRANSF=' || nr_seq_doc_transf_p || ';DS_LOCAL=' || ds_local_w || 
									';CD_PESSOA_APROV=' || cd_pessoa_aprov_w || ';NM_PESSOA_APROV=' || nm_pessoa_aprov_w),1,4000);
end if;

if	(nvl(ds_titulo_w, 'X') <> 'X') then
	
	gerar_comunic_padrao(	sysdate,
				ds_titulo_w,
				ds_comunicado_w,
				nm_usuario_p,
				null,
				nm_usuario_dest_p,
				'N',
				null,
				null,
				cd_estabelecimento_p,
				null,
				sysdate,
				null,
				null);
end if;


commit;

end pat_enviar_comunic_doc_aprov;
/