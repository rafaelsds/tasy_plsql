create or replace
procedure altera_classif_titulo_pagar(	nr_titulo_p		number,
					nr_seq_classe_p		varchar2) is 
					
nr_seq_trans_fin_baixa_w	number(10);
nr_seq_trans_fin_contab_w	number(10);
atualiza_trans_financ_w		varchar(1);
count_w				number(10);

begin

obter_param_usuario(851, 206,  obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, atualiza_trans_financ_w);

if	(nr_seq_classe_p is not null) then
	update	titulo_pagar
	set	nr_seq_classe = nr_seq_classe_p
	where 	nr_titulo = nr_titulo_p;
end if;

if	(atualiza_trans_financ_w = 'S') then
	select	count(*)
	into	count_w
	from 	classe_titulo_pagar
	where	nr_sequencia = nr_seq_classe_p;
	
	if	(count_w > 0) then
		select	nr_seq_trans_fin_baixa,
			nr_seq_trans_fin_contab
		into	nr_seq_trans_fin_baixa_w,
			nr_seq_trans_fin_contab_w
		from 	classe_titulo_pagar
		where	nr_sequencia = nr_seq_classe_p;
	end if;

	update	titulo_pagar
	set	nr_seq_trans_fin_baixa = nr_seq_trans_fin_baixa_w,
		nr_seq_trans_fin_contab = nr_seq_trans_fin_contab_w
	where	nr_titulo = nr_titulo_p;
end if;

commit;

end altera_classif_titulo_pagar;
/

