create or replace
procedure Atualiza_Teste_Soft_Exec(
			nm_usuario_p		Varchar2,
			nr_seq_prev_versao_p	number,
			cd_versao_p		Varchar2,
			nr_seq_exec_teste_p     number) is 
			
begin
update	teste_software_execucao                    
set      	nr_seq_prev_versao   = nr_seq_prev_versao_p,  
	cd_versao            = cd_versao_p,           
	dt_atualizacao       = sysdate,              
	nm_usuario           = nm_usuario_p           
where    	nr_sequencia         = nr_seq_exec_teste_p;

commit;

end Atualiza_Teste_Soft_Exec;
/