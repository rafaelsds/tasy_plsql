create or replace
procedure	change_date_external_physician(	cd_medico_p	in pf_medico_externo.cd_medico%type)	is

begin

if (cd_medico_p is not null) then
	
	for item in (	select  pme.nr_sequencia
			from    pf_medico_externo pme
			where   ((pme.dt_inicio_vigencia <= sysdate or pme.dt_inicio_vigencia is null) and (  pme.dt_fim_vigencia is null or pme.dt_fim_vigencia >= sysdate))
			and     pme.cd_medico = cd_medico_p) loop
			
		update	pf_medico_externo
		set	dt_fim_vigencia = sysdate
		where	nr_sequencia = item.nr_sequencia;
		
	end loop;
	
	commit;
	
end if;

end;
/