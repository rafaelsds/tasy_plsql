create or replace
procedure fleury_ws_atualiz_lab_result_i(		nr_ficha_p		number,
						nr_prescricao_p		number,
						nr_seq_prescr_p		number,
						cd_exame_p		varchar2,
						cd_analito_p		number,
						ds_resultado_p		varchar2,
						ds_data_coleta_p		varchar2,
						ds_referencia_p		varchar2,
						ds_unidade_medida_p	varchar2,
						cd_unidade_p		varchar2,
						ds_analito_p		varchar2,
						ie_resultado_referencia_p varchar2,
						ie_resultado_critico_p varchar2,
						ds_regra_result_critico_p varchar2,
						nm_usuario_p		varchar2,
						ds_erro_p	out		varchar2) is

dt_coleta_w		date;
cd_exame_w		varchar2(20);
nr_prescricao_w		number(10);
nr_seq_exame_w		number(10);
nr_seq_exame_analito_w	number(10);
nr_seq_grupo_w		number(10);
nr_seq_prescr_w		number(10);
ie_agrupa_w		varchar2(1);
ie_gera_analito_w	varchar2(1);
cd_estabelecimento_w	number(10);
ie_exige_formato_w	lab_parametro.ie_exige_formato%type;
nr_seq_resultado_w	exame_lab_resultado.nr_seq_resultado%type;
nr_seq_resultado_ww	exame_lab_resultado.nr_seq_resultado%type;
qt_result_item_w	number(10);
nr_regras_atendidas_w   varchar2(2000);

ie_existe_param_maq_w	varchar2(1);
ie_sepse_lib_exame_w	varchar2(1);
nr_atendimento_w		number(10);
cd_medico_prescr_w		varchar2(10);

begin

gerar_lab_log_interf_imp(nr_prescricao_p,
		null,
		null,
		null,
		substr('fleury_ws_atualiz_lab_result_i - cd_exame_p: '||cd_exame_p||'  cd_analito_p:'||cd_analito_p||' nr_seq_prescr_p: '||nr_seq_prescr_p||' nr_ficha_p: '||nr_ficha_p||' ds_resultado_p: '||ds_resultado_p,1,1999),
		'FleuryWS',
		'',
		nm_usuario_p,
		'N');

/*select	nvl(max(ie_agrupa_ficha_fleury),'N'),
	nvl(max(ie_gera_analito_fleury),'S'),
	max(cd_estabelecimento)
into	ie_agrupa_w,
	ie_gera_analito_w,
	cd_estabelecimento_w
from	lab_parametro
where	nvl(cd_unidade_fleury,nvl(cd_unidade_p,'0')) = nvl(cd_unidade_p,'0');*/

select	fleury_obter_dados_unidade(cd_unidade_p, 'AF'),
		fleury_obter_dados_unidade(cd_unidade_p, 'GA'),
		fleury_obter_dados_unidade(cd_unidade_p, 'E')
into	ie_agrupa_w,
		ie_gera_analito_w,
		cd_estabelecimento_w
from	dual;

if	((nr_prescricao_p is not null) and (nr_prescricao_p > 0)) then

	nr_prescricao_w := nr_prescricao_p;

else
	
	if not(ie_agrupa_w = 'N') then
	
		select	decode(count(*),0,'N','S')
		into	ie_existe_param_maq_w
		from	lab_param_maquina a
		where	a.cd_estabelecimento = cd_estabelecimento_w;
	
		if	(ie_existe_param_maq_w = 'S') then
			select	max(a.nr_prescricao)
			into	nr_prescricao_w
			from	(
					select	a.nr_prescricao,
						substr(lab_obter_parametro(cd_estabelecimento_w, a.nr_prescricao, null, 'UF'),1,255) cd_unidade_fleury
					from	prescr_procedimento a
					where	a.nr_controle_ext = nr_ficha_p
					) a
			where	a.cd_unidade_fleury =  cd_unidade_p;
		else
			select	max(a.nr_prescricao)
			into	nr_prescricao_w
			from	prescr_procedimento a,
					prescr_medica b
			where	a.nr_prescricao = b.nr_prescricao
			and		a.nr_controle_ext = nr_ficha_p
			and		b.cd_estabelecimento = cd_estabelecimento_w;
		end if;
	else
		select	max(a.nr_prescricao)
		into	nr_prescricao_w
		from	prescr_medica a
		where	a.nr_controle =  nr_ficha_p
		and		a.cd_estabelecimento = cd_estabelecimento_w;
	end if;
	
end if;

if	(nr_prescricao_w is null) then
	select	max(a.nr_prescricao)
	into	nr_prescricao_w
	from	prescr_medica a,
			prescr_procedimento b
	where	a.nr_prescricao = b.nr_prescricao
	and		b.nr_seq_exame is not null
	and		a.cd_estabelecimento = cd_estabelecimento_w
	and		nvl(a.nr_controle,b.nr_controle_ext) = nr_ficha_p;
end if;

select	max(nr_atendimento),
		max(cd_medico)
into	nr_atendimento_w,
		cd_medico_prescr_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_w;

if	(nr_seq_prescr_p is null) or
	(not nr_seq_prescr_p > 0) then
	
	nr_seq_prescr_w := obter_seq_prescr_prescricao(nr_prescricao_w, 'FLEURY', cd_exame_p);
		
else
	nr_seq_prescr_w := nr_seq_prescr_p;
	
end if;	

select	max(nvl(e.cd_exame_integracao,e.cd_exame)),
	max(e.nr_seq_exame),
	max(e.nr_seq_grupo)
into	cd_exame_w,
	nr_seq_exame_w,
	nr_seq_grupo_w
from	prescr_procedimento p,
	exame_laboratorio e
where	p.nr_prescricao	= nr_prescricao_w
and	p.nr_sequencia	= nr_seq_prescr_w
and	p.nr_seq_exame 	= e.nr_seq_exame;

dt_coleta_w  := to_date(substr(ds_data_coleta_p ,1,10),'dd/mm/yyyy');

if	(ie_gera_analito_w = 'S') and
	(nr_seq_exame_w is not null) then

	--  **  Procura se o analito enviado existe cadastrado na base  ** --
	select	nvl(max(nr_seq_exame),0)
	into	nr_seq_exame_analito_w
	from	exame_laboratorio
	where	nvl(cd_exame_integracao, cd_exame) = to_char(cd_analito_p)
	start 	with nr_seq_superior = nr_seq_exame_w
	connect by prior nr_seq_exame = nr_seq_superior;

	if	(nr_seq_exame_analito_w <= 0) then

		select	(max(nr_seq_exame) +1)
		into	nr_seq_exame_analito_w
		from	exame_laboratorio;
		
		
		-- ** Caso o analito n�o exista, � cadastrado um novo com o c�digo recebido ** -- 
		insert	into exame_laboratorio
			(
			nr_seq_exame,
			cd_exame,
			cd_exame_integracao,
			nm_exame,
			dt_atualizacao,
			nm_usuario,
			ie_campo_calculo,
			ie_restricao,
			ie_prescricao_rotina,
			nr_seq_superior,
			nr_seq_grupo,
			ie_solicitacao,
			ie_formato_resultado,
			ie_situacao,
			cd_cgc_externo,
			nr_seq_apresent
			)
		values
			(
			nr_seq_exame_analito_w,
			to_char(nr_seq_exame_analito_w),
			cd_analito_p,
			nvl(ds_analito_p,to_char(cd_analito_p)),
			sysdate,
			nm_usuario_p,
			'N',
			'N',
			'S',
			nr_seq_exame_w,
			nr_seq_grupo_w,
			'N',
			'DV',
			'A',
			'60840055000131',
			999999
			);
			
		commit;	
			
	end if;
end if;


select	count(1)
into	qt_result_item_w
from	exame_lab_resultado a
where 	a.nr_prescricao = nr_prescricao_w;

if	(qt_result_item_w = 0) then

	select	nvl(max(ie_exige_formato),'N')
	into	ie_exige_formato_w
	from	lab_parametro
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(ie_exige_formato_w <> 'S') then
	
		select	nvl(max(nr_seq_resultado),0) + 1
		into	nr_seq_resultado_w
		from	exame_lab_resultado;
		
		insert 	into exame_lab_resultado
				(
				nr_seq_resultado, 
				dt_resultado, 
				dt_atualizacao, 
				nm_usuario, 
				nr_prescricao,
				nr_atendimento
				)
		values	
				(	
				nr_seq_resultado_w, 
				sysdate, 
				sysdate, 
				nvl(nm_usuario_p,'Interface'), 
				nr_prescricao_w,
				nr_atendimento_w
				);
			
		commit;
	
	end if;

end if;


select	nvl(max(nr_seq_exame),0)
into	nr_seq_exame_analito_w
from	exame_laboratorio
where	nvl(cd_exame_integracao, cd_exame) = to_char(cd_analito_p)
start 	with nr_seq_superior = nr_seq_exame_w
connect by prior nr_seq_exame = nr_seq_superior;


lab_atualizar_result_item
			(
			nr_prescricao_w,
			nr_seq_prescr_w,
			cd_exame_w,
			to_char(cd_analito_p),
			null,
			null,
			ds_resultado_p,
			null,
			null,
			null,
			'FLEURYWS',
			dt_coleta_w,
			ds_referencia_p,
			ds_unidade_medida_p,
			null,
			null,
			ds_erro_p,
			nr_seq_exame_analito_w,
			ie_resultado_referencia_p,
			ie_resultado_critico_p,
			ds_regra_result_critico_p 
			);

			
select max (nr_seq_resultado)
into  nr_seq_resultado_ww
from  exame_lab_resultado a,
	  prescr_procedimento b
where a.nr_prescricao = b.nr_prescricao
and   a.nr_prescricao = nr_prescricao_w		
and	  b.nr_sequencia = nr_seq_prescr_w
and	  b.nr_seq_exame in (select nr_seq_exame from sepse_atributo_regra)
and b.ie_status_atend >= 35;


if (nr_seq_resultado_ww is not null) then

	GQA_Aprov_Exame_result_item(nr_seq_resultado_ww, nr_seq_prescr_w, nm_usuario_p, nr_regras_atendidas_w);
	
	
	select	nvl(max(ie_sepse_lib_exame),'N')
	into	ie_sepse_lib_exame_w
	from	parametro_medico
	where	cd_estabelecimento = cd_estabelecimento_w;

	if	(ie_sepse_lib_exame_w = 'S') then
		begin
			gerar_escala_sepse(nr_atendimento_w,cd_medico_prescr_w,0,nm_usuario_p);
		exception
		when others then
			null;	
		end;
	end if;

end if;
	
end fleury_ws_atualiz_lab_result_i;
/