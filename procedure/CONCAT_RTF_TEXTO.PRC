create or replace
procedure concat_rtf_texto(nr_origem_p  integer,
                          nr_apendice_p integer) is

ds_texto1_w	    varchar2(32767):= '';
ds_texto2_w	    varchar2(32767):= '';
ds_aux_w	    varchar2(32767);
ds_erro_w       varchar2(4000);
nr_seq_conversao_w	number(10);  

begin
  begin
    select ds_comentario  
    into   ds_texto1_w
    from   pe_prescr_comentario 
    where  NR_SEQUENCIA = nr_origem_p;
    
    select ds_comentario  
    into   ds_texto2_w
    from   pe_prescr_comentario 
    where  NR_SEQUENCIA = nr_apendice_p;
    
    --Remove �ltimo }
    ds_texto1_w := regexp_replace(ds_texto1_w, '}', '', instr(ds_texto1_w ,' ',-1));
  
    --Remove cabe�alho do RTF para anexar no RTF antigo
    if regexp_like(ds_texto2_w, '\par') then 
      ds_texto2_w := regexp_replace(ds_texto2_w,  '^.*', chr(13)||chr(13), 1);
    end if;
    
    update pe_prescr_comentario
    set    ds_comentario = ds_texto1_w || ds_texto2_w
    where  NR_SEQUENCIA = nr_origem_p;
    
  exception
  when others then
      begin
      converte_rtf_string( 'select ds_comentario from pe_prescr_comentario where nr_sequencia = :nr_sequencia_p', nr_origem_p, 'Tasy', nr_seq_conversao_w);
      
      select ds_texto
      into	 ds_texto1_w
      from	 tasy_conversao_rtf
      where	 nr_sequencia = nr_seq_conversao_w;  
      
      converte_rtf_string( 'select ds_comentario from pe_prescr_comentario where nr_sequencia = :nr_sequencia_p', nr_apendice_p, 'Tasy', nr_seq_conversao_w);
      
      select ds_texto
      into	 ds_texto2_w
      from	 tasy_conversao_rtf
      where	 nr_sequencia = nr_seq_conversao_w;    
      
      update pe_prescr_comentario
      set    ds_comentario = ds_texto1_w || ds_texto2_w
      where  NR_SEQUENCIA = nr_origem_p;
      
      exception
      when others then
        ds_erro_w	:= sqlerrm(sqlcode);
        update pe_prescr_comentario
        set    ds_comentario = ds_erro_w
        where  NR_SEQUENCIA = nr_origem_p;    
      end;
    
  end;    

end concat_rtf_texto;
/