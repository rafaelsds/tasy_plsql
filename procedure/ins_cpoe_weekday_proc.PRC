create or replace procedure ins_cpoe_weekday_proc(nr_seq_cpoe_order_unit_p 	number,
								ds_seq_proc_list_p			varchar2,
								nm_usuario_p				varchar2) is 

qt_count_w				number(10,0);
nm_tabela_w				VARCHAR2(255);

begin

select 		count(1)
into		qt_count_w
from 		cpoe_weekday_proc a
where 		nr_seq_cpoe_order_unit = nr_seq_cpoe_order_unit_p
and 		nm_usuario = nm_usuario_p;

if (qt_count_w = 0) then	

insert into cpoe_weekday_proc(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec, nr_seq_cpoe_order_unit,ie_domingo,ie_segunda,ie_terca,ie_quarta ,ie_quinta,ie_sexta,ie_sabado) values( cpoe_weekday_proc_seq.nextval,sysdate, nm_usuario_p, sysdate,nm_usuario_p, nr_seq_cpoe_order_unit_p ,'S', 'N', 'N', 'N', 'N', 'N', 'N');

insert into cpoe_weekday_proc(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec, nr_seq_cpoe_order_unit,ie_domingo,ie_segunda,ie_terca,ie_quarta ,ie_quinta,ie_sexta,ie_sabado) values( cpoe_weekday_proc_seq.nextval,sysdate, nm_usuario_p, sysdate,nm_usuario_p, nr_seq_cpoe_order_unit_p ,'N', 'S', 'N', 'N', 'N', 'N', 'N');

insert into cpoe_weekday_proc(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec, nr_seq_cpoe_order_unit,ie_domingo,ie_segunda,ie_terca,ie_quarta ,ie_quinta,ie_sexta,ie_sabado) values( cpoe_weekday_proc_seq.nextval,sysdate, nm_usuario_p, sysdate,nm_usuario_p, nr_seq_cpoe_order_unit_p ,'N', 'N', 'S', 'N', 'N', 'N', 'N');

insert into cpoe_weekday_proc(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec, nr_seq_cpoe_order_unit,ie_domingo,ie_segunda,ie_terca,ie_quarta ,ie_quinta,ie_sexta,ie_sabado) values( cpoe_weekday_proc_seq.nextval,sysdate, nm_usuario_p, sysdate,nm_usuario_p, nr_seq_cpoe_order_unit_p ,'N', 'N', 'N', 'S', 'N', 'N', 'N');

insert into cpoe_weekday_proc(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec, nr_seq_cpoe_order_unit,ie_domingo,ie_segunda,ie_terca,ie_quarta ,ie_quinta,ie_sexta,ie_sabado) values( cpoe_weekday_proc_seq.nextval,sysdate, nm_usuario_p, sysdate,nm_usuario_p, nr_seq_cpoe_order_unit_p ,'N', 'N', 'N', 'N', 'S', 'N', 'N');

insert into cpoe_weekday_proc(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec, nr_seq_cpoe_order_unit,ie_domingo,ie_segunda,ie_terca,ie_quarta ,ie_quinta,ie_sexta,ie_sabado) values( cpoe_weekday_proc_seq.nextval,sysdate, nm_usuario_p, sysdate,nm_usuario_p, nr_seq_cpoe_order_unit_p ,'N', 'N', 'N', 'N', 'N', 'S', 'N');

insert into cpoe_weekday_proc(nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec, nr_seq_cpoe_order_unit,ie_domingo,ie_segunda,ie_terca,ie_quarta ,ie_quinta,ie_sexta,ie_sabado) values( cpoe_weekday_proc_seq.nextval,sysdate, nm_usuario_p, sysdate,nm_usuario_p, nr_seq_cpoe_order_unit_p ,'N', 'N', 'N', 'N', 'N', 'N', 'S');


end if;

for rec_w in (select 		a.nr_sequencia,
			b.nr_registro
from 		cpoe_weekday_proc a,
			table(lista_pck.obter_lista(ds_seq_proc_list_p,',')) b
where 		nr_seq_cpoe_order_unit = nr_seq_cpoe_order_unit_p
and 		nm_usuario = nm_usuario_p
and 		not exists (select  1 
						from	cpoe_weekday_proc_item c
						where	c.nr_seq_proc_interno = b.nr_registro
						and		a.nr_sequencia = c.nr_seq_cpoe_weekday_proc)			
order by	nr_sequencia) loop 
	insert into cpoe_weekday_proc_item(	nr_sequencia,        
										dt_atualizacao,         
										nm_usuario,     
										dt_atualizacao_nrec,
										nm_usuario_nrec, 
										nr_seq_cpoe_weekday_proc,
										nr_seq_proc_interno,
										ie_selected)          
								values(	cpoe_weekday_proc_item_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										rec_w.nr_sequencia,
										rec_w.nr_registro,
										'S');

end loop;

delete from cpoe_weekday_proc_item
where	nr_sequencia in(select distinct b.nr_sequencia
from	cpoe_weekday_proc a,
		cpoe_weekday_proc_item b
where	a.nr_sequencia = b.NR_SEQ_CPOE_WEEKDAY_PROC
and		a.nr_seq_cpoe_order_unit = nr_seq_cpoe_order_unit_p
and		Obter_Se_Contido(b.nr_seq_proc_interno, ds_seq_proc_list_p) != 'S');

commit;


end ins_cpoe_weekday_proc;
/
