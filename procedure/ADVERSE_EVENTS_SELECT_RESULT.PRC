CREATE OR REPLACE
PROCEDURE Adverse_Events_Select_Result(
        nr_seq_registro_p                   number,
        cd_grade_p                          number,
        nr_seq_result_out_p         out     number) IS

nr_seq_result_w         number(10);
qt_result_w			    number(10);
ds_descricao_cliente_w	varchar2(255);

BEGIN

select  ds_descricao_cliente
into    ds_descricao_cliente_w
from    eventos_adversos_grade
where   cd_grade = cd_grade_p;

select  count(*)
into    qt_result_w
from    eventos_adversos_termos termos,
        eventos_adversos_grade grade
where   termos.cd_termo = grade.cd_termo
and     grade.cd_grade = cd_grade_p
and     termos.cd_termo in (
                                    select  grade2.cd_termo
                                    from    eventos_adversos_result result,
                                            eventos_adversos_grade grade2
                                    where   result.nr_seq_registro = nr_seq_registro_p
                                    and     result.nr_resultado = grade2.cd_grade
                           );

if (qt_result_w > 0) then
    exibir_erro_abortar(Wheb_mensagem_pck.get_texto(387890)/*'Esse item permite a selecao de apenas um resultado! #@#@'*/,null);    
else
	insert into eventos_adversos_result(
        nr_sequencia,
        nr_seq_registro,
        dt_atualizacao,
        ds_resultado,
        nr_resultado,
        dt_resultado,
        nm_usuario,
        ie_situacao)
    
        select eventos_adversos_result_seq.nextval,
        nr_seq_registro_p,
		    sysdate,
        ds_descricao_cliente_w,
        cd_grade_p,
        sysdate,
        wheb_usuario_pck.get_nm_usuario,
        'A'
	from	dual
	where not exists(select 1
                     from   eventos_adversos_result result
                     where	nr_seq_registro = nr_seq_registro_p
                     and	nr_resultado = cd_grade_p
                    );
end if;

select	nvl(max(nr_sequencia),0)
into	nr_seq_result_w
from	eventos_adversos_result
where	nr_seq_registro = nr_seq_registro_p;

nr_seq_result_out_p := nr_seq_result_w;

commit;

END Adverse_Events_Select_Result;
/
