create or replace
procedure Carrega_medicamentos_manip(	nr_seq_ordem_p 			number,
										nm_usuario_p			varchar2,
										cd_estabelecimento_p 	number) is

cd_material_w				number(10);
cd_material_prescrito_w		number(10);
nr_sequencia_w		number(10);
nr_sequencia_ww		number(10);
qt_estoque_w	 	number;
qt_estoque_ww	 	number;
ds_lote_fornec_w	varchar2(20);
cd_unidade_medida_consumo_w varchar2(30);
cd_unid_med_prescr_w	varchar2(30);
nr_seq_cabine_w		number(10);
nr_sequencia_www	number(10);
qt_dose_w		number;
qt_dose_ml_w		number;
qt_dose_ml_saldo_w	number;
qt_dose_ml_prescr_w	number;
qt_estoque_ml_w		number;
qt_dose_mg_ww		number;
nr_seq_lote_fornec_w	number(10);
nr_seq_ordem_w		number(10);
nr_seq_ordem_item_w	number(10);
qt_dose_real_w		number;
qt_total_cabine_ml_w	number;
qt_dose_util_cons_w	number;
can_ordem_item_prescr_w	number(10);
qt_dose_apres_ml_w	number;
cd_unid_med_concetracao_w	varchar2(30);
cd_unid_med_base_conc_w		varchar2(30);
ie_unidade_equivalentes_w	varchar2(1);
qt_conversao_mg_w	number;
Ie_sobras_overfill_w	varchar2(2);
Ie_sobras_overfill_ww	varchar2(1);
ie_tela_deferenciada_w	varchar2(1);
qt_dias_util_w		number(3);
ie_considera_overfill_w	varchar2(1);
ie_via_aplicacao_w		varchar2(30);
ie_conv_ml_w		varchar2(30);
qt_diluicao_w		number := 0;
ie_agrupador_w		number(2,0);
ds_erro			varchar2(2000);
dt_estabilidade_w		date;
nr_seq_sobra_overfill_w number(10);
nr_seq_medic_dil_w		number(10);
qt_dose_prescr_dil_w	number;
qt_ficha_tecnica_w	number(10);
qt_ficha_tec_sobra_w	number(10);
nr_seq_ficha_tec_w	number(10);
ie_exist_patient_medic_w	varchar2(1);
cd_pf_vinculo_w	far_estoque_cabine.cd_pessoa_fisica%type;
cd_material_patient_w	material.cd_material%type;

cursor C01 is
	select  f.nr_sequencia nr_seq_item_prescr,
		a.nr_sequencia,
		f.cd_material cd_material_prescrito,
		a.cd_material,
		sum(a.qt_estoque) qt_estoque,
		--sum(Obter_dose_convertida_quimio(f.cd_material,f.qt_dose,f.cd_unidade_medida,c.cd_unidade_medida_consumo)),
		c.cd_unidade_medida_consumo,
		b.ds_lote_fornec,
		f.qt_dose,
		f.cd_unidade_medida,
		a.nr_seq_lote_fornec,
		Obter_dose_convertida_quimio(f.cd_material,f.qt_dose,f.cd_unidade_medida,obter_unid_med_usua('ml')),
		'N',
		e.ie_via_aplicacao,
		null,
		null
	from    can_ordem_item_prescr f,
		can_ordem_prod e,
		far_etapa_producao d,
		material c,
		material_lote_fornec b,
		far_estoque_cabine a
	where  	f.nr_seq_ordem = e.nr_sequencia
	and 	e.nr_sequencia = nr_seq_ordem_p
	and 	d.nr_sequencia = e.nr_seq_etapa_prod
	and 	d.nr_seq_cabine = a.nr_seq_cabine
	and 	a.nr_seq_lote_fornec = b.nr_sequencia  (+)
	and		a.cd_material	     = c.cd_material
	and 	((a.cd_material = f.cd_material)  or
			(c.nr_seq_ficha_tecnica = (	select  max(x.nr_seq_ficha_tecnica)
										from    material x
										where	x.cd_material = f.cd_material))or
										(f.cd_material = c.cd_material_generico))
	and     qt_estoque	> 0
	and (ie_exist_patient_medic_w = 'N'
           or (ie_exist_patient_medic_w = 'S' and c.nr_seq_ficha_tecnica = nr_seq_ficha_tec_w and a.cd_pessoa_fisica = cd_pf_vinculo_w or
				(c.nr_seq_ficha_tecnica <> nr_seq_ficha_tec_w and a.cd_pessoa_fisica <> cd_pf_vinculo_w)))
		group by f.nr_sequencia,
		a.nr_sequencia,
		f.cd_material,
		a.cd_material,
		substr(obter_desc_material(a.cd_material),1,60),
		c.cd_unidade_medida_consumo,
		b.ds_lote_fornec,
		f.qt_dose,
		f.cd_unidade_medida,
		a.nr_seq_lote_fornec,
		Obter_dose_convertida_quimio(f.cd_material,f.qt_dose,f.cd_unidade_medida,obter_unid_med_usua('ml')),
		'N',
		e.ie_via_aplicacao
	union
	select  f.nr_sequencia nr_seq_item_prescr,
				null,--nr_seq_far_estoque_cab nr_sequencia,
		f.cd_material cd_material_prescrito,
		a.cd_material,
		sum(Obter_dose_convertida_quimio(a.cd_material,a.qt_saldo,a.cd_unidade_medida,c.cd_unidade_medida_consumo)),
		--sum(a.qt_saldo) qt_estoque,
		c.cd_unidade_medida_consumo,
		b.ds_lote_fornec,
		f.qt_dose,
		f.cd_unidade_medida,
		a.nr_seq_lote_fornec,
		Obter_dose_convertida_quimio(f.cd_material,f.qt_dose,f.cd_unidade_medida,obter_unid_med_usua('ml')),
		decode(ie_tipo,'OD','X',decode(ie_tipo,'SD','Y',ie_tipo)) ie_tipo,
		e.ie_via_aplicacao,
		a.dt_estabilidade,
		a.nr_sequencia
	from    can_ordem_item_prescr f,
		can_ordem_prod e,
		far_etapa_producao d,
		material c,
		material_lote_fornec b,
		quimio_sobra_overfill a
	where  	f.nr_seq_ordem = e.nr_sequencia
	and 	e.nr_sequencia = nr_seq_ordem_p
	and 	d.nr_sequencia = e.nr_seq_etapa_prod
	and 	d.nr_seq_cabine = a.nr_seq_cabine
	and 	a.nr_seq_lote_fornec = b.nr_sequencia  (+)
	and		a.cd_material	     = c.cd_material
	and 	((a.cd_material = f.cd_material)  or
			(c.nr_seq_ficha_tecnica = (	select  max(x.nr_seq_ficha_tecnica)
										from    material x
										where	x.cd_material = f.cd_material))or
										(f.cd_material = c.cd_material_generico))
	and     a.qt_saldo	> 0
	and 	Ie_sobras_overfill_ww = 'S'
	and   	(((a.ie_tipo = 'O') and (ie_considera_overfill_w = 'S')) or (a.ie_tipo = 'S') or ((ie_tela_deferenciada_w = 'S') and ((a.ie_tipo = 'SD') or (a.ie_tipo = 'OD')))) -- Adicionar os diluidos quando param 218 = T
	and		nvl(dt_estabilidade,sysdate + 1) > sysdate
	AND		(((round(Obter_conversao_ml(a.cd_material, a.qt_saldo,a.cd_unidade_medida),2) <= round(Obter_conversao_ml(a.cd_material, f.qt_dose,f.cd_unidade_medida),2)) AND (a.ie_tipo = 'SD' OR a.ie_tipo = 'OD')) OR (a.ie_tipo <> 'SD' AND a.ie_tipo <> 'OD')) --restringir medicamentos diluidos com dose superior a prescrita
	AND NVL((SELECT ROUND(MAX(obter_conversao_ml(x.cd_material, x.qt_saldo, x.cd_unidade_medida)), 2) 
		   FROM    quimio_sobra_overfill x 
		   WHERE   cd_estabelecimento = cd_estabelecimento_p
		   AND	   x.nr_seq_superior = a.nr_sequencia
		   AND	   x.ie_tipo = 'DI'),0) <= qt_dose_prescr_dil_w--OBTER_DOSE_CONV_QUIMIO_ORDEM(cd_dil_w, qt_dose_dil_w, ds_unid_med_dil_w, 'ML', f.nr_seq_ordem) --restringir medicamentos diluidos com dose do diluente superior a prescrita
	and		(ie_exist_patient_medic_w = 'N' or (qt_ficha_tec_sobra_w = 0 or (qt_ficha_tec_sobra_w > 0 and e.cd_pessoa_fisica not like cd_pf_vinculo_w)))
	group by f.nr_sequencia,
		f.cd_material,
		a.cd_material,
		substr(obter_desc_material(a.cd_material),1,60),
		c.cd_unidade_medida_consumo,
		b.ds_lote_fornec,
		f.qt_dose,
		f.cd_unidade_medida,
		a.nr_seq_lote_fornec,
		Obter_dose_convertida_quimio(f.cd_material,f.qt_dose,f.cd_unidade_medida,obter_unid_med_usua('ml')),
		ie_tipo,
		e.ie_via_aplicacao,
		a.dt_estabilidade,
		a.nr_sequencia
	order by 2;

cursor C02 is
	select 	a.nr_seq_ordem,
		a.nr_sequencia,
		a.cd_material,
		a.qt_dose qt_dose_mg,
		a.cd_unidade_medida cd_unidade,
		Obter_dose_convertida_quimio(a.cd_material,a.qt_dose,a.cd_unidade_medida,obter_unid_med_usua('ml')) QT_DOSE_ML,
		b.qt_dias_util,
		b.ie_via_aplicacao,
		nvl(a.ie_agrupador,1)
	from   	can_ordem_prod b,
		can_ordem_item_prescr a
	where  	b.nr_sequencia = a.nr_seq_ordem
	and	nr_seq_ordem = nr_seq_ordem_p
	order by 2;

begin

obter_param_usuario(3130, 366, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, Ie_sobras_overfill_ww);
obter_param_usuario(3130, 400, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_considera_overfill_w);
obter_param_usuario(3130, 404, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_conv_ml_w);
obter_param_usuario(3130, 218, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_tela_deferenciada_w);

delete from w_preparo_quimio_prescr
where 	nm_usuario = nm_usuario_p;
commit;

delete from w_preparo_quimio
where 	nm_usuario = nm_usuario_p;
commit;


open C02;
loop
fetch C02 into
	nr_seq_ordem_w,
	nr_seq_ordem_item_w,
	cd_material_w,
	qt_dose_w,
	cd_unid_med_prescr_w,
	qt_dose_ml_w,
	qt_dias_util_w,
	ie_via_aplicacao_w,
	ie_agrupador_w;
exit when C02%notfound;
	begin
	
	qt_dose_ml_saldo_w := 0;
	select	 round(sum(decode(cd_unidade_medida_real,obter_unid_med_usua('ml'),qt_dose_real,Obter_dose_convertida_quimio(cd_material,qt_dose_real,cd_unidade_medida_real,cd_unid_med_prescr_w))),3)
	into 	 qt_dose_real_w
	from	 can_ordem_prod_mat
	where 	 nr_seq_ordem = nr_seq_ordem_w
	and 	 nr_seq_item_prescr = nr_seq_ordem_item_w
	and 	 nr_seq_kit is null;

	if 	(nvl(qt_dose_real_w,0) = 0) then
		qt_dose_ml_saldo_w := qt_dose_w;
	else
		qt_dose_ml_saldo_w := qt_dose_w - qt_dose_real_w;
	end if;
	
	if	(ie_conv_ml_w = 'S') and
		(Obter_dose_convertida_quimio(cd_material_w,qt_dose_w,cd_unid_med_prescr_w,obter_unid_med_usua('ml')) = 0) and 
		(ie_agrupador_w = 1) then
		
		select 	max(Obter_dose_convertida_quimio(a.cd_material,a.qt_dose,a.cd_unidade_medida,obter_unid_med_usua('ml')))
		into	qt_diluicao_w
		from   	can_ordem_prod b,
				can_ordem_item_prescr a
		where  	b.nr_sequencia = a.nr_seq_ordem
		and		nr_seq_ordem = nr_seq_ordem_p
		and		ie_agrupador = 9;
		
		qt_dose_ml_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_prescr_w, qt_dose_w) * qt_diluicao_w;
	end if;
	if	(qt_dose_ml_saldo_w > 0) then
		insert into w_preparo_quimio_prescr(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_ordem_item,
			nr_seq_ordem,
			cd_material,
			qt_dose,
			cd_unidade_medida,
			qt_dose_ml,
			qt_dose_saldo,
			cd_unidade_medida_prescr,
			qt_dias_util,
			ie_agrupador)
		select 	w_preparo_quimio_prescr_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_ordem_item_w,
			nr_seq_ordem_w,
			cd_material_w,
			qt_dose_w,
			cd_unid_med_prescr_w,
			qt_dose_ml_w,
			qt_dose_ml_saldo_w,
			cd_unid_med_prescr_w,
			qt_dias_util_w,
			ie_agrupador_w
		from 	dual;
		commit;
	end if;
	end;
end loop;
close C02;

--Pegar dados do diluente da ordem. (mesmo tratemnto da procedure Obter_Diluente_ordem_prod)
select	min(nr_sequencia)
into	nr_seq_medic_dil_w
from	can_ordem_item_prescr
where	nr_seq_ordem	= nr_seq_ordem_p
and	nr_sequencia_diluente is null;

Select	nvl(max(OBTER_DOSE_CONV_QUIMIO_ORDEM(cd_material, qt_dose, cd_unidade_medida, upper(obter_unid_med_usua('ML')), nr_seq_ordem_p)),0)
into	qt_dose_prescr_dil_w
from	can_ordem_item_prescr
where	nr_seq_ordem		= nr_seq_ordem_p
and	nr_sequencia_diluente	= nr_seq_medic_dil_w
and	nr_sequencia	= substr((	select	min(nr_sequencia)
				from	can_ordem_item_prescr
				where	nr_seq_ordem	= nr_seq_ordem_p
				and	nr_sequencia_diluente is not null
				and	((ie_agrupador is null) or (ie_agrupador = 3))),1,10);

select nvl(max('S'),'N'),
		max(item.cd_material)
into ie_exist_patient_medic_w,
	cd_material_patient_w
from w_preparo_quimio_prescr w
left outer join can_ordem_item_prescr item
on w.cd_material = item.cd_material
join    can_ordem_prod ord_prod on item.nr_seq_ordem = ord_prod.nr_sequencia
join    far_etapa_producao etapa on etapa.nr_sequencia = ord_prod.nr_seq_etapa_prod
join    far_estoque_cabine cabin on etapa.nr_seq_cabine = cabin.nr_seq_cabine
left outer join    material_lote_fornec lote on cabin.nr_seq_lote_fornec = lote.nr_sequencia
join    material medic on cabin.cd_material = medic.cd_material
where ((cabin.cd_material = item.cd_material)  or
		(medic.nr_seq_ficha_tecnica = (	select  max(x.nr_seq_ficha_tecnica)
										from    material x
										where	x.cd_material = item.cd_material)) or
		(item.cd_material = medic.cd_material_generico))
and		cabin.qt_estoque	> 0
and cabin.cd_pessoa_fisica is not null
and item.nr_seq_ordem = nr_seq_ordem_p
and w.nm_usuario = nm_usuario_p;

/* control patient medicines to avoid duplicates */
if (ie_exist_patient_medic_w = 'S') then
begin
	select count(c.nr_seq_ficha_tecnica),
			c.nr_seq_ficha_tecnica,
			max(a.cd_pessoa_fisica)
	into qt_ficha_tecnica_w,
		nr_seq_ficha_tec_w,
		cd_pf_vinculo_w
	from	can_ordem_item_prescr f,
			can_ordem_prod e,
			far_etapa_producao d,
			material c,
			material_lote_fornec b,
			far_estoque_cabine a
	where	f.nr_seq_ordem = e.nr_sequencia
	and 	d.nr_sequencia = e.nr_seq_etapa_prod
	and 	d.nr_seq_cabine = a.nr_seq_cabine
	and 	a.nr_seq_lote_fornec = b.nr_sequencia  (+)
	and		a.cd_material	     = c.cd_material
	and 	((a.cd_material = f.cd_material)  or
			(c.nr_seq_ficha_tecnica = (	select  max(x.nr_seq_ficha_tecnica)
										from    material x
										where	x.cd_material = f.cd_material))or
										(f.cd_material = c.cd_material_generico))
	and 	e.nr_sequencia = nr_seq_ordem_p
    and     f.cd_material = cd_material_patient_w
	and     a.qt_estoque	> 0
    group by c.nr_seq_ficha_tecnica;

	/* seek leftover and/or overfill that has the same active principle of the medicine from the cabin, correlated to the prescribed medicine, to compare to the medicine owned by the patient */ 
	if (qt_ficha_tecnica_w > 0) then
	begin
		select nvl(max(count(medicine.nr_seq_ficha_tecnica)),0)
		into	qt_ficha_tec_sobra_w
		from    can_ordem_item_prescr item_prescr_ord_prod
		join    can_ordem_prod ord_prod on item_prescr_ord_prod.nr_seq_ordem = ord_prod.nr_sequencia
		join    far_etapa_producao etapa on etapa.nr_sequencia = ord_prod.nr_seq_etapa_prod
		join    quimio_sobra_overfill sobraoverfill on etapa.nr_seq_cabine = sobraoverfill.nr_seq_cabine
		left outer join    material_lote_fornec lote on sobraoverfill.nr_seq_lote_fornec = lote.nr_sequencia
		join    material medicine on sobraoverfill.cd_material = medicine.cd_material		
		where ((sobraoverfill.cd_material = item_prescr_ord_prod.cd_material)  or
				(medicine.nr_seq_ficha_tecnica = (	select  max(x.nr_seq_ficha_tecnica)
												from    material x
												where	x.cd_material = item_prescr_ord_prod.cd_material)) or
				(item_prescr_ord_prod.cd_material = medicine.cd_material_generico))
		and		qt_saldo	> 0
		and		ord_prod.nr_sequencia = nr_seq_ordem_p 
		and     item_prescr_ord_prod.cd_material = cd_material_patient_w
		and 	Ie_sobras_overfill_ww = 'S'
		and   	(((sobraoverfill.ie_tipo = 'O') and (ie_considera_overfill_w = 'S')) or (sobraoverfill.ie_tipo = 'S') or ((ie_tela_deferenciada_w = 'S') and ((sobraoverfill.ie_tipo = 'SD') or (sobraoverfill.ie_tipo = 'OD'))))
		and		nvl(dt_estabilidade,sysdate + 1) > sysdate
		and		(((round(Obter_conversao_ml(sobraoverfill.cd_material, sobraoverfill.qt_saldo,sobraoverfill.cd_unidade_medida),2) <= round(Obter_conversao_ml(sobraoverfill.cd_material, item_prescr_ord_prod.qt_dose,item_prescr_ord_prod.cd_unidade_medida),2)) and 
					(sobraoverfill.ie_tipo = 'SD' or sobraoverfill.ie_tipo = 'OD')) or
				(sobraoverfill.ie_tipo <> 'SD' and sobraoverfill.ie_tipo <> 'OD'))
		and nvl((select round(max(obter_conversao_ml(x.cd_material, x.qt_saldo, x.cd_unidade_medida)), 2) 
				from    quimio_sobra_overfill x 
				where   cd_estabelecimento = cd_estabelecimento_p
				and	   x.nr_seq_superior = sobraoverfill.nr_sequencia
				and	   x.ie_tipo = 'DI'),0) <= qt_dose_prescr_dil_w
		Group by item_prescr_ord_prod.cd_material;
	end;
	end if;
end;
end if;

open C01;
loop
fetch C01 into
	can_ordem_item_prescr_w,
	nr_seq_cabine_w,
	cd_material_prescrito_w,
	cd_material_w,
	qt_estoque_w,
	cd_unidade_medida_consumo_w,
	ds_lote_fornec_w,
	qt_dose_w,
	cd_unid_med_prescr_w,
	nr_seq_lote_fornec_w,
	qt_dose_apres_ml_w,
	Ie_sobras_overfill_w,
	ie_via_aplicacao_w,
	dt_estabilidade_w,
	nr_seq_sobra_overfill_w;
exit when C01%notfound;
	begin
	
	select	max(cd_unid_med_concetracao),
		max(cd_unid_med_base_conc)
	into	cd_unid_med_concetracao_w,
		cd_unid_med_base_conc_w
	from 	material
	where 	cd_material = cd_material_prescrito_w;

	qt_dose_util_cons_w := qt_dose_w;

	if 	(cd_material_prescrito_w is not null) and (cd_unid_med_concetracao_w is not null) and
		(lower(cd_unid_med_concetracao_w) <> lower(obter_unid_med_usua('ml')))  and (lower(cd_unid_med_base_conc_w) = lower(obter_unid_med_usua('ml'))) then
		select 	Obter_dose_convertida_quimio(cd_material_prescrito_w,qt_dose_w,cd_unid_med_prescr_w,cd_unid_med_concetracao_w)
		into	qt_dose_w
		from 	dual;

		select	nvl(max('S'),'N')
		into	ie_unidade_equivalentes_w
		from 	material
		where 	cd_material = cd_material_w
		and 	cd_unid_med_concetracao = cd_unid_med_concetracao_w
		and 	cd_unid_med_base_conc = cd_unid_med_base_conc_w;

		if 	(ie_unidade_equivalentes_w = 'S') then
			select	max(cd_unid_med_concetracao),
				max(cd_unid_med_base_conc),
				max(qt_conversao_mg)
			into	cd_unid_med_concetracao_w,
				cd_unid_med_base_conc_w,
				qt_conversao_mg_w
			from 	material
			where 	cd_material = cd_material_w;

		qt_dose_ml_w := dividir_sem_round(qt_dose_w,qt_conversao_mg_w);
		end if;
	else
		if (nvl(qt_dose_apres_ml_w,0) = 0) then
			select 	Obter_dose_convertida_quimio(cd_material_w,qt_dose_w,cd_unid_med_prescr_w,obter_unid_med_usua('ml'))
			into	qt_dose_ml_w
			from 	dual;
			
			if	(ie_conv_ml_w = 'S') then
			
				select 	max(Obter_dose_convertida_quimio(a.cd_material,a.qt_dose,a.cd_unidade_medida,obter_unid_med_usua('ml')))
				into	qt_diluicao_w
				from   	can_ordem_prod b,
						can_ordem_item_prescr a
				where  	b.nr_sequencia = a.nr_seq_ordem
				and		nr_seq_ordem = nr_seq_ordem_p
				and		ie_agrupador = 9;
				
				qt_dose_ml_w := obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_prescr_w, qt_dose_w) * qt_diluicao_w;
			end if;
		else
			qt_dose_ml_w := qt_dose_apres_ml_w;
		end if;

		select 	Obter_dose_convertida_quimio(cd_material_w,qt_estoque_w,cd_unidade_medida_consumo_w,obter_unid_med_usua('ml'))
		into	qt_estoque_ml_w
		from 	dual;
				
		if 	(qt_estoque_ml_w > qt_dose_ml_w) then
			qt_estoque_ww := qt_dose_ml_w;
			qt_dose_mg_ww := qt_dose_w;
		else
			qt_estoque_ww := qt_estoque_ml_w;
			qt_dose_mg_ww := qt_estoque_w;
		end if;
	end if;

	select 	Obter_dose_convertida_quimio(cd_material_w,qt_estoque_w,cd_unidade_medida_consumo_w,obter_unid_med_usua('ml'))
	into	qt_total_cabine_ml_w
	from 	dual;
	
	if	(ie_conv_ml_w = 'S') and
		(nvl(qt_total_cabine_ml_w,0) = 0) then

		select 	max(Obter_dose_convertida_quimio(a.cd_material,a.qt_dose,a.cd_unidade_medida,obter_unid_med_usua('ml')))
		into	qt_diluicao_w
		from   	can_ordem_prod b,
				can_ordem_item_prescr a
		where  	b.nr_sequencia = a.nr_seq_ordem
		and		nr_seq_ordem = nr_seq_ordem_p
		and		ie_agrupador = 9;
		
		qt_total_cabine_ml_w := (obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_prescr_w, qt_dose_w) * qt_diluicao_w) * qt_estoque_w;
	end if;

	begin
	insert into w_preparo_quimio(
		nr_sequencia, --Sequencial
		dt_atualizacao, --Data de atualizacao
		nm_usuario, --Usuario
		dt_atualizacao_nrec, -- Data de atualizacao
		nm_usuario_nrec, --Usuario Atualizacao
		nr_seq_cabine, --Numero da cabine
		ie_selecionado, --Se selecionado
		cd_material, --Material
		qt_material, -- Quantidade de medicamento na cabin
		cd_unidade_medida,
		qt_dose_ml,
		qt_dose_util_ml,
		nr_seq_lote_fornec,
		nr_seq_can_ordem_item,
		qt_total_cabine_ml,
		qt_dose_util_cons,
		ie_sobra_overfill,
		dt_estabilidade,
		nr_seq_quimio_over) -- nr_sequencia da tabela QUIMIO_SOBRA_OVERFILL
	select 	w_preparo_quimio_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_cabine_w,
		'N',
		cd_material_w,
		qt_estoque_w,
		cd_unidade_medida_consumo_w,
		nvl(qt_dose_ml_w,0),
		0,
		nr_seq_lote_fornec_w,
		can_ordem_item_prescr_w,
		qt_total_cabine_ml_w,
		0,
		Ie_sobras_overfill_w,
		dt_estabilidade_w,
		nr_seq_sobra_overfill_w
	from 	dual;
	exception
	when others then
	   null;
	end;

	end;
end loop;
close C01;

commit;

end Carrega_medicamentos_manip;
/