create or replace
procedure spa_gerar_movimento_prot(
			nr_seq_spa_p		Number,
			nr_seq_protocolo_p	Number,
			nm_usuario_p		Varchar2) is 

ie_data_contabil_spa_w	varchar2(1);	
dt_spa_w		spa.dt_spa%type;				
			
begin


ie_data_contabil_spa_w  := nvl(Obter_valor_param_usuario(7053, 23, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento),'N');
dt_spa_w		:=null;

if	(ie_data_contabil_spa_w = 'S') then
	begin
		select	dt_spa
		into	dt_spa_w
		from	spa
		where	nr_sequencia = nr_seq_spa_p;
	exception
	when others then
		dt_spa_w:=null;	
	end;
end if;

if	(nvl(nr_seq_protocolo_p,0) <> 0) then

	insert into spa_movimento (
		nr_sequencia,
		nr_seq_spa,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_classificacao,
		nr_seq_protocolo,
		dt_contabil
	) values (
		spa_movimento_seq.NextVal,
		nr_seq_spa_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		5,
		nr_seq_protocolo_p,
		dt_spa_w
	);
end if;

commit;

end spa_gerar_movimento_prot;
/