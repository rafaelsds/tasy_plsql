create or replace
procedure atualiza_lote_fornecedor_opm(	nr_sequencia_p		number,
					cd_lote_fornecedor_p	number	) is 

begin

update	ordem_producao_opm
set	cd_lote_fornecedor = cd_lote_fornecedor_p
where	nr_sequencia = nr_sequencia_p;

commit;

end atualiza_lote_fornecedor_opm;
/

