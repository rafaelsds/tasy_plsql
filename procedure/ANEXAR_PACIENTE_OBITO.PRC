create or replace
procedure Anexar_paciente_obito(nr_sequencia_p	       number,
			ds_arquivo_p	        Varchar2) is 

begin
if	(nvl(nr_sequencia_p,0) > 0) then

	update 	declaracao_obito
    	set 	ie_situacao = 'I',
		dt_inativacao = sysdate,
		nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario			
	where  	nr_sequencia = nr_sequencia_p;
	
	update	regra_numeracao_dec_item a
	set	ie_disponivel = 'S'
	where	nr_declaracao	= (	select	max(nr_declaracao) 
					from 	declaracao_obito 
					where  	nr_sequencia = nr_sequencia_p);
	commit;
	
	insert into declaracao_obito(nr_sequencia,           
								nm_usuario,             
								ie_utilizacao,          
								nr_atendimento,         
								nm_paciente,            
								nr_declaracao,          
								dt_obito,               
								dt_atualizacao,         
								nr_seq_causa_morte,     
								cd_cid_adic_3,          
								cd_cid_adic_4,          
								cd_medico,              
								ie_pos_cirurgico,       
								cd_cid_direta,          
								cd_cid_adic_1,          
								cd_cid_adic_2,          
								cd_cid_basica,          
								nm_pessoa_retirada,     
								ie_emissor,             
								ie_obito_mulher,        
								cd_perfil_ativo,        
								cd_medico_adicional,    
								ie_situacao,            
								dt_liberacao,                 
								ds_arquivo)
						(select	declaracao_obito_seq.nextval,
								wheb_usuario_pck.get_nm_usuario,
								ie_utilizacao,          
								nr_atendimento,         
								nm_paciente,            
								nr_declaracao,          
								dt_obito,               
								sysdate,         
								nr_seq_causa_morte,     
								cd_cid_adic_3,          
								cd_cid_adic_4,          
								cd_medico,              
								ie_pos_cirurgico,       
								cd_cid_direta,          
								cd_cid_adic_1,          
								cd_cid_adic_2,          
								cd_cid_basica,          
								nm_pessoa_retirada,     
								ie_emissor,             
								ie_obito_mulher,        
								cd_perfil_ativo,        
								cd_medico_adicional,    
								'A',            
								sysdate,               
								ds_arquivo_p
						from	declaracao_obito
						where	nr_sequencia = nr_sequencia_p);

end if;						
	

commit;

end Anexar_paciente_obito;
/
