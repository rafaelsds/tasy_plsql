create or replace
procedure pls_atualiza_estag_pedido_reen
			(	nr_seq_lote_p		Number,
				ie_estagio_p		Number,
				nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar o est�gios dos pedidos reenviados
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

update	pls_pedido_reenvio_scs
set	ie_estagio		= ie_estagio_p
where	nr_seq_lote_reenvio	= nr_seq_lote_p;

commit;

end pls_atualiza_estag_pedido_reen;
/