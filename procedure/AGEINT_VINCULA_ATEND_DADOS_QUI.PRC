create or replace
procedure ageint_vincula_atend_dados_qui	(	nr_atendimento_p	Number,
							nr_sequencia_p		Number,
							nm_usuario_p		Varchar2) is 

begin

if (nvl(nr_atendimento_p,0) > 0) and 
   (nvl(nr_sequencia_p,0) > 0)  then
   
   update	agenda_quimio
   set		nr_atendimento  = nr_atendimento_p,
		dt_atualizacao  = sysdate, 
		nm_usuario	= nvl(nm_usuario_p,obter_usuario_ativo)
   where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end ageint_vincula_atend_dados_qui;
/