create or replace
procedure sus_insert_proc_cbo(cd_procedimento_p		number,
			cd_cbo_p		varchar2,
			nm_usuario_p		Varchar2) is 

begin

insert into sus_procedimento_cbo
	(cd_procedimento, 
	ie_origem_proced, 
	cd_cbo, 
	dt_atualizacao, 
	nm_usuario, 
	dt_atualizacao_nrec, 
	nm_usuario_nrec, 
	nr_sequencia, 
	ie_bpa)
values(cd_procedimento_p, 
	7, 
	cd_cbo_p, 
	sysdate, 
	nm_usuario_p, 
	sysdate, 
	nm_usuario_p, 
	sus_procedimento_cbo_seq.nextval, 
	'S');

commit;

end sus_insert_proc_cbo;
/ 