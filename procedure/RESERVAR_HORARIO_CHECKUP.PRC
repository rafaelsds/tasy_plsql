create or replace
procedure reservar_horario_checkup(nr_seq_agenda_p	number,
						nm_usuario_p		varchar2,
						ie_reservado_p out	varchar2) is

qt_agenda_w		number(10,0);
ie_reservado_w	varchar2(1) := 'S';

qt_w			number(10);
begin

begin
if	(nr_seq_agenda_p is not null) then

	select	1
	into	qt_w
	from	checkup
	where	nr_sequencia = nr_seq_agenda_p
	for update;

	select	nvl(count(*),0)
	into	qt_agenda_w
	from	checkup
	where	nr_sequencia			= nr_seq_agenda_p
	and	nvl(ie_status_agenda,'L')	= 'L';

	if	(qt_agenda_w > 0) then
		update	checkup
		set	ie_status_agenda	= 'N',
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_agenda_p;
		commit;
		ie_reservado_w := 'S';
	else
		begin
		select	nvl(count(*),0)
		into	qt_agenda_w
		from	checkup
		where	nr_sequencia			= nr_seq_agenda_p
		and	cd_pessoa_fisica is not null;
		
		if	(qt_agenda_w > 0) then
			ie_reservado_w := 'S';
		else
			ie_reservado_w := 'N';
		end if;
		end;
	end if;
end if;
exception
	when others then
	commit;
end;

commit;

ie_reservado_p := ie_reservado_w;

end reservar_horario_checkup;
/