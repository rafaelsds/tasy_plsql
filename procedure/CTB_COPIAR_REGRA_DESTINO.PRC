CREATE OR REPLACE PROCEDURE ctb_copiar_regra_destino (
    NR_SEQUENCIA_ORIGEM_P   NUMBER,
    NR_SEQUENCIA_DESTINO_P     NUMBER,
    NM_USUARIO_P VARCHAR2
) IS
    SOMA_RATEIO INT;  
    CURSOR c01 IS
        SELECT
            nr_sequencia,
            nr_seq_regra_rateio,
            dt_atualizacao,
            nm_usuario,
            dt_atualizacao_nrec,
            nm_usuario_nrec,
            cd_conta_contabil_dest,
            cd_centro_custo_dest,
            pr_rateio,
            nr_seq_grupo
        FROM
            ctb_regra_rat_dest
        WHERE
            nr_seq_regra_rateio = nr_sequencia_origem_p;
    c01_w c01%rowtype;
BEGIN
    SELECT SUM(pr_rateio)
    INTO   SOMA_RATEIO
    FROM   ctb_regra_rat_dest
    WHERE  nr_seq_regra_rateio IN (nr_sequencia_origem_p, NR_SEQUENCIA_DESTINO_P);
    IF(SOMA_RATEIO > 100) THEN
        BEGIN
            wheb_mensagem_pck.exibir_mensagem_abort(455429);
        END;
    END IF;
    OPEN c01;
    LOOP
        FETCH c01
        INTO c01_w;
        EXIT WHEN c01%notfound;
        INSERT INTO ctb_regra_rat_dest
            (
                nr_sequencia,
                nr_seq_regra_rateio,
                dt_atualizacao,
                nm_usuario, 
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                cd_conta_contabil_dest,
                cd_centro_custo_dest,
                pr_rateio,
                nr_seq_grupo
                )
                VALUES(
                CTB_REGRA_RAT_DEST_SEQ.nextval,
                nr_sequencia_destino_p,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                c01_w.cd_conta_contabil_dest,
                c01_w.cd_centro_custo_dest,
                c01_w.pr_rateio,
                c01_w.nr_seq_grupo
                );
    END LOOP;
    CLOSE c01;
    COMMIT;
END ctb_copiar_regra_destino;
/
