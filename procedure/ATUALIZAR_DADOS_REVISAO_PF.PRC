create or replace
procedure atualizar_dados_revisao_pf(	dt_revisao_p		date ,
				nm_usuario_p		varchar2 ,
				cd_pessoa_fisica_p		varchar2 ) is 

begin

if	( dt_revisao_p		is not null) and
	( nm_usuario_p		is not null) and
	( cd_pessoa_fisica_p	is not null) then
	begin
	update	pessoa_fisica
	set	dt_revisao		= dt_revisao_p ,
		nm_usuario_revisao		= nm_usuario_p
	where	cd_pessoa_fisica		= cd_pessoa_fisica_p ;
	commit;
	end;
end if;

end atualizar_dados_revisao_pf;
/