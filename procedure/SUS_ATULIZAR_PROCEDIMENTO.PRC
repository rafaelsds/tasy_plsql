create or replace
procedure sus_atulizar_procedimento(
			ie_atualiza_p		varchar2,
			ie_origem_proced_p	varchar2,
			cd_procedimento_p	number,
			nm_usuario_p		varchar2) is

begin

if	(ie_origem_proced_p is not null) and
	(cd_procedimento_p is not null) then
	begin
	update	sus_procedimento
	set	ie_atualiza_cbo_medico = ie_atualiza_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	cd_procedimento = cd_procedimento_p
	and	ie_origem_proced = ie_origem_proced_p;
	end;
end if;

commit;

end sus_atulizar_procedimento;
/
