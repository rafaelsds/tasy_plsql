create or replace
procedure man_inserir_hist_urgencia_ccb (	nr_seq_ordem_serv_p		number,
						nm_usuario_p			varchar2 ) is

ds_relat_tecnico_w		varchar2(8000);

begin

ds_relat_tecnico_w := wheb_rtf_pck.get_cabecalho;
ds_relat_tecnico_w := ds_relat_tecnico_w || wheb_rtf_pck.get_fonte(20);
ds_relat_tecnico_w := ds_relat_tecnico_w || 'To be defined by Q' || chr(38) /* E comercial */ || 'R and DEC';
ds_relat_tecnico_w := ds_relat_tecnico_w || wheb_rtf_pck.get_rodape;

man_gravar_historico_ordem(
	nr_seq_ordem_serv_p,
	sysdate, -- dt_liberacao_p
	ds_relat_tecnico_w,
	'I', -- ie_origem_p
	190, -- nr_seq_tipo_p (Exce��o do CCB para urg�ncias fora do hor�rio comercial)
	nm_usuario_p
);

commit;

end man_inserir_hist_urgencia_ccb;
/