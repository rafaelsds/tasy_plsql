create or replace
procedure pep_liberar_med_avaliacao_pac (
		nr_sequencia_p				number,
		ie_avaliador_aux_p			varchar2,
		nr_seq_med_tipo_avaliacao_p		number,
		nm_usuario_p				varchar2,
		ds_mensagem_p			out	varchar2
		) is

ds_mensagem_w	varchar2(255);
		
begin

if	(nm_usuario_p is not null) and
	(nr_sequencia_p is not null) then 
	begin
	
	if	(ie_avaliador_aux_p = 'S') then
		begin
		
		update	med_avaliacao_paciente
		set     dt_liberacao_aux = sysdate,
			nm_usuario 	= nm_usuario_p,
			nm_usuario_lib = nm_usuario_p
		where	nr_sequencia 	= nr_sequencia_p;
		
		end;
	else
		begin
		
		liberar_avaliacao(nr_sequencia_p,nm_usuario_p);
		
		gerar_mensagem_avaliacao( nr_sequencia_p,nr_seq_med_tipo_avaliacao_p,ds_mensagem_w);
		
		end;
	end if;
		
	end;
end if;

commit;

ds_mensagem_p := ds_mensagem_w;

end pep_liberar_med_avaliacao_pac;
/