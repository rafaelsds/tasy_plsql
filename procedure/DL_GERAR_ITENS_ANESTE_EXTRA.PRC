create or replace
procedure dl_gerar_itens_aneste_extra(
			nr_seq_distribuicao_p	number,			
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number,
			vl_distribuicao_p	number) is 

nr_seq_socio_w			number(10);
nr_seq_lote_w			number(10);	
nr_seq_tipo_distrib_w		number(10);	
nr_seq_distrib_item_w		number(10);
qt_rotinas_extra_w		number(10);
qt_max_minutos_w		number(10);
qt_min_cirurgia_w		number(15,3);
vl_distribuicao_w		number(15,4);
vl_unitario_anestesia_w		number(15,4);
tx_criterio_w			number(15,6);
vl_pagar_socio_w		number(15,4);
cd_pessoa_fisica_w		varchar(10);
ie_plantao_w			varchar2(1);
dt_mesano_lote_w		date;
dt_inicial_lote_w		date;
dt_final_lote_w			date;
dt_entrada_w			date;
dt_saida_w			date;
dt_inicio_escala_w		date;
dt_fim_escala_w			date;
ds_msg_tp_distrib_w		varchar2(255);
			
Cursor C01 is
	select	a.dt_entrada,
		a.dt_saida,
		a.cd_pessoa_fisica
	from	dl_socio b,
		funcao_medico d,
		cirurgia c,	
		cirurgia_participante a
	where	a.cd_pessoa_fisica		= b.cd_pessoa_fisica
	-- and	trunc(a.dt_entrada, 'month')	= trunc(dt_mesano_lote_w, 'month')
	and	((trunc(a.dt_entrada,'dd') >= trunc(dt_inicial_lote_w,'dd')) and (trunc(a.dt_saida,'dd') <= trunc(dt_final_lote_w,'dd')))  -- afstringari 232919 30/07/2010		
	and	dl_obter_dados_socio(b.nr_sequencia,'T') = 'A'
	and	c.nr_cirurgia			= a.nr_cirurgia
	and	a.ie_funcao			= d.cd_funcao
	and	d.ie_anestesista			= 'S'
	and	c.cd_estabelecimento		= cd_estabelecimento_p
	order by
		a.dt_entrada;

Cursor C02 is
	select	a.dt_inicio,
		a.dt_fim
	from	escala b,
		escala_diaria a
	where	a.cd_pessoa_fisica		= cd_pessoa_fisica_w
	and	b.nr_sequencia			= a.nr_seq_escala
	and	b.ie_tipo			<> 'P'
	-- and	trunc(a.dt_inicio, 'month') 	= trunc(dt_mesano_lote_w, 'month')
	and	((trunc(a.dt_inicio,'dd') >= trunc(dt_inicial_lote_w,'dd')) and (trunc(a.dt_fim,'dd') <= trunc(dt_final_lote_w,'dd')))  -- afstringari 232919 30/07/2010		
	and	to_char(a.dt_inicio, 'dd')	= to_char(dt_entrada_w, 'dd')
	order by 
		a.dt_inicio;
	
begin

select	a.nr_seq_socio,
	a.nr_seq_lote
into	nr_seq_socio_w,
	nr_seq_lote_w
from 	dl_distribuicao a
where	a.nr_sequencia	= nr_seq_distribuicao_p;

if	(dl_obter_dados_socio(nr_seq_socio_w,'T') = 'A') then

	qt_rotinas_extra_w		:= 0;
	qt_max_minutos_w		:= 0;
	vl_distribuicao_w		:= 0;
	vl_unitario_anestesia_w		:= 0;
	tx_criterio_w			:= 0;
	vl_pagar_socio_w		:= 0;
		
	select	nvl(nvl(vl_distribuicao_p,a.vl_distribuicao),0),
		a.dt_mesano_ref,
		a.dt_inicial,
		a.dt_final
	into	vl_distribuicao_w,
		dt_mesano_lote_w,
		dt_inicial_lote_w,
		dt_final_lote_w			
	from	dl_lote_distribuicao a
	where	a.nr_sequencia	= nr_seq_lote_w;

	/* dominio DL - Tipo de item para calculo da distribuicao / Anestesias em hora extra */
	select	max(a.nr_sequencia),
		max(nvl(a.tx_item,0)),
		max(nvl(a.qt_max_minutos,0))
	into	nr_seq_tipo_distrib_w,
		tx_criterio_w,
		qt_max_minutos_w
	from	dl_tipo_item a
	where	a.ie_tipo_item		= 9
	and	a.nr_seq_socio		= nr_seq_socio_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p;
	
	if	(nr_seq_tipo_distrib_w is null) then
		select	max(a.nr_sequencia),
			max(nvl(a.tx_item,0)),
			max(nvl(a.qt_max_minutos,0))
		into	nr_seq_tipo_distrib_w,
			tx_criterio_w,
			qt_max_minutos_w
		from	dl_tipo_item a
		where	a.ie_tipo_item		= 9
		and	a.nr_seq_socio is null
		and	a.cd_estabelecimento	= cd_estabelecimento_p;
	end if;

	if	(nr_seq_tipo_distrib_w is null) then -- afstringari 236244 28/07/2010

		ds_msg_tp_distrib_w	:= substr(dl_obter_msg_tp_distrib(1,9),1,255);

		/* ds_msg_tp_distrib_w */
		wheb_mensagem_pck.exibir_mensagem_abort(262112,'DS_MSG_TP_DISTRIB_W='||ds_msg_tp_distrib_w);

	end if;	

	/*
	select	max(a.nr_sequencia),
		max(nvl(a.tx_item,0)),
		max(nvl(a.qt_max_minutos,0))
	into	nr_seq_tipo_distrib_w,
		tx_criterio_w,
		qt_max_minutos_w
	from	dl_tipo_item a
	where	a.ie_tipo_item		= 9
	and	a.cd_estabelecimento	= cd_estabelecimento_p;
	*/
	vl_unitario_anestesia_w	:= tx_criterio_w * vl_distribuicao_w;

	open C01;
	loop
	fetch C01 into	
		dt_entrada_w,
		dt_saida_w,
		cd_pessoa_fisica_w;
	exit when C01%notfound;
		begin
		
		open C02;
		loop
		fetch C02 into	
			dt_inicio_escala_w,
			dt_fim_escala_w;
		exit when C02%notfound;
			begin
			qt_min_cirurgia_w	:= 0;
			
			if	((to_char(dt_entrada_w, 'hh24:mi:ss') <= to_char(dt_inicio_escala_w, 'hh24:mi:ss')) and (to_char(dt_saida_w, 'hh24:mi:ss') <= to_char(dt_inicio_escala_w, 'hh24:mi:ss'))) then
			
				qt_min_cirurgia_w	:= ((dt_saida_w - dt_entrada_w) * 1440);
				qt_rotinas_extra_w	:= (qt_rotinas_extra_w + ceil(qt_min_cirurgia_w / qt_max_minutos_w));		
			
			elsif	((to_char(dt_entrada_w, 'hh24:mi:ss') >= to_char(dt_fim_escala_w, 'hh24:mi:ss')) and (to_char(dt_saida_w, 'hh24:mi:ss') >= to_char(dt_fim_escala_w, 'hh24:mi:ss'))) then

				qt_min_cirurgia_w	:= ((dt_saida_w - dt_entrada_w) * 1440);
				qt_rotinas_extra_w	:= (qt_rotinas_extra_w + ceil(qt_min_cirurgia_w / qt_max_minutos_w));					
				
			elsif	((to_char(dt_entrada_w, 'hh24:mi:ss') <= to_char(dt_inicio_escala_w, 'hh24:mi:ss')) and (to_char(dt_saida_w, 'hh24:mi:ss') <= to_char(dt_fim_escala_w, 'hh24:mi:ss'))) then
				
				qt_min_cirurgia_w	:= ((dt_inicio_escala_w - dt_entrada_w) * 1440);
				qt_rotinas_extra_w	:= (qt_rotinas_extra_w + ceil(qt_min_cirurgia_w / qt_max_minutos_w));		
				
			elsif	((to_char(dt_entrada_w, 'hh24:mi:ss') >= to_char(dt_inicio_escala_w, 'hh24:mi:ss')) and (to_char(dt_saida_w, 'hh24:mi:ss') >= to_char(dt_fim_escala_w, 'hh24:mi:ss'))) then
				
				qt_min_cirurgia_w	:= ((dt_saida_w - dt_fim_escala_w) * 1440);			
				qt_rotinas_extra_w	:= (qt_rotinas_extra_w + ceil(qt_min_cirurgia_w / qt_max_minutos_w));		
				
			elsif	((to_char(dt_entrada_w, 'hh24:mi:ss') <= to_char(dt_inicio_escala_w, 'hh24:mi:ss')) and (to_char(dt_saida_w, 'hh24:mi:ss') >= to_char(dt_fim_escala_w, 'hh24:mi:ss'))) then
				
				qt_min_cirurgia_w	:= ((dt_inicio_escala_w - dt_entrada_w) * 1440);						
				qt_min_cirurgia_w	:= qt_min_cirurgia_w + ((dt_saida_w - dt_fim_escala_w) * 1440);			
				qt_rotinas_extra_w	:= (qt_rotinas_extra_w + ceil(qt_min_cirurgia_w / qt_max_minutos_w));		
				
			end if;	
			
			end;
		end loop;
		close C02;
		
		end;
	end loop;
	close C01;

	vl_pagar_socio_w	:= vl_unitario_anestesia_w * qt_rotinas_extra_w;
	
	select	max(nvl(b.nr_sequencia,0))
	into	nr_seq_distrib_item_w
	from	dl_distribuicao a,
		dl_distribuicao_item b		
	where	a.nr_sequencia	= b.nr_seq_distribuicao
	and	a.nr_seq_lote	= nr_seq_lote_w
	and	a.nr_sequencia	= nr_seq_distribuicao_p
	and	b.nr_seq_item	= nr_seq_tipo_distrib_w;

	if	(nr_seq_distrib_item_w is null) and (vl_distribuicao_p is null) then
		insert into dl_distribuicao_item (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_distribuicao,
			nr_seq_item,
			tx_item,
			vl_item,
			qt_item,
			vl_calculado)
		values( dl_distribuicao_item_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_distribuicao_p,
			nr_seq_tipo_distrib_w,
			tx_criterio_w,
			0,
			qt_rotinas_extra_w,
			vl_pagar_socio_w);	
	else
		update 	dl_distribuicao_item
		set	dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			vl_item			= vl_pagar_socio_w
		where	nr_sequencia		= nr_seq_distrib_item_w
		and	nr_seq_distribuicao	= nr_seq_distribuicao_p;
	end if;
end if;

end dl_gerar_itens_aneste_extra;
/