create or replace
procedure oft_insere_auto_refracao (	nr_seq_consulta_p			number,
													nr_seq_consulta_form_p	number,
													vListaAutoRefracao	strRecTypeFormOft) is

nr_sequencia_w				oft_auto_refracao.nr_sequencia%type;
ds_observacao_w			oft_auto_refracao.ds_observacao%type;
dt_exame_w					oft_auto_refracao.dt_exame%type;
ie_receita_dinamica_w	oft_auto_refracao.ie_receita_dinamica%type;
ie_receita_estatica_w	oft_auto_refracao.ie_receita_estatica%type;
qt_distancia_np_w			oft_auto_refracao.qt_distancia_np%type;
qt_reflexo_are_od_w		oft_auto_refracao.qt_reflexo_are_od%type;
qt_reflexo_are_oe_w		oft_auto_refracao.qt_reflexo_are_oe%type;
qt_reflexo_od_w			oft_auto_refracao.qt_reflexo_od%type;
qt_reflexo_oe_w			oft_auto_refracao.qt_reflexo_oe%type;
vl_od_ard_eixo_w			oft_auto_refracao.vl_od_ard_eixo%type;
vl_od_ard_esf_w			oft_auto_refracao.vl_od_ard_esf%type;
vl_od_are_eixo_w			oft_auto_refracao.vl_od_are_eixo%type;
vl_od_are_esf_w			oft_auto_refracao.vl_od_are_esf%type;
vl_oe_ard_eixo_w			oft_auto_refracao.vl_oe_ard_eixo%type;
vl_oe_ard_esf_w			oft_auto_refracao.vl_oe_ard_esf%type;
vl_oe_are_eixo_w			oft_auto_refracao.vl_oe_are_eixo%type;
vl_oe_are_esf_w			oft_auto_refracao.vl_oe_are_esf%type;	
vl_oe_are_cil_w			oft_auto_refracao.vl_oe_are_cil%type;	
vl_oe_ard_cil_w			oft_auto_refracao.vl_oe_ard_cil%type;	
vl_od_are_cil_w			oft_auto_refracao.vl_od_are_cil%type;	
vl_od_ard_cil_w			oft_auto_refracao.vl_od_ard_cil%type;	
cd_profissional_w			oft_auto_refracao.cd_profissional%type;		
nm_usuario_w				usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
ie_registrado_w			varchar2(1) := 'N';
ds_erro_w					varchar2(4000);
											
begin
begin

if	(nvl(nr_seq_consulta_p,0) > 0) and (vListaAutoRefracao.count > 0) then
	for i in 1..vListaAutoRefracao.count loop
		begin
		if	(vListaAutoRefracao(i).ds_valor is not null) or 
			(vListaAutoRefracao(i).nr_valor is not null) then
			case upper(vListaAutoRefracao(i).nm_campo)
				when 'CD_PROFISSIONAL' then
					cd_profissional_w 	:= vListaAutoRefracao(i).ds_valor;
				when 'DT_EXAME' then
					dt_exame_w 	:= pkg_date_utils.get_DateTime(vListaAutoRefracao(i).ds_valor);
				when 'DS_OBSERVACAO' then
					DS_OBSERVACAO_w 	:= vListaAutoRefracao(i).ds_valor;
					ie_registrado_w	:= 'S';
				when 'IE_RECEITA_DINAMICA' then
					IE_RECEITA_DINAMICA_w 	:= vListaAutoRefracao(i).ds_valor;
				when 'IE_RECEITA_ESTATICA' then
					IE_RECEITA_ESTATICA_w 	:= vListaAutoRefracao(i).ds_valor;
				when 'QT_DISTANCIA_NP' then
					QT_DISTANCIA_NP_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w		:= 'S';
				when 'QT_REFLEXO_ARE_OD' then
					QT_REFLEXO_ARE_OD_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w		:= 'S';
				when 'QT_REFLEXO_ARE_OE' then
					QT_REFLEXO_ARE_OE_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w		:= 'S';
				when 'QT_REFLEXO_OD' then
					QT_REFLEXO_OD_w 		:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w		:= 'S';
				when 'QT_REFLEXO_OE' then
					QT_REFLEXO_OE_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OD_ARD_EIXO' then
					VL_OD_ARD_EIXO_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OD_ARD_ESF' then
					VL_OD_ARD_ESF_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OD_ARE_EIXO' then
					VL_OD_ARE_EIXO_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OD_ARE_ESF' then
					VL_OD_ARE_ESF_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OE_ARD_EIXO' then
					VL_OE_ARD_EIXO_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OE_ARD_ESF' then
					VL_OE_ARD_ESF_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OE_ARE_EIXO' then
					VL_OE_ARE_EIXO_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OE_ARE_ESF' then
					VL_OE_ARE_ESF_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OE_ARE_CIL' then
					vl_oe_are_cil_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OE_ARD_CIL' then
					vl_oe_ard_cil_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OD_ARE_CIL' then
					vl_od_are_cil_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				when 'VL_OD_ARD_CIL' then
					vl_od_ard_cil_w 	:= vListaAutoRefracao(i).nr_valor;
					ie_registrado_w	:= 'S';
				else
					null;
			end case;	
		end if;	
	end;
	end loop;

	select	max(nr_sequencia)
	into		nr_sequencia_w
	from		oft_auto_refracao
	where		nr_seq_consulta_form = nr_seq_consulta_form_p
	and		nr_seq_consulta		= nr_seq_consulta_p
	and		dt_liberacao 			is null
	and		nm_usuario				= nm_usuario_w;
	
	if	(nr_sequencia_w is not null) then
		update	oft_auto_refracao
		set		dt_atualizacao			=	sysdate,
					nm_usuario				= 	nm_usuario_w,
					ds_observacao			= 	ds_observacao_w,
					dt_exame					=	nvl(dt_exame_w,dt_exame),
					ie_receita_dinamica	= 	nvl(ie_receita_dinamica_w,ie_receita_dinamica),
					ie_receita_estatica	= 	nvl(ie_receita_estatica_w,ie_receita_estatica),
					qt_distancia_np		=	qt_distancia_np_w,
					qt_reflexo_are_od		=	qt_reflexo_are_od_w,
					qt_reflexo_are_oe		= 	qt_reflexo_are_oe_w,
					qt_reflexo_od			= 	qt_reflexo_od_w,
					qt_reflexo_oe			= 	qt_reflexo_oe_w,
					vl_od_ard_eixo			=	vl_od_ard_eixo_w,
					vl_od_ard_esf			= 	vl_od_ard_esf_w,
					vl_od_are_eixo			=	vl_od_are_eixo_w,
					vl_od_are_esf			=	vl_od_are_esf_w,
					vl_oe_ard_eixo			=	vl_oe_ard_eixo_w,
					vl_oe_ard_esf			=	vl_oe_ard_esf_w,
					vl_oe_are_eixo			=	vl_oe_are_eixo_w,
					vl_oe_are_esf			=	vl_oe_are_esf_w,
					vl_oe_are_cil			=	vl_oe_are_cil_w,
					vl_oe_ard_cil			=	vl_oe_ard_cil_w,
					vl_od_are_cil			=	vl_od_are_cil_w,
					vl_od_ard_cil			=	vl_od_ard_cil_w,
					cd_profissional		= nvl(cd_profissional_w,cd_profissional)
		where		nr_sequencia			=	nr_sequencia_w;
		wheb_usuario_pck.set_ie_commit('S');
	else
		if	(ie_registrado_w = 'S') then
			wheb_usuario_pck.set_ie_commit('S');
			select	oft_auto_refracao_seq.nextval
			into		nr_sequencia_w	
			from		dual;

			insert	into oft_auto_refracao (	nr_sequencia, 
															dt_atualizacao, 
															nm_usuario, 
															dt_atualizacao_nrec, 
															nm_usuario_nrec, 
															nr_seq_consulta, 
															ds_observacao,
															dt_exame,
															ie_receita_dinamica,
															ie_receita_estatica,
															qt_distancia_np,
															qt_reflexo_are_od,
															qt_reflexo_are_oe,
															qt_reflexo_od,
															qt_reflexo_oe,
															vl_od_ard_eixo,
															vl_od_ard_esf,
															vl_od_are_eixo,
															vl_od_are_esf,
															vl_oe_ard_eixo,
															vl_oe_ard_esf,
															vl_oe_are_eixo,
															vl_oe_are_esf,
															cd_profissional,
															nr_seq_consulta_form,
															vl_oe_are_cil,
															vl_oe_ard_cil,
															vl_od_are_cil,
															vl_od_ard_cil,
															ie_refracao_sugerida)
					values							(	nr_sequencia_w, 
															sysdate, 
															nm_usuario_w, 
															sysdate, 
															nm_usuario_w, 
															nr_seq_consulta_p, 
															ds_observacao_w,
															nvl(dt_exame_w,sysdate),
															nvl(ie_receita_dinamica_w,'N'),
															nvl(ie_receita_estatica_w,'N'),
															qt_distancia_np_w,
															qt_reflexo_are_od_w,
															qt_reflexo_are_oe_w,
															qt_reflexo_od_w,
															qt_reflexo_oe_w,
															vl_od_ard_eixo_w,
															vl_od_ard_esf_w,
															vl_od_are_eixo_w,
															vl_od_are_esf_w,
															vl_oe_ard_eixo_w,
															vl_oe_ard_esf_w,
															vl_oe_are_eixo_w,
															vl_oe_are_esf_w,
															nvl(cd_profissional_w,obter_pf_usuario(nm_usuario_w,'C')), 
															nr_seq_consulta_form_p,
															vl_oe_are_cil_w,
															vl_oe_ard_cil_w,
															vl_od_are_cil_w,
															vl_od_ard_cil_w,
															'N');
		end if;													
	end if;													
end if;	
exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,4000);
	update	OFT_CONSULTA_FORMULARIO
	set		ds_stack			=	substr(dbms_utility.format_call_stack||ds_erro_w,1,4000)
	where		nr_sequencia	= 	nr_seq_consulta_form_p;
end;

end oft_insere_auto_refracao;
/