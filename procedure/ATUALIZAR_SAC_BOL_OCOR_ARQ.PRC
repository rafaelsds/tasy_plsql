create or replace
procedure atualizar_sac_bol_ocor_arq(	nr_seq_bo_p	number,
					nm_usuario_p	varchar2) is 

begin

if	(nr_seq_bo_p is not null) then

	update	sac_bol_ocor_arq
	set	nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		ie_anexar_email	= 'N'
	where	nr_seq_bo	= nr_seq_bo_p;

	commit;

end if;

end atualizar_sac_bol_ocor_arq;
/