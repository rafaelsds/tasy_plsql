create or replace
procedure Excluir_fotos_antigas(qt_dias_p	number) is 

begin

delete	ATENDIMENTO_ACOMP_FOTO
where	DT_ATUALIZACAO < sysdate - qt_dias_p;

delete	ATENDIMENTO_VISITA_FOTO
where	DT_ATUALIZACAO < sysdate - qt_dias_p;

commit;

end Excluir_fotos_antigas;
/