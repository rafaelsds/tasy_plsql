create or replace
procedure gerar_ajustes_ap_lote(
								ie_evento_p			in ajuste_ap_lote_evento.ie_evento%type,
								nr_atendimento_p	in prescr_medica.nr_atendimento%type,
								nm_usuario_p		in usuario.nm_usuario%type,
								ie_tipo_movimento_p	in varchar2 default null) as


nr_seq_ajuste_w				ajuste_ap_lote_ident.nr_seq_ajuste%type;
nr_prescricao_ww			prescr_medica.nr_prescricao%type := 0;
nr_seq_ident_w				ajuste_ap_lote_alt.nr_seq_ident%type;
nr_seq_item_prescr_w		prescr_material.nr_sequencia%type;
ie_status_lote_w			ap_lote.ie_status_lote%type;
qt_min_impressao_w			pls_integer;
nr_seq_classif_w			ap_lote.nr_seq_classif%type;
ie_diferente_w				varchar2(1 char) := 'N';
cd_motivo_alta_w			atendimento_paciente.cd_motivo_alta%type;
cd_setor_w					atend_paciente_unidade.cd_setor_atendimento%type;
cd_local_estoque_ww			local_estoque.cd_local_estoque%type := 0;
cd_local_estoque_w			local_estoque.cd_local_estoque%type;
cd_local_estoque_new_w		local_estoque.cd_local_estoque%type;
cd_local_identificado_w		local_estoque.cd_local_estoque%type;
cd_local_identificado_ww	local_estoque.cd_local_estoque%type;
cd_local_old_w				local_estoque.cd_local_estoque%type;
cd_local_exclusao_w			local_estoque.cd_local_estoque%type;
cd_classif_setor_ant_w		setor_atendimento.cd_classif_setor%type;
cd_classif_novo_setor_w		setor_atendimento.cd_classif_setor%type;
dt_atend_lote_w				ap_lote.dt_atend_lote%type;
cd_setor_anterior_w			atend_paciente_unidade.cd_setor_atendimento%type;
cd_setor_anterior_ww  		atend_paciente_unidade.cd_setor_atendimento%type;
ie_gera_novo_atend_w		motivo_alta.ie_gera_novo_atend%type;
ie_somente_alta_hosp_w		ajuste_ap_lote_evento.ie_somente_alta_hosp%type;
ie_finaliza_w				varchar2(1 char) := 'N';
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_estab_prescr_w			estabelecimento.cd_estabelecimento%type := 0;
cd_estab_lote_w				estabelecimento.cd_estabelecimento%type := 0;
ie_arq_pyxis_w				parametro_atendimento.ie_arq_pyxis%type;
qt_existe_regra_local_w 	regra_local_dispensacao.nr_sequencia%type;
qt_existe_regra_w			pls_integer;
cd_setor_lote_w				ap_lote.cd_setor_atendimento%type;
nr_seq_turno_w				prescr_mat_hor.nr_seq_turno%type;
dt_prim_horario_w			ap_lote.dt_prim_horario%type;
ds_texto_w					ap_lote_historico.ds_evento%type;
ds_texto2_w					ap_lote_historico.ds_log%type;
ie_estab_dif_w				varchar2(1 char) := 'N';
nr_seq_interno_w			atend_paciente_unidade.nr_seq_interno%type;
cd_classif_setor_atend_w	setor_atendimento.cd_classif_setor%type;
ie_permite_atend_lote_w		setor_atendimento.ie_permite_atend_lote%type := 'S';
ie_local_horario_w			varchar2(1 char) := 'N';
final_w						boolean := true;

/*identifica os lotes do atendimento*/
cursor c01 is
select	a.nr_prescricao,
	a.nr_sequencia nr_seq_lote,
	nvl(a.nr_lote_agrupamento,0) nr_lote_agrupamento
from	ap_lote a,
	prescr_medica b
where	a.nr_prescricao = b.nr_prescricao
and	b.nr_atendimento = nr_atendimento_p
and	a.dt_cancelamento is null
and	b.dt_prescricao > (sysdate-30);

c01_w	c01%rowtype;

/*identifica as regras existentes... submete cada lote a regra identificando se deve ou nao entrar na rotina de alteracoes*/
cursor c02 is
select	nr_sequencia
from	ajuste_ap_lote_ident
where	nr_seq_ajuste	= nr_seq_ajuste_w
and	nvl(ie_status_lote, ie_status_lote_w) = ie_status_lote_w
and	(((nvl(ie_consiste_data_atend_lote,'N') = 'S') and (dt_atend_lote_w > sysdate)) or (nvl(ie_consiste_data_atend_lote,'N') = 'N')) --inserida 07/04/2011
and	(((nvl(ie_consiste_data_prim_hor,'N') = 'S') and (dt_prim_horario_w > sysdate)) or (nvl(ie_consiste_data_prim_hor,'N') = 'N'))
and	(((nvl(cd_classif_setor_ident,0) > 0) and (cd_classif_setor_ident = cd_classif_setor_ant_w)) or (cd_classif_setor_ident is null)) --01/06
and	(((nvl(cd_local_estoque,0) > 0) and (cd_local_estoque <> cd_local_exclusao_w)) or (cd_local_estoque is null))
and	(qt_min_impressao_w between nvl(qt_prim_horario_de, 0) and nvl(qt_prim_horario_ate, 9999999999) or (qt_min_impressao_w < 0))
order by nvl(cd_classif_setor_ident,0);
/*colocamos o qt_min_impressao_w < 0... para nao desconsiderar os casos que ja deveriam ter sido atendido e ainda nao foi*/

/*identifica o que deve ser alterado no lote*/
cursor c03 is
select	ie_status_lote,
	nr_seq_classif,
	ie_regra_local,
	ie_setor_atendimento,
	ie_desfazer_impressao,
	cd_local_estoque,
	cd_classif_setor_alt,
	ie_desdobra_lote,
	cd_setor_atendimento,
	ie_setor_lote,
	ie_alterar_turno,
	nvl(ie_tempo_dispensacao, 'N') ie_tempo_dispensacao
from	ajuste_ap_lote_alt
where	nr_seq_ident = nr_seq_ident_w
and	nvl(cd_classif_setor_alt,cd_classif_setor_atend_w) = cd_classif_setor_atend_w;

c03_w	c03%rowtype;

/*identifica os itens do lote p/ casos onde altera a regra de acordo com a regra dispensacao. assim verifica se todos os itens do lote devem ser alterados para o mesmo local de estoque */
cursor c04 is
select	b.nr_sequencia
from	ap_lote c,
	prescr_material b,
	ap_lote_item a
where	a.cd_material = b.cd_material
and	b.nr_prescricao = c.nr_prescricao
and	a.nr_seq_lote = c01_w.nr_seq_lote
and	b.nr_prescricao = c01_w.nr_prescricao
group by b.nr_sequencia;

/*identifica os itens superiores dos lotes para verificar se todos os itens sao dispensados no mesmo local de estoque */
cursor c05 is
select	nr_seq_material nr_seq_item_prescr,
		nr_sequencia nr_mat_hor
from	prescr_mat_hor
where	nr_seq_superior is null
and		nr_seq_lote = c01_w.nr_seq_lote
and		nr_prescricao = c01_w.nr_prescricao
group by nr_seq_material,
	nr_sequencia;

c05_w	c05%rowtype;

begin

obter_param_usuario(924,851,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w, ie_local_horario_w);

cd_classif_setor_atend_w := obter_classif_setor(obter_setor_atendimento(nr_atendimento_p));

select	nvl(max(cd_estabelecimento),obter_estabelecimento_ativo)
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select 	nvl(max(ie_arq_pyxis),'N')
into	ie_arq_pyxis_w
from	parametro_atendimento
where	cd_estabelecimento = cd_estabelecimento_w;

/*verifico se existe alguma regra para este evento*/
select	nvl(max(nr_sequencia), 0)
into	nr_seq_ajuste_w
from	ajuste_ap_lote_evento
where	ie_evento = ie_evento_p
and	cd_estabelecimento = cd_estabelecimento_w;

if	(nr_seq_ajuste_w > 0) then
	begin
	
	if	(ie_evento_p = 'A') then
	
		select	nvl(max(ie_somente_alta_hosp),'N')
		into	ie_somente_alta_hosp_w
		from	ajuste_ap_lote_evento
		where	nr_sequencia = nr_seq_ajuste_w;
		
		if	(ie_somente_alta_hosp_w = 'S') then
		
			select	nvl(max(cd_motivo_alta),0)
			into	cd_motivo_alta_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_p;
			
			select	nvl(max(ie_gera_novo_atend),'N')
			into	ie_gera_novo_atend_w
			from	motivo_alta
			where	cd_motivo_alta = cd_motivo_alta_w;
			
			if	(ie_gera_novo_atend_w = 'S') then
				ie_finaliza_w := 'S';
			end if;
		end if;
	end if;

	if	(ie_finaliza_w = 'N') then
		
		/* Rotina criada para identificar o setor anterior ao atual do paciente */
		select	count(1)
		into	qt_existe_regra_w
		from	atend_paciente_unidade b
		where	nr_atendimento = nr_atendimento_p
		and	dt_saida_unidade is null;
		
		if	(qt_existe_regra_w = 1) then
			select	nvl(max(a.nr_seq_interno),0)
			into	nr_seq_interno_w
			from 	atend_paciente_unidade a
			where	a.nr_atendimento 	= nr_atendimento_p
			and 	a.dt_entrada_unidade	=  (select max(b.dt_entrada_unidade)
							   from atend_paciente_unidade b
							   where b.nr_atendimento = nr_atendimento_p
							   and b.dt_saida_unidade is not null
							   and b.ie_passagem_setor <> 'S');
		elsif	(qt_existe_regra_w > 1) then
			select	nvl(max(nr_seq_interno),0)
			into	nr_seq_interno_w
			from	atend_paciente_unidade a
			where	a.nr_atendimento = nr_atendimento_p
			and	dt_entrada_unidade =	(select	max(b.dt_entrada_unidade)
							from	atend_paciente_unidade b
							where	b.dt_saida_unidade is null
							and	b.nr_atendimento = nr_atendimento_p
							and	b.dt_entrada_unidade <>	(select	max(b.dt_entrada_unidade)
											from	atend_paciente_unidade b
											where	b.nr_atendimento = nr_atendimento_p));
		end if;
		
		if	(nvl(nr_seq_interno_w,0) = 0) then
			select	nvl(max(nr_seq_interno),0)
			into	nr_seq_interno_w
			from	atend_paciente_unidade a
			where	a.nr_atendimento = nr_atendimento_p
			and	dt_entrada_unidade =	(select	max(a.dt_entrada_unidade)
							from	atend_paciente_unidade a
							where	a.nr_atendimento = nr_atendimento_p);
		end if;
		
		if	(nr_seq_interno_w > 0) then	
			select 	nvl(max(cd_setor_atendimento),0)
			into	cd_setor_anterior_w
			from 	atend_paciente_unidade
			where 	nr_seq_interno = nr_seq_interno_w;
		end if;
		/* Rotina criada para identificar o setor anterior ao atual do paciente */
		
		/*identifico todos os lotes deste atendimento*/
		open c01;
		loop
		fetch c01 into
			c01_w;
		exit when c01%notfound;
			begin
			if	(c01_w.nr_prescricao <> nr_prescricao_ww) then
				ie_estab_dif_w := 'N';
			end if;
			
			/*  Rotina para identificar se o paciente foi transferido de estabelecimento.  
			      Sistema ira verificar a alteracao de estabelecimento atraves do estab da prescricao e do estab da classificacao do lote.  */
			
			ie_estab_dif_w := 'N';
			
			select	nvl(max(cd_estabelecimento),cd_estabelecimento_w)
			into	cd_estab_prescr_w
			from	prescr_medica
			where	nr_prescricao = c01_w.nr_prescricao;
					
			select	nvl(max(nr_seq_classif),0)
			into	nr_seq_classif_w
			from	ap_lote
			where	nr_sequencia = c01_w.nr_seq_lote;
			
			select	nvl(max(cd_estabelecimento),cd_estab_prescr_w)
			into	cd_estab_lote_w
			from	classif_lote_disp_far
			where	nr_sequencia = nr_seq_classif_w;
			
			if	(cd_estab_prescr_w <> cd_estab_lote_w) then
				ie_estab_dif_w := 'S';
			end if;
						
			if	(ie_estab_dif_w = 'S') then
				atualizar_classif_lote(c01_w.nr_prescricao,cd_estab_prescr_w,nm_usuario_p);
			end if;
			
			if (ie_tipo_movimento_p = 'F') then
				select nvl(max(nr_seq_interno), 0)
				into nr_seq_interno_w
				from atend_paciente_unidade a
				where a.nr_atendimento = nr_atendimento_p
				and dt_entrada_unidade = (select max(b.dt_entrada_unidade)
											from atend_paciente_unidade b
											where b.dt_saida_unidade is not null
											and b.nr_atendimento = nr_atendimento_p);
											
				if (nr_seq_interno_w is not null) then
					select nvl(max(cd_setor_atendimento), 0)
					into cd_setor_anterior_ww
					from atend_paciente_unidade
					where nr_seq_interno = nr_seq_interno_w;
					
					if (cd_setor_anterior_ww is not null) then
						cd_setor_anterior_w := cd_setor_anterior_ww;
					end if;
				end if;
			end if;
			 
			/*busco as informacoes de cada lote*/
			select	max(ie_status_lote),
					max(trunc((dt_prim_horario - sysdate) * 1440)),
					max(dt_atend_lote),
					max(obter_classif_setor(cd_setor_anterior_w)),
					max(cd_local_estoque),
					max(dt_prim_horario)
			into	ie_status_lote_w,
					qt_min_impressao_w,
					dt_atend_lote_w,
					cd_classif_setor_ant_w,
					cd_local_exclusao_w,
					dt_prim_horario_w
			from	ap_lote
			where	nr_sequencia = c01_w.nr_seq_lote;
			
			/*busco as regras para identificacao dos lotes ou seja: submeto cada lote as regras afim de saber quais devem sofrer alteracoes*/
			open c02;
			loop
			fetch c02 into
				nr_seq_ident_w;
			exit when c02%notfound;
				begin
				
				/*neste momento ja tenho as regras de ajustes, entao identifico as regras para fazer os ajustes em cada lote*/
				open c03;
				loop
				fetch c03 into			
					c03_w;
				exit when c03%notfound;
					begin

					if	(c03_w.ie_setor_atendimento = 'S') then

						cd_setor_w := obter_setor_atendimento(nr_atendimento_p);
						
						select	nvl(max(ie_permite_atend_lote),'S')
						into	ie_permite_atend_lote_w
						from	setor_atendimento
						where	cd_setor_atendimento = cd_setor_w;
						
						if	(ie_permite_atend_lote_w <> 'N') then
							update	ap_lote
							set	cd_setor_atendimento	= cd_setor_w
							where	nr_sequencia		= c01_w.nr_seq_lote;
							
							ds_texto_w	:= substr(wheb_mensagem_pck.get_texto(311212),1,255);--Alteracao - Ajuste Setor
							ds_texto2_w	:= substr(wheb_mensagem_pck.get_texto(311214),1,255);--Alterado o Setor de atendimento do lote pela regra.
							
							insert into ap_lote_historico(
								nr_sequencia,				dt_atualizacao,
								nm_usuario,				nr_seq_lote,
								ds_evento,				ds_log) 
							values(	ap_lote_historico_seq.nextval,		sysdate,
								nm_usuario_p,				c01_w.nr_seq_lote,
								ds_texto_w,				ds_texto2_w);
						end if;
					end if;
					
					
					if	(ie_permite_atend_lote_w <> 'N') then						
						if	(c03_w.ie_status_lote is not null) then	
							if	(c01_w.nr_lote_agrupamento > 0) then
								update	ap_lote
								set		ie_status_lote	= c03_w.ie_status_lote
								where	nr_sequencia	= c01_w.nr_lote_agrupamento;
							end if;
						
							if	(c03_w.cd_classif_setor_alt is null) and (c03_w.cd_setor_atendimento is null) then
								update	ap_lote
								set	ie_status_lote	= c03_w.ie_status_lote
								where	nr_sequencia	= c01_w.nr_seq_lote;
								
							elsif	(c03_w.cd_setor_atendimento is null) then
								cd_classif_novo_setor_w	:= obter_classif_setor(obter_setor_atendimento(nr_atendimento_p));
								
								if	(c03_w.cd_classif_setor_alt = cd_classif_novo_setor_w) then
									update	ap_lote
									set	ie_status_lote	= c03_w.ie_status_lote
									where	nr_sequencia	= c01_w.nr_seq_lote;
								end if;
							else
								cd_setor_w := obter_setor_atendimento(nr_atendimento_p);

								if	(c03_w.cd_setor_atendimento = cd_setor_w) then
									update	ap_lote
									set	ie_status_lote	= c03_w.ie_status_lote
									where	nr_sequencia	= c01_w.nr_seq_lote;
								end if;
							end if;
						
							if	(ie_status_lote_w = 'CA') then
								update	ap_lote
								set	ie_status_lote	= c03_w.ie_status_lote
								where	nr_sequencia	= c01_w.nr_seq_lote;
							end if;						
							if	(c03_w.ie_status_lote = 'CA') then
								update	ap_lote
								set	dt_impressao		= sysdate
								where	nr_sequencia		= c01_w.nr_seq_lote;
							end if;
							if	(c03_w.cd_classif_setor_alt is null) or
								(c03_w.cd_classif_setor_alt = cd_classif_novo_setor_w) then
								
								ds_texto_w	:= substr(wheb_mensagem_pck.get_texto(311217),1,255);--Alteracao - Ajuste Status
								ds_texto2_w	:= substr(wheb_mensagem_pck.get_texto(311219),1,255);--Alterado o Status do lote pela regra.
							
								insert into ap_lote_historico(
									nr_sequencia,				dt_atualizacao,
									nm_usuario,				nr_seq_lote,
									ds_evento,				ds_log) 
								values(	ap_lote_historico_seq.nextval,		sysdate,
									nm_usuario_p,				c01_w.nr_seq_lote,
									ds_texto_w,				ds_texto2_w);
							end if;
						end if;
						
						cd_setor_w := obter_setor_atendimento(nr_atendimento_p);
						
						if	(c03_w.ie_desfazer_impressao in ('S','A')) then
							if	(c03_w.ie_desfazer_impressao = 'A') then
								if	(nvl(cd_setor_w,0) <> nvl(cd_setor_anterior_w,0)) then
									desfazer_impressao_ap_lote(c01_w.nr_seq_lote, nm_usuario_p,0);
								end if;
							else
								desfazer_impressao_ap_lote(c01_w.nr_seq_lote, nm_usuario_p,1);
							end if;
						end if;
						
						if	(c03_w.nr_seq_classif is not null) then
							update	ap_lote
							set	nr_seq_classif	= c03_w.nr_seq_classif
							where	nr_sequencia	= c01_w.nr_seq_lote;
							
							ds_texto_w	:= substr(wheb_mensagem_pck.get_texto(311221),1,255);--Alteracao - Ajuste Classificacao
							ds_texto2_w	:= substr(wheb_mensagem_pck.get_texto(311222),1,255);--Alterado a Classificacao do lote pela regra.

							insert into ap_lote_historico(
								nr_sequencia,				dt_atualizacao,
								nm_usuario,				nr_seq_lote,
								ds_evento,				ds_log) 
							values(	ap_lote_historico_seq.nextval,		sysdate,
								nm_usuario_p,				c01_w.nr_seq_lote,
								ds_texto_w,				ds_texto2_w);

						end if;
						
						if	(c03_w.ie_regra_local = 'P') then
							begin
							if	(c03_w.cd_classif_setor_alt is not null) then
								cd_classif_novo_setor_w	:= obter_classif_setor(obter_setor_atendimento(nr_atendimento_p));
								
								if	(c03_w.cd_classif_setor_alt <> cd_classif_novo_setor_w) then
									final_w := false;
								end if;
							end if;
							
							if	(final_w) then
								cd_setor_w := obter_setor_atendimento(nr_atendimento_p);
								select	nvl(max(cd_local_estoque), 0)
								into	cd_local_estoque_ww
								from	setor_atendimento
								where	cd_setor_atendimento = cd_setor_w;
								
								update	ap_lote
								set		cd_setor_atendimento = cd_setor_w
								where	nr_sequencia = c01_w.nr_seq_lote;
								
								select	nvl(max(cd_local_estoque),0)
								into	cd_local_old_w
								from	ap_lote
								where	nr_sequencia = c01_w.nr_seq_lote;
							end if;
							
							if	(cd_local_estoque_ww > 0) then
								update	ap_lote
								set	cd_local_estoque = cd_local_estoque_ww
								where	nr_sequencia = c01_w.nr_seq_lote;
								
								ds_texto_w := substr(wheb_mensagem_pck.get_texto(311223),1,255);--Alteracao - Ajuste Local
								ds_texto2_w := substr(wheb_mensagem_pck.get_texto(	nr_seq_mensagem_p => 311226,
																					vl_macros_p => 'ds_local_old_p='||obter_desc_local_estoque(cd_local_old_w)||
															 ';ds_local_estoque_p='|| obter_desc_local_estoque(cd_local_estoque_ww)),1,255);
								/* Alterado o Local do lote pela regra.
								Local (old/new) = (#@ds_local_old_p#@ / #@ds_local_estoque_p#@) */
								
								insert into ap_lote_historico(
									nr_sequencia,				dt_atualizacao,
									nm_usuario,				nr_seq_lote,
									ds_evento,				ds_log) 
								values(	ap_lote_historico_seq.nextval,		sysdate,
									nm_usuario_p,				c01_w.nr_seq_lote,
									ds_texto_w,				ds_texto2_w);
							end if;
							end;
						elsif	(c03_w.ie_regra_local = 'D') then
							
							intdisp_suspender_horario(nr_atendimento_p,c01_w.nr_prescricao, ie_evento_p, nm_usuario_p);

							if	(c03_w.ie_desdobra_lote = 'N') then
								begin
								cd_local_identificado_ww := 0;
								cd_local_identificado_w := 0;
								if	(c03_w.cd_local_estoque is not null) then
									open c04;
									loop
									fetch c04 into
										nr_seq_item_prescr_w;
									exit when c04%notfound;
										begin
										cd_local_identificado_w	:= obter_local_disp_prescr(
															c01_w.nr_prescricao,
															nr_seq_item_prescr_w,
															obter_perfil_ativo,
															nm_usuario_p);
															
										if	(cd_local_identificado_w <> cd_local_identificado_ww) and
											(cd_local_identificado_ww <> 0) then
											ie_diferente_w := 'S';
										end if;
										cd_local_identificado_ww := cd_local_identificado_w;
										end;
									end loop;
									close c04;
								end if;
								
								define_local_disp_prescr(
										c01_w.nr_prescricao,
										null,
										obter_perfil_ativo,
										nm_usuario_p);

								if (c03_w.cd_local_estoque is null) then
									if	(ie_local_horario_w = 'S') then
										select	max(cd_local_estoque)
										into	cd_local_estoque_w
										from	prescr_mat_hor
										where	nr_seq_lote	= c01_w.nr_seq_lote
										and 	nr_prescricao	= c01_w.nr_prescricao;
									end if;
									if	(cd_local_estoque_w is null) then
										select	max(cd_local_estoque)
										into	cd_local_estoque_w
										from	prescr_material
										where	nr_prescricao	= c01_w.nr_prescricao;
									end if;
								end if;
								
								if	(ie_diferente_w = 'N') and (cd_local_identificado_w > 0) then
									cd_local_estoque_w := cd_local_identificado_w;
								end if;

								select	nvl(max(cd_local_estoque),0)
								into	cd_local_old_w
								from	ap_lote
								where	nr_sequencia = c01_w.nr_seq_lote;
								
								if	(cd_local_estoque_w > 0) then
									update	ap_lote
									set	cd_local_estoque = cd_local_estoque_w
									where	nr_sequencia = c01_w.nr_seq_lote;
								end if;
								
								ds_texto_w	:= substr(wheb_mensagem_pck.get_texto(311223),1,255);--Alteracao - Ajuste Local
								ds_texto2_w	:= substr(wheb_mensagem_pck.get_texto(	nr_seq_mensagem_p => 311228,
																					vl_macros_p => 'ds_local_old_p='||obter_desc_local_estoque(cd_local_old_w)||
															';ds_local_estoque_p='|| obter_desc_local_estoque(cd_local_estoque_w)),1,255);
							/*	Alterado o Local do lote pela regra dispensacao.
								Local (old/new) = (#@ds_local_old_p#@ / #@ds_local_estoque_p#@) */

								insert into ap_lote_historico(
									nr_sequencia,				dt_atualizacao,
									nm_usuario,				nr_seq_lote,
									ds_evento,				ds_log) 
								values(	ap_lote_historico_seq.nextval,		sysdate,
									nm_usuario_p,				c01_w.nr_seq_lote,
									ds_texto_w,				'1 - '||ds_texto2_w);
								end;
							elsif	(c03_w.ie_desdobra_lote = 'S') then
								begin

								select	count(1)
								into	qt_existe_regra_w
								from	prescr_mat_hor
								where	nr_seq_superior is null
								and	nr_seq_lote = c01_w.nr_seq_lote
								and	nr_prescricao = c01_w.nr_prescricao;
								
								select	max(cd_local_estoque)
								into	cd_local_estoque_new_w
								from	ap_lote
								where	nr_sequencia = c01_w.nr_seq_lote;
								
								cd_local_old_w := cd_local_estoque_new_w;
								
								if	(qt_existe_regra_w > 1) then
								
									cd_local_identificado_ww := 0;

									open c05;
									loop
									fetch c05 into
										c05_w;
									exit when c05%notfound;
										begin
										define_disp_prescr_mat_hor(c05_w.nr_mat_hor, c01_w.nr_prescricao, c05_w.nr_seq_item_prescr, obter_perfil_ativo, nm_usuario_p, c03_w.ie_setor_atendimento, c03_w.ie_setor_lote);
										
										select	max(cd_local_estoque)
										into	cd_local_identificado_w
										from	prescr_mat_hor
										where	nr_sequencia = c05_w.nr_mat_hor;
										
										if	(cd_local_identificado_ww = 0) and
											(cd_local_identificado_w <> cd_local_estoque_new_w) then
											
											cd_local_estoque_new_w := cd_local_identificado_w;
											cd_local_identificado_ww := cd_local_identificado_w;
											
											update	prescr_mat_hor
											set	cd_local_estoque = cd_local_estoque_new_w
											where	nr_seq_material = c05_w.nr_seq_item_prescr
											and	nr_prescricao = c01_w.nr_prescricao
											and	nr_seq_lote = c01_w.nr_seq_lote;
											
											update	ap_lote
											set	cd_local_estoque = cd_local_estoque_new_w
											where	nr_sequencia = c01_w.nr_seq_lote;
										
										end if;
															
										if	(cd_local_identificado_w <> cd_local_identificado_ww) and
											(cd_local_identificado_w <> cd_local_estoque_new_w) and
											(cd_local_identificado_ww <> 0) then
											
											ie_diferente_w := 'S';
											
											ajuste_desdobra_lote_prescr(	c01_w.nr_prescricao,
															c05_w.nr_seq_item_prescr,
															c01_w.nr_seq_lote,
															cd_local_identificado_w,
															cd_estabelecimento_w,
															nm_usuario_p);	
										else
											cd_local_identificado_ww := cd_local_identificado_w;
										end if;
										
										end;
									end loop;
									close c05;
								else

									select	max(nr_seq_material)
									into	nr_seq_item_prescr_w
									from	prescr_mat_hor
									where	nr_seq_superior is null
									and	nr_seq_lote = c01_w.nr_seq_lote
									and	nr_prescricao = c01_w.nr_prescricao;
									
									cd_local_identificado_w	:= obter_local_disp_prescr_item(
															c01_w.nr_prescricao,
															nr_seq_item_prescr_w,
															obter_perfil_ativo,
															'N',
															c03_w.ie_setor_atendimento,
															c03_w.ie_setor_lote,
															nm_usuario_p);
															
									update	ap_lote
									set	cd_local_estoque = cd_local_identificado_w
									where	nr_sequencia = c01_w.nr_seq_lote;
									
								end if;
								
								if	(ie_diferente_w = 'N') and (cd_local_identificado_w > 0) then

									select	count(1)
									into	qt_existe_regra_w
									from	dis_regra_setor;
								
									if	(qt_existe_regra_w > 0) then
									
										if	(nvl(qt_existe_regra_w,0) > 0) then
								
											update	dispensario_mat_hor
											set	cd_local_estoque = cd_local_identificado_w,
												ie_lido		= null
											where	nr_seq_lote	= c01_w.nr_seq_lote;
										end if;
									end if;	
									
									if	(ie_arq_pyxis_w = 'S') then
										
										select 	count(*) 
										into	qt_existe_regra_w
										from	dis_regra_local
										where	cd_estabelecimento = cd_estabelecimento_w;		
										
										if	(nvl(qt_existe_regra_w,0) > 0) then
											
											select	cd_setor_atendimento
											into	cd_setor_lote_w
											from 	ap_lote
											where 	nr_sequencia = c01_w.nr_seq_lote;
											
											select	max(nr_sequencia)
											into	qt_existe_regra_local_w
											from	regra_local_dispensacao
											where 	cd_local_estoque = cd_local_identificado_w
											and 	nr_seq_estrut_int is not null
											and 	nr_seq_regra_disp is not null
											and 	cd_setor_atendimento = cd_setor_lote_w;

											if	(nvl(qt_existe_regra_local_w,0) > 0) then
												dis_gerar_arq_lote(c01_w.nr_seq_lote, '1', nm_usuario_p, cd_estabelecimento_w);
												
												select	nvl(max(obter_turno_horario_prescr(cd_estabelecimento_w,cd_setor_lote_w,to_char(dt_horario,'hh24:mi'),cd_local_identificado_w)),0)
												into	nr_seq_turno_w
												from	prescr_mat_hor
												where	nr_seq_lote = c01_w.nr_seq_lote
												and	nr_prescricao = c01_w.nr_prescricao;
											
												if	(nr_seq_turno_w > 0) then
													update	ap_lote
													set	nr_seq_turno = nr_seq_turno_w
													where	nr_sequencia = c01_w.nr_seq_lote;
													
													update	prescr_mat_hor
													set	nr_seq_turno = nr_seq_turno_w
													where	nr_seq_lote = c01_w.nr_seq_lote
													and	nr_prescricao = c01_w.nr_prescricao;
													
												end if;
											end if;
										end if;	
									end if;	
									
									ds_texto_w	:= substr(wheb_mensagem_pck.get_texto(311223),1,255);--Alteracao - Ajuste Local
									ds_texto2_w	:= substr(wheb_mensagem_pck.get_texto(	nr_seq_mensagem_p => 311228,
																						vl_macros_p => 'ds_local_old_p='||obter_desc_local_estoque(cd_local_old_w)||
															';ds_local_estoque_p='|| obter_desc_local_estoque(nvl(cd_local_estoque_ww, cd_local_estoque_w))),1,255);
								/*	Alterado o Local do lote pela regra dispensacao.
									Local (old/new) = (#@ds_local_old_p#@ / #@ds_local_estoque_p#@) */							
									insert into ap_lote_historico(
										nr_sequencia,				dt_atualizacao,
										nm_usuario,				nr_seq_lote,
										ds_evento,				ds_log) 
									values(	ap_lote_historico_seq.nextval,		sysdate,
										nm_usuario_p,				c01_w.nr_seq_lote,
										ds_texto_w,				'2 - '||ds_texto2_w);
								end if;
								end;
							end if;
						end if;
						
						if	(nvl(c03_w.ie_alterar_turno,'N') = 'S') then
							select	max(cd_setor_atendimento),
									max(cd_local_estoque)
							into	cd_setor_lote_w,
									cd_local_identificado_w
							from	ap_lote
							where	nr_sequencia = c01_w.nr_seq_lote;
							
							select	nvl(max(obter_turno_horario_prescr(cd_estab_prescr_w,cd_setor_lote_w,
									to_char(dt_horario,'hh24:mi'),cd_local_identificado_w)),0)
							into	nr_seq_turno_w
							from	prescr_mat_hor
							where	nr_seq_lote = c01_w.nr_seq_lote
							and		nr_prescricao = c01_w.nr_prescricao;
							
							if	(nr_seq_turno_w > 0) then
								update	prescr_mat_hor
								set	nr_seq_turno = nr_seq_turno_w
								where	nr_seq_lote = c01_w.nr_seq_lote
								and	nr_prescricao = c01_w.nr_prescricao;
								
								update	ap_lote
								set	nr_seq_turno = nr_seq_turno_w
								where	nr_sequencia = c01_w.nr_seq_lote
								and	ie_status_lote = 'G';
								
								/* Alteracao - Ajuste turno */
								ds_texto_w	:= substr(wheb_mensagem_pck.get_texto(318583),1,255);
								/* Alterado o Turno do lote pela regra. */
								ds_texto2_w	:= substr(wheb_mensagem_pck.get_texto(318584),1,255);
								
								insert into ap_lote_historico(
									nr_sequencia,				dt_atualizacao,
									nm_usuario,				nr_seq_lote,
									ds_evento,				ds_log) 
								values(	ap_lote_historico_seq.nextval,		sysdate,
									nm_usuario_p,				c01_w.nr_seq_lote,
									ds_texto_w,				ds_texto2_w);
							end if;
						end if;
						
						if	(c03_w.ie_tempo_dispensacao = 'S') and
							(ie_evento_p in ('M', 'S')) then
							regra_ajuste_tempo_dispensacao(c01_w.nr_seq_lote, cd_estab_prescr_w, nm_usuario_p);
							grava_log_ap_lote_hist(c01_w.nr_seq_lote, substr(wheb_mensagem_pck.get_texto(960847),1,255), substr(wheb_mensagem_pck.get_texto(960847),1,255), nm_usuario_p);
						end if;
					end if;
					end;
				end loop;
				close c03;
				
				end;
			end loop;
			close c02;
			
			nr_prescricao_ww := c01_w.nr_prescricao;
			
			if	(ie_estab_dif_w = 'S') then
				select	max(cd_setor_atendimento),
						max(cd_local_estoque)
				into	cd_setor_lote_w,
						cd_local_identificado_w
				from	ap_lote
				where	nr_sequencia = c01_w.nr_seq_lote;
				
				select	nvl(max(obter_turno_horario_prescr(cd_estab_prescr_w,cd_setor_lote_w,to_char(dt_horario,'hh24:mi'),cd_local_identificado_w)),0)
				into	nr_seq_turno_w
				from	prescr_mat_hor
				where	nr_seq_lote = c01_w.nr_seq_lote
				and	nr_prescricao = c01_w.nr_prescricao;
				
				if	(nr_seq_turno_w > 0) then
					update	prescr_mat_hor
					set	nr_seq_turno = nr_seq_turno_w
					where	nr_seq_lote = c01_w.nr_seq_lote
					and	nr_prescricao = c01_w.nr_prescricao;
					
					update	ap_lote
					set	nr_seq_turno = nr_seq_turno_w
					where	nr_sequencia = c01_w.nr_seq_lote
					and	ie_status_lote = 'G';
					
					atualizar_tempo_disp_lote(	nr_prescricao_p => c01_w.nr_prescricao,
												cd_estabelecimento_p => cd_estabelecimento_w,
												nm_usuario_p => nm_usuario_p);
				end if;
			end if;				
			end;
		end loop;
		close c01;
	end if;
	end;
end if;

if	(nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then 
	commit; 
end if;
end gerar_ajustes_ap_lote;
/