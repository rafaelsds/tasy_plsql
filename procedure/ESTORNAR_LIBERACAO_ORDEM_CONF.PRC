create or replace
procedure estornar_liberacao_ordem_conf(
			nr_nota_fiscal_p	varchar2,
			nm_usuario_p		varchar2) is 

begin

update 	ordem_compra_item_conf a
set 	dt_estorno = sysdate,
	nm_usuario_estorno = nm_usuario_p
where 	nr_nota_fiscal = nr_nota_fiscal_p
and	qt_conferencia is null;

commit;

end estornar_liberacao_ordem_conf;
/