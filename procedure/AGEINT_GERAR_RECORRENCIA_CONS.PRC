create or replace
procedure ageint_gerar_recorrencia_cons (nr_seq_agenda_int_p	number,
					nr_seq_ageint_item_p	number,
					nm_usuario_p		varchar2,
					ie_diario_p		varchar2,
					ie_semanal_p		varchar2,
					ds_dias_p		varchar2,
					dt_inicio_recorrencia_p	date,
					dt_fim_recorrencia_p	date,
					nr_minuto_duracao_p	number,
					qt_intervalo_p		number,
					ie_final_semana_p	varchar2) is 

nr_seq_item_w			number(10);
ds_dias_w			varchar2(255);
dt_dia_semana_w			number(1);
qt_dia_w			number(2);
ie_gerar_dia_w			varchar2(1);
ie_fim_sab_chec_w		varchar2(1);
ie_fim_dom_chec_w		varchar2(1);
dt_atual_w			date;
cd_dia_semana_w			varchar2(1);
dt_inicial_w			date;
dt_final_w			date;
nr_seq_item_princ_w		number(10);
ie_primeiro_item_recorr_w	varchar2(1);
dt_prevista_w			date;
ie_gerar_rec_exa_adic		varchar(1);
qt_exame_adic			number(10);

begin
ds_dias_w 		:= '';
dt_inicial_w	:= trunc(dt_inicio_recorrencia_p);
dt_final_w		:= trunc(dt_fim_recorrencia_p);
dt_Atual_w		:= dt_inicial_w;


if	(nr_seq_agenda_int_p is not null) then
	ie_primeiro_item_recorr_w := 'S';
	
	Obter_Param_Usuario(869, 405, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_gerar_rec_exa_adic);	
	if(ie_gerar_rec_exa_adic = 'S') then
		select	count(*)
		into	qt_exame_adic
		from	ageint_exame_adic_item
		where	nr_seq_item = nr_seq_ageint_item_p;
	end if;
	
	select	max(dt_prevista)
	into	dt_prevista_w
	from	agenda_integrada
	where	nr_sequencia = nr_seq_agenda_int_p;
	
	if	(dt_prevista_w is not null ) and
		(dt_inicio_recorrencia_p < dt_prevista_w) then
		update	agenda_integrada
		set	dt_prevista = dt_inicio_recorrencia_p
		where	nr_sequencia = nr_seq_agenda_int_p;
	elsif	(dt_prevista_w is null ) then
		update	agenda_integrada
		set	dt_prevista = dt_inicio_recorrencia_p
		where	nr_sequencia = nr_seq_agenda_int_p;
	end if;
	
	if	(ie_diario_p  = 'S') then
		qt_dia_w	:= 1;
	elsif	(ie_diario_p = 'N') and (qt_intervalo_p > 0) then
		qt_dia_w:= qt_intervalo_p;
	elsif	(ie_semanal_p = 'S') then
		qt_dia_w	:= 7;	
	end if;
	
	while	(dt_atual_w <= dt_final_w) loop
		begin		
		/* obter dias semana */

		select	obter_cod_dia_semana(dt_Atual_w) 
		into	dt_dia_semana_w
		from	dual;
		
		ie_gerar_dia_w	:= 'S';
		
		if	(ds_dias_p is not null) then
			select	substr(ds_dias_p,1,length(ds_dias_p) -2)
			into	ds_dias_w
			from	dual;
			
			ie_gerar_dia_w	:=  obter_se_contido(dt_dia_semana_w,ds_dias_w);			
		end if;
		
		select	obter_se_contido(7,ds_dias_w)
		into	ie_fim_sab_chec_w
		from	dual;
		
		select	obter_se_contido(1,ds_dias_w)
		into	ie_fim_dom_chec_w
		from	dual;
		
		if	(ds_dias_p is null) then
			if	(dt_dia_semana_w = 1) and			
				(ie_final_semana_p = 'N') then
				ie_Gerar_dia_w	:= 'N';
			elsif	(dt_dia_semana_w = 7) and			
				(ie_final_semana_p = 'N') then
				ie_Gerar_dia_w	:= 'N';
			end if;
		end if;
		
		if	(ie_gerar_dia_w = 'S') and
			(ie_primeiro_item_recorr_w = 'S') then
			update	agenda_integrada_item
			set	dt_prevista_item = dt_atual_w,
				ie_tipo_item = 'P'
			where	nr_sequencia = nr_seq_ageint_item_p;
			
			ie_primeiro_item_recorr_w	:= 'N';
		
		elsif	(ie_gerar_dia_w = 'S') then				
	
			select	agenda_integrada_item_seq.nextval
			into	nr_seq_item_w
			from	dual;
			
			insert into agenda_integrada_item
				(
				nr_sequencia,
				nr_seq_agenda_int,
				dt_atualizacao,
				nm_usuario,
				dt_prevista_item,
				nr_minuto_duracao,
				ie_tipo_agendamento,
				ie_tipo_item,
				nr_seq_item_princ,
				ie_classif_agenda,
				nr_seq_regiao,
				cd_medico,
				nr_seq_grupo_selec,
				cd_especialidade,
				ie_lado,
				nr_seq_proc_interno
				)
			select	nr_seq_item_w,
				nr_seq_agenda_int_p,
				sysdate,
				nm_usuario_p,
				dt_atual_w,
				nr_minuto_duracao_p,
				ie_tipo_agendamento,
				'S',
				nr_sequencia,
				ie_classif_agenda,
				nr_seq_regiao,
				cd_medico,
				nr_seq_grupo_selec,
				cd_especialidade,
				ie_lado,
				nr_seq_proc_interno
			from	agenda_integrada_item
			where	nr_sequencia = nr_seq_ageint_item_p;

			if(ie_gerar_rec_exa_adic = 'S') and (qt_exame_adic > 0) then
				
				insert into ageint_exame_adic_item
						(nr_sequencia,
						nr_seq_item,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec, 
						nr_seq_proc_interno,
						nr_seq_grupo_selec,
						cd_procedimento,
						ie_origem_proced,
						ie_lado)
					select
						ageint_exame_adic_item_seq.nextval,
						nr_seq_item_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_proc_interno,
						nr_seq_grupo_selec, 
						cd_procedimento,
						ie_origem_proced,
						ie_lado
					from 	ageint_exame_adic_item
					where 	nr_seq_item = nr_seq_ageint_item_p;					
			
			end if;
		end if;			
		dt_atual_w  	:= dt_atual_w + qt_dia_w;
		dt_inicial_w := dt_inicial_w + 1;
		
		end;
	end loop;	
	
	
end if;	

commit;

end ageint_gerar_recorrencia_cons;
/
