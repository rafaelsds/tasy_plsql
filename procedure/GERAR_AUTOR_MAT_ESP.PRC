CREATE OR REPLACE 
procedure gerar_autor_mat_esp(nr_atendimento_p	in	number,
			nr_seq_agenda_p		in	number,
			nr_seq_agenda_cons_p	in	number,
			nr_prescricao_p		in	number,
			nr_seq_mat_prescr_p	in	number,
			nr_seq_mat_autor_p	in	number,
			nm_usuario_p		in	varchar2,
			nr_seq_autorizacao_p	out	number) is

nr_sequencia_w			number(10)	:= null;
cd_estabelecimento_w		number(4);
nr_seq_autorizacao_w		number(10);
cd_material_w			number(6);
cd_material_validacao_w		number(6);
qt_solicitada_w			number(15,3);
nr_seq_autor_conv_w		number(10);
nr_prescricao_w			number(14);
ie_obter_preco_convenio_w	varchar2(2);
ds_retorno_w			varchar2(4000);
ds_observacao_w			varchar2(4000);
dt_agenda_w			date;
cd_convenio_w			number(15);
ie_valor_conta_w		varchar2(5);
ie_estagio_autor_w		varchar2(2);
cd_cgc_fornec_opme_w		varchar2(14)	:= null;
nr_orcamento_w			varchar2(20);
nr_seq_opme_w			number(10);
nr_seq_regra_plano_w		number(10,0);
pr_adicional_w			number(15,4);
ie_reducao_acrescimo_w		varchar2(1);
cd_cgc_fornec_w			varchar2(14);
nr_seq_marca_w			number(10,0);
ie_gerar_dados_lote_fornec_w	varchar2(1);
cd_cgc_fabricante_w		varchar2(14);
nr_doc_convenio_w		varchar2(20);
cd_senha_w			varchar2(20);
ie_autor_mat_esp_cirurgia_w	varchar2(1);
ie_preco_mat_esp_w		convenio_estabelecimento.ie_preco_mat_esp%type;
vl_preco_w			material_autor_cirurgia.vl_unitario_material%type;
nr_seq_mat_atend_w		material_atend_paciente.nr_sequencia%type;
ie_fornec_mat_autor_esp_w		parametro_faturamento.ie_fornec_mat_autor_esp%type;
nr_atend_agenda_w			atendimento_paciente.nr_atendimento%type;
ie_autor_generico_w		varchar2(1);
cd_pessoa_fisica_w		autorizacao_cirurgia.cd_pessoa_fisica%type;
vl_cotado_w			material_autorizado.vl_cotado%type;

begin


if	(nr_atendimento_p is not null) or
	(nr_seq_agenda_p is not null) then

	nr_seq_autorizacao_p	:= null;

	if	(nr_prescricao_p is not null) and
		(nr_seq_mat_prescr_p is not null) then

		select	cd_material,
			qt_material,
			cd_fornec_consignado,
			nr_seq_marca
		into	cd_material_w,
			qt_solicitada_w,
			cd_cgc_fabricante_w,
			nr_seq_marca_w
		from	prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_seq_mat_prescr_p;

	elsif	(nr_seq_mat_autor_p is not null) then
		select	cd_material,
			qt_solicitada,
			substr(ds_observacao,1,4000),
			nr_seq_opme,
			cd_cgc_fabricante,
			nr_seq_marca,
			nvl(vl_cotado,0)
		into	cd_material_w,
			qt_solicitada_w,
			ds_observacao_w,
			nr_seq_opme_w,
			cd_cgc_fabricante_w,
			nr_seq_marca_w,
			vl_cotado_w
		from	material_autorizado
		where	nr_sequencia	= nr_seq_mat_autor_p;
	end if;

	if	(nr_atendimento_p is not null) then
		select	max(a.cd_estabelecimento),
			max(b.cd_convenio),
			max(nr_doc_convenio),
			max(cd_senha),
			max(cd_pessoa_fisica)
		into	cd_estabelecimento_w,
			cd_convenio_w,
			nr_doc_convenio_w,
			cd_senha_w,
			cd_pessoa_fisica_w
		from	atendimento_paciente a,
			atend_categoria_convenio b
		where	a.nr_atendimento	= nr_atendimento_p
		and 	a.nr_atendimento	= b.nr_atendimento;
		
		select 	nvl(max(ie_estagio_autor),'1')			
		into 	ie_estagio_autor_w
		from 	parametro_faturamento
		where 	cd_estabelecimento	= cd_estabelecimento_w;

		if	(nr_seq_mat_autor_p is not null) then

			select	max(a.nr_sequencia_autor),
				max(b.nr_prescricao),
				max(a.nr_seq_regra_plano),
				max(b.cd_autorizacao),
				max(b.cd_senha)
			into	nr_seq_autor_conv_w,
				nr_prescricao_w,
				nr_seq_regra_plano_w,
				nr_doc_convenio_w,
				cd_senha_w
			from	autorizacao_convenio b,
				material_autorizado a
			where	a.nr_sequencia_autor = b.nr_sequencia
			and	a.nr_sequencia	= nr_seq_mat_autor_p;


			select	max(nr_sequencia)
			into	nr_seq_autorizacao_w
			from	autorizacao_cirurgia a
			where	nr_atendimento = nr_atendimento_p
			and	nvl(ie_estagio_autor,ie_estagio_autor_w) not in ('3','5','6','7','8','9')
			and	dt_fim_cotacao is null
			and	dt_autorizacao is null
			and	exists	(select	1
					from	estagio_autorizacao y,
						autorizacao_convenio x
					where	x.nr_seq_estagio	= y.nr_sequencia
					and	x.nr_seq_autor_cirurgia	= a.nr_sequencia
					and	nvl(x.nr_prescricao,nvl(nr_prescricao_w,0)) = nvl(nr_prescricao_w,0));


		else

			select	max(nr_sequencia)
			into	nr_seq_autorizacao_w
			from	autorizacao_cirurgia
			where	nr_atendimento = nr_atendimento_p
			and	nvl(nr_prescricao,0) = nvl(nr_prescricao_p,nvl(nr_prescricao,0))
			and	nvl(ie_estagio_autor,ie_estagio_autor_w) not in ('3','5','6','7','8','9')
			and	dt_fim_cotacao is null
			and	dt_autorizacao is null;
		end if;

	elsif	(nr_seq_agenda_p is not null) then

		select	max(b.cd_estabelecimento),
			max(a.hr_inicio),
			max(a.cd_convenio),
			max(a.nr_doc_convenio),
			max(a.nr_atendimento),
			max(a.cd_pessoa_fisica)
		into	cd_estabelecimento_w,
			dt_agenda_w,
			cd_convenio_w,
			nr_doc_convenio_w,
			nr_atend_agenda_w,
			cd_pessoa_fisica_w
		from	agenda b,
			agenda_paciente a
		where	a.cd_agenda	= b.cd_agenda
		and	a.nr_sequencia	= nr_seq_agenda_p;	
		
		select 	nvl(max(ie_estagio_autor),'1'),
			nvl(max(ie_gerar_dados_lote_fornec),'N'),
			nvl(max(ie_fornec_mat_autor_esp),'N')
		into 	ie_estagio_autor_w,
			ie_gerar_dados_lote_fornec_w,
			ie_fornec_mat_autor_esp_w
		from 	parametro_faturamento
		where 	cd_estabelecimento	= cd_estabelecimento_w;

		select	max(nr_sequencia)
		into	nr_seq_autorizacao_w
		from	autorizacao_cirurgia
		where	nr_seq_agenda	= nr_seq_agenda_p
		and	nvl(ie_estagio_autor,ie_estagio_autor_w) not in ('3','5','6','7','8','9')
		and	dt_fim_cotacao is null
		and	dt_autorizacao is null;

		nr_seq_autorizacao_p	:= nr_seq_autorizacao_w;
		
		if	(nr_seq_mat_autor_p is not null) then
			select	max(nr_sequencia_autor),
				max(nr_seq_regra_plano)
			into	nr_seq_autor_conv_w,
				nr_seq_regra_plano_w
			from	material_autorizado
			where	nr_sequencia	= nr_seq_mat_autor_p;
		end if;
	end if;
	

	select 	nvl(max(ie_vl_conta_autor), 'N'),
		nvl(max(ie_autor_mat_esp_cirurgia),'N'),
		nvl(max(ie_preco_mat_esp),'0'),
		nvl(max(ie_autor_generico),'N')
	into 	ie_valor_conta_w,
		ie_autor_mat_esp_cirurgia_w,
		ie_preco_mat_esp_w,
		ie_autor_generico_w
	from 	convenio_estabelecimento
	where	cd_convenio 		= cd_convenio_w
	and 	cd_estabelecimento	= cd_estabelecimento_w;
	
						
	if	(ie_autor_generico_w = 'S') and
		(cd_material_w is not null) then
		cd_material_validacao_w	:= obter_material_generico(cd_material_w);
	end if;
					
	
	
	if	(ie_autor_mat_esp_cirurgia_w = 'S') then
		if	(nr_atendimento_p is not null) then
			select	nvl(max(a.nr_sequencia), nr_seq_autor_conv_w)
			into	nr_seq_autor_conv_w
			from	autorizacao_convenio a
			where	a.nr_atendimento = nr_atendimento_p
			and	a.ie_tipo_autorizacao = 5
			and	exists (select 1
					from	material_autorizado x
					where	x.cd_material = nvl(cd_material_validacao_w,cd_material_w)
					and	x.nr_sequencia_autor = a.nr_sequencia);
		elsif	(nr_seq_agenda_p is not null) then
			select	nvl(max(a.nr_sequencia), nr_seq_autor_conv_w)
			into	nr_seq_autor_conv_w
			from	autorizacao_convenio a
			where	a.nr_seq_agenda = nr_seq_agenda_p
			and	a.ie_tipo_autorizacao = 5
			and	exists (select 1
					from	material_autorizado x
					where	x.cd_material = nvl(cd_material_validacao_w,cd_material_w)
					and	x.nr_sequencia_autor = a.nr_sequencia);
		end if;
	end if;

	/* Criar autoriza��o se ainda n�o criada */
	if	(nr_seq_autorizacao_w is null) then
			
		select	autorizacao_cirurgia_seq.nextval
		into	nr_seq_autorizacao_w
		from	dual;

		insert into autorizacao_cirurgia
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			dt_pedido,
			nm_usuario_pedido,
			nr_seq_agenda,
			nr_atendimento,
			nr_seq_agenda_consulta,
			cd_estabelecimento,
			ie_estagio_autor,
			nr_prescricao,
			nr_seq_autor_conv,
			dt_previsao,
			nr_doc_convenio,
			cd_senha,
			cd_pessoa_fisica)
		values	(nr_seq_autorizacao_w,
			nm_usuario_p,
			sysdate,
			sysdate,
			nm_usuario_p,
			nr_seq_agenda_p,
			nvl(nr_atendimento_p,nr_atend_agenda_w),
			nr_seq_agenda_cons_p,
			cd_estabelecimento_w,
			ie_estagio_autor_w,
			nr_prescricao_p,
			nr_seq_autor_conv_w,
			dt_agenda_w,
			nr_doc_convenio_w,
			cd_senha_w,
			cd_pessoa_fisica_w);

		nr_seq_autorizacao_p	:= nr_seq_autorizacao_w;
	end if;

	/* Inserir materiais na autoriza��o */
	if	(cd_material_w is not null) and
		(nr_seq_autorizacao_w is not null) then

		/* Procurar o mesmo material */
		select	max(a.nr_sequencia)
		into	nr_sequencia_w
		from	material_autor_cirurgia a
		where	a.nr_seq_autorizacao	= nr_seq_autorizacao_w
		and	a.cd_material		= nvl(cd_material_validacao_w,cd_material_w)
		and	((exists(select	1 from material_autor_cir_cot x
				where	x.nr_sequencia  = a.nr_sequencia
				and	nvl(x.cd_cgc,'X')	<> nvl(cd_cgc_fabricante_w,'X'))) or
			((a.nr_seq_opme is null) or
			(a.nr_seq_opme		= nr_seq_opme_w)));

		--pr_adicional_w	:= obter_tx_comercializacao_conv(cd_convenio_w,cd_estabelecimento_w,sysdate,cd_material_w);
		obter_tx_reducao_acresc_conv(cd_convenio_w,cd_estabelecimento_w,sysdate,cd_material_w,null,null,null,pr_adicional_w,ie_reducao_acrescimo_w);

		if	(nr_sequencia_w is null) then

			if	(nvl(ie_gerar_dados_lote_fornec_w,'N') = 'S') then
				select	max(a.cd_cgc_fornec),
					max(a.nr_seq_marca)
				into	cd_cgc_fornec_w,
					nr_seq_marca_w
				from	material_lote_fornec a,
					material_atend_paciente b,
					material_autorizado c,
					material_marca d
				where	a.nr_sequencia 		= b.nr_seq_lote_fornec
				and	b.nr_seq_mat_autor 	= c.nr_sequencia
				and	c.nr_sequencia		= nr_seq_mat_autor_p
				and	d.nr_sequencia		= a.nr_seq_marca
				and	c.cd_material		= d.cd_material;
			end if;

			select	material_autor_cirurgia_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			insert	into material_autor_cirurgia
				(nr_sequencia,
				nr_seq_autorizacao,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				cd_material,
				qt_solicitada,
				qt_material,
				vl_unitario_material,
				vl_material,
				ie_aprovacao,
				ie_valor_conta,
				ds_observacao,
				nr_seq_opme,
				nr_seq_regra_plano,
				pr_adicional,
				ie_reducao_acrescimo,
				cd_cgc_fornec,
				nr_seq_marca,
				ie_valor_informado,
				nr_prescricao,
				nr_seq_prescricao)
			values	(nr_sequencia_w,
				nr_seq_autorizacao_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				cd_material_w,
				qt_solicitada_w,
				0,
				0,
				0,
				'N',
				ie_valor_conta_w,
				ds_observacao_w,
				nr_seq_opme_w,
				nr_seq_regra_plano_w,
				pr_adicional_w,
				ie_reducao_acrescimo_w,
				cd_cgc_fornec_w,
				nr_seq_marca_w,
				ie_valor_conta_w,
				nr_prescricao_p,
				nr_seq_mat_prescr_p);

			insert into material_autor_cir_cot
				(nr_sequencia,
				cd_cgc,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_aprovacao,
				nr_orcamento,
				vl_unitario_cotado,
				vl_cotado)
			select	nr_sequencia_w,
				a.cd_cgc_fabricante,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				' ',
				a.nr_orcamento,
				nvl(a.vl_cotado,0),
				(nvl(a.vl_cotado,0) * a.qt_solicitada)
			from 	material_autorizado a
			where	a.nr_sequencia	    = nr_seq_mat_autor_p
			and	a.cd_cgc_fabricante is not null
			and	not exists 	(select 1
						from 	material_autor_cir_cot x
						where	x.nr_sequencia					= nr_sequencia_w
						and	x.cd_cgc					= a.cd_cgc_fabricante
						and	nvl(x.nr_orcamento,nvl(a.nr_orcamento,'X'))	= nvl(a.nr_orcamento,'X'));

		else

			update	material_autor_cirurgia
			set	qt_solicitada		= qt_solicitada_w,
				dt_atualizacao		= sysdate,
				nm_usuario 		= nm_usuario_p
			where	nr_seq_autorizacao 	= nr_seq_autorizacao_w
			and	nr_sequencia		= nr_sequencia_w;
			
			
			update	material_autor_cir_cot
			set	vl_unitario_cotado	= nvl(vl_cotado_w,0),
				vl_cotado		= nvl(vl_cotado_w * qt_solicitada_w,0),
				dt_atualizacao		= sysdate,
				nm_usuario 		= nm_usuario_p
			where	nr_sequencia 		= nr_sequencia_w
			and	nvl(cd_cgc,'X')	= nvl(cd_cgc_fabricante_w,'X');			
			

			insert into material_autor_cir_cot
				(nr_sequencia,
				cd_cgc,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_aprovacao,
				nr_orcamento,
				vl_unitario_cotado,
				vl_cotado)
			select	nr_sequencia_w,
				a.cd_cgc_fabricante,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				' ',
				a.nr_orcamento,
				nvl(a.vl_cotado,0),
				(nvl(a.vl_cotado,0) * a.qt_solicitada)			
			from 	material_autorizado a
			where	a.nr_sequencia	    = nr_seq_mat_autor_p
			and	a.cd_cgc_fabricante is not null
			and	not exists 	(select 1
						from 	material_autor_cir_cot x
						where	x.nr_sequencia					= nr_sequencia_w
						and	x.cd_cgc					= a.cd_cgc_fabricante
						and	nvl(x.nr_orcamento,nvl(a.nr_orcamento,'X'))	= nvl(a.nr_orcamento,'X'));			
						
		end if;

		Obter_Param_Usuario(3006,14,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_obter_preco_convenio_w);

		if	(nvl(ie_obter_preco_convenio_w,'N') = 'S') then
			Atualizar_Preco_Mat_Aut_Conv	(nr_sequencia_w,
							null,
							cd_estabelecimento_w,
							nm_usuario_p,
							ds_retorno_w);
		end if;
		
		
		
		if	(nvl(ie_fornec_mat_autor_esp_w,'N') = 'S') then
			gerar_mat_fornec_autor_cot(nr_seq_autorizacao_w,nm_usuario_p);
		end if;
		
	end if;

end if;

end gerar_autor_mat_esp;
/