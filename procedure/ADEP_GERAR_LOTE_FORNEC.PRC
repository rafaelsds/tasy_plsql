create or replace
procedure ADEP_gerar_lote_fornec(nr_lote_producao_p	number,
			dt_validade_p			date,	
			nr_seq_horario_p	number,
			nm_usuario_p		varchar2,
			nr_seq_lote_p	out	number) is

dt_confirmacao_w		date;
cd_material_w		number(06);
cd_estabelecimento_w	number(04);
nr_seq_lote_w		number(10);
qt_material_w		number(13,4);
cd_cgc_w		varchar2(14);
ie_tipo_descricao_w	varchar2(1);
ds_lote_fornec_w	varchar2(20);
nr_prescricao_w		number(15);
nr_seq_material_w	number(15);

begin

select	a.cd_material,
	a.qt_unitaria,
	c.cd_estabelecimento,
	substr(obter_cgc_estabelecimento(c.cd_estabelecimento),1,14),
	nvl(b.dt_horario, sysdate),
	a.nr_prescricao,
	a.nr_sequencia
into	cd_material_w,
	qt_material_w,
	cd_estabelecimento_w,
	cd_cgc_w,
	dt_confirmacao_w,
	nr_prescricao_w,
	nr_seq_material_w
from	prescr_mat_hor b,
	prescr_material a,
	prescr_medica c
where	b.nr_prescricao		= a.nr_prescricao
and 	b.nr_seq_material	= a.nr_sequencia
and 	b.nr_prescricao		= c.nr_prescricao 
and 	a.nr_prescricao		= c.nr_prescricao
and 	b.nr_sequencia		= nr_seq_horario_p;

select	nvl(max(obter_valor_param_usuario(143, 111, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w)),'L')
into	ie_tipo_descricao_w
from	dual;

select	material_lote_fornec_seq.nextval
into	nr_seq_lote_w
from	dual;

ds_lote_fornec_w := substr('Prod'||nr_seq_lote_w,1,20);

if	(ie_tipo_descricao_w = 'P') then
	ds_lote_fornec_w := substr('Prod'||nr_lote_producao_p,1,20);
end if;

insert into material_lote_fornec(
	nr_sequencia,
	cd_material,
	ie_origem_lote,
	nr_digito_verif,
	dt_atualizacao,
	nm_usuario,
	ds_lote_fornec,
	dt_validade,
	cd_cgc_fornec,
	qt_material,
	cd_estabelecimento,
	ie_validade_indeterminada,
	ie_situacao,
	ie_bloqueio,
	nr_lote_producao,
	dt_fabricacao,
	dt_atualizacao_nrec,
	nm_usuario_nrec)
values(	nr_seq_lote_w,
	cd_material_w,
	'L',
	calcula_digito('Modulo11', nr_seq_lote_w),
	sysdate,
	nm_usuario_p,
	ds_lote_fornec_w,
	dt_validade_p,
	cd_cgc_w,
	qt_material_w,
	cd_estabelecimento_w,
	decode(dt_validade_p,null,'S','N'),
	'A',
	'N',
	null,
	dt_confirmacao_w,
	sysdate,
	nm_usuario_p);
	
update	prescr_material
set		nr_seq_lote_fornec	= nr_seq_lote_w
where	nr_prescricao		= nr_prescricao_w
and		nr_sequencia		= nr_seq_material_w;

update	prescr_mat_hor
set		nr_seq_lote_fornec	= nr_seq_lote_w
where	nr_sequencia		= nr_seq_horario_p;

commit;

nr_seq_lote_p	:= nr_seq_lote_w;

end ADEP_gerar_lote_fornec;
/
