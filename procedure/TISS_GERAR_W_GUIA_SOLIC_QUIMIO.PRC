create or replace
procedure TISS_GERAR_W_GUIA_SOLIC_QUIMIO
			(nr_sequencia_autor_p	in number,
			nr_seq_lote_anexo_p	in number,
			nm_usuario_p		in varchar2,
			ds_dir_padrao_p		in varchar2,
			nr_seq_paciente_p in number default -1) is


cd_convenio_w			number(10);
cd_estabelecimento_w		number(10);
cd_ans_w			varchar2(100);
ds_arquivo_logo_w		varchar2(255);
ds_arquivo_logo_comp_w		varchar2(255);
ie_gerar_tiss_w			varchar2(10);
nr_seq_anexo_w			number(10);
nr_seq_guia_w			number(10);
ds_versao_w			varchar2(20);
cd_pessoa_fisica_w		varchar2(10);
cd_item_w			varchar2(10);
ds_item_w			varchar2(150);
cd_edicao_amb_w			varchar2(2);
ie_opcao_fabricante_w		number(1);
qt_solicitada_w			tiss_anexo_guia_item.qt_solicitada%type;
vl_solicitado_w			number(10,2);
nr_registro_anvisa_w		varchar2(15);
cd_ref_fabricante_w		varchar2(30);
nr_autorizacao_func_w		varchar2(30);
qt_item_w			number(10);
dt_provavel_w			date;
qt_dose_dia_w			number(15,2);
ie_via_aplicacao_w		varchar2(5);
nr_sequencia_autor_w		number(10);
cd_unidade_medida_w		varchar2(3);
ds_posicao_w			number(5);
ds_dir_padrao_w			varchar(255);
im_logo_convenio_w		tiss_logo_convenio.im_logo_convenio%type;


cursor c01 is
select	b.cd_item,
	b.ds_item,
	b.cd_edicao_amb,
	b.ie_opcao_fabricante,
	b.qt_solicitada,
	b.qt_dose_dia,
	b.dt_provavel,
	b.ie_via_aplicacao,
	b.cd_unidade_medida
from	tiss_anexo_guia a,
	tiss_anexo_guia_item b
where	a.nr_sequencia		= b.nr_seq_guia
and	a.nr_sequencia		= nr_seq_anexo_w
and	a.ie_tiss_tipo_anexo	= 2; --Quimio

cursor c02 is
select	a.nr_sequencia,
	a.nr_sequencia_autor
from	tiss_anexo_guia a
where	a.nr_sequencia_autor	= nr_sequencia_autor_p
and	a.ie_tiss_tipo_anexo	= 2
union
select	a.nr_sequencia,
	a.nr_sequencia_autor
from	tiss_anexo_guia a
where	a.nr_seq_lote_anexo	= nr_seq_lote_anexo_p
and	a.ie_tiss_tipo_anexo	= 2
union
select	a.nr_sequencia,
	a.nr_sequencia_autor
from	tiss_anexo_guia a,
	autorizacao_convenio b
where	a.nr_sequencia_autor	= b.nr_sequencia
and	b.nr_seq_paciente_setor = nr_seq_paciente_p
and	a.ie_tiss_tipo_anexo	= 2
and	nvl(nr_sequencia_autor_p,-1) = -1
and	nvl(nr_seq_lote_anexo_p,-1) = -1;

Cursor C03 is
	select	nr_sequencia
	from	autorizacao_convenio
	where	nr_seq_paciente_setor = nr_seq_paciente_p
	order by 1;

begin

delete	from w_tiss_relatorio
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_guia
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_beneficiario
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_solicitacao
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_contratado_solic
where	nm_usuario		= nm_usuario_p;

delete	from w_tiss_opm
where	nm_usuario		= nm_usuario_p;

commit;

if	(nvl(nr_sequencia_autor_p,-1) > 0) or
	(nvl(nr_seq_lote_anexo_p,-1) > 0) or
	(Nvl(nr_seq_paciente_p,-1) > 0) then

	if	(nvl(nr_sequencia_autor_p,-1) > 0) then

		tiss_atualizar_autorizacao(nr_sequencia_autor_p,nm_usuario_p);

		select	max(a.cd_convenio),
			max(a.cd_estabelecimento),
			max(c.cd_ans),
			max(b.ds_arquivo_logo_tiss)
		into	cd_convenio_w,
			cd_estabelecimento_w,
			cd_ans_w,
			ds_arquivo_logo_w
		from	autorizacao_convenio a,
			convenio b,
			pessoa_juridica c
		where	b.cd_convenio	= a.cd_convenio
		and	b.cd_cgc	= c.cd_cgc
		and	a.nr_sequencia	= nr_sequencia_autor_p;

	elsif	(nvl(nr_seq_lote_anexo_p,-1) > 0) then

		select	max(a.cd_convenio),
			max(a.cd_estabelecimento),
			max(c.cd_ans),
			max(b.ds_arquivo_logo_tiss)
		into	cd_convenio_w,
			cd_estabelecimento_w,
			cd_ans_w,
			ds_arquivo_logo_w
		from	tiss_anexo_lote a,
			convenio b,
			pessoa_juridica c
		where	b.cd_convenio	= a.cd_convenio
		and	b.cd_cgc	= c.cd_cgc
		and	a.nr_sequencia	= nr_seq_lote_anexo_p;
		
	elsif	(Nvl(nr_seq_paciente_p,-1) > 0) then
		
		
		select	max(a.cd_convenio),
			max(ps.cd_estabelecimento),
			max(c.cd_ans),
			max(b.ds_arquivo_logo_tiss)
		into	cd_convenio_w,
			cd_estabelecimento_w,
			cd_ans_w,
			ds_arquivo_logo_w
		from	paciente_setor_convenio a,
			paciente_setor ps,
			convenio b,
			pessoa_juridica c
		where	b.cd_convenio	= a.cd_convenio
		and	a.nr_seq_paciente = ps.nr_Seq_paciente
		and	b.cd_cgc	= c.cd_cgc
		and	a.nr_seq_paciente = nr_seq_paciente_p;
		
		
		
		nr_sequencia_autor_w := null;
		open C03;
		loop
		fetch C03 into	
			nr_sequencia_autor_w;
		exit when C03%notfound;
			
			tiss_atualizar_autorizacao(nr_sequencia_autor_w,nm_usuario_p);
			
		end loop;
		close C03;
		
		
	end if;
	
	

	select	max(ds_arquivo_logo_comp),
		nvl(max(ie_gerar_tiss), 'S')
	into	ds_arquivo_logo_comp_w,
		ie_gerar_tiss_w
	from	tiss_parametros_convenio
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_convenio		= cd_convenio_w;

	select	tiss_obter_versao(cd_convenio_w,cd_estabelecimento_w)
	into	ds_versao_w
	from	dual;

begin
	select	im_logo_convenio
	into	im_logo_convenio_w
	from	tiss_logo_convenio
	where	cd_convenio	   = cd_convenio_w
	and 	nvl(ie_situacao,'N') = 'A';
exception
when others then
	im_logo_convenio_w := null;
end;
		
if	(im_logo_convenio_w is null) and
	(ds_arquivo_logo_w is not null) then
	ds_arquivo_logo_w := tiss_diretorio_logo(ds_arquivo_logo_comp_w, ds_dir_padrao_p) || ds_arquivo_logo_w;
end if;		
	
	if 	(nvl(ie_gerar_tiss_w,'S') = 'S') then

		if	(ds_arquivo_logo_w is not null) and 
			(im_logo_convenio_w is null) then
			insert	into w_tiss_relatorio
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_arquivo_logo,
				im_logo_convenio)
			values	(w_tiss_relatorio_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_logo_w,
				null);
		else
			insert	into w_tiss_relatorio
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_arquivo_logo,
				im_logo_convenio)
			values	(w_tiss_relatorio_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				null,
				im_logo_convenio_w);
		end if;
		nr_sequencia_autor_w := null;
		open C02;
		loop
		fetch C02 into
			nr_seq_anexo_w,
			nr_sequencia_autor_w;
		exit when C02%notfound;
			begin
			
			qt_item_w	:= 0;

			open C01;
			loop
			fetch C01 into
				cd_item_w,
				ds_item_w,
				cd_edicao_amb_w,
				ie_opcao_fabricante_w,
				qt_solicitada_w,
				qt_dose_dia_w,
				dt_provavel_w,
				ie_via_aplicacao_w,
				cd_unidade_medida_w;
			exit when C01%notfound;
				begin
				
				if	(qt_item_w = 0) then
					TISS_GERAR_CABEC_SOLIC_QUIMIO( nr_sequencia_autor_w, nr_seq_anexo_w, nm_usuario_p, nr_seq_guia_w);
				end if;

				qt_item_w	:= qt_item_w + 1;

				insert into w_tiss_opm
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_guia,
					cd_opm,
					cd_edicao,
					ds_opm,
					qt_solicitada,
					nr_seq_apresentacao,
					dt_prevista,
					qt_frequencia,
					ie_via_aplicacao,
					cd_unidade_medida)
				values	(w_tiss_opm_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_guia_w,
					cd_item_w,
					cd_edicao_amb_w,
					ds_item_w,
					Trunc(qt_solicitada_w,2),
					qt_item_w,
					dt_provavel_w,
					qt_dose_dia_w,
					ie_via_aplicacao_w,
					cd_unidade_medida_w);

				if	(qt_item_w =8) then
					qt_item_w 	:= 0;
					TISS_COMPLETAR_GUIA(nr_seq_guia_w, nm_usuario_p);
				end if;

				end;
			end loop;
			close C01;

			TISS_COMPLETAR_GUIA(nr_seq_guia_w, nm_usuario_p);

			end;
		end loop;
		close C02;

	end if;
end if;

commit;

end TISS_GERAR_W_GUIA_SOLIC_QUIMIO;
/
