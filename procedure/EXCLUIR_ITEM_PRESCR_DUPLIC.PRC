create or replace
procedure Excluir_item_prescr_duplic(	nr_prescricao_p		number,
					nm_usuario_p		Varchar2) is 

cont_w			number(15,0);
nr_sequencia_w		number(15,0);
cd_material_w		number(15,0);
cd_intervalo_w		varchar2(10);
qt_dose_w		number(18,6);
cd_unidade_medida_dose_w	varchar2(30);
ie_via_aplicacao_w		varchar2(10);
ie_agrupador_w		number(10,0);

cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
qt_procedimento_w	number(8,3);
ie_lado_w		varchar2(10);
nr_seq_prot_glic_w	number(10,0);

cd_dieta_w		number(10,0);
qt_parametro_w		number(15,4);

ds_solucao_w		varchar2(255);
ds_componentes_w	varchar2(2000);
ie_bomba_infusao_w	varchar2(10);
nr_etapas_w		number(10,0);
qt_hora_fase_w		number(15,4);
qt_tempo_aplicacao_w	number(15,4);
nr_seq_dialise_w	number(15);
hr_prim_horario_w	varchar2(10);
ie_tipo_dosagem_w	varchar2(10);
qt_dosagem_w		number(18,6);
nr_seq_proc_interno_w	number(15,0);
cd_recomendacao_w		number(10,0);
IE_TIPO_SOLUCAO_W		varchar2(15);
ds_medic_nao_padrao_w 	varchar2(255);

cursor c01 is
select	nr_sequencia,
	cd_material,
	nvl(cd_intervalo,'XPTO'),
	qt_dose,
	cd_unidade_medida_dose,
	nvl(ie_via_aplicacao,'XPTO'),
	ie_agrupador,
	nvl(ds_medic_nao_padrao, '')
from	prescr_material
where	nr_prescricao		= nr_prescricao_p
and	nr_sequencia_solucao	is null
and	nr_sequencia_proc		is null
and	nr_seq_kit		is null
and	ie_agrupador not in (3,7,9)
order by cd_material;

cursor c02 is
select	cd_procedimento,
	ie_origem_proced,
	nvl(nr_seq_proc_interno,0),
	nr_sequencia,
	qt_procedimento,
	nvl(cd_intervalo,'XPTO'),
	nvl(ie_lado,'XPTO'),
	nvl(nr_seq_prot_glic,0)
from	prescr_procedimento
where	nr_prescricao		= nr_prescricao_p
order by dt_prev_execucao desc;

cursor c03 is
select	cd_dieta,
	nr_sequencia,
	nvl(cd_intervalo,'XPTO'),
	nvl(qt_parametro,0)
from	prescr_dieta
where	nr_prescricao		= nr_prescricao_p;

cursor c04 is
select	nvl(ds_solucao,'X'),
	nr_seq_solucao,
	nvl(ie_bomba_infusao,'X'),
	nvl(nr_etapas,0),
	nvl(qt_hora_fase,0),
	nvl(qt_tempo_aplicacao,0),
	nvl(hr_prim_horario,'X'),
	ie_tipo_dosagem,
	ie_via_aplicacao,
	nvl(qt_dosagem,0),
	substr(obter_componentes_sol(nr_prescricao, nr_seq_solucao, 'N', nm_usuario, 1),1,2000),
	NVL(IE_TIPO_SOLUCAO,'XPTO')
from	prescr_solucao
where	nr_prescricao	= nr_prescricao_p;

cursor c05 is
select	cd_recomendacao,
	nr_sequencia,
	nvl(hr_prim_horario,'00:00'),
	nvl(cd_intervalo,'XPTO')
from	prescr_recomendacao
where	nr_prescricao	= nr_prescricao_p;

begin

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	cd_material_w,
	cd_intervalo_w,
	qt_dose_w,
	cd_unidade_medida_dose_w,
	ie_via_aplicacao_w,
	ie_agrupador_w,
	ds_medic_nao_padrao_w;
exit when C01%notfound;

	select	count(*)
	into	cont_w
	from	prescr_material
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		<> nr_sequencia_w
	and	cd_material		= cd_material_w
	and	qt_dose			= qt_dose_w
	and	cd_unidade_medida_dose	= cd_unidade_medida_dose_w
	and	ie_agrupador		= ie_agrupador_w
	and	nvl(cd_intervalo, cd_intervalo_w)	 = cd_intervalo_w
	and	nvl(ie_via_aplicacao,ie_via_aplicacao_w) = ie_via_aplicacao_w
	and nvl(ds_medic_nao_padrao, 'XPTO') = nvl(ds_medic_nao_padrao_w, 'XPTO');

	if	(cont_w	> 0) then
		delete from prescr_material
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_sequencia_w;
	end if;
	
end loop;
close C01;

reordenar_medicamento(nr_prescricao_p);

cont_w	:= 0;

open C02;
loop
fetch C02 into	
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_proc_interno_w,
	nr_sequencia_w,
	qt_procedimento_w,
	cd_intervalo_w,
	ie_lado_w,
	nr_seq_prot_glic_w;
exit when C02%notfound;



	select	count(*)
	into	cont_w
	from	prescr_procedimento
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		<> nr_sequencia_w
	and	cd_procedimento		= cd_procedimento_w
	and	qt_procedimento		= qt_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w
	and	nvl(nr_seq_proc_interno, nr_seq_proc_interno_w) = nr_seq_proc_interno_w
	and	nvl(cd_intervalo, cd_intervalo_w) = cd_intervalo_w
	and	nvl(ie_lado, ie_lado_w)	= ie_lado_w
	and	nvl(nr_seq_prot_glic,0) = nr_seq_prot_glic_w;

	if	(cont_w	> 0) then
		delete from prescr_procedimento
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_sequencia_w;
	end if;
end loop;
close C02;

reordenar_procedimento(nr_prescricao_p);

open C03;
loop
fetch C03 into	
	cd_dieta_w,
	nr_sequencia_w,
	cd_intervalo_w,
	qt_parametro_w;
exit when C03%notfound;
	select	count(*)
	into	cont_w
	from	prescr_dieta
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		<> nr_sequencia_w
	and	cd_dieta		= cd_dieta_w
	and	nvl(qt_parametro,0)	= qt_parametro_w
	and	nvl(cd_intervalo, cd_intervalo_w) = cd_intervalo_w;

	if	(cont_w	> 0) then
		delete from prescr_dieta
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_sequencia_w;
	end if;
end loop;
close C03;

open C04;
loop
fetch C04 into	
	ds_solucao_w,
	nr_sequencia_w,
	ie_bomba_infusao_w,
	nr_etapas_w,
	qt_hora_fase_w,
	qt_tempo_aplicacao_w,
	hr_prim_horario_w,
	ie_tipo_dosagem_w,
	ie_via_aplicacao_w,
	qt_dosagem_w,
	ds_componentes_w,
	IE_TIPO_SOLUCAO_w;
exit when C04%notfound;

	select	count(*)
	into	cont_w
	from	prescr_solucao
	where	nr_prescricao		= nr_prescricao_p
	and	nr_seq_solucao		<> nr_sequencia_w
	and	nvl(ds_solucao, ds_solucao_w)			= ds_solucao_w
	and	nvl(ie_bomba_infusao, ie_bomba_infusao_w) 	= ie_bomba_infusao_w
	and	nvl(nr_etapas, nr_etapas_w)			= nr_etapas_w
	and	nvl(qt_hora_fase, qt_hora_fase_w) 		= qt_hora_fase_w
	and	nvl(qt_tempo_aplicacao, qt_tempo_aplicacao_w)	= qt_tempo_aplicacao_w
	and	nvl(hr_prim_horario, hr_prim_horario_w)		= hr_prim_horario_w
	and	nvl(ie_tipo_dosagem, ie_tipo_dosagem_w)		= ie_tipo_dosagem_w
	and	nvl(ie_via_aplicacao, ie_via_aplicacao_w)	= ie_via_aplicacao_w
	and	nvl(qt_dosagem, qt_dosagem_w)			= qt_dosagem_w
	and	nvl(IE_TIPO_SOLUCAO, IE_TIPO_SOLUCAO_w) = IE_TIPO_SOLUCAO_w
	and	nvl(substr(obter_componentes_sol(nr_prescricao, nr_seq_solucao, 'N', nm_usuario, 1),1,2000), ds_componentes_w)	= ds_componentes_w;

	if	(cont_w	> 0) then
		delete from prescr_solucao
		where	nr_prescricao	= nr_prescricao_p
		and	nr_seq_solucao	= nr_sequencia_w;
	end if;
end loop;
close C04;

reordenar_solucoes(nr_prescricao_p);

open C05;
loop
fetch C05 into	
	cd_recomendacao_w,
	nr_sequencia_w,
	hr_prim_horario_w,
	cd_intervalo_w;
exit when C05%notfound;
	cont_w	:= 0;
	select	count(*)
	into	cont_w
	from	prescr_recomendacao
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		<> nr_sequencia_w
	and	cd_recomendacao		= cd_recomendacao_w
	and	nvl(hr_prim_horario, hr_prim_horario_w)	= hr_prim_horario_w
	and	nvl(cd_intervalo, cd_intervalo_w)	= cd_intervalo_w;
	
	if	(cont_w	> 0) then
		delete from prescr_recomendacao
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_sequencia_w;
	end if;
end loop;
close C05;


reordenar_recomendacao(nr_prescricao_p);

commit;

end Excluir_item_prescr_duplic;
/