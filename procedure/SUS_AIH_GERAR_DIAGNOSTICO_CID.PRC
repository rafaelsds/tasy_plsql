create or replace
procedure Sus_AIH_Gerar_Diagnostico_CID(
			nr_atendimento_p	Number,
			cd_cid_p		Varchar2,
			nm_usuario_p		Varchar2) is 
			
ie_tipo_atendimento_w		Number(3);
cd_estabelecimento_w		Number(4);
cd_perfil_w			Number(5);
ie_tipo_diagnostico_w		Number(3);
cd_medico_w			Varchar2(10);
qt_diag_doenca_w		Number(5)	:= 0;
ie_classificacao_doenca_w	Varchar2(1);
dt_diagnostico_w		Date;

begin

select	ie_tipo_atendimento,
	cd_estabelecimento
into	ie_tipo_atendimento_w,
	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

cd_perfil_w	:= obter_perfil_ativo;
Obter_Param_Usuario(916, 83, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_tipo_diagnostico_w);

cd_medico_w	:= obter_medico_para_diag(nr_atendimento_p, ie_tipo_atendimento_w);

dt_diagnostico_w	:= sysdate;

insert into diagnostico_medico (	
	nr_atendimento,    
	dt_diagnostico,
	ie_tipo_diagnostico,
	cd_medico,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_atendimento)
values(	nr_atendimento_p,    
	dt_diagnostico_w,
	nvl(ie_tipo_diagnostico_w,0),
	cd_medico_w,
	sysdate,
	nm_usuario_p,
	ie_tipo_atendimento_w);

select	count(*)
into	qt_diag_doenca_w
from	diagnostico_doenca
where	nr_atendimento	= nr_atendimento_p;

if	(qt_diag_doenca_w	> 0) then
	begin
	ie_classificacao_doenca_w	:= 'S';
	end;
else
	begin
	ie_classificacao_doenca_w	:= 'P';
	end;
end if;

insert into diagnostico_doenca (	
	nr_atendimento,
	dt_diagnostico,
	cd_doenca,
	dt_atualizacao,
	nm_usuario,
	ie_classificacao_doenca,
	IE_TIPO_DIAGNOSTICO)
values(	nr_atendimento_p,
	dt_diagnostico_w,
	cd_cid_p,
	sysdate,
	nm_usuario_p,
	ie_classificacao_doenca_w,
	2);

commit;

end Sus_AIH_Gerar_Diagnostico_CID;
/