create or replace
Procedure gerar_itens_nota_req_material(	nr_requisicao_p		number,
					nr_seq_nota_p		number,
					cd_local_estoque_p	number,
					nm_usuario_p		varchar2) is

cd_estab_w			number(5);
cd_oper_estoque_w		number(3);
cd_setor_atend_w			number(5);
cd_local_estoque_w		number(4);
cd_centro_custo_w			number(8);
cd_material_w			number(6);
ie_tipo_conta_w			number(1);
cd_conta_contabil_w		varchar2(20) := '';
nr_sequencia_w			number(10);
nr_seq_lote_fornec_w		number(10);
cd_cgc_fornec_w			varchar2(14);
cd_unid_med_consumo_w		varchar2(30);
cd_unid_med_estoque_w		varchar2(30);
qt_conv_estoque_consumo_w 	number(13,4);
qt_item_estoque_w			number(13,4);
ie_baixa_estoq_pac_w		varchar2(1);
ie_consignado_w			varchar2(1);
dt_solicitacao_requisicao_w		date;
ds_consistencia_w			varchar2(255);

cursor c01 is
select	cd_material,
	cd_unidade_medida_estoque,
	obter_quantidade_convertida(cd_material, qt_item_nf, cd_unidade_medida_compra, 'UME'),
	cd_cgc_emitente,
	nr_seq_lote_fornec
from	nota_fiscal_item
where	nr_sequencia = nr_seq_nota_p
and	(nvl(cd_local_estoque_p,0) = 0 or cd_local_estoque = cd_local_estoque_p)
and	cd_material is not null;

begin

open C01;
loop
fetch C01 into
	cd_material_w,
	cd_unid_med_estoque_w,
	qt_item_estoque_w,
	cd_cgc_fornec_w,
	nr_seq_lote_fornec_w;
exit when C01%notfound;
	begin
	
	select	cd_estabelecimento,
		cd_operacao_estoque,
		cd_setor_atendimento,
		cd_local_estoque,
		cd_centro_custo,
		dt_solicitacao_requisicao
	into	cd_estab_w,
		cd_oper_estoque_w,
		cd_setor_atend_w,
		cd_local_estoque_w,
		cd_centro_custo_w,
		dt_solicitacao_requisicao_w
	from	requisicao_material
	where	nr_requisicao = nr_requisicao_p;

	select	decode(ie_tipo_requisicao,2,2,3)
	into	ie_tipo_conta_w
	from	operacao_estoque
	where	cd_operacao_estoque = cd_oper_estoque_w;

	define_conta_material(
			cd_estab_w,
			cd_material_w,
			ie_tipo_conta_w, 0,
			cd_setor_atend_w, '0', 0, 0, 0, 0,
			cd_local_estoque_w,
			Null,
			dt_solicitacao_requisicao_w,
			cd_conta_contabil_w,
			cd_centro_custo_w,
			null);

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_sequencia_w
	from	item_requisicao_material
	where	nr_requisicao = nr_requisicao_p;

	select	substr(obter_dados_material_estab(cd_material,cd_estab_w,'UMS'),1,30) cd_unidade_medida_consumo,
		substr(obter_dados_material_estab(cd_material,cd_estab_w,'UME'),1,30) cd_unidade_medida_estoque,
		qt_conv_estoque_consumo,
		/*ie_baixa_estoq_pac Fabio - 23/06/2004 Alterei para buscar da function*/
		substr(obter_material_baixa_estoq_pac(cd_estab_w, 0, cd_material),1,1)
	into	cd_unid_med_consumo_w,
		cd_unid_med_estoque_w,
		qt_conv_estoque_consumo_w,
		ie_baixa_estoq_pac_w
	from	material
	where	cd_material = cd_material_w;
		
	select ie_consignado
	into	ie_consignado_w
	from	material
	where	cd_material = cd_material_w;

	if	(ie_consignado_w <> 1) then
		cd_cgc_fornec_w := null;
	end if;		

	insert into item_requisicao_material(
		nr_requisicao,
		nr_sequencia,
		cd_estabelecimento,
		cd_material,
		qt_material_requisitada,
		qt_material_atendida,
		vl_material,
		dt_atualizacao,
		nm_usuario,
		cd_unidade_medida,
		dt_atendimento,
		cd_pessoa_recebe,
		cd_pessoa_atende,
		ie_acao,
		cd_motivo_baixa,
		qt_estoque,
		cd_unidade_medida_estoque,
		cd_conta_contabil,
		cd_material_req,
		nr_seq_lote_fornec,
		cd_cgc_fornecedor,
		ds_observacao,
		ie_geracao,
		nr_seq_nota_fiscal)
	values(	nr_requisicao_p,
		nr_sequencia_w,
		cd_estab_w,
		cd_material_w,
		(qt_item_estoque_w * qt_conv_estoque_consumo_w),
		0,
		0,
		sysdate,
		nm_usuario_p,
		cd_unid_med_consumo_w,
		null,
		null,
		null,
		'1',
		0,
		qt_item_estoque_w,
		cd_unid_med_estoque_w,
		cd_conta_contabil_w,
		cd_material_w,
		decode(nr_seq_lote_fornec_w,0,null),
		cd_cgc_fornec_w,
		null,
		null,
		nr_seq_nota_p);
	end;
end loop;
close C01;

commit;

end gerar_itens_nota_req_material;
/