create or replace function obter_seq_cpoe_evento(
    nr_prescricao_p number, 
    nr_seq_item_p number,
    ie_tipo_item_p varchar2)
  return number
is
  ie_retorno_w prescr_material.nr_seq_mat_cpoe%type := 0;
begin
  
  if ((nr_prescricao_p is not null) and (nr_seq_item_p is not null) and (ie_tipo_item_p IN ('M', 'IAH'))) then

    select nvl(max(a.nr_seq_mat_cpoe), 0)
      into ie_retorno_w
      from prescr_material a
     where a.nr_prescricao = nr_prescricao_p
       and a.nr_sequencia = nr_seq_item_p;  
  
  end if;
  
return ie_retorno_w;

end obter_seq_cpoe_evento;
/