create or replace
procedure fa_registrar_data_entrega ( 	nr_seq_paciente_p	number,
					nm_usuario_p		Varchar2) is 

begin

if (nr_seq_paciente_p is not null) then

	update 	fa_paciente_entrega
	set	dt_entrega		= sysdate,
		nm_usuario_entrega	= nm_usuario_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_paciente_p;
end if;

commit;

end fa_registrar_data_entrega;
/
