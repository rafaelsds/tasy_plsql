create or replace
procedure pls_exluir_item_analise
			(	ds_sequencia_itens_p	Varchar2,
				cd_estabalecimento_p	number,
				nm_usuario_p		Varchar2	) is 

/*Utilizar a rotina pls_excluir_item_analise*/
				
ds_sequencia_itens_w		varchar2(4000);
nr_seq_item_w			number(10);
nr_seq_item_ww			number(10);
ie_tipo_item_w			varchar2(1);
nr_seq_proc_w			number(10);
nr_seq_mat_w			number(10);
nr_seq_analise_conta_item_w	number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_analise_conta_item
	where	((nr_seq_conta_proc = nr_seq_proc_w)
	or	 (nr_seq_conta_mat  = nr_seq_mat_w))
	order by 1;
				
begin

ds_sequencia_itens_w := ds_sequencia_itens_p;

while	(instr(ds_sequencia_itens_w,',') <> 0) loop
	begin			
	
	--obtem-se a primeira regra
	nr_seq_item_w		:= substr(ds_sequencia_itens_w,1,instr(ds_sequencia_itens_w,',')-1);
	--remove-se essa do conjunto	
	ds_sequencia_itens_w	:= substr(ds_sequencia_itens_w,instr(ds_sequencia_itens_w,',')+1,255);
	
	select	nr_seq_item,
		ie_tipo_item
	into	nr_seq_item_ww,
		ie_tipo_item_w
	from	w_pls_resumo_conta
	where	nr_sequencia = nr_seq_item_w;
	
	if	(ie_tipo_item_w = 'P') then
		nr_seq_proc_w	:= nr_seq_item_ww;
		nr_seq_mat_w	:= null;
	elsif	(ie_tipo_item_w = 'M') then
		nr_seq_mat_w 	:= nr_seq_item_ww;
		nr_seq_proc_w	:= null;
	end if;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_analise_conta_item_w;
	exit when C01%notfound;
		begin
		
		delete	pls_analise_parecer_item
		where	nr_seq_item = nr_seq_analise_conta_item_w;
		
		end;
	end loop;
	close C01;
	
	delete	pls_analise_conta_item
	where	((nr_seq_conta_proc = nr_seq_proc_w)
	or	 (nr_seq_conta_mat  = nr_seq_mat_w));	
		
	delete	w_pls_resumo_conta
	where	nr_seq_item = nr_seq_item_ww;
	
	delete	pls_ocorrencia_benef	
	where	nr_seq_guia_plano is null
	and	nr_seq_requisicao is null
	and	((nr_seq_proc = nr_seq_proc_w)
	or	 (nr_seq_mat  = nr_seq_mat_w));
	
	delete	pls_conta_glosa
	where	((nr_seq_conta_proc = nr_seq_proc_w)
	or	 (nr_seq_conta_mat  = nr_seq_mat_w));
	
	delete	pls_proc_participante
	where	nr_seq_conta_proc  = nr_seq_proc_w;
	
	delete	pls_conta_proc
	where	nr_sequencia =  nr_seq_proc_w;
	
	delete	pls_conta_mat
	where	nr_sequencia =  nr_seq_mat_w;
	
	end;	
end loop;

commit;

end pls_exluir_item_analise;
/