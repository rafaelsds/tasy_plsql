create or replace
procedure gerar_solic_parecer_cot_compra(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 

nr_cot_compra_w			number(10);
dt_historico_w			date;
ds_titulo_w			varchar2(80);
ds_historico_w			varchar2(4000);
ie_origem_w			varchar2(1);
ie_tipo_w				varchar2(1);
dt_liberacao_w			date;
nm_usuario_lib_w 			varchar2(15);
ds_usuarios_envio_comunic_w	varchar2(255);
nm_usuario_w			varchar2(15);
nr_seq_hist_w			number(10);

Cursor C01 is
	select	b.nm_usuario
	from	cot_compra_hist a,
		usuario b
	where	b.nm_usuario <> a.nm_usuario_lib
	and	a.ds_usuarios_envio_comunic is not null
	and	substr(obter_se_contido_char(b.nm_usuario,replace(a.ds_usuarios_envio_comunic,' ',null)),1,1) = 'S'
	and	a.nr_sequencia = nr_sequencia_p
	and	not exists (	select 	1 
				from 	cot_compra_hist x 
				where 	x.nr_seq_superior = a.nr_sequencia 
				and 	x.ds_usuarios_envio_comunic like (b.nm_usuario)
				and	x.ie_evento = 'E')
	group by b.nm_usuario;
begin
select	nr_cot_compra,
	dt_historico,
	ds_titulo,
	ds_historico,
	ie_origem,
	ie_tipo,
	dt_liberacao,
	ds_usuarios_envio_comunic,
	nm_usuario_lib
into	nr_cot_compra_w,
	dt_historico_w,
	ds_titulo_w,
	ds_historico_w,
	ie_origem_w,
	ie_tipo_w,
	dt_liberacao_w,
	ds_usuarios_envio_comunic_w,
	nm_usuario_lib_w
from	cot_compra_hist
where	nr_sequencia = nr_sequencia_p;

ds_historico_w := substr(WHEB_MENSAGEM_PCK.get_texto(302224,'NR_COT_COMPRA_W='|| NR_COT_COMPRA_W ||';DS_HISTORICO_W='|| DS_HISTORICO_W),1,4000);
			/*Solicita��o de parecer cota��o de compra #@NR_COT_COMPRA_W#@. #@DS_HISTORICO_W#@*/

open C01;
loop
fetch C01 into	
	nm_usuario_w;
exit when C01%notfound;
	begin

	select	cot_compra_hist_seq.nextval
	into	nr_seq_hist_w
	from	dual;

	insert into cot_compra_hist(
		nr_sequencia,
		nr_cot_compra,
		dt_historico,
		ds_titulo,
		ds_historico,
		ie_origem,
		ie_tipo,
		dt_liberacao,
		ds_usuarios_envio_comunic,
		nm_usuario_nrec,
		nm_usuario,
		nm_usuario_lib,
		dt_atualizacao_nrec,
		dt_atualizacao,
		nr_seq_superior,
		ie_evento)
	values (nr_seq_hist_w,
		nr_cot_compra_w,
		dt_historico_w,
		ds_titulo_w,
		ds_historico_w,
		ie_origem_w,
		ie_tipo_w,
		dt_liberacao_w,
		nm_usuario_w,
		nm_usuario_p,
		nm_usuario_p,
		nm_usuario_lib_w,
		sysdate,
		sysdate,
		nr_sequencia_p,
		'E');

	insert into cot_compra_anexo (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_cot_compra,
		ds_arquivo,
		ie_enviar,
		ie_origem,
		nr_seq_parecer)
	select	cot_compra_anexo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_cot_compra,
		ds_arquivo,
		'S',
		'U',
		nr_seq_hist_w
	from	cot_compra_anexo
	where	nr_cot_compra  = nr_cot_compra_w
	and	nr_seq_parecer = nr_sequencia_p;

	end;
end loop;
close C01;

commit;
end gerar_solic_parecer_cot_compra;
/
