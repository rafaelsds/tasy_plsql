create or replace
procedure ptu_alterar_tipo_compart_benef
		(	nr_seq_benef_p			ptu_intercambio_benef.nr_sequencia%type,
			ie_tipo_compartilhamento_p	ptu_intercambio_benef.ie_tipo_compartilhamento%type,
			nm_usuario_p			usuario.nm_usuario%type) is
begin

update	ptu_intercambio_benef
set	ie_tipo_compartilhamento	= ie_tipo_compartilhamento_p,
	nm_usuario			= nm_usuario_p,
	dt_atualizacao			= sysdate
where	nr_sequencia			= nr_seq_benef_p;

commit;

end ptu_alterar_tipo_compart_benef;
/