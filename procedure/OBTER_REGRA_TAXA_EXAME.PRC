CREATE OR REPLACE
PROCEDURE Obter_regra_taxa_Exame(
				cd_estabelecimento_p		Number,
				cd_convenio_p			Number,
				cd_categoria_p			varchar2,
				cd_procedimento_p		Number,
				ie_origem_proced_p		Number,
				nr_seq_proc_princ_p		Number,
				nr_seq_proc_interno_p		number,
				ie_criterio_taxa_p	Out	Number,
				tx_proc_resultante_p	Out	Number,
				ie_regra_filme_p	Out	Varchar2) IS

cd_grupo_proc_w		number(15);
cd_especialidade_w	number(15);
cd_area_procedimento_w	number(15);
				
BEGIN

begin
select 	a.cd_area_procedimento,
	a.cd_especialidade,
	a.cd_grupo_proc
into	cd_area_procedimento_w,
	cd_especialidade_w,
	cd_grupo_proc_w
from 	estrutura_procedimento_v a,
	procedimento_paciente	 b
where 	a.cd_procedimento = b.cd_procedimento
and 	a.ie_origem_proced = b.ie_origem_proced
and 	b.nr_sequencia = nr_seq_proc_princ_p;
exception
	when others then
	cd_area_procedimento_w	:= null;
	cd_especialidade_w	:= null;
	cd_grupo_proc_w		:= null;
end;


Begin
select
	nvl(a.tx_procedimento,1),
	nvl(a.ie_criterio_taxa, 1),
	nvl(ie_regra_filme,'P')
into 	tx_proc_resultante_p,
	ie_criterio_taxa_p,
	ie_regra_filme_p	
from	procedimento_paciente b,
     	convenio_taxa_exame a
where	a.cd_estabelecimento	= cd_estabelecimento_p
and 	a.cd_convenio		= cd_convenio_p
and 	a.cd_categoria		= cd_categoria_p
and	a.cd_taxa_exame		= cd_procedimento_p
and	a.ie_origem_proced	= ie_origem_proced_p
--and   	a.cd_procedimento		= b.cd_procedimento
--and   	a.ie_origem_proced		= b.ie_origem_proced
and 	nvl(a.cd_procedimento, b.cd_procedimento) = b.cd_procedimento
and 	nvl(a.ie_origem_proced, b.ie_origem_proced) = b.ie_origem_proced
and 	nvl(a.cd_area_procedimento, cd_area_procedimento_w) = cd_area_procedimento_w
and 	nvl(a.cd_especialidade, cd_especialidade_w) = cd_especialidade_w
and 	nvl(a.cd_grupo_proc, cd_grupo_proc_w) = cd_grupo_proc_w
and 	nvl(a.nr_seq_proc_interno, nvl(nr_seq_proc_interno_p,0)) = nvl(nr_seq_proc_interno_p,0)
and   	b.nr_sequencia		= nr_seq_proc_princ_p
and   	nr_seq_proc_princ_p	<> 0
and	nvl(a.ie_situacao, 'A') = 'A';
exception
	when others then
	begin
	select
		nvl(max(a.tx_procedimento),1),
		nvl(max(a.ie_criterio_taxa), 0),
		nvl(max(a.ie_regra_filme),'P')
	into 	tx_proc_resultante_p,
		ie_criterio_taxa_p,
		ie_regra_filme_p	
	from	convenio_taxa_exame a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and 	a.cd_convenio		= cd_convenio_p
	and 	a.cd_categoria		= cd_categoria_p
	and	a.cd_taxa_exame		= cd_procedimento_p
	and	a.ie_origem_proced	= ie_origem_proced_p
	and     a.cd_procedimento	= cd_procedimento_p
	and     a.ie_origem_proced	= ie_origem_proced_p
	and 	nvl(a.nr_seq_proc_interno, nvl(nr_seq_proc_interno_p,0)) = nvl(nr_seq_proc_interno_p,0)
	and     nr_seq_proc_princ_p	<> 0
	and	nvl(a.ie_situacao, 'A') = 'A';
	end;
end;
END Obter_Regra_Taxa_Exame;
/
