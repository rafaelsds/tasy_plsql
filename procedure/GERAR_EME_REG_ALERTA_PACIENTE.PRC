create or replace
procedure gerar_eme_reg_alerta_paciente(
		cd_pessoa_fisica_p		varchar2,
		ds_alerta_p		varchar2,
		nm_usuario_p		varchar2) is 

begin
if	(nm_usuario_p is not null) then
	begin	
	if	(ds_alerta_p is not null) then
		begin
		insert into
		eme_reg_alerta_paciente (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			cd_pessoa_fisica,
			dt_alerta,	
			ds_alerta) 
		values (
			eme_reg_alerta_paciente_seq.nextval, 
			sysdate, 
			nm_usuario_p,
			cd_pessoa_fisica_p,
			sysdate,
			ds_alerta_p); 
		end;
	end if;
	end;
end if;
commit;
end gerar_eme_reg_alerta_paciente;
/