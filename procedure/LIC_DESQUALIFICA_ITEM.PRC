create or replace
procedure lic_desqualifica_item(	nr_seq_licitacao_p		number,
				nr_seq_lic_item_p		number,
				ds_motivo_desqualif_p	varchar2,
				nm_usuario_p		Varchar2) is 

ie_qualificado_w		varchar2(1);
					
begin

update	reg_lic_item_fornec
set	ie_qualificado		= 'N',
	ds_motivo_desqualif	= ds_motivo_desqualif_p
where	nr_seq_licitacao		= nr_seq_licitacao_p
and	nr_seq_lic_item		= nr_seq_lic_item_p;

commit;

end lic_desqualifica_item;
/