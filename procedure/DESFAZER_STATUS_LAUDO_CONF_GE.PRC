create or replace
procedure desfazer_status_laudo_conf_ge(	nr_sequencia_p		Varchar2,
						nm_usuario_p		Varchar2) is 
						
nr_sequencia_w	Varchar2(4000) := '';
ds_auxiliar_w	Varchar2(4000) := '';
k integer;

begin
--Utilizado na Gest�o de exames Java

nr_sequencia_w := nr_sequencia_p;

while	nr_sequencia_w is not null loop
	begin
	
	select	instr(nr_sequencia_w, ' ,') 
	into	k
	from	dual;

	if	(k > 1) and
		(substr(nr_sequencia_w, 1, k -1) is not null) then
		ds_auxiliar_w	:= substr(nr_sequencia_w, 1, k-1);
		ds_auxiliar_w	:= replace(ds_auxiliar_w, ',','');
		nr_sequencia_w	:= substr(nr_sequencia_w, k + 1, 4000);
	elsif	(nr_sequencia_w is not null) then
		ds_auxiliar_w	:= replace(nr_sequencia_w,',','');
		nr_sequencia_w	:= '';
	end if;
	
	update 	laudo_paciente 
	set 	dt_conferencia = null 
	where 	nr_sequencia = ds_auxiliar_w;
		
	end;
end loop;

commit;

end desfazer_status_laudo_conf_ge;
/
