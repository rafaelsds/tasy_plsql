create or replace procedure insert_mhr_doc_file (
	nm_usuario_p			in	mhr_doc_files.nm_usuario%type ,
	ds_doc_file_path_p		in	mhr_doc_files.ds_doc_file_path%type ,
	ds_doc_file_name_p		in	mhr_doc_files.ds_doc_file_name%type ,
	nr_seq_mhr_doc_meta_p	in	mhr_doc_files.nr_seq_mhr_doc_meta%type 
) is

begin

	insert into mhr_doc_files 
		(nr_sequencia, nm_usuario, dt_atualizacao, nr_seq_mhr_doc_meta, ds_doc_file_path, ds_doc_file_name ) 
	values 
		(mhr_doc_files_seq.nextval, nm_usuario_p, sysdate, nr_seq_mhr_doc_meta_p, ds_doc_file_path_p, ds_doc_file_name_p );

	commit;

end insert_mhr_doc_file;
/
