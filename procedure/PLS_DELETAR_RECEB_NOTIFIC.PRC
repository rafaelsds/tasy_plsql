create or replace
procedure pls_deletar_receb_notific(
			nr_sequencia_p		number) is 

begin

delete 
from 	pls_notificacao_receb
where	nr_sequencia = nr_sequencia_p;

commit;

end pls_deletar_receb_notific;
/