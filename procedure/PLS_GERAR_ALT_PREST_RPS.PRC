create or replace
procedure pls_gerar_alt_prest_rps(	cd_cgc_p			pessoa_juridica.cd_cgc%type,
					cd_pessoa_fisica_p		pessoa_fisica.cd_pessoa_fisica%type,
					nr_seq_prestador_p		pls_prestador.nr_sequencia%type,
					cd_cgc_cpf_old_p		pls_alt_prest_rps.cd_cgc_cpf_old%type,
					cd_municipio_ibge_old_p		pls_alt_prest_rps.cd_municipio_ibge_old%type,
					cd_cnes_old_p			pls_alt_prest_rps.cd_cnes_old%type,
					nm_usuario_p			varchar2,
					nr_seq_compl_pf_tel_adic_p	compl_pf_tel_adic.nr_sequencia%type,
					nr_seq_tipo_compl_adic_p	compl_pessoa_fisica.nr_seq_tipo_compl_adic%type,
					nr_seq_pj_compl_p		pessoa_juridica_compl.nr_sequencia%type) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Esta procedure � chamada por v�rias trigger n�o pode ter commit

			Em algumas bases podem existir cadastros duplicados, ou seja, registros
			diferentes de prestadores, com mesmo CNPJ ou pessoa fisica, portanto � feito uma busca
			priorizando as chaves compostas mais especificas, e depois as mais abrangentes
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 

nr_seq_prestador_w	pls_prestador.nr_sequencia%type	:= nr_seq_prestador_p;

begin
if	(nr_seq_prestador_w is null) then
	-- Prioriza a busca pelas chaves compostas, e depois pega as mais abrangentes
	if	(nr_seq_tipo_compl_adic_p is not null) and 
		(cd_pessoa_fisica_p is not null) and
		(nr_seq_prestador_w is null) then		
		select	max(a.nr_sequencia)
		into	nr_seq_prestador_w
		from	pls_prestador	a
		where	a.cd_pessoa_fisica		= cd_pessoa_fisica_p
		and	a.nr_seq_tipo_compl_adic	= nr_seq_tipo_compl_adic_p;
	end if;
	
	if	(nr_seq_pj_compl_p is not null) and
		(cd_cgc_p is not null) and
		(nr_seq_prestador_w is null) then
		select	max(a.nr_sequencia)
		into	nr_seq_prestador_w
		from	pls_prestador	a
		where	a.nr_seq_compl_pj	= nr_seq_pj_compl_p
		and	a.cd_cgc		= cd_cgc_p;
	end if;
	
	if	(nr_seq_compl_pf_tel_adic_p is not null) and
		(nr_seq_prestador_w is null) then
		select	max(a.nr_sequencia)
		into	nr_seq_prestador_w
		from	pls_prestador	a
		where	a.nr_seq_compl_pf_tel_adic	= nr_seq_compl_pf_tel_adic_p;	
	end if;
	
	if	(cd_cgc_p is not null) and
		(nr_seq_prestador_w is null) 	then -- Prestador PJ	
		select	max(a.nr_sequencia)
		into	nr_seq_prestador_w
		from	pls_prestador a
		where	a.cd_cgc = cd_cgc_p;
	end if;
		
	if	(cd_pessoa_fisica_p is not null ) and
		(nr_seq_prestador_w is null) then -- Prestador PF
		select	max(a.nr_sequencia)
		into	nr_seq_prestador_w
		from	pls_prestador a
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_p;
	end if;
end if;	
	
if	(nr_seq_prestador_w is not null) then
	insert into pls_alt_prest_rps
		(nr_sequencia,			nr_seq_prestador,		nm_usuario,
		nm_usuario_nrec,		dt_atualizacao,			dt_atualizacao_nrec,
		cd_cgc_cpf_old,			cd_municipio_ibge_old,		cd_cnes_old)
	select	pls_alt_prest_rps_seq.nextval,	nr_seq_prestador_w,		nm_usuario_p,
		nm_usuario_p,			sysdate,			sysdate,
		cd_cgc_cpf_old_p,		cd_municipio_ibge_old_p,	cd_cnes_old_p
	from 	dual;
end if; 

end pls_gerar_alt_prest_rps;
/