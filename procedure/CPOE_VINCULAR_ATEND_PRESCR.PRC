create or replace
procedure CPOE_Vincular_atend_prescr(	nr_prescricao_p		prescr_medica.nr_prescricao%type,
										nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
										nm_usuario_p		usuario.nm_usuario%type ) is 

ie_existe_cpoe_w		char(1);
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;

begin

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	prescr_medica
where	coalesce(dt_liberacao_medico, dt_liberacao, dt_liberacao_farmacia) is not null
and		nr_prescricao = nr_prescricao_p;

if	(cd_pessoa_fisica_w is not null) then
	select	nvl(max('S'),'N')
	into	ie_existe_cpoe_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p
	and		exists(	select	1
					from	prescr_dieta b
					where	b.nr_prescricao = a.nr_prescricao
					and		b.nr_seq_dieta_cpoe is not null
					union all
					select	1
					from	nut_pac b
					where	b.nr_prescricao = a.nr_prescricao
					and		b.nr_seq_npt_cpoe is not null
					union all
					select	1
					from	rep_jejum b
					where	b.nr_prescricao = a.nr_prescricao
					and		b.nr_seq_dieta_cpoe is not null
					union all
					select	1
					from	prescr_leite_deriv b
					where	b.nr_prescricao = a.nr_prescricao
					and		b.nr_seq_dieta_cpoe is not null
					union all
					select	1
					from	prescr_material b
					where	b.nr_prescricao = a.nr_prescricao
					and		coalesce(b.nr_seq_mat_cpoe,b.nr_seq_dieta_cpoe) is not null
					union all
					select	1
					from	prescr_gasoterapia b
					where	b.nr_prescricao = a.nr_prescricao
					and		b.nr_seq_gas_cpoe is not null
					union all
					select	1
					from	prescr_procedimento b
					where	b.nr_prescricao = a.nr_prescricao
					and		b.nr_seq_proc_cpoe is not null
					union all
					select	1
					from	prescr_solic_bco_sangue b
					where	b.nr_prescricao = a.nr_prescricao
					and		b.nr_seq_hemo_cpoe is not null
					union all
					select	1
					from	prescr_solucao b
					where	b.nr_prescricao = a.nr_prescricao
					and		b.nr_seq_dialise_cpoe is not null );
	
	if	(ie_existe_cpoe_w = 'S') then
		update	cpoe_dieta a
		set		a.nr_atendimento = nr_atendimento_p,
				a.dt_atualizacao = sysdate,
				a.nm_usuario = nm_usuario_p
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and		a.nr_atendimento is null
		and		exists(	select	1
						from	prescr_dieta b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_dieta_cpoe = a.nr_sequencia
						union all
						select	1
						from	nut_pac b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_npt_cpoe = a.nr_sequencia
						union all
						select	1
						from	prescr_material b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_dieta_cpoe = a.nr_sequencia 
						union all
						select	1
						from	prescr_leite_deriv b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_dieta_cpoe = a.nr_sequencia 
						union all
						select	1
						from	rep_jejum b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_dieta_cpoe = a.nr_sequencia );
						
		update	cpoe_material a
		set		a.nr_atendimento = nr_atendimento_p,
				a.dt_atualizacao = sysdate,
				a.nm_usuario = nm_usuario_p
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and		a.nr_atendimento is null
		and		exists(	select	1
						from	prescr_material b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_mat_cpoe = a.nr_sequencia );

		update	cpoe_procedimento a
		set		a.nr_atendimento = nr_atendimento_p,
				a.dt_atualizacao = sysdate,
				a.nm_usuario = nm_usuario_p
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and		a.nr_atendimento is null
		and		exists(	select	1
						from	prescr_procedimento b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_proc_cpoe = a.nr_sequencia );

		update	cpoe_hemoterapia a
		set		a.nr_atendimento = nr_atendimento_p,
				a.dt_atualizacao = sysdate,
				a.nm_usuario = nm_usuario_p
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and		a.nr_atendimento is null
		and		exists(	select	1
						from	prescr_solic_bco_sangue b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_hemo_cpoe = a.nr_sequencia );

		update	cpoe_gasoterapia a
		set		a.nr_atendimento = nr_atendimento_p,
				a.dt_atualizacao = sysdate,
				a.nm_usuario = nm_usuario_p
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and		a.nr_atendimento is null
		and		exists(	select	1
						from	prescr_gasoterapia b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_gas_cpoe = a.nr_sequencia );

		update	cpoe_recomendacao a
		set		a.nr_atendimento = nr_atendimento_p,
				a.dt_atualizacao = sysdate,
				a.nm_usuario = nm_usuario_p
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and		a.nr_atendimento is null
		and		exists(	select	1
						from	prescr_recomendacao b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_rec_cpoe = a.nr_sequencia );

		update	cpoe_dialise a
		set		a.nr_atendimento = nr_atendimento_p,
				a.dt_atualizacao = sysdate,
				a.nm_usuario = nm_usuario_p
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and		a.nr_atendimento is null
		and		exists(	select	1
						from	prescr_solucao b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_dialise_cpoe = a.nr_sequencia );
		
		update	cpoe_anatomia_patologica a
		set		a.nr_atendimento = nr_atendimento_p,
				a.dt_atualizacao = sysdate,
				a.nm_usuario = nm_usuario_p
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and		a.nr_atendimento is null
		and		exists(	select	1
						from	prescr_procedimento b
						where	b.nr_prescricao = nr_prescricao_p
						and		b.nr_seq_proc_cpoe = a.nr_sequencia
						and ie_tipo_proced in ('AP','APH','APC'));
		commit;
	end if;
end if;

end CPOE_Vincular_atend_prescr;
/
