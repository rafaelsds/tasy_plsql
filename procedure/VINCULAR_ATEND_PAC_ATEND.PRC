create or replace
procedure vincular_atend_pac_atend(	nr_seq_atendimento_p 	number,
					nr_atendimento_p 	number,
					nm_usuario_p 		varchar2,
					ds_erro_p		out varchar2) is

ie_permite_vinc_atend_w varchar2(1);
ie_atualizar_Dt_Prevista_w varchar2(1);
ie_vinc_atend_autor_quimio_w varchar2(1);
ie_vinc_presc_novo_atende_w varchar2(1);
nr_ciclo_w number(3);
nr_seq_paciente_w number(10);
nr_prescricao_w number(14);
dt_prevista_w date;
dt_entrada_w date;
ds_erro_w varchar(255) := '';

begin
if (nr_seq_atendimento_p is not null) then

	obter_param_usuario(3130,72,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_permite_vinc_atend_w);
	obter_param_usuario(3130,108,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_atualizar_Dt_Prevista_w);
	obter_param_usuario(3130,294,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_vinc_atend_autor_quimio_w);
	obter_param_usuario(3130,155,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_vinc_presc_novo_atende_w);

	select 	nvl(max(nr_seq_paciente), 0),
		max(dt_prevista),
		nvl(max(nr_ciclo), 0),
		nvl(max(nr_prescricao), 0)
	into 	nr_seq_paciente_w,
		dt_prevista_w,
		nr_ciclo_w,
		nr_prescricao_w
	from paciente_atendimento
	where  nr_seq_atendimento = nr_seq_atendimento_p;
	
	

	if ((nr_atendimento_p is not null) and (nr_atendimento_p > 0)) then
		begin

		if (ie_permite_vinc_atend_w = 'S') then
			update paciente_atendimento
			set dt_chegada = sysdate,
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p,
			nr_atendimento = nr_atendimento_P
			where obter_dados_paciente_setor(nr_seq_paciente,'C') = obter_dados_paciente_setor(nr_seq_paciente_w ,'C')
			and   ((nr_atendimento is null) or (nr_atendimento  = 0))
			and   trunc(dt_prevista) = trunc(dt_prevista_w);
		else
			update paciente_atendimento
			set dt_chegada = sysdate,
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p,
			nr_atendimento = nr_atendimento_p
			where nr_seq_atendimento = nr_seq_atendimento_p;
		end if;

		commit;

		if (ie_atualizar_Dt_Prevista_w = 'S') then
			begin

			Select nvl(dt_entrada, sysdate)
			into dt_entrada_w
			from atendimento_paciente
			where nr_atendimento = nr_atendimento_p;

			atualizar_ciclo_oncologia(nr_seq_paciente_w, nr_seq_atendimento_p, dt_entrada_w,
				'S', nm_usuario_p, 'N', ds_erro_w, 'S');

			end;
		end if;

		if (((ds_erro_w is null) or (ds_erro_w = '')) and (ie_vinc_atend_autor_quimio_w <> 'N')) then
			begin
			update  autorizacao_convenio
			set     dt_atualizacao    = sysdate,
					nm_usuario        = nm_usuario_p,
					nr_atendimento    = nr_atendimento_p
			where   nr_atendimento is null
					and     ((nr_ciclo = nr_ciclo_w) or (ie_vinc_atend_autor_quimio_w = 'T'))
					and     nr_seq_paciente_setor   = nr_seq_paciente_w;
			commit;
			end;
		end if;

		if ((ds_erro_w is null) or (ds_erro_w = '')) then
			begin
			if (ie_vinc_presc_novo_atende_w = 'S') then
			Update 	prescr_medica
			set 	nr_atendimento 	= nr_atendimento_p
			where 	nr_prescricao 	= nr_prescricao_w;
			end if;

			commit;

			end;
		end if;

		end;
	end if;
end if;
end vincular_atend_pac_atend;
/