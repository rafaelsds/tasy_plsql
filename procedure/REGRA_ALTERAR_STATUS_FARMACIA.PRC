create or replace
procedure regra_alterar_status_farmacia		( nr_prescricao_p	number,
						  ie_momento_p		varchar2,
						  nm_usuario_p		Varchar2) is 
						  
ie_possui_w		varchar2(1) := 'N';
nr_seq_status_w		number(10);

begin

select 	decode(count(*),0,'N','S')
into	ie_possui_w
from   	regra_status_farmacia
where  	ie_momento = ie_momento_p;


if (ie_possui_w = 'S') then

	select max(NR_SEQ_STATUS)
	into   nr_seq_status_w
	from   regra_status_farmacia
	where  ie_momento = ie_momento_p;
	
	if (nr_seq_status_w > 0) then
		update prescr_medica
		set    nr_seq_status_farm = nr_seq_status_w
		where  nr_prescricao	  = nr_prescricao_p;
	
	commit;
	end if;
end if;



end regra_alterar_status_farmacia;
/