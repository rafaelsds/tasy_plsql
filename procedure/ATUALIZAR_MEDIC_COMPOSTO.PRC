CREATE OR REPLACE
PROCEDURE Atualizar_medic_composto(
				nr_prescricao_p		Number,
				nr_sequencia_p		Number,
				nr_agrupamento_p	Number,
				cd_perfil_p		Number,
				nm_usuario_p		varchar2) is

ie_origem_inf_w			varchar2(1);
cd_intervalo_w			varchar2(7);
ds_horarios_w			varchar2(2000);
ie_via_aplicacao_w		varchar2(5);
nr_agrupamento_w		Number(7,1);
ie_utiliza_kit_w		varchar2(1);
ie_urgencia_w			varchar2(1);
ie_medicacao_paciente_w		varchar2(1);
hr_prim_horario_w		varchar2(5);
ie_se_necessario_w		varchar2(1);
ie_bomba_infusao_w		varchar2(1);
ie_aplic_bolus_w		varchar2(1);
ie_aplic_lenta_w		varchar2(1);
ie_acm_w			varchar2(1);
nr_sequencia_w			number(6);
ds_erro_w			Number(10);

Cursor C01 is
select	nr_sequencia
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	nr_agrupamento	= nr_agrupamento_p
and	nr_sequencia	<> nr_sequencia_p
and	ie_agrupador	= 1
order by nr_sequencia desc;

BEGIN

select	ie_origem_inf,
	cd_intervalo,
	ds_horarios,
	ie_via_aplicacao,
	nr_agrupamento,
	ie_utiliza_kit,
	ie_urgencia,
	ie_medicacao_paciente,
	hr_prim_horario,
	ie_se_necessario,
	ie_bomba_infusao,
	ie_aplic_bolus,
	ie_aplic_lenta,
	ie_acm
into	ie_origem_inf_w,
	cd_intervalo_w,
	ds_horarios_w,
	ie_via_aplicacao_w,
	nr_agrupamento_w,
	ie_utiliza_kit_w,
	ie_urgencia_w,
	ie_medicacao_paciente_w,
	hr_prim_horario_w,
	ie_se_necessario_w,
	ie_bomba_infusao_w,
	ie_aplic_bolus_w,
	ie_aplic_lenta_w,
	ie_acm_w
from	prescr_material
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nr_sequencia_p;



OPEN C01;
LOOP
FETCH C01 into
	nr_sequencia_w;
exit when c01%notfound;

	update	prescr_material
	set	ie_origem_inf		= ie_origem_inf_w,
		cd_intervalo		= cd_intervalo_w,
		ds_horarios		= ds_horarios_w,
		ie_via_aplicacao	= ie_via_aplicacao_w,
		nr_agrupamento		= nr_agrupamento_w,
		ie_utiliza_kit		= ie_utiliza_kit_w,
		ie_urgencia		= ie_urgencia_w,
		ie_medicacao_paciente	= ie_medicacao_paciente_w,
		hr_prim_horario		= hr_prim_horario_w,
		ie_se_necessario	= ie_se_necessario_w,
		ie_bomba_infusao	= ie_bomba_infusao_w,
		ie_aplic_bolus		= ie_aplic_bolus_w,
		ie_aplic_lenta		= ie_aplic_lenta_w,
		ie_acm			= ie_acm_w
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		= nr_sequencia_w;
	
	Atualiza_antimicrobiano_dia(nr_prescricao_p, nr_sequencia_w);
	consistir_prescr_material(nr_prescricao_p, nr_sequencia_w, nm_usuario_p, cd_perfil_p, ds_erro_w);

END LOOP;
CLOSE C01;

commit;

END Atualizar_medic_composto;
/