create or replace
procedure gerar_w_pep_apap_grafico(	cd_estabelecimento_p	number,
					nr_atendimento_p	number,
					nm_usuario_p		varchar2) is

nr_sequencia_w		number(5) := 0;
nr_count_w		number(5);
ds_informacao_w		varchar2(255);
nr_seq_avaliacao_w	number(10);
					
	
Cursor C01 is
	select	a.nr_sequencia,
		a.ds_informacao
	from	w_pep_apap a
	where	a.nr_atendimento  = nr_atendimento_p
	and	a.nm_usuario      = nm_usuario_p
	and	a.ie_grafico	 = 'S'
	order by
		a.dt_referencia,
		a.nr_ordem_grupo, 
		a.nr_ordem_inf, 
		a.nr_sequencia;
	
begin

if	(nr_atendimento_p is not null) then

/*	create table	w_pep_apap_grafico
		(NR_SEQUENCIA	NUMBER(5),
		DS_INFORMACAO	VARCHAR2(255),
		DT_HORA		DATE,
		QT_HORA		NUMBER(15,4),
		NM_USUARIO	VARCHAR2(20))  */
					


	delete	from w_pep_apap_grafico
	where	dt_atualizacao < sysdate - 12/24;

	delete	from w_pep_apap_grafico
	where	nr_atendimento	= nr_atendimento_p
	and	nm_usuario	= nm_usuario_p;
	
	

	open C01;
	loop
	fetch C01 into	
		nr_seq_avaliacao_w,
		ds_informacao_w;
	exit when C01%notfound;
		begin
			ds_informacao_w	:= replace(ds_informacao_w,chr(38)||'nbsp;','');
		
			nr_sequencia_w := nr_sequencia_w + 1;
			
			insert	into w_pep_apap_grafico (
				nr_sequencia,
				ds_informacao,
				dt_hora,
				qt_hora,			
				nm_usuario,
				dt_atualizacao,
				nr_atendimento)
			select	nr_sequencia_w,
				ds_informacao_w,
				ds_hora,
				qt_hora,
				nm_usuario_p,
				sysdate,
				nr_atendimento_p
			from
			(select to_date('02/01/2008 00:00:01','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_00) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('02/01/2008 01:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_01) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('02/01/2008 02:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_02) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('02/01/2008 03:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_03) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('02/01/2008 04:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_04) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('02/01/2008 05:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_05) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 06:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_06) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 07:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_07) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 08:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_08) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 09:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_09) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 10:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_10) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 11:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_11) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 12:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_12) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 13:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_13) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 14:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_14) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 15:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_15) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 16:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_16) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 17:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_17) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 18:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_18) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 19:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_19) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 20:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_20) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 21:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_21) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 22:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_22) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			union
			select to_date('01/01/2008 23:00:00','dd/mm/yyyy hh24:mi:ss') ds_hora,
				obter_somente_numero(ds_hora_23) qt_hora
			from	w_pep_apap
			where	nr_sequencia = nr_seq_avaliacao_w
			)
			where	qt_hora is not null
			and	qt_hora <> 0;
		end;
	end loop;
	close C01;
end if;

commit;

end gerar_w_pep_apap_grafico;
/
