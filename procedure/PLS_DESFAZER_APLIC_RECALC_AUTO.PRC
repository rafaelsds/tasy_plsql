create or replace
procedure pls_desfazer_aplic_recalc_auto(	nr_seq_lote_p	pls_lote_recalculo_autoger.nr_sequencia%type,
						nm_usuario_p	usuario.nm_usuario%type) is 

nr_seq_prot_w		pls_protocolo_conta.nr_sequencia%type;
nr_lote_contabil_w	pls_protocolo_conta.nr_lote_contabil%type;
nr_lote_contab_pag_w	pls_protocolo_conta.nr_lote_contab_pag%type;
nr_lote_prov_copartic_w	pls_protocolo_conta.nr_lote_prov_copartic%type;	
vl_lib_anterior_w	pls_conta_proc.vl_lib_original%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
vl_liberado_hi_w	pls_conta_proc.vl_liberado_hi%type;
vl_liberado_co_w	pls_conta_proc.vl_liberado_co%type;
vl_liberado_material_w	pls_conta_proc.vl_liberado_material%type;
qt_pgto_w		pls_integer;
aux_w			pls_integer;
qt_item_w		pls_integer;
vl_tot_partic_w		pls_proc_participante.vl_participante%type;
vl_dif_partic_w		number(15,2);

--Itens do lote de recalculo de autogerado
Cursor c_itens_recalculo (	nr_seq_lote_pc	pls_lote_recalculo_autoger.nr_sequencia%type) is 
	select	nr_sequencia, 
		nr_seq_conta_proc,
		nr_seq_conta, 
		vl_item_atual,
		vl_item,
		qt_item
	from	pls_itens_rec_auto
	where	nr_seq_lote = nr_seq_lote_pc;

--Obt�m infroma��es dos procedimentos para processamento
Cursor c_procedimentos(	nr_seq_conta_proc_pc	pls_conta_proc.nr_sequencia%type) is
	select	vl_liberado,
		vl_lib_original,
		vl_procedimento,
		qt_procedimento_imp,
		vl_calc_co_util,
		vl_calc_hi_util,
		vl_calc_mat_util
	from	pls_conta_proc
	where	nr_sequencia = nr_seq_conta_proc_pc;

Cursor c_contas( 	nr_seq_lote_pc	pls_lote_recalculo_autoger.nr_sequencia%type) is
	select	distinct nr_seq_conta,
		(select	nr_seq_protocolo 
		from 	pls_conta 
		where 	nr_sequencia  = a.nr_sequencia) nr_seq_protocolo
	from	pls_itens_rec_auto a
	where	nr_seq_lote = nr_seq_lote_pc;

begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

for r_itens_w in c_itens_recalculo(nr_seq_lote_p) loop

	--Primeiramente faz uma verifica��o para garantir que a conta n�o esta em um lote do SIP
	select	count(1)
	into	aux_w
	from	sip_nv_dados a
	where	a.ie_conta_enviada_ans = 'S'
	and	a.nr_seq_conta = r_itens_w.nr_seq_conta
	and	exists(	select	1
			from	pls_lote_sip b
			where	b.nr_sequencia = a.nr_seq_lote_sip
			and	b.dt_envio is not null);

	if	(aux_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(338028);
	end if;

	--Verifica se esta em lote cont�bil
	select	nr_sequencia,
		nr_lote_contabil,
		nr_lote_contab_pag,
		nr_lote_prov_copartic
	into	nr_seq_prot_w,
	        nr_lote_contabil_w,
	        nr_lote_contab_pag_w,
	        nr_lote_prov_copartic_w
	from	pls_protocolo_conta a
	where	nr_sequencia = (select	nr_seq_protocolo
				from	pls_conta b
				where	b.nr_sequencia = r_itens_w.nr_seq_conta);

	if	(nr_lote_contabil_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(335018, 'NR_LOTE_CONTABIL_W=' || nr_lote_contabil_w);
	end if;

	if	(nr_lote_contab_pag_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(335020, 'NR_LOTE_CONTABIL_W=' || nr_lote_contab_pag_w);
	end if;

	if	(nr_lote_prov_copartic_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(335021, 'NR_LOTE_CONTABIL_W=' || nr_lote_prov_copartic_w);
	end if;

	--Verifica se esta em lote de pagamento
	select	sum(qt)
	into	qt_pgto_w
	from	(
		select	count(1) qt
		from 	pls_conta_medica_resumo
		where	nr_seq_conta = r_itens_w.nr_seq_conta
		and	nr_seq_conta_proc = r_itens_w.nr_seq_conta_proc
		and	ie_situacao = 'A'
		and	ie_tipo_item <> 'I'
		and 	nr_seq_lote_pgto is not null
		union all
		select	count(1) qt
		from 	pls_conta_medica_resumo
		where	nr_seq_conta = r_itens_w.nr_seq_conta
		and	nr_seq_conta_proc = r_itens_w.nr_seq_conta_proc
		and	ie_situacao = 'A'
		and	ie_tipo_item <> 'I'
		and 	nr_seq_pp_lote is not null
	);

	if	(qt_pgto_w > 0) then
		/* N�o foi poss�vel desfazer o recalculo, j� existem itens vinculados a um lote de pagamento. */
		wheb_mensagem_pck.exibir_mensagem_abort(334860);
	end if;

	for r_procedimentos_w in c_procedimentos( r_itens_w.nr_seq_conta_proc) loop

		--Guarda o valor liberado antes de alter�-lo, devido ao log gravado ao t�rmino da altera��o.
		vl_lib_anterior_w := r_procedimentos_w.vl_liberado;

		--Atualiza valores proporcionalmente ao valor de recalculo(Vl unit�rio ser� alterado automaticamente).
		vl_liberado_hi_w	:= r_procedimentos_w.vl_lib_original * dividir_sem_round( r_procedimentos_w.vl_calc_hi_util, r_procedimentos_w.vl_procedimento);
		vl_liberado_co_w	:= r_procedimentos_w.vl_lib_original * dividir_sem_round( r_procedimentos_w.vl_calc_co_util, r_procedimentos_w.vl_procedimento);
		vl_liberado_material_w	:= r_procedimentos_w.vl_lib_original * dividir_sem_round( r_procedimentos_w.vl_calc_mat_util,r_procedimentos_w.vl_procedimento);

		--Inativa que havia sido gerada pela aplica��o dos valores de rec�lculo de autogerado.
		update	pls_ocorrencia_benef
		set	ie_situacao = 'I'
		where	nr_seq_proc = r_itens_w.nr_seq_conta_proc
		and	ds_observacao = 'Ocorr�ncia gerada pelo rec�lculo de autogerado';

		update	pls_conta_proc
		set	vl_liberado			= r_procedimentos_w.vl_lib_original,
			vl_pag_medico_conta		= 0,
			vl_total_partic			= 0,
			vl_prestador			= r_procedimentos_w.vl_lib_original,
			vl_unitario			= dividir(r_procedimentos_w.vl_lib_original, qt_procedimento),
			vl_glosa			= vl_procedimento_imp - r_procedimentos_w.vl_lib_original,
			vl_liberado_hi			= vl_liberado_hi_w,
			vl_liberado_co			= vl_liberado_co_w,
			vl_liberado_material		= vl_liberado_material_w
		where	nr_sequencia			= r_itens_w.nr_seq_conta_proc;

		--Retorna o valor calculado dos participantes
		update	pls_proc_participante
		set	vl_participante		= dividir((r_procedimentos_w.vl_lib_original * vl_calculado),r_procedimentos_w.vl_procedimento)
		where	nr_seq_conta_proc		= r_itens_w.nr_seq_conta_proc
		and	((ie_status is null) or (ie_status != 'C'))
		and	((ie_gerada_cta_honorario is null) or (ie_gerada_cta_honorario <> 'S'));

		--Para resolver algum poss�vel problema de arredondamento no calculo do vl_participante.
		select 	nvl(sum(vl_participante),0)
		into	vl_tot_partic_w
		from	pls_proc_participante
		where	nr_seq_conta_proc = r_itens_w.nr_seq_conta_proc
		and	((ie_status is null) or (ie_status != 'C'))
		and	((ie_gerada_cta_honorario is null) or (ie_gerada_cta_honorario <> 'S'));

		--Ocorreu diferen�a de arredondamento
		if	( vl_tot_partic_w > r_procedimentos_w.vl_lib_original ) then
			 
			 vl_dif_partic_w := vl_tot_partic_w - r_procedimentos_w.vl_lib_original;
			
			update	pls_proc_participante	a
			set	a.vl_participante = a.vl_participante - vl_dif_partic_w
			where	a.nr_sequencia = (	select	max(x.nr_sequencia)
							from	pls_proc_participante	x
							where	x.nr_seq_conta_proc	= r_itens_w.nr_seq_conta_proc
							and	((x.ie_status is null) or (x.ie_status != 'C'))
							and	((x.ie_gerada_cta_honorario is null) or (x.ie_gerada_cta_honorario <> 'S'))
							and	x.vl_participante >= vl_dif_partic_w);
		end if;

		update	pls_conta_proc
		set	vl_glosa = 0
		where	nr_sequencia = r_itens_w.nr_seq_conta_proc
		and	vl_glosa < 0;

		select	count(1)
		into	qt_item_w
		from	pls_conta_proc
		where	nr_sequencia = r_itens_w.nr_seq_conta_proc;

		if	(qt_item_w > 0) then
			pls_atualiza_conta_resumo_item( r_itens_w.nr_seq_conta_proc, 'P', nm_usuario_p,'N');
		end if;	
	end loop;
end loop;

--Ajuste dos valores a n�vel de conta(Considerando todas as contas do lote de rec�lculo)
for r_contas_w in c_contas(nr_seq_lote_p) loop
	pls_atualiza_valor_conta( r_contas_w.nr_seq_conta, nm_usuario_p);
	pls_cta_consistir_pck.gerar_resumo_conta( null, null, null, r_contas_w.nr_seq_conta, nm_usuario_p, cd_estabelecimento_w);
	pls_gerar_valores_protocolo( r_contas_w.nr_seq_protocolo, nm_usuario_p);
end loop;

update	pls_lote_recalculo_autoger
set	dt_aplicacao = null 
where	nr_sequencia = nr_seq_lote_p;
commit;

end pls_desfazer_aplic_recalc_auto;
/