create or replace
procedure executar_evento_agenda_geral(	ie_evento_p		varchar2,
					ie_evolucao_clinica_p	varchar2,
					nr_seq_agenda_p		number,
					cd_tipo_agenda_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is

ie_existe_evento_w		varchar2(1);
ie_agenda_w		varchar2(5);

begin

if	(cd_tipo_agenda_p = 1) then
	ie_agenda_w := 'CI';
elsif	(cd_tipo_agenda_p = 2) then
	ie_agenda_w := 'E';
elsif	(cd_tipo_agenda_p = 3) then
	ie_agenda_w := 'C';
elsif	(cd_tipo_agenda_p = 5) then
	ie_agenda_w := 'S';
end if;

ie_existe_evento_w	:= substr(obter_se_existe_evento_agenda(cd_estabelecimento_p, ie_evento_p, ie_agenda_w, ie_evolucao_clinica_p),1,1);

if	(ie_existe_evento_w = 'S') then
	executar_evento_agenda(ie_evento_p, ie_agenda_w, nr_seq_agenda_p, cd_estabelecimento_p, nm_usuario_p, ie_evolucao_clinica_p);
end if;

end executar_evento_agenda_geral;
/
