create or replace
procedure gerar_validacao_digital_rx(cd_pessoa_biometria_p	varchar2,
					nm_usuario_p		varchar2,
					nm_paciente_p 		out varchar2) is 


cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nm_paciente_w			pessoa_fisica.nm_pessoa_fisica%type;

begin

if (cd_pessoa_biometria_p is not null)  then
	select 	max(b.cd_pessoa_fisica),
		substr(obter_nome_pf(max(b.cd_pessoa_fisica)),1,255)
	into	cd_pessoa_fisica_w,
		nm_paciente_w
	from	rxt_tumor a,
		pessoa_fisica_biometria b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	b.nr_sequencia = cd_pessoa_biometria_p;

	if (cd_pessoa_fisica_w is not null) then
		update_validacao_rxt_agenda(cd_pessoa_fisica_w, nm_usuario_p);
	end if;
end if;

nm_paciente_p := nm_paciente_w;

end gerar_validacao_digital_rx;
/