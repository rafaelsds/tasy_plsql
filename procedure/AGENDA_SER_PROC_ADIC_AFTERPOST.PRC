create or replace
procedure agenda_ser_proc_adic_afterpost(	ie_evento_p			varchar2,
					nr_seq_proc_interno_p		number,
					nr_seq_agenda_consulta_p		number,
					nr_seq_agenda_proc_adic_p		number,
					nr_sequencia_p			number,		
					cd_procedimento_p			number,
					ie_origem_proced_p		number,
					nr_seq_exame_p			number,
					ie_acao_executada_p		number,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number
					) is

ie_perm_cons_reg_glosa_proc_w varchar2(1) := '';
		
begin

obter_param_usuario(866, 168, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_perm_cons_reg_glosa_proc_w);

if	(cd_procedimento_p is not null) then
	begin
	
	gerar_autor_regra(null,null,null,null,null,null,ie_evento_p,nm_usuario_p,null,nr_seq_proc_interno_p,null,nr_seq_agenda_consulta_p,null,nr_seq_agenda_proc_adic_p,'','','');


	if	(ie_perm_cons_reg_glosa_proc_w = 'S') and
		(ie_acao_executada_p <> 3) then
		begin	
		Agserv_cons_proc_regra_glosa(nr_sequencia_p,nr_seq_agenda_proc_adic_p,cd_procedimento_p,ie_origem_proced_p,nr_seq_exame_p,nr_seq_proc_interno_p,nm_usuario_p,cd_estabelecimento_p);
		end;
	end if;
	end;
end if;
		
end agenda_ser_proc_adic_afterpost;
/
