create or replace
procedure GERAR_CONCILIACAO_ABN
		(nr_seq_extrato_p	number,
		nm_usuario_p		varchar2) is

/* 
Aten��o !!

Layout de 200 posi��es */

vl_saldo_inicial_w	number(16,2);
vl_saldo_final_w	number(16,2);
dt_saldo_inicial_w	date;
dt_saldo_final_w	date;
cd_registro_w		number(1);
cd_agencia_w		number(4);
cd_conta_w		number(7);
ie_tipo_registro_w	varchar2(1);	
cd_historico_w		varchar2(4);
ds_historico_w		varchar2(25);
nr_documento_w		number(6);
dt_movto_w		date;
vl_extrato_w		number(16,2);
ie_deb_cred_w		varchar2(1);
ie_natureza_w		varchar2(1);
cd_categoria_w		varchar2(3);
nr_seq_conta_w		number(10);

cursor c01 is
select	substr(ds_conteudo,1,1),
	substr(ds_conteudo,18,4),
	substr(ds_conteudo,22,7),
	substr(ds_conteudo,42,1),
	substr(ds_conteudo,43,3),
	substr(ds_conteudo,46,4),
	substr(ds_conteudo,50,25),
	substr(ds_conteudo,75,6),
	substr(ds_conteudo,81,6),
	somente_numero(substr(ds_conteudo,87,16)) || ',' || LPAD(somente_numero(substr(ds_conteudo,103,2)),2,0),
	substr(ds_conteudo,105,1),
	decode(substr(ds_conteudo,183,1),'1','D','2','B','V')
from	w_interf_concil
where	substr(ds_conteudo,1,1) = '1'
and	substr(ds_conteudo,42,1) = '1'
and	nr_seq_conta = nr_seq_conta_w;

begin

select	nr_seq_conta
into	nr_seq_conta_w
from	banco_extrato
where	nr_sequencia	= nr_seq_extrato_p;

select	somente_numero(substr(ds_conteudo,87,16)) || ',' || somente_numero(substr(ds_conteudo,103,2)),
	substr(ds_conteudo,81,6)
into	vl_saldo_inicial_w,
	dt_saldo_inicial_w
from	w_interf_concil
where	substr(ds_conteudo,1,1) = '1'
and	substr(ds_conteudo,42,1) = '0'
and	nr_seq_conta	= nr_seq_conta_w;

select	somente_numero(substr(ds_conteudo,87,16)) || ',' || somente_numero(substr(ds_conteudo,103,2)),
	substr(ds_conteudo,81,6)
into	vl_saldo_final_w,
	dt_saldo_final_w
from	w_interf_concil
where	substr(ds_conteudo,1,1) = '1'
and	substr(ds_conteudo,42,1) = '2'
and	nr_seq_conta	= nr_seq_conta_w;

open c01;
loop
fetch c01 into
	cd_registro_w,
	cd_agencia_w,
	cd_conta_w,
	ie_tipo_registro_w,
	cd_categoria_w,
	cd_historico_w,
	ds_historico_w,
	nr_documento_w,
	dt_movto_w,
	vl_extrato_w,
	ie_deb_cred_w,
	ie_natureza_w;	
exit when c01%notfound;
	insert into banco_extrato_lanc
		(cd_agencia_origem,
		cd_categoria,
		cd_historico,
		ds_historico,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_movimento,
		ie_conciliacao,
		ie_deb_cred,
		ie_natureza,
		nm_usuario,
		nm_usuario_nrec,
		nr_documento,
		nr_seq_extrato,
		nr_sequencia,
		vl_lancamento)
	values	(cd_agencia_w,
		cd_categoria_w,
		cd_historico_w,
		ds_historico_w,
		sysdate,
		sysdate,
		dt_movto_w,
		'N',
		ie_deb_cred_w,	
		ie_natureza_w,
		nm_usuario_p,
		nm_usuario_p,
		nr_documento_w,
		nr_seq_extrato_p,
		banco_extrato_lanc_seq.nextval,
		vl_extrato_w);
end loop;
close c01;

update	banco_extrato
set	vl_saldo_inicial = vl_saldo_inicial_w,
	vl_saldo_final	= vl_saldo_final_w,
	dt_inicio	= dt_saldo_inicial_w,
	dt_final	= dt_saldo_final_w
where	nr_sequencia	= nr_seq_extrato_p;

end GERAR_CONCILIACAO_ABN;
/