create or replace procedure importar_d301_segmento_cux(nr_seq_dataset_p	 number,
                                                       ds_conteudo_cux_p varchar2,
                                                       nm_usuario_p		   varchar2) is 

  nr_seq_301_moeda_w d301_segmento_cux.nr_seq_301_moeda%type;
  
begin
  
  if upper(substr(ds_conteudo_cux_p,1,3)) = 'CUX' then

    nr_seq_301_moeda_w := obter_seq_valor_301('C301_18_MOEDA','IE_MOEDA','EUR');
    
    insert into d301_segmento_cux(nm_usuario,
                                  nr_seq_301_moeda,
                                  nr_sequencia,
                                  dt_atualizacao,
                                  dt_atualizacao_nrec,
                                  nm_usuario_nrec,
                                  nr_seq_dataset,
                                  nr_seq_dataset_ret) values (nm_usuario_p,
                                                              nr_seq_301_moeda_w,
                                                              d301_segmento_cux_seq.nextval,
                                                              sysdate,
                                                              sysdate,
                                                              nm_usuario_p,
                                                              null,
                                                              nr_seq_dataset_p);				
  
  end if;                                  

end importar_d301_segmento_cux;
/