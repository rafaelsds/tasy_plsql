create or replace
procedure gerar_update_autor_conta(	nr_seq_agenda_p		number,
					cd_material_p		number,
					ie_opcao_p		varchar2,
					nm_usuario_p		Varchar2) is 

nr_sequencia_autor_w	number(10);
qt_existe_w		number(10);	
					
begin

select	count(*)
into	qt_existe_w
from	autorizacao_cirurgia a,
	material_autor_cirurgia b
where	b.nr_seq_autorizacao	= a.nr_sequencia	
and	a.nr_seq_agenda		=  nr_seq_agenda_p
and	b.cd_material		= cd_material_p;



if	(nvl(nr_seq_agenda_p,0) <> 0) and (nvl(qt_existe_w,0) > 0) then
	begin
	
	select 	b.nr_sequencia
	into	nr_sequencia_autor_w
	from 	material_autor_cirurgia b,
		autorizacao_cirurgia a
	where	b.nr_seq_autorizacao	= a.nr_sequencia
	and	a.nr_seq_agenda		= nr_seq_agenda_p
	and	b.cd_material		= cd_material_p;	
	
	if	(nvl(nr_sequencia_autor_w,0) > 0) then
		
		update	material_autor_cirurgia
		set 	ie_valor_conta 	= ie_opcao_p,
                nm_usuario = nm_usuario_p,
                dt_atualizacao = sysdate
		where 	nr_sequencia 	= nr_sequencia_autor_w;
	end if;

	end;
end if;	

if	(nvl(qt_existe_w,0) = 0)  then
	wheb_mensagem_pck.exibir_mensagem_abort(263403);
end if;

commit;

end gerar_update_autor_conta;
/
