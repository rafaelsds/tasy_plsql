create or replace
procedure delete_doc_digitalizado_anexo(
    	nr_sequencia_p  number) is 
begin
	if  (nr_sequencia_p is not null) then
  	begin
  		delete 
  		from   doc_digitalizado_anexo
  		where   nr_sequencia = nr_sequencia_p;
	commit;
	end;
end if;
end delete_doc_digitalizado_anexo;
/
