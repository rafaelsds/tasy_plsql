create or replace
procedure pls_update_arquivo_lote_fed_sc(nr_seq_lote_p		number,
					ds_arquivo_p		varchar2,
					nm_usuario_p		Varchar2) is 

begin
if 	(nr_seq_lote_p is not null) and
	(ds_arquivo_p is not null) then
	update  pls_lote_preco_unimed_sc
	set	ds_arquivo 	= ds_arquivo_p,
		nm_usuario	= nm_usuario_p
	where  	nr_sequencia 	= nr_seq_lote_p;

	commit;
end if;

end pls_update_arquivo_lote_fed_sc;
/