create or replace
procedure Gerar_IGC_RN(	nr_atendimento_p	number,
						nm_usuario_p		Varchar2) is 


		
ie_regra_w		varchar2(10);
qt_dias_w		number(10);
qt_semanas_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
dt_correta_w		date;		
qt_diferenca_w		number(10);
dt_nascimento_w		date;
		
Cursor C01 is
	select	ie_regra
	from	REGRA_IGC
	order by nvl(NR_SEQ_PRIOR,99999);
						
						
pragma autonomous_transaction;
begin
begin
open C01;
loop
fetch C01 into	
	ie_regra_w;
exit when C01%notfound;
	begin
	
	
	if	(ie_regra_w	= 'NBS') then
		select	max(qt_dias),
				max(qt_semanas)
		into	qt_dias_w,
				qt_semanas_w
		from	escala_nbs a
		where	a.nr_sequencia	= (	select	max(x.nr_sequencia)
									from	escala_nbs x
									where	x.nr_atendimento	= nr_atendimento_p
									and	x.dt_inativacao is  null);
									
									

	
	elsif	(ie_regra_w	= 'DUM') then
	
		select	max(QT_SEM_IG_TOTAL),
				max(QT_DIA_IG_TOTAL)
		into	qt_semanas_w,
				qt_dias_w
		from	nascimento
		where	nr_atend_rn = nr_atendimento_p;
			
	elsif	(ie_regra_w	= 'CAP') then
		select	max(QT_SEM_IG),
				max(QT_DIA_IG)
		into	qt_semanas_w,
				qt_dias_w
		from	nascimento
		where	nr_atend_rn = nr_atendimento_p;
		
	elsif	(ie_regra_w	= 'US') then
		select	max(b.QT_SEM_IG_ECOGRAFICA),
                max(b.QT_IG_DIA_US)
		into	qt_semanas_w,
                qt_dias_w
		from	nascimento a,
				parto      b
		where	a.nr_atendimento = b.nr_atendimento
		and	    a.nr_atend_rn    = nr_atendimento_p;
	end if;
	
	
	if	(qt_dias_w	is not null) or
		(qt_semanas_w	is not null) then
		exit;
	end if;
	
	
	end;
end loop;
close C01;

qt_semanas_w := nvl(qt_semanas_w, 0);
qt_dias_w    := nvl(qt_dias_w, 0);

if	(ie_regra_w	is not null) and
	((qt_dias_w	is not null) or (qt_semanas_w is not null)) and
	(nvl(qt_semanas_w,0)	<= 36) and
	(nvl(qt_dias_w,0)	<=6)then
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_p;
	
	if	(cd_pessoa_fisica_w	is not null) then
		
		select	max(dt_nascimento)
		into	dt_nascimento_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		
		qt_diferenca_w	:= 280 - ((qt_semanas_w * 7) + qt_dias_w) ;
	
		dt_correta_w	:= (dt_nascimento_w	+ qt_diferenca_w);
		update	pessoa_fisica
		set		QT_DIAS_IG	=  nvl(qt_dias_w,0),
				QT_SEMANAS_IG = nvl(qt_semanas_w,0),
				ie_regra_ig = ie_regra_w,
				DT_NASCIMENTO_IG = dt_correta_w
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		

		
		
	end if;
	
end if;
commit;
exception
when others then
null;
end;




end Gerar_IGC_RN;
/