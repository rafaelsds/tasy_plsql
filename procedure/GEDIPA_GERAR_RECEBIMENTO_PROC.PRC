create or replace
procedure gedipa_gerar_recebimento_proc(nr_processo_p	number,
				    nm_usuario_p	Varchar2) is 

begin

if 	(nr_processo_p is not null) then 
	update	adep_processo 
	set	DT_RECEBIMENTO	   = sysdate, 
		NM_USUARIO_RECEB = nm_usuario_p
	where 	nr_sequencia 	   = nr_processo_p;
end if;

commit;

end gedipa_gerar_recebimento_proc;
/