create or replace
procedure fis_gerar_lote_siscoserv_nota(
			nm_usuario_p		varchar2,
			nr_seq_lote_p		number,
			nr_seq_nbs_padrao_p	number
			) is

dt_inicial_w		date;
dt_final_w		date;
qt_insert_w		number(10);
cd_moeda_w		moeda.cd_moeda%type;
nr_seq_pessoa_rvs_w	siscoserv_rvs.nr_sequencia%type;
nr_seq_operacao_rvs_w	siscoserv_rvs_operacao.nr_sequencia%type;
nr_seq_fatura_w		siscoserv_fatura.nr_sequencia%type;
nr_seq_item_fatura_w	siscoserv_fatura_item.nr_sequencia%type;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
dt_entrada_w		date;
dt_saida_w		date;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
cd_empresa_w		estabelecimento.cd_empresa%type;

cursor c01 is
	select	a.nr_sequencia nr_seq_nota_fiscal,
		a.dt_emissao,
		'PJ' ie_pf_pj,
		a.cd_pessoa_fisica,
		a.cd_cgc,
		somente_numero(d.cd_pais_siscoserv) cd_pais_mdic,
		a.vl_total_nota,
		a.nr_interno_conta
	from	estabelecimento f,
		pais d,
		pessoa_juridica c,
		operacao_nota b,
		nota_fiscal a
	where	1 = 1
	and	a.cd_operacao_nf = b.cd_operacao_nf
	and	a.cd_cgc = c.cd_cgc
	and	c.nr_seq_pais = d.nr_sequencia
	and	somente_numero(d.cd_pais_siscoserv) <> 105 -- 76 ou 105
	and	a.nr_interno_conta is not null
	and	a.ie_situacao = 1 -- Situa��o = ativa
	and	b.ie_operacao_fiscal = 'S' -- Notas de sa�da
	and	a.dt_emissao between dt_inicial_w and dt_final_w
	and	a.dt_atualizacao_estoque is not null -- notas j� calculadas
	and	nvl(a.cd_cgc,'X') <> 'X'
	--and	a.cd_estabelecimento = cd_estabelecimento_w
	and	f.cd_estabelecimento = a.cd_estabelecimento
	and	f.cd_empresa = cd_empresa_w
	and	f.cd_estabelecimento = nvl(cd_estabelecimento_w, f.cd_estabelecimento)
	and	not exists (	select	1
				from	siscoserv_fatura x
				where	x.nr_seq_nota_fiscal = a.nr_sequencia)
	union all
	select	a.nr_sequencia nr_seq_nota_fiscal,
		a.dt_emissao,
		'PF' ie_pf_pj,
		a.cd_pessoa_fisica,
		a.cd_cgc,
		somente_numero(e.cd_pais_siscoserv) cd_pais_mdic,
		a.vl_total_nota,
		a.nr_interno_conta
	from	estabelecimento f,
		pais e,
		compl_pessoa_fisica d,
		pessoa_fisica c,
		operacao_nota b,
		nota_fiscal a
	where	1 = 1
	and	a.cd_operacao_nf = b.cd_operacao_nf
	and	a.cd_pessoa_fisica = c.cd_pessoa_fisica
	and	c.cd_pessoa_fisica = d.cd_pessoa_fisica
	and	d.ie_tipo_complemento = 1 -- complemetnto residencial
	and	d.nr_seq_pais = e.nr_sequencia
	and	somente_numero(e.cd_pais_siscoserv) <> 105 -- 76 ou 105
	and	a.nr_interno_conta is not null
	and	a.ie_situacao = 1 -- Situa��o = ativa
	and	b.ie_operacao_fiscal = 'S' -- Notas de sa�da
	and	a.dt_emissao between dt_inicial_w and dt_final_w
	and	a.dt_atualizacao_estoque is not null -- notas j� calculadas
	and	nvl(a.cd_pessoa_fisica,'X') <> 'X'
	and	f.cd_estabelecimento = a.cd_estabelecimento
	and	f.cd_empresa = cd_empresa_w
	and	f.cd_estabelecimento = nvl(cd_estabelecimento_w, f.cd_estabelecimento)
	and	not exists (	select	1
				from	siscoserv_fatura x
				where	x.nr_seq_nota_fiscal = a.nr_sequencia);

c01_w		c01%rowtype;

begin

qt_insert_w := 0;

/* ********************************   INICIO TRATAMENTO MOEDA   *********************************** */
select	max(cd_moeda)
into	cd_moeda_w
from	moeda
where	cd_moeda_siscoserv = '790';
if	(nvl(cd_moeda_w,0) = 0) then
	begin

	begin
	select	cd_moeda
	into	cd_moeda_w
	from (	select	cd_moeda,
			1 ds_moeda,
			2 ds_sigla_moeda,
			decode(cd_moeda_siscoserv,null,2,1)
		from	moeda
		where	upper(ds_moeda) = 'REAL'
		union
		select	cd_moeda,
			2 ds_moeda,
			1 ds_sigla_moeda,
			decode(cd_moeda_siscoserv,null,2,1)
		from	moeda
		where	upper(ds_sigla_moeda) = 'R$'
		union
		select	cd_moeda,
			3 ds_moeda,
			3 ds_sigla_moeda,
			decode(cd_moeda_siscoserv,null,2,1)
		from	moeda
		where	upper(ds_moeda) like '%REAL%'
		order by
			4,
			2,
			3)
	where rownum = 1;
	exception
	when others then
		cd_moeda_w := null;
	end;
	end;
end if;
/* **********************************   FIM TRATAMENTO MOEDA   ************************************ */

begin
select	a.dt_inicio_lote,
	a.dt_fim_lote,
	a.cd_estabelecimento,
	a.cd_empresa
into	dt_inicial_w,
	dt_final_w,
	cd_estabelecimento_w,
	cd_empresa_w
from	fis_siscoserv_lote a
where	a.nr_sequencia = nr_seq_lote_p;

dt_inicial_w	:= trunc(dt_inicial_w);
dt_final_w	:= fim_mes(dt_final_w);
exception
when others then
	-- N�o foi poss�vel localizar o lote SISCOSERV
	dt_inicial_w	:= null;
	dt_final_w	:= null;
end;

open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin
	begin
	select	b.nr_atendimento,
		b.dt_entrada,
		b.dt_saida_real -- DT_FIM_CONTA | DT_ALTA
	into	nr_atendimento_w,
		dt_entrada_w,
		dt_saida_w
	from	atendimento_paciente b,
		conta_paciente a
	where	a.nr_atendimento = b.nr_atendimento
	and	a.nr_interno_conta = c01_w.nr_interno_conta;
	exception
	when others then
		-- Verificar mensagem para ser exibida
		-- Atendimento n�o encotrado
		nr_atendimento_w	:= null;
		dt_entrada_w		:= null;
		dt_saida_w		:= null;
	end;

/* **********************************   INICIO DO INSERT DO RVS   ************************************ */
	select	siscoserv_rvs_seq.nextval
	into	nr_seq_pessoa_rvs_w
	from	dual;

	insert into siscoserv_rvs (
			nr_sequencia,
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nr_seq_lote,
			cd_pessoa_fisica,
			cd_cgc,
			dt_referencia,
			nr_atendimento,
			cd_moeda
			)
	values		(
			nr_seq_pessoa_rvs_w,
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			sysdate,
			nr_seq_lote_p,
			c01_w.cd_pessoa_fisica,
			c01_w.cd_cgc,
			dt_entrada_w,
			nr_atendimento_w,
			cd_moeda_w
			);
/* ***********************************   FINAL DO INSERT DO RVS  ************************************* */

/* *******************************   INICIO DO INSERT DA OPERACAO   ********************************** */

	select	siscoserv_rvs_operacao_seq.nextval
	into	nr_seq_operacao_rvs_w
	from 	dual;

	insert into siscoserv_rvs_operacao (
					nr_sequencia,
					nm_usuario,
					nm_usuario_nrec,
					dt_atualizacao,
					dt_atualizacao_nrec,
					dt_inicial,
					dt_final,
					nr_seq_rvs,
					nr_seq_nbs,
					vl_operacao)
	values				(
					nr_seq_operacao_rvs_w,
					nm_usuario_p,
					nm_usuario_p,
					sysdate,
					sysdate,
					dt_entrada_w,
					nvl(dt_saida_w, c01_w.dt_emissao),
					nr_seq_pessoa_rvs_w,
					nr_seq_nbs_padrao_p,
					c01_w.vl_total_nota
					);

/* ********************************   FINAL DO INSERT DA OPERACAO   ********************************** */

/* *********************************   INICIO DO INSERT DA FATURA   *********************************** */

	select	siscoserv_fatura_seq.nextval
	into	nr_seq_fatura_w
	from	dual;

	insert into siscoserv_fatura (
				nr_sequencia,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_nota_fiscal,
				nr_seq_rvs,
				vl_total_fatura,
				dt_fatura,
				nr_seq_lote)
	values (		nr_seq_fatura_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				nm_usuario_p,
				c01_w.nr_seq_nota_fiscal,
				nr_seq_pessoa_rvs_w,
				c01_w.vl_total_nota,
				c01_w.dt_emissao,
				nr_seq_lote_p);

/* **********************************   FINAL DO INSERT DA FATURA   *********************************** */

/* *****************************   INICIO DO INSERT DO ITEM DA FATURA   ****************************** */

	select	siscoserv_fatura_item_seq.nextval
	into	nr_seq_item_fatura_w
	from	dual;

	insert into siscoserv_fatura_item (
					nr_sequencia,
					nr_seq_fatura,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					nr_seq_oper_rvs,
					vl_item,
					vl_exterior)
	values				(
					nr_seq_item_fatura_w,
					nr_seq_fatura_w,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					nr_seq_operacao_rvs_w,
					c01_w.vl_total_nota,
					null);

/* *****************************   FINAL DO INSERT DO ITEM DA FATURA   ******************************* */

/* *****************************   INICIO DO TRATAMENTO DE COMMIT   ******************************** */
	qt_insert_w := qt_insert_w + 1;
	if	(qt_insert_w > 100) then
		begin
		commit;
		qt_insert_w := 0;
		end;
	end if;
/* ******************************   FINAL DO TRATAMENTO DE COMMIT   ******************************** */
	end; -- END C01
end loop;
close c01;

commit;

end fis_gerar_lote_siscoserv_nota;
/
