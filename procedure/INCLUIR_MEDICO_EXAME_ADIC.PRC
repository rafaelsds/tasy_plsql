create or replace
procedure Incluir_medico_exame_adic (	
				nr_prescricao_p			number,
				nr_seq_exame_p			number,
				nr_seq_material_p		number,
				cd_medico_adic_p		number,
				ie_tipo_medico_p		varchar2,
				ie_amostra_p			varchar2,
		 		nm_usuario_p			Varchar2,
				cd_estabelecimento_p		number,
				cd_setor_usuario_p		number,
				ie_mesmo_medico_exame_p		varchar2,
				ie_urgencia_p			varchar2) is

nr_seq_prescr_w			number(10);
qt_medico_adic_exame_w		number(5);
nr_seq_prof_adic_w		number(10);
cd_material_exame_w		varchar2(20);
dt_prev_execucao_w		date;
nr_atendimento_w			number(10);
dt_prescricao_w			date;
ie_tipo_convenio_w			number(2);
ie_tipo_atendimento_w		number(3);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
cd_estab_w			number(4);
cd_setor_atend_w			prescr_procedimento.cd_setor_atendimento%type;
cd_setor_exec_w			varchar2(255);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_setor_col_w			varchar2(255);
cd_setor_entrega_w		varchar2(255):= null;
qt_dia_entrega_w			number(20,0);
ie_emite_mapa_w			varchar2(255);
ds_hora_fixa_w			varchar2(255);
ie_data_resultado_w		varchar2(255);
ie_atualizar_recoleta_w		varchar2(255);
qt_min_entrega_w			number(10);
ds_erro_w			varchar2(255);
ie_gera_passagem_w		varchar2(1);
ie_vincula_proc_interno_w varchar2(1);
qt_passagem_w			Number(03,0);
ie_setor_w			varchar2(20);
nr_seq_proc_interno_w		Number(15);
nr_seq_proc_interno_vinculo_w Number(15);
ie_dia_semana_final_w 		number(10);
qt_min_atraso_w		 	number(10);
ie_regra_dt_resultado_w		varchar2(1);
dt_resultado_w			date;
ie_dia_semana_upd_w		number(3);
nr_seq_proc_interno_aux_w	number(10);	
cd_plano_convenio_w		varchar2(10);
ie_forma_atual_dt_result_w	exame_lab_regra_setor.ie_atul_data_result%type;
nr_seq_agenda_w		agenda_paciente.nr_sequencia%type;
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
ie_regra_w 	varchar(1) := 'L';
ie_autorizacao_w  prescr_procedimento.ie_autorizacao%type;
nr_seq_regra_preco_w	number(10);
ie_glosa_w		varchar2(3);
nr_seq_regra_w		number(10);
ds_erro_ww		varchar2(255);
cd_setor_origem_w	prescr_medica.cd_setor_entrega%type;

begin

Obter_Param_Usuario(916, 127, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_gera_passagem_w);
Obter_Param_Usuario(916, 569, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_vincula_proc_interno_w);
Obter_Param_Usuario(916, 47, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_setor_w);
Obter_Param_Usuario(916, 832, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_regra_dt_resultado_w);

select  max(cd_material_exame)
into	cd_material_exame_w
from	material_exame_lab
where	nr_sequencia = nr_seq_material_p;

select	nvl(max(nr_sequencia),0)
into	nr_seq_prescr_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and	nr_seq_exame  = nr_seq_exame_p
and	cd_material_exame = cd_material_exame_w;

if	(nr_seq_prescr_w > 0) and
	(ie_mesmo_medico_exame_p = 'N')then

	select	count(*)
	into	qt_medico_adic_exame_w
	from	prescr_proced_prof_adic
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_procedimento = nr_seq_prescr_w
	and	cd_profissional = cd_medico_adic_p;

	if	(qt_medico_adic_exame_w = 0) then
		select	prescr_proced_prof_adic_seq.nextval
		into	nr_seq_prof_adic_w
		from	dual;

		insert into prescr_proced_prof_adic(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_prescricao,
			ie_tipo_profissional,
			cd_profissional,
			nr_seq_procedimento)
		values(	nr_seq_prof_adic_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_prescricao_p,
			ie_tipo_medico_p,
			cd_medico_adic_p,
			nr_seq_prescr_w);

	end if;

else
	
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_prescr_w
	from	prescr_procedimento
	where 	nr_prescricao = nr_prescricao_p;

	select	nvl(max(b.dt_prev_execucao),max(a.dt_prescricao))
	into	dt_prev_execucao_w
	from	prescr_procedimento b,
		prescr_medica a
	where	a.nr_prescricao		= nr_prescricao_p
	and	a.nr_prescricao		= b.nr_prescricao
	and	b.nr_seq_exame		= nr_seq_exame_p;

	select	max(nr_seq_proc_interno)
	into	nr_seq_proc_interno_w
	from	prescr_procedimento b,
		prescr_medica a
	where	a.nr_prescricao		= nr_prescricao_p
	and	a.nr_prescricao		= b.nr_prescricao
	and	b.nr_seq_exame		= nr_seq_exame_p;
	
	select	nvl(max(nr_atendimento), 0),
		nvl(max(dt_prescricao), sysdate)
	into	nr_atendimento_w,
		dt_prescricao_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;

	if	(nvl(nr_atendimento_w, 0) > 0) then
		begin
		select	a.ie_tipo_convenio,
			a.ie_tipo_atendimento,
			b.cd_convenio,
			b.cd_categoria,
			a.cd_estabelecimento,
			b.cd_plano_convenio
		into	ie_tipo_convenio_w,
			ie_tipo_atendimento_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_estab_w,
			cd_plano_convenio_w
		from	atend_categoria_convenio b,
			atendimento_paciente a
		where	a.nr_atendimento	= nr_atendimento_w
		and	b.nr_atendimento	= a.nr_atendimento
		and	b.nr_seq_interno	= OBTER_ATECACO_ATENDIMENTO(A.NR_ATENDIMENTO);
		exception
			when no_data_found then
			wheb_mensagem_pck.exibir_mensagem_abort(187685);
			--'Faltam informa��es do conv�nio na Entrada �nica.');
		end;


		obter_exame_lab_convenio(nr_seq_exame_p, cd_convenio_w, cd_categoria_w, ie_tipo_atendimento_w,
					 cd_estab_w, ie_tipo_convenio_w, nr_seq_proc_interno_w, cd_material_exame_w, cd_plano_convenio_w,
					 cd_setor_atend_w, cd_procedimento_w, ie_origem_proced_w, ds_erro_w, nr_seq_proc_interno_aux_w);

		obter_setor_exame_lab(nr_prescricao_p, nr_seq_exame_p, cd_setor_atend_w,nr_seq_material_p, null,'S',
							  cd_setor_exec_w, cd_setor_col_w, cd_setor_entrega_w, qt_dia_entrega_w,
							  ie_emite_mapa_w, ds_hora_fixa_w, ie_data_resultado_w, qt_min_entrega_w, ie_atualizar_recoleta_w, 'N',ie_dia_semana_final_w,
							  ie_forma_atual_dt_result_w, qt_min_atraso_w);
		if 	(nvl(ie_regra_dt_resultado_w,'N') = 'S') then
			
			if	((ie_data_resultado_w = 'P') or (ie_data_resultado_w = 'N')) then

				if (ds_hora_fixa_w is not null) then

					select	(trunc(nvl(dt_prev_execucao_w,dt_prescricao)) + (ds_hora_fixa_w/24)) + qt_dia_entrega_w + (decode(qt_min_entrega_w,0,0,qt_min_entrega_w/1440))
					into	dt_resultado_w
					from 	prescr_medica
					where 	nr_prescricao = nr_prescricao_p;		
				else 
					select	nvl(dt_prev_execucao_w,dt_prescricao) + qt_dia_entrega_w  + (decode(qt_min_entrega_w,0,0,qt_min_entrega_w/1440))
					into	dt_resultado_w
					from 	prescr_medica
					where 	nr_prescricao = nr_prescricao_p;		
				end if;
			end if;
		
			if (ie_dia_semana_final_w > 0) then

				select	pkg_date_utils.get_WeekDay(dt_resultado_w)
				into	ie_dia_semana_upd_w
				from	dual;
				
				while (ie_dia_semana_upd_w <> ie_dia_semana_final_w) loop
					dt_resultado_w := dt_resultado_w + 1;
					
					select	pkg_date_utils.get_WeekDay(dt_resultado_w)
					into	ie_dia_semana_upd_w
					from	dual;
				end loop;
			end if;
		end if;
	end if;

	if	(cd_setor_atend_w is null) then
		if	(ie_setor_w = 'Usu�rio') then
			cd_setor_atend_w	:= nvl(cd_setor_usuario_p,null);
		elsif	(ie_setor_w = 'Paciente') then
			select nvl(max(cd_setor_atendimento),nvl(cd_setor_usuario_p,null))
			into cd_setor_atend_w
			from atend_paciente_unidade
			where nr_seq_interno	= obter_atepacu_paciente(nr_atendimento_w, 'A');
			if	(cd_setor_atend_w is null) then
				cd_setor_atend_w	:= nvl(cd_setor_usuario_p,null);
			end if;
		end if;
	end if;
	
	IF (cd_setor_atend_w IS NULL) and (nr_seq_exame_p is not null) THEN
		select 	max(cd_setor_entrega)
		into	cd_setor_origem_w
		from 	prescr_medica
		where 	nr_prescricao = nr_prescricao_p;
		
		cd_setor_atend_w := OBTER_SETOR_ATEND_PROC_LAB(cd_estabelecimento_p, cd_procedimento_w, ie_origem_proced_w, null, cd_setor_origem_w, nm_usuario_p, nr_seq_exame_p, nr_atendimento_w);		
	end if;
	
	if	(cd_setor_atend_w is null) then

		select	max(cd_setor_atendimento)
		into	cd_setor_atend_w
		from	usuario
		where	nm_usuario = nm_usuario_p;

	end if;

	

	select	max(nr_seq_agenda),
			max(cd_pessoa_fisica)
	into	nr_seq_agenda_w,
			cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;


	
	consiste_plano_convenio(nr_atendimento_w,
					cd_convenio_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					nvl(dt_prev_execucao_w,sysdate),
					1,
					ie_tipo_atendimento_w,
					cd_plano_convenio_w,
					null,
					ds_erro_ww,
					cd_setor_atend_w,
					nr_seq_exame_p,
					ie_regra_w,
					nr_seq_agenda_w,
					nr_seq_regra_w,
					null,
					cd_categoria_w,
					cd_estabelecimento_p,
					cd_setor_atend_w,
					cd_medico_adic_p,
					cd_pessoa_fisica_w,
					ie_glosa_w,
					nr_seq_regra_preco_w,
					nr_prescricao_p,
					null);
				
	ie_autorizacao_w := 'L';
	if (ie_regra_w in (1,2)) then
		ie_autorizacao_w := 'B';
	elsif (ie_regra_w in (3,6,7)) then
		ie_autorizacao_w := 'PA';
	end if;
	
	
	if (ie_vincula_proc_interno_w = 'S') then
		
		select max(nr_sequencia) 
		into nr_seq_proc_interno_vinculo_w 
		from proc_interno 
		where nr_seq_exame_lab = nr_seq_exame_p 
		and ie_situacao ='A';
		
	end if;
	
	
	insert into prescr_procedimento (
		nr_prescricao, nr_sequencia, cd_procedimento, qt_procedimento, dt_atualizacao,
		nm_usuario, cd_motivo_baixa, ie_origem_proced, cd_intervalo,
		ie_urgencia, ie_suspenso, cd_setor_atendimento, dt_prev_execucao,
		ie_status_atend, ie_origem_inf, ie_executar_leito, ie_se_necessario,
		ie_acm, nr_ocorrencia, ds_observacao, nr_seq_interno, nr_seq_proc_interno,
		ie_avisar_result, nr_seq_exame,cd_material_exame, dt_resultado,
		cd_setor_coleta,cd_setor_entrega, cd_medico_solicitante, ie_amostra, ie_autorizacao)
	values(
		nr_prescricao_p, nr_seq_prescr_w, cd_procedimento_w, 1, sysdate,
		nm_usuario_p, 0, ie_origem_proced_w, null,
		ie_urgencia_p, 'N', cd_setor_atend_w, nvl(dt_prev_execucao_w,sysdate) ,5, '1','N','N','N',1,
		substr(obter_texto_tasy(759955,null),1,255),
		prescr_procedimento_seq.NextVal, nr_seq_proc_interno_vinculo_w, 'N', nr_seq_exame_p,cd_material_exame_w,nvl(dt_resultado_w,dt_prescricao_w),
		cd_setor_atend_w,cd_setor_atend_w, cd_medico_adic_p, ie_amostra_p, ie_autorizacao_w);
	
	if	(ie_gera_passagem_w = 'S') then
		select	nvl(count(*),0)
		into	qt_passagem_w
		from	atend_paciente_unidade
		where	nr_atendimento 			= nr_atendimento_w;
		
		if (qt_passagem_w = 0) then
			Gerar_Passagem_Setor_Atend(nr_atendimento_w, cd_setor_atend_w, sysdate, 'S', nm_usuario_p);
		end if;
	end if;
	
end if;

commit;

end Incluir_medico_exame_adic;
/
