create or replace
procedure insert_update_perda_ganho( nr_atendimento_p		number,
					                 dt_medida_p			varchar2,
					                 nr_seq_tipo_p			number,
					                 qt_volume_p			number,
					                 dt_referencia_p		varchar2,
					                 qt_ocorrencia_p		number,
					                 nr_hora_p			    number,
					                 nm_usuario_p			varchar2,
					                 cd_setor_atendimento_p number,
					                 cd_profissional_p		number,
					                 ie_exige_liberacao		varchar2,
					                 nr_seq_topografia_p    number default null,
					                 ie_lado_p			    varchar2 default null,
					                 nr_sequencia_p	        in out number,
					                 ds_observacao_p		varchar2 default null,
									 ds_lista_inf_adic_p	varchar2 default null) is 


dt_liberacao_w	Date	:= null;
nr_sequencia_w	number(10);

begin

if (ie_exige_liberacao = 'S') then
	dt_liberacao_w := sysdate;
end if;

if (nr_sequencia_p is not null) then

	update	atendimento_perda_ganho set 
                nr_atendimento	 	= nr_atendimento_p,  
                dt_medida     	 	= to_date(dt_medida_p, 'dd/mm/yy hh24:mi:ss'),    
                dt_atualizacao	 	= sysdate,  
                nr_seq_tipo   	 	= nr_seq_tipo_p,  
                qt_volume     	 	= qt_volume_p,  
                nm_usuario    	 	= nm_usuario_p, 
                cd_setor_atendimento 	= cd_setor_atendimento_p, 
                cd_profissional 	= cd_profissional_p,
                qt_ocorrencia   	= qt_ocorrencia_p,
                nr_hora         	= nr_hora_p,
		dt_liberacao		= dt_liberacao_w,
		ds_observacao		= ds_observacao_p,
		ie_lado			= ie_lado_p,
		nr_seq_topografia	= nr_seq_topografia_p
        where 	nr_sequencia 		= nr_sequencia_p ;
else
	select 	atendimento_perda_ganho_seq.nextVal 
	into	nr_sequencia_w
	from 	dual;

	insert into atendimento_perda_ganho ( 
                  nr_atendimento,  
                  dt_medida, 
                  dt_atualizacao ,  
                  nr_seq_tipo,  
                  qt_volume,  
                  dt_referencia,  
                  qt_ocorrencia,
                  nr_hora,
                  nr_sequencia,
                  nm_usuario, 
                  cd_setor_atendimento, 
                  cd_profissional,
                  ie_lado,
                  nr_seq_topografia, 
                  dt_liberacao,
                  ie_situacao,
                  ds_observacao,
                  cd_turno) 
        values ( 
                  nr_atendimento_p, 
                  to_date(dt_medida_p, 'dd/mm/yyyy hh24:mi:ss'),
                  sysdate,
                  nr_seq_tipo_p,
                  qt_volume_p,
                  to_date(dt_referencia_p, 'dd/mm/yyyy hh24:mi:ss'), 
                  qt_ocorrencia_p,
                  nr_hora_p,
                  nr_sequencia_w,
                  nm_usuario_p,
                  cd_setor_atendimento_p,
                  cd_profissional_p,
                  ie_lado_p,
                  nr_seq_topografia_p,		  
                  dt_liberacao_w ,
                  'A',
                  ds_observacao_p,
                  Obter_Turno_Atendimento(sysdate,to_number(obter_estab_atendimento(nr_atendimento_p)),'D')
              );
				  
		Inserir_info_adic_perda(ds_lista_inf_adic_p, nr_sequencia_w, nm_usuario_p);
		
end if;

nr_sequencia_p := nr_sequencia_w;

if (ie_exige_liberacao = 'S' and nr_sequencia_p is not null) then
    gerar_lancamento_automatico(nr_atendimento_p, null, 381, nm_usuario_p, nr_sequencia_p, nr_seq_tipo_p, null, null,null,null);
end if;

commit;

end insert_update_perda_ganho;
/
