create or replace
procedure qua_cancelar_nao_conform(nr_sequencia_p			number,
								   nr_seq_motivo_cancel_p	number,
								   ds_motivo_cancel_p		varchar2,
								   nm_usuario_p				Varchar2) is 

ds_motivo_w	varchar2(80);

begin

update	qua_nao_conformidade
set	nr_seq_motivo_cancel	= nr_seq_motivo_cancel_p,
	ie_status			= 'Cancelada',
	dt_encerramento		= sysdate,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p;

select	ds_motivo
into	ds_motivo_w
from	qua_nc_motivo_cancel
where	nr_sequencia = nr_seq_motivo_cancel_p;

insert into qua_nao_conform_hist(
	nr_sequencia,
	nr_seq_nao_conform,
	dt_atualizacao,
	nm_usuario,
	ds_historico,
	cd_pessoa_fisica,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_historico,
	ie_origem)
values(	qua_nao_conform_hist_seq.nextval,
	nr_sequencia_p,
	sysdate,
	nm_usuario_p,
	substr(	WHEB_MENSAGEM_PCK.get_texto(192713) || chr(13) || chr(10) || 
		WHEB_MENSAGEM_PCK.get_texto(305761, 'DS_MOTIVO_W=' || ds_motivo_w) || chr(13) || chr(10) ||
		ds_motivo_cancel_p,1,4000),
	substr(Obter_Pessoa_Fisica_Usuario(nm_usuario_p, 'C'),1,10),
	sysdate,
	nm_usuario_p,
	sysdate,
	'S');

insert into qua_nao_conform_leitura(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_nao_conform,
	nm_usuario_acesso,
	dt_acesso)
values(	qua_nao_conform_leitura_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_sequencia_p,
	nm_usuario_p,
	sysdate);
		
commit;

end qua_cancelar_nao_conform;
/