create or replace
procedure atualiza_guia_eup(	cd_senha_p		varchar2,
			nr_doc_convenio_p	varchar2,
			nr_seq_interno_p	number,
			nm_usuario_p		varchar2) is 

begin
if	(cd_senha_p is not null) and
	(nr_doc_convenio_p is not null) and
	(nr_seq_interno_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	atend_categoria_convenio a
	set	a.cd_senha		= cd_senha_p,
		a.nr_doc_convenio	= nr_doc_convenio_p,
		a.dt_atualizacao	= sysdate,
		a.nm_usuario		= nm_usuario_p
	where	a.nr_seq_interno	= nr_seq_interno_p;
	end;
end if;
commit;

end atualiza_guia_eup;
/