create or replace PROCEDURE Tws_Horario_Livre_Consulta(
													cd_estabelecimento_p Number,
													cd_agenda_p     	 number,
													dt_agenda_p   		 Date,
													nm_usuario_p  		 varchar2,
													cd_convenio_p  		 number,
													cd_paciente_p  		 varchar2,
													ie_followup_p  		 varchar2
												) is

ds_retorno_w     		varchar2(255);
qt_w     				number(10);
ie_follow_up_w   		varchar2(1);
dt_agenda_trunc_w  		date;
dt_agenda_final_dia_w 	date;
ie_consistencia_w  		varchar2(255) := '';
ie_agenda_w    			varchar2(1)   := 'S';
nr_seq_turno_w			number(10,0);
ds_consistencia_w    	varchar2(255) := '';

TYPE horarios_tws_row IS RECORD(
		nr_sequencia        number(10),
		dt_agenda        	date,
		ie_classif_agenda   varchar2(5),
		cd_medico  			varchar2(255),
		nr_minuto_duracao 	number(10),
		cd_especialidade 	number(10)
   );

TYPE horarios_tws_table IS TABLE OF horarios_tws_row;

horarios_array_w        horarios_tws_table;

begin

ie_follow_up_w    		:= nvl(ie_followup_p,'T');
dt_agenda_trunc_w   	:= PKG_DATE_UTILS.start_of(dt_agenda_p,'dd');
dt_agenda_final_dia_w  	:= PKG_DATE_UTILS.end_of(dt_agenda_p,'DAY');

Horario_Livre_Consulta(cd_estabelecimento_p,cd_agenda_p,'N',dt_agenda_p,nm_usuario_p,'S','S', 'S', 0, ds_retorno_w);

horarios_array_w := horarios_tws_table();

FOR x IN (SELECT 
    a.nr_sequencia 		as nr_sequencia,
    a.dt_agenda 		as dt_agenda,
    a.ie_classif_agenda as ie_classif_agenda,
    a.nr_minuto_duracao as nr_minuto_duracao,
    b.cd_pessoa_fisica 	as cd_medico ,
    b.cd_especialidade  as cd_especialidade,
    a.nr_seq_turno		as nr_seq_turno	
    from   agenda_consulta a 
    inner join agenda b 
    on b.cd_agenda           	= a.cd_agenda
    where  	a.cd_agenda      	= cd_agenda_p
    and   	((a.ie_status_agenda = 'L') or ((a.ie_status_agenda = 'B') and a.ie_bloqueado_manual = 'N'))
    and  	a.dt_agenda   between dt_agenda_trunc_w and dt_agenda_final_dia_w
    and   ((b.nr_dias_fut_agendamento is null) or (a.dt_agenda <= fim_dia(sysdate) + b.nr_dias_fut_agendamento))
    and   	(((ie_follow_up_w = 'S')
	and exists (select 	1
				from    agenda_classif
				where 	cd_classificacao = a.ie_classif_agenda
				and  	ie_agenda in ('A','C')
				and     ie_tipo_classif in ('R','U'))) or
     ( (ie_follow_up_w = 'N')
	and exists (select 1
				from    agenda_classif
				where 	cd_classificacao = a.ie_classif_agenda
				and  	ie_agenda  in ('A','C')
				and     ie_tipo_classif not in ('R','U'))) or
     (ie_follow_up_w = 'T'))
	and ( exists ( select 1
				 from 	agenda_turno x
				 where 	x.nr_sequencia  		= a.nr_seq_turno
				 and	x.ie_utiliza_ageweb = 'S')     
         or
         exists (select 1
				 from 	agenda_turno_esp x
				 where 	x.nr_sequencia  		= a.nr_seq_turno_esp
				 and	x.ie_utiliza_ageweb = 'S'))
   order by a.dt_agenda
   ) LOOP

   consistir_classif_agecon(cd_estabelecimento_p,cd_paciente_p,dt_agenda_p,cd_agenda_p,cd_convenio_p,0,0,0,0,0,ie_consistencia_w, ie_agenda_w);

   if (ie_consistencia_w is null) then

		consistir_turno_convenio(x.nr_sequencia, cd_agenda_p, 
		dt_agenda_p, x.nr_seq_turno, 
		cd_convenio_p, null, 
		ds_consistencia_w, nm_usuario_p, 
		cd_estabelecimento_p, '');
		
	end if;

    if(ie_consistencia_w is null and ds_consistencia_w is null) then
		horarios_array_w.extend(1);
		qt_w := horarios_array_w.count;
		horarios_array_w(qt_w).nr_sequencia   		:= x.nr_sequencia;
		horarios_array_w(qt_w).dt_agenda   			:= x.dt_agenda;
		horarios_array_w(qt_w).ie_classif_agenda  	:= x.ie_classif_agenda;
		horarios_array_w(qt_w).cd_medico  			:= x.cd_medico;
		horarios_array_w(qt_w).nr_minuto_duracao 	:= x.nr_minuto_duracao;
		horarios_array_w(qt_w).cd_especialidade  	:= x.cd_especialidade;
   end if;

 end loop;

for nr_item in 1 .. horarios_array_w.count loop

	insert into wsuite_schedule (
								nr_sequencia,
								cd_agenda,
								nr_seq_agenda,
								cd_profissional,
								dt_agenda,
								hr_agenda,
								nr_min_duracao,
								nm_usuario,
								ie_classif_agenda,
								cd_tipo_agenda,
								dt_atualizacao,
								cd_especialidade
							)values (
								wsuite_schedule_seq.nextval,
								cd_agenda_p,
								horarios_array_w(nr_item).nr_sequencia,
								horarios_array_w(nr_item).cd_medico,
								horarios_array_w(nr_item).dt_agenda,
								horarios_array_w(nr_item).dt_agenda,
								horarios_array_w(nr_item).nr_minuto_duracao,
								nm_usuario_p,
								horarios_array_w(nr_item).ie_classif_agenda,
								3,
								sysdate,
								horarios_array_w(nr_item).cd_especialidade
							);
  commit;
end loop;

end tws_Horario_Livre_Consulta;
/