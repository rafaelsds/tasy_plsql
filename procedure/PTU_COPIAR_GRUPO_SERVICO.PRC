create or replace
procedure ptu_copiar_grupo_servico(	nr_seq_versao_p		Number,
					cd_estabelecimento_p	Number,
					nm_usuario_p		Varchar2) is 

begin
insert into	ptu_versao_tabela ( dt_atualizacao, dt_atualizacao_nrec, ie_situacao,
				    nm_usuario, nm_usuario_nrec, nr_seq_grupo_servico,
				    nr_sequencia, nr_seq_versao)
		select		 sysdate, sysdate, a.ie_situacao,
				 nm_usuario_p, nm_usuario_p, a.cd_grupo_servico,
				 ptu_versao_tabela_seq.nextval, nr_seq_versao_p
		from		ptu_grupo_servico a
		where		cd_estabelecimento = cd_estabelecimento_p
		and		not exists	(	select 1
							from	ptu_versao_tabela x
							where	x.nr_seq_grupo_servico = a.cd_grupo_servico
							and	x.nr_seq_versao	= nr_seq_versao_p);

commit;

end ptu_copiar_grupo_servico;
/