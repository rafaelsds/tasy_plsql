create or replace
procedure pls_liberar_ausencia_operador
			(	nr_seq_atend_ausencia_p	Number,
				nm_usuario_p		Varchar2) is 

begin

update	pls_atendimento_ausencia
set	dt_liberacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	ie_liberado	= 'S'
where	nr_sequencia	= nr_seq_atend_ausencia_p;

commit;

end pls_liberar_ausencia_operador;
/
