create or replace
procedure	atualiza_vig_itens_nf_contrato(
			nr_seq_contrato_p			number,
			nm_usuario_p			varchar2) is

dt_inicial_w		date;
dt_final_w		date;

begin

select	dt_inicio,
	dt_fim
into	dt_inicial_w,
	dt_final_w
from	contrato
where	nr_sequencia = nr_seq_contrato_p;

if	(dt_inicial_w is null) or
	(dt_final_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265584);
	--'O contrato selecionado n�o possui vig�ncia definida para atualizar os itens!');
end if;

update	contrato_regra_nf
set	dt_inicio_vigencia = dt_inicial_w,
	dt_fim_vigencia = dt_final_w,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_seq_contrato = nr_seq_contrato_p;

commit;	

end atualiza_vig_itens_nf_contrato;
/
