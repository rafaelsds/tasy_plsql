create or replace
procedure pls_obter_regra_partic_mat(	nr_seq_conta_mat_p	in pls_conta_mat.nr_sequencia%type,
					ie_tipo_tabela_p	in number,
					ie_tipo_part_regra_p	out varchar2,
					ie_comp_pacote_p	in varchar2) is 

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter regras de participa��o cadastradas na fun��o OPS - Faturamento > Cadastros > Convers�o tipo participa��o PTU > Material
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 		UTILIZADA NA ROTINA 'PLS_GERAR_PTU_NOTA_SERVICO'
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_tabela_w		number(1) := ie_tipo_tabela_p;
cd_material_ops_w		pls_material.cd_material_ops%type;
ie_tipo_despesa_mat_w		pls_conta_mat.ie_tipo_despesa%type;
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type;
ie_tipo_participacao_w		pls_regra_tp_part_ptu_mat.ie_tipo_participacao%type;
dt_atendimento_w		pls_conta_mat.dt_atendimento%type;

Cursor C01 is
	select	a.ie_tipo_participacao
	from	pls_regra_tp_part_ptu_mat	a
	where	((nvl(a.ie_tipo_tabela,ie_tipo_tabela_w) = ie_tipo_tabela_w) or ie_tipo_tabela_w is null)
	and	((nvl(a.ie_tipo_despesa_mat,ie_tipo_despesa_mat_w) = ie_tipo_despesa_mat_w) or ie_tipo_despesa_mat_w is null)
	and	((nvl(a.ie_tipo_guia,ie_tipo_guia_w) = ie_tipo_guia_w) or ie_tipo_guia_w is null)
	and	((a.cd_material_inicial	is null	and cd_material_final is null) or (cd_material_ops_w between (cd_material_inicial) and (cd_material_final)))
	and	(a.ie_comp_pacote = ie_comp_pacote_p)
	and	dt_atendimento_w between dt_inicio_vigencia and fim_dia(nvl(dt_fim_vigencia,dt_atendimento_w))
	order by
		nvl(ie_tipo_tabela,0),
		nvl(ie_tipo_despesa_mat,'X'),
		nvl(cd_material_inicial,0),
		nvl(cd_material_final,0),
		nvl(ie_tipo_guia,0);

begin
select	max(b.cd_material_ops),
	max(a.ie_tipo_despesa),
	max(c.ie_tipo_guia),
	nvl(max(a.dt_atendimento),sysdate)
into	cd_material_ops_w,
	ie_tipo_despesa_mat_w,
	ie_tipo_guia_w,
	dt_atendimento_w
from	pls_conta c,
	pls_material b,
	pls_conta_mat a
where	c.nr_sequencia	= a.nr_seq_conta
and	b.nr_sequencia	= a.nr_seq_material
and	a.nr_sequencia	= nr_seq_conta_mat_p;

if	(ie_tipo_tabela_w is null) then
	if	(ie_tipo_despesa_mat_w = 1) then
		ie_tipo_tabela_w := 2;
	elsif	(ie_tipo_despesa_mat_w = 2) then
		ie_tipo_tabela_w := 3;
	elsif	(ie_tipo_despesa_mat_w = 3) then
		ie_tipo_tabela_w := 2;
	elsif	(ie_tipo_despesa_mat_w = 7) then
		ie_tipo_tabela_w := 2;
	end if;
end if;

open C01;
loop
fetch C01 into	
	ie_tipo_part_regra_p;
exit when C01%notfound;
end loop;
close C01;

end pls_obter_regra_partic_mat;
/