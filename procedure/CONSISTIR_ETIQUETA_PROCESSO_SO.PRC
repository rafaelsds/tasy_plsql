create or replace
procedure consistir_etiqueta_processo_so	(nr_seq_processo_p	number,
					nr_etiqueta_p		number,
					ds_consistencia_p out	varchar2) is

ds_consistencia_w	varchar2(240) := '';
ie_etiq_valida_w	varchar2(1);
ie_etiq_processo_w	varchar2(1);
nr_seq_frac_w		number(10,0);
nr_seq_digito_w		number(1,0);

begin
if	(nr_seq_processo_p is not null) and
	(nr_etiqueta_p is not null) then
	
	if	(length(nr_etiqueta_p) = 12) and
		(substr(nr_etiqueta_p,1,1) = '9') then
		begin
		nr_seq_frac_w	:= to_number(substr(nr_etiqueta_p,2,10));
		nr_seq_digito_w	:= to_number(substr(nr_etiqueta_p,12,1));
				
		select	decode(count(*),0,'N','S')
		into	ie_etiq_valida_w
		from	adep_processo_frac
		where	nr_sequencia	= nr_seq_frac_w
		and	nr_seq_digito	= nr_seq_digito_w;

		if	(ie_etiq_valida_w = 'S') then
			select	decode(count(*),0,'N','S')
			into	ie_etiq_processo_w
			from	prescr_mat_hor
			where	nr_seq_processo	= nr_seq_processo_p
			and	nr_seq_etiqueta	= nr_seq_frac_w
			and	nr_seq_superior	is null
			and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

			if	(ie_etiq_processo_w = 'N') then
				ds_consistencia_w	:= wheb_mensagem_pck.get_texto(306554, null);
										/*	Esta etiqueta de ordem de manipula��o/fracionamento n�o corresponde a este processo!
											Favor verificar.	*/
			end if;
		else
			ds_consistencia_w	:= wheb_mensagem_pck.get_texto(306553, null);
									/*	Etiqueta inv�lida! Este n�mero de ordem de manipula��o/fracionamento n�o existe!
										Favor verificar.	*/
		end if;
		end;

	else
		begin
		select	decode(count(*),0,'N','S')
		into	ie_etiq_processo_w
		from	adep_processo
		where	nr_sequencia	= nr_seq_processo_p
		and	nr_prescricao || nr_seq_solucao || nr_etapa	= nr_etiqueta_p;

		if	(ie_etiq_processo_w = 'N') then
			ds_consistencia_w	:= wheb_mensagem_pck.get_texto(306541, null); 
									/* 	Esta etiqueta n�o corresponde a este processo!
										Favor verificar.	*/
		end if;
		end;
	end if;

end if;

ds_consistencia_p := ds_consistencia_w;

end consistir_etiqueta_processo_so;
/
