CREATE OR REPLACE procedure ctb_gerar_interf_k030_ecd(
			nr_seq_controle_p		number,
			nm_usuario_p			Varchar2,
			cd_estabelecimento_p		number,
			dt_inicio_p			date,
			dt_fim_p			date,
			cd_empresa_p			number,
			qt_linha_p		in out	number,
			nr_sequencia_p		in out	number) is


ie_ind_movimento_w		varchar2(2);
ds_arquivo_w			    varchar2(4000);
ds_compl_arquivo_w		varchar2(4000);
ds_linha_w			      varchar2(8000);
nr_linha_w			      number(10) := qt_linha_p;
nr_seq_registro_w		  number(10) := nr_sequencia_p;
tp_registro_w		      varchar2(15) := 'K030';
dt_ini_w			        date := dt_inicio_p;
dt_fin_w  			      date := dt_fim_p;
sep_w 				        varchar2(1) := '|';

begin
ds_linha_w	:= substr(sep_w || tp_registro_w || sep_w || to_char(dt_ini_w,'ddmmyyyy') || sep_w || to_char(dt_fin_w,'ddmmyyyy') || sep_w,1,8000);

ds_arquivo_w		:= substr(ds_linha_w,1,4000);
ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
nr_seq_registro_w	:= nr_seq_registro_w + 1;
nr_linha_w		:= nr_linha_w + 1;
insert into ctb_sped_registro(
		nr_sequencia,
		ds_arquivo,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_controle_sped,
		ds_arquivo_compl,
		cd_registro,
		nr_linha)
	values(
		nr_seq_registro_w,
		ds_arquivo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		ds_compl_arquivo_w,
		tp_registro_w,
		nr_linha_w);

commit;
qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;
end ctb_gerar_interf_k030_ecd;
/
