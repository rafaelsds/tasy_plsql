create or replace 
PROCEDURE INSERIR_PRES_ITEM_RESULT  (   nm_usuario_p pe_prescr_item_result.nm_usuario%type,
                                        nr_seq_item_p pe_item_resultado.nr_seq_item%type,
                                        nr_seq_result_p pe_item_nursing_profile.nr_seq_result%type,
                                        nr_seq_prescr_p pe_prescr_item_result.nr_seq_prescr%type)
IS
BEGIN
    INSERT INTO pe_prescr_item_result   (   NR_SEQUENCIA, 
                                            DT_ATUALIZACAO,
                                            NM_USUARIO,
                                            NR_SEQ_ITEM,
                                            NR_SEQ_RESULT,
                                            NR_SEQ_PRESCR,
                                            QT_PONTO) 
                                VALUES  (   PE_PRESCR_ITEM_RESULT_SEQ.nextval,
                                            SYSDATE,
                                            nm_usuario_p,
                                            nr_seq_item_p,
                                            nr_seq_result_p,
                                            nr_seq_prescr_p,
                                            0);
END;
/