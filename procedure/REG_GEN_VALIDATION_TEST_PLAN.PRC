CREATE OR REPLACE PROCEDURE	reg_gen_validation_test_plan(
	dt_execution_p	DATE,
	ds_version_p	VARCHAR2,
	nm_usuario_p	reg_validation_pendencies.nm_usuario%TYPE
)	AS

BEGIN
	INSERT INTO	reg_validation_pendencies(
		nr_sequencia         ,
		dt_atualizacao       ,
		nm_usuario           ,
		dt_atualizacao_nrec  ,
		nm_usuario_nrec      ,
		nr_seq_cr            ,
		nr_seq_tc            ,
		ds_version           )
	SELECT	reg_validation_pendencies_seq.NextVal,
		SYSDATE,
		nm_usuario_p,
		dt_execution_p,
		nm_usuario_p,
		nr_seq_customer,
		nr_sequencia,
		ds_version_p
	FROM	reg_caso_teste
	WHERE	nr_seq_customer IS NOT NULL
	AND	dt_liberacao_vv IS NOT NULL;
	
	COMMIT;
END reg_gen_validation_test_plan;
/