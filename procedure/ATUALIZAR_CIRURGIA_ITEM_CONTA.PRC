create or replace
procedure Atualizar_Cirurgia_Item_Conta
		(nr_seq_item_p		number,
		ie_proc_mat_p		varchar2,
		nr_cirurgia_p		number,
		nm_usuario_p		varchar2) is

nr_interno_conta_w	number(15);
ie_status_acerto_w	number(2);
ie_atualizar_resumo_w	varchar2(10);

begin


if	(ie_proc_mat_p	= 'P') then
	update	procedimento_paciente
	set	nr_cirurgia 	= nr_cirurgia_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_item_p;
	
	select	nvl(max(nr_interno_conta),0)
	into	nr_interno_conta_w
	from	procedimento_paciente a
	where	a.nr_sequencia	= nr_seq_item_p;
	
elsif	(ie_proc_mat_p	= 'M') then
	update	material_atend_paciente
	set	nr_cirurgia 	= nr_cirurgia_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_item_p;
	
	select	nvl(max(nr_interno_conta),0)
	into	nr_interno_conta_w
	from	material_atend_paciente a
	where	a.nr_sequencia	= nr_seq_item_p;
end if;
ie_atualizar_resumo_w	:= nvl(Obter_Valor_Param_Usuario(obter_funcao_ativa,242,obter_perfil_ativo,nm_usuario_p,0),'N');

if	(nvl(nr_interno_conta_w,0)	<> 0)	and
	(ie_atualizar_resumo_w		= 'S')	then
	select	ie_status_acerto
	into	ie_status_acerto_w
	from	conta_paciente
	where	 nr_interno_conta	= nr_interno_conta_w;
	
	if	(ie_status_acerto_w	= 2) then
		atualizar_resumo_conta(nr_interno_conta_w,ie_status_acerto_w);
	end if;
end if;


commit;

end Atualizar_Cirurgia_Item_Conta;
/
