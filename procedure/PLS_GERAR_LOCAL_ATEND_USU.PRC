create or replace
procedure pls_gerar_local_atend_usu(	ie_acao_p			varchar2,
					nr_seq_usuario_p		number,
					nm_usuario_web_p		varchar2,
					ds_senha_p			varchar2,
					cd_pessoa_fisica_resp_p		number,					
					nr_seq_perfil_web_p      	number,
					nr_seq_local_atend_p    	number,
					ds_email_p       		varchar2,
					ds_observacao_p			varchar2,
					ie_solicitacao_autorizacao_p	varchar2,
					ie_exige_biometria_p		varchar2,
					ie_situacao_p			varchar2,
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number) is 

begin


/*ie_acao_p
I - Insert
U - Update
*/

if	(ie_acao_p = 'I') then
	insert into pls_usuario_web(
		nr_sequencia, dt_atualizacao, nm_usuario,             
		dt_atualizacao_nrec, nm_usuario_nrec, nm_usuario_web, 
		ds_senha, cd_pessoa_fisica_resp, nr_seq_perfil_web,
		nr_seq_local_atend, ds_email, ds_observacao,
		ie_solicitacao_autorizacao, ie_exige_biometria, cd_estabelecimento,
		ie_situacao)
	values(	pls_usuario_web_seq.nextval, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, nm_usuario_web_p, 
		ds_senha_p, cd_pessoa_fisica_resp_p, nr_seq_perfil_web_p,
		nr_seq_local_atend_p, ds_email_p, ds_observacao_p,
		ie_solicitacao_autorizacao_p, ie_exige_biometria_p, cd_estabelecimento_p,
		'A'); 
		
elsif	(ie_acao_p = 'U' and nr_seq_usuario_p is not null) then
	update	pls_usuario_web
	set	dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p,
		ds_senha = ds_senha_p,
		cd_pessoa_fisica_resp = cd_pessoa_fisica_resp_p,
		nr_seq_perfil_web = nr_seq_perfil_web_p,
		nr_seq_local_atend = nr_seq_local_atend_p,
		ds_email = ds_email_p,
		ds_observacao = ds_observacao_p,
		ie_solicitacao_autorizacao = ie_solicitacao_autorizacao_p,
		ie_exige_biometria = ie_exige_biometria_p,
		ie_situacao = ie_situacao_p
	where	nr_sequencia = nr_seq_usuario_p;

end if;

commit;

end pls_gerar_local_atend_usu;
/
