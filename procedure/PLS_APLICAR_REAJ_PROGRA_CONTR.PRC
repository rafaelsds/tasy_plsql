create or replace
procedure pls_aplicar_reaj_progra_contr
			(	nr_seq_lote_prog_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

nr_seq_prog_reajuste_w		number(10);
nr_seq_contrato_w		number(10);
nr_seq_reajuste_w		number(10);
tx_reajuste_w			number(7,4);
dt_reajuste_w			date;
ie_indice_reajuste_w		varchar2(10);
nr_contrato_w			number(10);
nr_seq_lote_reajuste_w		number(10);
tx_reajuste_copartic_w		number(7,4);
tx_reajuste_copartic_max_w	number(7,4);
tx_reajuste_inscricao_w		number(7,4);
ie_reajustar_copartic_lote_w	pls_prog_reaj_colet_lote.ie_reajustar_copartic%type;
ie_reajustar_copartic_w		pls_prog_reaj_coletivo.ie_reajustar_copartic%type;
ie_reajustar_inscricao_w	varchar2(1);
ie_reajustar_vl_manutencao_w	varchar2(1);
ie_reajustar_via_adic_w		varchar2(1);
nr_seq_lote_reaj_copart_w	number(10);
nr_seq_lote_reaj_inscricao_w	number(10);
qt_regra_copartic_w		number(10);
qt_regra_inscricao_w		number(10);
nr_seq_tipo_reajuste_w		number(10);
ie_vinculo_coparticipacao_w	pls_prog_reaj_colet_lote.ie_vinculo_coparticipacao%type;
qt_lote_reaj_copartic_w		number(10);
qt_lote_reaj_insc_w		number(10);
tx_reajuste_proposto_w		pls_prog_reaj_coletivo.tx_reajuste_proposto%type;
dt_aplicacao_reajuste_w		pls_reajuste.dt_aplicacao_reajuste%type;
ie_sem_reajuste_w		pls_reajuste.ie_reaj_nao_aplicado%type;
ie_status_w			pls_reajuste.ie_status%type;
ie_gerar_rpc_mes_aniver_w	pls_parametros.ie_gerar_rpc_mes_aniver%type;

Cursor C01 is
	select	trunc(dt_reajuste,'month')
	from	pls_prog_reaj_coletivo
	where	nr_seq_lote	= nr_seq_lote_prog_p
	and	nm_usuario_liberacao is not null
	and	dt_liberacao is not null
	and	nr_seq_reajuste is null
	group by trunc(dt_reajuste,'month');

Cursor C02 is
	select	nr_sequencia,
		nr_seq_contrato,
		tx_reajuste_programado,
		ie_indice_reajuste,
		nvl(tx_reajuste_copartic, tx_reajuste_programado),
		nvl(tx_reajuste_copartic_max, tx_reajuste_programado),
		nvl(tx_reajuste_inscricao, tx_reajuste_programado),
		nr_seq_tipo_reajuste,
		nvl(ie_reajustar_copartic,'N'),
		tx_reajuste_proposto
	from	pls_prog_reaj_coletivo
	where	nr_seq_lote	= nr_seq_lote_prog_p
	and	nm_usuario_liberacao is not null
	and	dt_liberacao is not null
	and	nr_seq_reajuste is null
	and	trunc(dt_reajuste,'month')	= dt_reajuste_w;

begin

select	nvl(max(ie_gerar_rpc_mes_aniver),'N')
into	ie_gerar_rpc_mes_aniver_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

begin
select	nvl(ie_reajustar_copartic,'N'),
	nvl(ie_reajustar_vl_manutencao,'N'),
	nvl(ie_reajustar_via_adic,'N'),
	nvl(ie_reajustar_inscricao,'N'),
	nvl(ie_vinculo_coparticipacao,'A')
into	ie_reajustar_copartic_lote_w,
	ie_reajustar_vl_manutencao_w,
	ie_reajustar_via_adic_w,
	ie_reajustar_inscricao_w,
	ie_vinculo_coparticipacao_w
from	pls_prog_reaj_colet_lote
where	nr_sequencia	= nr_seq_lote_prog_p;
exception
when others then
	ie_reajustar_copartic_lote_w	:= 'N';
	ie_reajustar_vl_manutencao_w 	:= 'N';
	ie_reajustar_via_adic_w		:= 'N';
	ie_reajustar_inscricao_w	:= 'N';
	ie_vinculo_coparticipacao_w	:= 'A';
end;

open C01;
loop
fetch C01 into
	dt_reajuste_w;
exit when C01%notfound;
	begin
	select	pls_reajuste_seq.nextval
	into	nr_seq_lote_reajuste_w
	from	dual;
	
	if	(trunc(sysdate,'month') > dt_reajuste_w) and
		(ie_gerar_rpc_mes_aniver_w = 'N') then
		dt_aplicacao_reajuste_w	:= trunc(sysdate,'month');
	else
		dt_aplicacao_reajuste_w := dt_reajuste_w;
	end if;
	
	insert	into	pls_reajuste
			(	nr_sequencia, cd_estabelecimento, nr_seq_lote_reaj_colet,nr_contrato,dt_reajuste,
				tx_reajuste, ie_indice_reajuste,ie_tipo_reajuste, dt_periodo_inicial, dt_periodo_final,
				ie_status, dt_atualizacao, nm_usuario,dt_atualizacao_nrec, nm_usuario_nrec,
				ie_aplicacao_reajuste,ie_reajustar_inscricao, ie_reajustar_vl_manutencao, ie_reajustar_copartic,
				ie_reajustar_via_adic,ie_reaj_nao_aplicado, ie_tipo_contrato, ie_reajuste_tabela,
				ie_vinculo_coparticipacao, dt_reajuste_aux, dt_aplicacao_reajuste)
		values(		nr_seq_lote_reajuste_w, cd_estabelecimento_p, nr_seq_lote_prog_p,nr_contrato_w,dt_reajuste_w,
				0, '3', 'C', dt_reajuste_w, (add_months(dt_reajuste_w,11)),
				'1', sysdate, nm_usuario_p,sysdate, nm_usuario_p,
				'G',ie_reajustar_inscricao_w, ie_reajustar_vl_manutencao_w, ie_reajustar_copartic_lote_w,
				ie_reajustar_via_adic_w,'N', 'O', 'A',
				ie_vinculo_coparticipacao_w,to_char(dt_reajuste_w,'mm/yyyy'), dt_aplicacao_reajuste_w);
	
	open C02;
	loop
	fetch C02 into
		nr_seq_prog_reajuste_w,
		nr_seq_contrato_w,
		tx_reajuste_w,
		ie_indice_reajuste_w,
		tx_reajuste_copartic_w,
		tx_reajuste_copartic_max_w,
		tx_reajuste_inscricao_w,
		nr_seq_tipo_reajuste_w,
		ie_reajustar_copartic_w,
		tx_reajuste_proposto_w;
	exit when C02%notfound;
		begin
		ie_sem_reajuste_w	:= 'N';
		ie_status_w		:= '1';
		
		select	max(nr_contrato)
		into	nr_contrato_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;
		
		if	(tx_reajuste_copartic_w = 0) then
			tx_reajuste_copartic_w	:= null;
		end if;
		
		if	(tx_reajuste_copartic_max_w = 0) then
			tx_reajuste_copartic_max_w	:= null;
		end if;
		
		if	(tx_reajuste_w is null) then
			select	nvl(max(ie_sem_reajuste),'N')
			into	ie_sem_reajuste_w
			from	pls_tipo_reajuste
			where	nr_sequencia	= nr_seq_tipo_reajuste_w;
			
			if	(ie_sem_reajuste_w = 'S') then
				ie_status_w	:= '2';
			end if;
		end if;
		
		select	pls_reajuste_seq.nextval
		into	nr_seq_reajuste_w
		from	dual;
		
		insert	into	pls_reajuste
			(	nr_sequencia, cd_estabelecimento, nr_seq_lote_reaj_colet, nr_seq_contrato,nr_contrato,dt_reajuste,
				tx_reajuste, ie_indice_reajuste,ie_tipo_reajuste, dt_periodo_inicial, dt_periodo_final,
				ie_status, dt_atualizacao, nm_usuario,dt_atualizacao_nrec, nm_usuario_nrec,
				ie_aplicacao_reajuste,ie_reajustar_inscricao, ie_reajustar_vl_manutencao, ie_reajustar_copartic,
				ie_reajustar_via_adic,ie_reaj_nao_aplicado, ie_tipo_contrato, ie_reajuste_tabela,
				nr_seq_lote_referencia, tx_reajuste_copartic, tx_reajuste_copartic_max, tx_reajuste_inscricao,
				ie_vinculo_coparticipacao, dt_reajuste_aux, nr_seq_tipo_reajuste, dt_aplicacao_reajuste,
				tx_reajuste_proposto)
		values(		nr_seq_reajuste_w, cd_estabelecimento_p, nr_seq_lote_prog_p, nr_seq_contrato_w, nr_contrato_w, dt_reajuste_w,
				tx_reajuste_w, ie_indice_reajuste_w, 'C', dt_reajuste_w, (add_months(dt_reajuste_w,11)),
				ie_status_w, sysdate, nm_usuario_p,sysdate, nm_usuario_p,
				'G', ie_reajustar_inscricao_w, 'N', ie_reajustar_copartic_w,
				'N',ie_sem_reajuste_w, 'O', 'A',
				nr_seq_lote_reajuste_w, tx_reajuste_copartic_w, tx_reajuste_copartic_max_w, tx_reajuste_inscricao_w,
				ie_vinculo_coparticipacao_w, to_char(dt_reajuste_w,'mm/yyyy'), nr_seq_tipo_reajuste_w, dt_aplicacao_reajuste_w,
				tx_reajuste_proposto_w);
		
		if	(tx_reajuste_w is not null) then
			pls_inserir_tabela_reajuste(nr_seq_reajuste_w,null,nm_usuario_p);
		end if;
		
		if	(ie_reajustar_copartic_w = 'S') then
			select count (1)
			into	qt_lote_reaj_copartic_w
			from 	(      	select 	a.nr_seq_reajuste
					from	pls_lote_reaj_copartic a,
						pls_reajuste b
					where	a.nr_seq_contrato = b.nr_seq_contrato
					and   	b.nr_sequencia = a.nr_seq_reajuste
					and   	a.nr_seq_contrato	= nr_seq_contrato_w
					and	a.dt_referencia	= dt_reajuste_w
					and	a.ie_vinculo_coparticipacao = ie_vinculo_coparticipacao_w
					and 	b.nr_seq_lote_deflator is null
					and  	b.tx_reajuste_correto <> 0);
			
			if 	(qt_lote_reaj_copartic_w = 0) then
				select	pls_lote_reaj_copartic_seq.nextval
				into	nr_seq_lote_reaj_copart_w
				from	dual;
				
				insert	into	pls_lote_reaj_copartic
					(	nr_sequencia, nr_seq_contrato, cd_estabelecimento,
						dt_referencia, dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, tx_reajuste,
						tx_reajuste_vl_maximo, nr_seq_reajuste, nr_seq_intercambio,
						ie_vinculo_coparticipacao)
					values
					(	nr_seq_lote_reaj_copart_w, nr_seq_contrato_w, cd_estabelecimento_p,
						dt_reajuste_w, sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, nvl(tx_reajuste_copartic_w,tx_reajuste_w),
						nvl(tx_reajuste_copartic_max_w,tx_reajuste_w), nr_seq_reajuste_w, null,
						'A');
				
				pls_incluir_copartic_lote_reaj(nr_seq_lote_reaj_copart_w, cd_estabelecimento_p, 'N', nm_usuario_p);
				
				select	count(1)
				into	qt_regra_copartic_w
				from	pls_reajuste_copartic
				where	nr_seq_lote	= nr_seq_lote_reaj_copart_w;
				
				if	(qt_regra_copartic_w = 0) then
					delete	from	pls_lote_reaj_copartic
					where	nr_sequencia	= nr_seq_lote_reaj_copart_w;
				end if;
			end if;
		end if;
		
		if	(ie_reajustar_inscricao_w = 'S') then
			select	count(1)
			into	qt_lote_reaj_insc_w
			from	pls_lote_reaj_inscricao
			where	nr_seq_contrato	= nr_seq_contrato_w
			and	dt_referencia	= dt_reajuste_w;
			
			if 	(qt_lote_reaj_insc_w = 0) then
				select	pls_lote_reaj_inscricao_seq.nextval
				into	nr_seq_lote_reaj_inscricao_w
				from	dual;
				
				insert	into	pls_lote_reaj_inscricao
					(	nr_sequencia, nr_seq_contrato, cd_estabelecimento,
						dt_referencia, dt_atualizacao, nm_usuario,
						dt_atualizacao_nrec, nm_usuario_nrec, tx_reajuste,
						nr_seq_reajuste, nr_seq_intercambio)
					values
					(	nr_seq_lote_reaj_inscricao_w, nr_seq_contrato_w, cd_estabelecimento_p,
						dt_reajuste_w, sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, tx_reajuste_inscricao_w,
						nr_seq_reajuste_w, null);
				
				pls_incluir_inscricao_reaj(nr_seq_lote_reaj_inscricao_w, cd_estabelecimento_p, nm_usuario_p);
				
				select	count(1)
				into	qt_regra_inscricao_w
				from	pls_reajuste_inscricao
				where	nr_seq_lote	= nr_seq_lote_reaj_inscricao_w;
				
				if	(qt_regra_inscricao_w = 0) then
					delete	from	pls_lote_reaj_inscricao
					where	nr_sequencia	= nr_seq_lote_reaj_inscricao_w;
				end if;
			end if;
		end if;
		
		update	pls_prog_reaj_coletivo
		set	nr_seq_reajuste	= nr_seq_reajuste_w,
			ie_status	= 'A',
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_prog_reajuste_w;
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

update	pls_prog_reaj_colet_lote
set	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_aplicacao_reajuste	= sysdate
where	nr_sequencia		= nr_seq_lote_prog_p;

commit;

end pls_aplicar_reaj_progra_contr;
/
