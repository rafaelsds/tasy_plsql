create or replace
procedure gerar_funcao_idioma(
		cd_funcao_p	number,
		nr_seq_idioma_p	number,
		nm_usuario_p	varchar2) is
		
nm_atributo_w		varchar2(50);
ds_texto_w		varchar2(255);
cd_funcao_w		number(5,0);
nr_seq_traducao_w	number(10,0);

cursor c01 is
select	'DS_FUNCAO',
	a.ds_funcao,
	a.cd_funcao
from	funcao a
where	a.cd_funcao = cd_funcao_p
and	a.ds_funcao is not null
and	not exists (
		select	1
		from	funcao_idioma x
		where	x.cd_funcao = a.cd_funcao
		and	x.nm_atributo = 'DS_FUNCAO'
		and	x.nr_seq_idioma = nr_seq_idioma_p);
		
begin
if	(cd_funcao_p is not null) and
	(nr_seq_idioma_p is not null) and
	(nm_usuario_p is not null) then
	begin
	open c01;
	loop
	fetch c01 into	nm_atributo_w,
			ds_texto_w,
			cd_funcao_w;
	exit when c01%notfound;
		begin
		if	(trim(ds_texto_w) is not null) then
			begin
			select	funcao_idioma_seq.nextval
			into	nr_seq_traducao_w
			from	dual;
		
			insert into funcao_idioma (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nm_atributo,
				ds_descricao,
				cd_funcao,
				nr_seq_idioma,			
				ds_traducao,
				ie_necessita_revisao)
			values (
				nr_seq_traducao_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nm_atributo_w,
				ds_texto_w,
				cd_funcao_w,
				nr_seq_idioma_p,
				' ',
				'T');
			end;
		end if;
		end;
	end loop;
	close c01;	
	end;
end if;
commit;
end gerar_funcao_idioma;
/