create or replace
procedure CTB_GERAR_BALANCETE_ECD(	cd_empresa_p		number, 
					cd_estab_p		number, 
					nr_seq_mes_ref_p	number,
					cd_grupo_p		number,
					nm_usuario_p		Varchar2,
					ie_informacao_p		Varchar2 default 'A',
					ie_normal_encerramento_p	varchar2) is 


cd_classif_ecd_w			varchar2(40);
cd_conta_contabil_w 			varchar2(20);
cd_grupo_w				number(10);
ds_conta_referencial_w			varchar2(255);
nr_nivel_w				number(10);
vl_movimento_w 				number(15,2);
vl_debito_w				number(15,2);
vl_credito_w				number(15,2);
vl_saldo_ant_w				number(15,2);
vl_saldo_w				number(15,2);
ie_normal_encerramento_w		varchar2(2) := nvl(ie_normal_encerramento_p, 'N');



cursor c01 is

select	c.cd_classificacao,
	c.ds_conta_referencial,
	a.cd_conta_contabil,
	sum(decode(ie_normal_encerramento_w, 'N',a.vl_movimento, 'E', a.vl_movimento - a.vl_encerramento)),
	sum(a.vl_debito),
	sum(a.vl_credito),
	sum(decode(ie_normal_encerramento_w, 'N',a.vl_saldo, 'E', a.vl_saldo - a.vl_encerramento))	
from	conta_contabil d,
	conta_contabil_referencial c,
	conta_contabil_classif_ecd b,
	ctb_saldo a
where	a.cd_conta_contabil		= b.cd_conta_contabil
and	c.cd_classificacao		= b.cd_classif_ecd
and	d.cd_conta_contabil		= a.cd_conta_contabil
and	d.cd_conta_contabil		= b.cd_conta_contabil
and	(d.cd_grupo			= cd_grupo_w or cd_grupo_w = 0)
and	((a.cd_estabelecimento		= nvl(cd_estab_p,0))
or	(nvl(cd_estab_p,0) = 0))
and	a.nr_seq_mes_ref		= nr_seq_mes_ref_p
and 	ie_informacao_p			= 'A'
group by c.cd_classificacao, c.ds_conta_referencial, a.cd_conta_contabil
union all
select	c.cd_classificacao,
	c.ds_conta_referencial,
	'' cd_conta_contabil,
	sum(decode(ie_normal_encerramento_w, 'N',a.vl_movimento, 'E', a.vl_movimento - a.vl_encerramento)),
	sum(a.vl_debito),
	sum(a.vl_credito),
	sum(decode(ie_normal_encerramento_w, 'N',a.vl_saldo, 'E', a.vl_saldo - a.vl_encerramento))	
from	conta_contabil d,
	conta_contabil_referencial c,
	conta_contabil_classif_ecd b,
	ctb_saldo a
where	a.cd_conta_contabil		= b.cd_conta_contabil
and	c.cd_classificacao		= b.cd_classif_ecd
and	d.cd_conta_contabil		= a.cd_conta_contabil
and	d.cd_conta_contabil		= b.cd_conta_contabil
and	(d.cd_grupo			= cd_grupo_w or cd_grupo_w = 0)
and	((a.cd_estabelecimento		= nvl(cd_estab_p,0))
or	(nvl(cd_estab_p,0) = 0))
and	a.nr_seq_mes_ref		= nr_seq_mes_ref_p
and 	ie_informacao_p			= 'S'
group by c.cd_classificacao, c.ds_conta_referencial;

begin

cd_grupo_w	:= nvl(cd_grupo_p,0);
vl_saldo_ant_w	:= 0;

delete 	w_ctb_balancete
where	nm_usuario	= nm_usuario_p;


open c01;

loop
fetch c01 into
	cd_classif_ecd_w,
	ds_conta_referencial_w,
	cd_conta_contabil_w,
	vl_movimento_w,
	vl_debito_w,
	vl_credito_w,
	vl_saldo_w;
exit when c01%notfound;
	begin
	nr_nivel_w		:= CTB_OBTER_NIVEL_CLASSIF_CONTA(cd_classif_ecd_w);
	ds_conta_referencial_w	:= substr(lpad(' ', 2 * nr_nivel_w) || ds_conta_referencial_w,1,254);
	
	vl_saldo_ant_w	:= vl_saldo_w - vl_movimento_w;	
	
	insert into w_ctb_balancete(
		cd_empresa,
		cd_estabelecimento,
		cd_conta_contabil,
		cd_classificacao,
		ds_conta_apres,
		vl_saldo_ant,
		vl_debito,
		vl_credito,
		vl_saldo,
		vl_movimento,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario)
	values	(cd_empresa_p,
		cd_estab_p,
		cd_conta_contabil_w,
		cd_classif_ecd_w,
		ds_conta_referencial_w,
		nvl(vl_saldo_ant_w,0),
		nvl(vl_debito_w,0),
		nvl(vl_credito_w,0),
		nvl(vl_saldo_w,0),
		nvl(vl_movimento_w,0),
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p);
	end;
end loop;
close c01;

commit;

end CTB_GERAR_BALANCETE_ECD;
/