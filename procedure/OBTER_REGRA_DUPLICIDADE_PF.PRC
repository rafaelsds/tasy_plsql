create or replace
procedure obter_regra_duplicidade_pf (	cd_pessoa_fisica_p	varchar2,
					nm_pessoa_fisica_p	varchar2,
					dt_nascimento_p		date,
					nm_mae_p		varchar2,
					cd_estabelecimento_p	number,
					ie_acao_p	out	varchar2,
					cd_perfil_p	in	number) is

nm_pessoa_param_w	varchar2(255);
nm_mae_param_w		varchar2(255);
cd_pessoa_duplic_w	varchar2(255);
ie_acao_w		varchar2(10);
ie_nome_mae_w		varchar2(10);
ie_dt_nascimento_w	varchar2(10);

cursor c02 is
select	ie_acao,
	ie_nome_mae,
	ie_dt_nascimento
from	regra_duplic_pf
where	cd_estabelecimento	= cd_estabelecimento_p
and	nvl(cd_perfil, cd_perfil_p) = cd_perfil_p
order 	by nvl(cd_perfil, 0);


begin

	open c02;
	loop
	fetch c02 into
		ie_acao_w,
		ie_nome_mae_w,
		ie_dt_nascimento_w;
	exit when c02%notfound;
	end loop;
	close c02;
	
	nm_pessoa_param_w	:= substr(padronizar_nome(nm_pessoa_fisica_p),1,60);
	nm_mae_param_w		:= substr(padronizar_nome(nm_mae_p),1,60);

	if	(ie_nome_mae_w = 'N') and
		(ie_dt_nascimento_w = 'N')	then
		
		select	max(a.cd_pessoa_fisica)
		into 	cd_pessoa_duplic_w
		from	pessoa_fisica a
		where	a.cd_pessoa_fisica 		<> cd_pessoa_fisica_p
		and	a.nm_pessoa_pesquisa		= upper(nm_pessoa_param_w)
		and	nvl(ie_nome_mae_w,'N')		= 'N'
		and	nvl(ie_dt_nascimento_w,'N')	= 'N'
		and	nvl(ie_status_usuario_event,'A') <> 'I';
	
	elsif	(ie_nome_mae_w = 'S') and
		(ie_dt_nascimento_w = 'S')	then

		select	max(a.cd_pessoa_fisica)
		into 	cd_pessoa_duplic_w
		from	compl_pessoa_fisica b,
			pessoa_fisica a
		where	b.ie_tipo_complemento		= 5
		and	b.cd_pessoa_fisica		= a.cd_pessoa_fisica
		and	a.cd_pessoa_fisica 		<> cd_pessoa_fisica_p
		and	a.dt_nascimento			= dt_nascimento_p
		and	a.nm_pessoa_pesquisa		= upper(nm_pessoa_param_w)
		and	b.nm_contato_pesquisa		= upper(nm_mae_param_w)
		and	nvl(ie_nome_mae_w,'N')		= 'S'
		and	nvl(ie_dt_nascimento_w,'N')	= 'S'
		and	nvl(ie_status_usuario_event,'A') <> 'I';

	elsif	(ie_nome_mae_w = 'N') and
		(ie_dt_nascimento_w = 'S')	then

		select	max(a.cd_pessoa_fisica)
		into 	cd_pessoa_duplic_w
		from	pessoa_fisica a
		where	a.cd_pessoa_fisica 		<> cd_pessoa_fisica_p
		and	a.nm_pessoa_pesquisa		= upper(nm_pessoa_param_w)
		and	a.dt_nascimento			= dt_nascimento_p
		and	nvl(ie_dt_nascimento_w,'N')	= 'S'
		and	nvl(ie_nome_mae_w,'N')		= 'N'
		and	nvl(ie_status_usuario_event,'A') <> 'I';
	
	elsif	(ie_nome_mae_w = 'S') and
		(ie_dt_nascimento_w = 'N')	then
		
		select	max(a.cd_pessoa_fisica)
		into 	cd_pessoa_duplic_w
		from	compl_pessoa_fisica b,
			pessoa_fisica a
		where	b.ie_tipo_complemento		= 5
		and	b.cd_pessoa_fisica		= a.cd_pessoa_fisica
		and	a.cd_pessoa_fisica 		<> cd_pessoa_fisica_p
		and	a.nm_pessoa_pesquisa		= upper(nm_pessoa_param_w)
		and	b.nm_contato_pesquisa		= upper(nm_mae_param_w)
		and	nvl(ie_nome_mae_w,'N')		= 'S'
		and	nvl(ie_dt_nascimento_w,'N')	= 'N'
		and	nvl(ie_status_usuario_event,'A') <> 'I';
		
	end if;
		
	if	(ie_acao_w is not null) and
		(cd_pessoa_duplic_w is not null) then
		ie_acao_p	:= ie_acao_w;
	end if;

end obter_regra_duplicidade_pf;
/
