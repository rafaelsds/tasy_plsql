create or replace
procedure atualizar_sequencia_cheque_cpa(   cd_conta_p		number,
					nr_cheque_p	Varchar2) is 

ie_obter_nr_cheque	varchar2(1);
ie_gerar_nr_cheque	varchar2(1);

begin

obter_param_usuario(127,36,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_obter_nr_cheque);
obter_param_usuario(127,42,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_gerar_nr_cheque);

if ((nvl(ie_obter_nr_cheque,'N') = 'S') and (nvl(ie_gerar_nr_cheque,'N') = 'S')) then
	
	if (nr_cheque_p is not null) then
		update	banco_estabelecimento
		set		nr_ultimo_cheque = nr_cheque_p
		where	nr_sequencia = cd_conta_p;
	end if;

	commit;
end if;

end atualizar_sequencia_cheque_cpa;
/